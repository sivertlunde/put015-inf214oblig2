Mr. and Mrs. Gambler
 
 
{{Infobox film
| name           = Mr. and Mrs. Gambler
| image          = MrMrsGambler.jpg
| alt            = 
| caption        = Film poster
| film name      = {{Film name
 | traditional    = 爛賭夫鬥爛賭妻
 | simplified     = 烂赌夫斗烂赌妻
 | pinyin         = Làn Dǔ Fū Dòu Làn Dǔ Qī
 | jyutping       = Laan6 Dou2 Fu1 Dau3 Laan6 Dou2 Cai1 }}
| director       = Wong Jing
| producer       = Wong Jing
| writer         = Wong Jing
| starring       = Chapman To Fiona Sit
| music          = Tang Chi Wai Ben Chong
| cinematography = Edmond Fung
| editing        = Lee Ka Wing
| studio         = Jings Movie Production
| distributor    = Gala Films Distribution
| released       =  
| runtime        = 110 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          = HK$5,827,572
}}

Mr. and Mrs. Gambler is a 2012 Hong Kong romantic comedy film written, produced and directed by Wong Jing and starring Chapman To and Fiona Sit.

==Plot==
Manfred and Flora are compulsive gamblers who can gamble on anything 24 hours a day, seven days a week. They meet at a casino in Macau, where they both suffer heavy losses and are held hostage by loan sharks. During their hostage ordeal, they start to have passion for each other. When they meet in Hong Kong again, they finally fall in love and decided to get married. After they married, their luck turned, making great progress in both their career and relationship. They soon gave birth to their daughter. With assistance of a charming producer, Michelle, Manfred got the chance to take a leading role in a new movie. The Casino Boss, Sam, invited Flora to work for him to chase away the con men. In fact, Michelle and Sam are ex-lovers. Both of them are now attracted by Manfred and Flora’s unique characters and go for them in full strength. Manfred and Flora cannot resist the temptation and decided to get divorced. However, both of them would like to obtain their daughter’s custody rights.

==Cast==
{| width ="70%" class="wikitable"
|- align=center
|style="width:25%"|Cast||style="width:25%"|Role||Description
|- ludomania Film actor Won a Best Actor award in Africa Sister Sans son Flora Cheungs husband Shu Siu Sius father Pursued by Michelle Siu Competed with Flora Cheung of one week without gambling to win his daughters custody Later forfeited out Father of three children Acted in Peerless Grandmaster 2012
|-
| Fiona Sit || Flora Cheung 張惠香 || Suffers from ludomania Sam Wongs ex-assistant Manfred Cheungs wife Shu Siu Sius mother Pursued by Sam Wong Competed with Manfred Shu of one week without gambling to win her daughters custody Canceled divorce later Mother of three children
|-
| Law Kar-ying || Shu Tak 舒突 || Extra actor Manfred and Shu Chings father Shu Siu Sius grandfather
|-
| Mimi Chu || Pauline Cheung || Flora Cheungs mother Shu Siu Sius grandmother
|-
| Sheh Cheuk Wing || Shu Siu Siu 舒小小 || Manfred Shu and Flora Cheungs daughter
|-
| Michelle Hu || Michell Siu || Film director Sam Wongs ex-girlfriend Invited Manfred Shu to be her films lead actor Pursues Manfred Shu Flora Cheungs love rival
|-
| Philip Ng || Sam Wong || Casino owner Diamond bachelor Michelle Sius ex-girlfriend Recruited Flora Cheung to fight of swindlers Pursues Flora Cheung Manfred Shus love rival
|-
| Wan Chiu || Chiu Na Sing 超那星 || Manfred Shus friend Manfred Shus best man at his wedding Shu Chings boyfriend
|-
| Zuki Lee || Shu Ching 舒青 || Manfred Shus younger sister Chiu Na Sings girlfriend Accepted Sam Wongs HK$3 million bribe to betray Manfred Shu
|-
| King Kong || Kam Nga Kwai 金牙貴 || Debt collector
|-
| Elena Kong || Judge 法官 || Ordered Manfred Shu and Flora Cheung to competed against each other of one week without gambling to win their daughters custody
|-
| Bonnie Wong || Sister San 珊姐 || Shu Taks wife Manfred and Shu Chings mother Suffers from alzheimers disease
|-
| Benz Hui || || Cafe owner Cooperates with Flora Cheung to open another cafe
|-
| Harriet Yeung || Vulgarlina 核突縺娜 || Flora Cheungs friend
|-
| Maria Cordero || Aunt Ten 十姨 || Fortune teller Offered fortune telling for Flora Cheung
|-
| Matt Chow || Wong Ching Wai 王晶衛 || International film director Beaten up by Michelle Sius henchmen
|-
| Yuen Cheung-yan || || Crazy guy
|-
| Victy Wong || || Kams man
|-
| Lee Hung Kei || || Kams man
|-
| Chan Ka Kai || || Fat Girl
|-
| Andrew Dasz || || Muscle Guy
|-
| Steven Dasz || || Waiter
|-
| Lee Kin Hing || || Casino Gambler
|-
| Michelle Wai || || (Cameo)
|-
| Wan Kwong || || (Cameo)
|-
| Bernard Chueng || Duncan || Assistant director
|-
| Fong Ho Yuen || || Cameraman
|-
| Shek Kit Wah || || Fat boy
|-
| Kevin Lui || || Best man
|-
| Ricky Lee || || Best man
|-
| Lai Tung Hong || || Best man
|-
|}

==Theme song==
Compulsive Gambling God (爛賭神君)
**Composer: Wu Man Sam
**Lyricist: Wan Kwong
**Singer: Wan Kwong, Chapman To

==Sequel==
Due to the films commercial success, director Wong Jing has decided to make a sequel, titled Mr. and Mrs Player (爛滾夫鬥爛滾妻) with Chapman To returning while the female lead will be Chrissie Chau.  The film is set for release on 12 September 2013. 

==References==
 

==External links==
* 
*  at Hong Kong Cinemagic
* 
*  at LoveHKFilm.com

 
 

 
 
 
 
 
 
 
 
 
 
 