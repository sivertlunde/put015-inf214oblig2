Grumpier Old Men
{{Infobox film
| title          = Grumpier Old Men
| image          = Grumpier old menposter.jpg
| caption        = Theatrical release poster
| director       = Howard Deutch John Davis Richard C. Berman
| writer         = Mark Steven Johnson
| starring       = Jack Lemmon Walter Matthau Ann-Margret Ann Guilbert Sophia Loren Kevin Pollak Daryl Hannah Burgess Meredith
| music          = Alan Silvestri
| cinematography = Tak Fujimoto
| editing        = Billy Weber Seth Flaum Maryann Brandon Warner Bros. Pictures
| released       =  
| runtime        = 101 minutes
| country        = United States Italian
| budget         = $25 million
| gross          = $71,518,503
}}
 Grumpy Old Men. The film stars Jack Lemmon, Walter Matthau, Ann-Margret, and Sophia Loren, with Burgess Meredith, Daryl Hannah, Kevin Pollak, Katie Sagona, Ann Morgan Guilbert. Grumpier Old Men was directed by Howard Deutch, with the screenplay written by Mark Steven Johnson and the original music score composed by Alan Silvestri.  The film was Merediths final motion picture appearance. He was already suffering from Alzheimers disease and had to be gently coached through his role in the film.

==Plot==
The lifelong feud between Max (Walter Matthau) and John (Jack Lemmon) has cooled. (They continue to call each other "moron" and "putz," but now with affection.) Their children, Melanie (Daryl Hannah) and Jacob (Kevin Pollak), who grew up together, have become engaged after a brief relationship. Meanwhile, John is enjoying his marriage to new wife Ariel (Ann-Margret).

The spring and summer fishing season is in full swing in Wabasha, Minnesota, with the annual quest to catch "Catfish Hunter," the lakes largest catfish, consuming the fishing community. However, the local bait shop closed after the death of its proprietor Chuck in the first film. A new arrival to Wabasha, Maria Ragetti (Sophia Loren), has purchased the property with the intent of converting it into a fancy Italian restaurant.

Irritated it will no longer be a bait shop, Max and John join forces to sabotage the restaurant. They are successful with cruel practical jokes in keeping others from trying Marias restaurant. However, when Ariel learns the truth, she tells John to apologize to Maria at once. He eventually does, but passes out at the restaurant. Max and Maria begin dating due to their shared passion in fishing, while Francesca (Ann Morgan Guilbert) dates Johns father (Burgess Meredith).

To complicate things further, Jacob and Melanies wedding plans are causing the couple stress because of their fathers wedding arrangements, so they call off their engagement. Upon hearing the news, John and Max call off their truce and reignite their feud. This causes Ariel so much stress, she leaves John and moves back into her old home.

At the restaurant, Francesca is worried that the more time Maria spends with Max, the more shell get hurt. She reminds her daughter of her five failed marriages and her fears of Max making it six. After being convinced to take a long look at herself, Maria reluctantly stops seeing Max.

Distraught over losing Ariel, John heads to the lake for his fathers advice. He finds his dad has died in his favorite fishing spot.  Following the funeral, John and Max call off their feud again. After realizing that their own inability to properly plan a wedding is what drove their kids to call it off, Max and John once more join forces to set things right. They help Jacob reconcile with Melanie, and soon  John reunites with Ariel while Max rekindles his relationship with Maria, whom he marries. Due to Maxs extensive knowledge of the types of bait for fish, Ragettis becomes a combination Italian restaurant and bait shop.

==Subplots==
Like the first movie, several subplots are explored, like Jacob and Melanies struggles to keep their cool as they become more stressed and frustrated over both John and Maxs inability to properly plan an appropriate wedding. Near the end, both Max and John realize their mistake and let their children properly plan their wedding. In the end they decide to simply elope to Las Vegas.

Johns attempts to help Max calm his nerves for his first date with Maria at Ragettis. He later voices his concerns to Ariel about giving Max the wrong advice in being himself and she reassures John that hell be fine.

A preschool Allie struggles to accept Jacob as a part of her life after her parents divorce. She soon accepts him and helps John and Max to bring Jacob and Melanie back together.

Max and Ariel assumes that John is having an affair with Maria and head to Ragettis to confront him. Francesca puts a stop to it and reveals everything. This includes her own relationship with John Sr. and Maria was being a friend to John in having him stay at the restaurant to sleep off the grappa knowing he wouldve been unstable to drive home. She also mentions Marias infatuation with Max, much to her embarrassment and Maxs shock as he earlier assumed she was a lesbian.

John Sr. comments that while many people he had come to know had passed on, he questions whether God forgot about him. He soon dies on the same fishing spot, which John commented "Looks like God remembered you, pop." Unlike his initial plans for the all the women in the first movie, John Sr.s perverted side is largely due to him wanting to have a relationship with Francesca Ragetti. He eventually succeeds with her finally being charmed by his overtures and Francesca leaves a rose in the lake.

Max stops by to speak to Maria to find out why she ended their relationship, only to find out she was married 5 times and it had ended in divorce. He previously became aware in one of her failed marriages, Antonio. Earlier in the movie at the closed down bait shop, Francesca angrily mentions him to Maria when she tried to calm her down and leave Max alone about the wine. However, he didnt make the connection about Antonio being one of Marias failed marriages until John finally told him the truth about it and convinced him to tell Maria how he feels. Then Max explained to her that he is loyal, honest and knows how to treat a lady. He proceeds to convince Francesca to give him a chance to treat Maria right, which she reluctantly agrees.

On the way to the wedding, John and Max makes a side trip to catch Catfish Hunter. Though they succeed, John convinces Max to return Catfish Hunter to the lake, revealing his father tried to catch him for over 20 years and both of them deserve to be in the lake together. The fishs name is a spoof of a former Major League Baseball pitching star, Catfish Hunter.

John pays Max back for his earlier prank in the first movie by sneaking his dog, Lucky, in the limousine

==Cast==
* Walter Matthau as Max Goldman
* Jack Lemmon as John Gustafson Jr.
* Ann-Margret as Ariel Truax Gustafson
* Sophia Loren as Maria Sophia Coletta Raghetti Goldman
* Ann Morgan Guilbert as Francesca "Mama" Ragetti
* Burgess Meredith as John Gustafson Sr.
* Daryl Hannah as Melanie Gustafson Goldman
* Kevin Pollak as Jacob Goldman
* Katie Sagona as Allie (Melanies daughter)

==Reception==

=== Box office ===
Grumpier Old Men grossed $71 million at the North American box office, against a production budget of $25 million.   Grumpier Old Men beat its predecessors total of $70 million and cost $10 million less to make than the original.

=== Critical response ===
The film received negative reviews. Rotten Tomatoes gave the film a score of 18% based on reviews from 17 critics.   Roger Ebert gave the film a score of 2 out of 4 stars. 
Kevin Thomas of the Los Angeles Times described the film as contrived and getting by on the star power of the cast. 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 