Our Dancing Daughters
{{Infobox film
| name           = Our Dancing Daughters
| image          = File:Our Dancing Daughters lobby card.jpg
| caption        = Lobby card
| director       = Harry Beaumont
| producer       = Hunt Stromberg
| writer         = Josephine Lovett Marion Ainslee Ruth Cummings John Mack Brown
| music          = William Axt George Barnes William Hamilton
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = Silent film English intertitles
| budget         = $178,000  . 
| gross          = $1,099,000 
}} silent drama John Mack Brown (later billed after a career decline as "Johnny Mack Brown"), about the "loosening of youth morals" that took place during the 1920s. The film was directed by Harry Beaumont and produced by Hunt Stromberg. This was the film that made Joan Crawford a major star, a position she held for the following half century.

While the film has no audible dialog, it was released with a synchronized soundtrack and sound effects.

==Plot== John Mack Brown). He takes Dianas flirtatious behavior with other boys as a sign of uninterest in him and marries Ann.  Diana becomes distraught for a while. Later, Diana throws a party which Ann hopes to attend with her lover, Freddie (Edward J. Nugent). She gets into an argument with her husband about the party but attends anyway; Ben attends behind Anns back.  Ben and Diana realize their love for each other, and, when Ann falls to her death due to drunkenness, the two are free to unite. Others in the cast include Nils Asther as Norman, Dorothy Sebastian as Beatrice, and Evelyn Hall and Sam De Grasse as Freddies parents.

==Cast==
* Joan Crawford as Diana Di Medford John Mack Brown as Ben Blaine
* Nils Asther as Norman
* Dorothy Sebastian as Beatrice Bea
* Anita Page as Ann Annikins
* Kathlyn Williams as Anns mother
* Edward J. Nugent as Freddie (billed as Edward Nugent)
* Dorothy Cumming as Dianas mother
* Huntley Gordon as Dianas father (billed as Huntly Gordon)
* Evelyn Hall as Freddies mother
* Sam De Grasse as Freddies father (billed as Sam de Grasse)

==Reception==
Bland Johnson in the New York Mirror commented, "Joan Crawford...does the greatest work of her career."  

===Box Office===
According to MGM records the film earned $757,000 in the US and Canada and $342,000 elsewhere resulting in a profit of $304,000. 

==DVD release==
This was released in 2010 on DVD.  

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 