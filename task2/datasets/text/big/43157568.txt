The Second Face
{{Infobox film
| name           = The Second Face
| image          = 
| image_size     = 
| caption        = 
| director       = Jack Bernhard
| producer       = Edward Leven
| writer         = Eugene Vale
| story          = 
| based on       =
| narrator       = 
| starring       = Ella Raines Bruce Bennett Rita Johnson
| music          = 
| cinematography = 
| editing        = 
| studio         = EJL Productions, Inc.
| distributor    = Eagle-Lion Classics, Inc.
| released       =  
| runtime        = 77 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
The Second Face (1950) is a romantic drama film starring Ella Raines as a plain woman whose live is drastically changed by plastic surgery.

==Plot== flashback ensues.

Phyllis works as a secretary to Paul Curtis in Fresno, California. Paul is attracted to her, despite her plainness, but is afraid to risk another potentially disastrous marriage (he is divorced). Mrs. Lockridge, his longtime housekeeper, is unable to get him to change his mind. As Phyllis has just graduated from design school, Paul puts her in touch with Claire Elwood, who works in a fashionable Los Angeles dress shop; Claires boss, Floyd Moran, is impressed by Phylliss dress designs, and offers her a job as a salesperson, with a promotion to designer if she works out.

Claire arranges a double date for Phyllis before she arrives, but Jerry Allison, Phylliss blind date, makes an excuse when he sees what she looks like. Moran, also put off by her looks, does not give her the promised job. When Phyllis is unable to find work, Claire has her fiance, advertising man Allan Wesson, take her on as a secretary.

Jerry uses his contacts to get Phylliss dresses manufactured, but under his name as the designer. A philanderer, he keeps her at a distance.

Meanwhile, Allan desperately needs to land the account of Mr. Hamilton. Jerry suggests he get to Hamilton through his attractive blonde daughter Lynn. Soon, Allan is seeing Lynn secretly, and he gets Hamilton as a client.

Paul has to stay in Los Angeles for several months for his work. He is soon a frequent visitor to the apartment Phyllis shares with Claire. She pretends she has been going out every night on dates, but he is not fooled. Jerry finally agrees to a date, but she accidentally overhears him telling Allan he intends to take her to a drive-in movie, where no one can see them together. She confronts him, then storms out. He goes to her apartment to make sure she is safe; suspecting that Jerry has done something to upset Phyllis, Paul punches him. When Phyllis returns, Paul asks her to return with him to Fresno, but she tells him he is the last person she could ever love. After he leaves, when questioned by Claire, she explains that she believes he does not love her, but is only interested in her because he believes her unattractiveness will ensure that she will not leave him, as his wife had done.

Allan arranges to fly to Honolulu with Lynn to get married. He sends Phyllis a telegram to ask her to break the news to Claire. Claire reads the telegram, grabs a pistol and drives away, pursued by Phyllis.

After the crash, plastic surgeon Dr. Vaughn is called in to repair Phylliss face. He does a wonderful job, making her beautiful. Soon she is seeing wealthy men like Todd Williams. She also visits Moran, demands ten times the salary she was to receive when she was plain, and gets it.

She discovers that her hospital bills have been paid by an anonymous benefactor. Through Mr. West, she is surprised to learn that the last check was signed by Jerry. They become engaged, but Claire knows Jerry too well. She gets Mrs. Lockridge to admit that it was actually Paul who paid. Phyllis rushes back to Fresno, but he says that he believes she is coming to him only out of gratitude for his financial assistance. He also admits that he fears that now that she is attractive, she might some day leave him.  As she prepares to leave, he relents and they embrace.

==Cast==
* Ella Raines as Phyllis Holmes
* Bruce Bennett as Paul Curtis
* Rita Johnson as Claire Elwood John Sutton as Jerry Allison
* Patricia Knight as Lynn Hamilton
* Roy Roberts as Allan Wesson
* Jane Darwell as Mrs. Lockridge
* Paul Cavanagh as Todd Williams
* Frances Karath as Annie Curtis, Pauls young daughter
* Mauritz Hugo as Dr. Vaughn
* Pierre Watkin as Mr. Hamilton (as Pierre Watkins) Charles Lane as Mr. West
* Grandon Rhodes as Floyd Moran

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 