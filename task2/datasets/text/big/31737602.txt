Rajapattai
 
 

{{Infobox film
| name           = Rajapattai
| image          = Rajapattai.jpg
| caption        = Promotional poster
| director       = Suseenthiran
| producer       = Prasad V. Potluri
| writer         = Bhaskar Sakthi   (dialogues)
| story          = Seenu Vasan
| screenplay     = Suseenthiran Vikram Deeksha Seth K. Vishwanath
| music          = Yuvan Shankar Raja
| cinematography = R. Madhi
| editing        = Mu. Kasi Viswanathan PVP Cinema
| distributor    = Gemini Film Circuit   
| runtime        = 133 minutes
| released       =   
| country        = India
| language       = Tamil
| budget         = 
| gross = 
}}
 Tamil masala Vikram and Prasad V. Telugu and was released as Veedinthe on 30 December.  The film received largely negative reviews and was flop at the box office.

==Plot== Pradeep Rawat). Murugan eventually falls in love with Dharshini (Deeksha Seth). The one-man army Murugan becomes the Dakshinamoorthy’s saviour as Ranganayaki and her men are after them. How Murugan single-handedly fights the powerful politician and brings them to light forms the rest of the story.

==Cast== Vikram as Anal Murugan
* Deeksha Seth as Dharshini
* K. Vishwanath as Dakshanamurthy Pradeep Rawat as Abdul Kadhir alias Vaappa
* Sana as Ranganayaki alias Akka
* Avinash as Chidambaram
* Thambi Ramaiah as Shanmugham
* Aruldass
* Jayaprakash in a guest appearance
* Saloni Aswani in a special appearance
* Shriya Saran in a special appearance
* Reemma Sen in a special appearance
* Ramachandran Durairaj

==Production==

===Development=== Vikram and masala film, with action, comedy and romance,     further adding that the dialogues would be penned by Bhaskar Sakthi.  By September 2011, reports confirmed that the film had been taken over by Prasad V. Potluris PVP Cinemas, after Ramesh faced financial restraints. 

===Casting=== Abhinaya were Pradeep Rawat, National Film Naan Mahaan Alla, were roped in for other supporting roles.  Suseenthiran also confirmed that he would rejoin with the Naan Mahaan Alla crew, with Yuvan Shankar Raja, R. Madhi and Mu. Kasi Viswanathan taking care of the music, cinematography and editing, respectively.

===Filming===
Filming began in June 2011, with the first schedule, during which filming was held in and around Chennai, lasting for 60 days.    In August 2011, the first song was shot for five days in   scene that was canned for eight days at the Binny Mills in Chennai,  had reportedly cost approximately   30&nbsp;million.   During the last week of October and early November, Saloni Aswani, who had previously starred in Madurai Veeran, was shooting for the song "Villadhi Villain" along with Vikram at the AVM Studios, Chennai.   The item number, featuring Shriya Saran and Reemma Sen, was initially to be filmed in Japan,  however, since Suseenthiran was not satisfied with the locations, he decided to can it in Lucca, Italy.    Other locations in Italy where filming was held include Volterra, Monteriggioni and Montecatini Terme. 

==Soundtrack==
{{Infobox album
| Name         = Rajapattai
| Longtype     = to Rajapattai
| Type         = Soundtrack
| Artist       = Yuvan Shankar Raja
| Cover        = Rajapattai cd cover.jpeg
| Border       = yes
| Alt          =
| Caption      =
| Released     = 7 December 2011  (Telugu version)  9 December 2011  (Tamil version) 
| Recorded     = 2011 Feature film soundtrack
| Length       = 23:10 Tamil
| Label        = Gemini Audio
| Producer     = Yuvan Shankar Raja
| Reviews      =
| Last album   = Kazhugu (2012 film)|Kazhugu   (2012)
| This album   = Rajapattai   (2011)
| Next album   = Vettai   (2012)
}}

Rajapattais score and soundtrack are composed by Yuvan Shankar Raja, becoming his first work for a Vikram (actor)|Vikram-starrer.  Since Yuvan Shankar Rajas previous single tracks from Vaanam and Mankatha had met with success,  the team planned to release a single track from Rajapattai, too, by late October.  However, the plan did not materialize. The final mixing of the songs was planned to be held in London. 
 Krish and Anna Centenary Auditorium in Kotturpuram, Chennai.  
 second season of the reality|reality-based singing competition Airtel Super Singer.

Tracklist
{{track listing
| extra_column   = Singer(s)
| total_length   = 23:10
| all_lyrics     = Yugabharathi

| title1      = Podi Paiyan Polave (I)
| extra1      = Haricharan
| length1     = 04:36

| title2      = Villathi Villain
| extra2      = Mano (singer)|Mano, Malathi
| length2     = 04:34

| title3      = Paniye Panipoove
| extra3      = Javed Ali, Renu Kannan
| length3     = 04:51

| title4      = Laddu Laddu Rendu Laddu Priyadarshini
| length4     = 04:37

| title5      = Podi Paiyan Polave (II)
| extra5      = Haricharan
| length5     = 04:36
}}

==Release==
In early November 2011, reports confirmed that Sun Pictures had bought the domestical distribution rights of the film.  However two weeks later, Sun Pictures cancelled the deal and opted out.  Subsequently, Gemini Film Circuit acquired the theatrical release rights, confirming the film release for the Christmas weekend on 23 December 2011.     The Telugu version would be released one week later, distributed by Suresh Movies.  The film released about 500+ screens worldwide.

The film accounted for 64% of the takings in Chennai box office in its second weekend. 

===Critical response===
The film was heavily panned by critics. It was considered the worst movie for both Vikram and the director. A view was that like this movie can be given a negative rating even. Behindwoods gave the film 2.5 out of 5 and wrote that it was "only for Vikrams fans and action lovers" and also stated Rajapattai is popcorn in its flavor.  Pavithra Srinivasan from Rediff described the film as bland, adding that it was "just an actors quest for superstardom, and is a disappointing fare", giving it 1 out of 5.  Sify wrote: "This old-fashioned masala entertainer is enjoyable in parts. It is vintage Vikram coupled with rich production values that makes Rajapattai watchable, despite its obvious flaws".  Rohit Ramachandran of Nowrunning.com rated Rajapattai 2.75/5 calling it "a trashy commercial".  CNN-IBN rated 2 out of 5 and wrote that it was a "road unfit to travel".  Chennaionline noted: "Had Suseenthiran managed to keep an element of suspense somewhere in the script, the film could have been more watchable". 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 