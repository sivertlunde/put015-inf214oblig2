Between Salt and Sweet Water
Between Salt and Sweet Water (Entre la mer et leau douce), also known as Drifting Upstream, is a 1967 Cinema of Quebec|Québécois film directed by Michel Brault, co-written by Brault, Gérald Godin, Marcel Dubé, Claude Jutra and Denys Arcand.
 Golden Gloves.   

==Plot== Claude Gauthier) leaves his small town on the Côte-Nord to go to Montreal, where he works several odd jobs and eventually falls in love with Geneviève (Geneviève Bujold), a pretty waitress who works in a local diner. Claude enters a singing contest that launches his career. As he gradually becomes more well known, he has a brief affair with a married woman and breaks up with Geneviève. He returns to his hometown but nothing seems the same. Back in Montreal, he becomes increasingly more successful as a singer. One night he meets Geneviève backstage, only to learn she is now married, and realizes one can be as lonely in a small town as in a big city.

==Cast== Claude Gauthier - Claude Tremblay
*Geneviève Bujold - Geneviève Paul Gauthier - Roger Tremblay
*Denise Bombardier - Denyse
*Robert Charlebois - Ti-Paul
*Louise Latraverse - Aude, la soeur de Claude
*Gérald Godin - Steve
*Reggie Chartrand - Réginald

==Additional information==
This film has also been released under the following titles:
*Entre la mer et leau douce - Canada (original title)
*Zwischen den Welten - Austria (TV title) / East Germany (TV title) / West Germany (TV title)
*Between Sweet and Salt Water - International (English title)
*Drifting Upstream - Canada (English title)
*Mellan hav och stilla vatten - Sweden

==Reception==
Evocative and engaging, Entre la mer et leau douce is widely regarded as Michel Braults most poetic and richly complex film. 

==References==
 

==External links==

  at the Internet Movie Database

 
 
 
 
 
 
 


 
 