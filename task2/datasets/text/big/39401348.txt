Avatharam (2014 film)
{{Multiple issues|
 
 
}}

{{Infobox film
| name = Avatharam
| image = File:avatharam.jpg
| image_size = 
| caption = Film Theatrical Poster
| director = Joshiy
| producer = Udayakrishna-Siby K. Thomas Dileep K Kunnath Vyaasan Edavanakadu
| writer = Vyaasan Edavanakadu
| narrator = Dileep Lakshmi Lakshmi Menon
| music = Deepak Dev
| cinematography =R D Rajasekhar
| editing =Shyam Sasidharan
| studio =Four B Productions 
| distributor = Kalasangam Release & Popcorn Entertainments Australia
| released =  
| runtime = 165 minutes 
| country = India
| language = Malayalam
| budget = 
| gross = 
}} Dileep and Lakshmi Menon in the lead roles. The film released on August 1.

==Plot== Lakshmi Menon) and falls in love with her. Madhavan tries to find the mystery behind his brothers death.

==Cast== Dileep as Madhavan Mahadevan Lakshmi Menon as Manimekala
* Mithun Ramesh as Karimban Joby
* Joy Mathew as Karimban John
* Babu Namboothiri as Moorthy Sir
* Sreejaya as Vatsala
* Sijoy Varghese as ACP Gautam Vasudev
* Shammi Thilakan as C I Jeevan
* Anju Aravind as Leelamma
* Devan as Dr. Mathew Philip
* Kannan Pattambi as Chacko
* Baby Drishya as Anjali Ganesh Kumar as Sudhakaran Janardhanan as Madhavans Father
* Kaviyoor Ponnamma as Madhavans Mother
* Chali Pala as Colony secretary
* Nandu Pothuval as Pappachan
* Balaji as Bhadran
* Prem Prakash as LIC advisor
* Shiju as Jabbar
* Sreeraman as Jabbars father
* Major ravi as Jamal
* Ambika Mohan as Saraswathy
* Vatsala Menon as Karimban Johns mother
* Vinaya Prasad as Karimban Johns wife
* Lakshmipriya as Karimban Johns sister
* Anil Murali as Tipper George
* Lishoy as Vatsalas father
* Gayathry as Vatsalas mother
* Kalabhavan Shajon as Sundaresan (Guest appearance) Siddique as Divakarettan (Guest appearance)
* Shivaji Guruvayoor as Advt. Thomas (Guest appearance)
* Idavela Babu as Sundaresans colleague (Guest appearance)
* Thesni Khan as Priya (Guest appearance)

The music of the film is composed by Deepak Dev with lyrics penned by Harinarayan. The music album has two songs:

{| class="wikitable"
|-
! Track !! Song Title !! Singer(s)
|-
| 1 || "Konji Konji" || Shankar Mahadevan, Rimi Tomy
|-
| 2 || "Njaan kanum" || Nivas
|}

==External links==
* 
*  

==References==
 

 
 
 


 