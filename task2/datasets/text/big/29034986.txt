The Devil's Double
 
 
{{Infobox film
| name = The Devils Double
| image = The Devils Double.jpg
| image_size = 215px
| alt =
| caption = Theatrical release poster
| director = Lee Tamahori
| producer = Paul Breuls Michael John Fedun Emjay Rechsteiner Catherine Vandeleene
| screenplay = Michael Thomas
| based on =  
| starring = Dominic Cooper Philip Quast Ludivine Sagnier
| music = Christian Henson
| cinematography = Sam McCurdy
| editing = Luis Carballar
| studio = Corsan Lionsgate Herrick Entertainment
| released =  
| runtime = 108 minutes
| country = Belgium Netherlands
| language = English
| budget = $19.1 million 
| gross = $4,807,493   
}} released in Lionsgate and Herrick Entertainment. 
Latif Yahias story behind the events depicted in the film has been questioned and there appears to be no proof that he had been Husseins double or even that he had had any connection to Uday Hussein or the highest levels of Saddam Husseins regime.

==Plot== Ferrari and slant drilling First Gulf War is launched with Uday proclaiming "The Age of the Sheikhs is over!"
 electric carving knife in front of all of the guests. The next morning, Udays bodyguards are seen dumping the partially naked, beaten body of the young girl.
 Republican Guard Coalition forces Mercedes with Udays lover, Sarrab (Ludivine Sagnier). The two escape to Valletta, but Sarrab, fearing for her daughter in Iraq, calls Uday begging for the chance to return without being harmed. A would-be assassin sent by Uday just misses shooting Latif almost as soon as they arrive on the island. Uday calls Latif and offers him one final chance to return to Iraq, threatening to kill his father if he refuses. Latifs father encourages him not to return and he is killed.
 15th Shaaban in 1996, Latif and his partner ambush Uday while he is attempting to lure young girls into his Porsche 911|Porsche. They wound him severely, including mangling his genitals with a direct shot. One of Udays bodyguards catches up to Latif as he flees the scene. The guard, however, is one who Latif could have killed as he fled from Udays birthday party before leaving the country but spared, and the guard extends him the same courtesy.

The movie ends by stating that Latif has been a very difficult man to find after these events. (Though apparently once spotted in Ireland with wife and two children.) Uday was permanently handicapped by the attack but survived until his killing by the U.S. forces in 2003.

==Cast==
* Dominic Cooper as Latif Yahia / Uday Hussein
* Philip Quast as Saddam Hussein
* Ludivine Sagnier as Sarrab
* Mimoun Oaïssa as Ali
* Raad Rawi as Munem
* Mem Ferda as Kamel Hannah
* Dar Salim as Azzam Al-Tikriti
* Khalid Laith as Yassem Al-Helou
* Pano Masti as Said Kammuneh
* Nasser Memarzia as Latifs father
* Tiziana Azzopardi as Latifs sister
* Akin Gazi as Saleeh
* Amrita Acharia as School Girl

==Production==
The film was shot in Jordan and Malta. 

==Critical reception==
The film received mixed reviews. The movie has a 52% rating on Rotten Tomatoes, though much critical acclaim has been given to Dominic Coopers dual role. IGN awarded it 3.5 out of 5 and said "certainly a fresh perspective on one of the Middle Easts most brutal dictators".  CinemaBlend.com also awarded it 3.5 out of five and said "and while the film feels deeply flawed, Cooper is worth the price of admission."  Rockstar Weekly awarded the film a positive review, saying "Hats off to director Lee Tamahori (Die Another Day) for taking a controversial topic and turning it into a masterful film."  However, the Los Angeles Times gave the film a negative review, saying "The story of Uday Husseins body double is relentlessly violent and lurid".  Roger Ebert awarded the film three out of four stars and said "All due praise to Dominic Cooper. It should have been more." 

==References==
 

==External links==
* 
* 
* 
* 
* 
* 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 