L'Avventura
 
{{Infobox film
| name           = LAvventura
| image          = LavventuraSmall.jpg
| caption        = Original Italian film poster
| border         = yes
| director       = Michelangelo Antonioni
| producer       = Amato Pennasilico
| studio         = Cino Del Duca
| screenplay     = Michelangelo Antonioni Elio Bartolini Tonino Guerra
| story          = Michelangelo Antonioni
| starring       = Gabriele Ferzetti Monica Vitti Lea Massari
| music          = Giovanni Fusco
| cinematography = Aldo Scavarda
| editing        = Eraldo Da Roma
| distributor    = Cino Del Duca  (Italy)  Janus Films  (USA) 
| released       =   
| runtime        = 143 minutes
| country        = Italy
| language       = Italian
| budget         =
| gross          =
}} Jury Prize at the 1960 Cannes Film Festival.    LAvventura is the first film of a trilogy by Antonioni, followed by La Notte (1961) and LEclisse (1962).           Gene Youngblood has described this trilogy as a "unified statement about the malady of the emotional life in contemporary times." 

==Plot==
Anna (Lea Massari) meets her friend Claudia (Monica Vitti) at her fathers villa on the outskirts of Rome prior to leaving on a yachting cruise on the Mediterranean. They drive into Rome to Isola Tiberina near the Pons Fabricius to meet up with Annas boyfriend, Sandro (Gabriele Ferzetti). While Claudia waits downstairs, Anna and Sandro make love in his house. Afterwards Sandro drives the two women to the coast where they join two wealthy couples and set sail south along the coast.

The next morning the yacht reaches the Aeolian Islands north of Sicily. After they pass Basiluzzo, Anna impulsively jumps into the water for a swim, and Sandro jumps in after her. When Anna yells that shes seen a shark, Sandro comes to her side protectively. Later onboard Anna confesses to Claudia that the "whole shark thing was a lie," apparently to get Sandros attention. After noticing Claudia admiring her blouse, she slips it into Claudias bag as a gift. At one of the smaller islands, Lisca Bianca, the party comes ashore. Anna and Sandro go off alone and talk about their relationship. Anna is unhappy with his long business trips. Sandro dismisses her complaints and takes a nap on the rocks.

Sometime later Corrado (James Addams) decides to leave the small island, concerned about the weather and rough seas. They hear a boat nearby. Claudia searches for Anna, but she is gone without a trace. Sandro is annoyed, saying this type of behavior is typical. They explore the island and find nothing. Sandro and Corrado decide to continue their search on the island while sending the others off to notify the authorities. Claudia decides to stay as well. Sandro, Corrado, and Claudia continue their search and end up at a shack where they stay the night. As they talk, Sandro takes offense at Claudias suggestion that Annas disappearance is somehow due to his neglect.

In the morning Claudia wakes up before the others and watches the sunrise. After finding Annas blouse in her bag, she meets Sandro out near the cliffs and they talk about Anna, but Sandro now seems attracted to Claudia. The police arrive and conduct a thorough search, but find nothing. Annas father, a former diplomat, also arrives in a high-speed hydrofoil. When he sees the books his daughter has been reading—Tender Is the Night by F. Scott Fitzgerald, and The Holy Bible—he feels confident that she hasnt committed suicide. The police announce that smugglers were arrested nearby and are being held in Milazzo. Sandro decides to investigate, but before leaving he finds Claudia alone on the yacht and kisses her. Claudia rushes off, startled by his actions. She decides to search the other islands on her own. They all agree to meet up at Corrados Villa Montaldo in Palermo.

At the Milazzo police station Sandro realizes the smugglers know nothing about Annas disappearance. When he discovers that Claudia has arrived from the islands, he meets her at the train station where their mutual attraction is evident, but Claudia urges him not to complicate matters and begs him to leave. She boards a train to Palermo, and as the train pulls away, Sandro runs after it and jumps aboard. On the train Claudia is annoyed, saying, "I dont want you with me." She says it would be easier if they sacrifice now and deny their attraction, but Sandro sees no sense in sacrificing anything. Still focused on her friends disappearance, Claudia is troubled by the thought that it "takes so little to change." Sandro relents and gets off the train at Castroreale.

At Messina Sandro tracks down the journalist, Zuria, who wrote an article about Annas disappearance. Their meeting is interrupted by crowds of excited men following a beautiful nineteen-year-old "writer" and aspiring actress named Gloria Perkins (Dorothy De Poliolo). Sandro stops to admire her beauty. Zuria says he heard stories that Anna was spotted by a chemist in Troina. After bribing Zuria to run another story on Anna, Sandro heads to Troina. Meanwhile Claudia meets up with her boating companions at Corrados Villa Montaldo in Palermo. No one seems to take Annas disappearance seriously except Claudia. Even Corrados young wife Giulia openly flirts with the young prince in front of her husband. After reading Zurias follow-up story, Claudia leaves the villa for Troina to continue her search.

In Troina Sandro questions the chemist who claimed to have sold tranquilizers to Anna. Claudia arrives and they learn that the woman identified by the chemist left on a bus to Noto in southern Sicily. Sandro and Claudia resume their search together and drive south. Outside Noto they stop at a deserted village, and then find a hill overlooking the town where they make love while a train goes by. Later in town they go to the Trinacria Hotel where they believe Anna is staying. Claudia asks Sandro to go in alone. While Claudia waits outside, a crowd of men gather around her. When she thinks she sees Sandro and Anna coming down the stairs she runs into a paint store, but Sandro follows and confirms that Anna is not there. Claudia remains torn between her feelings for Sandro and her friendship with Anna.

At the Chiesa del Collegio, a nun shows them the view from the roof. Sandro talks about his disappointments with his work, far removed from his youthful ambitions as an architect. Suddenly he asks Claudia to marry him, but she says no—things are too complicated. She accidentally tugs on a rope that rings the church bells, which are answered by connected church bells at another church. Claudia is delighted by the sounds. The next morning she wakes up in a joyful mood, dancing and singing in the room while Sandro looks on amused. They both seem passionately in love. Sandro goes for a walk to the Piazza Municipo, where he notices an ink sketch left by one of the students. With his keychain he "accidentally" knocks over the ink onto the sketch. The student notices and confronts Sandro, who denies he did it on purpose. Sandro returns to the hotel and tries to make love to Claudia, but she resists, telling him they should leave.

At Taormina they check into the San Domenico Palace Hotel where Sandros employer Ettore and his wife Patrizia are preparing for a party. Claudia decides not to attend because shes tired. At the party Sandro checks out the women—recognizing the beautiful aspiring actress Gloria Perkins. Back in the room Claudia is unable to sleep. Noticing that Sandro has not yet returned, she goes downstairs to Patrizias room to inquire about Sandro. Claudia confesses that shes afraid Anna has returned and that Sandro will return to her. After searching the hotel, Claudia finally discovers Sandro having sex with Gloria Perkins on a couch. Claudia runs off, and Sandro follows her onto the hotel terrace where he finds her quietly weeping. Sandro sits down on a bench and says nothing; he too begins to cry. Claudia approaches him, and after hesitating, she places her hand on his head in a gesture of compassion and comfort while looking out at the snow-covered image of Mount Etna on the horizon.

==Cast==
* Gabriele Ferzetti as Sandro
* Monica Vitti as Claudia
* Lea Massari as Anna
* Dominique Blanchar as Giulia
* Renzo Ricci as Annas Father
* James Addams as Corrado
* Dorothy de Poliolo as Gloria Perkins
* Lelio Luttazzi as Raimondo
* Giovanni Petrucci as Prince Goffredo 
* Esmeralda Ruspoli as Patrizia
* Jack OConnell as Old Man on the Island
* Angela Tommasi di Lampedusa as The Princess
* Prof. Cucco as Ettore
* Renato Pinciroli as Zuria, the journalist   

==Production==
Shooting began in August 1959 and lasted until January 15, 1960. Antononi began filming the island sequence with the scenes immediately after Anna disappears. The majority of shooting on the island was filmed on the island Lisca Bianca (white fish bone) with a cast and crew of 50 people. Other locations for the island sequence included Panarea (which was also the productions headquarters), Mondello and Palermo. Filming the island sequence was intended to take three weeks but ended up lasting for four months. Difficulties included the islands being infested with rats, mosquitoes and reptiles, the weather being unexpectedly cold and the Navy ship hired to transport the cast and crew to the island every day never showing up. The crew had to personally build small rafts out of empty gas canisters and wooden planks to carry personal items and equipment to the island, which were towed by a launching tug every morning. Criterion. Youngblood. 

One week after shooting began, the films production company went bankrupt, leaving the production in short supplies of food and water. Antonioni still had a large supply of film stock and managed to get the cast and crew to work for free until funding for the film was found. At one point, ships stopped making trips to Lisca Bianca and the cast and crew were stranded for three days without food or blankets. Eventually the crew went on strike and Antonioni and his Assistant Director shot the film themselves.  Due to the rough condition of the sea and the difficulty in landing a ship on the rough rocks of Lisca Bianca, the cast and crew were often forced to sleep on the island. Antonioni has stated that he "woke up every morning at 3 oclock in order to be alone and reflect on what I was doing in order to re-load myself against fatigue and a strange form of apathy or absence of will, which often took hold of us all."  After several weeks of working without a budget the production company Cino del Duca agreed to finance the film and sent money to Antonioni. 

While shooting on the 40-foot yacht for scenes early in the film the cast and crew totaled 23 people. Antonioni had wanted to shoot the film chronologically, but the yacht was not available until November. Due to the cold weather, actress Lea Massari developed a cardiac condition after spending several days swimming in the Mediterranean Sea during filming and spent several days in a coma after being rushed to Rome for medical treatment. 

After completing the island sequence, filming continued throughout Sicily and Italy. The sequence on the train from Castroreale to Cefalù took two days to shoot instead of the intended three hours. The scene in Messina where Sandro encounters Gloria Perkins took two days to shoot and Antononi initially wanted 400 extras. Only 100 showed up so crew members recruited passers-by on the street to appear in the scene.  The sequence where Sandro and Claudia visit a deserted town was shot in Santa Panagia, near Catania in Sicily and was an example of fascist Mezzogiorno architecture, which was commissioned by Benito Mussolini. The scene where Sandro and Claudia first have sex took ten days to shoot due to the crew having to wait for a train to pass by every morning. 

Antonioni wrote that the film was "expressed through images in which I hope to show not the birth of an erroneous sentiment, but rather the way in which we go astray in our sentiments. Because as I have said, our moral values are old. Our myths and conventions are old. And everyone knows that they are indeed old and outmoded. Yet we respect them." 

===Filming locations===
LAvventura was filmed on location in Rome, the Aeolian Islands, and Sicily.         

* Aeolian Islands, Messina, Sicily, Italy
* Bagheria, Palermo, Sicily, Italy 
* Basiluzzo, Aeolian Islands, Messina, Sicily, Italy (where Anna jumps from the yacht)
* Capo di Milazzo, Messina, Sicily, Italy 
* Castroreale, Messina, Sicily, Italy (where Sandro alights from the train)
* Province of Catania|Catania, Sicily, Italy 
* Cefalù, Palermo, Sicily, Italy (the platform after Sandro leaves the train) 
* Chiesa del Collegio, Noto, Syracuse, Sicily, Italy (where Claudia rings the church bells)
* Church of San Domenico, Piazza San Domenico 5, Taormina, Messina, Sicily, Italy
* Church of San Francesca, Piazza Immacolata, Noto, Syracuse, Sicily, Italy 
* Corso Umberto, Bagheria, Palermo, Sicily, Italy 
* Lipari, Aeolian Islands, Messina, Sicily, Italy 
* Lisca Bianca, Aeolian Islands, Messina, Sicily, Italy (the island from which Anna disappears)
* Messina, Sicily, Italy (where Sandro meets Zuria and encounters the "writer" Gloria Perkins)
* Milazzo, Messina, Sicily, Italy 
* Mondello, Palermo, Sicily, Italy 
* Mount Etna, Catania, Sicily, Italy 
* Museo Civico, Convent of Santissimo Salvatore, Duomo, Noto, Syracuse, Sicily, Italy (which is closed to Sandro)
* Noto, Syracuse, Sicily, Italy 
* Noto Cathedral, Noto, Syracuse, Sicily, Italy 
* Palermo Town Hall, Palermo, Sicily, Italy 
* Palermo, Sicily, Italy 
* Panarea, Aeolian Islands, Messina, Sicily, Italy 
* Piazza Garibaldi, Corso Umberto, Bagheria, Palermo, Sicily, Italy 
* Piazza Immacolata, Noto, Syracuse, Sicily, Italy 
* Piazza Municipo, Noto, Syracuse, Sicily, Italy (where Sandro "accidentally" spills the ink)
* Piazza San Domenico 5, Taormina, Messina, Sicily, Italy 
* Plain of Catania, Sicily, Italy (where Sandro questions the chemist)
* Pons Fabricius, Rome, Lazio, Italy (outside Sandros house)
* Rome, Lazio, Italy 
* San Domenico Palace Hotel, Piazza San Domenico 5, Taormina, Messina, Sicily, Italy (the terrace in the final scene)
* Santa Panagia, Syracuse, Sicily, Italy (where Sandro and Claudia make love on a hill while a train goes by)
* Sicily, Italy 
* St. Peters Basilica, Rome, Lazio, Italy (opening scene from Annas fathers villa)
* Syracuse, Sicily|Syracuse, Sicily, Italy 
* Tiber Island, Rome, Lazio, Italy (where Sandros house is located)
* Tiber River, Rome, Lazio, Italy (near Sandros house)
* Tyrrhenian Sea (where the yacht cruise takes place) 
* Villa Niscemi, Piazza Niscemi, Via di Fante, Palermo, Sicily, Italy (Villa Montaldo where Giulia is seduced by the Princess grandson)
* Villa Palagonia, Bagheria, Palermo, Sicily, Italy (the Customs House in Milazzo)
* Villa of Prince Niscemi, Palermo, Sicily, Italy

===Music=== Hellenic era." 

==Reception==

===Critical response=== Cannes Film Jury Prize    and went on to both international box office success and what has since been described as "hysteria."      Gene Youngblood has stated that audience members usually booed during long sequences where nothing happened to further the films plot, but has asserted that "quite a lot is happening in these scenes." 

Bosley Crowther of the New York Times called the film a "weird adventure" and praised its cinematography and performances.  Andrew Sarris of The Village Voice called it the movie-going phenomenon of 1961 and praised Antonionis depiction of characters that cannot communicate with each other.  Stanley Kauffmann of The New Republic said "Antonioni is trying to exploit the unique powers of the film as distinct from the theater...He attempts to get from film the same utility of the medium itself as a novelist whose point is not story but mood and character and for whom the texture of the prose works as much as what he says in the prose. 

===Awards and nominations===
* 1960 Cannes Film Festival Jury Prize (Michelangelo Antonioni) Won 
* 1960 Cannes Film Festival Palme dOr Nomination (Michelangelo Antonioni)
* 1960 British Film Institute Sutherland Trophy (Michelangelo Antonioni) Won    BAFTA Award Nomination for Best Film from any Source (Michelangelo Antonioni)   
* 1961 BAFTA Award Nomination for Best Foreign Actress (Monica Vitti) 
* 1961 Golden Globe Award for Best Breakthrough Actress (Monica Vitti) Won Italian National Syndicate of Film Journalists Silver Ribbon for Best Score (Giovanni Fusco) Won
* 1961 Italian National Syndicate of Film Journalists Silver Ribbon Nomination for Best Actress (Monica Vitti)
* 1961 Italian National Syndicate of Film Journalists Silver Ribbon Nomination for Best Cinematography, B/W (Aldo Scavarda)
* 1961 Italian National Syndicate of Film Journalists Silver Ribbon Nomination for Best Director (Michelangelo Antonioni)
* 1961 Italian National Syndicate of Film Journalists Silver Ribbon Nomination for Best Original Story (Michelangelo Antonioni)
* 1961 Italian National Syndicate of Film Journalists Silver Ribbon Nomination for Best Supporting Actress (Lea Massari) 

==Legacy== Time Out Michael Phillips of the Chicago Tribune defended the film against Andrews criticism, saying that "Its easy to bash Antonioni as passe. Its harder, I think, to explain the cinematic power of the way his camera watches, and waits, while the people on screen stave off a dreadful loneliness." 

It has appeared on   magazines "The 100 Best Films Of World Cinema."   

==Meaning==
Much has been made of Annas unsolved disappearance, which Roger Ebert has described as being linked to the films mostly wealthy, bored, and spoiled characters, none of whom have fulfilling relationships. They are all, wrote Ebert, "on the brink of disappearance." 

According to Alain Robbe-Grillet, many shots in the "continental" part of the film are taken from the point of view of an unseen character, as if Anna was following Sandro and Claudia to see what they would do. Alain Robbe-Grillet. Préface à une vie décrivain. Seuil, 2005. ISBN 9782020845885. Pg. 223-225.  When asked, Antonioni told Robbe-Grillet that the "missing" scene (showing Annas body recovered from the sea) was scripted and actually filmed but did not make it into the final cut, apparently for timing reasons. 

==Home media==
A digitally restored version of the film (optimal image quality: RSDL dual-layer edition) was released on DVD by   along with Nicholson’s personal recollections of the director.

==References==
;Notes
 
;Citations
 
;Bibliography
 
*  
*  
*  
*   

==External links==
*  
*  
*  
*  by  Geoffrey Nowell-Smith
*  by Gene Youngblood
* 
*  British author and film critic Chris Darke
* 
* 

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 