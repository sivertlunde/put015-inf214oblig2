Because of Him
{{Infobox film
| name           = Because of Him
| image          = Because of Him 1946 Poster.jpg
| alt            = 
| caption        = Theatrical release poster Richard Wallace
| producer       = Felix Jackson
| screenplay     = Edmund Beloin
| starring       = {{Plainlist|
* Deanna Durbin
* Charles Laughton
* Franchot Tone
}}
| music          = Miklós Rózsa
| cinematography = Hal Mohr
| editing        = Ted J. Kent
| studio         = Universal Pictures
| distributor    = Universal Pictures
| released       =  
| runtime        = 88 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Because of Him is a 1946 American romantic comedy film starring Deanna Durbin, Charles Laughton and Franchot Tone. It includes a memorable scene in which Durbin sings Danny Boy.  , Because of him (1946). 

== Cast ==
* Deanna Durbin as Kim Walker
* Charles Laughton as John Sheridan
* Franchot Tone as Paul Taylor
* Helen Broderick as Nora

== Plot ==
Kim Walker (Deanna Durbin) is an ambitious waitress who dreams of being on the stage. She tricks respected stage actor John Sheridan (Charles Laughton) into signing a letter of introduction. Thanks to the forged letter, Kim then wins the role of Sheridans co-star in his next play, much to the disgust of the writer Paul Taylor (Franchot Tone).

== Producion ==
Durbins husband Felix Jackson produced the picture, and at the time of filming, she was pregnant with her first child.  , imdb. 

== References ==
 

 


 