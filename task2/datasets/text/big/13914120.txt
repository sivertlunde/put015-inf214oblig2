Yathra
 
{{Infobox film
| name           = Yathra (The Journey)
| image          = Yathra.jpg
| image_size     =
| caption        =
| director       = Balu Mahendra
| producer       = Joseph Abraham
| writer         = John Paul  (story)  Balu Mahendra  (screenplay) 
| narrator       = Anju
| music          = Ilaiyaraaja
| cinematography = Balu Mahendra
| editing        = D. Vasu
| distributor    = Prakkattu Films
| studio         = Prakkattu Films
| released       =  
| runtime        =
| country        = India
| language       = Malayalam
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}
 Telugu film Nireekshana, directed by Balu Mahendra himself, and starring Bhanu Chander and Archana (actress)|Archana.
 the nationwide emergency of 1975-1977, when the fundamental rights of the citizens were suspended. The movie is an adaptation of the 1977 Japanese classic The_Yellow_Handkerchief_(1977_film)|"The Yellow Handkerchief".

==Plot==
The story unfolds as Unnikrishnan (Mammootty), a convict, now free from the jail tells his tragic love story to his fellow passengers in a school bus. An orphan and a forest officer by profession, he falls in love with a local girl, Thulasi, during his stay at a forest area. They decided to get married and he sets off to inform his marriage to his best friend. On his way back the police arrests him as a suspected criminal, who has some visible similarities of Unnikrishnan. There he accidentally kills a police man and gets life imprisonment. During his early days at jail, he writes a letter to Thulasi asking to forget him. When his prison term was about to complete, he wrote a letter to light a lamp if she still waits for him. After long years of torments in the jail he goes to meet Thulasi, and does she still waits for him? Thats the question of his fellow passengers too.
She waits for him and they get united

==Cast==
 
*Mammootty
*Shobhana
*Adoor Bhasi
*Thilakan
*Nahas
*Sunny
* Achankunju Kunchan
*Azeez Azeez
*Alummoodan
*K. P. A. C. Sunny
*T. R. Omana Anju
*Manohar
*P.R. Menon
*Baby Preethi
*K.R. Savithri
*Sreerekha
*Master Vimala
 

==Awards== Filmfare Award.

==Soundtrack== My Favorite Things" from the 1959 Rodgers and Hammerstein musical The Sound of Music.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Kunnathoru Kaavundu (Resung song from Asuravithu) || Cochin Alex || P. Bhaskaran ||
|-
| 2 || Thannannam Thaanannam || K. J. Yesudas, Ambili, Chorus, Antony, Anna Sangeetha || ONV Kurup ||
|-
| 3 || Yamune Ninnude || S Janaki, Chorus || ONV Kurup ||
|}

==References==
 

==External links==
*  
* http://popcorn.oneindia.in/title/5761/yathra.html

 

 
 
 
 
 
 
 