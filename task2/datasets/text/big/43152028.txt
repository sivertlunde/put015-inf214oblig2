4 Minute Mile
 

{{Infobox film
| name           = 4 Minute Mile
| image          = 4_Minute_Mile.jpg
| caption        =
| director       = Charles-Olivier Michaud
| producer       = 
| writer         = Josh Campbell Jeff Van Wie
| starring       = Analeigh Tipton Cam Gigandet Kim Basinger Kelly Blatz Richard Jenkins
| music          = Stephen Barton
| cinematography = Jean-Francois Lord
| editing        = Elisabeth Tremblay Dirk Westervelt
| studio         = One Square Mile Management Company Phoenix Rising Motion Pictures
| distributor    = Gravitas Ventures
| released       =  
| runtime        = 96 minutes 
| country        = United States
| language       = English
| gross          = 
}}
4 Minute Mile is a 2014 film directed by Charles-Olivier Michaud, and written by Josh Campbell and Jeff Van Wie. It stars Richard Jenkins, Kelly Blatz, Analeigh Tipton, Kim Basinger, and Cam Gigandet. It is distributed by Gravitas Ventures,   and the international sales rights are held by Double Dutch International. 

The film premiered at the Seattle International Film Festival (SIFF) on June 5, 2014.    Gravitas planned for a theatrical release on August 1, 2014. 

==Plot==
A teen track runner Drew (Kelly Blatz) is struggling on the track as well as at home.  He accepts an offer from his reclusive neighbor (Richard Jenkins) to train him and the two form a bond.

The movie 4 Minute Mile tells the inspirational story of Drew, a high school student struggling to overcome the inner-city surroundings that threaten to imprison him. After getting manipulated into running drug payments to a local dealer, Drew has nowhere to turn in a family with a deadbeat older brother and an overwhelmed single mom.

His only outlet is track practice, but that is taken from him as he continues down the path of his sibling; even the affections of a girl at school can’t set him straight.

Just as it seems hopeless, an ex-track coach with his own demons witnesses Drew’s speed and athleticism and decides to train him to reach for more than he ever imagined possible. Together they work towards finding solace in each other, but Drew’s resolve is threatened as tragedy strikes right before the biggest race of his life and forces him to confront everything that has been holding him back.
 

==Cast==

* Kelly Blatz - Drew Jacobs - Teen track runner
* Richard Jenkins - Coach Coleman - Recluse neighbor
* Kim Basinger - Claire Jacobs - Mother
* Cam Gigandet - Wes Jacobs - Older brother
* Analeigh Tipton - Lisa - Girlfriend
* Rhys Coiro - Eli - Drug dealer


==References==
 

==External links==
* 
* 
* 

 
 


 