G.O.R.A.
{{Infobox film
| name           = G.O.R.A
| image          = Gora.jpg
| image_size     =
| caption        = Film poster
| director       = Ömer Faruk Sorak
| producer       = Nuri Sevin Necati Akpınar Gökhan Tuncel
| writer         = Cem Yılmaz
| narrator       =
| starring       = Cem Yılmaz Özge Özberk Şafak Sezer Rasim Öztekin İdil Fırat Özkan Uğur
| music          = Sagopa Kajmer  Ozan Çolakoğlu
| cinematography = Veli Kuzlu
| editing        = Böcek Yapım BKM Film
| distributor    = 
| released       =  
| runtime        = 
| country        = Turkey
| language       = Turkish
| budget         =  TL  
}} Turkish science-fiction comedy film, directed by Ömer Faruk Sorak, which stars Cem Yılmaz as a used carpet salesman who is abducted by aliens from the planet G.O.R.A. The film, which went on nationwide general release across Turkey on  , was one of the highest grossing Turkish films of 2004 and was followed by the sequel A.R.O.G (2008).  {{cite news|url=http://www.milliyet.com.tr/2006/03/08/magazin/amag.html
|accessdate=2008-12-28
|title=Kurtlar Vadisi Irak tüm rekorları kırdı
|date=2002-03-08
|first=Birsen
|last=Altuntas
|language=Turkish
|work=Milliyet
}} 

== Synopsis ==
The film is about a used carpet salesman Arif (played by Cem Yılmaz) who is abducted by aliens from the planet G.O.R.A. There he becomes involved in thwarting a diabolical scheme being advanced by the planets security chief Logar (also played by Cem Yılmaz) and saves the planet. The film spoofs various science fiction films such as Star Wars, The Fifth Element and The Matrix.

== Reception ==
=== Reviews === the Matrix films in there," that "looked good...went on to become a huge hit in its native country," and "re-introduced Turkish genre film to international audiences as it rolled out on the festival circuit," proving that Turkey "was capable of producing big, glossy productions with the very best of them."      

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 