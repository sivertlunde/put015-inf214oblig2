Toot, Whistle, Plunk and Boom
{{Infobox Hollywood cartoon
| cartoon_name   = Toot, Whistle, Plunk, and Boom
| series         = Adventures in Music
| image          = Toot, Whistle, Plunk and Boom poster.jpg
| caption        = Poster for Toot, Whistle, Plunk and Boom
| alt            = Poster for Toot, Whistle, Plunk and Boom  Charles A. Nichols
| story_artist   = Dick Huemer Marc Davis  Henry Tanous  Art Stevens  Xavier Atencio  Bill Thompson  Thurl Ravenscroft   Loulie Jean Norman   Charlie Parlato   Gloria Wood Jack Elliot (songs)
| producer       = Walt Disney
| studio         = Walt Disney Productions Buena Vista Distribution Co. Inc.
| release_date   = November 10, 1953
| color_process  = Technicolor, CinemaScope
| runtime        = 10 mins (one reel)
| preceded_by    = Melody (1953 film)|Melody
| followed_by    = 
| movie_language = English
}}

Toot, Whistle, Plunk and Boom is an   ("toot"), a flute ("whistle"), a guitar ("plunk"), and a drum ("boom").
 Academy Award for Best Short Subject (Cartoons). In 1994, it was voted #29 of the 50 Greatest Cartoons of all time by members of the animation field.

Like many of Disneys early CinemaScope films, a "flat" version shot in 4:3 ratio was made for theaters that were not equipped for CinemaScope. This required rearranging the artwork for some shots to accommodate the smaller screen. Shots of multiple repeated characters (like the bird chorus at the end, for instance) were cut in half, using two repetitions instead of four. The most notable change comes at the transition from the end of the "Boom" section to the parade that starts the finale. In the CinemaScope version, the background and characters fade out, leaving the drum in the last scene alone; the drum then jumps from the side of the screen to the centre, and the parade fades in. In the flat version, the camera zooms in on the drum, dissolving into the parade and zooms back out.


Other changes occur in the flat version.
* Some of the characters like the kitchen bird band members near the beginning and the string quartet members get closer together.
* Bach with his clarinet has a book of sheet music with "BACH" written on it instead of the busts around him.
* Plunk morphs into the Egyptian playing the harp instead of fading out to him.
* In the CinemaScope version, the grand piano also in the plunk segment stretches but in the flat version, it stays a certain size.


Black stereotypes have been cut from this short for the DVD release.

== Synopsis ==
The credits roll over a stylised music shop. The names of cast and crew and title of the feature are superimposed over the various instruments and instrument cases. The scene then cuts to Professor Owl, who rushes to a schoolhouse full of bird children as a drumroll is played on a snare.

A brief musical section introduces us to "the subject for today": the study of musical instruments. Professor Owl explains to the class (and the viewer) that all music originates from four core sounds: toot (brass section|brass), whistle (woodwind section|woodwind), plunk (string section|strings) and boom (percussion section|percussion).

=== Toot (Brass) ===

The film then jumps to a group of four cavemen, each of whom have discovered the nuclear form of one of the above sounds. We begin with a portly Caveman Toot who has discovered that blowing through an old cows horn produces a pleasing "toot". We advance to ancient Egypt in 2000 BC, where Caveman Toot discovers that metal horns produce even better sounds. He celebrates by breaking into a two-note jazz solo as Egyptian characters painted on the walls boogie down.
 Roman trumpeter who crashes into a column and bends his horn into a grotesque shape... however, he soon discovers that despite this change in form, the trumpet does not sound any different: it is possible to change the horns shape without changing the pitch.

However, as Professor Owl explains, this horn can only produce certain notes; in order to get all of the notes required for even a simple tune, you would need four horns of different lengths. But if we create a horn with valves, we can effectively have four horns in one, and this fact is celebrated with another jazz solo.

=== Whistle (Woodwind) ===
We return to the cavemen, where Caveman Whistle is trying to impress his "cavegirl" by blowing on a tube of grass; he further discovers that adding holes to the tube allows him to modify the sound in interesting ways (the more holes Whistle adds the longer the grass tube gets and he invents the first flute). The cavegirl is impressed, but then a rival caveman appears, bonks the cavegirl on the head with his club, and drags her off by the hair (which makes Caveman Whistle angry). 

Professor Owl explains that this system of holes is the basis for every woodwind instrument, including the clarinet played by Johann Sebastian Bach and the saxophone played by a beatnik.

=== Plunk (Strings) ===
Caveman Plunk has discovered that plucking on the string of his bow produces a pleasant sound. An offscreen choir explains (as the animation shows) how to create a simple harp by adding a resonator, some extra strings and tuning pegs, and rearranging it all (and Plunk invents the first harp).
 pluck the harp, or play it with a Bow (music)|bow. We then briefly visit several periods in history, where we see several stringed instruments being played in similar fashion, and finish with a string quartet.

=== Boom (Percussion) ===
Caveman Boom beats on his stomach to produce a "boom", and hits other parts of his body and his club for other sounds. Professor Owl escorts us through history and explains how a variety of percussion instruments emerged from this basic theory, ranging from rattles to complex drum kits and even the bass drums of marching bands.

=== Conclusion ===
The chorus recaps that all music, from the banjo to Latin percussion to "music oriental" to an orchestra in a concert hall, emerging from the four core sounds with Caveman Toot in the brass section, Caveman Whistle in the woodwind section, Caveman Plunk in the string section and Caveman Boom in the percussion section, all wearing top hats.

== Re-releases and educational use ==
Toot, Whistle, Plunk and Boom was reissued in 1963 as a companion short to that years theatrical re-release of   DVD, and is one of the selected shorts included in the  
 How the Grinch Stole Christmas!, provided the voice of the owl on the album.

While the film was originally released into theatres as a part of a broader collection of shorts, it continues to be used today in music classrooms to provide an elementary understanding of how musical instruments work.

==Voices== Bill Thompson
*Thurl Ravenscroft
*Loulie Jean Norman
*Charlie Parlato
*Gloria Wood

==See also== Melody

==Footnotes==
 

== External links ==
* 
*  

 

 
 
 
 
 
 
 
 