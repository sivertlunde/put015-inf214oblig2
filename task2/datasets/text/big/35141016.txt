A Summer Tale
{{Infobox film
| name           = Den bästa sommaren
| image          = 
| image size     = 
| director       = Ulf Malmros Lars Jönsson Lars Johansson
| narrator       = 
| starring       = 
| music          = Dan Sundquist   Henrik Medquist
| cinematography = Mats Olofsson
| editing        = Fredrik Abrahamsen, Michal Leszczylowski
| studio         = Zentropa
| distributor    = Film i Väst   Sonet Film   Sandrew Metronome
| released       =  
| runtime        = 91 minutes
| country        = Sweden, Denmark
| language       = Swedish
| budget         = 
| gross          = 
}} 2000 Cinema Danish comedy-drama. Cecilia Nilsson, and Brasse Brännström, and was produced by Zentropa and Memfis Film.

==Plot==
The film takes place in 1958 in Molkom, where two children, Mårten from Stockholm and Annika from Uppsala, will be "summer children" to Yngve Johansson, who every summer takes children to his home, but it has been bad earlier years. In the beginning Mårten and Annika dont like Yngve, but they love each other more and more and they also start liking Yngve. Maybe he isnt as bad as they think? This summerll move everything in their life for all future.

==Cast==
*Kjell Bergqvist as Yngve Johansson
*Anastasios Soulis as Mårten
*Rebecca Scheja as Annika Cecilia Nilsson as Miss Svanström
*Brasse Brännström as Sven
*Marcus Hasselborg as Harald, Svens son
*Gachugo Makini as Jacques
*Göran Thorell as Erik Olsson, man working for Barnavårdsnämnden
*Ann Petrén as Mrs Ljungström
*Eivin Dahlgren as Headwaiter
*Anna Kristina Kallin as Doctor
*Ralph Carlsson as Priest
*Jerker Fahlström as Postman
*Johan Holmberg as Policeman

==Production==
The film was shot at Björkås Herrgård in Vargön, Restad Gård in Vänersborg, Anten-Gräfsnäs Railway, in Upphärad and in Sjuntorp. It premiered (in Sweden) on 8 March 2000 and is recommended from 7 years.

==External links==
*  
* 
*  

 
 
 
 
 
 
 
 
 
 


 
 