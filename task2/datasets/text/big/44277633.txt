Hollows Grove
{{Infobox film
| name           = Hollows Grove
| image          = File:Hollows Grove film poster.jpg
| alt            =
| caption        = 
| film name      = 
| director       = Craig Efros
| producer       = Gary Adelman Craig Efros Mel Efros Tony Palermo
| writer         = Craig Efros
| screenplay     = 
| story          = 
| based on       =  
| starring       = Mykelti Williamson Lance Henriksen Matthew Carey
| narrator       = 
| music          = 
| cinematography = Sidney Sidell
| editing        = Sam Bauer
| production companies = Hollows Grove
| distributor    = 
| released       =  
| runtime        = 81 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =  
}} digital download on 21 October 2014 and stars Matthew Carey as a filmmaker that decides to create a documentary centering upon a ghost hunting team, only to find that this could be the last thing he ever does. 

==Synopsis== Matt Doherty) reveals that a good chunk of the evidence collected during each investigation was planted there by their props director Bill (Lance Henriksen). In fact, everything that is seen in their show is the result of very careful planning and editing designed to get the maximum amount of entertainment per episode. However this time the group is going to investigate Hollows Grove, an abandoned orphanage where several atrocities were committed against their residents and two nurses committed suicide. Initially the group approaches the grounds with the idea that there will be no true threat to their beings, only to discover that Hollows Grove is very haunted and that their lives are very much in danger.

==Cast==
*Mykelti Williamson as F.B.I. Agent Jones
*Lance Henriksen as Bill
*Matthew Carey as Harold Maxwell
*Sunkrish Bala as Roger Rafkin
*Bresha Webb as Julie Mercade
*Val Morrison as Chad Groan Matt Doherty as Tim Royce
*Eddie Perez as Hector Gustavo
*Gary Sievers as Homeless Man
*Tanc Sade as George
*Lindsey Smith-Sands as Nurse
*Michael Scott Allen as Additional Voice

==Reception==
Critical reception has been mixed to negative.    Dread Central gave Hollows Grove one star, writing that while they were "mildly amused at the idea of how full of crap the crew knew they were about their show and its production", the film was ultimately a disappointment.  HorrorNews.net was slightly more positive and they praised the performances from Henriksen and Doherty but overall panned the film for being too "tightly tucked inside the established blue print for this kind of found footage film, it never pushes any boundaries nor reaches for any taboos or new ground. It is decidedly by the books which makes it often dull." 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 