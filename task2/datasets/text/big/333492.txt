First Kid
{{Infobox film
| name =  The First Kid
| image = FirstKidfilm.jpg
| caption = Theatrical release poster
| director = David M. Evans
| producer = Roger Birnbaum Tim Kelleher Sinbad Brock Pierce Zachery Ty Bryan James Naughton
| music = Richard Gibbs
| cinematography = Anthony B. Richmond
| editing =  Harry Keramidas
| studio = Walt Disney Pictures Caravan Pictures Buena Vista Pictures
| released =  
| runtime = 101 minutes
| country = United States
| language = English
| budget = $15 million
| gross = $26,491,793
}}
 Disney comedy Sinbad and Brock Pierce. It was mostly filmed in Richmond, Virginia.

==Plot== Secret Service agent assigned by his superior Wilkes (Robert Guillaume) to protect President Paul Davenports (James Naughton) rebellious 13-year-old son Luke Davenport (Brock Pierce) after Lukes behavior causes another agent Woods (Timothy Busfield) to be replaced for mistreating Luke in front of media cameras. Woods is later fired for failing his physical. Simms sees this assignment as undesirable, but a possible stepping stone to protecting the President. He fails to connect with the boy at first, and Luke continues to misbehave, including an incident where he releases his pet snake Poison into a White House party. After seeing Luke get beat up by the school bully Rob (Zachery Ty Bryan), Simms feels sorry for him - he had felt alone as a teenager, too (losing his father in Vietnam War|Vietnam) - and they become friends. Simms, a former boxing champion, agrees to sneak Luke out against the wishes of the chief of security Morton (Art LaFleur) and teach him how to fight.

Meanwhile, Luke agonizes over asking the cutest girl, Katie, to the school dance, which he finally does successfully with Simmss help. On the night of the dance, a backpack is left outside of the White House and Luke is not allowed to go due to the security risk, even though his parents gave him permission. Simms pities him and, breaking the rules again, he takes him to the dance. There, Rob tries to attack Luke again while Simms is distracted, but this time Luke puts him down.

After that, Secret Service agents bust the school dance and retrieve Luke. Simms is fired and not allowed to speak with Luke, who is crushed that his friend has apparently abandoned him. Luke,   and with a homing device attached to him, receives advice from an online friend, Mongoose12, on how to escape the White House and meet him at a local mall. Luke agrees, but it is revealed that Mongoose12 was in fact former agent Woods, who kidnaps him. When Luke goes missing, Simms is given another chance to protect him. With the help of his friend Harold (who owns a spy shop), he quickly tracks Luke to the mall. In a standoff, Woods says he was planning on returning Luke to the President so he could be a hero and get his job back but now he wants to kill him instead. He blames Luke for making him lose his job, and even his wife. Woods tries to shoot Simms but he takes cover and once Woods is out of bullets, Simms brings him down with a right uppercut. As other agents arrive, Woods tries to shoot Luke with a back-up revolver. Simms jumps in front of Luke and takes the intended bullet in the arm. Woods is also shot and subdued by other arriving Secret Service agents for kidnapping, assault, arson, and attempted murder.

In the final scene of the film, Simms is offered Presidential duty which he declines in order to stay with Lukes biology teacher. Luke is relieved of his last punishment, and while playing street hockey with friends, hits the puck in Simms forehead, ensuing a chase-off between Simms and Luke.

==Cast== Sinbad as Sam Simms
* Brock Pierce as Luke Davenport
* James Naughton as President Paul Davenport
* Timothy Busfield as Woods
* Art LaFleur as Morton
* Robert Guillaume as Wilkes
* Lisa Eichhorn as Linda Davenport
* Blake Boyd as Dash
* Erin Williby as Katie Warren
* Zachery Ty Bryan as Rob MacArthur
* Bill Cobbs as Speet
* Sonny Bono in a cameo appearance as himself.
* Sean "Oleus" Sullivan as Kid in shopping mall chase
* Bill Clinton in a cameo appearance as himself.

The film turns out to be the final on screen appearance of Sonny Bono, who, at the time of the films release, was serving in the United States House of Representatives|U.S. House of Representatives. Sonny plays himself as a Congressman coming to the White House to visit the President. Simms bumps into him (literally) outside the Oval Office and fawns over him.

==Ratings==
*When First Kid was rated for video release by the British Board of Film Classification|BBFC, 1 minute and 31 seconds were cut, due to the hostage attack in the shopping mall. At the time the BBFC felt that this, and the sudden onslaught of violence, were too intense for a PG certificate. In 2002, all cuts were waived for a PG rating.  

==Reception ==

===Box office===
The movie debuted at  No.3 

=== Critical response ===
Rotten Tomatoes gives the film a score of 23% based on reviews from 13 critics. 

==Locations==
* The mall scenes were filmed at the Tysons Galleria in Tysons Corner, Virginia.  
* A few scenes were shot at St. Catherines School, Richmond, Virginia.
* The dance scene was shot at Harry F Byrd Middle School in Richmond for the senators’ logo on the gym walls and floor.  

==References==
 
*  

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 