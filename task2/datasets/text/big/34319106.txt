The East (film)
{{Infobox film
| name           = The East
| image          = The East 2013 film poster.jpg
| alt            = An image washed out in blue, three bands, the top showing an older blonde, woman, the middle a young blonde woman and the lower showing a man with a short beard and a brunette. 
| caption        = Theatrical release poster
| director       = Zal Batmanglij
| producer       = {{Plain list |
*Ridley Scott Michael Costigan
*Jocelyn Hayes-Simpson
*Brit Marling
}}
| writer         = {{Plain list |
*Zal Batmanglij
*Brit Marling
}}
| starring       = {{Plain list |
*Brit Marling
*Alexander Skarsgård
*Ellen Page
*Patricia Clarkson
}}
| music          = Halli Cauthery
| cinematography = Roman Vasyanov
| editing        = {{Plain list |
*Andrew Weisblum
*Bill Pankow
}}
| studio             = Scott Free Productions
| distributor      = Fox Searchlight Pictures
| released       =   
| runtime        = 116 minutes
| country        = {{Plain list |
*United States
*United Kingdom
}}
| language       = English
| budget         = $6.5 million 
| gross          = $2.4 million 
}}
The East is a 2013 English-language thriller film directed by Zal Batmanglij and starring Brit Marling, Alexander Skarsgård, and Ellen Page. Writers Batmanglij and Marling spent two months in 2009 practicing freeganism and co-wrote a screenplay inspired by their experiences and drawing on thrillers from the 1970s. The American studio Fox Searchlight Pictures had bought rights to distribute Batmanglijs previous film Sound of My Voice and also collaborated with the director to produce The East. With Ridley Scott as producer and Tony Scott as executive producer, Fox Searchlight contracted Scott Free Productions, headquartered in London, to produce the film. The East was filmed in two months in Shreveport, Louisiana at the end of 2011. The film premiered at the 2013 Sundance Film Festival on  , 2013. It was released in theaters on  , 2013.

==Plot==
 
 attacks against corporations in an attempt to expose their corruption. Calling herself Sarah, she joins local drifters in hitching rides on trains, and when one drifter, Luca, helps her escape from the police, she identifies the symbol of The East hanging from Lucas car mirror. Sarah self-inflicts an arm injury that she tells Luca was caused in the escape so he can get medical attention for her. He takes her to an abandoned house in the woods where members of The East live and one of the members, Doc, treats her cut.

Sarah is given two nights to recover before she must leave the squat. During this time Sarah secretly observes a ritual by the groups members. They show her how they operate with an elaborate dinner set up involving straitjackets, to show her how selfish she and others live their lives.  Sarah is caught one night when spying by the deaf Eve and has a conversation in sign language with her. Sarah tells Eve that she is an undercover agent and threatens Eve with jail if she stays with the group; Eve leaves the next morning.  Sarah is recruited to fill the missing members role on a "jam," which is a code word for their anarchistic activities. After seeing the effectiveness of the pharmaceutical jam, compounded by her growing attraction to charismatic leader Benji (Alexander Skarsgård), Sarah gradually questions the moral underpinnings of her undercover duty.    Sarah reluctantly participates in The Easts next jam and comes to learn that each member of The East has been personally damaged by corporate activities. For example, Doc has been poisoned by a fluoroquinolone antibiotic and his neurosystem is degenerating. The East infiltrates a fancy party and puts a small, lethal dose of the drug in everyones champagne. As one executives mind and body begin to degenerate, the truth is revealed.

Another East member, Izzy (Ellen Page) is the daughter of a petrochemical CEO. The group uses the father/daughter connection to gain access to the CEO and forces him to bathe in the waterway he has been using as a toxic dumping ground. This jam goes wrong when security guards arrive and shoot Izzy in the back as she and the others flee. Back at the squat, because of the poison antibiotic, Docs hands tremble too much for him to perform surgery on Izzy. Sarah offers to do it for him and he tells her what to do. She manages to remove the bullet from Izzys abdomen, but Izzy dies and is buried near The Easts house.

Even though Sarah and Benji have grown closer and Sarah implores him to just disappear, he insists that they go together to complete the final jam. Sarah refuses at first but finally gives in and the two begin a long drive, during which Sarah falls asleep. When she awakens, she realizes that Benji is driving her to the Hiller Brood headquarters outside Washington, D.C. He reveals that he has always suspected her of being a Hiller Brood operative, and that Luca also thought this, but brought her in as a test. Benji wants Sarah to obtain an NOC list of Hiller Brood agents all over the world, which will be The Easts third and final jam, to "watch" them. Sarah confronts Sharon about the firms activities, thus revealing her new allegiances. Sharon has Sarahs cellphone confiscated as she leaves the building. As Hiller Brood was sharing information about their activities with the FBI, at the same time they (the FBI) raid The Easts hideout and arrest Doc, who sacrifices himself to ensure the getaway of the remaining members. Sarah tells Benji shes failed to get the agent data because Benji reveals he means to post all the Hiller Brood agents online to make an example of them. Since they are undercover, however, it is likely theyd be killed. Sarah can choose which side to be on, but decides she just cant sign on with The East. She and Benji part at a truck-stop as Benji heads out of the country. In truth, Sarah still has the data, and it is obvious that her time undercover with The East has changed her. The film ends with an epilogue of her personally contacting her former coworkers (those undercover) and attempting to demonstrate what Hiller Brood clients want to protect. She clearly hopes to change their minds about their undercover activities and perhaps join her in ecological activism. She is paying tribute to the things The East believes in and attempting to make a difference in her own way, without causing harm to anyone.

==Cast==
 
* Brit Marling as Sarah Moss / Jane Owen
* Alexander Skarsgård as Benji
* Ellen Page as Izzy
* Toby Kebbell as Doc / Thomas Ayres
* Shiloh Fernandez as Luca
* Julia Ormond as Paige Williams
* Patricia Clarkson as Sharon
* Jason Ritter as Tim
* Danielle McDonald as Tess
 

==Production==
 
* Zal Batmanglij – director, screenwriter
* Brit Marling – producer, screenwriter
* Ridley Scott – producer Michael Costigan – producer
* Jocelyn Hayes-Simpson – producer
* Tony Scott – executive producer
* Roman Vasyanov – cinematographer
* Andrew Weisblum – editor
* Bill Pankow – editor
* Halli Cauthery – music
* Harry Gregson-Williams – themes for score
* Alex DiGerlando – production designer
* Jenny Gering – costume designer
* Nikki Black – art director
* Cynthia Slagter – set decorator
 
 All The The Bourne Michael Clayton to craft The East,  which they wrote before they began filming Sound of My Voice for a 2011 release. 

  a drug to help quit smoking that resulted in some people committing suicide. He said they considered focusing on banks due to the financial crisis of 2007–2008, but they chose the pharmaceutical industry so the mission in the film would have emotional resonance. They titled the film The East to make a variety of references. The director explained, "The East is ... the East Coast, which is like something in our American collective consciousness—New England, tony, center of power. The Wicked Witch of the East in the Oz mythology was the bad witch because the book was about how the Midwest was getting screwed over by the east, by Washington. And then of course we have the Middle East or the Far East, which is seen as different or other. The ultimate Other. So, its funny that this word means two things, and I thought that was an interesting name for a resistance group that is combined of kids from New England who want to make themselves the Other." 
 Michael Costigan, who liked Sound of My Voice, got a copy of the screenplay for The East. Costigan liked the screenplay and approached Batmanglij to say that he and his fellow producers Ridley Scott and Tony Scott at Scott Free Productions were interested in making the film.  After the festival, Fox Searchlight Pictures acquired distribution rights for Sound of My Voice and Another Earth (also starring Marling). In the process, the distributor greenlighted production of The East.  By September 2011, Marling and Alexander Skarsgård were cast in the starring roles. Felicity Jones was attached to play Izzy, but she dropped out to promote Like Crazy. Jones was replaced by Ellen Page. 

Production of the film, which had a budget of  ,    took place in Shreveport, Louisiana.  Production designer Alex DiGerlando converted an alternative lifestyle club in Shreveport into a house for The East. The club was originally painted black and gold, and it was repainted different shades of green for the film.  Filming took place in late 2011;  Batmanglij said it lasted  . 

The director compared the films ambivalent ending to the one in the 2002 film 25th Hour: "I feel like its almost as if the film’s events never happened at its end... Its sort of like what were all capable of if we put our minds to it. Theres a lot of work that needs to be done in order to make changes, even for her to make changes." 

==Release==

===Screenings===
 ]] Michigan Theater in Ann Arbor, Michigan on  , 2013. Over 1,300 people were in attendance for the screening, and Zal Batmanglij and Brit Marling showed for a Q&A session.  Leading up to its theatrical release in May 2013, The East was chosen as the closing night film of the South by Southwest (SXSW) Film Conference and Festival,  and went on to screen at the Phoenix Film Festival,  Seattle International Film Festival,  and the San Francisco International Film Festival.  The Washington Post reported that Q&A sessions after the films screenings were popular. It said, "Filmgoers might not have agreed on their feelings about The East, but they had one thing in common: They needed to talk about it." According to Batmanglij, the Sundance screening retained 98% of the audience for its Q&A session. 

The East had its New York City premiere on  , 2013.  It was released in four theaters in Los Angeles and New York City on May 31, 2013.   The film grossed $77,031 over the opening weekend, a theater average of $19,258.    Indiewire reported, "Thats a strong number in general and also a big step up from Batmanglij and Marlings previous collaboration."  The Wrap said the film had a strong start,  while Box Office Mojo said it "opened to modest results".  In its second weekend, the release was expanded to  .    It grossed $228,561 over the weekend. Overall, it grossed  . 

===Critical response===
  
The Wall Street Journal reported that at the Sundance Film Festival, the film "opened to mostly strong reviews".    Variety (magazine)|Variety s Justin Chang reviewed the film, "This clever, involving spy drama builds to a terrific level of intrigue before losing some steam in its second half." He noted that, "the appreciable growth in filmmaking confidence here should translate into a fine return on Fox Searchlights investment".  John DeFore, writing for The Hollywood Reporter, described The East as "a social-conscience espionage film that has actually thought about its eco-terrorism themes beyond figuring out how to mine them for suspense". He said, "Batmanglij balances emotional tension with practical danger nicely, a must in a story whose activist protagonists can make no distinction between the personal and the political."  Joe Neumaier of the New York Daily News gave The East 5 stars and highlighted it as a Sundance standout. He said the film felt like a "sophisticated" Earth First! take of The Parallax View and other 1970s films with the theme of paranoia. 

Logan Hill, reviewing for indieWire, said, "Fast-paced and energetic, The East hits a beat and hurries along to the next Jam. As slickly paced as a big-studio espionage movie, it nearly succeeds as a pure adrenaline-rush thriller. In the end, the problem isnt that theres too much plot, but rather a certain dramatic illogic." Hill commended the cast and said of the direction, "Batmanglij has a particular talent for capturing that unmoored, twentysomething search for meaning, and the tight-knit allure of a group that offers a reason for living. But the film is so plot-driven, those dont have much room to breathe." 

Following the films release in May, the film review website Metacritic surveyed   and assessed 26 reviews to be positive, 9 to be mixed, and 1 to be negative. It gave an aggregate score of 68 out of 100, which it said indicated "generally favorable reviews".  The similar website Rotten Tomatoes surveyed   and, categorizing the reviews as positive or negative, assessed 103 as positive and 37 as negative. It gave the film a score of 74% and summarized the critical consensus, "Tense, thoughtful, and deftly paced, The East is a political thriller that never loses sight of the human element." 

===Home media===
The East was released on DVD and Blu-ray Disc in the United States on September 17, 2013  and in the UK on November 5, 2013. 

==See also==
* Eco-terrorism in fiction
* Hunted (TV series)|Hunted, a television series about an operative for a private intelligence company

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 