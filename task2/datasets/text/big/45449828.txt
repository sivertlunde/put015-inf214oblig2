Manju Virattu (film)
{{Infobox film
| name           = Manju Virattu
| image          = 
| image_size     =
| caption        = 
| director       = L. S. Valaiyapathy
| producer       = N. Anbazhagan
| writer         = L. S. Valaiyapathy
| starring       =   Deva
| cinematography = K. B. Ahmed
| editing        = K. B. Harikrishnan
| distributor    =
| studio         = Thennagam Creations
| released       =  
| runtime        = 150 minutes
| country        = India
| language       = Tamil
}}
 1994 Tamil Tamil drama Murali and Deva and was released on 9 December 1994.   

==Plot==

Pandi (Murali (Tamil actor)|Murali) and Poovazhagi (Mohana) hate each other since their childhood. According to their custom, Pandi has to get married with the brave man Pandi or with the womaniser Dhanraj (Vasu Vikram). The hatred between Pandi and Poovazhagi become more and more fierce until it creates troubles between their respective family. What transpires later forms the crux of the story.

==Cast==
 Murali as Pandi
*Mohana as Poovazhagi Janagaraj as Gnanam
*Vasu Vikram as Dhanraj
*Shanmugasundaram as Sethupathi, Pandians father
*Srividya as Meenakshi, Pandis mother
*Nambirajan as Sakthivel, Poovazhagis father
*Janaki as Rukmani, Poovazhagis mother
*Sabitha Anand as Chellamma
*S. N. Lakshmi
*Chitraguptan
*Master Vijayakumar as Pandi (child) Monica as Poovazhagi (child)

==Soundtrack==

{{Infobox album |  
| Name        = Manju Virattu
| Type        = soundtrack Deva
| Cover       = 
| Released    = 1994
| Recorded    = 1994 Feature film soundtrack |
| Length      = 23:45
| Label       =  Deva
| Reviews     = 
}}

The film score and the soundtrack were composed by film composer Deva (music director)|Deva. The soundtrack, released in 1994, features 5 tracks with lyrics written by Vaali (poet)|Vaali. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Duration
|- 1 || Kannu Padum || K. S. Chithra, Rajagopal || 5:21
|- 2 || Mamma Unn Perai || S. P. Balasubrahmanyam, K. S. Chithra || 4:39
|- 3 || Mammeh Vangi || S. Janaki || 4:59
|- 4 || Mano || 4:31
|- 5 || Uchiele Manju Thani || K. S. Chithra || 4:15
|}

==References==
 

 
 
 
 
 
 