Epsilon (film)
{{Infobox film
| name = Epsilon
| caption =
| image	=Alien Visitor FilmPoster.jpeg
| director = Rolf de Heer
| producer = Domenico Procacci Rolf de Heer 
| writer = Rolf de Heer
| narrator =
| starring = Ulli Birve Syd Brisbane
| music = Graham Tardif 
| cinematography = Tony Clark
| editing = Tania Nehme 
| distributor = Roadshow (AU) Miramax (US, UK)
| studio =
| released = 23 April 1997 (Australia)
| runtime = 88 minutes (1995 version) 92 minutes (1997 version)
| country = 
| language = English
| budget =
| gross = A$2,373 (Australia) 
| preceded_by =
| followed_by =
}}

Epsilon (also titled Alien Visitor) is a 1995 science fiction film that was directed by Rolf de Heer. It features Ulli Birve and Syd Brisbane (and Aletha McGrath, but in the 1997 version only) .   The extended version of the film runs for 92 minutes and was distributed by Miramax.

==Plot==
The film tells the story of a young female (Birve) that was sent from the planet Epsilon to judge how humans have managed the planet. She crash lands naked in a desert, but is found by a surveyor (Brisbane) who gives her some clothes. She informs the surveyor that other alien races consider humans to be failures that suffer from carelessness and greed. She has the ability to transport herself and the surveyor instantly to any location, and she uses this to demonstrate her point. Eventually the two fall in love.  

==Production==
De Heer came up with the idea for the film while driving to dinner at a friends house. Filming took over eight months. 

==References==
{{reflist|refs=

   

   

   

}}

==External links==
*  
*  

 

 
 
 
 

 