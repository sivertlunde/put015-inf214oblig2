Paranormal Activity 2
{{Infobox film
| name           = Paranormal Activity 2
| image          = Paranormal Activity 2 Poster.jpg
| caption        = Theatrical release poster Tod Williams
| producer       = Oren Peli Jason Blum Akiva Goldsman
| screenplay     = Michael R. Perry Christopher B. Landon Tom Pabst
| story          = Michael R. Perry
| starring       = Katie Featherston Sprague Grayden Brian Boland Molly Ephraim
| cinematography = Michael Simmonds
| editing        = Gregory Plotkin
| studio         = Blumhouse Productions
| distributor    = Paramount Pictures
| released       =   
| runtime        = 91 minutes 98 minutes (Directors cut)
| country        = United States
| language       = English
| budget         = $3 million   
| gross          = $177.5 million   
}}
 American supernatural Tod Williams and written by Michael R. Perry. The film is a prequel to the  2007 film Paranormal Activity, beginning two months before and following up with the events depicted in the original film. It was released in theaters at midnight on October 22, 2010 in the United States, the United Kingdom, Canada, Mexico, Brazil, Poland and Ireland.   

==Plot==
  evil spirits" but Dan arrives home and catches her burning Salvia officinalis|sage, and Martine is fired as a result.

Kristi believes that their home is haunted and tells Daniel; Daniel reviews the footage but dismisses her claim. Katie and Kristi talk about being tormented by a demon when they were children. Dans daughter, Ali (Molly Ephraim), begins investigating the mysterious occurrences; she discovers that humans can make deals with demons for wealth or power by forfeiting the soul of their first-born son— Hunter was the first male to be born on Kristis side since the 1930s.
 
The violence escalates; The familys German shepherd, Abby, becomes aware of the demons presence and is attacked. Dan and Ali take Abby to the veterinarian, leaving Kristi alone with Hunter. When Kristi checks on the baby, the demon assaults her and drags her down to the basement, where she stays for an hour. Finally, the basement door opens and a possessed Kristi walks out. The following day, Ali is home with Kristi, who will not get out of bed. She finds the basement door covered in scratches and the word meus (Latin for "Mine"), etched into it.

Ali goes upstairs to check on Hunter and sees Kristi there, with a strange bite mark on her leg. Ali tries to get Hunter but Kristi furiously orders her not to touch him. Ali, now terrified, begs Daniel to come home; after he arrives, she shows him the footage of Kristis attack. He immediately calls Martine, who prepares a cross to exorcise the demon; Kristi will have no memory of having been possessed. Since the curse can only be transferred to a blood relative, Dan tells Ali he is going to pass the demon onto Katie so that Kristi and Hunter will be safe. Ali begs him not to because it is unfair to Katie, but Dan sees no other way to save his wife and son.

That night, when Dan tries to use the cross on Kristi, she attacks him and all the houselights go out. Using the handheld cameras night vision, he finds Kristi and Hunter have disappeared. Furniture begins toppling over, and the chandeliers shake. Dan chases Kristi into the basement, where she attacks him. He touches her with the cross, causing her to collapse. Dan hears demonic growls and finally, the shaking stops. Dan puts Kristi to bed and burns a photo of a young Katie (the same photo Micah later finds in the attic of his and Katies house in the first film).

Three weeks later, Katie visits and explains that strange things have started happening at her house. She then returns home to her boyfriend Micah. On October 9, the night after Micah is killed, a possessed and bloodstained Katie breaks into Dan and Kristis home and kills Dan by snapping his neck while he watches TV. She then kills Kristi in Hunters room, violently hurling her at the camera, and takes the baby. She leaves the room, cradling Hunter. The screen fades to black as Hunters crying turns into laughter.
 Katie and Hunters whereabouts remain unknown.

==Cast==

* Sprague Grayden as Kristi Rey
* Brian Boland as Daniel Rey, Kristis husband
* Molly Ephraim as Ali Rey, Dans daughter
* Katie Featherston as Katie, Kristis older sister 
* Micah Sloat as Micah 
* Seth Ginsberg as Brad, boyfriend of Ali
* Vivis Cortez as Martine, the nanny / housekeeper
* Jackson Xenia Prieto and William Juan Prieto as Hunter Rey, Dan and Kristis infant son

==Production== Paramount and final film Tod Williams directed Paranormal Activity 2, which started production in May 2010 and finished filming in only three weeks. 

==Marketing==
In a special promotion set up by the films producers, participants had a chance to win a free movie ticket if they were in the top twenty cities to demand the film, via Eventful.com.  The teaser trailer was seen with   upon its release on June 30, 2010.   . A second theatrical trailer was released on October 1, 2010. The trailer was attached to Devil (2010 film)|Devil, My Soul to Take and Jackass 3D. 

==Release==
The film was released in the United States on October 22, 2010. The film was made available in IMAX format as well as standard. 

===Critical reception=== average score normalized rating out of 100 to reviews from mainstream critics, the film has received a "mixed or average" score of 53, based on 23 reviews.    Roger Ebert, who awarded the original film three and a half stars, awarded Paranormal Activity 2 one-and-a-half out of a possible four stars. 

===Box office===
Paranormal Activity 2 broke the record for biggest midnight gross for an R-rated movie with $6.3 million, beating the previous record-holder Watchmen (film)|Watchmen by $4.6 million, and broke the record for biggest opening for a horror movie of all time.  On its opening day, Paranormal Activity 2 placed number one at the box office, making $20,100,000 and finished with a total of $41,500,000 estimated over the weekend, placing first at the box office.  It has currently grossed $84,752,907 in North America and $92,759,125 overseas, giving the film a worldwide total of $177,512,032. 

===Home media===
Paranormal Activity 2 was released on DVD/Blu-ray Disc|Blu-ray and video on demand/pay-per-view on February 8, 2011, and includes an unrated directors cut and deleted scenes. Paranormal Activity 2 was placed at #1 for top Blu-ray and rental sales for its first week of being out. 

==Soundtrack==
* My Last Breath - Evanescence
* Electrical Storm - U2
* The Downfall of Us All - A Day To Remember

==Sequels==
 
Paranormal Activity 3 is a 2011 American supernatural horror film, directed by Henry Joost and Ariel Schulman. It is the third film of the Paranormal Activity series and serves as a prequel, set 18 years prior to the events of the first two films. It was released in theaters on October 21, 2011.

The fourth installment, Paranormal Activity 4, was released on October 18, 2012 in the United States. It was planned to take place five years after the events of Paranormal Activity 2; Katie and Hunter have moved into a new house and their neighbors, Alex and her mother, begin experiencing paranormal events taking place in their own home.  Although a box office success, Paranormal Activity 4 was a critical failure.

On October 21, 2012,   being pushed back to January 3, 2014.  Paramount later moved Paranormal Activity 5 to March 13, 2015, and then to October 23, 2015, also giving the film a new title ( ).

==See also==
*List of ghost films

==References==
 

==External links==
* 
* 
* 
* 
* 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 