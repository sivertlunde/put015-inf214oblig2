Golden Chicken 2
 
 
{{Infobox film
| name           = Golden Chicken 2
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = 金雞2
| director       = Samson Chiu
| producer       = Peter Chan Hui Yuet-Jan Eric Tsang
| writer         = Mark Cheung Lam Oi Wah James Yuen
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Sandra Ng
| music          = Peter Kam Joseph Wong
| cinematography = Keung Kwok-Man Cheung Ka-Fai
| studio         = 
| distributor    = 
| released       =  
| runtime        = 104 min
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          = 
}}
Golden Chicken 2 (金雞2 gam1 gai1) is a 2003 Hong Kong film directed by Samson Chiu. It is a sequel to the 2002 Golden Chicken.

==Cast and roles==
 
* Sandra Ng – Kam
* Bing – Granny B in the queue
* Cha Man-Wet – Mainland businessman A
* Chan Chi-Shuen – Teddy boy A
* Erica Chan – Mahjong player D
* Chan Lai-Fun – Mother-in-laws relative
* Chan Ming-Yam – Building watchman
* Chan Wai-Ling – Friend D at temple
* Ronald Cheng
* Dicky Cheung – Water man
* Jacky Cheung – Quincy
* Cheung Kin-Hung – CCB investigator A
* Sabrina Cheung – Northern hooker B
* Kenny Chin – Customer in appliance store
* Choi Tat-Wah – Happy Corner worker C
* Chui Hin-Fung – CCB investigator B
* Alva Chun – Northern hooker A
* Wancy Dai – 7-Eleven shopkeeper
* Fung Ka-Ying – Nightclub hostess B
* Ho Ka-Kou – Nightclub guest A
* Nichole Hor – Mahjong player B
* Ho Yat-Wa – Future cop B
* Hua Tau – Teddy boy B
* Carlos Koo – Male jogger
* Silver Kwok – Mahjong player C
* Leon Lai – Doctor Chow Man Kwong
* Lai Li-Li – Female jogger
* Rita Lai – Madam As best maid
* Lam Lai-Kuen – 7-Eleven customer D
* Lam Si-Wing – Registrar assistant chief executive in year 2046
* Lau Kuen – Mainland businessman B
* Lau Kwun-Hung – Temple worker B
* Lau Man-Ting – Woman D in the queue
* Lau Yo-Ha – Woman in house
* Lau Yu-Hai – Granny A in the queue
* Law Dong-Mei – Woman A in the queue
* Angelica Lee	
* Lee Ka-Sin – Nightclub hostess A
* Lee Ka-Wing – Happy Corner worker A
* Leung Ho-Ming – Pervert at temple
* Leung Wai-Ching – Woman C in the queue
* Leung Wai-Yan – Fishball girl C
* Leung Yau-Sen – Old man in the queue
* Ling Ling-Chui – Friend C at temple
* Michelle Lo – Mahjong player A
* Lui Chui-Tsang – Madam As mother-in-law
* Fiona Lui – Frightened female customer
* Lui Ho-Kwong – Noodle Stall worker B
* Lum Chun-Ka – Massage parlour manager
* May – Filipino maid
* Ming Ng – Dog owner
* Mok King-Wah – Nightclub guest B
* Kenneth Ng – Kums father
* Vincent Ng – Noodle stall worker A
* Ivy Pang – Friend A at temple
* Samdasani Rajesh – Indian client
* Nicky Shih – Happy Corner worker B
* Sin She-Kin – 7-Eleven Customer B
* Siu Hung – Temple worker A
* Sum Wai-Leung – Mainland China police B
* Tam Yuen-Si – Woman B in the queue
* Tang Hon-Tai – Future cop A
* Pat Tang – Woman E in the queue
* Lawrence Tan
* Kristal Tin
* Chapman To – Club owner
* Tsang Kit-Hing – Happy Corner cleaning maid
* Tsang Lap-Hung – Happy Corner worker D
* Anthony Wong Chau-sang – Chow
* Wong Chiu-Wa – Frightened male customer
* Felix Wong
* May Wong – Friend B at temple
* Wong Tsz-Kwan – Fishball girl B
* Wu Yak-Ping – 7-Eleven customer A
* Yai Tat-Sing – Little boy
* Yau Ka-Lam – Little girl
* Amy Yeung – Northern hooker C
* Devily Yeung – Fishball girl A
* Yip Ying-Wai – Marriage registrar
* Yu Cheung-Man – Mainland China police A
* Yuen Mun-Lam – 7-Eleven customer C
 
 {{Cite web |url=http://www.imdb.com/title/tt0396030/ |title=Golden Chicken 2 
 |accessdate=2 July 2010 |publisher=imdb.com}} 
   

==See also==
*Prostitution in Hong Kong

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 