List of Bollywood films of 2004
 
 
 
This is a list of films produced by the Bollywood film industry based in Mumbai in 2004 in film|2004: 

==2004==
{| class="wikitable"
! Title !! Director !! Cast !! Genre
|-
|99.9 FM (movie)|99.9 FM|| Sanjay Bhatia || Shawar Ali, Dipannita Sharma, Raima Sen|| Comedy, Drama
|-
|Aabra Ka Daabra|| Dheeraj Kumar || Hansika Motwani, Shweta Tiwari, Vishal Lalwani, Archana Puran Singh || Fantasy
|-
| || Madhur Bhandarkar || Akshay Kumar, Sunil Shetty, Lara Dutta, Raveena Tandon || Action
|- Ab Bas|| Rajesh Kumar Singh || Shawar Ali, Rohit Roy, Diana Hayden || Adult
|-
|Ab Tumhare Hawale Watan Saathiyo || Anil Sharma || Amitabh Bachchan, Akshay Kumar, Bobby Deol, Divya Khosla Kumar || War, Drama
|- John Abraham || Thriller
|-
|Agnipankh (2004 film)|Agnipankh|| Sanjeev Puri || Jimmy Shergill, Rahul Dev, Divya Dutta, Shamita Shetty ||
|-
|Aitraaz || Abbas-Mustan || Priyanka Chopra, Akshay Kumar, Kareena Kapoor || Drama, Romance, Thriller
|-
|Asambhav|| Rajiv Rai || Arjun Rampal, Priyanka Chopra || Mystery, Thriller
|- Banana Brothers|| Girija Shanker || Johny Lever, Anupam Kher, Gulshan Grover, Gursewak Mann, Seema Rahmani || Comedy
|- Black Friday Black Friday|| Imtiaz Ali, Zakir Hussain || Crime Fiction, Drama, Historical Drama
|-
|Bardaasht (2004 film)|Bardaasht|| E. Jasir || Bobby Deol, Lara Dutta, Ritesh Deshmukh, Rahul Dev || Action, Drama, Thriller
|-
|Chameli (2004 film)|Chameli|| Sudhir Mishra || Kareena Kapoor, Rahul Bose, Rinke Khanna || Drama
|-
|Chand Bujh Gaya|| Sharique Minhaj || Faizal Khan, Shama Sikander, Aliza || Romance
|-
| || Tigmanshu Dhulia || Jimmy Shergill, Irrfan Khan, Uday Chopra, Hrishita Bhatt, Namrata Shirodkar || Action
|-
|Deewaar (2004 film)|Deewaar|| Milan Luthria || Amitabh Bachchan, Sanjay Dutt, Akshaye Khanna, Kay Kay Menon, Amrita Rao || Drama
|-
|Dev (2004 film)|Dev || Govind Nihalani || Amitabh Bachchan, Fardeen Khan, Kareena Kapoor, Rati Agnihotri, Om Puri, Amrish Puri || Drama
|- John Abraham, Uday Chopra, Esha Deol, Rimi Sen || Thriller
|-
|Dil Bechara Pyaar Ka Maara|| Omkarnath Mishra || Vikas Kalantri, Aslam Khan, Divya Palat, Rajpal Yadav, Mallika Kapoor || Comedy
|-
|Dil Maange More || Anant Mahadevan || Shahid Kapoor, Ayesha Takia, Soha Ali Khan, Tulip Joshi || Romance
|-
|Dil Ne Jise Apna Kahaa|| Atul Agnihotri || Salman Khan, Preity Zinta, Bhoomika Chawla || Romance
|-
|Dobara|| Shashi Ranjan || Raveena Tandon, Jackie Shroff, Mahima Chaudhry || Drama
|-
|Dukaan (2004 film)|Dukaan|| Iqbal Durrani || Vikas Kalantri, Rambha (actress)|Rambha, Rati Agnihotri || Adult Romance
|- Ek Se Badhkar Ek|| Kundan Shah || Sunil Shetty, Raveena Tandon, Isha Koppikar || Comedy
|-
|Fida || Ken Ghosh || Fardeen Khan, Kareena Kapoor, Shahid Kapoor, Kim Sharma || Drama, Romance, Thriller
|-
|Flavors (2004 film)|Flavours|| Krishna D. K.   Raj Nidimoru || Anupam Mittal, Jicky Schnee, Pooja Kumar, Reef Karim || Drama
|- Arbaaz Khan || Drama
|-
|Gayab|| Prawal Raman || Tusshar Kapoor, Antara Mali, Raghubir Yadav || Fantasy, Thriller
|-
|Girlfriend (2004 film)|Girlfriend|| Karan Razdan || Amrita Arora, Isha Koppikar, Aashish Chaudhary || Drama, Romance
|- Green Card Fever|| Bala Rajasekharuni || Deep Katdare, Purva Bedi, Vikram Dasu || Drama
|-
|Gumnaam (2004 film)|Gumnaam|| Neeraj Pathak || Dino Morea, Mahima Chaudhry || Romance, Thriller
|-
|Hava Aney Dey|| Partho Sen-Gupta|| Nishikant Kamat, Tannishtha Chatterjee, Rajshree Thakur || Drama
|-
|Hawas (2004 film)|Hawas|| Karan Razdan || Shawar Ali, Meghna Naidu, Tarun Arora || Adult Romance
|- Arbaaz Khan, Sunil Shetty, Arshad Warsi, Jackie Shroff, Paresh Rawal, Amrish Puri || Comedy, Drama, Romance, Family, Crime
|-
|Hum Tum|| Kunal Kohli || Saif Ali Khan, Rani Mukerji, Kirron Kher, Jimmy Shergill, Rishi Kapoor, Rati Agnihotri, Isha Koppikar, Abhishek Bachchan || Comedy, Romance
|-
|Hyderabad Blues 2|| Nagesh Kukunoor || Tisca Chopra, Elahe Hiptoola, Jyoti Dogra, Vikram Inamdar, Anu Chengappa || Drama
|-
|I - Proud To Be An Indian || Puneet Issar || Sohail Khan, Gulzar Inder Chahal, Puneet Issar, Tim Lawrence,  || Action
|-
| || Shrey Srivastava || Dino Morea, Namrata Shirodkar, Sanjay Suri, Henna, Rajpal Yadav || Action
|-
| || Pankaj Parashar || Nethra Raghuraman, Manoj Bajpai, Isha Koppikar || Action
|-
|Ishq Hai Tumse|| G. Krishna || Bipasha Basu, Dino Morea || Drama
|-
|Julie (2004 film)|Julie|| Deepak Shivdasani || Neha Dhupia, Yash Tonk, Sanjay Kapoor, Priyanshu Chatterjee || Adult Drama
|-
|Kaun Hai Jo Sapno Mein Aaya|| Rajesh Bhatt || Anupam Kher, Richa Pallod, Rakesh Bapat, Kader Khan || Romance
|-
|Khakee (2004 film)|Khakee || Rajkumar Santoshi || Amitabh Bachchan, Akshay Kumar, Ajay Devgan, Aishwarya Rai, Tusshar Kapoor, Atul Kulkarni || Action, Thriller
|- Sabiha Kumar || Kirron Kher, Amar Ali Malik || Drama
|- King of Bollywood|| Piyush Jha || Om Puri, Sophie Dahl, Kavita Kapoor || Comedy
|-
|Kis Kis Ki Kismat|| Govind Menon || Dharmendra, Mallika Sherawat, Rati Agnihotri, Satish Shah || Comedy
|-
|Kismat (2004 film)|Kismat|| Guddu Dhanoa || Priyanka Chopra, Bobby Deol || Thriller
|-
|Kiss Kis Ko|| Sharad Sharan || Karan Oberoi, Sudhanshu Pandey, Sherrin Varghese, Siddharth Haldipur, Chaitnya Bhosale || Adult Comedy
|-
|Krishna Cottage || Santram Varma || Sohail Khan, Isha Koppikar, Natassha || Thriller
|- Samir Karnik|| Vivek Oberoi, Aishwarya Rai, Amitabh Bachchan, Sunil Shetty, Diya Mirza, Rati Agnihotri || Romance
|- Ahmed Khan John Abraham, Nauheed Cyrusi || Action, Drama, Romance
|-
|Lakshya (2004 film)|Lakshya|| Farhan Akhtar || Hrithik Roshan, Preity Zinta, Sharad Kapoor, Amitabh Bachchan || War, Drama
|-
|Lets Enjoy (2004 film)|Lets Enjoy|| Ankur Tewari Siddharth Anand Kumar || Shomendra Bose, Aashish Chaudhary, Roshni Chopra, Arzoo Govitrikar, Sahil Gupta, Dhruv Jagasiya || Adult Thriller
|- John Abraham, Priyanshu Chatterjee || Drama, Romance, Thriller
|-
|Main Hoon Na || Farah Khan || Shahrukh Khan, Sushmita Sen, Sunil Shetty, Amrita Rao, Zayed Khan, Kiron Kher, Naseeruddin Shah, Kabir Bedi, Satish Shah, Boman Irani, Bindu (actress)|Bindu, Murli Sharma, Rakhi Sawant  || Action, Musical
|- Irfan Khan, Moushumi Chatterjee, Om Puri || Mystery Drama
|-
|Masti (2004 film)|Masti || Indra Kumar || Vivek Oberoi, Aftab Shivdasani, Lara Dutta, Ritesh Deshmukh, Ajay Devgan, Genelia Dsouza, Tara Sharma, Amrita Rao, Rakhi Sawant || Comedy
|-
|  || M. F. Hussain || Tabu (actress)|Tabu, Kunal Kapoor, Rajpal Yadav, Nadira Babbar || Romance, Musical Drama
|-
|Meri Biwi Ka Jawab Nahin|| S. M. Iqbal Pankaj Parashar || Sridevi, Akshay Kumar || Action, Comedy, Romance
|-
|Mughal-e-Azam (2004 film)|Mughal-e-Azam (Colorized Version)|| K. Asif || Dilip Kumar, Madhubala, Prithviraj Kapoor || Drama
|-
|Mujhse Shaadi Karogi || David Dhawan || Salman Khan, Priyanka Chopra, Akshay Kumar || Comedy, Romance
|-
|Murder (2004 film)|Murder || Anurag Basu || Mallika Sherawat, Emraan Hashmi, Ashmit Patel || Thriller
|-
|Musafir (2004 film)|Musafir || Sanjay Gupta || Anil Kapoor, Sanjay Dutt, Sameera Reddy, Koena Mitra, Mahesh Manjrekar || Thriller
|-
|Muskaan (2004 film)|Muskaan|| Rohit Nayyar Rohit Manash|| Aftab Shivdasani, Gracy Singh, Kader Khan, Anupam Kher || Romance, Drama
|-
|Naach (2004 film)|Naach || Ram Gopal Varma || Antara Mali, Abhishek Bachchan, Ritesh Deshmukh || Drama
|-
|Naam Gum Jaayega|| Amol Shetge || Aryan Vaid, Rakesh Bapat, Dia Mirza, Divya Dutta || Romance
|-
|Nothing But Life|| Rajiv Anchal || R. Madhavan, Kaveri, Sreenivasan || Drama
|- John Abraham, Udita Goswami || Drama
|- Black Comedy
|-
|Phir Milenge || Revathy || Shilpa Shetty, Salman Khan, Abhishek Bachchan || Drama
|-
|Plan (film)|Plan || Hriday Shetty || Sanjay Dutt, Priyanka Chopra, Dino Morea, Sanjay Suri, Riya Sen, Rohit Roy, Sameera Reddy || Action
|-
| || Dilip Shukla || Akshay Kumar, Raveena Tandon || Action
|- Kabir Sadanand || Akshay Kapoor, Tanishaa Mukerji|Tanisha, Yash Tonk, Reshmi Nigam||
|-
|Raincoat (2004 film)|Raincoat || Rituparno Ghosh || Ajay Devgan, Aishwarya Rai || Drama
|-
| || Mahesh Manjrekar || Sunil Shetty, Neha Dhupia, Dino Morea, Himanshu Malik, Amrita Arora, Abhishek Bachchan, Bipasha Basu, Sanjay Dutt  || Horror
|-
|Rok Sako To Rok Lo|| Arindam Chowdhury || Sunny Deol, Yash Pandit || Sports Comedy
|-
|Rudraksh (film)|Rudraksh|| Mani Shankar || Sunil Shetty, Sanjay Dutt, Bipasha Basu, Isha Koppikar || Thriller
|- Jeeva || Abhishek Bachchan, Bhoomika Chawla, Ayesha Jhulka, Mahesh Manjrekar || Action, Drama, Romance
|-
|Shaadi Ka Laddoo|| Raj Kaushal || Sanjay Suri, Mandira Bedi, Divya Dutta, Aashish Chaudhary || Comedy
|-
| || Puri Jagannath || Tusshar Kapoor, Gracy Singh, Amrita Arora || Action Drama
|-
|Sheen (2004 film)|Sheen|| Ashok Pandit|| Tarun Arora, Raj Babbar, Anoop Soni, Kiran Juneja || Romance
|- Darshan Bagga || Kanishka Sodhi, Jas Pandher || Drama
|-
| || Anupam Sinha || Anupam Kher, Aftab Shivdasani, Shriya Saran || Drama
|-
|Silence Please...The Dressing Room|| Sanjay Srinivas || Salil Ankola, Sonali Kulkarni, Tom Alter || Thriller
|- Imtiaz Ali || Ayesha Takia, Abhay Deol, Ayesha Jhulka || Romance
|-
|Stop (2004 film)|Stop!|| Ajai Sinha || Rocky Bhatia, Dia Mirza, Gauri Karnik, Ishita Sharma || Romance Drama
|-
|Swades || Ashutosh Gowarikar || Shahrukh Khan, Gayatri Joshi, Kishori Ballal || Drama
|- Abbas Mustan || Vatsal Seth, Ayesha Takia, Ajay Devgan || Fantasy Thriller
|-
|Thoda Tum Badlo Thoda Hum|| Esmayeel Shroff || Arya Babbar, Shriya Saran, Ashok Saraf || Romance
|- Tumsa Nahin Dekha|| Anurag Basu || Emraan Hashmi, Diya Mirza || Romance
|-
|Uff Kya Jaadoo Mohabbat Hai|| Manoj J. Bhatia || Sammir Dattani, Pooja Kanwal, Sandhya Mridul, Akshay Anand || Romance
|- Vaastu Shastra|| Saurab Narang || J. D. Chakravarthy, Ahsaas Channa, Vicky Ahuja, Sushmita Sen || Horror
|-
|Veer-Zaara|| Yash Chopra || Shahrukh Khan, Preity Zinta, Rani Mukerji, Kiron Kher, Zohra Sehgal, Hema Malini, Amitabh Bachchan || Drama, Romance
|- Arbaaz Khan, Shamita Shetty, Gracy Singh || Thriller
|-
|Wheres The Party Yaar|| Benny Mathews || Diwakar Pathak, Sunil Malhotra || Comedy
|- Shahbaz Khan || Thriller
|-
|Yeh Lamhe Judaai Ke|| Birendra Nath Tiwary || Shahrukh Khan, Raveena Tandon || Drama
|- Yehi Hai Zindagi|| Ajay Phansekar || Gracy Singh, Sahil Khan, Parvin Dabbas || Drama
|-
|Yuva|| Mani Ratnam || Ajay Devgan, Abhishek Bachchan, Vivek Oberoi, Kareena Kapoor, Rani Mukerji, Esha Deol, Om Puri || Drama, Action
|-
|Zameer (2004 film)|Zameer|| Kamal || Ajay Devgan, Mahima Chaudhry, Amisha Patel || Action Drama
|}

==References==
 

==External links==


 
 
 

 
 
 
 
 