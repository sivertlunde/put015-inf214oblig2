Along Came Love
{{Infobox film
| name           = Along Came Love
| image          = Along Came Love poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Bert Lytell
| producer       = Richard A. Rowland
| screenplay     = Arthur Caesar
| starring       = Irene Hervey Charles Starrett Doris Kenyon H. B. Warner Irene Franklin Bernadene Hayes
| music          = Gregory Stone
| cinematography = Ira H. Morgan
| editing        = J. Edwin Robbins
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 72 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}

Along Came Love is a 1936 American comedy film directed by Bert Lytell and written by Arthur Caesar. The film stars Irene Hervey, Charles Starrett, Doris Kenyon, H. B. Warner, Irene Franklin and Bernadene Hayes. The film was released on November 6, 1936, by Paramount Pictures.  

==Plot==
 

== Cast ==
*Irene Hervey as Emmy Grant
*Charles Starrett as John Patrick ORyan
*Doris Kenyon as Mrs. Gould
*H. B. Warner as Dr. Martin
*Irene Franklin as Mrs. Minnie Goldie Grant
*Bernadene Hayes as Sarah Jewett
*Ferdinand Gottschalk as Mr. Vincent
*Charles Judels as Joe Jacobs
*Frank Reicher as Planetarium Lecturer
*Mathilde Comont as Customer
*Baby Edward as Baby Edward

== References ==
 

== External links ==
*  

 
 
 
 
 
 

 