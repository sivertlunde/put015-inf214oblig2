Sleeping Bride
 
{{Infobox film
| name = Sleeping Bride
| image =
| image_size =
| caption =
| director = Hideo Nakata
| producer =
| writer = Chiaki J. Konaka Osamu Tezuka (comic)
| narrator =
| starring = Risa Goto Yuki Kohara
| music =
| cinematography =
| editing =
| distributor =
| released =  
| runtime = 100 minutes
| country = Japan
| language = Japanese
| budget =
}} comic by Osamu Tezuka. It is probably best known to Western audiences for its inclusion in a Tartan Asia Extreme DVD release of the Ring (film)|Ring trilogy, which included it as a fourth Nakata feature unrelated to the Ring series.

==Plot==
On March 13, 1954, a passenger plane crashes in Japan, killing all but one passenger: a woman who is nine months pregnant. Ultimately she dies in a hospital, but her unborn daughter is saved. However, for a reason inexplicable by the science of the day, the daughter (named Yumi) does not wake up and is essentially in a comatose state. Her wealthy father has her placed in a hospital where she will be cared for indefinitely (and also offers a reward to anyone who can wake her) while he vanishes in search of a cure to her condition.

Seven years later (March 25, 1961) Yuichi (a boy who is roughly seven himself) is admitted to the same hospital (with asthma) and discovers Yumi. After being told of her condition and then reading the story of Sleeping Beauty, he resolves to awaken Yumi by kissing her. Saying, "Wake up, Im the prince," he kisses her on the lips. This has no effect, but Yuichi is undeterred, and makes a routine of this, even after he leaves the hospital, repeating the same line and kissing her. Although a nurse finds this "cute" and lets him continue, when Doctor Hikawa discovers his actions, he ejects Yuichi from the hospital, infuriated that Yuichi believes that he can awaken the girl, whom he himself could not awaken with all the powers of medical science.

For the next ten years Yuichi does not visit the hospital at all, until February 14, 1972 when he sees a new report on Yumi who, after 17 years, has still never awoken. Overcome by the memories of his childhood, Yuichi returns to the hospital, and although initially thwarted by a new (apparently American) nurse, he eventually resumes his ten-year-old routine, with similarly little results, although more genuine emotion. Doctor Hikawa &ndash; now a Director of the Hospital &ndash; gradually becomes aware of his actions.

After Yuichi accidentally kisses a girl at school as part of an elaborate setup on the part of his friends, he rushes to the hospital (it would appear he considered it an act of infidelity) despite the rain and late hour, and passionately kisses Yumi. He does not use the "Wake up, Im the prince" line, but instead begs her (apparently in his thoughts), stating, "Im no prince, Im Yuichi Nasagawa, but please wake up, I want to talk to you." Though it would appear to have no effect at first, as Yuichi is leaving he notices some movement, and then he realises that Yumi has indeed awoken.

Promptly, the silent Yumi is besieged by a team of doctors and nurses who start to run series of tests, essentially ignoring and shutting out Yuichi. Yuichi, seeing that Yumi has been revived only to become some sort of human guinea pig, abducts her. Although Yumi has the mentality of a baby (even lacking the ability to walk or talk) they have a mutually enjoyable time together, until Yumi is reminded* of the crash-site in which her mother died, and is returned to the hospital by orderlies.

Yumi is seen in bed, at night, with her eyes open; Yumi does not sleep at all any more.

The next day (the "second day" that Yumi is awake), Yumi has advanced to the mentality of a young child, able to speak and read, and also an amazing talent for photo-realistic art, as she draws the image of her father. Yuichi returns after school and is immediately recognised by Yumi. They (this time by mutual consent, and accompanied by orderlies) walk outside in the grounds of the hospital. Upon sighting a bus (which Yumi seems to want to ride) Yuichi decides to give the orderlies (who have the air of cartoon comic-relief thugs to them) the slip, and does so successfully. They have an enjoyable "date" together (although Yumi is still only capable of conversing in simple terms).

By the third day Yumi has been discovered by the media and is being paraded all over for the press. Yuichi, Yuichis nurse (who retired), and Yumis father all witness the report on TV. Yuichi, once more, decides of his own accord that this is not "right" (although unlike when she was being tested, Yumi can clearly be seen to be distressed by many of the questions about her own genesis) and abducts Yumi once more. By this time Yumi has acquired the vocal and cognitive abilities approximately the same as other girls her age.

Yumi and Yuichi have their first conversation as intellectual equals. Yumi asks several philosophical questions about life and death. She declares that she only has two more days left before she falls asleep again (she gives no explanation as to how she acquired this knowledge, but she simply has it). Overall, she only has five days to be awake. She goes on to inquire about a strange emotion she has when she thinks of someone. After hearing the description, Yuichi explains the emotion is probably love. Yumi declares that, therefore, she is in love with Doctor Hikawa, shattering Yuichi.

Yuichi comes to the conclusion that someone told Yumi that she could only be awake for five days while she was asleep, and as such she believes it, but it is not true. He urges Hikawa to marry Yumi, as Yumi apparently loves him, and Yuichis primary concern is Yumis happiness. Hikawa does not believe in the five day limit either, or that Yumi really loves him. Yumi hears their conversation through the door and is herself, heart-broken. She attempts to commit suicide by jumping off a cliff, but she is stopped by Yuichi.

On the fourth day, Yuichis old nurse returns to the hospital to see Yumi, who is now in a state of depression. After hearing the story from Yuichi, she reveals that Hikawa had been doing something sexual to her at night, and that she left the hospital upon discovering this. Yuichi is infuriated, he confronts Hikawa and punches him, knocking him to the floor. Hikawa does not get up, but simply talks, his head lying down, not facing anything, of how Yumi was abandoned by her parents, and God. She was left only with him, but he was unable to help her. He explains that it was because he was unwilling to admit his impotency that he did what he did.

Yuichi returns to Yumi. Yumi explains that she heard someone, for a long time, praying for her to wake up, and then felt something that made her feel very warm. She thought this was Doctor Hikawa, and as such, that she loved him. Yuichi kisses her, and she realises the kiss was what made her warm, and by extension, it was Yuichi who prayed to her, and he whom she loved all along. They continue to kiss, and presumably make love. The same night they announce to Yuichis mother that they are getting married.

On the fifth and final day of the film, Yuichi and Yumi are wed. Although Yumis father watches and is happy to see that his daughter has found happiness, he does not reveal himself, but simply leaves once more, content. Hikawa arrives after the wedding and speaks alone to Yuichi about the fifth day limit. While he maintains that it is simply autosuggestion, he reminds Yuichi that it is real for Yumi. When Yuichi asks what to do, all Hikawa can suggest is prayer.

That night as Yumi and Yuichi depart on their honeymoon on a train, Yumi states, "I will be with Yuichi forever," and that she remembers everything about him, even in sleep, going on to list all the things they did together. Yumis speech is rather bittersweet. Despite the fact she appears genuinely happy at moments, she is also on the verge of tears. Yuichi has a similar mix of anxiety and sorrow. Yumi inquires if Yuichi believes in God. Yuichi replies, that if God is the one who only allows her to be awake for five days, then he does not believe in him. Yumi says she will tell God that the world is "very big," paraphrasing something Yuichi told her before. Finally, she states she will thank God for meeting Yuichi as well, and then she drifts off to sleep. Yuichi tries to wake her, eventually even resorting to the sleeping-beauty kiss. Yumi opens her eyes briefly and says, "I… am… happy," before falling asleep once more, forever.

Twenty seven years later (August 7, 1999), an aged Yuichi, now a scientist, returns home from work and talks to his wife. Yumi, herself aged, but not dramatically (her hair has greyed, but presumably her face has not wrinkled since she has not been moving) and still sleeps. As he is showing her a picture of a friends son, he notices she has stopped breathing – Yumi is dead.

As Yuichi watches the video of Yumi talking to the press (before he, in his youth, liberates her – i.e. the third day) he explains by monologue that after her death he had an autopsy performed on Yumi. When her brain was removed, it was completely transparent, and beautiful. He believes that although she was only awake for five days, she was happier than anyone else.

The film ends, replaying the earlier footage of Yumi and Yuichi running from the press. Yumi happily calls, "Yuichi!" and he retorts, "The world is very big, Yumi."

==External links==
* 

 

 
 
 
 
 
 
 
 