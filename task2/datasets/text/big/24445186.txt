Did You Hear About the Morgans?
{{Infobox film name          = Did You Hear About the Morgans? image         = did_you_hear_about_the_morgans_ver2.jpg caption       = Theatrical release poster director  Marc Lawrence producer      = Martin Shafer Liz Glotzer writer        = Marc Lawrence starring  Michael Kelly music  Theodore Shapiro
|cinematography= Florian Ballhaus editing       = Susan E. Morse studio        = Relativity Media Castle Rock Entertainment Banter Films distributor   = Columbia Pictures released      =   runtime       = 103 minutes    country       = United States language      = English budget        = $58 million    gross         = $85,280,250 
}} Marc Lawrence. separated New New York witness protection, given new identities, and relocated to a small Wyoming town (the fictional Ray, Wyoming, 45 minutes out of Cody, Wyoming|Cody). Supporting roles are played by Sam Elliott, Academy Award winner Mary Steenburgen, Elisabeth Moss, and Wilford Brimley. It is the second collaboration of Grant and Parker, following the 1996 film Extreme Measures.

The film premiered in New York on December 14, 2009 and in London the following day. It was released to the United States on December 18 and to most of Europe in January 2010. It received mostly negative reviews from critics. Compiled ratings from the review aggregator website Rotten Tomatoes gave the film an average rating of 3.5 out of 10. The film grossed $6,616,571 in its opening weekend and earned $85 million worldwide Did You Hear About the Morgans? was the 70th most successful film worldwide for 2009.

==Plot== Michael Kelly) and must enter the Witness Protection Program.

Paul and Meryl are relocated to the small town of Ray, Wyoming and placed temporarily under the protection of husband and wife/sheriff and deputy, Clay and Emma Wheeler (Sam Elliot and Mary Steenburgen). For their own safety, they are permitted no outside contact via telephone or e-mail.  They have trouble adjusting to small-town life, but after an unfortunate encounter with a bear and bumbling attempts at shooting rifles, chopping wood, and horseback riding, they eventually adjust and begin assisting the local citizens professionally.  Meanwhile, neither of their assistants back in NY know their whereabouts. Vincent plants a bug at Meryls office and in her assistant Jackies purse, hoping to gain information, which is eventually successful.   

Jackie attempts to call Meryl but Pauls assistant Adam stops her by kissing her, which she responds to by tasering him. Paul and Meryl go on a "date" in town and begin to reconcile, but then Paul is alienated when he learns that Meryl had a one-night stand with one of their acquaintances during their separation. The next day, with Vincent in town unbeknownst to them, the Morgans anticipate leaving Ray for a permanent hiding place. The Wheelers invite them to a rodeo, but the Morgans are quarreling, so they decline. Leaving the Morgans without any form of security, the Wheelers leave for the rodeo. Vincent tries to attack the house but is accosted by a bear, which gives the Morgans sufficient warning to allow them to escape. They flee on horseback to the rodeo to seek assistance. Vincent follows them to the rodeo, they spot him and hide in a bull suit. However, they end up in the ring with a bull, which then charges them, injuring Meryl. Meryl, unable to walk, stays hidden from Vincent while Paul confronts him with a canister of bear repellent spray. Paul accidentally sprays himself in the face but is rescued by the Wheelers and his new friends from the town.

Six months later, Paul and Meryl have repaired their marriage. They have an adopted baby girl from China, named Rae in honor of the Wyoming town, and Meryl is pregnant with their biological child.  Jackie and Adam are dating.

==Cast==
* Hugh Grant as Paul Morgan
* Sarah Jessica Parker as Meryl Morgan
* Sam Elliott as Clay Wheeler
* Jesse Liebman as Adam Feller
* Elisabeth Moss as Jackie Drake Michael Kelly as Vincent
* Kim Shaw as Nurse Kelly
* Christopher Atwood as Rodeo
* Mary Steenburgen as Emma Wheeler
* Wilford Brimley as Earl Granger
* Gracie Bea Lawrence as Lucy Granger
* David Call as Doc D. Simmons

==Production==
Filming for Did You Hear About the Morgans took place over 25 days in May and June in 2009. The film was shot in New York City, Santa Fe, New Mexico, and Roy, New Mexico.  , Retrieved 2010-01-21 

==Reception== average score Worst Actress for her performance, but lost to Sandra Bullock for All About Steve.

===Box office===
In its first opening weekend, the film opened at #4 in the United States box office raking in $6,616,571. With a budget of $58 million, Did You Hear About the Morgans? grossed only $29.5 million domestically at the box office. It did a little better overseas, earning an additional $56 million, for a total of $85 million worldwide.

==Home media==
Did You Hear About the Morgans? was released on DVD and Blu-ray Disc on March 16, 2010. 

The DVD features:
*Audio commentary with director Lawrence, and stars Grant and Parker
*5 featurettes
*Deleted scenes
*Outtakes

==References==
 

==External links==
* 
* 
* 
* 
* 
*  at Metacritic

 
 
 
 
 
 
 
 
 
 
 