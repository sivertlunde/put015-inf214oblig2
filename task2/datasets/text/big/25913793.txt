Frankenstein (1992 film)
{{Infobox Film  | name      = Frankenstein image        = Frankenstein-tv-1992.jpg based on       =   director      = David Wickes starring      = Patrick Bergin Randy Quaid John Mills Lambert Wilson Fiona Gillies music  John Cameron language    = English
}}

Frankenstein is a television film first aired in 1992, based on Mary Shelleys novel Frankenstein. It was produced by Turner Pictures and directed by David Wickes.
 John Cameron. 

==Plot==
Starting at the North Pole, a sea captain and his explorer crew encounter Dr. Frankenstein and his creature trying to kill each other. The doctor is saved. As he warns the captain of danger, he tells how he made his creature in the Switzerland of 1818 by way of chemical and biological construction.

==Cast==
*Patrick Bergin as Dr. Frankenstein
*Randy Quaid as Frankensteins monster
*John Mills as De Lacey
*Lambert Wilson as Dr. Clerval Elizabeth
*Jacinta Mulcahy as Justine
*Timothy Stark as William
*Roger Bizley as the Captain

==External links==
*   on IMDB

==References==
 

 

 
 


 