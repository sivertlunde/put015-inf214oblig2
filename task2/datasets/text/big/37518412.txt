Mea Maxima Culpa: Silence in the House of God
{{Infobox film
| name           = Mea Maxima Culpa: Silence in the House of God
| image          = Mea Maxima Culpa - Silence in the House of God poster.jpg
| caption        = 
| alt            = A statue crying tears
| director       = Alex Gibney
| producer       = Alex Gibney Alexandra Johnes Kristen Vaurio Jedd Wider Todd Wider
| writer         = Alex Gibney
| narrator       = Alex Gibney Ivor Guest  Robert Logan
| cinematography = Lisa Rinzler
| editing        = Sloane Klevin
| studio         = Jigsaw Productions Wider Film Projects Below The Radar Entertainment
| distributor    = HBO Films
| released       =  
| runtime        = 107 minutes
| country        = United States
| language       = English
}}
 2012 documentary clerical sex abuse in the United States by four deaf men. It features the voices of actors Jamey Sheridan, Chris Cooper, Ethan Hawke and John Slattery, who provide the vocal translation of the deaf interviewees.

The title is derived from the Latin phrase "mea maxima culpa". It is taken from the Confiteor that is part of the Roman Catholic Mass. It translates into English as "My most grievous fault (or guilt)". 

==Synopsis==
   Lawrence Murphy case. Through their case the film follows a cover-up that winds its way from the row houses of Milwaukee, Wisconsin, through Irelands churches, all the way to the highest office of the Vatican.

==Release==
The film premiered on September 9, 2012 at the 2012 Toronto International Film Festival.  It later opened in limited release on November 16, 2012,  and premiered worldwide on HBO on February 4, 2013. 

==Reaction==

===Critical response===
The film received positive feedback from critics. Review aggregation website Rotten Tomatoes gives the film a score of 98% based on reviews from 42 critics, with a rating average of 7.8 out of 10.  Metacritic, which assigns a weighted average score out of 100 to reviews from mainstream critics, reports the film with a score of 73 based on 16 reviews. 

Mark Jenkins of NPR called the film "Alex Gibneys most powerful film since the Oscar-winning 2007 Taxi to the Dark Side."  A. O. Scott of The New York Times particularly praised the way that the interviews of the victims were shot writing, "Mr. Gibney films them, against dark backgrounds with soft, indirect light, emphasizes the expressivity of their faces and hands, and will remind hearing viewers of the richness and eloquence of American Sign Language."  Roger Ebert of the Chicago Sun-Times felt the film on a personal level writing, "To someone who was raised and educated in the Catholic school system, as I was, a film like this inspires shock and outrage." He went on to write that the film "is calm and steady, founded largely on the testimony of Murphys victims." 

===Awards===
In 2013, Mea Maxima Culpa was nominated for 6 Primetime Emmy Awards, and won in 3 categories, including Exceptional Merit In Documentary Filmmaking.  It won a Peabody Award in 2013 "for providing a harrowing story of clerical sexual abuse, empowering long-silenced victims and unveiling clandestine Church practices around accusations." 

==See also==
* Catholic sex abuse cases Deliver Us from Evil (2006), a documentary about a sex abuse case in Northern California
* Twist of Faith (2005), another HBO documentary film about abuse in the Catholic Church.

==References== 30em
 
{{cite web
| url = http://www.emmys.com/shows/mea-maxima-culpa-silence-house-god
| title = Emmys – Mea Maxima Culpa: Silence in the House of God
| accessdate = February 25, 2014
}}
 
}}

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 