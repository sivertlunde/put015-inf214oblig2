Vanishing Point (2012 film)
Vanishing Point is a 2012 National Film Board of Canada documentary film directed by Alberta filmmakers and environmental scientists Stephen A. Smith and Julia Szucs, chronicling life in the Arctic for two remote communities linked by a migration from Baffin Island to Greenland. The film is narrated in Inuktitut by Navarana Kavigak Sørensen, a polyglot Inughuit linguist who is the great-great-great-niece of a Baffin Island shaman who had led the migration in 1860.   

Production of the film took Smith and Szucs four years. The filmmakers accompanied Navarana on three hunting trips across the remote north for Vanishing Point, which contrasts traditional life on the tundra with life in modern-day communities. The film also shows the impact of Arctic sea ice decline on families who still travel the north by dog teams. Vanishing Point was co-written by Alberta novelist Marina Endicott and produced by Szucs along with David Christensen for the NFB. The film premiered at the Calgary International Film Festival and was nominated for best feature documentary at the 2nd Canadian Screen Awards.    

==References==
 

==External links==
* 
*  at the National Film Board of Canada
*  at Meltwater Media

 
 
 
 
 
 
 
 
 
 


 
 