Ponthan Mada
{{Infobox film
| name           = Ponthan Mada
| image          =
| image_size     =
| caption        =
| director       = T. V. Chandran
| producer       = Ravindranath
| story          = C. V. Sreeraman
| screenplay     = T. V. Chandran
| narrator       = Janardhanan Maniyanpilla Raju Johnson
| Venu
| editing        = Venugopal
| distributor    = Mac Release
| released       = 10 March 1994
| runtime        =
| country        = India
| language       = Malayalam
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}
 1994 Malayalam Indian feature written and directed by T. V. Chandran. The film stars Mammootty and Naseeruddin Shah. 

==Plot== British India, the film is about the extraordinary, uncanny and touching bonding of the so-called low-caste Ponthan Mada (Mammootty) with his colonial landlord Sheema Thampuran (Naseeruddin Shah), who was expelled to British India from England during his youth for supporting the Irish Republican Army. Crossing the class boundaries, the two communicate through Thampurans window with Mada hanging from a palm tree.

==Cast==
* Mammootty
* Naseeruddin Shah
*Laboni Sarkar Janardhanan
* Maniyanpilla Raju
* Sreejaya
* Zeenath
* Mavelikkara Ponnamma
*Premachandran

==Awards==
The film has won the following awards since its release: National Film Awards (India)  Silver Lotus Award - Best Actor - Mammootty  Johnson

==Trivia==
This is the only Malayalam language|Malayalam-language film of Naseeruddin Shah. 

==External links==
* 

 

 
 
 
 
 
 
 

 