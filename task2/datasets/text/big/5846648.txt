Tiresia
{{Infobox film
  | name = Tiresia
  | director = Bertrand Bonello
  | writer = Bertrand Bonello Luca Fazzi
  | starring = Laurent Lucas Clara Choveaux Thiago Telès Célia Catalifo
  | editing = Fabrice Rouaud
  | cinematography = Josée Deshaies
  | music = Albin De La Simone	 Laurie Markovitch
  | country = France
  | language = French
  | released =  
  | runtime = 115 minutes
 }}
 2003 cinema French film directed by Bertrand Bonello and written by Bonello and Luca Fazzi.

Based on the legend of Tiresias, it tells of a transsexual who is kidnapped by a man and left to die in the woods. She is then saved by a family and receives the gift of telling the future. The film stars Laurent Lucas, Clara Choveaux, Thiago Telès and Célia Catalifo.

Tiresia was nominated for the Palme dOr at the 2003 Cannes Film Festival.   

== Bibliography ==
* Bernard Stiegler, "Tirésias et la guerre du temps: Autour dun film de Bertrand Bonello," De la misère symbolique: Tome 1. Lépoque hyperindustrielle (Paris: Galilée, 2004): 163–85.

==References==
 

==External links==
*  
*   July 14, 2005

 

 
 
 
 
 
 
 
 


 