Königswalzer (1955 film)
{{Infobox film
| name           = Königswalzer
| image          =Koenigswalzer.jpg
| image_size     = 
| caption        = 
| director       = Victor Tourjansky
| producer       = Klaus Stapenhorst Walter Forster
| narrator       =
| starring       = 
| music          = Carl Loubé Franz Koch
| editing        =
| distributor    = 
| released       =  
| runtime        = 85–93 minutes
| country        = West Germany 
| language       = German
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 West German Michael Cramer. 1935 film of the same name.

==Cast==
*Marianne Koch	... 	Therese Michael Cramer	... 	Graf Ferdinand von Tettenbach
*Joe Stöckel	... 	Ludwig Tomasoni
*Linda Geiser	... 	Prinzessin Elisabeth Sissi
*Hans Fitz	... 	König Max II
*Hans Leibelt	... 	Minister Dönniges
*Sabine Hahn	... 	Anni Tomasoni Ellen Frank	... 	Herzogin
*Harry Hardt	... 	Leutnant Hakenstaller
*Harry Halm	... 	Zeremoniemeister
*Hans Danninger	... 	Ober Alois
*Walter Sedlmayr	... 	Konditor Franz
*Willy Rösner	... 	Revolutionär Pfandl
*Willem Holsboer	... 	Revolutionär Brandmeyer

== References ==
 

== External links ==
* 
* 

 
 
 
 
 
 
 

 