The Jungle Book 2
 
 
{{Infobox film
| name           = The Jungle Book 2
| image          = Junglebook2_movieposter.jpg
| image_size     = 220px
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| director       = Steve Trenbirth
| producer       = Christopher Chase Mary Thorne
| writer         = Karl Geurs
| based on       =  
| starring       = Haley Joel Osment John Goodman Mae Whitman Bob Joles Tony Jay Phil Collins John Rhys-Davies Jim Cummings
| music          = Patrick Griffin New songs:   Richard M. Sherman Robert B. Sherman
| editing        = Christopher K. Gee Peter Lonsdale
| studio         = Walt Disney Pictures DisneyToon Studios Buena Vista Pictures
| released       =  
| runtime        = 72 minutes
| country        = United States Australia
| language       = English
| budget         = $20 million   
| gross          = $135,703,599 
}} animated musical The Jungle Book, and stars Haley Joel Osment as the voice of Mowgli and John Goodman as the voice of Baloo.
 Peter Pan sequel, Return to Never Land. It is the third animated Disney sequel to have a theatrical release rather than going direct-to-video after The Rescuers Down Under in 1990 and Return to Neverland in 2002. The film is a continuation of The Jungle Book by Rudyard Kipling and is not based on The Second Jungle Book. However, they do have several characters in common. When released, it was criticized mainly for the quality of its animation and the similarity of its plotline to the original film.

==Plot==
Mowgli is living in the Man Village with the girl who lured him in, Shanti, his adopted brother Ranjan, and their parents. However, Mowgli longs to return to the fun of the jungle, and after nearly leading the other children of the village into the jungle, is punished by his adopted father for trying to lead them into danger. Meanwhile, in the jungle, Shere Khan has returned to Baloo and Bagheeras part of the jungle to exact revenge on Mowgli. Baloo sneaks into the Man Village and gets Mowgli to come with him to live in the jungle; however, unbeknownst to them, Shere Khan also infiltrated the village, only to be chased off by the village people. In the ensuing chaos of the tigers attack, Shanti and Ranjan go into the jungle to retrieve Mowgli, believing that Baloo is a hostile animal and kidnapped the boy.

Bagheera hears of Mowglis departure from the village when the humans search the jungle for him, and immediately suspects Baloo. Mowgli instructs Baloo to scare off Shanti should she appear, and bemoans the boring life he had in the Man Village. Baloo and Mowgli journey to King Louies old temple (King Louie is mentioned to have abandoned it), however when the animals of the jungle mock Shanti and other aspects of Mowglis life in the Man Village, the boy leaves, offended. He runs into Shanti and Ranjan, but Baloo scares Shanti as Mowgli wanted him to. When the truth comes out that Mowgli ordered Baloo to scare her, Shanti and Ranjan run away and leave Mowgli.

Baloo recognizes that Mowgli misses his old life, but when Mowgli tries to make amends with his human friends, they are attacked by Shere Khan. The tiger chases Mowgli and Shanti to a temple built above a lake of lava, and Baloo leaves Ranjan with Bagheera while he goes to protect Mowgli. After confusing Shere Khan by banging several different gongs, Shantis presence is revealed to Shere Khan. Baloo fights Shere Khan just as Mowgli is about to give himself up to save Shanti, but the tiger chases the two children to a statue across a pit of lava. Shere Khan is trapped within the statues mouth, and it plummets onto a large stone  that resides in the lava below. With his nemesis defeated, Mowgli returns to the Man Village with Shanti and Ranjan, but they still visit Baloo and Bagheera in the jungle regularly.

==Voice cast==
* Haley Joel Osment as Mowgli
* John Goodman as Baloo
* Mae Whitman as Shanti
* Bob Joles as Bagheera
* Tony Jay as Shere Khan
* Phil Collins as Lucky
* John Rhys-Davies as Ranjans father
* Jim Cummings as Kaa / Colonel Hathi / M.C. Monkey
* Jimmy Bennett as Hathi, Jr.
* Connor Funk as Ranjan
 Jeff Bennett, Baron Davis, Jess Harnell, Bobby Edner, Devika Parikh, Veena Bidasha, Brian Cummings, and an uncredited J. Grant Albrecht.

;Hidden appearances
* During one attempt at the classic song The Bear Necessities from the first film, two prickly pears land on and stick to Kaas head, making him look like Mickey Mouse. This is an example of a Hidden Mickey.
* During "W-I-L-D", Timon and Pumbaa can briefly be seen dancing until Baloo bounces them off with his backside.
* Osment and Whitman would later co-star again in Kingdom Hearts II, voicing Sora and Yuffie, respectively.

==Songs== Richard M. and Robert B. Sherman with new songs by Lorraine Feather, Paul Grabowsky, and Joel McNeely.
 I Wanna Be Like You" - Smash Mouth
# "Jungle Rhythm" - Mowgli, Shanti, Ranjan
# "The Bare Necessities" - Baloo
# "Colonel Hathis March"
# "The Bare Necessities" - Baloo, Mowgli
# "W-I-L-D" - Baloo
# "Jungle Rhythm (Reprise)" - Mowgli
# "The Bear Necessities (Reprise)" - Baloo, Mowgli, Shanti
# "Right Where I Belong" - Windy Wagner

==Production==
In the 1990s, screenwriting duo Bob Hilgenberg and Rob Muir submitted a Jungle Book 2 screenplay in which Baloo ventured to save his romantic interest from a poacher. Disney ultimately went in a different direction for the sequel. 
 a legal Jungle Book could not be included in this film. However, he makes a non-physical appearance as a shadow puppet in the beginning of the film and is briefly mentioned in the middle of the film.
The decision was made to keep Shere Khan in shadow during the beginning of the film to "reflect his wounded pride".

==Release==

===Critical reception===
The Jungle Book 2 received generally negative reviews from critics. Based on 88 reviews, the film a "rotten" approval rating of 19% on Rotten Tomatoes, with an average score of 4.4/10 and the general consensus "This inferior rehash of The Jungle Book should have gone straight to video."  Another review aggregator, Metacritic, which assigns a normalized rating out of 100 top reviews from mainstream critics, the film received an average score of 38%, based on 24 reviews. 

===Box office===
The film was released on February 14, 2003 and opened at #4 in its 4-day opening weekend with $14,109,797.  At the end of its run, the film grossed $47,901,582 in the United States and $87,802,017 in foreign countries totaling $135,703,599 worldwide. It could be considered a box office success, based on its $20 million budget. 

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 