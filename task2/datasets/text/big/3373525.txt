Cinderfella
{{Infobox film
| name           = Cinderfella
| image          = Cinderfella.jpg
| image_size     =
| caption        =
| director       = Frank Tashlin
| producer       = Jerry Lewis
| writer         = Frank Tashlin
| narrator       =
| starring       = Jerry Lewis Ed Wynn Judith Anderson Anna Maria Alberghetti
| music          = Walter Scharf
| cinematography = Haskell B. Boggs
| editing        = Arthur P. Schmidt
| distributor    = Paramount Pictures
| released       =  
| runtime        = 91 min.
| country        = United States
| language       = English
| budget         = gross = 936,799 admissions (France) 
}}
Cinderfella  is a comedy film adaptation of the classic Cinderella story, with most characters changed in gender from female to male and starring Jerry Lewis as Fella.  It was released December 16, 1960 by Paramount Pictures. 
 
==Plot==
 Robert Hutton). His stepfamily takes over the family mansion, while Fella is reduced to living in an unfinished room at the end of a long hallway. He has in essence become their butler, catering to their every whim.

Fella dreams nightly that his father is trying to relay a message to him about where he has hidden his fortune, but he always awakens before he learns the hiding place. His stepfamily knows of this secret fortune and some go to great lengths to discover its whereabouts, while others pretend to befriend him in order to wrangle Fellas fortune away once it is found.

Princess Charming of the Grand Duchy of Morovia (Anna Maria Alberghetti) is in town, so the stepmother decides to throw her a lavish ball in order to get her to marry one of the sons. Fella is not allowed to go to the ball, but his fairy godfather (Ed Wynn) says he will not remain a "people" much longer, but will blossom into a "person."

Before the ball, Fella is turned into a handsome prince. Count Basies orchestra is playing at the ball when Fella makes his grand entrance. The young man quickly gains the attention of the Princess and they dance. The night is cut short when midnight strikes and Fella flees, losing his shoe along the way.

Back home, one of Fellas stepbrothers realizes that Fella is the supposed "prince." They wind up in a struggle under a tree, in the process discovering that this is where Fellas fathers fortune is hidden. Fella gives the money to his stepfamily, saying he never needed money to be happy, he only wanted a family. Shamed, his stepmother orders her sons to return the money to Fella.

The Princess arrives with Fellas lost shoe, but Fella explains that they could never be together because she is a "person" and he is a "people." She tells him that, underneath the fancy clothes, she is a "people" too.

==Production==
Cinderfella was filmed from October 19 through December 15, 1959.

While rehearsing for the scene in which he makes his entrance to the ball, Lewis realized that the movement of his pants was distracting. On the day of filming, he asked the wardrobe staff to attach elastic bands to the hems of his pant legs that would go under his shoes, keeping the pants straight. In the DVD commentary to the film, Lewis called this "an old dancers trick." The scene was shot with one take of Jerry Lewis going down the stairs and one take going up. He ran up the stairs in less than nine seconds and collapsed at the top. He was taken to the hospital and spent four days in an oxygen tent with his second cardiac event. This delayed filming for two weeks.

The exterior shots of the mansion are the well-known Kirkeby mansion in Bel Air; in the years following Cinderfella, this mansion will become famous as the television mansion of the Beverly Hillbillies.

==Release== Fontainebleau Hotel in Miami Beach. That movie was released on July 20, 1960.

In 1967, the film was re-released on a double bill with another Jerry Lewis film, The Errand Boy.

==Studio cast recording==
A studio cast album for the film, featuring Lewis with a different supporting cast, was released by DOT Records (DLP 38001). Most of the songs included on the album were not in the final release of the film. The album featured a condensed version of the story as well.

===Track listing===
# OVERTURE (Arranged and Conducted by Walter Scharf)
# LET ME BE A PEOPLE (Jerry Lewis)
# TICKA-DEE (Jerry Lewis)
# IM PART OF A FAMILY (Jerry Lewis)
# TURN IT ON (Jerry Lewis & Choir)
# WERE GOING TO THE BALL (Salli Terri, Bill Lee & Max Smith)
# SOMEBODY (Jerry Lewis)
# THE PRINCESS WALTZ (Jerry Lewis, Loulie Jean Norman & Choir)
# TURN IT ON (Jerry Lewis, Del Moore & Choir)

==Home media==
The film was released on DVD on October 12, 2004.

==References==

 

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 