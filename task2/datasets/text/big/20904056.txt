Chor Sipahee
{{Infobox film
| name           = Chor Sipahee
| image          = ChorSipaheefilm.png
| image_size     = 
| caption        = Promotional Poster
| director       = Prayag Rai
| producer       = 
| writer         =
| narrator       = 
| starring       =
| music          = Laxmikant-Pyarelal
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1977
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
 1977 Bollywood film directed by Prayag Rai.

==Cast==
*G. Asrani ...  Doctor 
*Manju Asrani  (as Manju) 
*Shabana Azmi ...  Priya 
*Parveen Babi ...  Bharti Khanna 
*Bhagwan ...  Rahim 
*Leena Das ...  Jamals wife 
*Krishan Dhawan ...  Priyas dad 
*Shashi Kapoor ...  Inspector Shankar Kumar / Bada Saheb 
*Kader Khan ...  Munshilal (BMC Worker) 
*Vinod Khanna ...  Raja Dada / Raja Khanna 
*Durga Khote ...  Mrs. Khanna (Rajas mom) 
*Mac Mohan ...  Bichoo 
*Moolchand   
*Narendra Nath ...  Inspector Apte 
*Ranjeet ...  Sheikh Jamal 
*Om Shivpuri ...  Commissioner of Police 

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Chor Sipahee Mein Hoti Nahin"
| Mohammed Rafi, Kishore Kumar
|-
| 2
| "Dekha Sahab O Sahab"
| Asha Bhosle
|-
| 3
| "Ek Taraf Hai Yeh Zamana" Bhupinder Singh, Mohammed Rafi, Asha Bhosle
|-
| 4
| "Mujhse Mera Naam Na Poochho"
| Kishore Kumar
|}

==External links==
*  

 
 
 
 
 