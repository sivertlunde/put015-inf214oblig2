No sos vos, soy yo
{{Infobox Film |
  name           = No sos vos, soy yo |
  image          =|
  writer         = Juan Taratuto Cecilia Dopazo |
  starring       = Diego Peretti Soledad Villamil Cecilia Dopazo |
  director       = Juan Taratuto |
  producer       = Hernán Musaluppi|
  music          = Federico Jusid |
  cinematography = Marcelo Iaccarino |
  editing        = César Custodio |
  distributor    = |
  released       = October 28, 2004 |
  runtime        = 105 minutes |
  country        = Argentina |
  language       = Spanish |
  budget         =  |
  }} Argentine film directed by Juan Taratuto, starring Diego Peretti, Soledad Villamil and Cecilia Dopazo.

==Plot summary==

Thirty-year-old Javier (Diego Peretti) is a surgeon and in his free time works as a disc jockey. He decides to marry and move to the United States with his girlfriend María (Soledad Villamil). They make all their plans; they wed, and then María is the first to move and make contacts in their new home, while Javier packs up in Argentina and prepares to start his new life in the States. While he is on the way to the airport, he receives a call from María telling him that she is confused and has been seeing someone else.

With no home, job, or girlfriend, Javier moves in with his parents and starts seeing a therapist, which doesnt work. To avoid the loneliness he buys a Great Dane puppy and tries to meet new girls. When he gets tired of having the dog, he meets Julia (Cecilia Dopazo). He finds an apartment and gets a job in a plastic surgery clinic. However, María calls, telling him that she is coming back to Argentina. She needs him and wants to be sure that they will be back together and that Javier will forgive her. Now he needs to make a decision, Julia also waits for an answer, and Javier isnt the same person he was before all this happened.

==External links==
*  

 
 
 
 


 
 