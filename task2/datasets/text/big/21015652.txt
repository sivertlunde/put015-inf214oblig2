Run Barbi Run
{{Infobox film
| name           = Run Barbi Run
| image          = 
| caption        = 
| director       = Tony Y. Reyes
| producer       = Tony Gloria
| screenplay     = Rosauro de la Cruz Tony Y. Reyes
| story          = Tony Gloria
| starring       = Joey de Leon Maricel Laxa Jaime Fábregas
| cinematography = Sergio Lobo
| editing        = Eduardo Jarlego
| studio         = Cinemax Films
| distributor    = Moviestar Productions
| released       = 1995
| runtime        = 
| country        = Philippines
| language       = Tagalog
| budget         = 
| gross          = 
}}
 Cinemax Studios (nee GMA Films) and Moviestar Productions topbilled by Joey de Leon in 1995 with the Eraserheads and Maricel Laxa.  The movie was directed by Tony Y. Reyes.

==Story==
Bartolome is a Shakeys Pizza|Shakeys pizza delivery boy who witnesses the massacre of the Black Scorpion Gang committed by a group of henchmen while delivering pizza at night.  As a result, he was fired from his job and had to decide whether to turn state witness or hide forever.  Ultimately, he decides to turn himself over to the authorities after his grandmother and mother appeared in his dream.  Unknown to him however, he is already on the death list of the henchmens boss Gardo who was not pleased with his revelations.  A conflict with the authorities over a supposed filmization of the massacre witnessed by Bartolome almost cost him his life after Gardo was able to find his whereabouts.  Thus, he had to run again and hide.  He later found himself in a bar that features gay impersonators and rock bands.  Seeing an opportunity to escape from being hounded by different personalities involved in the investigation of the crime he witnessed, he disguised himself as Barbi, the gay impersonator of Barbra Streisand.

As Barbi, he was able to enjoy the life as an impersonator/DJ in the bar and was able to make friends with the bars in-house band, the Eraserheads.  He even got to help save the band from music pirates who were copying the bands songs.  But when he crossed paths with a policewoman named Victoria "Toyang" Fernandez, he had to reveal his true identity as the star witness of the crime that he saw.  As a result, he joined forces with Toyang and the Eraserheads to finally pin down the crime boss Gardo who wants him dead.

==Cast==
* Joey de Leon as Bartolome del Rosario
* Maricel Laxa as Victoria "Toyang" Fernandez
* Roldan Aquino as Gardo (crime boss)
* Subas Herrero as Attorney Ramon Lazaro
* Nanette Inventor as Flor
* Richard Merck as Junior
* Noel Trinidad as Major Velarde
* Eraserheads as themselves
* Lou Veloso as Shakeys supervisor Mr. Gloria
* Rolando Tinio as Mr. Tengko
* Gary Lising as Alex
* Archie Adamos as Direk Carlito Carlos
* Inday Garutay as himself
* Allan K. as himself/a female impersonator
* Vangie Labalan as Flora
* Mely Tagasa as Decia
* Winnie Cordero as Corder
* Amy Coronel
* Prospero Luna
* Robert Talby as Frankie
* Danny Labra as Versosa
* Nonong De Andres as Chris
* Rene Hawkins as Magsanoc
* Ernie Forte as Abarrientos
* Jun Encarnacion as a female impersonator
* Giovanni Calvo as himself

==Original Sound Track==
* Run Barbi Run: Eraserheads

==References==

 

 
 
 
 