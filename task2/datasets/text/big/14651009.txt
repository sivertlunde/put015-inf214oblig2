The Point (film)
{{Infobox Film
 | name = The Point
 | image_size = 
 | caption = 
 | director = Joshua Dorsey
 | producer = 
 | writer = Owen Coughlan Joshua Dorsey Alyssa Kuzmarov Melissa Malkin
 | narrator = 
 | starring =  Julie Chauvin, Satchel Babineau, Sabrina Law
 | music = 
 | cinematography = Alain Julfayan
 | editing = Maxime Chalifoux
 | distributor = Silo Productions
 | released = 2006
 | runtime = 
 | country = Canada English
 | budget = $1 million
 | gross = 
 | preceded_by = 
 | followed_by = 
 | image = The Point VideoCover.jpeg
}}

The Point is a 2006 film co-produced by Silo Productions and the National Film Board of Canada, filmed on location in Montreal. The movie, whose name is taken from the common name for one of the citys poorest neighbourhoods, Pointe-Saint-Charles, tells the story of a group of teenagers and how their lives intersect over the course of a weekend, with the story developed by teens, in workshops.   

The Point was shot with a group of 40 teens from the neighbourhood, for a reported budget of $1 million. The film premiered at Festival du nouveau cinema, followed by screenings at the Rendez-vous du cinema quebecois and the Slamdance Film Festival.   

== See also ==
* Docufiction
* List of docufiction films

==References==
 

== External links ==
*    (Requires Adobe Flash)
*  

 
 
 
 
 
 
 
 
 
 
 
 


 