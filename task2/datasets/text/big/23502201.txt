Rocket Singh: Salesman of the Year
 
 
{{Infobox film
| name = Rocket Singh: Salesman of the Year
| image = Rocket Singh - Salesman of the Year.jpg
| caption = Theatrical poster
| director = Shimit Amin
| producer = Aditya Chopra
| writer = Jaideep Sahni
| starring = Ranbir Kapoor   Gauhar Khan   Shazahn Padamsee   Prem Chopra    
| music = Salim-Sulaiman
| cinematography = Vikash Nowlakha
| editing = Arindam Ghatak
| distributor = Yash Raj Films
| released =  
| runtime = 156 minutes
| country = India
| language = Hindi
| budget =   
| gross =   
}}
Rocket Singh: Salesman of the Year is an Indian film directed by Shimit Amin and produced by Aditya Chopra under the Yash Raj Films banner and released in 2009. Screenplay of the film is by Jaideep Sahni, who collaborated with Shimit Amin for Chak De! India. It stars Ranbir Kapoor, D. Santosh (Actor)|D. Santosh & Naveen Kaushik in the leads roles. Prem Chopra and Shazahn Padamsee daughter of Alyque Padamsee and Sharon Prabhakar, who makes her debut in the film, play supporting roles. The film was declared a below average grosser at the box office. 

== Plot== B Com graduate with approximately 39 percent marks becomes a salesman with a big corporate computer assembly and service company, AYS. His idealistic vision of the working world shatters quickly. Within a few days, a client asks for a Bribe|kick-back. Harpreet is against and files a complaint only to find out that that this is how AYS operates. The top salesmen at AYS acquire large client contracts through bribery. In such corrupt company culture, Harpreets honesty only brings him a demotion and humiliation. After making concessions to a client for the company, Harpreet realizes that sales success is dependent on the customer; so if the customer is satisfied then bribery will not be necessary to secure these contracts. No one agrees with him but Harpreet remains firm on his belief and forms his own company,Rocket Sales Corporation, from within AYS. Rocket Sales is being managed from the AYS offices where the Rocket partners were still employed. Unlike AYS, Rocket Sales Corporations overall strategic goal is customer service and customer satisfaction as opposed to just selling the product through bribes and providing zero customer service. Other disgruntled employees of AYS find their way to Rocket Sales – a place where even the guy who serves tea is an equal partner because he brings talent to the table.

The company soon becomes successful because of its dedication to excellent customer service. The MD of AYS, Sunil Puri, becomes angered by the small companys success. Puri moves quickly to attempt to contact the MD of Rocket Sales Corporation and in a phone conversation attempts to entice him to sell Rocket Sales Corporation to AYS(he does not know it is Harpreet). Harpreet not only rejects Puris offer but says that his company will buy AYS Computers. After multiple failed attempts in locating the Rocket Sales Corporation office, Puri decides to call the number on Rocket Sales Corporationss brochures  and the phone at the AYS reception desk begins to ring. After thoroughly insulting him and firing both him and his partners, the MD has Harpreet sign a contract handing over Rocket Sales to him for a final compensation of Re.1 . However, AYS is unable to maintain Rocket Sales commitment to customer satisfaction because of its cold and greedy personnel. The MD, realizing his downfall in purchasing Rocket Sales, visits Harpreet at his new job at Croma, an electronics store, and returns the contract to Harpreet in return for Re.1 . He also tells him never to become a businessman again, because he will fail again. However, this is not intended as an insult, but rather as a compliment; he implies that what made Harpreet so successful was his eschewing of normal business practices such as kick-backs, false advertisement, and low wages.

The ending scene is the new Rocket Sales office building where a prospective worker is going in for an interview. It shows the former employees, all partners of the business, and finally closes with Harpreet smiling genially at a desk, showing that eventually honesty and hard work is a sound business decision.

==Cast==
* Ranbir Kapoor as Harpreet Singh Bedi
* Gauhar Khan as Koena Sheikh
* Shazahn Padamsee as Sherena Khanna
* D. Santosh (Actor)|D. Santosh as Girish Reddy
* Naveen Kaushik as Nitin Rathore Manish Chaudhary as Sunil Puri
* Mukesh Bhatt as Chhotelal Mishra
* Mokshad Dodwani as Tanmay (Taxi)
* Amol Parashar as Sai
* Debi Dutta as Aparna
* Prem Chopra as P. S. Bedi

== Release == released on 11 December 2009 worldwide.

==Critical Reception==

Film critic, Rachel Saltz of New York Times called it "a smart, focused Bollywood movie" and commended Ranbir Kapoor for "turning in a skillfully understated performance"  while Anupama Chopra in her NDTV review gave it 3 out 5 stars. 

Nikhat Kazmi in The Times of India gave 3 stars out 5.  An The Economic Times review gave it a 4 stars out of 5 and called it, "one of the most rocking films of the year"  An Outlook (magazine)|Outlook review by Namarata Joshi gave it 3 out of 5 stars, saying that the film "Marks a continuum and a departure from the middle-class cinema of Hrishikesh Mukherjee, Basu Chatterjee and Sai Paranjape."  Rajeev Masand of CNN-IBN gave the movie 3.5 stars out of 5. 

Rocket Singh was featured in Avijit Ghoshs book, 40 Retakes: Bollywood Classics You May Have Missed

==Soundtrack==
The soundtrack is composed by Salim-Sulaiman, with lyrics by Jaideep Sahni. 

==Box office==
Rocket Singh: Salesman of the Year made Rs 102 million (Rs 102.5&nbsp;million) in India on its opening weekend.  The film was declared a below-average grosser at the box office.

===Awards===
2010: Filmfare Awards Best Actor (Critics Choice) – Ranbir Kapoor
2010: IIFA Awards
*Nominated: IIFA Best Villain Award – Manish Chowdhary
won best film award international film awards

== References ==
 

==External links==
*  
*  - Yash Raj Films 
*  
*   Bollywood Hungama

 

 
 
 
 
 
 
 