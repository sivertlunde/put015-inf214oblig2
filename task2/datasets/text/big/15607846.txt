The Onion Field (film)
{{Infobox film
| name           = The Onion Field
| image          = OnionFieldPoster.JPG
| caption        = Theatrical release poster
| director       = Harold Becker
| producer       = Walter Coblenz
| writer         = Joseph Wambaugh
| narrator       = John Savage James Woods Franklyn Seales Ted Danson Ronny Cox
| music          = Eumir Deodato
| cinematography = Charles Rosher, Jr.
| editing        = John W. Wheeler Avco Embassy Pictures (1979, original) MGM (2002, DVD)
| released       =  
| runtime        = 122 minutes
| country        = United States
| language       = English
| gross          = $9,890,597 (US)  
}}
 1979 Cinema American crime crime drama novel of John Savage, James Woods,  Franklyn Seales and Ted Danson in his film debut.

==Plot==
The film focuses on an actual 1963 event in which Los Angeles Police Department detectives Karl Hettinger and Ian Campbell were kidnapped by criminals Greg Powell and Jimmy Smith (aka "Jimmy Youngblood") in Hollywood and driven to an onion field near Bakersfield, California|Bakersfield, where Campbell was shot and killed and Hettinger managed to escape.

Hettingers eyewitness account leads to the arrest of the two men, who are tried and convicted of first-degree murder. While they languish on death row, Powell and Smith learn how to exploit the legal system and after a series of appeals their sentences are reduced to life imprisonment following a court decision abolishing executions in California. Meanwhile, Hettingers physical condition and emotional state slowly deteriorate as his failure to act more aggressively on the night of the incident is questioned by those in authority and his fellow officers. Wracked with guilt and remorse, he experiences nightmares, impotence, weight loss, kleptomania, and thoughts of suicide.

==Cast== John Savage as Karl Hettinger
*James Woods as Gregory Powell
*Franklyn Seales as Jimmy Smith
*Ted Danson as Ian Campbell
*Ronny Cox as Sgt. Pierce Brooks
*David Huffman as District Attorney Phil Halpin
*Christopher Lloyd as Jailhouse lawyer
*Dianne Hull as Helen Hettinger
*Priscilla Pointer as Chrissie Campbell
*K Callan as Mrs. Powell
*Sandy McPeak as Mr. Powell

==Production== Taft in California. A courtroom of the Superior Court of Los Angeles County was used for the trial scenes. The jury panel was taken to an onion field in Valencia to inspect it as a replica of the scene of the crime.
 The Choirboys, the script for which was done by another writer. Wambaugh sued the makers of The Choirboys and got his name removed from the credits.

==Reception==
The movie opened to positive praise as a true story of justice mishandled.
Janet Maslin of the New York Times observed, "This is a strong, affecting story but its also a straggly one, populated by tangential figures and parallel plotlines. The criminals histories are every bit as convoluted and fascinating as those of the policemen they abducted. Even the courtroom drama is unusually complicated, introducing a new legal team with each new trial. In writing his book about this, Mr. Wambaugh had time and energy to explore each new twist. But the format of a movie demands something more concise. Harold Becker and Mr. Wambaugh have worked so hard to preserve the storys complexity that theyve left it fragmented. The film is generally crisp and at times exciting, but its also full of incidents that are only sketchily explained, and minus the all-important narrative thread that might have provided a clear point of view."  

Variety (magazine)|Variety called the film "a highly detailed dramatization" and said James Woods "is chillingly effective, creating a flakiness in the character that exudes the danger of a live wire near a puddle."  

Time Out London thought the film was "expertly performed" and added, "Its the usual heavy Wambaugh brew: police procedure closely observed without a trace of romanticism, suggesting simply that life in the force is psychological hell. So far, so good. But that very insistence on authenticity is followed by the film to the detriment of the narratives dramatic structure; half way through, the whole thing begins to ramble badly. Engrossingly sordid, nevertheless."  

==Awards and nominations==
James Woods won the Kansas City Film Critics Circle Award for Best Supporting Actor and was nominated for the Golden Globe Award for Best Actor – Motion Picture Drama.

==Home release==
MGM Home Entertainment released the Region 1 DVD on September 17, 2002. The film is in anamorphic widescreen format with an audio track in English and subtitles in English, Spanish, and French. Bonus features include commentary by director Harold Becker and a featurette about the making of the film.

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 