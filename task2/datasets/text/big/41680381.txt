The 10 Commandments of Chloe
{{Infobox film
| name           = The 10 Commandments of Chloe
| image          = The 10 Commandments of Chloe.jpg
| alt            = Chloe
| caption        = 
| director       = Princeton Holt
| producer       = Princeton Holt Naama Kates
| writer         = Princeton Holt and Naama Kates
| starring       = Naama Kates Jason Burkey Wynn Reichert Wendy Keeling
| music          = Cyrus Melchor and Naama Kates
| cinematography = Christopher C. Odom
| editing        = Ben Baudhuin
| studio         = One Way or Another Productions
| distributor    = 
| released       =  
| runtime        = 73 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
The 10 Commandments of Chloe is a 2012 American drama film directed by Princeton Holt and starring Naama Kates.  The film was also co-written and co-produced by Holt and Kates.

==Plot==
The film follows Chloe Van Dynne, (played by Naama Kates) a twenty-something who arrives in in Nashville, Tennessee, with one goal: to find success as a singer-songwriter, no matter what. Wandering from bar to bar with her demo CDs in tow, she meets Brandon (Jason Burkey), a local who quickly falls for her, but who may prove more of an obstacle to her success than an aid.  Shot entirely on location, Nashville’s neon lights, street vendors and yellow cabs reveal a beautiful urban jungle not too different from New York or Los Angeles.   

==Cast==
* Naama Kates ... Chloe
* Jason Burkey ... Brandon
* Wynn Reichart ... Mr. Turner
* John Greer ... Derek
* Wendy Keeling ... Jesse
* David Vaughn ... Jackson
* Michael Miller ... Lounge Owner
* Shearon Miller ... Miranda
* Nathan Rogers ... Nate

==Production== Cookies & Cream, director Princeton Holt hones in on the fierce competitiveness of the music industry with a grounded sense of reality, aided by a fearless and star-making performance by musician and actress Kates, whom hed cast as Jodie in his previous film.  After developing the story from opposite sides of the country through Skype, emails and lengthy phone calls, the two met halfway, cast the film in 48 hours, and shot it over a spontaneous four days. The result is a love letter, both to a city of under-appreciated beauty, and to the deeply personal stories found in the lives of its people. 

==Reception==
Critical response for the film was generally positive, if controversial, with regards to its mumblecore style, the likeability of the protagonist, and the ambiguous ending.
 realism and complexity as possibly "challenging...  ainly because the Chloe character, wonderfully portrayed by co-writer Naama Kates, is guarded to an extent that you never really know what’s going on with her. Is she a serious musician, is she interested in Brandon, is she just up for getting drunk and hanging out… she runs a lot of different ways. Which is something that plays out as much in the story as it does for the audience’s engagement, as Brandon is having the same difficulty figuring her out." 

Sonic Cinema gave the film an A- (grade)|A-.  "The film by Holt and Kates, who gives a terrific lead performance, is rooted as much in intellect as it is in emotion," asserts critic Brian Skutle. 

FilmMonthly.com called the film "about 93% banter" and noted the lack of "  musical presence in the film" until the very end, despite commending its structure and "fantastic" final performance,  while Kirsten Walsch of Rogue Cinema argued that the dialogue "holds the audiences attention", comparing the film to Richard Linklaters Before Sunrise. 

Jared Mobarak called the protagonist "a clichéd, scorned woman... ...an indifferent bitch unable to look past her own needs", but agreed that "what follows   redeems it. We watch the whole film, peering in on a darkened city of attractive architecture and glowing neon lights waiting for some grand gesture of love only to discover that Holt and Kates were keeping from us something else entirely. The music and Chloe’s talent for it proves to be the real mystery at hand as anything Brandon or we could hope to learn is present in her song—a personal diary of pain, joy, and truth." 

Richard Propes of The Independent Critic also distinguishes the films visual aesthetic and emphasis on music: "  opens the film with almost stunningly beautiful lensing that sets the tone for a film that dances a fine balance between real life and the shadows in which we live.  The films authenticity is enhanced, as well, by Naama Kates stellar original music" as well as director Holt and the film in general.  "The 10 Commandments of Chloe is precisely the kind of indie gem that film fest audiences love to discover. With an insightful and well tuned performance by Naama Kates and a Kates/Holt penned script that exudes honesty and intelligence,  The 10 Commandments of Chloe is yet another winner from New York-based One Way or Another Productions." 

In his rave review, Daniel Coffeen offered an alternate analysis of the film and its plotline and characters: 

"But this will never have been that movie. All the familiar trappings swirl around Chloe, trying to bring her into that movie, into that life. But neither Princeton Holt, the director, nor Chloe, the character, are having any of it. They divert and toss aside cliché with a deft hand.  In many ways, this is a film about the temptation of cliché. Throughout the film, both Princeton Holt and Chloe ask: Is this a story of all too human interests — life, love, self-discovery? Or is all that, as she suggests in one scene, irrelevant? Are there other forces — of music, of life, of the landscape — that are more interesting, vital, engaging? ...Naama Kates Chloe is no mere woman, not even a person per se. She is a force eager to join the powerful torrent of Nashville music — music that is infinite and erratic, ugly and soft, music that flows and tears and creates all at the same time. Naama Kates is bewilderingly astute as we see all these forces, all this complexity, play in her face.  At the end, when she performs her exquisitely deranged song and hears the audience clapping, we watch her become part of the landscape. The look she gives us, only for a moment, is devastating and gorgeous. It is a look at once fearful and brave, poised and terrified, a look of someone abandoning herself in order to become herself, to become a force amidst the storm. She has followed her first commandment. She has assimilated. She is becoming blur and its beautiful."

== Accolades ==

=== Festival Screenings ===
*World Premiere: Calgary International Film Festival, September 21, 2013.  Discovery Award Nominee 
*Bare Bones International Film Festival, Muskogee, Oklahoma, April 5, 2014. Winner- Best Romance Comedy Feature Film Official Selection. 
*On Location: Memphis Intl Film and Music Festival, Memphis, Tennessee, April 24, 2014.  Official Selection. 

=== Awards ===
* 2012 Amsterdam Film Festival - World Cinema Screenwriting Award 
* 2012 IndieFest - Best Lead Actress (Naama Kates) 
* 2012 Los Angeles Movie Awards - Narrative Feature Award of Excellence (dir. by Princeton Holt)   
* 2012 Los Angeles Movie Awards - Best Actress (Naama Kates) 
* 2012 Los Angeles Movie Awards - Best Supporting Actor (Jason Burkey) 
* 2012 Los Angeles Movie Awards - Best Screenplay 

==References==
 

==External links==
*   
*  

 
 