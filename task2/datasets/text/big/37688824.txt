Attarintiki Daredi
 
 
 
{{Infobox film
| name           = Attarintiki Daredi
| image          = Attarintiki Daredi.jpg
| caption        = Theatrical release poster
| director       = Trivikram Srinivas
| producer       = B. V. S. N. Prasad
| writer         = Trivikram Srinivas
| starring       = Pawan Kalyan Samantha Ruth Prabhu Pranitha Subhash Nadhiya Boman Irani Brahmanandam
| music          = Devi Sri Prasad
| cinematography = Prasad Murella
| editing        = Praveen Pudi
| studio         = Sri Venkateswara Cine Chitra Reliance Entertainment
| distributor    = Reliance Entertainment
| released       =  
| country        = India
| language       = Telugu
| budget         =  550 million 
| gross          =  748.8 million  (share)    
}}
 Telugu Drama|family drama film written and directed by Trivikram Srinivas. It stars Pawan Kalyan, Samantha Ruth Prabhu and Pranitha Subhash in the lead roles with Nadhiya, Boman Irani and Brahmanandam in supporting roles.
 background score were composed by Devi Sri Prasad. Prasad Murella was the cinematographer. The film focuses on Gautham Nanda, a business heir who acts as a driver in his estranged aunt Sunandas house to mend her strained relationship with his grandfather Raghunandan who expelled her for marrying against his wishes years before.

Attarintiki Daredi was made with a budget of  550 million. Principal photography began in January 2013 and ended in July 2013, with the film being primarily shot in and around Hyderabad, India|Hyderabad. Significant portions were shot in Pollachi and Europe. The film was released on 27 September 2013 and received positive critical reception. It earned a worldwide share of  748.8 million, grossed nearly  1.2 billion, and emerged as the highest-grossing Telugu film of all time.
 Filmfare Awards, SIIMA Awards Hindi as Kannada as Ranna (film)|Ranna with Sudeep, Rachita Ram and Haripriya in the lead roles.

==Plot==
Raghunandan is a rich, unhappy businessman based in  . Gautham tries to woo Prameela but gives up when he learns that Prameela is in love with another man. Sashi hates Gautham and is suspicious of him and Paddu, his friend and a nurse who is appointed to take care of Rajasekhar. 

Sunanda later reveals to Gautham that she is aware of his real identity much before the incidents and warns him to abstain from doing anything with the intention of taking her back to Raghunandan.
 NRI staying penchant for women, and enters Sunandas home as his assistant.

Bhaskar falls for Sashi but his attempts are repeatedly thwarted by Gautham. On the day of her marriage, Sashi elopes with Gautham. While waiting with him for the train to Chennai, Siddhappas men reach the station to stop them only to be trashed by Gautham and his assistants, led by Balu. Through Balu, Sashi comes to know Gauthams real motive. An angry Rajasekhar, with Sunanda, arrive to shoot Gautham but Rajasekhar is taken aback after knowing his true identity. Gautham reveals that the day Sunanda left the house, Raghunandan tried to commit suicide but accidentally killed Gauthams mother. He says that he chose to love his grandfather though he killed his mother. Sunanda chose to hate him as he injured Rajasekhar and expelled them.
 government of Uganda and he is left with the same amount with which he ran away from Rajasekhars house when he assisted him in the past. Raghunandan reconciles with Sunanda and Gautham is unanimously appointed as the CEO of the company thanks to the support of Sunanda and Raghunandan. The film ends with Gautham holding Raghunandans hand with affection.

==Cast==
 
* Pawan Kalyan as Gautham Nanda/Siddhu (Siddartha)
* Samantha Ruth Prabhu as Sashi
* Pranitha Subhash as Prameela
* Boman Irani as Raghunandan
* Nadhiya as Sunanda
* Rao Ramesh as Shekhar
* Kota Srinivasa Rao as Siddhappa Naidu Ali as Paddu
* Brahmanandam as Baddam Bhaskar
* M. S. Narayana as Balu
* Mukesh Rishi as Hari, Gauthams father Pragathi as Pooja, Gauthams mother
* Posani Krishna Murali as Raja Ratnam
* Navika Kotia as Sashis younger sister
* Raghu Babu as Murthy
* Ahuti Prasad as father of Prameelas fiancee
* Brahmaji as Shekhars brother Hema as Shekhars relative
* Devi Sri Prasad (special appearance) in "Ninnu Chudagane"
* Hamsa Nandini (special appearance) in "Its Time To Party" 
* Mumtaj (special appearance) in "Its Time To Party"   
 

== Production ==

=== Development === Gabbar Singh (2012) and Cameraman Gangatho Rambabu (2012). The technical crew of Kalyans and Srinivass previous collaboration, Jalsa (2008), were selected to work on the film.    B. V. S. N. Prasad was expected to produce Attarintiki Daredi while Kalyan was expected to allot dates in his schedule from October 2012.  Trivikram Srinivas gave finishing touches to the script in late September 2012 and Reliance Entertainment was confirmed to co-produce.  Devi Sri Prasad was selected as the music director and the music sittings were held at Barcelona during the location scout in December 2012. 
 Milo robotic camera which B. V. S. N. Prasad brought in from Mumbai as it was unavailable in Hyderabad.  In mid July 2013, the title was confirmed to be Attarintiki Daredi. 

=== Casting ===
Pawan Kalyan opted for a complete makeover and sported a look resembling the one he sported in his previous films Kushi (2001 film)|Kushi (2001) and Jalsa.   After Trivikram Srinivas narrated the script to Kalyan, he asked him who would play the role of his aunt. and Trivikram Srinivas replied it would be Nadhiya. Kalyan gave his approval for Srinivass selection and Nadhiya was selected before she signed her comeback film in Telugu, Mirchi (film)|Mirchi (2013). Trivikram Srinivas narrated the script in a conversation with her over the telephone. After she accepted, he said that he would ask her dates after their schedules were planned.    She later revealed that she was scared of being typecast but did the role only on Trivikram Srinivas persistence and added that the character’s emotions were set quite well. 

Trivikram Srinivas decided to cast Ileana DCruz as the female lead who worked with him on Jalsa and Julai.  Samantha Ruth Prabhu was confirmed as the female lead in late October 2012 which marked her first collaboration with Kalyan.  The second female lead was yet to be selected. By the end of December 2012, most of the films which were in pre-production phase were confirming their female leads. Because of this the production unit decided to begin the shoot without confirming the other heroine. 

Pranitha Subhash was selected as the second female lead in early January 2013 and was confirmed to play Samanthas sister.  Pranitha called it a lifetime opportunity and said that she would be seen in a "very sweet, girl-next-door character."  Boman Irani made his debut in Telugu with Attarintiki Daredi;  he played Raghunandan, the grandfather of Kalyans character.  Kota Srinivasa Rao played the role of Sidhappa Naidu for which he sported a realistic and rugged look.  Navika Kotia was selected for the role of the younger sister of Samantha and Pranitha in April 2013. 
 Sunil in Poola Rangadu (2012) as a lead actor, Srinivas felt that it would not not be fair to make him play a comedian.  Rao Ramesh, Mukesh Rishi, Brahmanandam, Ali (actor)|Ali, M. S. Narayana and Posani Krishna Murali were cast in supporting roles.    

Mumtaj and Hamsa Nandini were confirmed to make special appearances in the song "Its Time To Party" marking the formers comeback in Telugu cinema after 12 years after anchor Anasuya refused the offer.  

===Filming===
 

B. V. S. N. Prasad stated during the films launch that principal photography would begin in mid-December 2012.  Kalyan searched for new locations in Spain as the story demanded a foreign location and around 30 to 45 days of shooting schedule was planned there. It was the first time Pawan Kalyan went for a location hunt and returned in the last week of December 2012.  Due to unknown reasons, the films shoot was postponed to 8 January 2013 in late December 2012.  

Shooting finally commenced from 24 January 2013 at a private hotel in Hyderabad.  The team planned a new schedule from 4 February 2013 in Pollachi where scenes on Kalyan, Samantha and other support cast were planned to be shot.  The schedule was postponed to 15 February 2013 and Trivikram Srinivas continued the shoot in Hyderabad.  After shooting for a few days in Hyderabad at a private mall, the planned schedule at Pollachi began from 12 February 2013. 

 , where Attarintiki Daredi was significantly shot.]]
An action sequence featuring Kalyan and other cast members was shot in late February 2013.  The next schedule commenced from 1 March 2013 at a specially erected set at Ramoji Film City in Hyderabad where scenes featuring Kalyan and the other cast members were shot.  A few crucial scenes featuring the lead actors were shot there in early April 2013. Art director Ravindar supervised for the special set, which was worth  30 million.  By mid-April 2013, a major portion of the film was shot there. Kalyan and the films unit planned to leave for Spain to shoot important sequences.  

Scenes featuring the lead cast were shot in the end of April 2013 in a railway station set erected at Ramoji Film City.  A new schedule began at Ramoji Film City from 2 May 2013.   That schedule ended on 28 May 2013. Samantha informed .the media that the films speaking portions had been completed and that an overseas schedule was remaining. 
 dubbing for the films first half was completed. The team returned from Milan on 2 July 2013.  
 wrapped on 14 July 2013. 

== Themes and influences ==
Attarintiki Daredi focuses on a young mans journey trying to convince his grandfathers estranged daughter to rejoin the family.    In its review, Sify felt that the films story was a reversal of Nuvvu Naaku Nachav (2001) and Parugu (2008).      Kalyans character, Gautham, is shown watching a few films,  where the protagonist is in disguise trying to win over people who matter to him.    In the film, Brahmanandams character spoofs the Ridley Scott film, Gladiator (2000 film)|Gladiator (2000) named Radiator and wins the Bascar awards found by himself after being inspired from the Academy Awards. 
 Gabbar Singh (2012). 

== Music ==
 

Devi Sri Prasad composed the soundtrack album which consists of 6 songs. Sri Mani, Ramajogayya Sastry and Prasad himself wrote the lyrics for the soundtrack album, which was marketed by Aditya Music.  The album was released on 19 July 2013 in a promotional event at the Shilpakala Vedika with the films cast and crew attending the event. The soundtrack album received positive reviews from critics. 

== Release ==
In late April 2013, Attarintiki Daredi was expected to release on 7 August 2013.  It was then scheduled for a release on 9 August 2013 on the eve of Independence Day (India)|Indias Independence Day. But due to the then-ongoing Telangana movement, the release was put on hold. Later B. V. S. N. Prasad officially announced that the film would release on 9 October 2013 along with Ramayya Vasthavayya (2013).  But the makers were forced to reschedule the release date to 27 September 2013 after 90 minutes of the footage was leaked onto the internet.  On an average, 80% of the films tickets were sold out in all screens for the first three days in Hyderabad, India|Hyderabad. Apart from India, the film released in United States, United Kingdom, Germany, Canada and Dubai. 

=== Distribution ===
The theatrical rights in the Nizam region were acquired by Global Cinemas in late April 2013 for a then undisclosed record price,  which was revealed later as  120 million in early June 2013.  My3  Movies announced on 26 April 2013 that they acquired the overseas distribution and DVD rights of the film.  Y. Naveen, who financed several overseas distribution firms, acquired this films overseas rights on behalf of My3 Movies for  70 million which, according to Sify, was "a big gamble".  Colours Media acquired the distribution rights in the United Kingdom from My3 Movies in late July 2013.  Lorgan Entertainments acquired the theatrical distribution rights in Australia in early August 2013. 

=== Marketing === idlebrainlive on YouTube fetched more than 190,000 hits and around 6,200 likes.  As of 16 July 2013, the teaser fetched over 300,000 million hits and about 9,000 likes thus setting a record for any Telugu video uploaded on YouTube at that time. By the end of the next day, the teaser was viewed 706,927 times and received more than 6,000 comments. 
 IANS wrote Oneindia Entertainment wrote "Attarintiki Daredi appears to be an out-and-out entertainer with an apt tag-line celebration of entertainment. It is also going to be high in entertainment quotient as some noted comedians Bramhanandam, Srinivas Reddy, Ali tickle the funny bones of the audiences". 

The video featuring Kalyan recording the song "Kaatama Rayuda" was released on 4 August 2013.  The video received positive response and according to a report by The Times of India, Pawan Kalyans fans opined that the song composed by Devi Sri Prasad reminded them of the song sung by Kalyan in the film Thammudu (film)|Thammudu (1999).  In late October 2013, the makers planned to add two more scenes as a promotional stunt to discourage piracy.  Six minutes of footage were added to the film this version was screened from 31 October 2013. 

=== Piracy ===
On the night of 22 September 2013, a 90-minute piece of footage was leaked online and became Viral video|viral. B. V. S. N. Prasad filed a complaint the following day against the act of piracy, and sought cyber-protection for Attarintiki Daredi. Because of this incident, the films release was delayed to 27 September 2013.  The films female lead, Samantha,  as well as other celebrities such as, Siddharth (actor)|Siddharth, Harish Shankar, S. S. Rajamouli, Ram Gopal Varma and Nithiin (actor)|Nithiin, all condemned the piracy act.  However, police initially suspected that the incident might be a publicity stunt enacted by people who invested in the film.   
 CDs and hard discs video parlours and shops renting CDs in the Krishna district were seized, and some of the shop owners were taken into custody for cross-examination|questioning.  Pavan Kalyan fans went out into the streets, with signs containing slogans protesting the piracy.  Subsequent to their investigation, the police arrested five people, in addition to production assistant Cheekati Arunkumar, and recovered several pirated copies of the film on 20 September 2013. Arunkumar had worked as a production assistant for the films Oosaravelli (2011) and Ongole Githa (2013), both of which were produced by B. V. S. N. Prasad. 
 DVD from DSP K.V. Srinivasa Rao, with the support of the Crime Investigation Department, Hyderabad, arrested the suspects in Hyderabad. The police filed charges on 24 September 2013, against the five suspects under clauses 63, 65 and 66 of the Copyright (Amendment Bill) 2010 and 429 IPC. 

Kalyan and Trivikram Srinivas decided to return a significant portion of their remuneration to help Prasad overcome the financial crisis caused by the leak.  Samantha returned her entire salary to the producers.  Kalyan remained silent during the entire issue, but finally spoke at length about the episode at the "Thank You Meet" of the film on 14 October 2013. He said that this was a conspiracy and not piracy. He added that he was very well aware of the facts as to who was behind the leak of the film and will not spare anyone and will strike when the time is right. 

=== Home media ===
The Television broadcast rights were sold to an unknown channel in mid June 2013 for an amount of  90 million which happened to be the highest amount that a television channel paid for the telecast rights of a Telugu film till the sale of rights of Aagadu (2014) to Gemini TV in June 2014.   The films television premier was announced in mid December 2013 to be held on 11 January 2014 in MAA TV.  The film registered a TRP rating of 19.04 which was the highest for any Telugu film till date.  The Indian DVD and Blu-ray were marketed by Volga Videos.   The overseas DVD and Blu-ray were marketed by Bhavani Videos.  

== Reception ==

=== Critical reception ===
{{multiple image
 
| align     = right
| direction = vertical
| footer    = Kalyan (top) attracted positive reviews for his performance while Samantha (bottom) received praise for her performance in a limited role.
| width     = 
 
| image1    = Pawan2.jpg
| width1    = 200
| alt1      = 
| caption1  =

 
| image2    = Samantha Ruth Prabhu at 60th South Filmfare Awards 2013.jpg
| width2    = 200
| alt2      = 
| caption2  =
}}
Attarinitki Daredi received positive reviews from critics according to   rated the film 4 out of 5 and stated "If you are looking for sheer entertainment, know that Pawan Kalyan and Trivikram have gift packed for you with high dose of comedy. It is entertainment, entertainment and entertainment. Pawan Kalyan`s histrionics, his performances and Trivikram`s handling of the simple story in effective way is what makes Attharintiki Daaredhi a big entertainer". 
 Oneindia Entertainment rated the film 4 out of 5, calling it "a must watch film for the fans of Pawan Kalyan and Trivikram."  Jeevi of Idlebrain.com rated the film 4 out of 5 and praised Trivikram Srinivass script and added that Kalyans performance and Trivikram Srinivas story telling skill "makes sure that your heart is touched at times and heartily laugh all the time while watching the movie".  IndiaGlitz rated the film 4 out of 5 and praised Kalyans performance in the films climax, calling it "an unseen angle" before conlcuding that the film "is a treat for the family audience". 
 IANS rated silver lining". 

=== Box office ===
The film collected  335.2 million by the end of its three-day first weekend.    By the end of its first week, the film collected over  490 million at the worldwide box office.    The film managed to perform well despite facing competition from Ramayya Vasthavayya and collected  603.4 million in two weeks at the worldwide box office.  Trade analyst Taran Adarsh reported that till 13 October 2013, the film collected  633.1 million at worldwide box office.    The film collected  655 million in 19 days at worldwide box office.    The film collected  701.8 million in 24 days at the worldwide box office and became the second Telugu film to cross  700 million after Magadheera (2009).    It collected  712.4 million at the worldwide box office by the end of the fourth week.  The film collected  733.8 million at the worldwide box office in 39 days.    The film collected  733.6 million at the worldwide box office in six weeks.    The film collected  739 million at the worldwide box office by the end of its seventh weekend.  The film became the highest grossing Telugu film of all time surpassing Magadheera.  Attarintiki Daredi collected a worldwide share of  748.8 million and grossed  1.2 billion in its lifetime.  

==== India ==== Ceded region with  21.5 million, Guntur district with  14 million, East area with  10.4 million, Vishakhapatnam district with  8.2 million, West area with  7.5 million, Krishna district with  7.1 million and Nellore district with  5.7 million.    According to Adarsh, the film collected  54.7 million at the Andhra Pradesh and Nizam regions.    The film collected  227.1 million at the Andhra Pradesh and Nizam regions,  30 million together in Tamil Nadu and Karnataka and  16.6 million in the rest of India by the end of its three-day first weekend taking its worldwide total to  335.2 million.  The film collected  37.3 million taking its four-day total in the Andhra Pradesh and Nizam regions to  259.3 million, thereby overtaking the first week totals of Seethamma Vakitlo Sirimalle Chettu and Mirchi (film)|Mirchi. 
 blockbuster at the box office. 

The film collected over  13 million on its 19th day making its total in the Andhra Pradesh and Nizam regions cross the  500 million mark.  It stood in the third spot in the list of top 10 highest grossing (share) Telugu films in the Andhra Pradesh and Nizam regions preceded by Magadheera and Gabbar Singh. The film crossed the  200 million mark in the Nizam region and  90 million in the Ceded region in 20 days.  The film collected  9.5 million on its 24th day taking its total in the Andhra Pradesh and Nizam regions to  546 million, thereby leading to Gabbar Singh dropping to third place in the list of films with the highest distributors share in Andhra Pradesh. Gabbar Singh had collected  508.5 million in the Andhra Pradesh and Nizam regions in its lifetime.  The film completed its 25 day run on 21 October 2013.  The film collected  550 million at the Andhra Pradesh and Nizam regions,  52.2 million at Karnataka and  15 million from the rest of the country by the end of its fourth week.  The film crossed  230 million in the Nizam region in its fifth weekend.  The film collected  577.3 million at the Andhra Pradesh and Nizam regions in 39 days. 

The film collected  580.1 million at the Andhra Pradesh and Nizam regions in six weeks.  The film completed a 50 day run in around 170 theatres on 15 November 2013.   The film completed a 100 day run in 32 theatres on 4 January 2014.  On the 100th day of its theatrical run, Attarintiki Daredi was screened in four centres in Nizam, eleven in Ceded, one in Vishakhapatnam, four each in Krishna and Guntur, six and two centres in East and West Godavari respectively. 

==== Overseas ====
According to Adarsh, the film collected  26.7 million from paid previews in the United States box office.  The film collected more than $345,000 and grossed more than the other releases of 2013, Baadshah and Seethamma Vakitlo Sirimalle Chettu, at the United States box office.  He stated that the film collected $429,000 from Thursdays preview in the United States, thereby overtaking the collections of paid previews of Chennai Express (2013) in the country.  Adarsh reported that the film collected $495,000 on Friday taking its two-day total to $924,000 which was equivalent to approximately  50 million.  The film collected  61.5 million in the first three days at United States box office.  The film collected  109.1 million in its first weekend at the United States box office.  According to Adarsh, the film was the third biggest opener in the United States in 2013 after Chennai Express and Yeh Jawaani Hai Deewani and was the only Indian film apart from the other two to feature in the list of top 15 openers of 2013 in the country. 

The film grossed over $1.5 million within three days overtaking the preview collections of Baadshah in the United States and was expected to cross the $2 million mark there.  By the end of its fifth weekend, the film collected $2 million from the theatres included by the measurement and research company, Rentrak and $0.3 million from theatres not included by Rentrak at the United States box office taking the films total to  140 million. It collected a total  60 million from United Kingdom, Australia, New Zealand, Middle East region, Canada, Singapore and others, taking its total to  200 million, which made it the highest grossing Telugu film overseas. 

== Awards and nominations ==
{| class="wikitable"
|-
! Ceremony
! Category
! Nominee
! Result
|-
|B. Nagi Reddy Memorial Award  Best Telugu Family Entertainer
|B. V. S. N. Prasad
| 
|- 61st Filmfare Awards South   Best Film
|B. V. S. N. Prasad
| 
|- Best Director Trivikram Srinivas
| 
|- Best Actor- Male Pawan Kalyan
| 
|- Best Actor- Female Samantha Ruth Prabhu
| 
|- Best Music Director Devi Sri Prasad
| 
|- Best Supporting Actor- Female Nadhiya
| 
|- Best Supporting Actor- Female Pranitha Subhash
| 
|- Best Lyricist Sri Mani for "Aaradugula Bullet"
| 
|- Best Playback Singer - Male Shankar Mahadevan for "Bapu Gari Bommo"
| 
|- 3rd South Indian International Movie Awards   Best Film
|B. V. S. N. Prasad
| 
|- Best Director Trivikram Srinivas
| 
|- Best Actor - Male Pawan Kalyan
| 
|- Best Actor - Female Samantha Ruth Prabhu
| 
|- Best Cinematographer Prasad Murella
| 
|- Best Music Director Devi Sri Prasad
| 
|- Best Playback Singer - Male Shankar Mahadevan for "Bapu Gari Bommo"
| 
|- Best Lyricist Sri Mani for "Aaradugula Bullet"
| 
|- Best Supporting Actor - Male Boman Irani
| 
|- Best Supporting Actor - Female Nadhiya
| 
|- Best Fight Choreographer Peter Hein
| 
|- Best Dance Choreographer Ganesh for "Its Time to Party"
| 
|- Santosham Film Awards  Best Film
|B. V. S. N. Prasad
| 
|- Best Actor - Male Pawan Kalyan
| 
|- Best Actor - Female Samantha Ruth Prabhu
| 
|- Best Cinematographer Prasad Murella
| 
|}

== Remakes == Vijay was rumoured to reprise Kalyans role in the films Tamil remake post the films release but there was no official confirmation from the former.  The film was later remade into Kannada as Ranna (film)|Ranna in 2014 by Nanda Kishore with Sudeep, Rachita Ram and Haripriya in the lead roles.  In September 2014, Shah Rukh Khan was rumoured to reprise Kalyans role in the films Hindi remake which was supposed to be Khans third south remake.     He later clarified that it was just a rumour in an interview to a leading daily. 

== Notes ==
 

== References ==
 

==External links==
*  

 
 
 

 
 
 
 
 
 
 