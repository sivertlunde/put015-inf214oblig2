Ami, Yasin Ar Amar Madhubala
{{Infobox film
| name           = Ami, Yasin Ar Amar Madhubala
| image          = amiyasinomadhubala.jpg
| caption        = Screenshot
| director       = Buddhadev Dasgupta
| producer       =Anurradha Prasad, Shanjiv Shankar Prasad
| writer         = Buddhadev Dasgupta
| starring       = Prosenjit Chatterjee Sameera Reddy Amitabh Bhattacharjee
| music          =  Biswadep Dasgupta
| cinematography = Sunny Joseph
| editing        = Amitava Dasgupta
| distributor    =
| released       =  
| runtime        = 115 minutes
| country        = India Bengali
| budget         =
| gross          =
}}
Ami, Yasin Ar Amar Madhubala ( ) is a 2007 Indian Bengali film directed and written by Buddhadev Dasgupta. The film was screened at the Toronto International Film Festival.

==Plot==
When a Kolkata surveillance specialist and his roommate install a small camera in the home of their beautiful neighbor, they somehow become terror suspects in director Buddhadeb Dasguptas cutting commentary on CCTV society. Yasin (Amitav Bhattacharya) and his roommate Dilip (Prosenjit Chatterjee) are smitten with their beautiful new neighbor Rekha (Sameera Reddy). Innocent pining becomes silent obsession, however, when Dilip decides to install a surveillance camera directly over Rekhas bed. At first Rekha remains blissfully unaware that her privacy has been invaded, but when she finally realizes shes being spied on, her nosy neighbors are forced to go on the run. Little do Yasin and Dilip realize that across town a terrorist cell is plotting their latest attack, and now the local authorities believe that Yasin may be a key part of their diabolical plans.

==Cast==
*Amitav Bhattacharya as  Yasin
*Prasenjit Chatterjee as Dilip
*Sameera Reddy as  Rekha

==External links==
*  

 
 

 
 
 
 
 


 