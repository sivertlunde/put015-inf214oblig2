Mimino
{{Infobox film name = Mimino image = mimino poster.jpg caption = Film poster director = Georgiy Daneliya producer = Mosfilm writer = Georgiy Daneliya Revaz Gabriadze Viktoriya Tokareva starring = Vakhtang Kikabidze Frunzik Mkrtchyan Yevgeny Leonov cinematographer = Anatoliy Petritskiy music = Gia Kancheli released =   country = Soviet Union runtime = 97 minutes language = Russian Georgian Georgian
}} Soviet era comedy won the 1977 Golden Prize at the 10th Moscow International Film Festival.   

==Plot== Georgian Bush bush pilot Armenian truck driver Roobik Khachikyan (Frunzik Mkrtchyan) who is given a place in that hotel by mistake instead of another Khachikyan (Professor), and they have a lot of adventures in Moscow.

Always amicable and open to people, Mimino does not feel at home in the big city. Nevertheless, he becomes a pilot of a supersonic jet liner, the Tupolev Tu-144, flying all over the world. But feeling homesick, he finally comes back to his native town of Telavi in Georgia, to his family and friends.

Miminos real name in the film is Valiko Mizandari—his nickname Mimino ( ) is the Georgian word for sparrow hawk, although it is stated on the back cover of the DVD that Mimino means falcon.  Either way, it seems that this nickname is to equate to some kind of bird of prey, which is perhaps fitting for a pilot.

==Cast==
* Vakhtang Kikabidze as Mimino (as Buba Kikabidze)
* Frunzik Mkrtchyan as Rubik
* Elena Proklova as Komarova
* Evgeni Leonov as Volokhov
* Kote Daushvili as Grandfather (as Konstantin Daushvili)
* Ruslan Miqaberidze as Givi Ivanovich
* Zakro Sakhvadze as Varlaam
* Marina Dyuzheva as Advocate (as Mariya Dyuzheva)
* Rusiko Morchiladze as Lali
* Archil Gomiashvili as Papashvili
* Aleksandr Alekseyev as Prosecutor (as A. Alekseyev)
* Vladimir Basov as Sinitsyn (as V. Basov)
* Valentina Titova as Sinitsyns Wife (as V. Titova)

==DVD Extras==
The films cinematographer, Anatoliy Petritskiy, is interviewed about the film.  He reminisces about his experiences working with Daneliya and the actors.  He had known Daneliya for some time and had seen several of his films.  He was very happy to receive the offer to work with him.  Initially the two were working on a different film in 1977, and discussed the doubts they had about the script, after which they decided to work on "Mimino" as an alternative.  Petritskiy was surprised that Goskino had already approved the film and that funding had been secured, and the project moved forward.  Petritskiy discusses the locations of the various shots in the film, to include various villages in Georgia, the Tbilisi airport, Moscow, and what was then West Germany.  According to Petritskiy, casting the film was not a problem, as Daneliya was familiar with all of the actors in the film.  More strange to the cinematographer, was Daneliyas wish to shoot scenes in the mountainous region Tushetia, because it is was a very remote area that could only be reached by helicopter, rather than roads.  Petritskiy described the conditions as "medieval".  He noted that the lack of electricity had "put its stamp of the character of the population of Omalo|Amalo", which basically lived without light.  He described the society as "patriarchic".  He describes this as the conflict in the film—the conflict between the simple way of life and the way of life in the big city—the is the meaning of the film as he sees it.  It is because of this that he filmed the movie in a very simple style, "a restrained style".  He points out that even the portions of the film shot in Moscow are static shots or simple panoramas.  He considers the landscapes of Georgia in the film to be "extremely beautiful".  Petritskiy then discusses the various shots at airports, which were done as a montage—the helicopter in Tbilisi, and the magnificent TU plane shot in Moscow.  Initially the latter part of the film was to be shot in America, but because of "purely budgetary reasons" according to Petritskiy, the cast and crew could not film there, so opted to shoot in West Berlin.  Thus, the script was revised.  Petritskiy notes that the Soviets were in power during 1977, so it was arranged that the group would stay in East Berlin—again "for economic reasons", and every day had to cross the border to the American zone to film at the Tegel Airport.  As for the scenes with the cow (hanging from the helicopter), it was shot in two stages—a papier-mache dummy of a cow was used with the helicopter for the far off shots, and a live cow was lifted by a crane was used for the close-ups.  He notes that the cow was only hung at a low height, but high enough to use the blue sky as a backdrop, and the cow was not hurt.  Petritskiy did not consider the scene to be difficult, pointing out that the winter scenes were much harder.  He details the harsh living conditions in the village of Darklo where they stayed.  He describes how few locals stay during the winter, and how the living quarters were not guarded or locked, but housed a few shepherds who had stayed back for the winter.  He explains how there were "sleep-benches" within the houses, and how they lived on canned preserves during their stay, sharing their "feast" with the hungry shepherds there.  In conclusion, Petritskiy notes how he "got a very pleasant impression from the work on that film," noting that it was shown at the Moscow International Film Festival and won the top prize there.{1}

==Awards==
1977 – Golden Prize at the 10th Moscow International Film Festival
1978 – "Golden Lacheno" Award at the 19th IFF in Avellino (Italy)
1978 – Best Comedy Film Award at the 11th All-Union Film Festival in Yerevan
1978 – USSR State Prize (Director G. Daneliya, Actors V. Kikabidze, F. Mkrtchyan)

==References==
 
{1} The 2004 release of the film, which offers both English and French subtitles, includes the interview with Anatoliy Petritskiy as a DVD bonus feature.  The above text is a summary of Petritskiys comments in Russian, based on the English subtitles.  The ISBN of the film is 4606777006229.

==External links==
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 