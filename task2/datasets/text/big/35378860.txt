Le Halua Le
{{Infobox film 
 | name = Le Halua Le
 | image = LehalwaLefilm.JPG
 | caption = Promotional Poster
 | director = Raja Chanda
 | producer = Shree Venkatesh Films
 | writer = 
 | dialogue =  Payel Hiran Hiran
 | music = Jeet Ganguly
 | lyrics = 
 | associate director = 
 | art director = 
 | choreographer = 
 | released = April 13, 2012
 | runtime = 125 min.
| country = India Bengali
 | budget = ₹15 million
 | Box Office = ₹ 40 million
 | followed_by =  screenplay = N.K. Salil}}
  2012 Bengali Indian feature feature comedy directed by Raja Chanda, starring Mithun Chakraborty. The film is the remake of successful Malayalam language|Malayalam-language film Poochakkoru Mookkuthi directed by Priyadarshan, who again remade the film in Hindi language|Hindi-language as Hungama later.

==Cast==
 
* Mithun Chakraborty as Harshabardhan Banerjee
* Soham Chakraborty as Rahul Hiran as SUVOJEET aka Suvo Payel as Sonali
* Aritra Dutta Banik as Dipu
* Kharaj Mukherjee as Gobindo
* Kanchan Mullick as Langcha
* Laboni Sarkar as Sonali
* Kamalika Banerjee
* Shantilal Mukherjee
* Rajatava Dutta as Sona Da MLA

==Soundtrack==

* Le Halua Le - Bappi Lahiri & Priyanka Vaidya
* Darling O Amar Darling - Jeet Ganguly & Monali Thakur
* Chupi Chupi - Mohit Chauhan & Shreya Ghoshal
* Monta Amar Love You Love You Kore Re - Kunal Ganjawala 

==References==
 
* http://www.telegraphindia.com/1120402/jsp/entertainment/story_15322592.jsp

 
 
 
 

 
 