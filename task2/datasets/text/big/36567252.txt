Crazy Joe (film)
{{Infobox film
 | name = Crazy Joe
 | image =  Crazy Joe (film).jpg
 | caption =
 | director = Carlo Lizzani
 | writer = Lewis John Carlino  (screenplay) Nicholas Cage  (story)
 | starring =  Peter Boyle
 | music =  Giancarlo Chiaramello	 	
 | cinematography = Aldo Tonti
 | editing =  Peter Zinner
 | producer = Dino De Laurentiis
 | distributor =
 | released = 1974
 | runtime =
 | awards =
 | country = English
 | budget =
 }} 1974 Cinema Italian drama Joseph Gallo, a mobster member of the Colombo crime family.   The film stars Peter Boyle in the title role, with Fred Williamson, Eli Wallach, and Paula Prentiss.

== Plot==
As soon as Joe Gallo gets out of jail, he returns to New York and begins building his crime organization with the help of Harlem associates. He goes too far, however, and before long Joe himself becomes a target of both cops and crooks. The film chronicles twelve years in Gallos life (1960-1972).

== Cast ==
* Peter Boyle		as 	Joe
* Paula Prentiss		as  	Anne
* Fred Williamson		as  	Willy
* Eli Wallach		as 	Don Vittorio
* Rip Torn		as  	Richie
* Charles Cioffi		as  	Coletti
* Luther Adler		as 	Falco
* Carmine Caridi		as  	Jelly
* Henry Winkler		as 	Mannie
* Sam Coppola		as  	Chick
* Franco Lantieri		as 	Nunzio
* Louis Guss		as  	Magliocco
* Fausto Tozzi		as  	Frank
* Guido Leontini		as 	Angelo
* Mario Erpichini	as 	Danny
* Michael V. Gazzo		(credited as Michael Gazzo)

==References==
 

==External links==
* 

 
 

 

 
 
 
 
 
 
 
 
 
 

 