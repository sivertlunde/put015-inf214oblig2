Mad About Dance
 
{{Infobox film
| name           = M.A.D - Mad About Dance
| image          = Mad_About_Dance_Official_Poster.jpg
| director       = Saahil Prem
| producer       = 
| writer         = Saahil Prem
| dialogue       = Kanu Behl
| editor         = Meghna Manchanda Sen
| starring       = Saahil Prem Amrit Maghera Salah Benlemqawanssa Akhilesh Unnithan Raashul Tondon Abhishek Saha Mark Monero
| music          = Vidyadhar Bhave Dr. Zeus Saahil Prem Siddharth Haldipur Sangeet Haldipur
| cinematography = Uday Tiwari
| released       =  
| country        = India
| language       = Hindi
}}
 2014 Indian dance film. The film is directed by Saahil Prem. The film stars Saahil Prem and Amrit Maghera in the lead roles. Saahil Prem debuted as an actor alongside Sunny Leone in the 2014 film Ragini MMS 2, and is now making his debut as a director with Mad About Dance.

Essentially a dance film, it focuses on the youth, their dreams, anxieties, disappointments and hopes for their future. The film is based in the quaint and beautiful university town of Sheffield and brings to light the stories of Asian students who leave their home and country and go to study abroad…Their struggles, their heartbreaks, their trials and triumphs…

==Soundtrack==
{| border="4" cellpadding="4" cellspacing="0" style="margin:1em 1em 1em 0; background:#fff; border:1px #abd5f5 solid; border-collapse:collapse; font-size:95%;"
|-  style="background:#e9ab17; text-align:center;"
! Track !! Song !! Singer(s) !! Composer
|-
| 1
| Ishq Da Bukhar
| Krishna Beura & Amrit Maghera, Swati Asai
| Vidyadhar Bhave
|-
| 2
| Party Is Going Mad
| Sangeet-Siddharth ft Divya Kumar, Vidyadhar Bhave
| Sangeet-Siddharth
|-
| 3
| "Kahan Hai Khuda"
| Subhan Pradhan
| Saahil Prem
|-
| 4
| "Punjabi Mundeya"
| Ravindra Upadhyay, Lil Shorty
| Dr. Zeus
|-
| 5
| "Kahan Hai Khuda (Dubstep)"
| Vidyadhar Bhave
| Saahil Prem
|-
| 6
| "Kahan Hai Khuda (Remix)"
| Vidyadhar Bhave
| Saahil Prem
|}

==References==
 

==External links==
*  
* https://www.facebook.com/madabtdance

 
 
 


 