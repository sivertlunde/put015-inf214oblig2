Thunderbirds (film)
 Thunderbird}}
 
 
{{Infobox film
| name = Thunderbirds
| image = Thunderbirds movie poster.jpg
| caption = Theatrical release poster
| director = Jonathan Frakes
| producer = Tim Bevan Eric Fellner Mark Huffam
| writer =   Sylvia Anderson  
| based on = Thunderbirds (TV series)|Thunderbirds by Gerry Anderson and Sylvia Anderson
| starring = Bill Paxton Anthony Edwards Sophia Myles Ben Kingsley
| music = Ramin Djawadi Hans Zimmer  
| cinematography = Brendan Galvin Martin Walsh
| studio = StudioCanal Working Title Films 
| distributor = Universal Studios
| released =  
| runtime = 95 minutes
| country = United Kingdom United States France 
| language = English
| budget = $57 million
| gross = $28,283,637
}} 1960s television series of the same name, directed by Jonathan Frakes.

The film, written by William Osborne and Michael McCullers, was released on 24 July 2004 in the United Kingdom and 30 July 2004 in the United States, with later opening dates in other countries. Whereas the original TV series used a form of puppetry termed "Supermarionation", the films characters are portrayed by live-action actors.

Thunderbirds received mainly negative reviews, and was a box office bomb. The creator of the original series Gerry Anderson disliked the film calling it "the biggest load of crap Ive seen in my entire life" {{cite news|
url=http://www.guardian.co.uk/media/2009/feb/07/gerry-anderson-thunderbirds-auction | work=The Guardian | location=London | title=Gerry Anderson auctions Thunderbirds treasures | first=Caitlin | last=Fitzsimmons | date=7 February 2009 | accessdate=25 May 2010}} 
 Thunderbirds are UK charts Record of the Year award.

==Plot== Gordon – Lady Penelope Parker (Ron Cook), as well as genius scientist Ray "Brains (Thunderbirds)|Brains" Hackenbacker (Anthony Edwards). Brains and his son, Alans best friend, Fermat (Soren Fulton), also live on Tracy Island, along with caretaker Kyrano (Bhasker Patel), his wife and their adolescent daughter Tin-Tin Kyrano|Tin-Tin (Vanessa Hudgens). Alans brothers see him as a child, not yet ready to be part of IR.
 The Hood Thunderbird 5, Thunderbird 3 mind powers to overcome Brains resistance, he disables the control systems, stranding the rescuers aboard the disintegrating Thunderbird 5.
 Thunderbird 2 Bank of London.
 Thunderbird 1, chaperoned by Lady Penelope, while Parker heads for the Bank of London to meet up with them in FAB 1.
 Thunderbird 4 rescue submarine. When Alan and Fermat are unable to connect the lifting cable, Tin-Tin dives into the Thames, swims down to the stricken monorail where she secures the cable, then joins Alan aboard Thunderbird 4. Jeff and his other sons arrive just in time to see the successful rescue, then Jeff, Lady Penelope, Alan, Tintin and Fermat rush to the bank, where they are joined by Parker.

Lady Penelope and Jeff get captured while Fermat and Parker defeat Mullion and Tin-Tin defeats Transom. Alan fights the Hood, who repeatedly uses his mental abilities. When the Hood has Alan at his mercy Tin-Tin arrives, revealing that she has mental powers similar to her uncles, using her mind to overpower him in his weakened state, which results in the Hood falling into mortal danger. Alan, who has to choose between saving the Hood and letting him die, saves him, saying, "I dont want to save your life, but its what we do."
 President has the Thunderbirds &ndash; with Alan assuming Jeffs position in active duty &ndash; taking off for another mission.

==Cast==
 
*Brady Corbet as Alan Tracy
*Bill Paxton as Jeff Tracy Hood
*Vanessa Hudgens as Tin-Tin Kyrano|Tin-Tin
*Soren Fulton as Fermat Hackenbacker
*Sophia Myles as Lady Penelope Creighton-Ward
*Ron Cook as Aloysius Parker Ray "Brains" Hackenbacker
*Philip Winchester as Scott Tracy John Tracy
*Dominic Colenso as Virgil Tracy
*Ben Torgersen as Gordon Tracy
*Bhasker Patel as Kyrano
*Harvey Virdi as Onaha
*Deobia Oparei as Mullion
*Rose Keegan as Transom
 

==Production== Gerry and Sylvia Anderson. It was preceded by Thunderbirds Are Go in 1966 and Thunderbird 6 in 1968, both films using the Supermarionation production techniques of the series.

Thunderbirds is dedicated to the memory of Stephen Lowen, a rigger on the film, who died in a fall whilst dismantling one of the sets. 

==Reception== Empire gave the film two out of five stars. Rotten Tomatoes gave the film a 19% "rotten" rating and a consensus calling the film a "Live-action cartoon for kids." Yahoo! Movies and Entertainment Weekly gave the film a C+ rating. Metacritic gave the film 36 of 100.
 Ford rather than a Rolls-Royce car|Rolls-Royce.  However, this was because the producers could not reach a suitable agreement with BMW in the authorised use the Rolls-Royce marque; the car manufacturer insisted that only an actual production model could be used. Ford stepped in with special version of their Thunderbird model, duplicating the six-wheel system on the Supermarionation Rolls.  FAB-1 steers with the four front tyres.
 Ford Thunderbird which are owned by Lady Penelope, as well as many Ford C-MAX and Ford F-150s in various locations, leading to jeers over the too-obvious level of product placement by the car manufacturers - a sentiment actually shared by director Jonathan Frakes, as revealed in the DVD audio commentary.

During development, creator Gerry Anderson was invited to act as creative consultant, but was left out when the studio felt there were enough employees on the payroll acting as part of the creative team. The studio offered him $750,000 (£432,000) to attend the premiere but Anderson could not accept money from people he had not worked for. He eventually saw the film on DVD and was disappointed, declaring "It was disgraceful that such a huge amount of money was spent with people who had no idea what Thunderbirds was about and what made it tick."   He also said that it was "the biggest load of crap I have ever seen in my entire life." 

Co-creator Sylvia Anderson, and the one responsible for character development, was given a private screening of the film and attended the London premiere.  She had a far different opinion.  "I felt that Id been on a wonderful Thunderbirds adventure.  You, the fans, will Im sure, appreciate the sensitive adaptation and Im personally thrilled that the production team have paid us the great compliment of bringing to life our original concept for the big screen. If we had made it ourselves (and we have had over 30 years to do it!) we could not have improved on this new version. It is a great tribute to the original creative team who inspired the movie all those years ago.  It was a personal thrill for me to see my characters come to life on the big screen." 

Timed to coincide with the theatrical release of Thunderbirds, the two prior films were released on DVD. The DVD versions of all three films include a number of extra features, including historical and production information.

==Differences from the original==
 
 original series. Thunderbird instead of a Rolls-Royce and can now only seat two people as well as being able to turn into a jet plane. Thunderbird 3 was also shown to dock with Thunderbird 5 differently; in the film it docks side on instead of the rocket head going into the space station.

The organisation is also referred to more commonly as "Thunderbirds" rather than "International Rescue"; although on their induction at the end of the film Alan, Tin-Tin, and Fermat receive badges that are designed with the "IR" logo on them as per the original TV series, intimating that the team are still officially called this (it even says in the trailer and in the intro to the film that the organization is called International Rescue and that Thunderbirds is a nickname that comes from the names of their machines). Though it seems the medias common parlance of "Thunderbirds" has become the norm, and been adopted amongst the family members themselves for everyday use.

Also, the plot was changed dramatically by making the younger Tin-Tin, Alan Tracy, and Fermat Hackenbacker, who is depicted as being Brains son, the main characters. In the original, however, Alan Tracy does sometimes tend to have a larger role than the others and certainly a more emotional storyline (especially in the Thunderbirds Are Go! movie),  .  but he has never been the main character. In the original series, Alan and Tin-Tin were much closer to the age of the rest of the Tracy brothers; with Alan being captain of Thunderbird 3 which can be seen from the opening title sequence of the very first episode.  Episode 01.   Fermat Hackenbacker was only seen in this movie, because there is no mention of Brains ever being married, least of all having had a son. In the original series, Brains name was never officially revealed, with "Hiram Hackenbacker" merely an alias (as seen in the episode "Alias Mr. Hackenbacker");   Episode 29.   in the film it appears to be his actual name. Also Jeff Tracy never flew any of the Thunderbirds craft, and there has never been an instance where he went off to the danger zone (with the exception of the episode "Brink of Disaster",   Episode 11.   as he was caught in the accident).
 Grandma in the TV series) does not feature or get mentioned during the film. Tin-Tin and Kyrano change nationality in the film as well; they had been Malaysian throughout the TV series but were depicted as being from India in the film.

Other changes are more canonical. The feature film is set in 2010, while the original is set in 2065 (i.e. the date now accepted by many fans in Thunderbirds canon as the year of International Rescues first mission, although it says on the back of the DVD that the film in set in 2065).  In the feature film, the Hood said that he was left for dead in one of International Rescues earlier missions, but in "Trapped in the Sky"—which was stated as International Rescues first mission—he was already trying to get their technology, of whose existence he knew via Kyrano. 

A huge difference in the movie is the International Rescue uniforms. The movie had dropped the original and recognisable concept of the blue uniform with coloured sashes and hat. In the movie, they wear grey modern astronaut uniforms with coloured piping - the colour depends on which Thunderbird machine they handle. They have absolutely no similarities to the original design. Lady Penelope also wears an International Rescue uniform in the movie, which she never wore at all throughout the series.

Another notable difference between the 2004 film and the original TV series is that International Rescue now allows itself to be filmed and photographed on missions. One of the recurring "rules" in the original TV series was that under no circumstances was anything related to International Rescue—be it the pilots or the craft themselves—permitted to be photographed. For example, in the episode "Terror in New York City", Scott Tracy electromagnetically wipes a recording of Thunderbird 1 when news crew starts filming.   Episode 13. 

Another difference is that The Hoods powers seem to make him weaker when he uses them and his eyes are red, whereas in the original series they were always a bright yellow. Also contrasting the TV series, Tin-Tin actually shares the same powers as her uncle, the Hood, as seen in the movies finale.

In the TV series, Gordon Tracy was the pilot of Thunderbird 4 and Alan was the pilot of Thunderbird 3, with John Tracy subbing on occasion. In the film, the roles are reversed. However, as Alan only uses Thunderbird 4 during the climax when Gordon is unavailable, it may be that Gordon is trained to pilot both craft in the films continuity, since they are unlikely to be required for the same mission.

The smoking content featured in the original series has been dramatically reduced; no character is seen with cigar or cigarette in hand. In the original series, Lady Penelope was never without her cigarette holder, and Jeff Tracy smoked a cigar after the completion of a successful mission.

Unlike the TV series, the island is actually referred to as Tracy Island in the dialogue, whereas the TV version was only called Tracy Island on Thunderbirds merchandise. Also, in the film, when John calls the island just seconds before Thunderbird 5 is struck by a missile, he calls out "Thunderbird 5 to Tracy Island". In the series, the Tracy boys would normally radio something like "Base from Thunderbird 1, 2, 5" etc. to the island.

==References==
;Primary sources
 

;Secondary sources
 

==Further reading==
* 

==External links==
 
* 
* 
* 
* 

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 