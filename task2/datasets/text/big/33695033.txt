A Devil with Women
{{Infobox film
| name           = A Devil with Women
| image size     = 
| image	=	A Devil with Women FilmPoster.jpeg
| border         = 
| alt            = 
| caption        = 
| director       = Irving Cummings
| producer       = George E. Middleton Henry Johnson Dudley Nichols
| screenplay     = 
| story          = 
| narrator       = 
| starring       = Victor McLaglen Mona Maris Humphrey Bogart
| music          = 
| cinematography = Al Brick Arthur L. Todd Jack Murray
| studio         = 
| distributor    = 
| released       =  
| runtime        = 64 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
A Devil with Women is a 1930 film starring Victor McLaglen, Mona Maris, and Humphrey Bogart, and directed by Irving Cummings.  Set in a Central American country, adventurer McLaglen and sidekick Bogart find themselves in a fierce competition for a luscious young womans attentions.  Notable for being among Bogarts earliest large film roles.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 