Pyaasa (2002 film)
{{Infobox Film
| name           = Pyaasa
| image          = pyaasa1.jpg
| image_size     = 
| director       = A. Muthu
| producer       = Ramesh Sharma
| writer         = Sanjeev Duggal
| narrator       = 
| starring       = Yukta Mookhey Aftab Shivdasani Zulfi Syed Govind Namdeo Saadhika
| music          = Sanjeev Darshan
| Lyricist       = Pravin Bhardwaj, Dev Kohli    Harmeet Singh
| editing        = 
| distributor    = 
| released       = October 11, 2002
| runtime        = 145 min.
| country        = India Hindi
| budget         = 
}}
Pyaasa  (Eternal Thirst) is a 2002 Hindi film directed by A. Muthu. The film tells the story of Suraj (Aftab Shivdasani), a young man, full of dreams, aspirations, desire, but no money. He is desperate for money. In walks, the anchor of his life, Sheetal Oberoi (Yukta Mookhey in her first major leading role in Hindi cinema), boss of a Mega Corporate company. Who has with her only money... money... money...      

==Plot==
Pyaasa is a love triangle with revenge as its base. Suraj (Aftab Shivdasani) aspires to be a millionaire some day, but all hopes of making it big are squashed by his father, (Govind Namdeo) and uncle (Anang Desai), who dont approve of selling their ancestorial land. Sheetal (Yukta Mookhey), a tycoon, who hires Suraj to work for her business empire. Prem (Zulfi Syed) is Surajs cousin and an accomplished businessman, who Suraj detests. What Suraj does not know is that Sheetal is just using him as a pawn to exact revenge from Prem and his father, Anang Desai. How Suraj faces up to Sheetal and thwarts her motives forms the rest of the story. 

==Box-Office==
Pyaasa was one of the biggest flop at the box office 2002.

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Milti Hai Jhukti Hai"
| Udit Narayan, Alka Yagnik
|-
| 2
| "Tere Pyar Ka Chhaya Nasha"
| Adnan Sami, Sunidhi Chauhan
|-
| 3
| "Milte Milte Sanam"
| Udit Narayan
|-
| 4
| "Soni Roop Di"
| Sardool Sikander
|-
| 5
| "Na Jaane Mera Kya Hoga"
| Sonu Nigam
|-
| 6
| "Tere Pyar Ka Nasha"
| Abhijeet Bhattacharya|Abhijeet, Sunidhi Chauhan
|-
| 7
| "Usko Pata Hai"
| Narendra Bedi 
|-
| 8
| "Aankhon Mein Leke Pyar"
| Udit Narayan, Karsan Sanghita, Kavita Krishnamurthy
|-
| 9
| "Is Mohabbat Ke Siva"
| Sonu Nigam, Alka Yagnik
|}

==References==
 

==External links==
*  
*  
*  

 
 
 

 