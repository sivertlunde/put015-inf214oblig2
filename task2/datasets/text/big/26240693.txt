Ikarie XB-1
 
{{Infobox Film
| name           = Ikarie XB-1
| image          = Ikarie_DVD.jpg
| caption        = Original Czech poster
| production     = Filmové Studio Barrandov
| director       = Jindřich Polák
| screenplay     = Pavel Juráček Jindřich Polák
| based on       =  
| starring       = Zdeněk Štěpánek Radovan Lukavský František Smolík Otto Lackovič Jozef Adamovič
| music          = Zdeněk Liška
| cinematography = Jan Kališ
| editing        = Josef Dobřichovský
| distributor    = American International Pictures (USA)
| released       = Sept 1964 (USA)
| runtime        = 86 min.
| country        = Czechoslovakia
| language       = Czech
|}} Czechoslovak science English for release in the USA, where it is known by its alternate title, Voyage to the End of the Universe.

== Synopsis == relativity mean that 15 years will have elapsed on Earth by the time they reach their destination. During the flight the 40-strong multinational crew must adjust to life in space, as well as dealing with various hazards they encounter, including a derelict 20th century spaceship armed with nuclear weapons, a deadly radioactive "dark star" and the mental breakdown of one of the crew, who threatens to destroy the spacecraft.

== Production ==
Ikarie XB-1 was a hit at the 1963 Trieste Science Fiction Film Festival  and it is now widely regarded as one of the best Eastern Bloc science fiction films of the era, boasting impressive  production design, above-average special effects, a strong ensemble cast and an intelligent screenplay (although much of the subtlety of the original is lost in the English-language version).

While it shows some influence from earlier American ventures such as   (1968) and it is believed to have been one of the many space genre films that Kubrick screened while researching 2001. 

Like several other high-quality Eastern Bloc sci-fi films of the era — e.g. the Soviet film Planeta Bur (Planet of Storms), aka Voyage to the Prehistoric Planet — Ikarie XB-1 is best known internationally through an edited and English-dubbed version which was given a limited theatrical release in the USA in 1964 by American International Pictures. The AIP version also occasionally screened on television in the USA and other countries at various times over the ensuing years but, apart from its screening at Trieste, the original Czech version was rarely seen in the West until its release on DVD in 2005.

== Changes in the English-language version  ==
AIP made numerous alterations for the English-language version of the film, which it retitled Voyage to the End of the Universe. Almost ten minutes of footage was cut, the names of the cast and staff in the opening credits were Anglicized, and the ships destination was renamed "The Green Planet". 

However, the biggest change was AIPs recut of the closing scene, which created an entirely different ending from the original. In the Czech version, as the Ikarie approaches its destination its viewscreen shows the clouds around the White Planet parting to reveal a densely populated and industrialized planet surface. For the English version AIP excised the last few seconds and substituted stock aerial footage of view of southern Manhattan and the Statue of Liberty. According to one reviewer, Glenn Erickson, AIPs edits and script changes were intended to create a gimmicky "surprise" ending, revealing that the Ikarie and its crew have come from an alien world and that the "Green Planet" is in fact Earth.  

Contrary to some claims that the film was made in colour and reduced to monochrome for U.S. release, the original film was shot in black-and-white.

== Release ==
In 2005 Filmexport Home Video released a DVD of the original Czech version of the film with English subtitles and presented in its original Panavision 2.35:1 anamorphic widescreen aspect ratio. The DVD included opening credits from the US version and two scenes as a bonus material to show the differences.
In 2013 UK company Second Run released a DVD of the original Czech version with English subtitles in a same transfer.

== References ==
 

== External links ==
*  
*  
*  
*   at Trailers from Hell
 
 
 
 
 
 
 
 
 
 
 
 
 