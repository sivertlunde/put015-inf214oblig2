The Mudlark
{{Infobox film
| name           = The Mudlark
| image          = 92mudlark.jpg
| caption        = Original film poster
| director       = Jean Negulesco
| producer       = Nunnally Johnson
| writer         = Theodore Bonnet (novel) Nunnally Johnson
| starring       = Irene Dunne Alec Guinness Andrew Ray Beatrice Campbell Finlay Currie
| music          = William Alwyn
| cinematography = Georges Périnal
| editing        = Thelma Connell
| distributor    = 20th Century Fox
| released       =        
| runtime        = 99 minutes
| country        = United Kingdom United States
| language       = English
| budget         =  
| gross = $1 million (US rentals) 
}} Britain by Prince Albert. It was directed by Jean Negulesco, written and produced by Nunnally Johnson and based on the 1949 novel of the same name by American artillery sergeant and newspaperman Theodore Bonnet (1908–1983). It starred Irene Dunne, Alec Guinness and Andrew Ray.

"Mudlarks" were street children who survived by scavenging and selling what they could find on the banks of the River Thames. The film was a hit in Britain and made an overnight star of Andrew Ray, who played the title character.

==Plot==
A young street urchin (Andrew Ray), half-starved and homeless, finds a cameo containing the likeness of Queen Victoria (Irene Dunne). Not recognizing her, he is told that she is the "mother of all England". Taking the remark literally, he journeys to Windsor Castle to see her.

When he is caught by the palace guards, the boy is mistakenly thought to be part of an assassination plot against the Queen. Prime Minister Benjamin Disraeli (Alec Guinness) realises that the boy is innocent and pleads for him in Parliament of the United Kingdom|Parliament, delivering a speech that indirectly criticizes the Queen for withdrawing from public life. The Queen is infuriated by the speech, but she is genuinely moved upon meeting the boy for the first time, and once again enters public life.

==Cast==
*Irene Dunne as Queen Victoria
*Alec Guinness as Benjamin Disraeli
*Andrew Ray as Wheeler
*Beatrice Campbell as Lady Emily Prior John Brown, Queen Victorias servant Anthony Steel as Lieutenant Charles McHatten
*Raymond Lovell as Sergeant Footman Naseby
*Marjorie Fielding as Lady Margaret Prior
*Constance Smith as Kate Noonan
*Edward Rigby as The Watchman
*Ernest Clark as Hammond
*Wilfrid Hyde-White as Tucker

==Award nomination== Edward Stevenson and Margaret Furse).

==Historical inspiration==
In the semi-historical novel upon which this film was based, the story of the young mudlark Wheeler (aged ten in the film, but seven in the book) sneaking into Windsor Castle in 1875 to see Queen Victoria was inspired by a 14 December 1838 incident involving "the boy Jones", as newspapers called him. A boy was discovered in Buckingham Palace. At first mistaken for a chimney-sweep, until he ran off across the lawns, he was apprehended by a policeman. (Sweeping of chimneys by boys was not made illegal until 1840.)

The boy gave his name as Edward Cotton and said that he had been born in the palace; later he claimed to have been living there for only a year, after having come from Hertfordshire. In fact, his name was Edward Jones, the 14-year-old son of a tailor who lived in Bell Yard, some 300 yards distant from the palace. The tailor had turned him out for ill conduct. He had been employed as an errand boy by a carver and gilder in Coventry Street, but had disappeared three days previous to his arrest after saying that he wanted to see the palaces Grand Staircase to sketch it and also to see the Queen (who was actually then at Windsor).

At the Westminster Sessions on 28 December, the magistrates court jury found him not guilty of theft and he was taken back by his employer, who described him as an extremely good lad. (Some details were taken from contemporary reports in the London newspapers The Times, The Sun and The Standard.) The full history of the intruder has been revealed in Jan Bondesons book Queen Victorias Stalker: The Strange Story of the Boy Jones (2010).

==The novel==
The Mudlark refers to the seven-year-old waif from the East End but it also could be said to figuratively refer to the British PM Disraeli who came from humble beginnings, and whose life is described in some detail. The novel also manages to give a personality to the Fenians and the Irish question, it includes two love affairs as well as the slack cut by Queen Victoria for her Scottish gillie Brown and the relationship between Victoria and her subjects. It also includes the beginnings of enlightened social reform through parliamentary action, and the further extension of British world influence and of Britains imperial power in India. And for added humour, some bureaucratic overlap and exaggerated suspicions. Even though the novel was written in 1949, the allusions to certain aspects of London or Britain in the late 1870s are sometimes a challenge for a 21st-century reader.

==References==
 

==External links==
* 
* 
* 
*  

 

 
 
 
 
 
 
 
 
 
 
 