Sruthi (film)
{{Infobox film
| name           = Sruthi
| image          =
| caption        = Mohan
| producer       = MN Murali Sivan Kunnampilly Mohan (dialogues) Mohan
| Mukesh Thilakan KPAC Lalitha Nedumudi Venu Johnson
| cinematography = Saroj Padi
| editing        = G Murali
| studio         = Thushara Films
| distributor    = Thushara Films
| released       =  
| country        = India Malayalam
}}
 1987 Cinema Indian Malayalam Malayalam film, Mohan and produced by MN Murali and Sivan Kunnampilly. The film stars Mukesh (actor)|Mukesh, Thilakan, KPAC Lalitha and Nedumudi Venu in lead roles. The film had musical score by Johnson (composer)|Johnson.   

==Cast== Mukesh
*Thilakan
*KPAC Lalitha
*Nedumudi Venu Geetha
*Jagannathan Jagannathan

==Soundtrack== Johnson and lyrics was written by Balachandran Chullikkad. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Cheekithirukiya || Unni Menon, Lathika || Balachandran Chullikkad || 
|-
| 2 || Leelaaravindam || KS Chithra || Balachandran Chullikkad || 
|-
| 3 || Nimishamaam || K. J. Yesudas || Balachandran Chullikkad || 
|-
| 4 || Onam Vannu (Bit) || Unni Menon, Chorus, Lathika || Balachandran Chullikkad || 
|}

==References==
 

==External links==
*  

 
 
 

 