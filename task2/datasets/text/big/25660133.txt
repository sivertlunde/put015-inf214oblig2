The Caller (2011 film)
{{Infobox film 
 | name = The Caller
 | image          = Thecaller2010.JPG
 | alt            = 
 | caption        = Theatrical Poster
 | writer =  Sergio Casci
 | starring = Rachelle Lefevre Stephen Moyer
 | director = Matthew Parkhill
 | producer = Amina Dasmal Robin C. Fox Piers Tempest Luillo Ruiz The Pimienta Film Company 
 | studio = 
 | distributor = Bankside Films
 | released =  
 | runtime = 92 minutes
 | country = Puerto Rico
 | language = English
 | music = 
 | budget = 
 | gross =
 }}

The Caller is a supernatural thriller directed by Matthew Parkhill and written by Sergio Casci, starring Rachelle Lefevre, Stephen Moyer and Lorna Raver. The movie was filmed entirely in Puerto Rico. The Gala Premiere of the movie was on August 23, 2011 at Metro Cinema in Puerto Rico.   

==Plot==
When troubled divorcee Mary Kee sets up home in her new apartment, she stumbles across an old telephone which she quickly falls in love with. Struck by its antique charm, she gives it pride of place in her home. Before long, Mary begins to receive strange phone calls from a mysterious, unknown caller. Over time, she discovers that the caller is a woman named Rose, and the two strike up an unlikely friendship. However, when Rose claims to be calling from the past, Mary begins to question her new friends motives.

As Roses phone calls become ever more disturbing, Marys sense of terror escalates. Feeling haunted in her own home, she cuts all contact with Rose. Enraged by Marys betrayal, Rose threatens to exact her terrible revenge. Not on Mary in the present but on Mary as a child in the past. Mary finally realizes that she will have to kill Rose in order to save herself. But how can she kill someone living in the past?

She fails. Rose pours hot grease on the young Mary Kee causing the adult Mary Kee to wretch with her new burn scars.  After this Mary Kee tries to kill Rose by inviting her to a birthday party at a bowling alley she knew would catch fire and burn, killing all in it.  Rose misses the bus and the plan fails, so Rose has the young Mary talk to the adult Mary on a day that the old Rose attempted to barge in and kill her.  Mary coaches the young Mary to break a mirror and use the shards to kill Rose, thus ending the attack and the calls, but Mary’s abusive husband reappears.  In response, Mary kills her husband and puts his body in the bricked-in pantry that Rose had used to store bodies.

==Cast==
* Rachelle Lefevre as Mary Kee
* Stephen Moyer as John Guidi
* Gladys Rodriguez as Mrs. Guidi 
* Lorna Raver as Rose
* Luis Guzmán as George
* Ed Quinn as Steven
* Marisé Alvarez as Nurse
* Alfredo De Quesada as Attorney Davies
* Cordelia González as Judge
* Aris Mejias as Young Woman
* Brian Tester as Attorney Kirkby
* Grace Connelly as Dr. Hain
* Abimael Linares as Young Man
* Wilfred Perez as Young Guy
* David Lee Rittenhouse as a real estate guy

==Production==
Brittany Murphy was originally cast as Mary Kee, but left the production and was replaced by Rachelle Lefevre. 

==Awards==
{| class="wikitable"
|- style="background:#b0c4de; text-align:center;"
! Award
! Year
! Category
! Nominee
! Result
! Ref
|-
|Neuchâtel International Fantastic Film Festival
| align="center"| 2011
| Narcisse Award for Best Feature Film Matthew Parkhill
| 
|   
|-
|}

==References==
 

==External links==
*  
*  at subtitledonline.com

 
 
 
 
 
 