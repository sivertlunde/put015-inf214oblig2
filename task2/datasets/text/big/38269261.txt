Abhayam Thedi
{{Infobox film
| name           = Abhayam Thedi
| image          =
| caption        =
| director       = IV Sasi
| producer       =
| writer         = MT Vasudevan Nair
| screenplay     = MT Vasudevan Nair
| starring       = Sukumari Mohanlal Shobhana Thilakan Shyam
| cinematography = Vasanth Kumar
| editing        = K Narayanan
| studio         = Classic Arts
| distributor    = Classic Arts
| released       =  
| country        = India Malayalam
}}
 1986 Cinema Indian Malayalam Malayalam film, directed by IV Sasi. The film stars Sukumari, Mohanlal, Shobhana and Thilakan in lead roles. The film had musical score by Shyam (composer)|Shyam.   

==Plot==
The film tells the story of a girl who returns to her ancestral home after the death of her parents and how she is being treated there.

==Cast==
 
*Sukumari
*Mohanlal as Appu / Appettan
*Shobhana as Miranda / Meera
*Thilakan as Appooppan
*KPAC Lalitha Ashokan as Anil Rohini as Vasanthi Baby Anju
*Janardanan
*Kuthiravattam Pappu
*Y Vijaya
*Ramachandran Raveendran
 

==Soundtrack== Shyam and lyrics was written by S Ramesan Nair. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Kunnathoru kunniludichu || Unni Menon, Lathika || S Ramesan Nair || 
|-
| 2 || Maanathu Vethaykkana || Unni Menon, Lathika || S Ramesan Nair || 
|-
| 3 || Medakkonnaykku || Krishnachandran, Lathika || S Ramesan Nair || 
|-
| 4 || Thaathintha they || P Jayachandran || S Ramesan Nair || 
|}

==References==
 

==External links==
*  

 
 
 
 
 
 
 