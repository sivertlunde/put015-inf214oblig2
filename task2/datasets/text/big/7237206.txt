Roja Kootam
{{Infobox film
| name           = Roja Kootam
| image          = Sasi
| Sasi
| Srikanth Bhumika Vivek Raadhika Raghuvaran Jai Akash Vijay Adhiraj
| producer       = Viswanathan Ravichandran
| music          = Bharathwaj
| cinematography = M. V. Panneerselvam
| editing        = Peter Bhabiyaa Aascar Film Pvt. Ltd
| released       =  
| runtime        =
| language       = Tamil
| country        = India
}}
 Srikanth and Bhumika Chawla as the lead pair. The films score and soundtrack are composed by Bharathwaj. This was Srikanth (actor)|Srikanths debut movie.  The film was remade in Kannada as Manasina Maathu with Ajay Rao and Aindrita Ray. 

==Plot== job received through Srikanths parents. Before he leaves, he tells Srikanth that Bhoomika and himself are in love and asks Srikanth to look after his lover until he is back. Srikanth swallows his love secret for the sake of friendship. Meanwhile Rekha arranges for Bhumika to get married to Vijay Adhiraj for his wealth. At this juncture Bhumika and Srikanth lie that they are already married. They both leave their parents and Srikanth starts earning to support Bhumika. Finishing his job assignment Sriram returns only to give yet another family responsibility as an excuse. He apologizes for not being able to marry Bhumika as he is a tight corner to marry somebody else for the sake of his sisters marriage.Later Bhumika leave to Mumbai after a job opportunity. Srikanths parent comes to know the truth that he and Bhumika are not married. They accept them. Radhika know Bhumikai a the girl srikanth loved but he would not accept it since Bhumika might think wrong of him.Later in the train station things turn plates and Bhumika comes to know that she is the girl he loved and accepts him.  

==Cast== Srikanth
*Bhumika Chawla
*Raghuvaran
*Raadhika Sarathkumar
*Vivek as Auto Aarumugam
*Suvarna Mathew as Kiran
*Jai Akash Rekha
*Raghava Lawrence in the song Suppamma
*Mumtaj in the song Suppamma

==Production== director Sasis Tamil film after Badri (2001 film)|Badri, in which she co-starred with Vijay (actor)|Vijay. This was Srikanths first film.

==Soundtrack==
{{Infobox album |  
| Name = Roja Kootam
| Type = soundtrack
| Artist = Bharadwaj
| Cover = 
| Released = 2002
| Recorded =  Feature film soundtrack
| Length = 
| Label = Star Music
| Producer = Bharadwaj
| Reviews =
| Last album  = Aadanthe Ado Type (2003)
| This album = Pudhiya Geethai (2003)
| Next album = Thennavan (2003)
}}

The soundtrack was composed by Bharadwaj. The songs were received well by the audience and were chartbusters.
{| class="wikitable"
|-
! SL. No. !! Song Title !! Singer(s) !! Duration (mins)
|-
| 1. || Anna Saalaiyil || Karthik || 5:34
|-
| 2. || Apple Penne Neeyaaro || Srinivas || 5:32
|- Tippu || 5:14
|- Hariharan || 4:57
|-
| 5. || Putham Pudhu Rojaave || Unnikrishnan || 4:55
|-
| 6. || Subbammaa || Malgudi Subha, Manicka Vinayagam || 5:29
|-
| 7. || Uyir Konda Rojaave || Bharadwaj || 5:08
|-
|}

==Awards==
*ITFA Best New Actor Award
*Best Music Director Award (2002) 

==References==
 

==External links==
*  

 

 
 
 
 
 
 