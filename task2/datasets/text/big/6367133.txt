Behind the Mask (2006 film)
{{Infobox Film
| name = Behind the Mask: The Story Of The People Who Risk Everything To Save Animals
| image = BehindTheMask.jpg
| director = Shannon Keith Kevin Kjonaas Ronnie Lee Keith Mann Ingrid Newkirk Jerry Vlasak Paul Watson Robin Webb
| producer = Shannon Keith
| co-producer = Ramy Hassan
| distributor = Uncaged Films and ARME (Animal Rescue, Media & Education)
| editor = Sandra Mohr
| released = March 21, 2006
| runtime = 72 min.
| rating = R (MPAA)
| language = English
| budget = unknown
}}
Behind the Mask: The Story Of The People Who Risk Everything To Save Animals is a 2006 documentary film about the Animal Liberation Front (ALF). It took three years of filming, interviewing, and editing to complete. The movie was created by animal-rights lawyer Shannon Keith, who owns Uncaged Films and ARME (Animal Rescue, Media & Education). The documentary was produced in response to a perceived bias in the mainstream media against the animal rights movement. 

== About ==
The film is about animal rights activists who break into laboratories and other facilities to provide previously unseen footage of the way animals are used.

It includes well-known names within the  . 

The UK première was held on September 7, 2006 at the  . 

==Awards==

The film has been shown at film festivals, as well as other showings,  and has won the following awards:   
* Documentary of the Year, VegNews, 2006
* Best Documentary Feature, Other Venice Film Festival 2007
* Best Documentary Feature, Independent Features Film Festival 2007
* Best Documentary Feature, Santa Clarita Valley Film Festival 2007

The documentary was also in the Official Selection at Santa Cruz Film Festival 2007 and ReelHeART International Film Festival 2007. 

==See also==
*Animal liberation movement
*Animal Liberation Front (ALF)
*Animal Liberation Front Supporters Group (ALFSG)
*Animal Liberation Press Office
*Keith Mann

==References==
 

==External links==
* 
* , 2 minute video on YouTube, 2006.
*  (ALF)
*  (ALFSG)
* 
*  

 
 

 
 
 
 
 
 
 
 
 
 