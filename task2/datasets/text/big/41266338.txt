Sa Ngalan ng Ama, Ina, at mga Anak
{{Infobox film
| name           = Sa Ngalan ng Ama, Ina, at mga Anak
| image          =File:Sa_Ngalan_ng_Ama,_Ina,_at_mga_Anak_movie_poster.jpg
| caption        = Theatrical movie poster
| director       = Jon Villarin
| producer       =
| writer         =
| starring       =  
| music          =
| cinematography =
| screenplay     =
| editing        =
| studio         = RCP Productions
| distributor    = Star Cinema
| released       =  
| runtime        =
| country        = Philippines
| language       = Filipino
| budget         =
| gross          = P 1,280,012.38
}}

Sa Ngalan ng Ama, Ina, at mga Anak (  action film featuring the Padilla family. The film was on the shortlist to show at the 2013 Metro Manila Film Festival. 

==Cast==
*Robin Padilla as Ongkoy
*Daniel Padilla as Nato Mariel Rodriguez-Padilla as Indah
*Kylie Padilla as Ka Anna
*RJ Padilla as Ardut
*Bela Padilla as Damian
*Matt Padilla as Aldong
*Rommel Padilla as Old Nato
*Gideon Padilla as Dagul
*Queenie Padilla as Celia
*Royette Padilla

==Special Participation==
*Christopher de Leon as Ka Romeo
*Dina Bonnevie as Old Indah
*Karla Estrada as Erning Wife
*Pen Medina as Erning
*Lito Pimentel as Major Calaang
*Dennis Padilla as Wason
*Bugoy Cariño as Young Ongkoy
*Chacha Cañete as Young Indah
*Jao Mapa as Ongkoys Assassin 1
*Christian Vasquez as Ongkoys Assassin 2
*Minco Fabregas as Ongkoys Assassin 3
*Aljur Abrenica as Ka Dorio
*Ronnie Lazaro as Amadong Buwang
*Sylvia Sanchez as Ka Julieta
*Ketchup Eusebio as NPA Rebels
*John Wayne Sace as Ka Jose
*Joko Diaz as PNP Leader

==References==
 

 
 
 
 
 


 