The Wrong Trousers
 
{{Infobox film
| name           = Wallace and Gromit in The Wrong Trousers
| image          = WallaceandGromitinTheWrongTrousers.jpg
| caption        = Promotional poster
| director       = Nick Park
| producer       = Peter Lord David Sproxton Bob Baker
| starring       = Peter Sallis
| music          = Julian Nott
| cinematography =
| editing        =
| studio         = Aardman Animations
| distributor    = BBC
| released       =  
| runtime        = 30 minutes
| country        = United Kingdom
| language       = English
| budget         = £650,000 
}}
The Wrong Trousers is a 1993 stop-motion|stop-motion animated short film directed by Nick Park at Aardman Animations, featuring his characters Wallace and Gromit. It was his second half-hour short featuring the eccentric inventor Wallace (voiced by Peter Sallis) and his silent but intelligent dog Gromit, following  1989s A Grand Day Out, and preceding 1995s A Close Shave.

As in A Grand Day Out, the 30-minute film uses sight gags and exaggerated physical comedy and quiet moments, as well as a few subtle film parodies. The film premiered in the United States on 17 December 1993 and the United Kingdom on 26 December 1993.  It won the 1993 Academy Award for Best Animated Short Film.

It was highly successful and inspired a charity fundraising day, known as Wrong Trousers Day, one of several events organised by the charity Wallace and Gromits Childrens Charity. Wrong Trousers Day falls on the last Friday in June every year. During the day, participants wear the wrong trousers to work or school etc. and donate a pound to help sick children in hospitals and hospices. This is the second Wallace & Gromit video to have an introduction. The first was A Grand Day Out.

==Plot== Wallace is greeted with a large pile of bills. Wallace remembers Gromits birthday and presents Gromit with a somewhat unwelcome gift of a spiked dog collar, and a second present of a pair of ex-NASA robotic "Techno Trousers", acquired by Wallace to alleviate the burden of taking Gromit for walks. While Gromit is out on a "walk", Wallace realises they are in financial difficulty and decides to let the spare bedroom out. He is answered by an inscrutable-looking penguin named Feathers McGraw. The penguin comes to stay at the house, pushing Gromit out of his comfortable bedroom and keeping him awake at night with loud music. On the other hand, Wallace takes a liking to him. Feathers also takes an interest in the Techno Trousers after seeing Gromit use their suction feet to walk on the ceiling.

Upset that Feathers has intruded on his relationship with his master, Gromit leaves home. After watching him leave, Feathers gets to work on modifying the Techno Trousers for his own use, removing the controls on the trousers and adapting them into a remote control. The next morning, Gromit hunts for suitable lodgings; at a cafe he notices a wanted poster offering the £1000 reward for the capture of a suspiciously familiar "chicken" - actually a criminal penguin who disguises himself by wearing a rubber glove on his head. Meanwhile, Wallaces normal morning routine is interrupted when his expected trousers are replaced with the modified Techno Trousers. Trapped inside the "wrong trousers", Wallace is marched out of the house and sent running around town on an extended test run, unaware that Feathers is controlling them. Gromit witnesses this spectacle and later, spies on Feathers as he measures up the exterior of the city museum. He returns home and uncovers plans to steal a giant diamond from the museum, using the trousers and Wallace as tools. However, Gromit is too late to foil the plan. Feathers returns and Gromit hides inside Wallaces bed, where he sees Feathers arrive in the "chicken disguise." Realisation dawns on Gromit: Feathers is a wanted thief who had been stealing from all over the country.

In a deep sleep after the days misadventures, Wallace is unwittingly brought into the robbery by Feathers and marched out of the house to the museum. Feathers and Wallace climb the museum wall and the penguin waits on a window sill as Wallace enters the building through a roof air vent and walks across the ceiling to the room with the diamond, avoiding the laser alarm system. The helmet Wallace is dressed in contains a remote-controlled claw which hooks the diamond. A ceiling tile dislodges and the claw swings into the laser, activating the alarm and waking Wallace up. Feathers marches Wallace out and takes him back to West Wallaby Street. The penguin obtains the diamond and reveals himself to be the lodger. When Wallace protests, he traps Wallace in a wardrobe.

As Feathers makes for the door with the diamond, Gromit confronts Feathers with a rolling pin. Feathers pulls out a revolver and forces Gromit into the wardrobe with Wallace, locking both of them inside. Being an expert with electronics, Gromit manages to break into the trousers circuits to make them march and break open the wardrobe. There follows a fast chase aboard a train set, as Gromit tries to prevent Feathers from escaping with the diamond. Wallaces attempts to assist are mostly unsuccessful, though he removes Feathers revolver and frees himself from the trousers. After Feathers train collides with the trousers, he is captured and handed in to the police station and imprisoned in a zoo. For capturing Feathers, Wallace and Gromit are given the substantial reward money, which pays off their debts, and they both enjoy resting with tea and cheese and Cracker (food)|crackers. Meanwhile, the trousers, unceremoniously consigned to the dustbin, walk off by themselves into the sunset.

==Cast==
Peter Sallis as Wallace provides the only voice acting in the film, all other characters are silent.

==Soundtrack alterations== DreamWorks Home Happy Talk" South Pacific and "(How Much Is) That Doggie in the Window?", along with Wallaces humming of it the subsequent morning. The VHS was released in 1994 by CBS/Fox Video with a trailer for A Grand Day Out at the end.

In addition, Gromits TV at breakfast no longer plays the Open University theme. In the 2009 HD version of the film, Julian Notts soundtrack appears to have been remixed or rerecorded. The Blu-ray release also does not include the original music. However the original soundtrack can still be heard in the background of the commentary track of the DVD release. The original soundtrack can also be heard in The Wrong Trousers when viewed in other languages and in English when viewed with other language subtitles. 

==Reception==
The Wrong Trousers was voted as the 18th best British Television Show by the British Film Institute.  It has a unanimously positive score on Rotten Tomatoes with 24 reviews, 100% positive and an average score of 9.1/10. The film was awarded the Grand Prix at the Tampere Film Festival in 1994.

The Wrong Trousers won the Academy Award for Best Animated Short Film.

==In other works==
The character of Feathers McGraw has made various cameo appearances in other works by Aardman.  In follow-up short A Close Shave, the words Feathers was here can be seen written on the wall of Gromits jail cell, and in the series Cracking Contraptions a penguin resembling McGraw appears briefly on a television show named When Penguins Turn.  McGraw also briefly appears in the finale of The Curse of the Were-Rabbit, sitting in a pot on the roof of Tottington Manor.  In A Matter of Loaf and Death, he is featured both on a wanted poster on the wall of a zoo (next to a long rope over the wall, implying his escape) and briefly in the background of a shot in which Piella lands in an alligator enclosure.

McGraw also appeared as the main villain in the spin-off video game Wallace & Gromit in Project Zoo.

==See also==

* List of films featuring powered exoskeletons
==References==
 
 

== External links ==
 
*  
*  

 
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 