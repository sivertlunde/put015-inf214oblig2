Chasing Rainbows
 
{{Infobox film
| name           = Chasing Rainbows
| image          = Chasing Rainbows1930.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Charles Reisner
| producer       = 
| based on       =  
| writer         = Al Boasberg Wells Root Kenyon Nicholson Charles Reisner Charles King
| music          = Milton Ager Jack Yellen
| cinematography = Ira H. Morgan    
| editing        = George Hively
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English 
| budget         = 
}}
 romantic musical Charles King, and released by Metro-Goldwyn-Mayer.  The supporting cast features Jack Benny, Marie Dressler, and Polly Moran.  This was Jack Bennys first film role.

The movie introduced the song "Happy Days Are Here Again".

==Plot summary==
 
Eddie Rock (Jack Benny) is manager of a group of performers in the show "Goodbye Broadway".

==Cast==
* Bessie Love as Carlie Semour Charles King as Terry Fay
* Jack Benny as Eddie Rock
* George K. Arthur as Lester
* Polly Moran as Polly
* Gwen Lee as Peggy
* Nita Martan as Daphne Wayne Eddie Phillips as Don Cordova
* Marie Dressler as Bonnie
* Youcca Troubetzkov as Lanning

Marie Dressler became the studios most valuable star later that same year as a result of Min and Bill with Wallace Beery, only to die of cancer four years later.

== Film preservation == lost (originally Vault 7 fire), but the rest of the movie survives. It has been issued on DVD in the Warner Archive Collection. The lost sequences, with only the sound still existent on Vitaphone disks, include the "Happy Days Are Here Again" performance. 

==See also==
* List of American films of 1930
* List of early color feature films

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 


 
 