Thank God It's Friday
 
{{Infobox film
| name           = Thank God Its Friday
| image          = Thank God Its Friday (poster).jpg
| caption        = Theatrical release poster
| director       = Robert Klane Lauren Shuler (associate producer)
| writer         = Armyan Bernstein
| starring       = Jeff Goldblum Debra Winger
| music          = Giorgio Moroder Paul Jabara (song "Last Dance")
| cinematography = James Crabe
| editing        = Richard Halsey Motown Productions
| distributor    = Columbia Pictures
| released       =  
| runtime        = 89 minutes
| country        = United States
| language       = English
| budget         = $2.2 million
| gross          =
}} Motown Productions Last Dance" which won the Academy Award for Best Song in 1978. The film features an early performance by Jeff Goldblum and the first major screen appearance by Debra Winger.

==Summary==
Thank God Its Friday tells several intertwining stories of the patrons and staff of the fictional Los Angeles club The Zoo. These include:

* Tony Di Marco (Jeff Goldblum) – owner of The Zoo. Lecherous and promiscuous, hes inordinately fond of his car.
* Bobby Speed (Ray Vitte) – the clubs DJ, whos broadcasting his first live show from the club. KISS concert tickets.
* Carl (Paul Jabara) and Ken (John Friedrich) – hopelessly near-sighted shlub looking for a casual liaison, and his friend looking for a girlfriend.
* Dave (Mark Lonow) and Sue (Andrea Howard) – a young married couple celebrating their anniversary. Marya Small) – dental hygienist by day, drugged-out disco freak by night.
* Jennifer (Debra Winger) and Maddy (Robin Menken) – the new girl in town taken to the disco by her know-it-all friend whos not as sophisticated as she thinks she is.
* Nicole Sims (Donna Summer) – an aspiring disco singer.
* Marv Gomez (Chick Vennera) – a self-described "leatherman" who lives to dance.
* Malcolm Floyd (DeWayne Jessie) – the roadie for The Commodores, responsible for delivering their instruments to the club by midnight.
* Gus (Chuck Sacci) and Shirley (Hilary Beane) – mismatched computer blind daters.

Sue insists her uptight accountant husband Dave take her to the disco. On a bet with Bobby, Tony tries to pick up Sue. Dave is drugged and renamed "Babbakazoo" by Jackie, and makes a fool of himself. Carl and Ken are repeatedly thwarted in their attempts to meet girls. Frannie and Jeannie trick Marv into helping them sneak into the disco after several failed attempts at gaining entry. Jennifer tries to meet a guy, but Maddy vetoes each of the guys Jennifer is attracted to. Nicole repeatedly attempts to slip into the DJ booth to get Bobby to play her single. Crude garbage collector Gus is horrified that the dating service has matched him with a prim college educated woman, and one who is taller than he is. Floyd gets stopped repeatedly by the police on suspicion of stealing The Commodores instruments. Marv teaches the uptight Ken how to dance.

Maddy ditches Jennifer to attend a hot tub party (with the same sleazy guys who came on to Jennifer). Gus and Shirley decide to give it a try. Carl finally meets a girl, but is locked in the stairwell before they can leave together. Floyd makes it to the club in time for the Commodores to play but before they go on, Nicole sneaks up on stage and scores a huge triumph singing "Last Dance". Frannie, after tricking Marvs dance partner into the locked stairwell, enters the dance contest with Marv. Carl and Marvs dance partner hook up in the stairwell. Jennifer and Ken share a romantic dance, as do Nicole and Bobby. Dave comes down and Sue ditches Tony. Tonys parked car, having taken innumerable hits from pretty much every other characters car, falls apart in the parking lot. Marv and Frannie win the big dance contest. Deciding that the KISS concert is "kid stuff," Frannie and Jeannie, now self-proclaimed "disco queens," go with Marv to hit another disco for the 1 AM dance contest.

==Soundtrack==
{{Infobox album   
| Name        = Thank God Its Friday
| Type        = Soundtrack
| Artist      = Various Artists
| Cover       = Original Motion Picture Soundtrack - Thank God Its Friday.jpg
| Released    = 1978
| Recorded    = 1975–1978
| Genre       = Disco
| Length      =
| Label       = Casablanca Records (original release),  Rebound Records,  PolyGram,  Universal Music
| Producer    =
| Last album  =
| This album  =
| Next album  =
| Misc                = {{Singles
  | Name            = Thank God Its Friday
  | Type              = Soundtrack Thank God Its Friday
  | single 1 date = 1978 Last Dance
  | single 2 date = July 2, 1978
  | single 3         = Je taime... moi non plus
  | single 3 date = July 17, 1978
  }}
}}
{{Album ratings
| rev1 = Allmusic
| rev1Score =    
| noprose = yes
}}

The film contains many popular disco songs, with many key performers featured, including Donna Summer, Diana Ross, Thelma Houston, The Commodores. A triple album containing many of the tracks heard in the film was a commercial success.
 From Here Brick House", I Am What I Am."
 Last Dance", Academy Award Golden Globe US singles chart. The song was written by Paul Jabara, who the following year would go on to compose Summers duet with Barbra Streisand, "No More Tears (Enough Is Enough)". Jabara himself performed two of the songs on the Thank God Its Friday soundtrack, and appeared in the film as well.

===Chart positions===
{| class="wikitable sortable" border="1"
|-
! Chart (1978)
! Peak position
|-
 
|-
 
|-
 
|-
 
|- US  . Rovi Corporation. Accessed on August 10, 2013.  10
|- US R&B Albums (Billboard (magazine)|Billboard)  6
|- US Billboard Hot Dance Club Play    1
|}

==Album information==
The soundtrack album was originally issued as a 3 record set in 1978, of which the 3rd disc was a single side 12&nbsp;inch single of the 15:45 minutes Donna Summer, "Je taime... moi non plus" track. Upon its 1978 release, a promo set of separate 12" singles of every track was released to select DJs only. An edited CD came out in 1995 on the budget label Rebound Records. A digitally remastered version of the full soundtrack on a 2 disc set was released on PolyGram Records on March 25, 1997. The company that holds the rights to the album is as of 1998 the Universal Music Group.
 Last Dance", were especially recorded for the film.

Diana Ross "Lovin Livin and Givin" was remixed after the release of the soundtrack and used as the opening track on her 1978 album Ross (1978 album)|Ross. It was also released as a single in certain territories and has since been remixed and re-edited a number of times for inclusion on various hits packages issued by Motown/Universal Music.

The final part of   and 2003s  . The 12" single used the full-length 8:11 version. A live recording of the track was included on the album  . The 1979 mix can be found on Summers 1993 and 2005 compilations The Donna Summer Anthology and Gold (Donna Summer album)|Gold respectively.

An extended remix of Summers "With Your Love" was issued as a promo 12" single in 1978; a slightly shorter version of this can be found on the cd  . The 8 track cartridge and cassette both feature the full length version.

==Track listing==
;Side one
# " (Alec R. Costandinos) – 4:13
#*Producer: Alec R. Costandinos.
#Pattie Brooks: "After Dark" (Simon Soussan/Sabrina Soussan) – 7:50
#*Producer: Simon Soussan
#Donna Summer: "With Your Love" (Giorgio Moroder, Pete Bellotte, Donna Summer) – 3:58
#* Producers: Giorgio Moroder, Pete Bellotte
# " (Paul Jabara) – 8:11
#* Producers: Bob Esty, Giorgio Moroder
;Side two
#Paul Jabara: "Disco Queen" (Paul Jabara) – 3:45
#*Producers: Bob Esty, Paul Jabara
#Cameo (band)|Cameo: "Find My Way" (Johnny Melfi) – 4:56
#*Producer: Larry Blackmon
# , Milan Williams, Ronald LaPread, Thomas McClary, Walter "Clyde" Orange, William King) – 3:24
#*Producers: The Commodores, James Carmichael
#Wright Bros. Flying Machine:  "Leathermans Theme" (Arthur G. Wright) – 3:22
#*Producer: Arthur G. Wright
#Marathon: "I Wanna Dance" (Pete Bellotte, Thor Baldursson) – 5:58
#*Producer: Pete Bellotte
;Side three Joe Esposito) – 7:56
#*Producer: Arthur G. Wright
#Santa Esmeralda: "Sevilla Nights" (Jean-Manuel de Scarano, Nicolas Skorsky, Jean-Claude Petit) – 6:05
#*Producers: Jean-Manuel de Scarano, Nicolas Skorsky
#Love & Kisses: "Youre the Most Precious Thing in My Life" (Alec R. Costandinos) – 8:02
#*Producer: Alec R. Constandinos
;Side four
#D.C. LaRue: "Do You Want the Real Thing" (D.C. LaRue, Bob Esty) – 4:40
#*Producer: Bob Esty
#Paul Jabara: "Trapped in a Stairway" (Bob Esty, Paul Jabara) – 3:22
#*Producer: Paul Jabara, Bob Esty
#Natural Juices: "Floyds Theme" (Dick St. Nicklaus) – 2:57
#*Producer: Dick St. Nicklaus
#Diana Ross: "Lovin, Livin and Givin" (Kenneth Stover, Pam Davis) – 3:17 (CD releases: – 4:40, remixed version)
#*Producer: Hal Davis
#Thelma Houston: "Love Masterpiece"  (Art Posey, Josef Powell) – 4:01
#*Producer: Hal Davis Last Dance" (Paul Jabara) (Reprise) – 3:17
#* Producers: Bob Esty, Giorgio Moroder
;Side five
# " (Serge Gainsbourg) – 15:45
#* Producers: Giorgio Moroder, Pete Bellotte

==DVD release== Region 1 DVD on April 4, 2006.

==Critical response== Movie Guide, Last Dance). 

==Notes==
*The real nightclub used for the film was  s and the Cave, an ice cavern-themed room as seen in the film. The ice cavern-themed room also appears in the horror film Jennifer (1978 film)|Jennifer. Club owner Osko Karaghassian had a role as a bouncer in the film. Oskos nightclub was completely demolished by the early 1990s and was replaced by a large Loehmanns dress store.

== See also ==
* Saturday Night Fever (1977)
* Skatetown, U.S.A. (1979)
* Roller Boogie (1979)
* Cant Stop the Music (1980)
* Xanadu (film)|Xanadu (1980)
* Fame (1980 film)|Fame (1980)

== References ==
 

== External links ==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 