Love in the Time of Cholera (film)
{{Infobox film
| name           = Love in the Time of Cholera
| image          = Love cholera.jpg
| caption        = Theatrical release poster Mike Newell
| producer       = Scott Steindorff
| based on       =  
| screenplay     = Ronald Harwood
| starring       = Javier Bardem Giovanna Mezzogiorno Benjamin Bratt Angie Cepeda Catalina Sandino Moreno Hector Elizondo Liev Schreiber Fernanda Montenegro Laura Harring John Leguizamo  Antonio Pinto Shakira
| cinematography = Affonso Beato
| editing        = Mick Audsley
| distributor    = New Line Cinema  Stone Village Pictures (US)  20th Century Fox (Overseas)
| released       =  
| runtime        = 139 minutes
| country        = United States 
| language       = English
| budget         = $50 million
| gross          = $31,337,584
}} Mike Newell. novel of the same name by Gabriel García Márquez, it tells the story of a love triangle between Fermina Daza (played by Giovanna Mezzogiorno) and her two suitors, Florentino Ariza (Javier Bardem) and Doctor Juvenal Urbino (Benjamin Bratt) which spans 50 years, from 1880 to 1930.

Producer Scott Steindorff spent over three years courting Gabriel García Márquez for the rights to the book telling him that he was Florentino and would not give up until he got the rights. 

It is the first filming of a García Márquez novel by a Hollywood studio, rather than by Latin American or Italian directors.

It is the first English language work of Academy Award-nominated Brazilian actress Fernanda Montenegro, who portrays Tránsito Ariza.

==Film locations== Cartagena in Colombia. Some screen shots showed the Magdalena River and the Sierra Nevada de Santa Marta mountain range.   

==Production==
===Title sequence ===
London based animation studio VooDooDog created the title sequence and end sequence. These sequences draw inspirations from the colors and atmosphere of South America.

 

== Plot ==
In late 19th century Cartagena, a river port in Colombia, Florentino Ariza falls in love at first sight with Fermina Daza. They secretly correspond and she eventually agrees to marry but her father discovers their relationship and sends her to distant relatives (mainly her grandmother and niece). When she returns some years later, Fermina agrees to marry Dr. Juvenal Urbino, her fathers choice. Their fifty year marriage is outwardly loving, but inwardly marred by darker emotions.  Ferminas marriage devastates Florentino, who vows to remain a virgin, but his self-denial is thwarted by a tryst. To help him get over Fermina, his mother throws a willing widow into his bed and he discovers that sex is a very good pain reliever with which he replaces the opium that he had habitually smoked. He begins to record and describe each of his sexual encounters, beginning with the widow, and eventually compiling over 600 entries. A lowly clerk, he plods resolutely over many years to approach the wealth and social standing of Dr. Urbino. When the now-elderly doctor dies suddenly, Florentino immediately and impertinently renews courting Fermina.

==Cast==
*Javier Bardem as Florentino Ariza
*Giovanna Mezzogiorno as Fermina Daza
*Benjamin Bratt as Dr. Juvenal Urbino
*John Leguizamo as Lorenzo Daza
*Fernanda Montenegro as Tránsito Ariza
*Catalina Sandino Moreno as Hildebranda Sanchez
*Alicia Borrachero as Escolástica
*Liev Schreiber as Lotario Thurgot
*Laura Harring as Sara Noriega
*Hector Elizondo as Don Leo
*Ana Claudia Talancón as Olimpia Zuleta
*Angie Cepeda as The Widow Nazareth
*Patricia Castañeda as Grand Lady 4
*Unax Ugalde as Young Florentino
*Marcela Mar as America Vicuña
*Paola Turbay as Mystery Woman 2

==Reception==
===Gabriel García Márquez===
According to an interview by Colombian magazine Revista Semana, Scott Steindorff, producer of the film, showed an unreleased final edition of the film to Gabriel García Márquez in Mexico who at the end of the film is said to have exclaimed "Bravo!" with a smile on his face. 

===Critical response===
The film received mixed to negative reviews from critics. On the review aggregator Rotten Tomatoes, 26% of critics gave the film positive reviews, based on 108 reviews.  On Metacritic, the film had an average score of 44 out of 100, based on 27 reviews. 

Time (magazine)|Time rated it "D" and described it as "a serious contender   the worst movie ever made from a great novel ... Skip the film; reread the book." 

In her review in Entertainment Weekly, Lisa Schwarzbaum also rated it "D" and called it a "turgid and lifeless movie adaptation", opining that "those who have read Gabriel García Márquezs glowing and sexy 1988 novel about one mans grand love for a woman who marries another are bound to be peevishly disappointed ... those who havent read the book will now never understand the ardor of those who have — at least not based on all the hammy traipsing and coupling and scene-hopping thrown together here." 

In the Los Angeles Times, Carina Chocano stated, "the novel has made it to the screen in the form of a plodding, tone-deaf, overripe, overheated Oscar baiting telenovela ... Doubtless its an enormously daunting task to adapt a book at once so sweeping and internal, so swooningly romantic and philosophical, but it takes a lighter touch and a more expansive view than Newell and Harwood seem to bring." 
 Antonio Pinto was nominated for a Golden Globe for Best Song.

There are some noteworthy historical inaccuracies in the film.  Most notable is that the film covers the early 20th century (New Year celebrations for 1900 are shown), yet also depicts the British launching an attack on Cartagena de Indias, which actually occurred in 1740 during the War of Jenkins Ear.

==Box office performance==
In its opening weekend in the United States and Canada, the film ranked #10 at the box office, grossing $1.9 million in 852 theaters.  

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*  
*   

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 