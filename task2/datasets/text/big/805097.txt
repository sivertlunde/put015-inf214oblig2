School of Rock
 
 
{{Infobox film
| name = School of Rock
| image = School of Rock Poster.jpg
| border = yes
| alt =
| caption = Theatrical release poster
| director = Richard Linklater
| producer = Scott Rudin Mike White
| starring = Jack Black Joan Cusack Mike White Sarah Silverman Miranda Cosgrove Joey Gaydos Jr.
| music = Craig Wedren
| cinematography = Rogier Stoffers
| editing = Sandra Adair Scott Rudin Productions MFP Munich Film Partners GmbH & Company I. Produktions KG New Century Sor Productions
| distributor = Paramount Pictures
| released =  
| runtime = 109 minutes  
| country = United States
| language = English
| budget = $35 million
| gross = $131.3 million   
}} Mike White, rock singer prep school. musical adaptation Broadway in late 2015,  with a television adaptation for Nickelodeon also planned.

==Plot==
  prep school, inquiring for Ned about a short-term position as a substitute. Upon being given details of the job, realizing that he will paid generously, and desperate for money to avoid eviction from his home, Dewey impersonates Ned and is hired. On his first day working at the school, Dewey adopts the name “Mr. S” and spends his first day being lazy, much to the class’ confusion.

The next day, Dewey overhears the class in a music lesson and hatches a plan to form them into a new band to compete against No Vacancy in the Battle of the Bands tournament. He casts Zack Mooneyham as the lead guitarist, Freddy Jones as the drummer, Katie on bass guitar, Lawrence on keyboards, with Tomika, Marta, and Alicia as backup singers while he is lead vocals and guitarist. He assigns the rest of the class to various roles of groupies, roadies, and the class representative Summer Hathaway as band manager. The project takes over normal lessons, but helps the students to embrace their talents and grow confident. Dewey secretly takes the key band members out on a fake field trip to sign up for the competition, Summer getting them a spot by feigning a terminal illness. The next day, Dewey is almost exposed when Mullins decides to check on his teaching progress, forcing him to teach the real material.

Dewey befriends Mullins, who was once fun-loving and free-spirited, but the pressure of being principal and the expectations of the parents turned her into her current self. Dewey learns that a parent night is scheduled the night before the competition. Before attending the parent meeting, Ned receives a paycheck from the school addressed to him, forcing Dewey to confess that he has been impersonating Ned. Dewey urges Ned not to tell Patty, but after he leaves, she demands the truth and calls the police on Dewey. During the parents meeting, the parents question what Dewey was teaching the students while eventually, Ned, Patty, and the police arrive. With Mullins bursting in to question, Dewey reveals his true identity, admits that he is not really a licensed teacher, and becomes sacked. Back home, Dewey and Patty clash while Ned intervenes on behalf. Finishing the conversation, Ned admits that he misses playing music and hinted that it was time that Dewey had moved out. The next morning, Mullins is under pressure with the parents that attended the meeting last night, but the class decides to not let their hard work go to waste, so they decide to pick up Dewey and attend the competition. Knowing the truth that Dewey is leaving with the class, Patty calls Mullins, whilst an intrigued Ned decides to attend the competition, as well as breaking up with Patty. Mullins and the parents attend to the competition just as Dewey and the class take the stage as the School of Rock, and perform “Teacher’s Pet”, winning over the crowd including the stunned parents.

While No Vacancy wins, Dewey’s old band becomes amazed by Dewey’s new one, Summer meets several potential producers, and the audience requests for School of Rock to perform an encore. A few weeks later, Dewey and Ned open an after school program called the School of Rock at their apartment to teach children how to play instruments, whilst Dewey and the band practice.

==Cast==
 
* Jack Black as Dewey Finn (lead singer, guitar)
* Joan Cusack as Principal Rosalie "Roz" Mullins Mike White as Ned Schneebly
* Sarah Silverman as Patty Di Marco
* Miranda Cosgrove as Summer "Tinkerbell" Hathaway (band manager)
* Joey Gaydos Jr. as Zack "Zack-Attack" Mooneyham (lead guitar)
* Kevin Clark as Freddy "Spazzy McGee" Jones (drums)
* Rebecca Brown as Katie "Posh Spice" (bass)
* Robert Tsai as Lawrence "Mr. Cool" (keyboard)
* Maryam Hassan as Tomika "Turkey Sub" (second voice, lead choir)
* Caitlin Hale as Marta "Blondie" (choir)
* Aleisha Allen as Alicia "Brace Face" (choir)
* Brian Falduto as Billy "Fancy Pants" (stylist)
* Zachary Infante as Gordon "Roadrunner" (assistant, lights)
* James Hosey as Marco "Carrot Top" (assistant, special effects)
* Angelo Massagli as Frankie "Tough Guy" (security)
* Cole Hawkins as Leonard "Short Stop" (security)
* Jordan-Claire Green as Michelle (groupie)
* Veronica Afflerbach as Eleni (groupie)
* Adam Pascal as Theo
* Lucas Babin as Spider
* Lucas Papaelias as Neil
* Shawn Rodney as Shawn
 

==Production== Mike Whites concept for the film was inspired by the Langley Schools Music Project. 
 
A stage dive gone wrong incident involving Ian Astbury of rock band The Cult was witnessed by Jack Black, and was used as inspiration for a scene in School of Rock, in which the character Dewey Finn, stage dives and hits the floor; "I went to see a reunion, in Los Angeles, of The Cult; they were playing and Ian Astbury, the lead singer, took a dive. It was at The Viper Room, and it was just a bunch of jaded Los Angelinos out there, and they didnt catch him and he plummeted straight to the ground. Later I thought it was so hilarious. So that was put into the script." 

Many scenes from the movie were shot around the  " by Pink Floyd and "Come On Feel the Noize" is taken from "Cum On Feel the Noize" by Slade. One of the theaters used in many of the shots was at Union County Performing Arts Center located in Rahway, New Jersey.

Led Zeppelin had a policy that their music not be used in movies and other other promotional media.  The producers knew that having no songs from Led Zeppelin featured in a movie of School of Rocks nature would leave it sorely lacking.  So, Jack Black and the producers used some extra time during the filming of the crowd scene in the Battle of the Bands to send a personal video plea to the members of Led Zeppelin to use one of their songs in the movie.  This ploy ultimately worked, and The Immigrant Song was used in one scene from the movie. 

==Music==

===Soundtrack===
 
A soundtrack album of the same name was released on September 30, 2003. The films director, Richard Linklater, scouted the country for talented 13-year-old musicians to play the rock-and-roll music that features on the soundtrack and in the film.

The soundtrack includes "Immigrant Song" by Led Zeppelin, a band that historically has not allowed their songs to be used for commercial purposes, and rarely give permission for anyone to use their songs, one noted exception being filmmaker Cameron Crowe, who was the only person to write about them favorably while he was a writer for Rolling Stone magazine. To get permission, Richard Linklater came up with the idea to shoot a video on the stage used at the end of the film, with Jack Black begging the band for permission and the crowd cheering and chanting behind him. The video was sent directly to Led Zeppelin, and permission was granted for the song. The video can be seen on the DVD extras.

===Music featured within the film===
 
 
* "For Those About to Rock (We Salute You)" by AC/DC (Dewey uses the lyrics in a speech to the class)
* "Fight" by No Vacancy*
* "Stay Free" by The Clash Touch Me" by The Doors* Kiss
* "Sunshine of Your Love" by Cream (band)|Cream* Back in Black" by AC/DC Iron Man" by Black Sabbath (riff Dewey plays to Zack) Highway to Hell" by AC/DC (riff Dewey plays to Zack)
* "Smoke on the Water" by Deep Purple (riff Dewey plays to Zack)
* "Substitute (The Who song)|Substitute" by The Who*
* "Greatest Love of All" by Whitney Houston (Dewey mentions the lyrics as his reason for no testing)
* "Roadrunner (Jonathan Richman song)|Roadrunner" by The Modern Lovers
* "My Brain Is Hanging Upside Down (Bonzo Goes to Bitburg)" by Ramones* The Wait" (Killing Joke cover) by Metallica
 
* "Sad Wings" by Brand New Sin
* "Mouthful of Love" by Young Heart Attack The Darkness
* "Immigrant Song" by Led Zeppelin* Set You Free" by The Black Keys* Edge of Seventeen" by Stevie Nicks*
* "Ballrooms of Mars" by T.Rex (band)|T. Rex*
* "Moonage Daydream" by David Bowie
* "TV Eye" by The Stooges*
* "Ride into the Sun" by The Velvet Underground
* "Heal Me, Im Heartsick" by No Vacancy*
* "School of Rock" by School of Rock*
* "Its a Long Way to the Top (If You Wanna Rock n Roll)" (AC/DC cover) by School of Rock*
* "Math Is a Wonderful Thing" by Jack Black and Mike White*
 
 *  Featured on the Soundtrack album

==Reception==
School of Rock has received critical acclaim, with Blacks performance being praised by many critics. It received a "Certified Fresh" rating of 92% on Rotten Tomatoes based on 192 reviews with an average rating of 7.8/10. The sites critical consensus reads, "Blacks exuberant, gleeful performance turns School of Rock into a hilarious, rocking good time."    On Metacritic, the film has a score of 81 out of 100, based on 41 critics, indicating "universal acclaim". 

===Box office performance===
School of Rock opened at #1 with a weekend gross of $19,622,714 from 2,614 theaters for an average of $7,507 per venue. In its second weekend, the film declined just 21 percent, earning another $15,487,832 after expanding to 2,929 theaters, averaging $5,288 per venue, and bringing the 10-day gross to $39,671,396. In its third weekend, it dropped only 28 percent, making another $11,006,233 after expanding once again to 2,951 theaters, averaging $3,730 per venue, and bringing the 17-day gross to $54,898,025. It spent a total of six weeks among the Top 10 films and eventually grossed $81,261,177 in the United States and Canada and another $50,015,772 in international territories for a total gross of $131,282,949 worldwide, almost four times its budget of $35 million.

===Awards and nominations=== Best Actor Lost in MTV Movie Award for Best Comedic Performance.

==Legacy==

===Potential sequel===
In 2008, Jack Black stated a sequel was being considered.  It was later reported that director Richard Linklater and producer Scott Rudin would return.  Mike White was returning as screenwriter, titled School of Rock 2: America Rocks, which picks up with Finn leading a group of summer school students on a cross-country field trip that delves into the history of rock n roll. 

In 2012, Jack Black stated a sequel was unlikely, "I tried really hard to get all the pieces together. I wouldnt want to do it without the original writer and director, and we never all got together and saw eye-to-eye on what the script would be. It was not meant to be, unfortunately", but added, "never say never". 

===Stage adaptation===
 
On April 5, 2013, Andrew Lloyd Webber announced that he has bought the rights to School of Rock to a stage musical.    On 18 December 2014, the musical was officially confirmed and it was announced that the show would receive its world premiere on Broadway in autumn 2015, at the Winter Garden Theatre.  The musical has a book by Downton Abbey creator Julian Fellowes.  and is directed by Laurence Connor,  with choreography by JoAnn M. Hunter,  set and costume design by Anna Louizos  and lighting by Natasha Katz.  The musical features an original score composed by Andrew Lloyd Webber, with lyrics by Glenn Slater  and sound design by Mick Potter,  in addition to music from the original film.
School of Rock will be Andrew Lloyd Webbers first show opening on Broadway since Jesus Christ Superstar in 1971. 

===10-year reunion=== The Paramount Theatre. Those in attendance included director Richard Linklater, Jack Black, Mike White, Miranda Cosgrove and the rest of the young cast members except for Cole Hawkins (who played Leonard). 
 The Austin Film Society and Cirrus Logic, included a red carpet, a full cast and crew Q&A after the screening, where the now-grown child stars discussed their current pursuits in life, and a VIP after-party performance by the School of Rock band during which "School of Rock" and "Its a Long Way to the Top (If You Wanna Rock n Roll)" were played. 

===Television adaptation===
On August 4, 2014, Nickelodeon had announced that they are working with Paramount Television on a television show adaptation of the movie and it would be produced by the director and writer of the film. Production started in the fall and it is scheduled to premiere in 2015.  Richard Linklater was the director.  It will star Ricardo Hurtado, Lance Lim, Aidan Miner, Jade Pettyjohn, Breanna Yde and Tony Cavalero. 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 