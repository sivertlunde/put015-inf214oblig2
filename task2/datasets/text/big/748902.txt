Getting Any?
{{Infobox film
| name = Getting Any?
| director = Takeshi Kitano
| image = Getting Any? FilmPoster.jpeg Masayuki Mori Hisano Nabeshima Taiko Yoshida
| writer = Takeshi Kitano Beat Takeshi Sonomanma Higashi Tokie Hidari Shouji Kobayashi
| music = Senji Horiuchi Hidehiko Koike
| cinematography = Katsumi Yanagishima
| editing = Takeshi Kitano Yoshinori Oota
| distributor = Nippon Herald Films Herald-Ace Office Kitano
| released = February 2, 1995
| runtime = 110 minutes
| language = Japanese
| country = Japan
| preceded_by =
| followed_by =
}} 1995 Cinema Japanese film, Japanese filmmaker Takeshi Kitano.
 Japanese verb yaru, which is an informal word meaning to do, and has become slang for sexual intercourse.

Getting Any? is best described as a sex comedy movie. It showed Beat Takeshi, originally a very popular manzai performer, returning to his comedic roots. The movie features an Airplane!-like assemblage of comedic scenes centering around a Walter Mitty-type character whose obsession is to have sex.

The film met with little acclaim in Japan where its release was barely noticed. The film maker confessed in 2003 (while in production for Zatoichi (2003 film)|Zatoichi), that Getting Any? was one of his three favourite movies among the ten he had directed by that time. According to him, this work was the basis for many of the movies that followed, including the acclaimed Hana-bi, as it features all his recurrent themes plus its shares of violence and sorrow.

According to Kitano, his purpose in this movie was to laugh at his own gags, to make a mockery of them. He also wanted to laugh at the young Japanese men, those born after World War II, who were simple-minded and much too direct and simplistic when it came to talking with girls about having sex. Kitano denied satirizing Japanese society, and claimed that his aim in this movie was to make the audience laugh.
 
==Plot==
 In Getting any? Minoru Iizuka, also known as "Dankan" (Boiling Point), portrays Asao, a naive and goofy man who lives with his grandfather in Saitama Prefecture.  Even though Asao is 35 years old, he is very inexperienced with girls, but he absolutely wants to have sex. One day as he watches an erotic TV film, he realizes that all he needs to get girls and sex is a fancy car, so he runs to the closest car dealer.
 Porsche 911 luxury import Rolls Royce, Aston Martin and Ferrari displayed in the show room.

 
Then, he quickly restages the scene he has seen in the TV film, with the help of a female mannequin, before aiming for street girls. Quickly, Asao realizes that male-female social relations are not as simple as in porn, and that real life girls are not as naive as in fictional works. 
 roadsters are Austin Healey Sprite MkI, a classic British roadster, but once he learns how much money Asao really has, he changes his mind and sells him a Mazda Eunos Roadster (Mazda Miata), a popular Japanese roadster, claiming it is the same car. When Asao insists, the seller claims the 50s Austin-Healey was old-fashioned and the Eunos is a better model for cruising. The Eunos is driven a few meters and starts falling apart. Asao complains to the seller, but the latter is a con-man and refuses to talk with him any more, so Asao picks up the fallen pieces and attaches them to the back of his car.

The young man tries his luck anyway with girls on the street, but does not succeed, so he finally sells his wrecked car to a local salvage company.
 Mazda RX-7 Series 5 Turbo (FC), and he decides to steal it. Asao drives a few miles when he encounters a young woman walking along the road and decides to talk to her, as shown in the erotic film, but the sports car does not brake anymore and he runs over the girl, and crashes the car into a billboard. He is lucky enough to not get hurt and he ditches the coupé.

  747 customers, Kawaguchi City (near Tokyo) where he will be able to make his own revolver in the local iron foundry.
 The Fly.  The movie ends with his capture after diving into a large reservoir of feces. After the credits, there is a scene where Asao jumps around Tokyo as the fly-man before landing/getting impaled on Tokyo Tower.

== Cast ==
*Dankan - Asao
*Hideo Higashikokubaru - Yaku 
*Takeshi Kitano - Scientist
*Akiji Kobayashi - Chief of World Defence Force
*Masumi Okada - Russian actor
*Susumu Terajima - Injured Yakuza
*Ren Osugi - Head Yakuza 3

==References and parodies== satirize Popular popular Culture Japanese culture from the 1950s up to the 80s, including cinema, TV series, anime, and pop music; although, Kitano himself denied it.
 
 The Fly, so the gag is also at least partially based on the Hollywood film.  Hunting suits worn by the Doctor and his assistant are a reference to Ivan Reitmans 1984 SF-comedy Ghostbusters.  The alternative ending, which comes after the end credits, features the familiar reference to Steven Spielberg E.T. the Extra-Terrestrial|E.T., the character flying by the moon. 

Getting Any? also features some guest stars including Masumi Okada (playing Joseph Stalin) and Akiji Kobayashi (as leader of the Terrestrian Forces from Ultraman).

===Cinema=== Sword of Vengeance)
*Zatoichi series (座頭市, Zatōichi)
*Branded to Kill (殺しの烙印, Koroshi No Rakuin) The Secret Of The Telegian (電送人間, Densō Ningen)
*Tōmei ningen to hae-otoko (透明人間と蝿男, lit. Invisible Man & Fly-Man)
*Godzilla vs. Mothra (ゴジラVSモスラ)
*Ghostbusters
*The Fly (1986 film)

===TV series===
*Ultraman series (ウルトラマン)

===Anime=== Ge ge ge no Kitaro series (ゲゲゲの鬼太郎)

===Music=== Enka style
*J-pop|J-pop style Minyo style

==Scatological humour==
  scatological gags used in his movie, Takeshi Kitano answered that excrements and manure were a common source of humor in Japan since the country was traditionally an agricultural workers land.

A French interviewer even asked the film maker if the giant dirt, seen near the end of the movie, was a metaphor for the decadence of the Japanese society, but Kitano laughed and answered that not at all, it was only meant as a "local color" joke.

==References==
 
 

==Sources==
* Takeshi Kitano video interview featured in the Getting Any? DVD published by Cheyenne Films (EDV 1040), France (2003).

==External links==
*   at kitanotakeshi.com
*  
*  
*  
*    
*   at the Japanese Movie Database  

 

 
 
 
 
 
 