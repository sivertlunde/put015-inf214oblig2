A Closed Book (film)
 
{{Infobox film
| name           = A Closed Book
| image          = A Closed Book.jpg
| caption        = 
| director       = Raúl Ruiz (director)|Raúl Ruiz
| producer       = Andrew Somper
| based on       =  
| writer         = Gilbert Adair
| starring       = Daryl Hannah Tom Conti Miriam Margoyles Simon MacCorkindale Elaine Page
| cinematography = Ricardo Aronovich
| editing        = Sean Barton Adrian Murray Valeria Sarmiento
| distributor    = Eyeline Entertainment
| released       =  
| runtime        = 88 minutes
| country        = United Kingdom
| language       = English
| budget         = 
}} novel of the same name by Gilbert Adair, about a blind author who employs an assistant to help him write his novels. Throughout the film the assistant starts to play crueler and crueler tricks on her employer. The film is directed by Raúl Ruiz (director)|Raúl Ruiz, and stars Daryl Hannah as assistant Jane Ryder, and Tom Conti as author Sir Paul. A Closed Book was filmed at Knebworth House in the UK. 

==Plot==
The film starts with a famous author, Sir Paul (Tom Conti) who is looking for an amanuensis or "ghost writer." He interviews several unsuccessful candidates until Jane Ryder (Daryl Hannah) applies for the position. Because she is intelligent and forthright, Sir Paul hires her, and he explains to her about the house and introduces Mrs. Kilbride (Miriam Margoyles), the cook and housekeeper.

They have breakfast together and discuss their pet annoyances, with Sir Paul getting disproportionately angry about Jane saying "no problem" repeatedly. They start writing the book, and Sir Paul again gets very angry at Jane for not taking his instructions. Things run smoothly until Jane starts changing things ever so slightly, like taking paintings out of their frames and turning them upside down. Sir Paul starts to suspect Jane, when she sends Mrs. Kilbride home for a week without consulting him. 

Sir Paul has a "terrific fear of the dark" and while singing in the bath, he keeps hearing strange noises, which are really Jane attempting to scare him. While he is in the bath, someone turns the light off, and then Jane comes in naked and claims that the light is still on, to reassure him. Her lying and sneaking becomes more and more obvious as she lies to him about Madonna dying and O.J. Simpson committing suicide, and puts his books on the fire instead of logs. Sir Paul becomes very suspicious when Mrs. Kilbride returns to the house and she finds a puzzle that Sir Paul asked Jane to purchase, and it turns out to be the wrong puzzle. This particularly matters to Sir Paul as he wrote about the painting in his book, and he gets very, very angry at her and begins to lose trust in her.

However, his suspicions are assuaged when a Conservative MP visits his house to persuade him to vote for the Conservatives. Because of her fear of Sir Paul, she responds positively to all of his questions, and she reads aloud what Jane has written in his book. Fortunately for Jane, her transcription of his words is accurate.

However, Jane escalates her campaign against him, and one day she leaves a suit of armour lying on the floor, and displaces several desks and a number of books. She knows that Sir Paul will walk into them, which will cause him to trip and fall down the stairs. She then comes back into the house.

Eventually, Sir Paul realizes that Jane is attempting to kill him, and he has a confrontation with her, in his bedroom. She tells him that her husband had once had an art exhibition at a prestigious gallery, where he had been severely criticized by Sir Paul. Due to the subject material of his paintings, the artist had been arrested and accused of being a pedophile. After comparing Sir Paul to "a closed book," Jane shoves him into a closet and she leaves the house, with Sir Paul screaming after her. However, when she returns to the house out of guilt, and because she lacks the capacity to kill someone, Jane discovers that Sir Paul has escaped from the closet. Then, Sir Paul points a gun at Jane, and he reveals to her that he is also a pedophile. He invites Jane to shoot him, but she leaves the house, and he is forced to shoot himself.

==Cast==
* Tom Conti - Sir Paul
* Daryl Hannah - Jane Ryder
* Miriam Margoyles - Mrs. Kilbride
* Simon MacCorkindale - Andrew Boles
* Elaine Paige - Canvasser
* Ty Glaser - Janes successor

==Reception==
Critical reception was overall poor, with a few exceptions. The Times gave it one (out of five) stars and called it an "atrocious, creepy little film".   The Daily Telegraph gave it 2 out of 5 stars, stating, "A Closed Book feels less like a thriller than an aesthete’s tease".  The Guardian also gave a poor review and only 2 out of 5 stars, describing it as "a silly story about a blind art critic".  Perhaps the most scathing is a review from Time Out London which gave the film only 1 star, finding "Raul Ruizs apologists have their work cut out for them." 

Anthony Quinn of The Independent gave it 3 out of 5 stars stating that "its sheer unlikeliness is what also keeps you hanging in there."  Review aggregator Rotten Tomatoes shows an average of 3.8 out of 10 based on twelve reviews, but 67% among non-professional critics. 

==References==
 

==External links==
*  

 

 
 
 
 
 
 