Dilwale (2015 film)
 
 
{{Infobox film
| name           = Dilwale
| image          =
| caption        =
| director       = Rohit Shetty
| producer       = Gauri Khan Rohit Shetty
| writer         = Robin Bhatt Sajid-Farhad Yunus Sajawal
| story          = K. Subash
| editor         = Bunty Negi
| starring       = Shah Rukh Khan Kajol Varun Dhawan Kriti Sanon
| music          = Pritam
| cinematography = Dudley
| studio         = Red Chillies Entertainment
| released       = 25 December 2015
| country        = India
| language       = Hindi
}}

Dilwale (English: The Big Hearted) is an upcoming Indian comedy-drama film directed by Rohit Shetty and produced by Shetty and Gauri Khan .   The film stars Shah Rukh Khan, Kajol, Varun Dhawan and Kriti Sanon in lead roles.       Principal photography began on 20 March 2015 with Dhawan, and the film is scheduled for release during Christmas 2015.      

== Cast ==
{{columns-list|2|
*Shah Rukh Khan
*Kajol
*Varun Dhawan
*Kriti Sanon
*Boman Irani
*Varun Sharma
*Kabir Bedi
*Vinod Khanna
*Johnny Lever Sanjay Mishra
*Murli Sharma
*Mukesh Tiwari
*Chetna Pande
}}

==Production==
In January 2015, filmmaker Rohit Shetty announced that he will start his next project with Shah Rukh Khan in the lead, their second film together after Chennai Express in 2013. Shetty stated that he would start filming in March  2015.   Shetty also signed actor Varun Dhawan who was reportedly playing Shah Rukh Khans brother while actress Kriti Sanon was signed opposite him. 
On 14 March 2015, his birthday, the director confirmed that Kajol would play the female lead opposite Shah Rukh Khan in the film. The film will also star Kabir Bedi and Vinod Khanna.

==References==
 

==External links==
 

 
 
 
 