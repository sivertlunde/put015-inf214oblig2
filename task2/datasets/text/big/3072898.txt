Gupt: The Hidden Truth

 
 

{{Infobox Film 
| name           = Gupt: The Hidden Truth 
| image          = Gupt.jpg
| director       = Rajiv Rai
| producer       = Gulshan Rai Rajiv Rai
| story          = Rajiv Rai
| screenplay     = Rajiv Rai Shabbir Boxwala
| starring       = Bobby Deol Manisha Koirala Kajol
| music          = Viju Shah
| cinematography = Ashok Mehta
| additional cinematography = Rajiv Jain
| editing        = Rajiv Rai
| studio         = 
| distributor    = Trimurti Films
| released       = 4th July 1997
| runtime        = 174 mins
| country        = India
| language       = Hindi
| awards         = 
| budget         = 
| gross          =  
}}
Gupt: The Hidden Truth ( ), commonly known as Gupt, is a 1997 Bollywood suspense thriller film, directed by Rajiv Rai. It stars Bobby Deol, Manisha Koirala and Kajol in lead roles, whilst Paresh Rawal, Om Puri and Raj Babbar appear in supporting roles. The films soundtrack was composed by Viju Shah, while the lyrics were penned by Anand Bakshi.

Upon release, Gupt was a commercial success, with its storyline and soundtrack earning critical praise. It grossed   at the domestic box office.   

==Plot Synopsis==

Sahil is accusedto escape from jail. However a fearless cop is hot on his trail.

==Plot==

Governor Jaisingh Sinha (Raj Babbar) holds an authoritative position in the state administration and is also renowned among the populace as an even-handed person. Among his associates are the other prominent persons of the city - Mantriji (Prem Chopra), affluent industrialist Meghnath Chaudhry (Dalip Tahil) and union leader Vilas Rao (Sharat Saxena). Sinhas family include his step-son Sahil (Bobby Deol) and Sahils mother Sharda (Priya Tendulkar) along with his own son Harsh. Sinha shares a troubled relationship with Sahil, who dislikes his step father.

After a lengthy detachment, Sahil meets his childhood companion Isha (Kajol), the daughter of the Governors PA, Ishwar Diwan (Paresh Rawal) at college and mutual love escalates between the two. At the same time Sheetal (Manisha Koirala), the daughter of Meghnath Chaudhry, secretly loves Sahil. Considering Sheetal as perfect better-half for Sahil, Sinha, on the occasion of Sahils birthday party publicly declares Sahils engagement with Sheetal. Sahil, who is stupefied, takes exception to his fathers decision and at one point, furiously aims a knife at his step father in front of the guests.

On a later day, Sahil is intoxicated after drinking heavily at the house of Dr Gandhi (Kulbhushan Kharbanda), the family doctor, who advises Sahil to accept Sheetal. A pie-eyed Sahil decides to confront his father and returns home to find, to his shock and horror, that someone has mortally stabbed him. His mother sees him and believes him to be the murderer, and Sahil is framed for his step fathers murder, after many of Sinhas acquaintances, including the family lawyer Thanawala (Raza Murad) and even his mother, testify against him. The court condemns to Sahil 14 years of imprisonment. Before being taken to Central Jail, Sahil stealthily passes an unidentified locket to Sheetal, which he found at his fathers murder spot and asks her to keep it for as long as possible.

In jail, an old prisoner hearkens to Sahils account and narrates to him a secret, yet perilous scheme of escaping from the jail. Sahil beats up two prisoners along with the jailer and is thrown into the darkest cell in the prison. One day, Sahil succeeds in decamping from the jail along with the two prisoners he beat up, through an unheard-of gutter pipe from that very cell that leads to the sea, with a boat arrangement afforded by Sheetal. He then meets Isha, who decides to help him with finding the real killer. At this moment of extreme hardship, Sahil decides to meet Dr Gandhi to get some advice from him after a cryptic warning from the latter on the phone. On the other hand, Sahils escape creates unrest in the police department and an unyielding & resolute police officer Udham Singh (Om Puri) is commissioned the case of taking Sahil into custody. When Sahil goes to Dr Gandhis house later that night, he finds that the doctor has also been stabbed to death by possibly the same mysterious killer. With the testimony given by Dr Gandhis servant, an F.I.R. is lodged against Sahil.

Sahil, now framed for two murders, finds many grounds to figure out that his fathers killer must be one among the governors associates - lawyer Thanawala, union leader Vilas Rao, Meghnath Chaudhry and Mantriji, all of whom testified against him in the court. He attacks and interrogates them separately. However, this is inconclusive as all of them reveal ulterior motives, but deny having any hand in the murder.

With the evidence of the two typically made Kashmiri knives, found at both murder sites, Udham Singh figures out that they belong to a set. After some digging, he finds that the set belongs to Ishwar Diwan, hidden behind a painting in his house. Diwan tries to elude the clutches of the police but is promptly nabbed by Udham Singh, in front of Sahils mother and younger brother. Diwan then confesses to both murders, the motive being that Sinha had rejected Isha, believing her to be of a lower stature. With this confession, Sahils name is cleared and he returns home. The only unsolved mystery left now is that of the locket that Sahil found earlier. Udham Singh, is drinking in is house, along with two of his associates, celebrating the end of the case. When the associates leave, the mysterious killer then appears behind Udham Singh, just as Sahils brother cracks open the locket and shows it to him. The killers face is shown just as Sahil finds a picture inside the locket. It is Isha.

Isha stabs Udham Singh several times and escapes. Shocked, Sahil goes to the police station where Ishwar Diwan is locked up and confronts him about Isha. Diwan reveals to have known all along that his daughter was the killer. He then goes on to explain that ever since their childhood, Isha had harboured a possessive nature for Sahil. Once, Dr. Gandhis dog had bitten Sahil, and had been found dead later, Isha having killed him. Given her psychological problem, and despite Ishwars many protests, Dr. Gandhi and Governor Sinha had sent Isha away to a boarding school. When he met Isha later, their relationship progressed well. After Sahils disagreement with his father at his birthday party, Ishwar had gone with his daughter to Sinha and had pleaded to him to accept Isha as his daughter-in-law. Both he and Isha were rudely rejected by Sinha and Dr. Gandhi. The rejection had driven an already enraged Isha to the point of insanity, which had driven her to commit both murders.

Realising that Sheetal is in danger, since Isha had threatened her earlier, Sahil rushes to her house, where she is alone. Isha goes to her house and starts to attack her while Sahil is himself attacked by a henchman sent by Mantriji to kill him. Wounded and exhausted, Sahil finally reaches Sheetals house and prevents Isha from killing Sheetal but the henchman arrives and is killed instead. Isha still continues to attack Sheetal, with Sahil trying to wrest the knife from her. In the nick of time, Udham Singh, wounded but not dead, arrives and shoots Isha. As she lays dying in his arms, Isha confesses to Sahil that she had always loved him and she had been scared of losing him. Sahil sadly says that he had always been hers. He hugs Isha one final time as she dies in his arms.

==Cast==
* Bobby Deol as Sahil Sinha
* Manisha Koirala as Sheetal Choudhry
* Kajol as Isha Diwan

* Raj Babbar as Gov. Jaisingh Sinha
* Paresh Rawal as Ishwar Diwan 
* Sadashiv Amrapurkar as Insp. Neelkanth
* Prem Chopra as Mantri
* Om Puri as Insp. Udham Singh
* Kulbhushan Kharbanda as Dr. Gandhi
* Dalip Tahil as Meghnath Choudhry (Sheetals dad)
* Mukesh Rishi as Mantris bodyguard
* Tej Sapru as a Jailer
* Harish Patel as Phoolchand
* Ashok Saraf as Havaldar Pandu
* Anjan Srivastav as Commissioner Patwardhan
* Vishwajeet Pradhan as a Bounty hunter
* Dinesh Anand
* Dinesh Hingoo as Thanawalas servant
* Priya Tendulkar as Sharda Sinha (Sahils mom)
* Bob Christo as Boat organiser
* Raza Murad as Lawyer Thanawala
* Sharat Saxena as Vilas Rao
* Usman Khan
* Master Harsh Lunia as Sahils younger brother
* Aparajita as Mrs. Choudhry

==Soundtrack==
The films soundtrack composed by Viju Shah won the Filmfare Best Background Score award in 1998. 
{| class="wikitable" border="1"
! # !! Title !! Singer(s)!! Length
|-
| 1
| "Yeh Pyasi Mohabbat"
| Alka Yagnik
| 06:02
|- 
| 2
| "Mushkil Bada Yeh Pyaar Hai"
| Alka Yagnik, Udit Narayan
| 05:53
|- 
| 3
| "Gupt Gupt"
| Kavita Krishnamurthy, Hema Sardesai, Chetan 
| 02:55
|- 
| 4
| "Gupt Gupt (Extended)"
| Kavita Krishnamurthy, Hema Sardesai, Chetan
| 04:55
|- 
| 5
| "Mere Khwabon Mein Tu"
| Kumar Sanu, Alka Yagnik
| 05:33
|-
| 6
| "Duniya Haseeno Ka Mela" Sunita Rao
| 06:30
|- 
| 7
| "Yeh Pyaar Kya Hai"
| Kumar Sanu, Alka Yagnik, Kavita Krishnamurthy
| 05:13
|-  
| 8
| "Mere Sanam"
| Udit Narayan, Sadhana Sargam
| 05:48
|-
| 9
| "Gupt Gupt (Instrumental)"
|
| 02:22
|}

==Critical reception==
Gupt received positive reviews from critics. Fullhyderabad.com gave it a 7.5/10 rating and wrote, "It is a slickly made movie with stylish cinematography, beautiful locales, and pretty good performances. It falls in the genre of thriller movies and fares much better than any of its sorry predecessors did. The director, Rajiv Rai is not over-awed by the subject and so does not make a hosh-posh out of the whole thing."  Mohammad Ali Ikram of Planet Bollywood praised the suspense and music, but criticized Bobby Deols performance in the film. 

==Box office==
Gupt grossed   at the domestic box office. Box Office India declared it a "Hit". 

==Awards and recognition==
1997 was one of the most successful years for  s and was nominated in five other categories. It was also the fourth largest blockbuster of the year after Border, Dil To Pagal Hai and Pardes. Kajol was the first woman in Filmfare Award history to receive the Best Villain award.

===1998 Filmfare awards=== Best Villain - Kajol Best Background Score - Viju Shah Best Editing - Rajiv Rai

==See also==
*Red herring (narrative)
*Twist ending

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 