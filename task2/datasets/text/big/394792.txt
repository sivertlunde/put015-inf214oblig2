Jackie Brown (film)
 
 
{{Infobox film
| name           = Jackie Brown
| image          = Jackie Brown70s.jpg
| caption        = Theatrical release poster
| director       = Quentin Tarantino
| producer       = Lawrence Bender
| screenplay     = Quentin Tarantino
| based on       =  
| starring       = {{Plain list|
* Pam Grier
* Samuel L. Jackson
* Robert Forster
* Bridget Fonda
* Michael Keaton
* Robert De Niro
}}
| cinematography = Guillermo Navarro
| editing        = Sally Menke
| studio         = A Band Apart
| distributor    = Miramax Films
| released       =  
| runtime        = 154 minutes  
| country        = United States
| language       = English
| budget         = $12 million   
| gross          = $74.7 million 
}} crime drama adaptation of Foxy Brown (1974), both of which also starred Grier in the title roles.

The films supporting cast includes Robert Forster, Robert De Niro, Samuel L. Jackson, Bridget Fonda and Michael Keaton. It was Tarantinos third film following his successes with Reservoir Dogs (1992) and Pulp Fiction (1994).

Grier and Forster were both veteran actors but neither had performed a leading role in many years. Jackie Brown revitalized both actors careers. The film garnered Forster an Academy Award nomination for Best Supporting Actor, and Golden Globe Award nominations for Jackson and Grier.

==Plot==
Jackie Brown (Pam Grier) is a flight attendant for a small Mexican airline, the latest step down for her career, since she used to work for larger airlines. To make ends meet, she smuggles money from Mexico into the United States for Ordell Robbie (Samuel L. Jackson), a black-market gun runner living in the Los Angeles area under the Bureau of Alcohol, Tobacco, Firearms and Explosives|ATFs close watch, forcing him to use couriers.
 bondsman Max Cherry (Robert Forster), then coaxes Livingston into a car trunk and murders him.
 LAPD detective Michael Bowen) intercept Jackie as she returns to the United States with Ordells cash and some cocaine that Brown was unaware was stashed in her bag. Initially refusing to cut a deal, she is sent to jail on possession of drugs with intent to distribute. Sensing that Jackie may also now be a threat to inform, Ordell goes back to Max to arrange her bail.

Max arrives to pick up Jackie at the jail, and he begins to develop an attraction to her. He offers to buy her a drink and help explain her legal options. Ordell later arrives at Jackies house intending to murder her for silence. She surprises him by pulling a gun that she surreptitiously took from Maxs glove compartment. Jackie negotiates a deal with Ordell to pretend to help the authorities while smuggling in $550,000 of Ordells money, enough to allow him to retire.

To carry out this plan, Ordell is counting on Melanie Ralston (Bridget Fonda), a surfer girl he lives with who has little ambition past smoking marijuana and watching TV, and Louis Gara (Robert De Niro), a friend and former cellmate. He also intends to use a naive Southern girl, Sheronda (Lisa Gay Hamilton). Unaware that Jackie and Ordell plan to transfer $550,000, Nicolette and Dargus devise a sting to catch Ordell during a transfer of $50,000. Unbeknownst to all, Jackie plans to double-cross everyone and keep the other $500,000 for herself. She recruits Max for her plan and offers him a cut.

After a trial run, the stage is set for the actual event. In the Del Amo Fashion Center, Jackie enters a dressing room to try on a new suit. She has told Ordell that she will swap bags there with Melanie, supposedly passing off the $550,000 under the nose of Nicolette, who has been told that the exchange is to take place in the food court. Instead, the bag she gives Melanie contains only $50,000 and the rest is left behind in the dressing room for Max to pick up. Jackie then feigns despair as she calls Nicolette and Dargus out from hiding, claiming Melanie took all the money and ran.

In the parking lot, Melanie mocks Louis until he loses his temper and shoots her. Ordell asks Louis why he shot her and if she is really dead. Ordell grows even angrier when he discovers that most of the money is gone, and he realizes that Jackie is to blame. When Louis mentions that he saw Max Cherry in the store dress department and thought nothing of it, Ordell kills him and leaves with the bag. Ordell turns his anger on Max, who informs him that Jackie is frightened for her life and waiting in Maxs office to hand over the money. A menacing Ordell holds him at gunpoint as they enter the darkened office. Jackie suddenly yells that Ordell has a gun, and he is shot dead by Nicolette, hiding in another room.

Having had her charges dropped for her cooperation and in possession of the money, along with Ordells car, Jackie decides to leave the country and travel to Madrid, Spain. She invites Max to come along with her, but he declines, preferring to remain in his well-controlled life instead of an uncertain life with Jackie in Spain. Jackie kisses Max goodbye, shares with him a meaningful moment, and leaves.

==Cast==
* Pam Grier as Jackie Brown
* Samuel L. Jackson as Ordell Robbie
* Robert Forster as Max Cherry
* Bridget Fonda as Melanie Ralston
* Michael Keaton as Ray Nicolette
* Robert De Niro as Louis Gara
* Chris Tucker as Beaumont Livingston Michael Bowen as Mark Dargus
* Lisa Gay Hamilton as Sheronda Tommy "Tiny" Lister Jr. as Winston
* Hattie Winston as Simone
* Sid Haig as Judge
* Aimee Graham as Amy
* Diana Uribe as Anita Lopez
* TKeyah Crystal Keymáh|TKeyah Crystal Keymah as Raynelle
* Denise Crosby (uncredited) as Public defender
* Quentin Tarantino (uncredited) as Answering machine voice
* Helmut Berger (uncredited, archive footage) as Nanni Vitali
* Marissa Mell (uncredited, archive footage) as Giuliana Caroli
* Council Cargle (uncredited, archive footage) as Drew Sheppard
* Tony Curtis (uncredited, archive footage) as himself

==Production==

===Development===
After completing Pulp Fiction, Quentin Tarantino and Roger Avary acquired the film rights to Elmore Leonards novels Rum Punch, Freaky Deaky, and Killshot. Tarantino initially planned to film either Freaky Deaky or Killshot and have another director make Rum Punch, but changed his mind after re-reading Rum Punch, stating that he "fell in love" with the novel all over again. Jackie Brown: How It Went Down, Jackie Brown DVD, Miramax Home Entertainment  While adapting Rum Punch into a screenplay, Tarantino changed the ethnicity of the main character from white to black, as well as renaming her from Burke to Brown, titling the screenplay Jackie Brown. Avary and Tarantino hesitated to discuss the changes with Leonard, finally speaking with Leonard as the film was about to start shooting. Leonard loved the screenplay, considering it not only the best of the twenty-six screen adaptations of his novels and short stories, but also stating that it was possibly the best screenplay he had ever read. 

Tarantinos screenplay otherwise closely followed Leonards novel, incorporating elements of Tarantinos trademark humor and pacing. The screenplay was also influenced by blaxploitation films, but Tarantino stated that Jackie Brown is not a blaxploitation film. 

Jackie Brown alludes to Griers career in many ways. The films poster resembles those of Griers films Coffy and Foxy Brown and includes quotes from both films. The typeface for the films opening titles was also used for those of Foxy Brown; some of the background music is lifted from these films.
 the film of the same name that was a part of the same basic "blaxploitation" genre as that of Foxy Brown and Coffy.

===Casting===
Tarantino wanted Pam Grier to play the title character. She previously read for the Pulp Fiction character Jody, but Tarantino did not believe audiences would find it plausible for Eric Stoltz to yell at her.  Grier did not expect Tarantino to contact her after the success of Pulp Fiction.  When she showed up to read for Jackie Brown, Tarantino had posters of her films in his office. She asked if he had put them up because she was coming to read for his film, and he responded that he was actually planning to take them down before her audition, to avoid making it look like he wanted to impress her. 
 Out of Sight, directed by Steven Soderbergh, an adaptation of a Leonard novel that also featured the character of Ray Nicolett, and waited to see who Tarantino would cast as Nicolet for Jackie Brown.  Michael Keaton was hesitant to take the part of Ray Nicolett, even though Tarantino and Avary wanted him for it.  Keaton subsequently agreed to play Nicolett again in Out of Sight, uncredited, appearing in one brief scene. Although the legal rights to the character were held by Miramax and Tarantino, as Jackie Brown had been produced first, Tarantino insisted that the studio not charge Universal for using the character in Out of Sight, allowing the characters appearance without Miramax receiving financial compensation.

==Reception==

===Critical response===
Jackie Brown received positive reviews. Grier, Forster and Jacksons performances received praise from critics and was considered Grier and Forsters "comeback" by many. Review aggregation website Rotten Tomatoes gives it a score of 86% based on reviews from 74 critics with an average rating of 7.4/10. The sites consensus is: "Tarantinos third film, fashioned as a comeback vehicle for star Pam Grier, offers typical wit and charm—and is typically overstuffed." 

Roger Ebert rated the film as one of his favorites of 1997.  
Movie critic Mark Kermode for BBC Radio Five Live lists Jackie Brown as his favorite film by Quentin Tarantino.  Film co-star and frequent collaborator of Quentin Tarantino, Samuel L. Jackson names this his own favorite of Tarantinos work as well.

===Box office===
The film grossed $39,673,162 in the North American domestic box office on a $12 million budget,    and an additional $35,054,330 added the worldwide gross to $74,727,492. 

===Awards===
   Best Actress Best Actor Best Supporting Grand Prix of the Belgian Syndicate of Cinema Critics. In 2008, the film was selected by Empire (film magazine)|Empire magazine as one of The 500 Greatest Movies of All Time, ranking in at #215.

At the 48th Berlin International Film Festival, Jackson won the Silver Bear for Best Actor award.   

{| class="wikitable plainrowheaders" style="font-size: 95%;"
|- Award
! Date of ceremony Category
! Recipients and nominees Result
|-
! scope="row" | Academy Awards March 23, 1998 Best Supporting Actor
| Robert Forster
|  
|-
! scope="row" rowspan="2" | Berlin International Film Festival February 11 to 22, 1998
| Golden Berlin Bear
| Quentin Tarantino
|  
|-
| Silver Bear for Best Actor
| Samuel L. Jackson
|  
|-
! scope="row" rowspan="2" | Chicago Film Critics Association March 1, 1998 Best Actress
| Pam Grier
|  
|- Best Supporting Actor
| Robert Forster
|  
|-
! scope="row" rowspan="2" | Golden Globe Award January 18, 1998 Best Actor – Motion Picture Musical or Comedy
| Samuel L. Jackson
|  
|- Best Actress – Motion Picture Musical or Comedy
| Pam Grier
|  
|-
! scope="row" rowspan="2" | Saturn Awards
| rowspan="2"| 24th Saturn Awards
| Saturn Award for Best Actress
| Pam Grier
|  
|-
| Saturn Award for Best Supporting Actor
| Robert Forster
|  
|-
| Screen Actors Guild Award March 8, 1998 Outstanding Performance by a Female Actor in a Leading Role
| Pam Grier
|  
|-
|}

==Soundtrack==
 
The soundtrack album for Jackie Brown, entitled  , was released on December 9, 1997.
 Foxy Brown. the film of the same name, and Pam Griers "Long Time Woman", from her 1971 film The Big Doll House. The original soundtrack also features separate tracks with dialogue from the film. Instead of using a new film score, Tarantino incorporated Roy Ayers funk score from the film Coffy.

A number of songs used in the film do not appear on the soundtrack, such as "Cissy Strut" (The Meters) and "Undun" (The Guess Who).

==Home media== deleted and Siskel and Roger Ebert|Eberts review, Jackie Brown appearances on MTV, TV spots and theatrical trailers, written reviews and articles and filmographies, and over an hour of trailers for Pam Grier and Robert Forster films dating from the 1960s onwards. The box also includes a mini-poster of the film, similar to the one above, and on the back of that, two other mini-posters—one of Grier, the other of Forster, both similar to the album cover.

Although the Special Edition DVDs back cover states that the film is presented in a 2.35:1 aspect ratio, it was actually shot with a 1.85:1 ratio, the only Tarantino-directed film to date shot in such a format with the exception of his segment in the film Four Rooms, "The Man from Hollywood".

On October 4, 2011, Miramax released Jackie Brown on Blu-ray Disc along with Pulp Fiction. The film is presented in 1080p HD in its original 1.85:1 aspect ratio with a DTS-HD Master Audio 5.1 soundtrack. The disc was the result of a new licensing deal with Miramax and Lionsgate. 

==Cultural references== novel in which special agent Nicolette appears, although working for the FBI instead of ATF.

==References==
 

==External links==
 
 
*  – official site
* 
* 
* 
* 
* 
* 

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 