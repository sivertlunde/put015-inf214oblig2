Night Visitor
 
{{Infobox Film
| name           = Night Visitor/Never Cry Devil/The Night Visitor
| caption        = 
| image	=	Night Visitor FilmPoster.jpeg
| director       = Rupert Hitzig
| producer       = Alain Silver Tom Broadbridge (Executive Producer) Shelley E. Reid (Executive Producer) Richard Abramites (Associate Producer)
| writer         = Randal Viscovich    
| starring       = Richard Roundtree, Elliott Gould, Shannon Tweed, Michael J. Pollard, Derek Rydall, Allen Garfield
| music          = Parmer Fuller
| cinematography = Peter C. Jensen
| editing        = Glenn Erickson
| distributor    = United Artists Pictures/MGM/UA Home Entertainment/United Artists/Premiere Pictures Corporation 1989
| runtime        = 93 min
| country        = USA English
}}

Night Visitor is a horror film directed by Rupert Hitzig and starring Richard Roundtree, Elliott Gould, Allen Garfield, and Derek Rydall.

==Synopsis==
High school student Billy Colton (Derek Rydall) is spying on his sexy neighbor (Shannon Tweed) one night when he witnesses her being murdered by a man in a robe and a demonic mask. Billy recognizes the man as his unpopular history teacher, but the police do not believe him because he has a history of pranks.

==Cast==
* Richard Roundtree as Captain Crane
* Elliott Gould as Ronald Ron Devereaux
* Michael J. Pollard as Stanley Willard
* Allen Garfield as Zachary Willard
* Derek Rydall as Billy Colton
* Henry Gibson as Jake
* Shannon Tweed as Lisa Grace
* Bruce Kimmelas Townsend
* Brooke Bundyas Mrs. Colton
* Kathleen Bailey as Dolan
* Scott Fultsas Sam Loomis
* Michael Jason Rosen (Michael Rosen) as Bernstein
* Jovanni Brasciaas Tony
* Alain Silver (as Alain Joel Silver)as Thornhill
* Ann Dane as Theresa

==External links==
* 

 
 
 
 
 

 