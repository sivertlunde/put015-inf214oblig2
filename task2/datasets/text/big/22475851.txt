Ip Man 2
 
 
{{Infobox film name = Ip Man 2 image = IpMan2Poster.jpg caption = Theatrical poster alt =   traditional = 葉問2:宗師傳奇 simplified = 叶问2:宗师传奇 pinyin = Yè Wèn Ěr: Zōng Shī Chuán Qí jyutping = Jip6 Man6 Ji6: Zung1 Si1 Zyun6 Kei4}} director = Wilson Yip producer = Raymond Wong writer = Edmond Wong starring =   music = Kenji Kawai cinematography = Poon Hang-sang editing = Cheung Ka-fai studio = distributor = Mandarin Films released =   runtime = 109 minutes country = Hong Kong language = Cantonese  budget = United States dollar|US$12,902,809  gross = US$15,061,802  
}} biographical martial Ip Man, grandmaster of Ip Man, Hong Kong, British colonial rule. He attempts to propagate his discipline of Wing Chun, but faces rivalry from other practitioners, including the local master of Hung Ga martial arts.
 Raymond Wong first announced a sequel before Ip Man  theatrical release in December 2008. For Ip Man 2, the filmmakers intended to focus on the relationship between Ip and his most famed disciple, Bruce Lee. However, they were unable to finalize film rights with Lees descendants and decided to briefly portray Lee as a child. Principal photography for Ip Man 2 began in August 2009 and concluded in November; filming took place inside a studio located in Shanghai. For the sequel, Yip aimed to create a more dramatic martial arts film in terms of story and characterization; Wongs son, screenwriter Edmond Wong, wanted the film to portray how Chinese people were treated by the British and Western perceptions of Chinese martial arts.

Ip Man 2 is the second film in the Ip Man (film series)|"Ip Man" film series. It premiered in Beijing on 21 April 2010, and was released in Hong Kong on 29 April 2010. The film met with positive reviews, with particular praise for the films storytelling and Sammo Hungs martial arts choreography. The film grossed over HK$13 million on its opening weekend, immediately surpassing Ip Man  opening weekend gross. During its theatrical run, Ip Man 2 brought in over HK$43 million domestically, and its domestic theatrical gross made it the highest grossing Hong Kong film released during the first half of 2010. In total, Ip Man 2 grossed an estimated US$15 million worldwide.

== Plot ==
Continuing from where the first film ended, Wing Chun master Ip Man and his family move to Hong Kong in the early 1950s after their escape from Foshan. There, Ip desires to open a school to propagate his art, as well as make a living during the difficult times, but he has difficulty attracting students due to his lack of reputation in the city. One day, a young man named Wong Leung appears and promptly challenges Ip to a fight, but is easily defeated. Wong leaves humiliated, only to return with some friends to gang up on him. Ip beats them as well. Stunned and impressed by his skills, Wong and his friends become Ips first students, bringing more disciples to help the school thrive.

Wong is later caught posting promotional posters for the school by some Hung Gar students. One of them challenges Wong to a fight and loses, but his friends take Wong hostage in revenge and demand a ransom from Ip. Ip goes to the local wet market as directed, but the meeting ends in a confrontation with a growing mob of Hung Ga students. Ip and Wong fight their way outside to meet Jin Shanzhao — the martial artist and former bandit in the first film — who comes to their rescue with his own gang. The students master and head of the coalition of Hong Kong martial arts clubs, Hung Chun-nam, arrives to break up the fight. Ip introduces himself, and Hung informs him that before setting up a school, he needs to attend a special fighting ceremony to test his worth. Ip, Wong and Jin are subsequently arrested by Officer Fatso for disturbing the peace but are later released on bail. Hung and Fatso are then shown to be acting as reluctant collectors for the martial arts schools (including Hungs) as part of a protection racket headed by Superintendent Wallace, a corrupt officer in the Hong Kong police.

Ip attends the ceremony and defeats his first challengers, before striking a draw with the last challenger, Hung. Ip is allowed to keep running his school on the condition that he pay his monthly protection fees, but he declines. Hung thus has his students loiter in front of the Wing Chun School and harass anyone interested, causing a street brawl between them and Ips disciples. Ip is thus forced to close up and move the school nearer to home. Ip soon confronts Hung, who blames him since he wouldnt pay his protection fees, whereas Ip criticizes Hungs management of his students. Hung insists that he is doing what he must and also insists they finish their fight, but during this encounter, Ip stops Hung from accidentally kicking his son as he suddenly appears, earning respect from him. Ip leaves, and the next day, Hung invites him to a British boxing match he has helped to set up, quietly coming to terms with him.

The boxing competition allows for a demonstration by the various martial arts schools to help promote themselves and their Chinese culture. However, the events star boxer, Taylor "The Twister" Milos, an arrogant, racist and brutal man, openly insults and attacks the students, causing chaos as the masters try to restore order. Hung accepts Twisters challenge to a fight so that he can defend his culture. Although Hung has the upper hand at first due to his wider range of skills, in the second round he suffers a misfortunate and devastating blow that severely disorientates him. As he fights on, he begins to weaken from his asthma and is eventually beaten to death by the British boxer, as he refuses to give up and allow the man to insult his culture and people. News of Hungs death rapidly spreads throughout the enraged Chinese populace, causing a scandal that spurs Wallace to hold a press conference where he lied that Hungs death was an accident, that Twister held back during Hungs challenge and that he was a weakling who died after a few punches. Twister announces that he will accept any challenge from the Chinese and rubs it in to the audience that he would kill every Chinese boxer in Hong Kong to prove the supposed superiority of western boxing. Ip Man has already arrived to challenge Twister to a fight.

As his wife goes into labor, Ip finishes training and begins his fight with Twister. Ip exchanges blows with the boxer and seems overwhelmed by the westerners sheer muscle at first, but begins to make a comeback using his more diverse techniques and great speed. He receives an illegal punch from Twister after the second rounds bell, and is also told he will be disqualified for using kicks due to the judges (thanks to Twisters manager) changing the rules during the match. When it looks like the end, Ip remembers Hungs patriotic spirit and is spurred to go on. He changes his strategy and attacks the boxers arms to disable him. The fight is brought to a climactic finish as Ip Man rains blow after blow into the knocked-down Twisters face (reminiscent of the first film), with flashbacks reflecting the latters killing of Master Hung. While the Chinese celebrate, Wallace was finally arrested by his superiors for corruption, as Fatso has secretly reported him. Ip then gives a speech to the audience, stating that despite the differences between their race and culture, he wishes for everyone to respect each other regardless of their status. Both the Western and Chinese audience give him a standing ovation while Twisters manager walks away, unhappy at the defeat. Ip goes home and reunites with his family, including his newborn second son, Ip Ching.

A final scene shows Ip being introduced to a boy named Bruce Lee who wishes to study Wing Chun in order to "beat up people he doesnt like". Ip smiles and simply tells the boy to "come back when he is older".

== Cast ==

=== Main === Ip Man (葉問), the sole practitioner of the martial art Wing Chun. He arrives in Hong Kong with his family during the 1950s to settle there and set up a Wing Chun school.
* Sammo Hung as Hung Chun-nam (洪震南), a Hung Ga master who suffers from asthma. Initially, he is Ip Mans nemesis, but later becomes his friend.
* Huang Xiaoming as Wong Leung (黃梁), Ip Mans first student. This character is based on Wong Shun Leung (a Chinese martial artist from Hong Kong who studied wing chun kung fu under Yip Man).
* Lynn Hung as Cheung Wing-sing (張永成), Ip Mans wife.
* Simon Yam as Chow Ching-chuen (周清泉), Ip Mans friend who appeared in the first movie. He roams the streets of Hong Kong as a beggar with his son.
* Darren Shahlavi as Taylor "The Twister" Milos, a British boxing champion. His Chinese nickname is "Whirlwind" (龍捲風).

=== Supporting ===
* Li Chak as Ip Chun, Ip Mans son.
* Ashton Chen as Tsui Sai-Cheong, Ip Mans student.
* Kent Cheng as Fatso (肥波), a police officer under Superintendent Wallace. He is also Hung Chun-nams close friend.
* Dennis To as Cheng Wai-kei (鄭偉基), a gang leader and student of Hung Chun-nam
* Ngo Ka-nin as Leung Kan (梁根), the chief editor of a news agency whose father was from the same town as Ip Man.
* Louis Fan as Kam Shan-Chau (金山找), a martial artist and robber from the first film who has mended his ways. 
* Calvin Chen as Chow Kwong-yiu (周光耀), Chow Ching-chuens son. He takes care of his disabled father while working at Leung Kans news agency. Charles Mayer as Wallace, also a racist man, corrupted police superintendent and Fatsos superior.
* Lo Mang as Master Law (羅師傅), a Monkey Kung Fu master.
* Fung Hak-on as Master Cheng (鄭師傅), a baguazhang master.

== Production == Ip Man, Ip Man. Raymond Wong Mandarin Films martial arts choreographer. Kenji Kawai reprised his role as the films music composer.   

=== Development ===
Prior to Ip Man  theatrical release in December 2008, producer Raymond Wong announced plans to develop a sequel to the film. The sequel was intended to focus on the relationship between Ip Man and his most famed disciple Bruce Lee. In March 2009, Wong announced that the Lee character might not appear in the sequel,    as producers had not fully finalized negotiations with Lees descendants on the film rights.    In July 2009, it was announced that Ip Man 2 would focus on a young Bruce Lee, prior to Lee becoming Ip Mans most famed disciple.    The sequel continues Ip Mans story, focusing on his move to Hong Kong as he attempts to propagate Wing Chun in the region.    

=== Casting === Fan Siu-Wong reprises his role as Jin Shanzhao, Ips aggressive rival in the first film. In the sequel, Jin attempts to retire from the martial arts world by becoming an ordinary citizen, he later befriends Ip.  In a cameo appearance, Simon Yam reprises his role as Ips friend Chow Ching-chuen, who is now a beggar.    Li Chak reprises his role as Ip Chun, Ip and Wing-sings son.    
 southern Chinese To Yu-hang, who had a supporting role in the first film, appears in the sequel as a different character named Cheng Wai-kei. Cheng is a gang leader practicing Hung Ga, who decides to exact revenge on Wong after Wong defeats Cheng in a fight.    Wilson Yip commented on the casting of the veteran actors as being "a form of tribute to old school kungfu movies."   

Yen and Yip reportedly conducted a worldwide   and 12-year-old Pan Run Kang from Heilongjiang.  On 10 August 2009, it was announced that Jiang Dai Yan would be playing the role of a 10-year-old Bruce Lee.    While the Bruce Lee character makes a brief appearance in the film, director Wilson Yip has expressed interest in making a third film that will focus on the relationship between Ip and Lee.    Yen, however, has stated his lack of interest in making a third film, feeling that Ip Man 2 will "become a classic."   

In November 2008, Yip revealed that there would be another actor appearing in the film, a martial artist who has been a fan of Donnie Yen. Yip commented, "I can only say that he fights even more vehemently than Sammo Hung."    However, in February 2010, it was revealed that British actor and stunt performer Darren Shahlavi would have a supporting role as a boxing opponent fighting against Ip Man.    Yip later stated that Shahlavis character "has his own drama. He is also a personage, not just some random foreign guy that appears from nowhere for the sake of getting beaten up, like you see in other films."  Other cast members include Ngo Ka-nin and Kelvin Cheng. 

=== Writing and story === British colonial rule. Screenwriter Edmond Wong stated that the film also "deals with how Hong Kong people were treated under British colonial rule, and Western attitudes concerning Chinese kung fu." 

Wilson Yip stated that Ip Man 2 is a much better film than its predecessor in terms of characterization and storyline. The film focuses on disputes between the disciples of Hung Ga and Wing Chun martial arts, as well as the conflict and rivalry of the two practitioners. Wing Chun, as taught by Ip Man, is being viewed as a martial art meant only for girls; Hung Ga, as taught by Hung Chun-nam, is being seen as a macho form of boxing.  Of the two characters, Yip commented, "Sammo Hungs character is not exactly villainous, but hes very overbearing, just like his torrential Hung Ga. In contrast, Ip Man is very unassuming, much like his fist."  Yip also stated that the film has some moments of "family drama", such as the ongoing conflict between Ip and his wife Wing-Sing. 

=== Filming ===
Prior to filming, a production ceremony for Ip Man 2 was held in Foshan, receiving plenty of media coverage.  Principal photography began on 11 August 2009; filming took place in a sound stage at Songjiang Studios in Shanghai.   On 28 October 2009, reporters were invited to the set to view the anticipated duel between Donnie Yen and Sammo Hung as it was being filmed.  Filming ended on 8 November 2009. 

=== Stunts and choreography ===
The films martial arts sequences were choreographed by Sammo Hung, who also worked on the choreography for the first film. Prior to principal photography, Hung had undergone a major cardiac surgery. When he returned to the set, his dramatic scenes in the film were filmed first, with his fight sequences being filmed last.  Hung performed his own stunts in the film, which led to him receiving several injuries during filming. While filming a scene, Hung was struck in the face by co-star Darren Shahlavi. He insisted on completing the shoot before going to the hospital. Not wanting his injuries to hinder the production progress, Hung spent five hours trying to complete the scene before going to the hospital for four stitches.    After the completion of filming, Hung expressed that he was dissatisfied with the fight sequences involving his character, presumably due to his heart condition. He also stated that he plans to challenge Yen in a future film: "Although Im the martial arts choreographer, our moves were all rather regulated, being confined by the script. So, I made a pact with Donnie Yen to have a rematch next year if the opportunity arises." 
 wooden dummy. Huang received multiple bruises on his arms, due to his frequent practices on the dummy. Huang would also spend time practicing with the films stunt team.    Wilson Yip praised his performance in the film, stating that Huang "may not be a martial artist, but he specially ordered a wooden dummy, and trained daily at home. In the end, he is doing the action scenes better than Hiroyuki Ikeuchi in the first film." 

=== Film title ===
The Chinese title of the film ( ) literally means Ip Man 2: Legend of a Grandmaster. The title is a play on the first films working title which was Grandmaster Ip Man, a title that was changed when  , was released in January 2013.

== Release ==
Ip Man 2 was released in select Asian countries and in Australia on 29 April 2010.  Prior to its release, Mandarin Films publicly launched the films official website in Beijing on 6 April 2010.  The film held a premiere press conference in Beijing on 21 April 2010, only seven days after the 2010 Yushu earthquake. Guests were asked to wear dark-colored clothing in show of mourning; there was a moment of silence for the victims of the disaster.    The films cast, Donnie Yen, Sammo Hung, Huang Xiaoming, Lynn Hung, and Kent Cheng attended the premiere, and donated a total of Chinese yuan|¥500,000 (United States dollar|US$73,200) to relief efforts helping in the disaster recovery.    The film held private screenings in Chengdu on 21 April 2010, and in China on 27 April 2010, receiving positive reactions from audiences.  Mandarin Films has sold North American distribution rights for the film to distributor Well Go USA.    Ip Man 2 was released in the United States by Variance Films    on 28 January 2011. 

=== Box office ===
In Hong Kong, Ip Man 2 faced competition with the international release of Iron Man 2, which premiered in Hong Kong one day later than Ip Man 2. During its opening weekend, Ip Man 2 grossed Hong Kong dollar|HK$13 million (US$1,736,011),    surpassing Ip Man  opening weekend gross of HK$4.5 million (US$579,715).    The sequel claimed first place at the box office, grossing HK$1 million more than Iron Man 2.    The films revenues decreased by 28.1% in its second weekend, earning HK$9,719,603.56 (US$1,248,996) to remain in first place.    The film dropped 45.7% in its third week, bringing in HK$5,293,401 (US$678,613) while still remaining in first place.    Ip Man 2 continued to stay at number one at the box office, dropping an additional 39.4% in its fourth week and grossing HK$3,199,567 (US$411,115).    During its fifth week, the film moved to fifth place at 79.3%, grossing HK$664,535 (US$85,325).    Ip Man 2 grossed HK$43,268,228.72 (US$5,558,704) domestically.    The sequels domestic gross in Hong Kong puts it ahead of Ip Man  total box office gross of HK$25,581,958.69 (US$3,300,847).   

Ip Man 2 also broke box office records in Singapore. The film was the highest-grossing Hong Kong film to be released in the country, beating a five-year record held by Kung Fu Hustle.      On its opening weekend Ip Man 2 came in second place behind Iron Man 2, grossing Singapore dollar|SG$1.74 million (US$1,264,919).    The films opening weekend gross surpassed Ip Man  2008 weekend gross of SG$827,000 (US$463,946).   

In total, Ip Man 2 has grossed an estimated US$14,856,127 worldwide during its theatrical run. 

=== Performance analysis ===
Analysts believed that Ip Man 2  box office success was related to the favorable reputation and popularity of its first installment.    Huang Qunfei, a general manager of the Chinese theater chain New Film Association Company, made notice of Chinese viewers preferring films made domestically over ones made in Hollywood: "Chinese viewers are less obsessed with Hollywood blockbusters than before. Finally, it is the films quality that matters. With a good story, local films are likely to win more favor among audiences."    Liu Wei of China Daily noted that the films finale was similar to its competition against Iron Man 2 at the box office: "The hero of Ip Man 2...faces up to a Western boxer and knocks him out. Off screen, it is a similar story." 
 Mandarin Films hopes of having the sequel gross over ¥300 million in China was unlikely, due to competition with other films such as Iron Man 2.    Another factor was that the illegal recording, downloading and file sharing of the film would cause a potential loss in revenue.      A pirated version was released online, one week after the films release in China, and attracted more than 10 million online users.    Raymond Wong publicly expressed that he would be pursuing legal action against the originator of the illegal downloads. 
 Alice in Wonderland with HK$44 million.   

=== Critical reception ===
Ip Man 2 received mostly positive reviews from film critics. The film holds an average rating of 7.5/10 from 30,823 users in IMDB.
It currently has a 92% fresh rating on Rotten Tomatoes. Based on 24 reviews, the film currently carries an average rating of 6.8 out of 10.   

Singaporean film critic Genevieve Loh of   of the Chicago Sun-Times awarded Ip Man 2 three stars out of four, writing, "In its direct and sincere approach, its a rebuke to the frenzied editing that reduces so many recent action movies into incomprehensible confusion."   

Darcy Paquet of   awarded the film two stars out of five, writing in her review, "Its no surprise that Donnie Yen isnt willing to sign up for any more Ip Man movies, with the shameless repetition that is happening in these films, even the most ardent fan would be tired."   

=== Home media === theatrical trailers, cast and crew interviews, a making-of featurette, coverage of the films gala premiere, and a shooting diary.

Coinciding with the sequels home video release, both Ip Man and Ip Man 2 were released as a double feature on DVD and Blu-ray Disc. Releases include two-disc special editions of both feature films with a total of four discs on DVD,    as a well as a standard DVD edition featuring both films with a total of two discs.   

==Sequel==
 
The third installment is expected to begin filming in 2015.  Edmond Wong, Raymond Wong and Wilson Yip will return as screenwriter, producer and director again, respectively. Donnie Yen is confirmed to reprise his role as "Ip Man".  In March 2015, The Hollywood Reporter announced that principal photography began; they also revealed that Mike Tyson has been cast in a role and Bruce Lee will appear in Computer-generated imagery|CGI. 

==See also==
* The Legend Is Born – Ip Man
* The Grandmaster (film)|The Grandmaster (film)
*  
* Ip Man (TV series)|Ip Man (TV series)

== References ==
 

== External links ==
*  
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 