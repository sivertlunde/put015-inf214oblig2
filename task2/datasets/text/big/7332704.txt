Automatons
 
{{Infobox film
| name           = Automatons
| image_size     = 
| image	=	Automatons FilmPoster.jpeg
| caption        = 
| director       = James Felix McKenney
| producer       = James Felix McKenney, Lisa Wisely
| writer         = James Felix McKenney
| narrator       = 
| starring       = Christine Spencer Angus Scrimm Brenda Cooney The Noisettes
| cinematography = David W. Hale
| editing        = James Felix McKenney
| distributor    = 
| released       =  
| runtime        = 83 minutes
| country        = United States
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
Automatons is a 2006 black-and-white horror film about a war against robots. The movie was made under the working title Death to the Automatons. Christine Spencer, Angus Scrimm, and Brenda Cooney star. John Levene, Don Wood and Executive Producer Larry Fessenden have supporting roles. The film was directed by James Felix McKenney. The tagline was, "Men started this war. The machines will finish it." 

==Plot==
Somewhere in the distant future, The Girl is alone. She is the last of her people, the others having died in a generations-long war that the girl continues to fight with the assistance of a group of antiquated robot helpers and soldiers.

Her only connection to her long-dead people is a collection of recorded journal entries made by the scientist who cared for her as a baby. His is the only friendly human face she’s ever seen. The regular transmissions from her enemys leader are always filled with threats and taunts. The girl responds with an attack of her own, carried out by her mechanical soldiers on the contaminated surface where no human can survive.

==Cast==
*Christine Spencer
*Brenda Cooney
*Angus Scrimm
*Don Wood

==References==
 

==External links==
*  
* 
*  at Allmovie
* 

 
 
 


 