Khulay Aasman Ke Neechay
 
{{Infobox film
| name           = Khulay Asmaan Kay Neechay
| image          = Khulay Asmman Kay Neechay.png
| image_size     =
| caption        =
| director       = Javed Sheikh
| producer       = Javed Sheikh
| writer         = Babar Kashmiri Javed Sheikh Sana Meera Meera Humayun Saeed Behroze Sabzwari Badar Khalil Ismail Tara Javed Sheikh
| music          = Amjad Bobby(late) and Jeet Gannguli
| cinematography = Waqar Bukhari
| editing        = Akiv Ali
| released       =  
| runtime        =
| country        = Pakistan
| language       = Urdu
| budget         =
| domestic gross =
}}

Khulay Asmaan Kay Neechay (lit. Beneath the Open Sky) is a Pakistani movie directed by  , UAE, India and Pakistan. Sheikh said about his film, “They (Pakistanis) will all be proud of my film and will own up to it as a Pakistani product as it is a perfect family film.”   

== Synopsis ==

The story revolves around a young man who marries a young woman of his choice against the wishes of his family. His father had dotingly wanted him to marry the daughter of his family member and she, in  innocence, has lived this dream.

But the path of true love is seldom smooth. Over emotional turmoil, family drama and poignant scenes, true love triumphs over all because matches are made in heaven.

==Songs==
* Allah Allah Dulhan
* Dhoop Bahri
* Dil Day Tah
* Her Dil Mein Dil Bar
* Ladki Mombai Ke
* Tere Arzoo

==Budget==
The film was the most expensive Pakistani film ever till 2008.   

== Cast and crew ==
*Cast: Saleem Sheikh, Sana (actress)|Sana, Humayun Saeed, Nadeem Baig|Nadeem, Behroze Sabzwari, Javed Sheikh, Meera (actress)|Meera, Badar Khalil, Ismail Tara  vaishali mishra
*Director: Javed Sheikh
*Producer: Javed Sheikh
*Choreographer: Ganesh Acharaya
*Music: Amjad Bobby, Jeet Gannguli
*Lyrics: Nida Fazli
*Playback singers: Alka Yagnik, Sonu Nigam
*Cinematography: Waqar Bukhari

==References==
 

== External links ==
*  
*  

 
 
 
 