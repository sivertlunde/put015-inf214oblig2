Varthamana Kalam
{{Infobox film
| name           = Varthamanakaalam (1990)
| image          =
| caption        =
| director       = I V Sasi
| producer       = Liberty Basheer for Liberty Films
| writer         =Prasanth
| screenplay     =
| starring       = Suresh Gopi, Jayaram, Urvashi, Balachandra Menon, Ummer, Sidique, Soman, Suma Jayaram etc Johnson
| cinematography =Santhosh Sivan
| editing        =K.Narayanan
| studio         =
| distributor    =
| released       = January 1990
| country        = India Malayalam
}}
 1990 Cinema Indian Malayalam Malayalam film, Urvashi in lead roles. The film had musical score by Johnson (composer)|Johnson.   

==Plot==
Arundhathi Menon (Urvashi (actress)|Urvashi) arrives at a cottage that was gifted by her boyfriend Jameskutty. That night, her memories travels back to childhood and teenage days. Arundhathi, single daughter of Menon master (Jagannatha Varma)is full of life. She gets admission in a college in city and is accompanied by Bramhadathan Embranthiri (Jayaram), her neighbor. However, her dreams get shattered when Bramhadathan hands her over to a businessman. She is raped by the businessman who offers Bramhadathan a job in return. Arundhathis father commits suicide after the incident. Alone, Arundhathi had to go several sexual encounters from hooligans. One night, while in an attempt to save herself, Arundhathi falls into the net of a lady pimp nicknamed mummy. She meets Jameskutty (Balachandramenon), an ad film maker who falls in love with her. Jameskutty saves her from brothel by making her his concubine. However, things get an ugly turn when George Thomas alias GT (Lalu Alex), a friend of Jameskutty sets his eyes on Arundhathi. GT and gang attacks Jameskutty to kidnap Arundhathi. However, they are defeated by the valiant defence of Jameskutty, who but vomits blood after the fight. He discloses Arundhathi that he is a heart patient and may die soon. Arundhathi, who is now pregnant with the child of James Kutty is shattered by the news. Jameskutty dies after keeping her as nominee of all his wealth. Arundhathi is brutaly raped by GT and gang at a cemetery and this incident leads to her abortion. Arundhathi bids goodbye to the city and is back in the village to lead a retired life. She meets Balagopalan (Suresh Gopi), her childhood friend who is now separated from wife (Leena Nair). Though Balagopalan is attracted to Arundhati, she stays away from him. Meanwhile Balagopalans wife is back, making Arundhathi leave the village for ever. On her return journey, she finds GT and gang assaulting a girl. In an attempt to save her, Arundhathi runs over her car over GT and kills him. She waits inside while police arrives to arrest her for the murder. Bramhadathan, who is now a reporter also arrives at the scene to cover the news and is shocked to see Arundhathi while credits starts rolling.  
==Cast==
 
*Balachandramenon as Jameskutty
*Suresh Gopi as Balagopalan
*Jayaram as Brahmadathan Urvashi as Arundhathi Menon
*M. G. Soman as Ravunni Master
*K. P. Ummer as K. P. Menon
*Paravoor Bharathan as Thomachan
*Babu Namboothiri as Shekhara Pillai
*Lalu Alex as George Thomas
*Prathapachandran Siddique
*Sukumari as Mrs. Menon
 

==Soundtrack==
The music was composed by Johnson (composer)|Johnson. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Chithra || Sreekumaran Thampi || 00.00
|- Chithra || Sreekumaran Thampi ||
|- Chithra || Sreekumaran Thampi ||
|-
| 4 || Oru thari velicham|| m g sreekumar || Sreekumaran Thampi ||
|-
| 5 || Vasanthathin || Venugopal || Sreekumaran Thampi ||
|}

==References==
 

==External links==
*  

 
 
 


 