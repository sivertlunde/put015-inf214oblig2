Incident at Oglala
{{Infobox film
| name           = Incident at Oglala
| image          = Incident_at_oglala.jpg
| caption        = Theatrical release poster
| director       = Michael Apted
| producer       = Arthur Chobanian
| writer         =
| narrator       = Robert Redford
| starring       =  Norman Zigrossi Robert Sikma Darelle Butler Bob Robideau Norman Brown Leonard Peltier
| music          =
| cinematography = Maryse Alberti
| editing        = Susanne Rostock
| distributor    = Miramax Films
| released       =  
| runtime        = 89 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $536,848   
}}
 Ronald A. Williams, on the Pine Ridge Indian Reservation in the summer of 1975.  
 Dick Wilson.

== Background == Americanized Sioux. The American Indian Movement (AIM) was invited to the reservation to help assert traditional values. It was headquartered at Calvin Jumping Bulls property on the southern edge of Oglala, South Dakota|Oglala. The "incident at Oglala" was precipitated by the FBI investigation of a pair of stolen boots. Jimmy Eagle, one of the AIM teenagers, was thought to have taken a pair of boots after a fight, and two FBI agents, wanting to talk to him about it, pursued a vehicle they thought he was driving into the AIM camp.

==Accolades==
Apted was nominated for the Critics Award in 1992 for the film.

==See also==
*Wounded Knee incident
*Thunderheart

==References==
 

== External links ==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 