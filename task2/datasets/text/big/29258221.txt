On the Night of the Fire
{{Infobox film
| name           = On the Night of the Fire The Fugitive
| image          = On the Night of the Fire film poster.jpg
| caption        = 
| director       = Brian Desmond Hurst
| producer       = Josef Somlo Terence Young F. L. Green (novel)
| starring       = Ralph Richardson Diana Wynyard
| cinematography = Günther Krampf
| music          = Miklós Rózsa
| distributor    = GFD
| released       = 26 October 1939 (UK) 22 July 1940 (U.S.)
| runtime        = 94 minutes
| country        = United Kingdom English
}} 1939 British thriller, directed by Brian Desmond Hurst and starring Ralph Richardson and Diana Wynyard.  The film is based on the novel of the same name by F. L. Green.  It was shot on location in Newcastle upon Tyne and was released shortly after the outbreak of World War II.  It is regarded as an early example of British film noir, with the kind of subject matter and filming style which fell completely out of favour during the war years &ndash; when British studios felt that cinemagoers would want either light entertainment and escapism or topical patriotic propaganda pieces &ndash; and would not be taken up again until the later 1940s.
 David Quinlan describes the film as "grim but gripping".   Andrew Spicer, in his book European Film Noir, writes: "A riveting psychological study. With its sustained doom-laden atmosphere, Krampf’s expressive cinematography, its adroit mixture of location shooting and Gothic compositions and Richardson’s wonderful performance as a lower middle-class Everyman, On the Night of the Fire clearly shows that an achieved mastery of film noir existed in British cinema". 

==Plot==
 Struggling Tyneside barber Will Kobling (Richardson) is in financial trouble.  One evening, opportunistically and on impulse, he steals £100 from a local factory where a window has been accidentally left open.  He hopes the money will represent a new start for him and wife Kit (Wynyard).  His hopes are soon dashed when Kit confesses to being heavily in debt to local draper Pilleger (Henry Oscar), who has been pressuring her to pay up.  Most of the stolen cash has to go on settling Kits debt.

Pilleger banks the money, only to receive a visit from the police to inform him that the serial numbers of the notes match those stolen from the factory.  He professes himself an innocent party, claiming not to know which of his many customers the ill-gotten cash came from, and the police have to let the matter drop.  Pilleger comes up with a scheme to blackmail Kobling, promising silence in return for a payment of £3 per week.  Kobling is horrified to contemplate the extra financial burden being placed on him indefinitely, but sees no option but to consent. 

Some time later, and facing the loss of his business through lack of ready cash, Kobling decides to call Pillegers bluff.  An ideal opportunity presents itself when a huge fire breaks out in the neighbourhood, leading to chaos which distracts the police and the local population.  He confronts Pilleger and a fight breaks out, ending in Pillegers death.  The police suspect that Kobling is involved and begin to use psychological tactics to break him down, but he remains grimly silent and sends Kit and their baby to stay with her sister.  

Unknown to Kobling, his fight with Pilleger was witnessed by Lizzie Crane (Mary Clare), a well-known local eccentric, and she begins to tattle about what she saw.  The populace start to shun Kobling and bay for justice, but the police do not believe Lizzies word will stand up as evidence, and still do not think they can arrest him on that alone.  As they continue to put pressure on him, Kobling comes close to breaking point.  He is finally pushed over the edge when he receives the dreadful news that Kit and the baby have been killed in a road accident.

==Cast==
* Ralph Richardson as Will Kobling
* Diana Wynyard as Kit Cobling
* Romney Brent as Jimsey Jones
* Mary Clare as Lizzie Crane
* Henry Oscar as Pilleger
* Dave Crowley as Jim Smith
* Gertrude Musgrove as Dora Smith
* Frederick Leister as Inspector
* Ivan Brandt as Wilson
* Sara Allgood as Charwoman
* Glynis Johns as Mary Carr
* Amy Dalby as Hospital Nurse 
* Irene Handl as Neighbour 
* Maire ONeill as Neighbour

==References==
 

== External links ==
*  
*   at BFI Film & TV Database
*   
*  

 
 

 
 
 
 
 
 
 
 
 
 
 