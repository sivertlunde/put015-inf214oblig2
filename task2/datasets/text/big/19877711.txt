Biloxi Blues (film)
 
{{Infobox film
| name           = Biloxi Blues
| image          = Biloxi_Blues.jpg
| caption        = Original poster
| director       = Mike Nichols
| producer       = Ray Stark
| writer         = Neil Simon
| based on       = Biloxi Blues by Neil Simon Corey Parker Casey Siemaszko Penelope Ann Miller Park Overall Alan Pottinger Mark Evan Jacobs
| music          = Georges Delerue Bill Butler
| editing        = Sam OSteen
| studio         = Rastar
| distributor    = Universal Pictures
| country        = United States
| released       =  
| runtime        = 107 minutes
| language       = English
| budget         = $20 million 
| gross          = $51,684,798 (worldwide)   
}} play of Brighton Beach Broadway Bound.

==Synopsis==
The story centers on Eugene Morris Jerome, a 20-year-old Jewish Brooklynite who is drafted into the United States Army during the last year of World War II and is sent to Biloxi, Mississippi for basic training. While there he learns to cope with fellow soldiers from all walks of life, falls in love, and loses his virginity in less than ideal circumstances, all while having to cope with an eccentric drill instructor.

==Cast==
*Matthew Broderick as Eugene Morris Jerome
*Christopher Walken as Sergeant First Class Merwin J. Toomey
*Michael Dolan as James J. Hennesey
*Markus Flanagan as Roy W. Selridge
*Matt Mulhern as Joseph T. Wykowski Corey Parker as Arnold B. Epstein
*Casey Siemaszko as Donald J. Carney
*Penelope Ann Miller as Daisy
*Park Overall as Rowena
*Alan Pottnger as Peek
*Mark Evan Jacobs as Pinelli
*Christopher Phelps as Private Phelps

==Soundtrack== Morgan Lewis Blue Moon" by Richard Rodgers and Lorenz Hart, "Marie" by Irving Berlin, "Solitude" by Duke Ellington, Irving Mills, and Edgar DeLange, "Chattanooga Choo Choo" by Harry Warren and Mack Gordon, and "Dont Sit Under the Apple Tree (With Anyone Else but Me)" by Sam H. Stept, Charles Tobias and Lew Brown.

==Reception==
=== Critical ===
Biloxi Blues received generally positive reviews from critics. It currently holds an 81% on Rotten Tomatoes based on 26 reviews.

Vincent Canby of The New York Times called the film "a very classy movie, directed and toned up by Mike Nichols so theres not an ounce of fat in it." He added, "Mr. Nichols keeps the comedy small, precise and spare. Further, the humor is never flattened by the complex logistics of movie making, nor inflated to justify them."  Rita Kempley of the Washington Post thought the film was "an endearing adaptation" and "overall Nichols, Simon and especially Broderick find fresh threads in the old fatigues" despite some "fallow spells and sugary interludes." 

Variety (magazine)|Variety called it "an agreeable but hardly inspired film" and added, "Even with high-powered talents Mike Nichols and Matthew Broderick aboard,   World War II barracks comedy provokes just mild laughs and smiles rather than the guffaws Simons work often elicits in the theater." 

Roger Ebert of the Chicago Sun-Times called the film "pale, shallow, unconvincing and predictable" and added, "nothing in this movie seems fresh, well-observed, deeply felt or even much thought about ... Its just a series of setups and camera moves and limp dialogue and stock characters who are dragged on to do their business." 

===Box office===
The film opened on 1,239 screens in the US and earned $7,093,325 on its opening weekend, ranking #1 at the box office. It eventually grossed $43,184,798 in the US and $8,500,000 in foreign markets for a total worldwide box office of $51,684,798. 

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 