Bhole Shankar
 
 
{{Infobox film
| name           = Bhole Shankar
| image          = Bhole Shankar.jpg
| director       = Pankaj Shukla
| producer       = Gulshan Bhatia
| writer         = Pankaj Shukla
| narrator       = Manoj Tiwari Antara Mona Lisa Biswas Master Shivendu
| music          = Dhananjay Mishra
| cinematography = Raju Kaygee
| editing        = Prashant-Komal
| genre          = Family drama
| distributor    = Abhay Sinha
| released       = 12 September 2008
| runtime        =
| country        = India
| language       = Bhojpuri
| awards         =
| budget         =
}}
 2008 Cinema Indian feature directed by Manoj Tiwari in lead roles. The film took the biggest opening for a Bhojpuri film and is considered as the biggest Bhojpuri hit.   

== Synopsis ==
Bhole Shankar revolves around the unemployment issue where Mithun Chakraborty plays the role of Shankar, an underworld Don and the elder brother of Bhole, played by Manoj Tiwari. Bhole Shankar  is the first Bhojpuri film of Mithun Chakraborty.

== Cast ==
*Mithun Chakraborty as Shankar
*Manoj Tiwari as Bhole
*Anatara "Mona Lisa" Biswas
*Lovy Rohtagi
*Gopal Singh
*Shabnam Kapoo
*Rajesh Vivek R
*Shivendu Shukla
*Raghvendra MudgalR
*Nishtha
*Ujjwal

==Soundtrack==
The films music was composed by Dhananjay Mishra and the lyrics were written by Bipin Bahaar. Playback singer Shailender Singh came out of retirement to perform the song "Jai Ho Chhath Maiya" for the film.
#"Re Bauraee Chanchal Kirniya" – Raja Hasan
#"Dil Ke Haal Bataeen Kaise"  – Mauli Dave
#"Tori Marji" -Poonam Yadav, Manoj Tiwari
#"Piya More Gaile Rama" – Indu Sonali
#"Naak Ke Nathiya" – Manoj Tiwari
#"Kehu Sapna Mein" – Kalpana, Udit Narayan Shailendra Singh, Malini Awasthi
#"Jab Jab Aave Yaad" – Indu Sonali, Ujjaini Mukherjee|Ujjaini, Manoj Tiwari
#"Ae Bhai Ji" – Manoj Tiwari

===Box office===
Although, the film was initially banned in Bihar,  it earned more than Rs. 35 Lac. in Bihar alone in its opening weekend, a record for any Bhojpuri film in the state. The film went on to complete 100 days run at various cinema halls. The film got very good response even from Non-Bhojpuri speaking audience and film was running well after one year in many parts of Northern India. This was the first Bhojpuri film to get a simultaneous release in overseas market like Nepal, Fiji, Mauritius and Guiana.

==References==
 

== External links ==

 
 
 
 