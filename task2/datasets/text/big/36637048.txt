Vuelve el ojo de vidrio
{{Infobox film
| name           = Vuelve el ojo de vidrio
| image          = Vuelve el ojo de vidrio movie poster.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Lobby card
| director       = René Cardona Jr.
| producer       = Jacobo Derechín
| writer         = 
| screenplay     = Alfredo Varela, Jr.
| story          = Alfredo Varela, Jr. Antonio Aguilar
| based on       =   
| narrator       = 
| starring       = Antonio Aguilar Flor Silvestre Manuel Capetillo Eleazar García Alejandro Reyna Guillermo Rivas
| music          = Enrico C. Cabiati
| cinematography = Raúl Domínguez
| editing        = Federico Landeros
| studio         = Estudios América
| distributor    = Cinematográfica Águila
| released       =  
| runtime        = 107 minutes
| country        = Mexico
| language       = Spanish
| budget         = 
| gross          = 
}} El ojo de vidrio.

==Cast==
*Antonio Aguilar as Porfirio Alcalá y Buenavista "El Ojo de Vidrio"
*Flor Silvestre as María "La Coralillo"
*Manuel Capetillo as Gumaro Buenavista
*Eleazar García as Chelelo Buenavista 
*Alejandro Reyna as Plácido Buenavista 
*Guillermo Rivas as Jerónimo Buenavista  Arturo Martínez as Melitón Barbosa
*Yuyú Varela as Socorro González "La Cocorito" 
*Alfredo Varela, Jr. as Mr. Fregoli "Fregolini"
*Eduardo Alcázar as Colonel
*Emma Arvizu as Colonels Wife Jorge Cabrera		
*Miguel Ángel Gómez	as Melitóns Friend
*René Cardona Jr. as Mission Priest
*Mario García "Harapos" as Carrancista Soldier
*Fernando Durán Rojas (as Fernando Durán) Juan Hernández		
*Jesús Gómez (actor)|Jesús Gómez as Carrancista Sentinel Carlos Suárez		
*Fídel Castañeda		
*Salvador Aguilar		 Roberto Iglesias	

==External links==
* 

 
 
 


 