Three Smart Saps
{{Infobox film|
  | name           = Three Smart Saps |
  | image          = Lobby42threesmartsaps.jpg|
  | caption        = |
  | director       = Jules White |
  | producer       = Del Lord Hugh McCollum |
  | writer         = Clyde Bruckman | Barbara Slater Frank Coleman John Tyrrell Victor Travers Eddie Laughton Lew Davis|
  | cinematography = Benjamin H. Kline | 
  | editing        = Jerome Thoms |
  | distributor    = Columbia Pictures |
  | released       =   |
  | runtime        = 16 40" |
  | country        = United States
  | language       = English
}}

Three Smart Saps is the 64th short film starring American slapstick comedy team the Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959.

== Plot == John Tyrrell) out of jail. Apparently, the father is a prison warden who has been overthrown and put behind bars by the local mafia. The Stooges manage to sneak into the prison, find the father-in-law to be, and start snapping as many incriminating photos of the mafias party as possible. As a result, the real crooks are served justice, and the Stooges marry their sweethearts.

== Production notes ==
Three Smart Saps was filmed on April 7-10, 1942. This is the seventh of sixteen Stooge shorts with the word "three" in the title.     The films title is a play on the 1936 musical comedy film Three Smart Girls. 
 Leavenworth and Sing Sing, all well-known prisons of the day.   
 The Freshman.   

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 


 