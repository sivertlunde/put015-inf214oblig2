Yathrakarude Sradhakku
{{Infobox film
| name           = Yathrakarude Sradhakku
| image          = Yathrakarude Sradhakku.jpg
| director       = Sathyan Anthikad Sreenivasan
| Innocent Sreenivasan Sreenivasan
| Johnson
| cinematography = Vipin Mohan
| editing        =  K. Rajagopal  
| released       =  
| country        = India
| language       = Malayalam
}} Innocent in Keralite peoples conservative stance toward relationships between men and women. Yathrakarude Sradhakku is one of Soundaryas final films before her death two years later in an aircraft accident.

==Plot==
Ramanujan (Jayaram) meets a software engineer named Jyothi (Soundarya) on the train returning to Chennai. She has been engaged to Dr. Pradeep (Siddique (actor)|Sidhique) for many years. After they arrive in Chennai, Ramanujan asks his best friend Paul (Innocent (actor)|Innocent) to find Jyothi a new home. Jyothi is not married and is therefore unable to legally own a home, so when she receives a home from Paul she lies and tells the owner that she is married to Ramanujan. When she has to leave the home, she stays at Ramanujans house. They convince the neighbors that they are a couple. Ramanujan falls in love with Jyothi, but he understands that she is already engaged. She returns to Kerala for her engagement, and Ramanujan and Paul both attend the engagement party. Paul gets drunk at the party and tells everyone that Ramanujan and Jyothi were living in a single home together in Chennai, ending Jyothis relationship and forcing her to marry Ramanujan.

Upon returning to Chennai, Jyothi stops talking to Ramanujan despite his best attempts to please her. He returns to Kerala one day and Paul tells her that Ramanujans mother has died. Instead, Ramanujan brings Dr. Pradeep back to Chennai, saying that Dr. Pradeep and Jyothi can start a new life together if they wish. Jyothi understands Ramanujans real love and pardons him, choosing to continue her life with him.

==Cast==
*Jayaram as Ramanujan 
*Soundarya as Jyothi  Innocent as Paul  Sreenivasan as Gopi 
*Nedumudi Venu as Viswanathan 
*Oduvil Unnikrishnan as K.G. Nambiar 
*T. P. Madhavan as K.K. Karthikeyan  Siddique as Dr. Pradeep 
*C. I. Paul as Pradeeps Father 
*V. K. Sriraman as Isaac 
*Mamukkoya as Kunjali 
*Vettukili Prakash as Office boy

==Reception==
The film mostly received positive reviews. Veena Pradeep of the Deccan Herald said, "A new theme, good performances, Sreenivasan’s and Innocent’s comic interludes and some good music keep the audience engaged throughout the film." 

==Soundtrack==
{{Infobox album
| Name        = Yathrakarude Sradhakku
| Type        = Soundtrack
| Artist      = Johnson (composer) | Johnson 
| Language = Malayalam
| Genre       = Film
|}}

The film features songs composed by Johnson (composer) | Johnson  and written by Kaithapram.

{| class="wikitable"
|-
! Song Title !! Singer
|-
| Nombarakkoottile ||  Madhu Balakrishnan
|-
| Onnu Thodanullil ||  Jayachandran | P Jayachandran
|- Jyotsna
|-
| Vattayilappanthalittu ||  KS Chithra, Jayachandran | P Jayachandran
|}

==References==
 

==External links==
*  

 
 
 
 
 