Oru Naal Varum
{{Infobox film
| name           = Oru Naal Varum
| image          = Oru Naal Varum poster.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = T. K. Rajeev Kumar
| producer       = Maniyanpilla Raju Sreenivasan
| starring       =  
| music          = M. G. Sreekumar 
| cinematography = Manoj Pillai
| editing        = B. Ajith Kumar
| studio         = Maniyanpilla Raju Productions
| distributor    = Damor Cinema & PJ Entertainments UK
| released       =  
| runtime        = 144 min
| country        = India
| language       = Malayalam and tamil
| budget         = 
| gross          = 
}}
Oru Naal Varum ( ;  ) is a 2010 Malayalam satirical film produced by Maniyanpilla Raju and directed by T. K. Rajeev Kumar. It was scripted by Sreenivasan (actor)|Sreenivasan. It features Mohanlal and Sameera Reddy in the lead role. The film marks the Malayalam film debut of the Bollywood actress Sameera Reddy. 

==Plot== Kutralam  Waterfalls. Soon it is revealed that Gopi Krishnan is a corrupt assistant town planner using a government vehicle to come on holiday in Tamil Nadu during his duty hours. As his driver (Suraj Venjaramood) takes him back to his station in Kerala he has an altercation with protagonist Sukumaran (Mohanlal), whose identity and profession are not yet revealed.
 vigilance officer named Nandhakumar trying to arrest Gopi Krishnan in a corruption case. There is also a sub-plot involving the child custody battle between Nandhakumar and his wife Meera (Sameera Reddy).

Nandhakumar makes three attempts to get evidence against Gopi Krishnan. The first two fail, because Nandhakumars subordinate Sunny leaked the plans to Gopi Krishnan. Vasudevan commits suicide after the first failure. Finally after catching Sunny, the third attempt succeeds (though there is confusion over the evidence and some thugs attempt to murder Gopi Krishnan while he is in police custody). However Nandhakumar loses his court case against his wife and has to give up his daughter. Faced with the collapse of his corrupt life, Gopi Krishnan agrees to work with Nandhakumar to root out corrupt real estate developers. The film closes with Meera returning Nandhakumars daughter to him, saying that she could not handle her crying for her father all the time.

==Cast==
* Mohanlal as Kulappulli Sukumaran / DYSP Nandhakumar
* Sameera Reddy as Meera Sreenivasan as Gopi Krishnan Devayani as Rajalekshmi
* Nedumudi Venu as Havildar Vasudevan
* Maniyanpilla Raju as businessman
* Nazriya Nazim as Dhanya 
* Suraj Venjaramood as driver
*T. P. Madhavan
* Ambika Mohan as Family Court Judge
* Baby Ester as Nandhakumars daughter

==Soundtrack==
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! No !! Song !! Singer !! Lyrics 
|-
| 1
| Padan Ninakkoru
| M. G. Sreekumar, K. S. Chithra
| Murukan Kattakada
|-
| 2
| Oru Kandan Poocha Varunne
| Vidhu Prathap
| Murukan Kattakada
|- 
| 3
| Maavin Chottile Manamulla
| Shweta Mohan
| Murukan Kattakada
|-
| 4
| Naathoone Naathoone
| Mohanlal|Dr. Mohanlal, Rimi Tomy
| Murukan Kattakada
|-
| 5
| Padan Ninakkoru
| K. S. Chithra
| Murukan Kattakada
|-
| 6
| Pranaya Nilavite
| Nishad, Preethi Warrier
| Murukan Kattakada
|-
| 7
| Maavin Chottile Manamulla
| M. G. Sreekumar
| Murukan Kattakada
|}

==Awards==
;Kerala State Film Award Best Comedy Artist - Suraj Venjaramood

;Asianet Film Awards
* Best Lyricist - Murukan Kattakada
* Best Music Director - M. G. Sreekumar

;Asiavision Movie Awards
* Best Socially Committed Movie 
* Best Music Director - M. G. Sreekumar

;Jaihind TV Film Awards
* Best Music Director - M. G. Sreekumar
* Best Female Playback Singer - Shweta Mohan
* Best Comedy Artist - Suraj Venjaramood

==References==
 

== External links ==
*  
* 
* http://www.nowrunning.com/movie/7209/malayalam/oru-naal-varum/index.htm
* http://www.indiaglitz.com/channels/malayalam/preview/11850.html
* http://popcorn.oneindia.in/title/7823/oru-naal-varum.html
* http://sify.com/movies/malayalam/review.php?id=14948942&ctid=5&cid=2428
* http://movies.rediff.com/review/2010/jul/12/south-oru-naal-varum-review.htm
* http://www.indianexpress.com/news/orunaal-varum-family-hit/649285/

 
 
 
 
 
 