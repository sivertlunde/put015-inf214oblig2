Past Perfect (film)
 
{{Infobox film
| name           = Past Perfect
| image          = Passatoprossimo.jpg
| caption        = Film poster
| director       = Maria Sole Tognazzi
| producer       = 
| writer         = Daniele Prato
| starring       = Valentina Cervi Andrea Guerra
| cinematography = Giulio Pietromarchi
| editing        = 
| distributor    = 
| released       =  
| runtime        = 97 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}

Past Perfect ( ) is a 2003 Italian comedy film directed by Maria Sole Tognazzi. It was entered into the 25th Moscow International Film Festival.   

==Cast==
* Valentina Cervi as Carola Baiardo
* Paola Cortellesi as Claudia
* Claudio Gioè as Giammaria
* Ignazio Oliva as Edoardo Stella
* Claudio Santamaria as Andrea
* Alessia Barela as Monica
* Francesca Figus as Francesca
* Francine Berting as Elisa - Edoardos Fiancée (as Francina Berting)
* Francesca Borelli as Carolas Friend (as Francesca Haydèe Borelli)
* Giorgio Colangeli as Traffic Policeman
* Stefano Venturi as Vittorio Badaloni - the Assistant Director
* Pierfrancesco Favino as Filippo
* Gianmarco Tognazzi as Alberto

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 