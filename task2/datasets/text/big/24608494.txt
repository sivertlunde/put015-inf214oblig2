Tribute This!
{{multiple issues|
 
 
}}
{{Infobox film name           = Tribute This!  image          =  caption        =  director       = Mick McCleery starring       = Billy Franks, Gary Joseph, Mick McCleery, Mike Nicholson producers      = Gary Joseph, Mike Nicholson music    = Billy Franks released       =   runtime        = 89 minutes country        = United States language       = English budget         = $100,000
}}
Tribute This! is a 2008 documentary film directed by Mick McCleery and produced by Gary Joseph and Mike Nicholson. The subject of the film is British singer/songwriter Billy Franks (former frontman for the London 80s band The Faith Brothers).

The film follows the real life, 5 country trek that McCleery, Joseph and Nicholson took, along with Franks as they tried to track down 10 famous singers (Huey Lewis, Bruce Springsteen, Elvis Costello, Steve Earle, Aaron Neville, Rod Stewart, Tom Petty, Jon Bon Jovi, Bryan Adams and Paul McCartney) and ask them if they each would be willing to record 1 track for a tribute album to the mostly unknown Franks, a fellow songwriter who has given his life to his craft but is still an unknown. All proceeds from the album being earmarked for the UK music charity Youth Music.

Although the film is mostly a comedy road trip it also explores the deeper issues of the arbitrary nature of celebrity, the lifelong pursuit of dreams and just how important friendships can be.

==External links==
*  
*  

 
 
 
 
 

 