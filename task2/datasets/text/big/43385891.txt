Fort McCoy (film)
{{Infobox film
| name           = Fort McCoy
| image          = 
| alt            = 
| caption        = 
| director       = Kate Connor, Michael Worth
| producer       = Kate Connor, Eric Stoltz 
| writer         = Kate Connor
| starring       = Eric Stoltz  Kate Connor  Lyndsy Fonseca  Any Hirsch  Camryn Manheim  Seymour Cassel  Brendan Fehr   Rene Heger 
| music          = Dana Niu
| cinematography = Neil Lisk 
| editing        = Robert Brakey 
| studio         = Marzipan Entertainment
| distributor    = Monterey Media
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = $12,303 
}}
Fort McCoy is a 2011 American drama written by Kate Connor, Produced by Kate Connor and Eric Stoltz, and starring Eric Stoltz, Kate Connor, Lyndsy Fonseca, Any Hirsch, Camryn Manheim, Seymour Cassel, and Brendan Fehr. 

==Plot==
In 1944 Frank Stirn moves his family to become a barber for the American Army and a Prisoner of War camp at Fort McCoy in Wisconsin. Bitter that he is unable to fight, Frank takes a stand when a Nazi Officer threatens his wife.

==Production==
The movie was filmed in California, and La Crosse, Wisconsin.

==Release==
The film was first shown at Boston, Cannes Independent, Rhode Island, Savannah film festivals. The U.S. and Canadian rights were acquired by Monterey Media in March 2014.
 

===Festivals===
Fort McCoy was shown at the following festivals:
*Rhode Island International Film Festival 
*Savannah Film Festival 
*Hollywood Film Festival 
*Cannes Independent Film Festival
*TriMedia Film Festival 
*Worldfest Houston 
*Milan Intl Film Festival
*Boston Film Festival
*Ft Lauderdale Intl Film Festival 
*Stony Brook Film Festival
*Tacoma Film Festival 
*St. Louis Film Festival  
*Dublin Film Festival 

==References==
 

==External links==
*  
*  
*   at Monterey Media

 
 
 


 