The Great Consoler
 
{{Infobox film
| name           = The Great Consoler (Великий утешитель)
| image          = 
| image_size     = 
| caption        = 
| director       = Lev Kuleshov
| producer       = Ivan Gorchilin
| writer         = O. Henry Lev Kuleshov Aleksandr Kurs
| narrator       = 
| starring       = Konstantin Khokhlov
| music          =  Konstantin Kuznetsov
| editing        = Lev Kuleshov
| distributor    = 
| released       = 1933 in film
| runtime        = 95 minutes
| country        = Soviet Union 
| language       = Russian
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

The Great Consoler ( , Transliteration|translit.&nbsp;Velikiy uteshitel) is a 1933 Soviet drama film directed by Lev Kuleshov and starring Konstantin Khokhlov.     

==Cast==
* Konstantin Khokhlov - Bill Porter Ivan Novoseltsev - Jim Valentine - aka Ralph D. Spenser
* Vasili Kovrigin - Warden
* Andrei Fajt - Det. Ben Price
* Daniil Vvedensky - Jailguard
* Weyland Rodd - Black convict
* O. Rayevskaya - Jims mother
* S. Sletov - Jailguard
* Aleksandra Khokhlova - Dulcie
* Galina Kravchenko - Annabel Adams
* Pyotr Galadzhev - E. Adams - banker / reporter
* Vera Lopatina - Sadie
* Mikhail Doronin - Innkeeper
* Andrei Gorchilin - Convict

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 