Troll 3
 
{{Infobox film
| name           = Troll 3
| image          = Troll 3 dvd.jpg
| caption        = Poster under the name The Crawlers
| director       = Joe DAmato Fabrizio Laurenti
| producer       =  
| writer         = Fabrizio Laurenti Albert Lawrence Daniele Stroppa
| starring       = Mary Sellers Jason Saucier Bubba Reeves Chelsi Stahr Vince ONeil
| music          = Carlo Maria Cordio
| cinematography = 
| editing        = 
| distributor    = Columbia/Tri-Star Home Video Epic Home Video Metro-Goldwyn-Mayer (current)
| studio =         Filmirage
| released       =  
| runtime        = 91 mins.
| country        = Italy Canada United States
| language       = English
| budget         = 
}}
Troll 3 (also known as Creepers, Contamination .7, Troll III: Contamination Point 7, or The Crawlers) is a 1993 Italian horror film directed by Joe DAmato and Fabrizio Laurenti.

Like Troll 2, despite the English title, Troll 3 has no plot connection to the original Troll (film)|Troll, features no trolls, and is also a horror film, not fantasy-comedy. The film has none of the original cast, nor storyline continuation, from either Troll film. An early script was made with the original cast in mind.  In one scene in a bar, a banjo centric song from Troll 2 can be heard in the background.

== Plot == plant illegal dumps hazardous EPA investigates, mutated due bulldozes the plants, killing them, leaving the possibility that some more plants may have survived.

== Cast ==
* Mary Sellers
* Jason Saucier
* Bubba Reeves
* Chelsi Stahr
* Vince ONeil

== Distribution == Columbia TriStar Home Entertainment. It is also available for DVD rental through Netflix under its alternate name, Contamination .7.  Scream Factory (under license from MGM Home Entertainment) has released the film on DVD for the first time along with The Dungeonmaster, Catacombs & Cellar Dweller in a 4 Horror Movie Marathon collection on October 29, 2013.

== References ==
 

== External links ==
*  

 
 

 
 
 
 
 
 