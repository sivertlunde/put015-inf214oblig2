Badlapur Boys
 
{{Infobox film
| name = Badlapur Boys
| image = Badlapur Boys.Jpg
| caption = Movie Poster
| director = Shailesh Verma
| writer = Shailesh Verma
| producer = Satish Pillangwad Nishan  Puja Gupta 
| music = Sameer Anjaan   Shamir Tandon   Sachin Gupta   Raju Sardar
| cinematography = Sanket Shah
| editing = A. Muthu
| studio  = Karrm Movies
| released =  
| runtime        = 123 minutes 
| language = Hindi   country = India
}}
 Puja Gupta Tamil hit film Vennila Kabadi Kuzhu. 

== Cast  == Nishan as Vijay
* Sushant Kandya as Rajkumar
* Saranya Mohan as Sapna
* Annu Kapoor as Swathi Puja Gupta as Manjari
* Anupam Maanav as Dr. O.P Malhotra
* Kishori Shahane
* Aman Verma
* Preet Saluja as Sajid

==Plot==
BADLAPUR BOYS is a poignant story of a child Vijay, who journeys through his life in a village, which is deprived of water for irrigation for decades. The father of the child challenges the system with self-immolation, if the villages basic need for water is not fulfilled. To set an example to the society, the father sets himself ablaze before the villagers and the media but as fate would have it, his sacrifice is forgotten and instead his family is affected. Vijay grows up with the dream that one day, he will come face to face with the bureaucracy with a simple request that his village Badlapur needs the governments attention to solve their water crisis. 

==Box Office==
The First Weekend Box office collection of the movie is approximately 43 Lakhs INR.   

== References ==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 

 