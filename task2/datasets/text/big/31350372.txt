CNote (film)
{{Infobox film
| name           = cNote
| italic title   = no
| image          = 
| caption        =  Christopher Hinton
| producer       = 
| writer         = 
| starring       = 
| music          = Michael Oesterle
| cinematography = 
| editing        = 
| studio         = 
| distributor    = National Film Board of Canada (NFB)
| released       =  
| runtime        = 7 minutes
| country        = Canada
| language       = English
| budget         = 
| gross          = 
}}  Christopher Hinton, which received the Genie Award for Best Animated Short at the 26th Genie Awards. In this visual music short, Hinton animates to an original modern classical composition by Montreal-based composer Michael Oesterle.        

Influenced by Futurism and Abstract expressionism, the film was computer animated and represented a departure for Hinton, who generally uses traditional animation techniques.    

==References==
 

==External links==
* 
*  

 
 
 
 
 
 
 
 
 
 
 
 
 