The Jucklins (film)
{{Infobox film
| name           = The Jucklins
| image          = 
| alt            = 
| caption        =
| director       = George Melford
| producer       = Jesse L. Lasky
| screenplay     = Frank Condon Opie Read
| starring       = Winter Hall Mabel Julienne Scott Monte Blue Ruth Renick Fanny Midgley Z. Wall Covington J.M. Dumont
| music          = 
| cinematography = Paul P. Perry 
| editing        = 
| studio         = Famous Players-Lasky Corporation
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = Silent (English intertitles)
| budget         = 
| gross          =
}}
 silent drama film directed by George Melford and written by Frank Condon and Opie Read. The film stars Winter Hall, Mabel Julienne Scott, Monte Blue, Ruth Renick, Fanny Midgley, Z. Wall Covington, and J.M. Dumont. The film was released on January 9, 1921, by Paramount Pictures.  
 
==Plot==
 

== Cast ==
*Winter Hall as General Lundsford
*Mabel Julienne Scott as Guinea Jucklin
*Monte Blue as Bill Hawes
*Ruth Renick as Millie Lundsford
*Fanny Midgley as Susan Jucklin 
*Z. Wall Covington as Alf Jucklin 
*J.M. Dumont as Chyd Lundsford
*Clarence Burton as Dr. Etheridge
*Guy Oliver as Sheriff Parker
*Robert Brower as Attorney Conkwright
*Jack Herbert as Scott Aimes
*Jack Hull as Bill Aimes
*Walter F. Scott as Jim Aimes
*Frank Weatherwax as Johnny Aimes William Boyd as Dan Stuart
*Charles Stanton Ogle as Lim Judklin
*Jack Byron

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 

 