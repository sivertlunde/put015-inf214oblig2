Marihuana (film)
 
{{Infobox film
| name           = Marihuana
| image          = Marihuana (1936).jpg
| image_size     =
| caption        = theatrical release poster
| director       = Dwain Esper
| story           = Hildegarde Stadie
| narrator       = 
| starring       = Harley Wood Hugh McArthur Pat Carlyle Paul Ellis Dorothy Dehn Richard Erskine Juanita Fletcher Hal Taggart Gloria Browne Barry Norton
| music          = 
| cinematography = Roland Price
| editing        = Carl Himm
| distributor    = Roadshow Attractions Inc.
| released       = May 1936
| runtime        = 57 minutes
| country        = United States
| language       = English
| budget         = $100,000
}}

Marihuana is a 1936 exploitation film directed by Dwain Esper, and written by Espers wife, Hildegarde Stadie. 

== Plot ==
Burma is a confused girl who likes to party. One day, she meets some strangers in a bar who invite her and her group to a party. She goes to the party with her friends, they all drink alcohol, only the girls at the party smoke marijuana unknowingly, and keep on laughing. The other girls wind up going skinny-dipping while Burma has sex with her boyfriend on the beach (which leads to her pregnancy).

One of the girls drowns at the skinny-dipping party and the rest of the friends have to keep the details of the party a secret. When Burma tells her boyfriend she is pregnant, she pressures him to marry her. He says everything will be fine and turns to the strangers who threw the party for a job in order to support his new upcoming family and responsibilities. The stranger gives him a job unloading smuggled drugs from a secret shipment to the docks. The police find out about this shipment, chase the smugglers, and end up shooting and killing Burmas boyfriend.

After Burma finds out about this news, she runs away from home, is forced to give the child up for adoption, and becomes a drug dealer. Burma moves on to (harder drugs) injecting heroin into herself. In the films ending, Burma hatches a plan to kidnap and ransom her sisters adopted daughter for $50,000, but she later finds out that the child is, in fact, her own.

==Production and release==
The films screenwriter, Hildegarde Stadie, appears as an extra in the beginning of the film.
 trailer showed a girl being brutally attacked, but this scene does not appear in the final film.

In 1938, Roadshow Attractions packaged Marihuana with the short film How to Undress in Front of Your Husband. 

==References==
 

==See also==
* Reefer Madness
* She Shoulda Said No!
* Assassin of Youth
* List of films in the public domain
* List of films containing frequent marijuana use

==External links==
 
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 

 