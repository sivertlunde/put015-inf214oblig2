Now, Voyager
 
{{Infobox film
| name           = Now, Voyager
| image          = NowVoyagerPoster.jpg
| image size     =
| alt            =
| caption        = Theatrical release poster
| director       = Irving Rapper
| producer       = Hal B. Wallis
| based on       =  
| screenplay     = Casey Robinson
| narrator       =
| starring       = Bette Davis Paul Henreid Claude Rains Gladys Cooper
| music          = Max Steiner
| cinematography = Sol Polito
| editing        = Warren Low
| studio         = Warner Bros.
| distributor    = Warner Bros.
| released       =  
| runtime        = 117 minutes
| country        = United States
| language       = English
}}

Now, Voyager is a 1942 American drama film starring Bette Davis, Paul Henreid, and Claude Rains, and directed by Irving Rapper. The screenplay by Casey Robinson is based on the 1941 novel of the same name by Olive Higgins Prouty. 

Prouty borrowed her title from the Walt Whitman poem "The Untold Want", which reads in its entirety, The untold want by life and land neer granted,
Now, voyager, sail thou forth, to seek and find.}}

In 2007, Now, Voyager was selected for preservation in the United States National Film Registry by the Library of Congress as being "culturally, historically, or aesthetically significant."  The film ranks #23 on AFIs 100 Years... 100 Passions, a list of the top love stories in American cinema. Film critic Steven Jay Schneider suggests the film continues to be popular due not only to its star power but also the "emotional crescendos" engendered in the storyline. Schneider 2005, p. 183. 
 

==Plot== psychiatrist Dr. Jaquith (Claude Rains), who recommends she spend time in his sanatarium.
  Lee Patrick) James Rennie). It is from them that Charlotte learns of Jerrys devotion to his young daughter, Christine ("Tina"), and how it keeps him from divorcing his wife, a manipulative, jealous woman who does not love Tina, and who keeps Jerry from engaging in his chosen career of architecture, despite the fulfillment he gets from it.
 Sugarloaf Mountain when their car crashes. They miss the ship and spend five days together before Charlotte flies to Buenos Aires to rejoin the cruise. Although they have fallen in love, they decide it would be best not to see each other again.
 
When she arrives home, Charlottes family is stunned by the dramatic changes in her appearance and demeanor. Her mother is determined to once again destroy her daughter, but Charlotte is resolved to remain independent. The memory of Jerrys love and devotion help to give her the strength she needs to remain resolute.
 John Loder), but after a chance meeting with Jerry, she breaks off the engagement, about which she quarrels with her mother. During the argument, Charlotte says she didnt ask to be born, that her mother never wanted her, that its "been a calamity on both sides." Mrs. Vale is so shocked that her once-mousey daughter has found the courage to actually talk back to her, she has a heart attack and dies. Guilty and distraught, Charlotte returns to the sanatarium.
 
When she arrives, she is immediately diverted from her own problems when she meets lonely, unhappy Tina, age 12, who greatly reminds her of herself; both were unwanted and unloved by their mothers. She is shaken out of her depression and instead becomes interested in Tinas welfare. With Dr. Jaquiths permission she takes the girl under her wing. When she improves, Charlotte takes her home to Boston.
 top 100 movie quotes in American cinema.

==Production==
Producer Hal B. Wallis made Now, Voyager his first independent production at Warner Bros. under a new arrangement with the studio. He took an active role in the production, including casting decisions. Leaming 1992, pp. 204–205.  The initial choices for Charlotte were Irene Dunne, Norma Shearer, and Ginger Rogers. Higham 1981, pp. 159–167.  When Bette Davis learned about the project, she campaigned for and won the role. More than any other of her previous films, Davis became absorbed in the role, not only reading the original novel but becoming involved in details such as choosing her wardrobe personally. Consulting with designer Orry-Kelly, she suggested a drab outfit, including an ugly foulard dress for Charlotte initially, to contrast with the stylish, "timeless" creations that mark her later appearance on the cruise ship. 
 
The choice of Daviss leading men became important as well. Davis was aghast at the initial costume and makeup tests of Austrian actor Paul Henreid; she thought the "slicked back" gigolo-like appearance   made him look "just like Rudolph Valentino|Valentino." Henreid was similarly uncomfortable with the brilliantine image and when Davis insisted on another screen test with a more natural hairstyle, he was finally accepted as the choice for her screen lover. Spada 1993, pp. 189–190.  In her 1987 memoir, This N That, Davis revealed that co-star Claude Rains (with whom she also shared the screen in Juarez (1939 film)|Juarez, Mr. Skeffington, and Deception (1946 film)|Deception) was her favorite co-star. 

Initial production of the Prouty novel had to take into account that European locales would not be possible in the midst of World War II, despite the novelists insistence on using Italy as the main setting. Proutys quirky demands for vibrant colors and flashbacks shot in black and white with subtitles were similarly disregarded.  Principal photography was shifted to Warners sound stage 18 and various locations around California, including the San Bernardino National Forest, while European scenes were replaced by stock footage of Brazil.  One of the primary reasons for Davis being interested in the original project was that photography would also take place in her hometown of Boston. 
 
The film highlighted Daviss ability to shape her future artistic ventures, as not only did she have a significant role in influencing the decisions over her co-stars, the choice of director was predicated on a need to have a compliant individual at the helm.  Davis previously had worked with Irving Rapper on films where he served as a dialogue director, but his gratitude for her support turned into a grudging realization that Davis could control the film.  Although his approach was conciliatory, the to-and-fro with Davis slowed production and "he would go home evenings angry and exhausted".  The dailies, however, showed a "surprisingly effective" Davis at the top of her form. 

For years, Davis and co-star Paul Henreid claimed the moment in which Jerry puts two cigarettes in his mouth, lights both, then passes one to Charlotte, was developed by them during rehearsals, inspired by a habit Henreid shared with his wife, but drafts of Casey Robinsons script on file at the University of Southern California indicate it was included by the screenwriter in his original script.  The scene remained an indelible trademark that Davis later would exploit as "hers". 

==Main cast==
 
* Bette Davis as Charlotte Vale
* Claude Rains as Dr. Jaquith
* Paul Henreid as Jeremiah Duvaux Durrance
* Gladys Cooper as Mrs. Windle Vale
* Ilka Chase as Lisa Vale
* Bonita Granville as June Vale John Loder as Elliot Livingston Lee Patrick as Deb McIntyre James Rennie as Frank McIntyre
* Mary Wickes as Nurse Dora Pickford
* Janis Wilson as Christine "Tina" Durrance (uncredited) David Clyde as William
* Franklin Pangborn as Mr. Thompson

==Critical reception==
Theodore Strauss, a critic for The New York Times observed:
 

Leslie Halliwell:
 

==Awards and nominations==
* Academy Award for Best Actress (Bette Davis, nominee)
* Academy Award for Best Supporting Actress (Gladys Cooper, nominee) Academy Award for Best Original Score (Max Steiner, winner)

==References==

===Notes===
 

===Bibliography===
 
* Davis, Bette with Michael Herskowitz. This N That. New York: G.P Putnams Sons, 1987. ISBN 0-399-13246-5
* Leaming, Barbara. Bette Davis: A Biography. New York: Simon & Schuster, 1992. ISBN 0-671-70955-0
* Higham, Charles. Bette: The Life of Bette Davis. New York: Dell Publishing, 1981. ISBN 0-440-10662-1
* Moseley, Roy. Bette Davis: An Intimate Memoir. New York: Donald I. Fine, 1990. ISBN 1-55611-218-1
* Quirk, Lawrence J. Fasten Your Seat Belts: The Passionate Life of Bette Davis. New York: William Morrow and Company, 1990. ISBN 0-688-08427-3
* Schneider, Steven Jay. 1001 Movies You Must See Before You Die. Hauppauge, New York: Barrons Educational Series, 2005. ISBN 0-7641-5907-0
* Spada, James. More Than a Woman: An Intimate Biography of Bette Davis. New York: Bantam Books, 1993. ISBN 0-553-09512-9
 

==External links==
 
*  
*  
*  
*  
*  
*  
Streaming audio
*   on Lux Radio Theater: May 10, 1943
*   on Lux Radio Theater: February 11, 1946

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 