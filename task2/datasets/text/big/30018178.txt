Krishan Avtaar
 
{{Infobox film
 | name = Krishan Avtaar 
 | image = KrishanAvtaarNew.jpg
 | caption = DVD Cover
 | director = Ashok Gaekwad
 | producer = Rajiv Kumar
 | writer = 
 | dialogue = 
 | starring = Mithun Chakraborty Somy Ali Hashmat Khan  Paresh Rawal   Sujata Mehta
 | music = Nadeem-Shravan
 | lyrics = 
 | associate director = 
 | art director = 
 | choreographer = 
 | released = June 25, 1993
 | runtime = 140 min.
 | language = Hindi Rs 6 Crores
 | preceded_by = 
 | followed_by = 
 }}
 1993 Hindi Indian feature directed by Ashok Gaekwad, starring Mithun Chakraborty, Somy Ali, Hashmat Khan, Paresh Rawal and Sujata Mehta

==Plot==
Krishan Avtaar is an action movie starring Mithun Chakraborty and Somy Ali in lead roles. Mithun plays the role of a police officer, who is suffering from a brain tumour. The film shows his love for his child and his passion for his job.

Police Inspector Krishan Kumar lives a middle class life with his lovely wife, Suman. Suman gets pregnant and gives birth to a baby girl but tragically passes away, leaving Krishan widowed, heart-broken and devastated. Krishan also encounters a tumor, which is inoperable, and may eventually lead to his death. Then a large number of the citys children go missing, and Krishan, together with Inspector Avtaar are assigned to investigate this matter. They are also asked to investigate the murder of fellow police inspector Vishnu Sawant. Krishans investigation leads him to corruption in his very own department; the involvement of a prominent minister in the government; and the daughter of Inspector Sawant himself. Faced with failing health and vision, Krishan must make his move quickly before he becomes totally disabled from doing any more police work.

==Cast==

*Mithun Chakraborty		
*Somy Ali
*Sujata Mehta
*Hashmat Khan		
*Paresh Rawal
*Shakti Kapoor	
*Payal		
*Laxmikant Berde		
*Raza Murad		
*Shiva Rindani		
*Tinnu Anand		
*Ashok Saxena		
*Avtar Gill		
*Jay Kalgutkar		
*Guddi Maruti		
*Baby Udita Gaur		
*Goga Kapoor		
*Ankush Mohit
*Deep Dhillon		
*Anjana Mumtaz		
*Bharat Kapoor		
*Mac Mohan		
*Sunil Shende		
*Sunil Dhawan		
*Vibhakar		
*Renu Joshi

==Songs==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title 
|-
| 1
| Nigahon Ke Sawal Ka Nigahon
|- 
| 2
|  Phir Se Armaan Jaag Uthe Hain
|- 
| 3
| I Love You Daddy 
|- 
| 4
| Mera Mehboob Mujhse Aise Mila 
|- 
| 5
| Humse Pyar Karo 
|- 
| 6
| Gudiya Pyari Pyari Gudiya 	
|}

==References==
* http://www.imdb.com/title/tt0490440/
* http://ibosnetwork.com/asp/filmbodetails.asp?id=Krishan+Avtaar

==External links==
*  

 
 
 
 
 
 


 