The Cabin in the Cotton
{{Infobox film
| name           = The Cabin in the Cotton
| image          = The Cabin in the Cotton Film poster.jpg
| image_size     = 
| alt            = 
| caption        = Theatrical release poster
| director       = Michael Curtiz 
| producer       = Hal B. Wallis Darryl F. Zanuck Jack L. Warner
| writer         = 
| narrator       =  Dorothy Jordan Bette Davis
| music          = Leo F. Forbstein
| cinematography = Barney McGill
| editing        = George Amy First National Pictures
| distributor    = Warner Bros.
| released       =  
| runtime        = 78 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 Paul Green is based on the novel of the same title by Harry Harrison Kroll.
 Southern drawl - "Ahd love t kiss ya, but ah jes washed ma hayuh," a line lifted directly from the book. In later years it was immortalized by Davis impersonators and quoted in the 1995 film Get Shorty.

==Plot== vampish daughter Madge intercedes on his behalf. Blake uncovers irregularities in Norwoods accounts and soon finds himself embroiled in a battle between management and workers and torn between the seductive Madge and his longtime sweetheart Betty Wright.

==Production==
When producer  .    In later years, Davis observed, "Mr. Curtiz, I must say, monster as he was, was a great European moviemaker. He was not a performers director . . . You had to be very strong with him. And he wasnt fun . . . He was a real BASTARD! Cruelest man I have ever known. But he knew how to shoot a film well."  Chandler, Charlotte, The Girl Who Walked Home Alone: Bette Davis, A Personal Biography. New York: Simon & Schuster 2006. ISBN 0-7432-6208-5, pp. 83-84  She went on to make six additional films with Curtiz, including The Private Lives of Elizabeth and Essex in 1939.

Davis liked Barthelmess personally but was stymied by his acting style. "He did absolutely nothing in the long shots, followed basic stage directions for medium shots, and reserved his talent for the close-ups. In that way it was necessary to use his close-ups almost entirely."   Barthelmess said of Davis, "There was a lot of passion in her, and it was impossible not to sense . . . one got the sense of a lot of feeling dammed up in her, a lot of electricity that had not yet found its outlet. In a way it was rather disconcerting - yes, I admit it, frightening." 

Davis later confessed she was a virgin when she made the film. "Yes, thats absolutely true. No question about it," she added for emphasis. "But my part called for me to exude raging sexuality. Well, if they had known I was still a virgin, they wouldnt have believed I could carry it off. They wouldnt have trusted me if theyd known, but no one asked. It was assumed that a young actress had lived a bit of a loose life." 

==Cast (in credits order)==
*Richard Barthelmess as Marvin Blake Dorothy Jordan as Betty Wright
*Bette Davis as Madge Norwood
*Hardie Albright as Roland Neal David Landau as Tom Blake
*Berton Churchill as Lane Norwood
*Dorothy Peterson as Lilly Blake Russell Simpson as Uncle Joe
*Tully Marshall as Slick
*Henry B. Walthall as Eph Clinton
*Edmund Breese as Holmes Scott
*John Marston as Russell Carter
*Erville Alderson as Sock Fisher
*William Le Maire as Jake Fisher
*Clarence Muse as A Blind Negro

==Critical reception==
In his review in the New York Times, Mordaunt Hall described it as "a film which seldom awakens any keen interest . . . Richard Barthelmess gives a careful but hardly an inspired performance. His general demeanor lacks the desired spontaneity and often he speaks his lines in a monotone . . . Michael Curtiz is responsible for the direction, which is uneven, and sections of the narrative are rather muddled." 
 New York John Cromwell, Of Human Bondage, which went on to cement Davis reputation as one of the best actresses of the era. 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 