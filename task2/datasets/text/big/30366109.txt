Jeevana Tarangalu
{{Infobox film
| name           = Jeevana Tarangalu
| image          = Jeevana Tarangalu.jpg
| image_size     =
| caption        =
| director       = Tatineni Rama Rao
| producer       = D. Rama Naidu
| writer         = Yaddanapudi Sulochana Rani   (story)  Acharya Atreya   (screenplay and dialogues) 
| narrator       = Chandramohan Lakshmi Lakshmi Gummadi Sriranjani
| music          = J. V. Raghavulu
| cinematography = S. Venkataratnam
| editing        =
| studio         = Suresh Productions
| distributor    =
| released       = 1973
| runtime        =
| country        = India
| language       = Telugu
| budget         =
}}
 Kannada as Mangalya in 1990 with Malasri and Sridhar in lead roles.

==Cast==
* Shobhan Babu  ... Vijay
* Krishnamraju  ... Chandram
* Vanisree  ... Roja Chandramohan  ... Ananth
* Anjali Devi Lakshmi  ... Lavanya
* Gummadi Venkateswara Rao  ... Venugopal Rao Sriranjani

==Soundtrack==
* "Ee Andaniki Bandham Vesanokanaadu" (Singers: Ghantasala and P. Susheela)
* "Ee Jeevana Tarangalalo Aa Devuni Chadarangamlo" (Singer: Ghantasala Venkateswara Rao)
* "Nandamaya Guruda Nandamaya" (Lyrics:  )
* "Puttina Roju Panduge Andariki" (Lyrics: C. Narayana Reddy; Singer: P. Susheela)
* "Thenchukuntala Unchukuntava" (Singers: P. Susheela and L. R. Eswari)
* "Uduta Uduta Huth Ekkadikelatavuch" (Singers: Ghantasala and Ramola)

==Boxoffice==
* The film ran more than 100 days in 12 centers in Andhra Pradesh. 

==Awards==
* Filmfare Best Film Award (Telugu) - D.Rama Naidu
* Filmfare Best Director Award (Telugu) - Tatineni Rama Rao
* Filmfare Best Actress Award (Telugu) - Vanisri

==References==
 

==External links==
*  
 

 
 
 
 
 


 