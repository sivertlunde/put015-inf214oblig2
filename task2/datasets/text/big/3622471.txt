Cheyenne Autumn
{{Infobox film
| name           = Cheyenne Autumn 
| image          = Cheyenne autumn poster.JPG 
| caption        = Theatrical release poster Bernard Smith 
| director       = John Ford
| based on       =   James R. Webb 
| starring       = Richard Widmark Carroll Baker James Stewart Dolores del Río Edward G. Robinson Karl Malden 
| music          = Alex North 
| cinematography = William H. Clothier 
| editing        = Otho Lovering 
| distributor    = Warner Bros. 
| released       = October 3, 1964 
| runtime        = 154 minutes  English
| gross          = $3,500,000 (US/ Canada rentals) 
}}
 Western movie Native Americans who had been abused by the United States government|U.S. government and misinterpreted by many of the directors own films. With a budget of more than $4,000,000, the film was relatively unsuccessful at the box office and failed to earn a profit for its distributor, Warner Bros.

==Plot== Chiefs Little Captain Thomas Archer (Richard Widmark) is forced to lead his troops in an attempt to stop the tribe. As the press misrepresents the natives motives and goals for their trek as malicious, Secretary of the Interior Carl Schurz (Edward G. Robinson) tries to prevent violence from erupting between the army and the natives. Also featured are James Stewart as Wyatt Earp, Dolores del Río as "Spanish Woman" and Carroll Baker as a pacifist Quaker school teacher and Archers love interest.

==Cast==
 
* Richard Widmark as Captain Thomas Archer
* Carroll Baker as Deborah Wright
* James Stewart as Wyatt Earp
* Edward G. Robinson as Carl Schurz
* Karl Malden as Captain Oscar Wessels
* Sal Mineo as Red Shirt
* Dolores del Río as Spanish Woman
* Ricardo Montalban as Little Wolf
* Gilbert Roland as Dull Knife Arthur Kennedy as Doc Holliday
* Patrick Wayne as Second Lieutenant Scott Elizabeth Allen as Guinevere Plantagenet
* John Carradine as Major Jeff Blair
* Victor Jory as Tall Tree
* Mike Mazurki as 1st Sergeant Stanislaus Wichowsky George OBrien as Major Braden
* Sean McClory as Dr. OCarberry
* Judson Pratt as Mayor Dog Kelly
* Ken Curtis as Joe
* Shug Fisher as Skinny
 

==Production==

===Preproduction===
John Ford long wanted to make a movie about the Cheyenne exodus. As early as 1957 he wrote a treatment with his son Patrick Ford, envisioning a small-scale drama with non-professional Indian actors. Early drafts of the script drew on Howard Fasts novel The Last Frontier. However, the film ultimately took its plot and title from Mari Sandozs Cheyenne Autumn, which Ford preferred due to its focus on the Cheyenne. Elements of Fasts novel remain in the finished film however, namely the character of Captain Archer (Murray in the book), the depiction of Carl Schurz and the Dodge City scenes. 

Reluctantly abandoning the docudrama idea, Ford wanted Anthony Quinn and Richard Boone to play Dull Knife and Little Wolf, as well-known actors with some Indian ancestry. He also suggested Woody Strode for a role. The studio insisted on Fords casting Ricardo Montalban and Gilbert Roland. 

===Filming=== Academy Award. Best Supporting Actor.

===Editing=== Arthur Kennedy as Doc Holliday.  Some critics have argued that this comic episode, mostly unrelated to the rest of an otherwise serious movie, breaks the flow of the story.        It was later restored for the VHS and subsequent DVD releases.

===Locations=== The Searchers. Navajo tribe in this production.

===Casting=== Native American Navajo to portray the Cheyenne. This meant the dialogue that is supposed to be the "Cheyenne language" is actually Navajo. This made little differences to White audiences but for Navajo communities the film became very popular. This was because the Navajo actors were openly using ribald and crude language that had nothing to do with the film. For example during the scene where the treaty is signed, the chiefs solemn speech just pokes fun at the size of the colonels penis. Academics now consider this an important moment in the development of Native Americans identity because they are able to mock Hollywoods (i.e. mainstream White society) historical interpretation of the American West. 

==Reception==
The reviews were mixed. Bosley Crowther, critic for The New York Times, praised it highly, calling it "a beautiful and powerful motion picture that stunningly combines a profound and passionate story of mistreatment of American Indians with some of the most magnificent and energetic cavalry-and-Indian lore ever put upon the screen."  He was disappointed, however, that after the humorous (if "superfluous") Dodge City sequence, "the picture does not rise again to its early integrity and authenticity", and the climax is "neither effective and convincing drama nor is it faithful to the novel".  The New Yorkers Richard Brody cited the "rueful, elegiac grandeur of John Ford’s final Western". 

Variety (magazine)|Variety disagreed, however, calling it "a rambling, episodic account" in which "the original premise of the Mari Sandoz novel is lost sight of in a wholesale insertion of extraneous incidents which bear little or no relation to the subject." 

==Award nominations==
* Nominated:  
* Nominated:  

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 