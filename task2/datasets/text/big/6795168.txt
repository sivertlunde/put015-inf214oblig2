Amaravathi (1993 film)
{{Infobox film
| name = Amaravathi
| image =
| image_size =
| caption = Selva
| producer = Chozha Ponnurangam Selva J. Ramesh (dialogues)
| starring = Ajith Kumar Sanghavi Nassar
| music = Bala Bharathi
| cinematography = B. Balamurugan
| editing = Rajoo
| distributor = Chozha Creations
| released = May 24, 1993
| runtime =
| studio = Chozha Creations
| country = India Tamil
| budget =  32 lakh
}}
 Tamil film directed by Selva (director)|Selva. The film featured debutants Ajith Kumar and Sanghavi in the lead role,  and released in May 1993 to a positive response at the box office. 

==Cast==
* Ajith 
* Sanghavi
* Nassar
* Thalaivasal Vijay
* Charle

==Production== Vikram had to dub scenes for Ajith. 

==Music==
The music composed by Bala Bharathi and lyrics written by Vairamuthu. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers  || Length (m:ss)
|-
| 1 || "Adi Soku Sundari" || S. P. Balasubrahmanyam, Malgudi Subha || 04:58
|-
| 2 || "Ha Ha Kanaveh Thana" || S. P. Balasubrahmanyam || 04:34
|-
| 3 || "Poo Malaranthethu" || Minmini || 04:13
|-
| 4 || "Putham Pudhu Malare" || S. P. Balasubrahmanyam || 05:02
|-
| 5 || "Tajumahal Thevailla" || S. P. Balasubrahmanyam, S. Janaki || 04:09
|-
| 6 || "Udal Enna Uyir Enna" || Ashok || 04:40
|}

==Release==
The film was profitable and the success was partly credited to the chart-topping soundtracks composed by Bala Bharathi.  The film also gained media attention for its lead actor, Ajith Kumar, who was approached with several modelling assignments. 

==References==
 

==External links==
*  

 

 
 
 
 
 

 