Seniman Bujang Lapok
 
 
{{Infobox film name           = Seniman Bujang Lapok image          = SenimanBujangLapok.jpg caption        = One-sheet promotional poster. director       = P. Ramlee S Sudarmaji producer       =  writer         = P.Ramlee (Original Story)  H.M.Rohaizad (Screenplay) narrator       = starring  Zaiton
|music          = P. Ramlee cinematography = A. Bakar Ali editing        = Thian Yak distributor    = Shaw Organisation released       =   runtime        = country        = Malaysia Singapore language       = Malay budget         =
}}

Seniman Bujang Lapok (The Three Worn Out Actor Bachelors) is a 1961 Malay comedy film directed by P. Ramlee. It is the fourth instalment in the "Bujang Lapok" series of films, but the last to feature P. Ramlee, S. Shamsuddin and Aziz Sattar as the main trio of actors. However, it is not a direct sequel to the previous "Bujang Lapok" films, as there are no references to the events of the previous films.

The film is a self-referential spoof of the Malay film industry of the late 1950s/early 1960s. The plot revolves around the main trio attempting to become actors and break into the film business, and hence features a fictionalised look at the behind-the-scenes process of Malay film-making during that time. The line between reality and fiction is blurred as real film industry places are used, real film sets of previous Malay films are used for the film-within-the-film, and all the featured actors use their real-life names or derivatives thereof.

==Plot==
The "bujang lapok" trio of Ramli, Sudin and Ajis go to Jalan Ampas Studio in Singapore in response to an open call audition. During their audition, they drive director Ahmad Nisfu crazy by forgetting their scripts and improvising with their own lines. However, the studio boss likes their natural comedic talent and tells the director that the trio are to be signed on for an oncoming film. The three of them are themselves taken by surprise but are nonetheless pleased at the outcome.

Most of the movie centre around their lives in a house shared with other tenants (they all each rent a room). During their first evening memorising scripts for their new movie, they encounter one disruption after another. First, it was the husband and wife dancing to loud music, then its the neighbour with the motorcycle problems, then its the drunk trumpeter who is practising his instrument a bit too loudly. After dealing with each problematic neighbour in their own unique way, they then settle down to memorise their lines.

The following day, filming begins on their new movie. Despite having learned their lines, they fumble through them and end up infiurating the director so much that he collapses. He is brought to the hospital where he is diagnosed with stress and released. The three bachelors are on hand to visit him with flowers in hand. Initially pleased with the visit, he collapses again when he sees them, the ones who made him collapse in the first place. 

Back at home, they are greeted by Salmah, their next door neighbour, and the object of Ramlis affections. They banter to and fro and then Salmah runs off from the room in tears whilst trying to tell Ramli something. When they meet later that night, Salmah tells Ramli that someone has come to ask for her hand in marriage. When she reveals the person, it is Sharif Dol, a bully from a neighbouring village, who has been harassing Salmah for some time. He then appears at the scene and starts taunting Ramli and Salmah. Ramli tells Salmah to go home whilst he deals with Shariff Dol and challenges him to a fight. Back at the room, both Sudin and Ajis wait for him back at their room with food on the table. Sudin is already hungry but it is their agreement that they always eat together, the three of them. Unwilling to wait for him any more, Sudin wants to go and look for Ramli and they leave their dinner to find Ramli all bruised and passed out at his usual meeting place with Salmah. After settling him down, Sudin brings out some of the dinner that he has brought with him and the three of them enjoy them together.

The next day, Salmah and her mother receives a visit from Shariff Dol and his mother. They begin discussing the terms of the wedding and Shariff Dol gives Salmahs mother some money to tide her over until the big day. Salmah keeps silent but shows her disapproval to Shariff Dol from across the room. Later on that day, Sudin and Ajis go on their separate adventures in the evening whilst Ramli and Salmah lament their love for each other. When both of them return, they tell Ramli about their evening whilst he looks on in silence. When asked, he tells them that he wants to get married but doesnt have the money to do so. Sudin then tells him he has the answer for it and produces a magic stone he had purchased earlier in the evening by a roadside seller who assures him that with a little ritual, all his deepest wishes will be fulfilled. Ramli is consoled and thanks Sudin for being a good friend.

Later on during the night, they each encounter some more of their neighbours antics, one involving a policeman and another involving a dispute with husband and wife and a soiled baby diaper. They finally settle in for the night and when they wake up the next morning and make their way for their morning rituals, Ramli peeps into Salmahs room only to be scared off by her emergence from the room. The next few scenes show their lives in the village as they go about getting themselves ready to go to the film studio in their quest to fulfill Ramlis wish to be married. Sudin fails in his attempts to acquire the financial resources Ramli needs for his intended wedding and in fury, Sudin throws the magic stone into the stream. The next scene is of Salmah and her adamant refusal to not get married to Shariff Dol. She then reveals to her mother that he has been the one harassing her for so long and she has no wish to get married to him. Salmahs mother is similarly furious and returns him the money he had given to her. Shariff Dol takes the news in stride but promises revenge for the humiliation he has endured. 

Later on in the evening, Salmah informs Ramli that she has turned Shariff Dols marriage proposal down once and for all. Ramli is pleased but Salmah wants to proceed with their marriage as they had intended to but Ramli admits to her that he has no finances to support their marriage and wedding. Salmah suggests using her savings instead and they gleefully leave for home together. When they arrive home, they find their house in flames and their neighbours running around in horror. Whilst Ramli is resigned to the fate that they will be homeless, Salmah is convinced that it is Shariff Dols doing. She tells the same to Sudin and Ajis who round up the rest of their neighbours to find Shariff Dol and bring him to the police station. In the meantime, Ramli found a wallet at the scene of the crime proving that it was indeed Shariff Dol who perpetrated the crime. Ramli pursues to find him and they fight again when Ramli tells him he found his wallet at the scene of the fire. Shariff Dol eventually defeats when it is apparent he would not win the fight. About the same time, Salmah arrives with the rest of the angry tenants demanding justice for their burnt house. Ramli emerges from the house with Shariff Dol tied up and persuades them to surrender him to the police instead. The evening ends well for all three bachelors when all their romantic halves join them and they walk into the moonlight together. 

==Songs==
* Menchecheh Bujang Lapok
* Gelora
* Senandung Kasih
* Embun Menitik (by late Ahmad Patek)

The songs were sung by Saloma,P.Ramlee,S.Shamsuddin,Aziz Sattar,and Pancha Sitara band.

==Cast==
* P. Ramlee as Ramli
* S. Shamsuddin as Sudin
* Aziz Sattar as Ajis
* Saloma as Miss Salmah
* Sharif Dol as Sharif Dol
* Ahmad Nisfu as Director Ahmad Nisfu

==External links==
*  
*  

 
 
 
 
 
 