Black Mask 2: City of Masks
 
 
{{Infobox film
| name           = Black Mask 2: City of Masks
| image          = Black-mask-2-poster.jpg
| caption        =
| director       = Tsui Hark
| producer       = Tsui Hark
| writer         = Jeff Black Charles Cain
| starring       = Andy On Tobin Bell Traci Lords
| music          =
| cinematography =
| editing        = One Hundred Years of Film Film Workshop
| distributor    = China Star Entertainment Group (Hong Kong) Sony Pictures (U.S.)
| released       =  
| runtime        =
| country        = Hong Kong
| language       = English Cantonese
| budget         =
}}
Black Mask 2: City of Masks (黑俠II) is a 2002 Hong Kong action film directed by Tsui Hark. Andy On took over the role of Black Mask when original actor Jet Li opted not to return. The film also starred Tobin Bell, Jon Polito, Tyler Mane, Rob Van Dam, Traci Lords, and Scott Adkins 

==Overview== 1996 Black Black Mask. However, apart from the main character, the film is largely unrelated in story to the original film and follows Black Mask trying to find a cure for his supersoldier powers, all while being tracked by his creator, a giant brain (a detail at odds with the original film). The film also takes the tone of a fantastical superhero film, being set in a futuristic city, featuring a plot involving stopping a mutative DNA bomb and pitting the main character against a group of human-animal hybrid professional wrestlers.

Unlike the original film, which was in Cantonese and later dubbed into English language|English, Black Mask 2 was predominantly produced in English. The film was later released dubbed into Cantonese in Hong Kong in 2003, with Andy Lau being featured as a narrator for the Hong Kong version.

==Plot==
Because of genetic modifications, Black Mask has superhuman fighting and healing powers. He is searching for top geneticists around the world who might be able to reverse his modifications,  which have blocked his emotions. 

Elsewhere, someone is augmenting the DNA of top pro-wrestlers with animal genomes, making them stronger and deadlier. The process is causing the wrestlers to become more like animals. Black Mask tries to help the wrestlers, but their boss has plans of his own.

==Cast==
*Andy On - Black Mask
*Teresa Maria Herrera - Dr. Marco
*Tyler Mane - Thorn
*Oris Erhuero - Wolf
*Scott Adkins - Dr. Lang
*Sean Marquette - Raymond
*Traci Lords - Chameleon
*Jon Polito - King
*Andrew Bryniarski - Iguana
*Rob Van Dam - Claw
*Robert Allen Mukes - Snake
*Silvio Simac - Troy 

Oris Erhuero, Traci Lords, and Jon Polito have all been involved in the Highlander Franchise.

==Cantonese voice cast==
*Andy Lau - Narrator
*Louis Koo - Thorn Lau Ching-wan - Wolf
*Jordan Chan - Lang
*Cecilia Cheung - Raymond
*Cherrie Ying - Chameleon
*Chapman To - King
*Michael Tse - Iguana Patrick Tam - Claw Raymond Wong - Snake

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 