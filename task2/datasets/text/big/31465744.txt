Tengoku Kara no Yell
{{Infobox film
| name           = Tengoku Kara no Yell
| image          = Tengoku Kara no Yell movie poster.jpg
| caption        = Film poster advertising Tengoku Kara no Yell in Japan
| director       = Makoto Kumazawa
| producer       = Yoshiko Makabe, Keiichi Shigematsu, Hirohisa Mukuju
| writer         = Masaya Ozaki, Kimiko Ueno
| screenplay     = 
| story          = 
| based on       =   Hiroshi Abe, Nanami Sakuraba
| music          = Nobuhiro Mitomo, Yūsuke Hayashi
| cinematography = Masato Kaneko
| editing        =
| studio         = Django Film, Asmik Ace Entertainment
| distributor    = Asmik Ace Entertainment
| released       =  
| runtime        = 114 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
}}
 Hiroshi Abe plays the role of Hikaru Oshiro, whose character is based closely on Nakasone.    Actress and idol Nanami Sakuraba also stars in the film, playing the role of a student who aspires to be a singer and guitarist. 

Tengoku Kara no Yell was first screened at the 3rd Okinawa International Movie Festival on 27 March 2011.  It was subsequently released in Japanese cinemas on 1 October 2011.   

==Plot==
Hikaru Oshiro, noticing that there was a lack of music studios catering to high school students aspiring to become musicians, decided to build one studio under his bento shop. He allows high school students to use the studio for free, but he required them to be respectful and empathetic to others, and also do well academically in their schools.

==Cast== Hiroshi Abe plays Hikaru Oshiro, a man who builds a music studio in the basement of his shop when he learned that a group of high school students had no place to properly practice as a band.    He allows youth use the place for free, on condition that they are respectful, empathetic to others, and do well in school.  Mimura will star as Hikaru Oshiros wife.
* Nanami Sakuraba as Aya Higa, a student who is an aspiring singer and guitarist. 
* Masato Yano as Yuuya Makishi
* Win Morisaki as Kai Inoha 
* Shuhei Nomura as Kiyoshi Nakamura 
* Taeko Yoshida as Chiyo Oshiro 
* Reira Zyasiki
* Keita Tanabe as Seijun Akamine Higarino as Miki
* Ruri Yoshida
* Hitomi Kiyan as Aiko Makishi
* Tazuko Tamaki 
* Ma-chan
* Eri Maehara as a reporter from the "Town" magazine 
* Tamaki Mitshiru
* Masayuki Tahara

==Production== Churaumi Aquarium, and it caters to high school students who aspire to become musicians, providing them with a practice studio for free and monetary loans to help them start up.  Nakasone was diagnosed with kidney cancer in August 2005, and he died in November 2009 at 42 years old.    His life story was featured in a 2009 NHK documentary,  and was also complied into a book entitled  . 
 Hiroshi Abe. I Wish.    Hiroshi Abe said that he thought this was a "dream story" and that he was very impressed with the passion of the high school students displayed.
 television series idol Nanami Sakuraba will also be starring in this film, playing the role of Aya Higa, a high school student who aspires to become a musician. 

The filming of Tengoku Kara no Yell began soon after the cast announcement on the 20th of October 2011.  The main scenes of this film were entirely shot at the location of "Ajisai Ongaku Mura", the place where this film was based on.    Before the filming, interviews with people familiar with Nakasone was conducted in order for the cast to better understand him and to portray him more realistically. 

The film poster of Tengoku Kara no Yell was unveiled on 5 May 2011.   
 Hiroshi Abe said that he was "under heavy pressure during the filming of this film, but he had received help from various people.    Separately, actress Nanami Sakuraba revealed that she played a guitar for the first time in the filming of this film, described the experience as "fun, but my hands are shaking a lot". 

===Theme song===
The films theme song   is performed by the Okinawan rock band Stereopony.  This was announced by the films production committee on 25 March 2011.    Stereopony named this song "Arigato", which means "thank you" in Japanese language|Japanese, in expression of their thanks to the late Hikaru Nakasone.    Stereopony members were influenced by Mr Hikaru Nakasones teachings while they were at the music village he created.   

Arigato was released as the title track of a single album on 28 September 2011.    It was released in a Limited Edition A version and an ordinary B version.   The A version come a DVD containing special footage of this film.   Actor Hiroshi Abe appears on the front cover of the B version of this single. 

==Release==
Tengoku Kara no Yell was first released at the 3rd Okinawa International Movie Festival on 27 March 2011.    It was subsequently released in Japanese cinemas on 1 October 2011. 

==References==
 

==External links==
*     
*  

 
 
 
 