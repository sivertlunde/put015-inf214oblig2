Fathers' Day (film)
{{Infobox film
| name           = Fathers Day
| image          = Fathers day poster.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Ivan Reitman
| producer       = Ivan Reitman Joel Silver
| writer         = Francis Veber (film Les Compères) Lowell Ganz & Babaloo Mandel (screenplay)
| starring       = Robin Williams Billy Crystal Julia Louis-Dreyfus Nastassja Kinski Bruce Greenwood Charles Rocket Patti DArbanville
| music          = James Newton Howard
| cinematography = Stephen H. Burum
| editing        = Wendy Greene Bricmont Sheldon Kahn Northern Lights Entertainment
| distributor    = Warner Bros.
| released       =  
| country        = United States
| language       = English
| runtime        = 98 minutes
| budget         = $85 million
| gross          = $35,681,080 
}}
Fathers Day is a 1997 comedy film directed by Ivan Reitman and starring Robin Williams, Billy Crystal, Julia Louis-Dreyfus, and Nastassja Kinski. It is a remake of the 1983 French film Les Compères.

In the film, Collette Andrews (Kinski) enlists two former lovers, cynical lawyer Jack Lawrence (Crystal) and lonely, ex-hippie, suicidal writer Dale Putley (Williams) to help her search for her runaway teenage son Scott by telling each man that he is the father. When Jack and Dale run into each other and find out whats happening, they work together to find Scott and determine the identity of the actual father.
 IMDb credits Gibson as "Scott the Body Piercer". Catherine Reitman and Jason Reitman have small roles.

==Plot==
Scott (Charlie Hofheimer), a 17 year old kid, runs away from home with his girlfriend, Nikki (Haylie Johnson). His mother Collette (Nastassja Kinski), visits her ex-boyfriend, lawyer Jack Lawrence (Billy Crystal) and tells him that Scott is really his son and wants him to find him, to which he refuses. Writer Dale Putley (Robin Williams) is planning suicide when he gets a phone call from Collette, whom he is also another ex-boyfriend, and she tells him the same story and wants him found, to which he accepts. Realizing that his appointment with a client will make him stay over night, Jack changes his mind and decides to find Scott, starting with Russ (Charles Rocket), the father of Scotts girlfriend, where he and Dale meet. Thinking their sons are together, they go to the Marina to meet Scotts girlfriends mother, Shirley (Patti DArbanville), and learn that they went following rock band Sugar Ray. But when she asks for a picture of their sons, and it is learned that Jack and Dale have been told the same story that theyre Scott fathers and call to confront Collette. She confesses that she doesnt know, but begs them to find Scott then theyll settle the situation. The two agree and they head for Sacramento where they find Scott, drunk and lovestruck. Soon after, they bring him back to their hotel room, and the next day, Scott wakes up to the news of who they are and whats going on, and he doesnt take it lightly. While Dale watches him alone, Scott escapes by pouring coffee over Dales testicles. Dale gets in touch with Jack and they head to Reno, where Sugar Ray is now at. In Reno, Scott meets up with Nikki and the gang theyre hanging out with at a hotel, until he meets two drug dealers that he once took a job for involving $5,000 (to which he spent that money buying Nikki a necklace). He manages to escape but soon gets hit by a car, driven by Jack and Dale. Now with a broken arm, Scott demands Dale and Jack to leave him alone. But later that night, Scott opens up with why he ran away, and that its because of Nikki; shes his first love, but his parents disapprove of her, and they argue. They start getting along, then Scott tells them about the drug dealers, so they decide to help him. They go back to the hotel with Scott in the car, but when Jack and Dale go inside, the two drug dealers spot Scott and attempt to kidnap him and later kill him until Scott takes off with Jacks rental car. Jack returns outside, sees the car gone, and assuming Scott had been lying to them the whole time, calls it quits and decides to go home. Just then, Jacks wife Carrie (Julia Louis-Dreyfus) is at the hotel, following Jack (and Dale) because shes been confused and concerned given Jacks odd behavior. He tells her the truth about Scott, and that he could be the father, and the news sends both of them out of the hotel. Later, Jack and Carrie have an argument over the fact that Jack, having negative feelings of Scott and his actions, makes her scared over how hell react with his own child. Jack sees her point, and arrives at a Sugar Ray concert where Dale is also there finding Scott. They find him again, and watch as he confronts his girlfriend, to which she breaks up with him. Now heartbroken, Scott is then grabbed by the drug dealers, to which Dale and Jack fight them and soon a huge fight erupts within the crowd. Freed from jail, Jack, Dale, and Scott head home where Collette and his father Bob (Bruce Greenwood) embrace with their son. Collette tells the truth to Scott that neither Jack nor Dale are the father, but Scott understands what happened because his parents wanted him home so bad. Scott then lies to both Jack and Dale, separately and privately, that theyre the father, right before they leave. Jack figured out that Scott lied, but isnt mad but is rather happy as it has given him a new outlook over having children. Dale, riding in Jacks car, spots a woman having car trouble on her way to the airport. Upon finding out that Virginia (Mary McCormack) is single, takes a shot and decides to take her to her destination by car, much to Jacks annoyance.

==Cast==
* Robin Williams as Dale Putley
* Billy Crystal as Jack Lawrence 
* Julia Louis-Dreyfus as Carrie Lawrence
* Nastassja Kinski as Collette Andrews
* Charlie Hofheimer as Scott Andrews
* Bruce Greenwood as Bob Andrews
* Charles Rocket as Russ Trainor
* Patti DArbanville as Shirley Trainor
* Jared Harris as Lee
* Louis Lombardi as Matt

==Release==
In South Africa, Fathers Day was released as Whats Up Pops?, a title the distributor decided would be more appropriate for the local market. The name was subsequently changed to Whats Up Pops? for DVD release, when they realized the apostrophe had been used incorrectly.

==Reception==
The film received generally negative reviews from critics and was commercially unsuccessful with audiences. Fathers Day holds a 25% rating on Rotten Tomatoes based on 59 reviews. 

  Batman & Robin. 

==References==
 

==External links==
 
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 