008: Operation Exterminate
 
{{Infobox film
| name           = 008: Operation Exterminate (008: Operazione sterminio)
| image          = A 008, operazione Sterminio.jpg
| director       = Umberto Lenzi
| producer       = Fortunato Misiano 
| writer         = Umberto Lenzi 
| music          = Angelo Francesco Lavagnino   
| cinematography = Augusto Tiezzi
| editor         = Jolanda Benvenuti   
| released       = September 3, 1965
| runtime        = 84 minutes
| country        = Italy
| language       = Italian
}} 1965 Italian Eurospy action film directed and written by Umberto Lenzi and filmed in Egypt.

==Plot==
Agent 006 must recover a powerful super car technology called "chaff"; his fellow agent 008 follows him because she suspects something sinister. After agent 006 is able to recover the original plans of the machine in a journey from Switzerland to Egypt, agent 008 discovers that he is actually a Russian spy ...

==Cast==
*Ingrid Schoeller ...  MacDonald, agent 008 
*Alberto Lupo ...  Frank Smith, agent 006 
*Dina De Santis ...  Beauty institute manager 
*Ivano Staccioli ...  Kemp, hotel manager (as John Heston) 
*Mark Trevor ...  Munk 
*Omar El-Hariri ...  Police officer 
*Ahmed Louxor  (as Amed Luxor) 
*George Wang ...  Tanaka 
*Edoardo Toniolo ...  Mister X 
*Nando Angelini ...  Police lieutenant 
*Domenico Ravenna ...  Heinz 
*Omar Targoman
*Fortunato Arena ...  Stabbed man (as Lucky Arena)

== External links ==
*  

 

 
 
 
 
 
 
 
 
 



 
 