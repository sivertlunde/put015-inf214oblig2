Love in the Ring
{{Infobox film
| name = Love in the Ring
| image =
| image_size =
| caption =
| director = Reinhold Schünzel
| producer = Max Glass  
| writer =  Max Glass   Fritz Rotter 
| narrator =
| starring = Max Schmeling   Renate Müller   Olga Tschechowa
| music = Artur Guttmann   Will Meisel  
| cinematography = Nicolas Farkas   
| editing =     
| studio = Terra Film 
| distributor = Terra Film
| released =17 March 1930 
| runtime = 81 minutes
| country = Germany  German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Love in the Ring (German:Liebe im Ring) is a 1930 German sports film directed by Reinhold Schünzel and starring  Max Schmeling, Renate Müller and Olga Tschechowa.  Schmeling was a leading German boxer of the 1930s, and the film attempted to capitalise on this. Schmeling later appeared in another boxing-themed film in Knockout (1935 film)|Knockout (1935).
 sound was Otto Erdmann and Hans Sohnle. Some of the film was shot at the Berlin Sportpalast.

==Cast==
*  Max Schmeling as Max - Sohn der Obsthänderlin / Son of the Fruit seller
* Renate Müller as Hilde - Tochter des Fischhändlers / The Fish sellers daughter  
* Olga Tschechowa as Lilian  
* Frida Richard as Maxs Mutter / Obsthändlerin / Fruit seller
* Rudolf Biebrach as Fischhändler / Fish seller
* Kurt Gerron as Maxs Manager  
* Max Machon as Erster Trainer  
* Hugo Fischer-Köppe as Zweiter Trainer  
* Julius Falkenstein as Ein Lebemann / A Bon-Vivant  
* Yvette Darnys 
* Arthur Duarte 
* Emil Heyse
* Heinrich Gotho 
* José Santa as Selbst / Himself, Jose Santa  
* Harry Stein as Selbst / Himself, Harry Stein  
* Paul Noack as Selbst / Himself, Paul Noack  
* Fritz Rolauf as Selbst / Himself, Fritz Rolauf 
* Hermann Herse as Selbst / Himself, Hermann Herse 
* Egon Stief as Selbst / Himself, Egon Stief  
* Felix Friedemann as Selbst / Himself, Felix Friedemann 
* Erich Kohler as Selbst / Himself, Erich Kohler 
* Paul Samson-Körner as Unbestimmte Rolle / Undetermined Role  
* Reinhold Schünzel as Unbestimmte Rolle / Undetermined Role

== References ==
 

== Bibliography ==
* Waldman, Harry. Nazi Films in America, 1933-1942. McFarland, 2008.

== External links ==
*  

 

 
 
 
 
 
 
 

 