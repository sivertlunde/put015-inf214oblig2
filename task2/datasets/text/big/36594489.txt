Forever Blues
{{Infobox film
 | name = Forever Blues
 | image =  Forever Blues film.jpg
 | image_size =
 | caption =
 | director =  Franco Nero
 | writer =
 | starring =  Franco Nero
 | music =  Lino Patruno
 | cinematography = Giovanni Cavallini
 | editing = 
 | producer =
 | distributor =
 | released = 2005
 | runtime =
 | awards =
 | country =
 | language =  Italian
 | budget =
 }} 2005 Cinema Italian drama film written, directed and starred by Franco Nero, at his debut as director.  It is loosely based on the drama with the same name by Enrico Bernard.  At the time of its release the film was openly praised by Italian first lady Franca Ciampi, wife of President of the Italian Republic Carlo Azeglio Ciampi, that defined the film as "educational, emotional, bright and poetic". 

== Cast ==
*Daniele Piamonti: Marco  
*Franco Nero: Luca
*Valentina Mezzacappa: Marcos lover
*Robert Madison: Marco adult
*Minnie Minoprio: the singer

==References==
 

==External links==
* 

 
 

 