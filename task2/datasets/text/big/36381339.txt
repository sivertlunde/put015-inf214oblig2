Karmayodha
{{Infobox film
| name           = Karmayodha
| image          = Karma Yodha.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Major Ravi
| producer       = Haneef Mohammed Major Ravi
| writer         = Major Ravi Saikumar Aishwarya Devan MG Sreekumar  Background Score :  Jeffrey Jonathan
| cinematography = Pradeep Nair
| editing        = Donmax
| studio         = Red Rose Creations MR Productions
| distributor    = Red Rose Release 
                   PJ Entertainments Europe
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
}}

Karmayodha  is a 2012 Malayalam action thriller film co-produced, written and directed by Major Ravi. It features Mohanlal, Asha Sarath, Murali Sharma, Saikumar (Malayalam actor)|Saikumar, Mukesh (actor)|Mukesh, Bineesh Kodiyeri, Rajeev Pillai and Aishwarya Devan. The soundtrack was composed by M. G. Sreekumar with lyrics by Dr. Madhu Vasudevan; the background score was provided by Jeffrey Jonathan. Popular dancer and television actress Asha Sharath is featured as the wife of the character played by Mohanlal.The film deals about the illegal activities happening against women. The film was dubbed in Tamil as Vetrimaaran IPS.

==Plot==
Madhavan Menon (Maddy) is a ruthless investigating officer working with the Mumbai Police. He is an encounter specialist who makes decisions on the spot, without always waiting for orders from the top. His latest assignment is to investigate the missing of a teenage school girl from Mumbai. He reaches Kerala on being tipped off that the girl could be taken to Kerala. In the meantime, another girl gets kidnapped in Kerala, presumably by the same people or people linked to them. There is also information that some other girls too have been kidnapped. Following the leads, Maddy comes to the conclusion that all the girls have been abducted by a Kerala-based sex trafficking racket headed by a psycho criminal named Khais Khanna. Maddy embarks on a mission to eliminate the racket and save the girls, including his daughter who gets kidnapped, Diya.

The film depicts children being trapped by mafia groups taking advantage of technology like mobile phones, and dwells on the international sex trafficking. The film also depicts the diminishing joint family system in society, suggesting that with the increasing trend towards the nuclear family in society, what is getting uprooted is the bonding between family members and the security that they offer along with love and affection to the future generation.

==Cast==
 
* Mohanlal as Madhava Menon/Mad Maddy
* Murali Sharma as Khais Khanna
* Bineesh Kodiyeri as Manu
* Basil
* Badri Saikumar
* Mukesh
* Asha Sarath as Mad Maddys wife
* Aishwarya Devan
* Sona Heiden
* Malavya as Diya Rajeev Pillai as ACP K. Tony
* Kannan Pattambi as Thenkasi Selvam
* Vishnu Hariharan Shaalin
* Riyaz Khan as Sathan Sunny
*Sudheer Karamana   as Anoob
* Anil Murali
*Santosh Sleeba as cop Janardhanan
* Sukumari as Maddys mother
* Piyush Vijayan Lakshmi Menon
* Sasi Kalinga
* Vinita Menon
* Ajay MP
* Pushpa Rajan
* Pradeep Chandran
* Dr. Roni as Commissioner
 

==Production==
The film was shot on locations in Mumbai, Cochin, Munnar and Nagercoil. Filming began in August 2012.

The puja (Hinduism)|pooja ceremony of the film was held on 7 July 2012 at the BTH Sarovaram Hotel in Cochin, India. 
 Keerthi Chakra, Kurukshetra (2008 film)|Kurukshetra and Kandahar (2010 film)|Kandahar.   

==Marketing==
The first-look poster was released in September 2012; the films first teaser was released on October 3, 2012, and a theatrical trailer was released on November 25, 2012.

==Soundtrack==
{{Infobox album 
| Name = Karmayodha
| Artist = M. G. Sreekumar
| Type = Soundtrack
| caption = Original CD cover
| Cover = 
| Released = November 25, 2012
| Recorded =
| Genre = Film soundtrack
| Length = 15:28
| Language = Malayalam
| Label = Music Zone
| Producer = M. G. Sreekumar
| Last album = Ardhanaari (2012)
| This album = Karmayodha (2012)
| Next album = 
}} MG Sreekumar, Pradeep Nair, Pranav Mohanlal, Renji Panicker, Jeffrey Jonathan and other personalities from the film industry, along with the technical crew and cast of the film. The background score for the film was provided by Jeffrey Jonathan. The film notably features Murukan Kattakadas famous poem "Kannada", which has been rendered by the poet himself.

{{tracklist
| headline = Tracklist
| extra_column = Singer(s)
| total_length = 15:28
| lyrics_credits = yes
| title1 = Mooliyo Vimookamay
| lyrics1 = Madhu Vasudevan
| extra1 = Najim Arshad
| length1 = 4:20
| title2 = Karmayodha Theme 
| lyrics2 = Madhu Vasudevan
| extra2 = Jeffrey Jonathan
| length2 = 2:55
| title3 = Ellavarkkum
| lyrics3 = Murukan Kattakada
| extra3 = Murukan Kattakada
| length3 = 3:40
| title4 = Karmayodha Rap
| lyrics4 = Madhu Vasudevan
| extra4 = Jeffrey Jonathan
| length4 = 4:20
}}

==Release==
The distribution rights for Karmayodha in Kerala were bought by Red Rose Release. Karmayodhas premier show was held on December 20, 2012 in Dubai, United Arab Emirates.  The films worldwide release took place on December 21, 2012. Karmayodha was released in Europe and the UAE on December 27, 2012.

==Critical reception==
Sify.com deemed the movie "Disappointing", saying: "A rather flimsy plot, some bad performances, silly lines and villains who are unintentionally comical make you cringe in the seats. The plot moves ahead without a definite aim or direction with some weak links that barely look convincing." 

Rediff.com rated the movie 2 out of 5, saying: "Karmayodha is not worth watching". http://www.rediff.com/movies/review/south-review-karmayodha-is-not-worth-watching/20121226.htm 

Indiaglitz gave the film an average rating of 5.9/10, saying that "Karmayodha may satisfy the die-hard Mohanlal fans." 

== References ==
 

==External links==
*  