Krishna (2008 film)
{{Infobox film
| name           = Krishna
| image          = KIRSHNA.png
| caption        =
| director       = V.V. Vinayak
| producer       = B. Kasi Vishwanandham
| writer         = V.V. Vinayak Shiva Akula
| starring       = Ravi Teja Trisha Krishnan Raghu Babu  Kadhal Dhandapani  Sayaji Shinde Mukul Dev Bramhanandam Chakri
| music release  = 28 December 2007
| country        = India
| cinematography = Chota K. Naidu
| editing        = Gowtham Raju
| released       = 12 January 2008
| runtime        = Telugu
| budget         =  18 crores
}}

Krishna is a   in Telugu. 

==Plot== Hyderabad who Chandra Mohan) and sister in law (Sudha). In this process Krishna will have clash with Local Rowdy Lanka Raju (Kadhal Dhandapani). Mistaking him to be one Tapori, Sandhya hates him first but later on she realizes his true nature . She returns to Hyderabad and lives with her older brother (Sayaji Shinde), a former builder and now a very powerful rowdy who is very possessive and protective about his sister. Krishna follows Sandhya to Hyderabad and works his way into their house with the help of Bobby and finally both of them confess their love. There, Krishna knows the flashback of Sandhya and how she is being chased by the notorious and cruel Jagga (Mukul Dev) assisted by his uncle (Jaya Prakash Reddy) for marriage. Krishna fights Jagga, Sandhyas older brother kills Jagga, and Krishna marries Sandhya.

==Cast==
  was selected as lead heroine marking her first collaboration with Ravi Teja.]]
* Ravi Teja ... Krishna
* Trisha Krishnan ... Sandhya
* Mukul Dev ... Jakka Chandra Mohan ... Chandra Shekar
* Kadhal Dhandapani ... Lanka Raju
* Brahmanandam ... Bobby Sunil ... Krishnas friend Venu Madhav ... Tenant Kalyani ... Maid
* Sayaji Shinde ... Sandhyas brother
* Jaya Prakash Reddy ... Jakkas uncle
* Sana ... Bobbys wife
* Sudha ... Chandra Shekars wife

==Background==
This is the first movie which casts actor Ravi Teja with actress Trisha Krishnan.  This has been a much anticipated combination by many, including the hero Ravi Teja.
Ravi Tejas lack of enthusiasm in recent performances in Khatarnak and Dubai Seenu has spurred director V. V. Vinayak to tell the hero he must be more original, genuine, and enthusiastic about this present venture.  Meanwhile, Trisha is busy with two films simultaneously after the success of Aadavari Matalaku Ardhalu Verule. One of these films is Krishna and the other is Bujjigadu, starring Prabhas, filming mainly in her hometown and residence Chennai.

==Filming==
The filming crew filmed in Vijayawada for a couple of weeks, departing on the 20th of September.  There was a craze in Vijayawada to catch glimpses of filming. As per request of Ravi Teja, who hails from Vijayawada, the crew also filmed a song there.

==Box-office performance==
The film grossed  5.6 crores (56 million) in its opening week  and grossed  11.88 crores in 2 weeks. By the end of its 4th week, it had grossed  15 crores. 
==Crew==
* Direction: V.V. Vinayak
* Writer: Akula Siva
* Editing: Gowtham Raju
* Music: Chakri
* Action: Ram Lakshman

==Television rights==
The Television Rights of the film was sold to Zee Telugu.

==References==
 

== External links ==
*  

 

 
 
 
 
 
 