The Abominable Dr. Phibes
 
 
 
 
{{Infobox film
| name           = The Abominable Dr. Phibes
| image          = Abominablephibes1.jpg
| caption        = Theatrical Poster
| director       = Robert Fuest
| producer       = Ronald S. Dunas Louis M. Heyward
| writer         = James Whiton William Goldstein Uncredited: Robert Fuest
| starring       = Vincent Price Joseph Cotten Peter Jeffrey Virginia North
| music          = Basil Kirchin
| cinematography = Norman Warwick
| editing        = Tristam Cones American International England (UK)
| released       = April 1971
| runtime        = 94 minutes
| country        = United Kingdom
| language       = English
| budget         =
|}} dark humour cult classics. cameo appearance by future Bond girl Caroline Munro. 

The screenplay was written by James Whiton and William Goldstein and the 93 minute feature film was directed by Robert Fuest.  In the storyline the title character, Phibes, blames the medical team that attended to his wife for her death four years prior and sets out to exact vengeance on each one.  Phibes is inspired in his murderous spree by the Ten Plagues of Egypt from the Old Testament.  It is one of the last things Keith Moon of The Who did, just prior to his death from an overdose of Heminevrin in 1978 was to watch the film. 

==Plot==
Dr. Anton Phibes (Vincent Price) was an expert in theology and music who was supposedly killed in a car crash in 1921, shortly after the death of his beloved wife, Victoria, during an operation. However, he survived the crash, horribly scarred by the accident and left unable to speak, forcing him to remake his face with prosthetics and use his knowledge of acoustics to regain his voice. Resurfacing in 1925, Phibes believes that his wife died a victim of incompetent doctors, and begins elaborate plans to kill them.
 Scotland Yard, learns that they had all worked together under the direction of Dr. Vesalius (Joseph Cotten), who reveals that all of the deceased had been on his team in Victorias case, as well as four other doctors and a nurse. When another murder is reported, Trout suspects Phibes is alive, and he and Vesalius go to the Phibes mausoleum at Highgate Cemetery. They find ashes in a box in Phibes coffin, which Trout believes are the remains of Phibes chauffeur; Victorias coffin is empty.

Despite all of the polices best efforts, Phibes is able to kill the remaining doctors and the nurse. Reserving the final punishment for Dr. Vesalius, he kidnaps the doctors son Lem, then calls Vesalius and tells him to come alone to his mansion on Maldene Square if he wants to save his sons life. Despite Trouts protests, Vesalius knocks the inspector unconscious and immediately races to Phibes mansion, where he confronts the mad doctor. Phibes has placed Vesalius son under anesthesia and prepared him for surgery; a small key implanted near the boys heart will unlock his restraints, but Vesalius must perform the surgery within six minutes (the same amount of time Victoria was on the operating table before her death) to get the key before acid from a container above Lems head falls and destroys his face. Vesalius succeeds and moves the table out of the way; Vulnavia, backing away from the police, is sprayed with the acid instead.
 embalmed body they will encounter Phibes again.

;The Ten Plagues of Egypt
Dr. Phibes takes his inspiration for the murders from the Old Testament, the Ten plagues of Egypt.  The plagues described in the movie differ slightly from the Biblical account:
# Boils: Prof. Thornton is stung to death by bees. (Not shown, only referred to during the film)
# Bats: Dr. Dunwoody is mauled to death by bats.  (This one is not a Biblical plague but may be related to the plague of lice or gnats.)
# Frogs: Dr. Hargreaves (who is not a surgeon but a psychiatrist) has his head crushed by a mechanical mask of a frog
# Blood: Dr. Longstreet has all the blood drained out of his body
# Hail: Dr. Hedgepath is frozen to death by a machine spewing ice
# Rats: Dr. Kitaj crashes his plane when attacked by rats. (This is not a Biblical plague but may be related to the plague of pestilence.)
# Beasts: Dr. Whitcombe is impaled by a brass unicorn head
# Locusts: Nurse Allen is eaten by locusts.
# Death of the first born: Phibes kidnaps and attempts to kill Dr. Vesaliuss son Lem. (This is the final plague in the Biblical account.)
# Darkness: At the ambiguous ending of the film, Phibes drains the blood from his own body while injecting embalming fluid, apparently joining his wife in death.

==Cast==
{{columns-list|2|
* Vincent Price as Dr. Anton Phibes
* Joseph Cotten as Dr. Vesalius
* Hugh Griffith as Rabbi
* Terry-Thomas as Dr. Longstreet
* Virginia North as Vulnavia
* Peter Jeffrey as Inspector Harry Trout
* Derek Godfrey as Crow Norman Jones as Sgt. Tom Schenley
* John Cater as Waverley
* Aubrey Woods as Goldsmith
* John Laurie as Darrow
* Maurice Kaufmann as Dr. Whitcombe
* Sean Bury as Lem Vesalius
* Susan Travers as Nurse Allen
* David Hutcheson as Dr. Hedgepath
* Edward Burnham as Dr. Dunwoody Alex Scott as Dr. Hargreaves
* Peter Gilmore as Dr. Kitaj
* Caroline Munro as Phibess wife, Victoria Regina Phibes (uncredited)
}}

==Production notes== Writers Guild Credit Arbitration Committee confirmed their Written By credits. Peter Cushing was the first choice for the role of Vesalius, but he turned down the role because of the serious illness of his wife. The name "Vesalius" is a reference to Andreas Vesalius, a Flemish physician and founder of modern anatomy.

In order for Joseph Cotten to know his cues, Phibes dialogue was read aloud by a crew-member. Cotten would grumble that he had to remember and deliver lines, while Vincent Prices were all to be post-dubbed. Price responded, "Yes, but I still know them, Joe." (In fact, Price was well known in Hollywood for his ability to memorise all of the characters lines in a given production). Price commented that Cotten was uncomfortable doing these scenes, so he intentionally made faces to make him laugh. Price went through hours of make up, which often had to be reapplied as he kept laughing. Dr. Phibes wife, played by 1970s model, Caroline Munro (who later appeared in such Hammer Horror fare as Dracula A.D. 1972 and Captain Kronos, Vampire Hunter), was excluded from the films credits.

The movie was filmed at the "thirties era" sets at Elstree Studios in Hertfordshire. The cemetery scenes were shot in Highgate Cemetery, London.  The exterior of Dr. Phibes mansion was Immanuel College on Elstree Road. The film was followed by a sequel, Dr. Phibes Rises Again, in 1972. Several other possible sequels were planned, including Dr. Phibes in the Holy Land, The Brides of Phibes, Phibes Resurrectus and The Seven Fates of Dr. Phibes, but none were made. 

==Critical reception==
 
Critic Christopher Null wrote of the film, "One of the 70s juiciest entries into the horror genre, The Abominable Dr. Phibes is Vincent Price at his campy best, a former doctor and concert organist (go figure that one out yourself)  who is exacting revenge on the nine doctors he blames for botching his wifes surgery, which ended with her death. Through a series of tortuous means that would make a Bond villain green with envy, the hideous Phibes is matched by Joseph Cotten as the doc at the end of the road. A crazy script and an awesome score make this a true classic." 
 Time Out conducted a poll with several authors, directors, actors and critics who have worked within the horror genre to vote for their top horror films.  The Abominable Dr. Phibes placed at number 83 on their top 100 list. 
 American International Pictures home office until it became a hit at the box office. Gary A. Smith, The American International Pictures Video Guide, McFarland 2009 p 9-10 

==Home media availability==
MGM Home Entertainment released The Abominable Dr. Phibes on Region 2 DVD in 2001, followed by a tandem release with Dr.Phibes Rises Again in 2005. The film made its Blu-ray debut as part of Scream Factorys Vincent Price boxed set in Fall 2013.  

A limited edition two-disc set,The Complete Dr Phibes, was released in Region B Blu-ray in 2014 by Arrow Films. 

==Music==
The music that Dr. Phibes plays on the organ at the beginning of the film is "War March of the Priests" from Felix Mendelssohns incidental music to Racines play Athalie.
 incidental score was composed by Basil Kirchin and includes 1920s-era source music, most notably "Charmaine (song)|Charmaine" and "Darktown Strutters Ball".
 soundtrack LP LP was released concurrently with the films appearance, which contained few selections from the films score but rather was composed mostly of character vocalisations by Paul Frees.   A proper soundtrack was released on CD in 2004 by Perseverance Records and is now out of print.

==Sequel== screenplays and sequels were proposed well into the 1980s featuring potential actors such as David Carradine, Roddy McDowall, and Orson Welles. 

==References==
 

==Bibliography==
 
* {{Cite book |title= Robert Fuest e labominevole Dottor Phibes |last= Gerosa |first= Mario |year= 2010 |publisher= Edizioni Falsopiano |location= Alessandria,
|isbn=978-88-89782-13-2 }}
* Klemensen, Richard; publisher. "The Definitive Dr. Phibes." Little Shoppe of Horrors. Des Moines, Iowa, October 2012: Number 29.

==External links==
*  
*  
*  
*   at Hollywood Gothique. 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 