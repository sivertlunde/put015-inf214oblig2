Gabrielle (1954 film)
{{Infobox film
| name           = Gabrielle
| image          =
| caption        =
| director       = Hasse Ekman
| producer       = Svensk Filmindustri
| writer         = Hasse Ekman, Walter Ljungquist
| narrator       =
| starring       = Eva Henning Birger Malmsten Hasse Ekman Inga Tidblad
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = 90 minutes
| country        = Sweden Swedish
| budget         =
| gross          =
}}
 Swedish drama drama film directed by Hasse Ekman.

==Plot==
Bertil Lindström starts to work at the Swedish embassy in Paris while his wife Gabrielle choose to spend the summer alone in the Stockholm archipelago. She is very secretive about her past. He then starts to fantasize about all sorts of things she possibly can be doing separated from him, is she true to him? His jealousy and imaginative mind plays him all sorts of tricks on this theme.

==Cast==
*Eva Henning as Gabrielle Lindström
*Birger Malmsten as Bertil Lindström
*Hasse Ekman as Kjell Rodin
*Inga Tidblad as Marianne
*Carl Ström as Tor Fagerholm
*Karin Molander as Mrs. Fagerholm
*Gunnar Björnstrand as Robert Holmén 
*Åke Claesson as Malmrot
*Oswald Helmuth as Jensen, bartender  Gunnar Olsson as Engkvist 
*Lars Egge as Flight passenger
*Hanny Schedin as Newspaper woman

==External links==
* 

 

 
 
 
 
 

 