Paper Clips (film)
{{Infobox film
| name           = Paper Clips
| image          = Paper_Clips.jpg
| image_size     =
| caption        =
| director       = Elliot Berlin Joe Fab
| producer       = Joe Fab Robert M. Johnson Ari Daniel Pinchot
| writer         = Joe Fab
| narrator       = 
| starring       = Sandra Roberts, Dagmar Schroeder-Hildebrand, Tom Bosley
| music          = Charlie Barnett
| cinematography = Michael Marton
| editing        = Julia Dixon-Eddy
| studio         = Ergo Entertainment The Johnson Group
| distributor    = Miramax Films
| released       =  
| runtime        = 82 minutes 
| country        = United States
| language       = English
| gross          = $1,121,744    
| preceded_by    =
| followed_by    =
}}

Paper Clips is a 2004 documentary film written and produced by Joe Fab, and directed by Elliot Berlin and Joe Fab, about the Paper Clips Project, in which a middle school class that tries to collect 6 million paper clips to represent the 6 million Jews killed by the Nazis.

==Synopsis==
Paper Clips takes place in the rural, blue-collar Tennessee community of Whitwell, Tennessee|Whitwell, where a middle-school class attempts to gauge the magnitude of World War IIs Holocaust by collecting paper clips, each of which represents a human life lost in the Nazis slaughter of Jews.    The idea came in 1998 from three of the teachers at the school and was completed in their eighth grade classrooms.     The students ultimately succeeded in collecting over 25 million paperclips.   

==Production==
The documentary film about the project was officially released in 2004. This films genesis lies with Rachel Pinchot who initially saw an article about the Whitwell Middle School in the Washington Post.  She took the idea of a film to her husband, Ari Pinchot, of The Johnson Group. The Johnson Group sent a team to Whitwell to film key moments, such as the arrival of several Holocaust survivors from New York who shared their experiences with the community. Out of that footage, Elliot Berlin created a seven-minute presentation. With help from Ergo Entertainment and its partners Donny Epstein, Yeeshai Gross, and Elie Landau (who joined the project as executive producers), this "demo" helped to convince the Miramax film company that this project was worth a full-length movie. It was described as being not yet another movie showing the tragedy, but a project of hope and inspiration. The movie features interviews with students, teachers, Holocaust survivors, and people who sent paper clips. It also shows how the railcar traveled from Germany to Baltimore, and then Whitwell. The creators had accumulated about 150 hours of footage. The movie was shown for the first time in November 2003 in Whitwell.

==Awards==
* Audience Choice Award - Jackson Hole Film Festival (2004), Marco Island Film Festival (2004), Palm Springs International Film Festival (2004), Rome International Film Festival (2004), Washington Jewish Film Festival (2004)
* Cowboy Award - Jackson Hole Film Festival (2004)
* NBR Award for Top Five Documentaries - National Board of Review (2004)
* Best Overall Film, Best Director, Best Original Score - Rome International Film Festival (2004)

* Nominated for an Emmy for Outstanding Historical Programming (2006)   

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 