Alaap
{{Infobox film
| name           = Alaap
| image          = Alaap.jpg
| image_size     = 
| caption        = 
| director       = Hrishikesh Mukherjee
| producer       = Hrishikesh Mukherjee N. C. Sippy Romu N. Sippy
| writer         = Rahi Masoom Raza, Biren Tripathy, Jehan Nayyar (Dialogue)
| story          = Hrishikesh Mukherjee
| screenplay     = Bimal Dutta
| narrator       = 
| starring       = Amitabh Bachchan Rekha Asrani Farida Jalal Om Prakash
| music          = Jaidev Rahi Masoom Raza (Lyrics)
| cinematography = Jaywant Pathare
| editing        = Khan Zaman Khan
| studio         = 
| distributor    = Rupam Chitra
| released       =  
| runtime        = 161 minutes
| country        = India
| language       = Hindi
| budget         =   }}
Alaap is a 1977 Indian film produced by Hrishikesh Mukerjee and N.C Sippy and directed by Hrishikesh Mukherjee. The film stars Amitabh Bachchan, Rekha, Asrani, Farida Jalal, Om Prakash and A. K. Hangal. The music is by Jaidev.

==Plot==
Widowed Advocate Triloki Prasad (Om Prakash) lives a wealthy lifestyle in a small town in India with two sons, Advocate Ashok (Vijay Sharma) married to Geeta (Lily Chakravarty) and Alok (Amitabh Bachchan) who has yet to settle down in their law firm. Alok is fond of music and does enroll in classes run by Pandit Jamuna Prasad (A. K. Hangal). On his return, his father asks him to accompany Ashok to their law firm and start learning to practice, which he agrees to do. Then one day Triloki finds out that Alok has not been at the firm but is instead spending time in the local slums with a former courtesan named Sarju Bai Banaraswali (Chhaya Devi). He cautions Alok about this, but Alok continues to visit Sarju Bai. When Mr. Gupta (Yunus Parvez) approaches Triloki about taking possession & demolishing the slum area, Triloki readily agrees and with his expertise manages to sway the Courts decision in Guptas favor. As a result, Sarju Bai and others are rendered homeless. With the fee he receives from Gupta, he asks Alok to purchase a used car for himself. But Alok purchases a horse-carriage and decides to drive it himself to make a living. His enraged father asks him to leave the house. When Triloki finds out that Alok is doing well in his work, he decides to hire motor coaches to transport people at a much cheaper price, thus cutting off Aloks earnings and perhaps forcing him to reconsider his decision, apologize, and return home to his father. The question remains, will these actions force Alok to return home, or will the misunderstandings between father and son continue indefinitely

==Cast==
*Amitabh Bachchan  as  Alok Prasad
*Rekha  as  Radhakumari (Radhiya)
*Om Prakash  as  Advocate Triloki Prasad
*Vijay Sharma  as  Ashok Prasad Lily Chakraborty  as  Geeta A. Prasad
*Master Ravi  as  Alok & Radhas son
*Chhaya Devi  as  Sarju Bai Banaraswali
*Asrani  as  Ganesh (Ganeshi)
*Manmohan Krishna  as  Maharaj Dinanath
*Farida Jalal  as  Sulakshana Gupta
*Yunus Parvez  as  Mr. Gupta
*Lalita Kumari  as  Mrs. Gupta
*Benjamin Gilani  as  Kishan
*A. K. Hangal  as  Pandit Jamuna Prasad (Guest Appearance)
*Sanjeev Kumar  as  Raja Bahadur (Guest Appearance)
*Jhumur - Guest Appearance

==Crew==
*Director - Hrishikesh Mukherjee
*Story - Hrishikesh Mukherjee
*Screenplay - Bimal Dutta
*Dialogue - Rahi Masoom Raza, Biren Tripathy, Jehan Nayyar
*Editor - Khan Zaman Khan
*Producer - Hrishikesh Mukherjee, N.C. Sippy, Romu N. Sippy
*Production Company - Rupam Chitra
*Cinematographer - Jaywant Pathare
*Art Director - Ajit Banerjee
*Costume Designer - Bhanu Athaiya, Shalini Shah
*Costume and Wardrobe - Babu Ghanekar

==Soundtrack==
{{Track listing
| headline     = Songs
| extra_column = Playback
| all_lyrics   = Rahi Masoom Raza
| all_music    = Jaidev

| title1  = Aai Ritu Sawan Ki Bhupinder Singh, Kumari Faiyaz
| length1 = 3:33

| title2  = Binati Sun Le Tanik
| extra2  = Asrani
| length2 = 3:38

| title3  = Chand Akela Jaye Sakhi Ri
| extra3  = Yesudas
| length3 = 3:36

| title4  = Chand Akela
| extra4  = Yesudas
| length4 = 3:26

| title5  = Ho Rama Dar Lage Apni Umariya Se
| extra5  = Asrani
| length5 = 3:27

| title6  = Kahe Manva Nache
| extra6  = Lata Mangeshkar
| length6 = 3:11

| title7  = Koi Gata Main So Jata
| extra7  = Yesudas
| length7 = 3:31

| title8  = Mata Saraswati Sharda 	
| extra8  = Lata Mangeshkar, Dilraj Kaur
| length8 = 3:49

| title9  = Mata Saraswati Sharda 	
| extra9  = Yesudas, Dilraj Kaur, Madhurani
| length9 = 3:30

| title10  = Nai Ri Lagan Aur Meethi Batiyan
| note10   = 
| extra10  = Yesudas, Madhurani, Kumari Faiyaz
| length10 = 3:56

| title11  = Zindagi Ko Sanwarna Hoga
| extra11  = Yesudas
| length11 = 3:36
}}

==External links==
 

 

 
 
 
 