La Voltige
{{Infobox film
| name           = La Voltige
| image          = Cinématographe Lumière.jpg
| image_size     = 
| caption        = The poster advertising the Lumière brothers cinematographe
| director       = Louis Lumière
| producer       = Louis Lumière
| writer         = 
| narrator       = 
| starring       = 
| music          = 
| cinematography = Louis Lumière
| editing        = 
| distributor    = 
| released       = 1895 July 1896 (Finland)
| runtime        = 46 seconds
| country        = France Silent
| budget         = 
| gross          =
| preceded_by    = 
| followed_by    = 
}}
 1895 France|French short black-and-white silent documentary film directed and produced by Louis Lumière. It was filmed in Lyon, Rhône (département)|Rhône, Rhône-Alpes, France. Given its age, this short film is available to freely download from the Internet.

The film formed part of the first commercial presentation of the Lumière Cinématographe on December 28, 1895 at the Salon Indien, Grand Café, 14 Boulevard des Capuchins, Paris. 

==Production==
As with all early Lumière movies, this film was made in a  , an all-in-one camera, which also serves as a film projector and developer. 

==Plot==
Three men and a horse stand in a field. The first man in white holds the reins of the horse, the second man in black stands observing while the third man attempts to mount the horse. After six unsuccessful attempts he is finally able to seat himself and is set to ride off Sidesaddle.

==References==
 

==External links==
*   (requires QuickTime)
*  
*   on YouTube

 

 
 
 
 
 
 
 


 
 