Bonjour Tristesse (film)
 
{{Infobox film
| name           = Bonjour Tristesse
| image          = Bonjour Tristesse film poster.jpg
| image_size     =
| caption        = Film poster, designed by Saul Bass
| director       = Otto Preminger
| producer       = Otto Preminger
| based on       =  
| screenplay     = Arthur Laurents
| starring       = {{unbulleted list|
*Deborah Kerr
*David Niven
*Jean Seberg
*Mylène Demongeot
*Geoffrey Horne}}
| music          = Georges Auric
| cinematography = Georges Périnal   
| editing        = Helga Cranston
| distributor    = Columbia Pictures 
| released       =  
| runtime        = 94 minutes
| country        = {{unbulleted list|
*United Kingdom
*United States}}
| language       = English
| budget         = 
| gross          =
}}
 French "Hello, novel of the same title by Françoise Sagan. The film stars Deborah Kerr, David Niven, Jean Seberg, Mylène Demongeot and Geoffrey Horne, and features Juliette Gréco, Walter Chiari, Martita Hunt and Roland Culver. It was released by Columbia Pictures. This film had colour and black and white sequences, a technique unusual for the 1950s but widely used in silent movies and early talking films.

==Plot==
On the French Riviera, Cécile is a decadent young girl who lives with her rich playboy father, Raymond. Anne, a mature and cultured friend of Raymonds late wife, arrives at Raymonds villa for a visit. Cécile is afraid that Anne will disrupt the undisciplined way of life that she has shared with her father.

Despite his promises of fidelity to Anne, Raymond cannot give up his playboy life. Helped by Elsa, Raymonds young and flighty mistress, Cécile does her best to break up the relationship with Anne. The combination of the daughters disdain and the fathers rakishness drives Anne to a tragic end. 

==Cast==
* Deborah Kerr as Anne Larsen
* David Niven as Raymond
* Jean Seberg as Cécile
* Mylène Demongeot as Elsa
* Geoffrey Horne as Philippe
* Juliette Gréco as Juliette Greco
* Walter Chiari as Pablo
* Martita Hunt as Philippes Mother
* Roland Culver as Mr. Lombard
* Jean Kent as Mrs. Lombard
* David Oxley as Jacques
* Elga Andersen as Denise
* Jeremy Burnham as Hubert
* Eveline Eyfel as Maid

==Critical reception==
The film met with a lukewarm critical reception at the time. The BFIs Monthly Film Bulletin:  "The best performance is David Niven’s; he gives his part a pathetic touch that the writing never attains. Jean Seberg, who speaks rather than acts her lines, turns in the least effective performance. Bonjour Tristesse is an elegant, ice cold, charade of emotions, completely artificial and eventually torpid." Others enjoyed it rather more and it had some unexpected friends. François Truffaut described Seberg as “The best actress in Europe’. Jean-Luc Godard said "The character played by Jean Seberg   was a continuation of her role in Bonjour Tristesse, I could have taken the last shot of Premingers film and started after dissolving to a title: "Three years later". A Guardian piece   in 2012 described it as “an example of Hollywoods golden age, and both its star and its famously tyrannical director are ripe for rediscovery.”

The film currently holds an 86% approval rating according to Rotten Tomatoes. Critic Keith Uhlich of Time Out New York wrote: "the director uses the expansive CinemaScope frame and his eye for luxuriant, clinical mise en scéne to soberly probe rather than gleefully prod. The cast is across-the-board exemplary. Niven and Kerr keenly satirize their onscreen iconographies—the cad and the goody-goody, respectively—but it’s Seberg who cuts deepest."

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 