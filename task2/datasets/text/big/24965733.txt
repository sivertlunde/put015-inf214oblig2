Shocking Dark
 

 
{{Infobox film
| name           = Shocking Dark
| image          = Terminator-2-shocking-dark-poster.jpg
| image_size     = 
| alt            =  Terminator poster.
| director       = Bruno Mattei
| producer       = Franco Gaudenzi
| writer         = Claudio Fragasso
| starring       = Christopher Ahrens Haven Tyler Geretta Geretta Fausto Lombardi
| music          = Carlo Maria Cordio
| cinematography = Richard Grassetti
| editing        = Bruno Mattei
| studio         = Flora Film
| distributor    = Condor Vídeo
| released       =  
| runtime        = 90 minutes
| country        = Italy
| language       = Italian
| budget         = 
| gross          = 
}} science fiction film written by Claudio Fragasso and directed by Bruno Mattei.

==Background==
Despite the films original title and artwork presenting it as a sequel to 1984 film   (the official Terminator sequel) was released. The film was never released in the United States under its original title due to licensing problems.

==Partial cast==
* Christopher Ahrens as Samuel Fuller (as Cristopher Ahrens)
* Haven Tyler as Sara
* Geretta Geretta as Koster (as Geretta Giancarlo Field)
* Fausto Lombardi as Lieutenant Franzini (as Tony Lombardo)
* Mark Steinborn as Commander Dalton Bond
* Dominica Coulson as Samantha
* Clive Riche as Drake (as Clive Ricke)
* Paul Norman Allen as Kowalsky
* Cortland Reilly as Caine
* Richard Ross as Price
* Bruce McFarland as Parson
* Al McFarland as Raphelson

==Release==
In Late 2006, Terminator II (unofficial) was in a double feature with director Bruno Mattei, with his other "unofficial" sequel.

==References==
 

==External links==
* 
 
 
 
 
 
 
 
 