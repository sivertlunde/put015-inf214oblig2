The Astroduck
 
{{Infobox Hollywood cartoon|
| cartoon_name = The Astroduck
| series = Looney Tunes (Daffy Duck/Speedy Gonzales)
| image = 
| caption = 
| director = Robert McKimson
| story_artist =  Manny Perez Don Williams George Grandpré Norm McCabe
| layout_artist = Dick Ung
| background_artist = Tom OLoughlin
| voice_actor = Mel Blanc
| musician = Bill Lava
| producer = David H. DePatie Friz Freleng
| distributor    = Warner Bros. Pictures The Vitaphone Corporation
| release_date = January 1, 1966
| color_process = Technicolor
| runtime = 6
| movie_language = English 
}}
Astro Duck or The Astroduck  is a  1966 Looney Tunes cartoon featuring Daffy Duck and Speedy Gonzales. It was directed by Robert McKimson and was released on January 1, 1966.

==Synopsis==
Daffy buys a house in Mexico,  but Speedy will not leave the house. At the end Daffy blows his house up and Speedy says,
"We have a new astroduck!"

==See also==
* The Golden Age of American animation
* List of Daffy Duck cartoons

==Sources==
* . Online. July 1, 2008.
* . Online. July 1, 2008.

==External links==
* 

 
 
 
 
 


 