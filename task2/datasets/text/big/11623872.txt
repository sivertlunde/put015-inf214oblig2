One Way Passage
{{Infobox film
| name           = One Way Passage
| image          = One Way Passage - Film Poster.jpg
| image_size     = 225px
| caption        = Theatrical film poster
| director       = Tay Garnett
| producer       =   Joseph Jackson
| story          = Robert Lord
| narrator       = 
| starring       = William Powell Kay Francis
| music          =  
| cinematography = Robert Kurrle
| editing        = Ralph Dawson
| distributor    = Warner Bros.
| released       =  
| runtime        = 68 minutes
| country        = United States English
| budget         = $350,000   accessed 16 March 2014 
| gross          = $1.1&nbsp;million 
}}

One Way Passage (1932 in film|1932) is a romantic film starring William Powell and Kay Francis as star-crossed lovers, directed by Tay Garnett and released by Warner Bros.

==Plot== handcuffed (and non-swimmer) Steve with him, but spots Joan among the passengers and changes his mind. Once the ship is underway, he persuades Steve to remove his handcuffs. Dan and Joan fall in love on the month-long cruise, neither knowing that the other is under the shadow of death.

By chance, two of Dans friends are also aboard, thief Skippy (Frank McHugh) and con artist "Barrel House Betty" (Aline MacMahon), masquerading as "Countess Barilhaus". The countess distracts Steve as much as she can to help Dan. Just before the only stop, at Honolulu, Steve has Dan put in the brig, but he escapes with their help and goes ashore. Joan intercepts him and they spend an idyllic day together. When they drive back to the dock, Dan starts to tell her why he cannot return to the ship, only to have her faint. Dan carries her aboard for medical help, forfeiting his chance. Later, Joans doctor tells Dan about her condition and that the slightest excitement or shock could be fatal.

Meanwhile, the "countess" has spent so much time with the policeman that a romance blooms between them. When they near the end of the voyage, he awkwardly proposes to her. She tells him her true identity, but he still wants to marry her. As Steve and Dan get ready to disembark, a steward overhears the grim truth and, when Joan comes looking for Dan, tells her. The two lovers part for the last time without letting on they know each others secret, and Joan collapses after Dan is out of sight.

They had agreed to meet again on New Years Eve, a month later. At the appointed time and place, a bartender is startled when two glasses on the bar break with no one around.

==Cast (in credits order)==
* William Powell as Dan  
* Kay Francis as Joan  
* Aline MacMahon as Betty, aka "Barrel House" Betty and "Countess Barilhaus"
* Frank McHugh as Skippy
* Warren Hymer as Steve   Frederick Burton as The Doctor

== Reception ==
Mordaunt Hall wrote in The New York Times, "In its uncouth, brusque and implausible fashion, One Way Passage ... offers quite a satisfactory entertainment. ... Tay Garnetts direction is clever. He keeps the story on the move with its levity and dashes of far-fetched romance."  

In his autobiography Looking for a Street, Charles Willeford describes seeing the movie as a thirteen-year-old: 
:"One Way Passage" is still my all-time favorite movie, but I have never risked seeing it again. I cried so hard when the movie ended the usher took me out of the lobby and gave me a glass of water." 

===Box office===
According to Warners records, the film earned $791,000 in the US and Canada and $317,000 elsewhere. 

== Remake ==
It was remade in 1940 as Til We Meet Again, featuring Merle Oberon and George Brent.

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 