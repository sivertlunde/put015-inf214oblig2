Today and Tomorrow (1912 film)
{{Infobox film
| name           = Today and Tomorrow
| image          =
| image_size     =
| caption        =
| director       = Michael Curtiz|Mihály Kertész
| producer       =
| writer         = Imre Roboz Iván Siklósi
| narrator       =
| starring       = Gyula Abonyi Jenőné Veszprémy
| music          =
| cinematography =
| editing        =
| distributor    =
| released       = 1912
| runtime        =
| country        = Hungary
| language       =
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}} 1912 film directed by Michael Curtiz.

==Cast==
* Gyula Abonyi - Az öreg gróf
* Jenőné Veszprémy - Az öreg gróf felesége (as Veszprémyné)
* Endre Szeghő - Kázmér herceg
* Gyula Fehér - A bankár
* Ilonka Ordó - Pierette
* Gyula Szalay - Pierett
* Michael Curtiz - Arisztid
* Antal Hajdu - Dezső gróf
* Böske Kelemen - Szobaleány
* K. Kovács Andor - Inas (as K.Kovács Andor)
* László Gabányi - Tiszttartó (as Gabányi)
* Artúr Somlay - Mel gróf
* Ilona Cs. Aczél - A hercegnő (as Aczél Ilona)

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 