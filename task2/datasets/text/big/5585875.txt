The Aristocrats (film)
 
{{Infobox film
| name           = The Aristocrats
| image          = The Aristocrats.jpg
| caption        =
| director       = Penn Jillette Paul Provenza
| producer       = Paul Provenza Peter Adam Golden 
| writer         =
| starring       = Various
| music          = Gary Stockdale
| cinematography = Paul Provenza
| editing        = Emery Emery Paul Provenza
| studio         = THINKFilm
| distributor    = Lionsgate
| released       =  
| runtime        = 89 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $6,377,277
}} dirty joke of the same name.  It was conceived and produced by comedians Penn Jillette and Paul Provenza, edited by Emery Emery, and released to theaters by THINKFilm|TH!NKFilm.  The film is dedicated to Johnny Carson, as "The Aristocrats" was said to be his favorite joke. 

== The joke ==
 
 transgressive joke amongst comedians, in which the setup and punchline are almost always the same (or similar).  It is the jokes midsection – which may be as long as the one telling it prefers and is often completely improvised – that makes or breaks a particular rendition.
 shocking act they can possibly imagine. This often involves elements of incest, group sex, graphic violence, defecation, coprophilia, necrophilia, bestiality, child sexual abuse and various other taboo behaviors.

The joke ends with the agent, shocked but often impressed, asking "And what do you call the act?"  The punchline of the joke is then given: "The Aristocrats".

The joke, as first delivered in the film, contains the set-up line "What the heck do you call an act like that?" followed by the punchline "I call it The Aristocrats." In subsequent renditions of the joke, the agent asks, "What do you call your act?"

The film itself consists of interviews with various comedians and actors, usually in candid settings.  The interviewees engage both in telling their own versions of the joke, and in reminiscing about their experiences with it, the jokes place in comedy history, and even dissecting the logic behind the jokes appeal. A key aspect of the aristocrats joke is that it was never told to audiences as part of the comedians stand-up routine. Instead, it was an inside joke among comedians themselves, who used it as a tool to challenge each other as to who could tell the funniest and most outrageous rendition.

While most of the filmed versions of the joke follow the standard format of a raunchy description followed by the punchline of "the Aristocrats", some versions do vary the joke.  Two tellings of it, including that of comedian Wendy Liebman, invert the joke by describing an elegant and beautiful performance act which has been given a lewd and transgressive name.  Actor Taylor Negron tells his joke as a mixture of salacious sex acts and calmly delivered observations on life.

==Featured celebrities==
The following celebrities are featured in the film, telling the joke themselves and/or providing substantial commentary on its history:
 
*Chris Albrecht
*Jason Alexander
*Hank Azaria
*Shelley Berman Billy the Mime
*Lewis Black
*David Brenner
*Mario Cantone
*Drew Carey
*George Carlin Mark Cohen
*Carrot Top
*Billy Connolly
*Tim Conway
*Pat Cooper
*Wayne Cotter
*Andy Dick
*Frank DiGiacomo
*Phyllis Diller
*Susie Essman
*Carrie Fisher
*Joe Franklin
*Todd Glass
*Judy Gold
*Whoopi Goldberg
*Eddie Gorodetsky
*Gilbert Gottfried
*Dana Gould
*Allan Havey
*Eric Idle
*Dom Irrera
*Eddie Izzard
*Richard Jeni
*Jake Johannsen
*The Amazing Johnathan
*Alan Kirschenbaum
*Jay Kogen
*Sue Kolinsky
*Paul Krassner
*Cathy Ladman
*Lisa Lampanelli Richard Lewis
*Wendy Liebman
*Bill Maher
*Howie Mandel
*Merrill Markoe Jay Marshall
*Jackie Martling
*Chuck McCann
*Michael McKean
*Eric Mead Larry Miller
*Martin Mull
*Kevin Nealon
*Taylor Negron
*The Onion editorial staff
*Otto and George
*Rick Overton
*Gary Owens
*Trey Parker and Matt Stone
*The Passing Zone
*Penn & Teller
*Emo Philips
*Peter Pitofsky
*Kevin Pollak
*Paul Reiser 
*Andy Richter
*Don Rickles
*Chris Rock
*Gregg Rogell
*Jeffrey Ross
*Jon Ross (writer)
*Rita Rudner
*Bob Saget
*T. Sean Shannon
*Harry Shearer
*Sarah Silverman
*Bobby Slayton
*The Smothers Brothers
*Carrie Snow
*Doug Stanhope
*David Steinberg
*Jon Stewart
*Larry Storch
*Rip Taylor Dave Thomas
*Johnny Thompson
*Peter Tilden
*Bruce Vilanch
*Fred Willard
*Robin Williams
*Steven Wright
 

Many other comedians were filmed but not included due to time constraints. According to a letter from Penn Jillette to critic Roger Ebert, Buddy Hackett and Rodney Dangerfield were intended to be included, but died before they could be filmed. Jillette also indicated that, this being Johnny Carsons favorite joke, Carson was also invited to appear, but declined. 

==Joe Franklin controversy==
In the film,   allegation that Franklin had raped her during a phony rehearsal for the show. The New Yorker reported that Sarah Silvermans telling of the joke led Franklin, who is also featured in the film, to consider filing a defamation lawsuit against the comedian. 

On the April 20, 2011 episode of the WFMU show Seven Second Delay, Joe Franklin jokes that he had been so successful lately because he sued Sarah Silverman. 

==AMC banning==
The theater chain   2: Herbie Takes It Up the Ass."  

==Friars Club roast footage==
  roast of Aristocrats joke. According to the film, the telling was as much a cathartic experience for the audience as it was a shocking one, regardless of whether viewers were familiar with the joke or not. During his performance, Gottfried told the audience "They might have to clean this up for TV."

==Reception==

===Awards and nominations===
{| class="wikitable"
|-
!Year !! Award !! Organization !! Category !! Result
|- 2005
|U.S. Comedy Arts Festival Award
|U.S. Comedy Arts Festival Best Documentary Won      
|- Grand Jury Prize Sundance Film Festival Documentary
| style="background:#fdd;"| Nominated  
|- 2006
|Golden Trailer Award Golden Trailer Awards Best Documentary
| style="background:#fdd;"| Nominated  
|- Online Film OFCS Award Online Film Critics Society Best Documentary
| style="background:#fdd;"| Nominated  
|- Satellite Award International Press Academy Best Documentary DVD
| style="background:#fdd;"| Nominated   
|}

===Reviews=== rating average of 7.1 out of 10.  The sites general consensus is that the film is "Can a joke stand up to repeated tellings? Hilarious and revealing of the way comedy works, The Aristocrats demonstrates that its possible."    At Metacritic, which assigns a weighted mean rating out of 0–100 reviews from film critics, the film has a rating score of 72 based on 39 reviews, classified as a generally favorable reviewed film.   

==References==
 

==External links==
* 
* 
* 
* 

 
 
 
 
 
 
 
 
 