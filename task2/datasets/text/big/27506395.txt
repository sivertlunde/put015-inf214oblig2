A Stitch in Time (film)
{{Infobox Film | name           = A Stitch in Time image          = "A_Stitch_in_Time".jpg caption        = original UK 1-sheet poster director       = Robert Asher  producer  Hugh Stewart Earl St. John writer  Jack Davies Norman Wisdom Henry Blyth Eddie Leslie starring  Edward Chapman Jeanette Sterke Jerry Desmonde music  Philip Green     cinematography = Jack Asher editing        = Gerry Hambling
| studio        = Rank Organisation distributor    = J. Arthur Rank Film Distributors released       = 1963 country        = United Kingdom  runtime        = 89 min language       = English 
}} Edward Chapman, Peter Jones, Frank Williams. Johnny Briggs. 

==Plot==
Norman Pitkin is the apprentice to Mr Grimsdale an old fashioned butcher. When the store is raided by a young thug (Johnny Briggs), Mr Grimsdale (at Normans suggestion) puts his gold watch in his mouth for safe keeping. The result of which leads to Mr Grimsdale accidentally swallowing the watch and sent to hospital. Whilst visiting Mr Grimsdale, Norman (in his usual way) accidentally causes chaos around the hospital and meets a girl called Lindy who hasnt spoken since her parents were killed in an aeroplane accident. After Norman is unable to visit Lindy as he is banned from hospital he and Mr. Grimsdale join the St. John Ambulance Brigade which gives him the excuse to visit her, as usual chaos ensues, and in the end Lindy visits him at a charity ball where the St John Ambulance Brigade Band are performing, the ball descends into the inevitable chaos, caused entirely by Norman, but Norman redeems himself (and the reputation of the Brigade) as he addresses those attending the ball and everyone donates money for the charity.

==Release==
A Stitch in Time represents Wisdoms most commercially successful title.  The film was rereleased in 1984 in Chennai India. The film was a smash hit and ran for many weeks at the old Alankar Theatre (now demolished).

==Filming==
The film was shot almost entirely at Pinewood Studios in Buckinghamshire.  On-location filming was kept to a minimum.

==Cast==
*Norman Wisdom as Norman Pitkin Edward Chapman as Mr. Grimsdale
*Jeanette Sterke as Nurse Haskell
*Jerry Desmonde as Sir Hector
*Jill Melford as Lady Brinkley
*Glyn Houston as Cpl. Welsh of the St. John Ambulance Brigade
*Hazel Hughes as Matron
*Patsy Rowlands as Amy Peter Jones as Divisional Officer Russell of the St. John Ambulance Brigade
*Ernest Clark as Prof. Crankshaw
*Lucy Appleby as Lindy
*Vera Day as Betty Frank Williams as Driver Nuttall of the St. John Ambulance Brigade
*Penny Morrell as Nurse Rudkin
*Patrick Cargill as Dr. Meadows Francis Matthews as Benson
*John Blythe as Dale, Press Photographer
*Pamela Conway as Patient Danny Green as Ticehurst Johnny Briggs as Armed Robber
*Michael Goodliffe as Doctor on the Childrens Ward (uncredited)
* Tony Thawnton as St John Ambulance Driver in last few minutes of movie(uncredited)

==Critical reception==
*The Radio Times gave the film two out of five stars, writing, "this was (Wisdoms) final film in black and white and also his last big starring success at the box office, for he belonged to a more innocent age. The script sticks closely to the winning Wisdom formula as he knots his cap in confused shyness in his attempts to declare his love for a pretty nurse. Stalwart stooges Edward Chapman (Mr Grimsdale, of course) and Jerry Desmonde prove once more that straight men can often be much funnier than the comics."  
*Sky Movies gave the film three out of five stars, noting the film "has just enough inspired tomfoolery - a madcap race on casualty trolleys down the corridors of a hospital; a hectic ride for a bandaged Norman on top of an ambulance; Norman messing up a St Johns Ambulance Brigade concert - to ensure a decent quota of laughs. In his silly stunts, he is forever the naughty boy having the time of his life doing what he shouldnt."  
*TV Guide gave the film three out of five stars, and noted, "a charming and sentimental piece of characterization from Wisdom."  

==External links==
*  

==References==
 

 
 
 
 
 
 
 