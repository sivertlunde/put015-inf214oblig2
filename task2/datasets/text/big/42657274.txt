White Shadow (film)
{{Infobox film
| name           = White Shadow
| image          = White Shadow poster.jpg
| alt            = 
| caption        = Film poster
| director       = Noaz Deshe
| producer       = Noaz Deshe Ginevra Elkann Babak Jalali Matthias Luthardt Francesco Melzi dEril Alexander Wadouh
| writer         = Noaz Deshe James Masson
| starring       = Hamisi Bazili James Gayo Glory Mbayuwayu Salum Abdallah
| music          = Noaz Deshe James Masson	
| cinematography = Noaz Deshe Armin Dierolf
| editing        = Noaz Deshe Xavier Box Robin Hill Nico Leunen	
| studio         = Asmara Films Shadoworks Mocajo Film production
| distributor    = 
| released       =  
| runtime        = 115 minutes
| country        = Italy Germany Tanzania Swahili
}} Tanzanian drama film written, produced and directed by Noaz Deshe.   The film premiered in Critics’ Week selection at the 70th Venice International Film Festival on September 2, 2013. It won the Lion of the future award at the festival.    	

The film later premiered in-competition in the World Cinema Dramatic Competition at 2014 Sundance Film Festival on January 17, 2014.   The film also screened at 2014 San Francisco International Film Festival on May 4, 2014.   Ryan Gosling along with Matteo Ceccarini and Eva Riccobono served as the executive producers of the film.  

==Plot==
Alias, a young Albinism|Albino, is on the run from the local doctors, who are hunting Albinos to use their body parts for potions.

==Cast==
*Hamisi Bazili as Alias
*James Gayo as Kosmos
*Glory Mbayuwayu as Antoinette
*Salum Abdallah as Salum
*Riziki Ally as Mother
*John S. Mwakipunda as Anulla
*Tito D. Ntanga	as Father
*James P. Salala as Adin

==Reception==
White Shadow received mostly positive reviews from critics. Guy Lodge of Variety (magazine)|Variety, said in his review that "Noaz Deshe makes a staggering debut with this drama about the African albino multi trade."  Boyd van Hoeij in his review for The Hollywood Reporter said that "This harrowing account of a young albinos fight for survival in Tanzania is too long but nonetheless often gripping."  Jessica Kiang of Indiewire grade the film B+ and praised the film by saying that "We have to admit, it first took a lot of our patience, and then all of our nerve, to make it through to the end, but that simply makes it a film that is exactly as upsetting as its subject matter warrants." 

==Accolades==
  
 
{| class="wikitable sortable"
|-
! Year
! Award
! Category
! Recipient
! Result
|-
| rowspan="3" | 2014 Sundance Film Festival
| World Cinema Grand Jury Prize: Dramatic
| Noaz Deshe 
|  
|-
| Venice Film Festival
| Lion of the future
| Noaz Deshe 
|   
|-
| San Francisco International Film Festival
| New Directors Prize - Special mention
| Noaz Deshe 
|  
|}
 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 