Lower Learning
{{Infobox film
| name = Lower Learning
| image = Lower Learning - dvd cover.jpg
| image_size = 
| alt = 
| caption = DVD cover
| director = Mark Lafferty
| producer = Matthew Leutwyler Sim Sarna
| writer = Shahin Chandrasoma Mark Lafferty
| starring = Jason Biggs Eva Longoria Rob Corddry
| music = Ryan Shore
| cinematography = David Robert Jones
| editing = Shawna Callahan
| studio = Ambush Entertainment Starz Media
| distributor = Anchor Bay Entertainment
| released = October 10, 2008
| runtime = 97 minutes
| country = United States
| language = English
| budget = $2 million
| gross = $2,804
}} Ryan Newman, Monica Potter, and Andy Pessoa. It was directed by Mark Lafferty and written by Lafferty and Shahin Chandrasoma. The music was composed by Ryan Shore.

==Plot==
Geraldine Ferraro Elementary, one of the worst schools in the state, is in danger of being closed. The school suffers from low test results, drunken teachers, and a corrupt Principal. The Vice Principal, Tom Willoman (Jason Biggs), decides to try to save the school. When he finds out that the school inspector is a childhood friend, he recruits her to help him save the school by rallying the teachers and students against the principal.

==Cast==
* Jason Biggs as Vice-Principal Tom Willoman
* Eva Longoria as Rebecca Seabrook
* Rob Corddry as Principal Harper Billings
* Monica Potter as Laura Buchwald
* Will Sasso as Jesse Buchwald
* Nat Faxon as Turner Abernathy
* Jill Latiano as Nurse Gretchyn
* Kyle Gass as Decatur Doublewide
* Hayes MacArthur as Digdug OShaughnessy
* Zachary Gordon as Frankie Fowler
* Ed Helms as Maurice Bunting
* Patricia Belcher as Colette Bamboo
* Jack Donner as Old Curt
* Sandy Martin as Olympia Parpadelle
* Erik Palladino as Smooth Bob Willoman Matt Walsh as Mr. Conroy
* Miranda Bailey as Melody Ryan Newman as Carlotta
* Andy Pessoa as Walter

==Release==
Lower Learning was released theatrically at a single theater showing on October 10, 2008 and made $2,804 in domestic grosses. 

==Home media==
The film was released on DVD on December 2, 2008.

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 

 