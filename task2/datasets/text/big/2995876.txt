Pet Sematary Two
{{Infobox film
 | name = Pet Sematary Two
 | image = Pet sematary ii ver2.jpg 
 | caption = Theatrical Release Poster
 | director = Mary Lambert
 | producer = Ralph Singleton
 | writer = Richard Outten
 | starring = Edward Furlong Anthony Edwards Clancy Brown Jared Rushton Darlanne Fluegel Jason McGuire Lisa Waltz 
 | music = Mark Governor
 | cinematography = Russell Carpenter
 | editing = Tom Finan
 | distributor = Paramount Pictures 
 | released =  
 | runtime = 100 minutes
 | country = United States 
 | language = English
 | budget = $8 million  
 | gross = $17.1 million 
}} Pet Sematary. The film stars Edward Furlong, Anthony Edwards and Clancy Brown.

==Plot==
Following the accidental death of his mother, Renee, during production of her latest film, thirteen-year-old Jeff Matthews and his father, Chase, a veterinarian, move to Ludlow, Maine. Jeff learns about the Creed family and the cursed Indian burial ground. His friend Drews dog, Zowie, is shot by Drews stepfather, Gus Gilbert, the town sheriff. Jeff and Drew bury Zowie at the burial ground in an attempt to bring him back to life. The dog returns from the dead, but is unusually fierce, and his eyes have an unnatural glow to them even in daylight. Gus grounds Drew because he thinks he was lying when he said he only missed school because he was burying Zowie. Zowie is treated for his gunshot wound by Chase, who finds that the wound is not healing and Zowie has no heartbeat. Chase sends a blood sample to a lab and learns that Zowies cells have completely deteriorated and are no different from those of a dead canine. 

Jeff and Drew go to the pet cemetery on Halloween for a night of horror stories about the Creed murders when Gus comes looking for Drew and punches him. Before Gus can hit his stepson with one of the gravemarkers, Zowie rushes out of the shadows and kills him with a bite to the jugular. The boys bury Gus in the burial ground. Gus returns to life, but now exhibits stiff movements and treats Drew more fairly, but remains mostly silent and in a zombie-like state. Soon, he becomes more crude and sadistic; he rapes Drews mother and brutally skins his pet rabbits for supper.

Zowie breaks out of the veterinary clinic and kills three cats before entering Chases home and attacking him. Chase survives with an injured arm, but is shaken. The next day, Jeff encounters the school bully, Clyde Parker in a ditch. Clyde is about to cut off Jeffs nose with the spokes of his own bike when Gus arrives. Gus orders Jeff to go home, then kills Clyde with his motorcycle. Drew witnesses the killing. 

Gus traps Drew inside his house with Zowie, who has gone savage. Drew escapes through a window and gets in the car of his arriving mother. Gus chases them down the highway with his police car, killing them by forcing them to collide with a truck. Gus returns to Clydes corpse with a police shovel and body bag, saying, "Im takin you up the hill, Clyde, buddy. Thats the way the Indians did it." 

That night, Jeff becomes obsessed with the burial ground`s power and decides to reanimate his mother. He has Gus exhume her corpse, and meets him at the burial ground. His father is told that his wife has been removed from her grave by Gus. Chase rushes to Guss house and is attacked by Zowie but manages to shoot and kill the undead dog. He enters Guss house but finds the undead man waiting for him. Shooting Gus in his chest has little effect on him. Just as Gus is about to kill Chase with an electric drill, Chase retrieves his gun and shoots Gus through the head, killing him. 

Jeffs mother Renee has come back to life. She stabs and kills Chases housekeeper, Marjorie Hargrove, in the attic. Jeff hears Marjorie`s screams and walks calmly up to the attic, meeting the undead Renee up there. Chase arrives home and finds Marjories body as well as his son embracing his undead wife. He is unmoved by Renee and tries to warn Jeff to get away from her. Renee then asks Jeff to go downstairs so she and Chase can "talk". An undead Clyde arrives armed with an ax and fights Jeff. Chase is knocked out, and Renee locks everyone in the attic and sets the place on fire. 

Before Clyde can kill him with an ice-skate, Jeff kills Clyde with a severed insulated cable, then uses his axe to chop down the attic door and grabs his unconscious father. Renee tries convincing Jeff to stay by muttering, "I love you", but Jeff ignores her cries and pleas then leaves his mother to die in the flames. As Renee burns and melts, she screams out "Dead is better!" The next morning, Jeff and Chase leave Ludlow to start a new life in Los Angeles.

==Cast==
*Edward Furlong as Jeff Matthews
*Anthony Edwards as Chase Matthews
*Clancy Brown as Gus Gilbert
*Jared Rushton as Clyde Parker
*Jason McGuire as Drew Gilbert
*Darlanne Fluegel as Renee Hallow/Matthews
*Lisa Waltz as Amanda Gilbert
*Sarah Trigger as Marjorie Hargrove

==Release==
The film debuted at number three.   Paramount Home Video released it on VHS in April 1993,  and on DVD in September 2001. 

==Reception== Kevin Thomas Tales from the Crypt" episode and criticized the script as a rehash of the original.   Jay Carr of The Boston Globe called it "better entertainment than the first Pet Sematary" but more of a remake than a sequel.   Patrick Naugle of DVD Verdict wrote, "Everything about Pet Sematary Two stinks like the dead." 

== References ==
 

==External links==
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 