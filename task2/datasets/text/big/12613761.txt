Arizona (1940 film)
{{Infobox Film
| name           = Arizona
| image          = Arizona 1940.jpg
| caption        = Theatrical release poster
| director       = Wesley Ruggles
| producer       = Wesley Ruggles
| based on       =  
| writer         = Claude Binyon
| starring       = Jean Arthur William Holden Warren William
| music          = Stephen Foster Victor Young
| cinematography = Fayte Browne Harry Hallenberger Otto Meyer 
| distributor    = Columbia Pictures
| released       =  
| runtime        = 125 minutes
| country        = United States
| language       = English
}}
 Western film starring Jean Arthur, William Holden and Warren William. It was directed by Wesley Ruggles.
 Robert Peterson were considered for the Academy Award for Best Art Direction, Black-and-White.   

==Plot==  Paul Harvey). He, however, is determined to see California, but promises to return when his wanderlust is satisfied. 
 Union garrison pulls out, leaving them without protection against the Indians. Carteret pretends to be Phoebes friend, but coerces Ward into making him a secret partnership|partner. 
 Confederates gain the (temporary) allegiance of the community by sending some troops, but they are soon recalled east. Union troops of the California Column, with Peter among them as a sergeant, return in April 1862 just as Tucsons situation becomes desperate. He helps Phoebe secure a lucrative army freight contract, but Carteret has Ward slander her to the Union commander, claiming that she supplied ammunition for the departed Confederates. Peter and Phoebe get the truth out of Ward at gunpoint and regain the contract. Soon after, Peters enlistment expires.
 Mexican bandits. Carteret then offers to make her a loan, with her business and land as security. She accepts. Six months later, Carteret tells Phoebe that her loan comes due the next day. 

However, Peter is half a day away with their herd. Carteret gets the Indians to attack but Peter and his men are able to fight them off. Peter gets a confession from one of Carterets men, but Carteret kills the henchman after he shoots Ward in the back to rid himself of the last incriminating loose end. The entire town celebrates as Phoebe and Peter get married. Then he has her wait for him in Solomons store while he goes to settle accounts with Carteret. Shots are heard, then a relieved Phoebe takes her slightly wounded new husband home.

==Cast==
* Jean Arthur as Phoebe Titus
* William Holden as Peter Muncie
* Warren William as Jefferson Carteret
* Porter Hall as Lazarus Ward
* Edgar Buchanan as Judge Bogardus Paul Harvey as Solomon Warner
* George Chandler as Haley
* Byron Foulger as Pete Kitchen
* Regis Toomey as Grant Oury
* Paul Lopez as Estevan Ochoa
* Colin Tapley as Bart Massey
* Uvaldo Varela as Hilario Callego
* Earl Crawford as Joe Briggs
* Griff Barnett as Sam Hughes
* Ludwig Hardt as Meyer

==Set==
Arizona was filmed on a set located just outside the city of Tucson in the Sonoran Desert. After filming, it lay dormant for a few years during World War II, but was revived and made into a full studio after the war. The studio continues today as Old Tucson Studios.

==References==
 

==External links==
*  
*  
*  
*  
*  , film article by Jay Steinberg at Turner Classic Movies

 

 
 
 
 
 
 
 
 
 
 