The Crazy Clock Maker
 
{{Infobox film
| name           = The Crazy Clock Maker 
| image          = 
| caption        = 
| director       = Jerold T. Hevener
| producer       = 
| writer         = 
| starring       = Billy Bowers
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = United States  English intertitles
| budget         = 
}}
 silent comedy film starring Billy Bowers and featuring Oliver Hardy in a supporting role.

==Cast==
* Billy Bowers		
* Myra Brooks
* Ray Ford	
* Clay Grant
* Oliver Hardy (as Babe Hardy)
* Betty Holton		
* Beatrice Miller		
* Mabel Paige
* C.W. Ritchie		
* Walter Schimpf		
* Bill Watson		
* Hod Weston

==See also==
* List of American films of 1915
* Oliver Hardy filmography

==External links==
* 

 
 
 
 
 
 
 
 
 

 