The Disappeared (2012 film)
{{Infobox film
| name = The Disappeared image = Billy Campbell and Ryan Doucette in The Disappared.jpg
| director = Shandi Mitchell
| producer = Gilles Belanger Ralph Holt Walter Forsyth Karen Franklin
| screenplay = Shandi Mitchell Brian Downey   Gary Levert   Ryan Doucette   Neil Matheson
| studio = Two Dories Film Inc.
| country = Canada
| language = English
}} North Atlantic, Brian Downey, Gary Levert, Ryan Doucette, and Neil Matheson.

The film premiered at the Atlantic Film Festival September 14, 2012.

==Plot==
After their fishing boat sinks, six men in two small dories find themselves stranded in the middle of the North Atlantic Ocean. They have little food and fresh water. The film follows their physical and mental fatigue as the days pass and they try to get home again.

==Cast==

*Billy Campbell as Mannie, First mate
*Shawn Doyle as Pete, Harpoon striker Brian Downey as Captain Gerald
*Gary Levert as Merv, Bosun
*Ryan Doucette as Gib, Cook and Spotter
*Neil Matheson as Dickie, Deck hand

==Production==
 sea captain who had once been lost at sea after his ship went down.  Her resulting original script explores both physical and mental struggles of being lost at sea. 

The film received support from Telefilm Canada, Film Nova Scotia, the Canada Council for the Arts, and the Nova Scotia Department of Communities, Culture and Heritage.  The film was acquired by The Movie Network and Movie Central.
 zodiacs to hair and makeup artists as needed.  

Actor Billy Campbell told media he found the shoot wasnt too taxing despite being in a row-boat for 15 days. 

==Awards==
 On February 23, 2013, Shawn Doyle won Outstanding Performance–male for his performance in The Disappeared. at the 11th Annual ACTRA Awards   in Toronto, February 23, 2013. The Disappeared was also nominated for two Canadian Screen Awards. Zander Rosborough and Allan Scarth were nominated for achievement in overall sound and Scarth, Bob Melanson, Rosborough and Cory Tetford were nominated for achievement in sound editing.

==Release==
 Corus Entertainment’s Movie Central, The Disappeared received financial support from Telefilm Canada, Film Nova Scotia and the Canadian and Nova Scotia tax credit programs, and was developed with added support from the Canada Council for the Arts and the Nova Scotia Department of Communities, Culture and Heritage.

The film premiered at the Atlantic Film Festival to sold out audiences as the Atlantic Gala on September 14, 2012 at the Oxford Theatre in Halifax Regional Municipality|Halifax.  The entire cast was reunited for the premiere. 

The Disappeared is now available to rent or buy on iTunes and Vimeo On Demand. It is also also available on DVD & BLU-RAY at and amazon.com, and on DVD at amazon.ca.

{| class="wikitable"
|-
! Film Festival !! City !! Date of screening !! Theatre
|-
| Atlantic Film Festival || Halifax Regional Municipality|Halifax, Nova Scotia || September 14 (World Premiere) and September 15, 2012 || Oxford Theatre 
|-
| Cinéfest || Sudbury, Ontario|| September 18, 2012 || Silvercity Sudbury Cinemas
|-
| Calgary International Film Festival || Calgary, Alberta || September 25, 2012 || Eau Claire Cineplex
|-
| Edmonton International Film Festival || Edmonton, Alberta || October 4, 2012 || Empire City Centre 9
|-
| Vancouver International Film Festival || Vancouver, British Columbia || October 10 and October 11, 2012 || Empire Granville 4
|-
| St. Johns International Womens Film Festival || St. Johns, Newfoundland and Labrador || October 19, 2012 || LSPU Hall
|}

==References==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 
 
 