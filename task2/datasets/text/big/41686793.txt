Fever (2014 film)
 
{{Infobox film
| name           = Fever
| image          = 
| caption        = 
| director       = Elfi Mikesch
| producer       = 
| writer         = Elfi Mikesch
| starring       = Eva Mattes Martin Wuttke
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 80 minutes
| country        = Luxembourg Austria
| language       = German French 
| budget         = 
}}

Fever ( ) is a 2014 drama film directed by Elfi Mikesch and produced by Amour Fou. The film was shot in Luxembourg, Austria, Italy and Serbia and had its world premiere in the Panorama section of the 64th Berlin International Film Festival.       The film is set in Austria in the early 1950s.   

==Cast==
* Eva Mattes
* Martin Wuttke
* Carolina Cardoso
* Nicole Max
* Sascha Ley
* Nilton Martins as Berber Tilelli

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 