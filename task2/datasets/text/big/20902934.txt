Anhonee (1973 film)
 
{{Infobox film
| name           = Anhonee
| image          = Anhonee.jpg
| image_size     = 
| caption        = Theatrical poster
| director       = Ravi Tandon
| producer       = Ravi Tandon   G.M. Khwaja   Gullu Kochar   Jagdish C. Sharma  
| writer         = Dr. Balkrishna (dialogue), K.K. Shukla (screenplay)
| narrator       = 
| starring       = Sanjeev Kumar and Leena Chandavarkar
| music          = Laxmikant Pyarelal
| cinematography = S. Ramachandra
| editing        = Waman B. Bhosle  Gurudutt Shirali 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
Anhonee is a 1973 Bollywood suspense drama film directed by Ravi Tandon. The film stars Sanjeev Kumar, Leena Chandavarkar, Satyen Kappoo, Asrani, Manmohan & Bindu in positive roles and Kamini Kaushal in a negative role. Laxmikant Pyarelal have provided music for the film.

==Cast==
*Sanjeev Kumar ...  Sunil
*Leena Chandavarkar ...  Dr. Rekha
*Padma Khanna ...  Roopa
*Kamini Kaushal ...  Mrs. Rai Bahadur Singh
*Manmohan ...  Dr. Mathur
*Asrani ...  Gangaram
*Paintal ...  Dr. Tripathi Bindu ...  Rita
*Satyendra Kapoor ...  Manager
*Jankidas ...  Girdhari Lal
*Captain Manmohan
*M.B. Shetty ...  Henchman
*S.P. Dutta
*Ketty Irani
*Ranveer Raj ...  Dr. Shukla

Plot

After the tragic death of her dad, Dr. Rekha continues to live with her step-mother in Bombay, where she runs a Mental Institute. While traveling by train one night, they are accosted by a knife wielding escaped mental patient, Sunil, whose aggression is calmed by Rekha. She gets him admitted into her hospital where he is put under observation and treatment at the hands of Dr. Mathur and herself. He soon starts responding positively under their care, and is on his way to recovery. Once he saves Rekha from being molested in a parking lot, and this sparks romance between the two. All is set for the two to get engaged at a lavish party. It is at this party that Rekha will find out that Sunil is not who he claims to be, and subsequently Sunil will also find out that Rekha has a dark secret in her recent past - a secret for which she is being blackmailed by a man named Tiger - and a secret that may well expose her as a murderess.

==Music==
The songs of the film are composed by Laxmikant Pyarelalon lyrics by Verma Malik
* "Hungama Ho Gaya" Singer: Asha Bhosale
* "Buddha Pad Gaya Palle" Singer: Lata Mangeshkar
* "Balma Hamar Motorcar Leke Aayore" Singer: Asha Bhosale
* "Main To Ek Pagal" Singers: Kishore Kumar and Asha Bhosale

==Awards and Nominations==
 
|-
| 1973
| Asrani
| Shama Sushama Award for Best Comedian
|  
|}
==In popular culture==
In 2014 film, Queen (film)|Queen, starring Kangana Ranaut, the hit cabaret number Maine hothon se lagai toh hungama ho gaya sung by Asha Bhosle was remixed by Amit Trivedi with additional vocals by  Arijit Singh. 
== References ==
 
==External links==
*  
*  

 
 
 
 
 
 