Don't Say a Word
 
 
{{Infobox film name = Dont Say a Word image = Dont Say a Word movie.jpg caption = Theatrical release poster director = Gary Fleder producer = Arnon Milchan Arnold Kopelson Anne Kopelson screenplay = Anthony Peckham Patrick Smith Kelly based on =   starring = Michael Douglas Sean Bean Brittany Murphy Guy Torry Jennifer Esposito Famke Janssen Oliver Platt music = Mark Isham cinematography = Amir Mokri production design = Nelson Coates editing = Armen Minasian William Steinkamp studio = New Regency Further Films Epsilon Motion Pictures   distributor = 20th Century Fox released =   runtime = 113 minutes country = United States Australia language = English Italian budget = $50 million gross = $100,020,092
}} psychological Thriller thriller film novel of the same title by Andrew Klavan. Dont Say a Word was directed by Gary Fleder and written by Anthony Peckham and Patrick Smith Kelly. 

==Plot==
In 1991, a gang of thieves steal a rare $10-million gem, but, in the process, two of the gang double-cross their leader, Patrick Koster (Sean Bean) and take off with the precious stone.

Ten years later, on the day before Thanksgiving, prominent private practice Manhattan child Psychiatry|psychiatrist, Dr. Nathan R. Conrad (Michael Douglas), is invited by his friend and former colleague, Dr. Louis Sachs (Oliver Platt), to examine a disturbed young lady named Elisabeth Burrows (Brittany Murphy) at the state sanatorium.

Having been released from prison on November 4, Patrick and the remaining gang members break into an apartment which overlooks Nathans apartment, where he lives with his wife Aggie (Famke Janssen) and daughter Jessie (Skye McCole Bartusiak). That evening, Patrick kidnaps the psychiatrists daughter as a means of forcing him to acquire a six-digit number from Elisabeths memory.

As Nathan visits Elisabeth, she is reluctant at first, but he gains her trust later—especially when he reveals that his daughter has been kidnapped and would be killed if he doesnt get the number they want. Dr. Sachs admits to Nathan that the gang who kidnapped Jessie also kidnapped his girlfriend to force him to acquire the number from Elisabeth. Sachs is then visited by Detective Sandra Cassidy (Jennifer Esposito) who reveals to him that his girlfriend has been found dead.

Meanwhile, Aggie hears Jessies voice and realizes the kidnappers reside in the apartment nearby. The kidnappers send one of them to kill Aggie while the others escape with Jessie, but Aggie sets an ambush and kills him.
 Hart Island and that her doll is placed beside him in the coffin. She explains that she had stowed away on a boat that was taking her fathers coffin for burial in Potters field on Hart Island, where the gravediggers put the doll, named Mischka, inside.

Nathan and Elisabeth steal a boat to reach Hart Island. The gang members track them down and demand that Nathan give them the number they want. Elisabeth reveals the number and Patrick orders his companion to exhume her fathers coffin. He finds the doll and the gem hidden inside it. He then decides to kill Nathan and Elisabeth, but Detective Cassidy arrives before he can shoot them. Patricks companion is shot by Cassidy, but Patrick manages to wound her. Taking advantage of the confusion, Nathan takes the gem from Patrick and throws it to a nearby excavation machine. Patrick goes to recover the gem, but Nathan triggers the mechanism which covers Patrick with earth, burying him alive. Nathan is then reunited with his wife and daughter, and it is implied that Elisabeth goes on to live with the Conrads.

==Cast==
* Michael Douglas as Dr. Nathan R. Conrad
* Sean Bean as Patrick Koster
* Brittany Murphy as Elisabeth Burrows
* Guy Torry as Dolen
* Jennifer Esposito as Det. Sandra Cassidy
* Famke Janssen as Agatha "Aggie" Conrad
* Oliver Platt as Dr. Louis Sachs
* Skye McCole Bartusiak as Jessie Conrad
* Shawn Doyle as Russel Maddox
* Victor Argo as Sydney Simon
* Conrad Goode as Max Dunlevy
* Paul Schulze as Jake
* Lance Reddick as Arnie
* Aidan Devine as Leon Edward Croft
* Alex Campbell as Jonathan
* Arlene Duncan as Aide
* Judy Sinclair as Zelda Sinclair
* Larry Block as Doorman
* David Warshofsky as Ryan
* Darren Frost as Janitor
* Philip Williams as Large Cop
* Louis Vanaria as Cop at Scene
* Daniel Kash as Detective Garcia
* Lucie Laurier as Vanessa
* Isabella Fink as 8 Year Old Elisabeth
* Ray Iannicelli as Man at Marina
* Colm Magner as Cop #1
* Cyrus Farmer as Officer #1
* Martin Roach as Transit Cop
* Patricia Mauceri as Sofia

==Soundtrack==
The films musical score was composed by Mark Isham. The soundtrack contains eight songs from various scenes, including the Heist, the Kidnapping and the horrific events at the Subway. 

Some of the music in the film includes the following songs and performers:
#"Funky Cold Medina" by Tone Loc
#"5 by Steve" by Steve Weisberg
#"Fee Fie Foo" by Louis Prima
#"Promises" by India.Arie Pride and Joy" by Marvin Gaye Dream a Little, Dream of Me" by Ella Fitzgerald
#"Pink Toenails" by Martie Maguire and Laura Lynch, sung by Skye McCole Bartusiak

==Reception==
===Critical response===
 
Dont Say a Word received poor reviews from critics. Rotten Tomatoes gives the film a score of 24% based on 113 reviews. {{cite web
| title = Dont Say a Word (2001)
| url = http://www.rottentomatoes.com/m/dont_say_a_word/
| work = Rotten Tomatoes
| publisher = Flixster
}}  Metacritic gives the film a generally unfavorable review with a score of 38% based on 32 reviews. 

===Box office===
Despite poor reception by critics, the film was a box office success. It earned over $100 million worldwide against a budget of $50 million. {{cite web
| title = Dont Say a Word (2001)
| url = http://www.boxofficemojo.com/movies/?id=dontsayaword.htm
| work = Box Office Mojo
| publisher = Amazon.com
}} 

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 