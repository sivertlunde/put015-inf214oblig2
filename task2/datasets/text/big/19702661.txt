Fairy Tales (film)
 
{{Infobox film
| image = Fairytales1978.png
| caption = Theatrical poster
| name = Fairy Tales
| director = Harry Hurwitz
| producer = Charles Band
| writer =
| release = 1978
| runtime = 76 min (82 min directors cut)
| country = United States
| language = English
}}
Fairy Tales, released in the UK as Adult Fairy Tales, is a 1978 sex comedy directed by Harry Hurwitz, the plot of which revolves around the stereotypical Fairy Tale.

==Synopsis==
On his twenty-first birthday, the Prince of the land goes searching for the one woman that can get his libido moving again.

==Cast==
* Don Sparks as Prince
* Sy Richardson as Sirus
* Irwin Corey as Dr. Eyes (as Prof. Irwin Corey)
* Robert Harris as Dr. Ears
* Simmy Bow as Dr. Mustachio
* Robert Staats as Doorman / Tommy Tucker
* Martha Reeves as Aunt La Voh
* Anne Gaybis as Snow White
* Brenda Fogarty as Gussie Gander
* Frank Ray Perilli as Baron
* Angelo Rossitto as Otto
* Bob Leslie as Old King Cole
* Jeff Doucette as Jack
* Lindsay Freeman as Jill
* Nai Bonet as Scheherazade
* Angela Aames as Little Bo Peep
* Linnea Quigley as Sleeping Beauty

==External links==
*  

 

 
 
 
 
 
 


 