Main Street to Broadway
{{infobox film
| name           = Main Street to Broadway
| image          =
| imagesize      =
| caption        = James Anderson (assistant)
| producer       = Lester Cowan
| writer         = Samson Raphaelson(writer) Robert E. Sherwood(story) Mary Murphy Agnes Moorehead
| music          = Ann Ronell
| cinematography = James Wong Howe
| editing        = Gene Fowler, Jr.
| distributor    = MGM
| released       = October 13, 1953
| runtime        =
| country        = United States
| language       = English
| budget = $1,350,000  .  gross = $444,000 
}} Tom Morton, Mary Murphy, as a young lady from Indiana, and Herb Shriner, TV and radio humorist, in a rare acting role as a hardware store owner.  

Tallulah Bankhead is featured in a parody sequence of herself. The list of Broadway luminaries also playing themselves, in smaller cameos, includes Ethel Barrymore, Lionel Barrymore (in his last film), Shirley Booth, Louis Calhern, Faye Emerson, Rex Harrison, Helen Hayes, Mary Martin, Lilli Palmer, John Van Druten and Cornel Wilde. Included is New York baseball manager Leo Durocher. Many others are unidentified, such as Vivian Blaine, glimpsed in a theater lobby. 

In one scene, Richard Rodgers and Oscar Hammerstein II create a new song, "Theres Music in You", then perform it for their friends, with Rodgers at the piano and Hammerstein singing the vocals.  Mary Martin is later seen rehearsing the song for director Joshua Logan.  

The black-and-white film, which has a running time of 97 minutes, was directed by Tay Garnett, screenplay by Samson Raphaelson, based on a story Robert E. Sherwood, and photographed by James Wong Howe. Sequences were filmed in New York, with shots at the Martin Beck and old Empire theaters. Others as story characters include Gertrude Berg, as a landlady, Agnes Moorehead, Rosemary de Camp, Arthur Shields, and, in a fantasy sequence, Florence Bates, Madge Kennedy, Carl Benton Reid, Frank Ferguson, and Robert Bray.

==Reception==
According to MGM records the film earned $416,000 in the US and Canada and only $28,000 elsewhere. 
==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 