Marie's Soldier
{{Infobox film
| name = Maries Soldier
| image =
| image_size =
| caption =
| director = Erich Schönfelder
| producer = Richard Eichberg
| writer =  Leo Birinsky 
| starring = Xenia Desni   Harry Liedtke   Grit Haid
| music = Leo Ascher 
| cinematography = Theodor Sparkuhl   
| editing =     
| studio = Richard Eichberg-Film  UFA
| released = 14 February 1927
| runtime = 
| country = Germany German intertitles
| budget =
| gross =
}} Kurt Richter.

==Cast==
* Xenia Desni as Marie 
* Harry Liedtke as Reichsgraf von Kerzendorf 
* Grit Haid as Marianne 
* Hilde Maroff as Mariette 
* Margarete Kupfer as Witwe 
* Julia Serda as Fürstin Wendisch-Bommerdorf 
* Sig Arno as Maries Vater 
* Hans Albers as Wonneberger, Bursche 
* Kurt Gerron as Wachmeister Knöppke 
* Emmy Wyda as Frau Wachmeister 
* Else Reval as Frau Wachtmeisters Schwester 
* Hermann Picha
* Teddy Bill

== References ==
 

==Bibliography==
* Grange, William. Cultural Chronicle of the Weimar Republic. Scarecrow Press, 2008.

== External links ==
*  

 
 
 
 
 
 
 
 

 