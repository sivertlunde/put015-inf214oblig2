Lilya 4-ever
 
{{Infobox film
| name = Lilja 4-ever
| image = Lilya 4-ever poster.jpg
| image_size = 220px
| border = yes
| alt = 
| caption = Theatrical release poster
| director = Lukas Moodysson
| producer = Lars Jönsson
| writer = Lukas Moodysson
| starring = Oksana Akinshina Artyom Bogucharsky Lyubov Agapova Liliya Shinkaryova Nathan Larson
| cinematography = Ulf Brantås
| editing = Michal Leszczylowski Oleg Morgunov Bernhard Winkler Zentropa Entertainments Svenska Filminstitutet Det Danske Filminstitut
| distributor = Sonet Film   Newmarket Films  
| released =  
| runtime = 109 minutes  
| country = Sweden Denmark
| language = Russian Swedish English Polish SEK (United States dollar|USD$4.6 million)
| gross = $184,023   
}}
Lilja 4-ever is a 2002 Swedish-Danish drama film directed by Lukas Moodysson. Lilja 4-ever is an unremittingly brutal and realistic story of the downward spiral of Lilja, played by Oksana Akinshina, a girl in the former Soviet Union whose mother abandons her to move to the United States. The story is loosely based on the true case of Danguolė Rasalaitė,  and examines the issue of human trafficking and sexual slavery.
 Best Film, Best Actress at the European Film Awards.

==Plot==
The film starts with a figure running desperately towards a motorway bridge, with a factory belching smoke in the background, to a soundtrack of Mein Herz brennt by Rammstein. When the figure turns around the film introduces the audience to Lilja, who has recently been brutally beaten. The film reveals her past.

Lilja lives a fairly bleak life with her mother in a run down apartment block in a squalid, poor town in an unnamed former republic of the Soviet Union (principal filming took place in Paldiski, Estonia). For all intents and purposes she is a normal teenage girl (albeit an impoverished one). Liljas mother tells her they are emigrating to the United States with her new boyfriend, but at the last minute Lilja is left behind, in the care of her aunt. A forced move into a squalid flat (while the Aunt moves herself into the larger, nicer flat that Lilja and her mother had lived in) is only the beginning, and a succession of miseries are heaped upon Lilja. Liljas best friend encourages her to join her in prostituting herself for extra cash, though Lilja decides not to follow through. However, when the friends father finds some mysterious money, the friend claims that she was the one who sat at the bar while Lilja prostituted herself. Not content with ruining Liljas reputation at home, the friend soon circulates the story around the school. As Lilja has been abandoned, she now really does have to prostitute herself for money to live. One glimmer of hope is her friend Volodya, abused and rejected by his alcoholic father, with whom she forms a tender protective relationship. She buys Volodya a basketball with money she has earned as a prostitute, but Volodyas father punctures it with a pair of scissors. Another glimmer of hope is Andrei, who becomes her boyfriend and offers her a job in Sweden. But all is not what it seems, and only bad things await Lilja when she arrives there.

After arriving in Sweden, she is greeted by her future "employer" (in reality, a pimp) and taken to a nearly empty apartment where he imprisons her and rapes her. Lilja is then forced to perform sexual acts for a large number of clients; nearly all the abuse is seen from Liljas point of view.

Meanwhile in the former Soviet Union, Volodya commits suicide, devastated that Lilja had abandoned him to his fate. In the form of an angel, Volodya comes to Lilja to watch over her. On Christmas Day, he transports Lilja to the roof of the apartment, and gives Lilja the world as a present, but she simply finds it cold and unwelcoming. After one escape attempt Lilja is brutally beaten by her pimp, but she then escapes again. Finally, and much to the distress of Volodya (who regrets having killed himself) she commits suicide herself in the continuation of the scene from the beginning of the film by jumping from the bridge.

The films conclusion shows two alternate versions of events: a. Lilja and Volodya, now both dead, angelic and happily playing basketball on the roof of some tenement building, safe from all harm the world can do to them. It is also shown that somehow Lilja was sent back in time to when she made the decision to go to Sweden with Andrei (possibly as a result of the deceased Volodyas intervention). In essence she finds herself exactly at the moment she first made the decision, however this time she rejects Andreis offer to go to Sweden and she and Volodya are shown to presumably live happier lives.

==Cast==
* Oksana Akinshina as Lilja
* Artyom Bogucharsky as Volodya
* Lyubov Agapova as Liljas mother
* Liliya Shinkaryova as Aunt Anna
* Elina Benenson as Natasha
* Pavel Ponomaryov as Andrei
* Tomasz Neuman as Witek
* Anastasiya Bedredinova as neighbour
* Tõnu Kark as Sergei
* Nikolai Bentsler as Natashas boyfriend

==Production==
===Writing and pre-production=== SEK (US$2410 in 1999; $  today) for travel expenses, and she was forced to prostitute herself for the next month. She escaped from the apartment where she was being held in the rough suburb of Arlöv, moved to Malmö, and after three months, day after she had been raped by her boyfriend and two other men, on 7 January 2000 jumped from a bridge and died three days later in the hospital. Three letters she was carrying with her unravelled the story.       The screenplay was originally supposed to be deeply religious, with Jesus being a prominent character, walking next to Lilja throughout the story.    Moodysson wrote the script in Swedish and then had it translated into Russian.   
 Swedish and Danish Film Institutes as well as Nordisk Film- & TV-Fond.  The budget was 30 million SEK.   

During the casting period, Moodysson and the crew interviewed "something like 1000" young applicants for the leading roles. The actors had to improvise on a scenario where they had been grounded and were trying to convince their mother to let them go out.  While Artyom Bogucharsky had no previous acting experience, Oksana Akinshina had already starred in Sergei Bodrov, Jr.s 2001 crime film Sisters (2001 film)|Sisters. Moodysson has commented Akinshina as "  exactly what I had imagined. She is better than I imagined but different, somehow." 

===Filming and post-production===
  in Estonia where the film was largely shot.]] Show Me Love. Dahlström, whose mother is Russian, also served as assistant director, which the producers held as an advantage since she was the same age as the title character. 

Director of photography was Ulf Brantås, who started his career as a cinematographer for Roy Andersson and had filmed both of Moodyssons previous feature films. Lilya 4-ever was shot with an Aaton XTR Prod on 16&nbsp;mm film which was later transferred to 35&nbsp;mm. Minimum lighting was used, mainly from practicals, and whenever possible only sunlight. Locations were only sparsely rigged by the crew. A custom built Cycle rickshaw|rickshaw, made from the wheels of a mountain bike, was used for the long rearward-facing tracking shots. No correction filters were used though the stock was eventually graded in post-production in order to appear slightly warmer. 

==Release==
On 23 August 2002,  . Retrieved 2009-12-01.   A limited release in the United States begun on 18 April 2003 through Newmarket Films.  Metrodome released it on 25 April 2003 in the United Kingdom, where it opened in 13 theatres.  The Australian premiere followed on 7 August the same year, distributed by Potential Films. 

The film has also been utilised by humanitarian organisations, in information campaigns against human trafficking in various Eastern European countries. In Moldova, the International Organization for Migration received the distribution rights and organised screenings attended by 60,000 people, mostly young females but also members of the government.  

==Reception==
Swedish critics were very positive to Lilja 4-ever upon its release. Malena Janson started her review in   and Together (2000 film)|Together. 

The film was embraced by most English-language critics as well. As of January 2013, it had an 87% approval from 67 reviews listed at  s Tony Rayns. Rayns dismissed the film as melodramatic and lacking in substance, while also criticizing the stylistic choice of the dream sequences, as well as the soundtracks composition: "The most extreme case is   use of Rammsteins Mein Herz brennt, played at woofer-challenging volume over the opening and closing scenes. ... Even if we take the volume as a metaphor for the girls wish to block out the world, its absurd to imagine that Lilja would ever relate to or even listen to a Rammstein track in German. So the wall of sound comes from some higher version of MTV, not from the character or story." 

==Awards and honors==
Lilya 4-ever won several awards from film festivals around the world including Best Film at Gijón International Film Festival. Akinshina won the awards for Best Actress both in Gijón and at Rouen Nordic Film Festival.   Ulf Brantås was prized for Best Cinematography at Zimbabwe International Film Festival and Moodysson won the award for Best Director at Brasília International Film Festival. 
 Best Film, Best Direction, Best Actress at the European Film Awards.  It was List of Swedish submissions for the Academy Award for Best Foreign Language Film|Swedens submission for the Academy Award for Best Foreign Language Film at the 75th Academy Awards, which sparked some controversy when the Academy considered to deem it ineligible since the primary language is not Swedish. Eventually it was accepted, but failed to be nominated.  In November 2009 the film magazine FLM published a list of the 10 best Swedish films of the decade as voted by 26 of the countrys leading critics. Lilya 4-ever appeared as number three on the list, surpassed only by Involuntary (film)|Involuntary and Songs from the Second Floor. 

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 