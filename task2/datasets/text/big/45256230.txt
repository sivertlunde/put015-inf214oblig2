The Bare-Footed Kid
{{Infobox film
| name           = The Bare-Footed Kid
| image          = TheBare-FootedKid.jpg
| alt            = 
| caption        = DVD cover
| film name      = {{Film name
| traditional    = 赤腳小子
| simplified     = 赤脚小子
| pinyin         = Chì Jiǎo Xiǎo Zi
| jyutping       = Cek3 Geok3 Siu2 Zi2 }}
| director       = Johnnie To
| producer       = Mona Fong
| writer         = 
| screenplay     = Yau Nai-hoi
| story          = Sandy Shaw
| based on       = 
| starring       = Aaron Kwok Maggie Cheung Ti Lung Jacklyn Wu
| narrator       = 
| music          = William Wu
| cinematography = Horace Wong
| editing        = Wong Wing-ming
| studio         = Cosmopolitan Film
| distributor    = Newport Entertainment
| released       =  
| runtime        = 90 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          = HK$3,973,198
}}

The Bare-Footed Kid is a 1993 Hong Kong martial arts film directed by Johnnie To and starring Aaron Kwok, Maggie Cheung, Ti Lung and Jacklyn Wu. The film is a remake of the 1977 film Disciples of Shaolin which was directed by Chang Cheh.      

==Plot== Paul Chun) want to eradicate Hak, but they do not have enough evidence to bring him to justice. Later, Tin-yau meets Pak Siu-kwan (Maggie Cheung), the owner of "Four Seasons Weaver" and Wahs daughter Lin (Jacklyn Wu). Then it was revealed that Tuen was a fugitive who changed his name and hid in the dye factory to avoid arrest and developed affection towards Pak. Fung-yiu and Lin also develops a mutual bond after an incident.

"Tin Lung Spinners" had always been inferior to "Four Seasons Weaver". Feeling vengeful, Hak sets "Four Seasons Weave" on fire to vent his anger. Fung-yiu, who is witless, dazed and confused, gets up to the fighting arena, and was lured by Hak where he kills his friends father. Fung-yiu had fallen further and further into a quagmire of confusion. He also reveals Tuens past identity as a killer and Tuen is wanted by officials. Fortunately, Yuen Tin-yaus discerning eyes can tell greatness from Fung-yiu, and they work together to defeat the "Dragon Place". However, Tuen was unfortunately ambushed by Hak where he swallowed poison and shot to death by millions of arrows. Fung-yiu hurried off to save Tuen, but it was too late by then.

==Cast==
*Aaron Kwok as Kwan Fung-yiu
*Ti Lung as Tuen Ching-wan
*Maggie Cheung as Pak Siu-kwan
*Jacklyn Wu as Wah Wong-lin Paul Chun as Mr. Wah
*Kenneth Tsang as Hak Wo-po
*Cheung Siu-fai as Magistrate Yuen Tin-yau
*Wong Yat-fei as Kuei
*Tin Ching as Paks worker
*Wong San as Paks indebted worker
*Chu Tit-wo as Hung Chun-tin
*Benny Lai as City guard
*Johnny Cheng as City guard
*Yuen Ling-to as Pui
*Cheng Ka-sang as Haks bodyguard
*Leung Kai-chi as Weaver at Tin Lung Spinners
*Hau Woon-ling as Sam Ku, woman leading bridal march
*Jacky Cheung Chun-hung as Magistrate Yuens aide
*So Wai-nam as Magistrate Yuens aide
*Kent Chow as Magistrate Yuens aide
*Kong Miu-deng as Haks thug
*Mak Wai-cheung as Haks thug
*Huang Kai-sen as Haks thug
*Chan Min-leung as Chan
*Chan Man-hiu as Shoes vendor
*Ng Wui as Cow owner
*Kam Lau as Shop owner
*San Tak-kan as Boat passenger
*Kai Cheung-lung as Boatman
*Adam Chan
*Kwan Yung
*Jameson Lam

==Music==
===Theme song===
*The Expression After Speaking (留下句號的面容)
**Composer: William Wu
**Lyricist: Siu Mei
**Singer: Aaron Kwok

===Insert theme===
*Wait For You to Be Back (等你回來)
**Composer: William Wu
**Lyricist: Siu Mei
**Singer: Cass Phang

==Reception==
===Critical===
The Bare-Footed Kid received generally positive reviews. Ard Vijn of Twitch Film writes "its not a classic by any means but its a fun movie that definitely has its moments. Fans of either Aaron Kwok, Ti Lung or Maggie Cheung wont be disappointed." {{cite web|url=http://twitchfilm.com/2007/02/the-bare-footed-kid-dvd-review.html|title=
THE BARE-FOOTED KID DVD-review }}  Mark Polland of   rated it 4 out of 5 stars and writes The Bare-Footed Kid is a thoughtful kung fu film with an unusually strong story that winningly delivers a message that strength and fighting ability are useless without morality and sound judgment.  Andrew Saroch of   also rated film 4 out of 5 stars and writes "while not as good as Disciples of Shaolin, Bare-Footed Kid is an excellent film and like its inspiration, operates of a number of levels." 

===Box office===
The film grossed HK$3,973,198 at the Hong Kong box office during its theatrical run from 3 to 14 April 1993.

==See also==
*Aaron Kwok filmography
*Johnnie To filmography

==References==
 

==External links==
* 
*  at Hong Kong Cinemagic
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 