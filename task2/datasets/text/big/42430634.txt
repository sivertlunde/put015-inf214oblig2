Emperor Maciste
{{Infobox film
| name           = Emperor Maciste
| image          =
| caption        =
| director       = Guido Brignone 
| producer       = 
| writer         = Pier Angelo Mazzolotti
| starring       = Bartolomeo Pagano   Domenico Gambino   Franz Sala
| music          = 
| cinematography =  Massimo Terzano
| editing        = 
| studio         = Fert Film 
| distributor    = Societa Anonima Stefano Pittaluga
| released       = November 1924 
| runtime        = 70 minutes
| country        = Italy
| awards         =
| language       = Silent   Italian intertitles
| budget         =
| preceded_by    =
| followed_by    =
}} silent adventure peplum series of silent films featuring the strongman Maciste. The character of Maciste increasingly came to resemble Benito Mussolini, in this case striking Fascistic poses and defending order against criminal and dishonest elements. 

== Cast ==
* Bartolomeo Pagano as Maciste 
* Domenico Gambino as Saetta
* Franz Sala 
* Elena Sangro
* Oreste Grandi
* Augusto Bandini 
* Lola Romanos 
* Gero Zambuto 
* Felice Minotti
* Armand Pouget
* Lorenzo Soderini

== References ==
 

== Bibliography ==
* Gundle, Stephen. Mussolinis Dream Factory: Film Stardom in Fascist Italy. Berghahn Books, 2013. 
* Moliterno, Gino. Historical Dictionary of Italian Cinema. Scarecrow Press, 2008.
* Ricci, Steven. Cinema and Fascism: Italian Film and Society, 1922–1943. University of California Press, 2008.

== External links ==
*  

 

 
 
 
 
 
 
 


 