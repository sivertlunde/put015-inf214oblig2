Suddenly (2006 film)
{{Infobox Film
|name=Suddenly
|image=
|caption= Swedish
|runtime=96 minutes
|director=Johan Brisinger
|writer=Johan Brisinger, Mikael Bengtsson
|starring=Anastasios Soulis   Michael Nyqvist   Moa Gammel
|producer=Anders Birkeland, Göran Lindström
|distributor= country = Sweden language = Swedish
|released=  

|budget=
}} Swedish film from 2006 in film|2006, directed by Johan Brisinger. The film was shot on the island Härmanö in Orust Municipality.

==Plot==
Jonas, his brother Erik, and his parents are a typical family. The normal stresses and worries and squabbles. Until tragedy strikes one day on the way to grandmas house. Jonas world is shattered and worse yet, hes got to deal with it alone. His father, Lasse, is too distracted with his own grief to be much help. After his fathers suicide attempt, they go to the familys summer house where Jonas meets the rather direct Helena. As the summer unfolds, Jonas finally learns how to grieve through his friendship with Helena.

==Cast==
* Anastasios Soulis - Jonas
* Michael Nyqvist - Lasse
* Moa Gammel - Helena
* Catherine Hansson - Lotta
* Sten Ljunggren - Sven
* Anita Wall - Svea

==External links==
* 
* 

 

 