Countdown (2011 film)
{{Infobox film name           = Countdown  image          = Countdown2011Poster.jpg caption        = Film poster director       = Huh Jong-ho  producer       = Oh Jung-wan Jo Kwang-hee writer         = Huh Jong-ho  starring       = Jeon Do-yeon Jung Jae-young  music          = cinematography = Kim Tae-kyung editing        =  distributor  Sidus FNH released       =     runtime        = 119 minutes country        = South Korea language       = Korean budget         = $6 million admissions     = 471,631  gross          = $3,168,201 
}}
Countdown ( ) is a 2011 caper film that takes the audience on an entertaining journey through the underbelly of South Korea. Starring two of the countrys top actors Jeon Do-yeon and Jung Jae-young,  this highly assured debut feature by Huh Jong-ho premiered at the 2011 Toronto International Film Festival. 

==Plot==
Tae Gun-ho ( . Retrieved on 2011-09-08. 

Locating Cha turns out to be easy, since she’s about to be released from prison. The deal she proposes, however, which includes getting even with the sleazy crime boss who set her up, jeopardizes Tae’s future. He struggles desperately to keep Cha, and her liver, safe until the transplant — even while Cha has other plans. 

==Cast==
*Jeon Do-yeon as Cha Ha-yeon    
*Jung Jae-young as Tae Gun-ho Min as Jang Hyeon-ji
*Lee Geung-young as Jo Myung-suk 
*Oh Man-seok as Swy
*Kim Dong-wook as Nalpari ("gnat") 	
*Jung Man-sik as department head Han
*Kwon Hyuk-joon as Yoo-min Kim Kwang-kyu as Detective Park
*Kim Jong-goo as Director Yang
*Oh Kwang-rok as Dr. Song
*Bae Sung-woo as Dr. Ahn
*Choi Jong-ryul as Gun-hos father
*Park Hye-jin as Gun-hos mother
*Bae Yoon-beom as team leader Hong
*Noh Hyun-jung as CEO of CMC
*Kim Min-jae as Dong office clerk
*Jung Hye-sun as granny money lender (cameo)
*Song Hye-kyo as pretty girl (cameo)

==References==
 

==External links==
*    
*  
*  
*  

 
 
 
 
 
 
 
 
 