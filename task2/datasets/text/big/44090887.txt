Kaduvaye Pidicha Kiduva
{{Infobox film 
| name           = Kaduvaye Pidicha Kiduva
| image          =
| caption        =
| director       = AB Raj
| producer       = TE Vasudevan
| writer         = S. L. Puram Sadanandan
| screenplay     = S. L. Puram Sadanandan Lakshmi
| music          = V. Dakshinamoorthy
| cinematography = C Namasivayam
| editing        = BS Mani
| studio         = Jaya Maruthi
| distributor    = Jaya Maruthi
| released       =  
| country        = India Malayalam
}}
 1977 Cinema Indian Malayalam Malayalam film, Lakshmi in lead roles. The film had musical score by V. Dakshinamoorthy.   

==Cast==
 
*Prem Nazir
*Sukumari
*Adoor Bhasi Lakshmi
*Sam
*Sankaradi
*Sreemoolanagaram Vijayan GK Pillai
*Jayamalini
*K. P. Ummer
*P. K. Abraham
*Paravoor Bharathan
*Veeran
*Vijayalalitha
 

==Soundtrack==
The music was composed by V. Dakshinamoorthy and lyrics was written by Sreekumaran Thampi. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Chiriyo Chiri || K. J. Yesudas || Sreekumaran Thampi || 
|-
| 2 || Mounamithenthe Maayaavi || Vani Jairam || Sreekumaran Thampi || 
|-
| 3 || Neelaanjanamalayilu Neeli || K. J. Yesudas || Sreekumaran Thampi || 
|-
| 4 || Oru Swapnathil || P Susheela || Sreekumaran Thampi || 
|}

==References==
 

==External links==
*  

 
 
 

 