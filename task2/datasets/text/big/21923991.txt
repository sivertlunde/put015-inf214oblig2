How to Console Widows
{{Infobox film
| name           = How to Console Widows
| image          = Como Consolar Viuvas.jpg
| image_size     = 200px
| caption        = Theatrical release poster
| director       = José Mojica Marins (as J. Avelar)
| producer       = Augusto de Cervantes Georgina Duarte
| writer         = Georgina Duarte
| starring       = Vic Barone Zélia Diniz Vosmarline Siqueira Lourênia Machado
| music          = Solon Curvelo
| cinematography = Georgio Attili
| editing        = Roberto Leme
| studio         = MASP Filmes
| distributor    = Program Filmes
| released       =  
| runtime        = 93 minutes
| country        = Brazil
| language       = Portuguese
| budget         = 
}}
 1976 Cinema Brazilian film directed by José Mojica Marins. Marins is best known for the Zé do Caixão (Coffin Joe) film series. In this film Marins is credited as J. Avelar.      

==Plot==
 
A bankrupt playboy makes a plan to obtain money. He reads in the news about the death of three wealthy married rich men who die in a plane crash. Disguised as a ghost, that night he visits the three widows. Scared, they not only give him money, but also consent to having sex with him.

When the three women become pregnant, the father hires a priest to exorcise the little ghosts from them.

==Cast==
* Vic Barone
* Zélia Diniz
* Vosmarline Siqueira
* Lourênia Machado
* Walter Portela
* Vick Militello
* Chaguinha
* João Paulo Ramalho
* Helena Samara
* René Mauro
* José Carvalho
* David Húngaro

==References==
 

==External links==
*   
* 
* 
*  at Portal Heco de Brasil  

 

 
 
 
 
 
 

 