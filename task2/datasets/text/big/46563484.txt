Judge Saheb
{{Infobox film
| name           = Judge Saheb
| image          =
| caption        =
| director       = Pijush Debnath
| producer       = Indrani Roy
| screenplay     = Pijush Debnath
| story          = Pijush Debnath
| music          = Ajoy Das
| starring       = Prasenjit Chatterjee Ranjit Mallick Utpal Dutta Satabdi Roy Biplab Chatterjee Jnanesh Mukhopadhyay Sandhyarani Shekhar Chattopadhyay Sobha Sen
| runtime        = 
| released       =  
| country        = India
| genere         = Drama,Family
| editing        = Subodh Roy Bengali
| cinematography = Bijoy Ghosh
| awards         =
| budget         =
}}
 Bengali film directed by Pijush Debnath and produced by Indrani Roy.The film music composed by Ajoy Das.The film starring Prasenjit Chatterjee and Satabdi Roy in lead roles.

==Cast==
* Prasenjit Chatterjee
* Ranjit Mallick
* Utpal Dutta
* Satabdi Roy
* Biplab Chatterjee
* Jnanesh Mukhopadhyay
* Sandhyarani
* Manoj Mitra
* Shekhar Chattopadhyay
* Sobha Sen 


==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s) !! Music
|-
| 1
| Aami Eka Boro Eka
| Amit Kumar
| Ajoy Das
|-
| 2
| Bhalo Basha Kasha Bole
| Amit Kumar
| Ajoy Das
|-
| 3
| Manjhe Manjhe So Rodh
| Amit Kumar
| Ajoy Das
|-
| 4
| Mon Jodi Chaye
| Amit Kumar
| Ajoy Das
|-
| 5
| Ganye Jawar Elo Phire (Instrumental)
| Ajoy Das
| Ajoy Das
|-
| 6
| Choke Gelo Chole Gelo (Instrumental)
| Ajoy Das
| Ajoy Das 
|}

==References==
 

==External links==
 
*   in Gomolo
*  
*  
*  
*  
*  


 
 
 

 