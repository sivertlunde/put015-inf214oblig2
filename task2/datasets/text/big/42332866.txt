Kanasugara
{{multiple issues|
 
 
}}

{{Infobox film
| name = Kanasugara
| image =
| caption = 
| director = Karan
| writer = Vikraman
| based on = Unnidathil Ennai Koduthen (1998)
| producer = Sa. Ra. Govindu Prema   Shashikumar   Mandya Ramesh
| music = Rajesh Ramanath
| cinematography = G. S. V. Seetharam
| editing = Shyam
| studio  = Thanu Chitra
| released = August 3, 2001
| runtime = 147 minutes
| language = Kannada  
| country = India
}}
 Prema and Tamil blockbuster Karthik and Roja in lead roles.
 Filmfare Best Actress award for this film.

== Cast ==
* V. Ravichandran  Prema 
* Shashikumar 
* Doddanna 
* Srinath  Ambika
* Mandya Ramesh
* Anu Prabhakar
* Tennis Krishna
* Hema Chowdhary
* Mimicry Dayanand
* V. Manohar
* Umesh

== Soundtrack ==
All the songs are composed and scored by Rajesh Ramanath.  Almost all the songs are recomposed with the same tunes as in the original Tamil film composed by S. A. Rajkumar except the song "Om Namaha" which was reused from "Kaathoramai Kadhai" from Kannupadapoguthaiya also composed by Rajkumar.

{|class="wikitable"
! Sl No !! Song Title !! Singer(s) || Lyrics
|-
| 1 || "Chitte Banthu Chitte" || S. P. Balasubramanyam || K. Kalyan
|-
| 2 || "Kamana Bille" || S. P. Balasubramanyam, K. S. Chithra || K. Kalyan
|-
| 3 || "Suryana Gelethanake" || S. P. Balasubramanyam, K. S. Chithra || K. Kalyan
|-
| 4 || "Ello Adu Ello" || S. P. Balasubramanyam, K. S. Chithra || K. Kalyan
|-
| 5 || "Koti Pallavi Haaduva" || K. S. Chithra || K. Kalyan
|-
| 6 || "Om Namaha" || S. P. Balasubramanyam, Manjula Gururaj || K. Kalyan
|}

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 


 

 