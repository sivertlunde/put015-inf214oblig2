4:30
{{Infobox film
| name           = 4:30
| image          = 4-30poster.jpg
| caption        = Theatrical poster
| director       = Royston Tan
| producer       = Eric Khoo Jacqueline Khoo
| writer         = Royston Tan Liam Yeo
| narrator       = Andrew Seow Li-Yuan Kim Young-jun
| music          = Hualampong Riddim Vichaya Vatanasapt
| cinematography = Leong Ching Lim
| editing        = Low Hwee Ling
| distributor    = Peccadillo Pictures (UK)
| released       = 29 June 2006 (Singapore)
| runtime        =
| country        = Singapore
| language       = Korean Mandarin English
| budget         = S$400,000
| gross          =
}}
4:30 is a 2005 Singaporean film. It is directed by filmmaker  .  The film is made on a budget of S$400,000.  It was released in Singaporean Cinemas on 29 June 2006. 

==Film Festivals==

{| class="wikitable" style="font-size: 95%;"
|-
! Award
! Date of ceremony
! Category
! Recipients and nominees
! Result
|-
| NHK Asian Film Festival   Japan Broadcasting Co. || 17 December 2005|| Asian Film Festival || 4:30 || Participant
|-
|}

===2006 IFF Bratislava===
*Grand Prix - 4:30 (directed by Royston Tan, Singapore)

==References==
 

==External links==
*  
*  

 
 
 
 
 
 


 