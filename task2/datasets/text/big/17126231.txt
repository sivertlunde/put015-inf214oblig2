The Unholy (1988 film)
{{Infobox film
| name           = The Unholy
| image          = The Unholy (1988 film).jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = Camilo Vila	
| producer       = Mathew Hayden
| writer         = Philip Yordan Fernando Fonseca
| narrator       = 
| starring       = Ben Cross Ned Beatty William Russ Jill Carroll Hal Holbrook Trevor Howard
| music          = Roger Bellon
| cinematography = Henry Vargas
| editing        = Mark Melnick
| distributor    = Vestron Pictures
| released       = April 22, 1988 (US)
| runtime        = 102 min.
| country        = United States English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 1988 horror film directed by Camilo Vila and starring Ben Cross and Ned Beatty. It also features Trevor Howard in his final role.

==Plot==
Father Michael (Ben Cross) is appointed pastor of St Agnes church after surviving a fall. Closed for three years following the mysterious deaths of his predecessors, he attempts to find out why he has become the Chosen One while resisting a powerful demon who takes the form of a beautiful woman.

==Release==
The film was released theatrically in the United States by Vestron Pictures in April 1988. It grossed $6,337,299 at the box office. 

The movie was released on DVD by Lionsgate in the United Kingdom in 2007.  Lionsgate has released this film in 2012 on R1 DVD as part of an 8 horror film set.

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 


 