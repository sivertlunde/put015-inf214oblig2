To Save the City
{{Infobox film
| name           = To Save the City
| image          = 
| caption        = 
| director       = Jan Łomnicki
| producer       = 
| writer         = Andrzej Szczypiorski Jan Łomnicki
| starring       = Teresa Budzisz-Krzyżanowska
| music          = 
| cinematography = Jerzy Gościk
| editing        = 
| distributor    = 
| released       =  
| runtime        = 110 minutes
| country        = Poland
| language       = Polish
| budget         = 
}}

To Save the City ( ) is a 1976 Polish drama film directed by Jan Łomnicki. It was entered into the 10th Moscow International Film Festival.   

==Cast==
* Teresa Budzisz-Krzyżanowska as Jadwiga Nowacka
* Jan Krzyżanowski as Marian Nowacki
* Jacek Miśkiewicz as Janek Nowacki
* Alexander Borisovich Belyavsky as Cpt. Syemyonov (as Aleksander Bielawski)
* Nina Maslova as Masha
* Kirill Arbuzov as Seryozha
* Sergei Polezhayev as Gen. Ivan Konev
* Oleg Mokshantsev as Gen. Ivan Korovnikov
* Jerzy Bączek
* Henryk Bista as Capt. AK Sztych
* Barbara Bosak
* Andrzej Buszewicz as AK soldier
* Marian Cebulski as Kania
* Stanisław Chmieloch as AK soldier Malarz

==References==
 

==External links==
*  

 
 
 
 
 
 