A Stolen Life (1946 film)
{{Infobox film 
| name           = A Stolen Life
| image          = A Stolen life Theatrical release poster.jpg
| caption        = Theatrical release poster Jack Gage (dialogue director)	
| producer       = Bette Davis		
| writer         = Karel J. Benes
| based on       = Uloupeny Zivot
| screenplay     = Catherine Turney Margaret Buell Wilder
| starring       = Bette Davis Glenn Ford Dane Clark Walter Brennan Charles Ruggles Bruce Bennett
| music          = Max Steiner
| cinematography = Ernest Haller Sol Polito
| editing        = Rudi Fehr
| studio         = B.D. Production
| distributor    = Warner Bros.
| released       =  
| runtime        = 109 minutes
| language       = English
| country        = United States
| budget         =
}}
 directed by Stolen Life starring Elisabeth Bergner and Michael Redgrave.
 Dead Ringer (1964).

==Plot==
Kate Bosworth (played by Bette Davis|Davis) is a sincere, demure girl and artist who misses her boat to an island off New England, where she intends to meet her sister Patricia and cousin Freddie. She persuades Bill Emerson (played by Glenn Ford|Ford) to take her home in his boat. Their relationship grows while she paints a portrait of Eben Folger (Walter Brennan), the old lighthouse keeper, and Kate is very much in love.

However, her sister Pat (also played by Davis) happens to be a twin. A flamboyant, man-hungry manipulator, she fools Bill at first, pretending to be Kate. Then she pursues him on a trip out of town, and they return announcing to Kate their intention to marry.

Kate focuses on her work with artist Karnock (Dane Clark), but rejects his romantic overtures. Bill eventually goes to Chile, allowing Kate to spend some time with her sister and learn that the marriage is in trouble. When they go sailing, Pat is washed overboard and drowns. Kate is washed ashore, and, when she regains consciousness, she is mistaken for Patricia.

Bill is about to return, so Kate decides to assume her late sisters identity. Bill is angry at Pats many affairs and in no mood to continue the marriage. Freddie (Charles Ruggles) has guessed the truth and insists that Kate must reveal to Bill her real identity. When she does, Bill  realizes that Kate is the one he truly loves.

==Cast==
* Bette Davis as Kate and Patricia Bosworth
* Glenn Ford as Bill Emerson
* Dane Clark as Karnock
* Walter Brennan as Eben Folger
* Charles Ruggles as Freddie Linley
* Bruce Bennett as Jack R. Talbot
* Peggy Knudsen as Diedre
* Esther Dale as Mrs. Johnson
* Clara Blandick as Martha
* Joan Winfield as Lucy

==External links==
 
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 