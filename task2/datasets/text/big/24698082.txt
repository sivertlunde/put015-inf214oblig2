Jhinder Bandi
{{Infobox film
| name           = 
| image          = 
| alt            = 
| caption        = 
| film name      = 
| director       = Tapan Sinha
| producer       = Bhola Nath Roy
| writer         = 
| screenplay     = Tapan Sinha
| story          = 
| based on       =     
| narrator       = 
| starring       = Uttam Kumar  Arundhati Devi Soumitra Chatterjee
| music          = Ali Akbar Khan
| cinematography = Bimal Mukherjee
| editing        = Subodh Roy
| studio         = B. N. Productions
| distributor    = 
| released       =   
| runtime        = 
| country        = India Bengali
| budget         = 
| gross          = 
}}
 Bengali fictional fictional historical historical drama drama thriller film directed by Tapan Sinha, starring Uttam Kumar, Soumitra Chatterjee, Arundhati Devi and Radhamohan Bhattacharya.
 The Prisoner of Zenda (1937). Human emotions like hate, love, jealousy and betrayal are all well dealt in the movie. For the first time Soumitra Chatterjee is seen in a villainous role. Soul touching music by Ali Akbar Khan further adds a facet to the film.

==Plot==
A person comes to meet with Gauri Shankar Roy (Uttam Kumar) living in Kolkata. He introduces himself as a fauji Sardar of Jhind, Jhind being a small kingdom in Madhya Pradesh. He says that the to-be king of Jhind, Shankar Singh, is found missing from the kingdom right before his abhishek(oath taking of a king) and this is apparently a conspiracy by his own brother Udit Singh, who wants the kingdom for himself. Earlier too there were two occasions when abhishek was arranged but both the times the king was found missing. Udit is a cruel hearted man, unfit to be a good king. Shankar Singh has his vices too but is a kind hearted person who would care for his citizens and is thus worthy to be the king. Now, coincidentally, Gauri has exactly the same appearance as Shankar Singh(also played by Uttam Kumar). This being the last hope, Sardar requests him to enact the role of the King for the abhishek until he finds the real king. Gauri agrees.The two set off for Jhind.

Things arent easy at the Palace in Jhind. Despite the great luxury of kingly life, Gauri is constantly threatened by Udit and his friend, the dashingly handsome but evil Mayurvahan (Soumitra Chatterjee). The abhishek however takes place successfully in the occasion of which the king is also engaged to Rani Kasturi Bai (Arundhati Devi). While the atmosphere gets more and more tensed, the life of king is at stake, there is also a blooming romance between Kasturi Bai and Gauri.
  Dewan of Jhind,Kali Shankar Roy. The then-king being issue less, adopted Shankar and Udit as sons.

Planning a secret entry to the fortress where king was held prisoner, Gauri fights a battle with Mayurvahan, finally killing him as well as surprising and killing Udit. As he approaches his look-alike Shankar Singh, he has a momentary desire to kill the king, gaining the kingdom himself, he being as deserving by his blood as was Shankar Singh, but his humanity wins over and he approaches the king addressing him as "Your Highness". He then bids a last goodbye to Kasturi Bai and rides a horse, heading back to Kolkata.

==Cast==
* Uttam Kumar as Gauri Shankar Roy/Shankar Singh
* Arundhati Devi as Rani Kasturi Bai
* Soumitra Chatterjee as Mayurvahan
* Radhamohan Bhattacharya as Fauji Sardar of Jhind
* Tarun Kumar as Udit Singh, Prince of Jhind
* Dilip Roy as Rudrarup, a loyal companion of the king
* Sandhya Roy
* Dhiren Mukherjee

== External links ==
*  
* 
 

 
 
 
 
 
 