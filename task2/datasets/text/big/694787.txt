The Dukes of Hazzard (film)
{{Infobox film
| name = The Dukes of Hazzard
| image = Dukes of hazzard movie poster.jpg
| image_size = 215px
| alt = 
| caption = Theatrical release poster
| director = Jay Chandrasekhar
| producer = Bill Gerber John OBrien
| story = John OBrien  Jonathan L. Davis
| based on =  
| narrator = Junior Brown
| starring = Johnny Knoxville Seann William Scott Jessica Simpson Burt Reynolds Willie Nelson 
| music = Nathan Barr
| cinematography = Lawrence Sher
| editing = Lee Haxall Myron I. Kerstein
| studio = Village Roadshow Pictures
| distributor = Warner Bros. Pictures
| released =  
| runtime = 106 minutes
| country = United States
| language = English
| budget = $50 million
| gross = $111,069,515
}} American television series of the same name. The film was directed by Jay Chandrasekhar and released on August 5, 2005 by Warner Bros. Pictures. As in the television series, The Dukes of Hazzard depicts the adventures of cousins Bo, Luke, Daisy and their Uncle Jesse as they outfox crooked Hazzard County commissioner Boss Hogg and Sheriff Rosco P. Coltrane.

The film was the debut of   in 2007.  

==Plot== Bo (Seann Luke (Johnny Uncle Jesse Hazzard County, Charger that Jefferson Davis Hogg, widely known as "Boss Hogg" (Burt Reynolds), and his willing but dimwitted henchman, Sheriff Rosco P. Coltrane (M.C. Gainey).
 Kevin Heffernan). Cooter (David Koechner), who instead turns the car into a hot-rod and applies a new paint job and horn, in return for finally getting payment for all the work he has done ("...cause thats how this works...") for the boys in the past.

  Atlanta Police Sheriff Deputy Enos Strate (Michael Weston), that Billy Prickett has been hired by Boss Hogg to participate in the Rally as a ringer.  Boss Hogg then heads to Atlanta where he informs the Duke boys, in lock-up, that they are too late to stop him and reveals that the vote on Hoggs proposition is at the same time as the rally, explaining Billy Pricketts involvement. During a transfer from detainment, Daisy helps the boys escape from the patrol car and they speed home to try to inform the townsfolk, escaping Atlanta Police, and the Georgia State Patrol after Bo out-maneuvers the city cops.

Upon returning home, the Dukes discover that Rosco has taken Uncle Jesse and Pauline hostage, an obvious trap for the boys, and that Billy is in on the scheme because hes ashamed of the towns low status.  The two race to the farmhouse to cause a distraction to the waiting Hazzard County Sheriffs Deputies and Georgia State Troopers while Daisy and Cooter rescue Jesse and Pauline.  Meanwhile, the college girls head to the rally with Sheev to inform the townsfolk about the vote on the strip-mining ordinance.  Because of Sheevs armadillo hat and lack of pants, no one listens, so Bo leaves for the rally while Luke and Jesse team up to foil the county and state police who are chasing Bo, interfering with the race.  Upon crossing the finish line first, before Billy, the two continue racing back and forth all the way into town, leading the townsfolk to the courthouse just in time to vote against Boss Hoggs proposed ordinance.  At the courthouse, Daisy takes advantage of the governor of Georgias presence and TV cameras to convince him into pardoning the boys, so Uncle Jesse takes the opportunity to knock out Boss Hogg and gets a pardon for assaulting a county commissioner at the same time.
 television series main theme. Bo and Luke are romantically involved with the girls in the General Lee when they are caught by Lukes other love-interest Laurie Pullman (Alice Greczyn) from the intro of the film, who proceeds to chase them with a shotgun as they drive away.

==Cast==
 
* Johnny Knoxville as Luke Duke
* Seann William Scott as Bo Duke
* Jessica Simpson as Daisy Duke
* Burt Reynolds as Boss Hogg
* Willie Nelson as Uncle Jesse Duke
* David Koechner as Cooter Davenport
* M.C. Gainey as Sheriff Rosco P. Coltrane 
* Michael Weston as Deputy Enos Strate
* Lynda Carter as Pauline
* James Roday as Billy Prickett Kevin Heffernan as Derek "Sheev" Sheevington
* Nikki Griffin as Katie-Lynn Johnson
* Jacqui Maxwell as Annette
* Alice Greczyn as Laurie Pullman
* Junior Brown as The Balladeer (narrator)
* Joe Don Baker as Governor Jim Applewhite
* Barry Corbin as Bill Pullman

===Cameos===
All five members of the comedy film troupe Broken Lizard make appearances in the film, classified as cameos except for Kevin Heffernan who had a larger speaking role (Sheev).
;Broken Lizard cameos
* Steve Lemme appears as Jimmy Pullman, the son of Bill Pullman, in an early car chase scene in which he accidentally shoots the inside of his fathers truck.
* Jay Chandrasekhar and Erik Stolhanske reprise their roles as "Ramathorn" and "Rabbit" from the Broken Lizard comedy, Super Troopers.  The characters are now campus security officers, who give a warning to the Duke boys for driving too slowly.
* Paul Soter appears as TV newsman Rick Shakely, who is reporting from the Hazzard Road Rally.
* Charlie Finn, who played a dim-witted fast food employee in Super Troopers, appears as a dim-witted geology student who assists the Duke boys with the coal samples.

;Other notable  cameos:

* During the bar fight scene, Indy-car driver A.J. Foyt IV appears as himself.
* Bloopers roll under the end credits, one of which features  .

==Production==

===Stunts===
Knoxville said he was initially reluctant to take on the role but was persuaded by script changes and the presence of Dan Bradley as stunt coordinator and second unit for the car chase scenes. Knoxville praised him saying "everyone in Hollywood wants Dan Bradley to shoot their car stuff". 

===Locations===
The majority of the film was shot in and around Clinton, Louisiana.

The street scenes are set in Atlanta but filmed in the New Orleans Central Business District, and the University scenes on the campus of Louisiana State University.

==Reception==

===Box office===
The film was #1 at the box office its opening weekend and grossed United States dollar|$30.7 million on 3,785 screens. It also had an adjusted-dollar rank of #14 all-time for August releases. 

The film eventually collected $111 million worldwide, although it was much less successful financially outside the U.S.

===Critical reception===
The Dukes of Hazzard was panned by most film critics.   named it the worst film of 2005. 

Only 13% of critics gave the film positive reviews on Rotten Tomatoes, based on 165 reviews.  The film received an average rating of 33% on Metacritic based on 36 reviews.  Entertainment Weekly awarded a "B+" to the movie, one of the few positive reviews for the film.  Long-time fans of the original Dukes of Hazzard television series were generally disappointed by the film. 

===Awards===
 
At the 26th Golden Raspberry Awards, the film received seven nominations, but didnt win any. Worst Picture Dirty Love Worst Director (Jay Chandrasekhar) - lost to John Mallory Asher for Dirty Love Worst Screenplay John OBrien) - lost to Jenny McCarthy for Dirty Love
*   Worst Supporting House of Wax Worst Screen Couple (Jessica Simpson & Her Daisy Dukes) - lost to Nicole Kidman and Will Ferrell in Bewitched (2005 film)|Bewitched Worst Remake or Sequel - lost to Son of the Mask

At the Peoples Choice Awards,  Simpson won the "Favorite Song from a Movie" award for her cover of Nancy Sinatras "These Boots Are Made For Walkin".

The film was nominated for two MTV Movie Awards, including Best On-Screen Team (Johnny Knoxville, Seann William Scott, & Jessica Simpson), and Sexiest Performance (Jessica Simpson).

Simpson won the Choice Breakout Female award for her role in the film at the Teen Choice Awards.

==Controversy==

Before the release of this film, Warner Brothers reportedly paid $17.5 million to the producer of Moonrunners, the movie that inspired the TV show. This was soon followed by a claim from screenwriter Gy Waldron. The Hollywood Reporter reported that James Best, who portrayed Rosco P. Coltrane in the original series, filed suit in late July 2011 over royalties he was contracted to receive over spinoffs that "used his identity".  Ben Jones, who played Cooter Davenport in the original series, criticized the film for its emphasis on sexual content, suggesting that the original series was more family-oriented and not as sexualized.  He called for fans of the TV series to boycott the film "unless they clean it up before the August 5th release date."

Some have countered that the original series also contained sexual themes, primarily Catherine Bachs (Daisy Duke) much-displayed "short shorts" (which have become so ubiquitous in American culture that skimpy blue jean cutoff shorts are now often simply called "Daisy Dukes"). In a film review, a New York Daily News entertainment columnist said the movies sex humor is "cruder" than the TV series, but that it is "nearly identical to the TV series in... its ogling of the posterior of cousin Daisy Duke."   
 John Schneider, who played Bo Duke in the original TV series, was later asked if he saw the film and said: "My gosh... it was terrible! It wasn’t Dukes. It was true to whatever it was; I just don’t know what that was!" 

==Soundtrack==
 
 
  version of "Black Betty]]", and the Sylvia Massy produced remake by Spiderbait appear in the film. "If You Want Blood (Youve Got It)" and "Shoot to Thrill" are played by AC/DC.
{{cite news
| url = http://www.usatoday.com/life/television/news/2006-01-05-simpson-peoples-choice_x.htm
| title = USATODAY.com - Jessica Simpson kicks off Peoples Choice Awards
| publisher = www.usatoday.com
| accessdate = 2008-07-31
| last=
| first=
| date=January 5, 2006
}}
 

==Prequel==
A  , was released to television on March 7, 2005, and released to DVD on March 20, 2007.

==References==
 

==External links==
 
 
*  
*  
*  
*  
*  
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 