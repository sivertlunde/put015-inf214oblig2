The Powerpuff Girls Movie
 
{{Infobox film
| name = The Powerpuff Girls Movie
| image = The Powerpuff Girls Movie poster.jpg
| caption = Theatrical release poster
| director = Craig McCracken
| producer = Donna Castricone
| based on = The Powerpuff Girls by Craig McCracken Charlie Bean Lauren Faust Craig McCracken Paul Rudish Don Shank
| story = Charlie Bean Craig McCracken Amy Keating Rogers Paul Rudish Don Shank Lauren Faust
| narrator = Tom Kenny
| starring = Cathy Cavadini Tara Strong Elizabeth Daily|E.G. Daily Roger L. Jackson Tom Kane Tom Kenny
| music = James L. Venable
| cinematography = Genndy Tartakovsky Mike Moon
| editing = Rob Desales
| studio = Cartoon Network Studios Warner Bros. Pictures
| released =  
| runtime = 75 minutes
| country = United States
| language = English
| budget = $11 million
| gross = $16.4 million  
}} superhero Action Warner Bros. Cartoon Network, the film debuted in the United States on July 3, 2002. It is a prequel to the series, telling the origin story of how the Powerpuff Girls were created and how they came to be the defenders of Townsville.

The film received generally positive reviews from critics, but was a modest box office success. It is the only feature film so far to be based on a Cartoon Network series. In theaters, a Dexters Laboratory short entitled "Chicken Scratch" was shown prior to the film, which later aired as part of the series fourth season. The film made its TV debut on Cartoon Network on May 23, 2003. 

==Plot== Professor Utonium Blossom (the Bubbles (the Buttercup (the superpowers as a result of the additional Chemical X, though they all immediately grow to love each other as a family.
 tag and Gangreen Gang. They are rescued by Jojo, whose brain has mutated and given him superintelligence as a result of the Chemical X explosion.

Planning control of the city, Jojo gains the girls empathy, saying he is also hated for his powers, manipulating them into helping him build a laboratory and machine over a volcano in the middle of town that he claims will gain them the affections of the city. The girls give a batch of Chemical X to Jojo. As a reward, Jojo takes them to the local zoo and secretly implants small transportation devices on all the primates there. That night, Jojo transports all the primates from the zoo into his volcano lair and uses his new machine to inject them with Chemical X, turning them into evil mutant primates like himself. The next morning, after the Professor is released from prison, the girls show him all the "good" they have done, only to discover the city being attacked by the monkeys. Jojo, renaming himself Mojo Jojo, publicly denounces the girls as his assistants, turning everyone and the distraught Professor, against them. The girls blast off into space, dejected.

Mojo Jojo announces his intentions to rule the planet, but becomes frustrated when his minions, now as intelligent and evil as he is, begin concocting their own plans to terrorize the people of Townsville. Overhearing the turmoil from space, the girls return to Earth and use their powers to defeat the primates and rescue the citizens. In response, Mojo injects himself with Chemical X and grows into a giant monster, but the girls defeat him after an intense battle by pushing him off a skyscraper. Hoping to help the girls, the Professor develops an antidote for Chemical X which Mojo Jojo lands on, shrinking him down to his original size. The girls consider using the Antidote X to erase their powers, thinking they would be accepted as normal little ones, but the people of Townsville protest, apologizing for misjudging the girls and thanking them for their heroic deeds. At the insistence of the List of characters in The Powerpuff Girls#The Mayor of Townsville|Mayor, the girls agree to use their powers to defend Townsville and become the citys beloved crime-fighting team of superheroes: the Powerpuff Girls.

==Cast==
{| class="wikitable"
|-
!Voice Actor
!Role
|- Cathy Cavadini List of Blossom
|- Tara Strong List of Bubbles
|- Elizabeth Daily|E.G. Daily List of Buttercup
|- Roger L. Jackson List of Mojo Jojo
|- Tom Kane List of Professor Utonium
|- Tom Kenny List of Mayor List Narrator List Snake Cha-Ching Cha-Ching  Mitch Mitchelson
|- Jennifer Hale List of characters in The Powerpuff Girls#Ms. Keane|Ms. Keane
|- Jennifer Martin List of Sarah Bellum
|- Jeff Bennett List of Ace List Big Billy Grubber Baboon Kaboom Go-Go Patrol Hacha Chacha
|- Grey DeLisle Linda Woman at Zoo
|- Phil LaMarr
|I.P. Host Local Anchor
|- Rob Paulsen Hota Wata Killa Drilla Blah-Blah Blah-Blah The Doot Da Doot Da Doo Doos
|- Kevin Michael Richardson Rocko Socko Ojo Tango
|- Dee Bradley Baker Pappy Wappy Tonsa Muncha Rolla Ova Bongo Bango Whacko Smacko Unnamed newscaster Screaming telephone man
|- Frank Welker Whole Lotta Monkeys
|}

==Production notes==
During production, The Hollywood Reporter reported that voice actresses Cathy Cavadini, Tara Strong and E.G. Daily had gone on strike, protesting that they were not being paid enough to star in a feature. The studio threatened to replace them with more popular actresses, not just for the movie, but for the rest of the series too. Soon a deal was reached and the voice actresses continued their roles as Blossom, Bubbles and Buttercup. 

==Design==
The film featured substantially revised designs for many of the TV shows characters, with a much more angular look. Many of these changes were incorporated in the final seasons of the show, such as the Professors new eyes and Aces sharper teeth. 

==Critical reception==
 
Based on 100 reviews, the film holds a 63% approval rating on the website Rotten Tomatoes with the consensus "It plays like an extended episode, but The Powerpuff Girls Movie is still lots of fun". On Metacritic, the film currently has a rating of 65 out of 100, which indicates "generally favorable reviews". Bob Longino of the Atlanta Journal-Constitution praised the film, writing, "The intricate drawings emanate 1950s futuristic pizazz like a David Hockney scenescape. The inspired script is both sinfully cynical and aw-shucks sweet". He also called it "one of the few American creations that is both gleeful pop culture and exquisite high art."
 Ebert & Roeper gave it "two thumbs down," criticizing that the movie was too violent.

==Box office performance==
The film grossed $16.4 million internationally on a budget of $11 million. It was released straight-to-VHS and DVD in some countries. Shortly after its poor commercial performance, a Samurai Jack movie that had been in development was canceled. The Powerpuff Girls Movie earned $3.5 million and ninth place in its opening weekend and ultimately grossed $11 million in North America against its $11 million budget. As a result of its relatively low performance at the box office, it earned the title of "Lowest Grossing Animated Film of 2002," becoming one of the worst-grossing wide releases of 2002, including other flops such as The Adventures of Pluto Nash and Treasure Planet.

==Home video== original television show.  The Region 2 DVD release presents the film in its original widescreen aspect ratio, but omits the audio commentary and deleted scenes bonus features.

==See also==
 
* Theatrically released films based on Hanna-Barbera animations

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 