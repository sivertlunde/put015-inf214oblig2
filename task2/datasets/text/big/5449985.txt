Gentlemen Marry Brunettes
{{Infobox Film
| name           = Gentlemen Marry Brunettes
| image          = Gentlemen Brunettes.jpg
| caption        = Theatrical release poster Richard Sale Robert Waterfield Richard Sale
| based on       =   Richard Sale
| starring       = Jane Russell Jeanne Crain Rudy Vallee Alan Young
| music          = Robert Farnon
| cinematography = Desmond Dickinson
| editing        = Grant K. Smith
| distributor    = United Artists
| released       =  
| runtime        = 99 minutes
| country        = United States
| language       = English
| gross = $1.5 million (US) 
}} romantic musical musical comedy Richard Sale, produced by the director and Bob Waterfield (Russells husband) with Robert Bassler as executive producer, from a screenplay by Mary Loos and Sale, based on the novel But Gentlemen Marry Brunettes by Anita Loos.
 Gentlemen Prefer Blondes, which had been turned into a smash film with Jane Russell and Marilyn Monroe two years before. The studio attempted to repeat the formula, with Russell returning but Jeanne Crain stepping in for a presumably otherwise engaged Monroe (both women played new characters). Alan Young (later the star of TVs Mr. Ed), Scott Brady (brother of Lawrence Tierney), and Rudy Vallee also appear. This film was not as well received as the earlier one.
 Jack Cole, who had also contributed to the Gentlemen Prefer Blondes film. The dance ensemble includes the young Gwen Verdon.

Anita Loos had entitled her book But Gentlemen Marry Brunettes, but the studio dropped the first word from the title for the film.

==Plot==
Bonnie and Connie Jones are showgirls, who are also sisters. They are sick and tired of New York as well as not getting anywhere. Quitting Broadway, the sisters decided to travel to Paris to become famous and find true love.

==Cast==
* Jane Russell as Bonnie Jones / Mimi Jones Anita Ellis)
* Alan Young as Charlie Biddle / Mrs. Biddle / Mr. Henry Biddle
* Scott Brady as David Action (singing voice was dubbed by Robert Farnon)
* Rudy Vallee as Himself
* Guy Middleton as Earl of Wickenwave
* Eric Pohlmann as M. Ballard
* Robert Favart as Hotel Manager
* Guido Lorraine as M. Marcel
* Ferdy Mayne as M. Dufond

==Musical numbers==
The Musical Supervision is credited to “M.S.I.” Herbert Spencer and Earle Hagen.
 
Incidental Music Composed and Conducted by Robert Farnon.

*“Gentlemen Marry Brunettes” (music by Herbert Spencer and Earle Hagen, lyrics by Richard Sale) - Sung by Chorus.
:(note: screen credit gives “Sung by Johnny Desmond”, but the song is only heard in a male choral arrangement)
*“I Wanna Be Loved By You” (music by Herbert Stothart and Harry Ruby, lyrics by Bert Kalmar) - Sung by Jane Russell, Jeanne Crain (dubbed by Anita Ellis) and Rudy Vallee.
*“Have You Met Miss Jones?” (music by Richard Rodgers, lyrics by Lorenz Hart) - Sung by Rudy Vallee, Jane Russell, Jeanne Crain (dubbed by Anita Ellis), Scott Brady (dubbed by Robert Farnon) and Alan Young, Danced by Jane Russell and Jeanne Crain.
*“My Funny Valentine” (music by Richard Rodgers, lyrics by Lorenz Hart) - Sung by Jeanne Crain (dubbed by Anita Ellis) and Alan Young.
*"I’ve Got Five Dollars” (music by Richard Rodgers, lyrics by Lorenz Hart) - Sung by Jane Russell and Scott Brady (dubbed by Robert Farnon).
*“Daddy” (music & lyrics by Bobby Troup) - Sung and Danced by Jane Russell and Jeanne Crain (dubbed by Anita Ellis).
*“Miss Annabelle Lee” (music & lyrics by Sydney Clare and Lou Pollock) - Sung by Chorus, Danced by Jane Russell, Jeanne Crain and Chorus.
*“You’re Driving Me Crazy” (music & lyrics by Walter Donaldson) - Sung and Danced by Jane Russell and Jeanne Crain (dubbed by Anita Ellis). Thomas ‘Fats’ Harry Brooks, and Andy Razaf) - Sung and Danced by Alan Young, Jane Russell, Jeanne Crain (dubbed by Anita Ellis) and Chorus.

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 