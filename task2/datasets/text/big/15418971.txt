The Devil Worshippers
 
{{Infobox film
| name           = The Devil Worshippers
| image          = 
| caption        = 
| director       = Muhsin Ertuğrul
| producer       = 
| writer         = Marie Luise Droop Karl May
| starring       = Carl de Vogt Meinhart Maur Béla Lugosi
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = Weimar Republic
| language       = Silent
| budget         = 
| gross          = 
}}
 silent German film directed by the Turkish director Muhsin Ertuğrul, written by Marie Luise Droop, and featuring Carl de Vogt in the title-role of Kara Ben Nemsi. Later horror-star Béla Lugosi is being seen in one of his first supporting roles in a film. The film was an adaptation of a chapter from the novel Durch die Wüste (Through the Desert) of German author  Karl May.

This film was the first of a trilogy of the production company "Ustad-Film" with main actor Carl de Vogt, but only came as third into the cinemas. In several scenes, this black-and-white film has some coloring, e.g. blue for night scenes.

The film is said to have premiered on 2 January 1921 at "Vaters Lichtspiele" at Würzburg but the first showing is only documented for 14 January 1921 at Wilhelmsburg.

The film is now considered to be lost film|lost.

== Cast ==
* Carl de Vogt as Kara Ben Nemsi
* Meinhart Maur as Hadschi Halef Omar
* Tronier Funder as Officer
* Béla Lugosi
* Fred Immler
* Ilja Dubrowski
* Gustav Kirchberg
* Erwin Baron

==See also==
* Karl May movies
* Béla Lugosi filmography

==External links==
* 

 
 
 
 
 
 
 
 


 