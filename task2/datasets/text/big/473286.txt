Anything Else
{{Infobox film name           = Anything Else image          = Anythingelseposter.jpg caption        = Theatrical release poster director       = Woody Allen writer         = Woody Allen starring       = Woody Allen Jason Biggs Stockard Channing Danny DeVito Jimmy Fallon Christina Ricci cinematography = Darius Khondji producer       = Letty Aronson editing        = Alisa Lepselter distributor    = DreamWorks (USA) Metro-Goldwyn-Mayer (UK DVD) released       =   runtime        = 108 minutes country        = United States language       = English budget         = $18 million gross          = $13,585,075
|}}
Anything Else is a 2003 romantic comedy film. The film was written and directed by Woody Allen, produced by his sister Letty Aronson, and stars Jason Biggs, Christina Ricci, Woody Allen, Stockard Channing, Danny DeVito, Jimmy Fallon, Erica Leerhsen and KaDee Strickland.

Anything Else was the opening-night selection at the 60th annual Venice International Film Festival.

==Plot==

Jerry Falk (Biggs), an aspiring writer living in New York City, has a girlfriend Brooke (Strickland). He falls in love with Amanda (Ricci) and begins an affair. After listening to outlandish excuses about his behavior, Brooke dumps him. Jerry turns to an aging, struggling artist (Allen) who acts as his oracle, which includes trying to help sort out Jerry’s romantic life.

==Casting==
*Jason Biggs as Jerry Falk
*Christina Ricci as Amanda Chase
*Woody Allen as David Dobel
*Stockard Channing as Paula Chase, Amandas mother
*Danny DeVito as Harvey Wexler
*Jimmy Fallon as Bob
*KaDee Strickland as Brooke
*Adrian Grenier as Ray Polito
*David Conrad as Dr. Phil Reed

==Critical reception==
The film received generally mixed reviews from critics. The review aggregator Rotten Tomatoes reported that the film received 40 percent positive reviews, based on 129 reviews.  Metacritic reported the film had an average score of 43 out of 100, based on 37 reviews.  Leonard Maltin, in his movie and video guide, gave the film a “BOMB” rating (the only Allen-directed film he has ever rated BOMB), and called it “Allen’s all-time worst”. However, the reception was not entirely negative; in August 2009, it was cited by Quentin Tarantino as one of his favorite 20 films since 1992. 

==References==
 

==External links==
* 
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 