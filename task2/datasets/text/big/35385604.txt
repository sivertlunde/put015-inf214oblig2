Burning Man (film)
 
 
{{Infobox film
| name           = Burning Man
| image          = Burning Man (film) film poster.jpg
| caption        = Film poster
| director       = Jonathan Teplitzky
| producer       = Andy Paterson Jonathan Teplitzky
| writer         = Jonathan Teplitzky
| starring       = Matthew Goode Bojana Novakovic
| music          = Lisa Gerrard
| cinematography = Garry Phillips
| editing        = Martin Connor
| distributor    = 
| released       =  
| runtime        = 109 minutes
| country        = Australia
| language       = English
| budget         = 
}}

Burning Man is a 2011 Australian drama film written and directed by Jonathan Teplitzky.    

==Plot==
Burning Man tells the story of Tom (Matthew Goode), a British chef in a Bondi restaurant, who seems to have chosen to disobey his boss, and his actions are tolerated by everyone around him. As Tom descends into darkness, pieces of a different story start to emerge. Every woman around him tries, in different ways, to help Tom put himself back together.

==Cast==
* Matthew Goode as Tom
* Bojana Novakovic as Sarah
* Essie Davis as Karen
* Rachel Griffiths as Miriam
* Kerry Fox as Sally Anthony Hayes as Brian
* Jack Heanly as Oscar
* Kate Beahan as Lesley
* Gia Carides as Carol
* Marta Dusseldorp as Lisa
* Robyn Malcolm as Kathryn

==Reception and accolades==

Burning Man currently holds a Rotten Tomatoes approval rating of 74%.
 SBS awarded the film four stars out of five, observing that the film is an "experiential kaleidoscope of sex, love, and the numbing nihilism that accompanies a traumatic event." She also notes that "Burning Man manages to bring energy, originality and depth to a storyline that is itself something of a trope: ‘The Cancer Film’." 

{| class="wikitable"
|-
! Award
! Category
! Subject
! Result
|- AACTA Awards|AACTA Award  (2nd AACTA Awards|2nd)  AACTA Award Best Film Andy Paterson
| 
|- Jonathan Teplitzky
| 
|- AACTA Award Best Direction
| 
|- AACTA Award Best Original Screenplay
| 
|- AACTA Award Best Actor Matthew Goode
| 
|- AACTA Award Best Supporting Actress Essie Davis
| 
|- AACTA Award Best Cinematography Garry Phillips
| 
|- AACTA Award Best Editing Martin Connor
| 
|- AACTA Award Best Sound David Lee
| 
|- Gethin Creagh
| 
|- Andrew Plain
| 
|- AACTA Award Best Production Design Steven Jones-Evans
| 
|- AACTA Award Best Costume Design Lizzy Gardiner
| 
|- Australian Film Critics Association Awards Best Actor Matthew Goode
| 
|- Best Editing Martin Connor
| 
|- Australian Screen ASE Award Best Editing in a Feature Film
| 
|- Australian Screen Sound Guild Award Best Sound Mixing David Lee
| 
|- Gethin Creagh
| 
|- Andrew Plain
| 
|- Australian Writers AWGIE Award Feature Film - Original Jonathan Teplitzky
| 
|- Film Critics Circle of Australia Awards Best Film Andy Paterson
| 
|- Jonathan Teplitzky
| 
|- Best Director
| 
|- Best Screenplay
| 
|- Best Actor Matthew Goode
| 
|- Best Actress Bojana Novakovic
| 
|- Best Supporting Actress Essie Davis
| 
|- Best Young Actor Jack Heanly
| 
|- Best Cinematography Garry Phillips
| 
|- Best Editor Martin Connor
| 
|- Best Music Score Lisa Gerrard
| 
|- Australian Screen Music Award Feature Film Score of the Year
| 
|-
|}

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 