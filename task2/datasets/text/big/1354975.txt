Changing Lanes
{{Infobox film
| name           = Changing Lanes
| image          = Changing Lanes poster.JPG
| caption        = Theatrical release poster
| director       = Roger Michell
| producer       = Scott Rudin Scott Aversano
| writer         = Chap Taylor Michael Tolkin
| starring       = {{Plainlist | 
* Ben Affleck
* Samuel L. Jackson
* Toni Collette
* Sydney Pollack
* William Hurt
* Amanda Peet
}}
| cinematography = Salvatore Totino
| editing         = Christopher Tellefsen
| music          = David Arnold
| distributor    = Paramount Pictures
| released       = April 12, 2002
| runtime        = 99 minutes
| country        = United States
| language       = English
| budget         = $45 million 
| gross          = $94,935,764
}}
Changing Lanes is a 2002 drama film|drama-thriller film directed by Roger Michell and starring Ben Affleck and Samuel L. Jackson. The film was released on April 12, 2002 in North America by Paramount Pictures.

==Plot==
A successful New York City Lawyer|attorney, Gavin Banek, is in a rush to file a power of appointment, which will prove a dead man signed his foundation over to Baneks law firm. He has a collision on FDR Drive with another car, belonging to an insurance salesman, Doyle Gipson, who is also in a rush to a hearing to try to gain custody of his children and to prevent his estranged wife from taking them to Oregon. Banek tries to brush Gipson off with a blank check thereby disobeying the law. After Gipson refuses to accept the check and voices his desire to "do the right thing", that is, filing a police report and insurance claim, Banek strands Gipson, telling him, "better luck next time". After arriving to the court late, Gipson learns that it proceeded without him and that it did not go in his favor.

Unfortunately for Banek, he dropped the crucial power of appointment file at the scene of the accident, and the judge gives him until the end of the day to re-obtain the papers and present them. Gipson, who took the papers, is in a state of dilemma on whether to return the file especially after the events of the day. On the other hand, Banek desperate to get his papers back, goes to someone skilled with computers and gets him to switch off Gipsons credit. Gipson needed credit for a loan so he could buy a house for his family, and he becomes further enraged, determined to make life difficult for Banek.

Both men continue to do morally reprehensible things in an attempt to one-up each other, and eventually they begin to question their actions. Though it is made clear that Banek and Gipson are radically different, they both have an angry, vengeful streak, each capable of abandoning his morals just to punish the other. The film ends with both men having a new outlook on life, concentrating on ethics and the moral implications of their actions. Ultimately the two men apologize to each other and Gipson returns the file, but it looks to be too late for both of what they were trying to do. Banek ends up using the file to force his boss to do the right thing and plans to represent Gipson pro bono so he can get the house he wants. Banek also visits Gipsons wife to explain everything to her, knowing he owes Gipson that much. The movie ends with Gipsons wife and children smiling at him from across the street.

==Cast==
* Ben Affleck as Gavin Banek
* Samuel L. Jackson as Doyle Gipson
* Toni Collette as Michelle
* Sydney Pollack as Stephen Delano
* Richard Jenkins as Walter Arnell
* William Hurt as Doyles Sponsor
* Amanda Peet as Cynthia Delano Banek
* Matt Malloy as Ron Cabot
* Tina Sloan as Mrs. Delano
* Ileen Getz as Ellen
* Bruce Altman as Terry Kaufman
* Dylan Baker as Finch

==Reception==

===Box office===
The movie was a box office success, with a budget of $45,000,000, it grossed $66,818,548 in the United States and $28,117,216 internationally, for a total gross of $94,935,764.   

===Critical response===
The film received favorable reviews from critics. Review aggregation website Rotten Tomatoes gives the film a score of 77% based on reviews from 151 critics, with an average score of 7/10.  
Metacritic gave it an average score of 69/100 from the 36 reviews it collected. 

Roger Ebert of the Chicago Sun-Times praised the film, calling it one of the years best. 

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 