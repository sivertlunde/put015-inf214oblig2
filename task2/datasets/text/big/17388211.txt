Adam and Eve (1953 film)
{{Infobox film
| name           = Adam og Eva
| image          = Adamogevaposter.jpg
| image size     =
| caption        = Theatrical release poster by Kai Rasch
| director       = Erik Balling
| producer       = John Hilbert
| writer         = Erik Balling
| narrator       =
| starring       = Louis Miehe-Renard Sonja Jensen
| music          = Hans Schreiber
| cinematography = Poul Pedersen
| editing        =
| distributor    = Nordisk Film Kompagni
| released       = 1953
| runtime        = 99 minutes Denmark
| Danish
| budget         =
| preceded by    =
| followed by    =
}} 1953 Denmark|Danish family comedy written and directed by Erik Balling. The film was awarded the 1954 Bodil Award for Best Danish Film and Per Buckhøj won the Bodil Award for Best Actor for his role as the zealous schoolteacher.

==Plot==
On the way home from a conference in Paris, Mr. Johansen accidentally finds himself in possession of an insignificant little French book. He has no idea where the book came from or what it concerns, but he decides he should secretly smuggle it through customs. Thereafter, the book passes through the hands of 5 different people, and causes unexpected conflicts, suspicions and misunderstandings for each of them.

==Cast==
* Louis Miehe-Renard – Adam
* Sonja Jensen – Eva
* Per Buckhøj – Sven Aage Johansen
* Inger Lassen – Adams mother
* Gunnar Lauring – Claus father
* Beatrice Bonnesen – Claus mother
* Bertel Lauring – Claus
* Einar Juhl – Adams Teacher
* Birgitte Reimer – Peters secretary
* Poul Reichhardt – Peter
* Astrid Villaume – Peters wife
* Asbjørn Andersen – Carl Henriksen
* Karin Nellemose – Carls wife
* Bjørn Watt Boolsen – Larsen
* Preben Lerdorff Rye – Burglar
* Lis Løwert – Prostitute
* Poul Müller – Antique dealer
* Birgitte Federspiel – Antique dealers wife
* Emil Hass Christensen – Reverend
* Henning Moritzen – Customs agent
* John Wittig – Aagaard
* Bendt Rothe – Defender
* Karl Stegger – Man on the street
* Ellen Margrethe Stein – Judge Lauersen
* Ebbe Langberg – Classmate
* Jørgen Buckhøj – Classmate
* Otto Detlefsen – Judge

==External links==
*  
*  at the Danish Film Database (in Danish)
*[http://dnfx.dfi.dk/pls/dnf/pwt.page_setup?p_pagename=dnffuldvis&p_parmlist=filmid=1880 Adam og Eva at Det Danske Filminstitut (in Danish)

 

 
 
 
 
 
 
 

 
 