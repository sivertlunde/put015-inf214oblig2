Cosi (film)
 
 
 
{{Infobox film
| name = Cosi
| image = CosiPoster1996.jpg
| caption = Cosi poster
| director = Mark Joffe
| screenplay = Louis Nowra
| based on = Così by Louis Nowra
| starring = {{plainlist|
*Ben Mendelsohn
*Barry Otto
*Toni Collette
*Rachel Griffiths
*Aden Young
*Colin Friels
*Jacki Weaver
*David Wenham
}}
| producer = {{plainlist|
*Richard Brennan
*Timothy White
}}
| music = Stephen Endelman
| cinematography = Ellery Ryan
| editing = Nicholas Beauman
| distributor = {{plainlist|
*Australian Film Finance Corporation
*Meridian Films
*Miramax Films
*Smiley Productions
}}
| released = 28 March 1996
| runtime = 100 minutes Australia
| language = English
| budget = A$3.5 million 
| gross = A$2,896,980 (Australia)|
}}
 Australian comedy the play it is based on.

==Plot summary== Mozart opera Così fan tutte, an elaborate, demanding piece of theatre, an opera in Italian. And it is going to be performed by a cast that he must select from among the patients, who only speak English.

One of the patients, Roy ( ), Ruth ( ) and Doug (David Wenham). The musical director is Zac (Colin Hay). The enthusiasm of Roy infects the group, and they charge headlong into a memorable production.

Alongside the story of Lewis, the theme of Così fan tutte is explored as it relates to his personal life. Lewiss relationship with his girlfriend Lucy (Rachel Griffiths), already under pressure, is not helped by a friend called Nick (Aden Young), who seems more interested in testing Lucys faithfulness than anything else.
 Plenty Mental Hospital in suburban Melbourne in 1971. 

==Cast==
*Ben Mendelsohn as Lewis
*Barry Otto as Roy
*Toni Collette as Julie
*Rachel Griffiths as Lucy
*Aden Young as Nick
*Colin Friels as Errol
*Jacki Weaver as Cherry
*Pamela Rabe as Ruth
*Paul Chubb as Henry
*Colin Hay as Zac
*David Wenham as Doug

(Mendelsohn, Otto and Wenham are the only cast members who have performed in the original stage play, portraying the same characters that they do on screen.)

==Production==
Cosi was directed by Mark Joffe, and produced by Richard Brennan and Timothy White.

== Critical reception ==
In 1996, Cosi was the 3rd most popular Australian film at the Australian Box Office. As at 2010, Cosi ranked 71 on the list of Top 100 Australian feature films of all time as compiled by Screen Australia. 

David Stratton, writing in Variety (magazine)|Variety, described Cosi as "fast, funny and cleverly acted".    He also said it was "warm, generous, sentimental and expert entertainment."  The curator at Australian Screen Online said it "has a likeable humour, appealing characters and a compassionate heart. Its not really about mental illness so much as a tribute to the healing power of performance, and the theatre in general."   

Not all reviews were positive. James Berardinelli described Cosi as "a half-baked amalgamation of A Midwinters Tale and Shine (film)|Shine, and doesnt excel as either a comedy or a drama."  Cinephilia described Cosi as "a concatenation of caricatures in a predictable story of plucky determination and treacly redemption." 

==Box office==
Cosi grossed $2,896,980 at the box office in Australia. 

==See also==
 
* List of Australian films
* List of films shot in Sydney

==References==
 

==Further reading==
* Gillard, Garry (2001) H231 Australian Cinema: Unit Information and Study Guide. Perth, Murdoch University Press, p.&nbsp;65.
* Malone, P. "Cosi" – Review in Cinema Papers, No. 109, April 1996, p.&nbsp;41.
* ODometer, M.  , 1996
* ORegan, T, (1996) Australian National Cinema, New York/London, Routledge.
* Smith, M. "Running the Gamut" in Cinema Papers, No. 109, April 1996, p.&nbsp;6.

==External links==
*  
*  
*   by Joanne Ladiges

 

 
 
 
 
 
 
 
 