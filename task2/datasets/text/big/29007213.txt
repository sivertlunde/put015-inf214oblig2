In a Better World
 
 
{{Infobox film
| name           = In a Better World
| image          = In a Better World.jpg
| caption        = Theatrical release poster
| director       = Susanne Bier
| producer       = Sisse Graum Jørgensen
| writer         = Susanne Bier Anders Thomas Jensen
| starring       = Mikael Persbrandt Trine Dyrholm Ulrich Thomsen
| music          = Johan Söderqvist
| cinematography = Morten Søborg
| editing        = Pernille Bech Christensen Morten Egholm
| studio         = Zentropa
| distributor    = 
| released       =  
| runtime        = 118 minutes  
| country        = Denmark Sweden
| language       = Danish Swedish English Arabic Catalan Spanish
| budget         = DKK30 million (US$5.5 million   )
| gross          = US$9,629,826 
}} drama Thriller thriller film written by Anders Thomas Jensen and directed by Susanne Bier. The film stars Mikael Persbrandt, Trine Dyrholm, and Ulrich Thomsen in a story which takes place in small-town Denmark and a refugee camp in Africa.
 2011 Golden Best Foreign Best Foreign Language Film at the 83rd Academy Awards. 

==Plot==
Anton (Mikael Persbrandt) is a Swedish doctor who commutes between his home in Denmark and his work in a Sudanese refugee camp. In Sudan, he often treats female patients who are the victims of a sadistic warlord. Anton is married to Marianne (Trine Dyrholm), but they are separated, and struggling with the possibility of divorce over an affair that Anton had with another woman. They have two young sons, the older one being 12-year-old Elias (Markus Rygaard).

Christian (William Jøhnk Juels Nielsen), who has just moved from London with his father, Claus (Ulrich Thomsen), is a new boy at Elias school. Christians mother recently died from cancer, and Christian blames his father for lying to him that she would get well, and that, in a late stage of her disease, he "wanted" her to die. Elias is bullied at school, until he is defended by Christian, who assaults the main bully and threatens him with a knife. Christian gives Elias the knife, and both boys lie to the police, and their parents, about the incident.

When Anton separates his younger son from another child, while they are fighting at a playground, the father of the other child, a mechanic, tells Anton not to touch his child and slaps Anton in the face. Anton later visits the mechanic at his work, accompanied by his children and Christian, to discuss the matter, and to show the children that he is not afraid of the man. The mechanic slaps Anton several more times, who does not flinch from the blows.

Later, in Sudan, the warlord comes to Antons hospital for treatment of his wounded leg. To the horror of his staff and others, Anton treats the man, after demanding that no weapons, and only two of his guards, are allowed inside the hospital. However, after the warlord shows contempt for one of his victims, Anton drags him away from the clinic, allowing him to be beaten to death by local people.

In Denmark, Christian and Elias decide to make a bomb to destroy the mechanics car, on a Sunday morning so no passers-by are hurt. However, with the fuse already burning, they see two joggers approaching (a mother and her young daughter), and Elias leaves his protected position to warn them. He is knocked unconscious, but saves the joggers from harm. Christian is questioned by the police and then released, with the incident being addressed as an extreme case of vandalism. He goes to the hospital to visit Elias, but Marianne does not let him see the boy, instead telling him that he has killed her son. Christian, believing that Elias is dead, climbs to the roof of a silo, contemplating suicide, but is rescued by Anton. Christian is relieved that Elias is doing well, and he is now allowed to visit him.

Christian reconciles with his father, and Anton and Marianne have sex, apparently resuming their marriage.

==Cast==
 
* Mikael Persbrandt as Anton
* Trine Dyrholm as Marianne
* Ulrich Thomsen as Claus
* William Jøhnk Juels Nielsen as Christian
* Markus Rygaard as Elias
* Simon Maagaard Holm as Sofus
* Kim Bodnia as Lars
* Wil Johnson as doctor
* Eddie Kimani as doctor
* Emily Mglaya as nurse
* Gabriel Muli as interpreter
* June Waweru as patient
* Mary Hounu Moat as patient
* Synah Berchet as old lady
* Elsebeth Steentoft as Signe
* Satu Helena Mikkelinen as Hanna
* Camilla Gottlieb as Eva
* Birthe Neumann as Mariannes colleague
* Paw Henriksen as policeman
* Jesper Lohmann as policeman
* Bodil Jørgensen as head teacher
* Lars Kaalund as Lars colleague
* Lars Bom as investigator
 

==Production==
The idea for the film originated when Bier and screenwriter Anders Thomas Jensen had discussions about the perception of Denmark as a very harmonious society. They then wanted to write a story where dramatic turns of events would disrupt the image of a place perceived as blissful.    The development of the narrative started with the character that would be played by Mikael Persbrandt, and the idea of an idealist who becomes the victim of an assault.  The two writers had no specific actors in mind while developing the story but rewrote details when the leads had been cast.  The film was produced by Denmarks Zentropa in co-production with DR2 and Swedens Memfis Film, Sveriges Television, Trollhättan Film and Film i Väst.    It received seven million Danish kroner from the Danish Film Institute as well as funding from the Swedish Film Institute, the pan-Nordic Nordisk Film- & TV-Fond and the European Unions MEDIA Programme.     Filming started at the end of August 2009 and took place on Funen and in Kenya. 

==Themes==
Director Susanne Bier said: "Our experiment in this film is about looking at how little it really takes before a child – or an adult – thinks something is deeply unjust. It really doesnt take much, and I find that profoundly interesting. And scary."   

==Reception==

===Critical response===
Review aggregator Rotten Tomatoes reports that 77% out of 114 professional critics gave the film a positive review, with the site consensus stating that "In a Better World is a sumptuous melodrama that tackles some rather difficult existential and human themes." 

Kim Skotte called the film a "powerful and captivating drama" in Politiken. Out of the four collaborations between Jensen and Bier, he considered In a Better World to be the one most similar to Jensens solo films and compared the combination of biblical themes and high entertainment value to Jensens 2005 film Adams Apples. Skotte also praised the acting performances: "Mighty Mikael Persbrandt shows that he is Scandinavias most charismatic actor right now. Trine Dyrholms scenes are fewer, but in a split second she can dramatise the canvas to make the throat lace itself. Also Ulrich Thomsen is good as grief-stricken single father. With her successful directing of the two boys Markus Rygaard and William Jøhnk Nielsen, Bier adds a new chapter to her already extensive resumé of top tuned skills." 

Peter Nielsen of Dagbladet Information called In a Better World "in all ways a successful film", and although there "is no doubt that Susanne Bier can tell a good story", he was not entirely convinced: "She can seduce, and she can push the completely correct emotional buttons, so that mothers as well as fathers hearts are struck, but she doesnt earnestly drill her probe into the meat." 

===Accolades=== Best Foreign Best Foreign Language Film at the 83rd Academy Awards.  The film also won for Best Foreign Language Film at the 68th Golden Globe Awards.  Additionally, it won:

* Rome International Film Festival 2010:   
** Marc’Aurelio Audience Award for Best Film
** MarcAurelio Grand Jury Award
* Sevilla Festival de Cine 2010: 
** Best Director
** Best Screenplay
* Tallinn Tarta, Black Nights Film Festival 2010: 
** Best Male Actor
* Thessaloniki International Film Festival 2010: 
** Creative Excellence Award

==See also==
* List of submissions to the 83rd Academy Awards for Best Foreign Language Film
* List of Danish submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*   (US)
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 