A Woman With a Bad Reputation
A Woman With a Bad Reputation (  "Imraah sayiah al-samah" ) is a  1973 Egyptian film directed by Henry Barakat.  The main actress is Shams al-Baroudi. Anderson, Lisa. "Egypts cultural shift reflects Islams pull." Chicago Tribune. March 21, 2004. p.  . Retrieved on February 21, 2013. 

A young man asks his wife to dance with his boss at a party. The woman gets into an affair with the boss and commits infidelity. Her marriage dissolves and her life worsens. Her son has an illness, so the wife is forced to accept gifts from the boss. 

Lisa Anderson of the Chicago Tribune. uses the film as an example of more liberal filmmaking in Egypt prior to an increase in social conservatism in society. At the party in the beginning of the film, the women wear hot pants and miniskirts. The partygoers dance, smoke cigarettes, and drink alcohol. None of the women are in hijab. 

==See also==
 
* Cinema of Egypt

==References==
* Armes, Roy. Dictionary of African Filmmakers. Indiana University Press, July 11, 2008. ISBN 0253000424, 9780253000422.

==Notes==
 

 
 
 
 

 