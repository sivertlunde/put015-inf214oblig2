The Battle of the Somme (film)
 
 
 
{{Infobox film
| name           = The Battle of the Somme
| image          =  
| caption        = Yorkshire Evening Post advert for the film, 1916
| director       =
| producer       = W. F. Jury
| writer         =
| music          = J. Morton Hutcheson (original 1916 medley) Laura Rossi (2006)
| cinematography = G. H. Malins J. B. McDowell
| editing        = Charles Urban G. H. Malins
| distributor    = British Topical Committee for War Films
| released       =  
| runtime        = 77 minutes
| country        = United Kingdom English intertitles
| budget         =
}}
 documentary and propaganda film, shot by two official cinematographers, Geoffrey Malins and John McDowell. The film depicts the British Army in the preliminaries and early days of the Battle of the Somme (1 July – 18 November 1916). The film had its première in London on 10 August 1916 and was released generally on 21 August. The film depicts trench warfare, showing marching infantry, artillery firing on German positions, British troops waiting to attack on 1 July, treatment of wounded British and German soldiers, British and German dead and captured German equipment and positions. A scene where British troops crouch in a ditch then "go over the top" was staged for the camera behind the lines.
 Memory of the World Register. In 2005, the film was digitally restored and in 2008, was released on DVD. The Battle of the Somme is an early example of film propaganda, an historical record of the battle and a popular source of footage illustrating the First World War.

==Film==
===Content===
  29th Division artillery bombardment Hawthorn Ridge 1 July 1916, with some re-enactments and shows the recovery of British wounded and German prisoners. The fourth part shows more scenes of British and German wounded, the clearing of the battlefield and some of the aftermath. The final part shows scenes of devastation, including the ruins of the village of Mametz, Somme|Mametz, British troops at rest and preparations for the next stage of the advance. 

==Production==
===Background=== Ernest Brooks. |group="Note"}} ]] British & Colonial film company, volunteered to replace Tong and left for France on 23 June 1916.  On 24 June, the British Army began the preparatory artillery bombardment of German positions for the Battle of the Somme. 

===Photography=== Beaumont Hamel, 7th Division (XV Corps).  Before the battle, Malins worked at the north end of the British Somme sector, photographing troops on the march and heavy artillery west of Gommecourt, Pas-de-Calais|Gommecourt. The commander of the 29th Division, Major-General De Lisle suggested to Malins that a view of the bombardment of Beaumont Hamel could be had at Jacobs Ladder near White City, an abrupt drop down the side of a valley, with about   cut into the ground.  German shells were falling as Malins made his way to Lanwick Street Trench. Malins had to rise above the parapet to remove sandbags and then set up his camera, which was camouflaged with sackcloth. Malins then returned to film the speech by de Lisle to the 2nd Royal Fusiliers, after which Malins heard of the   postponement of the battle. Malins returned to White City, to film the bombardment of the Hawthorn Redoubt by trench mortars, during which there were three misfires, which caused the destruction of a trench mortar position nearby. 
 La Boisselle, Third Army St Pol. Albert and Fricourt and Capture of Mametz|Mametz. 

===Editing===
  negative on cut from British Expeditionary Montreuil and at the Fourth Army headquarters. The commander Lieutenant-General Henry Rawlinson, 1st Baron Rawlinson|H. Rawlinson said "some of it is very good but it cut out many of the horrors in dead and wounded".   The film was shown to the Secretary of State for War, David Lloyd George on 2 August and members of the cinema trade late on 7 August, the day that the film received final approval for release. 

===Release===
 
The film comprises five reels and is   long.  The first screening took place on 10 August 1916 at the Scala Theatre, to an audience of journalists, Foreign Office officials, cinema trade figures and officers of the Imperial General Staff. The screening was preceded by the reading of a letter from Lloyd George, exhorting the audience to "see that this picture, which is in itself an epic of self-sacrifice and gallantry, reaches everyone. Herald the deeds of our brave men to the ends of the earth. This is your duty." 
 cinemas and Royal Family received a private screening at Windsor Castle on 2 September; the film was eventually shown in more than eighteen countries. 

==Reception==
===Britain===
  from sequence 34: British Tommies rescuing a comrade under shell fire. (This man died 30 minutes after reaching the trenches.)  |group="Note"}} ]]

The popularity of the film was unprecedented and cinemas played the film for longer runs than usual, often to packed houses, while some arranged additional screenings to meet demand.  The film is thought to have achieved attendance figures of twenty million in its first six weeks of release.  The film also attracted more middle-class audiences, some of whom had never been to a cinema before.  William Jury, as the films booking director, initially charged exhibitors £40, with the fee decreasing by £5 a week; after two months, a reduced fee of £6 for three nights put the film within the price range of village halls wishing to show the film.  By October 1916, the film had been booked by more than two thousand cinemas in Britain, earning over £30,000. 
British authorities showed the film to the public as a morale-booster and in general it met with a favourable reception. The Times reported on 22 August that

 }}
 Dean of Durham protested "against an entertainment which wounds the heart and violates the very sanctity of bereavement". Others complained that such a serious film shared the cinema programme with comedy films.  On 28 August, the Yorkshire Evening Post printed the comment "If the exhibition of this Picture all over the world does not end War, God help civilisation."  The film was shown to British troops in France from 5 September; Rowland Feilding, an officer who saw the film in a muddy field near Morlancourt, described it as "a wonderful and most realistic production" but criticised the film for failing to record the sound of rifles and machine-guns, although there was an artillery accompaniment from guns firing nearby.  Feilding suggested that the film could reassure new recruits, by giving them some idea of what to expect in battle; the intertitles were forthright, describing images of injury and death. 

===International===
The film was shown in New Zealand in October 1916. On 12 October, the Wellington The Evening Post (New Zealand)|Evening Post ran an advertisement for the film, describing it as "the extraordinary films of the big push and "an awe-inspiring, glorious presentation of what our heroes are accomplishing today".  In a review published on 16 October, it was written that "these pictures of the Battle of the Somme are a real and valuable contribution to the nations knowledge and a powerful spur to national effort".  The film premièred in Australia on 13 October at Hoyts|Hoyts Picture Theatre in Melbourne. The Melbourne The Argus (Australia)|Argus considered that after the attack sequence, "... you are not in a Melbourne picture theatre, but in the very forefront of the Somme battle" and called the film "this moving reality of battle". 

The film and its reception was of great interest to Germans, who were able to read a report from a neutral reporter in London, filed in the  . British cinema was said to be taking a serious view of the war, helped by the government, which had arranged for the production of "real war films", The Battle of the Somme was "absorbing" and had attracted huge audiences. German troops recovered a letter from a British civilian, who had seen the film on 26 August and written to a soldier in France, describing the issue of a leaflet to each  filmgoer, which stated that the film was not entertainment but an official film. Having viewed the film, the writer was sure that it would enlighten the audience, in a way never before achieved. 

===Other films===
During the second week of August the King, Prime Minister and War Minister toured the Western Front and were filmed by Malins. The War Office released the film in October as The King Visits His Armies in the Great Advance.  Malins made a third film The Battle of the Ancre and the Advance of the Tanks which opened in January 1917.  |group="Note"}} In the same month a German film With our Heroes on the Somme was shown but "lacked the immediacy" of the British film, being re-enactments at training establishments behind the lines, which received a lukewarm reception.  A booklet to accompany the film proclaimed that it  depicted "the German will in war" and implied that the enemies of Germany were using colonial troops to undermine "German honour".  Malins final feature film The German Retreat and the Battle of Arras was released in June 1917, showing the destruction of Péronne and the first British troops across the Somme, following-up the German withdrawal; the film failed to attract the audiences of 1916. 

==Preservation== nitrate negative archive master acetate safety television documentaries, Memory of the World Register for the preservation of global documentary heritage. The film was described by UNESCO as a "compelling documentary record of one of the key battles of the First World War   the first feature-length documentary film record of combat produced anywhere in the world" and had "played a major part in establishing the methodology of documentary and propaganda film."  
 restoration project Armistice in 1918. The DVD included the score by Laura Rossi, an accompanying 1916 musical medley, a commentary by Roger Smither, a film archivist at the Imperial War Museum and interviews with Smither, Rossi, Toby Haggith (film archivist) and Stephen Horne (silent film musician) on the reconstruction of the contemporary medley; film fragments and missing scenes were also included in the DVD. 

==See also==
  The Somme (1927) The Somme – From Defeat to Victory (2006)

==Notes==
 

==Footnotes==
 

==References==
 
;Books
*  
|title=Beaumont Hamel |last=Cave |first=N. |authorlink= |year=1994 |publisher=Leo Cooper |location=Barnsley |edition= |isbn=0-85052-398-2}}
*  
|title=Through German Eyes: The British and the Somme 1916 |last=Duffy |first=C. |year=2006 |authorlink=Christopher Duffy|publisher=Weidenfeld & Nicholson |location=London |edition=Phoenix 2007 |isbn=978-0-7538-2202-9}}
*  
|title=War Letters to a Wife: France and Flanders, 1915–1919 |last=Feilding |first=R. |year=1929 |location=London |publisher=Medici |oclc=752947224}}
*  
|title=How I filmed the War : a Record of the Extraordinary Experiences of the Man Who Filmed the Great Somme Battles, etc |last=Malins |first=G. H. |authorlink= |year=1920 |publisher=Herbert Jenkins |location=London |edition= |url=http://ia700307.us.archive.org/12/items/ifilmedwar00maliuoft/ifilmedwar00maliuoft.pdf |accessdate=5 October 2013 |oclc=246683398}}
*  
|title=Bloody Victory: The Sacrifice on the Somme and the Making of the Twentieth Century |last=Philpott |first=W. |authorlink= |year=2009 |publisher=Little, Brown |location=London |edition=1st |isbn=978-1-4087-0108-9}}
*  
|last1=Prior |first1=R. |last2=Wilson |first2=T. |title=The Somme |publisher=Yale University Press |year=2005 |isbn=0-300-10694-7}}
*  
|title=Official British Film Propaganda During the First World War |last=Reeves |first=N. |authorlink= |year=1986 |publisher=Croom Helm |location=London |edition= |isbn=0-70994-225-7}}
*  
|title=Ghosts on the Somme: Filming the Battle, June–July 1916 |last1=Fraser |first1=Alastair |last2=Robertshaw |first2=Andrew |last3=Roberts |first3=Steve |authorlink= |year=2009 |publisher=Pen and Sword Books|location=Barnsley |isbn=1-84415-836-5}}
*  
|title=The Somme |last=Sheffield |first=G. |publisher=Cassell |year=2003 |location=London |isbn=0-304-36649-8}}
*  
|title=The Battle of the Somme (DVD viewing guide) |last=Smither |first=R. B. N. |year=2008 |publisher=Imperial War Museum |edition=2nd (rev) |location=London |url=http://www.iwm.org.uk/upload/package/100/Somme%20DVD/documents/viewing_guide.pdf |accessdate=6 October 2013 |isbn=0-90162-794-1}}
*  
|title=Memory of the World: The Battle of the Somme |author=UNESCO |year=2012 |publisher=UNESCO |url=http://www.unesco.org/new/en/communication-and-information/flagship-project-activities/memory-of-the-world/register/full-list-of-registered-heritage/registered-heritage-page-8/the-battle-of-the-somme/#c183653 |accessdate=6 October 2013 |isbn=978-92-3-104237-9}}

;Journal articles
*  
|title=Battle of the Somme: British war-propaganda |last=Badsey |first=S. |year=1983 |journal=Historical Journal of Film, Radio and Television |volume=3 |issue=2 |publisher=Routledge |url=http://www.tandfonline.com/doi/abs/10.1080/01439688300260081 |accessdate=21 June 2012 |issn=0143-9685 |doi=10.1080/01439688300260081}}
*  
|title=Reconstructing the Musical Arrangement for "The Battle of the Somme" |last1=Haggith |first1=Toby |year=2002 |journal=Film History |volume=14 |issue=1 |jstor=3815576 |issn=0892-2160}}
*  
|title=The Battle of the Somme (DVD review) |journal=Journal of Film Preservation |last=McWilliams |first=D. |editor=Dimitriu C. |date=April 2009 |issue=79/80 |publisher=International Federation of Film Archives  |url=http://www.fiafnet.org/content/finFIAF_79%201%201.pdf |accessdate=4 November 2010 |issn=1609-2694}}
*  
|title=Film Propaganda and Its Audience: The Example of Britains Official Films during the First World War |last1=Reeves |first1=Nicholas |year=July 1983 |journal=Journal of Contemporary History |volume=18 |issue=3 |page=467 |jstor=260547 |issn=1461-7250 |doi=10.1177/002200948301800306}}
*  
|title=Cinema, Spectatorship and Propaganda: Battle of the Somme (1916) and its Contemporary Audience |last=Reeves |first=N. |year=1997 |journal=Historical Journal of Film, Radio and Television |volume=17 |issue=1 |issn=1465-3451}}
*  
|title="Watch the Picture Carefully, and See If You Can Identify Anyone": Recognition in Factual Film of the First World War Period |last=Smither |first=R. B. N. |year=2002 |journal=Film History |volume=14 |issue=3/4 |jstor=3815439 |issn=0892-2160}}

;Newspapers
*  
|title=Real War Pictures |newspaper=  |publisher=National Library of Australia |location=Melbourne, Vic. |date=14 October 1916 |url=http://nla.gov.au/nla.news-article1596249 |accessdate=6 October 2013 |issn=1833-9700}}
*   The Evening Post |location=Wellington, New Zealand |date=12 October 1916 |url=http://paperspast.natlib.govt.nz/cgi-bin/paperspast?a=d&cl=search&d=EP19161012.1.2&srpos=3&e=12-10-1916-12-10-1916--100-EP-1-byDA-on--0-- |accessdate=6 October 2013 |oclc=320805360}}
*   The Evening Post |location=Wellington, New Zealand |date=16 October 1916 |url=http://paperspast.natlib.govt.nz/cgi-bin/paperspast?a=d&cl=search&d=EP19161016.1.3&srpos=4&e=16-10-1916-16-10-1916--100-EP-1-byDA-on--0-- |accessdate=6 October 2013 |oclc=320805360}}
*  
|title=Wars Realities on the Cinema |newspaper=The Times |date=22 August 1916 |location=London |issn=0140-0460}}
*  
|title=The Battle of the Somme |author=Geoff Brown |date=25 October 2006 |newspaper=The Times |url=http://www.thetimes.co.uk/tto/arts/article2404641.ece |accessdate=6 October 2013 |issn=0140-0460}}

;Websites
*  
|title=Focal Award Nominations 2007 |author=http://www.focalint.org/ |year=2007 |url=http://www.focalint.org/focal-international-awards/2007/the-focal-international-awards-2007/nominations/award-for-archive-restoration-or-preservation-project |accessdate=6 October 2013 |issn=1359-8708}}
*  
|title=Memory of the World Register: The Battle of the Somme (nomination form) |last=Smither |first=R. B. N. |year=2004 |work=UNESCO |publisher=UNESCO |url=http://www.unesco.org/new/fileadmin/MULTIMEDIA/HQ/CI/CI/pdf/mow/nomination_forms/united_kingdom_battle_of_the_somme.pdf |accessdate=6 October 2013}}

 

===Further reading===
 
* Culbert, David. "The Imperial War Museum: World War I film catalogue and `The Battle of the Somme (video)," Historical Journal of Film, Radio & Television (Oct 1995, Vol. 15, Issue 4)
*  
* Reeves, Nicholas. "Through the Eye of the Camera: Contemporary Cinema Audiences and Their Experience of War in the Film Battle of the Somme" in Hugh Cecil and Peter H. Liddle, eds.,  Facing Armageddon: The First World War Experienced (London: Leo Cooper, 1996), ch 55
*  
*  

 

== External links ==
 
 
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 