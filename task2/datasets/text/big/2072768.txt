The Sound Barrier
 
 
 
{{Infobox film
| name           = The Sound Barrier
| image          = Soundbarrier.jpg
| caption        = Film poster| U.S. theatrical release poster
| director       = David Lean
| writer         = Terence Rattigan
| starring       = Ralph Richardson Ann Todd Nigel Patrick John Justin Denholm Elliott
| producer       = David Lean
| music          = Malcolm Arnold
| cinematography = Jack Hildyard
| editing        = Geoffrey Foot London Film Productions
| distributor    = London Films British Lion Films United Artists
| released       =  
| runtime        = 118 minutes
| country        = United Kingdom
| language       = English
| budget         =£250,000 Kulik 1990, p. 316. 
| gross = £227,978 (UK) 
}}

The Sound Barrier (aka in the United States, as Breaking Through the Sound Barrier and Breaking the Sound Barrier) is a British 1952 film directed by David Lean. It is a fictional story about attempts by aircraft designers and test pilots to break the sound barrier. David Leans third and final film with his wife Ann Todd was also his first for Alexander Kordas London Films, following the break-up of Cineguild. The Sound Barrier stars Ralph Richardson, Ann Todd and Nigel Patrick.

The Sound Barrier was a great box-office success, but it is now rarely seen and has become one of the least-known of Leans films. Following on In Which We Serve (1942), the film is another of Leans ventures into a genre of filmmaking where impressions of documentary film are created. 
 

==Plot== Second World ferrying assignment of a two-seater de Havilland Vampire to Cairo, Egypt, returning later the same day as passengers on the de Havilland Comet.
 reverses his flight controls, allowing his aircraft to break the sound barrier.

Accepting that her father cared about those whose lives were lost in tests, Susan changes her plan of moving to London and takes her young son with her back to home and Sir John.

==Cast==
As appearing in screen credits (main roles identified):  , IMDb. Retrieved: 30 April 2015. 
{| class="wikitable" width="50%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|- Ralph Richardson || John Ridgefield
|- Ann Todd || Susan Garthwaite
|- Nigel Patrick || Tony Garthwaite
|- John Justin || Philip Peel
|- Denholm Elliott || Christopher Ridgefield
|- Joseph Tomelty || Will Sparks
|- Dinah Sheridan || Jess Peel
|- Jack Allen Jack Allen || Windy Williams
|- Anthony Snell || Peter Makepeace
|- Donald Harron || ATA officer
|- Vincent Holman || Factor
|- Ralph Michael || Fletcher
|- Douglas Muir Douglas Muir || Controller
|- Leslie Phillips || Controller
|}

==Production== Power Jets Ltd and others following.   
 supersonic speeds, DH 108.  

Contrary to what is depicted in the film, the first aircraft to break the sound barrier was the rocket-powered aircraft|rocket-powered Bell X-1 flown by Chuck Yeager of the United States Air Force in 1947.   As Yeager, who was present at the U.S. premiere, described in his first biography, The Sound Barrier was entertaining, but not that realistic – and any pilot who attempted to break the sound barrier in the manner portrayed in the film (the one action, forcing the centre stick "forward" to pull "out" of a dive) would have been killed.     Nevertheless, because the 1947 Bell X-1 flight had not been widely publicized, many who had seen The Sound Barrier thought it was a true story in which the first supersonic flight is made by British pilots. Yeager and Janos 1986, pp. 206–207.    
 British Aircraft Constructors Association, aircraft that were featured in The Sound Barrier were on loan from Vickers, de Havilland and other British aerospace companies. Pendo 1985, p. 135.   In addition, footage of early 1950s jet technology in Great Britain used in the film includes scenes of the de Havilland Comet airliner, the worlds first jet passenger airliner,  the Supermarine Attacker and the de Havilland Vampire. The Supermarine Swift which was featured as the experimental Prometheus jet fighter was one of the Swift prototypes (VV119). Not unlike its screen persona, the Swift was also a particularly troublesome aircraft design that underwent teething development trials.   

Malcolm Arnold (later Sir Malcolm Arnold) composed the music score, his first of three films with David Lean.  The two other collaborations were Hobsons Choice (1954 film)|Hobsons Choice (1954) and The Bridge on the River Kwai (1957). 

==Reception==
The Sound Barrier, in its guise as Breaking the Sound Barrier was reviewed by Bosley Crowther in The New York Times. Typical of the many positive reviews, he said, "... this picture, which was directed and produced in England by David Lean from an uncommonly literate and sensitive original script by Terence Rattigan, is a wonderfully beautiful and thrilling comprehension of the power of jet airplanes and of the minds and emotions of the people who are involved with these miraculous machines. And it is played with consummate revelation of subtle and profound characters by a cast headed by Ralph Richardson, Nigel Patrick and Ann Todd." 

Film historian Stephen Pendo further described the "... brilliant aerial photography. ... Along with the conventional shot of the aircraft there is some unusual creative camera work. To illustrate the passage of a plane, Lean shows only the wheat in a field being bent by air currents produced by the unseen jet. ... Even the cockpit shots are very good, with the test pilots in G suits and goggles framed by the plexiglass and sky backgrounds." 

The Sound Barrier was the 12th most popular movie at the British box office in 1952,  and also did well in the United States, making a comfortable profit.  Later releases in both VHS and DVD home versions have taken place.

==Awards==
===Academy Awards=== Best Sound Recording – London Films   oscars.org. Retrieved: 20 August 2011. 
* Nominee: Best story written directly for the screen (Terence Rattigan)

With this film, Ralph Richardson became the first actor to win the New York Film Critics Award for Best Actor who did not also go on to win an Oscar nomination.

===BAFTA Awards=== Best Film from any Source Best British Film
* Winner Best British Actor (Ralph Richardson)
* Nominee Best British Actor (Nigel Patrick)
* Nominee Best British Actress (Ann Todd)

===US National Board of Review===
* Winner Best Actor (Ralph Richardson)
* Winner Best Director (David Lean)
* Winner Best Foreign Film 
* Listed in Top Foreign Films

===New York Critics Circle===
* Winner Best Actor (Ralph Richardson)

==References==
Notes
 

Citations
 

Bibliography
 
* Eric "Winkle" Brown|Brown, Eric. The Miles M.52: Gateway to Supersonic Flight. Stroud, Gloucestershire, UK: The History Press, 2012. ISBN 978-0-7524-7014-6.
* Eric "Winkle" Brown|Brown, Eric. Wings on my Sleeve.  London: Weidenfeld & Nicolson, 2006. ISBN 978-0-297-84565-2.
* Carlson, Mark. Flying on Film: A Century of Aviation in the Movies, 1912–2012. Duncan, Oklahoma: BearManor Media, 2012. ISBN 978-1-59393-219-0.
* Davies, R.E.G. and Philip J. Birtles. Comet: The Worlds First Jet Airliner. McLean, Virginia: Paladwr Press, 1999. ISBN 1-888962-14-3.
* de Havilland, Geoffrey. Sky Fever: The Autobiography of Sir Geoffrey De Havilland. Ramsbury, Marlborough, Wiltshire, UK: Crowood Press Ltd., 1999. ISBN 1-84037-148-X.
* Hamilton-Paterson, James. Empire of the Clouds: When Britains Aircraft Ruled the World. London: Faber & Faber, 2010. ISBN 978-0-5712-4795-0.
* Hardwick, Jack and Ed Schnepf. "A Viewers Guide to Aviation Movies." The Making of the Great Aviation Films. General Aviation Series, Volume 2, 1989.
* Kulik, Karol. Alexander Korda: The Man Who Could Work Miracles. London: Virgin, 1990. ISBN 978-0-86369-446-2.
* Paris, Michael. From the Wright Brothers to Top gun: Aviation, Nationalism, and Popular Cinema. Manchester, UK: Manchester University Press, 1995. ISBN 978-0-7190-4074-0.
* Pendo, Stephen. Aviation in the Cinema. Lanham, Maryland: Scarecrow Press, 1985. ISBN 0-8-1081-746-2.
* Porter, Vincent. "The Robert Clark Account." Historical Journal of Film, Radio and Television, Vol. 20 No. 4, 2000.
* Pratley, Gerald. The Cinema of David Lean. Aurora, Colorado: Oak Tree Publications, !974. ISBN 978-0-4980-1050-7.
* Winchester, Jim. The Worlds Worst Aircraft: From Pioneering Failures to Multimillion Dollar Disasters. London: Amber Books Ltd., 2005. ISBN 1-904687-34-2.
* Wood, Derek. Project Cancelled. Indianapolis: The Bobbs-Merrill Company Inc., 1975. ISBN 0-672-52166-0.
* Yeager, Chuck, Bob Cardenas, Bob Hoover, Jack Russell and James Young. The Quest for Mach One: A First-Person Account of Breaking the Sound Barrier. New York: Penguin Studio, 1997. ISBN 0-670-87460-4.
* Yeager, Chuck and Leo Janos. Yeager: An Autobiography. New York: Bantam Books, 1986. ISBN 0-553-25674-2.
 

==External links==
*  
*  
*  

 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 