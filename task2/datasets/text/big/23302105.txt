The Hole (1997 film)
{{Infobox film
| name           = The Hole
| image          = The Hole film poster.jpg
| caption        = Theatrical release poster
| film name = {{Film name
 | hangul         =  
 | hanja          =  
 | rr             = Olgami
 | mr             = Olgami}}
| director       = Kim Sung-hong
| producer       = Kang Woo-suk  Kim Se-chang
| writer         = Yeo Hye-yeong
| starring       = Yoon So-jeong Choi Ji-woo Park Yong-woo
| music          = Kim Dong-seong
| cinematography = Lee Dong-sam
| editing        = Park Gok-ji
| studio         = Cinema Service
| distributor    = Cinema Service
| released       =  
| runtime        = 100 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          = 
}} 1997 South Korean film directed by Kim Sung-hong.

== Plot ==
Jin-sook has a close relationship with her son, Don-woo, and is surprised when he announces his engagement to Su-jin. After the wedding, the three end up living together, with a nervous Su-jin keen to impress her new mother-in-law. But Jin-sook is determined to sabotage her sons marriage.

== Cast ==
* Yoon So-jeong ... Jin-sook
* Choi Ji-woo ... Su-jin
* Park Yong-woo ... Dong-woo
* Mun Su-jin
* Lee Seung-woo
* Jeon Hong-ryeol
* Koo Hye-ryung
* Youn Sung-hun
* Tae Yu-rim
* Kim Gye-pae
* Seo Eun-sun
* Kim Tae-beom
* Gang Gyeong-ja
* Lee Seok-hwan
* O Hyo-seok

== Release ==
The Hole was released in South Korea on 1 November 1997 and received a total of 141,717 admissions in Seoul, making it the tenth biggest selling Korean film of that year. 

== Critical response ==
David Cornelius of DVD Talk found the film somewhat limited in scope, saying, "The limitations placed upon the story prevent any broadening of ideas, leaving us only with a clichéd chunk of domestic thriller that plays out by the numbers". However, he also acknowledged that such limitations also helped the film in other areas, saying, "The Hole becomes very claustrophobic, with a tension that never lets up for the last forty-some minutes. Its grandiose and outrageous, yes, but its also highly effective in building the right kind of scares". 
 
== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 

 
 