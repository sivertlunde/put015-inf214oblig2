The Rocket (2013 film)
 
{{Infobox film
| name           = The Rocket
| image          = The Rocket poster.jpg
| caption        = Film poster
| director       = Kim Mordaunt
| producer       = Sylvia Wilczynski
| writer         = Kim Mordaunt
| starring       = Sitthiphon Disamoe Loungnam Kaosainam Suthep Po-ngam
| music          = Caitlin Yeo
| cinematography = Andrew Commis
| editing        = Nick Meyers
| distributor    = 
| released       =  
| runtime        = 96 minutes
| country        = Australia
| language       = Lao
| budget         = 
}}
 Best Foreign Language Film at the 86th Academy Awards,       but it was not nominated.
 AFI Fest.   

==Plot==
In the Laos mountains, a woman named Mali (Alice Keohavong) gives birth to twins, of which one suvives. Her husbands mother, Taitok (Bunsri Yindi) says that the one living child must also die, because one twin is blessed while the other is cursed. Believing her only living twin son to be blessed, Mali refuses to kill it, and so she and Taitok keep it a secret from her husband, Toma (Sumrit Warin).

Seven years later, the living twin named Ahlo (Sitthiphon Disamoe) learns that a second dam is being built, so Toma takes Ahlo to see the dam, where its revealed in a presentation video that the people of Ahlos village will have to be relocated due to the valley they live in will be flooded between the two dams. So Ahlo and his family move through the woods while taking his boat with them. With the help of a ploughing buffalo, they manage to get the boat half way up the hill, only for the ropes to snap, sending the boat crashing into Mali, killing her. Furious, Taitok reveals to Toma that Ahlo is a twin and that "he should have died".

After burying Mali, Ahlo and his family ride a bus to their new village, which Taitok doesnt much like due to the running water and electricity that replaces their "traditions". There, Ahlo meets a girl named Kia (Loungnam Kaosainam), who had lost her family and now lives with her uncle, Purple (Suthep Po-ngam), who is a fan of James Brown. Kia shows Ahlo soft land for him to grow mangoes on, which he wishes to do in honour of his mother, but Toma tells Ahlo to no longer speak to them, making Ahlo destroy Tomas model house out of anger. He then visits Kia and Purple again and learns that all the electricity that was promised to the people are being used by the "hydro bosses". The following night, Ahlo sneaks out and attempts to get electricity for the people by hooking up some cables to the main power source, but being the cursed twin, he ends up causing a blackout for everyone else except for Kia and Purple, who have electricity for their television set. The next day, Ahlo tells Kia about his tribes tradition about being a cursed twin. When he accidentally desecrates a sacred shrine, Ahlo, Toma and Taitko have their house and belongings burnt in retaliation. Along with Kia and Purple, they all sneak out of the village in a cart filled with undetonated bombs from the war or Unexploded ordnance|UXO.

They all journey for days and eventually arrive at Purples village, they call Paradise, with no other inhabitants. Ahlo and Kia run off into the bushes to play, but Ahlo almost sets off a bombie in the event of smashing fruit that Kia threw to him. Toma proposes moving again due to the land being surrounded by these dangerous bombs (which explains why there are no other inhabitants). While journeying for yet another home, Ahlo and company cross paths with a parade of travelers, where he hears an announcer mention a rocket competition that gives out cash prizes. While settling down, they are met by the village chief who tells them that the purpose of the contest is to launch them into the clouds in which theyll explode and produce rain. Believing this could break his curse, Ahlo announces his intentions to build a rocket and enter the contest to earn the money for them to buy a new home, but Toma and Taitko wont allow him, due to his bad luck. Ahlo nevertheless still decides to build his own rocket and runs off.

While walking through the woods in search of things to build his rocket with, Ahlo comes across another unexploded bomb, but its hard casing proves to be usable for a rocket, but blows up seconds after Ahlo hits it with a rock; he survives. Later, Ahlo manages to get Purple to help him build the rocket. Purple takes Ahlo to a bat cave to collect bat droppings that, according to Purple, can be used for blasting up the rocket. Inside the cave, Ahlo comes across a woman who considers him to be an evil spirit, causing him to run out of the cave screaming.

The rocket festival begins the next day. While some people are launching their rockets, Ahlo is still in the woods trying to put his together. He grinds the bat droppings into powder, but tests it out on a few mini rockets, to no avail, so Ahlo tries urinating on the bat dropping powder (advised by Purple, who said that can make it work better). In the contest, Toma launches his rocket, named Lucky, but lets go of it too soon and launches it poorly. A champion rocket named The Million is launched afterwards which successfully reaches the clouds. Ahlo brings his rocket in, which he calls The Bat, but the announcer tells him that kids arent allowed to launch. Ahlo asks Toma to launch it, but Taitko doesnt allow him to, believing that The Bat will blow everyone up because of Ahlos bad luck. Nobody else volunteers to do so and Ahlo then runs off. Toma decides to launch it anyway. Kia catches up to Ahlo and tells him what his father is doing, and so he runs back to the contest, thinking the rocket will kill Toma, and shouts his apology over the loud roaring of the rocket. The Bat is launched properly this time and it reaches the clouds. The Bat then surprisingly explodes inside the cloud, causing it to rain. Ahlo wins 5 million kip and is now no longer considered cursed.

==Cast==
* Sitthiphon Disamoe as Ahlo
* Loungnam Kaosainam as Kia
* Suthep Po-ngam as Purple
* Bunsri Yindi as Taitok
* Sumrit Warin as Toma
* Alice Keohavong as Mali

==Reception and awards==

The Rocket was met with critical acclaim, earning an approval rating of 98% on Rotten Tomatoes.

Sheri Linden of the Los Angeles Times gave a positive review, commenting "Mordaunt doesnt always succeed at balancing the sentimental, the political and the ethnographic, but at its strongest the story is a seamless melding of historys dark undertow and a childs indefatigable optimism."  Kyle Smith of the New York Post thought the film was "long on atmosphere and less sentimental about poverty than Beasts of the Southern Wild, but the film carries a potent charge of authenticity."  Joe Morgenstern of The Wall Street Journal called the characters "cliches" but the performances "utterly fresh".  Avi Offer of NYC Movie Guru called the film "uplifting, engrossing and thrilling while remaining accessible to both art-house and mainstream audiences. Its destined to become a sleeper hit."  Noah Berlatsky of The Dissolve called the film a "well-constructed delivery system for sparkly cheer, but it lacks a more substantial payload."  Mark Adams of Screen International praised the films visuals, calling them "a stunning location of Laos that provides an enthralling and evocative backdrop for writer/director Kim Mordaunts engaging film The Rocket, a lush and bruising coming-of-age story." 

{| class="wikitable"
|-
! Award
! Category
! Subject
! Result
|- AACTA Awards|AACTA Award  (3rd AACTA Awards|3rd)  AACTA Award Best Film Sylvia Wilczynski
| 
|- AACTA Award Best Direction Kim Mordaunt
| 
|- AACTA Award Best Original Screenplay
| 
|- AACTA Award Best Actor Sitthiphon Disamoe
| 
|- AACTA Award Best Supporting Actor Suthep Po-ngam
| 
|- AACTA Award Best Supporting Actress Alice Keohavong
| 
|- AACTA Award Best Cinematography Andrew Commis
| 
|- AACTA Award Best Editing Nick Meyers
| 
|- AACTA Award Best Original Music Score Caitlin Yeo
| 
|- AACTA Award Best Sound Sam Petty
| 
|- Brooke Trezise
| 
|- Nick Emond
| 
|- Sam Hayward
| 
|- Yulia Akerholt
| 
|- AACTA Award Best Production Design Pete Baxter
| 
|- AACTA Award Best Costume Design Woranun Pueakpun
| 
|- Sylvia Wilczynski
| 
|- Australian Screen ASE Award Best Editing in a Feature Film Nick Meyers
| 
|- Film Critics FCCA Awards Best Film Sylvia Wilczynski
| 
|- Best Director Kim Mordaunt
| 
|- Best Script
| 
|- Best Young Actor Sitthiphon Disamoe
| 
|- Loungnam Kaosainam
| 
|- Best Editing Nick Meyers
| 
|- Best Cinematography Andrew Commis
| 
|- Best Music Caitlin Yeo
| 
|- Tribeca Film Tribeca Film Festival Audience Award    Best Narrative Feature Kim Mordaunt
| 
|- Best Actor in a Narrative Feature Film Sitthiphon Disamoe
| 
|-
|}

==See also==
* List of submissions to the 86th Academy Awards for Best Foreign Language Film
* List of Australian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 