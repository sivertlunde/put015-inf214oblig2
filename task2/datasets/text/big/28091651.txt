Act of Love (1953 film)
{{Infobox film
| name           = Act of Love
| caption        =
| image	         = Act of Love FilmPoster.jpeg
| director       = Anatole Litvak
| producer       = Anatole Litvak
| writer         = Joseph Kessel Irwin Shaw
| based on       =  
| starring       = Kirk Douglas Dany Robin
| music          = Michel Emer Joe Hajos Michel B. Rosenstein
| cinematography = Armand Thirard
| editing        = Léonide Azar (French) William Hornbeck
| studio         = Benagoss Productions
| distributor    = United Artists
| released       =  
| runtime        = 95 minutes (French) 108 minutes (English)
| country        = France United States
| language       = English French
| budget         =
| gross          =1,048,123 admissions (France) 
}}
 Alfred Hayes. A Parisian falls in love with an American soldier near the end of World War II.

==Plot==
Robert Teller (Kirk Douglas) visits a seaport in the south of France in the early 1950s. He reflects back to his time in the army shortly after Paris has been liberated.

Years earlier, to get away from the barracks and the other soldiers, Robert rents a room in a hotel-restaurant. Lise (Dany Robin), an orphan without money or identity papers, seeks a way to escape from the authorities. She asks Robert to pass her off as his wife. Even though he does not inspire trust, she starts to fall in love. Lise tells of the time she was the most happy and secure—living in a little seaside village.  

When a black market dragnet lands Lise in jail, she is humiliated because now she (like Jean Valjean) is branded a "criminal for life." By this time, Robert loves her deeply and is willing to marry her.

In order to do so, Robert must obtain the approval of his commanding officer, who refuses because the captain thinks he knows what is best for his men. Robert is transferred away from Paris immediately. He deserts, but is arrested. Lise now feels even more abandoned because Robert does not show up for their wedding.

His thoughts returning to the present, Robert runs into his old captain (who had been trying to place Roberts face). He hears the captain tell his wife what a troublemaker Robert was back then and how he "rescued" Robert from the clutches of a French girl. The captain says, "Well, I bet you havent seen her since the war."  Robert replies, "You are right. They pulled her body out of the river shortly after I was transferred."

==Cast==
* Kirk Douglas as Robert Teller
* Dany Robin as Lise Gudayec / Madame Teller
* Gabrielle Dorziat as Adèle Lacaud
* Barbara Laage as Nina
* Fernand Ledoux as Fernand Lacaud Robert Strauss as S/Sgt. Johnny Blackwood
* Marthe Mercadier as La fille de la terrasse / Young woman George Mathews as Captain Henderson
* Richard Benedict as Pete (GI who is rolled for his money)
* Leslie Dwyer as Le sergent anglais / English sergeant
* Sydney Chaplin as Un soldat / Soldier
* Brigitte Bardot as Mimi
* Nedd Willard		
* Serge Reggiani as Claude Lacaud
==References==
 
==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 