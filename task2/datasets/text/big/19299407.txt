Big Business (1924 film)
 
{{Infobox film
| name           = Big Business
| image          =
| caption        =
| director       = Robert F. McGowan
| producer       = Hal Roach
| writer         = Hal Roach H. M. Walker
| starring       =
| music          =
| cinematography =
| editing        =
| distributor    = Pathé|Pathé Exchange
| released       =  
| runtime        = 27 13" 
| country        = United States
| language       = Silent with English intertitles
| budget         =
}}
 short silent silent comedy film directed by Robert F. McGowan.       It was the 22nd Our Gang short subject released.

==Plot==
The gang starts up their own barbershop, giving the neighbourhood kids haircuts that wouldnt become popular for another sixty years. When they see Mickie in his Little Lord Fauntleroy outfit, they kidnap him and give him the works. Mickie then decides to join them in their enterprise.

==Cast==
===The Gang===
* Joe Cobb as Joe
* Jackie Condon as Jackie
* Mickey Daniels as Michael Mickie
* Allen Hoskins as Farina
* Mary Kornman as Mary
* Ernest Morrison as Sunshine Sammy
* Jannie Hoskins as Mango
* Andy Samuel as Andy
* Sonny Boy Warde as Sing Joy

===Additional cast===
* Allan Cavan – Office worker William Gillespie – Mickies father
* Lyle Tayo – Mickeys mother
* Charley Young – Gardener

==See also==
* Our Gang filmography

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 
 

 