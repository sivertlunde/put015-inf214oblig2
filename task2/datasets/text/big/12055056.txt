John Goldfarb, Please Come Home!
{{Infobox film
| name           = John Goldfarb, Please Come Home!
| image          = John Goldfarb-1965-poster.jpg
| image_size     = 220
| caption        = 1965 Theatrical Poster
| director       = J. Lee Thompson
| writer         = William Peter Blatty
| starring       = Shirley MacLaine Peter Ustinov Richard Crenna
| music          = John Williams
| cinematography = Leon Shamroy
| editing        = William B. Murphy
| producer       = Parker--Orchard Productions
| distributor    = 20th Century Fox
| released       =  
| runtime        = 96 min
| country        = United States
| language       = English
| budget         = $3,705,000 
| gross          = $3,000,000 (US/ Canada rentals) 
}}
John Goldfarb, Please Come Home!   is a 1965 black comedy film based on the novel by William Peter Blatty published in 1963. 
The movie was directed by J. Lee Thompson.

==Synopsis== American Francis kingdom of Fawzia. 

The countrys leader threatens to turn him over to the Soviets unless he agrees to coach a football team. Jenny Ericson, the magazine journalist who made Goldfarb famous, is on an undercover assignment as a member of the Kings harem, and when she discovers she was wrong in thinking the King is no longer romantically interested in his wives, she seeks help from Goldfarb. The King blackmails the U.S. Department of State into arranging an exhibition football game between the Notre Dame Fighting Irish and his own team from Fawz University. Jenny becomes a cheerleader and then the quarterback who scores the winning touchdown for Fawz University.

==Cast==
* Shirley MacLaine as Jenny
* Richard Crenna as Goldfarb
* Peter Ustinov as the King Richard Deacon, Teri Garr, Jackie Coogan, Patrick Adiarte, Barbara Bouchet, Sultanna and Jerry Orbach in supporting roles.

==Production notes== Doubleday (ISBN 0553142518). The novels success led Twentieth Century-Fox to acquire the film rights, and Blatty submitted his original script for a feature film directed by J. Lee Thompson. 

Fox expected the film to be its Christmas 1964 release; however, the  University of Notre Dame filed a defamation lawsuit and got a court injunction to delay the release of the film, claiming the studio had "knowingly and illegally misappropriated, diluted and commercially exploited for their private profit the names, symbols, football team, prestige, high reputation and goodwill" of the university.  The lawsuit wasnt settled until the following year, when the studio finally won its case. 

The film was a critical failure and earned back only $3,880,000 of its $4 million budget. 

Later, Jim Backus wrote a memoir called What Are You Doing After the Orgy?, the title taken from one of his lines in the film.

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 