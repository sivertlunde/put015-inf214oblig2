Meghe Dhaka Tara (2013 film)
 
 
{{Infobox film
| name           = Meghe Dhaka Tara
| image          = Meghe Dhaka Tara 2013 poster.jpg
| alt            = 
| caption        =  Kamaleswar Mukherjee
| producer       = Shrikant Mohta Mahendra Soni
| writer         = Kamaleswar Mukherjee
| dialogue  = Kamaleswar Mukherjee
| story        = Kamaleswar Mukherjee
| screenplay = Kamaleswar Mukherjee Rahul Banerjee Bidipta Chakrabarty Subhasish Mukhopadhyay Biswajit Chakraborty Debpratim Dasgupta
| music          = Debojyoti Mishra Salil Chowdhury Rabindranath Tagore
| cinematography = Soumik Halder
| editing        = Rabiranjan Moitra
| studio         = Shree Venkatesh Films
| distributor    = 
| released       =  
| runtime        = 2 hours 32 minutes 
| country        = India Bengali
| budget         = 
| gross          = 
}} Bengali film Tebhaga and Naxalite movements.

Anandabazar Patrika, after a special screening of the film in November 2012, wrote in their review that this film had potential to become a "classic" film. The acting of Saswata Chatterjee as Nilkantha Bagchi was widely appreciated too.

== Plot ==
To Ritwik Ghatak film was not merely a form of entertainment, but a weapon, a medium to portray struggle of common men. He was successful neither in his career nor could he become a good family man or husband. Still he was an artist who never compromised for personal gain, fame or profit.

The story of the film starts in 1969 and deals with Ritwik Ghataks time spent in a mental asylum. It shows Nilkantha being admitted to mental asylum under the supervision of Doctor S. P. Mukherjee. S. P. Mukherjee learns that the Prime Minister of India knows Nilkantha and is fond of his works and may request the hospital authority to take special care of him. In the hospital a police officer sees Nilkantha and tells Doctor Mukherjee that he is a wasted drunkard. Another patient of hospital mocks Nilkantha as a "disgraced intellectual". Even while he was undergoing treatment in the hospitals, he writes a play and stages it with other asylum patients.

The film shows how Durga (Nilkanthas wife) wants to leave him saying "separation is essential". Nilkantha witnessed partition of Bengal and its devastating effect and in his youth became attracted towards communism. Throughout the film Nilkanthas mental agony, struggles, inner contradictions, disappointments as well as financial troubles faced by him are shown. He clearly tells, it is not possible for him to make entertaining or commercial films. During a discussion Nilkantha asks Doctor Mukherjee that should a man perform mujra at a time when the society is burning? He also tells Doctor Mukherjee that he is a "peoples artist" and that is his job.

The film also shows the atmosphere of Bengal during Tebhaga movement|Tebhaga and Naxalite movements. A scene has been shown in the film where a book named "How to be a good communist?" is burning. This scene symbolises Ghataks dissatisfaction with Indias communist politics. When Durga tells Nilkantha that they dont have a single rupee to run the family, Nilkantha readily replies, it is not money but it is ones work which will remain forever.

== Credits ==

=== Cast ===
{{multiple image
 
| align     = right 
| direction = horizontal
| width     = 
| footer    = Mrinal Sen (left) was a close friend of Ghatak in real life. In this film Anindya Bose plays Sens character. Abir Chatterjee (centre) plays Doctor S. P. Mukherjee, a psychiatrist. In this film during a heated conversation Nilkantha directly asks Doctor Mukherjee that does he recommend someone to perform dance (mujra) when the society is burning. Mumtaz Sorcar (right) plays Supriya Devis role. Supriya Devi portrayed Neeta in Ghataks Meghe Dhaka Tara (1960)

 
| image1    = Mrinal-sen.jpg
| width1    = 100
| alt1      =

 
| image2    = Abirc.jpg
| width2    = 115
| alt2      =

 
| image3    = Mumtaz Sorcar.jpg
| width3    = 135
| alt3      =

}}
* Saswata Chatterjee as Nilkantha Bagchi
* Ananya Chatterjee as Durga
* Abir Chatterjee as Doctor S. P. Mukherjee Rahul Banerjee as Anil Chatterjee
* Joydeep Mukherjee as Hrishikesh Mukherjee
* Abhijit Guha as Salil Choudhury
* Padmanabha Dasgupta as Kali Banerjee
* Anindya Bose as Mrinal Sen
* Mumtaz Sorcar as Supriya Devi
* Bidipta Chakrabarty as Shobha Sen
* Subhasish Mukhopadhyay as Bijon Bhattacharya
* Biswajit Chakraborty as Charuprakash Ghosh
* Shantilal Mukherjee
* Arup Kumar Ganguly as Freedom Fighter
* Anindya Banerjee as Patient in Mental Asylum
* Debpratim Dasgupta as Patient in Mental Asylum

=== Crew ===
<!-- #################################################################################################################################
#######    THIS SECTION HAS SOME IMPORTANT DATA LIKE ART DIRECTOR, DANCE CHOREOGRAPHER ETC WHICH PLAY IMPORTANT ROLE IN THIS FILM.
#######                            PLEASE DONT REMOVE THIS SECTION                                                                -->
* Direction, story, screenplay: Kamaleswar Mukherjee
* Music direction, background music: Debojyoti Mishra
* Sound designing, film mix: Biswadeep Chatterjee
* Art direction: Tanmay Chakraborty
* Cinematography: Soumik Halder
* Editing: Rabiranjan Maitra
* Dance choreography: Tanusree Shankar

== Production ==

=== Development ===
{{Quote box quote = The reason my film is called "MDT" is because Nilkantha, despite being a star, is covered in clouds. The title will also create a connection between Ghatak and my films hero, Neelkantha. source = Kamaleswar Mukherjee in an interview  align = right width = 30% border = 1px solid fontsize =  bgcolor = #E9F6F7 tstyle =  qalign = center qstyle =  quoted = 1 salign = right 
}}
Director Kamaleswar Mukherjee wanted to pay tribute to film-maker Ritwik Ghatak and decided to make this film which is a fictional account of a filmmakers life and the filmmakers character is inspired by Ritwik Ghataks life and works.  He told, he got inspiration from all of Ghataks works and his life, Ghataks Meghe Dhaka Tara (1960)    was one of them.  This film was Mukherjees second films as a director after Uro Chithi which was a box-office failure.       At first the film was named Jwala (meaning, "pain" or "agony").    About the title of the film Mukherjee told, despite being a film director Ghatks life was capped with clouds which creates a connection between Ghatak and Nilkantha, the protagonist of the film. 

Mukherjee used the following books to do the research works for the film— 
# Ritwik Tantra   written by Sanjay Mukhopadhyay
# a) Ritwik and b) Padma theke Titash   written by Surama Ghatak, Anushtup publication
# Chalachitra, Manush o Aro Kichu   written by Sanjay Mukhopadhyay, Deys Publishing, ISBN 81-295-0397-2.
# Ritwik Ghatak   by Rajat Roy
# a) Ritwik ekti nadir naam and b) Sakshat Ritwik   written by Sanghita Ghatak
# Ritwik-da, Salil-da   written by Tushar Talukdar

=== Filming ===
The entire film is in black and white except the last scene which has been shot in colour. The scene is the films climax too.   The film was shot in Kolkata and Purulia District|Purulia. Some of the most important scenes of the film were shot in Purulia including a long rain scene where Ghatak too shot his last film Jukti Takko Aar Gappo.  Kamaleswar Mukherjee found the local people of Purulia enthusiastic and helpful.    Some of the rehearsals were organized at Kolkatas Muktangan theatre. 
 . ]]
Films director Kamaleswar Mukherjee told in an interview– "Neither is my film Ritwiks biopic, nor is it a remake of his legendary film. It tells a story in a completely new style, and has no similarity with the original. Im inspired by all of Ghataks works and his life; "MDT" is just one of them. The audience will realize this once they watch my film."  

Saswata Chatterjee described the film as a "dream project" and told his experiences during the shooting were incredible. He also told that he was amazed to see the amount of research works Kamaleswar Mukherjee did to direct this film.     

=== Characters and casting===

==== Saswata Chatterjee as Nilkantha Bagchi ====
Saswata Chatterjee portrayed Nilkanthas character in the film. Kamaleswar Mukherjee told in an interview that Saswata was their first choice for this role as he felt "Apu da" Nick name of Saswata Chatterjee is "Apu"  was perfect to portray the character. Mahendra Soni, one of the producers of the film, later told if Saswata had denied to play the character then they would have asked Kamaleswar Mukherjee to act the role himself as they knew Mukherjee had a theatre background and is a good actor too. 

After playing the role of Bob Biswas in film Kahaani Saswata received many similar offers, but he did not want to repeat his performance of Bob. When the offer of this films protagonist role came to him he accepted. Chatterjee had already worked with films director Kamaleswar Mukherjee before.    In an interview Chatterjee told that he tried to give his weight and he shed somebody weight to fit in the character of Nilkantha.  He told, he watched a lot of Ghataks films and documentaries to understand his body language. 

Chatterjee told in an interview that during the shooting he became so involved with his character that he even faced difficulties to talk to his wife Mohua at that time.   

==== Ananya Chatterjee as Durga ====
Ananya Chatterjee played the role of Durga, Nilkanthas wife.  In the film Jukti Takko Aar Gappo too the name of Nilkanthas wife character was Durga. Kamaleswar Mukherjee selected Chatterjee to portray this role for her acting skill and Mukherjee found a type of seriousness in her expressions which was needed for the role. To prepare for this role, Chatterjee used to read the script of the film over and over again. She also used to spend time to understand Durgas feelings towards Nilkantha.   

==== Doctor S. P. Mukherjee ====
Abir Chattejee played the role of psychiatrist Doctor S. P. Mukherjee. Doctor Mukherjee tries to find out Nilkanthas confusion.     This was an important character in the film. Ananya Chatterjee later told in a light mode— "actually, in this film, Abir is like Saswata’s wife because they are together in most of the scenes." 

==== Other characters ====
; Meenu
Meenus character, inspired by Supriya Devi, was played by Mumtaz Sorcar.    Sorcar felt, portraying this role was a very difficult job, but, she thanked Kamaleswar Mukherjee for giving her independence to develop the character in her own way.     

; Bikram
Bikram is playwright, theatre director and actor. The character was inspired from Bijon Bhattacharya and was played by  Subhasish Mukherjee. 

; Gurudas
Gurudas is also a theatre actor and theatre activist like Nilkantha. The character was inspired by Kali Banerjee and played by Padmababha Dasgupta. 

== Soundtracks ==
The music of the film was composed by Debojyoti Mishra. 
{{Track listing
| extra_column    = Singer(s)
| total_length    = 
| lyrics_credits  = yes

| title1          = Bandar, Bandar, Bandar, Bandar
| note1           = Song
| lyrics1         = 
| extra1          = Anurup, Arijit
| length1         =

| title2          = Math Pathar, Gram Nagar, Bandare Toiri Hao
| note2           = Poem Shapath, the actual line of the main poem is— (Tai)Gram Nagar, Math pathar, Bandare Toiri Hao
| extra2          = 
| lyrics2         = Salil Chowdhury
| length2         =

| title3          = Moder kono desh nai
| note3           = 
| extra3          = Anurup Mallick
| lyrics3         =
| length3         = 
}}

== Release ==
The film is released on 14 June in Kolkata with English subtitles.       In an interview given in August 2012, Kamaleswar Mukherjee expected that the film would release in mid-October of that year, but the release got delayed because the production house of the film wanted to send it in different international film festivals first. The film was attempted to be sent at Cannes Film Festival and it was screened at Berlin International Film Festival.  The film created lots of excitement before its release. 

== Reception ==

=== Pre-release reception ===
After a special screening of the film in Kolkata, Anandabazar Patrika published a review in their newspaper on 28 November 2012. In that review, the making of the film was widely appreciated and the film was tagged as an "authentic period film". The review highly praised Saswata Chatterjees acting as Nilkantha and gave it "letter marks". They also felt Saswata Chatterjees portrayal of Nilkantha was better than his previous popular character Bob Biswas of Kahaani. The review predicted that this film had potential to become a "cult" or "classic" film in future.     

After watching this film in a closed preview, Bengali film director Raj Chakraborty cancelled editing works of his own film for that day. Raj Chakraborty later told— "how could I do editing works of my own film on a day in which I saw a film like Meghe Dhaka Tara? I did not want to commit such mistake!"  He also told that he was even ready to work as an assistant director to Kamaleswar Mukherjee who made such a film. 

After watching the film, Shrikant Mohta of Shree Venkatesh Films felt this was the best film made under their banners. 

=== Post-release reception ===
{| class="wikitable infobox plain" style="float:right; width:20em; font-size:80%; text-align:center; margin:0.5em 0 0.5em 1em; padding:0;" cellpadding="0"
! colspan="2" style="font-size:120%;" | Professional reviews
|-
! colspan="2" style="background:#d1dbdf; font-size:120%;" | Review Scores
|-
! Source
! Rating
|-
| Anandabazar Patrika
|  
|}
The film received positive reviews from reviewers and critics. The Times of India commented in their review, though Ghataks personal life was well-known, Mukherjees fictional retelling of his life was interesting. They found the script of the film well-researched and the real life characters like Bijon Bhattacharya, Sobha Sen, Supriya Devi were woven neatly. They stated, using color in only the last few scenes was a masterstroke.   

Bengali newspaper Anandabazar Patrika widely appreciated the making of the film and gave it 8 out of 10 and commented Kamaleshwar Mukherjee should have come to film direction much earlier.   

=== Awards ===
 International Film Festival of India, Goa 2013 International Film Festival of Kerala, 2013 International Film Festival of Kerala, 2013    

== See also ==
* Chander Pahar (film)

== References ==

=== Footnotes ===
 

=== Citations ===
 

== External links ==
{{ external media
| align  = right
| width  =  
| audio3 = 
| video1 =  

}}
*  

 
 

 
 
 
 
 
 