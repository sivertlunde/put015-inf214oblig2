Hangar 18 (film)
{{Infobox film
| name           = Hangar 18
| image          = Hangar18poster.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = James L. Conway
| producer       = Charles E. Sellier Jr.
| screenplay     = Ken Pettus
| story          = Thomas C. Chapman James L. Conway
| narrator       =  Gary Collins James Hampton Pamela Bellwood
| music          = Andrew Belling John Cacavas
| cinematography = Paul Hipp
| editing        = Michael Spence
| studio         = Sunn Classic Pictures
| distributor    = Sunn Classic Pictures
| released       = July 1980
| runtime        = 97 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $11,000,000 
}} UFO interest Roswell incident). KTMA era.

The picture was released by Sunn Classic Pictures, an independent U.S.-based film distributor whose library is now owned by Paramount Pictures, notable for presenting what TV Guide called "...awful big-screen documentaries   In Search of Noahs Ark and In Search of Historic Jesus". 

==Plot==
Hangar 18 involves a U.F.O. cover-up following an incident aboard the space shuttle. The orbiter is launching a satellite, which collides with a nearby unidentified object. The collision kills an astronaut in the launch bay. The incident is witnessed by astronauts Price and Bancroff.

After returning to Earth, both men investigate what they know happened, but which the government tries its best to hide. The damaged alien spacecraft has been recovered after making a controlled landing in the Arizona desert. Although the aliens on board die, the government technicians begin researching the complex ship. On board the craft, the technicians make three discoveries: A woman in some sort of stasis, who later awakens screaming, symbols on the control panels are the same as those found in ancient Earth civilizations, and extensive surveillance footage of power plants, military bases, and major cities worldwide.

Meanwhile, with their dogged pursuit to uncover the truth, both Bancroff and Price are targeted by the government. Chased by agents, Bancroff manages to get away but Price is killed. Bancroft eventually manages to make his way to Hangar 18, where the alien craft is being studied.

As the researchers discover evidence aboard the spacecraft that the aliens were planning to return to Earth, government agents remotely fly a jet filled with explosives into the hangar—a move aimed at killing off all involved in the cover-up in a final attempt to maintain secrecy. After the explosion, a news bulletin is released about the hangar explosion, causing a congressional hearing for evidence about the activities in Hangar 18. It is revealed that Bancroft and a few others survived the explosion since they were inside the alien ship.

==Cast== Gary Collins as Steve Bancroft James Hampton as Lew Price
* Robert Vaughn as Gordon Cain
* Pamela Bellwood as Dr. Sarah Michaels
* Philip Abbott as Lt. General Frank Morrison
* Joseph Campanella as Frank Lafferty
* Tom Hallick as Phil Cameron
* William Schallert as Professor Mills
* Darren McGavin as NASA Deputy Director Harry Forbes
* Cliff Osmond as Sheriff Duane Barlow
* Stuart Pankin as Sam Tate
* H.M. Wynant as Flight Director

==Production== Midland and Big Spring, Texas, and at the former Pyote Air Force Base.

==Reception==

===Critical response===
When the film was released The New York Times film critic Vincent Canby dismissed the film, writing, "Hangar 18 is the sort of melodrama that pretends to be skeptical, but requires that everyone watching it be profoundly gullible...It stars ...Robert Vaughn as the ruthless and fatally unimaginative White House Chief of Staff...In the supporting cast is Debra Macfarlane, who plays a beautiful female specimen found aboard the saucer, a young woman who looks amazingly like a Hollywood starlet. But then, I guess, she is. The flying saucer itself looks like an oversized toy that might have been made in Taiwan." 
 1st TV channel on the New Year night of 1982. Because of general unavailability of films with elements of science fiction and action genre, it achieved enormous popularity among Soviet youth.

==References==
 

==External links==
*  
*  
*  
*   
*   at Manor on Movies

===Mystery Science Theater 3000===
*  

 
 
 
 
 
 
 
 