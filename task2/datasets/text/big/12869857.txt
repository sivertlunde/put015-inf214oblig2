The Tribe (2005 film)
{{Infobox Film
| name           =  The Tribe
| image          = Movie_poster_the_tribe.jpg
| image_size     = 
| caption        = Promotional poster for "The Tribe"
| director       = Tiffany Shlain
| producer       = Tiffany Shlain
| writer         = Tiffany Shlain  Ken Goldberg
| narrator       = Peter Coyote
| starring       = 
| music          = Paul Godwin
| cinematography = Sophie Constantinou
| editing        = Tiffany Shlain	
| distributor    = 
| released       = 3 December 2005 (USA)
| runtime        = 18 minutes
| country        = USA
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
The Tribe is an award winning short documentary film directed by Tiffany Shlain and narrated by Peter Coyote.    Weaving together archival footage, graphics and animation, it tells the history of both the Barbie doll and the Jewish people from Biblical times to the present.   

==Awards==
*Best Documentary: NY Shorts Fest 2007       
*Best Jewish Topics, Argentina Jewish Film Festival 2007 
*Best Documentary: LA Shorts Fest 2007 
*Grand Jury Prize Winner: Florida Film Festival 2007     
*Best Short Documentary, Warsaw Jewish Film Festival 2007  
*Best Contemporary Jewish Film: "Jewish Motifs" International Film Festival, 2007 
*Best Documentary Film: Golden Star Awards 2007 
*Best Short Documentary, Nashville Film Festival 2006 
*Directors Choice Award, Black Maria Film Festival 2006 
*Winner: Audience Award, Ann Arbor Film Festival 2006 
*Best Historical Film, SF International Women’s Film Festival 2006 
*Indiewire’s Sundance Critic’s Choice 2006 

===Official selections===
*Sundance Film Festival
*Tribeca Film Festival
*International Film Festival Rotterdam
*SilverDocs
*United Nations Association
*Black Maria Film Festival
*Hot Springs Documentary Film Festival

==Cameos==

Dina Amsterdam, Bill Barnes, Marina Berlin,
Stellah DeVille, Carlton Evans, Amy Gershoni,
Ken Goldberg, Romy Itzigsohn, Misha Leybovich,
Emily Morse, Jen Naylor, Ian Schneider,
Jordan Shlain, BJ Wasserman, Adam Werbach

==References==
 

==External links==
*  
* 
* 
* 

 
 
 
 
 
 
 


 
 