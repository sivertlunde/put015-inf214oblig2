Hearts in Exile (1915 film)
 
{{Infobox film
| name           = Hearts in Exile
| image          = Hearts in Exile - 1915 - newspaperad.jpg
| image_size     =
| caption        = Newspaper advertisement.  James Young
| producer       =
| writer         = Owen Davis (scenario) James Young (writer)
| based on       =  
| starring       = See below
| music          =
| cinematography = Arthur Edeson
| editing        =
| distributor    = World Film Company
| released       =  
| runtime        = 59 min.
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 James Young. It was produced by Peerless Studios when it and many other early film studios in Americas first motion picture industry were based in Fort Lee, New Jersey at the beginning of the 20th century.    A print of the film survives in the Archiva Nationala de Filme film archive.   

The film is also known as Hearts Afire (American reissue title).

==Cast==
*Clara Kimball Young as Hope Ivanovna
*Montagu Love as Count Nicolai
*Claude Fleming as Serge Palma
*Vernon Steele as Paul Pavloff
*Frederick Truesdell as Captain Sokaloff
*Paul McAllister as Ivan Mikhail
*Bert Starkey as Victor Rasloff
*Clarissa Selwynne as Madame Romanoff

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 
 

 