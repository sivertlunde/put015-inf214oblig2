Il tempo degli avvoltoi
{{Infobox film
| name = Il tempo degli avvoltoi
| image = Il tempo degli avvoltoi.jpg
| caption =
| director = Nando Cicero
| writer = Fulvio Gicca Palli
| music = Piero Umiliani Fausto Rossi
| editing =
| producer =
| distributor =
| released =  
| runtime =
| awards = 
| country = Italy
| language = Italian
| budget = 
}} 1967 Cinema Italian Spaghetti Western film directed by Nando Cicero.      

== Cast == George Hilton: Kitosch  Frank Wolff: Joshua Tracy, the Black one 
* Pamela Tudor: Steffy Mendoza 
* Eduardo Fajardo: Don Jaime Mendoza 
* Franco Balducci: Francisco 
* Femi Benussi: Rubia
* Maria Grazia Marescalchi: Traps
* Guglielmo Spoletini: Camaro

==Plot==
 
The womaniser Kitosch takes with good humour the whipping dealt to him by boss Don Jaime after he has fondled the wife of the foreman. When he finds Kitosch in a compromising situation with his own wife the Don brands Kitosch, so "he doesnt forget who is the boss.” Kitosch leaves but is caught and beaten. Next time he draws a hidden knife and uses it to get a gun, but the horse the he also takes is shot from under him.

In town he is arrested on a trumped up charge. The wanted Black Tracy arrives in town on a wagon with a coffin.  He is arrested but inside he kills the sheriff and deputy with a hidden derringer. The man who denounced him draws but is killed by a knife throw from Kitosch, who leaves with Tracy.

When they are overcome by Don Jaime and his men, Tracy trades Kitosch for the horse of Don Jaime. When they are to hang him Tracy shoots off the rope, and kills a man who draws. When they have left he gives the horse to Kitosch and says goodbye. Kitosch says that he though they would be together, and Tracy tells him, "Dont think, itll damage your brain.”
Tracy continues and finds that the road ends at a cliff. Kitosch is there waiting under it. He says that Tracy didnt ask about the road. He shows the way to the border, returning the quip about brains.

At night Tracy spreads his black leather jacket over Kitosch. When a sheriff and his men sneak in to arrest him they are shot by Tracy. When Kitosch protest against being used as a decoy Tracy replies that he didnt ask, and besides, he wouldnt have been so believable if hed known. Tracy collects sheriff’s stars and they both demonstrate their prowess in shooting at them.

After burying the coffin – with the corpse of his mother who died while he was in jail, Tracy sets out to avenge himself on his wife Traps and best friend Big John, who turned him in for $10.000.

They find Traps in a saloon with Big John s man Slim. Kitosch beats Slim and shoots some Mexicans that interfere. He counts holes in their forehead, and misses one – the man rises and is shot!  Traps have sensitive eyes and wears a blindfold. Tracy tortures her by exposing her eyes to light. When Kitosch forcibly stops him, Tracy has an epileptic fit. Traps tells Kitosch that she betrayed Tracy because he tortured her, and has scars to show. Tracy returns and promises to leave her alone, if she tells them the whereabouts of Big John. She says that he is at Vulture pass with a band of Mexicans. When they leave Tracy ignites petroleum he has earlier poured out on the floor and she is seen surrounded by flames.

At the pass Tracy and Kitosch arrive to see the gangs of Big John and Camaro attack a transport of gold. They attack in waves and gradually decimate the escort. The two now get into the fray and uses dynamite and tricks to kill the bandits. When only Big John and Camaro are left Big John shoots the Mexican, who aims his gun, but is blown up by Kitosch. When the bank clerks venture out of the wagon to extend their thanks they are shot by Tracy, who then pursues Big John while Kitosch kisses the gold.

Tracy catches Big John and drags him through the mud, and proceeds to screw his hands into a door. Kitosch protests and then shoots Big John. He rides off saying "You know where to find me.” Tracy cries out  "He should have suffered a lot more!" and has another fit.

In town Tracy finds Kitosch spending money on drink and women. Kitosch wants to divide the loot and stay there, ending their partnership. Tracy loads all gold on his horse and goes inside the cantina, where he fingers his gun while he observes Kitosch dancing. When he violently refuses a prostitute there starts a gunfight that develops into a shoot-out with soldiers. The horse carrying the loot cannot be reached, and they escape on other horses.

Afterwards Kitosch is upset and starts a fistfight. When Tracy gets the worst of it he draws his derringer, but has a fit when he is about to shoot. Kitosch takes the derringer and holds him during the attack, and when he afterwards is to leave Tracy calls out for him.

Tracy recovers in sickbed by the priest in the parish of Don Jaime. He is about to draw at Kitoschs back when he hears about Don Jaime’s cattle business, and suggests that the two rob him.

They are let in at night by the servant girl Rubia. She wants Kitosch to ask Don Jaime for forgiveness and then marry her. He says he will take her with him. Kitosch and Tracy break into Don Jaimes bedroom. Kitosch beats Don Jaime. Rubia enters and tries to stop him. When she calls for help, Tracy shoots her. As the cash is deposited in the bank, Kitosch take Don Jaimes wife as a hostage for $90.000.

They take position in the church. The wife comes on to Kitosch, but is rejected. The priest arrives with the ransom, which turns out to be paper. Tracy shoots him.

Don Jaime orders his men to attack at dawn. They suffer casualties but take the horses. They withdraw when Tracy takes the wife up in the bell tower, shoots at the bell and threatens to kill her.

The wife convinces Tracy that they should leave together and take the money that she says is hidden at the ranch, but Kitosch stops them. He returns Tracy’s gun when he sees fresh horses outside the church.

Don Jaime is in the church with the money. He gives his word of honour that they are free to leave. Tracy now points his gun at Kitosch. He says that the wife goes with him and as for Don Jaime he never leaves a witness. Kitosch provokes Tracy by scorning him for being ”a poor epileptic.” He takes one shot without visible effect, and then draws Tracy’s derringer – ”Your own little trick” – and shoots him several times, saying that he should have done that the moment they met. Don Jaime leaves him the money, but Kitosch says he will trade it for a horse. Don Jaime helps Kitosch up in the saddle, and waves his men off. He sees blood on his hand and calls out to Kitosch, but the latter continues riding.

==Reception==
Il tempo degli avvoltoi was shown as part of a retrospective on Spaghetti Western at the 64th Venice International Film Festival. 

In his investigation of narrative structures in Spaghetti Western films, Fridlund writes that Il tempo degli avvoltoi, "this dark and twisted tale", basically presents a variation on the stories of Spaghetti Western films like Death Rides a Horse and Day of Anger, about the relationship between an older gunfighter and a younger protagonist, who joins him and later confronts him, and he further traces the root of this type of plot to the play between the younger and the older bounty killer in For a Few Dollars More. Also, when Tracys disgust for women causes the loss of their booty, it is a reversal of the situation in Django (1966 film)|Django - and several other films in its wake - where the heros quest for money brings the loss or brutalisation of his woman. Fridlund compares the ending with Chuck Moll and also the American Western Shane (film)|Shane. 

==References==
 

==External links==
* 

 
 
 
 
 
 