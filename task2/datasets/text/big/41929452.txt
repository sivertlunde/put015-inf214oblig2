All the King's Men (1971 film)
 
{{Infobox film
| name           = Вся королевская рать
| image          = 
| caption        = 
| director       =  ,  
| producer       = 
| writer         =  
| narrator       = 
| starring       = Georgiy Zhzhonov Mikhail Kozakov Oleg Yefremov Alla Demidova Rostislav Plyatt
| music          = Roman Ledenyov
| cinematography = Alfredo Alvarez,  
| editing        = 
| studio         = Byelorussian TV
| distributor    = 
| released       =  
| runtime        = 196 minutes
| country        = USSR
| language       = Russian
| budget         = 
| gross          = 
}} 1971 Soviet TV mini-series, adaptation of Robert Penn Warren|R. P. Warren All The Kings Men|novel.

== Title ==
The movies title matches that of the Russian translation of Warrens novel: Вся королевская рать, literally Whole Kings Host. Just like in the source material, its a line from the famous nursery rhyme Humpty Dumpty, translated by Samuil Marshak:

 Шалтай-Болтай
Сидел на стене.
Шалтай-Болтай
Свалился во сне.
Вся королевская конница,
Вся королевская рать
Не может Шалтая,
Не может Болтая,
Шалтая-Болтая,
Болтая-Шалтая,
Шалтая-Болтая собрать! 

== Cast ==
* Mikhail Kozakov, as Jack Burden 
* Georgiy Zhzhonov, as Willie Stark
* Rostislav Plyatt, as Judge Irwin
* Tatyana Lavrova, as Sadie Burke
* Alla Demidova, as Anne Stanton
* Oleg Yefremov, as Adam Stanton
* Boris Ivanov, as Tiny Duffy
* Lev Durov, as Sugar Boy
* Anatoli Papanov, as Burden Sr.
* Yevgeniy Yevstigneyev, as Larson
* Aleksandra Klimova, as Mrs. Burden
* Valentina Kalinina, as Lucy Stark
* Valery Khlevinski, as Tom Stark
* Ada Vojtsik, as Mrs. Littlepaugh Yevgeni Kuznetsov, as Dr. Bland
* Stepan Birillo, as Hugh Miller
* Sergey Tseits, as Byram White

==External links==
* 
*   (russian)
*  
*  

 
 
 
 
 


 