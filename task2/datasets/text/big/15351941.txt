Genova (2008 film)
{{Infobox film
| name           = Genova
| image          = Genova poster.jpg
| caption        = Theatrical release poster
| director       = Michael Winterbottom
| producer       = Andrew Eaton
| writer         = Laurence Coriat Michael Winterbottom
| starring       = Colin Firth Catherine Keener Hope Davis
| music          = Melissa Parmenter
| cinematography = Marcel Zyskind
| editing        = Paul Monaghan
| distributor    = 
| released       =  
| runtime        = 94 minutes
| country        = United Kingdom
| language       = English
| budget         = 
}}
Genova (released in the US as A Summer in Genoa) is a film directed by Michael Winterbottom and starring Colin Firth, Catherine Keener, and Hope Davis. It was filmed in the titular city of Genoa (Genova in Italian people|Italian) during the summer of 2007. It was written by Wonderland (1999 film)|Wonderland screenwriter Laurence Coriat. It premiered at the 2008 Toronto International Film Festival  and won the best directors award in the San Sebastián International Film Festival.

==Plot==
Following the death of his wife in a car accident, a college professor (Colin Firth) decides to teach English Literature at an Italian University in Genova.  He is accompanied by his two daughters, aged 16 and 10.  The trio occupies a flat in the crowded Genova streets and soon adapt to the local way of life, taking day trips to the beach and hiring an Italian tutor in musical composition.

The elder daughter begins dating a local Italian teenager, surreptitiously making dates with him behind her fathers back.  The younger daughter remains close to her father, and still deals with painful memories of her mothers death. A passenger in the car herself when her mother was killed, she was directly responsible for the accident and remains haunted by her image. 

The Professor, while enjoying life in Genova, has to deal with the demands of being a single parent while also balancing his re-emergent love life. One romantic interest is a colleague at the university (played by Catherine Keener) with whom he shared a brief romantic relationship back at Harvard when both were students. The colleague tries to get close to the family, helping with translation and their day-to-day needs in Genova, but crossing the thin line between good advice and intrusion in their private ways in the process. Another romantic interest is a young Italian student in the professors literature class. She is brash and idealistic and quickly makes her intentions known to the suddenly single professor.

Matters come to a head one day when the professor makes a lunch date with the Italian student, simultaneously spurning his much older colleague. The elder daughter gets into a fight with her Italian boyfriend and is forced to hitch a ride home from the beach. Meanwhile the younger daughter is left to walk home alone due to the lateness of her older sister and instead follows an apparition of her late mother across a busy intersection, almost killing herself and causing yet another car crash, but luckily a minor one. The movie ends with the two daughters beginning their studies at a local Italian secondary school, eager to start a new chapter as a family, having already learned a great deal about family, love, and mourning, on the colourful streets of Genova.

==Cast==
*Colin Firth as Joe
*Catherine Keener as Barbara
*Hope Davis as Marianne
*Willa Holland as Kelly
*Perla Haney-Jardine as Mary
*Kyle Griffin as Scott
*Kerry Shale as Stephen
*Gherardo Crucitti as Mauro
*Margherita Romeo as Rosa
*Gary Wilmes as Danny
*Demetri Goritsas as Demetri
*Alessandro Giuggioli as Lorenzo

==Production==
 . 

==Music== La Nuit Americaine. Étude Op. 10, No. 3 (Chopin)|Étude No. 3 (Tristesse) by Chopin recurs throughout.

==Reception==
The film was generally well received. It holds a fresh rating of 79% on Rotten Tomatoes.  The film never opened in American theaters, and finally premiered there on DVD in April 2011, immediately after Firths Oscar win, under the alternate title A Summer in Genoa.

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 