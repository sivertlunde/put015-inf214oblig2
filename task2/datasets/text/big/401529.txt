Derrida (film)
{{Infobox film
| name           = Derrida
| image          = Derrida Poster.png
| director       = Kirby Dick Amy Ziering Kofman
| producer       = Amy Ziering Kofman
| writer         =
| narrator       =
| starring       =
| music          = Ryuichi Sakamoto and Robert Miles
| cinematography = Kirsten Johnson  Matthew Clarke
| studio         = Jane Doe Films
| distributor    = Zeitgeist Films
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = English French
| budget         =
| gross          =
}}		

Derrida is a 2002 American documentary film directed by Kirby Dick and Amy Ziering Kofman about the French philosopher Jacques Derrida.  It premiered at the 2002 Sundance Film Festival before being released theatrically on October 23, 2002.

==Synopsis==

The film uses interviews shot by the filmmakers, footage of Derridas lectures and speaking engagements, and personal footage of Derrida at home with his friends and family.  In several scenes, Ziering Kofman also reads excerpts from Derridas work or otherwise describes aspects of his life.

Derrida also focuses on Derridas thesis that scholars tend to ignore important biographical information when discussing philosophers lives. {{cite web
  | url = http://culturemachine.tees.ac.uk/Reviews/rev55.htm
  | title = Derridaphilia
  | last = Thomassen
  | first = Lasse
  | publisher = culturemachine.net
  | accessdate = 2009-08-30 }}    In one scene, Derrida comments that he would be most interested in hearing about famous philosophers sex lives because this topic is seldom addressed in their writings.  The filmmakers respond to many of these criticisms by probing Derrida on various aspects of his own personal life, though he usually refuses to answer directly questions about himself.
 racial issues.

==Analysis==

At several points, Derrida shows the philosopher applying his theory of deconstruction to the film itself. {{cite web
  | url = http://www.guardian.co.uk/film/2003/jan/18/artsfeatures.highereducation
  | title = Lights! Camera! Think!
  | last = Jeffries
  | first = Stuart
  | publisher = guardian.co.uk
  | date = 2003-01-18
  | accessdate = 2009-08-30 }}   Derrida often challenges the filmmaking process and argues against the capability of any film to portray him accurately.  The film also includes metacinematic scenes in which Derrida analyzes previously recorded footage of himself.  In one such scene, Derrida telescopically watches a video of himself analyzing footage of himself.

Nicholas Royle argues that the films labyrinthine, Ouroboros-like structure reinforces several key Derridean tenets:

:"If Dick and Ziering Kofman follow Derrida, Derrida is also following them.  Derrida is a film about following, about the compulsiveness and ghostliness of following, of following the camera, of following the story, of following a film.  But Derrida is also a film about the impossibility of following, about the consequences and effects of Derridas work vis-à-vis the story of a life, about the idea that Derrida cannot tell a story." {{cite book
  | last = Royle
  | first = Nicholas
  | editor1-first = Kirby
  | editor1-last = Dick
  | editor2-first = Amy
  | editor2-last = Ziering Kofman
  | title = Derrida
  | chapter = Blind Cinema
  | publisher = Routledge
  | year = 2005
  | page = 16
  | isbn = 0-415-97407-0}} 

==Reception==

Film critics generally gave Derrida positive reviews; the film has an 82% "fresh" rating on Rotten Tomatoes.   Kenneth Turan of The Los Angeles Times praised the film for its sophisticated style and said it was "the cinematic equivalent of a mind-expanding drug" {{cite web
  | url = http://www.calendarlive.com/cl-et-derrida8nov08,0,5435572.story
  | title = Movie Review: Derrida
  | last = Turan
  | first = Kenneth
  | publisher = latimes.com
  | date = 2002-11-08
  | accessdate = 2009-08-30 }}  while Film Threat  Tim Merrill described it as "a priceless historical record." {{cite web
  | url = http://www.filmthreat.com/index.php?section=reviews&Id=2660
  | title = Derrida
  | last = Merrill
  | first = Tim
  | publisher = filmthreat.com
  | date = 2002-10-27
  | accessdate = 2009-08-30 }}   Other critics, like The Guardian  Peter Bradshaw, found the film whimsical and entertaining but lamented Derridas evasive and mysterious demeanor. {{cite web
  | url = http://www.guardian.co.uk/culture/2003/jan/31/artsfeatures6
  | title = Derrida
  | last = Bradshaw
  | first = Peter
  | publisher = guardian.co.uk
  | date = 2003-01-31
  | accessdate = 2009-08-30 }} 

Derrida received the Golden Gate Award at the 2002 San Francisco Film Festival and screened in competition for the Grand Jury Prize at the 2002 Sundance Film Festival.

==Post-Release==

Derrida enjoyed the film and appeared at several promotional events to discuss the film and answer questions about the project. {{cite book
  | last = Dick
  | first = Kirby
  | editor1-first = Kirby
  | editor1-last = Dick
  | editor2-first = Amy
  | editor2-last = Ziering Kofman
  | title = Derrida
  | chapter = Resting on the Edge of An Impossible Confidence
  | publisher = Routledge
  | year = 2005
  | pages = 47–9
  | isbn = 0-415-97407-0}} 

Derrida died in October 2004.

In 2005, Routledge published a companion book, Derrida, which includes the films screenplay, several essays on the film, and interviews with Derrida, Dick, and Ziering Kofman.  The book describes many of the events that followed the films release, including Derridas unexpected celebrity status on the streets of New York City.  This phenomenon prompted Derridas wife to remark to the filmmakers, "I hear youve made him into Clint Eastwood." 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 