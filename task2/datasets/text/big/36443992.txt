Merah Putih
 
{{Infobox film
| name           = Merah Putih
| image          = Merah Putih 2009 Poster.jpg
| image size     =
| caption        = poster
| director       = Yadi Sugandi
| producer       =
| writer         = Connor Allyn Rob Allyn
| starring       = Donny Alamsyah Lukman Sardi Darius Sinathrya Rudy Wowor
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = 108 minutes
| country        = Indonesia
| language       = Bahasa Indonesia
| budget         =
| gross          =
}}
Merah Putih ("Red   White") is a movie, directed by Yadi Sugandi (director of photography for Laskar Pelangi and the Photograph), brought out in Indonesia in 2009.

==Genre== Cut Nyak Dien are just few of many war movies screened in cinemas years ago, portraying the spirit, struggles and courage of Indonesians in fighting the Dutch and Japanese colonialists to gain independence and freedom.

==Story== Van Mook offensive into the heart of republican territory in Central Java, Merah Putih is a story about young Indonesian cadets who bond together despite their differences in religion, ethnicity, class and culture, to become guerrilla fighters for Indonesias independence. The film was inspired by the real-life experiences of the brave cadets massacred in Lengkong, and all the men and women who fought for a free and united Indonesia between 1945 and 1948. The movie depicts the diversity of Indonesians through its unique and different characters. There is Amir, a quiet and devout Muslim, (played by prominent actor Lukman Sardi) who gives up his job as a teacher and even leaves his beloved wife to become a cadet, while hot-headed Tomas (Donny Alamsyah) comes all the way from Sulawesi to be a soldier in a bid to get revenge against the Dutch soldiers who slaughtered his family. There is also a cocky and elitist playboy Marius (Darius Sinathrya) and his best friend Soerono, who is a priyayi (Javanese aristocrat), as well as the Hindu street youth Dayan (T. Rifnu Wikana).

The movie starts with the tough lives of the aforementioned characters in a military school, showing friendship and even competition among them. But the lives of the young cadets changes when Dutch soldiers suddenly attack their camp, tearing them apart. With limited weaponry and forces, they decided to unite to fight against the Dutch and defend their freedom. Combining action, drama, humor, romance, human tragedy and strong personal stories, the movie is aimed at inspiring the new generation with the spirit of the generations before them who fought and sacrificed themselves for the freedoms Indonesia enjoys today.

===Idea===
The idea of the film first emerged when Allyn asked his close friend Hashim about two old portraits of youths in uniform on his wall. Hashim told Allyn they were pictures of his two uncles, First Lt. R.M. Subianto Djojohadikusumo and Cadet R.M. Sujono Djojohadikusumo, who had died in the battle of Lengkong in 1946. His uncles were the brothers of Sumitro Djojohadikusumo, one of the founding fathers of Indonesia and the economic guru who helped win recognition of Indonesias independence by the United Nations. "I thought it would make a great Hollywood movie!" said Allyn. Hashim and Allyn agreed that the story of brave young Indonesians willing to sacrifice their lives for the independence of Indonesia needed to be retold to the younger generation of Indonesia&nbsp;– and to the world outside, where few people know much about Indonesias long and bloody struggle for independence.

==Shooting the film==
Allyn brought technical experts from  ). The crew shot for about four months in very difficult conditions during the rainy season in Semarang, Yogyakarta, Depok and other places around Indonesia. A fight scene took about four or five days to shoot. A scene about the cadets attacking a Dutch convoy on a bridge, for instance, took six days and hundreds of takes because it involved gunfights and several blasts.

Before the film shoot began, the cast joined a military boot camp for 10 days where they were trained as if they were true military cadets. The daily training consisted of marching, grueling physical training, training in the use of various weapons, and reading the Pancasila (politics)|Pancasila, the national ideology. "Every morning we were woken up at 4 a.m. We had to be on the training site on time and studied Indonesian history on a daily basis," Darius said. "This movie is a reminder for all of us of why we become Indonesia at the beginning. I hope this movie can encourage the young generation to love this country and to make our Indonesia better," Donny said. Despite a series of fighting and explosions scenes, you wont find a spectacular amount of action in this film, especially if you are a war movie lover. Allyn said that this first movie was more of an introduction of the characters and transitions of how they became mature. He promised that there would be more action scenes in the second and third movies.

===Trilogy===
Merah Putih is the first part of the "Merdeka (freedom) Trilogy". The second part, Darah Garuda (Blood of Eagles) was brought out in 2010 and the 3rd part, Hati Merdeka (Hearts of Freedom) in 2011.

==See also==
* List of war films and TV specials#Indonesian National Revolution

==Sources==
* Triwik Kurniasari, THE JAKARTA POST, 08/09/2009
*   Merah Putih on IMDb
*   Darah Garuda (Merah Putih II) on IMDb
*   Hati Merdeka (Merah Putih III) on IMDb

 
 
 
 
 