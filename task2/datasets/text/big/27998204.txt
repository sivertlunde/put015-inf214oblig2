Second Chances (film)
{{Infobox film
| name           = Second Chances
| image          = 
| image_size     = 
| caption        = 
| director       = James Fargo
| writer         = 
| narrator       = 
| starring       =   Isabel Glasser Charles Shaughnessy Madeline Zima
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 1998
| runtime        = 107 min.
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
Second Chances is a 1998 film directed by  , Isabel Glasser, and The Nannys Charles Shaughnessy and Madeline Zima.  "Second Chances" is based on a true story. 


==Plot==
After a car accident kills her dad and damages her leg a 10-year-old girl named Sunny Matthews moves with her over protective mother Kathleen Matthews next door to a horse ranch run by former rodeo star Ben Taylor when theyre forced to sell their house. While adjusting to her new life Sunny develops a fondness for Ben and Ginger a beautiful barrel racing American Quarter Horse. Kathleen doesnt approve of Ben and Ginger, but she eventually trusts them.

==Cast==
*  as Sunny Matthews
*Tom Amandes as Benjamin "Ben" Taylor
*Isabel Glasser as Kathleen Matthews Terry Moore as Dallas Taylor Judd 
*Stuart Whitman as William "Buddy"
*Charles Shaughnessy as Dr. Hugh Olson
*Madeline Zima as Melinda Judd

==References==
 

==External links==
* 

 

 
 