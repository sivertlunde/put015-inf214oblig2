Fire, Water, and Brass Pipes
 
{{Infobox film
| name = Fire, Water, and Brass Pipes
| image = 
| image_size =
| caption = 
| director = Aleksandr Rou
| studio = Gorky Film Studio
| writer = Nikolai Erdman Mikhail Volpin
| narrator =
| starring = Natalya Sedykh Aleksei Katyshev Georgy Millyar Vera Altayskaya Alexander Khvylya Mikhail Pugovkin
| music = Nikolai Budashkin
| cinematography = Dmitri Surensky
| editing =
| distributor = 1968
| runtime = 81 min
| country = Soviet Union Russian
| budget =
| gross =
| preceded_by =
| followed_by =
}} Soviet fantasy film directed by Aleksandr Rou. Its story and characters are derived from Slavic folklore.

==Plot==
There exists a Russian idiom, "to go through  fire, water and brass pipes" (пройти огонь, воду и медные трубы) meaning approximately "to go to hell and back"; in other words, to persevere in the face of extreme adversity. 

The young collier Vasya goes into the forest to collect firewood. In a clearing he spies the lovely Alyonushka grazing her goat Byelochka. As soon as he has fallen in love with the girl, werewolves appear and kidnap her to deliver to the wicked Koshchei. To rescue his beloved, Vasya must go through a literal version of the titular proverb:  first he must pass through the kingdoms of fire and water, then contend with the more challenging "brass pipes", that is, to resist the temptation of fame and flattery.

==Cast==
* Natalya Sedykh — Alyonushka
* Aleksei Katyshev — Vasya
* Georgy Millyar — Koshchei, Baba Yaga
* Vera Altayskaya — daughter of Baba Yaga
* Koshcheis werewolves:
** Lev Potyomkin — Blackbeard
** Alexander Khvylya — Baldy
** Anatoly Kubatsky — Cyclops Leonid Kharitonov — Fedul VI
* Muza Krepkogorskaya — Sofyushka Aleksei Smirnov — Head firefighter
* Pavel Pavlenko — Vodyanoy
* Arkady Tsinman — Adviser
* Zoya Vasilkova — Adviser
* Mikhail Pugovkin — Tsar
* Lidiya Korolyova — Tsarina
* Inga Budkevich — Princess

==References==
 

==External links==
*  

 
 
 
 


 