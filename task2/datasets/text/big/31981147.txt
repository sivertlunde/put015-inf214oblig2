The Place at the Coast
 
 
{{Infobox film
| name           = The Place at the Coast
| image          = 
| caption        = 
| director       = George Ogilvie
| producer       = Hilary Furlong
| screenplay     = Hilary Furlong
| based on       =   John Hargreaves Heather Mitchell Tushka Bergen
| music          = Chris Neal
| cinematography = Jeff Darling
| editing        = Nicholas Beauman
| distributor    = 
| released       = 17 September 1987
| runtime        = 111 min
| country        = Australia
| language       = English
| budget         = A$2.4 million "Australian Productions Top $175 million", Cinema Papers, March 1986 p64 
| gross          = A$57,400 (Australia) 
}} Australian drama John Hargreaves. It is based on the novel of the same name by Jane Hyde. David Stratton, The Avocado Plantation: Boom and Bust in the Australian Film Industry, Pan MacMillan, 1990 p365-366 

==Plot==
Young Ellie McAdams passion and shelter is the pristine landscape surrounding the village of Kilkee on the Australian east coast where she and her father Neil, an abstracted widower, spend peaceful holidays in a ramshackle beach house disrupted by visits from their obstreperous extended family. When Neil is blinded by the sudden rediscovery of love, Ellie finds herself isolated in her opposition to a development that will destroy the landscape forever.

==Cast== John Hargreaves as Neil McAdam
*Heather Mitchell as Margot Ryan
*Tushka Bergen as Ellie McAdam
*Julie Hamilton as Enid Burroughs
*Aileen Britton as Gran
*Willie Fennell as Fred Ryan
*Michele Fawdon as Aunt Helen

==Nominations==
Australian Film Institute Awards 

*Australian Film Institute Award for Best Screenplay - Hilary Furlong
*Australian Film Institute Award for Best Actress in a Supporting Role - Julie Hamilton Owen Paterson
*Australian Film Institute Award for Best Costume Design - Anna French

==References==
 

==External links==
*  

 

 
 
 
 
 
 

 