La Danse (film)
{{Infobox film
| name           = La Danse: The Paris Opera Ballet
| image          = la_danse_le_ballet_de_lopera_de_paris.jpg
| caption        = Theatrical release poster
| director       = Frederick Wiseman
| released       =  
| runtime        = 159 minutes
| country        = France
| language       = French
}} Paris Opera Ballet. The ballets featured are:
* Genus (Wayne McGregor)
* La maison de Bernarda (Mats Ek)
* Le songe de Médée (Angelin Preljocaj)
* Paquita (Pierre Lacotte, after Joseph Mazilier and Marius Petipa)
* Orpheus und Eurydike (Pina Bausch)
* Roméo et Juliette (Hector Berlioz|Berlioz) (Sasha Waltz) The Nutcracker (Rudolf Nureyev version)  

The film proceeds without commentary,  and there are no voiceovers. 
 Dancers from the Paris Opera Ballet who appear in the film include the étoiles Kader Belarbi, Jérémie Belingard, Émilie Cozette, Aurélie Dupont, Mathieu Ganio, Dorothée Gilbert, Marie-Agnès Gillot, Manuel Legris, Nicolas Le Riche, Agnès Letestu, José Carlos Martínez (dancer)|José Martinez, Hervé Moreau, Delphine Moussin, Clairemarie Osta, Benjamin Pech, Laetitia Pujol and Wilfried Romoli,  and the premiers danseurs Yann Bridard, Stéphane Bullion, Isabelle Ciaravola, Nolwenn Daniel, Christophe Duquenne, Eve Grinsztajn, Mathias Heymann, Mélanie Hurel, Myriam Ould-Braham, Karl Paquette, Stéphane Phavorin, Stéphanie Romberg, Emmanuel Thibault and Muriel Zusperreguy. 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 


 
 