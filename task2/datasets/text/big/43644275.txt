Maria Leonora Teresa
{{Infobox film
| name = Maria Leonora Teresa
| image = 
| caption = Theatrical release poster
| director = Wenn V. Deramas
| producer = Malou N. Santos   Charo Santos-Concio
| writer = Wenn V. Deramas   Keiko A. Aquino
| starring = Iza Calzado   Zanjoe Marudo   Jodi Sta. Maria
| music = Idonnah C. Villarico   Rommel C. Villarico
| cinematography = 
| editing = Marya Ignacio
| distributor = Star Cinema
| released =  
| runtime = 
| country = Philippines
| language = Filipino language|Filipino, English
| budget =
| gross = 
}}
 Guy and Tirso Cruz III|Pips doll of the same name.

==Synopsis==
After a tragic incident during their field trip, three parents Faith (Calzado), Julio (Marudo), and Stella (Sta. Maria) mourned the deaths of their respective daughters: Maria, Leonora, and Teresa. To help cope with their loss, a psychiatrist named Manolo (Villanueva) offers them life-sized dolls to look after. But the situation only worsens when the toys appear to be brought to life by a sinister force.

==Cast==
* Jodi Sta. Maria as Stella de Castro
* Iza Calzado as Faith Pardo
* Zanjoe Marudo as Julio Sacdalan
* Rhed Bustamante as Maria Ann Pardo/Voice of Maria Doll
* Jonicka Cyleen Movido as Leonora Vera/Voice of Leonora Doll
* Juvy Lyn Bison as Teresa de Castro/Voice of Teresa Doll
* Marco Masa as Eldon Jacinto
* Dante Ponce as Stanley Pardo
* Joem Bascon as Don de Castro
* Cris Villanueva as Manolo Apacible
* Joey Paras as Augusto
* Maria Isabel Lopez as Linda
* Robert Bermudez as Robert
* Niña Dolino as Cherie
* Tess Antonio as Shirley
* Ruby Ruiz as Principal Josephine Punongbayan
* Eagle Riggs as Teacher Danilo
* Dang Cruz as Teacher Socorro
* Atak Arana as School Janitor
* Jaycee Domincel as Bus Driver
* June Macasaet as Mr.Tenorio

==Supporting Cast==
* John Jeffrey Carlos as Co-teacher
* Paolo Rodriguez as Policeman
* Mike Lloren as Eldons Guardian
* Daisy Carino as School Teacher
* Giovanni Baldeserri as Mayor
* Olive Cruz as Dra.Ana Fajardo
* Eric Sison as Psychiatrist
* Evelyn Santos as Stellas maid
* Roi Calilong as Driver
* Raul Montesa as Faiths Lawyer

==Reception==
The film received an R-13 rating from the MTRCB for its graphic violence. Reviews for the film were mixed to positive, with some critics praising the first act of the film for its atmosphere, while criticizing the final act to be too funny to be scary. Jodi Sta. Maria, Iza Calzado and Zanjoe Marudo were also praised for their performance.

==Deaths==
* Eldon Apacible (Died at the fire in school)
* Maria Ann Pardo (Died at the school bus)
* Leonora Vera (Died at the school bus)
* Teresa de Castro (Died at the school bus)
* Principal Josephine Punongbayan (Killed by Leonora)
* Shirley (Killed by Maria, but later revived)
* Cherie (Killed by Teresa)
* Linda (Killed by Teresa)
* Don de Castro (Killed by Teresa)
* Stanley Pardo (Killed by Maria, Leonora and Teresa while Driving)
* Julio Sacdalan (Killed by Dr. Manolo Apacible)
* Dr. Manolo Apacible (Killed by Julio)

==External links==
* 

==References==
 

 
 
 
 
 
 
 
 
 