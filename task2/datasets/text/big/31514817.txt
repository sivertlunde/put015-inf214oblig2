Memories of Overdevelopment
{{Infobox film
| name = Memories of Overdevelopment
| image =
| director = Miguel Coyula
| producer = David Leitner
| executive Producer = Steve Pieczenik, Suzana Dejkanovic
| writer = Miguel Coyula based on a novel by Edmundo Desnoes
| starring = Ron Blair Susana Pérez Lester Martínez
| music = Dika Durbuzovic, Hayes Greenfield, Miguel Coyula
| distributor = Pirámide
| released =  
| runtime = 112 minutes
| country = Cuba
| language = Spanish, English
}}
Memories of Overdevelopment ( ) is a 2010 Cuban film. Written and directed by Miguel Coyula, the story is based on a novel by Edmundo Desnoes, also the author of the 1968 classic Memories of Underdevelopment. This independent film was produced by David Leitner and features Cuban actor, Ron Blair as the lead character. It is the first Cuban dramatic feature film with scenes filmed both in Cuba and the United States. After its world Premiere at the 2010 Sundance Film Festival, it went to gather several awards and honors. The International Film Guide described it as one of the best films Cuba has produced.   

==Story==
Sergio Garcet is an intellectual who abandons the Cuban Revolution and underdevelopment behind only to find himself at odds with the ambiguities of his new life in the developed world.  A portrait of an alienated man, an outsider with no clear-cut politics or ideology: A stranger in a strange land struggling with old age, sexual desire and ultimately, the impossibility for the individual to belong in any society.

Highly episodical, the films narrative is a collage of flashbacks, daydreams, and hallucinations comprising live-action, animation, and newsreel footage assembled to suggest the way personal memory works, subjectively and emotionally.

==Production==

Production of “Memorias del Desarrollo” took five years, starting in 2005 and completed in 2010, with locations in five countries.  According to Director Miguel Coyula, his first draft of the screenplay was a faithful adaptation of Edmundo Desnoes novel.  But as he began filming and editing simultaneously, he became aware of new possibilities, which would radically alter the narrative structure of the original novel as well as its characters. The film did not follow the traditional chronology of writing, shooting and editing.  This required a new approach where pre-production, production and postproduction no longer needed to exist as distinct elements but rather as an integral interactive seamless process.  This new way of working was not meant as a shortcut, but rather as a way of revealing new narrative layers of the work as it evolved to create the director’s personal vision. 

Coyula would take advantage of everything around him that he thought would add to the film, heavily manipulating images digitally, into the film’s final highly fragmented narrative.  For example, the scenes with the 9/11 footage, which the director had recorded years before; the Paris, London and Tokyo scenes, which were shot while Coyula was travelling invited to film festivals presenting his film Red Cockroaches.  Most of the various location shoots were done without permits, trough favors from friends, and with a minimal crew consisting mostly of director Miguel Coyula, producer David W. Leitner and lead actor Ron Blair, easily adapting at a moment’s notice and without the constraints of a rigid schedule. The films most decisive work however took place in the editing room. In several viusal presentations Coyula stated that no shot in the film escaped digital manipulation of some kind, which included, green screen, compositing in recreating sets, double exposures, replacing backgrounds, changing lighting, weather conditions, performing digital art direction, and actors shot in different countries performing together on screen. 

Producer David Leitner initially hoped to raise 2 million dollars for the project. But after realizing there was no interest from film production companies neither in North or South America, they reverted to a strategy closer to Coyulas first feature, Red Cockroaches. The film was funded by executive producers Steve Pieczenik, and Suzana Dejkanovic, associate producers Juan Martinez, and Michael Ferris Gibson as well as the Guggenheim Fellowship Coyula was awarded in 2009 for this project. While working on a low budget delayed completion by five years, it also allowed Coyula to have total creative control of the film.

==Reception==

Memorias del Desarrollo received almost unanimous positive reviews throughout its Film Festival run.  An initial negative review by Variety’s Robert Koehler   after its premiere at Sundance, described the film as  “an unfortunate follow up to Tomas Gutierrez Alea’s masterpiece… the erratically edited film misses Desnoes rhythm and poetics, coming off as borderline amateur.”  Subsequently James Greenberg from Hollywood Reporter considered it “Thoughtful and cinematically bold... an affecting portrait of modern man becoming more and more isolated from a world he helped to create.”    Bérénice Reynaud at Senses of Cinema wrote “Coyula ups the ante by over-compositing the image and saturating the colors, creating a complex kaleidoscope, which in turns dictates the editing of the sequences in a kinetic, non-linear way.  Paralleling free association, the structure blurs the lines between actual events, fake memories, projections, irritation at American pop culture, political anger, rambling and resentments… These techniques allow Coyula to further destabilize his character, to create a palpable tension between the film and Desnoes’ text, and to eventually reappropriate (or salvage) his trajectory in exile.”   Latin American Film Scholar Michael Chanan declared it “A dazzling and disconcerting work of great bravado… A paradigm for a new digital cinema beholden to no orthodoxy.”   Cuban writer and critic Orlando Luis Pardo wrote in Diario de Cuba that the film was “A memorable moment in the Cuban Arts”   Cinema Without Border’s critic Robin Menken chose it as the Best Film of 2010.  The International Film Guide described it in its 2011 survey as “One of the Best Films Cuba has produced”   However the film remains largely unseen outside of Cuba, due to the lack of a distributor.

==Honors==

* Best Cuban Film of 2010 by the International Film Guide.
* Chosen among the 10 Best Cuban Films of 2010 by the ACPC (Cuban Association of Cinema Press)
* Best Film of 2010 by Cinema Without Borders

==Awards==

* Audience Award for Best Foreign Feature Film, Arraial CineFest, Brazil, 2012
* Best Narrative Feature, Extreme World Cinema Festival San Sebastián de Veracruz, Mexico, 2012
* Best Director, Latinamerican Territory, Málaga Film Festival, Spain, 2011
* Cine Latino Award, Washington DC Independent Film Festival, USA, 2011
* Special Jury Award, Encuentro Nacional de Video, Cuba, 2011
* Special Jury Award, El Almacén de la Imagen, Cuba, 2011
* FIPRESCI Award, Caracol Awards, Cuba, 2011
* Mention, Caracol Awards, Cuba, 2011
* UNEAC Award, Encuentro Nacional de Video, Cuba, 2011
* Best Film, Havana New Filmmakers Film Festival, Cuba, 2011
* Best Original Music, Havana New Filmmakers Film Festival, Cuba, 2011
* FIPRESCI Award, Havana New Filmmakers Film Festival, Cuba, 2011
* SIGNIS Award, Havana New Filmmakers Film Festival, Cuba, 2011
* Editora Musical Award, Havana New Filmmakers Film Festival, Cuba, 2011
* Special Award, ACE Awards, USA, 2011
* Most Innovative, Cero Latitud Film Festival, Ecuador, 2010
* Best Narrative Feature, Dallas Video Fest, USA, 2010
* Best Feature, New Media Film Festival, USA 2010
* Special Mention, Cine Las Americas International Film Festival, USA, 2010
* Best Film, Havana Film Festival New York, USA, 2010

==See also==
 
* Cinema of Cuba

==References==
 

==External links==
*  
* "Best Cuban Films of 2010"  . International Film Guide. Abu Dhabi Film Commission.
* Greenberg, James (14 October 2010) " ", The Hollywood Reporter - review
* Menken, Robin (23 January 2011) " " Cinema Without Borders - review
* Reynaud, Bérénice (11 July 2010) " ". Senses of Cinema, Issue 55. - review

 
 
 
 
 