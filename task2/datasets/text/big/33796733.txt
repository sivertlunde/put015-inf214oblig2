Grandpa's Love
 
Grandpas Love (Chinese name:祖孙情)is a Chinese film about the relationship between a grandfather and his grandson. The film starred Jimmy Lin (林志颖).

== Cast ==
* Jimmy Lin(林志颖) as Jiang XingJian(江行健)　
* Hung Long(郎雄) as Grandpa(爷爷)
* Siu-Man Fok(郝劭文) as Jiang XingKang(江行康)

== Basic information ==
Director: Yin-Ping Chu(朱延平)
Video Title: Grandpas Love
Chinese name: 祖孙情
Dialogue language: South Fujian Dialect(闽南语)
Production area: Taiwan(台湾)

== Synopsis ==
Jiang Xingjian and his four-year-old little brother parents die in a plane crash. Their two boys have to live with their grandfather, who is the leader of a big company. The grandfather hopes that Ken (阿健) can take responsibility for the whole family, especially his little brother. He requires Ken not to tell the little brother Allcorn (阿康) about their parents death. Ken wants to set a good example, but he is misunderstood by his grandfather because of Allcorns faults. Under his uncles advice, his grandfather decides to re-recognize him. While Ken is absorbed in the harmonious relationship established between him and his grandfather, Allcorn accidentally damages his precious video, taken on his birthday. He tries to repair it but fails. He is so depressed that get drunk. His grandfather decides to send him to boarding school after finding out. Ken is very upset and he climbs a tall tree. He falls, landing on a large rock, due to Allcorns unexpected appearance. The grandfather finds the unfinished letter and has a better understanding of Ken. However, Ken is gone.

== See also ==
* Jimmy Lin
* Chu Yen-ping

== External links ==
* 
* 
* 
*  
*  
*  

== References ==
 

 

 
 
 
 

 