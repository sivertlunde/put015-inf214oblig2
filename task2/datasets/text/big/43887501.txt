Katha Nayagan (1997 film)
{{Infobox film
| name           = Kadhanayakan
| image          =
| image_size     =
| caption        =
| director       = Rajasenan
| producer       =
| writer         = Mani Shornnur Rajan Kizhakkanela (dialogues)
| screenplay     = Mani Shornnur
| starring       = Jayaram Kalamandalam Kesavan Divya Unni Oduvil Unnikrishnan
| music          = Mohan Sithara
| cinematography = K. P. Nambyathiri
| editing        = G. Murali
| studio         =
| distributor    =
| released       =  
| country        = India Malayalam
}}
 1997 Cinema Indian Malayalam Malayalam film,  directed by  Rajasenan. The film stars Jayaram, Kalamandalam Kesavan, Divya Unni and Oduvil Unnikrishnan in lead roles. The film had musical score by Mohan Sithara.  

==Cast==
 
*Jayaram as Ramanathan
*Kalamandalam Kesavan as Payyarathu Padmanabhan Nair
*Divya Unni as Gopika
*Oduvil Unnikrishnan as Sankunni
*K. P. A. C. Lalitha as Kunjulakshmi
*Kalabhavan Mani as Kuttan
*Janardanan as Sathrughnan Pillai
*Bindu Panicker as Meenakshi
*Indrans as Sreedharan
*Kochu Preman as Vamanan Nampoothiri
*K. T. S. Padannayil as Konthunni Nair
*V. K. Sreeraman as Madhavan Nair
*T. P. Madhavan as Krishna Menon
*Zeenath as Ammalu
*Mamukkoya as Beerankutty
*Sona Nair
*Bobby Kottarakkara as Sadasivan Nair
*Cherthala Lalitha
*Bindu Ramakrishnan
 

==Soundtrack==
The music was composed by Mohan Sithara. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aalmaram || K. J. Yesudas || Ramesan Nair || 04.59
|-
| 2 || Dhanumaasa Penninu || K. J. Yesudas || Ramesan Nair || 04.55
|}

==References==
 

==External links==
*  

==External links==
*  

 
 
 


 