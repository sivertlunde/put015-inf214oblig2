65 Revisited
{{Infobox film
| name           = 65 Revisited
| image          =
| caption        =
| director       = D. A. Pennebaker
| producer       =
| writer         = D. A. Pennebaker
| narrator       =
| starring       = Bob Dylan Joan Baez Bob Neuwirth Nico
| music          = Bob Dylan
| cinematography = D. A. Pennebaker
| editing        = Walker Lamond
| released       =  
| runtime        = 65 min.
| country        = United States English
| budget         =
| gross          =
}}
65 Revisited is a 2007 American documentary film by D. A. Pennebaker, made from footage the director shot for his famous 1967 film, Dont Look Back. Both films show Bob Dylan and entourage during their 1965 concert tour of the United Kingdom|U.K. The newer film shows outtakes from its predecessor and contains several full-length song performances, something the first film did not provide.

The film opened in theaters on November 28, 2007.  It is also available as the second disc in a DVD reissue called Bob Dylan: Dont Look Back - 65 Tour Deluxe Edition. 

Besides Dylan himself, persons appearing in 65 Revisited include Joan Baez, Bob Neuwirth, and Nico. 

==Song performances==
Songs featured in 65 Revisited are: 
*"Dont Think Twice, Its Alright"
*"Wild Mountain Thyme" (a duet with Joan Baez|Baez)
*"Love Minus Zero/No Limit"
*"To Ramona" 
*"Its All Over Now, Baby Blue"
*"The Lonesome Death of Hattie Carroll"
*"Its Alright, Ma (Im Only Bleeding)"
*"It Aint Me Babe"
*"If You Gotta Go, Go Now"
*"She Belongs to Me"
*"It Takes a Lot to Laugh, It Takes a Train to Cry"
*"Ill Keep It With Mine"

==Cue card scene reprise== Tom Wilson substitutes for the originals Allen Ginsberg.

==References==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 


 