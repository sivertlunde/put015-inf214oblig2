Annadaata
 
{{Infobox film
| name           = Annadaata
| image          = annadaata.jpeg
| image_size     = 7 kbps
| border         = 
| alt            = 
| caption        = 
| film name      = Annadaata
| director       =  Ravi Kinagi
| producer       = Kusum Dhanuka
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Prosenjit Chatterjee Sreelekha Mitra
| music          = Babul Bose
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 2002
| runtime        = 
| country        = India Bengali
| budget         = 
| gross          = 
}}
 Bengali film directed by Ravi Kinagi and produced by  Kusum Dhanuka under the banner of  Eskay Movis. The film features actors Prosenjit Chatterjee and Sreelekha Mitra in the lead roles. Music of the film has been composed by Babul Bose     The film was a remake of Hindi film Swarg.

== Cast ==
* Prosenjit Chatterjee as Shankar
* Sreelekha Mitra as Barsha Raj Rajjak as Amar Chowdhury
* Anuradha Ray as Shanti Devi
* Arindam Seal as son of Amar Chowdhury
* Dulal Lahiri as Rudraprasad Sen
* Bharat Kaul as son of Rudraprasad
* Sunil Mukherjee as Kesto da
* Moumita Gupta
* Arun Banerjee as Vikram
* Jayanta Dutta Burman as son of Amar Chowdhury
* Sandipa Bandyopadhyay
* Shyamal Dutta
* Siddhanta Mahapatra as Special appearance in an item song named "Dhekhi joto soundoro

== References ==
 

 
 
 
 

 