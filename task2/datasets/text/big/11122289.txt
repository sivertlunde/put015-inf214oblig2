The Noose (film)
{{Infobox film
| name           = The Noose
| image          =File:The Noose lobby card.jpg
| caption        =Lobby card John Francis Dillon Henry Hobart Richard A. Rowland
| writer         = H.H. Van Sloan (story and play) Willard Mack (play) James T. ODonohoe (adaptation) Garrett Graham (titles)
| starring       = Richard Barthelmess
| cinematography = James Van Trees
| editing        = Jack Dennis
| distributor    = First National Pictures
| released       =  
| runtime        = 8 reels (7,331 feet)
| country        = United States
| language       = Silent film (English intertitles)
}} silent film The Noose, John Francis Dillon and Richard Barthelmesss performance was nominated for the Academy Award for Best Actor. 

The film survives at the Museum of Modern Art, New York.

==Cast==
*Richard Barthelmess - Nickie Elkins
*Montagu Love - Buck Gordon
*Robert Emmett OConnor - Jim Conley
*Jay Eaton - Tommy
*Lina Basquette - Dot
*Thelma Todd - Phyllis Ed Brady - Seth McMillan
*Fred Warren - Dave, Pianist
*Alice Joyce - Mrs. Bancroft
*Will Walling - Warden (*billed William Walling)
*Robert T. Haines - Governor
*Ernest Hilliard - Craig
*Emile Chautard - Priest
*Romaine Fielding - Judge
*Yola dAvril

uncredited
*William B. Davidson - Bill Chase
*Mike Donlin - Waiter
*Joseph W. Girard - Captain of the Guards
*Bob Kortman - Death Row Convict
*Ivan Linow - Death Row Convict
*Charles McMurphy - Bar Patron
*Monte Montague - Guard
*George H. Reed - Death Row Inmate
*Hector Sarno - Rival Hood
*Harry Semels - Waiter
*Charles Sullivan - Head Waiter

==References==
 

==External links==
*  at the Internet Movie Database
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 


 