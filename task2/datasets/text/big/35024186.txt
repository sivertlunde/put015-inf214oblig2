Cloudy with a Chance of Meatballs 2
 
{{Infobox film
| name           = Cloudy with a Chance of Meatballs 2
| image          = Cloudy with a Chance of Meatballs 2.jpg
| alt            =
| caption        = Theatrical release poster
| director       = {{Plainlist|
* Cody Cameron
* Kris Pearn }}
| producer       = {{Plainlist|
* Pam Marsden 
* Kirk Bodyfelt  }}
| screenplay     = {{Plainlist|
* John Francis Daley Jonathan Goldstein
* Erica Rivinoja }}
| story          = {{Plainlist| Phil Lord Chris Miller
* Erica Rivinoja  }}
| based on       = Characters created by Judi Barrett Ron Barrett
| starring       = {{Plainlist|
* Bill Hader
* Anna Faris
* James Caan
* Will Forte
* Andy Samberg
* Benjamin Bratt
* Neil Patrick Harris
* Terry Crews
* Kristen Schaal }}
| music          = Mark Mothersbaugh 
| editing        = Robert Fisher, Jr. 
| studio         = Sony Pictures Animation
| distributor    = Columbia Pictures
| released       =  
| runtime        = 95 minutes  
| country        = United States
| language       = English
| budget         = $78 million   
| gross          = $274.3 million 
}} Cloudy with Judi and book of the same name. It was directed by Cody Cameron and Kris Pearn, produced by Kirk Bodyfelt, and executive produced by the directors of the first film, Phil Lord and Chris Miller.  The film was released on September 27, 2013.  The film grossed over $274 million worldwide.
 Jonathan Goldstein, and Erica Rivinoja,  and it is based on an original story idea, not on that of Pickles to Pittsburgh, the Barretts follow up book.  It continues right after the first film, in which Flints food-making machine gets out of control, but Flint manages to stop it with the help of his friends. In the sequel, Flint and his friends are forced to leave their home town, but when the food machine reawakens—this time producing sentient food beasts—they must return to save the world.

Most of the main cast reprised their roles: Bill Hader as Flint Lockwood, Anna Faris as Sam Sparks, James Caan as Tim Lockwood, Andy Samberg as Brent McHale, Neil Patrick Harris as Steve, and Benjamin Bratt as Manny. Will Forte, who voiced Joseph Towne in the first film, voices Chester V in this one. New cast includes Kristen Schaal as orangutan Barb and Terry Crews as Officer Earl, replacing Mr. T in the role. 

==Plot==
After Flint Lockwood and his friends save the world from the food storm in the first film, super-inventor Chester V, the CEO of Live Corp, is tasked to clean the island. He relocates Flint, his friends, and the citizens of Swallow Falls to San Franjose, California. Unbeknownst to Flint, the FLDSMDFR survived the explosion and landed in the center of the island, and Chester is determined to find it. Chester invites Flint, his biggest fan, to work at Live Corp, where he meets Chesters assistant Barb, a talking orangutan with human intelligence. Six months later, Flint humiliates himself during a promotion ceremony when his invention, the "Celebrationator", explodes. Meanwhile, Chester is informed that his search-parties on the island have been attacked by monstrous cheeseburgers named Cheespiders (a combination of a Cheeseburger and a Spider) which are learning how to swim. Seemingly fearing the worlds inevitable doom, Chester tasks Flint to find the FLDSMDFR and destroy it once and for all. Despite Chesters demands to keep the mission classified, Flint recruits his girlfriend, meteorologist Sam Sparks; her cameraman Manny; police officer Earl Devereaux; Steve, a monkey who communicates via a device on his chest; and "Chicken" Brent. Much to Flints dismay, his father Tim joins the crew and they travel to Swallow Falls on his fishing boat.

Upon arriving back at Swallow Falls, they notice that a jungle-like environment made of food has overgrown the island. Tim stays behind while Flint and the others investigate, finding a vast habitat of living food animals called foodimals and meet a cute strawberry named Barry (named by Sam). Tim, searching for sardines at his abandoned tackle shop, encounters a family of humanoid pickles and bonds with them by fishing. Chester discovers that Flint allowed his friends to join him on the mission, so he travels to the island with Barb, chagrined and determined to separate them, and he arrives just in time to save them from a Cheespider. Flint then finds his old lab and invents a device to find the FLDSMDFR. After escaping a Tacodile attack, Sam notices that the foodimal was protecting its family, and begins to suspect Chester is up to no good. Sam attempts to convince Flint to spare the foodimals, but Flint is intent on making Chester proud. Sam leaves in anger, and Flints other companions go with her (including Steve). In the jungle, Manny confirms Sams suspicions when he reverses the Live Corp logo to reveal the "Live" as "Evil" spelled backwards. In addition, Sam proves that the foodimals mean no harm by taming a Cheespider. They also learn that the foodimals had known the truth about Live Corp before. Upon realizing Chesters intentions, the group is then ambushed by Evil Corp employees.

Flint finds the FLDSMDFR, but notices a family of cute marshmallows and becomes hesitant to destroy the machine. Chester immediately seizes control of the FLDSMDFR and announces his plot to make his updated line of food bars out of the foodimals. A crushed Flint is knocked into the river but rescued by the marshmallows. Flint is taken to his father, and they and the foodimals all work together to allow Flint and Barry to infiltrate the Evil Corp building that is under construction on the island. Flint frees the trapped foodimals and confronts Chester, who threatens to make food bars out of his friends. Chester makes several holograms of himself to overwhelm Flint, but Flint uses the Celebrationator to expose the real Chester, allowing Flint to rescue his friends. An army of foodimals led by Barry arrive and fight Chesters employees. Chester tries to make off with the FLDSMDFR, but is thwarted by Barb, who has a change of heart and Chester gets eaten by a Cheespider. With the island safe from Chester V and Evil Corp destroyed, Flint returns the FLDSMDFR to its place and frees it from Chesters control and the foodimals continue to live in peace as more are born. The film ends with Flint fishing with his father for the first time, finding it enjoyable.

==Cast==
{{multiple image footer = Bill Hader, Anna Faris and Terry Crews at the 2013 San Diego Comic-Con International footer_align = center
|image1=Bill Hader, 2013 San Diego Comic Con-cropped.jpg
|width1=70
|image2=Anna Faris, 2013 San Diego Comic Con-cropped.jpg
|width2=70
|image3=Terry Crews, 2013 San Diego Comic Con-cropped.jpg
|width3=70
|header_align=center
}}
* Bill Hader as Flint Lockwood, an inventor. 
** Bridget Hoffman as Young Flint Lockwood. She replaces Max Neuwirth for the role.
* Anna Faris as Samantha "Sam" Sparks, a weather intern from New York City and Flints girlfriend. 
* James Caan as Tim Lockwood, Flints widowed father. 
* Will Forte as Chester V, a world-famous super-inventor and the head of Live Corp.
* Andy Samberg as Brent McHale, an infamous/former celebrity mascot of Baby Brents Sardines, now known as Chicken Brent. 
* Neil Patrick Harris as Steve the Monkey, Flints pet Vervet monkey who communicates using a Speak and Spell monkey thought translator Flint invented; however, he only has a limited vocabulary and mostly just says his name, says a few random things, and reminds Flint that hes hungry. 
* Benjamin Bratt as Manny, Sams Guatemalan cameraman and a doctor, pilot, and comedian. 
* Terry Crews as Officer Earl Devereaux, the towns athletic cop.  Crews replaced Mr. T for the role.
* Kristen Schaal as Barb, Chester Vs talking and lipstick-wearing orangutan with a human brain.    
* Khamani Griffin as Cal Devereaux, Earls son. Griffin replaced Bobbe J. Thompson for the role.
* Al Roker as Patrick Patrickson, the anchorman of the weather station.
* Cody Cameron as Barry the Strawberry and Dill Pickles.   
* Melissa Sturm as Sentinel Louise and Live Corp Scientist.
* Kris Pearn as Shrimpanzees,  Sentinel Peter, and Labcoat Jenny.
* Craig Kellman as Flintly McCallahan and Idea Pants Guy.

==Production==
 ]] Jonathan Goldstein, Ron and Judi Barretts follow-up book.    In February 2012, it was announced that the sequel would be titled Cloudy 2: Revenge of the Leftovers,  but it was later retitled Cloudy with a Chance of Meatballs 2.  The film was originally scheduled for release on December 20, 2013,  then pushed to February 7, 2014,  before moving up to September 27, 2013.   

Bill Hader, Anna Faris, James Caan, Andy Samberg, Neil Patrick Harris, and Benjamin Bratt reprised their roles.    The role of Earl, the town cop, was taken over by Terry Crews, since Mr. T declined to return.    Kristen Schaal joined the cast to voice Barb, a talking and lipstick-wearing orangutan with a human brain.  Will Forte, who voiced Joseph Towne in the first film, voices Chester V, a world-famous super-inventor who commands Barb and is the head of the Live Corp Company.  On January 17, 2013, concept art from the film was released. 
 2013 album, was featured in the film. 

==Release==
  ]]

Cloudy with a Chance of Meatballs 2 was released theatrically on September 27, 2013. 
 Subway Kids Meals with a set of 6 customized bags. 

===Home media===
Cloudy with a Chance of Meatballs 2 was released on DVD and Blu-ray on January 28, 2014.    The home media was accompanied with four short animated films based on the main feature: Super Manny, Earl Scouts, Steves First Bath, and Attack of the 50-Foot Gummi Bear.  Two of the shorts, Super Manny and Earl Scouts, were already released online before the media release, premiering in October 2013 on Univision        and Fandango (ticket service)|Fandango,      respectively. David Feiss directed all four shorts,    which feature a computer-generated wraparound animation and a hand-drawn animation, provided by Six Point Harness.   

==Reception==

===Critical response===
On review aggregator site  , gave the film a score of 59 out of 100, based on 30 critics, indicating "mixed or average reviews". 
 Ice Ages; Monsters University; Planes (film)|Planes; Epic (2013 film)|Epic; Despicable Me 2; and though I could go on, I wont."  Jordan Hoffman of the New York Daily News gave the film four out of five stars, saying "Cloudy 2 is loud, weird and chaotic — just as kids like it. Theres plenty of screaming and running while arms flail about, and even the obligatory message bit is given a healthy dose of yeah, yeah, yeah. Your car ride back from the theater wont be a quiet one, but sometimes its good to have a sugary treat."  Dave McGinn of The Globe and Mail gave the film two out of four stars, saying "Unfortunately, the film promises more fun and laughs than it delivers, and this meal tastes like too many that have gone before it."  Bill Goodykoontz of The Arizona Republic gave the film four out of five stars, saying "Its the rare   that takes the spirit of the original and runs with it, coming up with something uniquely good in its own right." 

Tim Robey of   gave the film three out of four stars, saying "At times it felt as if this film might challenge Pixars decade-long reign, but that promise wanes. Instead, the movie is sometimes so strange, colorful and wildly cute that it may end up becoming a Yellow Submarine for a new generation."  Tom Russo of The Boston Globe gave the film two out of four stars, saying "Its another brightly rendered effort, but, as the title indicates, a lot of the real creativity seems to have been used up the first time around."  Mike Clark of USA Today gave the film two and a half stars out of four, saying "Theres not a surprise or moment of tension to be found here, but the film is all energy and color that makes the discomfort of 3-D glasses seem worth it." 
 Jurassic Park conjured up by Tex Avery, Cloudy 2 is a sight to behold ... as long as your brain hasnt turned to mush by the halfway point." 

Amy Nicholson of The Village Voice gave the film a positive review, saying "The Cloudy with a Chance of Meatballs franchise takes its comic cues from The Muppets and Pee Wees Playhouse, kids shows that ripen as their audience matures."  Keith Staskiewicz of Entertainment Weekly gave the film a B, saying "While no one was exactly crying out for second helpings of this animated meat-eorology franchise, Cloudy With a Chance of Meatballs 2 is charming enough on its own not to feel like just reheated leftovers."  Steve Davis of The Austin Chronicle gave the film two out of five stars, saying "For both kids and adults, CWCM2 is little more than a vague memory as soon as its over."  Peter Debruge of Variety (magazine)|Variety gave the film a positive review, saying "What Erica Rivinoja, John Francis Daley and Jonathan Goldsteins script lacks in lingering nutritional value, it compensates for with amusing food puns. If nothing else, the pics zany tone and manic pace are good for a quick-hit sugar high."    Betsy Sharkey of the Los Angeles Times gave the film three and a half stars out of five, saying "Honestly, anyone who can pull off a running joke about leeks that does not make you gag, and is in fact a silly delight, deserves props."  Bill Zwecker of the Chicago Sun-Times gave the film three out of four stars, saying "Unlike so many sequels, this fun-filled 3D adventure is sure to entertain younger kids but also charm the adults who will be accompanying them to the multiplexes." 

===Box office===
Cloudy with a Chance of Meatballs 2 grossed $119,793,567 in North America, and $154,532,382 in other countries, for a worldwide total of $274,325,949.  In North America, the film earned $9.3 million on its opening day,  and opened to number one in its first weekend, with $34,017,930.  In its second weekend, the film dropped to number two, grossing an additional $20,950,192.  In its third weekend, the film dropped to number three, grossing $13,774,742.  In its fourth weekend, the film dropped to number five, grossing $9,672,791. 

===Accolades===
{| class="wikitable" style="width:95%;"
|- style="background:#ccc; text-align:center;"
! colspan="5" style="background: LightSteelBlue;" | Awards
|- style="background:#ccc; text-align:center;"
! Award
! Category
! Recipients and nominees
! Result
|-
| Kids Choice Awards 
| Favorite Animated Movie
|
|  
|-
| Satellite Awards   Best Motion Picture, Animated or Mixed Media
|
|  
|- Visual Effects Society Awards 
| Outstanding Animation in an Animated Feature Motion Picture
| Peter Nash, Michael Ford, Chris Juen, Mandy Tankenson
|  
|-
| Outstanding FX and Simulation Animation in an Animated Feature Motion Picture
| Andrew Hofman, Alex Moaveni
|  
|- British Academy Childrens Awards 
| BAFTA Kids Vote - Film in 2014
|
| 
|}

==Video game== Android devices. 

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 