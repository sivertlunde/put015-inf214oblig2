Nema Aviona Za Zagreb
{{Infobox film
| name           = Nema Aviona Za Zagreb
| image          = Poster shot for Nema Aviona Za Zagreb.jpg
| caption        =
| director       = Louis van Gasteren
| producer       = Joke Meerman
| writer         = Louis van Gasteren
| screenplay  = Joke Meerman
| starring       = Timothy Leary Meher Baba
| music          = Mal Waldron
| cinematography = Jan de Bont
| editing        = llja Lammers 
| studio  =  Spectrum Film
| distributor    =  Spectrum Film
| released       =  
| runtime        = 82 minutes
| country        = Netherlands Dutch English English
}}
 Dutch film by Louis van Gasteren. The film is a retrospective of events in the directors life from 1964 to 1969, filmed by him in that period and reflected on from his vantage point over 40 years later at the age of 90. It is also Jan de Bonts first film credit as cinematographer since 1992s Basic Instinct.

In 1964 van Gasteren decided to film all his movements, both outward and inner. He narrates early in the film, "I wanted to make a film about my memories, my observations and experiences. As a filmmaker, I can make that visible." Explaining his idea to his wife in the film, he says, "I want to show everything, because every observation I make fits in with that. Look Jacq, every step I take (he takes a deliberate step forward in the room) – also inwards (points to his chest) – everything I am involved in (gestures at the objects in the room)." 

The film crisscrosses the globe. Countries the film is shot in include the Netherlands, Switzerland, England, Socialist Federal Republic of Yugoslavia|Yugoslavia, France, Canada, West Germany, the United States, India, and Spain. The film mixes documentary with enacted scenes, and is shot in both color and black-and-white.  

The film winds up being a journey of a man in search of truth, seeking to shed lies and illusions, as well as reconcile his relationships with human beings. 

==Plot==

As the film opens, a ninety-year-old Louis van Gasteren—a documentary filmmaker and artist famed in the Netherlands—is seated in a video editing suite, watching scenes of himself in the 1960s, a time when “anything was possible.” He reflects on how much he has changed, and that he is that same person and yet is not.

We then go back to 1964. Van Gasteren is touring a carnival with his second wife, Jacqueline, their baby girl Mardou, and two older children from his first marriage. The family rides the carousel and sees the sights, including a “Fat Lady” exhibit of a mother and daughter weighing 900 pounds.

From there Louis begins to recall his own youthful memories. His father, Louis van Gasteren, Sr., was a famous actor, and his mother, Elise Menagé Challa, was a singer who gave up the concert stage to promote Communism and traveled rural Spain to learn the songs of the peasants. We learn that his mother died by suicide a few months after his father’s death, and thoughts of his parents are with him every day.

The film takes a turn of style as Van Gasteren begins to act, playing himself as a good-timer who cheats on his wife with casual encounters. In the course of his travels he runs into an Italian journalist in Belgrade. The two decide to book a flight to Zagreb on a lark, but are told by the travel agent, Nema aviona za Zagreb (“There is no plane to Zagreb” in Serbian language|Serbian). Not knowing the language, they find the expression hilarious, and repeat it wherever they go. But later, when alone, the expression begins to haunt Louis. It becomes a personal catchphrase. “What Nema expresses is good will and a great incapacity. Nema and life as such cannot be tied down. You are in them, yet always off the mark.” What this means is never entirely clear, but what we see is that from this point forward Louis becomes a sincere seeker of truth.

He begins to be more critical of his assumptions about life and what he was told in his youth, and conducts a series of experiments to test them. Next he questions what he really sees, how many different angles and dimensions man can perceive, and whether life is simply one big illusion. To answer these questions, he experiments with Lysergic acid diethylamide|LSD, which at that time was permitted for therapeutic use. 

Shortly afterward, it is reported that a young American has died from jumping out a window while tripping on LSD, apparently believing he could fly like a bird. Troubled by this, Louis travels to a U.S. military base in Germany to talk to the boy’s parents, as well as to Millbrook, New York, to meet the LSD-advocate Timothy Leary. In the two skillfully interwoven interviews, a flower-bedecked Leary boasts that people come to his estate to find God through LSD, while the grieving parents puzzle over what went wrong with their son, who had been so healthy and virtuous. These conversations only leave Louis with more questions. He then goes to India to question the spiritual master Meher Baba about LSD and the search for God. 

First asserting that his own experience of God is continuous, Meher Baba explains that the upliftment produced by drugs is only temporary and thus not a true realization of Divinity; in the end, drugs lead only to madness, delusion, and hypocrisy. After one more disillusioning scene in Millbrook, Louis closes the door on drugs. He tells us that he stopped using LSD after it became illegal, but adds that had it not been for LSD, he would surely have taken his own life, just as his mother and grandfather did.
 Beat poetry in English.
 
:. . . I make up my mind
:Not knowing what my mind is.

At last Louis travels to the Spanish seaside and seems to resolve the loss of his mother by rescuing a man-sized living turtle being sold at market. Together with his children, Louis rows the gasping turtle out into the harbor and releases it back into the sea, to the joy of the whole family. The camera shifts to a café in which a flamenco singer improvises for the family a touching tribute to Mardou, “a little angel of God.”

At the end of the film we see Louis drive down a road and reach a dead end where a mountain of rubble has collapsed into the road. He steps out of the car in silence and faces the mountainous obstacle. It seems to symbolize that he had come to a halting place in the film’s story, or a terminus in that phase of his life. In 1969 Louis stopped making the film.

In his editing room at ninety years old, Louis van Gasteren at last reveals the heart-breaking event that blocked him from completing the film in the ensuing decades. But now, he says, after more than forty years, he finally found the courage to do it. With this we see him jump from an airplane and descend to earth with a parachute.

The credits roll over a shot of Louis in the 1960s gazing out the window of a train, where he first began to ponder the word "nema," as “Really and Sincerely” by the Bee Gees plays.

==Cast==
* Louis van Gasteren as himself and Professor Cerillo 
* Meher Baba as himself
* Timothy Leary as himself
* Michèle Girardon as the woman in the Zurich airport
* Nicholas Parsons as the English cuckold.
* Totti Truman Taylor as the cuckolds mother.
* Luigi de Santis (Italian journalist) as himself
* Snežana Nikšić as the travel agency receptionist
* Mal Waldron as himself

==Reception==

Begun in 1964 when Van Gasteren was 42 years old, Nema Aviona Za Zagreb finally premiered at EYE Film Institute Netherlands on the occasion of Van Gasterens 90th birthday, 20 November 2012.  Louis van Gasteren was present at the opening and afterwards was interviewed by journalist Raymond van den Boogaard.  On November 30 there was a public interview with the author by television critic Hans Beerekamp from NRC Handelsblad. 

Vimeo.com called the film "a journey in spiritual and geographical terms"  and Realtofilm.nl wrote, "The film not only gives a peak into a fascinating era, it is a highly original autobiographical portrait."  Some reviews were negative. Yara Plasman of Filmtotaal.nl described the film as "a veritable hotch-potch/jumble of various images, sometimes spontaneous while at other times obviously enacted, with or without a clear storyline . . ." 

==Production==
Principal photography for Nema began in 1964 in the Netherlands and lasted five years as Louis raised money as he went. Filming eventually covered ten countries. Jan de Bont was the cinematographer on the principal scenes of the film including scenes in New York, Vancouver, San Francisco, Timothy Leary in Millbrook, and Meher Baba in India. Additional cameramen during the very long shooting period included Milek Knebel, Theo Hogers, Roeland Kerbosch, Olof Smit, Bert Spijkerman, Louis van Gasteren, and Kester Dixon (for final filming in 2012).

Van Gasteren worked hard to get an interview with   and soundman Peter Brugman, and was shown around the location in preparation for the following days shoot and met Baba. The following day, September 20, the crew filmed forty minutes of film footage of Baba and the surrounding area, including the interview seen in the film.

Filming stalled in 1969. After two attempts to complete the film in the 70s and 80s, a final attempt to edit the film was begun in 2009 by filmmaker and editor Ilja Lammers who began to gather and assemble the material.  The 35mm film was digitized by EYE Film Institute Netherlands in 2010.  Completion financing was received from Netherlands Film Fund and EYE Film Institute and the film was finished in 2012, 48 years after principal photography began. The film premiered at EYE on Van Gasterens 90th birthday. Gasteren was present at the opening.

The film is in Dutch and English, with small amounts of French, Italian, Serbian, and Spanish. Available with Dutch or English subtitles.

==Music==
* Mal Waldron
* Bhimsen Joshi
* Piet Noordijk
* Misha Mengelberg
* Maarten Altena
* The Doors
* Bee Gees
* Lena Balaz
* Belgrado Alfonso Chozas

==References==

 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 