Rise of the Footsoldier
 
 
{{Infobox Film 
| name           = Rise of the Footsoldier
| image          = Rise of the footsoldier.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = Julian Gilbey
| producer       = 
| writer         = Julian Gilbey & Mike Hawk
| narrator       = 
| starring       = Ricci Harnett Craig Fairbrass Roland Manookian Terry Stone Coralie Rose
| music          = 
| cinematography = 
| editing        = 
| distributor    = Optimum Releasing
| released       = 7 September 2007
| runtime        = 113 minutes
| country        = United Kingdom English
| budget         = 
| gross          = £220,868 
| preceded_by    = 
| followed_by    = 
}}
Rise of the Footsoldier is a British crime film released on 7 September 2007.  The third production from BAFTA Award-nominated director Julian Gilbey, it is a gangster film based on the true story of the Rettendon murders and the autobiography of Carlton Leach,  a football hooligan of the infamous Inter City Firm (ICF) who became a powerful figure of the English underworld.  

==Cast==
 
* Ricci Harnett as Carlton Leach
* Craig Fairbrass as Pat Tate
* Roland Manookian as Craig Rolfe
* Terry Stone as Tony Tucker
* Coralie Rose as Denny
* Neil Maskell as Darren Nicholls Billy Murray as Mickey Steele
* Ian Virgo as Jimmy Gerenuk
* Kierston Wareing as Kate Carter
* Patrick Regis as Eddie
* Lara Belmont as Karen
* Emily Beecham as Kelly
* Frank Harper as Jack Whomes
* Jason Maza as Rob
* Mark Killeen as Terry
* Dave Legeno as Big John
* Dhaffer LAbidine as Emre Baran
* Mitchell Lewis as Kemal Baran
* Eden Ford as Paul
* George Calil as Police Killer
* Jay Taylor as Chris Wightman
* Phillip Weddell as Young Carlton
 

==Evaluation in film guides== Stanley knives then knock ten bells out of each other for two hours." After a one-sentence overview, the review concludes that "Leach is then unceremoniously swept aside as the film hastily attempts to give the Rettendon Range Rover murders a once-over in the scrappy second half."

In America, Videohounds Golden Movie Retriever also disliked it, throwing the film one bone (out of possible four) and dismissing it as "Brit crime flick, based on a true story, that has nothing going for it but violence". Indicating that "Carlton Leach goes from football hooligan in the 1980s to criminal muscle and gangster in the 1990s", the write-up ends with mention of "three murdered drug dealers who were found in rural Essex".

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 

 