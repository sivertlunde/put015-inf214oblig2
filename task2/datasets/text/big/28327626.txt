Assault (film)
{{Infobox film
| name           = Assault
| director       = Sidney Hayers George H. Brown   Peter Rogers
| writer         = John Kruse   Kendal Young (novel)
| narrator       = 
| starring       = Suzy Kendall Frank Finlay Freddie Jones Lesley-Anne Down Eric Rogers
| cinematography = Ken Hodges
| editing        = Anthony Palk
| studio         = 
| distributor    = J. Arthur Rank Film Distributors
| released       = 1971
| runtime        = 91 minutes
| country        = United Kingdom
| language       = English
| preceded by    = 
| followed by    = 
}}

Assault is a 1971 British thriller film directed by Sidney Hayers and starring Suzy Kendall, Frank Finlay and Freddie Jones,  with Lesley-Anne Down making an early screen appearance. It is based on the novel The Ravine by Kendal Young, and  tells about a police attempt to track down a dangerous rapist/killer on the loose. It is also known as Tower of Terror: in the U.S., it was retitled In the Devils Garden. 

==Synopsis==
After a schoolgirl is raped on her way home from school, police move in to investigate the case. After a further girl is attacked and killed, they call in the assistance of a doctor and a local schoolteacher to help solve the case.

==Cast==
* Suzy Kendall - Julie West
* Frank Finlay - Det. Chief Supt. Velyan
* Freddie Jones - Reporter
* James Laurenson - Greg Lomax
* Lesley-Anne Down - Tessa Hurst
* Tony Beckley - Leslie Sanford
* Anthony Ainley - Mr. Bartell
* Dilys Hamlett - Mrs. Sanford
* James Cosmo - Det. Sgt. Beale
* Patrick Jordan - Sgt. Milton
* Allan Cuthbertson - Coroner
* Anabel Littledale  - Susan Miller
* Tom Chatto - Police Doctor
* Kit Taylor - Doctor
* Jan Butlin - Day Receptionist
* William Hoyland - Chemist in Hospital
* John Swindells - Desk Sergeant
* Jill Carey - Night Receptionist
* David Essex - Man in Chemist Shop
* Valerie Shute - Girl in Chemist Shop John Stone - Fire Chief
* Siobhan Quinlan - Jenny Greenaway
* Marianne Stone - Matron Janet Lynn - Girl in Library

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 

 
 