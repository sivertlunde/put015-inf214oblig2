The Solitude of Prime Numbers
 
{{Infobox film
| name           = The Solitude of Prime Numbers
| image          = The Solitude of Prime Numbers.jpg
| caption        = Film poster
| director       = Saverio Costanzo
| producer       =  
| writer         =  
| starring       =  
| music          = Mike Patton
| cinematography = Fabio Cianchetti
| editing        = Francesca Calvelli
| distributor    = Medusa Film
| released       =  
| runtime        = 118 minutes
| country        = Italy
| language       = Italian
| budget         = 
}} the novel of the same name by Paolo Giordano, and directed by Saverio Costanzo. The film was nominated for the Golden Lion at the 67th Venice International Film Festival.   

The title is explained by arguing that Mattia and Alice are like twin primes: both lonely, close to each other but separated by an even number.

==Plot==
The film is set in Italy in 1984, 1991, 2001 and 2009 and jumps back and forth in time.

Mattia and Alice (children in 1984, teenagers in 1991) are both traumatized, and isolate themselves from the people around them.

When he was a child, Mattias mentally disabled twin sister Michela goes missing whilst he was supposed to look after her. He is intelligent and becomes a successful scientist, but he sometimes cuts himself on purpose.

When Alice was a child she had a skiing accident; which results in her having a permanent limp and a significant a scar on her thigh. As a teenager she is bullied by female classmates.

They meet while teenagers and become friends, but lose contact when Mattia goes to work in Germany. Eight years later Alice writes him a very brief request to come back to Italy, which he does, and they reunite.

==Cast==
* Alba Rohrwacher as Alice (adult)
* Luca Marinelli as Mattia Balossino (adult)
* Arianna Nastro as Alice (Teen)
* Vittorio Lomartire as Mattia (Teen)
* Martina Albano as Alice (children)
* Tommaso Neri as Mattia (children)
* Aurora Ruffino as Viola
* Giorgia Pizzo as Michela
* Isabella Rossellini as Adele
* Maurizio Donadoni as Umberto
* Roberto Sbaratto as Pietro
* Giorgia Senesi as Elena
* Andrea Jublin as Fabio
* Filippo Timi as Clown

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 