The Governess
 
 
 
{{Infobox film
| name           = The Governess
| image          = GovernessFilmPoster.jpg
| image_size     = 
| caption        = Original poster
| director       = Sandra Goldbacher
| producer       = Sarah Curtis Sally Hibbin
| writer         = Sandra Goldbacher
| narrator       =  Tom Wilkinson Jonathan Rhys Meyers
| music          = Edward Shearmur
| cinematography = Ashley Rowe
| editing        = Isabelle Lorente
| distributor    = Sony Pictures Classics
| released       = 31 July 1998 (U.S.) 23 October 1998 (UK) 3 December 1998 (Australia) 4 February 1999 (New Zealand)
| runtime        = 114 minutes
| country        = United Kingdom English
| budget         = 
| gross          = $3,800,509 (US)
| preceded_by    = 
| followed_by    = 
}}
 British period period drama film written and directed by Sandra Goldbacher. The screenplay focuses on a young Jewish woman of Sephardic background, who reinvents herself as a gentile governess when she is forced to find work to support her family.

==Plot synopsis== Italian descent Scottish family Isle of photographic image on paper, while his pretentious wife flounders in a sea of Boredom|ennui. Their young daughter Clementina initially resists Marys discipline, but eventually finds in her a friend and companion.

Mary, well-educated and unusually curious in an era when a womans primary focus is keeping house and attending to the needs of her family, surprises Charles with the depth of her interest and ability and becomes his assistant. He is delighted to find a kindred spirit in his isolation, and the admiration she feels soon turns to passion that he reciprocates. While secretly observing Passover in her room, she spills salt water onto one of Charles prints. The next morning she rushes to the laboratory to tell Charles, and their excitement spills over into making love for the first time. But he becomes increasingly consumed with the race to publish their new process, while she is captivated by the beauty of the photographs they create.

Complications ensue when the Cavendishs son Henry returns home after being expelled from Oxford University for smoking opium and being caught with a prostitute, and he becomes obsessed with Mary. While searching through her belongings, he uncovers evidence of her true background, and although he confesses to her he knows about her past, he promises to keep it secret. But eventually Henry tells Charles that he is in love with Mary, and Charles ridicules his affection and disparagingly remarks that Mary is "practically a demimondaine", refusing his consent and further alienating his son.

One day she leaves a gift for Charles, a nude photograph that she took of him asleep in the laboratory after lovemaking, and he begins to shun her. When a fellow scientist visits he claims sole credit for the technique she discovered. Angered by his rebuff and betrayal, Mary at first takes it out on Henry, but then decides to leave the island and return to London. On her way out, conspicuously dressed as a Jewess once more, she presents to Mrs. Cavendish at their dinner table the picture of her naked husband.

At home again, she embraces her true identity and becomes a portrait photographer noted for her distinct images of the Jewish people. Her sister announces her next sitter and when Charles appears, she quietly proceeds to take his portrait. When she has finished he asks her if they are done, and she says yes, "quite done," dismissing him. But his portrait remains foremost among the scattering of prints in her studio, as she muses, "I hardly think of those days at all."

==Production notes==
The film was shot on location at Brodick Castle in North Ayrshire, Wrotham Park in Hertfordshire, and London. Interiors were filmed at the Pinewood Studios in Buckinghamshire.
 Sephardi melodies Ladino by Ofra Haza. The soundtrack also includes "Standchen," a serenade for violin and piano by Franz Schubert.

==Principal cast==
*Minnie Driver ..... Rosina da Silva/Mary Blackchurch Tom Wilkinson ..... Charles Cavendish
*Harriet Walter ..... Mrs. Cavendish
*Jonathan Rhys Meyers ..... Henry Cavendish
*Florence Hoath ..... Clementina Cavendish

==Critical reception==
The film debuted at the Seattle International Film Festival before going into limited release in the US. On its opening weekend it grossed $57,799 in six theaters. Its total box office in the US was $3,719,509.  The film received positive reception from critics, especially for Drivers performance as Rosina da Silva.

In his review in the New York Times, Stephen Holden called the film "ravishingly handsome" and added, "The Governess is a wonderful showcase for Ms. Driver . . . If   performance is strong, it is less than great, because her face lacks the transparency of expression that would transport us inside her characters mind . . . The movie takes some missteps. Rosinas ability to support her family in luxurious style from her earnings as a governess is implausible. The screenplay includes some glaring lapses into contemporary slang. And dramatically, the movie peters out in its disappointingly perfunctory final scenes. But   still leaves a lasting after-image." 

In Variety (magazine)|Variety, Ken Eisner called the film "beautifully crafted" and said it "gets high marks for originality and style," then added, "Although first-time helmer Sandra Goldbacher, working from her own script, has come up with a fascinating premise, her follow-through is too scattered in concept and monotonous in execution to be truly rewarding . . .   has much to offer the senses . . . but the images are often art-directed to death, with more attention paid to fabrics, textures and colors than to narrative coherence. A little trimming could remove some of the distractions and repetition, but it wont be easy to hide the movies lack of a solid point or payoff." 

In Entertainment Weekly, Owen Gleiberman graded the film C and called it "a have-your-kugel-and-eat-it-too princess fantasy. Writer-director Sandra Goldbacher glorifies her heroine at every turn, but she also fills the movie with arid pauses, turning it into a claustrophobic study in repression." 

Barbara Shulgasser of the San Francisco Examiner observed, "Sandra Goldbacher, writing and directing her first feature, is a sure-handed filmmaker. The movie is a tableau of sensuality. The tactile attractiveness of the photographic images meld with the fire that devours the lovers . . . I found the end of the movie a bit of an anticlimax and sense that Goldbacher just ran out of steam. But it seems a sure bet that she has many more movies ahead of her, all of which I look forward to seeing." 

Ruthe Stein of the San Francisco Chronicle said, "As Rosinas extraordinary fate unfolds in The Governess, the real wonder becomes how British filmmaker Sandra Goldbacher was able to write and direct such an accomplished, touching and original movie her first time out . . . Rosina is a wonderfully rich role, and Driver gives it everything she has. Its her best work yet." 

==Awards and nominations== Evening Standard British Film Award for Best Technical/Artistic Achievement for his cinematography. Sandra Goldbacher was nominated for the Crystal Globe and won both the Audience Award and Special Prize at the Karlovy Vary International Film Festival.

==References==
 

==External links==
*  
*  
*  
*  
* 

 
 
 
 
 
 
 
 
 