Tell Me I'm Dreaming
{{Infobox film
| name           = Tell Me Im Dreaming
| image          = 
| image_size     = 
| caption        = 
| director       = Claude Mouriéras
| producer       = Philippe Carcassonne
| writer         = Claude Mouriéras
| starring       = Frédéric Pierrot
| music          = 
| cinematography = William Lubtchansky
| editing        = Monique Dartonne
| distributor    = 
| released       = 3 June 1998
| runtime        = 97 minutes
| country        = France
| language       = French
| budget         = 
}}

Tell Me Im Dreaming ( ) is a 1998 French drama film directed by Claude Mouriéras. It was screened in the Un Certain Regard section at the 1998 Cannes Film Festival.   

==Cast==
* Frédéric Pierrot - Luc
* Muriel Mayette - Jeanne
* Vincent Dénériaz - Julien
* Cédric Vieira - Jules
* Julien Charpy - Yannick
* Stéphanie Frey - Marion
* Suzanne Gradel - Grandmother
* Christophe Delachaux - Johnny
* Yvon Davis - Psychiatrist
* Katya Medici - Nini
* Patrice Verdeil - Policeman
* Hélène Wert - Charlotte
* Karine Kadi - Young doctor
* Rebecca Mahboubi - Talkative friend

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 