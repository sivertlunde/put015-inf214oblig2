Go West (1940 film)
 

{{Infobox film
| name           = Go West
| image          = Go West.jpg
| caption        = Theatrical release poster.
| director       = Edward Buzzell Jack Cummings
| writer         = Irving Brecher John Carroll Diana Lewis
| music          = George Bassman (orchestrations) Georgie Stoll (music direction) Leonard Smith
| editing        = Blanche Sewell
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 80 minutes
| country        = United States
| language       = English
| budget         = 
}} Harpo head to the American West and attempt to unite a couple by ensuring that an evil railroad baron is thwarted.  It was directed by Edward Buzzell and written by Irving Brecher, who receives the original screenplay credit.

==Plot== John Carroll) has contacted the railway to arrange for them to build through the land, making the deed holder rich.

==Cast==
 
*Groucho Marx - S. Quentin Quayle
*Chico Marx - Joseph Panello
*Harpo Marx - Rusty Panello John Carroll - Terry Turner
*Diana Lewis - Eve Wilson
*Walter Woolf King - John Beecher
*Robert Barrat - "Red" Baxter
*June MacCloy - Lulubelle
*Tully Marshall - Dan Wilson
*Iris Adrian - Mary Lou
*Joan Woodbury - Melody
*George Lessey - The Railroad President
*Joe Yule - Crystal Palace Bartender Joe
*Mitchell Lewis - Halfbreed Indian Pete

==Production==
 orange on the keys in sync with the melody.)

Groucho was aged 50 during the filming of Go West, and his hairline had begun receding. As such, he took to wearing a toupee throughout the film, as he did the previous film, At the Circus .

Go West Screenwriter Irving Brecher impersonated an ailing Groucho when publicity stills for the film were first taken. Brecher bore a remarkable resemblance to Groucho and is all but unrecognizable in the photos, sporting Grouchos glasses, greasepaint mustache and eyebrows.

==Musical numbers==
*"As If I Didnt Know"
*"You Cant Argue With Love"
*"From The Land Of The Sky-Blue Water"
*"Ridin The Range"

==External links==
* 
*  
* 

 
 

 
 
 
 
 
 
 
 
 
 
 