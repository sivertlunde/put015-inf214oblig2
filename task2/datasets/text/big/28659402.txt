9 KK Road
{{Infobox Film
| name           = 9 KK Road
| image          = 
| image_size     = 
| caption        = 
| director       = Simon Kuruvila
| producer       = Simon Kuruvila
| writer         = Kalloor Dennis
| narrator       =  Vijayaraghavan Nishanth Sagar
| music          = S. P. Venkatesh
| cinematography = Venugopal
| editing        = P. C. Mohanan
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}} Vijayaraghavan and Nishanth Sagar in the lead roles. Several major political leaders from Kerala make special appearances in the film.

==Plot==
A regular mystery tale, the story begins with the discovery of four dead bodies in a deserted quarry. The Chief Minister involves into the case and appoints the IPS officer Devdas (Babu Antony) to investigate. Initially, Devdas is accompanied by one aide (Mala Aravindan) but that person walks out giving some excuses. Then Devdas is joined by Noushad (Shammi Thilakan) and the duo gets brisk in their investigation. Devdas is one man who is known to get into the cracks of any issue until justice is got. Whether he is successful in his mission or not forms the rest of the story.

== Cast ==
* Babu Antony as Devdas Adiyodi
* C. K. Padmanabhan as Chief Minister
* V. N. Vasavan
* M. K. Muneer
* P. C. George
* T. N. Prathaphan
* Kulappulli Leela
* Ayyappa Baiju Vijayaraghavan
* Shammi Thilakan as Noushad
* Nishanth Sagar
* Mala Aravindan as S.I. Mathukkutty
* Sadiq Abu Salim
* Prashant
* Shamna Kasim
* Suvarna Mathew

==External links==
* http://www.nowrunning.com/movie/4667/malayalam/k-k-road/index.htm
* http://movies.sulekha.com/malayalam/9-kk-road/default.htm
* http://popcorn.oneindia.in/title/1425/9-kk-road.html

 
 
 
 


 