The Skinny (film)
{{multiple issues|
 
 
}}

{{Infobox film
| name           = The Skinny
| image          = The Skinny (film) poster.jpg
| caption        = 
| director       = Patrik-Ian Polk
| producer       = 
| writer         = Patrik-Ian Polk
| starring       = Jussie Smollett Anthony Burrell Blake Young-Fountain Jeffrey Bowyer-Chapman Shanika Warren-Markland
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
The Skinny is a 2012 American romantic comedy-drama film from the creator of the LOGO television series, Noahs Arc. It was released on April 6, 2012 in select theaters.

==Plot==

Magnus (  PhD student and gay-porn aficionado Langston (Shanika Warren-Markland); innocent and sometimes childlike Sebastian (Blake Young-Fountain), who has just come back from a year in Paris paid for by his parents trust fund; promiscuous Kyle (Anthony Burrell), now living in Los Angeles and enjoying a career in film production; and witty and sarcastic Joey (Jeffrey Bowyer-Chapman). Ryan gives Magnus the weekend alone to enjoy his time with his friends, and Magnus reveals to them that he and Ryan have agreed on a six-month abstinence pledge so they can appreciate one another more deeply than the physical level. Magnus takes them all to see the former home of Langston Hughes in Harlem, and the group reflects on their cultural history, while Magnus and Sebastian recite lines from Hughess poetry.
 hooks up with a stranger in the bathroom, while Joey fawns over an attractive go-go dancing|go-go boy, Junot, and Langston over a sultry bartender, Savannah. Both are afraid to approach the objects of their desire, Langston especially after an awkward encounter with Savannah. Sebastian reveals to Magnus that he is still a virgin and is secretly in love with Kyle, wanting him to be his "virginity|first". Kyles promiscuity is not an issue for him.

While browsing through a hook-up website|site, Kyle comes across Ryans active profile advertising a sex party in the area. Magnus is shocked and wants to catch Ryan in the act. Magnus and Kyle go into the party while the others wait outside. While hidden, Magnus witnesses Ryan having sex with many men. He is heartbroken and confronts him, and the two break up.
 bottom and Joey can teach him how to use an enema before engaging in anal sex. Magnus educates him on the risks of unprotected sex. While watching some gay porn on XTube, Langston and Joey are shocked to discover a video of Ryan having sex in Magnuss apartment. Magnus is furious, and when he and his friends see Ryan having dinner with a man at the restaurant where they are eating, Magnus starts a fight and punches Ryan.

At the club again, Sebastian is drunk and he and Kyle secretly take some ecstasy (drug)|ecstasy. The rest of the group leaves, after Joey and Langston fail to screw up the courage to talk to Junot and Savannah again, but Magnus tells Kyle to take care of Sebastian, who is already drunk. Sebastian reaches his high on ecstasy, but cant find Kyle in the club. Resting at the bar, he is confronted by two men who take advantage of his altered state, giving him alcohol, and take him back to their home.
 GHB in his system. Sebastian tests negative for HIV, but the status of the two men is unknown, and Sebastian agrees to take a one-month regimen of post-exposure prophylaxis (PEP) to help reduce his chances of contracting HIV if one of them was infected. Despite what happened, Sebastian refuses to press charges and wants to go out to forget what happened and help Magnus forget his relationship troubles, where they ride the float for Gay Mens Health Crisis.
 rapid HIV test at a nearby testing van. The results come back negative and he shows them to Sebastian, who is relieved, but still distant. Joey and Langston still cant talk to Junot and Savannah, who then take it upon themselves to make the first move. Savannah throws Langston her lighter from across the bar and asks her to light her cigarette while Junot flirts with Joey and invites him into the bathroom while he changes into his next dancing costume. The two couples hook up in the bathroom and Joey and Langston toast each other afterward. Saying goodbye, Junot sadly tells Joey that hes not from New York City: He actually lives in Atlanta and came to Pride to visit. Joey is delighted.

Ryan finds Magnus and they discuss what happened. Ryan says that he didnt tell Magnus about his sexual activities because this is what he does to survive after coming from a bad home and childhood of being tossed around in the foster care system, afraid that Magnus wouldnt understand, coming from a positive and wealthy upbringing. Magnus tells him that if he had been honest with him, he would have been more understanding and that he cannot trust him because he lied. They both agree to end the relationship.

At Magnus apartment, the group plays "Truth or Dare?" and when Sebastian steps out, Joey and Langston ask Kyle what happened to Sebastian. Kyle is forced to confront what happened and the group is furious with him, but Sebastian arrives and cheers them up. In the morning, Kyle returns to LA alone while looking visibly distraught after reading from a piece of paper which is presumed to be his HIV results coming out positive, and Magnus, says goodbye to Joey, Langston, and Sebastian, who comforts him with a quote from Marianne Williamson: “We are not held back by the love we didnt receive in the past, but by the love were not extending in the present.”

==Cast==
* Jussie Smollett as Magnus
* Blake Young-Fountain as Sebastian
* Shanika Warren-Markland as Langston
* Jeffrey Bowyer-Chapman as Joey
* Anthony Burrell as Kyle
* Jennia Fredrique as Savannah
* Dustin Ross as Ryan
* Robb Sherman as Junot
* B. Scott as Candy
* Darryl Stephens as Nurse
* Seth Gilliam as HIV testing worker
* Wilson Cruz as Dr. Velasquez
* Derrick L. Briggs as Rapist #1
* Dewayne Queen as Rapist #2
* Zeric Armenteros
* Bassey Ikpi as herself
* Phat Daddy
* Hot Rod

==External links==
*  
*  
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 