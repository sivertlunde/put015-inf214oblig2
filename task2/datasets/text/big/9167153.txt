Mandy (film)
{{Infobox film
| name           = Mandy
| image          = 1952_UK_film_poster_for_Mandy.jpg
| alt            = 
| caption        = Original UK cinema poster
| director       = Alexander Mackendrick Leslie Norman
| screenplay         = Nigel Balchin Jack Whittingham
| based on       =  
| starring       = Phyllis Calvert Jack Hawkins Mandy Miller
| music          = William Alwyn
| cinematography = Douglas Slocombe
| editing        = Seth Holt
| studio         = Ealing Studios
| distributor    = General Film Distributors
| released       =  
| runtime        = 93 min.
| country        = United Kingdom
| language       = English
| budget         =
}}
Mandy is a 1952 British film about a familys struggle to give their deaf-mute daughter a better life. It was directed by Alexander Mackendrick and is based on the novel The Day Is Ours by Hilda Lewis. It stars Phyllis Calvert, Jack Hawkins and Terence Morgan, and features the first film appearance by Jane Asher. In the US the film was released as The Story of Mandy,  later also distributed as Crash of Silence.  

==Plot==
Harry and Christine Garland have a deaf-mute daughter, Mandy.  As they realise their daughters situation, the parents enrol Mandy in special education classes to try to get her to speak. They quarrel in the process and their marriage comes under strain. There are also hints of a possible affair between Christine and Dick Searle, the headmaster of the school for the deaf where Mandy is enrolled. Eventually, the training succeeds to the point where Mandy says her own name for the first time. Mandys speech was achieved by using a balloon. She was able to feel the vibrations of sound onto the balloon and knew she had made a sound.

== Cast ==
 
* Phyllis Calvert as Christine Garland
* Jack Hawkins as Dick Searle
* Terence Morgan as Harry Garland
* Godfrey Tearle as Mr Garland
* Mandy Miller as Mandy Garland
* Marjorie Fielding as Mrs Garland
* Nancy Price as Jane Ellis Edward Chapman as Ackland
* Patricia Plunkett as Miss Crocker
* Eleanor Summerfield as Lily Tabor
* Colin Gordon as Wollard (junior)
* Dorothy Alison as Miss Stockton
* Julian Amyes as Jimmy Tabor
* Gabrielle Brune as Secretary
* John Cazabon as Davey
* Gwen Bacon as Mrs Paul
* W. E. Holloway as Woollard (senior)
* Phyllis Morris as Miss Tucker
* Gabrielle Blunt as Miss Larner
* Jean Shepherd as Mrs Jackson
* Jane Asher as Nina
 

==Production==
The films screenplay was written by by Nigel Balchin and Jack Whittingham. The film was shot at the Ealing Studios outside London, but also at the Royal Schools for the Deaf outside Manchester. 

==Reception==
Mandy premiered in London on 29 July 1952, and was the fifth most popular at the British box office that year.  The film was nominated for  British Academy Special Jury 1952 Venice Film Festival for his direction, and the film was nominated for the Golden Lion at the same festival. 
==References==
  

== External links ==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 