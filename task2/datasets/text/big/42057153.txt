Cop or Hood
{{Infobox film
| name     = Cop or Hood
| image    =
| director = Georges Lautner
| producer = 
| writer = Michel Audiard   Jean Herman
| starring = Jean-Paul Belmondo   Marie Laforêt   Michel Galabru   Georges Géret   Jean-François Balmer
| cinematography = Gaumont International
| music = Philippe Sarde
| country = France
| language = French gross = 3,950,691 admissions (France) 
| runtime = 107 minutes
| released =  
}} crime and action film directed by Georges Lautner.

== Plot ==
Stanislas Borowitz is a divisional commissioner from the IGPN ( ) who uses particularly expeditious methods to counteract the "ripoux" (French term for "corrupt cops"). Sent to Nice to struggle against the Mafia and enquire on a murder of a notoriously corrupt commissioner, he changes his identity into a thug named Antonio Cerruti to trigger a gang war between the two biggest local sponsors, Théodore Musard ("lAuvergnat") and Achille Volfoni ("le Corse"), and discovers a police organization with the mafia of the town. But the corrupt police inspectors Rey and Massard, on the pay of Volfini, absolutely want to harm him.

== Cast ==
* Jean-Paul Belmondo ... Antonio Cerutti (aka the divisional commissioner Stanislas Borowitz)
* Georges Géret ... Théodore Musard ("lAvergnat")
* Marie Laforêt ... Edmonde Puget-Rostand
* Jean-François Balmer ... Inspecteur Massard
* Claude Brosset ... Achille Volfoni ("le Corse")
* Julie Jézéquel ... Charlotte
* Michel Beaune ... Marcel Langlois Tony Kendall ... Rey
* Catherine Lachens ... Simone Langlois
* Juliette Mills ... Madame Bertrand
* Venantino Venantini ... Mario
* Charles Gérard ... Cazauban
* Michel Galabru ... Commissaire Grimaud
* Philippe Castelli ... driving school inspector
* Marc Lamole ... substitute
* Michel Peyrelon ... Camille
* Nicolas Vogel ... Marcel Gaston
==References==
 
== External links ==
* 

 
 
 
 
 