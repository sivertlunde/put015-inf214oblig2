Vengeance (2014 film)
{{Infobox film
| name           = Vengeance
| image          = Vengeance (2014 film).jpg
| caption        = 
| director       = Gil Medina
| producer       = 
| story          = Gil Medina Dallas Page Jason Mewes 50 Cent Tech N9ne
| cinematography = 
| editing        = 
| distributor    = 
| released       =    
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Vengeance is a 2014 film starring Danny Trejo and Diamond Dallas Page.

==Development==
The first trailer was released on May 22, 2010.  The films director Gil Medina is hoping for the film to receive a theatrical release.  Trejo started a campaign for people who give away free copies of the DVD of the film to be given roles in the sequel.  The film was released on February 22, 2014 in the United States.

==Plot==
Jack (Danny Trejo) is a retired cop whose wife and daughter are murdered. He is then sent to prison for a crime he did not commit, however, upon being released he seeks revenge.

==Cast==
* Danny Trejo Dallas Page
* Jason Mewes
* 50 Cent
* Tech N9ne
* Donal Logue

==Sequel==
Director Gil Medina has written a script for a proposed sequel. Danny Trejo is currently editing the script, and casting is set to begin in late September. 

==References==
 

==External links==
*  

 
 
 
 
 


 