Ali Baba (1973 film)
 
 
{{Infobox film
| name           = Ali baba (1973)
| image          = Ali_baba_(1973).jpeg
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = 
| director       = Rohit Mohra
| producer       = 
| writer         = Kshirodprashad Bidyabinod
| screenplay     = 
| story          = 
| based on       =  
| narrator       =
| starring       = Haradhan Bandopadhyay Sekhar Chattopadhyay Aparna Sen
| music          = V. Balsara
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Saregama
| released       = 1973
| runtime        = 1hr13m
| country        = India Bengali
| budget         = 
| gross          = 
}}

Ali Baba Bengali Short Animated Musical Drama Directed By Rohit Mohra,Asst.Directed By Suprabhat Mukherjee,Written by-Kshirodprashad Bidyabinod,starring Haradhan Bandopadhyay, Sekhar Chattopadhyay, Tarun Kumar, Aparna Sen was released on 1973.

== Plot ==
Ali Baba and his elder brother Cassim are the sons of a merchant. After the death of their father, the greedy Cassim marries a wealthy woman and becomes well-to-do, building on their fathers business. Ali Baba marries a poor woman and settles into the trade of a woodcutter.

One day, Ali Baba is at work collecting and cutting firewood in the forest, and he happens to overhear a group of forty thieves visiting their treasure store. The treasure is in a cave, the mouth of which is sealed by magic. It opens on the words "open sesame", and seals itself on the words "close sesame". When the thieves are gone, Ali Baba enters the cave himself, and discreetly takes a single bag of gold coins home.

Ali Baba and his wife borrow his sister-in-laws scales to weigh their new wealth. Unbeknownst to them, Cassims wife puts a blob of wax in the scales to find out what Ali Baba is using them for, as she is curious to know what kind of grain her impoverished brother-in-law needs to measure. To her shock, she finds a gold coin sticking to the scales and tells her husband. Under pressure from his brother, Ali Baba is forced to reveal the secret of the cave. Cassim goes to the cave, taking a donkey with him to take as much treasure as possible. He enters the cave with the magic words, but in his greed and excitement over the treasure, he forgets the words to get out again. The thieves find him there and kill him. When his brother does not come back, Ali Baba goes to the cave to look for him and finds the body quartered and with each piece displayed just inside the entrance of the cave as a warning to anyone else who might try to enter.

Ali Baba brings the body home, where he entrusts Morgiana, a clever slave-girl in Cassims household, with the task of making others believe that Cassim has died a natural death.  First, Morgiana purchases medicines from an apothecary, telling him that Cassim is gravely ill. Then, she finds an old tailor known as Baba Mustafa whom she pays, blindfolds, and leads to Cassims house. There, overnight, the tailor stitches the pieces of Cassims body back together, so that no one will be suspicious. Ali Baba and his family are able to give Cassim a proper burial without anyone asking awkward questions.

The thieves, finding the body gone, realize that yet another person must know their secret, and set out to track him down. One of the thieves goes down to the town and comes across Baba Mustafa, who mentions that he has just sewn a dead mans body back together. Realizing that the dead man must have been the thieves victim, the thief asks Baba Mustafa to lead the way to the house where the deed was performed. The tailor is blindfolded again, and in this state he is able to retrace his steps and find the house. The thief marks the door with a symbol, for the other thieves to come back that night and kill everyone in the house. However, the thief has been seen by Morgiana, and she, loyal to her master, foils his plan by marking all the houses in the neighborhood with a similar marking. When the forty thieves return that night, they cannot identify the correct house and their leader, in a furious rage, kills the unsuccessful thief. The next day, another thief revisits Baba Mustafa and tries again, only this time, a chunk is chipped out of the stone step at Ali Babas front door. Again Morgiana foils the plan by making similar chips in all the other doorsteps, and the second thief is killed for his failure as well. At last, the leader of the thieves goes and looks for himself. This time, he memorizes every detail he can of the exterior of Ali Babas house.

The chief of the thieves pretends to be an oil merchant in need of Ali Babas hospitality, bringing with him mules loaded with thirty-eight oil jars, one filled with oil, the other thirty-seven hiding the other remaining thieves. Once Ali Baba is asleep, the thieves plan to kill him. Again, Morgiana discovers and foils the plan, killing the thirty-seven thieves in their oil jars by pouring boiling oil on them. When their leader comes to rouse his men, he discovers that they are all dead, and escapes. The next morning Morgiana tells Ali Baba about the thieves in the jars; they bury them, and Ali Baba shows his gratitude by giving Morgiana her freedom.

To exact revenge, after some time the chief of thieves establishes himself as a merchant, befriends Ali Babas son (who is now in charge of the late Cassims business), and is invited to dinner at Ali Babas house. However the thief is recognized by Morgiana, who performs a dance with a dagger for the diners and plunges it into his heart when he is off his guard. Ali Baba is at first angry with Morgiana, but when he finds out the thief wanted to kill him, he is extremely grateful and rewards Morgiana by marrying her to his son. Ali Baba is then left as the only one knowing the secret of the treasure in the cave and how to access it.

== Cast ==
{| class="wikitable"
|-
! Cast !! Character
|-
| Satya Bandyopadhyay || AliBaba
|-
| Haradhan Bandyopadhyay || Kaseem
|-
| Sekhar Chattopadhyay || Captain
|-
| Ajitesh Bandyopadhyay || Abdalla
|-
| Tarun Kumar || Mustafa
|-
| Rudroprosad Sengupta || Hussain
|-
| Ajoy Gangopadhyay || Pirate
|-
| Dilip Bhattacharya || Friend
|-
| Ranjan Bandyopadhyay || Friend
|-
| Nilima Das || Sakina
|-
| Shobha Sen || Fatima
|-
| Aparna Sen || Marjina
|}

== Song ==
* Manna Dey
* Sandhya Mukhopadhyay
* Arati Mukhopadhyay
* Pratima Bandyopadhyay
* Banashree Sengupta
* Tarun Bandyopadhyay
* Adhir Bagchi
* Ila Bose
* Madhuri Chattopadhyay

==References==
 

==External links==
 
*  

 

 