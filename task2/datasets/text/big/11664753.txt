The Nameless (film)
 
{{Infobox Film
 | name = The Nameless
 | image = LosSinNombre.jpg
 | caption = Original Spanish poster
 | director = Jaume Balagueró
 | writer = Jaume Balagueró
 | starring = Emma Vilarasau, Karra Elejalde Tristan Ulloa Julio Fernández Filmax (Spain)
 | budget = 
 | released = 12 November 1999 (Spain)
 | runtime = 102 min. Spanish
 | country = Spain
 | }}
 independent Spanish Spanish horror film directed by Jaume Balagueró.  It is based on the 1981 horror novel by English writer Ramsey Campbell which mingled psychological suspense with supernatural horror.

== Synopsis ==

In Spain, the vanished six-year-old daughter of the editor Claudia (Emma Vilarasau) and her husband Marc is found completely mutilated in a well by the police, being recognized by her husband only due to a bracelet and leg defect (one legbone is shorter than the other). The body contains marks of many needle insertions and burns from sulfuric acid, which appear to have been inflicted while the child was still alive.

Five years later, Claudia is still unable to get over the death of her child and she is addicted to tranquiliizers.  Inexplicably, the presumed dead daughter Angela calls her mother, saying that she is still alive and needs her help.  After visiting a now-deserted clinic where she used to take her daughter (she has footage of it on a home video of herself and Angela), and finding strange pictures of angels there, Claudia calls a recently retired cop, Massera (Karra Elejalde), to investigate the matter. She also gives him a boot she found on the site, believing it to have been left for her to find.
 tabloid journalist Quiroga (Tristan Ulloa), a reporter for a magazine of parapsychology, after Quiroga receives in the mail a disturbing videotape which has Claudias phone number written on the label. The tape shows what appears to be the murder or torture of a naked woman, and also footage of Claudia when she visited the derelict clinic.

Visiting a theological expert at the university, the two begin to uncover a bizarre cult called The Nameless that is said to sacrifice children. The cult was led by a now-imprisoned man called Santini who believed in absolute evil, and created his cult which also links to Germanys Thule Society. A doctor who helped him was also arrested but was released for lack of evidence. To make matters worse, Claudia finds herself stalked by psychotic ex-boyfriend Toni. Toni meets a strange man with a scarred eye in a bar (the man has previously encountered Claudia on a train); they decide to go see Claudia, and are followed by mysterious figures as they leave.

Meanwhile Claudia visits Massera because she is scared to stay at home. She learns his wife died the year before. They then go to visit the prison where Santini is held. Santini has a degenerative skin disease which is believed to be due to inoculations he was given as a prisoner in Dachau. Santini talks to Claudi cryptically but though he talks about the painful experience of having been placed in a cobalt capsule at the concentration camp, and how he has vermin inside of him, Claudia pleads with him to reveal Angelas whereabouts but he only tells her to think, and says that Angela is in the place where the whole thing began. It soon becomes evident that while Santini may be in jail, his followers are still at large; as Claudia returns to her apartment, she finds Toni murdered, along with another cryptic message.

The trail leads to an abandoned hotel where Angela was conceived years ago. Quiroga scouts ahead, soon followed by Massera; both men are quickly subdued and slain by cult members. Another call from Angela urges Claudia to investigate. On the site the woman is greeted by the cultists, among them Marc, her own husband. He reveals it was their plan all along to lead her to their den. Claudia then confronts her now grown-up daughter, alive and well, but twisted and indoctrinated by the cult with the intent to create a perfect evil being. It is now her task to commit the ultimate atrocity and kill her own mother. However, Angela attacks Marc instead and briefly seems to resist the indoctrination – before proclaiming she has in fact prepared an even more sinister plan and does not wish to share its fruits with the cult. The girl states she will contact Claudia again, and appears to commit suicide as the movie ends.

== Release and Awards==
The film appeared at festivals worldwide, winning several awards including Best Film at the Fant-Asia Film Festival.
Miramax bought the US distribution rights for the film in 1999, but did not release it on home video until 2005.

== External links ==
* 
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 