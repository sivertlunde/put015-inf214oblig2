The Long Night (1947 film)
{{Infobox film
| name           = The Long Night
| image          = LongNightPoster.jpg
| alt            =
| caption        = theatrical release poster
| image_size     = 225px
| director       = Anatole Litvak Raymond Hakim Robert Hakim
| screenplay     = Jacques Viot John Wexley
| based on       =  
| starring       = Henry Fonda Barbara Bel Geddes Vincent Price Ann Dvorak
| cinematography = Sol Polito
| music          = Dimitri Tiomkin
| editing        = Robert Swink
| distributor    = RKO Pictures
| released       =  
| runtime        = 101 minutes
| country        = United States
| language       = English
| budget         =
}}

The Long Night is a 1947 American film noir directed by Anatole Litvak and produced by RKO. It is a remake of Le Jour Se Lève (1939) by Marcel Carné. The drama stars Henry Fonda, Barbara Bel Geddes, Vincent Price and Ann Dvorak. 

The Long Night was the first screen appearance by character actress Barbara Bel Geddes and it served as a springboard for Bel Geddess career. RKO signed Bel Geddes to a seven-year contract. 

==Plot==
A dead man tumbles down a flight of stairs. When the police arrive at the top-floor apartment of Joe Adams (Henry Fonda), he shoots at them through the door.

The sheriff calls in reinforcements and sets up snipers on nearby rooftops. Adams, in his room, begins a recollection of the events leading up to this, beginning with his first chance encounter with Jo Ann (Barbara Bel Geddes), who works in a flower shop. It turns out they had been raised in the same orphanage.

The story unfolds in a series of flashbacks, and even a flashback within a flashback, as Joe recalls what Jo Ann told him about her life before they met.

Finding her behavior suspicious, he follows her to a nightclub where Maximilian the Great (Vincent Price)is performing a magic act on stage. At the bar, Joe gets to know Charlene (Ann Dvorak), who recently quit as Maxs assistant.

Max claims to be Jo Anns long-lost father. She was picked out of the audience one night by Charlene and brought on stage to take part in the act, then continued a relationship. Jo Ann fiercely denies to Joe, however, that Max is related to her. In fact, she insists that she had to physically fend off Maxs romantic advances to her.

The two women have feelings for Joe but leave him mystified, particularly when both appear to have received exactly the same brooch from Max as a gift. Jo Ann naively believes that hers is a rare antique that once belonged to Montezumas daughter.  The more worldly-wise Charlene suggests she believed Maxs line at first too, but she now has a whole display card of them marked at a price of 85 cents each.  He is not sure whom to trust, and when Max comes to his apartment to kill him, Joe shoots first, sending Max falling to his death.

When Max first arrives at Joes shabby boarding house room, he demands that he leave Jo Ann alone.  In the ensuing argument Joe pushes Max halfway out of the window but cannot bring himself to kill his rival.  Max observes that it is not so easy to kill a man, and show Joe the pistol he brought with the intention of shooting him.  

Max, who has always been pretentiously snobbish, begins to taunt Joe.  He tells him that he thinks Joe is beneath him, and then begins to intimate that he and Jo Ann had a sexual relationship.  Joe becomes enraged and shoots Max.

Police are about to smoke him out with tear gas when Jo Ann arrives. She manages to talk Joe into giving himself up, promising to wait for him if he is sent away to prison.  Joe had considered himself friendless, but most of the assembled crowd, including Joes coworker and neighbor Bill Pulanski, and Frank Dunlap, a blind man who lives in the neighborhood, support him.

The title of the original French film is an idiom which translates roughly as "dawn is breaking". 

==Cast==
  
* Henry Fonda as Joe Adams
* Barbara Bel Geddes as Jo Ann
* Vincent Price as Maximilian
* Ann Dvorak as Charlene
* Howard Freeman as Sheriff Ned Meade
* Moroni Olsen as Chief of Police
 
* Elisha Cook Jr. as Frank Dunlap
* Queenie Smith as Mrs. Tully David Clarke as Bill Pulanski
* Charles McGraw as Policeman Stevens
* Patty King as Peggy
 

==Production==
When RKO acquired the distribution rights to Le Jour Se Lève in preparation for remaking it as The Long Night, they also sought to buy up all available prints of the original film and destroy them. For a time, it was thought that the French film had been lost completely, but copies of it re-appeared in the 1950s and its classic status was re-established. 

The score for the film makes extensive use of the second movement of Beethovens Seventh Symphony.

==Reception==

===Box-office===
The film recorded a loss of $1 million. Richard Jewell & Vernon Harbin, The RKO Story. New Rochelle, New York: Arlington House, 1982. p221 

===Critical response===
A Life (magazine)|Life magazine review at the time blamed "Hollywood commercialism and the stultifying institution of censorship" for the films poor quality, noting that, "because anything having to do with incest is banned from U.S. films by censors and because Hollywood considers sad endings unprofitable, a moving and mature tragedy has been remade into melodramatic goulash."   

==References==
Notes
 

==External links==
*  }
*  
*  
*  
*   information site and DVD review at DVD Beaver (includes images)

 

 

 
 
 
 
 
 
 
 
 
 
 
 
 