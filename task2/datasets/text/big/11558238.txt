The Boogeyman (1980 film)
{{Infobox Film
| name           = The Boogeyman
| image          = Boogeymanposter.jpg
| image_size     = 
| caption        = Theatrical Poster for The Boogeyman
| director       = Ulli Lommel
| producer       = Ulli Lommel   Gillian Gordon
| writer         = Ulli Lommel
| narrator       = 
| starring       = Suzanna Love   John Carradine   Ron James
| music          = Tim Krog
| cinematography = Jochen Breitenstein
| editing        = Terrell Tannen
| distributor    = The Jerry Gross Organization   Image Entertainment
| released       = 
| runtime        = 82 min   85 min (UK Extended version)
| country        = United States English
| budget         = $300,000 (estimated)
| gross          = 4,500,000 (USA)   $35,000,000 (Worldwide)
| preceded_by    = 
| followed_by    = 
}} 1980 American horror film directed by Ulli Lommel and starring Suzanna Love, John Carradine, and Ron James. The plot concerns an adult brother and sister who are tormented by what they believe to be the ghost of their mothers dead boyfriend.
It was followed by Revenge of the Bogey Man (aka Boogeyman 2) and Return of the Boogeyman.

==Plot==
The film opens with Willy and Lacey as children watching their mother and her lover preparing to have sex. When the mother notices them, she has her boyfriend tie Willy to his headboard before sending Lacey to her room. Lacey frees Willy from his bed, and Willy enters their room and repeatedly stabs his mothers boyfriend to death with a kitchen knife in front of a large mirror.

Twenty years later, Lacey, now an adult, is married with a young son and lives with her aunt and uncle on a farm. Willy also lives with them, but has not spoken a word since the night he killed his mothers lover, and sometimes, Willy takes various knives from the kitchen and hides them in a drawer. One night over dinner, Lacey finds a letter in the mail from her mother, who claims to be on her deathbed and wishes to see them one last time; that night, Willy burns the letter.

Lacey suffers from nightmares, and has a particularly frightening dream where she is dragged, tied to a bed and stabbed with a kitchen knife by an unseen person. Her husband, Jake, takes her to a psychiatrist to help her confront her fears, and they decide to visit the house she grew up in. They arrive not knowing who is actually living there, and they meet two teenage girls and their younger brother. Their parents, the homeowners, have apparently just placed the home for sale and then gone out of town, and the daughter thinks Lacey and Jake have been sent by the real estate company to view the house. Jake and Lacey pretend they want to buy the house so they can look around. At the house, however, Lacey sees a reflection of her mothers dead lover coming towards her in a mirror in the bedroom where he died, and smashes the mirror in a panic with a chair. Her husband takes the broken mirror with him in an attempt to repair it, but a piece is left behind which later glows red as the teenage girls and their brother are all killed by an unseen force. The murderous spirit of the dead lover has been released from the mirror.

Willy is also having problems with mirrors. Seeing his reflection in one causes him to nearly strangle a girl and so he paints all the mirrors in the house black. Later, pieces of a broken mirror in a bag at his feet cause a pitchfork to levitate and nearly impale him, but misses the attack, as he gets saved from it.

Another shard from the broken mirror becomes stuck to Laceys sons shoe, and is left on the ground where the light refracts across a lake where a group of teenagers are partying in an abandoned house. All of them are killed, including a couple who are impaled by a screwdriver while kissing in their car. Soon, Lacey then tries to get in the house, only to see that her shirt starts to tear apart from an unseen force.

Later, Laceys husband brings in the family priest to investigate the mirror, only to see that when the priests hand touches the mirror, the mirror turns red. A piece of the mirror floats across the room and becomes lodged over Laceys eye, possessing her; she begins levitation|levitating. It is only through the actions of the family priest that the shard is removed (during which the family priest is stabbed by various floating knives) and thrown into water, where it bursts into flames as he dies from his wounds. The remainder of the mirror is thrown into a well, where the same thing happens, as an explosion releases and thus, destroying the mirror once and for all.

The film ends with Lacey, her husband and the children visiting the graveyard, only to notice that one last mirror shard, missed by Lacey and her husband, glowing red on the ground.

==Cast==
*Suzanna Love as Lacey
*Ron James as Jake
*John Carradine as Dr. Warren
*Nicholas Love as Willy
*Raymond Boyden as Kevin

==Production==
 
The film also uses several apparent pieces of folklore and superstition regarding mirrors – as well as the belief that it is bad luck to break a mirror, the film also discusses the belief that breaking a mirror releases everything the mirror has ever seen and that placing the pieces of a broken mirror into a bag and burying it will counteract the bad luck from breaking the mirror. Additionally, there is the belief that a mirror in a room where someone has died will show the dead person looking back over the shoulder of anyone looking into the mirror. All this was used in the Mexican translation of the film title, released as "El espejo asesino" (the killer mirror)  {{cite web|url=http://www.iedb.net/movie/aka/el-espejo-asesino
|title=El espejo asesino|accessdate=2011-11-17}} 

==Release==
The film was given a limited release theatrically in the United States by The Jerry Gross Organization beginning in November 1980.  It was subsequently released on VHS by Wizard Video. 

The film has been released on DVD twice in the United States.  The first release was in 1999 by Anchor Bay Entertainment alongside Lommels The Devonsville Terror (1983).   This version is currently out of print.  It was subsequently re-released by Sony Pictures Home Entertainment in 2005 alongside Lommels Return of the Boogeyman (1994).   DPP list in 1984, but was later re-released on the Vipco label in 1992 in a cut form. In 2000 it was released uncut.
==Reception==
 
Some critics have said that director Lommel was inspired by John Carpenters Halloween (1978 film)|Halloween when he made The Boogeyman, most notably because of the similarities in the musical score and the fact that the killer in both films is a silent man with his face obscured as to make him effectively featureless.   In fact, the protagonist characters in Halloween specifically refer to that films killer as being, in effect, a physical embodiment of the "boogeyman" legend.

==Sequels== flashback sequences from the first film.

Return of the Boogeyman (or Boogeyman 3) was released on May 5, 1994, and is largely constructed around numerous flashbacks to The Boogeyman as well.

Lommel has expressed interest in making a fourth film, tentatively titled Boogeyman 4D. 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 