Cinerama Adventure
{{Infobox film
| name           = Cinerama Adventure
| image          = 
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = David Strohmaier
| producer       = Randy Gitsch
| writer         = 
| screenplay     = 
| story          = David Strohmaier
| narrator       = 
| starring       = 
| music          = 
| cinematography = Gerald Saldo
| editing        = David Strohmaier
| studio         = C.A. Productions Turner Entertainment
| distributor    = Warner Home Video
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Cinerama Adventure is a 2002 documentary about the history of the Cinerama widescreen film process.  It tells the story of the widescreen process evolution, from a primitive multi-screen pyramid process to a Vitarama format that played a big part in World War II, to the three-screen panoramic process it eventually became.  The film includes interviews with surviving cast and crew who personally worked on the Cinerama films, plus vintage interviews with late creator Fred Waller.
 How the West Was Won. 

It was written, produced, directed, edited and narrated by David Strohmaier; produced by Randy Gitsch; executive produced by Davids wife, Carin-Anne Strohmaier; and was presented in association with the American Society of Cinematographers.  The running time is 97 minutes.
 How the West Was Won by Warner Bros. Entertainment Inc.

Interviews:
*Carroll Baker
*Joe Dante Otto Lang
*A. C. Lyles
*Leonard Maltin
*David Raksin
*Debbie Reynolds
*Russ Tamblyn
*Lowell Thomas, Jr.
*Mike Todd, Jr.
*Eli Wallach

==External links==
* 
* 
 
 
 
 
 
 
 
 

 