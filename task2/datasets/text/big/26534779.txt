The Voice of Bugle Ann
 

{{Infobox film
| name           = The Voice of Bugle Ann
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Richard Thorpe
| producer       = John W. Considine Jr.
| writer         = Harvey Gates, Samuel Hoffenstein, MacKinlay Kantor (novel)
| starring       = Lionel Barrymore Maureen OSullivan
| music          = Rudolph Kopp
| cinematography = Ernest Haller
| editing        = George Boemler
| studio         = Metro-Goldwyn-Mayer
| distributor    = Loews Cineplex Entertainment|Loews, Inc.
| released       =  
| runtime        = 72 minutes
| country        = United States English
| budget         = 
| gross          =
}}

The Voice of Bugle Ann is a 1936 film directed by Richard Thorpe, based on a novel of the same title by MacKinlay Kantor.

== Brief Synopsis ==
A Missouri farmers love for his hunting dog triggers a feud that divides the county.  The movie is based on a novel of the same name by MacKinlay Kantor which in turn is loosely based on the tale of Old Drum, a foxhound which is said to the inspiration of the "Mans best friend (catchphrase)|Mans Best Friend" speech in 1870.

== Cast ==
* Lionel Barrymore as Spring Davis
* Maureen OSullivan as Camden Terry
* Eric Linden as Benjy Davis Dudley Digges as Jacob Terry
* Spring Byington as Ma Davis Charley Grapewin as Cal Royster
* Henry Wadsworth as Bake Royster William Newell as Mr. Tanner
* James Macklin as Del Royster
* Jonathan Hale as District Attorney Frederick Burton as The warden

==See also==
*Lionel Barrymore filmography

== Release Date ==

15 February 1936

== Production Dates ==

25 November—30 December 1935

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 


 