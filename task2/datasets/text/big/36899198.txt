Three Men to Kill
{{Infobox film
| name = Trois hommes à abattre Three Men to Kill
| image = Three Men to Kill - dvd cover.jpg
| caption = 
| imagesize = 
| director = Jacques Deray
| writer = Jean-Patrick Manchette (novel) Jacques Deray Alain Delon Christopher Franck
| music =  	Claude Bolling
| cinematography =  Jean Tournier
| starring = Alain Delon Dalila Di Lazzaro
| studio = Antenne-2 Adel Productions Union Générale Cinématographique (UGC)
| released = 31 October 1980
| country = France
| runtime = 95 minutes
| language = French gross = 2,194,795 admissions (France) 
}}
 French crime film released in 1980 in cinema|1980, directed by Jacques Deray, starring Alain Delon with Dalila Di Lazzaro. The screenplay is written by Jacques Deray, Alain Delon and Christopher Franck based on the novel by Jean-Patrick Manchette.

The story is about Michel Gerfaut (Delon), charming professional card player who out of nowhere becomes involved in some retribution between weapon traders of high level.

The movie was a great boxoffice success.  Three Men to Kill is the first film of a group of popular movies released in the 1980s and starring Alain Delon, which share a visual and narrative style, followed by For a Cops Hide (1981) and Le battant (1983).

==Synopsis==
Michel Gerfaut is a handsome middle-aged man who lives in Paris, France. He has a beautiful girlfriend and works as a professional poker player. One night, as he is returning from yet another card game, he comes across a car accident. Noticing that the driver of the crashed car is still alive, he brings him to the hospital. On the following day, Gerfaut learns from the newspaper that the man passed away and that he, actually was shot. On top of that, he has been known as a public figure involved in dirty deals with weapons. Gradually, Gerfaut realizes that he has become a target for assassins who eliminate any possible witnesses.

==Cast==
*Alain Delon as Michel Gerfaut
*Dalila Di Lazzaro as Béa
*Michel Auclair as Leprince
*Pascale Roberts as Mrs Borel
*Lyne Chardonnet as nurse
*Jean-Pierre Darras as Chocard
*Bernard Le Coq as Gassowitz
*François Perrot as Etienne Germer
*André Falcon as Jacques Mouzon
*Féodor Atkine as Leblanc Daniel Breton as Carlo
*Christian Barbier as Liethard
*Simone Renant as Mrs Gerfaut
*Pierre Dux as Emmerich

==References==
 

==External links==
*  

 
 
 
 
 
 
 


 
 