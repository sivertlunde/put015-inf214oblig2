Prom Night (1980 film)

{{Infobox film
| name           = Prom Night
| image          = Prom night film poster.jpg
| caption        = Theatrical release poster Paul Lynch
| writer         = William Gray Robert Guza, Jr.
| producer       = Peter R. Simpson Richard Simpson
| starring       = Leslie Nielsen Jamie Lee Curtis 
| distributor    = Avco Embassy Pictures
| music          = Paul Zaza Carl Zittrer
| cinematography = Robert C. New
| editing        = Brian Ravok
| released       =  
| runtime        = 93 minutes
| country        = Canada
| language       = English CAD    USD    }}
 slasher film Paul Lynch, based on a story by Robert Guza, Jr., and starring Leslie Nielsen and Jamie Lee Curtis. The story concerns a group of high school seniors who are targeted by an mysterious killer in revenge for their culpability in the accidental death of a young girl six years earlier. The anniversary of the incident falls on their high schools prom night, when the older sister of the dead girl is being crowned prom queen.
 Genie Award nominations for editing and for Curtis performance. 

==Plot==
In 1974, 11-year-olds Wendy Richards, Jude Cunningham, Kelly Lynch, and Nick McBride play hide and seek in an abandoned convent. 10-year-old Robin Hammond tries to join them, but they start teasing her by repeating "Kill! Kill! Kill!". Robin is backed towards a window from which she falls to her death. The children make a pact not to tell anyone what happened and keep the incident a secret, but after they leave, a shadow falls across Robins body. A rapist is mistakenly blamed for Robins death instead and is arrested.

Six years later in 1980, Robins family attend her memorial on the anniversary of her death. Robins teenage sister and brother, Kim and Alex, are also preparing for the school prom to be held that evening. Their parents will also attend, as their father is the school principal. Kelly, Jude and Wendy receive threatening phone calls from an unknown figure, while Nick ignores his ringing phone. Kim and Nick, whom she is dating, are attending prom together; Jude is asked by goofy jokester Seymour "Slick" Crane whom she meets by chance that morning; Kelly is going with her boyfriend Drew Shinnick (who is preoccupied with having sex with her despite her objections), while Wendy, previously Nicks girlfriend, asks the school rebel, Lou Farmer with plans to embarrass Nick and Kim at prom. 

In the changing room after gym class, Kim and Kelly discover the locker room mirror severely cracked and a shard missing. The offender blamed for Robins death has escaped and Lt. McBride, Nicks father, investigates. During the senior prom Kim and Nick perform a dance number to impress Wendy who had insisted Nick would be getting back with her after the prom. Kelly and Drew make out in the changing room, but Kelly refuses to continue to full sex. Drew angrily leaves. As Kelly gets dressed, a masked killer slits her throat with the mirror shard. Jude and Slick have sex and smoke marijuana in his van outside school grounds. However, they are being watched by Kellys killer, who kills Jude by stabbing her throat. Slick brawls with the killer while attempting to drive away. The killer escapes from the van as it tumbles off a cliff and explodes, crushing and burning Slick to death in the flaming wreckage. McBride, staking out the prom, is informed that the sex offender blamed for Robins death has been caught. He is relieved and ends his scrutiny of the event.

Wendy is confronted by the murderer, now using a felling axe and is chased through the school. After evading the killer several times, she is caught and hacked to death with the axe after she finds Kellys body. Kim and Nick prepare to be crowned prom king and queen. Wendys plan is put into action by Lou and his lackeys who tie up Nick with Lou taking his crown and assuming his position back stage. Thinking he is Nick, the assailant sneaks up behind Lou with the axe and decapitates him.

Lous head rolls onto the dance floor, sending the partygoers fleeing in horror. Kim finds Nick and frees him. As they prepare to escape, they are confronted by the killer who attacks Nick. After Kim hits the killer in the head with his own axe, Kim and the killer stare at each other for a moment, Kim realizes who he really is. He runs outside where the police have arrived. As guns are raised Kim screams for the officers not to shoot. The killer is revealed to be Alex, who tearfully tells his sister that Jude, Kelly, Wendy and Nick were responsible for their sisters death, dying in Kims arms as he cries Robins name.

==Cast==
* Leslie Nielsen as Mr. Hammond
* Jamie Lee Curtis as Kim Hammond
* Casey Stevens as Nick McBride Eddie Benton as Wendy Richards
* Michael Tough as Alex Hammond
* Robert A. Silverman as Mr. Sykes
* Pita Oliver as Vicki
* David Mucci as Lou Farmer
* Mary Beth Rubens as Kelly Lynch
* George Touliatos as Lt. McBride Melanie Morse as Henri-Anne
* David Bolt as Weller
* Jeff Wincott as Drew Shinnick
* David Gardner as Dr. Fairchild
* Joy Thompson as Jude Cunningham
* Sheldon Rybowski as Seymour "Slick" Crane
* Antoinette Bower as Mrs. Hammond

==Production==
===Development=== Paul Lynch developed Prom Night after a meeting with producer Irwin Yablans, who had previously produced Halloween (1978 film)|Halloween (1978). Lynch had wanted to work on a horror film, and, in response to Yablans suggestion that he utilize a holiday as a basis for the film, Lynch decided on building the premise around the event of the high school prom.   Writer Robert Guza Jr., whom Lynch was an acquaintance of, had written a story about a group of teenagers whose involvement in a tragic event as children came back to haunt them. Guzas story was then adapted and incorporated into the film as the central premise and motive for the films villain.   After approaching producer Peter Simpson with the idea, Lynch and Simpson signed an agreement within a matter of four days.  

In the documentary   (2006), Lynch stated he was having difficulty securing financing for the film until Jamie Lee Curtis signed onto the project. According to the producer of Prom Night, Eve Plumb (from televisions The Brady Bunch) originally auditioned for the role of Kim Hammond,  but was passed over after Jamie Lee Curtis manager contacted Paul Lynch about doing the film. {{cite DVD notes
| title       =The Horrors of Hamilton High: The Making of Prom Night
| url         =
| author      = 
| others      =
| type        = documentary
| publisher   = Synapse Films/Red Shirt Pictures
| location    =
| id          =
| year        =2014
}}  A great deal of the actors and actresses playing the students were stage actors and recent theater graduates from the University of Toronto.  

===Filming=== Queen Street Provincial Asylum was used for the abandoned building featured prominently in the beginning of the film.  

==Release== Friday the 13th, which premiered two months before Prom Night. 

Prom Night was a financial success, grossing $14,796,236 in the United States.    It earned an addition $6 million in rentals during its home video release. Richard Nowell, Blood Money: A History of the First Teen Slasher Film Cycle Continuum, 2011 p 260 

==Soundtrack== Black Christmas (1974).   The soundtrack of Prom Night includes several disco songs which are featured prominently in the films prom scene. Originally, the film was shot with the actors dancing to then-popular tracks by Donna Summer and Pat Benatar, but, according to Zaza, the publishing rights to the songs were far outside the films budget.   Under orders from producer Peter Simpson, Zaza wrote a series of disco songs over a five day period, closely copying the original tracks that were intended to be used in the film. This resulted in a copyright lawsuit for $10 million, which was eventually settled for $50,000.  
 Canadian horror productions that Paul Zaza scored as well; 1981s Ghostkeeper and 1983s Curtains (1983 film)|Curtains.  The song "Prom Night" was featured in Cabin Fever 2.

===Track listing===
# "All Is Gone" by Blue Bazar
# "Prom Night"
# "Changes"
# "Dancing in the Moonlight"
# "Fade to Black"
# "All Is Gone" (Instrumental) by Blue Bazar
# "Time to Turn Around"
# "Love Me Till I Die"
# "Prom Night 2"
# "Forever" by Blue Bazar

==Home media== MCA Universal in North America, at the beginnings of home video popularity, licensed directly from then-production company SimCom, who had licensed theatrical distribution to Avco-Embassy. In 1988, it was re-released on VHS by Virgin Vision in tandem with the in-name-only sequel Hello Mary Lou; Prom Night II, which Virgin handled through a separate deal with that films then-distributor The Samuel Goldwyn Company. In 1997, the film was re-released again, by Anchor Bay Entertainment, in both standard and "collectors" editions.  It was then released on DVD by Anchor Bay on February 18, 1998 with a re-mastered widescreen transfer, and was one of the companys first DVD releases. By 2000, Anchor Bays DVD release had gone out of print and became a rarity among film fans.

It was released again to DVD in Canada by Alliance Atlantis in March 2004, but was sourced from an extremely dark, low-quality VHS transfer, which resulted in some of the films darker scenes being nearly illegible; this transfer was also used for Platinum Discs full-screen DVD edition of the movie for the US, and has turned up in a couple horror movie collections as well. In September 2007, Echo Bridge Home Entertainment re-released the film on DVD in the US in a completely re-mastered print from a PAL source, which was given an uncorrected transfer to NTSC. Due to the uncorrected transfer, the film is slightly "sped up", which, though mostly unnoticeable to the naked eye, reduced the films run time by several minutes.

On September 9, 2014, the film was released on Blu-ray and DVD by Synapse Films, featuring a restored print from the original film negatives, as well as featuring a documentary as well as outtakes, original promotional material, and deleted scenes as bonus material.

==Reception==
The film has an approval rating of 37% on review aggregator website Rotten Tomatoes, based on 19 reviews, certifying it "rotten".  AllRovi|AllMovies review of the film was generally negative, but wrote that it "utilizes a surprising amount of skill both behind and in front of the camera as it goes through its paces". 

===Award nominations===
* 1981 Genie Awards
** Best Achievement in Film Editing – Brian Ravok
** Best Performance by a Foreign Actress – Jamie Lee Curtis

==Sequels and remake==
The Prom Night film series include four films and one remake (which tells a completely different story, with little connection to the 1980 film).

* Prom Night (1980)
*   (1987)
*   (1989)
*   (1992) Prom Night (2008)

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 