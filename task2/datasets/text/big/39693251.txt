We Stick Together Through Thick and Thin
{{Infobox film
| name           = We Stick Together Through Thick and Thin
| image          = 
| image_size     = 
| caption        = 
| director       = Herbert Nossen 
| producer       = 
| writer         = Hans Kahan
| narrator       = 
| starring       = Sig Arno   Kurt Gerron   Ernst Karchow   Vera Schmiterlöw
| music          = 
| editing        =
| cinematography = Willy Hameister
| studio         = Ama-Film  
| distributor    = 
| released       = 16 September 1929
| runtime        = 
| country        = Germany
| language       = Silent   German intertitles
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} German silent silent comedy film directed by Herbert Nossen and starring Sig Arno, Kurt Gerron and Ernst Karchow. It was one of a number of films starring Arno and Gerron in their characters of Beef and Steak in an effort to create a German equivalent to Laurel and Hardy. 

==Cast==
* Sig Arno as Beef 
* Kurt Gerron as Steak 
* Ernst Karchow as Theodor Klabautermann 
* Vera Schmiterlöw as Kitty, Tochter 
* Evi Eva as Köchin 
* Antonie Jaeckel as Freifrau von Gotha, Heiratsvermittlerin 
* Lotte Roman as Stubenmädchen 
* Edith Meller as Carola Triller 
* Claire Claery  
* Carl Geppert as Assessor 
* Ewald Wenck as Assessor 
* Arnold Hasenclever as Teddy 
* Max Grünberg as Justizrat Scharf 
* Otto Hoppe as Geschäftsführer des Restaurants Newa-Grill

==References==
 

==Bibliography==
* Prawer, S.S. Between Two Worlds: The Jewish Presence in German and Austrian Film, 1910-1933. Berghahn Books, 2005.

==External links==
* 

 
 
 
 
 
 
 


 
 