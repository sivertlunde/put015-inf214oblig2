Sandakozhi
{{Infobox film
| name = Sandakozhi
| image = Sandakozhi.jpg
| director = N. Linguswamy
| producer = Vikram Krishna
| writer = N. Linguswamy S. Ramakrishnan (dialogues) Vishal Meera Lal Suman Setty Ganja Karupu
| studio = GK Films Corporation
| music = Yuvan Shankar Raja Jeeva
| editing = G. Sasikumar
| released =  
| runtime = 151 minutes
| language = Tamil
| country = India
| budget = 
}}
 2005 Cinema Indian Tamil Tamil action film written and directed by N. Linguswamy and produced by Vikram Krishna under the banner of GK Film Corporation. The film stars Vishal Krishna|Vishal, Meera Jasmine, Rajkiran and Lal in lead roles, whilst Suman Setty, Raja, Shanmugarajan and Ganja Karuppu essay supporting roles. The films score and soundtrack are composed by Yuvan Shankar Raja.  EadThe film released on 16 December 2005 to rave reviews and has become one of the huge hits in 2005, running for more than 200 days in theatres.  The film was remade in Kannada as Vayuputra. It was declared a blockbuster at the Box office

==Plot==

The story is about Balu (Vishal), who on a visit to his friend’s town Chidambaram, takes on the local dada Kasi (Lal). Balu, an engineering student, also falls in love with his friend’s sister Hema (Meera Jasmine).

Kasi, after being at the receiving end of Balu’s macho ways, is smarting under the rebuff. He thirsts for revenge. So when Balu goes back to his own city Madurai, Kasi also goes there to extract blood for blood. But Balu is from no ordinary family. His father Dorai (Raj Kiran) is himself a powerful leader in his domain. So taking out Balu is not such an easy task. But Kasi wants revenge at any cost. He befriends a local goon(Thalaivaasal Vijay) who is made an outcast by Dorai and they together plan to exact revenge. Meanwhile Balus friend and his family visits his city as they are on a pilgrimage they meet Balu at the temple and accept his apology. He proposes to Hema and their love is accepted by their parents. The goons plan to exact revenge during the annual temple festival as they reason that it will be crowded and that Dorai and his henchmen will not be able to defend themselves .They attack but the events do not turn out in their favor. Finally Balu fights with Kasi and triumphs in the end.

==Cast==
{| class="wikitable" width="50%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
| Vishal Krishna || Balu
|-
| Meera Jasmine || Hema
|-
| Rajkiran || Durai
|- Lal || Kasi
|-
| Suman Setty || Ilango
|-
| Ganja Karuppu || 
|-
| Shanmugarajan ||
|-
| Thennavan ||
|-
| Raja || Karthik
|-
| Thalaivaasal Vijay ||
|-
| Kaadhal Thandapani ||
|- Monica ||
|-
| Dr Sridhar Kumar|K. Sridhar Kumar ||
|-
| Manochithra Vedi  ||
|-
| Vidharth ||
|-
| Elango Kumaravel (uncredited) || 
|}

==Crew==
* Story, Screenplay & Direction: N. Linguswamy
* Production: Vikram Krishna
* Music: Yuvan Shankar Raja Jeeva
* Editing: G. Sasikumar
* Art direction: V. Selvakumar
* Stunts: Kanal Kannan
* Audiography: H. Sridhar
* Choreography: Raju Sundaram, Tharun Kumar, Rekha Chinni Prakash & Dinesh
* Stills: Ranga Rao D.
* Lyrics: Pa. Vijay, Na. Muthukumar, Yuga Bharathi & Thamarai
* Dialogue: S. Rama Krishnan
* Co-Direction: Kanagu
* Production executive: K. Balakrishnan
* Banner: GK Films Corporation

==Production==
Meera Jasmine-Lingusamy duo, after successfully teaming up together for Run, come together again in Sandai Kozhi, shortly set to hit the theaters. The film gives scope for Meera and Vishal to dominate the first and second half respectively. For Vishal its his second release after Chellamey, and for director Lingusamy its his fourth film after Anandam, Run and Ji. Title is named after the song from Aayutha Ezhuthu which also featured Meera Jasmine.

Playing the villain in the film is popular Malayalam actor and producer Lal, whod played a dual role in the Vijaykanth-starrer Engal Anna. Rajkiran plays the village chief.

A fight scene involving hero Vishal and Lal was picturised in Dindigul for seven days. The songs have been shot at locations in Australia, New Zealand and Chennai. An introduction song for Rajkiran was shot at Theni. 

==Soundtrack==
{{Infobox album |  
| Name = Sandakozhi
| Type = soundtrack
| Artist = Yuvan Shankar Raja
| Cover = Sandakozhi CD cover.jpg
| Released =  25 November 2005 (India)
| Recorded = 2005 Feature film soundtrack
| Length = 
| Label = New Music
| Producer = Yuvan Shankar Raja
| Reviews =
| Last album  = Kanda Naal Mudhal (2005)
| This album  = Sandakozhi (2005) Kalvanin Kadhali (2005)
}}

The soundtrack, composed by Yuvan Shankar Raja, teaming up for the first time with director N. Linguswamy, was released on 25 November 2005. It features 5 tracks, the lyrics of which were written by Pa. Vijay, Na. Muthukumar, Yuga Bharathi and Thamarai. Both the film score as well as the songs were appreciated and praised as outstanding with the song "Ennamo Nadakirathe" considered as the pick of the album.  

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s)!! Duration !! Lyricist!! Notes
|- 1 || "Dhavanipotta Deepavali"  || Vijay Yesudas, Shreya Ghoshal || 4:23 || Yuga Bharathi ||
|- 2 || "Ennamo Nadakkirathe" ||   ||
|- 3 || "Gumthalakkadi Gana"  ||   ||
|- 4 || "Ketta Kodukkira Boomi"  ||   ||
|- 5 || "Mundasu Sooriyane"  ||   ||
|}

==Box office==
*The film was a commercial success grossing $3.5 million at the box office. 

==References==
 

==External links==
*  
* 

 

 
 
 
 
 
 
 
 
 