Namukku Parkkan
{{Infobox film
| name           = Namukku Parkkan
| image          = NamukkuParkkan.jpg
| image_size     =
| border         =
| alt            =
| caption        = Film poster
| director       =  Aji John
| producer       =  Joy Thomas Sakthikulangara
| writer         =
| screenplay     = Jayan-Sunoj
| story          =
| based on       =  
| narrator       =
| starring       = Anoop Menon  Meghna Raj   Jayasurya
| music          = Ratheesh Vegha
| cinematography =  S.B.Prijith
| editing        = Shamjith
| studio         =
| distributor    = Jithin Arts
| released       =  
| runtime        =
| country        = India
| language       = Malayalam
| budget         =
| gross          =
}}
Namukku Parkkan ( ) is a 2012 Malayalam film directed by Aji John, starring Anoop Menon and Meghna Raj in the lead roles.   
Jayasurya plays a pivotal guest appearance, while Mohanlal makes a vocal appearance in the film.

==Plot==
Namukku Parkkan reflects the realities of a middle class Malayali family.  Rajeev (Anoop Menon) is a veterinary surgeon and his wife Renuka (Meghna Raj) is a primary school teacher. They have two school- going daughters.They live in a small house and doesnt have a proper bathroom ,one day somebody peeps in when meghana was bathing. They lead a happy and contended life, but they have a dream and that is to own a house. They have plans for their ideal house, a house merging with nature, with lots of trees and a lovely garden and cattle roaming the compound. But the dream is beyond their means. All others in their family own houses. But Rajeev and family still live in a rented house. On top of that, Rajeev has been served with a note to vacate the house. He has asked for some grace period till he finds a new house. Then an unexpected incident in his life makes it inevitable for him to build a house. He sets out on a journey to own a house and the realities he encounters in his journey form the rest of the story.  C.I. Velu Nagarajan(Jayasurya) a Kannada police officer residing in Kerala "helps" Rajeev and rest of the story is of the other incidents he faces.

==Cast==
*Anoop Menon as Rajeev, as a man who dreams high of establishing a house for his loving family.
*Meghana Raj as Renuka, as the wife of Rajeev Janardhanan as Krishnan Ammavan
*Jayasurya as C.I. Velu Nagarajan, as Kannada speaking sub-inspector (Guest Appearance)
*Mohanlal (voice) Ashokan as K.K 
*Tini Tom as Balan
*Sudheesh
*Sudheer Karamana  
*Geetha Vijayan 
*Nandhu
* Kalabhavan Shajon

==Reception==
The music rights of movie was obtained by Sathyam Audios for a price close to 13 lakh.  Movie as a whole was rated as average by Sify  and Rediff review with rediff calling Anoop Menons acting impressive. 

==References==
 

 