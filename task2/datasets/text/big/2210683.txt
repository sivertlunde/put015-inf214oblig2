Jagte Raho
 
{{Infobox film
| name           = Jagte Raho
| image          = Jagte Raho 1956 film poster.jpg
| image_size     = 
| caption        = Film poster
| director       = Amit Maitra  Sombhu Mitra
| producer       = Raj Kapoor
| writer         = K. A. Abbas Amit Maitra Sombhu Mitra
| starring       = Raj Kapoor
| music          = Salil Choudhury
| cinematography = Radhu Karmakar
| editing        = G. G. Mayekar Vasant Sule
| distributor    = R.K. Films Ltd.
| released       = 1956
| runtime        = 149 min.
| country        = India Bengali
| budget         = 
}}
Jagte Raho ( , translation Stay Awake or Stay Alert) is a List of Bollywood films of 1956 | 1956  Bollywood film directed by Amit Maitra and Sombhu Mitra, produced by and starring Raj Kapoor.    The film centers on the trials of a poor villager (Kapoor) who comes to a city in search of a better life. However, the naive man soon becomes trapped in a web of middle-class greed and corruption. The film also features a cameo by Nargis in the final scene.

It was produced in Bengali as Ek Din Raatre, starring Raj Kapoor, Chhabi Biswas, Pahari Sanyal, Nargis Dutt, Daisy Irani.    

==Plot summary==

A poor peasant (Kapoor) from the village, who comes to the city in search of work, is looking for some water to quench his thirst. He unwittingly enters an apartment building, whose residents take him for a thief and chase him. He runs from one flat to the other trying to escape his predicament. Along the way, he witnesses many shady undertakings in the flats where he hides. Ironically, these crimes are being committed by the so-called "respectable" citizens of the city, who by day, lead a life totally in contrast to their nighttime deeds behind closed doors.

He is shocked by these events, and tries to escape by evading the search parties, that are patrolling the apartment building in search of the elusive thief. He is unfortunately seen, and people chase him to the roof of the building. He puts up a brave resistance, and then descends by the water pipes onto the porch of a flat. He goes in to find a young girl (Daisy Irani). She talks to him and kindles a self belief in the peasant, who determinedly tries to face the adversity waiting outside. But when he ventures out of the flat, he is surprised to find that nobody takes notice of him. He eventually leaves the apartment building, his thirst still unquenched. He hears a beautiful song and searching for its source arrives at the doorstep of a woman (Nargis) drawing water from a well. His thirst is finally assuaged.

==Music== Shailendra and Prem Dhawan and music is by Salil Choudhary.

The songs are

01. Zindagi Khawab Hai, Khvaab Me Jhuth Kya Aur Bhala Sach Hai Kya   -   Mukesh

02. Main Koi Jhoot Boleya  sung by Mohammed Rafi and S. Balbir and 

03. Jaago Mohan Pyaare   -   Lata Mangeshkar 

04. Aiven Duniya Deve Duhayi, Teki Main Jhuth Bolaya, Koi Na    -   Mohammed Rafi, Balbir

05. Jab Ujiyara Chhaye, Mann Kaa Andhera Jaye   -   Lata Mangeshkar 

06. Thandee Thandee Savan Kee Phuhar   -   Asha Bhosle

07. Maine Jo Li Angdayi   -   Haridhan, Sandhya Mukherjee
 
     are still remembered by people as classics.

==Music, Bengali version Ek Din Raatre==
The song Zindagi khwab hai, picturized on Motilal in Hindi was recorded as Ei Duniaye shobi hoi in the Bengali version and was picturized on Chhabi Biswas. Most of the story line is identical between the two versions, as were the songs; Teki main jhuth boliya, sung by Mohammed Rafi and picturized on Sikh drivers is consistent in both versions. Jago Mohan Pritam sung by Lata Mangeshkar is common to both versions - only the lyrics were changed to Hindi and Bengali, as applicable.

==Cast==
*Raj Kapoor
*Nargis (cameo)
*Motilal
*Pradeep Kumar
*Sumitra Devi
*Sulochana Chatterjee
*Smriti Biswas Nemo
*Nana Palsikar
*Iftekhar Ahmed
*Daisy Irani

==Cast, inter alia for the Bengali version (Ek Din Raatre)==
*Raj Kapoor 
*Chhabi Biswas
*Pahari Sanyal
*Nargis Dutt (cameo)
*Daisy Irani

==Awards== National Film Awards, the film won the Certificate of Merit. 

==References==
 

==External links==
*  

 

 
 
 