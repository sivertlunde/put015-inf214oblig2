Gangvaa
{{Infobox film|
| name = Gangvaa
| image = Gangvaafilm.jpg
| caption = Vinyl Records Cover Rajasekhar
| writer =
| starring =  
| producer = Dwarakish
| music = Bappi Lahiri
| editor =
| released = 14 September 1984
| runtime =
| language = Hindi
| budget =
}}
 Tamil film Malaiyoor Mambattiyan that starred Thyagarajan and Saritha in the lead roles.

==Plot==
The basic injustice at the core of Gangvaa is the way the landed classes take advantage of ordinary village folk.Early on in the film, a work crew finds a pot full of gold coins, and a suitably mustache-twirling zamindar (Amrish Puri) confiscates them to add to his already massive wealth.  Enter Gangvaa (Rajnikanth) to save the day;  Gangvaa kills zamindar and his goons. Gangvaa puts together a band of the men who had suffered under the zamindars tyranny. Together they hang out in the wilderness and perform Robin-Hood-esque raids for the sake of vigilante justice.  At some point Jamna (Shabana Azmi) encounters Gangvaa and is smitten.  Then a village girl accuses Gangvaa of rape, and Jamna is enraged - it is here that she gets down to find the truth. It turns out that the rape was actually done by a totally different guy named Gangvaa (Raza Murad), and righting this wrong wins Jamna back for our hero, but makes him a new set of enemies that he spends the rest of the film fleeing from.  Also on his tail is an police inspector(Suresh Oberoi), who cannot allow vigilante justice in his district, no matter how noble the intention.

==Cast==
*Rajinikanth
*Shabana Azmi
*Suresh Oberoi
*Amrish 
*Sarika

==Music==
{| border="4" cellpadding="6" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| Aare Aa Paas Aa
| Asha Bhosle, Bappi Lahiri
|-
| 2
| Aare Aa Paas Aa
| Kishore Kumar, Asha Bhonsle
|-
| 3
| Gangvaa
| Bappi Lahiri
|-
| 4
| O Jaanam
| Asha Bhonsle
|-
| 5
| Dance Music
| Bappi Lahiri
|-
| 6
| O Behna (Sad)
| Mohammad Aziz
|}

==References==
 

 
 
 
 


 