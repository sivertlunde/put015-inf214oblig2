The Sundowner (1911 film)
 
{{Infobox film
  | name     = The Sundowner
  | image    = 
  | caption  = 
  | director = 		
  | producer = 
  | writer   = 
  | based on = 
  | starring = Vera Remee Frank Mills Bohemian Dramatic Company
  | music    = 
  | cinematography = 
  | editing  = 
  | distributor = 
  | studio = Pathe Freres 
  | released = 19 August 1911 
  | runtime  = 2,000 feet   
  | language = Silent film English intertitles
  | country = Australia
  | budget   = 
  }}

The Sundowner is an Australian film shot in Victoria.  Set in the Australian bush, it was billed as "a romance with many startling adventures". 

It is not known who directed the movie but it may have been E. I. Cole as it featured his Bohemian Dramatic Company.  

It is considered a lost film.

==Plot==
A farmer refuses to let his daughter marry her admirer until he can show he can take care of her. The admirer turns out to be a villain. The girl marries a neighbouring squatter and they have a baby. The scorned admirer returns after a few years seeking revenge. He kidnaps the baby and there is a chase. 

==Cast==
*Vera Remee
*Frank Mills
*Bohemian Dramatic Company

==Production==
The film was shot in Victoria. 

==Reception==
According to contemporary reports, the film was well received by audiences in a number of states.  The Launceston Daily Telegraph said the film was "cleverly acted with the scenes cleanly depicted." 

Another report said:
 The Sundowner is a capital story of unbounded interest and excitement. The life in the Never, Never country, with its awesome loneliness and characteristic beauty, is defined throughout the lengthy picture with such incredible exactness that with small imagination the spectator is carried direct to the spot. "The Sundowner", being one of Pathe Freres first efforts in dramatic photography in our country, particular care has been paid in making it a pronounced success. Localities were explored and carefully considered, the site of operations being ultimately found out in the far west. A company of the first rank of colonial actors was selected to assume the many characters involved, and a staff of skilled photographic operators was deputed to carefully absorb this delightful dramatic story into photographic form for presentation to the many to whom this class of picture appeals  

==References==
 

==External links==
*  
*  at AustLit
 

 
 
 
 
 


 