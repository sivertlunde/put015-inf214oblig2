Sex Sells: The Making of Touché
 
{{Infobox film
| name           =Sex Sells: The Making of Touché 
| image          = 
| image_size     = 
| alt            = 
| caption        = 
| director       = Jonathan Liebert
| producer       = 
| writer         = 
| narrator       = 
| starring       = Priscilla Barnes
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 
| runtime        = 
| country        = 
| language       = 
| budget         = 
| gross          = $3,252 (USA)  
| preceded_by    = 
| followed_by    = 
}}
 2005 comedy film that was written and directed by Jonathan Liebert.  It was filmed as a faux documentary, in the same style as The Blair Witch Project and Cannibal Holocaust.  It tells the story of the making of a pornographic film.  Due to the films sometimes explicit (though simulated) sex scenes it has not received a rating from the MPAA.  Priscilla Barnes (who also served as an associate producer), Mark DeCarlo and Jay Michael Ferguson star.  96 minutes.

The U.S. DVD cover lists the films title simply as Sex Sells.

==Cast==
*Priscilla Barnes...Roxy Free
*Mark DeCarlo...Chuck Steak
*Jay Michael Ferguson...Bernard Heiman
*Lisa Jay...Pursey Galore
*Alexa Jago...Sally Ryder
*Adrian Zmed...Lance Long
*Jack Kyle...Pearce Boyle
*Eva Frajko...Nikki

==Plot==
The story concerns a young documentary film-maker, Bernard Heiman, who has decided to chronicle the making of a pornographic motion picture for his next project.  He selects as his subject a movie being directed by Chuck Steak (pronounced STEE-AK), a twenty-five year veteran of the porn industry.  Chuck plans to produce a $40,000 movie that is shot on motion picture film, as he regards himself as a film-maker first and pornographer second.  Bernard begins shooting his documentary and is introduced to Chucks large crew and cast.  He especially becomes attracted to a young performer named Pursey Galore, who is new to the porn industry.  As the film develops it becomes clear that Chuck treats his co-workers more like a family rather than colleagues, as they all deeply care about each other and view Chuck as a paternal figure.  As the film develops Chuck must deal with several crises, including a group of protesters who object to pornography and a pending legal proceeding that stems from him throwing a flower pot at a mans head.  All the while he remains determined to fulfill his ambition of filming the worlds largest recorded orgy.

==Box office==
Released on February 25, 2005, grossed $2,386 in the opening wekeend. Final domestic grossing is $3,252.

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 


 