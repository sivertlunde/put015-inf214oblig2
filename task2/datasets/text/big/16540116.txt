Days of Youth
{{Infobox film
| name           = Days of Youth
| image          =
| image_size     =
| caption        =
| director       = Yasujiro Ozu
| producer       =
| writer         = Akira Fushimi Yasujiro Ozu
| narrator       =
| starring       = Ichirō Yūki Tatsuo Saitō Junko Matsui
| music          =
| cinematography = Hideo Shigehara
| editing        =
| distributor    = Shochiku
| released       = April 13, 1929
| runtime        = 103 minutes
| country        = Japan
| language       = Japanese
}}
  is a Japanese comedy film directed by Yasujiro Ozu. It is the oldest known surviving film by the director.  The film tells of two friends from a university (played by Ichirō Yūki and Tatsuo Saitō) who vie for the attention of the same girl (Junko Matsui) during a skiing trip.

==Cast==

*Ichirō Yūki as Bin Watanabe (a student)
*Tatsuo Saitō as Shūichi Yamamoto (a student)
*Junko Matsui as Chieko
*Chōko Iida as Chiekos mother
*Eiko Takamatsu as Landlady
*Shōichi Kofujita as Shōji (her son)
*Ichirō Ōkuni as Professor Anayama
*Takeshi Sakamoto as Professor
*Shinichi Himori as Hatamoto (a student)
*Fusao Yamada as Kobayashi (a student)
*Chishū Ryū as Student

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 

 