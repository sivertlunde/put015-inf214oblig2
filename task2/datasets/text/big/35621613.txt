Kaun Hai Jo Sapno Mein Aaya
{{Infobox film
| name           = Kaun Hai Jo Sapno Mein Aaya
| image          = KaunHaiJoSapnoMeinAaya.png
| caption        = Theatrical release poster
| director       = Rajesh Bhatt
| producer       = Vibha-Ragini
| story          = Rajesh Bhatt
| screenplay     = Mehmood Ali
| starring       = Rakesh Bapat Richa Pallod
| music          = Nikhil-Vinay
| editing        = Sanjay Sankla
| cinematography = Uday Tiwari
| distributor    = Inspired Movies (UK)
| released       =  
| country        = India United Kingdom
| language       = Hindi
| runtime        = 127 minutes
| gross          = 11cr.
}}
 2004 Hindi romantic musical musical Bollywood|film directed by Rajesh Bhatt and written by Mehmood Ali. The film, produced by Vibha-Ragini under the Inspired Movies (UK) banner, stars Rakesh Bapat and Richa Pallod in their second film together after Tumse Milke... Wrong Number (2003).  Actor Anupam Kher is featured in a supporting role.

==Plot==
Sunny Khanna (Bapat) is a happy-go-lucky person who has a passion for poetry. He currently lives with his family who have settled in the United Kingdom. Although he and his family members are living together, Western cultures have influenced most of them to an extent of them being hardly the representation of a traditional Indian family.

Mahek (Pallod) arrives from India at the family home for a brief stay with the Khannas as her uncle, Dr. Verma (Kher) who is a family friend, is away to the United States for an important conference. During her stay at the Khannas, Mahek felt a certain awkwardness due to the family members’ dysfunctional attributes. Determined to fix the situation, she decides to bring the family members together in unison but failed. Things turn to the worse as she has been wrongfully accused for causing the death of an elderly family member due to negligence.

A tearful Mahek leaves the Khannas residence and soon afterwards, the Khannas are shocked to find that Mahek has been suffering a serious heart condition and has travelled to the UK as a final resort to seek treatment. The family members felt guilty of their ill-treatment towards Mahek and Sunny is sent to get Mahek in hopes of convincing her to return to the Khannas residence.

Mahek refuses to return, citing that her yearning in life is to be loved and not to be pitied. However, Mahek’s emotions are mixed as she tries hard not to fall in love with Sunny. Sunny was crestfallen; sad and disappointed by the events surrounding him and Mahek as he has fallen deeply in love with her.

Mahek ended up in the hospital for her surgery and beyond expectations, she made a full recovery. In the end, she is able to reunite with her love, Sunny.

==Cast==
* Rakesh Bapat as Sunny Khanna
* Richa Pallod as Mahek
* Kader Khan as Kuldeep Khanna
* Anupam Kher as Dr. Verma
* Reshmee Doolub as Sweety

==Reception==
The film received generally mixed, i.e. positive and negative reviews from film critics. Review aggregator Rotten Tomatoes reports that 67% of 22 critics have given the film a positive review, with a rating average of 3.7 out of 5. 

Taran Adarsh of Bollywood Hungama called the direction by Rajesh Bhatt "old-fashioned". He praised the performance of both Rakesh Bapat and Richa Pallod, but criticized the "lackluster and uninspiring presentation" and the "irritating" comedy track which "just fails to evoke mirth", summarizing the movie as a "dull fare" bound for box-office failure.   

==Soundtrack==
{{Infobox album  
| Name        = Kaun Hai Jo Sapno Mein Aaya
| Type        = Soundtrack
| Artist      = Nikhil-Vinay
| Cover       = KaunHaiJoSapnoMeinAayaSoundtrack.png
| Released    = 2004 Feature film Filmi music
| Length      = 53:29
| Language    = Hindi
| Label       = T-Series
}}

The soundtrack for Kaun Hai Jo Sapno Mein Aaya is composed by Nikhil-Vinay and the lyrics are penned by Sameer (lyricist)|Sameer. It is released by T-Series. 

===Track listing===
{{Track listing
| extra_column = Artist(s)
| title1 = Kaun Hai Jo Sapno Mein Aaya
| extra1 = Udit Narayan, Anuradha Paudwal Raju Pandit
| length1 = 6:55
| title2 = Mera Sona Sajan
| extra2 = Udit Narayan, Sneha Pant
| length2 = 7:09
| title3 = Tere Chehre Pe
| extra3 = Kumar Sanu, Anuradha Paudwal
| length3 = 6:17
| title4 = Agar Dil Kahe
| extra4 = Sonu Nigam, Shreya Ghoshal
| length4 = 6:14
| title5 = Sabke Chehron Mein
| extra5 = Udit Narayan
| length5 = 6:18
| title6 = Bheegti Aankhon Se
| extra6 = Sonu Nigam, Anuradha Paudwal
| length6 = 6:05
| title7 = Hare Hare Rama
| extra7 = Babul Supriyo, Nisha
| length7 = 7:27
| title8 = Dupatta Sarak Raha Hai
| extra8 = Udit Narayan, Alka Yagnik
| length8 = 7:04
| total_length = 53:29
}}

===Reception===
The soundtrack has been able to gain mainly positive reviews. Taran Adarsh stated that "Nikhil-Vinays music sounds good," with his personal favourite being the title track, "Kaun Hai Jo Sapno Mein Aaya".  Pankaj Shukla from SmasHits.com writes: "Tunes set by none other than Nikhil Vinay   sound melodious and sweet to ears." 

==References==
 

== External links ==
*  
*  

 
 
 
 
 
 
 