Murder on the Blackboard
{{Infobox film
| name           = Murder on the Blackboard
| image          = Murder on the Blackboard Danish poster.jpg
| image_size     = 225px
| caption        = Danish theatrical poster
| director       = George Archainbaud
| producer       = Pandro S. Berman (exec. producer)
| based on       = Murder on the Blackboard by Stuart Palmer 
| screenplay     = Willis Goldbeck
| starring       = Edna May Oliver James Gleason
| music          = Max Steiner
| cinematography = Nicholas Musuraca
| editing        = Archie Marshek
| distributor    = RKO Radio Pictures
| released       =  
| runtime        = 71 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

Murder on the Blackboard (1934) is a mystery film starring Edna May Oliver as schoolteacher Hildegarde Withers and James Gleason as Police Inspector Oscar Piper. Together, they investigate a murder at Withers school. It was based on the novel of the same name by Stuart Palmer. It features popular actor Bruce Cabot in one of his first post-King Kong roles, as well as Gertrude Michael, Regis Toomey and Edgar Buchanan. 

Murder on the Blackboard was the second of three films teaming Oliver and Gleason as Withers and Piper, following   

==Plot==
Miss Withers (Edna May Oliver) discovers the dead body of her colleague, music teacher Louise Halloran (Barbara Fritchie), in a schoolroom. She summons her old friend, Inspector Oscar Piper (James Gleason), but by the time he arrives, the corpse has disappeared. Having watched the only entrance (other than a fire exit with an alarm), Miss Withers knows the killer must still be inside. When the police search the building, Detective Donahue (Edgar Kennedy) is knocked out in the basement. Meanwhile, Miss Withers notices various clues, including a tune on the blackboard in Hallorans classroom. The body is found being burned in the basement furnace. Then, the fire alarm goes off; the murderer has escaped.

Oscar Schweitzer (Frederick Vogeding), the schools drunkard janitor, had some financial quarrel with Halloran. Piper arrests him, but Miss Withers does not believe he is the one they are after. She goes to the dead womans apartment, which she had shared with her friend and school secretary, Jane Davis (Gertrude Michael). There she discovers that Halloran held one of the tickets for the Irish Sweepstakes. A newspaper account reports it is for the favorite in the race and is already worth $50,000. If the horse were to win, the amount would be $300,000. Davis claims she had a half share in the ticket, giving her a motive for murder. Also, her teacher boyfriend Addison Stevens (Bruce Cabot) admits that Halloran was attracted to him. MacFarland (Tully Marshall), the womanizing head of the school, asks Withers to investigate the crime, but suspiciously suggests she leave town to check out Hallorans relatives. Snooping around, she finds a fragment of a burnt love letter from him to Halloran. 

Later, during another search of the basement, the light is turned off and someone throws a hatchet at Miss Withers head. After getting over her fright, she triumphantly points out to Piper that Schweitzer could not be the killer, as he is still in jail. Then, they see a newspaper report that he has escaped. It is discovered that the victim was already dying of "pernicious anemia of the bones". 

When Donahue comes to in the hospital, he cannot remember what happened, but Miss Withers has Piper tell the newspapers that Donahue knows the killers identity. When the murderer sneaks in to Donahues hospital room to poison his medicine, the trap is sprung. The criminal turns out to be Addison Stevens. (The tune on the blackboard spelled out the first few letters of his first name.)

Seeing no escape, Stevens drinks the poison himself, but reveals his motive before dying. He and Halloran were secretly married last summer. However, when his feelings changed, she would not give him up. He tried poisoning her slowly (causing the anemia), but she became suspicious, forcing him to act more decisively.  Later, when Miss Withers calls to console Davis, she is disillusioned when annoying Detective "Smiley" North (Regis Toomey) answers the telephone and reveals he is having breakfast with the pretty woman.

==Cast==
  
* Edna May Oliver as Hildegarde Withers
* James Gleason as Inspector Oscar Piper
* Bruce Cabot as Addison Stevens
* Gertrude Michael as Jane Davis
* Regis Toomey as Detective Smiley North
* Tully Marshall as MacFarland
* Frederick Vogeding as Otto Schweitzer
 
* Edgar Kennedy as Detective Donahue
* Jackie Searl as Leland Stanford Jones
* Barbara Fritchie as Louise Halloran
* Gustav von Seyffertitz as Dr. Max Von Immen
* Tom Herbert as Detective McTeague
* Jed Prouty as Dr. Levine
 

Cast notes: The Plot Thickens (1936) and Forty Naughty Girls (1937), it was played by Zasu Pitts.  In 1950, MGM adapted the novel Once Upon a Train into the film Mrs. OMalley and Mr. Malone, changing the name of the character to "Harriet Hattie OMalley", a Montana housewife, played by Marjorie Main.  "Hildegarde Wither" also appeared in a 1972 pilot for a TV series, A Very Missing Person, played by Eve Arden.  The characters final appearance was in the Neil Simon parody, Murder by Death, played by Estelle Winwood.  
*Gleason played the part of Inspector Piper in all six films. 

==References==
Notes
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 