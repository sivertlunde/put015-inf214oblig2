Jalisco canta en Sevilla
Jalisco Mexican film directed by Fernando de Fuentes, starring Jorge Negrete. This film represents the first Mexican-Spanish cinematic co-production. The story and musical numbers emphasize the cultural affinities between Mexico and Spain while warmly celebrating their differences.

== Plot ==
The story concerns a handsome charro from Jalisco, and his fat sidekick. The charro receives news that he has inherited a fortune from a distant relative in Spain, and so he must travel to Seville to collect it. A legal technicality impedes the speedy disbursement of his inheritance, so our the two heroes take jobs on a local ranch as farmhands. It turns out that the owner of the ranch was formerly a bullfighting|bullfighter, and has fond memories of Mexico. For this reason he befriends the charro. The charro goes on to win the heart of the ranch owners daughter, and manages to recover his inheritance with his father-in-laws help.

== References ==
* 

 
 
 
 


 