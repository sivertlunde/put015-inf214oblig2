Knockout (1935 film)
{{Infobox film
| name = Knockout
| image =
| image_size =
| caption =
| director = Carl Lamac   Hans H. Zerlett
| producer = Artur Hohenberg   Carl Lamac   Anny Ondra 
| writer =  Hans H. Zerlett
| narrator =
| starring = Anny Ondra   Max Schmeling   Hans Schönrath
| music = Leo Leux  
| cinematography = Otto Heller   Gustl A. Weiss   Ludwig Zahn  
| editing =  Ella Ensink   
| studio = Ondra-Lamac-Film   Bavaria Film
| distributor = Bavaria Film
| released =1 March 1935  
| runtime = 
| country =  Germany  German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Knockout is a 1935 German sports film directed by Carl Lamac and Hans H. Zerlett and starring Anny Ondra, Max Schmeling and Hans Schönrath.  After impressing a boxing trainer during a brawl over a woman, a young man is recruited and trained to be a boxer. He fights and defeats the British champion.
 Erich Zander.

==Cast==
* Anny Ondra as Marianne Plümke  
* Max Schmeling as Max Breuer  
* Hans Schönrath as Hawkins, Boxer  
* Edith Meinhard as Fräulein Melitta  
* Fritz Odemar as Der Theaterdirektor  
* Anni Markart as Camilla   Hans Richter as Josef, Hilfsbeleuchter  
* Otto Wernicke as P. F. Schmidtchen  
* Wilhelm Bendow as Der Assistant  
* Karl Platen as Der Inspizient  
* Hermann Meyer-Falkow as Der Trainer 
* Josef Eichheim as Der Mann, der seinen Hut aufißt 
* Paul Samson-Körner as Der Boxer Hütgen 
* Max Schreck as Leiter der Künstleragentur
* Isa Vermehren as Die Hamburger Sängerin
* Annie Ann 
* Lilian Bergo  Cornelius Booth 
* Beppo Brem 
* O.E. Hasse 
* Manfred Matwin
* Charlotte Radspieler 
* Rosi Rauch as Singer  
* Ernst Rotmund 
* Karl Schopp 
* Egon Stief

== References ==
 

== Bibliography ==
* Bock, Hans-Michael & Bergfelder, Tim. The Concise CineGraph. Encyclopedia of German Cinema. Berghahn Books, 2009.

== External links ==
*  

 
 
 
 
 
 
 
 


 