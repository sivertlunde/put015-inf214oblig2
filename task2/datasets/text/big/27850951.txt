Kill the Man
{{Infobox film
| name           = Kill the Man
| image          =  
| image size     = 
| alt            = 
| caption        =  
| director       = Tom Booker Jon Kean Andy Martin Marshall Persinger Joyce Schweickert
| writer         = Tom Booker Jon Kean
| narrator       = 
| starring       = Luke Wilson Joshua Malina Paula Devicq Paul Mills
| cinematography = Mark Mervis
| editing        = Sean Albertson
| studio         = 
| distributor    = 
| released       =   (Sundance Film Festival)
| runtime        = 88 minutes
| country        = United States English
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}
 American comedy large chain of copy-shops.

== Cast ==
* Luke Wilson - Stanley Simon
* Joshua Malina - Bob Stein
* Paula Devicq - Vicki Livingston
* Phillip Rhys - Seth
* Phil LaMarr - Marky Marx
* Jim Fyfe - Guy
* Teri Garr - Mrs. Livingston
* Michael McKean - Mr. Livingston
* Lisa Robin Kelly - Nan
* Brian Doyle-Murray - Grumpy Senior

== External links ==
*  

 
 

 