Love in the Afternoon (1957 film)
{{Infobox film
| name           = Love in the Afternoon
| image          = Love in the afternoon (1957) - movie poster.jpg
| caption        = American release poster by Saul Bass
| director       = Billy Wilder
| producer       = Billy Wilder
| screenplay     = {{Plain list|
* Billy Wilder
* I.A.L. Diamond
}}
| based on       =   
| starring       = {{Plain list|
* Gary Cooper
* Audrey Hepburn
* Maurice Chevalier
}}
| music          = Franz Waxman  William Mellor 
| editing        = Leonide Azar 
| studio         = Allied Artists Pictures Corporation Associated British (UK) Walter Mirisch|Mirisch, Walter (2008). "I Thought We Were Making Movies, Not History" (pp. 81-83). University of Wisconsin Press, Madison, Wisconsin. ISBN 0-299-22640-9.  United Artists (international) 
| released       =  
| runtime        = 130 minutes
| country        = United States
| language       = English
| budget         = $2.1 million 
| gross          = $2 million (US) 
}}
 Claude Anet novel Ariane, jeune fille russe (novel)|Ariane, jeune fille russe (trans., Ariane, Young Russian Girl), which previously was filmed as Scampolo in 1928 and Scampolo, ein Kind der Strasse (trans., Scampolo, a Child of the Street) in 1932, the latter with a script co-written by Wilder. Wilder was inspired by a 1931 German adaptation of the novel Ariane (film)|Ariane directed by Paul Czinner. 

==Plot==
Young  cello student Ariane Chavasse (Audrey Hepburn) eavesdrops on a conversation between her father, widowed private detective Claude Chavasse (Maurice Chevalier), and his client, "Monsieur X" (John McGiver). After learning of his wifes daily trysts with American business magnate Frank Flannagan (Gary Cooper), Monsieur X announces he will shoot Flannagan later that day. Claude is nonchalant, regretting only the business he will lose (Flannagan is a well-known international playboy with a long history of numerous casual affairs). When Ariane cannot get the police to intervene (until after a crime has been committed), she decides to warn him herself.

Ariane is in time. When Monsieur X breaks into Flannagans hotel suite, he finds Flannagan with Ariane, not his wife (she is cautiously making her escape via an outside ledge). Flannagan is intrigued by the mysterious girl, who refuses to give him any information about herself, even her name. He resorts to calling her "thin girl". She has no romantic history but pretends to be a femme fatale to interest him, and soon falls in love with the considerably older man. She agrees to meet him the next afternoon, because her orchestral practice is in the evenings (although she does not admit that is the reason). She comes with mixed feelings, but ends up becoming his lover for the evening until his plane leaves.

Her father, who has tried unsuccessfully to protect her from knowing about the tawdry domestic-surveillance details in his files, notices her change of mood but has no idea that it proceeds from one of his cases.

After a year, Flannagan returns to Paris. The two meet again when she sees him at an opera while surveying the crowd from a balcony and puts herself in his path in the lobby, and they start seeing each other again. This time, when he persists in his questioning, she makes up a long list of prior imaginary lovers based on her fathers files (Flannagan is number 20 on the list). Flannagan gradually goes from being amused to being tormented by the possible comparisons, but is unsure whether they are real. When he encounters a still-apologetic Monsieur X, the latter recommends Claude Chavasse to him, and thus Flannagan hires Arianes own father to investigate. 

It does not take Chavasse long to realize the mystery woman is Ariane. He informs his client that his daughter fabricated her love life. He tells Flannagan that she is a little fish that he should throw back, since she is serious and he wants to avoid serious relationships.

Flannagan decides to leave Paris, pretending to be on his way to meet former lovers. At the station, as Ariane runs along the platform beside his departing train, with her femme-fatale facade cracking as her love shows through, Flannagan changes his mind and sweeps her up in his arms onto the train. Chavasse reports that they got married and now live in New York.

==Cast==
 
 
 
 
* Gary Cooper as Frank Flannagan
* Audrey Hepburn as Ariane Chavasse
* Maurice Chevalier as Claude Chavasse
 
* John McGiver as Monsieur X
* Van Doude as Michel
* Lise Bourdin as Madame X
* Olga Valery as Hotel guest with dog
 
* The Gypsies as Themselves
 

==Production==
Love in the Afternoon was the first of twelve screenplays by Billy Wilder and I. A. L. Diamond, who met when Wilder contacted Diamond after reading an article he had written for the Screen Writers Guild monthly magazine. The two men immediately hit it off, and Wilder suggested they collaborate on a project based on a German language film he had co-written in the early 1930s. Chandler, Charlotte, Nobodys Perfect: Billy Wilder, A Personal Biography. New York: Simon & Schuster 2002. ISBN 0-7432-1709-8, pp. 189-194 

 
Wilders first choices for Frank Flannagan were Cary Grant and Yul Brynner.  "It was a disappointment to me that   never said yes to any picture I offered him," Wilder later recalled. "He didnt explain why. He had very strong ideas about what parts he wanted." The director decided to cast Gary Cooper because they shared similar tastes and interests and Wilder knew the actor would be good company during location filming in Paris. "They talked about food and wine and clothes and art," according to co-star Audrey Hepburn, Wilders only choice for Ariane. Talent agent Paul Kohner suggested Maurice Chevalier for the role of Claude Chavasse, and when asked if he was interested, the actor replied, "I would give the secret recipe for my grandmothers bouillabaisse to be in a Billy Wilder picture." 

Filming locations included the Château of Vitry in the Yvelines, the Palais Garnier (Paris Opera), and the Hôtel Ritz Paris. It was Wilders insistence to shoot the film on location in Paris. 

Music plays an important role in the film. Much of the prelude to the Richard Wagner opera Tristan und Isolde is heard during a lengthy sequence set in the opera house, and Gypsy style melodies underscore Flannagans various seductions. Matty Malneck, Wilders friend from their Paul Whiteman days in Vienna, wrote three songs for the film, including the title tune. Also heard are "Cest si bon," "Lame Des Poètes" by Charles Trenet, and "Fascination (1932 song)|Fascination," which is hummed repeatedly by Ariane. 

For the American release of the film, Maurice Chevalier recorded an end-of-film narration letting audiences know Ariane and Flannagan are married and living in New York City. Although Wilder objected to the addition, he was forced to include it to forestall complaints that the relationship between the two was immoral. 
 Allied Artists Friendly Persuasion prompted the studio to sell the distribution rights of Love in the Afternoon for Europe to gain more financing.  The film was a commercial failure in the United States, but it was a major success in Europe, where it was released under the title Ariane. 

==Critical reception== Lubitsch tradition" and added, "Like most of Lubitschs Masterpiece|chefs-doeuvre, it is a gossamer sort of thing, so far as a literary story and a substantial moral are concerned ... Mr. Wilder employs a distinctive style of subtle sophisticated slapstick to give the fizz to his brand of champagne ... Both the performers are up to it—archly, cryptically, beautifully. They are even up to a sentimental ending that is full of the mellowness of afternoon." 

Wilder is often mentioned as a "disciple" of Lubitsch. In his 2007 essay on the two directors for Stop Smiling magazine, Jonathan Rosenbaum wrote that Love in the Afternoon was "the most obvious and explicit and also, arguably, the clunkiest of his tributes to Lubitsch, partially inspired by Lubitschs 1938 Bluebeards Eighth Wife (which Wilder and Brackett also helped to script, and which also starred Gary Cooper, again playing a womanizing American millionaire in France)".  John Fawell wrote in 2008 that "Lubitsch was at his most imaginative when he lingered outside of doorways, particularly when something promiscuous was going on behind the door, a habit his pupil Billy Wilder picked up. In Wilders most Lubitsch-like film, Love in the Afternoon, we know when Gary Coopers rich playboy has bedded another conquest when we see the group of gypsy musicians (that travels with Cooper to aid in his wooing) tiptoe out of the hotel room, shoes in hand." 

In an undated and unsigned review, TV Guide notes that the film has "the winsome charm of Hepburn, the elfin puckishness of Chevalier, a literate script by Wilder and Diamond, and an airy feeling that wafted the audience along," but felt it was let down by Gary Cooper, who "was pushing 56 at the time and looking too long in the tooth to be playing opposite the gamine Hepburn ... With little competition from the wooden Cooper, the picture is stolen by Chevaliers bravura turn." 

Channel 4 thought "the film as a whole is rather let down by the implausible chemistry that is meant to develop between Cooper and Hepburn." 

==References==
 

==Bibliography==
* Gene D. Phillips|Phillips, Gene D. Some like it Wilder: the life and controversial films of Billy Wilder. University Press of Kentucky, 2010.

==External links==
 
*  
*  
*  

 
 

 
 
 

 

 
 
 
 

 
 
 
 
 