Hemingway's Adventures of a Young Man
 
{{Infobox film
| name           = Hemingways Adventures of a Young Man
| image      	 = Hemingways Adventures of a Young Man FilmPoster.jpeg
| image_size     = 
| alt            = 
| caption        = Theatrical release poster
| director       = Martin Ritt
| producer       = Jerry Wald
| screenplay     = A.E. Hotchner
| based on       = The Nick Adams Stories
| starring       = Richard Beymer
| music          = Franz Waxman
| cinematography = Lee Garmes
| editing        = Hugh S. Fowler Twentieth Century-Fox Film Corporation
| released       =  
| runtime        = 145 minutes
| country        = United States
| language       = English
| budget = $4.1 million 
}} Nick Adams, Arthur Kennedy, and Paul Newman. The 145 minute-long film was released in July 1962. 

==Plot==
Nick Adams is a young, restless man who wants a good life and to see the world. Though he is told it is not worth the attempt, he decides to go away from his midwestern home. Along the way, he encounters numerous people and later joins the Italian army to fight the Germans in World War I, where he falls in love.

==Cast==
* Richard Beymer as Nick Adams
* Diane Baker as Carolyn
* Corinne Calvet as Contessa
* Fred Clark as Mr. Turner
* Dan Dailey as Billy Campbell James Dunn as Telegrapher
* Juano Hernández as Bugs Arthur Kennedy as Dr. Adams
* Ricardo Montalbán as Major Padula
* Paul Newman as The Battler
* Susan Strasberg as Rosanna
* Jessica Tandy as Mrs. Adams
* Eli Wallach as John
* Edward Binns as Brakeman
* Philip Bourneuf as City Editor
* Tullio Carminati as Rosannas Father Marc Cavell as Eddy Boulton Charles Fredericks as Mayor
* Simon Oakland as Joe Boulton
* Michael J. Pollard as George
* Whit Bissell as Ludstrum
* Lillian Adams as Indian Woman
* Walter Baldwin as Conductor
* Laura Cornell as Headwaiter
* Laura Cornell as Burlesque Queen
* Miriam Golden as Indian Mid-Wife
* Pitt Herbert as Bartender
* Pat Hogan as Billy Tabeshaw
* Baruch Lumet as Morris
* Burt Mustin as Old Soldier
* Sherry Staiger as Burlesque Queen
* Sharon Tate as Burlesque Queen
* Alfredo Varelli as Father Ben
* Mel Welles as Italian Sergeant

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 