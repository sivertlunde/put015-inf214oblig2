Letters from a Killer
{{Infobox film
| name           = Letters from a Killer
| image          =
| caption        = David Carson
| writer         = John Foster, Screenwriter-in-Residence at Bournemouth Universities media school 
Nicholas Hicks-Beach Shelley Miller
| starring       =Patrick Swayze Kim Myers Tina Lifford Gia Carides Elizabeth Ruscio Olivia Birkelund
| producer       = Michael Jackman Bertil Ohlsson Peter Snell
| music          = Dennis McCarthy
| cinematography = John A. Alonzo
| editing        = Lance Luckey
| distributor    = Sterling Home Entertainment
| released       = August 21, 1998
| runtime        = 104 min
| country        =   /   English
| budget         =
| preceded_by    =
| followed_by    =
}}

Letters from a Killer is a 1998 film about a man who is falsely convicted of the murder of his wife. During his time in jail, he finds comfort from four women with whom he corresponds. After his second court appearance, he is finally freed from prison only to be framed for yet two more murders which he did not commit.

It stars Patrick Swayze as Race Darnell, a man who was convicted and framed for murdering his wife. The movie also features Gia Carides, Kim Myers, Olivia Birkelund, Tina Lifford, David Carson and writing by John Foster, Nicholas Hicks-Beach, and Shelley Miller.

==Filming locations==
* Echo, Utah
* Fair Oaks, California
* Ione, California
* Jordanelle, Utah
* Los Angeles, California
* New Orleans, Louisiana
* Reno, Nevada
* Sacramento, California
* Salt Lake City, Utah
* Wendover, Nevada
* Woodland, California

==Halting==

Letters from a Killer was originally supposed to be finished earlier than its release but filming was halted for two months due to Patrick Swayze suffering serious injuries in the lone area while he fell off his horse and hit a tree. He ended up with both legs broken and four tendons in his shoulder immediately became detached. Although it was released later, Swayze was reported to have trouble resuming his career until later.

==External links==
*  
*  

 

 
 
 
 
 


 