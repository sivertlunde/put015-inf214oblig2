Hold That Lion!
 
{{Infobox Film |
  | name           = Hold That Lion!
  | image          = Holdthatlion47stooge.jpg
  | caption        = 
  | director       = Jules White Felix Adler Kenneth MacDonald Emil Sitka Dudley Dickerson Heinie Conklin Victor Travers Blackie Whiteford
  | cinematography = George F. Kelley   Edwin Bryant 
  | producer       = Jules White 
  | distributor    = Columbia Pictures 
  | released       =  
  | runtime        = 16 27"
  | country        = United States
  | language       = English
}}

Hold That Lion! is the 100th short subject starring American slapstick comedy team The Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959.

==Plot== Kenneth MacDonald).  They try to claim the inheritance at the office of Cess, Poole, and Drayne.  Their attorney, played by Emil Sitka, explains that he doesnt have their money and they have to find Slipp.  While in the office, the trio try to help find some files and tangle with a very stubborn filing cabinet.  One by one the Stooges confront Slipp in his office. He in turn accuses first Larry Fine|Larry, then Moe Howard|Moe, then Shemp Howard|Shemp, of being that crook Slipp, and successfully flees his office with the money.

The Stooges follow Slipp on board a train. To avoid a conductor after them for tickets they hide out in a large crate in the baggage car. A lion is also in the crate, and the Stooges run, hiding in a sleeping berth.  Larry convinces the others that they should rest and confront Slipp the next day.  Moe sticks his bare foot out through the curtain and the lion licks it, causing Moe to laugh. The lion laughs himself after it takes takes two swipes on Moes sole to start with. Moe wakes up and punishes Shemp, thinking he was the culprit.  After they go back to sleep, the lion climbs up into the berth.  

The lion lays on all their feet, waking them up.  After bickering with each other, they realize the lion is in their booth.  The Stooges escape, pulling down all the curtains to the berths and waking everyone up.

As they make their getaway in the confusion, the Stooges spot Slipp and take off after him. They chase him to the baggage car and finally defeat him, reclaiming their inheritance.

==Production notes==
A new version of the "Three Blind Mice" theme music starts in this short. It will be used till Mummys Dummies.  
 football term, "Hold that line!"  

Icabod Slipps name appears on the door as "I. Slipp." This is a semantic parody on the Long Island town of Islip (town), New York|Islip, New York. 

Shemp Howard (a man of many phobias) was reportedly so frightened of the lion that he insisted a glass plate be placed in between him and the animal while filming the scene in the crate, and the Stooges reflection in the glass can be seen as they are hastily exiting the crate. Apparently, though, Columbia Pictures hired a lion who was getting on in years. Emil Sitka later commented that the feline was "so sickly, he would fall asleep in the middle of a take."   

==Curly Howard returns==
  returns in a cameo]]
Hold That Lion! is notable for a cameo appearance by former Stooge Curly Howard|Curly, younger brother of Shemp and Moe. He appears as a snoring passenger who the Stooges think is Icabod Slipp, the man they are looking for. This was the only film that featured all three Howard brothers — Moe, Curly, and Shemp — in the same film and all members of The Three Stooges including Larry Fine|Larry. This also marks the first time Curly is shown on camera with a full head of hair, and his only film appearance following the stroke that ended his career as a full-time Stooge.

Director Jules White remembers:
  "It was a spur of the moment idea. Curly was visiting the set; this was some time after his stroke. Apparently he came in on his own since I didnt see a nurse with him. He was sitting around, reading a newspaper. As I walked in, the newspaper, which he had in front of his face, came down and he waved hello to me. I thought it would be funny to have him do a bit in the picture, and he was happy to do it."  

==Recycling template==
Hold That Lion! would be the template for recycled films post-1953. Three films in a row utilized footage from this short:
*First, Booty and the Beast recycled the second half of Hold That Lion!.
*Next, Loose Loot recycled the first half.
*Finally, Tricky Dicks recycled the only segment from Hold That Lion! not previously used: the filing cabinet sequence.
Due to this successful practice, director Jules White would begin recycling entire shorts as a cost-saving tactic for the remainder of the Stooges tenure in Columbia Pictures shorts department. 
 )]]

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 