Duchess of Idaho
{{Infobox film
| name           = Duchess of Idaho
| image          = Duchess of idaho-poster.jpg
| caption        = Theatrical release poster
| director       = Robert Z. Leonard
| producer       = Jack Cummings
| writer         = Dorothy Cooper Jerry Davis Sid Fields
| starring       = Van Johnson Esther Williams
| music          = Albert Sendrey George Stoll
| cinematography = Charles Edgar Schoenbaum
| editing        = Adrienne Fazan  	
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 98 minutes
| country        = United States
| language       = English
| budget         = $1,705,000  . 
| gross          = $4,236,000 
}} musical romantic comedy produced in 1950 by Metro-Goldwyn-Mayer. Directed by Robert Z. Leonard, it was the fourth film pairing  Esther Williams and Van Johnson. It was shot mainly on location in Sun Valley, Idaho.   

==Plot== matchmaker for John Lund), the man Ellen loves, all the way to Idaho.  There, Christine decides to play a joke on Douglas.  After boarding his train to Sun Valley, Christine wins the mans affections and then shocks him with hints that she expects a commitment. Once shes in Sun Valley, however, things become problematic when Christine falls in love with hotel bandleader Dick Layne (Van Johnson).  During her time in Sun Valley, Christine wins the title of "Duchess of Idaho" in a dance contest.

==Cast==
*Esther Williams as Christine Riverton Duncan
*Van Johnson as Dick Layne John Lund as Douglas J. Morrison Jr.
*Paula Raymond as Ellen Hallit
*Mel Tormé as Cyril, the Bellhop
*Lena Horne as Herself - Cameo appearance
*Eleanor Powell as Herself - Cameo appearance
*Clinton Sundberg as Matson 
*Connie Haines as Peggy Elliot 
*Amanda Blake as Linda Kinston 
*Tommy Farrell as Chuck 
*Sig Arno as M. Le Blanche 
*Dick Simmons as Alec J. Collins 
*Red Skelton as Guest Master of Ceremonies
*The Jubalaires in a cameo appearance   

==Production==
In her autobiography, Million Dollar Mermaid, Williams called this a "re-hash of the Esther Williams formula: the mismatched lovers plot. It was enough to give one a case of cinematic deja vu."   

This marked Eleanor Powells first film appearance in six years as well as her last film appearance. Williams reported that Powell had practiced her cameos routine until her feet bled, claiming that it had to be perfect. 

Lena Hornes contract with MGM also ended with this film, though she would make several more MGM musical appearances later in the decade.

==Soundtrack==
*Lets Choo Choo Choo to Idaho; Written by Al Rinker and Floyd Huddleston; Sung by Van Johnson, Connie Haines and The Jubilaires
*You Cant Do Wrong Doin Right; Written by Al Rinker and Floyd Huddleston; Sung by Van Johnson and Connie Haines
*Of All Things; Written by Al Rinker and Floyd Huddleston; Sung by Connie Haines
*Baby Come Out of the Clouds; Written by Henry Nemo and Lee Pearl; Sung by Lena Horne

==Deleted performances==
Several musical numbers filmed for the movie were cut from the theatrical release. The deleted songs were: 
*"Warm Hands, Cold Heart," sung by Mel Tormé
*"You Wont Forget Me," sung by Lena Horne
*"You Do Something to Me," the Cole Porter standard, also sung by Lena Horne
These performances would later surface on a special DVD packaged in a 2004 box set of the Thats Entertainment! films.

==Box Office==
According to MGM records the film earned $2,851,000 domestically and $1,385,000 foreign, making the studio a profit of $921,000. 

==Critical reception==
Cue magazine found the film to be "a big, beautiful bore. The comedy is rapid and the pace is sleepy." 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 