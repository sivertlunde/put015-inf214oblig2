Herman U. S. A.
{{Infobox film
| name           = Herman U.S.A.
| image          = 
| caption        = 
| director       = Bill Semans
| producer       = Jeffrey Hohman Bill Semans Patrick Wells
| writer         = Bill Semans
| starring       = 
| music          = Stephen Graziano
| cinematography = Ross Berryman
| editing        = Daniel J. Geiger Michael N. Knue
| distributor    = Two Silks Releasing
| released       =  
| runtime        = 89 minutes
| country        = United States
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
Herman U.S.A. is a 2001 American romantic comedy directed by Bill Semans.

==Synopsis==
 
In a small Minnesota town, seventy-eight bachelor farmers advertise for companionship, leading to a response far outstripping expectations.

==Cast==
*Michael OKeefe as Dennis
*Kevin Chamberlin as Wayne
*Enid Graham as Dorrie Ann Hamilton as Sharon
*Garth Schumacher as Walter
*Wally Dunn as Sigurd
*Anthony Mockus Sr. as Arvid
*Christina Rouner as June
*Marjie Rynearson as Lillian
*Kim Sykes as Kim
*Richard Wharton as Vern
*Mark Benninghoffen as Rick
*Suzanne Warmanen as Nancy
*Tom Price as Harry
*Brian Baumgartner as Roger
*Michael D. Bang as Michael OKeefes (Dennis) station wagon driver; long-haired country-boy

==Background==
 New Germany. The crew painted over the water tower to read "Herman U. S. A." The town of Herman, Minnesota|Herman, Minnesota did not have enough hotel space to fit the entire cast and crew members.

==External links==
*  
* 

 
 
 
 
 
 


 