The Man Who Played God (1932 film)
{{Infobox Film
| name           = The Man Who Played God
| image          = The Man Who Played God.jpg
| caption        = Theatrical release poster
| director       = John G. Adolfi
| producer       = Darryl F. Zanuck
| writer         = Julien Josephson Maude T. Howell Based on a play by Jules Eckert Goodman
| starring       = George Arliss Bette Davis Louise Closser Hale
| music          = Leo F. Forbstein
| cinematography = James Van Trees William Holmes
| distributor    = Warner Bros.
| released       = February 20, 1932
| runtime        = 80 minutes
| country        = United States
| language       = English
}} The Silent Gouverneur Morris.

Goodmans play previously had been filmed under its original title in 1915 and in 1922 as The Man Who Played God, which also starred George Arliss. It was adapted for the screen yet again as Sincerely Yours in 1955.

==Plot== concert pianist Montgomery Royale is deafened when a bomb is detonated in an attempt to assassinate the foreign ruler. With his career over as a result of his injury, Royale returns to New York City with his sister Florence, close friend Mildred Miller, and considerably younger fiancée Grace Blair. 
 lip read, and he spends his days observing people in Central Park from his apartment window. As he learns of peoples problems, he tries to help them anonymously. He becomes absorbed in his game of "playing God" but his actions are without sincerity.

One day Montgomery witnesses a conversation between Grace and Harold Van Adam, during which she tells the young man she loves him but cannot leave Montgomery because of his handicap. Moved by the generosity of her sacrifice, Montgomery confronts her and ends their engagement, allowing her to follow her heart. 

Montgomery continues to act as a Philanthropy|philanthropist, but his attitude is changed and his motives become altruistic. He draws closer to Mildred, who always has loved him, and the two find happiness in their developing relationship.

==Production== The Menace, and yet  Murray Kinnell of The Menace cast had suggested me for the part . . . Out of all bad comes some good. I have always believed this." Stine, Whitney, and Davis, Bette, Mother Goddam: The Story of the Career of Bette Davis. New York: Hawthorn Books 1974. ISBN 0-8015-5184-6, pp. 18-32  

A decade earlier, Arliss had portrayed Montgomery Royale in the silent film version of The Man Who Played God. Now, at the age of sixty-four, he knew he was too old for the role and was concerned the age difference between him and the actress cast as Grace Blair would be ridiculous if she werent played by someone who could convey both love and hero worship for his character. After interviewing many young women, he felt Davis was the one most capable of handling the part. He sent her to studio makeup artist Perc Westmore, who suggested bleached blonde hair would heighten her screen appearance. "He was right. In The Man Who Played God - for the first time - I really looked like myself. It was for me a new lease on life." The two became close friends, and Westmore went on to make up Davis in more than two dozen films. 

After seeing a rough cut of the film, Jack L. Warner signed Davis to a five-year contract, starting at $400 per week. She would remain with Warner Bros. for the next eighteen years, and Davis was beholden to Arliss for the rest of her life, crediting him for "the career that finally emerged."  Of Davis, Arliss wrote in his 1940 biography, My Ten Years in the Studios, "I did not expect anything except a nice little performance. But when we rehearsed, she startled me; the nice little part became a deep and vivid creation, and I felt rather humbled that this young girl had been able to discover and portray something that my imagination had failed to conceive . . . I am not surprised that Bette Davis is now the most important star on the screen."  
 Moonlight Sonata by Ludwig van Beethoven, and Onward, Christian Soldiers by Arthur Sullivan.

==Cast (in credits order)==
  and Ann Forrest, page 65 of the December 1922 photoplay.]]
*George Arliss as Montgomery Royale
*Violet Heming as Mildred Miller
*Bette Davis as Grace Blair
*Andre Luguet as The King
*Louise Closser Hale as Florence Royale Donald Cook as Harold Van Adam
*Ivan F. Simpson as Battle
*Oscar Apfel as The Lip Reader
*Charles E. Evans as The Doctor
*Hedda Hopper as Mrs Alice Chittendon
*William Janney as First Boy
*Murray Kinnell as Kings Aide

==Critical reception==
Mordaunt Hall of the New York Times observed, "It is a neatly conceived story as it comes to the screen, with effervescent cheer in the introductory sequences, then a period of melancholy, and finally episodes of thankfulness and happiness . . . and while it seems a little lethargic at times, it has such a genuinely gentle and appealing touch that one would not wish it to be told any faster." He thought "Mr. Arliss delivers another of his effective and meticulous portrayals" but felt Bette Davis "often speaks too rapidly."  Davis agreed. "It was always difficult for me to speak slowly on or off the screen . . . William Wyler, when he directed me in Jezebel (film)|Jezebel, was constantly making me slow down." 

==References==
 

==External links==
*  


 
 
 
 
 
 
 
 
 
 
 