Fire Monsters Against the Son of Hercules
{{Infobox film
| name           = Fire Monsters Against the Son of Hercules
| image_size     =
| image	=	Fire Monsters Against the Son of Hercules FilmPoster.jpeg
| caption        =
| director       = Guido Malatesta
| producer       = Giorgio Marzelli (producer) Alfio Quattrini (producer)
| writer         = Arpad DeRiso (writer) Guido Malatesta (writer)
| narrator       =
| starring       = See below
| music          = Guido Robuschi Gian Stellari
| cinematography = Giuseppe La Torre
| editing        = Enzo Alfonzi
| distributor    =
| released       = 1962
| runtime        = 82 minutes (Italy)   82 minutes (USA)
| country        = Italy
| language       = Italian
| budget         =
| gross          =
}} Italian film directed by Guido Malatesta that was filmed in Yugoslavia and Italy. In the American version Maciste is called "Maxus". 

The film was originally known as Colossus of the Stone Age in the UK, and was re-released in 1975 as Land of the Monsters.

== Plot summary ==
Set in the Ice Age, a nomadic tribe of sun worshippers reach an area they decide to settle in. When Idar and Rhia, a young couple of the tribe come across a water monster, the monster is killed by Maciste with a spear. The two ask Maciste to join their tribe but Maciste explains he has a destiny to fulfill of righting wrongs and must leave.

Soon afterwards a cave dwelling tribe of moon worshippers led by Fuwan attack the village and carry off the sun worshippers women. Maciste returns to the sun worshippers and enter the moon worshippers underground city through a river.  There he discovers Moah, whose father and brother who had formerly led the tribe were murdered by Fuwan and wishes to take Moah. Moah explains that the moon worshippers will sacrifice the captured women in a celebration of the full moon that night.

Maciste passes through an underwater tunnel, defeats the three headed hydra that lives there and is able to eliminate the guards to the entrance and remove the stone gate to the entrance letting the vengeful sun worshippers in. The women are freed but Maciste destroys their path to the surface once they have left and is captured by the moon worshippers who bury him, and later Moah in the earth so the worms may eat their bodies.  They are saved by a volcanic eruption that sets them free and kills a large portion of the moon worshippers.

Seeking revenge, the moon worshippers make a pact with a tribe of cannibals to attack the sun worshippers.  The combined forces are defeated by the sun worshippers and Maciste.  Moah joins Maciste on his future journeys.

== Cast ==
*Reg Lewis as Maciste|Maciste/ Maxus Margaret Lee as Moah
*Luciano Marin as Idar son of Dorak
*Andrea Aureli as Rhia
*Birgit Bergen as Agmir
*Nello Pazzafini as Fuwan
*Miria Kent as Woman of the Sun
*Fulvia Gasser as Woman of the Sun
*Rocco Spataro as Dorak
*Ivan Pengow as Gamel the Fire Keeper

==Biography==
* 

==References==
 

== External links ==
* 
* 

 
 
 
 
 
 
 
 
 
 


 
 