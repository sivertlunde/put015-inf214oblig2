Greenwich Village (film)
{{Infobox film
| name           = Greenwich Village
| image          = Poster - Greenwich Village (1944).jpg
| image_size     = 200px
| caption        = Carmen Miranda in Greenwich Village
| director       = Walter Lang
| writer         = 
| narrator       = 
| starring       = Carmen Miranda
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = September 27, 1944	
| runtime        = 82 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}

Greenwich Village is a 1944 film from Twentieth Century Fox directed by Walter Lang. It stars Carmen Miranda and Don Ameche. 

==Plot== 
In 1922, aspiring composer Kenneth Harvey (Don Ameche) travels from the Midwest to Greenwich Village, New York, where he hopes to interest famed composer Kavosky in his concerto. Kenneth wanders into a speakeasy owned by the brash Danny OHare (William Bendix), who wants to put on a musical extravaganza showcasing his singing sweetheart, Bonnie Watson (Vivian Blaine). Danny hopes that the show will make Bonnie a star and make up for the fact that he cost her an opportunity of playing a leading role for Ziegfeld. Dannys other main entertainer, Princess Querida (Carmen Miranda), mistakenly assumes that Kenneth is rich, although the few hundred-dollar bills he innocently flashes are the extent of his traveling money. Danny immediately targets Kenneth as a chump and begins to get friendly with him, but Bonnie disapproves and allows Kenneth to escort her home. At her apartment, Bonnie confesses that when she came to Greenwich Village, she had aspirations to become a poet, and advises Kenneth to be more careful about displaying his money. Danny, jealous of Kenneth and Bonnies obvious attraction to each other, brings the gang up to Bonnies apartment for a party, and Kenneth plays some of his concerto for them. The next morning, Danny arranges for Kenneth to move to the top floor apartment and begin writing songs for their show, although Bonnie stipulates that music from Kenneths concerto must be withdrawn from the show if Kavosky likes it. Meanwhile, Hofer, a former violinist with Kavoskys orchestra, persuades the maestro to hear Kenneth play, which Kavosky reluctantly does to get rid of Hofer. Hofer then lies to Kenneth, telling him that Kavosky wants to perform his concerto at Carnegie Hall, and that they should begin the orchestrations immediately. Kenneth works hard on his music, which he withdraws from Dannys show, even though Bonnie has already written the lyrics. Danny is infuriated, especially when he sees Bonnie and Kenneth kiss, but Bonnie is thrilled by Kenneths seeming good fortune. Unknown to Bonnie, Danny, who continues to rehearse the numbers using Kenneths music, is aware of the situation when Hofer swindles Danny out of his life savings, which Hofer (Felix Bressart) says is the down payment on the musicians wages for the Carnegie Hall performance. Hofer disappears with the money, and Kenneth discovers his treachery after speaking to the surprised Kavosky. The heartbroken Kenneth is on his way home when he sees Hofer returning the money to Danny, who has realized that Bonnie is truly in love with Kenneth. The young composer misunderstands the situation and assumes that Danny and Bonnie were in on the swindle. While Kenneth is angrily packing, Querida questions him and learns of his misapprehension. She then gets him arrested by giving him some bootleg liquor to carry, and while Kenneth languishes in jail, Danny, Bonnie and the others step up their rehearsals and prepare to open the show. On opening night, Dannys right-hand man, Brophy, bails Kenneth out of jail, and the irate composer rushes over to the theater to confront Danny. As he watches from the audience, Kenneth is amazed to see Kavosky conduct his concerto, which has been turned into an elaborate number featuring Querida and Bonnie. Kenneth rushes backstage, where Danny reveals that Kavosky volunteered his services after learning of the swindle perpetrated by Hofer. Danny also advises Kenneth to make up with Bonnie, and after her final number, Kenneth embraces her in the wings. 

===Cast===
*Carmen Miranda as Princess Quereda OToole
*Don Ameche as Kenneth Harvey
*William Bendix as Danny OMara
*Vivian Blaine as Bonnie Watson
*Felix Bressart as Hofer

===Production===
  Phil Regan Phil Baker and Perry Como (who was to make his debut in the picture). In Jul 1943, HR also reported that Lillian Porter had been cast in the film, but her appearance in the completed picture has not been confirmed. Although the onscreen credits introduce actress Vivian Blaine "in Her First Featured Role," Blaine had appeared in several previous productions for Twentieth Century-Fox, including a starring role in the 1943 film Jitterbugs. According to a 2 Nov 1943 HR item, the studio placed Blaine into Greenwich Village after showing two theater audiences a test reel of Technicolor footage of Blaine, Gale Robbins, Faye Marlowe, Lois Andrews and Doris Merrick, then asking the audiences to choose their favorite.
        On the Town. Actor Felix Bressart was borrowed from MGM for the production.
        O Que e que a baiana tem?" by Dorival Caymmi and "Quando eu penso na baia" by Ary Barroso. 

==Critical reception==
  in a publicity photo for the movie]]
"Technicolor is the pictures chief asset," said The New York Times of Greenwich Village (1944), a Fox musical from a decidedly lesser tier than the studios great "A" productions, but still worth a look for the presence of Carmen Miranda and, yes, Leon Shamroys Technicolor cinematography. 

The Times review complained that the cast was hardly formidable enough to sustain a movie with such thin plot and characters, and while that may be true, its still interesting to see how Fox tried to launch Vivian Blaine as a big new star, and its unique (to say the least!) to see William Bendix sing and dance.

Bendix had recently become a star supporting player and had already perfected a screen persona of a lovable doofus. Time magazines review of Greenwich Village marveled at how the persona was just that; in real life, apparently, Bendix was cultivated and mannered. "Bendix is probably the worlds highest-paid professional ignoramus," said Time. "As such he now rates star billing at his studio and makes more money than the President of the U.S."

Vivian Blaine was touted as a newcomer by the Fox publicity machine, but in truth she had already been credited in four previous films, including Jitterbugs (1943), in which she played a significant role opposite Laurel and Hardy. She went on to do more movies and television but she never became a movie star. She will instead best be remembered for her stage work, especially as the original "Miss Adelaide" in the stage and screen versions of Guys and Dolls (1950 stage, 1955 film).
 The Gangs All Here (1943). Film historian Jeanine Basinger has written astutely that the Brazilian bombshell "wasnt a real movie star, but someone who did star turns in movies...She was an important escape fantasy of World War II." (from The Star Machine, Knopf) Known for her insanely over-the-top costumes, elaborate, fruit-laden headpieces, and hugely energetic singing and dancing, everything about Carmen Miranda was always exaggerated to a delightful level. 

== DVD release ==
The film was released on DVD in June 2008 as part of Foxs "The Carmen Miranda Collection." 

==References==
 

==External links==
* 
* 

 

 
 
 
 
 