Coimbatore Mappillai
{{Infobox film
| name = Coimbatore Mappillai
| image =
| director =  C. Ranganthan |
| producer = M. S. V. Murali|
| writer = C. Ranganthan | Vijay Sanghavi Karan Goundamani Nirmalamma| Vidyasagar |
| cinematography = R. Raja Ratnam
| editing = C. Cedrick |
| studio = Shree Vijayalakshmi Movieland|
| distributor = Shree Vijayalakshmi Movieland|
| released = 15 January 1996|
| runtime = 162 min |
| country =   India Tamil |
| budget =  2.3 crore
}}
 1996 Tamil Vijay and Sanghavi in lead roles. It was directed by C.Ranganathan and released for Pongal in 1996.  This movie got positive reviews and became a hit at the box office.This movie was dubbed in Hindi as Rampuri Damaad.

==Plot==
Vijay comes to the modern city and stays with his friend who claims that he has a job. Actually he is also unemployed. Both of them are tenants of a girl named Sanghavi. First the hero and heroine get into fights, but then their arguments and fights turn into love. Meanwhile Sanghavis uncle Karan is also love in with her. One day Vijay witnesses a thief stealing the necklace and when he tries to catch him, the thief inserts the necklace in his pocket and Vijay is blamed for stealing it. Sanghavi starts hating Vijay. Taking advantage of this situation, Karan creates a rift between them by hiring goons to attack them and blames Vijay for that too. Vijay explains his sad story to her grandmother that he lost his mother during small age and he could not endure the torture of his stepmother because he escaped from the home. Paati believes him but Karan takes revenge by setting up wires and making Paati paralyzed. In the hospital, Sanghavi overhears that Karan and his father wants to kill Sanghavi to steal their colony. Vijay pays the medicinal bills and he attempts to commit suicide but Sanghavi saves him. Also, Karan who tried to kill him, is killed in a stampede. The film ends with the hero and heroine living happily.

==Cast== Vijay as Balu
*Sanghavi as Sumithra
*Goundamani as Gopal Karan as Mahesh, Sumithras cousin
*Nirmalamma as Paattimaa, Sumithras and Maheshs grandmother Senthil as White
*Vinu Chakravarthy as Maheshs father
* J.Lalitha as Balus stepmother Pandu as Traffic Police Ponraj
*Silk Smitha in an item number

==Soundtrack==
Music : Vidyasagar (music director)|Vidyasagar. 

{|class="wikitable" width="60%"
! Song Title !! Singers !! Lyrics
|- Vaali (poet) Vaali
|- Vijay
|-
| "Jeevan En Jeevan" || S. P. Balasubrahmanyam || P. R. C. Balu
|- Vaali (poet) Vaali
|-
| "Bombai Party" || Vijay (actor)|Vijay, Swarnalatha
|-
|}

==References==
 

==External links==
*  

 
 
 
 


 