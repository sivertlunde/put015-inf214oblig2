Krazy Kat & Ignatz Mouse Discuss the Letter 'G'
{{Infobox Hollywood cartoon|
| cartoon_name = Krazy Kat & Ignatz Mouse Discuss the Letter G
| series = Krazy Kat
| image = 
| caption = 
| director =
| story_artist =  Frank Moser Leon Searl
| voice_actor = 
| musician = 
| producer = William Randolph Hearst
| studio = International Film Service
| distributor = Hearst-Vitagraph News Pictoral
| release_date = February 25, 1916
| color_process = Black and white
| runtime = 1:53 English
| preceded_by = Krazy Kat and Ignatz Mouse Believe in Signs
| followed_by = Krazy Kat Goes a-Wooing
}}

Krazy Kat & Ignatz Mouse Discuss the Letter G is a silent short animated film featuring the comic strip character Krazy Kat. As with other animated shorts at the time, it was featured as an extra along with news reports that were released on film.

==Plot==
Krazy is a chef who works in a bistro. His customer is none other than Ignatz. Ignatz orders slices of roast duck with some gravy. Krazy serves the order, and even includes a whole pizza as a bonus. He then talks to the mouse about the way a lot of well-known people wear names beginning with G. Ignatz, however, does not believe in the trend, and therefore asks the cat to name a few individuals who share it. Krazy gives "G. Washington" as an example, although the others such as "G. Rusalem" (Jerusalem) and "G. Hosafat" (Jehosaphat) sound more like puns. Krazy then offers Ignatz one more bonus in a large strawberry pie. Ignatz finds the subject rather ludicrous as the cynical mouse splats the pie on the cat. Though flat on his front and covered in jam, Krazy is pretty quiet on the matter.

==External links==
*  at the Big Cartoon Database

 

 
 
 
 
 
 
 
 
 
 
 


 