The Confession (1999 film)
{{Infobox film
| name = The Confession
| image =  David Hugh Jones
| producer = Elie Samaha Andrew Stevens Ashok Amritraj
| writer = Sol Yurick (novel) David Black (screenplay)
| starring = 
| music = Mychael Danna
| cinematography = Mike Fash
| editing = Ray Hubley
| distributor = El Dorado Pictures Phoenician Films
| released = 29 March 1999
| runtime = 114 minutes
| country = United States
| language = English
| budget = $4,000,000 
| gross =   
}} David Hugh Jones, starring Ben Kingsley and Alec Baldwin. It is based on the novel by Sol Yurick.

==Plot==
After his young son dies from the negligience of medical professionals at a hospital, Harry Fertig (Kingsley) takes matters into his own hands and kills the negligent doctors responsible. Slick lawyer Roy Bleakie (Baldwin), looking only to win a case and not caring of the matters involved, is assigned Fertigs case. Shocked to hear that his client wants to plead guilty, the case causes Bleakie to question his own morals by defending an honorable man.

==Cast==
*Alec Baldwin as Roy Bleakie
*Ben Kingsley as Harry Fertig  
*Amy Irving as Sarah Fertig 
*Boyd Gaines as Liam Clarke  
*Anne Twomey as Judge Judy Crossland
*Richard Jenkins as Coss ODonell Kevin Conway as Mel Duden
*Jay O. Sanders as Jack Renoble
*Chris Noth as Campuso

==Production==
Filming took place largely in Brooklyn and Manhattan, New York.

== External links ==
*  

 

 
 
 
 
 

 