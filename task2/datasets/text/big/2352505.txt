Bones (2001 film)
{{Infobox film
| name           = Bones
| image          = Bones movie poster.jpg
| caption        = Theatrical release poster Ernest Dickerson
| producer       = Rupert Harvey Peter Heller Lloyd Segan
| writer         = Adam Simon Tim Metcalfe
| starring       = Snoop Dogg Pam Grier Khalil Kain Clifton Powell Bianca Lawson Katharine Isabelle Michael T. Weiss
| music          = Elia Cmiral Snoop Dogg
| cinematography = Flavio Labiano
| editing        = Michael N. Knue Stephen Lovejoy
| distributor    = New Line Cinema
| released       =  
| runtime        = 96 minutes
| country        = United States
| language       = English French Spanish
| budget         = $16 million 
| gross          = $8,378,853 
}} Ernest Dickerson. It is about a gangster that comes back from the dead to avenge his murder. The film stars Snoop Dogg as the title character Jimmy Bones.

The film is also a tribute to the blaxploitation films of the 1970s. Pam Grier is seen in the film as Jimmy Bones love interest.

==Plot== Ricky Harris), Bones elegant brownstone building becomes his own tomb, and is closed.

The timeline flashes forward to 2001, where the neighborhood has become a black ghetto and Jimmys brownstone building is a condemned ruin. Four teens, Patrick (Khalil Kain), his brother Bill (Merwin Mondesir), their white step-sister Tia (Katharine Isabelle) and their best friend Maurice (Sean Amsing), buy the property and they want to renovate it as a nightclub. In the process, Tia finds a black dog who is actually the spiritual manifestation of Jimmys tortured spirit. As the dog starts to eat, Jimmy is slowly resurrected.

Patrick meets Pearl (Pam Grier), Jimmys old girlfriend, and her daughter Cynthia (Bianca Lawson). Patrick develops a romance with Cynthia. Patrick wanted to open a nightclub at the old ghetto neighborhood in hopes of making the neighborhood great again, and also to make a profit. While exploring the basement, Patrick, Cynthia, Bill, Tia and Maurice find Jimmy Bones body and they realize that he was actually murdered. Patrick, Cynthia, Bill, Tia and Maurice decide to keep Jimmys murder quiet or they wont be able to open the nightclub and they bury the remains.

Later, Jeremiah(Clifton Powell) (who is Patrick and Bills father as well as Tias stepfather), finds out about Patrick and the gangs plan to open the club at Bones old building, he freaks out and demands that Patrick and the others leave the building. Patrick, Bill and Tia refuses his request and opens the nightclub in spite of their fathers objections. On opening night Maurice is lured into an upstairs room where he is mauled to death by the spiritual black dog.

Once he is fully resurrected Jimmy sets the club on fire and he is intent on getting revenge on those responsible for his death, those who betrayed him, and anyone who gets in his way. Pearl and her neighbor felt that they should have burned the building down a long time ago. After the incident, Pearl admits to Cynthia that Jimmy Bones is her father, as she had a relationship with him.

Patrick confronts his father Jeremiah and demands to know if he murdered Jimmy Bones 22 years ago. His father admits he betrayed Jimmy Bones to make money to leave the neighborhood. Also, he got fed up living in Bones shadow and he wanted to be as popular and successful as him. Jeremiah allowed drugs into the neighborhood as long he got paid for it. Later, Eddie Mack is having sex with his white girlfriend Snowflake (Erin Wright). With Mack being one the people who betrayed him, Jimmy confronts him after murdering Snowflake and stuffing her body in a trash bin. Jimmy decapitates Mack and does the same to Lupovich, but keeps their heads alive to transport their souls. 

Pearl, knowing that Jeremiah is next, goes with Cynthia to his house to rescue him. They ending up being too late. Pearl, Cynthia, Patrick, Bill, Tia and Jeremiahs wife Nancy (Lynda Boyd) watch him get dragged off by Jimmy, leaving nothing but a melted hole in the window. Jimmy  brings Jeremiah back to the building, along with the heads of Lupovich and Mack. Jimmy sends Lupovich and Mack to hell for all eternity while Jeremiah begs for his life.

Patrick, Cynthia, Bill, and Pearl go underground to find that Jimmy Bones body has disappeared. Pearl know that they have to burn the dress that was buried with Jimmy because the blood on the dress is what is actually keeping Jimmy alive the whole time. As they look for Jimmy, Pearl steps in the elevator which closes and goes up. Meanwhile, Jeremiah asks Jimmy what he wants. He asks Jeremiah if he could give him his life back. When Jeremiah says no, Jimmy sends him to hell for eternity.

Pearl gets off the elevator and walks into a room that is filled with ignited candles. She has a flashback and Jimmy appears, and puts the bloody dress on her. Patrick, Cynthia, and Bill head to the second floor where they see a ghostly Maurice, who leads Bill in the wrong direction where he is captured and killed. Patrick tries to reach him but is too late.

Patrick and Cynthia make their way to the room where Pearl and Jimmy are at. Patrick knows its a trap. As Cynthia is lured to Pearl and Jimmy, Patrick hears his fathers voice in a mirror begging for help. When Patrick hesitates, Jeremiah chokes him. Patrick uses his knife to chop Jeremiahs arm off and he disappears into hell. Patrick goes after Jimmy, who disappears and reappears behind Patricks back much more demonic-looking. He grabs Patrick by his throat, as Cynthia begs him to let go. Pearl, realizing what is happening, tells Jimmy she loves him then grabs a candle and burns the dress.

Jimmy tries to stop her but he dies with Pearl, causing the whole building to collapse. Patrick and Cynthia try to get to the exit before the building comes down. They jump down the elevator shaft before escaping. As Cynthia rests, Patrick finds an old picture of Jimmy and Pearl together. He then hears Jimmys voice say "dog eat dog, boy." He turns around and sees Cynthia, smiling, with maggots in her mouth. As she spits them at Patrick, the end credits begin.

==Cast==
*Snoop Dogg as Jimmy Bones
*Pam Grier as Pearl
*Michael T. Weiss as Lupovich
*Clifton Powell as Jeremiah Peet Ricky Harris as Eddie Mack
*Bianca Lawson as Cynthia
*Khalil Kain as Patrick Peet
*Merwin Mondesir as Bill Peet
*Sean Amsing as Maurice
*Katharine Isabelle as Tia Peet
*Ronald Selmour as Shotgun
*Deezer D as Stank
*Garikayi Mutambirwa as Weaze
*Erin Wright as Snowflake
*Josh Byer as Jason
*Kirby Morrow as Palmer
*Ellen Stephenovna Ewusie as The Death Lady
*Lynda Boyd as Nancy Peet

==Soundtrack==
*Bones (soundtrack)|Bones (soundtrack)
The soundtrack to the film was released on October 9, 2001 on Doggystyle Records and Priority Records. It peaked at #39 on the Billboard 200|Billboard 200, #14 on the Top R&B/Hip-Hop Albums chart and #4 on the Top Soundtracks chart.

==Reception==
The film received generally negative reviews and has a 21% rating on Rotten Tomatoes based on 66 reviews with an average rating of 3.8 out of 10 with the consensus "Slow to start, the sleek looking Bones is more silly than scary.  The film also has a score of 42 on Metacritic based on 21 reviews.   

The film opened at #10 at the U.S. box office, earning $2,823,548 in 847 theaters its opening weekend averaging $3,333 per theater. It ended up earning $7,316,658 domestically and  $1,062,195 internationally for a total of $8,378,853, falling short of its $16 million budget.   

==See also==
*J. D.s Revenge

==References==
 

==External links==
* 
* 
* 
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 