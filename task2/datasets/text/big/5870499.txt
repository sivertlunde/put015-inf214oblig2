Lullaby of Broadway (film)
{{Infobox film  
| name           = Lullaby of Broadway
| image          = Lullabyofbroadway-title.jpg
| image_size     = 
| caption        = Title card from the trailer David Butler William Jacobs
| writer         = Earl Baldwin 
| starring       = Doris Day   Gene Nelson   S.Z. Sakall   Billy De Wolfe   Gladys George   Florence Bates   Anne Triola
| music          = Ray Heindorf
| cinematography = Wilfred M. Cline
| editing        = Irene Morra
| distributor    = Warner Bros. 
| released       =  
| runtime        = 92 minutes
| country        = United States
| language       = English
| gross = $2,225,000 (US rentals) 
}}
 musical romantic comedy film released by Warner Bros. in 1951. It starred Doris Day as Melinda Howard, an entertainer who travels to New York to see her mother, and Gene Nelson as Tom Farnham, a fellow entertainer and Melindas love interest. Gladys George appears as Jessica Howard, Melindas alcoholic mother, in addition to  S.Z. Sakall, Billy De Wolfe, Florence Bates and Anne Triola.
 album of the same name.

== Plot ==
Melinda Howard is an entertainer traveling from England to pay a surprise visit to her mother, Broadway singer Jessica Howard, who lives in New York City. Melinda believes that her mother lives in a mansion, however, Jessicas alcoholism has reduced her to singing in a Greenwich Village bar (establishment)|saloon, and the mansion actually belongs to Adolph Hubbell and his wife.

The Hubbells butler, Lefty Mack, and his fiancée, Gloria Davis, the maid, are a down-on-their luck vaudeville team and are good friends of Jessica and have been forwarding her letters to Melinda. Lefty pretends that Jessica has rented the house to the Hubbells while she is on tour, and, when a disappointed Melinda discloses that she has no money, offers her one of the servants rooms for the night. Lefty promises Melinda that her mother will return home soon, and then informs Jessica of her daughters arrival. He then suggests that she come to the house the next night when the Hubbells will be giving a party attended by many Broadway performers.

Meanwhile, Adolph has discovered Melindas presence, and after Lefty explains the situation, agrees to keep Jessicas secret. At the party, Melinda awaits her mothers arrival, and while waiting, sees that one of party guests brought along Tom Farnham, who was on the boat with Melinda and had made a pass at her. He had also kept his profession a secret while on the boat. At the party, he entertains the crowd with a song and dance, as he is the male lead in George Ferndels newest production, Lullaby of Broadway.

Ferndel, the Broadway producer, tries to persuade Adolphe to invest in his latest show, something Adolph refuses to do unless he is able to help cast the production. Jessica fails to appear at the party because she has been hospitalized with delirium tremens and Lefty explains to Melinda that Jessicas show was too popular for her to leave, which leads Melinda to vow to wait for her.
 fur coat. far from fatherly. Melinda, upset by the insinuations, insists on returning the coat, and informs Lefty and Gloria that they will both have parts in the musical.

Before the coat is returned, however, Mrs. Hubbell finds it and believes that it is a surprise for her. She wears it that night to a charity ball where Melinda sees her and candidly remarks to Tom that the coat had originally been meant for her. Tom misinterprets her statements, and the two quarrel bitterly. Although Jessica has been released from the hospital, she fears Melindas reaction to her present state and refuses to meet with her.

Right before the show opens, Mrs. Hubbell learns the truth about the fur, and names Melinda in a divorce suit against Mr. Hubbell. Tom offers to "forgive" the shocked Melinda, and she realizes that he, too, thought she was romantically involved with Adolph. Shortly afterward, an aggressive reporter recognizes Jessicas picture and tells Melinda the truth about her mother. Completely shattered, Melinda decides to return to England and begs Lefty to pay for her ticket. Gloria and Lefty meet Melinda at the ship and escort her to a stateroom where Jessica is waiting. Mother and daughter are tearfully reunited, and Lefty informs them that Mrs. Hubbell now knows that there was nothing between Melinda and Adolph. They all leave together for the theater, where opening night is a success, and Tom and Melinda are free to pursue their romance. 

==Cast==
* Doris Day - Melinda Howard
* Gene Nelson - Tom Farnham
* S.Z. Sakall - Adolph Hubbell
* Billy De Wolfe - Lefty Mack
* Gladys George - Jessica Howard
* Florence Bates - Mrs. Anna Hubbell
* Anne Triola - Gloria Davis

==Musical Numbers== Lullaby of Broadway - Doris Day
** music and lyrics by Harry Warren and Al Dubin
* Youre Getting to Be a Habit with Me - Doris Day
** music and lyrics by Harry Warren and Al Dubin
* Just One of Those Things - Doris Day
** music and lyrics by Cole Porter
* Somebody Loves Me - Doris Day and Gene Nelson (dubbed by Hal Derwin)
** music by George Gershwin, lyrics by B. G. DeSylva and Ballard MacDonald
* I Love the Way You Say Goodnight - Doris Day and Gene Nelson (dubbed by Hal Derwin)
** music and lyrics by Eddie Pola and George Wyle
* Please Dont Talk About Me When Im Gone - Gladys George
** music and lyrics by Sam H. Stept, Sidney Clare and Bee Palmer
* In a Shanty in Old Shanty Town - Gladys George Jack Little, Joe Young
* Zing! Went the Strings of My Heart - Gene Nelson (dubbed by Hal Derwin)
** music and lyrics by James F. Hanley
* Youre Dependable - Billy De Wolfe and Anne Triola
** music and lyrics by Sy Miller and Jerry Seleen
* Wed Like to Go on a Trip - Billy De Wolfe and Anne Triola
** music and lyrics by Sy Miller and Jerry Seleen

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 