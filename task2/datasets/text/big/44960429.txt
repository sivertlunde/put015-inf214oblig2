Mavana Magalu
{{Infobox film 
| name           = Mavana Magalu 
| image          = 
| caption        = 
| director       = S K A Chari 
| producer       = A V Subba Rao 
| writer         = Ashapoornadevi 
| screenplay     = 
| starring       = Jayalalithaa Kalyan Kumar K. S. Ashwath T. N. Balakrishna
| music          = T. Chalapathi Rao
| cinematography = Annayya 
| editing        = J Krishnaswamy 
| studio         = Prasad Art Pictures 
| distributor    = 
| released       =   
| country        = India  Kannada 
}}
 1965 Cinema Indian Kannada Kannada film, directed by S K A Chari and produced by A V Subba Rao. The film stars Jayalalithaa, Kalyan Kumar, K. S. Ashwath and T. N. Balakrishna in lead roles. The film had musical score by T. Chalapathi Rao. 

==Cast==
  
*Jayalalithaa 
*Kalyan Kumar
*K. S. Ashwath 
*T. N. Balakrishna
*Narasimharaju 
*Ranga 
*Guggu 
*Rangabhoomi Narayan 
*Girimaji 
*H R Sharma 
*Shivaram
*Shivaji 
*Master Basavaraj 
*Master Yogindrakumar 
*Swamy Rao 
*H M Gowda 
*Bhaskar 
*Bangalore Basappa 
*B Jayamma 
*Vidyavathi 
*B. Jayashree 
*Vasanthi 
*Baby Padmashree 
*Sharada 
 

==Soundtrack==
The music was composed by T. Chalapathi Rao.  
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|-  Janaki || Ku. Vem. Pu || 03.27
|}

==References==
 

==External links==
*  

 
 
 


 