Cirkus Columbia
{{Infobox film name           = Cirkus Columbia image          = Cirkus Columbia.jpg director       = Danis Tanović producer       =   screenplay     = Danis Tanović story          = Ivica Đikić (novel) starring       =   released       =   runtime        = 113 min. country        = Bosnia and Herzegovina language       = English Bosnian budget         = 
}} Bosnian drama film starring Miki Manojlović, Mira Furlan, Boris Ler and Jelena Stupljanin. The film is set in the Herzegovina part of Bosnia and Herzegovina in the early 1990s, after the dissolution of Yugoslavia, and slightly before the Yugoslav Wars. It tells an emotional story of a man coming back to his hometown after many years abroad and dealing with his past and current family, using the political dealings of the region as a backdrop. It is based on the novel Cirkus Columbia by well known Bosnian Croat writer Ivica Đikić. 

==Cast==
*Miki Manojlović - Divko Buntić
*Mira Furlan - Lucija
*Boris Ler - Martin Buntić
*Jelena Stupljanin - Azra
*Mario Knezović - Pivac
*Ines Fančović - Starica

==Awards== Best Foreign Language Film at the 83rd Academy Awards,    but did not make the final shortlist.   

==See also==
* List of submissions to the 83rd Academy Awards for Best Foreign Language Film
* List of Bosnian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  
 
 
 
 
 
 
 
 

 