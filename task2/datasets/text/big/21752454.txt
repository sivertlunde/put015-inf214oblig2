Bachelor in Paradise (film)
{{Infobox film
| name           = Bachelor in Paradise
| image          = Bachelor-in-paradise.jpg
| image size     =
| alt            =
| caption        = Thetarical release poster Jack Arnold
| producer       = Ted Richmond (executive producer, uncredited) 
| writer         = Valentine Davies Hal Kanter based on = story by Vera Caspary
| narrator       =
| starring       = Bob Hope Lana Turner Janis Paige Paula Prentiss
| music          = Henry Mancini
| cinematography = Joseph Ruttenberg
| editing        = Richard W. Farrell
| studio         = MGM
| distributor    = MGM
| released       =  (World Premiere, London)
| runtime        = 109 minutes
| country        = United States
| language       = English
| budget         = $1,989,000 
| gross          = $3.5 million 
}} Jack Arnold, it was written by Valentine Davies and Hal Kanter, based on a story by Vera Caspary.

It co-stars Paula Prentiss, Jim Hutton and Janis Paige.

The film won three Laurel awards for Best Comedy, Best Comedy Actor (Hope) and song ("Bachelor in Paradise", music:  ), which was also nominated for Academy Award for Best Original Song. Bob Hope was also nominated for the Golden Globe Award for Best Actor – Motion Picture Musical or Comedy.
 Coliseum Theatre in Londons West End on November 2nd 1961 with a personal appearance from Bob Hope.

==Plot==
A.J. Niles is a provocative best-selling author who discovers that he has a large tax debt owed to the IRS, due to being ripped off by his accountant, Herman Wapinger. He goes undercover under the alias "Jack Adams" in a California suburban community called Paradise Village to research a new book about the wives and lives there. Niles is pursued by a flirtatious married woman named Dolores while falling in love with a woman, Rosemary, who rents her house to him. Wapinger is found, Niles cash is returned to him, and he reveals his true identity on national television.  The husbands in Paradise Village all file for divorce, believing their wives are all having affairs with Niles.  In divorce court, Niles reveals that he is in love with Rosemary and asks her to marry him.  Everyone lives happily ever after.

==Cast==
* Bob Hope as  Adam J. Niles
* Lana Turner as  Rosemary Howard
* Janis Paige as  Dolores Jynson
* Jim Hutton as  Larry Delavane
* Paula Prentiss as  Linda Delavane
* Don Porter as  Thomas W. Jynson
* Virginia Grey as  Camille Quinlaw
* Agnes Moorehead as  Judge Peterson
* Florence Sundstrom as  Mrs. Pickering
* John McGiver as  Austin Palfrey Clinton Sunberg as  Rodney Jones
* Alan Hewitt as  Attorney Backett
* Reta Shaw as  Mrs. Brown
* Vin Scully as Himself
==Production==
The script was based on an original story for the movies by Vera Caspary - a 70 page document. Fear of Originals Scored by Writer: Art Lost, Says Vera Caspary; Under the Skin Sold to 20th
Scheuer, Philip K. Los Angeles Times (1923-Current File)   14 Apr 1961: 27.  

The film was Bob Hopes first of MGM. Her was persuaded to star in it by head of production Sol Siegel. Looking at Hollywood: M-G-M Signs Bob Hope to Play Bachelor Author
Hopper, Hedda. Chicago Daily Tribune (1923-1963)   09 Dec 1959: b6.   The film marked Lana Turners first romantic comedy in a number of years. Turner, Hayworth New Lead-Getters: Lana to Hope, Rita to Ford; Sinatra in Bid to Moss Hart
Scheuer, Philip K. Los Angeles Times (1923-Current File)   06 Apr 1961: A13.  

George Marshall was originally signed to direct. Looking at Hollywood: Aldrich Will Produce Now We Know
Hopper, Hedda. Chicago Daily Tribune (1923-1963)   17 Feb 1960: a1.  

Paula Prentiss and Jim Hutton were signed off the back of their success together in   and The Honeymoon Machine and pushed them as a new William Powell and Myrna Loy. JIM AND PAULA: SHADES OF POWELL, LOY?
Alpert, Don. Los Angeles Times (1923-Current File)   16 July 1961: N4.  

Filming took place in May 1961.
==Reception==
MGM were impressed by the film and signed Jack Arnold to direct for them for give years. South Seas Braced for Film Invasion: One-at-a-Time Decree Made Law as Tahiti Goes Hollywood
Scheuer, Philip K. Los Angeles Times (1923-Current File)   04 Sep 1961: C7.   
===Proposed Sequel===
Before the film was released they requested Hal Kanter star writing a sequel, An Armful of Girls with Hope as a married man chased over Europe by titled ladies. Judy Garland and Lancaster Costar: Joan Crawford Being Paged; Hope to Do Bachelor Sequel
Hopper, Hedda. Los Angeles Times (1923-Current File)   05 Oct 1961: B12.   This was never made.
===Critical===
The Los Angeles Times called the film "frequently diverting". Film Comedy Puts Hope in Paradise
Scott, John L. Los Angeles Times (1923-Current File)   26 Dec 1961: C8.  
===Box Office===
According to MGM records, the film earned $2.5 million in the US and Canada and $1 million elsewhere but ultimately lost $344,000.  . 
===Ann Margaret===
The title song for the film was nominated for an Oscar. It was performed by Ann Margaret at the Oscars ceremony, and reception to this greatly boosted her career. Looking at Hollwood: Bob Hope Tells Views on Oscar Awards Show
Hopper, Hedda. Chicago Daily Tribune (1923-1963)   12 Apr 1962: c6.   SUNDAY NIGHT: First TV Special for Ann-Margret
MacMINN, ALEENE. Los Angeles Times (1923-Current File)   29 Nov 1968: g36.     accessed 15 December 2014 

==References==
 

==External links==
*  
*  
*  
*   at Turner Classic Movies Spotlight by Frank Miller

 
 
 
 
 
 
 
 
 
 

 