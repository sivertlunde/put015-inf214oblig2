Red Love
{{Infobox film
| name           = Red Love
| image          =Red Love film poster.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Theatrical release poster
| director       = Rosa von Praunheim 
| producer       = Rosa von Praunheim
| writer         = 
| screenplay     = Rosa von Praunheim	 	
| story          = 
| based on       =   
| narrator       = 
| starring       = Sascha Hammer   Mark Eins  Helga Goetze, Olga Demetriescu  Rose Hammer  Bettina Sukroff  Eddie Constantine Helga Goetze
| music          = Ideal, Din- a-Testbild, Jakob Lichtmann
| editing        = Elke Granke   Rosa von Praunheim
| cinematography = Mike Kuchar
| studio         = Rosa von Praunheim Filmproduktion
| distributor    = 
| released       = 20 February 1982 		
| runtime        =80 minutes
| country        = West Germany
| language       = German
| budget         = 
| gross          = 
}}
 German documentary film directed by Rosa von Praunheim. The film, divided in two interspersing completely different segments, deals with two women deprived of independence for many years because of either their family obligations or an authoritarian spouse. One segment is a documentary interview; the other is a fictional tale. Murray, Images in the Dark, p. 108 

The fictional narrative, made in the style of an early 20th century morality play, is based on Red Love (1927), a novel by Alexandra Kollontai, a feminist writer who was the first Soviet ambassador to Norway. It tells the story of a young woman Vassilissa (Sascha Hammer) who breaks with her early ideals to enter into a conventional bourgeois marriage and learns how to stand up to her womanizing husband (Mark Eins). 
 Commune in Vienna in order to live a life of sexual freedom. She became radicalized and oversexed claiming that all she wants to do is to have sex. While advocating for sexual liberation, she holds outrageous sexual ideas.   Murray, Images in the Dark, p. 108 

==Notes==
 

== References ==
*Murray, Raymond. Images in the Dark: An Encyclopedia of Gay and Lesbian Film and Video Guide. TLA Publications, 1994, ISBN 1880707012

==External links==
* 

 
 
 
 

 
 