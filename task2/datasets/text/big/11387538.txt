Ringing Bell
{{Infobox animanga/Header
| image           =  
| caption         = Cover art from the VHS release of the fllm
| ja_kanji        = チリンの鈴
| ja_romaji       = Chirin no Suzu action
}}
{{Infobox animanga/Print
| type            = novel
| author          = Takashi Yanase
| illustrator     =
| publisher       = Shueisha
| demographic     = Shōnen manga|Shōnen
| imprint         =
| published       = 1978
}}
{{Infobox animanga/Video
| type            = film
| director        = Masami Hata
| producer        =
| writer          =
| music           =
| studio          = Sanrio
| released        = March 11, 1978 (Japan)
                    April 8, 1983 (US)
| runtime         = 47 minutes
}}
 

  is the 1978 Japanese/American anime film adaption of the book of the same name written by Takashi Yanase. It is most notable by fans and critics as a G-rated childrens film which makes a sharp-sudden turn into a dark and violent story. It is also recognized as one of the only Japanese shock films directed towards children.  It was banned in most countries while some countries give it a restricted rating.

Ringing Bell was produced by    also follow suit.

==Synopsis==
A baby lamb named Chirin is devastated when his mother is killed by a wolf who raids the farm in the night. Seeking revenge, he must become like the very thing he wishes to destroy, and he must venture far beyond the safety his home and childhood into the wilderness to seek the fearsome Wolf King.

Ringing Bell starts off as a childrens film, but quickly merges into a darkly-toned story of the laws of nature and revenge. It has also been viewed in the past as a cautionary tale about venturing away from home, non-conformity and revenge.

==Characters== lamb who bell around his neck.
*Chirins mother - A sheep who is very loving towards her son Chirin.
*Wolf (ウォー Uou in the Japanese version) - An aging black wolf with a scar across one eye. The wolf lives in the mountains surrounding the farm and kills based on the belief that he must continue the cycle of nature.
*The other sheep - The nameless sheep on the farm are timid creatures who offer Chirin no comfort or support when his mother is killed.

==Plot== lamb named Chirin living an idyllic life on a farm with many other sheep. Chirin is very adventurous and tends to get lost, so he wears a bell around his neck so that his mother can always find him. His mother warns Chirin that he must never venture beyond the fence surrounding the farm, because a huge black wolf lives in the mountains and loves to eat sheep. Chirin is too young and naive to take the advice to heart, until one night the wolf enters the barn and is prepared to kill Chirin, but at the last moment the lambs mother throws herself in the way and is killed instead.

The wolf leaves, and Chirin is horrified to see his mothers body. Unable to understand why his mother was killed, he becomes very angry and swears that he will go into the mountains and kill the wolf. He leaves alone, and when he finally comes upon the wolf he challenges him to fight. The wolf simply ignores him and walks away, and Chirin follows. This continues for some time, and Chirin realizes that the only way he can fight the wolf is by becoming strong like him. After much begging, the wolf relents and tells Chirin he will train him, knowing that Chirin intends to kill him one day. Chirins training lasts well into his adulthood, and by this time he has become a vicious killer, and views the wolf as his father. Together they travel the mountains, killing indiscriminately.

One night the wolf takes Chirin to the farm where the lamb was born. Chirin claims not to remember it, saying that his home is on the plains with the wolf. The wolf watches while the ram graphically fights the farm dogs, then watches Chirin enter the barn, where the terrified sheep have gathered, while the wolf waits outside. Chirin spots a very young lamb that strongly resembles himself, cowering in the middle of the barn, and as he approaches, the lambs mother throws herself in the way. Struck by this similarity to his past, Chirin is startled and confused, and leaves the barn without killing the sheep. When the wolf demands to know why, Chirin tells him that he cant bring himself to do it.
 impales the wolf on his horns. The wolf expresses his gratitude and pride for Chirin, and dies. Chirin is saddened, but turns to the sheep in the barn, who quickly shut the door. When Chirin tries to tell them that he grew up on the farm, none of the sheep believe him, saying that such a terrifying animal could not be one of them. Chirin gives up and returns to the mountains alone.

As Chirin stands by a pool of water near the wolfs den, he hallucinates the wolfs reflection in the water next to his own. Overjoyed, he turns to see the wolf, but realizes he is alone. Chirin stands alone in the mountains, yelling for the wolf, as the snow begins to fall, and as the movie draws to a close the narrator tells us that no one ever saw Chirin again.

==Japanese voice cast==
*Chirin - Minori Matsushima (lamb), Akira Kamiya (ram)
*Chirins mother - Taeko Nakanishi
*Uou - Seizō Katō
*Narrator - Hitoshi Takagi

==English voice cast==
*Chirin - Barbara Goodson (lamb), Gregg Berger (ram) Sandra Snow
*Wolf - Bill Capizzi
*Narrator - Ron Gans

==Reception==
Ringing Bell has received positive reviews from critics. In Japan, it has been regarded as one of the most revolutionary anime of the 1970s, along with Galaxy Express 999 and Alps no Shoujo Heidi|Heidi, Girl of the Alps.  Though it is not as well known outside of Japan, Western critics such as Justin Sevakis of Anime News Network praised the dark storyline and artwork, and noted that it delivered a "sort of quick punch-to-the-face of the innocent." Sevakis also commented that "there is almost nothing uplifting about Ringing Bell and yet it maintains its sense of adorable while simultaneously destroying our concepts of the beauty of nature."    One of the most negative comments about the film came from Kimiko Anime Network who stated that the film should not be viewed by young children and could cause emotional trauma. 

==References==
 

==Further reading==
* 

==External links==
 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 