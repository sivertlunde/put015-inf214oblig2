The V.I.P.s
 
 
 
{{Infobox film
| name           = The V.I.P.s
| image          = The V.I.P.s film poster.jpg
| image_size     = 200px
| caption        = original film poster
| alt            =
| director       = Anthony Asquith
| producer       = Anatole de Grunwald
| writer         = Terence Rattigan
| starring       = Richard Burton Elizabeth Taylor Louis Jourdan Maggie Smith Orson Welles Rod Taylor  Elsa Martinelli  Margaret Rutherford
| music          = Miklós Rózsa
| cinematography = Jack Hildyard
| editing        = Frank Clarke (editor)
| distributor    = Metro-Goldwyn-Mayer
| released       = 19 September 1963 (United States)
| runtime        = 119 minutes
| language       = English
| budget         = $4 million 
| gross          = $15,000,000   The Numbers. Retrieved 5 September 2013. 
}}
 British drama film in Metrocolor and Panavision. It was directed by Anthony Asquith, produced by Anatole de Grunwald and distributed by Metro-Goldwyn-Mayer. The film was written by Terence Rattigan, with a music score by Miklós Rózsa.

It has an all-star|all-star cast including Richard Burton, Elizabeth Taylor, Louis Jourdan, Elsa Martinelli, Maggie Smith, Rod Taylor, Orson Welles and Margaret Rutherford, who won the Academy Award for Best Supporting Actress as well as the Golden Globe Award for Best Supporting Actress – Motion Picture.

==Plot== Terminal 2 very important people) of the title play out the drama of their lives in a number of slightly interconnected stories. The delays have caused serious hardship for most of the characters and have plunged some of them into a deep personal or financial crisis.

The central story concerns famed actress Frances Andros (Elizabeth Taylor) trying to leave her husband, millionaire Paul Andros (Richard Burton), and fly away with her lover Marc Champselle (Louis Jourdan). Because of the fog, Andros has the opportunity to come to the airport to convince his wife not to leave him.

Film producer Max Buda (Orson Welles) needs to leave London, taking his newest protégée Gloria Gritti (Elsa Martinelli) with him, by midnight if he is to avoid paying a hefty tax bill. The Duchess of Brighton (Margaret Rutherford), meanwhile, is on her way to Florida to take a job which will pay her enough money to save her historic home.

Les Mangrum (Rod Taylor), an Australian businessman, must get to New York City to prevent his business from being sold. His dutiful secretary, Miss Mead (Maggie Smith), is secretly in love with him. It being a matter of great urgency, she decides to approach Andros and ask him to advance the money, which will save Mangrums company.

Buda spots a poster picturing the Duchesss home. She is offered a sum of money if she will permit Buda to use it as a location in a film, enough to keep the house she loves. Andros, meanwhile, about to lose the woman he loves, is spared a possible suicide at the last minute when he and his wife reconcile.

==Production==
===Script===
According to Rattigan, the film is based on the true story of actress Vivien Leighs attempt to leave her husband, actor Laurence Olivier, and fly off with her lover, the actor Peter Finch, only to be delayed by a fog at Heathrow. 

===Casting===
Asquith chose Sophia Loren for Andross role, remembering the box-office success of the romantic comedy film The Millionairess (1960) he did with Loren in the main role. However, Taylor, scared by the appeal Loren had for Burton, persuaded Asquith to hire her instead; "Let Sophia stay in Rome", she told him. 
 Raymond Austin, stuntman and a friend of Burtons, appears in the film as Andross driver. Television personality David Frost portrays a reporter interviewing the VIPs at the airport.

==Reaction==
===Box Office===
Critical reaction to the film was mixed. It nevertheless did extremely well at the box office, helped by the enormous publicity attached to Burton and Taylors previous release, the dramatic epic film Cleopatra (1963 film)|Cleopatra (1963). 
 theatrical rentals 10th highest grossing in the United States. It had admissions of 765,804 in France. 

The team of Asquith, De Grunwald and Rattigan later produced another portmanteau film, the dramatic composite film The Yellow Rolls-Royce (1964). Robert Murphy disapproved of both films, remarking that "Asquith spent his last years making increasingly banal prestige productions like The V.I.P.s and The Yellow Rolls-Royce ". 

==Cast==
 
* Elizabeth Taylor as Frances Andros
* Richard Burton as Paul Andros
* Louis Jourdan as Marc Champselle
* Elsa Martinelli as Gloria Gritti
* Margaret Rutherford as Duchess of Brighton
* Maggie Smith as Miss Mead
* Rod Taylor as Les Mangrum
* Orson Welles as Max Buda
* Linda Christian as Miriam Marshall
* Dennis Price as Commander Millbank
* Richard Wattis as Sanders Ronald Fraser as Joslin
* David Frost as Reporter
* Joan Benham as Miss Potter
* Michael Hordern as Airport Director
* Lance Percival as B.O.A.C. Officer Martin Miller as Dr. Schwutzbacher
* Peter Sallis as Doctor
* Stringer Davis as Hotel Waiter
* Clifton Jones as Jamaican Passenger
* Moyra Fraser as Air Hostess
  Duncan Lewis as Hotel Receptionist Raymond Austin as Rolls-Royce Chauffeur
* Cal McCord as Visitor
* Virginia Bedard as Knebworth House visitor
* Jill Carson as Air Hostesses
* Ann Castle as Lady Reporter
* Rosemary Dorken as Airport Announcer
* Betty Trapp as Waitress
* Maggie McGrath as Waitress
* Lewis Fiander as Third Reporter (uncredited) John Blythe as Barman
* Richard Briers as Meteorological Official
* Richard Caldicot as Hotel Representative
* Reginald Beckwith as Head Waiter Terry Alexander as Captain Frank Williams as Assistant to Airport Director
* Clifford Mollison as Mr. River the Hotel Manager
* Gordon Sterne as Official
* Joyce Carey as Mrs. Damer
* Robert Coote as John Coburn
* Angus Lennie as Meteorological Man
* Peter Illing as Mr. Damer
 

==See also==
 
*1963 in film
*British films of 1963
*List of drama films
 

==References==
 

;Bibliography

*  
*  
*  

==External links==
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 