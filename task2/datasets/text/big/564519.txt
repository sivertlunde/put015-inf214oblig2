London '66–'67
 
{{Infobox album
| Name        = London 66–67
| Type        = EP
| Artist      = Pink Floyd
| Cover       = PinkFloyd London 66 67.jpg
| Released    = 19 September 1995 Sound Techniques Studios
| Genre       = Psychedelic rock, avant-garde music, space rock, jazz fusion
| Length      = 28:30
| Language    = Instrumental See for Miles
| Producer    = Joe Boyd
| Compiler    = Colin Miles
| Chronology  = Pink Floyd EP Wish You Were Here EP (1995)
| This album  = London 66–67 (1995)
| Next album  = 
| Misc        =   {{Extra chronology
  | Artist      = Pink Floyd
  | Type        = video
  | Last album  =   (2003)
  | This album  = London 66–67 (2005)
  | Next album  = The Story of Wish You Were Here (2012)
  }}
}}
{{Album ratings
| rev1      = Allmusic
| rev1Score =   
}} EP and film of Pink Floyd music, containing two "lost" tracks—an extended version of "Interstellar Overdrive" and a previously unreleased track "Nicks Boogie". These tracks were originally recorded for Peter Lorrimer Whiteheads film Tonite Lets All Make Love in London in 1967,    and the former appeared in edited form on the soundtrack album.  Originally released in full on the 1990 See for Miles Records UK reissue of the soundtrack album, they are the earliest Pink Floyd recordings available commercially. 
 psychedelic compositions.

==Track listing==
{{tracklist
| title1 = Interstellar Overdrive Richard Wright
| length1 = 16:46
| title2 = Nicks Boogie
| length2 = 11:55
| note2 = Mason
}}

==DVD==
*London 66–67, the original film with the full length video of "Interstellar Overdrive" and "Nicks Boogie".
*Interview footage from the 1960s of Mick Jagger, David Hockney, Michael Caine and Julie Christie.
*Footage capturing the London Scene in the late sixties.
*Overview by director Peter Whitehead.

==Personnel==
;Pink Floyd
*Syd Barrett&nbsp;– electric guitar
*Roger Waters&nbsp;– bass guitar Richard Wright&nbsp;– organ
*Nick Mason&nbsp;– Drum kit|drums, percussion

;Production production
* engineering

==References==
 

 

 
 
 
 
 
 
 

 