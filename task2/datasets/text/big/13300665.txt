Umizaru (film)
{{Infobox Film 
| name = Umizaru 
| image          = Umizaru japanese poster.jpg released = 2004 
| writer         = Manga:    Karina Atsushi Atsushi Itō Miki Ryosuke Kyoko Jun Kunimura 
| director       = Eiichiro Hasumi 
| runtime = 120 min.  Japanese 
| music          = Sato Naoki 
|
}}
Umizaru is a  . The film is the first of the 3 part film and television project. The project is adapted from the popular 12 Shogakukan manga books Umizaru written by Yōichi Komori (小森陽一) and art by Shūhō Satō from 1998 to 2001. The film stars Ito Hideaki as Japan Coast Guard (JCG) rescue diver Senzaki Daisuke, and Kato Ai as his love interest Izawa Kanna.

There are also NHK dramas Umizaru (2002) and Umizaru 2 (2003).
 rescue diver trainees by local townsfolk of the city of Kure due to their excessive and uninhibited behaviour during off hours.
 Open Arms" as the theme song was used in the film.

==Cast==
{| class="wikitable"
|- style="background:#efefef;"
! Actor !! Role
|-
| Ito Hideaki || Senzaki Daisuke
|-
| Kato Ai || Izawa Kanna
|-
| Ken Kaito || Yuji Mishima
|- Karina || Nurse Erika Matsubara
|- Atsushi Itō || Hajime Kudo
|-
| Kyoko ||   Natsuko Nakasako 
|-
| Jun Kunimura || Admiral Masaki Igarashi
|-
| Tatsuya Fuji || Sergeant Minamoto
|}

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 


 
 