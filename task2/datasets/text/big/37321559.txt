Cesar Chavez (film)
{{Infobox film
| name           = Cesar Chavez
| image          = Cesar Chavez 2014 film.jpg
| caption        = Film poster
| director       = Diego Luna
| producer       = Emilio Azcárraga Jean Lianne Halfon John Malkovich Gael Garcia Bernal Russell Smith Diego Luna Larry Meli Keir Pearson Pablo Cruz
| writer         = Keir Pearson
| starring       = Michael Peña America Ferrera Rosario Dawson John Malkovich
| studio         = Canana Films
| distributor    = Pantelion Films Participant Media
| music          = Michael Brook
| cinematography = Enrique Chediak
| editing        = Douglas Crise Miguel Schverdfinger
| released       =  
| runtime        = 102 minutes
| country        = Mexico United States
| language       = English
| budget         = $10 million
| gross          = $6.7 million 
}} labor leader Cesar Chavez|César Chávez, who cofounded the United Farm Workers. The film stars Michael Peña as Chávez. John Malkovich co-stars as the owner of a large industrial grape farm who leads the opposition to Chávezs organizing efforts. It premiered in the Berlinale Special Galas section of the 64th Berlin International Film Festival.   

==Plot==
The film follows Chávezs efforts to organize 50,000 farm workers in  , the Salad Bowl strike, and the 1975 Modesto march.

==Cast==
* Michael Peña as Cesar Chavez|César Chávez Helen Chávez
* Rosario Dawson as Dolores Huerta
* Yancey Arias as Gilbert Padilla
* Wes Bentley as Jerry Cohen
* Michael Cudlitz as Sherriff Smith
* Gabriel Mann as Bogdanovich Junior
* John Malkovich as Bogdanovich Senior
* Mark Moses as Fred Ross
* Jacob Vargas as Richard Chavez
*  . June 5, 2012. Accessed 2012-10-14. 
* Héctor Suárez (cameo)

==Production==
  in 1974.]]

===Screenplay and production staff===
Although numerous books, magazine articles, and scholarly studies have been written about César Chávez, Chávez is the first feature film about the labor leader.   Los Angeles Times. July 1, 2012. Accessed 2012-10-14.  Keir Pearson, who wrote the Academy Award-nominated screenplay for the 2004 film Hotel Rwanda, wrote Chávez. McNary, Dave. "Participant Marches With Chavez." Variety (magazine)|Variety. June 5, 2012.  Many writers and producers had tried for years to obtain the rights to Chávezs life story, but failed.    Pearson negotiated for two years with Chavezs heirs before he and production partner, television producer Larry Meli, were able to secure the rights to Chávezs life in 2011. Pearson says his script focuses on the positive aspects of Chávezs personality, family life, and public accomplishments, but it is not a whitewash. Pearson and the producers reviewed the script with the Chávez family. Many of the comments made by the family, as well as anecdotes told by them, made it into the script.  Pearson also relied heavily on archival material held by the César Chávez Foundation.  The script focuses primarily on the grape boycotts and strikes of the 1960s and early 1970s. 

The producers of Chávez include Diego Luna, Gael Garcia Bernal, and Pablo Cruz (all principals of Canana Films); John Malkovich, Lianne Halfon, and Russell Smith (principals in Malkovichs production company, Mr. Mudd); writer Keir Pearson; and TV producer Larry Meli.  In June 2012, production company Participant Media purchased the North American distribution rights to the film, and Participant Medias Jeff Skoll and Jonathan King were added as executive producers. 

Chávez is directed by Diego Luna. The film is only Lunas second motion picture, and is his first film whose primary language is English. Luna said that directing in both English (for the main actors) and Spanish (for the extras) was a struggle. 

===Casting===
  into the Labor Hall of Fame and dedication of the Cesar E. Chávez Memorial Auditorium at the U.S. Department of Labor in March 2012.]]
Chicago-born Michael Peña stars as César Chávez, the Mexican American labor leader born in Yuma, Arizona, in 1927. Peña says he knew almost nothing about César Chávez prior to taking the role.  His father, a Mexican farmer who emigrated to the United States, almost wept when Peña told him that he was going to play César Chávez. Peña says he extensively studied historical records to gain a better understanding Chávez. Because Peña keeps his hair very short, he had to wear a wig during the production. 

America Ferrera was cast as Helen, the wife of César Chávez who played a quiet, behind-the scenes role in Chávezs work. In contrast to Michael Peña, Ferrera (who was born in Los Angeles, California, to parents who had emigrated from Honduras) said she had learned a great deal about who César Chávez was while growing up and in school. Ferrera said she met several times with Helen Chávez to learn more about her role in the farmworker movement. Ferrera says that she learned that Helen Chávez pushed her husband hard to keep the farmworker movement alive, all while raising eight children. Ferrera called the role daunting. 

Rosario Dawson was cast as Dolores Huerta, the  . April 17, 2012. Accessed 2012-10-14.  Huerta has expressed her happiness that Dawson took the role. 

John Malkovich became involved with Chávez through his role as producer. Diego Luna convinced him to take the role of an abusive grape-grower. Malkovich agreed to the role because he admired Lunas previous film, and wished to take part in telling an important story about fairness. Actor Gabriel Mann plays another abusive agricultural producer. Mann says he took the role because he felt it was a timely story that spoke to what happens when workers lack union protections. 

===Production locations and notes=== Filipino agricultural workers whom Chávez also sought to organize. The citys Art Deco public library served as the headquarters of one of the large agricultural companies Chávez dealt with, and a   field outside Hermosillo served as a farm near Delano, California. Scenes in grape fields were filmed in vineyards in the Mexican state of Sonora, where grape-growers still drape grape vines over wooden crosses as Californians did in the 1960s. The production built shacks in the Sonoran grape fields to replicate the housing of migrant workers in California in the 1960s. The shooting in the Sonoran grape fields was difficult. The production was afflicted with dust storms and a tremendous number of insects. It was also terribly hot, and several actors collapsed on the set from dehydration. 

Historic accuracy was important to the filmmakers. In addition to choosing locations which looked like California in the 1960s, actors were taught to speak in a Chicano dialect typical of the late 1960s and early 1970s. Dialect coach Claudia Vazquez says that dialect is very different from the Spanish and Spanish-inflected English spoken by many Mexican Americans in California today. 

The film has a production budget of $10 million, nearly all of which came from Mexican investors. 

== Reception ==
 
Cesar Chavez received a mixed reception from critics upon its release. It currently holds a 41% rating on  , the film has a score of 51 out of 100, based on 26 critics, indicating "mixed or average reviews" from critics.  One negative view, from historian Matt Garcia, expressed that the film concentrates too much on hero-making and avoids criticism and complexity, but offers that this is a limitation of the biopic genre.  However, critic Owen Gleiberman, in his positive review of the film, stated that it "couldnt be more timely." 

== See also ==
 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 