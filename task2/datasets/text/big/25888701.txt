Foster Daddy, Tora!
{{Infobox film
| name = Foster Daddy, Tora!
| image = Foster Daddy, Tora!.jpg
| caption = Theatrical release poster 
| director = Yoji Yamada
| producer = 
| writer = Yoji Yamada Yoshitaka Asama
| starring = Kiyoshi Atsumi Ran Itō
| music = Naozumi Yamamoto
| cinematography = Tetsuo Takaba
| editing = Iwao Ishii
| distributor = Shochiku
| released = December 27, 1980
| runtime = 100 minutes
| country = Japan
| language = Japanese
| budget = 
| gross = 
}}

  aka Torasans Song of the Seagull  is a 1980 Japanese comedy film directed by Yoji Yamada. It stars Kiyoshi Atsumi as Torajirō Kuruma (Tora-san), and Ran Itō as his love interest or "Madonna".  Foster Daddy, Tora! is the twenty-sixth entry in the popular, long-running Otoko wa Tsurai yo series.

==Synopsis==
Tor-san takes care of the teenage daughter of a friend after he dies. He takes the girl to Tokyo where she wishes to study, and becomes worried over her romantic life.       

==Cast==
* Kiyoshi Atsumi as Torajirō 
* Chieko Baisho as Sakura
* Ran Itō as Sumire
* Shimojo Masami as Kuruma Tatsuzō
* Chieko Misaki as Tsune Kuruma (Torajiros aunt)
* Gin Maeda as Hiroshi Suwa
* Hayato Nakamura as Mitsuo Suwa
* Hisao Dazai as Boss (Umetarō Katsura)
* Gajirō Satō as Genkō
* Chishū Ryū as Gozen-sama
* Takehiro Murata as Sadao Kikuchi
* Tatsuo Matsumura as Professor Hayashi

==Critical appraisal== Japan Academy Prize ceremony for his roles in Foster Daddy, Tora! and the following entry in the series, Tora-sans Love in Osaka (1981).  Stuart Galbraith IV writes that Foster Daddy, Tora! is one of the top films in the Otoko wa Tsurai yo series, and "like the best entries, both funny and sweet".  The German-language site molodezhnaja gives Foster Daddy, Tora! three and a half out of five stars.   

==Availability==
Foster Daddy, Tora! was released theatrically on December 27, 1980.  In Japan, the film has been released on videotape in 1996, and in DVD format in 1999, 2002, and 2008. 

==References==
 

==Bibliography==
===English===
*  
*  
*  

===German===
*  

===Japanese===
*  
*  
*  
*  

==External links==
*   at www.tora-san.jp (Official site)
*  

 
 

 
 
 
 
 
 
 
 
 

 