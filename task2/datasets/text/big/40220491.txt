Salim (film)
{{Infobox film
| name           = Salim
| image          = Salim vijay antony.jpg
| alt            =  
| caption        = 
| director       = N. V. Nirmal Kumar
| producer       = R. K. Suresh M. S. Saravanan Fathima Vijay Antony
| writer         = N. V. Nirmal Kumar
| screenplay     = 
| story          = 
| starring       = Vijay Antony Aksha Pardasany
| music          = Vijay Antony
| cinematography = M. C. Ganesh Chandra
| editing        = M. V. Rajesh Kumar
| studio         = Studio 9 Production Sree Green Productions  
| distributor    = Sree Green Productions
| released       =  
| country        = India
| language       = Tamil
| budget         = 
| gross          = 
}} Tamil action Bharathi Raja.  It stars Vijay Antony and Aksha Pardasany. Produced by Studio 9, Sri Green Productions and Vijay Antony Film Corporation, the film is named after the character Salim that Vijay Antony enacted in his debut film Naan (film)|Naan (2012).The film released on 29 August 2014 to positive reviews from critics and become Hit at the box office.

==Plot==

Salim (Vijay Antony) is an honest and simple doctor working in a private hospital, he often goes out of his ways to help the needy patients that come to him. His talent and nature earn wrath and jealousy from his coworkers at the hospital. One fine day, he learns that someone is stalking him. He finds that it is Nisha (Aksha Pardasany) his suitor, doing a background check on his character. Salim directly goes to Nisha and gives his personal diary for her to learn more about him. This response from him impresses Nisha so much and she falls in love with Salim. As their relationship progresses, she learns that Salim has dedicated himself to his work, so much that he always fails to take care of her and spend time with her and this leads to a rift forming between them. So to reconcile with Nisha, Salim plans to spend a whole day with her. They meet at a movie theatre and some goons tease Nisha, but Salim, instead of fighting them, chooses to take Nisha and leave the place. Nisha gets offended by this and she chooses to ignore Salim and his phone calls from then on.

Meanwhile, the hospitals Managing Directors are irritated with Salims generosity and charity and they give him a warning to stop refusing fees from patients. After few days, Salim manages to convince Nisha and they renew their relationship. Nisha asks Salim to go to a party with her and he agrees. But when he is on the way, he sees a young girl, a gang rape victim, who is hurt and bleeding on the road. He takes her to the hospital and treats her and misses the party. Nisha loses her temper this time and she fights and breaks up with him. 

The next day while Salim enters the hospital, he finds the girl he admitted is discharged from the hospital. He is disappointed as he comes to know that his money minded MD has evicted the girl as she is poor and unable to pay her fees. The same evening, he gets an invite to a party from his hospital. There, he learns that his MD has had enough of his charity and generosity at the hospitals cost and so he has been laid off and it is his own farewell party. He is also insulted by his boss for not using his talent and reputation to earn money. Salim is frustrated and storms out of the party in rage. On the way, he gets into a scuffle with a cop and beats him and ends up in police station. But he escapes from the station with the cops pistol, and goes straight to a hotel.

There he finds four men trying to rape the hotel singer. Salim fights with them and helps the girl leave. He feels that they have to learn their lesson, so he takes them as hostages inside the room. One of the men is the son of an MP, so the police team, led by officer Chezhian are pressurized to take action to free the hostages. Seeing this on the news, Nisha goes to the hotel and meets police officer Mr. Chezhian and explains about Salims character and argues that he is not a terrorist to take hostages. The police force plans to use Nisha as bait and capture Salim but they fail to do so and Salim in turn throws one of the men out of a window leading to serious injury. He asks them to take him seriously or they will have to face more disastrous results. 

Hearing this, the MP comes to the hospital to save his son and asks his demand. A whole day passes as they wait for response and Salim finally reveals that these four men have previously raped another poor girl , the one whom he found on the road and treated. He demands the MP that he has to give a public apology for his sons behavior or face serious consequences. When the MP hesitates to agree, Salim threatens to harm another person. Fearing that another man might be thrown out of the window, the police stand in their positions to rescue, but they find one chopped finger after another being thrown out. Fearing more damage, the MP gives an apology to the media, in front of the public, on behalf of his sons behavior and agrees to any punishment that is given by the law to the culprits and that he will not involve or use his political influence.

After this, Salim knows that he will not be left alive by the MP once he lets go of the hostages. So he demands a car for him to leave and asks the police not to follow him. This time the MP gives a car that is fixed with a bomb and remote detonator. Salim gets the car but lets only two hostages out, and takes the MPs son along with him and leaves. He also cleverly ignores the given car and takes the MP sons car and drives away. After a while, the MP receives a call from his son, telling that Salim left him and the middle of nowhere and escaped. Hearing this, the MP orders his men to bring back his son who is stranded and kill Salim who has driven away and make it look like an automobile accident. The men chase the car and make it fall off a bridge, only to find that Salim chose to leave the car and it was the MPs son who was driving inside; a common mistake that people make, to think that the hostage will be thrown out on the road and the kidnapper will flee in a vehicle. This indicates that Salim is still out there, alive and well.

==Cast==
* Vijay Antony as Mohammed Salim
* Aksha Pardasany as Nisha
* R. N. R. Manohar as Thavapooniam
* Swaminathan as Swaminathan
* Aruldass as a Police Officer
* Sushmitha
* Premji Amaren as himself (cameo appearance)
*priya ashmitha(item dancer"mascara song")
*PV Chandramoulli(police officer Chezhian)

==Production==
After his debut venture Naan, Vijay Antony decided to start Salim. Vijay Antony plays a role of a doctor while Aksha Pardasany was signed to portray his fiance in the film, making her Tamil film debut.  Since the movie required Vijay Antony to perform some dare devil stunts, he understood the necessity and underwent rigorous training in Taekwondo for a period of two months.  The shoot of the first schedule of the film was held in Chennai in June 2013.

==Soundtrack==
{{Infobox album  
| Name        = Salim
| Type        = Soundtrack
| Artist      = Vijay Antony
| Cover       = 
| Alt         = 
| Released  = June 05, 2014
| Recorded    = 2013 - 2014 Feature film soundtrack
| Length      = 21:21
| Label       = Saregama
| Producer = Vijay Antony
| Last album =Madha Gaja Raja (2013)
| This album = Salim (2014)
| Next album = Iruvar Ullam (2014)
}}

The music of the film was composed by Vijay Antony. The soundtrack album was released at Sathyam Cinemas, Chennai on June 5, 2014, with Bharathiraja, Bala (director)|Bala, M. Raja, R. K. Selvamani and R. Parthiepan among other attending the event.  Behindwoods gave 2 out of 5 and called the album "a mixed bag from Vijay Antony"but the album was well received and won good feedback. 

{{Track listing
| total_length   = 21:21
| lyrics_credits = yes
| extra_column	 = Singer(s)
| title1	= Prayer
| lyrics1 	= The Holy Quran
| extra1        = Yusuf
| length1       = 3:04
| title2        = Unnai Kanda Naal Mudhal
| lyrics2 	= Annamalai
| extra2        = Supriya joshi, Hemachandra, Srinivasan
| length2       = 5:05
| title3        = Siva Sambo
| lyrics3       = Gana Bala
| extra3 	= Mahalingam, Emcee Jesz
| length3       = 4:12
| title4        = Mascara Pottu
| lyrics4 	= Priyan
| extra4        = Supriya joshi, Sharmila, Vijay Antony
| length4       = 4:53
| title5        = Ulagam Nee
| lyrics5       = Annamalai
| extra5        = Prabhu Pandala
| length5       = 4:07
}}

==Release==
Salim released on 29 August 2014 in around 400 screens across the country, including Kerala and Karnataka, with Tamil Nadu contributing close to 300 screens. Gopuram Films and Sri Production distribute the film in India. Salim released in about 50 screens in the overseas space in key countries, through Suara Networks.

==Critical response==
The film received mixed to positive review from critics.   gave 3 stars out of 5 and wrote, "Vijay Antony is definitely not an expressive actor but with Naan earlier and now Salim, he has managed to find roles where impassiveness is a trait of the character...The first half of Salim nicely sets up whats in store...It is only in the second half that things get somewhat cinematic".  The New Indian Express wrote, Salim may not have the best of scripts, but with its racy pace and twists, it manages to keep one glued to the screen for the most part".  Sify wrote, "Salim is a decent enough thriller that compensates for its weak first half by a smarter second half and some composed acting by Vijay Antony".  Behindwoods in its review stated, "This is not the first time that Tamil Cinema witnesses such a plot, however the way Salim is treated, manages to keep the audiences engaged", calling it a "watchable average movie".  Moviecrow stated, Salim is definitely a sound choice by Vijay Antony as a follow up to Naan".  Indiaglitz.com wrote, "The film is clean and thought provoking, and is certainly worth the time invested".   

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 