Giovanna d'Arco al rogo
 
{{Infobox film
| name           = Giovanna dArco al rogo
| image          = Giovanna dArco al rogo film poster.jpg
| image_size     = 
| caption        = 
| director       = Roberto Rossellini
| producer       = Giorgio Criscuolo Franco Francese 
| writer         = Roberto Rossellini
| narrator       = [tua 
| starring       = Ingrid Bergman Tullio Carminati 
| music          = Arthur Honegger
| cinematography = Gábor Pogány
| editing        = Jolanda Benvenuti
| distributor    = 
| released       = December 20, 1954|
| runtime        = 76  Min
| country        = Italy Italian
| budget         = 
}}
 Italian film directed by  Roberto Rossellini and starring his wife Ingrid Bergman, which shows a live performance on December 1953 at the San Carlo Theatre in Naples. It is based on the oratorio Jeanne dArc au Bûcher by Paul Claudel and Arthur Honegger. It was filmed using a color process called Gevacolor.

==Plot==
 
The film takes place mostly in a surrealistic fantasy around the time of the execution of Joan of Arc. Joan of Arc, played by Ingrid Bergman, is being burned alive for heresy. In a kind of dream state, she departs from her body and looks back upon her life. She begins this journey depressed and demoralized. However, a priest appears to help guide her. First, he shows her those who accused her in the guise of animal characters, in order to show her their true nature. Then, he shows her the good that she has performed for people. In the end, she is proud of what she has done and is ready to face the flames.

==Reception==
Like most Bergman and Rossellini collaborations, it did not perform well at the box office.

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 
 

 
 