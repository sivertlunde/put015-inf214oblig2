Rao Saheb (film)
{{Infobox film
| name           = Rao Saheb
| image          = 
| alt            = 
| caption        = 
| director       = Vijaya Mehta
| producer       = Vinay Welling
| writer         = Vijaya Mehta Anil Chaudhary
| based on       =  
| starring       = 
| music          = Bhaskar Chandavarkar
| cinematography = Adeep Tandon
| editing        = Suresh Avdhoot
| studio         = 
| distributor    = 
| released       =  
| runtime        =  123 minutes
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}} 1985 Hindi period drama film directed by Vijaya Mehta, a noted theatre director. It is based on a Marathi novel Andharachya Parambya by Jaywant Dalvi, on which he also wrote the play Barrister. The film set in small rural town in Maharashtra in early 19th century, and deals with clash of progressives and orthodox traditions, while depicting plight of women especially widows in traditional Indian society under feudalism and oppressive patriarchy.  
   The film starred Anupam Kher, Vijaya Mehta, Nilu Phule, and Tanvi Azmi.
 Best Supporting Best Art Direction.   

==Synopsis==
A young barrister Rao Saheb return from England after education, with progressive ideas. He plans to break the orthodox customs of Brahmin society. He live in his old mansion in a small town in rural Maharashtra, where situation hasnt changed. His lives with his orthodox father and widowed aunty, Mausi, who has befriended, a young widow Radhika in a neighbouring home. Radhika rebels against the patriarchal customs imposed on widows at the time. Rao Saheb empathizes with her and comes close her, while his Mausi also fights for modernity by trying save Rahika from the shackles of widowhood.

==Cast==

* Anupam Kher as Rao saheb
* Vijaya Mehta as Mausi
* Nilu Phule 
* Tanvi Azmi as Radhika
* Arvind Gadgil
* Chandrakant Gokhale
* Vasant Ingle 
* Mangesh Kulkarni

== References ==
 

==External links==
*  

 
 
 
 
 
 
 
 
 