Crazy on the Outside
 
{{Infobox film
| name = Crazy on the Outside
| image = Crazy on the outside poster.jpg
| caption = Theatrical release poster
| director = Tim Allen
| producer = Brian Reilly Brett Gregory Anastasia Stanecki
| writer   = Judd Pillot John Peaslee
| starring = Tim Allen Sigourney Weaver Jeanne Tripplehorn J. K. Simmons Julie Bowen Kelsey Grammer Ray Liotta David Newman
| cinematography = Robbie Greenberg
| editing = Scott Conrad
| studio   = Boxing Cat Films
| distributor = Freestyle Releasing
| released =  
| runtime = 96 minutes
| country  = United States
| language = English
| gross    = $88,335
}}
 directorial debut, and is notable for reuniting Allen with co-stars from many of his previous films (Sigourney Weaver from Galaxy Quest, Ray Liotta from Wild Hogs, Kelsey Grammer from Toy Story 2 and Julie Bowen from Joe Somebody).

==Plot== movie piracy.  He is picked up by his eccentric sister Viki (Sigourney Weaver) who is revealed to be a habitual liar (she claims its so people arent hurt by the truth).
 back to France" soon).

During dinner, Gray (Ray Liotta), Tommys old partner from the movie piracy business, shows up and his sister runs him off.

The next day, Tommy meets with his parole officer, Angela Popadopolous (Jeanne Tripplehorn), who requires him to work at a pirate-themed restaurant called "Pirate Burger" as part of his parole.  He informs Angela that he wants to start up his fathers painting business instead, but is told that he is required to work at the restaurant.

While on the job, he sees Christy (Julie Bowen), his "dead" ex-girlfriend come up to the drive-through window. Realizing Viki lied to him, he takes the delivery car to see Christy leading them to spend the night together.  The next morning, it is revealed that Christy is engaged to Frank (Kelsey Grammer) and Tommy is forced to escape the apartment.

Since he took the delivery car without permission, he is fired from "Pirate Burger" and risks going back to jail for breaking the terms of his parole.  He is given another chance and goes back to work.

That night, he is "kidnapped" by Gray and Gray tries to persuade him to get back into the piracy business. Tommy refuses stating that he wants to "go straight".

Tommy is called to make a delivery. He arrives at the apartment of Angela. Her son is trying to play matchmaker to get her to start dating again. While there, he notices that the apartment is in need of a paint job. Tommy decides to show Angela the kind of work he can do. With the help of two ex-con coworkers Rick and Edgar (Malcolm Goodwin and Jon Gries) from Pirate Burger, he breaks into Angelas apartment while shes away and repaints the living room.
 French astronaut who was killed on the launchpad so that Tommys mother would not fly everyone to France to meet her (because she does not exist).
 little league team. Later, the judges wife who is impressed by the work invites Tommy to paint the upstairs as well.

While getting tools together, Edgar and Rick prepare the upstairs for painting and knock the judges wifes diamond ring off the dresser.  Wanting to do something nice for Tommy, Edgar steals the ring and gives it to Christy as a "gift from Tommy". Tommy, who doesnt know that the theft has occurred, joins Angela at the little league game and they begin to express feelings for each other. They set up a formal Dating|date.

When Tommy arrives at Angelas apartment for the date, Angela is furious over the theft and demands the ring back. He goes to Pirate Burger to confront Edgar and Rick, but they have disappeared. He steals the delivery car (his vehicle refuses to start) and calls Edgar and Rick. They inform him of the rings location and he drives to Christys house to get the ring back. After a short confrontation with Frank and Christy, he gets the ring and returns it to Angela.

The manager of Pirate Burger presses charges on Tommy and he goes back to jail. Gray shows up to bail him out and, disillusioned with his attempts to "go straight", Tommy teams back up with Gray.

Meanwhile, Angelas son shows up at Vikis house wanting to see Tommy but he is not there. Viki finds Tommy with Gray at the airport and makes up a story about Angelas son going missing. Tommy chooses to abandon Gray at the airport (after Gray pushes Viki to the ground) to find Angelas son. When he shows up at Angelas apartment, he finds out that Viki has lied to him again. Tommys concern for the well being of her son softens Angelas animosity towards him.

At the behest of Viki, Tommy invites Angela to come to a dinner at Vikis house. Angela shows up for the dinner. A story Viki told Tommys grandmother about Angela being Tommys "grief counselor" falls apart when Angela informs her that she is actually Tommys parole officer and the France story falls apart also.

The movie ends with Viki trying to act surprised but no one is buying it.

==Cast==
*Tim Allen as Tommy Zelda, the parolee    manipulative older sister 
*Jeanne Tripplehorn as Angela Papadopolous, a single-mom parole officer whom he tries to date 
*J. K. Simmons as Ed: Tommys sisters sarcastic, taunting husband 
*Julie Bowen  as Christy
*Kelsey Grammer  as Frank
*Ray Liotta  as Gray: Tommys old partner in crime
*Karle Warren as Alex Luboja
*Malcolm Goodwin as Rick
*Kenton Duty as Ethan Papadopolous
*Meeghan Holaway as Tina
*Jon Gries as Edgar
*Daniel Booko as Cooper Luboja
*Evelyn Iocolano as Denise
*Jeff Kueppers as Steve John Hayden as Judge Pierce
*Jean St. James as Mrs. Pierce

==Reception==
The film has received extremely negative reviews. Review aggregate Rotten Tomatoes reports that 8% of critics have given the film a positive review based on 12 reviews, with an average score of 2.6/10.    The film has been deemed "Rotten" by the aggregate. 

==References==
 

==External links==
 
* 
* 

 
 
 
 
 
 