Everything's Rosie
 
 
{{Infobox film
| name = Everythings Rosie James Dugan (assistant)
| producer = William LeBaron Louis Sarecky (associate)
| writer = 
| screenplay     = Ralph Spence  Tim Whelan  Al Boasberg 
| story          = Al Boasberg   
| starring = Robert Woolsey Anita Louise John Darrow
| music = 
| cinematography = Nicholas Musuraca   
| editing = Doris Drought 
| distributor = RKO Pictures
| released =   | ref2= }}
| runtime = 67-76 minutes 
| country = United States
| language = English
| budget =$140,000 Richard Jewel, RKO Film Grosses: 1931-1951, Historical Journal of Film Radio and Television, Vol 14 No 1, 1994 p56  
| gross = $275,000 
}}
Everythings Rosie is a 1931 slapstick comedy film directed by Clyde Bruckman, from a screenplay by Ralph Spence, Tim Whelan, and Al Boasberg, based on a story by Boasberg. Although the screenplay was credited as original, it bore a striking resemblance to a 1923 play which starred W.C. Fields, Poppy. The film starred Robert Woolsey, one-half of the comedy team of Wheeler & Woolsey, and was an attempt by RKO Radio Pictures to capitalize on the popularity of the comedy duo, having each of the team star in their own solo films. The film also starred Anita Louise and John Darrow, but was a critical failure, although it did manage not to lose money in a year when most RKO films did exactly that.

==Plot==
Dr. J. Dockweiler Droop (Robert Woolsey) is a carnival charlatan, scamming local shills out of their hard earned money.  He adopted Rosie (Anita Louise) when she was three, and has raised her to become a pretty young woman, who is just as good an operator as her adoptive father is.  As they pass through a small town, Rosie falls in love with Billy Lowe (John Darrow), and pleads with Dockweiler to leave the carnival life and settle down. Dockweiler agrees, and the two leave the carnival.

To support them, Dockweiler becomes partners with a jewelry store owner, Al Oberdorf (Alfred James), who is on the verge of bankruptcy.  Due to Dockweilers sales skills, he saves the store from failure.  He has also been spending his time convincing the gullible townspeople that he is actually a European noblemen.  While Rosie is in love with Billy, she finds out that he is engaged to a snobbish socialite, Madeline Van Dorn (Lita Chevret). Heartbroken, when Billy invites her to his birthday, she agrees to go, along with Dockweiler.  While at the party, Dockweiler decides to get back at the townspeople who have heartbroken his daughter, and runs a crooked shell game, bilking the locals of large amounts of cash. When Rosie discovers that Billy has true feelings for her, and intends to marry her, she asks Dockweiler to lose back the money he has won.  He agrees, but before the evening is out, the Sheriff (Clifford Dempsey) arrives and asks him to leave town for running a dishonest game.

Before they can leave, however, the jewelry store is robbed, and suspicion falls on Dockweiler who is arrested for the theft.  He escapes from the jail, and is leaving town with Rosie, when the Sheriff and Billy track them down to let them know that the real jewel thieves have been apprehended.  Dockweiler understands that he will never fit in with the local gentry, so, now assured of Rosies happiness with Billy, bids them adieu and departs.

==Cast==
*Robert Woolsey as Dr. J. Dockweiler Droop
*Anita Louise as Rosie Droop
*John Darrow as Billy Lowe
*Florence Roberts as Mrs. Lowe
*Clifford Dempsey as Sheriff Hopkins
*Lita Chevret as Madeline Van Dorn
*Alfred James - Al Oberdorf
*Frank Beal - Mr. Lowe
 AFI database) 

==Reception==
The film was routinely panned by critics.  Mordaunt Hall, of The New York Times came right to the point in his review, "One of the cinemas minor indiscretions, an item entitled "Everythings Rosie," was inflicted last evening on a small audience at the Globe which found it as lacking in wit as in intelligence and ordinary good taste."    Silver Screen magazine gave it a "fair" rating, stating, "Robert Woolsey without Bert Wheeler is nothing to turn cartsprings about"  Screenland was slightly more generous, saying that Woolsey in his solo performance "... tickles the customer with this one." 

The film made a slight profit of $35,000. 

==Notes== Too Many Cooks.  The lukewarm reception to both films cemented the two as a team, as those two films were the only ones either made without the other after becoming a comedy team.  

The film did achieve a milestone in the technical aspects of filmmaking, when their sound crew managed to record several natural sound effects in the field.  The sounds of leaves rustling and bird calls were successfully recorded, along with natural wind effects, while in the field at Sherwood Forest, outside Hollywood. Sound engineer Hugh McDowell, Jr. had invented the equipment, the "silencer and ground noise eliminator", which enabled the recording. 

The working title of the film was Going! Going! Gone!, but was changed to Everythings Rosie after Anita Louises characters name was chosen to be Rosie. 
 Apollo Theatre from September 1923 through June 1924.  Coincidently, Woolsey had a significant role in that play.  

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 