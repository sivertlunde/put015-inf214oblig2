Cyrus: Mind of a Serial Killer
 
{{Infobox film
| name           = Cyrus
| image          = Cyrus-mind-of-s-serial-killer.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = Mark Vadik
| producer       = Mark Vadik
| writer         = Mark Vadik
| starring       = {{Plainlist|
* Brian Krause
* Danielle Harris
* Lance Henriksen}}
| music          = Frederik Wiedmann
| cinematography = Brian Levin
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Cyrus, longer title Cyrus: Mind of a Serial Killer is a 2010 thriller horror film based on real events, written and directed by Mark Vadik and starring Brian Krause, Lance Henriksen and Danielle Harris.

==Synopsis==
A thriller film based on real events,  the film shows an independent television reporter and her cameraman, part of a small independent news crew interviewing a man in regards to a serial killer the man knew by the name of Cyrus dubbed The County Line Cannibal. The man traces back through the story of the serial killer and why he became the monster he is.

==Cast==
*Brian Krause as Cyrus
*Danielle Harris as Maria
*Lance Henriksen as Emmett
*Wylie Allen as Young Cyrus
*Patricia Belcher as Maybelle
*Sam Brilhart as Young Emmett
*Rae Dawn Chong as Vivian
*Jeff Christian as Sherriff
*Regina Daniels as Victim Girl #2
*Ben Dubash as Trooper Davis
*Zachary Christopher Fay as Joseph
*Corey Gibbons as Greg
*Andrew Gregg as Salesman Doug Jones as Dr. Albert
*Anne Leighton as Vicky
*Trevor Luce as Luke
*Mike McNamara as Rick
*Valerie Meachum as Tanny
*Aris Mendoza as Tina
*Bill Miller as	John
*Tanya Newbould as Molly
*Paul Preiss as Katz
*Kim Rhodes as Dr. Dallas
*Jill Sandmire as Amber
*Tiffany Shepis as Cyruss Mother
*Michael Steen as Warden
*Shawna Waldron as Chloe
*Tony Yalda as Tom

== References ==
 

==External links==
* 

 
 
 
 
 


 