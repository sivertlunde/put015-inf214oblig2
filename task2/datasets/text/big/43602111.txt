Hamari Betiyan
{{Infobox film
| name           = Hamari Betiyan
| image          = 
| image_size     = 
| caption        = 
| director       = R. S. Choudhary
| producer       = Ardeshir Irani
| writer         = R. S. Choudhary
| narrator       =  Kumar Rose Mubarak Pramila
| music          = Annasaheb Mainkar
| cinematography = Adi M. Irani
| editing        = 
| distributor    =
| studio         = Imperial Film Company
| released       = 1936
| runtime        = 154 min
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}} 1936 social Hindi film directed by R. S. Chowdhary.    It was produced by Ardeshir Irani under his Imperial Film Company banner.    The film starred Rose, Kumar, Pramila, Mubarak, Jilloobai, Baba Vyas and Baby Shri.    Rose co-starred with her cousin Pramila who had started acting in films from 1935 with Ardeshir Irani’s  Imperial Film Company’s Return Of Toofan Mail (1935).    The story of Hamari Betiyan is about the travails of Radha and her subsequent revenge against the people who caused her grief and anguish.

==Plot==
Radha (Rose) and Prince Madan (Kumar) are college mates who fall in love. Despite strong objection from his father the king (Baba Vyas), Madan marries Radha. Through the evil intrigues of Vasant (Pramila) and Lal Singh (Mubarak), Madan is cast out of the kingdom. Radha becomes blind and her husband abandons her. Radha meets up with her mother who had been separated from her and has now become a priestess. Radha’s sight is restored and she gets hold of a buried treasure. With that she pretends to be a princess and takes revenge against all who wronged her.

==Cast==
Cast
*Rose: Radha
*Kumar (actor)|Kumar: Prince Madan
*Pramila: Vasanti
*Mubarak (actor)|Mubarak: Lal Singh
*Baba Vyas: The King
*Jilloobai: Radha’s mother
*Baby Shri

==Songs==
*Ae Ri Nindiya Aan Baso In Nainun Mein
*Aaye Mandir More
*Dekho Leela Prabhu Ki
*Ae Mahajabin Yeh Rang Roop Tera
*Nahin Bhaye Jo Kismet Mein
*Jaag Jaag Re Murkh Banday
*Mamjanani, Shaant Sheele

==References==
 

==External links==
* 


 
 
 