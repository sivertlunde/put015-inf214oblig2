Crime of Passion (1957 film)
{{Infobox film
| name           = Crime of Passion
| image          = CrimepassionPoster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Gerd Oswald
| producer       = Herman Cohen
| screenplay     = Jo Eisinger
| narrator       = 
| starring       = Barbara Stanwyck Sterling Hayden Raymond Burr
| music          = Paul Dunlap
| cinematography = Joseph LaShelle
| editing        = Marjorie Fowler
| studio         = Robert Goldstein Productions
| distributor    = United Artists
| released       =  
| runtime        = 84 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Crime of Passion is a 1957 American crime film noir directed by Gerd Oswald and written by Jo Eisinger.  The drama features Barbara Stanwyck, Sterling Hayden and Raymond Burr.

==Plot==
Kathy Ferguson (Stanwyck) is a San Francisco newspaper advice columnist.  One day, Lieutenant Bill Doyle (Hayden), a Los Angeles police detective, and his partner, Captain Charlie Alidos (Royal Dano), track a fugitive wanted for murder to San Francisco. He meets Kathy and they fall in love. She manages to gain the female fugitives trust and locate her. Kathys resulting front page story leads to an offer of a big job in New York City, but she abandons her career, marries Doyle and moves to Los Angeles.

Her new role as a 1950s suburban wife and homemaker quickly makes her unhappy. She wants her husband to move up in the world, to become "somebody". Doyle has different values. He works in order to afford a comfortable lifestyle, no more. Kathy schemes to push her husband up the career ladder without his knowledge. She arranges to get into a car accident with Alice Pope (Fay Wray), in order to become acquainted with her husband, Police Inspector Tony Pope (Burr), head of Bills division. Pope realizes what she has done, and why, but plays along.

Her continuing ploys inevitably bring her into conflict with Sara Alidos (Virginia Grey), the captains equally ambitious wife, and Captain Alidos begins to find fault with Bill at every opportunity. Vicious rumors circulate about Kathys relationship with Tony Pope. When Bill sees a poison pen letter that Kathy has received, he rushes to work and punches his boss, Alidos, in front of two police witnesses. During the investigation, Pope shifts enough of the blame to Alidos, suggesting he reached for his gun when the visibly angry Bill burst into the room, that he can hush up the whole incident. Alidos is then transferred to another division, and Bill is given his former position as an acting homicide captain.

When Alice Pope breaks down under the years of mental strain of being a policemans wife and is hospitalized, Tony decides to retire. When Tony comes to tell Kathy about Alices breakdown and his plans to retire, Kathy tries to persuade him to recommend her husband for the vacancy his departure will create. During their talk, he seems to consider the idea favorably, then grabs and kisses her. She recoils at first, then embraces him. Afterward, however, he avoids her. When she finally forces him to meet her, he makes it clear that he believes that Bill is not qualified, and that he is going to recommend Charlie Alidos as his successor. This elevation of a man she hates over her husbands ambition infuriates Kathy.

When Kathy accompanies Bill to the police station, she steals a gun used in a robbery and murder that her husband is investigating.  Kathy then confronts Pope in his home and pleads that he at least not recommend Charlie Alidos. If Pope recommends no one, Kathy argues, her husband still has a small chance and she can justify what she did.  Pope coldly refuses so she shoots him dead.

The entire police department works on Popes murder investigation. Bill figures out the killer has to have been his own wife. When Bill confronts Kathy, she tells him, "Now Ill know just how much of a cop you really are."  Bill responds, "The same cop, Kathy.  The same cop you met in Frisco. Same cop I was 10 years ago, pounding a beat. The same cop."  He then drives her to police headquarters. Inside, before they disappear behind a door, she nods her head.

==Cast==
* Barbara Stanwyck as Kathy Ferguson Doyle
* Sterling Hayden as Police Lieutenant Bill Doyle
* Raymond Burr as Police Inspector Anthony Pope
* Fay Wray as Alice Pope
* Virginia Grey as Sara Alidos
* Royal Dano as Police Captain Charlie Alidos
* Robert Griffin as Police Sergeant Jame
* Dennis Cross as Police Sergeant Jules
* Jay Adler as Mr. Nalence
* Stuart Whitman as Laboratory Technician
* Malcolm Atterbury as Police Officer Spitz
* Robert Quarry as Sam
* Gail Bonney as Mrs. London
* Joe Conley as Delivery Boy

==Reception==

===Critical response===
Critic Dan Callahan gave the film a positive review, writing, "Hayden installs Stanwyck into a hellish suburbia where the women only talk about their TV sets; after a particularly trying montage of idle housewife chatter, Stanwyck rages against the mediocrity all around her. When she rails against her kitchen duties, shes a 30s star railing potently against 50s conformity. Though her character turns violent, the reasons behind her anger are powerfully expressed and the film puts you on her side. This overlooked, subversive movie has a strong feminist message and an even stronger Stanwyck performance." 

Critic Glenn Erickson liked the films noir screenplay and wrote, "Crime of Passion is a fascinating film that goes head-on with the classic conception of the femme fatale character. Screenwriter Jo Eisinger wrote the delirious 1946 Gilda, noirs most romantically perverse epic, but here she dissects the murderous female from a 50s perspective. Its hard-edged, direct in its theme and both dated and progressive at the same time. Barbara Stanwyck and Sterling Hayden make an exceptional screen couple." 

==References==
 

==External links==
*  
*  
*  
*  
*   informational site and film review at DVD Verdict
*  
 
 
 
 
 
 
 
 
 
 
 
 