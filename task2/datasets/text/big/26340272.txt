Rock River Renegades
{{Infobox film
| name           = Rock River Renegades
| image          = Poster of the movie Rock River Renegades.jpg
| image_size     =
| caption        =
| director       = S. Roy Luby
| producer       = George W. Weeks (producer)
| writer         = Earle Snell (writer) Faith Thomas (story) John Vlahos (writer)
| narrator       =
| starring       = See below
| music          =
| cinematography = Robert E. Cline
| editing        = S. Roy Luby
| distributor    =
| released       =
| runtime        = 56 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Rock River Renegades is a 1942 American film directed by S. Roy Luby, one of the films in the Range Busters series.

== Cast ==
*Ray Corrigan as Crash Corrigan
*John Dusty King as Dusty King
*Max Terhune as Alibi Terhune
*Elmer as Elmer, Alibis Dummy
*Christine McIntyre as Grace Ross John Elliott as Dick Ross (editor, Rock River Advocate)
*Weldon Heyburn as Jim Dawson aka Phil Sanford
*Kermit Maynard as Marshal Luke Graham Frank Ellis as Chuck (stage robber)
*Carl Mathews as Joe (stage robber)
*Richard Cramer as Ed (bartender)
*Tex Palmer as Henchman Tex

== External links ==
 
* 
* 

 
 
 
 
 
 
 
 


 