The Show (1995 film)
 
{{Infobox Film
| name           = The Show
| image          = The Show (1995 film) poster.jpg
| caption        = 
| director       = Brian Robbins
| producer       = Brian Robbins Michael Tollin
| writer         = 
| starring       = Russell Simmons
| music          = Stanley Clarke
| cinematography = Dasal Banks Larry Banks Stephen Consentino Ericson Core John L. Demps Jr. Michael Negrin John Simmons 
| editing        = Michael Schultz
| studio         = Rysher Entertainment
| distributor    = Savoy Pictures
| released       = August 20, 1995
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

The Show is a 1995 documentary film about hip hop music. It was directed by Brian Robbins and featured interviews with some of hip hops biggest names. Def Jam founder Russell Simmons stars in and narrates the film. The film grossed $1,482,892 in its opening weekend and $2,702,578 during its theatrical run.

== Cast ==
* Afrika Bambaataa
* The Notorious B.I.G.
* Kurtis Blow Sean "Puffy" Combs Snoop Doggy Dogg
* Dr. Dre
* Warren G
* Andre Harrell of Uptown Records
* Kid Capri
* LL Cool J
* Craig Mack
* Method Man
* Melle Mel
* Naughty by Nature
* Raekwon
* Run-D.M.C.
* Slick Rick
* Russell Simmons
* Tha Dogg Pound
* Twinz
* Whodini
* Wu-Tang Clan

== Soundtrack ==
 
A soundtrack consisting entirely of hip hop was released on August 15, 1995 by Def Jam Recordings. The soundtrack was very successful, peaking at 4 on the Billboard 200|Billboard 200 and 1 on the Top R&B/Hip-Hop Albums and was certified platinum on October 16, 1995.

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 


 