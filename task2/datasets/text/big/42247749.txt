Letters to a Stranger
{{Infobox film
| name               = Letters to a Stranger
| image              = Letters_to_a_stranger.jpg
| alt                = 
| caption            = Film poster
| director           = Fred Amata
| producer           = Fred Amata
| writer         = Victor Sanchez Aghahowa
| starring       =  
| music          = Tunji Awelenje
| cinematography = Jonathan Gbemuotor
| editing        = Thomas Tille
| studio         =  
| distributor   = Project Nollywood
| released       =  
| runtime        = 95 minutes
| country        = Nigeria
| language       = English
| budget         =
| gross          =
}}

Letters to a Stranger is a 2007 Nigerian romantic comedy drama film written by Victor Sanchez Aghahowa, produced and directed by Fred Amata. It stars Genevieve Nnaji, Yemi Blaq, Fred Amata, Joke Silva, Elvina Ibru and Ibinabo Fiberesima, with Special Appearances from Segun Arinze and DBanj.

It tells the story of Jemima (Genevieve Nnaji), whose love life with her boyfriend, Fredrick (Fred Amata) hasnt been on the smooth path as she expected - as Freds mother (Joke Silva) is always dictating the tune. She decides to make a call to her sister, Tare (Ibinabo Fiberesima) on a day, but she dials the wrong number which leads to her getting acquainted with Sadiq (Yemi Blaq). 

==Plot==
Jemima (Genevieve Nnaji) isnt having the best of times. Her boyfriend, Frederick (Fred Amata), is too busy with other parts of his life to give her the attention and affection she needs, and she is distracted at work. A writer, she records her feelings in a series of "letters to a stranger" on her computer. 

While she is on a months leave of absence, a chance misdial of the telephone introduces her to Sadig (Yemi Blaq). Sadiq, too, is a writer, and he seems far more affectionate towards Jemima than Frederick. Torn by her feelings for the men, Jemima plans to return Fredericks engagement ring to him, but Frederick discovers her "letters to a stranger" and learns how unhappy Jemima has been. They talk, but she cannot answer the question he asks as to why she had not left him for his neglect.

Eventually, Jemima must make a choice and tells her two suitors that she will deliver the news by telephone. On Monday morning, she will call the man she has chosen first. But when the time comes, she decides instead to begin by calling the other, to apologize and explain. When she dials Sadiq to tell him that she has decided to marry Frederick, she finds he has turned off his phone. She assumes he has given her up easily and only months later learns that this is not true. Sadiq reveals to her that he had turned off the phone because he knew what her choice would be; a gift she had given him had been addressed to Frederick. 

==Cast==
*Genevieve Nnaji as Jemima Lawal
*Yemi Blaq as Sadiq
*Fred Amata as Fredrick Okoh
*Elvina Ibru as Tare
*Ibinabo Fiberesima as Kemi
*Joke Silva as Mrs Okoh
*Eucharia Anunobi as Mrs Bankole
*Segun Arinze as Cab driver
*DBanj as DBanj
*Alex Lopez as Secretary

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 