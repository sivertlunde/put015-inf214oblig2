The Legend of Lucky Pie
{{Infobox film
| name=The Legend of Lucky Pie
| image=Legend of Lucky Pie opening title.jpg
| alt=
| caption=Opening title
| released= 
| runtime=12 minutes
| country=China
| language=Chinese
}}
The Legend of Lucky Pie ( ) is a Chinese animated short film. Produced by a group of artists without funding or a studio environment, the short was released in early January 2015. Writers for news sites observed striking similarities to the American animated television series Adventure Time, calling it a "knock-off" and a "rip-off", while some praised the animation and dedication of the producers.

==Production and release==
Lucky Pie was produced by a group of Chinese artists who, according to Wendy Zhao of Cartoon Brew, are "enthusiastic animation friends" working against "tacky" and "trite"  Chinese animated television series. While the short was produced with neither funding nor in a studio environment, the group hopes to produce more episodes.  The short was released in early January 2015 (either in the first  or second week .}}) on several Chinese video sites, such as Sina Weibo, Baidu, and Tudou. 

==Reception==
On Western-based sites, Lucky Pie was discussed first on a 4chan thread dated 12 January, and later posted to YouTube.  Writers for news sites referred to it as a "knock-off"   and a "rip-off"   of the American animated television series Adventure Time.  Zhao wrote that the response from Chinese viewers towards these accusations were split. For Cartoon Brew, Amid Amidi wrote that, while it may be an "honest-to-goodness knock-off or an elaborately produced hoax", the producers have "certainly spent a fair amount of time studying Adventure Time", and expressed curiosity towards the identity of the creators.  Steve Wolfhard, a storyboard artist on Adventure Time, expressed his approval of the short on his Twitter account, calling it "the cutest". 
 crossover between it and Adventure Time, should the former "pick up some steam". 

Chris Person of Kotaku called the short both "pretty damn cool" and "pretty solid", and although the animation was "a bit rough" compared to that of Adventure Time, considering the exhausting nature of animation, "they did alright".  A writer for Publimetro wrote that, like Adventure Time, it elicited the same response of "what did you just see?".  A writer for SDP Noticas wrote that, despite the similarities and the lack of budget, the producers gave "life" to it.  Andrew Whalen of iDigitalTimes called it "adorable". 

==Explanatory notes==
 

==References==
{{Reflist|colwidth=45em|refs=
 {{Cite news
| author=Amidi, Amid
| date=14 January 2015
| url=http://www.cartoonbrew.com/tv/is-legend-of-lucky-pie-the-chinese-knock-off-of-adventure-time-107870.html
| title=Is Legend of Lucky Pie the Chinese Knock-off of Adventure Time?
| work=Cartoon Brew
| publisher=
| accessdate=16 January 2015
| archiveurl=https://web.archive.org/web/20150116053912/http://www.cartoonbrew.com/tv/is-legend-of-lucky-pie-the-chinese-knock-off-of-adventure-time-107870.html
| archivedate=16 January 2015
}} 
 {{Cite news
| author=Anon.
| date=15 January 2015
| url=http://www.sdpnoticias.com/geek/2015/01/15/china-tiene-su-propia-version-de-adventure-time
| title=China tiene su propia versión de Adventure Time
| work=SDP Noticas
| language=Spanish
| publisher=
| accessdate=16 January 2015
| archiveurl=https://web.archive.org/web/20150116054715/http://www.sdpnoticias.com/geek/2015/01/15/china-tiene-su-propia-version-de-adventure-time
| archivedate=16 January 2015
}} 
 {{Cite news
| author=Anon.
| date=15 January 2015
| url=http://publimetro.pe/redes-sociales/noticia-youtube-mira-version-asiatica-adventure-time-30683
| title=YouTube: Mira la versión asiática de Adventure Time
| work=Publimetro
| language=Spanish
| publisher=Metro International
| accessdate=16 January 2015
| archiveurl=https://web.archive.org/web/20150116062812/http://publimetro.pe/redes-sociales/noticia-youtube-mira-version-asiatica-adventure-time-30683
| archivedate=16 January 2015
}} 
 {{Cite news
| author=Anon.
| date=16 January 2015
| url=http://www.lifeboxset.com/2015/hora-de-aventura-china-version/
| title=En China tienen su propia versión de Adventure Time y es aún más rara que la original
| work=LifeBoxset
| language=Spanish
| publisher=
| accessdate=18 January 2015
| archiveurl=https://web.archive.org/web/20150117003639/http://www.lifeboxset.com/2015/hora-de-aventura-china-version/
| archivedate=17 January 2015
}} 
 {{Cite news
| author=Belizón, Salvador
| date=16 January 2015
| url=http://www.alfabetajuega.com/noticia/hora-de-aventuras-llega-a-china-bajo-la-imitacion-the-legend-of-lucky-pie-n-47825
| title=Hora de Aventuras llega a China bajo la imitación The Legend of Lucky Pie
| work=Alfa Beta Juega
| language=Spanish
| publisher=Partners Entertainment S.L.
| accessdate=18 January 2015
| archiveurl=https://web.archive.org/web/20150117003647/http://www.alfabetajuega.com/noticia/hora-de-aventuras-llega-a-china-bajo-la-imitacion-the-legend-of-lucky-pie-n-47825
| archivedate=17 January 2015
}} 
 {{Cite news
| author=Furse, Ben
| date=15 January 2015
| url=http://moviepilot.com/posts/2015/01/15/new-chinese-adventure-time-rip-off-is-crazy-awesome-2590779
| title=  Chinese Adventure Time Rip-off Is Crazy Awesome!
| work=Moviepilot
| publisher=
| accessdate=16 January 2015
| archiveurl=https://web.archive.org/web/20150116054338/http://moviepilot.com/posts/2015/01/15/new-chinese-adventure-time-rip-off-is-crazy-awesome-2590779
| archivedate=16 January 2015
}} 
 {{Cite news
| author=Giadach, Cha
| date=16 January 2015
| url=http://www.disorder.cl/2015/01/16/hora-de-aventura-hecho-en-china/
| title=Legend of Lucky Pie el Hora de Aventura hecho en China
| work=Disorder Magazine
| language=Spanish
| publisher=
| accessdate=18 January 2015
| archiveurl=https://web.archive.org/web/20150117003729/http://www.disorder.cl/2015/01/16/hora-de-aventura-hecho-en-china/
| archivedate=17 January 2015
}} 
 {{Cite news
| author=Person, Chris
| date=15 January 2015
| url=http://kotaku.com/chinese-shows-imitation-of-adventure-time-is-actually-p-1679702484
| title=Chinese Shows Imitation of Adventure Time Is Actually Pretty OK
| work=Kotaku
| publisher=Gawker Media
| accessdate=16 January 2015
| archiveurl=https://web.archive.org/web/20150116053628/http://kotaku.com/chinese-shows-imitation-of-adventure-time-is-actually-p-1679702484
| archivedate=16 January 2015
}} 
 {{Cite news
| author=Reyes, Miguel
| date=15 January 2015
| url=http://atomix.vg/2015/01/15/esos-chinos-ahora-hasta-copian-adventure-time-y-toda-la-cosa/
| title=Esos chinos, ahora hasta copian Adventure Time y toda la cosa
| work=Atomix
| language=Spanish
| publisher=Gamers Media
| accessdate=16 January 2015
| archiveurl=https://web.archive.org/web/20150116060403/http://atomix.vg/2015/01/15/esos-chinos-ahora-hasta-copian-adventure-time-y-toda-la-cosa/
| archivedate=16 January 2015
}} 
 {{Cite news
| author=Vincent, Brittany
| date=15 January 2015
| url=http://www.nerdist.com/2015/01/surprise-this-chinese-knockoff-of-adventure-time-is-quite-good-actually/
| title=Surprise! This Chinese Knock-off of Adventure Time Is Quite Good, Actually
| work=Nerdist
| publisher=Nerdist Industries
| accessdate=16 January 2015
| archiveurl=https://web.archive.org/web/20150116060811/http://www.nerdist.com/2015/01/surprise-this-chinese-knockoff-of-adventure-time-is-quite-good-actually/
| archivedate=16 January 2015
}} 
 {{Cite news
| author=Whalen, Andrew
| date=15 January 2015
| url=http://www.idigitaltimes.com/adventure-time-chinese-rip-what-does-adorable-animation-short-have-do-gamergate-408327
| title=Adventure Time Chinese Rip-off: What Does an Adorable Animation Short Have to Do with Gamergate?
| work=iDigitalTimes
| publisher=IBT Media
| accessdate=16 January 2015
| archiveurl=https://web.archive.org/web/20150116001608/http://www.idigitaltimes.com/adventure-time-chinese-rip-what-does-adorable-animation-short-have-do-gamergate-408327
| archivedate=16 January 2015
| ref= 
}} 
}}

==Further reading==
*  

 
 
 
 
 