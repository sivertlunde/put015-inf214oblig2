Dolls (1987 film)
 

 
 
{{Infobox film
| name           = Dolls
| image          = Dollsposter1987.jpg
| caption        = Theatrical release poster
| director       = Stuart Gordon
| producer       = Charles Band, Brian Yuzna
| writer         = Ed Naha Stephen Lee Guy Rolfe Hilary Mason Ian Patrick Williams Carolyn Purdy Gordon Cassie Stuart Bunty Bailey Carrie Lorraine
| music          = Fuzzbee Morse Victor Spiegel
| cinematography = Mac Ahlberg
| editing        = Lee Percy MGM (2005, DVD) Scream Factory (under license from MGM) (2014, Blu-Ray DVD)
| released       = March 6, 1987
| runtime        = 77 min
| country        = Italy United States
| language       = English
| budget         = Unknown
| gross          = Unknown
| preceded_by    =
| followed_by    =
}} horror film directed by Stuart Gordon. The film was shot in Italy in 1985 and released in 1987.

== Plot == gothic English countryside including little Judy, whos traveling with her distant, uncaring father, David Bower, and her callous, arrogant stepmother Rosemary. They are accompanied by an amiable, mild-mannered businessman Ralph, who has given a ride to Isabel and Enid, two British punk rock girls who are hitchhiking. They all wind up at a mansion inhabited by Gabriel and Hilary Hartwicke, a kindly elderly people who appear to be toy makers; their house literally abounds with dolls, puppets, and other beautifully detailed toys. They give Judy a new doll, Mr. Punch, after she had been forced to give up her old teddy bear by her cruel stepmother before they arrived at the house.  The Hartwickes invite the stranded travelers to join them for dinner and stay as guests until the storm ends.

One by one, the overnight guests are attacked by dolls, who turn out to be  , Rosemary ends up being ambushed by the toys before leaping to her death out of the window into a mutilated mess. Her body is brought back to the bedroom, where she is found by her husband. Thinking that Ralph murdered her, David prepares to kill him.

In the meantime, Ralph accompanies Judy for her search of the first punk girl after he notices blood on the little girls slippers. They encounter the dolls, but out of fear, Ralph initially refuses to believe that they are really alive and attempts to fight his way through them. He is overwhelmed, but spared from death when Judy pleads for his life. Right afterwards, however, they are both attacked and knocked out by Judys maddened father, but before a killing blow can be struck, the dolls intervene, dragging the unconscious Ralph and Judy away while Mr. Punch distracts David. Mr. Punch is destroyed by David, who is then confronted by Gabriel and Hilary. It is revealed that Gabriel and Hilary are actually witches who see toys as the heart and soul of childhood and believe toys will be around for as long as children want them. They also believe that the bitterness adults feel can turn to love if they surrender to the goodwill that toys provide. They explain that people come to their house every now and then and spend the night. When this happens, they test their visitors to see if they can change, giving them a sporting chance to save themselves. Some people like Ralph -people who are able to see the love and respect of childhood- are saved and leave the house with a much better perspective of life. But the ones like David, Rosemary and the two punk girls who refuse to show respect to childhood and dont change their ways, never leave and have to start over and play a new role in the world of children by becoming toys. It is shown what that new role is when David is transformed into a doll to replace Mr. Punch.

The next morning, Ralph and Judy wake up, being convinced by Gabriel and Hilary that the nights events were just a dream, and receive a letter saying that David is leaving her behind because he never was a good father to her and that she will be much happier living in Boston with her mother. The letter also states that her father and stepmother are moving to another country and changing their names and have taken the two girls with them, but have left enough money behind for Judy and Ralph to buy plane tickets back to Boston. Though the letter was really written by the Hartwickles and that Ralph was a little suspicious that the letter wasnt written by David, in a twisted way, everything in it is the truth. Ralph and Judy then leave the house together after being bid farewell by Gabriel and Hilary, who even invite them to come back whenever they want and even rejoin Judy with her lost teddy. After Judy returns Teddy to the old people as a gift, she and Ralph then drive off for the airport. Judy, who has grown quite fond of Ralph, tells him that hell really like her mother and hints to him if hed like to stay with her and her mother and be Judys new father. Though Ralph doesnt answer, he seems interested in the idea.
 another family with a set of obnoxious parents suddenly arrives and their car breaks down yards from the mansion.

==Cast==
* Ian Patrick Williams as David Bower
* Carolyn Purdy Gordon as Rosemary Bower
* Carrie Lorraine as Judy Bower
* Guy Rolfe as Gabriel Hartwicke
* Hilary Mason as Hilary Hartwicke
* Bunty Bailey as Isabel
* Cassie Stuart as Enid Stephen Lee as Ralph Morris

==Cancelled sequel==
 
Stuart Gordon was, at one point, very interested in directing a sequel to this film. The initial storyline would have followed Judy and Ralph back to Boston in which Ralph would have indeed married Judys mother and they would all become a family. Until, one day Judy would receive a box sent from England which would contain the toy makers, Gabriel and Hilary, as dolls. The said sequel never happened.

==Reception==
Critical reception for Dolls has been largely positive, with the movie holding a 63% "fresh" rating at Rotten Tomatoes. Aint It Cool News reviewed the DVD, calling it "a movie that really stands above the type of film you might expect from this era, with this subject matter."  Roger Eberts review of the film was mostly negative, writing "At some point Dolls remains only an idea, a concept. It doesnt become an engine to shock and involve us," though also conceded that the film "looks good" and "the haunted house looks magnificent.   HorrorNews.nets Jeff Colebank listed the toymaking couple as one of the "13 Best Horror Movie Couples", stating that Rolfe was "the creepiest toymaker of them all".  AllRovi|Allmovies review of the film was mildly favorable, calling it "a serious-minded, lovingly-crafted modern fairy tale that only misses classic status by a few clumsy, low-budget moments." 

==Home media==
Dolls was released to DVD by MGM Home Video on September 20, 2005, as a Region 1 widescreen DVD and by the Scream Factory division of Shout! Factory (under license from MGM) on November 11th 2014 as a Region 1 widescreen Blu-Ray.

==See also==
* Killer Toys

==References==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 