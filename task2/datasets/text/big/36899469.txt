The Jewel (2011 film)
 
{{Infobox film
| name           = The Jewel
| image          = The Jewel (2011 film).jpg
| caption        = Film poster
| director       = Andrea Molaioli
| producer       = Francesca Cima
| writer         = Andrea Molaioli Ludovica Rampoldi Gabriele Romagnoli
| starring       = Toni Servillo
| music          = Teho Teardo
| cinematography = Luca Bigazzi
| editing        = 
| distributor    = 
| released       =  
| runtime        = 110 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}

The Jewel ( ) is a 2011 Italian drama film directed by Andrea Molaioli.    

==Cast==
* Toni Servillo as Ernesto Botta
* Remo Girone as Amanzio Rastelli
* Sarah Felberbaum as Laura Aliprandi
* Fausto Maria Sciarappa as Franco Schianchi
* Lino Guanciale as Filippo Magnaghi
* Vanessa Compagnucci as Barbara Magnaghi
* Lisa Galantini as Segretaria
* Renato Carpentieri as Crusco
* Gianna Paola Scaffidi as Augusta Rastelli
* Maurizio Marchetti as Giulio Fontana
* Igor Chernevich as Igor Yashenko
* Jay O. Sanders as Mr. Rothman

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 