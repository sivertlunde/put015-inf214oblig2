The Subconscious Art of Graffiti Removal
{{Infobox film
| name           = The Subconscious Art of Graffiti Removal
| image          = 
| border         = 
| alt            = 
| caption        = 
| director       = Matt McCormick
| producer       = 
| writer         = 
| starring       = Miranda July
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 2001
| runtime        = 16 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
The Subconscious Art of Graffiti Removal (USA, 2001, 16 min) is an experimental documentary directed by filmmaker Matt McCormick and narrated by Miranda July based on original ideas and concepts of Avalon Kalin, that makes the tongue-in-cheek argument that municipal efforts by Portland, Oregon to mask and erase graffiti is an important new movement in modern art stemming from the repressed artistic desires of city workers.  The film screened at such venues as the Sundance Film Festival and the Museum of Modern Art and received both critical and popular acclaim. 
 
 
 

==References==
 

 
 
 
 
 
 
 
 
 
 
 


 
 
 