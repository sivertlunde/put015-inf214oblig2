The Prince of Temple Street
 
 
 
{{Infobox film
| name           = The Prince of Temple Street
| image          = PrinceofTempleStreet.jpg
| alt            = 
| caption        = DVD cover
| film name      = {{Film name
| traditional    = 廟街十二少
| simplified     = 庙街十二少
| pinyin         = Miào Jiē Shí Èr Shǎo
| jyutping       = Miu6 Gaai1 Sap6 Ji4 Siu3}}
| director       = Jeffrey Chiang
| producer       = Liu Kin Fat
| writer         = 
| screenplay     = Yuen Sai Man Lai Man Cheuk
| story          = 
| based on       = 
| narrator       = 
| starring       = Andy Lau Joey Wong Ng Man-tat Deanie Ip
| music          = Dominic Chow
| cinematography = Ardy Lam
| editing        = Ma Chung Yiu Mui Tung Lit
| studio         = Cheung Wang Channel Film & Video Wing Fat Film Production Golden Princess Amusement
| released       =  
| runtime        = 95 minutes Hong Kong
| language       = Cantonese
| budget         = 
| gross          = HK$12,620,570
}}
 1992 Cinema Hong Kong crime drama film directed by Jeffrey Chiang and starring Andy Lau, Joey Wong, Ng Man-tat and Deanie Ip. Aside of portraying the titular protagonist in the film, Lau also briefly reprises his role as "Lee Rock", the real life-inspired protagonist from the film series of the same name.

==Plot==
Triad leader Chiu (Ray Lui) calls up a meeting hoping to find someone willing to adopt an abandoned male infant he picked up in Temple Street. During the meeting, nobody was able to stop the infant from crying until it was passed to a servants arms. He is Tong Chau Sui (Ng Man-tat), who is chosen as the adoptive father of the infant. Because there were twelve people in the meeting and everyone were the infants godparents, Chiu names the infant Tong Sap Yee (lit. "Tong Twelve"), whom everyone calls as "Prince Twelve".

On the surface, Prince Twelve (Andy Lau) seems like a favored person, but down his heart, he is lonelier than everyone else.  His adoptive father Chau Sui is a worker of a mahjong house in Temple Street while his adoptive mother Phoenix (Deanie Ip) is an ex-prostitute who claims to have abandoned his son in Temple Street on the same day Twelve was abandoned, and thus, treats Prince Twelve as her biological son.

The renowned Prince Twelve has a rival named Lap Ling (Chin Ho), who is the leader of the Kwoon Chung Gang and a person more terrible than a devil. One day, Prince Twelve meets a girl who came preaching at Temple Street named Teresa (Joey Wong). From the first moment he looked at her, he fell in love with her.

==Cast==
*Andy Lau as Prince Twelve / Lee Rock
*Joey Wong as Teresa
*Ng Man-tat as Tong Chau Sui
*Deanie Ip as Phoenix
*Chin Ho as Lap Ling
*Ray Lui as Chiu (cameo)
*Charles Heung as Lam Kong (cameo)
*Lau Siu Ming as Ho (cameo)
*Lau Kong as Ngan Tung (cameo)
*Amy Yip as Ha (cameo)
*Wong Chi Keung as Piggy (cameo)
*Kent Cheng as Cheng (cameo)
*Jamie Luk as Little Ma (cameo)
*Frankie Chan as Peter Pan
*Jameson Lam as Motel manager
*Wan Seung Lam as Kins man
*Leung Kam San as Uncle Tsuen
*Kingdom Yuen as Phoenixs hooker
*Choi Kwok Hing as Kwan
*Jeffrey Lam as Uncle Kin
*Carol Lee as Smarties girlfriend
*Danny Poon as Cheating
*Leung Kai Chi as White haired old man
*Fong Yue as Brothel Spruiker
*Ho Chi Moon
*Kong Foo Keung as Uncle Fats thug
*Choi Cho Kuen as Uncle Fats thug
*Annabelle Lau
*Tang Cheung
*Wong Kim Ban
*Lo Kwok Wai
*Lee Ka Hung

==Theme song==
*Things I Used to Hold Tight On (從前我所緊抱的)   / Me and the Dreams I Chase (我和我追逐的夢)  
**Composer: Steve Chow
**Lyricist: Calvin Poon  , Steve Chow  
**Singer: Andy Lau

==Box office==
The film grossed HK$12,620,570 at the Hong Kong box office during its theatrical run from 20 August to 3 September 1992 in Hong Kong.

==See also==
*Andy Lau filmography

==External links==
* 
*  at Hong Kong Cinemagic
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 