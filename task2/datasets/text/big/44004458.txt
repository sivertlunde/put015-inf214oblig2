Lakshaprabhu
{{Infobox film 
| name           = Lakshaprabhu
| image          =
| caption        =
| director       = P. Bhaskaran
| producer       = K. Ravindran Nair
| writer         = Malayattoor Ramakrishnan
| screenplay     = Malayattoor Ramakrishnan
| starring       = Prem Nazir Sheela Sukumari Adoor Bhasi
| music          = MS Baburaj
| cinematography = EN Balakrishnan
| editing        = Das G Venkittaraman
| studio         = General Pictures
| distributor    = General Pictures
| released       =  
| country        = India Malayalam
}}
 1968 Cinema Indian Malayalam Malayalam film, directed by P. Bhaskaran and produced by Raveendranathan Nair. The film stars Prem Nazir, Sheela, Sukumari and Adoor Bhasi in lead roles. The film had musical score by MS Baburaj.   

==Cast==
 
*Prem Nazir
*Sheela
*Sukumari
*Adoor Bhasi
*Manavalan Joseph
*Pattom Sadan
*P. J. Antony
*PK Nair
*Sankaradi
*Bhadran
*Bahadoor GK Pillai
*Jayanthi
*K. P. Ummer Khadeeja
*Master Ayyappan
*Master Sathyan Meena
*KS Parvathy
*Panicker
*Prathapan Ravikumar
*Johnson
*Padmakumar 
 

==Soundtrack==
The music was composed by MS Baburaj and lyrics was written by P. Bhaskaran. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Karayum Kadalthirayum || K. J. Yesudas || P. Bhaskaran || 
|-
| 2 || Manmadhanaam Chithrakaaran || P Jayachandran || P. Bhaskaran || 
|-
| 3 || Panamoru Ballaatha || CO Anto || P. Bhaskaran || 
|-
| 4 || Swarnavalakalitta || S Janaki || P. Bhaskaran || 
|-
| 5 || Vennilaavinenthariyaam || S Janaki || P. Bhaskaran || 
|}

==References==
 

==External links==
*  

 
 
 

 