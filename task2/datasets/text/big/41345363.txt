Ride Out for Revenge
{{Infobox film
| name           = Ride Out for Revenge
| image          = Ride Out for Revenge.jpg
| image_size     =
| caption        = 
| director       = Bernard Girard
| producer       = 
| writer         = 
| starring       = Rory Calhoun 
| music          =Leith Stevens
| cinematography = Floyd Crosby
| editing        = Leon Barsha
| distributor    = 
| released       = 1957
| runtime        = 
| country        = USA
| language       = English
}}
Ride Out for Revenge is a 1957 American Western film.

==Cast==

*Rory Calhoun: Tate
*Gloria Grahame: Amy Porter
*Lloyd Bridges: capitano George
*Joanne Gilbert: Pretty Willow
*Frank DeKova: Chief Yellow Wolf
*Vince Edwards: Chief Little Wolf
*Michael Winkelman: Billy
*Richard Shannon: Garvin
*Cyril Delevanti: predicatore 
*Iron Eyes Cody:  Cheyenne
*John Mitchum: Sergeant

==External links==
*  at TCMDB
*  at IMDB
*  at New York Times

 
 
 
 


 