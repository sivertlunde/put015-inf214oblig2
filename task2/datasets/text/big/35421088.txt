Stranded (1916 drama film)
{{infobox film
| name           = Stranded
| image          =
| imagesize      =
| caption        =
| director       = Lloyd Ingraham
| producer       = 
| writer         = Anita Loos
| starring       = De Wolf Hopper
| cinematography = Frank Urson 
| editing        =
| studio         = Fine Arts Film Company
| distributor    = Triangle Film Corporation
| released       = July 23, 1916
| runtime        = 5 reels
| country        = United States
| language       = Silent film (English intertitles)
}}
Stranded (1916 in film|1916) is a silent film drama produced by Fine Arts Film Company and distributed by Triangle Film Corporation. The film stars De Wolf Hopper with a small part by newcomer Bessie Love. 

==Preservation status==
The film is now considered a lost film.   

==Cast==
*De Wolf Hopper as H. Ulysses Watts
*Carl Stockdale as Stoner
*Frank Bennett as Hotel Proprietor
*Loyola OConnor as His Mother
*Bessie Love as The Girl

==See also==
*List of lost films

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 


 