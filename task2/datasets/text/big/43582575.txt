I'll Take My Chances
{{Infobox film
| name               = Ill Take My Chances
| image              = Ill Take My Chances poster.jpg
| alt                = 
| caption            = Theatrical release poster
| director           = Desmond Elliot
| producer           = Emem Isong   Ini Edo
| writer         = 
| story          = Emem Isong
| screenplay     = Anthony Kehinde Joseph   Bola Aduwo   Uduak Isong
| starring       =  
| music          = Victor Ehi Amedu
| cinematography = Giuseppe Pugliese
| editing        = Victor Ehi Amedu
| studio         = Royal Arts Studios   Ancient Digit Media 
| distributor   = Royal Arts Academy
| released       =  
| runtime        = 101 minutes
| country        = Nigeria
| language       =  
| budget         = 
| gross          =
}}
 dance romantic drama film, produced by Emem Isong and directed by Desmond Elliot. It stars Ini Edo and Bryan Okwara in lead roles, with Sam Loco Efe, Jide Kosoko, Ini Ikpe, Ashleigh Clark and Abiola Segun Williams in supporting roles.  The films synopsis says: "Tradition demands that a troubled young woman take the position of priestess of the land or else allow calamity to befall the village. She refuses to accept her fate and is further convinced not to heed customs call by a handsome stranger". 
 Akwa Ibom on 24 September 2011, before its general release on 24 February 2012 and was met with generally mixed to negative critical reviews. It received five nominations at the 2012 Golden Icons Academy Movie Awards, winning the award for "Best Costume", and also got three nominations at the 2012 Best of Nollywood Awards, receiving the award for "Best Edited Movie".

==Plot==
Ikechukwu (Bryan Okwara) and his girlfriend, Giselle (Ashleigh Clark) travels to Lagos from the United States to set up a dance group, they are however met with disappointments after attending several auditions without being selected. IK is posted to the village of Ikot-Uyai for his NYSC, with the hope that hell work his redeployment back to Lagos in a few weeks, but his request is however refused. Idara (Ini Edo), a village girl enjoys dancing so much; her father, Chief Ekene (Sam Loco Efe) takes her to the priest, concerned about her behaviour with the suspicion that she might be possessed. The village priestess eventually reveals that Idara must become the next priestess of the village, lest calamity would befall the entire village.

Ikechukwu (IK), now serving in a village starts attending traditional dancing sessions; he meets Idara, masked at one of the sessions and he is fascinated by how good she dances. Using a veil Idara dropped while dancing, IK meets Idara once again, but Idara is quite hostile towards him. After much persistence, Idara gives in, and becomes IKs friend. IK and Idara fall in love with each other, as IKs relationship with Giselle becomes strained.

While dancing, Idara grows unconscious and shes taken to the shrine; she gives up to fate, deciding to obey the customs and become a Priestess, but IK tries to convince her otherwise, to no avail. With the help of Giselle, IK finally gets his redeployment letter back to Lagos, and he convinces Idara to tag along, and go against the customs. Her father is strongly against the decision, while her mother only want whats best for her daughter. Idara eventually decides to follow IK to Lagos; the village priestess, along with her crew tries to hypnotize her at the car park, but IK carries Idara into the vehicle and they zoom off.

Giselle notices a difference in IKs behaviour and she soon realizes IK and Idaras unusual closeness at a dance rehearsal. IK asks that Idara become the new lead, but Giselle blatantly refuses. Giselle later catches Idara and IK kissing. A plague breaks out in Ikot-Uyai and people start dropping dead. It is believed that the gods are angry, and Idara now has to go back and obey the custom of the land for the people to be saved. Idara goes back to the village, but still cant pull through with the rituals and returns to Calabar to join the other dance crew at a major competition. The village priest prophecizes that Idara would die before 4pm of the day due to her disobedience. The prophecy however doesnt come to pass and Idaras crew wins the dance competition. It is revealed at the epilogue of the film that the deaths of the villagers was caused by the poisoned fertilizers given to the villagers by a senatorial candidate, and not due to Idaras disobedience.

==Cast==
*Ini Edo as Idara
*Bryan Okwara as Ikechukwu Okereke
*Ashleigh Clark as Giselle
*Sam Loco Efe as Chief Ekpene
*Jide Kosoko as Minister
*Ini Ikpe as Idaras mother
*Abiola Segun Williams as
*Moses Armstrong as Administrator
*Ekene Nkanga as Chief Priest

==Production== Akwa Ibom. Cross River.  The film was one of the last films shot by Sam Loco Efe, as he died before its release; it was dedicated to him as a result. 

==Release== Akwa Ibom on 24 September 2011.  Emem Isong revealed that the decision to premiere the film in Akwa Ibom is due to the fact that the film was mostly shot there and it presents the culture of the area as a backdrop of the story; so its "a show of solidarity with the good people of Akwa Ibom". September was also fixed for the premiere, because it is the month in which Akwa Ibom state was created.  The premiere featured a short documentary on the late Sam Loco Efe who starred in the film, but died before its release.   The film had its Lagos premiere on 24 February 2012, before going on a general theatrical release.  The two halls used for the film screenings at the Lagos premiere was reportedly jam packed, that some people who wanted to see the film at all costs had to stand and some sat on the cinema floors. The Ushers at the event were dressed in an Akwa Ibom native outfit.  The film was also screened across two cities in Italy, a non traditional market. 

==Reception==
===Critical reception===
The film received mixed to negative critical reviews.   commended the music and dance choreography, but describes the screenplay as "average", commenting: "I’ll Take my Chances melded fantastical sequences with quasi-vérité visuals, brisk editing and urban grime. It also addressed issues like career choices, local beliefs, politics, falling in love and Parenting, while celebrating the glorious dancing images of youth in full creative flower". He gave 3 out of 5 stars and concluded: "I’ll Take My Chances ends up been decidedly average drama but with decent dance numbers".  Wilfred Okiche of YNaija believes that the fault in the film lies within in its conception through execution, commenting: "Desmond Elliot is at the helm and while his direction is smooth and exciting at some points, the clunky screenplay defeats him. It moves at a leaden pace, does not quite know what to do with itself and becomes stuck in limbo between the both genres it attempts to conquer". While he praised visuals of the film which aptly captures the beautiful scenery of Akwa Ibom, he talked down on Ini Edos performance and concluded: "Like all movies from the Royal Arts Academy stable, an unnecessary and untidy sub-plot is introduced and it leads to an ending that is clumsily tied together. As it stands now, brilliant young filmmakers are stepping up big time and even the old ones are upping their game and reinventing themselves. For the continued relevance of Royal Arts Academy, they need to do a lot more than Ill Take My Chances. We deserve better than this – and so do they". 

===Accolades===
Ill Take My Chances received five nominations at the 2012 Golden Icons Academy Movie Awards, including "Best Motion Picture" and won the award for "Best Costume". It also got three nominations at the 2012 Best of Nollywood Awards and received the award for "Best Edited Movie".

{| class="wikitable" style="width:100%"
|+Complete list of Awards
|-
!  Award !! Category !! Recipients and nominees !! Result
|- 2012 Golden Icons Academy Movie Awards
| Best Motion Picture
| Desmond Elliot
|  
|-
| Best New Actor
| Bryan Okwara
|  
|-
| Best Film Producer
| Emem Isong
|  
|-
| Best Costume
| Angel Nwakibie, Obijie Oru
|  
|-
| Best Sound
| semiu Adewuyi
|  
|- 2012 Best of Nollywood Awards
| Most Promising Act (male)
| Bryan
|  
|-
| Best Edited Movie
| Victor Ehi Amedu
|  
|-
| Best use of Costume
| Angel Nwakibie, Obijie Oru
|  
|}

==Home Media==
Ill Take My Chances was released on DVD on 26 November 2012.  The VCD version is split in two parts. 

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 