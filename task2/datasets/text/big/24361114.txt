Janumada Jodi
{{Infobox film name           = Janumada Jodi image          = Janumada_Jodi_poster.jpg image_size     = 200px
|caption= Janumada Jodi poster director       = T. S. Nagabharana producer       = Parvathamma Rajkumar writer         = Pannalal Patel screenplay     = T. S. Nagabharana based on       =    starring  Shilpa
|music          = V. Manohar cinematography = B. C. Gowrishankar editing        = Shashikumar distributor    = Vaishnavi Combines released       = 15 November 1996 language       = Kannada country        = India
}} Kannada film Shilpa in lead roles. The story deals with the caste system.

==Plot==
 
Lovelorn village boy and girl crosses all barriers of caste and tradition to unite.

==Awards==

*Film Fare Award for Best Direction.

*Special Jury State Award.

*Best Actress Award.

*Best Music Award.

*Best Lyrics Award.

*Best Screenplay Award.

*Best Dialogues Award.

*Best Background Music Award.

*A subject for study in humanities by an American University.http: 

==SoundTrack==

{{Infobox album |
 Name = Janumada Jodi |
 Type = Soundtrack |
 Artist = V. Manohar |
 Cover = | Feature film soundtrack |
}}

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| Track # || Song || Singer(s) || Duration
|- 1 || Dehavendare O Manuja || Rajkumar (actor)|Rajkumar||   
  India club 
|- 2 || Ivanyara Maganu || Rajesh Krishnan & Manjula Gururaj ||
|- 3 || Janumada Jodi Neenu || Rajesh Krishnan & Manjula Gururaj ||
|- 4 || Kolamande Jagnamayya || L. N. Shastri ||
|- 5 || Mani Mani Mani Mani || Shivarajkumar & Manjula Gururaj ||
|- 6 || Januma Jodi Yadaru || Rajkumar (actor)|Rajkumar||
|}

==References==
 

 
 
 
 
 


 