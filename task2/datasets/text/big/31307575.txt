Singham
 
 
 
{{Infobox film
| name = Singham
| image = Singham (2011 Hindi film) Theatrical poster.jpg
| caption = Theatrical release poster
| director = Rohit Shetty
| producer = Reliance Entertainment
| writer = Sajid-Farhad  (Dialogue)
| screenplay = Yunus Sajawal Hari
| Hari
| starring = Ajay Devgan Kajal Aggarwal Prakash Raj
| music =  Ajay-Atul
| cinematography = Dudley
| editing = Steven H. Bernard
| studio = Reliance Entertainment
| distributor = Reliance Entertainment
| released =  
| runtime = 142 minutes
| country = India Marathi
| budget =     
| gross =        
}}
 Tamil blockbuster Suriya and Anushka Shetty. The film is produced under Reliance Entertainment, which co-produced the original Tamil movie. The theatrical trailer was released with Ready (2011 film)|Ready in June 2011. 

The film was released in July 2011, and received good reviews worldwide. It became a blockbuster at the box office, and one of the highest grossing movies of 2011. A sequel titled Singham Returns was released in August 2014.

==Plot==
Singham opens with an honest police officer in Goa, Rakesh Kadam (Sudhanshu Pandey), committing suicide because of false accusations of corruption by Jaikant Shikre (Prakash Raj), a don and politician in Goa running a kidnapping racket. Kadams wife Megha Kadam (Sonali Kulkarni) vows revenge.

The story moves to Shivgarh, a small village near the Goa-Maharashtra border. Bajirao Singham (Ajay Devgan), an honest Maratha police inspector like Kadam, is in charge of the Shivgarh police station. He resolves most of the problems in his town informally and without filing charge sheets, thereby gaining much reputation and love from the villagers. Gautam Bhosle ("Gotya," Sachin Khedekar) is an industrialist and a friend of Singham’s father Manikrao Singham (Govind Namdeo). He comes to Shivgarh with his wife and daughter Kavya (Kajal Aggarwal). Eventually Singham and Kavya fall in love with each other. Their courtship takes place through a series of comic events where she initially hates him and then has a change of heart when she sees his honest and simple nature.

Everything seems to run smoothly until Jaikant, who is given a conditional bail for a murder he committed, is required to travel to Shivgarh to sign the bail every fortnight. He, instead, sends one of his allies to do the formalities, much to the anger and rage of Singham who demands Jaikant sign the bail in person. Humiliated, Jaikant reaches Shivgarh but is unable to take any revenge on Singham fearing the wrath of villagers. Using his political contacts, he gets Singham transferred to Goa to take revenge.

Singham, unaware of Jaikant’s hand behind his transfer, joins Colva police station. His co-workers, Sub-Inspector Phadnis (Vineet Sharma), Sub-Inspector Abbas (Ankur Nayyar), Head Constable Savalkar (Ashok Saraf) hate Jaikant for his crimes but are unable to take any action because of Jaikant’s political powers. DSP Patkar (Murli Sharma), Singhams senior, is on Jaikants payroll and takes care in concealing and eliminating the evidence of Jaikant’s crime from the eyes of the law. Singham tries to take this to the notice of DGP Vikram Pawar (Pradeep Velankar) but it turns out to be of no use as there is no evidence against Jaikant and Patkar. The local minister Anant Narvekar (Anant Jog) does not help Singham and, in turn, he warns him to stay away from Jaikants case. Defeated Singham wants to return to his hometown but is stopped by Kavya who encourage him to fight against the evil and not run like a coward.

Being mentally tortured by Jaikant, Singham arrests Jaikant’s top lieutenant Shiva (Ashok Samarth) in a fake case of illegally smuggling alcohol. He thwarts off DSP Patkar in full view of the public when Patkar, bounded by his duties to Jaikant, tries to protect Shiva. Meanwhile, Megha Kadam, after being ridiculed by DGP Pawar and minister Narvekar for her corrupt husband’s death, seeks help from Singham to abolish the corruption charges against her husband; he obliges. Jaikant kidnaps Kavya’s younger sister for ransom. Rescuing her, Singham successfully traces the origins of the kidnapping racket to Jaikant but is unable to arrest him as he wins an election and becomes a minister of Goa.

Jaikant, sends transfer orders to Singham to go back to Shivgarh. That night at a police function organized for the officers with their family, Singham confronts the officers for not abiding to their duties and being dishonest and unfaithful to their profession by protecting Jaikant. At first, the officers disagree with Singham, but filled with guilt, the officers decide to help Singham fight Jaikant. They reach his home to kill him, with DGP Pawar and Patkar, now in support with Singham. Jaikant eventually escapes but, after running through the city, he is arrested by the police the next morning. They bring him to the police station and shoot him dead on the same chair where Inspector Rakesh Kadam shot himself. Then they threaten Shiva to change his statement. Jaikant and Minister Narvekar are proved guilty. At a media conference, DGP Pawar and Singham clear Kadam of all corruption charges.

The film ends with Singham and other police officers saluting Mrs. Kadam.

==Cast== Inspector Bajirao Singham
* Kajal Aggarwal as Kavya Bhosle
* Prakash Raj as Jaikant Shikre
* Sonali Kulkarni as Megha Kadam
* Murali Sharma as DSP Patkar
* Ashok Samarth as Shiva Naik (Jaikant Shikres right hand)
* Sachin Khedekar as Gautam Bhosle / Gotya
* Ashok Saraf as Head Constable Savalkar
* Sudhanshu Pandey as Inspector Rakesh Kadam
* Govind Namdev as Manikrao Singham
* Vineet Sharma as Sub-Inspector Phadnis
* Ankur Nayyar as Sub-Inspector Abbas
* Vijay Patkar as Havaldar Kelkar
* Suchitra Bandekar as Mrs. Bhosle
* Sana Amin Sheikh as Anjali Bhosle (Kavyas Younger Sister)
* Anant Jog as Minister Anant Narvekar

==Character Map of Remakes==
{| class="wikitable" style="width:50%;"
|-  style="background:#ccc; text-align:center;" Kempe Gowda (2011) (Kannada language|Kannada) || Shotru (2011) (Bengali language|Bengali)
|- Jeet
|- 
| Anushka Shetty || Kajal Agarwal || Ragini Dwivedi || Nusrat Jahan
|- Ravi Shankar || Supriyo Dutta
|}

==Production== Hari in 2010, the films remake rights were sold by the producers for Hindi and Kannada versions. The co-producers of the Tamil version, Reliance Big Pictures purchased the Hindi remake rights and announced in November 2010 that the version would feature Rohit Shetty as director and Ajay Devgan in the lead role.  Prakash Raj was signed on to reprise his role as the antagonist from the original, whilst reports emerged that Asin and Anushka Shetty were being considered for the female lead role.  Anushka strongly refused the offer, stating that she didnt want to work in Bollywood, and that she will never do it. She was then replaced by Kajal Aggarwal, another actress who predominantly featured in South Indian films.  

Singham went on floors in November 2010, with its scheduled cast. The first schedule began in early March 2011 with action sequences shot in Goa featuring technicians from South India.  

A scene with Prakash Raj and Ajay Devgan was shot in Goregaon in Mumbai with around 500 junior artists as villagers. Another scene was shot at Vagator in Goa for which 100 police jeeps were called.  Shooting was stalled by the Federation of Western India Cine Employees (FWICE) members who demanded  45 lakh to be paid to the workers when it was being shot in Film City in Mumbai. 

==Release==
Reliance Entertainment released Singham on 22 July 2011 in 2000 screens worldwide with 1500 prints excluding overseas.    The films DVD was released on 23 August 2011. The Delhi High Court, upon Reliance Entertainments request, issued an order to all Indian ISPs to block file sharing sites to prevent piracy of Singham. 

==Reception==

===Critical reception===
The film received positive to mixed reviews from critics. Based on 93 reviews, review aggregate site desimartini.com gave the film the verdict, "A shout out to yesteryear masala movies, Singham is an out and out action entertainer with Ajay Devgan reminding us of his old action films, while Prakash Raj gives another solid performance. If masala movies amuse you, go for it!" The sites average audience rating is 3.0/5.   

Nikhat Kazmi of The Times of India gave it four out of five stars and stated "Singham is over-the-top retro kitsch, spilling over with high-voltage stunts, slow-motion action cuts and fiery dialogues delivered in high decibels. It is meant for all those action buffs interested in time travel to the angry young 1970s and 1980s when cinema was larger-than-life and totally unrealistic. But then, retro is currently chic, isnt it?"  Komal Nahta of Koimoi gave it four and a half stars out of five stars and said "On the whole, Singham is a powerful action-emotional drama which boasts of equally powerful dialogues and absolutely power-packed performances. It’s a super-hit and will be loved by the masses and the classes, the men and the women, the young and the old, the rich and the poor. It is the kind of film which consumes the viewer and gives him the feeling that he was part of the fight against corruption! The film has immense repeat-value. Its business in Maharashtra will get a further boost because of the liberal use of Marathi in the dialogues. Kaajal Aggarwal acts with effortless ease. Her performance is good."   Suparna Sharma of The Asian Age gave it two out of five stars and stated "Singham is a primitive, archetypal genre piece, and it is a hit. Rohit Shetty taps into the sentiment of the moment – emasculation, frustration – and gratifies it. But endowing a cop with nobility doesn’t ring true, especially not when he is neither Chulbul-charming nor when the target of his anger and lashing is generic sleaze… Singham is vigilante cop let loose on all things foul. I felt fluctuating connect with Singham, but mostly he made me queasy."  Taran Adarsh of Bollywood Hungama gave the four and a half stars out of five and said "Singham pays homage to the action films of 1970s, which was known for the heroism, death-defying action sequences and pulse pounding thrills. Its an acknowledgement to one of the most successful genres of Bollywood – action movies – known for the trademark good versus evil themes and well choreographed stunts."  Saibal Chatterjee from NDTV also gave four out of five stars and said "Singham is an old-fashioned but rousing Hindi commercial film that pretty much restores one’s faith in this often-maligned brand of cinema. It has super-duper hit written all over it. No matter how dismissive you might be of films that have no space for shades of grey, chances are that Singham will disarm you, if only for a bit."  Shivesh Kumar of IndiaWeekly awarded the movie 4 out of 5 stars.    Kajal looked pretty, and delivers her city-based yet traditional girl character confidently."  Dailybhaskar gave a score of three stars out of five and said "The action takes over the romance in the film. Go for it, if you want to catch one hell of an action flick! " 

Sukanya Venkatraghavan from   said "The original wasnt the best film around but it had a few smarts, pace and fury, and worked despite its cheesy visual effects purely because of Suriya who made the corniest lines sound good. Devgn does exactly the opposite. He takes some half-decent lines (by Farhad and Sajid) and makes them sound cheesy." 
 The Pioneer describes the film as "the David Dhawan of action, or for that matter the Golmaal of fights. To keep the audience engaged all through such unending babble needs some kind of acumen which normal people do not always have and through which people like Rohit Shetty get to make a whole lot of money, if not sense." giving it seven out of ten stars.  Kunal Guha from Yahoo|Yahoo! Movies gave two stars and says that "The films assumption that mispronunciation is funny makes us endure words like honest (with a loud h), clean cheet (clean chit), noun-saans (nonsense) and sooocide (suicide). The dialogues are spouted with immense enthusiasm but the words defuse the intensity and make them seem trivial. Devgn does a fair job and conveys sufficient conviction and humility through his character. Kajal Aggarwal makes an unobjectionable debut and her eyes would surely inspire a few compliments." 

===Box office===
Singham started extremely well at single screens with occupancy around 90% and was average at multiplexes with 50%–60% occupancy,  In the first four days, the film collected  .   After five days, the earnings were around   without any drop.    The opening week gross collections were   in India and   from overseas to fetch a total opening week gross of  , thus putting the nett weekend collections at  ,   including   in Mumbai area alone. In the second week, it collected   nett to take the total two-week net collections at  .    The film is currently the sixth highest second week grosser.  The third weekend collections were estimated to be  .  After three weeks, the nett collections amounted to  .    It collected   in its fourth week bringing its collections to   in four weeks.  Singham earned   nett in India, at the end of its theatrical run. http://www.boxofficeindia.com/boxnewsdetail.php?page=shownews&articleid=6252&nCat= 

The film grossed over   worldwide.   It was given a  blockbuster verdict by Box Office India. In Mumbai, the film earned  and remains among the top five in terms of distributors share. 

==Soundtrack==
{{Infobox album
| Name = Singham
| Type = Soundtrack
| Artist = Ajay-Atul, Chandan Sharma
| Cover = Singham.jpg
| Released = 19 June 2011
| Recorded =
| Genre = Film soundtrack
| Length =
| Label = T-Series
| Producer =
}}

The music of the film is composed by Ajay-Atul with lyrics penned by Swanand Kirkire.

===Track listing===
{{Track listing
| extra_column = Singer(s)
| lyrics_credits =
| title1 = Maula Maula Richa Sharma
| lyrics1 =
| length1 = 04:04
| title2 = Saathiya Ajay Gogavale
| lyrics2 =
| length2 = 05:10
| title3 = Singham
| extra3 = Sukhwinder Singh
| lyrics3 =
| length3 = 05:50
| title4 = Maula Maula – Remix
| extra4 = Kunal Ganjawala, Richa Sharma
| lyrics4 =
| length4 = 03:44
| title5 = Saathiya – Remix
| extra5 = Shreya Ghoshal, Ajay Gogavale
| lyrics5 =
| length5 = 05:00
| title6 = Singham – Remix
| extra6 = Sukhwinder Singh
| lyrics6 =
| length6 = 03:23
}}

===Reception===
{{Album ratings
| rev1 = Bollywood Hungama
| rev1Score =   
| rev2 = Planet Bollywood
| rev2Score =   
}}
The album was panned by music critics. Joginder Tuteja of Bollywood Hungama awarded the album two stars out of five and said "Singham turns out to be a fine album though one does feel that there could have been much more than just three songs here. While Saathiyaa is the pick of the lot and has the potential to play on beyond the theatrical run of the film as well, title song "Singham" brings in the right mood despite its setting. However given the fact that the film is not quite a musical and the focus would be primarily on pushing its action flavour, the album would find it tough to make much of a mark commercially."    Tanuj Manchanda of Planet Bollywood gave the album five and a half stars citing that "Ajay-Atul produces a satisfactory soundtrack, after their first attempt in Virudh and shows their versatility in a short span of 3 songs. One looks forward to their forthcoming venture My Friend Pinto which will be their second Bollywood film as composers. Overall, the songs are decent and have the ability in them to become a success. It will help in enhancing the films narrative."   

==Controversies==

===Protests in Karnataka===
Singham had been removed from cinemas in Karnataka while some cinemas had cancelled the shows following pressure from various groups protesting against derogatory statements against the Kannadigas. Various organisations raised voices against the anti-Kannadiga dialogues in Singham and the film which was released faced problems in continuing with the shows. There was a demand to remove such scenes from the film and the filmmakers contemplated on the next course of action.

Singham commenced in Karnataka after the removal of some "objectionable" dialogues, a day after disruptions in its screening. 

==Sequel==
  Singham 2, and that it was expected to start filming soon after the release of his directorial film Chennai Express (2013). The filming went on to begin and complete, and the film was released on 15 August 2014, under the title of Singham Returns, with Ajay Devgn reprising his title role from the prequel. The sequel received mixed response from both critics and audiences, though managed to get an excellent opening at the box office, crossing the first week collections of Singham during the opening weekend itself.

== References ==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 