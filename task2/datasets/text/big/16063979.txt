Audience of One (film)
{{Infobox Film
| name           = Audience of One
| image          = Audience_Of_One_poster.jpg
| caption        = Audience of One promotional poster
| director       = Michael Jacobs
| producer       = Alec Farbman Gary Jacobs Randy Woods Michael Jacobs Zack Sanders Matthew Woods
| starring       = Richard Gazowsky
| music          = Jeff Forrest
| cinematography = Jim Granato Michael Jacobs
| editing        = Kyle Henry
| distributor    = Revolve Productions, Indiepix Films
| released       = 9 March 2007
| runtime        = 88 minutes
| country        = United States
| language       = English
}}
 documentary directed South by Southwest Film Festival in Austin, Texas.

==Overview==
The film follows the story of a   (described by him as "Star Wars meets The Ten Commandments"). The film follows him and members of his church as they go through preproduction and fly to Alberobello, Italy, for initial shooting that turns out to be marred with difficulties. After returning home, Gazowsky manages to arrange a lease of the Treasure Island film studio, but as their promised financing from German investors never materializes, they get evicted and eventually sued by the city of San Francisco for not paying their rent. The final part of the film shows Gazowsky, still determined to make his film, presenting what appears to be a global domination plan to members of his church.

==Awards== South by Southwest Film Festival - Special Jury Award Silverdocs Film Festival - Beyond Belief Award
* 2007 San Francisco DocFest - Audience Award BendFilm Festival - Special Jury Prize for Documentary  ZagrebDox Film Festival - International Critics Jury Award ZagrebDoX Film Festival - Mali pečat for Best Young Director

==Film festivals==
# Big Sky Documentary Film Festival 2008 (Missoula, United States)
# Salem Film Festival 2008 (Mass., USA)
# True/False Film Festival 2008 (Columbia, United States)
# Camden International Film Festival 2007 (Camden, United States)
# Denver International Film Festival 2007 (Denver, United States)
# Independent Film Festival of Boston 2007 (Boston, United States)
# Leeds International Film Festival 2007 (Leeds, United Kingdom)
# New Directors/New Films 2007 (New York City, United States)
# New Zealand International Film Festival 2007 (New Zealand)
# Newport International Film Festival 2007 (Newport, United States)
# Toronto After Dark Film Festival 2007 (Toronto, Canada)

==Critical reception==
James Rocchi of Cinematical called the film "a fascinating documentary -- unblinking but not inhuman, sympathetic but never afraid to ask questions", and described Richard Gazowsky, the films blustery yet relatable protagonist, as "a natural showman", stating that while "Its easy to see a sprinkling of Ed Wood-style mania in Gazowsky...hes also in service of a higher idea."  In the Austin American-Statesman, Chris Garcia agreed, describing Gazowsky as "a sanguine if irresponsible Quixote who enlists our goodwill" while also lauding director Michael Jacobs treatment of his subject as "ceaselessly engaging, scrupulously nonjudgmental".  The Boston Globes Wesley Morris likened Gazowskys struggle to that of other infamous cinematic iconoclasts, stating that "as he   seems to swell past Felliniesque portliness to Wellesian girth, he makes a comical and complex example of the conflict of religious devotion." 

Critic Ronnie Scheib of Variety praised  the documentarys intimacy, the way that Jacobs "sticks close to his subjects, eschewing disdainful distance from their cinematic pipe dreams"  and the Village Voices J. Hoberman noted that the film was a "festival favorite throughout the US".  

The films humor has been compared to film-making-gone-awry classics such as Lost in La Mancha and American Movie, while others have cited comparisons with the searching investigations of obsession in the films of Werner Herzog as well as Errol Morris idiosyncratic character studies. V.A. Musetto of the New York Post summed the film up by simply stating that "Somewhere above us, God is Watching Audience of One and chuckling. Mere mortals will be doing likewise." 

==References==
 

==External links==
*  
*  
*  
*  
*   at MySpace
*  
*  
*  
*  

 
 
 
 
 
 
 
 