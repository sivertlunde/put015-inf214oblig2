Another Dawn (film)
{{Infobox film
| name           = Another Dawn
| image          = Another Dawn.jpg
| image_size     =
| caption        =
| director       = William Dieterle
| producer       = Harry Joe Brown
| writer         = Laird Doyle based on = original story by Laird Doyle Ian Hunter
| music          = Erich Wolfgang Korngold
| cinematography =
| editing        = Ralph Dawson
| distributor    = Warner Bros.
| released       =  
| runtime        = 73 mins
| country        = United States
| language       = English
| budget         = $552,000   accessed 26 Jan 2014 
| gross          = $1,045,000 
}}
Another Dawn is a 1937 American film melodrama starring Errol Flynn and Kay Francis.

==Plot==
Colonel John Wister is in charge of a post in the British desert colony of Dickit. While on leave in England he meets and falls in love with the beautiful American Julia Ashton, whose aviator fiance died in a plane accident. Although Julia does not love John, she likes him and agrees to his marriage proposal.

John takes Julia to Dickit, where she meets Johns best friend, Captain Denny Roark, and Dennys sister, Grace, who is secretly in love with John. Denny reminds Julia of her dead fiance and the two of them fall in love. John discovers this and although he would give her a divorce he knows that she is too decent to leave him.

An uprising by local Arabs means one of the soldiers must fly a fatal bombing mission. Denny volunteers but as he is saying good bye to Julia, John flies off instead, sacrificing his life so that his best friend and wife can be together.
==Cast==
*Kay Francis as Julia Ashton Wister
*Errol Flynn as Capt. Denny Roark Ian Hunter as Col. John Wister
*Frieda Inescort as Grace Roark
*Herbert Mundin as Wilkins, Col. Wisters Orderly
*G.P. Huntley as Lord Alden
*Billy Bevan as Pte. Hawkins Clyde Cook as Sgt. Murphy Richard Powell as Pte. Henderson
*Kenneth Hunter as Sir Charles Benton
*Mary Forbes as Lady Lynda Benton
*Eily Malyon as Mrs. Farnold

==Production notes==
===Development===
Somerset Maughams play Caesars Wife was first performed in 1919 starring C. Aubrey Smith. It concerned Sir Arthur Little, a British consulate agent in Cairo, who married a 19 year old wife, Violet. Violet likes Little but falls in love with his private secretary, Ronald Perry. "CAESARS WIFE"
MAUGHAM, W SOMERSET. Play pictorial34.206 (Apr 1919): 66-79. 

The play was filmed in 1928 as Infatuation starring Corinne Griffith. Somerset Maugham Comes To Screen: Corinne Griffin Stars in Film Version Of "Caesars Wife"
The China Press (1925-1938)   20 Dec 1928: 7.  
===Development===
Warners bought the rights to Caesars Wife in late 1935. SCREEN NOTES
New York Times (1923-Current file)   01 Nov 1935: 25.   Errol Flynn had just impressed with Captain Blood and was announced as the male lead of Caesars Wife in February 1936. "His Majesty, Bunker Bean" Announced as Starring Picture for John Beal: Actor Returning to Hollywood April 1 Robert Taylor Scheduled Soon to Portray Piccadilly Jim; Olivia de Havilland Will Appear Opposite Flynn in Feature
Schallert, Edwin. Los Angeles Times (1923-Current File)   14 Feb 1936: A23.   

In March it was announced Flynn and David would co star in Another Dawn and that the film would be set in Iraq with Laird Doyle, who had written Dangerous, doing the script. Sigmund Romberg Will Prepare His Operetta, "Princess Flavia," for Screen: Picture May Be for MacDonald and Eddy Errol Flynn and Bette Davis Slated to do "Another Dawn;" Marion Talley to Change Film Type; Colbert Cinema Planned
Schallert, Edwin. Los Angeles Times (1923-Current File)   10 Mar 1936: A19.   Doyle would receive an original story credit for the film, with no attribution being given to Maugham, although the film also dealt with a love triangle between two friends and the wife of one of them in a colonial outpost.
 Ian Hunter later joined as the third star. Errol Flynn Will Again Become Pirate Chieftain in "Sea Hawk" Revival: Stalwart Favorite Faces Lively Season Jack Oakie Sought for Re-Impersonation of Comedy Character in "Street Girl;" Paramount Gets "Double or Nothing"
Schallert, Edwin. Los Angeles Times (1923-Current File)   25 May 1936: 15.   In June Warners announced the movie would be one of their "special productions" for the following year. WARNERS TO SHOW 60 FEATURE FILMS: 1936-37 Production Schedule Announced at Convention in Progress Here. GREEN PASTURES LISTED Seven Other Stage Successes to Be Screened -- Adaptation of Anthony Adverse Ready.
New York Times (1923-Current file)   04 June 1936: 27.  

Flynn insisted that Warner Bros give him three months off after the movie so he could travel to Borneo and take footage for a movie based on a story of his. The White Rajah. Flynn to Film Background of Jungle Cinema
Los Angeles Times (1923-Current File)   18 Aug 1936: A14. 

Davis then went on suspension forcing Warners to find other actors to take over her roles. Tallulah Bankhead was announced for Another Dawn but the New York Times said this casting was "subject to change without notice." Roles of Bette Davis Being Redistributed -- Ann Hardings New Play, Love From a Stranger.
New York Times (1923-Current file)   20 July 1936: 11.   

Eventually Warners decided to give the role to Kay Francis and William Dieterle was to direct. METRO PAYS $100,000 FOR FILM RIGHTS TO UNPUBLISHED BOOK: Fannie Hursts Great Laughter to Be Produced
Schallert, Edwin. Los Angeles Times (1923-Current File)   04 Sep 1936: A19.  

"I dont do much in it," said Francis. "Things just happen about me. I am just a wife who has been unfortunate in love, as usual." Kay Francis Stars in Film of Army Life: Calls Love Scenes Just Part of Days Work.
Shaffer, George. Chicago Daily Tribune (1923-1963)   20 Oct 1936: 20.  
===Shooting===
Filming took place on the Warners backlot and in Yuma, Arizona. Flynn apparently wrecked his ankle whilst playing tennis during the making of the film and required hospitalization.  Francis also missed three days filming due to toxic poisoning. JEAN FONTAINE, SISTER OF STAR, WINS "QUALITY STREET" ROLE: Charles Bickford to Play Villain for Paramount
Schallert, Edwin. Los Angeles Times (1923-Current File)   29 Oct 1936: 15.  

William Dieterle later claimed he did not want to make the film but did it as a favour to Hal Wallis.  Errol Flynn did not want to make the film and did not like working for William Dieterle. He was also negotiating with Warner Bros for a new contract and on one occasion refused to come out of his trailer. 

Filming began September 26, 1936, but Francis was exhausted after making back to back films and it was showing up in footage. Warners was interrupted when an exhausted Francis went in holiday in Europe in November 1936. FIRE SWEEPS ROOMING HOUSE
Los Angeles Times (1923-Current File)   20 Nov 1936: A1.   It resumed and was completed in February of 1937. 
 Violin Concerto he wrote some years later. Tony Thomas, Rudy Behlmer * Clifford McCarty, The Films of Errol Flynn, Citadel Press, 1969 p 57 

==Reception==
The film earned $572,000 domestically (in the US and Canada) and $473,000 overseas. 
==Follow Up==
In 1939 it was announced Flynn and Geraldine Fitzgerald would star in The Outpost adapted from Caesars Wife by Somerset Maugham and directed by Michael Curitz. SCREEN NEWS HERE AND IN HOLLYWOOD: Samuel Goldwyn to Start at Once Production of Blackout Over Europe AVIATION DRAMA TODAY Flight at Midnight to Open at Loews Criterion--Strand Will Hold Over The Old Maid Fondas New Assignment Signed for Various Casts Of Local Origin
By DOUGLAS W. CHURCHILL Special to THE NEW YORK TIMES.. New York Times (1923-Current file)   06 Sep 1939: 32.   Eventually Flynn was replaced in announcements by Cary Grant. Warners were advertising the movie was late as 1941 but it appears it was never made. Warner Bros. Stories Listed: Million Dollars Worth of Plots on Hand for Films to Be Made in 1941
Los Angeles Times (1923-Current File)   03 Jan 1941: 10.  

==References==
 

==External links==
*  at Project Guterberg
*  at IMDB
*  at TCMDB

 

 
 
 
 
 
 
 
 
 