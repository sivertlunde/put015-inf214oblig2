Nuvve Nuvve
{{Infobox film
| name           = Nuvve Nuvve
| image          = Nuvve_Nuvve.png
| imdb_id        =
| writer         = Trivikram Srinivas
| screenplay     = Trivikram Srinivas Tarun Shriya Saran Prakash Raj
| director       = Trivikram Srinivas
| producer       = Sravanthi Ravi Kishore
| distributor    =
| released       = 10 October 2002
| editing        = Sreekar Prasad Koti
| awards         =
| budget         =
| language       = Telugu
| country        = India
}}
Nuvve Nuvve  (English: Its you) is a Telugu language Indian drama film that was written and directed by Trivikram Srinivas. The film starred Tarun (actor)|Tarun, Shriya Saran, and Prakash Raj in important roles.

==Plot==
Anjali (Shriya Saran) is the daughter of a millionaire Vishwanath (Prakash Raj) who loves his daughter very much. So much that he even buys her an Icecream Parlor when his daughter asks him for an ice cream. What Vishwanath expects is a son-in-law who will never oppose him in any matter. Rishi (Tarun (actor)|Tarun) hails from a middle-class family. His father, played by Chandramohan, owns a department store. He is a good-hearted but happy-go-lucky guy. Anjali happens to join the college in which Rishi is a senior. Soon, they fall in love with each other. On one occasion, Rishi takes Anjali to Mumbai for dinner. Anjali tries to shake off the issue with her father, saying that she was with one of her friends. But unfortunately, Vishwanath calls that friend up when Anjali was missing. He tells him that Anjali went to Mumbai with someone called Rishi. This is enough for Vishwanaths suspicion to rise that Anjali has fallen in love. He asks Rishi to prove his worth and asks him in some way to earn any money so that he can support Anjali, who he has brought up in riches. When Rishi refuses to comply, he gives Rishi one crore rupees to forget his daughter. Rishi takes it and then later gives it back, insulting Viswanath, and tries to prove that his love is greater than everything and money cant buy his love. To retaliate, Vishwanath insults Rishis family by getting a beggar for marriage with Rishis sister to try to convey to them the pain he must feel. Rishi then creates a scene in Viswanaths office beating up security guy and destroying computers in the office, Rishi clarifies that in the proposal that beggar be married to Rishis sister there is no love between the two, but Rishi and Anjali love each other. Anjali starts to show more feelings for Rishi. Worried, Vishwanath tries to set Anjalis marriage with another guy. Anjali meets Rishi at his home and requests him to marry her right at the moment, but Rishi convinces Anjali that this is not the right way. Rishi takes her back to her home, there Rishi challenges Vishwanath that if it is correct that Anjali getting married to some one else then that marriage cant be stopped, but if it is wrong then that marriage cannot happen. Through some more incidents that Vishwanath realizes his mistakes and the arranged marriage fails and Anjali marries Rishi.

==Cast== Tarun as Rishi
* Shriya Saran as Anjali
* Prakash Raj as Viswanath Sunil as Pandu Chandra Mohan
* Rajeev Kanakala
* Anitha Chowdary
* M. S. Narayana Pragathi

==DVD==
The DVD version of the film was released by KAD Entertainment on 10 October 2003, with special features and interviews.

Copyright holder is KAD Entertainment.

==Soundtrack==
{{Track listing
| collapsed       =
| headline        =
| extra_column    = Singers
| total_length    =

| all_writing     = Sirivennela
| Koti

| writing_credits =
| lyrics_credits  =
| music_credits   =

| title1          = Computerlu
| note1           =
| writer1         =
| lyrics1         =
| music1          =
| extra1          = Devan, Anuradha Sriram 
| length1         = 05:23

| title2          = Naa Mansakemayyindi
| note2           =
| writer2         =
| lyrics2         =
| music2          =
| extra2          = Udit Narayan, Nitya Santoshini
| length2         = 04:47

| title3          = Nuvve Nuvve Kavalantundhi
| note3           =
| writer3         =
| lyrics3         =
| music3          = Chitra
| length3         = 04:47

| title4          = I Am Very Sorry
| note4           =
| writer4         =
| lyrics4         =
| music4          = Kay Kay
| length4         = 05:43

| title5          = Ammayi Nachhesindi
| note5           =
| writer5         =
| lyrics5         =
| music5          = Kousalya
| length5         = 05:31

| title6          = Niddura Potunna
| note6           =
| writer6         =
| lyrics6         =
| music6          =
| extra6          = Shankar Mahadevan
| length6         = 04:32
}}

==Awards==
;Filmfare Awards South Best Supporting Actor - Prakash Raj 

;Nandi Awards   Best Second Feature Film - Sravanthi Ravi Kishore Best Dialogue Writer - Trivikram Srinivas

==Trivia==
* Shriya and Prakash Rajs performance are the highlights of the movie.
* Shriyas performance was praised by Critics adding that nobody can act as Shriya in this movie.
* This film was critical and commercially success.
* Actress Madhumitha played supporting role as Shriyas friend

==References==
 

==External links==
*  

 
 

 
 
 
 
 