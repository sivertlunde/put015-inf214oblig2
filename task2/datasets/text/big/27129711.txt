Suckers (film)
  2001 film written by Roger Nygard and Joe Yanetty which revolves around events at a car dealership. The film stars Daniel Benzali, Lori Loughlin, and Louis Mandylor and co-stars Walter Emanuel Jones.  It was filmed in 1999 but was not released until 2001, and has never been released in the United States, although region one DVD formats are available in Australia and distributed by Creative Light Entertainment.  The film is a cult-classic and is especially popular with those in the automobile retail sales trade.  Due to this the film ranks in the top 250 selling DVDs at CD Universe and is also very often sold on eBay. 
 Boiler Room.  During the scene Reggie both preaches to his sales staff and is shown selling a vehicle to a "sucker" customer.

==Plot==
Bobby DeLuca, (Mandylor) who is a general good guy who finds himself in a lot of trouble with some loan sharks, is being pressured by his wife, Donna (Loughlin) to find a job.  Desperate for fast money, he reluctantly becomes a salesman at South Side Cars, where Donna works in the office.

Donnas boss, and now Bobbys new boss is Reggie, who trains his team to suck every possible cent from their clients, and in fact fires a top salesman for throwing in floor mats on an otherwise profitable deal.  Soon after starting, Bobby becomes a top salesman, and uses his demonstrator vehicle and a potential $10,000 bonus to buy time from the loan sharks.

Among the events taking place during the film are a salesman being seduced and ultimately robbed of the test drive vehicle by a woman who drives off naked and forces the salesman to walk back to the dealership in his underwear, and the tardiness issues of Sean, (often mistaken to have been played by Seth Green).  There also is a fight on the showroom between Kevin, a black salesman, and Mohammed, a middle-eastern salesman who is estranged from his sister.  These events all happen under the nose of Reggie, who himself, along with Kevin and his cousin (Jones), are dealing in smuggling contraband.  The mix of these events and Bobbys dealings with the loan sharks result in a shoot-out at the climax of the film resulting in several of the main characters deaths.

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 