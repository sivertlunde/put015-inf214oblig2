Fallen Champ: The Untold Story of Mike Tyson
 
Fallen Champ: The Untold Story of Mike Tyson is a 1993 film made by acclaimed American documentary filmmaker Barbara Kopple.  

Though Tyson was in jail serving a sentence for rape, Kopple used existing interviews with the boxer, as well as her own extensive interviews with those closest to Tyson, to explore the mans history. The film traces Tysons story from his troubled and tumultuous upbringing, through his rapid ascendancy in the ranks of the boxing world and his subsequent struggle with the trappings of fame. Fallen Champ earned Barbara Kopple a Directors Guild of America (DGA) award as Best Documentary Director of 1993.

==External links==
*  

 

 
 
 
 
 
 
 


 