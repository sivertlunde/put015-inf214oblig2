Fierce People (film)
{{Infobox film
| name           = Fierce People
| image          = Fierce people ver2.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Griffin Dunne Nick Wechsler Dirk Wittenborn
| screenplay     = Dirk Wittenborn
| based on       =   Chris Evans Kristen Stewart
| music          = Nick Laird-Clowes
| cinematography = William Rexer
| editing        = Allyson C. Johnson
| studio         = Industry Entertainment Lions Gate Films Autonomous Films
| released       =  
| runtime        = 111 minutes
| country        = United States Canada
| language       = English Tagalog
| budget         = 
| gross          = $269,755 
}} independent Drama drama Thriller thriller film adapted by Chris Evans, and Donald Sutherland.

==Plot== anthropologist father whom hes never met. Finns plan has to change after he is arrested when he buys drugs for his mother, Lower East Side Liz (Diane Lane), who works as a massage therapist. Determined to get their lives back on track, Liz moves the two of them into a guesthouse for the summer on the country estate of her ex-client, the aging billionaire, Ogden C. Osbourne (Donald Sutherland).

In Osbournes world of privilege and power, Finn and Liz encounter the super rich, a tribe portrayed as fiercer and more mysterious than anything the teenager might find in the South American jungle. (Dirk Wittenborn, the author of the novel on which the film is based, grew up in a modest household and felt like an outsider among the super rich in an upper-crust New Jersey enclave. )
 Chris Evans); and wins the favor of Osbourne. When violence ends Finns acceptance within the Osbourne clan, the promises of this world quickly sour. Both Finn and Liz, caught in a harrowing struggle for their dignity, discover that membership in a group comes at a steep price.

==Cast==
* Diane Lane as Liz Earl
* Anton Yelchin as Finn Earl
* Donald Sutherland as Ogden C. Osbourne Chris Evans as Bryce Langley
* Kristen Stewart as Maya Langley
* Paz de la Huerta as Jilly
* Blu Mankuma as Detective Gates
* Elizabeth Perkins as Mrs. Langley
* Christopher Shyer as Dr. Richard "Dick" Leffler Garry Chalk as McCallum Ryan McDonald as Ian
* Dexter Bell as Marcus Gates
* Kaleigh Day as Paige
* Aaron Brooks as Giacomo
* Teach Grant as Dwayne
* Dirk Wittenborn as Fox Blanchard
* Eddie Rosales as Iskanani shaman
* Will Lyman as Voice of documentary narrator

==Production==
Portions of the film were shot on location in British Columbia, Canada at Hatley Castle. 

==Release==

===Critical reception===
Fierce People earned negative reviews.  

The film currently holds a 24% Rotten rating on Rotten Tomatoes, with the consensus, "Fierce People s premise of a teenager studying rich people like animals is grating and self-satisfied, and Anton Yelchins smug performance makes the film even harder to agree with." 

===Box office===
The film received a limited release and grossed $85,410 at the box office in the US. 

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 