Felix the Cat Trifles with Time
 

{{Infobox Hollywood cartoon|
| cartoon_name = Felix the Cat Trifles with Time
| series = Felix the Cat
| image = 
| caption = 
| director = Pat Sullivan
| story_artist = 
| animator = Otto Messmer
| voice_actor = 
| musician = 
| producer = E. W. Hammons
| studio = Pat Sullivan Studios
| distributor = Bijou Films, Inc. Educational Pictures
| release_date = August 23, 1925
| color_process = Black and white
| runtime = 7:51 English
| preceded_by = Felix Dopes It out
| followed_by = Felix the Cat Busts into Business
}}
 Pat Sullivan Studios, featuring Felix the Cat. Produced by E. W. Hammons, it featured the work of the animator Otto Messmer. It was the first cartoon of the series to be distributed by Educational Pictures. 

==Production history== MJ Winkler Productions concluded, the animation staff made the transit to Educational Pictures. This new company would become the series largest distributor.

==Plot==
Felix is a hungry cat in the city, looking for food remnants in a trash bin. Finding nothing inside, he heads toward a nearby apartment building. To his amazement, he finds roast chicken hanging next to an upper window. To reach it, he sneaks into a musicians tuba which then propels him upward. Felix gets his hands on the meat, only to have himself grabbed and thrown back down by a resident.

Felix is walking in an open field, still wondering how to obtain some food. Suddenly spotting Father Time standing by, Felix asks the old time master to put him in a period that may do well for him, just for a day. Father Time at first declines, but agrees when Felix offers a silver dollar. The time wizard brings out a wand a grants Felix his wish.

Father Time sends him into was the pre-historic era where cavemen and dinosaurs were common. Looking for something to eat, Felix picks up a large bone, only to be chased by a giant lizard.

After outrunning another dinosaur, Felix finds himself in front of a pre-historic tailor shop. Inside the shop, a caveman is looking for a suitable garment but appears uninterested in the few clothes the tailor shows him. The tailor steps outside and notices Felix. Finding the cats pelt as a good material, the tailor lures him into the shop, where a frenzy happens. The caveman then emerges wearing a black garment with a long tail on it, and Felix shows up almost entirely bones from foot to neck. The caveman removes the outfit to go for a swim in the lake. Felix uses this opportunity to get back his pelt. He continues wandering until he comes across a mastodon.

Father Time has been napping on the ground until his alarm clock rings. He wakes and finds it is time to return Felix to the present. Felix is still having trouble with the mastodon but the time wizards magic finally takes effect and Felix is at last sent back to his own period. Though he has to return to searching trash cans again, he figures it is better than his experience of the pre-historic era.

==Sound track==
The song "Tears" by King Olivers Creole Jazz Band is featured in the soundtrack.

==References==
 

==External links==
*  at the Big Cartoon Database
* 

 
 
 
 
 
 
 
 
 
 
 
 
 