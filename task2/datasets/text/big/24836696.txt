Chnam Oun 16 (1973 film)
{{Infobox film| name           = Chnam Oun 16 (1973)
| image          = Chhnam-one-16.png
| director       = 
| producer       = Chea Yuthon
| writer         =
| starring       = Chea Yuthon Saom Vansodany
| music          = Sinn Sisamouth
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 1973
| runtime        = 
| country        = Cambodia
| language       = Khmer
| budget         = 
| gross          = 
| followed_by    = 
}}
Chnam Oun 16  is a 1973 Khmer film starring Chea Yuthon and Saom Vansodany. Shortly after the film was released, the public found Chea Yuthon and saom Vansodany engaged.

==Cast==
*Chea Yuthon
*Saom Vansodany

==Soundtrack==
 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Song !! Singer(s)!! Notes
|-
| Cham 10 Kae Teat Sen
| Ros Serey Sothear
| 
|-
| Im So Shy
| Ros Serey Sothear
| 
|-
| Chnam Oun 16 or Sweet 16
| Ros Serey Sothear 
| 
|}

==References==
http://translate.google.ca/translate?u=http://nostalgie-films-khmers-avant-1975.kazeo.com/&sl=fr&tl=en&hl=en&ie=UTF-8

 
 
 
 


 