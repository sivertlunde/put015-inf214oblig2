Exodus: A Journey to the Mountain of God
{{Infobox film
| name           = Exodus: A Journey to the Mountain of God
| image          = Tablets of the Law Rock Art.jpg
| caption        =Prehistoric rock art possibly depicting the Tablets of the Law (The Ten Commandments)
| director       = Alon Bar   Eitan Bin Noun
| writer          = Alon Bar
| producer       = Alon Bar   Eitan Bin Noun
| music          = Itai Galed
| cinematography = Shlomo Avital
| editing        = David Mandil
| Narration    = Will Setz
| Scientific Consultant    = Emmanuel Anati
| released       =  
| runtime        = 52 minutes
| country        = Israel
| language       = English Hebrew
}} Mount Karkom Exodus took expedition route by the crew and on camel-back.

The film was co-directed by Alon Bar and Eitan Bin Noun and written by Bar. It premiered at the Casablanca International Film Festival, Morocco on November 24, 1993, and was the first Israeli film ever to be shown in a film festival in an Arab country, and the first time that Israeli filmmakers officially took part in such an event.

== Awards ==
*1995 Best  
*1993 Grand Prix Coronne DOr at the Casablanca international film festival, Morocco
*1993 Best Production Award by the Israeli Film Institute, Israel

==MedFilm Festival Jury Citation==
BEST DOCUMENTARY AWARD goes to the film "Exodus - a journey to the mountain of God" by Alon Bar & Eitan Bin-Noun. The 52-minute long documentary film tells about the journey of an expedition of scientists towards Mt. Sinai, on the path of the Biblical Exodus. It is a scientific and archeological trip, but, at the same time, it tells the story of a human spirit, of the passage from slavery in Egypt to freedom, and to the Promised Land. The young directors Alon Bar and Eitan Bin-Noun carried out the shooting with natural, emotional participation and great scientific preparation, producing a film that is not only centered on the archaeology of objects and rock art, but also on anthropological elements, which underlines the psychology and living habits of the desert people".

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 


 
 