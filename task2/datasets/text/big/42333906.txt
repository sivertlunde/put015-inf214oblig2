Aandhiyan (1952 film)
 
{{Infobox film
| name           = Aandhiyan
| image          = 
| alt            = 
| caption        = 
| film name      =  Chetan Anand
| producer       = Navketan Films 
| writer         =
| screenplay     = Chetan Anand
| story          = Chetan Anand Hameed Butt
| based on       =  
| narrator       = 
| starring       = Dev Anand Nimmi Kalpana Kartik
| music          = Ali Akbar Khan Narendra Sharma  (lyrics) 
| cinematography = Jal Mistry
| editing        = M. D. Jadhav
| studio         = 
| distributor    = 
| released       = 1952 
| runtime        = 96 min.
| country        = India Hindi
| budget         = 
| gross          = 
}}
 1952 Hindi Hindi drama Chetan Anand. The story was written by Chetan Anand and Hameed Butt based on an actual event in Amritsar.   It starred Dev Anand, Nimmi and Kalpana Kartik in lead roles.  Music of the film was given classical musician, Ali Akbar Khan with lyrics by Narendra Sharma.   

Jaidev who was a student of Khan from Lucknow, and later became a noted music director, started his career by assisting Khan in film music.      The background score of the film was also done by Ali Akbar Khan along with other Hindustani classical musicians Pandit Ravi Shankar and Pannalal Ghosh.   Lata Mangeshkar sang the title song, "Har Kahin Pe Shaadmani" and as a token of her respect to sarod maestro, did not charge any fee.    Dances were choreographed by Lakshmi Shankar, who also sang a song, while Gopi Krishna (dancer)| Gopi Krishans chorographed his own dances.  

==Cast==
* Dev Anand as Ram Mohan
* Nimmi 
* Kalpana Kartik as Janaki
* Durga Khote
* K. N. Singh as Kuber Das
* Leela Mishra
* Pratima Devi
* M. A. Latif Johnny Walker

==Soundtrack==

{{tracklist
| headline = Aandhiyan
| extra_column = Singer(s)
| total_length = 
| all_lyrics = Narendra Sharma
| all_music = Ali Akbar Khan  
| title1 = Woh Chand Nahin Hai Dil Hai 
| extra1 = Hemant Kumar, Asha Bhosle 
| length1 = 3:19
|title2=Dil Ka Khazana Khol Diya
|extra2= Asha Bhosle 
|length2=3:10
|title3=Hai Kahin Par Shaadmani 
|extra3=Lata Mangeshkar
|length3=03:11
|title4=Main Mubarakbaad Dene Aai Hoon
|extra4=Surinder Kaur
|length4=03:10
}}

== References ==
 

==External links==
 
*  
*  

 
 
 
 
 
 