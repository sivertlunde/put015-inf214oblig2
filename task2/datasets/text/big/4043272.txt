The Gong Show Movie
{{Infobox film
| name           = The Gong Show Movie
| image          = Poster of the movie The Gong Show Movie.jpg
| image_size     = 
| caption        = 
| director       = Chuck Barris
| producer       = Budd Granoff Robert Downey
| starring       = Chuck Barris Robin Altman
| music          = Milton DeLugg
| cinematography = Richard C. Glouner
| editing        = Jacqueline Cambas James Mitchell Sam Vitale
| distributor    = Universal Pictures
| released       =  
| runtime        = 89 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $6,621,520 
}}

The Gong Show Movie is a 1980 film directed by Chuck Barris.   

==Plot==
The film shows a fictional week in the life of Chuck Barris as the host and creator of The Gong Show, through a series of outrageous competitors, stressful situations, a nervous breakdown (which compels him to run away and hide in the desert) and other comic hijinks in his life and work on the TV show. Among the highlights included a group of men dressed as a Roman Catholic priest and three nuns lip-synching Tom Lehrers song "The Vatican Rag", a man blowing out a candle with flatulence, and the uncensored version of Jaye P. Morgans infamous breast-baring incident.

==Reception==
The film failed at the box office. Among the bad reviews at the time, George Burns, after seeing the movie, went on the record and said, "For the first time in 65 years, I wanted to get out of show business". 

==References==
  

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 


 