Léolo
{{Infobox film |
  name     = Léolo |
  image          =|
    writer         = Jean-Claude Lauzon |
  starring       = Gilbert Sicotte Maxime Collin Ginette Reno Julien Guiomar |
  director       = Jean-Claude Lauzon |
  producer       = Aimée Danis Lyse Lafontaine |
  distributor       = |
  released   = September, 1992 |
  runtime        = 107 minutes |
  country = Canada |
  language = French |
  budget         = |
}}
Léolo is a 1992 film by Quebecois director Jean-Claude Lauzon.
 Italian tomato, he rechristens himself Léolo Lozone, and begins to have sexual fantasies about his neighbour Bianca (Giuditta del Vecchio). 

Gilbert Sicotte, as the adult Léolo, narrates the film. The cast also includes Pierre Bourgault, Andrée Lachapelle, Denys Arcand, Julien Guiomar and Germain Houde.
 plane crash in 1997 while working on his next project.

==Cast==
* Gilbert Sicotte - Narrator (voice)
* Maxime Collin - Leolo
* Ginette Reno - Mother
* Julien Guiomar - Grandfather
* Pierre Bourgault - Word Tamer
* Giuditta Del Vecchio - Bianca
* Andrée Lachapelle - Psychiatrist
* Denys Arcand - Director
* Germain Houde - Teacher
* Yves Montmarquette - Fernand
* Lorne Brass - Fernands Enemy
* Roland Blouin - Father
* Geneviève Samson - Rita
* Marie-Hélène Montpetit - Nanette
* Francis St-Onge - Leolo, age 6

==Awards==
The film was entered into the 1992 Cannes Film Festival.   

In 2005, Time magazine|TIME named Léolo one of Times All-TIME 100 Movies.  In 2015, the Toronto International Film Festival placed it in the Top 10 Canadian Films of All Time. 

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 

 
 