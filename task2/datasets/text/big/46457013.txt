Dorothea Angermann
{{Infobox film
| name = Dorothea Angermann
| image =
| image_size =
| caption =
| director = Robert Siodmak
| producer =  Utz Utermann
| writer =  Gerhart Hauptmann  (play)   Herbert Reinecker
| narrator =
| starring = Ruth Leuwerik   Bert Sotlar   Alfred Schieske
| music = Siegfried Franz   
| cinematography = Georg Krause   
| editing =     Walter Boos 
| studio = Divina-Film
| distributor = Gloria Film
| released =22 January 1959 
| runtime = 106 minutes
| country = West Germany  German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Dorothea Angermann is a 1959 West German drama film directed by Robert Siodmak and starring Ruth Leuwerik, Bert Sotlar and Alfred Schieske.  A young woman marries the man who has got her pregnant, but he is brutal and unpleasant. When he is found dead she is suspected of his murder.

==Cast==
* Ruth Leuwerik as Dorothea Angermann  
* Bert Sotlar as Michael Sever  
* Alfred Schieske as Pastor Angermann  
* Kurt Meisel as Mario Malloneck 
* Edith Schultze-Westrum as Frau Lüders  
* Alfred Balthoff as Weiss  
* Monika John as Rosa  
* Ursula Herwig as Irene  
* Ernst Konstantin as Gerichtsvorsitzender  
* Holger Hagen as Verteidiger  
* Heliane Bei as Wally  
* Claudia Gerstäcker as Irmgard  
* Wilmut Borell as Staatsanwalt  
* Walter Sedlmayr as Willi  
* Karl Lieffen

== References ==
 

== Bibliography ==
* Alpi, Deborah Lazaroff. Robert Siodmak: A Biography. McFarland, 1998.

== External links ==
 

 

 
 
 
 
 
 
 

 