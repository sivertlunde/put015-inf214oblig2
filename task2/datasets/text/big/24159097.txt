Our Time (film)
{{Infobox film
| name           = Our Time
| image_size     =
| image	         = Our Time FilmPoster.jpeg
| caption        =
| director       = Peter Hyams
| writer         = Peter Hyams Jane C. Stanton
| narrator       =
| starring       = Pamela Sue Martin Parker Stevenson Betsy Slade
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| website        =
| amg_id         =
}}
Our Time (also known as Death of Her Innocence) is a 1974 film directed by Peter Hyams. The film was written by Hyams and Jane Stratten, and stars Pamela Sue Martin, Parker Stevenson and Betsy Slade. The story is set in a New England school for girls in the 1950s. Though not a hit, it was liked as a study of a boy-girl relationship.

==Plot==
Penfield was a girls school in 1955. The curriculum ranged from Latin to Etiquette, from Shakespeare
to Field Hockey. Abigail is one of the new girls coming in to learn about these and other subjects. Coming from a well to do family, she hopes to be the schools best student. Just as she is getting started in the school, she meets Michael, a male student in the school. Soon the two fall deeply in love, but their relationship becomes challenged by those around them, leading to difficulties and tragedy.

==Production==
Peter Hyams had previously made Busting, an R-rated movie about vice cops which had not performed well commercially. With Our Time, "I was trying to do the opposite of what I had done before," he says.   accessed 27 July 2014  Upon viewing the film, critics took note of the bad cinematography, particularly the repeated instances of seeing sound and lighting equipment in many shots.

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 


 