Shiver My Timbers
 
{{Infobox film
| name           = Shiver My Timbers
| image          = Shiver my timbers.JPEG
| caption        = Title card
| director       = Robert F. McGowan
| producer       = Robert F. McGowan Hal Roach
| writer         = H. M. Walker
| starring       = Billy Gilbert June Marlowe
| music          = Leroy Shield Marvin Hatley
| cinematography = Art Lloyd
| editing        = Richard C. Currier MGM
| released       =  
| runtime        = 20:45
| country        = United States
| language       = English
| budget         = 
}}
 short comedy film directed by Robert F. McGowan.    It was the 109th (21st talking) Our Gang short that was released.

==Plot==
A loud sea captain (Billy Gilbert) tells violent stories about adventures out on the sea as pirates. The gang is playing hookey from school in order to hear his stories. Miss Crabtree (June Marlowe) finds where they are and decides to team up with the sea captain to teach the kids a lesson and scare them from ever wanting to be pirates.

The sea captain invites the gang back that night to become pirates. When they board the ship, the sea captain puts on a show and scares the kids. He acts mean and pretends to be sending other pirates overboard. Miss Crabtree even is there and pretending that she would be next to walk the plank. The gang then decides they want to go back to school and take the sea captain seriously.

However, during a staged "raid" on their ship, the children turn the tables on the crewmen.

==Cast==
===The Gang===
* Sherwood Bailey as Spud Matthew Beard as Stymie
* Dorothy DeBorba as Dorothy
* Bobby Hutchins as Wheezer
* Georgie Ernest as Georgie
* Jerry Tucker as Jerry
* Pete the Pup as Himself

===Additional cast===
* Carlena Beard as Stymies sister
* Harry Bernard as Cook
* Dick Gilbert as Dick, one of the crew
* Billy Gilbert as The sea captain Jack Hill as One of the crew
* June Marlowe as Miss Crabtree
* Charles Oelze as Crew member with funny glasses
* Cy Slocum as Cy, one of the crew

==Memorable quote==
According to Leonard Maltin, one of the most memorable lines in the history of the movies was spoken in this short. Gilbert has called two of his crew over for mock abuse to scare the kids. He bellows at Harry Bernard, "What are you two doing over there?" Bernard answers, "We dont know!" 

==Notes== Cabin Fever.

==See also==
* Our Gang filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 