National Lampoon's Christmas Vacation
{{Infobox film
| name           = National Lampoons  Christmas Vacation
| image          = NationalLampoonsChristmasVacationPoster.JPG
| caption        = Theatrical release poster Jeremiah Chechik John Hughes Tom Jacobson
| writer         = John Hughes
| starring       = Chevy Chase Beverly DAngelo Randy Quaid
| music          = Angelo Badalamenti
| cinematography = Thomas E. Ackerman Jerry Greenberg Michael A. Stevenson John Hughes Entertainment
| distributor    = Warner Bros.
| released       =  
| runtime        = 97 minutes
| country        = United States
| language       = English
| budget         = $25 million 
| gross          = $71.3 million
}}
 National Lampoons John Hughes, based on his short story in National Lampoon (magazine)|National Lampoon magazine, "Christmas 59". The film stars Chevy Chase, Beverly DAngelo and Randy Quaid, with Juliette Lewis and Johnny Galecki as the Griswold children Audrey and Rusty, respectively.

Since its release in 1989, Christmas Vacation has often been labeled as a modern Christmas classic.

The film has achieved three home video releases: VHS and Laserdisc in early 1990, and a "Special Edition" DVD in 2003. In 2009, the film was released as an "Ultimate Collectors Edition." At the same time of this release, it was also released on a simple Blu-ray/DVD combo.   

==Plot==
With Christmas only a few weeks away, Chicago resident Clark Griswold (Chevy Chase) decides it is time to get a Christmas tree. He gathers his wife Ellen (Beverly DAngelo), daughter Audrey (Juliette Lewis) and son Rusty (Johnny Galecki) and drives out to the country where he picks out a huge tree. Realizing too late that they didnt bring any tools to cut the tree down, they are forced to uproot it instead.
 power shortage RV they drove to reach Chicago, as he is broke and has been forced to sell his home. Clark offers to buy gifts for Eddies kids, to help them have a good Christmas.

With Christmas approaching quickly, Clark begins to wonder why his boss, Frank Shirley (Brian Doyle-Murray), has not given him his yearly bonus, which he desperately needs to replace an advance payment he has made to install a swimming pool. After a disastrous Christmas Eve dinner, he finally receives an envelope from a company messenger, who had overlooked it the day before. Instead of the presumed bonus, the envelope contains a free years membership for the Jelly of the Month Club. This prompts Clark to snap and go into a tirade about Frank, and out of anger, requests that Frank be delivered to the house so Clark can insult him to his face.

Eddie takes the request literally, drives to Franks mansion, and forcibly brings him back. Clark is shocked at this development, but confronts Frank about the cancellation of the employees Christmas bonuses. Meanwhile, Franks wife calls the police, and a SWAT team storms the Griswold house and holds everyone at gunpoint. Frank decides not to press charges and explains the situation to his wife and the authorities, who both scold him for his decision to scrap the bonuses, and decides to reinstate them (with Clark getting an extra 20%).
 William Hickey) says the light is coming from the sewage treatment plant; Clark is reminded of an earlier incident where Eddie had been dumping sewage into a storm drain. But before he can stop him, Uncle Lewis tosses a match he had used to light his cigar aside, triggering an explosion sending him flying into the family. Lewis wife Aunt Bethany (Mae Questel), who is utterly senile, proceeds to sing the Star Spangled Banner and the whole family and the SWAT officers join in, gazing at Clarks Santa Claus and reindeer set(which he destroyed earlier out of anger), still burning and flying into the distance. The entire Griswold family, the Shirleys and the SWAT team members then celebrate inside the house, while Clark and Ellen embrace. After Ellen goes inside, Clark looks at the sky, happily smiling toward the stars and saying: "I did it."

==Cast==
* Chevy Chase as Clark W. "Sparky" Griswold, Jr.
* Beverly DAngelo as Ellen Smith Griswold
* Randy Quaid as Cousin Edward "Eddie" Johnson
* Juliette Lewis as Audrey Griswold
* Johnny Galecki as Rusty "Russ" Griswold John Randolph as Clark Wilhelm Griswold, Sr.
* Diane Ladd as Nora Griswold
* E. G. Marshall as Arthur "Art" Smith
* Doris Roberts as Frances Smith
* Miriam Flynn as Cousin Catherine Johnson
* Cody Burger as Cousin Rocky Johnson
* Ellen Hamilton Latzen as Cousin Ruby Sue Johnson William Hickey as Uncle Lewis
* Mae Questel as Aunt Bethany
* Sam McMurray as Bill
* Nicholas Guest as Todd Chester
* Julia Louis-Dreyfus as Margo Chester
* Brian Doyle-Murray as Frank Shirley
* Natalia Nogulich as Helen Shirley
* Nicolette Scorsese as Mary, the lingerie counter clerk
* Devin Bailey as Clark Griswold, Jr. (age 9)
* Jeremy Roberts as Cop

==Reception==

===Box office===
The movie debuted at #2 at the box-office while grossing $11,750,203 during the opening weekend, behind Back to the Future Part II. The movie eventually topped the box-office charts in its third week of release and remained #1 the following weekend. It went on to gross a total of $71,319,546 in the United States while showing in movie theaters. 

===Critical response===
At the time of the films release, the film received mixed to positive reviews; however, over time, many have cited it as a Christmas classic. Review aggregator Rotten Tomatoes reports that 63% of 35 film critics have given the film a positive review, with a rating average of 6.2 out of 10. 

Entertainment magazine Variety (magazine)|Variety responded positively to the film stating, "Solid family fare with plenty of yocks, National Lampoons Christmas Vacation is Chevy Chase and brood doing what they do best. Despite the title, which links it to previous pics in the rambling Vacation series, this third entry is firmly rooted at the Griswold family homestead, where Clark Griswold (Chase) is engaged in a typical over-reaching attempt to give his family a perfect, old-fashioned Christmas."  Rita Kempley of The Washington Post gave the film a positive review explaining that "it will prove pater-familiar to fans of the 1983 original and the European Vacation sequel. Only its a bit more whimsical." 

Janet Maslin of The New York Times gave the film a mediocre review explaining that the "third look at the quintessentially middle-American Griswold family, led by Clark and the very patient Ellen is only a weary shadow of the original National Lampoons Vacation." Maslin went on to say that "the best thing the new film does is to bring back Cousin Eddie, the wily, scene-stealing slob whose disgusting habits are a source of considerable amusement."  Roger Ebert of the Chicago Sun-Times gave the film two out of four stars saying, "The movie is curious in how close it comes to delivering on its material: Sequence after sequence seems to contain all the necessary material, to be well on the way toward a payoff, and then it somehow doesnt work." 

==Music==
 
The films musical score was composed by Angelo Badalamenti. It is the only installment of the Vacation film series not to include Lindsey Buckinghams "Holiday Road". In its place is a song entitled "Christmas Vacation" that was written for the movie by the husband-wife songwriting team of Barry Mann and Cynthia Weil and was performed by Mavis Staples of The Staple Singers fame. The song was covered in 2007 by High School Musical star Monique Coleman for the 2007 Christmas album Disney Channel Holiday.

Despite several popular songs being present in the film, no soundtrack album was released. In 1999, bootleg copies of a "10th Anniversary Limited Edition" began to appear on Internet auction sites with the claim that Warner Brothers and RedDotNet had pressed 20,000 CDs for   by composer John Williams and does not actually contain any of Badalamentis Christmas Vacation score.

==Sequels== European Vacation reprises the role, only this time being credited as "British Man on Plane".
Christmas Vacation is preceded in the Vacation series by:
* National Lampoons Vacation (1983)
* National Lampoons European Vacation (1985)

Christmas Vacation is followed in the series by:
* Vegas Vacation (1997)
* National Lampoons Christmas Vacation 2 (2003)
* Hotel Hell Vacation (2010)

Chevy Chase, Beverly DAngelo, and Juliette Lewis reprised their roles as the Griswolds in three Old Navy commercials which aired during the 2012 holiday season. In the second commercial, Anthony Michael Hall and Jason Lively also reprised their roles as Rusty and Barron returned as Audrey.

==See also==
* List of National Lampoon films List of contemporary Christmas classics
 

==References==
 

==External links==
 
*  
*  
*  
*  

{{Navboxes|list1=
 
 
 
 
}}

 
 
 
 
 
 
 
 
 
 
 
 
 