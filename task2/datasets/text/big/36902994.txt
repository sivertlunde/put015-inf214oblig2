Ajj De Ranjhe
{{multiple issues|
 
 
}}

{{Infobox film
| name           = Ajj De Ranjhe
| image          = 
| caption        =  Manmohan Singh
| producer       = Punjaab Movies International in association with Reliance Entertainment
| starring       = Aman Dhaliwal, Gurpreet Ghuggi, Kimi Verma, Gurleen Chopra, Kul Sidhu, Rana Ranbir, Deep Dhillon Manmohan Singh
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| country        = India
| language       = Punjabi
| budget         = 
| gross          = 
}} Manmohan Singh. The movie is to be released on 7 September 2012.

==Plot==
Ajj De Ranjhe is a satirical take on the state of the youth law and order system of Punjab. 

The film follows the journey of an unemployed youngster Ambar (Aman Dhaliwal) and a rookie cop Manjeet (Gurpreet Ghuggi). While Ambar hopes to make it as a TV journalist like his idol and romantic interest Kranti (Gurleen Chopra), Ghuggi is a dedicated cop, committed to clean up the messy state of affairs of his police station and bring a known local criminal Soocha Singh (Deep Dhillon) and his son to justice.

On their quest both Manjeet and Ambar encounter hilarious situations involving their romantic interests, family members, the people of the village & even gangsters. They both keep crossing paths at various junctures as the story progresses and eventually become a formidable team ready to take on the system.

Will this unlikely duo succeed in achieving their individual goals using unconventional tactics forms the rest of the story.

Set in the heartland of Punjab, Ajj De Ranjhe is a comedy blended with action, romance, songs and picturesque locations for good measure.

==Cast==
* Aman Dhaliwal as Ambar
* Gurpreet Ghuggi as Manjeet
* Kimi Verma as NRI woman
* Gurleen Chopra as Kranti (Day and Night News channel Reporter)
* Kul Sidhu as lady constable
* Rana Ranbir as constable
* Deep Dhillon

==References==
 

==External links==
*   
*  
*   \
*  

 
 
 


 