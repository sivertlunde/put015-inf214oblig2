The Invincible Masked Rider
{{Infobox film
 | name = The Invincible Masked Rider
 | image = The Invincible Masked Rider.png
 | caption =
 | director =  Umberto Lenzi
 | writer =   Umberto Lenzi Guido Malatesta Gino De Santis Luciano Martino
 | starring =  
 | music =  Angelo Francesco Lavagnino
 | cinematography = Augusto Tiezzi 
 | editing =    
 | producer =   Fortunato Misiano
 | released =  
 | language =   Italian
 }} 1963 Italian-French adventure film directed by Umberto Lenzi.         

== Cast ==

* Pierre Brice: Don Diego
* Daniele Vargas: Don Luis 
* Hélène Chanel: Carmencita 
* Massimo Serato: Don Rodrigo 
* Gisella Arden: Maria 
* Aldo Bufi Landi: Francisco 
* Carlo Latimer: Tabuca 
* Nerio Bernardi: Don Gomez 
* Romano Ghini: Maurilio 
* Tullio Altamura: Dr. Bernarinis 
* Guido Celano: Dr. Aguilera  
* Nello Pazzafini: Alonzo

==References==
 

==External links==
* 
 

 
 
 
  
 
 
 
 
 
 