Men of San Quentin
{{Infobox film
| name           = Men of San Quentin
| image_size     =
| image	=	Men of San Quentin FilmPoster.jpeg
| caption        =
| director       = William Beaudine Max King Martin Mooney
| writer         = Martin Mooney (story) Ernest Booth (screenplay)
| starring       = See below
| cinematography = Clark Ramsey
| editing        = Dan Milner
| distributor    = Producers Releasing Corporation
| released       =  
| runtime        = 80 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

Men of San Quentin is a 1942 American film directed by William Beaudine.

== Plot summary ==
 

== Cast ==
*J. Anthony Hughes as Jack Holden
*Eleanor Stewart as Anne Holden
*Dick Curtis as Butch Mason
*Charles B. Middleton as Saunderson
*Jeffrey Sayre as Jimmy
*George P. Breakston as Louie Howard
*Art Mills as Big Al Michael Mark as Mike, Convict in Ravine John Ince as Board Chairman
*Joe Whitehead as Joe Williams
*John Skins Miller as Convict Skins Miller
*John Shay as Phone Guard
*Jack Cheatham as Court Gate Guard
*Drew Demorest as Guard Gaines Nancy Evans as Mrs. Doakes
*Ted R. Standish as Prison Department of Music Supervisor Ted R. Standish
*John A. Hendricks as Prison Orchestra Conductor John A. Hendricks
*G. Rolph Burr as Prison Broadcast Announcer G. Rolph Burr
*Jack Reavis as Prison Glee Club Director Jack Reavis
*Carl C. Hocker as San Quentin Prison Guard Carl C. Hocker

== Soundtrack ==
 

== External links ==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 


 
 