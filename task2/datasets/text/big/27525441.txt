Target of an Assassin
{{Infobox film
| name           = Target of an Assassin
| image          = 
| image_size     = 
| caption        =  Peter Collinson
| writer         = 
| narrator       = 
| starring       = Anthony Quinn
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 10 February 1977
| runtime        = 105 min.
| country        = South Africa
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}} Peter Collinson. It stars Anthony Quinn and John Phillip Law. Target of an Assassin was completed in South Africa in 1976 as Tigers Dont Cry, but was not put into general American release for nearly nine years. Other alternate titles include African Rage, The Long Shot, and Fatal Assassin.  

==Cast==
*Anthony Quinn as Ernest Hobday
*John Phillip Law as Shannon
*Simon Sabela as President Lunda
*Marius Weyers as Colonel Albert Pahler
*Sandra Prinsloo as Sister Janet Hobart

==Plot summary==
The film is set in a South African hospital. Top-billed Anthony Quinn plays a male nurse, assigned to care for a foreign President (Simon Sabela). With many threats against his well-being, the leader is heavily guarded around the clock. But Quinn manages to kidnap his patient for strictly personal gain, unaware that a hired sniper is still attempting to take the life of the foreign leader while in Quinns custody.  This leads to a series of curious plot twists leading to a climatic scene with cable cars on a high plateau ridge.

==References==
 

==External links==
* 

 

 
 
 
 
 

 