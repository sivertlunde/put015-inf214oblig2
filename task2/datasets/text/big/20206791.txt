Dreyfus (1930 film)
{{Infobox film
| name           = Dreyfus
| image          = 
| image_size     = 
| caption        = 
| director       = Richard Oswald
| producer       = Richard Oswald Bruno Weil (novel)	 	
| starring       = 
| music          = 
| cinematography = Heinrich Balasch,  Friedl Behn-Grund	 	
| editing        = 
| distributor    = 
| released       = 1930
| runtime        = 
| country        = Weimar Republic
| language       = German
| budget         = 
| gross          = 
}} Bruno Weil. It was one of the sources for the later English film Dreyfus (1931 film)|Dreyfus.

==Cast==
*Fritz Kortner – Alfred Dreyfus
*Grete Mosheim – Lucie Dreyfus
*Erwin Kalser – Mathieu Dreyfus (brother of the accused)
*Heinrich George – Émile Zola
*Albert Bassermann – Georges Picquart|Col. Picquart
*Oskar Homolka – Ferdinand Walsin Esterhazy|Maj. Ferdinand Walsin Esterhazy
*Nora Mestom – Marguerite Pays (his girlfriend)
*Ferdinand Hart – Hubert-Joseph Henry|Maj. Henry
*Fritz Rasp – Armand du Paty de Clam|Maj. Du Paty de Clam
*Paul Bildt – Georges Clemenceau Labori (defense counsel)
*Paul Henckels – Edgar Demange (defense counsel)
*Ferdinand Bonn – Auguste Mercier|Gen. Mercier
*Leopold von Ledebur – Raoul Le Mouton de Boisdeffre|Gen. De Boisdeffre
*Bernhard Goetzke – Georges-Gabriel de Pellieux|Gen. Pellieux Godefroy Cavaignac
*Eduard Rothauser – Jules Lauth|Capt. Lauth
*Josef Reithofer – Ferdinand Forzinetti|Maj. Forzinetti (prison governor)
*Bruno Ziener – Alphonse Bertillon
*Sigmund Nunberg – Chairman of the Jury Court
*Elsa Basserman – Parisian lady
*Fritz Reiff – Jean Jaures
*Bernd Aldor – Dubois
*Ferry von Gorup – Officer in military prison, Cherche-Midi

==External links==
* 

 

 
 
 
 
 
 
 
 


 