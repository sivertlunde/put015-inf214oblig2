Service (film)
 
 
{{Infobox film
| name           = Service
| image          = 
| alt            = 
| caption        = 
| film name      = Serbis
| director       = Brillante Mendoza
| producer       = Ferdinand Lapuz
| screenplay     = Armando Lao
| story          = Armando Lao Boots Agbayani Pastor
| starring       = Gina Pareño Jaclyn Jose Julio Diaz Kristoffer King Mercedes Cabral Coco Martin
| music          = Gian Gianan
| cinematography = Odyssey Flores
| editing        = Claire Villa-Real
| studio         = 
| distributor    = Centerstage Productions Swift Productions
| released       =  
| runtime        = 90 minutes
| country        = Philippines
| language       = Tagalog
| budget         = 
| gross          = $155,156   
}}
Service (  directed by   in 1984. {{cite web|url=http://pinoyfilm.com/serbis-vying-for-the-palme-dor-in-cannes|title=Serbis Vying for the Palme dOr in Cannes
|accessdate=2008-05-25}}  

== Plot ==
Service is a drama that follows the travails of the Pineda family in the Filipino city of Angeles. Bigamy, unwanted pregnancy, possible incest and bothersome skin irritations are all part of their daily challenges, but the real "star" of the show is an enormous, dilapidated movie theater that doubles as family business and living space. At one time a prestige establishment, the theater now runs porn double bills and serves as a meeting ground for hustlers of every conceivable persuasion. The film captures the sordid, fetid atmosphere, interweaving various family subplots with the comings and goings of customers, thieves and even a runaway goat while enveloping the viewer in a maelstrom of sound, noise and continuous motion.

== Cast ==
*Gina Pareño as Nanay Flor
*Jaclyn Jose as Nayda
*Julio Diaz as Lando
*Kristoffer King as Ronald
*Mercedes Cabral as Merly
*Coco Martin as Alan
*Dan Alvaro as Jerome

== Release ==
=== Box office ===
 

=== Critical response ===
Service caused a stir in the Philippines with its loud ambient noise and its graphic depiction of sex and nudity.  Submitted to the Movie and Television Review and Classification Board for public exhibition in 2008, the movie survived with two major cuts to sex scenes and was rated an R18. 

Such was the advance international buzz of the film that it was invited to compete at the  ). Its premiere at the festival was marked by the walking out of several veteran film critics who protested Mendozas version of "misery porn." 

On 30 January 2009, the film premiered in New York. Writing for the New York Times, its chief film critic Manohla Dargis described the film: "The heavenly bodies that populate our films bring their own pleasures ... alighting onscreen as if from a dream. But the bodies in  , which received little love at the 2008 Cannes, are not heaven-sent, but neither are they puppets in a contrived nightmare. Rather, they lust, sweat, desire and struggle with the ferocious truth."  Roger Ebert of the Chicago Sun-Times gave Service two and a half out of four stars, stating that " f you see only one art film this month, this shouldnt be the one. If you see one every week, you might admire it." 

== Accolades ==
{| class="wikitable"
|-
! Year
! Event
! Category
! Recipient
! Result
|- 2008
|2008 Cannes Film Festival Palme dOr
|Serbis
| 
|- 2009
|rowspan=3|3rd Asian Film Awards Asian Film Best Director Brillante Mendoza
| 
|- Asian Film Best Supporting Actress Gina Pareño
| 
|- Jaclyn Jose
| 
|- Gawad Urian Awards Best Picture
|Serbis
| 
|- Best Director Brillante Mendoza
| 
|- Best Actress Gina Pareño
| 
|- Jaclyn Jose
| 
|- Best Supporting Actor Julio Diaz
| 
|- Best Screenplay Armando Lao Boots Agbayani Pastor
| 
|- Best Cinematography Odyssey Flores
| 
|- Best Editing Claire Villa-Real
| 
|- Best Production Design Benjamin Padero Carlo Tabije
| 
|- Best Sound Emmanuel Clemente
| 
|- 2011
|Gawad Urian Awards Best Film of the Decade (2000-2009)
|Serbis
| 
|-
|}

== References ==
 

== External links ==
* 
* 
* 
* 
* 
* 
* 
* 

 

 
 
 
 
 