Die Ratten
 
{{Infobox film
| name           = Die Ratten
| image          =
| caption        =
| director       = Robert Siodmak
| producer       =
| writer         = Jochen Huth
| starring       = Maria Schell Curd Jürgens Heidemarie Hatheyer Gustav Knuth Ilse Steppat
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = 97 minutes
| country        = West Germany
| language       = German
| budget         =
}} The Rats by Gerhart Hauptmann which transferred the story in the early fifties, shortly after the Second World War. It tells the story of the destitute Pole Pauline, who sells her illegitimate baby for a few hundred Deutsche Mark to the childless forwarders wife Anna John. The film won the Golden Bear award.   

==Cast==
* Maria Schell - Pauline Karka
* Curd Jürgens - Bruno Mechelke
* Heidemarie Hatheyer - Anna John
* Gustav Knuth - Karl John
* Ilse Steppat - Frau Knobbe
* Fritz Remond - Harro Hassenreuter
* Barbara Rost - Selma Knobbe
* Hans Bergmann
* Walter Bluhm
* Carl de Vogt
* Erich Dunskus
* Karl Hellmer Manfred Meurer
* Edith Schollwer
* Lou Seitz
* Hans Stiebner

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 

 