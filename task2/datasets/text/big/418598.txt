Gentlemen Prefer Blondes (1953 film)
 
{{Infobox film
| name           = Gentlemen Prefer Blondes
| image          = Gentlemen Prefer Blondes (1953) film poster.jpg
| caption        = Theatrical Poster
| director       = Howard Hawks
| producer       = Sol C. Siegel
| writer         =  Charles Lederer Gentlemen Prefer Blondes by Anita Loos and Joseph Fields
| starring       = Jane Russell Marilyn Monroe
| music          = Hoagy Carmichael Jule Styne Eliot Daniel Lionel Newman
| cinematography = Harry J. Wild
| editing        = Hugh S. Fowler
| distributor    = 20th Century Fox
| released       =  
| runtime        = 91 minutes
| country        = United States English French French
| budget         = $2,260,000   
| gross          = $5.3 million   
|}} 1949 stage directed by Howard Hawks, and starring Jane Russell and Marilyn Monroe with Charles Coburn, Elliott Reid, Tommy Noonan, George Winslow, Taylor Holmes, and Norma Varden in supporting roles.
 silent movie, Ruth Taylor, Alice White, Ford Sterling, and Mack Swain, which is now lost.
 pink dress are considered iconic, and the performance has inspired homages by Madonna (entertainer)|Madonna, Geri Halliwell, Kylie Minogue, Nicole Kidman, Anna Nicole Smith, Christina Aguilera, and James Franco.
 Jack Cole, whereas the music was written by songwriting teams Hoagy Carmichael & Harold Adamson and Jule Styne & Leo Robin. The songs by Styne and Robin are from the Broadway show, while the songs by Carmichael and Adamson were written especially for the film.

==Plot==
  as Lorelei Lee]] showgirls and best friends. Lorelei has a passion for diamonds, believing that attracting a rich husband is one of the few ways a woman can succeed economically. She is engaged to Gus Esmond (Tommy Noonan), a naive nerd willing to do or buy anything for her. However, Gus is under the control of his wealthy, upper-class father. Dorothy, on the other hand, is looking for a different kind of love, attracted only to men who are good-looking and fit.

Lorelei has planned to wed Gus in France, but Esmond, Sr. stops his son from sailing, believing that Lorelei is bad for him. Although Loreleis job requires that she travels to France with or without Gus, before she leaves, Gus gives her a letter of credit to cover expenses upon her arrival, and promises to later meet her in France. However, he also warns her to behave, noting that his father will prohibit their marriage if rumors of misdeeds make their way to Esmond, Sr. Unbeknownst to both of them, Esmond, Sr. has hired a private detective, Ernie Malone (Elliott Reid), to spy on Lorelei.

During the Atlantic crossing, Malone immediately falls in love with Dorothy, but Dorothy has already been drawn to the members of the Olympic athletics team. Lorelei meets the rich and foolish Sir Francis "Piggy" Beekman (Charles Coburn), the owner of a diamond mine, and is attracted by his wealth; although Piggy is married, Lorelei naively returns his geriatric flirtations, which annoys his wife, Lady Beekman (Norma Varden).

Lorelei invites Piggy to the cabin she shares with Dorothy, whereupon he recounts his travels to Africa. While Piggy demonstrates how a python squeezes a goat by hugging Lorelei, Malone spies through the window and takes pictures of the two, but is caught by Dorothy as he walks away nonchalantly. She tells Lorelei, who fears for her reputation. They come up with a scheme to intoxicate Malone and then search him to recover the incriminating film while he is unconscious. They find the film in his pants, then Lorelei promptly prints and hides the negatives. Revealing her success to Piggy, she persuades him to give her Lady Beekmans tiara as a thank you gift. However, Malone reveals he had planted a recording device in Loreleis cabin, and has heard her discussion with Piggy about the pictures and the tiara. Malone implies Lorelei is a golddigger and, when Dorothy scolds him for his actions, admits that he himself is a liar. However, Dorothy reveals to Lorelei she is falling for Malone, after which Lorelei chastises her for choosing a poor man when she could easily have a rich man.

  as Dorothy Shaw]]
The ship arrives in France, and Lorelei and Dorothy spend time shopping. However, the pair are then kicked out of their hotel and discover Loreleis letter of credit has been cancelled due to the information Malone shared with Esmond, Sr. When Gus shows up at their show, Lorelei rebuffs him, after which she performs Diamonds are a Girls Best Friend, the musical number whose lyrics explain why and how women need to pursue men with money. Meanwhile, Lady Beekman has filed charges regarding her missing tiara, and Lorelei is charged with theft. Dorothy persuades Lorelei to return the tiara, but the pair discover it is missing from her jewelry box. Piggy tries to weasel out of his part in the affair when Malone catches him at the airport.

Dorothy stalls for time in court by pretending to be Lorelei, disguised in a blonde wig and mimicking her friends breathy voice and mannerisms. When Malone appears in court and is about to unmask "Lorelei" as Dorothy, she reveals to Malone in covert language that she, Dorothy, loves him but would never forgive him if he were to do anything to hurt her best friend, Lorelei. Malone withdraws his comments, but then reveals Piggy has the tiara, exonerating Lorelei.

Back at the nightclub, Lorelei impresses Esmond, Sr. with a speech on the subject of paternal money, and also makes an argument that if Esmond, Sr. had a daughter instead of a son, he would want the best for her, to which he agrees and consents to his sons marriage to Lorelei. The film closes with a double wedding for Lorelei and Dorothy, who marry Esmond and Malone, respectively.

==Cast==
 
* Jane Russell as Dorothy Shaw
* Marilyn Monroe as Lorelei Lee (singing voice on high notes was dubbed by Marni Nixon)
* Charles Coburn as Sir Francis "Piggy" Beekman
* Elliott Reid as Ernie Malone
* Tommy Noonan as Gus Esmond
* Taylor Holmes as Mr. Esmond Sr.
* Norma Varden as Lady Beekman
* George Winslow as Henry Spofford III
 

==Reception== ninth highest-grossing film of 1953, whereas Monroes next feature How to Marry a Millionaire was the fourth.    Monroe and Russell were both praised for their performances as Lorelei and Dorothy; and, as a result, the characters have become extremely popular in pop culture. 

Rotten Tomatoes retrospectively collected 37 reviews, and gave the film a score of 97%,  while German film director Rainer Werner Fassbinder declared it one of the ten best films ever made.   

===Award nominations===
{| class="wikitable"
|- Date of ceremony Award
! Category
! Recipients and nominees Result
|- Writers Guild February 25, 1954  Writers Guild of America Awards Writers Guild Best Written Musical Charles Lederer  ||  
|-
|}

===Accolades===
  
As a result of the films success, Monroe and Russell were given the chance to put their hand and foot prints in cement at Graumans Chinese Theatre; a spectacle that got a lot of publicity for both actresses. 

==Gentlemen Marry Brunettes==
Loos wrote a sequel to her novel entitled But Gentlemen Marry Brunettes, with further adventures of Lorelei and Dorothy. The 1955 Gentlemen Marry Brunettes musical film used only the books name and starred Russell and Jeanne Crain playing completely new characters.

==References==
 

==External links==
 
 
* 
*  
*  
*  
*  

 
 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 