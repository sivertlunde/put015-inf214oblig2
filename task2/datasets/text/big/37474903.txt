Outback Vampires
{{Infobox film
| name           = Outback Vampires
| image          = 
| image size     =
| caption        = Guild Home Video VHS cover
| director       = Colin Eggleston
| producer       = James Vernon Jan Tyrrell
| writer         = David Young Colin Eggleston
| based on = 
| narrator       = John Doyle
| music          = 
| cinematography = Gary Wapshott
| editing        = Josephine Cook
| studio = Cine Funds Limited Somerset Film Productions
| distributor    = 
| released       = 1987
| runtime        = 
| country        = Australia English
| budget         =
| gross = 
| preceded by    =
| followed by    =
}}
Outback Vampires is a 1987 Australian film directed by Colin Eggleston and features Richard Morgan, Angela Kennedy, Brett Climo and Richard Carter. It was written by Colin Eggleston and David Young. It was filmed in Yarralumla, Australian Capital Territory, Australia.

==Plot==
Whilst on their way to a rodeo festival Lucy (Angela Kennedy), Nick (Richard Morgan) and Broncos (Brett Climo) car breaks down, leaving them stranded in a small town. The odd-ball locals send them to Sir Alfreds house on the top of hill where once inside things become even weirder. Sir Alfreds wife seems quite deranged, his daughter almost psychotic and his son is extremely eccentric. After becoming separated, Lucy, Nick and Bronco are taken on a surrealist journey through the mansion, which is still decorated with Christmas decorations. The scenes in the house are shot in blue-tones, characters are able to climb on walls, people are told to "follow the bouncing ball", doors suddenly vanish and there is a music-video style performance by a band at one point. The three friends must band together to find a way out of this haunted house and rid the town of this un-dead family once and for all.

==Production==
The film was also known as The Wicked and  Prince at the Court of Yarralumla. 
The film was not released in cinemas and aired on TV 18 June 1988. 

==References==
 

==External links==
*  at IMDB
*  at AustLit
*  at  
 

 
 
 
 
 
 


 
 