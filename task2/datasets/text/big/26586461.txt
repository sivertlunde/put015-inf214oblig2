Battleship (film)
 
{{Infobox film
| name           = Battleship
| image          = Battleship Poster.jpg
| caption        = Promotional poster
| director       = Peter Berg
| producer       = {{plainlist|
* Peter Berg
* Brian Goldner
* Scott Stuber
* Sarah Aubrey
* Duncan Henderson
* Bennett Schneir }}
| writer         =  
| based on       =  
| starring       = {{plainlist|
* Taylor Kitsch
* Alexander Skarsgård
* Rihanna
* Brooklyn Decker
* Tadanobu Asano
* Liam Neeson }}
| music          = Steve Jablonsky 
| cinematography = Tobias A. Schliessler
| editing        = {{plainlist| 
* Colby Parker Jr.
* Billy Rich
* Paul Rubell }} Bluegrass Films Film 44 Universal Pictures
| released       =  
| runtime        = 131 minutes   
| country        = United States
| language       = English
| budget         = $220 million 
| gross          = $303 million 
}} board game. Universal Pictures. Dentsu Inc., which left NBCUniversal Entertainment Japan before being spun off as a separate company in February 17, 2014. The film stars Taylor Kitsch, Liam Neeson, Alexander Skarsgård, Rihanna, John Tui, Brooklyn Decker and Tadanobu Asano.

The film was originally planned to be released in 2011, but was rescheduled to April 11, 2012, in the United Kingdom and May 18, 2012, in the United States.  The films world premiere was in Tokyo, Japan, on April 3, 2012.

==Plot==
  conditions similar Admiral Terrance Shane. Stone Hopper, Alexs older brother and a naval officer, is infuriated at Alexs lack of motivation and forces Alex to join him in the U.S. Navy.
 lieutenant and commander and discharged from 2012 Rim of the Pacific Exercise (RIMPAC) in Hawaii.
 JMSDF Kongō force field friend or foe status, changing the monitor color for the Navy ships from green to red. The aliens destroy the Sampson and Myōkō, and damage John Paul Jones, killing the commanding and executive officers. Alex sees his brother die and returns to his ship, where he is forced to take command, albeit reluctantly and to the disbelief of the crew, as the most senior officer left on the ship. Enraged, Alex orders an attack, but Beast manages to convince him to recover the survivors from Myōkō, including Captain Nagato, with whom Alex is in a rivalry. The alien ships send weapons to the islands that destroy the military equipment and island infrastructure, killing a large number of civilians when they destroy the freeway leading to a military base.

In Oahu, Sam, a physical therapist, is accompanying retired U.S. Army veteran and double amputee Mick Canales, on a mountain hike to help him adapt to his prosthetic legs. Sam and Mick run into some police officers who order them off the mountain. The cops head up the mountain and are ambushed and killed by aliens. Later, Sam and Mick run into scientist Cal Zapata, who works at the communications array and informs them that the aliens have killed his grad student and taken over the communications array, and they realize that the aliens are using it to signal back to their home planet.
 tsunami warning buoys around Hawaii to track the aliens as he has been using it to track other countries ships for every RIMPAC game. 

Ashore, Mick forces Zapata to retrieve a spectrum analyzer that allows Sam and Mick to contact the John Paul Jones and relay that the satellite used by the communications array will be in position in four hours and that the aliens will be able use it to contact their planet. Zapata manages to take the analyzer, and, though he is temporarily caught by an alien, successfully escape. 

During a night battle, the John Paul Jones sinks two alien ships but is unable to hit the evasively maneuvering third. Alex decides to lure the third alien vessel close to shore at dawn, where he and Nagata shoot out its bridge windows as the sunrise blinds the aliens. John Paul Jones destroys the last alien escort vessel only to fall prey to their motherships long range weapons. The crew abandon ship as the John Paul Jones is destroyed by these weapons and sunk. 

Realizing the aliens are attempting to contact a larger invasion fleet, Alex and the survivors of the John Paul Jones and Myōkō return to Pearl Harbor and assume command of  , a decommissioned battleship turned into a museum ship. They reactivate the battleship with the aid of the retired veterans preserving her, but they end up coming face to face with the alien mothership. In the ensuing battle, the Missouris 16"/50 caliber Mark 7 gun|16-inch main guns severely damage the alien mothership, destroying its force field in process. Meanwhile, Mick, Sam and Zapata distract the aliens to buy the Missouri more time. Alex uses the last shell to destroy the communications array on the island, leaving the Missouri defenseless. With the force field down, Admiral Shane scrambles the fleet and Australian F/A-18 fighters from the RIMPAC fleet, who arrive and save the crew by eliminating the alien threat.
 lieutenant commander. Navy Cross. After the ceremony, Alex is given an offer to become a Navy SEAL, and then asks Admiral Shane for his daughters hand in marriage. The admiral initially refuses but invites Alex to lunch to discuss the matter, referencing how Alex and Sam met.

In a post-credits scene, three teenagers and a handyman in Scotland discover a crashed alien pod.  When they open it, an alien hand reaches out, and they run off in terror.

==Cast==
* Taylor Kitsch as Lieutenant (Lieutenant Commander-select) Alex Hopper, an undisciplined, ex-enlisted, U.S. Navy officer assigned to the guided missile destroyer   as Weapons Officer.
* Alexander Skarsgård as Commander Stone Hopper, Alexs older brother and Commanding Officer of the guided missile destroyer  . Second Class (GM2) Cora Raikes, a weapons specialist aboard the John Paul Jones.
* Brooklyn Decker as Samantha "Sam" Shane, a physical therapist and Alex Hoppers girlfriend.
* Tadanobu Asano as Captain Nagata, JMSDF, Commanding Officer of the  .
* Hamish Linklater as Cal Zapata, a scientist working on Oahu.
* Liam Neeson as Admiral Terrance Shane, Commander of the U.S. Pacific Fleet and father of Samantha Shane.
* Peter MacNicol as the U.S. Secretary of Defense. Chief Petty Officer Walter "Beast" Lynch, a crew member aboard the John Paul Jones.
* Gregory D. Gadson as retired Lieutenant Colonel Mick Canales, a U.S. Army combat veteran and double amputee.
* Adam Godley as Dr. Nogrady, the scientist leading the Beacon program.
* Jesse Plemons as Jimmy "Ordy" Ord, undesignated Seaman on the John Paul Jones.
* Joji Yoshida as Chief Engineer Hiroki

==Production==
  2012.]] Gold Coast in 2010, but the production company changed location due to a lack of Australian government tax incentives and a high estimated budget of $209 million. 
 Baton Rouge, Louisiana. 

The Science & Entertainment Exchange provided science consultation for the film. 

A Kongō-class destroyer|Kongō-class destroyer of the Japan Maritime Self-Defense Force also appeared in the film. 

===Casting===
Jeremy Renner was originally considered for the role of Hopper, but the actor chose to star in a Paul Thomas Anderson drama filming at roughly the same time.      In April 2010, it was reported that Taylor Kitsch had been cast as Alex Hopper,   Alexander Skarsgård played his brother Stone Hopper, Brooklyn Decker stars as Sam, Hoppers fiancee and Liam Neeson as Admiral Shane, Sams father and Hoppers superior officer.     Barbadian R&B singer Rihanna makes her acting debut in the film, as a sailor.  In an interview with GQ, Berg explained how he came up with the idea to cast her. He realized she could act after seeing her 2009 interview about the Chris Brown assault on Good Morning America with Diane Sawyer during which he found her "intelligent and articulate", and her appearance on Saturday Night Live.  She accepted the role because she wanted "to do something badass" and also because it wasnt a role too big for her to play.  Gregory Gadson, LTC Mick National Geographic Magazine. 
 Friday Night Lights. Berg said he loves working with friends and explained he knew how comfortable Kitsch was with Plemons, "I know that he’s really good for Taylor and he makes Taylor better. So, I wrote that whole part for Jesse." He added, "I never thought of it as a Friday Night Lights reunion. I thought of it as protection, bringing a trusted family member in." 

U.S. Navy sailors were used as extras in various parts of this film. Sailors from assorted commands in Navy Region Hawaii assisted with line handling to take   in and out of port for a day of shooting in mid 2010. A few months later, the production team put out a casting call for sailors stationed at various sea commands at Naval Station Mayport, Florida to serve as extras.  Sailors were also taken from various ships stationed at Naval Station Mayport, Jacksonville, Florida:  ,   and USS   were some of the ships that provided sailors. 

==Soundtrack==
 
{{Infobox album  
| Name        = Battleship
| Type        = Soundtrack
| Artist      = Steve Jablonsky
| Cover       =
| Released    = May 8, 2012
| Recorded    =
| Genre       = Film score
| Length      = 77:28
| Label       = Varèse Sarabande
| Producer    = Hans Zimmer
| Reviews     =
}}
Due to his success with the Transformers (film series)|Transformers franchise, composer Steve Jablonsky was chosen to score the official soundtrack. The soundtrack features original compositions from Jablonsky and features rock guitarist Tom Morello from Rage Against The Machine. Director Peter Berg stated:
 

===Track listing===
All songs written and composed by Steve Jablonsky except where noted.

{{Track listing
| headline        = Battleship: Original Motion Picture Soundtrack
| music_credits   = no
| total_length = 77:28
| title1 = First Transmission
| length1 = 3:19
| title2 = The Art of War
| length2 = 4:33
| title3 = Full Attack
| length3 = 3:55
| title4 = Youre Going to the Navy
| length4 = 1:04
| title5 = The Beacon Project
| length5 = 5:09
| title6 = Objects Make Impact
| length6 = 4:40
| title7 = First Contact, Part I
| length7 = 1:53
| title8 = First Contact, Part II
| length8 = 2:10
| title9 = Its Your Ship Now
| length9 = 4:05
| title10 = Shredders
| length10 = 4:07
| title11 = Regents Are on the Mainland
| length11 = 2:44
| title12 = Trying to Communicate
| length12 = 3:17
| title13 = Water Displacement
| length13 = 2:20
| title14 = Buoy Grid Battle
| length14 = 3:05
| title15 = USS John Paul Jones
| length15 = 2:25
| title16 = We Have a Battleship
| length16 = 2:51
| title17 = Somebodys Gonna Kiss the Donkey
| length17 = 4:35
| title18 = Super Battle
| length18 = 1:34
| note18 = composed by Tom Morello
| title19 = Thug Fight
| length19 = 3:31
| note19 = featuring Tom Morello
| title20 = Battle on Land and Sea
| length20 = 2:50
| title21 = Silver Star
| length21 = 1:56
| title22 = The Aliens
| length22 = 4:20
| title23 = Planet G
| length23 = 4:01
| title24 = Hopper
| length24 = 3:15
}}

==Release==
 
The films world premiere took place in Tokyo on April 3, 2012. The event was attended by director Peter Berg, actors Taylor Kitsch, Brooklyn Decker, Alexander Skarsgård and Rihanna. Later on they initiated a Press Tour visiting Madrid, London and Cartagena de Indias to promote the film.

===Box office===
The film earned $303,025,485, of which $65,422,625 was in North America. 

The film opened outside North America on Wednesday, April 11, 2012, more than five weeks before its North America release, earning $7.4 million.    Through Friday, April 13, the film had earned a 3-day total of $25 million.    By the end of its opening weekend, it earned $55.2 million from 26 markets, ranking second behind the 3D re-release of Titanic (1997 film)#3D Theatrical Re-release|Titanic. 

However, on its second weekend, it topped the box office outside North America, with $60 million.  In South Korea, it achieved the highest-grossing opening day for a non-sequel and the third-highest overall ($2.8 million).  In comparison to other   (£1.71 million). 

In North America, Battleship grossed $8.8 million on its opening day (Friday, May 18, 2012), with $420,000 originating from midnight showings,  and finished the weekend with $25.5 million. It settled in second place for its opening day and opening weekend behind Marvels The Avengers.    Its opening weekend grosses were well below the anticipated $35–$40 million range that Universal and director Peter Berg were hoping for. 

===Critical reception===
The film has received generally negative reviews from critics. Metacritic has given the film an average score of 41 out of 100 based on 39 reviews.  Rotten Tomatoes gives the film a score of 34% based on reviews from 208 critics, with a rating average of 4.6 out of 10. The sites consensus reads: "It may offer energetic escapism for less demanding filmgoers, but Battleship is too loud, poorly written, and formulaic to justify its expense -- and a lot less fun than its source material." 

Megan Lehmann of The Hollywood Reporter thought that the "impressive visual effects and director Peter Bergs epic set pieces fight against an armada of cinematic clichés and some truly awful dialogue."    Empire (film magazine)|Empire magazines Nick de Semlyen felt there was a lack of character development and memorable action shots, and sums up his review of the movie in one word: "Miss."   

Many reviews panned the "based on a board game" concept driving the film, although some, such as Jason Di Rosso from the Australian Broadcasting Corporations Radio National, claimed the ridiculousness of the setup is "either sheer joy or pure hell – depending on how seriously you take it", while de Semlyen "had to admire   jumping through hoops to engineer a sequence that replicates the board game."    Several compared the film to Michael Bays Transformers (film)|Transformers film series in terms of quality and cinematic style, with Giles Hardie of The Sydney Morning Herald claiming that the movie "finds the same balance between action-packed imagination and not taking the premise seriously that made Michael Bays original Transformers such a joyride."     Andrew Harrison of Q (magazine)|Q magazine called the film "crushingly stupid".  Film critic Kenneth Turan, in a review written for the Los Angeles Times, also expressed disappointment, criticizing the films "humanoid aliens", stating that they are "as ungainly as the movie itself, clunking around in awkward, protective suits." He called the content "all very earnest", but added "its not a whole lot of fun". 
 Independence Day, Pearl Harbor, Jurassic Park The Hunt for Red October".  Giving it a B+ grade, Lisa Schwarzbaum of Entertainment Weekly  said, "For every line of howler dialogue that should have been sunk, theres a nice little scene in which humans have to make a difficult decision. For every stretch of generic sci-fi-via-CGI moviemaking, theres a welcome bit of wit."  The Washington Post gave the film a three-star rating out of four commenting it is "an invigorating blast of cinematic adrenaline".  Roger Ebert of the Chicago Sun-Times gave the film 2½ stars out of 4, praising the climax as "an honest-to-God third act, instead of just settling for nonstop fireballs and explosions, as Bay likes to do. I dont want to spoil it for you. Lets say the Greatest Generation still has the right stuff and leave it at that." 

===Accolades===
{| class="wikitable sortable" width="99%"
|- style="background:#ccc; text-align:center;"
! colspan="4" style="background: RoyalBlue;" | List of awards and nominations
|- style="background:#ccc; text-align:center;"
! Award
! Category
! Recipient(s) and nominee(s)
! Result
|-
| Annie Awards 
| Best Animated Effects in a Live Action Production
| Willi Geiger, Rick Hankins, Florent Andorra, Florian Witzel, Aron Bonar
| rowspan="8"  
|-
| rowspan="2"| Golden Trailer Awards  Best Sound Editing
|
|-
| Best Summer Blockbuster 2012 TV Spot
|
|-
| Houston Film Critics Society 
| Worst Film
|
|-
| rowspan="7"| Golden Raspberry Awards   Worst Picture
|
|- Worst Director
| Peter Berg
|- Worst Supporting Actor
| Liam Neeson
|- Golden Raspberry Worst Supporting Actress
| Brooklyn Decker
|-
| Rihanna
|  
|- Worst Screenplay
| Jon Hoeber and Eric Hoeber
| rowspan=2  
|- Worst Screen Ensemble
|
|- Saturn Awards  Best Special Effects
| Grady Cofer, Pablo Helman, Jeanie King and Burt Dalton
|  
|- Teen Choice Awards 
| Choice Movie Breakout
| Rihanna
|  
|-
| rowspan="2"| Visual Effects Society 
| Outstanding Visual Effects in a Visual-Effects Driven Film
| rowspan="2"| Battleship
| rowspan="2"  
|-
| Outstanding FX and Simulation Animation in a Live Action Feature Motion Picture
|-
|}

===Home media===
Battleship was released on DVD and Blu-ray disc on August 20, 2012 in the United Kingdom,  and on August 28 in the United States and Canada. 

==Video game==
A video game based on the film, titled  , was released on May 15, 2012 to coincide with the films international release. The game was published by Activision and developed by Double Helix Games for PlayStation 3, Wii, and Xbox 360, and developed by Magic Pockets for Nintendo 3DS and Nintendo DS.

==Board game==
Hasbro released several new editions of the classic board game, including an update to the regular fleet-vs.-fleet game and a "movie edition", featuring the alien vessels and a card-based play mode.

==See also== USS Iowa.
* Under Siege, a 1992 film also set on board the USS Missouri starring Steven Seagal

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 