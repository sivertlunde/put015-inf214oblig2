Little Murder
 
{{Infobox Film
| name           = Little Murder
| image          = 
| caption        = 
| director       = Predrag Antonijević
| producer       = Silvio Muraglia, Eric Fierstein Co-Executive Producers John Gaston, Jeff Spilman, Jeff Stern, Executive Producer Stephen Hays 
| writer         =  
| starring       =  
| music          =
| cinematography = Steve Mason
| editing        = Devin Maurer
| distributor    = Mind in Motion
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
}} thriller film directed by Predrag Antonijević. The film premiered at the Palm Springs International Film Festival on January 8, 2011. 

== Plot ==
The story centers on a humiliated detective Ben Chaney (Josh Lucas) who gets a sudden shot at salvation when the ghost of a murdered cellist Corey Little (Lake Bell) solicits his help in finding her killer as he was on a stakeout of a serial killer suspect, Drag Hammerman. As Chaney unravels the truth behind Littles night of murder, a little turn of events revealed the killer to all of the murders, including Littles, to be the very same man all along. 

== Cast ==
* Josh Lucas as Ben Chaney   
* Terrence Howard as Drag Hammerman 
* Lake Bell as Corey Little 
* Sharon Leal as Jennifer   
* Cary Elwes as Barry Fitzgerald 
* Bokeem Woodbine as Lipp 
* Peter Jason as Lt. Wills 
* Noah Bean as Paul Marais 
* Nick Lashaway as Tom Little
* Deborah Ann Woll as Molly 
* Brandon Molale as Bobby
* Ele Bardha as Manny
* Sonya A. Avakian as Pauls Housekeeper
* Sean H. Robertson as Crime Scene Officer #2

== Production ==
The film is shooting in Detroit and New Orleans,    Little Murder is the third US production for the Serbian director Predrag "Peter Gaga" Antonijevic. 

Rumors of the films budget problems surfaced after numerous complaints to the Michigan state film office were made by local vendors for goods and services that were not paid for. The Townsend Hotel in Birmingham, Michigan filed suit, claiming the production company accumulated a tab of over $37,000 before leaving town without paying. 

==References==
 

== External links ==
*  

 
 
 
 
 
 
 
 