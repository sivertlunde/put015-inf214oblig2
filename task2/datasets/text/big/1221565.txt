Junior (1994 film)
{{Infobox film
| name           = Junior
| image          = Juniorposter.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Ivan Reitman
| producer       = Ivan Reitman
| writer         = Kevin Wade Chris Conrad
| starring       = Arnold Schwarzenegger Danny DeVito Emma Thompson Frank Langella Pamela Reed
| music          = James Newton Howard Adam Greenberg
| editing        = Wendy Greene Bricmont Sheldon Kahn
| studio         = Northern Lights Entertainment
| distributor    = Universal Pictures
| released       =  
| runtime        = 109 minutes
| country        = United States
| language       = English
| budget         = $60 million
| gross          = $108.4 million
}}
Junior is a 1994 American comedy film written by Kevin Wade and Chris Conrad and directed by Ivan Reitman. It stars Arnold Schwarzenegger as a scientist who undergoes a male pregnancy as part of a scientific experiment. 

==Plot== Food and Drug Administration has not yet approved the drug; so Hesse and Arbogast move forward in their research. In response, Hesse breaks into the laboratory and locks himself in. The head of the review board, Noah Banes (Frank Langella), informs Arbogast that while the FDA has denied their team the right of human experimentation, the team has managed to receive a donation from fellow geneticist Dr. Diana Reddin (Emma Thompson) from the ovum cryogenics department. When Hesse questions the chances of a woman taking an unapproved drug during pregnancy, Arbogast reveals that there is no need to identify the gender of the experiment and convinces Hesse to impregnate himself, using an ovum codenamed "Junior".
 his own water shows no signs of fetal distress. Alex feels the pressure as Arbogast and the other doctor deliver his baby. Arbogast announce the arrival to Reddin, who is on hands and knees helping Angela cope with contractions. Reddin hands Angela over to Arbogast and rushes off to see Alex.  Arbogast has Angela prepped for childbirth.  Reddin visits Hesse in the Post-op, getting her first glance at her baby,  together they decide to name the baby girl, Junior. Arbogast delivers Angelas child and the two reconcile to raise the boy, Jake, as their own.

In the final scene, the two families are on a beach on vacation celebrating the birthdays of Junior and Jake. Reddin is heavily pregnant with her and Hesses second child, and when Angela mentions that she would like to have another baby but does not want to go through pregnancy again, they all begin trying to convince a reluctant Arbogast to carry the child.

==Cast==
* Arnold Schwarzenegger as Dr. Alex Hesse (Research geneticist)
* Danny DeVito as Dr. Larry Arbogast (Obstetrician and gynecologist) 
* Emma Thompson as Dr. Diana Reddin (Research geneticist) 
* Frank Langella as Dr. Noah Banes (Head of research review board) 
* Pamela Reed as Angela
* Aida Turturro as Louise
* James Eckhouse as Ned Sneller
* Megan Cavanagh as Willow
* Christopher Meloni as Mr. Lanzarotta
* Brianna & Brittany McDonnell as Junior
* Ryan & Zachary Doss as Jake

==Reception==

=== Box office ===
In North America the film grossed slightly more than half its budget ($37 million vs $60 million), worldwide it grossed $108 million. 

===Critical response===
The film received mixed to negative reviews with 32% approval rating on aggregate review site Rotten Tomatoes, with an average score of 4.7 out of 10, based on 31 collected reviews. 

Comedian and former Mystery Science Theater 3000 host Michael J. Nelson named the film the second-worst comedy ever made. 

Notably Roger Ebert was a fan of the film, giving it 3½ out of four stars and maintaining that:

 
"I know this sounds odd, but Schwarzenegger is perfect for the role. Observe his acting carefully in Junior, and youll see skills that many serious actors could only envy." 
 

Ebert and his partner Gene Siskel gave the film "two thumbs up" on their television show.

 

In February 2008, Scottish artist Sandy Smith launched an essay-writing competition, asking entrants to attempt to prove that Junior could be considered the greatest movie of all time. He obsessively started collecting copies of the movie in November 2005, and eventually collected 24 copies. Despite being covered in the national press,  the competition received fewer entries than there were prizes offered. The essays submitted, and one commissioned from an academic essay-writing company to Smiths own specifications, are available to read on the competition website -  

==See also== Rabbit Test, a 1978 comedy film with Billy Crystal with a similar premise.

* A Slightly Pregnant Man, an earlier French 1973 film by Jacques Demy with Marcello Mastroianni playing the man who becomes pregnant.

== References ==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 