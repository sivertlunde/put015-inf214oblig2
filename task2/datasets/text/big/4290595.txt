Return to Oz (TV program)
 
{{Infobox film
| name           = Return to Oz
| image          = 
| image size     = 
| caption        = 
| director       = F. R. Crawley Thomas Glynn Larry Roemer    
| producer       = Arthur Rankin Jr.
| writer         = L. Frank Baum (novel) Romeo Muller (teleplay)
| narrator       = 
| starring       = Carl Banas Susan Conway Peggi Loder Susan Morse Larry D. Mann Alfie Scopp
| music          = Gene Forrell James Polack Edward Thomas   
| cinematography = William&nbsp;N.&nbsp;Clark&nbsp;(as&nbsp;Bill&nbsp;Clark) Ron Haines Gary Morgan 
| editing        = 
| studio         = Rankin/Bass Productions Crawley Films
| distributor    = 
| released       = February 9, 1964 (USA)
| runtime        = 51 minutes
| country        =     USA
| language       = English
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}} Crawley Films for Rankin/Bass (Videocraft). It first aired February 9, 1964 in the United States on NBC|NBCs General Electric Color Fantasy Hour.  It was directed by F. R. Crawley, Thomas Glynn, and Larry Roemer from a teleplay by Romeo Muller, who later wrote Dorothy in the Land of Oz.

Crawley Films also produced the earlier 1961 animated series, Tales of the Wizard of Oz and brought similar artistic character renditions to the special.
 Disney film of the same name.

==Production== New York RCA Victor studios.  The animation consisted of 140,000 images drawn by 40 staff members at the Crawley studios in Canada.   

==Plot==
 Dorothy receives Wizard gave Oz again by another Kansas twister, this time not by house, but an apple tree. Once she arrives there she is greeted by the Munchkins in Munchkinville.  Glinda arrives to tell her that the previously melted Wicked Witch of the West has become reconstituted and is wreaking havoc again, having taken Socrates diploma and burned it up, destroyed the heart of the Tin Woodman, called Rusty, by turning herself into a Tin Woman, and dropping him into a pond where he rusted over again.  She has also stolen the medal that belonged to the Cowardly Lion, called Dandy, and turned it into a daisy, and is planning to get Dorothys silver shoes again.

Dorothy sets off to find her friends, without knowing the Wicked Witch is watching them in her Crystal Ball. She finds and oils Rusty who has rusted after the Witch tricked him. They find Socrates in a corn field on a pole scaring crows again and get him down. They find Dandy crying and, after some unexpectedly cruel bullying from Socrates and Rusty, they cheer him up.  After the four friends are reunited, they arrive at the Emerald City, only to be tricked by the Witch, who has captured the Wizard and taken over as the ruler of Oz. The Wizard, who in this continuity is not an Omaha huckster but an Ozite born and bred and the elected ruler of Oz, tells them to destroy her again and he will give them what they want. She arrives back at her castle just before Dorothy and her friends, but before they arrive she sends flying alligators to kill them. Socrates quick thinking saves them as they hide under his straw (a method employed in more than one of the Oz books). Rusty saves them from a lightning bolt by sacrificing himself, which kills him, despite his being made of tin. Dorothy asks Glinda if she will help and a glowing ball brings him back to life. They arrive and are trapped by the Witch. She grabs Dorothy and tries to take her silver slippers. The gang (including the Wizard himself) try to get her back from the Witch, who gives her and Dandy the slippers. Dorothy, who is being held upside-down from the window, tells Dandy that he will turn to stone if he takes them. The Witch takes them only to be turned into stone, crumble, and fall apart. The gang returns to the Emerald City, only to find out that the Wizard is, after all, a humbug, unable as he always was to return Dorothy home. Glinda appears to tell Dorothy the reason that her friends didnt turn to stone was because they had brains, a heart, and courage. She also explains that the Witch was cruel and heartless, brainless enough to think evil could conquer good and cowardly in that she used slaves and suppressed others. Dorothy wishes to go back, and instantly a Kansas twister whisks her and Toto back home to Aunt Em and Uncle Henry again.

==Characters==
The following characters appear in the special, with associated voice actors:

* Dandy Lion (Cowardly Lion) and The Wizard of Oz &ndash; Carl Banas
* Dorothy Gale &ndash; Susan Conway
* Dorothy Gale (singing) &ndash; Susan Morse 
* Glinda, the Good Witch of the North &ndash; Peggi Loder
* Rusty the Tin Man (Tin Woodman) and The Wicked Witch of the West &ndash; Larry D. Mann
* Socrates the Strawman (Scarecrow) &ndash; Alfie Scopp Toto &ndash; Stan Francis

==Video/DVD==
Return to Oz was released on VHS in the late 1980s by Prism Entertainment.  It was released on DVD by Sony Wonder and Classic Media in March 2006.  It had previously been available for syndication, and a few local stations picked it up.

==See also==
 
*The Wizard of Oz (adaptations) — other adaptations of The Wonderful Wizard of Oz

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 