Bad Company (1925 film)
{{Infobox film
| name           = Bad Company
| image          =
| image size     =
| caption        =
| director       = Edward H. Griffith
| producer       = St. Regis Pictures
| writer         = John C. Brownell
| narrator       =
| starring       = Madge Kennedy Bigelow Cooper
| music          =
| cinematography = Marcel Picard Walter Arthur
| editing        =
| distributor    = Associated Exhibitors
| released       = February 15,  
| runtime        = 50 minutes
| country        = United States
| language       = Silent film English intertitles
| budget         =
| preceded by    =
| followed by    =
}}
Bad Company is a lost  1925 drama film directed by Edward H. Griffith and written by John C. Brownell. It stars Madge Kennedy and Bigelow Cooper.  

==Cast==
*Madge Kennedy - Gloria Waring
*Bigelow Cooper - Peter Ewing
*Conway Tearle - James Hamilton
*Lucille Lee Stewart - Teddy Lamont
*Charles Emmett Mack - Dick Reynolds

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 


 