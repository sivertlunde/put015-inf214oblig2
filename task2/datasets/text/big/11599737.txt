26 Years Diary
{{Infobox film
| name           = 26 Years Diary
| image          = 26 Years Diary movie poster.jpg
| caption        = Japanese theatrical poster
| director       = Hanadō Junji
| producer       = Junichi Mimura Atsuko Yamakawa Akifumi Sugihara
| based on       =  
| screenplay     = Hanado Junji
| starring       = Lee Tae-sung Mākii Naoto Takenaka Takatoshi Kaneko Chung Dong-hwan
| music          = Masayoshi Sugimura
| editing        = Hirohide Abe
| cinematography = Ryu Segawa
| distributor    = Sony Pictures International Releasing
| released       =  
| runtime        = 131 minutes
| language       = Japanese Korean
| country        = Japan South Korea
| budget         =
| gross          = $3,614,835 
}}
26 Years Diary ( : 너를 잊지 않을거야 Neoreul Ijji Nanheulgeoya; literally "I Wont Forget You") is a biopic that tells the story of Lee Su-hyons  life and death.

The film details the 26-year-old Korean students experiences in Japan, including going to school and his developing romance with a Japanese student (played by Mākii). He died on January 21, 2001, along with a Japanese photographer, Shiro Sekine, while both were trying to save the life of a man who had fallen onto the tracks at the Shin-Ōkubo Station in Tokyo.

==Plot==
The film is based on a true story, Soo-hyun (Lee Taesung) travels from Korea, studying in Japan he meets Yuri (High and Mighty Colors lead singer, Maakii). Both share similar interests in music and sports as they become closer while dealing with language and racial barriers.

==Cast==
*Lee Tae-sung - Lee Soo-hyun  Maki Onaga - Yuri Hoshino
*Takatoshi Kaneko - Ryuji Kazama
*Naoto Takenaka - Kazuma Hirata
*Junko Hamaguchi - Rumiko Okamoto (Miruki)
*Seo Jae-kyung
*Hideko Hara - Humie Hoshino
*Miho Yoshioka - Asako Kojima
*Jung Dong-hwan - Soo-hyuns father
*Lee Kyung-jin - Shin Yun-chan 
*Lee Seol-ah - Lee Su-geon 
*Hong Kyung-min
*Lou Oshiba

== References ==
 

== External links ==
*  
* 

 
 
 
 
 
 
 


 
 
 