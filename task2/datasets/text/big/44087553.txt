La Belle Meunière
{{Infobox film
| name           = La Belle Meunière
| image          =
| image size     =
| caption        =
| director       = Marcel Pagnol
| producer       = 
| writer         = Marcel Pagnol
| narrator       =
| starring       = 
| music          = 
| cinematography = 
| editing        =
| distributor    =
| released       =     
| runtime        =
| country        = France
| language       = French
| budget         =
}}
La Belle Meunière is a 1948 French musical film directed by Marcel Pagnol.

==Synopsis==
Composer  ,  He meet a girl, Brigitte, near a windmill.   However, the local lord wants her as his lover.   Distraught, Schubert leaves and writes some new music to get over it.  

The film was shot with Rouxcolor technology. Brett Bowles, Marcel Pagnol, Manchester: Manchester University Press, p. 4 

==Cast==
*Tino Rossi as Franz Schubert. 
*Jacqueline Pagnol as Brigitte. 
*Raoul Marco as Maître Guillaume. 
*Lilia Vetti as the Countess. 
*Raphael Patorni as Count Christian. 
*Emma Lyonel as the Baroness. 
*Suzanne Desprès as the lavender-girl. 
*Roger Monteaux. 
*Christian Bertola. 
*Jean-Paul Coquelin. 
*Pierrette Rossi. 
*Gustave Hamilton. 
*Edouard Hemme. 
*Jules Dorpe. 

==References==
 

 

 
 
 
 
 


 
 