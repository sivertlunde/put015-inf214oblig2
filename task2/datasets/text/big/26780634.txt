Le Havre (film)
{{Infobox film
| name          = Le Havre
| image         = Le Havre poster.jpg
| caption       = Festival poster
| director      = Aki Kaurismäki
| producer      = Aki Kaurismäki
| writer        = Aki Kaurismäki
| starring      = André Wilms Kati Outinen Jean-Pierre Darroussin Blondin Miguel
| cinematography= Timo Salminen
| editing       = Timo Linnasalo
| studio        = Sputnik Pyramide Productions Pandora Film
| distributor   = Future Film Distribution
| released      =  
| runtime       = 93 minutes
| country       = Finland France Germany
| language      = French
| budget        = € 3.8 million
| gross         = $12,944,958	 	    
}} La Vie de Bohème from 1992.
 FIPRESCI Prize. Kaurismäki envisions it as the first installment in a trilogy about life in port cities. His ambition is to make follow-ups set in Spain and Germany, shot in the local languages. 

==Plot==
Marcel Marx, a former bohemian and struggling author, has given up his literary ambitions and relocated to the port city Le Havre. He leads a simple life based around his wife Arletty, his favourite bar and his not too profitable profession as a shoeshiner. As Arletty suddenly becomes seriously ill, Marcels path crosses with an underage illegal immigrant from Africa. Marcel and friendly neighbors and other townspeople help to hide him from the police.  The police inspector may, or may not, be hot on their heels. 

==Cast==
 
* André Wilms as Marcel Marx
* Kati Outinen as Arletty
* Jean-Pierre Darroussin as Monet
* Blondin Miguel as Idrissa
* Elina Salo as Claire
* Evelyne Didi as Yvette
* Quoc Dung Nguyen as Chang
* Laika as Laika
* François Monnié as Grocer 
* Roberto Piazza as Little Bob 
* Pierre Étaix as Doctor Becker 
* Jean-Pierre Léaud as Denouncer

==Production==
Kaurismäki had the idea of a film about an African child who arrives in Europe three years before the production started.  His original intention was to set the story on the Mediterranean coast, preferably in Italy or Spain, but he had difficulties finding a suitable city. According to Kaurismäki, he "drove through the whole seafront from Genoa to Holland", and eventually settled on Le Havre in northern France, which attracted him with its atmosphere and music scene.      
 La Vie de Bohème, where he also was played by André Wilms. The character Monet was inspired by Porfiry Petrovich, the detective from Fyodor Dostoyevskys Crime and Punishment. 
 Elvis of this Kingdom as long as Johnny Hallyday stays in Paris and even then it would be a nice fight."  Filming started 23 March and ended 12 May 2010. 

==Release== 64th Cannes Drifting Clouds, The Man Without a Past and Lights in the Dusk.  The Finnish premiere is set to 9 September 2011 through Future Film Distribution.  Pyramide Distribution will release it in France on 21 December.  Janus Films acquired the American distribution rights. 

===Critical response===
Rotten Tomatoes gives the film a "fresh" score of 99% based on reviews from 71 critics.   

Leslie Felperin wrote in   (obvious, given the protagonists names), Jean-Pierre Melville, Robert Bresson and others. But on its own terms, Le Havre is a continual pleasure, seamlessly blending morose and merry notes with a deftness thats up there with Kaurismakis best comic work." Felperin complimented the craft of Kaurismäkis regular cinematographer Timo Salminen and editor Timo Linnasalo, and wrote: "Its like listening to a band thats been cheerfully churning it out for years, whose members all know each others timings inside out, not unlike onscreen performers Little Bob and his grizzled, perfectly in-sync crew." 

===Accolades=== FIPRESCI Prize Special Mention Palm Dog Best Foreign Language Film at the 84th Academy Awards,       but it did not make the final shortlist.    Le Havre also won the Gold Hugo at the Chicago International Film Festival.

==See also==
* List of submissions to the 84th Academy Awards for Best Foreign Language Film
* List of Finnish submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 