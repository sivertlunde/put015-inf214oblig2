Panic in the Parlour (1956 film)
 
{{Infobox film 
| name = Panic in the Parlour
| image = Sailor Beware 1956 Film.jpg
| caption        = DVD cover Gordon Parry 
| writer = Falkland L Cary Philip King  Ronald Lewis Gordon Jackson Geoffrey Keen
| producer = Jack Clayton
| distributor = Independent Film Dist. (UK)  Distributors Corporation of America (US)
| studio = Remus Films Romulus Films
| released =  
| runtime = 81 minutes
| country = United Kingdom
| language = English
| gross = £221,779 (UK) 
}}
 Gordon Parry. It was released as Sailor Beware! in the United States.  

It follows the story of a sailor betrothed to be married, but wary that home-life may echo that of her parents: a hen-pecked husband and battle-axe mother.

It was one of Michael Caines very first films, where he has a small, uncredited role as a sailor.

==Cast==
* Peggy Mount	 ... 	Emma Hornett
* Shirley Eaton	... 	Shirley Hornett Ronald Lewis	... 	Albert Tufnell Cyril Smith	... 	Henry Hornett
* Esma Cannon	... 	Edie Hornett Gordon Jackson ...   Carnoustie Bligh
* Geoffrey Keen	... 	Rev. Mr. Purefoy
* Joy Webster	... 	Daphne Pink
* Thora Hird	... 	Mrs. Lack
* Eliot Makeham	... 	Uncle Brummell
* Fred Griffiths	... 	Taxi Driver
* Edie Martin	... 	Little Woman in Church
* Margaret Moore	... 	Little Girl
* Barbara Hicks ... Little Girls Mother George Rose   ...   Waiter at Banfields
* Frank Atkinson	... 	Chauffeur (uncredited)
* Alfie Bass	... 	Organist (uncredited)
* Richard Beynon	... 	Bearded Sailor (uncredited)
* Douglas Blackwell  ... 	Co-op Man (uncredited)
* Anne Blake	... 	(uncredited)
* Michael Caine	... 	Sailor (uncredited)
* Peter Collingwood	... 	Verger (uncredited)
* George A. Cooper	... 	Petty Officer (uncredited)
* Paul Eddington	... 	Bearded Sailor (uncredited)
* Charles Houston	... 	(uncredited)
* Jack MacGowran	... 	Toddy (uncredited)
* Henry McGee	... 	Milkman (uncredited)
* John Pike	... 	Bit Part (uncredited)
* Anthony Sagar	... 	Naval Rating (uncredited)

==Reception==
The film was one of the ten most popular films at the British box office in 1956. BRITISH. FILMS MADE MOST MONEY: BOX-OFFICE SURVEY
The Manchester Guardian (1901-1959)   28 Dec 1956: 3 
==References==
 

==External links==
* 

 
 
 
 
 
 
 

 

 