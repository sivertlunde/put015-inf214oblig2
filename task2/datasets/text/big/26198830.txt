Big Man on Campus
{{Infobox film
| name           = Big Man on Campus
| image          = 
| image_size     = 
| caption        = 
| director       = Jeremy Kagan
| writer         = 
| narrator       = 
| starring       = Allan Katz
| music          = 
| cinematography = 
| editing        = 
| distributor    =  
| released       = 1989
| runtime        = 105 min.
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}} Corey Parker. 

==Plot==

The film opens with a news broadcast on the apparent sighting of a "mysterious creature" on the UCLA campus. Among those interviewed are underachieving student Alex Kominski (Corey Parker) and his girlfriend Cathy Adams (Melora Hardin). Although neither claim to believe in the creatures existence, a hunchbacked figure (Allan Katz) is shown looking down from the bell tower, spying on Cathy through a telescope.

While attending a renaissance-themed carnival on campus, Alex gets involved in a scuffle after insulting another students girlfriend. When Cathy interjects herself into the fight, the creature suddenly comes to her defense. He motions affectionately toward Cathy, but is chased away by campus security, who manage to corner and capture him. After a cursory examination, the creature is brought to trial, where psychiatrist Dr. Victoria Fisk (Jessica Harper) is quick to label him as a menace to society. Cathy refutes the doctors opinion, stating that the creature was only trying to protect her. Another witness, Dr. Richard Webster (Tom Skerritt), the head of the universitys Psychology department, suggests it might be possible to rehabilitate the creature.

Finally the hunchback himself is called to the stand, where Dr. Fisk asks him who he believes is better qualified to determine his fate, Dr. Webster or herself. Demonstrating that the primitive creature can only repeat the last thing he hears spoken, he predictably answers, "Dr. Fisk". She then further humiliates him by prompting him to describe himself as a "complete and total fool". Given the evidence, Judge Ferguson (John Finnegan) orders that the hunchback be confined in a mental facility. When Cathy protests, the creature stands and acknowledges both her and the judge by name, leading the judge to overturn his ruling and award temporary custody to the university under the supervision of Dr. Webster, with the condition that should the creature exhibit any violent behavior, he will be institutionalized.

Upon being escorted back to the university, the creature insists on showing Alex, Cathy, and Dr. Webster his home in the bell tower, cluttered with various objects scavenged from around campus. Reluctant to leave him alone in the tower, Cathy encourages Alex to stay and watch over him, with Dr. Webster offering to get him special consideration for the task from his instructors at the university. Left with little choice in the matter, Alex agrees.

Over the next few days, the hunchback undergoes a series of observational tests, including speech therapy with Dr. Diane Girard (Cindy Williams). Despite his unusual behavior, he learns quickly, and with time he and Alex eventually become friends. During a session with Dr. Webster, the creature is asked to choose his own name. After settling on "Bob Maloogaloogaloogaloogalooga", he explains that after his father deserted them and his mother died from illness, a group of people came and locked him away. He then managed to escape custody and take up residency in the bell tower.

Despite all his progress, Bob remains infatuated with Cathy. While Alex struggles to catch up on his studies in order to pass his finals, Bob strives to better himself in order to win Cathys affections, even using Dr. Girard to refine his romantic technique. But when he suddenly presents Cathy with an engagement ring, she has no choice but turn Bob down. Heartbroken, Bob returns to the bell tower where he receives a phone call from Dr. Fisk. Desperate to prove her theory that Bob is a danger to others, she falsely tells him that Cathy was injured in an accident. Bob immediately races across campus to the girls dormitories, but is confused to find Cathy alive and well. With campus security closing in on him, Bob flees into the streets of Los Angeles.

Alex and Cathy and the rest of Bobs supporters then appear on a controversial talk show hosted by Stanley Hoyle (Gerrit Graham), who attempts to demonize Bob in every way possible. When Hoyle states that Alex and the others are "in big trouble" for supporting the hunchback, Bob, who has been watching the program on television, is prompted to come to his friends aid. After taking a taxi to the studio, he makes his way in through the roof and swings down from the catwalk to snatch up Dr. Fisk. Forced to confess her lie in front of everyone, the live audience quickly turns to Bobs side, chanting his name. Stanley Hoyle then apologizes to Bob for his slanderous remarks, and Dr. Webster suggests looking into a scholarship for Bob at the university. When asked by Hoyle if he would have done anything different up to this point, Bob takes Dr. Girard into his arms and kisses her at "two speeds", a reference to an intimate experience Bob once had with a Hoover vacuum cleaner. Bob then smiles sheepishly into the camera as the film ends.

==Cast==
*Allan Katz as Bob Maloogaloogaloogaloogalooga Corey Parker as Alex
*Cindy Williams as Dr. Diane Girard
*Melora Hardin as Cathy
*Gerrit Graham as Stanley Hoyle
*Tom Skerritt as Dr. Richard Webster
*Jessica Harper as Dr. Victoria Fisk
*John Finnegan as Judge Ferguson

==References==
 

==External links==
* 

 

 
 
 