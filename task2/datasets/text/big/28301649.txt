Raising Hell (film)
 
{{Infobox film
| name           = Raising Hell
| image          =
| alt            =  
| caption        = 
| director       = Ed Webb-Ingall
| producer       = 
| writer         = 
| starring       = 
| music          = 
| cinematography = 
| editing        = Dettie Gould
| studio         = 
| distributor    = 
| released       =  
| runtime        = 31 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}} Jeffrey Weeks, psychologist Susan Golombok and family lawyer Gill Butler.

==Production==
Research began in 2009 with Webb-Ingall seeking to "profile the experiences of young adult and adult children of lesbian and gay parents in the United Kingdom, starting back in the late 1960s".  The half-hour documentary explores "themes of school, gender, sexuality, prejudice, and what the word family means   children of lesbian and gay parents in the United Kingdom, aged 12 to 35". 

===Style===
Webb-Ingall made the film with the dual purpose of investigating the "personal and political history of kids with LGBT parents   himself a lesson in the film theory and techniques of queer filmmakers of the 70s, 80s and 90s".  This stylistic decision was not for nostalgic purposes, but to instead "embed the film in a wider queer history,   the film  ". 

===Interviewed Subjects===
The film was made with children in mind. Though parents and parenthood are strong themes, they are evoked by situating the kids in a social and political context; no parents actually appear in the film. Webb-Ingall was interested in demonstrating that "whatever   felt about their families,   didnt want to change or hide them, but   proud of who and what   have made them". 
 Jeffrey Weeks was the first academic subject Webb-Ingall approached. Weeks, currently the Executive Dean of Arts and Human Sciences at London South Bank University, has been on the editorial board of several journals, including the Journal of the History of Sexuality and the Journal of Homosexuality. He is among the school of academics that emerged from the Gay Liberation Front (GLF), which he joined in 1970.  He coined the phrase "families of choice", a concept Webb-Ingall explores in his film.

Webb-Ingall next approached Susan Golombok, head of the Centre for Family Research at the University of Cambridge. Her research "focuses on new family forms with an emphasis on parent-child relationships and childrens social, emotional and gender development in lesbian mother families". 
 British "solicitor since 1979   represented lesbians and gay men for almost 30 years. In the 1980s and 1990s,   represented many lesbian mothers fighting for custody of their children and gay fathers seeking contact". 

==Release==
Raising Hell is still awaiting a release date, but has shown at the British Film Institute as part of Film Londons Best of Boroughs Film Fund competition. The film is pending contribution to the Lesbian Herstory Archives in Brooklyn, New York. The film will next be shown at the Voices of Children Conference on September 8, 2010, in Dublin, Ireland. Following the screening of his film, Webb-Ingall will take part in a panel discussion about the experiences of young people and children growing up with LGBT parents in Ireland. 

==See also==
*Male egg
*Same-sex marriage and the family
*Sperm donation
*Surrogacy
*Third party reproduction
*LGBT rights in Europe
*Artificial insemination
*Assisted reproductive technology
*Female sperm
*In vitro fertilisation
*LGBT adoption

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 