Detour (1967 film)
 
{{Infobox film
| name           = Detour
| image          = 
| caption        = 
| director       = Grisha Ostrovski Todor Stoyanov
| producer       = Hristo Karaneshev
| writer         = Blaga Dimitrova
| starring       = Nevena Kokanova
| music          = 
| cinematography = Todor Stoyanov
| editing        = 
| distributor    = 
| released       =  
| runtime        = 78 minutes
| country        = Bulgaria
| language       = Bulgarian
| budget         = 
}}

Detour ( , Transliteration|translit.&nbsp;Otklonenie) is a 1967 Bulgarian drama film directed by Grisha Ostrovski and Todor Stoyanov. It was entered into the 5th Moscow International Film Festival where it won the Special Golden Prize and the Prix International Federation of Film Critics|FIPRESCI.   

==Cast==
* Nevena Kokanova as Neda
* Ivan Andonov as Boyan
* Katya Paskaleva as Vera
* Stefan Ilyev as Kosta
* Dorotea Toncheva as Pavlina
* Tzvetana Galabova as Lili ot muzeya
* Lyuben Zhelyazkov as Priyatelyat ot kafeneto
* Dora Markova as Selyankata s praskovite
* Svetoslav Peev as Asistentat
* Dimitar Lalov as Zaekvashtiyat
* Nikolai Ouzounov as Yacho
* Nencho Yovchev as Varadin

==References==
 

==External links==
*  

 
 
 
 
 
 
 