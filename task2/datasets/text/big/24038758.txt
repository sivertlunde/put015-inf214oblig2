Newsboys' Home
{{Infobox film 
| name           = Newsboys Home
| image          = newsboyshome.jpg
| image_size     = 
| caption        = Theatrical Poster Harold Young
| producer       = Ken Goldsmith
| writer         = Gordon Kahn (story and screenplay) Charles Grayson (story)
| starring       = Little Tough Guys Jackie Cooper
| music          = Frank Skinner
| cinematography = Milton R. Krasner John W. Boyle
| editing        = Philip Cahn
| distributor    = Universal Studios
| released       =  
| runtime        = 73 minutes
| country        = United States 
| language       = English
| budget         = 	
}} The Little Tough Guys.

==Plot==
When his father, a small town sheriff, is slain by a big city gangster, "Rifle" Edwards (Jackie Cooper) becomes a homeless vagabond, drifting from town to town. Arriving broke and hungry in a large metropolis, he seeks food and shelter at the Newsboys Home, where the kids force him to fight an amateur bout with the champ, Danny Shay (Elisha Cook, Jr.), before he can eat. When Rifle knocks Danny out, the country boy is accepted into the gang of newsies. He goes to work selling the Globe, which is published by Howard Price Dutton (Samuel S. Hinds), the founder and benefactor of the home. When Dutton dies, his daughter Gwen (Wendy Barrie) becomes the new publisher. Globe reporter Perry Warner (Edmund Lowe) is in love with Gwen, but they quarrel over her ideas about turning the Globe into a highbrow paper. Perry warns Gwen that she will ruin the paper, but she is stubborn and refuses to listen. 

Meanwhile, Tom Davenport (Irving Pichel), a crooked politician, buys the opposition paper, the Star, in order to swing the election for his candidates, and tries to bribe Perry to work for him. After Perry refuses, Davenport starts a ruthless circulation war, and Globe sales begin to fall off dramatically. Gwen still refuses to heed Perrys advice and abandon her disastrous editorial policies, and in frustration, Perry quits and leaves on a trip. When the Globe can no longer support the Newsboys Home, Danny and some of the boys go to work for Bartsch (Horace McMahon) on the Star, leaving only Rifle and Sailor (Harris Berger) behind at the Globe. Perry returns to find the Globe in dire straights and Gwen tearfully refutes her policies. Assuming editorship of the paper, Perry sets out to whip the Star at its own game. 

Growing bolder, Davenport hires mobster Francis Barber (Edward Norris) to escalate the circulation war, and Globe trucks are wrecked, newsstands smashed and burned. The war comes to a climax when a street fight erupts during which boys from the two rival papers meet in open combat, and police squads are required to quell the riot. Angered because one of his pals has been shot by one of Barbers men, Danny goes to Barber to quit his job while Rifle follows the gangsters to Barber, whom he recognizes as his fathers killer. Barber and his men are preparing to take Rifle "for a ride" when Danny and the newsboys stage a sensational rescue in which they take Barber prisoner and turn him over to the police. With the newspaper war brought to a close, the Globe regains its popularity and Gwen and Perry are married.

==Cast==
===The Little Tough Guys===
* Harris Berger as Sailor
* Hally Chester as Murphy
* Charles Duncan as Monk
* David Gorcey as Yap
* William Benedict as Trouble

===Additional cast===
* Jackie Cooper as "Rifle" Edwards
* Edmund Lowe as Perry Warner
* Wendy Barrie as Gwen Dutton
* Edward Norris as Francis Frankie Barber
* Samuel S. Hinds as Howard Price Dutton
* Irving Pichel as Tom Davenport
* Elisha Cook, Jr. as Danny Shay
* Harry Beresford as ODowd
* Horace McMahon as Bartsch
* George McKay as Hartley

==External links==
*  

 

 
 
 
 
 
 
 
 
 


 