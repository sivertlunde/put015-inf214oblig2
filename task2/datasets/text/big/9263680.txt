Juego de Niños
 
 
{{Infobox Film name            =Juego de niños director        =Leopoldo Laborde producer        =Leopoldo Laborde writer          =Leopoldo Laborde starring        ={{Plainlist|
*Francisco Rey (as Francisco Ruíz and as Francisco Porcallo)
*Alain Rangel
*Angélica Consuegra
}} music           =Leopoldo Laborde cinematography  =Leopoldo Laborde editing         =Leopoldo Laborde studio          =Omicrón Films distributor     =Vanguard Cinema (USA) released        =  runtime         =94 minutes country         =Mexico language        =Spanish
}}
 analog video, from the then experimental era of Mexican director Leopoldo Laborde.

==Plot==
A string of serial killings among the children of Mexico City is sending shivers through the community. The story follows a child who holds key to solving the murders and is in danger.

==Cast==
*Francisco Porcallo (Francisco Rey) as Lalo, the protagonist
*Alain Rangel as Miguel, the antagonist
*Angélica Consuegra as La Maestra, Lalos teacher
*Angelina Neria as La Madre, Lalos mother
*Sheilla Lissette as La Hermana, Lalos sister
*Rogelio Castillo as El Hermano, Lalos brother
*Luis Fernanado Ruíz as El Amigo, one of Lalos other schoolmates, perhaps Pablito
*Esteban Mireles as Comandante Sanchez, the detective investigating the murders
*Marilú Carrillo as Dra. Montemar, Sanchezs office associate
*Victor Vera as Judicial, Sanchezs field associate
 extras and victim in the opening credits.

==Crew==
{| class="sortable" style="text-align:left;width:75%;font-size:95%"
|-
! Member !! class="unsortable" | Credit !! 
|-
|  }} ||  , casting (I) ,  ,  ,  ,  ,  ,  ,  ,  ,   || style="text-align:center" | 1
|-
|  }} ||  ,  ,  ,   || style="text-align:center" | 2
|-
|  }} ||  ,  ,  ,  ,   || style="text-align:center" | 3
|-
|  }} ||   || style="text-align:center" | 4
|-
|  }} ||   || style="text-align:center" | 5
|- conductor (I)  || style="text-align:center" | 6
|-
|  }} || casting (II)  || style="text-align:center" | 7
|-
|  }} || casting (III) ,   || style="text-align:center" | 8
|-
|  }} ||   || style="text-align:center" | 9
|-
|  }} ||  ,   || style="text-align:center" | 10
|-
|  }} ||  ,   || style="text-align:center" | 11
|-
|  }} ||  , props (III)  || style="text-align:center" | 12
|-
|  }} || props (II)  || style="text-align:center" | 13
|-
|  }} ||   || style="text-align:center" | 14
|-
|  }} ||  ,   || style="text-align:center" | 15
|-
|  }} ||   || style="text-align:center" | 16
|-
|  }} ||   || style="text-align:center" | 17
|-
|  }} ||   || style="text-align:center" | 18
|-
|  }} || conductor (II)  || style="text-align:center" | 19
|-
|    }}  || stunt || style="text-align:center" | 20
|-
|  }} ||   || style="text-align:center" | 21
|-
| Videonics ||   || style="text-align:center" | 22
|}

==See also== The Good Son
*Relative Fear

==External links==
* 
* 
*  at Vanguard Cinema, the distributor
*  at Barnes & Noble, with information about the DVDs contents

 
 
 
 
 
 


 
 