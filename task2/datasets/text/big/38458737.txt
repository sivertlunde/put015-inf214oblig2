Death and Glory in Changde
{{Infobox film
| name = Death and Glory in Changde
| image = Death and Glory in Changde.jpg
| alt = 
| caption = Film poster
| film name = {{Film name| traditional = 喋血孤城
| simplified = 喋血孤城
| pinyin = Dié Xuè Gū Chéng}}
| director = Shen Dong
| producer = Chen Weiping Shen Dong Han Sanping Chen Jiancheng Zhou Pixue
| writer = Ma Weigan Lian Xiufeng Shen Dong Peng Daocheng
| starring = Ray Lui Yuan Wenkang Ady An
| music = 
| cinematography = Yu Xuejun
| editing = Yang Xiaoying
| studio = August First Film Studio
| distributor = China Film Group Corporation
| runtime = 94 minutes
| released =  
| country = China
| language = Mandarin Japanese
| budget = 
| gross = 
}}
Death and Glory in Changde is a 2010 Chinese war film based on the events in the Battle of Changde in 1943 during the Second Sino-Japanese War. The Chinese title of the film literally means "bloodbath, isolated city". Directed by Shen Dong, the film starred Ray Lui, Yuan Wenkang and Ady An in the lead roles. The film was released in mainland China on 19 August 2010.

==Plot== Shashi and Yangtze and Xiang rivers, and surrounds the Chinese city. Yu Chengwan, a division commander in the Chinese National Revolutionary Army, and his men have received orders to defend Changde to the death. Most of Changdes civilian population have evacuated, leaving behind Yu and his troops to defend the isolated city.
 Miao youth, to join his company. Erhu, who turns out to be an excellent sharpshooter, looks up to Feng as a role model. The Chinese forces fight bravely on the frontline, holding off wave after wave of Japanese troops outside Changde until all the external defences have fallen. They retreat into the city and continue to prevent the invaders from advancing beyond the gates. By then, the defenders are already running low on ammunition and supplies but they continue to put up fierce resistance, to the point of secretly salvaging unexpended rounds from the bodies of dead soldiers at night. Fengs fiancee, a singer called Wanqing, is determined to accompany him, so she becomes a nurse and helps to attend to the wounded. Feng and Wanqing are married and they spend one night together.
 chemical and biological weapons against the Chinese forces. As the defenders retreat further into the city, fierce fighting takes place in the streets and alleys. Erhus elder sister is killed by a Japanese soldier who breaks into their house. In one skirmish, Feng is shot and mortally injured when he tries to use a bundle of explosives to kill a Japanese machine gunner in a concealed position. He succeeds in his attempt but dies in the ensuing blast. In the meantime, the Japanese have captured the Chinese medical officer and the wounded Chinese soldiers and made them line up near the city gate while handing out a Japanese flag to each of them. The Japanese attempt to record these "scenes of victory" on camera, but eventually have all the prisoners gunned down when they refuse to play along with their captors.

Yu Chengwan writes a farewell letter to his wife before donning his battlegear and joining his surviving men as they fight their way out of Changde. Off screen, it is stated that only 83 of the 8,000 defenders survived. The Chinese defeated the Japanese and retook Changde a few days later. Yu was court-martialled for abandoning his post at Changde and was sentenced to two years imprisonment, but was acquitted and released after spending four months in prison.

==Cast==
* Ray Lui as Yu Chengwan, the division commander of the 57th Division, 74th Corps.
* Yuan Wenkang as Feng Baohua, a company commander in the 57th Division.
* Ady An as Wanqing, Feng Baohuas fiancee. Miao youth who joins Feng Baohuas company.
* Yang Zi as Taoer, Erhus elder sister.
* Liu Yijun as Chen Xuyun, the deputy division commander of the 57th Division.
* Yang Hanbin as Chai Yixin, the chief of staff in the 57th Division and regiment commander of the 169th Regiment.
* Jin Chi as Deputy Li
* Fan Lei as Battalion Commander Ruan, the commander of the battalion at Hefushan.
* Xu Guangyu as Battalion Commander Zhao, the commander of the battalion at Deshan.
* Peng Bo as Dr. Zhang, the medical officer.
* Zhou Jianpeng as Deng Xianfeng, the regiment commander of the 188th Regiment, 63rd Division, 100th Corps.
* Hatae Kiyoshi as Yokoyama Isamu, the commander of the Japanese 11th Army.
* Miura Kenichi as Iwanaga Ō, a Japanese regiment commander.
* Shibuya Tenma as Itō Saburo, a Japanese military officer.

==Reception==
Matthew Lee of Twitch Film wrote:

{{blockquote|
Its certainly melodramatic, simplistic and overtly nationalist, but its also a pleasing ninety minutes or so of fairly undemanding, handsomely produced period action with a couple of particularly explosive setpieces, and it doesnt whitewash history anywhere near as much as you might expect. ... The cast give solid, if unexceptional performances across the board, all of which lends the whole thing a simple humanity most main melody films dont even bother with.
  
}}

==References==
 

==External links==
*  

 
 
 
 
 
 
 