Gandeevam
{{Infobox film
| name           = Gandeevam
| image          =
| caption        =
| writer         = Satyanand  
| story          = V.M.C. Hanesha
| screenplay     = Priyadarshan
| producer       = Satyam Babu Sampath Kumar
| director       = Priyadarshan Roja Srividya
| music          = M. M. Keeravani
| cinematography = S. Kumar
| editing        = N. Gopalakrishnan
| studio         = Sri Lakshmi Narasimha Combines
| released       =  
| runtime        = 154 minutes
| country        = India
| language       = Telugu
| budget         =
| gross          =
}}

Gandeevam is a 1994 Telugu cinema|Telugu, drama film produced by  Satyam Babu, Sampath Kumar on Sri Lakshmi Narasimha Combines banner, directed by Priyadarshan.  Starring Akkineni Nageswara Rao, Nandamuri Balakrishna, Roja (actress)|Roja, Srividya in lead roles.  This is the first and the only film in which Malayalam Superstar Mohanlal appeared on Telugu screen, he did a cameo in a song "Goruvanka Vaalagane" which was one of the best compositions of the music director M. M. Keeravani.

== Plot ==
The story opens with Chakravarthy (Akkineni Nageswara Rao), a millionaire who has everything in his life. He is a successful businessman. He has achieved everything in his life but he doesnt have a successor to enjoy his wealth. He breaks up his relation with his wife suspecting her to have an affair with his colleague where he was working 20 years ago. During the breakup period his wife (Srividya) was a pregnant. He repents for losing her and his child. One fine day three people Nandamuri Balakrishna, Roja, Brahmanandam enter into his life saying that they are his children. The remaining story is how chakravarthy finds out his child from the three with the help of Nagayya (Nagesh), his personal assistant.

==Cast==
 
* Akkineni Nageswara Rao as Chakravarthy
* Nandamuri Balakrishna as Raja
* Mohanlal as Captain(Cameo) Roja as Roja Rupini as Rekha
* Swapna as Rani
* Srividya as Parvathi
* Captain Raju as Michael 
* Mukesh Rishi as Inspector Veeru Pentaiah
* Nagesh as Idea Appa Rao		
* Allu Ramalingaiah	as Chakravarthys Uncle
* Brahmanandam
* Giri Babu	as Chakravarthys brother-in-law	
* Narra Venkateswara Rao
* Chalapathi Rao as Chakravarthys brother	
* Ananth as Peon
* Jaya Bhaskar as Seshu				
* Y. Vijaya as Chakravarthys Sister
 

== Soundtrack ==
{{Infobox album
| Name        = Gandeevam
| Tagline     = 
| Type        = film
| Artist      = M. M. Keeravani
| Cover       = 
| Released    = 1994
| Recorded    = 
| Genre       = Soundtrack
| Length      = 24:01
| Label       = Akash Audio
| Producer    = M. M. Keeravani
| Reviews     =
| Last album  = Muddula Priyudu   (1994)  
| This album  = Gandeevam   (1994)
| Next album  = Bobbili Simham   (1994)
}}

Music composed by M. M. Keeravani. All songs are blockbusters. Music released on Akash Audio Company.
{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 24:01
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = yes
| music_credits =

| title1  = Hai Thisaade Debba
| lyrics1 = Veturi Sundararama Murthy SP Balu,K. Chitra
| length1 = 4:21

| title2  = Chi Chi Chi
| lyrics2 = Veturi Sundararama Murthy
| extra2  = SP Balu,Chitra
| length2 = 4:41

| title3  = Goruvanka Vaalaga
| lyrics3 = Veturi Sundararama Murthy
| extra3  = SP Balu,Chitra,M. G. Sreekumar
| length3 = 4:50

| title4  = Siri Siri Poola
| lyrics4 = Veturi Sundararama Murthy
| extra4  = SP Balu,Chitra,M. M. Keeravani
| length4 = 3:41

| title5  = Mama Baba Mama
| lyrics5 = Veturi Sundararama Murthy 
| extra5  = SP Balu,Chitra
| length5 = 5:44

| title6  = Tadi Gudi Mudi Padi  Sirivennela Sitarama Sastry   
| extra6  = SP Balu,Chitra
| length6 = 4:45
}}

==Others== Hyderabad

== References ==
 

== External links ==

 

 
 
 
 


 