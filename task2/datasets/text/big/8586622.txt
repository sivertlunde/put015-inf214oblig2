Planet Terror
{{Infobox film
| name           = Planet Terror
| image          = Planet-terror-1.jpg
| caption        = One of the theatrical promotional posters
| director       = Robert Rodriguez
| producer       =  
| writer         = Robert Rodriguez
| starring       = {{plainlist|
* Rose McGowan Freddy Rodriguez
* Michael Biehn
* Jeff Fahey
* Josh Brolin
* Marley Shelton
* Stacy Ferguson
* Bruce Willis }} 
| music          = Robert Rodriguez
| cinematography = Robert Rodriguez
| editing        = Robert Rodriguez Ethan Maniquis
| studio         =  
| distributor    = Dimension Films
| released       =  
| runtime        = 106 minutes 109 minutes (unedited version)
| country        = United States
| language       = English Spanish
}} action horror horror science-fiction directed by zombie film Freddy Rodriguez, Stacy Ferguson and Bruce Willis. It was released theatrically in North America as part of a double feature with Quentin Tarantinos Death Proof under the title Grindhouse (film)|Grindhouse, to emulate the experience of viewing exploitation films in a "grindhouse" theater. In addition to directing the film, Rodriguez wrote the script, directed the cinematography, wrote the musical score, co-edited, and produced it.

Released on April 6, 2007, Grindhouse ticket sales were significantly below box office analysts expectations, despite mostly positive reviews. Outside the U.S and released separately, Planet Terror and Death Proof screened in extended versions.   Two soundtracks were also released for the features and include music and audio snippets from the film. Planet Terror was released on DVD in the United States and Canada on October 23, 2007.

==Plot== Freddy Rodriguez) bisexual anesthesiologist wife Dakota (Marley Shelton) at a local hospital.
 Stacy "Fergie" syringe needles, stabbing her repeatedly in the hands, rendering them useless, before locking her in a closet to tend to other patients, including Cherry, who is still alive.

El Wray is detained by Sheriff Hague based on past encounters between the two men. As the patients transform into zombies, El Wray leaves the police station and arrives at the hospital, attaching a wooden table leg to Cherrys stump. As El Wray and Cherry fight their way out of the zombie-infested hospital, Dakota escapes to her car, but in struggling to open its door with her numbed hands, accidentally breaks her left wrist. She eventually manages to drive away. Meanwhile, Block becomes infected along with others, while Cherry and El Wray take refuge at the Bone Shack.
 Texas Ranger. a missing Mexican border, before being stopped by a large mob of zombies. Muldoons men arrive, and kill the zombies before arresting the group. They learn from Abby that the soldiers are stealing Abbys supply of the gas because they are infected with it and the only treatment is by constant inhalation of the gas, which delays mutation. They also learn that a small percentage of population is immune to the gas, suggesting a possible treatment, which is why Muldoon quarantined the survivors.

As Cherry and Dakota are taken away by two soldiers (Quentin Tarantino credited as "Rapist #1" and Greg Kelly credited as "Rapist #2"), the others defeat the security guards. J.T. sustains a gunshot wound in the process, and the group searches for Muldoon. Discovered by El Wray and Abby, Muldoon explains that he killed Osama bin Laden before he and his men were infected with DC2 and were ordered to protect the area. El Wray offers a respectful recognition of Muldoons military service before he and Abby shoot the mutating Muldoon.

Meanwhile, Cherry is forced to dance by Rapist #1 while being held at gunpoint. Cherry attacks by breaking her wooden leg across the rapists face and stabbing him in his eye with the stump. Dakota, after realizing she has regained feeling in her hands, quick-draws her syringe launcher and stuns Rapist #2. El Wray and Abby arrive to rescue Cherry and Dakota, and El Wray replaces Cherrys broken wooden leg with a modified M4 Carbine with a M203 grenade launcher attachment. She promptly kills Rapist #1 and several zombies with it.

J.T., wounded and lying beside his dying brother, stays behind to detonate explosives to eliminate the zombies still in the complex while the others flee. The survivors make plans to escape by stealing helicopters but must fight past the remaining zombies. Abby dies (and hope for a cure with him) when a ballistic projectile blows his head up. An infected Block then arrives and is killed by Earl, shortly before the survivors use the blade tips of their transport helicopter to kill all the remaining zombies. However, while saving Cherry from a zombie, El Wray is fatally wounded. In the epilogue, Cherry (now sporting a minigun prosthetic leg) leads the group and many more survivors to the Caribbean beach at Tulum, Mexico, where they start a peaceful new society during a world-wide zombie outbreak. In the final moments of the film, it is revealed that Cherry Darling has given birth to El Wrays daughter (alluded to earlier during El Wrays final scene when he puts his hand on her stomach and restates his motto "I never miss").

In a post-credits scene, Dakotas son Tony is sitting on the beach at the survivors "base" playing with his turtle, scorpion, and tarantula.

==Cast==
 
* Rose McGowan as Cherry Darling Freddy Rodriguez as El Wray
* Josh Brolin as Dr. William Block
* Marley Shelton as Dr. Dakota Block
* Jeff Fahey as J.T. Hague
* Michael Biehn as Sheriff Hague
* Bruce Willis as Lt. Muldoon
* Rebel Rodriguez as Tony Block
* Naveen Andrews as Abby
* Julio Oscar Mechoso as Romey Stacy Ferguson as Tammy Visan
* Nicky Katt as Joe
* Hung Nguyen as Dr. Crane
* Tom Savini as Deputy Tolo Carlos Gallardo as Deputy Carlos
* Skip Reissig as Skip
* Electra and Elise Avellan as the Crazy Babysitter Twins
* Quentin Tarantino as Rapist #1
* Greg Kelly as Rapist #2 Earl McGraw
* Jerili Romero as Ramona McGraw
* Felix Sabates as Dr. Felix
 

==History and development==
Robert Rodriguez first came up with the idea for Planet Terror during the production of The Faculty.  "I remember telling Elijah Wood and Josh Hartnett, all these young actors, that zombie movies were dead and hadnt been around in a while, but that I thought they were going to come back in a big way because they’d been gone for so long," recalled Rodriguez, "I said, Weve got to be there first. I had   I’d started writing. It was about 30 pages, and I said to them, There are characters for all of you to play. We got all excited about it, and then I didnt know where to go with it. The introduction was about as far as Id gotten, and then I got onto other movies. Sure enough, the zombie   invasion happened and they all came back again, and I was like, Ah, I knew that I shouldve made my zombie film." The story was reapproached when the idea for Grindhouse was developed by Rodriguez and Quentin Tarantino.   

  Mexican exploitation DEA have a really tough job that they dont want to get their own agents killed on, theyll hire an agent from Mexico to come do the job for $25,000. I thought, Thats Machete. He would come and do a really dangerous job for a lot of money to him but for everyone else over here its peanuts. But I never got around to making it."    It was later announced that the trailer would be made as a feature film Machete (film)|Machete.   As for the reference to "Project Terror," Rodriguez paid homage to the late night horror show "Project Terror" which aired in Rodriguezs hometown of San Antonio, Texas on KENS-TV during the 1970s and early 1980s.

==Production==

===Directing===
According to actress Marley Shelton, "(Rodriguez and Tarantino) really co-directed, at least Planet Terror. Quentin was on set a lot. He had notes and adjustments to our performances and he changed lines every once in a while. Of course, he always deferred to Robert on Planet Terror and vice versa for Death Proof. So its really both of their brainchild."      Tarantino has stated, "I cant imagine doing Grindhouse with any other director in the way me and Robert did it because I just had complete faith and trust in him. So much so that we didnt actually see each others movie completed until three weeks before the film opened. It was as if we worked in little vacuums and cut our movies down, and then put them together and watched it all play, and then made a couple of little changes after that, and pretty much that was it."    Rodriguez acted as cinematographer on Planet Terror, as he had done on some of his earlier films.   

===Casting=== Sin City.  Bruce Willis had appeared in Sin City.  Tom Savini had previously acted in From Dusk Till Dawn, Michael Parks reprises the role of Earl McGraw, a role the actor first portrayed in From Dusk Till Dawn, and Quentin Tarantino himself appears in a small role, as he also does in Death Proof.

===Special effects===
The film uses various unconventional techniques to make Planet Terror appear more like the films that were shown in grindhouse theaters in the 1970s. Throughout the feature and the Machete trailer, the film is made to look damaged; five of the six 25,000 frame reels were edited with real film damage, plug-ins, and stock footage.   

Planet Terror makes heavy use of digital effects throughout the film, mostly for Cherrys fake leg. During post-production, the effects teams digitally removed McGowans right leg from the shots and replaced it with computer-generated props &mdash; first a table leg and then an assault rifle. During shooting for these scenes, McGowan wore a special cast which restricted her leg movement to give her the correct motion. 

===Editing===
During pre-production, Tarantino and Rodriguez came up with the idea of inserting a "missing reel" into the film. "(Quentin) was about to show an Italian crime movie with Oliver Reed," Rodriguez recalls, "and he was saying, Oh, its got a missing reel in it. But its really interesting because after the missing reel, you dont know if he slept with a girl or he didnt because she says he did and he says that he didnt. It leaves you guessing, and the movie still works with 20 minutes gone out of it. I thought, Oh, my God, thats what we’ve got to do. Weve got to have a missing reel! Im going to use it in a way where it actually says missing reel for 10 seconds, and then when we come back, youre arriving in the third act.   The late second acts in movies are usually the most predictable and the most boring, thats where the good guy really turns out to be the bad guy, and the bad guy is really good, and the couple becomes friends. Suddenly, though, in the third act, all bets are off and its a whole new story anyway." 

===Music=== Nouvelle Vague soundtrack album Death Proof.

==Theatrical release==
 
Planet Terror was released in the United States and Canada alongside Death Proof as part of a double feature under the title Grindhouse. Both films were released separately in extended versions internationally, approximately two months apart.  The Dutch poster artwork for Planet Terror claimed that the film would feature "coming attractions" from Quentin Tarantino.  In the United Kingdom, Planet Terror was released in cinemas on November 9, 2007.  In reaction to the possibility of a split in a foreign release, Tarantino stated, "Especially if they were dealing with non-English language countries, they dont really have this tradition ... not only do they not really know what a grind house is, they dont even have the double feature tradition. So you are kind of trying to teach us something else."   

==Alternative versions==
With the exception of Grindhouse and Single Theatrical versions of the movie, Rodriguez shot an alternative version where Tony Block did not accidentally shoot himself and survives throughout the film. The official theatrical version features a snippet of Tony on the beach after the end credits and snippets of scenes from this version appears on Rodriguezs 10 Minute Film School feature on Planet Terror DVD. Rodriguez mentioned that this version is especially made for his son Rebel, and has shown Rebel the film with the happy ending rather than the version where he is dead. He also mentioned that Tonys death makes his "horror film... more horrifying in his way".

==Home release==
Planet Terror was released on DVD on October 16, 2007 in a two-disc special edition featuring the extended version of the film presented in a "flat" 1.78:1 screen ratio (the theatrical version in Grindhouse was matted to 2.35:1), audio commentary with Rodriguez, an audience reaction track, several behind the scenes featurettes about casting and special effects, and a "10 Minute Film School" segment,   in which Rodriguez confirmed that a box set of the two films would be available soon, and that his 10 Minute Cooking School on Texas BBQ would appear on it. 

The film was released on Blu-ray Disc|Blu-ray on December 16, 2008. This version ports over the features from the DVD special edition, and also includes a "scratch-free" version of the movie, which doesnt feature the aforementioned intentional "damaged" look to the print. However, all American home video releases of the film are the extended version only and do not include the theatrical cut.

In mid-February 2009, Germany also released a steel box collectors edition for Planet Terror which comes with the famous BBQ sauce recipe and 2 scratch-and-sniff discs of the film which smell like the BBQ sauce. The pack also contains a limited edition Planet Terror blood pack.

The Grindhouse double feature was released on Blu-ray Disc in October 2010.

==References==
 

==External links==
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 