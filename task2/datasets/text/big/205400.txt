Jubilee (1978 film)
 
{{Infobox film
| name           = Jubilee
| image          = Jubilee (1977 film) poster.jpg
| image_size     =
| caption        =
| director       = Derek Jarman
| producer       = Howard Malin James Whaley
| writer         = Derek Jarman Christopher Hobbs
| narrator       = Jordan Nell Campbell Linda Spurrier Toyah Willcox Adam Ant Chelsea Suzi Wayne County Toyah Willcox Adam Ant Ludwig Minkus
| cinematography = Peter Middleton
| editing        = Nick Barnard Tom Priestley
| distributor    =
| released       = February 1978 (UK) September 1979 (USA)
| runtime        = 103 min.
| country        = United Kingdom
| language       = English
| budget         = £50,000 Alexander Walker, National Heroes: British Cinema in the Seventies and Eighties, Harrap, 1985 p 235  or £200,000 
| preceded_by    =
| followed_by    =
}}
Jubilee is a 1978 cult film directed by Derek Jarman. It stars Jenny Runacre, Ian Charleson, and a host of punk rockers, including Adam Ant and Toyah Willcox|Toyah. The title refers to the Silver Jubilee of Elizabeth II in 1977.

==Plot== John Dee Ariel (a Britain of Queen Elizabeth Amyl Nitrite (Jordan (Pamela Rooke)|Jordan), Bod (Runacre in a dual role), Chaos (Hermine Demoriane), Crabs (Nell Campbell), and Mad (Toyah Willcox). 
 Wayne County. cameo appearances by The Slits and Siouxsie and the Banshees. The film was scored by Brian Eno.  The uncredited piece of music used in the Jordans Dance scene was written by Ludwig Minkus in 1884 for Act I in the revived ballet Giselle. 

Beginning with a vignette of Elizabeth I and John Dee summoning The Tempests Ariel (The Tempest)|Ariel, the action moves to an anarchic 1978, where law and order has broken down and punk gangs roam the streets, committing acts of murder and larceny. In one squat, Amyl Nitrate is instructing a group of young women about appropriate female behaviour, which valourises the violent criminal activity of Myra Hindley, before she reminisces about her time as a ballet dancer and introduces the audience to Mad, Crabs, Chaos, Sphinx and Angel (two incestuous bisexual brothers) and Bod, a virgin anarchist. Bod has just strangled and killed Elizabeth II and appropriated her crown. From there, the group move on to a café, where Mad attacks a waitress, and Bod contacts impresario Borgia Ginz. However, she is surprised to find Amyl performing a pastiche of Rule Britannia. Sphinx and Angel establish a relationship with Viv, a young former artist, who they take to meet an ex-soldier. Borgia Ginz is branching out into property management and has purchased "abandoned properties like St Pauls Cathedral and Buckingham Palace, which are transformed into musical venues. Meanwhile, Mad, Bod and Crabs asphyxiate one of Crabs one night stands, and a fight breaks out at a disco session in a St. Pauls Crypt. Violent police activity causes the death of Sphinx and Angel and two revenge attacks on police officers by Bod and Mad. Finally, Borgia Ginz takes the three women off to Dorset and signs a recording contract with them.  Interspersed with these displays of contemporary anarchic violence, Dee, Ariel and Elizabeth try to interpret the signs of anarchic modernity around them, before they undertake a pastoral and nostalgic return to the sixteenth century at the films end.

==Cast==
*Jenny Runacre – Queen Elizabeth I / Bod
*Nell Campbell – Crabs (as Little Nell)
*Toyah Willcox – Mad Jordan – Amyl Nitrite
*Hermine Demoriane – Chaos
*Ian Charleson – Angel
*Karl Johnson – Sphinx
*Linda Spurrier - Viv
*Jack Birkett – Borgia Ginz (as Orlando)
*Jayne County – Lounge Lizard (as Wayne County)
*Richard OBrien – John Dee
*Adam Ant – Kid
*Helen Wellington-Lloyd - Lady in waiting
*Claire Davenport – First Customs Lady
*Barney James – Policeman
*Lindsay Kemp – Cabaret performer
*Gene October – Happy Days
*Siouxsie Sioux – Herself
*Steven Severin – Himself

==Influences== punk aesthetic London Blitz.

==Reaction==
The film had many critics in British punk circles. Fashion designer Vivienne Westwood manufactured a T-shirt on which was printed an "open letter" to Jarman denouncing the film and his misrepresentations of punk.  Jarman described the project as "a film about punk" during pre-production, but later explained that it had a much broader thematic scope.  The film is now considered a cult classic, and was released by the Criterion Collection.

==References==
 

==External links==
*  
*  
*Julian Upton:   Bright Lights Film Journal, Issue 30, October 2000
*  
*  

 

 
 
 
 
 
 
 