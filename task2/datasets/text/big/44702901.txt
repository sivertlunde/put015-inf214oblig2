The Supernatural Events on Campus
{{Infobox film
| name     = The Supernatural Events on Campus
| image    = 
| caption  =  traditional = 校花詭異事件
|                          simplified = 校花诡异事件
|                              pinyin =Xiàohuā Guǐyì Shìjiàn }}
| director = Guan Er
| producer = Liu Hong Chen Qing Liao Libin
| writer   = Liang Xiaoxiao Zhu Bo
| starring = Zhao Yihuan Wang Yi Li Manyi Zhai Wenbin Kong Qianqian
| music    = Yang Bing Qu Peng
| cinematography = Li Hongjian Zhang Wenchao
| editing  = Hao Zhengda
| studio   = Beijing Zexi Niandai Film Company
| distributor = Huaxia Film Distribution Co.,LTD Beijing Zexi Niandai Film Company
| released =  
| runtime = 102 minutes
| country = China
| language = Mandarin
| budget = 
| gross = 
}} thriller horror film directed by Guan Er and written by Liang Xiaoxiao and Zhu Bo, and stars Zhao Yihuan, Wang Yi, Li Manyi, Zhai Wenbin, and Kong Qianqian.  It based on the novel of the same name by Lan Ze. The film was released in China on 12 July 2013. 

==Cast==
* Zhao Yihuan as Su Su, the most beautiful school beauty of Huaxi College. 

* Wang Yi as Lin Feng, Su Sus classmate and first lover.

* Li Manyi as Sister An.

* Zhai Wenbin as Yang Dong.

* Kong Qianqian as Lu Miao, one of the school beauty of Huaxi College.

* Li Sa as the boss/doctor.

* Chen Meihang as Li Shujia.

* Zheng Huixin as Tuantuan.

* Fu Yuhan as Haohao.

* Yang Li as Tuantuans mother.

* Bai Lihui as Tuantuans father.

* Liao Libin as Su Sus father.

* Wang Ruli as Su Sus mother.

* Mo Xier as the broadcaster.

==Music==
* Zhao Yihuan - "The Past That Cant Go Back"

==Accolades==
{| class="wikitable"
|-  style="background:#b0c4de; text-align:center;"
! Award
! Subject
! Nominee
! Result
|- 3rd Beijing International Film Festival Best New Film
|The Supernatural Events on Campus
|  
|}

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 

 
 