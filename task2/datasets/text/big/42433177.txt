The Truth About Beauty
 
 
{{Infobox film
| name           = The Truth About Beauty
| image          = The Truth About Beauty poster.jpg
| alt            = 
| caption        = 
| director       = Lam Oi-wah
| producer       = Peter Chan
| writer         = Lam Oi-wah   Chan Ga-yee
| starring       = Bai Baihe   Ronald Cheng   Zhang Yao   Guo Jingfei
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 84 mins
| country        = China   Hong Kong
| language       = Mandarin
| budget         = 
| gross          = US$13,380,000
}}
 romantic comedy film directed by Lam Oi-wah and starring Bai Baihe, Ronald Cheng, Zhang Yao and Guo Jingfei. 

==Cast==
* Bai Baihe
* Ronald Cheng
* Zhang Yao
* Guo Jingfei

==Reception==
The film has earned  US$13,380,000 in China. 

On Film Business Asia, Derek Elley gave the film a grade of 7 out of 10, calling it a "scabrous satire on success and cosmetic surgery". 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 

 