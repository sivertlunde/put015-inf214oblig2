Bharathi (film)
{{Infobox Film
| name           = Bharathi
| image          = Bharathi Poster.jpg
| image_size     = 
| caption        = Poster
| director       = Gnana Rajasekaran
| producer       = Media Dreams P limited. 
| writer         = Gnana Rajasekaran
| based on       = Subramanya Bharathi 
| narrator       =  Devayani Nizhalgal Ravi ramesh Kumar Pushpaak
| music          = Ilayaraja
| cinematography = Thangar Bachan
| editing        = B. Lenin V. T. Vijayan
| studio         = Media Dreams Pvt Ltd
| distributor    = Media Dreams Pvt Ltd
| released       =  
| runtime        = 149 minutes
| country        = India
| language       = Tamil
| budget         =  1.30 crores 
}}
 National Film 2000 Indian Devayani and Nizhalgal Ravi.    It is a biographical film based on the life of Mahakavi Subramaniya Bharathiyaar.

The film was directed by Gnana Rajasekaran    and won the National Film Award for Best Feature Film in Tamil for the year 2000.

==Plot==
It is a biographical film based on the life of Mahakavi Subramaniya Bharathiyaar.

==Cast==
{| class="wikitable"
|-
! Actor !! Role
|-
| Sayaji Shinde || Subramania Bharathi
|-
| Devayani || Chellamal
|-
| Pushpaak Ramesh || childhood Subramania Bharathi
|-
| Ramesh Kumar || Subramania Bharathi friend
|-
| Nizhalgal Ravi || S. P. Y. Surendranath Arya
|-
| T. P. Gajendran || Kuvalai
|}

==Production==
The female lead role was initially offered to actress Suvalakshmi, whose refusal prompted the team to sign on Devayani (actress)|Devayani. 

==Soundtrack==		
{{Infobox album  	
| Name        = 	Bharathi
| Type        = 	soundtrack
| Artist      = 	Ilayaraja
| Cover       = 	
| Caption     = 	
| Released    = 	 
| Recorded    = 	
| Genre       = 	
| Length      = 	 Tamil
| Label       = 	Sa Re Ga Ma
| Producer    = 	
| Reviews     = 	
| Compiler    = 	
| Misc        = 	
}}	

{{tracklist	
| collapsed       = 	
| headline        = 	
| extra_column    = 	Singers
| total_length    = 	
	
| all_writing     = 	
| all_lyrics      = 	
| all_music       = 	
	
| writing_credits = 	
| lyrics_credits  = 	
| music_credits   = 	
	
| title1          = 	Agini Kunjondru
| note1           = 	
| writer1         = 	
| lyrics1         = 	
| music1          = 	
| extra1          = 	K. J. Yesudas
| length1         = 	
	
| title2          = 	Baratha Samuthayam
| note2           = 	
| writer2         = 	
| lyrics2         = 	
| music2          = 	
| extra2          = 	K. J. Yesudas
| length2         = 	
	
| title3          = 	Ethilum Ingu
| note3           = 	
| writer3         = 	
| lyrics3         = 	
| music3          = 	
| extra3          = 	Madhu Balakrishnan
| length3         = 	
	
| title4          = 	French Music
| note4           = 	
| writer4         = 	
| lyrics4         = 	
| music4          = 	
| extra4          = 	Ilaiyaraaja
| length4         = 	
	
| title5          = 	Keladaa Manida
| note5           = 	
| writer5         = 	
| lyrics5         = 	
| music5          = 	
| extra5          = 	Rajkumar Bharathi
| length5         = 	
	
| title6          = 	Mayil Pola Ponnu onnu
| note6           = 	
| writer6         = 	
| lyrics6         = 	
| music6          = 	
| extra6          = 	Bhavatharini
| length6         = 	
	
| title7          = 	Nallathor Veenai
| note7           = 	
| writer7         = 	
| lyrics7         = 	
| music7          = 	 Mano
| length7         = 	
	
| title8          = 	Ninnaichcharan Adainthen
| note8           = 	
| writer8         = 	
| lyrics8         = 	
| music8          = 	
| extra8          = 	Bombay Jayashree
| length8         = 	
	
| title9          = 	Ninnaichcharan
| note9           = 	
| writer9         = 	
| lyrics9         = 	
| music9          = 	
| extra9          = 	Ilaiyaraaja
| length9         = 	
	
| title10          = 	Nirpathuve Nadapathuve
| note10           = 	
| writer10         = 	
| lyrics10         = 	
| music10          = 	
| extra10          = 	Harish Raghavendra
| length10         = 	
	
| title11          = 	Vante Matharam
| note11           = 	
| writer11         = 	
| lyrics11         = 	
| music11          = 	
| extra11          = 	Madhu Balakrishnan
| length11         = 	
	
}}

==Reception==
Malathi Rangarajan of The Hindu said, "THE MAJESTIC GAIT, the intimidating, piercing eyes that sparkle with a mix of eccentricity, anger, defiance and passion - Shayaji Shinde is indeed a remarkable choice for the role of Bharati." and "It is another fantastic break for Devyani as Chellamma, the wife of Subramania Bharati. Her soft, vulnerable docility and her helpless effete submission to her husbands impractical way of life have been beautifully portrayed."  Rediff said, "Sayaji Shinde as Bharati is simply splendid. His is an impressive performance, with nary an inkling of the trepidation that he is enacting Bharatis role -- he, a Maharashtrian and Tamil, an alien tongue."    Thiraipadam.com said, "This is a good biography, but in the end, it seems like its more a collection of stories than a continuous film. Which is unfortunate because the film had great potential to be absolutely stunningly excellent." 

==See also==
* Kamaraj (film)
* Periyar (film)

==References==		

 

==External links==
* 
		
 

 
 
 
 
 
 