The Three Musketeers (1953 film)
{{Infobox film
| name           = The Three Musketeers
| image          = Three Musketeers 1953.jpg
| caption        = 
| director       = André Hunebelle
| producer       = Paul Cadéac
| writer         = Michel Audiard
| based on       =  | }}
| starring       = Georges Marchal Bourvil Jean Martinelli  Danielle Godet
| music          =  Jean Marion   Costantino Ferri
| cinematography = Marcel Grignon Henri Thibault
| editing        = Jean Feyte
| distributor    = Pathé Titanus
| released       =      
| runtime        = 120 minutes
| country        = France  Italy
| awards         =  French
| budget         = 
}} French The novel of the same name. This adaptation is one of five films director André Hunebelle and screen writer Michel Audiard achieved together. {{Cite web|url= http://www.michelaudiard.com/biographie/biographiehunebelle.htm
|title= Leur collaboration ne sarrêtera pas là, puisquils tourneront cinq films ensemble entre 1947 et 1956|accessdate=2011-11-25}}  Georges Marchal portrayed dArtagnan. In real life he was clearly older (born in 1920) than this character, but Marchal happened to be one of the most famous actors of European cinema in these days. With hindsight this adaptation might be regarded as one of his lesser important appearances  because later on he even starred for Luis Buñuel.

==Plot== fencer and henchmen take care of dArtagnan and steal from him. The enraged dArtagnan is determined to take revenge and will eventually have the chance to do so, for the Queen has given a present to her secret admirer the Duke of Buckingham, and dArtagnan must retrieve it from him, although he is now already back in England.  If he fails her, Cardinal Richelieu is going to disclose Queen Annes infidelity to King Louis XIII, in order to force a war against England upon him. The Cardinal and Count de Rochefort will do everything in their power if only they can put paid to dArtagnans mission. But with help from his three new friends dArtagnan prevails.

==Cast==
{|class="wikitable"
|-
!Actor!!Character
|- Georges Marchal|| dArtagnan
|-
|Bourvil|| Planchet
|- Jean Martinelli Athos (fictional Athos
|- Gino Cervi|| Porthos
|- Jacques François Aramis
|- Danielle Godet || Constance Bonacieux
|- Marie Sabouret Queen Anne
|- Louis Arbessier King Louis XIII
|- Renaud Mary || Cardinal Richelieu
|- Yvonne Sanson  || Milady de Winter
|- Count De Rochefort
|- Steve Barclay Duke of Buckingham
|-
|Françoise Prévost (actress)|Françoise Prévost || Ketty
|-
|Félix Oudart || Comte de Troisville|M. de Tréville	
|- Claude Dauphin Claude Dauphin || the narrator
|}

==Production==
The film was shot in the Studios de Saint-Maurice, on the premises of castle Fontainebleau and in the Forest of Fontainebleau. In 1966 André Hunebelle returned to Fontainebleau for his film Fantômas contre Scotland Yard.

==Reception== Le Bossu, Captain Blood Le Miracle des loups) and hereby established Jean Marais as a fixture for this genre.

==References==
 

This article incorporates information from the French Wikipedia.

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 