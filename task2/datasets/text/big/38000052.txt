9 Full Moons
{{Infobox film
| name           = 9 Full Moons
| image          = 9FM_Poster.jpg
| alt            =
| caption        = Official release poster for 9 Full Moons
| director       = Tomer Almagor
| producer       = Gabrielle Almagor Tomer Almagor Bret Roberts Joy Saez
| writer         = Tomer Almagor
| starring       = Amy Seimetz Bret Roberts Donal Logue
| music          = Ted Speaker
| cinematography = Robert Murphy
| editing        = Tomer Almagor
| studio         = 
| distributor    = Indie Rights
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
9 Full Moons is a 2013 American film starring Amy Seimetz, Bret Roberts and Donal Logue. It was directed by Tomer Almagor and released in theaters in 2014.   

==Plot== East Los Angeles looking for castoff furniture and clothing, which she sometimes sells for spending money. Otherwise, she hangs out with local musicians as an opportunity to meet people.

Lev is a limousine driver who dreams of making it in the music business. He meets Frankie in a dive bar and they hook up, quickly escalating to the point where she moves in with him. Lev also makes a musical connection with Charlie King Nash, a well-known roots-rocker who has hit a creative wall and welcomes the chance to make a new start.

Meantime, Lev and Frankie try to work through the ups and downs of a serious relationship and decide whether each is ready for it.    

==Cast==
*Amy Seimetz as Frankie
*Bret Roberts as Lev
*Donal Logue as Charlie King Nash
*Foster Timms as Spencer Walsh Brian McGuire as Ronnie
*Dale Dickey as Billie
*Harry Dean Stanton as Dimitri
*James Duval as Terry
*Joey Capone as Sam

==Critical reception==
9 Full Moons was originally released June 6, 2013, at the Seattle International Film Festival    and made the festival circuit in Los Angeles, Mexico and The Bahamas.    It was nominated for Best International Feature at the Raindance Film Festival    while Seimetz won the Agave Award for Outstanding Achievement in Acting at the Oaxaca FilmFest.    Film Threat called Moons "an uncommon love story ... driven by complex characters" and said Seimetz was perfectly cast.   

The film premiered in theaters on November 7, 2014. Its accompanying review in the Los Angeles Times called Moons "a gentle millennial spin on Barfly (film)|Barfly" that rings true, even though a back-story "proves a waste of Harry Dean Stanton". 

== References ==
 

==External links==
*  
*  

 
 
 
 