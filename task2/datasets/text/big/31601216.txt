Le Masque de la Méduse
{{infobox film
| name           = Le masque de la Méduse
| image          = Le_masque_de_la_meduse.jpg
| imagesize      = 300px
| caption        = Original poster
| director       = Jean Rollin
| producer       =
| writer         = Jean Rollin
| starring       = Simone Rollin Sabine Lenoël Marlène Delcambre Juliette Moreau Delphine Montoban 
| music          = Philippe DAram
| cinematography = Benoit Torti
| editing        = Janette Kronegger
| distributor    = Insolence Productions Les Films ABC
| released       =  
| runtime        = 75 minutes
| country        = France
| language       = French
| budget         = €150,000  
}} Greek mythological same name.  It is Jean Rollins final film, as the director died in late 2010.

==Cast==
*Simone Rollin as la Méduse
*Sabine Lenoël as Euryale
*Marlène Delcambre as Sthéno
*Juliette Moreau as Juliette
*Delphine Montoban as Cornelius
*Jean-Pierre Bouyxou as le gardien
*Bernard Charnacé as le collectionneur
*Agnès Pierron as la colleuse daffiche au Grand-Guignol
*Gabrielle Rollin as la petite contrebassiste
*Jean Rollin as lhomme qui enterre la tête
*Thomas Smith as Thomas

==Production== Clash of HD Video video on a low-budget of €150,000, before the release it was inflated (blow-up) to 35mm film.

==Release==
The film was not released theatrically, it was, however premiered at the eleventh edition of the Extreme Cinema Film Festival at the Cinémathèque de Toulouse, in which it hosted "An Evening with Jean Rollin" on 19 November 2009.  It was shown as a double feature, with Rollins 2007 film La nuit des horloges showing at 20.30 and the main feature Le masque de la Méduse showing at 22.30. 

==Availability==
The DVD has no official release at present, although, it is currently only available for a limited time with the first 150 copies of Rollins book Jean Rollin: Écrits complets Volume 1.  ISBN 978-2-84608-277-8

==References==
 

==External links==
*  
* The making of  

 

 
 
 