Dil Pardesi Ho Gayaa
{{Infobox film
| name = Dil Pardesi Ho Gayaa
| director = Saawan Kumar Tak
| producer = Saawan Kumar Tak
| Story = Saawan Kumar Tak
| Dialogue = Shyam Gupta
| Screenplay = Shyam Gupta
| starring = Kapil Jhaveri Saloni Aswani Amrish Puri Ashutosh Rana Mukesh Rishi Prem Chopra
| released =  
| country = India
| language = Hindi
}}
Dil Pardesi Ho Gayaa (My Heart Became a Stranger) is a 2003 romantic drama Bollywood movie directed by Saawan Kumar Tak.

==Cast==
* Kapil Jhaveri .... Salman Sunny
* Saloni Aswani .... Ruksar Khan 
* Amrish Puri .... Brig. Sarfaroz Khan   
* Ashutosh Rana .... Major Ram 
* Mukesh Rishi .... Tabrez Baig
* Prem Chopra .... Muslim Priest 
* Raza Murad .... Indian army Bregadier
* Mushtaq Khan .... Khushmisaz
* Yunus Parvez .... Ramzan Khan
* Navni Parihar .... Niharika,ljmllm
* Ghanshyam Rohera .... Waiter (as Ghanshyam)
* Sanjay Sharma .... Capt Nasir khan

== Plot ==
Major Ram is one of the few soldiers from the Indian Army who have been held captive in Pakistan, following the war with India. The Indian army and politicians are unable to make any decision so as not to jeopardize the lives of the captives. Major Rams brother, Arjun, alias Sunny decides to take it upon himself to enter Pakistan and get his brother free. He now calls himself Salman. On the way he meets with beautiful Ruksar and both fall in love with each other. Unfortunately for them, they cannot be married, so they elope, leading to an unrest within their community, and a hunt is on for them. Salman and Ruksar chance upon the prison camp that is housing Ram, and do get him free, only to find themselves trapped by Pakistani soldiers.

== References ==
 

== External links ==
* 

 
 
 
 


 
 