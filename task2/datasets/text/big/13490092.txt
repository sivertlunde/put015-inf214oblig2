Now That I Have You
{{Infobox Film
| name = Now That I Have You
| image = Now_that_i_have_you.jpg
| caption = Now That I Have You Official Poster
| director = Laurenti M. Dyogi
| producer = ABS-CBN Star Cinema Film Productions Inc.
| writer = Jose Javier Reyes
| starring = John Lloyd Cruz   Bea Alonzo
| music = Jesse Lucas
| cinematography = 
| editing = Marya Ignacio
| distributor = Star Cinema
| released =  
| runtime =
| country = Philippines English / Tagalog
| gross    = 84 million
}}

Now That I have You is a Filipino film starring John Lloyd Cruz and Bea Alonzo. The film premiered on August 11, 2004 at SM Megamall, under Star Cinema and directed by Laurenti M. Dyogi. The film got a "B" rating from the Cinema Evaluation Board of the Philippines. Now that I Have You" is also a song.

==Plot==

Betsy (Bea Alonzo) and Michael (John Lloyd Cruz) are only two of the people who ride the Manila Metro Rail Transit System Line 3 everyday. Betsy is a hopeless romantic while Michael is a non-believer when it comes to love and romance.

Betsy is overcome with excitement when her best friend (Nikki Valdez) and her boyfriend set Betsy up on a date with Michael. However, when they finally meet, Michael turns out to be the exact opposite of her ideal man. But Betsy doesnt let herself become disheartened, instead she allows herself to fall in love with the real Michael.

==Cast==
===Main===
* John Lloyd Cruz as Michael Morelos
* Bea Alonzo as Betsy Rallos

===Also starring===
* John Arcilla as Oscar Morelos
* Rio Locsin as Ceres Rallos
* Jean Saburit as Lucille Morelos
* Noel Colet as Pocholo Rallos
* Nikki Valdez as Stefi  
* Kristopher Peralta as Jacob
* Lui Villaruz as Martin
* Jojit Lorenzo as Gabriel
* Hyubs Azarcon as Chito Perez
* Cholo Escaño as Khalil Rallos

===Introducing===
* Roxanne Guinoo as Katherine
* Neri Naig as Joey

==Credits==
*AdProm and Publicity: Roxy A. Liquigan, El Oro (The Team)
*Sound Engineer: Bebet Casas
*Musical Director:Jesse Lucas
*Film Editor:Marya Ignacio
*Production Designer:Elfren Vibar
*Story and Screenplay by:Jose Javier Reyes
*Director of Photography: Regiben O. Romana
*Line Producer: Marizel Samson-Martinez
*Executive Producers: Malou N. Santos, Charo Santos-Concio
*Directed by: Laurenti Dyogi

== External links ==
*  

 
 
 
 
 
 
 