Reflexões de um Liquidificador
{{Infobox film
| name           = Reflexões de um Liquidificador
| image          = Reflexões de um Liquidificador Poster.jpg
| caption        =
| director       = André Klotzel	
| producer       = Rui Pires André Montenegro
| writer         = José Antônio de Souza	 
| starring       = Selton Mello Ana Lúcia Torre
| music          = Mário Manga	
| cinematography = Ulrich Burtin
| editing        = Leticia Giffoni
| studio         = Bras Filmes Aurora Filmes
| distributor    = Bras Filmes 
| released       =  
| runtime        = 80 minutes
| country        = Brazil
| language       = Portuguese
| budget         = 
| gross          = R$ 179,52   
}}
Reflexões de um Liquidificador (Reflections of a Blender ) is a 2010 Brazilian black comedy film directed by André Klotzel.

==Plot==
The film follows the story of Elvira, a housewife with a busy lifestyle. Onofre, her husband, disappeared, and she decides to go to the police to find out about his disappearance. The trajectory of the couple is narrated by Elviras blender, who came to life when, long ago, Onofre switched its propeller by a much larger one. 

==Cast==
*Selton Mello as the blender
*Ana Lúcia Torre as Elvira
*Germano Haiut as Onofre
*Marcos Cesana as postman
*Aramis Trindade

==Production and release== road show.    It was not a success but grossed R$179,521,  and a public of 24,149 people, while other domestic releases do not achieved 100 viewers.  It also guaranteed Klotzel the Best Director Award at the Brazilian Film & Television Festival of Toronto. 

==References==
 

==External links==
*    
*  

 
 
 
 
 
 


 
 