Charles Mingus: Triumph of the Underdog
{{Multiple issues|
 
 
}}

Charles Mingus: Triumph of the Underdog is a 1998 documentary film about the life of jazz musician Charles Mingus.

==Synopsis==
 
Charles Mingus narrates pieces of his own story. Born from a half black/half Swedish father and a half black/half Chinese mother (later having a half black/half Indian step-mother) Mingus tried to be all types of races but found he was a misfit that didnt belong anywhere; so he says hes just a negro. He sums this up stating, "In a society which sends messages that are racists capitalists, and have Judaeo-Christian ethic roots, the transmission of these messages to Black people can lead to socializing of their young with non-complementary values." 

He plays the cello. Although he was a famed jazz musician, he was not famed enough to, make a living.  Gunther Schuller a composer/conductor/historian states that Charles Mingus is high up with American composers. Minguss work is compared by Schuller to Duke Ellington.  Mingus is more known as a bass leader than as a composer. Mingus as a composer, is not highly recognized for how great he was. Mingus states that in relation to race, "one day he will pledge allegiance to see that some day (America) will look to its own promises, to the victims that they call citizens". 

John Handy and Sue Mingus describe Charles as individual, volatile, strong, supremely honest, uncompromising, and having a very dynamic personality that ran like the color spectrum; testing people to see how far he could go. Wynton Marsalis describes Charles as never victimized by a style and that he was always relating his music to something human.

Jerome Richardson says Mingus was aware of what was going in regards to race and never backed down from it. John Handy says he did what most others could not, mainly because he was conscious of the race issue. his music portrayed dramatized events, past and present maybe even futuristic. He also notes that Mingus was writing compositions when everyone else was writing tunes.
Gunther Schuller adds that all of his variety of his personality comes out in Minguss music, and not only that it is the widest range of music composed by one single human being. It covers the entire range of human emotions, conditions. so he reflected who he was through his music. In addition, Schuller states that anything that was music, Mingus absorbed, and he absorbed it fast.

When asked by Chris Albertson how Mingus feels about the term music, or jazz, Mingus replied with its like someone is using a substitute name for music.
Randy Brecker notes that the willingness to expose yourself, is something that Mingus did.

Schuller notes that Mingus studied composers like Stravinski.
He was a bass player that studied avant garde music in the 1930s.
Snooki Young says that there were many clubs on Central avenue back then, and that they would have random jazz sessions. Mingus was just one of the guys.

Britt Woodman, who played trombone with Duke Ellington, recalls that Mingus really liked Duke Ellington emerged in 1920s as one of the greatest composers of all time, perfectly encapsulating jazz musics innovative creativity. {{cite book|last=Earle|first=Jonathan|title=The Routledge Atlas Of African American History|publisher=Routledge|location=Great Britain|year=2000|pages=122|isbn=0-7591-0929-X
}} 

Juan Tizol asked Mingus to play for him. Mingus couldnt play, as he was nervous, and Tizol said that he knew that niggas cant read. This made Mingus really mad.
Charles said that he was going to get Tizol. Mingus pushed Tizol out of the way, and Mingus played with Duke.
Eddie Bert remembers that Mingus said that they carried on like it was a dance routine.
Duke fired Tizol and hired Mingus the same day.


After Mingus discovered Duke Ellington, and classical music, he just needed another step up with the crazy be-bop music.

In May 1953 Mingus worked with Parker.

Mingus was one of the first artists that had an artist-run record company. Minguss wife helped pack records started on a shoe string

Mingus would not play without his drummer Dannie Richmond.  Dannie first drum lesson was from Mingus. when Dannie and Mingus finally talked, Mingus said that playing an instrument is like having a conversation.

Mingus felt he was being cheated with the major record company. So he started his own company called Charles Mingus Enterprises, this was one of his first real successes. They put out four albums.

At the peak of Jazz in the late 50s and early 60s, Mingus performed every night for six months at a time. Then in the late 1960s, the music world realized how much money could be made from rock music. When Mingus realized this, he went crazy and spent two months a psych ward. 
After he got out, he completely dropped out of music. 
He got a gig as a photographer but went crazy. He threw scenes at bars.
When he did the documentary, he was in an anguished time. He was thrown out of his apartment that he just rented. It was a sad, difficult time for Mingus.

A tribute to Duke took him out of it, was a tribute to Duke. they wanted Mingus to attend. George Adams would come to work and the fifth wheel would sometimes be gone.

"Sues Changes" was written for his wife Sue in July 1975 and first recorded in 1974.
"Goodbye Porkpie Hat" was originally recorded in 1959 as a tribute to the recently deceased saxophonist Lester Young. Mingus found himself more in demand.

Things were going wrong with Mingus. He finally went to the doctors and they said he has amiotropic lateral sclerosis (ALS), also called Lou Gehrigs Disease.
The doctors said he had only a little while to live.

Randy and Micheal Brecker made an album called me, myself, and I. Mingus looked it over. He oversaw it in a way that no one could oversee.  

When he was paralyzed, he sang with what was left into a tape recorder.

He went to the White House.
Dorian Mingus is the son of Charles and Celia.
Mingus told his son that he had no color.

Charles Mingus died on January 5, 1979.

== Characters ==
* Charles Mingus: as himself;jazz musician, composer, bandleader, andcivil rights activist. narrates.
* Sue Mingus as herself.
* Gunther Schuller: as himself;composer, conductor, horn player, author, historian, and jazz musician. narrates.
* Duke Ellington as himself.
* Jack Walrath as himself.
* George Adams as himself.
* Craig Handy as himself.
* Wynton Marsalis as himself.
* Randy Brecker as himself.
* Britt Woodman as himself.
* Charlie Parker as himself.
* Dizzy Gillespie as himself.
* Bud Powell as himself.
* Max Roach as himself.
* Jerome Richardson as himself.
* John Handy as himself.
* Eddie Bert as himself.
* Don Butterfield as himself.
* Jimmy Knepper as himself.
* Eric Dolphy as himself.
* Andrew Homzy as himself.

== Background ==
The website Culture vulture states that "Director Don McGlynn, who has directed documentaries about such American musical icons as Art Pepper, the Mills Brothers and Spike Jones, has given us a rich and many layered story with Charles Mingus: Triumph of the Underdog. The documentary is grainy, as was Mingus (1922-1979) himself: a tangled, mercurial and often misunderstood musical genius who is known today primarily as a seminal bass player, but whose compositions are the primary focus of this film.

You wont leave the theater whistling. You dont hum back a Mingus tune. The music is exciting but convoluted, and may be beyond the attention span of many film audiences. But you cannot help reacting sympathetically to Mingus himself, a photogenic, funny, and hard-hitting man who always told it, and played it, as he saw it.

The documentary opens with a close-up of Mingus playing bass as a tuba player arrives late to the gig. The look Mingus gives this man, as he tries to sneak in and get set up in a hurry, is a classic. We know right away that Charles Mingus is not a man to fool around with. In case we werent sure, next we hear Minguss version of Shortenin Bread:

     "Mamas little baby loves shortenin bread.
     Thats just a lie some American white man said."

Mingus was always a misfit. He had the fortune, or misfortune, to be forever between worlds: part black, Swedish, Chinese and German. He grew up in Watts and turned to music as a release. A gifted bassist, he was also an intellectual. In the 1930s he was studying the classics, not only Bach but also the avant-garde Schoenberg, father of the 12-tone scale. Minguss idol was Duke Ellington, and he played for a while in Ellingtons band. We hear the story of how saxophonist Juan Tizol and Mingus got into a fight and Duke was forced to fire Mingus. He then hooked up with saxophonist Eric Dolphy (there is fabulous footage of Dolphy and Mingus playing duets), and we see how he was shattered at Dolphys sudden death (unexplained here) in Germany.

We also experience first-hand one of the eternal themes of art: the unrecognized masterpiece. Minguss jazz symphony, Epitaph, was a complete disaster the one and only time it was ever performed in his lifetime. But after Minguss death, the score to Epitaph was rediscovered, and his longtime associate Gunther Schuller put together an all-star orchestra to play this very demanding piece of music. As trumpeter Wynton Marsalis puts it, "Youll find Epitaph in the Etude Book, under Hard." The concert, at New Yorks Town Hall in 1989, was a triumph, if ten years too late for Charles Mingus to enjoy it.

There are interesting comments and interviews with Minguss two wives, Celia Mingus Zaentz and Sue Mingus (who refer to their late husband only as "Mingus," no nicknames, not even Charlie or Charles), as well as with one of his sons, Dorian Mingus. There is a wealth of wonderful performances of Mingus tunes by jazz luminaries such as Charlie Parker, Clifford Jordan, Gerry Mulligan, Lionel Hampton, Bud Powell and Duke Ellington.

But above all there is the sense of this mans life. McGlynn has not put a gloss on it. We feel all the components of the music, particularly in the wonderful footage of the Epitaph Concert.  Listening to this abstract but spellbinding work we understand better some of what Charles Mingus was trying to say. This segment alone is worth the price of admission to the film.

McGlynns documentary captures beautifully the complex and cryptic nature that at times nurtured and other times overcame Mingus. But the music survives. Thats what the world will remember."

==Song clips==
* "Started Melody" rehearsed by The Mingus Big Band at the Time Cafe (1997); intercut with the Town Hall performance of the same song (October 1962). 
* "Epitaph" (which includes "Started Melody") at the Lincoln Center/Alice Tully Hall (June 1989).
* "This Subdues My Passion" by Boron Mingus and his Octet (May 1946).
* "Caravan" by Duke Ellington and His Orchestra (composer Juan Tizol)
* "Slide Hamp Slide" by  Lionel Hampton and His Orchestra. 
* The Massey Hall Concert with Charlie Parker, Dizzy Gillepie, Bud Powell, and Max Roach in Toronto in (May 1953).
* "Pithecanthropus Erectus" performed October 1970 (January 1956).
* "Better Get it in Your Soul" (May 1959).
* "Weird Nightmare"
* "The Clown" introduced by Duke Ellington at UCB (September 1969). 
* "Sues Changes" (July 1975). 
* "Celia" (August 1957).
* "Goodbye Porkpie Hat" (1959).
* "Something Like a Bird" rehearsal (January 1978).
* "Chair in the Sky" written by Joni Mitchel (1978).
* "Sue Changes" by The Mingus Dynasty Band.
* "The Childrens Hour of the Dream"

== Movie Information ==
Jazz/independent/documentary

Director: Don McGlynn

Producers: Don McGlynn & Sue Mingus

== References ==
 

==External links==
*  
 

 
 