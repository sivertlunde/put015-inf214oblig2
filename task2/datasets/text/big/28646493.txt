Buick Riviera (film)
 
{{Infobox film
| name           = Buick Riviera
| image          = Buick Riviera film.jpg
| director       = Goran Rušinović
| producer       = Boris T. Matić, Mike Downey, Sam Taylor, Kate Barry
| writer         = Goran Rušinović, Miljenko Jergović
| starring       = Slavko Stimac, Leon Lucev, Aimee Klein, Shawn Miramontes
| released       =  
| runtime        = 86 minutes
| country        = Croatia
| language       = English
}}
Buick Riviera is a 2008 drama film by director Goran Rušinović, based on the novel Buick Rivera by Miljenko Jergović.  It was awarded the "Heart of Sarajevo" award as the Best Film at the 2008 Sarajevo Film Festival. 
{{cite web url = http://www.sff.ba/default.aspx?template=\Templates\Article&articleid=15881&lang=en title = Heart of Sarajevo for film "Buick Riviera" date = 23 August 2008
|publisher=sff.ba}} 

==Plot== 1963 Buick Riviera. Surrounding everything that Hasan loves is a prejudiced world afraid of a culture they cannot understand and a religion they cannot comprehend. Hasan is a quiet man who stands distant from his beliefs, however it is these beliefs that inevitably lead to his untimely death.

On the way to pick up his wife from work at the local hospital, he falls asleep at the wheel of his Buick and runs it off the road, getting stuck in the snow. He is discovered by Vuko Salipur, who stops to help him. They both immediately recognise that they are from the same country and both understand the irony of a Bosnian stopping to help another Bosnian in a country full of all different races and colours.

Despite their similar background it soon becomes clear that Muslim Hasan and non-Muslim Vuko are very different. Their continuing interaction results in ever increasing tension between them, ultimately with tragic consequences.

==Accolades==
*Best Screenplay. Pula Film Festival 2008. Golden Arena.
*Heart of Sarajevo. Sarajevo Film Festival 2008.
*Best Actor. Slavko Stimac. Leon Lucev. Sarajevo Film Festival 2008.

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 