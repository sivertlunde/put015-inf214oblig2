Savate (film)
{{Infobox film
| name = Savate
| image = Savate movie cover.jpg
| caption = Savate movie cover
| director = Isaac Florentine
| producer =
| writer = Isaac Florentine Julian Stone
| starring = Olivier Gruner Ian Ziering
| music =
| cinematography =
| editing =
| studio =
| distributor =
| released =  
| runtime = 88 minutes
| country = United States
| language = English
| budget =
| gross =
| followed by =
}} 1995 Western_(genre)#Martial_arts_Western|martial arts western starring Olivier Gruner, promoted as the allegedly true story of the worlds first kickboxer.

==Plot== officer of French Foreign Legion in Mexico. Looking for the murderer, Charlegrand is heading for a martial arts tournament in the USA because the murderer takes his pride in being a skilled fighter.  
On his way from Mexico to Texas some American rogues take him for a Yankee and ambush him. He can fight them off but loses his horse. On foot he runs out of water and eventually breaks down. Two young farmers (Ashley Lawrence and Ian Ziering as Mary and Cain Parker) save his life.   business man who wants their land very badly. The films protagonist returns the farmers favour by applying his savate. Yet it is obvious they need further support.   precision dice. Cain recognises this object, follows the culprit into town and confronts him. After Cain has been shot dead, all farmers are ready to sell out.  
The hero decides he mustnt let that happen, hence he takes the dead farmers place in the tournament and tells the farmers to bet all their money on him. In order to prevent him from being successful, his friends murderer, the German language|German-speaking von Trotta (Marc Singer) is hired.  
But the bad guys leave nothing to chance and also take Mary Parker as hostage. Charlegrand manages to cause enough confusion to disappear between two fights, so that he can free Mary and force Colonel Jones (James Brolin) to spill the beans. The alleged new taxes turn out to be a hoax but the farmers savings are on Charlegrand and so they still need him to win the tournament. 
Therefore his final battle with von Trotta mustnt be postponed, even though Charlegrand has been shot in the course of action.

==Background==
The French Foreign Legion had been founded in 1831, one year after the Garde Écossaise had been officially dissolved. Right from the beginning many German-speaking men joined the forces, often hiding behind false names. In 1861 Napoleon III used the Legion for the French intervention in Mexico. It lasted until 1867. At that time Charles Lecour had already created and established French boxing as a blend of savate and English boxing. Hitherto savateurs had used their hands mainly to block kicks or to fence with sticks (canne de combat) at the same time. One of Lecours students was former army instructor Joseph Charlemont. Against this background it is imaginable a savateur like Joseph Charlegrand could have served in Mexico and that he might have met a German-speaking legionnaire during this time. Yet in spite of the claim the story was true, it seems to be mere (even though entertaining) fiction, since with the slow communications and travel of 1865, and the lack of cross-over in combat sports of the time, the idea of an international martial arts contest in the wilds of the American West is highly anachronistic.

==Cast==
*Michael Palance as Mitchum
*Olivier Gruner as Joseph Charlegrand
*Ian Ziering as Cain Parker
*Ashley Laurence as Mary Parker
*Marc Singer as Ziegfield Von Trotta
*James Brolin as Col. Jones
*Rance Howard as Farmer

==External links==
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 