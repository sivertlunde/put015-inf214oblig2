Puthiya Velicham
{{Infobox film
| name = Puthiya Velicham
| image =
| image_size =
| caption =
| director = Sreekumaran Thampi S Kumar
| writer = Sreekumaran Thampi
| screenplay = Sreekumaran Thampi
| starring = Jayan Jayabharathi Srividya Jagathy Sreekumar
| music = Salil Chowdhary
| cinematography = NA Thara
| editing = K Narayanan
| studio = Sastha Productions
| distributor = Sastha Productions
| released =  
| country = India Malayalam
}}
 1979 Cinema Indian Malayalam Malayalam film, S Kumar. The film stars Jayan, Jayabharathi, Srividya and Jagathy Sreekumar in lead roles. The film had musical score by Salil Chowdhary.  
  
 
==Cast==
  
*Jayan 
*Jayabharathi 
*Srividya 
*Jagathy Sreekumar 
*Thikkurissi Sukumaran Nair  Hari 
*Sankaradi 
*Sreelatha Namboothiri  Meena 
*Poojappura Ravi  Master Raghu
 

==Soundtrack==
The music was composed by Salil Chowdhary. 
{| class="wikitable"
|- Lyrics || Length (m:ss)
|- 
| 1 || Aaraattukadavil || Jayachandran || Sreekumaran Thampi ||
|- 
| 2 || Aararo Swapnajaalakam || Ambili || Sreekumaran Thampi ||
|- 
| 3 || Chuvannapattum Thettippoovum || P. Susheela|Susheela, Chorus || Sreekumaran Thampi ||
|- 
| 4 || Jil Jil Jil Chilambanangi || P. Susheela|Susheela, Jayachandran || Sreekumaran Thampi ||
|-  Janaki || Sreekumaran Thampi ||
|- 
| 6 || Poovirinjallo || K. J. Yesudas || Sreekumaran Thampi ||
|}

==References==
 

==External links==
*   
*  

 
 
 


 