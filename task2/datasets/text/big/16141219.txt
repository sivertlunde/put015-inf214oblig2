The Fighting Heart (1925 film)
 
{{Infobox film
| name           = The Fighting Heart
| image	         = The Fighting Heart FilmPoster.jpeg
| caption        = Film poster
| director       = John Ford
| producer       = 
| writer         = Larry Evans Lillie Hayward George OBrien Billie Dove
| cinematography = Joseph H. August
| editing        = 
| distributor    = Fox Film Corporation
| released       =  
| runtime        = 70 minutes 
| country        = United States  Silent English intertitles
| budget         = 
}} 
The Fighting Heart is a 1925 American drama film directed by John Ford. The film is now considered to be a lost film.      

==Cast== George OBrien as Denny Bolton
* Billie Dove as Doris Anderson
* J. Farrell MacDonald as Jerry
* Victor McLaglen as Soapy Williams
* Diana Miller as Helen Van Allen
* Bert Woodruff as Grandfather Francis Ford as Town Fool
* Hazel Howell as Oklahoma Kate
* Edward Peil Sr. as Flash Fogarty (as Edward Piel)
* James A. Marcus as Judge Maynard (as James Marcus)
* Frank Baker Harvey Clark
* Lynn Cowan
* Hank Mann
* Francis Powers

==See also==
*List of lost films

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 


 