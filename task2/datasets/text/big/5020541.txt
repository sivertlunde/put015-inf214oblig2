Death in Gaza
{{Infobox film
| name = Death in Gaza
| image = deathingaza.jpeg
| caption = DVD cover James Miller James Miller Saira Shah
| writer = Saira Shah James Miller Saira Shah
| music = Nick Powell James Miller
| editing = Misha Manson-Smith Channel Four Television Corporation
| released =  

| runtime = 80 minutes
| language = Arabic English Hebrew
| budget =
}}
Death in Gaza is a 2004 documentary film about the Israeli-Palestinian conflict, opening in the West Bank but then moving to Gaza and eventually settling in Rafah where the film spends most of its time. It concentrates on 3 children, Ahmed (age 12), Mohammed (age 12) and Najla (age 16).

==Topics==

===Children===
The film follows the children in different aspects of their lives including life in the vicinity of military forces and games born out of the conflict – such as running towards, throwing rocks and homemade explosives, quwas, at armored vehicles; Study materials in schools which focus on Palestinian perceptions of the conflict, as well as time spent with family and friends, including following one of the children as he plays with and helps militant fighters. The film also makes note of the political use of public mourning for conflict enhancement.

===Martyrdom===
For a short while the film concentrates on martyrdom and the opinions of the people there about dying for Palestine and Islam. It briefly tells the story of a young boy who was shot while attacking Israeli forces much like the main boys Ahmed and Mohammed, as well as numerous other unnamed boys. The film follows the boy from being brought into the medical center and the initial treatment, to his death and public reaction, to the parade and his burial, and celebration at his success in becoming a martyr.

==James Millers death== James Miller was killed by an Israeli soldier.    His death was incorporated as a major part of the film, with an explanation by the narrator, Saira Shah, at the beginning of the film, and the full story and reactions at the end. It is mentioned that Palestinian people made posters declaring that Miller was a martyr against the film crews wishes.   

==See also==
* Gaza Strip (film)

==References==
 

==External links==
*  
*  
*   page from HBO.

 
 
 
 
 