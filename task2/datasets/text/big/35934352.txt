The Girl and Death
{{Infobox film
| title = The Girl and Death
| running time = 124 minutes
| directed = Jos Stelling
| writer = Jos Stelling Bert Rijkelijkhuizen
| producer = Jos Stelling Films Anton Kramer
| music = Bart van de Lisdonk
| released =  
| country = Dutch
| language = Russian German French
}}
The Girl and Death ( ) is a 2012 Dutch film directed by Jos Stelling.  

==Plot==
The Girl and Death is a love story of Nicolai and Elise (a courtesan). Their love is obstructed by materialism, wealth, and death. The film, based on an original script by Jos Stelling and Bert Rijkelijkhuizen, tells the story of the Russian physician Nicolai, who returns to an old hotel, the place where he first met his great love half a century ago and relives his romantic tragedy.  

A large part of the film is set in the late nineteenth century, and the action takes place in an abandoned hotel/sanatorium in   where most of the film was also shot. The hotel still bears the traces of past glory.  Eventually, it becomes clear why Nicolai has really returned.

== Cast ==
* Sylvia Hoeks as Elise
* Leonid Bichevin as Nicolai
* Sergey Makovetsky as Old Nicolai
* Renata Litvinova as Nina
* Dieter Hallervorden as The Count
* Svetlana Svetlichnaya as Old Nina
* Eva-Maria Kurz as The Old Actress
::::: 

==References==
 

== External links ==
*  .

 
 
 