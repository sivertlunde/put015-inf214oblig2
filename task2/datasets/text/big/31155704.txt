Kavalam Chundan (film)
{{Infobox film 
| name           = Kavalam Chundan
| image          =
| caption        =
| director       = J. Sasikumar
| producer       = VPM Manikkam
| writer         = J. Sasikumar Thoppil Bhasi (dialogues)
| screenplay     = Thoppil Bhasi Sathyan Sharada Sharada Adoor Bhasi P. J. Antony
| music          = G. Devarajan
| cinematography = U Rajagopal
| editing        = Vijaya Rangan
| studio         = Bhagavathy Pictures
| distributor    = Bhagavathy Pictures
| released       =  
| country        = India Malayalam
}}
 1967 Cinema Indian Malayalam Malayalam film, directed by J. Sasikumarand produced by VPM Manikkam. The film stars Sathyan (actor)|Sathyan, Sharada (actress)|Sharada, Adoor Bhasi and P. J. Antony in lead roles. The film had musical score by G. Devarajan.   

==Cast==
  Sathyan
*Sharada Sharada
*Adoor Bhasi
*P. J. Antony
*Adoor Bhavani
*Adoor Pankajam
*Aranmula Ponnamma
*Kaduvakulam Antony
*Kottarakkara Sreedharan Nair
*Pankajavalli
*S. P. Pillai
 

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Vayalar Ramavarma. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aambalppoove || K. J. Yesudas, Chorus || Vayalar Ramavarma || 
|-
| 2 || Akalukayo Thammil || K. J. Yesudas || Vayalar Ramavarma || 
|-
| 3 || Cheekiminukkiya || S Janaki || Vayalar Ramavarma || 
|-
| 4 || Kanniyilam Muthalle || P Susheela || Vayalar Ramavarma || 
|-
| 5 || Kuttanaadan Punchayile || K. J. Yesudas, Chorus || Vayalar Ramavarma || 
|}

==References==
 

==External links==
*  

 
 
 

 