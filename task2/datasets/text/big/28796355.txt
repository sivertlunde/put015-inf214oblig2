Gonks Go Beat
{{Multiple issues|
 
 
}}

 
 

{{Infobox film
| name           = Gonks Go Beat
| image          = Gonksgobeat.jpg
| caption        = Region 2 DVD cover
| director       = Robert Hartford-Davis
| producer       = Peter Newbrook
| writer         = Robert Hartford-Davis Peter Newbrook Pamela Brown
| music          = Various
| cinematography =
| editing        = Teddy Darvas
| studio         = Titan Film Productions
| released       = 1965
| runtime        = 90 minutes
| country        = United Kingdom
| language       = English
}}
 Graham Bond Derek Thompson (now known as Charlie Fairhead in the long-running BBC series Casualty (TV series)|Casualty) performing with his twin sister Elaine. The title highlights the fad for gonks in mid 1960s Great Britain.

==Plot==
At a point in the distant future, the inhabitants of Planet Earth have become divided into two factions who despise each other. In Beatland live the hip and trendy people who have long hair, dress in turtlenecks, jeans and sunglasses and listen to cool beat music. Their counterparts on Ballad Isle keep their hair short and tidy, wear button-down shirts and pressed slacks or floral dresses and twinsets, and listen exclusively to crooners. A musical competition is staged annually between the two sides, overseen by the neutral and powerful record company executive Mr. A&R (Thornton). For the rest of the year they regard each other with suspicion and antipathy, although they are not above sneaking into each others territory to steal musical ideas.

Meanwhile, the overlords of a far-flung galaxy have been observing the squabblings and goings-on on Planet Earth with increasing exasperation. Finally, their patience with the earthlings is pushed beyond its limit and they decide to send their bungling representative Wilco Roger (Connor) to sort the situation out and bring about a reconciliation between the parties, with the warning that if he fails hell be exiled to Planet Gonk, a fearsome and dreaded place where spherical furry soft toys shuffle around all day listening to Dixieland jazz.

On arrival, Wilco Roger makes contact with Mr. A&R. Theyre aware of a forbidden romance between a Beatland boy and a Ballad Isle girl, and use a combination of Mr. A&Rs cunning and Wilco Rogers mystical powers to enable the couple to get together without fear and come up with a musical composition which will be acceptable to both sides. The time for the annual competition comes around, and the inhabitants are appalled when the Beatland boy and the Ballad Isle girl take to the stage together. But their song "Takes Two to Make Love" turns out to be the hit of the night, loved by both sides of the musical divide. Mr. A&R declares it the unquestioned winner and orders an end to the silliness as it has now been proved that everyone can live together and learn to appreciate all types of music. 

==Reputation==
Gonks Go Beat was widely ridiculed on its original release as being so ill-advised and botched in execution as to have no appeal to any cinema audience demographic, whether of the younger or older generation. British film historian I.Q. Hunter
included Gonks Go Beat in his list of contenders for ""the worst British film ever made".  But in later years, its acquired a reputation as a cult film, particularly appreciated by connoisseurs of the "Cult film#So-bad-theyre-good cult movies and camp classics|so-bad-its-good" genre. Its attractions include the absurdly silly storyline, cheap, cramped and wobbly cardboard sets, clunkily unfunny comic dialogue, a general air of amateurish ineptitude and the embarrassing spectacle of middle-aged actors such as Connor and Thornton obviously all-at-sea in this would-be "trendy" setting. The film was released on DVD in the UK in 2007 by Optimum Home Entertainment, who tipped the wink to its intended audience by describing it as "the Plan 9 from Outer Space of film musicals", a description originally coined by the UK film critic Mark Kermode.   

==Cast==
 
 
* Kenneth Connor as Wilco Roger
* Frank Thornton as Mr. A&R
* Barbara Brown as Helen
* Iain Gregory as Steve
* Terry Scott as PM
* Reginald Beckwith as Professor
* Jerry Desmonde as Great Galaxian
* Arthur Mullard as Drum Master
* Pamela Donald as Tutor
* Gillian French as Beatland Prime Minister
  Babs Lord as Beat Girl Lulu as Herself
* The Nashville Teens as Themselves
* Ginger Baker as Himself
* Jack Bruce as Himself
* Graham Bond as Himself
* Dick Heckstall-Smith as Himself John McLaughlin as Himself Derek Thompson as Singer
 

==Reception==
* In "The Spinning Image" Graeme Clark wrote, "time has been kinder to its daft novelty than a few of its peers, although that doesnt mean its any good, it has just grown quainter with age."  
* TV Guide gave the film one and a half stars and wrote, "only fans of obscure mod bands will enjoy this teen exploitation item."  
* "Cinedelica" wrote, "a fairly enjoyable, if very dated, slice of period silliness."  

==References==
 

==External links==
*  
*  
*   at BFI Film & TV Database

 
 

 
 
 
 
 
 
 
 
 