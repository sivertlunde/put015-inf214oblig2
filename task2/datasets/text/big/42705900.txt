Raman Ethanai Ramanadi
{{Infobox film
| name           = Raman Ethanai Ramanadi
| image          = Raman Ethanai Ramadi.jpeg
| caption        = 
| director       = P. Madhavan
| producer       = P. Madhavan
| writer         = Bala Murugan
| starring       = Sivaji Ganesan K. R. Vijaya R. Muthuraman
| music          = M. S. Viswanathan
| cinematography = P. N. Sundaram
| editing        = R. Devarajan
| studio         = Arun Prasad Movies
| distributor    = Sivaji Productions
| released       =   
| runtime        = 146 minutes
| country        = India
| language       = Tamil
}}
 1970 Tamil language drama film directed by P. Madhavan. The film features Sivaji Ganesan, K. R. Vijaya and R. Muthuraman in lead roles.
The film, had musical score by M. S. Viswanathan. The film won National Film Award for Best Feature Film in Tamil for the year. P. Madhavan also directed the Hindi remake Ram Tere Kitne Nam starring Sanjeev Kapoor.

==Cast==
* Sivaji Ganesan
* K. R. Vijaya
* R. Muthuraman
* Kathadi Ramamurthy

;Guest Appearance
* M. N. Nambiar Padmini
* Chittor V. Nagaiah

==Production==
Ganesan had earlier enacted the character of Maratha King Sivaji in a play "Chandramohan" by C. N. Annadurai and won the title "Sivaji" from E. V. Ramasamy (Periyar). The portion from the stage play has been used in the film. Producers thanked Annadurais family for permitting them to use the episode in the film. Kannadasan wrote the dialogues for the portion. 

==Soundtrack==
The music composed by M. S. Viswanathan.  G. Dhananjayan wrote "The song Ammadi ponnukku   stands out as the best among the lot in this film and became a cult song over the years". 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Kannadasan || 03:40
|-
| 2 || Ammadi Ponnukku (Sad) || T. M. Soundararajan || 02:49
|-
| 3 || Chera Chola Pandiyar || L. R. Eswari, P. Madhuri || 04:47
|-
| 4 || Chithrai Maadham || P. Susheela || 03:59
|-
| 5 || Nilavu Vanthathu || P. Susheela || 04:22
|-
| 6 || Veera Sivaji || Sivaji Ganesan || 04:32
|-
| 7 || Ammadi Ponnukku (Pathos) || P. Susheela || 02:50
|}

==References==
 

==External links==
* 

==Bibliography==
*  

 
 

 
 
 
 
 


 