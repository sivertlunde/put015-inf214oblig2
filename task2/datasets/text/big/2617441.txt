The Cowboys
 
{{Infobox film
| name           = The Cowboys 
| image          = Cowboys 1972.jpg
| caption        = Film poster
| director       = Mark Rydell
| producer       = Mark Rydell
| screenplay     = {{plainlist|
*Irving Ravetch
*Harriet Frank, Jr.
}}
| story          = William Dale Jennings
| starring       = {{plainlist|
*John Wayne
*Roscoe Lee Browne
*Bruce Dern
*Colleen Dewhurst
*Slim Pickens
}}
| music          = John Williams
| cinematography = Robert L. Surtees
| editing        = {{plainlist|
*Robert Swink
*Neil Travis
}}
| distributor    = Warner Bros.
| released       =  
| runtime        = 131 minutes
| country        = United States
| language       = English
| budget         = $6 million
| gross          = $7,500,000 
}} Western film|motion Stephen Hudis, as cowboys. It was filmed at various locations in New Mexico, Colorado and at Warner Brothers Studio in Burbank, California. Based on the novel by William Dale Jennings, the screenplay was written by Irving Ravetch, Harriet Frank, Jr., and Jennings, and directed by Mark Rydell.

==Plot==
 
When his ranch hands abandon him to join a gold rush, rancher Wil Andersen (John Wayne) is forced to find replacement drovers for his upcoming   long cattle drive.  He rides into deserted Bozeman, Montana.  There,  Anse Peterson (Slim Pickens) suggests using local schoolboys. Andersen visits the school but departs unconvinced.  The next morning, a group of the boys show up at Andersens ranch to volunteer for the drive.  Andersen tests the boys ability to stay on a bucking horse. As the boys successfully take turns, Cimarron (A Martinez), another young man slightly older than the others, rides up.  After successfully subduing and riding the test horse, Cimarron gets into a fight with Slim, the oldest of the boys.  Andersen, though impressed by Cimarrons abilities, has misgivings because of his angry nature and sends him away.  Andersen reluctantly decides to hire the boys.
 Black camp cook arrives with a chuck wagon, making Andersons  trail crew complete.
 brand and herd the cattle and horses. Much to Andersens concern, Cimarron follows the drive from afar. However, while crossing a river, Slim slips off his horse and, unable to swim, starts to drown.  Although Slim is saved by Cimarron, Andersen berates one of the boys for his stuttering problem which nearly caused Slims death.  The stuttering boy swears at Andersen repeatedly, losing his stutter in the process.  Satisfied, Andersen decides to let Cimarron stay.  During another episode, the boys steal Nightlingers whiskey and drink it, all of them getting severely drunk.  Afterwards, one of the boys, Charlie, falls off his horse and is trampled to death by the herd. Slowly, the boys learn under Andersens tutelage and become rather good cowhands, impressing both Andersen and Nightlinger.
 Belle Fourche and sell the cattle, they use some of the proceeds to pay a stonemason to carve a marker with Andersens name and the legend "Beloved Husband and Father," in clear reference to the position that Andersen had earned in their lives. They place the marker in the approximate location of Andersens grave and head for home.

==Reception==
Some critics debated the films implication that boys become men or confirm their manhood through acts of violence and vengeance. Jay Cocks of Time Magazine and Pauline Kael of The New York Times were especially critical of these aspects of the film. 

Film historian   and Sands of Iwo Jima. In all three films, an adult takes a group of youngsters and initiates them into manhood by instructing them the right skills and values. Wayne did not hesitate to appear in The Cowboys, despite the fact that no actor in his right mind, would try to match the antics of eleven kids on screen, but for him it became the greatest experience of my life." 
 Western Heritage Awards

==Cast==
* John Wayne as Wil Andersen
* Roscoe Lee Browne as Jebediah Nightlinger
* Bruce Dern as Asa Watts
* Colleen Dewhurst as Kate Collingwood (traveling madam)
* Slim Pickens as Anse Peterson
* Lonny Chapman as Mr. Weems  Sarah Cunningham as Annie Andersen
* Allyn Ann McLerie as Ellen Price (a teacher)
* Alfred Barker Jr. as Fats (Cowboy)
* Nicholas Beauvy as Dan (Cowboy)
* Steve Benedict as Steve (Cowboy)
* Robert Carradine as Slim Honeycutt (Cowboy)
* Norman Howell as Weedy (Cowboy)
* Stephen R. Hudis as Charlie Schwartz (Cowboy)
* Sean Kelly as Stuttering Bob (Cowboy)
* A Martinez as Cimarron (Cowboy)
* Clay OBrien as Hardy Fimps (Cowboy)
* Sam OBrien as Jimmy Phillips (Cowboy)
* Mike Pyeatt as Homer Weems (Cowboy)
* Charles Tyner as Stonemason Matt Clark as Smiley
* Jerry Gatlin as Howdy
* Walter Scott as Okay
* Wallace Brooks as Red Tucker
* Charise Cullin as Elizabeth
* Larry Randles as Ben
* Larry Finley as Jake
* Jim Burk as Pete
* Ralph Volkie as Ralphie (uncredited) 
* Lonny Chapman as Homers Father 
* Maggie Costain as Phoebe    
* Richard Farnsworth as Henry Williams
* Wallace Brooks as Red Tucker 
* Collette Poeppel as Rosemary
* Norman Howell Sr. as Jims Father
* Rita Hudis as Charlies Mother 
* Margaret Kelly as Bobs Mother 
* Fred Brookfield as Rustler
* Tap Canutt as Rustler
* Chuck Courtney as Rustler
* Gary Epper as Rustler
* Tony Epper as Rustler
* Kent Hays as Rustler
* J.R. Randall as Rustler Henry Wills as Rustler 
* Joe Yrigoyen as Rustler

==Television adaptation==
  ABC starring Jim Davis, Diana Douglas, and Moses Gunn. David Dortort, best known for Bonanza, The High Chaparral, and The Restless Gun, produced the series. Only A Martinez, Robert Carradine, as well as Sean Kelly and Clay OBrien, were in both the film and the television series. At the last moment, ABC decided to change the shows format by reducing its run-time from one hour to one half-hour, a change which made it difficult to tell stories effectively considering the shows large cast.  

==References==
 

==External links==
*  
*  
*  

 

 

 
 
 
 
 
 
 
 
 
 