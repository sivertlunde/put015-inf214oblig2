She's the Man
{{Infobox film
| name           = Shes the Man
| image          = Shes the man poster.jpg
| caption        = Theatrical release poster
| director       = Andy Fickman
| producer       = Lauren Shuler Donner Tom Rosenberg Gary Lucchesi Kirsten Smith
| based on       = Twelfth Night by William Shakespeare
| starring       = {{Plainlist|
* Amanda Bynes
* Channing Tatum
* Laura Ramsey
* David Cross
* Vinnie Jones
* Alex Breckenridge James Kirk Robert Hoffman
* Jonathan Sadowski
* Emily Perkins}}
| music          = Nathan Wang
| cinematography = Greg Gardiner
| editing        = Michael Jablow
| studio         = Lakeshore Entertainment
| distributor    = DreamWorks Pictures
| released       =  
| runtime        = 105 minutes
| country        = United States
| language       = English
| budget         = $20 million
| gross          = $57.2 million 
}}

Shes the Man is a 2006 American romantic comedy film directed by Andy Fickman, inspired by William Shakespeares play Twelfth Night. The film stars Amanda Bynes, Channing Tatum, Laura Ramsey, and Vinnie Jones.

The film centers on teenager Viola Hastings who enters her brothers school in his place, pretending to be male, in order to play with the boys soccer team.

==Plot== James Kirk), is supposed to enter Illyria as a new student, and since he is instead going to a contest in London with his fledgling band, Sebastian asks Viola to cover for him by telling the school that he is sick and making each of their parents (who are divorced) think that he is staying with the other.
 Robert Hoffman), who plays for the rival team. With the help of her stylist friend Paul and his girlfriends, Viola becomes the quirky "Sebastian".
 striker on the soccer team. Tryouts see Viola assigned to second string, much to her dismay. Meanwhile, "Sebastians" lab partner, Olivia (Laura Ramsey) develops a crush on him, which frustrates Duke, who likes Olivia. In exchange for "Sebastians" help in getting Olivias attention, Duke agrees to put in extra soccer practice time with him. Coach Dinklage eventually moves "Sebastian" up to first string, and he is delighted.

"Sebastian" is reminded, by a message from his mother, about the Junior League carnival that the siblings promised to attend. Due to the fact that Dukes mother is also a part of the Junior League, he must go as well. "Sebastian" is forced to switch between herself (Viola) and Sebastian the whole time, while trying to avoid her mother and Monique, Sebastians ex-girlfriend, so that she is not discovered as impersonating her brother. Viola is trying to run around and do both her and her brothers jobs at the carnival. Duke misses his chance with Olivia when her shift is up at the kissing booth. He is not disappointed by her replacement, who turns out to be "Sebastians" sister, Viola. Duke and Viola meet for the first time and exchange their kiss at the kissing booth, only to be interrupted by Violas ex-boyfriend, Justin. Duke and Justin get into a fight and decide they will finish the battle on the soccer field. Duke realizes he might be crushing not only on Olivia, but on Viola as well.

Olivia goes on a date with Duke to make "Sebastian" jealous, remaining far more interested in him than in Duke. Encouraged by Viola, she decides to go directly to Sebastian and tell him how she feels.

The plot becomes complicated when the real Sebastian returns from London a day early. When he arrives at Illyria, Olivia runs up and kisses him. Duke, seeing this, believes his roommate has betrayed him and after arguing with "Sebastian" kicks him out. Viola stays in Eunices room and oversleeps, causing the real Sebastian to wind up on the pitch playing in what should be his sisters spot in the next days much-anticipated game against Cornwall. Principal Gold (David Cross), who has been told of Violas impersonating Sebastian by Malcolm and Sebastians ex-girlfriend, Monique, he halts the game and informs the crowd that Sebastian is a girl. The real Sebastian being present instead, he proves himself to be male by pulling down his shorts and exposing his genitals (off-screen). At half-time, Viola explains the situation to Sebastian and they switch places again.

Later on in the game Viola explains that she has been impersonating her brother, finally convincing Duke and everyone else by showing them her breasts (again, off screen). The coach (Vinnie Jones) agrees to let Viola keep playing anyway, sternly informing the Cornwall coach that Illyria doesnt discriminate based on gender and he calls out the coach for his sexist ways. Illyria wins the game on a penalty kick when Viola scores a goal (after a save and then pass from Duke), finally humiliating Justin and the rest of the Cornwall boys.

Everyone at Illyria celebrates their victory over Cornwall, except for Duke who is hurt at Violas deception. She invites Duke to her debutante ball, but she leaves as he doesnt show. Whilst in the garden, Duke turns up and they head to the debutante ball, where they are introduced, come on stage, and kiss. Monique and Justin come on stage together followed by Olivia and Sebastian. At the end of the film, Viola and Duke are shown playing on the soccer team of Illyria a year later, both on the first string.

==Cast==
* Amanda Bynes as Viola (Twelfth Night)|Viola/Sebastian Hastings Orsino
* Brandon Jay McLaren as Toby Olivia Lennox James Kirk Sebastian Hastings
* David Cross as Principal Horatio Gold
* Vinnie Jones as Coach Dinklage
* Alex Breckenridge as Monique Valentine
* John Pyper-Ferguson as Roger
* Julie Hagerty as Daphne
* Lynda Boyd as Cheryl Robert Hoffman as Justin Drayton
* Jonathan Sadowski as Paul Antonio
* Amanda Crew as Kia
* Jessica Lucas as Yvonne James Snyder as Malcolm Feste
* Robert Torti as Coach Pistonek
* Clifton MaCabe Murray as Andrew
* Emily Perkins as Eunice Bates
* Mark Acheson as Groundskeeper

==Reception==

===Box office===
The film opened at #4 at the North American box office making $10.7 million USD in its opening weekend. Its budget was approximately $20,000,000. Shes the Man grossed a total of $33.7 million domestically with a total gross of $57.2 million worldwide.   

===Critical response===
Review aggregation website  , which assigns a weighted mean rating out of 100 reviews from film critics, the film has a rating score of 45 out of 100 based on 28 reviews. 

==See also==
* Hana-Kimi
* Just One of the Guys
* Twelfth Night

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 