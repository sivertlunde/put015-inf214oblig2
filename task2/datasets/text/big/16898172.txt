Walking Tall: Final Chapter
{{Infobox film
| name           = Walking Tall: Final Chapter
| image          = File:Film_Poster_for_Walking_Tall_Final_Chapter.jpg
| imagesize      =
| caption        = Theatrical release poster
| director       = Jack Starrett
| producer       = Charles A. Pratt
| writer         = Howard B. Kreitsek  Samuel A. Peeples
| starring       = Bo Svenson  Lurene Tuttle  Forrest Tucker  Leif Garrett  Dawn Lyn
| music          = Walter Scharf
| cinematography = Robert B. Hauser
| editing        = Houseley Stevenson, Jr.
| distributor    = Cinerama
| released       =  
| runtime        = 112 min
| country        = United States 
| language       = English
| budget         =
| gross          =
}} Walking Tall film series. The film was directed by Jack Starrett. The film opened in the U.S. on August 10, 1977. The on-screen title of the movie is "Final Chapter Walking Tall". All Walking Tall films were shot in Chester and Madison Counties, Tennessee; Pusser was sheriff of McNairy County.

==Plot==
 
The movie begins with Buford (Bo Svenson) remembering the ambush that killed his wife. One year has passed, and Buford still has a hard time dealing with it. He goes to his wifes grave and breaks down, telling her  that he wished he had listened to her about not becoming sheriff.

Buford then goes to the Tennessee Bureau of Investigation wanting to know why John Witter (Logan Ramsey) is not in jail. Buford is told that there is nothing that can be done since they have no case against him. Pinky Dobson is paralyzed and his girlfriend retracted her statement implicating Witter. Buford then goes to see his friend, attorney Lloyd Tatum (Sandy McPeak) and asks if this is true. Lloyd tells Buford that its just the way the law reads, and to be patient and let the TBI do their job.

John Witter is now in New York City, and tells his boss (Morgan Woodward) that he will settle things with Buford once and for all. The boss tells Witter that he should face the fact that Pusser beat him, and let it be at that. There isnt any prostitution, stills or gambling left in McNairy County, Witter is told that due to his mistakes, he must cede 25% of his territory, which Witter says he doesnt have to accept, but the boss tells him he will.

Meanwhile Buford and Grady (Bruce Glover) are staking out a still being run by O.Q. Teal (Clay Tanner) and his brother Udell (Michael Allen Honacker). Buford witnesses O.Q. beating his son Robby (David Adams), and intervenes. He takes Robby to the car and tells Grady to entertain him while he deals with O.Q.  Buford orders O.Q. to drop his trousers and proceeds to give him the same treatment he gave his son. They then depart, blasting the still as they leave. Buford takes Robby to a boys home, and returns to the office to find a message from Luan Paxton (Margaret Blye) a prostitute who helped Buford get rid of the state line gang. Buford arrives at the motel and upon seeing Luan, asks her if she is still hooking. He is surprised when she tells him that she is on her way to the gulf coast and works in real estate.

Buford arrives at home and his dad, Carl (Forrest Tucker) tells him that his tax situation is bad due to all the expenditures he incurred while sheriff. Buford says he will ask the county for a raise after reelection. Carl asks Buford if he really wants to remain sheriff despite all the pain it has caused, and Buford says it is the only thing he knows how to do.

Buford arrives at his office the next morning to find O.J. Teals lawyer, French (Taylor Lacher) waiting. French shows Buford the pictures taken of O.Q. after the beating that Buford inflicted on him. French wants Buford to drop the charges against O.Q. and in  return he will not file police brutality charges. Buford declines to which French reminds him that the office of sheriff is up for election soon.

Buford has a meeting with Sheriff Clegg (Lecile Harris) from Hardin County. Sheriff Clegg wants Buford to check out a new club called the 3 Deuces. The club is in an area that no county claims, and Buford is reluctant to involve himself, but reluctantly agrees. As they are talking, some boys steal Bufords car and go for a joy ride. Buford and Sheriff Clegg give chase, and finally catch them. Buford takes them into custody, handcuffs them to each other and has them clean up the courthouse lawn. French sees this, and confronts Buford about their civil rights. Buford says that the alternative would be a lot worse. One of the witnesses places a phone call to John Witter telling him everything that happened. Witter believes he can use this against Pusser on election day.

Pusser goes to the 3 Deuces bar, and is surprised to see Luan there. Angry that she is still a prostitute, he offers her money, which Bulow (H.B. Haggerty) the bar manager sees. After Buford leaves, Bulow confronts Luan, and tortures her. Witters boss is angry that Witter opened the 3 Deuces without his consent, and orders him to shut it down.

With the election coming up, Lloyd tells Buford that French has mounted a good campaign against him, and that there will be a citizens meeting, and wants Buford to attend. Buford is reluctant at first, but finally concedes and agrees to attend. As the meeting is set to begin, Buford gets a radio call that they have found a dead body, Buford departs. Lloyd speaks on Bufords behalf, reminding the residents that Buford put his life on the line to make McNairy County safe. French then speaks, and says that the time is passed for the type of law enforcement that Buford administers. Buford arrives at the scene, and sees that the dead body is that of Luan. He angrily leaves for the 3 Deuces. Once there, he storms in, orders everyone to leave, and starts busting up the place. Milo and his thugs fight with Buford, and defeated, they leave. Buford then goes back to his car, gets a flare gun, and fires shots into the windows, proceeding to burn the place down.

Election day comes, and the next day Buford is informed by his dad that he lost in his bid for reelection. John Witter returns home elated that Pusser lost, and is ready to do business again. He is told that it might be hard to do. Buford has lunch with his secretary Joan (Libby Boone) and tells her that he will run in the next election. She asks him what he will do in the meantime, and he says he has applied for the highway patrol. Bufords parents are beginning to worry about Bufords situation, and their own finances since the lumber mill they owned is no longer in operation.

Buford is now earning some money fixing up old cars and selling them. While in town, O.Q. Teal and his brother, now out of jail, see Bufords car and finding Bufords stick in the front seat, begin smashing it. Buford confronts O.Q. and his brother and knocks them out. The new sheriff (R.D. Smith) arrives, and believes Buford is causing trouble. Since no one is willing to witness the confrontation, Lloyd does, and presses charges against the Teals.  The sheriff apologizes, but tells Buford not to cause any more trouble.

Carl has a talk with Buford and says that he had to borrow money from Bufords brother to help pay their bills. Buford then goes to his wifes grave and says that he doesnt know what to do. Bufords daughter Dwana (Dawn Lyn) reassures him that everything will be alright.

Mel Bascum (John Malby), a Hollywood movie producer, sees a news story about Buford, and is impressed at what he sees. He goes to Buford with a movie deal, which Buford is reluctant to sign since it will require filming the ambush that killed his wife. Buford seeks Lloyd for advice, and signs the deal, seeing that it will solve his financial situation. Buford acts as technical consultant and when the movie premiers, Buford is nervous. When the ambush scene comes, he walks out of the movie.

Witter is angered at the publicity that the movie has generated, and his boss tells him to leave it alone. Buford purchases mini bikes for his children, much to his mothers (Lurene Tuttle) dismay. She asks him if he plans to spoil his children, and he replies "I hope so".

Buford has dinner with Joan, and tells her that he still plans to run for sheriff. He also tells her that he did a screen test in Hollywood, and will play himself in the next movie. With part of the money he gets, Buford buys himself a new car. The children are going to the state fair, and Buford has to meet with the movie execs. That night Buford returns, and as he parks his car, he is seen by Johnny (Robert Phillips), one of the goons from the 3 Deuces. Buford enjoys the evening with his daughter, telling her "good times are just beginning". Dwana rides home with her friend.  Witter calls his boss saying he is ready to do business again. The Boss hangs up and orders a hit on Witter. Driving home, Bufords car suddenly goes out of control and leaves the road, with Buford being thrown clear. The car then bursts into flames. Minutes later, Dwana arrives at the scene and breaks down at the sight of her father. The movie closes with a shot of the marker at the actual site where Buford Pusser died.

==Cast==
*  
* Forrest Tucker: Grandpa Carl Pusser
* Lurene Tuttle: Grandma Helen Pusser
* Leif Garrett: Mike Pusser
* Dawn Lyn: Dwana Pusser
* Simpson Hemphill: Brownard
* Sandy McPeak: Lloyd Tatum
* Logan Ramsey: John Witter
* Morgan Woodward: The Boss
* Clay Tanner: O.Q. Teal
* Maggie Blye: Luan Paxton
* Bruce Glover: Deputy Grady
* David Adams: Robbie Teal
* Michael Allen Honaker: Udell Teal
* Vance Davis: Aaron
* Libby Boone: Joan, Pussers Secretary

==External links==
*  
*  

 
 

 
 
 
 
 