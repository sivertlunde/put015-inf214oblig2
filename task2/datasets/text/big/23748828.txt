Wuthering Heights (1988 film)
{{Infobox film
| name           = Wuthering Heights
| image          =
| caption        =
| director       = Yoshishige Yoshida
| producer       =
| writer         = Emily Brontë Yoshishige Yoshida
| starring       = Yusaku Matsuda
| music          =
| cinematography = Junichiro Hayashi
| editing        =
| distributor    =
| released       =  
| runtime        = 143 minutes
| country        = Japan
| language       = Japanese
| budget         =
}}

  is a 1988 Japanese drama film directed by Yoshishige Yoshida, based on the novel by Emily Brontë. It was entered into the 1988 Cannes Film Festival.   

==Cast==
* Yusaku Matsuda as Onimaru
* Yuko Tanaka as Kinu
* Rentarō Mikuni as Takamaru
* Tatsuo Nadaka as Mitsuhiko
* Eri Ishida as Tae
* Nagare Hagiwara as Hidemaru
* Keiko Ito as Shino
* Masato Furuoya as Yoshimaru
* Tomoko Takabe as Kinu, daughter
* Masao Imafuku as Ichi
* Taro Shigaki

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 