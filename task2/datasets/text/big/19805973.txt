Flagermusen
 
{{Infobox film
| name           = Flagermusen
| image          = 
| caption        = 
| director       = Annelise Meineche
| producer       = 
| writer         = John Hilbard Volmer Sørensen
| narrator       = 
| starring       = Annette Blichmann
| music          = 
| cinematography = Ole Lytken
| editing        = Lizzi Weischenfeldt
| distributor    = 
| released       =  
| runtime        = 96 minutes
| country        = Denmark 
| language       = Danish
| budget         = 
}}

Flagermusen is a 1966 Danish film directed by Annelise Meineche and starring Annette Blichmann.

==Cast==
* Annette Blichmann - Felicita, dancer and friend of Ida
* Lily Broberg - Rosalinde
* Poul Bundgaard
* Dario Campeotto
* Paul Hagen - Fængelsbetjent
* Holger Juul Hansen - Dr. Falke
* Susanne Heinrich - Dancer and friend of Ida
* Knud Hilding
* Niels Hinrichsen
* Valsø Holm Arthur Jensen
* Dida Kronenberg - Dancer and friend of Ida
* Grethe Mogensen Henry Nielsen
* Ghita Nørby - Kammerpige Adele
* Bjørn Puggaard-Müller - 
* Poul Reichhardt - Von Eisenstein
* Birgit Sadolin - Ida, Adeles Søster
* Ove Sprogøe - Advokat
* Karl Stegger - Fængelsdirektør
* Jeanette Swensson - Dancer and friend of Ida
* Olaf Ussing

==External links==
* 

 
 
 


 