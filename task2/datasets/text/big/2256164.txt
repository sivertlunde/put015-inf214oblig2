Hellfighters (film)
{{Infobox film
| name           = Hellfighters
| image          = Hellfighters theatrical poster.jpg
| caption        = Original theatrical poster
| director       = Andrew V. McLaglen Robert Arthur
| writer         = Clair Huffaker
| starring       = {{plainlist|
* John Wayne
* Katharine Ross
* Jim Hutton
* Vera Miles
* Jay C. Flippen
}}
| music          = Leonard Rosenman
| cinematography = William H. Clothier
| editing        = Folmar Blangsted
| studio         = Universal Pictures
| distributor    = Universal Pictures
| released       =  
| runtime        = 121 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $3.75 million (US/ Canada rentals) 
}}
Hellfighters is a 1968 American film starring John Wayne and featuring Katharine Ross, Bruce Cabot, Jim Hutton, Jay C. Flippen and Vera Miles. The film, directed by Andrew V. McLaglen, is about a group of oil well firefighters, based loosely on the life of Red Adair.  Adair, "Boots" Hansen, and "Coots" Matthews, served as technical advisers on the film.

Hellfighters was for the most part negatively received.

==Plot==
Chance Buckman is the head of a Houston–based oil-fire fighting outfit. With a team that includes Joe Horn, Greg Parker, and George Harris, Chance travels around the world putting out blazes at well heads from industrial accident, explosion or terrorist attack.  Chance enjoys the thrills, but longs for ex-wife Madelyn (Vera Miles) he divorced 20 years earlier, taking their daughter Leticia with her, because Madelyn could not bear to see her husband risk his life. Though they love each other, Madelyn could not deal with her terror that Chance might burn to death in a fire.

While extinguishing a burning wellhead, Chance suffers a near-fatal accident when he is crushed by a bulldozer blade. Against his wishes, his daughter visits. He discovers that his assistant Greg married Tish five days after meeting her. (Greg has a notorious reputation for using fires to pick up women. Generally, any woman he takes to a fire ends up in bed with him. In the case of Buckmans spitfire of a daughter, they fell in love at first sight and married instead.) In spite of Gregs reputation, Buckman comes to trust his daughters choice. He accepts Greg into the family. Madelyn, projecting her own fears onto her daughter, though gracious is rather less accepting despite her liking for Greg.

Greg suspects that his new father-in-law is growing increasingly protective of him after the marriage in an effort to protect his daughter from heartbreak should her new husband be harmed or killed.  Tish wishes to see the fires that her husband and father fight, something that neither man encourages. Her father relents and allows her to accompany Greg into the field.

Chance, trying to re-unite with his ex-wife, leaves The Buckman Company to take a safer job at Lomaxs oil company as a way to win her back. Chance gives his company to his son-in-law as a "wedding present", although Gregs pride forces him to tell Buckman he "doesnt want any gifts" and that he will "pay twice what its worth." Greg and Tish begin traveling the world to put out oil-well fires. Soon the older couple announce that they will remarry, to the delight of Tish. Chance accepts an executive position from his old friend Jack Lomax to serve on the board of directors for Lomax Oil. Madelyn is happy to see her husband in a safe job; but before too long Chance becomes bored with corporate life and longs to be back in the field. As Jack Lomax earlier told Tish, "Your father is the best there is at what he does. No man can walk away from that."
 guerrillas who are trying to undermine the operation. He asks Chance to return and help fight the fire. He does so without hesitation. Chance goes to Venezuela, unaware that Madelyn and Tish are going as well.  Madelyn declares "This is it for me," in the sense that it will either make or break her ability to deal with the fires once and for all, fully aware that her relationship is on the line. The team puts out the fires with the help of the Venezuelan army while under attack by rebel P-40 Tomahawk fighters that strafe the oilfield. Madelyn explodes in anger at what she perceives as the Venezuelans inability to protect the team from the unexpected airplane attack, at which point Chance pulls her away during her tirade.  She snaps, "Damned if I understand your attitude!", to which he replies, "Its very simple -- youll do." When Greg asks Tish for her take on it, she just says, "I think we ought to get her a tin hat," referring to the bright red hardhats with The Buckman Company logo worn by the Hellfighters.

==Cast==
* John Wayne as Chance Buckman
* Vera Miles as Madelyn Buckman
* Katharine Ross as Tish Buckman
* Jim Hutton as Greg Parker
* Jay C. Flippen as Jack Lomax
* Bruce Cabot as Joe Horn
* Barbara Stuart as Irene Foster
* Edward Faulkner as George Harris
* Pedro Gonzalez Gonzalez as Hernando (Houseboy) (uncredited)  
* Edmund Hashim as Colonel Valdez 
* Alberto Morin as General Lopez 
* Howard Finch as Ed Cal Calhoun 
* Valentin de Vargas as Amal Bokru
* Frances Fong as Madame Loo 
* Alan Caillou as Harry York  John Alderson as Jim Hatch

==Critical reception==
Hellfighters received mostly negative reviews, garnering a 14% rating on Rotten Tomatoes (one "Fresh" rating and seven "Rotten" ratings).  Roger Ebert of the Chicago Sun-Times described the film as a "slow moving, talkative, badly plotted bore".   A. H. Weiler of The New York Times wrote that John Wayne made "actionful, if not stirringly meaningful, childs play of exotic disasters" and remarked that "the unrestrained cast and director maintain a welcome sense of humor". 

==See also==
*John Wayne filmography
*List of firefighting films

==Notes==
 

==External links==
 
* 
* 
* 
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 