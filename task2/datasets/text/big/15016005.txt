The Baby Maker
{{Infobox Film
| name           = The Baby Maker
| image          = Poster of the movie The Baby Maker.jpg
| image size     = 
| caption        = Spanish film poster
| director       = James Bridges
| producer       = Richard Goldstone  Jack Larson
| writer         = James Bridges
| narrator       = 
| starring       = Barbara Hershey Scott Glenn Collin Wilcox-Horne Sam Groom
| music          = Fred Karlin
| cinematography = Charles Rosher, Jr. Walter Thompson
| distributor    = National General Pictures (1970,original) Warner Bros. (2009, DVD)
| released       =  
| runtime        = 109 minutes
| country        = United States
| language       = English
| budget         = 
| preceded by    = 
| followed by    = 
}}

The Baby Maker (1970) is a film directed and co-written by James Bridges and released by National General Pictures.

==Plot== Collin Wilcox-Horne and Sam Groom). 

The film exposes the clash of values between Hershey, along with her boyfriend (Scott Glenn), and the couple. It also deals with the emotional turmoil all four characters go through.

==Reception==
Criticizing the directing and writing of James Bridges, critic Shirley Rigby said of the "bizarre" film, "Only the performances in the film save it from being a total travesty."  Rigby went on to say, "Barbara Hershey is a great little actress, much, much more than just another pretty face." Rigby, Shirley.  . The Miami News, December 16, 1970, pg. 19 A. 

==Cast==
* Barbara Hershey as Tish Gray Collin Wilcox Paxton as Suzanne (as Collin Wilcox-Horne)
* Sam Groom as Jay Wilcox
* Scott Glenn as Tad Jacks
* Jeannie Berlin as Charlotte
* Lili Valenty as Mrs. Culnick
* Helena Kallianiotes as Wanda
* Jeff Siggins as Dexter
* Phyllis Coates as Tishs Mother
* Madge Kennedy as Tishs Grandmother
* Ray Hemphill as the Toy Store Killer
* Paul Linke as Sam
* Bobby Pickett as Dr. Sims Samuel Francis as The Single Wing Turquoise Bird
* Alan Keesling as The Single Wing Turquoise Bird (2)

==DVD==
The Baby Maker was released to DVD by Warner Home Video on March 23rd, 2009, via the Warner Archives DVD-on-demand service as a Region 1 DVD.

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 

 