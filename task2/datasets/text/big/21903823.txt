Vennila Kabadi Kuzhu
{{Infobox film
| name = Vennila Kabadi Kuzhu
| image =
| image size =
| caption =
| director = Susindran
| producer = K. Anand Chakravarthy
| writer = Susindran Bhaskar Sakthi (dialogue) Kishore Saranya Mohan  Srithika
| music = V. Selvaganesh
| cinematography = J. Laxman Kumar
| editing = Kasi Viswanathan
| studio = Imagine Creations
| distributor =
| released = 29 January 2009
| runtime =
| country = India
| language = Tamil
| budget =
| gross =  7.1 crore
| preceded by =
| followed by =
}}
 Indian Tamil Tamil drama Kishore and Saranya Mohan along with numerous other newcomers in the lead roles.
 Nani and Saranya Mohan in the lead. And remade in Hindi as Badlapur Boys in the year 2014 with Nishan and Saranya Mohan in the lead.

==Story==
The story unfolds in a remote village near Palani called Kanakkanpatti with a poor goat-herd, Marimuthu as its protagonist who lost education as his father died @ his age of 13, he also plays kabbadi very well. Marimuthu along with his childhood friends Ayyappan (Vairavan), Sekar (Nithish), Murthy (Sundar), Appu Kutty (Appu Kutty), Suri (Parota Suri) and Pandi (Pandi) are kabbadi players who dream of winning a local tournament.

Sekar is a rich short tempered rice mill owner. Appu Kutty is a short, tea shop owner who often gets a heavy scolding from his old mother-in-law. Suri is a very newly wed man who has only one hobby: eating many (max 100) parotas per meal. Pandi is a big fat local stores owner who is a son of an ex-army man. Their kabbadi team, Vennila Kabadi Kuzhu (Full-Moon Kabbadi Team), is infamous for never winning a match in their history. In a sub-plot, Marimuthu meets and flirts with an unnamed young lady who comes visiting to his villages annual festival. They organize a kabbadi match for the festival in which an outbreak in the game occurs. Then they go to Madurai for a non-detail known match where Saouda Muthu is one of the chief conductors and a coach. He tells it is a state-level tournament in which no local teams may participate. Then Saouda Muthu separates from his state winning team and starts coaching the Vennila Kabadi Kuzhu.

The titular kabbadi team, Vennilla, finds itself by chance in a State-level tournament as dindigul team give an unknown walkover, with no chance of progressing beyond the first round. The team, however, finds an ally in Saouda Muthu (Kishore (actor)|Kishore), a professional kabbadi coach who teaches them the finer nuances of the game. The team progresses through the tournament into the final despite the myriad hurdles and disadvantages, with the help of their coach.

In the final, Vennilla struggles initially but manages to fight back into contention in the second half. As the game ends in a tie, players from each team go up individually against each other as a tie-breaker. In the final stages of the head-to-head, Marimuthu wins the game for Vennilla. However, it is revealed later that, Marimuthu dies in the match when he won the winning point.

The scene changes to several month later when the unnamed young lady returns to the village festival where she met Marimuthu the previous year. However, his friends, despite noticing her search for him, decide not to reveal to her Marimuthus demise to spare her the anguish. The movie ends ambiguously, with the unnamed lady leaving the village without knowing Marimuthus fate.

==Cast==
*  Vishnu Vishal as Marimuthu Kishore as Souda Muthu
*  Saranya Mohan
*  Vijay Sethupathi Soori as Subramani
*  Appukutty as Appukutty
*  Srithika
*  Nithish as Sekar
*  Mayi Sundar as Murthy
*  Vairavan as Ayyappan
*  Ramesh Pandiyan as Paandi
*  B. Neelan
*  Chandraprakash
*  Janaki
*  ShivaMuthu as Kalimuthu
*  Ramachandran Durairaj

==Production==
According to Susindran, the film is based on real life incidents as his father was a kabadi player and Susindran got to know about the insults and pains the player went through, which he wanted to showcase in a film.    Also he was said to be inspired from success of the 2002 Bollywood film Lagaan, which was based on cricket. 

==Box office==
*The film was successful at the box office. 

==Soundtrack==

The songs were penned by Na. Muthukumar, Francis Kriba, Snehan & Karthiknetha and composed by V. Selvaganesh

{| class="wikitable"
|-  style="background:#cccccf; text-align:center;"
| No. || Song || Lyricist || Singers ||Time 
|- Francis Kriba|| Shankar Mahadevan || 
|-
| 2|| "Lesa Parakkuthu" ||Na. Muthukumar|| Karthik & Chinmayi||04:19 
|-
| 3|| "Vandanam Vandanam" ||Snehan|| Pandi, Malathi, Maya & Vijay||  
|-
| 4|| "Pada Pada" ||Karthiknetha|| Karthik||  
|-
| 5|| "Uyiril Yetho" ||Na. Muthukumar|| Haricharan||  
|}

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 