Volcano (2011 film)
{{Infobox film
| name           = Volcano
| image          = Volcano (2011 film).jpg
| caption        = Film poster
| director       = Rúnar Rúnarsson
| producer       = Egil Dennerline
| writer         = Rúnar Rúnarsson
| starring       = Theodór Júlíusson
| music          = 
| cinematography = Sophia Olsson
| editing        = 
| distributor    = 
| released       =  
| runtime        = 95 minutes
| country        = Iceland
| language       = Icelandic
| budget         = 104 million Icelandic krona|króna (estimated)
}}
 Best Foreign Language Film at the 84th Academy Awards,       but it did not make the final shortlist.    At the 2012 Edda Awards, the film was nominated in 14 categories, winning in 5.    

The film screened within many international film festivals, including the 2011 Toronto International Film Festival and the 2012 Maryland Film Festival.

==Cast==
* Auður Drauma Bachmann as Tinna
* Þorsteinn Bachmann as Ari
* Kristín Davíðsdóttir as Nurse
* Benedikt Erlingsson as Pálmi
* Elma Lísa Gunnarsdóttir as Telma
* Þröstur Leó Gunnarsson as Janitor
* Harald G. Haraldsson as Principal
* Margrét Helga Jóhannsdóttir as Anna
* Theodór Júlíusson as Hannes

==See also==
* List of submissions to the 84th Academy Awards for Best Foreign Language Film
* List of Icelandic submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 
 
 