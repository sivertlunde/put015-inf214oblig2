Hulda from Holland
 
{{infobox film
| name           = Hulda from Holland
| image          =File:Hulda From Holland poster.jpg
| imagesize      =
| caption        =Film poster
| director       = John OBrien
| producer       = Adolph Zukor
| writer         = Edith Barnard Delano
| cinematography = Emmett A. Williams
| editing        =
| distributor    = Paramount Pictures
| released       =   reels
| country        = US
| language       = Silent film English intertitles

}}
Hulda from Holland is a 1916 silent film produced by Famous Players Film Company and released by Paramount Pictures. It stars Mary Pickford, then the biggest movie star in America. The story is an original for the screen called Miss Jinny.  

==Cast==
*Mary Pickford - Hulda
*Frank Losee - John Walton John Bowers - Allan Walton
*Russell Bassett - Uncle Peter
*Harold Hollacher - Little Yacob
*Charles E. Vernon - The Burgomaster

unbilled
*Cesare Gravina - Apartment Neighbor

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 


 