Chi-hwa-seon
{{Infobox film
| name = Painted Fire
| image    = Painted_Fire_movie_poster.jpg
| caption  = Poster for "Painted Fire"
| film name      = {{Film name
 | hangul   =  
 | hanja    =  
 | rr       = Chwihwaseon
 | mr       = Chwihwasǒn}}
| director = Im Kwon-taek		
| producer = Lee Tae-won
| writer   = Kim Yong-ok Do-ol
| starring = Choi Min-sik Ahn Sung-ki Yoo Ho-jeong
| music    = Kim Yong-dong
| cinematography = Jeong Il-seong
| editing  = Park Sun-deok
| distributor = Cinema Service
| released =  
| runtime  = 117 minutes
| country  = South Korea
| language = Korean
| budget   =
| gross    = $6,906,235 
}} painter who changed the direction of Korean art.

==Synopsis==
It begins with the Korean artist being suspicious of a Japanese art-lover who values his work.  The story then goes back to his mans early years. Beginning as a vagabond with a talent for drawing, he has a talent for imitating other peoples art, but is urged to go on and develop a style of his own.  This process is painful and he often behaves very badly, getting drunk and being hostile to those who care about him and try to help him.
 China and Japan (annexed by Japan in 1910, outside the films time-frame).

==Cast==
* Choi Min-sik: Jang Seung-up
* Ahn Sung-ki: Kim Byung-Moon
* Yoo Ho-jeong: Mae-hyang
* Kim Yeo-jin: Jin-jong
* Son Ye-jin: So-woon

== Awards ==
* Best Film; Blue Dragon Film Awards  Best Director; 2002 Cannes Film Festival    Grand Prix; Belgian Syndicate of Cinema Critics 

=== Nominations ===
* Golden Frog; Jung Il-sung
* Golden Palm; Im Kwon-taek
* César Award for Best Foreign Film|César Best Foreign Film (Meilleur film étranger)

== References ==

=== Notes ===
 

=== Sources ===
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  

== External links ==
*  
*   at koreanfilm.org

 
 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 


 