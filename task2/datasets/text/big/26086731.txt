Damon and Pythias (film)
{{Infobox film
| name           = Damon and Pythias
| image_size     =
| image	=	Damon and Pythias FilmPoster.jpeg
| caption        =
| director       = Curtis Bernhardt Sam Jaffe (associate producer) Samuel Marx (associate producer) Franco Riganti  (producer) (uncredited)
| writer         =   (dialogue: Italian version)   (dialogue: english version) Jon Cleary (uncredited)
| narrator       =
| starring       = See below
| music          = Angelo Francesco Lavagnino
| cinematography = Aldo Tonti
| editing        = Niccolò Lazzari
| distributor    = Metro-Goldwyn-Mayer
| released       =
| runtime        = 101 minutes (Italy)   99 minutes (USA)
| country        = Italy, USA
| language       = English
| budget         =
| gross          =
}}
 American film Greek legend of Damon and Pythias, and set during the reign of Dionysius I of Syracuse (432-367 BC).

==Plot summary==
 

==Cast== Guy Williams as Damon
*Don Burnett as Pythias
*Ilaria Occhini as Nerissa
*Liana Orfei as Adriana
*Marina Berti as Mereka - Nerissas Friend Dionysius the Tyrant
*Carlo Giustini as Cariso
*Aldo Silvani as Patriarch
*Andrea Bosic as Arcanos
*Maurizio Baldoni as Dionysius the Younger
*Franco Fantasia as Rumius the Fencing Master
*Osvaldo Ruggieri as Demetrius - Nerissas Brother
*Lawrence Montaigne as Flute Player
*Enrico Glori as Nikos
*Gianni Bonagura as Philemon
*Vittorio Bonos as Digenis
*Giovanna Maculani as Hermione
*Carlo Rizzo as Libia
*Enzo Fiermonte
*Tiberio Mitri
*Franco Ressel
*Tiberio Murgia
*Luigi Bonos
*Maurizio Bedoni
*Giovanni Bagliori
*Carolyn De Fonseca as Chloe
*Carla Bonavera
*Enrico Salvatore

==Reception==
The film made a profit of $6,000.  . 

==Release==
The international distribution was taken over by MGM. 

==Biography== 
* 

==References==
 

==External links==
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 


 
 