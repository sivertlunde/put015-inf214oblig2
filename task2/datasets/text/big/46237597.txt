Walter (2015 film)
{{Infobox film
| name           = Walter
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = 
| director       = Anna Mastro
| producer       = Ryan Harris Brenden Patrick Hall Mark Holder
| writer         = Paul Shoulberg
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Milo Ventimiglia Leven Rambin Neve Campbell William H. Macy
| music          = Dan Romer
| cinematography = Steven Capitano Calitri
| editing        = Kristin McCasey
| studio         = 
| distributor    = Phase 4 Films
| released       =  
| runtime        = 94 minutes
| country        = USA
| language       = English
| budget         = 
| gross          = 
}}
Walter is a 2015 American comedy film, directed by Anna Mastro. It was released on March 13, 2015. The film is based on writer Paul Shoulbergs short film. 

==Plot==
 

==Cast==
* Andrew J. West ... Walter
* Milo Ventimiglia ... Vince
* Leven Rambin ... Kendall
* Neve Campbell ... Allie
* William H. Macy ... Dr. Corman
* Peter Facinelli ... Jim
* Virginia Madsen ... Karen
* Brian J. White ... Darren

==External links==
* 

==References==
 

 

 