No Vacation for Mr. Mayor
{{Infobox film
| name           = No Vacation for Mr. Mayor
| image          = 
| caption        = 
| director       = Maurice Labro
| producer       = 
| writer         = Christian Duvaleix Jacques Emmanuel
| starring       = Grégoire Aslan Louis de Funès
| music          = Henri Contet Paul Durand
| cinematography = 
| editing        = 
| distributor    = 
| released       = 4 November 1951
| runtime        = 87 minutes
| country        = France
| awards         = 
| language       = French
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 French Comedy comedy film from 1951, directed by Maurice Labro, written by Christian Duvaleix, starring Grégoire Aslan and Louis de Funès. 

== Cast ==
* André Claveau: Philippe Lebon, the singer
* Grégoire Aslan: Mr Beaudubec
* Albert Duvaleix: the mayor
* Jacques Emmanuel: Adolphe
* Fernande Montel: Maharanée
* Sylvie Pelayo: Annie
* Fred Pasquali: Tracassin
* Noël Roquevert: Uncle Joachim
* Louis de Funès: the adviser
* Jo Charrier: the photographer
* Emmanuel Cheval: the concierge

== References ==
 

== External links ==
*  
*   at the Films de France

 
 
 
 
 
 


 