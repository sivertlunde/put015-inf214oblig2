Bangarada Jinke
{{Infobox film name           = Bangarada Jinke image          =  image_size     = caption        =  director       = T. S. Nagabharana producer       =  B. S. Somasundar T. S. Narasimhan writer         = B. S. Somasundar screenplay = B. S. Somasundar narrator       = starring  Vishnuvardhan Bharathi Bharathi Aarathi Leelavathi Sundarraj music          = Vijaya Bhaskar cinematography = S. Ramachandra editing        = Yadav Victor studio         = Kavitha Komal Productions released       = 1980 runtime        = 140 minutes country        = India language       = Kannada budget         = preceded_by    = followed_by    = website        =
}}
 Leelavathi and Sundarraj in lead roles.

==Plot==
A story of rebirth and revenge in the midst of a treasure hunt for the golden deer.

==Cast== Vishnuvardhan
* Bharathi Vishnuvardhan|Bharathi,
* Aarathi as Asha Leelavathi
* Pramila Joshai
* Sundarraj Aras
* Lokanath
* Shivaram
* Musuri Krishnamurthy
* Umashri
* Shakthi Prasad, Vijay, B R Jayaram, Mico Chandru, B S Achar, Shivajirao, Kaminidharan, Sripramila, Rekha, Master Chethan 

==Notes == Vishnuvardhan and Bharathi was shot at the Channarayana Durga Hill.
Bangara Nayakas Fort is Channarayana Durga which is atop the Channarayana Hill in the vicinity of the Madhugiri Betta near Tumkur in Karnataka. 

==Soundtrack==
{{Infobox album	
| Name = Bangarada Jinke
| Longtype = to Bangarada Jinke
| Type = Soundtrack	
| Artist = Vijaya Bhaskar
| Cover = 
| Border = Yes
| Alt = Yes	
| Caption =	
| Released = 
| Recorded =	
| Length =  Kannada 
| Label = Sangeetha
| Producer = 
| Last album = 
| This album = 
| Next album = 
}}

The music of the film was composed by Vijaya Bhaskar
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers || Lyricist
|- 1 || Sangaathiyu Bali Baarade  || Vani Jayaram || Doddarange Gowda
|-
| 2 || Olume Siriya Kandu || S. P. Balasubrahmanyam, Vani Jayaram || Doddarange Gowda
|- 3 || Kenakiruve || Vani Jayaram || Doddarange Gowda
|- 4 || Olume Siriya Kandu || S. P. Balasubrahmanyam || Doddarange Gowda
|-
|}

==Awards and honors==

 

==References==
 

 
 
 
 


 