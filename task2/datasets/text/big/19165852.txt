The Lady Is a Square
{{Infobox film
| name           = The Lady Is a Square
| image          = 
| image_size     = 
| alt            = 
| caption        = 
| director       = Herbert Wilcox
| producer       = Herbert Wilcox
| writer         = Pamela Bower   Nicholas Phipps   Harold Purcell 
| narrator       = 
| starring       = Anna Neagle   Frankie Vaughan   Janette Scott
| music          = Angela Morley
| cinematography = Gordon Dines
| editing        = Basil Warren
| studio         = Herbert Wilcox Productions
| distributor    = Associated British-Pathé
| released       = 1959
| runtime        = 100 minutes United Kingdom English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} 1959 British comedy musical film directed by Herbert Wilcox and featuring Anna Neagle, Frankie Vaughan and Janette Scott. An aspiring singer goes to work as a butler in the house of a classical music patron. It was Neagles final film appearance and the last film directed by Wilcox although he produced several further films before his bankruptcy in 1964.

==Plot==
Mrs Baring, a businesswoman and patron of classical music, has arranged for a celebrated Eastern Bloc musician, Spolenski, to play in a series of concerts in Britain. However, she is aware that she is on the brink of bankruptcy and the Spolenski tour offers a final chance to save her finances.

Johnny Burns, an aspiring singer is hanging around a music shop he frequents when he spot Mrs Baring’s daughter, Joanna. Enraptured he pretends to be a piano-tuner and goes round to her house to help prepare the piano for a party held in Spolenksi’s honour. Later, when Mrs Baring is short of a butler he offers his services and is so successful at his duties that he is taken on in a more permanent basis. He slowly begins to bond and court Joanna while doing his best to conceal his love of popular, modern music from Mrs Baring who is resolutely opposed to it and has forbidden her daughter to listen to it. Her financial problems continue to mount up and her phone is cut due to unpaid bills.

Burns’ friend and agent, Freddy, meanwhile has secured him an audition with Greenslade, a major popular record label, who are impressed with his performance. Convinced he is going to be a major star, they make plans to sign him up on a long-term contract. Burn’s first demand of Greenslade is for money to pay for Mrs Baring’s telephone to be restored. Mrs Baring is relieved by this gesture, but  believes the money came from one of her other friends rather than Burns.
 Talk of the Town, while still keeping his new success a secret from the Barings. Mrs Baring has further problems when tickets for her Spolenski concerts sell badly, and he threatens to leave for home unless she is able to put up £3,000. Once again Burns secretly steps in, securing the money as an advance from Greenslades. After learning of Burn’s fame, Joanna goes to his concert. When her mother discovers this, she grows furious and confronts both of them firing Burns and forbidding her daughter from seeing him again. In his anger, he calls her a “square”. 

Their rift is not helped by his next song, “The Lady is a Square” which appears to be directly mocking her. However she relents when she discovers that he has secretly been paying her bills  and that he is trying to abandon a concert of his, which is scheduled at the same time as her Spolenski concert, in a bid to help her ticket sales. Spolenski then falls ill, which turns out to be a blessing as it will mean they will be able to re-launch the tour with financial assistance from Greenslade.
 National Youth Orchestra, to wild applause from his fans. Ultimately they are able to agree on the co-existence of popular and classical music and Mrs Baring ends the film dancing to one of Burns’ songs.

==Cast list==
*Anna Neagle as Frances Baring
*Frankie Vaughan as Johnny Burns
*Janette Scott as Joanna Baring
*Anthony Newley as Freddy
*Wilfrid Hyde-White as Charles
*Christopher Rhodes as Greenslade
*Kenneth Cope as Derek
*Josephine Fitzgerald as Mrs. Eady
*Harold Kasket as Spolenski
*John Le Mesurier as Fergusson
*Ted Lune as Harry Shuttleworth
*Mary Peach as Mrs. Freddy

==Bibliography==
* Harper, Sue & Porter, Vincent. British Cinema of the 1950s: The Decline of Deference. Oxford University Press, 2007.

==External links==
* 

 

 
 
 
 
 
 
 