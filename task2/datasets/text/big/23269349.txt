The Eliminator (film)
{{Infobox film
| name          = The Eliminator
| image         = EliminatorRutten.jpg
| caption       = movie poster
| director      = Ken Barbet
| story         = David Neilsen
| starring      = Bas Rutten Michael Rooker
| distributor   = MTI Home Video  artist view entertainment
| released      =  
| runtime       = 90 minutes
| country       = USA english
}}

The Eliminator is a 2004 direct-to-video action film starring UFC champion Bas Rutten.The film was directed by Ken Barbet.

==Plot==
Former LAPD cop, Dakota Varley (played by UFC champion Bas Rutten), enters a power boat race seeking the prize of $250,000, but immediately discovers, first hand, that this races risk matches the prize: one racer is killed, 3 others seriously injured in multiple action-packed crashes and explosions. Varley prevails. Varley tries to leave the lake and collect his prize money but is drugged and kidnapped by Dawson (portrayed by Michael Rooker) and thrown into a different world: a world where survival means everything and no-one follows the rules. Varley awakens on an island, strapped to pole, like a pig being carried to a barbecue. Surrounded by heavily armed men, he soon learns that he and six other victims have been assembled to be contestants in the ultimate survival game: they will be stalked nightly by hunters with rifles until there is one remaining survivor, who will win a $10,000,000 cash prize! As the story unfolds, we meet the other contestants: Jesse is an undercover DEA agent in Venezuela; Santha is military instructor for a group of revolutionaries in Sierra Leone; and Darius is a sociopathic murderer, court-martialed and supposedly imprisoned by the U.S. Army for slaughtering 40 innocents in Kosovo. We also learn, in bits and pieces, that each contestant has been brought to the game by a "Player." Wealthy men and women who have each selected of one of the Contestants. Among these men are Carlos Alvarez, Venezuelan Drug Lord; U.S. Army General Ellison; and Ochiro Sumanni, a Japanese Gang Boss. All the while, we follow the Contestants as they struggle to survive the attacks not only by the hunters, but also from each other.

==Cast==

*Michael Rooker as Miles Dawson.

*Bas Rutten as Dakota Varley.

*Dana Lee as Ochiro Sumanni.

*G. Anthony Joseph as Warden Sutherland.

*Wolf Muser as Deitrich Remmel.

*Marco Ruas as Salvador.

*Michael Gregory (actor)| Michael Gregory as General Ellison.

*Gareth Myles as Watson.

*Geoffrey Forward as Sir Crawley.

*Jamal Duff as Darius.

*Ivo Cutzarida as Alvarez.

*Danielle Burgio as Santha.

*Paul Logan (actor)| Paul Logan as Jesse.

*Vicki Phillips as Tonya.

*Stephen Wozniak as Rocker. 

==References==
 

==External links==
*  (2004)


 
 
 