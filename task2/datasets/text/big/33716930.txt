Always Goodbye
{{Infobox film
| name           = Always Goodbye
| image          = Always Goodbye.jpg
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| director       = Sidney Lanfield
| producer       = Samuel Goldwyn
| writer         = {{Plainlist|
* Kathryn Scola
* Edith Skouras
}}
| story          = {{Plainlist|
* Douglas Z. Doty
* Gilbert Emery
}}
| starring       = {{Plainlist|
* Barbara Stanwyck
* Herbert Marshall Ian Hunter
}}
| music          = Cyril J. Mockridge
| cinematography = Robert H. Planck Robert L. Simpson
| studio         = 20th Century Fox
| distributor    = 20th Century Fox
| released       =  
| runtime        = 75 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 romance drama Ian Hunter.      

==Plot== Ian Hunter) and his wife. Margot insists that neither the Marshalls nor the child can ever know that she is his mother.

Five years later, while working as a well-paid buyer Harriet Martin (Binnie Barnes), Margot meets Jim Howard again, and the two begin to fall in love. When Margot is sent to Europe on a business trip for Harriet, she meets and is wooed by the charming but carefree Count Giovanni Corini (Cesar Romero). While in Paris, she happens to meet her son Roddy (Johnnie Russell), who is traveling with his aunt, who has been taking care of the boy since his adoptive mother died.

On the trip back to America, Margot and Roddy become close. Count Corini is also on the same ship, and he continues to pursue Margot. Back home, Margot becomes convinced that Jessica (Lynn Bari), Phil Marshalls new fiancee, doesnt love him, and would be a bad mother to Roddy. Margot decides to break up the engagement, though Jim, beginning a career as a scientist, reminds her of her earlier promise not to interfere in the boys life.

Phil overhears a conversation between Margot and Jessica which brings their engagement to an end. Meanwhile Jim and Margot become engaged, but then Phil asks Margot to marry him for his and Roddys sake. Though she admits she loves Jim, he steps aside so that she can have a life with Roddy and Phil.

==Credits==

* Barbara Stanwyck as Margot Weston
* Herbert Marshall as Jim Howard Ian Hunter as Phillip Marshall
* Cesar Romero as Count Giovanni Gino Corini
* Lynn Bari as Jessica Reid
* Binnie Barnes as Harriet Martin
* Johnny Russell as Roddy Weston Marshall (as John Russell)
* Mary Forbes as Aunt Martha Marshall
* Albert Conti as Modiste Benoit
* Marcelle Corday as Nurse
* Franklin Pangborn as Bicycle Salesman George Davis as Taxi Driver
* Ben Welden as Taxi Driver
* Eddy Conrad as Barber 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 