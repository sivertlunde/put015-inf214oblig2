Wrinkles (film)
{{Infobox film
| name = Wrinkles
| image = Wrinkles (Arrugas) poster.jpeg
| caption = Poster
| director = Ignacio Ferreras
| producer = Manuel Cristobal Oriol Ivern
| screenplay = Paco Roca Ignacio Ferreras Rosanna Cecchini Ángel de la Cruz
| based on =  
| starring = Álvaro Guevara Tacho González
| music = Nani Garcia
| cinematography = David Cubero
| editing = Ignacio Ferreras
| studio = Perro Verde Films
| released =  
| runtime = 89 minutes
| country = Spain
| language = Spanish
| budget = € 2 million
}}

Wrinkles ( ) is a 2011 Spanish animated drama film directed by Ignacio Ferreras, based on the comic book with the same title by Paco Roca. The story is set in a retirement home and revolves around the friendship between two elderly men, one of them in the early stages of Alzheimers disease.

Wrinkles was released to UK DVD and Blu-ray on April 28, 2014, following a limited theatrical release on April 18. Special features in this release include Wrinkles Animatic, Wrinkles Making Of, Peter Bradshaw reviews Wrinkles, Wrinkles Trailer, Wrinkles Teaser Trailer, and Recording the Music for Wrinkles.

==Cast==

* Álvaro Guevara as Emilio
* Tacho González as Miguel
* Mabel Rivera as Antonia

==Production==
The film was produced through Perro Verde Films and co-produced by Cromosoma. The budget was two million euros. 75% of the animation work was done in Spain; the rest was outsourced to the Philippines. 

==Reception== The Illusionist and he carries the flame forward here with the moving cel animation Wrinkles (Arrugas), easily one of the better films to emerge from San Sebastian this year." Halligan praised the characterisations of the two main characters and their relationship and wrote: "Some of the storys other aspects are more broadly sketched, however, and they could occasionally be accused of labelling out the pathos too liberally ... There are nicely-judged moments of humour, however, and Wrinkles restrains itself in a most dignified manner when it comes to the inevitable, but tender, denouement." 

===Accolades===
{| class="wikitable"
|-
! Award
! Category
! Recipient(s)
! Result
|- Annie Awards  Best Animated Feature
| 
|  
|- 26th Goya Goya Awards  Best Adapted Screenplay
| Ángel de la Cruz, Ignacio Ferreras, Paco Roca, Rosanna Cecchini 
|  
|- Best Animated Film
| 
|  
|- European Film Awards  Best Animated Feature Film
| 
|  
|-
| Festival of European Animated Feature Films and TV Specials 
| Kecskemét City Award
| Ignacio Ferreras
|  
|-
|}

==See also==
* 2011 in film
* Cinema of Spain
* List of animated feature films

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 