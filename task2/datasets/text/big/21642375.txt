Dheewarayo
{{Infobox Film | name = Dheewarayo
 | image = 
 | imagesize = 
 | caption =
 | director = M. Masthan
 | producer = K. Gunaratnam
 | Screenplay = K. Hugo Fernando
 | starring = Gamini Fonseka, Sandhya Kumari, Anthony C. Perera
 | music = Karunaratne Abeysekera (lyrics) M. K. Rocksamy (music)
 | cinematography = Willie Blake
 | editing = Titus Thotawatte
 | distributor = 
 | country    = Sri Lanka
 | released =   April 10, 1964 
| runtime = Sinhala
 | budget = 
 | followed_by =
}}

Dheewarayo is a 1964 Sri Lankan film starring Gamini Fonseka. It was a box office success in the country.

==Plot==
Story about fishing folk.

Cast

*Gamini Fonseka &ndash; Francis
*Sandhya Kumari;Roseline
*Anthony C. Perera
*Ignatius Gunaratne
*Hugo Fernando

==Songs==
*"Sathuta Soke" &ndash; Latha Walpola
*"Samaye Samagiye Pathurala" &ndash; Mohideen Baig, Mallika Kahawita, Christy Leonard Perera and chorus
*"Nilata Nile Wihide Yayi" &ndash; Latha and Dharmadasa Walpola
*"Awilla Awilla" &ndash; Latha Walpola, Mohideen Baig, Christy Leonard Perera and chorus
*"Walle Sibina Ralle" &ndash; Mohideen Baig, Mallika Kahawita, Christy Leonard Perera and chorus
*"Agada Sagaraye" &ndash;J. A. Milton Perera
*"Ra Bowi Bowi Yayi" &ndash; J. A. Milton Perera, Haroon Lanthra and Noel Guneratne
*"Maligawe Ma Rajina" &ndash; Sujatha Perera

==External links==
* 
*  

 
 


 