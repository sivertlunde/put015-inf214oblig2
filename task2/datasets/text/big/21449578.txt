Hamsun (film)
{{Infobox film
| name           = Hamsun
| image          = Hamsun poster.jpg
| image_size     =
| caption        =
| director       = Jan Troell
| producer       = Erik Crone
| writer         = Screenplay:   Marie Hamsun (autobiography)
| narrator       =
| starring       = Max von Sydow Ghita Nørby
| music          =
| cinematography = Jan Troell Mischa Gavrjusjov
| editing        =
| distributor    =
| released       = Norway: 19 April 1996 Denmark: 26 April 1996 Sweden: 26 April 1996 United States: 6 August 1997
| runtime        = 159 min.
| country        = Denmark Sweden Norway Germany Swedish Danish Danish Norwegian Norwegian German German
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}
 German Drama drama directed occupation of Norway during World War II.
 Swedish and Danish respectively, Norwegian or German language|German.
 Gulbagge Awards Best Film, Best Actor Best Actress Best Screenplay (Per Olov Enquist|Enquist).

==Selected cast==
* Max von Sydow as Knut Hamsun
* Ghita Nørby as Marie Hamsun
* Anette Hoff as Ellinor Hamsun
* Gard B. Eidsvold as Arild Hamsun
* Eindride Eidsvold as Tore Hamsun
* Åsa Söderling as Cecilia Hamsun
* Sverre Anker Ousdal as Vidkun Quisling
* Erik Hivju as Dr. Gabriel Langfeldt
* Edgar Selge as Josef Terboven
* Ernst Jacobi as Adolf Hitler
* Svein Erik Brodal as Holmboe
* Per Jansen as Harald Grieg
* Jesper Christensen as Otto Dietrich
* Johannes Joner as Finn Christensen
* Finn Schau - Doctor

==Production== NRK dropped out on the project, believing it would be too controversial.

Fourteen years later, in 1993, von Sydow brought the project back to life when he got the Danish production company  , providing the screenplay.

The shooting took place during the spring and summer 1995, with a budget of around 40 million   

==Release==
The film was initially meant to be released in the autumn 1996 at the Venice Film Festival, but was brought forward to the spring as Norwegian television would release another film about Hamsun the same year. It also saved the film from having to compete against Bille Augusts historical epic Jerusalem (1996 film)|Jerusalem.
 Svensk Filmindustri, for the sloppy handling of the film, something he had also experienced with his previous film Il Capitano. 

==See also== Notable film portrayals of Nobel laureates

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 