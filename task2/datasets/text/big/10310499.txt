Golgotha (1935 film)
{{Infobox Film
| name           = Golgotha
| image          = Golgotha1935.jpg
| caption        = 
| director       = Julien Duvivier
| producer       =  
| writer         = Julien Duvivier
| starring       = 
| music          = Jacques Ibert 
| cinematography = Jules Kruger
| editing        = Marthe Poncin 
| distributor    = 
| released       = 1935 (France) 1937 (US)
| runtime        = 95 minutes
| country        = France
| language       = French
| budget         = 
}}
Golgotha is a 1935 French film about the death of Jesus Christ, released in English-speaking countries as Behold the Man. The film was directed by Julien Duvivier, and stars Harry Baur as Herod, Jean Gabin as Pontius Pilate, and Robert Le Vigan plays Christ. 
 British Board of Film Censors "would not allow British eyes to see it."  
 Quo Vadis, The Robe, but there are also a few closer shots and even close-ups.
 National Board of Review named the film the sixth best foreign film of 1937. The score for the movie was composed by French composer Jacques Ibert.

==Cast==
*Harry Baur ...  Herod Antipas 
*Jean Gabin ...  Pontius Pilate 
*Robert Le Vigan ...  Jesus Christ 
*Charles Granval ...  Caiaphas
*André Bacqué ...  Annas (as Andre Bacque) 
*Hubert Prélier ...  Peter (as Hubert Prelier) 
*Lucas Gridoux ...  Judas 
*Edmond Van Daële ...  Gerson (as Van Daele) 
*Edwige Feuillère ...  Claudia Procula 
*Juliette Verneuil...  Mary
*Marcel Chabrier ...  Joseph of Arimathea (as Chabrier) 
*Georges Saillard ...  Un Sanhédrite (as Saillard) 
*Marcel Carpentier ...  Le scribe 
*Victor Vina ...  Un Sanhédrite 
*François Viguier ...  Un Sanhédrite (as Viguier)

==References==
  

== External links ==
* 

 

 
 
 
 
 
 

 
 