An Ordinary Miracle (1964 film)
{{Infobox film
| name           = An Ordinary Miracle
| image          = 
| image_size     = 
| caption        = 
| director       = Erast Garin Khesya Lokshina
| producer       = 
| writer         = Erast Garin Khesya Lokshina Yevgeni Shvarts  (play)
| narrator       = 
| starring       = Oleg Vidov Georgi Georgiu Erast Garin
| music          = Vladimir Tchaikovsky L. Rapoport
| cinematography = Viktor Grishin
| editing        = 
| distributor    =
| studio         = Gorky Film Studio
| released       =  
| runtime        = 94 minutes
| country        = Soviet Union
| language       = Russian
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} Soviet 1964 romantic fantasy film, directed by Erast Garin and based on a play by Yevgeni Shvarts.
 Vorontsov Palace.

== Cast ==
* Aleksei Konsovsky
* Nina Zorskaya	
* Oleg Vidov as The Bear
* Erast Garin as The King
* Nelli Maksimova as The Princess
* Georgi Georgiu as Minister Administrator
* Aleksei Dobronravov
* Valentina Karavayeva
* Viktor Avdyushko
* Klavdiya Lepanova
* Svetlana Konovalova
* Yevgeni Vesnik as The Hunter
* Georgi Millyar

==See also==
* An Ordinary Miracle (1978 film) - another adaptation of the same play.

== External links ==
* 
* 

 
 
 
 
 
 
 
 
 
 


 
 