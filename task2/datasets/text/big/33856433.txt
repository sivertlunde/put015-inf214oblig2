Run, Run, Joe!
{{Infobox film
 | name =Run, Run, Joe!
 | image =  Run, Run, Joe!.jpg
 | caption =
 | director =  Giuseppe Colizzi
 | writer =
 | starring =  Keith Carradine Tom Skerritt
 | music =   Guido & Maurizio De Angelis 
 | cinematography =   Marcello Masciocchi 
 | editing =  Antonio Siciliano
 | producer =   Carlo Ponti
 | distributor =
 | released = September 1974
 | runtime =
 | awards =
 | country = Italy
 | language = English
 | budget =
 }} Italian action-comedy movie directed in 1974 by Giuseppe Colizzi. It is the penultimate movie of Colizzi, before the posthumously released Switch.  The movie reprises the style of the  movies of the popular duo Bud Spencer-Terence Hill, that the same Colizzi had launched in 1967 in God Forgives... I Dont!.     It was co-produced with West Germany (where it is known as J & M - Dynamit in der Schnauze and Dufte Typen räumen auf), Spain (where it was released as Joe y Margherito) and France.

== Plot ==
Joe is assigned to protect Don Salavatore who is about to leave Italy for the United States. Don Salavatore has a number of enemies but Joe is supported by a new friend, the courageous mariner Margherito. But even the efforts of both of them are eventually not sufficient to keep Don Salavatore alive. Since the mafia considers Joe accountable, the new friends have to seek cover. They start to pretend they were English.

== Cast ==
*Keith Carradine as Joe
*Tom Skerritt as  Margherito
* Cyril Cusack as  Parkintosh
*Sybil Danning as  Betty
*José Calvo as  Don Salvatore
* Raymond Bussières as  Don Sulpicone
* Marcello Mandò as  Sapicone

==References==
 

==External links==
* 

 


 
 
 
 
 
 
 

 