Reform School Girl
 
{{Infobox film
| name           = Reform School Girl
| image          = Reform School Girl Movie Poster.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Edward Bernds
| executive producer = James H. Nicholson
| producer       = Samuel Z. Arkoff
| writer         = Edward Bernds
| starring       = Gloria Castillo Ross Ford Edward Byrnes Ralph Reed Yvette Vickers Jan Englund
| music          = Ronald Stein
| editing        = Richard C. Meyer
| distributor    = American International Pictures
| released       =  
| runtime        = 71 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

Reform School Girl is a 1957 film starring Gloria Castillo as a teenage girl who is sent to a reformatory.    The film was directed by Edward Bernds and was produced by Samuel Z. Arkoff.  Reform School Girl was one of many sexploitation films released by American International Pictures (AIP) during the 1950s and 1960s.  AIP’s films during this period were largely focused on juvenile delinquency.  As part of the AIP formula, young girls were typically depicted as “good” or “bad” the latter of which were petty criminals, gang members, or the girlfriends of gang members.  Other AIP films in the same genre included Drag Strip Girl, Hot Rod Girl, and High School Hellcats.   Reform School Girl is available for on-line viewing at AMCtv.com.  
The film was released by American International Pictures as a double feature with Shake, Rattle & Rock! (1956 film)|Shake, Rattle and Rock.

==Plot==
Donna Price (Gloria Castillo) is picked up on a double date by Vince (Edward Byrnes).  During the course of the evening she discovers that the car they are riding in is stolen.  As the night unfolds, an argument ensues in the car and Vince tells the others to get out, leaving Donna as his only passenger.  When she asks him if he is worried about the other couple notifying the police, Vince tells her that he would murder anybody who ever turned on him.  Subsequently, the police attempt to pull Vince over for speeding.  During the high speed chase, Vince strikes a pedestrian and kills him.  He flees the scene leaving Donna alone in the car.  Fearing for her life, she refuses to identify Vince as the car’s driver.  Considered a juvenile delinquent, Donna is sent to a state school for girls.  

At the school, Donna attends a history class that is taught by David Lindsay (Ross Ford) who is also the school’s psychologist.  Although Donna does not reveal her secret to David, he believes that she is a good person who may have been caught up in a series of bad events and that she can straighten out her life with his help.  

While Donna is in the school, the police continue the accident investigation.  Vince is troubled by the event and is worried that Donna will eventually tell authorities what happened.  He develops a ruse where his girlfriend places a phone call to the police and identifies herself as Donna.  She tells the police that one of the girls in the reform school is a thief.  Eventually the other girls come to believe that Donna is a police informer. She is attacked by Roxy (Yvette Vickers) and while defending herself, Donna cuts Roxy with a pair of scissors.  Despite David’s plea to conduct an investigation, the school superintendent determines that Donna is a high risk student and that she should be transferred to the state prison.  School authorities also question David’s judgment, his relationship with Donna, and his ability to help students.  Vince, who is still convinced that Donna will turn him into the police, attempts to break into the school and kill her.  He fails and is apprehended.  The police and school authorities investigate the incident and as a result of the investigation David is vindicated, and Donna is exonerated and released.  

The film marked the acting debut of Sally Kellerman.  While filming the movie, she met Vickers and Byrnes and the three of them became lifelong friends. 

==Reviews==
The entertainment site Rotten Tomatoes gives the movie a "0%" audience rating and provides the following narrative:

  "The inherent trashiness of Reform School Girl is redeemed by the sincere performance of Gloria Castillo and the matter-of-fact direction of Edward Bernds. Castillo plays mixed-up teenager Donna Price, who is shipped off to a girls reformatory when she is involved in a fatal car crash. Actually, Donna is innocent, but she refuses to reveal who was driving. Only when the culprit (a pre-77 Sunset Strip Edward Byrnes) reveals himself to be a total piece of excrement is Donna able to extricate herself from her dilemma."  

Movie critic Leonard Maltin has characterized the movie as a “bomb.”
 "Hokey time capsule about a pretty teen from the “other side of the tracks” (Castillo). After a fatal car mishap she ends up in reform school.  Will she or won’t she squeal on the party responsible for her dilemma?"  
Since the film’s release in 1957, the theatrical movie poster, featuring blond haired Vickers fighting dark haired Castillo, has become a collector’s item. “… the two girls wrestling for a pair of scissors promise anything that a teenage male, from any decade, could want.”   The poster has also become popular on the internet since it was used as part of an advertisement for a 2006 Scrabble tournament held in France.  The posters slogan says, "In Scrabble anything is possible. Grand National Tournament September 23, 2006."  

==In popular culture==
An April 21st, 1977 episode of SCTV (Season 1, Episode 11) contains a satirical skit called Broads Behind Bars which has half of the story of the movie, but mostly contains a subplot about the smoking of marijuana making one do criminal things. The skit is featured on the DVD SCTV: Best Of The Early Years. 

==References==
 

==External links==
*  


 
 
 
 
 
 
 