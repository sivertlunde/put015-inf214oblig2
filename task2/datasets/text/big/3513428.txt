House of Fools (film)
{{Infobox Film name = House of Fools image = Dom_durakov_video_title.jpg caption  =  director  = Andrei Konchalovsky producer = Felix Kleiman Andrei Konchalovsky writer = Andrei Konchalovsky starring = Julia Visotskaya Sultan Islamov Yevgeni Mironov distributor = Paramount Vantage released = 2002 runtime = 104 minutes cinematography = Sergei Kozlov country = Russia language = Chechen
|music = Eduard Artemyev budget = 
}}
 psychiatric patients and combatants during the First Chechen War. It stars Julia Vysotskaya and Sultan Islamov and features a number of cameo appearances by Bryan Adams, with the music composed by Eduard Artemyev.

Distinctly anti-war, unbiased and controversial in Russia, House of Fools is a bizarre blend of black comedy, touching drama, horrific warfare and subtly disturbing psychological content. 

==Storyline==
The film tells the story of a psychiatric hospital in the Russian republic of Ingushetia on the border with war-torn republic of Chechnya in 1996. With the medical staff vanishing to apparently find help, the patients are left to their own endeavors.  Zhanna (Yuliya Vysotskaya), a young woman, lives in the belief that the pop star Bryan Adams is her fiancé, that he is off on tour and will, at some point in the future, come to take her away with him. Zhanna is sort of the ad hoc keeper of peace, happiness and control of the others; she attempts to help curb some of the other patients exuberant impulses. Blissfully unaware of the terror of the war, the patients stick it out in the hospital. Their guests include a group of Chechen rebels, one of whom, Ahmed (Sultan Islamov), gives Zhanna the idea that he will marry her. At this point Zhanna falls in love with Ahmed.  She goes back to the "House" where, with the help of her fellow residents, she prepares for her marriage to Ahmed. From this point on Zhanna prepares for and expects to be swept away by Ahmed. Her hopes do not come to fruition and Ahmed and Zhanna part ways.  Zhanna returns to the "House" in order to resume her life there.

The story was partially inspired by the real-life tragedy of the psychiatric hospital in Shali, Chechen Republic|Shali, Chechnya, which was abandoned by the personnel during the Russian bombing campaign and in which many patients subsequently died from attacks and neglect. 
 King of Hearts (Le Roi de coeur, starring Alan Bates) about the inmates of an asylum abandoned by the staff during World War I who take over the neighboring town.  The two films even share similarities in their conclusion, with a soldier taking refuge from the insanity of war in the asylum when it returns to normal.  Although there are some similar aspects to King Of Hearts, the difference in the two films is that the inmates/patients in King of Hearts take on the various personalities of the town folk; mayor, baker, prostitute, etc.

==Selected cast==
*Julia Vysotskaya - Zhanna
*Sultan Islamov - Ahmed Yevgeni Mironov - Russian officer
*Stanislav Varkki - Ali
*Bryan Adams - Himself Lithuanian sniper
*Pavel Grachev - Himself (archival footage)

The film also features several genuine mental patients alongside actors.

==Reception==

===Awards===
Awards: Grand Special Jury Prize UNICEF Award
*Bergen International Film Festival - Jury Award (Honorable Mention)

Nominations:
*Venice Film Festival - Golden Lion Best Foreign Language Film (representing Russia)
*Nika Awards - Best Music

===Ratings===
House of Fools received a rating of 52/100 at Metacritic  and 40% "fresh" rating at Rotten Tomatoes.  Chicago Sun-Times film critic Roger Ebert gave the film three out of four stars, saying that House of Fools is "A film that succeeds not by arguing that the world is crazier than the asylum, but by arriving at the melancholy possibility that both are equally insane." 

==References==
 

==External links==
* 
* 
*  at Variety (magazine)|Variety

 

 
 
 
 
 
 
 
 
 
 
 