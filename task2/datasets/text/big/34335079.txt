The Berlin Government District Part 1: 1932-1938
{{Infobox film
| name           = The Berlin Government District Part 1: 1932-1938
| director       = Christoph Neubauer
| starring       = Ulrike Krumbiegel: Speaker Joachim Schönfeld: Speaker
| music          = Lorraine Shannon Robert Schöder
| studio         = 25fps-filmproduction GmbH & Co. KG 
| released       =  
| runtime        = 70 minutes
| country        = Germany
| language       = English, Afrikaans
}}

The Berlin Government District is the first part of a trilogy, which deals with the historical Berlin government district between 1932 and 1945,  and its destruction after the Second World War. The movie is completely 3D animated and was produced in South Africa.  Preview was in Berlin (Germany) and Frankfurt (Oder) on 29 September 2005.

==Plot==
This documentary is the first part of the three-dimensional computer-animated film trilogy. It gives a detailed overview about the architecture and building measures at the historical Berlin government district between 1932 and 1945. Through showmanships and cuts there are shown structural alterations and spatial relation which is not possible with a classic photograph-show.

The following streets are shown in the movie:
* Vossstrasse
* Wilhelmstrasse
* Wilhelmplatz

Additionally, the following buildings and places are specially mentioned:
* Reich Chancellery
* Palais Borsig
* Hotel Kaiserhof
* Ministry of Public Enlightenment and Propaganda
* Mohrenstrasse (Berlin U-Bahn)

==Soundtrack==
The soundtrack of the film was composed and played by Lorraine Shannon and Robert Sch√∂der. Both musicians are members of SAMRO, the Southern African Music Rights Organisation. Primary the music is pianomusic accompanied by many strings.

==Critical reception==
The DVD was reviewed in several German newspapers:

* "Every stucco and windowsill were originally reconstructed, even the street-lamps and a advertising column with posters."—Berliner Kurier (29 May 2005)

* "... nothing is more spectacular than this kind of view the movie shows. Not only for historians it is a useful work. Everybody comes to rhapsodize because of the gigantic, rich decorated, monumental government buildings out of the 19th century."—PSM

==Home media==
The Berlin Government District was released in Region 0 territories on September 26, 2005.

==References==
{{reflist|refs=
   
   
}}

==External links==
*  
*  

 
 
 