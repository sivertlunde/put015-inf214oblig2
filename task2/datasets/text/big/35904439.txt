The Child and the Pope
{{multiple issues|
 
 
 
}}

{{Infobox film
| name           = The Child and the Pope
| director       = Rodrigo Castaño
| starring       = Carmenza Duque Cristopher Lago 
                   Verónica Castro Andres Garcia
| country        = Colombia Mexico
| runtime        = 96 minutes
}} 1987 Colombian film which tells the story of a nine-year-old Mexican boy named Angel who wanted to meet the Pope after he was a victim of the 1985 Mexico City earthquake.

The film was directed by Rodrigo Castaño and played role by Christopher Lago and Verónica Castro. In 1987 it was released after Pope John Paul IIs visit to Colombia.

==Plot==
In a humble neighborhood of Mexico City, a small boy named Angel lived with his mother who was a homemaker and worked as a seamstress, She really dreams that her son to study and someday hell receive bachelors degree. When Angel was going to school, a strong earthquake struck the city leaving thousands of people injured (or worse), including Angel and his mother.

Angel was desperate to find his mother alive, he decides to walk a few steps until to arrive to the Basilica of our lady of Guadalupe with the intention of praying for his missing mother, At the same time she is helped by Carlos a hospital doctor. While the child leaves the church, he saw a big statue of Pope John Paul II and a priest tells him that the next visit of saint father will be to Colombia.

One hour later, he went forward onto the registration of missing people, and he realized that his mothers name would not appear among those found persons and he should be sent to an orphanage, hearing these words, he decided to flee until he arrived curiously to the airport, where he thought that it was a good idea to take the next flight bound to Colombia with the aim to meet the Pope.

Arriving in Colombia, some flight attendants found him hidden in a bathroom of the plane and handed over to a woman to take charge, but suddenly the child escaped.  The next day, Angel went to a food stand and he is seen by Carmen who worked as a saleswoman at that place, when she offers him a potato, the child steals a piece of "black pudding" and he is caught immediately by the business owner, when Carmen realized he was attacking Angel, she intervened on behalf of the child and eventually quit her job.

Thereafter, Angel apologizes for bad behavior, until she decides to forgive him, then explains what happened with the earthquake and his mother and insists to stay in their home, she finally believes him and agrees to give shelter. On the other hand, the doctor realizes that Alicia suffers from amnesia, because she doesnt remember anything about her past life and she had forgotten her own name, and the doctor decides to call her "Guadalupe".
 
Carmen knew the childs wish is to know the pope personally, and she accompanies him to the Bolivars square and tells some of the places where he should go to get information regarding the Pope.

After unsuccessful attempts to establish contact with the Pope and in the midst of a difficult situation with Carmen unemployed, Angel read the newspaper and he noticed that they were looking for a performer to sing at the reception of the Pope. Angel realized that Carmen has a great aptitude for singing, get to convince her to answering this call. On the other hand, Alicia remembers little about her past and invites Carlos to meet his mother, who cordially received at home. Meanwhile, Angel decides to ask for help from "Mr Fulgencio" to compose a song to the Pope, being precisely the singer Carmen. However, Carmen, was not entered formally in the competition, they were rejected. Despite the adversities, the father of the parish in which the contestants were presented, was that Carmen had a great talent and felt the song as the best of all, so that gave him the opportunity to perform. Time passed and Carmen did not receive the invitation to present that act, because the contest was rigged in favor of another singer. However, Angel decided to investigate and discovered that everything had been controlled, is with the Father and then apologizes for not coming, so does perform alongside Carmen "Mr Fulgencio" and is chosen to sing the July 2, the day the pope arrives. When Carmen was ready to sing before thousands of people in the pavilion of the Tunal park, the child is prevented from entering, since only the singer could be near the Pope at the time of the tribute. But ultimately, the child manages and went up to the pavilion and he is received by the Pope, who gives him a kiss on the forehead and blessed him.
Carloss mother turns on the TV just in time in transmitting the Popes visit to Colombia, so that Alicia sees his son and she began to remember everything that happened at that moment she asks for help to Carlos, who manages communicate with the singer. The film ends when Angel, eager after all this time, greets his mother by phone.

 
 
 
 