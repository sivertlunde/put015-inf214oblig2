Pink Floyd: Live at Pompeii
 
 
 
{{Infobox film name = Pink Floyd: Live at Pompeii image = Pink-floyd-poster.jpg  caption = Film poster director = Adrian Maben producer =   writer =  starring = Pink Floyd music = Pink Floyd cinematography =  Willy Kurant   Gábor Pogány   editing = Jose Pinheiro distributor = Universal Home Video released =   runtime = 60 min. (1972) 80 min. (1974) 92 min. (directors cut) language = English country = France Belgium West Germany  budget =
}} concert documentary|documentary ancient Roman amphitheatre in Pompeii, Italy, directed by Adrian Maben. Although the band are playing a typical live set from this point in their career, the film is notable for having no audience.

The main footage in and around the amphitheatre was filmed over four days in October 1971, using the bands regular touring equipment, including studio-quality 24-track recorders. Additional footage filmed in a Paris television studio that December made up the original 1972 release. The film was then re-released in 1974 with additional studio material of the band working on The Dark Side of the Moon, and interviews at Abbey Road Studios.

The film has subsequently been released on video numerous times, and in 2003 a "Directors Cut" DVD appeared which combines the original footage from 1971 with more contemporary shots of space and the area around Pompeii, assembled by Maben. A number of notable bands have taken inspiration from the film in creating their own videos, or filming concerts without an audience.

==Background==
  KQED TV studios in April 1970.  Adrian Maben had become interested in combining art with Pink Floyds music,  and during 1971, he attempted to contact the bands manager, Steve ORourke, to discuss the possibilities of making a film to achieve this aim. After his original plan of mixing the band with assorted paintings had been rejected, Maben went on holiday to Naples in the early summer.
 Gimme Shelter, University of Naples, Maben managed to persuade the local authorities to close the amphitheatre for six days that October for filming.   
 

==Filming==

===Pompeii===
  A Saucerful One of These Days" were filmed from 4 to 7 October 1971.  ORourke delivered a demo to Maben in order for him to prepare for the various shots required, which he finally managed to do the night before filming started. The choice of material was primarily the bands, but while Maben realised it was important to include material from Meddle, he was also keen to include "Careful with That Axe, Eugene"  and "A Saucerful of Secrets", as he felt they would be good numbers to film.  "Intro Song" was an early version of "Speak to Me" from The Dark Side of the Moon.

The band insisted on playing live, and brought their regular touring gear with them. Their roadie, Peter Watts, suggested that their 24-track recorder would produce a sound tantamount to that of a studio recording. In addition, it transpired that the natural echo of the amphitheatre also provided good acoustics for the recording.  The equipment was carried by truck from London, and took three days to reach Pompeii. When it arrived, it was discovered there was insufficient power to drive the equipment correctly, which blew every time it was plugged in. This issue plagued filming for several days, and was finally resolved by running a lengthy cable from the local town hall. 

The first section of footage to be filmed were montage shots of the band walking around Boscoreale, mixed with shots of mud,  which can be seen at various points in "Echoes" and "Careful with That Axe, Eugene". For the live performances, the band recorded portions of the songs in sections, which were later spliced together. After each take, they listened to the playback on headphones. Maben closed all the entrances to the amphitheatre, but a few children managed to sneak in, and were allowed to watch the filming quietly from a distance. 

===Paris===
  is featured prominently on several songs in the film, particularly Set the Controls&nbsp;... and Echoes Part II.]] Richard Wrights front projection footage for insertion into the Pompeii performances. While both the director and the band were disappointed with this footage, due to a lack of time and money, there was no alternative left but to use it. {{cite video
  | people = Adrian Maben
  | title = Pink Floyd: Live at Pompeii: Directors Cut
  | medium = DVD
  | publisher = Universal Music & Video Distribution
  | location = 
  | date = 2003}} 
 Seamus from Meddle. Maben knew Madonna Bouglione, daughter of circus director Joseph Bouglione, who was known to walk about Paris with a dog called Nobs. Accordingly, Nobs was invited into the studio, where the footage was filmed. 

Maben subsequently did some of the editing of the final cut at home, due to financial constraints. He regretted doing this, as he felt it was important to separate his work and home life, but, at the time, he had no choice. 
 

==Release history==
 
 
 British Board of Film Censors and the theatre could be seen to be in competition with established cinemas. 

Maben was concerned that one of the problems with the film was that it was too short.  In early 1973, Maben was fly fishing with Waters, and suggested the possibility of improving the film by watching them at work in a recording studio. Subsequently, Maben was invited with a small crew using a single 35&nbsp;mm camera to Abbey Road Studios to film supposed recording sessions of The Dark Side of the Moon, as well as interviews conducted off-camera by Maben, and footage of the band eating and talking at the studio cafeteria. Maben was particularly happy about this footage, feeling it captured the spontaneous humour of the band.  This version premiered on 21 August 1974, and ran for 80 minutes.  The recording sessions were actually staged for the film,   as the recording of the album had been completed when these sessions were filmed in January 1973 and the band was mixing the album at the time.

The film wasnt financially successful according to Mason,  though Maben disagrees,  and suffered particularly from being overshadowed by the release of The Dark Side of the Moon  not long after the original theatrical showing.  It was released on various home video formats several times. 

In 2003, the directors cut version of the film was released, running for 92 minutes. In addition to the concert and interview footage, it includes more overlaid imagery including footage from the Apollo space program and computer-generated images of space and Pompeii, and overall has a busier, "updated" feel. The original 1.37:1 aspect footage is cropped to 1.78:1 in this version. The DVD also contains the original one-hour cut in its original aspect ratio as a bonus, a collection of black-and-white footage of the band in Paris filmed by Maben in 1972, and an extensive interview with the director.

==Reception==
  in Set The Controls, such as this young couple. (Portrait of Paquius Proculo)]]
Maben was particularly pleased with positive reviews that came out of the films showing at the Edinburgh International Film Festival, but was disappointed to hear one New York critic describe it "like the size of an ant crawling around the great treasures of Pompeii."   

Billboard (magazine)|Billboard magazine was not enthusiastic about the 1974 re-release, thinking it looked dated, and stated that the film was "dull, unimaginative and hokey, and does not do justice to the Pink Floyd Vision".  However, more recent reviews have been favourable. Billboard reviewed a video release in 1984, and on this occasion, Faye Zuckerman, while not particularly keen on the footage in the Abbey Road canteen, stated it was "vastly superior to most other concert movies".  Reviewing the Directors Cut DVD, Richie Unterberger said the film had "first-rate cinematography" and was "undeniably impressive",  while Peter Marsh, reviewing for the BBC, stated it was "my favourite concert film of all time", though his opinions of the new computer generated imagery were mixed. 

==Outtakes==
Due to the lack of time in filming, no tracks were filmed that were unreleased, but several alternative shots and outtakes were held in the Archives du Film du Bois DArcy near Paris. At some point, an employee of the owners, MHF Productions, decided this footage was of no value and incinerated all 548 cans of the original 35&nbsp;mm Original camera negative|negatives.   Maben was particularly frustrated about the lack of additional shots for "One of These Days," which is primarily a Mason solo-piece in the released version.  Mason recalls the reason for that is that the reel of film featuring the other members was lost attempting to assemble the original cut. 

==Track listing==

===1972 original film===
#"Intro Song"
#"Echoes (Pink Floyd song)|Echoes, Part 1" (from Meddle, 1971)
#"Careful with That Axe, Eugene" (from Point Me At The Sky, B-side, 1968) A Saucerful of Secrets" (from A Saucerful of Secrets, 1968) One of These Days Im Going to Cut You into Little Pieces" (from Meddle, 1971)
#"Set the Controls for the Heart of the Sun" (from A Saucerful of Secrets, 1968) Mademoiselle Nobs" (from Meddle, 1971)
#"Echoes, Part 2" (from Meddle, 1971)

===1974 theatrical version===
Featured on VHS, CED Selectavision and Laserdisc releases.

#"Intro Song"
#"Echoes, Part 1" (from Meddle, 1971) On the Run" (studio footage) (from The Dark Side of the Moon, 1973)
#"Careful with That Axe, Eugene" (from Point Me At The Sky, B-side, 1968)
#"A Saucerful of Secrets" (from A Saucerful of Secrets, 1968) Us and Them" (studio footage) (from The Dark Side of the Moon, 1973)
#"One of These Days" (from Meddle, 1971)
#"Set the Controls for the Heart of the Sun" (from A Saucerful of Secrets, 1968) Brain Damage" (studio footage) (from The Dark Side of the Moon, 1973) Mademoiselle Nobs" (from Meddle, 1971)
#"Echoes, Part 2" (from Meddle, 1971)

===2003 directors cut===
#"Echoes, Part 1"/"On the Run" (studio footage) (uncredited) (from Meddle/The Dark Side of the Moon, 1971/1973)
#"Careful with That Axe, Eugene" (from Point Me At The Sky, B-side, 1968)
#"A Saucerful of Secrets" (from A Saucerful of Secrets, 1968)
#"Us and Them" (studio footage) (from The Dark Side of the Moon, 1973)
#"One of These Days" (from Meddle, 1971)
#"Mademoiselle Nobs" (from Meddle, 1971)
#"Brain Damage" (studio footage) (from The Dark Side of the Moon, 1973)
#"Set the Controls for the Heart of the Sun" (from A Saucerful of Secrets, 1968)
#"Echoes, Part 2" (from Meddle, 1971)

Also Known as:
*Echoes: Pink Floyd (US)
*Pink Floyd in Pompeii (Belgium)

==Credits==
;Pink Floyd vocals on "Echoes", "Careful with That Axe, Eugene" and "A Saucerful of Secrets", additional vocals on "Set the Controls for the Heart of the Sun" keyboards on "Echoes" Farfisa Compact Duo organ, piano, vocals on "Echoes"
* , Percussion instrument|percussion, vocal phrase on "One of These Days Im Going to Cut You into Little Pieces"

Based on an idea and Directed by Adrian Maben

* Cinematography: Willy Kurant, Gabor Pogany
* Camera: Claude Agostini, Jaques Boumendil, Henri Czap, Gérard Hameline Peter Watts
* Script: Marie-Noel Zurstrassen
* Road Managers: Chris Adamson, Robert Richardson, Brian Scott
* Production Directors: Marc Laurore, Leonardo Pescarolo, Hans Thorner
* Editor: José Pinheiro
* Assistant Editor: Marie-Claire Perret
* Mixer: Paul Berthault
* Special Effects: Michel Francois, Michel Y Gouf
* Post Production: Auditel, Eclair, Europasonor
* Special thanks to : Professor Carputi (University of Naples), Haroun Tazieff, Soprintendenza alle Antichità della Provincia di Napoli
* Associate Producers: Michele Arnaud, Reiner Moritz
* Executive Producer: Steve ORourke

==Legacy==
The hip hop group the Beastie Boys made a music video for their song "Gratitude (song)|Gratitude" that appears to be a homage to the film. Shot by David Perez in New Zealand,  in addition to copying its directorial style of slow horizontal tracking shots, overhead shots of the drums, close up shots of the bass and multiple shots of guitar filling the screen, the video shows a number of speaker cabinets that the group managed to purchase, still labelled "Pink Floyd, London". The video ends with a message that reads, "This video is dedicated to the memory of all the people who died at  Pompeii#Eruption of Vesuvius|Pompeii."  The Beastie Boys claimed in interviews that the song and the video came about due to their desire to progress from being a straightforward hip hop group and add vintage instruments to their repertoire.   

The rock band  , in June 2010 to promote their ninth studio album,  . The show took place in a crop circle in Bakersfield, California, and had no audience beyond the crew workers. 
 Jonny made the whole band watch the film, saying "now this is how we should do videos," Colin, however was critical of the direction, which he described as "Dave Gilmour sitting on his arse playing guitar and Roger Waters with long greasy hair, sandals and dusty flares, staggers over and picks up this big beater and whacks this gong. Ridiculous." 

The Pink Floyd tribute band The Pink Tones performed "Atom Heart Mother" at the empty roman amphiteatre of Segobriga (Spain) on April 2014 with an orchestra and a chorale and filmed it, as an attempt to homage and complement the "Pink Floyd at Pompeii" film with a different song. 

==References==
{{reflist|2|refs=
   
   
   
   }}

==Sources==
 
*  
*  
*  
*  
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 