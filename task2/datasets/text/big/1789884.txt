The Best of Youth
{{Infobox film
| name           = The Best of Youth (La meglio gioventù)
| image          = BestofYouth.jpg
| caption        = Original Italian film poster
| director       = Marco Tullio Giordana
| producer       = Angelo Barbagallo
| writer         = Sandro Petraglia Stefano Rulli
| starring       = Alessio Boni Luigi Lo Cascio Jasmine Trinca Adriana Asti Sonia Bergamasco Fabrizio Gifuni Maya Sansa Lidia Vitale
| music          = 
| cinematography =  Roberto Forza
| editing        = 
| studio         = 
| distributor    = Miramax
| released       = Italy: June 22, 2003 United States: 2005
| runtime        = 354 minutes (theatrical version, two parts) / 400 (TV version, four episodes)
| country        = Italy
| language       = Italian
| budget         = 
| gross          = $2,693,053 
}}
 2003 Cinema Italian film directed by Marco Tullio Giordana. Originally planned as a four-part mini-series, it was presented at the 2003 Cannes Film Festival where it won the Un Certain Regard award.    It was then given a theatrical release in Italy in two three-hour parts in which 40 minutes were edited out. The complete version was aired in Italy from December 7 to 15, 2003 on Rai Uno in four parts.

In the U. S., the film was screened in several cities in two three-hour parts. The two-disc DVD of the film is similarly divided.
 The Leopard. 

==Plot== parenthood and retirement in the early 2000s. The film aims to show the interaction of the personal and the political, the ways in which small events may become turning points in the important choices made by individuals.

===1966 summer===
Two brothers go their separate ways after attempting to rescue a young girl, Giorgia (Jasmine Trinca), from an abusive sanitarium. The brothers are Matteo and Nicola Carati (Alessio Boni and Luigi Lo Cascio). Their parents are Angelo (Andrea Tidona) and Adriana (Adriana Asti), their older sister is Giovanna (Lidia Vitale), and their younger sister is Francesca (Valentina Carnelutti). Their friends, their lovers and others drift through, including Giorgia who struggles with mental issues, but whose life seems to follow in parallel.
 logotherapist (a person who takes mental patients for walks to help them begin to feel normal) and his patient was Giorgia. Noticing that Giorgia has been wounded by electroshock therapy, he decides to remove her from the institution and take her along with him and Nicola&mdash;who are about to go on a trip to Norway.
 Arno River flood. Here, Nicola meets a university student, Giulia (Sonia Bergamasco).

===1968 February===
Nicola and Giulia are living together in Turin, but the two do not marry.

===1974===
Matteo leaves the army and joins the police force. During this time, Matteo shows signs of continuing depression and anger. He accepts an assignment in Sicily, a place corrupted by the Mafia. Meanwhile, Nicola and Giulia conceive and care for Sara, their daughter.

===1977===
In Sicily, Matteo meets a photographer in a caffè named Mirella (Maya Sansa). She wants to be a librarian, and he advises her to work at a beautiful library in Rome.

Because of his temper, Matteo is forced to leave Sicily. He decides to reside in Rome but refuses to visit his mother.

Meanwhile, Nicola becomes a psychiatrist, and works to eliminate the abuse and maltreatment of patients in mental hospitals. He finds Giorgia in one of these hospitals. She is tied to a bed in inhumane conditions, does not talk, and shows fear of being touched by others.
After some time, Giulia gets drawn into a secret Red Brigades covert cell|cell. One night, she leaves Nicola and Sara and disappears into the terrorist underground.

===1983===
Years later Matteo walks into that same library and sees Mirella for the second time. They fall in love, and one evening, make love in a car.  Eventually he pushes her away.

===1983 December===
Mirella meets with Matteo with news for him, but he behaves so harshly to her that she does not tell him that she is pregnant with his child.  On New Years Eve, Matteo decides to finally visit his mother. Everyone is there to celebrate. Instead of waiting for the traditional toasts, however, Matteo decides to leave early and, at midnight, jumps off the balcony of his apartment and kills himself.

The family is devastated by the tragedy. No longer motivated, Nicolas mother quits her teaching job and lives a life in solitude in Rome. Nicola, feeling that he could have saved Matteo and not wanting to make the same mistake again, arranges for the capture of Giulia to prevent her from killing someone else or from getting killed. She is sentenced to 17 years in jail. During her jail term, Nicola visits Giulia and proposes to her but is rejected.

===1992 spring===
Nicola finds a photograph of Matteo taken by Mirella. He is encouraged by Giorgia to meet with Mirella which, after some hesitation, he agrees to do. When he meets Mirella, Nicola learns about her son (Andrea) and that Matteo was the father. Nicola breaks this exciting news to his mother and they visit Mirella and the boy on the small island of Stromboli. Inspired by new meaning in her life, Nicolas mother decides to stay with Mirella and her grandson. She will die there, some years later.
 art conservation and becomes engaged to Mimmo. During this time, Nicola finds out his mother has died and, as a result, travels to Stromboli to visit Mirella and pay his respects.

===2000 spring===
Having finally moved past the death of Matteo, Nicola and Mirella fall in love. Sara, now happy and strong, is encouraged by Nicola to confront her mother and try to patch things up. Giulia, now out of jail and in desperate need of love, embraces Sara, but is not ready to open up completely.

===2003=== North Cape, which is where his father and Nicola ventured to go at the beginning of the movie, but never completed their journey.

== Cast ==
*Alessio Boni: Matteo Carati 
*Luigi Lo Cascio: Nicola Carati 
*Jasmine Trinca: Giorgia Esposti
*Adriana Asti: Adriana Carati 
*Sonia Bergamasco: Giulia Monfalco 
*Fabrizio Gifuni: Carlo Tommasi 
*Maya Sansa: Mirella Utano 
*Valentina Carnelutti: Francesca Carati 
*Nila Carnelutti: Francesca Carati a 8 anni
*Camilla Filippi: Sara Carati
*Greta Cavuoti: Sara Carati a 8 anni
*Sara Pavoncello: Sara Carati a 5 anni
*Andrea Tidona: Angelo Carati 
*Lidia Vitale: Giovanna Carati 
*Claudio Gioè: Vitale Micavi 
*Giovanni Scifoni: Berto
*Paolo Bonanni: Luigino
*Riccardo Scamarcio: Andrea Utano
*Francesco La Macchia: Andrea Utano a 6 anni
*Paolo De Vita: Don Vito
*Mario Schiano: Professore di medicina
*Michele Melega: Professore di lettere
*Pippo Montalbano: Commissario di Palermo
*Antonello Puglisi: Sacerdote di Palermo
*Roberto Accornero: Presidente del tribunale di Torino
*Stefano Abbati: Spacciatore
*Giovanni Martorana: Maghrebino
*Patrizia Punzo: Gallerista
*Fabio Camilli: Detenuto Tangentopoli
*Domenico Centamore: Enzo
*Mimmo Mignemi: Saro
*Kristine M. Opheim: Ermione
*Therese Vadem: Therese
*Maurizio Di Carmine: Terrorista
*Krum De Nicola: Brigo
*Walter Da Pozzo: Mario
*Sergio Risso: Maggiordomo
*Emilia Marra: Dottoressa
*Nicola Vigilante: Infermiere
*Joana Jiménez: Lolita
*Maria Grazia Bon: Portinaia

==Accolades==
{| class="wikitable" style="font-size:small;" ;
|- style="text-align:center;"
! colspan=4 style="background:#B0C4DE;" | List of Accolades
|- style="text-align:center;"
! style="background:#ccc; width:32%;"| Award / Film Festival
! style="background:#ccc; width:25%;"| Category
! style="background:#ccc; width:35%;"| Recipient(s)
! style="background:#ccc; width:8%;"| Result
|- style="border-top:2px solid gray;"
| rowspan="1" | 3rd Bastia Italian Film Festival
| Audience Award
| Marco Tullio Giordana
|  
|- 2004 Belgian Syndicate of Cinema Critics  Grand Prix
| Marco Tullio Giordana
|  
|- 56th Cannes Film Festival
| Un Certain Regard
| Marco Tullio Giordana
|  
|-
| rowspan="1" | 29th César Awards Best Film from the European Union
| Marco Tullio Giordana
|  
|-
| rowspan="3" | 16th European Film Awards Best Actor
| Luigi Lo Cascio
|  
|- Best Director
| Marco Tullio Giordana
|  
|- Best Screenwriter
| Sandro Petraglia, Stefano Rulli
|  
|- 45th Globo doro
| Best Director
| Marco Tullio Giordana
|  
|-
| Best Screenwriter
| Sandro Petraglia, Stefano Rulli
|  
|-
| Special Jury Award
| Adriana Asti
|  
|-
| Best Actor
| Luigi Lo Cascio
|  
|-
| Best Actress
| Sonia Bergamasco
|  
|-
| Best Film
| Marco Tullio Giordana
|  
|- 15th Palm Springs International Film Festival
| Audience Award – Best Narrative Feature
| Marco Tullio Giordana
|  
|- 33rd Rotterdam International Film Festival
| Audience Award
| Marco Tullio Giordana
|  
|- 30th Seattle International Film Festival Best Director
| Marco Tullio Giordana
|  
|-
|}

== See also ==
*List of longest films by running time

==References==
 

==Sources== Rai Radiotelevisione Italiana. ISBN 88-397-1269-0

==External links==
*  
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 