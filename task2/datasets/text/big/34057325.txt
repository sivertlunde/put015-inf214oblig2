Hiling
{{Infobox film
| name = Hiling
| image =  
| image_size =
| alt = 
| caption = 
| director = José Javier Reyes
| producer = Charo Santos-Concio Malou Santos
| writer =  José Javier Reyes
| starring = Camille Prats Shaina Magdayao Serena Dalrymple
| music = Jaime Fabregas
| cinematography = Eduardo Jacinto
| editing = Kelly Cruz
| studio = Star Cinema Available Light Productions
| distributor = Star Cinema
| released =  
| runtime = 
| country = Philippines English Tagalog
| budget = 
| gross =  
}}
 family fantasy film starring the then child stars Camille Prats, Shaina Magdayao and Serena Dalrymple. The film was directed by the acclaimed José Javier Reyes and released by Star Cinema. 

==Plot==
The film revolves around the character of Anna (played by Camille Prats), an ordinary girl who on her birthday was given a magical power by Manang Garcia (played by Gina Pareño), a mysterious stranger. Anna would eventually discover that she was given a power to grant any wishes of other people by merely touching them with her palm. Her power would eventually change the lives of all people around her including herself.
 
==Cast and characters==
*Camille Prats as Anna who has the ability to grant unrestricted wishes to others. When someone wishes something and she touches them afterwards, the wish becomes real. An alternate outcome of the wishs result comes when the wisher gains abilities too indicating that she could also grant peoples most deepest desires manifesting as powers.
*Shaina Magdayao as Abigail a close friend of Anna and Trinket
*Serena Dalrymple as Trinket also a close friend of Anna and Abigail
*Gina Pareño as Manang Gracia who has the ability to bestow powers to others and as such was the one who gave Anna the power of unrestricted wishes via touch and Power Bestowal.
*Nida Blanca as Lola Melyang grandmother of Anna who at first was hesitant on believing her granddaughters powers but after seeing all the miracles Anna has done, she changed her mind.
*Tirso Cruz III as Oscar father of Anna who is so concern of the childs safety that he want to hide her child to a place that no one knows them.
*Cherry Pie Picache as Cely mother of Anna who loves her so much and is willing to protect her from everything no matter what.
*Paolo Contis as Elwood, Annas hopeless romantic neighbor who wished to become very attractive and/or a chick magnet and wants the ladies in their town to aggressively want him and love him like literally follow him wherever he goes. When Anna touched him, the wish came true but the bad side effect of it is that the girls are willing to kill each other just for his yes. The only immune to his love pheromone is Rowena whom is a close friend of his prompting an interest to her.
*Carlo Aquino as Rolly, a close friend of Elwood

===Supporting roles===
* Rez Cortis as Mario, husband of Margie
* Dexter Doria as Margie, who wished that her husband should become more attached to her and would serve her all the time like a queen, that she will be the only beautiful in her husbands eyes. It did came true when Anna touched her afterwards saying the wish, the bad side effect is that her husband "exaggerated" everything she wished to a point of giving up his job just for her.
* Mely Tagasa as Pura, who wished that if only her flowers grew a lot bigger and taller and that whenever she talks to them or pour on some water to them, it will grow. Anna touched her and said "Dont worry maam Rica, it will happen", just a few days later when she talked and poured water to her plants, the plants grew big and taller. But the bad side effect of it is that the plants become deadly flower vines constricting anyone whos in contact with it except Rica herself whos the only one that calm the plants down. A possible result of her gaining the ability of Plant Communication and Plant Growth.
* Ogie Diaz as Ahrnell, who wishes to become a beautiful, sexy and lovely woman and yells excitingly "I wanna be a mother!" that shocks Anna who accidentally touched him on his legs. Later that day, he eventually becomes a lady and called himself as "Annabel". The bad side effect of his wish is that he becomes an instant pregnant lady that the same day at night. Ahrnell now Annabel seeing his tummy instantly growing rapidly called for help as it seems like hes going to bore a child soon.
* Via Veloso as Annabel, the lady that Ahrnell has become.
* Menggie Cobarrubias as Minggoy, the father of Trinket who wishes that if only they were rich then all of their problems would be solved. After saying the wish, Anna touched him with comfort and the lotto ticket he bought did get the overwhelming jackpot prize, making them the richest in their town. But because of their instant luck, they become greedy, boastful and selfish to a point of wanting to have more.
* Joy Viado as Joy, the mother of Trinket whom as a result of her greediness, planned to kill Lolo Sotero for them to be able to own all the land area of their town in which Lolo Sotero refuses to sell to them.
* Koko Trinidad as Lolo Sotero, who almost got killed thanks for the help from Anna, Trinket, Abigail and their friends it was avoided.
* Arlene Tolibas as Precy, a resident in Annas town
* Archie Adamos as Dado, husband of Ursula who managed to witness some of Annas miracles
* Mel Kimura as Ursula, who owns a vacant lot in their town. But when a child wishes that on the mini tab pool on the vacant lot was a big swimming pool for him and his friends to enjoy more. After Anna touches the boy, the lot did automatically have a very big pool for children but since Ursula owns the lot, no one will enter it if they wont per her for the entrance.
* Rudy Meyer as Carding, a concern neighbor who debated on Ursulas decision for the pool entrance.
* Boom Labrusca as Junjun
* Andrea del Rosario as April, one of the girls who was willing to die and kill anyone that will get Elwoods heart and partnership.
* Vanessa del Bianca as Valerie, one of the girls who was willing to die and kill anyone that will get Elwoods heart and partnership.
* Carol Banawa as Rowena, a close friend of Elwood and Anna and the only girl in the town who is immune to Elwoods love pheromones. This could be a fact that when she wished on wanting to have a true love in life, one that will not lie to her and protect her all the time and knows her inside and out (actually thinking of Elwood then Anna touched her) giving her the ability to be immune on any form of seduction.
* Monina Bagatsing as Marivi
* Bacci Garcia as Shirley
* Mon Confiado as Choy, the one who was hired by Joy to kill Lolo Sotero but failed due to the main protagonists rescue.
* Mhalouh Crisologo as Ruby Valera, a Pinay TV reporter who reported the unusual happening in the town of Anna and Anna herself.
* Pocholo Montes as Serge
* Shamaine Buencamino as Rica, a neighbor in Annas town
* Don Laurel as Darwin, a neighbor in Annas town
* Felindo Obach as Kikoy, a neighbor in Annas town
* Noel Carpio as Cosme, a neighbor in Annas town
* Ruby Solmenaro as Meding, a neighbor in Annas town
* Lani Tapia as Melody, a neighbor in Annas town
* Luz Imperial as Oyang, a neighbor in Annas town
* John Tionloc as Popoy, a neighbor in Annas town
* Isaac Villalobos as Jasper, a neighbor in Annas town
* Wamachill Guangco as Aida, a neighbor in Annas town
* Jon Villarin as Matias, a neighbor in Annas town

==Production==
The majority of the visual effects of the film were handled by Roadrunner Network, Inc.. The titles were handled by Cinemagic. The film processing were handled by Star Film Laboratories.

==References==
 

==External links==
* 

 
 
 
 