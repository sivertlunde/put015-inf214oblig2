Darwaaza Bandh Rakho
{{Infobox film
| name           = Darwaza Bandh Rakho
| image          = Darwaza Bandh Rakho film poster.jpg
| image_size     =
| caption        = Film poster
| director       = J. D. Chakravarthi
| producer       = Ram Gopal Varma
| writer         =
| narrator       =
| starring       = Aftab Shivdasani Isha Sharvani Manisha Koirala Chunky Pandey Ishrat Ali Snehal Dabi Divya Dutta Gulshan Grover Aditya Srivastava
| music          =
| cinematography =
| editing        =
| distributor    = RGV Film Company
| released       = 4 August 2006
| runtime        =
| country        = India
| language       = Hindi
| budget         =
}}

Darwaza Bandh Rakho (English: Keep the Door Shut) is an Indian Bollywood film directed by J. D. Chakravarthy released in 2006.

== Synopsis == Zakir Hussain Gujarati family of 35. The head of the household is played by Ishrat Ali.

Their stay in the house gets extended when they learn that the hostages father has gone abroad and they have to wait until he returns. The kidnappers are forced to take more hostages to keep their identity secret and prevent the kidnapping venture from failing. Eventually other people are stuck in the house, such as: Mughale Azam (pizza guy), Gunpat Kale (police constable), Julie (sales girl), Sharad Shetty (guy who wants money from Kantilal shah), Munish Mehta (real estate guy) What follows is a "mad-mad comedy of errors". Manisha Koirala plays the part of a sales girl who is dragged into the fray when she arrives in the house in order to sell them shampoo.

Taneja finally appears he gives the four kidnappers the money, however there is now a fight between the four, and then Taneja calls the police. The kidnappers go to the court, Isha says Ajay Goga Raghu are innocent but not Abbas. After 16 months Ajay owns a hotel and works with Mughale Azam Isha and Ajay are both friends, Julie works with Raghu on the sales business, Goga owns a sweet shop, Ganpat Kale is not a policeman but police driver, Abbaas goes to jail, and the Shah residence live happily ever after. 

==Reviews==
ome called it a "thrilling comedy"; other reviewers regarded it as an innovative theme that "ran out of steam" midway into the plot. The performance was regarded as "lackluster". 

==References==
 

==External links==
*  

 

 
 
 