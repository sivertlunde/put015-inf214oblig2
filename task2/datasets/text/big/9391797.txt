Double Exposure (1944 film)
{{Infobox film
| name           = Double Exposure
| image_size     =
| image	=	Double Exposure FilmPoster.jpeg
| caption        =
| director       = William Berke
| producer       = William H. Pine (as William Pine) William C. Thomas (as William Thomas)
| writer         = Ralph Graves (story) Winston Miller (story and screenplay) Maxwell Shane (screenplay)
| narrator       =
| starring       = Chester Morris Nancy Kelly Alexander Laszlo
| cinematography = Fred Jackman Jr.
| editing        = Henry Adams Howard A. Smith
| distributor    =
| released       = 18 December 1944
| runtime        = 63 minutes
| country        = USA
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}} 1944 American crime comedy film directed by William Berke, and starring Chester Morris and Nancy Kelly.

==Plot summary== Iowa Falls who gets a job offer in New York City for a magazine. She was hired due to a misunderstanding from the magazines publisher, and sports fanatic James R. Tarlock (Richard Gaines) that she was a man. Larry Burke (Chester Morris), the magazines editor, is thrilled that his new co-worker is a female, and takes an immediate interest in her. Pats boyfriend Ben Scribner (Phillip Terry) is less enthusiastic about the girl who he is intending on marrying, leaving him for a job in the big city, and goes as far as following her to make sure that she is not unfaithful to him. Pat, meanwhile, quickly impresses Larry by providing him photographs of Dolores Tucker (Jane Farrar), as she is lying on the ground in a restaurants toilet after a failed suicide attempt. The publishing of the photo unleashes some controversy, because Dolores is the estranged wife of millionaire Sonny Tucker (Charles Arnt). To deliver him a successful follow-up, Pat travels to Sonnys apartment to photograph him, and disguises herself as a chorus girl to seek entrance. Inside, she quickly flatters him, enabling herself to take as many photos as she wants. Dolores walks in minutes later, and grows convinced that Pat is Sonnys mistress.

A more important theme of the film includes the love triangle between Larry and Ben that Pat becomes involved with. Fearing that her boss will lose interest in her when he finds out that she is engaged to another man, Pat introduces Ben to him as her own brother. Ben reluctantly goes along with the scheme, though troubles her by constantly interfering whenever she is talking to Larry. Larry finally finds out about their relationship, and in anger sends Ben on a new task, that (without him knowing) brings him to Russia. Pat, meanwhile, is imprisoned after setting up a fake crime, which she created for the magazine. Dolores, by this time, is found murdered, and both Pat and Larry work together to reveal that she was killed by Sonny. Through the process, they fall in love with each other, and they are free to be together after finding out that Ben married another woman while in Russia.

==Cast==
*Chester Morris as Larry Burke
*Nancy Kelly as Patricia Pat Marvin
*Phillip Terry as Ben Scribner
*Jane Farrar as Dolores Tucker
*Richard Gaines as James R. Tarlock
*Charles Arnt as Sonny Tucker
*Claire Rochelle as Smitty
*Roma Aldrich as Mavis

==Soundtrack==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 


 
 