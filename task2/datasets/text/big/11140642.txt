Maharasan
{{Infobox Film
| name = Maharasan
| image = 
| image_size = 
| caption = 
| director = G. N. Rangarajan
| producer = G. N. Rangarajan
| writer = Gopu Babu (dialogues)
| screenplay = G. N. Rangarajan
| story = Gopu Babu
| narrator = 
| starring = Kamal Haasan  Bhanupriya
| music = Ilaiyaraaja
| cinematography = M. Kesavan
| editing = K. R. Ramalingam
| studio = Kumaravel Films
| distributor = Kumaravel Films
| released =  
| runtime = 149 minutes
| country = India
| language = Tamil
| budget = 
| preceded_by = 
| followed_by = 
}} 1993 Tamil Tamil comedy film directed and co-written by G. N. Rangarajan. The film features Kamal Haasan and Bhanupriya in the lead roles, while Ramesh Aravind and Raghavi play supporting roles. The film released in May 1993. 

==Cast==
*Kamal Haasan
*Bhanupriya
*V. K. Ramaswamy (actor)|V. K. Ramaswamy
*Ramesh Aravind
*Goundamani Senthil
*Vadivelu
*Raghavi
*Vaigai Chandrasekhar
*Jaiganesh
*Vadivukkarasi MRK

==Production==
Kamal Haasan worked again with director G. N. Rangarajan for the fifth time after a twelve-year break following three previous collaborations in Kalyanaraman (1979 film)|Kalyanaraman (1979), Ellam Inba Mayyam (1981), Kadal Meengal (1981) and Meendum Kokila (1981). 

==Release==
The film was a low-budget production and received mixed reviews. Kamal did not get a salary for this project. 

==Soundtrack==
{{Infobox album|  
  Name        = Maharasan
|  Type        = Soundtrack
|  Artist      = Ilaiyaraaja
|  Cover       =
|  Released    = 1993
|  Recorded    = Feature film soundtrack
|  Length      =
|  Label       =
|  Producer    =
|  Reviews     =
|  Last album  = 
|  This album  = Maharasan (1993)
|  Next album  = 
}}
The soundtrack of the film was composed by Ilaiyaraaja and lyrics written by Vaali (poet) |Vaali.

{{tracklist
| headline        = Track-list
| extra_column    = Singer(s)
| total_length    = 
| title1          = Arachu Arachu
| extra1          = Mano (singer) |Mano, S. Janaki
| length1         = 05:06
| title2          = Avana Ivana
| extra2          = Malaysia Vasudevan
| length2         = 04:20
| title3          = Entha Velu
| extra3          = Malaysia Vasudevan, K. S. Chithra
| length3         = 05:04
| title4          = Rakoozhi Koovum
| extra4          = S. P. Balasubrahmanyam, S. Janaki
| length4         = 04:59
| title5          = Rasa Maharasa
| extra5          = Malaysia Vasudevan, Minmini
| length5         = 04:23
}}

==References==
 

==External links==
*  

 
 
 
 
 


 