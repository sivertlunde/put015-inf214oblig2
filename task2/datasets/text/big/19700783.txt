She's 19 and Ready
{{Infobox film
| name           = Shes 19 and Ready
| image          =
| caption        =
| director       = Franz Josef Gottlieb
| producer       = Karl Spiehs Wolfgang von Schiber
| writer         = Henry Kwan
| starring       = Sabine Wollin Ekkehardt Belle Claus Obalski Gina Janssen
| music          = Gerhard Heinz
| cinematography = Franz Xaver Lederle
| editing        = Gisela Haller Neue Constantin Film
| released       = April 20, 1979
| runtime        = 89 min.
| country        = West Germany
| awards         = German
| budget         =
| preceded_by    =
| followed_by    =
}}
 1979 Cinema German Sex sex comedy film.

==Plot==
The story centers on a rather uninhibited young woman named Eva (Sabine Wollin) and her two boyfriends - Stefan (Ekkehardt Belle) and Claus (Claus Obalski). Eva needs to decide which of the two is to be her "steady". While pondering that question, Eva inherits some overseas properties from a rich uncle. The three of them embark on a world tour to exotic places, meeting up with Evas cousin Britta (Gina Janssen) along the way. At the films conclusion, Evas "choice" is to keep both young men as beaus.

==Cast==
*Sabine Wollin as Eva
*Ekkehardt Belle as Stefan 
*Claus Obalski as Claus
*Gina Janssen as Britta

==Soundtrack== Dschinghis Khan" in a dance club performed by the band themselves and "Slip Away Susie" by Bernie Paul in a music video style sequence during a guest appearance by Bernie himself.

==Airplay==
The film received a good deal of airplay in the early 1980s on HBO and other cable TV pay channels which sought R-rated programming not available on regular TV.

==External links==
* 

 
 

 