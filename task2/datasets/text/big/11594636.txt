Letty Lynton
{{Infobox film
| name           = Letty Lynton
| image          = Letty1.jpg
| caption        = Original film poster.
| director       = Clarence Brown
| producer       = Hunt Stromberg (uncredited)
| writer         = Wanda Tuchock (adaptation) John Meehan
| based on       =   Robert Montgomery Nils Asther Lewis Stone May Robson
| cinematography = Oliver T. Marsh
| editing        = Conrad A. Nervig
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 84 min.
| country        = United States English
| budget         = $347,000  . 
| gross          = $1,172,000 
}}
 Robert Montgomery and Nils Asther.  The film was directed by Clarence Brown, and based on the 1931 novel of the same name by Marie Adelaide Belloc Lowndes. Crawford plays the title character, in a tale of love and blackmail. 
 Adrian - a white cotton organdy gown with large ruffled sleeves, puffed at the shoulder.  Macys department store copied the dress in 1932, and it sold over 50,000 replicas nationwide.  Letty, the character played by Crawford, also gets away with murder, in a classic example of a Pre-Code Hollywood movie.

==Synopsis==
New York City socialite Letty Lynton has been living in Montevideo, Uruguay and wants to end her affair with Emile Renaul. On a steamship to the United States, Letty sees wealthy American Jerry Darrow and is immediately attracted to him. At dinner, their attraction increases, and after two weeks at sea, they have fallen in love. 

On Christmas Eve, a worried Letty tells her maid that they must leave the boat in Havana because she doesnt want Jerry to know about her wild past, but when Jerry comes to her room to propose, she accepts. In New York, Letty is shocked to see Emile waiting for her on the dock. Making an excuse to Jerry, she leaves the boat before him and learns from Emile that he flew from South America to see her and plans to take her back with him. After she leaves Emile in the customs office, Letty goes home, accompanied by Jerry, who tells her that they have been invited to the home of his parents in Upstate New York and will leave that night, after Letty tells her mother about the engagement. Lettys mother, Mrs. Lynton, is an embittered woman who shows no affection for Letty, whom she regards as irresponsible. Soon Emile arrives, having read about the engagement in the newspapers, and warns her to meet him in his hotel room that night or he will show Jerry her explicit love letters. Letty is revolted and resolves to commit suicide rather than spend her life with Emile. She calls Jerry to change their departure to the next day, then goes to Emiles hotel, taking a bottle of poison with her. Letty begs for her letters, but he refuses and tells her that their affair will only be over when he says so. While Emile goes to the door to talk to a waiter, Letty puts the poison in her champagne glass, planning to drink it herself. 

When Emile returns, however, he strikes her, then picks up her glass by mistake and drinks the poison, as Letty mutely watches. Letty then lets him carry her to the bedroom, and waits until he starts to feel the poisons effects. As he dies, she screams that shes glad she did it, even if she hangs. She then cleans up the room and leaves. 

The next day, soon after Letty and Jerry have arrived at the home of his parents, a detective from New York arrives looking for Letty and demanding that she come with him. Jerry, Mrs. Lynton and Lettys maid accompany her to see District Attorney John J. Haney, who produces the letters and accuses Letty of murder. After she admits that she went to see Emile, Jerry lies by saying that he and Letty spent the night together at his apartment after she left Emiles, and that he knew all about the letters. Mrs. Lynton corroborates Jerrys story by saying that she followed Letty to Jerrys apartment. She also says that she overheard Emile say he would kill himself if Letty did not return to him. Lettys maid, Miranda, also corroborates the story, after which Haney says that the case is closed and Letty is free to go.

==Cast==
*Joan Crawford as Letty Lynton Robert Montgomery as Jerry Darrow
*Nils Asther as Emile Renaul
*Lewis Stone as District Attorney Haney
*May Robson as Mrs. Lynton, Lettys Mother
*Louise Closser Hale as Miranda, Lettys Maid
*Emma Dunn as Mrs. Darrow, Jerrys Mother
*William Pawley as Hennessey Walter Walker as Mr. Darrow, Jerrys Father

==Reception==
Photoplay magazine commented,  "The gripping, simple manner in which this picture unfolds stands it squarely among the best of the month....Joan Crawford as Letty is at her best. Nils Asther is a fascinating villain. Robert Montgomery gives a skillful performance....The direction, plus a strong cast, make Letty Lynton well worth seeing."

Motion Picture Herald noted, "Almost everything one can wish for in entertainment has been injected into this superbly acted and directed production. The gowns which Miss Crawford wears will be the talk of your town for weeks after...and how she wears them!"

===Box Office===
According to MGM records the film earned $754,000 in the US and Canada and $418,000 elsewhere resulting in a profit of $390,000. 

==Legal status== District Court Second Circuit awarded one-fifth of the net of Letty Lynton to plaintiffs Sheldon and Ayer Barnes in their plagiarism action against MGM. This case was said to be the first copyright decision ever to direct the apportionment of profits on the relative basis as in patent suits where a patent has been appropriated. 

On November 7, 1939, MGM petitioned the United States Supreme Court to overturn the Court of Appeals ruling, stating that the questions arising in the suit were predicated solely upon the copyright laws of the U.S., and not the patent laws. However, MGM did not prevail in this latter action, and the film is unavailable even to this day save for some bootlegged copies. 
 Robert Stevenson, based on the play by Sheldon and Ayer Barnes.

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 