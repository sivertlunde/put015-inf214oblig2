Unaru
{{Infobox film
| name           = Unaru
| image          = Unaroovcdcover.jpg
| producer       = N.G John
| director       = Mani Ratnam
| writer         = T. Damodaran
| starring       = Mohanlal Sukumaran Ratheesh Sabitha Anand
| cinematography = Ramachandra Babu
| editing        = B. Lenin
| studio         = Geo Movie Production
| distributor    = Geo Pictures
| released       =  
| runtime        = 146 minutes
| language       = Malayalam
| music          = Ilaiyaraaja
}}
 Indian feature directed by Ashokan and Balan K. Nair.   

The film gives the inside view of the problems that arose in the labour trade union parties in Kerala. Filled with acclaimed performances by all the major cast, it shows the social as Peter in lead roles.The movie enjoys the unique distinction of being the first Manirathnam directorial in Malayalam language.The movie was also Manirathnams first box office success.The movie is the one where Manirathnam and Mohanlal joined together for the first time.Manirathnam later worked with Mohanlal after 10 years for Iruvar.

==Crew==
The score and soundtrack is composed by Ilaiyaraaja, and the films cinematography is by Ramachandra Babu.

== Cast ==
 
*Mohanlal... Ramu
*Sukumaran....Janardanan
*Ratheesh... Peter
*Sabitha Anand
*Balan K. Nair
*Unni Mary
*Krishnachandran Ashokan
*Jagannatha Varma
*Lalu Alex
*Kundara Johny
*Prathapachandran
*Philomina
*Paravoor Bharathan Janardhanan
*Sathyakala
 

==Soundtrack==
The music was composed by Ilayaraja and lyrics was written by Yusufali Kechery.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Deepame || S Janaki, Chorus, CO Anto, Krishnachandran || Yusufali Kechery ||
|-
| 2 || Theeram thedi olam padi || S Janaki || Yusufali Kechery ||
|}

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 