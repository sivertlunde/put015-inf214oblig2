Saheb Bahadur
{{Infobox film
| name           = Saheb Bahadur
| image          = 
| image_size     = 
| caption        =  Chetan Anand
| producer       = 
| writer         =
| narrator       = 
| starring       =
| music          = Madan Mohan
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1977
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
 1977 Bollywood film directed by Chetan Anand.

==Plot==
In a small town, situated in a picturesque Indian valley, lives a corrupt Deputy collector, Hare Murari, an equally corrupt Police Superintendent, Pasupathi; a doctor; a Judge; & Professor Rampyare. These officials always ensure that no one gets anything done without their permission, thus ensuring that their palms are adequately greased. When a young man named Prem Pratap seeks a license for conducting a song and dance sequence, he too is asked to bribe them, which he does. Subsequently, Hare Murari finds out that Prem Pratap may be a Government Official who has come incognito to investigate and expose them. Hare Muraris and the others worst fears are realized when they find out that Prem has been speaking long distance with none other than the President of India. What follows is hilarious chaos as the officials come together to try and portray themselves as honest and law-abiding citizens.

==Cast==
* Jalal Agha ...  Judge 
* Ajit ...  Ajays dad 
* Dev Anand ...  Prem Pratap 
* G. Asrani ...  Aainu 
* Bhagwan ...  Dy. Collectors assistant 
* Anil Dhawan ...  Officer  Dhumal ...  Charandas 
* Preeti Ganguli ...  Vimla Murari 
* Jankidas ...  Astrologer 
* I. S. Johar ...  Prof. Rampyare 
* Bharat Kapoor ...  Doctor 
* Durga Khote ...  Meenas grandmother  Manorama ...  Mrs. Hare Murari 
* Keshto Mukherjee ...  Trumpet Player 
* Mukri ...  Waiter 
* Raza Murad ...  Ajay 
* Paintal ...  Madhav 
* Om Prakash ...  Dy. Collector Hare Murari 
* Priya Rajvansh ...  Meena 
* Maruti Rao ...  Havaldar Totaram (as Maruti)  Asit Sen ...  Forest Officer Fakirchand 
* K. N. Singh ...  Police Supdt. Pasupathi  Sudhir ...  Ajays partner  Sunder ...  Hotel Manager

==Trivia==
* Chetan Anand directed his brother Dev Anand in an earlier version of the same movie, Afsar (film)|Afsar.
* This movie is based on the play by Nokolai Gogol.

==External links==
*  

 
 
 


 