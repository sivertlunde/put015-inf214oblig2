Air Hostess (1980 film)
{{Infobox film 
| name           = Air Hostess
| image          =
| caption        =
| director       = P Chandrakumar
| producer       =
| writer         = Gulshan Nanda S. L. Puram Sadanandan (dialogues)
| screenplay     = S. L. Puram Sadanandan
| starring       = Prem Nazir Rajani Sharma Jagathy Sreekumar Jose Prakash
| music          = Salil Chowdhary
| cinematography = Anandakkuttan
| editing        = G Venkittaraman
| studio         = Jacob Movies
| distributor    = Jacob Movies
| released       =  
| country        = India Malayalam
}}
 1980 Cinema Indian Malayalam Malayalam film,  directed by P Chandrakumar and produced by . The film stars Prem Nazir, Rajani Sharma, Jagathy Sreekumar and Jose Prakash in lead roles. The film had musical score by Salil Chowdhary.   

==Cast==
 
*Prem Nazir as Jayan
*Rajani Sharma as Rathi
*Jagathy Sreekumar as Damodaran Kutty
*Jose Prakash as Mr. Menon
*Sankaradi as Shankara Panicker Shubha as Sandhya
*Lalu Alex as Gopinath
*Master Sujith as Anil Meena as Rathis Mother
*Nanditha Bose as Kamala
*P. K. Abraham as Jayans Brother
*Baby Thulasi as Anitha Kunchan
 

==Soundtrack==
The music was composed by Salil Chowdhary and lyrics was written by ONV Kurup.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Onnaanaam Kunninmel || K. J. Yesudas, Vani Jairam || ONV Kurup || 
|-
| 2 || Unaru unaru ushaadevathe || K. J. Yesudas, Vani Jairam || ONV Kurup || 
|}

==References==
 

==External links==
*  

 
 
 


 