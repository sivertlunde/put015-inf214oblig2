Josh and S.A.M.
{{Infobox film
|name=Josh and S.A.M.
|image=Joshandsam.jpg
|caption=Theatrical release poster
|director=Billy Weber
|producer=Martin Brest
|writer=Frank Deese
|starring=Jacob Tierney Noah Fleiss Stephen Tobolowsky Joan Allen Chris Penn Martha Plimpton Maury Chaykin Udo Kier Ronald Guttman Ann Hearn
|music=Thomas Newman Don Burgess
|editing=Chris Lebenzon
|studio=Castle Rock Entertainment New Line Cinema
|distributor=Columbia Pictures
|released=November 24, 1993
|runtime=98 minutes
|country=United States
|language=English
|budget=$18,000,000
|gross=$1,640,220
}} American drama-comedy MPAA rating system rated this film PG-13. 

==Plot== dark comedy brainwashes his younger brother Sam (Noah Fleiss) making him believe that he was a genetically designed child warrior.    Josh says that Sam is actually an acronym, and that he is a "Strategically Altered Mutant" that was designed by the government to fight in a secret war in Africa. After a series of various suspicious coincidences in Joshs lies, Sam eventually believes that he is a S.A.M.
 high school reunion to seek refuge. He later lies that his mother was a graduate, and he finds Derek Baxter (Chris Penn), a drunken man claiming to be his father. Before Josh has time to clear his lie, Sam appears and, shortly thereafter, Derek drives them to their "grandparents" house to tell the good news. Upon entering the house Derek overreacts to a picture of the real family and goes after Josh. After Sam hits him with a cueball, Josh reacts defensively and hits Derek on the head with a pool cue, supposedly killing him. In panic, the two brothers steal his rental car and begin their trek to Canada.

After a day of Josh and Sam driving they encounter Alison (Martha Plimpton), who is an older teen runaway from Hannibal, Missouri.  They pick her up due to a resemblance to another lie of Joshs, the Liberty Maid. According to the Liberty Maids description she aids fleeing S.A.M.s to Canada, in the similar way of Harriet Tubman. Alison travels with them as their driver and during the run develops a bond with Josh. After a run-in with a cop outside of Salt Lake City, Sam flees, causing a chase through the desert that nearly kills Sam as he crawls under a train. After Josh and Allison reach the car, they dash to the road to continue their journey.

During a night stop in a motel, Sam decides to leave Josh and Alison as he steals the car. Later that day, Josh and Alison part ways after she fails to convince him to live in Seattle with her. After a long walk, he discovers the car on the side of the road. Unfortunately, Sam is not there, but he discovers a bus stop nearby and rides it the rest of the way to Canada. On the bus, he sees Sam riding on the back of a Semi-trailer truck|semi-truck and, after he and Sam reunite, they walk across the border into Canada.
 Orlando on a plane. Feeling unwanted at home and considering himself a fugitive, Josh stays behind. He soon finds out that Derek is alive and is given a reason to return home. Among one of the advancements of the film was learning to trust and love one another in the face of the emotional turmoil of their new family situation.

==Cast==
*Jacob Tierney as Joshua "Josh" Whitney
*Noah Fleiss as Samuel "Sam"/"Killer Sam" Whitney
*Stephen Tobolowsky as Thomas "Thom" Whitney
*Joan Allen as Caroline Whitney LaTourette
*Chris Penn as Derek Baxter
*Martha Plimpton as Alison (The Liberty Maid)
*Maury Chaykin as Pizza Man
*Udo Kier as Tanning Salon Manager
*Ronald Guttman as Jean-Pierre "J.P." LaTourette
*Ann Hearn as Teacher
*Anne Lange as Ellen Coleman
*Sean Baca as Curtis Coleman
*Jake Gyllenhaal as Leon Coleman
*Amy Wright as Waitress

==References==
 

==External links==
* 
* 
* 
* 

 
 
 
 
 
 
 
 
 
 
 