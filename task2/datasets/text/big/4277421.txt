Chamatkar
{{Infobox film
| name = Chamatkar चमत्कार  
| image = Chamatkar.jpg|
| caption = Promotional Poster|
| director = Rajiv Mehra  Lilliput Rajiv Mehra 
| producer = Parvesh C. Mehra
| starring = Shahrukh Khan Naseeruddin Shah Urmila Matondkar Shammi Kapoor
| music = Anu Malik
| distributor = Eagle Films
| released = 8 July 1992
| runtime = 171 min
| country = India
| language = Hindi
| box office=  INR 80 Million  
}}

Chamatkar ( ,  }}, Translation: Miracle) is a Hindi ghost comedy movie which was directed by Rajiv Mehra and released in India in 1992. It cast Naseeruddin Shah and Shahrukh Khan in pivotal roles. Rajiv Mehra chose to work again with Khan in the future Ram Jaane. The film was quite popular among kids but was a commercial failure.

==Plot==

Sunder Srivastava (Shahrukh Khan) is a young graduate whose main ambition in life is to fulfill his fathers dream of starting a school on his half-acre property in his village, though he has no funds to execute his plans. Sunders childhood friend Prem, a seasoned conman in Mumbai, convinces the gullible Sunder to mortgage. When Sunder comes to Mumbai, he is first tricked and loses his luggage, then pick-pocketed and loses his money. He then finds that Prem tricked him and fled to Dubai with using his money. Sunder is forced to take shelter in a cemetery. Sunder starts cursing his stars and venting his anger. A voice responds to him and a surprised Sunder asks the person to identify himself. The source of the voice, who cannot be seen, is surprised and asks Sunder whether the latter can really hear him.

Sunder realizes that he has been talking with a ghost and panics. The ghost suddenly becomes visible to Sunder and introduces himself as Amar Kumar alias Marco (Naseeruddin Shah). Marco tells Sunder that only he can help Sunder and vice versa. Marco tells his story. Marco was an underworld gangster who fell in love with one Savitri Kaul (Malvika Tiwari), daughter of one Mr. Kaul (Shammi Kapoor). Savitri declined to marry him if he did not change his ways. To show that he was serious, Marco resolved to give up crime. This did not bode well for his protégé Kunta (Tinu Anand), who wanted to become as big as Marco himself. On his wedding night, Marco was kidnapped and killed by Kunta, after which he was buried in the cemetery. Marco tells Sunder that many crimes taking place in the city under his name are actually done by Kunta and his minions.

Marco tells Sunder that due to his sins, he cannot attain redemption. Marco was foretold that only his savior would be able to see and hear him, so Sunder has to help him. Sunder declines, but Marco surprises him by reminding him of his dream, about which Sunder had not told a thing to Marco. Marco tells him that he wants to see Savitri and Mr. Kaul. He also tells that he cannot touch or harm anybody until time comes. Marco manages to get Sunder a position as a cricket coach in a school run by Mr. Kaul. Marco is angered when he finds out that   after his murder, Kunta and his goons came to the Kaul household and told Savitri that Marco was alive and had fled India never to come back, suggesting that Marco only married her in order to sleep with her. Kunta had told them that Marco wanted Savitri and her father to hand over the ownership documents for his hotel to Kunta, but Savitri refused to hand them over unless Marco himself came to ask for them. Kunta then tried to rape Savitri, but was stopped when her father broke down, promising to give them the documents. Hearing all of this, Marco is furious and vows revenge. He is grieved to then find out that Savitri died some time afterwards, but overjoyed when he learns that he has a daughter from the wedding night with Savitri called Mala (Urmila Matondkar).

Sunder and Marco also find out that the school lacks funding and that Kunta is trying to usurp its land. Mala and Sunder start falling in love. Marco helps and keeps Kuntas goons away. Later, Marco shows a secret room to Sunder where he had kept all his loot. The room was not known to Kunta or anybody else, so Marco proposes that an anonymous donation be made, which will be more than enough to save the school and help Sunder. However, due to their oversight, Kunta finds out the location of the room, and Marco loses all his money. In a desperate bid, Marco steals some money and bets to double the money. Sunder is held responsible for the theft, although no proof is present. Marco tells the truth to Sunder, and they have a fall-out.

Sunder agrees to a cricket match between his team and a team headed by Kuntas nephew: if they win the game, they will win funds to keep the school. Initially, Sunders team is losing, but Marco then steps into the game (still invisible to everyone), sabotages the opponent team and helps Sunders team, leading Sunders team to succeed massively.

After a brief meeting between Sunder and Kunta prior to the start of the match where Sunder mentions Marcos ghost, Kunta becomes suspicious. During the match, he abducts Sunder along with Mala and buries them alive in the very place he had buried Marco. Marco manages to lead the police to the cemetery where a fight erupts between Kunta and his goons on the one hand, Marco, Mala, Sunder and the policemen on the other. After succeeding in beating up the goons, Marco starts to strangle Kunta with a rope while Sunder forces Kunta to confess his role in Marcos murder. Marco then pushes Kunta into the empty grave and as he is about to kill him with a large rock, Mala calls out for him to stop, calling him "father" and entreating not to kill and sully his hands with blood because of Kunta. On hearing this, Marco immediately relents and lets Kunta live.

Finally, Sunder succeeds in his mission. Sunder and Mala marry, with Marco attending the wedding. At the wedding, a ray of light falls upon Marco who then ascends to heaven, although not before asking for "a minute" to entreat the viewer to do the right thing while they are alive, because they will not have the chance that he did to set things right after death.

==Plot Similarity==
The plot of Chamatkar has in common with the American film Blackbeards Ghost the concept of a sports coach who arrives at a new town where he encounters the reformed ghost of a criminal and aids in that criminals redemption while courting a nearby woman of his own age. Both films also feature a local band of wrongdoers who act as the villains of the film and a quarrel between the ghost and the protagonist beginning when the ghost places a bet of which the protagonist disapproves. Throughout most or all of both films, the ghost is invisible and inaudible to every character except the protagonist, but is able to handle objects normally, producing the bizarre appearance of objects or people moving without any apparent cause.

== Cast ==
* Shahrukh Khan ... Sunder Srivastava
* Naseeruddin Shah ... Amar Kumar, alias Marco
* Urmila Matondkar ... Mala Kumar
* Shammi Kapoor ... Mr. Kaul / Marcos father-in-law
* Deven Verma ... Inspector P.K. Santh
* Ashutosh Gowarikar ...  Monty
* Johnny Lever ... "Battery" Peon / Cricket commentator
* Tinu Anand ... Kunta
* Anjan Srivastav ... Police Commissioner I.M.Tripathi Ali Asgar ... Rakesh
* Guddi Maruti ... She-Girl
* Gavin Packard ... Goonga
* Malvika Tiwari ... Savitri Kaul
* Anjana Mumtaz ... Mrs. Kaushalya Raj Mehta
* Rakesh Bedi ... Moti
* Arun Bakshi ... Umpire
* Achyut Potdar ... Ticket Checker
* Ravi Patwardhan ... Money Lender

==Tracks list==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)!! Length
|-
| 1
| Is Pyar Se Meri Taraf Na Dekho
| Kumar Sanu, Alka Yagnik
|  05:29
|- 
| 2
| Is Pyar Se Meri Taraf Na Dekho
| Kumar Sanu
| 05:11
|- 
| 3
| Yeh Hai Pyaar Pyaar 
| Kumar Sanu, Asha Bhosle
| 04:52
|- 
| 4
| Pyaar Ho Jayega 
| Kumar Sanu, Alka Yagnik
| 05:29
|- 
| 5
| Bichoo O Bichoo 
| Asha Bhosle
| 05:47
|- 
| 6
| O Meri Neendein Churane Wale 
| Kumar Sanu, Asha Bhosle
| 07:40
|- 
| 7
| Dekho Dekho Chamatkar 
| Kumar Sanu, Sukhwinder Singh, Nandu Bhende
| 05:57
|- 
| 8
| Dekh Dekh Chamatkaar 
| Kumar Sanu, Nandu Bhende
| 05:55
|- 
| 9
| Jawani Deewani  Poornima 
| 06:35
|}

==See also==
* List of ghost films

== References==
 

==External links==
* 

 
 
 