En carne viva (1951 film)
{{Infobox film
| name           = En carne viva
| image          = 
| caption        =
| director       = Alberto Gout
| producer       = Alfonso Rosas Priego
| writer         = Alberto Gout
| starring       = Rosa Carmina Rubén Rojo Crox Alvarado
| music          = Rosalío Ramírez
| cinematography = Agustín Jiménez
| editing        = 
| distributor    = Producciones Rosas Priego
| released       = February 24, 1951 (México) 
| runtime        = 91 min 
| country        = Mexico
| language       = Spanish
| budget         =
| gross          =
}}
En carne viva (In the Cut) is a Mexican drama film directed by Alberto Gout. It was released in 1951 and starring Rosa Carmina and Rubén Rojo. 


==Plot==
Maria Antonia (Rosa Carmina) is a naive cabaret dancer who is seduced and abandoned by a sailor named Fernando (Crox Alvarado). After giving birth to their daughter, her suicide mark the life of her daughter Laura, who reach youth follows in the footsteps of her mother and becomes successful dancer. The twists and contragiros of fortune take Laura to live delirious and distressing family recognitions.

==Cast==
* Rosa Carmina ... María Antonia / Laura
* Rubén Rojo ... Arturo
* Crox Alvarado ... Fernando
* Dagoberto Rodríguez ... Miguel
* José María Linares Rivas ... Don Hilario
* Toña la Negra ... Mercé
* Maruja Griffel ... Arturos Mother
* Celia Viveros ... Lola


==Reviews==
The film was created as a vehicle for showcasing of the beautiful Cuban-Mexican rumbera Rosa Carmina. Very neat formally, En carne viva has his best times during the first hour of the film, where the native beauty of Rosa Carmina stands with own light among the foliage of a Veracruz recreated in a studio where Rosa Carmina seems her princess blue. Above all, the film is a feast for the eyes to recreate again and again in the beauty of the Cuban-Mexican actress, who runs very well her character: not only her oblique and distrustful look is enormously seductive, also it is very believable as she very going from the tenderness to the sensuality and from anguish to the wise resignation. 

The producer, Alfonso Rosas Priego had a particularly fond of the argument. The film is also rehearsed like theater, it was very rare in the cinema of that time.   

==References==
 

==External links==
*  
*  


 
 
 
 
 
 
 