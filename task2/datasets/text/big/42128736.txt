Tabloid Truth
 
{{Infobox film name           = Tabloid Truth image          =  director       = Kim Kwang-sik producer       = Shin Beom-soo   Jeong Sung-hoon   Shin Chang-gil writer         = Hwang Sung-gu   Kim Yu-jin starring       = Kim Kang-woo   Jung Jin-young   Ko Chang-seok   Park Sung-woong  music          = Lee Byung-hoon cinematography = Park Hong-ryeul editing        = Kim Sang-beom   Kim Jae-beom distributor    = CJ Entertainment  released       =   runtime        = 121 minutes country        = South Korea language       = Korean budget         = gross          =
}} thriller film directed by Kim Kwang-sik, and starring Kim Kang-woo, Jung Jin-young, Ko Chang-seok and Park Sung-woong. 
 sensationalistic and tabloid culture of South Korea, where rumor-mongering drives lucrative web traffic while also wreaking unaccountable havoc on its victims lives. 

==Plot== wiretapper Baek Moon (Ko Chang-seok), Woo unravels the opaque and lucrative world of secret tips and salacious rumors.  
 Japanese word chirashi, meaning "leaflet"), or the stock market tipsheet. In weekly secret meetings consisting of corporate personnel, politicians, reporters, public officials, and others, insiders exchange information about the latest goings-on. The collected intelligence is published into a tip sheet and distributed through paid subscriber channels before getting picked up by the tabloids as juicy blind items. What makes this combination particularly toxic is the sheen of respectability and truth given by the financial market players involved, despite the only occasional accuracy of its content. 

==Cast==
===Main characters===
*Kim Kang-woo as Lee Woo-gon
:Giving up has never been an option for talent manager Woo-gon. Ever since he first discovered actress Mi-jins potential as a star, Woo has worked tirelessly to build her acting career. Just as she was getting her big break, Mi-jin became the target of a nasty rumor that she was having an affair with a married politician, which caused her to take her own life. This infuriates Woo and drives him to seek out the people responsible for starting the rumor so that they can pay for their wrongdoing.

*Jung Jin-young as Mr. Park corporate corruption. After a mysterious accident that led to a major leg injury, Mr. Park changed his career path to become a tabloid distributor. He has a great sense of humor and lives a carefree lifestyle. That is, until Woo-gon seeks his help and he is forced to get back into his investigative mode, carefully following the trail of secrets and rumors.

*Ko Chang-seok as Baek Moon
:An infamous illegal wiretapper, Baek Moons life philosophy is to carefully "observe" everything and everyone. He even believes that there is nothing wrong with spying on his lovers. He normally avoids high-risk projects but he unintentionally gets pulled into Woo-gon and Mr. Parks private investigation.

*Park Sung-woong as Cha Seong-joo
:Cha Seong-joo is a "fixer," a professional problem solver who believes that the world is full of manipulations and lies. He is a perfectionist who always stays cool and sharp under any circumstances. He is hired by shadowy power players to threaten and prevent Woo-gon from finding the truth behind Mi-jins death.

===Supporting characters===
*Park Won-sang as Oh Bon-seok
*Kim Eui-sung as Park Young-jin
*Ko Won-hee as Mi-jin
*Lee Chae-eun as Miss Kim
*Lee Joon-hyuk as Nam-soo
*Yoon Young-kyun as Bungae ("Lightning")
*Han Cheol-woo as Han Seong-ho
*Woo Ki-hong as broadcasting station PD
*Kim Hee-chang as head of Zeus department
*Jeong Woo-hyeok as Daebbang ("Leader")
*Han Seung-do as Bo-seong
*Jin Seon-kyu as Nam-heung
*Jo Jae-wan as Joo-young
*Im Hyung-joon as Jo Woo-chan (cameo) Kwon Yul as Han Jung-soo (cameo)
*Ahn Sung-ki as Nam Jung-in (cameo)
*Jang Gwang as O& C chairman (cameo)

==Release==
Tabloid Truth was released in South Korea on February 20, 2014. It sold 442,234 tickets on its opening weekend, and 1.22 million by its second week. Though it received a positive reaction from critics, the film failed to ignite the larger interest of the public, box office-wise, despite a topical subject surrounding corruption in the media, entertainment industry, big business and politics.  
 CGV Cinemas in Los Angeles on March 7, 2014, before expanding its run to New York City, Vancouver and Hawaii on March 14.  

==See also==
*Choi Jin-sil
*Jang Ja-yeon

==References==
 

==External links==
*   
*   
* 
* 
* 

 
 
 
 