Zebra Force
{{Infobox film name           = Zebra Force image          =Zebra Force.jpg image_size     = caption        = Can the end justify the means? When Its Revenge...All Hell Breaks Loose! director       = Joe Tornatore producer       = Eddy Donno (associate producer) Joseph Lucchese (producer) Deno Paoli (executive producer) Larry Price (producer) Jefferson Richard (associate producer) Joe Tornatore (producer) writer         = Robert Leon (screenplay) Annette Lombardi (writer) Joe Tornatore (concept) narrator       = starring       = See below music          = Charles Bernstein Louis Febre Peter Rotter cinematography = Thomas F. Denove Billy Dickson Robert Maxwell editing  Michael Kahn studio		= Pac West Cinema Group distributor    = Echo Bridge Home Entertainment Trans World Entertainment released       = 1976 (Argentina) 1980 (Philippines) 1984 (USA) runtime        = 100 minutes 86 minutes (Swedish cut version) country        = USA language       = English budget         = gross          = preceded_by    = followed_by    = website        =
}}

Zebra Force (Codename: Zebra, USA title) is a 1976 American film directed by  , also named Code Name: Zebra followed in 1987. 

==Plot== flashbacks with a patrol being ambushed by the Vietcong, and the resulting Battle|firefight. The leader of the vigilante veterans is a man known simply as the Lieutenant who was their platoon leader and disfigured in the action. Recuperating in a hospital he regroups the survivors for a series of escalating raids to not only to enrich themselves, but to wipe out organised criminal gangs involved in illegal gambling and narcotics distribution.

The main protagonist of the film is Carmine Longo, a Mafia enforcer sent to meet with local chief to discover who performed the action. The two are assisted by a corrupt Los Angeles Police Department detective sergeant.  Their suspicion falls on their only known suspects, a gang of drug dealing black criminals who deny their involvement. Longo schemes to eliminate them through their police contact who will set up a drug deal where they can be killed by the police.

==Cast==
{{columns-list|2|
*Mike Lane ... Carmine Longo
*Richard X. Slattery Timothy Brown ...Jim Bob Cougar
*Rockne Tarkington
*Glenn R. Wilder    Anthony Caruso  ...  Salvatore Moreno  
* Stafford Morgan  ...  Sgt. Stangman  
* Clay Tanner  ...  Lt. Claymore  
* Tony Cristino  ...  Willie  
* Mario Milano  ...  Peter  
* David Ankrum  ...  Art  
* Buddy Ochoa  ...  Larry  
* Patrick Strong  ...  Nick  
* Bernard Tierman  ...  Ernie  
}}

==Notes==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 


 