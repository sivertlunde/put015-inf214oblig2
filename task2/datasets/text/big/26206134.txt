The Farmer Takes a Wife (1953 film)
{{Infobox film
| name           = The Farmer Takes a Wife
| image          = Farmerwife.JPG
| image_size     =
| caption        =
| director       = Henry Levin
| producer       = Frank P. Rosenberg
| based on       =  
| writer         = Marc Connelly (play) Walter Bullock Sally Benson Joseph Fields
| starring       = Betty Grable Dale Robertson Thelma Ritter
| music          = Cyril J. Mockridge
| cinematography = Arthur E. Arling
| editing        = Louis R. Loeffler
| distributor    = 20th Century Fox
| released       =  
| runtime        = 81 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $1,150,000 (US) 
}}
 musical comedy film of Call Me Mister (1951).

==Plot summary==
  John Carroll) she hires mild-mannered farmer Daniel Harrow (Dale Robertson) to work on the boat. Molly and Dan fall in love and marry. 

==Cast==
* Betty Grable as Molly Larkins
* Dale Robertson as Dan Harrow
* Thelma Ritter as Lucy Cashdollar John Carroll as Jotham Klore
* Eddie Foy, Jr. as Fortune Friendly
* Charlotte Austin as Pearl Dowd
* Kathleen Crowley as Susanna
* Merry Anders as Hannah
* May Wynn as Eva Gooch
* Nancy Abbate as Little Girl (uncredited)
* Doreen Tracey as Little Girl (uncredited)

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

 
 