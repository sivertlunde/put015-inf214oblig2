The Outsider (1939 film)
{{Infobox film
| name           = The Outsider
| image          = The-outsider-1939.jpg
| image_size     = 250px
| caption        = Australian film poster
| director       = Paul L. Stein
| producer       = 
| writer         = 
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1939
| runtime        = 
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 British drama The Outsider and was based on a play by Dorothy Brandon.

==Cast==
* George Sanders ...  Anton Ragatzy
* Mary Maguire ...  Lalage Sturdee
* Barbara Blair ...  Wendy
* Peter Murray-Hill ...  Basil Owen
* Frederick Leister ...  Joseph Sturdee
* Walter Hudd ...  Dr. Helmore
* Kathleen Harrison ...  Mrs. Coates
* Kynaston Reeves ...  Sir Montague Tollemach
* Edmund Breon ...  Dr. Ladd
* Ralph Truman ...  Sir Nathan Israel

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 


 