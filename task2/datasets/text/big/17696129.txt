On the Beat (1995 film)
{{Infobox film
| name           = On the Beat
| image          = 
| caption        = 
| director       = Ning Ying
| producer       = Han Sanping Francesco Cosentino
| writer         = Ning Ying
| starring       = Li Zhanho Li Jian
| music          = Su Cong
| cinematography = Zhi Lei Wu Hongwei
| editing        = Ning Ying
| distributor    = 
| released       =  
| runtime        = 105 minutes
| language       = Mandarin
| budget         = 
| film name = {{Film name| jianti         = 民警故事 
| fanti          = 民警故事 
| pinyin         = Mín jǐng gù shì}}
}} Chinese film directed by Ning Ying. It is the second film in Ning Yings Beijing Trilogy, a collection of three films that follows the massive changes to Beijing in the last decades of the twentieth century. Whereas Nings previous film, For Fun dealt with the older generation, On the Beat is firmly focused on the story of the middle-aged. I Love Beijing, meanwhile, would follow characters belonging Beijings younger generations. On the Beat was coproduced by Eurasia Communications, Euskal Media and the state-operated Beijing Film Studio. Some funding was also from the International Film Festival Rotterdams Hubert Bals Fund. 

On the Beat follows a group of police officers in the Deshengmen Precinct during the slow months of winter. The policemen, stuck in the dull winter months of Beijing, get stuck in routine, only occasionally finding excitement, as when they chase after a rabid dog.
 Telluride and San Sebastian, among others. 

== Plot ==

On the Beat consists mainly of a series of vignettes involving members of a local precinct in the Beijing Public Security Bureau. Filmed in the slow months of December, January, and February  the members of the PSB find they have little in the way of substantive police work. Instead they spend time (and manpower) on small incidents like the chasing of a possibly rabid dog, the arrest of a small-time con artist, and the reprimanding of a man who sells posters of a woman in a bathing suit.

Though the incidents seem small, the PSB treats each with deadly seriousness, often to comedic effect. The film culminates in the arrest and intense interrogation of a man who may have insulted a police officer. Realizing that they have no evidence, the officers bluster as they attempt to avoid losing face.

== Cast ==
Known for her style that echoes Italian Neorealism and cinema verite films,  Ning Ying cast her film primarily with non-professional actors. Most of the police officers, for example, were actual police officers with the Beijing Police, while the residents on the police officers beats were actual residents of Beijing neighborhoods.     

While essentially an ensemble piece, the film generally revolves around the character of Yang Guoli (played by police officer Li Zhanhe).

== Reception ==
On the Beat was well received by western critics and further cemented Ning Yings reputation as a major talent to emerge from China in the early to mid 1990s. As such, Ning is often considered part of the "Sixth Generation" as opposed to the earlier "Fifth Generation" to which Ning is actually closer in both in terms of age and experience.

Derek Elley of Variety (magazine)|Variety for example gives the film praise, but notes that perhaps the films "ironic humor is an acquired taste."  The New York Times meanwhile praised the films "effortless authenticity and a quiet, watchful style." 

Several critics saw the film as a sly critique of the Chinese bureaucracy,   a subject that was the focus of The Story of Qiu Ju, Zhang Yimous earlier film, and which would also be referenced several years later in Lu Xuechangs Cala, My Dog!.
 city wall to the constant construction overtaking the citys hutongs.    In so doing, Kraicer suggests, Ning Ying has created an image of a "traditional city under siege" by modernity. 

===Awards and nominations===
*1995 Entrevues Film Festival
**Grand Prix
*1995 San Sebastián International Film Festival
**Special Mention
*1995 Torino International Festival of Young Cinema
**Best Film - International Feature Film Competition
*1996 Fribourg International Film Festival
**Special Mention

== References ==
 

== External links ==
* 
*  at the Chinese Movie Database

 

 
 
 
 
 
 
 