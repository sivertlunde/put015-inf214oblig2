Time Runner
 
{{Infobox film
| name           = Time Runner
| image          = Time Runner DVD or VHS cover.jpg
| alt            = The videos cover art shows Michael Raynor holding a gun and a spaceship shooting a city.
| caption        = VHS Cover
| director       = Michael Mazo
| writer         = Greg Derochie
| starring       = Mark Hamill Rae Dawn Chong Brion James New Line Home Video
| released       =  
| runtime        = 90 minutes
| language       = English
}}

Time Runner is a 1993 science fiction film starring Mark Hamill.

==Plot==
During an alien invasion in the year 2022, fighter pilot Michael Raynor is sent back in time to the year 1992. Raynor enlists the help of scientist Karen Donaldson (Rae Dawn Chong) in an attempt to change the future by preventing the alien invasion. Meanwhile he is pursued by agents sent by the head alien Neila (Brion James).

==Cast==
*Mark Hamill as Michael Raynor
*Rae Dawn Chong as Karen Donaldson
*Brion James as Neila
*Mark Baur as Freeman 
*Gordon Tipple as  Arnie

==References==
 

== External links ==
* 
* 
* 
* 
* 
* 
 

 
 
 

 