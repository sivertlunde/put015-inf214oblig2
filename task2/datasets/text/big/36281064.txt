Gramam
{{Infobox film
| name           = Gramam
| image          = 
| caption        = 
| alt            =
| director       = Mohan Sharma
| producer       = Mohan Sharma
| writer         =  Mohan Sharma
| starring       = {{Plainlist| Nishan
* Samvrutha
* Nedumudi Venu
* Sukumari
* Mohan Sharma
* Y. G. Mahendra Nalini
* Fathima Babu
}}
| music          = 
| cinematography = Madhu Ambat
| editing        = 
| studio         =
| distributor    = 
| released       =  
| runtime        = 131 minutes
| country        = India
| language       = Malayalam Tamil
| budget         = 
| gross          = 
}} Tamil languages National Film Awards and two Kerala State Film Awards among other laurels. 

Gramam is the first part of a trilogy on the Palakkad Brahmin community. Set between 1937 and 1947, the film is about a child widow and how she fights societal conventions. The second and third parts of the trilogy will be respectively set between 1947-1962 and 1962-1975.   

The film released in Kerala on 10 August 2012.

==Awards== National Film Awards Silver Lotus Award - Best Supporting Actress - Sukumari Silver Lotus Award - Best Costume Design - Indrans Jayan
; Kerala State Film Awards Best Story - Mohan Sharma Best Male Playback Singer - Balamuralikrishna

==Cast==
* Mohan Sharma as Rao Bahadur Mani Swami Nishan as Kannan Samvrutha as Thulasi
* Nedumudi Venu as Govindankutty Maashu
* Sukumari as Amminiyamma/ Paatti Nalini as Kunju Ammaal Priya as Nechumu/Lakshmiyamma Renuka as Thankam
* Kozhikode Narayanan Nair as Panikkar Jolsyan
* Kalamandalam Radhika as Narayani
* Fathima Babu as Karthyayani
* Y. G. Mahendraa as Burma Ambi Swami
* Maya Vishwanath as Bhargavi
* Kripa as Vrinda
* Aneesh G Menon as Kuttan
* Ravi Shankar
* Abhilash

==External links==
*  

==References==
 

 
 
 
 
 
 
 
 
 
 


 