Project Shadowchaser II
{{Infobox film|
| name = Project Shadowchaser II
| image =
| caption =
| director = John Eyres
| writer = Nick Davis
| producer = Jeff Albert (associate producer) Shari Lane Bowles (associate producer) Danny Dimbort (executive producer) John Eyres (producer) Geoff Griffith (producer) Avi Lerner (executive producer) Danny Lerner (co-producer) Trevor Short (executive producer) Gregory Vanger (producer)
| starring = Frank Zagarino Bryan Genesse Beth Toussaint Daniel Bonjour
| cinematography = Alan M. Trow
| editing = Amanda I. Kirpaul
| music = Stephen Edwards	 	 
| distributor = Nu Image
| released =  
| awards =
| budget =
| runtime = 94 min.
| language = English
| country = United States
}}

Project Shadowchaser II, also known as Shadowchaser II, Night Scenes: Project Shadowchaser II, Night Siege and Armed And Deadly, is a 1994 science fiction film by director John Eyres. It is the second installment in the Project Shadowchaser film series.

==Synopsis==

A berserk android threatens mankind with nuclear annihilation, and three unlikely heroes must destroy it before it destroys everything.

==DVD release==

The film was released on DVD in 2007 by Image Entertainment as a double feature with Project Shadowchaser III.

==External links==
* 

 
 
 
 
 
 
 
 


 