All the Light in the Sky
{{Infobox film
| name           = All the Light in the Sky
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = 
| director       = Joe Swanberg
| producer       = Joe Swanberg
| writer         = Joe Swanberg Jane Adams
| screenplay     = 
| story          = 
| based on       =  
| narrator       =  Jane Adams Sophia Takal Kent Osborne
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 79 minutes
| country        =  English
| budget         = 
| gross          = 
}} Jane Adams, who co-wrote the movie. 

==Plot==
The film follows Marie (Jane Adams), an actress struggling with the prospect of aging, which becomes all the more apparent when her 25 year-old, aspiring actress niece (Sophia Takal) arrives for a weekend stay.

==Cast== Jane Adams as Marie
* Sophia Takal as Nica
* Lawrence Michael Levine as Larry
* Larry Fessenden as Rusty
* Kent Osborne as Dan

==Production==
Director Joe Swanberg served as the films only crew member.  

==Reception==
All the Light in the Sky has been positively received by critics. The film currently holding a 78% certified fresh rating on Rotten Tomatoes.  Andrew Barker, writing for Variety (magazine)|Variety, noted that the film delivered, "clever pacing, solid technique and a deeply soulful lead performance from co-scripter Jane Adams." 

==References==
 

==External links==
*  

 
 
 
 