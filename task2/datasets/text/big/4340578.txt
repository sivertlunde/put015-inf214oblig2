Ik Omhels Je Met 1000 Armen
{{Infobox film
| name           = A Thousand Kisses
| image          = Ik omhels je met 1000 armen.jpg
| caption        = Film poster
| director       = Willem van de Sande Bakhuyzen
| producer       = Brechtje Schaling
| writer         = Ronald Giphart Ruud Schuurman Edward Stelder
| starring       = Tijn Docter Carice van Houten Catherine ten Bruggencate Halina Reijn Johnny de Mol
| music          =
| cinematography =
| editing        = Wouter Jansen
| distributor    =
| released       =  
| runtime        = 110 min
| country        = Netherlands
| awards         = Dutch
| budget         = € 2,500,000
}}
 2006 Netherlands|Dutch drama film directed by Willem van de Sande Bakhuyzen. It was based on the novel of the same name by  .

==Plot==

Giph (Tijn Docter) is a young security guard and writer. He has a  girlfriend Samarinde (Carice van Houten), who is a physician and model. He is on holiday in Spain with her and their friends. Giphs mother Lotti ( ) has recently died. Giph plans to end the relationship after the holiday. Samarinde turns out be pregnant. The question arises whether Samarinde will have an induced abortion. However, she has a miscarriage. Giph loves Samarinde again and they continue the relationship.

A large part of the film consists of flashback (literary technique)|flashbacks, about Lottis multiple sclerosis and euthanasia by lethal injection, and Giphs relationship with Samarinde.

==Trivia==
Contains some male and female full frontal nudity.

==External links==
* 
* 

 
 
 


 
 