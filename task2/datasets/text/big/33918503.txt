Out of Our Minds (film)
{{Infobox film
| name           = Out of Our Minds
| image          = OOOMposter.jpg
| border         = yes
| caption        = Official film poster Tony Stone
| producer       = Melissa Auf der Maur Chemi Karasawa Nancy Barber  Ben Scola
| story         = Melissa Auf der Maur
| starring       = Melissa Auf der Maur Eliza Douglas Mark Peetsma Stephen G. Rhodes John Rosenthal Jeremy Gibson Damien Paris
| music          = Melissa Auf der Maur
| cinematography = Tony Stone Nathan Corbin Niles Roth
| editing        = Tony Stone
| distributor    = PHI Heathen Films
| released       =    
| runtime        = 30 minutes 
| country        = Canada United States
}} Tony Stone. studio album of the same name.

Development on Out of Our Minds began as early as 2005, when Auf der Maur began the recording sessions for the album, and pre-production began in summer 2006. Independently self-produced and financed, the film itself was shot over the course of two years, in autumn 2006 and spring 2007 in Stones hometown in   on January 16, 2009 and was also screened at the Woodstock Film Festival and NXNE among others. During her North American and European tours in support of the album in 2009 and 2010, Auf der Maur also held screenings of the film. The film was released on DVD as part of her package including the album and comic book on March 30, 2010. 

The film received positive critical reception upon its release with various reviews citing it as a "manifestation of an idea created amidst a life devoted to artmaking,"  "crazily ambitious,"  and "spectacular." 

==References==
 

==External links==
* 

 
 
 