The Guillotines
 
 
{{Infobox film
| name = The Guillotines
| image = The Guillotines poster.jpg
| alt = 
| caption = Film poster
| film name = {{Film name| traditional = 血滴子
| simplified = 血滴子
| pinyin = Xiě Dī Zǐ / Xuè Dī Zǐ
| jyutping = Hyut3 Dik1 Zi2}}
| director = Andrew Lau
| producer = Peter Chan Andrew Lau Jojo Hui Qin Hong Lorraine Ho
| writer = Jojo Hui Joyce Chan Aubrey Lam Philip Lui Guo Junli Chit Ka-kei
| starring = Huang Xiaoming Ethan Juan Shawn Yue Li Yuchun Jing Boran
| music = Chan Kwong-wing
| cinematography = Edmond Fung
| editing = Azrael Chung
| studio = Stellar Megamedia Group We Pictures
| distributor = 
| released =  
| runtime = 112 minutes
| country  = China Hong Kong
| language = Mandarin
| budget = US$15,000,000 
| gross = 
}}
The Guillotines is a 2012 Chinese-Hong Kong wuxia film directed by Andrew Lau,  starring Huang Xiaoming, Ethan Juan, Shawn Yue, Li Yuchun and Jing Boran.

==Plot==
During the Manchu people|Manchu-ruled Qing Dynasty, the Yongzheng Emperor established a secret assassination squad known as the Flying guillotine|Guillotines to eliminate all who posed a threat to him. Once heavily favored by the emperor, the Guillotines are deemed expendable once the Qianlong Emperor ascends to the throne and adopts Western ideas and technology. To consolidate his power under a new regime, the Qianlong Emperor continues to use the Guillotines to persecute the conquered Han Chinese in a reign of terror and oppression. The current guillotine squad is led by master fighter Leng and it consists of six young warriors: Musen, Houjia Shisan, Chen Tai, Hutu, Su and Buka. Serving the Qing court unquestioningly, they have never failed in 348 missions to eliminate their targets. However, their most recent mission would make it their last.

The Hans have been oppressed by their Manchurian-ruled government, causing social unrest and internal conflicts. Tian-Lang (Wolf), is the charismatic prophetic Han rebel leader of a group known as the Shepherd Gang. Wolf was captured alive by Leng and Wolf tells him that Leng is destined to kill him, but not at this place and time. Wolf was scheduled for execution the following day, however, Wolfs gang ambushes the execution parade and fends off both the Guillotines and the Green Army; it allowed Wolf to escape, but Bai-Lan (Shepherd member) was captured. In the chaos of fighting, Wolf takes Musen (the only female member of the Guillotines) hostage in hopes to exchange back Bai-Lan. However, the Guillotines were dispatched to recover both Musen and finish off Wolf.

With reports that Wolf has fled to the outer frontiers, the squad tracks the Shepherds to a remote village outpost, heavily occupied by Han and smallpox diseased inhabitants. Added to the mission, the Qianlong Emperor (who favors advanced weaponry over antiquated assassins) has sent his most trusted agent, Haidu (secret sworn brother to Leng and right-hand man to the emperor) for the same mission. Unknown to the Guillotines, the emperor wants to absolve his legacy and the Guillotines is a living taint to his rule; they must be erased. Although Leng knew of the emperors intentions and also wanted to help end the Guillotines, the group has become a family to him and he couldnt eliminate them. Therefore, Haidu was also sent there to finish a job Leng didnt have the heart to do; the definitive moment would help during their exchange.

At the exchange site, the Han and Shephards appeared in a display of unity and force, there to complete the exchange of prisoners when Haidu dispatched his rile attack squadron on both the Guillotines and the Shepherds; Bai-Lan was killed in the attack. Shocked at the attack on his own teammates, Leng resisted Haidu in open defiance to the emperors will, making him an enemy target as well; both the Guillotines and Shepherds survived the attack and escaped to safety. Confused as to what has happened, it was then Leng revealed his real identity to the Guillotines. The team was furious and wanted to know is Leng here to finish them off, however, he told his team to scatter and survive. Unfortunately, the Guillotines wouldnt live long.

The government had officially decreed the Guillotines as wanted criminals in league with the Shepherds. Publicly forsaken by their own emperor and given a huge bounty upon their heads, the Guillotines were cruelly killed off one-by-one. Saddened and enraged, Leng confronted Haidu and called Haidu a "lap dog," infuriating him to shoot Leng down a cliff; Leng survived and was nursed back to health by Wolf in a peaceful secluded village. Confused to why he was saved, Wolf did it out of mercy; to Lengs surprise, he found Musen as part of the village. Musen no longer wanted to be a Guillotine (after learning the cruelty the Guillotines had caused to Wolfs family) and only lived there as a villager. After talking with Wolf, Leng finally admits hes secretly Han Chinese. Leng tells Wolf that it was the former emperor that chose him to be the right-hand man for Qianlong and his Han identity was replaced with a Manchurian one. Although Leng is now on Wolfs side, Wolf prophesied that Leng will kill him very soon. 

The final battle is near ready. It was anticipated that Haidu would find them with a large attack force to destroy them and the village was evacuated ahead of time; only volunteers chose to remain to defend the village. Unfortunately, the attack force used mainly guns and cannons; the village defenses were useless against the might of ballistics. Musen volunteered to defend the escaping villagers, unfortunately, she was gunned down before she could even use her weapon to stop anyone. Leng sadly found Musen dying and mourned for her passing. Haidu had completely decimated the Han village and finds Leng on a mountain edge with Wolf. Believing his time had come, Wolf asked Leng to relay a message of peace to the emperor and welcomed Leng to end his life. With the death of Wolf by Lengs hands, Haidu could now return Leng to the palace with honor.

Back at the palace, Wolfs head was displayed as proof of the rebellions defeat; both Leng and Haidu received praises from their emperor, believing his golden age shall come. However, Leng uncharacteristically spoke up and told his lord that the reason there is rebellion is because theres neither prosperity nor happiness with the public; the emperors vision of a golden age would be hindered. Heeding the emperor, Leng told his lord must make peace and relieve the suffering of the people to sustain his vision. When asked would Leng be there when it arrives, Leng pointed out the emperor wishes to erase the shameful past legacies and that would mean his life as well, since hes the last Guillotine. With a sad reluctance, the emperor had Leng executed; the epilogue reveals that Qianlong was one of the longest running rulers and held peace between the Manchurian and Han, but there was no historic mention of the Guillotines during his reign.

==Cast==
* Huang Xiaoming as Tianlang (Wolf)
* Ethan Juan as Leng
* Shawn Yue as Haidu
* Li Yuchun as Musen
* Jing Boran as Houjia Shisan
* Wen Zhang as Qianlong Emperor
* Purba Rygal as Chen Tai
* Andrew Lau as Yongzheng Emperor Jimmy Wang as Gong E Li Meng as Bai Lan
* Gao Tian as Hutu
* Zhou Yiwei as Buka
* Stephy Tang as Bai Lan
* Guan Xiaotong

==References==
 

==External links==
*  
*  
*     on movie.mtime.com

 

 
 
 
 
 
 
 
 
 
 
 