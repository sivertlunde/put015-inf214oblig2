Badha (2005 film)
{{Infobox film
| name              = Badha
| image             = 
| writer            = Monoar Hossion Sukumar
| based on          =   Purnima Riaz Riaz
| director          = Shahin Shumon
| producer          = Ami Boni Kothachitro
| distributor       = 
| editing           = Thofik Hossion
| cinematography    = Aziz
| released          = 2005
| runtime           = 170 minutes
| country           = Bangladesh Bengali
| music             = Ahmed Imtaz Bulbol
}} Bengali film Purnima and Riaz in the main lead. It is inspired from 2004 Telugu film Arya (2004 film)|Arya; it was success at the box-office.

==plot==
Aakash (Riaz) is a spoiled brat, and is the brother of local MP(Dipjol). He likes to flirt with beautiful girls and wants them to be his girlfriends. One day he sees Bristy(Purnima) and proposes to her. When she refuses, he threatens to jump from the top of college building. Being a weak-minded girl, Bristy accepts the proposal with the thought that as some guy has already died for her and she doesnt want someone to get hurt because of her and starts dating him. Apon (Shakib Khan) is a happy-go-lucky guy who enjoys his life with friends. On his first day to the college, he sees Bristy who was just confessing her love to Aakash who was at the top of the college building, threatening her. Charmed by her beauty, Apon falls in love with her and proposes to her. She actually rejects his proposal saying that she is already committed to Aakash. One day MP arranges a party in his house and it so turns out that Aakas introduces Bristy to his Brother and persuades him to fix their marriage. Aakashs Brother, who initially pretended to accept hiss marriage with Bristy, turns tables upside down by introducing another girl Daina and announces that his brother is going to marry her. He threatens his brother not to marry anyone except Daina. Being embarrassed by the situation and helpless, Aakash sits down and starts to get frustrated. Apon, who loves Bristy so much that he never hesitated to help her, decides to bring Aakash to Bristy and helps them elope. On their way, they are followed by MPs henchmen. The three youngsters find a train and get into it.

==Cast==
*Shakib Khan
*Riaz Purnima
*Don
*Dipjol
*Ilash Kobra

==Music==

===Soundtrack===
{| border="3" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Trac !! Song !! Singer’s !! Performer’s !! Note’s
|- 1
|Comeon Baby Love Me Baby  Monir Khan  and Konok Chapa Shakib Khan Purnima
|
|- 2
|Chadni Raat
|
| Shakib Khan and Purnima
|
|- 3
|Ei Boke Kan Pathe Shuno Monir Khan, Fahmida Chowdhury and Polash Shakib Khan, Riaz
|
|- 4
|Ei Tumi Sei Tumi Asif and Doli Riaz and purnima
|
|-
|}

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 