Double Agent (2003 film)
{{Infobox film
| name           = Double Agent
| image          = Double Agent (2003) movie poster.jpg
| caption        = Theatrical Poster
| film name = {{Film name
 | hangul         =  
 | hanja          =  
 | rr             = Ijung gancheob
 | mr             = Ijung kanch‘ŏp}}
| director       = Kim Hyeon-jeong 
| producer       = Koo Bon-han Han Seon-kyu 
| writer         = Hyeon-jeong Kim  
| starring       = Han Suk-kyu Ko So-young 
| music          = Michael Staudacher
| cinematography = Kim Sung-bog 
| editing        = Kim Sang-beom Kim Jae-beom
| distributor    = Showbox 
| released       =  
| runtime        = 123 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          = $8,393,392 
}} Kim Hyeon-jeong about a North Korean intelligence officer who defects to South Korea.

==Plot==
June 1980.  Rim, a North Korean intelligence officer posted to East Berlin, attempts to cross Checkpoint Charlie with the intention of defecting to South Korea.  During his attempt to cross to West Berlin, he is wounded by North Koreans attempting to prevent his defection.  South Korean agents successfully retrieve Rim from no-mans land, and congratulate him on his freedom.  A week later, Rim is under brutal interrogation by South Korean Intelligence, who suspect he is a spy despite Rims steadfast denials.  Meanwhile, at senior level, a decision is made to allow him to work for South Korean Intelligence, after files brought over from North Korea prove to be authentic.

December 1982.  Rim has been posted to Gangwon, a rural province, and is assisting in the training of South Korean spies, who will infiltrate North Korea. During his down time, Rim listens to classical music on a radio program hosted by Yun.  Having spent two years in the backwaters, Baek, a senior agent, arrives in Gangwon and informs Rim that he has been promoted and is to return to Seoul as an analyst.

February 1983.  Rim has begun to assimilate with and earn the trust and respect of his fellow agents although he is still kept away from highly sensitive materials.  Rim continues to listen to Yuns classical music radio program, until Yun announces the program is to end.  The last musical piece played is a special request, and this transpires to be a code - Yun is a North Korean sleeper agent, and Rim is to make contact.  He is a double agent, and when he contacts Yun at the radio station, she is to be his handler.  Although Yun no longer has a job at the radio station, they are able to maintain contact when Baeks wife, a churchgoing acquaintance of Yuns, sets Rim up with Yun with the approval of Baek who has been wanting Rim to settle down. A relationship between the two begins to flourish.

Rim passes intelligence on an upcoming mission to insert South Korean spies into North Korea via a fishing boat to Yun.  Yun informs her own handler, "Blue River", who is a doctor named Song.  The mission fails when the fishing boat sinks, and all men on board are presumed drowned. Returning supplies to a hidden cache in a rural area, Song and Yun are discovered by an innkeeper who is alerted by their suspicious behavior in a heavy downpour.  Song follows the innkeeper and kills her but not before the police has been informed. Song is subsequently captured while Yun was able to slip away undetected.

Rim is brought in to interrogate Song, who is revealed to be the ranking North Korean agent in South Korea.  Song resists all attempts to question him, and Rim beats Song into unconsciousness.  Later, left unattended, Song attempts to kill himself and is taken to hospital in a critical condition.  Baek is made the head of Intelligence and promises to increase efforts to eradicate South Korea of radicals and Communist sympathizers.  Rim finds himself interrogating a young student who had recently been to East Berlin while on a visit to Germany, but concludes he is harmless.  More aggressive agents are brought in to question the student, who eventually confesses to be an North Korean agent, based on information provided to him by Rim during his own interrogation with the student.  It is clear that the confession was made under duress, and suspicions have been raised about Rims own reliability.  Later, Rim is contacted by another North Korean agent who questions Rims lack of commitment to the cause.  It transpires that Rim had been ordered to eliminate Song, but Yun, who received the orders, deliberately did not pass them on. Yun has begun to have second thoughts about her work and realizes both her and Rim are merely chattels to their superiors, and expendable.  

The South Koreans have been following leads in relation to Song, who has since died in hospital, in order to find other North Korean spies.  In doing so, they uncover a link to Yun, who is identified by Baek.  Yun is able to get out of the country with assistance from Sean Howard, a British reporter who was present at Rims confirmation ceremony as a South Korea citizen, and who suspects that Rim is a spy for the North Koreans.  Baek questions Rim over a lunch, and it is made clear that Rim has lost the trust of Baek.  Rim excuses himself to use the restroom, and makes his escape.  Returning to headquarters, he retrieves secret documentation on the failed mission.  Meeting with Sean Howard (who had earlier introduced himself to Rim and passed on his contact details), Baek hands over the documentation, and in return, Howard organizes Rims escape to Japan.  Rim knows he cannot return to either North or South Korea, having betrayed both countries.

1985 Brazil.  Both Rim and Yun have reunited in Brazil and Rim, under a new identity, is working as a crewman on a fishing boat while Yun is pregnant with his child.  Returning home from work, Rim stops to help a man with a broken down car.  The man calls Rim by his real name, and reveals himself as an assassin, but is not clear whether it is the North or South Koreans who have found him.  Rim is shot dead on the side of the road.  Yun waits for him to return home.

==Cast==
* Han Suk-kyu as Rim Byeong-ho
* Ko So-young as Yun Su-mi
* Song Jae-ho as Song Kyeong-man
* Chun Ho-jin as Baek Seung-cheol

==Reception== Kim Hyeon-jeong and upon release was considered a technically competent film, although somewhat grim.     Despite the high profile presence of Han Suk-kyu and Ko So-young, both of whom had been on an extended hiatus from the big screen,   Double Agent was a disappointment at the box office with a little over 360,000 admissions in Seoul theaters.  Although Han acquitted himself well in his comeback role, Ko was criticized for her performance. 

==See also==
* Cinema of Korea

==References==
 

==External links==
*  
*  
*   at koreanfilm.org

 
 
 
 
 
 
 
 