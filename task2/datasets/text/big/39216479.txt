Unclaimed
 
 
{{Infobox film
| name           = Unclaimed
| image          = Unclaimed 2013 film poster.jpg
| image_size     = 
| border         = 
| alt            = The poster shows a blank dog tag on the ground. Below the tag is the film title "Unclaimed".
| caption        = Theatrical poster
| film name      = 
| director       = Michael Jorgensen
| producer       = Michael Jorgensen
| writer         = Michael Jorgensen
| narrator       = 
| starring       = 
| music          = Mike Shields
| cinematography = Allan Leader
| editing        = Jonathan Mathew Nick Zacharkiw
| studio         = Myth Merchant Films
| distributor    = 
| released       =  
| runtime        = 77 minutes
| country        = Canada
| language       = English
| budget         = 
| gross          = 
}} Special Forces Green Beret Master Sgt. John Hartley Robertson, who was declared dead after being shot down over Laos on a classified mission on 20 May 1968. The documentary is written, directed, and produced by Michael Jorgensen. It follows Tom Faunce, a veteran of the Vietnam War, in tracking down the man who claimed to be Robertson. Faunce was skeptical of Robertsons identity but eventually became convinced. He convinced Jorgensen to make a documentary about Robertsons story as a way to unite the man with his American family.

Leading up to the films release, the validity of Robertsons identity was challenged by groups of Vietnam War veterans and groups that advocate the Vietnam War POW/MIA issue. Jean Robertson-Holley, Robertsons surviving sister, was convinced the man was her brother but initially declined DNA testing as unnecessary. Eventually she and her daughter (and Robertsons niece) Gail Metcalf expressed openness to going through with testing. The documentary was screened at the Hot Docs Canadian International Documentary Festival on  , 2013. A day later, The Independent reported the contents of a memo from a 2009 report by the Defense Prisoner of War/Missing Personnel Office that the man who claimed to be Robertson was actually Dang Tan Ngoc, "a 76-year-old Vietnamese citizen of French origin who has a history of pretending to be US army veterans". 

==Synopsis==
Vietnam veteran Tom Faunce now works as a missionary in Vietnam, where he hears of an elderly man claiming to be Robertson. Faunces meeting with the man spurs him to try to repatriate him, against the wishes of the U.S. government.   

The man claiming to be Robertson states in the film that he was kept in a bamboo cage in the jungle by the North Vietnamese and tortured for a year. Then, confused and badly injured, he was released and married a Vietnamese woman who had helped to care for him, assuming the name of her dead husband. Aged 76 as of 2013, he lives in a remote village in south-central Vietnam and is unable to remember his birthday or his American children’s names, and is now only able to speak Vietnamese. Scenes include a meeting with a veteran who was trained by Robertson back in 1960, and who recognized him on sight. 

==Production==

Unclaimed is written, directed, and produced by Michael Jorgensen. A veteran of the Vietnam War, Tom Faunce, had been on a humanitarian mission in southeast Asia in 2008 when he learned about John Hartley Robertson, a fellow soldier who was reported killed in action in the war, still alive in Vietnam. Faunce was skeptical when meeting Robertson but was eventually convinced of his identity. Faunce then contacted Jorgensen in 2012 to appeal to him to make a documentary as a way to help Robertson reunite with his family. Jorgensen was also skeptical but became convinced to make the documentary about Robertson. The filmmaker said he experienced difficulty with the U.S. military in making the documentary, such as being unable to contact Robertsons family. Jorgensen said a government source told him, "Its not that the Vietnamese wont let him (Robertson) go; its that our government doesnt want him."

==Validity of identity==

===Lead-up to film premiere===

With the publicity of Unclaimed, the identity of the man who claimed to be Robertson was challenged. In the documentary, the Toronto Star reported the case made, "There is physical proof of Robertsons birthplace, collected in dramatic fashion onscreen; a tearful meeting in Vietnam with a soldier who was trained by Robertson in 1960 and said he knew him on sight; and a heart-wrenching reunion with his only surviving sister — 80-year-old Jean Robertson-Holly  ." Leading up to the films premiere, Jorgensen said Robertsons American wife and two children had volunteered to participate in DNA testing but withdrew their intent. The filmmaker said, "Somebody suggested to me maybe thats (because) the daughters dont want to know if its him. Its kind of like, that was an ugly war. It was a long time ago. We just want it to go away... I dont know. What would compel you not to want to know if this person is your biological father?" 

 
After the reunion, the Toronto Star said of the man who claimed to be Robertson, "He is back in Vietnam and has no desire to leave, having fulfilled his one wish: to see his American family once more before he dies." The sister, Robertson-Holley, said it was "not necessary" to conduct DNA testing since she confidently identifies the man as her brother.  Some groups of Vietnam War veterans and groups that advocate the Vietnam War POW/MIA issue questioned Robertsons identity.    The Toronto Star reported, "The film is already generating heated debate online, along with allegations the man claiming to be Robertson is a fraud." Robertson-Holleys daughter (and Robertsons niece) Gail Metcalf said in response, "I dont blame people. I think some of the vets who say that are truly trying to protect us. They don’t want us to be scammed and their hearts are in the right place."  Robertson-Holley subsequently alleviated her adamant stance on DNA testing. Metcalf spoke for her mother, "We plan to do it when we can. We havent been able to do it yet. In the beginning, moms position was we dont care if people doubt. Were not spotlight kind of people." Expensive medical bills related to her parents critical injuries was cited as a reason for delaying DNA testing. The Toronto Star said in April 2013 pressure would likely increase when the documentary screens at the GI Film Festival in May 2013. Metcalf said, "Of course were more than willing. The bottom line is even if the DNA test came back negative, he’s still proven to be an American. My mother will never believe he is not her brother." 

===Post-release===
 Soldier of Fortune magazine, indicates "...mitochondrial DNA sequences from the hair samples obtained were compared to family reference samples taken from Robertson’s brother and one of his sisters."   

In response to the controversy, the GI Film Festival, which exhibited Unclaimed after the Toronto premier, posted a disclaimer on its site. 

===Directors view===
When Jorgensen was asked by the Toronto Star if he believed the mans identity, he replied, "It does not matter what I think. Theres no doubt in the family."    Jorgensen did confirm to Macleans magazine, however, he believes "the circumstantial evidence strongly indicates this man is John Hartley Robertson."   

===Results of Robertson Familys Independent DNA test===
In the wake of the controversy, the Robertson family raised funds in late 2013 to conduct their own independent DNA test. With results in hand, the family, on their funding site,  admitted the DNA of the man claiming to be Robertson did not match the nDNA of Robertsons nephew. 
 Stars and Stripes suggested people should not focus on the failed DNA test but an isotope test conducted on Ngocs last remaining tooth.  Jorgensen believes isotope test suggests Ngoc is likely an American. Stars and Stripes noted the isotope results match other areas of the word and failed to eliminate the possibility Ngoc grew up in another non-American location that matches the isotope levels measured in the tooth. In comments to the Stars and Stripes article a responder suggested the test could say nothing definitive as the filmmaker only tested one tooth. The responder suggested one needs to test two teeth that form at different times. If both teeth share similar isotope levels then one could say the person definitively grew up in a given geographic area. One cannot, by testing only one tooth, eliminate the possibility the man in the movie was moved around as a child and the isotope levels represent an "average", which just happen to correspond to some place in the USA, an area with very diverse geography where one is likely to find a match within error bars.

==Release==

Michael Jorgensen sought for Unclaimed to screen in his home country of Canada first. The documentary was screened for the first time at an invitation-only patrons screening at the Metro Cinema at the Garneau in early January 2013.  It then premiered at the Hot Docs Canadian International Documentary Festival on  , 2013.  The documentary had its first American screening at the GI Film Festival in Washington, DC on  , 2013.   

James Adams, reviewing for The Globe and Mail, gave the documentary three out of four stars and described it, "Part mystery, part forensic investigation, part journey to troubled pasts, part redemption song, Unclaimed is all heart."  Linda Barnard, who covered Unclaimed in the Toronto Star,   also reviewed the documentary, "Canadian director Michael Jorgensen’s emotional story... seems unbelievable. To the filmmakers credit, Jorgensen lets the audience decide whether or not to believe it as he follows Vietnam vet Tom Faunce’s quest to make good on a pledge to leave no man behind." 

==Spring 2014 Release==

Unclaimed was re-released in May 2014, playing in New York City and Los Angeles. Martin Tsai of the LA Times notes "Michael Jorgensen...doesnt do serious investigative work to check   claim that hes John Hartley Robertson".  The Hollywood Reporter assess the films commercial potential as "slim for a doc that...seemingly has little interest in uncovering the truth."  The Village Voice calls Unclaimed "a nakedly manipulative film". 

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 