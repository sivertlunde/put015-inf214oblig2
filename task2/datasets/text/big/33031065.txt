Khap (film)
{{Infobox film
| name           = Khap
| image          = Khap film.jpg
| caption        = Theatrical poster
| director       = Ajai Sinha
| chief assistant director        =  Rahul Jaiswal
| producer       = Sangita Sinha   Siddhant Sinha 
| writer         = Ashok Lal (script)   Vinod Ranganathan (script)   Ishaan Trivedi (dialogues)   Vijay Verma (dialogues)
| screenplay     = 
| story          = Ajai Sinha
| based on       =  
| starring       = Yuvika Chaudhary  Om Puri  Govind Namdeo
| music          = Annujj Kappoo
| cinematography = Lokesh Bhalla
| editing        = Sanjay Sankla
| post production executive        = Rahul Jaiswal
| studio         = Ananda Film & Telecommunications
| distributor    = 
| released       =    
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}}
 2011 Standard Hindi film starring Yuvika Chaudhary Om Puri, Govind Namdeo, Manoj Pahwa, Mohnish Bahl.  Directed by Ajai Sinha, the film is a socio-political drama based on the Manoj-Babli honour killing case and Khap Panchayats in villages of Haryana, Rajasthan and Uttar Pradesh, which order honour killing to prevent marriages within the same gotra.   

==Plot==
Madhur Chaudhary (played by Mohnish Bahl) moves out of his rural home leaving his father alone after various disputes they have. Madhur re-locates to Delhi along with his wife, Komal, and daughter, Ria (played by Yuvika Chaudhary). Sixteen years later, Ria is now in college, while Madhur is an Investigator with Human Rights Department. He is asked to investigate a case in his village of alleged suicide of a couple, Veer and Surili. The locals, including the fathers of the couple, Daulat Singh and Sukhiram respectively, admit that the couple had killed themselves. After going in depth in his investigation Madhur finds out that the deaths are one of many that have taken place in a region that is still bound by Khap Panchayat. It dictates that couples cannot marry distant relatives nor can they have an inter-caste/inter-religious marriage. As he delves further into this issue, he ends up being attacked, is hospitalized and then subsequently dies. Komal and Ria move in with Madhurs father Omkar Singh Chaudhary (played by Om Puri) and eventually settle down in the village.

Omkar finds out that Ria is in love with fellow-collegian, Kush (played by Sarrtaj Gill), the son of South Africa-based Jagmohan and Saroj Mitter, and arranges their marriage. After the couple return from their honeymoon, they find themselves locked in the same inhumane customs.

==Cast==
* Yuvika Chaudhary as Ria
* Om Puri as Omkar Singh Chaudhary
* Govind Namdeo as Daulat Singh
* Manoj Pahwa as Sukhiram
* Mohnish Bahl as Madhur Chaudhary
* Sarrtaj Gill as Kush J. Mitter
* Alok Nath as Professor
* Anuradha Patel as Komal
* Shammi as Masterni
* Nivedita Tiwari as Surili

==See also==
*Khap

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 