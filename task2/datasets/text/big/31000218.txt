Patrick the Great
{{Infobox film
| name           = Patrick the Great
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Frank Ryan
| producer       = Howard Benedict
| writer         = Ralph Block  Jane Hall Frederick Kohner
| screenplay     = Dorothy Bennett   Bertram Millhauser
| starring       = Donald OConnor   Peggy Ryan   Frances Dee
| cinematography = Frank Redman
| editing        = Ted J. Kent
| music          = Hans J. Salter (as H.J. Salter)
| studio         = 
| distributor    = Universal Pictures
| released       = 4 May 1945 
| runtime        = 84 min.
| country        =   English
}}
 American drama film starring Donald OConnor, Peggy Ryan, and Frances Dee. This was the last ever film for OConnor and Ryan together, who had been a teenage team for the past several years. This was also OConnor last film before he went to serve in World War II.

==Cast==

* Donald OConnor as Pat Donahue, Jr.
* Peggy Ryan as Judy Watkin
* Frances Dee as Lynn Andrews Donald Cook as Pat Donahue, Sr.
* Eve Arden as Jean Mathews
* Thomas Gomez as Max Wilson Gavin Muir as Prentis Johns
* Andrew Tombes as Sam Bassett
* Irving Bacon as Mr. Merney
* Emmett Vogan as Alsop
* unbilled players include Neely Edwards, Lassie Lou Ahern

== External links ==

*  

 
 
 
 

 