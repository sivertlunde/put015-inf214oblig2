Pains of Autumn
{{Infobox film
| name           = Pains of Autumn 
| image          = PainsAutumnFilmPoster.jpg
| image size     =
| alt            = 
| caption        = Theatrical poster
| director       = Tomris Giritlioğlu
| producer       = Bahadir Atay Fatih Enes Omeroglu
| writer         = Etyen Mahçupyan Nilgün Öneş Tayfun Pirselimoglu Ali Ulvi Hünkar
| narrator       =  Murat Yıldırım Beren Saat Okan Yalabık
| music          = Tamer Çıray
| cinematography = 
| editing        = 
| studio         =
| distributor    = Özen Film
| released       =  
| runtime        = 112 mins.
| country        = Turkey
| language       = Turkish
| budget         = 
| gross          = $3,388,636
| preceded by    = 
| followed by    = 
}}

Pains of Autumn ( ) is a 2009 Turkish drama film, directed by Tomris Giritlioğlu, based on the novel by Yilmaz Karakoyunlu. The film, which went on nationwide general release across Turkey on  , was one of the highest-grossing Turkish films of 2009.

==Production==
Production for the film, originally planned for 2002, commenced on 1 June 2008. The film was shot on location in Istanbul, Turkey from 3 August - 29 September 2008. Post production was completed on 23 January 2009.         

==Plot== Pogrom of nationalist movement Greek prostitute, who has been exploited by her grandmother after her mother had left them. Elena is aware that she is being observed by Behçet and falls in love with him. They get close to each other which does not please Behçets father and his political party. The death of his friend Suat and the role that his father plays with the leaders of the party to exterminate the opposition and even the tolerated voices force Behçet to reconsider his political belonging. Behçet finds Elena dead after being hit by the activist. He takes her and remembers the words she said while lying on the bed in her apartment.

==Cast== Murat Yıldırım - Behçet
* Okan Yalabık - Suat
* Beren Saat - Elena
* Belçim Bilgin - Nemika
* Umut Kurt - Ferit
* Zeliha Berksoy - Grandmother

==Release==
The film opened in 180 screens across Turkey on   at number one in the Turkish box office chart with an opening weekend gross of $555,543.   

{| class="wikitable sortable" align="center" style="margin:auto;"
|+ Gross Chart
|-
!Date!!Territory!!Screens!!Rank!!Opening Weekend Gross!!Total Gross!!As Of
|-
|  
| Turkey
| 180
| 1
| $555,543
| $3,105,177
| 8/23/09
|-
|  
| Greece
| 17
| 4
| $90,966
| $254,157
| 5/24/09
|-
|  
| Germany
| 21
| 37
| $15,447
| $28,179
| 5/10/09
|-
|  
| Austria
| 3
| 36
| $1,123
| $1,123
| 5/3/09
|}

==Reception==

===Box office=== Turkish film of 2009 and has made a total worldwide gross of $3,388,636. 

===Public controversy===
Television talk shows and newspapers have covered both the film and the discussion of the events on which it is based. Its makers say the public debate is a result of an easing of curbs on freedom of expression accompanying Turkeys drive to meet European Union membership standards. This film couldnt have been made 10 years ago, screen writer Etyen Mahçupyan told Todays Zaman, Though the laws on the books still limit free speech, the reality is theres less and less that cant be criticized.   

September 6–7 was our Kristallnacht, Rev. Dositheos Anagnostopulous, a spokesman for the Greek Orthodox Church in Istanbul said, The chances of something like this happening again are slim, because Turkish youth today are more critical in their thinking. But to be sure, they need to learn that this catastrophe occurred, thats why the film is important. 

A film like this might be just a film in another country, Mahçupyan continued, because theres been a vacuum and this issue was never discussed, the film now fulfils an important mission.

Yet, seen from the Greek perspective, the film does not depict the events of September 1955 in their actual historic depth, not to mention the portrayal of the main Greek character as a prostitute sold off by her family, totally contrasting the social reality of the actual Greek community of the city, which quite reflects the Turkish views over their non-Muslim neighbours, particularly the women. The film remains still highly controversial for being an attempt to white-wash the responsibilities of the Turkish people pretending it was all down to a political manipulation when this is a ponmgrom in which tens of thousands of Turkish citizens took part willingly and which totally erased the Greek community from the city of Constantinople.

== See also ==
* 2009 in film
* Turkish films of 2009

== External links ==
*   for the film
*  

==References==
 

 
 
 
 
 
 
 