Unindian
 
{{Infobox film
| name           = Unindian
| image          = 
| caption        = 
| director       = Anupam Sharma
| producer       = Anupam Sharma Lisa Duff
| writer         = 
| starring       = Tannishtha Chatterjee
| music          = Salim Sulaiman
| cinematography = Martin McGrath
| editing        = Marcus DArcy
| distributor    = 
| released       =  
| runtime        = 
| country        = Australia India
| language       = English
| budget         = 
| official website         =  
}}
UnIndian is an upcoming Australian-Indian film starring former Australian cricketer, Brett Lee.  The film is set for release in 2015. Directed by Anupam Sharma and starring Tannishtha Chatterjee in the lead, the film was shot in Sydney.

==Synopsis==
Beautiful divorcee and single mother of one, Meera (Tannishtha Chatterjee) is an Australian of Indian origin. Smart and independent, she has carved out a successful life for herself and her daughter … despite family pressure to find a nice Indian match. Then Meera meets Will (Brett Lee)… tall and blonde with a charming smile.  But falling in love with an Australian man is not only scandalous - its unindian! Does she do as her family wishes? … Or does she follow her heart and live her life the way she wants? Highlighting the complexities of wooing another from a different culture, unINDIAN is a comedy with a lot of heart and a little spice!

==Cast==
 
* Brett Lee as Will
* Tannishtha Chatterjee as Meera
* Stephen Hunter as Detective Thompson
* Pallavi Sharda as Shanthi
* Sarah Roberts as Priya
* Gulshan Grover as Deepak
* Andy Trieu as Detective Burly
* Supriya Pathak as Meeras mum
* Maha Wilson as Emma
* Heather Maltman as Michelle
* Arka Das as DK
* Nicholas Brown as Samir
* Akash Khurana as Meeras dad
* David E. Woody as Boat Captain
* Adam Dunn as Mich
* Bali Padda as Arjun
* Kashif Amjad as Vik
* Anupam Sharma as Swami
* Warren Ekermans as Yoga instructor
* Laura Bunting as Jess
* Maya Sathi as Smitha
* Kumud Merani as Binky Aunty
* Abhinav Bali as Venkatesh
* Mark Sheridan as Professor Brown
* Susi Ling Young as Chinese lady
 

– Source:  

==References==
{{reflist|refs=
   
   
}}

==Further reading==
*  
*  
*  

==External links==
*  
*  

 
 
 
 
 