My Teenage Daughter
{{Infobox film
| name           = My Teenage Daughter
| image          = 
| image_size     = 
| alt            = 
| caption        = 
| director       = Herbert Wilcox
| producer       =  Herbert Wilcox
| writer         = Felicity Douglas
| narrator       = 
| starring       = Anna Neagle   Sylvia Syms   Norman Wooland
| music          = Stanley Black
| cinematography = Mutz Greenbaum
| editing        = Basil Warren
| studio         = Herbert Wilcox Productions British Lion
| released       =  
| runtime        = 100 minutes United Kingdom English
| budget         = 
| gross          = £181,467 (UK) 
| preceded_by    = 
| followed_by    = 
}} British drama film directed by Herbert Wilcox and starring Anna Neagle, Sylvia Syms and Norman Wooland.  A mother tries to deal with her teenage daughters descent into Juvenile delinquency|delinquency. It was intended as a British response to Rebel Without a Cause. It was the last commercially successful film made by Wilcox. 

==Cast==
* Anna Neagle - Valerie Carr
* Sylvia Syms - Janet Carr
* Norman Wooland - Hugh Manning
* Wilfrid Hyde-White - Sir Joseph
* Kenneth Haigh - Tony Ward Black
* Julia Lockwood - Poppet Carr
* Helen Haye - Aunt Louisa
* Josephine Fitzgerald - Aunt Bella
* Wanda Ventham - Gina
* Michael Shepley - Sir Henry
* Avice Landone -Barbara
* Michael Meacham - Mark
* Edie Martin - Miss Ellis
* Ballard Berkeley - Magistrate
* Myrette Morven - Anne
* Grizelda Harvey - Miss Bennett
* Betty Cooper - Celia
* Daphne Cave - Deirdre
* Launce Maraschal - Senator

==References==
 

==Bibliography==
* Harper, Sue & Porter, Vincent. British Cinema of the 1950s: The Decline of Deference. Oxford University Press, 2007.

==External links==
* 

 

 
 
 
 
 
 


 