Absinthe (film)
 
{{Infobox film
| name           = Absinthe
| image          =File:Absinthe (1913) - Glen White.jpg
| image_size     =
| caption        =Film poster
| director       = Herbert Brenon
| producer       = Herbert Brenon Carl Laemmle
| writer         = Herbert Brenon (scenario)
| starring       = King Baggot Leah Baird
| cinematography = William C. Thompson
| studio         = Independent Moving Pictures Company Universal Film Manufacturing Company 
| released       =   reels 
| country        = United States
| language       = Silent English intertitles
| budget         =
| gross          =
}} silent drama film starring King Baggot and Leah Baird and directed by Herbert Brenon. Some sources also credit George Edwardes-Hall as a director.

==Overview==
A Parisian artist becomes addicted to the liquor absinthe and sinks to robbery and murder.

==Cast==
*King Baggot as Jean Dumas
*Leah Baird as Madame Dumas

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 

 