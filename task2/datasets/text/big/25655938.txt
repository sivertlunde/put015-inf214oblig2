I Was a Mail Order Bride
{{Infobox film
| name           = I Was a Mail Order Bride
| image          = 
| image_size     = 
| caption        =
| director       = Marvin J. Chomsky
| producer       = Andrew Hill Michael Jaffe Ervin Zavada
| writer         = Stephen Zito
| narrator       =  Ted Wass
| music          = John Addison
| cinematography = Donald H. Birnkrant
| editing        = James T. Heckert
| studio         =
| distributor    = CBS
| released       = December 14, 1982
| runtime        = 92 minutes USA
| English
| budget         =
| gross          = 
| preceded_by    = 
| followed_by    = 
}} 1982 television film|made-for-TV romantic comedy film directed by Marvin J. Chomsky.

== Plot ==
Kate Tosconi is a journalist in her early 20s working in Chicago for a womens magazine called Contemporary Woman Magazine. Having a special interest for trains, she is enthusiastic to do an article on rail transport. However, her boss Dottie Birmington only allows her to do the piece if she also does an article on mail-order brides. She reluctantly places an advertisement, which is responded by Robert Fitzgerald.

Robert is a successful business man in L.A. who constantly competes with his business partner Joe Kimbel. For his latest bet, Robert is challenged to take in a mail-order bride and not have sex with her for two weeks. Soon, Kate packs her bags and travels to Los Angeles, where she takes an immediate interest in Robert. She notices that he is a nice guy and feels guilty about using him for an article. They eventually fall in love with each other and Kate cancels the article.

While drunk one night, Robert is taken home by Kate. She attempts to seduce him, but he falls asleep. The next day, she jokes about having had sex with him. Robert, thinking he has lost the bet, admits to the whole truth. Kate is furious about having been a part of a bet and refuses to believe his claims that he has fallen in love with her in the meantime. She immediately returns home and writes an offensive article on Robert. When he reads the article, he is outraged and sues her for Defamation|libel.

In court, they are initially mad at each other, but they soon realize they still love each other. Robert wins the case, after which Kate has to give him a written apology. After doing this, she returns home. Robert, however, is encouraged by Kates father to go after her and he is able to climb on the train. In the end, they kiss each other.

==Cast==
*Valerie Bertinelli as Katarina Kate Tosconi Ted Wass as Robert Fitzgerald
*Kenneth Kimmins as Joe Kimbel
*Karen Morrow as Eve Whister
*Holland Taylor as Dottie Birmington
*Sam Wanamaker as Frank Tosconi Jack Collins as Archibald Trucker
*Jason Bernard as Judge
*Judith Baldwin as Rita Kimbel

==Reception==
Reviewer Fred Rothenberg wrote that the film was "a standard television vehicle" and "totally implausable, halfway diverting". "Mediocrity batters Mail Order Bride" by Fred Rothenberg. Lawrence Journal-World, December 14, 1982. p. 27  He furthermore complained about Wass and Bertinellis characters "charade reminiscent in style, but not substance", and continued: "The premise is weak but has enough fluffy charm to sustain early attention. Its never an intelligent farce, though, opting for pie-in-the-face philosophy rather than cleverness.   One major problem of the film is that this unconventional farce too quickly forgets the building relationship for the dissolving one." 

==References==
 

==External links==
* 

 

 
 
 
 
 
 