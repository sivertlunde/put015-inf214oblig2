First Love (1939 film)
{{Infobox film
| name           = First Love
| image          = First_Love_1939_Poster.jpg
| border         = yes
| caption        = Theatrical release poster
| director       = Henry Koster
| producer       = Henry Koster Joe Pasternak
| writer         = 
| screenplay     = {{Plainlist|
* Lionel Houser
* Bruce Manning
}}
| story          = {{Plainlist|
* Henry Myers
* Gertrude Purcell
}}
| starring       = Deanna Durbin
| music          = Hans J. Salter
| cinematography = Joseph A. Valentine
| editing        = Bernard W. Burton
| studio         = Universal Pictures
| distributor    = Universal Pictures
| released       =  
| runtime        = 84 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
First Love is a 1939 American musical film directed by Henry Koster and starring Deanna Durbin.       Based on the fairy tale Cinderella, the film is about an orphan who is sent to live with her wealthy aunt and uncle after graduating from boarding school. Her life is made difficult by her snobby cousin who arranges that she stay home while the rest of the family attends a major social ball. With the help of her uncle, she makes it to the ball, where she meets and falls in love with her cousins boyfriend. The film received Academy Award nominations for Best Art Direction, Best Cinematography, and Best Music.   

==Plot==
Constance Harding is an unhappy orphan who will soon graduate from Miss Wiggins school for girls. Her only real relatives are members from the James Clinton family, but they show little interest in the teenager. She is brought to New York by one of their butlers, where she moves in with a bunch of snobs. The upperclass people are not impressed with her, but Connie is able to befriend the servants.

One afternoon, her cousin Barbara Clinton orders Connie to stop Ted Drake from going riding without her. Connie tries the best she can, which results into embarrassing herself. She has secretly fallen in love with him and is full with joy when she learns the Drake family is organizing a ball. The servants raise money to buy her a fashionable dress. However, Barbara spreads a lie and Connie is eventually prohibited from attending the ball.

Connie is heartbroken, until the servants arrange a limousine she can use until midnight. At the ball, everyone is impressed with her singing talents. Ted notices her and tries to charm her. They eventually kiss, when Connie realizes it is midnight. She runs off, but accidentally leaves one of her slippers behind. Ted finds the slipper and tries to locate the owner.

Meanwhile, Barbara has found out Connie was at the ball. Infuriated, she tries to break Connies confidence and fires all the servants. The next day, Connie is missing as well. She returns to Miss Wiggins school in the hope of becoming a music teacher. Ted follows her and they reunite in the end.

==Cast==
* Deanna Durbin as Constance (Connie) Harding
* Robert Stack as Ted Drake
* Eugene Pallette as James F. Clinton
* Helen Parrish as Barbara Clinton
* Lewis Howard as Walter Clinton
* Leatrice Joy as Grace Shute Clinton
* June Storey as Wilma van Everett
* Frank Jenks as Mike the Cop
* Kathleen Howard as Miss Wiggins
* Thurston Hall as Anthony Drake
* Marcia Mae Jones as Marcia Parker
* Samuel S. Hinds as Mr. Parker
* Doris Lloyd as Mrs. Parker Charles Coleman as George, Clintons Butler
* Jack Mulhall as Terry
* Mary Treen as Agnes, Barbaras Maid
* Dorothy Vaughan as Ollie, Mrs. Clintons Maid
* Lucille Ward as Clintons Cook   

==Reception==
In his review in The New York Times, Frank S. Nugent wrote that the film "affords the usual pleasant scope for the talents, graces and charming accomplishments of Miss Deanna Durbin."    Nugent continued:
 

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 