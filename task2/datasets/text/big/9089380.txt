La Cage aux Folles (film)
{{Infobox film
| name           = La Cage aux Folles
| image          = La Cage aux Folles (film).jpg
| image_size     =  
| alt            = 
| caption        = French release poster
| director       = Édouard Molinaro
| producer       = Marcello Danon
| screenplay     = Édouard Molinaro Francis Veber Marcello Danon Jean Poiret
| based on       =  
| starring       = Ugo Tognazzi Michel Serrault
| music          = Ennio Morricone   
| cinematography = Armando Nannuzzi
| editing        = Monique Isnardon Robert Isnardon
| distributor    = United Artists
| released       =  
| runtime        = 97 minutes
| country        = France Italy
| language       = French Italian FRF 7,000,000  ($1.4 million USD) 
| gross          = $20,424,259 
}} La Cage aux Folles by Jean Poiret. It is co-written and directed by Édouard Molinaro and stars Ugo Tognazzi and Michel Serrault.  In Italian it is known as Il vizietto.

==Plot== gay couple drag entertainment, and Albin Mougeotte (Michel Serrault), his star attraction – and the madness that ensues when Renatos son, Laurent (Rémi Laurent), brings home his fiancée, Andrea (Luisa Maneri), and her ultra-conservative parents (Carmen Scarpitta and Michel Galabru) to meet them.

==Cast==
* Ugo Tognazzi as Renato Baldi (voiced in French by Pierre Mondy)
* Michel Serrault as Albin Mougeotte/Zaza Napoli (voiced in Italian by Oreste Lionello)
* Claire Maurier as Simone
* Rémi Laurent as Laurent Baldi
* Carmen Scarpitta as Louise Charrier
* Benny Luke as Jacob
* Luisa Maneri as Andrea Charrier
* Michel Galabru as Simon Charrier

==Reception==
For years, the film remained the   1 foreign film to be released in the United States;   , it is   10. 

The film holds a 100% "Fresh" rating on Rotten Tomatoes. 
 Best Adapted Best Costume Design, but it didnt win in any category.

==Sequels and remake==
 
The film was followed by two sequels:   (1985), directed by Georges Lautner.
 of the same name based on the play and the film was also successful.

In 1996, an American remake titled The Birdcage, directed by Mike Nichols, was released, relocated to South Beach, Miami, and stars Robin Williams and Nathan Lane.

==Adam and Yves== pitched the concept of a weekly series about a gay couple similar to the one in the film to American Broadcasting Company|ABC. His planned title was Adam and Yves, a play on both Adam and Eve and a slogan used by some anti-gay groups. After months in development, Arnold realized that the concept was unsustainable as a weekly series, which led to the show getting dropped. 

==References==
 
*  

==External links==
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 