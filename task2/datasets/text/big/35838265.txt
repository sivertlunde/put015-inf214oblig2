Number 111 (1919 film)
{{Infobox film
| name           = Number 111
| image          = 
| image_size     = 
| caption        = 
| director       = Alexander Korda
| producer       = Alexander Korda
| writer         = Jenő Heltai (novel)   László Vajda  
| narrator       = 
| starring       = Gábor Rajnay María Corda Gyula Bartos   Lila Gacs
| music          = 
| editing        = 
| cinematography = István Eiben
| studio         = Corvin Film
| distributor    = 
| released       = 1919
| runtime        = 
| country        = Hungary Silent  Hungarian intertitles
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} Hungarian silent silent thriller White Terror. remade in 1938.

==Cast==
* Gábor Rajnay - Ivashiro 
* María Corda - Olga / Vera 
* Gyula Bartos - Sidney Balbrock 
* Lila Gacs - Mabel 
* Jenő Törzs - Baron Vásárhelyi 
* Dezső Kertész
* Jenő Balassa   
* Bäby Becker   
* Sándor Dániel

==Bibliography==
* Kulik, Karol. Alexander Korda: The Man Who Could Work Miracles. Virgin Books, 1990.

==External links==
* 

 

 

 
 
 
 
 
 
 
 