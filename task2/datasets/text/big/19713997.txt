Dog Daze
 
:Not to be confused with the 1937 Merrie Melodies cartoon of the same name.

{{Infobox film
| name           = Dog Daze
| image_size     =
| image	=	Dog Daze FilmPoster.jpeg
| caption        =
| director       = George Sidney
| producer       = MGM
| writer         = Alfred Giebler
| narrator       =
| starring       =
| music          =
| cinematography = Harold Marzorati
| editing        = Tom Biggart MGM
| released       =  
| runtime        = 10 34"
| country        = United States
| language       = English
| budget         =
}}
 short comedy film directed by George Sidney.  It was the 181st Our Gang short (182nd episode, 93rd talking short, 94th talking episode, and 13th MGM produced episode) that was released.

==Plot==
The gang must raise 37 cents to pay off Butch. After earning a dollar for taking care of an injured dog, the kids hit upon a sure-fire moneymaking scheme; they will "rescue" every dog in town, thereby collecting a dollar from each grateful owner. Naturally, the pet owners are upset when their pooches mysteriously disappear, and before long the gang is in hot water with the cops.   









==Cast==
* Darla Hood as Darla Eugene Lee as Porky
* George McFarland as Spanky
* Carl Switzer as Alfalfa
* Billie Thomas as Buckwheat
* Scotty Beckett as Wilbur

===Additional cast===
* Tommy Bond as Butch
* Sidney Kibrick as Woim
* Wade Boteler as Precinct Officer Riley
* Lee Phelps as Officer Sweeney
* John Power as Captain Pindle

==See also==
* Our Gang filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 