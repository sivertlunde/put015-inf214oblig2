Under Suspicion (1991 film)
 
 
{{Infobox film
| name           = Under Suspicion
| image          = 
| image_size     = 
| caption        =  Simon Moore
| writer         = 
| narrator       = 
| starring       = Liam Neeson Laura San Giacomo
| music          = Christopher Gunning
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Sony Pictures Entertainment|Columbia-TriStar
| released       = 28 February 1992 (US)
| runtime        = 99 mins.
| country        = United Kingdom and United States
| language       = English
| budget         = 
| gross          =  $221,295 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}} Simon Moore. It stars Liam Neeson and Laura San Giacomo.  Neeson won best actor at the 1992 Festival du Film Policier de Cognac for his performance.  The film mostly had negative reviews and currently holds a 25% on Rotten Tomatoes based on eight reviews. 

==Cast==
*Liam Neeson as Tony Aaron
*Laura San Giacomo as Angeline
*Kenneth Cranham as Frank
*Maggie ONeill as Hazel Aaron Kevin Moore as Barrister

==References==
 

==External links==
* 

 
 
 
 
 


 