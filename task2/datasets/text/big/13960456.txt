What a Way to Go: Life at the End of Empire
 
{{Infobox film
| name           = What A Way To Go: Life at the End of Empire
| image          = Wwtg cvr.jpg
| image size     = 
| director       = Timothy S. Bennett
| producer       = Sally Erickson
| writer         = Timothy S. Bennett
| narrator       = Timothy S. Bennett William Catton, Paul Roberts, William Schlesinger
| music          = Original score by Chamber Corps (Chris Rossi and James Hepler) “Let’s Build a Boat” Written and Performed by Brian Hall
| distributor    = VisionQuest Pictures
| released       = 2007
| runtime        = 123 min
| country        = United States English
}}

What A Way To Go: Life at the End of Empire is a 2007 documentary film about the current situation facing humanity and the world. 
 population overshoot and species extinction, as well as how this situation has developed. The documentary features supporting data and interviews of Daniel Quinn, environmental activist Derrick Jensen and academics such as Richard Heinberg and many others.

The tagline of the documentary is, "A Middle class in the United States|middle-class white guy comes to grips with Peak Oil, Climate Change, Mass Extinction, Population Overshoot and the demise of the American lifestyle."

==See also==
*Holocene extinction
*Extinction debt

== External links ==
*  
*   and  
*   on The Social Contract Press
*  
*  

=== Related films ===
*  
*  
*    

 

 
 
 
 
 
 
 
 
 


 