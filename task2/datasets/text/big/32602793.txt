Dimensions (film)
{{Infobox film
| name           = Dimensions
| image          = Dimensions Title.jpg
| alt            =  
| caption        = Title Art
| director       = Sloane U’Ren
| producer       = Sloane U’Ren  Ant Neely
| screenplay     = Ant Neely
| starring       = Henry Lloyd-Hughes Camilla Rutherford Patrick Godfrey  Olivia Llewellyn   Sean Heart
| music          = Ant Neely
| cinematography = Simon Dennis
| editing        = Adam Garstone
| studio         = Sculptures of Dazzling Complexity Ltd
| distributor    = 
| released       =  
| runtime        = 99 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
Dimensions is a 2011 science fiction-love story film set in the 1920s and 1930s. The film was directed by Sloane U’Ren and written by Ant Neely, who are a married couple. The film is U’Ren’s feature film directorial debut and is also known as Dimensions:  A Line, A Loop, A Tangle of Threads.
 Tinker Tailor The Help and Midnight in Paris.   After the Cambridge screenings, the film underwent a minor re-edit and was finished in early 2012. Dimensions was voted Best Film 2012 at the 37th Boston Science Fiction Film Festival and awarded the Gort Award.  Previous Gort Award winners have included Duncan Jones Moon (film)|Moon.  The film went on to win Best Film at the London Independent Film Festival   and Best Film at the Long Island International Film Expo.  Director Sloane URen was also awarded Best Director at the Long Island International Film Expo.

==Plot==
The film follows Stephen, a brilliant young scientist who lives in Cambridge, England, in what appears to be the 1920s.  His world is turned upside down upon meeting a charismatic and inspirational professor at a garden party, who demonstrates to Stephen and his friends what life would be like if they were one-, or two-dimensional beings.  He then proceeds to explain that by manipulating other dimensions, time travel may actually be possible.

Soon after the professors visit, Stephan,his cousin, Conrad, and his neighbor, Victoria, were fooling around by a well. Conrad throws Victorias skipping rope down the well. The nanny catches the boys rolling around fighting and drags them in the house by the ears leaving Victoria alone to play outside by herself. After some time, she decides to climb down the well to get her skipping rope. She never climbs out of the well and her body is never found.

As Stephen’s life unfolds, events lead him to dedicate himself to turning the Professor’s theories of time travel into reality.  Jealousy, love, obsession, temptation and greed surround him, influencing his fragile mind and the direction of his work.

==Cast==
* Henry Lloyd-Hughes as Stephen
* Camilla Rutherford as Jane
* Patrick Godfrey as The Professor
* Olivia Llewellyn as Annie
* Sean Heart as Conrad
* Richard Heffer as Dr. Schmidt
* Georgina Rich as Alice
* James Greene as Old Man
* Sam Harrison as Young Stephen
* George Thomas as Young Conrad
* Hannah Carson as Victoria

==Production==
Sloane URen and Ant Neely have worked in the film industry for many years on large Hollywood films and television productions,  URen as art director and designer with Neely as a composer and musician.   The couple wanted to make their own film for many years and rather than wait for a large studio to invest decided to produce the film independently, Neely said, “We could wait for someone to come knock on our door and get old waiting, or we could just go and do it.” 

URen and Neely sold their house to help finance the production, a move that caused some media attention in the UK.  

Principal photography started on 26 July 2010 and ended on 21 August 2010.  Dimensions joins a small but growing list of feature films shot entirely using DSLR cameras, at times the film used three Canon 5D Mk II and a Canon 7D.

The film was shot entirely in and around Cambridge, England including Downing College, Cambridge University. Sloane URen and Ant Neely, along with almost all of the cast, attended the world premiere: they introduced themselves before the screening, and answered questions about the project and how it was completed afterwards.

==Music==
The original score was composed by Neely and recorded at British Grove Studios with the London Metropolitan Orchestra.

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 