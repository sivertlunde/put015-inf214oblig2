The Girl from Petrovka
{{Infobox film
| name           = The Girl from Petrovka
| image          = Girl from petrovka movie poster.jpg
| image size     =
| caption        = 1974 movie poster
| director       = Robert Ellis Miller David Brown Richard D. Zanuck Allan Scott based on the novel by George Feifer
| starring       = Goldie Hawn Hal Holbrook Anthony Hopkins
| music          = Henry Mancini
| cinematography = Vilmos Zsigmond
| editing        =
| distributor    = Universal Pictures
| released       =  
| runtime        = 103 min.
| country        = United States
| awards         =
| language       = English
| budget         =
}}

The Girl from Petrovka is a 1974 feature film starring Goldie Hawn and Hal Holbrook, based on the novel by George Feifer.

==Cast==
* Goldie Hawn as Oktyabrina  
* Hal Holbrook as Joe  
* Anthony Hopkins as Kostya  
* Grégoire Aslan as Minister  
* Anton Dolin as Ignatievitch  
* Bruno Wintzell as Alexander  
* Zoran Andric as Leonid  
* Hanna Hertelendy as Judge  
* Maria Sukolov as Old Crone  
* Zitto Kazann as Passport Black Marketeer  
* Inger Jensen as Helga Van Dam  
* Raymond OKeefe as Ministers Driver  
* Richard Marner as Kremlin Press Official  
* Michael Janisch as Police Chief Valinikov  
* Harry Towb as American Reporter

==Plot==
Joe (Hal Holbrook) is a cynical American journalist assigned to work in the Soviet Union, where he meets Oktyabrina (Goldie Hawn), a spirited and erratic Russian ballet dancer who lives illegally without proper documents. Their ensuing romance opens new possibilities for both; but also draws the attention of the Soviet authorities.

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 


 