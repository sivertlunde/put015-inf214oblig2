Chithirai Pookkal
{{Infobox film
| name           = Chithirai Pookkal
| image          = 
| image_size     =
| caption        = 
| director       = Kanmani Subbu
| producer       = Kamatchi Thamizhmani Yasodha Thamizhmani K. Chidhambaram
| writer         = Kanmani Subbu
| starring       =  
| music          = M. S. Murali
| cinematography = M. V. Ramkrishnan
| editing        = K. R. Ramalingam
| distributor    =
| studio         = Nachiyar Movies
| released       =  
| runtime        = 140 minutes
| country        = India
| language       = Tamil
}}
 1991 Tamil Tamil romance film directed by Kanmani Subbu. The film features newcomers Jayanthkumar and Vinodhini in the lead roles, with R. Sarathkumar, Radha Ravi, Charle, S. S. Chandran and Vinu Chakravarthy playing supporting roles. The film had musical score by M. S. Murali and was released on 23 February 1991.   

==Plot==

Hari (Jayanthkumar) and his parents (Vinu Chakravarthy and Vaani) as well as Bharathi (Vinodhini) and her parents (S. S. Chandran and Kovai Sarala) settle for the holidays in the same hotel in Ooty. Hari and Bharathi gets into several quarrels until they fall in love with each other. Their parents have come to know their love affair and refuse to unite them. Later, the lovers elope and they try to commit suicide. Johnson David (R. Sarathkumar), a retired military officer, saves them at time and he accommodates the young lovers. What transpires later forms the crux of the story.

==Cast==

*Jayanthkumar as Hari
*Vinodhini as Bharathi
*R. Sarathkumar as Johnson David
*Radha Ravi as Lollai Kaadu
*Charle
*S. S. Chandran as Mayil Vahanam, Bharathis father
*Vinu Chakravarthy as Haris father
*Kovai Sarala as Bharathis mother
*Vaani as Haris mother
*Dr. Nadesan
*Master Kiruba
*Joker Thulasi
*Karmegham

==Soundtrack==

{{Infobox Album |  
| Name        = Chithirai Pookkal
| Type        = soundtrack
| Artist      = M. S. Murali
| Cover       = 
| Released    = 1991
| Recorded    = 1991 Feature film soundtrack |
| Length      = 23:45
| Label       = 
| Producer    = M. S. Murali
| Reviews     = 
}}

The film score and the soundtrack were composed by film composer M. S. Murali. The soundtrack, released in 1991, features 7 tracks with lyrics written by Vaali (poet)|Vaali, Ilavenil, Kalaivanan Kannadasan, Maruthi and Kanmani Subbu.   

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Duration
|- 1 || Aanandh Geedhangal || Uma Ramanan || 5:00
|- 2 || Aranam Un Sanidhanam || Mano (singer)|Mano, Uma Ramanan || 4:57
|- 3 || Do You Love Me || Mano, Uma Ramanan || 4:17
|- 4 || Mandhira Punnagai || Mano || 4:58
|- 5 || Onnu Rendu Moonu ||  Mano, Uma Ramanan || 4:10
|- 6 || Sangeetham Kadal || Mano || 4:02
|- 7 || Vaadi My dear Lady || Mano || 5:02
|} 	

==References==
 

 
 
 
 