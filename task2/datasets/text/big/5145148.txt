House IV
{{Infobox film
| name           = House IV
| alt            =
| image          = House4cover.jpg
| caption        = Promotional poster
| director       = Lewis Abernathy
| producer       = Sean S. Cunningham Debbie Hayn-Cass
| screenplay     = Geoff Miller Deirdre Higgins
| story          = Geoff Miller Deirdre Higgins Jim Wynorski R. J. Robertson
| starring       = Terri Treas William Katt Scott Burkholder Denny Dillon Melissa Clayton Dabbs Greer Ned Romero Ned Bellamy
| studio         = New Line Cinema
| released       = 29 January 1992
| runtime        = 94 minutes
| country        = United States
| language       = English
| cinematography = James Mathers
| music          = Harry Manfredini
| budget         = $1,600,000  (estimated) 
}}

House IV is a 1992  , but the film otherwise does not connect its storyline to the first film. Kane Hodder was the stunt cordinator on the film.

==Synopsis==
 Mafia real estate developers, without any success. Roger is soon killed in a car accident that leaves Laurel requiring a wheelchair, and Burke is unable to convince Kelly to sell the house.

Various supernatural events start occurring in the house, and after Kelly consults with a native American spiritual guide, she learns that the spirit of Roger and some Indians have been trying to warn Kelly that Rogers tragic car accident was in fact cold-blooded murder and that Burke is trying to sell the land to the Mafia so that it can be used for the illegal dumping of toxic waste.

==Reception==
 AllMovie wrote, "this installment marks a slightly more effective return to the horror comedy formula that made the original a surprise hit",  the film currently holds a low 3.3 out of 10 user rating on the Internet Movie Database based on 1,454 ratings. 

==References==

 

==External links==

*  

 

 
 
 
 
 
 
 