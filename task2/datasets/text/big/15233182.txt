Following Sean
{{Infobox Film | name = Following Sean 
| image          =
| caption        = 
| director       = Ralph Arlyck
| producer       = Ralph Arlyck, Malcolm Pullinger
| writer         = Ralph Arlyck
| starring       = 
| music          = Eric Neveux
| cinematography = 
| editing        = Malcolm Pullinger
| distributor    = 
| released       = 
| runtime        = 
| language       = English
| budget         =  
}}

Following Sean is a 2005 documentary film directed by Ralph Arlyck, and a follow-up to his 1969 student short "Sean," which features four-year-old Seans thoughts on marijuana, police presence, and freewheeling lifestyles. The films notoriety landed a screening in the White House and a variety of predictions regarding the outcome of Seans life - whether he could grow up to embody the hippy philosophy, or whether he would turn out a drug dealer or stock broker. 

Following Sean picks up in the mid-1990s and turns Seans story into a meditation on generational changes and legacies that are handed down as a result of choices made in heated political climates. The film was met with high critical praise, receiving an 86% "Fresh" rating on Rotten Tomatoes  and a 64 on Metacritic. 

== References ==
 

==External links==
*  
*  
*  
*  
*   - PBSs site dedicated to the film

 
 
 
 
 
 

 