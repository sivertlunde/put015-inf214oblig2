Parisian Life (1936 film)
{{Infobox film
| name =Parisian Life
| image =
| image_size =
| caption =
| director = Robert Siodmak
| producer = Seymour Nebenzal
| writer =  Henri Meilhac (libretto)   Ludovic Halevy (libretto)   Michel Carré   Benno Vigny   Emeric Pressburger   Marcel Carné
| narrator =
| starring = Max Dearly   Conchita Montenegro   George Rigaud
| music = 
| cinematography = Michel Kelber   Armand Thirard  
| editing = Ernest Hajos 
| studio = Nero Film
| distributor = Gaumont Film Company
| released = 22 January 1936  
| runtime = 95 minutes
| country = France French 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Parisian Life (French:La vie parisienne) is a 1936 French musical film directed by Robert Siodmak and starring Max Dearly, Conchita Montenegro and George Rigaud. 
 Jacques Colombier. A separate Parisian Life|English-language version was also produced. The production was not a success, causing financial problems for the company.

==Cast==
* Max Dearly as Ramiro Mendoza  
* Conchita Montenegro as Helenita  
* George Rigaud as Jacques Mendoza  
* Christian Gérard as Georges  
* Germaine Aussey as Simone  
* Marcelle Praince as Liane dYsigny 
* Bergeol 
* Valentine Camax 
* Roger Dann 
* Maurice Devienne 
* Gaston Dupray 
* Enrico Glori 
* Jacques Henley 
* Richard Lamy 
* Georges Morton 
* Jean Périer 
* Claude Roussell 
* Germaine Sablon 
* Sinoël
* Michèle Morgan as Extra  
* Austin Trevor as Don Joâo

== References ==
 

== Bibliography ==
* Bock, Hans-Michael & Bergfelder, Tim. The Concise CineGraph. Encyclopedia of German Cinema. Berghahn Books, 2009.

== External links ==
 

 

 
 
 
 
 
 
 


 