The Big Fix
 

{{Infobox film
| name           = The Big Fix
| image          = Big fix.jpg
| caption        = Theatrical release poster
| director       = Jeremy Kagan
| producer       = Carl Borack Richard Dreyfuss
| writer         = Roger L. Simon
| starring       = Richard Dreyfuss Susan Anspach Bonnie Bedelia John Lithgow Ofelia Medina Nicolas Coster F. Murray Abraham Fritz Weaver
| music          = Bill Conti Frank Stanley
| editing        = Patrick Kennedy
| distributor    = Universal Pictures
| released       =  
| runtime        = 108 minutes
| country        = United States
| language       = English
| budget         =
}}
The Big Fix is a 1978 film directed by Jeremy Kagan and based on the novel by Roger L. Simon, who also wrote the screenplay. It starred Richard Dreyfuss as private detective Moses Wine and co-starred Susan Anspach and John Lithgow. The Big Fix had no relationship to the 1947 film or the 2012 film of the same name.

==Plot== Berkeley activist with whom he shared "the barricades."
 radical named Howard Eppis &mdash; a fugitive who had been convicted in absentia for inciting violence against the government &mdash; falsely claiming that Eppis is supporting him for governor.  This libel is a clear attempt to destroy Hawthornes chances for being elected.

Moses sets out to find out who is responsible &mdash; with deadly results.

==Cast==
* Richard Dreyfuss as Moses Wine
* Susan Anspach as Lila Shay
* Bonnie Bedelia as Suzanne
* John Lithgow as Sam Sebastian
* Ofelia Medina as Alora
* Nicolas Coster as Spitzer
* F. Murray Abraham as Howard Eppis
* Fritz Weaver as Oscar Procari, Sr.

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 


 