The Daddy of Rock 'n' Roll
 
{{Infobox film
| name           = Wesley Willis: The Daddy of Rock n Roll
| image          = Daddy Rock Roll DVD cover.jpg
| caption        = The DVD cover.
| director       = Daniel Bitton
| producer       = Daniel Bitton
| writer         =
| narrator       =
| starring       = Wesley Willis
| music          = Wesley Willis
| cinematography =
| editing        =
| distributor    = Music Video Distributors
| released       =  
| runtime        = 58 minutes
| country        = United States
| language       = English
| budget         =
}} rock musician and artist Wesley Willis, who died in 2003 at age 40. Willis, challenged by a weight disorder as well as paranoid schizophrenia, is followed in his daily tasks, trials and tribulations in autumn 2000, showing him writing his unique yet virtually identical songs, playing a show, and attending to daily chores.

== Plot ==
Shot in and around Willis native Chicago area, the documentary simply shows footage of Willis daily life with his songs serving as the soundtrack. Willis appears unkempt, morbidly obese and mentally unstable.

His songwriting technique is shown as he visits a local FedEx Kinkos|Kinkos dressed sloppily and barefoot, and types expletives and repetitive bestiality references on a computer as lyrics and prints the document. His songs deal with subjects such as praise for celebrities that he had met or admired, reports on bands that he had seen, stories about violent confrontations with superheroes, and expletive-packed rants at his mental "demons". Willis says music helps him silence the discouraging mental voices, but his fan following comes from people who find his songs humorous. Many of his compositions are very similar, a fact that is highlighted by scenes of him recording songs such as "I Whipped Supermans Ass", "I Whipped Spider-Mans Ass" and "I  Whupped Batmans Ass", which are overlaid to highlight their similarities.

Willis also visits a zoo, overdubbed with one of his many songs about bestiality.

Towards the end Willis is shown playing with his band, the Wesley Willis Fiasco.

Several of Willis friends are interviewed, among them recording engineer Steve Albini. They reminisce over how they met Willis and how they feel about him. Generally they are all sympathetic about his plight and it is stated he would likely be dead if he had not cultivated such a network.

One friend describes Willis poor upbringing (involving forced viewing of his mother having sex in exchange for drugs), and touchingly, Willis own despair at his condition:

"Im already doomed. I cant find a girlfriend, I cant do a goddamned thing."

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 


 