Far och flyg
{{Infobox Film
| name           = Far och flyg
| image          = 
| image size     = 
| caption        = 
| director       = Gösta Bernhard
| producer       = Lars Burman
| writer         = Gösta Bernhard
| narrator       = 
| starring       = Dirch Passer
| music          = 
| cinematography = Jan Lindeström
| editing        = Carl-Olov Skeppstedt
| distributor    = 
| released       = 21 February 1955
| runtime        = 74 minutes
| country        = Sweden 
| language       = Swedish
| budget         = 
| preceded by    = 
| followed by    = 
}}

Far och flyg is a 1955 Swedish film directed by Gösta Bernhard and starring Dirch Passer.

==Cast==
* Dirch Passer - Peder
* Åke Grönberg - Hagfors
* Irene Söderblom - Agneta Jansson
* Georg Adelly - Albin
* Rut Holm - Mrs. Hermansson
* Arne Källerud - Dröm
* Curt Åström - Åkerlind
* Sven Holmberg - Pilot at restaurant
* Stig Johanson - Prof. Schmultz
* Gunnar Lindkvist - Lindström
* Sven Lykke - Passenger / Gangster in Peders dream
* John Melin - Saint Peter in Peders dream
* Gösta Bernhard - Professor Edvardsson - archeologist (uncredited)
* Curt Randelli - Matildas parrot (voice) (uncredited)
* Ib Schønberg - Autograph hunter (uncredited)

==External links==
* 

 
 
 
 
 
 