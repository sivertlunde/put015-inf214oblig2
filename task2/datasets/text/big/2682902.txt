Missing in Action (film)
{{Infobox film
| name = Missing in Action
| image = Missing in action (film poster).jpg
| director = Joseph Zito
| producer = Menahem Golan Yoram Globus Lance Hool
| writer = Arthur Silver Larry Levinson John Crowther Lance Hool James Bruner Steve Bing (characters)
| starring = {{plainlist|
* Chuck Norris
* M. Emmet Walsh
* Lenore Kasdorf
* James Hong
* David Tress
}}
| music = Jay Chattaway
| editing = Joel Goodman Daniel Lowenthal
| cinematography = João Fernandes
| released =    MGM (current)| Warner Bros Pictures América Vídeo (VHS) 1987
 runtime = 101 min
| country  = United States Vietnamese
 imdb_id = 0087727
| budget = $1.5 million Andrew Yule, Hollywood a Go-Go: The True Story of the Cannon Film Empire, Sphere Books, 1987 p58 
| gross = $22,812,411 
}}

Missing in Action is a 1984   (1985) and a   (1988).

Missing in Action 2 was filmed back to back with Missing in Action, and was actually set to be released first before the producers changed their minds.  

Despite the overwhelmingly negative reception, the film was a commercial success and has become one of Chuck Norris most popular films.

== Plot ==
Colonel James Braddock is an American officer who spent seven years in a North Vietnamese POW camp, then escaped 10 years ago. After the bloodiest war, Braddock accompanies a government investigation team that goes to Ho Chi Minh City to check out reports of Americans still held prisoner. Braddock gets the evidence then travels to Thailand, where he meets Tuck, an old Army buddy turned black market kingpin. Together, they launch a mission deep into the jungle to free the American POWs from General Trau.

==Cast==
*Chuck Norris - Colonel James Braddock
*M. Emmet Walsh - Tuck
*David Tress - Sen. Porter
*Lenore Kasdorf - Ann
*James Hong - Gen. Trau
*Erich Anderson - Masucci (as E. Erich Anderson)

==Reception==
Missing in Action received overwhelmingly negative reviews.   The film has been widely described as a  , even though that film was released the following year (1985). Scott Weinberg of UGO Networks|eFilmCritic.com gave the film 2 stars out of 5, writing that "Norris does Stallone... badly" in his review.    In a 2003 BBC article entitled "Rambo: Pretenders to the Throne",  Almar Haflidason wrote "the runaway success of the Rambo trilogy inspired dozens of rip-offs", citing that the Missing in Action series was the most famous of the Rambo clones.  
 Time Out Rovi described the film as a "crass, dopey Rambo-esque film that ultimately fails to connect with anything interesting in the realm of fact or fiction" and that its "chop-socky, shoot-em-up, explosion-a-minute action quickly wears thin".  Steve Crum of Video-Reviewmaster.com wrote that MIA was "Chuck Norris best film, and that isnt saying much".  The film currently holds a 23% "Rotten" rating on the review aggregate website Rotten Tomatoes. 

The film earned Cannon a profit of $6.5 million on the basis of its US release alone. 

==References==
 

==External links==
* 
* 

 

 

 
 
 
 
 
 
 
 
 
 
 
 