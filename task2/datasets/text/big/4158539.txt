Who's the Man?
{{Infobox film
| name           = Whos the Man?
| image          = Whos the Man film poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Ted Demme
| producer       = Grace Blake
| screenplay     = Seth Greenland
| story          = Doctor Dré Ed Lover Seth Greenland Salt Badja Richard Bright
| music          = Michael Wolff Nic. tenBroek
| cinematography = Adam Kimmel
| editing        = Jeffrey Wolf John Gilroy
| studio         = de Passe Entertainment
| distributor    = New Line Cinema
| released       = April 23, 1993
| runtime        = 85 minutes
| country        = United States
| language       = English
| gross          = $11,299,730 
}}
 1993 thriller thriller comedy directing debut. The film stars Yo! MTV Raps hosts Doctor Dré and Ed Lover as its two main protagonists., it features dozens of cameo appearances from some of the top Rapping|rap/Hip hop|hip-hop acts of the time, including (though not limited to) Busta Rhymes, Bushwick Bill, Guru, Eric B., House of Pain, Ice-T, Kris Kross, Queen Latifah, KRS-One and Run-D.M.C.. This film is also the feature film debut of Terrence Howard.

==Plot== Jim Moody) tells the two maybe they should try out for the police academy. Crazily enough, it works out for the two, and they are accepted on the New York City police force. Things seem to be going well for them, when tragedy suddenly strikes, and they lose Nick.  Now enforcers of the law, the tag team decides to investigate the incident, which they believe to be a murder.
 Richard Bright) might have had something to do with their friends death, and proceed to attempt to dig up as much dirt on him as possible. This proves to be difficult, however, when theyve got a nutty Sergeant (Denis Leary), a moody detective (Rozwill Young), and a bunch of unwilling street hoods (Guru (rapper)|Guru, Ice-T) to go through to get the information they need. Though there arent any certain clues to be found, strange happenings are certainly going on, as Demetrius company seems to be digging for something rather than looking to build on all the property hes buying up in their Harlem neighborhood, and the bodies slowly continue to pile up around them.

Tagline: The first hip-hop whodunnit!
 

==Cast==
* Doctor Dré as Himself
* Ed Lover as Himself
* Badja Djola as Lionel Douglas Cheryl "Salt" James as Teesha Braxton
* Colin Quinn as Frankie Flynn
* Denis Leary as Sergeant Cooper
* Bernie Mac  as G-George
* Richard Gant as Albert Guru as Martin Lorenzo
* Ice-T as Chauncey "Nighttrain" Jackson
* Larry Cedar as Officer Barnes Jim Moody as Nick Crawford
* Joe Lisi as Captain Reilly
* Karen Duffy as Officer Day
* House of Pain as Bad Guys Richard Bright as Demetrius
* Rozwill Young as Bo Griles Vinny Pastore as Tony "Clams" Como
* Caron Bernstein as Kelly
* Angelo Montagnese as The Manno
* Michael Giordano as Not the Manno Dano
* Kim Chan as Fuji

Cameo appearances:

* B-Real as Jose/Test Taker #3
* Andre B. Blake as Lamar Apache as Bubba Worker #1
* Bill Bellamy as K.K.
* Full Force|Bow-Legged Lou as Forty
* Bushwick Bill as Bar Vagrant
* Busta Rhymes as Jawaan
* CL Smooth as Robber #2
* D-Nice as Male Nurse
* Eric B. as Robber #5 Fab 5 Freddy as Himself
* Flavor Flav as Himself
* Freddie Foxxx as Bartender
* Garfield as Customer in Chair Heavy D as Himself
* House of Pain - Card players Humpty Hump as Club Doorman
* Kid Capri as Himself
* Kris Kross as Karim/Micah
* KRS-One as Rashid
* Leaders of the New School as Passengers in Jeep
* Leslie Segar as Sheneequa
* Little Daddy Shane as Homeboy
* Melle Mel as Delroy
* Monie Love as Vanessa
* Naughty By Nature as Themselves
* Pete Rock as Robber #1
* Phife Dawg as Gerald
* Queen Latifah as Herself
* Run-D.M.C. as Detectives Sandra "Pepa" Denton as Sherise
* Smooth B as Bubba Worker #2 Stretch as Benny
* T-Money as Bubba
* Todd 1 as Shorty Yo Yo as Woman

== Reception ==
Rotten Tomatoes gives the film a score of 43% based on reviews from 7 critics. 

Roger Ebert gives a favorable review, with a score of 3 stars out of 4. 

==Soundtrack==
 
A soundtrack containing hip hop music was released on April 20, 1993 through MCA Records. It peaked at #32 on the Billboard 200|Billboard 200 and #8 on the Top R&B/Hip-Hop Albums.

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 