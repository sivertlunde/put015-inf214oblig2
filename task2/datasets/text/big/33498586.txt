Krishnanum Radhayum
 
 
{{Infobox film
| name           = Krishnanum Radhayum
| image          = KrishnanumRadhayum.jpg
| alt            =  
| caption        = 
| director       = Santhosh Pandit
| producer       = Santhosh Pandit
| writer         = Santhosh Pandit
| starring       = Santhosh Pandit Rupa Jith Souparnika Devika
| lyrics         = Santhosh Pandit
| music          = Santhosh Pandit
| cinematography = Sujith
| editing        = Santhosh Pandit
| studio         = 
| distributor    = Sree Krishna Films
| released       =  
| runtime        = 165 minutes
| country        = India Malayalam
| budget         = 
| gross          = 
}} Malayalam film,   directed and produced by Santhosh Pandit who also plays the lead role in the film and also does the lyrics, music, fights, art, editing, back-ground music, effects, singer, story, dialogues, script, costumes, production designing and title graphics.

The film was released in 3 theatres on 21 October 2011 in Kerala. The film ran housefull on the first day of release.  The film has been universally panned by the mainstream media,    

The Deccan Chronicle reported that on one of the Saturdays in November 2011, Pandit was number two on Google trends and that the searches had "crested on October 21, the day when his debut movie Krishnanum Radhayum was released and it quickly got damned by film pundits". Srikandath, Sivaram (16 November 2011)  . Manorama Online.     

==Plot==
 
This film focuses on the love life of John (Santhosh Pandit) and Radha (Souparnika) who admire each other despite belonging to different religions. They get married against the will of their families and consider leasing a house. Yeshodha and her daughter Rugmini are struggling for money and so they decide to rent a section of their house to tenants on the condition that they were Hindus. John and Radha come across this offer and to get the house, John changes his name to Krishnan. Throughout the film, John and Radha face many issues commonly concerning disagreements on their religions.

One day, Radha and John set off to assist Sreelatha, who refuses to eat her food after the death of her husband. John uses a few sneaky tactics and soon Sreelatha commences to follow a healthy diet. She calls John everyday to thank him and invites him to join her on her interview. Radha begins to get a bit suspicious and gets angry towards Sreelatha.

John has a kind natured heart and so he tends to help anyone who is in trouble. One day he saves his brother who was involved in a nasty fight. However, he gets hit in the head and collapses, ending up in hospital. When they return home, Radha sets off to buy some medicine but does not return. Radha finds herself in trouble as she gets killed by the Uncle of Rugmini . When he came to know about it, he sets off to kill Rugminis Uncle and his goons and gets into prison for murder . Life changes when he gets released .

==Music==
All songs were composed by Pandit.  Lyrics of seven songs were written by Pandit and A. S. Prasad wrote the lyrics of the song Guruvayoorappa.

===Track List===
{{tracklist
| extra_column    = Artist(s)
| title1          = Rathri Shubharathri
| extra1          = Santhosh Pandit
| length1         = 5:03
| title2          = Anganavadiyile Teachere
| extra2          = Master Navajyoth Pandit, Bhavya
| length2         = 4:15
| title3          = Ma Ma Ma Ma Ma Ma Mayavi
| extra3          = Santhosh Pandit, Nimmy
| length3         = 4:51
| title4          = Radhe Krishna
| extra4          = Vidhu Prathap
| length4         = 4:31
| title5          = Gokulanathanayi
| extra5          = Santhosh Pandit, Bhavya, Praseetha
| length5         = 3:47
| title6          = Sneham Sangeetham
| extra6          = M. G. Sreekumar
| length6         = 5:04
| title7          = Dehiyilla
| extra7          = M. G. Sreekumar
| length7         = 5:33
| title8          = Guruvayoorappa
| note8           = Lyrics: A. S. Prasad
| extra8          = K. S. Chithra
| length8         = 4:10
}}

==References==
 

==External links==
*  

 
 
 
 
 