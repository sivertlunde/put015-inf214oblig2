Fish Meat
 

{{Infobox film
| name           = Fish Meat
| image          = 
| caption        = Fish Meat poster
| director       = Joe Cunningham
| producer       = Ted Caplow
| associate producer       = Sarah Curry
| writer         = Ted Caplow
| starring       = Ted Caplow  Andy Danylchuk
| sound          = Joe Cunningham
| cinematography =Stuart Culpepper
| editing        = 
Ann Foo 
Javier Mayol
| studio         =  
| released       =  
| runtime        = 51 minutes
| country        = United States
| language       = English
}}
 Idyllwild Film Catalina Film Festival and.  It has also screened around the country, including at the University of Colorado  and at University of Massachusetts Amherst.  The documentary examines different methods used in modern aquaculture from Atlantic bluefin tuna ocean pens, to trout closed system farms, to old fashioned carp farms and concludes that the fish from lower in the food chain is better for sustainable aquaculture.

==Synopsis==
 ecological engineer) and Andy Danylchuk  fish ecologist) are concerned by how little consumers know about the farm-raised fish they buy. They decide to investigate the state of aquaculture by sailing around Turkey, a country known for its rich tradition in fishing, but transitioning to aquaculture.

They visit a high tech seabass farm, partly automated and equipped with video surveillance. While this is one of the more efficient ways of raising fish since the fish farmers can use the minimum amount of feed to fatten their fish, it is very expensive. It also causes a lot of waste and pollution with excess feed and fish waste falling below the cage.

Caplow and Danulchuk set out to define sustainable aquaculture in Turkey to see how it could apply to the rest of the world. They are surprised to find that the fish meal used to feed farmed fish is made of wild caught fish in South America. Between the food production and waste produced by offshore fish farms, Caplow and Danylchuck are concerned these methods are unsustainable.

The tuna ranch they visit next is by far the most modern operation they come across. Watching the harvesting process and learning about the amount of technology and fish feed required to produce a pound of Atlantic bluefin tuna, Caplow and Danylchuk conclude that it is also the least sustainable. It also removes fish from the open ocean that have not had a chance to spawn yet, which affects the wild populations.

  in Turkey at tuna ranch]]

Caplow decides that the ideal fish farm uses a minimal amount of energy, cause a minimal amount of pollution and involve growing fish that are as low on the food chain as possible.

Caplow and Danylchuk visit an inland seabass and seabream farm in the Muğla Province, that uses dug out pens supplied by a salt water spring. Not only did they find that this farm uses less energy but the fish looked and tasted better.
 Taurus mountains, Caplow is impressed by the management of their freshwater streams, recycling the water several times through each stage of the fish growing process from egg to fish before allowing it to rejoin the stream. But the problem remains that the fish from their feed comes from far away, and the trout must themselves be transported long distances to reach consumers.

Next they visit a restaurant come fish farm in the Saklikent canyon, that raises their fish directly across the street from where they are later eaten by consumers. This provides the smallest waste of energy since virtually no fuel is needed to transport the fish.

Finally, they visit a carp farm in Burdur Province, an inland farm where the fish feed very low on the food chain. The feed conversion ratio is much lower than in the carnivorous fish they had seen. The fish waste was used to grow vegetables, and the waste water flowed downstream to nourish farms.

 

Caplow and Danylchuk discuss the differences between eating from the sea and eating from land. The range of types of fish production is very broad, whereas eating say a cow is very specialized and polluting. However, the variety of fish will inevitably diminish with time as more fish species become overfishing|overfished. Aquaculture will be the only way to eat fish, and if it is done in a sustainable way it could be the answer to feeding a growing human population.

The film concludes that the least sustainable fish farming method they saw was also the newest, raising an endangered species, a disruptive fishing method, requiring a lot of fish to feed them. The most sustainable fish was also the oldest, raising an abundant species of fish feeding low on the food chain, using an ancient technique.

==Cast==

* Ted Caplow (narrator)

* Andy Danylchuk

==Versions==

A 29-minute version of the documentary was released at the beginning of 2012. A longer, Academic version was released in mid-2012 at 52 minutes.

==Reception==

The documentary has been well-received  by the scientific community  as well as by sustainable aquaculture advocates  such as celebrity chef  ).

Céline Cousteau liked the movie, saying ""I appreciate how the message was shared:  its not about being against fish farming, its about doing it right!"

==Awards and nominations==

Honorable mention at   in the Innovations and Solutions category

==References==
 

==External links==
* 
*  at the  
*  at  
* 

 
 
 