Permission to Kill
{{Infobox Film   
| name           =  Permission to Kill
| image          = Permission to Kill001.jpeg
| caption        = Original film poster
| director       = Cyril Frankel 
| producer       = Paul Mills 
| writer         = Robin Estridge
| starring       = Dirk Bogarde,  Ava Gardner,  Bekim Fehmiu
| music          = Richard Rodney Bennett 
| cinematography = Freddie Young
| editing        = Ernest Walter AVCO Embassy Pictures
| released       = 20 November 1975 (United Kingdom)
| runtime        = 93 min 
| country        = Austria, UK, USA
| awards         =  English
| budget         = 
| preceded_by    = 
| followed_by    =  1975 spy spy thriller film thriller AVCO Embassy Pictures. It was directed by Cyril Frankel and produced by Paul Mills from a screenplay by Robin Estridge. The film had original music by Richard Rodney Bennett and the cinematography was by Freddie Young.

The film stars Dirk Bogarde, Ava Gardner and Bekim Fehmiu with Timothy Dalton, Nicole Calfan and Frederic Forrest.
 American co-production and was shot on location in Gmunden, Austria.

== External links ==
*  

 
 
 
 
 
 
 
 
 


 