Marx Reloaded
{{Infobox film
| name           = Marx Reloaded
| image          = Marx Reloaded promo.jpg
| alt            =  
| caption        = 
| director       = Jason Barker
| producer       = Jason Barker Irene Höfer Andreas Schroth
| writer         = Jason Barker
| starring       = Jason Barker John N. Gray Michael Hardt Antonio Negri Nina Power Jacques Rancière Peter Sloterdijk Alberto Toscano Slavoj Žižek Ivan Nikolic
| music          = Markus Rieger
| cinematography = 
| editing        = Nebojsa Andric Stevan Djordjevic Carsten Piefke
| studio         = Films Noirs Medea Film ZDF
| distributor    = Arte
| released       =        
| runtime        = 52 minutes
| country        = Germany
| language       = English French German
| budget         = 
| gross          = 
}} British writer and theorist Jason Barker. Featuring interviews with several well-known philosophers, the film aims to examine the relevance of Karl Marxs ideas in relation to the effects of the Great Recession.      

==Background== economic and financial crisis of 2008–09.” The film also considers, in the context of an alleged revival of Marxist thinking, whether “communism might provide the solution to the growing economic and environmental challenges facing the planet”.   

In an interview with Verso Books, writer-director Jason Barker described his intention in making the film “to reload or reimagine Marx as a thinker, without the usual totalitarian moralising.” Barker criticised the “cliché” according to which “Marxs diagnoses of capitalism are validated whereas his prescription of communism is rubbished on the grounds that its utopian.” Asked whether the renewed popularity of Marx is evidence of a return of communism as a political force, or “just the spectre of Marx haunting the academies", Barker replied that "political thinking today is again converging on precisely the type of social conditions in which Marx lived." 

In a separate interview Barker also discussed the films use of animation, in particular his decision to parody The Matrix, admitting that although it was an "obvious parody" and "fun to make", there was also a philosophical dimension to the animation scenes in which Marx meets Leon Trotsky and Slavoj Žižek.   

==Film== John Gray, science fiction-action Morpheus first meets Reeves character Neo (The Matrix)|Neo.

==Reception==
Marx Reloaded had its TV premiere on Arte on 11 April and was repeated on 20 April. The film was subsequently broadcast on the Romanian television channel B1 TV on 12 August 2011,    followed by a studio debate involving political analyst Dinu Flămând, journalist Cristian Tudor Popescu and writer Vasile Ernu. 
 
On 25 September 2011 the film was screened (out of competition) at the 2011 DMZ International Documentary Film Festival.    The screening was followed by a panel discussion involving writer-director Jason Barker, Professor Taek-Gwang Lee of Kyung Hee University and Yongjune Park, the editor of Indigo, an English-language Korean humanities magazine.    Both the film and director    were the subject of national press coverage in the Hankook Ilbo.

On 3 October 2011 the film had its Serbian premiere at the Centre for Cultural Decontamination in Belgrade,    with further screenings planned in the same venue on 20, 21 and 24 October.   

Marx Reloaded premiered in the UK on 10 February 2012 at the Institute of Contemporary Arts in London, where it screened to sell-out audiences    until 20 April.   
 Time Out Little White Lies called it an "engaging hour-long talking-head-meets-animation doc"; "the film shines a light on the many causes of the financial crumble, creating a compelling dialogue of Marx’s theories on capitalism as they apply to its contemporary form."    Subtitledonline.com awarded the film three out of a possible five stars, although criticized it for its "failure to balance quirky presentation with challenging content".    However, writing in CounterPunch, Louis J. Proyect gave an unqualified endorsement: "I cant recommend this film highly enough... The film is a fast-paced and even exciting treatment of what might induce a yawn on the printed page".   

As well as reviews The Financial Times featured the film as part of its "Capitalism in Crisis" series, followed by an interview with Jason Barker and the editor of New Left Review Robin Blackburn.   

  as evidence of a resurgence of left-wing ideas.   

==Blue or Red Pill?==
 
On 16 December 2011 the first in a series of public debates entitled "Blue or Red Pill?" (Crvena ili plava pilula?) was held at the Centre for Cultural Decontamination (CZKd), Belgrade, in which the social and political themes from the film were explored. Serbian film director Želimir Žilnik – himself noted for his socially-engaged film-making, most recently in the 2009 film Stara škola kapitalizma – participated in the event along with Jason Barker.   
 Paul Mason, the blogger and The Independent journalist Laurie Penny, and Robin Blackburn joined Jason Barker. The debate considered the implications of Marxs work in the context of a growing popular resistance to the global economic and financial crisis, and whether the revolutionary change advocated by Marx had finally arrived: "Is humanity standing at a crossroads where a decision – and by whom or in whose name? – for "another world" must be taken?"   

==See also==
*Karl Marx in film
*Communism
*Marxism

==References==
;Notes
 

==External links==
* 
* 
*  
*  
* 
*  (Korean site)

 
 
 
 
 
 
 
 
 