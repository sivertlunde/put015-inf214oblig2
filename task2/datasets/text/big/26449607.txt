45365
{{Infobox film
| name           = 45365
| image          = 
| image_size     = 
| caption        = 
| director       = Bill Ross IV Turner Ross
| producer       = Bill Ross IV Turner Ross
| writer         = 
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = Bill Ross IV
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
45365 is a 2009 documentary film made by first-time directors and brothers Bill Ross IV and Turner Ross. The film is about the everyday life of small town Sidney, Ohio and the people living in it; the title comes from the towns postal (zip) code.

45365 premiered at the 2009 South by Southwest Film Festival, where it won the Grand Jury Prize.  It won the Roger and Chaz Ebert Truer than Fiction award at the 2010 Independent Spirit Awards. {{cite web |title=Nominees: Spirit Awards
|url=http://www.spiritawards.com/nominees#Chaz%20and%20Roger%20Ebert%20Truer%20than%20Fiction%20Award |accessdate=2010-06-17 |work=Spirit Awards| archiveurl= http://web.archive.org/web/20100504124359/http://spiritawards.com/nominees| archivedate= 4 May 2010  | deadurl= no}}  Jeannette Catsoulis has described the film as follows: "A beguiling slice of Midwestern impressionism, 45365 drops in on the residents of Sidney, Ohio, to observe their lunches and haircuts, trials and transgressions." 

==References==
 

==External links==
*  
*   PBS

 
 
 
 
 
 
 
 

 