The Phantom of the Opera (1962 film)
 
 
{{Infobox film
| name           = The Phantom of the Opera
| image          = Phantom of opera 1962 poster.jpg
| image_size     = 
| caption        = British original poster
| director       = Terence Fisher
| producer       = Anthony Hinds Basil Keys John Elder Gaston Leroux (novel)
| based on       =  
| starring       = Herbert Lom Heather Sears Edward de Souza Michael Gough Edwin Astley Arthur Grant
| editing        = Alfred Cox
| studio         = Hammer Film Productions
| distributor    = J. Arthur Rank Film Distributors (UK), Universal Pictures (USA)
| released       =  
| runtime        = 84 minutes
| country        = United Kingdom
| language       = English
| budget         = ₤180,000 Marcus Hearn & Alan Barnes, The Hammer Story: The Authorised History of Hammer Films, Titan Books, 2007 p 72 
| gross          = 580,164 admissions (France) 
}}
The Phantom of the Opera is a 1962 British horror film based on the novel by Gaston Leroux. The film was made by Hammer Film Productions.

==Plot== Victorian London London Opera House finds the opening of a new opera by Lord Ambrose DArcy (Michael Gough), a wealthy and pompous man, who is annoyed and scornful when the opera manager Lattimer (Thorley Walters) informs him the theatre has not been completely sold out.  No one will sit in a certain box because it is haunted. Backstage, despite the soothing efforts of the operas producer, Harry Hunter (Edward de Souza), everyone, including the shows star, Maria, is nervous and upset as if a sinister force was at work.  The climax comes during Marias first aria, when a side of the scenery rips apart to reveal the body of a hanged stage hand.  In a panic, the curtain is rung down, and Maria refuses to sing again.
 Ian Wilson).  Investigating the murder, Harry leaves Christine by herself. where she is approached by a man dressed in black, wearing a mask with only one eye. The Phantom of the Opera. He tells her she must come with him but she screams and The Phantom flees. Harry comforts her and takes her home.

The next day Lord Ambrose sends a dismissal to Christine for refusing to come back to his apartment. Lord Ambrose chooses a more willing but less talented singer to take Christines place. When Harry refuses to accept this he is also dismissed by Lord Ambrose. Visiting Christine at her boarding house, Harry finds some old manuscripts which he recognizes as a rough draft of the opera he has been producing. Questioning Christines landlady Mrs. Tucker, he learns that it was written by a former boarder by the name of Professor Petrie, who had been killed in a fire at a printers that was to print his music. Making further inquiries, he learns that Petrie did not actually perish in the fire, but was splashed with Nitric Acid while apparently trying to extinguish the blaze, had run away in agony and was drowned in the River Thames.  This is confirmed by the policeman who was in the area at the time, but the body was never recovered. Harry and Christine have a romantic day together. While having a moonlight carriage ride harry tells her about Petrie, and that he is convinced that Lord Ambrose stole Petries music. He leaves it at that, as he believes that the Professor is long since dead. 

When she gets home later that night she confronted in bedroom by the dwarf, Christine faints from fright and is carried off. She returns to consciousness, deep in the cellars of the opera house, to see the Phantom playing a huge organ. He tells the frightened girl that he will teach her to sing properly, and rehearses her with fanatical insistence until she collapses from exhaustion. Meanwhile, Harry, reinstated as the opera producer, is worried about Christines disappearance. Pondering the story of the mysterious Professor, he checks the river where he had last been seen.  At that same moment, he hears the echo of Christines voice emanating from a storm drain, and soon finds himself following the voice through one of Londons water-filled sewers. The faint sound of the organ playing draws him down a tunnel where the dwarf attacks him with a knife.  Harry subdues him, and finds himself facing the missing Professor as Christine looks on from a bed (where shed been sleeping).

Harry asks the professor what had happened in his past. In a flashback, the Phantom says that five years before, as a poor and starving composer, he had been forced to sell all of his music, including the opera, to Lord Ambrose for a pitifully small fee with the thought that his being published would bring him recognition. When he discovered that Lord Ambrose was having the music published under his own name, Petrie became enraged and broke into the printers to destroy the plates. In burning sheet music that had already been printed, Petrie unwittingly started a fire, then accidentally splashed acid on his face and hands in an effort to put it out, thinking it was water. In terrible agony, he ran out, jumped into the river, and was swept by the current into an underground drain, where he was rescued and cared for by the dwarf, whose passion was music and who existed in the cellars underneath the opera house. The Phantom says that he is dying but his wishes to see his opera performed by Christine. They both agree to allow him time to complete her voice coaching.
 
When the opera is presented several weeks later, Lord Ambrose is confronted in his office by the Phantom. He rips off The Phantoms mask and sees his terrifying face, he runs out screaming into the night. As the curtain rises, with Christine in the lead role, the Phantom watches eagerly in the "haunted" box. Her performance brings him to tears as he hears his music finally presented. Listening enraptured to the music, the dwarf is discovered in the catwalks by a stage-hand and in the chase, he jumps onto a huge chandelier poised high above the stage over Christine. As the rope begins to break from the weight, the Phantom spots the danger. He rips off his mask, leaps from his box to the stage and pushes Christine safely from harm. He is impaled by the chandelier before the eyes of the horror-stricken audience.

==Cast==
* Herbert Lom as The Phantom of the Opera/Professor Petrie
* Heather Sears as Christine Charles
* Edward de Souza as Harry Hunter
* Michael Gough as Lord Ambrose DArcy Ian Wilson as The Dwarf
* Thorley Walters as Lattimer Harold Goodwin as Bill
* Marne Maitland as Xavier
* Miriam Karlin as Charwoman
* Patrick Troughton as The Rat Catcher
* Renée Houston as Mrs. Tucker
* Keith Pyott as Weaver

==Production==
 1943 remake, Universal was interested in revisiting the story again.  The first plans for remake were in-studio, with William Alland producing and Franklin Coen writing.  Plans for this remake fell through, but upon the success of the distribution of Dracula (1958 film)|Dracula for Hammer, Universal decided to let the British outfit tackle the project instead, and the project was announced in February, 1959. 
 The Mummy The Hound of the Baskervilles for United Artists) could be fulfilled, but thereafter, could only produce two pictures a year for other studios.  Phantom of the Opera was among the announced for Universal. 

Over the next two years, the project fell on and off the charts.  In 1960, the project was connected with Kathryn Grayson, although she had not been in pictures for some years.   According to producer Anthony Hinds, the romantic lead (Harry Hunter) was written for Cary Grant, who had expressed his interest in doing a Hammer horror film, at a time when it was common for American actors to be featured in British films. He was not, as is often supposed, slated to play the Phantom himself. 
 Bray Studios on a modest budget. Lom recalled in one interview how the producers at Hammer expected actors to throw themselves into their work: "For one of my scenes, the Hammer people wanted me to smash my head against a stone pillar, because they said they couldnt afford one made of rubber", Lom reveals. "I refused to beat my head against stone, of course. This caused a big crisis, because it took them half a day to make a rubber pillar that looked like stone. And of course, it cost a few pennies more. Horror indeed!"
 Wimbledon Theatre in London, which was rented for three weeks.  Over 100 musicians and chorus people were hired for the shoot.   The film had a reported budget initially of £200,000,  but it was reported after principal shooting to be £400,000, both figures unusually high for a Hammer film. 

All of the flashback scenes showing how Professor Petrie became the Phantom were filmed with "Dutch angles", meaning the camera was noticeably tilted to give an unreal, off-kilter effect - a time-honored method in film of representing either a flashback or a dream.

The Phantom of the Opera opened in New York City on 22 August 1962 at the RKO Palace Theater.  In person was Sonya Cordeau, who played "Yvonne" in the picture.  Cordeau later went on tour with the film for Universal.  

The film failed at the box office but gained a cult following. Mike Sutton from the webpage Dvdtimes.com had this to say about Loms performance "as the sad, deformed Petrie is a triumph in every respect. Using exquisitely subtle body language and managing, somehow, to make the expressions in his single eye tell a whole story of pain and frustration, Lom is unforgettable. It may be heretical to say this but when I think of the Phantom of the Opera, it is Lom who comes immediately to mind."
 John Maddison) looking for the Phantom was filmed to increase the running time. This footage was shot at Universal Studios and Hammer Productions had no input at all. The Kiss of the Vampire and The Evil of Frankenstein also had American-shot footage added to their television showings as well. This was a common practice when it was thought that parts of the film were too "intense". These scenes were edited out and more acceptable scenes replaced them or extended the running time. 

In common with Hammers usual practice, when shown in British cinemas in 1962, the film was paired with Captain Clegg, another of the studios films.

==Music==
 Toccata and Fugue in D minor, a well-known piece of church organ music that is commonly associated with horror films, mainly due to the musics association to this film .

== Critical reception ==

The Hammer Story: The Authorised History of Hammer Films wrote of the film: "Although distinguished by some fine acting, sets and music, The  Phantom of the Opera seems decidedly half-baked." The author(s) called Terence Fishers direction "misguided", and noted that distributor J. Arthur Rank Film Distributors "emasculation of the British print sealed its fate."  The film also takes away much of the Phantoms dark, morbid side, making him a tragic hero.

==Home video release==

In North America, the film was released on 6 September 2005 along with seven other Hammer horror films on the 4-DVD set The Hammer Horror Series (ASIN: B0009X770O), which is part of MCA-Universals Franchise Collection.

==References==
 

; Sources

*  

==External links==
* 
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 