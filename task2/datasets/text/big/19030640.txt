The Little Bear Movie
{{Infobox Film
|name=The Little Bear Movie
|image=The Little Bear Movie.jpg
|caption=DVD cover
|director=Raymond Jafelice
|producer=Maurice Sendak based on= 
|story=Nancy Barr
|screenplay=Raymond Jafelice Tracy Ryan Elizabeth Hanna Amos Crawley
|music=Marc Jordan Antony Vanderberg
|editing= Nelvana Limited Nickelodeon Movies
|distributor=Paramount Pictures
|released= 
|runtime=77 minutes
|country=Canada United States
|language=English
}} Little Bear, same name Nelvana Limited for Paramount Pictures. The film was released in theaters Christmas 2000 and released on direct-to-video on August 7, 2001 by Paramount Home Video. It stars Kristin Fairlie as the voice of Little Bear.

==Characters==
*Little Bear (voiced by Kristin Fairlie)
*Mother Bear (voiced by Janet Laine-Green)
*Owl (voiced by Amos Crawley) Tracy Ryan)
*chicken|Hen (voiced by Elizabeth Hanna)
*Cat (voiced by Andrew Sabiston)
*Father Bear (voiced by Dan Hennessey)
*Moose (voiced by Ray Landry) 
*Cub (voiced by Kyle Fairlie) - A wild dark brown bear who lives in the wilderness with his parents. Unlike Little Bear, he walks mostly on all fours rather than upright, though he occasionally walks on two paws. He can sometimes be a bit mean, but he becomes a good friend of Little Bear.
*Trouble (voiced by Wayne Best) - A hungry Mountain lion who aims to eat Little Bear, Cub or one of their friends.
*Little Moose (voiced by Max Morrow) - A shy friend of Cubs.
*Mother Moose (voiced by Catherine Disher) - Little Mooses mother.
*Poppy and Pete (voiced by Cole Caplan and Asa Perlman) - Two playful red foxes that keep Cub company in the absence of his parents.
*Cubs Father (voiced by Maurice Dean Wint)
*Cubs Mother (voiced by Alison Sealy Smith)

==Plot==
Little Bear and Father Bear go out camping and they meet a new young bear named Cub and his friend Little Moose. Then Little Bear is met with Trouble, the mountain lion, but Cub saves him. Cub tries to get acquainted with Little Bears house then plays with Little Bear and his friends, Cat, Owl, Duck, and Hen. When they learn that Cubs parents got lost, they search for them. Little Bear, Cub, and Duck get separated from Moose and the others. They soon encounter Cubs friends Poppy and Pete the two silly red foxes. When they get to the canyon, Trouble spies on them and tries to eat Duck, but Poppy and Pete save Duck. Little Bear manages to "defeat" Trouble and find Cubs parents.

==Music==
The Little Bear Movie never had its launched soundtrack. The film contains two pieces of unknown music and the instrumental soundtrack of the Little Bear (TV series)|series.

(1) Great Big World and (2) Everybody Wants To Paint My Picture:
 Composed by: Marc Jordan and Antony Vanderberg
 Performed by: Shawn Colvin

(3) The Little Bear (TV series) Instrumental

==Release and response==
The film premiered on television and was released on DVD by Paramount Home Entertainment. Shawn Colvin (performer) and Marc Jordan/Antony Vanderburgh (composers) were nominated for Best Original Song at the 2001 Video Premiere Awards  for the song "Great Big World." 

==References==
 

==External links==
 
* 
* 
 

 
 
 
 
 
 
 
 
 
 
 
 
 