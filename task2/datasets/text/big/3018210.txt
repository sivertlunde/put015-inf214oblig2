What Price Glory? (1926 film)
{{infobox film
| name           = What Price Glory?
| imagesize      =
| image	=	What Price Glory? FilmPoster.jpeg
| caption        = German language release poster
| director       = Raoul Walsh William Fox
| writer         = James T. ODonohoe (scenario) Malcolm Stuart Boylan (intertitles)
| based on       =  
| starring       = Victor McLaglen Edmund Lowe Dolores del Río Phyllis Haver
| music          = Ernö Rapée Lew Pollack
| cinematography = Barney McGill John Marta John Smith
| editing        =
| distributor    = Fox Film Corporation
| released       =  
| runtime        = 116 mins.
| country        = United States
| language       = Silent film (English intertitles)
| gross = $2 million 
}}
 silent comedy-drama What Price What Price Fox attraction.  

== Plot ==
Flagg and Quirt are veteran United States Marines sergeants whose rivalry dates back a number of years. Flagg is commissioned a Captain (United States)|Captain, he is in command of a company on the front lines of France during World War I. Sergeant Quirt is assigned to Flaggs unit as the senior non-commissioned officer. Flagg and Quirt quickly resume their rivalry, which this time takes its form over the affections of Charmaine, the daughter of the local innkeeper. However, Charmaines desire for a husband and the reality of war give the two men a common cause.

== Cast ==
* Edmund Lowe as 1st Sergeant Quirt
* Victor McLaglen as Captain Flagg
* Dolores del Río as Charmaine de la Cognac
* William V. Mong as Cognac Pete
* Phyllis Haver as Shanghai Mabel
* Elena Jurado as Carmen, Philippine girl
* Leslie Fenton as Lieutenant Moore
* Barry Norton as Private Mothers Boy Lewisohn
* Sammy Cohen as Private Lipinsky
* Ted McNamara as Private Kiper
* August Tollaire as French Mayor
* Mathilde Comont as Camille, fat lady Patrick Rooney as Mulcahy (billed as Pat Rooney)
* J. Carrol Naish bit part

==Production==
 , Delores del Rio and Victor McLaglen in What Price Glory?]] Sam H. Fox Movietone sound-on-film system.  In January 1927, Fox re-released What Price Glory? with synchronized sound effects and music in the Movietone system. 

Part of its fame revolves around the fact that the characters can be seen speaking profanities which are not reflected in the intertitles, but which can be deciphered by lip reading|lipreaders. The studio was reportedly inundated by calls and letters from enraged Americans, including deaf and hearing impaired people, to whom the vivid profanity between Sergeant Quirt and Captain Flagg was extremely offensive.
 William Boyd would play characters similar to Quirt and Flagg in the 1928 film Two Arabian Knights.

==Adaptation==
McLaglen and Lowe reprised their roles from the movie in the radio program Captain Flagg and Sergeant Quirt, broadcast on the Blue Network September 28, 1941 - January 25, 1942, and on NBC February 13, 1942 - April 3, 1942. Dunning, John. (1998). On the Air: The Encyclopedia of Old-Time Radio. Oxford University Press. ISBN 978-0-19-507678-3. Pp. 136-137. 

==Sequels==
*The Cock-Eyed World (1928) (directed by Raoul Walsh)
*Women of All Nations (1931) (directed by Raoul Walsh)
*The Stolen Jools (1931) (cameo) Hot Pepper (1933)

Lowe and McLaglen played two similar Marines in the RKO Radio Pictures film Call Out the Marines (1942).

==References==
 

== External links ==
*  
*   at SilentEra
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 