I Was a Teenage Frankenstein
{{Infobox film
|name=I was a Teenage Frankenstein
|image= i-was-teenage-frankenstein.jpg image size= 200px
|director=Herbert L. Strock Original theatrical poster
|writer=Kenneth Langtry based on = an original story by Kenneth Langtry 
|starring=Whit Bissel Phyllis Coates Robert Burto Gary Conway George Lynn
|producer=Herman Cohen executive Samuel Z. Arkoff James H. Nicholson studio = Santa Rose Productions distributor = American International Pictures (USA) Anglo-Amalgamated (UK))
|released= 
|runtime=74 mins.
|language=English gross = $310,000 |}}
 How to Robert Burton, Gary Conway and George Lynn.

==Plot==
Professor Frankenstein (Whit Bissell), a guest lecturer from England, talks Dr. Karlton (Robert Burton) into becoming an unwilling accomplice in his secret plan to actually assemble a human being from the parts of different cadavers. After recovering of a body from a catastrophic automobile wreck, Professor Frankenstein takes the body to his laboratory-morgue, where in various drawers he keeps spare parts of human beings. The Professor also enlists the aid of Margaret (Phyllis Coates), as his secretary to keep all callers away from the laboratory.

Margaret, becoming suspicious of what is going on, decides to investigate and goes down to the morgue. She is panic-stricken by the monster (Gary Conway) who has been activated by electricity following the grafting of a new leg and arm. She dares not tell the Professor about her feelings, and keeps silent for the present. On a couple of occasions, the professor takes discarded human body parts and feeds them to an alligator concealed in a hidden chamber.

One night, the monster leaves the laboratory. He peers into a girls apartment, the girl becomes hysterical and 
starts screaming, in his attempt to silence her, he kills her in panic and flees. The next morning the hunt for the murderer is on. Margaret, angry at the Professor, tells him that she knows that the monster is responsible for the murder. The Professor, taking no chances, has the monster kill her and feeds her remains to the alligator. Dr. Karlton, sent out of town, knows none of this.

The Professor accompanies the monster to a Lovers Lane, where he kills a teenage boy in order to obtain his face. The boys face is successfully grafted onto the monster. Professor Frankenstein tells Dr. Karlton of his plans to dismember his creation and ship him in various boxes to England — there to put him together again. When they strap the monster down again, he becomes suspicious and tears loose to throw Dr. Frankenstein into the alligator pit, while Dr. Karlton runs for help.

When Dr. Karlton arrives with the police, the monster, who is maddened with fright, backs into the electrical dial board. Contact with the iron wrist bands electrocutes him, and he falls to the ground dead. Karlton tells the police that he will never forget the way the monsters face looked after the accident, and the original mangled face of the victim who became the monster.

==Cast==
 
* Whit Bissell as Professor Frankenstein
* Phyllis Coates as Margaret
* Robert Burton as Dr. Karlton
* Gary Conway as Teenage Monster/Bob
* George Lynn as Sergeant Burns
* John Cliff as Sergeant McAfee
* Marshall Bradford as Dr. Randolph
* Claudia Bryar as Arlenes mother
* Angela Blake as Beautiful girl
* Russ Whiteman as Dr. Elwood
 

==Production==
I Was a Teenage Werewolf had been a big success for AIP and a Texas exhibitor requested two new horror movies from the studio if they could be ready by Thanksgiving. AIP commissioned Herman Cohen to make I Was a Teenage Frankenstein and Blood of Dracula. Cohen says the two films were written and put in front of the cameras in only four weeks "so I had to really, really cut down" in terms of production values.  Weaver, Tom.   Herman Cohen. Retrieved: January 13, 2015. 

I Was a Teenage Frankenstein was filmed in black and white, with the ending in color for a vivid effect. The film was shot at Ziv studios. Cohen says that the alligator they hired for the movie had been used to dispose of bodies by a serial killer from a small town outside Dallas. 

==Reception==
Film critic Richard W. Nason in his review for The New York Times, said:
 If you discount any immediate connection between the mass media and the temper of the culture, then the film warrants little attention ... the automaton, enacted by Gary Conway, is a teen-ager assemble  from the limbs of other teenagers. This is, in one sense, abhorrent. It forces one to acknowledge the impression that such films may aggravate the mass social sickness euphemistically termed "juvenile delinquency." ... In this particular film, there are graphic displays of human dismemberment. Before one such act of surgical perversion, the mad doctor  assistant says, "I have no stomach for it." That would be a plausible reaction for any adult who had read the days headlines about teen-age crime."  

Film historian and critic Leonard Maltin dismissed I Was a Teenage Frankenstein as,  "... campy junk ... Doesnt live up to that title, worth catching only for Bissells immortal line: Answer me! You have a civil tongue in your head! I know – I sewed it in there!" 

I Was a Teenage Frankenstein was released on VHS/NTSC videocassette in 1991 by RCA/Columbia Pictures Home Video under the shortened title Teenage Frankenstein which was the original theatrical title also used when released in the UK by Anglo-Amalgamated.
==See also==
* List of films featuring Frankensteins monster

==References==
===Notes===
 
===Bibliography===
 
* Maltin, Leonard. Leonard Maltins Movie Guide 2009. New York: New American Library, 2009 (originally published as TV Movies, then Leonard Maltin’s Movie & Video Guide), First edition 1969, published annually since 1988. ISBN 978-0-451-22468-2.
 

==External links==
*   at TCMDB
*  
*  
*   at Hermancohen.com

 

 
 
 
 
 
 
 
 