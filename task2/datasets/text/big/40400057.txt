Jungle Moon Men
{{Infobox film
| name           = Jungle Moon Men
| image          = Jungle moon men poster.jpg
| image_size     = 
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| film name      = 
| director       = Charles S. Gould
| producer       = Sam Katzman
| writer         = 
| screenplay     = Jo Pagano Dwight Babcock
| story          = Jo Pagano
| based on       =   
| narrator       = 
| starring       = Johnny Weissmuller
| music          = 
| cinematography = Henry Freulich
| editing        = Henry Batista
| studio         = Columbia Pictures
| distributor    = Columbia Pictures
| released       = April 1955 
| runtime        = 70 minutes  
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Jungle Moon Men (1955) is the fifteenth Jungle Jim film produced by Columbia Pictures. It features Johnny Weissmuller in his second performance as the protagonist adventurer Johnny Weissmuller. The film was directed by Charles S. Gould and written by Dwight Babcock and Jo Pagano. 

The film centres on jungle roamer Johnny Weissmuller and his team attempting to rescue an acquaintance in the jungle of Baku, which is inhabited by "moon men" worshipping an apparent Moon Goddess. Filming took place in May 1954 in California. It was theatrically released in the United States in April 1955.

==Plot==
Adventurer Johnny Weissmuller (Johnny Weissmuller) is roped in by Egyptian archaeologist Ellen Marsten (Jean Byron) to traverse the African jungle of Baku. They seek to rescue an acquaintance, Marro (Benjamin F. Chapman, Jr.), from his captors, pygmies known as the "Moon Men". The Moon Men are devoted to a "Moon Goddess" Oma (Helen Stanton), who is apparently an immortal whose only weakness is sunlight. Marro is chosen to be Omas chief religious official. 

After being joined by Marstens friend Bob Prentice (Bill Henry), the team of Weissmuller, Marstern, and Prentice, set off for Baku. They find Marro and urge him to escape. However, he dies the moment he steps outside the parameters of the jungle. Interrogating a pygmy Damu (Billy Curtis), Weissmuller learns that Marro was fed a voodoo potion that would kill him once he tried to escape Baku. Just then, the Moon Men overpower the team and capture them. Prentice is selected to take over Marros position, while Weissmuller and Marstern are brought to Omas temple. 

There, they are stopped by Santo (Myron Healey) and his right-hand man Max (Frank Sully). The evil duo command Weissmuller to lead them into the temple. They meet Oma and also find loads of precious stones in the building. Knowing that not everybody can leave Baku, Weissmuller sacrifices himself for the rest. He asks Prentice to contact the police as soon as he gets to the mainland. Santo pockets a large amount of the jewels and turns to flee. The Moon Men stop him, letting loose a pride of vicious lions. Santo and Max are gorily killed, while the rest manage to escape. 

With not much time left, Weissmuller requests for Oma to reveal a fast exit route from Baku. She reluctantly tells him but crumbles into fine dust after being dragged by the explorer to the sunny open. After returning to civilisation, Prentice kvetches about the absence of any evidence to prove Bakus existence. However, Weissmullers pet chimpanzee Kimba (Rory Mallinson) is shown to have taken a diamond pendant with him.

==Production==
  (1887). 

While still a work-in-progress, the film was referred to as simply Moon Men.  It was directed by Charles S. Gould with assistance from Eddie Saeta. Sam Katzman was in charge of production for Columbia Pictures, while Jo Pagano and Dwight Babcock wrote the screenplay based on a story by Pagano. Henry Freulich signed on as cinematographer. The set decorator was Sidney Clifford. Mischa Bakaleinikoff headed the musical direction, and Henry Batista edited the film. Filming locations included Iverson and Corriganville. Principal photography was completed in about a weeks time. It officially began on May 19, 1954, and ended on May 25, 1954.  Archived footage from a preceding Jungle Jim film, Jungle Manhunt (1951), was featured in Jungle Moon Men. 

==Release==
The film was officially released in the United States in April 1955. A reviewer for the magazine Variety (magazine)|Variety wrote that it "fits into the past offerings in the Sam Katzman series and should do the same type of biz in program situations". The Hollywood Reporter  review criticised it for being "negligently written and executed". In evaluating the film in his 2012 book Columbia Pictures Movie Series, 1926—1955: The Harry Cohn Years, Gene Blottner praised Goulds directorial effort, as well as the fine cinematography and "superb" lighting. He stated that the film was "far-fetched" in its plot yet "isnt dull". 

==Notes==
 

==References==
 

==Bibliography==
*  }}
*  }}

==External links==
*  
*  
*  
 

 
 
 
 
 
 
 
 