Come Back To Erin
{{infobox film
| name = Come Back To Erin
| image          = Come_Back_Erin_Wiki.jpg
| image size      =
| caption        = Sidney Olcott (left), Jack J. Clark.
| director = Sidney Olcott
| producer = Gene Gauntier Feature Players
| writer = Sidney Olcott
| starring = Gene Gauntier Jack J. Clark Sidney Olcott
| distributor = Warners Features
| released =  
| runtime = 3000 ft
| country = United States language = Silent film (English intertitles) 
}}

Come Back To Erin is a 1914 American silent film produced by Gene Gauntier Feature Players and distributed by Warners Features. it was directed by Sidney Olcott with himself, Gene Gauntier and Jack J. Clark in the leading roles.

==Cast==
* Gene Gauntier - Peggy OMalley
* Jack. J Clark - Jerry
* Sidney Olcott - Peggys Father

==Production notes==
* The film was shot in New York city, in Ireland, in Killarney and Beaufort, County Kerry and in Queenstown (now Cobh), co Cork.

==References==
* Michel Derrien, Aux origines du cinéma irlandais: Sidney Olcott, le premier oeil, TIR 2013. ISBN 978-2-917681-20-6  

==External links==
* 
*    website dedicated to Sidney Olcott
*  at YouTube

 
 
 
 
 
 
 
 
 


 
 