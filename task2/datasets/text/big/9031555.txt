The Brain from Planet Arous
 
{{Infobox film
| name           = The Brain from Planet Arous
| image          = The-Brain-from-Planet-Arous.jpg
| image size     =
| caption        =
| director       = Nathan H. Juran
| producer       = Jacques R. Marquette
| writer         = Ray Buffum Robert Fuller
| music          = Walter Greene
| cinematography = Jacques R. Marquette
| editing        =
| distributor    = Howco International
| released       =  
| runtime        = 70 min.
| country        = United States
| awards         = English
| budget         = $58,000 (estimated) 
}}
The Brain from Planet Arous is a 1957 science-fiction film that features the theme of alien demonic possession|possession. 

==Plot==
An outer-space terrorist from a planet named Arous - a brain-shaped creature named Gor - arrives on Earth and possesses young scientist Steve March. Gor then proceeds to use his vast, destructive powers to bend the world to his will, threatening to wipe out the capital city of any nation that defies him.

Meanwhile, another brain from Arous - named Vol - arrives on Earth and eventually inhabits the body of Marchs fiancees dog. Vol explains that Gor is a wanted criminal on their world. Gors only weakness is the Fissure of Rolando and he is only vulnerable during one brief period when he needs to exit his host to absorb oxygen.

==Production notes==
The effect on Agars eyes was achieved using special contact lenses lined with metal foil. This effect was later used on actor Gary Lockwood for the second Star Trek pilot episode "Where No Man Has Gone Before".

Stock footage of test houses being flash incinerated in A-bomb tests was used to show Gors psychic powers.
 The Hidden also shares similar elements.  
 

==Release==
This film was shown as a double-feature with Teenage Monster from 1958 onwards. 

==Legacy== American TV The Butcher Boy, which is viewed by the main character at his local cinema. 
 House of the Dead 2. A clip from the film was also used in a TV Guide ad in 1991, which asked, "Were you watching this when the Beverly Hills 90210 Christmas special aired?", prompting multiple viewers to respond that Brain from the Planet Arous was the better choice.
 Datsik has also used the first part of this sample, "After Im gone your earth will be free to live out its miserable span of existence," in his song "Light The Fuse". The sample of Gors introduction has also been featured in various songs, including "Maid-san Para Para" by Pilko Minami and in L.E.D.s remix of the Anubis Final Battle theme for the album Zone of the Enders ReMix Edition.

In 2002 for their song Perfekte Droge from their album Herzwerk II.  the German band Megaherz.  used Gors laugh and a sample of:   

The samples of speech from the film are available in Sonys royalty-free sample library pack, Methods of Mayhem: Industrial Toolkit. 

==See also==
Donovans Brain (film)|Donovans Brain, earlier film dealing with "brain" possession 

==References==
  

==External links==
* 
*  
* 
*  
 

 
 
 
 