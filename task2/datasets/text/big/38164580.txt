The Last Exorcism Part II
{{Infobox film
| name = The Last Exorcism Part II
| image = The Last Exorcism Part II Poster.jpg
| caption = Theatrical Release Poster
| director = Ed Gass-Donnelly
| producer = Eli Roth Thomas A. Bliss
| screenplay = Damien Chazelle Ed Gass-Donnelly
| story = Damien Chazelle
| based on =  
| starring = {{Plainlist |
* Ashley Bell
* Julia Garner
* Spencer Treat Clark
* David Jensen
* Tarra Riggs
* Louis Herthum
* Muse Watson
}}
| music = Michael Wandmacher
| cinematography = Brendan Steacy
| editing = Ed Gass-Donnelly
| studio = Arcade Pictures Strike Entertainment StudioCanal
| distributor = CBS Films
| released =  
| runtime = 88 minutes   89 minutes  
| country = United States
| language = English
| budget = $5 million   
| gross = $15.1 million 
}} drama horror film co-written and directed by Ed Gass-Donnelly. It stars Ashley Bell, Julia Garner, Spencer Treat Clark, David Jensen, Tarra Riggs, Louis Herthum, and Muse Watson. It is a sequel to 2010s The Last Exorcism, and released on March 1, 2013.
 found footage format.    

==Plot==
A couple, Jared (Judd Lormand) and Lily (Boyana Balta), finds a demonic-looking Nell Sweetzer (Ashley Bell) squatting next to their refrigerator. She soon is taken to a hospital, where she appears to be catatonic. After spending a few months at Franks (Muse Watson) home for girls in New Orleans and settling in as a chambermaid at a hotel under the supervision of her boss Beverly (Diva Tyler), Nells condition seems to have improved and she no longer has "bad dreams". Nell and her group of friends Gwen (Julia Garner), Daphne (Erica Michelle), and Monique (Sharice Angelle Williams) visit a Mardi Gras event; Nell witnesses many strange happenings there, including masked men watching her. Her personality changes as things get darker. She begins to get hints that the demon Abalam is back.
 previous film revealing that the lost footage has been discovered; this frightens and angers Nell as she becomes a reluctant internet celebrity because of the video. Chris (Spencer Treat Clark), another worker at the hotel who likes Nell, slits his own throat after viewing a clip from the same footage which depicts Nell lying about how she got pregnant. Cecile seeks help from the Order of the Right Hand, and Nell is introduced to Calder (David Jensen) and Jeffrey (E. Roger Mitchell). They attempt to rid the demon which is "in love" with Nell by transferring it into the body of a sacrificed chicken. The supernatural force in Nell proves to be too powerful, and Calder is forced to kill her by injecting her with a lethal dose of morphine.

The demon appears in front of Nell, taking her appearance as a doppelganger, her father, and Chris, and begs her to accept its hand. Nells pulse stops temporarily and leads Calder, Jeffrey, and Cecile, to believe that she has died. She finally gives in and grabs the demons hand; the house is left to burn with the three secret society members being killed. Nell murders Frank and burns the home for girls, leaving her friends to die. Nell gets inside a car and the prophecy of the end times is proven to be correct as she sets multiple buildings and vehicles aflame whilst driving past them.

==Cast==
 
* Ashley Bell as Nell Margaret Sweetzer
* Julia Garner as Gwen
* Spencer Treat Clark as Chris
* David Jensen as Calder
* Tarra Riggs as Cecile
* Louis Herthum as Louis Sweetzer
* Boyana Balta as Lily
* Muse Watson as Frank Merle
* Erica Michelle as Daphne
* Sharice Angelle Williams as Monique
* Joe Chrest as Pastor
* Raeden Greer as Stephanie
* Judd Lormand as Jared
* E. Roger Mitchell as Jeffrey 
* Steven Dane Rhodes as Drunken Tourist
* Ashlynn Ross as Michelle
* Andrew Sensenig as Doctor
* Diva Tyler as Beverly
* Marcellus Grace as Janitor
* Deacon John Moore as Old Bluesman
* Gideon Hodge as Tin Man 
* Cristina Franco as Marie
 

==Production==
Production and filming began March 19, 2012 and ended on April 21, 2012. Principal photography mostly took place in New Orleans, Louisiana.

==Release== MPAA but cuts were made to the finished product to receive a neutered PG-13 rating (all deleted footage was restored in the unrated edition of the film).

===Home media===
The film was released on DVD and Blu-ray Disc|Blu-ray on June 18, 2013. The Blu-ray disc only contains the unrated cut of the film. The unrated version restored approximately 1 minute of footage deleted from the final cut which extended the length of certain scenes by a few seconds and depicted a more graphic version of Chriss suicide.

==Reception==

===Box office===
In its opening weekend The Last Exorcism Part 2 grossed $7,728,354. Domestically the film grossed $15,161,327 and $17,975 internationally grossing a total of $15,179,302 turning in a 10 million dollar profit but being a box office failure compared to the previous films $67,738,090 gross to a $1.8 million budget.

===Critical reception===
The film was not screened in advance for critics. The film was widely panned by critics and audiences. Review aggregation website Rotten Tomatoes gives the film a score of 16% based on reviews from 65 critics, with an average score of 3.7/10.  Metacritic, which assigns a rating of 0 to 100, gave The Last Exorcism Part 2 a 35 indicating generally unfavorable reviews.

Frank Scheck of The Hollywood Reporter wrote that The Last Exorcism Part II was an "unimpressive follow-up  ", though praising Ashley Bells "  memorable, unsettling performance that easily can be compared to Sissy Spacek’s Carrie (1976 film)|Carrie".  Christopher Runyon of Movie Mezzanine also gave a negative review stating "Not a single aspect of this film suggests that anyone involved in the production cared".

Not all reviews for the film were negative though, Mark Olsen of Los Angeles Times gave a positive review stating "The Last Exorcism Part II is an effectively unnerving, slow-burn supernatural horror tale". 

Audiences polled by CinemaScore gave the film a C- grade. 

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 