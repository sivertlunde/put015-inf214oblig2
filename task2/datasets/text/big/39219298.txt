Il vento fa il suo giro
{{Infobox film
| name     = Il vento fa il suo giro
| image    =
| director = Giorgio Diritti
| producer = 
| Script = 
| starring =
| cinematography = 
| music = 
| country = Italy
| language = Italian, French, Occitan
| runtime = 110 min
| released = 2005
}}

Il vento fa il suo giro is a 2005 Italian drama film directed by Giorgio Diritti.

==Plot==
Philippe is a former teacher who left everything to become a shepherd in the French Pyrenees. After the construction of a nuclear power plant near his sheep, he decided to leave the area. After an unsuccessful search in Switzerland, he found a the village Chersogno in the Maira Valley . The village is inhabited mainly by elderly residents and people who come only for summer holidays. It is a very closed community, which preserves the Occitan language and culture in Italy. After some doubts, the town council finds a home to let to Philip and the villagers set to work to restore it.
Initially, the village seems to welcome Philippe, his wife and three children. But soon some misunderstandings arise caused by the new residents who are not always sensitive to local customs and property rights. Especially Philip goats, who venture into abandoned fields get some owners angered. Thus, over time the family is less accepted by the villagers and malicious acts begin.

==Awards== Grand Prix at the Annecy Film Festival.

==See also==
*Movies about immigration to Italy

== External links ==
* 

 
 
 
 
 

 