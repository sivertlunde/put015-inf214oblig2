Griffin and Phoenix (1976 film)
 
{{Infobox film
| name           = Griffin and Phoenix
| image          = Griffin and Phoenix.jpg
| caption        = Video release cover
| director       = Daryl Duke Tony Thomas David M. Shapiro Judith Craig Marlin John Hill
| starring       = Peter Falk Jill Clayburgh Sally Kirkland Dorothy Tristan George Chandler
| music          = George Tipton
| cinematography = Richard C. Glouner
| editing        = O. Nicholas Brown 20th Century Fox Video (1982)
| released       =   March 18, 1977  (Theatrical)  ABC Circle Films
| runtime        = 97 min
| country        = United States English
| budget         =
}}
Griffin and Phoenix (sometimes     and   in select countries under the title Today Is Forever from 1977 through 1980.    – Turner Classic Movies. Retrieved February 2, 2012.  It was nominated in the category of Outstanding Achievement in Cinematography at the 28th Primetime Emmy Awards.   – Academy of Television Arts and Sciences. Retrieved January 31, 2012.    – IMDb.com. Retrieved January 31, 2012. 
 20th Century Fox in 1982,  but has never been formally released on DVD or Blu-ray.

==Plot== flashback to terminal melanoma and will soon die, which is assumed to have been the motivating factor behind his initiative to take the trip, which his family had talked about doing before he and his wife legal separation|separated. After becoming increasingly frustrated at his familys apathy about the trip, their reviling attitude toward him, and their trivial demands such as that he stop to walk the dog, he detaches his car from the camper and drives away, deserting them.
 consults another doctor who delivers the same information. Griffin is seen driving to Los Angeles, where he meets with his brother George (John Lehne) who updates him on his ex-wife and children and gives him $1,500 to spend at his leisure. Faced with his grim diagnosis, Griffin expresses his interest in a course entitled "Psychological Processes of Death and Dying", which he attends. Phoenix is also in attendance at this class, and it is where the two characters first meet. After class, Griffin asks Phoenix to meet him the following Wednesday night for dinner if she was interested. She is guarded and shows a lack of interest in developing a close relationship with Griffin due to her terminal illness, and instead of showing up for the date she spies on Griffin from the neighboring restaurant, where he spots her and invites her over.

Griffin and Phoenix spend the next several weeks together falling in love and living their lives to the fullest, engaging in fun but outrageous and childish public behavior and constantly switching between an on-again, off-again relationship|on-again and off-again relationship. Among their activities include sneaking into a movie theater, freighthopping, riding amusement park roller coasters and evading police after being caught painting messages on a water tower, where Phoenix writes "Class of 1959|59" and Griffin secretly writes "Griffin Loves Phoenix" encased in a heart and arrow design. Neither knows that the other is also terminally ill until Phoenix discovers books related to cancer, death and dying in Griffins apartment, and bursts out at him in rage under the mistaken assumption that he had been stalking her and knew she was terminally ill. She tries to run away, and when Griffin catches her and questions her they both learn of each others shared impending deaths. Phoenix tells Griffin that when she becomes too ill to continue living a normal life, she will resign herself to a hospital and she does not want him to come and find her. 

That day finally comes; Griffin finds a note from Phoenix telling him that her pain was too unbearable, and that he should go spend time with his family and remember his promise not to try and find her. Distraught, Griffin seeks her out and finally finds her after searching at over a dozen hospitals, where he sits by her bedside and they comfort each other. Unable to handle the emotional distress of Griffins presence, Phoenix asks him to leave, which he does, presumably never to return before her death. We then see Griffin browsing a cemetery, where he finds Phoenixs gravestone that under her name and dates of birth and death reads: "P.S. – Hi Griffin. Thought youd probably drop by". The film ends as we see a maintenance man painting over Phoenixs and then Griffins water tower messages.

==Cast==
*Peter Falk as Geoffrey Griffin — An upbeat middle-aged man who faces a terminal illness and whose estranged wife and children, unaware of his diagnosis, seem uninterested in his company.
*Jill Clayburgh as Sarah Phoenix — An eccentric woman who faces similar dire circumstances and whose family is also distant.
*John Lehne as George Griffin — Geoffreys older brother, the family member with whom he seemingly has the closest relationship.
*Dorothy Tristan as Jean Griffin — Geoffreys unsympathetic wife with whom he is separated and has two children.
*Ben Hammer as Dr. Feinberg — Geoffreys physician who delivers to him the news of his diagnosis.
*George Chandler as Old Man — A friend and neighbor of Geoffrey and Sarahs.
*Milton Parsons as Professor — A college professor who is teaching the course on death and dying at which Geoffrey and Sarah first meet.
*John Harkins as Dr. Glenn — Sarahs first doctor, with whom she becomes frustrated at her diagnosis.
*Ken Sansom as Dr. Harding — Sarahs second doctor.
*Randy Faustino as Randy Griffin — Geoffreys first son.
*Steven Rogers as Bob Griffin — Geoffreys second son.
*Sally Kirkland as Jody — Sarahs high school friend.
*Rod Haase as Usher

==Novelization==
The screenplay was novelized and published by Warner Books in 1976 under the title: Griffin Loves Phoenix, a novel by John Hill.

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 