Stonewall Uprising
{{Infobox film
| name           = Stonewall Uprising
| image          = Stonewall uprising.jpg
| alt            =  
| caption        = Theatrical release poster
| director       =  
| producer       =  
| writer         = David Heilbroner
| starring       = 
| music          = Gary Lionelli
| cinematography =  
| editing        = Kate Davis
| studio         = 
| distributor    = First Run Features
| released       =  
| runtime        = 82 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Stonewall Uprising is a 2010 American documentary film examining the events surrounding the Stonewall riots that began during the early hours of June 28, 1969. Stonewall Uprising made its theatrical debut on June 16, 2010, at the Film Forum in New York City.    The movie features interviews with eyewitnesses to the incident, including the New York Police Department deputy inspector Seymour Pine. 

The film was produced and directed by the documentary makers Kate Davis and David Heilbroner, and is based on the book by the historian David Carter, Stonewall: The Riots That Sparked the Gay Revolution.  The title theme is by Gary Lionelli. 

==Overview==
About the first third of Stonewall Uprising explores a general overview of societal attitudes toward homosexuality in 1960s America. Combining interviews with Virginia Apuzzo, Martin Boyce, Raymond Castro, Danny Garvin, Jerry Hoose, Tommy Lanigan-Schmidt, Dick Leitsch, John OBrien,  ", the film presents both a national perspective and a personal one. The film also touches on List of pre-Stonewall LGBT actions in the United States|pre-Stonewall activism, including the Annual Reminder pickets held in Philadelphia.

The film then shifts to the days immediately preceding the riot and the specific conditions in New York City, including a raid on the Stonewall Inn that had happened days before the raid that triggered the riot, to explain why conditions were ripe for some action to happen. Archive film from the riots, dramatic re-enactments and eyewitness testimony are presented, along with animation of the streets surrounding the Stonewall Inn showing how rioters were able to evade and outflank responding police.

Stonewall Uprising concludes with an examination of the aftermath of the rioting, including the energizing of the gay community as a political force and the establishment of Christopher Street Liberation Day, the genesis of gay pride parades in the United States.

==Reception==
David Mixner, the author, political strategist, civil rights activist and public affairs advisor, wrote on his blog, Like the movie Milk (film)|Milk, this film can have a major impact on the LGBT movement. We need to get people into the theaters and see this amazing historical document...

...With much surprise, I learned so much new information from this film about the evening (of Stonewall)...Another surprise to me was the broad spectrum of citizens who participated in the riots that extended far beyond the young and drag queens. {{cite web
 | url = http://www.davidmixner.com/2010/06/film-review-stonewall-uprising-is-outstanding.html
 | title = Film Review: "Stonewall Uprising" Is Outstanding
 | first = David
 | last = Mixner
 | publisher = David Mixner
 | accessdate = June 16, 2010
}} }}

==American Experience== PBS on season 23 as episode 10 of the series American Experience.

==Notes==
 

==Further reading==
* 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 