The Color of Time
{{Infobox film
| name           = The Color of Time
| image          = The Color of Time film poster.png
| caption        = Film poster
| director       =  
| producer       =   Edna Luise Gabrielle Demeestere|Alexis Shruti Ganguly|Brooke Goldfinch| Pamela Romanowsky|Bruce Tine Thomasen|Virginia Omar Zúñiga Hidalgo}}
| starring       =  
| music          = Garth Neustadter Daniel Wohl
| cinematography = Pedro Gómez Millán Bruce Thierry Cheung
| editing        = Jennifer Ruff
| studio         = RabbitBandini Productions
| distributor    = Starz Digital Media
| released       =  
| runtime        = 72 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

The Color of Time (aka Tar) is a 2012 drama film written and directed by twelve New York University film students Edna Luise Biesold, Sarah-Violet Bliss, Gabrielle Demeestere, Alexis Gambis, Shruti Ganguly, Brooke Goldfinch, Shripriya Mahesh, Pamela Romanowsky, Bruce Thierry Cheung, Tine Thomasen, Virginia Urreiztieta and Omar Zúñiga Hidalgo.    It stars James Franco, Henry Hopper, Mila Kunis, Jessica Chastain and Zach Braff. It premiered on November 16, 2012 at the Rome Film Festival. It was released in the United Kingdom in 2014 under the new title Forever Love. The film was released in the United States in theaters and on demand beginning on December 12, 2014.

==Plot==
The different parts of Pulitzer Prize winner C.K. Williams life told through his poems. Flashbacks of his childhood, his teens, college years, to when he meets and marries his wife, Catherine (Kunis) and the birth of his children and parenthood. The film is narrated by different versions of Williams (Franco, Hopper, March, Unger), depicting the different aspects of Williams through the years. 

==Cast==
*James Franco as C.K. Williams - age 40   Variety|url=http://www.variety.com/review/VE1117948803/|publisher=Variety|accessdate=16 January 2013|date=25 November 2012}} 
*Henry Hopper as C.K. Williams
*Mila Kunis as Catherine
*Jessica Chastain as Mrs. Williams
*Zach Braff as Albert
*Bruce Campbell as Goody
*Giavani Cairo as Dan
*Vince Jolivette as Mr. Williams
*Jordan March as C.K. Williams
*Kathi J. Moore as Phyllis
*Ziam Penn as Ron
*Joshua Saba as John
*Mia Serafino as Sarah
*Zachary Unger as C.K. Williams - age 7
*Danika Yarosh as Irene

==References==
 

==External links==
* 

 
 
 