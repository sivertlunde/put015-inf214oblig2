Nur Kasih The Movie
 
 
{{Infobox film
| name            = Nur Kasih The Movie
| image           = Nur Kasih The Movie.jpg
| director        = Kabir Bhatia
| producer        = Kabir Bhatia Mira Mustaffa Francis Foo Leng Lai Ahmad Puad Onah Tengku Iesta Tengku Alaudin
| writer          = Mira Mustaffa
| starring        = Remy Ishak Tiz Zaqyah Fizz Fairuz
| country         = Malaysia
| language        = Malay
| runtime         = 93 minutes
| released        =  
| studio          = Filmscape Grand Brilliance Juita Viden
| budget          = RM 2.83 million
| gross           = RM 4,930,000
}}

Nur Kasih is a romance film that premiered on 19 May 2011 in Malaysia.

==Plot==
The continuation of the story of Adam (Remy Ishak), Aidil (Fizz Fairuz) and Nur Amina (Tiz Zaqyah) from television series. The storyline movie begins with a view of the desert in Jordan, appeared two Bedouin Arabs are walking the street and suddenly startled by a freak accident a train collided with a vehicle.

Story continues in the village where Nur Amina and Adam with their nostalgic of past memories. Wife of Aidil, Aliya had died of an accident and he left two children, Ilyas, 7 years old and Mariam, 5 years . When Aidils wife death he become sadness and depression and he became a single parent to raise a child alone.

Ilyas gets older and he understood the sadness and loneliness that his father suffered. Mariam is still relatively immature and not yet understood as it is still small when his mothers death.

Nur Amina confirmed pregnant and it is the happiest news for Adam, because they wait for a long time to have a baby. Adam and Nur involved for guide children to participate in the shelter, there is the character of Sara Ali as a wild teenager who put heart and soul to Adam. Shafie Naswip as a lover to Sarah Ali is a wild youth too.

Finally, trials will come upon him again, Nur miscarriage and their son death. Adam feel very sad and almost lost faith in God because of that trials. Adam and Nur Amina was determined to go to Mecca for pleasure and closeness with God, they both want to continue education in Jordan. Aidil replace Adam and Nur task of guiding children in the shelter wildlife.

Adam and Nur was very happy there, but Adam was hit by a recurring dream about a bad luck to come. Adam had a dream that Nur died in her lap and he was very concerned if a tragedy occurs.

==Cast==
===Main Cast===
* Remy Ishak as Adam Hj. Hassan
* Tiz Zaqyah as Nur Amina Abu Bakar
* Fizz Fairuz as Aidil Hj. Hassan

===Extended cast===
* Liza Othman as Hajjah Khadijah
* Ayu Raudhah as Alia
* Muniff Isa as Mamat
* Syafie Naswip as Jamal
* Sara Ali as Juriah
* Mia Sara Nasuha as Mariam
* Mohd Ilyas Suhaimi as Ilyas

===Cameo===
* Dato Rahim Razali as Aidil old
* Bonda Afida Es as Amina old
* Mazian Ahmad as Adam old
* Beto Kusyairy as Ustaz Wahid
* Zain Hamid as Saif
* Jehan Miskin as Iskandar
* Norman Hakim as Ilyas old
* Naeim Ghalili as Jordanian Officer

==Reception==
The film still retains the same elements as a series of television, the beauty of the cinematography to pull this film. Unfortunately, the end of the self-Nur Kasih looks quite berterabur. For those who do not follow the first television series, the audience looked a little bit difficult to know the storyline provided. Acting given by the three main proponents (Remy Ishak, Tiz Zaqyah, Fizz Fairuz) successfully moving the audience. The use of CGI in this film was inserted. Unfortunately, it is not handled well. Located to the audience to evaluate the film. 

==Soundtrack==
{{Track listing
| extra_column= Artist
| title1      = Nur Kasih
| extra1      = Tiz Zaqyah ft. Yassin
| length1     = 3:44
| title2      = Bertemu Cinta
| extra2      = Mila Jirin
| length2     = 3:30
| title3      = Tanpamu
| extra3      = Helena Adrian ft. Sabhi Saddi
| length3     = 4:15
| title4      = Usah Pergi
| extra4      = Bob Yusof
| length4     = 3:20
| total_length  = 14:09
}}

==Awards and nominations==
{| class="wikitable"
|-
! Year
! Award
! Category
! Nominated work
! Result
|- 2011
| rowspan=2|Anugerah Skrin 2011
| Best Film Actress Tiz Zaqyah
|  
|-
| Best Film Act Mira Mustaffa
|  
|- 24th Malaysia Film Festival
| Best Men Actor Remy Ishak
|  
|-
| Best Women Actress Tiz Zaqyah
|  
|-
| Best Act Mira Mustaffa
|  
|-
| Best Story Point
|  
|-
| Best Cinematography Norhanisham Muhamad
|  
|-
| Best Music Score Hafiz Hamidun
|  
|-
| Best Sound Arrangement Dustin
|  
|-
| Best Costume Designer Faizal Abdullah
|  
|-
| Best Theme Song Nur Kasih "(Tiz Zaqyah with Yassin)"
|  
|-
| Best Promising Actress Sara Ali
|  
|-
| Best Child Actor Mohd Ilyas Suhaimi
|  
|-
| rowspan=6|Profima Awards 
| Best Director Kabir Bhatia
|  
|-
| Best Key Grip Mohd Yunus Mohd Hanafiah
|  
|-
| Best Camera Operator Shahizi Osman
|  
|-
| Best Cinematography
|  
|-
| Best Focus Puller Ismail Abu Bakar
|  
|-
| Best Film Nur Kasih The Movie
|  
|- 2012
| rowspan=8|Blockbuster Awards
| Best Film Award
|  
|-
| Best Director Award Francis Foo, Kabir Bhatia & Mira Mustaffa
|  
|-
| Best Musical Drama Award Nur Kasih The Movie
|  
|-
| Best Theme Songs Award Nur Kasih "(Tiz Zaqyah with Yassin)"
|  
|-
| Best Hero Award Remy Ishak
|  
|-
| Best Heroine Awards Tiz Zaqyah
|  
|-
| Best New Hero Award Syafie Naswip
|  
|-
| Best New Heroine Award Sara Ali
|  
|-
| rowspan=2|Shout! Awards    
| Best On Screen Chemistry Award Tiz Zaqyah and Remy Ishak
|  
|-
| Breakthrough Local Feature Award Nur Kasih The Movie
|  
|}

==References==
 

==External links==
*   di Sinema Malaysia
*   di FINAS

 
 
 
 