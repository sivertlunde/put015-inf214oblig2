Suryamukhi
{{Infobox film
| name           = Suryamukhi
| image          =
| caption        =
| director       = Prafulla Sengupta
| writer         = Gurukrushna Goswami,   Soumendra Misra
| starring       = Soumendrs Misra,   Minati Misra,  Sukhalata Mohanty, Dhira Biswal
| producer       = Saumendra Misra
| music          = Shantanu Mahapatra
| cinematography = Kanai Dey
| editing        = Sukumar Sengupta
| studio         = Uday Pictures
| distributor    =  
| released       =  
| country        = India
| runtime        =  Oriya
}}
 Oriya film directed by Prafulla Sengupta.       

==Plot==
Raju lives with his brother Mohan and mother in a remote village. Gita and Raju loves each other. As Raju is a brilliant student goes to Cuttack for higher education. There he meets Usha, his class mate. Usha inspired by the honesty, simplicity and intellect of Raju and falls love with him. But Ushas father Rai Bahadur, a wealthy person in the town, doesnt accept his daughters love interest with a poor village boy. Rai Bahadur insulted Raju and thertain him to leave away Ushas life. In a desperation Raju leaves Cuttack and ventures to an unknown journey. In the meanwhile Rajus widowed mother send Loknath and Mohan to search whereabout of Raju. Loknath meets Rai Bahadur, who happens to his old friend but cant trace out Raju. Lokanath pursues Rai Bahadur a proposal of Mohans marriage with Usha. Rai Bahadur agrees. Mohan and Ushas marriage takes place.

Raju eventually finds a job in a factory and one day lost his eyes in an accident. by knowing this his mother gets terrible blow and passes away. Raju admits in a hospital and become fully cured. Raju returns his home and surprisingly find his girlfriend Usha is now his Sister-in-law. Later Usha convinces Raju to face the reality and asks him to marry Gita. At last Raju agrees to marry Gita with a happy ending.

==Cast==
* Soumendra Misra   as   Raju
* Meenati Misra    as   Usha
* Sukhalata Mohanty  as  Gita
* Dhira Biswal   as Lokanath
* Ananta Mahapatra   as Debasis
* Laxmipriya Mahapatra    as Odishi dancer
* Samuel Sahu   as Mohan
* Bimal Choudhury   as Rai Bahadur
* Lakshmi   as Rajus mother

==Soundtrack==
The music for the film is composed by Shantunu Mahapatra.  
{| class="wikitable sortable" style="text-align:center;"
|-
! Song
! Lyrics
! Singer(s)

|- Antara Kande Pranab Patnaik
|- Duniare Samayara Manna Dey
|- Sei Chuna Lata Mangeshkar
|- Sei Nila Sikandar Alam, Nirmla Mishra
|-
|}   

==Box office==
. The film appreciated by mass and was a success. 

==Awards==
*  10th National Film Awards (India) 1962
** National Film Award for Best Feature Film in Oriya

   

==References==
 

==External links==
*  
*  

 
 
 
 