Speak Easily
{{Infobox film
| name           = Speak Easily
| image          = DVD cover of the movie Speak Easily.jpg
| image_size     =
| caption        = DVD cover
| director       = Edward Sedgwick
| producer       =
| writer         =
| narrator       =
| starring       = Buster Keaton Jimmy Durante Thelma Todd
| music          =
| cinematography =
| editing        =
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 80 minutes
| country        = United States
| language       = English
| budget         =
}} A Night At The Opera.
 

==Plot==
Prof. Post (Buster Keaton) is a shy Classics professor at Potts College, who has lived a sheltered life and has little experience of life outside of academia. Feeling that the professor should see more of the real world, his assistant tricks the professor into thinking that he has inherited $750,000, allowing the professor to leave academia and see the world.

Boarding a train bound for[New York City, Prof. Post encounters James (Jimmy Durante), the manager of a dancing troupe that has an engagement in the backwater town of Fishs Switch. The professor becomes infatuated with one of the dancers, Pansy Peets (Ruth Selwyn), and accidentally alights at Fishs Switch when attempting to learn her name. He attends a performance by the dancing troupe at the local theatre, and is impressed by their act.

Feeling that the troupe should continue their act, Prof. Potts finances the troupe and takes them to perform on Broadway theatre|Broadway, but only after James insists that the act be improved to a higher standard. Potts suggestions of using inspiration from Ancient Greece are taken on board, with some minor alterations, and the show is turned into a grandiose musical revue. Although Potts wishes that Pansy be the leading lady, the show is quickly turned into a star-vehicle for spoiled actress Eleanor Espere (Thelma Todd), who attempts to win over the professor in order to take total control over both the show and the money it is expected to earn at its debut. Pansy attempts to warn the professor of Eleanors bad influence, with mixed results.

On the night of the shows debut, James discovers that the professor does not really have the $750,000 he believes to possess, and attempts to keep the professor away from the production for fear of ruining it. The professor stumbles on-stage at several points, amusing the audience who think it to be part of the act. However, his antics cause Eleanor to throw a tantrum, and the professor can finally admit his love to Pansy.

==Cast (in credits order)==
*Buster Keaton as Professor Post
*Jimmy Durante as James
*Ruth Selwyn as Pansy Peets
*Thelma Todd as Eleanor Espere
*Hedda Hopper as Mrs Peets
*William Pawley as Griffo
*Sidney Toler as Stage Director
*Lawrence Grant as Dr Bolton
*Henry Armetta as Tony
*Edward Brophy as Reno

==Production==
A myth persists that the Keaton talkies were critical and popular failures that virtually finished Keatons career. While it is considered true that these later Keaton efforts were artistically inferior to his best work, many of them were solid moneymakers. In reality, it was Keatons increasingly vocal discontent, with the exhausting workload, and growing drinking problem that led to the studio letting him go. 

MGM supposedly never copyrighted this film, hence its long existence in the Public Domain.

==DVD release==
"Speak Easily" exists in several versions on US DVD:  from Alpha Video in 2004, from Synergy Ent. in 2007, from Reel Classics in 2007 and 2008, and in a double-bill with "Steamboat Bill Jr." from East West Entertainment.  The first ever DVD release in the UK is from Powis Square Pictures in January 2009.

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 