Mysterious Skin
 
{{Infobox film
| name = Mysterious Skin
| image = Mysterious skin.jpg
| image_size = 220px
| alt = 
| caption = Theatrical release poster
| director = Gregg Araki
| producer = Gregg Araki
| screenplay = Gregg Araki
| based on =  
| starring = Joseph Gordon-Levitt Brady Corbet Michelle Trachtenberg Mary Lynn Rajskub Elisabeth Shue
| music = Harold Budd Robin Guthrie
| cinematography = Steve Gainer
| editing = Gregg Araki
| distributor = Tartan Films
| released =  
| runtime = 105 minutes  
| country = United States Netherlands
| language = English
| budget =
| gross = $1,524,966 
}} premiering at the 61st Venice International Film Festival in 2004, although it was not more widely distributed until 2005.

Mysterious Skin tells the story of two pre-adolescent boys who are sexually abused by their baseball coach, and how it affects their lives in different ways into their young adulthood. One boy becomes a reckless, sexually adventurous male prostitute, while the other retreats into a reclusive fantasy of alien abduction.

==Plot== sexually abused by their baseball coach. Both boys are targets for abuse due to their dysfunctional families: Neils single mother, Ellen, is neglectful and preoccupied with a string of boyfriends, while Brians parents are on the verge of divorce.
 homosexual proclivities at an early age—he was fascinated with male models depicted in his mothers Playgirl magazines. He interprets the coachs abuse as an initiation into sexuality and becomes sexually compulsive, being mainly attracted to Bear (gay culture)|bearish, middle-aged men. Neil begins to prostitute himself at the age of fifteen. Eventually Neil leaves home, drifts into petty crime, and becomes a prostitute in New York City. His friend Wendy, who harbors an unrequited crush, describes Neil as having not a heart, but "a bottomless black hole, that you fall into." Neil does begin to show a compassionate side, particularly following an encounter with a man who was dying from AIDS, and wanted nothing more than a backrub, only to feel touched. The encounter leaves Neil to withdraw from his life as a prostitute and take a job as a cashier. One day on his way home, he is tricked by a large man into giving him a ride home. The man makes him snort cocaine before raping and beating him.
 abducted by aliens. At the age of 18, Brian meets a young woman named Avalyn who also believes she was abducted by aliens. They begin to form a fragile friendship; though, when she takes a romantic interest in Brian and touches him sexually, he reacts with intense panic and refuses to speak to her again.
 groomed both boys to make the abuse seem normal and acceptable, and how a bluish porch light shining through the bedroom window gave the abusive incidents an eerie atmosphere. He then told Brian that the coach made them kiss each other before he kissed them both and then made them perform sexual acts on him. Brian breaks down and collapses into Neils arms as Christmas carolers sing Silent Night.

==Cast==
* Joseph Gordon-Levitt as Neil McCormick
** Chase Ellison as young Neil McCormick
* Brady Corbet as Brian Lackey 
** George Webster as young Brian Lackey  
* Michelle Trachtenberg as Wendy 
** Riley McGuire as young Wendy
* Jeff Licon as Eric Preston
* Mary Lynn Rajskub as Avalyn Friesen
* Elisabeth Shue as Ellen McCormick
* Bill Sage as Coach
* Chris Mulkey and Lisa Long as Mr. and Mrs. Lackey
* Richard Riehle as Charlie
* Kelly Kruger as Deborah
** Rachael Nastassja Kraft as young Deborah
* Billy Drago as Zeke

==Production==
Both   (2001).    Made on a low budget, filming commenced in August 2003 and lasted only three weeks, which gave the cast and crew no possibility of doing retakes. 

A number of measures were taken to avoid exposing the child actors to the sexual and abusive aspects of the story. Although their parents were given the entire shooting script to review, the boys were given separate scripts which included only the activities they would be performing, and their roles and the characters relationships were explained to them in innocent terms. All of the sexual abuse involving children is implied rather than being directly depicted, and the scenes in which this seduction and abuse takes place were filmed with each actor performing alone and addressing the camera rather than the other actor, then edited together, so the children did not see or hear the performance by the adult actor playing the abuser. (This subjective approach to filming was consequently used in various places throughout the film.)  

==Reception==
The film received an 84% "fresh" rating on review aggregator  , which uses a Weighted mean|"metascore", the film holds 73 out of 100 based on 32 reviews.  

Lou Lumenick from the New York Post commented, "Not for the squeamish, but it is a beautifully crafted and thoughtful film that genuinely provokes."     Ella Taylor from LA Weekly wrote “A warped, but beautiful and strangely hopeful, coming-of-age tale.”  Roger Ebert  gave Mysterious Skin 3.5 out of a possible 4 stars, describing it as "at once the most harrowing and, strangely, the most touching film I have seen about child abuse". Steven Rhea of The Philadelphia Inquirer awarded the film 3 out of 4 stars, stating that "Mysterious Skin" ultimately "manages to deal with its raw, awful subject matter in ways that are both challenging and illuminating".  Gordon-Levitt was praised by critics for his performance, and the actor has stated that people on the streets had come up to him to applaud his performance in the film.  His portrayal of a teenage hustler inspired director Scott Frank to cast him in The Lookout (2007). 

According to psychologist Richard Gartner,  the novel Mysterious Skin is an uncommonly accurate portrayal of the long-term effect of child sexual abuse on boys.

===Rating issues===

The US MPAA rated the film NC-17, which the studio appealed unsuccessfully.  The film was released in the US without rating. 

The film was the subject of some controversy in   expressed that the film does more for the case against pedophilia, stating: "People who do indulge in crimes like that, if they saw this film they would understand the damage that they do." 

==Soundtrack==
 
The film score was composed by Harold Budd and Robin Guthrie.

Other songs include:
# "Golden Hair" – Slowdive (written by Syd Barrett) Curve
# "Game Show" – Dag Gabrielsen, Bill Campbell, Nelson Foltz, Robert Roe
# "Catch the Breeze" – Slowdive
# "Crushed" – Cocteau Twins
# "Dagger" – Slowdive
# "I Guess I Fell in Love Last Night" – Dag Gabrielsen, Alex Lacamoire
# "I Could Do Without Her" – Dag Gabrielsen, Alex Lacamoire Ride
# "O Come All Ye Faithful" – Tom Meredith, Cydney Neal, Arlo Levin, Isaiah Teofilo
# "Away in a Manger" – Tom Meredith, Cydney Neal, Arlo Levin, Isaiah Teofilo
# "Silent Night" – Tom Meredith, Cydney Neal, Arlo Levin, Isaiah Teofilo, Evan Rachel Wood, John Mason
# "Samskeyti" – Sigur Rós
# "Blue Skied an Clear" – Slowdive

==Awards==
* 2004 Bergen International Film Festival – Jury Award 
* 2006 Polished Apple Awards – Best Movie 
* 2006 Icelandic Queer Film Festival – Best Fictional Work 

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  
*   at  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 