Muktir Kotha
{{Infobox film
| name           = Muktir Kotha
| image          = File:Muktir Kotha (1999).png
| image_size     = 250px
| border         = 
| alt            = 
| caption        = 
| film name      = Words of Freedom
| director       = Tareque Masud   Catherine Masud
| producer       = Tareque Masud
| writer         = 
| screenplay     = 
| story          = 
| based on       = Bangladesh Liberation War
| narrator       = 
| starring       = 
| music          = 
| cinematography = Mishuk Munier
| editing        = Catherine Masud   Fauzia Khan
| studio         = Audiovision
| distributor    = 
| released       = 1999
| runtime        = 80 minutes
| country        = Bangladesh Bengali
| budget         = 
| gross          = 
}}

Muktir Kotha (  directed by Tareque Masud and Catherine Masud. Muktir Kotha follows a group of young men and women began traversing the far corner of Bangladesh projectionists from 1996-1999, showing Muktir Gaan, a documentary on the 1971 Bangladesh Liberation War. The film screenings prompted ordinary villagers to share their own stories of wartime suffering and resistance. Often the projection space would be spontaneously transformed into a folk concert. Through these interactions with village audiences, the young projectionist came to relearn the wider history of the Liberation War, and the continuing struggle of ordinary people for a more just and democratic society. 

== Synopsis ==

Muktir Katha is a film about the liberation struggle of 1971. The film is an archive of the ways in which ordinary people fell victim to genocide, rape and other atrocities. The struggle still ranging in the countryside, and struggle for a more just and democratic society.  The combined footage shot used in the film was taken from American film maker Lear Levin. 

==DVD==
The DVD of Muktir Kotha was released on 25 March 2012 by Laser Vision. It is thought to be the first interactive DVD in Bangladesh.  

==References==
 

==External links==
* 

 

 
 
 
 
 
 