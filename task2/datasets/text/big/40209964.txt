Crush (2013 film)
{{Infobox film
| name           = Crush
| image          = Crush2013poster.jpg
| caption        = Theatrical released poster
| director       = Malik Bader
| producer       = Trevor Macy Marc D. Evans
| writer         = Sonny Mallhi
| starring       = Lucas Till Crystal Reed Sarah Bolger Caitriona Balfe Reid Ewing Leigh Whannell Holt McCallany Camille Guaty
| music          = Julian Boyd
| cinematography = Scott Kevan
| editing        = Jeff W. Canavan
| studio         = Intrepid Pictures
| distributor    = FilmNation Entertainment
| released       =  
| runtime        = 94 minutes   
| country        = United States
| language       = English
| budget         = $1.2 Million
| gross          = $8,123,213
}} VOD outlets and direct-to-video on DVD and Blu-ray in the United States on April 9, 2013;  it is rated Motion Picture Association of America film rating system#PG-13|PG-13 for "disturbing thematic material, violence, sensuality, some teen drinking, and language".

The films tagline is "Careful who you wish for", and its plot has been summarized as "A secret admirers crush on a high school athlete takes a fatal turn." 

==Plot==
A promising high school soccer player, Scott (Lucas Till), injures his knee in a game and doctors say it might take up to two years for his knee to heal. fast forward to 6 months later, Jules likes him but Scott is fixated on recovering his physical condition and considers her his best friend. Bess (Crystal Reed) has a crush on Scott, and works in a store owned by David with her mature colleague Andie (who also has a crush on Scott). When Scott is stalked by a mysterious person that threatens Jules, he believes that Bess is responsible for the weird situations. Things begin to get worse and worse as the stalker takes extreme measures that humiliate Jules, among others. In the end, the stalker turns out to be Andie. Bess dates Jeffrey, a guy who has a crush on her and Scott reconciles with Jules. He reciprocates her feelings and they kiss.  

==Cast==
*Lucas Till as Scott Norris
*Crystal Reed as Bess
*Sarah Bolger as Jules Lindstrom
*Caitriona Balfe as Andie
*Meredith Salenger as Besss Mother
*Leigh Whannell as David
*Camille Guaty as Ms. Brown
*Holt McCallany as Mike Norris
*Michael Landes as Dr. Graham
*Reid Ewing as Jeffrey
*Nikki SooHoo as Maya
*Isaiah Mustafa as Coach
*Kristin Quick as Goth Girl
*Preston Davis as Casey
*Ashleigh Craig as Young Girl

==Critical reception==
Writing for Fearnet|FEARnet, Scott Weinberg praised the acting of the three leads but criticized the films "paper-thin premise" and script for being derivative (pointing out Mallhis previous work on 2011s The Roommate), and said "Even putting aside the casual plagiarism in Mallhis screenplays, theres nothing here in the way of character, suspense, intensity, or surprises. Its just someone telling the same old story with slightly different character."  Ian Sedensky of Culture Crypt gave the film a score of 45/100 and judged: "The young cast is attractive, the production is well put together, and the twist, however absurd, might be genuinely shocking and entertaining to some. But this is a movie about a crush. And buying into the thin motivations behind this obsession requires a level of crazy reserved exclusively for the characters in this movie."  John Strand of Best-Horror-Movies awarded the film 2.5/5 "Freakheads" (the sites rating system), and concluded the film is "A neat little thriller that will appeal to teens and young adults who are not interested in scares and gore, but like a fairly executed, albeit predictable, storyline." 

==References==
 

==External links==
* 
* 

 
 
 
 