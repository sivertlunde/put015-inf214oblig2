Out of Athens
 
  gay pornographic film from Falcon Studios, starring John Brosnan and directed by Johnny Rutherford.  It is split into two parts: part one runs 90 minutes,  and part two runs 85 minutes. 

== Plot ==
Note: Names of real-life performers are substituted for character names.

A college janitor Johnny Brosnan and his friends want to join the fraternity. They are accepted by the fraternity club except Brosnan, solely due to his status. Later, Brosnan goes to the cruise ship and steals cash and a Harvard University sweatshirt from his casual interest Travis Wade. When the ship lands at Greece, Brosnan, posing as a Harvard alumnus, is invited by one of Harvard alumni (Lindon Hawk) to their reunion party. Then he meets Ronald Dane, the Greek resident who helps him tour around, and then is invited  to stay at the place of Danes cousin George Vidanov and Vidanovs partner Joe Calderon.  At the Harvard reunion, Brosnan is exposed as an imposter by Wade, who just arrives, and is gang raped. Later, Dane goes out looking for him, finds him "dirty and disheveled" at the streets, and manages to retrieve him back to his cousins place. At the end, Brosnan and Dane make passionate love.  

=== Scenes ===
Part one: 
# Fraternity orgy: Cameron Fox, Hans Ebson, Jeremy Tucker, Jeremy Jordan, Emilio Santos, Tristan Paris, Billy Kincaid, Seth Adkins, Nick Young
# Cruise ship: Johnny Brosnan and Travis Wade
# Greek tour: Johnny Brosnan and Roland Dane
# Threesome: Alexei Gromoff (credited as Thomas Williams), Jeffrey Dickinson, José Ganatti  (Some copies omit this scene.) 
# Terrace threesome: Dean Phoenix, Franco Corsini, Eric Leneau; Johnny Brosnan participates for a short while and then leaves.

Part two: 
# Residence: Joe Calderon and George Vidanov; Johnny Brosnan witnesses the whole scene
# Flashback: Colby Taylor and Eric Hart
# Gang rape at reunion party: Johnny Brosnan against Colby Taylor, Travis Wade, Robert Balint, Erik Hanlan, Lindon Hawk, Matt Spencer, David Moretti, Ryan Michaels
# Finale: Johnny Brosnan and Roland Dane

== Production ==
Out of Athens was directed by John Rutherford, the past executive vice president and president of Falcon Studios,  and shot on film and videotape by Todd Montgomery in California and Greece.    Its story is loosely based on Rutherfords youthful experiences.  Rutherford said, "Out of Athens was my story  I decided to take three months off and go to Europe after high school. And I traveled around Greece...I did a loose story   on my travels and the conflict of being who you are." 

== Reception == The Talented Mr. Ripley." 

The fraternity orgy scene was awarded the "Best Group Scene" at the 2001 Grabby Awards and the 2001 GayVN Awards.  John Rutherford and Todd Montgomery were awarded, respectively, the "Best Director"  and the "Best Videography" at the 2001 GayVN Awards.   The film was awarded the "Best Video" in the 1999 Probe/Men in Video Awards. 

== References ==
{{reflist|refs=
   
   
   
   
   
   
 {{cite book |first=Jeffrey |last=Escoffier |pages=280, 282
|url=http://books.google.com/?id=ovrTqZuHVPkC&lpg=PA282&dq=%22out%20of%20athens%22%20reviews%20falcon&pg=PA280#v=snippet&q=%22out%20of%20athens%22&f=false |year=2009 |title=Bigger Than Life: The History of Gay Porn Cinema from Beefcake to Hardcore |location=Philadelphia |publisher=Running Press Book Publishers |chapter=The Perfect Orgy |isbn=0786747536 }} 
   
}}

== External links ==
*  
*  

 
 
 
 