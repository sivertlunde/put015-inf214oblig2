The War on Kids
{{multiple issues|
 
 
}}

{{Infobox film
| name = The War on Kids
| director = Cevin Soling
| released =  
| runtime = 95 minutes
| country = United States
| language = English
}}

The War on Kids is a 2009 documentary film about the American school system. The film takes a look at public school education in America and concludes that schools are not only failing to educate, but are increasingly authoritarian institutions more akin to prisons that are eroding the foundations of American democracy. Students are robbed of basic freedoms primarily due to irrational fears; they are searched, arbitrarily punished and force-fed dangerous pharmaceutical drugs. The educational mission of the public school system has been reduced from one of learning and preparation for adult citizenship to one of control and containment.

==Synopsis== Zero Tolerance policies in public schools in the 1990s, which were designed to eradicate drugs and weapons at schools. By arbitrary application of this policy via unchecked authority, soon nail clippers, key chains, and aspirin were considered dangerous and violations of the rules. This policy, combined with Columbine-inspired fear, has resulted in kindergartners being suspended for using pointed fingers as guns in games of cops and robbers and students being suspended for having Midol and Alka-Seltzer. This policy has turned schools into Kafka-esque nightmares, absurd and demoralizing. Increasingly, issues once dealt with by the guidance counselor or a trip to the principal’s office are now handled by handcuffs and tasers in the hands of police. 

Students are denied basic constitutional rights. They can be searched, drug-tested, forced to incriminate themselves, and capriciously punished. Surveillance cameras, locker searches, and metal detectors are shown to be commonplace. Courts routinely uphold the school’s right to do whatever they choose, creating an atmosphere of fear and loathing, anger and despair. The physical structure of these institutions are themselves oppressive, resembling prisons in many ways, yet even more dreary. 

Ironically, the film shows that the drastic measures schools employ are ineffective as tools of protection. Security cameras did nothing more than film the Columbine massacre for news outlets. This oppressiveness does nothing to advance learning. Various teachers state on camera that this atmosphere is frustrating to work in, with all curriculum handed down from the state and that this “one-size-fits-all” approach doesn’t work well with human beings.   

Even more harmful than this physical oppression is the use and abuse of psychiatric tools. The rampant diagnoses of ADD and similar conditions are shown to be intimately connected to pharmaceutical companies’ promotional activities. The alleged disorder known as ODD - oppositional defiance disorder - is used to further control kids by serving as a gateway for further authoritative measures, often of the extreme kind.  Ritalin and other drugs are being over-prescribed. These strong drugs can have dire consequences, including suicide and murder. Some school shooters, including the Columbine killers, have used or been on these drugs.

This film touches on an area almost completely ignored in any discussion of education - the genesis of compulsory education. Public schools are modeled after a Prussian system, one geared towards creating compliant soldiers.  Later, it was modified during the industrial revolution to train people for the work force (hence the bells signaling movement).    Ultimately, the film argues that more money, smaller classrooms, better trained teachers and other bromides won’t produce effective education because the problems are deep and institutional. In director Cevin Solings words, “I was converted by teachers, by a number of people I interviewed is that the main mission of school is submission to authority.” 

==Awards==
Named the best educational documentary by the New York International Independent Film and Video Festival.

==Reception==
The documentary opened in November, 2009 on screens in select areas to favorable reviews. "A startling wake-up call about appalling conditions prevailing in American schools, The War on Kids contradicts popular wisdom," said Ronnie Scheib in the Daily Variety. Film critic Ella Taylor in the Village Voice called it a “lively documentary (that) lays out in hair-raising detail the authoritarian underpinnings of Americas child-centered culture, in which a pervasive climate of fear distorts perception of the dangers posed by and to children." 

Jeannette Catsoulis wrote in the New York Times, "A shocking chronicle of institutional dysfunction, The War on Kids likens our public school system to prison and its disciplinary methods to fascism. At least now you know why little Johnny won’t get out of bed in the morning." 

Writer Randy Cassingham’s review is somewhat more qualified than the above. He admits that “I dont agree with every aspect of the film, but I can tell you one thing: it sure made me think.” He adds a very timely recent post-script to The War on Kids film by citing an egregious abuse of constitutional rights and professional ethics as relates to a kids access to knowledge. 

The review of the documentary offered by Michael Haas, president of the Political Film Society, starts by pointing out that only two nations have refused to sign the Convention on the Rights of the Child, Somalia and the United States. They write “ . . . the media has focused on the waterboarding of three suspected terrorists . . . but not on the thousands of children who have been captured, incarcerated, deprived of parental contact, interrogated, and thereby abused to such an extent that they have been tortured with impunity. Why the disinterest in the rights of children within the United States? The apparent main reason, that American adults want to maintain control over children, is the thesis of the documentary The War on Kids.” 

The documentary touched a nerve in cultural critic Robert Barry Francos. He relates an incident in from his own life in middle school and contrasts that experience with the current atmosphere in today’s schools:  "I was standing on the lunchroom line. The much bigger kid behind me kept poking me in the ribs . . . It was painful . . . and I told him to stop . . .but he would not. When I finally had enough, I turned, jumped (he was tall), and punched him in the face. He was definitively more shocked than hurt. The lunchroom ladies came out see what was the ruckus. When they heard what happened, they gave me a free lunch and the kid had to stay on line. That was the end of it. If that incident were to happen now, posits the new film documentary The War on Kids, I more likely to have been suspended for a year, been taken to court in shackles, and had a felony conviction on my record.”  

==See also==
* Taking Children Seriously Paul Graham; addresses similar themes to this movie.

==References==
 

==External links==
*  
*  

 
 
 
 
 