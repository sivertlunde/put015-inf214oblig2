Pyaar Koi Khel Nahin
{{Infobox film
| name           = Pyaar Koi Khel Nahin
| image          = Pyaar_Koi_Khel_Nahin.jpg
| image_size     =
| caption        =
| director       = Subhash Sehgal
| producer       = Sibte Hassan Rizvi
| writer         = Subhash Sehgal
| narrator       =
| starring       = Sunny Deol   Apoorva Agnihotri   Mahima Chaudhry
| music          = Jatin Lalit
| cinematography = Harmeet Singh
| editing        = Subhash Sehgal
| distributor    = Columbia Pictures
| released       = August 1, 1999
| runtime        = 165 min.
| country        = India
| language       = Hindi
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
 Indian Bollywood film directed by Subhash Sehgal and produced by Sibte Hassan Rizvi. It stars Sunny Deol, Apoorva Agnihotri and Mahima Chaudhry in pivotal roles.  

==Plot==

Sunil (Apoorva Agnihotri) and Ashok are two close friends. They are of marriageable age. One day Ashoks mom tells him to introduce himself to Shalu. But Ashok is reluctant to do so. He asks Sunil to impersonate him, and somehow reject Shalu. That way his mom will be satisfied and not pester him any more. In the meantime, Shalu and Nisha (Mahima Chaudhry) have also decided like-wise, and planned to fool Sunil. But when the two couples meet, Sunil and Nisha fall in love, but are unable to tell each other of their impersonations. When they do so, they are already in love, and after meeting each others parents, they get married. Enter Sunils elder brother Anand (Sunny Deol), and complications abound. Anand is involved in criminal activities, he wants to give up those, but his partners will not allow him to do so. Nisha works for Anand and he is in love with her and would like to marry her, but when he finds out that his brother loves her he steps down. Sunil and Nisha get married and have a kid but then Sunil is killed by Anands business partners who believe it to be snail. Nishas parents want her to get married again so Anand marries her as he still loves her. Anand then finds out that Sunil is alive and brings him back home even though Sunil had left because he knew that Anand loved Nisha. Anand is killed by the bad guys and Sunil and Nisha live happily ever after.

==Cast==
* Sunny Deol...Anand
* Apoorva Agnihotri...Ashok / Sunil
* Mahima Chaudhry...Nisha
* Kulbhushan Kharbanda...Ashok
* Dolly Bindra...Gudei
* Mohnish Bahl
* Dalip Tahil
* Reema Lagoo

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Apni To Life Main"
| Asha Bhosle
|-
| 2
| "Kuch Hamare Hain"
| Alka Yagnik, Udit Narayan
|-
| 3
| "Ladke Ne Ladki" Abhijeet Bhattacharya|Abhijeet
|-
| 4
| "Nazar Milte Hi"
| Alka Yagnik, Vinod Rathod
|-
| 5
| "Pyaar Koi Khel Nahin"
| Kumar Sanu
|-
| 6
| "Pyaar Koi Khel Nahin (Sad)"
| Kumar Sanu
|-
| 7
| "Tere Galon Ki"
| Alka Yagnik, Udit Narayan
|-
| 8
| "Yaad Piya Ki"
| Falguni Pathak
|-

|}

==References==
 
Arbaaz Khan was offered Apoorva Agnihotris role but opted out later

==External links==
* 

 
 
 
 