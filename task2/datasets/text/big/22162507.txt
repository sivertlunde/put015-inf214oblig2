Slayer (film)
{{Infobox television film 
| name            = Slayer
| image           =
| caption         = 
| director        = Kevin VanHook
| producer        = Karen Bailey Morris Berger Stephen Brown John W. Hyde
| writer          = Kevin VanHook
| starring        = Casper Van Dien Jennifer ODell Tony Plana Kevin Grevioux Danny Trejo Lynda Carter
| music           = Ludek Drizhal
| editing         = Kevin VanHook Sci Fi Channel Anchor Bay Entertainment
| release      = 2006-07-08 (USA)
| runtime         = 88 minutes
| country         = United States
| language        = English
| budget          = $2 million (estimated)
}}
 vampire themed Sci Fi Channel, directed and written by Kevin VanHook, starring Casper Van Dien and Kevin Grevioux, about an elite commando squad in South American rain forest, dealing against a deadly and bloodthirsty vampire clan living in the forest.

==Plot summary==
The plot of Slayer concerns a group of US military soldiers led by Hawk (Van Dien), who are sent to South America, where they must fight vampires. However, one of the members of the squad, Grieves, is transformed into a vampire, forcing him to battle his former friend Hawk.

==Cast==
* Casper Van Dien - Hawk
* Kevin Grevioux - Grieves
* Ray Park - Acrobatic Twins
* Danny Trejo - Montegna
* Tony Plana - Javier
* Pablo Espinosa - Jimmy
* Alexis Cruz - Alex
* Jennifer ODell - Dr. Laurie Williams
* Merritt Bailey - Merritt
* Ski Carr - Guillermo
* Lynda Carter - Colonel Jessica Weaver
* Joyce Giraud - Estrella

==Development==
Slayer was filmed in Puerto Rico on a $2,000,000 budget. The company Tata Elxsi Visual Computing Lab handled the post production. The film premiered on the Sci-Fi channel on 8 July 2006. 

==Release==
Slayer was released on region 1 DVD in the United States on November 21, 2006, distributed by Anchor Bay Entertainment. The DVD included subtitles and Dolby Digital 5.1, as well as special features including a commentary by Van Dien and the director, and a photo and concept art gallery. 

==Reception==
The film received negative reviews from critics.  Dread Central awarded the film two out of a possible five mugs of blood and said "Slayer is pretty much what you’d expect; a bit dull, full of hammy acting, logic gaps you could drive a truck through, and ridiculously sparse on the special effects." The website Absolute Horror awarded the film two out of four stars. 

==Background== Underworld series Raze in the first Underworld film. Kevin VanHook also worked in DC Comics vampire-themed comics, Superman and Batman vs Vampires and Werewolves.

==See also==
* Vampire film

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 