Finding Graceland
{{Infobox film
| name           = Finding Graceland
| image          = 
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = David Winkler
| producer       = Cary Brokaw
| screenplay     = Jason Horwitch
| story          = {{plainlist|
* Jason Horwitch
* David Winkler
}}
| starring       = {{plainlist|
* Harvey Keitel
* Johnathon Schaech
* Bridget Fonda
* Gretchen Mol
}}
| music          = Stephen Endelman
| cinematography = Elliot Davis
| editing        = Luis Colina
| studio         = {{plainlist|
* Largo Entertainment
* TCB Productions
}}
| distributor    = 
| released       =  
| runtime        = 97 minutes
| country        = 
| language       = English
| budget         = 
| gross          = 
}}

Finding Graceland is a 1998 American film starring Harvey Keitel, Johnathon Schaech, Bridget Fonda, and Gretchen Mol. The film features a character who claims to be an alive-and-well Elvis, years after staging his death.

==External links==
*  

 
 
 
 
 


 