Lee Rock II
 
 
{{Infobox film
| name           = Lee Rock II
| image          = LeeRockII.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Film poster
| film name      = {{Film name
| traditional    = 五億探長雷洛傳II之父子情仇
| simplified     = 五亿探长雷洛传II之父子情仇
| pinyin         = Wǔ Yì Tàn Zhǎng Léi Luò Chuán Èr Zhī Fù Zǐ Qíng Chóu 
| jyutping       = Ng2 Yik1 Taam3 Zeong2 Leoi4 Lok6 Zyun2 Ji6 Zi1 Fu6 Zi2 Cing4 Sau4 }}
| director       = Lawrence Ah Mon
| producer       = Jimmy Heung Wong Jing
| writer         = 
| screenplay     = Chan Man Keung Chan Wah
| story          = 
| based on       = 
| narrator       =  Paul Chun
| music          = Chow Kung Sing
| cinematography = Gigo Lee Andrew Lau
| editing        = Kwong Chi Keung Yu Shun
| studio         = Wins Entertainment Golden Harvest
| released       =  
| runtime        = 111 minutes Hong Kong
| language       = Cantonese
| budget         = 
| gross          = HK$23,135,334
}}

Lee Rock II is a  , who is portrayed in the film as Lee Rock by Lau.

==Plot==
The film directly continues with Lee Rock, with a few minute recap of the main details of its predecessor.

In 1959, Yau Ma Tei foreign detective Reeve retires, which leads to a competition between Lee Rock and Ngan Tung for the position as Yau Ma Teis chief detective. Ngan Tung does not hesitate to collide with many of Hong Kongs businessmen and drug dealers in hopes of taking the throne. Lee Rock also refuses to back down and pleases to Commissioners wife, which the Commissioner also highly regards Lee. Although ultimately Ngan Tung gets the position as the chief detective of Yau Ma Tei, Lee Rock was also promoted as the Chief Chinese Detective, a newly created post, which puts Lee ahead of Ngan. After Lee takes office, he reforms the procedures of each department, gaining support from many businessmen and his momentum appears rises. As contrast to Ngan Tung, who is all plummeted, which makes his hatred toward Lee more stronger. Therefor, got King Crabs henchman to assassinate Lee, where Lee escaped unharmed. To avoid having Lee find out that he was the mastermind behind the assassination, Ngan kills Crab.

During the 1960s, Lee Rocks power is monstrous, not only does he control the entire Hong Kong Police Forces operations, but also actively invests in commercial real estate, entertainment and many other fields. He becomes a major figure with an estimated wealth of HK$500 million.
 ICAC was established and is committed to combating all forms of corruption, which leads to Lee Rocks idea of early retirement. Lee decides to transfer all of its assets in Canada and emigrating to Canada with his family.

In the aspect of his family, Roses arrival to Hong Kong coincides with the time of polygamy being abolished in Hong Kong. In order to avoid being the third party between Lee and his wife Grace, she chooses to bring his son Bill Lee away from Lee, where they settle in Australia. Rose works hard to raise Bill, and they come back to Hong Kong after Bill grows up. Bill also became a staff member of the ICAC. Bill learned a number of ways with that his father sinful, which causes his strong hatred towards his father. The conflict between the father and son causes Rose ill and hospitalized. When Lee Rock learns that Rose is dying, he rushes to the hospital but was hunted down at the same time. It was King Crabs younger brother form Netherlands, who came to avenge his brother when he heard the lie from Ngan that Lee killed Crab. Lee suffers from a gunshot wound and witnessed Roses death at the hospital. The killers also arrived at that time and Lee Rock and his son work together to kill of the killers, which also improves the relationship between the father and son.

In the end, a lot of corruption is still happening in Hong Kong, especially in the police force where various police officers have been arrested by the ICAC and treated by the law; while the wanted Lee Rock enjoys a stable life with his family in Canada.

==Cast==
*Andy Lau as Lee Rock
*Sharla Cheung as Grace Pak
*Aaron Kwok as Bill Lee
*Ng Man-tat as Lardo
*Chingmy Yau as Rose Paul Chun as Ngan Tung
*Charles Heung as Sergeant Lam Kong Michael Chan as King Crab James Tien as Silverfish
*Victor Hon as Hau
*Louis Roth as Commissioner Alan
*Wong Chi Keung as Detective Yeung
*Peter Chan as Kirin / Fire Dragon
*Jameson Lam as Rocks detective at station
*Cheung Tat-ming as Ng Hak Ping
*Hung Yan-yan as Shrimp Head / Lu
*John Wakefield as Translator for Commissioner Alan
*Dave Lam as Detective Tak
*Wai Ching as Sergeant at meeting
*Wong Siu Ming as Detective with gun at night club
*Danny Chow as One of Shrimp Heads Men
*Ridley Tsui as One of Shrimp Heads Men
*Wong Chi Keung as thug beating molester
*Ho Wing Cheung as thug beating molester
*Michael Dinga as ICAC officer
*Tam Wai Man as molester
*Jim James as police officer
*Lam Foo Wai as thug
*Lau Chi Ming as Shrimps thug at hospital
*Tsim Siu Ling as Shrimps thug at hospital
*Huang Kai Sen as Shrimps thug at hospital

==Box office==
The film grossed HK$23,135,334 during its theatrical run from 10 October to 20 November 1991 in Hong Kong.

==See also==
*Andy Lau filmography
*Aaron Kwok filmography
*Wong Jing filmography

==External links==
* 
*  at Hong Kong Cinemagic
* 
*  at LoveHKFilm.com

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 