Batuque, the Soul of a People
{{Infobox film
| name = Batuque, the Soul of a People
| image = Batuque, the Soul of a People.jpg
| image size =
| border =
| alt =
| caption = Theatrical poster
| director = Júlio Silvão Tavares
| producer = Marie Clémence Paes Luis Correia
| writer = Júlio Silvão Tavares
| music =
| sound editor = António Pedro Figueiredo
| cinematography = Cesar Paes
| editing = César Paes Agnès Contensou
| studio = LX Filmes Laterit Production R.F.O.
| distributor = LX Filmes (Portugal) Marfilmes (worldwide)
| released =  
| runtime = 52 minutes
| country = Cape Verde Portuguese French-based Creole English subtitles
| budget = Euro|€2,000
| gross =
}} batuque musical group Raiz di Tambarina, and roots of this musical genre in Santiago, Cape Verde.     

==Synopsis==
African slaves were first brought to Cape Verde by Portuguese settlers in 1462. These slaves brought with them the cultural rhythm and music which would become Batuque: a musical form punctuated by drums while participants danced in a circle. The dance, repressed during the Colonial era, has been adopted as a symbol of the Cape Verdan cultural identity. The film seeks to document the dance form through interviews and performance by the musical group Raiz di Tambarina.

==Cast==
 

==Production== pitched by Silvão in Senegal, filmed in Cape Verde and edited in France.   

==Reception==
 

==Release==
The film was screened in Lisbon in November 2010, with the filmmaker in attendance, before traveling to festivals in Brazil and the U.S.    It had previously screened at Africa in the Picture, Netherlands,  the Copenhagen International Documentary Festival, Denmark, the
24th International Documentary Film Festival Amsterdam, Netherlands,  the Africa in Motion film festival, Edinburgh, Scotland,  and the AfryKamera Film Festival, Poland.

==References==
 

==External links==
*   at the Internet Movie Database
*   at the Internet Movie Database

 
 
 
 
 
 
 


 