The Blade Spares None
 
 
{{Infobox film
| name           = The Blade Spares None
| image          = TheBlade71.jpg
| image_size     = 
| caption        = 
| director       = Teddy Yip Wing Cho
| producer       = Raymond Chow Man Wai
| writer         = Lo Wei, Ni Kuang
| narrator       = 
| starring       = 
| music          =
| cinematography = 
| editing        =  Golden Harvest
| release        = 1971
| runtime        = 102 minutes
| country        = Hong Kong Mandarin
| budget         = 
}}
 1971 Hong Kong action film with sword fighting directed by Teddy Yip Wing Cho.

==Plot==

Ho Li-Chun, an attractive but powerful swordswoman, fights at a tournament at Prince Kueis Palace. A knight, Chen Jo-Yu, is defeated but later returns with another knight, Tang Ching-Yun, who is in possession of a peculiar sword. Ho recognizes this as the weapon once used by Sun Tien-Chen, an enemy of her family. Investigating the case, she learns that Prince Kuei was actually murdered, and it is Sun who has assumed his identity as an imposter. Ho, in coordination with Chen and Tang, plan to confront him and eventually attack the palace.

==Cast==
*Nora Miao
*Patrick Tse James Tien 
*Paul Chang Chung
*Feng Yi
*Chiang Nan
*Ng Ming Tsui
*Lee Wan Chung
*Kwan Shan
*Eddie Ko Hung
*Sammo Hung
*Lam Ching Ying
*Wilson Tong
*Jackie Chan

==External links==
* The Blade Spares None at  
*  

 
 
 
 
 
 
 


 
 