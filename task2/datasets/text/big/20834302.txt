The Clinic (1982 film)
 
{{Infobox Film name           = The Clinic  caption        = 
| image	=	The Clinic FilmPoster.jpeg producer       = Robert Le Tet Bob Weis director  David Stevens  writer         = Greg Millin starring       = Chris Haywood Simon Burke Gerda Nicolson music          = Red Symons cinematography = Ian Baker editing        = Edward McQueen-Mason distributor    = Village Roadshow released       = 17 September 1982 (Toronto Film Festival) 28 February 1985 (USA) runtime        = 93 minutes country        = Australia language       = English  budget         = A$1 million 
| gross = AU $414,000
}} David Stevens. VD clinic focusing on four doctors and their patients. It was filmed in Melbourne, Australia and Deniliquin, Australia. David Stratton, The Avocado Plantation: Boom and Bust in the Australian Film Industry, Pan MacMillan, 1990 p321 

==Award nominations== AFI Award nominations for Best Actress in a Supporting Role (Pat Evison) and Best Original Screenplay (Greg Millin). 

==Cast==
*Chris Haywood as Dr. Eric Linden
*Simon Burke as Paul
*Gerda Nicolson as Linda
*Rona McLeod as Dr. Carol Young
*Suzanne Roylance as Patty
*Veronica Lang as Nancy
*Pat Evison as Alda
*Max Bruch as Hassad
*Gabrielle Hartley as Gillian
*Jane Clifton as Sharon

==Box Office==
 The Clinic grossed $414,000 at the box office in Australia,  which is equivalent to $1,092,960
in 2009 dollars.

==See also==
*Cinema of Australia

==References==
 

==Further reading==
* 

==External links==
* 
*  at Oz Movies
* 
* 

 
 
 
 
 
 


 
 