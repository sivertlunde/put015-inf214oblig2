36 Hours (1953 film)
 
 
 
{{Infobox film
| name           = 36 Hours
| image          = Terror Street poster.jpg
| caption        = Theatrical release poster
| director       = Montgomery Tully
| producer       = Anthony Hinds Steve Fisher
| starring       = Dan Duryea
| music          = Ivor Slaney
| cinematography = Walter J. Harvey
| editing        = James Needs
| studio         = Hammer Film Productions
| distributor    = Lippert Pictures (USA) Exclusive Films (UK)
| released       =  
| runtime        = 83 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
36 Hours, released in the United States as Terror Street, is a 1953 British film noir directed by Montgomery Tully. It was made by Hammer Film Productions. 

==Plot==
Bill Rogers (Dan Duryea), an American jet pilot, comes to England to find out why he hasnt heard from his wife lately. Upon his arrival, he learns that his wife has been murdered, and that hes the prime suspect. With only 36 hours at his disposal, Rogers takes it upon himself to track down the actual killer.

==Cast==
* Dan Duryea as Major Bill Rogers
* Elsie Albiin as Katherine Katie Rogers
* Gudrun Ure as Sister Jenny Miller
* Eric Pohlmann as Slossen, the smuggler John Chandos as Orville Hart
* Kenneth Griffith as Henry Slosson Harold Lang as Harry Cross, desk clerk Jane Carras Soup Kitchen Supervisor Michael Golden as The Inspector
* Marianne Stone as Pam Palmer

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 


 