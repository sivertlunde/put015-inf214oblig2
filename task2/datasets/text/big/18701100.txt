Her Jungle Love
 
{{Infobox film
| name           = Her Jungle Love
| image          = Her-Jungle-Love-1938.jpg
| caption        = Film poster
| director       = George Archainbaud
| producer       = George M. Arthur
| writer         = Gerald Geraghty Curt Siodmak
| starring       = Dorothy Lamour
| music          =
| cinematography = Ray Rennahan
| editing        = Hugh Bennett
| distributor    = Paramount Pictures
| released       =  
| runtime        = 81 minutes
| country        = United States
| language       = English
| budget         =
}}

Her Jungle Love is a 1938 American adventure film directed by George Archainbaud and starring Dorothy Lamour.    Portions of the film were shot at Palm Springs, California. {{Cite book
 | last = Niemann
 | first = Greg
 | title = Palm Springs Legends: creation of a desert oasis
 | publisher = Sunbelt Publications
 | year = 2006
 | location = San Diego, CA
 | pages = 286
 | url =  http://books.google.com/books?id=RwXQGTuL1M0C&pg=PA169&lpg=PA169&dq=palm+springs+film+locations&source=bl&ots=N8AgHRYZbg&sig=RMIwy_g3fEV3KoK1aa7P4VxuJWM&hl=en#v=onepage&q=palm%20springs%20film%20locations&f=false
 | doi =
 | id =
 | isbn = 978-0-932653-74-1
 | mr =
 | jfm =
|oclc=61211290}}  ( )  

==Cast==
* Dorothy Lamour as Tura
* Ray Milland as Bob Mitchell
* Lynne Overman as Jimmy Wallace
* J. Carrol Naish as Kuasa
* Virginia Vale as Eleanor Martin (as Dorothy Howe)
* Jonathan Hale as J.C. Martin
* Archie Twitchell as Roy Atkins
* Edward Earle as Capt. Avery
* Sonny Chorre as Guard
* Tony Urchel as Guard
* Richard Denning as Pilot
* Phil Warren as Pilot (as Philip Warren)

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 

 