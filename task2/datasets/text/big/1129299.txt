Dead of Night
 
 
 
{{Infobox film
| name           = Dead of Night
| image          = DeadOfNight1.jpg
| caption        = Dead of Night US release poster Cavalcanti ("Christmas Party" and "The Ventriloquists Dummy") Charles Crichton ("Golfing Story") Basil Dearden ("Hearse Driver" and "Linking Narrative") Robert Hamer ("The Haunted Mirror")
| producer       = Michael Balcon
| writer         = H.G. Wells (original story) E.F. Benson (original story) John Baines (original story and screenplay) Angus MacPhail (original story and screenplay)
| starring       = Michael Redgrave Mervyn Johns Frederick Valk Roland Culver
| music          = Georges Auric London Philharmonic Orchestra
| cinematography = Jack Parker Stanley Pavey Douglas Slocombe
| editing        = Charles Hasse Universal Pictures (US)
| released       = 4 September 1945 (UK) 28 June 1946 (USA)
| runtime        = 102 min.
| country        = United Kingdom
| language       = English
}} portmanteau horror film (a gothic or horror anthology) made by Ealing Studios; the individual stories were directed by Alberto Cavalcanti, Charles Crichton, Basil Dearden and Robert Hamer. The film stars Mervyn Johns, Googie Withers and Michael Redgrave. The film is probably best-remembered for the ventriloquists dummy episode with Redgrave.

Dead of Night stands out from British film of the 1940s, when few horror films were being produced in the country (horror films had been banned from production in Britain during the war), and it had an influence on subsequent British films in the genre. Both of the segments by John Baines were recycled for later films, and the possessed ventriloquist dummy episode was adapted as the audition episode of the long-running CBS radio series Escape (radio program)|Escape.

==Plot== dummy is truly alive. The framing story is then capped by a twist ending.

==Cast==
===Linking narrative===
(directed by Basil Dearden)
*Anthony Baird  (credited as Antony Baird)  as Hugh Grainger
*Roland Culver as Eliot Foley
*Renee Gadd|Renée Gadd as Mrs. Craig
*Sally Ann Howes as Sally OHara
*Mervyn Johns as Walter Craig
*Barbara Leake as Mrs OHara
*Mary Merrall as Mrs Foley
*Frederick Valk as Dr. van Straaten
*Googie Withers as Joan Cortland

===Hearse Driver sequence===
(directed by Basil Dearden; based (uncredited) on "The Bus-Conductor", a short story by E. F. Benson published in The Pall Mall Magazine in 1906)
*Anthony Baird as Hugh Grainger
*Judy Kelly as Joyce Grainger
*Miles Malleson as Hearse Driver / Bus Conductor
*Robert Wyndham as Dr. Albury

===Christmas Party sequence===
(directed by Alberto Cavalcanti; story by Angus MacPhail)
*Michael Allan as Jimmy Watson
*Sally Ann Howes as Sally OHara
*Barbara Leake as Mrs OHara

===Haunted Mirror sequence===
(directed by Robert Hamer; story by John Baines)
*Ralph Michael as Peter Cortland
*Esme Percy|Esmé Percy as Mr. Rutherford&nbsp;— the Antiques Dealer
*Googie Withers as Joan Cortland

===Golfing Story sequence===
(directed by Charles Crichton; story based (uncredited) on a story by H.G. Wells)
*Peggy Bryan as Mary Lee
*Basil Radford as George Parratt
*Naunton Wayne as Larry Potter
* Peter Jones as Fred the barman (uncredited)

===Ventriloquists Dummy sequence===
(directed by Alberto Cavalcanti; story by John Baines)
*Allan Jeayes as Maurice Olcott
*Magda Kun as Mitzi
*Miles Malleson as Jailor
*Garry Marsh as Harry Parker
*Hartley Power as Sylvester Kee
*Michael Redgrave as Maxwell Frere
*Frederick Valk as Dr. van Straaten
*Elisabeth Welch as Beulah

==Legacy==
The circular plot of Dead of Night inspired Fred Hoyles Steady State model of the universe, developed in 1948. Jane Gregory, Fred Hoyles Universe, Oxford University Press, 2005. ISBN 0-19-850791-7, pp.36–7
 

Dead of Night also currently holds a 96% "fresh" rating on Rotten Tomatoes.
 Time Out Christopher Smith was inspired by the circular narrative in Dead of Night when making his 2009 film Triangle (2009 British film)|Triangle. 

==Related==
The theme of a recurring nightmare has been visited in other works and media: Shadow Play", a 1961 episode of The Twilight Zone (1959 TV series)|The Twilight Zone.
*"The Secret Miracle", a short story by Jorge Luis Borges also contains a recurring nightmare inside a framing story

The theme of the mad ventriloquist has been visited in other works and media:
*The Great Gabbo, a 1929 film starring Erich von Stroheim Knock on Wood (1954), a Danny Kaye musical-comedy
*"The Glass Eye," a 1957 episode of the Alfred Hitchcock Presents television series, starring Jessica Tandy
*"The Dummy", a 1962 episode of The Twilight Zone (1959 TV series)|The Twilight Zone television series, starring Cliff Robertson
*"Caesar and Me", a 1964 episode of The Twilight Zone (1959 TV series)|The Twilight Zone television series, starring Jackie Cooper Devil Doll, a 1964 film starring Bryant Haliday
*Magic (1978 film)|Magic, a 1978 film starring Anthony Hopkins It Couldnt Happen Here, a 1988 film by the Pet Shop Boys The Ventriloquist, a Batman nemesis appearing in 1988 The Puppet Buffy the Vampire Slayer
* The Beaver (film)|The Beaver, a 2011 film starring Mel Gibson.

The theme of the fatal crash premonition has also been visited in other works and media:
* "The Bus-Conductor", a short story by E. F. Benson published in The Pall Mall Magazine in 1906 which was the basis for the segment in Dead of Night
*Famous Ghost Stories, a 1944 anthology by Bennett Cerf which retells the Benson short story but changes the main character to a woman and transfers the action to New York City Twenty Two", The Twilight Zone inspired by the Cerf story

The theme of a mirror casting a murderous spell has been visited in other works and media: The Mirror", The Twilight Zone.

==See also==
*List of ghost films

==References==
===Notes===
 

===Bibliography===
*Jerry Vermilye The Great British Films, 1978, Citadel Press, pp 85–87, ISBN 0-8065-0661-X

==External links==
*  
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 