None but the Brave
 
{{Infobox Film
| name           = None but the Brave
| image          = Nonebutthebravepost.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = Frank Sinatra
| producer       = William H. Daniels associate producer  Howard W. Koch executive producer Frank Sinatra  Kikumaru Okuda
| writer         = Kikumaru Okuda (story) John Twist Katsuya Susaki
| narrator       =  Tommy Sands
| music          = John Williams (as Johnny Williams)
| cinematography = Harold Lipstein (director of photography)
| editing        = Sam OSteen
| distributor    = Warner Bros. Pictures 1965
| runtime        = 106 minutes
| country        = United States Japan English Japanese Japanese
| budget         = 
| gross = $2,500,000 (US/ Canada rentals) }}
 1965 war Tommy Sands and Brad Dexter. This is the only film directed by Frank Sinatra, and the first Japanese-American co-production,  produced by Sinatra for Warner Bros. and Kikumaru Okuda for Toho Studios.

==Plot== Pacific with no means of communicating with the outside world. Lieutenant Kuroki (Tatsuya Mihashi) keeps his men firmly in hand and is supervising the building of a boat for their escape.
 Tommy Sands). Confidante to Bourke is the chief pharmacist mate (Frank Sinatra). As both sides learn of each others existence on the island, tension mounts resulting in a battle for the Japanese boat. The vessel is destroyed and a Japanese soldier is seriously injured. Calling a truce, Koruki trades the Americans access to water in exchange for a visit from their doctor to treat the wounded soldier, whose leg has to be amputated.

The truce results in both platoons living side by side, although a line is drawn forbidding one from encroaching on the others side of the island. At first, there is some clandestine cooperation and trading and earnest respect and friendship. When the Americans establish radio contact and their pick-up by a US naval vessel is arranged they demand that the Japanese surrender. As the Americans proceed to the beach, the American captain orders his men to shoot to kill. They are ambushed by the coerced Japanese platoon, who presumably were also unwillingly acting on orders to shoot to kill. The Americans were given no option, but to retaliate in self-defense that results in an ensuing bloody and pointless firefight during which all the Japanese (including Kuroki) and most of the Americans are shot dead. The medic, Bourke, Bleeker, Blair and Corporal Ruffino (Richard Bakalyan) are the only survivors of the skirmish. They move onto the beach and wait to be rescued by the American naval vessel, stationed just offshore. Kurokis final narration calls what he is to do "just another day." The film ends with a long shot of the island, superimposed with the words "Nobody ever wins".

==Cast==
Japanese: 
*Tatsuya Mihashi as Lt. Kuroki Takeshi Katô as Sgt. Tamura
*Homare Suguro as L/Cpl. Hirano
*Kenji Sahara as Cpl. Fujimoto
*Mashahiko Tanimura as Lead Pvt. Ando
*Toru Ibuki as Pvt. Arikawa
*Ryucho Shunputei as Pvt. Okunda (the fishermen)
*Hisao Dazai as Pvt. Tokumaru
*Susumu Kurobe as Pvt. Goro (as Susume Kurobe)
*Takashi Inagaki as Pvt. Ishi
*Kenichi Hata as Pvt. Sato

American:
*Frank Sinatra as Chief Pharmacist Mate
*Clint Walker as Capt. Dennis Bourke Tommy Sands as 2nd Lt. Blair
*Brad Dexter as Sgt. Bleeker
*Tony Bill as Air Crewman Keller
*Sammy Jackson as Cpl. Craddock
*Richard Bakalyan as Cpl. Ruffino
*Jimmy Griffin as Pvt. Dexter
*Christopher Dark as Pvt. Searcy
*Don Dorrell as Pvt. Hoxie
*Phillip Crosby as Pvt. Magee (as Phil Crosby)
*Howie Young as Pvt. Waller
*Roger Ewing as Pvt. Swensholm
*Richard Sinatra as Pvt. Roth
*Rafer Johnson as Pvt. Johnson

==Production notes==
 
The title is from the John Dryden poem, Alexanders Feast (Dryden)|Alexanders Feast, stanza 1: "None but the brave/deserve the fair."
 riptide along with Ruth Koch, wife of producer Howard Koch. Dexter and two surfers were able to rescue Sinatra and Ruth Koch, saving their lives.   The executive producers were famed cinematographer and former president of the American Society of Cinematographers, William H. Daniels and Howard W. Koch, former president of the Academy of Motion Picture Arts and Sciences.

Some posters for the film featured the American cast on the left side and the Japanese cast on the right, with an island in the middle.

The Director of Special Effects was Eiji Tsuburaya, who helmed visual effects on numerous war pictures, the original Godzilla for Toho Studios in 1954, as well as the subsequent sci-fi and fantasy films that followed in Godzillas wake.
 For All We Know".

==Critical response==
Upon release, The New York Times’ Bosley Crowther ignored the films anti-war overtones and gave the production a largely negative review, writing, "A minimum show of creative invention and a maximum use of cinema clichés are evident in the staging of this war film," and "Mr. Sinatra, as producer and director, as well as actor of the secondary role of the booze-guzzling medical corpsman, displays distinction only in the latter job. Being his own director, he has no trouble stealing scenes, especially the one in which he burbles boozy wisecracks while preparing to saw off the shivering Japaneses leg. Mr. Sinatra is crashingly casual when it comes to keeping the Japanese in their place." Crowther also noted "Clint Walker … Tommy Sands … Brad Dexter … and Tony Bill … make over-acting—phony acting—the trademark of the film. What with incredible color and the incredible screenplay of Katsuya Susaki and John Twist, this adds up to quite a fake concoction." 
 The Herald) Mister Roberts Flags of Our Fathers and Letters from Iwo Jima), but that "in a way, Sinatra had already done it, and in one movie." 

==References==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 