The Tell-Tale Heart (1934 film)
 
 
{{Infobox film
| name           = The Tell-Tale Heart
| image          = 
| caption        = 
| director       = Brian Desmond Hurst
| producer       = Harry Clifton
| writer         = David Plunkett Greene (screenplay), Edgar Allan Poe (short story)
| starring       = Norman Dryden John Kelt
| music          = John Reynders
| cinematography = Walter Blakeley
| editing        = Vernon Clancey
| studio         = Blattner Studios
| distributor    = Fox Film Company
| released       =  
| runtime        = 55 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
 short story of the same title by Edgar Allan Poe. It is the earliest known "talkie" film adaptation of the story.

== Plot ==
A young manservant is driven mad by his obsession with the repulsive diseased eye of an old man who cares for him. He kills his master and hides the remains under the floorboards. When the police investigate the old mans disappearance, the imagined beating of the victims heart haunts the murderers thoughts so much that his words and actions arouse the suspicions of the police.

== Cast ==
*Norman Dryden as The Boy
*John Kelt as The Old Man
*Yolande Terrell as The Girl

== Production == Harry Clifton as the listed producer ), it was filmed at the Blattner Studios in Elstree Studios|Elstree, and released in the USA under the title "Bucket of Blood". The film was considered so gruesome that it was withdrawn from some cinemas in the UK. 

== See also ==
List of horror films of the 1930s

== References ==
 

== External links ==
* 
*   at the website dedicated to Brian Desmond Hurst

 
 

 
 
 
 
 
 
 
 


 