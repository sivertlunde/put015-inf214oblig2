Quest for Camelot
 
{{Infobox film
| name = Quest for Camelot
| image = Quest for Camelot- Poster.jpg
| image_size = 220px
| border = yes
| alt = 
| caption = Theatrical release poster
| director = Frederik Du Chau
| producer = Andre Clavel Dalisa Cohen Zahra Dowlatabadi Kirk De Micco William Schifrin Jacqueline Feather David Seidler
| based on =   Jane Seymour Pierce Brosnan Gary Oldman Eric Idle Don Rickles Bronson Pinchot Jaleel White Gabriel Byrne John Gielgud
| music = Patrick Doyle
| editing = Stanford C. Allen
| studio = Warner Bros. Feature Animation Warner Bros. Family Entertainment
| distributor = Warner Bros.
| released =  
| runtime = 86 minutes  
| country = United States
| language = English
| budget = $40 million
| gross = $22.5 million (domestic)
}} animated Musical musical fantasy Jane Seymour, Steve Perry flopped at the box office.

==Plot==
Kayley dreams of becoming a knight like her father Lionel, who journeys to Camelot to meet with King Arthur as a knight of the Round Table. While there, one of the greedier knights, Baron Ruber attempts to usurp Arthur and in the fray, Lionel is killed while Ruber escapes. Ten years later, a griffin attacks the kingdom, stealing Excalibur. Merlins falcon, Ayden forces the griffin to release Excalibur and it falls into the dark forest. Word is soon sent of the missing sword, falling on the ears of Kayley; still as spirited as always, and still dreaming of becoming a knight, she desires to find Excalibur, but her mother Julianne forbids it. Ruber pays them a visit and forces Julianne to agree to give him and his men safe passage into Camelot as part of a plan to take control. Kayley escapes and enters the forest, losing Rubers men and encountering Garrett; a blind hermit who is assisted by Ayden. He decides to go after the sword, but Kayley insists on following him to his chagrin, as he works better on his own.

They enter Dragon Country and encounter a two-headed dragon; the gruff Cornwal, and the intellectual Devon, who are bullied by their fellow dragons as they cannot breathe fire or fly and they dream of a time where they could be separate, each believing the other is holding them back and they do not get along. They narrowly escape being attacked by other dragons and Ruber and his men; who have been melded with weapons thanks to a potion Ruber had bought. Thinking they lost them, Kayley will not remain quiet at a crucial moment and Garrett is injured by an arrow after discovering a giant has made off with Excalibur. Kayley manages to lose Ruber and his men and they flee to a cave where the magic of the forest heals Garretts wounds. They make their way to the giants cave and try to hypnotize the beast to sleep in order to steal the sword back. Ruber and his men arrive again and try to steal the sword back, but they trick the giant into falling on them and they get away with the sword.

Returning to the main road, Garrett insists that he will never be able to fit in with the normal world and returns to the forest. Kayley is captured by Rubers men and Devon and Cornwal rush to tell Garrett. Kayley is held as insurance for safe passage into Camelot, but Kayley manages to warn them in time to rouse a defense. Meanwhile, Bladebeak; a chicken transfigured with the beak of an axe, turns on Ruber and begins to aid Julianne and Kayley while Ruber tracks Arthur in his throne room. Devon and Cornwal, finding that by working together they can fly and breathe fire, fend off the griffin and save Ayden from being killed. Kayley goes after Ruber and encounters him about to kill the injured Arthur with Excalibur; now bonded to his hand with the magic potion. Defiantly, and using the same words as her father, Kayley faces off against Ruber, and they find their way out to the courtyard where the Stone and Anvil that once held Excalibur are held. Garrett comes to Kayleys aid, and together they avoid Rubers attack, causing the sword to be inserted back into its stone. The conflict of magic causes Ruber to evaporate into nothingness, leaving the sword whole in the stone, as well as returning Bladebeak and Rubers men to normal. King Arthur removes the sword, thanking Kayley and Garrett for their help.

Sometime later, Kayley and Garrett dance at an elegant ball at the castle. When they leave, they do so on horses with the signs "Just Knighted" tied to them.

==Cast==
* Jessalyn Gilsig as Kayley
** Andrea Corr as Kayley (singing voice) Sarah Freeman as Young Kayley
* Cary Elwes as Garrett
** Bryan White as Garrett (singing voice)
* Gary Oldman as Ruber 
* Eric Idle as Devon
* Don Rickles as Cornwall Jane Seymour as Juliana 
** Céline Dion as Juliana (singing voice)
* Pierce Brosnan as King Arthur  Steve Perry as King Arthur (singing voice)
* Bronson Pinchot as Griffin 
* Jaleel White as Bladebeak
* Gabriel Byrne as Lionel 
* John Gielgud as Merlin 
* Frank Welker as Ayden

==Musical numbers==
# "United We Stand" - King Arthur and Knights
# "On My Fathers Wings" - Kayley
# "Ruber" - Ruber
# "The Prayer" - Julianna
# "I Stand Alone" - Garrett
# "If I Didnt Have You" - Devon and Cornwall
# "Looking Through Your Eyes" - Garrett and Kayley
# "I Stand Alone (Reprise)" - Garrett

==Production==
In May 1995, The Quest for the Grail was  ) was originally going to direct with his wife, Sue, producing, but creative differences forced the husband and wife team to leave the project in February 1997. Beck (2005), pp. 217.  Kenny Ortega served as the films Choreography|choreographer. CGI was used for a few scenes, such as to create the rock ogre.  According to Kit Percy, head of CGI effects, the software they used was designed for use with live-action.   

Chrystal Klabunde, leading animator of Garrett, said in an article in Animation Magazine, "It was top heavy. All the executives were happily running around and playing executive, getting corner offices—but very few of them had any concept about animation at all, about doing an animated film. It never occurred to anybody at the top that they had to start from the bottom and build that up. The problems were really coming at the inexperience of everyone involved. Those were people from Disney that had the idea that you just said, Do it, and it gets done. It never occurred to them that it got done because Disney had an infrastructure in place, working like clockwork. We didnt have that."  Effects supervisor, Michel Gagné also said, "People were giving up. The head of layout was kicked out, the head of background, the executive producer, the producer, the director, the associate producer---all the heads rolled. Its kind of a hard environment to work in." Dalisa Cooper Cohen, producer of the film, said "We made this movie in a year, basically. That was a lot of the problem. We worked around the clock." Beck (2005), pp. 218. 

Reportedly, "cost overruns and production nightmares" led the studio to "reconsider their commitment to feature animation."  Filmmaker Brad Bird (who helmed The Iron Giant, Warner Bros. next animated film) thought that micromanaging, which he said had worked well for Disney but not for Warner Bros., had been part of the problem.   

===Animators=== Athanassios Vakalis - Kayley
* Chrystal Klabunde - Garrett
* Cynthia Overman - Juliana Alexander Williams - Ruber
* Dan Wagner - Devon and Cornwall
* Stephen Franck - The Griffin and Bladebeak
* Mike Nguyen - Ayden

==Promotion==
The film was heavily promoted by Wendys, who offered themed Kids meals that included toys and discounts on theater admission. Warner Bros. also teamed up with UNICEF to promote the home video release of the film by advertising trick-or-treat donation boxes before Halloween arrived.
 Buffy the Vampire Slayer.
 The Little Mermaid. Kids WB did promo spots for the film in May 1998.

==Reception== Deep Impact The Horse Whisperer, the latter of which also starred Jessalyn Gilsig,  and the following week by the hyped release of Godzilla (1998 film)|Godzilla. 

David Kronke of the Los Angeles Times described the film as "formulaic", and wrote that it was "a nearly perfect reflection of troubling trends in animated features," called Kayley "a standard-issue spunky female heroine," and said that "Garretts blindness is the one adventurous element to the film, but even it seems calculated; his lack of sight is hardly debilitating, yet still provides kids a lesson in acceptance".  Kevin J. Harty, editor of a collection of essays called Cinema Arthuriana, says that the film is “slightly indebted to, rather than, as Warner publicity claims, actually based on” Chapman’s novel.  The New York Times wrote "Coming on the heels of 20th Century Foxs lush but silly Anastasia (1997 film)|Anastasia (a much better film than this one), Quest for Camelot suggests that Disney still owns the artistic franchise on animated features." 

==Soundtrack==
{{Infobox album   Name = Quest for Camelot: Music from the Motion Picture Type = Soundtrack Artist = Various Artists Cover =  Released = May 5, 1998 Recorded =  Genre = Various Length =   Label = Atlantic
|Producer = Daniel A. Carlin Last album = This album = Next album =
}}
{{Album ratings rev1 = Allmusic rev1score =   
}} The Prayer", and was also nominated for the Academy Award for Best Original Song, also for "The Prayer" (though it lost the latter to "When You Believe" from DreamWorks The Prince of Egypt).
 The Prayer", Steve Perry 71st Academy The Celtic Tenors, covered it for her solo album. Another rendition of "The Prayer" was performed at the Closing Ceremonies of the 2002 Winter Olympics by Josh Groban and Charlotte Church.

{{Tracklist extra_column = Artist extra1 = LeAnn Rimes title1 = Looking Through Your Eyes length1 = 4:06 extra2 = Steve Perry title2 = I Stand Alone length2 = 3:43 extra3 = Celine Dion title3 = The Prayer length3 = 2:49 extra4 = Steve Perry title4 = United We Stand length4 = 3:20 extra5 = Andrea Corr title5 = On My Fathers Wings length5 = 3:00 extra6 = The Corrs and Bryan White title6 = Looking Through Your Eyes length6 = 3:36 extra7 = Gary Oldman title7 = Ruber length7 = 3:56 extra8 = Bryan White title8 = I Stand Alone length8 = 3:26 extra9 = Eric Idle and Don Rickles title9 = If I Didnt Have You length9 = 2:55 extra10 = Patrick Doyle title10 = Dragon Attack/Forbidden Forest length10 = 3:14 extra11 = Patrick Doyle title11 = The Battle length11 = 2:49 extra12 = David Foster title12 = Looking Through Your Eyes length12 = 3:57 extra13 = Celine Dion and Andrea Bocelli title13 = The Prayer length13 = 4:09
}}

==Video game==
 
The video game was released in 1998 for Game Boy Color.

==See also==
* List of films based on Arthurian legend

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 