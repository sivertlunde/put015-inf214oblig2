Awakening of the Beast
{{Infobox film
| name           = Awakening of the Beast
| image          = Despertar.jpg
| image size     = 175px
| caption        = Brazilian theatrical poster
| director       = José Mojica Marins
| producer       = Giorgio Attili José Mojica Marins George Michel Serkeis
| writer         = Rubens F. Lucchetti
| starring       = José Mojica Marins Mário Lima
| music          =
| cinematography = Giorgio Attili
| editing        = Luiz Elias
| studio         = Fotocena Filmes M.M. Multifilmes OVNI
| distributor    =
| released       = October 1970 (Brazil)
| runtime        = 93 minutes
| country        = Brazil
| language       = Portuguese
| budget         =
}}

Awakening of the Beast ( ) is a 1970 Brazilian Horror films|horror/exploitation film directed by José Mojica Marins. Marins is also known by his alter ego Coffin Joe (Zé do Caixão). Marins appears as himself and as the Coffin Joe character in the fictional film which is in the form of a pseudodocumentary.      

==Synopsis==
In the films first portion, filmed in Black-and-white|B&W, Dr. Sergio, a psychiatrist, appears on a television program on a panel with three other contemporary psychiatrists after he claims to have conducted experiments on four volunteer drug addicts with LSD in order to investigate his claim that sexual perversion is caused by use of illegal drugs. As evidence, he presents a series of documented accounts of drug use which lead to lewd and bizarre sexual acts. Marins appears (as himself) on the panel with the psychiatrists as some type of expert on the subject of depravity. During the program, Dr. Sergio recounts the experiment to his colleagues on the panel, who reject his claims.
 color and each patients experience is vividly portrayed in a series of surreal scenes.

==Cast==
*Andréa Bryan
*Annik Malvil
*Graveto
*Ítala Nandi
*José Mojica Marins as Coffin Joe
*Lurdes Vanucchi Ribas
*Mário Lima as Volunteer
*Ozualdo Candeias as Volunteer
*Roney Wanderney

===The television panel===
*Carlos Reichenbach
*Jairo Ferreira
*João Callegaro
*José Mojica Marins as himself
*Maurice Capovilla
*Sérgio Hingst as Dr Sérgio
*Walter C. Portella

==References==
 

==External links==
*   
* 
*  on    
*   

 

 
 
 
 
 
 
 