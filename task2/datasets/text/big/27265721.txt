Monte Carlo (2011 film)
{{Infobox film
| name           = Monte Carlo
| image          = Monte Carlo Poster.jpg
| caption        = Theatrical release poster
| alt            = Three girls, one girl wears a white dress, with cowboy boots underneath. 
| director       = Thomas Bezucha
| producer       = Denise Di Novi Alison Greenspan Nicole Kidman Arnon Milchan
| screenplay     = Thomas Bezucha April Blair Maria Maggenti
| story          = Kelly Bowe
| based on       =  
| starring       = {{Plain list | 
* Selena Gomez 
* Leighton Meester 
* Katie Cassidy 
}}
| music          = Michael Giacchino
| cinematography = Jonathan Brown Jeffrey Ford Di Novi Pictures Dune Entertainment
| distributor    = 20th Century Fox
| released       =  
| runtime        = 109 minutes
| country        = United States Hungary
| language       = English
| budget         = $20 million 
| gross          =  $39,667,665 
}}

Monte Carlo is a 2011 American romantic comedy film directed by Thomas Bezucha. Denise Di Novi, Alison Greenspan, Nicole Kidman, and Arnon Milchan produced the film for Fox 2000 Pictures and Regency Enterprises. It began production in Harghita|Harghita, Romania on May 5, 2010.
 Monte Carlo, Who Says" by Selena Gomez & the Scene and numerous songs by British singer Mika (singer)|Mika.

==Plot==
Grace Bennett is a Texas high-school student who works as a waitress with her best friend Emma Perkins to earn money for a trip to Paris after graduation. Graces stepfather pays for her stepsister Meg Kelly to come with them on the trip. Emma goes to Paris despite her boyfriend Owens proposal of marriage. After being left behind by their tour guide, the three girls seek refuge from the Paris rain in a posh hotel. There, the hotel staff and paparazzi mistake Grace for celebutante British heiress Cordelia Winthrop-Scott, Graces double, who leaves rather than stay to attend an auction for a Romanian charity for which she is to donate an expensive Bulgari necklace. The Americans spend the night in Cordelias suite, and the next day fly to Monte Carlo with Cordelias luggage.

At Monte Carlo the girls meet Theo Marchand, the son of the philanthropist hosting Cordelia. Theo dislikes Cordelias spoiled nature but escorts them to a ball, where Grace successfully fools Cordelias aunt Alicia and Emma dances with a prince. Meg reunites with Riley, an Australian backpacker she briefly met in Paris. They find they have things in common, and spend time together before he leaves for Italy. When Grace has to take part in a game of polo, Alicia discovers the impersonation because of Graces different riding style. Alicia believes her niece has hired a lookalike to take her place while she parties, but in order not to endanger the charity auction she agrees to keep silent. Theo is attracted to "Cordelias" frank personality, while Emmas prince invites her to a party on a yacht. Emma dresses for the party in Cordelias necklace but meets Meg on the way, and Meg takes it for safekeeping, but later forgets it in Rileys backpack. Emma is disillusioned at the party by the princes arrogance toward the servants.

Owen arrives in Monte Carlo in search of Emma. So does Cordelia, and she sees the newspaper account of Graces appearance at the ball. She finds that the necklace is missing and calls the police. The girls have gone in search of Riley but he shows up at the hotel with the necklace; they find Cordelia in the room. Cordelia threatens to withdraw the necklace from the auction, and the girls tie her up so Grace can take her place at the auction. Cordelia escapes, and reveals Graces fraud at the auction. She demands that Grace be arrested, but after Graces sincere public confession Alicia bids the unexpectedly large amount of €6 million for the necklace to save her. The film ends with Meg joining Riley on his travel around the world; Owen and Emma moving into their own home in Texas; and Theo Marchand and Grace reuniting at the Romanian charity.

==Cast==
 
* Selena Gomez as Grace Ann Bennett/Cordelia Winthrop Scott
* Leighton Meester as Mary Margaret "Meg" Kelly
* Katie Cassidy as Emma Danielle Perkins
* Cory Monteith as Owen Andrews
* Pierre Boulanger as Theo Marchand
* Catherine Tate as Alicia Winthrop Scott
* Luke Bracey as Riley
* Andie MacDowell as Pamela Bennett
* Brett Cullen as Robert Kelly
* Giulio Berruti as Prince Domenico Da Silvano

==Production==
 
Monte Carlo is loosely based on the novel Headhunters by Jules Bass. The novel tells the story of four young Texas women who pretend to be wealthy heiresses while searching for rich potential husbands in Monte Carlo. There, they meet four gigolos posing as wealthy playboys. Fox bought the film rights to the novel in 1999, three years prior to the novels publication.  In 2005, Hollywood trade magazine Variety (magazine)|Variety announced that siblings Jez and John Henry Butterworth would be writing the script. It also reported that actress Nicole Kidman had signed on to play the lead as well as produce the film with Rick Schwartz. 

The Butterworths were later fired and Tom Bezucha was hired to direct and co-write Monte Carlo. Bezucha and Maria Maggenti turned in a draft of the screenplay by July 2007; it starred Kidman as "one of three Midwestern schoolteachers who decide to ditch a disappointing no-frills holiday in Paris and pose as wealthy women vacationing in Monaco".  However, in 2010, executives had the film rewritten again after deciding that the film should be made more youthful. The updated script was co-written by Bezucha and April Blair, and changed the three school teachers to two college students and a recent high-school graduate.  Monte Carlo was shot in Budapest, Hungary; Dunakeszi, Hungary; Paris|Paris, France; Harghita County|Harghita, Romania; and Monte Carlo, Monaco.  It began filming in Harghita on May 5, 2010, and wrapped on July 7, 2010. {{cite web| url= http://selenagomez.com/news/note-selena| title=Note from Selena!
| publisher=SelenaGomez.com| date=05-06-2010| accessdate=05-07-2010 }}   It is the first film to use the film studio, Raleigh Studios Budapest. 
 English accent. French actor Pierre Boulanger made his English-speaking feature debut in the film.  Leighton Meester and Katie Cassidy had previously worked together on Meesters hit show Gossip Girl.

==Reception==

===Critical response===
Monte Carlo received mixed reviews from critics. Rotten Tomatoes gives the film a score of 38% based on 91 reviews. The websites consensus states "Although it has its charming moments, Monte Carlo is mostly silly, predictable stuff that never pushes beyond the boundaries of formula." 
At Metacritic the film received a score of 43/100 based on reviews from 23 critics indicating "mixed or average reviews. 

Ben Sachs of the Chicago Reader claimed that "the movie hits a surprising range of emotional grace notes, including several moments of genuine regret, and concludes with an understated moral lesson about the value of self-respect over social status."  Sandie Chen of Common Sense Media said the film was "silly, but sweet".  It was released in France on August 24, 2011.

===Accolades===
{| class="wikitable"
! Award !! Category !! Recipients !! Result
|- Teen Choice Awards 
| Choice Summer Movie
|
| rowspan=4  
|-
| Choice Summer Movie Star – Male
| Cory Monteith
|-
| Choice Summer Movie Star – Female Selena Gomez
|- ALMA Awards
| Favorite Movie Actress – Comedy/Musical
|-
| Hollywood Teen TV Awards
| Favorite Film Actress
|  
|}

==Home media==
Fox Home Entertainment released Monte Carlo on   of the film.   

==Soundtrack==
The films musical score was composed by Michael Giacchino. To coincide with the films release, a soundtrack album was released by Varèse Sarabande on June 28, 2011.

 
; Tracklist 
# "Graceful Exit"
# "What Mom Would Have Wanted"
# "Its Not Magic"
# "Feeling Eiffel"
# "Grace Under Pressure"
# "Mirror Coincidence"
# "The Seduction of Paris"
# "Along for the Ride"
# "Seizing the Moment"
# "The Full Monte Carlo"
# "One Suite Deal"
# "Junk in the Trunks"
# "Ball In"
# "Pairing Up"
# "A Little Horse (sil vous) Play"
# "Of Another Color"
# "Dressing Up and Dressing Down"
# "Jazz Cafe"
# "Staying Classy"
# "Hotel Keys"
# "Youre Goin Places, Kid"
# "Chasing Emma"
# "Have a Nice Trip"
# "Megsmerized"
# "Cordelia Arrives"
# "Cordelias Not So Suite"
# "Time to Go"
# "Missing Links"
# "Return Engagement"
# "Protection and (Room) Service"
# "Just Stay Here"
# "I Dont Want to Lose You"
# "Its Too Much"
# "Just a Regular Girl"
# "Almost Everyone Is Happy"
# "Separate Ways"
# "Grace Be with You"
# "Of Another Color" (extended version)
# "Making Light"
# "Graces Theme"

==References==
{{Reflist|refs=
   

   

     

   

   

   
}}

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 