Janma Rahasya
{{Infobox film
| name           = Janma Rahasya
| image          = 
| image size     = 
| caption        = 
| director       = S. P. N. Krishna
| producer       = Shrikant Nahata Shrikant Patel
| story          = Abdul Muthalib
| screenplay     = S. P. N. Krishna
| narrator       = Rajkumar Bharathi Bharathi K. S. Ashwath Dinesh Dwarakish Niranjan
| music          = M. Ranga Rao
| cinematography = 
| editing        = S. P. N. Krishna
| studio         = Satya Studios
| distributor    = Venus Movies
| released       =  
| runtime        = 
| country        = India
| language       = Kannada
}}
 Kannada film Rajkumar and Bharathi in lead roles.

==Cast== Rajkumar as Kumar Bharathi
* K. S. Ashwath
* Dinesh
* Dwarakish
* Niranjan
* Pandari Bai
* Keshava Murthy
* Advani Lakshmi Devi

==Soundtrack==
The music of the film was composed by M. Ranga Rao with lyrics for the soundtrack penned by R. N. Jayagopal and M. Narendra Babu.
{{Infobox album	
| Name = Janma Rahasya
| Longtype = to Janma Rahasya
| Type = Soundtrack	
| Artist = M. Ranga Rao
| Cover = 
| Border = Yes	
| Alt = Yes	
| Caption = 
| Released = 1972
| Recorded = 
| Length =  Kannada 
| Label = 
| Producer = 
| Last album = 
| This album = 
| Next album = 
}}

===Tracklist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|- 
| 1
| "Beeso Gaali Ale"
| P. B. Sreenivas, S. Janaki
|-
| 2
| "Dhummaana Yeke"
| L. R. Eswari
|-
| 3
| "Kaveri Theeradalli"
| S. Janaki
|-
| 4
| "Kannanchali"
| S. Janaki
|}

==External links==
*  
*  

 
 
 
 


 