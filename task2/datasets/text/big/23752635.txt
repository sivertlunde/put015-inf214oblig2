L'enfance de l'art
 
{{Infobox film
| name           = Lenfance de lart
| image          = Lenfance de lart.jpg
| caption        = Film poster
| director       = Francis Girod
| producer       = Ariel Zeitoun
| writer         = Francis Girod Yves Dangerfield
| starring       = Clotilde de Bayser
| music          = 
| cinematography = Dominique Chapuis
| editing        = Geneviève Winding
| distributor    = 
| released       =  
| runtime        = 107 minutes
| country        = France
| language       = French
| budget         = 
}}

Lenfance de lart is a 1988 French drama film directed by Francis Girod. It was entered into the 1988 Cannes Film Festival.   

==Cast==
* Clotilde de Bayser – Marie
* Michel Bompoil – Simon
* Anne-Marie Philipe – Régine
* Yves Lambrecht – Jean-Paul
* Marie-Armelle Deguy – Ludivine
* Régine Cendre – Martine
* Bruno Wolkowitch – Samuel
* Etienne Pommeret – Lucas
* Yves Dangerfield – Philippe (as Vincent Vallier)
* Hélène Alexandridis – Juliette
* André Dussollier – Luc Ferrand
* Laurence Masliah – Valérie
* Pierre Gérard – Louis
* Olivia Brunaux – Lydia
* Judith Magre
* Dominique Besnehard

==References==
 

==External links==
* 
 
 
 
 
 
 
 
 