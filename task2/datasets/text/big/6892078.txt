The Garbage Picking Field Goal Kicking Philadelphia Phenomenon
 
{{Infobox Film
| name           = The Garbage Picking Field Goal Kicking Philadelphia Phenomenon
| image          = The Garbage Picking Field Goal Kicking Philadelphia Phenomenon.jpg
| caption        =  Tim Kelleher
| producer       = Kevin Inch Tim Kelleher Greg Fields
| starring       = Tony Danza Jessica Tuck Art LaFleur Jaime Cardriche Gil Filar
| music          = 
| cinematography = 
| editing        = 
| distributor    = Disney
| released       = 1998
| runtime        = 78 minutes USA
| awards         =  English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
The Garbage Picking Field Goal Kicking Philadelphia Phenomenon (also known as The Philadelphia Phenomenon)  is a 1998 television movie starring Tony Danza from Walt Disney Pictures.

==Plot==
 
Barney Gorman (Tony Danza) works hard as a garbageman, but his career indirectly embarrasses his family. All the Philadelphia Eagles coach wants to do is impress the football fans in Philadelphia, but having a team that lacks success makes that hard for them. But one day, when Barney and his friend are picking up garbage, Barney takes out his frustration and kicks a water jug at the city dump, which also happens to be a scouting location for the Eagles to buy land in order to build a new stadium, and the owner is present. The jug goes so far, that the Eagles coach sees and asks Barney to be their new kicker, which the owner feels is an excellent publicity stunt in "giving an average Joe a shot at the National Football League|NFL".

Barney joins the Eagles, but at first isnt really accepted by his teammates, especially his roommate, Bubba. But once Barney starts playing and makes a lot of game winning field goals, his teammates and football fans all over town begin to love him. They even dub him a nickname, "G-Man".

Unfortunately, the fame and popularity goes to his head, and he becomes spoiled and selfish. He begins to think that his teammates are his lackeys, and that they should all worship the ground he walks on. After all this, Barney misses a game-winning field goal attempt and a chance to make it to the playoffs.

Barney goes to a bar, depressed and lonely, when an attractive blonde woman approaches him. She asks him if he is Barney Gorman and if she could take a picture with him. She takes him to the photographer, but is caught off guard when the woman kisses him. She says thanks while giving him an alluring smile as she walks away.

About two days later, Barney is still depressed over the loss and gets kicked off the team. He then misses a date with his wife (Jessica Tuck), and she sees the picture of him kissing the blonde woman in the newspaper. When Barney tries to go home later, he sees that his suitcase is packed with the newspaper photo of him kissing the blonde woman outside the front door.

After a while, Barney comes to his senses and goes home to apologize to his wife, son, father, and his teammates. The Eagles let him back on the team, just in time for the final game of the season. At the end of the game, the ball placeholder fumbles the ball, and Barney grabs it and scores the game winning touchdown. Barney then remembers his roots as a garbageman, and at a press conference points out that garbagemen are deserving of respect as they work hard to keep the city clean.

==Cast==
{| class="wikitable" width="50%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|- 
| Tony Danza || Barney Gorman
|- 
| Jessica Tuck || Marie Gorman
|- 
| Art LaFleur || Gus Rogenheimer
|- 
| Jaime Cardriche || Bubba
|- 
| Gil Filar || Danny Gorman
|}
 Canadian football player and professional placekicker. The movie also featured sportscaster Chris Berman and NFL footage with Troy Aikman of the Dallas Cowboys.

==Production==
Most of the movie was actually filmed in Toronto, Ontario. Other filming locations for the movie included Hollywood, California and Philadelphia, Pennsylvania.

==External links==
*  
*  

 

 
 
 
 
 