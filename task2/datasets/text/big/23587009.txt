Evil Bong 2: King Bong
{{multiple issues|
 
 
}}

{{Infobox film
| name           = Evil Bong 2: King Bong
| image          = Evil Bong 2.jpg
| caption        =
| director       = Charles Band
| producer       =
| writer         =
| starring       = John Patrick Jordan Amy Paffrath Sonny Davis Brett Chukerman Mitch Eakins
| music          = District 78
| cinematography =
| editing        =
| distributor    = Full Moon Features
| released       =  
| runtime        = 80 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} Charles Bands Evil Bong.

== Plot ==
 
The film begins with Larnell (John Patrick Jordan) pacing around in his new dorm room, looking afraid. Suddenly theres a knock at the door, and its Alister McDowell (Brett Chukerman), who Larnell called over. Alister asks Larnell how things are going and Larnell tells him that hes on the run because he burned down the colleges administrations office because he believes that the reason theyre forced by their parents to go to school is because theyre being taken control underneath the government, trying to control their minds into becoming like them. Hes even thinking about writing a number one best seller about it. Larnell also tells him that hes still rooming with Bachman (Mitch Eakins), who now has a job at Sloppy Burger. Alister asks Larnell why he called over and Larnell tells him that somethings wrong with Bachman. Bachman then comes home from work and starts telling him about how his day went, which led to him getting fired, until he drops onto the floor, fast asleep. Larnell explains that its what he was trying to tell him. Alister explains that its narcolepsy and Bachman needs medical help. Larnell also explains that he and Brett (Brian Lloyd) have problems also. Brett comes to the apartment, along with his girlfriend Luann (Robin Sydney), where its shown that Brett has gain a lot of weight. Larnell then suddenly starts humping Luanns leg, and explains to Alister that his sexually urges make him pounce like a cougar. Alister then realizes that Larnell, Bachman and Brett were the first ones to smoke from the bong and are the only ones who were killed within the bong world, which is probably the reason why Luann and Alister arent experiencing side-effects.
 Jimbo Leary Amazon in previous film. Believing that they could possibly find a cure, Larnell, Rabbit, Alister, Bachman and Brett head out to the amazon. On the way they bump into Velicity (Amy Paffrath), a woman who claims that she and her partner (who was a friend of her deceased father) are there because theyre doing some research. She takes them inside her hut for some food and after witnessing Bachman black out again, the group then explains to her whats going down. However, she believes them but then switches the subject when Rabbit starts asking her if theres marijuana near them. Rabbit and Larnell go outside to take a piss, where they find a whole pile of marijuana and theyre greeted by Velicitys partner, who turns out to be Larnells grandfather Cyril (Jacob Witkin), whos somehow out of his wheelchair. Larnell and Rabbit go back to the hut to have Velicity explain to them whats going on.

She explains that the research her father was uncovering was the marijuana. She explains that the marijuana was believed to belong to an ancient tribe thats gone around for generations...the Poontang Tribe! Cyril knew Velicitys father from college and after her fathers death, she called him to help her with her fathers research and they discovered that marijuana can be used to help peoples pain problems and severe injuries, explaining the reason how Cyril is able to walk again, and Velicity is planning on using the marijuana to give to doctors so she could help those who have cancer. Bachman then tells them that if the marijuana helped Cyril, it could probably help them with their side effects. Velicity wonders if the bong is in any connection with the marijuana from the poontang tribe. Larnell pulls out a bag, revealing a few pieces the blown up bong, Eebee. After putting the pieces together, forming her face, Velicity easily recognizes the bong being the works of the tribe. Suddenly, Eebee (voiced by Michele Mais) comes back to life. Alister theorizes that the proximity of the marijuana has brought her back to life, which Eebee agrees. Larnell puts her back in the bag and Velicity tells them that shell show her to Cyril. As she finds Cyril, she finds him talking on the phone with a guy who he was planning on selling to. Realizing that he was planning on selling it to the black market and was planning on helping her at all, she confronts him until she steals Eebee out of his hands and goes off to cry to the others. Hearing about Cyrils plan on selling the marijuana, Eebee tells him that he can bring her back to her original form with the marijuana, and tells him that she and he will rule the world.

Back at the hut, Rabbit goes to confront Cyril while Larnell, Bachman and Brett take a toke of the marijuana, in hopes that it will cure them. Rabbit arrives to find Eebee reconstructed into her original form. Rabbit, however, makes a deal with Cyril that he wants to help him sell the marijuana so he too can get paid, since he only gets paid $200 a week just to get up at five in the morning to deliver packages. If Cyril doesnt agree, Rabbit will take Eebee back to the poontang tribe. Eebee, suddenly, cries for Rabbit not to take her back because she claims that her man ordered the tribe to take her away, leading to her falling in the hands of Jimbo. Suddenly, three women appear behind them, who Eebee claims to be members of the poontang tribe. Cyril confronts them but gets hit in the face with a spear. The tribe then take Eebee and Rabbit hostage and brings them back to Eebees man, who is revealed to be another bong, called the King Bong (voiced by Michael A. Shepard). King Bong then tempts Rabbit to him and he and the tribe head back into the bong, leaving King Bong to be yelled at Eebee, whos still mad at him for having the tribe kick her out. Back at the hut, Larnell, Bachman and Brett turn back to normal again. Larnell has no sexual urges, Bachman doesnt feel like hes about to pass out again, and Brett is now back to being thinner. Suddenly, Cyril barges in, bleeding from his head and he tells them that the tribe kidnapped Eebee and Rabbit. Realizing that they could be killing Rabbit any moment, Larnell, Velicity, Alister, Bachman and Brett take a couple of spears and head out to save Rabbit.

They find the King Bong and realize that not only do they need to get Rabbit back, they also have to kill the King Bong. They just dont know how, until Eebee tells them that they have to destroy his symbol, which gives him his power. When asked why shes telling them the information, she declares that she wants to see the bong die and be gone for good because shes still pissed at him. Bachman and Brett take a hit first and its revealed that this bong is more powerful than Eebee, so instead of their spirits getting sucked up into the bong, theyre physically sucked up into the bong. Bachman and Brett end up in the King Bongs bong world, where its in the jungle and the duo end up getting lap-danced by two members of the pibe. Back in the real world, Alister takes a hit and ends up finding Rabbit getting pleasured by another two members of the tribe on a bed. Alister is almost pleasured by a member until he threatens to beat her up, which she simply replies by smacking him in the face, knocking him out. Rabbit is then taken by the tribe to see King Bong. Back in the real world, Larnell tells Velicity to stay while he deals with the bosses her goodbye. Larnell ends up finding a knocked out Alister on the bed, wakes him up and continue to find the bong. There, they find the tribe rolling Rabbit up in paper and suddenly, the bong shrinks him, turning him into a doobie and find Bachman and Brett tied up. Alister and Larnell then see a medallion wrapped around the bong.
 real world. As the group gets ready to leave, Larnell decides to stay behind and help Velicity with her idea of sending the marijuana to doctors, providing them the caner. As they walk back to the hut to get business started, Eebee calls back for them to not leave her alone. Back at the dorm, Bachman finally gets his job back at Sloppy Burger, Alister is continuing to get his second major, Brett and Luann are back together, Larnell is still helping Vel with the cure, and Rabbit is now a preacher. He claims that when he got rolled up into a doobie, he prayed to god that hell do good and ask for his sins. Bachman then offers him some marijuana, which Rabbit gladly agrees to. During the end credits, the King Bong can be heard moaning as hes having one of the tribe members pleasure with him.

== Cast ==
* John Patrick Jordan as Larnell
* Amy Paffrath as Velicity
* Sonny Davis as Rabbit
* Brett Chukerman as Alistair McDowell
* Mitch Eakins as Bachman Brian Lloyd as Brett
* Jacob Witkin as Cyril
* Robin Sydney as Luann
* Michele Mais as V.O. Eebee
* Michael A. Shepard as V.O. King Bong
* Ariel X as Poontang Tribe August
* Emilianna
* Kat
* Kyle Stone

== External links ==
*  

 
 

 
 
 
 
 
 
 