Coweb
 
 
{{Infobox film
| name = Coweb
| image = Coweb poster.jpg
| caption = Promotional poster Joe Ma
| director = Xiong Xin Xin
| writer = Chan Wing-Sun Sam Lee   Eddie Cheung
| editing = Li Kar Wing
| cinematography = Chan Chor-Keung
| studio = Beijing Channel Pictures Company   Joy Charm Entertainment   Singing Horse Productions
| distributor = Kind Legend Investment Ltd.   Hong Kong Film Development Fund
| released =  
| runtime = 87 minutes
| country = Hong Kong
| language = Cantonese Mandarin
| budget = 
}} Sam Lee, and Eddie Cheung.

==Plot==
A martial arts Instructor is recruited as a bodyguard for an extremely powerful couple. On her first day of duty, her employers are kidnapped. As she searches for the couple, she is led into the deadly world of underground fighting by cryptic messages from the kidnappers, who put her in the ring. There, her martial arts skills are tested, and she gets a few steps closer to freeing her clients. The next opponent that enters the ring will have her facing the ultimate challenge - with hopes of getting out of the ring alive.

==Cast==
* Jiang Lui Xia Sam Lee
* Eddie Cheung
* Kane Kosugi
* Mike Möller
* Edison Chen (Guest role) 
* Tseng Pei-Yu
* Power Chan
* Wanja Götz
*  
* Courtney Wu
* Xiong Xin Xin (Brief Appearance, Club Manager)

==Production==
The film wasnt easy to put together for Xiong Xin Xin. His film was turned down by several studios  while some approved of the script, none of them wants to risk it with a newcomer Jiang Lui Xia as female lead. With support from producer Joe Ma and Eddie Chan, Xiong Xin Xin formed his own studio with limited budget and resources,  finished filming Coweb.

==International Release==
On 11 June 2013, Coweb was released on Region 1 DVD by Lionsgate under the title Ninja Masters. Despite the name change there are no ninja featured in the movie. The Lionsgate release also includes a making of special feature that includes interviews with cast and crew as well as a behind the scenes look at some of the fights.

==References==
 

==External links==
*  
*  
*   at the Hong Kong Movie Database

 
 
 
 
 