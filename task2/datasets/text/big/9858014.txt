Diez segundos
{{Infobox film
| name           =Diez segundos
| image          =Diez Segundos.jpg
| image_size     =
| caption        =
| director       =Alejandro Wehner Carlos DAgostino
| producer       = Ciriaco Hernández Carlos DAgostino
| writer         =Manuel Arellano Martín Horacio Estol
| narrator       =
| starring       =Ricardo Duggan María Esther Buschiazzo Patricia Castell Delfy de Ortega María Rosa Gallo  Oscar Valicelli Oscar Villa
| music          =Juan Ehlert C. Sesso Ocampo     Antonio Prieto
| editor       =
| studio    =Emelco
| released       = 1949
| runtime        =83 minutes
| country        = Argentina Spanish
| budget         =
}}
 1949 Argentina|Argentine film directed by Alejandro Wehner, produced by Emelco studios.  The film is a boxing drama starring Ricardo Duggan, María Esther Buschiazzo, Patricia Castell, Carlos DAgostino (voice), Delfy de Ortega,  María Rosa Gallo, Oscar Valicelli and Oscar Villa.  It premiered on November 23, 1949 in Buenos Aires. The film was distributed by Interamericana.    Castell and Rosa Gallo would later star alongside each other in several films and television series over several decades including Perla Negra and Zíngara (1996).       

==Plot==
 
  laying a hand on the chest of Raul del Valle on the right holding the towel. ]]
The film is loosely based on Horacio Estols 1946 Vida y combates de Luis Angel Firpo, a biography of the Argentine boxer Luis Ángel Firpo who came close to beating Jack Dempsey in 1923. {{cite book
 |title=Guía quincenal de la actividad intelectual y artística argentina, Issues 57-68
 |author=Argentina. Comisión Nacional de Cultura
 |date=1950
 |page=56}} 

In the film, a humble lad starts to learn to box to defend himself, then goes on to become a professional boxer. He is trained heavily by Oscar Villa. Duggans love interest in the film is played by Patrica Castell.

== Cast ==
* María Esther Buschiazzo   
* Patricia Castell   
* Carlos DAgostino 
* Delfy de Ortega   
* Ricardo Duggan   
* María Rosa Gallo   
* Oscar Valicelli   
*Raul del Valle
* Oscar Villa

==Reception==
 
The book which the film was based on, Estols Vida y combates de Luis Angel Firpo (1946) was received quite poorly and Wehner was inexperienced as a director, resulting in a disappointing film. 

A critic of the newspaper Noticias Gráficas said (in English): "They have used a narrative method that disappoints the viewer" and compared it to the delirium a boxer experiences when being knocked out. Diario Critica newspaper said (in English) "Some secrets of the technique get some interesting effects, but it reveals absolute nullity in the management of the interpreter." Manrupe, Raúl y Portela, María Alejandra: Un diccionario de films argentinos (1930-1995) pág. 178 Buenos Aires 2001 Editorial Corregidor ISBN 950-05-0896-6 

The authors of a 2009 analysis of the role of sports in Juan Peron’s government see the film as an example of the government’s pursuit of national advancement and social mobility through sporting achievement.  Its MALBA entry describes it as a classic Argentinian B movie and as a faithful, almost anthropological, representation of life in a Buenos Aires neighborhood of the time. 

==References==
 

==External links==
 
*  

 
 
 
 
 
 