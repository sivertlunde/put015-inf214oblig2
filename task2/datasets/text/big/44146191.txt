Anthichuvappu
{{Infobox film
| name           = Anthichuvappu
| image          =
| caption        =
| director       = Kurian Varnasala
| producer       = Kurian Varnasala
| writer         =
| screenplay     = Shankar Jalaja Sankaradi
| music          = A. T. Ummer
| cinematography =
| editing        =
| studio         = Anand Movie Makers
| distributor    = Anand Movie Makers
| released       =  
| country        = India Malayalam
}}
 1984 Cinema Indian Malayalam Malayalam film, directed and produced by Kurian Varnasala. The film stars Mammootty, Shankar Panicker|Shankar, Jalaja and Sankaradi in lead roles. The film had musical score by A. T. Ummer.   

==Cast==
*Mammootty
*Mohan Elias Shankar
*Jalaja
*Sankaradi
*Mala Aravindan

==Soundtrack==
The music was composed by A. T. Ummer and lyrics was written by Poovachal Khader. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Moodalmanjumaay || Vani Jairam || Poovachal Khader || 
|-
| 2 || Naale naale || K. J. Yesudas || Poovachal Khader || 
|-
| 3 || Vellichilankayaninju vaadee || S Janaki, Chorus || Poovachal Khader || 
|}

==References==
 

==External links==
*  

 
 
 

 