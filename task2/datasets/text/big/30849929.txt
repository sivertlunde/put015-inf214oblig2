Melancholia (2008 film)
{{Infobox film
| name           = Melancholia
| image          = File:Melancholialavdiaz.jpg
| caption        = 
| director       = Lav Diaz
| producer       = Lav Diaz
| writer         = Lav Diaz
| starring       = Angeli Bayani Perry Dizon
| music          = Lav Diaz The Brockas
| cinematography = Lav Diaz
| editing        = Lav Diaz
| studio         = 
| distributor    = 
| released       =  
| runtime        = 450 minutes
| country        = Philippines
| language       = Filipino
| budget         = 
}}
Melancholia is a 2008 Philippine film directed by Lav Diaz. It won the Horizons prize at the 65th Venice International Film Festival. 

== Cast ==
*Angeli Bayani as Alberta Munoz / Jenine
*Perry Dizon as Julian Tomas / pimp
*Roeder as Renato Munoz
*Dante Perez as rebel
*Raul Arellano as rebel
*Malaya as Rina Abad / nun
*Irma Adlawan as Spiritist / store owner
*Cookie Chua as Patricia / kundiman singer
*Yanyan Taa as Hannah

== Reception ==
Ronnie Scheib of Variety (magazine)|Variety called the film "Lav Diazs latest madly uncommercial 7½ -hour magnum opus", and particularly complimented the "extraordinary final chapter". Scheib summarised: "Simultaneously lamenting the futility of change yet celebrating reinvention, the improbable Melancholia lingers on the brain." 

== References ==
 

== External links ==
* 
* 
* 

 
 
 
 
 
 

 