Si Agimat at si Enteng Kabisote
{{Infobox film
| name           = Si Agimat at Si Enteng Kabisote
| image          = Si Agimat at si Enteng Kabisote.jpg
| alt            = Theatrical release poster
| caption        = Theatrical movie poster
| director       = Tony Y. Reyes  . inquirer.net.  
| producer       =  
| writer         = 
| starring       = Vic Sotto Bong Revilla Gwen Zamora Sam Pinto Oyo Boy Sotto Bing Loyzaga Amy Perez
| music          = 
| cinematography = 
| editing        = 
| studio         = APT Entertainment GMA Films Imus Productions OctoArts Films M-Zet Productions
| distributor    = GMA Films OctoArts Films
| released       =  
| runtime        = 
| country        = Philippines
| language       = Filipino Tagalog
| budget         = 
| gross          = 
}} action comedy fantasy crossover film directed by Tony Reyes which stars Vic Sotto and Sen. Bong Revilla Jr. This was the union of their famous characters (Agimat and Enteng Kabisote) in one blockbuster movie which grossed a P31M on its opening day. It is followed by the sequel Si Agimat, si Enteng Kabisote at si Ako.

==Cast==
===Main Cast===
*Vic Sotto as Enteng Kabisote
*Bong Revilla|Sen. Bong Revilla Jr. as Agimat

===Supporting Cast===
*Aiza Seguerra as Aiza Kabisote
*Oyo Boy Sotto as Benok Kabisote
*Mikylla Ramirez as Ada Kabisote
*Gwen Zamora as Faye Kabisote
*Sam Pinto as Samara
*Amy Perez as Ina Magenta
*Peque Gallaga as Ermitanyo
*Bing Loyzaga as Satana Jeorge Estregan Jr. as Magat
*Benjie Paras as Abugan
*Alex Crisano as Asupre
*Jose Manalo as Jose
*Wally Bayola as Bodyguard Boggart
*Ruby Rodriguez as Amy
*Jillian Ward as Bebeng
*Barbie Forteza as Bratty
*Joshua Dionisio as Jayson

===Extended Cast===
*Bea Binene as Saling
*Jake Vargas as Odoy
*King Gutierrez as Sartonia Saida Diola as Engkantada #1
*Ellen Adarna as Engkantada #2

===Special Guest===
*Rufa Mae Quinto
*Boobay
*Mang Enriquez
*Shalala
*John Feir
*Marissa Delgado Bayani Casimiro Jr.

==Reaction==
While the movie opened at #1 on December 25, 2010 with P31,000,000 in ticket sales,  it was criticized for being predictable, having sexual innuendos, and having some home-truths as to how Filipino viewers idealize heroism and romance. Also noted is how the film tries to play fair when it comes to mixing the two heroes together.

In an online review, Philbert Ortiz Dy described Si Agimat at Si Enteng Kabisote as "really nothing more than a commercial proposition" and that it "took the two highest grossing stars of Filmfests prior and stuffed them together with little rhyme or reason, putting them in the same fantasy adventure settings that made them so successful. There’s precious little love to be found within these frames, all the elements of the film existing for the pure fact that they worked before. This doesn’t even feel like the movie the filmmakers wanted to make." 

Although the critical reception was not too good, the movie still ended making P159 million by the end of the festival and went on to have extended showings in theaters.

==Accolades==
Si Agimat at Si Enteng Kabisote got two awards at the "Gabi ng Parangal" on December 26, 2010. 

{|| width="90%" class="wikitable sortable"
|-
! width="10%"| Year
! width="30%"| Award-Giving Body
! width="25%"| Category
! width="25%"| Recipient
! width="10%"| Result
|- 2010
| rowspan="2" align="left"| Metro Manila Film Festival  Best Visual Effects
| align="center"| Rico Guttierez
|  
|- Best Make-up Artist
| align="center"| –
|  
|- 2011 GMMSF GMMSF Box-Office Vic Sotto and Bong Revilla|| 
|- Most Popular Tony Y. Reyes  (with Wenn Deramas) || 

|}

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 