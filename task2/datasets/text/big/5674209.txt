Sugar Hill (1994 film)
{{Infobox film
| name = Sugar Hill
| image = Sugar hill 1994 movie poster.jpg
| caption = The film poster for Sugar Hill
| writer = Barry Michael Cooper
| starring = {{plainlist|
* Wesley Snipes Michael Wright
* Theresa Randle
* Clarence Williams III
* Abe Vigoda
* Ernie Hudson|
}}
| director = Leon Ichaso
| producer = {{plainlist|
* Greg Brown
* Rudy Langlais
}}
| music = Terence Blanchard
| distributor = 20th Century Fox
| released =  
| runtime = 123 minutes
| language = English
| budget = $10 million   |
| gross = $18.3 million 
}} Michael Wright Harlem neighborhood of New York City.

==Plot==
Through the course of the film, several flashbacks are shown involving the Skuggs brothers, including (in the beginning of the film) the drug-overdose death of their mother Ella (Khandi Alexander), the non-fatal shooting of their drug-addicted musician father, Arthur Romello "A.R." Skuggs (Clarence Williams III) (ultimately at the hands of the man they would later work for—Gus Molino (Abe Vigoda), and a scene where Roemello is offered a full scholarship to Georgetown. Roemello, as a teenager (Dulé Hill) avenges his father’s shooting by shooting and killing Sal Marconi (Raymond Serra), a business partner of Gus’s.  After contemplating for a while, Roemello decides to quit dealing and start a new life with his girlfriend, Melissa (Theresa Randle), to the disdain of Raynathan, who is scared and hesitant to leave the drug game.  However, Roemello learns that getting out is nowhere near as easy as getting in.
 Steve Harris) at the hands of an up-and-coming Brooklyn drug dealer and former boxing champion, Lolly Jonas (Ernie Hudson).  The Skuggs brothers and their associates find Ricky’s burned body hanging from the side of a neighborhood apartment building.  They later go after and then kill Tony Adamo, one of the other men responsible for Ricky Goggles’ death.  Because of this, an eventual street war starts off between The Skuggs crew and Lolly’s organization.

Melissa becomes more hesitant of being involved with Roemello, because of his lifestyle.  After learning of the death of an aspiring teenage “stick-up kid”, Kymie (Donald Faison) in Roemello’s neighborhood (Kymie, in fact, saves Roemello’s life in a drive-in shooting by Lolly’s people), she decides to break off with Roemello and would have one date with an abusive basketball star, Mark Doby (Vondie Curtis Hall).  After an episode where Mark physically abused Melissa, she eventually returns to Roemello and they begin to make plans to leave New York City.

Before Roemello and Melissa depart for North Carolina, they stop by to visit A.R.  However, upon arriving at A.R.’s apartment, they find him dead of a drug overdose.  Raynathan gave A.R. the heroin that would eventually kill him, with Raynathan’s reasoning being that he wanted “put him out of his misery”.  Raynathan is found across the street, coming out of Gus’ restaurant, where he gunned down Gus, Lolly, and Harry, Gus’s son.

Roemello tells Raynathan what happened to A.R., but Raynathan accepts responsibility of their father’s death.  After seeing Melissa waiting for Roemello, Raynathan fires his gun at her, and the brothers proceed to fight each other, with Raynathan accidentally shoots Roemello.  Realizing this, Raynathan panics and shoots himself in the stomach, taking his own life.

Roemello and Melissa, sometime later, do move to North Carolina, where they have a young son, but Roemello is found in a wheelchair, likely paralyzed from the waist down (though the extent of the paralysis is not fully explained), however he is enjoying family life.

==Cast==
*Wesley Snipes as Romello Skuggs Michael Wright as Raynathan Skuggs
*Clarence Williams III as Arthur Romello "A.R." Skuggs
*Theresa Randle as Mellisa
*Abe Vigoda as Gus Molino
*Ernie Hudson as Lolly Jonas Steve Harris as Ricky Goggles
*O.L. Duke as Tutty
*Donald Faison as Kymie
*Joe Dallesandro as Tony Adamo
*Leslie Uggams as Doris Holly
*Vondie Curtis Hall as Mark Doby
*Khandi Alexander as Ella Skuggs
*DeVaughn Nixon as Raynathan Skuggs (age 11) Sam Gordon as Raynathan Skuggs (age 18)
*Marquise Wilson as Roemello Skuggs (age 10)
*Dulé Hill as Roemello Skuggs (age 17)

==Reception==
Rotten Tomatoes, a review aggregator, reports that 20% of ten surveyed critics gave the film a positive review; the average rating is 4.9/10.   Todd McCarthy of Variety (magazine)|Variety called it "a self-indulgent drama" that plays like a dreary variation on New Jack City".   Janet Maslin of The New York Times called it "an ambitious but terminally self-important film".   Kenneth Turan of the Los Angeles Times wrote that it "sinks under the weight of excessive violence and a welter of overwrought plot contrivances".   Owen Gleiberman of Entertainment Weekly rated it C− and wrote, "Though the movie itself isn’t much — a dawdling inner-city pastiche of Mean Streets and the Godfather films — a couple of the performers do succeed in fleshing out their threadbare roles." 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 