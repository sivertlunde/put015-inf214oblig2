Pra Quem Fica, Tchau
 
{{Infobox film
| name           = Pra Quem Fica, Tchau
| image          = 
| caption        = 
| director       = Reginaldo Faria
| producer       = Reginaldo Faria Roberto Farias
| writer         = Reginaldo Faria
| starring       = Reginaldo Faria
| music          = 
| cinematography = José Medeiros
| editing        = 
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = Brazil
| language       = Portuguese
| budget         = 
}}
 Best Foreign Language Film at the 44th Academy Awards, but was not accepted as a nominee. 

==Cast==
* Reginaldo Faria as Didi
* Stepan Nercessian as Lui
* Rosana Tapajós as Maria
* Flávio Migliaccio as Chuca
* José Lewgoy as Tio Gustavo
* Jorge Cherques as Marido de Maria
* Irma Álvarez as Mulher do jeep
* Tânia Scher as Mulher do conversível
* Hugo Bidet as Teleco
* Gracinda Freire as Tia Lourdes
* Wilza Carla as Dalva
* Henriqueta Brieba as Mãe de Maria

==See also==
* List of submissions to the 44th Academy Awards for Best Foreign Language Film
* List of Brazilian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 
 
 

 
 