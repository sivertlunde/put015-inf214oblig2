The Wedding Director
{{Infobox Film
| name           = The Wedding Director
| image          = The Wedding Director.jpg
| caption        = Film poster
| director       = Marco Bellocchio
| producer       = Marco Bellocchio
| writer         = Marco Bellocchio
| narrator       = 
| starring       = Sergio Castellitto
| music          = 
| cinematography = Pasquale Mari
| editing        = Francesca Calvelli
| distributor    = 
| released       = 21 April 2006
| runtime        = 97 minutes
| country        = Italy
| language       = Italian
| budget         = 
| preceded by    = 
| followed by    = 
}}

The Wedding Director ( ) is a 2006 Italian drama film directed by Marco Bellocchio. It was screened in the Un Certain Regard section at the 2006 Cannes Film Festival.   

==Cast==
* Sergio Castellitto - Franco Elica
* Donatella Finocchiaro - Bona di Gravina
* Sami Frey - Principe di Gravina
* Gianni Cavina - Smamma
* Maurizio Donadoni - Micetti
* Bruno Cariello - Enzo Baiocco
* Simona Nobili - Maddalena Baiocco
* Claudia Zanella - Chiara Elica
* Corinne Castelli - Fara Domani / Lucia Mondella
* Silvia Ajelli - Gioia Rottofreno / Monaca di Monza
* Aurora Peres - Sposa
* Giacomo Guernieri - Sposo
* Carmelo Galati - Luigi

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 
 