Kid Glove Killer
{{Infobox film
| name           = Kid Glove Killer
| image_size     =
| image	         = Kid Glove Killer FilmPoster.jpeg
| caption        =
| director       = Fred Zinnemann
| producer       = Jack Chertok
| writer         = John C. Higgins (story and screenplay) Allen Rivkin (screenplay) Marsha Hunt Lee Bowman Samuel S. Hinds David Snell
| cinematography =
| editing        =
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 74 minutes
| country        = United States
| language       = English
| budget         = $199,000  . 
| gross          = $550,000 (initial release) 
}}
Kid Glove Killer is a 1942 crime film starring Van Heflin as a criminologist investigating the murder of a mayor. The B film was the feature-length directorial debut of Fred Zinnemann.

==Plot Summary==
Ambitious young attorney Gerald I. Latimer helps mayoral candidate Daniels and district attorney candidate Turnely to be elected; the pair had vowed to rid the city of its pernicious criminal rackets.

The two elected officials are unaware that Gerald has paired up with one of the citys biggest gangsters, Matty, to get help in getting Gerald elected to the U.S. Senate in exchange for future political favors.

When Turnely becomes troublesome, Gerald and Matty arrange his murder. Gerald is appointed special prosecutor, and gets to meet the crew that investigates the district attorneys murder. His good friend, forensic scientist Gordon McKay, and his assistant, Jane Mitchell, examine the body and determine the identity of the hit man, who dies while trying to avoid capture.

Restauranteur Eddie Wright, tired of being harassed by the racketeers, visits the mayors office and volunteers to help fight the criminals. The police take him for a hobo and he is taken into custody and questioned.

The mayor questions Gerald about a large insurance policy he bought, wanting to know where the money came from. Gerald is worried that the mayor will find out about his dealings with the gangsters and decides to get rid of him too. He places a bomb in the mayors car, and the mayor dies when the bomb goes off.

The police suspect Eddie of having placed the bomb, and detain him. Some circumstantial evidence points to Eddie but Gordon is skeptical and continues the investigation although Gerald calls for Eddies arrest. 
 
Gerald spends a lot of time in the police crime lab and eventually falls in love with Jane. He even asks her hand in marriage, but she rejects him, explaining she cant marry and quit her job until the double homicide investigation is finished. She tells him Gordon has concluded that the man planting the bomb should have gun powder under his nails.

Gerald rushes off to scrub his hands meticulously, but Gordon later finds a note in the mayors office implicating Gerald. He suspects Gerald of both murders, and calls on Gerald to surreptitiously obtain a hair sample from him.

After getting the sample, Gordon tells Jane he has found the killer, but he wont reveal his name. When Jane and Gerald meet again and she agrees to marry him, she tells Gerald that Gordon has found the killer through a hair sample. Gerald realizes he has to kill his friend Gordon.

Gerald sets up a meeting with Gordon and Matty, and gives his car keys to Jane so she can drive herself home. She sees the cigar cutter on the key ring and realizes it could have been used to cut bomb wires. She takes it to the crime lab for examination.

Gerald gets a gun from Matty, who shows him how to use it. He rushes to the crime lab to kill Gordon. When he enters Gordons office he asks him to hand over the evidence incriminating him, and Jane overhears the shouting from the lab.

Gerald is confessing the killings to Gordon when Jane enters the office. Gordon overpowers Gerald and gets the gun. The police arrive at the scene shortly after, and both Gerald and Matty are arrested. Gordon realizes he is in love with Jane and proposes to her. She willingly accepts. 

==Cast==
*Van Heflin as Gordon McKay Marsha Hunt as Jane Mitchell
*Lee Bowman as Gerald I. Ladimer
*Samuel S. Hinds as Mayor Daniels
*Cliff Clark as Captain Lynch
*Eddie Quillan as Eddie Wright
*John Litel as Matty
*Cathy Lewis as Bessie Wright
*Nella Walker as Mrs. Daniels
*Jeff York as Henchman (uncredited)
*Ava Gardner as Carhop (uncredited)
*Paul Fix as Allison Stacy (uncredited)

==Reception==
The film earned $336,000 in the US and Canada and $214,000 elsewhere during its initial theatrical run, making MGM a profit of $161,000. 
==References==
 
==External links==
* 
*  
*  
 

 
 
 
 
 
 
 
 