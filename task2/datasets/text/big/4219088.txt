Gregory's Two Girls
 
{{Infobox film|
  name        = Gregorys Two Girls |
  image       = Gregorys Two Girls - DVD cover.jpg |
  caption     = DVD release cover |
  writer      = Bill Forsyth |
  starring    = John Gordon Sinclair Carly McKinnon Maria Doyle Kennedy Hugh McCue Dougray Scott Dawn Steele Alexander Morton |
  director    = Bill Forsyth | Christopher Young|
  distributor = Channel 4 |
  released    = 15 October 1999 |
  runtime     = 116 minutes |
  language    = English |
  budget      = |
}}

Gregorys Two Girls is a 1999 British film. It is the sequel to Gregorys Girl (1981), which also starred John Gordon Sinclair and was written and directed by Bill Forsyth. The movie received mixed reviews.

==Plot summary==
Eighteen years after the events of Gregorys Girl, Greg Underwood (Sinclair) – now a 35-year-old English teacher in his former secondary school – has fantasies about 16-year-old student Frances (McKinnon). His politically motivated lessons inspire Frances and Douglas, another student, to plot to overthrow a businessman they suspect of trading in torture equipment.

==References==
 

==External links==
* 
*  Edinburgh Film Festival

 
 

 
 
 
 
 
 
 

 