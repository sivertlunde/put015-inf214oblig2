The Fury of Achilles
{{Infobox film
 | name =The Fury of Achilles
 | image =The Fury of Achilles.jpg 
 | caption =
 | director = Marino Girolami
 | writer = Homer (poem) Gino De Santis   
 | starring =  Gordon Mitchell Jacques Bergerac Piero Lulli
 | music =   Carlo Savina
 | cinematography =   Mario Fioretti
 | editing =  Mirella Casini
 | producer =  Samuel Z. Arkoff Italian
| country = Italy
| released = 1962
| runtime = 118 min
 }} historical drama set in the ninth year of the Trojan War, and is based primarily on Homers Iliad.  The film was directed by Marino Girolami and starred Gordon Mitchell as Achilles. 

==Plot==
Care fellow Greeks to Troy, since the prince Paris has abducted the Spartan Princess Helen, wife of Menelaus. In the fighting stands the invincible hero Achilles, who leads his Myrmidons to assault. Now in the tenth year of the war, Troy has not yet been destroyed. For the contention of a slave, Agamemnon, king of the Greeks offends Achilles because a female slave and the hero withdraws from the war, creating confusion in the army. The Trojans in fact have the opportunity to drive out the Greeks at sea, and so Patroclus, Achilles best friend wears, without the knowledge of Achilles, his divine armor to instill courage in the soldiers Myrmidons. But Patroclus is killed by the Trojan prince Hector; Achilles rages, killing many Trojans, and the same Hector.

==Cast==
* Gordon Mitchell as "Achilles"
* Jacques Bergerac as "Hector"
* Cristina Gaioni as "Xenia" 
* Gloria Milland as "Briseis"
* Piero Lulli as "Odysseus"
* Roberto Risso as "Paris (mythology)|Paris"
* Mario Petri as "Agamemnon"
* Erminio Spalla as "Nestor (mythology)|Nestor"
* Fosco Giachetti as "Priam"
* Ennio Girolami as "Patroclus"
* Tina Gloriani as "Andromache"

==Production== The Trojan Horse. These scenes are omitted in the UK version which is altogether 20 minutes shorter. 

==Biography==
* 
* 

==See also==
* List of historical drama films
* Greek mythology in popular culture

== References ==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 

 
 