Mohabbat Zindagi Hai (1966 film)
{{Infobox Film
| name           =Mohabbat Zindagi Hai
| image          = 
| image_size     = 
| caption        = 
| director       = Jagdish Nirula
| producer       = K C Gulati
| writer         = 
| narrator       =  Mehmood
| music          = O. P. Nayyar S. H. Bihari (lyrics)
| cinematography = V Ratra
| editing        = VK Singh
| distributor    = 
| released       = 1966
| runtime        = 
| country        =   India
| language       = Hindi 
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

Mohabbat Zindagi Hai is a 1966 Hindi movie produced by K. C Gulati for Roop Chaya. Its directed by Jagdish Nirula. The film stars Dharmendra, Rajshree, Mehmood Ali|Mehmood, Deven Verma and Johnny Walker (actor)| Johnny Walker. The films music is by O. P. Nayyar with lyrics by S. H. Bihari.

== Music ==

* bijli ho ya ghata ho ek shokh dilruba ho (Mahendra Kapoor)
* mehfil mein dilwaalon ki aata hai jo aane do (Asha Bhosle, Mahendra Kapoor)
* na jaane kyon hamaare dil ko tumne dil nahin samjha (Mohammed Rafi)
* nazar nazar se milaao to koi baat bane (Asha Bhosle)
* raaton ko chori chori bole mora kangna abke jo barkha aayi (Asha Bhosle)
* tum sabse haseen ho aur sabse jawaan ho (Asha Bhosle, Mahendra Kapoor)
* tumhaari mulaaqaat se mujhko pata yeh chala ki mere bhi seene mein dil hai (Mohammed Rafi)
* yeh purnoor chehra yeh dilkash ada (Mohammed Rafi)



== External links ==
*  

 
 
 
 