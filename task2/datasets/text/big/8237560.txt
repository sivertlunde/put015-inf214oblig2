Gunhed (film)
 
 
{{Infobox film
| name           = Gunhed
| image          = Gunhedposter.jpg
| caption        = Original theatrical poster for Gunhed
| director       = Masato Harada
| producer       = Yoshishige Shimatani Tomoyuki Tanaka Tetsuhisa Yamada Eiji Yamaura
| writer         = James Bannon Masato Harada
| screenplay     = 
| story          = 
| based on       = 
| starring       = Masahiro Takashima Brenda Bakke
| music          = Toshiyuki Honda Takayuki Baba
| cinematography = Junichi Fujisawa
| editing        = Yoshitami Kuroiwa	 	 Sunrise Bandai Kadokawa Shoten IMAGICA
| distributor    = Toho Manga Entertainment (Australia and the UK)
| released       =  
| runtime        = 100 min.
| country        = Japan Japanese English English
| budget         = ¥1,500,000,000
| gross          = 
| italic title=no
}}
  is a 1989 Japanese live-action mecha film. It was adapted by Kia Asamiya into the manga Gunhed, based on a screenplay by James Bannon and Masato Harada.

==Plot==
In 2038, a gang of scavengers infiltrate an industrial complex on an island within a prohibited zone. They are looking for the element Texmexium, which is rare and very valuable. The scavengers are killed off by the automated defenses, until only Brooklyn, the groups mechanic, is left. 

He discovers a stranded Texas Air Ranger and two children living among the complexs rubble. Together, they must destroy the Kyron-5 computer in order to escape and warn humanity. Brooklyn discovers a GUNHED (from "Gun UNit Heavy Elimination Device") combat robot left over from when a battalion of Gunheds were sent to destroy Kyron-5. Brooklyn works to restore it to operation. Meanwhile, Babe, one of the scavengers killed earlier, has been transformed into a bio-droid and is seeking out Sergeant Nim and the Texmexium she stole. 

In order to save her and destroy Kyron-5, Brooklyn must overcome his fear of flying and pilot the GUNHED to the top of the complexs tower. The computers single large defence is the robotic Aerobot.

==Production notes==
The concept for the film came from a story contest that Toho held in 1986, which was to decide on the narrative for the next installment in the Heisei Godzilla series. Tatsuo Kobayashi was the contestant who was noted for his Godzilla 2 script, which had the King of the Monsters facing off against a giant computer, but was beat out by Shinichiro Kobayashi with his early draft for Godzilla vs. Biollante. However, Toho did not scrap Tatsuos entry, but instead had Masato Harada heavily rework the idea, removing Godzilla and other elements from the film, until they were left with the final product. Harada had his name removed from the US version; he is credited as Alan Smithee instead. The Gunhed was designed by Shoji Kawamori. The tank prop that portrayed Gunhed is in Japan on public display. The image of Gunhed has been digitized into several early generation space shooting games, but the actual version of the tank was never used and was replaced by a flying spaceship.

==Legacy== video game based on the film was released for the PC-Engine. In North America, it was localized as Blazing Lazers. Another video game based on the film was released in 1990 for the Famicom titled Gunhed Aratanaru Tatakai. It was developed by Varie.  The Front Line Assembly music video for their 1992 single "Tactical Neural Implant|Mindphaser" was created with a mixture of footage from Gunhed, with some original filming of the band members in a cyberpunk style. The William Gibson novel Virtual Light includes an armored patrol vehicle nicknamed "Gunhead". James Cameron, the director of Avatar (2009 film)|Avatar, is a fan of the film.

==Cast==
* Masahiro Takashima as Brooklyn
* Brenda Bakke as Sergeant Nim, Texas Air Ranger
* Aya Enyoji as Babe
* Yujin Harada as Seven
* Kaori Mizushima as Eleven
* Brewster Thompson as Barabbas James
* Doll Nguyen as Boomerang
* Jay Kabira as Bombbay
* Randy Reyes as Gunhed (Voice)
* Mickey Curtis as Bansho the Captain of Mary Ann
* Yōsuke Saitō (actor)|Yōsuke Saitō as Boxer
* Michael Yancy as the Narrator

==Notes and references==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 