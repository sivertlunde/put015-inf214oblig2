Wins Out
{{Infobox Hollywood cartoon|
| cartoon_name = Wins Out
| series = Oswald the Lucky Rabbit
| image = 
| caption = 
| director = Walter Lantz Bill Nolan
| story_artist = 
| animator = Manuel Moreno Ray Abrams Fred Avery Lester Kline Vet Anderson
| voice_actor = 
| musician = James Dietrich
| producer = 
| studio = Walter Lantz Productions
| distributor = Universal Pictures
| release_date = March 14, 1932
| color_process = Black and white
| runtime = 7:04 English
| preceded_by = Mechanical Man
| followed_by = Beau and Arrows
}}

Wins Out is a short animated film by Walter Lantz Productions, featuring Oswald the Lucky Rabbit.

==Plot==
Oswald is a baker who bakes for a hippo king. For the majestys dinner, Oswald bakes a giant pie filled with living crows, a reference to the nursery rhyme Sing a Song of Sixpence. When a little mess from his work gets on his masters face, the hippo king threatens to penalize Oswald. Fortunately for Oswald, the princess, who is a living doll, comes to his defense and begs to give the rabbit a second chance. The hippo king forgives Oswald but warns him that hed be beheaded if something unpleasant happens to the pie.

Oswald is back at the kitchen, standing next to the pie. While the hippo kings dinner is still hours away, a crow from within the pie calls another crow from out the window. To Oswalds surprise, the outsider crow cuts a hole in the pie, enabling the crows from inside the dish to escape.

As the crows are just outdoors, Oswald attempts to put salt on their tails. One of the crows, however, is clever to replace the salt shaker with one containing pepper. When Oswald sprinkles pepper which has no effect, that crow blows the condiment on the rabbits face. But as Oswald sneezes, the crows have their feathers blown off, therefore eliminating their ability to fly. The crows still choose to get away on foot. Oswald resorts to shooting arrows. One of Oswalds arrows unintendedly hits a giant serpent. The provoked serpent heads toward the hippo kings castle. Oswald and the crows hide themselves in the pie. The serpent also enters the dish.

The hippo king calls for the pie as it is dinner time. The servants are the ones who carry the dish to him. When Oswald pops out of the pie, the hippo king is quick to give the rabbit to the headsman. But before Oswald could lose a head, the serpent also pops out of the pie. The serpent frightens everybody around, especially the headsman who leaves the castle for good. The serpent catches the doll princess but Oswald is able to rescue her. Oswald and the doll princess continue to keep distance as the long beast pursues them. As the chase goes on in circles, the serpent, without realizing, devours its tail and up to its torso, thus destroying itself.

Following the defeat of the serpent, the hippo king resumes to have the pie, and no longer seeks to penalize Oswald. The crows come out of the dead serpents mouth (implying they were swallowed sometime earlier) before reentering the pie. As the pie is presented to the hippo king, the crows rise to the surface of the dish, and play musical instruments in the style of an orchestra.

==External links==
*   at the Big Cartoon Database
*  

 

 
 
 
 
 
 
 
 
 

 