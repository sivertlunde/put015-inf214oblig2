Suvorov (film)
{{Infobox film
| name           = Suvorov
| image          = Suvorov (film).jpg
| image_size     = 250px
| caption        = Film poster
| director       = Vsevolod Pudovkin Mikhail Doller
| producer       = 
| writer         = Georgiy Grebner Nikolai Ravich
| narrator       = 
| starring       = Nikolai Cherkasov-Sergeyev Aleksandr Khanov
| music          = Yuri Shaporin
| cinematography = Anatoli Golovnya Tamara Lobova
| editing        = 
| distributor    = 
| studio         = Mosfilm
| released       = 23 January 1941 (USSR) 19 September 1941 (U.S.)
| runtime        = 2948 metres (108 minutes)
| country        = Soviet Union
| language       = Russian
| budget         = 
}}
 Soviet film Alexander Vasilyevich Suvorov (1729 – 1800), one of the few great generals in history who never lost a battle. It was released as General Suvorov in the USA. In 1941 Pudovkin, Doller, Cherkasov-Sergeyev, and Khanov received the Stalin Prize for the film.
 
==Cast== Alexander Vasilyevich Suvorov
* Aleksandr Khanov - Platonych
* Mikhail Astangov - Aleksandr Andreyevich Arakcheyev Pavel I Prokhor
* S. Kiligin - Pyotr Bagration
* Vsevolod Aksyonov - Meshchersky
* Aleksandr Antonov
* Aleksandr Khvylya
* Galina Kravchenko
* Anatoli Solovyov

==External links==
* 

 

 
 
 
 
 
 
 
 

 