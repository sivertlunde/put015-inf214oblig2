Les Misérables (1958 film)
 
{{Infobox film
| name = Les Misérables
| image = 
| image_size = 
| alt = 
| caption = 
| director = Jean-Paul Le Chanois
| screenplay = Michel Audiard René Barjavel
| based on =  
| starring = Jean Gabin
| music = Georges Van Parys
| cinematography = Jacques Natteau
| editing = Lieselotte Johl Emma Le Chanois
| studio = Deutsche Film (DEFA)
| distributor = Pathé   VEB Progress Film-Vertrieb   Continental Distributing  
| released =  
| runtime = 217 minutes
| country = France East Germany Italy
| language = French
}} novel released in France on 12 March 1958. Written by Michel Audiard and René Barjavel, the film was directed by Jean-Paul Le Chanois. It stars Jean Gabin as Jean Valjean. 

==Adaptation==
The bishops background is briefly sketched rather than detailed as in the novel. Javert is a young boy, the son of a guard in the Toulon prison, when he sees Valjean as a convict. Sister Simplice admits Valjean and Cosette to the convent instead of Father Fauchevent. Thénardier, in disguise, meets Marius and proves to him with the help of newspaper clippings that he is completely mistaken about Valjeans criminal past.

==Cast==
{{columns-list|2|
* Jean Gabin as Jean Valjean/Champmathieu
* Bernard Blier as Javert (father and son)
* Danièle Delorme as Fantine
* Bourvil as Thénardier
* Elfriede Florin as La Thénardier
* Giani Esposito as Marius Pontmercy
* Béatrice Altariba as Cosette
** Martine Havet as young Cosette
* Silvia Monfort as Éponine
** Mireille Daix as young Éponine
* Jimmy Urbain as Gavroche
* Serge Reggiani as Enjolras
* Fernand Ledoux as Monseigneur Myriel
* Isabelle Lobbé as Azelma
* Jean dYd as Mabeuf
* Jean Murat as Colonel Georges Pontmercy
* Lucien Baroux as  Monsieur Gillenormand
* Suzanne Nivette as Mademoiselle Gillenormand
* Jacques Harden as Courfeyrac
* Marc Eyraud as Grantaire
* Werner Dissel as Brevet
* Beyert as Bahorel 
* Hans-Ulrich Laufer as Combeferre 
* Gérard Darrieu as Feuilly
* Pierre Tabard as Prouvaire
* Henri Guégan as Laigle
* Julienne Paroli as Madame Magloire
* Laure Paillette as Toussaint
* Madeleine Barbulée as Soeur Simplice
* Christian Fourcade as Petit Gervais
* Bernard Musson as Bamatabois
* René Fleur as The cardinal Ardisson as A gendarme
* Jean Ozenne as The prefect of Montreuil
* Bernard Musson as A bourgeois
* Gerhard Bienert as The president of the court
* Harry Hindemith as Un bagnard
}}

==Production== film adaptations Valjean and Javert were far apart in age, rather than near contemporaries as in the novel. Instead of Javert recognizing Valjean as a convict he had often guarded years earlier, he remembers how, when he was just a boy, his prison guard father had pointed out this man as "the worst kind of prisoner, who tried to escape four times". 

==Release==
The New York Times described it as one of the first French "blockbusters" that appeared in response to such lengthy feature films as Around the World in 80 Days and The Ten Commandments. It said it was "a ponderous four-hour retelling of Victor Hugos oft-filmed epic.... Not a page is skipped... Too literary, it has the saving grace of Jean Gabins truly heroic depiction of Jean Valjean plus some stirring scenes on the barricades."  It was a "quintessential Gabin role ... that of a loner, an outsider, usually a member of the lower orders who may flirt with love and happiness but knows they are not for him".   

The film did not premiere in New York until July 1989, when it ran to coincide with the celebration of the bicentennial of the French Revolution. 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 