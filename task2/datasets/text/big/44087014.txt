Makara Vilakku (film)
{{Infobox film 
| name           = Makara Vilakku
| image          =
| caption        =
| director       = PK Joseph
| producer       =
| writer         = S. L. Puram Sadanandan
| screenplay     = S. L. Puram Sadanandan Jose Sankaradi Sumesh Prathapachandran
| music          = KJ Joy
| cinematography =
| editing        =
| studio         = Malithra Productions
| distributor    = Malithra Productions
| released       =  
| country        = India Malayalam
}}
 1980 Cinema Indian Malayalam Malayalam film,  directed by PK Joseph. The film stars Jose (actor)|Jose, Sankaradi, Sumesh and Prathapachandran in lead roles. The film had musical score by KJ Joy.   

==Cast== Jose
*Sankaradi
*Sumesh
*Prathapachandran
*Sathaar
*Aranmula Ponnamma
*Balan K Nair
*Jayanthi
*Kanakadurga
*Vazhoor Rajan

==Soundtrack==
The music was composed by KJ Joy and lyrics was written by Sreekumaran Thampi. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Makaravilakke makaravilakke || Sreekanth || Sreekumaran Thampi || 
|-
| 2 || Vasanthathin Virimaaril || Karthikeyan || Sreekumaran Thampi || 
|}

==References==
 

==External links==
*  

 
 
 

 