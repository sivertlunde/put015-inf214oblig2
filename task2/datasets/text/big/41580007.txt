Faithful in My Fashion
{{Infobox film
| name           = Faithful in My Fashion  
| image_size     =
| image	         = 
| caption        =
| director       = Sidney Salkow
| producer       = 
| writer         = 
| based on = 
| starring       = Donna Reed Tom Drake Edward Everett Horton Spring Byington Harry Davenport (actor)
| music          =
| cinematography =
| editing        =
| distributor    = Metro-Goldwyn-Mayer
| released       = 1946
| runtime        = 
| country        = United States English
| budget         = $680,000  . 
| gross          = $626,000 
| preceded_by    =
| followed_by    =
}}
Faithful in My Fashion is a 1946 film. Jeff (Tom Drake) arrives home to New York City after being away in the Navy for several years. Unaware that his fiancée, Jean (Donna Reed), is now dating a man (Warner Anderson) at the department store where she works, Jeff assumes she is still intends to marry him. In order to save Jeff from heartache, several employees at Jeans store set up a ruse to keep Jeff unaware of Jeans new man until he is deployed again. Jean cooperates with the ruse, but it isnt long before secrets get revealed.


==Reception==
According to MGM records the movie was not a hit, earning $486,000 in the US and Canada and $140,000 elsewhere, making a loss to the studio of $307,000. 

==References==
 

==External links==
*  at TCMDB
* 

 
 
 
 
 
 


 