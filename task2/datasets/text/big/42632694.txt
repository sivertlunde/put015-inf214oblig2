The Possessed (1977 film)
{{Infobox television film
| name           = The Possessed
| director       = Jerry Thorpe
| producer       = Philip Mandelker Jerry Thorpe
| screenplay     = John Sacret Young
| starring       = James Farentino Joan Hackett Harrison Ford Claudette Nevins
| music          = Leonard Rosenman 
| cinematography = Chuck Arnold
| editing        = Michael A. Hoey
| distributor    = Warner Bros.
| released       =   }}
| runtime        = 76 minutes 
| country        = United States
| language       = English
| budget         = $8 million  
| network        = NBC
| image = The Possessed (1977 film).jpg
}} 1977 Television TV film directed by Jerry Thorpe, written by John Sacret Young, and starring James Farentino and Joan Hackett.  It is about a priest who returns from the dead to battle satanic forces at an Single-sex education|all-girls school.

== Plot == alcoholic priest who has strayed from his faith, dies in a car crash.  As penance, he is sent back to Earth to fight evil as an exorcist.  His travels take him to an all-girls school, where he fights a demon that has taken up residence.

== Cast ==
* James Farentino as Kevin Leahy 
* Joan Hackett as  Louise Gelson
* Harrison Ford as Paul Winjam
* Claudette Nevins as Ellen Sumner
* P. J. Soles as Marty
* Diana Scarwid as Lane

== Release ==
The Possessed was first broadcast on NBC on May 1, 1977. 

== Reception ==
Steve Barton of Dread Central rated it 3.5/5 stars and called it a "token TV movie The Exorcist (film)|Exorcist knock-off" that is "actually ridiculously entertaining."   Paul Mavis of DVD Talk rated it 4/5 stars and called it an "extremely effective made-for-TV supernatural horror film." 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 
 
 
 


 
 