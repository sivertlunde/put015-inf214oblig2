Detective Lloyd
{{Infobox Film
| name           = Detective Lloyd
| image_size     = 
| image	=	Detective Lloyd FilmPoster.jpeg
| caption        =  Ray Taylor
| producer       = 
| writer         = 
| narrator       = 
| starring       = Jack Lloyd,  Muriel Angelus 
| music          = 
| cinematography = 
| editing        = 
| distributor    = Universal Pictures   General Films
| released       = 1932
| runtime        = 12 chapters
| country        = United Kingdom United States English
| budget         = 
| preceded_by    = 
| followed_by    = 
}} Universal Serial movie serial. American company Universal and British company Britain with British and Commonwealth actors.

It is also known by the titles Lloyd of the C.I.D. and In the Hands of the Hinfu.  Material from this serial was edited into a movie called The Green Spot Mystery (1932). It is on the British Film Institutes BFI 75 Most Wanted list of lost films. 

==Chapter titles==
# The Green Spot Murder
# The Panther Strikes
# The Trap Springs
# Tracked by Wireless
# The Death Ray
# The Poison Dart
# The Race with Death
# The Panthers lair
# Imprisoned in the North Tower
# The Panthers Cunning
# The Panther at Bay
# Heroes of the Law
 Source:  {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | year = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 205
 | chapter = Filmography
 }} 

==See also==
* List of American films of 1932
* List of film serials by year
* List of film serials by studio

==References==
 

==External links==
* , with extensive notes
* 

 
{{succession box  Universal Serial Serial 
| before=The Spell of the Circus (1931)
| years=Detective Lloyd (1932)
| after=The Airmail Mystery (1932)}}
 

 

 
 
 
 
 
 
 
 
 
 
 

 