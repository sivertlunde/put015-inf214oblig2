Rajamalli
{{Infobox film
| name           = Rajamalli
| image          =
| caption        =
| director       = RS Prabhu
| producer       = RS Prabhu
| writer         = Muthukulam Raghavan Pillai
| screenplay     = Muthukulam Raghavan Pillai Sharada Sukumari Kottayam Santha
| music          = BA Chidambaranath
| cinematography = A Venkat
| editing        = G Venkittaraman
| studio         = Janatha Productions
| distributor    = Janatha Productions
| released       =  
| country        = India Malayalam
}}
 1965 Cinema Indian Malayalam Malayalam film,  directed and produced by RS Prabhu. The film stars Prem Nazir, Sharada (actress)|Sharada, Sukumari and Kottayam Santha in lead roles. The film had musical score by BA Chidambaranath.   

==Cast==
 
*Prem Nazir Sharada
*Sukumari
*Kottayam Santha
*Subhadra
*T. S. Muthaiah
*Malathi
*Abbas GK Pillai
*JAR Anand
*Paravoor Bharathan
 

==Soundtrack==
The music was composed by BA Chidambaranath and lyrics was written by P. Bhaskaran.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Jayakaali || K. J. Yesudas, S Janaki || P. Bhaskaran || 
|-
| 2 || Kaatte Vaa || P. Leela || P. Bhaskaran || 
|-
| 3 || Karppoora Thenmaavil || S Janaki || P. Bhaskaran || 
|-
| 4 || Kunninmele Neeyenikku || S Janaki || P. Bhaskaran || 
|-
| 5 || Kuppivala || A. M. Rajah || P. Bhaskaran || 
|-
| 6 || Neelamukilukal || S Janaki || P. Bhaskaran || 
|}

==References==
 

==External links==
*  

 
 
 


 