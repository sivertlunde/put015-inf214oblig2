The Boost
{{Infobox film
| name           = The Boost
| image          = Boostposter1988.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Harold Becker
| producer       = Daniel H. Blatt
| writer         = Ben Stein Darryl Ponicsan
| narrator       =
| starring       = James Woods Sean Young John Kapelos Steven Hill Kelle Kerr
| music          = Stanley Myers
| cinematography = Howard Atherton
| editing        = Maury Winetrobe
| distributor    = Hemdale Film Corporation
| released       =  
| runtime        = 95 minutes
| country        = United States English
| budget         = $8 million
| gross          = $784,990
| website        =
}}

The Boost is a 1988 drama film directed by Harold Becker.    It stars James Woods, Sean Young, John Kapelos, Steven Hill, June Chandler and Amanda Blake.

==Plot==
Lenny Brown is a real-estate hustler looking to strike it rich. He is married to Linda, a paralegal and amateur dancer. The two are poor in money but rich in love. Linda vows to stick with her husband until she "falls off the earth". He moves to California and goes to work for a prosperous businessman named Max Sherman, selling lucrative investments in tax shelters.

Everything is suddenly first-class for Lenny and his beautiful wife, Linda. But when the tax laws abruptly change, they find themselves $700,000 in debt.

They become increasingly desperate, made worse by the fact that a friend, Joel Miller, turns them on to cocaine for "a boost." Lenny and Linda both become addicted. They lose their home, car and jobs. Linda becomes pregnant, but falls and suffers a miscarriage after using cocaine.

Lennys life unravels little by little as the drug habit gets the better of him. He gets straight temporarily for one last great business opportunity, but he cant pull it off. This culminates in Lenny severely beating Linda and putting her in the hospital.

As the end credits roll, we see Lenny still using cocaine in his filthy apartment, reduced to a babbling shell of himself.

==Cast==
* James Woods as Lenny Brown
* Sean Young as Linda Brown
* John Kapelos as Joel Miller
* Steven Hill as Max Sherman
* Kelle Kerr as Rochelle
* John Rothman as Ned
* Amanda Blake as Barbara

==Reaction== The Shining, its hard to distinguish the before Lenny from the after." 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 


 