Gulliver Mickey
 
{{infobox Hollywood cartoon cartoon name= Gulliver Mickey series = Mickey Mouse image = image_size = alt = caption = director =Burt Gillett producer = Walt Disney voice actor = Walt Disney musician = Frank Churchill studio = Walt Disney Productions distributor = United Artists release date = May 19, 1934 color process Black & White country = USA language = English
}}
Gulliver Mickey is a black and white Mickey Mouse short, produced by Walt Disney and released by United Artists in 1934.

==Plot==
Mickey is first seen reading Gullivers Travels while the mice orphan children are pretending to be sailors. After ruining their game Mickey tries to make it up to them by retelling the Liliput sequences of Gullivers Travels, pretending it was a real event that happened to him by portraying the role of Gulliver. The story ends with Mickey saving the town from a giant spider (Pete (Disney)|Pete). However after telling the story, one of the children dangles a fake spider attached to a fishing rod, which scares Mickey out of his wits.

==Censorship==
While the people of Liliput are searching Mickeys belongings one of the numerous gags featured in this sequence is two men searching a pen (one at the tip of the pen one at the other end). One of them accidentally squirts ink onto the others face, which causes him to appear in blackface.

==Legacy==
This short would later be adapted as part of the Timeless River world in Kingdom Hearts II as a mission where Characters of Kingdom Hearts#Sora|Sora, Donald Duck and Goofy protect the town from the Universe of Kingdom Hearts#Heartless and Nobodies|Heartless, led by one attacking from an airplane.

==External links==
*  

 
 
 
 
 
 
 
 
 
 

 