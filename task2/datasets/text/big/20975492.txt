Gulama
{{Infobox film
| name           = Gulama
| image          = Gulama.JPG
| alt            =  
| caption        = movie poster
| director       = Tushar Ranganath
| producer       = Ramu
| writer         = Tushar Ranganath
| screenplay     = Tushar Ranganath
| starring       = Prajwal Devraj Bianca Desai sonu
| music          = Gurukiran
| cinematography = KM Vishnuvardhan Shekhar Chandra
| editing        = Deepu S Kumar
| studio         = 
| distributor    = Ramu Enterprises
| released       =  
| runtime        = 
| country        = India
| language       = Kannada
| budget         = 
| gross          = 
}}

Gulama (  directed by Tushar Ranganath and produced by Ramu. The film stars Prajwal Devraj, and Bianca Desai in the lead roles. Music was composed by Gurukiran. The film released statewide on 1 January 2009. 

Gulama means bondsman in Kannada. In the film the hero Prajwal Devaraj is slave for love or slave for loving girl is the crux of the film. 

==Cast==
 
*Prajwal Devraj as Anil Sonu as Divya
* Bianca Desai as Priyanka
*Rangayana Raghu

==Synopsis==
Theme of the film is based on triangle-love story. The story of the film revolves around two girls and a boy, where the boy Anil has a love with Priyanca and the girl Divya loves the same guy Anil

==Soundtrack==
{{Infobox Album  		
| Name        = Gulama	
| Type        = film
| Artist      = Gurukiran	 125px
| Cover size  = 150
| Caption     = Official CD Box Cover	
| Released    =  
| Recorded    = 	
| Genre       = Film score		
| Length      = 		 Kannada	
| Label       = Anand Audio
| Producer    = Ramu
| Reviews     = 	
| Compiler    = 	
| Misc        = 	
}}

The film has five songs composed by Gurukiran, with the lyrics primarily penned by Tushar Ranganath and Kaviraj.

{{tracklist
| collapsed       = 
| headline        = 
| extra_column    = Singers
| total_length    = 
| all_writing     = 
| all_lyrics      = 
| all_music       =
| writing_credits = 
| lyrics_credits  = yes
| music_credits   = 
| title1          = Shakala Shakala Umma
| note1           = 
| writer1         = 
| lyrics1         = Tushar Ranganath
| music1          = 
| extra1          = Gurukiran, Sunitha
| length1         = 
| title2          = Snehitane Snehitane
| note2           = 
| writer2         = 
| lyrics2         = Tushar Ranganath
| music2          = 
| extra2          = Srinivas, K. S. Chithra
| length2         = 
| title3          = Ishte Ishtu Iruva Hrudaya
| note3           = 
| writer3         = 
| lyrics3         = Tushar Ranganath
| music3          = 
| extra3          = Madhu Balakrishnan
| length3         = 
	
| title4          = Taare Annutaare
| note4           = 
| writer4         = 
| lyrics4         = Kaviraj
| music4          = 
| extra4          = Sunidhi Chauhan
| length4         = 
	
| title5          = Chandalana Kaili
| note5           = 
| writer5         = 
| lyrics5         = Tushar Ranganath
| music5          = 
| extra5          = Gururaj Hoskote, Murali Mohan, Gurukiran
| length5         = 
}}

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 