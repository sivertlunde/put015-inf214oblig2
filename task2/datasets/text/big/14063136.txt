Dog Eat Dog (2001 film)
{{Infobox film
| name           = Dog Eat Dog
| image          = 
| image_size     = 
| alt            = 
| caption        = 
| director       = Moody Shoaibi Paul Webster
| writer         = Moody Shoaibi Mark Tonderai John Thomson
| music          = Mark Hinton Stewart
| cinematography = John Daly
| editing        = Luke Dunkley
| studio         = FilmFour Senator Film Produktion Shona Productions Tiger Aspect Productions
| distributor    = FilmFour
| released       = Warsaw International Film Festival   United Kingdom  
| runtime        = 93 minutes
| country        = United Kingdom Germany
| language       = English
| budget         = 
| gross          = 
}}
Dog Eat Dog is a 2001 British film, directed by Moody Shoaibi and written by Moody Shoaibi and Mark Tonderai.

==Plot== breaking and entering, dognapping—all with a spectacular lack of success. And into the bargain, theyve fallen foul of drugs baron, Jesus (Gary Kemp), whose slogan is "Youve gotta have faith in Jesus".

==Cast==
*Mark Tonderai as Rooster
*Nathan Constance as Jess
*David Oyelowo as CJ 
*Melanie Blatt as Jany, the ex-girlfriend
*Crunski as Chang 	
*Alan Davies as Phil 
*Gary Kemp as Jesus
*Steve Toussaint as Darcy 
*Ricky Gervais as Bouncer
*Rebecca Hazlewood as Mina
*Stewart Wright as Eastwood
*Dilys Laye as Edith Scarman
*Daniel Kitson as Bus Driver

==External links==
* 

 
 
 
 
 
 

 