Asathal
{{Infobox film
| name           = Asathal
| image          =
| caption        =
| director       = P.Vasu
| writer         = P. Vasu Swathi Vadivelu Ramesh Khanna
| producer       = S. Rajaram
| cinematography = B. Kannan
| music          = Bharathwaj 
| editing        = P. Mohanraj
| studio         = Mala Cine Creations
| released       = May 18,  2001
| runtime        = 138 minutes Tamil
| country        = India
}}

Asathal is a 2001 Tamil comedy film written and directed by P. Vasu.  The film featured Sathyaraj and Ramya Krishnan in the leading roles.  Produced by Mala Cine Creations and featuring music composed by Bharathwaj, the film was released on May 19, 2001.

==Cast==
* Sathyaraj as Vetri
* Ramya Krishnan as Gowri Swathi as Catherine
* Vadivelu as Venugopal
* Ramesh Khanna as Victor
* Ajay Rathnam as Jayaraj

==Production==
The film was produced by S. Rajaram, a theatre owner and film distributor under his production house Mala Cine Combines.  He signed on P. Vasu to write and direct the comedy film, with the director collaborating with Sathyaraj again after several previous successful ventures.  Scenes which showed the characters in a house were filmed in a bungalow at Neelankarai, Chennai.  Sathyaraj worked on Asathal alongside two other ventures Kunguma Pottu Gounder and the later-shelved Mr. Narathar. 

==Release==
Malathi Rangarajan of The Hindu gave the film a mixed review, noting that "Certain scenes are too contrived - the aim is humour but they only tend to irritate", while adding "the screenplay is fast-paced, the dialogue is crisp, it is the story that lags behind".   Reviewer Balaji Balasubramaniam also gave the film a negative review, remarking that "Vasu has lost his touch and run out of ideas in comedies".   Another critic noted "A full-length comedy, the narration is fast-paced, the script crisp, and it manages to hold ones attention for the most part." 
 Sajid Khans 2007 Hindi comedy film Heyy Babyy which featured Akshay Kumar and Vidya Balan. 

==References==
 

 
 
 
 


 