Oru CBI Diary Kurippu
{{Infobox film
| name           = Oru CBI Diary Kurippu
| image          = cbidairyfilm.png
| image_size     = 
| caption        = Poster designed by Gayathri Ashokan
| director       = K. Madhu
| producer       = M. Mani
| writer         = S. N. Swamy
| narrator       =  Mukesh Sukumaran
| cinematography = Vipindas Shyam
| editing        = V. P. Krishnan 
| released       =  
| runtime        = 137 minutes
| studio = Sunitha Productions 
| distributor = Aroma Movies 
| country        = India
| language       = Malayalam
| budget         =  
| gross          =  
}} 1988 Malayalam Malayalam mystery film directed by K. Madhu, written by S. N. Swamy, and starring Mammootty, Suresh Gopi, Jagathy Sreekumar, Mukesh (actor)|Mukesh, and Sukumaran.
 series of CBI officer Sethurama Iyer. It was produced by M. Mani under the banner Sunitha Productions. Sequels to the film are Jagratha (1989), Sethurama Iyer CBI (2004), and Nerariyan CBI (2005). A fifth sequel Veendum CBI in the franchise has been announced and is now in the pre-production stage. 
 state of Kerala to prove a murder case.
 National Investigation Agency.   
He died in Kochi on 21 June 2012 at the age of 62, due to lung complications.  

The movie was a huge box office success. The background score of the film became quite popular, and years after the release of the first two sequels, became popular as mobile phone ringtones. This was one of the reasons that led the filmmakers to plan a third sequel after 15 years. All the sequels use the same background score, composed by music director Shyam (composer)|Shyam.
 Rajasekhar and in Hindi as Police Public with Raaj Kumar.

== Plot ==
The movie opens with DySP Prabhakara Varma (Captain Raju) observing the local police force during a training drill.  Another officer approaches him and informs him that he is needed at a recent crime scene, and they depart immediately.  Varma arrives at the home of Ouseppachan (Janardhanan (actor)|Janardhanan), a local businessman, and sees the body of Ouseppachans daughter-in-law, Omana (Lizy (actress)|Lizy) on the grounds behind the house, in an apparent suicide.  He is then introduced to Omanas husband Sunny, her father-in-law Ouseppachan, Ouseppachans son-in-law Johny (Vijayaraghavan (actor)|Vijayaraghavan), Ouseppachans best friend Narayan, and several of the household servants and neighbors (Adoor Bhavani, T.P.Madhavan). The initial report conducted by the first responders concluded that Omana had committed suicide by flinging herself from the roof of the two-story house onto the stone pavement below.  Varma questions the members of the household as well as neighbors and servants about Omanas relationship with her husband Sunny (Sreenath).  Sunny admits to arguing with Omana the night she died and throwing her onto their bed before leaving to drink, but swears he never hit her.  He also tells Varma that Omana had attempted suicide before.  Ouseppachan claims he came home from a business trip that night, dropped off by his driver, Vasu (Johny), only to find himself locked out of the house.  He decided to go around the back to where the cooks quarters were located when he stumbled upon Omanas body.  Dissatisfied with inconsistent testimonies from several suspects, Varma immediately suspects foul play and is determined to find the truth.  The next morning, the Regional Medical Officer (Kollam Thulasi) concludes that Omana died from head trauma at 8pm the previous night.

Omanas cousin Chacko (Mukesh (actor)|Mukesh), a police officer working under Varma, finds this autopsy report suspicious and tells Varma and Officer Alex (K.P.A.C. Sunny) why he has doubts to its accuracy.  He informs them that it rained severely until 9:30pm the night Omana allegedly committed suicide, and if she had died at 8pm, her corpse would have been wet, and the blood from her injury would have been mixed in with the rain water.  Alex, who is taking bribes from Ouseppachan, warns Ouseppachan to do what he can to get Varma and Chacko off the case.

Sensing that Varma will cause him trouble, Ouseppachan implores his best friend, Narayanan (Prathapachandran), a local politician, to call his politician friends (including the Minister) to have Varma transferred off the case and onto Rural.  They also managed to transfer Omanas cousin, Chacko to Traffic.  A new Dy SP, Devadas (Sukumaran), takes over the case.  Devadas, who is also being bribed by Ouseppachan, intimidates Omanas grieving family and any witnesses that may implicate Ouseppachan or Sunny.  He quickly concludes the investigation stating that Omana committed suicide.
 CBI to re-open the case and find the truth.  The Director of the CBI assigns CBI DySP Sethuraman Iyer (Mammootty) to head the investigation. He is assisted by CBI Circle-Inspector Harry (Suresh Gopi) and CBI Sub-Inspector Vikram (Jagathy Sreekumar).  

Iyer and his team find evidence of political interference in Omanas murder investigation, citing an increased amount of phone calls to Ministers and Party Leaders from Narayans phone number, and thousands of rupees worth of cash withdrawals from Ouseppachans bank that were never reported in the books.  Iyer meets with Varma and Chacko who help him with his investigation by informing him of inconsistent testimony from suspects and the inconsistencies with the Regional Medical Examiners autopsy report.  Later that week, Iyer and his team bring two dummies the same height and weight as Omana to Ouseppachans house.  They take the dummies to roof and drop one straight down as if it jumped, and threw the other off the roof as if someone had thrown the body.  They find that Omanas body landed approximately the same place as the dummy that was thrown, and concluded that she could not have jumped that far off the roof on her own. Next, they decide to track down Ouseppachans driver, Vasu, who has been missing since the morning after Omanas death.  Harry finds out that Vasu had given a local banker some jewelry as collateral for a loan.  Among the jewelry was a bracelet belonging to Omana, which immediately put all suspicion on Vasu.  They finally find him and take him into custody, where he confesses that shortly after he got home after dropping off Ouseppachan the night of Omanas death, Narayan came to his house and told him that Sunny had accidentally killed Omana during a domestic dispute.  Vasu then said Narayan and the others implored him to carry Omanas body from her bed to the roof and fling her off.  It was during a moment alone with Omanas dead body that Vasu had noticed her gold bracelet and decided to steal it, before throwing her corpse of the roof.  

Despite Vasus testimony implicating Sunny as the killer, Iyer is not convinced.  Sunny had left the house at 8pm after hitting Omana and that would mean that she died at 8pm.  However, Iyer examines photographs of the crime scene and finds that Omana still bled when she was dropped off the roof some 2 hours later, and since dead bodies do not retain heat thus causing the blood to clot, she could not have died at 8pm.  Upon scrutinizing the photographs further, Iyer notices a small blood stain on Omanas saree below the waist.  Since the autopsy report stated that she sustained no injuries below the waist, Iyer is convinced that the blood was left behind by the real killer.

After testing the blood they find that it is O-negative, a rare blood type.  Remembering that the town had imposed mandatory blood tests on all its citizens recently by the local authorities to combat malaria, Iyer goes to the Malaria Research Center and finds that the blood type was so rare that only two people in the entire town had it.

In the final scene, Iyer arrives at Ouseppachans house with a large police backup unit and places Ouseppachan under arrest for the murder of Omana.  Shocked, Ouseppachan vehemently denies killing her, but Iyer would not relent.  Seeing his father being arrested for a crime he committed, a guilt-ridden Sunny confesses to Iyer that he killed Omana accidentally by hitting her too hard when they fought.  Iyer tells Sunny that Omana did not die when he hit her, that she only fainted.  Iyer then goes on to explain the blood on Omanas saree, which did not match her own, was left behind by the real killer.  He informs them that only two people in the entire town were O-negative and one of those two people is Johny, Ouseppachans son-law.  Confronted with evidence for his involvement, Johny confesses that he had come to the house the night Omana had died.  Finding her passed out on the bed, he attempted to rape her, but when she woke up and started screaming, he suffocated her to death.  In the process he accidentally cut himself and left his blood on her saree.

Thomachan and Annie thank Iyer, Harry and Vikram for their hard work and dedication and bringing Omanas killer to justice.  The movie ends with a reference to Sukumara Kurup, a real life notorious fugitive wanted in India for murder, who to this day is still on the run.

== Sethurama Iyer ==
 

== Trivia== Saikumar was cast as the son of Devadas; he was required to mimic Sukumarans style of performance.
* The movie was one of the first of its kind to concentrate on the central investigative story only, without the songs and dances generally associated with popular Indian commercial films.
*CBI Diary Kurippu was one of the top grosser in 1988. It ran more than 200 days (175 days in TVM) in Kerala.

== Cast ==
*Mammootty as Sethurama Iyer, DySP, CBI.
*Suresh Gopi as Harry, Sub Inspector, CBI.
*Jagathy Sreekumar as Vikram, Sub Inspector, CBI. Mukesh as Chacko, Police Constable.
*Sukumaran as Devadas, DySP, Kerala Police. Lizy as Omana, the murdered woman. Janardhanan as Ouseppachan, a businessman. Urvasi as Annie, sister of Omana.
*Bahadoor as Thomachan, father of Omana.
*Prathapachandran as Narayanan, friend of Ouseppachan.
*K. P. A. C. Sunny as Alex, CI of Police.
*Kollam Thulasi as Police Surgeon.
*Jagannatha Varma as SP, CBI.
*Captain Raju as DySP Prabhakara Varma, Local Police. Vijayaraghavan as Johnny, son in law of Ouseppachan.
*Sreenath as Sunny, husband of Omana.
*Adoor Bhavani as servant.
*C. I. Paul as Bhargavan, Minister.
*T. P. Madhavan
*Kundara Johnny as Driver Vasu.

== References ==
 

==External links==
*  

 

 
 
 
 
 