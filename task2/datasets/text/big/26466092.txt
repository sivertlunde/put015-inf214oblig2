Conan the Barbarian (2011 film)
 
{{Infobox film
| name           = Conan the Barbarian
| image          = Conan the Barbarian (2011 film).jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Marcus Nispel
| producer       = Fredrik Malmberg Avi Lerner Boaz Davidson Joe Gatta George Furla John Baldecchi Les Weldon Thomas Dean Donnelly Joshua Oppenheimer Sean Hood
| based on       =  
| narrator       = Morgan Freeman  Rachel Nichols Stephen Lang Rose McGowan Saïd Taghmaoui Leo Howard Bob Sapp Ron Perlman
| music          = Tyler Bates
| cinematography = Thomas Kloss
| editing        = Ken Blackwell
| studio         = Nu Image Films Millennium Films Paradox Entertainment Lionsgate
| released       =    
| runtime        = 113 minutes   
| country        = United States
| language       = English
| budget         = $90 million    
| gross          = $48,795,021 
}} Rachel Nichols, Rose McGowan, Stephen Lang, Ron Perlman, and Bob Sapp with Marcus Nispel directing.

The film had spent seven years in development at  , Belgium, Iceland, and the Philippines   prior to the North American release on August 19. The film was a box office bomb and received largely negative reviews.   

==Plot==
Conan is the son of Corin, chief of a barbarian tribe. The youth is a skilled but violent warrior, who his father believes is not ready to wield his own sword. One day, their village is attacked by the forces of Khalar Zym, a warlord who wishes to reunite the pieces of the Mask of Acheron in order to revive his dead wife and conquer Hyboria. Thousands of years ago, the Mask, crafted by a group of sorcerers and used to subjugate the world, was broken into many pieces, which were scattered among the barbarian tribes. After locating Corins piece of the mask, and murdering the entire village, Zym leaves. Conan is the only survivor, and swears revenge.

Years later, Conan has become a pirate, but still seeks revenge. He encounters a slave colony and frees it, killing all of the slave handlers in the process. In the city of Messantia, he encounters Ela-Shan, a thief being chased by a man whom Conan recognizes as Lucius, one of Zyms soldiers from years before. He allows himself to be captured alongside Ela-Shan. Conan escapes imprisonment, kills several of the guards, and confronts Lucius, forcing him to reveal that Zym seeks a girl, the pure-blood descendant of the sorcerers of Acheron; sacrificing the descendant and using blood from the body of the girl  will unleash the masks power. Conan helps the rest of the prisoners to escape, and, in gratitude, Ela-Shan tells Conan that, if he ever needs him, Conan will find him at the City of Thieves, Argalon. Lucius is then killed by the prisoners.

Zym and his daughter, the sorceress Marique attack a monastery where they hope to find the pure-blood descendant. Sensing something is wrong, Fassir, an elderly monk, tells one of his students, Tamara, to run away and return to her birthplace. When Fassir refuses to reveal his knowledge of the descendant, Zym kills him. Marique also slays several of the priestesses. Tamaras carriage is chased by Zyms men, but Conan rescues her, kills three of her pursuers, and also captures one of Zyms men, Remo. After forcing him to reveal Tamaras importance as the pure-blood, Conan catapults Remo into Zyms nearby camp, killing him.

Zym and Marique confront Conan, who pretends to be interested in exchanging Tamara for gold. Conan attacks Zym, but Marique assists her father by invoking soldiers made of sand, and then poisons Conan with a poison-laced boomerang sword. Tamara rescues him and they return to Conans ship, stationed nearby, where his friend Artus helps Conan recover. The boat is attacked by Zyms men, but although they kill several of Conans men, they are defeated. Conan orders Artus to return to Messantia with Tamara and departs to confront Zym in his kingdom. Artus tells Tamara that Conan left a map behind and she follows him, meeting with him in a cave, where they strip naked and copulate. The next day, as she is returning to the boat, Zyms men and daughter capture her.

Conan learns of Tamaras capture and departs to Argalon, where he asks Ela-Shan to help him break into Zyms castle unnoticed. Zym prepares to drain Tamaras blood, mending the mask. He then plans to use the girls body as a vessel for his wife. After confronting an octopus-like monster that guards the dungeons and killing its four handlers, Conan infiltrates Zyms followers, kills a guard and steals his robe, and watches as Zym puts on the empowered mask. Conan releases Tamara, and she escapes as he battles Zym with the castle falling around them. Marique attacks Tamara, but Conan hears Tamaras scream and defeats Marique, cutting off her hand. Tamara kicks her into a pit, where her body pierced by a large spike. Zym comes and, finding his daughters body impaled by the spike, he swears revenge upon Conan.

Conan and Tamara become trapped on an unstable bridge as Zym attacks them. He uses the masks power to call forth the spirit of his deceased wife, Maliva, a powerful sorceress who was executed by the monks from Tamaras monastery for attempting to unleash occult forces to destroy Hyborea, and Malivas spirit begins to possess Tamaras body. She begs Conan to let her fall, but he refuses, and instead stabs the bridge before jumping to safety with Tamara. The bridge collapses, taking Zym along. The power-hungry ruler falls to the lava below the immense precipice screaming the name of his wife, implying his demise.

Conan and Tamara escape and he returns her to her birthplace, telling her that theyll meet again. He then returns to Corins village and tells the memory of his father that he has avenged his death and recovered the sword Marique stole from him, restoring his honor.

==Cast== Conan
** Leo Howard as young Conan Rachel Nichols Acheronian necromancers.
* Stephen Lang as Khalar Zym, a ruthless empire-building warlord. Zym seeks Acherons powers over life and death to resurrect his wife Maliva who was burned for her evil. The character was originally going to be called Khalar Singh 
* Rose McGowan as Marique, Zyms daughter and a powerful witch. She is presumed to have inherited her powers from her mother Maliva.
** Ivana Staneva as young Marique
* Saïd Taghmaoui as Ela-Shan, a thief who pays his debt to Conan by helping him.
* Bob Sapp as Ukafa, a leader of Kushite Tribemen from the savannahs of Kush and Zyms lieutenant.
* Ron Perlman as Corin, a blacksmith, a leader of the Cimmerians, and Conans father. Steven ODonnell as Lucius, the leader of Zyms Legion of Aquilonian Mercenaries. Lucius is disfigured by Conan during the ransacking of the Cimmerian village. He became warden of a prison soon after.
* Diana Lubenova as Cheren, a blind archer who leads a similar band of blind archers in Zyms mercenary army.
* Nonso Anozie as Artus, a Zamoran pirate and friend of Conan.
* Milton Welsh as Remo, a mysterious warrior of dark magic.
* Raad Rawi as Fassir, an elder monk and leader of the monastery charged with the care of Tamara.
* Anton Trendafilov as Xaltotun
* Aysun Aptulova as Sacrificial victim
* Daniel Rashev as Acolyte priest
* Gisella Marengo as Maliva
* Morgan Freeman as the Narrator

==Production==

===Development===
There had been talk in the late 1990s of a second Conan sequel following  , this project came to an end. 
 Larry and CEO Fredrik Malmberg told Variety (magazine)|Variety "we have great respect for Warner Bros., but after seven years, we came to the point where we needed to see progress to production." Paradox were auctioning the rights after and various groups took interest in producing, including New Line Cinema, Hollywood Gang, and Millennium Films. 

Due to development-time frustrations felt when the rights were with Warner, Malmberg made deal terms where he was asking for $1 million for a one-year option, with another $1 million for each years renewal. In August 2007, it was announced that Millennium had acquired the right to the project in an unrevealed seven-figure deal, with Malmberg and Millenniums Avi Lerner, Boaz Davidson, Joe Gatta, and George Furla set to produce. The deal was brokered by Gatta, who originally made the deal between Paradox and Warner in 2002. Production was aimed for a Spring 2006 start, with intention of having stories more faithful to the Robert E. Howard creation. 
 Red Sonja with Rose McGowan as the lead, Robert Rodriguez had mentioned in July 2008 he had been in discussions to produce Conan also.  Dirk Blackman and Howard McCain were announced in August to have been hired for a re-write of the script, with the intention of returning to the original source material and in the desire of making an Motion Picture Association of America film rating system|R-rated film. 
 Paramount and Beverly Hills Cop   first, no matter what. Avi shouldnt be telling you or anyone else in the press what Im doing."   However, Gatta revealed in May 2009 that after six months of discussions on developing the film, Ratner was off the project due his busy schedule. Regardless, Gatta was hopeful of still meeting the intention of Millennium to start filming on August 24 in Bulgaria.  June 2009 revealed Marcus Nispel would take the reins as director to the film.    Sean Hood was announced in February 2010 to be rewriting the script once more for the producers. 

Early in pre-production, Conan was a temporary title for the film, until it was changed to Conan 3D. Finally, early on December 2010, the title was definitely changed to Conan the Barbarian, as was titled the 1982 film. 

===Casting=== The Bourne David Leitch, and the martial arts stunt coordinator for The Matrix Reloaded and The Matrix Revolutions, Chad Stahelski. 

Leo Howard was cast as the younger Conan in the film, as the first 15 minutes focused on Conans youth. 

The casting call for Conans father, Corin, reveals the character to be "powerfully built, intelligent, graceful, master swordsman, skilled blacksmith, de facto leader of Cimmerians and Conans father. He resolves to answer the terrible request of his dying wife and cuts Conan out of her so she can see him. He then shoulders the burden of raising Conan, which proves to be daunting given the boys savage nature. Corin teaches his son the meaning of the sword: a hot blade must be cooled and tempered. When Khalar finally corners him and tortures him to death, he shows no regret nor pain, hiding his concern for his sons safety from the eyes of the enemy."    Mickey Rourke first entered negotiations. Originally talks had happened before but after a period of no talk, offers were returned to Rourke in February, 2010.  Rourke had however left the project for a second time, in apparent favor of the Immortals (2011 film)|Immortals film. Ron Perlman took on the character in March, 2010.  

Bob Sapp portrays Ukafa, "a leader of Kushite Tribemen from the savannahs of Kush. Ukafa is Khalar Zyms second in command, jealous that Zyms daughter, Marique, will one day be warlord. He obeys his leader but plots the overthrow of his daughter. He is a mighty warrior and unbeatable in battle—until he meets Conan." 
 Rachel Nichols joined the cast as Tamara, described as "the Queens servant, bodyguard and best friend. She and many other female bodyguards to the queen have been in hiding most of their lives because of the curse of Acheron, which would take the queen’s life to bring almost immortal power to its king. When Khalar Zym, a powerful warlord with ambitions to become the king of Acheron, storms the monastery and captures all of the novitiates, she is separated from Ilira, the one she must protect. With all of her strength and will, Tamara is determined to find and rescue her. She finds herself in league with Conan because of a mutual need to find Khalar Zym. She is not in the least intimidated by Conan’s size or grim demeanor and their alliance eventually blossoms into something that surprises them both." 

Stephen Lang plays Khalar Zym, described to be "commanding in size and manner, a warlord and formidable warrior, brilliant, cruel, weathered and tanned by the many campaigns he has waged and won. He is driven in his quest to find the Queen of Acheron and has been building an empire to do so." 

Rose McGowan also stars as "an evil half-human/half-witch", as announced by Variety (magazine)|Variety in March, 2010.    She plays Marique, the daughter of Khalar Zym (Stephen Lang). Although the part was originally written as a male character called Fariq, McGowan impressed the producers with her take of the role, so her character was rewritten to be female. McGowan attempted to inject an Electra complex for the character, noting: “Initially, that obsession with trying to seduce her father was not scripted; it was something that the writer and I talked about quite a bit. But, unsurprisingly, the studio was freaked out by it. A lot of the dialogue that we’d come up with to support that, and really give it a deviant, complicated feel, had to be taken out. So I just figured, OK, then, I’ll just act it and make it as uncomfortable as possible without any dialogue!”    When asked about preparation to play a witch, Mcgowan stated that her costume had significance in her preparation and that makeup for her character took 6 hours.

Dolph Lundgren had spoken to the producers in November, 2009 about an unspecified role.  This never came to fruition.

===Filming===
Principal photography was first hoped to be started in Spring 2008. Nothing was set until Ratner came on board. Filming had a set date for August 24, 2008, in Bulgaria. Ratner, however, departed in May that year, and the start-date for filming was pushed back, with South Africa being revealed as another filming destination.  Filming finally began in Bulgaria on March 15, 2010,  wrapping on June 15. 

The Bulgarian shooting locations were Nu Boyana Film Studios, Bolata, Pobiti Kamani, Bistritsa, Sofia, Zlatnite Mostove, Pernik, and Vitosha.  

The film had a 3D conversion in post production. 

==Release==
Conan the Barbarian was first released on August 17, 2011 in France, Belgium, Iceland, and the Philippines. It was released in Australia, Italy, and Israel on August 18; in the United States, Canada, and Spain on August 19,  in Switzerland on August 21,  in the United Kingdom on August 26,  among others.

===Box office===
In its first weekend, Conan made $10,021,215 in 3,015 theaters and opened at #4 in the United States domestic box office. By the end of its run, the film had grossed $21,295,021 domestically and $27,500,000 overseas for a worldwide total of $48,795,021. Based on its $90 million budget, the film was a box office bomb. 

===Critical response===
As of August 30, 2011, the film has received generally negative reviews, attaining a 24% aggregate approval rating based on 137 reviews on the review-aggregate website Rotten Tomatoes.  The website provided a consensus that "while its relentless, gory violence is more faithful to the Robert E. Howard books, Conan the Barbarian forsakes three-dimensional characters, dialogue, and acting in favor of unnecessary 3D effects."  The film also received a score of 36 out of 100 from review aggregate Metacritic, which indicates "generally unfavorable" reviews based on 29 reviews.    CinemaScore polls reported that the average grade moviegoers gave the film was a "B−" on an A+ to F scale.  Internet review show Half in the Bag criticized Conan the Barbarian for having an overly simplistic screenplay and pointless action scenes. 

However not all reviews were negative. Betsy Sharkey of the Los Angeles Times wrote that "it is with a certain amount of guilt that I say its kind of a wicked blast to watch."    Scott Weinberg of Twitchfilm.com stated, "Some action scenes are tighter and more cohesive than others, but theres little denying that Nispels Conan moves like a shot, tosses a lot of hardcore lunacy at the screen, and shows a decent amount of respect for basic matinee action-fests." Although criticizing the stock characters and cliché-ridden script, Variety (magazine)|Variety magazine also gave a mildly positive review, stating "With all earnestness, Nispel embraces the propertys classic roots, placing this new Conan squarely within the tradition of sword-and-sorcery pics."

==Future==
In late 2011, it was mistakenly reported that Momoa was penning a   and the reboot.
 titular role. World Trade Center writer Andrea Berloff would assume scripting duties for The Legend of Conan, while Morgan continued his involvement as a producer. Fleming, Mike, Jr. (2013-10-01).    Deadline.com. Retrieved 2014-02-11. 

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 