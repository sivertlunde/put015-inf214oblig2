List of Bengali films of 2004
  Tollywood (Bengali language film industry) based in Kolkata in the year 2004. 

==Highest grossing==
 

===Top ten movies===
 
1Bandhan (2004 film)|Bandhan(Jeet,Koel Mallick)

2Barood (2004 film)|Barood(Mithun Chakraborty,Rajatava Dutta)

3Coolie(Mithun Chakraborty,Meghna Naidu)

====Critically acclaimed films====
 

==2004==

{| class="wikitable sortable" border="0"
|-
! Title
! Director
! Cast
! Genre
! Notes/Music
|-
|- Aabar Asibo Romance ||
|- Abhishek (2004 Romance ||
|- Adhikar (2004 Romance ||
|- Romance ||
|- Romance ||
|- Romance ||
|- Ami Je Romance ||
|- Anurag (film)|Anurag|| Romance ||
|- Anyay Atyachar|| Romance ||
|- Romance ||
|- Badsha The Romance ||
|- Bandhan (2004 Romance ||
|- Romance ||
|- Romance ||
|- Dadu No. Romance ||
|- Romance ||
|- Romance ||
|- Dwitio Paksha|| Romance ||
|- Ek Chilte Romance ||
|- Romance ||
|- Hathat Nirar Romance ||
|- Iti Srikanto|| Romance ||
|- Kalo Chita|| Romance ||
|- Romance ||
|- Romance ||
|- Mahulbonir Sereng|| Romance ||
|- Mamatar Bandhan|| Romance ||
|- Manasa Aamar Romance ||
|- Romance ||
|- Mayer Mon|| Romance ||
|- Mon Jake Romance ||
|- Ora Kara|| Romance ||
|- Romance ||
|- Romance ||
|- Prem Korechhi Romance ||
|- Romance ||
|- Romance ||
|- Raja Babu Raja Babu|| Romance ||
|- Ram Lakshman|| Romance ||
|- Sagar Kinare|| Romance ||
|- Romance ||
|- Samudra Sakshi|| Romance ||
|- Romance ||
|- Satabdir Galpo|| Romance ||
|- Romance ||
|- Shudhu Tumi|| Romance ||
|- Sindurer Bandhan|| Romance ||
|- Romance ||
|- Swami Chhintai|| Romance ||
|- Swapne Dekha Romance ||
|- Tin Ekke Romance ||
|- Tista Parer Romance ||
|- Romance ||
|- Romance ||
|-
|}

==References==
 
{{cite news
|url=http://www.telegraphindia.com/1041229/asp/calcutta/story_4188463.asp
|title=The Telegraph - Calcutta : Metro
|publisher=www.telegraphindia.com
|accessdate=2008-11-10
|last=
|first=
|location=Calcutta, India
|date=2004-12-29
}}

==External links==
*   at the Internet Movie Database

 
 
 
 
 

 
 
 
 
 
 