Who Do You Love? (2008 film)
{{Infobox Film
| name           = Who Do You Love?
| image          = Who Do You Love? FilmPoster.jpeg
| caption        = 
| director       = Jerry Zaks Les Alexander  Andrea Baynes  Jonathan Mitchell
| writer         = Peter Martin Wortmann  Robert Conte
| starring       = Jon Abrahams   Alessandro Nivola  David Oyelowo  Marika Dominczyk  Tendal Mann
| music          = Jeff Beal
| cinematography = David Franco Scott Richter
| studio         = 
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Who Do You Love?". It is also known by its working title of Chess. It is a biopic of the record producer Leonard Chess and was directed by Jerry Zaks and written by Peter Martin Wortmann and Robert Conte. Leonard Chess is played by Alessandro Nivola and the cast includes David Oyelowo as Muddy Waters. Location filming was in New Orleans.

==Cast==
*Jon Abrahams - Phil Chess
*Tim Bellow - Chuck Berry
*Brett Beoubay - Bo Diddleys Manager Robert Randolph - Bo Diddley
*Rus Blackwell - Missouri Sheriff
*Marcus Lyle Brown - Jess Chris Burnett - Stage Manager
*Joe Chrest - Malcolm Chisholm Heather Clark - Waitress Deloris
*Noell Coet - Frances Joshua Davis - Terry Chess
*Miko DeFoor - Little Walter
*Marika Dominczyk - Revetta Chess
*Dominique DuVernay - Core Swing Dancer	 Thomas Elliott - Radio DJ
*J. D. Evermore - Jake
*Madeline Gaudet - Concert Fan Lisa Goldstein - Sheva Chess
*Russell M. Haeuser - Concert security guard Lonnie Johnson
*Brent Henry - Police Officer
*Albert H. Bongard IV - Security Guard
*Elise Kaywood - Dancer
*Jeremy Evan Kerr - Assistant Stage Manager
*Cynthia LeBlanc - Concert Fan
*Elton LeBlanc - Security Guard
*Ian Leson - Alan Freed
*Naima Imani Lett - Chess Receptionist
*Earl Maddox - Bar Owner
*Tendal Mann - Marshall Chess
*Roy McCrerey - Hank Lakin
*Hunter McGregor - Petey		
*Crystal Morgan - Concert Fan
*Alessandro Nivola - Leonard Chess
*David Oyelowo - Muddy Waters
*Ryan Puszewski - Young Leonard Chess
*Kermit Rolison - Mr. Fitzgerald
*Benjamin Pete Rose - Young Phil Chess
*Logan Stalarow - Ethan
*Martha Twombly - Passerby
*Rodney Wiseman - Exotic Dancer

==External links==
* 
* 

 
 
 
 
 
 

 