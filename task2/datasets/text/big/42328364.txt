Ghost Son
{{Infobox film
| name           = Ghost Son
| image          = Ghost Son film poster.jpg
| image_size     =
| caption        = 
| director       = Lamberto Bava
| producer       = Pino Gargiulo, Enzo Giulioli, Marco Guidone, Terence S. Potter, Jacqueline Quella, Gianni Ricci, Paul Raleigh, Liza Essers, Enrico Coletti, Richard Green
| writer         = Lamberto Bava
| narrator       = John Hannah, Pete Postlethwaite, Coralina Cataldi-Tassoni
| music          = Simon Boswell
| cinematography = Davide Bassan
| editing        = 
| distributor    = Moviemax
| released       = 2007
| runtime        = 96 minutes
| country        = Italy Spain United Kingdom
| language       = English
| budget         =
| preceded_by    =
| followed_by    =
| website        =
}} thriller movie directed by Lamberto Bava and producer by Pino Gargiulo.

==Plot==
Stacey and Mark have recently married and are deeply in love for each other, living in Marks farm in South Africa. When Mark dies in a fatal car accident, the widow Stacey misses him and decides to stay with their orphan teenage maid, Thandi, in the farm. Later, her friend and doctor Doc finds that Stacey is pregnant. After a complicated delivery, Stacey notes that her baby in some moments seems to be possessed by the spirit of Mark, trying to kill her to bring her to spend the eternity with him.

==Cast==
*Laura Harring: Stacey John Hannah: Mark
*Pete Postlethwaite: Doc
*Coralina Cataldi-Tassoni: Beth
*Mosa Kaiser: Thandi
*Susanna Laura Ruedenberg: pediatrician
*Jake David Matthewson: Martin
*Mary Twala: Leleti
*Vanessa Cooke: gynecologist
*Jeremiah Ndlovu: Bongani

==References==
 

==External links==
* 

 


 
 
 
 
 
 
 
 
 
 
 


 