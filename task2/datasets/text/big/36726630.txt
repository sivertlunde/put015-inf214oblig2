Puthiya Theerangal
{{Infobox film
| name              = Puthiya Theerangal
| image             = Puthiya Theerangal.jpg
| caption           = Film poster
| director          = Sathyan Anthikad
| producer          = Anto Joseph
| writer            = Benny P Nayarambalam
| starring          = Nedumudi Venu Namitha Pramod Nivin Pauly
| music             = Ilaiyaraaja
| lyrics            = Kaithapram Venu
| editing           = K. Rajagopal
| studio            = Ann Mega Media
| distributor       = Ann Mega Media
| released          =  
| runtime           =126 minutes 
| country           = India
| language          = Malayalam
| budget            = 
| gross             =

}}
Puthiya Theerangal (  directed by Sathyan Anthikad, produced by Anto Joseph, and written by Benny P Nayarambalam. It stars Nedumudi Venu, Namitha Pramod and Nivin Pauly in the lead roles.  The music is composed by Ilaiyaraaja. The film was a box office failure, a first for Sathyan Anthikad after long years of consistent box office successes.

==Plot==
The film is happening by the seaside.The story is focused on the life of a young girl named Thamara, who lives on her own at the shore, after the death of her parents. Sometime later she finds an old man and she feels like having got her father back. The story dwells on the bond between the two and how that changes in the presence of the few new people who land up and start to live on the banks of the sea.

==Cast==
* Namitha Pramod as Thamara
* Nedumudi Venu as Advocate Immanuel (Original Name)/Kurian Paulose/Kumara Panickker (K.P.)
* Nivin Pauly as Mohanan Maash Innocent as Father Michael Sidharth as Appachan Mallika
*Rohit Nambi
* S.P.Sreekumar
* Molly Aunty
*Dharmajan

==Production==
Puthiya Theerangal is produced by Anto Joseph under the banner of Aan Mega Media and scripted by Benny P Nayarambalam.  Audiography was done by M. R. Rajakrishnan .

==Soundtrack==
{{Infobox album 
| Name = Puthiya Theerangal
| Artist = Ilaiyaraaja
| Type = Soundtrack
| caption = Original CD cover
| Cover = 
| Released =  
| Recorded = 2012
| Genre = Film soundtrack
| Length =  
| Language = Malayalam
| Label = Mathrubhumi Music
| Producer = Ilaiyaraaja
| Last album = Neethane En Ponvasantham • (2012)
| This album = Puthiya Theerangal • (2012)
| Next album = Yeto Vellipoyindhi Manasu • (2012)
}}
The soundtrack will be composed by Ilaiyaraaja, with lyrics penned by Kaithapram Damodaran Namboothiri|Kaithapram. The album consists of three songs. The audio rights of the film were acquired by Mathrubhumi Music. The album was launched on 9 September 2012 at Kochi. The soundtrack received generally positive reviews from music critics. "Rajagopuram" song was widely appreciated and the pick of the album.

{{tracklist
| headline = Tracklist
| extra_column = Singer(s)
| total_length = 12:50
| lyrics_credits = yes
| title1 = Rajagopuram Kaithapram
| extra1 = Vijay Yesudas, Shweta Mohan
| length1 = 4:05
| title2 = Sindoora pottu Kaithapram
| extra2 = Madhu Balakrishnan
| length2 = 4:25
| title3 = Marippeelikkatte Kaithapram
| extra3 = Madhu Balakrishnan
| length3 = 4:20
}}

==Reception==
Puthiya Theerangal received mixed reviews upon release with most critics complaining that it is a typical Sathyan Anthikad film with nothing new to offer. Sify.com gives the verdict "Disappointing" and says, "Puthiya Theerangal has nothing new about it and ends up as a meek affair. With clichéd situations and half-baked characters, the film is a lazy effort from one of Kerala’s most popular directors ever."  Paresh C Palicha of Rediff.com says Puthiya Theerangal has some good performances but in the end it leaves us with mixed feelings. He rated the film  .  Oneindia.in was somewhat impressed with the film. In a review which rates the film  , its critic comments that the film "spells the same Sathyan Anthikkad magic".  it.flopped

== Accolades ==
{| class="wikitable"
|-
! Ceremony
! Category
! Nominee
! Result
|- 2nd South Indian International Movie Awards SIIMA Award Best Lyricist Kaithapram for "Rajagopuram"
| 
|- SIIMA Award Best Female Debutant Namitha Pramod
| 
|-
|}

==References==
 

==External links==
*  

 
 
 
 
 