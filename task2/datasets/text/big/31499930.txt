Sacrifice (2011 film)
{{Infobox film
| name           = Sacrifice
| alt            = 
| image	         = Sacrifice FilmPoster.jpeg
| caption        = DVD cover
| director       = Damian Lee
| producer       = Michael Baker Lowell Conn Robert Menzies
| writer         = Damian Lee
| starring       = Cuba Gooding Jr. Christian Slater Devon Bostick Lara Daans Kim Coates
| music          = 
| cinematography = David Pelletier
| editing        = Joseph Weadick
| studio         = 
| distributor    = 
| released       =  
| runtime        = 100 minutes
| country        = United States
| language       = English
| budget         = $6,800,000 (estimated)
}} action thriller thriller film written and directed by Damian Lee, and starring Cuba Gooding Jr. and Christian Slater. It was filmed in Ottawa, Ontario. 

==Plot==
A tough undercover cop (John) inadvertently gets involved in a dangerous heroin ring when a young defector of the drug trade (Mike) leaves his five-year-old sister (Angel) in his care.  Mike is killed, Angel is kidnapped, and John sets out to save the girl.

==Cast==
* Cuba Gooding Jr. as John Hebron 
* Laura Daans as Jade 
* Christian Slater as Father Porter
* Kim Coates as Arment
* Devon Bostick as Mike
* Hannah Chantée as Noelle Hebron

==Release==
The film was released on DVD and Blu-ray in the United States on April 26, 2011. 

== References ==
 

 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 

 