At the Death House Door
{{Infobox film
| name           = At the Death House Door
| image          = Atdhd_poster.jpg
| image_size     =
| caption        = Steve James Peter Gilbert
| producer       = Peter Gilbert Steve James
| writer         =
| narrator       =
| starring       = Carroll Pickett
| music          = Leo Sidran
| cinematography = Peter Gilbert
| editing        = Steve James Aaron Wickenden
| distributor    = IFC
| released       =  
| runtime        = 98 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} Steve James Peter Gilbert, co-produced by Zak Piper and Aaron Wickenden. James and Gilbert had previously worked together on the well-received Kartemquin Films documentary Hoop Dreams, on which James was the producer and director and Gilbert served as producer and director of photography.  The film was produced by Kartemquin Films in association with the Chicago Tribune, which provided partial funding. 

==Synopsis==
Pickett presided over 95 executions in his 15 year career, including the very first by lethal injection. He kept his feelings about his work from his family, instead audiotaping an account of each one. Initially pro-execution, he became an anti-death penalty activist.

Pickett was most affected by the execution of Carlos DeLuna in 1989. He firmly believed in De Lunas innocence. In 2006, Chicago Tribune reporters Maurice Possley and Steve Mills published a detailed investigation suggesting that another man had committed the crime for which De Luna was executed,  and the film recounts the evidence brought forth in that investigation.

==References==
 

==External links==
*  
*  
* 
*  IFC.com
*  Hollywood Reporter
*  Failure Magazine

 
 

 
 
 
 


 