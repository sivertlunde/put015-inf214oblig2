Carbine's Heritage
 
 
{{Infobox film
 | name = Carbines Heritage 
 | image = 
 | caption = 
 | director = Ted Coubray
 | producer = Ted Coubray
 | writer = Ted Coubray 
 | starring = Ted Preston Queenie Grahame  
 | music = 
 | cinematography = Ted Coubray
 | editing = 
 | distributor =
 | released = 1927 
 | runtime =  English
 | budget =
 | gross = 
 }}
Carbine’s Heritage is a 1927 New Zealand film created by Ted Coubray who directed, produced and wrote the screenplay etc. It is now lost.

==Plot==
A young man finds a loose strayed colt near Hamilton which is not advertised for. So after a year he enters the colt in a show at which the owner a young lady who comes to the show with her farher claims. They come to an arrangement about the horse, which is a descendant of the famous horse Carbine. Then the horse is stolen by the pair of villains when being trained ..... The climatic end sequence was filmed at the Christmas 1926 Auckland Cup meeting.

==Cast==
* Ted Preston ... Tim Hogan 
* Queenie Grahame ... Alice Wylie
* Stuart Douglas ... Harold Wylie 
* Pat OConnor ... Tom Patten 
* Cook Brothers ... Two crooks

==References==
*New Zealand Film 1912-1996 by Helen Martin & Sam Edwards p37 (1997, Oxford University Press, Auckland) ISBN 019 558336 1

 
 
 
 
 
 
 
 
 
 


 
 