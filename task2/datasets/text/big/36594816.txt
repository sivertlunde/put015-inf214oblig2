Not Another Happy Ending
 
{{Infobox film
| name = Not Another Happy Ending
| image = Not Another Happy Ending Poster.jpg
| image_size = 250px
| border =
| alt =
| caption = UK release poster John McKay
| producer = Claire Mundell Wendy Griffin
| writer = David Solomons Gary Lewis Kate Dickie Henry Ian Cusick
| music = Lorne Balfe
| cinematography = George Geddes
| editing = Calum Ross
| studio = Synchronicity Films Ltd. British Film Company
| distributor = Kaleidoscope Home Entertainment
| released =  
| runtime = 102 minutes  
| country = United Kingdom
| language = English
| budget =
| gross =
}} John McKay, premiered at the Edinburgh International Film Festival on 30 June 2013. 

It was largely shot in Glasgows Merchant City. 

==Plot==
Tom Duvall, the editor of a struggling publishing company, discovers that his only successful author, Jane Lockhart, cannot write when happy. Due to her recent success, she is left unable to write and he is stuck. He is forced to make her incredibly unhappy. The thing is, the worse he makes her feel, the more he realizes hes in love with her.

==Cast==
* Karen Gillan as Jane Lockhart
* Stanley Weber as Tom Duvall
* Iain de Caestecker as Roddy
* Freya Mavor as Nicola Ball
* Amy Manson as Darsie Gary Lewis as Benny Lockhart
* Kate Dickie as Anna le Fevre
* Henry Ian Cusick as Willie Scott
* Matilda Thorpe as Andrea

==Production==
The film was financed in part by a crowdfunding campaign on the website Indiegogo, which raised USD$22,660.  Additional funding came through the efforts of producer Claire Mundell, who "  a bunch of different sources of funding together, none of whom interfered in any kind of way with the way the film turned out".   

Scottish actor Emun Elliott was originally attached to play the role of Tom, but was replaced with French actor Stanley Weber. This led the filmmakers to write into the script the characters French background, as well as "a lot of explanation about how this French guy could end up in Glasgow and be a publisher". 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 