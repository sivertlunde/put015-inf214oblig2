The Informer (1929 film)
{{Infobox film
| name           = The Informer
| image          =
| caption        =
| director       = Arthur Robison
| producer       = 
| writer         = Liam OFlaherty (novel)    Benn W. Levy   Rolf E. Vanloo
| starring       = Lya De Putti Lars Hanson Warwick Ward   Carl Harbord Harry Stafford
| cinematography = Werner Brandes   Theodor Sparkuhl
| editing        = Emile de Ruelle
| studio         = British International Pictures
| distributor    = Wardour Films
| released       = 17 October 1929
| runtime        = 83 minutes
| country        = United Kingdom 
| awards         =
| language       = English
| budget         =
| preceded_by    =
| followed_by    =
}} The Informer The Informer. In the film, a man betrays his best friend, a member of a terrorist organisation, to the authorities and is then pursued by the other members of the organisation. 

==Production== sound revolution was taking place. The film was made with a soundtrack, sound effects and talking scenes. A fully silent version was also released. Robison was one of a number of Germans engaged to work in the British Film Industry following the Film Act of 1927.

==Cast==
* Lya De Putti - Katie Fox 
* Lars Hanson - Gypo Nolan
* Warwick Ward - Dan Gallagher
* Carl Harbord - Francis McPhilip
* Dennis Wyndham - Murphy
* Janice Adair - Bessie
* Daisy Campbell - Mrs McPhillip
* Craighall Sherry - Mulholland
* Ray Milland - Sharpshooter
* Ellen Pollock - Prostitute
* Johnny Butt - Publican

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 

 