Marriage Blue
{{Infobox film name           = Marriage Blue image          =  director       = Hong Ji-young producer       = Min Jin-su writer         = Ko Myung-ju starring       = Kim Kang-woo   Kim Hyo-jin   Ju Ji-hoon   Lee Yeon-hee   Ok Taecyeon   Ma Dong-seok   Guzal Tursunova   Lee Hee-joon   Ko Joon-hee music          = Lee Jae-jin  cinematography = Lee Seon-yeong editing        = Kim Sun-min studio         = Soo Film distributor    =  released       =   runtime        = 118 minutes country        = South Korea language       = Korean
}}
Marriage Blue ( ; lit. "The Night Before the Wedding") is a 2013 South Korean romantic comedy film that follows the misadventures of four engaged couples in the week leading up to their weddings.   

==Plot==
Tae-kyu (Kim Kang-woo) is a former professional baseball player who currently coaches for a minor league team. His girlfriend Joo-young (Kim Hyo-jin) runs a successful urology clinic. Theyve broken up once before then gotten back together, and now theyre about to get married. But a week before their wedding, Tae-kyu learns something about Joo-young that makes him feel betrayed and jealous. Growing increasingly neurotic, he becomes obsessed with finding all about her exes and dating past.

Chef Won-chul (Ok Taecyeon) and nail artist So-mi (Lee Yeon-hee) have been a couple for seven years and their wedding day is fast approaching. But feeling that something is missing in their life, So-mi suspects that Won-chul doesnt feel passionate towards her anymore and merely regards their relationship as a comfortable living arrangement. Several days before getting hitched, So-mi travels to Jeju Island for a nail art competition, and there she meets and becomes attracted to Kyung-soo (Ju Ji-hoon), a tour guide and webcomic writer.
 flower shop, Korean citizenship.

Dae-bok (Lee Hee-joon) works at Joo-youngs urology clinic. Hes recently began dating Yi-ra (Ko Joon-hee), a wedding planner. Then one day, during a baseball game, Yi-ra tells him that shes pregnant, and Dae-bok immediately proposes. But as they prepare for their wedding, they keep arguing constantly. It isnt that Dae-bok is scared of becoming a father, but he senses from Yi-ras cynical attitude that she doesnt feel their relationship has any kind of long-term future, so he desperately tries to change her mind.

==Cast==
*Kim Kang-woo as Tae-kyu
*Kim Hyo-jin as Joo-young
*Ju Ji-hoon as Kyung-soo 
*Lee Yeon-hee as So-mi  
*Ok Taecyeon as Won-chul    
*Ma Dong-seok as Geon-ho
*Guzal Tursunova as Vika
*Lee Hee-joon as Dae-bok
*Ko Joon-hee as Yi-ra  
*Oh Na-ra as Seon-ok Joo as Ah-reum 
*Song Jae-ho as Kim Seok-dong 
*Lee Joo-sil as Kim Seok-dongs wife 
*Jang Gwang as Yi-ras father
*Jeon Soo-kyung as Dress shop owner, Yi-ras boss Kim Kwang-kyu as Doctor Kim, Joo-youngs ex-husband
*Hwang Jeong-min   as Woman touring Jeju
*Lee Dal-hyeong as Installation engineer 
*Lee Mi-do as Eccentric bride  Kim Ji-young as Dae-boks mother

==Release==
Marriage Blue opened in South Korea on November 21, 2013 at third place in the box office, with a gross of  .  Despite little hype, it succeeded in securing a niche market amongst some heavier hitters, and on November 25, the fifth day of its release, the film ranked first in the daily box office chart, beating   and  .  On its second week, it placed second on the weekly box office chart, with 841,563 admissions.  At the end of its run, it had a total of 1,214,351 admissions.

In addition to South Korea, rights to Marriage Blue were pre-sold to seven Asian countries, and local theaters in Singapore, Hong Kong, Japan, Thailand, Taiwan and China screened the film from December 2013 to May 2014. 

==References==
 

==External links==
*   
* 
* 
* 

 
 
 
 
 