Tehilim (film)
{{Infobox film
| name           = Tehilim
| director       = Raphael Nadjari
| producer       = Geoffroy Grison Fred Bellaiche Marek Rosenbaum Itai Tamir Noah Harlan
| writer         = Raphaël Nadjari and  
| starring       = Michael Moshonov Limor Goldstein Yonathan Alster Shmuel Vilojni Yohav Hait Ilan Dar Reut Lev
| music          =  
| cinematography = Laurent Brunet Sean Foley
| distributor    = Haut et Court
| released       =  
| runtime        = 100 minutes
| country        = Israel France
| language       = Hebrew
| budget         =
| gross          =
}}

Tehilim (2007) is Raphael Nadjaris fifth feature film. It was shot in Jerusalem in 2006.

==Plot==
In contemporary Jerusalem, a small Jewish family leads an ordinary life until following a car accident, the father mysteriously disappears. They all deal with his absence and the difficulties of everyday life as best they can. While the adults take refuge in silence or traditions, the two children, Menachem and David, seek their own way to find their father.

==Cast==
* Michael Moshonov - Menachem
* Yonathan Alster David
* Limor Goldstein - Alma
* Shmuel Vilozni - Eli
* Ilan Dar - Shmuel
* Yoav Hait - Aharon
* Reut Lev - Deborah

==Awards and nominations== Cannes Film Festival (2007) - In Competition   
*Tokyo Filmex (2007) - Grand Prize    

==Format & Release==
The film was shot in HD, using prime lenses. The film was released in France by Haut et Court, in Spain, Belgium and Germany and exhibited at the Museum of Modern Art in New York    the following year.

Tehilim received top prize in the Tokyo Filmex.  The jury conducted by Lee Chang Dong wrote the following statement : "The mysterious loss of the father of an average Israeli family brings to the fore a universal problem of todays world - the lack of orientation. It is left to the individual whether to see this as a human subject matter, an intimate story or a reflection of Israeli society today. It is told in a personal style, which - as world cinema- transgresses borders and religions.".   

==References==
 

==External links==
* 

 
 
 
 
 
 
 

 