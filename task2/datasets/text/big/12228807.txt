The Duke (short film)
{{Infobox film  
|  name        = The Duke
|  director    = John McArdle
|  producer    = Sara Proudfoot Clinch
|  screenplay  = John McArdle Tony Booth Lee Gilbert Nicola Stephenson
|  music       = Stephen Warbeck
|  cinematography = Bruce McGowan Stephen Parry
|  released    =  
|  runtime     = 17 minutes
|  country     =   English
}}
 Tony Booth,  Lee Gilbert, and Nicola Stephenson. Set in 1955 Liverpool, it tells the story of a young boy who believes that his grandfather is John Wayne.

== Plot == Western at the cinema with his mother, 7-year-old Jack (Gilbert) discovers that his grandfather Tom (Booth) looks like John Wayne. Upon returning home, he excitedly informs his grandfather, who asks him whether he can keep a secret. At school Jack reveals the secret to his schoolmates, who jeer and refuse to believe him. Later that day his grandfather discovers him crying, and decides to help him out. There is a dramatic showdown, where Tom organizes a horse and rides in to vindicate Jack. In the end, Jack and his grandfather ride down off down a Liverpool street lined with brick houses that dissolve into Monument Valley.

== Cast == Tony Booth as Tom, an elderly Liverpudlian gentleman who lives with his daughter-in-law and grandson. He has a love for the Old West and a soft spot for his grandson, Jack. He allows his grandson to believe that he is John Wayne, leading to a showdown where he intimidates his grandsons tormentor.
* Lee Gilbert as Jack, a 7-year old boy. He believes his grandfather resembles John Wayne.
* Nicola Stephenson as Edith, Jacks mother. She supports her family during the difficult times after the war, in which (it is hinted) her husband died. Thomas Rider as Alan Dobson, a bully who torments Jack.
* Carl Chase as Mr Dobson, Alans father.

== Production == score was composed by Academy Award winner Stephen Warbeck.

== Reception ==
It was screened at film festivals around the world, including Cannes, and won several awards,    including Presidente del Consiglio Regionale del la Compagnia at the Giffoni Film Festival  and Best Short at the Kodak Emerging New European Talent festival.  It also was screened at the 1999 BBC Short Film Festival  

:Note: The British Film Directory inaccurately lists the film as being released in 2004. 

== References ==
 

==External links==
*  
*   at The British Films Directory
*   at The Film Centre 

 
 
 
 