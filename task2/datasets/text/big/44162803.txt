Swapname Ninakku Nandi
{{Infobox film
| name           = Swapname Ninakku Nandi
| image          =
| caption        =
| director       = Kallayam Krishnadas
| producer       =
| writer         =
| screenplay     =
| starring       = Jayabharathi Sukumaran Bahadoor
| music          = G. Devarajan
| cinematography =
| editing        =
| studio         = Mahadeva Films
| distributor    = Mahadeva Films
| released       =  
| country        = India Malayalam
}}
 1983 Cinema Indian Malayalam Malayalam film, directed by Kallayam Krishnadas. The film stars Jayabharathi, Sukumaran and Bahadoor in lead roles. The film had musical score by G. Devarajan.   

==Cast==
*Jayabharathi as Nabisa
*Sukumaran as Madhavankutty
*Bahadoor as Bakker
*KPAC Sunny as Johny
*Poojappura Ravi as Pachu Pilla
*Lalithasree as Kaduthi Ponnamma Kunchan as Mallan
*Alummoodan as Broker Gopalan
*Ravi Menon as Mammukka/Appu
*Archita as Rasiya

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Kallayam Krishnadas and Chunakkara Ramankutty. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Kalichirimaaraatha || K. J. Yesudas, P. Madhuri || Kallayam Krishnadas || 
|-
| 2 || Madanolsava mela || K. J. Yesudas || Chunakkara Ramankutty || 
|-
| 3 || Muthuchilankakal || K. J. Yesudas, P. Madhuri || Chunakkara Ramankutty || 
|-
| 4 || Velli nilaavil || K. J. Yesudas || Kallayam Krishnadas || 
|}

==References==
 

==External links==
*  

 
 
 

 