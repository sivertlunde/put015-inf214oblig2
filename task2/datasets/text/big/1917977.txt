8 Man
 

{{multiple issues|
   
   
}}
{{Infobox animanga/Header
| name            = 8 Man
| image           =  
| caption         = Image from the 1960s television series
| ja_kanji        = エイトマン
| ja_romaji       = Eitoman
| genre           = Science fiction, Action genre|Action, Adventure, Drama
}}
{{Infobox animanga/Print
| type            = manga Kazumasa Hirai
| illustrator     = Jiro Kuwata
| publisher       = Kodansha
| demographic     = Shōnen manga|Shōnen
| magazine        = Weekly Shōnen Magazine
| first           = 1963
| last            = 1966
| volumes         =  10
| volume_list     = 
}}
{{Infobox animanga/Video
| type            = tv series
| director        = Haruyuki Kawajima
| producer        = 
| writer          = 
| music           = Tetsuaki Hagiwara
| studio          = Eiken (company)|Eiken/TCJ Animation Center TBS
| network_en      = Nine Network
| first           = 7 November 1963
| last            = 31 December 1964
| episodes        = 56
| episode_list    = 
}}
{{Infobox animanga/Video
| type            = special
| title           = 8 Man Has Returned
| director        = Akinori Kikuchi
| producer        = 
| writer          = Masakazu Shirai
| music           = 
| studio          = 
| network         = Fuji TV
| released        = 31 August 1987
| runtime         = 
}}
{{Infobox animanga/Video
| type            = live film
| title           = Subete no Sabishii Yoru no Tame ni
| director        = Yasuhiro Horiuchi
| producer        = Isao Urushidani
| writer          = Mitsuyuki Miyazaki Junko Suzuki
| music           = Carole King
| studio          = 
| released        = 1992
| runtime         = 
}}
{{Infobox animanga/Video
| type            = ova
| title           = 8 Man After
| director        = 
| producer        = 
| writer          = 
| music           = 
| studio          = J.C.Staff
| first           = August 21, 1993
| last            = November 22, 1993
| runtime         = 104 minutes   
| episodes        = 4
| episode_list    = 
}}
{{Infobox animanga/Print
| type            = manga
| title           = 8 Man After
| author          = Masahiro Suematsu
| publisher       = Kodansha
| demographic     = Shōnen manga|Shōnen
| magazine        = Weekly Shōnen Magazine
| first           = 1994
| last            = 
| volumes         = 
| volume_list     = 
}}
{{Infobox animanga/Print
| type            = manga
| title           = 8 Man Infinity
| author          = Kyoichi Nanatsuki
| illustrator     = Takayuki Takashi
| publisher       = Kodansha
| demographic     = Seinen manga|Seinen
| magazine        = Magazine Z
| first           = 2005
| last            = 2007
| volumes         = 6
| volume_list     = 
}}
 
 Kazumasa Hirai and manga artist Jiro Kuwata.  He is considered Japans earliest cyborg superhero, predating even Kamen Rider (the same year, Shotaro Ishinomori created Cyborg 009), and was supposedly the inspiration for RoboCop.

The manga was published in Weekly Shōnen Magazine and ran from 1963 to 1966.  The anime series, produced by Eiken with the TCJ Animation Center, was broadcast on Tokyo Broadcasting System, and ran from November 17, 1963 to December 31, 1964, with a total of 56 episodes (plus the "farewell" special episode, "Goodbye, Eightman").

==The story==
 android body. christening himself as "Hachiro Azuma".  He keeps this identity a secret, known only to Tani and his police boss Chief Tanaka.  Even his girlfriend Sachiko and friend Ichiro do not know he is an android.  As 8-Man, Hachiro fights crime (even bringing his murderers to justice). He smoked "energy" cigarettes, to rejuvenate his powers, that he carried in a cigarette case on his belt.

In Japan, the characters origin actually varies significantly between the original manga, the TV series, and the live-action movie.  In the original Japanese manga and TV series, the characters name does not change when he is reborn as 8 Man.  The "Detective Yokoda" name was created for the live action version.  In the manga, Detective Azuma is trapped in a warehouse and gunned down while the TV series has him killed when he is run over by a car.  Also, in the Japanese story, the character is called "8 Man" because he is considered an extra member of the Japanese police force.  There are seven regular police precincts and 8 Man is treated an unofficial eighth precinct all to himself.

The Japanese manga was presented as serial novella stories along with a set of one-shot stories.  Many of the stories were edited down and adapted for the TV series but not all of them.  The novella stories were originally printed one a weekly basis in Shukuu Shōnen Magazine in 16-page increments that actually consisted of 15 story pages and one title page.  Ten additional one-shot stories were presented in seasonal and holiday specials of Shuukuu Shōnen Magazine.  The one-shot stories were generally between 30-40 pages in length.

In the North American version of the series the resurrected detective/android is known as "Tobor" or the word robot spelled backwards. Tani is referred to as "Professor Genius" and the sobriquet of 8-Man is changed slightly to "8th-Man". The story content was clearly directed toward a wider audience of both young and adult viewers.

==Original Japanese Manga Story Titles==

===Novella Stories===

*怪人ゲーレン (Kaijin Geren) - Galen, The Mystery Man

*サタンの兄弟 (Satan no Kyodai) - Satans Brothers

*怪力ロボット007 (Kairiki Robotto 007) - Super-Powered Robot 007

*光線兵器レーザー (Kosen Heiki Reza) - The Laser Beam Gun

*超人サイバー (Chojin Saiba) - Cyber, The Superhuman

*人間ミサイル (Ningen Misairu) - The Human Missile

*殺人ロボット005 (Satsujin Robotto 005) - Murderous Robot 005

*魔女エスパー (Majo Esupa) - Esper, The Witch

*超人類ミュータント (Chojinri Mutanto) - Superhuman Mutant

*魔人コズマ (Majin Kozuma) - The Demon Kozuma
-Jiro Kuwata was imprisoned for possession of a handgun before the final 16-page serial of The Demon Kozuma was completed.   The final serial was drawn by Takaharu Kusunoki for the magazine version.  Jiro Kuwata later redrew the final pages of the story himself by request of Kazumasa Hirai and Rim Publishing so that they could publish a complete version of the final story.  (The publishers were not able to use Kusunokis artwork,  so the story was omitted or left incomplete in previous official releases.)

===Short Episode Stories===

*死刑囚タランチュラ - The Condemned Criminal Tarantula

*決闘 - The Duel

*シャドウ・ボクサー - Shadow Boxer

*復讐鬼ゴースト - Vengeful Demon Ghost

*超振動砲 - The Super Vibration Gun

*マッド・マシン - Mad Machine

*サイボーグPV1号 - Cyborg Number PV1

*殺し屋イライジャ - The Assassin Elijah

*燃える水 - Burning Water

*幽霊ハイウェイ - Phantom Highway

*太陽衛星サンダー (単行本未収録) - Solar Satellite "Thunder" (unreleased story)
This was intended as a lead-in to a series of 23 comic stories adapted from the TV series.

==Original Japanese TV Episode Titles==

# エイトマン登場 - Introducing Eightman
# 殺し屋ゲーレン - Galen, The Hitman
# サタンの兄弟 - Satans Brother
# 死刑台B3 - The B3 Gallows
# 暗黒カプセル - The Darkness Capsule
# 黄金ギャング - The Gold Gang
# 消音ジェット機 - The Stealth Jetplane
# 超小型ミサイル - The Ultra Micro Missile
# 光線銃レーザー - The Lazer Ray Gun
# ロボット007 - Robot 007
# まぼろしの暗殺者 - The Phantom Assassin
# 海底のウラン - The Undersea Uranium
# 人間パンチカード - The Human Punch Card
# スーパーパイロット - The Super Pilot
# 黒い幽霊 - The Black Ghost
# 怪盗黄金虫 - Goldbeetle, The Mysterious Thief
# 超音波ドクター - The Ultrasonic Wave Doctor
# 台風男爵 - The Typhoon Baron
# ゲーレンの逆襲 - Galen Strikes Again
# スパイ指令100号 - Spy Directive No. 100
# ロボットタイガー - The Robot Tiger
# ゼロへの挑戦 - Challenge to Zero
# ナポレオン13世 - Napoleon The 13th
# サラマンダー作戦 - Operation: Salamander
# 超人サイバー - Cyber, The Superhuman
# 地球ゼロアワー - Zero Hour: Earth
# 大怪物イーラ - Eeler, The Giant Monster
# バクテリア作戦 - Operation: Bacteria
# 人間ミサイル - The Human Missile
# サイボーグ人間C1号 - Cyborg No. C1
# 幽霊ハイウェイ - The Phantom Highway
# 太陽衛星サンダー - Thunder, The Solar Satellite
# 人工生命ヴァルカン - Vulcan, The Artificial Lifeform
# 決闘 - The Duel
# 冷凍光線 - The Freeze Ray
# バイラス13号 - Virus No. 13
# 悪夢の7日間 - The 7 Day Nightmare
# 怪人ゴースト - The Mysterious Ghost
# まぼろしを作る少年 - The Boy Who Made a Phantom
# 透明ロボット・ジュピター - Jupiter, The Invisible Robot
# エイトマン暗殺指令 - Order: Assassinate Eightman
# 女王蜂モンスター - The Queen Bee Monster
# 魔女エスパー - Esper, The Witch
# 世界電撃プラン - The World Blitz Plan
# 死刑囚タランチュラ - Tarantula, The Condemned Criminal
# 空飛ぶ魔人 - The Flying Devil
# バブル・ボール作戦 - Operation: Bubble Ball
# 火星人SAW - SAW, The Martian
# 30億人の人質 - 3 Billion Hostages
# 怪像ジャイアント - Giant, The Mysterious Statue
# 狙われた地球 - Target Earth
# 人喰魚ピラニア - The Man-Eating Piranha
# ムタールの反乱 - Moutards Rebellion
# シャークの掟 - Law Of The Shark
# 超人類ミュータント(前編) - Superhuman Mutant (Part One)
# 超人類ミュータント(後編) - Superhuman Mutant (Part Two)
* "Good Bye Eight Man" - a special look back at the TV series.

===The US version===
In 1965, 8 Man was brought to the United States|U.S. as 8th Man (sometimes called "Tobor the 8th Man," as in its English-language theme music), with ABC Films as its syndicated distributor. 52 of the original 56 episodes were "converted" into English.

The characters were renamed as follows:
* Yokota/Azuma/8 Man - Special Agent Brady/Tobor ("robot" spelled backwards)/8th Man
* Tani - Professor Genius
* Tanaka - Chief FumbleThumbs
* Sachiko - Jenny Hartsweet
* Ichiro - Skip

English lyrics were written by Winston Sharples for the theme song, referencing the FBI in one line, which would not be relevant to the Japanese storyline.

The lyrics to the US version are as follows:

Theres a prehistoric monster 
That came from outer space 
Created by the Martians 
To destroy the human race.

The FBI is helpless,  
Its twenty stories tall.  
What can we do? Who can we call?

Call Tobor, the Eighth Man.  
Call Tobor, the Eighth Man.  
Faster than a rocket.  
Quicker than a jet.  
Hes the mighty robot.  
Hes the one to get.  
Call Tobor, the Eighth Man.  
Quick, call Tobor, the mightiest robot of them all!

Japanese characters on signs would be mentioned jokingly, along the lines of "I could figure out where we were if the signs werent written in gibberish".

==Legacy==
 
The 8 Man franchise was revived in the early 1990s by a live action film, video game and new animated series.

===Video game===
In 1991, SNK released a video game edition of Eight Man for the Neo Geo (console)|Neo-Geo arcade and home video game system (both versions are identical) where the player took the role of 8 Man and his robo-comrade 9 Man in a fight against an invading evil robot army.  The game was released internationally.  While the game stayed true to the concept of a crime-fighting super-robot, it was widely panned for being tedious and relying too much on the gimmick of its speed-running effect.

===Live action movie===
 
In 1992, a live-action film version of 8 Man was produced in Japan.  Titled Eitoman - Subete no Sabishī Yoru no Tame ni ( , lit. 8 Man - For All the Lonely Night ), it was directed by  Yasuhiro Horiuchi and starred Kai Shishido as the title character and Toshihide Wakamatsu as Detective Yokota.  Distributed in the United States by Fox Lorber video simply as 8 Man, the movie was widely panned for its choppy editing, mediocre direction and low-budget feel.  Many modern American viewers, unfamiliar with the older animated series, felt the movie was an inferior version of RoboCop, despite the fact that the latter was a much more recent franchise.

===8 Man After=== OVA series 8 Man After. Existing in a world far more corrupt than that of his predecessor, the new 8 Man had no qualms about being extremely violent towards the cybernetic criminals who had murdered him previously. Licensed by Streamline Pictures, it has since gone out of print.

8 Man After begins with Sachiko remembering the night when Azuma, the original 8 Man disappeared. After that it cuts to Sachiko working at a cybernetics research firm that Hazama is investigating as part of a case he is working on. Hazama is investigating an employee named Eddie Schmidt who has disappeared and took many cybernetic developments and secrets with him. The head of the research firm is contemplating running for mayor but is also a criminal kingpin named Mister Halloween. One of his cybernetic henchmen named Tony Gleck has a history with Hazama from when they were both cops. Hazama is attacked and mortally wounded by Tony Gleck and the Professor tells the police chief that he is transferring Hazamas mind into the 8-Mans body.

Soon after, Sachiko is witness to a gang war between cyborgs and is in danger when something unseen begins attacking the cyborgs and disabling them via removal of their cybernetic limbs. This unseen something is in fact the 8-Man moving at super speed. After he stops, Sachiko calls to him, calling him Azuma-san but this gets no response from 8-Man who leaves after the fight is over. Soon, Hazama appears on the scene with no wounds from his attack by Tony Gleck and Sachiko is both concerned and suspicious. Sachiko asks Hazama to protect her from the cyber criminals as she is a material witness, and they together attempt to download secret records from the company she works for. This trips alarms and she is knocked out by security. Hazama attempts to escape with her, but then switches to 8-man to complete the escape from the guards.

Later the 8-Man is at the Professors lab downloading the data for the Professor to study but it is encrypted and will take time to decode. The Professor then gives 8-man a capsule of stimulant, stating that he will need it to stay recharged and assures him that it is safe unlike the illegal cybo-mechamine that other cyborgs use to keep their brains stimulated at the price of getting brain damaged and berzerk. 8-man places this capsule in his belt buckle (instead of it being an "energy cigarette" like in the old cartoon.) The Professor also advises him that even though his body is that of a cyborg, his "heart and soul" is that of Hazama. 8-man assures him that he detects no trace of his "hosts" emotions in him. The Professor warns him that this must always be the case, and that the previous 8-Man paid the "ultimate price" for forgetting this fact.

Later at a football game where the local team is supposedly rebuilt and now a contender for the title is going to face the world champions, Hazama and Sachiko accompany a boy that hired Hazama to find his lost Dad who is an ex-football player that is on the new team. The team however are all cyborgs and under the effects of the cybo-mechamine, making them berzerk. After crippling and killing the other team they attack the fans in the stadium. Hazama ducks out of site and returns as 8-Man however something unexpected occurs. As a woman is killed, some of her blood splashes onto 8-Mans face. This triggers a traumatic memory within Hazamas mind that in turn causes 8-Mans eyes to glow red and he goes berzerk and brutally demolishes the cyborgs and nearly kills the father of his client. It is only seeing Sachiko that snaps 8-Man out of this and causes his cybernetic brain to resume control and 8-Man looks around in shock and confusion and then leaves even as Sachiko realizes this 8-Man is not Azuma-san.

The Professor later speaks to Hazama (8-Man in the form of Hazama) and tells him it was a glitch in the system. He then states that Hazama is the glitch, not the 8-Man. He informs Hazama that the 8-Man cyborg body is a deadly weapon that he long ago installed a behavioral circuit breaker to prevent such outbursts and keep the 8-Man under control. However the mixing of human emotions with cyborg technology is unpredictable and that it can happen again. The Police chief later tells the Professor that Hazamas sister was killed by Tony Gleck as Hazama was forced to watch. In flashback we see that after she was shot, her blood splashed onto Hazamas face. Hazama after this incident has some trouble reconciling that he is a cyborg now and it also seems that he and 8-Man are indeed separate minds for a time.

Tony Gleck takes over the corporation and reveals that Mr. Halloween is the CEO of the cybernetics firm. Tony is slowly becoming more of a cyborg due to the cyber drug destroying his body, and Eddie who is revealed to have had his brain placed into a computer bank tells Tony that if they capture 8-Man then Tony can become the new 8-Man. Tony captures 8-Man and as Eddie scans 8-Mans memory banks so that he can purge Hazama he finds a reference file containing Azumas name. Sachiko asks him to help Azuma but Eddie reveals that Azuma has been erased from the cyborgs brain. 8-Man gets free and both Tony and Eddie are destroyed.

Hazama then leaves town with the boy having promised to look after him. Sachiko eventually finds them and joins Hazamas detective agency.

Some inconsistencies in the English dub of 8-Man After:

1. When the police chief speaks of Hazamas past as a cop, he states that Hazama killed 3 cops and 4 enforcers of a crime syndicate. It was never proven the 3 cops were dirty, but one of them was Tony Gleck. The chief states they killed Hazamas fiance while he was held down and forced to watch. Later it would be stated that it was his sister and not his fiance that was murdered.

2. The Professor tells Hazama/8-Man that he had installed a behavior circuit breaker in 8-man when the cyborg body was being developed to prevent it from running amok. When the Professor later speaks to the Chief about 8-Mans "malfunction" on the football field he says "the cyborg brain I implanted should have prevented this".  This is inconsistent in that to become the 8-Man, the persons brainwaves, memories and personality are transferred into the cyborg brain of the 8-Man.

3. When the Professor speaks to 8-Man (before 8-man had his first malfunction) he warns him to constantly monitor himself to ensure that no emotions affect his systems. 8-Man states "If you refer to the human whose body serves as my host, I detect no traces of his emotions".  This again is inconsistent since Hazamas brainwaves were transferred into the body of the 8-Man.

There were two novelizations of 8-Man After in Japan.  The first one was titled "8-Man New Generation" and starts off with the words "Another time, another place" (in English).  Sachikos last name, Yokogawa, is not the same last name given for the character in the 8-Man and 8-Man Infinity series.

===8 Man Infinity===
A manga series called 8 Man Infinity is being authored by Kyoichi Nanatsuki under Kodansha, which is being serialized under Kodanshas Magazine Z.

==Reception==
8 Man was ranked ninth in Mania Entertainments 10 Most Iconic Anime Heroes written by Thomas Zoth who commented that "Before  , 8 Man helped to shape the trajectory of robot and cyborg heroes for the next decade." {{cite web|url=http://www.mania.com/10-iconic-anime-heroes_article_119883.html|title=10 Most Iconic Anime Heroes|last=Zoth|first=Thomas|date=January 12, 2010
|publisher=Mania Entertainment|accessdate=January 22, 2010}} 

==References==
Notes
 

==External links==
*   Official Tobor the 8th Man English-Dub DVD Website.
*   (1963)
*   (1965)
*   (1992)
*   (1993)
*  
*   of the video game version of 8 Man for Neo Geo, drawing comparisons with the greater canon

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 