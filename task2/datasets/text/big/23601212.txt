The Spy with My Face
{{Infobox Film
| name           = The Spy with My Face
| image          = Spy With My Face Poster.JPG
| image_size     = 
| caption        = 
| director       = John Newland
| producer       = Sam Rolfe
| writer         = Clyde Ware (story and screenplay) Joseph Calvelli (screenplay)
| narrator       = 
| starring       = Robert Vaughn Senta Berger David McCallum
| music          = Morton Stevens
| cinematography = Fred J. Koenekamp
| editing        = 
| studio         = 
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 86 min. (UK); 88 min. (US)
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
The Spy with My Face is a 1965 spy-fi spy film based on The Man from U.N.C.L.E. television series. Robert Vaughn and David McCallum reprised their roles as secret agents Napoleon Solo and Illya Kuryakin respectively. THRUSH tries to steal a super weapon by substituting a double for Solo.  
 1966 as a double feature with To Trap a Spy. "Alpine" sequences were filmed at the Griffith Park Observatory in California. Sequences added to the original The Double Affair for a feature were reused in The Four-Steps Affair and The Dippy Blonde Affair episodes of the series.

==Plot==
Mr. Waverly (Leo G. Carroll) gives U.N.C.L.E. agents Solo, Kuryakin, Arsene Coria (Fabrizio Mioni) from Italy, and Namana (Bill Gunn) from Liberia their assignment: they are to take a top secret code to a hidden location. After they leave, Waverly sends another agent, Australian Kitt Kittridge (Donald Harron), to follow them without their knowledge.

However, THRUSH has gotten wind of the "August Affair", though the villains know little more than the name of the operation. They send Serena (Senta Berger) to entice Solo to her apartment. After each unsuccessfully tries to find out what the other is after, he is gassed into unconsciousness and a THRUSH agent, surgically altered to look and sound like Solo, takes his place. An attempt is made to kill Kuryakin to minimize the chance of the substitution being detected, but it fails.

Aboard a jet liner, the fake Solo manages to open the briefcase and photograph the code. Kittridge greets his old comrade (from Solos prior mission) and makes the mistake of saying that his instincts are telling him that there is something wrong with Solo, forcing the double to kill him. Solo tells Kuryakin that Kittridge was a THRUSH assassin. Solos latest girlfriend, stewardess Sandy Wister (Sharon Farrell), is miffed that he acts as if he does not know her, but makes no fuss.

When the agents reach their destination a secret, heavily guarded underground vault in the Swiss Alps (filmed at Griffith Observatory), they are told what is inside. Project Earth Save is a super weapon, designed by scientists from many nations because possible signs of an alien attack had been detected. The weapon is so deadly, even the sight of it is fatal, so they have to wear goggles. The code they have brought opens the vault, and is changed every year in August. When Namana spots Solos missing jacket button in the briefcase, the fake Solo rips off Namanas goggles before he can warn anyone; the African agent is mesmerized by the weapon and stumbles into the vault to die.

Meanwhile, the real Solo is kept prisoner not far away. He escapes, killing the head villain in the process, and heads for the vault. The double and Serena intercept him on the highway. When U.N.C.L.E. agents drive up, Serena shoots the fake Solo. Afterwards, she talks Solo into letting her go to return the favor.

==Production and release==
Filming for what would be edited into both the episode "The Double Affair" as well as the feature "The Spy with My Face" was begun in August 1964.  All the scenes were filmed in color, although the television version was broadcast in black and white.  The movie edit premiered in London in August, 1965. The film was advertised in the U.K. as a "Mr. Solo" adventure rather than a tie-in to "The Man from U.N.C.L.E.", but during its two-month run the series became a top-rated show in the U.K.

==Cast==
*Robert Vaughn as Napoleon Solo
*Senta Berger as Serena
*David McCallum as Illya Kuryakin
*Leo G. Carroll as Alexander Waverly Michael Evans as Darius Two, the head THRUSH agent
*Sharon Farrell as Sandy Wister
*Fabrizio Mioni as Arsene Coria
*Donald Harron as Kitt Kittridge
*Bill Gunn as Namana
*Jennifer Billingsley as Taffy, Sandys friend and fellow stewardess
*Paula Raymond as Director
*Donna Michelle as Nina
*Harold Gould as Doctor, who operated on Solos double

==External links==
* 
*  

 

 
 
 
 
 
 