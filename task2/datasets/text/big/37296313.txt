First Love (1921 film)
{{infobox film
| name           = First Love
| image          =
| imagesize      =
| caption        =
| director       = Maurice Campbell
| producer       = Realart Pictures Percy Heath Aubrey Staufer
| starring       = Constance Binney  Warner Baxter       
| cinematography = H. Kinley Martin
| editing        =
| distributor    = Paramount Pictures
| released       =   reels
| country        = USA
| language       = Silent film (English intertitles)
}}
First Love is a 1921 silent film produced by the Realart Company and distributed through the related Paramount Pictures. It stars Constance Binney and was directed by Maurice Campbell. Warner Baxter has one of his earliest screen portrayals here. Only the first reel of this film is known to survive at the Museum of Modern Art.  

==Cast==
*Constance Binney - Kathleen ODonnell
*Warner Baxter - Donald Halliday
*George Webb - Harry Stanton
*Betty Schade - Yvette De Vonne
*George Hernandez - Tad ODonnell
*Fanny Midgley - Mrs. ODonnell (*as FAnnie Midgley) Edward Jobson - Peter Holliday
*Agnes Adams - Icecream-cone Girl
*Maxine Elliott Hicks - Speeder
*Dorothy Gordon - Elsie Edwards

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 


 
 