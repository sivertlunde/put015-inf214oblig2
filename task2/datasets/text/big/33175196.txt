Untitled (film)
 
{{Infobox film
| name           = Untitled
| image          = 
| caption        = 
| director       = Shaun Troke
| producer       = Shaun Troke Stuart Kennedy Michael Lonsdale
| writer         = Shaun Troke Steven Jarrett
| screenplay     = 
| story          = 
| based on       =  
| starring       = Shaun Troke Nikki Harrup Leonora Moore Danny Goldberg
| music          = Kris Sanders Carl Troke Shaun Troke
| cinematography = 
| editing        = 
| studio         = Shaunywa Films
| distributor    = 
| released       =  
| runtime        = 
| country        = United Kingdom
| language       = English
| budget         = £2,000
| gross          = 
}} found footage, the film stars Troke alongside Nikki Harrup, Leonora Moore and Danny Goldberg.    {{cite web|url=http://www.dreadcentral.com/news/41886/terror-has-no-name-untitled
|title=Terror Has No Name in Untitled|work=Dread Central|date=14 January 2011|accessdate=22 September 2011}}  {{cite web|url=http://www.bloody-disgusting.com/news/23041
|title=New Found Footage Flick Goes Untitled|work=Bloody Disgusting|date=14 January 2011|accessdate=22 September 2011}}  {{cite web|url=http://horrormoviesastuff.webng.com/Untitled.html
|title=New supernatural horror indie is "Untitled" |work=Horror Movies and Stuff|accessdate=22 September 2011}} 

== Plot ==
A filmmaker and his crew spend a weekend making a documentary in a cottage in Wales.  Their film is never completed due to sinister forces in the building.

== Cast ==
* Shaun Troke as Shaun
* Nikki Harrup as Nikki
* Leonora Moore as Leo
* Danny Goldberg as Danny
* Steve Purbrick as Evan Driscoll
* Holly Kenyon as Celyn Bowen

== Production ==
Alongside writer Steven Jarrett, Troke explored the background mythology of the film and had created a script by early 2009.  Troke had originally intended to shoot the film by the end of 2009, but, after he was hired to direct the horror film Sparrow (2010 film)|Sparrow, production on Untitled halted.  After working with both Moore and Harrup on Sparrow, Troke ultimately decided to cast both of them in the lead female roles in Untitled. {{cite web|url=http://denachtvlinders.nl/2011/untitled-trailer-shaun-troke/
|title=‘Untitled’ is een eng huisje in Wales|work=DENACHTVLINDERS.nl|language=Dutch|date=5 April 2011|accessdate=22 September 2011}}    

== Accolades ==
The film won Best Narrative Feature at the American International Film Festival in September 2011.    Untitled will go on a limited cinematic release in 2012.

==Marketing and release==
Prior to destival release, Untitled used a viral marketing campaign by promoting various clips of the film via the films official YouTube channel.     Similar to many mockumentary films, this footage was presented as "genuine".       Untitled premiered at the American International Film Festival in September 2011. 

== Reception==

 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 