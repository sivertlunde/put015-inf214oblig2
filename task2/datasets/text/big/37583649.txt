Las delicias del poder
{{Infobox film
| name           = Las delicias del poder
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Iván Lipkies
| producer       = Ivette E. Lipkies
| writer         = María Elena Velasco
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = María Elena Velasco Ernesto Gómez Cruz Irma Dorantes Farnesio de Bernal Adalberto Martínez Héctor Ortega
| music          = Álvaro Cerviño
| cinematography = Alberto Lee
| editing        = Antonio Belmont Iván Lipkies
| studio         = Estudios Churubusco
| distributor    = Vlady Realizadores
| released       =  
| runtime        = 129 minutes
| country        = Mexico 
| language       = Spanish
| budget         = 
| gross          = $11,157,108 (MXN)    
}}
Las delicias del poder ("The Delights of Power") is a 1999 Mexican political satire comedy film directed by Iván Lipkies starring María Elena Velasco as La India María with Ernesto Gómez Cruz, Irma Dorantes, and Adalberto Martínez. The film broke the box-office record in Mexico at the time of its release    earning 11 million pesos and having been the second-highest grossing film of the year. 

==Plot==
In 1952, the presidential candidate of the PUM political party Carmelo Barriga visits the rural town of San Jilemón el Alto where he gives a speech promoting his candidacy and political party. During the speech and among the townspeople, a young woman gives birth to two twin girls. In a political move, Carmelo decides to adopt one of the girls whom he names Lorena and who becomes an active politician in her own political party, the PUF. Lorena then becomes a presidential candidate running against her rival, Santos Barboza of the PUM. When Lorena and her campaign visits San Toribio del Trueno, a firework accident causes her to abandon the candidacy. Fortunately, campaign member Gonzalo remembers about Lorenas twin and decides to search for her so that she can substitute Lorena and therefore the campaign can resume.

==Cast==
*María Elena Velasco as María Becerra / Lorena Barriga
*Ernesto Gómez Cruz	as Carmelo Barriga
*Irma Dorantes as Consuelo Artigas
*Farnesio de Bernal as Gonzalo
*Adalberto Martínez as Remígio
*Héctor Ortega as Santos Barboza
*Arturo Echeverría as Germán
*José Luis Moreno as Cajigas (credited as Moreno López)
*Alicia Sandoval as Remedios
*Fernando Mena as Ocampo Alejandro Márquez as Assistante de Santos
*Raúl Martínez (actor)|Raúl Martínez as Assistante de Santos
*Carlos Rotzinger as Presidente
Campaña de Carmelo
*Pancho Cruz as Porrista
*Rosa Isela González as Mujer embarazada
*Paola Flores as Mujer indígena
*Esperanza Valerio as Comadrona
*Ingrid Quintana as Jovencita
*Goretti Lipkies as Enfermera Miguel Ramírez as Doctor
Auditorio W.T.C
*Sergio Zaldivar as Reportero
*Joel Adame Salazar as Reportero
*Gaby Cairo as Reportera de T.V.
*Miguel Ángel Mejía as Guarura de Carmelo
*Enrique Juárez as Guarura de Carmelo
Oficina Santos
*Osvaldo Álvarez as Periodista 
*Daniel Rojo as Periodista
*Jovita Ocampo as Ciudadana
*Blanca Vargas as Ciudadana
Mercado
*Andrea Villa as Locataria
*Tony Martínez as Marchanta
*Tere Ocampo as Marchanta jarocha
Campaña Lorena
*Adrián Méndez as Cohetero
*Jorge Lazcano as Cohetero
*Fernando Castro as Presidente municipal
Hospital
*Claudia Fandiño as Enfermera
*Rodrigo de Garay as Locutor de T.V.
*Alfredo Visoso as Doctor en camilla
*Gloria Velasco as Doctora
Pueblo sin agua
*Gregorio Ruíz as Campesino
Mercado de Jamaica
*Víctor Guzmán as Pancho
*Tomás Nomás as Trovador
*Alejandro Bautista as Teporocho
*Juan Carlos Ocampo as Teporocho
*Felipe Solís as Chente el policleto
Delegación
*Ramón Barrera as Burócrata
Casa de Carmelo
*Eduardo Pesqueira as Director de periódico
*Francisco Duarte as Director de periódico
*Cecilia Alcocer as Locutora de T.V.
*Roberto Morlet as Guardacasa en calle
*Marcial Casale as Guardacasa en caseta
Teotihuacan
*Ernesto Pape as Empresario americano
*Rubén Cerda as Congresista gordo
*José Luis Pape as Americano con puro
*Yekaterina Kiev as Edecán con camotes
*Sandra Rojas as Bailarina
*Pola Klagge as Mtra. de ceremonias
*Víctor Jackson as Bailarín
*Domingo Rivera as Mesero
*Alejandro Cairo as Arqueólogo
*Lázaro Patterson as Americano de color
Noticiero de televisión
*Álvaro Cerviño as Locutor
*Rocío Yeo as Locutora
Aeropuerto
*Carlos Gastélum as Gobernador
*Maru Dueñas as Marilyn
*Montserrat Gómez as Amiga de Marilyn
*Cap. P.A. Enrique Berra as Piloto
Escuela de policía
*Alejandro Linares as Policía entrenador
*Eugenio Lobo as Reportero
*Rafael Rodríguez as Tirador
Cena japonesa
*Gerardo Carmona as Chef japonés
*Kiyohiro Watanabe as Chef japonés
*Shoki Goto as Embajador
*Tamy Yumibe as Esposa
*Kosuke Heianna as Canciller
*Roberto Wock as Guardaespaldas
*Kasuko Nagao as Comensal
*Sayuri Takahashi as Comensal
*Lulu Delgado as Geisha
*Minerva García as Geisha
Debate
*Jorge Obregón as Curioso en debate
*Hilario García as Jefe de seguridad
*Oscar Vives as Turrubiates
*Evaristo Osorio as Opportunista
*Gerardo Campbell as Benavides
*Adriana del Río as Moderadora
Avenida
*Miguel Galván as Cochinón

==References==
 

==External links==
* 
* 

 
 
 
 