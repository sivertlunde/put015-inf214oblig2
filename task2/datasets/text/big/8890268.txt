Back in the Seventies
{{Infobox film
| name           = Back in the Seventies
| image          = Alláenelsetenta1.jpg
| image size     =
| caption        =
| director       = Francisco Múgica
| producer       =
| writer         = Manuel Agromayor  Tulio Demicheli   Alfredo de la Guardia
| narrator       =
| starring       = Silvana Roth   Carlos Cores   María Armand   Alberto Bello
| music          = Julián Bautista 
| cinematography = Hugo Chiesa
| editing        =José Cañizares 
| studio         = Corporación Cinematográfica Argentina
| distributor    =
| released       = 24 May 1945
| runtime        = 90 minutes
| country        = Argentina
| language       = Spanish
| budget         =
| preceded by    =
| followed by    =
}} historical drama film directed by Francisco Múgica and starring Silvana Roth, Carlos Cores and Felisa Mary. Roth plays a fictionalized version of Élida Paso (1867-1893) an Argentine Pharmacist who became the first woman from South America to Graduation|graduate.

The films sets were designed by Mario Vanarelli.

==Cast==
*Silvana Roth as Élida Paso
*Carlos Cores
*María Armand
*Alberto Bello
*Carlos Bellucci
*Olimpio Bobbio
*Dario Garzay
*Gloria Grey
*José María Gutiérrez (actor)|José María Gutiérrez
*Virginia Luque
*Domingo Mania
*Federico Mansilla
*Felisa Mary
*Mario Medrano
*Gonzalo Palomero
*Matilde Rivera
*Jorge Villoldo

==External links==
* 
 
 
 
 
 
 
 
 
 
 
 
 

 