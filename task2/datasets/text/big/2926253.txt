Misty (film)
  James B. Clark. 

==1961 filming== Chincoteague on the Delmarva Peninsula, it was largely filmed on-location. Using local people for most roles, it starred only six professional actors, including Arthur OConnell, Pam Smith and future Hollywood executive David Ladd. Misty was played by another horse, although she may be seen in the background in a group of ponies.
 The Pony ponies who Chincoteague Volunteer Fire Department. It features two real life characters of Chincoteague, a young brother and sister, who capture an elusive mare on Assateague named the Phantom and later own her foal, Misty.
 Island Theatre, now called Roxy Theater. The book and the movie brought widespread publicity to Chincoteague and Assateague, and the local culture, traditions, and natural beauty and wildlife on the remote and isolated barrier islands.

==Aftermath==
The coastal area on the Atlantic Ocean is no stranger to volatile weather. The year after the film was released, a winter storm, one of the worst Noreasters to ever occur there, struck. During the Ash Wednesday Storm of 1962, the Beebe family, the real-life owners of Misty, brought her inside their home to weather the storm. Shortly thereafter, she gave birth to a foal, which the children named "Stormy." This prompted author Henry to write a sequel:  Stormy, Mistys Foal, published in 1963.  Henry had written a previous sequel, Sea Star: Orphan of Chincoteague, published in 1949.

Possibly due in part to publicity from Ms. Henrys books and the movie, most of Assateague Island was protected from development by the National Park Service and became Assateague Island National Seashore in 1965. 
 Eastern Shore, the annual "Pony Swim" and auction continues, helping both the community and the herd of wild ponies.  

Misty, a classic family movie, is available on home video and DVD. 

 

==References==
 

==External links==
* 
* 

 
 
 
 
 

 