The Holiday
 
{{Infobox film
| name           = The Holiday
| image          = Theholidayposter.jpg
| border         = yes
| alt            =  
| caption        = Theatrical release poster
| director       = Nancy Meyers
| producer       = Nancy Meyers Bruce A. Block
| writer         = Nancy Meyers
| starring       = {{Plain list | 
* Cameron Diaz
* Kate Winslet
* Jude Law
* Jack Black
* Eli Wallach
* Edward Burns
* Rufus Sewell
}}
| music = Hans Zimmer
| cinematography = Dean Cundey
| editing        = Joe Hutshing
| studio         = Relativity Media Waverly Films
| distributor    = Columbia Pictures   (United States)  Universal Pictures  (International) 
| released       =  
| runtime        = 136 minutes  {{cite web 
| url = http://www.rottentomatoes.com/m/the_holiday 
| title = The Holiday Reviews, Pictures 
| work = Rotten Tomatoes 
| publisher = Flixster 
| accessdate = 2010-10-21
}} 
| country        = United States English
| budget         = $85 million    
| gross          = $205,135,324 
}} exchange homes holiday season. Jude Law and Jack Black co-star, with Eli Wallach, Shannyn Sossamon, Edward Burns and Rufus Sewell playing key supporting roles.

The Holiday was first released on December 6, 2006, in Spain and on December 8, 2006, in North America and the United Kingdom. It grossed over $205 million worldwide. Reviews were positive towards the films visual aesthetic design and the acting, most notably Winslets performance as society column editor Iris. However, the plot drew a mixed response from critics, who criticized plot elements that lacked any surprises or were predictable. Diaz garnered an ALMA Award nomination for her performance, while Winslet was nominated for an Irish Film and Television Award the following year. The film itself won the 2007 Teen Choice Award in the Chick Flick category.

==Plot==
Iris Simpkins (Kate Winslet), a society column editor for The Daily Telegraph in London, has been in love with Jasper Bloom (Rufus Sewell) for over three years, despite his infidelities. When she finds out that he is engaged to the "other woman," Iris despairs over the state of affairs in her life. Meanwhile, Amanda Woods (Cameron Diaz), a workaholic who owns a company that produces movie trailers in Los Angeles, discovers that her live-in boyfriend Ethan Ebbers (Edward Burns) has been cheating on her with his 24-year-old secretary.  She decides she wants to get away for the holidays and visits a home swap website on which Iris had previously listed her "quaint cottage in Surrey.” Amanda contacts Iris about her interest. Iris quickly agrees and the two agree to swap homes for two weeks.

Iris revels in the luxury of Amandas Los Angeles home, while Amanda is disappointed by the slower, quieter pace of life in Surrey. Amanda grows bored after just a few hours, and books a flight home for the next day. Later that night, Iris’ brother Graham (Jude Law) knocks at the door assuming Iris is home. Graham asks Amanda to let him spend the night despite the fact that he is a stranger, as he has been drinking at the pub and doesnt want to drive. Because of the obvious chemistry between them, they end up sleeping together.

In the morning, Graham receives a number of phone calls from Sophie and Olivia, which rouses the suspicions of Amanda that Graham is a womanizer. Graham, knowing that Amanda is leaving to return home, says to Amanda that "if the flight gets cancelled,   having dinner at the pub" with friends. At the airport Amanda decides to stay and goes to the pub. Graham enters the pub and looks for her but cannot see her until he meets his friends and then sees Amanda. Amanda drinks far too much that night. Graham suggests they go to lunch to get to know one another better. During lunch, Amanda shares with Graham that her parents divorced when she was fifteen and since then she has been unable to cry. Graham responds that he cries all the time: movies, books, and birthday cards. He also reveals himself to be a book editor. While obviously enjoying each others company, Amanda is worried that the relationship will become "complicated," and tries to keep Graham at arms length.

Meanwhile, Iris is enjoying the stay at Amandas house. She meets Ethans friend Miles (Jack Black) when he comes with his girlfriend Maggie (Shannyn Sossamon) to Amandas house. Later, she finds an old man, Arthur Abbott (Eli Wallach) standing lost at the corner, and she escorts him home. She learns that he was a screenwriter during the Golden Age of Hollywood. The two become fast friends. Arthur notes that Iris behavior toward Jasper does not match her strong personality, and he suggests movies for her to watch that include strong female characters, in hopes that she can acquire what he calls "gumption."

Amanda chooses not to see Graham again but once alone in the house she has a change of heart and surprises Graham at his house. He looks extremely nervous at the door, Amanda asks Graham whether he is alone, and he says he isnt. Before she can turn away, Olivia and Sophie come to the door, revealing that they are Grahams young daughters. Amanda is shocked and asks Graham in a whisper if he is married. He says no, because his wife died two years ago. Amanda asks him why he did not reveal the existence of his daughters to her during their sharing at lunch. Graham explains that he doesnt usually tell women about them because he doesnt know how to date women while also being a dad. Amanda fits in beautifully with Sophie and Olivia, making Graham reconsider his decision to keep their relationship strictly casual.

Iris finds out that the Screenwriters Guild wants to throw a grand celebration in Arthurs honor, but he is reluctant to go for several reasons, chief among them being that he cannot walk without the assistance of his walker, and does not want to embarrass himself on stage. Iris encourages him to go because its a real honor, and promises to help him prepare to attend the award function with her.  She undertakes an exercise program with him, determined to help him walk without his walker. During these days Jasper contacts her several times to get her to help with the book he is writing. She agrees to look over his pages, but ends up having so much fun that she doesnt have time.
 Santa Fe on an extended film shoot and would not be in L.A. for this Christmas Eve. He runs out of the store to find out the truth and realizes that she has been cheating on him. Iris and Miles return to Amandas house, where they discuss their tendencies to fall for people that they know are wrong for them, and they grow closer for it. They are now both single, and begin to spend more time together. But one day while Miles and Iris are eating lunch together, Maggie calls Miles wanting to see him. Miles leaves to see her, but promises to come to Arthur’s award ceremony that night. Iris goes back to Amandas, where she is shocked to find Jasper having flown in from England. Iris is touched at first, but then asks if he is still engaged. When he responds that he is, she finally realises he is no good for her and breaks up with him for good. Meanwhile, Maggie tries in vain to convince Miles to forgive her. He realises that Maggie was never meant for him and he can not trust her anymore. He breaks up with her and rushes to attend Arthur’s award ceremony.

Iris and Arthur arrive at the ceremony and are very surprised to find that despite Arthurs fears that not many people would attend the hall is actually filled with people all standing and applauding his achievements. That, plus the song that Miles wrote especially for Arthur for this moment, gives him the confidence to walk onto the stage unassisted. Miles arrives and asks Iris for a date on New Years Eve. Iris responds that she will be back in London by then; Miles replies "I have never been to London." Iris responds that she would love to spend the evening with Miles. Meanwhile, Graham confesses his love for Amanda on the night before she is scheduled to depart. Amanda insists it is over as she is certain a long-distance relationship would never work. On her way to the airport, she cries for the first time since she was fifteen years old, and returns to the house to find Graham crying. Amanda tells him that she has decided to stay until New Years Eve with him and they embrace. Iris and Miles celebrate the New Year with Amanda and Graham and his daughters, Sophie and Olivia, enjoying the evening laughing and dancing together.

==Cast==
{{multiple image
   | direction = vertical
   | width     = 175
   | footer    = Top to bottom: Cameron Diaz, Kate Winslet, and Jack Black star in the film as, respectively, Amanda Woods, Iris Simpkins, and Miles Dumont.
   | image1    = Cameron Diaz WE 2012 Shankbone 4.JPG
   | alt1      =
   | caption1  =
   | image2    = KateWinsletByAndreaRaffin2011.jpg
   | alt2      =
   | caption2  =
   | image3    = Jack Black 2 2011.jpg
   | alt3      =
   | caption3  =
}}
* Cameron Diaz as Amanda Woods:
:Amanda is the owner of a prospering   on set: "There were a few scenes that were written on the page but then Nancy and I fooled around with them a bit. We didn’t want to take it   too broad. We wanted it to be believable, so we included realistic moments," she said. 

* Kate Winslet as Iris Simpkins: Jude   screwball comedies The Philadelphia Story, to study the dialogues and performances. 

* Jude Law as Graham Simpkins: womanizing single father forced to raise his two daughters by himself after his wifes death.  Law accepted the role as he was interested in playing a type of character that he had never played on film before.    After his appearances in a string of period dramas and science fiction films in the early to mid-2000s, Law found it tricky to approach the contemporary role of Graham. Like Winslet, the actor stated, he felt more vulnerable about playing a character who fitted his own look and did not require an accent, a costume or a relocation.  Meyers, who was not immediately sure if Law was going to fit into the genre and whose character evolved more during the writing than the others, decided to cast him after meeting in which they we went through the script together.  In preparing for his role, Meyers sent him a collection of Clark Gable movies to prepare the performance that she wanted in The Holiday. 

* Jack Black as Miles Dumont: musical comedy film School of Rock (2003).    On his cast, Meyers commented that "when I was thinking of this movie I thought he was someone I would like to write a part for and Im aware hes not Clark Gable, hes not tall dark and handsome, but hes adorable, hes lovable. Its my way of saying this is the right kind of guy, this is what most guys look like if theyre lucky, hes so adorable, and why not?"  Cast against type, Black felt "flattered   a little bit nervous" about Meyers approach to star in a romantic comedy|rom-com,    though he eventually agreed to sign on upon learning that he would play opposite Winslet.  While he felt, it was difficult to find the adorable side in his role, Black appreciated Miles relationship with music: I could relate to that Miles was a film composer and I just got done composing my music for my score. So I knew about that world."  

* Eli Wallach as Arthur Abbott:
:Arthur is Amandas neighbor, a famous screenwriter from the Golden Age of Hollywood whom Iris befriends.  Wallach was 90-years-old when The Holiday was filmed. Meyers found him so animated and energetic on the set, that she had to remind him several times during filming to slow down, move more slowly, and act more like an older man. 

Shannyn Sossamon appears as Maggie, Miles girlfriend and aspiring actress, while Edward Burns plays as Ethan Ebbers, Amandas boyfriend; Rufus Sewell portrays Jasper Bloom, Iris on-and-off affair. Extended Simpkins family include Miffy Englefield and Emma Pritchard as Sophie and Olivia, Grahams daughters, respectively. The film also cast Bill Macy as Ernie and Shelley Berman as Norman, friends of Arthur, as well as Kathryn Hahn as Bristol and John Krasinski as Ben, Amandas employees. Jon Prescott appears as Maggies short-time affair.
 cameo as The Parent Trap (1998), and James Franco, a friend of Meyers, make uncredited appearances in the trailer of the fictional movie Deception, which Amanda and her team finish at the beginning of The Holiday. 

==Production==

===Location===
  Brentwood area Westside of San Marino, Culver City. Silver Lake area, near downtown. 

The UK part of the film was partially shot in Godalming and Shere, a town and village in the county of Surrey in South East England that date back to the 11th century. 

==Reception==
===Box office===
The film opened at number three on the United States box office, raking in $12,778,913 in the weekend of December 8, 2006.  Altogether, The Holiday made $63 million at the North American domestic box office, and $142 million at the international box office.    The film grossed at total $205,135,175, worldwide, against a production budget of $85 million, and an estimated advertising spend of $34 million.  The Holiday became the twelfth highest-grossing film of the 2000s to be helmed by a female director. 

===Critical response===
The Holiday received mixed reviews by critics. The review aggregator website Rotten Tomatoes reported that 47% of critics gave the film a positive rating, based on 135 reviews, with an average score of 5.5/10. Its consensus states "The Holiday, while sweet and somewhat touching, lacks any surprises and eventually overstays its welcome."  On Metacritic, which uses a normalized rating system, the film holds a 52/100 rating, based on 31 reviews, indicating "mixed or average reviews". 

In her review for USA Today, Claudia Puig found that The Holiday "is a rare chick flick/romantic comedy that, despite its overt sentimentality and fairy-tale premise, doesnt feel cloyingly sweet." She felt that "much of the credit goes to inspired casting and the actors chemistry."  Carina Chocano, writing for the Los Angeles Times noted that "like a magic trick in reverse, The Holiday reveals the mechanics of the formula while trying to keep up the illusion. She complimented Winslet and Laws performances, but was critical toward Diaz, who she felt "strikes the off-note, but then you tend to think its not her fault."  Rex Reed from The New York Observer noted that "at least 90 percent of The Holiday is a stocking-stuffer from Tiffany’s   so loaded with charm that it makes you glow all over and puts a smile in your heart." While he felt that the final 15 minutes of film "diminish a lot of the film’s good intentions," he added that Meyers "created some hearth-cozy situations, written some movie-parody zingers, and provided Eli Wallach with his best role in years."  

Somewhat less enthusiastic, Owen Gleiberman of Entertainment Weekly graded the film with a B– rating, summing it as a "cookie-cutter chick flick." He concluded that "its a self-consciously old-fashioned premise, with too much sub-Bridget Jones dithering, but Nancy Meyers dialogue has a perky synthetic sheen."  Justin Chang from Variety (magazine)|Variety wrote that while "Meyers’ characters tend to be more thoughtful and self-aware (or at least more self-conscious) than most   this overlong film isn’t nearly as smart as it would like to appear, and it willingly succumbs to the very rom-com cliches it pretends to subvert." He added, that "in a spirited cast   the Brits easily outshine their Yank counterparts. Winslet weeps and moans without sacrificing her radiance or aud’s sympathy, while the marginally less teary-eyed Law effortlessly piles on the charm in a role that will have some amusing resonances for tabloid readers."  Ruthe Stein of the San Francisco Chronicle remarked that the film was "the most love-centric movie since Love Actually." She felt that The Holiday "has charming moments and a hopeful message for despondent singles, but it lacks the emotional resonance of Meyers Somethings Gotta Give (film)|Somethings Gotta Give and the zaniness of What Women Want. Clocking in at two hours and 16 minutes, Holiday is ridiculously long for a romantic comedy and would benefit from losing at least a half hour." 

===Accolades===
{| class="wikitable" style="font-size: 95%;"
|+List of awards and nominations
! Award
! Category
! Recipients and nominees
! Result
|- ALMA Awards   
| Outstanding Actress - Motion Picture
| Cameron Diaz
|  
|- Irish Film & Television Awards 
| Best International Actress (Peoples Choice)
| Kate Winslet
|  
|- NRJ Ciné Awards 
| Meilleur baiser ("Best Kiss")
| Cameron Diaz   Jude Law
|  
|- Teen Choice Awards 
| Choice Movie: Chick Flick
|  
|  
|-
| Choice Movie: Hissy Fit
| Cameron Diaz 
|  
|}

==Soundtrack==
 
{{Infobox album   
| Name        = The Holiday
| Type        = Soundtrack
| Artist      = Hans Zimmer, Heitor Pereira, various artists
| Border      = yes
| Cover       = TheHolidaySoundtrack.jpg
| Released    = January 9, 2007
| Recorded    =
| Genre       = Film soundtrack
| Length      = 48:12
| Label       = Varèse Sarabande
| Producer    = Hans Zimmer, Nancy Meyers, Robert Townson
| Reviews     =
| Last album  =
| This album  =
| Next album  =
}}
The official soundtrack contains music by various artists, Heitor Pereira and Hans Zimmer, and is released on the Varèse Sarabande label.

 
# "Maestro" by Hans Zimmer - 3:53
# "Iris and Jasper" by Hans Zimmer and Lorne Balfe - 3:24
# "Kayak for One" by Ryeland Allison - 1:30
# "Zero" by Hans Zimmer and Atli Örvarsson - 2:44
# "Dream Kitchen" by Hans Zimmer and Henry Jackman - 1:35
# "Separate Vacations" by Hans Zimmer, Lorne Balfe and Imogen Heap - 1:47
# "Anything Can Happen" by Hans Zimmer and Heitor Pereira - 0:48
# "Light My Fire" by Hans Zimmer - 1:14
# "Definitely Unexpected" by Hans Zimmer and Lorne Balfe - 3:34
# "If I Wanted To Call You" by Hans Zimmer and Atli Örvarsson - 1:50
# "Roadside Rhapsody" by Hans Zimmer and Henry Jackman - 1:39
# "Busy Guy" by Hans Zimmer and Henry Jackman - 1:28
# "For Nancy" by Hans Zimmer, Atli Orvarsson and Lorne Balfe - 1:27
# "Its Complicated" by Hans Zimmer and Imogen Heap - 1:00
# "Kiss Goodbye" by Heitor Pereira and Herb Alpert - 2:33
# "Verso E Prosa" by Heitor Pereira - 1:59
# "Meu Passado" by Hans Zimmer, Henry Jackman and Lorne Balfe - 1:25
# "The Cowch" by Hans Zimmer, Heitor Pereira, Lorne Balfe and Imogen Heap - 2:42
# "Three Musketeers" by Hans Zimmer, Heitor Pereira, Lorne Balfe and Imogen Heap - 2:44
# "Christmas Surprise" by Hans Zimmer and Lorne Balfe - 2:32
# "Gumption" by Hans Zimmer, Atli Orvarsson and Henry Jackman - 3:45
# "Cry" by Hans Zimmer, Lorne Balfe and Heitor Pereira - 2:39
# "Its a Shame" by the Spinners
# "You Send Me" by Aretha Franklin

* In the video rental store, Miles (Jack Black) sings the theme tune of Driving Miss Daisy by "Hans". Hans Zimmer also composed and produced the score for The Holiday. Jack Black later spoofed the movie in Be Kind Rewind.
* According to a radio interview on BBC Radio 1, the song "Kill the Director" by The Wombats was written about this film. From the lyrics "this is no Bridget Jones" and according to the radio interview, they hated the film, and hence decided to write a song about it. 

==See also== Tara Road
* Meet cute
* Home Exchange

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 