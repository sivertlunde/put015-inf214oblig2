Old Gringo
{{Infobox film 
| name           = Old Gringo
| image          =Old gringo.jpg
| caption        = Theatrical Poster
| writer         = Screenplay:  
| starring       = Jane Fonda Gregory Peck Jimmy Smits | director       = Luis Puenzo
| producer       = Executive Producer: David Wisnievitz Producer: Lois Bonfiglio
| cinematography = Félix Monti
| editing        = William M. Anderson Glenn Farr Juan Carlos Macías
| music          = Lee Holdridge
| distributor    = Columbia Pictures
| released       =  
| runtime        = 119 minutes
| country        = United States
| language       = English
| budget         = $27 million
| gross          = $3,574,256 
|}} 1989 film directed by Luis Puenzo and co-written with Aída Bortnik, based on the novel Gringo Viejo by Mexican novelist Carlos Fuentes. 

The film stars Jane Fonda, Gregory Peck, and Jimmy Smits.

The film was screened out of competition at the 1989 Cannes Film Festival.   

==Plot==
When American schoolteacher Harriet Winslow (Jane Fonda) goes to Mexico to work as a governess for the Miranda family, she becomes caught up in the Mexican revolution.
 Chihuahua actually belong to a unit of Pancho Villas army. They use her luggage to smuggle weapons to the servants at the Miranda hacienda, who in turn aid the attacking revolutionary army of General Tomas Arroyo (Jimmy Smits).

During the attack, a sardonic "Old Gringo," who is really American author Ambrose Bierce (Gregory Peck), joins in the fighting on the side of the revolution, operating the track switch that ensures a railroad flatcar laden with explosives reaches its target.

After the Miranda hacienda is taken, Winslow becomes romantically smitten alternately with Bierce and Arroyo. Bierce has come to Mexico to die in anonymity, feeling that his fifty years as a writer have won him praise only for his style, not for the truth that hes tried to tell. Arroyo, by contrast, has returned to the hacienda where he was born. His father was actually a Miranda who had raped his peasant mother. Later, in his youth, Arroyo murdered his father.

While his army enjoys luxuries they have never known on the war-damaged but palatial Miranda estate, Arroyo becomes obsessed with his past and, transfixed by childhood memories of his family buried there, fails to move his army when ordered by Villa. To snap Arroyo out of his fixation, thus averting a mutiny of his officers, Bierce burns papers that the illiterate Arroyo considers sacred—papers that supposedly entitle the peasants to the hacienda land. But Arroyo responds by shooting Bierce in the back, killing him. Bierce dies in Winslows arms.

Winslow later goes to the U.S. embassy in Mexico to claim Bierces body and bring it back to the United States, saying that it is that of her long-lost father. This puts Villa in a predicament because a U.S. citizen was murdered by one of his generals. So, wishing to avoid American meddling in the revolution, he has Winslow sign a statement that her father had joined the revolution and was executed for disobeying orders, as was General Arroyo who had shot him, and that she witnessed both executions. She signs the statement, is provided with the coffin bearing Bierces body, and witnesses the execution of Arroyo.

==Reception==
The film received mixed to negative reviews, with a 45% "Freshness" rating at the review aggregator site Rotten Tomatoes.  It was a box-office failure. 

==Cast==
* Jane Fonda as Harriet Winslow
* Gregory Peck as Ambrose Bierce
* Jimmy Smits as Gen. Tomas Arroyo
* Patricio Contreras as Col. Frutos Garcia
* Jenny Gago as La Garduna
* Gabriela Roel as La Luna
* Sergio Calderón as Zacarias
* Guillermo Ríos as Monsalvo
* Jim Metzler as Ron
* Samuel Valadez De La Torre as Consul Saunders
* Anne Pitoniak as Mrs. Winslow
* Pedro Armendáriz Jr. as Pancho Villa
* Pedro Damián as Capt. Ovando

==Awards==
Nominations
*  ; Worst Actress, Jane Fonda; 1990.

==References==
 

==External links==
*  .
*  
*  
*  

 
 
 
 
 
 
 
 