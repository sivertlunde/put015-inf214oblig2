Spring of Life (film)
{{Infobox film
| name           = Spring of Life
| image          = 
| alt            =  
| caption        = Theatrical release poster
| film name      = {{Infobox name module
| original       = Der Lebensborn: Pramen života}}
| director       = Milan Cieslar
| producer       = 
| writer         = Vladimír Körner (novel)
| screenplay     = Milan Cieslar
| starring       = Monika Hilmerová
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = Czech Repubilc
| language       = Czech
| budget         = 
| gross          = 
}}
Spring of Life ( ) is a 2000 Czech film directed by Milan Cieslar. 
== Plot ==
The film depicts a little-known operation of the Nazi SS, started just before the outbreak of World War II.
Grétka  (Monika Hilmerová) has been selected by the Nazis for the Lebensborn. She falls in Love with the Jew Leo (Michał Sieczkowski) who has been hiding there.

== Cast ==
* Monika Hilmerová : Grétka 
* Michał Sieczkowski: Leo
* Johana Tesarová : Klára
* Vilma Cibulková : Waage
* Karel Dobrý : Odillo
* Bronislav Poloczek : Kasuba
* Josef Somr : Teacher 

==External links==
*  

 
 
 

 