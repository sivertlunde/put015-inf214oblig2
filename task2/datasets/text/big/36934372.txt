Greeku Veerudu
 
 

{{Infobox film
| name           = Greeku Veerudu (2013)
| image          = Greeku Veerudu poster.jpg
| alt            =  
| caption        = Film poster
| writer         = Dasarath  
| story          = Dasarath Kona Venkat Gopimohan  
| screenplay     = Hari   M.S. Ravindra
| producer       = D Siva Prasad Reddy Dasarath
| Nagarjuna Akkineni   Nayantara
| music          = S. Thaman
| cinematography = Anil Bandhari
| editing        = Marthand K. Venkatesh
| studio         = Kamakshi Movies
| distributor    = BlueSky   
| released       =  
| runtime        = 155 mins
| country        =   India 
| language       = Telugu
| budget         = 
| gross          = 
}}
 Telugu Family Nagarjuna Akkineni, Nayantara in the lead roles and music composed by S. Thaman. The film recorded as flop at box-office. 
 Tamil as Love Story and in Hindi as America vs India   The films soundtrack and background score were composed by S. Thaman. The cross over cinema received mixed to positive reviews upon release,  and eventually turned out to be an average grosser at box office,   completing a successful 50-day run on 21 June 2013. 

==Plot==
Chandu (Akkineni Nagarjuna|Nagarjuna) is a very selfish, self made and cut throat businessman in New York who runs a successful Event Organizing Agency and is aided by his uncle Sundar (M. S. Narayana), friends Bharath (Bharath Reddy) and Maya (Meera Chopra). Chandu never believes in relationships and family bonding citing them as a mess to discard. He is always involved in romantic activities with many girls sans commitment. Maya loves Chandu but as fate would have it, She marries Kamaraju (Brahmanandam), an aged yet wealthy doctor to rob his wealth. On the advice of Chandu, she applies for divorce and gets the money from him. However she realizes that Chandu dumped her and makes a plan to teach him a lesson. She manipulates the terms and conditions of agreement entered between Chandu and another big businessman PR (Ashish Vidyarthi) regarding organizing a charity show. According to the fraudulent terms since the show was unsuccessful, PR gets every right to recover the whole consignment amount from PR. Chandu lands in financial soup and is desperately in need of Money. Thus he finally decides to visit his grandparents in India who are hoping to meet him since years.

Chandu recollects his past in the due course. Chandus father Sanjay (Sanjay Swaroop) opposed his father Ramachandra Prasad (Kaashinathuni Vishwanath|K. Viswanath) and marries an NRI of his choice only to be expelled from the house. After Chandu was born, Sanjay dies. Ramachandra Prasad visits his daughter-in-law and invites her to come to India. After seeing his Maternal Grandfather insisting his mother to remarry and his mother replying that she cant do it as she cant forget Sanjay. She tells to Chandu to not indulge in any relationships in order to avoid the pain and struggle in them. At the airport, he sees Sandhya (Nayantara), a humble doctor who works as a volunteer in Make A Wish Foundation to fulfill the last wishes of the children who are going to die soon. Being a clever scheme maker and soft talker, Chandu becomes friend with Sandhya who is his co-passenger to India. He later goes to his Grandfathers home and studies the situations there. That family consists of His Grand parents, their two daughters, their Husbands who are employees in the company Ramakrishna Prasad runs and their kids. Both the nephews of Ramakrishna Prasad are incapable persons. To avoid some unexpected situations, Chandu lies that he is married. He plans to get 250&nbsp;million from his fathers trust and is successful. But to transfer such amount proofs should be enclosed that Chandu is married as Ramakrishna Prasad wishes to make Chandus wife as one of the trustees of the trust.

To Chandus rescue, Sandhya visits their house to meet Chandu in a friendly meeting. However the photos of both in the airport make the family misunderstand that Sandhya is Chandus wife. Chandu takes her to a lonely room and explains the situation. He asks Sandhya to act as his wife in order to save hi Grandparents life and in turn promises that he would sponsor the three kids who are going to die and are aspiring to make their last wishes come true by Make A Wish Foundation. He also adds that he loved a girl named Maya in New York to avoid further possible complications with Sandhya. Sandhya who is in search of sponsors for the kids accepts his request as it is a noble intention. In the mean time certain incidents Chandu has a change of heart and starts believing in family bonding and relationships and falls in love with Sandhya as he believed that she was the main reason for this change. A scheduled, the fake marriage of Chandu and Sandhya is held by Ramakrishna Prasad and family for the transfer of money and appointment of Sandhya as the trustee. On the day of leaving back to New York, Chandu feels guilty and tells the whole truth to Ramakrishna Prasad including the fact that Sandhya is not his wife. He asks Ramakrishna Prasad to forgive him and leaves with Sandhya. Ramakrishna Prasad is very happy as he feels that the time when Chandu asked him to forgive him, it was the time when he heart fully accepted him albeit their tragic past. Sandhya too leaves back at the New York airport not giving any chance to Chandu to propose her.

In New York, Sandhya plans a secret meeting of Chandu with Maya to re unite them as he told that Maya hated him because of some misunderstandings during their stay in India. There Maya reveals the true colors of Chandu to Sandhya which makes Chandu earn the wrath of Sandhya. Ramakrishna Prasad comes to New York and gives him the necessary amount to clear his debts thus helping him sustain in his business. He advises Chandu to prove Sandhya in their next re union during the trip of the children by Make A Wish Foundation that he is a changed man now. He accompanies Sandhya and the kids and along with them, Kamaraju re enters the scene as the doctor of the kids. Chandu leaves no stone unturned to prove that he is a changed man now and is sincerely in love which go vain in front of Sandhya. In order to fulfill the kids lat wishes, he strives a lot. After fulfilling the second of the 3 wishes, Chandu invites Sandhya for a dinner one day to talk to her which co incidentally marks her birthday. PR arrives to the spot to celebrate Sandhyas birthday but is infuriated after listening to the rambling story between Chandu and Sandhya. As PR is her elder brother, Sandhya promises to marry the guy he approves and Chandu promises that he himself will never disturb her after the third wish is fulfilled. He risks his life to fulfill the last wish which makes Sandhya believe that Chandu is changed.

To the duos surprise, PR welcomes them at the airport with his associate Subrahmanyam (Vennela Kishore) with whom he arranges marriage with Sandhya on account of his enmity with Chandu. This makes Chandu much grieved and finally goes back to attend the "Seetha Ramula Kalyanam" event held at the temple in his village in India. Though he promised his family members that he would attend along with Sandhya, the truth is only known to Chandu, Ramakrishna Prasad and Sundar, who stays back in that village after he falls in love with a local middle aged woman. To their surprise, Sandhya arrive with her family and Subrahmanyams family. She promises Chandu to accompany him to the Kalyanam event after PR permits her to go after much resent. Kamaraju lands there unexpectedly and he is the brother-in-law of Subrahmanyam. After some comic situations, all of them unite at the Temple where Chandu tells the truth to his entire family. The family is pleased by him and accepts him, Sandhya accepts his love, PR and Subrahmanyam forego the previous plans and let Chandu marry Sandhya. The film ends with Chandu and Sandhya participating happily together as a couple in the Kalyanam event to which their families and rest of the village people attend.

==Cast==
{{columns-list|3| Nagarjuna Akkineni as Chandu
* Nayantara as Sandhya
* Meera Chopra as Maya
* Kaashinathuni Vishwanath|K. Viswanath as Ramakrishna Prasad
* Ashish Vidyarthi as PR
* Brahmanandam as Kamaraju
* Kota Srinivasa Rao Ali
* M. S. Narayana
* Dharmavarapu Subramanyam Venu Madhav 
* Nagineedu 
* Sarath Babu as Sarath 
* Nagendra Babu|K.Nagababu 
* Kasi Viswanath 
* Raghu Babu
* Bharath Reddy (actor)|Dr.Bharath Reddy 
* Supreth 
* Vennela Kishore as Subrahmanyam
* Sanjay Swaroop 
* Benerjee 
* Thagubothu Ramesh
* Giridhar 
* Pradeep 
* Srinivasa Reddy  Ashok Kumar  Jenny
* Geetanjali 
* Sudha  
* Kovai Sarala
* Lahari Tulasi 
* Meena  
* Jaya Lakshmi 
* Jayavani 
* Deepthi Vajpai
* Master Abhi 
* Master Gaurav Naidu 
* Master Rakshith Varma 
* Baby Kaveri
}}

==Soundtrack==
{{Infobox album
| Name        = Greeku Veerudu 
| Tagline     = 
| Type        = film
| Artist      = S. Thaman
| Cover       = 
| Released    = 2013
| Recorded    = 
| Genre       = Soundtrack
| Length      = 25:35
| Label       = Aditya Music
| Producer    = S. Thaman
| Reviews     =
| Last album  = Baadshah (2013 film)|Baadshah   (2013)
| This album  = Greeku Veerudu   (2013)
| Next album  = Gouravam (2013 film)|Gouravam   (2013)
}}
The music was composed by S. Thaman. Music released on ADITYA Music Company.
{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 25:35
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = yes
| music_credits =

| title1  = I Hate Love Storys Krishna Chaitanya
| extra1  = Ranjith (singer)|Ranjith, Naveen Madhav
| length1 = 3:36

| title2  = Ne Vinnadi Nijamena
| lyrics2 = Balaji Ranjith
| length2 = 3:31

| title3  = O Naadu Washington Sahithi
| SP Balu
| length3 = 4:51

| title4  = Ee Parikshalo Sirivennela Seetarama Sastry Vandhana
| length4 = 3:27

| title5  = Yevvaru Lerani
| lyrics5 = Vanamali
| extra5  = Mallikarjun Rao
| length5 = 4:39

| title6  = Osina Bangaram
| lyrics6 = Ananta Sriram
| extra6  = Vedala Hemachandra|Hemachandra,MM Manasi
| length6 = 3:26

| title7  = Maro Janme
| lyrics7 = Balaji
| extra7  = Rahul Nambiar
| length7 = 2:26
}}

==Production==

===Casting=== Nagarjuna who plays an NRI, Meera Chopra is playing one of the key roles.  Nayantara has been roped in as Nagarjunas heroine. 

===Filming=== Nagarjuna and Nagarjuna and Nayantara. After the Swiss schedule, the unit moved to United States. 

===Marketing===
On 25 January 2013 Greeku Veerudu first look posters and photos were released and got extraordinary response. Photos and working stills were released on 23 February   Greeku veerudu pressmeet was conducted on 26 February 2013,  following public function for Greeku Veerudu audio
 

==Reception==
Greeku Veerudu received positive to mixed reviews from critics, with nearly an average rating of 3.25/5 from most of the sites at reviewbol.com.  123telugu.com gave a review of rating 3/5 stating "Greeku Veerudu is a clean family drama. The film has some good emotional moments and fine performances from Nagarjuna, Nayanathara and Vishwanadh. This is a movie that will appeal more to older sections of the audience."  idlebrain.com gave a review of rating 3/5 stating "The title of the film might fool you. Greeku Veerudu title represents heroism. But its a film with a lot of family orientation. And there is some comedy too. Nagarjunas look is the major asset of the film."  Oneindia Entertainment gave a review of rating 3/5 stating "In nutshell, Greeku Veerudu is a good family entertainer, which has best performances from Nagarjuna, and Nayantara and sound production values. Although it has some drawbacks, the movie is interesting and enjoyable. Do watch it with your family."  SuperGoodMovies gave a review of rating 2.75/5 stating "Greeku Veerudu is a complete family show with a bit of comedy and a very less percentage of mass elements. It is just an above Average movie."  APHerald.com gave a review of rating 2/5 stating "Greeku Veerudu is slow, narrated film, its a one-time watcher. " 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 