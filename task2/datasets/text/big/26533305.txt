Víctimas del Pecado
{{Infobox film
  | name = Vìctimas del Pecado
  | image = VictimasDelPecado.jpg
  | caption =
  | director = Emilio Fernández
  | producer = Guillermo Calderòn Pedro Calderòn
  | writer = Emilio Fernández Mauricio Magdaleno
  | starring = Ninón Sevilla Rodolfo Acosta Tito Junco Rita Montaner
  | music = Antonio Díaz Conde
  | cinematography = Gabriel Figueroa
  | editing = Gloria Schoemann
  | distributor = Cinematográfica Calderón SA
  | released = February 2, 1951
  | runtime = 90 minutes
  | country = México
  | language = Spanish
  | budget =
    }}

Víctimas del Pecado (Victims of the Sin) is a 1951 Mexican drama film directed by Emilio Fernández and starring Ninón Sevilla.    
 mambo numbers by Pérez Prado and Pedro Vargas".   The film was also released as Hells Kitchen.    

==Plot==
Violeta (Ninón Sevilla), a Cuban dancer from the Cabaret Changó, rescues an infant from a garbage can in Mexico Citys redlight district. She decides to raise the baby, and her decision places her at odds with Rodolfo (Rodolfo Acosta), the Cabaret Changó club owner. Rival club owner Santiago (Tito Junco), falls in love with Violeta and offers her his help. Tragedy takes place when Rodolfo (Rodolfo Acosta) kills Santiago and after Rodolfo is killed by Violeta in turn.

==Cast==
* Ninón Sevilla as Violeta
* Tito Junco as Santiago
* Rodolfo Acosta as Rodolfo
* Rita Montaner as Rita
* Arturo Soto Rangel as Director de prisión
* Francisco Reiguera as Don Gonzalo
* Lupe Carriles as Doña Longina
* Ismael Pérez as Juanito
* Margarita Ceballos as Rosa

==Recognition==
===Awards and nominations===
* 1952, Mexican Academy of Film Ariel Awards nomination for Best Cinematography for Gabriel Figueroa   
* 1952, Mexican Academy of Film Ariel Awards nomination for Best Child Actor for Ismael Pérez 

===Reception===
Twitch Film felt the film would be a perfect match to be double-billed with Josef von Sternbergs Blonde Venus.  

Without imagining that were on the verge of securing international fame with the film cabaret series performed by Ninon Sevilla, the Calderon Brothers contracted to Emilio Fernández to direct this musical melodrama that would be more intense and exacerbated that Salón Mexico (1948), the previous raid of " El Indio" into the cabaret environments of the Mexican capital.

As in Salon Mexico, Victimas del Pecado demonstrated the joy of the director in a very particular direct scenes that take place in the cabaret. Also, the director does not hide his love of uplifting moral, nor can avoid some moments of unintentional comedy as one in which the "Pachuco" Rodolfo Acosta shows his ability to speak more than one language while teaching him how to walk with style to a French prostitute. Despite its considerable shortcomings, Victimas del pecado has kept pace in the filmography of "El Indio" Fernández and the passage of time has not been treated so poorly. Figueroas photography continues to be splendid and as representative of the filmography of Ninon Sevilla is important to note that the film won an unprecedented success in France and Belgium, where he was known as Quartier interdit (Forbidden Neighborhood or Hells Kitchen). 

Ninon was unsurpassed as the flower that falls in the mud, the star that travels in rotten environments, even though she retains a heart of gold for delivery to a pariah, a hero or a stranger which a past  unknown. Could be brave and help to her man, and betray to the gangster that had knocked on his networks. It could also infatuated with someone of the opposite sex, amazed at her honesty, and the love he feels for his adopted son. All this happens in Victimas del Pecado.   

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 