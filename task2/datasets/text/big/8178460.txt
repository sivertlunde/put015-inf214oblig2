Maria Marten, or The Murder in the Red Barn
{{Infobox film
| name           = Maria Marten, or The Murder in the Red Barn
| image_size     =
| image	         = Maria Marten, or The Murder in the Red Barn FilmPoster.jpeg
| caption        =
| director       = Milton Rosmer George King (producer)
| writer         = Randall Faye (writer)
| narrator       =
| starring       = Leo T. Croke
| music          = Leo T. Croke
| cinematography = George Stretton Charles Saunders
| distributor    =
| released       = 18 December 1923 
| runtime        = 70 minutes 58 minutes (Ontario, Canada)
| country        = UK
| language       = french
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
 1935 British film melodrama film starring Tod Slaughter and Eric Portman. It was directed by Milton Rosmer, whose most famous film was the English-language version of Emil and the Detectives that same year. It is based on the true story of the 1827 Red Barn Murder. The film is also known as Murder in the Red Barn (short UK title).
 George King. Slaughter gives a full-throated over-the-top performance in a calculatedly melodramatic style, encouraging the audience to vicariously share in his villainy; this approach became his trademark and gives his films a cult status of their own peculiar kind.

==Plot summary==
 
The unfortunately-surnamed actor Tod Slaughter, in his film debut, plays William Corderes, the lover who seduces then murders innocent country maiden Maria Marten (Sophie Stewart) in the red barn before burying her body beneath the barn floor.

== Cast ==
*Tod Slaughter as Squire William Corder
*Sophie Stewart as Maria Marten
*D. J. Williams (actor)|D. J. Williams as Farmer Thomas Marten
*Eric Portman as Carlos, the gypsy
*Clare Greet as Mrs. Marten
*Gerard Tyrell as Timothy Winterbottom
*Ann Trevor as Nan, the maid
*Stella Rho as Gypsey Crone
*Dennis Hoey as Gambling Winner
*Quentin McPhearson as Matthew Sennett
*Antonia Brough as Maud Sennett
*Noel Dainton as Officer Steele of the Bow Street Runners

== Soundtrack ==
 

== External links ==
* 
* 
 

 

 
 
 
 
 
 
 
 
 
 


 
 