Bhaji in Problem
{{Infobox film
| name = Bhaji in Problem
| image = Bhaji in Problem.jpg
| alt =
| caption =
| director = Smeep Kang
| producer = Ashvini Yardi Akshay Kumar
| writer =
| starring = Gippy Grewal Gurpreet Ghuggi Ragini Khanna Misha Bajwa B.N. Sharma Khushboo Grewal
| music = Jatender Shah & Surinder Rattan
| cinematography =
| editing =
| studio = Viacom 18
| distributor = Grazing Goat Pictures Pvt. Ltd   Round Square Production
| released =  
| runtime =
| country = India
| language = Punjabi
| budget =   5 crores   
| gross =   17 crores 
}}
 Punjabi comedy film directed by Smeep Kang, who had earlier directed films like Carry on Jatta and Lucky Di Unlucky Story, both of which featured Gippy Grewal in the lead role. Grewal also appears in this film, along with an ensemble cast including Ragini Khanna, Gurpreet Ghuggi, B.N. Sharma, Karamjit Anmol, Japji Khaira, Khushboo Grewal and Misha Bajwa amongst others. The film is produced by Ashvini Yardi, and Bollywood actor Akshay Kumar who also appears in the film shortly, enacting as a lookalike of himself.

Besides from Akshay Kumar, the film also features actor Om Puri and cricketer Harbhajan Singh. Bhaji in Problem was released on 15 November 2013. It has released in over 600 screens worldwide. Bhaji in Problem was declared "superhit" at the Indian box office.


==Synopsis==
The movie centres around Sundeep Cheema (Gurpreet Ghuggi), a man married to two women. Both of his wives, Anu (Misha Bajwa) and Jasmeet (Khushboo Grewal), are unaware of the existence of the other and believe that Cheema has business-related reasons that he has to be away from them half of the time. While Sundeep is leading a happy life, the balance of his duality is threatened when Jeeta (Gippy Grewal) comes into his life. Jeeta falls head over heels for Preet (Ragini Khanna), a match that Sundeep is firmly against. Sundeep is aware of Jeetas notorious past, and Jeeta is privy to Sundeeps dual existence, leading to a comedy of errors in which both sides know the deepest and the most intimate secrets of the other. This leads to a battle of wits and charm wherein two friends encounter further problems. 

==References==
 

==External links==
*  
*  
*  

==Awards and nominates==

2014 PTC Punjabi Film Awards

Won

PTC Punjabi Film Award for Best Supporting Actor - Gurpreet Ghuggi

Nominated

PTC Punjabi Film Award for Best Dialogues - Naresh Kathooria & Gurpreet Ghuggi

PTC Punjabi Film Award for Best Supporting Actress - Khushboo Grewal

 