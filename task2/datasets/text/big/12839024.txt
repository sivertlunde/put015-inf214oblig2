Ang Cute Ng Ina Mo
{{Infobox film
| name           = Ang Cute Ng Ina Mo
| image          = Angcute.jpg
| image size     =
| caption        = Poster
| director       = Wenn V. Deramas
| producer       = Jocelyn D. Bracamonte Veronique Del Rosario-Corpus Vicente del Rosario III Vic del Rosario Jr. Tess V. Fuentes Kara Kintanar and June Rufino for Viva Films Charo Santos-Concio and Malou N. Santos for Star Cinema
| writer         = Christian Jan D. Santos Mel Mendoza-Del Rosario
| narrator       =
| starring       = Ai-Ai delas Alas   Anne Curtis   Luis Manzano   Eugene Domingo   John Lapus   Nikki Bacolod   Vice Ganda
| music          = Vincent de Jesus
| cinematography = Sherman So
| editing        = Marya Ignacio
| studio         = Star Cinema VIVA Films
| distributor    = Star Cinema
| released       =  
| runtime        = 102 minutes
| gross          = 110 million Tagalog Australian English
| budget         = P74.8 million
}} 2007 Cinema Filipino Comedy comedy Drama Viva split from ABS-CBN in 2001.

The Movie also spanned the title of being the Mega Blockbuster Movie of 2007 Second to Paano Kita Iibigin starring Piolo Pascual and Regine Velasquez.

==Plot==
The story revolves around Georgia, a Filipina patis (fish sauce) manufacturer and a mother who is finally reunited with her long-lost Filipino Australian daughter after 20 years of separation.

Christine can think of 101 reasons why her Australian dad must not marry her mom. The number one reason is that Georgia, her mother, disappeared from her life twenty years ago and never came back.

Now Christine is confused: Why does her father want to marry her mom, who had caused them so much heartache?

When Christine goes to her mothers town in Malabon, what starts out as an investigation turns out to be a hilarious yet touching journey for both mother and daughter. Christines outrageous attempts to expose her mothers flaws are all foiled, as Georgia also comes up with hysterical schemes to win her daughters love. Meanwhile, things get more riotous when Nanny Ninonus blind heart falls for Junjun, who is obviously gay and definitely shows no interest liking her back.

Again and again, Christine is frustrated as no matter how she tries to harden her own heart, she cannot help but fall in love with her mothers town and its people, especially Val, Georgias adopted son. Through Vals eyes, she reluctantly begins to see what it means to have a real mother.

Despite his discovery of Christines feelings of attraction towards Val, however, Val is suspicious of Christines intentions. What happens if here plot to discredit Georgia? Will Georgia have a place in Christines life? Can Christine learn to accept Georgia as her mother, even if she discovers a dark secret in her mothers past?

==Cast==
*Ai Ai de las Alas as Georgia Quizon vda de Goloid - Outback
*Anne Curtis as Christine Outback
*Eugene Domingo as Nanny Ninonu John "Sweet" Lapus as Junjun
*Luis Manzano as Val Quizon
*DJ Durano as Delfin
*Richard Everley as Jack Outback Vince Saldaña as Jojo
*Nikki Bacolod as Lisa
*Lou Veloso as Don Emong Goloid
*Makisig Morales as Super Inggo
*Jojit Lorenzo as Hostage Taker
*Shamaine Buencamino as Imelda Marcos
*Pen Medina as Ferdinand Marcos
*Moises Miclat as Gringo Honasan

==Awards and recognitions==
*24th PMPC Star Awards for Movies
*: Movie Actress of the Year - Ai-Ai delas Alas
==External links==
* 

 
 
 
 
 
 
 