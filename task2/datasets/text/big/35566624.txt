Beyond the Hills
 
{{Infobox film
| name           = Beyond the Hills
| image          = Beyond the Hills.png
| caption        = Film poster
| director       = Cristian Mungiu
| producer       = 
| writer         = Cristian Mungiu
| based on       =  
| starring       = Cosmina Stratan Cristina Flutur
| cinematography = Oleg Mutu
| editing        = Mircea Olteanu
| studio         = Mobra Films Sundance Selects   Madman Entertainment  
| released       =  
| runtime        = 155 minutes
| country        = Romania
| language       = Romanian
| budget         = 
}}
 Orthodox convent in Romania. 
 Best Screenplay, Best Actress. Best Foreign Language Oscar at the 85th Academy Awards,     making the January shortlist.   

==Plot==
This is a story of two Romanian orphan girls, Voichita (Cosmina Stratan), who has become a nun in the orthodox church, and her troubled friend Alina (Cristina Flutur), who has been working in Germany but has returned home to visit her friend. In their previous life they were roommates at a childrens home and had shared a physical relationship. It soon becomes clear that Alina wishes for this relationship to continue, however Voichita refuses. Alina falls ill and is hospitalised and Voichita begs the Priest (Valeriu Andriuta) to allow her to stay along with the nuns at the convent. The priest acquiesces to Voichitas demands, on the condition that she will be responsible should anything happen. He insists that Alina remains faithful to God and undertakes confession for her sins. Alina seeks to convince Voichita to break free of orthodox environment and come and join her in Germany. Voichita, for her part, tries to convince Alina to take God into her heart, in the hope that this will finally give her peace. The nuns agree to care for Alina, as they can find no other place for her to stay.

==Cast==
* Cosmina Stratan as Voichiţa
* Cristina Flutur as Alina
* Valeriu Andriuţă as Priest
* Dana Tapalagă as Mother superior
* Cătălina Harabagiu as Antonia
* Gina Ţandură as nun Iustina
* Vica Agache as nun Elisabeta
* Nora Covali as Nun Pahomia
* Dionisie Vitcu as Mr. Valerică

==Production==
The screenplay was inspired by two novels by the writer Tatiana Niculescu Bran, documenting the Tanacu exorcism, in which a young member of a monastery in Moldavia died in 2005 after an exorcism ritual. The film production was made through the directors company, Mobra Films. It also received production support from Belgium and France. It received €273,100&nbsp; from Romanias National Centre for Cinema and €400,000 from Eurimages. Filming took place from November 2011 to February 2012.   

==Release==
  Best Screenplay, Best Actress.   

===Critical response=== Screen Daily wrote from Cannes: "Spare, unadorned and strikingly shot, Cristian Mungius film is an unusual rendering of a Romanian exorcism case and is bound to split both audience and critical opinions, some considering it a major achievement and others blaming it for overlong pretentious sensationalism. But it will certainly not pass unnoticed." 

Sight & Sound film magazine listed the film at #8 on its list of best films of 2012. 

Mark Kermode named it his second best film of 2013 so far (from January to June).

==See also==
* Romanian New Wave
* List of submissions to the 85th Academy Awards for Best Foreign Language Film
* List of Romanian submissions for the Academy Award for Best Foreign Language Film
* List of lesbian, gay, bisexual or transgender-related films of 2012

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 