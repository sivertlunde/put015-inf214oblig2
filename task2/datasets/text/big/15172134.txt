A Good Man in Africa
 
{{Infobox film
| name           = A Good Man in Africa
| image          = A Good Man In Africa.jpg William Boyd
| based on       =   Joanne Whalley-Kilmer Sean Connery John Lithgow Louis Gossett Jr. Diana Rigg
| director       = Bruce Beresford
| music          = John Du Prez Jim Clark
| producer       = John Fiedler Mark Tarlov Bruce Beresford
| studio         = Polar Entertainment Capitol Films South African Breweries Southern Sun
| distributor    = Gramercy Pictures (US) UIP (UK)
| released       =  
| runtime        = 94 min
| country        =USA United Kingdom South Africa English
| preceded_by    =
| followed_by    =
| budget         = $20 million
| gross          = $2,308,390
}} William Boyds 1981 novel A Good Man in Africa (novel)|A Good Man in Africa and directed by Bruce Beresford.   

==Cast==
*Colin Friels as Morgan Leafy
*Sean Connery as  Dr. Alex Murray
*John Lithgow as Arthur Fanshawe
*Diana Rigg as Chloe Fanshawe
*Louis Gossett Jr. as Prof. Sam Adekunle
*Joanne Whalley as Celia Adekunle
*Sarah-Jane Fenton as Priscilla Fanshawe
*Simon Laverie as Dr. Alex Murrays Son
*Maynard Eziashi  as Friday, Leafys Houseman

==Reception==

The movie gained negative criticism.     

==DVD release==

Focus Features released an Amazon.com exclusive DVD of the film on January 11, 2010.

== References ==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 