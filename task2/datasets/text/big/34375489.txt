Kaveri (1955 film)
{{Infobox film
| name = Kaveri காவேரி
| image = 
| director = D. Yoganand
| writer = A. S. A. Samy Padmini Lalitha Lalitha N. S. Krishnan T. A. Madhuram P. S. Veerappa M. N. Nambiar
| producer = Letchumanan Chettiar
| music = G. Ramanathan Viswanathan–Ramamoorthy
| cinematography = M. A. Rehman P. Ramasamy
| editing = V. B. Natarajan
| studio = Krishna Pictures
| distributor = Krishna Pictures
| released = 13 January 1955
| runtime = 16127 feet
| country = India Tamil
| budget = 
}}
 Padmini and Lalitha in the lead roles. The film was released in the year 1955.

==Plot==
A story of two rival kings, played by R. Balasubramaniam and T.E. Krishnamachari, Sivaji Ganesan is the son of the latter. Krishnamachari is under the influence of an ambitious  spiritual guru (Nambiar) who manipulates the king for his own ends. The prince works hard to prevent him from achieving his designs. He moves around the kingdom incognito fighting for people’s rights. During this time he meets and falls in love with a dancer (Padmini). The rival king’s daughter Lalitha is also in love with him... The wily advisor (Veerappa) of the other king tries to work out things in his favour and marry the dancer. After many twists and turns, the villains are exposed and the princess sacrifices her love in favour of the dancer.

==Cast==
*Sivaji Ganesan Padmini  Lalitha 
*N. S. Krishnan 
*T. A. Madhuram 
*P. S. Veerappa  
*M. N. Nambiar

==Crew==
*Producer: Letchumanan Chettiar
*Production Company: Krishna Pictures
*Director: D. Yoganand
*Music: G. Ramanathan & Viswanathan–Ramamoorthy
*Lyrics: Udumalai Narayana Kavi
*Story: A. S. A. Samy
*Screenplay: A. S. A. Samy
*Dialogues: A. S. A. Samy
*Art Direction: Ganga
*Editing: V. B. Natarajan
*Choreography: Valuvur Ramaiyya Pillai, Hiralal, Sohanlal
*Cinematography: M. A. Rehman, P. Ramasamy
*Stunt: Stunt Somu
*Audiography: T. S. Rangasamy
*Costume: A. Nadesan Ragini

==Soundtrack== Playback singers are C. S. Jayaraman, M. L. Vasanthakumari, Jikki, P. Leela & A. G. Rathnamala.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) || Music
|- 
| 1 || Manjal Veyil Maalaiyile Vanna Poongkavile ||  
|- 
| 2 || En Sindhai Noyum Theeruma || Jikki ||  || 03:08
|- 
| 3 || Anbe En Aaruyire Angu Nirpadheno || C. S. Jayaraman & Jikki ||  || 03:58
|- 
| 4 || Maangaai Paalundu Malai Mel || C. S. Jayaraman || ||  02:06
|- 
| 5 || Sandhosham Kollame Saapaadum Illaame || Jikki || || 04:09
|- 
| 6 || Sindhai Arindhu Selva Kumaran || C. S. Jayaraman || || 01:15
|- 
| 7 || Sivakaama Sundhari.... Kanani Endhan Kaadhal Un Arulaal || Jikki || || 03:21
|- 
| 8 || Singaara Regaiyil Kaanudhu ||  
|- 
| 9 || Kaaveri Thanneer Pattaal Kanniyar Meni Thangam || P. Leela & A. G. Rathnamala || Udumalai Narayana Kavi || 
|- 
| 10 || Manadhile Naan Konda || M. L. Vasanthakumari || Udumalai Narayana Kavi || 
|}

==Reception==
Kaveri was a box office success running for 100 days in many centres of the state. 

==References==
*  
*  
 

== External links ==
* 
* 
* 
* 
* 
* 
* 
* 
* 

 
 
 
 
 
 