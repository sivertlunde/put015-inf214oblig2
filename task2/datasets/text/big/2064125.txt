December Boys
 
{{Infobox film
| name           = December Boys
| image          = Decemberboysposter.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Rod Hardy
| producer       = Richard Becker Marc Rosenberg
| story          = Ronald Kinnoch
| based on       =   Jack Thompson Victoria Hill Teresa Palmer
| music          = Carlo Giacco David Connell
| editing        = Dany Cooper
| studio         = 
| distributor    = International:   
| released       =  
| runtime        = 105 minutes
| country        = Australia
| language       = English
| budget         = 
| gross          = $1.2 million 
}}
 Michael Noonan. It was released on 14 September 2007 in the UK and US and 20 September 2007 in Australia.    

==Plot== Roman Catholic outback of Australia   Maps, Misty, Spark and Spit   were all born in the month of December, and for their birthday, they are sent on a holiday to the beach to stay with Mr. and Mrs. McAnsh. While there, they meet Fearless, a man who claims to be the risk motorbike rider in the nearby carnival, and his wife, Teresa. Misty, Spark and Spit instantly become closer to Teresa, but Maps, eldest of the four, is still reluctant to talk to her. He instead finds more fun in spending time with an older teenage girl named Lucy, who had come to the beach to stay with her uncle. He often goes up to a place with strange rocks, and meets her there.

A few days later, the orphans peek through a window in Fearless house to see Teresa undressing, but Misty, being the most religious of the four, throws a rock at the wall to make them go away. Misty runs back to Mr. McAnshs house and looks through the small opening of a door to see someone in the shower, only to find that it is the sickly  Mrs. McAnsh. They soon discover that she has breast cancer.
 adopting one of the orphans. Excited about the opportunity to finally have parents, he keeps it to himself until he decides to reveal it to a priest who has driven to the beach for the orphans confessions. The other boys realise that he is taking too long, and once he is finished, they force it out of him with the threat of Spit spitting on him while he is pinned to the ground. Misty, Spark and Spit are eager to compete for the love of the seemingly perfect Fearless and Teresa, but Maps is less than excited, even saying to Lucy, "Whats the big deal about parents, anyway?" Maps experiences his first kiss with Lucy, and soon loses his virginity to her in one of the caves of the Remarkable Rocks.

There, she tells him to promise that he will always remember her as his first. The next day, he goes up to the Remarkable Rocks, only to find Lucy is not there. Her uncle tells him that shes left the beach to return to her father, and will not likely be back until next summer. Heartbroken, he goes to the carnival to find Fearless and talk to him, but discovers that he is not a motorbike rider there, and instead cleans up after the animals. Furious that hed lied all along, he finds a painting made by Misty of him as the son of Fearless and Teresa, and destroys it. Misty attacks him and hits him with the fragments of the frame hed put the painting in, and the bond between the four orphans is broken.

Fearless finds Maps in the cave of the Remarkable Rocks, and explains to him what had really happened. It is revealed to that Fearless was formerly a bike rider, and did all of the stunts with Teresa riding on the back of the bike. Then, there was an accident that kept Teresa in the hospital for nearly a year, making her unable to have children. That was the reason they had wanted to adopt one of the orphans.

Maps returns to the beach and finds out from Spark and Spit that Misty has gone into the water, and is drowning. Maps goes after him despite the fact that he cannot swim. Both he and Misty nearly drown. Underwater, they open their eyes to see a vision of the Virgin Mary, possibly meaning that they are dying. Before they can reach out to it, the two boys are grabbed by Fearless and brought back to the shore. Maps and Misty reconcile with each other and the four are friends again.

The next day, the boys are called to Fearless and Teresas house for an announcement. There, they reveal the couple is going to adopt Misty. He takes leave of his friends and he watches on the front porch with Fearless and Teresa as the other three orphans walk away and begin playing on some rocks down the beach. Misty realises that they are his true family, and asks Fearless and Teresa if he can stay with them instead. They accept, and he returns home with the orphans.

Many decades later, Misty, as an old man, drives to the same beach along with the ashes of Maps, who had recently died while working as a priest in Africa helping refugees, and Lucys ring that she gave to Maps on that holiday long ago.  He meets up with Spark and Spit, and they toss the ashes and ring loose into the wind from the hill above the beach, remembering Maps and their time there, with a cheer to "The December Boys."

==Cast==
*Daniel Radcliffe as Maps
* Lee Cormie as Misty
* Christian Byers as Spark
* James Fraser as Spit Jack Thompson as Bandy McAnsh
* Teresa Palmer as Lucy
* Sullivan Stapleton as Fearless
* Victoria Hill as Teresa
* Kris McQuade as Mrs. McAnsh
* Ralph Cotterill as Shellback
* Frank Gallacher as Father Scully

==Production== Radcliffe because he would not leave the United Kingdom, as he wanted to be with his sick grandmother.

==Differences from the book==
{| class="wikitable"
|-
! Points of difference {{cite web  | last =   | first =  | authorlink =  | coauthors =  | year = 2005
 | url = http://www.danradcliffe.com/news/fullnews.php?id=950  | title = December Boys: Book Summary | work = News | publisher = danradcliffe.com | accessdate = 2007-10-16}} 
! Book   
! Film
|-
| Setting 1930s after outbreak of World War II 1960s
|-
| Orphanage St Rodericks St Gregorys
|-
| Characters: Orphans
| There are five orphans in the book: Spark, Maps, Fido, Misty, and Choker (the narrator).
None of the characters in the book smoke.  The time frame sticks to the one period of the summer - that is the boys are not shown as adults.
* Maps is "the sharpest in looks and ways" but is not significantly older or bigger than the others
* Misty - has only one eye
* Spark aspires to be an auctioneer and is "ginger and well-freckled"
* Fido is the smallest.  He catches Henry the fish.
* Choker, the narrator, is the one who overhears the conversation about adoption.  He uses a found picture frame to dream through. 
| There are four orphans: Maps, Sparks, Spit, Misty (often the story is told from Mistys point of view)
* Maps - (called so because of a birth mark on his chest that is shaped like Tasmania) is significantly older than the others
* Spark - (called so because of a story in which he nearly set the orphanage on fire by sticking a knife in a toaster) "is the real fun loving guy of the four. He looks at all the lingerie ads, he smokes, he’s out there looking for fun."  He catches Henry.
* Spit - (called so due to the fact that his father flew a spitfire fighter plane in the war) is "a character who loves himself and thinks that the sun doesn’t rise without his permission! Maps is his idol though: he sees him as a parental figure." 
* Misty - (called so because he is considered a "water works") "Misty is the youngest, but he’s the most mature. He’s the one that says ‘don’t do that, don’t do this’, and he makes his hair nice and tucks his shirt in."  He is the boy who overhears the conversation about adoption.  He has freckles and glasses but apparently has sight in both eyes.  He uses the picture frame to imagine things.
* The boys are also shown as adults remembering that summer holiday together.
|- Mr and Mrs McAnsh
| The visit of the children was paid for by Lady Hodge who paid for their fares and food.   The McAnsh are looking after the children and are partial to drink.  Mrs McAnsh had worked as a bar maid.  Mr McAnsh had been the late Sir Henry Hodges groom in the first world war and later gardener for the Hodges.  Mrs McAnsh retains her health throughout the book.
| A retired Petty Officer and his wife. The McAnshs are the sponsors of the holiday, paying for the children to come and looking after them. Mrs McAnsh falls ill.
|- Fearless and Teresa
| Teresa runs the local shop. She is notable to the children for doing cartwheels and applying lotion to them to protect them from sunburn.  Fearless is the head of a gang working on a city underground railway tunnel. On the weekends he is the beltman of the local lifesaving team.   Fearless and Teresa do not offer to adopt any of the children. 
| Fearless works at the circus. Fearless and Teresa offer to adopt one of the children.
|-
| Socrates
| is a grey horse who kills fish to feed a family of wild cats. 
| is dark brown; cats are not shown.  Several reviewers found the inclusion in the film of the wild stallion as a less than successful overtly literary element. 
|-
| Lucy
| Lucy does not appear in the book nor do any of the events associated with her Darwin with whom Maps has a relationship
|-
| Motorbike Red Indian   Triumph
|- Music
| In the book a character, Fingers Galore, plays the piano - mainly classical works but also  .
| The film features popular music from the 1960s and Anachronism|anachronistically, the 1970s, including Wholl Stop the Rain and Have You Ever Seen the Rain?
|}

==Critical reception==
Review aggregator Rotten Tomatoes reported that 42% of critics gave the film positive reviews, based on 67 reviews.    Metacritics score was 56/100 based on 21 reviews.  In Australia several newspapers remarked that December Boys was a commercial bomb due to its relatively large distribution and advertisement versus its small box office return.

==Box office==
December Boys grossed $633,606 at the box office in Australia. 

==See also==
 
* Cinema of Australia
* South Australian Film Corporation

==References==
 

==External links==
*  
*  

 

 

 
 
 
 
 
 
 
 
 
 
 