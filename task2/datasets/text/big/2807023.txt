Bad Guy (film)
{{Infobox film
| name        = Bad Guy
| image       = Bad_Guy_Poster.jpg
| caption     = Bad Guy movie poster
| director    = Kim Ki-duk
| writer      = Kim Ki-duk
| starring    = Cho Jae-hyun Seo Won
| producer    = Lee Seung-jae
| cinematography = Hwang Chul-hyun
| editing     = Ham Sung-won
| music       = Park Ho-jun
| distributor = CJ Entertainment
| released    =  
| runtime     = 100 minutes
| country     = South Korea
| language    = Korean
| budget      =  
| film name   = {{Film name hangul      =     hanja       = 나쁜   rr          = Nappeun namja mr          = Nappŭn namja}}
}}
Bad Guy is a South Korean film by director Kim Ki-duk about a man who traps a woman into prostitution, then becomes protective of her. The film was controversial for its frank portrayal of gangsters, prostitution, and sexual slavery, but also was a minor box office hit as its release coincided with a burgeoning audience interest in its male lead and director.

==Plot==
A young lady named Sun-hwa is sitting on a bench when an unusual man comes and sits by her side. She realizes that he is constantly looking at her and walks away, irritated. Her boyfriend comes and while they are talking, the silent man grabs and forcefully kisses her. Her boyfriend tries to pull him away with no results. When he stops, the woman demands an apology. He starts walking and some soldiers among the crowd that gathered due to the spectacle beat and restrain him. The woman insults him and spits on his face. He remains silent.
 won (the rough equivalent of  ) in it. As the wallet didnt contain that much money she is unable to give it to him, and he proposes a solution – a loan secured on her face and body. After a short time, she is unable to pay the loan shark and is at this point conveniently rescued by the silent man. 

She is forcefully taken to a small brothel where she is to work, never leaving until her debt is paid. 

She asks the madam for permission to see her boyfriend so that she can give her virginity to him, but when the boyfriend does not respond, the silent man attacks the boyfriend in their car. Adjustment to her new life is hard and her innocence is quickly taken away. The silent man, a pimp enforcer named Han-gi, watches her through the one-way mirror installed in her room. When she later sees him and recognizes him she attacks him, realizing that he is the one who trapped her into this life. 

One of the silent mans fellow pimps also develops a crush on the woman, paying to sleep with her and apologizing for ruining her life. She convinces him to help her escape, then he pulls the iron bars from her window and she sneaks away. Her freedom is short-lived, however, as the silent man finds her (through ingenious use of a T-shirt) and seizes her, taking her back to the brothel. First, though, he takes her to a beach, where they witness a woman committing suicide by drowning. In the sand the woman finds a torn-up photograph.

Back in the brothel the woman reconstructs the photo, taping it to her mirror; she has all of the pieces except the faces of the couple it depicts.

An attack by a rival nearly kills the silent man, but when he returns from the hospital he forbids any retaliation. One of his men kills his attacker anyway; Han-gi confesses to the police to save the man from certain execution. But the man for whom he has taken a fall is consumed by guilt and commits a crime so that he can be sent to the same prison. On the scheduled day of execution he escapes from his cell, finds the silent man, attacks him viciously, but confesses everything to the guards, winning his bosss release.

Eventually the woman realizes that she is being spied upon in her room, smashing the mirror to reveal the silent man. It is revealed that the faces of the couple in the photograph are those of Han-gi and Sun-hwa. Han-gi pays the debt of the woman and she decides to stay with him, selling herself to fishermen in the back of a truck to earn them money. The film ends with them sitting by the waterfront, and then driving away, while the Swedish hymn Blott en dag, written by Lina Sandell, is sung in Swedish in the background by Carola Häggkvist.

==Cast==
*Cho Jae-hyun as Han-gi
*Seo Won as Sun-hwa
*Kim Yoon-tae as Yoon-tae
*Choi Deok-mun as Myung-soo
*Kim Jung-young as Eun-hye
*Choi Yoon-young as Hyun-ja
*Shin Yoo-jin as Min-jung
*Namgoong Min as Hyun-soo

==References==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 