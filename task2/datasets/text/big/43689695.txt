Men Who Save the World
 
 
{{Infobox film
| name           = Men Who Save the World
| image          = Lelaki_Harapan_Dunia.jpg
| caption        = Theatrical release poster
| director       = Liew Seng Tat
| producer       = Sharon Gan
| writer         = Liew Seng Tat
| starring       = {{Plainlist |
* Wan Hanafi Su
* Harun Salim Bachik
* Soffi Jikan
* Jalil Hamid
* Azhan Rani
}}
| music          = Luka Kuncevic
| cinematography = Teoh Gay Hian
| editing        = Patrick Minks Liew Seng Tat
| distributor    = Everything Films
| released       =  
| runtime        = 94 minutes
| country        = Malaysia Netherlands Germany France
| language       = Malay
| budget         = RM 2.5 million ($ 750 000)
}}
 Hubert Bals World Cinema Prince Claus (The Netherlands), Sundance Institute | Mahindra Global Filmmaking Award, Sundance Institute Feature Film Program dan the Doris Duke Foundation (USA).

==Cast==
* Wan Hanafi Su as Pak Awang
* Soffi Jikan as Wan
* Jjamal Ahmed as Mat Kacamata
* Harun Salim Bachik as Megat
* Azhan Rani as Cina
* Muhammad Farhan Mohamad Nizam as Zakari
* Hazeehan Husain as Isteri Penghulu
* Othman Hafsham as Encik Juta Seri
* Jalil Hamid as Tok Bilal
* Azman Hassan as Khamis
* Khalid Mboyelwa Hussein as Solomon
* Bob Idris as Mat Lembu

==Production== Sundance Screenwriters Lab. The film, shot in Kuala Kangsar, Perak. Lelaki Harapan Dunia was supposed to be Liew’s debut feature film but he ended up shelving it and making Flower in the Pocket first. Luckily, the critical success of Liew’s debut paved the way to the young director receiving grants from all over the world for Lelaki Harapan Dunia. 

==Release== 67th Locarno International Film Festival on 6 August 2014. Its North American premier was held at the 2014 Toronto International Film Festival on 4 September 2014. The film was released in Malaysia on 27 November 2014.

==Critical reception==
The film get mixed reviews with praise for the humour elements and stunning cinematography. Critics also more focused on the issue of the absence of actresses, homosexuality and racism that exists in this film. Kamran Ahmed of entertainment portal Nextprojection.com in describing the film," It’s an extreme and exaggerated form of humour that works well to make certain ironic or sardonic remarks...". Shelly Kraicer of film portal Cinema-scope.com reviewed this film as "... sets out to be a boisterous comedy of rural cross-dressing and ghostly hauntings, but it has fascinating, somewhat disguised undercurrents suggesting something more serious."   Jenna Hossack of dorkshelf.com entertainment portal also provides two words to describe this film, namely "Vibrant and funny".  Shane Scott-Travis of entertainment portal vivascene.com give 4 out of 5 stars for this movie. 
Muzaffar Mustafa from Astro Awani also praised "... among the best films of in production aspects, both in terms of art direction and cinematography."  Irvan Syed Syed Mahdi of Utusan Malaysia also praised the film directing and cinematography.  Hassan Abd Al-Muttalib from the same newspaper also commented "... a continuation of the early history of Malaysia movie ..."   K.Anand of news portal The Malaysian Insider also looming this film as "... the best local film this year and among the best films of the decade. " 
 Variety criticised The Star also comment on this movie as a movie filled with potential that can not be translated properly. 

==Accolades==
{| class="wikitable"
|-
! Award
! Category
! Recipients and nominees
! Result
|- 2014 Toronto International Film Festival   Contemporary World Cinema & Grolsch Audience Selection Award Sharon Gan
| 
|- 33rd Vancouver International Film Festival  Dragons & Tigers Sharon Gan 
| 
|- 19th Busan International Film Festival 
| A Window On Asian Cinema Sharon Gan
| 
|- 67th Locarno International Film Festival     
| Concorso Cineasti del Presente Sharon Gan
| 
|- 25th Singapore International Film Festival  Asian Feature Film Sharon Gan 
| 
|- 7th Nara International Film Festival    Main Competition Sharon Gan
| 
|- Golden Horse Film Festival and Awards 2014   Asian Window Sharon Gan 
| 
|- Kolkata International Film Festival     Asian Select (NETPAC Award) Sharon Gan
| 
|-
|- International Film 2015 International Film Festival Rotterdam  Bright Future Sharon Gan
| 
|- 2015 Göteborg International Film Festival  Den gudomliga komedin Sharon Gan
| 
|-
|}

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 