Aragami
{{Infobox film
| name = Aragami
| image= Aragami VideoCover.png
| caption =
| director = Ryuhei Kitamura
| producer = Shinya Kawai
| writer = Ryuhei Kitamura Ryuichi Takatsu
| starring = Takao Osawa Masaya Kato Kanae Uotani Tak Sakaguchi Hideo Sakaki
| music = Nobuhiko Morino
| distributor = Micott Inc.
| released =  
| runtime = 70 minutes
| country = Japan
| language = Japanese
}}
Aragami is a 2003 Japanese action film directed by Ryuhei Kitamura. It was Kitamuras contribution to the Duel Project, a challenge issued by producer Shinya Kawai to him and fellow director Yukihiko Tsutsumi to film a feature length movie with only two actors, battling in one setting, in only the time frame of one week.

==Plot==
Two seriously wounded samurai find refuge from a storm at an isolated temple, the home of a swordsman and a mysterious young woman.

One samurai awakes to find that not only has his comrade died, but that his wounds have miraculously healed. He discovers that he has been given the power of immortality by the swordsman, a man once known as the legendary Miyamoto Musashi, who now lives an endless existence as Aragami, a "god of battle".

==See also==
*2LDK – Yukihiki Tsutsumi’s Duel Project film.

==External links==
* 
*  
*  

 

 
 
 
 
 
 
 
 

 