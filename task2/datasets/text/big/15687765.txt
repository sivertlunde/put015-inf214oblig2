Hands Up! (serial)
 
{{Infobox film
| name           = Hands Up!
| image          = Hands Up 1918.jpg
| caption        = Film poster
| director       = Louis J. Gasnier James W. Horne
| producer       = Jack Cunningham Gilson Willets
| starring       = Ruth Roland George Larkin
| cinematography =
| editing        = Astra Films
| released       =  
| runtime        = 15 episodes
| country        = United States
| language       = Silent with English intertitles
| budget         =
}}
 adventure film serial directed by Louis J. Gasnier and James W. Horne.

==Plot==
A newspaperwoman finds trouble aplenty when an Inca tribe believes her to be the
reincarnation of their long-lost princess.

==Cast==
* Ruth Roland as Echo Delane. This serial was Ruth Rolands breakthrough role. {{cite book
 | last = Stedman
 | first = Raymond William
 | title = Serials: Suspense and Drama By Installment
 | year = 1971
 | publisher = University of Oklahoma Press
 | isbn = 978-0-8061-0927-5
 | pages = 44
 | chapter = 2. The Perils of Success
 }} 
* George Larkin as Hands Up
* George Chesebro as Hands Up
* Easter Walters as Judith Strange
* William A. Carroll as Sam Killman / Omar the High Priest
* George Gebhardt as The Grand Envoy
* W. E. Lawrence as Prince Pampas (as William E. Lawrence)
* Thomas Jefferson
* Monte Blue

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 


 