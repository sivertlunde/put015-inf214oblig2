Nevada Smith
{{Infobox film
| name           = Nevada Smith
| image          = Nevada Smith DVD cover.jpg
| caption        = DVD Cover
| director       = Henry Hathaway
| producer       = Henry Hathaway Joseph E. Levine (executive producer)
| writer         = John Michael Hayes
| based on       =   Arthur Kennedy Alfred Newman
| cinematography = Lucien Ballard
| editing        = Frank Bracht
| studio         = Embassy Pictures Solar Productions
| distributor    = Paramount Pictures
| released       =  
| runtime        = 128 minutes
| language       = English
| budget         =
| gross          = $6.5 million (est. US/ Canada rentals) 
}}
 highly successful Arthur Kennedy, Suzanne Pleshette, Pat Hingle, and Paul Fix.

==Plot== Arthur Kennedy) Indian mother of young Max Sand (Steve McQueen). Max sets out to avenge their death. Fitch keeps a tobacco pouch containing a bit of deerskin decorated with Indian beads from Sands mothers shirt &ndash; the tobacco pouch was made from the breast of Maxs mother and Max knew this &ndash; and it is a clue Max uses in his search.

Max cannot read or write and is not skilled with a gun.  He unsuccessfully tries to rob Jonas Cord, Sr. (Brian Keith), a traveling gunsmith.  Cord recognizes that Maxs revolver is unloaded and too old and rusty to be of use or even have ammo made for it. Cord then takes pity on Max, feeds him and teaches him how to shoot.

Max hunts the killers, who have since separated. With the help of saloon girl Neesa (Janet Margolin), a woman from the same tribe as his mother, he tracks down Jesse Coe in an Abilene, Texas|Abilene, Texas saloon, killing him in a knife fight inside a corral. A wounded Max is taken to the tribes camp by Neesa, who helps his wounds mend while becoming his lover.

After recovering, Max leaves Neesa to continue his pursuit. He pulls a robbery and deliberately gets caught, in order to be sent to a prison in a Louisiana swamp where Bowdre is now serving time. Pilar (Suzanne Pleshette), a Cajun girl working in the rice fields near the convicts’ camp, gives Max comfort and finds a boat to help him escape through the swamps. Max lets Bowdre join them and murders Bowdre along the way. The boat tips in the swamp, and Pilar dies from a snake bite.

Still blinded by revenge, Max goes after Fitch, the last of the murderers. He infiltrates Fitchs gang, calling himself Nevada Smith. Fitch is aware that Max Sand is out there somewhere, waiting to ambush him.  Though he accepts Sand into the gang, Fitch is wary and suspicious, possibly accepting "Smith" to keep him close and under his eyes.  While riding out to do a robbery with the gang, Sand is spotted by Cord, who calls out "Max," but Sand ignores Cord to not be given away, as the men ride on.

At the start of the robbery, Fitch suspects that one of the men is Max Sand without knowing who, and warns him not to make a mistake or Fitch will kill him.  During the frenzy of the robbery, the gang greedily scoops up gold-- only Sand is alone, apart, looking down from a hill watching, uninterested in gold. Fitch sees that Nevada Smith is Sand, knowing now what Sands real interest is, and, throwing down his share of the gold, runs for his life.  Sand pursues and corners Fitch in a creek.  The men trade gunfire.  Sands bullets find their mark and Fitch surrenders.  But Sand continues to torture Fitch slowly, firing bullet after bullet into Fitchs body.  The outlaw begs to be killed to release him from the agony. Max appears ready to kill Fitch, but finally decides not to, saying that he is not worth killing, and rides away.

==Cast==
* Steve McQueen as Max Sand (a.k.a. Nevada Smith)
* Karl Malden as Tom Fitch
* Brian Keith as Jonas Cord
* Martin Landau as Jesse Coe Arthur Kennedy as Bill Bowdre
* Suzanne Pleshette as Pilar
* Raf Vallone as Father Zaccardi
* Janet Margolin as Neesa
* Pat Hingle as Big Foot (work camp trustee)
* Howard Da Silva as Warden of work camp
* Paul Fix as Sheriff Bonnell
* Iron Eyes Cody as Taka-Ta (uncredited)
* Josephine Hutchinson
* Loni Anderson Brunette Saloon Girl (uncredited)

== Production == Alfred Newman The Carpetbaggers, in which Alan Ladd had played a much older version of the character Nevada Smith.
 Sierra mountains.

In the scene in the cattle pens when Max (Steve McQueen) fights Jessie Coe (Martin Landau), Max crouches behind a fence and opens the gate to let the cattle out. Some cattle come out the gate while others knock down the fence, and Max must dodge the flailing legs and hooves of the stampeding cattle. The knocking down of the fence was accidental, and Steve McQueen was very nearly trampled for real. 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 