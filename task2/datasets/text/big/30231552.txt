Phantom Ranger (1938 film)
{{Infobox film
| name           = Phantom Ranger
| image_size     =
| image	=	Phantom Ranger FilmPoster.jpeg
| caption        =
| director       = Sam Newfield
| producer       = Maurice Conn (producer) Stanley Roberts Joseph ODonnell Joseph ODonnell (screenplay)
| narrator       =
| starring       = See below
| music          =
| cinematography = Jack Greenhalgh
| editing        = Richard G. Wray
| distributor    =
| released       = 27 May 1938
| runtime        = 53 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

Phantom Ranger is a 1938 American film directed by Sam Newfield.

== Cast ==
*Tim McCoy as Tim Hayes
*Suzanne Kaaren as Joan Doyle
*Karl Hackett as Sharpe
*John St. Polis as Pat Doyle
*John Merton as Henchman Bud
*Edward Earle as Matthews
*Robert Frazer as Chief McGregor
*Harry Strang as Henchman Jeff Charles King as Henchman Dan
*Richard Cramer as Barton
*Tom London as Reynolds
*Bruce Warren as Rogers Robert McKenzie as Saloon-Owner Charlie
*Jimmy Aubrey as Telegraph Operator

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 
 
 


 