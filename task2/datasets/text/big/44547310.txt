You Remember Ellen
{{infobox film name = You Remember Ellen image = You_Remember_Wiki.jpg caption = Adertising published in The Moving Picture World, Vol 11, p 654 director = Sidney Olcott producer = Kalem Company writer = Gene Gauntier based on =   starring = Jack J. Clark Gene Gauntier distributor = General Films cinematography = George K. Hollister
| released =  
| runtime = 1028 ft
| country = United States language = Silent film (English intertitles) 
}}

You Remember Ellen is a 1912 American silent film produced by Kalem Company and distributed by General Films. It was directed by Sidney Olcott with Gene Gauntier and Jack J. Clark in the leading roles.

==Cast==
* Gene Gauntier - Ellen
* Jack J. Clark - William
* Anna Clark - Ellens Mother
* Alice Hollister - Jane Arthur Donaldson 
* Robert Vignola 
* J.P. McGowan

==Production notes==
The film was shot in Beaufort, County Kerry, Ireland, during summer of 1911.

==References==
* Michel Derrien, Aux origines du cinéma irlandais: Sidney Olcott, le premier oeil, TIR 2013. ISBN 978-2-917681-20-6  

==External links==
* 
*    website dedicated to Sidney Olcott
*  at YouTube

 
 
 
 
 
 
 
 


 
 