Flags of Our Fathers (film)
 
{{Infobox film
| name           = Flags of Our Fathers
| image          = Flags of our fathers.jpg
| caption        = Theatrical poster
| director       = Clint Eastwood
| producer       = {{Plainlist|
* Clint Eastwood
* Robert Lorenz
* Steven Spielberg
}}
| screenplay     = {{Plainlist|
* William Broyles, Jr.
* Paul Haggis
}}
| based on       =   Ron Powers
| starring       = {{Plainlist|
* Ryan Phillippe
* Jesse Bradford
* Adam Beach
* John Benjamin Hickey
* Paul Walker
* John Slattery
* Barry Pepper
* Jamie Bell
* Robert Patrick
* Neal McDonough
}}
| music          = {{Plainlist|
* Clint Eastwood
* Kyle Eastwood (uncredited)
* Michael Stevens (uncredited)
}} Tom Stern
| editing        = Joel Cox
| studio         = {{Plainlist|
* Malpaso Productions
* Amblin Entertainment DreamWorks Pictures Warner Bros. Pictures
}}
| distributor    = {{Plainlist|
*   Paramount Pictures Warner Bros. Pictures
}}
| released       =  
| runtime        = 132 minutes
| country        = United States
| language       = English
| budget         = $55 million   
| gross          = $65,900,249 
}} book of James Bradley raising the flag on Iwo Jima, and the aftereffects of that event on their lives.
 Battle for Iwo Jima, while its companion film, Letters from Iwo Jima, which Eastwood also directed, is from the Japanese viewpoint of the battle. Letters from Iwo Jima was released in Japan on December 9, 2006 and in the United States on December 20, 2006, two months after the release of Flags of Our Fathers on October 20, 2006.

The film was produced by Clint Eastwood, Robert Lorenz and  Steven Spielberg.

==Plot==
  Marine Private Navy Corpsman John "Doc" Bradley are feted as heroes in a war bond drive, they reflect on their experiences via Flashback (narrative)|flashback.
 28th Marine 5th Marine Division sails to the small island of Iwo Jima as part of an invading armada. Tough Japanese resistance is expected, and the Navy bombards suspected Japanese positions for three days. Sergeant Mike Strank is put in charge of Second Platoon.
 Ralph "Iggy" Ignatowski wonders if the defenders are all dead before Japanese heavy artillery and machine guns open fire on the advancing Marines and the Navy ships. Casualties are heavy but the beaches are secured.
 Mount Suribachi Navy Cross. Finally, the mountain is secured.
 United States flag atop the mountain to cheers from the beaches and the ships. Secretary of the Navy James Forrestal, who witnesses the flag raising as he lands on the beach, requests the flag for himself. Colonel Chandler Johnson decides his 2nd Battalion deserves the flag more. Rene is sent up with Second Platoon to replace the first flag with a second one for Forrestal to take. Mike, Doc, Rene and three other marines (Corporal Harlon Block, Private First Class Franklin Sousley and Private First Class Ira Hayes), are photographed by Joe Rosenthal as they raise the second flag.

On March 1, Second Platoon are ambushed from a Japanese machine gun nest. During the fight over the nest Mike is hit by a U.S. Navy shell and dies from his wounds. Later that day Hank is shot in the chest and dies almost instantly, and Harlon is killed by machine gun fire.

Two nights later, while Doc is helping a wounded Marine, Iggy is abducted by Japanese troops and dragged into a tunnel. Doc finds his viciously mangled body a few days later. On March 21 Franklin is killed by machine gun fire and dies in Iras arms. Of the eight men in the squad only three are left: Doc, Ira and Rene. A few days after Franklins death, Doc is wounded by artillery fire while trying to save a fellow corpsman. He survives and is sent back home. On March 26, the battle ends and the U.S. Marines are victorious.

After the battle the press gets hold of Rosenthals photograph. It is a huge morale booster, and newspapers all over the country ask for prints. Rene is asked to name the six men in the photo: he identifies himself, Mike, Doc and Franklin, but misidentifies Harlon as Hank. Rene believes that Ira is the sixth man in the photograph; when he tells Ira this, Ira furiously denies it, insisting that it was Harlon in the photograph, not he. Rene pleads with Ira that as flag raisers they will both be sent home, but Ira reacts by holding a bayonet to Renes throat, telling Rene he will kill him if he names him as the man in the photograph. Rene initially refuses to identify the sixth man, but when he is threatened with being sent back to the fighting, he names Ira.
 Treasury Department, who tells them that the country cannot afford the war and if the bond drive fails the U.S. will abandon the Pacific and their sacrifices will be for nothing. The three agree not to tell anyone that Hank was not in the photograph.
 Native American, and is haunted by memories of the battle. He descends into alcoholism and throws up one night in front of General Alexander Vandegrift, commandant of the Marine Corps. A furious Vandegrift orders Ira sent back to his unit and the bond drive continues without him.
 USMC War his son, James, and in a final flashback to 1945, the men swim in the ocean after raising the flags.

==Cast== John Bradley, the only one of the six flag raisers who was not a Marine John Bradley, 
*Jesse Bradford as Corporal Rene Gagnon
*Adam Beach as Corporal Ira Hayes
*John Benjamin Hickey as Sergeant Keyes Beech
*John Slattery as Bud Gerber
*Barry Pepper as Sergeant Michael Strank
*Jamie Bell as Private Ralph Ignatowski Hank Hansen, who helped with the first flag raising and was misidentified as Harlon Block
*Robert Patrick as Colonel Johnson
*Neal McDonough as Captain Dave Severance
*Harve Presnell as Older Dave Severance
*Melanie Lynskey as Pauline Harnois Gagnon Tom McCarthy James Bradley
*Chris Bauer as General Alexander Vandegrift, the Commandant of the Marine Corps
*Gordon Clapp as General Holland Smith, who led the invasion of Iwo Jima
*Judith Ivey as Belle Block
*Ann Dowd as Mrs. Strank
*Myra Turley as Madeline Evelley Joseph Michael Cross as Private First Class Franklin Sousley Benjamin Walker as Corporal Harlon Block, who was misidentified as Hank Hansen Chuck Lindberg Scott Reeves as Private Roberto Lundsford
*David Patrick Kelly as President Harry S. Truman

==Production==
The film rights to the book were purchased by DreamWorks in June 2000.  Producer Steven Spielberg brought William Broyles to write the first drafts of the script, before director Clint Eastwood brought Paul Haggis to rewrite.    Eastwood was by the books story of an "exploitation of making celebrities of people who didn’t feel they deserved to be celebrities." In the process of reading about the Japanese perspective of the war, in particular General Tadamichi Kuribayashi, Eastwood decided to film a companion piece with Letters from Iwo Jima, which was shot entirely in Japanese.    Bradley Cooper auditioned for one of the leading roles.  Flags of Our Fathers was shot in the course of 58 days.  Jared Leto was originally cast as Rene Gagnon but had to back out due to a tour commitment with his band, 30 Seconds to Mars. 

Flags of Our Fathers cost $55 million although it was originally budgeted at $80 million. Variety subsequently downgraded the price-tag to $55 million. Although the film is taken from the American viewpoint of the battle, it was filmed almost entirely in Iceland and Southern California, with a few scenes shot in Chicago.  Shooting ended early 2006, before production for Letters from Iwo Jima began in March 2006.

==Critical reception and box office==
The film received positive reviews with the review tallying website Rotten Tomatoes reporting that 138 out of the 189 reviews they tallied were positive for a score of 73% and a certification of "fresh." {{cite web 
| url = http://www.rottentomatoes.com/m/flags_of_our_fathers/
| title = Flags of Our Fathers (2006)
| accessdate = 2007-01-21
| publisher = Rotten Tomatoes}}  On Metacritic, the film scored a 79 out of 100 based on 39 reviews, indicating "Generally favorable reviews." 
 Best Sound David E. Sound Editing.    Film critic Richard Roeper said "Clint Eastwoods Flags of Our Fathers stands with the Oscar-winning Unforgiven and Million Dollar Baby as an American masterpiece. It is a searing and powerful work from a seventy-six-year-old artist who remains at the top of his game." and "Flags of Our Fathers is a patriotic film in that it honors those who fought in the Pacific, but it is also patriotic because it questions the official version of the truth, and reminds us that superheroes exist only in comic books and cartoon movies." {{cite web 
| url = http://www.suntimes.com/entertainment/movies/104399,WKP-News-flags20.article
| title = Grand old Flags
| date = 2006-10-20
| accessdate = 2009-07-05
| work = Chicago Sun-Times
| last = Roeper
| first = Richard}} 

Despite critical acclaim, the movie underperformed at the box office, earning just $65,900,249 worldwide on an estimated $55 million production budget.  It took $2.7 million less than its companion film Letters From Iwo Jima, which had $36 million less in budget (its total budget being $19 million).   

==Spike Lee controversy==
At the 2008 Cannes Film Festival, director Spike Lee, who was making Miracle at St. Anna, about an all-black U.S. division fighting in Italy during World War II, criticized director Clint Eastwood for not depicting black Marines in Flags of Our Fathers. Eliot (2009), p.322-323  Citing historical accuracy, Eastwood responded that his film was specifically about the Marines who raised the flag on Mount Suribachi at Iwo Jima, pointing out that while black Marines did fight at Iwo Jima, the U.S. military was segregated during World War II, and none of the men who raised the flag were black. Eastwood believed Lee was using the comments to promote Miracle at St. Anna and angrily said that Lee should "shut his face".  Lee responded that Eastwood was acting like an "angry old man", and argued that despite making two Iwo Jima films back to back, Letters from Iwo Jima and Flags of Our Fathers, "there was not one black Marine in both of those films". {{cite web 
| url = http://abcnews.go.com/Entertainment/story?id=5015524&page=1 
| title = Spike Strikes Back: Clints an Angry Old Man
| date = 2008-06-06
| accessdate = 2009-07-05 ABC
| last = Marikar
| first = Sheila}}  {{cite web 
| url = http://news.bbc.co.uk/2/hi/entertainment/7439371.stm 
| title = Eastwood hits back at Lee claims
| date = 2008-06-06
| accessdate = 2009-07-05
| work = BBC News}}  {{cite journal
| last = Lyman
| first = Eric J.
| title = Lee calls out Eastwood, Coens over casting
| journal = The Hollywood Reporter, The Daily from Cannes
| volume = 
| issue = 8
| pages = 3, 24
| publisher = 
| location = Cannes
| date = 2008-05-21
| url = http://www.hollywoodreporter.com/hr/content_display/news/e3if545c66bc7e57054b34aaa6cd2b36458
| accessdate =}} 

Contrary to Lees claims, however, black Marines (including an all-black unit) are seen in scenes during which the mission is outlined, as well as during the initial landings, when a wounded black Marine is carried away. During the end credits, historical photographs taken during the Battle of Iwo Jima show black Marines. Although black Marines fought in the battle, they were restricted to auxiliary roles, such as ammunition supply, and were not involved in the battles major assaults; they did, however, take part in defensive actions. {{cite web 
| url = http://www.mpma28.com/page/page/2271596.htm 
| title = MONTFORD POINT MARINES
| accessdate = 2009-07-05
| work = Mpma28.com}}  According to Alexander M. Bielakowski and Raffaele Ruggeri, "Half a million African Americans served overseas during World War II, almost all in segregated second-line units."  The number of African Americans killed in action was 708. 

Steven Spielberg later intervened between the two directors, after which Lee even sent a copy of a film he was working on to Eastwood for a private screening as a seeming token of apology. 

==Home media release== DreamWorks Home Entertainment  and Internationally by Warner Home Video on February 6, 2007. It is devoid of any special features. 

A two-disc Special Collectors Edition DVD (with special features) was released on May 22, 2007.  It was also released on HD DVD and Blu-ray formats. 
 History Channels Warner Home Video. 

==References==
 

===Bibliography===
*  

==External links==
 
*  
*  
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 