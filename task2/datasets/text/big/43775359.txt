Mamele
{{Infobox film
| image         = Poster_for_Mamele_film_1938.jpg
| name           = Mamele
| writer         = Konrad Tom   Meyer Schwartz
| starring       = Molly Picon Edmund Zayenda Joseph Green &  Konrad Tom
| studio         = Green Films Poland
| released       = December 24, 1938
| runtime        = 97 minutes
| language       = Yiddish
}} Yiddish Language Polish musical film made in 1938 in film|1938.

==Synopsis==
Set in  ). Eventually Khavtshis morale breaks, and she moves in with the handsome musician Schlesinger (Zayenda) across the courtyard. In this period of distress Khavtshi imagines the life of her grandmother in a dream-like song and dance sequence. The Samet family begs for Khavtshi to return, which she does with Schlesinger. The film closes with Khavtshi busy preparing the feast during her wedding.   

==Cast==
*Molly Picon as Khavtshi Samet
*Edmund Zayenda as Schlesinger
*Max Bozyk as Berl Samet
*Gertrude Bullman as Berta Samet
*Simche Fostel as Nadirman
*Ola Shlifko as Jentka Samet
*Menashe Oppenheim as Maks Katz
*Karol Latowicz as Zeisher
*Max Perelman as Dawid Samet
*Ruth Turkow as Bailchi
*Lew Szrftzecer as Konchicker

==Production==
Mamele marks the second collaboration between Green and Picon after their success with Yiddle with His Fiddle. Green, by then an American citizen, was in Europe preparing for filming of A Letter to Mother, when Picons husband (and manager) proposed that the two should make another film.  Picon suggested to adapt the play of Mamele which she had performed years before onstage and although Green was reluctant to make a film based on a play, he eventually agreed.    Green convinced the cast and crew of A Letter to Mother to hold off production for some time and Mamele was filmed in six weeks in the fall of 1938, mostly in Warsaw with some exteriors shot in Ciechocinek.  Green took great care to show many aspects of typical shtetl life, depicting holidays, nightclubs, unemployment and gangsterism.

==References==

 

==External links==
* 
* 

 
 
 
 
 
 