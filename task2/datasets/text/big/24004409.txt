Angel John
{{Infobox film
| name           = Angel John
| image          = Angel John1.jpg
| caption        =
| director       = S. L. Puram Jayasurya
| producer       = K. K. Narayana Das
| writer         = S. L. Puram Jayasurya Manaf Baiju
| music          = Ouseppachan
| cinematography = Ajayan Vincent Lokanathan
| editing        = Maxlaab Entertainments
| released       =  
| runtime        = 130 minutes
| country        = India
| language       = Malayalam
}} drama Thriller thriller comedy film directed by Jayasurya and starring Mohanlal and Shanthnoo Bhagyaraj, who is making his debut in Malayalam. The cast also includes Vijayaraghavan (actor)|Vijayaraghavan, Lalu Alex, Jagathy Sreekumar, Salim Kumar, Ambika and Nithya Menon. Plot of the film has got small resemblance to the Jim Carrey film Bruce Almighty (2003). 

==Plot==
Maradonna (Shanthnoo) is an aimless youngster who likes to waste his time doing practically nothing other than fooling around. For him everything in life is Oru Rasam (just fun) and wants to take the easy way out and lacks focus in his life.
 Nithya Menon) who has a limp and is the daughter of a failed filmmaker Kuruvilla (Vijayaraghavan (actor)|Vijayaraghavan).

Maradonna messes up his life by getting into company of friends and starting an internet café that fails and to recoup his losses and attracted by ‘easy money’ he gets into drugs. Soon he is conned and loses everything after pledging his house to a ‘blade’ moneylender (Salim Kumar).

On hearing the news his father is hospitalized and his mother finally loses faith in him. A shattered Maradonna decides to commit suicide by jumping from a lighthouse into the sea. As he is about to jump, he is held back by a divine force in the form of Angel John (Mohanlal), who offers him a new lease of life. The next half of the film shows what thrill and what tragedy comes into Maradonnas life with the entry of John... I mean, Angel John.

==Cast==
* Mohanlal as Angel John
* Shanthnoo Bhagyaraj as Maradonna Joseph
* Jagathy Sreekumar as Khater Moosa
* Lalu Alex as Joseph Nithya Menon as Sofia Ambika as Mary Joseph Vijayaraghavan as James Kuruvilla Baiju as Karate Master
* Salim Kumar as a Moneylender
* Prem Kumar as Fireman
* Pradeep Chandran as Gangster

==Soundtrack== 
The music was composed by Ouseppachan and lyrics was written by Ouseppachan, SL Puram Jayasurya, Manaf and Subhash Varma. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Makane || Franco (singer)|Franco, Rajesh Vijay || Ouseppachan, SL Puram Jayasurya, Manaf || 
|-  Hariharan || Subhash Varma || 
|}

==Box office==
The film opened with unanimous negative reviews and was a failure.

==References==
 

==External links==
*   
*  
* http://sify.com/movies/malayalam/review.php?id=14916424&ctid=5&cid=2428
* http://movies.rediff.com/report/2009/oct/23/south-malayalam-movie-review-angel-john.htm

 
 
 

 