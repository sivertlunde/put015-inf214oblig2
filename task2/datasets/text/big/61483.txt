The Informer (1935 film)
{{Infobox film
| name = The Informer
| image = The_Informer_poster.jpg
| image_size =
| caption = Theatrical release poster
| director = John Ford
| producer = John Ford
| screenplay = Dudley Nichols
| based on  =   Heather Angel Una OConnor
| music = Max Steiner
| cinematography = Joseph H. August
| editing = George Hively
| studio = RKO Radio Pictures
| distributor = RKO Radio Pictures  
| released =  
| country = United States
| language = English
| runtime = 91 minutes
| budget = $243,000 Jewel, Richard. RKO Film Grosses: 1931-1951, Historical Journal of Film Radio and Television, Vol 14 No 1, 1994, p55.  
| gross = $950,000 
}}
  Heather Angel, Una OConnor The Informer The Informer (1929).
 Mutiny on Best Picture, Best Film Best Director Best Actor Best Writing Best Score.
==Plot== Black and streetwalker girlfriend Katie Madden (Margot Grahame) trying to pick up a customer. After he throws the man into the street, Katie laments that she does not have Pound sterling|£10 for passage to America to start afresh.
 Una OConnor) Heather Angel) blood money and let him go.
 American sailor. He goes to Frankies wake, and acts suspiciously when coins fall out of his pocket. The men there tell him that they do not suspect Gypo of informing, but he then meets with several of his former IRA comrades, who wonder who informed on Frankie. Gypo claims it was a man named Mulligan (Donald Meek). Though Gypo is drunk and talking nonsense, the others begin to suspect him but do not have enough evidence as yet. Gypo leaves and give out £1 notes to a blind man (DArcy Corrigan) and some bar patrons, but people wonder why he had such a sudden influx of cash. Meanwhile, Mary tells the IRA that the only person Frankie talked to that day was Gypo, and the men intend to hold an inquest into the death.

Gypo goes to an upper-class party to look for Katie, but gets drunk and buys rounds of drinks.  Gypo is then taken away by his former IRA comrades when they figure out it was him. He is taken to a kangaroo court, where Mulligan is questioned and is accused once again by Gypo. However, the comrades do not believe Gypo, and give him a detailed accounting of where he spent his entire £20 reward. Gypo then confesses to ratting out Frankie.

Gypo is locked up, but before he can be executed, he escapes through a hole in the ceiling. He runs to Katies apartment, where he tells her that he informed on Frankie. Katie goes to see the commissioner who presided over the trial, Dan Gallagher (Preston Foster), to beg him to leave Gypo alone. The rigid Gallagher says he cannot do anything, and Gypo might turn in the entire organization to the police if he is allowed to live.  However, other IRA members, having overheard Katie, go to her apartment and shoot Gypo much to Katies horror who hears the shots. Gypo wanders into a church where Frankies mother is praying and begs forgiveness as he confesses to her. She does forgive him, telling him that he did not know what he was doing, and the absolved Gypo dies content on the floor of the church after calling out to Frankie with glee.
==Cast==
 
  
* Victor McLaglen - "Gypo" Nolan Heather Angel - Mary McPhillip
* Preston Foster - Dan Gallagher
* Margot Grahame - Katie Madden
* Wallace Ford - Frankie McPhillip Una OConnor - Mrs McPhillip
* J. M. Kerrigan - Terry
* Joe Sawyer - Bartly Mulholland (credited as Joseph Sauers)
 
* Neil Fitzgerald - Tommy Connor
* Donald Meek - Peter Mulligan
* DArcy Corrigan - The Blind Man
* Leo McCabe - Donahue
* Steve Pendleton - Dennis Daly (credited as Gaylord Pendleton) Francis Ford - "Judge" Flynn
* May Boley - Madame Betty
 
==Reception==
The film was popular at the box office, earning a profit of $325,000  and remains one of John Fords most widely referenced films. 

==Awards and nominations==

===Academy Awards – 1935=== Best Actor Mutiny on Best Director. Best Writing, Outstanding Production, Best Film Editing.

{| class="wikitable" border="1"
|-
! Award !! Result !! Winner
|- Outstanding Production RKO Radio Mutiny on the Bounty (Metro-Goldwyn-Mayer|MGM) (Irving Thalberg and Albert Lewin, Producers) 
|- Best Director ||   || John Ford
|- Best Actor ||   || Victor McLaglen
|- Best Writing, Screenplay ||   || Dudley Nichols
|- Best Film A Midsummer Nights Dream 
|- Best Music (Scoring) ||   || Max Steiner
|-
|}

The films other awards and nominations: National Board of Review - Best Picture New York Film Critics Circle Awards - Best Film and Best Director
*Venice Film Festival - John Ford nominated for the Mussolini Cup

==Adaptations in other media==
The Informer was adapted as a radio play on the July 10, 1944, and October 17, 1950, episodes of The Screen Guild Theater, the March 28, 1948, episode of the Ford Theatre. On the Academy Award Theaters May 25, 1946, episode, McLaglen reprised his role.

==References==
 

==External links==
 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 