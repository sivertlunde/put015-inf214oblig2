Good-Time Girl
{{Infobox film
| name           = Good-Time Girl
| caption        = 
| image	=	Good-Time Girl FilmPoster.jpeg David MacDonald     
| producer       = Sydney Box Ted Willis
| starring       = Jean Kent Dennis Price Herbert Lom Bonar Colleano Peter Glenville Flora Robson
| music          = Lambert Williamson Clifton Parker
| cinematography = Stephen Dade
| editing        = Vladimir Sagovsky
| distributor    = General Film Distributors Eagle-Lion Films|Eagle-Lion Classics Victory Films 
| released       =  
| runtime        = 81 minutes
| country        = United Kingdom
| awards         =
| language       = English
| budget         =
| gross          = 
| preceded_by    = 
| followed_by    =
}}

Good-Time Girl is a 1948 British drama film directed by David MacDonald. The film was based on Arthur La Berns novel Night Darkens the Street. 

==Plot==
The film opens with Miss Thorpe, the chairman of the Juvenile court, giving advice to troubled teenager Lyla Lawrence. Miss Thorpe tells Lyla that her life has a similar beginning to that of Gwen Rawlings.

Gwen Rawlings was a teenage girl who continually fell into the wrong crowd. Gwens first troubles began with her employer who caught her "borrowing" a brooch from his pawnshop. Though Gwen had only borrowed it to use for a dance and had every intention of returning it, she was fired. When she arrived home and informed her father, he beat her. The next day Gwen packed her things and moved into a boarding house. There, she met Jimmy, a sharp-dressed man who immediately took a liking to her.

Jimmy found her a job at the Blue Angel Nightclub where she was employed as a hat-check girl. While working, she met Red, a bandmember for the club who felt the need to look after her well-being. Jimmy attempted to pursue Gwen but was rejected. He grew extremely angry towards the growing relationship between Red and Gwen and later beat her. Max Vine, his employer at the club, discovered Jimmys crime and fired him. Angry at Gwen, who he felt had lost him his job, Jimmy began to plot to set her up. He stole their landladys jewellery and told Gwen to pawn it for him. Believing that the jewelry belonged to his mother, Gwen followed his instructions. Later, after learning that Max had been attacked by a gang, Gwen implored Red to allow her to stay the night at his place. Red permitted her a nights stay but insisted that she leave the following day as it was unsuitable for a young girl to live with him.

However, the police soon found Gwen and she was sent to court where she was accused of having stolen jewellery. Miss Thorpe presided over the hearing and decided to sentence her to a reform school for three years. During a school fight, Gwen runs away and finds Max who has opened another club. Max is reluctant to take her back but due to her desperation, he gives her a job. Gwen soon becomes close to Danny Martin, a regular at the club. One drunken night both are out a for a drive when they accidentally hit and kill a police officer. Danny Martin forbids anyone from speaking to the police. However, once Danny is questioned, Gwen flees.

Danny later finds Gwen and beats her. Gwen is found and helped by two American soldiers who are AWOL. They decide to band together and become robbers in London. After becoming too well known in London for their crimes, they decide to head to Manchester. As they flag down a car to steal, Gwen realizes that the driver of the car is Red. When her companions realize the two recognize each other, they shoot Red dead. All three are eventually caught for their crimes and Gwen is currently serving fifteen years in prison.

At the end of the film, Lyla thanks Miss Thorpe and decides to head home.

==Cast==
*Jean Kent.....Gwen Rawlings
*Dennis Price.....Michael Red Farrell
*Herbert Lom.....Max Vine
*Bonar Colleano.....Micky Malone
*Peter Glenville.....Jimmy Rosso
*Flora Robson.....Miss Thorpe (Chairman of the Juvenile Court)
*George Carney.....Mr. Rawlings
*Beatrice Varley.....Mrs. Rawlings Hugh McDermott.....Al Schwartz Griffith Jones.....Danny Martin
*Amy Veness.....Mrs. Chalk
*Elwyn Brook-Jones.....Mr. Pottinger
*Diana Dors.....Lyla Lawrence
*Jill Balcon.....Roberta
*Harry Ross.....Fruity Lee
*Margaret Barton.....Agnes

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 