Dharma Seelan
{{Infobox film
| name           = Dharma Seelan
| image          = Dharma Seelan DVD cover.svg
| image_size     =
| caption        = DVD cover
| director       = Cheyyaaru Ravi
| producer       = V. Mohan V. Natarajan
| writer         = Agasthiya Vasahan (dialogues)
| story          = Agasthiya Vasahan
| screenplay     = Cheyyaaru Ravi Prabhu Kushboo Napoleon Geetha Geetha Salim Ghouse
| music          = Ilaiyaraaja
| cinematography = Thangar Bachan
| editing        = B.S. Mahendran
| distributor    = Anandhi Films
| studio         = Anandhi Films
| released       = 16 July 1993
| runtime        =
| country        = India
| language       = Tamil
| budget         =
| preceded_by    =
| followed_by    =
| website        =
}}
 1993 Tamil Prabhu and Geetha and Salim Ghouse play supporting roles. The film was released on 16 July 1993.   

==Plot==
Thamizh Selvan (Prabhu (actor)|Prabhu), an orphan who works in a hospital, tries to discover the truth of his parentage. Though the head of his orphanage claims that a recently deceased nun was his mother, others deny it. So Thamizh teams up with a reporter Durga (Kushboo), he meets at the nuns wake, and the two work to unearth the facts.

==Cast==
 Prabhu as Thamizh Selvan and Dharma Seelan
*Kushboo as Durga Napoleon as Geetha as
*Salim Ghouse as Manorama as
*Goundamani as Senthil as Dindigul Sahayam

==Soundtrack==

{{Infobox album |  
  Name        = Dharma Seelan|
  Type        = soundtrack |
  Artist      = Ilaiyaraaja |
  Cover       = |
  Released    = 1993 |
  Recorded    = 1993 | Feature film soundtrack |
  Length      = 37:15 |
  Label       = |
  Producer    = Ilaiyaraaja |  
  Reviews     = |
}}

The film score and the soundtrack were composed by film composer Ilaiyaraaja. The soundtrack, released in 1993, features 8 tracks with lyrics written by R. V. Udayakumar, Gangai Amaran, Pulamaipithan and Vaali (poet)|Vaali.   

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s)!! Lyrics !! Duration
|- 1 || Kinnaaram Kinnaaram || S. P. Balasubrahmanyam || R. V. Udayakumar || 4:58
|- 2 || Engumulla Allah || "Nagore" E.M. Hanifa, S. P. Balasubrahmanyam  || Gangai Amaran || 5:09
|- 3 || Vaali || 4:52
|- 4 || Iruppathai Maranthu || Mano || Pulamaipithan || 3:39
|- 5 || Vaali ||4:50
|- 6 || Aiya || Sunandha || 3:31
|- 7 || Ada Sonnaal || K. S. Chitra || 5:11
|- 8 || Kadhal Nilave || Arun Mozhi, Swarnalatha || 5:05
|}

==References==

 

 
 
 
 
 