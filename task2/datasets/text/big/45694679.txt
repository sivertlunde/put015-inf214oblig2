Mareyada Deepavali
{{Infobox film|
| name = Mareyada Deepavali
| image = 
| caption =
| director = R. Sampath
| writer = R. Sampath
| based on =  Rajesh  Kalpana   R. N. Sudarshan
| producer = M. Jagannath Rao
| music = Vijaya Bhaskar
| cinematography = R. Sampath
| editing = B. Gopala Rao
| studio = S V S Movies
| released =  
| runtime = 138 minutes
| language = Kannada
| country = India
| budget =
}}
 Kannada romantic Kalpana and R. N. Sudarshan in lead roles. 

The films soundtrack and score by Vijaya Bhaskar was widely acclaimed. 

== Cast == Rajesh  Kalpana 
* R. N. Sudarshan
* Sampath
* K. S. Ashwath
* Srilalitha
* Baby Rani
* Ramachandra Shastry

== Soundtrack ==
The music was composed by Vijaya Bhaskar with lyrics by R. N. Jayagopal. 

{{track listing
| headline = Track listing
| extra_column = Singer(s)
| lyrics_credits = yes
| collapsed = no
| title1 = Jeevanave Sukha Payana
| extra1 = P. B. Sreenivas, S. Janaki
| lyrics1 = R. N. Jayagopal
| length1 = 03:13
| title2 = Chirayuvagu Kanda
| extra2 = S. Janaki
| lyrics2 = R. N. Jayagopal
| length2 = 03:31
| title3 = Amma Muddu Amma
| extra3 = S. Janaki, B. K. Sumithra
| lyrics3 = R. N. Jayagopal
| length3 = 03:27
}}


== References ==
 

== External links ==
*  
*  


 
 
 
 
 
 
 


 