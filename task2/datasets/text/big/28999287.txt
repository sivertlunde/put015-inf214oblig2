Bully Dance
Bully Dance (French: La danse des brutes) is a 2000 animated short film by Janet Perlman about bullying. In this 10-minute short film without words, a community is disrupted when a bully victimizes a smaller member of the group.    The whole community becomes involved in dealing with the bully, who is himself a victim at home.   

In the film, characters move in rhythm to an all-percussive soundtrack for the film, which was inspired by the filmmakers own experiences in dance classes. Character animation and backgrounds were drawn on paper in ink, then scanned into a computer. It was the first time Perlman had used computers to create an animated film.   

The film received thirteen awards, including the Award for Best Animated Short Film from the Childrens Jury and the Grand Prix de Montréal - Category: Best Short Film at the Festival International du film pour enfants and First Prize in short film & video animation in recognition of outstanding achievement in childrens media from the Chicago International Childrens Film Festival.    

Bully Dance was produced by the National Film Board of Canada as part of its animated ShowPeace series on conflict resolution. 

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 


 
 