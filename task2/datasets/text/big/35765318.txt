Arrambam
 
 

{{Infobox film
| name = Arrambam 
| image          = Arrambam.jpg
| alt            =  
| caption        = First look poster Vishnuvardhan
| producer       = A. Raghuram  A. M. Rathnam 
| writer         =   Arya Nayantara Taapsee Pannu
| music          = Yuvan Shankar Raja Om Prakash
| editing        = A. Sreekar Prasad
| studio         = Sri Satya Sai Movies
| distributor    =
| runtime        = 157 minutes
| released       =  
| country        = India
| budget         =   
| gross          =   
}}
 Tamil thriller soundtrack of Om Prakash and editing by A. Sreekar Prasad.

Following pre-production works for six months and the official announcement of the project in May 2012, filming commenced in June 2012 and was held for the next 15 months. The majority of filming took place in Mumbai, where the story unfolds, while other shooting locations include Chennai, Bangalore, Dubai, Hyderabad, India|Hyderabad, Mahabaleshwar, Jaisalmer, Leh and Ladakhs.  The film was not titled until it entered post-production and was mostly referred to as Thala 53 or Valai, a supposed working title. The film was released on 31 October 2013 and emerged a box-office success.

==Plot== hacker Arjun (Arya (actor)|Arya) who starts hacking one system after the other. Ashok then threatens Sridhar Raghavan (Aadukalam Naren) by trying to kill his baby so that he can extract the truth about black money, Ashok murders him in a fit of rage. Arjun, who is particularly irritated by the things happening around him, complains about the misdeeds of Ashok to inspector Prakash (Kishore). When Ashok gets closer to achieving his goals, he gets arrested by the police.
 Assistant Commissioner Sanjay (Rana Daggubati|Rana). It turns out that Home Minister Mahadev Rane (Mahesh Manjrekar) and his associates (Atul Kulkarni) and Ramya Radhakrishnan (Suman Ranganathan) are the people involved in the scam. Ramya kills Sanjays pregnant wife and her parents by poisoning while Maya, who is also poisoned, and Mango, who was placed on railway track, survive the attack. Arjun who hears the flashback  decides to help them.

Ashok and Arjun arrive at Dubai to meet Deeksha (Akshara Gowda), daughter of Mahadev Rane. Arjun enters a bank in the guise of a service engineer, opening an account so that Ashok can transfer all the money of the minister into his account. Maya kills Ramya by pushing her off a building. Deeksha later finds out their plan but later she is threatened at gunpoint by Ashok. Ashok, Arjun and Maya later arrive in Mumbai but they come to know that Mahadev has kidnapped Anitha. Ashok tactfully kidnaps Deeksha and Durrani in Kashmir. Mahadev arrives at Kashmir with Anitha. Here, both Durrani and Deeksha get killed in the mayhem while Anitha is saved. Ashok kidnaps Mahadev and ties him to various bombs and he questions his corrupt activities. Prakash who arrives there calls Ashok to defuse the bombs. Ashok advises Prakash to think from the point of view of a common man, instead of a policeman, to think whether it is right on his part to save a corrupt person. Prakash later changes his mind and asks the other policemen to leave him. Mahadev is killed in the blast. Arjun and Anitha get married while Ashok transfers all the illegal money of the minister to Reserve Bank without revealing his appearance.

==Cast==
 
* Ajith Kumar as Ashok Kumar (AK) Arya as Arjun
* Nayantara as Maya Arrambam (DVD): opening credits from 1.24 to 1.44 
* Taapsee Pannu as Anitha 
* Mahesh Manjrekar as Mahadev Rane  JCP Milind Virekar  Kishore as Prakash 
* Suman Ranganathan as Ramya Radhakrishnan  Krishna as Mango 
* Akshara Gowda as Deeksha Rane  ACP Sanjay (Guest appearance) 
* Aadukalam Naren as Sriram Raghavan Arrambam (DVD): closing credits from 2.32.58 to 2.33.03 
* Murli Sharma as Durrani 
* Mona Kakade as Prakashs wife 
 

==Production==

===Development=== Shankar to make a sequel to his 1996 vigilante film Indian (1996 film)|Indian.    The claims were refuted by a source close to Ajith Kumar.  In January 2012, Rathnam informed that he had signed Ajith Kumar and director Vishnuvardhan for his next film,  while the director himself confirmed later that he was "undoubtedly directing Ajith’s next flick".    A press release published by A. M. Rathnam in May 2012 officially announced the project and listed the cast and crew members. According to the press release, the film would be produced by A. Raghuram on behalf of Sri Satya Sai Movies, while Rathnam would supervise it. 
 Subha joined the team and worked on the script and dialogues. According to a "close source from the production house", a remake of Race was planned initially, as there was not much time to write a fresh script, but after Subhas entry into the team, the idea was scrapped and work on an original script began,  with the writers telling that they took "nearly three months to come out with the sketch".  In an early interview during the scripting phase, Vishnuvardhan opened up about the film that he was planning to work "on a drama, more on family action drama".    Upon completion of the film, he made clear that the film was not a gangster film,  and labelled it as an "action drama",  further adding that it was a "fictional story based on a real incident".    Vishnuvardhan also said that work on the story began only after he was signed on to direct a film with Ajith Kumar.  Ajith Kumar continued the "salt and pepper look" from Mankatha,  as he would play a "mature guy",  and underwent supervised weight-training, working out at the gym for six hours a day.     The actor later disclosed that he played a "mean guy ... a politically incorrect character" while adding that the film would have a "strong social message".  Vishuvardhan opened up that Ajith would play an officer in the cyber crime department of the country.   Closer to the films release, the director revealed that Ajith would have "two faces in the film". 

The film was started without a title and remained untitled for over 15 months,  during which it was widely referred to as Thala 53 in the media, denoting Ajith Kumars 53rd film.     A teaser trailer released in May 2013 on YouTube did not feature a title either.    The makers had tossed with the idea of naming the film Thala, Ajith Kumars nickname, which was refused by the actor,  while Vishnuvardhan also requested fans to suggest a suitable title.  Several false titles including Surangani,  and Paravai went around in the media,   and in January 2013 "A source in the know" stated that the title was Valai,  after one of the writers, Suresh, had tweeted that the title begins with a V.   Although not officially confirmed, the film was henceforth referred to as Valai by the media.     At Anna University’s Gateway 2013 Short Film Festival by the Department of Media Sciences in April 2013, Vishnuvardhan revealed that the film was not titled Valai, adding that two titles were under consideration.  On 24 July 2013, Arrambam was unveiled as the films official title.  The dubbed Telugu version was titled Aata Arambham. 

===Casting===
Vishnuvardhan told that he was looking for an actress who could speak Tamil properly to be cast for the lead female role,  with Anushka Shetty and Amala Paul being among the considered candidates.  Kajal Aggarwal was also reportedly approached for the lead heroine.  Although he wanted a "fresh pairing for Ajith",  the director eventually selected Nayantara, who had earlier worked with both Ajith Kumar and Vishnuvardhan in Billa (2007 film)|Billa.  Several media sections carried reports that Vishnuvardhans brother Kreshna would be signed for a parallel role in the film,   but the role went to Arya (actor)|Arya, who had been part of four Vishnuvardhan directorials.  Several sources claimed that Arya may play an antagonistic role,      which Vishnuvardhan did not comment on but he informed that Aryas role in the film was that of a Hacker (computer security)|hacker. 
 Kishore joined the cast in July 2012 to enact an Anti-terrorist squad member, claimed to be a prominent role in the film.  Akshara Gowda stated that she was also part of the film,  while Prashant Nair informed that he had a small role in Valai. 
 Om Prakash. 

===Filming=== ligament tear.    Despite his injuries, Ajith Kumar continued shooting.  Vishnuvardhan later revealed that it was Arya who drove the car when Ajith hurted his legs during the stunt scene.  In late December a short shooting schedule was held in Chennai,  during which a fight sequence featuring Ajith and Arya was shot.  
 Manali in Himachal Pradesh.  The schedules were finished in July 2013,  following which patch work remained. On 20 August 2013, another schedule was started in Chennai.  The entire filming was completed in early October 2013 with the last day shoot held in a Mumbai mall featuring Ajith and Arya. 

Cinematographer Om Prakash stated that he wanted to give the visuals a "green tint because the colour green is commonly associated with prosperity and knowledge".    He also added that some scenes featuring Ajith were intendedly shot so that the audience would see both him and his reflection and that hand-held cameras were used to shoot those scenes from close "not because it would look fancy, but the script demanded such an approach".  He described the speed boat stunt where Ajith Kumar performed a 180-degree turn at high speed as risky and "scary" as he filmed the entire scene seated at the front of the boat. 
 dubbing for their roles in Mumbai.  In October 2013, Yuvan Shankar Raja was working on the Re-recording (filmmaking)|re-recording, while Digital intermediate works were in the "final stages". 

==Music==
 
 Vishnuvardhan and iTunes India after its release at midnight,   which, according to Sify, "no Tamil album has done before".  It also received positive reviews from critics.  

==Marketing==
On 1 May 2013, coinciding with Ajith Kumars birthday, a 42-second teaser trailer was uploaded on YouTube, which indicated that the film was untitled.  The teaser crossed 1 million hits within 2 days time.  Making-of clips of the film were telecast on Jaya TV on 15 August 2013, during which the official logo design of Arrambam was also revealed. 

The films theatrical trailer was released along with the music album on 19 September 2013. The video received a positive response and reached the 1 million hits mark on YouTube within two days.  

==Release==
The satellite rights of the film were sold to Jaya TV. In early September 2013, producer A. M. Ratnam officially announced that the film would release for the Diwali festival.  It was advanced by two days as the makers wanted the film to release on a Thursday.  On 18 October 2013 the film was given a "U" (Universal) certificate by the Indian Censor Board.  The British Board of Film Classification issued an uncut 15 certificate with an advice that it "contains strong threat and violence".  At the request of the distributor, cuts of five minutes length were made, obtaining a 12A certificate for the cut version.  

The theatrical rights for the film in Coimbatore, Tirupur, Ooty and Erode areas were purchased by Cosmo Pictures Siva for  69 million,   while the rights for Madurai-Ramnad area were sold to Alagar for an undisclosed amount.  Ayngaran International bought the rights of the NSC (North Arcot-South Arcot–Chengalpet) area from the producer. Director and distributor Rama Narayanan purchased the Chengalpet area.  The Kerala theatrical rights of were sold for  14.5 million to Sree Kaleeshwary, while the Karnataka theatrical rights of were purchased by Sri Lakshmi Swami Enterprise for over  30 million.  The US distribution were acquired by GK Media,  which planned to release the film in 78 locations in the USA. 

===Controversies===
By mid-October 2013, a petition was filed in the Madras High Court by B. Rajeswari alleging that that Arrambam producer A. M. Rathnam had not returned money borrowed from the financier and remanding the films release to be stalled until the dues were met. Rathnam however clarified that the film was not produced by him but by A Raghurams Sri Sathya Sai Movies.  A day prior to the planned release, film producer K. Kannan filed a suit in the City Civil Court seeking to restrain the film from releasing as he had already commenced the production of a film with a similar title, Ini Dhan Arambam that had been registered with Film and Television Producers Guild of South of India. 

===Reception===
Arrambam opened to positive reviews from critics.  Behindwoods stated, "Director Vishnuvardhan, writer duo Subha and editor Sreekar Prasad have to be commended for managing to keep the audiences hooked to their seats for a majority of the movie with very few drag moments. With a running time of more than two and a half hours, the screenplay is taut and crisp enough to keep the crowd arrested."  Sify said, "Arrambam lives up to the expectations and is a satisfying thrill ride for its variety in providing no-holds barred entertainment. Ajith terrific screen presence and powerful dialogue delivery, smart writing, charismatic cast and action which is fast-paced are smartly packaged by director Vishnuvardhan." About the performances, the critic said, "Ajith himself gives a low-key, well-nuanced performance as Ashok, the conscience of this film; Arya is superb as the happy-go-lucky Arjun and his comedy scene in college with Nayan is a rocker."  Indiaglitz commented, "Simple, elegant and class but richly crafted to keep you tuned to the screen –  Arrambam is an interesting story in a gripping screenplay." Talking about the technical aspects, the website says, "Om Prakash has canned every frame with passion. Cohesion and continuity is edgy and distinctly better only as the film progresses. Srikar Prasad has however done a commendable job in putting the pieces of action together in thorough entertainment. Background score adds volume to the story, supporting it substantially well." 

  wrote, "The film will not disappoint the average viewer despite not living up to the hype and hoopla". 
 The List rated it 2/5 and called it a "derivative but splashy and colourful action-fest that makes the most of a charismatic star-turn from Kumar. What’s missing is any real sensitivity to the material; Vishnuvardhan stages his set pieces with style, but the uneasy mixture of genre styles places Arrambam firmly in the file marked disposable entertainment". 

The Telugu version Aata Arrambam received mixed reviews from critics.  IndiaGlitz said, "Ajith has done a truly great job in the movie – more than a hero (or anti-hero, as he is cast), he has performed as the typical character that Subha and Vishnuvardhan have conceived. Carrying himself at ease in the contradicting roles he plays in both the halves of the film, Ajith proves his worth once again as an excellent actor." and added, "Nayanthara who comes around as his ally has added the right amount spice wherever required. The actress seems to have distinctly matured from her Billa days, though. The romance that Arya and Taapsee share is cute, bubbly and keeps the movie going forward on a lighthearted note. Though Rana Dagubatti and Kishore are cast on brief roles, their characters are pivotal and support the crux of the story strongly."  123telugu said, "Aata Arrambham banks too much on Ajiths screen presence and the actor doesnt disappoint. He may not dance like a dream or look fit, but he makes up for all that with his daredevil stunts and screen presence. The second half in particular drags too much and the climax is quite disappointing. Aata Arrambham is not a bad film at all, but it could have been so much better and riveting."  APHerald said, "Despite having a good storyline, the poor execution makes it uninteresting. If the screenplay would have been more gripping, we could have witnessed a promising thriller. On the whole, an uninteresting action- thriller that pushes its viewers to have a sigh of relief when the film gets over." 

==Box office==
According to trade analyst Trinath, Arrambam collected approximately  92.1 million nett on the opening day in Tamil Nadu, which was the best opening for an Ajith film.  According to Behindwoods, the film grossed  43.9 million at the Chennai box office alone over the first four days. 

In the US, the film earned 330,615 USD ( 2.04 crore) from 73 reported screens in its opening weekend.  As of 9 November 2013, it has grossed $369,150, approximately  2.3 crore and became the fifth highest grossing Tamil film and highest grossing Ajith Kumar film in the US.  In the UK, the opening weekend gross was £135,213 ( 13.4 million) from 37 screens, inclusive of the preview shows of £34,830 previews on Wednesday evening.   After the second weekend, the film had earned £176,282 at the UK box office overall.  In Australia, the film earned an opening-weekend total of   ( 33 lakh) from 7 screens.  In Malaysia, Arrambam opened at the second spot as it grossed $788,469 (around  85 million) in its opening weekend (31 October to 3 November), from 80 screens.  After the second weekend, the total collection was $1,359,407. 

==Awards and nominations==
* Vijay Award for Favourite Film - Won
* Filmfare Award for Best Actor – Tamil - Ajith Kumar - nominated
* Vijay Award for Favourite Hero - Ajith Kumar - nominated

==References==
 

==External links==
*  
*  
 
 

 
 
 
 
 
 
 
 
 
 
 
 