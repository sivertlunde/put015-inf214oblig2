In the Shadow of the Blade
 
In the Shadow of the Blade is a 2004 American documentary film produced and directed by Patrick and Cheryl Fries.  It won awards in the film industry and with the Vietnam veteran community, including the WorldFest-Houston International Film Festival "Best of Show" and "Gold Documentary" and the Vietnam Veterans of America Presidents Award for Outstanding Documentary.  The documentary was acquired for North American television broadcast by Discovery Communications.

==Content==
The film follows a restored UH-1 Iroquois "Huey" helicopter on a flight to reunite Vietnam War veterans and families of the dead with the iconic aircraft three decades after the war as a vehicle for eliciting the stories of ordinary Americans affected by the war.

==The aircraft==
The films centerpiece is UH-1 "Huey 091" (tail number 65-10091), which is now permanently displayed in the Smithsonian Museum of American History "Price of Freedom: Americans At War" exhibit.  "Huey 091" served during the Vietnam War with the U.S. Army in the 173rd Assault Helicopter Company, "The Robin Hoods, in 1966-67 before being removed stateside after incurring battle damage.

==Route==
The documentarys "mission of healing and reconciliation" lifted off at Fort Rucker, Alabama, where many Vietnam helicopter pilots trained before deployment.  War journalist Joseph L. Galloway spoke at the ceremonial event, after which veteran Huey pilot  Michael J. Novosel, a Medal of Honor recipient took the left seat.  The inaugural flight carried Vietnam veterans representing the United States Army, the United States Marine Corps, the United States Navy, and the United States Air Force.  The first landing zone was the Wall South in Pensacola, Florida.

More than 40 more landing zones across eight states captured stories of veterans from all branches and many military roles.  Each flight leg carried Vietnam War veterans and/or families of servicemen Killed in action.  The film made its final landing on Veterans Day at the Vietnam Veterans Memorial State Park in Angel Fire, New Mexico.

==Film Subjects==
The producers encouraged Vietnam veterans in local areas to create their own landing zone events in their communities.  Some, like the one in Liberty County, Georgia became public "welcome home" ceremonies for local veterans.  Others, like the backyard landing zone near Columbus, Georgia where the son and grandson of Charles L. Kelly heard stories about the founder and legacy of Medical evacuation|"Dustoff" missions, were more private.  A landing near Atlanta, Georgia resulted in a later reunion of a Vietnam War Army nurse with a now-grown Vietnamese baby shed helped save during the war. General  Hal Moore, commander of the Battle of Ia Drang was interviewed later by the film producers.

==Screenings==
The film debuted in 2004 at the Lyndon Baines Johnson Library and Museum to an audience of more than 1,000.  Other major screenings have been held at the Women In Military Service for America Memorial, the Museum of Flight the Corning Museum of Glass and the Ronald Reagan Presidential Library.

==External links==
*  
*  

 
 
 
 
 
 
 