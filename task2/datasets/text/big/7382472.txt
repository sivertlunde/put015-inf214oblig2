Dangerous Mission
{{Infobox film
| name           = Dangeous Mission
| image          = Dangerous mission Poster.jpg
| image_size     =
| caption        = Theatrical release poster
| alt            =
| director       = Louis King
| producer       = Irwin Allen Charles Bennett W. R. Burnett Horace McCoy
| story          = James Edmiston Horace McCoy
| narrator       =
| starring       = Victor Mature Piper Laurie Vincent Price
| music          = Roy Webb
| cinematography = William E. Snyder
| editing        = Frederic Knudtson Gene Palmer
| distributor    = RKO Pictures
| released       =    |ref2= }}| runtime        = 75 minutes
| country        = United States
| language       = English language
| budget         =
}} dramatic color "resort thriller (genre)|thriller"  starring Victor Mature, Piper Laurie, Vincent Price, and William Bendix.  The movie was produced by Irwin Allen, directed by Louis King, and released by RKO Pictures.  It is remembered today largely for its use of 3-D film technology.

==Plot== Glacier National Park. She is followed by two men, Matt Hallett and Paul Adams, one of whom is a federal agent, who had sworn to protect her and bring her back as a witness, the other a ruthless killer, determined to murder her.

==Cast==
* Victor Mature as Matt Hallett
* Piper Laurie as Louise Graham
* William Bendix as Chief Ranger Joe Parker
* Vincent Price as Paul Adams
* Betta St. John as Mary Tiller
* Dennis Weaver as The Ranger Clerk
* Harry Cheshire as Mr. Elster
* Steve Darrell as Katoonai Tiller Walter Reed as Ranger Dobson
* Marlo Dwyer as Mrs. Elster

==Production== Glacier National Park, Montana, and was largely filmed there.

==Reception==

===Critical response===
When the film was first released, film critic Bosley Crowther panned the film, writing, "Since our great national parks are open to virtually anyone who cares to visit them, there probably is no way of preventing their occasionally being exploited and abused. And that is most certainly what has happened to Glacier Park in the R. K. O. film, Dangerous Mission, which was plopped down flatly at the Holiday yesterday...a company of Hollywood people has the cheek to play a tale that hasnt the vitality or intelligence of a good comic-strip episode. It is a miserably dull and mixed-up fable about a hunt for a missing witness to a crime, with Vincent Price eventually emerging as some sort of villain, which is obvious all along." 

More recently, film critic Dennis Schwartz has also panned the film, writing, "An action movie made for 3D that starts off looking like a real corker but winds up looking as stale as month-old bread. Director Louis King (Frenchie/Green Grass of Wyoming) never steers it away from its awkwardness. Despite a fine cast (unfortunately they all give corpse-like performances), capable screenwriters Charles Bennett and W.R. Burnett, and veteran story writers Horace McCoy and James Edmiston, the film is at best bearable...William Bendix plays a blustery park ranger chief who knew Mature from their days as marines. His mission, in this film, is to put out a forest fire that has nothing to do with the plot, but looks swell on 3D. The film is noteworthy for the clumsy job Gene Palmer turned in as editor." 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 