Arrowhead (film)
{{Infobox film
| name           = Arrowhead
| image          = Heston-Jurado-Arrowhead.jpg
| image_size     = 250px
| caption        = Charlton Heston and Katy Jurado in a scene of the film.
| director       = Charles Marquis Warren
| producer       = Nat Holt
| writer         = William R Burnett|W.R.Burnett (novel) Charles Marquis Warren (screenplay)
| starring       = Charlton Heston Jack Palance
| cinematography = Ray Rennahan
| editing        = Frank Bracht
| distributor    = Paramount Pictures
| released       =  
| runtime        = 105 minutes
| country        = United States
| language       = English
| gross          = $1.2 million (US) 
}} western film directed by Charles Marquis Warren and starring Charlton Heston and Jack Palance. The film is based on the novel Adobe Walls by William R Burnett|W.R.Burnett. The screenplay was also by Charles Marquis Warren.

==Plot synopsis==
Maverick scout, Ed Bannon, (Charlton Heston) is working with cavalry stationed at Fort Clark, Texas. The US army is trying to talk peace with the Apaches and move them to reservations in Florida. Bannons activities seem counter productive to this new policy. Toriano (Jack Palance), the son of the Apache chief, returns from an Eastern education. Bannon is suspicious of his motives and their distrust of each other is eventually resolved by single combat.

==Cast==
*Charlton Heston as Ed Bannon  
*Jack Palance as Toriano
*Katy Jurado as Nita
*Brian Keith as Capt. Bill North
*Mary Sinclair as Lela Wilson
*Milburn Stone as Sandy MacKinnon Richard Shannon as Lt. Kirk Lewis Martin as Col. Weybright
*Frank DeKova as Chief Chattez
*Robert J. Wilkie as Sgt. Stone Peter Coe as Spanish Kyle James as Jerry August John Pickard as John Gunther
*Pat Hogan as Jim Eagle

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 


 