The Sound of a Dirt Road
{{Infobox film
| name           = The Sound of a Dirt Road
| image          = The Sound of a Dirt Road.jpg
| image_size     = 
| alt            = 
| caption        = 
| director       = Graham D. Alexander
| producer       = Danny Alexander Erin Alexander
| writer         = Graham D. Alexander
| narrator       = 
| starring       = Ross Renfroe Jenny Hamilton
| music          = Taylor Davis
| cinematography = 
| editing        = 
| studio         = MyShow Productions
| distributor    = 
| released       =  
| runtime        = 119 minutes
| country        = United States
| language       = English
| budget         = $900
| gross          = 
}}
The Sound of a Dirt Road is a 2008 Christian film directed by Graham D. Alexander. In less than a year, Alexander wrote, directed and filmed the movie East Texas, for less than $900.    The Sound of a Dirt Road was featured at the San Antonio Independent Christian Film Festival.  It was chosen as one of eight finalists in the feature film category, out of 50 submissions. 

== Plot ==
John Crowe (Ross Renfroe) is a rancher whose family has owned and managed the Crowe Ranch for over 150 years. John meets Ellen Sower, and his life slowly begins to change. Jeremiah Stillwell, the soon-to-be pastor of the local church, also becomes interested in Ellen. Intellectual warfare ensues as John rescues Ellen, then learns she is deathly ill. Jeremiah desires power, and once he gets it, he goes after John.

== References ==
 

== External links ==
*  
*   at the San Antonio Independent Christian Film Festival
*   at  

 
 
 
 
 