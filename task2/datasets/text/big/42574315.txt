Don Bosco (1935 film)
{{Infobox film
| name = Don Bosco
| image =
| image_size =
| caption =
| director = Goffredo Alessandrini
| producer = 
| writer =  Sergio Amidei   Onorato Castellino   Rufillo Uguccioni  Aldo Vergano    Goffredo Alessandrini
| narrator =
| starring = Gianpaolo Rosmino   Maria Vincenza Stiffi   Ferdinando Mayer
| music = Giorgio Federico Ghedini
| cinematography = Arturo Gallea
| editing = Giorgio Simonelli
| studio =   Lux Film
| distributor = 
| released = 1935
| runtime = 90&nbsp;minutes
| country = Italy Italian
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Don Bosco is a 1935 Italian drama film directed by Goffredo Alessandrini and starring Gianpaolo Rosmino, Maria Vincenza Stiffi and Ferdinando Mayer. The film is a portrayal of the life of the Catholic Priest John Bosco (1815–1888). It was made by Lux Film, one of the bigger Italian companies of the era.  Alessandrini later went on to direct a later, more celebrated biopic of a nineteenth century religious figure with his Cardinal Messias (1939).

==Cast==
*Gianpaolo Rosmino as Don Giovanni Bosco
*Maria Vincenza Stiffi as Margherita, sua madre
*Ferdinando Mayer as Giovanni Bosco as child
*Roberto Pasetti
*Vittorio Vaser
*Felice Minotti
*Cesare Gani Carini
*Arturo Zan

== References ==
 

== Bibliography ==
*Moliterno, Gino. The A to Z of Italian Cinema. Scarecrow Press, 2009.

== External links ==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 


 