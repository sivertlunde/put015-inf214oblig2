La dame de chez Maxim's (1950 film)
{{Infobox film
| name           = La dame de chez Maxims
| image          = 
| image_size     = 
| caption        = 
| director       = Marcel Aboulker
| producer       = 
| writer         = Georges Feydeau (play)   Marcel Aboulker   Robert Beauvais
| narrator       =  Jacques Morel
| music          = Paul Durand
| editing        = Jacques Grassi
| cinematography = Pierre Levent 
| distributor    = Gaumont Distribution
| released       = 15 September 1950
| runtime        = 92 minutes
| country        = France
| language       = French
| budget         = 
| preceded_by    = 
| followed_by    =  French comedy La Dame de chez Maxim by Georges Feydeau. 

==Cast==
* Arlette Poirier : La môme Crevette
* Saturnin Fabre : Le général Petypon du Grêlé
* Marcelle Monthil : Mme. Petypon Jacques Morel : Le docteur Petypon
* Marcelle Praince : La duchesse
* Robert Vattier : Le docteur Montgicourt
* Jean Marsan : Le lieutenant Corrigon
* Luc Andrieux : Etienne
* Colette Ripert : Clémentine
* Jacques Fabbri : Le duc
* Jacques Beauvais : Eugène Pierre Flourens
* Philippe de Chérisey
* Albert Rieux

==References==
 

==Bibliography==
* Hayward, Susan. French Costume Drama of the 1950s: Fashioning Politics in Film. Intellect Books, 2010.
* Oscherwitz, Dayna & Higgins, MaryEllen. The A to Z of French Cinema. Scarecrow Press, 2009.

==External links==
* 
 
 

 
 
 
 
 
 