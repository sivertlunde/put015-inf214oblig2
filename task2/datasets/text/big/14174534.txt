I Will Always Love You (film)
{{Infobox film
| name           = I Will Always Love You
| image          =
| caption        =
| director       = Mac C. Alejandre
| producer       =
| writer         =
| starring       = Richard Gutierrez Angel Locsin
| music          =
| cinematography =
| editing        =
| distributor    = Regal Films GMA Films
| released       =  
| runtime        =
| country        = Philippines
| awards         = Tagalog English English
| budget         =
| gross          = P180,000,000
}}

I Will Always Love You is a Philippine movie starring Richard Gutierrez and Angel Locsin. The theme song of the film, "Ill Always Love You (Michael Johnson song)#Nina version|Ill Always Love You", was performed by Nina Girado|Nina.

==Plot==
Justin (Richard Gutierrez) is a rich, smart, confident mestizo from a private school in Manila while Cecille (Angel Locsin) is a simple scholar from a rural public school. The two fall for each other against the wishes of his parents, (Jean Garcia and Lloyd Samartino). They want him to marry Donna (Bianca King), their business partner’s daughter.

His parents order Justin to study in the States to separate him from Cecille. But Justin finds a way to take her with him to San Francisco without anyone knowing. The young lovers live their dream in America. One day, however, Justin’s mother drops by to visit, bringing Donna with her. Justin hides Cecille with a family friend (Suzette Ranillo). One day, she catches him and Donna in a tight embrace.

She takes the first plane back to Manila, ignoring Justin’s attempts to explain why he was kissing Donna. Back home, Cecille’s childhood friend Andrew, (James Blanco) courts her relentlessly. In a few months, Andrew and Cecille are engaged to be married. And then, Justin flies home to try to win Cecille back..

==Cast==
*Richard Gutierrez as Justin Ledesma
*Angel Locsin as Cecille
*James Blanco as Andrew
*Bianca King as Donna
*Tuesday Vargas as Frida
*Bearwin Meily as Ogie
*Jean Garcia as Adelle Ledesma
*Lloyd Samartino as Edward Ledesma
*Amy Austria as Encar

===Also starring===
Alphabetically
*Melissa Aguirre as Cindy
*Louie Alejandro as Peping
*Malou Crisologo as Grace
*Soliman Cruz as Roger
*Karen delos Reyes as Tessa
*Ehra Madrigal as Mitch
*Miriam Pantig as Mrs. Rivera
*Suzette Ranillo as Tita Emma
*Miguel Tanfelix as Jonjon
*Nash Aguas as young Justin
*Gabriel Roxas as young Ogie
*Miguel Aguila

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 