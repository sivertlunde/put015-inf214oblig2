The Glitter Dome
{{Infobox film
| name           = The Glitter Dome
| image          = GarnerKidderGlitter.jpg
| alt            = 
| caption        = theatrical poster
| director       = Stuart Margolin
| producer       = Frank Konigsberg
| writer         = Joseph Wambaugh
| screenplay     = Stanley Kalis
| based on       = the novel by Joseph Wambaugh
| starring       =  James Garner Margot Kidder John Lithgow
| music          = Stuart Margolin
| cinematography = Michael W. Watkins Jon Kranhouse
| editing        = M.S. Martin
| studio         = HBO Pictures
| distributor    = HBO Pictures Thorn EMI
| released       =  
| runtime        = 97 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
The Glitter Dome is a 1984 made-for-HBO film starring James Garner, Margot Kidder and John Lithgow.  The film, based on the 1981 Joseph Wambaugh Hollywood-set homicide novel, was directed by Stuart Margolin, who also scored the film and played a supporting part.  The movie was filmed in Victoria, British Columbia and co-starred Colleen Dewhurst.  It was subsequently released on video in 1985.  The film was also the last film for John Marley.

==Synopsis==
The world of the Hollywood detective division is exposed via The Glitter Dome (the bar they frequent and a slang reference to Hollywood, California).  When the investigation of a high profile studio president is going nowhere, the case is handed over to two experienced detectives, Al Mackey and Marty Welborn, who have a reputation for being able to solve the toughest cases, through creative means.  For this case, they need help.  They get it from a pair of vice cops, called the Ferret and the Weasel, and a pair of street cops commonly referred to as the street monsters thanks to their fondness for violence.

==Cast==
*James Garner ...  Sgt. Aloysius Mackey 
*Margot Kidder ...  Willie 
*John Lithgow ...  Sgt. Marty Wellborn 
*John Marley ...  Capt. Woofer 
*Stuart Margolin ...  Herman Sinclair 
*Paul Koslo ...  Griswold Veals 
*Colleen Dewhurst ...  Lorna Dillman 
*Alek Diakun ...  Weasel 
*Billy Kerr ...  Ferret 
*William S. Taylor ...  Hand 
*Dusty Morean ...  Phipps 
*Christianne Hirt ...  Jill 
*Tom McBeath ...  Farrell 
*Dixie Seatle ...  Amazing Grace 
*Dawn Luker ...  Gladys 
*Harvey Miller ...  Harvey Himmelfarb 
*Enid Saunders ...  Eleanor St. Denis 
*Alistair MacDuff ...  Malcolm Sinclair 
*Max Martini ...  Steven
*Benson Fong ...  Wing  Dale Wilson ...  Lloyd / Bozeman

==Controversy==
When first telecast on November 11, 1984, The Glitter Dome was criticized for a brief  ". 

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 