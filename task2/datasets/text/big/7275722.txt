Break, Break, Break (film)
{{Infobox film
| name           = Break, Break, Break
| image          = Break! Break! Break!.png
| image_size     =
| caption        = Still from Break, Break, Break 
| director       = Harry A. Pollard
| producer       =
| writer         = Sydney Ayres
| narrator       =
| starring       = William Garwood Louise Lester
| cinematography =
| editing        = Mutual Film Corporation
| studio         = American Film Manufacturing Company
| released       =  
| runtime        = Short
| country        = United States English intertitles
| budget         =
| website        =
}}
 short film directed by Harry A. Pollard. A period drama written by Sydney Ayres, the film starred William Garwood and Louise Lester.
 Moving Picture World shortly after its release: A pretty picture telling an idyillic love story; it should go very well; for, though it depends on sentiment rather than on thrilling dramatic suspense, it holds the attention strongly and is filled with the atmosphere of the good, old-time stories and poems. The costumes are of the mid-Victorian period in rural England. Many of its scenes are as charming as good pictures. The acting is also excellent quality. Vivian Rich is the heroine; Harry Von Meter, the hero, and Jack Richardson, the light villain. Much of the action is among the hay fields and then the seashore.  

Break, Break, Break was a single-reel film produced by the  . Retrieved on September 7, 2009.   which distributed 58 prints. 

==Cast==
 
*B. Reeves Eason as Grandfather Day
*William Garwood as Tom Day, a son of the People
*Louise Lester as Mary Elizabeth Day, Toms mother Jack Richardson as Dan Moore, a son of the Rich
*Vivian Rich as June, the adopted daughter
*Harry von Meter as Squire Moore, wealth land owner

==References==
 

==External links==
 
* 

 

 
 
 
 
 
 
 
 
 
 


 