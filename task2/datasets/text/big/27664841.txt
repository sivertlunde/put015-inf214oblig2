Stop Press Girl
{{Infobox film
| name           = Stop Press Girl
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Michael Barry
| producer       = John Croydon
| writer         = 
| screenplay     = T.J. Morrison Basil Thomas
| story          = 
| based on       = 
| narrator       =  Gordon Jackson James Robertson Justice Kenneth More
| music          = Arthur Goehr
| cinematography = 
| editing        = 
| studio         = Aquila Film
| distributor    = General Film Distributors
| released       = June 1949 (UK)
| runtime        = 78 min
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}} British fantasy Michael Barry Gordon Jackson, Basil Radford and Naunton Wayne; the latter two appearing in several different roles in the film. The film was one of four of David Rawnsleys Aquila Films that used his proposed "independent frame" technique.

==Plot== Liberator airliner!

==Main cast==
* Sally Ann Howes – Jennifer Peters Gordon Jackson – Jock Meville
* Basil Radford – The Mechanical Type
* Naunton Wayne – The Mechanical Type
* James Robertson Justice – Arthur
* Sonia Holm – Angela Carew
* Nigel Buchanan – Rory Fairfax
* Joyce Barbour – Aunt Mab
* Campbell Cotts – John Fairfax
* Cyril Chamberlain – Johnie
* Julia Lang – Carole Saunders
* Percy Walsh – Editor, Evening Comet
* Oliver Burt – Editor, Morning Sun
* Kenneth More – Police Sgt. Bonzo
* Humphrey Lestocq – Radio Commentator
* Michael Goodliffe – McPherson

==References==
 

==External links==
* 

 
 
 
 
 
 
 



 
 