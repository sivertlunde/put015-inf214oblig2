Slaphappy Sleuths
{{Infobox Film |
  | name           = Slaphappy Sleuths |
  | image          = Slaphappy 1sht.jpg|
  | caption        = |
  | director       = Jules White Felix Adler|
  | starring       = Moe Howard Larry Fine Shemp Howard Stanley Blystone Gene Roth Emil Sitka Nanette Bordeaux Vernon Dent|
  | cinematography = Vincent J. Farrar | 
  | editing        = Edwin H. Bryant |
  | producer       = Jules White |
  | distributor    = Columbia Pictures |
  | released       =  
  | runtime        = 16 09"
  | country        = United States
  | language       = English
}}

Slaphappy Sleuths is the 127th short subject starring American slapstick comedy team the Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959.

==Plot==
The Stooges are investigators for the Onion Oil company, whose service stations are being robbed by a gang of crooks. On the job, the Stooges provide nothing less than first-class service. However, most of the services are not typical of your standard gas station (shaves, manicures and cologne), and they still somehow manage to be robbed when their backs are turned. 

Tracing a trail of motor oil to the crooks hideout, the Stooges demonstrate boxing skills far more effective than their earlier detective skills.

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 

 