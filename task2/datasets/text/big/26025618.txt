Illarikam
{{Infobox film
| name           = Illarikam
| image          = Illarikam.png
| image_size     =
| caption        =
| director       = T. Prakash Rao
| producer       = Anumolu Venkata Subba Rao B. A. Subba Rao
| writer         = Arudra, Kotayya Pratyagatma
| narrator       = Jamuna C.S.R. Girija Peketi Gummadi
| music          = T. Chalapathi Rao
| cinematography = A. Vincent
| editing        = A. Sanjeevi
| studio         =
| distributor    =
| released       = May 1, 1959
| runtime        = 152 minutes
| country        = India Telugu
| budget         =
}} Sasural in 1961.

Illarikam literally means bridegroom goes to the bride family and lives permanently with them; as against the custom of bride going into the groom family. This custom is observed in some rich families where the bride is the only child to their parents.

==The plot==
Venu (Akkineni Nageswara Rao) studies with the help of his maternal uncle. He falls in love with the daughter of a zamindar (Gummadi Venkateswara Rao) i.e. Radha (Jamuna). He married to her and stays in their house as Illarkam. Sundaramma (Hemalatha), wife of Zamindar does not like this and insults him indirectly. Govindayya (C.S.R. Anjaneyulu) creates problems between Venu and Radha. How Venu solves all the problems is the story.

==Cast==
{| class="wikitable"
|-
! Actor !! Character
|-
| Akkineni Nageswara Rao || Venu
|- Jamuna || Radha
|-
| Gummadi Venkateswara Rao || Zamindar
|-
| Relangi Venkata Ramaiah || Brahmanandam
|-
| Hemalatha || Sundaramma, Zamindars wife
|-
| C.S.R. Anjaneyulu || Govindayya
|-
| R. Nageswara Rao || Sadanandam/Seshagiri
|-
| Bala || Savithri
|- Girija || Kanakadurga
|-
| T. G. Kamala Devi || Zamindars sister
|-
| Allu Ramalingaiah || Panakalu
|-
| Ramana Reddy || Dharmayya
|-
| Boddapati || Dance master
|-
| Peketi Sivaram
|-
| Vijayalakshmi
|-
| Surabhi Kamalabai || Mother of Venu
|}

==Crew==
* Screenplay, Direction: Tatineni Prakash Rao
* Producer: Anumolu Venkata Subbarao
* Story: Vempati Sadasivabrahmam
* Dialogues: Arudra
* Music Director: T. Chalapathi Rao
* Playback Singers: Ghantasala Venkateswara Rao, P. Susheela, P.G. Krishnaveni (Jikki), Madhavapeddi Satyam
* Lyrics: Arudra, Srirangam Srinivasa Rao, Kosaraju Raghavayya Chowdary
* Choreography: Hiralal, Pasumarthy Krishnamurthy
* Camera: A. Vincent
* Editing: A. Sanjeevi
* Associate Directors: K. Pratyagatma, Koganti Gopalakrishna
* Assistant Director: Tatineni Rama Rao (debut)

==Songs==
* Adigindaaniki cheppi eduraaDaka praSnanu vippi... (Playback: Ghantasala (singer)|Ghantasala, P. Susheela; Lyrics: Kosaraju Raghavaiah Chowdary; Cast: A.N.R., Jamuna, and others)
* ekkaDi dongalu akkaDanE gapchip!... (Playback: Ghantasala; Lyrics: Sree Sree; Cast: A.N.R., Jamuna)
* chEtulu kalasina chappaTlu, manasulu kalasina mucchaTlu... (Playback: Ghantasala, Madhavapeddi Satyam, P. Susheela; Lyrics: Arudra; Cast: A.N.R., Jamuna, Relangi, and others)
* nEDu Sreevaariki mEmanTE paraakaa? (Playback: Ghantasala, P. Susheela; Lyrics: Arudra; Cast: A.N.R., Jamuna)
* madhupaatra nimpavOyii... (Playback: Jikki, Chorus; Lyrics: Arudra; Cast: Girija)
* bhalE chance-lE... (Playback: Madhavapeddi Satyam; Lyrics: Kosaraju Raghavayya Chowdary; Cast: Relangi, Peketi Sivaram)
* niluvavE vaalukanula daanaa!... (Playback: Ghantasala; Lyrics: Kosaraju Raghavayya Chowdary; Cast: A.N.R., Jamuna)

==External links==
*  
*  
*  

 
 
 
 