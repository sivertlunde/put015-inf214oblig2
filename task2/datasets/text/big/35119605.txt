Al Halfout
{{multiple issues|
 
 
}}

{{Infobox film
| name           = Al Halfout
| image          = 
| director       = Samir Seif Mustafa Gamaleddin
| producer       = Sout Al Fan Films
| writer         = Wahid Hamid
| starring       = Adel Emam Saeed Saleh Ilham Chahine Salah Kabil
| music          = Mokhtar El Said
| screen play    = Wahid Hamid
| studio         = EL Nahar
| distributor    = Saout El Fen
| released       =  
| runtime        = 118 minutes
| country        = Egypt
| language       = Arabic
}}

Al Halfout is a 1985 film and satire written by Wahid Hamids and directed by Samir Seif   and Mustafa Gamaleddin. The cast includes Adel Emam, Ilham Chahine, Saeed Saleh and Salah Kabil. Filming of Al Halfout occurred mainly in Egypt. The film takes a satirical look at Egypts government and its attitude to the poor.  

==Concept==
The movie examines life in the suburbs of Egypt. It focuses on a small village affected by social inequality and a corrupt regime. The role of government is not apparent whereas landlords and rich businessmen appear as unrestrained criminals. To survive, the villagers sell unusual services such as witchcraft. One lady opens a gentlemens club. The village is timeless with the only dating feature being the train line.

==Protagonists==

===Arafa===
Adel Emam plays Arafa, a 34 year old grocery delivery man who likes to hang out at the market or the railway station. He is lean, clumsy and odd; disorganised with a slow way of speaking. He is poor with no education or prospects. He dreams of marriage but events in the village make this more and more unlikely.

===Dosoki===
Dosoki, played by Saeed Saleh, works in a cafe at the railway station. He is an Arafas best friend and does his best to help and consol him. For example, he will give Arafa money or help him to find a different job.

===Warda===
Warda is a widow played by Ilham Chahine. Arafa maries her.

==Plot==
Arafas progress is influenced by traditional cultural practices such as sorcery and by degeneration of society into activities such as prostitution. Despite these things, Arafa eventually marries the widow Warda. A few days later, Arafa finds himself jobless when a younger man takes his place at the market. A local hitman kills a famous businessman. Arafa has the opportunity to assume the identity of the hitman (neither he nor the hitman have ever been seen by the boss intermediary). Arafa is hired to make a hit. However, when Dosoki is killed by mafia in a gun fight, Arafa tries to escape. Warda discovers this and follows Arafa only to be killed by the mafia herself. Arafa kills all the mafia.

==Cast==
* Adel Emam as Arafa MashawirWitchcraft
* Ilham Chahine as Warda
* Saeed Saleh as Dosoki
* Salah Kabil as Asran

== References ==
 

== External links ==
*  

 
 
 