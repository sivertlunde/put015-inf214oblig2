Do I Have to Take Care of Everything?
{{Infobox film
| name = Do I Have to Take Care of Everything?
| image = Pitääkö mun kaikki hoitaa.jpg
| director = Selma Vilhunen
| producer = Elli Toivoniemi
| based on = 
| writer = Kirsikka Saari
| released =  
| country = Finland
| language = Finnish
}} Best Live Action Short Film at the 86th Academy Awards.  The film is a comedy about a busy morning in a family and a mother who is trying to take care of everything by herself. 

Do I Have to Take Care of Everything? is the second Finnish film as an Academy Award nominee. The first was The Man Without a Past by Aki Kaurismäki in 2002. 

The film had one of only two U.S. screenings at the Chicago Comedy Film Festival in 2012.

==Plot==
Married couple Sini and Jokke sleep in past their alarm.  Sini awakes in shock realizing that they will be late to a wedding.   She wakes up her two daughters and everyone hurries to get ready.  The girls can’t find their dresses, and Sini discovers them in the washing machine, still soaking wet.  She instructs the girls to find something “to wear to a party.”

She then looks through the house trying to find the wedding gift.  Unable to find it she starts making a handmade card and spills coffee on her husband, staining his shirt.  With no gift, he suggests taking a potted house plant.  Sini starts to argue and the girls walk into the living room wearing Halloween costumes saying they wore them to a friend’s party.

With no time to change, Sini grabs the house plant and the family runs out of the house to catch the bus.  While running for the bus Sini trips, shattering her heel and smashing the potted plant.  She picks herself up, and carrying the now broken plant, hurries her family onto the bus.

The frantic family runs into the church, but find that they have walked into a funeral service.  Sini and Jokke realize in shock they have the wrong day.  The Priest calls them up to pay their respects.  Jokke stoically addresses the church while Sini places the trashed house plant and the card (which says congratulations) on the casket.

Outside the church Sini stands in shock.  The little girls ask what they are going to do now.  Jokke informs them “absolutely nothing” and gives Sini a tender kiss.  Sini enthusiastically reciprocates and the family goes to have a picnic in the cemetery.

== Cast ==
*Joanna Haartti as Sini Ketonen
*Santtu Karvonen as Jokke Ketonen
*Ranja Omaheimo as Ella
*Ella Toivoniemi as Kerttu
*Jukka Kärkkäinen as Priest

== References ==
 

 
 
 
 


 