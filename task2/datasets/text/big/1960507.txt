Small Time Crooks
{{Infobox film
| name = Small Time Crooks
| image = Small time crooks.jpg
| caption = Theatrical release poster
| director = Woody Allen
| producer = Jean Doumanian
| screenplay = Woody Allen
| starring = Woody Allen Tracey Ullman Elaine May Hugh Grant
| cinematography = Zhao Fei
| editing = Alisa Lepselter FilmFour
| distributor = DreamWorks Pictures 
| released =  
| runtime = 95 minutes
| country = United States
| language = English
| budget = $18 million
| gross = $29,934,477
|}} Golden Globe Award for Best Actress – Motion Picture Musical or Comedy, and May won the Best Supporting Actress citation at the National Society of Film Critics Awards.

==Plot==
Career criminal Ray (Woody Allen) and his cronies want to lease an old restaurant so they can tunnel from the basement of the restaurant to a nearby bank. Rays wife Frenchy (Tracey Ullman) covers what they are doing by selling cookies from the restaurant. The bank robbery scheme is a miserable failure, but after they franchise the business, selling cookies makes them millionaires.

In the films second act, Frenchy throws a big party and overhears people making fun of their decorating taste and lack of culture.  She asks an art dealer named David (Hugh Grant) to train her and Ray so they can fit in with the American upper class. Ray hates every minute of it, but Frenchy likes their new culture.

What Ray and Frenchy do not know is that David is really just using Frenchy to finance his art projects. Ray finally gets fed up and leaves Frenchy. David and Frenchy go to Europe for more cultural enlightenment and while there, she gets a call and finds out she has been defrauded by her accountants. Shes lost everything including her cookie company, home, and possessions. David immediately dumps her.

Meanwhile, Ray has gone back to being a crook and tries to steal a valuable necklace at a party. He has had a duplicate made and through a series of circumstances gets the duplicate and real one mixed up. At the party he finds out that Frenchy is broke so he leaves and goes to see her. He consoles her by saying he stole the valuable necklace and shows it to her. Her new-found cultural enlightenment enables her to tell the necklace was a fake; Ray has gotten the wrong one. But she produces a cigarette case that she had given to David as a gift, and stole back after he dumped her. It once belonged to the Duke of Windsor.  They reconcile, decide to sell it and retire to Florida.

==Cast==
* Woody Allen as Ray Winkler
* Tracey Ullman as Frances "Frenchy" Fox-Winkler
* Elaine May as May Sloane
* Elaine Stritch as Chi-Chi Potter
* Kristine Nielsen as Emily Bailey
* Hugh Grant as David Perrette
* Michael Rapaport as Denny
* Tony Darrow as Tommy
* Jon Lovitz as Benjamin "Benny" Bukowski
* George Grizzard as George Blint
* Larry Pine as Charles Bailey

==Critical reception==
The film received generally positive reviews from critics. The review aggregator Rotten Tomatoes reported that the film received 66% positive reviews, based on 96 reviews.  Metacritic reported the film had an average score of 69 out of 100, based on 32 reviews. 

Ullman was nominated for a Golden Globe Award for Best Actress – Motion Picture Musical or Comedy for her performance, and Elaine May won Best Supporting Actress at the National Society of Film Critics Awards for her performance.

Small Time Crooks was the highest grossing film directed by Allen at the North American box office between 1989s Crimes and Misdemeanors and 2005s Match Point with a gross of $17.2 million. However it was also one of the few later Allen films which did less well outside the U.S. and Canada, and its global gross was $29.9 million. 

The films plot is very similar to that of the 1942 comedy Larceny, Inc.. Robert Osborne of Turner Classic Movies on June 15, 2006  Allen has never commented on whether this was deliberate or if his film was in any way inspired by Larceny, Inc..  The plot also parallels an episode of the American TV series Gomer Pyle entitled "Dynamite Diner". 

==See also==
*Woody Allen bibliography
*Woody Allen filmography

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 