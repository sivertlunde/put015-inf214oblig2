All About My Wife
{{Infobox film
| name           = All About My Wife 
| image          = All-about-my-wife-poster.jpg
| film name      = {{Film name
| hangul         =        
| rr             = Nae anae-ui modeun geot
| mr             = Nae anae-ŭi modŭn gŏt}}
| director       = Min Kyu-dong 
| producer       = Park Joon-ho   Min Jin-soo   Lee Yoo-jin 
| based on       =  
| writer         = Heo Sung-hye   Min Kyu-dong
| starring       = Im Soo-jung   Lee Sun-kyun   Ryu Seung-ryong 
| music          = Lee Jin-hee   Kim Jun-seong
| cinematography = Kim Dong-young 
| editing        = Kim Sun-min
| distributor    = 
| released       =  
| runtime        = 121 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          =     
}}
All About My Wife ( ) is a 2012  . Retrieved 2013-02-03.  Starring Im Soo-jung, Lee Sun-kyun and Ryu Seung-ryong,   the movie was released in theaters on May 17, 2012. 

It is a remake of the Argentinean film Un novio para mi mujer ("A Boyfriend for My Wife"). 

==Plot==
After seven years of marriage, mild-mannered Doo-hyun (Lee Sun-kyun) is at the end of his rope.  Jung-in (Im Soo-jung), his wife, is beautiful, sexy, and a fantastic cook, but drives her husband crazy with her endless nagging and complaining.  He can’t even bring himself to ask for a divorce because of the fights that will follow. When Doo-hyun’s company transfers him out of state, it seems like his dream of getting away is coming true.  But to his horror, Jung-in surprises him by moving across the country to be with him. Desperate but too afraid to ask for a divorce, Doo-hyun recruits his next-door neighbor and legendary Casanova Sung-ki (Ryu Seung-ryong) to seduce his wife and make her leave him first.  After scoping her out, Sung-ki is intrigued by the challenge and confidently agrees to seduce Jung-in as his career finale. Meanwhile, to give her something to do, Doo-hyun has already arranged for Jung-in to get a spot on the local radio station, shooting her mouth off about lifes injustices. True to his reputation, Sung-ki eventually succeeds in grabbing Jung-in’s attention, and the two slowly develop feelings for each other. But though Doo-hyun asked for it, he grows to regret his decision and decides to spy on his wife and her lover.   

==Cast==
*Im Soo-jung - Yeon Jung-in    
*Lee Sun-kyun - Lee Doo-hyun  
*Ryu Seung-ryong - Jang Sung-ki  
*Lee Kwang-soo - PD Choi, radio host  Kim Ji-young - Song, radio writer
*Kim Jung-tae - Park Kwang-shik, Doo-hyuns colleague  Lee Sung-min - Company Director Na, Doo-hyuns boss
*Kim Do-young - Nas wife
*Jung Sung-hwa - newspaper delivery man
*Lee Dal-hyeong - captain at police station
*Park Hee-bon - female cop at police station
*Jo Han-cheol - public officer at divorce court
*Nam Myung-ryul - judge at divorce court
*Lee Do-ah - Pyeongchang Company employee
*Kim Sun-ha - waitress at noodle shop

==Box office== The Avengers and Men in Black 3.  Benefiting from positive word-of-mouth, it continued its impressive commercial run, with over List of highest-grossing films in South Korea|4.5 million admissions in total.    

==Awards and nominations==
2012 Buil Film Awards
*Nomination - Best Actress - Im Soo-jung
*Nomination - Best Supporting Actor - Ryu Seung-ryong
*Nomination - Best Screenplay - Min Kyu-dong, Heo Sung-hye

2012 Grand Bell Awards
*Nomination - Best Actress - Im Soo-jung
*Nomination - Best Supporting Actor - Ryu Seung-ryong
 2012 Blue Dragon Film Awards 
*Best Actress - Im Soo-jung
*Best Supporting Actor - Ryu Seung-ryong
*Nomination - Best New Actor - Lee Kwang-soo
*Nomination - Best Screenplay - Min Kyu-dong, Heo Sung-hye
*Nomination - Best Art Direction - Jeon Kyung-ran
*Nomination - Best Music  - Kim Jun-seong, Lee Jin-hee

2012 Women in Film Korea Awards   
*Best Actress - Im Soo-jung

2013 KOFRA Film Awards (Korea Film Reporters Association)  
*Best Supporting Actor - Ryu Seung-ryong
 2013 Baeksang Arts Awards   
*Nomination - Best Director - Min Kyu-dong
*Nomination - Best Actress - Im Soo-jung
*Nomination - Best Supporting Actor - Ryu Seung-ryong
*Nomination - Best Screenplay - Min Kyu-dong, Heo Sung-hye

==References==
 

== External links ==
*    
*    
*  
*  
*  

 

 
 
 
 
 
 
 
 
 