The Road to Ruin (1934 film)
{{Infobox film
| name           = The Road to Ruin
| image          = Road to Ruin 1934 dvd.jpg
| image_size     =
| caption        = DVD cover (detail)
| director       = Dorothy Davenport Melville Shyer
| producer       = Willis Kent
| writer         = Dorothy Davenport Willis Kent (uncredited)
| narrator       = Helen Foster Nell ODay Glen Boles
| music          = James Diamond
| editing        = S. Roy Luby
| distributor    = True-Life Photoplays First Division Pictures
| released       =  
| runtime        = 62 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

Road to Ruin is a 1934 exploitation film directed by Dorothy Davenport, under the name "Mrs. Wallace Reid", and Melville Shyer, and written by Davenport with the uncredited contribution of the films producer, Willis Kent. The film, which is in the public domain, is about a young girl whose life is ruined by sex and drugs.

==Cast== Helen Foster as Ann Dixon
*Nell ODay as Eve Monroe
*Glen Boles as Tommy
*Robert Quirk as Ed Paul Page as Ralph Bennett
*Richard Hemingway as Brad
*Virginia True Boardman as Martha Dixon Richard Tucker as Mr. Dixon Donald Kerr as Drunk shooting dice
*Eleanor Thatcher as Dancer
*Neal Pratt as Brad
*Jimmy Tolson as Club Singer Jimmy

Director/writer Dorothy Davenport appears in the film in the role of "Mrs. Merrill."

==Production==
The Road to Ruin is a sound re-make of a 1928 silent film of the same name, written and produced by Willis Kent and also starring Helen Foster. TCM    Foster, reprising her role as a high school girl, was 27 years old at the time, and six years older than her on-screen boyfriend, Glen Boles.

The titles and composers of the three songs performed in the film are not recorded. 
 Film censors in Virginia required a "record number" of cuts in the film before clearing it for release, according to Film Daily, while in Detroit, the film was boycotted by the Catholic Church, but was cleared by the local censors after some cuts. 

A novelization of the film was put out by the producers, apparently intended for use by school and civic groups as an aid to discussion of the social problems presented in the film: teenage drinking, promiscuity, pregnancy and abortion. 

==Reception==
The reviewer for Variety (magazine)|Variety found the film "restrained" in comparison to the more "hotly sexed" silent version, while other reviewers found it to be an improvement over the earlier film, and "sensational".  A modern critic called the film "  sordid drive down the path of moral and physical degradation, capped off with just enough of a moral lesson to alleviate any guilt the viewer might feel for watching such a decadent display." 

==Notes==
 

===Bibliography===
* Felicia Feaster and Bret Wood. Forbidden Fruit: The Golden Age of Exploitation Film. (Midnight Marquee Press, 1999). ISBN 1-887664-24-6

==See also==
*Pre-Code sex films

==External links==
 
* 
* 
* 
* 

 
 
 
 
 
 
 
 
 