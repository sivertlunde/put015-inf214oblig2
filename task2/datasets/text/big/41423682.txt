Appaji
{{Infobox film
| name = Appaji
| image =
| caption = 
| director =D. Rajendra Babu
| story = K. V. Vijayendra Prasad
| screenplay = D. Rajendra Babu
| writer = Richard Louis (dialogues)
| producer = H S Janakiram   C S Manju Vishnuvardhan  Aamani   Pankaj Dheer
| music = M. M. Keeravani
| cinematography = Prasad Babu
| editing = K. Balu
| studio  = Surya International
| released =  January 4, 1996
| runtime = 139 min
| language = Kannada  
| country = India
}}
 Vishnuvardhan and Aamani in the lead roles. The film is directed by D. Rajendra Babu and written by Vijayendra Prasad and produced under Surya International banner. The film, upon release, met with positive reviews and declared a blockbuster. The music was scored by M. M. Keeravani.

==Cast== Vishnuvardhan as Appaji
*Aamani
*Pankaj Dheer Sharanya
*Sihi Kahi Chandru
*Doddanna
*Keerthi
*Krishne Gowda
*M. S. Karanth

==Release==
The film was released on January 4, 1996 across Karnataka state cinema halls. The film met with positive response at the box-office. 

==Soundtrack==
All the songs are composed and scored by M. M. Keeravani. 

{|class="wikitable"
! Sl No !! Song Title !! Singer(s) || Lyricist
|-
| 1 || "Ene Kannadathi" || S. P. Balasubramanyam || R. N. Jayagopal
|-
| 2 || "Balu Dambada" || Mano (singer)|Mano, K. S. Chithra || R. N. Jayagopal
|-
| 3 || "Maama Jaba Jaba" || Malgudi Subha || R. N. Jayagopal
|-
| 4 || "Yaava Deva Shilpi" || S. P. Balasubramanyam, K. S. Chithra || R. N. Jayagopal
|-
| 5 || "Baanu Ello Kanenu" || S. P. Balasubramanyam, K. S. Chithra || R. N. Jayagopal
|-
| 6 || "Gharshane Gharshane" || S. P. Balasubramanyam || R. N. Jayagopal
|-
|}

==References==
 

== External links ==
* 

 
 
 
 
 
 
 
 
 


 
 