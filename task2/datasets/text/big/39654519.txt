Vaalee (film)
{{Infobox film
| name       = Vaalee
| image      = Vaalee Kannada Movie.jpg
| director   = S. Mahendar
| producer   = Ramesh Yadav
| story      = S. J. Suryah
| screenplay = 
| starring   = Sudeep Poonam Singar
| music      = Rajesh Ramanath
| cinematography = Sundarnath Suvarna
| editing    = P.R. Soundar Raj
| studio     = 
| distributor = 
| released   =  
| country    = India
| language   = Kannada
| runtime    = 159&nbsp;min
| budget     = 
| gross      = 
| website    =  
}}
 Kannada romance-thriller-drama film directed by S. Mahendar featuring Sudeep and Poonam Singar in the lead roles. The film features background score and soundtrack composed by Rajesh Ramanath and lyrics by K. Kalyan. The film released on 19 October 2001. This film is remake of Tamil movie Vaali.    

==Plot==


Deva and Shiva (both played by Sudeep) are twins. Deva, the elder, is deaf and mute. But he is a genius, an expert at lip-reading and the head of a successful advertising company. Shiva loves and trusts his brother. Priya (Poonam Singar) wants to marry only someone who is an ex-smoker, an ex-drunkard and ditched by a girl but still pining for her. Learning this, Shiva invents an old romance between him and Sona and finds his way into Priyas heart.

Deva meanwhile chances upon Priya and becomes obsessed with attaining her. His obsession continues even after his younger brother gets married to the girl of his dreams and he devises various means of getting close to Priya and keeping Shiva and her separated. Some of the methods Deva uses to woo Priya are masochistic (wounding his hand by the running car engine to stop the couple’s first night) and psychotic (trying to kill his brother in so many ways).

Priya realises the not-so-honourable intentions Deva has towards her but Shiva refuses to believe her and has full faith in his brother. He even goes as far as to take Priya to a psychiatrist. To get away from it all, Shiva and Priya go on a long-delayed honeymoon. But Deva shows up there too. Shiva watches Deva kissing the photo of Priya and realises Priya was right all along. While Shiva is away, Priya has to take care of Deva. Deva beats Shiva mercilessly, packs the unconscious Shiva in a gunny bag and throws him in a lorry.

Deva disguises himself as Shiva and goes near Priya. Priya comes to know he is Deva and escapes from him before shooting him with her revolver. Deva falls in pool and dies. Devas soul talks about his inability to express his feeling as he was mute. Shiva comes and she narrates the whole incident to him. Both of them hug each other tearfully.

==Cast==
*Sudeep as Shiva, Deva (Dual Role)
*Poonam Singar as Priya
*Sadhu Kokila as Vicky
*Gurukiran as Ravi (Guest Appearance)
*Bank Janardhan as shop owner
*Shanthammma as (Shiva, Deva)s   grandmother

==Soundtrack==
{{Infobox album
| Name = Vaale
| Type = soundtrack
| Artist = Rajesh Ramanath Kannada
| Label = Anand Audio
| Producer = 
| Cover = 
| Released    =   Feature film soundtrack
| Last album = 
| This album = 
| Next album = 
}}
Soundtrack is composed by Rajesh Ramnath reusing all the tunes from the original Tamil version which was originally composed by Deva. 

{{Track listing
| total_length   = 
| lyrics_credits = K. Kalyan
| extra_column	 = Singer(s)
| title1	= O Sona O Sona
| lyrics1 	= K. Kalyan Hariharan and Sudeep
| length1       = 
| title2        = Vasantha Maasadalli
| lyrics2 	= K. Kalyan Unni Krishnan and Anuradha Sriram
| length2       = 
| title3        = Dil Meri Dil
| extra3        = Gurukiran and Ganga
| lyrics3 	= K. Kalyan
| length3       = 
| title4        = Mele Chandrana
| extra4        = Anuradha Sriram and Rajesh
| lyrics4       = K. Kalyan
| length4       = 
| title5        = Chandirana Hididu Unni Krishnan and Anuradha Sriram
| lyrics5       = K. Kalyan
| length5       =
}}    

==Awards==
{| class="infobox" style="width: 22.7em; text-align: left; font-size: 85%; vertical-align: middle; background-color: #eef;"
|-
| colspan="3" |
{| class="collapsible collapsed" width="100%"
! colspan="3" style="background-color: #d9e8ff; text-align: center;" | Awards and nominations
|- style="background-color:#d9e8ff; text-align:center;"
!style="vertical-align: middle;" | Award
| style="background:#cceecc; font-size:8pt;" width="60px" | Wins
| style="background:#eecccc; font-size:8pt;" width="60px" | Nominations
|-
|align="center"|
;Filmfare Awards South
| 
| 
|}
|- style="background-color:#d9e8ff"
| colspan="3" style="text-align:center;" | Totals
|-
|  
| colspan="2" width=50  
|-
|  
| colspan="2" width=50  
|}
Filmfare Awards South :- Best Film  
 Best Director - S. Mahendar  
 Best Music Director - Rajesh Ramanath    

==References==
 

==External links==
* 

 
 
 