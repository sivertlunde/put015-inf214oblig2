Dark Ride (film)
{{Infobox film name = Dark Ride producer = Daniel Bickel Chris M. Williams
| image	=	Dark Ride FilmPoster.jpeg director = Craig Singer writer = Craig Singer Robert Dean Klein starring = Jamie-Lynn Sigler Patrick Renna Jennifer Tisdale Andrea Bogart editing = Sam Bauer cinematography = Vincent E. Toto music         = Kostas Christides studio    = My2Centences Blue Omega Entertainment distributor = Lionsgate After Dark Films runtime = 90 minutes released =   country = United States language = English budget =  gross =
}}
Dark Ride is a 2006 American horror-thriller film directed by Craig Singer and written by Singer and Robert Dean Klein. It was selected to play at the 8 Films To Die For film festival, being one of the first 8 films to be in the 8 Films To Die For films series. The film revolves around a group of friends who are terrorized by a crazy masked murderer at a dark ride in an amusement park.

==Synopsis==

Two twin teenage girls, Sam and Colleen, enter the mysterious Dark Ride. Sam, who is tough and competitive, gets annoyed at Colleen because she is anxious and scared. The killer kidnaps Sam and slices her stomach, then brutally kills Colleen.

Ten years later, Cathy (Jamie-Lynn Sigler) and Liz (Jennifer Tisdale) are getting ready for Spring Break. They decide to take a road trip along with three of their male friends, Bill (Patrick Renna), Steve (David Clayton Rogers), and Jim (Alex Solowitz). The friends embark together in Jims van, and meet Jen (Andrea Bogart). After riding for some time, the van is in need of gasoline. While at the gas station, Bill wanders around trying to find the bathroom. When he joins the others, he claims to have found a pamphlet about the Dark Ride re-opening after many years of being closed. The group decides to make a detour to the amusement park and spend the night in the Dark Ride attraction. Once they arrive, Cathy decides to stay in the van while the others go into the Dark Ride attraction.

Liz, Steve, Jim, and Jen find a door inside. Jim switches on the power, which illuminates the lights and launches the ride, as well as its scary theatrical effects. The 4 then sit and smoke Marijuana. Bill tells them about the two girls that were killed ten years earlier, and reveals that they were his cousins. After some initial skepticism, they eventually all believe him. Jen and Steve wander into the hallway to fool around. Jen sees something and notices Cathys fake corpse sitting in a chair with her throat slashed. The prank was meant to be pulled on Steve, who is livid due to the trauma. Cathy argues with him and they both stop fighting when Bill breaks it up. Steve, angry about the prank, wanders off by himself.

The others are moving along when the power goes out. Jim goes to the basement to fix it, since he had first turned it on. Jen wanders into the basement and starts flirting with Jim, kissing and fellating him. The killer slides through a hidden entrance on the floor and cuts through Jens neck. Jim, unaware of what has happened, tries to kiss Jen and pulls her severed head off her body. He tries to run, but hits his head on a pipe and knocks himself out. The killer then takes Jens head upstairs.

While Jim and the now deceased Jen are in the basement, Liz, Cathy, and Bill start trying to find their way out of the ride. They stumble upon a body hanging from the ceiling, which Cathy realizes is Steves. The killer has apparently used Steves body as an attraction in the Dark Ride. Frightened, the girls go one way and Bill goes another.

Cathy is in the vehicle and screams but no one hears. Once she finds Lizs corpse the police detective arrives and insists everything will be all right. Cathy sees the killer behind the detective and tries telling him to back away. Not knowing this the killer slashes the officers head in half using a machete, revealing his blood, veins, and brain. Cathy screams and the killer looks at her with a brief smile on his face. Cathy runs up the stairs, finds an opening, jumps out to safety, and gets into the van.

Using her cellphone, Cathy tries calling her friends but cant get Mobile phone signal|service. Meanwhile, the killer attempts to murder Jim with his hook scraping through the concrete. Cathy accelerates the van into the building, impaling the killer on a wall of spikes. This causes Cathy to pass out, and Jim goes to check on her. Bill appears and tells Jim that the killer is actually his brother and that he has been committing the murders for him, and stabs Jim. Bill thanks Cathy, who runs out of the Dark Ride and falls to her knees as she hears sirens approaching. The film ends showing what appears to be Bill wearing the killers mask.

==Production and release==

Filming began 25 October 2004 and finished 19 November 2004. The film saw a limited release on November 17, 2006 at the "After Dark Horrorfest", an event in which movies "too graphic" for theaters are finally shown to the public for one weekend only, across several states in the United States.

The DVD was released on March 27, 2007.

==Cast==
  stars in the film, playing Cathy.]]

{| class="wikitable sortable" style="text-align:center; font-size:85%"
|-
! width="50%" | Actor/Actress
! width="50%" | Role
|-
!Jamie-Lynn Sigler Cathy
|-
!Patrick Renna Bill
|-
!David Clayton Rogers Steve
|-
!Alex Solowitz Jim
|-
!Andrea Bogart Jen
|-
!Jennifer Tisdale Liz
|-
!Brittany Coyle Colleen
|-
!Chelsea Coyle Samantha
|-
!David Warden Jonah
|-
!Jim Williams Ticket Taker
|-
!Erin Dawson Hippie
|-
!Jack Doner Old Man
|-
!David Ury Attendant #1
|-
!Atticus Todd Attendant #2
|-
!Steve Mattila Homeless Man
|-
!Damon Standifer Reggie
|-
!Julie Bickel Teen Female #1
|-
!Jessica Lobaina Teen Female #2
|-
|}
Twin actresses Chelsey and Brittney Coyle play twin sisters in the movie. The film was Dave Wardens debut role.

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 