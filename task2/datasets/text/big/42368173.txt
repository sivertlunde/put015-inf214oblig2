Lion Jagapathi Rao
{{Infobox film name           = Lion Jagapathi Rao image          = image_size     = caption        = director       = Om Sai Prakash producer       = writer         = Sainath Thotapalli narrator       = starring  Vishnuvardhan Bhavya Lakshmi
|music          = Upendra Kumar cinematography = D. V. Rajaram editing        = Victor Yadav studio         = Sri Vinayaka Combines released       =   runtime        = 144 minutes country        = India language       = Kannada budget         =
}}
 Kannada drama Vishnuvardhan in Lakshmi and Bhavya in the lead roles.  The film was produced by Sri Vinayaka Combines.
 Best Actor for the year 1991. 

==Cast== Vishnuvardhan  Lakshmi 
* Bhavya
* Mukhyamantri Chandru
* Aravind
* Sadashiva Saliyan
* Rajanand
* Ramamurthy
* Umesh
* Mysore Lokesh
* Shanthamma

==Soundtrack==
The music of the film was composed by Upendra Kumar and lyrics written by R. N. Jayagopal. 

{{Infobox album  
| Name        = Lion Jagapathi Rao
| Type        = Soundtrack
| Artist      = Upendra Kumar
| Cover       = 
| Alt         = 
| Released    =  
| Recorded    =  Feature film soundtrack
| Length      = 
| Label       = Sangeetha
}}

{{Track listing
| total_length   = 
| lyrics_credits = yes
| extra_column	 = Singer(s)
| title1	= Kumar Kumar
| lyrics1 	= R. N. Jayagopal
| extra1        = S. P. Balasubrahmanyam, S. Janaki
| length1       = 
| title2        = Swagatha Nimage
| lyrics2 	= R. N. Jayagopal
| extra2        = S. Janaki
| length2       = 
| title3        = Vidhyabuddhi
| lyrics3       = R. N. Jayagopal
| extra3 	= S. Janaki
| length3       = 
| title4        = Enidu Shodhane
| extra4        = S. P. Balasubrahmanyam
| lyrics4 	= R. N. Jayagopal
| length4       = 
| title5        = Agajanana
| extra5        = S. P. Balasubrahmanyam
| lyrics5       = Traditional
| length5       = 
| title6        = Premake Permit Banthu
| extra6        = S. P. Balasubrahmanyam, S. Janaki
| lyrics6       = R. N. Jayagopal
| length6       =
}}

==References==
 

==External source==
*  
*  

 
 
 
 
 


 

 