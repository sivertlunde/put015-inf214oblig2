Naked Ambition: An R Rated Look at an X Rated Industry
{{Infobox film
| name           = Naked Ambition: An R Rated Look at an X Rated Industry (film)  
| image          = Naked Ambition- An R Rated Look at an X Rated Industry.jpg
| alt            = 
| caption        = 
| film name      =  
| director       = Michael Grecco Charles Holland
| writer         = Charles Holland
| screenplay     = 
| story          = 
| based on       =  
| starring       = 
| narrator       = Michael Grecco
| music          =  
| cinematography =  
| editing        = Jeremy Troy
| production companies =  
| distributor    = Lantern Lane Entertainment
| released       =  
| runtime        = 81 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =  
}}

{{Infobox book 
  | name           =  Naked Ambition: An R Rated Look at an X Rated Industry (book)
  | image          =   
  | author         =   
  | editor         =  
  | country        =  United States English
 Sexuality
  | publisher      =  Rock Out Books
  | release_date   =  November 11, 2007
  | media_type     =  Print (hardcover)
  | pages          =  224
  | isbn           =  0-9793314-0-4
  | oclc= 190967572
}}

Naked Ambition: An R Rated Look at an X Rated Industry is a 2009 American documentary film written by Charles Holland and directed by Michael Grecco based upon the Greccos third portrait photography book by the same name. 

In both the documentary and his book, Grecco, sets out to capture the energy and spirit of the AVN Awards and convention where American pornography is displayed, celebrated and honored.

==Film==
A documentary based on the shooting for the book and directed by Michael Grecco premiered in April 2009. The film was a production by Lantern Lane Entertainment in association with KHG Documentary.  

===Partial cast===
 
* Michael Grecco
* Tera Patrick 
* Evan Seinfeld 
* Heather Veitch 
* Jesse Jane 
* Paul Fishbein
* Sunny Lane 
* Joanna Angel 
* Nautica Thorn
* Rob Rotten
* Luc Wylder
* Alexandra Silk
* Janine Lindemulder 
* Julia Ann  Peter North Randy West
* Steve Hirsch
* Brittney Skye
* Greg Fitzsimmons
* Evan Stone
* Stormy Daniels
* Ron Jeremy Mary Carey  Paul Thomas 
* Jenna Jameson
* Larry Flynt Cytherea
* McKenzie Lee
 

==Book== adult entertainment, Las Vegas, the convention which surrounds the annual AVN Awards.   The books forewords, written by rock musician Dave Navarro and Hustler magazine editor Larry Flynt, discuss modern cultures acceptance of pornography into the mainstream, which allows an overtly sexual work like this to be considered a coffee table book.

Included among the portrait subjects are Larry Flynt, Jenna Jameson, Ron Jeremy, Chi Chi LaRue, Tera Patrick, Alexandra Silk, Katsuni, Dana DeArmond, Sunny Lane, Kurt Lockwood, and Michelle Aston. Uncommon in pornography-related works, the photographs do not seem to have been altered with any significant image editing or photo manipulation, thus avoiding the layer of fantasy usually applied to actors in this field. 

The book was edited by former RIP magazine editor Lonn Friend and former FHM editor Rob Hill.

===Reception===
Variety (magazine)|Variety compared the book to the film in their review, praising both and writing "In contrast with Grecco’s evocative photos, his film is scattered and platitudinous, skimming the surface of his subjects and betraying his high-art aspirations with a shallow E! aesthetic. Reliable interest in the subject matter should land the film, which opens theatrically May 1, a home on an adventurous cabler, though such a contentious substratum of American culture deserves more thoughtful treatment."   

Hollywood Reporter wrote that "Greccos book is filled with stunning portraits and nicely captures his subjects personalities", but it was felt the film was too tame and seemed more an extended infomercial for the book.   

DVD Verdict made note that Grecco appears to "play the pornography-as-free-speech card," in his depicting pornography as "just regular folks who express themselves through public fornication,"   expanding that he portrays pornography as "one big happy family", and ignores the problems affecting the industry: drug use, exploitation, and abuse of young women who come to L.A. to become part of "this supposedly upstanding industry."   His message is "smut is all about consenting adults, the privacy of their own bedrooms, and the First Amendment."   It is also noted that with Grecco publishing a book about the topic, his "sugarcoating the business serves his interests," but through his repeated comparisons of porn actors to main stream film stars, he "misses an opportunity to show the irony inherent in glossing up pornography," and shows a "willful ignorance of the fact that most women in the porn industry are not icons of empowerment, but figurines with a limited shelf life and zero opportunities beyond sex work."   The reviewer wrote "the fact that Grecco glosses over the negatives of a business rife with them makes his documentary disingenuous—and worse, boring."   It was concluded "in the end, Naked Ambition is as dreary and pedestrian as its punny title. Consenting adults or not, Greccos subjects are fringe players who doth protest way too much about the wonderfulness of the porn industry, but their rah-rah cheerleading in support of it is more sorry than celebratory."   

==References==
 

==External links==
*  

*  
*  

 
 
 