Vrijdag
{{Infobox film
| name           = Vrijdag
| image          = 
| image size     = 
| caption        = 
| director       = Hugo Claus
| producer       = Jan van Raemdonck
| writer         = Hugo Claus
| starring       = Frank Aendenboom
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =     
| runtime        = 96 minutes
| country        = Belgium Netherlands
| language       = Dutch
| budget         = 
}}

Vrijdag is a 1980 Belgian-Dutch drama film directed by Hugo Claus, based on his 1969 play (a "classic" Dutch play).  It was entered into the 31st Berlin International Film Festival.   

==Cast==
* Frank Aendenboom - Georges
* Jakob Beks - Model voor Jules
* Chris Cauwenbergs - Verpleger in gevangenis
* Dirk Celis - Advokaat Georges
* Kitty Courbois - Jeanne
* Theo Daese - Alex
* Jo De Caluwé - Bewaking Delhaize
* Herman Fabri - Secretaris
* Herbert Flack - Erik
* Blanka Heirman - Buurvrouw
* Karin Jacobs - Solange
* Guido Lauwaert - Agent
* Ann Petersen - Moeder van Erik
* Fons Rademakers - Chef van Jules
* Roger Raveel - Bijtebier
* Hugo Van Den Berghe - Jules
* Raymond Van Herbergen - Maurice
* Suzy Van Herbergen - Vrouw op begrafenis
* Fred Van Kuyk - Dokter
* Hilde Van Mieghem - Christiane
* Nick Van Suyt - Bewaking Delhaize

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 