Kavalukku Kettikaran
{{Infobox film
| name           = Kavalukku Kettikaran
| image          = KavalukkuKettikaran.jpg
| caption        = LP Vinyl Records Cover
| director       = Santhana Bharathi
| producer       = V. Kalanithi S. Thananchayan R. Murugasamy
| story          = Ananthu
| screenplay     = Karunanidhi
| writer         = Karunanidhi
| starring       =  
| music          = Ilaiyaraaja
| cinematography = M. M. Rengasamy
| editing        = Durairaj R. Dhinakaran
| studio         = Thirai Koodam
| distributor    = Thirai Koodam
| released       =  
| country        = India
| runtime        = 140 min
| language       = Tamil
}}
 1990 Cinema Indian Tamil Tamil film, Prabhu and Nirosha in lead roles. The film had musical score by Ilaiyaraaja and was released on 14 January 1990.   
 
==Plot==

Panjavarnam (V. K. Ramaswamy (actor)|V. K. Ramasamy) is a retired police officer and he wants that his anxious son Dilipan (Prabhu (actor)|Prabhu) becomes also a police officer. Dilipan works in a school as teacher and he falls in love with Arivukodi (Nirosha), a new teacher. She later accepts his love.

Panjavarnam manages to fire his son and sends him to the police training. Dilipan becomes a sub-inspector and he is transferred in Arivukodis village. Arivukodi is also fired after slapping the school headmaster. Dilipan doesnt join immediately and lives with Siva (Nassar). Balayya (Rajesh (actor)|Rajesh), a corrupt politician, spreads terror among the villagers.

Dilipan saves Arivukodis father from Balayyas henchmen. Arivukodi tells the reason to her lover: Arivukodis father and Balayya were rich villagers, Balayya wanted to appropriate his land. Balayya sent Arivukodis father in jail, grabbed his land and Arivukodis committed suicide.

Siva, an angry villager, decides to sell his harvests in the city. Balayyas henchmen beats Siva, but Dilipan saves by giving him his blood. Balayya rapes Chellakili, Sivas wife, in front of Dilipan and she commits suicide.

Dilipan joins as sub-inspector in the police station and he fails to arrest Balayya because of his political background. Back form the hospital, Siva decides to kill and Dilipan arrests him. Mannar (S. S. Chandran) reveals that Balayya stole the temple Murugan statue, he arrests Balayya and his superior releases Balayya.

Balayya kidnaps Siva from the jail and Dilipan is suspended. The villagers rebels against Balayya and his henchmen. During the confrontation, Balayya dies in a car accident.

==Cast==
 Prabhu as Dilipan
*Nirosha as Arivukodi Rajesh as Balayya
*Nassar as Siva
*V. K. Ramaswamy (actor)|V. K. Ramasamy as Panjavarnam, Dilipans father Manorama as Ponnuthayi, Dilipans mother
*S. S. Chandran as Mannar
*Venniradai Moorthy as a school headmaster
*Major Sundarrajan as a police trainer
*Pandu as Kannayya
*R.S. Shivaji as Kittu, Dilipans friend
*T. S. Raghavendra as Arivukodis father
*Baby Jennifer as Sivas son
*Karunanidhi in a cameo appearance

== Soundtrack ==

{{Infobox Album |  
  Name        = Kavalukku Kettikaran |
  Type        = soundtrack |
  Artist      = Ilaiyaraaja |
  Cover       = |
  Released    = 1990 |
  Recorded    = 1989 | Feature film soundtrack |
  Length      = |
  Label       = |
  Producer    = Ilaiyaraaja |  
  Reviews     = |
  Last album  = |
  This album  = |
  Next album  = |
}}

The film score and the soundtrack were composed by film composer Ilaiyaraaja. The soundtrack, released in 1990, features 5 tracks with lyrics written by Ilaya Bharathi and Karunanidhi. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Duration 
|-  1 || "Amma Amma" || Malaysia Vasudevan || 4:37
|- 2 || "Ithazhenum" || Mano (singer)|Mano, S. Janaki ||  5:15
|-  3 || "Kavalukku" || Ilaiyaraaja || 2:50
|- 4 || "Kutham" || Malaysia Vasudevan ||  4:58
|-  5 || "Solai Ilangkuil" || Mano, K. S. Chithra || 5:08
|}

==External links==
* 

==References==

 

 
 
 
 
 