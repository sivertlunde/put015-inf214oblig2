Desire (1936 film)
{{Infobox film
| name           = Desire
| image          = Desire (film).jpg
| caption        = Theatrical French Poster
| director       = Frank Borzage
| producer       = Frank Borzage Ernst Lubitsch
| writer         = Screenplay: Edwin Justus Mayer Waldemar Young Samuel Hoffenstein Story: Hans Székely Robert A. Stemmle John Halliday William Frawley
| music          = Frederick Hollander
| cinematography = Charles Lang Victor Milner William Shea
| distributor    = Paramount Pictures
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English Spanish
| budget         =
}}
Desire is an American romantic drama film released in 1936 and directed by Frank Borzage.  It was produced by Borzage and Ernst Lubitsch.  The picture is a remake of the 1933 German film Happy Days in Aranjuez. The screenplay was written by Samuel Hoffenstein, Edwin Justus Mayer and Waldemar Young based on the play Die Schönen Tage von Aranjuez by Hans Székely and Robert A. Stemmle. The music score was composed by Frederick Hollander and the cinematography was shot by Charles Lang and Victor Milner. Marlene Dietrichs wardrobe was designed by Travis Banton.
 John Halliday, William Frawley, Akim Tamiroff,and Alan Mowbray.

==Plot== John Halliday). It doesnt take long for Tom to figure out what de Beaupre and Margoli are up to. Yet, Bradley also knows that hes fallen for de Beaupre, and hes willing to go along as long as hes near her.

==Cast==
* Marlene Dietrich as Madeleine de Beaupre
* Gary Cooper as Tom Bradley John Halliday as Carlos Margoli
* William Frawley as Mr. Gibson
* Ernest Cossart as Aristide Duvalle
* Akim Tamiroff as Avilia, Police Official
* Alan Mowbray as Dr. Maurice Pauquet
* Zeffie Tilbury as Aunt Olga

==Background==
  John Gilbert was initially cast as Carlos Margoli, which was to be his comeback role. He had a heart attack in his dressing room a few weeks later and was immediately replaced by John Halliday. A few days later, Gilbert died of alcohol-induced heart failure. Some of the scenes in the film were directed by Ernst Lubitsch whilst Frank Borzage was fulfilling a prior commitment at Warner Bros. The film was shot at Paramount Studios and at the Iverson Ranch, Chatsworth, California and, unusual for its time, on location in France and Spain.

Of the film, Dietrich said:
:The only film I need not be ashamed of is Desire, directed by Frank Borzage and based on a script by Ernst Lubitsch. I found Gary Cooper a little less monosyllabic than before. He was finally rid of Lupe Vélez, who had been at his heels constantly throughout the shooting of Morocco (1930 film)|Morocco.
and: 
:Desire became a good film and, moreover, also proved to be a box-office success. The script was excellent, the roles superb - one more proof that these elements are more important than actors. 

==Footnotes==
 

==External links==
*  
*  
*  
*   at Virtual History

 

 
 
 
 
 
 
 
 
 
 