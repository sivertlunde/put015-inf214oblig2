De Greep
{{Infobox film
| name           = De Greep
| image          = De Greep 1909.jpg 
| image_size     = 
| caption        = Publicity photo of De Greep showing Louis Bouwmeester as Jean-Marie Hardouin.
| director       = Leon Boedels
| producer       = Franz Anton Nöggerath jr.
| writer         = 
| narrator       = 
| starring       = Louis Bouwmeester Ko van Sprinkhuysen
| music          =   
| cinematography = 
| editing        = 
| distributor    = Filmfabriek F.A. Nöggerath
| released       = 1909
| runtime        = 8 minutes
| country        = Netherlands
| language       = Silent
| budget         = 
}} Dutch silent French play La Griffe by Jean Sartène.

==Plot==
Jean-Marie Hardouin (played by  ) is an old man who once was notorious because of the iron grip he exerted on his family but now he is physical disability|lame. He whiles away his days in a chair in the house of his son and daughter-in-law. He has to see how his adulterous daughter-in-law plots to murder her two foster children and her husband. Jean-Marie cant intervene and because he cant talk he cant warn his own family. Eventually he gathers all his strengths and strangles her.

==See also==
*List of Dutch films before 1910

==Sources==
*  Algemeen Handelsblad, 4 August 1909
*  F. van der Maden, Mobiele filmvertoning in Nederland 1895-1913, Nijmegen (1981), p.&nbsp;295
*  Nijmeegsche Courant, 5 October 1909
*  H. de Wit, Film in Utrecht van 1895 tot 1915, Utrecht (1986); Annex: p.&nbsp;133
*  De Kinematograaf, No. 102, 1 January 1915
*  De Kinematograaf, No. 105, 22 January 1915
*  De Kinematograaf, No. 121, 14 May 1915
*G. Donaldson, Of Joy and Sorrow. A Filmography of Dutch Silent Fiction, Amsterdam (1997), p.&nbsp;75

==References==
 

==External links==
* 
*   
* 

 
 
 
 
 


 