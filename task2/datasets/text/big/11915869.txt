The Hours and Times
{{Infobox Film name           = The Hours and Times image          = The Hours and Times.jpg caption        = Movie poster director       = Christopher Münch producer       = Christopher Münch writer         = Christopher Münch narrator       = starring       = David Angus Ian Hart music          = cinematography = Christopher Münch editing        = Christopher Münch distributor    = Good Machine (USA) released       =  : January 1992 runtime        = 60 minutes country        = United States language       = English budget         = gross          =
}}

The Hours and Times is a 1991 drama film written and directed by Christopher Münch. Starring David Angus and Ian Hart, it is a fictionalized account of what might have happened during a real holiday taken by John Lennon and The Beatles manager Brian Epstein in 1963.

==Plot==
It is 1963 and John Lennon flies to Barcelona with The Beatles manager Brian Epstein for a weekend of relaxation for John. On the flight over they meet air hostess Marianne. John flirts with her and gives her their hotel telephone number.

John asks Brian about gay sex and says that he thinks about it sometimes, but is put off by the thought that it would be painful. They play cards and Brian tells John he is surprised that he brought that up, that he feels awkward about it, that the situation between them is hopeless. John tells him that he finds Brian charming but does not want to have sex with him. He is angry at the thought that everyone they know thinks they are having a sexual relationship. He goes to bed and receives a telephone call from his wife, Cynthia Lennon|Cynthia. She says that she misses him, and John says that he misses their son, Julian Lennon|Julian.
 Spanish man named Quinones. John invites him back to the hotel where the three of them have drinks. Quinones is gay but married. After some friendly conversation he leaves early. Brian is angry with John, calling Quinones a fascism|fascist, and saying that nothing matters because he cannot have the one thing he wants. He goes to bed and confides in Miguel, the hotel boy. He asks Miguel for oral sex but then says he is only joking. Later he talks to his mother on the telephone.

The pair look around Barcelona and John takes photographs of Brian. They discuss, among other things, John’s relationship with Cynthia, which he does not like to talk to Brian about.

John has a bath and plays the harmonica. Brian enters and sits on the bath. John asks him to scrub his back with a flannel, which Brian starts doing. John starts kissing Brian, who quickly undresses and gets into the bath. They kiss a little more, then John abruptly gets out of the bath and leaves the room. Brian finds him smoking in bed. John says he is not angry but can not put into words what he is thinking. The telephone rings, it is Marianne. John tells her to come up. Brian is angry, saying that he is tired of making allowances for people. Marianne arrives and Brian leaves. Marianne asks John why Brian is upset, and they argue. She says that she can see they care about each other but she thinks John torments Brian. She has brought a new Little Richard record, which they dance to.

John asks Brian about his first time in Barcelona. Brian says he was sent there by his mother a couple of years previously following an incident where he had been robbed and blackmailed by a man he met for sex. Following the trial, Brian was forced to see a psychiatrist and his mother sent him to Spain. Two months later he met the Beatles. Brian tries to get John to promise to meet him in Barcelona in 10 years, no matter what they are doing. John agrees to at least remember the arrangement.

Later, Brian lies awake in bed with John sleeping next to him. Brian remembers a time when he took John to his “special place”, the roof of his family’s shop and told John how special the time they spent together was to him.

Later, Brian and John plan to go to a bull fight, and John hopes he will not be too squeamish for it.

==Cast==
* David Angus as Brian Epstein
* Ian Hart as John Lennon
* Stephanie Pack as Marianne
* Robin McDonald as Quinones
* Sergio Moreno as Miguel
* Unity Grimwood as Mother

==Production==
Director Christopher Münch originally saw The Hours and Times as a "DIY exercise", not expecting the film to secure any distribution. {{cite video
  | people = Christopher Münch
  | title = The Hours and Times Featurette
  | medium = DVD
  | publisher = 
  | location = 
  }}
  He wrote the script over a few weeks in early 1988 and traveled to England to cast it that spring. 

==Awards==
The Hours and Times won the Special Jury Recognition award at the 1992 Sundance Film Festival. {{cite web
  | title = Sundance Special Jury Prize
  | publisher = Reel.com
  | year = 2008
  | url = http://www.reel.com/reel.asp?node=features/awards/sundance/specialjury
  | accessdate =2008-08-08 }}    It was also nominated for the Grand Jury Prize at the same festival.

==References==
 

==External links==
*  
*  
* Siskel and Eberts film  
*  

 

 
 
 
 
 
 
 
 
 
 
 