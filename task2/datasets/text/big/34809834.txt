Vivasayee
{{Infobox film
| name           = VIVASAYEE
| image          = Vivasayee.jpg
| image_size     =
| caption        =
| director       = M. A. Thirumurugam
| producer       = Sandow M. M. A. Chinnappa Thevar
| writer         = Ma. Ra
| story          = Sandow M. M. A. Chinnappa Thevar
| narrator       = Manoramma M. N. Nambiar S. A. Asokan Nagesh
| music          = K. V. Mahadevan
| cinematography = N. S. Varma
| editing        = M. A. Thirumugam M. G. Balu Rao
| studio         = Devar Films
| released       = 1 November 1967
| runtime        = 155 mins
| country        = India Tamil
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
 Indian Tamil Tamil directed Manorama among others enacting supporting roles.

==Plot==
Somewhere in the Tamil country, in one of its vast green campaigns...

Muthaiya (MGR), a young agronomist and a farmer, full of ingenuity, a high integrity, a worker and a pride of his parents, the rich big landowner, The Pannaiyar Duraiswamy (Major Sundarrajan) and of his wife, devoted Sivagami (S. N. Lakshmi), revolutionize the exploitation of his father, by applying new methods of sowings.

He (MGR) so hopes to multiply tenfold the yield for humanitarian purposes and not necessarily or only lucrative.

Because one day, he takes the defense of unfortunate farmers despoiled of their ground, by his neighbor, another big farmer, The Pannaiyar Velupandhiyan (M. N. Nambiar), a being without scruples, Muthaiya incurs the wrath of this very vindictive character.

The situation complicates when very attractive Vidjaya (K. R. Vijaya) falls in love with the beautiful Muthaiya.

Indeed, she (K. R. Vijaya) is to be the sister-in-law of Velupandhiyan.
And that the latter desired her ardently under the nose of his wife, very believer Kaveri (C. R. Vijayakumari).

To see her taking off between the fingers, makes him (M. N. Nambiar) particularly dangerous and obnoxious with his wife, Kaveri.

Now, Muthaiya is even more in danger, Velupandhiyan wants to kill him...

==Cast==
{| class="wikitable" width="72%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
|-
| M. G. Ramachandran || as Muthaiya
|-
| M. N. Nambiar || as Pannaiyar Velupandhiyan
|-
| S. A. Ashokan || as Gurusamy
|-
| V. K. Ramasamy (actor)|V. K. Ramaswamy || as Shoki s father, a pawnbroker
|-
| Major Sundarrajan || as Pannaiyar Duraiswamy, Muthaiya s father
|-
| Sandow M. M. A. Chinnappa Thevar || as Madhasamy
|-
| Nagesh || as Shokan
|-
| K. R. Vijaya || as Vidjaya, Muthaiya s lover
|-
| C. R. Vijayakumari || as Kaveri, Pannaiyar Velupandhiyan s wife
|- Manorama || as Shoki
|-
| S. N. Lakshmi || as Sivagami, Muthaiya s mother
|-
|}

The casting is established according to the original order of the credits of opening of the movie, except those not mentioned

==Around the movie==

The film, released in 1967, is considered a hit and ran for 80 days. 

Vivasayee is the thirteenth collaboration MGR-Devar, nd it goes out for the Deepavali (Divali) of year 1967. 

There are 16 "Devar Films" with MGR.

The winning combination(overall) was always :

MGR (Main actor, hero)/M.A.Thirumugham (Director)/K.V.Mahadevan (Composer)/"Sandow" M.M.A.Chinnappa Devar (Producer, in the movie, sometimes, the henchman)

In the song of opening "Kadavul ennum...", we see the flag of the new elected party, The Dravida Munnetra Kazhagam|DMK, at the head of the country since February 23, 1967.

5 years more, MGR will find his partner, the big actress K.R.Vijaya. in another Devar Films of 1972, the last for MGR, the famous Nalla Neram.

==Songs==

{{tracklist	
| headline     = Tracklist 
| extra_column = Singer(s)
| lyrics_credits = yes
| total_length = 24.77
| title1       = Nalla nalla nilam parthu
| extra1       = T. M. Soundararajan
| lyrics1      = Udumalai Narayana Kavi
| length1      = 3.39
| title2       = Ennama Singara
| extra2       = T. M. Soundararajan, P. Susheela
| lyrics2      = A. Maruthakasi
| length2      = 3.34
| title3       = Ippadithan Irukka vendum
| extra3       = T. M. Soundararajan, P. Susheela
| lyrics3      = Udumalai Narayana Kavi
| length3      = 3.41
| title4       = Kadavul ennum
| extra4       = T. M. Soundararajan
| lyrics4      = A. Maruthakasi
| length4      = 3.45
| title5       = Yevaradithum
| extra5       = P. Susheela
| lyrics5      = Udumalai Narayana Kavi
| length5      = 3.21
| title6       = Kadhal Enthan
| extra6       = T. M. Soundararajan, P. Susheela
| lyrics6      = Udumalai Narayana Kavi
| length6      = 3.45
|title7=Vivasayee
|extra7=T. M. Soundararajan
|lyrics7=A. Maruthakasi
|length7=4.22
}}

==References==
 

==External links==
*  

 
 
 
 
 


 