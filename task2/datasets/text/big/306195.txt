The Net (1995 film)
{{Infobox film name = The Net image = Netposter1995.jpg caption = Theatrical release poster director = Irwin Winkler producer = Rob Cowan Irwin Winkler writer = John Brancato and Michael Ferris starring = Sandra Bullock Jeremy Northam Dennis Miller music = Mark Isham Jeff Rona editing = Richard Halsey distributor = Columbia Pictures released =   runtime = 114 minutes country = United States language = English budget = $22 million gross = $110,627,965 (Worldwide) {{cite web
| title = The Net at Box Office Mojo
| url = http://www.boxofficemojo.com/movies/?id=net.htm
| accessdate = 2010-08-23}} 
}} cyber thriller action thriller film directed by Irwin Winkler and featuring Sandra Bullock, Jeremy Northam and Dennis Miller. 

==Plot==
The film opens with United States Undersecretary of Defense Michael Bergstrom (Ken Howard), who commits suicide after discovering that he has tested positive for HIV.
 telecommutes to Cathedral Software in San Francisco. Her interpersonal relationships are completely online and on the phone, limiting interactions with neighbors and her mother (Diane Baker) who is institutionalized with Alzheimers disease. Bennetts co-worker Dale sends her a floppy disk with a backdoor (computing)|backdoor, labeled "Pi|π", to a commonly used computer security system called "Gatekeeper" sold by Gregg Microsystems. Dale and Bennett agree to meet, but his private planes navigation system malfunctions and it crashes, killing him.

Bennett travels to Cozumel, Mexico on vacation, where she meets Jack Devlin (Jeremy Northam). Devlin pays a mugger to steal Bennetts purse with the disk, then shoots the thief. He takes Bennett out on his speedboat to kill her, but she finds his gun and confronts him. While fleeing with the disk and Devlins wallet, Bennetts dinghy collides with rocks, destroying the disk and hospitalizing her, unconscious, for three days.
 LAX parking lot, and her credit cards are invalid. Bennetts home is empty and listed for sale and, because none of the neighbors ever saw her, they cannot confirm her identity. Bennetts Social Security number is now assigned to a "Ruth Marx", who has an arrest record. Another woman has taken her identity at Cathedral; the impostor offers Bennett her old life back in exchange for the disk. She contacts the only other person who knows her by sight, psychiatrist and former lover Alan Champion (Dennis Miller). He checks her into a hotel, offers to contact a friend at the FBI, and arranges to have her mother moved for her safety.

Using her knowledge of the backdoor and a password found in Devlins wallet, Bennett logs into the Bethesda Naval Hospitals computers and learns that Bergstrom, who had opposed Gatekeepers use by the federal government, was misdiagnosed. Fellow hacker "Cyberbob" identifies π with the "Praetorians", a notorious group of cyberterrorists linked to recent computer failures around the country. They plan to meet at Pacific Park on the Santa Monica Pier, but the Praetorians intercept their online chat. Bennett escapes from Devlin&mdash;a contract killer for the cyberterrorists&mdash;at the park, but the Praetorians kill Champion by tampering with pharmacy and hospital computer records. After Bennett is arrested by the California Highway Patrol, a man identifying himself as Champions FBI friend frees her from jail. She realises he is an impostor and escapes again.

Now wanted for murder, Bennett hitchhikes from Los Angeles to Cathedrals office in San Francisco where, using her impostors computer, she connects the terrorists to Gregg Microsystems and uncovers their scheme; once the Praetorians sabotage an organizations computer system, Gregg sells his Gatekeeper product to them and gains unlimited access through the backdoor. Bennett emails evidence of the backdoor to the FBI from the Moscone Center and tricks Devlin into releasing a virus into Greggs mainframe, undoing the erasing of her identity. They battle on the convention centers catwalks, where Devlin accidentally shoots and kills the imposter (the real Ruth Marx). Bennett then ambushes Devlin with a fire extinguisher, causing him to fall to his death. The film closes with Bennett reunited with her mother and the conspiracy exposed.

==Cast==
* Sandra Bullock as Angela Bennett
* Jeremy Northam as Jack Devlin
* Dennis Miller as Dr. Alan Champion
* Diane Baker as Mrs. Bennett
* Wendy Gazelle as Ruth Marx
* Ken Howard as Michael Bergstrom Ray McKinnon as Dale Hessman
* Robert Gossett as Ben Phillips
* Wren T. Brown as Trooper

==Production== Macworld on January 5, 1995,  as well as Washington, D.C., locations in April 1995. 

==Reception==

===Box office===
With an estimated budget of $22 million and a release date of  , The Net earned $50,727,965 in domestic box office. Including foreign markets, the film grossed $110,627,965 worldwide. {{cite web
| title = The Net at Box Office Mojo
| url = http://www.boxofficemojo.com/movies/?id=net.htm
| accessdate = 2010-08-23
}}  and an additional $23,771,600 in rentals (USA).

===Critics===
Critical reaction to the film was mixed. Based on 47 reviews, it has an average score of 5.1 out of 10 on Rotten Tomatoes with 36% of critics giving a positive review.  Roger Ebert gave the film three out of four stars. 
Owen Gleiberman, writing for Entertainment Weekly, complimented Sandra Bullocks performance, saying "Bullock pulls you into the movie. Her overripe smile and clear, imploring eyes are sometimes evocative of Julia Roberts". {{cite web
| date = August 4, 1995
| author = Owen Gleiberman
| title = The Net review at EW
| url = http://www.ew.com/ew/article/0,,298206,00.html
| accessdate = 2010-08-23
}} 

==Spinoff TV series and sequel== spinoff TV series starring Brooke Langton as Angela Bennett.

A sequel named The Net 2.0, starring Nikki DeLoach as Hope Cassidy and directed by Charles Winkler, son of Irwin Winkler, was announced in February 2005. It was released direct-to-video in 2006, and was about a young systems analyst who arrives in Istanbul for her new job to find that her identity has been stolen.

==See also==
* List of films featuring surveillance

==References==
 

==External links==
 
* 
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 