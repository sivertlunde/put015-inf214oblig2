Saving Lincoln
{{Infobox film
| name           = Saving Lincoln
| image          = Saving Lincoln poster.jpg
| caption        = Theatrical release poster
| director       = Salvador Litvak
| producer       = Reuben Lim Salvador Litvak
| writer         = Salvador Litvak Nina Davidovich Litvak
| starring       = Tom Amandes Lea Coco Penelope Ann Miller Bruce Davison Creed Bratton Saidah Arrika Ekulona Josh Stamberg Robert Craighead Lew Temple Michael Maize Steven Brand Adam Croasdell
| music          = Mark Adler
| cinematography = Alexandre Naufel
| editing        = Josh Noyes
| casting    = Matthew Lessall
| studio         =
| distributor    =
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          =
}} Civil War, thwarting several assassination attempts while holding the post of US Marshal. Lincoln considered Lamon a close friend, and Lamon felt likewise. Lincoln sent Lamon to Richmond, Virginia, on Reconstruction business a few days before April 14, 1865, the day that John Wilkes Booth assassinated the President.

The film was shot on a green screen stage, with a technique known as "CineCollage" used to create interior and exterior locations. Actors, extras, furniture, and props were filmed and combined with period photographs via the CineCollage process, which relied on off-the-shelf visual effects tools. The end result was a stylized look that works hand-in-hand with the movies narrative structure, which involves Lamons personal recollections of his friend. This was the first time such a technique was used to create an entire feature film. 

==Plot==
Director Salvador Litvak and his writing partner, Nina Davidovich Litvak, based their screenplay on their extensive research into Lincolns friendship with Lamon. Saving Lincoln charts their relationship from their initial meeting to Lincolns Presidency. Lamon was a tall, boisterous Southerner who liked to drink whiskey, tell jokes and stories, play the banjo, and wrestle. Despite some pronounced differences between the two men, they shared a fondness for telling jokes and stories, and both felt slavery should be eliminated. Lamon often served as Lincolns private confidant.

The movie jumps from their initial meeting to Lincolns presidency and the repeated attempts that were made on his life. Many well-known incidents are recounted, including the plot to kill Lincoln in Maryland, while he was traveling to Washington, D.C. after his first election&nbsp;– Lamon worked with Allan Pinkerton, who founded the famous Pinkerton Detective Agency, to thwart that plan. Other events include the time a bullet went through Lincolns hat while he was riding his horse late one evening&nbsp;– he blamed it on a hunter firing an errant shot, but Lamon saw it as a sign that Lincoln was in mortal danger and needed even tighter security. Lamon was said to sometimes sleep by Lincolns bedroom door, a striking image that appears in the film.

Themes in the story involve Lincolns anguish over Civil War casualties, his conflicts with members of his cabinet, and the death of the Lincolns son Willie, which drove Mary Todd Lincoln to the depths of despair. Such situations complicate Lamons efforts to keep Lincoln safe.

==Cast==
* Tom Amandes as President Abraham Lincoln
* Lea Coco as Ward Hill Lamon
* Penelope Ann Miller as Mary Todd Lincoln
* Bruce Davison as William H. Seward, Lincolns Secretary of State. Lewis Powell, a co-conspirator with John Wilkes Booth, attempted to kill Seward on the night of Lincolns assassination but only managed to wound him
* Creed Bratton as Senator Charles Sumner, who was vehemently against slavery and often tried to convince Lincoln to immediately free the slaves
* Josh Stamberg as Secretary of the Treasury Salmon P. Chase, who oversaw the establishment of a national banking system and the issuance of paper currency during the Civil War
* Robert Craighead as Secretary of War Edwin Stanton, who clashed with both Lincoln and Lamon. Several of those incidents are recounted in the film. After Lincoln was assassinated, Stanton famously said, "Now he belongs to the ages." William Herndon, who was Lincolns law partner before Lamon. He was a staunch abolitionist who insisted on an immediate end to slavery, even by violent means. His views often brought him into conflict with the more pragmatic Lamon.
* Saidah Arrika Ekulona as Mrs. Elizabeth Keckley
* Lew Temple as Montgomery Blair
* Steven Brand as Ned Baker
* Adam Croasdell as Col. Elmer Ellsworth
* Elijah Nelson as Willie Lincoln
*

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 