Honolulu (film)
{{Infobox film
| name           = Honolulu
| image          = Honolulu (1939).jpg
| image_size     = 190px
| caption        = Film poster
| director       = Edward Buzzell
| producer       = Jack Cummings
| writer         = Herbert Fields Frank Partos Harry Ruskin (uncredited) Robert Young George Burns Gracie Allen
| music          = Georgie Stoll Franz Waxman
| cinematography = Ray June
| editing        = Conrad A. Nervig
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 102 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} Robert Young, Eddie "Rochester" Anderson, and Rita Johnson.

==Plot== Hawai i-based businessman George Smith. Mason is tired of being in the public eye, so when he discovers that Smith is close enough to be his twin, he arranges to switch places with Smith temporarily. When Mason steps into Smiths life, he finds himself in a tug-of-war between Smiths fiancée, and a dancer named Dorothy March (Powell), with whom he has fallen in love. Meanwhile, Smith discovers that being a famous movie star is not all that it is made out to be.
 
==Notes== Bill Bojangles Robinson. The comedy of Burns and Allen is also featured, although the two actors work separately for much of the movie, their characters only meeting in the final minutes. The film is also notable for offering a somewhat rare cinematic look at pre-World War II Honolulu.

There is a notable musical sequence featuring Gracie Allen, accompanied by musicians made to look like the Marx Brothers (including two Grouchos), while several actors in the audience are costumed to look like such famous actors as Clark Gable, W.C. Fields and Oliver Hardy.

Footage of one of Powells dance routines (done in a hula skirt to a tiki drum orchestra) would be reused in the later comedy, I Dood It, while another dance performance that was cut from the film appeared seven years later in the "hodge-podge" production The Great Morgan.

==Cast==
*Eleanor Powell as Miss Dorothy Dot March Robert Young as Brooks Mason / George Smith / David in the movie
*George Burns as Joe Duffy
*Gracie Allen as Millicent Millie De Grasse
*Rita Johnson as Cecelia Grayson
*Clarence Kolb as Mr. Horace Grayson
*Jo Ann Sayers as Nurse
*Ann Morriss as Gale Brewster
*Willie Fung as Wong, Masons Hawaiian servant
*Cliff Clark as First Detective
*Edward Gargan as Second Detective Eddie Rochester Anderson as Washington, Masons Hollywood servant (billed as Eddie Anderson)
*Sig Ruman as Professor Timmer, psychiatrist (billed as Sig Rumann)
*Ruth Hussey as Eve, Davids wife in the movie
*Kealohu Holt as Native Dancing Girl (billed as Kealoha Holt)

==External links==
*  

 

 
 
 
 
 
 
 