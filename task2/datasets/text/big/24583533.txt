Kallarali Hoovagi
{{Infobox film name           = Kallarali Hoovagi image          = Kallarali Hoovagi.Jpg caption        = d.mp3 director       = T.S. Nagabharana producer       =  writer         =  starring  Umashankari Ambarish Kishore Ananth Avinash Bharathi Bharathi
|music          = Hamsalekha cinematography =  editing        =  distributor    =  released       =   runtime        =  country        = India language       = Kannada budget         = 
}}

Kallarali Hoovagi is an Indian   kingdom during the reign of Madakari Nayaka, one of the Palegars of Chitradurga.  It stars Vijay Raghavendra, Uma (actress)|Umashankari, Anantnag, Bharathi (actress)|Bharathi, Ambareesh, Avinash(Kannada actor)|Avinash.

==Plot==
This film is set in the time of Madakari Nayaka, a palegar of Chitradurga. The story revolves around a young Hindu boy who falls in love with a Muslim girl of an enemy kingdom whom he meets at the war-ruined area.

==Critical reception==
"Kallarali Hoovaagi" got very good critical reaction. Most newspapers and websites hailed it as an exceptional film. 

==Soundtrack==
Kallarali Hoovaagi music album, composed by Hamsalekha. was released on October 2006,

{{Track listing
| extra_column = Singer(s)
| lyrics_credits = no
| total_length = 50:13
| title1 = Kallarali Hoovaagi | extra1 = Hemanth Kumar, Chorus | length1 = 5:26
| title2 = Wah Wah Khana | extra2 = Udit Narayan, Chorus | length2 = 5:51
| title3 = Ee Bhoomi | extra3 = Hemanth Kumar, Chorus | length3 = 5:42
| title4 = Nanna Nechhina Koteya | extra4 = Kunal Ganjawala, Chitra | length4 = 5:26
| title5 = Hanatheya Adiyalle | extra5 = Shankar Mahadevan | length5 = 3:57
| title6 = Dayavillada Dharmavu | extra6 = MM Keeravani | length6 = 2:58
| title7 = Barappa O Thingala Mava | extra7 = Kunal Ganjawala | length7 = 5:08
| title8 = Akka Kelavva | extra8 = Nanditha | length8 = 4:51
| title9 = Ninna Nenapinali | extra9 = Rajesh Krishnan, Chitra | length9 = 4:54
| title10 = Alimola Alimola | extra10 = Chorus | length10 = 2:37
| title11 = Mysore Desh | extra11 = Jayatheertha Mevundi | length11 = 1:40
| title12 = Sampige Siddesha | extra12 = Hemanth Kumar, Chorus | length12 = 1:53
}}

==Awards==
*National Award for Best Film on National Integration 
*State Award for Best Editing Basavaraj Urs from Karnataka Government in 2006-2007
*State Award for Best Art Director Vittal in 2006-2007
*State Award Special Award for Costume Design Nagini Bharana and Roshni in 2006-2007

==See also==
*Janumada Jodi

==References==
 
*http://www.kannadaaudio.com/Songs/Moviewise/home/KallaraliHoovagi.php
*http://www.hindu.com/2007/01/09/stories/2007010912890300.htm
*http://www.nowrunning.com/movie/3093/kannada/kallarali-hoovagi/review.htm

 

 
 
 
 
 