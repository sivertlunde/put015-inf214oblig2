Black Butler (film)
{{Infobox film
| name           = Black Butler
| image          = Black-butler-film-poster.jpg
| alt            =
| caption        = 
| film name      = 
| director       = {{plainlist|
*Kentaro Otani
*Keiichi Sato}}
| producer       = Shinzo Matsuhashi
| writer         = Tsutomu Kuroiwa
| screenplay     = 
| story          = 
| based on       =  
| starring       = Mizushima Hiro 
| narrator       = 
| music          = Akihisa Matsuura
| cinematography = Terukuni Ajisaka 
| editing        = Tsuyoshi Imai 
| production companies = {{plainlist|
*C&I Entertainment
*Rockworks}}
| distributor    = 
| released       =  
| runtime        = 119 minutes
| country        = Japan
| language       = 
| budget         = 
| gross          =  
}}
 of the same name by Yana Toboso. 

==Plot==
 

==Cast==
* Mizushima Hiro as Sebastian Michealis, the "Black Butler"
* Ayame Goriki as Genpo Shiori, aka Count Genpo Kiyoharu Yuka as Wakatsuki Hanae, Shioris aunt
* Mizuki Yamamoto as Rin/Lyn, the housemaid
* Tomomi Maruyama as Akashi, the butler
* Masato Ibu as Kuzo Shinpei
* Takuro Ono as Matsumiya Takaki
* Yu Shirota as Charles Bennett Sato
 

==Production==
The film changes the setting of the original manga which was set in 19th-century London to an unnamed Eastern nation in the year 2020.  The film stars Mizushima Hiro as Sebastian the lead, his first starring role in three years. 

==Release==
Black Butler was released in Japan on January 18, 2014.  The film debuted at third place on its opening weekend in the Japanese Box office being beaten by   and The Eternal Zero.  The film grossed a total of $5,243,260 in Japan. 

==Reception==
Derek Elley of Film Business Asia gave the film a three out of ten rating calling it "a failure at every level", noting that the film was "stodgily directed, appallingly constructed (with an especially confusing exposition) and laden down with yards of flat dialogue. When any action does finally come, its just so-so.".  The Guardian gave the film three stars out of five, noting that "Much of the dialogue and performances are stilted, but as a kitsch cult watch it has its charms."  The Times awarded the film three stars out of five, describing it as "compellingly weird". 

==See also==
* List of films based on manga
* List of Japanese films of 2014

==Notes==
 

==External links==
*  

 

 
 
 
 
 
 
 

 