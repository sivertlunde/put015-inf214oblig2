The Shoes of the Fisherman
 
 
{{Infobox film
| name        = The Shoes of the Fisherman
| image       = The Shoes of the Fisherman.jpg
| caption     = Theatrical release poster by Howard Terpning Michael Anderson John Patrick James Kennaway
| based on  =  
| starring    = Laurence Olivier Anthony Quinn Oskar Werner David Janssen Vittorio De Sica Leo McKern John Gielgud
| music      = Alex North
| cinematography = Erwin Hillier
| producer    = George Englund MGM
| released    =  
| runtime     = 162 minutes
| country     = United States
| language    = English
| budget      = $6.7 million Metro-Goldwyn Omits Dividend; O Brien Resigns: Board Cites Possible Loss Of Up to $19 Million in The Current Fiscal Year Bronfman Named Chairman
Wall Street Journal (1923 - Current file)   May 27, 1969: 2.  
}} Michael Anderson and released by Metro-Goldwyn-Mayer.

== Plot == Metropolitan Archbishop of Lviv (or Lvov as it is spelled in the movie adaptation), is unexpectedly set free after twenty years in a Siberian labour camp by his former jailer, Piotr Ilyich Kamenev (Laurence Olivier), now the premier of the Soviet Union.
 Cardinal in title of Athanasius of Alexandria|St. Athanasius. Lakota is reluctant, begging to be given "a simple mission with simple people," but the Pope insists that he kneel and receive the scarlet zucchetto that designates the rank of cardinal.

When the Pontiff suddenly collapses and dies, the process of a papal conclave begins, and Cardinal Lakota participates as one of the electors. During the sede vacante, two cardinals in particular, Cardinal Leone (Leo McKern) and Cardinal Rinaldi (Vittorio De Sica) are shown to be papabili (candidates). After seven deadlocked ballots, Lakota is unexpectedly elected Pope as a compromise candidate (suggested by Cardinal Rinaldi) by acclamation after the cardinals, unable to decide between the leading candidates, interview him and are impressed by his ideas and his humility. Lakota takes the name of Pope Kiril. Meanwhile, the world is on the brink of nuclear war due to a Chinese-Soviet feud made worse by a famine caused by trade restrictions brought against China by the United States.

The evening after his election, Pope Kiril, with the help of his personal aide Gelasio (Arnoldo Foà), sneaks out of the Vatican and explores the city of Rome dressed as a simple priest. By chance, he encounters Dr. Ruth Faber, who is in a troubled marriage with a Rome-based television journalist, George Faber (David Janssen). Later, the Pope returns to the Soviet Union to meet privately with Kamenev and Chairman Peng (Burt Kwouk) of China to discuss the ongoing crisis.
 tiara (in a gesture of humility) and pledges to sell the Churchs property to help the Chinese, much to the delight of the crowds in St. Peters Square below.
 heterodox views. To the Popes deep grief, Father Telemond dies.

== Cast ==
* Anthony Quinn as Kiril Lakota/Pope Kiril I
* Laurence Olivier as Piotr Ilyich Kamenev
* Oskar Werner as Fr. David Telemond
* David Janssen as George Faber
* Barbara Jefford as Dr Ruth Faber
* Vittorio De Sica as Cardinal Rinaldi
* Leo McKern as Cardinal Leone
* John Gielgud as The Elder Pope
* Burt Kwouk as Peng
* Arnoldo Foà as Gelasio
* Leopoldo Trieste as Dying Mans Friend
* Frank Finlay as Igor Bounin
* Rosemary Dexter as Chiara
* Clive Revill as Vucovich
* Niall MacGinnis as Capuchin monk
* Isa Miranda as Marquesas

==Notes==
 Michael Anderson.

The papal tiara used for the coronation scene in the film is modelled after Pope Paul VIs own papal tiara.

==Reception==
The film was the sixth most popular movie at the Australian box office in 1969. 
 Best Score, George Davis Best Art Direction.   

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 