Breaking with Old Ideas
{{Infobox film
| name           = Breaking with Old Ideas
| image          = 
| image_size     = 
| caption        = 
| director       = Li Wenhua
| producer       = 
| writer         = Chun Chao Zhou Jie
| narrator       = 
| starring       = Guo Zhenging Bao Lie Wang Suya
| music          = 
| cinematography = 
| editing        = 
| studio         = Beijing Film Studio
| distributor    = 
| released       = 1975
| runtime        = 110 minutes
| country        = China
| language       = Mandarin
| budget         = 
| gross          = 
}}

Breaking with Old Ideas ( ) is a 1975 Chinese film directed by Li Wenhua. The film is one of the few that were produced during the Cultural Revolution. Zhang, Yingjin & Xiao, Zhiwei. "Breaking with Old Ideas" in Encyclopedia of Chinese Film. Taylor & Francis (1998), p. 101. ISBN 0-415-15168-6.  As a result of the political climate, Breaking with Old Ideass plot was heavily regulated under highly codified guidelines on story and characterization. 

== Plot summary == Chairman Mao himself. 

== Cast ==
*Guo Zhenqing as Long Guozheng
*Wang Suya as Li Jinfeng
*Wen Xiying as Deputy Secretary Tang
*Xu Zhan as Xu Niuzai
*Zhang Zheng as Old Representative

== See also ==
* Cultural Revolution

== References ==
 

== External links ==
* 
* 

 
 
 
 
 
 

 
 