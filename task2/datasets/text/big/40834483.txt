Tales from the Dark 1
 
 
{{Infobox film
| name           = Tales from the Dark 1
| image          = Tales from the Dark 1.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = 
| director       = {{plainlist|
*Simon Yam
*Lee Chi-ngai
*Fruit Chan}}
| producer       = {{plainlist|
*Mathew Tang
*Bill Kong }}
| writer         = 
| screenplay     = {{plainlist|
*Lilian Lee
*Lee Chi-ngai
*Fruit Chan}} 
| story          = Lilian Lee
| based on       =  
| narrator       = 
| starring       = 
| music          = Kenji Kawai 
| cinematography = {{plainlist|
*Jason Kwan
*Wade Muller
*Lam Wah-chuen }}
| editing        = {{plainlist|
*Kwong Chi-leung
*Lee Kar-wing
*Lee Chi-ngai
*Fruit Chan }}
| studio         = Movie Addict Productions
| distributor    = 
| released       =  
| runtime        = 112 minutes 
| country        = Hong Kong 
| language       = Cantonese
| budget         = 
| gross          = HK$7,677,402 
}}

Tales from the Dark 1 (李碧華鬼魅系列　迷離夜) is a 2013 Hong Kong portmanteau horror film directed by Simon Yam, Lee Chi-ngai, and Fruit Chan. The film is split into three stories based on short stories by Lilian Lee. The first film is titled Stolen Goods (贓物) directed by and stars Simon Yam. It featured Yam, a man in Hong Kong who has recently lost his job and decides to make money by stealing funeral urns and blackmailing the families into buying them back from him. The second story is directed by Lee Chi-Ngai and is titled A Word in the Palm (放手). The film is about a fortune teller played by Tony Leung Ka-fai who retired from his job to study music, but retains the ability to see ghosts. The final film is directed by Fruit Chan and titled Jing Zhe (驚蟄). The film stars Josephine Koo who is asked to beat up an unusual woman (Dada Chan) to beat up an entire group of strangers.

The film marked the first director work of actor Simon Yam. It was shown at the New York Asian Film Festival on 28 June 2013. The film was released theatrically in Hong Kong on 11 July 2013. The film was followed up with  Tales from the Dark 2 which was also released in 2013.

==Cast==
 
 
;Stolen Goods cast 
* Simon Yam as Kwan Fu-keung
* Yuen Qiu as a restaurant owner
* Maggie Shiu as Kwok Wing-nei 
* Felix Lok as Chu Wing-kit
* Lam Suet as Boss Lee
 
;A Word in the Palm cast 
* Tony Leung Ka-fai as Ho Ho
* Kelly Chen as Lan
* Eileen Tung as Hos wife
* Cherry Ngan as Chan Siu-ting
* Eddie Li as Cheung Ka-chun
* Jeannie Chan as Mrs. Cheung (Ka-chuns wife)
 
;Jing Zhe cast 
*Susan Shaw as Chu, the fortune teller
*Josephine Koo as Koo, her wealthy customer
*Dada Chan as a ghost
*Lo Hoi-pang as Leung Chan-ying/Z.Y. Leung
 

==Production and style==
The films story was adapted from the second book in Lilian Lees five-volume book series that were originally published in newspaper form.  The films directors included Fruit Chan and Lee Chi-ngai who have not directed many films in the past decade and Simon Yam who made his directorial debut with this film. 

Derek Elley of Film Business Asia described the film as a "portmanteau horror film" and felt it was similar in tone and style to 1980s and early 1990s Hong Kong horror films.  The Hollywood Reporter echoed this statement, referring to it as a "film that harkens back to the glory days of Hong Kong horror in the 1980s."  

==Release==
Tales from the Dark 1 was the opening night film at the New York Asian Film Festival on 28 June 2013.    It was released theatrically in Hong Kong on 11 July 2013.  The film was followed-up with a sequel titled Tales from the Dark 2 released on 8 August 2013. 

==Reception==
 |title=Film Review: ‘Tales From the Dark Part 1’|author=Scheib
, Ronnie|accessdate=19 October 2013|date=26 July 2013}}  The Hollywood Reporter referred to the film as a "mostly engaging film"  and that "Anthologies are by definition hit and miss endeavors and often swing wildly in quality from one segment to the next. That’s exactly what happens here, except the producers have wisely managed to bottom load the film so that sitting through the first entry eventually pays off"   

==Notes==
 

==External links==
*  

 

 
 
 
 
 
 