Eyes Wide Open (2013 film)
{{multiple issues|
 
 
}}
{{Infobox film
| name           = Eyes Wide Open
| director       = Chris Chloupek  
| producer       = Chris Chloupek & Jason Bond
| writer         = 
| starring       = Nate Holston, Morgan Martin, Chris Chloupek, and Jason Bond 
| music          = 
| cinematography = Kevin Stevenson
| editing        = Isaac Diaz, Ed Washington, and Kolby Jones
| studio         = Good News Network, AMS Films, How Ya Doin Productions
| distributor    = 
| released       =  
| runtime        = 16 minutes
| country        = United States
| language       = English
| budget         = $1,000 (estimate) 
| gross          = 
}}
 
Eyes Wide Open is 2013 American dramatic short film directed by Chris Chloupek and starring Nate Holston as David Foster. It is also the first dramatic short film of Good News Network  The film tells the story of David Foster as he gambles and after dreaming of causing the death of his wife, Julie Foster, he becomes a Christian.

==Plot== football game that hes watching on TV. His wife, Julie Foster, jokes saying if only would be that excited about other things to which David quietly reveals to her daughter that he had won money though that game. Julie asks if he was coming to the to something but David replies no as there is another game coming on later. Julie sighs in agreeance and she along with her daughter leave. David turns and focuses on a Bible sitting on a counter.

The next day Julie arrives from grocery shopping while David is playing poker on his laptop. She tries to grab his attention but he responds disinterested and continues with his poker game. Julie yells at him and walks of into their kitchen. David follows her and tries to comfort her but when asked why their Debit card declined $700, he tells her that he lost it in a bet. After arguing he goes outside to work on his car. While there he is visited by Jason who tries to help him out because he cares about him. David says that he appreciates but that he has to go.

David is then in the "hood" where he enters a house with people drinking, making out, and gambling. He receives money from one of the men in the house but then sneaks into a back room where he finds people playing a game of poker. He tries to get into the game but is told by Xavier that this game is only for serious money. And after being told to leave, David throws a stack of money on the table and says that he came to play. One Xaviers henchmen counts it and say that he doesnt have enough. David insists on playing and Xavier eventually permits him to. One by one the other players leave the game until Xavier and David are left. He loses and tries to leave but is grabbed so that Xavier can see his ID, upon reading his name he tells David that he has 24 hours to pay him back.

David is sleeping on his couch but wakes up when he heres the doorbell ring. He opens the door and finds Xavier and his henchmen there. After being punched and has the rules explained to him he tries to fight them. The henchmen pulls a gun out but is told not to by Xavier. David and the henchmen then fight for control of the gun when a gunshot is heard. Xavier and his henchmen look in fear towards David and then run away from the home. David feels for a bullet but when he doesnt feel it he turns and sees Julie bleeding and falling to the ground. After crying over her he wakes up from this dream and when he hears the doorbell ring, he looks, afraid, toward the door.

The movie pauses and reads, "Eyes Wide Open". The screen then turns black and shows "If we deliberately keep on sinning after we have received the knowledge of the truth, no sacrifice for sins is left." (Hebrews 10:26 NIV). After this the screen only shows "IF...".

Two weeks later Julie begins to share about seeing Davids changes after studying the Bible. Jason then shares about what he has seen and finally David shares and says that he never imagined being baptized. Another man steps in and asks him "two of the most important questions that you will have to answer ever in your life." He asks David, "Do you believe that God sent Jesus to earth, he   lived, he died, was crucified for our sins, and he raised on the third day?" to which David replied, "Yes, I do." The man then asks, "What is your Good Confession?" and David replies, "Jesus is Lord!" After cheers he is led to a hot tub and then the actual baptism of Nate Holston, who played David Foster, is shown along with the date in which he was baptized. After coming out of the water, the film goes on to show the credits.

==Cast==
In order of appearance:
 
* Nate Holston as David Foster
* Saniyah Redeau as Kelly Foster
* Morgan Martin as Julie Foster
* Jason Bond as Jason
* Jermaine Simmons as Thug #1 Bouncer
* Aleyah Grant as Xaviers Girl
* Anthony Wise II as Party Couple Guy
* Alicia Raye as Party Couple Girl
* Princeton George as Dice Thrower #1
* Jabari Farrow as Dice Thrower #2
* Nate Pilate as Bookie
* Thurzday Lyons as Hostess
* Terrance Delane as Thug #2
* Chris Chloupek as Xavier
* Beach Eastwood as Xaviers Henchman
* Richard Lewis, Miguel Angel Mendez, and Argo Arneson as Poker Playees #1,2,3 respectively
* Heather Harvin as Poker Room Girl
* Chad Welch as Henchman

 

==References==
 

 
 
 