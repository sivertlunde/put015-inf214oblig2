The Time Machine (1960 film)
{{Infobox film
| name           = The Time Machine
| image          = File:Brown,r time macine60.jpg
| image_size     =
| alt            = 
| caption        = Theatrical release poster by Reynold Brown
| director       = George Pal
| producer       = George Pal David Duncan
| based on       =  
| narrator       = Rod Taylor Sebastian Cabot Whit Bissell Russell Garcia
| cinematography = Paul Vogel
| editing        = George Tomasini
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 103 minutes
| country        = United States
| language       = English
| budget         = $829,000  . 
| gross          = $2,610,000 
}}
 novel of the same name by H. G. Wells in which a man from Victorian England constructs a time-travelling machine which he uses to travel to the future where a new civilisation has gone wrong after a nuclear war. The film stars Rod Taylor, Yvette Mimieux and Alan Young.

The film was produced and directed by   functions as a sequel of sorts.  In 1985, elements of this film were incorporated into The Fantasy Film Worlds of George Pal, produced by Arnold Leibovit. 
 Oscar for time-lapse photographic effects showing the world changing rapidly.

==Plot==
On January 5, 1900, four friends arrive for a dinner at the house of The Time Machine#The Time Traveller|H. George Wells (Rod Taylor), who staggers in to describe his strange experiences.
 Sebastian Cabot) and Walter Kemp (Whit Bissell). He had shown them at  that earlier meeting a small prototype  machine which he had claimed could travel in time and which disappeared when the miniature lever on it was pressed, thereby apparently demonstrating the success of the invention. The friends are dismissive, saying that it is preposterous, despite the model transporting a cigar through time. 
 the Great new war", air raid sirens. An elderly James Filby tells him to get into the shelter. James, eventually recognising Wells,  is surprised that Wells has not aged. A nuclear explosion causes a volcano to erupt. Civilisation is destroyed in a nuclear holocaust. Wells restarts the machine just in time to avoid being incinerated, but lava surrounds the machine in time, then cools and hardens, forcing him to travel far into the future until it erodes away.
 Weena (Yvette Mimieux) and her people the Eloi. 

Wells questions the Eloi about their civilisation and finds they have no books save for some crumbling ones. He discovers that the time machine has been dragged into the sphinx building and locked inside. Weena follows Wells and insists they go back, for fear of "Morlocks" at night. A Morlock jumps out of the bushes and tries to drag Weena off, but Wells rescues her.
 nuclear war. One group of survivors remained underground in the shelters and evolved into the Morlocks, while the other group returned to the surface and became the Eloi. Wells starts to climb  down a shaft  but turns back when a siren blares from atop the sphinx building. Weena and the rest of the Eloi enter a trance-like state, and they complacently file through the now-open doors of the building. When the siren stops, the doors close, trapping Weena and others inside.

To rescue Weena, Wells climbs down the  air-shaft again and enters the subterranean caverns. He is horrified to discover that the Eloi are little more than free range livestock to the Morlocks. He fights the Morlocks, aided by the Eloi, who prove not to be completely helpless. Before escaping with the Eloi up the shaft, he starts a fire that spreads through the cavern. Under his direction, the Eloi drop dry tree branches into the shafts to feed the fire. The entire area caves in, crushing and suffocating most of the Morlocks below. The next morning, Wells finds the sphinx building in charred ruins and the doors open again, with his time machine sitting just inside the entrance. He enters to retrieve the machine, but the doors close behind him and he is attacked by the remaining Morlocks. He uses the time machine to escape, travelling  to January 5, 1900, in time to meet his old friends for dinner and tell them about his adventure. Wells  friends scoff at his story, but he finds Weenas flower in his coat pocket, convincing Filby. Wells leaves again in the time machine back to the future. Filby notices tracks where Wells dragged the machine back to its original location before leaving so that he would be outside the sphinx building when he returns to the future. Filby and Wells housekeeper notice three books are missing from Wells  library.  They cannot be sure which three books they are.

==Cast==
* Rod Taylor as George (H. George Wells, as written on the time machine)
* Alan Young as David Filby/James Filby
* Yvette Mimieux as Weena Sebastian Cabot as Dr. Philip Hillyer
* Tom Helmore as Anthony Bridewell
* Whit Bissell as Walter Kemp
* Doris Lloyd as Mrs. Watchett
* Paul Frees (uncredited) as Voice of the Rings

;Cast notes
*Rod Taylor and Yvette Mimieux also co-starred in 1968s Dark of the Sun. 

* Young and Mimieux are the only surviving primary cast members.

==Home media releases== letterbox and pan & scan LaserDisc. The film was released on DVD in October 2000 and on Blu-ray Disc in July 2014.

==Soundtrack== CD was released in 1987. The tracklisting is as follows:
 

# Main Title / Credits
# London 1900 (Filbys Theme)
# Time Machine Model
# The Time Machine
# Quick Trip Into The Future
# All The Time In The World
# Beautiful Forest / The Great Hall
# Fear
# Weena (Love Theme)
# Rescue
# Reminiscing
# Morlocks
# End Title (Reprise)
# Fight With The Morlocks
# Time Traveler
# Escape
# Prayer / Off Again
# Trapped In The Future
# Love And Time Return
# End Title
# Atlantis, The Lost Continent (Overture) - Main Title / Credits / Love Theme / Night Scene / Submarine / End Title

==Production== Tom Thumb) friendlier. 

Pal originally considered casting a middle-aged British actor in the lead role, such as David Niven or James Mason. He later changed his mind and selected the younger Australian actor Rod Taylor to give the character a more athletic, idealistic dimension. It was Taylors first lead role in a feature film. 

MGM art director Bill Ferrari created the Machine, a sled-like design with a big, rotating vertical wheel behind the seat. The live action scenes were filmed from May 25, 1959 to June 30, 1959, in Culver City, California.

==Box Office==
According to MGM records the film earned $1,610,000 in the United States and Canada and $1 million elsewhere, turning a profit of $245,000. 

It had admissions of 363,915 in France. 

==Awards and honors== Academy Award for Best Effects, Special Effects winner (1961) - Gene Warren and Tim Baar Hugo Award nomination (1961)

==1993 sequel/documentary== David Duncan, Rod Taylor, Alan Young and Whit Bissell reprised their roles.

==See also== The Time remake directed by Simon Wells and an uncredited Gore Verbinski, and starring Guy Pearce in the Taylor role. Time After Time, a 1979 science-fiction film in which H. G. Wells (played by Malcolm McDowell) travels to modern-day San Francisco in his time machine in pursuit of Jack the Ripper.
* The Big Bang Theory (season 1)#ep14|"The Nerdvana Annihilation", episode of The Big Bang Theory in which the characters purchase the films time machine.
* Quantum Leap episode "Future Boy" has a character who builds a time machine similar to that one used in the 1960 movie.

==References==
;Notes
 

==External links==
 
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
Streaming audio
*   on Favorite Story: May 28, 1949
*   on Escape (radio program)|Escape: October 27, 1950

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 