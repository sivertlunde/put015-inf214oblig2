Fish and Elephant
{{Infobox film
| name           = Fish and Elephant
| image          = Fish_Elephant.jpg
| caption        =  Li Yu
| producer       = Cheng Yong
| writer         = Li Yu Shitou Pan Yi
| music          = He Wen
| cinematography = Fei Xiaoping
| editing        = Li Yu
| distributor    =
| released       =  
| runtime        = 106 minutes
| language       = Mandarin
| budget         = 
| film name = {{Film name| jianti         = 今年夏天
| fanti          = 今年夏天
| pinyin         = Jīn nián xià tiān}}
}} Li Yus title =
Chinas First Lesbian Film Wins Praise |author=DeGroat, T. J.|publisher= Asian Diversity, Inc.| accessdate=2008-02-02}}   

The story follows the elephant keeper in the Beijing Zoo who maintains an aquarium of fish in her home (hence the two-animal title), and her lesbian lover, a fabric saleswoman in an outdoor market. Her relationship is tested, however, when her recently divorced mother returns to town in the hope of setting her daughter up in marriage. Further complicating matters is one of Xiaoquns ex-lovers also returning to her life with the law in pursuit. 

The film was produced by Cheng Yong Productions and was an "underground" production (in that it was not made with official support). Moreover, the film was cast entirely with non-professional actors and actresses. 

==Cast==
* Pan Yi as Xiaoqun, the elephant keeper at the Beijing Zoo.
* Shitou as Xiaoling, Xiaoquns girlfriend, she sells clothes she makes in a market stall. 
* Zhang Qianqian as Junjun, Xiaoquns ex-girlfriend. She re-enters Xiaoquns life a fugitive, wanted for the murder of her father. 
* Zhang Jilian as Xiaoquns mother, a divorcee who is unaware of her daughters sexual orientation. She attempts to set her daughter up on a series of blind dates.

==Production== Li Yu, who had previously directed several documentaries, including 1997s Sisters, a film which had run into state interference.   

Casting by Li Yu was done entirely with non-professionals. The two leads, Xiaoqun (Pan Yi) and Xiaoling (Shitou), were cast after Li began visiting various lesbian bars in Beijing. Similarly, several of the films "blind dates" were based on actual dates set up by Li through classified ads. The mens reactions upon Xiaoqun revealing her sexual orientation therefore are entirely natural and accurate.  Some have reported that this focus on amateurs was only partially by choice, in that Li had been unable to sign on established talent due to the controversial script and story. 

==Release==
===International and domestic film festivals===
Once the films production was complete, it faced several challenges before it could be seen by audiences. Due to its underground status, Li Yu did not have the support of the Chinese authorities. An attempt to deliver a print to the Venice Film Festival in 2001 led to it being lost.  Later screenings, therefore, like at the Toronto Film Festival, for example, had to be on videotape instead of a film reel.  Fortunately for Li, copies of Fish and Elephant eventually managed to screen at over 70 different film festivals around the world. International Bar Association, et al.  . Haworth Press, p. 182. ISBN 1-56023-555-1. Google Book Search. Retrieved 2008-10-14 

However, it was shown only once in Mainland China at a LGBT film festival that was quickly shut down by authorities. 

===DVD release===
Fish and Elephant was released on DVD in the United States by the LGBT-focused distributor, Ariztical Entertainment on May 23, 2006. The edition features the original Mandarin Chinese|Mandarin-language dialogue and includes English subtitles.

==Reception== Time Out New York, for example,
though generally positive in its praise also noted that there was "nothing here to surprise western viewers."  Chinese film critic Shelly Kraicer, meanwhile, analyzed the film and concluded, "  flaws are those, entirely forgivable, of a first film, of a novice director who really wants to try to cram all of her ideas into one work."  The films release was similarly noted by the film criticism website Senses of Cinema, which stated that, "Fish and Elephant might be overlooked by film festivals in favour of other more glossy underground Chinese films produced this year...", but the author would nonetheless "recommend it and will keep an eye out for   future works." 

===Awards and nominations===
* 2001 Venice Film Festival 
** Winner of the Elvira Notari Prize     
* 2002 Berlin International Film Festival
** Winner of the Best Asian Film Prize at Forum of New Cinema 
** NETPAC Special Mention 

==See also==
*East Palace, West Palace - director Zhang Yuans 1996 film, often referred to as the first homosexual-themed film out of mainland China.
* Homosexuality in China

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 