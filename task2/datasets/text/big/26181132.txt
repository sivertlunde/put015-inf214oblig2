A Shot in the Dark (1935 film)
{{Infobox film
| name           = A Shot in the Dark
| image          = "A_Shot_in_the_Dark"_(1935).jpg
| image_size     =
| caption        =
| director       = Charles Lamont
| producer       = George R. Batcheller (producer) Charles Belden (screenplay)
| based on       = novel The Dartmouth Murders by Clifford Orr 
| narrator       =
| starring       = See below
| music          =  Abe Meyer
| cinematography = M.A. Anderson
| editing        = Roland D. Reed
| studio         =  Chesterfield Motion Pictures Corporation
| distributor    = Universal Pictures
| released       = 1 February 1935
| runtime        = 69 minutes
| country        = USA
| language       = English
}}

A Shot in the Dark is a 1935 American mystery film directed by Charles Lamont. 

==Plot==
A college student (Charles Starrett) discovers his roommate’s body hanging from a window and calls the police. What at first looks like suicide turns out to be murder.  While a police investigation is ongoing, more students are killed.

== Cast ==
*Charles Starrett as Kenneth "Ken" Harris
*Robert Warwick as Joseph Harris
*Edward Van Sloan as Prof. Bostwick
*Marion Shilling as Jean Coates
*Helen Jerome Eddy as Miss Lottie Case
*Doris Lloyd as Lucille Coates James Bush as Byron Coates / John Meseraux
*Julian Madison as Charlie Penlon
*Eddie Tamblyn as Bill Smart Ralph Brooks as Sam Anderson Robert McKenzie as Sheriff John Davidson as Prof. Brand
*Herbert Bunston as College President George Morrell as Deputy Ab Barber
*Broderick OFarrell as Dr. Howell
*Jane Keckley as Bostwicks Housekeeper

==Critical reception==
The New York Times wrote, "A Shot in the Dark, which pictures a trilogy of murders on a rural college campus, telegraphs its punches in a way that may seem insignificant to Chesterfield Productions, Inc., but is as good as a confession to us amateur gumshoes...a decided absence of liveliness both in the writing and the playing" ; {{cite web|url=http://www.nytimes.com/movie/review?res=9503E4D81139E33ABC4A51DFB366838E629EDE|title=Movie Review -
  A Shot in the Dark - THE SCREEN - NYTimes.com|work=nytimes.com}}  whereas Fantastic Movie Musings and Ramblings noted, "a very good mystery...It does have some problems, particularly in having a rather stiff and static presentation, but outside of that, this is one of the more pleasant discoveries Ive made."   

== Soundtrack ==
 

==References==
 

== External links ==
* 
* 

 
 

 
 
 
 
 
 
 
 
 

 