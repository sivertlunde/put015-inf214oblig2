Paths of Death and Angels
{{Infobox film
| name           = Paths of Death and Angels
| image          = 
| caption        = 
| director       = Zoltán Kamondi
| producer       = János Dömölky Gábor Hanák
| writer         = Zoltán Kamondi
| starring       = Enikő Eszenyi
| music          = 
| cinematography = Gábor Medvigy
| editing        = Mari Miklós
| distributor    = 
| released       =  
| runtime        = 114 minutes
| country        = Hungary
| language       = Hungarian
| budget         = 
}}

Paths of Death and Angels ( ) is a 1991 Hungarian drama film directed by Zoltán Kamondi. It was screened in the Un Certain Regard section at the 1991 Cannes Film Festival.   

==Cast==
* Enikő Eszenyi - Ilona
* Rudolf Hrusínský - Schrevek József
* Gregory Hlady - Schrevek István (as Grigorij Gladyij)
* István Dégi - Árpi
* Gábor Reviczky - Boldizsár Tamás és Nagy Károly
* Eszter Csákányi
* Frigyes Hollósi
* Edit Illés - Jolán
* Anikó Für - Gina

==References==
 

==External links==
* 

 
 
 
 
 
 