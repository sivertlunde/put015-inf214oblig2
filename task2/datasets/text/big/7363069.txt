The Loves of Hercules
{{Infobox film
| name           = The Loves of Hercules  
| image          = Gliamoridiercoleposter.jpg
| image size     =
| caption        = Original movie poster
| director       = Carlo Ludovico Bragaglia
| producer       = Alberto Manca
| writer         = Luciano Doria
| narrator       =
| starring       = Jayne Mansfield Mickey Hargitay
| music          = Carlo Innocenzi
| cinematography = Enzo Serafin
| editing        = Renato Cinquini
| distributor    = 1960
| runtime        = 98 min.
| country        = Italy / France Italian
| budget         =
}} 1960 Cinema French international co-production epic fantasy-feature film  starring Jayne Mansfield and her then husband Mickey Hargitay.  The film was distributed internationally as Hercules vs the Hydra.

==Plot==
While Hercules (Mickey Hargitay) is away, his village is plundered and his wife is killed by the army of Ecalia as the first part of a scheme by the treacherous Licos (Massimo Serato) to gain the throne of Ecalia for himself. Licos plans that Hercules will come to Ecalia for vengeance and murders the King himself to save Ecalia from his revenge. Hercules learns of the murder of his wife and seeks vengeance, but is thwarted when the Kings daughter and heir to the throne Queen Deianira offers herself rather than her realm of Ecalia as the guilty party.  In accordance with the law Deianira must face the wrath of Hercules; she is bound to a wall as Hercules throws axes at her that miss Deianira proving her innocence in the eyes of the gods.

Hercules admires Deianira and her bravery.  When escorting Deianira back to her capital they come across a band of peasants who have been attacked by a monster.  As Hercules seeks the monster their cattle is stampeded and Hercules kills a wild bull with his dagger.  Arriving in the city Hercules discovers Deinaira is betrothed to Acheloo and leaves.

The resourceful Licos hatches another scheme where he murders Acheloo with the dagger Hercules left behind in the bull; not only casting guilt on Hercules but making Deianira available for marriage to him.  The actual murderer is sent by Licos to the Underworld with the plan that Hercules can only prove his innocence by going to the Underworld where no mortal has ever returned from due to the monstrous Lernaean Hydra|Hydra.

The real murderer is killed by the Hydra, Hercules kills the Hydra but his battle weakens him into unconsciousness where he his rescued by the Amazons, ruled by Queen Némée (Moira Orfei). Némée turns her lovers into living trees after making love to them, but Hercules is only interested in Deinaira. Angered that a man is not interested in her but determined to make love to Hercules and turn him into a tree, Némées advisor suggests the only way she can gain the attention of Hercules is to change her face and body through magic into resembling Deianira (Mansfield with red hair).  Hercules manages to escape with his life due to the help of the General of the Amazon Army (René Dary) while the Queen is done in by one of her living tree victims. He eventually returns to free Queen Deinaira and Ecalia.

== Production notes ==
This was one of the earlier movies to follow from Italy in the wake of the success of Hercules starring Steve Reeves, and marked an attempt to add some star power to the notion of a muscleman movie, not so much in the title role as the female lead.

Mansfield was offered the film while she was shooting The Sheriff of Fractured Jaw in Spain; she agreed on the condition that her husband Mickey Hargitay played Hercules.  Mansfield received permission from her studio 20th Century Fox to film this movie in the early weeks of 1960 whilst she was four months pregnant 
  
Filmed on location in Italy during the height of the sword and sandal craze, this was not one of Mansfields "loan out" films.  She received a fee of $75,000 for starring in the movie. A hit in Italy, it was later broadcast as a movie of the week in 1966 on American television and has since gained a cult following. Mansfield was in the early stages of pregnancy with her third child when production started.

The scene where Mickey Hargitay wrestles a bull was prepared by treating the animal with tranquilizers first. 

==Main Cast==
{| class="wikitable"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
| Mickey Hargitay || Hercules
|-
| Jayne Mansfield || Queen Deianira / Hippolyta
|-
| Massimo Serato || Licos
|-
| René Dary || Amazon General
|-
| Moira Orfei || Némée
|-
| Gil Vidal || Achilles
|-
|}

==Alternative titles==
*Hercules vs. the Hydra (USA TV title)
*Gli amori di Ercole (Italy)
*Les amours dHercule (France)
*Herkules ja rakkauden kuningatar (Finland)
*O iraklis kai oi Amazones (Greece)
*Die Liebesnächte des Herkules (West Germany)
*Los amores de Hercules (Spain)

==Notes==
 

==Biography==
* 

==External links==
* 

 
 
 
 
 
 
 