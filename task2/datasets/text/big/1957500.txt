Kill Them All and Come Back Alone
{{Infobox film 
 | name = Kill Them All and Come Back Alone
 | image = Go Kill Everybody and Come Back Alone.jpg
 | director = Enzo G. Castellari
 | writer =  Enzo G. Castellari Tito Carpi
 | starring =  Chuck Connors 
 | music = Francesco De Masi
 | cinematography = Alejandro Ulloa
 | editing =   
 | producer =  
 | distributor =   
 | released = 1968 
 | runtime =  
 | awards = 
 | country = Italy
 | language =  
 | budget = 
 }}
 Frank Wolff, and features a film score by Francesco De Masi.

==Story== Confederate prisoner, Clyde McKay, attempts to steal a box of gold from a Union prison camp. He is aided by a group of prisoners and a prison guard but he is double-crossed along the way.

==Cast==
*Chuck Connors as Clyde McKay/Link Frank Wolff as Captain Lynch
*Franco Citti as Hoagy
*Leo Anchoriz as Deker/Dexter
*Giovanni Cianfriglia (as Ken Wood) as Blade
*Roberto DellAcqua (as Robert Widmark) as Kid
*Herules Cortez as Bogard
*Furio Menicori (as Men Fury) as Buddy
*Alfonso Rojas as Sergeant
*John Bartha as Prison camp captain
*Vincenzo Maggio as Soldier
*Osiride Pevarello as Soldier
*Sergio Citti as Soldier
*Antonio Molino Rojo as Soldier
*Pietro Torrisi as Soldier
*Ugo Adinolfi
*C. Fantoni

==Releases==
Wild East Productions released this on a limited edition DVD in 2008.

==References==
 

==External links==
* 

 

 
 
 
 
 


 