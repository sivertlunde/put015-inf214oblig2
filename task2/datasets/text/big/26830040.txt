Oru Abhibhashakante Case Diary
 
{{Infobox film
| name           = Oru Abhibhashakante Case Diary
| image          = Oru Abhibhashakante Case Diary.gif
| image_size     =
| alt            = 
| caption        = 
| director       = K. Madhu
| producer       = M. Ali
| writer         = S. N. Swamy
| narrator       =  Vijayaraghavan  Maathu
| music          = Rajamani   Raveendran
| cinematography = 
| editing        =    
| studio         = 
| distributor    = 
| released       = 1995
| runtime        = 
| country        =  
| language       = Malayalam
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}

Oru Abhibhashakante Case Diary is a 1995 Malayalam movie about a lawyer who seeks justice for a teenage girl who was raped and murdered.

==Synopsis==

A young servant girl is raped, murdered and her body is dumped in a pond. The police accuse Unni Thampuran of murder and arrest him. Following torture methods a confession is extracted from Unni Thamburan and the case is ready to be presented at the court.

Anniyan Kuruvilla (Mammootty), the public prosecutor, discovers flaws in the investigation and declines to fight. Convinced about Unni Thampurans innocence he switches sides and decides to fight for him.The prosecution hires a bent and a crooked lawyer Jagatheesh T Nambiar (Narendra Prasad) and through a barrage of false witnesses and doctored testimonies Unni Thampuran is found guilty and sentenced to Life Imprisonment.

Aniyan Kurivilla, though beaten, doesnt lose hope and starts a private investigation of his own and discovers that a group of spoilt boys Reji and his friends are responsible for the brutal murder. Incidentally they also meet another woman who was raped by the trio. Reji is the son of a very powerful and influential woman, Acama. 

Aniyan Kuruvila reopens the case in court. Nambiar asks the court to dismiss the reopening as such move is very rare. He produces that proves that the accused were in Palakkad attending a wedding and couldnt reach the spot of rape on the time as mentioned in the case file. Aniyan Kuruvila successfully proves the alibi produced as forged and thus helps to serve justice.

==Cast==

*Mammootty ...  Kuruvilla Aniyan Kuruvilla 
*Heera Rajgopal ...  Indu 
*Kaveri ...  Seetha  Maathu ...  Shalini
*Jagathy Sreekumar ...  Mani Kunju   Vijayaraghavan ...  Balan 
*Rajan P. Dev ...  Sugunapal 
*Narendra Prasad ...  Jagadish T. Nambiar 
*Manianpilla Raju ...  Unni Thampuran 
*Oduvil Unnikrishnan ...  Ramavarma Thampuran 
* Sadiq... Police Officer Augustine ...  Bahuleyan 
*Adoor Bhawani ...  Deenamma / Annamma 
*Bindu Panicker ...  Sindhu 
*T. P. Madhavan ...  Pothuval 
*Madhupal ...  Reji 
*Mala Aravindan ...  Kuttan 
*V. K. Sreeraman ...  Rajan  Santhosh ...  Akkammas husband
*Krishna ...  Rejis Friend
*T.S.Krishnan

==External links==
*  
* http://popcorn.oneindia.in/title/7237/oru-abhibhashakante-case-diary.html

 
 
 
 


 