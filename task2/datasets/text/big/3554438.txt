In the Heat of the Sun
{{Infobox film
| name           = In the Heat of the Sun
| image          = Intheheatofthesun.jpg
| image_size     = 
| caption        = 
| director       = Jiang Wen
| producer       = Guo Youliang Hsu An-chin Po Ki
| writer         = Jiang Wen
| based on       =   Xia Yu Ning Jing
| music          = Guo Wenjing
| cinematography = Gu Changwei
| editing        = Zhou Ying
| distributor    = 
| released       = 1994
| runtime        = 134 minutes
| country        = China Mandarin
| budget         =  
}}
 Wild Beast (动物凶猛).

==Synopsis== Xia Yu; some of Monkeys experiences mimic director Jiangs during the Revolution ), who is a teenage boy at the time. Monkey and his friends are free to roam the streets of Beijing day and night because the Cultural Revolution has caused their parents and most adults to be either busy or away. Most of the story happens during one summer, so the main characters are even more free because there is no school. The events of that summer revolves around Monkeys dalliances with his roguish male friends, and his subsequent angst-filled crush with one of the female characters, Mi Lan (Ning Jing).

This film is significant in its unique perspective of the Cultural Revolution. Far from the Cultural Revolution-set films of Chinese 5th-generation filmmakers (Zhang Yimou, Chen Kaige, Tian Zhuangzhuang) which puts the era behind a larger historical backdrop, In The Heat Of the Sun is mellow and dream-like, portraying memories of that era with somewhat positive and personal resonances.  It also acknowledges, as the narrator recalls, that he might have misremembered parts of his adolescence as stated in the prologue: "Change has wiped out my memories. I cant tell whats imagined from whats real",  as the director offers alternative or imagined versions to some events as people seek to romanticize their youthful memories.

==Development==
The film was a co-production of three Chinese studios, and $2 million USD (about $  when adjusted for inflation) of the budget was generated from  . Sunday October 16, 1994. Retrieved on September 23, 2011.  Daniel Vukovich, author of China and Orientalism: Western Knowledge Production and the PRC, wrote that the film version makes its characters "a small group of male friends, plus one female "comrade"" instead of being "violent hooligans". 

The original title of film may be translated as "Bright Sunny Days". In the Heat of the Sun was chosen as its international English title for the film during a film festival in Taiwan as a less politicized name to avoid the original titles positive association of the period during the Cultural Revolution. 

== Cast ==
*Han Dong - Ma Xiaojun (S: 马小军, T:馬小軍, P: Mǎ Xiǎojūn, young boy) Xia Yu - Ma Xiaojun (teenage Monkey)
**At the time of filming, Xia was a high school student,  and was not a professional actor. Wendy Larson, author of From Ah Q to Lei Feng: Freud and Revolutionary Spirit in 20th Century China, wrote that the selection of "an awkward-looking boy" who "contrasts with the more conventional tall good looks" of Liu Yiku was clever on part of Jiang Wen, and that Xia Yu "portrays   as charmingly shy and mischievous in social relationships yet forceful and engaging in his emotions." Larson, p.   (Google Books PT187).  The character has the nickname "Monkey" in the film version. "Monkey" was the nickname of director Jiang Wen.  Derek Elley of Variety (magazine)|Variety says that Xia as Xiaojun has "both an uncanny resemblance to Jiang himself and a likable combination of insolence and innocence." 
*Feng Xiaogang - Mr. Hu (S: 胡老师, T: 胡老師, Hú-lǎoshī), the teacher
*Geng Le - Liu Yiku (S: 刘忆苦, T: 劉憶苦, P: Liú Yìkǔ, teenage)
*Jiang Wen - Ma Xiaojun (adult; narrator)
*Ning Jing - Mi Lan (S: 米 兰, T: 米 蘭, P: Mǐ Lán) Tao Hong - Yu Beipei (于北蓓 Yú Běipèi)
**In the beginning Yu Beipei accompanies the boys and gives rise to sexual tension amongst them, but after Mi Lan is introduced, Yu Beipei does not appear with the group until the second telling of the birthday party. Larson states that Yu Beipei "is a significant character" in the first part of the film and that her disappearance is a "persistent clue that all is not as it seems".  National Games in August 1993. Jiang Wen had intended to find a synchronized swimmer for Mi Lans role when he visited the team, but after spotting Tao, he cast her as Yu Beibei instead. 
*Shang Nan - Liu Sitian (S: 刘思甜, T:劉思甜, P: Liú Sītián)
*Wang Hai - Big Ant
*Liu Xiaoning - Liu Yiku (adult)
*Siqin Gaowa - Zhai Ru (翟 茹 Zhái Rú - Xiaojuns mother)
*Wang Xueqi - Ma Wenzhong (S: 马文中, T:馬文中, P: Mǎ Wénzhōng - Xiaojuns father)
*Fang Hua - Old general
*Dai Shaobo - Yang Gao (羊搞 Yáng Gǎo)

==Music==
The Chinese version of the Soviet song "Moscow Nights" features prominently in the film.

==Reception==
The film was commercially successful in China. Vukovich wrote that the film however did cause some controversy in China for its perceived "nostalgic" and "positive" portrayal of the Cultural Revolution. Vukovich, page unstated (Google Books  ). 

According to Vukovich, the film "received much less attention than any fifth-generation classics" despite the "critical appreciation in festivals abroad".  Vukovich stated that in Western countries "the film has been subjected to an all too familiar coding as yet another secretly subversive, dissenting critique of Maoist and Cultural Revolution totalitarianism",  with the exceptions being the analyses of Chen Xiaoming from Mainland China and Wendy Larson. 

===Awards and recognition=== 51st Venice Best Actor Xia Yu (Xia was then the youngest recipient of the Best Actor award at Venice) as well as the Golden Horse Film Awards for Best Picture, Best Director and Best Actor.   American director Quentin Tarantino also gave high praises to the film, calling it "really great." 

It was the first Peoples Republic of China film to win Best Picture in the Golden Horse Film Awards, in the very year where a liberalization act allows Chinese-language films from the mainland to participate.

== References ==
* Larson, Wendy. From Ah Q to Lei Feng: Freud and Revolutionary Spirit in 20th Century China. Stanford University Press, 2009. ISBN 0804769826, 9780804769822.
* Vukovich, Daniel. China and Orientalism: Western Knowledge Production and the PRC (Postcolonial Politics). Routledge, June 17, 2013. ISBN 113650592X, 9781136505928.

==Notes==
 

==Further reading==
* Qi Wang. "Writing Against Oblivion: Personal Filmmaking from the Forsaken Generation in Post-socialist China." (dissertation) ProQuest, 2008. ISBN 0549900683, 9780549900689. p.&nbsp;149-152. 
*Silbergeld, Jerome (2008), Body in Question: Image and  Illusion in Two Chinese Films by Director Jiang Wen (Princeton: Princeton University Press)
*Su, Mu ( , 2013. ISBN 1625165080, 9781625165084. See   at Google Books.

== External links ==
 
 
*  
*  
* Braester, Yomi. " ." Screen 42:4 Wnter 2001.
* Lu, Tonglin. " ." Project MUSE. ( )
* Williams, Louise. " ." China Information 2003 17: 92  . ( )
*" ." ( ). Wooster University.
* (hk)   at the Hong Kong Movie Database, hkmd.com Inc
 

 
 

 
 
 
 
 
 
 
 
 
 