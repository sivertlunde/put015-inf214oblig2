Chic!
{{Infobox film
| name           = Chic!
| image          = 
| caption        =
| director       = Jérôme Cornuau
| producer       = Alain Terzian
| writer         = Jean-Paul Bathany
| starring       = Fanny Ardant   Marina Hands  Éric Elmosnino Laurent Stocker
| music          = René Aubry 
| cinematography = Stéphane Cami 	 
| editing        = Brian Schmitt
| studio         = Alter Films   StudioCanal   France 2 Cinéma
| distributor    = StudioCanal 
| released       =  
| runtime        = 103 minutes
| country        = France
| language       = French
| budget         = 
| gross          = 
}}

Chic! is a 2015 French romantic comedy film directed by Jérôme Cornuau. 

== Cast ==
* Fanny Ardant as Alicia Ricosi 
* Marina Hands as Hélène Birk 
* Éric Elmosnino as Julien Lefort 
* Laurent Stocker as Alan Bergam   
* Catherine Hosmalin as Caroline Langer 
* Philippe Duquesne as Jean-Guy 
* India Hair as Karine Lefort

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 

 
 