Don (1978 film)
 
 

{{Infobox film
| name = Don
| image = Don 1978 poster.jpg
| caption = Film Poster
| director = Chandra Barot
| producer = Nariman A. Irani Nariman Films
| writer = Salim-Javed Pran Helen Helen Iftekhar
| music = Kalyanji Anandji Anjaan Indeewar
| cinematography = Nariman A. Irani
| editing = Wamanrao
| released =    
| studio = Nariman Films
| runtime = 175 minutes
| country = India
| language = Hindi
| budget =   
| gross =     
| preceded_by =
| followed_by =
}}
 Anjaan and Helen and A remake sequel were released on 20 October 2006 and 23 December 2011, respectively.

==Plot== its title, Don makes a few other enemies through his merciless approach to running his organization. Notably, Don kills one of his own men, Ramesh, when Ramesh decides to leave the business. This introduces Don to two new enemies, Kamini (Helen Jairag Richardson|Helen), Rameshs fiancee, and Roma, (Zeenat Aman) Rameshs sister. When Kamini seduces Don and attempts to have the police arrest him, her plan backfires as Don outsmarts her and the police in his escape. In the process, Kamini is killed. A shattered, revenge-seeking Roma gets her hair cut short, trains in judo and karate, then enters Dons gang after deceiving them into thinking that she too is on the wrong side of the law. Don is impressed with her fighting skills and allows her to work for him without suspecting any ulterior motive. 

After years of unsuccessful attempts at nabbing Don, the police finally succeed. Unfortunately, Don dies in the heat of the chase, botching Officer DSilvas plan to reach the source of all crime—the man Don reported to—through capturing Don alive. DSilva buries Dons body, ensuring that people believe he may still be alive. As luck would have it, DSilva remembers his chance encounter with Vijay (Amitabh Bachchan), a simpleton trying to survive in the hustle and bustle of Bombay in order to support two small foster children, who is an exact lookalike of Don. DSilva hatches a plan to transform Vijay into Don so he can arrest the rest of the gang.

Around the time Vijay "returns" to Dons gang as Don under the guise of amnesia, Jasjit (Pran (actor)|Pran), just released from jail, begins his mission of revenge against DSilva and his search for his children Deepu and Muni, who had been saved and taken care of by Vijay. Vijay manages to replace the red diary with a blank one, and tells his gang that he is going to take revenge on the DSP, but is actually going there to give him the red diary. Roma goes after him, but Vijay survives the attack and he tries to explain to her that he is not Don, but Vijay. She refuses to believe him at first but DSilva intervenes and tells her that the man she is trying to kill is indeed Vijay.

Meanwhile, as Vijay learns more and more about Don through his discovery of his diary and Romas help, he announces to his colleagues that his memory has returned. Celebrations ensue as Don announces his return to the world, but things take a drastic turn when the police raid the celebrations, acting upon Vijays information, but Vijays only witness to his true identity, DSilva, dies in the crossfire.

Tangled in a web of confusion where the police refuse to believe that he is Vijay while his underworld gang realize that he is indeed not Don, Vijay incites the ire of both the police and Dons right-hand man, Narang. To add to Vijays woes, the diary that Vijay had handed over to DSilva – his last hope of proving his innocence – is stolen by Jasjit in an attempt to track down his lost children, without realizing that Vijay is the one man who can reunite them. Vijay escapes the clutches of the police and the underworld with Romas help and returns to his old self though he struggles to prove his identity and innocence.  After Vijay fights against Vardans men Roma ends up getting the diary. One of the gangsters snatches it from her and burns it. The ending reveals that the burnt diary was, in fact, the fake diary, and that Vijay had the real one in order to trick Vardan. Vijay gives the proof to the police and all charges are put against him. Vardan is arrested and Vijay returns to his old life.

==Cast and Crew==

===Cast=== Don and Vijay: Don is the most wanted criminal. The police are always unsuccessful at nabbing him. However, in a police encounter Don is fatally shot. Vijay is the spitting image of Don and is soon forced to pose as Don in order to help the police arrest Dons gang members.
* Zeenat Aman as Roma: A simple girl whose brother Ramesh works for Don. Roma despises Don for his having murdered her brother. She joins Dons business with the secret motive to kill him. Pran as Jasjit (JJ)
* Iftekhar as DSP DSilva
* Om Shivpuri as Vardan/Interpol Officer R.K. Malik (Impostor)
* Pinchoo Kapoor as Interpol Officer R.K. Malik (Real)
* Satyendra Kapoor as Inspector S. Verma
* Jagdish Raj as Police Officer
* Keshav Rana as Police Officer
* Abhimanyu Sharma as Inspector Sharma
* Prem Sagar as Police Inspector (inspected the ambulance)
* Paidi Jairaj as Dayal, Judo Karate Instructor
* Kamal Kapoor as Narang
* Arpana Choudhary as Anita Helen as Kamini
* M. B. Shetty as Shakaal
* Mac Mohan as Mac
* Baby Bilkish as Munni
* Alankar Joshi as Deepu
* Moolchand as Govinda
* H. L. Pardesi as Banarasi, Pan Seller
* Gyanesh DJ as Police Officer
* Sharad Kumar as Ramesh
* Kedar Saigal as Doctor
* Rajan Haksar as Kishan, Jasjits Employer
* Yusuf Khan as Vikram

===Crew===
* Director: Chandra Barot
* Writer: Salim-Javed
* Producer: Nariman A. Irani
* Production Company: Nariman Films
* Cinematographer: Nariman A. Irani
* Editor: Wamanrao
* Art Director: Sudhendu Roy
* Costume Designer: Ramola Bachchan, V. Scharwachter
* Wardrobe: S. Irani, Manikrao Jagtap, Mani J. Rabadi, Mehboob Shaikh
* Stunts: Haji Khan, A. Mansoor
* Choreographer: P. L. Raj
* Music Director: Kalyanji Anandji
* Lyricist: Anjaan (lyricist)|Anjaan, Indeevar
* Playback Singers: Asha Bhosle, Kishore Kumar, Lata Mangeshkar, Amitabh Bachchan

==Production==
Producer and cinematographer Nariman Irani was in a financial mess when his film Zindagi Zindagi (1972) starring Sunil Dutt flopped. He was in debt for Rs 1.2&nbsp;million and couldnt pay the money off on a cinematographers salary. When he was doing the cinematography for Manoj Kumars major hit Roti Kapada Aur Makaan (1974), the films cast (Amitabh Bachchan, Zeenat Aman, Pran (actor)|Pran) and crew (assistant director Chandra Barot) decided to help him out. They all recommended that he produce another film and that they would participate in its production. They all approached scriptwriting duo Salim-Javed, who gave them an untitled script that had already been rejected by the entire industry. The script had a character named Don (character)|Don. Bachchan would play Don, and Barot would direct the film. Aman and Pran would play key roles in the film.  The film took three-and-a-half years to complete.  Before filming was completed, producer Irani died from an accident on the set of another film he was working on. Barot faced budget restraints but received aid.  Aman did not take any money for her work in the film.  Barot showed the film to his mentor Manoj Kumar, who felt that the film was too tight and needed a song in the midst of the action-filled film, and so "Khaike Paan Banaraswala" was recorded. The film was released without any promotion on 12 May 1978 and was declared a flop the first week. Within a week, the song "Khaike Paan Banaraswala" became a big hit, and word of mouth spread, so by the second week, the films fortunes were reversed and it was declared a blockbuster. The profits from the film were given to Iranis widow to settle her husbands debts. 

The hit-song "Khaike Pan Banaraswala" was choreographed by P.L. Raj.   

==Soundtrack==
{{Infobox album
| Name = Don
| Type = soundtrack
| Artist = Kalyanji Anandji
| Cover = Don 1978 CD.jpg
| Released = 1978
| Recorded =
| Genre =
| Length =
| Label = EMI Records
| Producer = Kalyanji Anandji
| Reviews =
| Last album =
| This album =
| Next album =
}} Anjaan and Indeevar|Indivar.

According to film music expert Rajesh Subramanian the song "Khaike Pan Banaraswala" was composed by Babla (younger brother of famous music director Kalyanji Anandji.

Kishore Kumar and Asha Bhosle received accolades at filmfare for the tracks "Khaike Pan Banaraswala" and "Yeh Mera Dil" respectively.
{| class="wikitable"
|-
!Song Title !!Singer !!Lyricist !!Time
|-
|"Main Hoon Don" Kishore Kumar Anjan
|4:45
|-
|"Yeh Hai Bombay Nagaria" Kishore Kumar Anjan
|5:53
|-
|"Khaike Pan Banaraswala" Kishore Kumar Anjan
|3:57
|-
|"Jiska Mujhe Tha Intezar" Lata Mangeshkar, Kishore Kumar Anjan
|4:20
|-
|"Yeh Mera Dil" Asha Bhosle Indeevar
|4:18
|-
|}

==Awards==
* Amitabh Bachchan won Filmfare Award for Best Actor, for this movie.
* Asha Bhosle won Filmfare Award for Best Female Playback Singer for the song "Yeh Mera Dil".
* Kishore Kumar won Filmfare Award for Best Male Playback Singer for the song "Khaike Paan Banaraswala".

==Legacy and influence== China Town (1962) and is apparently inspired by it. The movie had Shammi Kapoor playing the double roles of both a gangster and his lookalike - a common man turned informer. Don was itself remade several times. 

===Don series===
 

The film was remade in 2006 as Don (2006 Hindi film)|Don starring Shah Rukh Khan in the lead role of Don (character)|Don, Priyanka Chopra as Roma, Arjun Rampal as Jasjit, Boman Irani as DSilva, and Om Puri as Malik. It was directed by Farhan Akhtar. With some changes in script, the film proved to be one of the highest grossing films of the year. A sequel to that film, Don 2 was released on 23 December 2011. 

===Other Hindi remakes===
* One Hindi remake was titled Dav Pech (1989), starring Jeetendra. It was unsuccessful.
* Duplicate (1998 film)|Duplicate (1998), directed by Mahesh Bhatt and starring Shahrukh Khan and Juhi Chawla, is partly a parody of Don.
* A film with the similar plot line was made starring Bollywood actress Rekha. The film was named Madam X. The plot was similar that the look-alike of a don replaces him after the Don was caught/died. The look-alike was used to catch the whole gang which has international network in drugs and smuggling. Madam X has the same plot: The lead character is a gangster who is caught and replaced in the gang by her look-alike who was a road singer. This film was not a big hit but showed the acting prowess of Rekha who was on the top in the industry when it was released. Don was successful .
===Remakes in other languages=== Telugu
* Telugu as Yugandhar, starring N.T. Rama Rao|NTR, Jayasudha and Jayamalini.  Namitha and Tamil remakes.
 Tamil
* Tamil as Billa (1980 film)|Billa, starring Rajnikanth.  Helen (actress)|Helen, who played Kamini in the original Hindi film  Don (1978), repeated her role in this remake.
* The 2006 Hindi remake starring Shahrukh Khan, titled Don (2006 Hindi film)|Don, inspired the Tamil directors who made the Ajith Kumar starrer Billa (2007 film)|Billa — a remake of the same-titled Rajinikanth film.
* Billa II (2012) is a Tamil film starring Ajith and a prequel to Billa (2007).
 Malayalam
* In 1986, the movie was remade in Malayalam as Shobaraj, starring Mohanlal and Madhavi (actress)|Madhavi. Superhit in Kerala

;English Hollywood movie The Death and Life of Bobby Z had a similar premise to this movie. It revolved around a lookalike of a gang leader who is asked to masquerade as him by law enforcement officer(s), though the remainder of the plot was largely different. It is based on a 1997 novel of the same name. The novel was well received, but the movie received negative critical and commercial response.

===Music=== sample from BMI Award for being the originators of the melodies used in "Dont Phunk with My Heart."   

The third season American Dad! episode "Tearjerker (American Dad!)|Tearjerker" uses the 1978 Don theme music in its intro sequence.

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 