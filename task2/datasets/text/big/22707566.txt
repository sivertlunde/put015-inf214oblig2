Listen to the Voices of the Sea
{{Infobox Film
| name = Listen to the Voices of the Sea
| image = Listen to the Voices of the Sea - 1950 movieposter.jpg
| image_size =
| caption = Theatrical poster for Listen to the Voices of the Sea (1950)
| director = Hideo Sekigawa 
| producer = Shigeru Okada
| writer = 
| narrator = 
| starring = 
| music = Akira Ifukube
| cinematography = 
| editing = 
| distributor = Toei Company 1950
| runtime = 109 min.
| country = Japan Japanese
| budget = 
| gross = 
| preceded_by = 
| followed_by = 
}}
  1950 black-and-white Listen to the Voices from the Sea.
 cynicism of their commanders. Soldiers are also victims of military bullying by their commanders.

==Cast==
* Hajime Izu ( )
* Yasumi Hara ( )
* Akitake Konō ( )
* Kinzō Shin ( )
* Haruko Sugimura ( )
* Yuriko Hanabusa ( )
* Yōichi Numata ( )
* Sōji Kamishiro ( )
* Kōichi Hayashi ( )
* Kyōsuke Tsuki ( )
* Toshio Takahara ( )
* Kazuo Tokito ( )
* Tokue Hanazawa ( )
* Yoshio Ōmori ( )
* Shōzō Inagaki ( )
* Tamotsu Kawasaki ( )
* Kiichi Sugi ( )
* Asao Sano ( )
* Kazuo Masabuchi ( )
* Shōichi Onjō ( )
* Tadashi Suganuma ( )
* Jirō Kozaki ( )
* Kieko Sawamura ( )
* Kōichi Fujima ( )

==DVD release== motion picture rating system in the Czech Republic).

==See also==
* List of films in the public domain

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 

 