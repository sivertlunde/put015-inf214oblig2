Dollar Down
 
{{Infobox film
| name           = Dollar Down
| image          = 
| caption        = 
| director       = Tod Browning
| producer       = 
| writer         = Jane Courthope Ethel Hill Frederick Stowers
| starring       = Ruth Roland Henry B. Walthall
| music          = 
| cinematography = Allen Q. Thompson
| editing        = 
| distributor    = Truart Films States Rights
| released       =  
| runtime        = 6 reels
| country        = United States 
| language       = Silent with English intertitles
| budget         = 
}}

Dollar Down is a 1925 American drama film directed by Tod Browning.    One of the six reels is missing.   

==Cast==
* Ruth Roland - Ruth Craig
* Henry B. Walthall - Alec Craig
* Mayme Kelso - Mrs. Craig
* Earl Schenck - Grant Elliot
* Claire McDowell - Mrs. Meadows (Craigs sister)
* Roscoe Karns - Gene Meadows
* Jane Mercer - Betty Meadows (her daughter)
* Lloyd Whitlock - Howard Steele
* Otis Harlan - Norris
* Edward W. Borman - Tilton (as Edward Borman)
* Michael Dark (uncredited)

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 


 