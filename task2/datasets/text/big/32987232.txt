Captain Starlight, or Gentleman of the Road
 
 
{{Infobox film
  | name     = Captain Starlight, or Gentleman of the Road
  | image    = 
  | caption  =  Alfred Rolfe		
  | producer = Charles Cozens Spencer Alfred Rolfe
  | based on = play by Alfred Dampier & Garnet Walch novel by Rolf Boldrewood Alfred Rolfe Lily Dampier
  | music    = 
  | cinematography = Ernest Higgins
  | editing  = Ernest Higgins
  | distributor = 
  | studio = Spencers Pictures
  | released = 16 March 1911 "Raymond Longford", Cinema Papers, January 1974 p51  
  | runtime  = 3,500 feet 
  | language = Silent film English intertitles
  | country = Australia
  | budget   = 
  }}

Captain Starlight, or Gentleman of the Road is a 1911 Australian silent film about the bushranger Captain Starlight. It was based on Alfred Dampiers stage adaptation of the novel Robbery Under Arms. 

It is considered a lost film.

==Film==
It is possible to gauge some idea of the plot from contemporary reviews. One critic wrote that "plenty of relief in the way of comedy has been introduced; and whilst there are many grim incidents, these are not allowed to unduly obtrude themselves."
   

The film started at the home of the Marston family. Some troopers, led by Sergeant Goring, are looking for Captain Starlight. Goring harasses Aileen Marston when a Mr Beresford appears. Beresford introduces himself as Captain Starlight and tells them to put their hands up. According to a contemporary report, "Sergeant Goring, with both hands in the air, carries the situation off with as much dignity as can be expected, but some of his men fall to the ground in terror." 

Then Starlight and his friends go to the horse races at Turon where Starlight enters his horse Rainbow in the Goldfield Cup under the name Darkie. Starlight encourages all his friends to bet on the horse, but then his jockey is found to be too drunk to ride and Starlight has to run the race himself. He wins but is chased off the course by police.

Starlight and his gang, including the vicious Dan Moran, hold up the mail coach at Rocky Rises. Moran robs a pretty young girl but Starlight points his revolver at him and persuades him to hand the young lady back her money. When Starlight discovers that the young lady has no other money apart from that on her possession, he takes her purse and puts a wad of bank notes in it then restores it to her.   

Later Sir Ferdinand Morringer is captured by Moran. He is tied to a tree at Terrible Hollow in order to make him cofess where they money to pay the troopers has been concealed. Morringer will not tell so Moran starts beating him. The bushranger is about to kill Morringer when Starlight intervenes, overpowers Moran and sets Morringer free.  

Moran is captured by the police He leads Sergeant Goring and the troopers to Starlights stronghold. During the final battle, Moran is mortally wounded but Starlights life is saved by Warrigul, his faithful aboriginal servant who carries him across a river to safety.

Starlight and Dick Marston are subsequently pardoned for their crimes, due in part to the influence of Morringer. (This happy ending was in contrast to the novel but consistent with the play adaptation).  

Contemporary advertisements listed a synopsis of scenes and events. They were as follows: 
*The Home of the Marstons
*Starlight Protects Aileen from Insult
*The Race for the Gold Cup
*The Favourite Wins
*The Rocky Rises
*Sticking up the Mail Coach
*An Awkward Situation
*Sir Ferdinand in a Tight Corner
*Storefields Homestead
*Moran and His Gang Foiled
*The Burning Stables
*Rescue of the Horses
*Starlights Last Stand
*Warrigals Devotion
*The Day of Reckoning and Consolation

According to contemporary reports the character of "Warrigul is always funny-except in the touching scene where, careless of his own life, he brings the wounded Starlight to a place of safety." 

==Cast== Alfred Rolfe as Captain Starlight 
*Lily Dampier as Eileen Marston
*Raymond Longford
*Augustus Neville
*Lottie Lyell
*Stanley Walpole
According to a contemporary review "the parts of Sir Ferdinand Morringer and of Sergeant Goring were played by two officials in the police force, who, as most people will agree, would have made their mark at acting had they not takes to the more prosaic business of preserving the peace."  Warrigul was played by a white actor.

==Original stage adaptation==
{{Infobox play
| name       = Robbery Under Arms
| image      = Robbery Under Arms play.jpg
| image_size = 
| caption    = Poster from a 1902 production
| writer     = Alfred Dampier Garnet Walch
| characters =
| setting    = 
| premiere   = 1 March 1890
| place      = Alexandra Theatre, Melbourne
| orig_lang  = English
| subject    = 
| genre      = Melodrama
}}
In collaboration with journalist Garnet Walch, Dampier adapted the novel into a play in 1890.

The play made some key changes to the story, such as:
*making Dick Marston and Captain Starlight both innocent of murder, enabling them to be both pardoned; 
*Starlight was no longer killed and Dick Marston did not serve 12 years in gaol;  
*Sub Inspector Goring, a relatively minor character in the novel was turned into more of a villain, harassing Aileen Marston;
*the major villain was bushranger Dan Moran;
*the climax involves Moran holding Sir Ferdinand captive in Terrible Hollow, rescued by Starlight;
*it ends with Moran attempting to stab Dick Marston, killing Kate Morrison who saves Dicks life;
*the addition of comic Irish policemen, O’Hara and Maginnis, plus a comic maid, Miss Euphrosyne Aspen (variations on these characters appeared in plays based on the Kelly Gang);
*changing the character of Warrigan from a villainous type loyal to Starlight to a comic character.   accessed 26 November 2014 

The play drew heavily on the story of Ned Kelly and his family – in particular Constable Fitzpatricks harassment of Kate Kelly, which inspired Gorings treatment of Martson – as well as the themes of the novel Les Misérables. 

It is considered likely Garnet Walchs main contribution to the play was providing a comic subplot. 

The play was highly successful and was much revived during the 1890s and 1900s, including performances in London in 1894. Dampier played the role of Captain Starlight on stage many times to great acclaim, and it was the role he was most identified with during his career.  

Alfred Rolfe, who later directed the movie, was well versed with the play, having played Sir Ferdinand Morringer in its first production, George Storefield in a revival, and Dick Marston in later productions. In 1893 he married Dampiers daughter Lily, who played Aileen Marston in numerous productions of the play. 

The play was highly influential on bushranging drama as a genre, leading to a number of imitators such as Arnold Denhams The Kelly Gang (1899), which was likely the basis for the film The Story of the Kelly Gang (1906). 

==Production== 1907 film version of the novel which was still playing in cinemas. 

==Reception==
It appears the film was a success at the box office, screening in cinemas as late as 1917. 

===Critical===
It also received good reviews, the Sunday Times saying "the chase scene through the bush and across a lagoon is very realistic."  The Register called it "a thrilling and absorbingly interesting story of Australian bush life, and several daring feats of horsemanship are displayed." 

The Sydney Morning Herald stated that "one of the most applauded of the scenes... was the burning of the stables and the rescue of the horses, Starlights Last Stand also excited enthusiasm." 

The Argus said the film was:
 A series of exciting incidents, situations, and escapades... sustained interest throughout and tells the story in a straightforward businesslike way, with a judicious mixture of comedy to relieve it from too great a weight of sensationalism. Much picturesque scenery is introduced, and many fine views of the bush are shown the most notable examples being Terrible Hollow, the Rocky Rises and the localities selected for the sticking up of the mail coach, and Starlights last stand against the police.    

==References==
 
*Fotheringham, Richard, "Introduction", Robbery Under Arms by Alfred Dampier and Garnet Walch, Currency Press 1985

==External links==
*  
*  at AustLit
*   at National Film and Sound Archive
* 

 
 

 
 
 
 
 
 
 