3 Ninjas
 
{{Infobox film
| name = 3 Ninjas
| image = 
| alt = 
| caption =  Simon S. Sean McNamara (4)
| producer = Martha Chang James Kang
| writer = 3 Ninjas: Kenny Kim Edward Emanuel Kick Back: Sang-ok Shin Mark Saltzman Knuckle Up: Alex S. Kim High Noon at Mega Mountain: Sean McNamara Jeff Phillips Victor Wong Richard Marvin (1 & 2) Gary Stevan Scott Louis Febre (3) John Coda (4)
| cinematography = Richard Michalak (1) Christopher Faloona (2) Eugene Shluglet (3) Blake T. Evans (4) David Rennie (1) David Rennie Jeffrey Reiner (2) Pam Choules (3) Annamaria Szanto (4)
| studio = Sheen Productions (2-4)
| distributor = Touchstone Pictures (1) TriStar Pictures (2-4)
| released =  
| runtime = 355 minutes
| country = United States
| language = English
| budget = $26.5 million (First two films)
| gross = $41,588,439
}}
3 Ninjas is a series of action films directed towards the pre-teen audience about the adventures of three young brothers who are trained by their Japanese grandfather in the art of Ninjutsu.

==Film series==
===3 Ninjas===
 
Each summer, Samuel, Jeffrey, and Michael, three suburban California brothers, visit their grandfather Mori Tanakas cabin in the desert to train to become ninjas. Given the ninja-names Rocky, Colt, and Tum Tum, the boys witness a confrontation between their grandfather and Hugo Snyder, ex-student/partner of Tanaka and a terrorist who is being pursued by the boys father, FBI agent Sam Douglas, on their last day of summer vacation. Snyder plans to kidnap the boys with the help from his assistant Mr. Brown, who employs his irresponsible nephew Fester and his friends Hammer and Marcus to abduct them. After a failed attempt at kidnapping foiled by the three boys outsmarting the three men while their parents are out, Mr. Brown along with heavyweight fighter "Rushmore" appear and they easily capture the trio, leaving a note with next door neighbor Emily telling Sam that Snyder has kidnapped his children for his constant attempts to arrest him. Mori arrives to the house and Sam reluctantly agrees to give him one hour to rescue the children. Mori tracks the children to a ship at the docks where Snyder is training an army of ninjas and begins searching for the boys and Snyder. Meanwhile, the boys escape their containment cell using the training bestowed by their Grandfather and begin fighting their way out of the ship, leading up to a final confrontation between Snyder and Mori. Mori wins despite Snyders foul play and Sam ambushes the ship with a slew of FBI agents, killing Snyder and foiling his evil organization.

===3 Ninjas Kick Back===
 
During a championship baseball game, Rocky, Colt, and Tum Tum have to decide to go to Japan to help their grandfather Mori Tanaka or stay for the baseball game but they knew they had to take the dagger to him. On the way they meet Miyo who Rocky is very fond of. When Koga gets the dagger and sword he tries to open the cave of gold. After that Koga and Mori become friends and Miyo gets the dagger. On their whole Japan adventure they learned a lot of important skills and life lessons that helps them win the final baseball game of their season. In the end Colt hits a home run and the 3 ninjas end up winning the game along with Miyo. A poor sport of the opposing team picks on the boys and Miyo after the game, while attempting to attack the girl Miyo thinking it will be an easy challenge he is easily mistaken and he ends up getting knocked out to close out the finishing scene.

===3 Ninjas Knuckle Up===
 
Rocky, Colt, and Tum Tum must battle an evil wealthy toxic waste dumper in order to save a local Indian tribe and their friend Jo. The 3 Ninjas must help find Jos father and find a secret disk that contains evidence that could stop the toxic landfill that is destroying the Indian community. However the town is owned by the rich man and he controls the police and even the mayor. They must fight a motorcycle gang and renegade cowboys in this non-stop ninja adventure.

===3 Ninjas: High Noon at Mega Mountain===
 
Rocky, Colt, and Tum Tum-along with their neighbor friend, computer whiz Amanda-are visiting Mega Mountain amusement park when it is invaded by an army of ninjas led by master criminal Mary Ann "Medusa" Rogers. Now the ninjas have to break Medusas vicious plans and liberate Mega Mountain.

==Cast==
{| class="wikitable" style="text-align:center; width:99%;"
|-
! rowspan="2" style="width:15%;"| Character
! colspan="4" style="text-align:center;"| Film
|- 3 Ninjas
!  style="text-align:center; width:15%;"| 3 Ninjas Kick Back
!  style="text-align:center; width:15%;"| 3 Ninjas Knuckle Up
!  style="text-align:center; width:15%;"|  
|-
! Mori Tanaka Victor Wong
|-
! Samuel Rocky Douglas Jr.
| colspan="1" | Michael Treanor
| colspan="1" | Sean Fox
| colspan="1" | Michael Treanor
| colspan="1" | Mathew Botuchis
|-
! Jeffrey Colt Douglas
| colspan="3" | Max Elliott Slade
| colspan="1" | Michael OLaskey II
|-
! Michael Tum Tum Douglas
| colspan="1" | Chad Power
| colspan="1" | J. Evan Bonifant
| colspan="1" | Chad Power
| colspan="1" | James Paul Roeske II
|-
! Samuel Douglas Sr.
| colspan="2" | Alan McRae
| colspan="1" style="background:#d3d3d3;" |
| colspan="1" | Alan McRae
|-
! Jessica Douglas
| colspan="2" | Margarita Franco
| colspan="1" style="background:#d3d3d3;" |
| colspan="1" | Margarita Franco
|-
! Emily
| colspan="1" | Kate Sargeant
| colspan="3" style="background:#d3d3d3;" |
|-
! Hugo Snyder
| colspan="1" | Rand Kingsley
| colspan="3" style="background:#d3d3d3;" |
|-
! Rushmore
| colspan="1" | Professor Toru Tanaka
| colspan="3" style="background:#d3d3d3;" |
|-
! Mr. Brown
| colspan="1" | Joel Swetow
| colspan="3" style="background:#d3d3d3;" |
|-
! Miyo
| colspan="1" style="background:#d3d3d3;" |
| colspan="1" | Caroline Junko King
| colspan="2" style="background:#d3d3d3;" |
|-
! Koga
| colspan="1" style="background:#d3d3d3;" |
| colspan="1" | Sab Shimono
| colspan="2" style="background:#d3d3d3;" |
|-
! Glam
| colspan="1" style="background:#d3d3d3;" |
| colspan="1" | Dustin Nguyen 
| colspan="2" style="background:#d3d3d3;" |
|-
! Jo
| colspan="2" style="background:#d3d3d3;" |
| colspan="1" | Crystle Lightning
| colspan="1" style="background:#d3d3d3;" |
|-
! Charlie
| colspan="2" style="background:#d3d3d3;" |
| colspan="1" | Donald L. Shanks
| colspan="1" style="background:#d3d3d3;" |
|-
! Jack Harding
| colspan="2" style="background:#d3d3d3;" | Charles Napier
| colspan="1" style="background:#d3d3d3;" |
|-
! J.J.
| colspan="2" style="background:#d3d3d3;" |
| colspan="1" | Patrick Kilpatrick
| colspan="1" style="background:#d3d3d3;" |
|-
! Dave Dragon
| colspan="3" style="background:#d3d3d3;" |
| colspan="1" | Hulk Hogan
|-
! Medusa
| colspan="3" style="background:#d3d3d3;" |
| colspan="1" | Loni Anderson
|-
! Lothar Zogg
| colspan="3" style="background:#d3d3d3;" |
| colspan="1" | Jim Varney
|-
! C.J.
| colspan="3" style="background:#d3d3d3;" |
| colspan="1" | Dwayne Carrington
|-
! Carl
| colspan="3" style="background:#d3d3d3;" |
| colspan="1" | Kirk Baily
|-
! Buelow
| colspan="3" style="background:#d3d3d3;" |
| colspan="1" | Travis McKenna
|-
! Zed
| colspan="3" style="background:#d3d3d3;" |
| colspan="1" | Brendan OBrian
|-
! Jennifer
| colspan="3" style="background:#d3d3d3;" |
| colspan="1" | Lindsay Felton
|-
! Amanda
| colspan="3" style="background:#d3d3d3;" |
| colspan="1" | Chelsey Earlywine
|-
|}

==Release==
===Critical reception===
{| class="wikitable" width=99% border="1"
! Film
! Rotten Tomatoes
! Internet Movie Database|IMDb
|-
| 3 Ninjas
| 29% 
| 5.1/10 
|-
| 3 Ninjas Kick Back
| 17% 
| 4.1/10 
|-
| 3 Ninjas Knuckle Up
| 0% 
| 3.9/10 
|-
| 3 Ninjas: High Noon at Mega Mountain
| 0% 
| 2.6/10 
|}

===Box office performance===
{| class="wikitable" width=99% border="1"
! Film
! Release date
! Budget
! Gross
|-
| 3 Ninjas
| August 7, 1992
| $6.5 million
| $29,000,301
|-
| 3 Ninjas Kick Back
| May 6, 1994
| $20 million
| $11,798,854
|-
| 3 Ninjas Knuckle Up
| April 7, 1995
| N/A
| $413,479
|-
| 3 Ninjas: High Noon at Mega Mountain
| April 10, 1998
| N/A
| $375,805
|-
! colspan=2 | Total
! $26.5 million
! $41,588,439
|}

==References==
 

 

 