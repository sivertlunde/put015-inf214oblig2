Mansion of the Living Dead
{{Infobox film
| name           = Mansion of the Living Dead
| image          = 
| alt            = 
| caption        = 
| film name      = La mansión de los muertos vivientes
| director       = Jesús Franco
| producer       = 
| screenplay     = Jesús Franco
| based on       =  
| starring       = {{plainlist| Candy Coster
* Mabel Escano
* Robert Foster
* Eva León
}}
| music          = Jesús Franco
| cinematography = Jesús Franco
| editing        = Jesús Franco
| studio         = Golden Films Internacional
| distributor    = 
| released       =  
| runtime        = 
| country        = Spain
| language       = Spanish
| budget         = 
| gross          = 
}}
Mansion of the Dead ( ) is a 1982 erotic horror film written and directed by Jesús Franco, based on his own novel.  It stars Lina Romay, who is credited as Candy Coster.   

== Plot ==
Several waitresses visit a resort hotel, only to find that the former monasterys monks have returned as zombies.

== Cast == Candy Coster
* Mabel Escano
* Robert Foster
* Eva León

== Release ==
Severin Films released it on DVD on October 31, 2006. 

== Reception ==
Ian Jane of DVD Talk rated it 3.5/5 stars and wrote, "More of an artsy softcore romp than a horror film, Mansion Of The Living Dead should still appeal to those who appreciate the low budget charm of Francos erotic films and who dig the odd touches that can be found in so much of his work."   Rob Lineberger of DVD Verdict wrote that the film consists mostly of boring scenes where the actresses walk around the set.   Writing in The Zombie Movie Encyclopedia, academic Peter Dendle said, "The film is marked throughout, however, by a disturbing hostility toward women: they are sexual objects from the start to the finish, and the voyeuristic impulse would seem less offensive if it werent accompanied by unashamed misogynistic violence."     Dendle called the zombies featured in the film "among the ideological and aesthetic low-points of the species". 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 
 