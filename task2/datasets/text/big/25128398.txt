Checkpoint (2003 film)
 
{{Infobox film
| name           = Checkpoint
| director       = Yoav Shamir
| producer       = Amit Breuer Edna Kowarsky Elinor Kowarsky
| editing        = Era Lapid
| cinematography = Yoav Shamir
| released       =  
| runtime        = 80 minutes
| country        = Israel
| language       = Hebrew, Arabic, English
}}
 Palestinian civilians at several of the regions Israel Defence Forces checkpoints. The film won five awards at various film festivals, including Best International Documentary at the Hot Docs Canadian International Documentary Festival, best feature-length documentary at the International Documentary Film Festival Amsterdam and the Golden Gate Award for Documentary Feature at the San Francisco International Film Festival. Although the film was generally well received, it was also controversial and reactions from audience members and critics were sometimes very angry.  

==Synopsis==
Checkpoint is shot in cinéma vérité style with no narration and very little context. Shamir himself is absent from the film except for one scene in which a border guard asks him to try to make him "look good," and Shamir  asks how he should do that.
 Hebrew and English language|English.

==Significance==

Checkpoint is a part of the independent digital documentary movement in the early 2000s, thanks to the introduction of relatively inexpensive digital tape-based video cameras and the sudden affordability of powerful desktop video editing systems.   Fahrenheit -9-11 and  Supersize Me may have been driven by narrative and personal point of view, but their camera style certainly draws on the verite trope of letting reality play out before the camera.  Recalled as The Year of the Documentary,  these films came out in 2004 during  a convergence of new media possibilities and world conflict including 9-11, as well as tensions in the Middle East during the Second Intifada,  according to Paul Falzone in his dissertation,  Documentary of Change.   
“From this conflict emerges a generation of muckraking filmmakers and a new style of documenting filmmaking,” wrote Falzone.  Lumiere Brothers’ invention of the Cinematograph during the turn of the 19th Century.  According to Falzone, a specific kind of documentary was forged by filmmakers looking to challenge  mainstream media’s interpretation of events. She observed that this new batch of films tended to replace the protagonist with antagonist.  
In Checkpoint’s case, the director Shamir replaced the antagonist with the subject, which in turn served as the antagonist. 
But while the director may have omitted narration or any stated point of view, he sticks to the verite technique of hammering home a focused point. Checkpoint follows the looming sense of futility from shutting down access between two groups of people.
In an interview with Documentary Film Quarterly in 2004,  Shamir says, “Everybody is like a victim; the soldiers, the Palestinians. I want to show what effects the occupation has on the Palestinians but even more what the effects are on society.”  

==Awards== festival awards, including Best Feature Documentary at the International Documentary Film Festival Amsterdam, Best International Documentary at the Hot Docs Canadian International Documentary Festival and the Golden Gate Award for Documentary Feature at the San Francisco International Film Festival. 

==References==
 

==Bibliography==
Zanger, Anat. "Blind Space: Roadblock Movies in the Contemporary Film."   24.1 (2005): 37-48. American University Library. Web.

==External links==
* 

 
 
 
 
 
 