A Night at Earl Carroll's
{{Infobox film
| name           = A Night at Earl Carrolls
| image          = A Night at Earl Carrolls poster.jpg
| alt            = 
| caption        = Theatrical release poster Kurt Neumann
| producer       = Earl Carroll
| screenplay     = Lynn Starling 
| starring       = Ken Murray Rose Hobart Elvia Allman Blanche Stewart Earl Carroll J. Carrol Naish Lela Moore
| music          = Victor Young
| cinematography = Leo Tover
| editing        = Alma Macrorie 
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 62 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Kurt Neumann and written by Lynn Starling. The film stars Ken Murray, Rose Hobart, Elvia Allman, Blanche Stewart, Earl Carroll, J. Carrol Naish and Lela Moore.   The film was released on December 6, 1940, by Paramount Pictures.

==Plot==
 

== Cast ==
*Ken Murray as Barney Nelson
*Rose Hobart as Ramona Lisa
*Elvia Allman as Cobina Gusher
*Blanche Stewart as Brenda Gusher
*Earl Carroll as himself
*J. Carrol Naish as Steve Kalkus
*Lela Moore as herself
*Jack Norton as Alonzo Smith
*Russell Hicks as Mayor Jones of Hollywood 
*William B. Davidson as Mayor Green of San Bernadino John Harmon as Mac
*Forbes Murray as Mayor Brown of Pasadena
*Ralph Emerson as Mayor Gray of Bakersfield
*Ray Walker as Jerry
*Allan Cavan as Mayor White of El Centro
*George McKay as Mayor Stokes of San Diego Truman Bradley as Radio Announcer
*Beryl Wallace as Miss DuBarry
*Ruth Rogers as Miss DeMilo
*Sheila Ryan as Miss Borgia
*John Laing as Vic 
*Mary Lou Cook as The Hot Singer
*Florine McKinney as Girl Orchestra Leader

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 

 