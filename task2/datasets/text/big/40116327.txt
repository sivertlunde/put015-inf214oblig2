Yeti: A Love Story
 
{{Infobox film 
| name           = Yeti: A Love Story
| image          = 
| caption        = 
| director       = Adam Deyoe & Eric Gosselin
| producer       = 
| writer         = Adam Deyoe, Eric Gosselin, Jim Martin, and Moses Roth
| starring       = Adam Deyoe, Eric Gosselin, Adam Malamut, Brie Bouslaugh, Laura Glascott, David Paige
| music          = 
| cinematography = 
| editing        = 
| distributor    = Troma Entertainment 
| released       = 2006
| runtime        = 70 minutes
| country        = United States
| language       = English
| tagline           = Its Brokeback Mountain meets King Kong!
}}

Yeti: A Love Story is a 2006 comedy horror movie directed by Adam Deyoe and Eric Gosselin, and released by Troma Entertainment. A sequel was announced via Kickstarter on February 11, 2014. 

== Plot ==

The movie is set in   that worships a yeti that lives in the woods. The yeti was captured in the Himalayas and brought to the town as an old man′s sideshow, but had escaped, and is reportedly the last of its kind. Each night, Debra, one of its members, lures young men to the cult with the intention of offering it up to the yeti as a means of keeping it sexually sated. The movie opens up with such an example, with Debra and Raymond, the cult′s leader, looking on while laughing sadistically.

Five college students—fraternity brothers Adam and Dick, their girlfriends Sally and Emily, and a fifth member named Joe—are coming to Quatssack on a camping trip, unaware of the town′s secrets. On their first night, Joe is killed while going to the bathroom in the woods, and since he was the one who had the car keys, the remaining four cannot leave town. The old man, who now owns a hideous-looking creature called “Tentacle Boy” and displays it as a sideshow, informs them of the yeti that he used to own now living somewhere in the woods.

Emily goes into a nearby church to pray for Joe′s safety, and while she is in there, she is discovered by a priest as a “Chosen One” that was prophesied to take down the Children of the Yeti. She accepts her destiny, and the priest gives her supplies for her mission.

Meanwhile, Adam, Dick, and Sally, who are waiting for Emily outside, are ambushed by a redneck demanding the whereabouts of the Chosen One. Emily emerges from the church and shoots the redneck with a crossbow, and despite his seemingly near-fatal wound, demands that the redneck take them to the cult′s location. Only Adam and Emily follow the redneck; Sally is sent back to their campsite, while Dick had left earlier, having met Debra.

As the sun goes down, the redneck brings Adam and Emily to the place where the cult reside before leaving. They spot Dick, but not as a sacrifice for the yeti; he′s standing off to the side with Debra, watching on as another young man is offered up to the yeti instead. The yeti appears and brutally rapes the young man, and once he′s finished, he makes off with Adam. Emily cannot kill the yeti because of this. Raymond, who had been watching this, is concerned about the yeti′s recent actions, but assures himself that he will return tomorrow night. Dick, still disturbed by the ritual, is assured by Debra that he′s not going to be a sacrifice, and that despite what he′d witnessed, the yeti is very gentle and will not hurt Adam.

Adam, meanwhile, is taken deep into the woods by the yeti. Adam becomes attracted to the yeti and the two proceed to have sexual intercourse. Not only do the yeti and Adam become lovers, but Sally and Emily, who have had their boyfriends run off, become a couple, while Dick and Debra become smitten with each other as well.

In spite of the new romances, the cult is still causing trouble. The priest that sent Emily on her mission reveals to her that Raymond was once a disciple of his. While traveling through Nepal, the two had come across a monk who taught them about the legend of the yeti, who could be summoned by a magic gong, which had been on display in a museum in Hong Kong. A special scroll called “The Book of the Yeti” was entrusted into the care of Raymond and the priest, but Raymond had became obsessed with the legend. He and the priest then stole the gong and found the yeti in a sideshow (the old man’s sideshow). Raymond used the gong to free the yeti and, with the gong, the scroll, and control over the yeti, went on to found the cult.
 finger them. A fight breaks out that results in Sally’s death, though Sex Piss doesn’t die despite multiple wounds. Emily has to flee and find Adam and the yeti. Back at the church, the priest is confronted and killed by Raymond.

Back with the cult, Raymond confides with Debra that the gong isn’t working on the yeti at all and he is worried that he is losing control over the beast. He also notes that Debra has been acting different since she’s found Dick. Raymond orders Debra to find and place the yeti under their control once again, or face death. She manages to find Adam and the yeti, but they are in a moment of intimacy when she finds them. She goes back to Raymond and lies to him, claiming that the yeti is still under his control.

Debra then betrays Dick to the cult and he becomes the evening’s intended sacrifice for the yeti. The gong is rung, the yeti appears, but instead of raping Dick, the yeti releases him. Adam and Emily show up, as do the old man and Tentacle Boy, who are in disguise as cult members and are here to reclaim the yeti. In the fight that ensues, several are killed, among them Raymond, Debra (killed by Dick), Tentacle Boy, and the yeti. Adam mourns the loss of his lover. Emily and Dick mourn their respective lovers as well.

So it seems that with Adam’s yeti dead, the whole yeti species are extinct for good…or are they? Adam drops a hint that he is now pregnant with the yeti’s child.

== Cast ==

*Adam Malamut as Adam, a sexually repressed fraternity boy.
*Dave Paige as Dick, Adam′s fraternity brother, whom Adam accuses of being gay.
*Lauren Glascott as Emily, destined to take down the Children of the Yeti.
*Loren Mash as Sally, Adam′s former girlfriend.
*Joe Mande as Joe, treated horribly by Adam and later killed in the forest by a hunter.
*Curt Croner as the Yeti, Adams lover. He was captured by an old man in the Himalayas and put on display, but he escaped and becomes worshiped by the Children of the Yeti. 
**Croner also appeared as a police officer.
*Mark Wahlberg as a priest who recognizes Emily as the “Chosen One” destined to take down the evil cult. Raymond was once a follower of his.
*Eric Gosselin as Sex Piss, a Casanova who has difficult luck with Sally and Emily. Sex Piss later appears in Psycho Sleepover, as an inmate. 
**Gosselin also appeared as a Cult member.
*Leo Boivin as Raymond, the leader of the Children of the Yeti. 
*Adam Deyoe as an Old Man, the Yeti′s previous owner.
*Adam Balevet as Tentacle Boy, a sideshow owned by the Old Man.
*Brie Bouslaugh as Debra, a member of the Children of the Yeti. Her goal is to lure as many young men to the cult, where they are captured and offered to the Yeti as sacrifices. Dick falls in love with her.

== Production ==

The film was shot on a budget of $200, over a five-day period.

== Reception ==

This movie currently has 4.4 out of 10 stars on IMDB, and a score of 42% on Rotten Tomatoes.

== Sequel ==

A sequel was announced in February 2014, called Yeti: A Love Story 2: Life on the Streets. According to writer Jim Martin, the movie is set years after the first movie, in a gay underground nightclub in Los Angeles. Adam works there to help support his human/yeti child and his wheelchair-bound boyfriend, when he meets and falls in love with a yeti prostitute. The characters “Dick” and “Sex Piss” are due to appear.

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 