Empire of Dirt (film)
 
 
{{Infobox film
| name           = Empire of Dirt
| image          = Empire of Dirt (film).jpg
| caption        = Film poster
| director       = Peter Stebbings
| producer       = 
| writer         = Shannon Masters
| starring       =  
| music          = 
| cinematography = David Greene
| editing        = 
| distributor    = 
| released       =  
| runtime        = 99 minutes
| country        = Canada
| language       = English
| budget         = 
}}

Empire of Dirt is a 2013 Canadian drama film directed by Peter Stebbings. It was screened in the Contemporary World Cinema section at the 2013 Toronto International Film Festival.      
 Best Actress Best Supporting Best Original Best Editing (Jorge Weisz).

==Plot==
The film stars Cara Gee as Lena, a young single First Nations mother struggling to bridge the generation gap with her daughter Peeka (Shay Eyre) and her mother Minerva (Jennifer Podemski).

==Production==
The film was shot from 19 September to 4 October 2012 at  , 30 December 2012. 

It was written by Shannon Masters.  Producers included Colin Brunton (line producer), Bob Crowe and Jennifer Podemski (as producers), Heather K. Dahlstrom and Geoff Ewart (as co-producers), and Avi Federgreen (as executive producer). 

==Cast==
* Cara Gee as Lena 
* Shay Eyre as Peeka 
* Jennifer Podemski as Minerva "Minnie"
* Luke Kirby as Russell
* Jordan Prentice as Warren
* Michael Cram as Doc Baker
* Sarah Podemski as Charmaine
* Lawrence Bayne as Hank
* Kate Corbett as Wendy
* Tonya Lee Williams as Sandra
* Shannon Kook as Angel

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 