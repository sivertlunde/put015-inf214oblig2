Crows and Sparrows
{{Infobox film
| name           = Crows and Sparrows
| image          = Crows and Sparrows poster.jpg
| caption        = Chinese poster for Crows and Sparrows (1949)
| director       = Zheng Junli
| producer       = Xia Yunhu Ren Zongde
| writer         =  Wu Yin
| music          = Wang Yunjie
| studio         = Kunlun Film Company
| released       =  
| runtime        = 111 minutes
| language       = Mandarin
| country        = China
| budget         = 
| film name = {{Film name| jianti         = 乌鸦与麻雀
 | fanti          = 烏鴉與麻雀
 | pinyin         = Wūyā yŭ máquè}}
}} Communist victory Nationalist bureaucrats, the film was made as Chiang Kai-sheks Nanjing-based government was on the verge of collapse, and was not actually released until after the Chinese Civil War had ended.

The film starred Zhao Dan in a leading role.

==Plot==

At a Shanghai apartment, Mr Hou, a Nationalist official, gets ready to move to Taiwan upon the imminent defeat of the KMT during the Civil War.  Mrs Hou gives an ultimatum to the rest of the tenants to move out on behalf of her husband, who is the "owner" of the flat and who is now planning to sell it.  From the conversations with the rest, we find out that Hou has been a Hanjian during the Second Sino-Japanese War|Sino-Japanese War and that he has since taken over the apartment by force from the old landlord, Mr Kong.
 Wu Yin), Little Broadcast (Mr Xiao, played by Zhao Dan) and a schoolteacher, Mr Hua, and his wife, initially plan to band together, but circumstances force them to find other ways out.  Mr Hua tries to find a place to stay at the KMT-sponsored school he is teaching in.  Little Broadcast and Mrs Xiao invest in black market gold.  As the situation escalates, Mr Hua gets arrested by KMT agents and his young daughter falls desperately ill.

Hou makes his advances towards Mrs Hua and is rejected. Little Broadcast and Mrs Xiao find their scheme of making rich go to nought after gold price rises sharply.
 long distance telephone call from Nanjing telling him to retreat to Taiwan as the KMT capital is to be abandoned.  The KMT agents release Mr Hua while his fellow teachers are executed.  As the remaining KMT members all run off from Mainland China, the tenants celebrate Chinese New Year in 1949, promising to improve themselves in face of the coming new society.

==External links==
*  from the Chinese Movie Database
*  
*  from Ohio State University

 
 
 
 
 
 
 