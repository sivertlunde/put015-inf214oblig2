Bert Rigby, You're a Fool
{{Infobox film
| name           = Bert Rigby, Youre a Fool
| image size     =
| image	         = Bert Rigby, Youre a Fool FilmPoster.jpeg
| caption        =
| director       = Carl Reiner
| producer       = George Shapiro
| writer         = Carl Reiner
| narrator       = Robert Lindsay Robbie Coltrane Bruno Kirby Corbin Bernsen Anne Bancroft
| music          = Ralph Burns
| cinematography = Jan de Bont
| editing        = Bud Molin
| studio         = Lorimar
| distributor    = Warner Bros.
| released       = February 24, 1989
| runtime        = 94 minutes
| country        = USA
| language       = English
| budget         = $7 million
| gross          = $75,868
| preceded by    =
| followed by    =
}} Robert Lindsay in the title role.

==Plot==
Bert Rigby is a miner in a small dying town of Langmore in northern England, with aspirations to show business. He tells the story in flashback, while sitting in a bar. He lives with his mother, a musical fan, and next door to his sweetheart, Laurel Pennington. She lives above the pub where she works, and they have a bomb shelter straddling their back yards where they have secret meetings. While his fellows are on strike once again, Bert decides to try his luck in show-biz. He gets his chance when he performs in an amateur show, singing "Isnt It Romantic?", and his first appearance on stage goes all wrong, when his nose starts bleeding after an injury sustained playing football - but the audience loves him anyway. So he starts as a comedian in a traveling amateur show for $50 a night, touring around the country with his manager, Sid Trample, and Sids wife Tess. Bert repeats the act he did in his first appearance, until he tires of it and starts doing a Buster Keaton imitation. During the tour they come across a crew filming a contraceptives commercial.

One day Bert gets an offer from an ad director from Hollywood and flies to America with Sid, expecting a great career ... and again leaving behind his pregnant young wife. Bert is calling Laurel when he is about to leave, and when he has to break off, he swears when he discovers his bags are missing, leaving Laurel with a misunderstanding. In Hollywood they film a commercial with him playing Buster Keaton, and directed by Kyle DeForest, the same director he had seen filming the contraceptives commercial, but the ad is dropped when a demographic survey reveals that most of the target audience had never heard of Keaton. Bert phones Laurel to apologize, but he swears again when he slips on the wet bathroom floor, causing yet more misunderstanding. Bert then discovers that Sid has left him high and dry, and stranded in America, they part ways.
 Dream a Little Dream of Me." The Perlesteins have a dinner party at their house, with Bert acting as the servant. The party becomes a disaster when the curtain hiding a priceless masterpiece is set on fire. The flashback ends, and the bartender tells Bert that the person he has been telling his story to does not speak English. Bert then dances in the bar, which catches the attention of an ad producer. Bert eventually returns to England in triumph, with a showing of his song-and-dance Crown Royal commercial in the town theatre. The commercial is followed by Bert doing a rendition of "Puttin on the Ritz."

== Reception ==

Janet Maslin gave the film a mediocre review in The New York Times, saying "the film that Carl Reiner has fashioned around Mr. Lindsays music-loving mineworker, is extremely odd. Its pace isnt quick. Its point isnt clear. Its satire is so gentle that it hardly has a target. Its only guiding sentiment is a loose, soft-hearted affection for a certain kind of show-business magic."  Anne Bancroft earned a Golden Raspberry Award nomination for Worst Supporting Actress for her performance in the film.

==Cast==
*Robert Lindsay - Bert Rigby
*Robbie Coltrane - Sid Temple
*Cathryn Bradshaw - Laurel Pennington
*Anne Bancroft - Meredith Perlestein
*Corbin Bernsen - Jim Shirley
*Bruno Kirby - Kyle DeForest
*Jackie Gayle - I.I. Perlestein
*Carmen du Sautoy - Tess Temple Liz Smith - Mrs. Rigby Mike Grady - Mick OGrady

== References ==
 

==External links==
* 
* 

 

 
 
 
 
 
 