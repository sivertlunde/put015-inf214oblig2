Enter the Void
 
 
{{Infobox film
| name = Enter the Void
| image = Enter-the-void-poster.png
| alt = 
| caption = Theatrical release poster
| director = Gaspar Noé
| producer = {{Plainlist|
* Brahim Chioua
* Vincent Maraval
* Olivier Delbosc
* Marc Missonnier}}
| writer = Gaspar Noé
| starring = {{Plainlist|
* Nathaniel Brown
* Paz de la Huerta
* Cyril Roy
* Ed Spear}}
| cinematography = Benoît Debie
| editing = {{Plainlist|
* Gaspar Noé
* Marc Boucrot
* Jérôme Pesnel}}
| production companies = {{Plainlist|
* Fidélité Films Wild Bunch}}
| distributor = Wild Bunch Distribution
| released =  
| runtime = {{Plainlist|
* 161 minutes    
* 143 minutes    }}
| country = {{Plainlist|
* France
* Canada
* Germany
* Italy}}
| language = {{Plainlist|
* English
* Japanese}}
| budget = Euro|€12.38 million
| gross = United States dollar|$754,249 
}} fantasy drama film written and directed by Gaspar Noé and starring Nathaniel Brown, Paz de la Huerta, and Cyril Roy. Set in the neon-lit nightclub environments of Tokyo, the story follows Oscar, a young American drug dealer who gets shot by the police, but continues to watch subsequent events during an out-of-body experience. The film is shot from a First-person narrative|first-person viewpoint, which often floats above the city streets, and occasionally features Oscar staring over his own shoulder as he recalls moments from his past. Noé labels the film as a "psychedelic melodrama". 
 Wild Bunch, while Fidélité Films led the actual production. The cast is a mix of professionals and newcomers. The film makes heavy use of imagery inspired by experimental cinema and psychedelic drug experiences. Principal photography took place on location in Tokyo, and involved many complicated crane shots. Co-producers included the visual effects studio BUF Compagnie, which also provided the computer-generated imagery. The films soundtrack is a collage of electronic pop and experimental music.

A rough cut premiered at the 2009 Cannes Film Festival, but post-production work continued, and the film was not released in France until almost a year later. A cut-down version was released in the United States and United Kingdom in September 2010. The critical response was sharply divided: positive reviews described the film as captivating and innovative, while negative critics called it tedious and puerile. The film performed poorly at the box office.

==Plot== The Tibetan Book of the Dead, a Buddhist book about the afterlife. The first segment begins with Linda leaving for work (at a local strip club) and then follows Oscars nightly routine through strict point-of-view shots, including momentary blackouts that represent blinking, private internal thoughts, and extended sequences of a drug-induced hallucination.

Next, Alex meets Oscar at the apartment and they leave so that Oscar can deliver drugs to his friend Victor (Olly Alexander). On the way, Alex explains parts of The Tibetan Book of the Dead to Oscar: how the spirit of a dead person sometimes stays among the living until it begins to experience nightmares, after which it attempts to reincarnate. They arrive at a bar called The Void. Oscar enters alone and sits down with a distressed Victor, who mutters "Im sorry" before they are swarmed by police officers. Oscar seals himself in a bathroom stall and attempts to flush his drugs. When the flush does not work, he yells through the door that he has a gun and will shoot. In response, a police officer opens fire and hits Oscar, who falls to the floor.

Oscars viewpoint rises and looks at his body from above, and then we begin to witness his life in a roughly chronological order. His loving parents were killed in a violent car crash; Oscar and Linda, devoted to each other, were sent to different foster homes; Oscar moved to Tokyo and earned money through drug dealing until he could afford to bring Linda to live with him; Linda found work as a stripper for the nightclub owner Mario, to Oscars distress; Oscar increased the scope of his dealing operations and started using potent Psychedelic drug|psychedelics—in particular, Dimethyltryptamine|DMT—more frequently; Victor discovered that Oscar slept with Victors mother; and finally, we again see Oscar meet Victor at The Void to sell him drugs, only to be shot in the bathroom.

Afterwards, a disembodied Oscar floats over Tokyo and witnesses the aftermath of his death. Linda becomes withdrawn and despondent, especially after getting an abortion; Oscars dealer, Bruno, destroys his stash; Alex lives in hiding on the streets; and Linda wishes she would have been with Alex instead of Mario, as Oscar had wanted. On one occasion Linda wishes that Oscar would come back to life; Oscar then enters Lindas head, after which he wakes up at the morgue, from which his body is taken to be cremated.

Meanwhile, Victor and his mother scream at each other because she had sex with Oscar, and because Victor informed the police about Oscars drug dealing; Victor is then thrown out of his parents home. He shows up at Lindas apartment and apologizes for having her brother killed, but says Linda is partially to blame since she hung around with creeps. This angers Linda, who repeatedly screams that Victor should kill himself.

The perspective now hovers high above Tokyo and enters an airplane, where Oscars mother breast-feeds a baby to whom she whispers Oscars name. The view then drops to Linda and Alex, who take a taxi to a Tokyo love hotel and have sex. The perspective moves among hotel rooms and observes several other couples having sex in various positions. Each couple emanates a pulsating electric-like glow from their genitals. Oscar enters Alexs head and witnesses the sex with Linda from Alexs point of view. He then travels inside Lindas vagina to witness Alexs thrusting, then observes his ejaculation and follows the semen into the fertilization of his sisters ovum. The final scene is shot from the perspective of a baby being born to Oscars mother. According to the director, this is a flashback to Oscars birth in the form of a false memory. 

==Themes== starts over again in an endless loop, due to the human brains perception of time. 

==Production==
===Development=== Robert Montgomerys Lady in the Lake, a 1947 film shot entirely in a first-person perspective. He then decided that if he ever made a film about the afterlife, that was the way it would be filmed. Noé had been working on different versions of the screenplay for fifteen years before the film went into production. The story had initially been more linear, and the drafts were set in different locations, including the Andes, France, and New York City.    Tokyo was chosen because it could provide colourful environments required for the films hallucinogenic aspects, and because Japans repressive drug laws add to the drama, explaining the intensity of the main characters fear of the police.   
 Wild Bunch. When the producers at Wild Bunch asked Noé what he wanted to do next, he answered Enter the Void. The project was once again considered too expensive in relation to its commercial potential, but when Wild Bunch discovered that Noé had started to develop the film for Pathé instead of them, they said they were willing to fund it. Since development went slowly at Pathé, Noé chose to not renew his contract with the studio and accepted Wild Bunchs offer. 

Enter the Void was produced under Fidélité Films, with 70% of the budget invested by Wild Bunch. French co-producers included Noés company Les Cinémas de la Zone and the visual effects studio BUF Compagnie. It received pre-sales investment from Canal+ and funding from Eurimages. Additional co-production support was provided by Essential Filmproduktion of Germany and BIM Distribuzione of Italy.       The total budget was 12.38 million euro.  In retrospect Noé called Irréversible a bank robbery, a film made in order to finance Enter the Void. He also saw it as a helpful technical exercise. 

===Casting===
{| class="infobox"
|- Actor
! style="width:7%;"| Role
|-
| Nathaniel Brown
|
| Oscar
|-
| Paz de la Huerta
|
| Linda
|-
| Cyril Roy
|
| Alex
|-
| Emily Alyn Lind
|
| little Linda
|-
| Jesse Kuhn
|
| little Oscar
|-
| Olly Alexander
|
| Victor
|-
| Ed Spear
|
| Bruno
|-
| Masato Tanno
|
| Mario
|}

The decision to use English-speaking actors was made early. Since the film would be very visual, the director wanted audiences to be able to focus on the images, and not have to rely on subtitles. He later expressed his approval of the use of dubbed voice tracks in non-English speaking countries. 

The role of Linda was the first to be cast. Noé found Paz de la Huerta after holding auditions in New York City.  "I met Paz and I really liked her. She had the profile for the character because she likes screaming, crying, showing herself naked—all the qualities for it."    Due to a desire that Linda and Oscar should be believable as siblings, Nathaniel Brown, a non-professional, was cast because of his resemblance to Huerta. Noé feared that a professional actor would be frustrated by being shown almost exclusively from behind, but he felt that Brown, an aspiring director, would find it stimulating to merely be present on the set.  Auditions were held for westerners living in Japan for other Tokyo-based roles. Cyril Roy went to an audition with a friend only because he wanted to talk with the director, whose previous films he admired. Roy was cast as Alex, since Noé found his talkative personality suitable for the role. Noé said about Brown and Roy: "The thought of acting in a film had never even entered their minds. Theyre easy-going people, they have a good time in front of the camera and I dont think there was a single moment where either of them felt they were working. Paz, however, was definitely conscious of the fact that she was interpreting a role." 

===Visual conception===
 , Art Forms of Nature]]
Noé had tried various hallucinogens in his youth, and used those experiences as inspiration for the visual style. Later, when the director was already planning the film, he tried the psychoactive brew ayahuasca, in which the active substance is dimethyltryptamine (DMT). This was done in the Peruvian jungle, where the brew is legal due to its traditional use as an entheogen.  Noé described the experience as very intense, and said he regarded it "almost like professional research."  Since few on the design team had ever taken a hallucinogen, it was necessary for Noé to collect and provide visual references in the forms of paintings, photographs, music videos, and excerpts from films.  One reference used was the works of biologist Ernst Haeckel, whose drawings influenced the organic patterns seen during Oscars visions.   

{|class="toccolours" style="float: left; margin-right: 1em; font-size: 85%; color:black; width:20em; max-width: 35%;" cellspacing="5"
|style="text-align: left;"|"There is nothing radically new, I took some techniques or narrative modes that were already in place from right to left in films, but used them in an obsessive manner."
|- Gaspar Noé interviewed in Libération 
|} Snake Eyes and other films which feature hovering overhead shots inspired Noé to make a film largely from such a perspective. 

There were two reasons for showing Oscars head and shoulders within the frame during the flashback scenes, rather than letting the camera be the characters eyes. The first was that this is the way Noé usually sees himself in dreams and when recalling past events. He also thought it would be easier for the viewer to care about a character who is visible, as many point-of-view films, in his opinion, look unintentionally funny. 

===Filming===
The crew filmed in Tokyo from 19 October to 15 December 2007. Flashback scenes were shot in Montreal over the course of four weeks the following spring, until 16 May 2008.   The team went back to Tokyo twice for additional footage, once before the Montreal session and once when principal photography was complete.  Only four persons on the Tokyo set were French; the rest of the crew was Japanese.    Marc Caro worked as the supervisor of set designs in Tokyo. According to Noé, Caro had three months free after finishing Dante 01, his first effort as a solo director, so Noé asked him to come to Japan. 

  Toho Studios.  More scenes than originally planned had to be filmed in the studio because of the many complicated crane arrangements. Some of the overhead sequences took a full day to arrange and film.    The scenes where Oscar is alive were mostly shot on location, but the crane shots were exclusively taken in the studio; this included revisits to some of the previous locations, which were replicated as large indoor sets.  Other shots were taken from helicopters flying over the city.  Much attention was paid to the continuity of the geography, and filming was overseen by a supervisor from the visual-effects team. 
 Kodak Vision3 super 16 programmable disco lights, which allowed for all different hues. The disco lights were easy to hide. They were also used for simulation of neon flashes, and to add a tint of red to the dressing-room scenes. Another exception was the use of strobe lights, which were programmed together with the coloured lights. Blue colour was avoided throughout, since the filmmakers did not associate it with dreams.  Noé was the films camera operator, except for a few shots of Oscar running in the streets, as they required a taller cameraman. In those scenes the camera was held by Debie.   

===Post-production===
Enter the Voids post-production process lasted more than a year.  Work on the digital effects was led by Pierre Buffin of BUF Compagnie. Every scene in the film includes computer-generated imagery (CGI)—even the flashback scenes, where the backdrops were digitally altered.  Studio scenes, helicopter shots, and CGI were forged together in the hovering sequences with the intention that the viewer should be unable to determine which is which.  For shots from high altitudes, the team started with helicopter footage from video, and then created computer models of the neighbourhoods with textures from photographs. Neon lights, reflections, and dark areas were consistently accentuated. Flickers were created through a mixture of motion blur, chromatic aberration, and focus effects. For scenes seen as through a fisheye lens, the team recreated the sets digitally and progressively increased the environments reflection values along with the lens effect. 
 Coil is heard during Oscars first DMT trip. The Throbbing Gristle song "Hamburger Lady" plays as Oscar tries to deliver drugs to Victor at the bar. The soundtrack notably includes excerpts from nearly every part of Jean-Claude Éloys two compositions, Shânti and Gaku-no-Michi. Other songs on the soundtrack include Toshiya Tsunodas "Music for Baby", Alvin Luciers "Music for Gamelan Instruments, Microphones, Amplifiers and Loudspeakers", and works by Denis Smalley, Lullatone, and Zbigniew Karkowski. 

When the film premiered at film festivals, it was initially shown in a version without any credits. As several people at the screenings complained about the length of the film, Noé decided that if the final version would have any opening titles, they would have to be "as fast as possible and as graphic as possible".  The German experimental filmmaker Thorsten Fleisch was hired to create the title logo. Noé discovered Fleisch through his 2007 film Energie!, for which the technique of animated sparks had been developed. 

==Release==
  Stockholm international 2010 Sundance Film Festival.  At the Cannes premiere, the film had alternatively been listed with the French title Soudain le vide, which means "Suddenly the void".    When it was released in French cinemas, it used the English title. It premiered in France on 5 May 2010 through Wild Bunch Distribution.  The Japanese release followed ten days later. 

Distribution rights for the United States were picked up at Sundance by IFC Films.  Trinity Filmed Entertainment was the British distributor.    The film was released in the United States and the United Kingdom on 24 September 2010.   In both these countries, the film was distributed without the seventh of its nine reels. The running time was therefore 137 minutes at 25 frames per second, which the director had instructed that the film should be played at, or 142 minutes at the more common 24 frames per second.   Noé says that none of the cut material is essential for the film. He describes it as "some astro-visions, an orgy scene with Linda and the Japanese girl, the scene where you see   waking up at the morgue and he thinks hes alive but hes not, and then the camera goes down the plughole where shes tipping his ashes."    The reason the shorter version was made was that Noé had promised the investors to make an alternative edit if the film ended up being longer than two hours and 20 minutes.    The film was released on DVD and Blu-ray Disc in France on 1 December 2010. Each edition features both the complete version and the shorter cut.  

==Reception==
===Critical response=== Soporific cinema." 

Upon the Japanese release, the critic writing for   is the film youd make when all you know about Japan are the pampered press junkets at  ) takes Tokyos love of neon gaudiness to a surreal extreme".   

As of 25 January 2012, the film had an aggregated approval of 72% from 83 English-language reviews collected at  . Male called it "technically stunning", but also "dreadfully acted, tediously profound and painfully overlong", and accused the director of misogyny.    thought the film was successful as an "attempt to transport moviegoers to a hallucinatory version of the hereafter unlike anything theyve ever witnessed on film", but, "The problem is that its also the most excruciating sit in recent cinematic memory. And no, the fact that its intentionally excruciating doesnt make it less excruciating."  The film was included in The Guardians top 50 films of the decade so far list by critic Peter Bradshaw.   

===Box office===
The film was a financial failure; according to Wild Bunch in February 2011, the film had returned 1.25% of the investment.  In France, it was launched on 30 prints and sold 51,126 tickets in total.  Producer Brahim Chioua said the film had been difficult to sell abroad for a reasonable price due to the late-2000s financial crisis.  As of 20 July 2011, Box Office Mojo reported that the worldwide theatrical revenues corresponded to United States dollar|US$754,249.   

===Accolades===
Enter the Void won the Special Jury Award and the prize for Best Cinematography at the 2009 Sitges Film Festival.  It received the main award for best film at the 2010 Neuchâtel International Fantastic Film Festival|Neuchâtel Film Festival.  This especially delighted Noé, since one of the jury members in Neuchâtel was Douglas Trumbull, the special effects supervisor of 2001: A Space Odyssey. 

==See also==
 
;Cinematic context
* Cinema of France
* Oneiric (film theory)
 
;Lists
* List of drug films
* List of films featuring hallucinogens
* List of films set in Japan
* List of ghost films
 
;Thematically related
* Near-death experience
* Recreational drug use
 

==References==
 

==External links==
*   (UK)
*   (US)
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 