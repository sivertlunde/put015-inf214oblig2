The Neighbor (2012 film)
{{Infobox film
| name           = The Neighbor
| image          = File:The_Neighbors(2012)_poster.jpg
| director       = Kim Hwi
| producer       = Gu Seong-mok   Park Jeong-hyeok   Lee Yong-ho
| writer         = Kim Hwi
| based on       =  
| starring       = Kim Yunjin   Kim Sae-ron   Chun Ho-jin   Kim Sung-kyun
| music          = Shin Yi-gyeong
| cinematography = Jung Seong-wook
| editing        =
| distributor    = Lotte Entertainment
| released       =  
| runtime        = 110 minutes
| country        = South Korea
| language       = Korean
| budget         =
| admissions     = 2,434,099
| gross          =  
}}
The Neighbor ( ; also known as My Neighbor) is a 2012 South Korean suspense thriller starring Kim Yunjin in the lead role. 

Residents of a small apartment building learn that a serial killer lives within their building. Kyung-hee feels heavy guilt for not being able to prevent her stepdaughter’s murder, so she takes it upon herself to find the killer. Meanwhile, a young girl that resembles the murdered girl becomes the next target of the serial killer. 

Based on Kang Fulls eponymous webcomic|web-based comic book series (referred to as "webtoon" in Korea),   the film sold over 2.43 million tickets, becoming the highest grossing film among the movie adaptations of Kang Full’s works. 

==Plot==
What would you do if you found out someone living in your building is a serial killer? A man, whose identity is known, kills his own neighbors ― including a middle-school girl ― living in his building, and continues to stay there even after committing the grisly murders.

All the neighbors in the film are reluctant to act due to self-interest. One character does not want the property price to fall after a scandal. Another wants to avoid attention from the police, as he has just five months left before his statute of limitations runs out. Some simply do not want to meddle without evidence, clinging to their daily routines. Meanwhile, the criminal continues to kill.  

==Cast==
*Kim Yunjin - Song Kyung-hee, stepmother of dead girl
*Kim Sae-ron - Won Yeo-seon (first victim) / Yoo Soo-yeon
*Chun Ho-jin - Pyo Jong-rok
*Jang Young-nam - Ha Tae-seon
*Im Ha-ryong - Kim Sang-young
*Ma Dong-seok - Ahn Hyuk-mo, ex-con and loan shark
*Kim Sung-kyun - Ryu Seung-hyuk, crew man of fishing vessel
*Do Ji-han - Ahn Sang-yoon
*Kim Jung-tae - Kim Jong-guk, ghost
*Jung In-gi - Kim Hong-jung, Ahn Hyuk-mos uncle
*Kim Ki-cheon - Hwang Jae-yeon, security guard 1
*Cha Hyeon-woo - Detective Lee
*Kwak Min-seok - Ahn Dong-joo, pizza store owner
*Cha Kwang-soo - Won Jung-man, Won Yeo-seons father

==Awards and nominations==
2012 Grand Bell Awards
*Best New Actor - Kim Sung-kyun

2012 Korean Association of Film Critics Awards
*Best New Actor - Kim Sung-kyun
 2012 Blue Dragon Film Awards
*Nomination - Best New Director - Kim Hwi
*Nomination - Best Supporting Actor - Ma Dong-seok
*Nomination - Best Supporting Actress - Jang Young-nam

2012 Busan Film Critics Awards
*Best New Actor - Kim Sung-kyun
 2013 Baeksang Arts Awards
*Best Supporting Actor - Ma Dong-seok
*Nomination - Best New Director - Kim Hwi

2013 Buil Film Awards
*Nomination - Best Supporting Actor - Ma Dong-seok

==References==
 

== External links ==
*    
*  
*  
*   Daum  

 
 
 
 
 
 
 
 
 
 
 