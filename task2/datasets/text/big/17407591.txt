Penny Wisdom
{{Infobox film
| name           = Penny Wisdom
| image          = 
| caption        =  David Miller Pete Smith
| writer         = Robert Lees Frederic I. Rinaldo
| starring       = Prudence Penny
| music          = 
| cinematography = William V. Skall
| editing        = 
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 10 minutes
| country        = United States 
| language       = English
| budget         = 
}}
 short comedy David Miller Pete Smith. 1938 for Best Short Subject (Color).       This could be considered a remake of the MGM short film Menu (film)|Menu (1933), also produced by Pete Smith.

==Plot==
The opening of the film uses the music of "Pop Goes the Weasel", which already indicates that this "epicurian epic" will entail much comedic content.

The scene starts as Matthew E. Smudge calls his wife, Chloe, to inform her that hes bringing his boss and a customer home for dinner. Unstressed, Chloe enters the kitchen, expecting to tell the cook there will be two more for dinner; she finds a note. Apparently, her constant high-maintenance demands have caused "her culinary queen to quit." Chloe haplessly attempts to fix dinner herself.
 it aint a fit night for man nor beast."), and ultimately turned the kitchen into a complete disaster. Pete Smith, as narrator, asks sobbing Chloe the whereabouts of a telephone. He decides to make a personal call to Prudence Penny, advice columnist for the Los Angeles Examiner. With 35 minutes before the husband and company arrives, Penny shows doubtful Chloe how to prepare a full course, mouth-watering meal with what is left in the icebox as well as applying unusual housewife remedies to salvage some of Chloes cooking.

The meal is prepared just in time for the arrival of Mr. Smudge, boss and customer. Chloe greets the guests as Smith whispers to Smudge, "Psst, your cook left this morning." Smudges countenance drastically changes and is now in a dither about dinner; he knows how Chloe cooks.

Smudge is surprised by the quality and taste of the courses Chloe has presented to him and his guests. Smith interjects that the entire course only cost Smudge a grand total of $2.83. As Penny secretly sneaks away, Smith also lies to Smudge saying Chloe cooked the entire meal herself. Of course, Chloe emphatically nods in agreement, much to her dogs disbelief.

==Cast==
* Prudence Penny - Herself (appears courtesy of Los Angeles Examiner)
* Harold Minjir - Matthew E. Smudge (uncredited)
* Gertrude Short - Chloe Smudge (uncredited) Pete Smith - Narrator (voice) (uncredited) William Worthington - Dinner Guest (uncredited)

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 