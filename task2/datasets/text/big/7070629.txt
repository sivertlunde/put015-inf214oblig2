The Opry House
{{Infobox Hollywood cartoon
| image =
| image_size =
| caption =
| director = Walt Disney, Ub Iwerks
| producer = Walt Disney
| story_artist =
| narrator =
| recorded with = Cinephone
| musician = Carl Stalling
| animator = Ub Iwerks
| layout_artist =
| background_artist = Walt Disney Celebrity Productions
| distributor = Columbia Pictures
| release_date = March 28, 1929
| color_process =
| runtime = 7:25 min.
| country = United States
| language = English
| preceded_by = Plane Crazy When the Cats Away
}}
The Opry House (1929 in film|1929), first released on March 28, 1929, was the fifth Mickey Mouse short to be released, the second of that year. It cast Mickey as the owner of a small theater (or opera house according to the title). Mickey performs a vaudeville show all by himself. Acts include his impersonation of a snake charmer, his dressing in drag and performing a belly dance, his caricature of a Hasidic Jew and, for the finale, a piano performance.

==Synopsis==
The cartoon starts with the opening of a theater and Mickey Mouse sweeping and using the broom as an instrument and a dance partner. Mickey is then faced with a large show goer, who must be deflated in order to fit through the doorway. The band takes over, with a large variety of short gags occurring throughout. Following, Mickey Mouse becomes the star of the show, taking on the multiple roles of a vaudeville star. The cartoon ends with a humorous fight between himself and a piano and a stool.  Mickeys interactions are highly stylized in order to capture the essence of what a vaudeville performance should be. Barrier, Michael. Hollywood Cartoons: American Animation in Its Golden Age. OUP Oxford, 2003 pg. 59. 

==Production== Pat Powerss cinephone system.  It was animated mostly by Ub Iwerks, Walt Disneys first employee who later became known as a “Disney Legend”.  The short became the most expensive early Disney short, its negative cost being almost $2,500 more than the Steamboat Willie cartoon produced just a year prior. 

This short is an early example of how the Walt Disney animations became more sophisticated than previous animation. The short shows more realistic cartooning. The early Disney cartoons, like Steamboat Willie show many similarities to the Oswald the Lucky Rabbit cartoons that preceded them. However, as Mickey Mouse evolved from the silent film era, the cartoons became more intricate. Mickey Mouse began interacting within his space in his cartoon world to create a more realist feel. 

Walt Disney himself reinforces this idea:
  “...Our characters were beginning to act & behave like real persons. Because of this we could begin to put real feeling and charm into our characterization. After all you cant expect charm from animated sticks and that is what Mickey Mouse was in his first pictures”  Barrier, Michael. Hollywood Cartoons: American Animation in Its Golden Age. OUP Oxford, 2003 pg. 86   

==Use of sound== Prelude in C# Minor (Op. 2/3)" and George Bizets Carmen. It is also the first appearance of "Hungarian Rhapsody No. 2" by Franz Liszt in a cartoon, and its use heavily influenced later cartoons including "The Cat Concerto", "Rhapsody Rabbit" and "Rhapsody in Rivets" and a Walter Lantz cartoon Convict Concerto with Woody Woodpecker.

The sound for the short was recorded in February 1929 in New York by Walt Disney and Carl Stalling, who recorded the scores for the early Disney shorts including Steamboat Willie. Because there is no dialogue the music aids in telling the story. The music not only matches the actions being animated but the two are deeply connected. In one sequence a drummer pulls the tails of three different cats; those cats meows become notes in the music sequence. Notable dialogue did not come until 1934, when Walt Disney voiced Mickey Mouse using a falsetto. Therefore the comedy of the short comes in large parts to the use of music. 

==Mickeys gloves==
 
This short also introduced Mickeys gloves; Mickey can be seen wearing them in most of his subsequent appearances. Supposedly one reason for adding the white gloves was to allow audiences to distinguish the characters hands when they appeared against their bodies, as both were black (Mickey did not appear in color until The Band Concert in 1935).

==Other characters==
Mickey Mouse takes on multiple roles within the story of his vaudeville show. The short holds true to the early structure of Mickey Mouse cartoons. They are short, with less emphasis on storytelling and more focus on adventure and comedy.  A variety of additional animals make up the crowd and the band members. In addition, the instruments, especially the piano and stool take on human qualities, making them integral characters.

Minnie Mouse does not appear in person in this short. Instead, a poster of her can be seen which introduces her as a member of the Yankee Doodle Girls, apparently a group of female performers. The only other recurring character to appear in the short is known as Kat Nipp (apparently a play on the word catnip). This would be his debut; he would appear in two more shorts during the year as a minor antagonist.

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 