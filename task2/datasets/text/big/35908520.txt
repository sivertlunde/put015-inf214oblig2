The Rawhide Years
{{Infobox film
| name           = The Rawhide Years
| image          = 
| image size     =
| caption        = 
| director       = Rudolph Mate
| producer       = 
| executive producer = 
| writer         =  starring        = Tony Curtis Colleen Miller
| music          = 
| cinematography = 
| editing        = 
| distributor    = Universal Pictures
| released       = July 1, 1956
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = $1 million (US)  
| preceded by    =
| followed by    =
}} Western Directed by Rudolph Mate and starring Tony Curtis and Colleen Miller. 

==Plot==
Ben Matthews gives up the flashy life of a riverboat gambler, hoping to settle down in Galena with his girlfriend, luscious entertainer Zoe. But, Galenas leading citizen is murdered on the riverboat while on their way. Ben is implicated on arrival and flees, all the while working both sides of the law to clear his name. Three years of wandering later, Zoes letters stop coming and Ben returns to find her. Encountering Rick Harper, whom he initially takes as a would be bandit, they develop a friendship that winds up with both men saving the others neck.

==Cast==
*Tony Curtis as Ben Matthews   
*Colleen Miller as Zoe Fontaine     Arthur Kennedy as Rick Harper     
*William Demarest as Brand Comfort   
*William Gargan as Marshal Sommers 
*Peter van Eyck as Andre Boucher   
*Minor Watson as Matt Comfort    
*Donald Randolph as Carrico

==References==
 

==External links==
* 

 

 
 
 
 
 
 


 