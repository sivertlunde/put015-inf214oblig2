The Great and the Little Love
{{Infobox film
| name = The Great and the Little Love 
| image =
| image_size =
| caption =
| director = Josef von Báky Helmut Schreiber
| writer =  Heinrich Oberländer 
| narrator =
| starring = Jenny Jugo   Gustav Fröhlich   Rudi Godden   Maria Koppenhöfer Hans Sommer  
| cinematography = Friedl Behn-Grund    Wolfgang Becker     
| studio = Klagemann-Film 
| distributor = Tobis Film
| released = 29 April 1938  
| runtime = 82 minutes
| country = Germany  German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} on location in Italy.

==Cast==
*    Jenny Jugo as Erika Berghoff, Stewardeß  
* Gustav Fröhlich as Prinz Louis Alexander alias Dr. Alexander Bordam  
* Rudi Godden as Der Bordfunker  
* Maria Koppenhöfer as Die Königinmutter 
* Aribert Wäscher as Ein Gast im Café 
* Kurt Seifert as Heinrod  
* Gertrud de Lalsky as Frau Berghoff, Erikas Mutter  
* Flockina von Platen as Dr. Bordams französische Freundin  
* Walter Steinbeck as Der Ministerpräsident  
* Walter Werner as Erikas Vater  
* Elisabeth Eygk as Prinzessin Irina  
* Günther Hadank as Der König 
* Hans Kettler as Erster Pilot der Fluglinie 
* Hans Meyer-Hanno as Pilot  
* Georg H. Schnell as Der amerikanische Flugpassagier  
* Alfred Heynisch as Flugpassagier   Hans Schneider as Der Co-Pilot  
* Walter Schenk as Ein italienischer Flugpassagier   Klaus Pohl as Ein besorgter Flugpassagier  
* Walter Lieck as Peter Siebert, der blinde Flugpassagier  
* Albert Ihle as Flugpassagier  
* Michele Danton as Der italienische Kellner im Gartenrestaurant  
* Alfred Karen as Der Pokalüberreicher  
* Annemarie Steinsieck as Begleiterin  
* Herbert Weissbach as Der Kenner am Billardtisch  
* Max Mothes as Der Hotelportier in Zürich  
* Gustav Püttjer as Ein Flugmechaniker  
* Jac Diehl as Ein Bildreporter 
* Hans Leibelt
* Gustav Waldau
* Erika von Thellmann

== References ==
 

== Bibliography ==
* Hake, Sabine. Popular Cinema of the Third Reich. University of Texas Press, 2001.

== External links ==
*  

 

 

 
 
 
 
 
 
 
 
 