Robot Wrecks
{{Infobox film
| name           = Robot Wrecks
| image          =
| image_size     =
| caption        = Edward Cahn
| producer       = Metro-Goldwyn-Mayer
| writer         = Hal Law Robert A. McGowan
| narrator       =
| starring       =
| music          =
| cinematography = Jackson Rose
| editing        = Leon Borgeau
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 10 53"
| country        = United States
| language       = English
| budget         =
}}
 short comedy Edward Cahn.  It was the 200th Our Gang short (201st episode, 112th talking short, 113th talking episode, and 32nd MGM produced episode) that was released.

==Plot==
Slicker sells the Our Gang kids some "invisible rays," with which they hope to power their homemade mechanical robot. Miracle of miracles, the robot not only begins to move, but actually performs several of the gangs household chores. In truth, the robot is being manipulated by Slickers cohort Boxcar, but the kids dont find out until their rampaging mechanical man nearly lays waste to the entire neighborhood.   

==Cast==
===The Gang=== Mickey Gubitosi (later known as Robert Blake) as Mickey
* Darla Hood as Darla
* George McFarland as Spanky
* Billy Laughlin as Froggy
* Billie Thomas as Buckwheat

===Additional cast===
* Vincent Graeff as Boxcar
* Freddie Walburn as Slicker
* Margaret Bert as Froggys Mother
* Billy Bletcher as Froggys father / Voice of Froggys Mother
* Emmet Vogan as Robot owner
* Giovanna Gubitosi as Onlooker at robot demonstration

==See also==
* Our Gang filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 