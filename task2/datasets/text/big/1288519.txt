Repeaters
{{Infobox film
| name           = Repeaters
| image          = 
| caption        = 
| director       = Carl Bessai
| producer       = Jason James Carl Bessai Richard DeKlerk Irene Nelson
| writer         = Arne Olsen
| starring       = Dustin Milligan Amanda Crew Richard de Klerk
| music          = Jeff Danna
| cinematography = Carl Bessai
| editing        = Mark Shearer
| studio         = Rampart Films Raven West Films Resonance Films
| distributor    = Alliance Films
| released       =   }}
| runtime        = 89 minutes
| country        = Canada
| language       = English
| budget         = 
| gross          = 
}} thriller film directed by Carl Bessai, written by Arne Olsen, and starring Dustin Milligan, Amanda Crew, and Richard de Klerk as young drug addicts who find themselves stuck in a time loop.

==Plot==
Kyle Halsted, Sonia Logan, and Michael Weeks are inmates at a rehabilitation facility. Bob Simpson, the administrator, tasks them with apologizing to those they have hurt with their addiction. When Kyle attempts to apologize to his younger sister Charlotte, she angrily blows him off, and the principal kicks him off school grounds. Sonia goes to the hospital where her dying father is a patient, but she is unable to bring herself to face him. Michael visits his father in jail, but the conversation is cut short by his fathers abusive threats. When Bob tries to get them to discuss their day in group therapy, they refuse, and Michael storms off. Later, while discussing the pointlessness of Bobs therapy, Sonia is told that her father has died. As the trio try to deal with their emotional pain, a storm rolls in, and each of them is shocked and knocked unconscious.

When they wake up the next morning, the events of the previous day repeat. Kyle, Sonia, and Michael stumble through the day and repeat their actions in a daze. When they discuss the situation, Michael is intrigued by the consequence-free possibilities open to them, but Kyle convinces them to act on a news report that he recalls. They go to the dam but are too late to stop a jumper. Michael suggests that they take advantage of the situation, and they commit petty crimes that result in a stay at jail. Eventually, as the day repeats endlessly, they embark on a drug bender and crime spree that culminates in the violent kidnapping of a drug dealer who has been selling to Charlotte. At the dam, Michael carelessly risks his life walking on top of the railing and dares Sonia to do the same. When she slips, Michael merely laughs and refuses to try to help Kyle save her. Sonia falls to her death, though she wakes up with a gasp the next morning. Sonia claims to remember nothing of her death, and the trio become emboldened by their apparent immortality.

The next day, as Kyle and Sonia save the jumper at the dam, they discover that Michael has raped one of Charlottes friends. When Kyle and Sonia confront Michael, Michael accuses them of hypocrisy and says that his actions are excusable because everything will become reset the next day. Michaels behavior becomes more violent and antisocial as the days repeat. Shaken by Michaels behavior, Kyle ambushes him and ties him to a chair. Kyle and Sonia fall in love and work toward redemption, but Michael laughs at Kyle; Michael claims that Sonias story of childhood sexual abuse is just an act. However, when Kyle and Sonia successfully make peace with their pasts, the time loop abruptly ends, but Michael does not realize it until the middle of a violent rampage that ends with the senseless murders of two people. Freaked out, Michael takes Charlotte hostage, but he commits suicide after Kyle attempts to reason with him. In the last scene, Michael wakes up again, stuck in his own time loop.

==Cast==
  (left to right): Alexia Fast, Amanda Crew, Richard de Klerk, and Dustin Milligan]]

* Dustin Milligan as Kyle Halsted
* Amanda Crew as Sonia Logan
* Richard de Klerk as Michael Weeks
* Alexia Fast as Charlotte Halsted
* Benjamin Ratner as Bob Simpson Gabrielle Rose as Peg Halsted
* Hrothgar Mathews as Ed Logan
* John Tench as Charles Weeks
* Emily Perkins as Jumper
* Tom Scholte as Sgt. Gerald Tibbs
* Michael Adamthwaite as Tiko Taylor
* Manoj Sood as Mr. Singh
* John Cassini as Lieutenant Howard
* Anja Savcic as Michelle
* Michael Kopsa as Mr. Greeber

==Production==
Shooting took place in Mission, British Columbia on January 11 to 31, 2010. 

==Release==
The film premiered at the Toronto International Film Festival.   It was released on DVD August 9, 2011.

==Reception== Groundhog Day, including star Richard de Klerk, who described the film as "Groundhog Day on crack". Robert Koehler of Variety (magazine)|Variety called it "a dead-serious version of Groundhog Day" that "brings little personal energy".   Scott A. Gray of Exclaim! wrote that the film is not as preachy as expected, but it is still not the promised mindbender of the tagline.   Joel Harley of Starburst (magazine)|Starburst rated it 8/10 stars and wrote that it is a cynical take on Groundhog Day, neither original nor too derivative to be enjoyed.   Chris Knight of the National Post called it a complicated, intelligent, and elegant version of Groundhog Day that introduces more variables and selfish characters.   Bruce DeMara of the Toronto Star rated it 3.5/4 stars and described it as "Groundhog Day but without the laughs and with a wild, cerebral spin."   Kate Taylor of The Globe and Mail rated it 2.5/4 stars and called it "a small but interesting thriller" that "does not do full psychological justice to its clever premise." 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 