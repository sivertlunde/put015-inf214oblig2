The Dinosaur Project
{{Infobox film
| name           = The Dinosaur Project
| image          = The Dinosaur Project.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        =  Sid Bennett
| producer       = 
| writer         = Sid Bennet Jay Basu
| screenplay     =
| visual effects by = Jellyfish Pictures
| story          = 
| based on       =  
| narrator       = 
| starring       = 
| music          = Richard Blair-Oliphant
| cinematography = Tom Pridham
| editing        = Ben Lester
| studio         = 
| distributor    = 
| released       =  
| runtime        = 83 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = $526,165 (Foreign)
}} British adventure science-fiction film written and directed by Sid Bennett.

==Plot==
A group of explorers from the British Cryptozoological Society go on an expedition into the Congo in search of a cryptid, the so-called Mokele Mbembe which is believed to be a Plesiosaur. Along with the five explorers are two television cameramen who will be recording the whole expedition. The explorers consist of their leader Johnathan, their medic Liz, their sponsor organizer Charlie, their pilot, and a local guide, named Amara. During the helicopter flight, Johnathan discovers that his son Luke has snuck into the chopper as a stowaway. 

Shortly after that, a flock of large flying reptiles appears next to the helicopter and one of them crashes into the chopper blades, causing the helicopter to crash. The pilot dies in the crash and everyone else escapes just before the chopper explodes. Now lost, they discover that the satellite phone they had with them was broken during the crash, Amara suggests that they should head to the village they saw while they were in the helicopter. Upon arriving, the group discovers that the villagers have been killed and the village destroyed. Johnathan chooses a hut to stay in for the night and his son, who is technically talented, installs a night vision camera outside the hut. 

In the night, everyone is woken up by strange noises and Luke sees a swarm of bat-like reptiles outside the hut on the monitor linked to his night vision camera. Charlie accidentally alerts the creatures by making a noise and they try to flee from the village which is infested with the animals. Liz is attacked and killed while the rest escape in a pair of wooden boats and spend the night and part of the day on the river, until they arrive on a small island. On that island they encounter a group of dinosaurs with one in particular that seems to like Luke, which he decides to name Crypto. He spits a fluid on Luke. The next morning, they discover that the dinosaurs are still there and Luke decides to attach one of his cameras onto Cryptos neck to see where he would go to. The broadcast cuts out as the dinosaurs swim into a cave. 

Afterwards the group heads back on the river when the broadcast returns and Luke and Charlie see that the dinosaurs went straight to where they originated, some kind of underground gateway, where the camera is dropped. When they try to steer through a whitewater, Lukes and Charlies boat gets separated and drifts of towards the cave where the dinosaurs went earlier. The rest of the explorers follow them and, rejoining them in a river canyon they encounter a Plesiosaur. A suddenly emerging Pliosaur attacks the group. Charlie and Luke get out of the water, believing that the rest of the group died and continue to search for the place where the dinosaurs came from, in the jungle. Eventually they arrive there and when Charlie learns that Luke has fixed the satellite phone, he pushes Luke to fall down into the gateway, intending to kill him. 

On the other side of the underground passageway, Luke speaks into the camera, trying to reach the other survivors via the monitor. In the next scene the survivors are seen walking around at the river beach amidst the remains of the boats, the camera monitor lies shattered on the ground. Amara reveals that she was supposed to lead the group to do some records at a safe place in the jungle but she refuses to go towards the gateway and leaves, taking one of the boats. Jonathan and Pete continue the search for Luke and Charlie. In the meantime, Luke meets Crypto and follows him deeper into the jungle, where he is attacked by the bat-like creatures. He is rescued by Pete, who suddenly appears together with Jonathan and drives off the creatures. Pete chases them into the jungle, when he is suddenly encircled by the creatures who attack and presumably kill him. Charlie is seen, speaking to the camera at an unknown place, when he is interrupted and forced to hide by Jonathan and Luke, continuing through the jungle. They follow a steep cliffway, when they are hit by a rockfall, causing Jonathan almost to fall down the cliff. As Luke looks up, Charlie is seen with a stick in his hands, indicating that he caused the rocks to fall. Luke tries to help his father who is holding onto a rock, but, telling him that he loves him, he lets go of the rock and falls down the cliff. 

Luke escapes from Charlie and hides in the dense jungle, evading Charlie who is chasing him, when he meets the little dinosaur once more who leads him to the place where he dropped his camera earlier, when suddenly Charlie emerges in front of him, intending to kill him. Crypto spits the fluid into Lukes face, when two adult dinosaurs appear just behind Charlie, sniffing on Luke and smelling the fluid, they leave him alone and kill Charlie. 

Luke proceeds into the jungle and stops at a high cliff, filming himself and the little dinosaur, saying that the satellite phone has been crushed again and that he has to destroy the cameras in order to use the parts, he waves the camera over the view from the cliff, showing a big valley full of dinosaurs. In the next scene he is seen throwing the backpack down a waterfall into a river, as a message, then the camera blacks out, Lukes fate is unknown.

It is shown how the floating backpack is picked out of the river by men in a boat, who find video hard drives and tapes labeled "the Dinosaur Project" inside it.

The last scene shows a blurry image of Luke, and Luke says "I think it works", probably indicating that his father survived and reunited with him and they managed to fix the satellite phone.

==Cast==
* Richard Dillane ... Jonathan Marchant
* Peter Brooke ... Charlie Rutherford Matthew Kane ... Luke Marchant
* Natasha Loring ... Liz Draper
* Stephen Jennings ... Dave Moore
* Andre Weideman ... Pete Van Aarde
* Abena Ayivor ... Amara 
* Sivu Nobogonza ...Etienne

==Release==

The first trailer for the film was released on 11 July 2012.  The film was released in cinemas in the UK on 10 August 2012.  It is scheduled for release in cinemas worldwide through 2012–13, starting with South-East Asia on 23 August 2012. 

==Reception==

The movie has received generally mixed reviews. On IMDB, the movie has a rating of 4.7 out of 10. 

The Financial Times awarded the film 4 out of 5 stars (80%) and called it "rip-roaring fun...Feature debutant Sid Bennett’s motion-capture movie also does emotion capture. You will jump, gasp, grimace, just like the characters. The creatures are often very creepy. The Cloverfield-style dependency on “found footage” means everyone on screen must have a camera, but even that is carried off with dash and nonchalance."  

Digital Spy awarded the movie 3 stars out of 5 (60%) and labelled it "Decent found-footage caper...The Dinosaur Project is good and undemanding fun. There are enough sharp claws to overcome the flaws and ensure this is never a dino-bore...  

The Guardian wrote The CGI monsters are surprisingly convincing and children will identify with the intrepid 15-year-old lad who stows away on his dads helicopter and turns up trumps by using his computer wizardry."  

Katie Fraser of Screenjabber.com awarded the film 3½ stars out of 5, writing "If you don’t mind the shakiness of the found-footage genre (beware – a headache is likely), definitely go see this film if not for the computer-generated dinosaurs. It’s no Jurassic Park, but they did create some pretty cool-looking prehistoric monsters."  

Total Film awarded it three stars out of five and said “Though its desperate to be the next Jurassic Park, there’s little Spielbergian bite to this low-budget Brit flick. Instead we get wobbly cameras and equally wobbly acting from a cast of unknowns as a group of explorers hunt dinos in the Congo....Its money shots generally impress, and the breakneck pace bounds over a multitude of sins – including Park’s deadly dilophosaurus getting a makeover as a cute little critter that’ll have the nippers cooing.”  

IGN awarded the film 4 out of ten and said “Writer-director Sid Bennett does manage to eek moments of tension out of the premise, and the vast African vistas glimpsed throughout are a joy to behold...a found footage flick that disappoints at just about every turn, and makes you wish the tapes had remained lost.”  

The Independent awarded the film two stars out of five and said “The script stinks like dino-poo... but for all the silliness you may find yourself entertained.”  
 SFX awarded the film two and a half out of five stars and said “...has just enough charm to be worth a look – particularly if you have kids who like watching giant reptiles eating people...A few unexpected twists that mark it out as interesting (the running order of the deaths isn’t quite what you’d expect, for example) and the dinos are a cut above most films of this nature – particularly one little cutie who’s basically dino-Lassie. Admirably, the vast majority of the action is shot on location, with some white-water rafting to spice up the scenes with no reptiles in. Despite the plus points, though, it’s still a cheesy, childish adventure which tries very hard not to be the TV movie it actually is.”    

Empire Magazine awarded the film two stars out of five and said that the film was “More Terra Nova meets Sanctum 3D than Jurassic Park meets Cloverfield.” 

On its international cinema release, debuting in South East Asia, the movie was a commercial hit, entering the Malaysian box office charts in second position behind The Expendables 2.  On its release in Thailand, The Dinosaur Project attained third spot. 

On its DVD rental release in the US, The Dinosaur Project reached number 14 in the American DVD Rental Charts  

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 