Goliath II
 
{{Infobox Hollywood cartoon
| cartoon name = Goliath II
| series =
| image = 
| caption =
| director = Wolfgang Reitherman
| story_artist = Bill Peet
| followed = Thief and the Swamps of England Hal King Dick N. Lucas Cliff Nordberg Amby Paliwoda John Sibley
| layout = Collin Campbell Basil Davidovich Vance Gerry
| background = Richard H. Thomas Thelma Witmer Gordon Legg
| narrator = Sterling Holloway
| voice_actor = Kevin Corcoran Barbara Jo Allen Paul Frees
| musician = George Bruns
| producer = Walt Disney Buena Vista Distribution Co, Inc.
| release_date = January 21, 1960
| color_process = Ralph Hulett (color stylist) Technicolor
| runtime = 15 minutes
| language = English
}}
 animated short Walt Disney Productions and released on January 21, 1960. Sterling Holloway narrates this cartoon film, starring Kevin Corcoran. It was released to theaters in the U.S., alongside the Disney live-action motion picture Toby Tyler.
 Best Animated Short, but lost to Gene Deitchs Munro (film)|Munro. The cartoon is also called "Slonić Ćiro" or "Slonić Ćira" in Serbo-Croatian and was very popular there during the late 1970s and the early 80s.

==Synopsis== Peter Pan), but his mother saves him.
 The Jungle Book, which was released seven years later). Goliath IIs Mother and the Tiger, Raja, search for Goliath and manage to find him in a snail hole.  After a short tug of war between Goliaths Mother and Raja, Goliath is rescued. Shortly afterwards, Goliath is scolded for dishonoring his Mothers warnings on not wandering off and is put into a birds nest as punishment.  It is at this point that Goliath is now fed up with being treated like a baby (even though hes nine years old) and feels confident that he can take care of himself. 

That night, while the herd is sleeping, Goliath runs away and vows never to return. Afterwards he is startled by various jungle noises and is again attacked by Raja after mistakenly waking him up with a cattail, and he walked on and crying for his Mother. After his mother hears his cries she goes to his aid and finds that he is being taken away by Raja. After saving Goliath from Rajas grasp, she grabs the tiger by his tail and throws him straight into the crocodiles mouth; he escapes from its belly and runs away, scared out of his wits, and he was never seen again. Afterwards, Goliath gets a spanking from his mother for trying to desert the herd, because a deserter from the herd is branded a "scoundrel, a rogue elephant, a traitor to that high and imperial order of pompous pachyderms", and to make it worse, he has disgraced his father.
 elephants are Baggy Pants" and threatens Goliath by telling him that if he doesnt run away until he counts 3, he will hurt him. 

After the mouse counts 3, a tussle starts between the two as Goliath I and Mother watch. The fight ends with Goliath holding the mouse over a cliff, where below is the waiting hungry crocodile. Not wanting to become the crocodiles next meal, the mouse cowardly begs Goliath not to drop him and tells him that he (Goliath) is the Champion|champ. Goliath agrees and spares the mouse. After this, Goliath is respected by his father and is named the top elephant of the herd.

==Cast and Characters==

{| class="wikitable"
|-  style="background:#b0c4de; text-align:center;"
! Character
! Voice actor
! Profile
|- Narrator
|Sterling Holloway
|- Goliath II Kevin Corcoran A tiny elephant who wishes to prove that he can be as good as his father, though all his efforts and curiosities get him into trouble. At first considered a "disgrace" after leaving the herd, he is branded a hero after saving the other elephants.
|- Goliath IIs Mother Barbara Jo Allen Proud of Goliath IIs efforts, but upset at the mischief he causes.
|- Goliath I Paul Frees Goliath IIs father. Due to Goliath IIs size, he doesnt like his son at all.
|- Raja
| A tiger who wishes to eat Goliath II
|- The Mouse Paul Frees Another antagonist who tries to kill Goliath II.
|- Eloise
| Another female elephant that almost steps on Goliath II.
|- The Mugger Crocodile None
|Just behaves like a normal croc.  Based on Tick-Tock from Peter Pan.
|- The Mother Hornbill Gets irritated whenever she gets disturbed from watching her eggs.  Animation is based on the bird from Alice in Wonderland.
|}

==References==
 

==External links==
* 

 
 
 
 
 
 