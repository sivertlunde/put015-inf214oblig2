Brawn of the North
{{infobox_film
| name           = Brawn of the North
| image          = Brawn of the North (1922) - 3.jpg
| image_size     = 150px
| caption        = Newspaper ad
| director       = Laurence Trimble Jane Murfin
| producer       = Laurence Trimble Jane Murfin
| writer         = Jane Murfin (story) Laurence Trimble (story) Philip Hubbard (writer, scenario)
| starring       = Irene Rich Strongheart (a dog)
| music          =
| cinematography = Charles Dreyer
| editing        =
| studio        = Trimble-Murfin Productions Associated First National
| released       = November 12, 1922 1925 (Germany)
| runtime        = 8 reels
| country        = United States Silent (English intertitles)
}} lost  silent Northwoods Associated First National Pictures. The film stars Irene Rich and a new canine find by Trimble named Strongheart.  This was the second film starring the dog after his introduction in The Silent Call (1921).

The film is presumed to be lost. 

==Cast==
*Irene Rich - Marion Wells
*Lee Shumway - Peter Coe
*Joseph Barrell - Howard Burton
*Roger James Manning - Lester Wells
*Philip Hubbard - The Missionary
*Jean Metcalfe - The Missionarys Wife
*Baby Evangeline Bryant - The Baby
*Strongheart - The Dog

==See also==
*List of lost films

==References==
 

==External links==
 
* 
* 
* , Northeast Historic Film 

 
 
 
 
 
 
 

 