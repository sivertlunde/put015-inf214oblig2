Footsteps in the Sand (film)
{{Infobox film
| name           = Footsteps in the Sand
| image          = 
| caption        = 
| director       = Ivaylo Hristov
| producer       = 
| writer         = Ivaylo Hristov
| starring       = Ivan Barnev Yana Titova
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 2010
| runtime        = 89 minutes
| country        = Bulgaria
| language       = Bulgarian
| budget         = 
| gross          = 
}}
Footsteps in the Sand ( ) is a 2010 Bulgarian drama film directed by Ivaylo Hristov.

== Plot ==
Slavi (Ivan Barnev) once believed to have found true love in Nelly (Yana Titova). When he realizes that his girlfriend is in love with another man, he drowns his sorrows in alcohol, gets into conflict with the government and eventually runs away from Bulgaria. This is the start of a year-long odyssey that takes him halfway around the world: from a refugee camp in Austria to the streets of New York and the Utah desert. There he meets an Native American  who gives him an amulet, which he can win back his love. Slavi returns to Bulgaria and indeed he meets Nelly ....

== External links ==
*  

 
 
 

 
 