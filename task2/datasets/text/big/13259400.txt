Cutaway (2000 film)
 
{{Infobox Film
| name           = Cutaway
| image          = Cutaway-movie.jpg 
| image_size     = 
| caption        = 
| director       = Guy Manos
| producer       = Cutting Edge Entertainment Golden ParaShoot Entertainment
| writer         = Tony Griffin Greg Manos Guy Manos Ted Williams
| narrator       = 
| starring       = Tom Berenger Stephen Baldwin Dennis Rodman Maxine Bahns Casper Van Dien Thomas Ian Nicholas Phillip Glasser Marcos Ferraez Adam Wylie Roy Ageloff And Ron Silver Larry Brown Norman Kent Gerry Lively Martin Hunter
| distributor    = Artisan Entertainment
| released       =  
| runtime        = 104 min.
| country        =  English
| budget         = US$9 million (est.) 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 2000 action film about skydiving, directed by Guy Manos and starring Tom Berenger, Stephen Baldwin, Dennis Rodman, Maxine Bahns, Ron Silver, Casper Van Dien, and Thomas Ian Nicholas. The film features numerous aerial stunts and much of the film was shot in Miami, Florida|Miami. This was Dennis Rodmans third film. The term "cut-away" is used frequently in the film, in reference to parachuting and also in reference to life in general.

==Cast==
*Tom Berenger as Red Line
*Stephen Baldwin as Agent Victor Vic Cooper
*Dennis Rodman as Randy Turbo Kingston
*Maxine Bahns as Star
*Ron Silver as Lieutenant Brian Margate
*Roy Ageloff as Boom-Boom
*Marcos Ferraez as Ground Rush
*Adam Wylie as Cal
*Thomas Ian Nicholas as Rip
*Phillip Glasser as Cord
*Casper Van Dien as Delmira
*Cat Stone (credited as Cat Wallace) as Blonde Female Pilot
*Allison McKay as Mrs. Shipley William Booth as Bearded Man
*Rena McPherron as Woman with Baby
*Sam Blount Jr. as 12 Year Old Boy
*Danielle Waxman as Little Girl
*Ed Berliner as Burt
*Tony Griffin as Craig
*Sam Blount as Judge
*Craig Fronk as Pilot Championships
*Jefferey Jones as Meet Official
*Troy Manos as Boy #1
*Rex Manos as Boy #2
*Robert Small as Air Traffic Controller
*Bennett Liss as Surveillance Agent

==Plot==
A U.S. Customs Agent, Victor "Vic" Cooper (Stephen Baldwin), is about to make a drug bust as a plane lands, but the pilot (Cat Stone) and the plane are searched and no evidence is found. Cooper later suspects that the drugs are being jettisoned by parachute and goes to a drop zone to investigate. He notices a picture of the pilot and then seeks to infiltrate a group of skydivers, led by a man named Red Line (Tom Berenger), believing they are involved in drug smuggling. The group is training for an upcoming competition where teams compete to be the fastest to form a ring formation of eight people during free-fall. Cooper trains in a vertical wind tunnel with a man in the Army, Delmira (Casper Van Dien), in order to quickly improve his skills. Cooper falls for a woman in the group named Star (Maxine Bahns) and clashes with another member of the group named Turbo (Dennis Rodman). Lieutenant Brian Margate (Ron Silver) questions Agent Coopers judgement and later questions his loyalty as Cooper becomes more and more enmeshed in the group.

The term "cutaway" is used universally by skydivers for the act of jettisoning their main parachute canopy if it malfunctions, before pulling their reserve.  By extension, in this case, it refers to Cooper cutting away from (leaving) his former life (family, jobs, etc.) to skydive.

==See also==
*Point Break (1991) Drop Zone (1994) Terminal Velocity (1994)

==References==
 

==External links==
* 
* 

 
 
 
 
 
 

 