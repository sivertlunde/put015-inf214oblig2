Koi Aap Sa
{{multiple issues|
 
 
}}

{{Infobox film
| name           = Koi Aap Sa
| image          = Koi Aap Sa poster.png
| image_size     = 200px
| border         = 
| alt            = Koi Aap Sa poster
| caption        = Film poster
| director       = Partho Mitra
| producer       = Shobha Kapoor Ekta Kapoor
| writer         = 
| screenplay     = Mahesh Pandey	
| story          = 
| based on       =  
| narrator       = 
| starring       = Aftab Shivdasani Natassha
| music          = Himesh Reshammiya
| cinematography = Deepak Malwankar	
| editing        = Salil Malik
| studio         = 
| distributor    = 
| released       =   
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
}}
 Indian Bollywood film directed by Partho Mitra. It stars Aftab Shivdasani and Natassha in pivotal roles.  

==Cast==
* Aftab Shivdasani...Rohan
* Anita Hassanandani Reddy...Simran / Simi
* Dipannita Sharma...Preeti
* Himanshu Malik...Vicky
* Vaidya Advait...Sam
* Pushy Anand...Vickys mom
* Shama Deshpande...Sumati - Simrans mom
* Rajendra Gupta...Mahesh - Simrans dad

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Seene Mein Dil"
| Udit Narayan, Alka Yagnik
|-
| 2
| "Aadat Ho Chuki"
| Shaan (singer)|Shaan, Sunidhi Chauhan
|-
| 3
| "Baandh Mere Pairon Mein"
| Sonu Nigam, Sunidhi Chauhan
|-
| 4
| "Tere Dil Ka Rishta"
| Sonu Nigam
|-
| 5
| "Koi Aap Sa"
| Sonu Nigam, Alka Yagnik
|-
| 6
| "Kabhi Na Sukoon Aaya"
| Udit Narayan, Alka Yagnik
|-
| 7
| "Kabhi Na Sukoon Aaya (Sad)"
| Udit Narayan, Alka Yagnik
|-
| 8
| "Seene Mein Dil (Female)"
| Alka Yagnik
|}

==References==
 

==External links==
* 

 

 
 
 


 