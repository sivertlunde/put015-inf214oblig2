Newspaper Boy (1955 film)
 
 
{{Infobox Film
| name           = Newspaper Boy
| image          = Newspaperboyfilm.jpg
| image_size     = 
| caption        = A promotional poster of the film
| director       = P. Ramdas
| producer       = A group of students
| writer    = Story: P. Ramadas Screenplay: P. Ramadas Dialogues: Nagavally R. S. Kurup P. Ramadas (uncredited)
| narrator       = 
| starring       = Master Moni Master Narendran Neyyattinkara Komalam Nagavally R.S. Kurup Master Narendran Kumari Madhuri Master Venkiteswaran Kuriyathi
| music          = Songs: Vijayan A. Ramachandran Background score: Mahatma Sangeethamela
| cinematography = Madhavan Nair
| editing        = K. D. George
| studio         = Adarsh Kalamandir
| distributor    = R. S. Pictures   Variety Pictures  
| released       =  
| runtime        = 130 minutes
| country        = India
| language       = Malayalam
| budget         =   
| gross          = 
}} drama of stark realism, narrates the life of the common man on the street. The film is noteworthy in that the entire production programme from script-writing to direction was controlled and executed by students. The group of students were from Adarsh Kalamandir  and was written and directed by P. Ramdas. The screenplay was based on a short story written by Ramadas himself. 
 shoestring budget of  , featured mostly amateur actors, and was made by an inexperienced crew, Newspaper Boy was a critical success. Influenced by Italian neorealism, the film is also remembered for being the worlds first commercial film made by students.   

==Plot==
Set in Kerala of the 1950s, Newspaper Boy focuses on the lives of Appu (Master Moni), the young protagonist of the film, and the members of his impoverished family. Appus father Sankaran Nair (Nagavally R. S. Kurup) earns a meagre living as a worker in a printing press. He is easily exploited in his work – he cannot even muster the courage to ask his employer for overdue wages. Nairs wife, Kalyani Amma (Neyyattinkara Komalam), takes care of their three children, Leela (Kumari Madhuri), Appu and Balan (Master Narendran). Kalyani Amma helps her husband by taking up the job of a housemaid in rich Madhava Menons (G. R. Nair) house. She is treated like a slave by Menons wife Lakshmi Amma (Adoor Pankajam). Extreme poverty forces Leela to stop her education. However Nair manages to provide education to Appu, his only hope.

Adding oil to the fire, one day, Nair injures his hand during his work in the press. He could not go to work for two-three weeks. The employer fires Nair from the job during this period. He offers Nair the payment for those days, but Nair refuses it, although his family is in dire need of money. Nair tries to find another job but in vain. He cannot even pay the house-rent. House owner Kesavan Pillai (Veeran) asks Nair either to pay the rent or to leave the house. Soon Nair dies of extreme poverty and illness. After his demise, the family sinks even deeper into poverty. The whole family is now on Appus shoulders. Appu finds no way to save his family from this situation. Kittummavan (Kuriyathi), a kind-hearted neighbour, is his only help. Kittummavan asks his son Raghavan (P. Ganga) to take Appu to Madras and find him a job.
 newspaper boy to look after his brother and sister.

==Cast==
* Master Moni as Appu
* Master Narendran as Balan
* Master Venkiteswaran as Pappan
* Master Mohan as Gopi, Madhava Menons son
* Baby Usha as Indira, Madhava Menons daughter
* Kumari Madhuri as Leela
* Nagavally R. S. Kurup as Sankaran Nair
* Veeran as Kesavan Nair
* Kuriyathi as Kittummavan
* G. R. Nair as Madhava Menon
* P. Gangadharan Nair as Raghavan
*T. R. Omana
* K. Madhavan as Sreedhara Menon
* Neyyattinkara Komalam as Kalyani Amma
* Adoor Pankajam as Lakshmi Amma
* Omana Madhavan as Kamalamma
* Chandni as Pankajam, Kittummavans daughter
*Miss Kumari
*Snehalatha
*Venkiteswaran

==Production==
===Development===
P. Ramdas once read in Filmfare magazine that Raj Kapoor was Indias youngest film director. Ramdas, who was 18 then, dreamed of making a film and told his friends that he would soon take this honour.    One of his friends, S. Parameswaran, who studied with him at University College in Thiruvananthapuram, was also passionate about movies. The idea to make the film was born out of Ramadass  and Parameswarans desire to make a movie "of the beaten track."    They were heavily inspired by the themes of Italian neorealism, which had already created waves in the Malayalam literary circles. The two young minds wanted to bring this new wave to cinema too. Ramadas was sure his own short story "Compositor" would be the best option to start with.    The story was first published in Mahatma Malayala Masika, the worlds first magazine run by students. It was later included in the book Thalirukal, published by Mahatma Publishing House.     Movies of V. Shantaram also inspired Ramadas and Parameswaran to make such a "different" movie.  As they had little knowledge about the technical aspects of cinema, they became regular visitors to the erstwhile American Information Library in Thrivananthapuram and read as much books available on cinema.  Ramadas travelled to Madras, the then centre of South Indian filmmaking, and collected an 8mm Kodak Baby Browny Movie Camera using which he later filmed two short films, Footpath (which was filmed from Thrissur) and Life for Film (which was filmed from Kanyakumari), which all helped him to gain some knowledge in professional movie-making.   

Ramadas wrote a "film treatment", by making certain changes in his short story, and developed it into a complete script by 1953 September–October.    The story is retold from the perspective of a twelve-year-old boy in the screenplay. The storys central character is Lonappan, who was renamed as Sankaran Nair in the screenplay. Nairs son Appu is the central character of the film while this character has little importance in the story.  Ramadas approached popular writer Nagavally R. S. Kurup to write dialogues, who agreed on a payment of  1000. Kurup submitted his work soon, but was rejected by Ramadas, stating that it was written in a typical Travancore accent but the story is taking place in Thrissur. Ramadas himself rewrote the dialogues in the Thrissur-slang of Malayalam.    He planned to film the movie in May 1954, after his exams, and booked a floor of the erstwhile Merryland Studio in Thiruvananthapuram for indoor shooting.   

The film was produced by Adarsh Kalamandir, a subdivision of Mahatma Memorial Association, founded by Ramadas and his friends in 1945 as Balasangham.    The ever-growing enthusiasm of the students found the Association venturing into a field hitherto undreamt of by the student population of the world. 

===Casting===
P. Ramdas wanted to cast amateurs and new-faces in all the departments of his film. K. C. Ponkunnam (K. C. Francis) was assigned to write the songs, Kanthaswamy was chosen as the art director, and Vijayan and his brother Ramachandran as music directors. All of them were members of Mahatma Memorial Association and had a long-term association with Ramadas through the plays he had directed.    V. Balakrishnan, who played woman characters in all of Ramadass plays, was given the job of costume designing.  Krishnan Elaman, Balakrishnan and K. D. George, all of them from Merryland Studios, were chosen for sound recording, makeup and editing respectively. Ramadass and Parameswarans relatives were also given some jobs, like production control, lighting etc. P. K. Madhavan Nair, who was an assistant cinematographer in Malayalam films till then was chosen as the director of photography. 

The lead character Appu was inspired by a twelve-year-old newspaper boy Ramadas had met somewhere in a journey to Ernakulam.  Ramadas and Parameswaran visited almost all schools and orphanages to find a suitable face but failed. Appus role was at last given to Master Moni, who was introduced to Ramadas by Nagavally R. S. Kurup.  His original name was Narayanan Pillai and was a relative of Kurup.  Appus brother Balans role was given to a four-year-old boy named Narendran.  Another major character, Sankaran Nair, Appus father, was inspired by a press worker named Cheekutty. Ramadas approached Cheekutty to play this role who instantly rejected, which was later taken by Kurup himself, who had previously acted small roles in movies like Sasidharan and Navalokam.  A theatre actor named Jagadamma was originally cast in the role of Kalyani Amma, Appus mother, but was later replaced by Neyyattinkara Komalam.  Other major roles were played by the members of the association or by their friends. 

===Filming===
Ramadas knew that his Kodak Baby Browny would not be useful to film a feature-length movie. So he imported an Avery camera and a Ferrograph tape-recorder from England. The camera was found useless for professional movie making and was not used for the film. The tape-recorder was used to mix sound in the film, a too pre-mature way which would later be criticised by various mainstream critics.  As planned earlier, Ramadas was able to commence shooting in May 1954 itself. The camera-switch-on ceremony was conducted by Colonel G. V. Raja on 5 May 1954 in Merryland Studios.  Indoor sequences were completely filmed from Merryland using a Michelle camera while outdoor sequences were mainly filmed from Thrissur and Madras.    The realism-influenced train sequence, inspired by a scene from Akira Kurosawas Rashomon (film)|Rashomon, was filmed from Ernakulam South railway station.  Ramadas was ready to go for any number of retakes to get the right output from the cast. For instance, he wanted to shoot the scene in which Kittummavan explains his son about Sankaran Nairs condition in a single shot. To get what he had in mind, Ramadas needed seven takes. 

Dialogue-dubbing was not common then and live sound recording was to be used. The Ferrograph tape-recorder was used for mixing much of the background sound. Shooting was finished in early 1955. 

==Release==
P. Ramdas was approached by various distribution companies while filming itself, but he rejected all the offers since he was sure that they would interfere in the making of his dream film. Ramadas could not find a distributor after the production for a long time.    Later, Ernakulam-based R. S .Pictures and Kottayam-based Variety Pictures agreed to distribute the film in Kochi-Malabar and Travancore respectively. The distributors gave them   50,000 on the condition that if the film did not fetch the amount within a year Ramdas had to pay back the difference.  The total production cost of the film was  1,25,000, which along with the amount charged by the distribution companies for print and publicity, will make the total budget of the film   . 
 Malabar states. In Thrissur, it was screened at the Jose theatre.    The film, made at a budget of    , was panned by audiences who were not interested in seeing "their own lives onscreen", and ultimately became a big box office failure. Ramadas as well as his colleagues were bankrupt after the films failure.   

==Critical reception==
The movie opened to good reviews from various critics with many of them appreciating Ramadas for taking up such a risky and experimental film. However the film received criticisms as well, mainly for its editing, sound mixing and music.  A reviewer from Bombay Chronicle said that it was pleasant to find a film that was not "only progressive in abandoning set formulae, but progressive" in how it did realism. The intent was said to be serious, but without bitterness underneath. The reviewer also complimented Newspaper Boy for its universality.  Sunday Standards reviewer said the film is praiseworthy, and lacking amateur quality despite being made by students; the reviewer also commented on the films emotional nature.  A critic from Blitz said that the films title is picturesque, and the film itself was "wonderful". The critic also said the film was thought-provoking, not only about its heartwarming but "disturbing" theme, but from its being produced by students.  Reel-News said that the direction was excellent and the acting was livened by its young stars.  A review by Screen India stated that the unusual part of the film was its lack of usage of "the usual box-office devices."

Reviewers from the South also praised the film. Jayakeralam weekly said that the film was excellent and well-directed.    A review byDeenabandhu said that the film presented the everyday life of a middle-class family with complete realism.    A reviewer from Kerala Kaumudi compared the direction to that of the best non-Indian pictures.  A review by Sakhavu said that those who "forget art and culture to make money" were hit hard by the film.  A review by Deepam said that the producers of this film, Adarsa Kala Mandir (Temple of Art), have justified their name by this film. The beauty of Kerala, according to the reviewer, came through with no disruption of the plot.  Writer-producer Rajinder Singh Bedi said that it was well-presented, well-directed and well-photographed, and wished that it would open "the eyes of our producers, in Bombay."    Film Divisions director V. R. Sharma stated that it marked a refreshingly new approach in Southern Indian film-making due to its realistic nature. He praised the acting, and said that the films makers "can justifiably be proud of their achievement and deserve every encouragement."  I. K. Menon, Secretary of the Indian Motion Picture Producers Association, said that the realism of the film was unbelievable, with a correct portrayal of everyday life in "Kerala without any exaggeration or colour merely to entertain, as is usual in films." 

==Songs==
The songs are composed by Vijayan and his brother A. Ramachandran, both of them members of Mahatma Memorial Association. Background score was by Mahatma Sangeethamela, a subdivision of the Association. There are eleven songs in the film, and the playback singers include Kamukara Pusushothaman and Santha P. Nair. Ramachandran, one of the composers, says that all the songs were for a specific purpose, such as showing the passage of time. He adds his amazement about important singers agreeing to sing for them. The re-recording was done by Ramachandran alone, without his brother Vijayan, due to illness; he was not able to transfer the songs to better recording media due to inexperience.  Lyrics were penned by K. C. Poonkunnam and P. Gangadharan Nair. Three traditional poems were also used.    Nine songs were recorded from Merryland Studios, while the songs "Chirichukondeevazhikku" and "Pazhaya Yugangal" were recorded from Udaya Studios and from a studio in Madras respectively.  Malayalam film music was copied from Hindi and Tamil film songs in those times. Ramadas wanted to change this system and in Newspaper Boy, all the tunes are original and follows the folk traditions of the State. Different from the films of that time, majority of songs were used as montage songs that appeared in the background of the film.  Audio rights were bought by a newly formed Bombay-based company named Bulbil. But the company closed down before the films release and no records were released. 

#"Kallilum Mullilum" — T. A. Kumaresan, T. A. Lakshmi
#"Devi Sarweswari" — Shyamala
#"Naranayingane Janichu Bhoomiyil" — P. Ganga
#"Omanathinkal Kidavo" — Santha P. Nair
#"Thekkankaatte" — Lakshmi
#"Maveli Naadu Vaaneedum Kaalam" — Kamukara Purushothaman, Santha P. Nair, Lakshmi, Vijayan, Ramachandran
#"Udayagiri Chuvannu" — P. Ganga
#"Enthinu Kanneerennum" — Kamukara Purushothaman
#"Chirichukondeevazhikku" — Ramachandran
#"Pazhaya Yugangal" — Vijayan
#"Thellakalathuninnu" — Kumaresan, T. A. Lakshmi

==Awards==
* 1956 – Madras Film Fans Association Award for Best Malayalam Film   
* 1962 – Kerala Sangeeta Nataka Academy Award for Best Script (selected from all the Malayalam films released between 1954 and 1962) 

==Merchandise==
Malayalee Indian director Pradeep Nair made a 24-minute documentary film titled Oru Neorealistic Swapnam (A Neorealistic Dream), which detailed the making of Newspaper Boy. The film was produced by V. S. Rajesh with credit from the Public Relations Department of Government of Kerala, and was conceived for Film Buff.     
 John Paul edited the book. The book consists of the script (rewritten by P. Ramdas since the original screenplay was lost), the original story, the songbook, reviews and memoirs. 

==References==
===Notes===
 

===Bibliography===
*  

== External links ==
*  

 
 
 
 
 
 
 
 