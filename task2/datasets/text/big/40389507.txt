Hoodlum Empire
{{Infobox film
| name           = Hoodlum Empire
| image          = Hoodlum Empire poster.jpg
| alt            =
| caption        = Theatrical release poster
| director       = Joseph Kane
| producer       = Herbert J. Yates
| screenplay     = Bruce Manning Bob Considine
| story          = Bob Considine
| narrator       = 
| starring       = Brian Donlevy Claire Trevor Nathan Scott
| cinematography = Reggie Lanning
| editing        = Richard L. Van Enger
| studio         = Republic Pictures
| distributor    = Republic Pictures
| released       = 15 April 1952
| runtime        = 98 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} Kefauver Committee hearings dealing with organized crime.   

==Plot==
Former gangster Joe Gray (Russell) joined the army during World War II and became a hero is now leading a respectable life. When he is called before a grand jury to testify against organized crime activities, his former mobster colleagues prepare to take measures to ensure that he doesnt.

==Cast==
 
 
* Brian Donlevy as Senator Bill Stephens 
* Claire Trevor as Connie Williams 
* Forrest Tucker as Charley Pignatalli 
* Vera Ralston as Marte Dufour 
* Luther Adler as Nick Mancani  John Russell as Joe Gray 
* Gene Lockhart as Senator Tower 
* Grant Withers as Rev. Simon Andrews 	 
* Taylor Holmes as Benjamin Lawton 	 
 
* Roy Barcroft as Louis Draper  William Murphy as Pete Dailey 
* Richard Jaeckel as Ted Dawson 
* Don Beddoe as Senator Blake 
* Roy Roberts as Chief Tayls 
* Richard Benedict as Tanner 
* Phillip Pine as Louis "Louie" Barretti 
* Damian OFlynn as Ralph Foster  Pat Flaherty as Mikkelson
 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 


 