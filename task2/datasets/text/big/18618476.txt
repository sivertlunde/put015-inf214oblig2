The Hearse
{{Infobox film
| name           = The Hearse
| image          = The Hearse.jpg
| caption        = Theatrical poster George Bowers
| producer       =
| writer         = William Bleich
| starring       = Trish Van Devere Joseph Cotten David Gautreaux Donald Hotton
| music          =
| cinematography =
| editing        =
| distributor    = Crown International Pictures
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 horror film starring Trish Van Devere and Joseph Cotten.

==Plot==
Jane Hardy (Trish Van Devere) arrives in the town of Blackford to stay in an old house left to her by a late aunt. As time passes, Jane learns secrets her aunt kept from her in life, but that were well known by the townspeople.
 devil worshipper, and upon her death, the hearse carrying her body crashed, but no sign of the driver or of the coffin were ever found. Since then, the house inherited by Jane has been haunted by evil spirits and the rural road out of Blackford has been haunted by the hearse that crashed.

As these stories come to light, Jane attempts to leave Blackford to avoid being drawn in by her aunts spirit, but finds herself pursued by the ghostly hearse and held prisoner inside Blackford by spirits.

==Full cast==

*Trish Van Devere - Jane Hardy
*Joseph Cotten - Walter Pritchard
*David Gautreaux - Tom Sullivan
*[Donald Hotton - Reverend Winston
*Med Flory - Sheriff Denton
*Donald Petrie - Luke
*Christopher McDonald - Pete
*Perry Lang - Paul Gordon
*Fred Franklyn - Mr. Gordon
*Al Hansen - Bo Rehnquist
*Dominic Barto - The Driver
*Nicholas Shields - Dr. Greenwalt
*Chuck Mitchell - Counterman
*Allison Balson - Alice
*Jim Gatherum - Boy #1
*Victoria Eubank - Lois
*Tanya Bowers - Schoolgirl

==Critical reception==
 The Changeling, which came out last April and starred Van Devere, her husband George C. Scott and, of course, the obligatory self-banging doors and self-playing musical instruments)." 
 Friday the 13th. Isnt that nice to know?" 
 The Texas Friday the Slob Zombie constitutes the future of Horror, The Hearse is not for you." 

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 