March of the Penguins
 
{{Infobox film
| name           = March of the Penguins
| image          = march_of_the_penguins_poster.jpg
| image_size     = 215px
| alt            = 
| caption        = North American release poster
| director       = Luc Jacquet
| producer       = Yves Darondeau Christophe Lioud Emmanuel Priou
| writer         = Luc Jacquet Michel Fessler
| narrator       = Charles Berling Romane Bohringer Jules Sitruk Morgan Freeman
| music          = Émilie Simon   Alex Wurman  
| cinematography = Laurent Chalet Jérôme Maison
| editing        = Sabine Emiliani Wild Bunch National Geographic Films
| distributor    = Walt Disney Pictures   Warner Independent Pictures  
| released       =    
| runtime        = 80 minutes  
| country        = France
| language       = French
| budget         = $8 million
| gross          = $127,392,693 
}} French La Marche de lempereur ;  ) is a 2005 French nature documentary film directed and co-written by Luc Jacquet, and co-produced by Bonne Pioche and the National Geographic Society. The documentary depicts the yearly journey of the emperor penguins of Antarctica. In autumn, all the penguins of breeding age (five years old and over) leave the ocean, their normal Habitat (ecology)|habitat, to walk inland to their ancestral breeding grounds. There, the penguins participate in a courtship that, if successful, results in the hatching of a chick. For the chick to survive, both parents must make multiple arduous journeys between the ocean and the breeding grounds over the ensuing months.
 Dumont dUrville in Adélie Land.
 2005 Academy Best Documentary Feature.   

==Subject matter== Antarctic summer, the breeding ground is only a few hundred meters away from the open water where the penguins can feed. However, by the end of winter, the breeding ground is over   away from the nearest open water. In order to reach it, all the penguins of breeding age must traverse this great distance.

The penguins practice serial monogamy within each breeding season. The female lays a single egg, and the co-operation of the parents is needed if the chick is to survive. After the female lays the egg, she transfers it to the feet of the waiting male with minimal exposure to the elements, as the intense cold could kill the developing embryo. The male tends to the egg when the female returns to the sea, now even farther away, both in order to feed herself and to obtain extra food for feeding her chick when she returns. She has not eaten in two months and by the time she leaves the hatching area, she will have lost a third of her body weight.

For an additional two months, the males huddle together for warmth, and incubate their eggs. They endure temperatures approaching  , and their only source of water is snow that falls on the breeding ground. When the chicks hatch, the males have only a small meal to feed them, and if the female does not return, they must abandon their chick and return to the sea to feed themselves. By the time they return, they have lost half their weight and have not eaten for four months. The chicks are also at risk from predatory birds such as northern giant petrels. 

The mother penguins come back and feed their young, while the male penguins go all the way back to sea (70&nbsp;miles) to feed themselves. This gives the mothers time to feed their young ones and bond with them. Unfortunately, a fierce storm arrives and some of the chicks perish.

The death of a chick is tragic, but it does allow the parents to return to the sea to feed for the rest of the breeding season. When a mother penguin loses its young in a fierce storm, it sometimes attempts to steal another mothers chick. At times, the young are abandoned by one parent, and they must rely on the return of the other parent, who can recognize the chick only from its unique call. Many parents die on the trip, killed by exhaustion or by predators (such as the leopard seal), dooming their chicks back at the breeding ground.

The ingenious fight against starvation is a recurring theme throughout the documentary. In one scene, near-starving chicks are shown taking sustenance out of their fathers throat-sacs, 11th-hour nourishment in the form of a milky, protein-rich substance secreted from a sac in the fathers throat to feed their chicks in the event that circumstances require.

The parents must then tend to the chick for an additional four months, shuttling back and forth to the sea in order to provide food for their young. As spring progresses, the trip gets progressively easier as the ice melts and the distance to the sea decreases, until finally the parents can leave the chicks to fend for themselves.

==International versions==
The style of the documentary differs considerably between the original French version and some of the international versions.

The original French language release features a first-person narrative as if the story is being told by the penguins themselves. The narration alternates between a female (Romane Bohringer) and a male (Charles Berling) narrator speaking the alternate roles of the female and male penguin, and as the chicks are born their narration is handled by child actor Jules Sitruk. This style is mimicked in some of the international versions. For example, in the Hungarian version actors Ákos Kőszegi, Anna Kubik, and Gábor Morvai provide the voices of the penguins, and the German version as seen in German movie theaters (and in the televised broadcast in April 2007 on channel ProSieben) uses the voices of Andrea Loewig, Thorsten Michaelis and Adrian Kilian for the "dubbed dialog" of the penguins. This style of narration is also used in the Danish and Cantonese DVD versions.

The English language distribution rights were acquired at the Sundance documentary Festival in January, 2005, by  ; and the Swedish version, narrated by Swedish actor   (English: Child, Child, How Were You Made?)
 score by Alex Wurman.

==Releases and responses== The Aviator during its opening week.

The original French version was released in  , grossing over $77 million in the United States and Canada (in nominal dollars, from 1982 to the present.) It grossed over $127 million worldwide.  Its the only movie from Warner Independent to be rated G by the MPAA.
 Region 2 release featured no English audio tracks or subtitles.

An extra on the DVD issue was the controversial 1948 Bugs Bunny cartoon Frigid Hare, in which Bugs visits the South Pole and meets a young penguin fleeing an Eskimo hunter. The cartoon is not frequently seen because of its stereotypical depiction of the Inuit hunter, but it was included here uncut and uncensored. This is substituted in the European release with 8 Ball Bunny.

In November 2006, the documentary was adapted into a video game by DSI Games for the Nintendo DS and Game Boy Advance platforms. It features Lemmings (video game)|Lemmings-like gameplay.

In 2007, a Direct-to-video|direct-to-DVD parody written and directed by Bob Saget called Farce of the Penguins was released. It is narrated by Samuel L. Jackson and features other stars providing voice-overs for the penguins. Although the film uses footage from actual nature documentaries about penguins, the parody was not allowed to include footage from March of the Penguins itself. 

==Production==
The DVD version includes a 54 minute film entitled Of Penguins and Men made by the film crew Laurent Chalet and Jérôme Mason about the filming of March of the Penguins. 
 
 

Director and film crew spent more than 13 months at the Dumont dUrville Station where the Institut polaire français Paul-Émile Victor is based. Although the penguins meeting place, one of four in Antarctica, was known to be near, the day on which it occurs isnt known so they had to be ready every day. Fortunately the gathering that year was huge – more than 1,200 penguins, compared with the norm of a few hundred.

For cameras to operate at −40&nbsp;°C (−40&nbsp;°F) they had to use film and to load all the film for the day, as it was impossible to reload outside. Because of the isolation from any film studios it was necessary to remember each shot to ensure continuity and to make sure that all the necessary sequences were finished.  

The main challenge of making the documentary was the weather with temperatures between −50 and −60&nbsp;°C (−58 and −76&nbsp;°F). At dawn the film crew would spend half an hour putting on six layers of clothes, and on some days they couldnt spend more than three hours outside. They worked in winds with gusts up to 125 miles per hour, "which in some ways is worse than the cold temperatures" according to director Jaquet. 
 
 

==Political and social interpretations== conservative family values by showing the value of stable parenthood.  Medveds comments provoked responses by others, including Andrew Sullivan,  who pointed out that the penguins are not in fact monogamous for more than one year, in reality practicing serial monogamy. Matt Walker of New Scientist pointed out that many emperor penguin "adoptions" of chicks are in fact kidnappings, as well as behaviours observed in other penguin species, such as ill treatment of weak chicks, prostitution, and ostracism of rare albino penguins.  "For instance, while it is true that emperor penguins often adopt each others chicks, they do not always do so in a way the moralisers would approve of."  Sullivan and Walker both conclude that trying to use animal behavior as an example for human behavior is a mistake.

The director, Luc Jacquet, has condemned such comparisons between penguins and humans. Asked by the San Diego Union Tribune to comment on the documentarys use as "a metaphor for family values – the devotion to a mate, devotion to offspring, monogamy, self-denial", Jaquet responded: "I condemn this position. I find it intellectually dishonest to impose this viewpoint on something thats part of nature. Its amusing, but if you take the monogamy argument, from one season to the next, the divorce rate, if you will, is between 80 to 90 percent... the monogamy only lasts for the duration of one reproductive cycle. You have to let penguins be penguins and humans be humans." 

Some of the controversy over this may be media driven. Rich Lowry, editor of National Review, reported in the magazines blog that the BBC "have been harassing me for days over March of the Penguins ... about what, Im not sure. I think to see if I would say on air that penguins are Gods instruments to pull America back from the hell-fire, or something like that. As politely as I could I told her, Lady, theyre just birds." 
 evolution by Steve Jones, professor of genetics at University College London, is quoted as saying, "Supporters of intelligent design think that if they see something they dont understand, it must be God; they fail to recognise that they themselves are part of evolution. It appeals to ignorance, which is why there is a lot of it in American politics at the moment."  Author Susan Jacoby claims in her 2008 book, The Age of American Unreason (page 26), that the distributors of the movie deliberately avoided using the word "evolution" in order to avoid backlash from the American religious right, and writes, "As it happens, the emperor penguin is literally a textbook example, cited in college-level biology courses, of evolution by means of natural selection and random mutation. ... The financial wisdom of avoiding any mention of evolution was borne out at the box office ..."

==Awards==
* Won – Academy Award for Documentary Feature
* Won – American Cinema Editors, Best Edited Documentary: Sabine Emiliani
* Nominated - , Jérôme Maison
** Best Editing: Sabine Emiliani
* Won – Broadcast Film Critics Association, Best Documentary
* Won – César Awards, Best Sound (Meilleur son): Laurent Quaglio, Gérard Lamps
** Nominated, Best Editing (Meilleur montage): Sabine Emiliani
** Nominated, Best First Work (Meilleur premier film): Luc Jacquet
** Nominated, Best Music Written for a Film (Meilleure musique): Émilie Simon
* Won – National Board of Review, Best Documentary
* Nominated – Online Film Critics Society Awards, Best Documentary
* Nominated – Satellite Awards, Outstanding Documentary DVD
** Nominated, Outstanding Motion Picture, Documentary
* Won – Southeastern Film Critics Association Awards, Best Documentary
* Nominated – Writers Guild of America, Documentary Screenplay Award: Jordan Roberts (narration written by), Luc Jacquet (screenplay/story), Michel Fessler (screenplay)
* Won –  
* Won – Victoires de la musique, Original cinema/television soundtrack of 2006

==References==
 

;Further reading
*   – David Smith, Guardian Unlimited Film News, 18 September 2005
*     – Amelie Zola, IMAGO, European Federation of Cinematographers, March 2007

==External links==
 
*  
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 