Seven Slaves Against the World
 
{{Infobox film
| name           = Gli schiavi più forti del mondo (Seven Slaves Against the World)
| image          = Seven slaves against the world movie poster.jpg
| image_size     = 150px
| border         = yes
| alt            = 
| caption        = Theatrical release poster in the United States
| director       = Michele Lupo
| producer       = Elio Scardamaglia
| writer         = Roberto Gianviti Michele Lupo
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Roger Browne Gordon Mitchell Arnaldo Fabrizio 
| music          = Francesco De Masi	 	
| cinematography = Guglielmo Mancori	 
| editing        = 
| studio         = Leone Film
| distributor    = Paramount Pictures (within the United States)
| released       =  
| runtime        = 96 min  (United States)  85 min  (France) 
| country        = Italy
| language       = Italian language
| budget         = 
| gross          = 
}}

Seven Slaves Against the World (  adventure film, directed by Michele Lupo, produced by Elio Scardamaglia, written by Lupo and Roberto Gianviti and starring Roger Browne, Gordon Mitchell and Arnaldo Fabrizio. First released in Italy in 1964, it premiered in New York City, United States on August 18, 1965. 

==Cast==
*Roger Browne as Marcus
*Gordon Mitchell as Balisten
*Arnaldo Fabrizio as Goliath
*Scilla Gabel as Claudia
*Aldo Pini as Traidor
*Alfredo Rizzo as Efrem
*Giacomo Rossi-Stuart as Gaius
*Carlo Tamberlani as Lucius Terentius
*Germano Longo as Lucius Emilius		
*Alfio Caltabiano as Gladiatore
*Calisto Calisti as Selim

==See also==
*List of Italian films of 1964

==References==
 

==External links==
* 
* 

 
 
 
 


 
 