Cheriya Lokavum Valiya Manushyarum
{{Infobox Film
| name           = Cheriya Lokavum Valiya Manushyarum
| image          = Cheriya Lokavum Valiya Manushyarum.jpg
| image_size     = 
| caption        = 
| director       = Chandrasekharan
| producer       = Chaithram Cini Arts
| writer         = Chandrasekharan (story),   T. A. Rasaq (screenplay),   A. R. Murukesh (screenplay)
| narrator       =  Mukesh  Thilakan, Innocent (actor)|Innocent, Jagathy Sreekumar Johnson
| cinematography = Saloo George
| editing        = Rajashekaran
| distributor    = Charangat Release
| released       = 1990
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}}
Cheriya Lokavum Valiya Manushyarum is a 1990 Malayalam comedy film directed by Chandrasekharan. The film stars Mukesh (actor)|Mukesh, Thilakan, Innocent (actor)|Innocent, Jagathy Sreekumar and Sreeja in the lead roles. The movie was produced under the banner of Chaithram Cini Arts and was distributed by Charangat Release.

== Cast == Mukesh
* Thilakan Innocent
* Jagathy Sreekumar
* Mamukkoya
* Babu Namboothiri
* Sreeja
* Unnimary Shivaji
* Sreelatha Menon

== Crew ==
* Cinematography: Saloo George
* Editing: Rajashekar
* Makeup: Mohandas
* Costumes: Indrans
* Stunts: A. R. Pasha
* Advertisement: Gayathri
* Lab: Chithranjali
* Stills: Sreekumar
* Sound mixing: Krishnanunni
* Re-recording: Tharamgini
* Associate Director: Gandhikkuttan

== Songs ==
The songs for this movie were penned by Kaithapram Damodaran Namboothiri and was composed by Johnson (composer)|Johnson.

* "Thoovennilavu": G. Venugopal, Sujatha Mohan
* "Athikkulangara Melam": M. G. Sreekumar, Sujatha Mohan

==External links==
*  

 
 
 
 
 

 
 