Jaan Amar Jaan
{{Infobox film
| name           = Jaan Amar Jaan
| image          = Jaan Amar Jaan.jpg
| alt            =  
| caption        = VCD Cover
| director       = M B Manik|M. B. Manik
| producer       = Five Star Movies
| writer         = Kasim Ali Dolal
| starring       = Shakib Khan Apu Biswas Uzzal Suchorita  Maruf  Misha Soudagor
| music          = Ali Akram Subho
| cinematography = Assudinzamin Mazno
| editing        = Zinnath Hossion Zinnath
| studio         = 
| distributor    = Five Star Movies
| released       =   
| runtime        = 
| country        = Bangladesh
| language       = Bengali
| budget         = 
| gross          = 
}} romantic action Action Bangladeshi Bengali language film. The film is directed by M B Manik|M. B. Manik, produced by Five Star Movies, and features Shakib Khan and Apu Biswas in the lead roles. It became one of the top-grossing film of 2009 in Bangladesh.

==Cast==
* Shakib Khan
* Apu Biswas
* Uzzal
* Suchorita
* Maruf
*Misha Soudagor
* Suvrot
*Rebeka
*Polash
* Abu Sayid Khan
* Shipu
*Nice
* Faysal
* Ilias Kubra
* Gangua
* Azad Khan
* Sushant
* Salim
* Labu
* Gulzar Khan
* Afzal
* Noronjon
* Roni
* Babu
* Sourov
* Adil

==Crew==
* Producer: Five Star Movies
* Story: Kasim Ali Dolal
* Screenplay: Kasim Ali Dolal
* Director: M. B. Manik
* Script: Kasim Ali Dolal
* Dialogue: M. B. Manik
* Cinematography: Assudinzamin Mazno
* Editing: Zinnath Hossion Zinnath
* Music: Ali Akram Subho
* Choreography: Masum Babul
* Distributor: Five Star Movies

==Music==
{{Infobox album
| Name = Jaan Amar Jaan
| Type = Album
| Artist = Ali Akram Subho
| Cover = 
| Background = Gainsboro |
| Released = 2009 (Bangladesh)
| Genre = Soundtrack/Filmi
| Length =
| Label =
| Producer =Cd choich
| Reviews =
| Last album = 
| This album =  Jaan Amar Jaan  (2009)
| Next album = 
|}}
The films music was directed by famous Bangladeshi director Ali Akram Subho, and the all songs become hits.

===Soundtrack===
{| border="3" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Trac !! Song !! Singer’s !! Performer’s !! Note’s
|- 1
|Jaan amar jaan Monir Khan  and Samina Eismin Shakib Khan and Apu Biswas Title song
|- 2
|Pria Re Pria Re Haresi a Mon  Asif
| Shakib Khan
|
|- 3
|ridoey ridoey  S I Tutol and Konok Chapa Shakib Khan and Apu Biswas
|
|- 4
|O Akash Bole Samina Eismin
| Apu Biswas
|
|- 5
|Amar ai mon  Andrew Kishore  Shakib Khan and Apu Biswas
|
|-
 

==Box Office==
Jaan Amar Jaan was released in 2009 all over Bangladesh and become one of the top-grossing film of that year in Bangladesh.

==Home media==
Jaan Amar Jaan the films copyrights take Cd Vision and made as VCD, DVD and cassette also marketed-distributed in Bangladesh.

==References==
 

 
 
 
 
 
 
 
 


 
 
 