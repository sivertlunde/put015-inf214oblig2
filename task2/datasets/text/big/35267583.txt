The Rhythm of My Life: Ismael Sankara
{{Infobox film
| name           = The Rhythm Of My Life: Ismael Sankara
| image          =
| alt            = Official Poster for The Rhythm Of My Life: Ismael Sankara
| caption        = Official poster
| director       = Marc A. Tchicot Frank A. Onouviet
| producer       = Willy Conrad Asseko  Frank A. Onouviet Marc A. Tchicot
| writer         = Frank A. Onouviet Marc A. Tchicot
| starring       = Ismael Sankara Michael Mefane Rodney Ndong-Eyogo Hamed Mouliom
| music          = Michael Mefane Rodney Ndong-Eyogo
| cinematography = Frank A. Onouviet
| editing        = Frank A. Onouviet Marc A. Tchicot
| studio         = BVK Films Onome Nwa
| distributor    = 
| released       =  
| runtime        = 20 minutes  
| country        = Gabon
| language       = English
| budget         = 
| gross          = 
}}
 The Rhythm Of My Life: Ismael Sankara  is a short documentary film produced in Libreville, Gabon. It is written, directed and produced by Franck A. Onouviet and Marc A. Tchicot. The music is composed by Michael “Mike Mef” Mefane and Rodney “Hokube” Ndong-Eyogo. It focuses on musical moments between people  sharing a strong passion for their craft and an in depth and personal interview of rapper Ismael Sankara. The documentary is presenting a new recording artist with a passion going further than the name he is carrying.

==Synopsis==

"The Rhythm Of My Life: Ismael Sankara" follows in an unconventional way the journey of Ismael "Ish" Sankara, a former Miami based rapper, who traveled to Africa to visit family. Little did he know that Libreville (Gabon) would be the place where the project of his dreams would fall in his lap.

== Cast ==
* Ismael Sankara: Himself
* Michael "Mike Mef" Mefane: Himself
* Rodney "Hokube" Ndong-Eyogo: Himself
* Hamed Mouliom: Cab driver

== Release ==
The movie was first available for streaming on the Arte Creative website.    from March 25 to March 27, 2011. Then, it has been deleted  from the site and since has only been screened during film festivals.

== Festival run ==
Since its first diffusion, the short documentary had a worldwide festival run, including:

* 64th Cannes Film Festival (Short Film Corner of the Marché du Film )
* 15th Urbanworld Film Festival (Official Selection, Short Documentary category).   
* 5th Nashville Black Film Festival (Official Selection, Short Documentary category)
* Amiens International Film Festival (Retrospective of gabonese cinema).   
* 10th San Diego Black Film Festival (Official Selection, Short Documentary category)
* Cinemafrica Festival (Sweden).   
* 7th ÉCU The European Independent Film festival  (Audience Award, Non-European Short Documentary category)
* 5th Indie Spirit Film Festival (Official Selection, Short Documentary category)

==Notes and references==
 
 
* http://www.shadowandact.com/?p=42964
* http://www.jewanda-magazine.com/2011/04/cinema-the-rythm-of-my-life-ismael-sankara/
* http://www.westwax.com/home/2011/4/24/the-rhythm-of-my-life.html
* http://www.djobusyproductions.com/vu_therythmofmylife.php

== External links ==
*  
*  

 
 
 
 
 
 