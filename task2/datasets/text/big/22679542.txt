Electra, My Love
{{Infobox film
| name           = Electra, My Love
| image          = 
| caption        = 
| director       = Miklós Jancsó
| producer       = 
| writer         = László Gyurkó Gyula Hernádi
| starring       = Mari Törőcsik
| music          = 
| cinematography = János Kende
| editing        = Zoltán Farkas
| distributor    = 
| released       = 12 December 1974
| runtime        = 70 minutes
| country        = Hungary
| language       = Hungarian
| budget         = 
}}

Electra, My Love ( ) is a 1974 Hungarian drama film directed by Miklós Jancsó. It was entered into the 1975 Cannes Film Festival.    It is known for its extremely long takes, often as long as the camera would allow without stopping because of the film stock finishing.

==Cast==
* Mari Törőcsik as Elektra
* György Cserhalmi as Oresztész
* József Madaras as Aegisztosz
* Mária Bajcsay as Kikiáltó
* Lajos Balázsovits as Vezér
* József Bige
* Tamás Cseh
* György Delianisz
* Balázs Galkó
* Gabi Jobba as Krisotemis

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 