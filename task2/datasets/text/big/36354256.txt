Thunderbirds (1952 film)
{{Infobox film
| name           = Thunderbirds
| image          = Thbirpos.jpg
| image_size     =
| caption        = Original film poster
| director       = John H. Auer
| producer       =
| writer         = Kenneth Gamet (story)  Mary C. McCall, Jr. (screenplay)
| starring       = John Derek John Drew Barrymore
| music          = Victor Young
| cinematography = Reggie Lanning
| editing        = Richard L. Van Enger
| distributor    = Republic Pictures
| released       =  
| runtime        = 97 mins
| country        = United States
| language       = English
| budget         =
| gross          = $1 million (US) 
}} 45th Infantry Italian campaign of World War II. The film was made by Republic Pictures with sequences filmed at Fort Sill, Oklahoma. 

==Plot==
Close friends Gil Hackett and Tom McCreary both love and leave the same girl, Mary Caldwell, after they are called up to the Oklahoma National Guard and then on to the Army for wartime duty in 1940.

A tough, stoic sergeant named Logan keeps an eye on them as the unit ships out to Europe following the Japanese bombing of Pearl Harbor. Tom speaks of his father, who supposedly died a heros death in the first world war, but another soldier claims hes heard that Toms father was disgraced and dishonorably discharged.

Mary reveals that Tom is the one she loves. Gil finds solace in meeting Lt. Ellen Henderson, a nurse. The fighting continues in Italy and Sicily and when Tom ends up missing, Logan will not permit a search. Logan later heroically reports a movement of German tanks just before being shot.
 invasion of Southern France at hand, it is learned that Logan had been court-martialed during World War I for authorizing a search party that led to the death of more soldiers. He re-enlisted under a false name to prove himself again, particularly to Tom, who is actually his son.

==Cast==
* John Derek as Gil
* John Barrymore Jr. as Tom
* Ward Bond as Logan
* Mona Freeman as Ellen
* Eileen Christy as Mary
* Gene Evans as Braggart
* Barton MacLane  ...  Sgt. Durkee  
* Wally Cassell  ...  PFC Sam Jacobs  
* Ben Cooper  ...  Calvin Jones

==References==
 

==External links==
*  at IMDB

 

 
 
 
 
 
 
 