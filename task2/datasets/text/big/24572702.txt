Mike (film)
 
{{Infobox film
| name = Mike
| image =
| image_size =
| caption =
| director = Marshall Neilan
| producer =
| writer = Marion Jackson and Marshall Neilan
| narrator =
| starring = Sally ONeil William Haines
| music =
| cinematography =
| editing =
| distributor = Metro-Goldwyn-Mayer
| released = April 1926
| runtime = 70 minutes
| country = United States Silent English intertitles
| budget =
| preceded_by =
| followed_by =
}}

Mike is a 1926 film directed by Marshall Neilan. The film is a modest production, featuring Sally ONeil and William Haines.

==Plot==
Mike (Sally ONeil) is a boy of the railroads, living with his Father (Charles Murray) in a converted freight car, in love with telegraphist Harlan (William Haines).

==Cast==
* Sally ONeil - Mike (as Sally ONeill)
* William Haines - Harlan Charles Murray - Father (as Charlie Murray)
* Ned Sparks - Slinky
* Ford Sterling - Tad
* Frankie Darro - Boy
* Frank Coghlan Jr. - Boy (as Junior Coghlan)
* Muriel Frances Dana - Girl
* Sam De Grasse - Brush

==External links==
* 

 
 
 
 
 
 
 
 


 