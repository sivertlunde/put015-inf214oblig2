Badmaash Company
 
 

{{Infobox film
| name           = Badmaash Company
| image          = Badmash-Companywall.jpg
| caption        = Theatrical release poster
| director       = Parmeet Sethi
| producer       = Aditya Chopra
| story          = Parmeet Sethi
| screenplay     = Parmeet Sethi
| narrator       = Shahid Kapoor
| starring       = Shahid Kapoor Anushka Sharma Meiyang Chаng Vir Das
| music          = Songs: Pritam Background Score: Julius Packiam
| cinematography = Sanjay Kapoor
| editing        = Ritesh Soni
| studio         = Yash Raj Films
| distributor    = Yash Raj Films
| released       =  
| runtime        = 144 minutes
| country        = India
| language       = Hindi
| budget         =  
| gross          =     
}}
Badmaash Company (stylised as Badmaa$h Company;  , literally meaning "Rogue Company") is a 2010 Indian crime film|crime-comedy film directed by actor-turned-director Parmeet Sethi, produced by Aditya Chopra and starring Shahid Kapoor, Anushka Sharma, Meiyang Chаng and Vir Das in the lead roles. The film was released on 7 May 2010 under the banner of Yash Raj Films. It was average at the box office. The film is inspired from 21, Oceans Eleven, The Italian Job and Bluffmaster.

== Plot ==
In 1994 middle-class Bombay, three ordinary youngsters, Karan (Shahid Kapoor), Zing (Meiyang Chаng) and Chandu (Vir Das), graduate from college and start a business together. They meet Bulbul (Anushka Sharma), and she and Karan eventually fall in love. They name their venture "Friends and Company" and proceed to make a large fortune by smuggling expensive foreign shoes in from Bangkok and avoiding the import duty on them. When Karan is caught by his father putting away the black money, he explains the situation, including the fact that he is doing business with a smuggler. As a result, he is forced to leave the house and starts living with Bulbul and uses her to get contracts. After the government in India decides to reduce the import duty on foreign goods (including shoes) , the friends decide to go to United States|America. However, Karan becomes too greedy and the group begins to fall apart. Karan and Zing get into an argument about Zings excessive drinking; Zing leaves and opens his own bar. Bulbul leaves when she finds out that Karan has married another step woman for a green card; Chandu gets married and quits the company to start a video store.

Alone and heartbroken, Karan visits India and secretly attends a function where his father is being honored for his years of service to his company. Upon seeing this, he realizes his grave mistake. On his return to America, he is arrested and put in jail for 6 months. He is bailed out after Bulbul comes and gives up Zings, Chandus and her share from what they earned in the company. Karan starts working with his uncle. One day, he meets Bulbul, who is revealed to be pregnant with his child. The lovers reconcile, and Karan patches up with Zing, Chandu, and his family, while saving his uncle from a huge financial loss. "Friends and Company" is formed again but is turned into a public limited company. With his wife, son, friends, and having made his father proud, Karan is finally content with his life.

== Cast ==
*Shahid Kapoor as Karan Kapoor
*Anushka Sharma as Bulbul Singh(Karans wife)
*Vir Das as Chandu
*Meiyang Chаng as Tenzing aka Zing
*Pavan Malhotra as Mamu Jazz(Karans uncle)
*Anupam Kher as Sajjan Kapoor (Karans Father)
*Kiran Juneja as Maya Kapoor (Karans Mother)
*Jameel Khan as Archie Bhai
*Shalini Chandran as Anu Kapoor (Karans Sister)
*Alexandra Vino as Linda
*John Marenjo as Charlie
*Obaid Kadwani as the Lawyer

==Production==
Parmeet Sethi wrote the script for the film with dialogues in only six days. The four main characters are all based on real-life people. Parmeet reveals that he got tired of television and was keen on pursuing film direction.  Filming locations for the film included New York, Atlantic City, Philadelphia, Bangkok, Mumbai, and Hyderabad, India|Hyderabad.

== Critical reception ==
The film received mixed reviews from critics. Taran Adarsh of Bollywood Hungama gave it a rating of 3 out of 5, saying; "On the whole, the movie is a watchable experience for various reasons, the prime reason being it offers solid entertainment, but doesnt insult your intelligence."  Rajeev Masand of CNN-IBN gave the movie 1.5 out of 5 and claimed it to be outrageously silly.  Gaurav Malani of Indiatimes gave the film 3.5 out of 5 saying "Badmaash Company is a good entertainer. Worth a watch!" and praising Shahid Kapoor.  Komal Nahta gave the film 2.5 out of 5 praising the performance of Shahid Kapoor and calling Badmaash Company an entertainer.  Sukanya Verma of rediff gave the film 2 out of 5 stars saying Sethis directorial debut starts out with cocksure confidence and zing. 

Nikhat Kazmi of the Times of India gave the film 3 out of 5 stars and said, "Indeed, Badmaash Company does have a bunch of riveting scenes, although the story does follow a very predictable line of crime and punishment/repentance."  Daily News and Analysis (DNA) gave the film 2.5 out of 5 saying, "This company is worth keeping."  Anupama Chopra of NDTV called it a "staggeringly tedious film" while Raja Sen of Rediff said, "Theres not a single scene in the film that actually works". Mayank Shekhar of the Hindustan Times criticized the film as half-written; he only liked the film until the interval and gave it 2 out of 5.  The film received an aggragate rating of 4/10 at ReviewGang. 

==Box office==
The film had a decent opening on its first weekend despite competition from Housefull (2010 film)|Housefull. The film made a drop in overseas collections, but it did not have much effect on its domestic collections. By the end of its first week, the film grossed Rs. 317.5&nbsp;million. The film collected Rs. 430&nbsp;million  at the end of its theatrical run and was declared an above average grosser at the box office. 

==Awards and nominations== 2011 Zee Cine Awards
Nominated  Best Male Debut – Meiyang Chang and Vir Das Best Debutante Director – Parmeet Sethi

WINNER: Best Male Debut – Meiyang Chang

==Soundtrack==
{{Infobox album
| Name        = Badmaash Company
| Cover       = 
| Type        = soundtrack
| Artist      = Pritam
| Released    =  
| Recorded    = 
| Genre       = Film soundtrack
| Length      = 30:43 YRF Music Last album = Atithi Tum Kab Jaoge? (2010) This album = Badmaash Company (2010) Next album = Raajneeti (2010)
}}
The soundtrack of the film is composed by Pritam while the lyrics are penned by Anvita Dutt Guptan and Julius Packiam has scored the background music. 

{{Track listing
| extra_column = Artist(s) KK || length1= 04:24
| title2 = Jingle Jingle | extra2 = Mohit Chauhan, Master Saleem, Monali Thakur || length2= 04:27
| title3 = Chaska | extra3 = Krishna Beura || length3= 05:14
| title4 = Fakeera | extra4 = Rahat Fateh Ali Khan || length4= 04:38
| title5 = Badmaash Company | extra5 = Benny Dayal || length5= 03:58
| title6 = Ayaashi | note6 = Remix | extra6 = KK || length6= 04:12 
| title7 = Chaska | note7 = Remix | extra7 = Krishna Beura ||length7= 03:49
}}

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 