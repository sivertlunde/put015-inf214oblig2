Flying Dagger
 
 
 
{{Infobox film name = Flying Dagger image = Flying Dagger (1993 film) DVD boxart.jpg alt =  caption = DVD cover art traditional = 神經刀與飛天貓 simplified = 神经刀与飞天猫 pinyin = Shénjīng Dāo Yǔ Fēitiān Māo jyutping = San4 Ging1 Dou1 Jyu2 Fei1 Tin1 Maau1}} director = Chu Yen-ping producer = Hui Pooi-yung writer = Wong Jing starring = Tony Leung Sharla Cheung Jacky Cheung Maggie Cheung Jimmy Lin Gloria Yip Ng Man-tat Chan Hung-lit music = Chan Daai-lik Foo Laap cinematography = Chan Wing-shu editing = Ma Chung-yiu studio = Chang Hong Channel Film & Video Ltd. Co. (HK) distributor = SYS Entertainment released =   runtime = 86 minutes country = Hong Kong language = Cantonese budget =  gross = 
}}
Flying Dagger is a 1993 wuxia comedy film directed by Chu Yen-ping and written and produced by Wong Jing.  The film features a large cast of stars and parodies numerous Hong Kong films.

==Plot==
The Hon   Brothers, Chung and Lam, who are in fact uncle and cousin, are rival bounty hunters to the Fung   Sisters, and take glee in thwarting each other because Chung refused to marry Lady Fung. Fung Ling and Lam are also in love, but do not acknowledge it.

Emperor Tsao hires the Hons to capture the Nine-Tails Fox, whom he claims has made a major robbery of his household, including the rape and murder of his daughter and the murder of 41 servants. The Hons are distracted when they find the Fungs captured by Never Die and his brothers, who in spite of decapitation and loss of a hand, proceed to chase them (the severed hand grabbing at one of them) chase the Hons to the lair of Nine-Tails Fox, who has recently caused his wife, the apsara known as Flying Cat to walk out on him. The group discovers that Never Die is allergic to glib talk, and begin speaking to each other glibly, Chung, too embarrassed to speak to Lady Fung in that way, speaking to Nine-Tails Fox, and the two pulling off each other shirts, and trap him. Never Die soon dissolves, and the four bounty hunters and Never Dies hand take him away. Flying Cat returns to find drawn images of what has gone on previously, including what she interprets as her husbands homosexual behavior, and chastises the old man who sends the drawings out through a slot for not intervening, which he says is not his place.

Nine-Tails Fox, who proudly admits to being a thief but insists he never hurts anyone, escapes from the bounty hunters to the inn of Pang Tin-hong, where he becomes involved with numerous courtesans. The bounty hunters follow him, but not before Flying Cat catches up with him. Still other bounty hunters, including the burly Western Ace, a gay man who sings 1980s American pop tunes, and a transsexual who delivers poisoned kisses, arrive at the inn. Because the Hons and Fungs pay well and are supported by the police, Pang sides with them against the new bounty hunters, claiming that he and his wife are Leslie Cheung and Anita Mui. The transsexual kisses Chung, causing him to turn green and infect him with poison. After the other bounty hunters are defeated, Pang sends Lam and Ling outside, insisting that they are under eighteen, although both claim to be eighteen, in order to explain that the cure for the poison is to have sex nine times, then eat the strange fetus that will be born three days later. Chung tries to take Lady Fung to bed, but inadvertently passes the disease to Pang, whose wife and he fight a lot and never want it at the same time.

While Ling and Lam are outside, Never Dies hand grabs Lings shoulder, and she thinks it is Lams and rests her head on his shoulder. Thus distracted, Tsao captures the two of them, and the thieves and the bounty hunters team up against their common enemy and rescue the young ones.

At the end of the film, Pangs wife gives birth to the strange fetus, and the Hons, Fungs, Fox, and Cat seemingly forgetting about the idea that it is to be eaten and ready and excited to receive it as if a normal birth, are horrified to see that the baby is in fact Never Die.

==Cast==
*Tony Leung Ka-fai as Big Dagger Hon Cheung
*Jacky Cheung as Nine Tail Fox
*Jimmy Lin as Little Dagger Hon Lam
*Sharla Cheung as Big Bewitchment
*Maggie Cheung as Flying Cat
*Ng Man-tat as Pang Ting-hong (Innkeeper)
*Gloria Yip as Little Bewitchment
*Chan Hung-lit as Dicky Lui
*Kingdom Yuen as Brothel keeper
*Yuen Cheung-yan as Never Die
*Lee Ka-ting as Erotomania man
*Lo Lieh as Must Die Pauline Chan as Evil Lady of Yi Ho David Wu as Western Ace
*Ku Pao-ming
*Fang Fang
*Sam Hoh
*Wong Chung-kui

==Music== David Newmans Heathers, among others.

==External links==
* 
* 
*  on Hong Kong Cinemagic

 

 
 
 
 
 
 
 
 
 