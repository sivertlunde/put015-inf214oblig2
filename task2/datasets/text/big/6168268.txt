Smile (1975 film)
{{Infobox film
| name           = Smile
| image          = Smile (1975 film).jpg
| image_size     =
| caption        = Original Theatrical Poster Michael Ritchie
| producer       = Michael Ritchie
| screenplay     = Jerry Belson
| narrator       = Geoffrey Lewis Eric Shea
| music          = Dan Orsborn
| cinematography = Conrad L. Hall
| editing        = Richard A. Harris
| distributor    = United Artists
| released       =  
| runtime        = 117 minutes
| country        = United States English
| budget         =
}} 1975 satire|satirical Michael Ritchie with a screenplay by Jerry Belson about a beauty pageant in Santa Rosa, California.

It stars Bruce Dern and Barbara Feldon and introduced a number of young actresses who later went on to larger roles, such as Melanie Griffith. The film satirizes small-town America and its peculiarities, hypocrisies and artifice within and around the pageant.
 1986 Broadway musical with songs by Marvin Hamlisch and Howard Ashman.

==Plot Synopsis==

The plot revolves around the contestants and people involved with the California pageant of the fictional Young American Miss Pageant, held in Santa Rosa, California.

Big Bob Freelander (Dern), the head judge, is a used car dealer. Brenda DiCarlo (Feldon), is the pageants Executive Director, and her husband Andy (Nicholas Pryor) is an alcoholic.

In separate subplots, the film focuses on Andys unhappiness, as he is about to be inducted into a fraternal society, which requires a humiliating ritual, Little Bob (Shea), Big Bobs son, who conspires with his friends to photograph the contestants in various states of undress, and the activities of the contestants themselves.

Wilson Shears (Lewis), the pageant producer, clashes with a choreographer brought in from Hollywood, Tommy French (Kidd), who is cynical and blunt.

Andy refuses to go along with the induction ceremony, which involves kissing the behind of a dead chicken. He shoots his wife, who is wounded, and is jailed. But she refuses to press charges and he is released, and Big Bob tries to convince him to not move from town.

The show becomes more expensive than was anticipated, and Shears pressures French to remove a ramp, because it is taking up seating. This results in an injury to a contestant, and French agrees to reinstate the ramp and to make up the difference out of his fee.

The pageant concludes successfully, though the contestants that have been the focus of the films attention do not win.

==Cast==
*Bruce Dern ... Big Bob
*Barbara Feldon ... Brenda
*Michael Kidd ... Tommy Geoffrey Lewis ... Wilson
*Eric Shea ... Little Bob
*Nicholas Pryor ... Andy
*Titos Vandis ... Emile 
*Paul Benedict ... Orren Brooks
*William Traylor ... Ray Brandy
*Dennis Dugan ... Logan
*Kate Sarchet ... Judy - Young American Miss
*Joan Prather ... Robin - Young American Miss
*Denise Nickerson ... Shirley - Young American Miss 
*Melanie Griffith ... Karen - Young American Miss 
*Annette OToole ... Doria - Young American Miss
*Maria OBrien ... Maria - Young American Miss 
*Colleen Camp ... Connie - Young American Miss 
*Caroline Williams ... Helga - Young American Miss 
*Shawn Christianson ... Young American Miss / The Winner

==Production==
The movie was filmed on location in and around Santa Rosa, with the pageant held at Veterans Memorial Auditorium.

==Reception==
Smile was well received upon release, with praise for the humour, satire and performances. Vincent Canby of The New York Times declared Jerry Belsons screenplay  excellent  and that,  Smile, which is Mr. Ritchies best film to date (better than both Downhill Racer and The Candidate), questions the quality of our fun, while adding to it .  Roger Ebert of The Chicago Sun-Times gave the film 3 out of 4 stars, saying that though  Ritchie has so messy targets that he misses some and never quite gets back to others, the film still does a good job of working over the hypocrisy and sexism of a typical beauty pageant. . 

The film holds a 100% fresh rating on review aggregate Rotten Tomatoes. 

==See also==
* Nashville (film)|Nashville - Another satirical American film, released the same year, directed by Robert Altman. This film looks at the lives of several individuals leading up to and around a local concert.

==References==
 

==External links==
* 
*  at Rotten Tomatoes
* 

 

 
 
 
 
 
 
 
 
 
 