Kingdom Come (2001 film)
{{Infobox film
| name           = Kingdom Come
| image          = Kingdom come ver1.jpg
| caption        = Theatrical release poster
| director       = Doug McHenry Edward Bates John Morrissey
| writer         = David Dean Bottrell Jessie Jones
| narrator       =
| starring       = LL Cool J Jada Pinkett Smith Vivica A. Fox Anthony Anderson Toni Braxton Loretta Devine Whoopi Goldberg
| music          = Tyler Bates John E. Rhone
| music supv.    = Derrick L. Wade
| cinematography = Francis Kenny
| editing        = Richard Halsey
| distributor    = Fox Searchlight Pictures
| released       =  
| runtime        = 95 minutes
| country        = United States English
| budget         = $7 million
| gross          = $23,396,049
}} 2001 comedy drama film, directed by Doug McHenry.  This film stars LL Cool J, Jada Pinkett Smith, Vivica A. Fox, Anthony Anderson and Whoopi Goldberg.

==Plot==
===Summary===
Kingdom Come is a story of a family called the Slocumbs, living out in the country, who must come together after the death of a family member, whom no one seems to remember with much fondness. It is based on the Off-Broadway play Dearly Departed.

First, theres Woodrow "Bud" Slocumb, the man in question, whose wife, Raynelle ( ) has blown all of his money on a failed invention, and his loud-mouthed wife, Charisse (Jada Pinkett Smith), is no help; she hits the roof after his infidelity and reminds him often that she could have been married to his rich lawyer cousin, who, it is later revealed, left his own wife Juanita (Toni Braxton). Then, theres Marguerite (Loretta Devine), a pious, overbearing mom who usually calls her son, Royce (Darius McCrary), a "Demon Seed"; she fears that he will end up in jail like his brother, and the latter is an unemployed worker who is irritated by his mothers unsolicited and shrill advice on how to live his life.

===Story Plotline===
The film begin with the a radio morning announcement by Rev. Beverly H. Hooker (Cedric the Entertainer). Raynelle receives and reads her sister Marguerites letter (much to Buds annoyance), which says that she was expecting Bud to attend Sunday service, but assumed that he decided to watch wrestling again like hed always did, but wonders that there was going to be television in hell. She had also heard about Juniors and Charisses business going bad, and wonders that would he ever learn. She thinks that both her nephew and her own son could learn from Buster Kincaids child, Tiny. She had him over the weekend, and he did some chores around the house; as a reward, she gave him three dollars for his birthday the following week instead of the actual two dollars. Moreover, she says that she has decided to come over to visit after church; she believes that one of them needs to get things right with the Lord, and she is the woman for the job. She decides that they are going to spend of whole day in prayer, discussing scripture, and singing hymns until they wear her down, like it says in one scripture, "One day is with the Lord as a thousand years." After Raynelle finishes reading, Bud, who both agrees and disagrees with some things said in the letter, collapses and dies, with the former noticing.

Later, Royce is awaken by a loud ring of his phone. The one calling is his mother, who starts singing too pitchy, getting him annoyed that he hangs up. After he answers again, he calls her off for waking him up, in which she replies that she says that hes taking the Lords name in vain. Royce talks harshly to his mother, but she talks back and asks why he is still in his bed. She tells him that its 7:00 in the morning and why he isnt up looking for a job, but he replies that its not that at all. She assumes that he has a love hangover and begins accusing him of laying with a harlot, in which she starts slapping her phone to get back at him. He tells her to stop that and asks what does she want. She though that he want to know that his uncle Bud has passed this morning. He seems to be compassionate, saying that he is sorry to hear that and asking how his aunt Raynelle was doing. She replies that his aunt is not doing well, as his uncle dropped dead at a breakfast table in the middle of her letter. He then pretends to be sympathetic about the cause of his uncles death. She says that they are going to their house and giving them comfort and some Christian counsel.

==Cast==
* LL Cool J as Ray Bud Slocumb
* Jada Pinkett Smith as Charisse Slocumb
* Vivica A. Fox as Lucille Slocumb
* Loretta Devine as Marguerite Slocumb
* Anthony Anderson as Junior Slocumb
* Toni Braxton as Juanita Slocumb
* Cedric the Entertainer as Rev. Beverly H. Hooker
* Darius McCrary as Royce Slocumb
* Whoopi Goldberg as Raynelle Slocumb

==Awards and nominations==
  
* In 2002, received a NAACP Image Award nomination for "Outstanding Actress" for Whoopi Goldberg; "Outstanding Supporting Actor" for Cedric the Entertainer; "Outstanding Supporting Actress" for Loretta Devine; and "Outstanding Supporting Actress" for Vivica A. Fox.

==Soundtrack==
  
* "Kingdom Come" - Written by Kirk Franklin
Performed by Kirk Franklin and Jill Scott
Produced by Kirk Franklin for Fo Yo Soul Productions/GospoCentric Records|B-Rite Music
Kirk Franklin appears courtesy of GospoCentric Records
Jill Scott appears courtesy of Hidden Beach Recordings
 Joseph Knapp
Performed by Loretta Devine

* "Its Alright" - Written by Kirk Franklin
Performed by  
Produced by Kirk Franklin for Fo Yo Soul Productions/B-Rite Music
Trin-i-tee 5:7 appears courtesy of B-Rite Music

* "Someday" - Written by Kirk Franklin
Performed by Crystal Lewis
Produced by Kirk Franklin for Fo Yo Soul Productions/B-Rite Music Metro One Records

* "Help Lord (Wont You Come)" - Written by Stanley Burrell, Ontario Haynes, Maurice Stewart and John Rhone
Performed by M.C. Hammer  Giant Records
By Arrangement with Warner Special Products
Produced by The Whole 9
 Just a Closer Walk With Thee" - Traditional Bobby Jones and the Nashville Super Choir
Courtesy of GospoCentric Records

* "Every Woman" - Written by Kirk Franklin
Performed by Az Yet
Produced by Kirk Franklin for Fo Yo Soul Productions/B-Rite Music

* "Stand" - Written by Kirk Franklin
Performed by Shawn Stockman
Produced by Kirk Franklin for Fo Yo Soul Productions/B-Rite Music
Shawn Stockman appears courtesy of Universal Records

* "Gods Got It All In Control" - Written by Kurt Carr
Performed by Kurt Carr and friends featuring Tamela Mann from The Family
Produced by Kurt Carr for Kurt Carr Productions/GospoCentric Records
Kurt Carr, Yvette Williams and Tamela Mann appear courtesy of GospoCentric Records

* "Just a Closer Walk With Thee" - Traditional

* "It Is Well With My Soul" - Written by Horatio Spafford and Philip Bliss

* "Amazing Grace" - Written by John Newton

* "Thy Will Be Done" - Written by Kirk Franklin
Performed by Deborah Cox
Produced by Kirk Franklin for Fo Yo Soul Productions/B-Rite Music
Deborah Cox appears courtesy of J Records LLC

* "Daddys Song" - Written by Kirk Franklin Carl Thomas, Natalie Wilson and SOP
Produced by Kirk Franklin for Fo Yo Soul Productions/B-Rite Music
Carl Thomas appears courtesy of Bad Boy Records
Natalie Wilson and SOP appear courtesy of GospoCentric Records

* "Try Me" - Written by Kirk Franklin
Performed by the cast with Ashley Guilbert, Shanika Leeks and Candy West

* "Try Me" - Written by Kirk Franklin
Performed by Tamar Braxton and One Nation Crew
Produced by Kirk Franklin for Fo Yo Soul Productions/B-Rite Music
Tamar Braxton appears courtesy of DreamWorks Records
One Nation Crew appears courtesy of B-Rite Music

* "Thank You" - Written by Kirk Franklin
Performed by Kirk Franklin and Mary Mary
Produced by Kirk Franklin for Fo Yo Soul Productions/B-Rite Music
Mary Mary appears courtesy of Columbia Records
Kirk Franklin appears courtesy of GospoCentric Records

 

== References ==
 

==External links==
*  

 
 
 
 
 