Hostage (film)
 
{{Infobox film
| name           = Hostage
| image          = Hostage poster.JPG
| caption        = Promotional poster
| alt            =
| director       = Florent Emilio Siri
| producer       =  
| writer         = Doug Richardson Robert Crais  
| starring       =  
| music          = Alexandre Desplat
| cinematography = Giovanni Fiore Coltellacci
| editing        = Richard Byard  Olivier Gajan Equity Pictures
| distributor    = Miramax Films
| released       =  
| runtime        = 113 minutes
| country        = United States
| language       = English
| budget         = $75 million 
| gross          = $77,944,725 
}} novel by Robert Crais, and was adapted for the screen by Doug Richardson.
 West Coast.

==Plot== Los Angeles. Talley is negotiating with a man who has taken his wife and son hostage after learning she has been cheating on him. Talley denies a SWAT commanders request to give snipers the green light to take out the suspect. There are three gunshots in the house. Talley runs through the barricaded door and finds the man and his wife dead. In the boys room he finds the son, who passes away in Talleys arms. This leaves Talley emotionally scarred. He moves with his family to become police chief in Bristo Camino, a peaceful suburban hamlet in Ventura County, California.

A year after the incident, Talley finds himself in another hostage situation. Two teenagers, Dennis and his brother Kevin, and their mysterious accomplice Marshall "Mars" Krupcheck take hostage Walter Smith and his two children, a teenager Jennifer and a young child Tommy, in Smiths house after a failed robbery attempt. The first officer to respond, Carol Flores, is brutally shot twice by Mars just before Talley and a fellow officer arrive. Talley attempts to rescue the seriously injured officer, who dies in front of him. Traumatized and unwilling to put himself through yet another life-or-death situation, Talley hands authority over to the Ventura County Sheriffs Department and leaves.
 laundering money for a mysterious criminal syndicate through offshore shell corporations. He was preparing to turn over a batch of important encrypted files (recorded on a DVD) when he was taken hostage. To protect such incriminating evidence from discovery, the syndicate orders someone known only as the Watchman to kidnap Talleys wife and daughter. Talley is told to return to the hostage scene and stall for time until the organization can launch its own attack against Smiths house.

Dennis has his partners Mars and Kevin tie up the kids. Dennis pistol-whips Walter, knocking him out, then finds a large amount of cash. In an attempt to end the standoff (and secure the DVDs himself), Talley meets with Dennis and agrees to provide a helicopter in exchange for half the money. When the helicopter arrives, Dennis and Kevin bring the money to Talley in the courtyard and prepare to leave, but Mars refuses to leave without Jennifer. Talley tries to get the boys to leave Jennifer and walk away, but he says that the helicopter will only carry three additional people, and insists that  Jennifer stay behind. The deal breaks down as the boys return to the house. Talley tells the chopper to stand down and he exits the courtyard.

Kevin is upset that his older brother picked Mars over him and confronts Dennis, demanding he make a decision: its either him or Mars.  Dennis picks the money and Kevin is even more upset so he grabs the bags full of money and empties them onto the floor, so Dennis punches him. Thomas escapes, grabs his fathers gun and talks to Talley on Jennifers cell phone.

Talley learns that Mars is a killer, who could turn on the hostages and his own accomplices at any moment. Mars does, in fact, kill Kevin, just when Kevin is about to release the kids to the police. Dennis comes to Kevins side and assumes the cops killed him. Mars then shoots Dennis. The brothers die in each others arms.

The syndicate sends fake FBI agents to recover the DVD and they storm the house. Mars is stabbed in the cheek by Jennifer. She and her brother flee. They lock themselves in the houses panic room. Talley hears the children screaming as they flee.

Mars throws a Molotov Cocktail at Talley, destroying his patrol car. Mars begins to kill the majority of the fake FBI agents using his pistol and multiple Molotov Cocktails, but is shot in the side by the only surviving agent. The agent tracks down Talley and the children, demanding to be given the encrypted DVD. Talley gives him the DVD. Mars reappears, distracting the agent long enough to be killed by Talley.

Mars prepares to throw the last bomb and kill everyone in the room. He collapses to his knees, weakened by the wounds in his torso and blood loss. He makes eye contact with Jennifer, with whom he was apparently infatuated, then drops the Molotov Cocktail. Mars dies, setting himself on fire and vanishing in a pillar of flame.

Talley evacuates the hostages. He and Walter Smith then go to the inn where Talleys wife and daughter are being held hostage by the Watchman and other masked men. Smith, set free by Talley and grateful for his own family being saved, shoots the Watchman in the head. This allows Talley to kill the other masked gunmen. Talleys family is safe.

==Cast==
* Bruce Willis as Police Chief Jeff Talley
* Kevin Pollak as Walter Smith
* Jimmy Bennett as Tommy Smith
* Michelle Horn as Jennifer Smith
* Ben Foster as Marshall "Mars" Krupcheck
* Jonathan Tucker as Dennis Kelly
* Marshall Allman as Kevin Kelly
* Serena Scott Thomas as Jane Talley
* Rumer Willis as Amanda Talley
* Kim Coates as The Watchman
* Robert Knepper as Wil Bechler
* Tina Lifford as Deputy Sheriff Laura Shoemaker
* Ransford Doherty as Officer Mike Anders
* Marjean Holden as Officer Carol Flores
* Michael D. Roberts as Bob Ridley
* Art LaFleur as Bill Jorgenson
* Keith Hines as Simmons
* Randy McPherson as Kovak
* Hector Luis Bustamante as Officer Ruiz
* Kathryn Joosten as Louise Johnny Messner as Mr. Jones
* Glenn Morshower as Lt. Leifitz
* Jamie McShane as Joe Mack
* Jimmy Pinchak as Sean Mack
* Chad Smith as Bobby Knox

==Production== Malibu area (in western Los Angeles County). The exterior views of Smiths lavishly appointed house were filmed at a real house in the unincorporated Topanga Canyon area, between Malibu and Los Angeles; the interior scenes were done on sound stages in Hollywood, California|Hollywood. 
 Bay Area Mars by Robert Crais after a friend Dennis Bsharah urged him to look into the horrorcore genre. In the movie adaptation, Foster strongly resembles the rapper.     Jonathan Tuckers name was later changed to Dennis.
 Boyle Heights neighborhood of East Los Angeles, just east of downtown.   
 Ojai or Moorpark, California|Moorpark. Bristo Bay is the name of Bristo Camino in the original 2001 Robert Crais Hostage (novel)|novel.   

==Reception==

===Critical response   ===
 

The film received mixed reviews from critics. Rotten Tomatoes gave the film a score of 36% based on 152 reviews. {{cite web
| title = Hostage (2005)
| url = http://www.rottentomatoes.com/m/1141099-hostage/
| publisher = Rotten Tomatoes
| accessdate = 2010-05-30
}}
 
Roger Ebert gave the film three stars out of four. Ebert, Roger (2005-03-11).  . 
Popular movie database, IMDB has given a rating of 6.6/10 for the film


===Box office===
The film earned $34,639,939 at the box office in the United States and a total international gross of $77,944,725.   

==Home media   ==
Hostage was released on DVD June 21, 2005. 

==See also==
 
* The Aggression Scale—a film with a similar premise
* List of films featuring home invasions

==References==
 

==External links==
*  
*  
*  
*  

 

 

 
 
 
 
 
 
 
 
 
 
 