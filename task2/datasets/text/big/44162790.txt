Ahangaaram
{{Infobox film
| name           = Ahangaaram
| image          =
| caption        =
| director       = D Sasi
| producer       =
| writer         =
| screenplay     =
| starring       = MG Soman Santhakumari Ravi Menon Prabhu
| music          = Maharaja
| cinematography = Vijayendra
| editing        = K Sankunni
| studio         = Shalimar Films
| distributor    = Shalimar Films
| released       =  
| country        = India Malayalam
}}
 1983 Cinema Indian Malayalam Malayalam film, directed by D Sasi. The film stars MG Soman, Santhakumari, Ravi Menon and Prabhu in lead roles. The film had musical score by Maharaja.   

==Cast==
 
*MG Soman
*Santhakumari
*Ravi Menon
*Prabhu
*Master Suresh
*Kaduvakulam Antony
*Aranmula Ponnamma
*Rajalakshmi
*Jose Prakash
*Thikkurissi Sukumaran Nair
*Jagathy Sreekumar
*Sathyakala
 

==Soundtrack==
The music was composed by Maharaja and lyrics was written by Bichu Thirumala. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Arayaal thaliril || Jayamma || Bichu Thirumala || 
|-
| 2 || Arayaalthaliril || K. J. Yesudas || Bichu Thirumala || 
|-
| 3 || Brahmaasthrangal || K. J. Yesudas || Bichu Thirumala || 
|-
| 4 || Chilankakale kadha parayu || Vani Jairam || Bichu Thirumala || 
|}

==References==
 

==External links==
*  

 
 
 

 