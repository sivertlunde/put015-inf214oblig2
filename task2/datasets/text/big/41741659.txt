Game (2002 film)
{{Infobox film
| name           = Game
| image          = 
| caption        = 
| director       = John Amritraj
| producer       = S. Soundara Pandi
| screenplay     = 
| story          = Karthik Radha
| music          = S. P. Venkatesh
| cinematography = D. Dhamu
| editing        = Krishna
| studio         = Srimasani Amman Pictures
| distributor    =
| released       =  
| country        = India
| runtime        = 
| language       = Tamil
}}
 2002 Cinema Indian Tamil Tamil drama Karthik and Radha in lead roles with Rajashree in a pivotal role. The film, produced by Srimasani Amman Pictures, had musical score by S. P. Venkatesh and was released after several delays in November 2002 to negative reviews. 

==Cast==
  Karthik as Raja
*Vinod Kumar as Joseph IPS
*Manoj K. Jayan as Lenin
*Radha as Jaya Urvashi as Janaki Rajashree as Malar
*R. Sundarrajan
*John Amritraj as GK
*R. Sundarrajan (director)|R. Sundarrajan
*Major Sundarrajan Pandu
*Kazan Khan
*Vijay Krishnaraj Rani
*Divya Dutta
*Manikraj
*Kullamani
*Vincent Roy
*Thideer Kannaiah
*Mahanadi Shankar
*Manager Cheen
 

==Production==
John Amritraj had originally planned to make a film with Karthik in the late 1990s as Koottali which also had Divya Dutta in the cast, though delays meant that the films budget and cast were subsequently affected.  For the film, Karthik participated in a song sequence shot with 100 car lights, sung by Anuradha Sreeram and performed by Rani (Tamil actress)|Rani. The film was based on a real life event. 

==References==
 

 
 
 
 


 