Daddy's Double
 
 
{{Infobox film
| name           = Daddys Double
| image          = File:Daddys Double scene.jpg
| caption        = At the Parsons house
| director       =
| producer       = Thanhouser Company
| writer         =
| narrator       =
| starring       =
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        =
| country        = United States English intertitles
}} silent short short drama produced by the Thanhouser Film Corporation. The film focuses on Hal, a young man, who proposes to Sue, his love interest. Her father overhears and becomes furious before whisking his daughter away to a boarding school. Hal and Sue attempt to elope, but it is foiled. Hal then gets an idea to disguise himself as the father and reclaim Sue. The plan works, but the father follows in pursuit. He breaks into the wedding ceremony at the Parsons house and sees his double. He then laughs and approves of the marriage. The film was the fourth release by Thanhouser and it was met with favorable reviews by critics. In 2007, a surviving print was released on DVD with a new original score composed and performed by Raymond A. Brubacher.

== Plot ==
At the Posts home, Hal Dunton sits down with his love interest Sue Post, cautiously checking to see if they are alone. Hal then proposes to Sue and in the excitement, her father comes into the room. He is outraged and believes his daughter is too young to be married and promptly makes Hal leave before scolding his daughter. Before departing, Hal meets and pleads with the father as he takes the daughter away by car. The father refuses and Hal is knocked down before the father and daughter depart to a boarding school. The father meets with the mistress and leaves Sue in her care, much to Sues distress. 

Whisked away from her home and her beloved, Sue is in despair until Hal appears outside her window with a ladder. The attempted elopement fails when the mistress catches Hal in the act and dismisses him. She awaits Sue to descend and round the corner before halting her and forcing her back inside. The father pulls up to a place of business and Hal, who is walking by, attempts to greet the father. The angry father ignores him, but Hal turns and sees the wigmakers store next to him. An idea strikes him and he pays a photographer to get a photo of the father on the way out, which further angers him. Hal then sends a message to Sue, that he will come and claim her the following day, disguised as her father. 

Hal head to the wigmaker and is disguised as the father. So well is the disguise that the fathers chauffeur is convinced and takes Hal to the boarding school. The father quickly exits in time to see his chauffeur drive away and hails a taxi. Hal arrives at the boarding school and the disguise works, claims Sue and drives away. Seconds later the father arrives and confronts the mistress, departing to pursue in a fit of anger. Hal and Sue go to the Parsons house to be immediately wed. The father is delayed for a short time by car trouble, but quickly arrives before the two are wed. He bursts in and sees his double and both he and Hal laugh. The father relents and approves the marriage as Hal removes his disguise in front of the stunned Parson.   

== Cast ==
* Frank H. Crane as Mr. Post or "Daddy" 
* Fred Santley as Hal Dunton or "Daddys double" 
* Isabelle Daintry as Sue Post 

== Production == Romeo and Juliet. Lloyd B. Carleton was the stage name of Carleton B. Little, a director who would stay with the Thanhouser Company for a short time, moving to Biograph Company by the summer of 1910.    Film historian Q. David Bowers does not attribute either as the director for this particular production, but he does credit Blair Smith as the cameraman.  The film is composed of 28 shots, 8 titles and 1 Insert (filmmaking)|insert. 

The cast for the film featured Thanhousers first leading male actor, Frank H. Crane, along with two stage actors, Isabelle Daintry and Fred Santley.   Isabelle Daintry has one other known Thanhouser credit, but this marks Fred Santleys only known credit.   During this era, the players in the film were not credited and anonymity was the rule. 

==Release and reception==
The one reel drama, approximately 960 feet, was released on Tuesday April 5, 1910.  The film would also be released in the United Kingdom on October 16, 1910. The surviving feel reel says the Gaumont Company of London (Gaumont British) had exclusive rights to Thanhouser films outside of the United States.  The film was shown across the United States, including theaters in Kansas,  New York,  Pennsylvania,  Texas,  and Indiana.  One of the last advertisements for the film came three years after its release by the Neosho Picture Show Company of Neosho, Missouri. 

The film received positive attention from film critics. The Morning Telegraph praised it as the best work from the new Thanhouser Company. The The New York Dramatic Mirror found it to have good acting which could still be more expressive, but was more indifferent to the use of unnecessary connecting scenes. The Moving Picture World stated, in a brief review, that the acting and photography was good. 
 negative and a 35mm reference positive. 

==References==
 

==External links==
*  

 
 
 
 
 
 