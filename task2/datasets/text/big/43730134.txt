Sathi (1938 film)
 
 
{{Infobox film
| name           = Sathi
| image          = 
| image_size     = 
| caption        = 
| director       = Phani Majumdar
| producer       = New Theatres
| writer         = 
| narrator       = 
| starring       = K. L. Saigal Kanan Devi Boken Chatto Shyam Laha
| music          = R. C. Boral
| cinematography = Dilip Gupta Sudish Ghatak
| editing        = 
| distributor    =
| studio         = New Theatres
| released       = 1938
| runtime        = 137 min
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}} Bengali film Street Singer and the film was the debut of Phani Majumdar as a director.    The film was one of Saigal’s "most famous films" and a "classic" as far the music and songs of the films were concerned.    The music was composed by R. C. Boral with lyrics by Ajoy Bhattacharya.    The cast of the film included K. L. Saigal, Kanan Devi, Boken Chatto, Amar Mullick, Sailen Chowdhury and Shyam Laha.    The story involved two young street children growing up together singing on the streets and hoping to make it big in show world. 

==Cast==
*K. L. Saigal
*Kanan Devi
*Boken Chatto
*Rekha
*Amar Mullick
*Sailen Chowdhury
*Bhanu Bannerjee
*Ahi Sanyal
*Khagen Pathak
*Sukumar Pal
*Shyam Laha
*Binoy Goswami

==Music==
The music was composed by R. C. Boral with lyrics by Ajoy Bhattacharya.    
Songlist:
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer
|-
| 1
| "Raakhal Raja Re"
| Kanan Devi
|-
| 2
| "Jhumar Jhumar Nupur Baaje"
| K. L. Saigal
|-
| 3
| "Shonaar harin aaye re aaye"
| Kanan Devi
|-
| 4
| "Ae Gaan Tomaar Sheesh"
| K. L. Saigal
|-
| 5
| "Prem Bhikhaari Premer Jogi"
| Kanan Devi
|-
| 6
| "Paaye Cholaar Pother Kathaa Ki Jaane Re Jogi"
| Kanan Devi
|-
| 7
| "Ghor Je Aamaay Daak Diyechhe"
| Kanan Devi
|-
| 8
| "Tomare Bhulte Paarina"
| Kanan Devi
|-
|}

==References==
 

==External Links==
* 

 
 
 

 