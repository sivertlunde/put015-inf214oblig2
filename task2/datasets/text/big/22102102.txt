Police Court (film)
{{Infobox Film
| name= Police Court
| image= File:Police Court lobby card.jpg
| caption= Lobby card
| director= Louis King
| producer= I. E. Chadwick
| writer= Stuart Anthony (story and screenplay)
| narrator= 
| starring= Henry B. Walthall Leon Janney Lionel Belmore King Baggot
| music= 
| cinematography= Archie Stout
| editing= Charles J. Hunt
| distributor= 
| released= February 20, 1932
| runtime= 63 min.
| country= United States English
| budget= 
}} drama film|motion Directed by produced by I. E. Chadwick, the screenplay was adapted by Stuart Anthony from his story. Police Court features an all-star cast from the silent film era.

==Plot==
A once popular actor, Nat Barry (played by Henry B. Walthall), is a has-been because of his alcoholism. The legendary film star is forced by necessity to take a job selling patent medicine at a traveling sideshow dressed in a costume as Abraham Lincoln. Having trouble staying sober, he is arrested and taken before a "police court" for drunken disorder. His teenage son, Junior Barry (played by Leon Janney), pleads on Barrys behalf and Judge Robert Webster (played by Edmund Breese) grants him a reprieve.

Junior is determined to see his father make good again, vowing to keep him off the bottle and on the screen. He attempts to get bit parts for Barry, but he has trouble delivering his lines on the movie set for the compassionate director, Henry Field (played by King Baggot).

==Cast==
*Henry B. Walthall as Nat Barry
*Leon Janney as Junior Barry
*Lionel Belmore as Uncle Al Furman
*King Baggot as Henry Field, movie director
*Al St. John as Skid
*Edmund Breese as Judge Robert Webster
*Aileen Pringle as Diana McCormick Walter James as Cappy Hearn
*Al Bridge
*Bud Osborne 
*Paul Panzer as movie actor
*Natalie Joyce as actress Jack Richardson
*Fred Toones as Snowflake

==External links==
*  at the Internet Movie Database

 
 
 
 
 
 
 


 