The Serpent's Egg (film)
 
 
{{Infobox film name           = The Serpents Egg image          = The_Serpents_Egg.jpg caption        = Theatrical release poster image_size     = 190px director       = Ingmar Bergman writer         = Ingmar Bergman cinematography = Sven Nykvist starring       = David Carradine Isolde Barth Heinz Bennent Toni Berger Christian Berkel Liv Ullmann producer       = Dino De Laurentiis distributor    = Paramount Pictures MGM Home Entertainment (DVD) released       =   runtime        = 120 min. language       = English German budget         =
}} Brutus in Julius Caesar: And therefore think him as a serpents egg/Which hatchd, would, as his kind grow mischievous;/And kill him in the shell.

== Synopsis == German economy at the time. He lives with his sister-in-law Manuela, a prostitute and cabaret performer. The story takes place in the week following his brothers death. Abel takes a job offered by an acquaintance, Professor Hans Vergerus.

== Cast ==

 
* Liv Ullmann (Manuela Rosenberg)
* David Carradine (Abel Rosenberg)
* Gert Fröbe (Inspector Bauer)
* Heinz Bennent (Hans Vergérus)
* James Whitmore (Priest)
* Glynn Turman (Monroe)
* Georg Hartmann (Hollinger)
* Edith Heerdegen (Mrs Holle)
* Kyra Mldeck (Miss Dorst)
* Fritz Straßner (Doctor Soltermann)
* Hans Quest (Doctor Silbermann)
* Wolfgang Weiser (Official)
* Paula Braend (Mrs. Hemse)
* Walter Schmidinger (Solomon)
* Lisi Mangold (Mikaela)
* Grischa Huber (Stella)
* Paul Bürks
* Toni Berger (Mr. Rosenberg)
* Erna Brunell (Mrs. Rosenberg)
* Isolde Barth
* Rosemarie Heinikel
* Andrea LArronge
* Beverly McNeely
* Hans Eichler (Max)
* Kai Fischer
* Harry Kalenberg
* Gaby Dohm
* Christian Berkel (Student)
* Paul Burian
* Charles Regnier
* Günter Meisner
* Heide Picha
* Günter Malzacher
* Hubert Mittendorf
* Hertha von Walther
* Ellen Umlauf
* Renate Grosser
* Hildegard Busse
* Richard Bohne
* Emil Feist
* Heino Hallhuber
* Irene Steinbeisser
 

==Response== tax evasion charge. The film opened to mostly negative reviews by critics. Many felt David Carradine was terribly miscast and that the movie was not like Bergmans past films. 

==References==

 

== External links ==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 

 