Kavya (film)
{{Infobox film name           = Kavya image          = image_size     = caption        = director       = Kodlu Ramakrishna producer       = Vaidehi Venkataram   Mangalagowri   Kodlu Ramakrishna writer         = Vijaya Thandavamurthy   Kodlu Ramakrishna narrator       = starring  Sithara Sudharani Kalyan Kumar music          = Sadhu Kokila cinematography = B. S. Basavaraj editing        = B. S. Kemparaj studio         = Spandana Films released       =   runtime        = 147 minutes country        = India language       = Kannada budget         =
}} Indian Kannada Kannada romantic Sithara in the lead roles with Sudharani playing the titular role.  The film was produced under Spandana Films banner and the original score and soundtrack were composed by Sadhu Kokila.

==Cast==
* Ramkumar 
* Sudharani as Kavya Sithara 
* Kalyan Kumar Ramakrishna
* Gorur Venkatram
* Girija Lokesh
* Chandrashekar
* Jaishree Sridhar
* Ramachandra

==Soundtrack==
The music of the film was composed by Sadhu Kokila and lyrics written by Prof.Doddarange Gowda and Geethapriya. It also has a couplet written by poet Kuvempu. The soundtrack also features one song sung by actor Rajkumar (actor)|Rajkumar. 

Prof. Doddarange Gowda was awarded with the Karnataka State Film Award for Best Lyricist for the year 1995-96 for the song "Vandane Vandane" written by him.

{{Infobox album  
| Name        = Kavya
| Type        = Soundtrack
| Artist      = Sadhu Kokila
| Cover       = 
| Alt         = 
| Released    =  
| Recorded    =  Feature film soundtrack
| Length      = 
| Label       = Akash Audio
}}

{{Track listing
| total_length   = 
| lyrics_credits = yes
| extra_column	 = Singer(s)
| title1	= Vandane Vandane
| lyrics1 	= Prof. Doddarange Gowda
| extra1        = S. P. Balasubrahmanyam
| length1       = 
| title2        = Ettha Thirugali Kannu
| lyrics2 	= Kuvempu
| extra2        = K. S. Chithra
| length2       = 
| title3        = Olumeya Kavya
| lyrics3       = Prof. Doddarange Gowda
| extra3 	= Rajesh Krishnan, K. S. Chithra
| length3       = 
| title4        = Nee Midida Madhura 
| extra4        = S. P. Balasubrahmanyam, Manjula Gururaj
| lyrics4 	= Devappa
| length4       = 
| title5        = Aasegala Lokadali Rajkumar
| lyrics5       = Geethapriya
| length5       = 
}}

==References==
 

==External source==
*  
*  

 
 
 
 
 
 


 

 