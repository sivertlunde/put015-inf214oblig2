A Good Little Devil
{{infobox film
| name           = A Good Little Devil
| image          = A Good Little Devil - 1914 - poster.jpg
| imagesize      =
| caption        = Movie poster.
| director       = Edwin S. Porter
| producer       = Adolph Zukor Daniel Frohman David Belasco (supervisor)
| writer         = Rosemonde Gerard (play Un bon petit diable) Maurice Rostand (play Un bon petit diable) Austin Strong (Broadway version of play) 
| starring       = Mary Pickford
| music          =
| cinematography = Edwin S. Porter
| editing        =
| studio         = States Rights 
| distributor    = Famous Players Film Company
| released       = July 10, 1913 (preview) March 1, 1914 (nationwide) reels
| country        = United States
| language       = Silent film English intertitles
}}
A Good Little Devil is a 1914 silent film starring Mary Pickford (her first feature-length film), produced by Adolph Zukor and Daniel Frohman, and distributed on a States Rights basis.

Pickford, along with friend Lillian Gish, appeared in the Broadway play version  of the story prior to Pickford making this film. Much of the cast of the play appeared in the film version. This film is essentially lost film|lost, with only one of the five reels surviving.  

==Cast==
*Mary Pickford - Juliet
*Ernest Truex - Charles MacLance
*William Norris - Mrs. MacMiche
*Iva Merlin - Betsy
*Wilda Bennett - Queen Mab
*Arthur Hill - Rab, the dog
*Edward Connelly - Old Nick, Sr.
*David Belasco - himself Paul Kelly - unknown role (unbilled)

==See also==
*The House That Shadows Built (1931 promotional film by Paramount)

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 


 