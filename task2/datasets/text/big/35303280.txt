Before It Had a Name
{{Infobox film
| name           = Before It Had a Name
| image          = Before It Had a Name.jpg
| alt            =
| caption        = Film poster
| director       = Giada Colagrande
| producer       = Brian Bell Rita Capasa Randall Emmett Frank Frattaroli George Furla Avi Lerner
| writer         = Giada Colagrande Willem Dafoe
| starring       = Giada Colagrande Willem Dafoe Seymour Cassel
| music          = 
| cinematography = Ken Kelsch Brian Pryzpek
| editing        = Natalie Cristiani		 
| studio         = Bidou Pictures Canary Films Emmett/Furla/Oasis Films|Emmett/Furla Films In Between Pictures Millennium Films Nu Image Films
| distributor    = 
| released       = September 2005 (Venice Film Festival
| runtime        = 101 minutes
| country        = United States
| language       = English
| budget         = $450,000
| gross          =
}} The Black Widow when it was released on DVD. It marked the first time Dafoe had developed a project to the point of being shot as well as the first time Colagrande had written in English language|English.   

==Synopsis==
After her lover Karl dies, Eleonora goes to his New York estate known as The Rubber House in hopes of learning about him. While there, she becomes involved with the propertys strange caretaker, Leslie.

==Principal cast==
{| class="wikitable" style="width:50%;"
|- "
! Actor !! Role
|-
| Willem Dafoe || Leslie 
|-
| Giada Colagrande || Eleonora 
|-
| Seymour Cassel || Jeff 
|-
| Isaach de Bankolé || Waiter 
|-
| Emily Cass McDonnell || Gail
|-
| Claudio Botosso || Karl 
|-
| Bari Hyman || LP
|}

==Critical reception==
Boyd Van Hoei wrote in Cine Europa: 
 

==References==
 

== External links ==
* 
* 

 
 
 
 
 