The Cutting Edge: Going for the Gold
{{Infobox film
| name        = The Cutting Edge: Going for the Gold
| image       = The Cutting Edge Going for the Gold.jpg
| caption     = DVD cover art Sean McNamara
| producer    = David Brookwell Sean McNamara David Buelow David Grace Dean Valentine Scott Immergut Dan Berendsen Ross Thomas Scott Thompson Baker Kim Kindrick Stepfanie Kramer
| music = John Coda
| cinematography = Mark Doering–Powell
| editor = Jeff W. Canavan
| studio = First Family Entertainment Brookwell McNamara Entertainment
| distributor = MGM Home Entertainment
| released    =  
| runtime     = 94 minutes
| language    = English
}}
The Cutting Edge: Going for the Gold (also known as The Cutting Edge 2 or The Cutting Edge 2: Going for the Gold) is a 2006 American   aired on ABC Family on March 16, 2008 {{cite web |title=Cutting Edge 3 Skates to ABC Family: A male ice skater and a female ice hockey player find love |url=http://www.zap2it.com/tv/news/zap-cuttingedge3premiere,0,4122153.story 
|date=February 4, 2008 |publisher=Zap2it.com}}  and another sequel,   aired on March 14, 2010.

==Plot summary==
Jackie Dorsey, the daughter of Olympic Gold medalists Doug Dorsey and Kate Moseley from the original film, also enters the figure skating field, with ambitions of winning her own Olympic gold. However, a serious injury derails that ambition. After months of training, Jackie is finally ready to skate again but has trouble keeping up with the rigorous demands of singles skating. Her parents send her on a vacation to L.A., where she meets surfer boy/in-line skater Alex Harrison. Sparks fly between the two, but when Alex discovers who Jackie is, he rejects her. In the meantime, Jackie realizes that, considering the circumstances, pairs skating will give her a better chance at Olympic gold. After many unsuccessful interviews with prospective partners, she becomes frustrated. Then, after Alex Harrison sees Jackie on TV, discussing her search for a partner, he shows up for an interview. Even though Alex has no experience and has trained for only a short time, he shows remarkable natural talent.

However, Jackie feels that Alex is lazy and unreliable. Inevitably, the two lock horns. They fight so constantly that Jackies mother locks them together with a harness, forcing them to do everything together. After that, Jackie and Alex start to get along, and their attraction grows. But then, Alexs old girlfriend Heidi shows up and jeopardizes Alex and Jackies chance at gold. After a blowout fight at regionals, Alex leaves. Jackie convinces him to return, but also learns that Heidi and Alex are engaged. Alex and Jackie train for the Olympics, which will be held in Torino, Italy. Later, while in Torino, Heidi lets it slip that they are getting married immediately after the closing ceremonies and that Alex will be hanging up his skates for good. This causes friction between Jackie and Alex, resulting in a passionless short program, and they end up in fourth place.

Jackie then reveals to her father that she still loves Alex, and her father convinces her to talk to him. Jackie goes to Alexs room and pours her heart out to someone that she thinks is Alex, but that person is actually Heidi. After hearing Jackie’s confession of love, Heidi locks the door and Jackie takes off. When Alex discovers what Heidi has done, he goes after her. Heidi tells him that if he leaves, she will not be there when he returns. He leaves anyway. Jackie refuses to talk to Alex, but right after their program begins, Alex tells Jackie that he is in love with her and that he wants to be with her for the rest of his life. Their long program is flawless and includes a move that has never been done before. In the final shot, Alex and Jackie are seen kissing.

==Cast==
* Christy Carlson Romano as Jackie Dorsey Ross Thomas as Alex Harrison
* Stepfanie Kramer as Kate Moseley-Dorsey
* Scott Thompson Baker as Doug Dorsey
* Kim Kindrick as Heidi Clements

===Cameos===
* Oksana Baiul as a commentator. Sean McNamara as a guest waiting for his car. (Wearing an orange shirt in the scene just before Jackie and Alex have their first kiss.)
* Michael Kuluva as a figure skater trying out to be Jackies pair partner.

==Timeline==
While the first film—starring Moira Kelly and D. B. Sweeney—was itself set in 1988 to 1992, the second film is clearly set during the run up to the 2006 Olympics. This would make Jackie—played by then 21-year-old Romano–only 14 years old.

In an example of retroactive continuity, Going for the Gold is based on the assumption that the first film has been moved from 1992 to 1984. This was confirmed in an interview with Stepfanie Kramer, included on the DVD release of the first film. In the interview, Kramer says that the second film takes place more than 20 years after the events of the original.

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 