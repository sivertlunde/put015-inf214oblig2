The Girl Who Came Late
 
{{Infobox Film
| name           = The Girl Who Came Late
| image          = 
| image_size     = 
| caption        = 
| director       = Kathy Mueller Ben Gannon	
| writer         = Saturday Rosenberg 
| narrator       =  Martin Kemp Gia Carides
| music          = Todd Hunter Johanna Pigott
| cinematography = Andrew Lesnie
| editing        = Robert Gibson
| distributor    = Beyond Films
| released       = 1991
| runtime        = 82 minutes
| country        = Australia
| language       = English
| budget         = 
| gross = A$24,885 (Australia) 
| preceded_by    = 
| followed_by    = 
}} Martin Kemp Australian Film The Lord War of the Worlds.

==Plot==
The Girl of the title is Nell Tiscowitz (Otto), a struggling actress with an affinity for horses.    She meets wealthy Rock music promoter and stable owner Digby Olsen (Kemp bass guitarist of  ) provides dubious love-lorn advice. After Nell uses her telepathy to help Digby tame horses they eventually fall in love.

==Production==
The film was one of five films financed by the FFC Film Fund in 1990. Otto was cast after over 200 girls auditioned; it was only her second major role, after Emmas War. The role of Digby entailed looking at actors from London and Los Angeles; after Martin Kemp was cast, the occupation of the character was changed from theatre entrepreneur to rock promoter. Eva Friedman, "Kathy Muellers Daydream Believer", Cinema Papers, May–June 1992 p16-18 

==References==
 

==External links==
* 
*  at  

 
 
 
 
 
 


 
 