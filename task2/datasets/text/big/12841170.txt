Long May It Wave
 
{{Infobox film
| name           = Long May It Wave
| image          =
| caption        =
| director       = John A. Murphy
| producer       = Arthur Hotaling
| writer         = E. W. Sargent	
| starring       = Raymond McKee
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        =
| country        = United States
| language       = Silent film English intertitles
| budget         =
}}
 silent comedy film featuring Oliver Hardy.

==Cast==
* Raymond McKee - Pat McCarthy
* Mae Hotely - Nora
* Marguerite Ne Moyer - Lucille
* Ed Lawrence - General Grabimoff
* Oliver Hardy - The King (as Babe Hardy)
* Ben Walker - Paul

==See also==
* List of American films of 1914
* Oliver Hardy filmography

==External links==
* 

 
 
 
 
 
 
 

 