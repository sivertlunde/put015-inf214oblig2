Hurricane Hutch
 
{{Infobox film
| name           = Hurricane Hutch
| caption        = Film poster
| image	         = Hurricane Hutch FilmPoster.jpeg
| director       = George B. Seitz
| producer       = George B. Seitz
| writer         = Charles Hutchison
| starring       = Charles Hutchison Lucy Fox
| cinematography = 
| editing        = 
| studio         = George B. Seitz Productions
| distributor    = Pathé Exchange
| released       =  
| runtime        = 15 episodes 
| country        = United States  Silent English intertitles
| budget         = 
}}
 adventure film serial directed by George B. Seitz. The film is considered to be lost film|lost.    The story concerns the search for a lost formula for making paper from seaweed that will save a mortgaged papermill.

==Cast==
* Charles Hutchison as Larry Hutch Hutchdale
* Lucy Fox as Nancy Kellogg
* Warner Oland as Clifton Marlow
* Diana Deer as Belle Brinkley
* Ann Hastings as Ann Haviland
* Harry Semels as Jim Tiegerley
* Frank Redman as John Brinkley
* Tom Goodwin as Silas Haviland (as Tomas G. Goodwin)
* Charles Patch Revada as Bill Hogan (as Charles Revada)
* Joe Cuny as Wilson Winslow

==Chapter titles==
 
# The Secret Cipher
# The Cycle Bullet
# The Millionth Chance
# Smashing Through
# One Against Many
# At the Risk of his Neck
# On a Dangerous Coast
# Double Crossed
# Overboard
# The Show Down
# Hare and Hounds
# Red Courage
# Neck and Neck
# The Secret of the Flame
# The Last Duel

==See also==
* List of film serials
* List of film serials by studio
* List of lost films

==References==
 

==Bibliography==
*Lahue, Kalton C., Continued Next Week: A History of the Moving Picture Serial (University of Oklahoma Press, 1964)

==External links==
 
* 

 

 
 
 
 
 
 
 
 