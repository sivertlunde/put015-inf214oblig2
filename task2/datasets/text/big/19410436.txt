Night 'n' Gales
{{Infobox Film
| name           = Night n Gales
| image          = Night n gales TITLE.JPEG
| image_size     = 
| caption        = Title card Gordon Douglas
| producer       = Hal Roach
| writer         = 
| narrator       = 
| starring       = 
| music          = Leroy Shield Marvin Hatley
| cinematography = Art Lloyd
| editing        = William H. Ziegler
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 10 43"
| country        = United States
| language       = English
| budget         = 
}}
 short comedy Gordon Douglas.  It was the 156th Our Gang short (157th episode, 68th talking short, and 69th talking episode that was released.

==Plot==
Though he would rather spend his evening in peace and quiet, Mr. Hood (Johnny Arthur) is forced to endure the offkey harmonizing of The Four Nightengales, a junior singing aggregation composed of Spanky, Alfalfa, Buckwheat and Porky.

After interminable choruses of "Home! Sweet Home!", the four boys are finally ready to leave, but are forced to stay in the Hood home due to a sudden thunderstorm. Both Darla and her mother are delighted, but Mr. Hood is dismayed, especially when he is told that he must share his bed with the Four Nightengales. Driven crazy by the boys unintentionally disruptive shenanigans, Mr. Hood escapes to the living room and tries to sleep on the couch, covering himself with a bear rug to keep warm. Naturally, the gang mistake him for a real bear, and comic chaos ensues.   

==Notes==
Comedian Johnny Arthur, who played Spanky McFarlands absent-minded father in Anniversary Trouble, returns as Darlas father in Night n Gales. He was so popular with audiences that he played Darlas father once again in Feed em and Weep.

==Cast==
===The Gang===
* Darla Hood as Darla Eugene Lee as Porky
* George McFarland as Spanky
* Carl Switzer as Alfalfa
* Billie Thomas as Buckwheat
* Gary Jasgur as Junior

===Additional cast===
* Johnny Arthur as Arthur Hood, Darlas father
* Elaine Shepard as Darlas mother

==See also==
* Our Gang filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 