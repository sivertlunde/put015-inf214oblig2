Steadfast Benjamin
{{Infobox film
| name           = Steadfast Benjamin
| image          = 
| image_size     = 
| caption        = 
| director       = Robert Wiene
| producer       = Oskar Messter
| writer         = Robert Wiene   
| narrator       = 
| starring       = Guido Herzfeld   Martha Altenberg   Arnold Rieck
| music          =  
| editing        = 
| cinematography = 
| studio         = Messter Film
| distributor    = Hansa Film
| released       =  
| runtime        = 
| country        = Germany Silent  German intertitles
| budget         = 
| gross          = 
}} German silent silent comedy film directed by Robert Wiene and starring Arnold Rieck, Guido Herzfeld and Martha Altenberg. After inherting a large sum of money, a shoe store worker has to continue in his job due to his ten-year contract, but lives the high life each night. 

==Cast==
* Arnold Rieck 
* Guido Herzfeld 
*  Martha Altenberg
* Magda Madeleine   
* Agda Nielson   
* Emil Rameau   

==References==
 

==Bibliography==
* Jung, Uli & Schatzberg, Walter. Beyond Caligari: The Films of Robert Wiene. Berghahn Books, 1999.

==External links==
* 

 

 

 
 
 
 
 
 