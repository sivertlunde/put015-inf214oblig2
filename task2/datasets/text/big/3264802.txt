Awara Paagal Deewana
{{Infobox film
| name           = Awara Paagal Deewana
| image          = Awarapaagaldeewanadvd.jpg
| caption        = Theatrical release poster
| director       = Vikram Bhatt
| producer       = Firoz A. Nadiadwala
| screenplay     = Mangesh Kulkarni
| starring       = Akshay Kumar Sunil Shetty Aftab Shivdasani Paresh Rawal Preeti Jhangiani Aarti Chhabria Amrita Arora
| music          = Anu Malik
| cinematography = Pravin Bhatt
| editing        = Amit Saxena
| distributor    = Base Industries Group
| released       =  
| runtime        = 164 mins
| country        = India Hindi
| budget         = 
| gross          = 
}}
 action comedy The Whole Nine Yards.

==Plot== don (Om Puri), who dies of a heart attack at the beginning of the film. He leaves diamonds worth $200mn at the New York Bank, to be distributed equally between his son Vikrant (Rahul Dev), his daughter Preeti (Preeti Jhangiani), and Preetis husband Guru Gulab Khatri (Akshay Kumar). To claim the diamonds, all three benefactors must be present at the bank or, if dead, their death certificates must be presented.
 US disguised as someone else to escape prosecution. Guru moves to the street where Anmol (Aftab Shivdasani) and his family live. Anmol recognizes him from the news. His mother-in-law (Supriya Pilgaonkar) forces him to go to India, along with his father-in-law, Manilal (Paresh Rawal) to tell Vikrant about Guru and get the reward. But everything turns wrong when Vikrant doesnt give the reward and instead sends Anmol and Manilal back to the USA with his hired men, Yeda Anna (Sunil Shetty) and Chota Chathri (Johnny Lever) to kill Guru. Later, they find out that Yeda Anna was a double agent: He was working for Guru the whole time because he offered more money. The group receive Vikrant and Preeti at the airport and drive to the hotel. At the hotel, Vikrant is kidnapped by a mysterious group of Chinese goons. Yeda Anna receives a phone call and finds out that the kidnapped Vikrant was a duplicate of the real one and the real one was going to come later by plane. They kill the second Vikrant and put his body in a car by the Brooklyn Bridge.

With Vikrants death, Preeti and Guru each get half of the diamonds. When they are outside the bank, a group of policemen arrest them and take them to an unknown desert area. It is revealed that Vikrant is still alive and the first Vikrant was kidnapped by Chinese goons. Vikrant gets the diamonds and tries to kill Guru. A fight occurs. At the end of the fight, Guru kills Vikrant. When its all over, they ask each other to find out who has the diamonds. Anmol has them and says he will give them to Guru if he gives Preeti a divorce. Guru arrives at the location selected by Anmol and gets the diamonds, while giving Anmol the divorce papers. Yeda Anna betrays Guru and tries to steal the diamonds. Guru wins in a fight and gives the diamonds to Yeda Anna while hanging him to a bar and having him stand on Chota Chathris shoulders. In the end, Anmol and his love, Preeti, are seen going to India. Anmols ex-father-in-law gives him some diamonds that he received from Guru. Preeti comments that she didnt know that Guru was so nice.

==Cast==
* Akshay Kumar as Guru Gulab Khatri
* Sunil Shetty as Yeda Anna
* Aftab Shivdasani as Anmol Acharya
* Paresh Rawal as Manilal Patel
* Aarti Chabria as Tina
* Johnny Lever as Chhota Chhatri
* Preeti Jhangiani as Preeti
* Amrita Arora as Mona
* Rahul Dev as Vikrant
* Supriya Pilgaonkar as Paramjeet
* Om Puri as Don
* Asrani as Champaklal

==Soundtrack==
{{Track listing Sameer
| all_music = Anu Malik
| extra_column = Singer(s)
| title1 = Awara Paagal Deewana
| extra1 = Shaan (singer)|Shaan, Sunidhi Chauhan
| length1 = 4:00
| title2 = Jise Hasna Rona
| extra2 = Udit Narayan, Sonu Nigam, Shaan (singer)|Shaan, Alka Yagnik, Sunidhi Chauhan, Sarika Kapoor
| length2 = 7:30
| title3 = Love
| note3 = Theme
| extra3 =  
| length3 = 1:10
| title4 = Maine To Khai Kasam Abhijeet
| length4 = 5:02
| title5 = More Sawariya
| extra5 = Sunidhi Chauhan, Shaan (singer)|Shaan, Anu Malik
| length5 = 6:36
| title6 = Ya Habibi
| extra6 = Adnan Sami, Shabbir Kumar, Sunidhi Chauhan
| length6 = 7:19
| title7 = Yeh Tune Kya Kiya (not used in the film)
| extra7 = Sonu Nigam, Anuradha Paudwal
| length7 = 7:18
}}

==Awards==
 
|- 2003
| Paresh Rawal
| Filmfare Award for Best Performance in a Comic Role
|  
|-
| Zee Cine Award for Best Actor in a Comic Role
|  
|- 2004
| Screen Award for Best Comedian
|  
|-
| Johnny Lever
|  
|}

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 