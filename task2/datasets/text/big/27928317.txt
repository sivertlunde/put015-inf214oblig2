There's One Born Every Minute
{{Infobox Film
| name           = Theres One Born Every Minute
| image          =
| image size     =
| caption        = Harold Young
| producer       =
| writer         =Robert B. Hunt
| narrator       =
| starring       =
| music          =
| cinematography =
| editor         =
| studio         = Universal Studios
| released       =  
| runtime        = 60 minutes
| country        = United States English
| budget         =
}}
 1942 United American Universal Harold Young. It was the debut film of Elizabeth Taylor. It is also known as Man or Mouse.

==Plot Summary==

With a helpful push from his wife Minerva, Lemuel P. Twine, "Lem", decides to enter the scene of politics, by running a campaign as reform-mayor of his hometown Witumpka Falls. Normally he runs the Twines Tasty Pudding Powder Company.

Lem is unaware that his financial backer, Lester Caldwalader Sr., wants him to run in order to secure that the current mayor, Moe Carson, is re-elected. When Lems oldest daughter Helen Barbara is on her way to her date with Caldwaladers son Lester Jr., aka "Les", she accidentally bumps into her ex-boyfriend Jimmy Hanagan, who is just back from marketing studies at college.

Since Jimmy hasnt found work within his field of advertising yet, he is currently working as a clerk at the Caldwaladers general store. When Les sees Helen and Jimmy together he is jealous and end up firing Jimmy from the store. Instead Jimmy gets a job as advertising director for Helens fathers company. At work, Jimmy comes up with the idea of telling the consumers that the puddings are full of Vitamin Z, a made-up healthy ingredient. He goes on to persuade a local scientist, Asa Quiesenberry, to work with him on the marketing of the product, claiming the discovery of the new fantastic vitamin. The product is tested in a faux laboratory and the media is informed of the vitamins superior qualities. Among other effects, it is said to enhance the female sexual appetite.

The pudding business sky-rockets and the small town is famous nationwide for the new "Zumf" vitamin products. Lem is awarded Witumpka Falls Man of the Year" by the town Chamber of Commerce, and in a newspaper interview with Minerva, she announces the engagement of Helen and Jimmy. These news all upset Les Jr. And Sr. greatly, and they start scheming a plan to ruin the Twine family and their business. 
 
The Caldwaladers claim that the Vitamin Z disappears from the pudding powder after it has aged a while. After some intensive testing of old cases of powder, the Caldwaladers announce, at a dinner in Lems honor, that they have found no trace of Zumf in the old pudding powder. They also claim Jimmy is a fraud and accuses him of bribing scientists to play along.

Humiliated, Jimmy leaves the Twine family. Lem continues his campaign for mayor and tells the people to elect him as a man, not as the leader of a pudding company. Jimmy returns after a while, and when he does he sees Helen reunited with Les, and becomes very jealous.

That night, Lem meets the ghost of his grandfather Claudius, emerging from a painted portrait on the wall. Claudius warns Lem of the Caldwaladers, and the morning after he meets Jimmy and Quisenberry to tell them about his dream.

When Lem tries to tell Moe Carson about Caldwaladers treacherous behavior, Les Sr arrives and a fistfight between him and Moe ensues. The Caldwaladers are exposed as traitors to the town, through flyers handed out by Jimmy and Helen, and Lem is elected mayor.

Lem urges Jimmy to reconcile with Helen, and he does, after giving her a lesson for getting back together with Ls when he was gone. After they make up, Claudius watches happily them from his painting on the wall. 

==Cast==
*Hugh Herbert	... 	Lemuel P. Twine / Abner Twine / Colonel Cladius Zebediah Twine
*Peggy Moran	... 	Helen Barbara Twine Tom Brown	... 	Jimmy Hanagan
*Guy Kibbee	... 	Lester Cadwalader, Sr.
*Catherine Doucet	... 	Minerva Twine
*Edgar Kennedy	... 	Mayor Moe Carson
*Gus Schilling	... 	Professor Asa Quisenberry
*Elizabeth Taylor	... 	Gloria Twine
*Charles Halton	... 	Trumbull
*Renie Riano	... 	Miss Aphrodite Phipps
*Carl "Alfalfa" Switzer	... 	Junior Twine (as Alfalfa Switser)
*Mel Ruick	... 	Radio Announcer (as Melville Ruick)

== External links ==
*  

==References==
 

 
 
 
 
 
 
 