Haunted Summer
 
{{Infobox film
| name = Haunted Summer
| image = 
| alt = 
| caption = 
| director = Ivan Passer
| producer = Martin Poll
| writer = Lewis John Carlino
| based on =  
| starring = 
| music = Christopher Young
| cinematography = Giuseppe Rotunno
| editing = Cesare DAmico Rick Fields
| studio =  Cannon Films
| released =  
| runtime = 106 minutes
| country = United States
| language = English
| budget = 
| gross = 
}}

Haunted Summer is a 1988 drama film directed by Ivan Passer.

It stars Philip Anglim as Lord Byron, Eric Stoltz as Percy Shelley, Alice Krige as Mary Wollstonecraft Godwin, Alex Winter as Dr. John William Polidori and Laura Dern as Claire Clairmont.

== Plot summary ==
Like the 1986 film Gothic (film)|Gothic, Haunted Summer is set in 1816. Authors Lord Byron, Mary Shelley (née Godwin) and Percy Shelley get together for some philosophical discussions, but the situation soon deteriorates into mind games, drugs, and sex. It is a fictionalization of the summer that Lord Byron and the Shelleys, together with Lord Byrons ex-lover and his doctor, John Polidori, spent in the isolated Villa Diodati by Lake Geneva. It is there they devise a contest to adduce the best horror story to kill the dullness of summer. It is also there that one of the worlds most famous books was given life—Mary Shelleys Frankenstein.

 

 
 

 