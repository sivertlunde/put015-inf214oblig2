Oru Kaidhiyin Diary
{{Infobox film
| name           = Oru Kaidhiyin Diary
| image          = Oru-Kaidhiyin-Diary-1984.jpg
| image_size     =
| caption        = 
| director       = P. Bharathiraja
| producer       = Chandraleela Bharathiraja
| writer         = K. Bhagyaraj
| screenplay     = P. Bharathiraja
| story          = K. Bhagyaraj
| narrator       = Radha Revathi Vijayan Vinu Chakravarthy
| music          = Ilaiyaraaja
| cinematography = B. Kannan
| editing        = V. Rajagopal
| studio         = Janani Art Creations
| distributor    = Janani Art Creations
| released       = 14 January 1985
| runtime        = 152 minutes
| country        = India Tamil
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
| amg_id         =
| imdb_id        =
}}
 1985 Tamil Indian feature directed by Bharathiraaja, starring Kamal Haasan in the lead role of the protagonist.  The movie was a huge success at the box office.

==Summary==

David gets released from prison after 22 long years and visits his dear friend Velapan.  The flashback shows that David and Rosy were married and had a happy life.  David is politically active in the grassroots movement and introduces his wife and newborn son to the leader of his party, the powerful politician Suryaprakasam, whom David trusted and had a high regard. The child is named James with Suryaprakasams blessings. However, Suryaprakasam is an evil man and was smitten by Rosy, when he saw her.  He concocts a plan; he organizes a political protest with David as the leader, knowing David will get arrested; thereby allowing him access to Rosy.

Suryaprakasam succeeds in his plan. Rosy then comes to him for help in getting David released, and Suryaprakasam rapes her violently with his henchmens help. Shamed and humiliated, Rosy then commits suicide and leaves her husband a handwritten note with the details.  Velapan bails out David.  David who is proud to have gone to jail for Suryaprakasams protest, comes home singing his praises until he sees Rosy hanging dead and reads her note.  Upset, David confronts Suryaprakasam at his birthday function and makes a scene. Inspector Viswanathan and Dr. Unnikrishnan pretend to help David, but are actually Suryaprakasams friends and they destroy Rosys note, thereby the entire evidence of the crime.

The trio then frame David for Rosys murder and easily win the case and David gets sentenced to life imprisonment for killing his wife. He leaves his only son with his close friend Velappan, who promises to raise Davids son as a hardened thug, who will help David avenge Rosys humiliation and death, and Davids wrongful imprisonment.

Upon release from prison, David sees that Velapan is a changed man, no longer engages in questionable activities, educated Davids son and raised him in a righteous way. Davids son James is now called "Shankar" and is a well-respected and courageous police inspector.  David is upset at this news, knows he cannot count on Velapan and his own son, and vows his revenge against the trio who framed him by himself, without anyones help.

David comes to a church to confess his plan; he says to the priest that, he would kill 3 people, without naming them. The priest informs this to the police, who try in vain to capture David. David starts his revenge by killing the now Superintendent Viswanathan methodically and escapes by fooling Inspector Shankar.   He then kills Dr. Unnikrishnan despite strong police protection and escapes. David is supported by Sharadha, the love of Shankar. The climax shows David killing his last target, Suryaprakasam, and how Shankar has to do his duty of shooting his father.

==Cast==

*Kamal Hassan as David and Shankar Radha as Rosy
*Revathi as Sharadha
*Janagaraj as Velappan
*Malaysia Vasudevan as Suryaprakasam the Politician Vijayan as Dr.Unnikrishnan
*Vinu Chakravarthy - Inspector and SP Viswanathan

==Production==
The makeup for Kamal Haasan was provided by Michael Westmore. 

==Soundtrack==
The music composed by Ilaiyaraaja. 
{{Infobox album Name     = Oru Kaidhiyin Diary Type     = film Artist    = Illayaraja Cover    = Orukaithiyinfilm.png Released =  Genre  Feature film soundtrack Length   = 17:36 Label    = Echo
}}

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Vairamuthu || 04:01
|-
| 2 || Ithu Rosa Poovu || Gangai Amaran, Vani Jairam || 04:30
|-
| 3 || Naan Thaan Sooran || S. P. Balasubrahmanyam || 04:31
|-
| 4 || Ponmane Kovam Yeno || Unni Menon, Uma Ramanan || 04:34
|}

==Remake== Aakhree Rasta (1986), directed by K. Bhagyaraj starring Amitabh Bachchan, Sridevi, Jayapradha, Anupam Kher. The Hindi version was highly successful too. 

==References==
 

==External links==
 

 

 
 
 
 
 
 
 
 
 