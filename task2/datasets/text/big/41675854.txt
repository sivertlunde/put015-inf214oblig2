The People vs. Paul Crump
{{Infobox film
| name           = The People vs. Paul Crump
| image          = The People vs. Paul Crump.jpg
| caption        =
| director       = William Friedkin
| producer       = Sterling “Red” Quinlan
| writer         =
| based on       =
| starring       =
| music          = Marty Rubenstein Wilmer Butler
| editing        = Glenn McGowean Facets
| released       =  
| runtime        = 60 mins
| country        = United States English
| budget         = $6,0000 
| gross          =
}}
The People vs. Paul Crump is a 1962 documentary about the prisoner Paul Crump who was on death row for robbery and murder. 

The film was made for Chicago television and was highly praised and crucial to the career of its director William Friedkin, helping him get an agent and jobs making documentaries for David Wolper, and then an episode of The Alfred Hitchcock Hour.  The film won the Golden Gate Award Winner for Film as Communication at the 1962 San Francisco International Film Festival. 

Friedkin says when he made the film he was convinced Crump was innocent but now feels he was guilty. 
 Facets in May 2014. 

==References==
 
Friedkin, William, The Friedkin Connection, Harper Collins 2013

==External links==
*  at IMDB
*  at New York Times
 

 
 
 
 
 

 