Naruto the Movie: Blood Prison
 
{{Infobox film
| name           = Naruto the Movie: Blood Prison
| image          = Naruto Shippuden 5 Blood Prison poster.jpg
| caption        = 
| director       = Masahiko Murata
| producer       = 
| writer         = Akira Higashiyama
| starring       = Junko Takeuchi
| music          = Yasuharu Takanashi Studio Pierrot
| distributor    = Toho
| released       =  
| released on USA =  
| runtime        = 
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = ¥800,000,000
$9,065,101
}}
  is the eighth overall Naruto film and fifth Naruto Shippūden film. It was released in theaters in Japan on July 27, 2011. The DVD release was on the 25th of April 2012. The theme song for the film is "Otakebi" by Yusuke Kamiji. The film was released in North America on February 18, 2014 by Viz Media. The film was released in indonesia on April 19, 2015 

==Plot==
Naruto Uzumaki is sent to a criminal containment facility known as   in the Hidden Grass Village after he is declared responsible for the attempted assassination of the Raikage, leader of the Hidden Cloud Village, as well as the murders of Jōnin from the Hidden Mist Village and the Hidden Rock Village. Once there,  , the head of the prison, places the   seal on Naruto which limits his chakra. On the first night, Naruto uses a clone as a decoy to escape from prison. During his escape, his clone is secretly taken by Mui to have the List of Naruto characters#Kuruma|Nine-tailed beasts chakra extracted which leads him to learn about Narutos escape plan. Naruto is shortly recaptured and placed in solitary confinement which delays Muis plans.

During his time in prison, Naruto befriends  , a female ninja from the Hidden Grass Village pretending to be a male inmate. She explains Mui and a select few from the Hidden Grass Village framed Naruto to imprison him so they may extract the Nine tails chakra to release the seal on the Box of Enlightenment which can grant any wish. Ryuzetsu also shares her plans to avenge her childhood friend  , Muis son who was sacrificed to the box ten years prior. Naruto accepts her offer of collaboration to destroy the box before it is unsealed. They begin their plan by initiating a prison riot which distracts the guards and allows Naruto to find the room with the sealed box. He is then captured by Mui and has his chakra extracted which unlocks the box. Disregarding his collaborators commands, Mui asks the box to return his son. Muku is released from the box and is possessed by the boxs demon,  . He fatally wounds his father, transforms into a monster, and goes on a rampage.

Naruto enters Sage Mode but is unable to defeat it. He is assisted by his peers from the Leaf Village and Cloud Village who were aware of the Hidden Grass plans. During the battle, Naruto and Ryuzetsu are fatally wounded. Mui regains consciousness and seals Muku with the Tenrou which gives Naruto a chance for a direct attack. Muku returns to his human form, has a short bonding experience with his father, and kills him when Satori regains control. Muku then kills himself by using the Tenrou and Satori returns to the box after his death. With Muis death, the prisoners are free from the Tenrou and attempt to escape from prison but are quickly recaptured. As Naruto nears death, Ryuzetsu uses a technique which sacrifices her life to save him. In the aftermath, the Box of Enlightenment is thrown into the ocean and Naruto returns to his comrades after burying Mui and Ryuzetsu.

A post credits scene shows that Narutos arrest was all rehearsed. Though some worry about how theyve hurt Narutos feelings, Kakashi says they all can buy him all the Ichiraku ramen he can eat as an apology once the missions over.

==Voice cast==
{| class="wikitable"
|-
!Character
!Japanese Voice Actor
!English Voice Actor
|- Naruto Uzumaki Junko Takeuchi Maile Flanagan
|- Sakura Haruno Chie Nakamura Kate Higgins
|- Kakashi Hatake Kazuhiko Inoue Dave Wittenberg
|- List of Yamato
|Rikiya Koyama Troy Baker
|- List of Tsunade
|Masako Katsuki Debi Mae West
|- Shikamaru Nara Showtaro Morikubo Tom Gibis
|- List of Choji Akimichi
|Kentarō Itō Robbie Rist
|- List of Kiba Inuzuka
|Kōsuke Toriumi Kyle Hebert
|- List of Neji Hyuga
|Kōichi Tōchika Steve Staley
|- List of Tenten
|Yukari Tamura Danielle Judovits
|- Gamabunta
|Hiroshi Naka Michael Sorich
|- Fourth Raikage, Ay Hideaki Tezuka Beau Billingslea
|- List of Killer Bee Hisao Egawa Catero Colbert
|- Samui
|Hikari Yono Cindy Robinson
|- Omoi
|Kunihiro Kawamoto Ogie Banks
|- Karui
|Yuka Komatsu Danielle Nicolet
|- Mui
|Masaki Terasoma Matthew Mercer
|- Ryuzetsu
|Mie Sonozaki Carrie Keranen
|-
|Muku/Satori Yuichi Nakamura Yuichi Nakamura Kengo Kawanishi (child) Grant George Bryce Papenbrook (child)
|- Kazan
|Kōsei Hirota Steve Staley
|- Maroi
|Takaya Kamikawa Andrew Kishino
|}

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 