Timeline (film)
 
{{Infobox film
| name           = Timeline
| image          = Timeline theatrical poster.jpg
| caption        = Theatrical release poster
| director       = Richard Donner
| producer       = Lauren Shuler Donner Jim Van Wyck Richard Donner
| screenplay     = Jeff Maguire George Nolfi
| based on       =  
| starring       = Paul Walker Frances OConnor Gerard Butler Billy Connolly David Thewlis Anna Friel Neal McDonough Matt Craven Ethan Embry Michael Sheen Lambert Wilson Marton Csokas Rossif Sutherland Brian Tyler Jerry Goldsmith (unused score)
| cinematography = Caleb Deschanel
| editing        = Richard Marks
| distributor    = Paramount Pictures
| released       =  
| runtime        = 116 minutes
| country        = United States
| language       = English
| budget         = $80 million {{cite web|title=Timeline
|url=http://www.the-numbers.com/movies/2003/TIMEL.php}}  
| gross          = $43,935,763 {{cite web|title=Timeline
|url=http://www.boxofficemojo.com/movies/?id=timeline.htm}} 
}} of the same name by Michael Crichton. A team of present-day archaeologists are sent back in time to rescue their professor from medieval France in the middle of a battle. It stars Paul Walker, Frances OConnor, Gerard Butler, Billy Connolly, David Thewlis and Anna Friel among others.
 Brian Tyler, box office failure.

==Plot ==
The American ITC Corporation has sponsored an archeological study of the ruins of the village of Castlegard and its nearby monastery, located near LaRoque Castle in France. The village and monastery were the site of the 1357 hanging of Lady Claire, the sister of Arnaut de Cervole, her martyrdom leading to inspire the French to victory in the Hundred Years War. The team is led by Prof. Edward Johnston, his students Kate Erickson, Josh Stern, and François Dontelle, Scottish archaeologist André Marek, and Johnstons son Chris, who is infatuated with Kate. The team finds a sarcophagus of a French knight with a lopped ear holding hands with his lady, an unusual position for burial of that time period. Johnston suspects ITC may have tampered with the site and returns to their headquarters while the team continues to dig. Shortly after his departure, the team finds a pair of bifocals that belong to Johnston and a parchment with a message pleading for help in Johnstons handwriting, but both date more than 600 years. They attempt to contact Johnston at ITC, but the company asks the group come to their headquarters instead.

There, the team is met by president Robert Doniger and vice-president Steven Kramer, and they are shown what ITC has been developing, a teleportation device that has linked onto a stable wormhole that leads to the village of Castlegard in 1357. Doniger explains that when Johnston arrived, they offered him the opportunity to see the past, but something went wrong and their team had not returned, and requests the students, with their knowledge of that period and location, to travel back and rescue Johnston. All but Josh agree to go. The team is stripped of all modern technology and given period outfits, as well as disguised markers that signal the teleportation machine to recall them to the future. The team is accompanied by ITCs head of security, Frank Gordon, and two former military officers, Bill Baretto and Jimmy Gomez, for support.

When they arrive in 1357, they are quickly set on by English knights chasing a young woman. Gomez is killed by Sir William DeKere, who is leading the knights, and Baretto primes a grenade improperly he brought with him while triggering his marker, but is killed by a knight; his body is brought back to the present, but the primed grenade goes off and severely damages the teleportation machine. In the past, the rest of the group, with the womans help, are able to flee, and she leads them to the nearby village before going back into hiding. In the village, the group is soon captured by the English forces controlling it, led by Lord Oliver de Vannes and his second in command, DeKere. de Vannes believes François is a spy and kills him, while the others are taken to an attic prison, where Johnston is also being held. Johnston quickly explains how he lost his marker soon after arrival and was caught by the English, but kept alive by assuring de Vannes that he was a scientist that could make a new weapon for him, in the form of Greek fire. 

With Kates acrobatics, the group is able to engineer their escape, but this is quickly discovered by the English. Kate, Chris, and Marek escape, freeing the young woman that saved them earlier, for whom Marek has developed romantic feelings, while Gordon and Johnston are recaptured by DeKere. DeKere reveals himself as a former ITC employee, Decker; while he had used the teleportation device many times, the company found that each use damaged his DNA, and he could not safely return after one last trip; he is now posing as an English knight to get back at ITC for hiding this information, and kills Gordon while taking their markers. de Vannes orders the troops to start an assault of LaRoque castle, and DeKere brings Johnston along to keep an eye on him. Meanwhile, the others reach the monastery, where de Cervoles forces wait. They realize that the young woman is Lady Claire, and that by rescuing her, they have changed the course of history. Relieved to have his sister back, de Cervolte gives Marek a horse to free their friends. However, Marek is captured by the English and held captive as they march on the castle. Kate and Chris, realizing they must set the course of history right, help de Cervole to form a plan to sneak around behind de Vannes troops using the underground passages of the monastery. 

Meanwhile, in the present, Josh and Kramer work to repair the damage to the teleportation machine, finding Doniger trying to stall their efforts. Kramer realizes that Doniger wants to try to leave the students in the past, fearing the revelation about the impact of the teleportation device would severely harm the companys finances. Kramer and Josh ignore his plight and race to fix the machine.

In the past, the French and English troops clash in battle. de Vannes is able to capture Lady Claire in battle, but Marek frees himself and Johnston, and with Chriss help, de Cervole, kills de Vannes. DeKere attacks Marek and slashes his ear lobe off; Marek realizes that he is the knight they had seen in the sarcophagus earlier. Recognizing his fate, Marek is invigorated and fights DeKere to his death. He recovers the markers and gives them to Kate, Chris, and Johnston, and gives them enough time to return to the present after saying his goodbyes. In the present, as the markers start up the transportation device, Doniger runs in to try to stop it, but instead is sent to the past while Kate, Chris, and Johnston return; Doniger is quickly killed by an English knight on the field of battle. Later, back at Castlegard, the three archaeologists read the sarcophagus inscription from Marek and Claire, detailing three children, Christophe, Katherine, François, and a full, joyous life together.

==Cast==
*Paul Walker as Chris Johnston
*Frances OConnor as Kate Ericson
*Gerard Butler as André Marek
*Billy Connolly as Professor Edward A. Johnston
*David Thewlis as Robert Doniger
*Anna Friel as Lady Claire
*Neal McDonough as Frank Gordon
*Matt Craven as Steven Kramer
*Ethan Embry as Josh Stern
*Michael Sheen as Lord Oliver de Vannes
*Lambert Wilson as Lord Arnaud de Cervole
*Marton Csokas as Sir William De Kere/William Decker
*Rossif Sutherland as François Dontelle
*Patrick Sabongui as Jimmy Gomez
*Steve Kahan as Baker

==Production==
The battle sequences used medieval reenactors. Richard Donner limited the use of CGI in the film as much as possible. 
 Brian Tylers score, because of the changes in the final cut of the film.  However, both Goldsmith and Tylers scores were released on CD.

==Reception==
Timeline was panned by most critics and did poorly at the box office, only recouping $43 million worldwide from a budget of $80 million. The film also received a 12% fresh rating on film website Rotten Tomatoes based on 139 reviews, stating that this "incoherently plotted addition to the time-travel genre looks and sounds cheesy". 

==References==
 

==External links==
 
* 
* 
* 
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 