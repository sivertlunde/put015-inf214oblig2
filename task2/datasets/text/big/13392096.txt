The Unwelcome Guest
 
{{Infobox film
| name           = The Unwelcome Guest
| image          = An Unwelcome Guest.jpg
| caption        = 
| director       = D. W. Griffith
| producer       = 
| writer         = George Hennessy
| starring       = Mary Pickford
| music          = 
| cinematography = G. W. Bitzer
| editing        = 
| distributor    = 
| released       =  
| runtime        = 17 minutes (16 Frame rate|frame/s)
| country        = United States 
| language       = Silent with English intertitles
| budget         = 
}}
 silent drama film directed by  D. W. Griffith. 

==Plot==
Just before she dies, an elderly married woman stashes the horde of money shes secretly accumulated beneath the false bottom of an old shipping trunk. After her death, her husband, believing himself penniless, has to leave their old home and move in with his sons family, where hes treated with no respect or consideration. Also on the scene is a newly hired kindly young housekeeper (Mary Pickford); she and the old gentleman become close friends and eventually run away together (taking the old shipping trunk with them).

==Cast==
* Mary Pickford - The Slavey
* W. Chrystie Miller - The Old Father
* Charles Hill Mailes - The Son
* Claire McDowell - The Wife
* Jack Pickford - One of the Children
* Elmer Booth - The Hired Hand
* Kate Bruce - The Old Wife Harry Carey - The Sheriff
* J. Jiquel Lanoe - The Doctor
* Lionel Barrymore - At Auction Frank Evans - At Auction
* Lillian Gish - At Auction
* Adolph Lestina - At Auction George Nichols - (unconfirmed)
* W. C. Robinson - At Auction
* Henry B. Walthall - (unconfirmed)

==See also==
* Harry Carey filmography
* D. W. Griffith filmography
* Lillian Gish filmography
* Lionel Barrymore filmography

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 

 