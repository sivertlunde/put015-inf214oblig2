The Last Cartridges
{{Infobox film
| name           = The Last Cartridges
| image          = TheLastCartridges.jpg
| image_size     = 
| caption        = Screenshot from the film
| director       = Georges Méliès
| producer       = 
| writer         = 
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| distributor    = Star Film Company
| released       =  
| runtime        = 1min 11secs
| country        = France Silent
| budget         = 
| gross          =
| preceded_by    = 
| followed_by    = 
}}
 Battle of Sedan during the Franco-Prussian War.
 Gaumont studios to film imitations. 
 

==Synopsis==
A group of soldiers attempt to defend a derelict house, where a nun cares for their wounded, but the house is bombed as they fire the last of the rounds of ammunition they have gathered from the floor.

==References==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 


 