Daava
{{Infobox film
| name = Daava
| image =Daava_poster.jpg
| director = Sunil Agnihotri
| producer = Nitin Raj
| story    = S. khan
| dialogue = dileep misra
| starring = Akshay Kumar Naseeruddin Shah Raveena Tandon Akshay Anand
| music = Jatin-Lalit
| editor = Pappu Sharma
| cinematography = Pramod Bhandari
| released = 18 July 1997
| country = India
| language = Hindi
| Gross = 
}}
 action movie directed by Sunil Agnihotri and starring Akshay Kumar, Raveena Tandon, Naseeruddin Shah and Akshay Anand.

== Plot ==

Inspector Arjun is an honest and diligent police officer. He has an older stepbrother named Bhishma, and a younger brother named Suraj. While Bhishma lives with their mother in the village, Arjun and Suraj live in the city. Suraj gets employed at a poultry farm, but finds out that this is just a front for drugs like cocaine. His attempts to get this information to the police and his brother are in vain, as he is captured by the owners of the poultry farm, who owe their allegiance to notorious gangster Dhaman Chamunda. When Dhaman comes to know about Suraj, he decides to teach Arjun and Bhishma a lesson – first by splitting them up over the property they own in the village, then by framing Bhishma for the death of Suraj. With anger and hostilities reigning high amongst the two remaining brothers, the Chamunda Brothers decide to take full advantage of this situation, and watch in glee as the two brothers go against each other, in a fight to death. Bhishma and Arjun clear their misunderstandings but when their stepmother is killed by the Chamunda Brothers they unite and kill them one by one until eventually destroying their empire.

==Cast==
* Akshay Kumar ...... Inspector Arjun
* Raveena Tandon ...... Seema 
* Naseeruddin Shah ...... Bhishma 
* Akshay Anand ...... Suraj 
* Mohan Joshi ...... Dhaman Chamunda
* Tinu Anand ...... Madan Chamunda
* Goga Kapoor ...... Bihari Baadshah 
* Asha Lata ...... Bhishmas mom; Stepmom of Arjun & Suraj 
* Divya Dutta ...... Deepa 
* Deepak Shirke ...... Anna
* Gufi Paintal ...... Havaldar
* Rajendra Gupta...... Advocate Chamunda brother 
* Brahmachari ...... Havaldar Barood Singh

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)!! Length
|-
| 1
| "Dil Mein Hai Tu" Poornima
| 06:08
|- 
| 2
| "Ru Tu Tu Tu"
| Kumar Sanu, Kavita Krishnamurthy
| 05:51
|- 
| 3
| "Hum Sabhi Hai"
| Kumar Sanu
| 04:58
|- 
| 4
| "Kyun Aanchal Hamara"
| Asha Bhosle
| 05:53
|- 
| 5
| "Jao Jao" Abhijeet
| 05:24
|}

==External links ==
*  

 
 
 
 

 