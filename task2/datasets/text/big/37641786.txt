The Phantom of the Opera at the Royal Albert Hall
 
 
 
{{Infobox film
| name = The Phantom of the Opera at the Royal Albert Hall
| image = The Phantom of the Opera at the Royal Albert Hall Poster.jpg
| director = Nick Morris Laurence Connor  (stage direction) 
| producer = Cameron Mackintosh Dione Orrom Brett Sullivan
| based on =  
| story = Gaston Leroux
| starring = Ramin Karimloo Sierra Boggess Hadley Fraser
| music = Andrew Lloyd Webber
| editing =   Nick Morris Really Useful Films Universal Pictures
| released =  
| runtime = 160 minutes  
| country = United Kingdom
| language = English
}}
 The Phantom Le Fantôme de lOpéra by Gaston Leroux.
 The Phantom of the Opera, three special performances were filmed at the Royal Albert Hall, the third of which was screened live worldwide on 2 October 2011. For further releases, footage from all three performances was edited together.

==Production==

===Idea===
To mark the extraordinary milestone of 25 years, Andrew Lloyd Webber and Cameron Mackintosh planned a special 3-day production to take place at Londons Royal Albert Hall in October 2011. Designer Matt Kinley initially planned to hold a concert-style production not unlike the Les Misérables 25th Anniversary concert at the O2 Arena, but Mackintosh made it clear the show would be fully staged, as both he and Lloyd Webber felt it would not work unless it was the whole show. As a result, the event was planned as a full show.

===Royal Albert Hall===
Designing the staged show at the Royal Albert Hall was a daunting task, as the space (or lack of) was not an easy one to translate a proscenium show into. As a concert hall rather than a theatre, many of the shows elements (such as the chandelier, which instead of falling exploded) had to be toned down and simplified: the Royal Albert Hall was simply not capable of accepting a show the size of The Phantom of the Opera, or at least not the full original. The balconies of the hall were used to build uprights to form an opera house proscenium with boxes on each side. The orchestra was elevated on a platform and backed by a gauze which projected the opera sets via LEDs.

===Live Streaming===
Tickets for the three performances sold out within five hours of going on sale. In order to enable more people to see the production, the final performance was relayed live to cinemas around the world via Fathom Events.

==Plot==
 

===Prologue===
At the fictional Opera Populaire (based on the Paris Opéra House) in 1905, an auction of old theatre props is underway. Lot 665, purchased by the elderly Raoul, Vicomte de Chagny, is a music box in the shape of a monkey; it is familiar to him, and he speaks of a mysterious "she" - that the details of the strange little music box appear "exactly as she said." Lot 666 is a shattered chandelier that is claimed by the auctioneer to have been related to "the strange affair of the Phantom of the Opera, a mystery never fully explained," having appeared in some great disaster in years past. As the chandelier - which has been replaced, in part, with new electric wiring - is uncovered, it illuminates as the years roll back and the Opéra returns to its 1880s grandeur ("Overture").

===Act I===
It is now 1881. As Carlotta, the Opéras resident soprano prima donna, rehearses for that evenings performance, a backdrop collapses without warning. "The Phantom! Hes here!" the anxious cast members whisper. The Operas new owners, Firmin and André, try to downplay the incident, but Carlotta refuses to continue and storms offstage. Meg Giry, the daughter of the Opéras ballet mistress Madame Giry, tells Firmin and André that Christine Daaé, a Swedish chorus girl and orphaned daughter of a prominent violinist, has been "well taught", and could sing Carlottas role. With cancellation of the performance their only alternative, the owners reluctantly audition Christine, and to their surprise she is equal to the challenge ("Think Of Me").

Backstage after her triumphant début, Christine confesses to Meg that she knows her mysterious teacher only as an invisible "Angel of Music" ("Angel of Music"). The new patron, Raoul, Vicomte de Chagny, finds Christine, his old childhood playmate, in her dressing room ("Little Lotte"). Christine reminds Raoul about the "Angel of Music" stories that her late father used to tell them, and confides that the Angel has been visiting her and given her her astonishing voice. Raoul good-naturedly accepts her revelation, taking it in stride, and he invites her to dinner, clearly hearkening back to - and desiring to continue - their childhood friendship. Christine, to his puzzlement, is not as willing, claiming that her Angel is a stern taskmaster and that he will not be pleased by her absence. Raoul exits, still intending to take Christine to dinner, and a jealous Phantom materializes in Christines mirror in the guise of The Angel of Music ("The Mirror/Angel of Music (Reprise)"). Christine begs him to reveal himself. The Phantom obliges, then guides her into a ghostly underground realm ("The Phantom of the Opera"). They cross a subterranean lake to his secret lair beneath the Opéra house. He then explains that he has chosen Christine to sing his music and serenades her ("The Music of the Night"). Overwhelmed, Christine faints and the Phantom carries her to a bed and expresses his adoration for her.

As the Phantom composes music at his organ ("I Remember…"), Christine awakens to the sound of the monkey music box. She slips up behind the Phantom, lifts his mask, and beholds his face. The Phantom rails at her, damning her for her curiosity, then ruefully expresses his longing to look normal—and to be loved by her ("Stranger Than You Dreamt It").

Meanwhile, inside the opéra house, Joseph Buquet, the Opéras chief stagehand—who, like Madame Giry, inexplicably knows much about the Phantom—regales everyone with tales of the "Opéra Ghost" and his terrible Punjab lasso ("Magical Lasso"). Madame Giry warns Buquet to exercise restraint. In the managers’ office, Madame Giry delivers a note from the Phantom: he demands that Christine replace Carlotta in the new opera, Il Muto, or there will be a terrible disaster "beyond imagination" ("Notes…"). Firmin and André assure the enraged Carlotta that she will remain the star ("Prima Donna"), but during her performance, disaster strikes ("Poor Fool, He Makes Me Laugh"). The Phantom reduces Carlottas voice to a frog-like croak. A ballet interlude begins, to keep the audience entertained. Suddenly, the corpse of Buquet, hanging from the Punjab lasso, drops from the rafters. Firmin and André plead for calm — "It was just an accident...simply an accident!" — as the Phantoms diabolical laughter is heard.

In the ensuing mêlée, Christine escapes with Raoul to the roof, where she tells him about her subterranean rendezvous with the Phantom. Raoul is skeptical ("Why Have You Brought Me Here?/Raoul, Ive Been There"), but swears to love and to protect her always ("All I Ask of You"). The Phantom, who has overheard their conversation, is heartbroken. He angrily vows revenge ("I Gave You My Music"), and destroys the Opéras mighty chandelier with a massive explosion as the curtain falls.

===Act II===
Six months later, in the midst of a splendid masquerade ball, the Phantom, costumed as the Red Death, makes his first appearance since the chandelier disaster ("Masquerade/Why So Silent?"). He announces to the stunned guests that he has written an opera entitled Don Juan Triumphant. He demands that it be produced immediately, with Christine (who is now engaged to Raoul) as the lead female role, and threatens dire consequences if his instructions are not implicitly obeyed before vanishing in a ghostly flash of fire and smoke. Raoul demands that Madame Giry tell him about the Phantom. She reluctantly replies that he is a brilliant musician and magician born with a terrifyingly deformed face, who escaped from captivity in a traveling freak show and disappeared.

During rehearsals, Raoul - tired of the tyranny with which the Phantom rules the Opera - thinks to use the premiere of Don Juan Triumphant as a trap to capture the Phantom and put an end to his reign of terror once and for all. Carlotta falsely accuses Christine of being the mastermind and that it is her plan so she can be the star. Christine confirms that it is not true, and no one believes Carlotta. Carlotta storms in rage. Raoul, knowing of the Phantoms obsession with his fiancé, he asserts that the Phantom will be sure to attend ("Notes/Twisted Every Way"). Christine, torn between her love for Raoul and her fear of/pity for the Phantom, visits her fathers grave, longing for his guidance ("Wishing You Were Somehow Here Again"). The Phantom appears, once again under the guise of the Angel of Music ("Wandering Child"). Christine, tired and heartbroken, once again accepts her "Angel" as a friend, nearly succumbing to the Phantoms influence, but Raoul arrives to both rescue her and confront her would-be captor. The Phantom taunts Raoul, magically summoning pillars of fire at them ("Bravo Monsieur"), until Christine begs Raoul to leave with her. Furious, the Phantom sets fire to the cemetery, promising revenge on them both.

Don Juan Triumphant opens with Christine and Ubaldo Piangi, the Opéras principal tenor, singing the lead roles. During Don Juans and Amintas duet, Christine comes to the sudden realization that she is singing not with Piangi, but with the Phantom himself ("The Point of No Return"). Mimicking Raouls vow of devotion from "All I Ask Of You", the Phantom once again expresses love for Christine and slips his ring onto her finger; Christine rips off his mask, exposing his horrifically deformed face to the shocked audience. Chaos ensues. Piangis garroted body is revealed backstage, the cast and audience fly into a state of panic, and the Phantom seizes Christine and flees the theatre. An angry mob, vowing vengeance for Buquets and Piangis untimely demises, searches the theatre for the Phantom, while Madame Giry tells Raoul how to find the Phantoms subterranean lair, and warns him to beware his Punjab lasso.

In the lair, Christine is forced to don a wedding dress ("Down Once More/Track Down This Murderer"). Raoul finds the lair and attempts to persuade the Phantom to spare Christine, pleading that the Phantom vent his wrath upon him instead. The Phantom refuses, claiming that his intention was never to hurt Christine for Raouls "sins." The Phantom captures Raoul with his lasso and turns to Christine - he bargains that he will free Raoul if she agrees to stay with him forever; if she refuses, Raoul will die. Christine, heartbroken, indicates to the Phantom that it is his soul that is deformed, not his face, and kisses him full on the lips showing him compassion but not realizing that she would feel something more and kisses him again longer with all the passion in her. The Phantom, having experienced kindness, compassion and love for the first time, sets them both free giving Christine the life he cannot in darkness and underground. Christine returns the Phantoms ring to him, and he tells her he loves her as she returns his ring. Clearly struggling, Christine forces herself to turn away knowing she loves him but has to leave and exits with Raoul. Now out of the thrall of his madness, the Phantom makes his way to his throne and covers himself with his cloak, lamenting all the while for Christine. The mob storms the lair and Meg pulls away the cape—but the Phantom has vanished; only his mask remains.

===Grand Finale===
After the cast bow-out, Andrew Lloyd Webber delivered a speech to the audience before bringing out the Royal Albert Hall creative team, the original creative team, as well as the original leads from both the London and Broadway productions,   from the Canadian production, Anthony Warlow from the Australian production, Peter Jöback, who played the role on both the West End and Broadway, John Owen-Jones from the London and UK Tour productions, and Ramin Karimloo. The performance concluded when the five Phantoms sang "The Music of the Night", along with the entire cast and creative teams.

==Cast== The Phantom
* Sierra Boggess as Christine Daaé
* Hadley Fraser as Viscount Raoul de Chagny|Raoul, Vicomte de Chagny
* Wendy Ferguson as Carlotta Giudicelli
* Liz Robertson as Madame Giry
* Daisy Maywood as Meg Giry
* Barry James as Monsieur Richard Firmin
* Gareth Snook as Monsieur Gilles André
* Wynne Evans as Ubaldo Piangi
* Nick Holder as Joseph Buquet
* Earl Carpenter as Auctioneer

==Home video==
The production was recorded live and later released on Blu-ray, DVD, CD and digital download. The initial releases were in the UK on 14 November 2011. The digital download format was released three days earlier on 11 November. The North American release followed on 7 February 2012.

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 