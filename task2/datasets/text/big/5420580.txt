Look, Up in the Sky: The Amazing Story of Superman
 
{{Infobox Film
| name = Look, Up in the Sky! The Amazing Story of Superman
| image = Look, Up in the Sky - The Amazing Story of Superman, DVD boxart.jpg
| caption =
| writer = James Grant Golding   Steven Smith
| starring = Brandon Routh Dean Cain Gerard Christopher
| music =
| cinematography =
| editing =
| director = Kevin Burns
| producer = Scott Hartford Mark McLaughlin
| distributor = DC Comics Warner Home Video
| released = June 20, 2006 (USA)
| runtime = 110 mins English
| budget =
}} big screen. outtakes from the Christopher Reeve Superman films, including an outtake of Marlon Brando improvising during the recitation of a poem in a scene deleted from the original version of Superman II.

The documentary was released on DVD on June 20, 2006, shortly before the theatrical release of Superman Returns.  A two DVD Best Buy exclusive Limited Edition version was released the same day (extra material on the second disc included Bryan Singers video journals and five official Superman movie poster mini-prints).  A shortened version of the documentary was played on A&E Network|A&E on June 12, 2006. Finally, the DVD (along with Singers video journals) was included as part of the 14-disc box set release Superman Ultimate Collectors Edition, in November 2006.

==Cast==
Kevin Spacey, who also played Lex Luthor in Superman Returns, narrates the documentary. Bill Mumy and Mark Hamill are also credited as consultants on the documentary. The rest of those appearing are listed below. 

Those directly associated with Superman:
* ; Curtis Knox, Smallville)
*Gerard Christopher (Clark Kent/Superboy (Kal-El)|Superboy, Superboy (TV series)|Superboy)
* ,  )
* )
*Alfred Gough and Miles Millar (executive producers and creators of Smallville)
* , Smallville)
*Jack Larson (Jimmy Olsen, Adventures of Superman (TV series)|Adventures of Superman (TV series); Bo the Bartender, Superman Returns)
*Bill Mumy (Tommy Puck, Superboy)
*Noel Neill (Lois Lane, Adventures of Superman, Superman (serial)|Superman film serial; Lois Lanes Mother, Superman: The Movie; Gertrude Wanderworth, Superman Returns) Martha Kent, Smallville)
*Brandon Routh (Clark Kent/Superman, Superman Returns)
*Ilya Salkind (Executive Producer, Superman: The Movie, Superman II, Superman III)
*Bryan Singer (Director, Superman Returns)
*Lesley Ann Warren (Lois Lane, Its a Bird...Its a Plane...Its Superman )
Those not directly associated with Superman: The Joker in animated incarnations. He is best known for his role as Luke Skywalker in Star Wars) Hulk and the X-Men)
*Gene Simmons (musician and comic book fan)
* , Batman (TV series)|Batman)

==References==
 

==External links==
*  - An extended preview of the film
* 
*  (Further down the page)
* 
*  
*  

 

 
 
 