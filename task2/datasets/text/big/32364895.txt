Poongsan
 
{{Infobox film
| name           = Poongsan 
| image          = Poongsan2011Poster.jpg
| caption        = Theatrical poster
| film name      = {{Film name
 | hangul         =  
 | rr             = Pungsan-gae
 | mr             = Pungsangae}}
| director       = Juhn Jai-hong
| producer       = Jeon Yoon-chan Kim Ki-duk 
| writer         = Kim Ki-duk Kim Gyu-ri
| music          = Park In-young
| cinematography = Lee Jeong-in 
| editing        = Shin Cheol
| studio         = Kim Ki Duk Films
| distributor    = Next Entertainment World 
| released       =  
| runtime        = 121 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          =   
}} thriller film Kim Gyu-ri.

Poongsan, whose real name is never revealed, is a mysterious messenger who crosses the demarcation line between the two Koreas, delivering letters and cherished heirlooms between separated families in Seoul and Pyongyang. 

One day he is commissioned by South Korean government agents with the task of smuggling in In-ok, the beautiful lover of a high-ranking North Korean defector, into the South. Yet things take an unexpected turn when the deliveryman falls for the young woman, but their romance is put in jeopardy by the obstacles they encounter.

Part love story, part postwar tragedy, with a dose of comedy thrown in, the low-budget film Poongsan depicts the sad reality of the division between the two Koreas.

==Plot== Korean DMZ, Kim Gyu-ri), from Pyongyang to her lover (Kim Jong-soo), a high-ranking North Korean official who recently defected and is still guarded by NIS agents. The arrogant official, who is paranoid about being assassinated, has been holding out on writing a report for the NIS until In-ok joins him. On the journey across the DMZ, In-ok accidentally sets off a mine that almost kills her and Poongsan, and also has to be revived by mouth-to-mouth resuscitation when she almost drowns. The mission is successful but In-ok has become attached to the man who saved her life. Suspicious that the two made love during the crossing, the official abuses In-ok after they are reunited and she expresses a desire to return to the North. Meanwhile, Poongsan is tortured by an NIS team leader (Choi Mu-seong) to find out whether he is a North Korean agent, but is rescued by the team leaders boss (Han Gi-jung). Poongsan is forced to rescue NIS agent Kim Yong-nam, whos been caught in the North and is under interrogation; in gratitude, and appalled by his own agencys methods, Kim later helps Poongsan escape from the NIS control. But then Poongsan and In-ok are captured by North Korean agents in the South. 

==Cast==
*Yoon Kye-sang ... Poongsan Kim Gyu-ri ... In-ok
*Kim Jong-soo ... North Korean defector
*Han Gi-jung ... section chief
*Choi Moo-sung ... team leader
*Yoo Ha-bok ... North Korean public officer
*Kim Yun-tae ... chief bodyguard
*Joe Odagiri ... North Korean border guard 1 (cameo)

==Production==
Written and co-produced by Kim Ki-duk, and directed by his protégé Juhn Jai-hong, the   ( ) film was shot over 25 takes in just 30 days, while the entire cast and crew took part in the project with no guarantee (filmmaking)|guarantee. "I wanted to show that it is possible to make a film with passion rather than money," Juhn told reporters at a press preview. 

Kim handed Juhn the story idea for Poongsan in fall 2010, saying he wanted a young director to bring a fresh perspective to inter-Korean issues. Juhn, known as an "artsy" filmmaker (his directorial debut  . Retrieved 2013-03-11. 
 Lord of the Rings franchise, edited the soundtrack. 

==References==
 

== External links ==
*    
*  
*  
*  

 
 
 
 
 
 
 