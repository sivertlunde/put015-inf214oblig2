The Cry (2007 film)
{{Infobox film
| name = The Cry
| image = Poster of the movie The Cry.jpg
| director = Bernadine Santistevan
| producer = Lara Blum Juan Dapena Javier Ramirez Sandoval Bernadine Santistevan
| writer = Bernadine Santistevan Monique Salazar
| starring = Adriana Domínguez Christian Camargo Carlos Leon Miriam Colon
| music = Dean Parker
| cinematography = Richard Lopez
| editing = Dario Bigi Doug Forbes   
| distributor = Monterey Media
| released =  
| runtime = 83 minutes
| country = United States
| language = English
| budget = 
| gross = $21,427 
}}
The Cry (also called La Llorona, which translates to "The Crying Woman") is a 2007 independent horror film directed by Bernadine Santistevan and co-written with Monique Salazar.    , "The Cry - La Llorona (2008)", accessed 01-04-2009    

==Synopsis== New York, detective Alex Scott (Christian Camargo) is investigating the mysterious disappearance of several children. They interview Gloria the Curandera (Miriam Colon) who advises that an evil force is pursuing the reincarnation of her son and is drowning missing children to bring pain to their mothers. 

==Cast==
* Adriana Domínguez as Maria 
* Christian Camargo as Alex Scott 
* Carlos Leon as Sergio Perez 
* Miriam Colon as Gloria the Curandera 
* Kate Blumberg as Judy Hardwich 
* Jayden Vargas as Tonio 
* Quinn McCann as Ryan Weit 
* Jane Petrov as Lynn Weit 
* Ron Dailey as Man with Stroller 
* Lisa G. as Reporter Diane Penn 
* Izzy Ruiz as Detective Vega 
* Kristin Taylor as Detective Taylor 
* Caroline Cole as Assistant D.A. Tanin 

==Background== Mexican urban mythos where the goddess Cihuacoatl was said to have taken the form of a beautiful lady draped in white garments to predict the death of her children. This early myth evolved into the modern Mexico version of La Llorona, a woman who, betrayed by her husband, drowned her children out of revenge. As punishment for this horrific act, La Llorona’s spirit is condemned to roam the earth for eternity, crying for her children.  

==Theatrical and festival release== Ravenna Nightmare Film Festival on October 30, 2007, after which it was picked up for DVD distribution by Monterey Media. 

==Reception== Box Office Movie Reviews panned the film by stating "...Less-than-middling attempt to exploit the potent Mexican myth of La Llorona (the crying woman) fails largely due to a crutch-like reliance on already weak genre conventions and haphazard script".   Kryten Syxx of Dread Central also felt the film was meritless, and after watching it concluded, "Bernadine Santistevan has some talent hidden somewhere, but it sure isn’t used here".   Being more forgiving, Justin Felix of DVD Talk wrote "Despite The Crys letdown of an ending, it was still an interesting character-driven horror film".   The Cry does have its supporters.  Anthony Thurber of Film Arcade wrote, the "screenplay written by Santistevan and writer Monique Salazar was very frightening. They make this film haunting and very disturbing".  Elliot Kotek of Moving Pictures Magazine stated, "Half the brilliance in the films direction is its speed, so any awkward moments are over quickly and, by not over-penning the piece with long conversations, the filmmakers are rewarded with a rich rising tension often lacking in more fiscally-blessed flicks".   And Best Horror Movies wrote "The Cry is suspenseful and shocking, especially in light of the victims of this terrible curse". 

==References==
 

==External links==
*  
*  
*   at AbandoMoviez
*   at american-vulture.com
*   at dvdverdict.com

 
 
 
 