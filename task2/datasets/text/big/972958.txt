The Transporter
 
{{Infobox film
| name           = The Transporter
| image          = Transporterposter.jpg
| alt            = Jason Statham wearing a suit and holding two guns, in black and white.  A band of orange colour with the title Transporter is overlaid in the middle of the picture.
| caption        = Film poster
| director       = Louis Leterrier Corey Yuen
| producer       = Luc Besson Stephen Chasman
| writer         = Luc Besson Robert Mark Kamen
| starring       = Jason Statham Shu Qi François Berléand Matt Schulze Aivis Vlasevičs
| music          = Stanley Clarke
| cinematography = Pierre Morel
| editing        = Nicolas Trembasiewicz TF1 Films Production Current Entertainment Canal+
| distributor    = 20th Century Fox  (United States)  EuropaCorp. Distribution  (France) 
| released       =  
| runtime        = 92 minutes   
| country        = France
| language       = English French Chinese
| budget         = $21 million 
| gross          = $43,928,932 
}} action thriller film directed by Louis Leterrier and Corey Yuen and written by Luc Besson, who was inspired by BMW Films The Hire series.
 Frank Martin, a driver for hire&nbsp;– a mercenary "transporter" who will deliver anything, anywhere&nbsp;– no questions asked&nbsp;– for the right price. It also stars Shu Qi as Lai Kwai.

It is the first film in a   premiered in 2012 on October 11 in Germany on RTL and on December 6 in France on M6.

== Plot synopsis ==
Frank Martin (Jason Statham) is a highly skilled driver known only as "The Transporter." He will transport anything, no questions asked, always on time, and he is known as the best in the business. He strictly follows three rules when transporting: Rule Number 1: "Once the deal is made, it is final", Rule Number 2: "No names", and Rule Number 3: "Never open the package." Frank has been hired to transport some robbers from a bank heist.  On delivery to their destination, they hoist an extra man in the car so he refuses to drive until they decide to kill one robber. Later they offer more money for Frank to drive further from the city. He refuses the deal. The robbers escape in another car, and Frank leaves.
 police Inspector Tarconi (François Berléand) arrives to question Frank about the robbery. However, Tarconi has no concrete proof about the heist and leaves. Frank is then hired to deliver a package to an American gangster, Darren "Wall Street"  Bettencourt (Matt Schulze). While changing a flat tire, he notices something moving in the package. Along the way, he opens the package and finds a bound and gagged woman. She manages to escape but Frank recaptures her and disables two policemen who spot them. He delivers the package to Bettencourt as promised.

Bettencourt then asks Frank to transport a briefcase; Frank accepts the job. He stops at a gas station to rest but the briefcase turns out to be a bomb that destroys his car. Frank snaps and returns to Bettencourts residence with vengeance and beats up several of his henchmen in a fight. After which, he steals a car to get away, only to find "the package" tied to a chair in the back seat. He brings her along to his house where she introduces herself as Lai (Shu Qi). The next day, Tarconi arrives and asks about Franks car. Lai says she is Franks new cook and, afterwards, girlfriend, supporting Franks alibi. Tarconi again leaves with no proof. Shortly after he leaves, Bettencourts henchmen rain missiles down on the house, with Frank and Lai barely escaping.
 human trafficker and that he is shipping two containers full of Chinese people including her family, and is planning to sell them into slavery. Lai and Frank go to Bettencourts office where Frank holds Bettencourt at gunpoint and asks him why he tried to kill him. Bettencourt replies that he didnt have a choice as Frank "opened the package". He also reveals that Lais father, Kwai (Ric Young), is also a human trafficker and that they are partners. Kwai arrives and his henchmen subdue Frank just as Tarconi arrives at the office. When Tarconi enters the office, Lais father and Bettencourt accuse Frank of kidnapping Lai. Tarconi has Frank arrested and locked up in the station.

At the station, Tarconi agrees to aid Franks escape as his faux hostage. Frank then tracks the criminals to the docks, where they load the containers onto trucks. However, Frank is spotted and is forced to fight his way through the guards, failing to stop the trucks. He then steals a small airplane and parachutes onto one of the trucks. After a lengthy fight, Frank manages to kill Bettencourt and some of his henchmen, gets out of the truck, only to be ambushed by Lais father. However, Frank is saved when Lai shoots her own father. Afterwards, Tarconi arrives with the police and they rescue the people trapped inside one of the two containers.

== Cast ==
* Jason Statham as Frank Martin/The Transporter
* Shu Qi as Lai Kwai
* François Berléand as Inspector Tarconi
* Matt Schulze as Darren "Wall Street" Bettencourt
* Ric Young as Mr. Kwai

== Releases ==

=== Theatrical release ===
The Transporter premiered in 2,573 theaters. With a production budget of $21,000,000 it grossed $25,296,447 in the United States and a total of $43,928,932 worldwide. 

=== Cut and uncut releases ===
The film was cut to receive a PG-13 rating in the United States, and this version was also released in the United Kingdom and several other countries. Japan and France received the uncut versions. Certain sequences of violence were either cut or toned down for the PG-13 cut. These include:
*The fight on the bus, which included Frank using a knife.
*The final fight on the highway, where Frank fights Wall Street in the truck. In the original French version, Wall Street is crushed beneath the wheels of the truck after Frank throws him from it. In the US PG-13 version, he is simply thrown out of the truck and onto the highway.

The uncut fight on the bus can be seen in the "Extended Fight Sequences" on the North American DVD, but with no sound.

The Japanese region-free Blu-ray cut of this film has the original uncut French version of the film. It also has several special features and deleted scenes. However, it does not include the North American special feature of the uncut fight scenes (with no sound). The uncut version of Transporter 2 is also included in this special boxed set.

=== Soundtrack ===
# Tweet – "Boogie 2Nite"
# Nate Dogg – "I Got Love"                                                                                                               
# Fat Joe featuring Angie Martinez and Sacario – "Live Big (Remix)"†                                                                                                                                 
# Benzino – "Rock The Party"†
# Knoc-Turnal – "Muzik"
# Angie Martinez featuring Lil Mo and Sacario – "If I Could Go!"†
# Tamia – "Be Alright"†
# Missy Elliott – "Scream AKA Itchin 
# Gerald Levert – "Funny"†
# Hustlechild – "Im Cool"†
# Keith Sweat – "One on One"†
# Nadia – "Life of a Stranger"
† indicates that the song did not appear in the film

=== Home media ===
The DVD version was released on 23 October 2003. It included fifteen minutes of extended fight scene footage and a feature-length commentary. On 23 August 2005, the film was released again in a "Special Delivery Edition". This version included all the features of the original release plus a new behind-the-scenes documentary, a making-of featurette, and a storyboard-to-film comparison. The film was also released as a part of "The Transporter Collection", which featured the first two films in the series. A Blu-ray Disc|Blu-ray format was released on November 14, 2006.

== Reception ==
The Transporter has received mixed reviews. On  , which assigns a weighted average score out of 100 to reviews from mainstream critics, the film received an average score of 51 based on 27 reviews, indicating "mixed or average reviews". 

Manohla Dargis, of the Los Angeles Times, complimented the action, saying, "  certainly seems equipped to develop into a mid-weight alternative to Vin Diesel. Thats particularly true if he keeps working with director Corey Yuen, a Hong Kong action veteran whose talent for hand-to-hand mayhem is truly something to see." 

Roger Ebert took the opposite stance, stating, "Too much action brings the movie to a dead standstill." 

Eric Harrison, of the Houston Chronicle, says, "Its junk with a capital J. The sooner you realize that, the more quickly you can settle down to enjoying it." 

== See also ==
 

== References ==
 

== External links ==
*  
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 