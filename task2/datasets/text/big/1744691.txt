Intimate Relations (1996 film)
{{Infobox film
| name           = Intimate Relations
| image          = Intimate-relations.jpg
| image_size     =
| caption        = 
| director       = Philip Goodhew
| producer       = Angela Hart Lisa Hope Jon Slann
| writer         = Philip Goodhew
| starring       = Julie Walters Rupert Graves Laura Sadler
| music          = Lawrence Shragge
| cinematography = Andres Garreton
| editing        = Pia Di Ciaula
| distributor    = Fox Searchlight
| released       = 10 September 1996 (Toronto F.F.) 6 June 1997 (US) 20 June 1997 (UK) 11 September 1997 (Aus)
| runtime        = 105 minutes
| country        = Canada United Kingdom
| language       = English
| budget         = 
| gross          =
}}
Intimate Relations is a 1996 Canadian-British film, the first movie by writer and director Philip Goodhew. It stars Rupert Graves, Julie Walters and a fifteen-year-old Laura Sadler, the only feature film in her short career. The film is a drama and black comedy about a young man who has an affair with the middle-aged housewife he is lodging with. Matters are soon complicated when the housewifes teenage daughter gets involved after developing a crush on the young lodger.

The film takes place in the 1950s in the suburbs of London. The film depicts the hypocritically prudish residents of a seemingly respectable household who, behind closed doors, indulge in the sort of sordid goings on they would publicly sneer at.

==Plot==
Marjorie Beaslie (Walters) is a housewife in her forties who takes in a lodger named Harold Guppey (Graves), who has just stumbled into town to look up his long-lost brother (played by Les Dennis). Although seemingly prudish (she no longer sleeps in the same bed as her husband, for "medical reasons"), Marjorie takes a liking to Harold despite him being a good twenty-years her junior. They begin to have a clandestine affair, sneaking into bed together at night. Ever since taking in her lodger, Marjorie insists that Harold refer to her as "mum", giving more than a little oedipal slant to their subsequent lustful antics.

Marjories youngest daughter is fourteen-year-old Joyce (Sadler), a precocious, Lolita-like girl who alternates between trying to act grown up by putting on make up and smoking cigarettes, and acting childish by grossing people out with tales of medieval punishments and giggling at rude words. 

Joyce is fascinated by Harold and with her teasing behaviour she cunningly turns him from being apathetic towards her to being intrigued by her. At one point, she catches Harold in bed with her mum, but seemingly does not realise what they are up to and merely thinks theyre having an innocent "bunk up". She talks her way into getting them to let her climb into the bed, and Harold and Marjorie continue their intimate relations whilst Joyce is asleep, or rather, pretending to be and steadily realising what is actually going on. A few days later, Joyce blackmails Harold into taking her to a hotel for the night, where he turns the tables on her and all but seduces her before spurning her.

Marjories husband, Stanley, is a one-legged World War I veteran, older than his wife, who sleeps in a separate room from his wife and is as oblivious to all the sordid antics of his wife and daughter initially as the rest of the suburban neighbourhood is.

Sick of being caught between a mother and daughter, who are too old and too young for him respectively, Harold tries to get out of the house and move away, joining the army and getting a new more suitable girlfriend but Marjorie manages to emotionally blackmail him into coming back.

One day, Harold takes Marjorie and Joyce out for a picnic, although things are tense between the trio. Having sent her daughter Joyce away to play, Marjorie begins to ravish Harold, but Joyce returns and hits her mother with an axe. Harold panics and attempts to get Marjorie into the car to take her to hospital but, with blood streaming down her face, Marjorie manages to pick up a knife Harold drops and attacks him with it. Harold fights Marjorie off and stabs her to death. Joyce then tries to attack Harold and so he stabs her to death too. Finally, Harold stabs himself in the stomach in an attempt to emphasise that is actions were out of self-defence. The post-script to the movie says that he was sentenced to death for Joyces murder, but the sentence was commuted to life imprisonment, and that a charge of murdering Marjorie was dropped due to lack of evidence.

==Inspiration==
The film is based on the true story of Albert Goozee, who was put on trial in 1956 in England. He was arrested after his landlady and her teenaged daughter were found murdered. Goozee was tried only for the murder of the teenaged girl (Joyce, in the movie), convicted and imprisoned for life. The movie follows Goozees own version of events, portraying him as an increasingly desperate young man caught in a love triangle between a mother and daughter, although as the only survivor there is no way of verifying if his version of the events was entirely truthful. 

Goozee was released from prison in 1971 but was imprisoned again in 1996, the year Intimate Relations came out, for unrelated sex offences. 

==Notes==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 