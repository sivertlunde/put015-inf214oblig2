Fantasm
 
{{Infobox film
| name           = Fantasm
| image          = 
| image size     =
| caption        =  Richard Franklin (as "Richard Bruce")
| producer       = Antony I. Ginnane
| writer         = Ross Dimsey
| based on = an idea by Antony I Ginnane
| narrator       =
| starring       = John Bluthal
| music          = 
| cinematography = Vince Monton
| editing        = Tony Patterson (as "Ford Footpole")
| studio = TLN Film Productions
| distributor    = Filmways
| released       = 16 July 1976
| runtime        = 
| country        = Australia English
| budget         = A$50,000   
| gross = A$650,000 (Australia) Beilby, Peter; Murray, Scott (January/February 1979). "Antony I. Ginnane". Cinema Papers: 175. 
| preceded by    =
| followed by    =
}} softcore pornographic Richard Franklin under a pseudonym. It was followed by a sequel, Fantasm Comes Again, the following year, directed by a pseudonymous Colin Eggleston.

==Plot==
German psychiatrist Professor Jurgen Notafreud takes the audience through a series of female sexual fantasies including:
*sex in a beauty salon
*fruit fetishism
*lesbianism in a sauna
*teacher student seduction
*rape in a gym
*transvestism
*reverse Oedipus complex.

==Cast==
*Dee Dee Levitt as Abby
*Maria Arnold as Barbara
*Bill Margold as husband
*Gretchen Gayle as Gabrielle
*Rene Bond as Dianne
*Al Williams as rapist
*Con Covert as transvestite
*Mara Lutra as Felicity
*Uschi Digart as Super Girl
*Maria Welton as Irish John Holmes as Neptune
*Mary Gavin (Candy Samples) as Belle
*Gene Allan Poe as son
*Robert Savage as visitor 1
*Kirby Hall as visitor 2
*Shayne as Celeste
*Sue Doloria as Harriet
*Al Ward as teacher
*Clement St George as Satanist
*Serena as victim
*John Bluthal as Professor Jurgen Notafreud

==Production==
In 1974 and 1975 Antony I Ginnane decided to enter the production field. He attempted to set up a Roger Corman type "nurses" film which he would produce and direct budgeted at $250,000 and then a crime drama set against the background of the massage parlour business called Sexy Little Me budgeted at $150,000, but was unable to find the money. However he could raise $50,000 and allocated directing duties to Richard Franklin, with whom Ginnane had worked with on the overseas marketing for The True Story of Eskimo Nell. 

Franklin and Ginnane wanted to make something commercial so their options were a bikie, horror or sex film. They decided to make a sex film with Franklin pushing to make it more of a comedy. Ross Dimsey wrote a script, originally called Fantale, which was a send up of Swedish sex education films such as  Language of Love.  Franklin:
 With this thin veneer of medical therapy, or whatever, these films were being shown in these little underground cinemas and so on around the place. They were all essentially softcore, and we decided to make a kind of send-up of one, really. Not because we wanted to do a send-up, but because we didnt think we wanted to do a genuine one. We just wanted to make a fun film about sex!    
Ginnane and Franklin had trouble finding actors who would appear in the film in Australia so only the  linking scenes with the professor were shot in that country, with the sex scenes filmed in Los Angeles by Franklin and his cinematographer Vince Monton, using American porn stars.  An old classmate of Franklins from USC, Doug Knapp, was working in the area of porn and put them in touch with casting agent Bill Margold.

The US shoot took ten days, the Australian shoot took one day.    Franklin says that John Bluthal, who played the professor, ad-libbed some of his dialogue, and that the porn actors were paid around $200 a day, except for John Holmes who was paid around $400 a day.  Franklin:
 That was an era where hardcore was NOT being produced in L.A. In fact, as I recall, we had the police call in on more than one occasion while we were shooting, just to make sure we werent doing hardcore. But I was told, and I guess it would be evident if you studied the films of the era, but virtually all of the actors did hardcore work, but they did that in San Francisco or in Europe. But it was just a short period when hardcore porn was not… There was no problem shooting what they call T and A, tits and arse, but you werent allowed to have people fucking on the set.  
Despite the presence of American actors, Franklin says it was always his intention that the film be made primarily for the Australian market. 

==Reception==
Although the movie was entirely soft core, around 45 seconds were cut out by the Australian censor prior to release. It was originally banned completely in the UK by the BBFC and is still only available with heavy cuts. 

The film was extremely popular at the box office and by 1979 had taken approximately $650,000 in Australia, despite being banned in Queensland.  It led to a sequel Fantasm Comes Again (1977). 

Franklin says its success enabled him to get financing for Patrick.
 Even though I had done it under a pseudonym, it was perceived that I could make films that would make money... I usually dont list it in my filmography, not because Im ashamed to have done it - I dont know whether Coppola lists HIS in his filmography! - but because it was such a low budget thing, and done in as I recall ten or eleven days, so it was really just like a series of student films strung together, if that makes sense. So I dont really, even though it runs feature length, I dont sort of think of it as a feature film.  

==References==
 

==External links==
*  at IMDB
*  at Oz Movies
 
 
 
 
 
 