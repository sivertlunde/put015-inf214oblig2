Un novio para Yasmina
{{Infobox film
| name           = Un novio para Yasmina 
| image          = 
| caption        = 
| director       = Irene Cardona
| producer       = Tragaluz, Tangerine Cinema Services
| writer         = 
| starring       = Sanaa Alaoui, José Luís García Pérez, María Luisa Borruel, Francisco Olmo, Paca Velardiez, José Antonio Lucía
| distributor    = 
| released       = 2008
| runtime        = 95
| country        = Spain
| language       = 
| budget         = 
| gross          = 
| screenplay     = Irene Cardona, Nuria Villazán
| cinematography = Ernesto Herrera
| editing        = Jorge Berzosa
| music          = Oscar López-Plaza
}} 2008 film.

==Synopsis==
Lola loves weddings, but her marriage is in crisis and she suspects that Jorge, her husband, has fallen in love with Yasmina. Yasmina wants to marry Javi as soon as possible, but Javi, a local policeman, prefers to take it slowly. Alfredo doesn’t believe in marriage, but he wouldn’t mind marrying for friendship… or money. The film is a summer tale concerning arranged marriages, social commitment and life as a couple.

==Awards==
* Festival de cine español (Málaga, 2008)

==References==
 

 
 
 


 