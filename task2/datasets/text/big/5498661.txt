Never Talk to Strangers
{{Infobox film
| name           = Never Talk to Strangers
| image          = Never talk to strangers.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster Peter Hall
| producer       = Andras Hamori Jeffrey R. Neuman Martin J. Wiley Rebecca De Mornay Robert Lantos Barbet Schroeder
| writer         = Lewis Green Jordan Rush
| starring       = Rebecca De Mornay Antonio Banderas Dennis Miller
| music          = Pino Donaggio Steve Sexton
| cinematography = Elemér Ragályi
| editing        = Roberto Silvi
| studio         = Alliance Atlantis
| distributor    = TriStar Pictures
| released       =  
| runtime        = 86 minutes
| country        = United States Canada Germany
| language       = English
| budget         = $6.4 million
| gross          = $6,858,261 
}} thriller film Peter Hall and starring Antonio Banderas and Rebecca De Mornay.

==Plot==
Psychologist Dr. Sarah Taylor (Rebecca De Mornay) is a guarded, aloof criminal psychologist who interviews a client who is a rapist, and is pleading not guilty by reason of insanity. It is later revealed that she was the subject of daily rapes as a child by her estranged father, who is now shown to be very ill. Sarah meets Tony Ramirez (Antonio Banderas) in a shopping mall, and she gives him her number. She begins a relationship with Tony, despite the creepy advances of her neighbor, Cliff (Dennis Miller), with whom she once had a one-night stand.

Days into this new relationship, Sarah begins to get death threats and strange gifts, such as dead flowers. As she gets more romantic with Tony, the gifts get more extreme. Her lifelong cat is dismembered, at which point Sarah goes to the police. Sarah then hires a detective and has Tony followed, and breaks into his apartment only to discover that he has a file on her, including information about her mothers death in a car accident, twenty years before. Tony is actually investigating her, trying to learn the whereabouts of a former boyfriend of hers who had disappeared suddenly. 

Ultimately, it is revealed that Sarah suffers from multiple personality disorder, brought about from her abuse as a child, and from her father brainwashing her to cover up the murder of her mother. Her alternate personality is responsible for all of the strange gifts, and for murdering her ex-boyfriend. When Tony goes to her fathers house, Sarah (under the control of her alternate personality) follows him, shoots and kills him there, and then kills her father when he tries to intervene. When Sarah reverts to her normal personality, not remembering what has happened, she presumes that Tony was crazy and killed her father, and that she killed Tony in self-defense. In the end, she is seen entering into a relationship with Cliff.

==Cast==
* Rebecca De Mornay as Dr. Sarah Taylor
* Antonio Banderas as Tony Ramirez
* Dennis Miller as Cliff Raddison
* Len Cariou as Henry Taylor
* Harry Dean Stanton as Max Cheski
* Eugene Lipinski as Dudakoff
* Martha Burns as Maura
* Beau Starr as Grogan
* Phillip Jarrett as Spatz Tim Kelleher as Wabash

==Reception==
Never Talk to Strangers received negative reviews, holding only 15% on Rotten Tomatoes. 

==Soundtrack==
The film features "Love Sick" performed by Alfonzo Blackwell.

==References==
 

==External links==
*  
*  
*  
*  
*   at Virtual History

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 