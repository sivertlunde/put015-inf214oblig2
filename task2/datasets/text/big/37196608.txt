Mam (film)
 
{{Infobox film
| name           = Mam
| image          = Mam_film_poster.jpg
| alt            =
| caption        = 
| director       = Hugo Speer
| producer       = Rob Speranza & Vivienne Harvey
| writer         =   Paul Barber  and Ronan Carter
| music          = Amelia Warner
| cinematography = Sam Care
| editing        = Josh Levinsky
| studio         = Vigo Films
| distributor    = Shorts International
| released       =  
| runtime        = 15 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
Mam is a 2010 British short film by writer Vivienne Harvey and director Hugo Speer. Produced by Vigo Films in association with South Yorkshire Filmmakers Network. It has a running time of 15 minutes.

== Plot ==
When Mam won’t get out of bed, 12 year-old Danny must fend for his brothers and sisters - whilst trying to protect a secret that threatens to break up the family forever.

== Cast ==
* Josie Lawrence as Reenie Paul Barber as The Chemist
* Ronan Carter as Danny
* Tisha Merry as Charlie
* Karren Winchester as The Neighbour
* Charlie Street as Jimmy
* Katie Gannon as Lauren
* Patrick Downes as Tommy
* Sylvie Caswell as Kyla
* Elly May Taylor as Debs
* Jodie McEnery as Gang Member
* James Varley as Gang Member
* Dwayne Scantlebury as Gang Member
* Danny Gregory as Gang Member
* Paul Tomblin as Gang Member

== Accolades ==
* Best Foreign Film - Williamsburg Independent Film Festival, Brooklyn, USA (2011)
* Best Yorkshire Short - Hull International Short Film Festival, UK (2011)
* Best Community Short - Rob Knox Film Festival, UK (2012)

== Festivals ==
{| class="wikitable"
|-
! Country !! Date !! Festival
|-
| UK || 20 November 2010 || Encounters Film Festival
|-
| UK || 15 January 2011 || London Short Film Festival
|- USA ||22 Slamdance Film Festival
|- USA ||5 Cinequest Film Festival
|- UK ||17 Bradford Film Bradford International Film Festival
|- UK ||29 East End Film Festival
|- Russia ||July Moscow Film Festival
|- UK ||July Rushes Soho Shorts Film Festival
|- UK ||August Deep Fried Film Festival
|- USA ||September 2011 || JC PowerHouse Short Film Festival
|- UK ||October 2011 || Hull International Short Film Festival
|- Germany ||November 2011 || Berlin Interfilm Festival
|- Mexico ||November Oaxaca Film Fest
|- UK ||November Aesthetica Short Film Festival
|- USA ||November 2011 || Williamsburg Independent Film Festival
|- Ireland ||December Kerry Film Festival
|- Germany ||January 2012 || British Shorts, Berlin
|}

== External links ==
*  
*  
*  
*  
*  

 
 
 
 
 