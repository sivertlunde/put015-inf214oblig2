We Are All Demons
{{Infobox Film
| name           = We Are All Demons
| image          = 
| image size     = 
| caption        = 
| director       = Henning Carlsen
| producer       = Henning Carlsen Bo Christensen
| writer         = Henning Carlsen Poul Borum Aksel Sandemose
| narrator       = 
| starring       = Lise Fjeldstad
| music          = 
| cinematography = Jørgen Skov
| editing        = Leif Erlsboe
| distributor    = Nordisk Film
| released       = 27 June 1969
| runtime        = 98 minutes
| country        = Denmark
| language       = Danish
| budget         = 
| preceded by    = 
| followed by    = 
}}

We Are All Demons ( ) is a 1969 Danish drama film directed by Henning Carlsen. It was entered into the 19th Berlin International Film Festival.   

==Cast==
* Lise Fjeldstad - Inger
* Hans Stormoen - Asbjørn Bauta
* Claus Nissen - Asthor Asbjörnsen
* Allan Edwall - Tor, den syke matrosen Peter Lindgren - First Mate
* Gunnar Strømvad - Besetningsmedlem
* Knud Hilding - Crew
* Jørgen Langebæk - Crew
* Erling Dalsborg - The Jew
* Ole Søgaard - Klaus Bornholmeren
* Torben Færch - Yngste lettmatros Ove Pedersen - Skipsgutten
* Margit Carlqvist - Nurse
* Flemming Dyjak - Esbjørn
* Ole Larsen - Toldbetjent Kim Andersen - Ingers søn Arnor

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 