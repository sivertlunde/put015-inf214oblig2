The Waning Sex
{{Infobox film
| name           = The Waning Sex
| image          = The Waning Sex.jpg
| image_size     = 200px
| caption        =
| director       = Robert Z. Leonard
| producer       = Harry Rapf Joe Farnham (titles) F. Hugh Herbert Frederica Sagor (uncredited)
| based on       =  
| starring       = Norma Shearer Conrad Nagel
| music          =
| cinematography = Ben Reynolds
| editing        = William LeVanway
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 70 minutes
| country        = United States Silent English intertitles
| budget         =
}}
 silent romantic comedy film directed by Robert Z. Leonard. Based on the 1923 play of the same name by Fanny and Frederic Hatton, the film starred Norma Shearer and Conrad Nagel.  The films survival status is currently lost film|lost. 

==Synopsis==
Nina Duane (Norma Shearer) is a criminal lawyer whose gender was professionally resented by Philip Barry (Conrad Nagel), the District Attorney. She wins acquittal for man-chasing widow Mary Booth (Mary McAllister), then defeats her in romancing the D.A.

==Cast==
* Norma Shearer - Nina Duane 
* Conrad Nagel - Philip Barry 
* George K. Arthur - Hamilton Day 
* Mary McAllister - Mary Booth 
* Charles McHugh - J.J. Flannigan 
* Tiny Ward - J.J. Murphy 
* Martha Mattox - Ellen B. Armstrong

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 


 
 