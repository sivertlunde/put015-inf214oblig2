Gentleman's Fate
{{Infobox film
| name           = Gentlemans Fate
| image          = Gentlemans Fate poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Mervyn LeRoy
| producer       = 
| screenplay     = Leonard Praskins 
| story          = Ursula Parrott John Gilbert Louis Wolheim Leila Hyams Anita Page Marie Prevost
| music          = 
| cinematography = Merritt B. Gerstad William S. Gray
| studio         = Metro-Goldwyn-Mayer
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 John Gilbert, Louis Wolheim, Leila Hyams, Anita Page and Marie Prevost. The film was released on March 7, 1931, by Metro-Goldwyn-Mayer.  

==Plot==
Jack Thomas has grown up in the belief that he was an orphan. His guardian tells him that he has an elder brother (Frank Tomasulo) and a dying father. They are both in the  liquor business prohibition era. 

On his deathbed the father gives Jack an emerald necklace. Jack gives it to his fiancé. It turns out that the necklace was stolen. Frank does not want his fathers name to be put in the dirt, so he forces his kid brother to admit that he stole the nacklace. So he does. His fiancé Marjorie Channing  breaks off the engagement and travels to England. Jack decides to join the family business. He kills the member of a rival mob, and its leader wants to revenge it by killing Jack.

== Cast ==	  John Gilbert as Giacomo Tomasulo / Jack Thomas
*Louis Wolheim as Frank Tomasulo
*Leila Hyams as Marjorie Channing
*Anita Page as Ruth Corrigan
*Marie Prevost as Mabel
*John Miljan as Florio George Cooper as Mike
*Ferike Boros as Angela
*Ralph Ince as Dante
*Frank Reicher as Papa Francesco Tomasulo
*Paul Porcasi as Papa Mario Giovanni
*Tenen Holtz as Tony 

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 

 