Bullet for a Badman
{{Infobox film
| name           = Bullet for a Badman
| image          =Bullet for a Badman.jpg
| caption        = Film poster
| director       = R.G. Springsteen
| producer       = Gordon Kay
| writer         = Mary Willingham Willard Willingham
| based on       = novel by Marvin H. Albert
| narrator       =
| starring       = Audie Murphy Darren McGavin
| music          = Frank Skinner
| cinematography = Joseph Biroc
| editing        = Russell F. Schoengarth
| studio         = Gordon Kay & Associates
| distributor    = Universal Pictures
| released       =  
| runtime        = 80 minutes
| country        = United States
| language       = English
| budget         = $500,000 Don Graham, No Name on the Bullet: The Biography of Audie Murphy, Penguin, 1989 p 292 
| gross          =
| website        =
}} Western film starring Audie Murphy. 

==Plot==
Logan Keliher, a retired Ranger, meets up with an old friend, Sam Ward, who has turned outlaw and is angry at Keliher for marrying his former wife. Ward and his gang rob a bank and Keliher goes to track him down.

==Cast==
*Audie Murphy as Logan Keliher
*Darren McGavin as Sam Ward
*Ruta Lee as Lottie
*Beverley Owen as Susan
*Skip Homeier as Pink
*George Tobias as Diggs
*Alan Hale Jr. as Leach
*Berkeley Harris as Jeff
*Edward Platt as Tucker
*Kevin Tate as Sammy
*Cece Whitney as Goldie
*Mort Mills

==References==
 

==External links==
*  
*  

 
 
 
 
 

 