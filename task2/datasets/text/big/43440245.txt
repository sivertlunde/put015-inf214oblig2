Talk Back and You're Dead
{{Infobox film
| image          = TBYDPoster.jpg
| caption        = left to right Joseph Marco Nadine Lustre and James Reid
                   Theatrical movie poster
| name           = Talk Back and Youre Dead  
| director       = Andoy Ranay 
| based on       = Talk Back and Youre Dead by Alesana Marie    
| producer       =  
| music          = 
| cinematography = 
| story          = 
| screenplay     = Keiko Aquino
| writer         = Alesana Marie     (novel)
| editing        = 
| starring       =  
| studio         =  
| distributor    = Viva Films
| released       =   
| country        = Philippines
| language       =  
| runtime        = 120 minutes 
| budget         =
| gross          = PHP 76,941,733       

 
}}
 romantic Comedy James Reid, Nadine Lustre and Joseph Marco. It was distributed by Viva Films and Skylight Films (a subsidiary of Star Cinema) and was released on August 20, 2014, in theaters nationwide.      

==Summary==
After an encounter with the schools leader, Samantha accidentally gets death threats from the elite leader TOP whom she unexpectedly crossed paths with again later on. She must bow down to his list of needs or he will take on her life. What ensues is a mystery of secrets, lies, deception and true love but her heart must make a choice when it comes to another leader of the LUCKY 13 gang, Red. When she is forced in a marriage she is not willing to commit, will this love end untimely in a demise or will she find the one in the two men she once despised?

==Plot==
Miracle Samantha Perez (aka Miracle, Sammy, Sam, Samantha) is a student of St. Celestine. She is the unica hija of Selene and Crisostomo Perez, the owners of a very famous company. While shopping, she sees her best friends, Michie Sta. Maria, Maggie Dela Vega and China Dela Vega, also known as the Crazy Trios, freaking outside the shop claiming that the boy in front of them had cheated on her. Enraged, Miracle charges at the group and slaps the boy. Unknowing to Miracle, the Crazy Trios are only acting and does not really know who the boy is. When Miracle finally realizes this, she runs away, but not until the scene has been captured on video and posted on a social networking site, where the boy, revealed to be Timothy Odelle Pendleton or TOP, a gang leader, comments that he will kill her.

The next day, the Lucky 13 gang visits St. Celestine to find Miracle and takes her to a club. There, TOP asks Miracle on a date. They are then disturbed by another gang lead by "Piggy". With only Red and TOP, they fight the whole gang. TOP is hurt and taken to the hospital as he took a hit intended for Miracle. Under the impression that TOP is dying, Miracle says that she will date him. At school, rumor has spread that Audrey Dela Cruz, Miracles rival, has a gangster boyfriend. But when TOP visits her school to fetch Miracle, the rumor has been proven false. TOP takes Miracle to his condo and introduces her to his parents as his girlfriend. After that, TOP breaks up with Miracle, implying that he only used her to stop his parents from bugging him to have a girlfriend. Although she does not care about TOP, she is still devastated at the break up. Audrey tells her that TOP is an ex-convict, to Miracles surprise. At a bar, Red asks TOP why he broke up with Miracle. TOP says it is because he had promised a girl named Sammy that he will marry her. Red tells TOP that Sammy and Miracle are the same.

Miracle is kidnapped by Piggys gang and taken to a warehouse but is later saved by the Lucky 13. Bitter because of the break up, Miracle sarcastically asks why an ex-convict would save her. TOP is hurt and leaves. Guilty, Miracle goes to Pendleton High, TOPs school, and asks him to go with her to a park, where they become friends. The next day, Miracle goes to see TOPs basketball game with his sister, Sweety. TOP does not want her there and, enraged, calls her a "stupid bitch". That night, TOP apologizes to Miracle and confesses that he likes her. TOP, along with his best friend and gang member Red Dela Cruz (Audreys brother) gives Miracle a teddy bear at her house, but sees that she is with her cousin, Lee Perez. Lee tells TOP to stay away because Miracle is set up on an arranged marriage. After they leave, Lee also tells Miracle to stay away from TOP and that her parents have come home. Miracle visits TOP at his condo and the two of them elope to TOPs beach house. The Lucky 13 finds them there. Red tells Miracle that she can no longer be with TOP or else their family will destroy TOPs familys company. She chooses to leave TOP because she does not want his family to suffer, seeing that TOP already has a strained relationship with his dad. At home, her parents tell her that TOP is an ex-convict before, kidnapped a girl who happened to be Miracles friend Amarie. They visit Amarie, who tells them that TOP is her half-brother, did not kidnap her but actually saved her. He took her to the same beach house to hide her from Amaries abusive father. But other people thought it was kidnapping and sent him to prison.

At a Christmas ball, TOP and Miracle make up again. But at that same night, the Perez and Dela Cruz clan announces that Miracle will be engaged to Red. Surprised, TOP punches Red at a terrace. The three fight, then Miracle tells him that she is sick of pitying TOP. TOP is hurt and leaves. Red asks her why she lied, answering that sometimes you just have no choice but to do/say things you dont want to but have to. Miracles parents also said that Miracle and Red will go to France for good. During a Christmas concert held by the Pendleton High, the Lucky 13 says that "He will always be waiting". Miracle goes to her own school where she finds TOP, who asks her if she will leave him again. They kiss and make up, knowing that Miracle will leave the next day for France. TOP promises her that he will wait for her.

2 years later, Miracle and Red goes back to the Philippines. Audrey says that TOP had been in a terrible accident and that no one can find him. Miracle goes to his beach house, where she finds an old tape of TOP and herself as kids. In another place at the same time, Red reveals that he, Audrey, TOP and Miracle were childhood friends. But Miracle suffered from amnesia as a kid and forgot her memories. She regains it back after watching the tape. A dog leads her outside to TOP, who asks God for a miracle to happen just before Miracle calls him. He has lost his eyesight and is blind because of an accident. They embrace and reunite, with TOP saying that he will never let her go again.

== Cast ==

===Main Cast=== James Reid as Timothy Odelle Pedleton "Top"
*Nadine Lustre as Miracle Samantha "Sam" Perez
*Joseph Marco as Jared "Red" Dela Cruz

===Supporting Cast===
*Yassi Pressman as Audrey Dela Cruz
*Via Carillo as Aphrodite "Sweety" Pendleton
*Aj Muhlach as Lee Perez
*Candy Pangilinan as Selene Perez
*Bobby Andrews as Mr. Pendleton
*Cristopher Roxas as Crisostomo Perez
*Jana Victoria as Mrs. Pendleton
*Isabelle Pressman as Aniya Marie du Courdrey

Crazy Trios
*Coraleen Waddell as Michie Sta. Maria
*Donnalyn Bartolome as China Dela Vega
*Rosalie Van Ginkel as Maggie Dela Vega

Lucky 13
*Bret Jackson as Jack Maunnick
*Arkin Del Rosario as Seven Barasque
*Josh Padilla as Six Barasque
*Carlo Lazerna as Dos Cetventez
*Kiko Ramos as Philip Morello
*King Certeza as Kyohei Sagara
*Aki Torio as Jun Morales
*Cliff Hogan as Romeo DArrez
*Billy Villeta as Alvince Montelegre
*Ryan Kevin as Mond Villaraza
*Clark Merced as Raine Montecillo

*Extended cast*
Maricar Mabazza

==Production==

===Casting===
Joseph Marco is the perfect role for Timothy "Top" Pendleton according to the author of the book, but the producers instead gave Marco the character of Jared "Red" Dela Cruz. 

===Deleted Scene===
The scene where Samantha lost her other shoe after TOP fights the gang members who chased her. Jared carries her home. The clip wasnt featured in the actual movie but can be seen in the trailer. Another scene that features Jared invites Samantha to a date. 

==Reception==

===Critical response===
Philbert Ortiz-Dy of Click the City gave a mixed review in the film, he stated that "The film just feels hastily assembled, its only real purpose to provide another platform for the onscreen pairing of Nadine Lustre and James Reid   but the pair works best outside the machinations of the plot, when the movie simply gives them the room to linger in an intimate moment." He also stated that "Talk Back and You’re Dead makes so little sense that it may as well be taking place in an entirely different universe. What starts out as a weird little tale of animosity growing into affection turns into a monster of melodramatic convolutions reliant on information that’s mostly hidden from the audience". 

Zig Marasigan of Rappler gave a harsh review of the film, stating "Trying to make sense of Talk Back and You’re Dead is very much like watching a snake eat its own tail. It’s fascinatingly grotesque, watching a serpent devour itself from the outside. It slowly works towards its delicate innards, oblivious to its own demise. The snake eventually dies from its own gastric juices, and all you’re left with is a reptilian corpse that is just too eager but too consumed to tell its tail from its meal. Talk Back and You’re Dead doesn’t have a horrible story, it simply doesn’t have a story. But if a film like Talk Back and You’re Dead can be produced and released without a need for story, who needs writers?". 

===Box office===
According to the showbiz news reporter Ginger Conejero of TV Patrol, the film earns PHP 30 Million on its first 3 days of showing.   A more recent update from Box Office Mojo places the total gross at PHP 69 Million as of August 31, 2014, second to the top-grosser Rurouni Kenshin.   There has been no official release of the box office gross from either Skylight or Viva films.

==Sequel==
During the end credits, a clip from the sequel was shown and it is confirmed that there will be a sequel of this film, entitled Never Talk Back To A Gangster. 

== See also ==
*List of Filipino films and TV series based on Wattpad stories

== References ==
 

== External links ==
*  Star Cinemas Information

 
 
 
 
 
 
 
 
 
 
 
 
 