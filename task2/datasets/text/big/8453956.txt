The Third Miracle
{{Infobox Film
| name           = The Third Miracle
| image          = Third miracle.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = Agnieszka Holland
| producer       = Fred Fuchs
| writer         = Richard Vetere John Romano
| narrator       = 
| starring       = Ed Harris Anne Heche
| music          = Jan A.P. Kaczmarek
| cinematography = Jerzy Zielinski
| editing        = David Siegel
| studio         = American Zoetrope
| distributor    = Sony Pictures Classics
| released       = April 20, 2000
| runtime        = 119 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $591,142  
| preceded_by    = 
| followed_by    = 
}} 1999 drama film directed by Agnieszka Holland starring Ed Harris and Anne Heche. The film was shot in Hamilton, Ontario, Canada.   

==Plot summary==
In Bystrica, Slovakia in 1944, near the end of World War II, an Allied bombing raid causes a tiny girl to pray for deliverance.
 Vatican performed by a devout woman whose death caused a statue of the Virgin Mary to bleed upon and cure a girl with terminal lupus. Now the woman has been nominated for sainthood.
 debunking false claims of miracles.  Father Frank is suffering a crisis of faith when he is sent to investigate the miracles of a woman, the late Helen ORegan who has been nominated for sainthood, and winds up becoming the greatest advocate for her canonization. 

Father Frank uncovers a series of extraordinary events but the most extraordinary thing of all may be the "saints" very earthly daughter, Roxane (Anne Heche). Roxane is a non-believer who cannot forgive her otherwise selfless mother for abandoning her at the age of 16. 

==Cast==
* Ed Harris as Father Frank Shore
* Anne Heche as Roxanne
* Armin Mueller-Stahl as Werner
* Michael Rispoli as John Leone
* Charles Haid as Bishop Cahill
* Barbara Sukowa as Helen

==Production notes==
In a scene shot at the front doors of   by Henry Moore can be clearly seen in the background.

==Soundtrack==

The soundtrack to The Third Miracle was released on December 14, 1999.

{{Track listing
| collapsed       = no
| extra_column    = Artist
| total_length    = 49:57 

| title1          = Interrupted Dream: Opening Titles
| length1         = 1:33
| extra1          = Jan A.P. Kaczmarek

| title2          = Bystricia At War
| length2         = 2:51
| extra2          = Jan A.P. Kaczmarek

| title3          = First Prayer
| length3         = 2:18
| extra3          = Jan A.P. Kaczmarek

| title4          = Frank and Roxanne
| length4         = 3:33
| extra4          = Jan A.P. Kaczmarek

| title5          = Before Your Eyes
| length5         = 2:53
| extra5          = Jan A.P. Kaczmarek

| title6          = Prayer At The Lake
| length6         = 1:27
| extra6          = Jan A.P. Kaczmarek

| title7          = Falcone
| length7         = 1:49
| extra7          = Jan A.P. Kaczmarek

| title8          = Domine Jesu
| length8         = 1:11
| extra8          = Jan A.P. Kaczmarek

| title9          = Call From Bystricia
| length9         = 2:12
| extra9          = Jan A.P. Kaczmarek

| title10         = Helen OReagan
| length10        = 2:49
| extra10         = Jan A.P. Kaczmarek

| title11         = They Call You A Miracle Killer
| length11        = 0:43
| extra11         = Jan A.P. Kaczmarek

| title12         = Meeting In The Hospital
| length12        = 2:59
| extra12         = Jan A.P. Kaczmarek

| title13         = Ordinary Woman
| length13        = 2:18
| extra13         = Jan A.P. Kaczmarek

| title14         = Maria In The Hospital
| length14        = 2:34
| extra14         = Jan A.P. Kaczmarek

| title15         = I Wanted It To Be True
| length15        = 2:15
| extra15         = Jan A.P. Kaczmarek

| title16         = The Confession
| length16        = 1:57
| extra16         = Jan A.P. Kaczmarek

| title17         = Unfinished Love Story
| length17        = 2:55
| extra17         = Jan A.P. Kaczmarek

| title18         = Farewell
| length18        = 1:09
| extra18         = Jan A.P. Kaczmarek

| title19         = Memories From The Lake
| length19        = 2:22
| extra19         = Jan A.P. Kaczmarek

| title20         = The Bleeding Statue
| length20        = 4:49
| extra20         = Jan A.P. Kaczmarek

| title21         = The Third Miracle
| length21        = 3:20
| extra21         = Jan A.P. Kaczmarek

}}

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 


 