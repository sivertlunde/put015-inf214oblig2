Thérèse Raquin (1953 film)
{{Infobox film
| name           = Thérèse Raquin 
| image          = Thérèse Raquin (1953 film).jpg
| caption        = 
| director       = Marcel Carné
| producer       = Raymond Hakim Robert Hakim
| writer         = Marcel Carné Charles Spaak Émile Zola (novel)
| starring       = Simone Signoret Raf Vallone Jacques Duby
| music          = Maurice Thiriet
| cinematography = Roger Hubert
| editing        = Marthe Gottie Suzanne Rondeau Henri Rust
| distributor    = Paris Film Productions
| released       =  
| runtime        = 102 minutes
| country        = France
| language       = French
| budget         =
| gross          =
}}
Thérèse Raquin (also The Adultress) is a 1953 French Drama film directed by Marcel Carné and starring Simone Signoret and Raf Vallone. The story is loosely based on the novel by Émile Zola but updated to 1953. It was screened at the 14th Venice International Film Festival where it won the Silver Lion.

==Plot==
Thérèse is unhappily married to her cousin, a haberdasher in poor health. They share their home with his domineering mother who makes Thérèses life even more miserable. When she attempts to run away with a truck driver she has fallen in love with her husband tries to prevent her, forcing her lover to take extreme action.

==Cast==
*Simone Signoret as Thérèse Raquin (character)|Thérèse Raquin
*Raf Vallone as Laurent LeClaire
*Jacques Duby as Camille Raquin
*Maria Pia Casilio
*Marcel André
*Martial Rèbe
*Paul Frankeur

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 
 

 