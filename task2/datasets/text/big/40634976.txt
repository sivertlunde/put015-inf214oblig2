Daniel & Ana
{{Infobox film
| name           = Daniel & Ana
| image          = Daniel_&_Ana_Poster.jpg
| caption        = 
| producer       = Daniel Berman Ripstein
| director       = Michel Franco
| writer         = Michel Franco
| starring       = Dario Yazbek Bernal Marimar Vega José María Torre
| music          = 
| cinematography = Chuy Chávez
| editing        = Óscar Figueroa	 	
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = Mexico
| language       = Spanish
| budget         =
}} Mexican drama thriller film directed by Michel Franco. It had its world premiere on May 18, 2009 at the Cannes Film Festival and stars Dario Yazbek Bernal and Marimar Vega. The movie follows two siblings that are kidnapped and forced to have sex on camera, as well as the emotional trauma that follows afterwards.

==Synopsis==
Ana (Marimar Vega) and Daniel (Darío Yazbek Bernal) are as close as siblings can get. Ana is excited about her upcoming wedding while Daniel is trying to figure out his own way in life. This is all shattered when the two are abducted in Mexico City and forced into having incestuous sex while on camera. The two then have to try to piece their lives back together the best they can, all while having the specter of the past events looming over them.

==Cast==
*Dario Yazbek Bernal as Daniel Torres
*Marimar Vega as Ana Torres
*José María Torre as Rafa (as Josemaría Torre-Hütt)
*Franco Abisua as Agrupación Cariño
*Cecilia Franco Abruch as Amiga Boda
*José de Jesús Aguilar as Sacerdote
*Gary Alazraki as Amigo Borracho
*Mark Alazraki as Amigo Fiesta
*Elías Alfille as Borracho 1
*Irma Berlanga as Recepcionista
*José Luis Caballaro as Amigo Boda
*Jéssica Castelán as Mariana
*Gabriel de Cervantes as Secuestrador 3
*Cecilia Levy Franco as Amiga Boda
*Sara Levy Franco as Niña Boda

==Reception==
Critical reception for Daniel & Ana was mixed  and the film holds a rating of 50% on Rotten Tomatoes (based on 6 reviews) and 43 on Metacritic (based on 5 reviews).   The New York Times gave a mixed review, stating that while the films "stylistic restraint may help deflect accusations of exploitation", they also thought that the muted emotions in the film "impedes our connection with the victims".  The Village Voice also gave a mixed review, saying that the film was effective until the final act which they felt "tips the films delicate balance over into lurid grotesquerie, even as   staging remains as consciously muted as ever." 
VARIETY: A horrible kidnapping shatters the lives of a brother and sister in austere, controlled Mexican drama. Attractive, outgoing college girl Ana Torres (Marimar Vega), the daughter of a wealthy, closeknit Mexico City family, is planning to get married to stolid Rafael (Jose Maria Torre) in three months. While Ana is out shopping with her shy but good-natured 16-year-old brother Daniel (Dario Yazbek Bernal), the two of them are kidnapped by a gang. The sequence proves striking:

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 