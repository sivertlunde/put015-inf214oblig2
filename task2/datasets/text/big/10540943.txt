Spring Silkworms (film)
{{Infobox film
| image          =
| caption        =
| name           = Spring Silkworms
| writer         =  
| starring       = Xiao Ying Yan Yuexian Ai Xia
| director       = Cheng Bugao Wang Shizhen
| studio         = Mingxing Film Company
| runtime        = 96 min. (at 25 frames per second)
| released       = 1933 China
| language       = Silent with Chinese intertitles
| budget         =
}} 1933 silent novella of the same name by Chinese author Mao Dun.

The film stars Xiao Ying, Yan Yuexian, Gong Jianong, Gao Qianping and Ai Xia and was produced by the Mingxing Film Company.

Today the film is considered one of the earliest films of the leftist movement in 1930s Shanghai.

== Plot == silk farmers in Zhejiang province, who suffer hardship and deprivation when their crop of silkworm cocoons die off. The film criticizes not only the harsh market conditions that have forced the family into poverty, but the familys own superstitions and selfishness.

Old Tong Bao is the patriarch of a silkworm-rearing family in Zhejiang.  He refuses to buy foreign breeds of silkworms for his coming crop.  The market conditions are harsh and despite the efforts his family put in in rearing the silkworms, the ensuing cocoons are unable to fetch a price in the market.  The film also features a subplot where a married woman, Lotus, is ostracized by Tong Baos family for being a supposed jinx.

==External links==
* 
*  at the Chinese Movie Database UCSD Chinese Cinema Web-based Learning Center

 
 
 
 
 
 
 
 

 