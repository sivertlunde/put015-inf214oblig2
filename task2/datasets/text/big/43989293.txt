Gayathri (film)
{{Infobox film 
| name           = Gayathri
| image          =
| caption        =
| director       = P. N. Menon (director)|P. N. Menon
| producer       =
| writer         = Malayattoor Ramakrishnan
| screenplay     = Malayattoor Ramakrishnan Shubha
| music          = G. Devarajan
| cinematography = Ashok Kumar
| editing        = Ravi
| studio         = Sreeram Pictures
| distributor    = Sreeram Pictures
| released       =  
| country        = India Malayalam
}}
 1973 Cinema Indian Malayalam Malayalam film, Shubha in lead roles. The film had musical score by G. Devarajan.   

==Cast==
*Jayabharathi
*Adoor Bhasi
*Sankaradi Shubha
*Raghavan Raghavan
*Bahadoor
*Kottarakkara Sreedharan Nair
*MG Soman
*Roja Ramani

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Vayalar Ramavarma. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Padmatheerthame Unaroo || K. J. Yesudas, Chorus || Vayalar Ramavarma || 
|-
| 2 || Sreevallabha Sreevalsaankitha || P. Madhuri || Vayalar Ramavarma || 
|-
| 3 || Thankathalikayil Pongalumaay Vanna || K. J. Yesudas || Vayalar Ramavarma || 
|-
| 4 || Thirakal Thirakal || K. J. Yesudas, Chorus || Vayalar Ramavarma || 
|-
| 5 || Thrithaapoovukal || P. Madhuri || Vayalar Ramavarma || 
|}

==References==
 

==External links==
*  

 
 
 

 