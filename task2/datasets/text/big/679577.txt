Canadian Bacon
 Canadian bacon}}
{{Infobox film
| name           = Canadian Bacon
| image          = Canadian Bacon (movie poster).jpg
| caption        = Theatrical release poster
| director       = Michael Moore 
| producer       = Michael Moore
| writer         = Michael Moore Kevin J. OConnor Bill Nunn Kevin Pollak G.D. Spradlin Rip Torn
| music          = Elmer Bernstein Peter Bernstein
| cinematography = Haskell Wexler
| editing        = Michael Berenbaum Wendey Stanzler Maverick Picture Company
| distributor    = Gramercy Pictures
| released       = September 22, 1995
| runtime        = 91 minutes
| country        = United States Canada
| language       = English
| budget         = $11 million
| gross          = $178,104 
}}

Canadian Bacon is a 1995 comedy film which satirizes Canada–United States relations along the Canada–United States border written, directed, and produced by Michael Moore. It was screened in the Un Certain Regard section at the 1995 Cannes Film Festival,    and was the final released film to star John Candy, though it was shot before the earlier-released Wagons East!.

==Plot== war on international terrorism is deemed too absurd. 
 CIA agent Kevin J. OConnor) and Kabral Jabar (Bill Nunn). After they apprehend a group of Americans "dressed as Canadians" attempting to destroy a hydroelectric plant, they sneak across the border to litter on Canadian lands, which leads to Honey being arrested by the Royal Canadian Mounted Police. In a rescue attempt, Boomer, Roy Boy and Kabral sneak into a Canadian power plant and cause a countrywide blackout. When the President learns of this, he orders Boomers immediate removal from Canada before its too late.

Hacker, seeking revenge on the President for shutting down his business, uses a software program ("Hacker Hellstorm") to activate missile silos across the country. The President learns that the signal causing the activation of the silos originated from Canada, and summons Hacker. Hacker offers to sell a program to the President that can cancel out the Hellstorm — for $1 trillion. Stuart realizes that Hacker is the one controlling the silos, and takes the operating codes from him required to stop the Hellstorm (accidentally killing Hacker in the process). The President orders Stuarts arrest, despite his protests that he is now able to deactivate the missiles. As the launch time approaches, which are aimed at Moscow, the President pleads with Canadian Prime Minister Clark MacDonald (Wallace Shawn) over the phone to stop the launch. 

Meanwhile, Honey was taken to a mental hospital upon her capture and escaped all the way to the CN Tower. She discovers the central computer for the Hellstorm located at the top and destroys it with a machine gun, aborting the launch sequence. She then reunites with Boomer, who had tracked her to the Tower, and they return to the United States via a speedboat.

An ending montage reveals the characters fates: Boomer realizes his dream of appearing on Cops (1989 TV series)|Cops; Honey has been named "Humanitarian of the Year" by the National Rifle Association; Kabral has become a hockey star; and MacDonald is "still ruling with an iron fist."

==Cast== Niagara County
* Alan Alda as President of the United States
* Rhea Perlman as Honey, Deputy sheriff of the Niagara County Sheriff Department and girlfriend and colleague of Sheriff Bud Boomer
* Bill Nunn as Kabral Jabar, Deputy sheriff of the Niagara County Sheriff Department and friend and colleague of Sheriff Bud Boomer Kevin J. OConnor as Roy Boy, friend of Sheriff Bud Boomer
* Kevin Pollak as Stu Smiley, National Security Advisor
* G. D. Spradlin as R.J. Hacker, Owner of Hacker Dynamics
* Rip Torn as General Dick Panzer, U.S. Army Chief of Staff
* Steven Wright as Niagara Mountie Jim Belushi as Charles Jackal, news reporter for NBS News
* Richard E. Council as Russian President Vladimir Kruschkin
* Brad Sullivan as Gus
* Stanley Anderson as Edwin S. Simon, news anchor for NBS News
* Wallace Shawn as Canadian Prime Minister Clark MacDonald
* Michael Moore as Redneck guy
* Dan Aykroyd (uncredited) as Ontario Provincial Police officer Mountie 

==Production== Buffalo and Twelve Mile Creek in St. Catharines, Ontario|St. Catharines.  Parkwood Estate in Oshawa was the site for the White House, and Dofasco in Hamilton was the site for Hacker Dynamics.  The scene where the American characters look longingly home at the US across the putative Niagara River is them looking across Burlington Bay at Stelco steelworks in Hamilton, Ontario.  

The hockey game/riot were shot at the Niagara Falls Memorial Arena in Niagara Falls, Ontario,    and the actors portraying the police officers (who eventually join in the riot upon hearing "Canadian beer sucks") are wearing authentic Niagara Regional Police uniforms.   
 cameos by Canadian actors, including Dan Aykroyd, who appears uncredited as an Ontario Provincial Police officer who pulls Candy over (ticketing him not for the crude anti-Canadian graffiti on her truck, but its lack of a French translation). Moore himself appears as an American gun nut.

==Reception==
Canadian Bacon received poor reviews from film critics, receiving a 14% from Rotten Tomatoes. 

Nathan Rabin in a 2009 review concluded, "After generating solid laughs during its first hour, Canadian Bacon falls apart in its third act," lamenting the film "was perceived as too lowbrow for the highbrows, and too highbrow for the lowbrows." 

==See also==
*Canada-United States relations
*   comedy about a fictional element of the cold war
* The Mouse that Roared fictional documentary about how Canadian entertainers are conquering TV and movies in the United States.
* Wag the Dog, a 1997 film about a war devised for similar reasons
* War Plan Red,  also known as the Atlantic Strategic War Plan, was a plan for the United States to make war with Great Britain, by attacking Canada. preemptive strike" against Canada. A Speculative Fiction", a song by Canadian band Propagandhi that explores a war between Canada and the U.S.
*  , the 1999 South Park film about a similar war (though in this case the wars reason is a moral panic rather than explicitly to boost a presidents sagging poll numbers).
* The real life War of 1812 between the United States and British North America (now Canada).

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 