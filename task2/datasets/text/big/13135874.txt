The Moonshine War
The 1970 film directed by Richard Quine, based on the novel of the same name by Elmore Leonard.  It starred Patrick McGoohan, Richard Widmark, Alan Alda, Will Geer, John Schuck, and Teri Garr.

==Plot==
 Internal Revenue agent.

Long is willing to look the other way in exchange for a percentage of Sons business. But when he is unable to persuade Son to cut him in on the profits or even reveal where the moonshine is hidden, Long sends for the dangerous Dr. Taulbee, who uses more violent methods in getting what he wants.

Taulbee and his henchmen go too far, killing Sheriff Baylor and even Taulbees girlfriend when she tries to get away. Long can see that he made a mistake, so he volunteers to help Son fend off the gang. Still outnumbered, Son finally tells Taulbees men where the whiskey is buried in exchange for his life. But when the crooks start digging, they set off Sons buried dynamite instead.

==Cast==

* Alan Alda as Son Martin
* Patrick McGoohan as Frank Long
* Richard Widmark as Dr. Taulbee
* Will Geer as Sheriff Baylor

==Reception==

The Moonshine War was a critical and commercial failure upon release in July, 1970.  Reviewing the 2014 Warner Archive Collection DVD release of the movie, Paul Mavis of DVDTalk.com stated, "Scripted by Elmore Leonard from his novel, The Moonshine War suffers from one malady after another: cosmically misconceived casting, blank, lackluster direction, an indifferent, cheap production, and what I suspect was post-production tampering. Some trademark violent Leonard goofiness cant help but shine through; however, the final result is embarrassingly bad.... A flat-out disaster--and an especially disappointing one--considering the promising set-up." 

==References==
 

==External links==
* 

 ·
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 


 
 