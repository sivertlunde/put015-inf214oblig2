Strings (2014 film)
{{Infobox film
| name           = Strings
| image          = 
| alt            = 
| caption        = 
| director       = Daniel Duran
| producer       = Daniel Duran   Danny Rodriguez   Oscar Orlando Torres   Phyllis Laing
| writer         = Oscar Orlando Torres
| starring       = Josh Duhamel   Maria Bello   Lucas Till   Laura Dern   Tom Everett Scott
| music          = 
| cinematography = Angel Barroeta
| editing        = 
| studio         = Strings of Films
| distributor    = 
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Strings is a 2014 American drama film directed by Daniel Duran and written by Oscar Orlando Torres. The film stars Josh Duhamel, Maria Bello, Sunny Winter, Lucas Till, Laura Dern and Tom Everett Scott.

== Cast ==
* Josh Duhamel
* Maria Bello
* Lucas Till
* Laura Dern
* Tom Everett Scott
* Katrina Norman
* Jae Head
* Kherington Payne
* Shamier Anderson
* Sharlene Taulé
* Daryl Dorge
* Adam Hurtig
* Nigel Rhoden
* Brittany Scobie
* Michelle Dawley
* Lorraine James
* Cole Humphrey
* Jessie Posthumus 
* Clay Bakkum
* Alex Schween
* Udit Suryavanshi
* Mitch Reuther
* Kenechukwu Igweagu
* Monica Igweagu

== Production ==
The principal photography of the film began on June 9, 2013 in Winnipeg, Canada.  

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 


 