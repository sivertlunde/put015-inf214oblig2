The Still Alarm
{{Infobox film
| name           = The Still Alarm
| image          = The Still Alarm 1918 film poster.jpg
| caption        = Film poster for 1918 silent film version of The Still Alarm
| director       = 
| producer       = 
| writer         = 
| based on       = 
| starring       = 
| music          = 
| choreography   = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 1887 (play), 1911 (film), 1918 (film), 1926 (film)
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Joseph Arthur that debuted in New York in 1887 and enjoyed great success, and was adapted to silent films in 1911, 1918, and 1926.  Though never a favorite of critics, it achieved widespread popularity.  It is best known for its climactic scene where fire wagons are pulled by horses to a blazing fire. Rahill, Frank.  , p. 257 (1967) ("the greatest of all the fireman plays") 

==Background==

The play debuted at the    Burt, Daniel S. (ed.)  , p. 271 (2004)  (28 August 1887).  , The New York Times   Harry Lacy played the lead role of Jack Manley.  , The Theatre (August 1887) 

Though it ran only a few weeks in its initial engagement, the play returned in March 1888 and ran for over 100 more performances.  Bordman, Gerald Martin.  , p. 259 (1994)  (27 March 1888).  ,     Its popularity was then well-secured.  In September 1889, it re-appeared at the     It ran again at the Fourteenth for two weeks in 1891, (30 August 1891).  ,   

The play was also successful in England, and ran for 100 nights at the   

Critics noted its success with guarded bemusement.  An August 1888 note on its London success reported that "the critics have come down rather severely on "The Still Alarm", but as this was not unexpected, the management does not worry.  Meanwhile,   
 Blue Jeans (1890) and The Cherry Pickers (1896).

==Film adaptations==
The Still Alarm has been adapted to silent film three times, in 1911, 1918, and 1926.
 William Selig produced the film, which has been preserved by the Museum of Modern Art Department of Film.  , Film-foundation.org (listing for 1911 version of Still Alarm, preserved), Retrieved October 11, 2011 

Selig produced a wholly new film version in 1918.  This version starred    (14 December 1918).  ,    (25 August 1918).  , Film Daily|Wids Daily, p. 13-14 

The final silent film version of the film, from     Film archivist William K. Everson reviewed the film positively in 1956, noting that though it has "no reputation and is little known", it is "one of the very best" of the "fire-fighting thrillers" popular in the 1920s. Everson, William K.  , William K. Everson Archive at nyu.edu, Retrieved October 11, 2011 

==Unrelated "Still Alarm" titles==

A 1903 Edison short called The Still Alarm consists of footage of moving New York fire equipment and is not a film adaptation of the play. Niver, Kemp R.  , p. 316 (1967) 

A 1930 Vitaphone short of the same title is a comedy skit by George S. Kaufman, where Fred Allen and Harold Moffet debate what to wear before exiting a burning hotel, later joined by similarly blase firemen.  The sketch comes from The Little Show, a revue that opened on Broadway in 1929. Bordman, Gerald Martin.  , p.501  (3d ed. 2001) 

==Legacy==

Despite its roaring success as a play in New York, London and elsewhere, including repeated revivals and local productions mounted for many years, (18 May 1911).  , Flushing Daily Times (reporting on 1911 revival)    the sensationalistic fame of The Still Alarm eventually had to ebb.  In 1920, a feature in the  , p.3   But Eversons observation in 1956 that it has "no reputation and is little known" (though he was only referring to the 1926 film version) fairly characterizes its lack of long-term staying power. 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 