American Mayor
{{multiple issues|
 
 
}}
 

{{Infobox film
| name           = American Mayor
| image          =
| caption        =
| director       = Travis Irvine
| producer       = Daniel Brown Travis Irvine Brian Kamerer Mark Lammers Matthew Lyons Nick Lyons Colin Scianamblo Jeremy Weber
| writer         = Travis Irvine
| starring       =
| music          = Travis Irvine Brian Kamerer Seth Pfannenschmidt Allen Price
| cinematography = Brian Kamerer Matthew Lyons
| editing        = Daniel Brown Travis Irvine
| studio         = Overbites Pictures
| distributor    =
| released       =  
| runtime        =
| country        = United States
| language       = English
| budget         =
| gross          =
}}
American Mayor is a 2009 documentary film detailing Travis Irvines 2007 campaign for mayor of Bexley, Ohio.

==Background==
The films creator, Travis Irvine, was raised and schooled in Bexley, and is a graduate of Ohio University.   
When he announced his candidacy for the office of Mayor,   
he also announced his intention to create a documentary about the process and his involvement in it to encourage others to be more active in the democratic process.   
When interviewed about an August 2008 screening of his earlier film Coons! Night of the Bandits of the Night,   
he spoke of plans to show a 10-minute clip of American Mayor during the screening to raise funds to finish the documentary.   
Then in September he announced that the film was in post production and would be distributed by Troma Entertainment.   

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 


 