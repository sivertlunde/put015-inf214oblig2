Kasethan Kadavulada
 
: For 2011 Tamil film, see Kasethan Kadavulada (2011 film)

{{Infobox film
| name           = Kasethan Kadavulada
| image          = Kasethan Kadavulada dvd.jpg
| image_size     = 
| caption        = Official DVD Box Cover
| director       = Chitralaya Gopu
| producer       = M. Murugan M. Kumaran M. Saravanan (film director) |M. Saravanan M. Balasubramaniam
| writer         = Chitralaya Gopu
| narrator       =  Lakshmi Thengai Srikanth Manorama Manorama Venniradai Moorthy
| music          = M. S. Viswanathan
| cinematography = K. S. Bhaskar
| editing        = R. G. Gopu
| studio         = AVM Productions
| distributor    = AVM Productions
| released       = 1972
| runtime        = 120 mins
| country        = India
| language       = Tamil
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 Srikanth and Manorama and Venniradai Moorthy in supporting roles. The film was a low-budgeted film of the time and is perhaps best known for Thengai Srinivasan dressing up as a fake saint, making a lot of situational comedies along with it.

==Plot==

Parvathi (Manorama) is an extremely domineering second wife to Sivaswamy (Vennira Aadai Moorthy) and controls all his money treating him with scorn and disdain. When Sivaswamys son Ramu (Muthuraman), son of his first wife, requires a sum of Rs. 3000 for his Sisters husband, she refuses instantly. With plenty of worry, Ramu and his cousin Shekar hatch a plot with Appaswamy (Thengai Sreenivasan) who pretends to be a fake swamiji in order to get to her money safe and steal a sum of Rs. 50,000.

Meanwhile Rama (Lakshmi) is an orphan whose only friend is the doctor of a mental institution. When Rama wants a conduct certificate, the doctor bungles and hands over the certificate of a mad girl. Rama arrives at Parvathis house to work as a secretary and Parvathi sees the certificate and decides to keep Rama at her house to humour her fearing her insanity.

The film leads to plenty of humorous situations.

==Cast==
* R. Muthuraman Lakshmi
* Thengai Srinivasan
* Venniradai Moorthy Manorama
* Sachu
* Suruli Rajan
* Senthamarai Srikanth
* Rama Prabha

==Soundtrack==
The music composed by M. S. Viswanathan. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Vaali (poet) Vaali || 03:29
|-
| 2 || Aval Enna Ninaithal || P. Susheela || 03:24
|-
| 3 || Indru Vantha Intha Mayakkam || P. Susheela || 03:35
|-
| 4 || Jambulingame Jadaadaraa || K. Veeramani, Kovai S. Rajan, Dharapuram Sundarrajan || 03:33
|-
| 5 || Mella Pesungal || Kovai S. Rajan, L. R. Eswari || 04:18
|}

==References==
 

==External links==
*  

 
 
 
 
 


 