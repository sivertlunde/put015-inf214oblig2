Devara Duddu
{{Infobox film 
| name           = Devara Duddu
| image          =  
| caption        = 
| director       = K. S. L. Swamy
| producer       = N Rani
| writer         = Remake of Kaliyuga Kannan (Tamil) (1974)
| screenplay     = K. A. Natarajan Rajesh Srinath Jayanthi Chandrashekar
| music          = Rajan-Nagendra
| cinematography = N G Rao
| editing        = Bal G Yadav Victor
| studio         = Rani Arts
| distributor    = Rani Arts
| released       =  
| runtime        = Array
| country        = India Kannada
}}
 1977 Cinema Indian Kannada Kannada film, Jayanthi and Chandrashekar in lead roles. The film had musical score by Rajan-Nagendra.  

==Cast==
  Rajesh
*Srinath Jayanthi
*Chandrashekar
*Hema Choudhary Balakrishna
*Dwarakish Manorama
*Lokanath
*Ashwath Narayan
 

==Soundtrack==
The music was composed by Rajan-Nagendra. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Susheela || R. N. Jayagopal || 03.54
|-
| 2 || Krishna..Gaaliya Patadante || PB. Srinivas || Hunsur Krishna Murthy || 05.19
|}

==References==
 

==External links==
*  
*  

 

 
 
 
 


 