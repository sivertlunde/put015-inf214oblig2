Android Re-Enactment
 
 
{{Infobox film
| starring       = {{Plainlist|
* Jeff Sinasac
* Adam Buller
* Sarah Silverthorne
* Todd Thomas Dark
* Bill Poulin
* Melissa Cline
* Dean Tedesco
}}
| director       = Darryl Shaw
| music          = Dave Coleman
| cinematography = Kevin Davidson
| editing        = Darryl Shaw
| studio         = God in the Grass Productions
| released       =  
| runtime        = 97 minutes
| country        = Canada
| language       = English
}}
Android Re-Enactment is a 2011 Canadian science fiction film directed by Darryl Shaw and starring Jeff Sinasac, Adam Buller, and Sarah Silverthorne. 

The film depicts a stylized, retro future in an unspecified year, in which genetically engineered organic androids, visually indistinguishable from humans, are manufactured by the Empathtek Corporation.  The latest and most advanced model they have designed, soon to be released, is the PX-50.  The plot focuses on a young Socionics Engineer named Ermus Daglek who has retired from Empathtek, but who has turned down the millions he is owed in residuals in exchange for the company donating him a defunct factory and asking no questions.  He uses the factory to manufacture androids based on key individuals from the time in his life when he experienced his greatest heartbreak, and runs simulations to see if any other courses of action on his part could have produced an outcome with romantic success. 

==Plot== androids based on his lost love, Candy Droober (Sarah Silverthorne), her father, Franklin Droober (Bill Poulin), her mother, Maureen Droober (Melissa Cline), and his romantic rival, Trace Mayter (Adam Buller).  Deciding that the key moment where he lost any chance of becoming Candys lover was a dinner party where the real life Trace Mayter humiliated Ermus in front of Candy and her family, he recreates Candys dining room in a test lab, and goes about simulating the dinner several times, trying different conversational tactics and outfits. 

Frustrated that no attempt yields any result but Candy and Trace becoming lovers, he reprograms the Franklin android to murder the Trace android over dinner.  Having vented his anger, Ermus repairs Trace and goes back to new simulations, unaware that the damage inflicted by Franklin has set off malfunctions in Trace, gradually allowing him to remember the prior simulations and realize what and who he is. 

Trace eventually goes berserk over dinner, beheading Franklin and damaging Maureen, before Candy and Ermus escape to the main laboratory.  Ermus deactivates Candy and, armed with an Ion Disruptor (a ray gun designed to disable androids), goes in search of Trace elsewhere in the lab.  After battling the headless Franklin android, he confronts Trace and forces him to deactivate himself.

Ermus attempts to repair the androids and reprograms Candy to be a Hypersexuality|nymphomaniac.  His attempted seduction of her fails, though, as, even though Ermus has wiped her memory banks, she retains preview files of Trace and is still in love with him.

Enraged, Ermus chains up Trace before reactivating him.  Trace tells Ermus that hes infiltrated the mainframe computer and broadcast his personality via wireless signal to all the androids.  Ermus sets about to dismember him to sell for scrap, but Trace seals the laboratory, cuts off the oxygen supply and begins filling it with carbon monoxide.  He agrees to let Ermus live if Ermus will unchain him and re-initiate his motor functions, but when Ermus does so, Trace destroys the mainframe before escaping the complex.

Ermus is forced to call in Kray Facer (Todd Thomas Dark), an Empathtek android hunter, to retrieve Trace.  The two go in search of Trace, but cant find him.  Kray gives Ermus a revolver with explosive tipped bullets, instructing him to shoot Trace in the face if he sees him again.

Ermus returns to the lab which, after the mainframe was destroyed, lost all of its security systems.  He finds the doors unlocked and sees signs of an intruder within.  When he goes to the deactivated Candy android, he finds her inactive form being raped by an apparent transient (Dean Tedesco).  A fight ensues and Ermus winds up shooting the transient with one of the explosive shells.  He finds the transient to be wearing a gold cross which was always worn by Trace.  Not knowing that Trace had discarded the necklace after fleeing the lab, he comes to the idea that the transient must be Trace wearing a new facial mold.

Ermus drags the transient back to the lab and attempts to remove his main processor chip by drilling into the transients ear.  He realizes his mistake when he finds only blood and brain matter within and, panicking, calls Kray again.  Kray shows up but refuses to have anything to do with what is a blatant murder by Ermus.  After Kray leaves, Ermus finds an empty photograph frame that used to contain a picture of the real Candy Droober, and realizes Trace has likely gone in search of his real-life counterparts former wife and daughter.

Ermus rides his scooter to New Jersey, where Candy lives with her daughter, Tristan.  The real life Trace died in a car accident some time ago.  Barging into their home, he doesnt find Trace, but Tristan lets slip that the android version was there previously and Ermus decides to wait for his return.  When Trace does come back, Ermus shoots him in the face, as instructed.  Trace says goodbye to his family before dying messily when his head explodes.

Ermus returns to the laboratory, and wires up the corpse of the transient to take Traces place at dinner.  He recommences the simulations with his heavily damaged family.

== Cast ==
* Jeff Sinasac - Ermus Daglek
* Adam Buller - Trace Mayter
* Sarah Silverthorne - Candy Droober
* Bill Poulin - Franklin Droober
* Melissa Cline - Maureen Droober
* Todd Thomas Dark - Kray Facer
* Dean Tedesco - Transient
* Delina Porter - Tristan
* Dana Tartau - voice of the Mainframe

== Production ==
Principal photography started in early July 2008, and extended until late October 2008, mostly in Niagara Falls, Ontario, Canada, with some limited filming also occurring in Toronto, Ontario, Canada.   Post Production continued through late 2010. 

== Awards ==
* Best Science Fiction Film, Moving Images Film Festival, 2011. 

==References==
 

== External links ==
*  IMDB listing.
* 
* 
*  on Twitch
*  on Dread Central
*  on Quiet Earth
*  on Bloody Disgusting
*  on I09
*  on Horror Society
*  Android Re-Enactment @ Niagara Macabrecon
*  about shooting Android Re-Enactment in Niagara Falls

 
 
 
 