Seven Beauties
{{Infobox film
| name           = Seven Beauties
| image          = Pasqualino Settebellezze 1975 film poster.jpg
| border         = yes
| alt            = 
| caption        = Italian theatrical release poster
| director       = Lina Wertmüller
| producer       = {{Plainlist|
* Arrigo Colombo
* Lina Wertmüller
}}
| writer         = Lina Wertmüller
| starring       = {{Plainlist|
* Giancarlo Giannini
* Fernando Rey
}}
| music          = Enzo Jannacci
| cinematography = Tonino Delli Colli
| editing        = Franco Fraticelli
| studio         = Medusa Distribuzione
| distributor    = {{Plainlist|
* Medusa Distribuzione  
* Cinema 5 Distributing  
}}
| released       =  
| runtime        = 115 minutes
| country        = Italy
| language       = Italian
| budget         = 
| gross          = 
}}
Seven Beauties ( ) is a 1975 Italian language film written and directed by Lina Wertmüller and starring Giancarlo Giannini, Fernando Rey, and Shirley Stoler. Written by Wertmüller, the film is about an Italian everyman who deserts the army during World War II and is captured by the Germans and sent to a prison camp, where he does anything to survive. Through flashbacks, we learn about his family of seven unattractive sisters, his accidental murder of one sisters lover, his imprisonment in an insane asylum, his rape of a patient, and his volunteering to be a soldier to escape confinement.    The production design and costume design were by the directors late husband, Enrico Job. The film received four Academy Award nominations, including for Best Foreign Language Film,    and one Golden Globe Award nomination for Best Foreign Film.   

==Plot==
The picaresque  story follows its protagonist, Pasqualino (Giannini) who, as a dandy and small-time hood in Naples, to save the family honour, is sent to prison for  killing a pimp (and then dissecting the victim and placing the body in suitcases) who had turned Pasqualinos sister into a prostitute.  Convicted and sent to prison,  Pasqualino succeeds in being transferred to a psychiatric ward.  Desperate to get out, he volunteers for the Italian Army, but then somewhere in Germany he deserts with a comrade.  They are captured and sent to a concentration camp.  There, in a bid to save his own life, Pasqualino decides to survive by providing sexual favors to the obese and ugly female commandant (Stoler). His plan succeeds, except for the fact that he is then put in charge of the barracks as a Kapo (concentration camp)|kapo, and is obliged to select six men to be killed under the threat that if he doesnt do so, they will all be killed. Pasqualino ends up executing the soldier with whom he was captured and being responsible for the death of another fellow prisoner, a Spanish  anarchist.  At the wars end, upon his return to Naples, Pasqualino discovers that his seven sisters, his fiancée and even his mother have all survived through prostitution.

==Cast==
* Giancarlo Giannini as Pasqualino Frafuso aka Settebellezze
* Fernando Rey as Pedro the Anarchist Prisoner
* Shirley Stoler as The Prison Camp Commandant 
* Elena Fiore as Concettina (a sister)
* Piero Di Iorio as Francesco (Pasqualinos comrade)
* Enzo Vitale as Don Raffaele
* Roberto Herlitzka as Socialist
* Lucio Amelio as Lawyer
* Ermelinda De Felice as Pasqualinos Mother
* Bianca DOriglia as The Psychiatrist
* Francesca Marciano as Carolina
* Mario Conti as Totonno "18 Carati" (Concettinas pimp)
* Barbara Valmorin as The Prison Camp Commandants Secretary
* Emilio Salvatori
* Aristide Caporale as Madman
* Pasquale Vitiello   

==Production==
===Casting=== Swept Away (1974).

===Filming locations===
* Seven Beauties was filmed on location in Naples, Campania, Italy.   

===Opening sequence===
In the opening sequence of Seven Beauties, spoken over World War II archival footage showing the destruction of cities and men, Wertmüller defines the object of her critique—a "particular petty bourgeois social type".   
:The ones who dont enjoy themselves even when they laugh. Oh yeah.
:The ones who worship the corporate image not knowing that they work for someone else. Oh yeah.
:The ones who should have been shot in the cradle. Pow! Oh yeah.
:The ones who say, "Follow me to success, but kill me if I fail," so to speak. Oh yeah.
:The ones who say, "We Italians are the greatest he-men on earth." Oh yeah.
:The ones who are from Rome.
:The ones who say, "Thats for me."
:The ones who say, "You know what I mean?" Oh yeah.
:The ones who vote for the right because theyre fed up with strikes. Oh yeah.
:The ones who vote blank ballot in order not to get dirty. Oh yeah.
:The ones who never get involved with politics. Oh yeah.
:The ones who say, "Be calm ... calm."
:The ones who still support the king.
:The ones who say, "Yes, sir." Oh yeah.
:The ones who make love standing in their boots and imagine theyre in a luxurious bed.
:The ones who believe Christ is Santa Claus as a young man. Oh yeah.
:The ones who say, "Oh what the hell."
:The ones who were there.
:The ones who believe in everything ... even in God. 
:The ones who listen to the national anthem. Oh yeah.
:The ones who love their country.
:The ones who keep going, just to see how it will end. Oh yeah.
:The ones who are in garbage up to here. Oh yeah.
:The ones who sleep soundly, even with cancer. Oh, yeah.
:The ones who even now dont believe the world is round. Oh yeah. Oh yeah.
:The ones who are afraid of flying. Oh yeah.
:The ones whove never had a fatal accident. Oh yeah. The ones whove had one.
:The ones who at a certain point in their lives create a secret weapon: Christ. Oh yeah.
:The ones who are always standing at the bar.
:The ones who are always in Switzerland.
:The ones who started early, havent arrived, and dont know theyre not going to. Oh yeah.
:The ones who lose wars by the skin of their teeth.
:The ones who say, "Everything is wrong here."
:The ones who say, "Now lets all have a good laugh." Oh yeah. Oh yeah. Oh yeah. Oh yeah.

==Critical response==
The subject of the film is survival. It was controversial at the time for its graphic depiction of Nazi concentration camps. In his 1976 essay "Surviving", Bruno Bettelheim, while admiring the films artistry, severely criticizes the impression it makes of the experience of concentration camp survivors. 

On the aggregate reviewer web site Rotten Tomatoes, the film received a 91% positive rating from top film critics based on 11 reviews, and an 88% positive audience rating based on 1,940 reviews.   

==Awards and nominations==
{| class="wikitable"
|- Year
!style="width:250px;"|Award Category
!style="width:250px;"|Nominee Result
|- 1977
| Academy Awards
| Best Actor in a Leading Role
| Giancarlo Giannini
|  
|-
| Best Director
| Lina Wertmüller
|   
|-
| Best Writing, Screenplay
| Lina Wertmüller
|  
|-
| Best Foreign Language Film
| 
|  
|-
| Directors Guild of America Award
| Outstanding Directorial Achievement
| Lina Wertmüller 
|  
|-
| Golden Globe Award
| Best Foreign Film
| 
|  
|- New York Film Critics Circle Award
| Best Film
| 
|  
|-
| Best Director
| Lina Wertmüller
|  
|-
| Best Actor
| Giancarlo Giannini
|  
|-
| Best Screenplay
| Lina Wertmüller
|  
|}

==See also==
* List of submissions to the 49th Academy Awards for Best Foreign Language Film
* List of Italian submissions for the Academy Award for Best Foreign Language Film
* The Seven Beauties - medieval Iranian romance poem about foreign escapades of the pre-islamic Persian ruler, Bayram Gur, framed in pleasantries in seven pavillions modelled on seven climes controlled by the seven planets followed by various sufferings, grievances and injustices encountered in seven of his subjects

==References==
;Notes
 

;Citations
 

;Bibliography
 
*  
*  
*  
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 