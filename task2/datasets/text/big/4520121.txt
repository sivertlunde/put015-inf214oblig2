Nothing but Trouble (1991 film)
 
{{Infobox film
| name           = Nothing But Trouble
| image          = Nothing but trouble poster.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Dan Aykroyd
| producer       = Lester Berman Robert K. Weiss
| screenplay     = Dan Aykroyd
| story          = Peter Aykroyd
| starring       = Chevy Chase Demi Moore John Candy and Dan Aykroyd
| music          = Michael Kamen
| cinematography = Dean Cundey
| editing        = Malcolm Campbell James R. Symons
| distributor    = Warner Bros.
| released       =  
| runtime        = 94 minutes
| country        = United States
| language       = English
| budget         = $40 million Box office and business for   at the Internet Movie Database 
| gross          = $8,479,793 (US)
}}
 adventure comedy comedy horror horror film, directed by and co-starring Dan Aykroyd, who also co-wrote the screenplay with his brother Peter Aykroyd|Peter. The cast featured Chevy Chase, Demi Moore, John Candy, with Taylor Negron, Raymond J. Barry, and Brian Doyle-Murray, in supporting roles.

== Plot ==
While hosting a party in his Manhattan penthouse, financial publisher Chris Thorne (Chase) meets lawyer Diane Lightson (Moore) and agrees to escort her to consult a client in Atlantic City on the following day. Thornes clients, obnoxious but wealthy Brazilian siblings Fausto and Renalda, whom he calls "Brazillionaires", meet up with them and invite themselves along.

Along the way, Chris takes a supposed scenic detour off of the New Jersey Turnpike, ultimately ending up in the run-down village of Valkenvania. Failing to comply with a stop sign and subsequently attempting to escape pursuing officer Dennis Valkenheiser (Candy), the group is captured and taken before Denniss 106-year-old father Judge Alvin Valkenheiser (Aykroyd), who confiscates their identification cards. After Chris makes too many smart-alecky remarks, the reeve drops a trap door out from under them in order to hold the offenders in his courthouse/funhouse to be judged. Later, some disrespectful drunk drivers that had even tried to threaten Dennis are called before the judge, who sentences them to immediate death at the hands of a deadly roller coaster nicknamed "Mr. Bonestripper".

Invited up to dinner, the group is repulsed by the bizarre food choices (involving a hot dog train and a warm can of Hawaiian punch) but also learns the Judge has labeled Chris as a "banker" for his financial affiliations, and is holding them there out of revenge for the Valkenheiser familys misfortune at the hands of a corrupt coal deal long before. The group attempts an escape, but due to a series of mishaps, Chris and Diane are overtaken by Alvins mute granddaughter Eldona (also John Candy). Meanwhile, being chased by Dennis trigger-happy cousin, Miss Purdah, the two Brazillionaires escape by cutting a deal with Dennis, who decides to escape with them.

The Judge is angered by their actions and imprisons Chris and Diane in a room from which the pair eventually escapes (again with help from Dennis) and getting lost through hidden hallways and slides, become separated. Diane makes it out of the house and into the propertys salvage yard; here, she meets two troll-like creatures by the names of Bobo and Lil Debbull, the judges severely deformed grandchildren. Earning the creatures friendship, Diane catches glimpses of Eldona destroying Chriss BMW.

Meanwhile, Chris sneaks into the Judges personal quarters but is quickly caught. Valkenheiser punishes him according to house policy, which decrees that Chris must marry Eldona. Meanwhile, in the court room, the alternative rap group Digital Underground is being held on charges of speeding, but the Judge releases them after being charmed by an impromptu performance of one of the groups hits. He also asks them to stay as witnesses for the wedding, which Chris reluctantly goes through with in exchange for his life, but is later caught pleading the band to help him escape. The band leaves without understanding him, and Alvin sentences Chris to die in "Mr. Bonestripper". The machine breaks down the instant before Chris is fed into it, and he escapes. The Judge nearly kills Diane with another claw contraption, but Chris retrieves her at the last second and the two jump on a train back to New York.

After the two report their plight to the authorities, the Judges courthouse is raided by local and state police. Chris and Diane are asked to accompany the officers to the site, only to find out that the officers involved are fully aware of and in league with the Judge. The couple escapes when the areas underground coal fires cause a collapse, destroying the town.

The Brazillionaires are shown to have made their way back to South America; Dennis is now their personal head of security and Renaldas lover. Chris and Diane are shown relaxing in New York. Chriss relief does not last, however, as he stumbles on a televised news segment covering the ruined town of Valkenvania, in which Valkenheiser, brandishing Chriss drivers license, announces that he and his family plan to move in with his new grandson-in-law in New York, saying "See you soon Banker!" Chris runs away panicking while making a cartoon-style impression of himself through a wall.

== Cast ==
*Chevy Chase as Chris Thorne
*Demi Moore as Diane Lightson
*John Candy as Dennis Valkenheiser/Eldona
*Dan Aykroyd as Judge Alvin J.P Valkenheiser/Bobo
*Taylor Negron as Fausto
* Bertila Damas as Renalda
*Valri Bromfield as Miss Purdah
*Digital Underground as themselves

== Awards ==
The film won the  ), Worst Actress (Demi Moore, who lost to   

The film was also nominated for a   

== Reception == rating average of  .  According to one biography, Chase "knew that the film was going to be the worst film he would ever make," but because of his friendship with Aykroyd, he accepted the role of Chris Thorne.  Reflecting on the film some years later, it was noted as an "unfortunate turning point" in Aykroyds career that, as the director, writer and star, left "only (Aykroyd) to blame for the films spectacular failure." 

== Soundtrack ==
{{Infobox album  
| Name        = Nothing but Trouble|
| Type        = Soundtrack
| Cover       =  
| Released    = 1990  
| Recorded    =
| Genre       =  Soundtrack
| Length      = Warner Bros.
| Producer    = Tupac Shakur
}}
AllMusic rated the soundtrack two and a half stars out of five. 

#"The Good Life" - Ray Charles Same Song" - Digital Underground with Tupac Shakur & Dan Aykroyd
#"Get Over" - Nick Scotti Four Seasons
#"Tie The Knot" - Digital Underground Damn Yankees
#"Atlantic City (Is a Party Town)" - Elwood Blues Revue
#"La Chanka" - Bertila Damas
#"I Mean I Love You" - Hank Williams Jr.
#"Valkenvania Suite" - Michael Kamen

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 