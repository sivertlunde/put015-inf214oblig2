The Crooked Web
{{Infobox film
| name           = The Crooked Web
| image_size     = 
| image	=	The Crooked Web FilmPoster.jpeg
| caption        = 
| director       = Nathan Juran
| producer       = Sam Katzman
| writer         = Lou Breslow
| narrator       = 
| starring       = Frank Lovejoy Mari Blanchard Richard Denning
| music          = Mischa Bakaleinikoff
| cinematography = Henry Freulich 
| editing        = Edwin H. Bryant
| studio         = 
| distributor    = Columbia Pictures
| released       = November 30, 1955
| runtime        = 77 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
The Crooked Web is a 1955 film directed by Nathan Juran. It stars Frank Lovejoy, Mari Blanchard and Richard Denning. 

==Plot==
Stan Fabian runs a drive-in restaurant with girlfriend Joanie Daniel, whose brother Frank turns up for a visit. Joanie has declined to marry Stan because hes strapped for cash, but Frank tempts him with a proposition, mentioning that he and a partner hid a stash of gold in Germany during the war.

Stan accepts an offer to help recover the gold for a cut of the loot. What he doesnt know is that Joanie and Frank are actually undercover cops. A rich businessmans son was apparently killed by Stan during a deal gone wrong, but the German police are unable to extradite him to charge him with a crime.

Frank pretends to shoot his partner, using blanks. He secretly meets with Berlin chief of police Koenig, pretending to be looking for the gold. Stan fears a double-cross, but confesses his wartime murder to Joanie, and is shocked to be placed under arrest.

==Cast==
*Frank Lovejoy as Stanley Fabian
*Mari Blanchard as Joanie Daniel
*Richard Denning as Frank Daniel
*John Mylong as Herr Koenig
*Harry Lauter as Sgt. Mike Jancoweizc
*Steven Ritch as Ramon Ray Torres
*Lou Merrill as Herr Schmitt

==References==
 

==External links==
* 
* 
*  

 

 
 
 
 
 
 

 