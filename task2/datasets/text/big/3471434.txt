16 December (film)
 
 

{{Infobox Film
| name           = 16 December
| image          = 16December.jpg 
| director       = Mani Shankar
| producer       = Anjali Joshi  Arunima Roy
| writer         = Mani Shankar
| distributor    = 
| starring       = Danny Denzongpa Gulshan Grover Milind Soman Dipannita Sharma Sushant Singh Aditi Govitrikar
| released       =  
| runtime        = 158 minutes
| country        = India Hindi
| music          = Karthik Raja
}}
16 December (  spy thriller film by director Mani Shankar, based on a plot to destroy the capital city of India, New Delhi with a nuclear bomb on 16 December 2001 – 30 years after the surrender of Pakistan at the end of the Indo-Pakistani War of 1971. 
The film was highly critically acclaimed and was praised worldwide and many of the critics compared this film to   and was declared a big hit worldwide. It was selected to be screened at the 2002 Cannes Film Festival where the film was also highly praised.
 Victory Day), Independence for Bangladesh.

== Plot ==
A four member team—Vir Vijay Singh (Danny Denzongpa), Vikram (Milind Soman), Sheeba (Dipannita Sharma), and Victor (Sushant Singh) who are Indian Revenue Service officers belonging to the Income Tax Revenue Intelligence Wing, who have been wrongly implicated in the killing of their corrupt superior officer and removed from service, are hired by the Chief of the same agency to investigate a series of large illegal money transfers. The team is equipped with hi-tech equipment such as mini spy cameras, computers, internet and other communication devices. Through various encounters they discover that the money is being transferred to a Euro-Swiss Bank account. By means of an Indian employee, Sonal Joshi (Aditi Govitrikar) working in the Auckland (New Zealand) branch of the same bank, they investigate the account in New Zealand and, with her help, find that the money is being transferred to an international terrorist organization named KAALA KHANJAR. This organization, working in conjunction with Dost Khan (Gulshan Grover), manages to smuggle a Russian-made nuclear bomb into India. Dost Khan (Gulshan Grover) plans to explode the nuclear bomb on the same day, 16 December.

Although the ruling dictator of Pakistan surrendered unconditionally to India, some of the hard-lined Pakistani soldiers were bitter and angry at the surrender, as they wanted to continue fighting the Indians until their last breath. They retreated in silent and later came together to form their own groups of communal soldiers to carry out terrorists attacks against neighboring India.

Led by Dost Khan (Gulshan Grover), a hardliner Pakistani army officer, who against his wishes had to surrender after the end of 1971 war, the terrorists planned to take a revenge by having a nuclear explosion in the heart of New Delhi. They transport it into a music competition disguised as a musical instrument. When Vir Vijay Singh comes to know about the plan, he plans to find out the location of the nuclear bomb as soon as possible by taking the help of Remote Radiation Sensors in satellites and innumerable beggars in the city. This helps the team zero in on the location.

After they overpower most of the terrorists in a commando operation, Dost Khan comes to know about it and sets the nuclear bomb to explode in a few minutes. This creates a lot of problem for Vijay Vir Singh, as the bomb can be defused only by exclusive voice command of Dost Khan saying: Dulhan Ki Vidaai Ka Waqt Badalna Hai. They adopt a novel way to do it by speaking to Dost Khan and making him say fragments of this sentence without making him realize that it was being done to defuse the bomb. After the conversation was over, they synthesize the sentence to defuse the bomb just in time.

== Cast ==
* Danny Denzongpa as Vir Vijay Singh
* Milind Soman as Vikram
* Dipannita Sharma as Sheeba
* Sushant Singh as Victor
* Aditi Gowitrikar as Sonal Joshi
* Gulshan Grover as Dost Khan
* Reza as Irfan
* Sajeel Parakh
* Sohil Mathur as Pappu (Informer)

==Critical reception==
16 December received mostly positive reviews from critics in India and abroad. Ronjita Kulkarni of Rediff.com gave the movie 0/5 stars and wrote "For a first-timer, director Mani Shankar does a valiant job with 16 December. It has four songs, three of which appear in the background, but it certainly does not follow the routine song-and-dance formula. 16 December entertains as well makes you think". 
Taran Adarsh of Bollywood Hungama gave the movie 1/5 stars and wrote "The film does have a love angle though there is no undue focus on the romantic couple. Now for the film. In 16 December Mani Shankar has tried to explore white-collar crime. The premise being, millions of rupees leave the Indian shores daily to Swiss bank accounts. The account holders always remain a secret. What the director tries to do is trace this movement of money from the grass-roots level".
Rachit Gupta of Filmfare rated the movie -1/5 stars, stating, "16 December moves at a brisk pace. Mani Shankar does not waste time on unnecessary details. The film requires a fair amount of concentration to understand the chain of events".
 .

===Overseas===
Rachel Saltz of The New York Times wrote, "16 December may not be a box office hit. But it is definitely different from the usual love stories that we are subjected to."
Sneha May Francis of Emirates 24/7 wrote "After his debut in Tarkieb, Milind Soman gives a composed and good performance in this film. Debutante Dipannita Sharma lends some freshness to the film. There is no helplessness at any point of time."
Simon Foster of the Special Broadcasting Service gave the film 4 out of 5 stars and described it as "thrilling, fast-paced and loud a movie that catches hollywood standard".
Robert Abele of Los Angeles Times 16 December has all merits to strike a chord with the youth", praising dannys bravura performance, the films Hollywood style look and action sequences, as well as the music.

 

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Dil Mera Ek Tara" 
| Sadhana Sargam
|-
| 2
| "Main Cheez Badi Hoon Chaalu" Shaan
|-
| 3
| "I Am A Cool Cat"
| Shaan, Subhiksha
|-
| 4
| " Dil Ye Tera" KK
|-
| 5
| "Dhuan Dhuan Sa"
| Milind Soman, Chitra Sivaraman
|-
| 6
| "Chim Chimiya"
| Sapna Awasthi
|}

== See also ==
* Bangladesh Liberation War
* Indo-Pakistani War of 1971

== External links ==
*  
*  

 
 
 
 