Blockade (2006 film)
 

Blockade is a 2006 documentary film by Sergei Loznitsa. The film was shot with no dialogue and consists of black and white footage shot during the Siege of Leningrad. It has a run time of 52 minutes.

The documentary moves from initial depiction of military measures, and the marching of German prisoners of war through the hostile city streets, to the upheaval facing the city - the bombed out buildings and ice slicked streets -the increasingly horrific reality of wartime Leningrad -  cloth wrapped bodies being dragged to mass graves upon sleds -. filminwords.com  
  

== References ==
 
A Chronicle for Our Time, Loznitsas Blockade - Denise J. Youngblood, Russian Review 66,  October 2007
 
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 
 


 