Elephant Boy (film)
{{Infobox film
| name           = Elephant Boy
| image          = Elephantboyposter.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Robert J. Flaherty Zoltan Korda
| producer       = Alexander Korda (producer)
| based on       = Toomai of the Elephants John Collier Marcia De Silva Ákos Tolnay
| writer         = Rudyard Kipling
| narrator       = Sabu W.E. Holloway Walter Hudd John Greenwood
| cinematography = Osmond Borradaile
| editing        = Charles Crichton
| studio         = London Films
| distributor    = United Artists Corporation
| released       = 5 April 1937 (US) 9 April 1937 (UK)
| runtime        = 85 minutes
| country        = United Kingdom
| language       = English
| budget         =
| gross          =
}}
 Sabu in his film debut. Documentary filmmaker Robert J. Flaherty who produced some of the remarkable Indian footage and supervising director Zoltan Korda who completed the film won the Best Director Award at the Venice Film Festival. The film was made at the London Films studios at Denham, and in Mysore, India, and is based on the story "Toomai of the Elephants" from Rudyard Kiplings The Jungle Book (1894).

==Plot==
Toomai (Sabu Dastagir|Sabu), a young boy growing up in India, longs to become a hunter. In the meantime, he helps his mahout (elephant driver) father with Kala Nag, a large elephant that has been in their family for four generations.

Petersen (Walter Hudd) hires the father and Kala Nag, among others, for a large annual government roundup of wild elephants to be tamed and put to work. Amused by Toomai and learning that he has no one but his father to look after him, Petersen allows the boy to come too.

Strangely, no elephants have been seen in the region in a while, so Petersen has staked his reputation on a guess that they will be found further north. However, six weeks of hunting prove fruitless. He is ready to give up, but his right-hand man, Machua Appa (Allan Jeayes), persuades him to keep hunting for another month. When the other hired natives learn of Toomais ambition, they mock him, telling him that he will become a hunter only when he sees the elephants dance (a myth).

One night, Toomais father spots a tiger prowling near the camp and wakes Petersen. When the two go out to shoot the beast, Toomais father is killed. Kala Nags grief becomes so intense, he rampages through the camp, only stopping when Toomai calms him down.
 Bruce Gordon) to Kala Nag, as Toomai is too young for the job. When Rham Lahl beats the elephant, however, Kala Nag injures his tormenter. The mahout insists that Kala Nag be destroyed, as is the law. Petersen manages to get him to change his mind and accept 100 rupees instead by threatening to have him removed from the safety of the camp.

Unaware of this reprieve, Toomai takes Kala Nag and runs away into the jungle. There, they stumble upon the missing wild elephants, and Toomai sees them dancing. He leads Petersen to them. The other natives are awed, and hail him as "Toomai of the Elephants". Machua Appa offers to train the boy to become a hunter, a plan Petersen approves.

==Cast== Sabu as Toomai
* W. E. Holloway as Father
* Walter Hudd as Petersen
* Allan Jeayes as Machua Appa Bruce Gordon as Rham Lahl
* D. J. Williams (actor)|D. J. Williams as Hunter
* Wilfrid Hyde-White as Commissioner
* Iravatha as Kala Nag (uncredited)
* Harry Lane as Bit Part (uncredited)

==References==
 	

==Bibliography==

Michael Korda, Charmed Lives: The Fabulous World of the Korda Brothers (1980) 118-20.

==External links==
*  
*  
*  
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 