The Gaucho War
 
{{Infobox film
| name           =The Gaucho War
| image          = La Guerra Gauchaposter.jpg
| image_size     =
| caption        = Theatrical poster
| director       = Lucas Demare
| producer       =
| writer         = Ulyses Petit de Murat Homero Manzi Novel Leopoldo Lugones
| narrator       =
| starring       = Enrique Muiño Francisco Petrone Ángel Magaña Sebastián Chiola Amelia Bence René Mugica
| music          = Lucio Demare Jorge Di Lauro
| cinematography = Bob Roberts
| editing        =
| distributor    = Artistas Argentinos Asociados Estudios San Miguel
| released       = November 20, 1942
| runtime        = 95 minutes Argentina
| Spanish
| budget         =
| gross          =
}}
 1942 Silver Argentine historical based on Best Film, Best Director Best Adapted Screenplay  (Ulises Petit de Murat and Homero Manzior),  given by the Argentine Film Critics Association at the  1943 Argentine Film Critics Association Awards for the best films and performances of the previous year.
 royalist army, loyal to the Spanish monarchy. For exterior filming, a village was established in the same area where the actual conflict had taken place. The cast of some thousand participants was unprecedented in Argentine cinema until that time.
 Axis or Allies during neutrality during the war. The film stresses the values associated with nationalism as expressed in the union of the people, the army, and the church in defense of the country, which was considered by some a prelude to the revolutionary ideology that led to, on June 4, 1943, the overthrowing of the government of president Ramón Castillo.

The film was produced by Artistas Argentinos Asociados (Associated Argentine Artists), a cooperative of artists created just a short time before production began. It required an investment far beyond other productions of the period but the commercial success of the film allowed it to recover the cost in the first-run theaters, where it remained for nineteen weeks.  

== Plot ==
In Salta Province in 1817 during the War of Independence, the irregular forces commanded by General Martín Güemes carry out a guerrilla action against the Spanish army. The commander of a Spanish army contingent, Lieutenant Villarreal, is wounded, captured by the guerrillas, and put under the medical care of Asunción, the mistress of an estancia. She finds out from his idenfication paper that the Lieutenant, though serving in the Spanish army, was born in Lima. She persuades him of the justice of liberating America from Spain.
The patriot forces receive help from the sacristan of a chapel located next to the grounds of the royalist troops. The sacristan fakes loyalty to the king, but during the battles he sends messages to the gaucho guerrillas hiding in the mountains by means of a messenger boy and by ringing of the bell. When the royalists discover this, they attack and burn the chapel and smash the sacristans eyes. Blinded, the sacristan unwittingly guides the royalists to the patriot camp. The royalists then proceed to annihilate the gauchos. In the final sequence, after the battle, the only three surviving characters (the badly injured sacristan, an old man, and the lieutenant who has fallen in love with Asunción and converted to the patriot cause) see Güemes arriving troops, which will continue the battle.

=== Prologue ===
The film begins with a prologue on screen providing the historical circumstances of the place and time in which the action is placed, and advancing the position of its authors. From 1814 to 1818, Güemes and his gauchos resisted the royalist armies, that systematically ransacked the country from the Alto Peru since the withdrawal of the regular troops. This conflict of small battles was characterized by the heroism of the adversaries.

The opening states:  

== Cast ==
 , Enrique Muiño and Sebastián Chiola ]]

*Enrique Muiño as Sacristán Lucero.
*Francisco Petrone (Francisco Antonio Petrecca Mesulla) as Capitán Miranda
*Ángel Magaña as Teniente Villarreal
*Sebastián Chiola as Capitán Del Carril
*Amelia Bence as Asunción Colombres
*Ricardo Galache
*Dora Ferreiro
*Elvira Quiroga
*Juan Pérez Bilbao
*Carlos Campagnale
*Aquiles Guerrero
*Roberto Combi
*Amílcar Leveratto
*Antonio Cytro
*Carlos Enzo
*Roberto Prause
*René Mugica
*Raúl Merlo
*Ricardo Reinaldo
*Alberto Contreras (son)
*Antonia Rojas
*Laura Moreno
*José López
*Jacinta Diana

== Production ==

===The novel=== Ricardo Rojas said:
 Rojas, Ricardo, Historia de la literatura argentina, 4* edición, t. VIII pág. 638, Buenos Aires, 1957, Editorial Guillermo Kraft. }}

To write La guerra gaucha Lugones traveled to Salta Province, to visit the actual places where the events happened and to record the oral tradition of the area. It is an epic story composed of several histories described with a wide vocabulary full of metaphors. Dialogs are short, but descriptions and subjective vision are plentiful. The landscape characteristics and Saltas nature are described in detail and have great importance in the book.

===Historical context in Argentina ===
 
On February 20, 1938 Roberto M. Ortiz became president of Argentina. A member of the Unión Cívica Radical Antipersonalista party, he expressed his intention of ending the systemic electoral fraud imposed since the 1930 military coup. This idea found resistance within the political coalition named "The Concordance" ("La Concordancia") to which he belonged. Finally the worsening of his diabetes forced him to relinquish the presidency to his vice president Ramón Castillo, first in provisionally and after June 27, 1942 permanently. The new president was not in agreement with Ortizs policies and from his post he condoned the fraud practices, disappointing the followers who believed in the changes proposed by his predecessor.   Halperín Donghi, Tulio, La república imposible (1930-1945) pág. 236, 2004, Buenos Aires, Ariel Historia, ISBN 950-9122-85-8 
 First World War (1914–1918). Great Britain supported the decision as it was interested in Argentina being neutral and continuing the supply of food during the war.   Halperín Donghi, Tulio, La república imposible (1930-1945) pág. 261, 2004, Buenos Aires, Ariel Historia, ISBN 950-9122-85-8 
 Pearl Harbor. In January 1942, the Third Consulting Meeting of Chancellors of the American Republics met in Río de Janeiro as the U.S. wished other American nations to break relations with the Axis powers. Argentina, which had had frictions with the U.S. in previous years,   Rapoport, Mario, ¿Aliados o neutrales? La Argentina frente a la Segunda Guerra Mundial pág. 11, 1988, Buenos Aires, Editorial Universitaria de Buenos Aires, ISBN 950-23-0414-4  was opposed to said goals and influenced successfully to "recommend" the breakup of relations instead of making it mandatory.   Rapoport, Mario, ¿Aliados o neutrales? La Argentina frente a la Segunda Guerra Mundial pág. 174, 1988, Buenos Aires, Editorial Universitaria de Buenos Aires, ISBN 950-23-0414-4    Halperín Donghi, Tulio, La república imposible (1930-1945) pág. 264, 2004, Buenos Aires, Ariel Historia, ISBN 950-9122-85-8 

The problems associated with foreign policy took on more importance in Argentina and revived the conflict between the three political factions, the one pushing for siding with the Allies of World War II|Allies, the neutrals, and the one more in tune with the Axis. This latter minority group included the followers of nationalism and some officers of the army. The subject of the position the country should take on the war displaced other issues in the national political arena.    Buchrucker, Cristian, Nacionalismo y peronismo pág. 222, 1987, Buenos Aires, Editorial Sudamericana, ISBN 950-07-0430-7 
 Socialist Party of Argentina, and in the unions organized under the umbrella of the Confederación General del Trabajo (Argentina)|Confederación General del Trabajo the favorable currents for the State to become interventionist were growing, in order to push the preservation on the national interests and promote industrialization.
 tango and indigenous "gaucho roots".   La guerra gaucha was then selected as the subject, written and filmed in the context of expansive nationalism and debates over issues of war.

=== State of the film industry in Argentina === directors debuted. celluloid due to the war. In 1941, there were 47 openings and in 1942, 57.   Mahieu, José Agustín, Breve historia del cine argentino pág. 30, 1966, Buenos Aires, Editorial Universitaria de Buenos Aires 
 

=== Artistas Argentinos Asociados ===
 
A group of unemployed artists, Enrique Muiño, Elías Isaac Alippi|Elías Alippi, Francisco Petrone, Ángel Magaña, the director Lucas Demare and the chief of production of a movie company Enrique Faustín (son) met regularly at the beginning of the 1940s at the "El Ateneo" cafe in Buenos Aires.

The Ateneo Group ("Barra del Ateneo") decided to found a cooperative film production company following the style of the American United Artists, so on September 26, 1941 they started "Artistas Argentinos Asociados Sociedad Cinematográfica SRL".   Maranghello, César, Artistas Argentinos Asociados. La epopeya trunca, 2002, Buenos Aires pág. 31, Ediciones del Jilguero, ISBN 987-9416-04-X    Zolezzi, Emilio, Noticias del viejo cine criollo, pág.119, 2006, Buenos Aires, Ediciones Lumiere S.A., ISBN 987-603-018-3 

=== Origins of the film ===
Artistas Argentinos Asociados had the idea of making this movie since the company had been established. Homero Manzi had the idea since he wrote the script to the film "Viento Norte" ("North Wind") and convinced director Lucas Demare of the projects viability. Francisco Petrone proposed that the script be written by Manzi and Ulyses Petit de Murat. The rights for the movie were purchased from Leopoldo "Polo" Lugones (son of the writer) for 10,000 pesos received two jazz records that were unavailable in the country.   Maranghello, César, Artistas Argentinos Asociados. La epopeya trunca, 2002, Buenos Aires pág. 35, Ediciones del Jilguero, ISBN 987-9416-04-X 
 

Meanwhile, Elías Alippi, who would star in the role of captain Del Carril, fell ill with cancer (he would die on May 3, 1942). The company, knowing he was not in physical condition to survive the tough filming schedule and not wishing to replace him for another actor while he was alive, postponed the filming with an excuse and started to film "El viejo Hucha" ("Old Man Hucha"), in which he had no role.

Remembering the proposal to write the screenplay, Ulyses Petit de Murat said:  

Due to having had spent all the budget needed for La guerra gaucha on El viejo Hucha, the partners at Artistas Argentinos Asociados decided to fund the film with their own fees. This financial effort was insufficient and they had to partner with San Miguel Studios and undersell the exhibition rights for the movie earlier in some areas. These decisions allowed them to make the film with "a little less belt-tightening but without splurging".   Zolezzi, Emilio, Noticias del viejo cine criollo, pág.70, 2006, Buenos Aires, Ediciones Lumiere S.A., ISBN 987-603-018-3 

=== Script ===

==== Homero Manzi ====
 
  castilian professor but for political reasons (in addition to his membership in the Unión Cívica Radical) he was expelled of his professorship and decided to dedicate himself to the arts.

In 1935 he participated on the beginnings of FORJA (Fuerza de Orientación Radical de la Joven Argentina – Force of Radical Orientation of the Young Argentina), a group whose position has been classified as “peoples nationalism”. It was centered in the problematic Argentina and Latin America and on its discussions suggested “reconquer the political Sunday from our own land” since it considered the country was still under a colonial situation. It supported neutrality in WWII on the premise that was no great interest was in play in Argentina or Latin America, it was more of a rejection position towards fascism just as much as communism.   Buchrucker, Cristian, Nacionalismo y peronismo pág. 258 y 269, 1987, Buenos Aires. Editorial Sudamericana, ISBN 950-07-0430-7 

In 1934, Manzi founded Micrófono ("Microphone") magazine which covered subjects related to radiotelephony, Argentine movies and film making. He wrote the screenplay for Nobleza Gaucha in 1937 in collaboration with Hugo Mac Dougall, and a remake of Huella ("Footprint") (1940), for which they received second prize from Buenos Aires City Hall and also Confesión ("Confession") (1940), without achieving commercial success with any of these movies.   Salas, Horacio, Homero Manzi y su tiempo pág. 198, 2001, Buenos Aires, Javier Vergara editor, ISBN 950-15-2244-X 

In 1940 Manzi started what would be a long collaboration with Ulyses Petit de Murat, writing the screenplay for Con el dedo en el gatillo ("Finger on the trigger") (1940) and later Fortín alto ("High Fort") (1940).

==== Ulyses Petit de Murat ====
 
Ulyses Petit de Murat was born in Buenos Aires on January 28, 1907 and was interested in literature and journalism from a young age. He was in charge of the music page in the daily magazine Crítica and, with   In 1932 he moved to the motion film section of Crítica and in 1939 wrote his first cinematographic script for the movie Prisioneros de la Tierra, an adaptation of four tales of Horacio Quiroga, made with his son, Dario Quiroga, who later in 1940 wrote Con el Dedo en el Gatillo, with the collaboration of Homero Manzi.

The screenwriters started by selecting the stories that would provide them with the elements for the work. Dianas was chosen as the main source, some characters were taken from Alertas and some from other stories. They compiled the words, traditions, life styles and idioms from that era for which they used books and even a trip was made to Salta to talk with the locals. A script Then a text was made from the tales and a first draft of the images. At this point the director and actors collaborated with their comments and finally the final script was written.   Guillermo Corti, cit. por Salas, Horacio, Homero Manzi y su tiempo pág. 205, 2001, Buenos Aires, Javier Vergara editor, ISBN 950-15-2244-X 

=== Direction===
 
Born July 14, 1907 Demare was a music scholar. In 1928, he traveled to Spain as a bandoneón player for the Orchestra Típica Argentina, where his brother Lucio also played. In 1933 he worked as an interpreter and singer for Spanish movies Boliche and Aves sin rumbo.  
Demare quit the orchestra and started working in the film industry; he quickly rose from chalkboard holder to director assistant. Some time later he was hired to debut as a director, but the civil war broke out and he returned to Buenos Aires.   Maranghello, César, Artistas Argentinos Asociados. La epopeya trunca, 2002, Buenos Aires pág. 29, Ediciones del Jilguero, ISBN 987-9416-04-X 

Emilio Zolezzi, aside from being a movie critic, was also the Artistas Argentinas Associados attorney. He tells about the director:   Zolezzi, Emilio, Noticias del viejo cine criollo, pág.84, 2006, Buenos Aires, Ediciones Lumiere S.A., ISBN 987-603-018-3 }}

When he returned to Spain, his brother Lucio got him a job as custodian in the Rio de la Plata cinematographic studios. In 1937, he was hired as director and screen-writer for the movies Dos amigos y un amor (Two friends and one love) and Veinticuatro horas de libertad (Twenty-four hours of liberty), both starring comedy actor Pepe Iglesias. In 1939, he directed El hijo del barrio (1940, Son of the neighborhood), Corazón the Turco (1940, Turkish Heart) and Chingolo (1941) all of them with their own script.   Maranghello, César, Artistas Argentinos Asociados. La epopeya trunca, 2002, Buenos Aires pág. 30, Ediciones del Jilguero, ISBN 987-9416-04-X 
This movie was well received by the public and critics, “It consolidated the exceptional tech team accompanied by Artistas Argentinos Asociados: his brother Lucio on the music band, the assistant Hugo Fregones, the montajussta Carlos Rinaldi, the set builder Ralph Pappier, the lighting specialist from the United States Bob Roberts (from the American Society of Cinematographers), the cinematographer Humberto Peruzzi, the electrician Serafín de la Iglesia, the make up artist Roberto Combi and some others.” 
The following movie was El cura gaucho, in which he met Enrique Muriño, but even with his abundant commercial success, he was fired from Pampa Films. 

=== Filming ===
Lucas Demare thought that January and February (summer) were the best months to work on the filming in Salta but they were told that it was better to do it in winter due to summer being flood season. Demare travelled to Salta to reconnoiter the area. Later, the crew and equipment moved to an old estate and big house. They worked on a big ballroom and had two small rooms; each crewman had a cot and an upside down beer wooden box as night-stand. The actresses and Enrique Muiño, due to his age, stayed in a hotel.   Demare, Lucas, Cómo se filmó La guerra gaucha, pág. 132, en Cuentos de cine (Sergio Renán, sel.), 1996, Buenos Aires, Alfaguara, ISBN 950-511-259-9 

 
At their arrival in Salta, they met with the local military commander, Colonel Lanús, but he was not eager to help, instead placing obstacles in their way. Demare told how they solved the problem:
  Maranghello, César, Artistas Argentinos Asociados. La epopeya trunca, 2002, Buenos Aires pág. 51, Ediciones del Jilguero, ISBN 987-9416-04-X }}

Demare had brought gaucho clothing for the cast, but he realized they were not appropriate for the feel he wanted in the movie as they were brand-new, so he traded the new clothing for local gauchos own clothing.   Ardiles Gray, Julio, Lucas Demare: mi vida en el cine, en diario Convicción, Buenos Aires, 10 de agosto de 1980, Suplemento Letras n* 106, III   Demare sent Magaña and Chiola on long horse rides to "weather" their uniforms and accustom the actors to riding. The locals were surprised to encounter two soldiers in antiquated uniforms. 

Lucas Demare shows up in the film as an extra a few times. The scene in which the town burns had to be done in one take as they could not afford to rebuild it. Demare had the cameramen and the rest of the crew dressed as gauchos or royalist troops so that if they were accidentally included, they would not ruin the shot. While directing this scene, a sudden wind change moved the fire towards Demare himself making him lose his wig and singeing his fake beard and mustache.

In another scene, Demare played the part of a Spanish soldier who, being attacked by the gauchos, receives a lance hit through the chest. Magaña tells   Maranghello, César, Artistas Argentinos Asociados. La epopeya trunca, 2002, Buenos Aires pág. 61, Ediciones del Jilguero, ISBN 987-9416-04-X }}

In another scene where the characters played by Amelia Bence, Petrone and Magaña argue, the latter was supposed to fall down the stairs but doubted his ability to do so. Demare stood at the top of the stairs with his back to it and rolled down, to demonstrate that the scene could be done without undue risk. This was in fact the sequence shown in the movie.   Maranghello, César, Artistas Argentinos Asociados. La epopeya trunca, 2002, Buenos Aires pág. 62, Ediciones del Jilguero, ISBN 987-9416-04-X 

A scene where a group of horses ran down a hill with burning branches tied to their tails needed to be filmed from in front, so the crew built a hut made of wood, stones, and rocks in which stood Peruzzi the cameraman, who tells that "At the order of Action! I saw this mass of heads and hooves coming at me at full speed, and did not breath until I saw them open up to the sides of the hut, right in front of me. We had to improvise and replace the lack of technology with smarts, ingenuity and valor."

The filming included more than 1,000 actors as extras for the crowd scenes, although only eighty actors had speaking parts.   Pappier, Ralph, Un anticipo de La guerra gaucha, en revista Cine argentino, Buenos Aires, 19 de noviembre de 1942, n* 237  Among the extras there were local gauchos hired by the producers and others provided as laborers by their employer, the Patrón Costa, a wealthy local family.   Maranghello, César, Artistas Argentinos Asociados. La epopeya trunca, 2002, Buenos Aires pág. 57, Ediciones del Jilguero, ISBN 987-9416-04-X  There were also the aforementioned fencing trainer and soldiers lent by the military garrison and two pato players from Buenos Aires, experts falling from horses. As the gauchos did not want to be dressed as Spaniards, military conscripts played the part.   Maranghello, César, Artistas Argentinos Asociados. La epopeya trunca, 2002, Buenos Aires pág. 53, Ediciones del Jilguero, ISBN 987-9416-04-X 

=== Location ===
For the scenes in the local village where the royalists had established their headquarters, they selected the village of San Fernando. Nearby is the Gallinato Creek, where they filmed the gaucho encampment scenes and the assault against Mirandas woman.

They brought material from Salta in fifty trucks to build a village. It had an area of about a thousand square meters, fifteen houses, a church with a belfry, hospital, horse barn, corrals, commanders office, cemetery, and ovens, all of which was destroyed by the fire in the final scenes. The director requested five hundred horses, four hundred cattle, oxen, mules, burros and chickens. Also many props such as wheelbarrows, wagons, and period-military equipment.  

The interior and exterior scens of the Asunción ranch, the royalist encampment at night, the interior of the church and belfry, the death of the child and the musical number by the Ábalos Brothers group were filmed at the studios in Buenos Aires.

== Soundtrack and choreography==
The music score was done by Lucio Demare. Born in Buenos Aires on 9 August 1906, he studied music from the age of six and from the age of eight he was playing piano in movie theatres &ndash;it was still the age of silent movies.

In Spain in 1933, he created the music for two movies in which he also acted. He started his work in Argentine cinema in 1936 with the musical score for the film Ya tiene comisario el pueblo ("The village now has a constable"), directed by Claudio Martínez Payva  and in 1938, he continued with Dos amigos y un amor ("two friends and one love"), with Francisco Canaro, and directed by his brother Lucas Demare.

The musical numbers and native dances were played by the Ábalos Brothers group.

== Reception ==

La guerra gaucha was well received by the critics and the public and received several awards. The article in the El Heraldo de Buenos Aires said:   diario El Heraldo de Buenos Aires, cit. por Manrupe, Raúl y Portela, María Alejandra, Un diccionario de films argentinos (1930-1995) pág. 261, 2001, Buenos Aires, Editorial Corregidor, ISBN 950-05-0896-6 }}
  La Nación said:  diario La Nación, cit. por Zolezzi, Emilio, Noticias del viejo cine criollo, pág.86, 2006, Buenos Aires, Ediciones Lumiere S.A., ISBN 987-603-018-3 }}

Claudio España wrote:  España, Claudio, El cine sonoro y su expansión pág. 80 en Historia del cine argentino (Jorge Miguel Couselo, director), 1984, Buenos Aires, Centro Editor de América Latina }}

The opinion of film critic José Agustín Mahieu is as follows:   Mahieu, José Agustín, Breve historia del cine argentino, pág. 30, 1966, Buenos Aires, Editorial Universitaria de Buenos Aires }}

Lastly, César Maranghello says:   Maranghello, César, Artistas Argentinos Asociados. La epopeya trunca, 2002, Buenos Aires pág. 49, Ediciones del Jilguero, ISBN 987-9416-04-X }}

The film stayed on the opening theaters for nineteen weeks where it was seen by 170,000 viewers, including four weeks in Montevideo by that time.

== Awards ==
La guerra gaucha received the following awards: Argentine Film Critics Association Argentine Academy of Cinematography Arts and Sciences
*Best Screenplay (Petit de Murat and Manzi) from the Comisión Nacional de Cultura
*First prize for Best Picture, Best Director, Best Screenplay, Best Lead Actress (Amelia Bence), Best Actor (Francisco Petrone), Best Photography, Best Music and Best Sound Editing from Municipalidad de la Ciudad de Buenos Aires
*Best Foreign film in Cuba shown in 1947 from the Asociación de Cronistas Cinematográficos de La Habana (Cuba / 1948)

== Economic aspects == pesos  US dollars. extras in Salta were paid between 3 and 4 pesos per workday, when a theater seat cost 3 pesos. Amelia Bence was paid 5,000 pesos for about six days of filming. This was completely recouped in the nineteen weeks the film stayed at the opening theaters.   Zolezzi, Emilio, Noticias del viejo cine criollo, pág.71, 2006, Buenos Aires, Ediciones Lumiere S.A., ISBN 987-603-018-3 

Nonetheless, due to the partners lack of business experience and their scant resources put into starting the business, critical and public acclaim did not translate into big earnings.

== References ==
 

==External links  ==
 
*  
*   at Cinenacional

== Authorization ==
 This article incorporates material from  , which has given   to the use of content and images and published them under GNU license. 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 