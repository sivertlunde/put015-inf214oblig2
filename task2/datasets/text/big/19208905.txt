Mysterious Mr. Moto
{{Infobox film
| name           = Mysterious Mr. Moto
| image          =
| image_size     =
| caption        = Norman Foster
| producer       = Sol M. Wurtzel
| based on       =  
| screenplay     = Philip MacDonald Norman Foster Erik Rhodes
| music          = Samuel Kaylin
| cinematography = Virgil Miller
| editing        = Norman Colbert
| distributor    = Twentieth Century-Fox
| released       = 17 September 1938 SCREEN NEWS HERE AND IN HOLLYWOOD: RKO and Cantor Negotiating for Comedian to Star on Percentage Basis MGM GETS WIMAN SHOW Acquires Rights to I Married an Angel--New Mr. Moto Film Opens Today Picture for Bing Crosby Of Local Origin
Special to THE NEW YORK TIMES.. New York Times (1923-Current file)   17 Sep 1938: 20.  
| runtime        = 63 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

Mysterious Mr. Moto, produced in 1938 by Twentieth Century Fox, is the fifth in a series of eight films starring Peter Lorre as Mr. Moto. NEWS OF THE SCREEN: George Arliss Will Appear in Suez at 20th CenturyFox-Donald OConnor Gets Role in Crosby Film Other Items at Fox Coast Scripts Of Local Origin Norman Foster.

It was originally known as Mysterious Mr. Moto of Devils Island.
==Synopsis==
The film opens with a daring escape from the French penal colony on Devil’s Island.  Mr. Moto, pretending to be Ito Matsuka, a Japanese murderer, is in the company of Paul Brissac, who belongs to a group of assassins.  Brissac changes his name to Romero when they arrive in London and Moto stays on as his houseboy.

Moto then uncovers a plot to assassinate pacifist industrialist Anton Darvak.
==Cast==
* Peter Lorre as Mr. Kentaro Moto
* Mary Maguire as Ann Richman
* Henry Wilcoxon as Anton Darvak Erik Rhodes as David Scott-Frensham
* Harold Huber as Ernst Litmar Leon Ames as Paul Brissac (alias Romero)
* Forrester Harvey as George Higgins
* Fredrik Vogeding as Gottfried Brujo
* Lester Matthews as Sir Charles Murchison John Rogers as Sniffy Karen Sorrell as Lotus Liu
==Production== Michael Whalen in the cast. RICHARD DIX CAST FOR GROUND CREW: To Star in Picture Which RKO Will Make One of Its More Impressive Productions DICK POWELL GETS ROLE Will Appear in The Hottentot-Start Cheering Opens at Rialto Here This Morning Frances Farmer Replaced Coast Scripts Of Local Origin
Special to THE NEW YORK TIMES.. New York Times (1923-Current file)   16 Mar 1938: 21.   It was an early Hollywood role for Australian actor Mary Maguire. In Hollywood
With HEDDA HOPPER. The Washington Post (1923-1954)   07 Apr 1938: X26.  

During filming Peter Lorre, as Moto, impersonated a seventy-year-old German painter. Age Moto 40 Years In 20 Minutes
The Washington Post (1923-1954)   08 May 1938: TT4.  
==References==
 
==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 

 