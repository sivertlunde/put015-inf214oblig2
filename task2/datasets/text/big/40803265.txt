Time Piece
{{Infobox film name          = Time Piece image         =  caption       =  director      = Jim Henson writer        = Jim Henson starring      = Jim Henson producer      = Jim Henson music         = Don Sebesky
|cinematography= Ted Nemeth released       = 1965 studio        = The Jim Henson Company|Muppets, Inc. distributor   = Pathé|Pathé Contemporary Films runtime       = 9 min. language  English
|country       = United States
}} experimental short Best Live Action Short Film in 1966. 

==Plot== doctor enters the room and checks the mans heart rate, which begins to pulse rhythmically.

As the rhythm increases, the film begins to follow the mans peculiar habits such as crossing a busy street in different clothes and different locations, working in a busy office, working on a conveyer belt, walking through different locations and ending up in a forest where he has the appearance of Tarzan, eating dinner with his wife, walking down the street seeing pogo stick riders, and visiting a strip club while simultaneously maintaining himself in motion.
 strikes twelve and the films events flash quickly on-screen.

Back in the hospital room, the doctor covers the mans seemingly lifeless body. The camera then pans up towards the doctors face, revealing him to be the same man smiling gleefully while winking.

==Cast==
* Jim Henson - Man
* Enid Cafritz - Mans Wife Gorilla Suit 
* Jerry Juhl - Bartender 
* April March - 
* Sandy Patterson - 
* Diana Birkenfield -  Dave Bailey - 
* Dennis Paget - 
* Jim Hutchison - 
* Barbara Richman - 

==Production==
Unlike most films, Time Piece was not written as a script. Instead, Jim Henson had storyboarded the entire film prior to filming.    Between shuffling performances with The Muppets for The Jimmy Dean Show and film commercials, Henson shot the film intermittently from June 1964 to May 1965.  Due to this restricted time frame, every shot in the film lasts only one to four seconds. Henson even calculated the amount of frames each shot would contain. 
 Muppet designer Don Sahlin was responsible for the films visual effects shots. 

==Release== Paris Theatre in Manhattan. 
 CINE Eagle Award and the American Film Festivals Blue Ribbon Award, and received recognition at the XII International Short Film Festival Oberhausen.  

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 