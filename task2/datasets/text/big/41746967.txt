Dark Fields (2006 film)
{{Infobox film
| name           = Dark Fields
| image          = Dark Fields.jpg
| alt            =
| caption        =
| director       = Mark McNabb Al Randall
| producer       = Mark McNabb Al Randall
| writer         = Al Randall
| starring       = Jenna Scott Lindsay Dell Eric Phillion Brian Austin Jr. Ryan Hulshof A.K. Randall
| music          = Levi Oliver
| cinematography = Y. Robert Tymstra
| editing        = Mark McNabb
| studio         = Rigtown Pictures 402 Films Skylight Films
| distributor    = Lionsgate Home Entertainment
| released       =   }}
| runtime        = 80 minutes
| country        = United States Canada
| language       = English
| budget         =
| gross          =
}}
Dark Fields is a Canadian–American horror film directed by Mark McNabb and Al Randall, written by Randall, and starring Jenna Scott, Lindsay Dell, Eric Phillion, Brian Austin Jr., and Ryan Hulshof as teens hunted down by a psychopathic farmer played by Al Randall. Filmed in October of 2003, on a budget of $1,000, it was not released until September 2006. 

== Plot ==
 
After their car runs out of gas, a group of teens steal gas from an isolated house.  Unknown to them, the owner of the house, Farmer Brown, had a traumatic past involving gasoline thieves.  Farmer Brown goes insane with rage and hunts down the teens, one by one, and kills them.

== Cast ==
* Jenna Scott as Taylor
* Lindsay Dell as Justine
* Eric Phillion as Josh
* Brian Austin, Jr. as Zach
* Ryan Hulshof as Drew
* Al Randall as Farmer Brown

== Release ==
Lionsgate released Dark Fields on September 12, 2006. 

== Reception ==
Steve Anderson of Film Threat rated it 1/5 stars and wrote, "I dont believe I ever want to see the barrel that Lions Gate had to scrape the bottom of to dredge up this roaring suckfest."   Scott Foy of Dread Central rated it 1/5 stars and called it a boring, pointless film with poorly written characters.  

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 
 


 