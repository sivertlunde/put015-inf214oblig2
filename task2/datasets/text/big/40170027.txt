Supermensch: The Legend of Shep Gordon
 
{{Infobox film
| name           = Supermensch: The Legend of Shep Gordon
| image          = Supermensch The Legend of Shep Gordon poster.jpg
| border         = 
| alt            = 
| caption        = Theatrical release poster
| director       = Mike Myers
| producer       = Mike Myers
| writer         = 
| starring       = Shep Gordon
| music          = 
| cinematography = Michael Pruitt-Bruun 	
| editing        = Joseph Krings 
| studio         = A&E IndieFilms Nomoneyfun Films
| distributor    = RADiUS-TWC
| released       =  
| runtime        = 84 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $220,065   
}}

Supermensch: The Legend of Shep Gordon is a 2013 American documentary film about talent manager Shep Gordon, produced and directed by Mike Myers in his directorial debut.   The film is the account of Gordons career and his clients such as Alice Cooper, Blondie (band)|Blondie, Teddy Pendergrass, and Pink Floyd. The film also addresses Gordons personal life and his interest in cooking, producing films, and his Buddhist beliefs.

The film was screened in the Gala Presentation section at the 2013 Toronto International Film Festival.     It won the audience award for best documentary at the 2014 Sarasota Film Festival,  and also screened at the 2014 Tribeca Film Festival.   It was released theatrically on June 6, 2014. 

==Cast==
* Shep Gordon as himself
*Alice Cooper as himself
*Michael Douglas as himself
*Emeril Lagasse as himself
*Anne Murray as herself
*Willie Nelson as himself
*Derek Shook as himself
*Sylvester Stallone as himself 
*Mike Myers as himself

==Reception==
Supermensch: The Legend of Shep Gordon received generally positive reviews upon its release.   gives the film a score of 64/100 based on reviews from 26 critics, indicating "generally favorable reviews".  David Rooney of  , March 15, 2014. 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 