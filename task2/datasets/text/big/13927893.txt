Special Delivery (1927 film)
{{Infobox film
| name           = Special Delivery
| image          = 
| caption        =  Fatty Arbuckle (as William Goodrich)
| producer       = Jesse L. Lasky B. P. Schulberg Adolph Zukor
| writer         = Eddie Cantor John F. Goodrich George Marion Jr. Larry Semon
| starring       = Eddie Cantor
| music          = 
| cinematography = Harry Hallenberger
| editing        = 
| distributor    = Paramount Pictures
| released       =  
| runtime        = 60 minutes
| country        = United States Silent English intertitles
| budget         = 
}}
 silent comedy Fatty Arbuckle (as "William Goodrich"), starring Eddie Cantor, written by Cantor, John F. Goodrich and George Marion Jr. (with Larry Semon, uncredited). It was released by Paramount Pictures.  

==Cast==
* Eddie Cantor - Eddie, The Mail Carrier
* Jobyna Ralston - Madge, The Girl
* William Powell - Harold Jones Donald Keith - Harrigan, The Fireman
* Jack Dougherty - Flannigan, a cop
* Victor Potel - Nip, a detective Paul Kelly - Tuck, another detective
* Mary Carr - The Mother
* Marilyn Cantor Baker - Extra (uncredited) (as Marilyn Cantor)
* Marjorie Cantor - Extra (uncredited) Tiny Doll - Baby on Eddies Route (uncredited) Robert Livingston - Extra at Postal Ball (uncredited)
* Natalie Cantor Metzger - Extra (uncredited) (as Natalie Cantor)
* Spec ODonnell - Office Boy (uncredited)

==See also==
* Fatty Arbuckle filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 