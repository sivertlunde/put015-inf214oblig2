A Bullet for Sandoval
 
{{Infobox film
 | name =A Bullet for Sandoval 
 | image =  A Bullet for Sandoval.jpg
 | caption =
 | director =   Julio Buchs
 | writer =
 | starring = Ernest Borgnine
 | music =   Gianni Ferrio
 | cinematography =   Francisco Sempere 
 | editing =
 | producer =
 | distributor =
 | released =  	1969
 | runtime =
 | awards =
 | country =
 | language =
 | budget =
 }}
A Bullet for Sandoval (also known as Those Desperate Men Who Smell of Dirt and Death and Desperate Men) is a 1969 spaghetti western film. It is a co-production between Spain (where it was released as Los Desperados) and Italy (where is known as Quei disperati che puzzano di sudore e di morte). The film was generally well received by critics. 

==Plot==
John Warner is a soldier for the Confederate States of America. When he receives a message that his lover is about to deliver a child he becomes a deserter. On his way to her he is captured and brought before a tribunal. Yet two old friends make sure he can escape. He makes it to the town where he expects his lover to be. Only is he too late too marry her because shes died in the meantime. He tries to take care of his child as a single father but the citizens of this town arent supportive and thus another tragedy takes place. 

== Cast == George Hilton: Corporal John Warner
* Ernest Borgnine: Don Pedro Sandoval
* Alberto De Mendoza: Lucky Boy
* Leo Anchóriz: Padre Converso
* Annabella Incontrera: Rosa Sandoval
* Antonio Pica: Sam Paul
* Manuel Miranda: Francisco Sandoval
* Gustavo Rojo: Guadalupano
* Andrea Aureli: Morton
* Manuel De Blas: José Sandoval

==Plot==
The love of army deserter John Warner dies at deliverance and their baby child perishes after its grandfather, the landowner Sandoval, rejects it. Warner forms a feared outlaw gang with of two fellow deserters, one runaway lay brother, and some outlaws that join along the way.  The local ranchers call in the army for protection, but except for an episode where Warner refuses to make a woman a hostage for money, we mainly see the gang harass Sandoval. One of the gang eventually tries to sell out Warner for a reward. The four core members stay together until Warner has avenged himself on Sandoval, and then they die a spectacular death at a shoot-out in a bull-fighting arena.

==Reception==
In his discussion of the cases of double motives in Spaghetti Western films Fridlund contrasts stories like A Bullet for Sandoval - where the revenge motive of Warner comes into conflict with the monetary motive of some gang members - with the bounty killer pair in For a Few Dollars More, where at first their common monetary motive creates a conflict which is resolved when the revenge motive of the Mortimer character is revealed and he leaves the collection of all the bounty to the Man with No Name character.  

==References==
 

==External links==
* 

 
 
 
 
 


 
 