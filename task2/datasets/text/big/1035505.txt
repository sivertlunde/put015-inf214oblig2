Pool Sharks
{{Infobox film
| name           = Pool Sharks
| image          = Pool Shark (1915).webm thumbtime=1
| caption        =
| director       = Edwin Middleton
| producer       = Franco Cristaldi
| writer         = W.C. Fields
| narrator       =
| starring       = W.C. Fields Bud Ross
| music          =
| cinematography =
| editing        =
| distributor    = Mutual Film Corporation  
| released       = September 19, 1915 (U.S.)
| runtime        = 15 min.
| country        = United States Silent
| budget         =
}}
 1915 silent silent short film. The film is notable for being the film acting and writing debut of W. C. Fields, and also features an early stop-motion animation scene, during a game of Pocket billiards|pool.

==Plot summary== romantic slapstick comedy short. Fields and his rival (played by Bud Ross) vie over the affections of a woman. When their antics get out of hand at a picnic, it is decided that they should play a game of Billiards|pool. Both of them are pool sharks, and after the game turns into a farce, a fight ensues. Fields throws a ball at his rival, who ducks. The ball flies through the window and breaks a hanging goldfish bowl, soaking the woman they are fighting over and leaving goldfish in her hair. She storms into the pool hall and rejects both men.

==Production==
===Casting=== Janice Meredith

Fields wore his obviously fake moustache in this film, as he did in all of his silent films. His character and mannerisms bear some resemblance to Charlie Chaplins, although  the persona Fields later developed in his sound comedies is foreshadowed  during the picnic scene, when Fields character dumps a small child out of a chair so that he can steal it to get closer to the woman he is chasing.

===Animation===
 
Fields was an expert Juggling|juggler. As with his early films, Pool Sharks was intended to highlight a pool ball juggling act that featured in the actors vaudeville show. In the final film, however, there is only a brief shot of Fields juggling several billiard balls, as his act was largely replaced with several poorly edited stop motion sequences depicting impossible shots, such as the balls jumping off the table and re-racking themselves on the wall. Though innovative for the time, they are poorly animated, with obvious edits, and the animators hand can actually be seen moving the balls along in one of the frames.

==Reaction==
Today, Pool Sharks is best remembered as Fields first film effort. Film historian William K. Everson critiques the film as an "auspicious debut", with Fields routines and pacing already finely honed. It was one of two short films Fields made for a company called Gaumont Film Company|Gaumont, distributed by Mutual. He and Ross made another short around the same time, His Lordships Dilemma.

==References==
*The Films of W.C. Fields, by Donald Deschner, The Citadel Press, New York, 1966.
*The Art of W.C. Fields, by William K. Everson, Bonanza Books, New York, 1967.

== External links ==
*  
*  
*  
*   on YouTube
*   on YouTube

 
 
 
 
 
 
 
 