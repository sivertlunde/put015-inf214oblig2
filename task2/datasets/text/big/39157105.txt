Sunlight Jr.
 
{{Infobox film
| name           = Sunlight Jr.
| image          = Sunlight Jr. movie POster.jpg
| caption        = Film poster
| director       = Laurie Collyer
| producer       = 
| writer         = Laurie Collyer
| starring       = Naomi Watts Matt Dillon
| music          = J Mascis
| cinematography = Igor Martinovic
| editing        = 
| distributor    = 
| released       =     
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
}}

Sunlight Jr. is a 2013 American drama film directed by Laurie Collyer and starring Matt Dillon and Naomi Watts.   

==Cast==
* Norman Reedus as Justin 
* Naomi Watts as Melissa 
* Matt Dillon as Richie 
* William Haze as Actor 
* Tess Harper as Kathleen 
* Antoni Corone as Edwin 
* Vivian Fleming-Alvarez as Store Patron 
* Keith Hudson as Micky 
* Victoria Vodar as Pretty Woman 
* Rachael Thompson as Crime and Punishment Woman
* Beth Marshall as Molly

==Reception==
According to Rotten Tomatoes, 66% of critics have given the film positive reviews, based on 32 reviews, while the aggregate consensus is "It doesnt offer much in the way of uplift, but Sunlight Jr.s grim storyline is offset by Laurie Collyers empathetic screenplay and strong performances from Naomi Watts and Matt Dillon."  At Metacritic, which assigns a weighted mean rating out of 100 to reviews from mainstream critics, the film holds an average score of 61, based on 19 reviews, which indicates "generally favorable reviews." 

==Awards==

{| class="wikitable" style="font-size:90%"
|- style="text-align:center;"
! colspan=4 style="background:#B0C4DE;" | Awards
|- style="text-align:center;"
! style="background:#ccc;"| Award
! style="background:#ccc;"| Category
! style="background:#ccc;"| Recipient(s)
! style="background:#ccc;"| Outcome
|-
| Tribeca Film Festival 
| Best Narrative Feature
| Laurie Collyer
|  
|}

==References==
 

==External links==
*   
*  

 
 
 
 
 
 


 