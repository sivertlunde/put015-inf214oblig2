The Far Call
{{infobox film
| name           = The Far Call
| image          = The_Far_Call_1929_Poster.jpg
| caption        = Theatrical release poster
| director       = Allan Dwan William Fox
| writer         = {{Plainlist|
* Seton I. Miller
* H.H. Caldwell (intertitles)
}}
| story          = Edison Marshall
| starring       = {{Plainlist| Charles Morton
* Leila Hyams
}}
| music          =
| cinematography = Harold Rosson
| editing        = H.H. Caldwell
| distributor    = Fox Film Corporation
| released       =  
| runtime        = 59 minutes (6 reel#Motion picture terminology|reels)
| country        = United States
| language       = English
}} Charles Morton and Leila Hyams. Produced and distributed by the Fox Film Corporation. It is a late silent film with Foxs Movietone sound on film system containing music and sound effects.   

==Cast== Charles Morton as Pat Loring
* Leila Hyams as Hilda Larsen
* Warner Baxter Arthur Stone as Schmidt
* Warren Hymer as Soup Brophy
* Dan Wolheim as Black ONeil
* Tiny Sandford as Captain Storkerson (*as Stanley J. Sandford) Ullrich Haupt as London Nick
* Charles B. Middleton as Kris Larsen Pat Hartigan as Lars Johannson Charles Gorman as Haycox
* Ivan Linow as Red Dunkirk
* Harry Gripp as Pete
* Sam Baker as Tubal
* Bernard Siegel as Aleut Chief

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 


 