The Smashing Bird I Used to Know
{{Infobox film
| name           = The Smashing Bird I Used to Know
| image          = Schoolunclaimed.jpg
| image_size     =
| caption        = U.S. publicity poster
| director       = Robert Hartford-Davis
| producer       = Peter Newbrook
| writer         = John Peacock
| starring       = Madeleine Hinde Maureen Lipman Patrick Mower Dennis Waterman
| music          = Bobby Richards
| cinematography = Peter Newbrook
| editing        = Don Deacon
| studio         = Titan International
| released       = 25 August 1969
| runtime        = 95 minutes
| country        = United Kingdom English
}}
 1969 British drama/sexploitation film, directed by Robert Hartford-Davis and starring Madeleine Hinde, Maureen Lipman, Patrick Mower and Dennis Waterman.  As with other Hartford-Davis films, The Smashing Bird I Used to Know contains elements from different genres including psychological drama and social commentary.  It is best known however as a sexploitation piece featuring nudity, attempted rape and lesbianism.  The film features the first screen credit of the then 15 year-old Lesley-Anne Down in a supporting role.

The film was not released in the U.S. until 1973, retitled School for Unclaimed Girls.  More recent issues of the film in the UK have also used this title as being less dated and more indicative of the films content.

==Plot==
Nine-year-old Nicki Johnson attends a funfair with her parents.  Her father takes her on a merry-go-round ride, where Nicki becomes frightened.  Attempting to reach over to comfort her, her father instead falls from the ride and is crushed to death in its machinery.  The tragedy leaves Nicki traumatised, particularly as in its aftermath she overhears comments suggesting that she was to blame for what happened, which leave her with a permanent sense of guilt.

Seven years on and Nicki (Hinde) is a troubled and confused teenager living with her mother, plagued by flashback nightmares and with an obsession with horses and riding stemming from the merry-go-round horror.  Since being widowed, her mother Anne (Renée Asherson) has withdrawn emotionally from her daughter and has sought consolation with a succession of younger lovers.  Her latest boyfriend Harry (Mower), who Nicki detests, is a sleazy con-artist who makes his living out of latching on to wealthy older women and fleecing them financially before moving on.  Nicki is left largely to her own devices and often plays truant from school, spending the time with her boyfriend Peter (Waterman).

Returning home one day from a riding lesson, Nicki finds herself alone in the house with Harry.  He attempts to seduce her, and when she proves resistant, taunts her with the fact that her trust fund is now in his control.  A struggle ensues, during which Nicki stabs him several times leaving him seriously injured.  For her trouble, she is sent to a remand home for young women with emotional and behavioural problems.

Coming from a middle-class background, Nicki is overwhelmed by her new environment among a large group of tough, delinquent and maladjusted girls, where bullying and violence is the norm.  She tries to keep a low profile to avoid being victimised, but matters improve when she strikes up an unlikely friendship with lesbian fellow inmate Sarah (Lipman).  Despite Sarahs fearsome reputation as one of the toughest girls on the block, she becomes Nickis unofficial protector.  As the friendship develops Sarah reveals her more vulnerable side to Nicki and they discover that they have much in common with regard to how they ended up where they are.  Sarah makes it clear that her feelings towards Nicki go beyond friendship and a tentative intimacy develops between the pair.

Sarah and Nicki finally make the decision to abscond together rather than face the prospect of being sent to borstal.  Soon after, Sarah is apprehended but Nicki avoids capture and manages to make it to Peters flat.  The sensible Peter tries to convince her that she has done herself no favours by running away from her problems and that in the long-term it is better that she should face up to them by returning to the remand home.  Nicki is initially unconvinced, but finally realises that he is right.  She agrees to being driven back; however fate intervenes before they complete the journey.

==Cast==
* Madeleine Hinde as Nicki Johnson
* Renée Asherson as Anne Johnson
* Patrick Mower as Harry Spenton
* Dennis Waterman as Peter
* Faith Brook as Dr. Sands
* Maureen Lipman as Sarah
* Janina Faye as Susan David Lodge as Richard Johnson
* Derek Fowlds as Geoffrey
* Megs Jenkins as Matron
* Colette ONeil as Miss Waldron
* Cleo Sylvestre as Carlien
* Lesley-Anne Down as Diana
* Sheila Steafel as Young Woman
* Valerie Van Ost as Amanda

==References==
 
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 