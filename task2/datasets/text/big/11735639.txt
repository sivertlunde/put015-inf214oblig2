The Pardon
 
{{Infobox film
| name           = The Pardon
| image          = 
| caption        = 
| director       = Tom Anton
| producer       = Tom Anton Blair Daily Jacqueline George Sandi Russell
| writer         = Tom Anton Sandi Russell
| starring       = Jaime King Jason Lewis (actor)
| music          = 
| cinematography = Matthew Irving
| editing        = 
| studio         = 
| distributor    = Monterey Media  
| released       = 
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 	 
}} John Hawkes as "Arkie" Burke, Henrys partner in the crime. 

The picture is based on an actual case tried in Louisiana in the 1940s.

==Plot==
Murder. With all the picturesque glamour of the 1940s, The Pardon recounts the unlikely true story of Toni Jo Henry (Jaime King), a woman tried three times and executed in Louisiana, 1942.

Surviving a legacy of childhood abuse, which lands her in the art deco brothels of the time, Toni Jo briefly discovers love and happiness when she marries the dashing boxer Cowboy Henry (Jason Lewis (actor)). Cowboy is soon after sent to prison, leaving the bereft Toni Jo to embark on an ill-fated mission with Cowboy’s sometime partner Arkie (John Hawkes). A grisly murder and a series of sensational trials where she pleads her innocence instantly makes the beautiful Toni Jo into a celebrity. Facing conviction after conviction, will she find true redemption in the face of the crimes for which she is accused?
 
==Cast==
* Jaime King as Toni Jo Henry 
* Jason Lewis (actor) as Cowboy 
* M.C. Gainey as Gibbs Duhon 
* Leigh Whannell  as Clement Moss  John Hawkes as Finnon Arkie Burke 
* T.J. Thyne as Father Richard 
* Tim Guinee as Norman Anderson  
* Niki Spiridakos as Niki 
* Brad Dison as a Reporter 
* Stuart Greer as George McQuiston 
* Ed Bruce as J.P. Copeland 
* Kip Cummings as Deputy Sheriff
* Nancy Ellen Mills as Mrs. Calloway
* Celeste Roberts as Aunt Emma
* Jeddah Danielle Salera as Young Toni Jo Henry
* Jim Dickson as prison guard

==External links==
*  
*  

 
 
 
 
 
 
 
 
   
 