Body (2007 film)
{{Infobox film
| name           = Body
| image          = Body_film_poster.jpg
| caption        = The Thai theatrical poster.
| director       = Paween Purijitpanya
| producer       = Jira Maligool Yongyoot Thongkongtoon
| writer         = Chukiat Sakweerakul
| narrator       = 
| starring       = Arak Amornsupasiri Ornjira Lamwilai Kritteera Inpornwijit Patharawarin Timkul
| music          = 
| cinematography = 
| editing        =  GTH
| released       = List of cinemas in Thailand|Thailand: October 4, 2007
| runtime        =  Thailand
| language       = 
| budget         = 
| preceded_by    = 
| followed_by    = 
}} Thai horror film|horror-thriller film. It is produced by GMM Grammy#Grammy Tai Hub|GTH, the same production company that made the hit Thai horror film, Shutter (2004 film)|Shutter. Body is directed by Paween Purijitpanya and co-written by Chukiat Sakweerakul, who had previously directed the thriller, 13 Beloved.

Body has parallels to an actual murder case in Thailand, in which a physician was convicted and given the death penalty in the dismemberment of his estranged wife.

Among the cast is Arak Amornsupasiri, who plays the main protagonist Chon. Arak is the guitarist in the Thai rock band, Slur.

==Plot==
Chon is an engineering student living in a rented house in Bangkok with his sister, Aye. Chon is having bad dreams, in which he is visited by a ghostly woman who appears to have been dismembered and put back together. He also sees a misshapen black cat and occasionally sees a fetus. Chon sometimes finds himself in places, such as a musical performance, and cannot remember how he got there.

After Chon slices his finger open while cleaning some prawns for dinner (the prawns had started moving around and bleeding profusely), Chons medical student sister Aye takes him to the hospital.

Chon is eventually referred to a Dr. Usa for psychiatric treatment. It becomes apparent to Dr. Usa that there is some connection between Chon, herself and her increasingly distant husband, Dr. Sethee.

In the course of Usas investigation, she discovers there is a connection between her husband and a mysterious university lecturer, Dr. Dararai, who possesses supernatural powers of hypnosis.

Meanwhile, Chon continues to have bad dreams, and they are becoming increasingly horrifying and real. He is repeatedly drawn to a spare room in his house, and when he opens the door, he sees a man chopping up a body. When the man turns his head to look at Chon, the mans face is Chons. It turns out to be another dream and after discovering that Dararai has gone missing he realises that Dararai is dead and communicating with him, Dararai asks him to find her.

A teaching assistant whom Usa questioned about Dararai ends up being killed in a gruesome accident involving barbed wire around a university museum exhibit. A young doctor also meets his end in a vat of acid after talking to Chon about Dararai.

Chon comes to the conclusion that whoever tells of Dararais disappearance is killed and tries to stop the killings, but is always too late.

Back at their rented house Chon is being tormented by Dararai again who constantly says "find me" and now discovers a secret room in the house (the one in his dreams) which he suspected earlier behind a cupboard and discovers burnt photos of Dararai and Sethee in what seems to be an intimate relationship. It now emerges that Sethee had an affair with Dararai and that Dararai took photos of themselves in bed with each other. Dararai wanted to blackmail Sethee with the photos or tell Usa and as a result Sethee met up with Dararai, drugged her drink, injected her with a paralysing liquid and chopped her to pieces (in the hidden room that Chon finds).

Chon is about to tell Usa that her husband has killed Dararai but while at her house he is again attacked by Dararais ghost who says she will kill his sister, enraged Chon stabs her with a pole in front of Usa and Sethees daughter May. After seeing May (whos scared and terrified) he leaves to find his sister.

These events between Usa, Chon, Sethee and Dararai(and her ghost) lead to the university hospitals morgue, and body number 19. Chon goes to the morgue and discovers that the body in the morgue, which is in drawer 19, is actually Chons body. Chon has, in fact, died a couple of years ago and Sethee who has a multiple personality disorder believes that he is Chon when in reality he has killed the teaching assistant, the doctor and has stabbed his own wife Usa who he believed was Dararais ghost when he was believing he was Chon.

He is arrested but because of his mental health issues his trial is put on hold. A flashback occurs back to the scene when he is about to drug Dararais drink. It is revealed that Dararai was aware that Sethee has drugged her drink hypnotises him and gives him a multiple personality disorder.

The flashback finishes and he is being escorted to prison in a van with his Lawyers in the van(to protect his safety). He escapes out of the van and runs from the police and lawyers, He runs in front of another van that is carrying metal poles on top. The van stops in time and doesnt run over Sethee but the metal poles are flung off the van and they all hit and impale Sethee. He is left on the road with lots of metal poles pieced inside him.

The last scene shows Sethee in hospital being operated on, he is unconscious but suddenly Dararais ghost appears and it turns out her ghost wasnt part of Sethees mental mind but was real and was manipulating Sethee. She finally reveals that she forgives him and after clicking her fingers his mind goes back to normal but this makes Sethee conscious again and wakes him up halfway through his operation leaving him screaming in pain as Dararai disappears and the ending credits show.

==Cast==
*Arak Amornsupasiri as Chon
*Ornjira Lamwilai as Aye
*Kritteera Inpornwijit as Usa
*Patharawarin Timkul as Dararai

==Parallels==
The film production companys official synopsis states: A human being contains 5 liters of blood, 6 pounds of skin, 206 bones, 600 muscles, and 35 million glands. It takes a human body more than 25 years of life to grow such things.

But one man actually believes he can rid himself of every single piece of human flesh by just using straight scissors and a small surgical blade. And he is going to prove it. }}

The story in Body has parallels to an actual crime case in Thailand, about Dr. Wisut Boonkasemsanti, a gynaecologist who also taught in the Faculty of Medicine at Chulalongkorn University. He was convicted and sentenced to the death penalty for the 2001 death of his wife, Phassaporn, who was also a gynaecologist. Wisut was found guilty of dismembering his wife and flushing her remains down toilets.  

==Soundtrack==
The song being performed during the musical performance that Chon attends is "Kid tung teo took tee ti yoo kon deaw" ("I Miss You Every Time I Am Alone") by Pat Suthasini Puttinan. 

==References==
 

==External links==
* 
*  

 
 
 
 
 
 
 
 