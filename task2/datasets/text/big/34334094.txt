Rappadikalude Gatha
{{Infobox film
| name           = Rappadikalude Gatha
| image          = 
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = K. G. George
| producer       = Pleasant Pictures
| writer         = Padmarajan
| screenplay     = 
| story          = 
| based on       =  
| narrator       =  Soman
| music          = Devarajan
| cinematography = B. Kannan
| editing        = M. N. Appu
| studio         = Pleasant Pictures
| distributor    = Angel Films
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}}
Rappadikalude Gatha ( ) is a 1978 Malayalam film written by Padmarajan, directed by K. G. George and starring Vidhubala in the lead role.

==Plot==
Rappadikalude Gatha is woven around Gatha, a young girl who finds happiness in drugs and music. She marries a doctor of humble indisposition. Their marriage runs into difficulty but, after several traumatic experiences, the couple are reconciled. The film is a sharp indictment of society, its awry priorities, on the frustrations of the youth, their disappointments, and how youngsters get hooked to drugs. On the other side, the film is also on the conflicts and complexities of marital life.

==Cast==
*MG Soman
*Vidhubala
*Sukumari Jose
*Krishnachandran
*Aranmula Ponnamma
*Santhakumari
*Sharmila
* Prem Prakash

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Yusufali Kechery.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Layam Layam || K. J. Yesudas || Yusufali Kechery ||
|-
| 2 || Snehaardra || P. Madhuri || Yusufali Kechery ||
|}

==Production==
It was Padmarajan who advised the producers to ask K. G. George to do this film. The shoot took about two weeks and the film was completed in around 90 days. The whole film was shot in and around Ernakulam and Fort Kochi.   

The screenplay written by Padmarajan was believed to be lost, till it was unearthed in 2006. Twelve 35mm prints were made and all the prints have been lost forever. 

==Reception==
The film opened to universal acclaim with numerous praises to George, Padmarajan and Vidhubala. Despite all the acclaim, the film failed at the box office. 

==References==
 

==External links== British Film Institute Film & TV Database
*  
*   at the Malayalam Movie Database

 

 
 
 

 