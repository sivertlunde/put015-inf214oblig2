Swing High, Swing Low (film)
{{Infobox film
| name           = Swing High, Swing Low
| image          = Swinghigh poster.jpg
| image_size     =
| caption        = Theatrical poster
| director       = Mitchell Leisen
| producer       = Arthur Hornblow, Jr.
| writer         = Virginia Van Upp Oscar Hammerstein II
| starring       = Carole Lombard Fred MacMurray
| music          = Phil Boutelje Victor Young
| cinematography = Ted Tetzlaff
| editing        = Eda Warren
| distributor    = Paramount Pictures
| released       =  
| runtime        = 92 min.
| country        = United States Spanish
| budget         = $739,600
| gross          =
}} 1937 United American romance romantic drama starring Carole Lombard and Fred MacMurray.   
 When My Baby Smiles at Me (1948), of the popular Broadway play Burlesque (play)|Burlesque, by George Manker Watters and Arthur Hopkins.

==Plot==
Working her way on board a liner travelling through the Panama Canal Zone as a hairdresser, Maggie meets a brash young soldier, "Skid" Johnson who is leaving the army the next day.  When her inattention to a client causes the clients hair to catch on fire, Maggie is fired and has to disembark in Panama.  She and Skid eventually fall in love and marry.  She discovers Skids amazing prowess with a trumpet that leads him to support her after their marriage.  Greedy for money, she encourages the reluctant Skid to travel to New York City to play in a major night club leaving her behind.

==Cast==
* Carole Lombard ... Marguerite Maggie King
* Fred MacMurray ... Skid Johnson Charles Butterworth ... Harry
* Jean Dixon... Ella
* Dorothy Lamour ... Anita Alvarez
* Harvey Stephens ... Harvey Howell
* Cecil Cunningham ... Murphy
* Charles Arnt ... Georgie Herman
* Franklin Pangborn ... Henri
* Anthony Quinn ... The Don
* Charles Judels ... Tony Morelli

==Releases== public domain (in the USA) due to the claimants failure to renew its copyright registration in the 28th year after publication. 
*  

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 

 