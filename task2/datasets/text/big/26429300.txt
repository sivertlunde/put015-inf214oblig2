Semum
{{Infobox film
| name           = Semum
| image          = SemumFilmPoster.jpg
| image size     =
| alt            = 
| caption        = Theatrical poster
| director       = Mubariz Hajimuradov
| producer       = Mubariz Hajimuradov
| writer         = Mubariz Hajimuradov
| narrator       = 
| starring       = Bigkem Karavus  Burak Özçivit  Kurtulus Sakiragaoglu
| music          = Justin R. Durban
| cinematography = Seyhan Bilir
| editing        = 
| studio         = J-Plan
| distributor    = 
| released       =  
| runtime        = 116 min  
| country        = Turkey
| language       = Turkish
| budget         = $1,200,000 (estimated)
| gross          = $1,827,047
| preceded by    =
| followed by    = 
}}

Semum is a 2008 Turkish horror film produced, written and directed by Hasan Karacadağ which reportedly recounts the true story of a woman who lives in İzmir, Turkey.   

== Plot ==
Twenty-seven-year-old Canan (Ayça İnci) and her husband Volkan (Burak Hakkı) have just moved into a large, new house. Life seems to continue on its routine track in the young couples new house until one day, when Canan starts feeling that strange things are happening to her although she cannot understand what or why. Canan gradually starts turning into an evil creature as a mysterious and malicious being takes control of her body and actions day after day. Semum, the most loyal servant of the devil, has taken control of Canan, leading her toward hell.

== Cast ==
* Ayça İnci as Canan Karaca 
* Burak Hakkı as Volkan Karaca
* Cem Kurtoğlu as Mikail Hoca
* Sefa Zengin as Raci
* Bahtiyar Engin as Macit
* Nazlı Ceren Argon as Banu
* Yıldırım Öcek as Emlakçı 
* Hakan Meriçliler as Profesör Oğuz
* Levent Sülün as Ali
* Süha Tok

==Production== The Exorcist and star Ayça İnci will be compared with Linda Blair, the director told Todays Zaman, noting that the Turkish horror films made so far have been far from satisfactory. 

===Filming===
The film was shot on location in Istanbul, Turkey.   

== Release ==
The film opened in 142 screens across Turkey on   at number 1 in the box office chart with an opening weekend gross of $441,665.   

==Reception==

===Box office=== Turkish film of 2008 with a total gross of $1,827,047. 

=== Reviews === Constantine in the demon and hell effects, he adds.   

AnthroFred, writing for Slasherpool, wrote, Forgettable Exorcist rip-off which didnt do much for me. Its an okay movie but its a bit too long and a bit too unoriginal for my taste. Its refreshing to see something different from Turkey though but I just wish that they would stop recycling others scripts and come up with some ideas of their own instead. Worth a watch if youre a fan of The Exorcist.     

== See also ==
* 2008 in film
* Turkish films of 2008

==External links==
*   for the film
*  
*  

==References==
 

 
 
 
 
 
 