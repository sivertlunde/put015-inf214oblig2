Kool...Sakkath Hot Maga
{{Infobox film
| name = Kool
| image = 
| caption =  Ganesh
| producer = Shilpa Ganesh Ganesh
| Ganesh Sana Khan
| music = V. Harikrishna 
| cinematography = Rathnavelu
| editing = Saad Khan
| distributor = Golden Movies 
| released =  
| runtime = 
| country = India
| language = Kannada
| budget = 
| gross = 
}} romance genre Ganesh and Sana Khan in the lead roles . The film marks the directorial debut of Ganesh. His wife, Shilpa Ganesh produces this movie under the home banner Golden Movies. V. Harikrishna has composed the music and a long associate, Ratnavelu works as the cameraman. 

==Plot==
The film has supposedly a comedy and romance mixed storyline. Ganesh plays a college going student in the film. The film has been shot in some picturesque locations such as Egypt, Dubai and Jordan and Middle East. 

==Cast== Ganesh
* Sana Khan- Making her debut in a Kannada movie
* Saad Khan
* Rangayana Raghu
*Deepa Shetty
* Sharan
* Dattanna
*Sangeetha Shetty

==Box Office==

The movie was an average grosser at box office by completing 25 days.

==Soundtrack==
{|class="wikitable" width="70%"
! Song Title !! Singers !! Lyricist
|-
| "Inky Pinky" || Naveen Madhav || Kaviraj
|- Ganesh || Kaviraj
|-
| "Nodutha Nodutha" || Sonu Nigam || Kaviraj
|-
| "Neenu Ninthare" || Shaan (singer)|Shaan, Anuradha Bhat || Kaviraj
|- Ranjith || Kaviraj
|-
| "Udayavani" || V. Harikrishna || Kaviraj
|-
|}

==Home media==
The movie was released on DVD with 5.1 channel surround sound and English subtitles and VCD.

==Reception==
The film was both a critical and commercial failure.
It is been appreciated on TV a lot for family audiences.

==References==
 

==External links==
*  

 
 
 
 


 