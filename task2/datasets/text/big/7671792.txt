Bring Me the Head of Charlie Brown
 
{{Infobox film
| name           = Bring Me the Head of Charlie Brown
| image          = 
| alt            =  
| caption        = 
| director       = Jim Reardon
| producer       = 
| writer         = Jim Reardon
| based on       =  
| narrator       = Rich Moore
| starring       = Etienne Badillo Rich Moore Mike Reardon William Holden Bret Haaland Ethan Kafner Jeff Pidgeon
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 4 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} animated short directed and animated by Jim Reardon, who would later become director and storyboard consultant for The Simpsons. The cartoon was made in 1986 while he was at CalArts. It has a rough draft-like, unfinished black-and-white look to it, with no real color other than the necessary black outlines.

== Synopsis == Madison Barns, pooftas and wussy cakes".
 bounty on Lucy tries Schroeder dumps gushes blood), Linus strangles him with his blanket (which he had turned into a makeshift Garroting Wire).
 decapitated by gunfire. Following this is a montage of a small dragon breathing flame, Pig Pen vomiting profusely in Violets face, two biplanes colliding in midair, Dagwood Bumstead getting kicked in the groin by his wife, Blondie (comic strip)|Blondie, which causes his head to pop off, resulting in another blood gush, Mickey Mouse getting hit over the head by a lead pipe, Rocky Balboa getting punched in the face by Popeye, and Godzilla squeezing Dr Pepper out of a giant soda can. It ends with Charlie Brown announcing that "Happiness is a warm uzi" in a thick Germanic accent reminiscent of Arnold Schwarzenegger and a cut to him smoking a cigarette in bed with the Little Red-Haired Girl (who, fittingly, is not fully seen), who asks Charlie Brown to turn off the bedroom light. 
 Charlie Brown" by The Coasters plays over the ending credits (where it is incorrectly attributed to The Platters). The credits end with a note from Jim Reardon:

 "The creator of this picture wishes to state that he does not in any way wish to tarnish or demean the beloved characters of Charles M. "Dutch" Schultzs comic strip, "Peanuts". No malice or damage to their goodwill was intended. So please dont sue me, because it will drag through the courts for years, and I havent got a lawyer - and besides, youve already got half the money in the world, and I havent got any. OK?" 

== References to other media == National Lampoons parody of TV Guide.
 
The Peanuts massacre is a major satire of the climax in Sam Peckinpahs classic film The Wild Bunch. There are slow-motion death scenes intercut with rapid shots, much like Peckinpahs editing style. Violets death scene, in which she spins around with her revolver, is a copy of Herreras death scene at the start of the gun battle. The sequence in which Lucy shoots at Charlie Brown from behind and he spins around screaming and kills her with a shotgun is word-for-word, shot-for-shot taken from the sequence where the prostitute shoots William Holden in the back. There is even a part where Charlie Brown waves his submachine around, screaming the famous Warren Oates scream, and the camera pan across several Mexican bandits being blown away. An interesting note is that Reardon actually uses sound bytes from the movie in these two previous scenes.

The references to Peckinpah are made even more clear at the end of the film when Reardon dedicates it to Sam "The Man" Peckinpah.

There are some non-Peckinpah references made in the short, such as Charlie Browns mohawk, a reference to Travis Bickle in Taxi Driver and Lucy speaking in a John Wayne impression. Reardon also identifies Peanuts creator Charles M. Schulz as "Charles M. Dutch Schultz", as in mobster Dutch Schultz. 

Godzilla squeezing the giant Dr. Pepper can is a reference to the then-recent Dr. Pepper ad campaign featuring the famed monster.
 Peter Robbinss similar-sounding yell that was used in most Peanuts specials to that point). 
 Farewell to John Denver".

The brief image of a small fire-breathing dragon is from Snookles, an animated short by Juliet Stroud, which, like this film, was produced at the California Institute of the Arts in 1986.

== Cast and credits ==
*Charlie Brown - Etienne Badillo, Rich Moore, Mike Reardon, William Holden
*Linus van Pelt - Ethan Kafner
*Lucy van Pelt - Bret Haaland
*Great Pumpkin - Jeff Pidgeon
*Additional Voices - Ed Bell, Bruce Johnson, Mike Reardon, Bret Haaland
*Narration - Rich Moore
*"Aided and Abetted by" - Ed Bell, Dale Mcbeath, Bob Winquist, Mike Giaimo, Craig Smith, Bret Haaland, Nate Kanfer, Doug Frankel, Mike Reardon, Rich Moore, Russ Edmonds, Hal Ambro, Dan Hansen, Jim Ryan, Tony Fucile, Jeff Pidgeon, Bob McCrea, Sarge Morton, Mom, Eileen, and Beverly

==References==
 

== External links ==
* 
*  at YouTube

 

 
 
 
 

 