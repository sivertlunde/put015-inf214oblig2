The Heroes (1980 film)
 
 
{{Infobox film
| name           = The Heroes
| image          = TheHeroes.jpg
| alt            =
| caption        = Film poster
| film name      = {{Film name
 | traditional    = 俠骨英雄傳
 | simplified     = 侠骨英雄传
 | pinyin         = Xiá Gǔ Yīng Xióng Chuán
 | jyutping       = Hap6 Gwat1 Jing1 Hung4 Zyun2 }}
| director       = Wu Ma Pao Hsueh Li
| producer       = Ko Fei Katy Chin Chen Tian Ching
| writer         = Ni Kuang Katy Chin Danny Lee Michael Chan Dorian Tan
| narrator       =
| music          = Joseph Koo Wong Mau San
| cinematography = James Wu
| editing        =
| studio         =
| distributor    = Ocean Shores
| released       =  
| runtime        = 90 minutes
| country        = Hong Kong
| language       = Mandarin
| budget         =
| gross          =
}}
 Danny Lee.

==Plot== Michael Chan). At that time, Ko was also killed and his heroic deed was not known even after he died. Ko is considered a true hero who can tolerate the most insult and humiliation.

==Cast==
* Ti Lung as Ko Fei / Wong Fei
* Shih Szu as Princess Danny Lee as Righteous Monk Michael Chan as Qing Emperor
* Dorian Tan as Si Ying
* Wong Chung as Fong Gau
* Wong Ching as Gap toothed official
* Goo Chang as Bald official
* Wu Ma as Ng Ging
* Tsai Hung
* Lee Ho
* Joh Yau
* Chan Pik Fung
* Sit Hon as Abbot
* Wong Yeuk ping
* Ma King Shun
* Cho Boon Feng
* Lau Yau Bun
* Wong Kwok Fai
* Robert Tai
* Ko Chang Sheng

==Theme song==
* Hero (英雄)
** Composer: Joseph Koo
** Lyricist: Chang Cheh
** Singer: Jenny Tseng

==Reception==
J. Doyle Wallis of DVD Talk gave the film 4.5 out of 5 stars and a positive review praising it for having "one of the better plots in kung fu filmdom" as well as the action choreography and pacing. 

==References==
 

==External links==
*  
*   at Hong Kong Cinemagic
*  

 
 
 
 
 
 
 
 
 
 
 
 
 