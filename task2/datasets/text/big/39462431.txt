American Sniper (film)
 
{{Infobox film
| name           = American Sniper
| image          = American Sniper poster.jpg
| alt            = Chris Kyle is seen wearing desert fatigues army BDU, while his wife Taya embraces him. They are standing in front of a tattered US flag. 
| caption        = Theatrical release poster
| director       = Clint Eastwood
| producer       = {{Plain list |
* Clint Eastwood
* Robert Lorenz
* Andrew Lazar
* Bradley Cooper Peter Morgan}} Jason Hall
| based on       =  
| starring       = {{Plain list |
* Bradley Cooper
* Sienna Miller}}
| music          =  Tom Stern
| editing        = {{Plain list |
* Joel Cox
* Gary D. Roach}}
| studio         = {{Plain list |
* Village Roadshow Pictures 
* Mad Chance Productions
* 22nd & Indiana Pictures 
* Malpaso Productions}}
| distributor    = Warner Bros. Pictures
| released       =  
| runtime        = 132 minutes    
| country        = United States
| language       = English
| budget         = $58.8 million      
| gross          = $542.3 million    
}}
 Department of Defense. The film stars Bradley Cooper as Kyle and Sienna Miller as his wife Taya Kyle|Taya, with Luke Grimes, Kyle Gallner, Sam Jaeger, Jake McDorman, and Cory Hardrict in supporting roles.
 American Film Institute Festival, followed by a limited theatrical release in the United States on December 25, 2014 and a wide release on January 16, 2015. The film became a major commercial success, with a worldwide gross of over $542 million,  making it the highest-grossing film of 2014 in the United States, the highest-grossing war film of all time unadjusted for inflation, and Eastwoods highest-grossing film to date.
 War in Best Picture, Best Adapted Best Actor Best Sound Editing. 

==Plot==
  1998 U.S. embassy bombings and decides to enlist in the U.S. Navy, where he is eventually accepted for SEAL training, becoming a United States Navy SEALs|U.S. Navy SEAL sniper.

Chris meets Taya Renae at a bar, they marry, and he is sent to Iraq after the September 11 attacks of 2001. His first kills are a woman and boy who attacked U.S. Marines with a grenade. Chris is visibly upset by the experience but earns the nickname "Legend" for his many kills. He is assigned to hunt for the al-Qaeda leader Abu Musab al-Zarqawi; during house-to-house searches in evacuated areas, Chris interrogates a family, and for $100,000 the father offers to lead the SEALs to "The Butcher", al-Zarqawis second-in-command whose favorite torture device is a drill. The plan goes awry when The Butcher captures the father and son, and kills them while Chris is pinned down by a sniper using an Dragunov sniper rifle|SVD. Meanwhile, the insurgents issue a bounty on Chris.

Chris returns home to his wife and the birth of his son. He is distracted by memories of his war experiences and argues with Taya over bootleg footage of U.S. Marines shot dead by enemy sniper "savages". Taya expresses her concern for them as a couple and wishes Chris would focus on his home and family.

Chris leaves for a second tour, promoted to Chief Petty Officer. He is involved in a shoot out with The Butcher, who is located operating out of a ground floor restaurant.

Chris returns home from his second tour to a newborn daughter, and he becomes increasingly distant from his family. On his third tour, the Dragunov sniper seriously injures a unit member, and the unit is evacuated back to base. The unit decides to return to the field and continue the mission. Another SEAL is killed by gunfire, compelling Chris with guilt and duty to undertake a fourth tour. Taya does not understand his decision, tells him she needs him, and for a moment, implies they should stay apart.
 the 8th longest sniper kill ever recorded), but this exposes his teams position to a large number of armed insurgents. In the midst of the firefight and low on ammunition, Chris calls Taya and tells her he is ready to come home. A sandstorm provides cover for a chaotic escape in which Chris is injured and almost left behind.
 Veterans Affairs psychiatrist he is "haunted by all the guys   couldnt save". The psychiatrist encourages him to help wounded veterans in the VA hospital. Chris meets veterans who suffered severe injuries, coaches them at a shooting range in the woods, and gradually begins to adjust to home life.
 Cowboys Stadium .

==Cast==
 
* Bradley Cooper as Chris Kyle    Taya Renae Kyle   
* Max Charles as Colton Kyle  Marc Lee   
* Kyle Gallner as Goat-Winston   
* Sam Jaeger as Captain Martens   
* Jake McDorman as Ryan "Biggles" Job   
* Cory Hardrict as D / Dandridge   
* Navid Negahban as Sheikh Al-Obodi   
* Eric Close as DIA Agent Snead * 
* Eric Ladin as Squirrel 
* Rey Gallegos as Tony 
* Kevin "Dauber" Lacz as himself   
* Brian Hallisay as Captain Gillespie
* Ben Reed as Wayne Kyle
* Elise Robertson as Debby Kyle
* Keir ODonnell as Jeff Kyle
* Marnette Patterson as Sarah
* Leonard Roberts as Instructor Roll
* Sammy Sheik as Mustafa, a character partially based on Iraqi sniper Juba (sniper)|Juba.      The Butcher" 
 
*Fahim Fazli Messianic Tribal Leader

==Production==
 
On May 24, 2012, it was announced that Warner Bros. had acquired the rights to the book with Bradley Cooper set to produce and star in the screen adaptation.  Cooper had thought of Chris Pratt to play Kyle, but WB agreed to buy it only if Cooper would star.  On September 2012, David O. Russell said he was interested in directing the film.  On May 2, 2013, it was announced that Steven Spielberg would direct.  Spielberg had read Kyles book, though he desired to have a more psychological conflict present in the screenplay so an "enemy sniper" character can serve as the insurgent sharpshooter who was trying to track down and kill Kyle. Spielbergs ideas contributed to the development of a lengthy screenplay approaching 160 pages. Due to Warner Bros. budget constraints, Spielberg felt he could not bring his vision of the story to the screen.    On August 5, 2013, Spielberg dropped out of directing.  On August 21, 2013, it was reported that Clint Eastwood would instead direct the film. 

===Casting=== Delta sniper.    On June 3, Max Charles was added to the cast to portray Kyles son, Colton Kyle.   

===Filming=== Santa Clarita El Centro; date factory Culver City, Marina del Rey.  On June 3, Cooper was spotted in the uniform of a Navy SEAL marksman aiming during the filming of some scenes at a Los Angeles shooting range.  The pier and bar scenes were filmed in Seal Beach, California. 
Bradley Cooper gained 40 pounds for his role.
 Tom Stern Arri Alexa digital cameras anamorphic lenses. Jersey Boys.   

===Music=== scores for Mystic River Someone Like You by Van Morrison, which plays during the wedding scene. 
 

==Historical accuracy==
The film takes several dramatic liberties with the story; The Guardian wrote that "this film alters Kyle’s book significantly".  The child that Kyle is forced to shoot in his first engagement is not found in the memoir. The characters of Mustafa and the Butcher were created for the film, and Kyles real-life 2100 yard shot was taken to kill an insurgent holding a rocket launcher. 

==Reception==

===Box office===
 
As of May 3, 2015, American Sniper had grossed $348.8 million in North America and $193.4 million in other territories for a worldwide total of $542.2 million, against a budget of around $58 million.  Calculating in all expenses and revenues,  . {{cite web|url=http://deadline.com/2015/03/american-sniper-profit-box-office-2014-1201391240/|title=No. 2 ‘American Sniper’ – 2014 Most Valuable Blockbuster Movie Tournament
|author=Mike Fleming Jr|publisher=Deadline.com|date=March 13, 2015|accessdate=March 14, 2015}}  Worldwide, it is the highest-grossing war film of all time (breaking Saving Private Ryan  record)  and Eastwoods highest-grossing film to date. It is the seventh R-rated film to gross over $500 million. 

==== North America ==== The Grinch (2000 in film|2000) to top the year-end rankings. 
 premiered at the AFI Fest on November 11, 2014, just after a screening of Selma (film)|Selma at Graumans Egyptian Theatre in Los Angeles.  In North America, the film opened to a limited release on December 25, 2014, playing at four theaters — two in New York, one in Los Angeles, and one in Dallas — and earned $610,000 in its opening weekend ($850,000 including Christmas Day) at an average of $152,500 per venue debuting at #22.   The following week the film earned $676,909 playing at the same number of locations at an average of $169,277 per theater, which is the second-biggest weekend average ever for a live-action movie (previously held by 2001s Moulin Rouge!).  American Sniper holds the record for the most entries in the top 20 Top Weekend Theater Averages with 3 entries. It earned a total of $3.4 million from limited release in three weekends. 
 Martin Luther King weekend setting a record for the biggest R-rated four day gross. 

In its   ($334 million), judging from its gradual decline and strong holdovers.  It became the highest-grossing IMAX film of January grossing $18.8 million from 333 IMAX theaters.  On Thursday, January 29, 2015 – 35 days after its initial release, the film surpassed Saving Private Ryan ($216.5 million) to become the highest-grossing war film in North America, unadjusted for inflation. 

By its third weekend of wide release, the film expanded to 3,885 theaters (180 additional theaters added), breaking its own record of being the widest R-rated film ever released.   The film topped the box office through its third weekend earning $30.66 million which is the second highest  ).  After topping the box office for three consecutive weekends (the longest of 2015 so far), the film was overtaken by   in its fourth weekend.   

==== Other territories ====
The film had the biggest debut weekend for a Clint Eastwood movie ever and went on to become the directors top grossing movie of all time in each of the countries it was released in.  It opened in Italy at No. 2 with $7.1 million which is Eastwood’s best opening of all time and Warner Bros second biggest opening for a non-franchise U.S. film there  and topped the box office the following weekend.  France with $6.3 million,    where it topped the box office for four consecutive weekends,  Australia with $4.3 million ($4.6 million including previews),  the UK, Ireland and Malta with $3.8 million,    Spain with $3.2 million,  Japan with $2.8 million,  Mexico with $2.6 million,  Brazil with $1.8 million,  and Korea with $1.2 million. 

===Critical response===
American Sniper has received positive reviews from critics. Rotten Tomatoes gives the film a "Certified Fresh" rating of 73%, based on 218 reviews from critics, with an average rating of 6.9/10. The sites consensus states, "Powered by Clint Eastwoods sure-handed direction and a gripping central performance from Bradley Cooper, American Sniper delivers a tense, vivid tribute to its real-life subject."  On Metacritic, the film has a score of 72 out of 100, based on reviews from 48 critics, indicating "generally favorable reviews".  In CinemaScore polls conducted during the opening weekend, cinema audiences gave American Sniper a rare grade of A+ on an A+ to F scale. 

  gave the film a positive review, saying "...an excellent performance from a bulked-up Bradley Cooper, this harrowing and intimate character study offers fairly blunt insights into the physical and psychological toll exacted on the front lines..."     , 23 December 2014  Chris Nashawaty of Entertainment Weekly gave the film a C+, saying "The films just a repetition of context-free combat missions and one-dimensional targets."  Elizabeth Weitzman of New York Daily News gave the film four out of five stars, saying "The best movies are ever-shifting, intelligent and open-hearted enough to expand alongside an audience. American Sniper   is built on this foundation of uncommon compassion."   Kyle Smith of the New York Post gave the film four out of five stars, saying "After 40 years of Hollywood counterpropaganda telling us war is necessarily corrupting and malign,   American Sniper nobly presents the case for the other side." 

Peter Travers of Rolling Stone gave the film three and a half stars out of four, saying "Bradley Cooper, as Navy SEAL Chris Kyle, and director Eastwood salute Kyles patriotism best by not denying its toll. Their targets are clearly in sight, and their aim is true."  Ignatiy Vishnevetsky of The A.V. Club gave the film a B, saying "American Sniper is imperfect and at times a little corny, but also ambivalent and complicated in ways that are uniquely Eastwoodian."  James Berardinelli of ReelViews gave the film three and a half stars out of four, saying "American Sniper lifts director Clint Eastwood out of the doldrums that have plagued his last few films."  Rafer Guzman of Newsday gave the film three out of four stars, saying "Cooper nails the role of an American killing machine in Clint Eastwoods clear-eyed look at the Iraq War."  Kenneth Turan of the Los Angeles Times gave the film a positive review, saying "Eastwoods impeccably crafted action sequences so catch us up in the chaos of combat we are almost not aware that were watching a film at all."  Claudia Puig of USA Today gave the film three out of four stars, saying "Its clearly Coopers show. Substantially bulked up and affecting a believable Texas drawl, Cooper embodies Kyles confidence, intensity and vulnerability."  Joshua Rothkopf of Time Out New York gave the film four out of five stars, saying "Only Clint Eastwood could make a movie about an Iraq War veteran and infuse it with doubts, mission anxiety and ruination." 
 Rolling Stone, Paul Edwards wrote in CounterPunch that he believes the film to be dangerous due to mutilating the classic heros journey into a simplistic, brutal, and sadistic destruction of "evildoers".   Inkoo Kang of The Wrap gave the film a negative review, saying "Director Clint Eastwood‘s focus on Kyle is so tight that no other character, including wife Taya (Sienna Miller), comes through as a person, and the scope so narrow that the film engages only superficially with the many moral issues surrounding the Iraq War."   Several other articles have also been critical of the movie.     
 Korean conflict, and then after that there was Vietnam, and it goes on and on forever ... I just wonder ... does this ever stop? And no, it doesnt. So each time we get in these conflicts, it deserves a lot of thought before we go wading in or wading out. Going in or coming out. It needs a better thought process, I think." 

Bradley Cooper stated that much of the criticism ignores that the film was about widespread neglect of returning veterans, and that people who take issue with Kyle should redirect their attention to the leaders who put troops there in the first place. He said: "We looked at hopefully igniting attention about the lack of care that goes to vets. Discussion that has nothing to do with vets or what we did or did not do, every conversation in those terms is moving farther and farther from what our soldiers go through, and the fact that 22 veterans commit suicide each day." Cooper said that an increasing number of soldiers are returning from conflict psychologically damaged, only to be more or less discarded. 
 Republican Party vice presidential nominee Sarah Palin also spoke out in support of the movie.   

===Home media===
American Sniper is scheduled to be released on Blu-ray and DVD on May 19, 2015. 

==Accolades==
 

{| class="wikitable sortable" style="width:99%;"
|- style="text-align:center;"
! colspan="5" style="background:#B0C4DE;" | List of awards and nominations
|- style="background:#ccc; text-align:center;"
! scope="col" | Award / Film festival
! scope="col" | Category
! scope="col" | Recipient(s) and nominee(s)
! scope="col" | Result
! scope="col" class="unsortable"|  
|-
! scope=row rowspan=6 | Academy Awards Best Picture Peter Morgan
|  
| style="text-align:center;" rowspan="6"| 
|- Best Actor
| Bradley Cooper
|  
|- Best Adapted Screenplay Jason Hall
|  
|- Best Film Editing
| Joel Cox and Gary D. Roach
|  
|- Best Sound Editing
| Alan Robert Murray and Bub Asman
|  
|- Best Sound Mixing
| John T. Reitz, Gregg Rudloff and Walt Martin
|  
|- Art Directors Guild Awards Excellence in Production Design for a Contemporary Film
| James J. Murakami, Charisse Cardenas
|  
| style="text-align:center;"| 
|- ACE Eddie Awards Best Edited Feature Film – Dramatic
| Joel Cox and Gary D. Roach
|  
| style="text-align:center;"| 
|-
! scope="row"| American Film Institute Awards 2014
| Top Ten Films of the Year
| 
|  
| style="text-align:center;"|   
|-
! scope=row rowspan="2" | British Academy Film Awards Best Adapted Screenplay
| Jason Hall
|  
| style="text-align:center;" rowspan="2"| 
|- Best Sound
| Walt Martin, John Reitz, Gregg Rudloff, Alan Robert Murray, Bub Asman
|  
|-
! scope="row"| Cinema Audio Society Awards
| Outstanding Achievement in Sound Mixing for a Motion Picture – Live Action
| Walt Martin, Gregg Rudloff, John Reitz, Robert Fernandez, Thomas J. O’Connell, James Ashwell
|  
| style="text-align:center;"| 
|-
! scope=row rowspan="2"| 20th Critics Choice Awards|Critics Choice Award Best Action Movie
| American Sniper
|  
| style="text-align:center;" rowspan="2"| 
|- Best Actor in an Action Movie
| Bradley Cooper
|  
|-
! scope=row rowspan="6"| Denver Film Critics Society
| Best Picture
| American Sniper
|  
| style="text-align:center;" rowspan="6"|  
|-
| Best Director
| Clint Eastwood
|  
|-
| Best Actor
| Bradley Cooper  (tied with Ralph Fiennes in The Grand Budapest Hotel) 
|  
|-
| Best Supporting Actress
| Sienna Miller
|  
|-
| Best Adapted Screenplay
| Jason Hall
|  
|-
| Best Cinematography
| Tom Stern
|  
|-
! scope="row"| Directors Guild of America Award Outstanding Directing – Feature Film
| Clint Eastwood
|  
| style="text-align:center;"| 
|-
! scope="row"| Empire Awards Best Actor
| Bradley Cooper
|  
| style="text-align:center;"| 
|-
! scope="row"| Iowa Film Critics
| Best Movie Yet to Open in Iowa
| American Sniper  (tied with A Most Violent Year)
|  
| style="text-align:center;"|  
|- MPSE Golden Reel Awards
| Feature English Language - Effects/Foley
| Bub Asman, Alan Robert Murray
|  
| style="text-align:center;"| 
|-
! scope=row rowspan="2"| MTV Movie Awards
| Movie of the Year
| American Sniper
|  
| style="text-align:center;" rowspan="2"| 
|-
| Best Male Performance
| Bradley Cooper
|  
|-
! scope=row rowspan="2"| National Board of Review
|  
|
|  
| style="text-align:center;" rowspan="2"|  
|- Best Director
| Clint Eastwood
|  
|-
! scope="row"| Producers Guild of America Awards Best Theatrical Motion Picture
| Bradley Cooper, Clint Eastwood, Andrew Lazar, Robert Lorenz, Peter Morgan
|  
| style="text-align:center;"| 
|-
! scope=row rowspan="2"| Satellite Awards Best Adapted Screenplay
| Jason Hall
|  
| style="text-align:center;" rowspan="2"|
|- Best Editing Gary Roach and Joel Cox
|  
|-
! scope="row"| Saturn Awards
| Best Thriller Film
| American Sniper
|  
| style="text-align:center;"|
|-
! scope="row"| Writers Guild of America Awards Best Adapted Screenplay
| Jason Hall
|  
| style="text-align:center;"| 
|}

== See also ==
* List of films featuring the United States Navy SEALs

== References ==
 

== External links ==
 
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 