Frozen Flashes
{{Infobox film
| name           = Die gefrorenen Blitze
| image          = 
| image size     =
| caption        =
| director       = János Veiczi
| producer       = Erich Kühne
| writer         = Dr. Julius Mader, Harry Thürk Alfred Müller
| music          = Günter Klück
| cinematography = Günter Haubold
| editing        = Ruth Ebel
| studio         = DEFA  
| distributor    = PROGRESS-Film Verleih
| released       = 14 April 1967 
| runtime        = 165 minutes part 1: 77 minutes part 2: 88 minutes
| country        = East Germany
| language       = German
| budget         =
| gross          =
}}

Die gefrorenen Blitze (Frozen Lightning; English-language title: Frozen Flashes; French-language title: Et lAngleterre sera détruite, England Would Be Destroyed) is a two-part 1967 East-German film. The plot revolves around the history of the resistance movement in Peenemünde during the Second World War and its attempt to sabotage the V-2 program.

==Plot==
===Part 1: Target Peenemünde===
on 5 November 1939, the British consulate in Norway receives the Oslo Report, but the Military Intelligence doubts its veracity. The German research in Peenemünde goes undisturbed from the British. In spite of this, various resistance groups gather information about the site and attempt to hinder the missiles development, taking great risks.

===Part 2:  Password Paperclip=== Los Alamos, a new, deadlier weapon is being developed.

==Cast==
*Alfred Müller as Dr. Grunwald
*Leon Niemczyk as  Stefan
*Dietrich Körner  as  the rocket baron (Wernher von Braun)
*Emil Karewicz as Jerzy
*Victor Beaumont as chief of British intelligence
*Mark Dignam as Sir John
*Ewa Wiśniewska as Hanka
*Fritz Diez as Adolf Hitler
*Mikhail Ulyanov as General Alexander Gorbatov
*Gerd Michael Henneberg as Albert Speer
*Renate Blume as Ingrid
*Werner Lierck as Private First Class Draeger
*Georges Aubert as Father Mollard
*Reimar Baur John as Dr. Kummerow
*John Mercator as Colonel Briggs
*Alan Winnington as chief of the reconnaissance
*Peter Doherty as squadron commander
*Helmut Schreiber as attaché
*Jiří Vršťala as Professor Rahn
*Steffen Klaus as Colpi
*Vera Oelschlegel as secretary
*Ingeborg Ottmann as Marianne
*Achim Schmidtchen as the Obergruppenführer
*Heinrich Narenta as Heinrich Himmler
*Hannjo Hasse as SA officer Zech

==Production==
The work on Die Gefrorene Blitze began already at 1964, and took three years to be completed.  Writer Harry Thürk and director  János Veiczi conducted extensive research in four countries: the United Kingdom, France, Germany and Poland. The script was mainly based on Dr. Julius Maders book "Secrets from Huntsville: the True Career of the Rockets Baron Werner von Braun." The producers deemed the film as one that continued the tradition of DEFAs classical antifascist pictures, focusing on the struggle of people from many different countries - including a catholic priest from France, a character whose portrayal as positive was not common in East Germany - to prevent the Nazis from developing long-range missiles. The title was derived from a common sobriquet for the V2 missiles - "Frozen Lightning". 

==Reception==
The film was screened outside the competition in the 1969 Cannes Film Festival.  It won the Golden Apsara Award in the 1969 Phnom Penh International Film Festival. 

==References==
 

==External links==
*  on the IMDb.

 
 
 
 
 