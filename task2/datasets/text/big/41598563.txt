Stand and Deliver (1928 film)
{{Infobox film
| name           = Stand and Deliver
| image          = Poster - Stand and Deliver.jpg
| imagesize      =
| caption        = Promotional movie poster.
| director       = Donald Crisp
| producer       = Ralph Block Cecil B. DeMille
| writer         = Sada Cowan (writer)  John W. Krafft (intertitles)
| starring       = Rod La Rocque  Lupe Velez
| cinematography = David Abel
| editing        =
| distributor    = Pathé Exchange
| released       =  
| runtime        = 57 minutes
| country        = United States
| language       = Silent film(English intertitles)
}}
Stand and Deliver is a 1928 silent film starring Rod La Rocque and Lupe Velez and directed by Donald Crisp. Cecil B. DeMille produced the picture with release through Pathé Exchange.  

==Cast==
*Rod La Rocque - Roger Norman
*Lupe Velez - Jania, a Peasant Girl
*Warner Oland - Ghika, Bandit Leader
*Louis Natheaux - Captain Dargia
*Clarence Burton - Captain Melok Charles Stevens - Pietro

unbilled
*James Dime - Patch Eye
*Frank Lanning -
*Alexander Palasthy - Juja
*Bernard Siegel - Blind Operator
*Donald Crisp - London Club Member

==Preservation status==
Prints of the film are preserved at George Eastman House and the UCLA Film and Television Archive. 

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 


 