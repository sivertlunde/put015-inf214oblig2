Just a Kiss (film)
 
{{Infobox Film
| name           = Just a Kiss
| image          = 
| image_size     = 
| caption        = 
| director       =  Fisher Stevens
| producer     =  John M. Penotti Dolly Hall Tim Williams
| writer         =  Patrick Breen
| narrator       =
| starring       = Ron Eldard Kyra Sedgwick Marisa Tomei Marley Shelton Taye Diggs
| music          =  Sean Dinsmore
| cinematography =  Terry Stacey
| editing        =   Gary Levy
| studio         =  Greene Street Films
| distributor    =  Paramount Classics
| released       =  27 September 2002
| runtime        =  89 min.
| country        =  United States
| language       =  English
| budget         = 
| gross          =  $ 63,797
| preceded_by    = 
| followed_by    = 
}}

Just a Kiss (2002) is a dark comedy and the first feature film directed by actor-turned-filmmaker Fisher Stevens.  Fisher also is credited with playing the harmonica (credited as Fisher Super Harp Stevens).  Patrick Breen wrote the screenplay adapted from his own off-Broadway play and starred in the film. It was filmed in New York, N.Y.
 
==Plot==
Dag (Ron Eldard) is a successful director of television commercials who shares his home with his beautiful girlfriend, Halley (Kyra Sedgwick). Dag, however, has a serious case of roving eye and is given to frequent flings with other women. Halley tries to turn a blind eye to Dags infidelity, but when she discovers he had a one-night stand with Linda (Idina Menzel), a beautiful but troubled modern dancer who is dating Dags close friend Peter (Patrick Breen), she decides things have gone too far. Halley gives Dag his walking papers and she soon makes the acquaintance of Andre (Taye Diggs), a very handsome and well-mannered classical musician. Andre, however, is married to Colleen (Sarita Choudhury), a woman with exotic sexual tastes who meets up with Peter, now suddenly without a girlfriend, on an airline flight. Meanwhile, Peters very angry confrontation with Dag attracts the attention of Paula (Marisa Tomei), a mysterious but very sexy woman who has taken a decidedly carnal interest in Peter. However, as Paula makes her way through Peters daisy-chained circle of friends, events begin taking a strange turn as her new acquaintances begin dropping like flies.

==Cast==
*Ron Eldard  - Dag
*Kyra Sedgwick  - Halley
*Marisa Tomei  - Paula
*Idina Menzel  - Linda
*Taye Diggs  - Andre
*Sarita Choudhury  - Colleen
*Patrick Breen  - Peter
*Zoe Caldwell  - Jessica
*Peter Dinklage  - Dink
*  Bruno Amato  - Joe

==Awards==
*2002 won Prize of the City of Setúbal at the Festróia - Tróia International Film Festival for 
*2002 nominated for the Open Palm Award for outstanding directorial debut at the Gotham Awards

==External links==
*http://movies.yahoo.com/movie/1804850634
* 
* 

 
 
 


 