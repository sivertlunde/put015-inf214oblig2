Too Much Is Enough
 
{{Infobox Film
| name           =  Too Much Is Enough
| image          =  
| image_size     = 
| caption        =  
| director       =   
| producer       =   
| writer         =   
| narrator       =   
| starring       =   
| music          =   
| cinematography =   
| editing        =   
| studio         =   
| distributor    =   
| released       =   
| runtime        =   
| country        =  
| language       =   
| budget         =   
| gross          = 
| preceded_by    = 
| followed_by    = 
}} Chalmers Award for best Canadian documentary, in 1996.

== Synopsis ==

Too Much is Enough pays homage to Gilles Groulx (1931-1994), considered by many to be one of Quebec’s most important and original filmmakers. Tragically, Groulx’s career was cut short in 1981 after he was seriously injured in a car accident. From 1989 to 1994, Richard Brouillette met regularly with Groulx, recording the latter’s thoughts on his work and life. Too Much is Enough combines footage from these sessions, excerpts from Groulx’s films and interviews Groulx gave while at the peak of his career.

== Technical details ==

* Producer, Director, Editor: Richard Brouillette
* Scriptwriter and Researcher: Richard Brouillette
* Cinematography: Michel Lamothe
* Sound recording: Claude Beaugrand, Pierre Bertrand, Simon Goulet
* Original music:  
* Sound mix: Louis Gignac
* Production and distribution: Les films du passeur
* Format : Colour and Black and White - 16mm - Mono
* Country: Quebec (Canada) French
* Genre:  
* Running time: 111 minutes

== Starring ==
* Gilles Groulx
* Jean-Paul Mousseau
* Barbara Ulrich
* Sara Raphaëlle Groulx
*  
* Josée Yvon

== Awards == Chalmers Award for best Canadian documentary (1996)

 
 
 
 
 
 
 
 
 
 


 
 