Barney (film)
{{Infobox film
| name           = Barney 
| image          = 
| image_size     =
| alt            =
| caption        = 
| director       = David Waddington
| producer       = David Waddington
| writer         = Colin Drake
| based on = 
| narrator       =
| starring       = {{plainlist|
* Brett Maxworthy
* Sean Kramer
* Lionel Long
* Spike Milligan
}}
| music          = Tommy Tycho
| cinematography = Richard Wallace
| editing        = Rod Hay
| studio         = Columbia Pictures
| distributor    = Columbia Pictures
| released       = 16 December 1976
| runtime        = 93 mins
| country        = Australia
| language       = English
| budget         = AU$230,000 Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, p 307 
| gross          = 
| preceded_by    =
| followed_by    =
}}
Barney is a 1976 Australian film for children set during the convict era. A 12-year-old boy and a convict are shipwrecked together.

It was also known as Lost in the Wild. 

==Plot==
In the 1880s, a small boy called Barney, a wombat called Amanda and an Irish convict called Rafe are the sole survivors of a shipwreck. They head off to Ballarat together to find Barneys father. On the way two gypsy woman drug Rafe and try to steal Barneys money. Rafe is wrongly accused of horse theft and his imprisoned by Barney helps him scape. Barney is eventually reunited with his father.

==Cast==
*Brett Maxworthy as Barney Dawson
*Sean Kramer as Rafe Duggan
*Lionel Long as Charles Dawson
*Spike Milligan as Hawker
*Jack Allen as Sergeant

==Production==
The budget was provided by Columbia Pictures and Australian Film Commission. Shooting began in May 1976 on the New South Wales coast and at the Australiana village at Wilberforce. 

==Release==
Despite the involvement of a Hollywood studio, commercial results were disappointing in Australia.  However it performed better in Hong Kong and Japan. 

==References==
 

==External links==
*  
*  at Oz Movies

 
 
 
 


 