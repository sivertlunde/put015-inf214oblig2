The Human Experience
{{Infobox film
| name = The Human Experience
| image = The_human_experience_poster_image2.jpg
| writer = Michael Campo
| producer = Joseph Campo
| starring = Clifford Azize Jeffrey Azize Michael Campo Matthew Sanchez
| director = Charles Kinnane
| music = Thomas J. Bergersen
| distributor = Grassroots Films
| editing = Charles Kinnane
| released =  
| runtime = 90 minutes
| country = United States
| language = English
}}
The Human Experience (2008) is a 90-minute documentary produced by Grassroots Films and directed by Charles Kinnane. The film tells the story of brothers Clifford and   and their travels as they search for answers to the question, "What does it mean to be human?". Their friends Michael Campo and Matthew Sanchez participate in some of the travels. The film is divided into three sections, covering the experiences of Jeffrey and his friends in New York, Peru, and Ghana. The Human Experience is rated PG-13.

==Plot==
The first experience follows Jeffrey and his brother Clifford Azize to the streets of New York City, where the boys live with the homeless for a week in one of the coldest winters on record. The brothers are the children of divorced parents; they have not seen their father for 10 years. The Azize brothers look for hope and camaraderie among their homeless companions, while learning how to survive on the streets.

During their second experience, the brothers join a group of surfers from Surf For The Cause   traveling to Peru. There, they visit a hospital for abandoned children in the foothills of the Andes Mountains. The boys are surprised to find joy among the children and their caretakers despite serious medical problems.

Throughout the story, the film features commentary from spiritual, artistic, and medical experts and philosophers, including William B. Hurlbut M.D.; Rabbi Simon Jacobson; Dr. Alveda King; and Rev. Richard Neuhaus (who was filmed before his death in January 2009).

In their final experience, the brothers, along with their friends Michael Campo and Matthew Sanchez, go to Africa. Michael is on his way to visit a leper colony in rural Ghana. On their way to the colony, the boys meet victims of AIDS and their families. Once they reach the leper colony, they befriend lepers who are disfigured from the disease and have been exiled from their villages.

At the end of the film, the boys return to their life in New York with a changed outlook and insight into the human condition. Also, at the end of the film, Jeffrey and Clifford have a surprise meeting with their father and are reunited with him. They have not seen each other for 10 years, but Jeffrey and Clifford still love their father and are able to reconcile with him.

==Production==
The Human Experience is produced by Grassroots Films’ Joseph Campo, Clifford Azize and Michael Campo. The film is directed by Charles Kinnane.

==Screenings and releases==
Pre-screenings of the film were held within the United States as well as internationally.  The film was initially screened in “rough cut”. The first public pre-screening was held on January 26, 2009 in Phoenix, Arizona.

The Human Experience was theatrically released in select Harkins Theatres in Arizona, Colorado, and Texas on April 9, 2010 in the midst of a world screening tour. The tour encompassed over 400 screenings in 15 countries across North America, South America, Europe, and Australia.

The film was released on DVD and Video on demand|V.O.D. by distributor   on March 29, 2011.

==Reception==
The film was well received at the Maui Film Festival (2008). Harriet Yahr of indieWire called it, "A doc with so much heart in the right place."  Alicia Colon, writer for the Irish Examiner and NY Sun described The Human Experience as, "Inspiring... an affirmation of what binds all of us around the world-our humanity." 
 Archbishop of Denver released a statement on his official website. "Grassroots Films has produced an astonishing witness to the beauty of the human person under the most trying conditions. For anyone committed to the cause of human dignity, or simply hungry to again see the beauty in life, it shouldn’t be missed."   In June, the Archbishop of Krakow, Poland sent an open letter to the producers of The Human Experience, which concluded with a hope that the film would "contribute to an ever deeper knowledge of the essence of human life and human dignity."  

==Awards==
The film has won many awards and festival accolades, including the Audience Choice Award at the   ( ). 

==Soundtrack==
An original score composed by Thomas J. Bergersen is featured in The Human Experience. The soundtrack, titled  , was released on March 29, 2011.

==References==
This article incorporates text from  , licensed under GNU Free Documentation license.
 

==External links==
*  
* 
* , 
* 
* 

 
 
 
 
 