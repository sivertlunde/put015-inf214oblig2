Death Hunt
{{Infobox film
| name           = Death Hunt
| image          = DeathHunt1981.jpg
| image_size     = 200px
| caption        = Promotional poster
| director       = Peter R. Hunt (credited as Peter Hunt)
| producer       = Murray Shostak
| writer         = Michael Grais and Mark Victor
| starring       = Charles Bronson Lee Marvin Andrew Stevens Carl Weathers Ed Lauter Angie Dickinson
| music          = Jerrold Immel
| cinematography = James Devis
| editing        = John F. Burnett Allan Jacobs Golden Harvest
| distributor    = 20th Century Fox
| released       =  
| runtime        = 97 min.
| country        = United States
| language       = English
| budget         = $10,000,000 
| gross          = $5,000,000 (domestic) 
| aspect ratio   = 1,85:1
}} Albert Johnson. The Mad Trapper (1972),  a British made-for-television production and Challenge to Be Free (aka Mad Trapper of the Yukon and Mad Trapper) (1975).   

==Plot==
In the Yukon Territory in 1931, Albert Johnson (Charles Bronson), a solitary American trapper, comes across an organized dog fight. A white German Shepherd is badly injured and Johnson forcibly takes it, paying $200 to its owner, a vicious trapper named Hazel (Ed Lauter).

Aggrieved by his treatment and claiming the dog was stolen from him, Hazel leads several of his friends to Johnsons isolated log cabin|cabin. Some begin shooting while others create a diversion. After the shooting of Sitka, the dog that Johnson has nursed back to health, the trapper kills one pursuer, Jimmy Tom (Denis Lacroix),

Once they discover that Johnson has bought 700 rounds of ammunition from the local trading post and paid in $100 bills, many conclude that he is the "mad trapper", a possibly mythical, psychopathic, serial killer who supposedly murders other trappers in the wilderness and takes their gold teeth. An old trapper, Bill Luce (Henry Beckman), warns Johnson that the law is coming for him. Johnson fortifies his cabin.

Sergeant Edgar Millen (Lee Marvin), commander of the local Royal Canadian Mounted Police post, seems a tough but humane man. He has a veteran tracker named "Sundog" Brown (Carl Weathers) and a young constable, Alvin Adams (Andrew Stevens), plus a new lover in Vanessa McBride (Angie Dickinson). He reluctantly agrees to investigate Hazels accusations that Johnson stole his dog and murdered Jimmy Tom.
 posse of mounties and trappers to the cabin. He parleys with Johnson, telling him that he has a pretty good idea of what happened and if Johnson comes with him they can get it sorted out. However, before Johnson can answer, one of the trappers opens fire. Several end up killed, including one who is shot by one of his own friends. The posse uses dynamite to blow up the cabin, but Johnson escapes, shooting dead a Mountie, Constable Hawkins (Jon Cedar).

Millen, Sundog and Adams, joined by Hazel with his tracker dogs, set off into the frozen wilderness after Johnson. The case has made front page news across the country, and many trappers join in the chase, attracted by the $1,000 bounty that has been placed on Johnsons life. Captain Hank Tucker (Scott Hylands), a Royal Canadian Air Force pilot, is sent by the government to join the hunt, which is causing a national embarrassment. He reveals that Johnson was a member of a United States Army special intelligence unit during World War I.
 tracking techniques to avoid Millens posse and the bounty hunters, living off the land in treacherous winter conditions. As the hunt continues, Millen begins to respect Johnsons uncommon abilities, while growing to resent the intrusion of so many outsiders.

Luce comes across two of the trappers camping in the wilderness and shoots them both dead before pulling out their gold teeth. Luce, it seems, is the real mad trapper.

The pursuers catch up to Johnson. Tucker begins to strafe the area indiscriminately with his aircraft machine gun, killing Sundog. The enraged Millen and Adams shoot down the aircraft with their rifles; Tucker crashes into a canyon wall and is killed. Johnson escapes after killing Hazel.

Luce comes across Johnson and tries to kill him, presumably attracted by the reward. Johnson tricks him and captures him at gunpoint. Millen spots Johnson and opens fire; the bullet hits him in the face, rendering him unrecognizable. As they examine the body, both Millen and Adams spot the real Johnson, dressed in Luces clothes, on a ridge above them. The man they shot was Luce dressed in Johnsons clothes.

The Mounties allow Johnson to flee into Alaska, well aware that everything he did was in self-defense. As the other pursuers appear, Adams tells them that Millen has killed Johnson. A trapper finds that the body has a pocket full of gold teeth, so they celebrate the killing of the "mad trapper".

==Cast==
Listed in credits order:   IMDb. Retrieved: December 1, 2014. 
  
* Charles Bronson as Albert Johnson
* Lee Marvin as Sgt. Edgar Millen
* Andrew Stevens as Constable Alvin Adams
* Carl Weathers as George Washington Lincoln "Sundog" Brown
* Ed Lauter as Hazel
* Scott Hylands as Captain Hank Tucker
* Angie Dickinson as Vanessa McBride 
* Henry Beckman as Bill Luce
  
* William Sanderson as Ned Warren
* Jon Cedar as Constable Hawkins
* James OConnell as Hurley
* Len Lesser as Lewis
* Maury Chaykin as Clarence
* August Schellenberg as Deak De Bleargue
* Richard Davalos as Beeler (credited as Dick Davalos)
 
 

==Historical accuracy==
Death Hunt bears little semblance to the true story of the manhunt of Albert Johnson, the reputed "Mad Trapper of Rat River". Johnson was eventually killed after a remarkable and highly publicized pursuit over several weeks. Of special note was the fact that Johnson eluded his RCMP pursuers in the dead of winter in the lower Arctic, crossing the Richardson Mountains in the process, a feat previously considered impossible. On February 17, 1932, Johnson was finally surrounded by Mounties on the frozen Eagle River, and shot and killed. 
 Bellanca aircraft who was involved in the hunt for Johnson. Contrary to the film, May, who was portrayed as "Captain Tucker", did not wildly shoot at everyone, including the posse on the ground, nor did he get shot down. May was unscathed and lived until 1952. 

During the siege of Johnsons cabin, at least five men were killed by Johnson in the film. Constable Millen was shot and killed by Johnson and was the only Mountie killed by Johnson during the manhunt. Two other RCMP officers that Johnson shot survived. 

In the film, it was claimed that Johnson was a World War I veteran, with Captain Tucker providing Johnsons military service record to Millen and the other RCMP officers. According to researcher Frank W. Anderson, virtually nothing is known of Albert Johnson before his arrival at Fort McPherson on July 9, 1931. To this day, the Mad Trappers true identity has never been established. 

==Production==
  replica featured prominently in Death Hunt.]] Canmore and Bristol F.2b Fighter replica in RCAF markings. The aircraft was on skis and equipped with an upper wing-mounted machine gun. 

==Reception==
In Vincent Canbys review for The New York Times, he noted that the plot had problems. "Nothing in Death Hunt makes a great deal of sense, though the scenery is rugged and the snowscapes beautiful." Canby, however, recognized that two old pros were at work. "Mr. Bronson and Mr. Marvin are such old hands at this sort of movie that each can create a character with ease, out of thin, cold air."  Reviewer Leonard Maltin characterized Death Hunt as having "... good action, but not enough of it." 

==Home media==
Death Hunt was first released on VHS by CBS/Fox video in the early 1980s. A DVD of Death Hunt was released by   (1979).  
==References==

===Notes===
 

===Citations===
 

===Bibliography===
 
* Anderson, Frank W. and Art Downs. The Death of Albert Johnson, Mad Trapper of Rat River. Surrey, British Columbia, Canada: Heritage House, 1986. ISBN 978-1-89438-403-2.
* Lentz, Robert F. Lee Marvin: His Films and Career. Jefferson, North Carolina: McFarland & Company, Inc., 1999. ISBN 978-0-78640-723-1.
* Maltin, Leonard. Leonard Maltins Movie Guide 2009. New York: New American Library, 2009 (originally published as TV Movies, then Leonard Maltin’s Movie & Video Guide), First edition 1969, published annually since 1988. ISBN 978-0-451-22468-2.
* North, Dick. The Mad Trapper of Rat River: A True Story of Canadas Biggest Manhunt. Toronto, Ontario, Canada: Macmillan Company, 1972. ISBN 978-1-59228-771-0.
* Pigott, Peter. Flying Canucks: Famous Canadian Aviators. Toronto, Ontario, Canada: Hounslow Press, 1994. ISBN 978-0-88882-175-1. 
* Solomon, Aubrey. Twentieth Century Fox: A Corporate and Financial History. Lanham, Maryland: Scarecrow Press, 1989. ISBN 978-0-81084-244-1.
 

==External links==
*  
*  
*  
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 