Venetian Bird
{{Infobox film
| name           = Venetian Bird
| image          = VenetianBird-Poster.jpg
| image size     =
| alt            =
| caption        =
| director       = Ralph Thomas
| producer       = Betty Box (producer) Earl St. John (executive producer)
| writer         = Victor Canning
| narrator       =
| starring       =
| music          = Nino Rota
| cinematography = Ernest Steward
| editing        = Gerald Thomas
| studio         =
| distributor    = General Film Distributors (1952, UK, theatrical) United Artists (1953, USA, theatrical)
| released       = 1952
| runtime        = 95 minutes; USA:90 minutes
| country        = United Kingdom
| language       = English
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}

   1952 British thriller film directed by Ralph Thomas and starring Richard Todd, Eva Bartok and John Gregson. 

==Synopsis==
British private detective Edward Mercer (Richard Todd) is employed to travel to Venice and locate an Italian who is to be rewarded for his assistance to Allied airmen during the Second World War. Once he arrives in Italy, however, he becomes mixed up in an assassination and a great deal of mystery. The mystery revolves around whether Renzo Uccello (John Gregson) actually died a few years earlier in World War II or not.  The city of Venice provides a familiar and magnificent backdrop for the story.

==Cast==
* Richard Todd as Edward Mercer
* Eva Bartok as Adriana Medova
* John Gregson as Renzo Uccello
* George Coulouris as Chief of Police Spadoni
* Margot Grahame as Rosa Melitus
* David Hurst as Minelli
* Walter Rilla as Count Boria John Bailey as Lieutenant Longo
* Sid James as Bernardo
* Sydney Tafler as Boldesca
* Miles Malleson as Grespi
* Eric Pohlmann as Gostini

==References==
 

==External links==
*  
 
 
 
 
 
 
 
 
 
 
 