Deccani Souls
{{Infobox film
| name           = Deccani Souls
| image          = 
| alt            =  
| caption        = 
| director       = Kaz Rahman
| producer       = Kaz Rahman
| writer         = Kaz Rahman
| starring       = M.A. Siddiq Sathya Bhama H.K.S. Babu
| music          = Hédi Hurban
| cinematography = Kaz Rahman
| editing        = Kaz Rahman
| studio         = Charminar Films
| distributor    = 
| released       =  
| runtime        = 106 minutes
| country        = Canada India USA
| language       = 
| budget         = 
| gross          = 
}} Hyderabad and are unknowingly connected by the history of Operation Polo.  

==Cast==
* M.A. Siddiq as Siddiq
* Sathya Bhama as Sathya
* H.K.S. Babu as Babu
* Mr. Sultan as Mr. Sultan
* Mr. Syed as Mr. Syed
* Kaz Rahman as Hamza

==Production== Hyderabad and Bidar in India and in Ontario, Canada. 

The details of Operation Polo mentioned in the film are taken from the Sunderlal report.  

==Screenings== Hyderabad and Mumbai. 

The film was screened as part of the 31st Three Rivers Film Festival in Pittsburgh, USA in November 2012. 

==References==
 

==External links==
*  
*  
*  

 
 
 