Shatru
 
 

{{Infobox film
| name           = Shatru
| image          = Shatrufilm.jpg
| image_size  = 
| caption        = Promotional Poster
| director       = Pramod Chakravorty
| producer     =  Pramod Chakravorty
| story        =  
| lyrics          = Anand Bakhshi Shabana Sadique Raj Kiran Sudhir Mac Mohan
| music          = R.D.Burman
| cinematography = V. K. Murthy
| art director      = Shanti Das	
| distributor    = 
| released       = 15 Aug 1986 
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 Shabana Sidique was from Bangladesh.  The film had box office collection of 17.0&nbsp;million in 1986. 

==Synopsis== Shabana Sidique) and also gets fond of an orphan child from the village named Chotu. Ashok then learns that Chotus father was a loyal farmer who was duped and was killed by Nishikants men. He even promises Chotu of bringing the culprits to book. Somehow then Shah and his friend Choudhry manage to put Ashok in trouble by fixing a blame on him of killing a man in his custody. Due to this he then faces a situation where he could lose his job and his good reputation too.

==Reception==
The film had box office collection of   in 1986.The film was a commercial success and was given three and a half stars by critics in the Bollywood guide Collections.

==Cast==
*Rajesh Khanna as Inspector Ashok Sharma
*Shabana Sadique as Asha
*Prem Chopra as Nishikant Shah
*Arun Govil as Sub-Inspector Salim
*Master Tapu as Chotu
*Ashok Kumar as Superintendent of Police
*Om Shivpuri as Gopal Choudhry, MLA Raj Kiran as Raj Choudhry
*Mac Mohan as Kanu
*Anoop Kumar Das as Anup Chatterjee

==Music==
*"Tere aanchal mein" - Lata Mangeshkar, Asha Bhosle
*"Baaboojee dil loge" - Asha Bhosle
*"Main teraa bismil hoon" - Andrew Kishore
*"Iskee topee uske sar" - Kishore Kumar& Chorus
*"Sooraj chandaa saagar parbat" - Andrew Kishore & Chorus 

==References==
 

==External links==
*  

 
 
 
 


 