Burning the Wind
 
{{Infobox film
| name           = Burning the Wind
| director       = {{plainlist|
*Herbert Blaché
*Henry MacRae
}}
| producer       = Hoot Gibson
| writer         = {{plainlist|
*Gardner Bradford
*George H. Plympton
*William MacLeod Raine
*Raymond L. Schrock
}}
| starring       = {{plainlist|
*Hoot Gibson
*Virginia Brown Faire
}}
| cinematography = {{plainlist|
*Harry Neumann
*Ray Ramsey
}}
| editing        = Tom Malloy Universal Pictures
| released       =  
| runtime        = 60 minutes
| country        = United States
| language       = Silent with English intertitles
}}

Burning the Wind is a 1929 American romance film, directed by Herbert Blaché and Henry MacRae, starring Hoot Gibson and featuring Boris Karloff.    The film is considered to be lost film|lost. 

==Cast==
* Hoot Gibson as Richard Gordon Jr
* Virginia Brown Faire as Maria Valdez
* Cesare Gravina as Don Ramón Valdez
* Boris Karloff as Pug Doran
* Gilbert Holmes as Peewee (as Pee Wee Holmes)
* Robert Homans as Richard Gordon Sr
* George Grandee as Manuel Valdez

==See also==
* List of American films of 1929
* Hoot Gibson filmography
* Boris Karloff filmography

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 

 
 