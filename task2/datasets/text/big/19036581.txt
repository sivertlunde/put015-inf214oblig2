Hotel Sahara
{{Infobox film
| name           = Hotel Sahara
| image          = Hotsahpos.jpg
| image_size     =
| caption        = US film poster
| director       = Ken Annakin
| producer       = George Hambley Brown
| writer         = George Hambley Brown Patrick Kirwan
| starring       = Yvonne De Carlo Peter Ustinov David Tomlinson Roland Culver
| music          = Benjamin Frankel
| cinematography = David Harcourt Jack Hildyard
| editing        = Alfred Roome
| distributor    = General Film Distributors (UK) United Artists (US)
| released       = 1951
| runtime        = 96 minutes
| country        = United Kingdom
| language       = English
| budget         =
}}
Hotel Sahara is a 1951 British comedy film directed by Ken Annakin and starring Yvonne De Carlo, Peter Ustinov and David Tomlinson. It was produced and co-written by George Hambley Brown.

==Plot==
A luxury hotel in the Sahara Desert faces occupation and destruction from a number of armies during the North African Campaign.  The hotel is saved through the quick thinking and chameleon actions of the four headed staff in particular the owner Yasmin, who dresses, sings, and dances in a manner to make all the conquerors appreciate her charms.

==Production== Vice Versa.  Based on some of his experiences in the Western Desert Campaign, Brown sought Yvonne De Carlo for the role of Yasmin by writing her and saying that he observed that she had a flair to play comedy. 

==Cast==
* Yvonne De Carlo as Yasmin Pallas
* Peter Ustinov as Emad
* David Tomlinson as Captain Puffin Cheyne
* Roland Culver as Major Bill Randall
* Albert Lieven as Lieutenant Gunther von Heilicke Bill Owen as Private Binns
* Guido Lorraine as Captain Giuseppi
* Mireille Perrey as Mme. Pallas
* Ferdy Mayne as Yusef
* Sydney Tafler as Corporal Pullar
* Eugene Deckers as a French Spahi officer
* Anton Diffring as a German Soldier 
* Olga Lowe as Fatima

==Soundtrack==
Say Goodbye Soldier Boy  
Music by Benjamin Frankel (as Ben Bernard) 
Lyrics by Harold Purcell  
Sung by Yvonne De Carlo 
 
I Love A Man 
Music by Benjamin Frankel (as Ben Bernard) 
Lyrics by Harold Purcell  
Sung by Yvonne De Carlo 
 
Ill Be Seeing You (song)|Ill Be Seeing You  
Music by Sammy Fain 
Lyrics by Irving Kahal 
Heard on a radio sung by Yvonne De Carlo 
 
Early One Morning 
Traditional 
Sung by Yvonne De Carlo 
 
Lillibullero 
Traditional 
 
Santa Lucia 
Traditional

==Notes==
 

==External links==
* 

 

 
 
 
 
 
 
 
 


 
 