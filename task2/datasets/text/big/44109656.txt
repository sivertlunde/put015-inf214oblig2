Cowboy (2013 film)
 
 
 
{{Infobox film
| name             = Cowboy
| image            =
| caption          = Theatrical Poster
| director         = P. Balachandrakumar
| producer         = K. Anil Mathew
| writer           = Dr. V. S. Sudhakaran Nair Khushboo Bala Bala Mythili Lena  Saikumar
| music            = Berny-Ignatius
| background music = Rajamani
| cinematography   = Sanjeev Shankar
| editing          = Samjith
| country          = India
| language         = Malayalam
| released         =  
}}
 malayalam thriller thriller film Khushboo and Bala and Saikumar play the main antagonists and Mythili, Jagathy Sreekumar, Anoop Chandran and  Indrans play supporting roles. The film is produced by K. Anil Mathew and music is done by Benny Ignatious and background score is by Rajamani. The film is inspired by the 1999 Bollywood comedy thriller blockbuster Baadshah (1999 film)|Baadshah. Although the original film was a superhit, cowboy received mostly poor reviews and was a disaster at the box office. The film released on 15 February 2013.

== Plot ==
Vinay (Asif Ali) lives in Kuala Lumpur, Malaysia with his parents. His father (Kalasala Babu) is totally against him and his ways. Vinay runs a pub named ‘Cowboy’ which makes his father comment that he makes a living by shamelessly running a brothel. Vinay’s sister Veena (Lena (actress)|Lena) and brother-in-law Mohan (Irshad (actor)|Irshad), who also live in Kuala Lumpur, have no love for him.

When ‘Cowboy’ begins, Veena and Mohan are set to arrive from India. Their son Pankaj (Master Pankaj Krisshna) is in Singapore and Vinay gets set to take Pankaj to the airport in Mohan’s expensive car, hand over the kid and the car to Veena and Mohan and come back. But on the way, Vinay is forced to chase and fight two guys who try to steal Pankaj’s diamond necklace. Vinay in action is what catches the fancy of an Indian Police Officer Xavier (Bala (actor)|Bala), who happens to pass by. Xavier then takes Pankaj hostage and threatens to kill him if Vinay doesn’t do what he wants him to do. Xavier wants Vinay to kill someone. He hands Vinay a gun and asks him to go to a hotel, where his target is. Xavier himself would be there, he is told. Vinay finds that his ‘target’ is Revathy Menon (Khushboo (Pakistani actress)|Khushboo) the external affairs minister of India. But he finds out that the mastermind behind the conspiracy is actually Revathys husband (Saikumar (Malayalam actor)|Saikumar) when he and Krishna, (Mythili) revathys PA, go to her husbands room and he kills Krishna with the help of Xavier. On the day of the murder, Vinay slips into the ministers office by crossing Xaviers eyes and tells Revathy about the conspiracy occurring there (in the original version, it is the heroine that comes into the ministers room but will not tell the minister anything but will put a paper with all the details into the ministers speech file.) At the auditorium, Vinay expects that Revathy will not come to the auditorium. But shockingly she turns up and having no choice, he aims the gun at her and waits for a while. Meanwhile, a man (Jagathy Sreekumar) who is the brother of a hairstylist (Anoop Chandran) who had helped Vinay get into the ministers room gets Pankaj out of Xaviers possession on the saying of Vinay. when Vinay is assured that Pankaj is safe, he turns the gun at Xaviers henchmen who were the security guards at the place and shoots them. Then a shootout occurs at the place and taking advantage of Revathys distraction, Xavier takes his pistol out and shoots her. Her husband runs up towards her and starts crying. Then immediately he begins to laugh and reveals that he had actually arranged for the killing to get even with Revathy. It turns out that he had also applied for the elections with her but she promised him that if she wins the post, she will immediately hand it over to him. But fearing the illegal operations he might create, she does not give the post to him and takes charge. Then just after he reveals everything, She opens her eyes and it is revealed that she had actually come to the press meet to check whether Vinay was speaking the truth and for safety, she and her bodyguards wear bulletproof jackets within their clothes. the police arrive there and take Xavier and Revathys husband into custody and Vinay is given a reward by the Indian government for his bravery.

== Cast ==
*Asif Ali as Vinay Bala as Xavier Khushboo as Revathy Menon
*Mythili as Krishna
* Master Pankaj Krisshna as Pankaj
*Kalasala Babu as Vinays father Lena as Veena Irshad as Mohan

== Tracklisting ==
{{infobox album  
| Name             = Cowboy 
| Type             = soundtrack
| Arist            = Benny Ignatious
| Lyrics           = Rajamani
| Cover            = 
| Alt              =         
| Recorded         =
| Genre            = Film soundtrack
| Length           =  
| Producer         = 
}}

{{Tracklist
| collapsed           = 
| headline            = Tracklist 
| total_length        = 09: 46
| writing_credits     = 
| lyrics_credits      = Yes
| music_credits       = 
| title1              = Thottithodan Thoni
| lyrics1             = Benny Ignatious
| length1             = 04: 25
| title2              = Ullaga Vaalaki
| lyrics2             = Benny Ignatious
| length2             = 05: 21
}}

== Critical reception and box office ==
The film was a disaster at the box-office collecting only a total of Rs. 88,00,000. Unni. R. Nair of Kerala9.com reviewed and said "Well, nothing new about the film, but still it could have been watchable if it was written, directed and acted out stylishly. ‘Cowboy’ as it is, is totally flawed. Bad scripting, poor direction, performances that are not up to the mark and a totally flawed execution makes the film insufferable. In fact, as I sit typing out this review, I wonder if at all the film needs a review in the first place. No, maybe!!"

 
 
 
 
 