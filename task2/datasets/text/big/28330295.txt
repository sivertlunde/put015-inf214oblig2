Tři vejce do skla
 
{{Infobox film
| name           = Tři vejce do skla
| image          = 
| image size     = 
| caption        = 
| director       = Martin Frič
| producer       = 
| writer         = Václav Wasserman
| starring       = Vlasta Burian
| music          = 
| cinematography = Ferdinand Pečenka
| editing        = 
| distributor    = 
| released       =  
| runtime        = 94 minutes
| country        = Czechoslovakia
| language       = Czech
| budget         = 
}}
 Czech comedy film directed by Martin Frič.    It was released in 1937...Vincenc Babočka, a meaningless employee of police station, thinks he is a detective with exceptional abilities. Holiday in Karlovy Vary gives him an opportunity to prove to everyone what he is really worth. He is mistakenly considered to be international and police prosecuted adventurer Leon Weber. He decides to take advantage of the confusion and tries to find Leons accomplice that are getting ready to steal diamonds belonging to Maharaja of Yohir. According to Weber plan, he meets Maharaja in disguise for Prince Narishkin. Everything is going according to the plan until the real Weber appears...

==Cast==
* Vlasta Burian as Vincenc Babočka / Leon Weber / Prince Narishkin
* Antonín Novotný (actor) as Van Houden
* Helena Bušová as Sandra, secretary
* Míla Reymonová as Jiřina, Webers lover
* Bohuš Záhorský as Alois, accomplice of Weber
* Rudolf Kadlec as Yohir, false maharaja
* Karel Dostál as the secretary of the false maharaja
* Jaroslav Marvan as a doctor Theodor Pištěk as a police commissioner
* Čeněk Šlégl as a client of Babočka

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 


 
 