Futurama: The Beast with a Billion Backs
{{Infobox film
 
| name           = Futurama:  The Beast with a Billion Backs
| image          = Futurama BillionBacks.jpg
| caption        =
| alt            = Cartoon image of purple tentacles coming from bright tear in the red sky. The tentacles hold Leela in the air. Below, other characters shake fists in anger, while Fry runs away. The text reads "Futurama: The Beast with a Billion Backs. All new feature-length adventure!"
| director       = Peter Avanzino
| producer       = Lee Supercinski Claudia Katz
| screenplay     = Eric Kaplan
| story          = Eric Kaplan David X. Cohen Billy West Katey Sagal John DiMaggio Tress MacNeille Maurice LaMarche Phil LaMarr Lauren Tom David Herman Brittany Murphy David Cross Dan Castellaneta
| music          = Christopher Tyng
| cinematography =
| editing        = Paul D. Calder
| distributor    = 20th Century Fox Home Entertainment
| studio         = The Curiosity Company
| released       =  
| runtime        = 89 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} English with Shakespeares Othello. Comedy Central aired the movie as a "four-part epic" on October 19, 2008.  The movie won an Annie Award for "Best Animated Home Entertainment Production".

==Plot==
  Amy and Kif get Fry starts many more boyfriends.

At a scientific conference,   after beating his rival, List of recurring Futurama characters#Dr. Ogden Wernstrom|Wernstrom, in a game of Deathball (a gigantic version of Labyrinth (game)|Labyrinth). When Bender explores the anomaly, his touch causes it to emit a shock wave that sends him and the ship flying. Farnsworth and Wernstrom discover that only living beings can pass through the anomaly; electrical objects such as robots are either repelled or destroyed. The two plan to initiate another expedition but are rejected in favor of a military assault on the anomaly led by Zapp Brannigan.

Meanwhile, both Fry and Bender begin to feel lonely. Fry sneaks aboard Zapps ships lint cabinet just before the ship takes off so that he can find solace on the other side of the anomaly. Bender attempts suicide, only to be approached by the League of Robots, a secret society of robots who like to blather about and tease humans, led by his hero List of recurring Futurama characters#Antonio Calculon|Calculon. Bender becomes a very prestigious member due to his perceived hatred of humans, although Calculon suspects that Bender is deceiving them.

Fry enters the anomaly while Kif is killed during Zapps unsuccessful plan of attack. On the other side of the anomaly, Fry comes across a Beholder (Dungeons & Dragons)|colossal, one-eyed, tentacled creature, which begins forcing its appendages through the anomaly. The tentacles attack everyone in the universe, and nothing can stop them since they are made of electro-matter that can only be harmed by other electro-matter. Fry returns to Earth with a tentacle attached to the back of his neck and tells everyone to "love the tentacle." The tentacles attach themselves to nearly everyone, causing their victims to fall in love with it. With the monsters influence spreading, Fry becomes the pope of a new religion established to worship the tentacles.

Bender, meanwhile, believes that the League of Robots should uphold a strict anti-humans policy. However, when he assists his friends in eluding the tentacles by hiding them in another members leg, he is caught red-handed by the other members. When Calculon calls his bluff about hating humans, he challenges Calculon to a duel in which he cheats. This confrontation results in the loss of Calculons arm and severe damage to much of the city, including the Leagues own headquarters. Calculon is outraged by Benders behavior and resigns from the League, making Bender its new leader.
 Leela ends up the last person in the universe unattached to a tentacle, after Zapp and Amy are taken over after Zapp seduces Amy. She examines a tentacle fragment and discovers that they are actually reproductive organs, revealing this to everyone at a universal religious gathering. The creature, named "Yivo", admits that mating with everyone in the universe was its original intention but explains that it is now truly in love with them.  As a sign of good faith, Yivo resurrects Kif, who is displeased to learn that Zapp lured the lonely Amy into sleeping with him. Yivo asks to begin the relationship anew and removes its tentacles from everyone.
 Robot Devil for an army of robots to take over the world and rebuild his human-hating reputation, which happens in exchange for Benders first-born son, whom Bender kicks into a pit of lava after a heartwarming reunion. Before Bender can attack, humanity leaves Earth willingly to live on Yivo, along with the other civilizations of the universe, moving onto Yivos body via golden escalators. This leaves Bender lonely once more and stagnates the robot population, who are built to serve humans. As the robots inherit Earth, everyone else promises never to make contact with other universes. Fry, however, cannot help writing a letter to Bender, which is sent without Yivo knowing. Yivos body appears like a classic vision of Heaven, with Angels really being harmless birds. Leela does not trust Yivos motives at first, but when she sees that everyone is happy, she succumbs to her own loneliness and accepts Yivo.

Bender receives Frys letter, which is made out of electro-matter, and decides to set out and "rescue" his friend from his relationship with Yivo. He and his army harpoon Yivo from the other side of the tear in space-time and tow it into their own universe where they are able to attack it. Fry convinces Bender to spare Yivo, but Yivo discovers that the robots weapons are lined with the electro-matter from Frys letter, allowing them to harm it. Since Fry broke his promise to never make contact with other universes, Yivo breaks up with the universe and makes everyone leave. While everyone else leaves aboard Benders ship, Yivo finds consolation with Colleen, and they begin a relationship as they head back to the other universe together and close the anomaly, as Yivo cannot stay in the Universe due to the fact it would suffocate.

Everyone laments that they will never know happiness or love with Yivo again. Fry decides to find love elsewhere and tries to ask Leela out, but she rejects him since he had already stopped trying once he met Colleen; Kif and Amys relationship is strained due to Amys affair with Zapp; and Farnsworth and Wernstrom go back to being archrivals. Bender breaks up his friends quarrel and assures them that what they experienced was not love, as love is a jealous, hard-to-get emotion that does not share itself with the world. Bender shares his own love with Fry and Leela by giving them a big hug, which actually strangles them.

==Cast==
 
{| class="wikitable"
|-
! Actor
! Character
|- Billy West Professor Farnsworth Dr. Zoidberg Zapp Brannigan Additional voices
|- Leela
|- Bender Additional voices
|-
| Tress MacNeille || Additional voices
|- Calculon Hedonismbot Additional voices
|- Billionairebot Additional voices
|-
| Lauren Tom || Amy Wong Additional voices
|-
| David Herman || List of recurring Futurama characters#Dr. Ogden Wernstrom|Dr. Wernstrom Additional voices
|- The Robot Devil
|-
| David Cross || Yivo
|-
| Stephen Hawking || Himself
|-
| Brittany Murphy || Colleen OHallahan
|}

==Features==

===Opening===
  Mickey role), Fry, Bender. The Planet Express Ship then flies by the moon and zaps the Hydroponic Farmer from "The Series Has Landed" with the ships ray gun. Following the cartoon, the Planet Express Ship crashes out from the television billboard.
 binary representation The Robots are Coming! The Robots are Coming!". The opening cartoon of the first part is the 1931 Flip the Frog cartoon The Soup Song, while the opening cartoons for the last three parts is the same one seen in "The Devils Hands Are Idle Playthings", that of Futurama s own opening sequence.

===Guest stars=== polyamorous girlfriend; Professor Stephen the Robot Devil. 
Aside from her regular work on the animated series King of the Hill, this was Murphys last voice-over role before her death in December 2009.

===DVD===
The DVD features a commentary track, deleted scenes, and footage animated for the   . The Lost Adventure also features its own commentary track.

==References==
 

==External links==
 
* 
* 
* 
* 
*  at the Infosphere.
*  at  
* 
* 

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 