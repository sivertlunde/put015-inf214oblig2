Foodland (film)
{{Infobox film
| name           = Foodland
| image          = Foodland Poster-1.jpg
| caption        = Original theatrical release poster
| director       = Adam Smoluk
| producer       = Juliette Hagopian
| writer         = Adam Smoluk
| starring       = James Clayton   Ross McMillan   Kim Poirier 
| cinematography = Keith Eidse
| music          = Mitch Dorge
| editing        = Ron Wisman
| studio         = Julijette Inc.
| runtime        = 80 minutes
| released       =  
| country        = Canada
| language       = English
}}
Foodland is a 2010 Canadian comedy film written and directed by Adam Smoluk.

==Synopsis==
The story follows Trevor, a naive grocery clerk, as his life spirals out of control when he inadvertently helps Ian, his inept manager, rob the store. When the money is lifted, they enlist the help of Glen Munn, a slimy, incompetent detective, to retrieve the stolen cash. But events soon go awry and Trevor must now determine friend from foe, and get the cash back before it’s too late.

==Cast==
*James Clayton as Trevor Wolnik
*Ross McMillan as Ian Cullmore
*Kim Poirier as Lucy Eklund
*Stephen Eric McIntyre as Glen Munn
*Aaron Merke as Rick
*Jason Malloy as Anthony
*Nancy Drake as Lesley
*Aaron Hughes as Cliff
*Kate Yacula as Vicki
*Alicia Johnston as Shelly
*Peter Jordan as George Leap
*Doreen Brownstone as Ians Mother

==Production==
Foodland is an 2010 Canadian film which was filmed in Winnipeg, Manitoba. The script was selected for the National Screen Institute of Canadas Features First Program.  The film stars James Clayton, Ross McMillan and Kim Poirier. It had its premiere in Winnipeg on  , 2010 and a limited run from  , 2011.  The film is actor Kim Poiriers first time singing on screen in a feature film. She sings a cover version of the classic song "You Made Me Love You (I Didnt Want to Do It)".     

==Release==
 Super Channel, IFC in March 2014.   

==Reception==
Winnipegs foremost alternative weekly Uptown (newspaper)|Uptown Magazine compared Foodland to the films of the Coen Brothers for the way its "twisty, often brilliant plotting... seems to want to set records for sheer number of turns: the plot fishtails continuously to the final scene". Beyond that, Uptown also said that "Splendidly written Winnipeg indie comedy Foodland highlights what a good screenplay can do for a film."   

An article in the Winnipeg Free Press highlighted the strong performances of the cast, "The particular triumph of Smoluks film is that the performances are generally in sync with the Winnipeg milieu of the crime caper."   
 
A positive review by the Canadian Broadcasting Corporation|CBCs Alison Gilmor wrote, "In this offbeat crime caper by young local filmmaker... Smoluk is working on a tight budget, but he gets quite a lot out of a little. His cast is crackerjack... and the flat, dead-of-winter Winnipeg setting is weirdly atmospheric."   

==References==
 

==External links==
*  
*  

 
 
 
 
 
 