The Dark Avenger
{{Infobox film 
 | name        = The Dark Avenger
| image          = File:The Dark Avenger - Poster.jpg
| image_size     = 
| caption        = 1955 British Theatrical Poster
 | director    = Henry Levin 
 | producer    = Walter Mirisch
 | writer      = Daniel B. Ullman Phil Park (uncredited) based on = story by Daniel B. Ullman
 | starring    = Errol Flynn Joanne Dru Peter Finch Yvonne Furneaux
 | music       = Cedric Thrope Davie Guy Green 
 | editing     = Edward B. Jarvis
 | studio      = Allied Artists Pictures 20th Century Fox
 | distributor = Allied Artists Picture Corp 20th Century Fox
 | released    = Mid-  (England)   9 September 1955 (US) Of Local Origin
New York Times (1923-Current file)   09 Sep 1955: 19.  
 | runtime     = General release: 85 min.
 | language    = English  gross = 890,587 admissions (France) 
}}
 
The Dark Avenger is a 1955 English Adventure film directed by Henry Levin. The screenplay was written by Daniel B. Ullman (and an uncredited Phil Park ).  The film stars Errol Flynn, Joanne Dru and Peter Finch. The music score is by Cedric Thorpe Davie. It is also known as The Warriors in the United States,  and had a working title of The Black Prince in the United Kingdom. 

The Dark Avenger follows the adventures of Edward the Black Prince, son of King Edward III and heir to the throne of England, as he tries to liberate the people of Aquitaine from the cruel grasp of France.
 historical action film Errol Flynn would ever make.  

==Plot==
Edward, Prince of Wales, son and heir to his father King Edward III of England, leads an English army to the French province of Aquitaine to protect the inhabitant from the ravages of the French. After defeating the French in battle, the defeated French plot to kill the prince. Failing in this, they kidnap his lady, the lovely Lady Joan Holland. Of course Prince Edward has to ride to the rescue, adopting numerous guises to save his paramour, which ultimately end in him leading his men into one final climactic battle against the French.

== Cast ==
 
* Errol Flynn as Edward, the Black Prince Lady Joan Holland
* Peter Finch as Comte de Ville
* Yvonne Furneaux as Marie
* Patrick Holt as Sir Ellys
* Michael Hordern as Edward III
* Moultrie Kelsall as Sir Bruce Robert Urquhart as Sir Philip Du Guesclin
* Frances Rowe as Genevieve
* Alastair Hunter] as Libeau
* Rupert Davies as Sir John
* Ewen Solon as DEstell John Holland Richard OSullivan Thomas Holland Jack Lambert as Dubois  John Welsh as Gurd 
* Harold Kasket as Arnaud 
* Leslie Linder as François Le Clerc  Robert Brown as First French Knight  John Phillips as Second French Knight
 

==Production==
The film was originally known as The Black Prince. 
 Allied Artists in 1953-54, a co-production with Associated British Pictures, to be filmed in England, shot in CinemaScope and Technicolor. 20 FULL-LENGTH FILMS SET BY ALLIED ARTISTS
Los Angeles Times (1923-Current File)   17 July 1953: A3.   36 FILMS PLANNED BY ALLIED ARTISTS: Studio Announces Ambitious Program With Both 3-D and Standard Movies
Special to THE NEW YORK TIMES.. New York Times (1923-Current file)   17 July 1953: 13.   It was personally produced by Walter Mirisch, who was production head of Allied Artists at the time. 16 WARNER MOVIES ON BIG-SCREEN LIST:  A Star Is Born and The High and the Mighty Included in CinemaScope Schedule
By THOMAS M. PRYORSpecial to THE NEW YORK TIMES.. New York Times (1923-Current file)   18 Nov 1953: 41.   Mirisch had developed the project with Dan Ullman.

The use of CinemaScope saw 20th Century Fox became involved as partners in production and distribution on the movie, as part of an arrangement between it and Allied Artists. (It was a two-picture arrangement, the other film being The Adventures of Haji Baba). Allied Artists took Western Distribution Rights, Fox took Eastern. Drama: Frank Lloyd Readying Texian; Dana Andrews Gets Builder-Upper
Schallert, Edwin. Los Angeles Times (1923-Current File)   15 Apr 1954: A13.   This enabled the studios to share costs, and for Allied to take advantage of Foxs superior distribution system in foreign countries when it came to handling CinemaScope films. It also enabled them to afford Errol Flynn in the lead role. FOX JOINS ALLIED TO MAKE 2 FILMS: Companies Will Produce and Distribute Pictures -- Use of CinemaScope Planned
By THOMAS M. PRYORSpecial to The New York Times.. New York Times (1923-Current file)   15 Apr 1954: 35.   

Joanne Dru was also imported to play the female lead; Peter Finch was cast as the main villain. Henry Levin was chosen to direct on the basis of several swashbuckling movies he had made for 20th Century Fox and Columbia.   accessed 4 March 2015  The film was reported to be the biggest undertaking in Allied Artists history. 

Filming started on 2 August 1954.  It mainly took place at Elstree Studios, using a castle  constructed by MGM for Ivanhoe (1952). Tony Thomas, Rudy Behlmer & Clifford McCarty, The Films of Errol Flynn, Citadel Press, 1969 p 204 

Flynn made the film shortly after his proposed movie of William Tell had fallen over and was in bad need of funds.  Walter Mirisch wrote in his memoirs that Flynn shaved off his moustache in preproduction to make him look younger; Mirish did not agree and arranged for the script to include Flynn growing his moustache back. The producer said that Flynns drinking frequently held up the production, with the actor occasionally drinking during takes and being unable to remember his lines. 
 The Adventures Captain Blood, The Sea Hawk and all those other great adventure films of his youth."  
==Reception== Allied Artists Guy Green) in color at Englands Elstree Studios, with a spanking array of period castles and costumes cluttering the lovely countryside, it all looks quite fetching. Number two, it moves. Finally&mdash;perhaps as a consequence&mdash;the familiar, history-laden plot unwinds with a surprising lack of pretentiousness for this type of film.  Peeled of its vintage trappings, however, the picture would play&mdash;indeed, does&mdash;like the mouldiest kind of Western, the one about the noble cowboy who routs the greedy land barons (French), saving the land for the settlers and papa (His Majesty, King Edward I)." 

The Los Angeles Times called it "an inferior but colorful swashbuckler." James Dean Sympathetic Rebel Without a Cause
Scheuer, Philip K. Los Angeles Times (1923-Current File)   10 Nov 1955: B12.  
==References==
{{reflist|refs=
   
   
}}

==External links==
* 
*  at TCMDB
 

 
 
 
 
 
 
 
 
 
 
 