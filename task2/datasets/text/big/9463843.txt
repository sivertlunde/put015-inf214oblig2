La Vallée (film)
 
{{Infobox film
| name        = La Vallée
| image       = La Vallee.jpg
| caption     = Theatrical release poster
| writer      = Barbet Schroeder Monique Giraudy
| director    = Barbet Schroeder
| distributor = Imperia Films
| released    =  
| runtime     = 100 minutes
| country     = France
| language    = French
| budget      =
| music       = Pink Floyd
| awards      =
}} French film written and directed by Barbet Schroeder.  The film stars Bulle Ogier as Viviane, a woman who goes on a strange and accidental voyage of self-discovery through the New Guinea bush. 

Pink Floyd recorded an album, Obscured by Clouds, as the soundtrack to the film. After recording had finished, the band fell out with the film company, prompting them to release the soundtrack album as Obscured by Clouds, rather than La Vallée. In response, the film was retitled La Vallée (Obscured by Clouds) on its release.   
 Gong with System 7 with Hillage.

==Synopsis== consul in Mapuga tribe, one of the most isolated groups of human beings on earth, who inspire them to explore their own humanity, unfettered by their own subjective ideas of "civilization". The search becomes a search for a paradise said to exist within a valley marked as "obscured by cloud" on the only map of the area available dated as surveyed in 1969.

==Trivia==
Footage from La Vallée was incorporated into the horror film Hell of the Living Dead.

==Cast==
* Bulle Ogier: Viviane
* Jean-Pierre Kalfon: Gaëtan
* Valérie Lagrange: Hermine
* Michael Gothard: Olivier
* Jérôme Beauvarlet: Yann
* Miquette Giraudy (credited as Monique Giraudy): Monique
* The Mapuga Tribe and its Chiefs

==References==
 

==External links==
*  
*  

 
 

 

 
 
 
 
 
 
 

 