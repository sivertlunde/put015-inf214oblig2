Second Thoughts (1983 film)
{{Infobox film
| name           = 
| image          = 
| alt            = 
| caption        = 
| film name      = Second Thoughts
| director       = Lawrence Turman
| producer       = Lawrence Turman David Foster
| screenplay     = Steve Brown
| story          = Steve Brown Terry Louise Fisher
| based on       = 
| starring       = Lucie Arnaz Craig Wasson Ken Howard
| narrator       = 
| music          = Henry Mancini King Baggot
| editing        = Neil Travis
| studio         = David Foster Productions EMI Films
| distributor    = Universal Pictures
| released       = February 1983
| runtime        = 98 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} American drama film directed by Lawrence Turman and starring Lucie Arnaz, Craig Wasson, Ken Howard and Anne Schedeen. 

==Cast==
* Lucie Arnaz - Amy
* Craig Wasson - Will
* Ken Howard - John Michael
* Anne Schedeen - Janis Arthur Rosenberg - Doctor Eastman
* Peggy McCay - Doctor Carpenter
* Tammy Taylor - Sharon
* James OConnell - Chief Staab
* Louis Giambalvo - Sergeant Cabrillo
* Alex Kubik - Officer Behnke
* Charles Lampkin - Judge Richards
* Michael Prince - Alfred Venable
* Susan Duvall - Trudy
* Larry David - Monroe Clark
* Joseph Whipp - Jailer

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 


 