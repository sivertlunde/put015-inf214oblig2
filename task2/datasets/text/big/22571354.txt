The Bear Cage
{{Infobox Film
| name           = The Bear Cage
| image          = 
| image_size     = 
| caption        = 
| director       = Marian Handwerker
| producer       = Jacqueline Pierreux
| writer         = Paul Paquay
| narrator       =  Jean Pascal
| music          = 
| cinematography = Michel Baudour
| editing        = Michèle Maquet Denise Vindevogel
| distributor    = 
| released       = 1974
| runtime        = 90 minutes
| country        = Belgium
| language       = French
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

The Bear Cage ( ) is a 1974 Belgian drama film directed by Marian Handwerker. It was entered into the 1974 Cannes Film Festival.   

==Cast== Jean Pascal - Léopold Thiry
* Yvette Merlin - La mère Michel François - Bernard Puce - Julie
* Pascal Bruno - Grand-père
* Daniel Dury - Lucien Jacques Courtois - Prof. de français
* Marcel Melebeck - Frère du père
* Tine Briac - Femme du frère
* Jules Goffaux - Le colonel
* Patrick Boelen - Dumont

==References==
 

==External links==
* 

 
 
 
 
 
 