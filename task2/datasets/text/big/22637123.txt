Pippi in the South Seas (film)
{{Infobox film
| name           = Pippi in the South Seas
| image          = Pippi3 gip.jpg
| caption        = DVD cover
| director       = Olle Hellbom
| producer       = Olle Nordemar
| writer         = Astrid Lindgren (novel) Olle Hellbom (screenplay)
| starring       = Inger Nilsson Maria Persson Pär Sundberg
| music          =  , Sov alla
| editing        =  Jan Persson
| released       =  January 24, 1970 (Sweden)

June 1974 (USA) (Limited)

September 1, 1975 (USA)
| distributor    = Beta Film
| runtime        = 86 minutes
| country        = Sweden Swedish
}} 1970 Swedish eponymous childrens Pippi Longstocking. It was released in the USA in 1975.

==Synopsis== South Seas to hunt for her father, who has been kidnapped by Piracy|pirates. 
== Cast ==
* Inger Nilsson-Pippi Longstocking
* Maria Persson-Narrator/Annika
* Pär Sundberg-Tommy
* Beppe Wolgers-Captain Efraim Longstocking
* Martin Ljung-Jocke with the Knife
* Jarl Borssén-Blood-Svente
* Öllegård Wellton-Annikas and Tommys Mother
* Fredrik Ohlsson-Annikas and Tommys Father
* Staffan Hallerstam-Marko
* Tor Isedal-Pedro
* Håkan Serner-Franco
* Alfred Schieske-Innkeeper
* Wolfgang Völz-Oscar
* Nikolaus Schilling-Pirate
* Olle Nordemar-Pirate
* Gunnar Lantz-Pirate
* Carl Schwartz-Pirate
* Per Bakke-Pirate
* Kelvin Leonard-Pirate
* Ingemar Claesson-Pirate
* Olle Nyman-Pirate
* Nibbe Malmqvist-Pirate
* Åke Hartwig-Pirate
* Thor Heyerdahl-Pirate
* Arne Ragneborn-Pirate
* Per Møystad Backe-Pirate 

==References==
 

==External links==
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 