Sade (film)
{{Infobox film 
| name = Sade
| image = Sade film.jpg
| caption = 
| director = Benoît Jacquot 
| producer = Patrick Godeau
| writer = Serge Bramly ( novel ) Jacques Fieschi
| starring = Daniel Auteuil Marianne Denicourt 
| cinematography = Benoît Delhomme
| editing = Luc Barnier
| distributor = Empire Pictures
| budget = 
| released = 23 August 2000 ( France ) 
| runtime = 100 minutes
| country = France
| language = French
}}
 2000 French directed by Benoît Jacquot, adapted by Jacques Fieschi and Bernard Minoret from the novel La terreur dans le boudoir by Serge Bramly.

== Plot ==

Paris in 1794: After prolonged detention is Marquis de Sade, who claims during the hearing, neither noble nor the author of the novel Justine to be matched with other nobles in a former monastery in Picpus. This serves as a precious prison, where the inmates are referred to the outside world as a patient. The drive to the property shall Sade returns with Madame and Vicomte de Langris and their daughter Emilie. He is fond of the young woman, but precedes him his bad reputation. The parents prevent any contact between Emilie and Sade.
Sade gets in the prison visiting his mistress Marie-Constance Quesnet, which he calls "sensitive". She brings him writing materials and fine food he has wanted from her. In his absence, she began a relationship with the Convention member Fournier, through which it hopes to keep Sade before the execution. In fact, Fournier Sade can repeatedly save them from death, however, believes that he can close tie so sensitive in. Sade writes daily for the detention. Emilie, who is interested in his writings, they may see, after initial opposition. Sade suspects that the content for the young woman can not be processed, and actually hastens Emilie shocked out of his room, after reading a few lines. She holds the following time away from him, but defeated their curiosity with time their fear.
Sade begins to rehearse with the inmates of the prison, a theater piece that initially banned by the warden, but then allowed as a dumb game. Although Emily does not want to participate, Sade can persuade them to play along. Both meet at the home gardener of the monastery, which is inhabited by the young Augustine. Emilie indicated its intention to submit Sade. However, its nature, unabashedly to take her in the crotch and open her mouth with her hands, she irritated. He can feel her insecurity and leaves her. Some time later, the silent performance of the play takes place, which is interrupted suddenly. The park of the prison is confiscated by soldiers who are on the property but the dead of executions in mass graves to be buried. Robespierre and his men are now planning systematically the execution of nobles who oppose the new direction. Sensitive learns of Fournier that Sade is to be executed in a few days. She goes to him and warns him intently. See also the inmates of the prison, as more and more people will be picked up. In addition, every day new carts laden with dead on the property. Emilie feared that they never existed, because if it were murdered. She agrees Sade, to submit to his actions. On the night Sade Emilie brings to Augustin into the garden house. Both are inexperienced and Sade redirected to them. It can be whipped by Augustin, but it will stop beating after a while conscious. He then staged encounters between Augustine and Emilie the kidnapping. He deflowered Emilie finally by hand before leaving they sleep together and Augustin.
Sade missing the next morning in the monastery building, be proclaimed as a new person to the execution, including he. As one of the inmates explains Sade was already no longer existed weeks, he escapes execution. Shortly after Robespierre was arrested and executed. Fournier also a member of the Convention is killed. The nobles prison is dissolved. Sensible appears to take Sade with it. She thanks Emilie, Sade have sweetens life in prison, but Emilie is even grateful for everything she has learned. Sade advises her not to have children because they deform the body, but they feared much more to be trapped in the future in a monotonous marriage. Sensitive departs with the coach. Sade goes along beside the carriage, but she climbs after a while.

== Principal cast ==
* Daniel Auteuil – Marquis de Sade 
* Marianne Denicourt – Sensible
* Jeanne Balibar – Madame Santero
* Grégoire Colin – Fournier
* Isild Le Besco – Emilie de Lancris 
* Jean-Pierre Cassel – Le vicomte de Lancris 
* Sylvie Testud – Renée de Sade

==See also==
* Quills was an English-language film covering similar subject matter, released the same year.

==References==
 
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 