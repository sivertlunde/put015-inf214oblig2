Orania (film)
{{Infobox film
| name           = Orania
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Tobias Lindner
| producer       = Tobias Lindner Sascha Supastrapong
| writer         = 
| screenplay     = 
| story          = 
| narrator       = 
| starring       = 
| music          = 
| cinematography = Tobias Lindner
| editing        = Melanie Schütze
| studio         = Beuth Hochschule für Technik Berlin
| distributor    = 
| released       =  
| runtime        = 94 minutes
| country        = Germany South Africa
| language       = Afrikaans
| budget         = 
| gross          = 
}}
Orania is a 2012 German documentary film written and directed by Tobias Lindner. The film is an insight into the social experiment of the town of Orania, Northern Cape, a majority Afrikaner enclave in South Africa. The film explores cultural identity and the line between self-determination and discrimination. On 12 April 2012 a teaser trailer was released.  The film received its World Premiere at the Raindance Film Festival on 28 September 2012 in London where it competed in the Best Documentary category. 

==Synopsis==
In the town of Orania, 800 white Afrikaans people form an independent community. Their town is private property (bought in 1990) and they live independently from multicultural South Africa. Since the fall of apartheid, increasing crime levels, unemployment and social pressure has led to a small migration of people towards the town. In the town, the residents concentrate on preserving their shared culture. Residents stay in the town for their cultural ideals or for the towns safety and opportunities, and others stay out of desperation. 

==References==
 

==External links==
*  

 
 
 
 
 
 

 