Poor Little Peppina
{{Infobox film
| name = Poor Little Peppina
| image = Poor Little Peppina poster.jpg
| caption = Theatrical poster
| director = Sidney Olcott
| writer = Kate Jordan
| starring = Mary Pickford
| producer =
| distributor = Paramount Pictures
| budget =
| released =  
| country = United States English intertitles
| runtime = 48 minutes
}}
Poor Little Peppina is a 1916 American silent film directed by Sidney Olcott. The film was in 1916 Mary Pickfords longest film to be made. It was soon surpassed by her later films. 

==Plot==
Robert Torrens (Edwin Mordant) is a wealthy American, residing in Italy with his wife (Edith Shayne) and only daughter, Lois (Eileen Stewart).  Franzoli Soldo (Antonio Maiori) is a mafia chief who pretends to be a butler and is in Torrens employ. One day, he drinks too freely of his masters wine. Fellow employee Pietro (Ernest Torti) informs Mr. Torrens, who next dischargs Soldo.

Soldo wants revenge and kills Pietro. He is caught, however, and is being put on trial for the murder. He is found guilty and sentenced to a life in jail. One month later, a mafia member helps him escape. He is determined to take revenge on the Torrens family and kidnaps Lois. When the parents find out, they call the police. Soldo is soon thought of to be the kidnapper, but he ordered some of Torrens staff member to convince the parents Lois drowned in an accident.

Meanwhile, Soldo flees to his relatives, including his wife Bianca (Mrs. A. Maiori) - who is ordered to raise Lois as her own - and his son Beppo (Jack Pickford). Lois grows up to be Peppina (Mary Pickford), Beppos sister. Soldo decided to meanwhile take refuge in America. Fifteen years later. The Duchess, an American heiress, takes an interest in Peppina and teaches her English.

A man named Bernando wants to marry Peppina and convinces her parents to let him take her hand. Peppina, however, has no desire to be with him and asks the Duchess what to do. She helps her escape overseas and promises her a friend of hers will provide her a home in America. Peppina runs away from home in disguise and dresses up as a boy so nobody will recognize her.
 Eugene OBrien) is on the boat as well and meets Amy, a socialite from New York. Peppina takes refuge in his cabin, but is soon caught by him. He provides her comfort and food and offers her to stay at his cabin for the night. However, he doesnt know Peppina is actually a girl.

In New York, Soldo finds out the Torrens family will move to New York as well. He thinks he will be rewarded if he brings their daughter back to him and is determined to make some money. He sends his relatives in Italy a letter they should bring Peppina to him. Bianca responds she doesnt know where Peppina is. Meanwhile, Peppina spots Hugh together with Amy and decides to leave him. In New York, she applies for a job in Soldos café.

After a bad experience with Soldo, Peppina becomes a messenger "boy". When she is taken under arrest, she confesses she is actually a girl. Hugh happens to be a chief at the police station and releases Peppina and orders for Soldo to be taken under arrest. After Soldo arrives at the police station, Peppina realizes he was the one who abducted her as a child. Peppina is now recognized as the Torrens kid. Mr. and Mrs. Torrens are soon informed and reunited with their child.

Three years have passed. Peppina, now living in wealth, and Hugh are in love with each other.

==Cast==
* Mary Pickford as Peppina Eugene OBrien as Hugh Carroll
* Antonio Maiori as Soldo
* Ernest Torti as Pietro
* Edwin Mordant as Robert Torrens
* Jack Pickford as Beppo
* Edith Shayne as Mrs. Torrens
* Cesare Gravina as Villato

==See also==
*Mary Pickford filmography

==References==
 

==External links==
* 
* 
* 
*  available for free download at  
*    website dedicated to Sidney Olcott

 
 
 
 
 
 
 