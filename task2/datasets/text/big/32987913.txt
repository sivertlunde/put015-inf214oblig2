Cupid Camouflaged
 
 
{{Infobox film
  | name     = Cupid Camouflaged
  | image    = 
  | caption  =  Alfred Rolfe		
  | producer = 
  | writer   = 
  | based on = 
  | starring = 
  | music    = 
  | cinematography = Lacey Percival
  | editing  = 
 | studio = Australasian Films
  | distributor = 
  | released = 31 May 1918 
  | runtime  = 4 reels
  | language = Silent film English intertitles
  | country = Australia
  | budget   = 
  }}
 Alfred Rolfe. It is a high society melodrama. The film was made to aid fund raising for the Red Cross. It is considered a lost film. 

==Plot==
Rosita Manners (Rosamund Lumsdaine) falls in love with Tony (Captain Saltmarshe) and become engaged on a picnic at Port Hacking. Rositas mother (Mrs T. H. Kelly) wants her daughter to marry Valentine Loring (JBN Osborne), who she believes is of noble blood. Tony and Rosita elope, so Rositas mother tries to marry Valentine herself – until she discovers he is not from nobility, just a dress designer.

==Cast==
*Ethel Knight Kelly|Mrs. T. H. Kelly as Mrs Manners 
*Miss Rosamund Lumsdaine as Rosita Manners 
*Miss Madge Hardy as Althea Gardner
*Mr. James Osborne as Valentine Loring
*Captain Saltmarsh, A.D.C. as Tony Martin
*Mr. J. L. Maude as Charles Leslie
*Col. and Mrs. Macarthur Onslow 
*Captain and Mrs. Glossop
*Mr. and Mrs. Arthur Allen
*Miss Edith Walker 
*Miss Barbara Bowker
*Miss Betty Levy
*Miss Nina Massie
*Miss Joyce Allen
*Mr. K. Austin
*Mr. George Merivale
*Mr. Ferguson

==Production==
Scenes were shot at Rona, Bellevue Hill, and Mr. A. W. Allens well-known Port Hacking house, Moonbara,  The cast included many members of Sydney society and production of the film was extensively covered in society columns. 

==Reception==
An early screening of the film earned £1,100 for the Red Cross. 

According to one review:
 Cupid Camouflaged has certainly succeeded in swelling the Red Cross funds ; but it is a poor advertisement for the acting talent of the nobility of Sydney. Cupid used to be a lively little cherub ; this camouflaged Cupid has taken a sleeping draught, and cant stay awake. The slight plot is effectively smothered under about a thousand feet of uninteresting fox-trotting and ungraceful acrobatic dancing, under another thousand of garden party, and an endless amount of tea-drinking.  
A writer from the Sydney Morning Herald said that "This little photoplay... was beautifully photographed, so brightly, and with naturally, acted that it afforded vivacious entertainment to everyone in the theatre." 

Another review in The Sunday Times said:
 In producing this film, Mr, Alfred Rolfe had much to cope with, but he has made the best of a difficult job, and turned out a creditable piece of celluloid amusement. Though Cupid Camouflaged is distinctly amateurish, and there is an inclination to gaze into the cameras eyes, still, even professionals are not always free from those weak nesses. Captain Saltmarshc does his best with the role of the hero, and, when he lets himself go makes quite a likeable character. In Miss Rosamunde Lumsdaines acting there is much to be thankful for. She has not cultivated Mary Pickfords curls, and she behaves like an ordinary girl. Mrs, T. H. Kelly makes a dashing, well gowned figure of the designing matron, and some of the best work of the picture is done by Mr. James Osborne... The photography is good.  
Another film to aid the Red Cross, His Only Chance (1918), was made in Melbourne.

==References==
 

==External links==
*  
*   at National Film and Sound Archive
*  at AustLit
 

 
 
 
 
 
 
 
 


 