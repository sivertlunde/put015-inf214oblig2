Christopher Columbus – The Enigma
{{Infobox film
| name           = Christopher Columbus - The Enigma
| image          = 
| caption        = 
| director       = Manoel de Oliveira
| producer       = François dArtemare	
| writer         = Manoel de Oliveira Manuel da Silva and Sílvia da Silva (book)
| starring       = Ricardo Trêpa Leonor Baldaque Manoel de Oliveira
| music          = 
| cinematography = Sabine Lancelin
| editing        = Valérie Loiseleux	 		
| distributor    = 
| released       = 13 December 2007 (Portugal)
| runtime        = 75 minutes
| country        = Portugal
| language       = Portuguese English 
| budget         =
| gross          =
}}
Christopher Columbus – The Enigma (Cristóvão Colombo - O Enigma) is a 2007 Portuguese film directed by Manoel de Oliveira. It was filmed in both Portugal and the United States.

==Cast==
* Ricardo Trêpa as Manuel Luciano (1946–60)  
* Leonor Baldaque as Sílvia (1957–60)  
* Manoel de Oliveira Manuel Luciano (2007) 
* Maria Isabel de Oliveira as Sílvia (2007) 
* Jorge Trêpa as Hermínio 
* Lourença Baldaque as O Anjo   
* Leonor Silveira as Mãe  
* Luís Miguel Cintra as Director Museu Porto Santo

==See also==
 
*Cinema of Portugal

==References==
 

==External links==
*  

 

 
 
 
 
 


 
 