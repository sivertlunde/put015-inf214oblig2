Tag: The Assassination Game
 
{{Infobox film
| name           = TAG: The Assassination Game
| image          = Tag The Assassination Game.jpg
| image_size     =
| caption        = poster
| director       = Nick Castle
| writer         = Nick Castle
| producer       = Dan Rosenthal Peter Rosten
| narrator       =
| starring       = Robert Carradine Linda Hamilton Kristine DeBell Perry Lang John Mengatti Michael Winslow Frazer Smith Xander Berkeley Bruce Abbott
| music          = Craig Safan
| cinematography = Willy Kurant
| editing        = Tom Walls
| distributor    = Ginis Films
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| website        =
| amg_id         =
}}

TAG: The Assassination Game is a 1982 film directed by Nick Castle and starring Robert Carradine and Linda Hamilton in her first feature film starring role.   It is based on the game Assassin (game)|Assassin.  It was also released under the title Everybody Gets it in the End. 

==Plot==
At an American college, a group of students play a game with suction cup dart toy guns similar to The 10th Victim where a pair of students are assigned to "kill" the other one first by shooting him with a dart.  One student, Loren Gersh (Bruce Abbott) lives purely to play the game with his expertise in "killing" all of his opponents and not being "killed" himself making him a renowned master.

When one of his cringing victims accidentally drops his dart gun, it goes off and hits Gersh, "killing" him.  Faced with the embarrassment of losing his reputation by a geek getting lucky, Gersh really kills his opponent, setting him on the goal to use actual weapons and real killing from then on.  His opponents in the game are unaware of Gershs new rules.

Gersh slowly transforms from an average student to a James Bond-type killer.

==Production notes==
One of Gershs planned victims is Susan Swayze, played by Linda Hamilton.  The two performers met on the set of the film and subsequently married. 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 


 
 