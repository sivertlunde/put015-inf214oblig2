White Slave Ship
{{Infobox film
| name           = White Slave Ship
| image          =White Slave Ship.jpg
| image_size     =
| caption        =
| director       = Silvio Amadio
| producer       =
| writer         = 
| based on       =
| starring       = Pier Angeli  Edmund Purdom
| music          =Les Baxter  Angelo Francesco Lavagnino 
| cinematography =Aldo Giordani 
| editing        =
| studio         =
| distributor    =American International Pictures (US)
| released       = 1961 (IT) 1962 (US)
| runtime        = 
| country        = Italy
| language       = English
| budget         =
| gross          =
}}
White Slave Ship is a film released in the United States in 1962.  The original Italian version of the film was released in December 1961 as Lammutinamento.

==Plot==
In 1675 the prison ship the Albatross sails from England to the New World.

==Cast==
*Pier Angeli as Polly 
*Edmund Purdom as Dr. Bradley 
*Armand Mestral as Calico Jack 
*Ivan Desny as Captain Cooper 
*Michèle Girardon as Anna
*Mirko Ellis as Lord Graveston

==References==
 

==External links==
*  at IMDB

 
 
 


 