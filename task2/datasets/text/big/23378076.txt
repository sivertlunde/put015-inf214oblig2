Prem Granth
{{Infobox film
| name           = Prem Granth
| image          = Prem Granth.jpg
| caption        = dvd cover Jainendra Jain
| starring       = Rishi Kapoor, Madhuri Dixit, Anupam Kher
| director       = Rajiv Kapoor
| producer       = Randhir Kapoor cinematography  =Jal Mistry
| distributor    = Filmkraft
| released       = 22 May 1996
| runtime        = 156 min.
| language       = Hindi
| music          = Laxmikant-Pyarelal
| budget         = 
}}

Prem Granth ( : Scripture of Love) is a Hindi movie which was released in India on 22 May 1996. Directed by Rajiv Kapoor, the movie stars Rishi Kapoor and Madhuri Dixit and deals with the subject of rape. The movie was a commercial failure. 

== Summary ==

Somen (Rishi Kapoor), a lawyer and son of head priest Swami Dharam Bhushan Maharaj (Anupam Kher), is a man with a very strong character and courage of conviction. He believes in the principles of equality and freedom and often confronts his father on issues of social justice and religion. He especially disapproves of Kedar Nath (Prem Chopra), who uses his fathers clout to exploit the treasury in the temple. His uncle Nandlal (Shammi Kapoor) owns a prosperous dairy farm and keeps himself away from the affairs of religion and social obligations, citing them as unjust and outdated.

Somen meets a beautiful young woman Kajri (Madhuri Dixit) at the annual festival and is drawn towards her at first sight &mdash; her lower social caste notwithstanding &mdash; and that becomes the dawn of their love. They part unexpectedly, leaving the fire of love burning in Somen. He tries his best to find Kajri but in vain. Kajri and her father Baliram are on their way back to their village Bansipura where en route Kajri is kidnapped and forcefully raped by a drunken mysterious man. Kajri becomes pregnant with the mans child. Kajris mothers sister wants to marry her to a shoemaker but then Kajri leaves the village in secret and gives birth to the baby of the man who raped her.She begged people for food to eat so that her breast becomes full with milk and her child can survive. The baby then dies and Kajri meets Swami Dharam Bhushan to cremate the child but then he refuses. Kajri buries the baby.

A year passes and they meet again. Somen finds Kajri working at his uncle Nandlals farm and his love blossoms. He tries to express his love towards her, but she remains evasive. Destiny left marks of pain and agony on her soul. She writes a letter to Somen that she was a rape victim, an unwed mother, and unjustly ostracized from her society. She loves Somen as well, but the misfortune of her past weighs on her mind at every moment. Meanwhile Kedarnath burns a woman alive since she wanted to file a case against a powerful man named Roop Sahai (Govind Namdeo). 

Kajri and Somen are engaged in Nandlals dairy farm, but Dharam Bhushan comes there and reveals that a tearful Kajri came to him requesting to cremate her dead child even though she didnt know the childs fathers name. Somen then leaves but Nandlal reveals the truth that Kajri was raped and it is not her fault. Kajris aunt reveals that the man who raped her is Roop Sahai who once raped her aunt as well.

Kajri goes to Shreepur and reveals to Somen that Roop Sahai was the drunkard man who raped her. Kajri, Somen and Baliram then burn Roop Sahai and Kedarnath on Dusshera and then Somen marries Kajri.

== Cast ==
* Shammi Kapoor - Nandlal 
* Rishi Kapoor - Somen
* Madhuri Dixit - Kajri
* Anupam Kher - Swami Dharam Bhooshan 
* Om Puri - Baliram
* Prem Chopra - Kedar Nath
* Reema Lagoo - Parvati  
* Himani Shivpuri - Natho
* Sulabha Arya - Laxmi
* Govind Namdeo - Roop Sahai

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Jungle Mein Sher"
| Alka Yagnik, Vinod Rathod
|-
| 2
| "Dil Dene Ki Ruth"
| Alka Yagnik, Vinod Rathod
|-
| 3
| "Is Duniya Men Prem Granth"
| Alka Yagnik, Vinod Rathod
|-
| 4
| "Bajoo Bandh"
| Alka Yagnik, Suresh Wadkar
|-
| 5
| "Main Kamjor Aurat"
| Lata Mangeshkar
|-
| 6
| "Teri Kasam Main Hoon"
| Vinod Rathod
|}

==References==
 

== External links ==
*  

 

 
 
 
 
 
 