Sumangali (1971 film)
{{Infobox film
| name           = Sumangali
| image          =
| caption        =
| director       = MK Ramu
| producer       = PS Veerappa
| writer         = Swamy Jagathy NK Achari (dialogues)
| screenplay     = Jagathy NK Achari
| starring       = Sheela Prasad Kamalam Sankaradi
| music          = R. K. Shekhar
| cinematography = CJ Mohan
| editing        = SA Murukesh
| studio         = PSV Pictures
| distributor    = PSV Pictures
| released       =  
| country        = India Malayalam
}}
 1971 Cinema Indian Malayalam Malayalam film, directed by MK Ramu and produced by PS Veerappa. The film stars Sheela, Prasad, Kamalam and Sankaradi in lead roles. The film had musical score by R. K. Shekhar.   

==Cast==
*Sheela
*Prasad
*Kamalam
*Sankaradi
*T. R. Omana
*Paul Vengola
*Bahadoor Khadeeja
*ML Saraswathi

==Soundtrack==
The music was composed by R. K. Shekhar and lyrics was written by Sreekumaran Thampi. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Maanmizhikaladanju || P Jayachandran || Sreekumaran Thampi || 
|-
| 2 || Neelakkarimbinte || S Janaki, P Jayachandran || Sreekumaran Thampi || 
|-
| 3 || Nishaageethamaay Ozhuki || S Janaki, Chorus || Sreekumaran Thampi || 
|-
| 4 || Pulakamunthiri Poovanamo || K. J. Yesudas || Sreekumaran Thampi || 
|-
| 5 || Ushasso Sandhyayo || K. J. Yesudas || Sreekumaran Thampi || 
|}

==References==
 

==External links==
*  

 
 
 

 