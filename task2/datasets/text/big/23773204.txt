Mr. Justice Raffles (film)
{{Infobox film
| name           = Mr. Justice Raffles
| image          = 
| image_size     = 
| caption        = 
| director       = Gaston Quiribet
| producer       = 
| writer         = E.W. Hornung (novel)   Blanche McIntosh 
| narrator       = 
| starring       = Gerald Ames   Eileen Dennes   James Carew   Hugh Clifton
| music          = 
| cinematography = 
| editing        = 
| studio         = Hepworth Pictures
| distributor    = Hepworth Pictures 
| released       = 20 September 1921
| runtime        = 
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}} British crime film directed by Gaston Quiribet and starring Gerald Ames, Eileen Dennes and James Carew. 

It was based on the 1909 novel Mr. Justice Raffles by E.W. Hornung featuring his gentleman thief AJ Raffles. The plot changed a number of details from the novel and inserted a romantic interest into the plot which sees Raffles fall in love with Camilla Belsize, while trying to conceal his secret life as a leading cracksman from her. 

==Cast==
* Gerald Ames as A.J. Raffles 
* Eileen Dennes as Camilla Belsize 
* James Carew as Dan Levy 
* Hugh Clifton as Teddy Garland 
* Lyonel Watts as Bunny 
* Gwynne Herbert as Lady Laura Belsize 
* Henry Vibart as Mr. Garland 
* Peggy Patterson as Dolly Fairfield 
* Pino Conti as Foreigner 
* Townsend Whitling as Tough 

==References==
 

==External links==
 
 


 
 
 
 
 
 

 