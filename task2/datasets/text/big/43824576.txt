Lumière (film)
 
{{Infobox film
| name           = Lumière
| image          = 
| caption        = 
| director       = Jeanne Moreau
| producer       = Claire Duval
| starring       = Lucia Bosè Francine Racette Keith Carradine Jeanne Moreau François Simon (actor)|François Simon Bruno Ganz Niels Arestrup Francis Huster
| screenplay     = Jeanne Moreau
| based on       = 
| music          = Astor Piazzolla
| cinematography = Ricardo Aronovich
| editor         = Albert Jurgenson
| distributor    = Gaumont Film Company
| released       =     
| runtime        = 
| country        = France
| language       = French
| budget         = 
| gross          = $758,647  
}} drama directed by Jeanne Moreau.

==Cast==
* Lucia Bosè as Laura
* Francine Racette as Julienne
* Caroline Cartier as Caroline
* Jeanne Moreau as Sarah
* Keith Carradine as David
* François Simon (actor)|François Simon as Grégoire
* Bruno Ganz as Heinrich Grün
* René Féret as Julien
* Niels Arestrup as Nano
* Francis Huster as Thomas
* Patrice Alexsandre as Pétard
* Jacques Spiesser as Saint-Loup
* Chloé Caillat as Marie
* Marie Henriau as Flora
* Hermine Karagheuz as Camille
* Carole Lange as Carole
* Paul Bisciglia as The Candle

==Accolades==
{| class="wikitable" style="width:99%;"
|-
! Year !! Award !! Category !! Recipient !! Result
|- 1976
| Chicago International Film Festival
| Grand Prize (Best Feature)
| Jeanne Moreau
|  
|-
| Taormina Film Fest
| Golden Charybdis
| Jeanne Moreau
|  
|- 1977
| César Award Best Supporting Actress
| Francine Racette
|  
|-
|}

==References==
 

==External links==
*  

 
 
 
 


 
 