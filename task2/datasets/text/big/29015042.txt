Sree Narayana Guru (film)
{{Infobox film
| name           = Sree Narayana Guru
| image          =
| caption        =
| director       = PA Bakker
| producer       = Kollam Jaffer
| writer         = Dr Pavithran Vaikkom Chandrasekharan Nair (dialogues)
| screenplay     = Dr Pavithran
| starring       = Kanakalatha Master Vaisakh Sree Kumar
| music          = G. Devarajan
| cinematography = Hemachandran
| editing        = Ravi
| studio         = Navabharath Chithralaya
| distributor    = Navabharath Chithralaya
| released       =  
| runtime        = 96 minutes
| country        = India Malayalam
}}
 1986 Cinema Indian Malayalam Malayalam film, directed by PA Bakker and produced by Kollam Jaffer. The film stars Kanakalatha, Master Vaisakh and Sree Kumar in lead roles. The film had musical score by G. Devarajan.    It won the Nargis Dutt Award for Best Feature Film on National Integration.

==Cast==
*Kanakalatha as Narayanans mother
*Master Vaisakh
*Sree Kumar
*Vijayakumari as Narayanans sister
*KArakulam Chandran as Narayanans father

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Kumaranasan, Sreenarayana Guru, Kollam Jaffer and S Ramesan Nair. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aaraayukil || P. Madhuri || Kumaranasan || 
|-
| 2 || Aazhiyum thirayum || P Jayachandran, Chorus || Sreenarayana Guru || 
|-
| 3 || Chenthaar mangum mukham || G. Devarajan || Kumaranasan || 
|-
| 4 || Daivame || P. Madhuri || Sreenarayana Guru || 
|-
| 5 || Jaya Naarayanagurupriye || G. Devarajan || Kumaranasan || 
|-
| 6 || Maathaave pol || G. Devarajan || Kumaranasan || 
|-
| 7 || Mangalame   ||  || Kollam Jaffer || 
|-
| 8 || Mizhimunakondu || M Balamuralikrishna || Sreenarayana Guru || 
|-
| 9 || Shivasankara || P Jayachandran, Chorus || Sreenarayana Guru || 
|-
| 10 || Sree nammalkkanisam || G. Devarajan || Kumaranasan || 
|-
| 11 || Udayakunkumam || M Balamuralikrishna || S Ramesan Nair || 
|-
| 12 || Unnipirannu || P Jayachandran, P. Madhuri || Kollam Jaffer || 
|-
| 13 || Vaazhka vaazhka || Chorus, Dr Dilip || S Ramesan Nair || 
|}

==References==
 

==External links==
*  

 

 
 
 
 
 