Poovarasam Peepee
{{Infobox film
| name           = Poovarasam Peepee
| image          = Poovarasam Peepee.jpg
| caption        = Poster
| director       = Halitha Shameem
| producer       = Manoj Paramahamsa Sujatha Chenthilnathan
| writer         = Halitha Shameem
| starring       = Gaurav Kalai Pravin Kishore Vasanth
| music          = Aruldev
| cinematography = Manoj Paramahamsa
| editing        = Halitha Shameem
| studio         = V Talkies Baby Shoe Productions
| distributor    = SPI Cinemas
| released       =  
| runtime        = 
| country        = India
| language       = Tamil
| budget         = 
| gross          = 
}}

Poovarasam Peepee ( ) is a 2014    Distributed by SPI Cinemas,  it released on May 30, 2014. 

==Cast==
* Gaurav Kalai as Venu Kanna
* PravinKishore as Harish
* Vasanth as Kapil Dev
* Giri Prasath as Karthi
* Varshini as Lollypop
* Agalya
* Kaali Venkat as Magudi 
* Sai Hari as RS Shanmugam
* Sundar as Manjunathan
* Karthik as Muniyan
* Samuthirakani (Cameo appearance) 

==Production==
Halitha Shameem, a graduate in electronic media, had trained for seven years under directors Pushkar-Gayathri, Mysskin and Samuthirakani.   When she decided to make her directorial debut, she said that Poovarasam Peepee was the first story she could think of.  She stated that it was a coming-of-age film   "about a bunch of boys who lead a carefree life and play whole day long, till they are forced to grow up, overnight".  Cinematographer Manoj Paramahamsa, besides agreeing to shoot the film, decided to produce it as well, while Shameem also worked as the editor, colourist and lyricist. 

For the roles of the three boys, Vasanth, Pravin Kishore and Gaurav Kaalai were selected. Barring Vasanth, who had acted in Aaranya Kaandam and Veyyil, the other children were newcomers with no acting experience. While the director met Gaurav Kaalai accidentally when she was scouting for locations around Madurai, Pravin Kishore contacted the team after seeing an advertisement on local cable channels.  The film was shot for over 60 days in and around Pollachi and Dharapuram. 

==Soundtrack==
The films soundtrack was composed by Aruldev. Aruldev stated that he had gone for "ethnic and deep sounds" and that instruments associated with world music like the hand drum and duduk were used for the score.  The composer had also planned to record a musical piece for the film with a quartet from the UK.  The soundtrack album was launched on May 14, 2014. 

{{Track listing
| headline       = Track listing 
| extra_column   = Singer(s)
| music_credits  = no
| lyrics_credits = no
| all_lyrics     = Halitha Shameem
| total_length   = 

| title1 = Gnayiru Dhinangalin (Version 1)
| extra1 = Abhay Jodhpurkar
| length1 = 
| title2 = Angry Birds
| extra2 = Ranjith (singer)|Ranjith, Aajeedh Khalique, Gautham
| length2 = 
| title3 = En Ulagam Karthik
| length3 = 
| title4 = Ko Ko Ko
| extra4 = Aajeedh Khalique, Gautham, Vignesh, Iyshwarya, Swetha, Reagan
| length4 = 
| title5 = Enakkondrum Vaanveli
| extra5 = Aajeedh Khalique
| length5 = 
| title6 = Gnayiru Dhinangalin (Version 2) Karthik
| length6 = 
| title7 = Theme Music
| extra7 = Instrumental
| length7 = 
}}

==Critical reception==
The Times of India gave the film 3 stars out of 5 and called it "a refreshing (and a bit uneven) adventure story", going on to add, "The pace during the first hour is definitely leisurely and less lively it is only with a chase in the second half that the film hits its stride. It isnt exactly too late because once it finds its rhythm, Poovarasam Peepee turns into a thrilling ride with its irresistible David vs Goliath plot".  Rediff gave it 2.5 stars out of 5 and wrote, "The consistency seems to be missing. At times, the boys appear childish and naïve, but some of the dialogues are quite explicit and mature and don’t seem to go with their character. Also, there are several scenes in the story depicting the puppy love between the different characters, which seem totally unnecessary, even ridiculous at times. However, this is a totally different concept and newcomer Halitha Shameem, does deserve credit for weaving an excellent suspense thriller".  

Sify called the film "an enjoyable summer break" and further wrote, "Halitha has done a neat job, backed by green unseen locations and awesome camerawork by Manoj Paramahamsa", although adding that it was "not in the same league as a Pasanga or a Goli Soda, because   has too many sub-plots which are not well etched".  The director herself objected to the Goli Soda comparison, saying "The only thing my film has in common with Goli Soda is the casting of child artistes. Apart from that, there is absolutely no similarity.    Behindwoods.com gave it 3 stars out of 5 and wrote, "big cheers to director, Halitha Shameem! She uses both the information sources and her voice to the fullest. In a name like RSS, in a character like the kid who rents his toy, in a scene where the villain falls in a pothole – she announces that she is here to stay".  Silverscreen.in gave the movie an aggregated rating of 9.1/10 based on 10 different reviews.    Silverscreen also reviewed the movie positively, praising Shameem for "documenting adolescence with unnerving honesty.  

==References==
 

==External links==
*  

 
 
 
 
 