Hello Madras Girl
{{Infobox film
| name           =  Hello Madras Girl
| image          =  Hellomadrasgirl.jpg
| image_size     = 
| alt            = 
| caption        = LP Vinyl Records Cover
| director       = J. Williams
| producer       = J. Williams
 2nd unit director = G.premkumar
| writer         =   Shankar Madhavi Madhavi Rajkumar Sethupathi Mohanlal Poornima Jayaram  
| music          = Gangai Amaran
| cinematography = Venu
| editing        = Sreenivasalu   
| studio         = 
| distributor    = Ambika films
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 1983 Cinema Indian feature directed by Madhavi and Poornima Jayaram. The film also stars Mohanlal in an important role.    

The film also has Priyadarshan, in one of the scenes as an actor.

==Cast== Shankar
*Madhavi Madhavi
*Mohanlal .... Lal
*Rajkumar Sethupathi
*Poornima Jayaram
*Kuthiravattam Pappu
*Priyadarshan .... Man in Restaurant Urvashi

==Soundtrack==
The music was composed by Gangai Amaran and lyrics was written by Poovachal Khader.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aashamsakal noorunooraashamsakal || K. J. Yesudas || Poovachal Khader ||
|-
| 2 || Kandaaloru poovu || S Janaki || Poovachal Khader ||
|-
| 3 || Maduramee darshanam || K. J. Yesudas, SP Shailaja || Poovachal Khader ||
|-
| 4 || Nirvrithi yaamini || Vani Jairam || Poovachal Khader ||
|}

==References==
 

==External links==
*  

 
 
 
 


 
 