Kollura Sri Mookambika
{{Infobox film
| name           = Kollura Sri Mookambika
| image          = 
| caption        = 
| director       = Renuka Sharma
| producer       = Renuka Sharma
| writer         = Chi. Udaya Shankar
| screenplay     = Chi. Udaya Shankar
| narrator       =  Sridhar  Bhavya Vijay Raghavendra Vajramuni
| music          = P. Mahadevan
| cinematography = S. V. Srikanth
| editing        = K. Balu
| distributor    = 
| studio         =Sri Annapoorneshwari Creations
| released       =  
| runtime        = 138 minutes
| country        = India
| language       = Kannada
}} Kannada mythological Sridhar , Bhavya, Vijay Raghavendra and Vajramuni in lead roles. The film is centered on the Goddess Mookambika.
 State Film Awards in Art direction category.

==Cast== Sridhar 
* Bhavya 
* Vijay Raghavendra
* Vajramuni 
* Srijyothi
* Umashree Ramakrishna 
* Mukhyamantri Chandru
* Srinivasa Murthy
* Doddanna
* Thoogudeepa Srinivas 
* Master Anand

==Soundtrack==
{|class="wikitable" 
! Sl No. !! Song Title !! Singer(s) !! Lyrics 
|-
| 1 || Kanasinali Banda || K. S. Chithra || Chi. Udayashankar
|-
| 2 || Sarvamangale || Chorus || Chi. Udaya Shankar
|-
| 3 || Namo Devadevam || S. P. Balasubrahmanyam || Chi. Udaya Shankar
|-
| 4 || Oh Janani || S. P. Balasubrahmanyam || Chi. Udaya Shankar
|-
| 5 || Amma Daye Baradenu || K. S. Chithra || Chi. Udaya Shankar
|-
| 6 || Gedde Yamananu || S. P. Balasubrahmanyam || Chi. Udaya Shankar 
|-
| 7 || Namasthesthu Mahamaye || B. R. Chaya || Adi Shankaracharya
|-
| 8 || Jaya Mangalam || S. P. Balasubrahmanyam || Chi. Udaya Shankar
|-
| 9 || Kodachadri Shikaradalli  || S. P. Balasubrahmanyam || Vijayanarasimha
|-
| 10 || Thillana || Manjula Gururaj || Vijayanarasimha
|-
|}

==References==
 

==External links==
*  

 
 
 
 
 


 

 