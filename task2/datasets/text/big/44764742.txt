The Lost Paradise
{{Infobox film
| name           = The Lost Paradise
| image          = 
| alt            = 
| caption        = 
| director       = J. Searle Dawley
| producer       = 
| writer         = Henry C. DeMille Ludwig Fulda 
| starring       = H. B. Warner Catherine Carter Mark Price Arthur Hoops Rita Stan Amy Summers Phillips Tead
| music          = 
| cinematography = H. Lyman Broening Harry Leslie Keepers 	
| editing        = 	
| studio         = Famous Players Film Company
| distributor    = Paramount Pictures
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          =
}}

The Lost Paradise is a 1914 American  drama film directed by J. Searle Dawley and written by Henry C. DeMille and Ludwig Fulda. The film stars H. B. Warner, Catherine Carter, Mark Price, Arthur Hoops, Rita Stan, Amy Summers and Phillips Tead. The film was released on September 1, 1914, by Paramount Pictures.  

==Plot==
 

== Cast ==
*H. B. Warner as Reuben Warren
*Catherine Carter as Margaret Knowlton
*Mark Price as Andrew Knowlton
*Arthur Hoops as Ralph Standish
*Rita Stan as Nell
*Amy Summers as Cinders
*Phillips Tead as Billy Hopkins
*Trixie Jennery as Kate
*Wellington A. Playter as Schwartz
*Augustus Balfour as Joe Barrett 
*Marcus Moriarity as Old Bensel

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 

 