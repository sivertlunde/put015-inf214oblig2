The Grissom Gang
{{Infobox film |
| name = The Grissom Gang
| image =Grissomgang.jpg
| caption =
| director = Robert Aldrich
| producer = Robert Aldrich
| writer = James Hadley Chase (novel) Leon Griffiths Scott Wilson Tony Musante Robert Lansing Irene Dailey Connie Stevens Wesley Addy Joey Faye Ralph Waite
| music = Gerald Fried
| cinematography = Joseph Biroc
| editing = Michael Luciano Frank J. Urioste
| studio = The Associates & Aldrich Company MGM (2004, DVD)
| released = May 28, 1971
| runtime = 128 minutes
| country = United States English
| budget = $3 million  Alain Silver and James Ursini, Whatever Happened to Robert Aldrich?, Limelight, 1995 p 281 
| gross = $590,000 "ABCs 5 Years of Film Production Profits & Losses", Variety, 31 May 1973 p 3 
}} American period period gangster directed and produced by No Orchids Scott Wilson, Tony Musante, Robert Lansing, Irene Dailey, Connie Stevens, Wesley Addy, Joey Faye and Ralph Waite.

==Plot== Scott Wilson) under its original title, and the central conceit was that the heiress, who felt stifled by her upper-class life-style, fell in love with the abductor and his comparative freedom to live his life on the edge. In this remake, Aldrich and Griffiths reversed this angle; here, she merely strings him along in an attempt to escape. This version was also played more for laughs, in particular the outlandishly deranged behavior of the gang.

== Cast ==
* Kim Darby as Barbara Blandish Scott Wilson as Slim Grissom
* Tony Musante as Eddie Hagan
* Robert Lansing as Dave Fenner
* Irene Dailey as Gladys "Ma" Grissom
* Connie Stevens as Anna Borg
* Wesley Addy as John P. Blandish
* Joey Faye as Woppy
* Ralph Waite as Mace

==Reception==
The film earned $340,000 in North American rentals and $250,000 in other countries. It recorded an overall loss of $3,670,000.  It had admissions of 239,768 in France.   at Box Office Story 

The film holds a 67% approval rating on Rotten Tomatoes.

== Trivia ==

In 2009 Empire Magazine named it #12 in a poll of the 20 Greatest Gangster Movies Youve Never Seen* (*Probably)

==DVD==
The Grissom Gang was released to DVD by MGM Home Video on November 2nd, 2004 as a Region 1 widescreen DVD.

==Notes==
 

==External links==
 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 


 

 