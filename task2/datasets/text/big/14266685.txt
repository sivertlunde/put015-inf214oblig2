Youth of Chopin
{{Infobox Film
 | name = Youth of Chopin
 | director = Aleksander Ford
 | producer = 
 | screenplay = Aleksander Ford
 | starring =Czesław Wołłejko Aleksandra Śląska
 | released = 25 March 1952
 | runtime = 121 minutes
 | country = Poland Polish
}} 1952 Cinema Polish film directed by Aleksander Ford. 

==Plot==
A story of Chopins life between 1825 and 1830.

==Main cast==
* Czesław Wołłejko as Fryderyk Chopin
* Aleksandra Śląska as Konstancja Gładkowska
* Jan Kurnakowicz as Józef Elsner
* Tadeusz Białoszczyński as Joachim Lelewel
* Gustaw Buszyński as Adam Jerzy Czartoryski
* Igor Śmiałowski as Tytus Woyciechowski
* Jerzy Kaliszewski as Maurycy Mochnacki
* Justyna Kreczmarowa
* Emil Karewicz
* Jerzy Duszyński
* Leon Pietraszkiewicz
* Seweryn Butrym

==References==
 

== External links ==
*  

 
 

 
 
 
 
 
 
 
 

 