Cast a Giant Shadow
{{Infobox film
|  name           = Cast a Giant Shadow
|  image          = Cast_original.jpg
|  caption        = film poster by Howard Terpning
|  director       = Melville Shavelson
|  producer       = Melville Shavelson
|  writer         = Ted Berkman (book) Melville Shavelson (screenplay)
|  starring       = Kirk Douglas Senta Berger Stathis Giallelis James Donald Yul Brynner Frank Sinatra John Wayne Angie Dickinson Chaim Topol Michael Hordern
|  cinematography = Aldo Tonti
|  editing        = Bert Bates Gene Ruggiero
|  music          = Elmer Bernstein
|  studio         = Batjac Productions Bryna Productions
|  distributor    = United Artists
|  released       = March 30, 1966
|  runtime        = 146 mins.
|  country        = United States
|  language       = English
| gross = $3.5 million (est. US/ Canada rentals) 
|}}
Cast a Giant Shadow is a 1966 big-budget action film  based on the life of Colonel Mickey Marcus, and stars Kirk Douglas, Senta Berger, Yul Brynner, John Wayne, Frank Sinatra and Angie Dickinson.  Melville Shavelson adapted, produced and directed. 

==Plot== David "Mickey" Marcus, who commanded units of the fledgling Israel Defense Forces during the 1948 Arab-Israeli War.

Marcus, a reserve Colonel in the Judge Advocate General Corps of the US Army, recently released from active duty and now working as a New York lawyer, is approached by an Haganah agent, Major Safir, played by James Donald, while on his way to court. Safir requests his assistance in preparing Israeli troops to defend the newly declared state against the invasion of its Arab neighbors.

Marcus, still an Army Reserve officer, is refused permission by the Pentagon to go, unless he travels as a civilian. The Haganah gives him a false passport with the alias "Michael Stone". As "Michael Stone", he arrives in Israel to be met by a Haganah member, Magda Simon, whom he immediately starts flirting with.

Marcus, who parachuted into occupied France during World War II, helped to organize the relief mission for one of the first Nazi concentration camp liberated by American troops, is initially viewed with suspicion by some Haganah soldiers. But after he leads a commando raid on an Arab arms dump and assists in a landing of illegal refugees, he is more accepted. He prepares his training manuals and then returns to New York, where his wife has suffered a miscarriage.
 Burma Road," besieged Jerusalem, where the population is on the verge of starvation.

Many of the soldiers under his command are newly arrived in Israel, determined and enthusiastic but untrained. Dubbing them the schnooks, Marcus is inspired by them to discover that he is proud to be a Jew. But just before the convoy of trucks to Jerusalem starts out, he is shot and killed by a lone sentry who does not speak English - the last casualty before the United Nations-imposed truce. The coffin containing his body is ceremonially carried by the soldiers whom he trained and inspired.

Cameo roles (listed as Special Appearances Cast) include:
* John Wayne as The General, Marcuss commanding officer at Normandy and now a senior general officer at the Pentagon, who initially refused him permission to go, but later supports him.

* Yul Brynner as Asher, a Haganah commander.

* Frank Sinatra as Vince Talmadge, an expatriate American pilot who takes part in what becomes a suicide mission to bomb Arab positions.

== Cast == David "Mickey" Marcus
* Angie Dickinson as Emma Marcus
* Senta Berger as Magda Simon
* James Donald as Maj. Safir
* Yul Brynner as Asher
* Stathis Giallelis as Ram Oren
* John Wayne as The General Mike Randolph
* Frank Sinatra as Vince Talmadge
* Luther Adler as Jacob Zion 
* Michael Shillo as Andre Simon  Ruth White as Mrs. Chaison
* Rina Ganor as Rona  Gordon Jackson as McAffee Topol as Abou Ibn Kader
* Michael Douglas as Jeep Driver (uncredited)
* Allan Cuthbertson as Immigration Officer 
* Michael Hordern as British Ambassador 
* Jeremy Kemp as Senior British Officer  Sean Barrett as Junior British Officer 
* Roland Bartrop as Bert Harrison 
* Robert Gardett as General Walsh 
* Michael Balston as 1st Sentry
* Robert Ross as Aide to Chief of Staff 
* Rod Dana as Aide to Gen. Randolph
* Dan Sturkie as Jump Sergeant 
* Hillel Rave as Yaakov 
* Shlomo Hermon as Yussuff 
* Arthur Hansel as Pentagon Officer 
* Claude Aliotti  as 2nd Sentry 
* Micha Shagrir as Truck Driver
* Frank Latimore as 1st U.N. Officer
* Ken Buckle as 2nd U.N. Officer
* Vera Dolen as Mrs. Martinson (scenes deleted)
* Gary Merrill as Pentagon Chief of Staff (scenes deleted)  Geoffrey Palmer as David (uncredited)
* Danny Perlman as Jeep Driver (uncredited) 
* Jimmy Shaw as Jeep Driver (uncredited) 
* Samra Dedes as Belly Dancer

==Trivia==
Winston Churchill, Rudolf Hess, Fiorello LaGuardia, Franklin D. Roosevelt and Joseph Stalin appear as themselves in archive footage in flashbacks

==Production notes==
The film includes a toast scene where John Wayne says L’chaim.  Footage from this film was later used in a Coors Light commercial.

==Home media==
It was released on DVD in early 2000s but went out of print. It was Re-released on DVD, and the 1st time on Blu-ray August 26th, 2014.

==See also==
*John Wayne filmography

==References==
 

==Further reading==
*Shavelson, Melville. How to Make a Jewish Movie, 1971. (ISBN 0-491-00156-8).

==External links==
 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 