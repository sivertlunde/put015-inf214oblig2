Follow Your Star
{{Infobox film
| name =  Follow Your Star
| image =
| image_size =
| caption =
| director = Sinclair Hill
| producer = Harcourt Templeman      George Pearson
| narrator = Mark Daly   Horace Hodges
| music = Bretton Byrd 
| cinematography = Cyril Bristow 
| editing = 
| studio = Belgrave Films
| distributor = General Film Distributors
| released = 1938
| runtime = 80 minutes
| country = United Kingdom English 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} Mark Daly. It was made at Pinewood Studios. 

==Cast==
*    Arthur Tracy as Arthur Tee  
* Belle Chrystall as Mary   Mark Daly as Shorty  
* Horace Hodges as Mr. Wilmot  
* Nina Boucicault as Mrs. Tee  
* James Harcourt as Mr. Tee  
* Dick Tubb as Freddy 
* Finlay Currie as Maxie  

==References==
 

==Bibliography==
* Low, Rachael. Filmmaking in 1930s Britain. George Allen & Unwin, 1985.
* Wood, Linda. British Films, 1927-1939. British Film Institute, 1986.

==External links==
* 

 

 
 
 
 
 
 
 
 

 