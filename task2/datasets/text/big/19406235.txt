A Lingering Face
{{Infobox film
| name           = A Lingering Face
| image          = A Lingering Face.jpg
| caption        =
| director       = Lu Xuechang
| producer       = Han Sanping Tong Gang Cao Wei Tian Yuping
| writer         = Lu Xuechang Li Jixian Li Min Ge Yaming He Xi Cheng Lianzhong Shi Xiaojun Gao Xilu
| music          = Nathan McCree Wang Yu
| editing        = Kong Leijin
| distributor    = China Film Group
| released       =  
| runtime        = 94 minutes
| language       = Mandarin
| country        = China
| budget         = 
| film name = {{Film name| jianti         = 非常夏日
| fanti          = 非常夏日
| pinyin         = Fēicháng xiàrì}}
}} 2000 Cinema Chinese film directed by Lu Xuechang. The film is Lus second after 1997s The Making of Steel. Compared to that earlier film, which was plagued with censorship problems, A Lingering Faces production and release was relatively free of problems or obstacles. 

== Plot == Li Min). Waking up from a nap, Haiyang finds the truck parked in the woods and the driver raping Yanzi. Hiding in the undergrowth, Yanzi sees Haiyang (the titular "lingering" face, but does not reveal his hiding position. Terrified, Haiyang runs away.

In Beijing, Haiyang sees a news report of a female corpse found in the woods. Later, while walking in the streets, Haiyang chances upon a woman who looks exactly like Yanzi. She denies it, but the two embark on a friendship which blossoms into romance. Soon, Haiyang will discover what happened on that curious summer day.

== See also == Suzhou River Lunar Eclipse, two contemporary Chinese films that also deal with plots involving the double identities of women.

== References ==
 

== External links ==
*  
*  
*   at the Chinese Film Database

 
 
 
 
 
 
 


 