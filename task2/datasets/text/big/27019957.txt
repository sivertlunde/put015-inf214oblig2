Pulliman (2010 film)
{{Infobox film
| name           = Pulliman
| image          = Pulliman.jpg
| image_size     = 
| caption        = A promotional poster
| director       = Anil K. Nair
| producer       = Antony Paimpalli
| writer         = Anil K. Nair
| narrator       = 
| starring       =  
| music          = Sharreth
| cinematography = Rajaratnam
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}}
Pulliman is a 2010 Malayalam film directed by Anil K. Nair. The movie stars Kalabhavan Mani and Meera Nandan in the lead roles.

== Plot ==
Kunjunni (Kalabhavan Mani) is a youngster in Perimannur village who is loved by all. Radha (Meera Nandan) belongs to a nadodi gang, who has come to the village to sell statuettes of Lord Krishna. Kunjunni and Radha are in love. Radhas father forbids her to marry the orphan. Kunjunni later finds out that he actually belongs to a rich family and he had left home some twenty years back, as his mother (Saranya) was a bit too strict.

== Cast ==
* Kalabhavan Mani
* Biju Menon
* Nedumudi Venu Innocent
* Ravikumar
* Jaffar
* Mamukkoya
* Ponvarnnan
* Prathap Pothan Siddique
* Suraj Venjaramood
* Meera Nandan
* Sharanya
* Shari
* Bindu Panicker
* KPAC Lalitha
* Ambika Mohan

==External links==
* http://www.nowrunning.com/movie/7088/malayalam/pulliman/index.htm
* http://popcorn.oneindia.in/title/1788/pulliman.html
* http://sify.com/movies/malayalam/review.php?id=14938831&ctid=5&cid=2428
* http://movies.rediff.com/review/2010/apr/19/south-malayalam-movie-review-pulliman.htm

 
 
 


 