Vote for Huggett
{{Infobox film
| name           = Vote for Huggett
| image          = 
| image_size     = 
| caption        = 
| director       = Ken Annakin
| producer       = Betty E. Box
| writer         = Allan MacKinnon
| narrator       =  Jack Warner Kathleen Harrison Susan Shaw Petula Clark
| music          = Antony Hopkins
| cinematography = Reginald H. Wyer
| editing        = Gordon Hales
| studio         = 
| distributor    = 
| released       =  
| runtime        = 84 mins
| country        = United Kingdom
| language       = English
}}
 Jack Warner, The Huggetts, after 1948s Here Come the Huggetts. In it, Joe Huggett decides to run as a candidate in the local municipal elections. It was followed later that year by The Huggetts Abroad.

==Synopsis==
After writing a letter to the local newspaper, calling for the construction of a pleasure garden for a new war memorial, Joe Huggett is overwhelmed by the response of the public. However, his call is awkward for a corrupt local counciller who has plans of his own for the space.

==Cast== Jack Warner as Joe Huggett, Father
* Kathleen Harrison as Ethel Huggett, Mother
* Susan Shaw as Susan Huggett
* Petula Clark as Pet Huggett
* David Tomlinson as Harold Hinchley
* Diana Dors as Diana Gowan Peter Hammond as Peter Hawtrey
* Amy Veness as Grandma Huggett
* Hubert Gregg as Maurice Lever
* John Blythe as Gowan
* Anthony Newley as Dudley
* Charles Victor as Mr Hall
* Adrianne Allen as Mrs Hall
* Frederick Piper as Bentley
* Eliot Makeham as Christie
* Clive Morton as Campbell, Huggetts boss
* Norman Shelley as Wilson
* Lyn Evans as Police Sergeant Pike
* Hal Osmond as Fishmonger
* Elizabeth Hunt as Mrs Lever
* Ferdy Mayne as Waiter
* Nellie Bowman - Eccentric Old Lady
* Empsie Bowman as Eccentric Old Lady
* Isa Bowman as Eccentric Old Lady

==External links==
*  

 
 

 
 
 
 

 