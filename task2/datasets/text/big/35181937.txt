Eyes and Ears of God: Video Surveillance of Sudan
{{Infobox film
| name           = Eyes and Ears of God: Video Surveillance of Sudan
| image          = 
| caption        = 
| director       = Tomo Križnar
| producer       = Tomo Križnar Živa Ozmec
| writer         = Tomo Križnar Maja Weiss
| starring       = 
| music          = Igor Leonardi
| cinematography = 
| editing        = Svetlana Dramlič
| distributor    = YouTube
| released       =  
| runtime        = 95 minutes
| country        =  Slovene Arabic Zaghawa Fur Nuba Komo Ganza
| budget         = 
| gross          = 
}}
Eyes and Ears of God: Video Surveillance of Sudan is a 2012 documentary film by Tomo Križnar and Maja Weiss. 

It shows the ethnic Nuba civilians defending themselves with the help of over 400 cameras distributed by himself and Klemen Mihelič, the founder of humanitarian organisation H.O.P.E., to volunteers across the war zones in the Nuba Mountains, Blue Nile, and Darfur, documenting the (North) Sudan militarys war crimes against local populations. 

== See also ==
* Sri Lankas Killing Fields, a 2011 documentary film.
* Darfur Now, a 2007 documentary film
* Nuba Conversations, a 2000 documentary film

== References ==
 

==External links==
*  
*   report from New York Times columnist Nicholas D. Kristof

 
 
 
 
 
 
 


 