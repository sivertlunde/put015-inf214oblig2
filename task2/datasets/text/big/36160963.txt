Sofia's Last Ambulance
{{Infobox film
| name           = Sofias Last Ambulance
| caption        = Official poster
| image          =SofiasLastAmbulancePoster.jpg
| director       = Ilian Metev
| producer       = Ingmar Trost, Siniša Juričić, Ilian Metev, Dimitar Gotchev
| starring       = Krassimir Yordanov Mila Mikhailova Plamen Slavkov
| cinematography = Ilian Metev
| editing        = Ilian Metev, Betina Ip
| sound          = Tom Kirk
| sales agent    = Films Boutique
| released       =  
| runtime        = 75 minutes
| country        = Bulgaria
| language       = Bulgarian
}}

Sofias Last Ambulance (a co-production of Germany, Bulgaria, and Croatia) is a feature-length observational documentary film by Bulgarian director Ilian Metev. The film premiered at the 51st Semaine de la Critique (International Critics Week) at the 2012 Cannes Film Festival,  where it won the inaugural France 4 Visionary Award (France 4 Prix Revelation).  It was the second documentary ever to compete in the sections 51 year history. 

==Synopsis==
 
Sofias Last Ambulance opens on an ordinary working day of Krassi, Mila and Plamen, the paramedic crew on one of Sofias dangerously dwindling fleet of emergency ambulances.  The medical infra-structure is in ruins and the ambulance services is one of the hardest hit. Soon the daily pressure on the team is revealed in a sequence of absurdities.  Unusual for a work about medical services, patients remained tactfully outside of the frame and sensationalism was rigorously avoided. Camera work focused closely on the faces of the ambulance crew to capture changes in their state of mind  in their persistent efforts to work despite lack of means, exhaustion and ineffective bureaucracy.  

==Production==
Research for Sofias Last Ambulance started in 2008. During the research period, Metev met Dr Yordanov, who has been working in the emergency services for 23 years, and was fascinated by his humbleness and industry. Dr Yordanov and his two team mates were responsible for the most critical cases in the capital. The observational documentary was then shot over the period of two years,  with the shooting crew consisting of Metev and sound recordist Tom Kirk planted in the back of the ambulance, trying to be as discrete and invisible as possible. 
The film was a co-produced in association with German television channel Westdeutscher Rundfunk|WDR, German/French channel Arte, American equity fund Impact Partners, Bulgarian National Film Center, the Croatian Audiovisual Centre and the German Film und Medienstiftung NRW. Sales agents are Berlin based Films Boutique.

===Cast===
*Doctor Krassimir Yordanov as himself
*Nurse Mila Mikhailova as herself
*Driver Plamen Slavkov as himself

==Reception==
Sofias Last Ambulance premiered at the 51st International Critics Week in Cannes. The AFP wrote that "after the screening filmmakers and protagonists were in tears. Number of spectators also."  Jay Weissberg of Variety described the film as "rigidly constructed and deeply human" and Julien Gester of Liberation called it a "fine and strong investigation of a state of crisis."

==Social impact==

At the premiere of the film in Sofia, Bulgarian health minister Desislava Atanasova admitted that "Sofias Last Ambulance reflects the reality of things" and pronounced that since the film, two new emergency response units have been opened up in the capital to speed up response times; ambulance crews wages were to be increased by 18%. Protagonist and emergency doctor Krassimir Yordanov criticized that these reforms were insufficient to address the true cause of the problems: chronically low wages that makes medical professionals leave in droves. Despite 25-years experience in the job, his monthly pay is "less than the weekly pay of any colleague in other European Union countries". 

==Selected awards==
{| class="wikitable"
|-
! Festival !! Country !! Category !! Award !! Jury !! Date
|- May 2012
|- Documentary Competition || Best Documentary Feature   ||  Ron Xerxa (US),  José Luis Cienfuegos (Spain), Tizza Covi (Italy), Pamela Jahn (UK), Jiri Konecny (Czech Republic)  || July 2012
|-
| Prizren Dokufest || Kosovo || Documentary Competition || Best Balkan Documentary  || Marta Andreu (Spain), Srdjan Keċa(Serbia), Cinta Peleja (Portugal) || July 2012
|- 13th Mediterranean Film Festival || Bosnia Herzegovina || Feature Film Competition || Grand Prix  || Jovan Marjanović, Doron Tsabari (Israel), Zdenko Jurilj (B&H)|| August 2012
|- Zurich Film Festival || Switzerland || Documentary Competition || Special Mention  || Jessica Yu (USA),Mohamed Al-Daradji (Iraq),Philippe Diaz (France),Janus Metz (Denmark)|| September 2012
|- Mark Cousins (UK),Bakhtiyar Khudoinazarov (Russia)|| October 2012

|- DOK Leipzig|| Germany || International Feature Competition || Silver Dove  || Larry Kardish (USA),Paweł Łoziński (Polonia),Marina Razbezhkina (Russia),Martha Orozco (Mexico),Susanne Schüle (Germany)|| November 2012

|- German Documentary Film Awards|| Germany || Feature Competition || Best Documentary Film  || Michael Verhoeven, Sung-Hyung Cho, Gereon Wetzel, Dr. Heike Hupertz, Wieland Speck, Prof. Heiner Stadler, Hans-Robert Eisenhauer || June 2013
|}

== References ==
 

== External links ==
*  
*  
* 

 
 
 
 
 
 
 