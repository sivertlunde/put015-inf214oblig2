Hank and Mike
{{Infobox film
| name           = Hank and Mike
| image          =
| caption        =
| director       = Matthiew Klinck Pierre Even
| writer         = Paolo Mancini Thomas Michael Chris Klein
| music          = Phil Electric
| cinematography = Glen Keenan
| editing        = Matthiew Klinck
| distributor    =
| released       =  
| runtime        = 86 minutes
| country        = Canada
| language       = English
| budget         = 
| gross          = 
}} Easter Bunnies who get fired and try their hand at an assortment of odd jobs.

The film premiered in 2008 at the NATPE NextGen Film Festival and was slated for general audience release on October 24, 2008 in the United States. The film was released in Canada on March 27, 2009.

==Plot==
Two blue-collar Easter Bunnies get fired and try their hand at an assortment of odd jobs, failing at each. Fighting depression, debt and eventually each other, their lives start to unravel until they realize that without their job they are nothing.

==Cast== Hank
*Paolo Mike
*Chris Chris Klein as Conrad Hubriss
*Joe Mantegna as Mr. Pan
*Maggie Castle as Lena
*Tony Nappo as Stu
*Jane McLean as Brenda
*Cynthia Amsden as Really curious office worker
*Sandra Ardagna as Chic Office Worker
*Andre Arruda as Luiz
*Boyd Banks as Leon
*Ho Chow as Phil
*Jonathan Collard as Keen Co-worker
*Tarah Consoli as Depressed Skinny Girl
*Tim Dorsch as Arty Guy
*Louis Durand as Graham
*Dawne Furey as Athena
*Derek Gilroy as Liam
*Michael Hanrahan as Jenkins
*David Huband as Mr. Sulsky

==Production== TV series Y B Normal?. Shooting began on February 1, 2007 in an abandoned Canadian Tire store in Toronto, Ontario|Toronto, Ontario.

==Release==
The film premiered in 2007 at the Vancouver International Film Festival and was slated for general audience release on October 24, 2008 in the United States. The film was released in Canada on March 27, 2009.
 ]]

==Festivals==
* Karlovy Vary International Film Festival
* CineVegas
* Palm Beach International Film Festival
* Vancouver International Film Festival
* Victoria International Film Festival
* Seattles True Independent Film Festival
* Hoboken International Film Festival
* Canadian Film Festival

==References==
 

==External links==
* 

 
 
 
 
 
 