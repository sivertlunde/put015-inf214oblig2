Happy Ghost IV
 
 
{{Infobox film name          = Happy Ghost IV  image         = Happy Ghost IV Poster.jpg caption       = Original Hong Kong poster. film name traditional   =開心鬼救開心鬼 simplified    =开心鬼救开心鬼 pinyin        = Kai xin gui jiu kai xin gui}} director      = Clifton Ko producer  Raymond Wong      writer        = Raymond Wong  starring      ={{plainlist|*Raymond Wong
*May Lo
*Fennie Yuen
*Charine Chan}} music         = 
|cinematography= Sander Lee  editing       = Kam Ma  studio        = Pak Ming Films Ltd.  Ko Chi Sum Films Co. Ltd. distributor   = Newport Entertainment Co. Ltd. released      =   runtime       = 84 minutes country       = Hong Kong language      = Cantonese budget        =  gross         = HK$ 11,780,725
}}
Happy Ghost IV is a 1990 Hong Kong comedy film directed by Clifton Ko. The film stars Raymond Wong and May Lo.

==Release==
Happy Ghost grossed a total of HK$ 11,780,725. The movie ran in theatres from 30 June 1990 to 2 August 1990. 

==See also== Clifton Ko filmography
*List of Hong Kong films of 1990

==References==
 

==External links==
* 

 

 
 
 
 
 


 