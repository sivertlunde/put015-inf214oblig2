Disturbia (film)
 Disturbia}}
 
{{Infobox film
| name           = Disturbia
| image          = Disturbia.jpg
| caption        = Theatrical release poster
| director       = D. J. Caruso
| producer       = Joe Medjuck E. Bernett Walsh Jackie Marcus Christopher Landon Carl Ellsworth
| story          = Christopher Landon David Morse Sarah Roemer Carrie-Anne Moss
| music          = Geoff Zanelli
| cinematography = Rogier Stoffers
| editing        = Jim Page DreamWorks Pictures Montecito Picture Company
| distributor    = Paramount Pictures
| released       =  
| runtime        = 105 minutes
| country        = United States
| language       = English   Spanish   Japanese
| budget         = $20 million 
| gross          = $117,760,134 http://boxofficemojo.com/movies/?id=disturbia.htm 
}}
 mystery horror thriller film directed by D. J. Caruso. It stars Shia LaBeouf, Sarah Roemer and Carrie-Anne Moss. It was released on April 13, 2007.

==Plot== David Morse), a man who is having an affair with his cleaning maid, the boys next door who occasionally play pranks on Kale, and the new neighbor, Ashley Carlson (Sarah Roemer) who Kale and his best friend Ronnie (Aaron Yoo) spy on swimming in her pool.

Kale and Ronnie begin to research Turner after Kale witnesses several strange occurrences at Turners house and thinks Turner might be a serial killer. Ashley becomes aware of Kale and Ronnies spying, confronts them, and decides to join the pair in investigating Turner. Later that night, Turner brings home a date and serves her wine and dances for Turner while Kale and Ashley watch. After Ashley has to leave for the night, Kale returns to his room and observes a date of Turners in a panicked state, whom seems to be chased by Turner. Kale uses his binoculars to get a better view and accidentally turns on his video camera flash, revealing Kales spying to Turner. After Turner turns off his lights, Kale pans back to Turners house and Turner is in the window looking straight at Kale. After Kale hides, he witnesses Turners date leaving and the next morning, Kale enters his kitchen and is shocked to see Turner and his mom flirting with each other (They ran into each other earlier at the grocery store). Before Turner leaves, he implies threats to Kale that go unnoticed by Julie.

After Kale becomes jealous of Ashley being hit on by other teenage boys at a party that Ashley is hosting later that night, Kale attempts to ruin her party and she goes over to confront him. Kale tells her of his deep feelings for her. Fascinated by what Kale has said, Ashley and Kale begin kissing, and shortly after, they see Turner dragging a heavy bag into his garage with what looks like blood on it. Kale sends Ronnie to break into another car that belongs to Turner with the garage code in it, while Ashley keeps track of Turner at the store. Ronnie manages to get the garage code at the same time Ashley has lost track of Turner after being distracted. Turner stands right in front of Ashleys car as she is driving and breaks into the car. Turner is calm and tells Ashley that all he wants is some privacy, while insinuating that Ashley may be harmed if she continues to follow him.

Later that night, Ronnie realizes that he left his phone in Turners car and attempts to get it back. While in Turners garage with a video camera, Ronnie finds his phone when the garage door suddenly closes. Ronnie runs and hides in Turners house and as Kale attempts to rescue him, his ankle monitor goes off. When the police arrive, Kale informs them that Ronnie is in danger. Hearing the police outside, Turner comes out and allows them to search his garage. Ronnie is nowhere to be found, when Kale suggests they look in the bloody bag. They open it to find a deer that Turner claims he had hit with his car.

When Julie goes to Turners to talk him into not pressing charges, Ronnie reveals himself to be alive and unharmed. While watching Julie and Turner next door at the same time, Kale watches Ronnies videotape and he sees the club girls dead body inside a bag hidden behind an air vent that Ronnie did not see himself. While Julie is at Turners, she turns her back and he knocks her out. Turner goes to Kales house and knocks out Ronnie and after a struggle with Kale, binds and gags him with duct tape. Turner reveals to Kale that he plans to frame Kale for the murders of Ronnie and Julie and make the motive seem like cabin fever. Turner tries to force Kale to write a suicide note to Ashley, but is distracted when Ashley enters and calls for Kale at the same time. They both attack Turner and manage to subdue him. After Ashley frees Kale in his room, Turner breaks down the door and Kale and Ashley escape by jumping out a window into Ashleys pool.

Kale takes a pair of gardening shears and goes to search for his mother in Turners house, while Ashley goes to warn the police. While Kale is searching Turners house, Kale finds a red-hair wig and it is apparent that Turner wore it and dressed in the club girls clothing after killing her, to drive her car away from his house. An officer alerted to Kales bracelet arrives and enters Turners house, only to have Turner break his neck. Kale falls through the floor in Turners basement and lands in a pool of dead bodies in various states of bacterial decay|decay. When he climbs out, he finds his mom when Turner appears and slashes Kale with a knife. Julie stabs Turner in the leg, giving Kale just enough time to kill Turner with the gardening shears. Kale and Julie exit the house as the police arrive. After Kales ankle monitor is removed and he is released from house arrest for "good behavior", the police officer asks Kales mother to pay for the incarceration fee. Kale and Ashley become a couple and Ronnie, sporting a large bruise, but otherwise alive and well, videotapes them kissing, also getting revenge on their annoying neighbour children by telling their mother about their adult television programming.

==Cast==
 
*Shia LaBeouf as Kale Brecht, a 17-year-old high school junior
*Sarah Roemer as Ashley Carlson, Kales neighbor
*Carrie-Anne Moss as Julie Brecht, Kales mother David Morse as Robert "Rob" Turner, Kales neighbor 
*Aaron Yoo as Ronald "Ronnie" Chu, Kales best friend
*Jose Pablo Cantillo as Officer Gutierrez, Señor Gutierrezs cousin
*Matt Craven as Daniel "Dan" Brecht, Kales deceased father
*Viola Davis as Detective Parker
*Rene Rivera as Señor Gutierrez, Kales Spanish teacher

==Production==

===Development===
The script was written in the 1990s and was optioned. The original studio let the option expire after hearing about Christopher Reeves remake of Rear Window. It was not until 2004 that the script was rewritten and sold.
 Straw Dogs, and The Conversation starring Gene Hackman. They also viewed the 1989 romantic film Say Anything... and "mixed all the movies together."    LaBeouf says he spoke to people on house arrest and locked himself in a room with the bracelet to feel what the confinement of house arrest is like.  He commented in an interview, "...its hard. Im not going to say its harder than jail, but its tough. House arrest is hard because everything is available.   The temptation sucks. Thats the torture of it."  Caruso gave him the freedom to improvise whenever necessary to make the dialogue appeal to the current generation.

===Filming===
Filmed on location in the cities of Whittier, California and Pasadena, California. Filming took place from January 6, 2006 to April 29, 2006. The homes of Kale and Mr. Turner, which were supposed to be across from each other, were actually located in two different cities.   

During filming, LaBeouf began a program that saw him gain twenty five pounds of muscle in preparation of his future films Transformers (film)|Transformers and Indiana Jones and the Kingdom of the Crystal Skull. 

According to LaBeouf, David Morse who plays Mr. Turner, did not speak to LaBeouf or any of the other teens while on set. LaBeouf said, "When we finished filming, he was very friendly. But hes a method actor, and as long as we were shooting, he wouldnt say a word to us."

==Reception==

===Box office===
Disturbia was released on April 13 in the United States and opened at #1 in its first week at the box office with $23 million.  Despite a 10 million decrease in its second week, it remained on top of the box office.  In its third week, it held on with $9.1 million.  In its fourth week, it earned $5.7 million and finished second behind the record-breaking Spider-Man 3. 

===Critical response===
 
The film received generally positive reviews with most praise going to LaBeoufs performance. Review aggregation website Rotten Tomatoes gives the film a score of 68% based on reviews from 165 critics, with the consensus that the film is "a tense, subtle thriller with a noteworthy performance from Shia LaBeouf".  On Metacritic, the film had an average score of 62%, based on 28 reviews. 
 two thumbs up" rating from Richard Roeper and A.O. Scott (filling in for Roger Ebert), with Roeper saying, "This is a cool little thriller with big scares and fine performances."  slasher horror film". 
 David Denby of The New Yorker judged the film "a travesty", adding: "The dopiness of it, however, may be an indication not so much of cinematic ineptitude as of the changes in a movie culture that was once devoted to adults and is now rather haplessly and redundantly devoted to kids." 

 
The film won 3 Teen Choice Awards including Choice Movie: Horror/Thriller, Choice Movie: Breakout Male (LaBeouf) and Choice Movie Actor: Horror/Thriller (LaBeouf) .

===Lawsuit===
The Sheldon Abend Revocable Trust filed a lawsuit against Steven Spielberg, DreamWorks, its parent company Viacom, and Universal Studios on September 5, 2008.   The suit alleged that Disturbia infringed on the rights to Cornell Woolrichs 1942 short story "It Had to Be Murder" (the basis for the classic Alfred Hitchcock film Rear Window), and that DreamWorks never bothered to obtain motion picture rights to the intellectual property and evaded compensating the rights holder for the alleged appropriation. (Ownership of the copyright in Woolrichs original story "It Had to Be Murder" and its use as the basis for the movie Rear Window was previously litigated before the United States Supreme Court in Stewart v. Abend, 495 U.S. 207 (1990).)  Contrary to some media reports, it should be noted that the claim was based on the original Woolrich short story, not the movie Rear Window.

This claim was rejected by the U.S. District Court in Abend v. Spielberg, 748 F.Supp.2d 200 (S.D.N.Y. 2010), on the basis that the original Woolrich short story and Disturbia are only similar at a high level of generality and abstraction. "Their similarities derive entirely from unprotectible elements and the total look and feel of the works is so distinct that no reasonable trier of fact could find the works substantially similar within the meaning of copyright law."  Disturbia contained many subplots not in the original short story.  

After the dismissal of the copyright claim in federal court, the Abend Trust filed another lawsuit in California state court against Universal Studios and the Hitchcock Estate on October 28, 2010, for a breach of contract claim based on earlier agreements which allegedly restricted the use of ideas from the original Woolrich short story and the movie Rear Window whether or not the ideas are copyright protectable, that the defendants had entered into with the Abend Trust after the Supreme Courts Stewart v. Abend decision.    

===Home media  ===
The film was released on DVD and HD DVD on August 7, 2007 and on Blu-ray Disc on March 15, 2008.  In the Making of Disturbia section of the DVDs special features section it is revealed that LaBeouf and Morse did not have much contact off-set, so as to make the fight scenes at the end of the movie as realistic as possible.

==Music==

===Soundtrack===
 
Disturbia: Original Motion Picture Soundtrack is a soundtrack to the film of the same name, composed by Geoff Zanelli and released on March 4, 2007 in the United States by Lakeshore Records.   

===Score===
 
Disturbia: Original Motion Picture Score is a score to the film of the same name. It is composed by Geoff Zanelli, conducted by Bruce Fowler and produced by Skip Williamson. It was released on July 10, 2007 in the United States by Lakeshore Records.   

==See also==

* List of films featuring surveillance

==References==
 

==External links==
* 
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 