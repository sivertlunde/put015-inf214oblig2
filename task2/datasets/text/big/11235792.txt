Kali Ghata (1980 film)
{{Infobox Film
| name           = Kali Ghata
| image          = 
| image_size     = 
| caption        = 
| director       = Ved Rahi
| producer       = Ved Rahi Shivalik Pictures
| writer         =
| story          = Ved Rahi
| narrator       = 
| starring       = Shashi Kapoor Rekha Danny Denzongpa
| music          = Laxmikant Pyarelal
| cinematography = Kay Gee
| editing        = Subhash Gupta
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Hindi 
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

Kali Ghata ( ;   film produced and directed by Ved Rahi. This romance and mystery thriller stars Shashi Kapoor, Rekha, Danny Denzongpa, A. K. Hangal, Aruna Irani, Lalita Pawar and Jagdeep. Music in the film is composed by Laxmikant Pyarelal with lyrics written by Anand Bakshi.

==Plot==
Raibahadur Satpal Singh (Nazir Husain) is killed cold-blooded in a remote village. He is survived by his twin daughters Rekha & Rashmi (both played by Rekha). Rekha manages their business with the help of their chief estate manager Deewan (A. K. Hangal) along with his son and factory manager Kishore (Danny). Rashmi and Kishore are in love with each other which displeases Rekha, who sends Rashmi abroad to attend a painting course. On the other hand, Rekha too is disinterested about her marriage. Meanwhile, driving through their estate, Rekhas car breaks down and is helped by Prem (Shashi Kapoor), with a golden hand for such machines. After a few meets, both fall in love with each other. After a few days, Rekha announces her engagement with Prem to the surprise of everyone around. She also wills to assign the entire estate to Rashmi before engagement. During the engagement, when Rekhas Police Inspector friend Vikas shows up to wish the couple for the occasion, Prem is suspiciously found missing. The next evening, the couple go out to spend private time on a house-boat anchored in a lake on their estate. Later in the stormy night, Rekha finds Prem missing from the boat and starts looking for him. While searching, someone pushes her in the lake, but not before Rekha glimpses the shadow of this person. Everyone at the estate is surprised with the news of Rekhas death. Meanwhile, she survives and reaches the house of her friend Pinky (Aruna Irani). Once there, Rekha learns about Rashmis arrival for Rekha funeral, and both plan to investigate the incident. On the other hand, Kishore, Prem and the Police too start their investigation separately to unmask the killer. Meanwhile, when Rashmi (Rekha disguised) meets him, Prem discloses the intense love for Rekha. She finds quite a few on her suspect list. Kishore wills to go to any extent to marry Rashmi. Prem for his suspicious movements. The staff at their estate, like the cook (Jagdeep), house keeper Ambu (Lalita Pawar) et al. The story progresses and after a few thrilling sequences, the murderer is revealed in the climax.

==Cast==
*Shashi Kapoor  as  Prem
*Rekha  as   Rekha/Rashmi (double role)
*A. K. Hangal  as  Deewaan
*Jagdeep  as  Cook
*Nazir Hussain  as  Raibahadur Satpal Singh
*Aruna Irani  as  Pinky
*Danny Denzongpa  as  Kishore
*Lalita Pawar  as  Ambu, House Keeper
*Jankidas
*Pinchoo Kapoor
*Urmila Bhatt
*Hiralal
*Inder Kumar
*M. B. Shetty
*Raj Mehra
*Rajkumar

==Crew==
*Direction &ndash; Ved Rahi
*Story &ndash; Ved Rahi
*Production &ndash; Ved Rahi
*Production Company &ndash; Shivalik Pictures
*Editing &ndash; Subhash Gupta
*Cinematography &ndash; Kay Gee
*Art Direction &ndash; Sudhendu Roy
*Choreography &ndash; Kiran Kumar, Oscar, Robert, Suresh Bhatt, Vijay
*Music Direction &ndash; Laxmikant Pyarelal
*Lyrics &ndash; Anand Bakshi
*Playback &ndash; Asha Bhosle, Hemlata, Lata Mangeshkar, Mohammad Rafi

==Soundtrack==
{{Track listing
| collapsed       =
| headline        =
| extra_column    = Singer(s)
| total_length    =

| all_writing     =
| all_lyrics      =  Anand Bakshi
| all_music       = Laxmikant-Pyarelal

| writing_credits =
| lyrics_credits  =
| music_credits   =

| title1          = Janewalon Ka Gham
| note1           =
| writer1         =
| lyrics1         =
| music1          =
| extra1          = Mohammed Rafi
| length1         = 4:50

| title2          = Kali Ghata Chhai Prem Rut Aai
| note2           =
| writer2         =
| lyrics2         =
| music2          =
| extra2          = Lata Mangeshkar
| length2         = 5:15

| title3          = Kali Ghata Chhai Prem Rut Aai
| note3           =
| writer3         =
| lyrics3         =
| music3          =
| extra3          = Mohammed Rafi
| length3         = 5:10

| title4          = Mohabbat Ek Wada Hai
| note4           =
| writer4         =
| lyrics4         =
| music4          =
| extra4          = Asha Bhosle, Hemalata
| length4         = 8:55

| title5          = Nainon Ki Khidki Dil Ka Jharokha
| note5           =
| writer5         =
| lyrics5         =
| music5          =
| extra5          = Asha Bhosle
| length5         = 5:00

| title6          = O Piya Bahroopiya
| note6           =
| writer6         =
| lyrics6         =
| music6          =
| extra6          = Asha Bhosle
| length6         = 4:40

| title7          = Tu Chali Aa Meri Mehbooba
| note7           =
| writer7         =
| lyrics7         =
| music7          =
| extra7          = Mohammed Rafi, Asha Bhosle
| length7         = 4:55
}}

==References==
 

== External links ==
*  

 
 
 