Legal Eagles
{{Infobox film
| name = Legal Eagles
| image = Legal_eagles.jpg
| caption = Theatrical release poster
| director = Ivan Reitman
| producer = Ivan Reitman Sheldon Kahn
| screenplay = Jim Cash Jack Epps, Jr.
| story = Ivan Reitman Jim Cash Jack Epps, Jr.
| starring = Robert Redford Debra Winger Daryl Hannah Brian Dennehy Terence Stamp Steven Hill
| music = Elmer Bernstein Laszlo Kovacs
| editing = William Gordean Pem Herring Sheldon Kahn
| studio = Northern Lights Entertainment Mirage Universal Pictures
| released =  
| runtime = 116 minutes
| country = United States
| language = English
| budget = $40 million
| gross = $93,151,591
}}
Legal Eagles is a 1986 American legal crime comedy film written and directed by Ivan Reitman, and starring Robert Redford, Debra Winger, and Daryl Hannah. 

==Plot== New York City District Attorneys Office, who is on his way to becoming the new District Attorney. Into his life enters Laura Kelly (Debra Winger), an attorney who is representing Chelsea Deardon (Daryl Hannah). Chelsea is accused of stealing a painting from an art dealer, Victor Taft (Terence Stamp). However, Chelsea claims that the painting is actually hers, as her father made it for her and signed it to her on her 8th birthday 18 years ago - the same day that her father and most of his paintings went up in smoke in a mysterious fire. 

Kelly eventually manages to talk Logan into helping them (after she creates an impromptu press conference at the dinner where Logan is being introduced as the new candidate for District Attorney). However, things turn even more mysterious when Taft suddenly drops all charges against Chelsea and a police detective, Cavanaugh (Brian Dennehy), comes up with proof that the paintings that supposedly were lost in the fire—which were worth millions in insurance—are still in existence. Chelsea also continues coming on to Logan, coming to his apartment being fearful of someone following her. When Logan goes to investigate, he is shot at. Logan agrees to continue trying to protect Chelsea while Kelly continues her investigation into the paintings. After an attempt by Taft to blow up a warehouse with incriminating information—which almost blows Logan and Kelly up as well—Logan and Kelly find evidence of a massive insurance fraud between Taft, Deardon and Joe Brock, a third man who was sent to prison. 

Logan and Kelly go to Tafts apartment and find him murdered and Chelsea hiding in Tafts apartment. When Logan is caught in bed with Chelsea after her being accused of Tafts murder, Logans career as an Assistant District Attorney is finished. Logan reluctantly teams up with Kelly, which is also encouraged by Logans daughter, Jennifer (Jennifer Dundas), who thinks the two make a good couple. Realizing that a sculpture Taft claimed to them was priceless but told Chelsea was worthless, Logan goes to find Cavanaugh while Kelly and Chelsea go to an exposition in honor of Taft to find the sculpture. There, the person they thought was Cavanaugh reveals himself as Joe Brock. Brock forces Kelly and Chelsea to break open the hollow sculpture, grabs the painting inside and cuffs them to a table. He then sets fire to another part of the gallery, forcing an evacuation. Logan, having realized that Cavanaugh was not who he thought he was, races to the gallery. Logan and Brock fight, with Brock eventually falling to his death. Logan finds Kelly and Chelsea, grabs the painting and the three escape from the burning gallery, where Chelsea tearfully reveals the signature "To Chelsea" on the back of the painting. After all charges are dropped against Chelsea, Logans former boss offers to take him back, based on Logans publicity. Logan, however, decides to stay with Kelly, and the two set up an office together.

===Alternate endings===
Legal Eagles has several different endings.  The film opened to mediocre reviews in theaters  and by the time it reached television, defendant Chelsea Deardon, had gone from innocent to guilty.  The alternate endings include her guilty of one murder, innocent, and then guilty of a different murder.
===Tom Logans Speech to the Jury===
*Tom Logan: Ladies and gentlemen, Chelsea Deardon didnt kill Victor Taft. The prosecution has suggested a possible motive, but one based on hearsay, conjecture and circumstantial evidence. Evidence that appears to have some substance, but upon closer examination, will prove to have no relevance whatsoever to this case.
*(Logan stops and looks at the jury)
*Tom Logan: Youre not buying this, are you? Youre not listening to a word Im saying. Yeah? Guess what? I dont blame you. After listening to Mr. Blanchard lay out the prosecutions evidence, even Im convinced my client murdered Victor Taft.
*(There are murmurs of surprise in the courtroom.)
*Tom Logan: After all, if I had found Victor Taft, dead on the floor, and Chelsea Deardons fingerprints on the weapon, there isnt much that would convince me she isnt guilty. Look, lets save ourselves a lot of time. Lets be honest. There are better things we could be doing. Who thinks Chelsea Deardons guilty? 
*(The jurors all raise their hands.)
*Tom Logan: See, my hand is raised also. 
*Tom Logan:  But ladies and gentlemen, we have a problem. We have developed a legal concept in this country to protect ourselves. Its called Presumption of Innocence. This means that we must assume, we must believe in our hearts, that anyone is innocent, until they have been proven guilty after considering all of the evidence.
*(Then the defense continues with its case.)

==Cast==
* Robert Redford - Tom Logan 
* Debra Winger - Laura Kelly 
* Daryl Hannah - Chelsea Deardon 
* Brian Dennehy - Cavanaugh 
* Terence Stamp - Victor Taft
* Steven Hill - Bower
* Christine Baranski - Carol Freeman
* Roscoe Lee Browne - Judge Dawkins
* David Clennon - Blanchard
* John McMartin - Forrester
* Robert Curtis Brown - Roger
* Grant Heslov - Usher
* Sara Botsford - Barbara
* Jennifer Dundas - Jennifer Logan David Hart - Marchek
* James Hurdle - Sebastian Dearden
* Gary Howard Klar - Hit Man (as Gary Klar)
* Christian Clemenson - Clerk Bruce French - Reporter Lynn Hamilton - Doreen
* Brian Doyle-Murray - Shaw
* Burke Byrnes - Second Cop

Over the end credits is played a song Love Touch performed by Rod Stewart.

==Production==
The script was written by Jim Cash and Jack Epps who were represented by CAA. The film was originally meant to be a vehicle for Bill Murray, a CAA client, and was written as a buddy movie. Murray pulled out and then Robert Redford, another CAA client, expressed interest in doing a romantic comedy, so it was rewritten to be a Spencer Tracy-Katharine Hepburn-type movie. It was set up at Universal, run by CAA client Frank Price and directed by Ivan Reitman, another CAA client. Tom Mankiewicz was called in to rewrite the script. 

==Reception==
The film received mixed reviews and holds a 47% on Rotten Tomatoes based on reviews from 15 critics.  
 

With production costs of $40 million, the film was one of the most expensive ever released up to that point. It grossed a total of $49,851,591 in North America and $43,300,000 internationally, totaling $93,151,591 worldwide. 

==References==
 

==External links==
 
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 