Planet of the Apes (1968 film)
 
{{Infobox film
| name          = Planet of the Apes
| image         = PlanetoftheapesPoster.jpg
| caption       = Theatrical release poster
| director      = Franklin J. Schaffner
| producer      = Arthur P. Jacobs
| screenplay    = {{Plain list| Michael Wilson
* Rod Serling
}}
| based on      =  
| starring      = {{Plain list|
* Charlton Heston
* Roddy McDowall Maurice Evans
* Kim Hunter
* James Whitmore James Daly Linda Harrison
}}
| music          = Jerry Goldsmith
| cinematography = Leon Shamroy
| editing        = Hugh S. Fowler
| studio         = APJAC Productions
| distributor    = 20th Century Fox
| released       =  
| runtime        = 112 minutes 
| country        = United States
| language       = English
| budget         = $5.8 million   
| gross          = $33.4 million  }} 
}} Maurice Evans, James Daly Michael Wilson La Planète series of five films made between 1968 and 1973, all produced by Arthur P. Jacobs and released by 20th Century Fox.   
 dominant species and humans are mute creatures wearing animal skins.

The script was originally written by Rod Serling but underwent many rewrites before filming eventually began.  Directors J. Lee Thompson and Blake Edwards were approached, but the films producer Arthur P. Jacobs, upon the recommendation of Charlton Heston, chose Franklin J. Schaffner to direct the film. Schaffners changes included creating a more primitive ape society, instead of the more expensive idea of having futuristic buildings and advanced technology.  Filming took place between May 21–August 10, 1967, in California, Utah and Arizona, with desert sequences shot in and around Lake Powell, Glen Canyon National Recreation Area. The films final "closed" cost was $5.8 million.
 John Chambers, film franchise, David Watson in the role of Cornelius), and also in the television series.

The original series was followed by   by the Library of Congress as being "culturally, historically, or aesthetically significant".

==Plot== hibernation when their spaceship crashes in a lake on an unknown planet after a long near-light speed voyage, during which, due to time dilation, the crew ages only 18 months. As the ship sinks, Taylor finds Stewart dead and her body desiccated. They throw an inflatable raft from the ship and climb down into it; before departing the ship, Taylor notes that the year is AD 3978, approximately two millenia after their departure in 1972. Once ashore, Dodge performs a soil test and pronounces the soil incapable of sustaining life.
 Linda Harrison).

Suddenly, armed, uniformed gorillas on horseback charge through the cornfield, brandishing firearms, snares, and nets. They capture some humans and kill the rest. Dodge is shot in the back and killed. Landon is wounded and rendered unconscious. Taylor is shot in the throat and taken prisoner. The gorillas take Taylor to Ape City, where his life is saved after a blood transfusion administered by two chimpanzees, an animal psychologist Zira (Kim Hunter) and surgeon Galen (Wright King). While his wound is healing, he is unable to speak.

Taylor discovers that the various apes, who can talk and are in control, are in a strict  s are the police, military, hunters and workers; orangutans are administrators, politicians, lawyers and priests; and chimpanzees are intellectuals and scientists. The apes have developed a primitive society based on the beginnings of the human Industrial Era.  They  have rifles, ride horses, have carts, even primitive photography.  Humans, who are believed by the apes to be unable to talk, are considered vermin and are hunted: either killed outright, enslaved, or used in scientific experiments.
 Maurice Evans). Back in his cage, Taylor steals Ziras pencil and notebook and uses it to write the message My name is Taylor. Zira and Cornelius become convinced that Taylor is intelligent, but upon learning of this, Dr. Zaius orders that Taylor be castrated.

Taylor escapes and during his flight through Ape City finds himself in a museum, where Dodges stuffed and eyeless corpse is now on display. When Taylor is recaptured by gorillas, he overcomes his injured throat and roars one of the films most famous lines, "Take your stinking paws off me, you damn dirty ape!"
 James Daly) catatonic and unable to speak.
 . The cliff face of the Point is obscured by the matte painting of the Statue of Liberty.]]
After the tribunal, Dr. Zaius privately threatens to castrate and lobotomize Taylor if he does not tell the truth about where he came from. With help from Ziras socially rebellious nephew Lucius (Lou Wagner), Zira and Cornelius free Taylor and Nova and take them to the Forbidden Zone, a taboo region outside Ape City that has been out of bounds for centuries by Ape law. A year earlier, Cornelius led an expedition into the Forbidden Zone that found a cave containing artifacts of an earlier non-simian (believed to be human) civilization. The group sets out for the cave to answer questions Taylor has about the evolution of the ape world and to prove he is not of that world.

Arriving at the cave, Cornelius is intercepted by Dr. Zaius and his soldiers. Taylor, now armed, holds them off, threatening to shoot them if necessary. Zaius agrees to enter the cave to disprove their theories and to avoid physical harm to Cornelius and Zira. Cornelius displays the remnants of a technologically advanced human society pre-dating simian history. Taylor identifies artifacts such as dentures, eyeglasses, a heart valve and, to the apes astonishment, a talking childrens doll. More soldiers appear and Lucius is overpowered, but Taylor again fends them off.

Dr. Zaius is held hostage so Taylor can escape, but he admits to Taylor that he has always known that a human civilization existed long before apes ruled the planet and that "the Forbidden Zone was once a paradise, your breed made a desert of it… ages ago!" Taylor nonetheless prepares to search for answers, but Dr. Zaius warns him that he may not like what he finds. Once Taylor and Nova have ridden off, Dr. Zaius has the gorillas lay explosives to seal off the cave and destroy the remaining evidence of the human society. He has Zira, Cornelius and Lucius charged with heresy.
 a global thermonuclear war. Taylor falls to his knees in despair and anger, condemning humanity for destroying the world.

==Cast==
 
 
 
*Charlton Heston as George Taylor
 
 
*Roddy McDowall as Cornelius
*Kim Hunter as Zira Maurice Evans as Dr. Zaius
*James Whitmore as President of the Assembly James Daly as Honorious
  Linda Harrison as Nova
 
 
*Robert Gunner as Landon
*Lou Wagner as Lucius
*Woodrow Parfrey as Maximus
*Jeff Burton as Dodge
*Buck Kartalian as Julius
*Norman Burton as Hunt Leader
*Wright King as Dr. Galen Paul Lambert as Minister
 

==Production==

===Origins=== Doctor Dolittle, he managed to convince Fox vice-president Richard D. Zanuck to greenlight Planet of the Apes.   
 props and blacklisted screenwriter Michael Wilson was brought in to rewrite Serlings script and, as suggested by director Franklin J. Schaffner, the ape society was made more primitive as a way of reducing costs. Serlings stylized twist ending was retained, and became one of the most famous movie endings of all time. The exact location and state of decay of the Statue of Liberty  changed over several storyboards. One version depicted the statue buried up to its nose in the middle of a jungle while another depicted the statue in pieces. 
 Linda Harrison, Linda Harrison, Richard Zanuck, its first Planet of Richard Zanuck. Ten Commandments (1956) co-star Heston.
 John Chambers, who designed prosthetic make up in the film,    held training sessions at 20th-century Fox studios, where he mentored other make-up artists of the film. 

===Filming===
  in Glen Canyon.]]  Malibu and Oxnard with cliffs that towered 130 feet above the shore. Reaching the beach on foot was virtually impossible, so cast, crew, film equipment, and even horses had to be lowered in by helicopter.  The remains of the Statue of Liberty were shot in a secluded cove on the far eastern end of Westward Beach, between Zuma Beach and Point Dume in Malibu.  As noted in the documentary Behind the Planet of the Apes,  the special effect shot of the half-buried statue was achieved by seamlessly blending a matte painting with existing cliffs. The shot looking down at Taylor was done from a 70-foot scaffold, angled over a 1/2-scale papier-mache model of the Statue. The actors in Planet of the Apes were so affected by their roles and wardrobe that when not shooting, they automatically segregated themselves with the species they were portraying. 

===Taylors spacecraft===
The spacecraft onscreen is never actually named in the film.  But for the 40th anniversary release of the Blu-ray edition of the film, in the  short-film created for the release called A Public Service Announcement from ANSA, the ship is called "Liberty 1". {{cite book
| last1            = Handley
| first1           = Rich
| year             = 2009
| title            = Timeline of the Planet of the Apes: The Definitive Chronology
| publisher        = Hasslein Books
| publication-date = 
| page             = 11
| isbn             = 9780615253923
}}  The ship had original been called "Immigrant One" in an early draft of the script, and then called "Air Force One" in a test set of Topps Collectible cards, and even dubbed "Icarus" by a fan which caught on some fansites.  {{cite journal
| last         = Key
| first        = Jim
| date         = January 1, 1999
| title        = The Flight of the Icarus
| journal      = Sci-Fi & Fantasy Models International
| publisher    = Next Millennium Publishing Ltd
| issue        = 38
| pages        = 14
}} 

==Reception==

===Critical response===
Planet of the Apes was well received by critics and is widely regarded as a classic film and one of the best films of 1968, applauded for its imagination and its commentary on a possible world gone upside down.    The film holds an 89% "Certified Fresh" rating on the review aggregate website Rotten Tomatoes, based on 47 reviews.  In 2008, the film was selected by Empire Magazine|Empire magazine as one of The 500 Greatest Movies of All Time. 

===Accolades=== Academy Award John Chambers Best Costume Best Original Score for a Motion Picture (not a Musical) (Jerry Goldsmith).  The score is known for its avant-garde compositional techniques, as well as the use of unusual percussion instruments and extended performance techniques, as well as his 12-note music (the violin part using all 12 chromatic notes) to give an eerie, unsettled feel to the planet, mirroring the sense of placelessness. 

;American Film Institute Lists
*AFIs 100 Years...100 Movies—Nominated 
*AFIs 100 Years...100 Thrills—#59
*AFIs 100 Years...100 Heroes and Villains:
**Colonel George Taylor—Nominated Hero 
*AFIs 100 Years...100 Movie Quotes:
**"Get your stinking paws off me, you damned dirty ape."—#66
*AFIs 100 Years of Film Scores—#18
*AFIs 100 Years...100 Movies (10th Anniversary Edition)—Nominated 
*AFIs 10 Top 10—Nominated Science Fiction Film 

==Sequels and reboots==
 
Writer Rod Serling was brought back to work on an outline for a sequel. Serlings outline was ultimately discarded in favor of a story by associate producer Mort Abrahams and writer Paul Dehn, which became the basis for Beneath the Planet of the Apes. 

Planet of the Apes was followed by four sequels:
*Beneath the Planet of the Apes (1970)
*Escape from the Planet of the Apes (1971)
*Conquest of the Planet of the Apes (1972)
*Battle for the Planet of the Apes (1973)

and two short-lived television series: Planet of the Apes (1974)
*Return to the Planet of the Apes (animated) (1975)

Remake/Reboots: Planet of the Apes (2001) The film was "re-imagined" by director Tim Burton. 

*Rise of the Planet of the Apes (2011) A series Reboot (fiction)|reboot, directed by Rupert Wyatt, was released in August 2011 to critical and commercial success. It is the first in a new series of films.  . Lussier, Germain. (April 14, 2011). Collider.com. Retrieved 2011-08-14. 

*Dawn of the Planet of the Apes (2014) The sequel to Rise, directed by Matt Reeves, was released on July 11, 2014.  
 Comic book adaptations of the films were published by Gold Key (1970) and Marvel Comics (b/w magazine 1974-77,  color comic book 1975-76 ). Malibu Comics reprinted the Marvel adaptations when they had the license in the early 1980s. Dark Horse Comics published an adaptation for the 2001 Tim Burton film. Currently Boom! Studios has the licensing rights to Planet of the Apes. Their stories tell the tale of Ape City and its inhabitants before Taylor arrived. However in July 2014, it was announced that Boom! Studios and IDW Publishing will do a crossover between Planet of the Apes and Star Trek the original series. 

===In popular culture===
A parody of the film series titled "The Milking of the Planet That Went Ape" was published in Mad Magazine. It was illustrated by Mort Drucker and written by Arnie Kogen in regular issue #157, March 1973.  , MAD #157 March 1973. 

Numerous parodies and references have appeared in films and other media, including Spaceballs#Spaceball One|Spaceballs, The Simpsons, Futurama, Family Guy, Jay and Silent Bob Strike Back, Mad Men and The Big Bang Theory.

==Gallery==
  The crash of the astronauts ship, later named Icarus, was partially filmed in and around Lake Powell. Horseshoe Bend Horseshoe Bend on the Colorado River, near Page, Arizona, was a part of the Forbidden Zone, through which Taylor, Zira, and Cornelius fled Ape City. Malibu Creek State Park, part of which was formerly the 20th Century Fox Movie Ranch, was the location of the astronauts initial encounter with primitive humans and superior apes, and of Cornelius, Zira and Taylors escape from Ape City. The final Malibu coast.
 

== See also ==

* Apocalyptic and post-apocalyptic fiction, about the film genre, with a list of related films

== Notes and references ==
 

==External links==
 
 
*  
*  
*  
*  
*  
*   An interactive celebration of the Planet of the Apes franchise in sight, sound and motion
*  
* 

 
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 