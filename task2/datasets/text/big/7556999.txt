Frog Dreaming
 
{{Infobox film
| name        = Frog Dreaming| 
| image       = Go Kids DVD Cover.jpg
| caption     = 
| director    = Brian Trenchard-Smith| 
| producer    = Barbi Taylor| Everett De Roche
| writer      = Everett De Roche
| starring    = Henry Thomas Tony Barry Rachel Friend Tamsin West Katy Manning Brian May Brian Kavanagh
| studio      = Middle Reef Productions
| distributor = UAA Films (theatrical) Miramax Films (USA theatrical)
| released    =  
| runtime     = 93 mins
| country     = Australia
| language    = English
| budget      = A$388,000 
| gross       = A $171,000
}}
Frog Dreaming is a 1986 Australian film starring Henry Thomas and directed by Brian Trenchard-Smith who described it as "a charming mystery adventure." 

In the United States the film was named The Quest. In the United Kingdom it was named The Go-Kids.

==Synopsis== American boy, Aboriginal myth about "frog Dreamings" and bunyips. Cody tries to investigate. The occurrences revolve around a small lake where a monster the locals call "Donkegin" supposedly lives. Another myth explored by the children is the story of the Kurdaitcha Man who acts as a sort of Australian version of the Boogey Man as well as a supernatural judge who deals out punishment. The children are told that he punishes any wrongs done according to the laws of the ancient Aboriginees including harm to one another, murder of animals without need for food, and destroying the environment (his appearance being most notable according to myth when white men came). The Kurdaitcha Man supposedly wanders the countryside, specifically at night, and wears shoes made of Emu feathers in order to cover any tracks.

With incidents at the lake increasing, Cody fashions a makeshift diving suit and goes into the lake to search, but he never comes back up. Thinking that he has drowned, the townsfolk drain the lake to recover his body. But what they really find is shocking...

The townspeople find that Cody is trapped inside Donkegin, who raises its head and begins to let out an unusual cry. The cry is reminiscent of old, rusted metal. One of the officials recognizes the shape as lights penetrate the greenery that is covering Donkegin and giving it its monstrous appearance.

They discover that Donkegin is in fact an old Donkey engine or a type of excavator or steam-shovel used in construction work years ago. It is also revealed that several other items have accumulated at the bottom of the lake including a car and a bicycle. The locals manage to get Cody out and to safety and dispel the myth of the monster in the lake. The myth of the Kurdaitcha Man is further explored when Cody believes he sees him in a dream-like state putting the Donkey-Engine back into the lake. The Kurdaitcha Man is seen as an older Aboriginee man with the feather shoes.

The film ends with the mystery unfolded and Cody alongside his friends safe and sound with the Kurdaitcha Man and Donkegin still living and active in their minds.

==Production==
The film was originally directed by Russell Hagg. However the producer and writer were not satisfied with progress and tracked down Brian Trenchard-Smith who had just finished an episode of Five Mile Creek and asked him to take over. Trenchard-Smith liked the script and was interested in working with Henry Thomas, so accepted. Brian Jones,  A Horse for all courses, Cinema Papers, March 1986 p 28 

Scenes from the movie were filmed at the former quarry site of Moorooduc Quarry Flora and Fauna Reserve located in Mount Eliza, Victoria.

==Box office==
Frog Dreaming grossed $171,000 at the box office in Australia. 

==See also==
*Cinema of Australia

==References==
 

==External links==
* 
* 

 

 
 
 
 
 