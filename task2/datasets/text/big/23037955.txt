Absolutely Secret: Girl Torture
{{Infobox film
| name = Absolutely Secret: Girl Torture
| image = Absolutely Secret Girl Torture poster.jpg
| image_size =
| caption = Theatrical poster for Absolutely Secret: Girl Torture (1968)
| director = Kiyoshi Komori aka Haku Komori       
| producer =
| writer =
| narrator =
| starring = Naomi Tani
| music =
| cinematography =
| editing =
| distributor = Tokyo Kōei / Shintōhō
| released = April 1968
| runtime = 80&nbsp;min.
| country = Japan Japanese
| budget =
| gross =
| preceded_by =
| followed_by =
}}
 1968 Japanese Pink film in the ero guro style directed by Kiyoshi Komori aka Haku Komori. The film features future Nikkatsu SM-queen Naomi Tani in a role during the first half of her career, working outside of the large studio system.

==Synopsis==
Set in the Edo Era, the film opens with a group of women being convicted of various crimes. The rest of the film is given to graphic depiction of the tortures the women endure as part of their sentences. 

==Cast==
* Koji Satomi
* Naomi Tani
* Kemi Ichiboshi
* Reiko Ōzuki (大月麗子)
* Midori Hinoki (檜みどり)
* 瀬黒 ユリ
* Kaoru Miya (美矢 かほる)

==Kiyoshi Komori==
Born about 1920, Kiyoshi Komori began directing in 1953, and concentrated on war films and comedies during the first decade of his career. He made his first pink film-- Japan Torture Punishment History—in 1964, and used torture as a central theme in his pink oeuvre from that point on. Absolutely Secret: Girl Torture, along with Snake Lust (1967) is one of Komoris most notorious works. He retired from filmmaking in the mid-1970s. 

==Critical reception== Wakamatsu films." 

==References==
 

==External links==

===English===
*  
*  

===Japanese===
*  

 
 
 
 