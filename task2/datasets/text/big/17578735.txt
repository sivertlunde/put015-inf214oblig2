An Education
{{Infobox film
| name           = An Education
| image          = Education ver3.jpg
| caption        = UK release poster
| director       = Lone Scherfig
| producer       = {{Plainlist|
*Finola Dwyer
*Amanda Posey
}}
| writer         = Nick Hornby
| based on       =  
| starring       = {{Plainlist| 
* Carey Mulligan
* Peter Sarsgaard
* Alfred Molina
* Dominic Cooper
* Rosamund Pike
* Olivia Williams
* Emma Thompson
}}
| music          = Paul Englishby
| cinematography = John de Borman
| editing        = Barney Pilling
| distributor    = {{Plainlist|
* Sony Pictures Classics
* BBC Films
*Endgame Entertainment
}}
| released       =  
| runtime        = 95 minutes
| country        = {{Plainlist|
*United Kingdom
*United States }}
| language       = English
| budget         = $7.5 million   
| gross          = $26,096,852   }}
 Best Actress for Carey Mulligan. 

An Education premiered at the 2009 Sundance Film Festival to critical acclaim.  It screened on 10 September 2009 at the Toronto International Film Festival {{cite web
| title=An Education premiere at the 2009 Toronto International Film Festival
| url=http://www.digitalhit.com/galleries/34/491
| year=2009
| author=Lambert, Christine
| work=DigitalHit.com
| accessdate=2009-12-11
}}  and was featured at the Telluride by the Sea Film Festival in Portsmouth, New Hampshire, USA, on 19 September 2009.  The film was shown on 9 October 2009, at the Mill Valley Film Festival. It was released in the US on 16 October 2009 and in the UK on 30 October 2009.

==Plot==
In 1961 London, Jenny Mellor is a 16-year-old schoolgirl preparing for Oxford University when she meets a charming older man, David Goldman, who pursues her romantically. He takes her to concerts, clubs and fine restaurants, easily charming her parents into approving of the relationship. Later, Jenny discovers that David is a con man who makes money through a variety of shady practices. She is initially shocked but silences her misgivings in the face of Davids charm. Soon, David takes Jenny to Paris as a birthday gift. Jennys parents invite Graham, a boy Jenny knows from Youth Orchestra, to Jennys birthday party but David arrives and Graham goes home. When David proposes marriage, Jenny accepts and leaves school. However, she later discovers David is already married. When she reveals her discovery to David, he drops out of sight. Jenny despairs, feeling she has thrown her life away but with the help of her favourite teacher, resumes her studies and is accepted at Oxford the following year.

==Cast==
* Carey Mulligan as Jenny Mellor 
* Peter Sarsgaard as David Goldman 
* Dominic Cooper as Danny, Davids friend and partner in crime (Orlando Bloom was originally cast in this role but dropped out before shooting began). {{cite news
| url=http://www.comingsoon.net/news/movienews.php?id=43005
| title=Orlando Bloom Drops Out of Education
| date=2008-03-17
| accessdate=2008-05-23
| archiveurl= http://web.archive.org/web/20080522085742/http://www.comingsoon.net/news/movienews.php?id=43005| archivedate= 22 May 2008  | deadurl= no}} 
* Rosamund Pike as Helen, Dannys girlfriend.
* Alfred Molina as Jack Mellor, Jennys father. 
* Cara Seymour as Marjorie Mellor, Jennys mother.
* Emma Thompson as Miss Walters, the headmistress at Jennys school. 
* Olivia Williams as Miss Stubbs, Jennys concerned teacher. 
* Sally Hawkins as Sarah Goldman, Davids wife.  Matthew Beard as Graham, a boy Jenny knows from the Youth Orchestra.
* Ellie Kendrick as Tina, Jennys friend from school. James Norton as a student.
* Beth Rowley as a nightclub singer.

==Production==

===Writing===
 
 . September 13, 2009. Retrieved September 14, 2009.  Although the screenplay involved Hornby writing about a teenage girl, he did not feel it was more challenging than writing any other character: "I think the moment youre writing about somebody whos not exactly you, then the challenge is all equal. I was glad that everyone around me on this movie was a woman so that they could watch me carefully. But I dont remember anyone saying to me, That isnt how women think." 

===Recreating 1961 England=== The Japanese School in Acton, London|Acton, which used to be the site of the girls school called Haberdashers Askes School for Girls.  The area is convincingly arranged to appear as it would have in the 1960s, with the only noticeable exception being the 1990s-era street lighting. There are several other anachronisms, such as a police two-tone horn at a time when bells were still used, the skirt lengths and hairstyles of the schoolgirls, and the fact that St Johns Smith Square was not opened as a concert hall until 1969. The Pentax camera featured in the film (at 1.02.11)   (or similar), which was available at the time.

==Release==
  and Peter Sarsgaard at the New York premiere in October 2009]]

===Critical response===
An Education was released to critical acclaim, with Mulligans performance being raved about. It has a 94% "Fresh" rating and an 8.0 average rating on Rotten Tomatoes, based on 179 critics reviews.    The film has a Metacritic score of 85 for "universal acclaim", based on 34 reviews.    Rotten Tomatoes offers this consensus: "Though the latter part of the film may not appeal to all, An Education is a charming coming-of-age tale powered by the strength of former newcomer Carey Mulligans standout performance." 

===Box office===
An Education grossed £1,633,504  in the UK.  and $US26,096,852 worldwide.   

===Accolades===
 
An Education won the Audience Choice award and the Cinematography award at the 2009 Sundance Film Festival.    Mulligan won a Hollywood Film Festival award for Best Hollywood Breakthrough Performance for a Female. 
It was selected as Sight & Sounds film of the month.
 Best Actress Best Adapted Satellite Awards nominations.  

===Home media===
An Education was released on DVD and Blu-ray Disc|Blu-ray on 30 March 2010. 

==References==
 

==External links==
 
 
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 