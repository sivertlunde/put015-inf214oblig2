Baaz: A Bird in Danger
 
 
{{Infobox film
| name           = Baaz: A Bird in Danger
| image          = Baaz,_A_Bird_in_Danger.jpg
| caption        = Theatrical release poster
| director       = Tinnu Verma
| producer       = Tinnu Verma
| writer         = Shyam Goel
| starring       = Sunil Shetty Karisma Kapoor Jackie Shroff Dino Morea
| music          = Ismail Darbar
| cinematography = Raju Kaygee
| editing        = Keshav Naidu
| distributor    = Kapishek Films
| released       = 7 February 2003
| runtime        = 176 mins
| country        = India
| language       = Hindi
| budget         = 
| gross          =
}}

Baaz: A Bird in Danger is 2003 Hindi thriller movie directed by Tinnu Verma and starring Sunil Shetty, Karisma Kapoor, Jackie Shroff. Other cast members include Aditi Gowitrikar, Dino Morea, Suhasini Mulay and Preeti Jhangiani.

== Synopsis ==

This film revolves around a killer who suffers from a mental disorder which causes him to kill those who he is attracted to. Karishma Kapoor plays a girl from Delhi named Neha Chopra, who gets a new job as an interior designer at Nainital, and moves to lonely neighbourhood to pursue her career. She later falls in love with a man named Raj, who is played by Dino Morea. However, the cops inform her that he may be the killer of many young beautiful girls in the area, and she doubts the cops until something causes her to doubt him instead, so she decides to leave the city. The police officer Sunil Shetty asks her to see the condition of a sister of the killer who witnessed the murder, but is in coma. Seeing her terrible condition, Neha decides to stay and help with the case and acts as if she is in love with Raj. Raj who has lost the love from Neha has to now gain her trust back and save her from the dangers that await her.

== Plot ==
The film opens with the life of a child born with a strange disorder where he tends to destroy anything that fascinates him. He would be looked after by his grandmother.

Then the film moves to Nainital 17 years later, where Neha Chopra (Karisma Kapoor) from Delhi comes to Nainital to work as an interior designer for Jai Singh Debral (Jackie Shroff), the city Mayor who is also awomaniser and attracted to Neha, at one of his secluded and deserted mansions. A serial killer is said to be on the prowl in the city, killing beautiful women ruthlessly and who is always eluding the police, getting away from the law without evidence if at all he gets arrested. Neha soon meets Raj (Dino Morea), a loafer whom everyone in the city seems to avoid as he is the suspected serial killer, but she likes him, and soon both fall in love. The city police commissioner Harshvardhan (Sunil Shetty) and subordinate Preeti Rastogi (Preeti Jhangiani) observe the movements of the couple all overt the city, and soon they call Neha and warn her that Raj is a serial killer and she must help them nab him red handed as she is soon going to be his next victim. Neha refuses to believe, especially because Raj saves her life on one occasion. But later she finds items of murder in Rajs house, and thinks Raj is the killer. Brokenhearted, she then decides to leave Nainital for good, much against Debrals wishes, but Harshvardhan and Preeti convince her to stay back and nab Raj red handed.

Raj goes to meet Neha but sees that she is very dull, knowing that something is wrong. Soon one of the killers victims sister comes out of coma and tells Preeti that she can identify the killer. Preeti informs Harshvardhan, but when they both arrive at the house, the comatose girl dies of shock unexpectedly. Later that night Preeti thinks over everything, and goes to question Harshvardhan. Debral notices her racing in her car somewhere at night. The next day, Preeti is found murdered in Rajs house. Raj is chased by Harshvardhan and the cops till he jumps off a waterfall and seeks shelter in Nehas house.

Debral threatens to Demote Harshvardhan if he does not nab the killer in 24 hours, to which the latter replies assertively. Harshvardhan gives a mobile phone to Neha, telling her that if Raj calls on her, she must dial his number and inform him immediately. Raj does go to Neha to explain his situation but she pushes him off and dials Harshvardhan.  Surprisingly, Harshvardhan does not answer the phone. She then leaves a voice message saying that Raj has come to kill her. Raj hits Neha and she falls unconscious.

Then the movie reaches its climax where Harshvardhan appears on the scene. He takes Raj and the unconscious Neha to a temple where Raj and Neha had been before and chains him there. Here, Harshvardhan changes completely. He tells Raj that he is the serial killer, the child who was neglected always because if his disorder. So he sought to make his own world. It was he who committed all the murders and hid their bodies. He also says that he had to murder Preeti because she had come to know of things told everything she knew to his grandmother, had also stumbled upon his secret hideout, and wouldve spilled the beans. It is also known that Harshvardhan never had any feelings for Preeti at all, though she was madly in love with him and was hoping he would reciprocate. He goes on to say that Neha is his alone, and he will take her. He was the one who also framed Raj by placing the items of murder in his house. He says "Kaam mera, naam tera", meaning that he has recorded Nehas voice message stating that Raj is the killer, and Raj will be convicted. Harshvardhan then takes Neha to his underground enclosure to kill her, where they find bodies of women kept in glass boxes, as though it were a park. He says that nothing can separate them both now.

In the meantime, Debral comes and frees Raj, and both of them attack him at his house. A huge fight ensues between the three of them and suddenly, Harshvardhans grandmother appears on the scene out of nowhere and shoots Harshvardhan.

The film ends with Raj and Neha planning to get married, and Debral giving the deserted house for them to live in.

== Cast ==

* Sunil Shetty as Harshvardan Bhatti
* Karisma Kapoor as Neha Chopra
* Jackie Shroff as Jai Singh Debral, the Mayor
* Dino Morea as Raj Singh
* Preeti Jhangiani as Preeti Rastogi
* Aditi Govitrikar as Winner of the beauty contest
* Razak Khan as Nathuram Nada
* Vivek Shauq as Havaldar Chautala
* Suresh Bhagwat as Tiwari

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|-  style="background:#ccc; text-align:center;"
! # !! Title !! Singer(s)
|-
| 1
| "Chehre Pe"
| Shaan (singer)|Shaan, Sukhwinder Singh, Sadhana Sargam
|-
| 2
| "Aye Subah (Duet)"
| Roop Kumar Rathod, Sadhana Sargam
|-
| 3
| "Aye Subah (Female)"
| Sadhana Sargam
|-
| 4
| "Sannate Mein (Female)"
| Sunidhi Chauhan
|-
| 5
| "Sannate Mein (Male)"
| Kunal Ganjawala, Vijay Prakash, Harmony By, Clinton
|-
| 6
| "Arre Arre Jala Koi"
| Kailash Kher, Sunidhi Chauhan
|}

== References ==
 
Salman Khan and Saif Ali Khan were offered Dino Moreas role But both declined due to busy with other projects.
Abhishek Bachchan was offered Sunil Shettys role but declined due to date issues.
Rani Mukherjee was the first choice of Nehas role

== External links ==
*  

 
 
 
 