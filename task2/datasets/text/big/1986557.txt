Invasion U.S.A. (1952 film)
 
{{Infobox film
| name =Invasion, U.S.A.
| image =Invasion_U.S.A._promo_art.jpg
| caption =Promo material for Invasion U.S.A.
| writer =Robert Smith Franz Schulz
| starring =Gerald Mohr Peggie Castle Dan OHerlihy
| director =Alfred E. Green
| producer =Albert Zugsmith Robert Smith
| distributor =Columbia Pictures
| released =  
| runtime =74 min.
| language =English
| music =Albert Glasser
| awards =
| budget = $127,000 (estimated) 
}}

Invasion, U.S.A. (sometimes Invasion USA) is a 1952 motion picture set during the Cold War and portraying the invasion of the United States by an unnamed Communist enemy meant to be taken as the Soviet Union.

== Plot == television newscaster society woman material wealth lower taxes and dont see the need for industrial support of government. As he swishes the brandy around his snifter, Ohman tells the others that many Americans want safety and security, but do not want to make any sacrifices for it.
 Washington and Oregon. Shipyards in Puget Sound are A-bombed with large casualties.
 Boulder Dam President makes ineffectual broadcasts with inflated claims of counter-attacks to rally the morale of the people. But things are only going to get worse, much worse. And each American talks about how if they could only do everything over again...

== Production background == Slavic accents, "Peoples Army" proclamations, and use of Soviet fighter aircraft (Yakovlev Yak-17|Yak-17s and Mikoyan-Gurevich MiG-15|MiG-15s) and bombers (Tupolev Tu-4|Tu-4, a clone of the American B-29 Superfortress).
 Red Scare material of the time.   
On a philosophical level, the film is also often viewed as humorously (and unintentionally) ironic, as the lesson it communicates encourages citizens to subordinate their individual needs and desires to that of the State in order to combat Communism.

Phyllis Coates, who will be a future Lois Lane actress, Noel Neill, also a future Lois Lane actress, and William Schallert, a B-movie stalwart all have small parts in the film.


Invasion, U.S.A., after its initial success, grossing in $1,200,000  in the USA was shown some on television in the late 1960s, but then was not widely viewed for a long time. In 1994, it was spoofed on the movie-mocking television show Mystery Science Theater 3000.  The short was released on VHS in 1998, then on DVD in 2002.
 Chuck Norris film of the same name.

== Cast ==
*Gerald Mohr (Vince Potter)
*Peggie Castle (Carla Sanford)
*Dan OHerlihy (Mr. Ohman)
*Robert Bice (George Sylvester)
*Tom Kennedy (Tim the Bartender)
*Wade Crosby (Illinois Congressman Arthur V. Harroway)
*Erik Blythe (Ed Mulfory)
*Phyllis Coates (Mrs. Mulfory)
*Aram Katcher (Factory Window Washer)
*Knox Manning (Himself)
*Edward G. Robinson Jr. (Radio Dispatcher)
*Noel Neill (Second Airline Ticket Agent)
*Clarence A. Shoop (Army Major)

== Footnotes ==
 

== External links ==
*   at Conelrad.com
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 