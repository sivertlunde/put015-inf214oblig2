The Girl I Loved
{{Infobox film
| name           = The Girl I Loved
| image          = 
| alt            = 
| caption        =
| director       = Joe De Grasse Charles Ray
| screenplay     = Harry L. Decker Albert Ray Edward Withers 
| based on       =   Charles Ray Patsy Ruth Miller Ramsey Wallace Edythe Chapman William Courtright
| music          = 
| cinematography = George Rizard 
| editing        = 
| studio         = Charles Ray Productions
| distributor    = United Artists
| released       =  
| runtime        = 60 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 drama silent Charles Ray, Patsy Ruth Miller, Ramsey Wallace, Edythe Chapman and William Courtright. The film was released on February 15, 1923, by United Artists.  

==Plot==
 

== Cast == Charles Ray as John Middleton 
*Patsy Ruth Miller as Mary
*Ramsey Wallace as Willie Brown
*Edythe Chapman as Mother Middleton
*William Courtright as Neighbor Silas Gregg
*Charlotte Woods as Betty Short
*Gus Leonard as Neighbor Perkins
*F.B. Phillips as Hired Man
*Lon Poff as Minister 
*Jess Herring as Hiram Lang
*Ruth Bolgiano as Ruth Lang
*Edward Moncrief as The Judge
*George F. Marion as The Judge
*Billie Latimer as A Spinster

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 
 


 