Monsieur Klein
{{Infobox film 
  | name = Monsieur Klein
  | image = Monsieur Klein film.jpg |
  | caption =
  | director = Joseph Losey	
  | producer = Alain Delon		
  | writer = Franco Solinas Fernando Morandi
  | starring = Alain Delon Jeanne Moreau Francine Bergé Juliet Berto Jean Bouise Suzanne Flon
  | music = Egisto Macchi Pierre Porte
  | cinematography = Gerry Fisher
  | editing = Marie Castro-Vasquez Henri Lanoë Michèle Neny
  | distributor =  
  | released = 27 October 1976 (France)
  | runtime = 123 min
  | country = France
  | language = French
  | budget = $3,500,000 gross = 711,752 admissions (France) 
  }}
 French film directed by Joseph Losey, with Alain Delon starring in the title role.

== Synopsis == French Jews roundup of Parisian Jews. He is reunited with Jews who once were his clients as they board boxcars for Germany.

The film offers no clear resolution of its contradictory evidence and blind alleys. According to Vincent Canby, the filmmakers "are not as interested in the workings of the plot as in matters of identity and obsession". 

==Cast==
* Alain Delon - Mr. Klein
* Jeanne Moreau - Florence
* Francine Bergé - Nicole (credited as Francine Berge)
* Juliet Berto - Jeanine
* Jean Bouise - the vendor
* Suzanne Flon - the concierge
* Massimo Girotti - Charles
* Michael Lonsdale - Pierre (credited as Michel Lonsdale)
* Michel Aumont - the civil servant at the  prefecture
* Roland Bertin - the journals editor
* Jean Champion - the coroner Pierre Vernier - a policeman
* Etienne Chicot - a policeman
* Magali Clément - Lola (as Magali Clement)
* Gérard Jugnot - the photographer
* Hermine Karagheuz - the working girl

== Symbolism and allusions == the Vichy regime.
 The Castle, which describes a search for ones own identity by way of getting to know "the other"; The Trial, which sees an accused man become an outlaw of society. 

== Awards ==
The film was nominated for the Palme dOr at the 1976 Cannes Film Festival    but lost to Taxi Driver. However, Monsieur Klein did win the César Award for Best Film while Losey won the César Award for Best Director. Alexandre Trauner won the César Award for Best Production Design, and in addition the film was nominated for Césars in four other categories. 

== References ==
 

== External links ==
*  
*  , an article by Christopher Weedman, at  .

 
 

 
 
 
 
 
 
 
 
 
 