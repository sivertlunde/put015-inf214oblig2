Yellow Canary (film)
 
{{Infobox film
| name           = Yellow Canary
| image          = The-yellow-canary-movie-poster-1943.jpg
| image_size     = 200 px
| caption        = Theatrical poster
| director       = Herbert Wilcox
| producer       = Herbert Wilcox
| writer         = P. M. Bower Miles Malleson DeWitt Bodeen
| narrator       = 
| starring       = Anna Neagle Richard Greene Albert Lieven
| music          = 
| cinematography = 
| editing        = 
| distributor    = RKO Radio Pictures
| released       =  
| runtime        = 95 minutes (UK) (also given as 98 minutes)  84 minutes, edited (U.S.) 
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}} Second World War. Neagle and director/producer Wilcox had collaborated on a number of previous film projects previously. Neagle 1974, p. 119. 

==Plot==
Sally Maitland (Anna Neagle) boards a ship bound from Britain for Canada during World War II. Two of the passengers, Jim Garrick (Richard Greene) and Polish officer Jan Orlock (Albert Lieven), seek her acquaintance despite her long-time, and well-known admiration for Nazi Germany. It soon becomes common knowledge that Jim is in British intelligence. Sally rebuffs his advances, but welcomes Jans attention. 

The ship is stopped in mid-ocean by the heavy cruiser German cruiser Prinz Eugen|Prinz Eugen and a boarding party takes Jim prisoner. To the puzzlement of the ships captain, the cruiser allows the ship to continue on its way. It also turns out that the Germans have captured an imposter, when Jim emerges from hiding.

When they reach the port of Halifax, Nova Scotia, Jan introduces Sally to his mother, Madame Orlock (Lucie Mannheim), an invalid. The two women disagree over their opinions of the Nazis. Later, when Sally tries to break off their relationship, Jan reveals that he is working for the Nazis. 
 devastating accident First World War.

Orlock orders Sally to telephone Jim and tell him that an attempt will be made to sabotage the RMS Queen Mary|Queen Mary, scheduled to sail later that night, and that all available agents should be immediately sent to stop it. Sally shows her true colours when she divulges the actual plot to the British over the telephone. Canadian bombers are then dispatched to blow up the ship. Meanwhile, Jan shoots Sally before Jim can rescue her; fortunately the bullet is stopped by a cigarette case which he gave to her earlier, and she and Jim are married.

==Cast==
As appearing in Yellow Canary, (main roles and screen credits identified):   Turner Classic Movies. Retrieved: 6 January 2013. 
*Anna Neagle as Sally Maitland
*Richard Greene as Lieutenant Commander Jim Garrick
*Albert Lieven as Jan Orlock
*Lucie Mannheim as Madame Orlock
*Nova Pilbeam as Betty Maitland
*George Thorpe as Colonel Charles Hargraves
*Marjorie Fielding as Lady Maitland
*Franklin Dyall as Captain Foster
*Valentine Dyall as officer on German cruiser   
*Margaret Rutherford as Mrs. Towcester
*Aubrey Mallalieu as Reynolds
*Sybille Binder as Madame Orlocks Attendant

==Production==
Although never identified as Unity Mitford, the central character played by Neagle has some obvious similarities to the pro-Nazi British dilettante who had a great deal of notoriety in pre-war times. Pryce-Jones 1977, p. 121.  In production during 1943, while the United Kingdom was still fearful of Nazi spies, Yellow Canary was obviously made as wartime propaganda, with the aim not only of keeping up morale but also of warning the British public to be on their guard.  
 Sandhurst and was commissioned. He was promoted to captain in the 27th Lancers in May 1944. He was relieved from duty in 1942 to appear in the British propaganda films Flying Fortress and Unpublished Story. In 1943, Greene appeared in Yellow Canary while on furlough.  

Although set aboard a ship in the early scenes, the majority of the principal photography for Yellow Canary took place at the massive lots at Denham Film Studios (D&P Studios), located near the village of Denham, Buckinghamshire|Denham, Buckinghamshire. All of the location sequences of Halifax were strictly "B" roll, but did provide a realistic, "atmospheric" look at wartime conditions in the busy Canadian military and civilian port. 

After production had wrapped, Neagle and Wilcox made their professional relationship a personal one as well when they married on 9 August 1943. 

==Reception==
Although the leading actors in Yellow Canary were well received by critics, the convoluted storyline was considered implausible when reviewed by The New York Times.   More recent reviews were more favourable, commenting on the production values and acting as "better-than-average."  

==References==
===Notes===
 

===Bibliography===
 
* Aldgate, Anthony and Jeffrey Richards. Britain Can Take it: British Cinema in the Second World War. Edinburgh: Edinburgh University Press, 2nd Edition, 1994. ISBN 0-7486-0508-8.
* Barr, Charles, ed. All Our Yesterdays: 90 Years of British Cinema. London: British Film Institute, 1986. ISBN 0-85170-179-5.
* Neagle, Anna. Anna Neagle Aays, Theres Always Tomorrow: An Autobiography. London: W. H. Allen Books, 1974. ISBN 978-0-491-01941-5.
* Parish, James and William Leonard. Hollywood Players: The Thirties. New York: Arlington House Publishers, 1976. ISBN 978-0-87000-365-3.
* Pryce-Jones, David. Unity Mitford: An Enquiry into Her Life and the Frivolity of Evil. New York: Dial Press, 1977. ISBN 978-0-8037-8865-7.
* Skinner, James. Growing Up In Wartime Uxbridge. Stroud, UK: Tempus Publishing, 2008. ISBN 978-0-7524-4543-4.
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 