Copernicus (film)
{{Infobox film
| name           = Copernicus
| image          = 
| caption        = 
| director       = Ewa Petelska Czesław Petelski
| producer       = 
| writer         = Jerzy Broszkiewicz Zdzisław Skowroński
| starring       = Andrzej Kopiczyński
| music          = 
| cinematography = Stefan Matyjaszkiewicz
| editing        = 
| distributor    = 
| released       =  
| runtime        = 128 minutes
| country        =  
| language       = Polish
| budget         = 
}} Best Foreign Language Film at the 46th Academy Awards, but was not accepted as a nominee. 

==Cast==
* Andrzej Kopiczyński as Nicolaus Copernicus|Mikołaj Kopernik
* Barbara Wrzesińska as Anna Schilling - cousin
* Czesław Wołłejko as Lukasz Watzenrode - bishop of Warmia
* Andrzej Antkowiak as Andrzej Kopernik
* Klaus-Peter Thiele as Georg Joachim von Lauchen gen. Rhetikus
* Henryk Boukołowski as Cardinal Hipolit dEste
* Hannjo Hasse as Andreas Osiander - editor
* Henryk Borowski as Tiedemann Giese - bishop of Chełmno
* Jadwiga Chojnacka as Thief Kacpers mother
* Aleksander Fogiel as Matz Schilling - Annas father
* Emilia Krakowska as Kacpers wife
* Gustaw Lutkiewicz as Jan Dantyszek - bishop of Warmia
* Leszek Herdegen as Mönch Mattheusz
* Witold Pyrkosz as Prepozyt Płotowski
* Wiktor Sadecki as Wojciech z Brudzewa

==See also==
* List of submissions to the 46th Academy Awards for Best Foreign Language Film
* List of Polish submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 
 
 