Bayaning 3rd World
{{Infobox film 
|  name               = Bayaning 3rd World
|  image              =
|  caption            =
|  writer             =  
|  starring           =  
|  director           = Mike de Leon
|  producer           =
|  music              = Lorrie Ilustre
|  cinematography     = Ding Achacoso
|  editing            = Armando Jarlego
|  distributor        = Cinema Artists Philippines
|  released           = 1999
|  runtime            = Filipino
|  preceded_by        = 
|  followed_by        = 
|  budget             =
|  gross              =
|  country            = Philippines
}}
 Filipino film national hero Spanish colonization period in the country.

==Plot==
Two filmmakers try to create a film venturing on the life of Jose Rizal. Before they do that, they try to investigate on the heroism of the Philippine national hero. Of particular focus is his supposed retraction of his views against the Roman Catholic Church during the Spanish regime in the Philippines which he expressed primarily through his two novels Noli Me Tangere and El Filibusterismo. The investigation was done mainly by "interviewing" key individuals in the life of Rizal such as his mother Teodora Alonso, his siblings Paciano Rizal|Paciano, Trinidad, and Narcisa, his love interest and supposed wife Josephine Bracken, and the Jesuit priest who supposedly witnessed Rizals retraction, Fr. Balaguer. Eventually, the two filmmakers would end up "interviewing" Rizal himself to get to the bottom of the issue.

==Production==
In 1997, director Mike de Leon had been working on a film project on the life of Jose Rizal with Cinemax Films (now known as GMA Films). Along the way, he invited screenwriter Clodualdo Del Mundo Jr. to collaborate with him in the project. Problems during production eventually led to the project being scrapped, though it would later be completed by director Marilou Diaz-Abaya as the 1998 film Jose Rizal (film)|Jose Rizal.   

Despite this, de Leon and Del Mundo decided to proceed with producing an independently financed film on Jose Rizal. The duo tried to conceptualize on a film different from that of the original film project they worked on. De Leon created his first draft of the script in the English language, while Del Mundo would translate the script in the Filipino language and add some revisions.   

==Cast==
* Ricky Davao as Filmmaker # 1
* Cris Villanueva as Filmmaker # 2
* Joel Torre as Jose Rizal
* Daria Ramirez as Teodora Alonzo
* Joonee Gamboa as Paciano
* Rio Locsin as Trinidad
* Cherry Pie Picache as Narcisa
* Lara Fabregas as Josephine Bracken
* Ed Rocha as Fr. Vicente Balaguer
* Bon Vivar as Fr. Jose Villaclara
* Janmar Risen

==Release==

===Film rating===
The film had been rated "A" by the Film Ratings Board in 1999. Members of the FRB who viewed the pre-release screening on November 9, 1999 noted that the film brought hope that the local film industry will still be able to excel as the new millennium unfolded.   

===Reception===
The film has been critically praised for its unique depiction on the life of Jose Rizal. Those who viewed the film at the FRB pre-release screening noted that the film managed to make a difficult subject "accessible, comprehensible and even exciting" and presented itself in a manner that the younger audience could relate to.  Film critic Nestor Torre views the film as an "artistic triumph" and believes that it would generate discussion among its viewers on Rizals status as a hero.   

In 2004, the New York-based film magazine Film Comments included the film in its list of 10 Best Films in Alternative Cinema.   

Despite receiving critical acclaim, it was not able to earn well financially when it was first released to Philippine theaters in a limited run. It was re-released in 2000, but was soon pulled due to its poor performance.   

===Accolades===
The film won six of 13 awards at the 23rd Gawad Urian in 2000, namely Best Picture, Best Director, Best Supporting Actor (Joel Torre), Best Cinematography, Best Sound, and Best Music.  Aside from these, it was also nominated for Best Screenplay, Best Actor (Cris Villanueva), Best Supporting Actor (Ed Rocha), Best Production, and Best Editing.   

The film also got three nominations from the Film Academy of the Philippines, namely Best Picture, Best Supporting Actress (Daria Ramirez), and Best Editor.   

==See also==
* Rizal sa Dapitan - 1997 Filipino film about Jose Rizals exile in Dapitan
* Jose Rizal (film)|Jose Rizal - 1998 Filipino film on the life of Jose Rizal

==References==
 

==External links==
*   at the Internet Movie Database
*   at the Manunuri ng Pelikulang Pilipino
*   at the New York Times

 
 
 
 