Itha Oru Theeram
{{Infobox film 
| name           = Itha Oru Theeram
| image          =
| caption        =
| director       = PG Vishwambharan
| producer       = OM John
| writer         = A. Sheriff
| screenplay     = A. Sheriff
| starring       = Sukumari Jayabharathi Kaviyoor Ponnamma Kuthiravattam Pappu
| music          = KJ Joy
| cinematography = C Ramachandra Menon
| editing        = K Narayanan
| studio         = St.Joseph Cine Arts
| distributor    = St.Joseph Cine Arts
| released       =  
| country        = India Malayalam
}}
 1979 Cinema Indian Malayalam Malayalam film,  directed by PG Vishwambharan and produced by OM John. The film stars Sukumari, Jayabharathi, Kaviyoor Ponnamma and Kuthiravattam Pappu in lead roles. The film had musical score by KJ Joy.   

==Cast==
*Sukumari
*Jayabharathi
*Kaviyoor Ponnamma
*Kuthiravattam Pappu
*MG Soman

==Soundtrack==
The music was composed by KJ Joy and lyrics was written by Yusufali Kechery. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Akkareyikkare || K. J. Yesudas || Yusufali Kechery || 
|-
| 2 || Premamenna kalayil || S Janaki || Yusufali Kechery || 
|-
| 3 || Raajakumaran pandoru || P Jayachandran, Vani Jairam, Chorus || Yusufali Kechery || 
|-
| 4 || Thaalolam kili Raareeram || P Jayachandran, Vani Jairam || Yusufali Kechery || 
|}

==References==
 

==External links==
*  

 
 
 

 