Sagan (film)
{{Infobox Film
  | name = Sagan
  | caption = Theatrical poster
  | director = Diane Kurys
  | producer = Catherine Grandjean  Diane Kurys Cindy Tempez
  | writer = Diane Kurys  Claire Lemaréchal  Martine Moriconi
  | starring = Sylvie Testud Pierre Palmade  Lionel Abelanski  Jeanne Balibar  Arielle Dombasle
  | music = Armand Amar
  | cinematography = Michel Abramowicz
  | editing = Sylvie Gadmer
  | released = June 11, 2008
  | runtime = 117 minutes/180 minutes (2 part version)
  | country = France French 
  }} 2008 French biographical film, directed by Diane Kurys, starring Sylvie Testud as French author Françoise Sagan and Pierre Palmade as a dancer and a society man, Jacques Chazot, who was very well known in France. The film starts in the mid-1950s as Sagan (then still known under her real name Quoirez) closes a publishing deal for her controversial debut novel Bonjour Tristesse.

The film then follows Sagans road to fame, her drug abuse, alcoholism, and gambling, her hedonistic lifestyle spending too much and becoming poor, as well as several complex love affairs with both men and women.

Sagan was released in France on June 11, 2008.

==Synopsis==
In 1958, Françoise Sagan is 23 years old. Her first couple of novels have made her rich and famous. She lives an easy life, filled with sex and debauchery, surrounded by her group of friends. On the 8th of August that year, at the Casino in Deauville, she gambles her last chips on the number 8 and wins 8 million francs, with which, a couple of hours later, she buys the house which she rented, in nearby Honfleur. This makes her the owner of that property, and she vows that she will never leave the place. Why is she, 40 years later, living there all alone, as a recluse? What events made a young promising novelist end up all alone, without the ones she loved, and used, all those years?

Note: This is an English translation of the French synopsis  supplied by the production company.

==Awards and nominations==
*César Awards (France) Best Actress &ndash; Leading Role (Sylvie Testud) Best Actress &ndash; Supporting Role (Jeanne Balibar) Best Costume Design (Nathalie du Roscoat)

==References==
 

== External links ==
* 

 

 
 
 
 
 


 
 