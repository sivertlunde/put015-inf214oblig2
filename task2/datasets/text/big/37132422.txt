Drums of Africa
{{Infobox film
| name           = Drums of Africa
| image          = Jungafpos.jpg
| image_size     =
| caption        = Original film poster James B. Clark
| producer       = Philip N. Krasne Al Zimbalist
| writer         = Robin Estridge
| based on       = story by Arthur Hoerl
| starring       = Frankie Avalon
| music          = Johnny Mandel
| cinematography = Paul C. Vogel
| editing        =
| studio         = MGM
| distributor    =
| released       =  
| runtime        =
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 
 James B. Clark.

==Plot==
Three adventurers fight slave traders in the Congo.
==Cast==
*Frankie Avalon as Brian Ferrers
*Mariette Hartley as Ruth Knight
*Lloyd Bochner as David Moore
*Torin Thatcher as Jack Cuortemayn
*Hari Rhodes as Kasongo
*George Sawaya as Arab
*Michael Pate as Viledo
*Ron Whelan as Ship captain
*Peter Mamakos as Chavera
==Production== King Solomons Mines (1950). 
==References==
 
==External links==
*  at IMDB
*  at TCMDB

 
 
 
 