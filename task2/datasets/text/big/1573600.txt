Family Plot
 
 
 
{{Infobox film
| name = Family Plot
| image = Family_plot_movie_poster.jpg
| caption = Original release poster
| director = Alfred Hitchcock
| producer = Alfred Hitchcock ( )
| writer = Ernest Lehman
| based on = The Rainbird Pattern by Victor Canning Barbara Harris William Devane John Williams
| cinematography = Leonard J. South
| editing = J. Terry Williams Universal Pictures
| released =  
| runtime = 121 minutes
| country = United States
| language = English
| budget = $4.5 million Nat Segaloff, Final Cuts: The Last Films of 50 Great Directors, Bear Manor Media 2013 p132-135 
| gross = $13,200,000 
}}
 dark comedy/thriller Barbara Harris and William Devane. The film was screened at the 1976 Cannes Film Festival, but was not entered into the main competition.   

The story involves two couples; one couple are amateur petty criminals, the other couple are smooth professionals. Their lives come into conflict because of a search for a missing heir.

The films title is a pun: "family plot" can refer to an area in a cemetery that has been bought by one family for the burial of its various relatives; in this case it also means a dramatic plot line involving various family members.

==Plot== Barbara Harris), and her boyfriend, George Lumley (Bruce Dern), attempt to locate the nephew of a wealthy and guilt-ridden elderly woman, Julia Rainbird (Cathleen Nesbitt). Julias deceased sister gave the baby boy up for adoption, but Julia now wants to make him her heir, and will pay Blanche $10,000 if the heir, Edward Shoebridge, can be found. George Lumley discovers that Shoebridge is thought to be dead, but he tracks down another criminal, Joseph Maloney (Ed Lauter), who paid for the tombstone over an empty grave.

Shoebridge murdered his adoptive parents, faked his own death and is now a successful jeweler in San Francisco known as Arthur Adamson (William Devane). He and his live-in girlfriend, Fran (Karen Black), kidnap millionaires and dignitaries, returning them in exchange for ransoms in the form of valuable gemstones. Arthur conceals an enormous diamond in plain sight in their crystal chandelier.

Arthur enlists Maloney, who had helped murder his adoptive parents, to kill Blanche and George. Maloney initially refuses to help, but then contacts Blanche and George, telling them to meet him at a café atop a mountain. He cuts the brake line of Blanches car, but the couple manage to survive a dangerous high-speed descent. Maloney then tries to run them over, but another vehicle causes him to lose control and drive off a cliff.

At Maloneys funeral, his wife (Katherine Helmond) confesses to George that Shoebridges name is now Arthur Adamson. George has to go to work, so Blanche tracks down various A. Adamsons in San Francisco, eventually reaching the jewelry store as it closes for the day. Arthurs assistant Mrs. Clay (Edith Atwater) offers to let Blanche leave a note, but Blanche says she is Arthurs friend and asks for his address.
 William Prince), into their car when Blanche rings their doorbell. They attempt to drive out of their garage, but Blanches car is blocking their way. She tells Arthur that his aunt wants to make him her heir. Blanche sees the unconscious bishop, and swears she will not tell, but Arthur drugs her, leaving her in the cellar while they drop the bishop off for ransom.

Searching for Blanche, George finds her car outside Arthur and Frans house, but no-one answers the door. He breaks in and searches for her. Arthur and Fran return home; George hides upstairs. He overhears Arthurs decision to kill Blanche and frame her death as a suicide. George manages to talk to Blanche, who is faking unconsciousness in the open cellar. Arthur and Fran enter to carry Blanche out to the car, but she darts out and George locks the kidnappers in.

Blanche then goes into a "trance", climbs the stairs into the house and halfway up the next stairs, where she points at the huge diamond hidden in the chandelier. Blanche then "wakes" and asks George what she is doing there. He excitedly tells her that she is indeed a real psychic. He calls the police to collect the reward for capturing the kidnappers and finding the jewels. A smiling Blanche looks at the camera and winks.

==Cast==
* Karen Black as Fran
* Bruce Dern as George Lumley Barbara Harris as Blanche Tyler
* William Devane as Arthur Adamson / Edward Shoebridge
* Cathleen Nesbitt as Julia Rainbird
* Ed Lauter as Joseph P. Maloney
* Katherine Helmond as Mrs. Maloney
* Nicholas Colasanto as Constantine
* Edith Atwater as Mrs. Clay
* Warren J. Kemmerling as Grandison William Prince as Bishop Wood
* Marge Redmond as Vera Hannagan

==Production==
The film was adapted for the screen by Ernest Lehman, based on Victor Cannings 1972 novel The Rainbird Pattern. Lehman wanted the film to be sweeping, dark, and dramatic but Hitchcock kept pushing him toward lightness and comedy. Lehmans screenplay earned him a 1977 Edgar Award from the Mystery Writers of America.
 No Bail John Williams. Although Hitchcock eventually had a fine screenplay and pre-production (location scouting and costumes) was at an advanced stage, the film was never made; Hepburn became pregnant and Hitchcock turned to another project, Psycho (1960 film)|Psycho (1960), instead.
 
Hitchcock, who often liked to specify the locales of his films by using on-screen titles or by using recognizable landmarks, deliberately left the storys location unspecific, using sites in both San Francisco and Los Angeles. The chase scene in the movie, which writer Donald Spoto called a spoof on car chases prevalent in films at the time, was filmed on the extensive Universal backlot. The restaurant used in the film was also built on the backlot and was shown on studio tours in 1975.

List of Hitchcock cameo appearances|Hitchcocks signature cameo in Family Plot can be seen 40 minutes into the film. He appears in silhouette through the glass door of the Registrar of Births and Deaths.

Following Family Plot, Hitchcock worked on the script for a projected spy thriller, The Short Night. His declining health prevented the filming of the screenplay, which was published in a book during Hitchcocks last years. Universal chose not to film the script with another director, although it did authorize sequels to Hitchcocks Psycho.
 Dazed and Confused, when characters pass a drive-in movie theater.

==Casting==
  Barbara Harris Golden Globe Award for Best Actress – Musical/Comedy for her performance in this film. 

Hitchcock had earlier worked with Bruce Dern on episodes of Alfred Hitchcock Presents and on Marnie (film)|Marnie (1964), in which he had a brief role in a flashback playing a doomed sailor. 

William Devane was Hitchcocks first choice for the role of nefarious jeweler Arthur Adamson, but Devane was unavailable when the film went into production. Hitchcock finally settled on Roy Thinnes as Adamson and shot several scenes with him. When Devane became available, Hitchcock fired Thinnes and re-shot all of his scenes. Later, Thinnes confronted Hitchcock in a restaurant and asked the director why he was fired. Hitchcock simply looked at Thinnes until the actor left. Some shots of Thinnes as the character (from behind) remain in the film. 
 One Flew Over the Cuckoos Nest. 

==Music== Oscar for Steven Spielbergs Jaws (film)|Jaws. Williams has stated that Hitchcock wanted choir voices for Madame Blanche to make her seem psychic towards the beginning. Williams also stated that Hitchcock was at the scoring sessions most of the time and would often give him suggestions. For the scene in which Maloney suddenly disappears from Adamsons office, Hitchcock suggested that Williams stop the music when the camera cuts to the open window to indicate to the audience that Maloney has left through it.  Hitchcock then went on to say, "Mr. Williams, murder can be fun", when he suggested that he should conduct the music lightly for a darker scene of the film. Williams stated that it was a great privilege, and that he had a wonderful working experience with the director.

The complete soundtrack was not released upon the films release date. Few themes from the film were released on John Williams and Alfred Hitchcock compilation albums. For years afterwards, the original soundtrack was not available, spawning many bootleg copies of the complete scoring sessions of the film over the internet. Finally in 2010, Varèse Sarabande made an official release of the complete Original Motion Picture Soundtrack, 34 years after the films initial release. 

{{Track listing
| collapsed = yes
| headline  = Original Motion Picture Soundtrack
| title1    = The First Séance
| length1   = 5:29
| title2    = Blanches Challenge
| length2   = 2:09
| title3    = The Mystery Woman
| length3   = 3:27
| title4    = The Rescue of Constantine / The Diamond Chandelier
| length4   = 2:21
| title5    = Kitchen Pranks
| length5   = 2:08
| title6    = The Shoebridge Headstone
| length6   = 2:23
| title7    = Maloneys Visit to the Jewelry Store / Maloneys Knife / The Stake Out
| length7   = 4:58
| title8    = The Second Séance
| length8   = 2:20
| title9    = Nothing Held Back
| length9   = 0:37
| title10   = The White Mustang
| length10  = 1:19
| title11   = Blanche and George
| length11  = 1:17
| title12   = Maloneys Exit
| length12  = 2:03
| title13   = Share and Share Alike
| length13  = 0:54
| title14   = The Mondrian Shot / The Revealed Identity
| length14  = 1:47
| title15   = The Search Montage
| length15  = 3:41
| title16   = Blanches Arrival / Blanches Note
| length16  = 2:29
| title17   = Blanche Gets the Needle
| length17  = 3:03
| title18   = Breaking Into the House
| length18  = 5:16
| title19   = The Secret Door / Blanche Wakes Up
| length19  = 1:39
| title20   = End Cast
| length20  = 4:14
| title21   = Family Plot Theme
| length21  = 2:38
| title22   = The Stonecutter
| length22  = 6:35
}}

==Reception==
Family Plot has received positive reviews from critics. The film holds a "fresh" rating on Rotten Tomatoes, with the site reporting that 95% of critics have given the film a positive review, and an average rating of 7 out of 10.  Roger Ebert gave the film three out of four stars, saying of the film, "And its a delight for two contradictory reasons: because its pure Hitchcock, with its meticulous construction and attention to detail, and because its something new for Hitchcock—a macabre comedy, essentially. He doesnt go for shock here, or for violent effects, but for the gradual tightening of a narrative noose.  The film earned $6.5 million in rentals. 

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 