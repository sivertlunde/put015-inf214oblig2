Ekalavyan (film)
{{Infobox film name           = Ekalavyan image          = Ekalavyan malayalam.png director       = Shaji Kailas writer         = Renji Panicker starring  Geetha Maathu(actress)|Maathu Siddique  Vijayaraghavan  Ganesh Kumar   Janardanan   Madhu   Jagathy Sreekumar   Pappu   Devan producer       = Grahalakshmi Productions cinematography = Ravi K. Chandran editing        = L. Bhoominathan distributor    = Kalpaka Release  released       = 1993 runtime        = 153 minutes country = India language  Malayalam
|music          = Rajamani gross          = 
}} Ganesh Kumar, Janardanan, Madhu, Maathu(actress)|Maathu, Jagathy Sreekumar, Pappu, and Devan. Directed by Shaji Kailas and written by Renji Panicker, this film broke many records in collection and successfully completed 150 days in theaters. With the success of this film Suresh Gopi established himself as a lead actor in Malayalam cinema. Bollywood movie Singham Returns (2014) is an inspired remake of this movie.

==Plot== godman with strong international connections also runs a powerful narcotic drug mafia in the state. He also has several connections with in political circles. A good orator, Swami also draws a large number of devotees from abroad and many are slowly turned into addicts. A series of murders at Kovalam beach invites sharp criticism of the govt and the chief minister (Madhu) decides to bring a new head to the state narcotics wing. Madhavan IPS (Suresh Gopi), thus arrives in Kerala from New Delhi, and is assisted by Sharath Chandran (Siddique (actor)|Siddique), a smart CI of Kerala police. Madhavans aggressive way of investigation leads him to the ashram or Swami Amoorthananda, which creates a panic in the state. Swami decides to eliminate Madhavan and also plans to topple the CM by appointing his left arm, Velayudhan (Rajan P. Dev) as the new CM. Mahesh Nair (Devan (actor)|Devan) arrives in Kerala upon the order of Swami. He kills Sarath Chandran, which forces Madhavan to react violently. Madhavan raids the ashram and spoils Swamis plot to cause a series of bomb blasts in the state. In the climax, Madhavan kills Swami and Mahesh Nair, thus saving the state from a series of blasts and communal riots.

==Controversies==
The film, when released, became one of the most controversial films for portraying a godman as villain. The houses of director and script writer were attacked and there were attempts to block the screenings.

==Trivia==
Shaji Kailas, later in an interview had revealed that he had plans of casting Mammooty as the hero in the film. Suresh Gopi was supposed to play the role of Sarath Chandran. But when Renji Panicker, the script writer, approached Mammooty to explain the script, Mammootys response was not positive. So Suresh Gopi was cast as the hero instead. The huge success of Ekalavyan made Suresh Gopi a superstar along with Mammooty and Mohanlal.The performance of Narendra Prasad was highly appreciated. he also grabbed many awards for the best villain. Script Writer Renji Panicker also had appeared in a cameo role in this film.

==Shooting==

The film was shot mostly in and around Kovalam. A large part was also shot in Kozhikode. It was produced by P. V. Gangadharan for Grihalakshmi Productions. The camera was done by Ravi K. Chandran and editing by L.Bhoominathan. Rajamani did the background score and Boban was the art director.

==Cast==
*Suresh Gopi ...  Madhavan I.P.S. Siddique ... Sharath Chandran
*Narendra Prasad ...  Swami Amoorthananda
*Vijayaraghavan (actor)|Vijayaraghavan... Cheradi Skariah
*K. B. Ganesh Kumar ...  Unni Joseph Mulaveedan Geetha ...  Maya Menon Maathu ...  Malu Janardhanan ...  Krishnan Madhu ...  Sreedharan
*Jagathi Sreekumar ...  Achuthan Nair Augustine
*Kuthiravattam Pappu ...  Govindan Kutty Devan ...  Mahesh Nair Chithra ...  Hemambaram
*Rajan P. Dev...  Velayudhan Azeez
*Sadiq
*T.S.Krishnan

==External links==
*  

 
 
 
 