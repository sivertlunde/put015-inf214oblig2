Rasputin the Mad Monk
 
 
 
 
{{Infobox film
| name           = Rasputin, the Mad Monk
| image          = Po rasp f01 leeholics.net.jpeg
| caption        = Original French film poster
| director       = Don Sharp
| producer       = Anthony Nelson Keys
| writer         = Anthony Hinds
| narrator       =  Francis Matthews  Richard Pasco  Suzan Farmer
| music          = Don Banks Michael Reed
| editing        = Roy Hyde Seven Arts
| distributor    = Associated British Picture Corporation|Warner-Pathé Distributors (UK) 20th Century-Fox (US) Lumiere Pictures (1995) (UK) (VHS) Anchor Bay Entertainment & Video Treasures (Both 1997 USA VHS) Warner Home Video (1999 & 2003) (UK) (VHS & DVD) Castle Pictures (UK) (VHS) Studio Canal (2009) (UK) (DVD)
| released       = 6 March 1966 (UK) 6 April 1966 (US)
| runtime        = 91 min. UK
| English
| budget         = ₤100,000 (approx) Marcus Hearn & Alan Barnes, The Hammer Story: The Authorised History of Hammer Films, Titan Books, 2007 p 96 
| preceded_by    = 
| followed_by    = 
}}
 Hammer film mystic who Russian Revolution. Francis Matthews, Prince Yusupovs account of the story. For legal reasons, the character of Yusupov was replaced by Ivan (Matthews). Yusupov was still alive when the film was released, dying on 27 September 1967.

The emphasis is on Rasputins terrifying powers both to work magic and to seduce women.

==Plot== Orthodox bishop rumored connection to Khlysty, an obscure Christian sect which believed that those deliberately committing fornication, then repenting bitterly, would be closer to God). He also claims to have healing powers in his hands, and is unperturbed by the bishops accusation that his power comes from Satan.

Rasputin heads for St. Petersburg, where he forces his way into the home of Dr Zargo (Pasco), from where he begins his campaign to gain influence over the Tsarina (Asherson). He manipulates one of the Tsarinas ladies-in-waiting, Sonia (Shelley), whom he uses to satisfy his voracious sexual appetite and gain access to the Tsarina. He places her in a trance to injure the czars heir Alexei, so that Rasputin can be called to court to heal him. After, this success, he hypnotises the Tsarina to replace her existing doctor with Zargo (who has previously been struck off after a scandal). 

However, Rasputins ruthless pursuit of wealth and prestige, and increasing control over the royal household attracts opposition. Sonias brother, Peter (Landen), enraged by Rasputins seduction of his sister, enlists the help of Ivan to bring about the monks downfall. Peter, in challenging the monk, is horribly scarred by acid thrown in his face, and suffers a lingering death.

Tricking Rasputin into thinking his sister Vanessa (Farmer) is interested in him, Ivan arranges a supposed meeting. However, Zargo has poisoned the wine and chocolates (Rasputin is a glutton), which the Monk starts to consume. and although Rasputin collapses, te poison is not enough to kill him. In the ensuing struggle between the three men, Zargo is stabbed by Rasputin and quickly dies. Ivan manages to throw Rasputin out of the window to his death.

==Production==
*This was filmed back-to-back in 1965 with  , using the same sets at Hammers Bray Studios. Lee, Matthews, Shelley and Farmer appeared in both films. In some markets, it was released on a double feature with The Reptile.
*The original ending had the lifeless Rasputin lying on the ice with his hands held up to his forehead in benediction. However, it was considered controversial for religious reasons, and was removed. Stills of the original ending still exist.
*As a child in the 1920s, Lee had actually met Rasputins killer, Felix Yusupov.
 John Burke as part of his 1967 book The Second Hammer Horror Film Omnibus.

==Cast==
*Christopher Lee as Grigori Rasputin
*Barbara Shelley as Sonia
*Richard Pasco as Dr. Boris Zargo Francis Matthews as Ivan
*Suzan Farmer as Vanessa
*Dinsdale Landen as Peter
*Renée Asherson as Tsarina
*Derek Francis as Innkeeper John Welsh as The Abbot
*Joss Ackland as The Bishop
*Robert Duncan as Tsarvitch
*Alan Tilvern as Patron
*Bryan Marshall as Vassily
*Brian Wilde as Vassilys Father

==References==
 

==External links==
* 
*  at  

 
 
 

 
 
 
 
 
 
 
 
 
 
 