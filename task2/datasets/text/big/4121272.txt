The Dentist 2
 
{{Infobox film
| name = The Dentist 2
| image = The_dentist2_dvd_cover.jpg
| caption = The Dentist 2: Brace yourself  DVD cover.
| director = Brian Yuzna Charles Finch Dennis Paoli Susanne Wright Ralph Martin Clint Howard Linda Hoffman
| producer = Mark Amin Corbin Bernsen Pierre David Noël A. Zanitsch Bruce David Eisen Ken Sanders Robert Lansing Parker
| distributor = Trimark Pictures
| released =  
| runtime = 100 minutes
| country= United States
| language = English
| budget = $1,800,000
}}
The Dentist 2 (also known as The Dentist 2: Brace Yourself) is a 1998 American horror film, and sequel to the 1996 film The Dentist. It was directed by Brian Yuzna. The film stars Corbin Bernsen, Jillian McWhirter, Jeff Doucette, and Susanne Wright.

==Plot== the first film. While talking to the facilitys psychiatrist, he remembers the murders he committed in his own mind, while convincing the doctor that it was another man who did those things. His remorseful story distracts her from seeing him pull a sharpened tool that he stitched into his own leg, and he uses her as a hostage to escape the hospital. Alans ex-wife Brooke is alive despite her missing tongue and inability to speak (She has since had new dental implants put in to replace all the teeth that Alan pulled out in the first film); she hires an investigator to find out where Alan has escaped to, believing that he had been putting away money before he went crazy. Brooke has in her possession some postcards that Alan had left behind, and she believes he is in one of those places.

Alan winds up in the small town of Paradise, Missouri, pretending that he had grown upset at life in the big city. He uses a previously established false identity of Dr. Lawrence "Larry" Caine, and has a bank account where he had been sending the money he skimmed off from his practice to hide from the Internal Revenue Service|IRS. The bank officer Mr. Wilkes introduces Alan to his niece Jamie, hoping that she can rent out her small cottage for "Larry" to live in so she could collect money from it.

Jamie, who physically resembles Brooke, becomes a target of Alan’s affections. When he has problems with a cap on one of his teeth, Alan visits the inept town dentist, Dr. Burns, whom he takes an instant disliking to. Alan threatens Dr. Burns with a golf club, causing him to accidentally fall down the stairs to his death. Mr. Wilkes convinces Alan that he should take over as the new dentist for Paradise; Alan soon resumes his murderous ways with a passing tourist (Clint Howard) who accidentally recognizes him from Los Angeles, California.

As the private detective tracks Alan down to Paradise, Alan learns that Jamie has an old admirer named Robbie, who also is the drywall contractor hired to finish his new office. Alans jealousy causes him to ruin a romantic dinner when it is interrupted by a call from Robbie on her answering machine, despite Jamies insistence that she only thinks of Robbie as a friend from the third grade. Meanwhile, Bev, a teller at the bank, has doubts about "Larry" and finds out his real identity while researching on the computer.

Bev sets up an appointment to tell him she knows the truth, but when she asks too many questions he realizes that she knows something. He goes behind her and sedates her with nitrous oxide. She finds herself duct taped to the dental chair and cries and begs him to let her go. He puts a mouth clamp in her mouth to keep it open and drills her bottom-right molar tooth to the raw nerve as a "lie detector" to find out who else she has told. If she lied he would take a dental hook and painfully wiggle the tooth he drilled. Robbie comes to install some more drywall, and after Bev screams Robbie goes to check on her. Alan and Robbie get into a fight. Alan kills Robbie with a hatchet and turns back to Bev and re-tapes her to the dental chair. He takes a pair of dental pliers and plays a game of "truth or tooth". He asks her what did she tell Jeremy about Washington but he doesnt believe her then pulls out her left front tooth, then he asks a her what did she tell Jamie. He then pulls her left incisor tooth out. Before finishing her off Alan painfully extracts all of her teeth.

Later that night Alan begins to have his obsessive-compulsive visions of germs and decay again after seeing his blood-stained uniform. Suddenly Brooke appears, and begins to seduce him into one of his chairs; just before she can cut his tongue off with a pair of scissors, Jamie knocks her out with an overhead lamp. However, as Jamie is calling the police about Brooke, she spots the bloody hatchet, and opens a closet door to find Robbies and Bevs maimed corpses.

Alan turns on Jamie and a fight ensues, with him chasing her to an upstairs bathroom and finally overpowering her. He takes her to an unfinished room in the office, which in his mind is spotless, germ-free and pure white, with opera music playing, and picks up an electric drill (which in his mind is a dental drill) and tries to drill her teeth. Jamie escapes and hides, until Brooke has revived and she and Jamie trap Alan in a hallway. Brooke lunges to stab him with her scissors, but Jamie inadvertently hits her over the head with a 2x4, killing her.

Alan finds Jamie hiding behind some drywall, and after banter between the two, Jamie fires a nail gun repeatedly and hits him with numerous nails. A stunned Alan walks downstairs into the midst of a surprise welcome party being given to him by the people of Paradise. Alan calmly exits out the front door, leaving the townpeople shocked and Jamie to recover herself from what just happened. Alan drives off into the night with numerous nails embedded in his head and shoulders. He begins to pull them out, using one as a toothpick for his cap which was lost in his fights with Jamie and Brooke, and maniacally laughs repeatedly as he drives down the road in his car.

==Cast==
* Corbin Bernsen as Dr. Lawrence Caine/Dr. Alan Feinstone
* Jillian McWhirter as Jamie Devers
* Jeff Doucette as Jeremy Wilkes
* Susanne Wright as Bev Trotter
* Jim Antonio as Doc Burns
* Lee Dawson as Robbie Mauro
* Wendy Robie as Bernice Ralph Martin as Det. Jenkins
* Clint Howard as Mr. Toothache
* Linda Hoffman as Brooke Sullivan
* Judy Nazemetz as Margaret
* Audra Wise as Shawna
* Mary Coleston as Glenda
* Rae Norman as Dr. Cussler
* Gina-Raye Carter as Sidewalk cafe owner

==Reception==
AllRovi|Allmovies review of the film was negative, writing "this perfunctory sequel repeats those earlier grotesqueries   pretty much shot for shot". 

Brian Yuzna was nominated for Best Film at the 1998 Sitges Film Festival .

==References==
 

==External links==
 
* 
* 

 

 
 
 
 
 
 
 
 
 