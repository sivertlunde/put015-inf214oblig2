Vaanam
 
 
 
{{Infobox film
| name = Vaanam
| image = Vaanam Poster.jpg
| alt =
| caption = Early Promotional Poster Krish
| producer = VTV Ganesh R. Ganesh
| story = Krish
| screenplay = Krish
| writer = S. Gnanagiri  (dialogues)   Silambarasan Bharath Saranya Sonia Agarwal
| music = Yuvan Shankar Raja
| cinematography = Nirav Shah Gnana shekar v s Anthony Gonsalves
| studio = VTV Productions Magic Box Pictures
| distributor = Cloud Nine Movies
| released =  
| runtime = 161 minutes
| country = India
| language = Tamil
}}
 Telugu film Saranya and Anthony Gonsalves, respectively.
 five natural elements— aether (classical element)|aether, air, water, fire and earth  —and illustrates how their fates intertwine on New Years Eve at a hospital in Chennai. The film, jointly produced by Ganesh Janardhanan and R. Ganesh and distributed by Dayanidhi Azhagiris Cloud Nine Movies, was released on 29 April 2011,  and opened to critical acclaim upon release,     and was successful at the Box office.

==Plot==
Cable Raja (Silambarasan Rajendar|Silambarasan) is a cable operator born and raised in a slum area in Chennai. He bemoans his poverty and wishes to become rich by marrying his girlfriend Priya (Jasmin Bhasin). He cons her into believing he is well off, and adopts a well-bred persona in front of her. Raja is always accompanied by his best friend Seenu (Santhanam (actor)|Santhanam) and guided by Bhajan singer Ganesh (VTV Ganesh). When he is asked by Priya to buy high-priced passes to a New Years Eve gala, he finds himself severely short of money. After a failed attempt at chain snatching and crossing paths with the police, he heads to a hospital with theft on his mind.

Bharath Chakravarthy (Bharath) is an aspiring rockstar from Bangalore, who is irresponsible but well-meaning. His mother disapproves of his dreams and wishes him to become an army officer, which he opposes. When his band misses their flight to a live concert because of him, he takes them to Chennai via road. En route, Bharath and his girlfriend Laasya (Vega Tamotia) are attacked by religious fanatics, but are aided by strangers, which causes him to reflect on life. They finally arrive in Chennai, but their vehicle collides with an auto-rickshaw carrying a pregnant woman. Sacrificing their concert, Bharath and Laasya rush her to the hospital.
 Andhra border. When she discovers that her life is constantly in danger while working there, she escapes to Chennai with her co-worker Karpooram (Nikki), a eunuch, in hopes of starting her own business venture. On arrival, she is hounded by both thugs and policemen, ultimately being led into a trap. In an ensuing fight, Karpooram is mortally wounded. Desperate, Saroja carries her to the hospital.

Rahim (Prakash Raj) and his wife Zara (Sonia Agarwal) had lost their unborn twins in a communal riot in Coimbatore. When Zara becomes pregnant again a few months later, Rahim heads to Chennai to locate his younger brother who had run away following the riots. There, he comes into repeated conflict with anti-Muslim officer Shivaram (Ravi Prakash), who suspects him of being a terrorist. Despite his claims of innocence, Rahim is brutally assaulted by the cop, and admitted in the hospital under close watch.
 sell her kidney to obtain the money. After initial hassles, she undergoes the operation and receives the money, although it is still insufficient. At that point, Cable Raja enters and steals the cash from her, ignoring her pleas. In a state of despair, Lakshmi and her father-in-law contemplate suicide.

Struck by his conscience, Raja is unable to continue his plan. He admits the truth to Priya and returns the money, even giving up some of his own money, gaining Lakshmis forgiveness. Meanwhile, Rahim discovers that terrorist leader Mansoor Khan (Jayaprakash) and his gang are planning to kill all the people at the hospital. Rahim tries to escape, but on seeing Bharath wheeling in a pregnant woman, he is reminded of his wife and offers to help them. The terrorist strike begins and several people are shot dead. Raja, Bharath, Rahim and Saroja lead a small bunch of survivors to an abandoned room.
 human bomb, and primes to explode. Cable Raja sacrifices himself by pulling Khan and falling out of the window followed by the explosion.

In the aftermath, Bharath loses his hand but is alive; he is hailed as a national hero. Saroja and Karpooram head toward a new decent life, hoping to find redemption. Lakshmi pays off the money lender and gives her son a proper education. Shivaram apologized to Rahim and asks for forgiveness, which he grants and accepts him as a brother. Cable Raja is mourned by the people in his community, and Priya as well, while Ganesh and Seenu hail him as a martyr.

==Cast== Silambarasan as Thillai Raja alias "Cable" Raja
* Bharath as Bharath Chakravarthy
* Anushka Shetty as Saroja
* Prakash Raj as Rahim
* Saranya Ponvannan as Lakshmi Santhanam as Seenu
* Sonia Agarwal as Zara Rahim
* Vega Tamotia as Laasya
* Jasmin Bhasin as Priya Ganesh as "Bajna" Ganesh
* Radha Ravi as Police Officer
* Jayaprakash as Mansoor Khan
* Ravi Raghavendra
* Ravi Prakash as Shivaram
* Kadhal Thandapani as Narasimman
* Brahmanandam as Bhairavan
* Nikki as Karpooram Krish in a cameo appearance

==Production== Jeeva was Silambarasan secured the project with N. Linguswamy and Dayanidhi Azhagiri, who were unhappy about Silambarasan starting to shoot for this film without informing them and further delaying their film.    

Meanwhile, Anushka was reported to have denied the offer, following which Taapsee Pannu, who was the female lead in Vetrimarans Aadukalam, was considered for the prostitute character, Saroja.  During the launch of the film then, sources claimed that Anushka, following her refusal, had been roped in by the producers for an item number only, which was specially included for the Tamil version and didnt feature in the original, for which she was supposedly paid an "exorbitant price".     However, Anushka dismissed all these reports and eventually confirmed that she would reprise her role from the original, citing that "at no point did I refuse to be a part of this project".  Mohan Babus son Manoj Manchu was also supposed to reprise his original character and enact the rockstar role in Vaanam, but was forced to opt out, following an accident during the shoot of his Telugu film Jhummandi Naadam and had to undergo an operation.  He was replaced by Bharath, who was approached by Silambarasan himself for the role and immediately agreed as he loved  Vedam (film)|Vedam". 
 dubbed for Santhanam after Jagan had fallen out with Silambarasan. 

==Soundtrack==
{{Infobox album 
| Name = Vaanam
| Longtype = to Vaanam
| Type = Soundtrack
| Artist = Yuvan Shankar Raja
| Cover = VaanamFront-01.jpg
| Border = yes
| Alt =
| Caption = Front CD Cover
| Released = 27 November 2010  (Single release)  21 March 2011  (Soundtrack release) 
| Recorded = 2010 – 2011  Prasad Studios  (Chennai)  Feature film soundtrack
| Length = 23:25 Tamil
| Label = Venus Music
| Producer = Yuvan Shankar Raja
| Last album = Kadhal 2 Kalyanam (2011)
| This album = Vaanam (2011)
| Next album = Jolly Boy (2011)
}}
{{Album ratings
| rev1 = Behindwoods.com
| rev1Score =   
| rev2 = Rediff
| rev2Score =   
}} single track to promote and popularise the film. The track was initially planned to be launched in London, but after several complications and delays,  it was officially unveiled on 28 November 2010 at the Citi Center, Chennai,  four months ahead of the actual audio launch.

While the original soundtrack consisted of eight tracks, the Vaanam soundtrack featured only five tracks, including the earlier released single. The entire soundtrack album was finally launched on 21 March 2011 at the Residency Towers in Chennai in a simple manner.   Na. Muthukumar had penned lyrics for three songs, while the other two songs ("Evan Di Unna Pethan" and "Cable Raja") were penned by the singers themselves. Film composer Srikanth Deva had lent his voice for the fifth song, collaborating with Yuvan Shankar for the first time, as did the composer-duo Abhishek-Lawrence.

{{tracklist
| headline = Tracklist
| extra_column = Singer(s)
| total_length = 23:25
| all_lyrics = Na. Muthukumar, except where noted

| title1 = Evan Di Unna Pethan
| extra1 = Silambarasan Rajendar|Silambarasan, Yuvan Shankar Raja
| note1 = Silambarasan, Yuvan Shankar Raja
| length1 = 5:54

| title2 = Vaanam
| extra2 = Yuvan Shankar Raja
| length2 = 3:46

| title3 = Cable Raja
| extra3 = Abhishek, Lawrence
| note3 = Abhishek, Lawrence
| length3 = 4:01

| title4 = Who Am I
| extra4 = Benny Dayal
| length4 = 4:20

| title5 = No Money No Honey
| extra5 = Silambarasan, Andrea Jeremiah, Srikanth Deva
| length5 = 5:24
}}

===Reception===
The soundtrack received mixed reviews from music critics. Richard Mahesh from Behindwoods.com gave a 3/5 rating and said "‘Vaanam’ can be regarded as one of the Yuvan’s better composition and it is sure to make high waves now. While ‘Evandi Unna Pethan’, ‘Vaanam’ and ‘Who Am I’ touches the surpassing degrees on different styles, ‘Cable Raja’ and ‘No Money’ caters to the tastes of mass audiences."    Kollywoodz said "On the whole, ‘Vaanam’ has a power-packed performance by Yuvan Shankar Raja. Sony BMG fetches one more feather to its cap following the huge success of ‘Engeyum Kadhal’ and ‘Ko’."  Pavithra Srinivasan of Rediff gave a 2.5/5 rating and said "Like some of Yuvans most recent albums, Vaanam too, scores in some areas: the number Vaanam and Who am I. The rest fall into the Yuvan template, while the appeal of Evandi Unnai Pethan is almost purely its in-your-face lyrics. For those who hoped for melodious numbers, this one might prove a dampener."    Indiaglitz said "The audio album of Vaanam strikes an instant appeal. While all the songs are beautiful in their own right, Evandi Unna Pethan and No Money are picks of the lot, as both come with that special touch." 

==Release== Sun TV. The film was given a "U/A" certificate by the Indian Censor Board.

===Critical reception===
Vaanam garnered positive response. Behindwoods.com rated it as three and a half out of five, citing that the film was "succulent with diverse complex human emotions from deceit to greed to rage to remorse. The film has a plot that can find patrons among wider variety of audience as the theme of humanity is much a catholic one that transcends barriers", going to describe the film as an "intelligent fare with substance".  Rediffs Pavithra Srinivasan rated it as three out of five and stated that Vaanam was "engaging", further adding that "if you ignore the minor lapses, youve got a reasonably engaging story, and a moving climax."  Sify.coms critic felt the film was "very good", citing that the "this short story genre   manages to work well for the new age audience. Almost all the stories are deftly told, with the mandatory twist in the climax which keeps you riveted". The reviewer further praised the director and his team as they "push the cinematic envelope and bring savvy freshness to Tamil cinema".  A reviewer from Indiaglitz.com wrote that Vaanam was a "novel attempt in Tamil cinema and Krish pulls it off well thanks to a beautifully conceived script and well-written characters", comparing the film to "reading a set of interesting short stories".  A Oneindia critic claimed that "the first half was entertaining and the second half makes the audience to sit on the edge of their seats", while pointing out that "the common and mass audiences could not understand the story because of its clumsy nature". Krish was labelled as the "real hero" of the film and was lauded for "writing a ripping story, excellent screenplay and brilliant direction". 
Malathi Rangarajan from The Hindu said "Pithy, poignant, funny and serious as the situation warrants, dialogue (Gnanagiri) is a highpoint of Vaanam. Climax is another. Krish seems to have cut and pasted a few scenes from the Telugu original – they give a dubbed-film feel to Vaanam. Coming after the stupendous hit, VTV, Vaanam should be another significant film in STRs career." 

In contrast, Rohit Ramachandran of nowrunning.com gave the film one out of five, stating that "Vaanam is an unrealistic film about uninteresting people that deadlocks into blandness."  National Film Award winning critic Baradwaj Rangan wrote that his "glass-half-full side wants to deliver a smallish pat on Vaanam’s back and label it   a "praiseworthy attempt", especially within mainstream parameters", while the "glass-half-empty side" was "still shaking angry fists at what could have and should have been a milestone", criticising that "half the story strands are simply not interesting enough". 

===Box office===
The film collected  9.68 crore in its first day which is the biggest opening day for Simbu till now.The film collected  27.25 crore in its opening weekend.The film   90 lakhs at the Chennai box office in its first weekend,  notably without much publicity. The film collected  45.55 crore in its first week.The film recovered its production cost and publicity in first week itself by share.At the end of its seventh week, the film had collected   4.50 crore in Chennai alone and was declared a hit.  The film crossed 10 weeks in Chennai. The film collected  75.8 crore in its final worldwide collection.The film declared Hit at the box office.

===Accolades=== Santhanam received the Chennai Times Awards and SIIMA Awards for "Best Actor In a Comic Role" and "Best Comedian" respectively.  

==Controversy== Ajith film. Srikanth are Vijay starrer". 

==Dubbed version==
The film was dubbed in Hindi as Zindagi Ek Sangarsh.

==References==
 

==External links==
*  
*  

 
 
 
 
 
 