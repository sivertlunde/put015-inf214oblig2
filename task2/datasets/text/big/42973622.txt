The Joker King
{{Infobox film
| name = The Joker King
| image =
| image_size =
| caption =
| director = Enrico Guazzoni
| producer =  
| writer =  Guglielmo Giannini   Guglielmo Rovetta
| narrator =
| starring = Luisa Ferida   Armando Falconi   Luigi Cimara    María Denis 
| music = Umberto Mancini
| cinematography = Anchise Brizzi
| editing = Ferdinando Maria Poggioli 
 | studio = Produzione Capitani Film   ICAR
| released = 1935
| runtime = 94 minutes
| country = Italy Italian 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
The Joker King (Italian:Il re Burlone) is a 1935 Italian comedy film directed by Enrico Guazzoni and starring Luisa Ferida, Armando Falconi and Luigi Cimara. 

==Cast==
* Luisa Ferida as Trottola  
* Armando Falconi as Ferdinand II
* Luigi Cimara as Il conte Di Verolengo
* María Denis as Fanya 
* Paolo Stoppa as Filuccio 
* Gino Viotti as Count Di Castellucio  
* Ellen Meis as Queen of Naples  
* Evelina Paoli as Nennellas Mother  
* Luigi Pavese as Captain Alliana  
*Bruno Persa as Taniello  
* Emilio Petacci as Don Liborio 
* Mario Pisu as Captain Rodriguez  
* Diana Lante as Mirabella 
* Pino Locchi as Franceschiello 
* Achille Majeroni as Interior Minister 
* Nicola Maldacea as The butler  
* Tullio Galvani as Servant  
* Carlo Duse as Captain Ciro Romoa 
* Eugenio Duse as Capitain Holtmann 
* Flavio Díaz as Savoldi  
* Luigi Cimara as Il conte Di Verolengo  
* Romolo Costa as Don Flaminio  
* Vasco Creti as Barone Di Battifarno 
* Olinto Cristina as Prussian Ambasador  
* Luigi Erminio DOlivo as Colonel Muller  
* Miranda Bonansea as The kings youngest daughter

== References ==
 

== Bibliography ==
* Moliterno, Gino. Historical Dictionary of Italian Cinema. Scarecrow Press, 2008.

== External links ==
* 

 

 
 
 
 
 
 
 
 
 
 
 


 