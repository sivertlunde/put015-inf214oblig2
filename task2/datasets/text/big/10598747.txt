Daag: A Poem of Love
 
{{Infobox film
| name           = Daag
| image          = Daag72,jpg.jpg
| image_size     =
| caption        = 
| director       = Yash Chopra
| producer       = Yash Chopra
| writer         = Gulshan Nanda (story) Akhtar ul-Iman|Akhtar-Ul-Iman (dialogue)
| narrator       = 
| starring       = Sharmila Tagore Rajesh Khanna Raakhee
| music          = Laxmikant Pyarelal
| cinematography = Kay Gee
| editing        = Pran Mehra
| distributor    = 
| released       =  
| runtime        = 146 min
| country        = India
| language       = Hindi
| budget         = 
| gross          =   6.5 crores  
}}
Daag: A Poem of Love is a 1973 Bollywood film produced and directed by Yash Chopra in his debut as a producer, which laid the foundation of Yash Raj Films. It is an adaptation of the novel The Mayor of Casterbridge.

The film stars Sharmila Tagore, Rajesh Khanna, Raakhee (who won her first Filmfare award), Madan Puri, Kader Khan, Prem Chopra and A.K. Hangal.
 Telugu film Vichitra Jeevitham (1978) starring Akkineni Nageswara Rao, Vanisri, and Jayasudha. 

==Plot summary==
A young man, Sunil Kohli (Rajesh Khanna), falls for beautiful Sonia (Sharmila Tagore). Soon, they get married and leave for their honeymoon. On the way, owing to bad weather, they decide to spend a night at a bungalow owned by Sunils  boss. The bosss son, Dheeraj Kapoor (Prem Chopra), tries to rape Sonia when she is alone. But Sunil arrives in time, and a fight ensues, resulting in the death of Dheeraj. Sunil is arrested and, later, sentenced to death by the court. But, on the way to prison, the police van carrying him meets with an accident. All occupants are killed. Years later, Sonia, working as a school teacher and bringing up Sunils and her son, finds out that her husband is still alive. He is living with a new identity as Sudhir, and is married to a rich woman named Chandni (Raakhee). After escaping from the police van, Sunil met Chandni, whose lover had ditched her on learning of her pregnancy. Sunil married her to provide legitimacy to her child, in return for her help in establishing his new identity. Now, after so many years, the law is once again at his door step. This time, however, there is an added crime to his name: bigamy.

==Cast==
*Sharmila Tagore - Sonia Kohli
*Rajesh Khanna - Sunil Kohli
*Raakhee - Chandni
*Prem Chopra - Dheeraj Kapoor
*Baby Pinky - Pinky
*Raju Shrestha (Master Raju)- Rinku
*Manmohan Krishna - Deewan
*Madan Puri - K. C. Khanna
*Achala Sachdev - Mrs. Malti Khanna
*Iftekhar - Inspector Singh
*Hari Shivdasani - Jagdish Kapoor
*Yashodra Katju - School Principal
*Kader Khan - Prosecuting attorney
*A. K. Hangal - Prosecuting Attorney / Judge
*S. N. Banerjee - Judge
*Karan Dewan - Doctor Kapoor
*Surendra Nath - Sunils uncle
*Jagdish Raj - Ram Singh (driver)
*Manmohan - Prisoner in van
*Padma Khanna - Dancer Habib - Smith removing Sunils handcuffs

==Crew==
*Director - Yash Chopra
*Story - Gulshan Nanda
*Dialogue - Akhtar-Ul-Iman
*Producer - Yash Chopra
*Editor - Pran Mehra
*Art Director - R. G. Gaekwad Kay Gee
*Stunts - Ravi Khanna, M. B. Shetty
*Choreographer - Suresh Bhatt
*Lyricist - Sahir Ludhianvi
*Music Director - Laxmikant Pyarelal
*Playback Singers - Kishore Kumar, Lata Mangeshkar,  Rajesh Khanna

==Soundtrack==
The soundtrack includes the following tracks, composed by Laxmikant Pyarelal, and with lyrics by Sahir Ludhiyanvi  
*Song "Ab chaahe maa roothhe ya baaba" was listed at #7 on Binaca Geetmala annual list 1973
*Song "Mere dil mein aaj kya hai" was listed at #20 on Binaca Geetmala annual list 1973
*
{{Infobox album  
| Name        = Daag: A Poem of Love
| Type        = Soundtrack
| Artist      = Laxmikant Pyarelal
| Cover       = Daag- A Poem of Love, 1973 film Soundtrack album cover.jpg
| Released    =  1973 (India)
| Recorded    =  
| Genre       = Film soundtrack|
| Length      = 
| Label       = Sa Re Ga Ma|
| Producer    = Laxmikant Pyarelal
| Reviews     = 
| Last album  = Gaai Aur Gori  (1973)
| This album  = Daag: A Poem of Love  (1973)
| Next album  = Barkha Bahar   (1973)
|}}

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Song !! Singer (s)
|-
|"Mere Dil Mein Aaj Kya Hai" Kishore Kumar
|-
|"Ab Chahe Ma Roothe Yaa Baba" Kishore Kumar, Lata Mangeshkar
|-
|"Hum Aur Tum Tum Aur Hum" Kishore Kumar, Lata Mangeshkar
|-
|"Jab Bhi Jee Chahe" Lata Mangeshkar
|-
|"Main To Kuchh Bhi Nahin" Rajesh Khanna
|-
|"Ni Mein Yaar Manana Ni"
| Lata Mangeshkar, Minoo Purshottam
|-
|"Hawa Chale Kaise" Lata Mangeshkar
|}

==Awards and nominations==
*Filmfare Best Director Award--Yash Chopra
*Filmfare Best Supporting Actress Award--Raakhee
*Filmfare Nomination for Best Actor-Rajesh Khanna
*Filmfare Nomination for Best Music-Laxmikant Pyarelal
*Filmfare Nomination for Best Male Playback Singer-Kishore Kumar for the song "Mere Dil Mein Aaj" 

==References==
 

==External links==
*  
*   at Yash Raj Films

 

 
 
 
 
 
 
 
 