Love Is Elsewhere
 
 
{{Infobox film 
| name = Love Is Elsewhere 
| image = Love Is Elsewhere.jpg
| caption =
| film name = {{Film name| traditional = 愛情萬歲 
| simplified = 爱情万岁}}
| director = Vincent Chiu 
| producer = 
| writer = Cheuk-yin Chan  Wai-yan Chan  Cheung Fan  Suet-yan Lau  Chi-hoi Pang  Ray Pang Jason Chan  Sherman Chung  Yumiko Cheng  Chelsea Tong  Charmaine Fong  Patrick Tang 
| music = 
| cinematography = 
| editing = 
| studio = Buddy Film  Entertainment International  Mastermind Film Production 
| distributor = 
| released =  
| runtime = 98 minutes
| country = Hong Kong
| language = Cantonese
| budget = 
}} Vincent Chui (崔允信). The film had its world premiere at the 32nd Hong Kong International Film Festival in 2008.   


==Cast==
*Pakho Chau as Joe Yueng (楊溢德)
*Ken Hung as Ah Sing (阿星) Jason Chan as Ah Sung (阿生)
*Sherman Chung as Yat-Ching Wong (黃日晴)
*Yumiko Cheng as Sandra (阿如)
*Chelsea Tong as Kelly
*Charmaine Fong as Maggie
*Patrick Tang as Ji-Ho Fong (方志豪)
*Louis Cheung as Martin Hui (許少雄)

;Guest appearance
*Clarence Hui (許願) as Ah Sings uncle

==Featured songs==
*"Foolish Boy" (傻小子) - Pakho Chau from the album Continue Jason Chan from the album First Experience
*"Love. Gutless" (愛. 無膽) - Ken Hung from the album  Love. Gutless

==References==
 

==External links==
*  
*  

 

 