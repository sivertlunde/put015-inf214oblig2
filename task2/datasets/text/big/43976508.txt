Adholokam
{{Infobox film
| name           = Adholokam
| image          =
| caption        =
| director       = Thevalakkara Chellappan
| producer       =
| writer         = Balu Kiriyath
| screenplay     =
| starring       = Jagathy Sreekumar Nedumudi Venu Ranjini Thiagarajan
| music          = Raveendran
| cinematography =
| editing        =
| studio         = Centenary Productions
| distributor    = Centenary Productions
| released       =  
| country        = India Malayalam
}}
 1988 Cinema Indian Malayalam Malayalam film, directed by Thevalakkara Chellappan. The film stars Jagathy Sreekumar, Nedumudi Venu, Ranjini and Thiagarajan in lead roles. The film had musical score by Raveendran.   

==Cast==
*Jagathy Sreekumar
*Nedumudi Venu
*Ranjini
*Thiagarajan

==Soundtrack==
The music was composed by Raveendran and lyrics was written by Balu Kiriyath. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aakashappookkal || R Usha || Balu Kiriyath || 
|-
| 2 || Aakashappookkal   || R Usha || Balu Kiriyath || 
|-
| 3 || Annam Pookkula || KS Chithra || Balu Kiriyath || 
|}

==References==
 

==External links==
*  

 
 
 

 