Public Enemies (2009 film)
{{Infobox film
| name       = Public Enemies
| image      = PEPOSTERsm.jpg
| caption    = Theatrical release poster
| alt        = A man wearing a hat and a longcoat, a large gun held in his right hand Michael Mann
| producer   = Michael Mann Kevin Misher
| screenplay = Michael Mann Ronan Bennett Ann Biderman
| based on   = Public Enemies: Americas Greatest Crime Wave and the Birth of the FBI, 1933–34 by Bryan Burrough
| starring   = {{Plain list | 
* Johnny Depp
* Christian Bale 
* Marion Cotillard
* Billy Crudup
* Stephen Dorff
* Stephen Lang 
}}
| music      = Elliot Goldenthal
| cinematography = Dante Spinotti Jeffrey Ford
| studio    = {{Plain list | 
* Relativity Media Forward Pass Misher Films
* TriBeCa Productions
* Appian Way Productions
}} Universal Pictures
| released   =  
| runtime    = 143 minutes 
| country    = United States
| language   = English
| budget     = $100 million   
| gross      = $214,104,620 
}}
 FBI agent Stephen Graham).

Burrough originally intended to make a television miniseries about the Depression-era crime wave in the United States, but decided to write a book on the subject instead. Mann developed the project, and some scenes were filmed on location where certain events depicted in the film occurred, though the film is not entirely historically accurate.

==Plot== Charles Floyd FBI agent Melvin Purvis (Bale) is promoted by J. Edgar Hoover (Crudup) to lead the hunt for bank robber John Dillinger (Depp). Purvis shares Hoovers belief in using scientific methods to battle crime, ranging from cataloging fingerprints to tapping telephone lines.
 Billie Frechette (Cotillard) at a restaurant and woos her by buying her a fur coat. Frechette falls for Dillinger even after he reveals his identity, and the two become inseparable.
 Tommy Caroll Charles Winstead (Lang).
 Sheriff Lillian Holley (Taylor) has him locked up in the Lake County Jail in Crown Point. Dillinger and other inmates use a fake gun to escape. Dillinger is unable to see Frechette, who is under tight surveillance. Dillinger learns that Frank Nittis (Camp) associates are unwilling to help because his crimes are motivating the FBI to prosecute interstate crime, which imperils Nittis bookmaking racket (crime)|racket, thus severing his connections with the Mafia.
 Pierpont (Wenham) Makley (Stolte), Red Hamilton (Clarke) convinces him this is unlikely.

Purvis and his men apprehend Carroll and torture him to learn the gangs location. Purvis organizes an ambush at Little Bohemia. Dillinger and Hamilton escape separately from the rest of the gang. Agents Winstead and Hurt (Frye) pursue Dillinger and Hamilton through the woods, engaging in a gunfight in which Hamilton is fatally wounded. Trying to escape, Nelson, Shouse, and Van Meter hijack a Bureau car, killing Purvis partner Carter Baum (Cochrane) in the process. After a car chase, Purvis and his men kill Nelson and the rest of the gang. Hamilton dies that night.

Dillinger meets Frechette, telling her he plans to commit one more robbery that will pay enough for them to escape together. When Dillinger drops her off at a tavern he thinks is safe, she is arrested. Frechette is beaten during interrogation to learn Dillingers whereabouts, which she does not reveal; Purvis and Winstead eventually arrive and intervene. Dillinger agrees to participate in a train robbery with Alvin Karpis (Ribisi) and the Barker Gang, intending to flee the country the next day. He receives a note from Billie through her lawyer, Louis Piquett (Gerety), telling him not to try to break her out of jail.

Through Zarkovich, Purvis enlists the help of madam and Dillinger acquaintance Anna Sage (Katić), threatening her with deportation if she does not cooperate. She agrees to set up Dillinger, who she believes will come to hide out with her. Dillinger and Sage see Manhattan Melodrama at the Biograph Theater. After the film, Purvis signals other agents upon seeing them leave. Dillinger spots the police but is shot before he can draw his gun. Winstead listens to Dillingers last words. Purvis goes to inform Hoover of Dillingers death.

Winstead tells Frechette, still incarcerated, that he thinks Dillingers dying words were, "Tell Billie for me, Bye bye Blackbird". Billie sheds a tear - Bye Bye Blackbird was the song the houseband was playing when Billie and Dillinger first met each other and danced together in a dinner-club. The closing text reveals that Melvin Purvis quit the FBI in 1935 and died by his own hand in 1960, and that Billie lived out the rest of her life in Wisconsin following her release in 1936.

==Cast==
* Johnny Depp as John Dillinger, a notorious and charismatic bank robber whom the FBI declares to be "Public Enemy No. 1". Depp was involved in a film adaptation of Shantaram (novel)|Shantaram which was postponed in late 2007, allowing him to star in Public Enemies.        He was officially cast that December.   Depp described Dillinger as "that eras rock and roll star. He was a very charismatic man and he lived the way he wanted to and didnt compromise."     He felt "some kind of inherent connection" to Dillinger through one of his grandfathers, who ran moonshine, and his stepfather, who committed burglaries and robberies and spent time in the same prison Dillinger helped his associates escape from.   Depp could not find a recording of Dillingers own voice, but did find recordings of Dillingers father. He said when he heard Dillingers fathers voice, "I started to do the math and think, Well, he was raised basically a farm boy in southern Indiana.   I was born and raised in Owensboro, Kentucky, which is about 70 miles from where Dillinger was born and thats when it all clicked for me. I knew how he moved. I knew how he talked."    FBI Special Agent Melvin Purvis. Bale was not familiar with who Purvis was before making the film   and "spent a great deal of time" with Purvis son Alston and met other family and friends of Purvis, who died in 1960.     Bale said he "never viewed Purvis as having a real personal zeal for taking down Dillinger. I think that he was somebody who was very understanding in acknowledging why the public felt Dillinger to be almost a hero. He wasnt unaware of the problems of the day and the terrible deprivation of the majority of the population."   He thought Purvis "driving motivation was that he truly believed in Hoover and had a great desire to realize Hoovers brilliant vision. Thats really what I played with in my mind throughout this movie was the conflict between wanting to achieve that vision but recognizing Hoovers own compromises which Purvis wasnt entirely happy with making. In fact, very unhappy with making."  Billie Frechette, a singer and waitress who immediately becomes John Dillingers love interest. Cotillard was cast after Nine (2009 live-action film)|Nine (2009) was postponed.   Multiple American actresses also wanted the part; Mann found Cotillard "focused and artistically ambitious".   She trained herself to speak in a French-Canadian-Menominee-Wisconsin-Chicago accent    and spoke only English for three months during filming.   Cotillard "really wanted to know about   childhood" and met with relatives of Frechette in northern Wisconsin.   "At a young age, she was sent to a boarding school, and it was a very difficult place where they tried to erase everything that was Indian in her. And I think that she encountered there a great injustice, and she shared with Dillinger a suspicion of authority. I think the two of them saw that in each other and they fell in love immediately, and there was a very strong connection between them", Cotillard said. 
* Billy Crudup as J. Edgar Hoover. Crudup was cast as the future director of the Federal Bureau of Investigation by April 2008. 
* Stephen Dorff as Homer Van Meter, one of Dillingers associates and a fellow bank robber. Charles Winstead, one of the men who shot Dillinger.
* Michael Bentt as Herbert Youngblood, who helped Dillinger in his Crown Point jailbreak.

  Stephen Graham as Baby Face Nelson
* Channing Tatum as Pretty Boy Floyd Jason Clarke Red Hamilton
* David Wenham as Harry Pierpont Tommy Carroll
* Christian Stolte as Charles Makley
* James Russo as Walter Dietrich
* Giovanni Ribisi as Alvin Karpis
* John Ortiz as Phil DAndrea
* Domenick Lombardozzi as Gilbert Catena
* Bill Camp as Frank Nitti
* Rory Cochrane as Agent Carter Baum
* Carey Mulligan as Carol Slayman
* John Michael Bolger as Martin Zarkovich Anna Sage
* Emilie de Ravin as Barbara Patzke
* Shawn Hatosy as Agent John Madala
* Don Frye as Clarence Hurt
* Matt Craven as Gerry Campbell
* Lili Taylor as Sheriff Lillian Holley
* David Warshofsky as Warden Baker
* Peter Gerety as Louis Piquett
* Leelee Sobieski as Polly Hamilton. 
* Diana Krall as jazz singer
 

==Production==

===Development===
  premiere]] Michael Mann Shutter Island.   
 his two-part biopic about Guevara. Starting in 2006, Bennett worked for over 18 months on adapting Burroughs book,    writing several drafts.  Former NYPD Blue writer and Southland creator Ann Biderman rewrote the screenplay with Mann,     who polished it before shooting began.   Of the screenplay, Burrough has said "its not 100 percent historically accurate. But its by far the closest thing to fact Hollywood has attempted, and for that I am both excited and quietly relieved."   

===Filming=== Joliet and Beaver Dam, Madison and several other places in Wisconsin; including the Little Bohemia Lodge in Manitowish Waters, Wisconsin, the actual location of a 1934 gun fight between Dillinger and the FBI.    Some parts of the film were shot in Crown Point, Indiana, the town where Dillinger was imprisoned and subsequently escaped from jail. The actual 1932 Studebaker used by Dillinger during a robbery in Greencastle, Indiana was also used during filming in Columbus, borrowed from the nearby Historic Auto Attractions museum.   
 Baraboo and Columbus as Madison area. Lake County Crown Point, Darlington is the location for the courthouse scenes. A bank robbery scene was shot inside the Milwaukee County Historical Society, a former bank in Milwaukee that still has much of the original period architecture. 

In late March 2008 portions of the film were shot at Libertyville High School. Footage includes one of the schools science labs, an office, the schools front entrance, and the locker rooms. 
 Wisconsin Dells area.

The film became a flash point in the public debate about the "film tax credits" that are offered by many states.  The state of Wisconsin gave NBC Universal $4.6 million in tax credits, while the film company spent just $5 million in Wisconsin during filming. 

Michael Mann, the director, decided to shoot the movie in HD format instead of using the traditional 35mm film.  Public Enemies would be Manns first all-digital feature.
  Image:Biograph Theater redressed for movie.jpg| The Biograph Theater and (adjoining businesses) redressed for the film.
File:Farmers&MerchantsBankColumbusWisconsinPublicEnemiesSet.jpg|Farmers & Merchants Bank, redressed for the film. First National Bank during filming
File:Dillinger alley redressed for movie.jpg| The alley where John Dillinger was killed, redressed for the film.
 

===Post-production===
Elliot Goldenthal composed the score of Public Enemies. Before Goldenthal wrote any music, he and Mann "sifted through tons and tons of American blues" as the director had talked about Billie Holidays music "from the very beginning." Goldenthal said, "My job was chiefly composing dramatic music that didnt necessarily have to sound like it came from 1931 or 1933. It could be timeless." Goldenthal previously worked with Mann on Heat (1995 film)|Heat (1995). He commented that Mann "doesnt like too many twists and turns in the musics structure. He really responds to things that evolve very, very slowly. He wants music that the images, the edits, the dialogue can float above without it corresponding too much." 

==Release== trailer being released shortly after on March 5, 2009. Public Enemies had its world premiere in Chicago on June 19, 2009,    and was screened at the Los Angeles Film Festival on June 23, 2009.    The film was given wide release in the United States on July 1.

===Box office ===
Public Enemies opened at number three behind   and   with $25,271,675. The following weekend it had a 45.5% drop to $13,794,240 for a total of $66,221,110. The next three weekends the movie would go on to have decent drops of 46% or less.    As of January 18, 2010 the film grossed $97.1 million domestically with a worldwide gross of $214.1 million in revenue, more than twice its reported production budget.   

=== Home media ===
Public Enemies was released on DVD and Blu-ray Disc in the United States December 8, 2009. The two-disc special edition features a commentary track by the director and featurettes about the making of the film and the historical figures depicted in the film.   In promotion of the home media release, the multiplayer browser game Mafia Wars featured collectible "loot" from characters in the film. 

===Critical response===
  premiere]]
The film received positive reviews from critics. Rob James from  , who gave the film 3.5/4 stars, said "Its movie dynamite."

Most critics reviewing the film praised individual performances, specifically Depp as Dillinger.   s Todd McCarthy. 
 digital by a filmmaker whos helping change the way movies look, it revisits with meticulous detail and convulsions of violence a short, frantic period in the life and bank-robbing times of John Dillinger." 

While most critics praised the film, others expressed displeasure. Critic Liam Lacey, of The Globe and Mail, believed the film was missing "any image of the economic misery that made Dillinger a folk hero", and, "the most regrettable crime here is the way that Mann, trying to do too much, robs himself of a great opportunity."  Similarly, Richard Corliss of Time (magazine)|TIME magazine claimed the films emphasis on docudrama allowed for "precious little dramatic juice". 

Rotten Tomatoes gave the film a rating of 68%, based on 263 reviews, with an average rating of 6.4/10.  At Metacritic it is ranked 70/100, which indicates "generally favorable reviews." 

==Historical accuracy==
Shortly before the theatrical release of Public Enemies, Burrough wrote that director Michael Mann "impressed   as a real stickler for historical accuracy. Yes, there is fictionalization in this movie, including some to the timeline, but thats Hollywood; if it was 100% accurate, you would call it a documentary." Dillingers jailbreak from Crown Point, Indiana, the gunfight at the Little Bohemia Lodge, and Dillingers death near the Biograph Theater in Chicago were all filmed where they actually happened.    Burroughs non-fiction book on which the film is based details the demise of multiple infamous criminals in a 14-month period in 1933–34, including Dillinger, Bonnie and Clyde, the Barker-Karpis gang, the Kansas City Massacre, and Machine Gun Kelly. In focusing on Dillinger, Mann and co-writers Biderman and Bennett omitted Bonnie and Clyde entirely, briefly included only one member of the Barker gang (Alvin Karpis), and left out Pretty Boy Floyd except for his death. 

In the film, Dillinger is shown participating in a 1933 prison break from   at the time, and "few shots were fired" according to historian Elliott Gorn (the only injury was a clerk shot in the leg, and no guards were killed).    Dillingers preexisting friendship with those he helped break out, like Pierpont and Makley, who had taught Dillinger how to rob banks while he was in prison with them previously,  is not presented. Mann explained that "  employed techniques picked up from the military by a man     mentored Walter Dietrich, the man who died at the beginning of the movie, who mentored Dillinger. So Dillingers time in prison was really a post-graduate course in robbing banks, but what really interested me was he doesnt so much get out of prison when hes released but he explodes out".    There is then a scene where Purvis is promoted by J. Edgar Hoover after personally gunning down Pretty Boy Floyd in an apple orchard near the start of the movie. Floyd was actually killed three months after Dillingers death, and Purvis was just one of several agents present. He was also killed in an open field beside a barn, not in an apple orchard.   

During a phone call with Hoover, Purvis requests assistance from experienced cops in the film, a decision that Hoover actually made on his own.  In reference to Dillingers escape from Crown Point, Mann said "  didnt take six or seven people hostage, he took 17 officers hostage with that wooden gun he had carved. It wouldnt be credible if you put it in a movie, so we had to tone it down."  In the course of Dillingers 1933–34 crime spree, he is depicted as killing multiple people; Gorn writes that Dillinger himself "probably murdered just one man": William Patrick OMalley, a cop who had been shot during a holdup in East Chicago, Indiana.  Although Purvis was in charge of the Bureau of Investigations office in Chicago as depicted in the film, fellow agent Samuel Cowley lead the Dillinger investigation in its final months before Dillingers death.  Homer Van Meter and Baby Face Nelson are shot to death by Purvis after a vehicular pursuit from the Little Bohemia Lodge in the film. Van Meter was actually killed by St. Paul police a few weeks after Dillingers death, and Nelson was killed on November 27, 1934 in a gunfight with Cowley.

In the film, Dillinger and Purvis have a brief conversation in person while Dillinger is incarcerated.  In reality, they came close to seeing each other (right before Dillinger died), but never actually exchanged words. In the film, Dillinger walks into the detective bureau of a Chicago police station unrecognized and asks an officer for the score of a baseball game being broadcast on the radio, something he actually did according to Mann and Depp, however the game being broadcast is anachronistic for the time period.  Additionally, Winstead hears Dillingers last words – "Bye, bye, blackbird" – and later relays them to Frechette in the film. Burrough wrote that Dillingers lips were reportedly moving just after he fell from being shot outside the Biograph Theater and that "Winstead was the first to reach him", but what he might have said is unknown (not to mention that his speech may have been slurred due to his injuries). 

==References==
 

==External links==
 
 
*  
*  
*  
*  
*  
*  
*   Retrieved 2012-12-11
*   Retrieved 2012-12-11
*   Retrieved 2012-12-11

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 