Cat, Dog & Co.
 
{{Infobox film
| name           = Cat, Dog & Co.
| image          = 
| caption        =  Anthony Mack
| producer       = Robert F. McGowan Hal Roach
| writer         = Anthony Mack H. M. Walker
| starring       = 
| music          = 
| cinematography = Art Lloyd
| editing        = Richard C. Currier
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 20 52" 
| country        = United States Silent English intertitles
| budget         = 
}}
 short silent silent comedy film directed by Robert A. McGowan under the pseudonym "Anthony Mack".      
 phonographic disc.

==Plot==
Joe, Farina, and Harry are racing their dog-powered cars when they are stopped and reported to the President of the Be Kind to Animals Society.  After promising to be kind to animals, the boys are made honorary society members.  They soon convince other children to be kind to animals, and they release them from their cages to ensuing chaos.

==Cast==
===The Gang===
* Joe Cobb as Joe
* Jean Darling as Jean
* Allen Hoskins as Farina
* Bobby Hutchins as Wheezer
* Mary Ann Jackson as Mary Ann
* Harry Spear as Harry
* Donnie Smith as Don
* Pete the Pup as Pete

===Additional cast===
* Chet Brandenburg as Taxi driver
* Ray Cooke as Pedestrian
* Clara Guiol as Pedestrian Jack Hill as Pedestrian
* Hedda Hopper as President of the Be Kind to Animals Society
* John B. OBrien as Fruit vendor
* Bob Saunders as Trucker
* Syd Saylor as Pedestrian Dorothy Vernon as Pedestrian
* Adele Watson as Lady who snitched
* S. D. Wilcox as Officer

==See also==
* Our Gang filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 
 

 