Andin. Armenian Journey Chronicles
 
{{Infobox film
| name = Andin. Armenian Journey Chronicles
| image = And Post 2015.jpg
| caption = 
| director = Ruben Giney
| writer = Ruben Giney
| producer = Grant Sahakyan
| editing = Ruben Giney
| cinematography = Azat Gevorgyan, Leo Chun Zi, Zhao Lin, Jerome Colin, Ivan Kuptsov, Janek Schwirten, David Rust, Dima Lujanskiy
| music = Luis Argüelles
| narrator = Christopher Kent
| runtime = 128 minutes
| studio = Orion
| distributor = 
| released =  
| country = Armenia  China France India United Kingdom Spain Russia Mexico
| language = English
| gross = 
}}
Andin. Armenian Journey Chronicles is an epic  documentary directed by Ruben Giney. The film centers on historical links between Armenians travelling to India, China and other countries on the Silk Road.

==Production==
Because of a very small number of researchers proficient in the Armenian language, most of relevant study materials produced during the Soviet period have never been published in Europe. This is the exact reason why the films creators set a goal of revealing all known facts that affected World History. In pursuit of unique archival materials, manuscripts, personal journals and diaries from the libraries of Paris, London, Mexico and Lisbon were filmed.

The documentary is designed for both ordinary viewers and experts specializing in the history of the Silk Road and the Spice trade.

==Crew==
The film was directed by Ruben Giney who had prior experience participating in several archaeological expeditions to the Tarim Basin.
During three years of filming, Andin’s crew has visited 72 cities in 11 countries on 4 continents.     Because of a low budget, a completely new crew had to be put together in every single country.  This resulted in a total number of 7 camera operators having worked on the film. 
 

In August 2012, one of the crew members died in Calcutta amidst the outbreak of dengue fever. 

At a later stage, Luis Argüelles, a Mexican composer who had previously worked on Aquí y allá, a Cannes winning film, joined the team. He spent more than a year practicing duduk, Armenia’s national instrument, before he wrote the music for the film.

== Achievements ==
* For the first time, the ruins of the ancient Armenian trading city of Dvin were demonstrated in the film, combined with a 3D model of the city. 
* The director gained permission to film inside Areni-1, a unique cave where the world’s oldest known Areni-1 shoe and a Copper Age Areni-1 winery were found in 2007.

== Historical Discoveries ==
A number of important discoveries were made during filming in different countries:

* Pantusov’s Stone that was considered lost since 1894 was found, 
* Underwater footage (by Dave Rust) of legendary pirate William Kidd’s ship was shot at the site of its discovery off coast of Santo Domingo in 2007,
* Footage of the original manuscript by historian Hethum Patmich (Hayton of Corycus) describing the journey of the Armenian king Hethum I to the borders of China in 1254.  It is remarkable that Marco Polo was only one year old then,
 
* Attempts were made to find the legendary monastery on the shores of Issyk Kul where according to the Catalan Atlas relics of St. Matthew the Apostle are kept,
* Footage of a document confirming the presence of Armenian merchants in the New World at the early stage of the colonial period was for the first time shot at the National Archives of Mexico. 

==Release==
The film debuted at the Golden Apricot film festival in Yerevan in July of 2014  where it received the award of the Union of Cinematographers of Armenia for Best Documentary Film. Ruben Giney is currently the youngest film director to have been awarded with this prize. 
On 9 September 2014 the film was separately shown in Yerevan at a public screening attended by Galust Sahakyan, Chairman of the National Assembly of Armenia, and representatives of Chinese Embassy in Armenia.

In November 2014, the film premiered in North America at the Pomegranate Film Festival, where it received the Jury Honorable Mention and Audience Choice Documentary Film awards. 
A month later the films creators were invited to participate at the First International Silk Road Film Festival in China, Fuzhou City.

==Reception==
Andin appeared on several critics lists of the best films of 2014, including:
Best Ten of 2014 by Alex Deleon No. 6 - www.filmfestivals.com 

===Awards and nominations===
{| class="wikitable" style="font-size:100%;"
|-
! Year !! Group !! Award !!  Result
|- 2014
| Yerevan International Film Festival
| Union of Cinematography Award for Best Documentary
|  
|-
| Pomegranate International Film Festival
| Audience Choice Documentary Film, Jury Honourable Mention   
|  
|-
|}

==See also==
*Mongol invasions of Georgia
*Armenians in China
*Odoric of Pordenone

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 
 