The Cobbler (1923 film)
{{Infobox film
| name           = The Cobbler
| image          =
| caption        = Tom McNamara
| producer       = Hal Roach
| writer         = Hal Roach H. M. Walker Jack Davis Allen Hoskins Mary Kornman Ernie Morrison Richard Daniels Dick Gilbert Katherine Grant Clara Guiol Charley Young
| cinematography =
| editing        =
| distributor    = Pathé
| released       =  
| runtime        = 20 minutes
| country        = United States
| language       = Silent film English intertitles
| budget         =
}}

The Cobbler is the eighth Our Gang short subject comedy released. The Our Gang series (later known as "The Little Rascals") was created by Hal Roach in 1922, and continued production until 1944.

==Plot== Mickey and Jack accidentally disturb the rest of a sleeping hobo, who tries to punish them by doing them serious bodily harm.  After their dog chases the hobo off, the gang gets the car going again by rigging it with a sail and they sail off into the sunset.

==Notes== Jack wiggles Farina gorging herself in the watermelon patch is another very familiar gag in the Our Gang series.
*Mary Kornman is not a member of the gang, but a local rich girl who shows up at the cobbler’s shop.
*When the television rights for the original silent Pathé Our Gang comedies were sold to National Telepix and other distributors, several episodes were retitled. This film was released into TV syndication as Mischief Makers in 1960 under the title "The Lucky Shoemaker". Approximately two-thirds of the original film was included. Some footage from the beginning of the short were used in the Mischief Makers hybrid episode "Play Ball!".

==Cast==

===The Gang===
* Jackie Condon as Jackie
* Mickey Daniels as Mickey Jack Davis as Jack
* Allen Hoskins as Farina
* Ernie Morrison as Ernie

===Additional cast===
* Mary Kornman as Little Miss Riches
* Richard Daniels as Mr. Tuttle, the cobbler
* Dick Gilbert as Dandy Dick, the hobo
* Katherine Grant as Mary’s nursemaid
* Clara Guiol as Customer
* Charley Young as postman

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 


 