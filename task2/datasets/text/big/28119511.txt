The Anchorite
 
{{Infobox film
| name           = The Anchorite
| image          = 
| image size     = 
| caption        = 
| director       = Juan Estelrich
| producer       = 
| writer         = Juan Estelrich Rafael Azcona
| starring       = Fernando Fernán Gómez
| music          = 
| cinematography = Alejandro Ulloa
| editing        = Pedro del Rey
| distributor    = 
| released       =  
| runtime        = 108 minutes
| country        = Spain
| language       = Spanish
| budget         = 
}}
 1976 cinema Spanish drama film directed by Juan Estelrich. It was entered into the 27th Berlin International Film Festival where Fernando Fernán Gómez won the Silver Bear for Best Actor.   

==Cast==
* Fernando Fernán Gómez - Fernando
* Martine Audó - Arabel Lee
* José María Mompín - Augusto
* Charo Soriano - Marisa Claude Dauphin - Boswell
* Maribel Ayuso - Clarita
* Eduardo Calvo - Calvo
* Ángel Álvarez - Alvarez
* Ricardo G. Lilló - Lillo (as Ricardo Lillo)
* Isabel Mestres - Sandra
* Luis Ciges - Wis-Burte
* Sergio Mendizábal
* Antonio Almorós - Norberto
* Vicente Haro - Maitre
* Pedro Beltrán - Cajo
* Rafael Albaicín - Lenteja
* Enrique Soto - Inspector

==References==
 

==External links==
* 

 
 
 
 
 
 