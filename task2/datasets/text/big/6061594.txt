Gossip (2000 American film)
 
{{Infobox film
| name = Gossip
| image = Gossipposter.jpg
| caption = Theatrical release poster
| director = Davis Guggenheim Jeffrey Silver Joel Schumacher
| writer = Gregory Poirier Theresa Rebeck
| starring = James Marsden Lena Headey Norman Reedus Kate Hudson Marisa Coughlan Joshua Jackson
| music = Graeme Revell
| cinematography = Andrzej Bartkowiak
| editing = Jay Cassidy
| studio = Village Roadshow Pictures
| distributor = Warner Bros. Roadshow Entertainment   
| released =  
| runtime = 90 minutes
| country = United States
| language = English
| budget = $24 million
| gross = $12,591,270
}}
 teen drama drama film directed by Davis Guggenheim and featuring an ensemble cast including James Marsden, Lena Headey, Norman Reedus, and Kate Hudson.

==Plot==
The movie takes place on a college campus in the Northeast.  Derrick Webb (James Marsden), Cathy Jones (Lena Headey), and Travis (Norman Reedus) are students as well as roommates.  They all take a Communications class with Professor Goodwin (Eric Bogosian), in which the subject of gossip is brought up. For their final project, the three students decide to start a rumor and track and see the results. One night while at a nightclub, they run into Naomi Preston (Kate Hudson) and her boyfriend Beau Edson (Joshua Jackson). It is common knowledge through the campus that Naomi is saving her virginity for marriage.  Jones (as Cathy is called throughout the movie) in particular has a problem with Naomis wealth and sense of entitlement.  Furthermore, Naomi seems to have started a rumor about Jones having sex with Professor Goodwin.

While at the party, Derrick meets a girl and brings her upstairs. At this point the girl becomes ill; Derrick then notices Beau and Naomi in the adjacent room kissing. Beau attempts to have sex with Naomi but is rejected. Derrick, Jones, and Travis proceed to begin a rumor that Naomi and Beau had sex. The story begins to vary wildly, first involving a threesome, but soon, due to the nature of gossip, it becomes a story of Beau raping Naomi.


==Cast==
* James Marsden as Derrick Webb
* Lena Headey as Cathy Jones
* Norman Reedus as Travis
* Kate Hudson as Naomi Preston
* Eric Bogosian as Prof. Goodwin
* Edward James Olmos as Det. Curtis
* Joshua Jackson as Beau Edson
* Sharon Lawrence as Det. Kelly
* Marisa Coughlan as Sheila

==Reception==
Gossip received negative reviews from critics and currently holds a 28% rating on Rotten Tomatoes based on 65 reviews.

===Box office===
The film opened at #12 at the North American box office making $2.3 million USD in its opening weekend. The film suffered a 59% decline in box office earnings the following week, descending to #17.

== References ==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 