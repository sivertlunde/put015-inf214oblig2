Captain Horatio Hornblower R.N.
{{Infobox film
| name      = Captain Horatio Hornblower
| image     = Captain Horatio Hornblower 1951 film.jpg
| caption   = 
| director  = Raoul Walsh
| producer  = Gerry Mitchell Ben Roberts Aeneas MacKenzie
| starring  = Gregory Peck Virginia Mayo Robert Beatty Terence Morgan James Robertson Justice
| music     = Robert Farnon Guy Green
| editing   = Jack Harris
| studio = Warner Bros.
| distributor =Warner Bros.
| released  = September 13, 1951
| runtime   = 117 min.
| country   = United Kingdom United States
| language  = English
| budget    = 
| gross = $2.75 million (US rentals) 
}}

Captain Horatio Hornblower R.N. (released in the United States without the R.N.) is a 1951 naval adventure film. It was directed by Raoul Walsh and stars Gregory Peck, Virginia Mayo, Robert Beatty and Terence Morgan.
 Flying Colours. Forester is credited with the adaptation; as a result, the film is faithful to his novels and features an occasionally introspective tone unusual for an old-fashioned swashbuckler.

==Plot==
  El Supremo" Bush (Robert Beatty), "War breeds strange allies."

Upon his arrival, Hornblower is told that a larger, much more powerful Spanish warship, the 60-gun Natividad, has been sighted. When it anchors nearby, Hornblower and his crew board and capture it in a surprise nighttime attack. He then reluctantly hands the ship over to Alvarado to appease the madman, and they go their separate ways.
 Duke of Wellington (an anachronism, as the title was created in 1814 and he would have been Sir Arthur Wellesley at this time), Hornblower is in no position to refuse her request for passage to England.

Using superior seamanship and masterful tactics, Hornblower sinks the Natividad, and when the ships surgeon is killed in the battle, Lady Barbara insists on helping by nursing the wounded. When she later falls gravely ill, Hornblower nurses her back to health. On the voyage back to England, they fall in love. However, when she makes advances (although she is engaged), Hornblower informs her he is married.

After arriving home, Hornblower learns that his wife has died in childbirth, leaving him an infant son. He is given command of the Sutherland, a Seventy-four (ship)|74-gun ship of the line captured from the French, and is assigned to a squadron commanded by Rear Admiral Leighton (Denis ODea), Lady Barbaras new husband. The squadrons mission is to help enforce the British blockade against Napoleonic France.
 French Ship ships of campaign on the Iberian Peninsula.
 prize money. Leighton therefore expressly forbids Hornblower from taking any independent action if he sights the French.

Hornblowers French-built ship is subsequently mistaken for a friendly vessel by a small French brig, which flies the enemys recognition signal for the day. After capturing the vessel, Hornblower learns from interrogating its captain that he was transporting army supplies to the four warships for use in Spain. Rather than return to the squadron, Hornblower sends the brig back with a prize crew and the news.
 dismast all scuttles his ship in the channel, bottling up the French ships.
 Dutch officers, they board The Witch of Endor, a captured British ship. They overpower the skeleton crew, free a working party of British prisoners of war to man her, and sail away to freedom.

Hornblower is hailed as a national hero, and learns that Leighton was killed in the battle. Hornblower returns home to visit his young son, and finds Lady Barbara there. The two embrace.

==Cast==
 
*Gregory Peck as Captain Horatio Hornblower, R.N.
*Virginia Mayo as Lady Barbara Wellesley William Bush
*Terence Morgan as Second Lieutenant Gerard, gunnery officer on the Lydia
*Moultrie Kelsall as Third Lieutenant Crystal, navigator on the Lydia
*James Kenney as Midshipman Longley
*James Robertson Justice as Seaman Quist, who helps Hornblower and Bush escape
*Denis ODea as Rear Admiral Sir Rodney Mucho Pomposo Leighton
*Richard Hearne as Polwheal, Hornblowers steward
*Michael Dolan as Surgeon Gundarson
*Stanley Baker as Mr. Harrison, Hornblowers bosun
*Alec Mango as El Supremo / Don Julian Alvarado
*Christopher Lee as the Spanish captain of the Natividad
*Diane Cilento as the voice of Hornblowers wife Maria, reciting a letter written to him on her deathbed

===Casting=== first three 1948 adventure adventure romance film Adventures of Don Juan, growing difficulties with the actor, or his advancing age,  Flynn was not cast. Warners was already building up Burt Lancaster as their new swashbuckling screen star, but the role of a British sea captain seemed to be outside his range, so Peck was ultimately cast on a loan-out from David O. Selznick who received screen credit in the opening titles. Virginia Mayo was only cast after a number of high-profile British actresses were not free or  not interested. Pecks personal choice was Margaret Leighton. {{cite web |url=http://www.dvdjournal.com/quickreviews/c/captainhoratiohornblow.q.shtml
 |title=Captain Horatio Hornblower |accessdate=2008-11-02 |publisher=DVDjournal.com}}
 

==Production== 1950 Disney Treasure Island was reused as the frigate HMS Lydia. However, the ship was rocked instead of moving the horizon background, which caused many problems because of the combined weight of ship, crew and equipment. The Italian brigantine Marcel B. Surdo represented the The Witch of Endor for all at-sea exterior footage.   The Marcel B. Surdo would also appear in such seafaring films as The Crimson Pirate, The Master of Ballantrae#Film.2C TV or theatrical adaptations|The Master of Ballantrae, and John Paul Jones (film)|John Paul Jones.  The explosive and fire effects were supervised by Cliff Richardson. 

==Release==
The film made its worldwide premiere in New York City on September 13, 1951. It was the 9th most popular film at the British box office that year. 

==Modern reviews==
The film has been well received by modern critics with film aggregate site Rotten Tomatoes giving it 7.3 out of 10 and a 100% fresh rating  

==Other versions==
Gregory Peck and Virginia Mayo recreated their roles on a one-hour Lux Radio Theater program broadcast on January 21, 1952, which is included as an audio-only feature in the films DVD release. {{cite web|url=http://www.classicfilmguide.com/indexa33f.html|title=Review @ Classic Film Guide
|accessdate=2006-08-17}} 

==References==
 

==External links==
 
* 
* 
* 
* 
*  @ DVD Journal
*  @ Decent Films Guide

 

 
 
 
 
 
 
 
 
 
 
 
 
 