The Love Master (film)
{{infobox film
| title          = The Love Master
| image          =
| imagesize      =
| caption        =
| director       = Laurence Trimble Ray Connell(asst director)
| producer       = Laurence Trimble Jane Murfin
| writer         = Laurence Trimble(story,scenario) Jane Murfin(story) Donna Barrell(continuity) Joseph Barrell(continuity)
| starring       = Strongheart Lillian Rich
| music          =
| cinematography = Charles Dreyer Glen Gano John Leezer
| editing        = Cyril Gardner
| distributor    = Associated First National (*later First National Pictures)
| released       = February 1924
| runtime        = 70 minutes; 7 reels
| country        = USA
| language       = Silent film..(English intertitles)
}}
The Love Master is a 1924 silent film family drama starring canine star Strongheart and actress Lillian Rich and was directed by Laurence Trimble.  

A surviving film, held by a French archive. 

==Cast==
*Strongheart - Strongheart the dog
*Lady Julie the dog - The Fawn
*Lillian Rich - Sally
*Harold Austin - David
*Hal Wilson - Alec McLeod
*Walter Perry - Andrew Thomas Francis Joseph Mulligan
*Joseph Barrell - The Ghost Jack Richardson - unbilled

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 


 