Pillow Talk (film)
{{Infobox film
| name           = Pillow Talk
| image          = Pillowtalk poster.jpg
| image_size     = 250
| caption        = Theatrical poster Michael Gordon
| producer       = Ross Hunter Martin Melcher
| writer         = Russell Rouse Maurice Richlin Stanley Shapiro Clarence Greene
| starring       = Rock Hudson Doris Day Tony Randall Thelma Ritter
| music          = Frank De Vol
| cinematography = Arthur E. Arling
| editing        = Milton Carruth
| studio         = Arwin Productions
| distributor    = Universal-International
| released       =  
| runtime        = 98 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $7 million (est. US/Canada rentals) 
}}
 Michael Gordon. Nick Adams. The film was written by Russell Rouse, Maurice Richlin, Stanley Shapiro and Clarence Greene.
 Best Actress Best Actress Best Art Best Music, Scoring of a Dramatic or Comedy Picture.   
 Lover Come Back and Send Me No Flowers.

Upon its release, Pillow Talk brought in a then staggering domestic box-office gross of  A Farewell to Arms earlier that year.

In 2009, it was entered into the National Film Registry by the Library of Congress for being "culturally, historically or aesthetically" significant and preserved. 

==Synopsis== drunken Housekeeper party line Broadway composer and playboy. She is unable to obtain a private phone line because the telephone company has been overwhelmed by the recent demand for new phone lines in the area.

Jan and Brad, who have never met, develop a feud over the use of the party line. Brad is constantly using the phone to chat with one young woman after another, singing to each of them an "original" love song supposedly written just for her, though he only changes the name or language he sings in. Jan and Brad bicker over the party line, with Brad suggesting that the single Jan is jealous of his popularity.

One of Jans clients is millionaire Jonathan Forbes (Tony Randall), who repeatedly throws himself at her to no avail. Unknown to Jan, Jonathan is Brads old college buddy and current Broadway benefactor.

One evening in a nightclub, Brad finally sees Jan dancing. Attracted to her, he fakes a Texan accent and invents a new persona: Rex Stetson, wealthy Texas rancher. He succeeds in wooing Jan, and the pair begin seeing each other regularly. Jan cannot resist bragging about her new beau on the phone to Brad Allen, while Brad teases Jan by having "Rex" show an interest in traditionally effeminate things, thereby implying "Rexs" homosexuality.   

When Jonathan finds out about Brads masquerade, he forces Brad to leave New York City and go to Jonathans cabin in Connecticut to complete his new songs. Brad uses the opportunity to secretly ask Jan to go away with him, and she does. Once there, romance is in the air until Jan stumbles upon a copy of Rexs sheet music. She plunks the melody on the nearby piano and recognizes Brads song. She confronts Brad angrily and ignores his attempts at explanation, leaving with Jonathan who has arrived just in time to expose Rex as Brad and who takes her back to New York City.

Once Brad returns to New York, Jonathan is pleased to learn that the mighty oak of a playboy has finally fallen in love, while conversely Jan will have nothing to do with Brad for deceiving her. Not ready to give up, Brad turns to Jans maid, Alma, for advice. Alma, pleased to finally meet Brad after listening in on his phone calls on the party line for so long, suggests he hire Jan to decorate his apartment so they will be forced to collaborate. Jan only concedes so that her employer will not lose the commission. Little does she know her employer is also in on the scheme. Brad leaves all the design decisions up to Jan, telling her only to design a place that shed want to live in herself. Still quite angry however, Jan completely redoes Brads apartment in the most gaudy, ghastly, hideous decor she can muster.  Horrified by what he finds, Brad angrily storms into Jans apartment and carries her in her pajamas through the street back to his apartment, where he asks her how it feels to return to the scene of the crime. In his frustration he tells her of all the changes hes made to end his bachelor lifestyle because he thought he was getting married. Her face lights up from his admission but hes so angry he turns to leave. She quickly reaches for one of the tacky remote control switches he installed to accommodate his "purposes," which immediately locks the door. She flips the second switch and the player piano pounds out a honky-tonk version of Brads standard love song. He turns around in defeat, their eyes meet, each smiles, and they lovingly embrace.

At the end of the film, Brad goes to tell Jonathan that he is going to be a father. During the end credits, four pillows appear on the screen — pink, blue, pink, and blue — signifying the children Brad and Jan have together.

==Cast==
* Rock Hudson as Brad Allen
* Doris Day as Jan Morrow
* Tony Randall as Jonathan Forbes
* Thelma Ritter as Alma Nick Adams as Tony Walters
* Julia Meade as Marie
* Allen Jenkins as Harry
* Marcel Dalio as Mr. Pierot Lee Patrick as Mrs. Walters Mary McCarty as Nurse Resnick
* Alex Gerry as Dr. Maxwell
* Hayden Rorke as Mr. Conrad
* Valerie Allen as Eileen
* Jacqueline Beer as Yvette
* Arlen Stuart as Tilda
* Perry Blackwell as Lounge Singer
* Muriel Landers as Moose Taggett
* William Schallert as Hotel Clerk

==Songs==
Doris Day sings three songs in the film: "Pillow Talk" during the opening credits, "Roly Poly" in the piano bar with Hudson, and "Possess Me" on the drive up to Jonathans cabin. Singer Perry Blackwell performs three songs in the piano bar: "I Need No Atmosphere", "Roly Poly" (in part), and "You Lied"—a song directed at Hudsons character, Brad.

==See also==
* Down with Love, an homage 2003 film that contains elements of Pillow Talk (with Tony Randall himself making a cameo.)

==References==
 

==External links==
 
 
*  
*  
*  
*  
*  
*  
*   - Bright Lights Film Journal
*   - DorisDay.Net
*   - RockHudsonBlog.com

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 