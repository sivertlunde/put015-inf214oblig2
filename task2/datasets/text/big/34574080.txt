The Grand Heist
{{Infobox film
| name           = The Grand Heist
| image          = The Grand Heist-poster.jpg
| caption        = Promotional poster for The Grand Heist
| film name      = {{Film name
| hangul         =      
| rr             = Baramgwa Hamjje Sarajida
| mr             = Paramgwa Hamkke Sarajita}}
| director       = Kim Joo-ho
| producer       = Oh Jeong-hyun
| writer         = Kim Min-sung 
| starring       = Cha Tae-hyun Oh Ji-ho
| music          = Kim Tae-seong
| cinematography = Lee Seong-jae
| editing        = Steve M. Choi
| studio         = AD406 and DHUTA
| distributor    = Next Entertainment World Finecut (international)
| released       =   
| runtime        = 121 minutes
| country        = South Korea
| language       = Korean
| budget         =  
| gross          =   
}}
 

The Grand Heist ( ; lit. "Gone With the Wind") is a 2012 South Korean historical comedy film about a gang of 11 thieves who try to steal ice blocks from the royal storage, Seobingo, during the last years of the Joseon era.  It was released on August 8, 2012. 

==Plot==
The story takes place in the late 18th century of Joseon Dynasty (1392-1910), during the last years of King Yeongjo’s reign. Ice is a commodity more valuable than gold. Blocks of it are harvested from frozen rivers in winter, put in royal storage and distributed or sold throughout the year for general consumption. When corrupt officials conspire to form a monopoly and fix its price, a gang of 11 professionals is formed to stop the scheme — and to do that they must make all the royal ice blocks in five storage rooms disappear for a night.    

==Cast==
*Cha Tae-hyun   - Lee Deok-mu, the intelligent bastard son of the minister of the right, the courts only clean official. The laid-back bookseller initially chases pretty girls and rare exotic books, but after his father is falsely accused of a crime by his political rival, Deok-mu becomes the leader of the heist gang.
*Oh Ji-ho   - Baek Dong-soo, a trained soldier and ousted chief guard of the royal ice storage
*Min Hyo-rin - Baek Soo-ryun, a diver and Dong-soos sister
*Lee Chae-young - Seol-hwa, a spy-cum-gisaeng
*Sung Dong-il - Soo-gyun, the chief financial backer of the gang
*Ko Chang-seok - Seok-chang, a grave-digging specialist 
*Shin Jung-geun - Dae-hyun, a near-deaf explosives maker
*Kim Gil-dong - Cheol-joo
*Chun Bo-geun - Jung-goon, the "idea bank"
*Kim Hyang-gi - Nan-i, a rumor-spreader
*Song Jong-ho - Jae-joon, a master of disguises
*Nam Kyeong-eup - Jo Myung-soo, the minister of the left who wants to monopolize the ice in Seobingo
*Kim Ku-taek - Jo Young-cheol
*Oh Na-ra - Jo Myung-soos concubine
*Lee Moon-sik - Mr. Yang (cameo) older Jung-goon (cameo)

==Reception==
The film drew 4 million viewers in just 19 days after its release, becoming the seventh homegrown movie in Korea to achieve the feat in 2012.  Its total admissions is at a little over List of highest-grossing films in South Korea|4.9 million.  

The film ranked second and grossed   in its first week of release,  and grossed a total of   domestically after five weeks of screening. 

==Awards==
*2013 Fantasporto Orient Express Awards: Best Film 

==References==
 

==External links==
*    
*   at Naver  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 