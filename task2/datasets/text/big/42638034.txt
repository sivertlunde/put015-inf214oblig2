Zatoichi's Cane Sword
{{Infobox film
| name = Zatoichis Cane Sword
| image =
| film name      =  {{Infobox name module
| kanji          = 座頭市鉄火旅
| romaji         = Zatōichi tekka-tabi
}}
| director = Kimiyoshi Yasuda
| producer = Ikuo Kubodera
| writer = Ryozo Kasahara
| based on       =  
| starring = Shintaro Katsu Shiho Fujimura Eijirō Tōno
| music = Ichirō Saitō
| cinematography = Senkichiro Takeda
| editing        = Toshio Taniguchi
| studio         = Daiei Studios
| distributor    =
| released =  
| runtime = 93 minutes
| country = Japan
| language = Japanese
}} Daiei Motion Picture Company (later acquired by Kadokawa Pictures).

Zatoichis Cane Sword is the fifteenth episode in the 26-part film series devoted to the character of Zatoichi. 

==Plot==
 
In a town dominated by yakuza, Zatoichi (Katsu) encounters a master swordsmith named Senzo (Tōno) who identifies Zatoichis cane sword as the work of his late mentor. Senzo also spots a crack in the blade and warns that it will snap after one more kill.

==Cast==
* Shintaro Katsu as Zatoichi
* Shiho Fujimura as Oshizu
* Eijirō Tōno as Senzo
* Yoshihiko Aoyama as Seikichi Tatsuo Endo as Boss Iwagoro
* Masumi Harukawa as Oryu
* Makoto Fujita as Umazo
* Kiyoko Suizenji as Oharu
* Masako Akeboshi as Matsu
* Fujio Suga as Kuwayama 

==Reception==
===Critical response===
Roger Greenspun, in a review for The New York Times, wrote that " here it is quiet enough to allow Ichi his peaceful idiocyncrasies, Zato Ichis Cane Sword is a pleasantly modest film, an amiable contrast to the fateful solemnities of the Toshiro Mifune samurai dramas. Ichis very invulnerability makes for a certain relaxation, a few songs, a little buffoonery, and much of it to the good."   

==References==
 

==External links==
* 
* 
* 
* , review by J. Doyle Wallis for DVD Talk (28 May 2004)
* , review by Judge Dan Mancini for DVD Verdict (8 June 2004)
* , review by D. Trull for Lard Biscuit Enterprises
* , review by Hubert for Unseen Films (17 February 2014)
* , by Thomas Raven for freakengine (February 2012)
* , by Mark Pollard for Kung Fu Cinema
* , review by Trash Cinema Club (22 April 2014)

 

 
 
 
 
 
 
 
 
 

 