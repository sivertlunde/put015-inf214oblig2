Five Corners (film)
{{Infobox film
| name           = Five Corners
| image          = Five corners poster.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Tony Bill Denis OBrien (Exec. Prod.)
| writer         = John Patrick Shanley
| narrator       =
| starring       = Jodie Foster Tim Robbins John Turturro
| music          =
| cinematography =
| editing        =
| studio         = HandMade Films
| distributor    = Cineplex-Odeon Films
| released       =  
| runtime        = 90 min
| country        = United States United Kingdom
| language       = English
| budget         = $5.5 million 
}}
Five Corners is a 1987 American low budget crime drama film starring Tim Robbins, Jodie Foster, John Turturro, and Rodney Harvey. It was directed by Tony Bill.  It depicts 48 hours in the lives of a group of young New Yorkers in the 1960s.

==Plot==
 
The film is set in The Bronx in 1964.  We are introduced to Heinz (John Turturro), the neighborhood bully who has just been released from prison, who wastes no time in letting everyone know he hasnt changed by stirring up trouble again. Heinz has just been released from prison after serving a term for attempted rape, and has returned to his old neighborhood to resume his relationship with his demented mother and to "rekindle" his own demented version of a relationship with Linda (Jodie Foster), the near-rape victim.  

Harry (Tim Robbins) had protected Linda in the near-rape, but since then he has adopted a policy of non-violent response to violence (caused by the murder of his policeman father and the non-violent protests against racism espoused by Dr. Martin Luther King).   Harry has now become a Buddhist and a pacifist, and seeks to join Dr. Kings movement, making protecting Linda again a difficult task.

We also see Mr. Glasgow, an overly strict high-school teacher murdered by being shot in the back with an arrow while he walks down the street.  That night we see Sal driving his fiancé Melanie and her girlfriend Brita around the neighborhood while they take pills and sniff glue in the back seat. 

Fed up with how (once again) theyve gotten so loaded, Sal offers to give them to Castro and Willie, two boys from the neighborhood, who have just finished up vandalizing the storefront sign for the local deli. Sal even offering them cash to take the women off his hands.  They accept and walk off with the girls, who are so high they dont even know who theyre supposed to be with.  

The next morning, Brita and Melanie wake up in a strange apartment, lying naked under sheets.  They get dressed and find Castro and Willie in the hall.  Picking up where they left off, the boys tell Melanie and Brita, "Someone murdered our teacher, so we have the day off. Want to go for a ride?". But the ride they have in mind is climbing up on top of the buildings elevators with the girls, and making out with them as the elevators go up and down during the day. 

Then the four of them go bowling, and Sal shows up to apologize to Melanie for what he did, and eventually tells her he just felt frustrated because he can see them getting married and having children together someday, and wishes she would stop getting loaded all the time.

Heinz calls Linda, and tells her to meet him in a park at midnight. She reluctantly agrees, knowing that he may become dangerous if she doesnt comply. When arriving at the pool, she finds a board to use for protection and hides it. Heinz shows her a present he got for her: two penguins he stole from the Bronx zoo. She tells him that he has to return them because penguins need special food. 

Heinz becomes outraged, thinking that she was rejecting his gift, and kills one of the penguins. Heinz comes on strong with Linda, pushing her further and further until in desperation she fights Heinz off, and runs off with one of the penguins. She takes it to Jamie, who wishes Linda was his girl. He reluctantly helps her get the penguin set up in a tub at the bar he works in, then escorts her to the subway.

Heinz shows up and kidnaps Linda and knocks her out. He carries her to a taxi, kicks out the window to gain entry, and lays her down in the backseat. Meanwhile, Jamie has run to get the police, and Harry, having belatedly heard that Linda had wanted him to go with her when she met Heinz, has gone to the police also, after walking the neighborhood with his Saint Bernard in an attempt to find her. 

The dog leads them to the taxi and while attempting to arrest Heinz, one of the detectives is disarmed by Heinz, who grabs the gun and shoots the detective, then takes off with Linda. In a murderous rage, Heinz drives around playing chicken with several drivers before ramming into the phone booth in front of his mothers house, killing the detective inside it, who was staking out the place in case Heinz showed up.

At the limit of his sanity, and sensing that hes going to die tonight, Heinz carries Linda up to his mothers apartment. He tries to get his mom to recognize the man hes become, but becomes frustrated when this appeal fails and throws his mother out the window.

The movie ends with Heinz taking the still-unconscious Linda up the fire escape and across to an adjacent rooftop, where police surround the building. But Harry and Jamie have gone up first to try to rescue Linda themselves. Harry tries to convince Heinz to let Linda be taken to safety, offering himself as a substitute, and Heinz can do whatever he wants to him. 

As he talks, Jamie sneaks slowly around behind Heinz and attacks him. Both Jamie and Harry take turns struggling with Heinz. A police sharpshooter is in a position to kill Heinz but doesnt because it would endanger Linda (who Heinz has laid down on the ledge of the rooftop), as well as the others. Out of nowhere, Heinz is killed by a mysterious arrow to his back, just like the teacher at the beginning of the film. 

Heinz falls to his death, and as the camera pans to the back of the roof, we see it is Castro who has shot the arrow, accompanied by his friend Willie.

As the credits roll, Jamies boss opens the bar the next morning, and is shocked to find the penguin inside. As he backs out in disbelief, the penguin comes waddling out.

==Cast==
*Jodie Foster - Linda
*Tim Robbins - Harry
*Todd Graff - Jamie
*John Turturro - Heinz
*Michael R. Howard - Murray
*Pierre Epstein - George
*Jery Hewitt - Mr. Glasgow
*Rodney Harvey - Castro
*Daniel Jenkins - Willie Elizabeth Berridge - Melanie
*Cathryn de Prume - Brita
*Carl Capotorto - Sal 
*John Seitz - Detective Sullivan Jack McGee - Desk Sergeant

==Critical reception==
Critics generally praised this movies actors, many of whom went on to become A-list names, but were less generous with the script:

"Five Corners is so filled with inspired, memorable moments that its tough to completely fault it when it inanely begins spiraling out of control towards the conclusion.  After all, the entire picture is so beautifully messy from the outset that any hope for rational tie-ups is equally absurd . ." 

==Behind the Scenes== Van Nest neighborhood of The Bronx, where writer John Patrick Shanley grew up. 

==Home Video==

The movie has received numerous VHS and DVD releases. A laserdisc released by Criterion shows the film in widescreen.

Image Entertainment is now set to release a new widescreen DVD of the film in February 2011. The film is also set to make its debut onto Blu-ray around the same time.

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 