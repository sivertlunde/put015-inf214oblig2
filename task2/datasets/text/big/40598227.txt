Three Days of Rain (film)
 
{{multiple issues|
 
 
}}

Three Days of Rain is a 2002 US film directed by Michael Meredith. Based on Anton Chekhovs short stories, the plot takes place in Cleveland city during a rainstorm. It was the last film to star Peter Falk, famous for playing disheveled detective Columbo.

== Credits ==
* Director: Michael Meredith
* Script: Michael Meredith
* Executive producer: Philip Bligh, Brad Hillstrom, Christine U. King, Bruce Randolph Tizes
* Producer: Robert Casserly, Bill Stockton

== Starring ==
* Peter Falk
* Penelope Allen

== Links ==
* http://www.imdb.com/title/tt0162838/

 
 


 