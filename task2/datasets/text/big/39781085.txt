Living on One Dollar
 

{{Infobox film
| name           = Living on One Dollar
| director       = 
| producer       = Ryan Christofferson   Zach Ingrasci  Sean Leonard  Chris Temple
| writer         = 
| screenplay     = 
| story          = 
| distributor    = IndieFlix
| released       =  
| country        = United States
| language       = English
}}

Living on One Dollar is a 2013 documentary film directed, produced and edited by Chris Temple, Zach Ingrasci, Sean Leonard, and Ryan Christofferson.

== Premise ==

The film follows the experience of four young friends as they live on less than $1 a day for two months in rural Guatemala. They battle hunger, parasites and the realization that there are no easy answers. Yet, the generosity and strength of Rosa, a 20 year old woman, and Chino, a 12 year old boy gives them hope that there are effective ways to make a difference.   

== Inception ==

The project did not start out as a film, but rather short YouTube videos that went viral, amassing over 650,000 views on the site.     Inspired by the response, the four college friends produced a 56-minute film from the experience. They have since taken the film on a national tour to 20 major universities, receiving coverage on CBS This Morning with Charlie Rose.    The film won best documentary for the audience award at the Sonoma International Film Festival and Nobel Laureate Muhammad Yunus calls it, "A must watch film, that provides a unique look into the hardship and hope of life in extreme poverty." Similarly, Gary Ross, the famed Writer and Director of the first Hunger Games film, calls the film "moving, inspiring and important."   

== Distribution ==

As of Feb 2015, the film is available on Netflix in Canada and the United States. Internationally its available on iTunes and on their website - www.Livingonone.org. You can also book events in your school or church and bring them to speak.

== References ==

 

== External links ==

* 
* 