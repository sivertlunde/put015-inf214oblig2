Munnabhai S.S.C
 

{{Infobox film
| name           = Munnabhai S.S.C - Marathi Movie
| image          = Munnabhai S.S.C.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = Sunil Naik
| producer       = Santosh Waje
| screenplay     = 
| story          = 
| starring       = Padarinath Kamble Vijay Chavan Dr. Girish Oak Vasudha Deshpande
| music          = Chinar Mahesh
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Marathi
| budget         = 
| gross          = 
}}
Munnabhai S.S.C is a Marathi movie released on 12 August 2005. Produced by Santosh Waje and directed by Sunil Naik. The movie is based on a young lad whose repute of mischief has landed him in a situation where he has to prove his innocence to his father.

== Synopsis ==
The comedy drama is about a mischievous boy who is always up to pranks. Be it on his parents or the school staff, everyone is fed up of Munnas antics. Although he has a heart of gold and is a good sportsman, his good qualities go unnoticed because of his mischievous nature.

One day, Munna learns that one of the boys working in the school canteen needs money to take his ailing mother to a doctor. He doesnt even think twice before helping the boy out with the money his father had given him to pay the electricity bill. Munnas kind gesture, however, results in his life taking a drastic turn. His father accuses him of having spent the money on himself and then lying to cover it up. Munnas repeated pleas of innocence fall on deaf ears and he is kicked out of his house. Pushed into a corner, Munna is forced to turn over a new leaf and prove his innocence to everyone.

== Cast ==

The cast includes Padarinath (Paddy) Kamble, Vijay Chavan, Dr. Girish Oak, Vasudha Deshpande & Others. 

==Soundtrack==
The music is provided by Chinar Mahesh. 

== References ==
 
 

== External links ==
*  
*  
*  

 
 
 


 