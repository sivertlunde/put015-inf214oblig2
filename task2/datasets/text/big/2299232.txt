Elling
 
 
{{Infobox film name           = Elling image size     = 190px image          = Elling.jpg caption        = Norwegian theatrical poster director       = Petter Næss producer       = writer         = Ingvar Ambjørnsen (novel Brødre i blodet) Axel Hellstenius starring       = Per Christian Ellefsen Sven Nordin music          = Lars Lillo-Stenberg cinematography = Svein Krøvel editing        = Inge-Lise Langfeldt distributor    = released       =   runtime        = 89 min. country        = Norway language  Norwegian
|budget         =
}}
Elling is a Norwegian film directed by Petter Næss. Shot mostly in and around the Norwegian capital Oslo, the film, which was released in 2001, is primarily  based on Ingvar Ambjørnsens novel Brødre i blodet ("Blood brothers", 1996), one of a series of four featuring the Elling character – the others are Utsikt til paradiset ("A view of paradise", 1993), Fugledansen ("The bird dance", 1995), and Elsk meg i morgen ("Love me tomorrow", 1999). The film was followed by an original prequel not based on any of the novels, Mors Elling (2003), and a sequel, Elsk meg i morgen (2005) based on the fourth and last book in the series.

==Plot==
The movie deals with the main character, Elling, a man with generalized anxiety in his 40s, and his struggle to function normally in society. He suffers from anxiety, dizziness, and neurotic tendencies, preventing him from living on his own. Elling has lived with his mother for his entire life, and when his mother dies, the authorities take him from the apartment where he has always lived and send him to an institution. His roommate is the simpleminded, sex-obsessed Kjell Bjarne. The Norwegian government pays for the two to move into an apartment in Oslo, where every day is a challenge as they must prove they can get out into the real world and lead relatively normal lives. With the help of social worker Frank and a few new friends, they learn to break free from their respective conditions.

==International== Oscar nomination Best Foreign Language Film.

==Awards==
*  — Nominated (2002)
* ) — Nominated (2001)

In addition, the film was nominated and won several other awards all over the world.

==Characters==
*Elling — (Per Christian Ellefsen)
*Kjell Bjarne — (Sven Nordin)
*Reidun Nordsletten — (Marit Pia Jacobsen)
*Frank Åsli — (Jørgen Langhelle)
*Alfons Jørgensen — (Per Christensen)

==References==
 

== External links ==
*  
*  


 

 
 
 
 
 
 
 
 

 
 