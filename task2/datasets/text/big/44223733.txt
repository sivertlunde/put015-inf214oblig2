Aaj Ka Samson

{{Infobox film
| name           = Aaj Ka Samson
| image          = 
| caption        = 
| director       = Kukoo Kapoor
| producer       = Kukoo Kapoor
| writer         = Kukoo Kapoor
| starring       = Hemant Birje Sahila Chadha
| music          = Prem Gupta
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}}

 Aaj Ka Samson  is a 1991 Bollywood film directed by Kukoo Kapoor and starring Hemant Birje, Sahila Chadha, Goga Kapoor and Puneet Issar.

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title
|-
| 1
| "Aayi Hai Jabse Jawani"
|-
| 2
| "Har Janam Mera Hoga Tere Liye"
|-
| 3
| "Aaj ka Samson"
|}

==References==
 

==External links==
 

 
 
 
 


 