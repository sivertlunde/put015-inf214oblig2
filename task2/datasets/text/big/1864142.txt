The Beyond (film)
 
{{Infobox film
| name = The Beyond
| image = The Beyond original Poster.jpg
| caption = US poster
| director = Lucio Fulci
| producer = Fabrizio De Angelis
| screenplay = {{plainlist|
* Giorgio Mariuzzo
* Lucio Fulci
* Dardano Sacchetti
}}
| story = Dardano Sacchetti
| starring = {{plainlist| Katherine MacColl
* David Warbeck
* Cinzia Monreale
* Antoine Saint-John
* Veronica Lazar
}}
| cinematography = Sergio Salvati
| music = Fabio Frizzi
| editing = Vincenzo Tomassi
| studio = Fulvia Film
| distributor = Medusa Distribuzione
| released =  
| runtime = 89 minutes
| language = Italian English
| budget = $400,000  
}} Italian horror film directed by Lucio Fulci. The second film in Fulcis unofficial Gates of Hell trilogy (along with City of the Living Dead and The House by the Cemetery), The Beyond has gained a cult following over the decades,  in part because of the films gore-filled murder sequences,  which had been heavily censored when the film was originally released in the United States in 1983.

==Plot==
In Louisianas Seven Doors Hotel in 1927, a lynch mob murders an artist named Schweick, whom they believe to be a warlock. This opens one of the Seven Doors of Death, which allow the dead to cross into the world of the living. Several decades later, Liza, a young woman from New York, inherits the hotel and plans to re-open it. Her renovation work activates the hell portal, and soon she and a local doctor contend with strange incidents: a painter falls off his rig, horribly injuring himself; Joe, a plumber, investigates a serious flood from the cellar and a demonic hand gouges out his eye. His body as well as another are later discovered by a hotel maid, Martha.

Liza encounters a sinister blind woman named Emily, who warns her that she has made a huge mistake moving into the hotel. At the hospital, Joes wife Mary-Anne and their daughter Jill have come to the morgue to examine his corpse. Jill enters the room to find her mother laying on the floor unconscious with her face being burned by acid. Liza meets with Dr. John McCabe in a nearby bar and receives a phone call informing her of Mary-Annes death. After the funerals, Liza encounters Emily at the hotel. She tells Liza the story of Schweick and the hotel, warning her to not enter room 36. When Emily examines Schweicks painting, she begins to bleed, and Emily flees the hotel.

Liza, ignoring Emilys advice, investigates hotel room 36. She discovers an ancient book titled The Book of Eibon|Eibon. She enter the bathroom and sees Schweicks corpse nailed to the wall. Frightened and hysterical, Liza runs from the room but is stopped by John. She takes him to room 36 but both the corpse and the book are gone. Liza reveals her fearful encounters with Emily, but John insists that Emily is not real. While in town, Liza spots a copy of Eibon in the window of a book store. The shop owner denies the books existence, and it is no longer there when Liza looks for it. At the hotel, a worker named Arthur attempts to repair the same leak as Joe, but is killed off-screen by ghouls.

Lizas friend Martin visits the public library to find the hotels blueprints. Alone, he is struck by a sudden force and falls from a ladder, paralyzing him. Spiders suddenly appear and ravage his face and kill him. Martha is cleaning the bathroom in Room 36 when Joes animated corpse emerges from the bathtub. Joe pushes her head into an exposed nail, killing her and destroying one of her eyes. The walking corpses of Schweik, Joe, Mary-Anne, Martin and Arthur invade Emilys house, and she pleads with them to leave her alone. She insists she will not return with Schweik and commands her guide dog Dickie to attack the corpses. He repels them but soon turns and attacks Emily, tearing out her throat.

At the hotel, spirits terrorize Liza. John breaks into Emilys house, which appears to have been abandoned for years, and finds The Book of Eibon. Arriving at the hotel, he tells Liza it is a gateway to Hell. As he insists there has to be a rational explanation and questions Lizas origins, the gate finally opens. The two quickly flee to the hospital, which has been overrun by zombies. Liza is attacked, but John gets a gun out of his desk and shoots the shambling corpses. Only Harris and Jill are found still alive, but Harris is killed by flying shards of glass. Jill, having shown signs of possession since the funeral, finally attacks Liza. John is forced to kill Jill.

Overcome by the zombies, John and Liza rush down a set of stairs to escape but find themselves back in the basement of the hotel. Stuck in an impossible situation, John and Liza move forward through the flooded labyrinth. They stumble into a supernatural wasteland of dust and corpses depicted in one of Schweicks paintings. They cannot escape, always finding themselves back at their starting point. They are ultimately blinded just like Emily, succumb to the darkness, and disappear.

==Cast==
* Catriona MacColl as Liza Merril
* David Warbeck as Dr. John McCabe
* Cinzia Monreale as Emily
* Antoine Saint-John as Schweick
* Veronica Lazar as Martha
* Larry Ray as Larry
* Giovanni De Nava as Joe the Plumber
* Al Cliver as Dr. Harris
* Michele Mirabella as Martin Avery
* Gianpaolo Saccarola as Arthur
* Maria Pia Marsala as Jill
* Laura De Marchi as Mary-Anne

==Production== metaphysical concepts — in particular, the ways in which the realms of both the living and the dead might bleed into each other. Fulci also wanted to do a film that would pay homage to his idol, the French playwright Antonin Artaud. Artaud, a sometime member of the early 20th Century Surrealist movement, envisioned theatre being less about linear plot and more about "cruel" imagery and symbolism that could shock its audience into action. 

Thus, Fulcis original outline for The Beyond was of a non-linear haunted house story with the only solid plot element being that of a woman moving into a hotel built on one of the seven gates of hell (another such gate is depicted in City of the Living Dead). This original story focused on the dead leaving hell and entering the hotel with little outside the ensuing carnage to link the scenes together. 
 German distribution oneiric incoherence.  
 Otis House at Fairview-Riverside State Park was used as the main location, the Seven Doors Hotel. 

==Themes==
According to Fulci, the ending of The Beyond is not a happy ending but at the same time not an unhappy one. The other-dimensional realm that the main characters find themselves trapped in at the end is, according to Fulci, a refuge of sorts that exists outside time and space. Some fans have taken the notion that the realm is a sort of purgatory for souls, with the presence of the blind girl as evidence of it. 

==Release==
 
The film was originally released theatrically in the UK in 1981 and received extensive BBFC cuts to scenes of violence, notably the assorted eye gouge scenes, the opening whipping sequence, and the killings of Emily and Martin by (respectively) dog and tarantulas. It later spent some time on the video nasty list before being removed without prosecution, and all VHS releases featured the same cut cinema print. It was finally passed fully uncut in 2001 and released on DVD on the Vipco label.

Though it was released in Europe in 1981, The Beyond did not see a U.S. release until 1983 through Aquarius Releasing. The film was released to theaters for a brief theatrical run under the alternate title, Seven Doors of Death. Besides changing the name of the film, the film was heavily edited to tone down the films graphic murder sequences with a new musical score. This alternately titled re-edited version was quickly released on video by Thriller Video.

As years went on, demand for a high quality, official uncensored release of The Beyond grew considerably, especially as the VHS copies under the title of Seven Doors of Death went out of print and became next to impossible to find.

In the mid-1990s, Bob Murawski and Sage Stallone of Grindhouse Releasing went to Italy and met with director Lucio Fulci (and subsequently with his daughter) in order to obtain the rights to re-master and distribute the film. Murawski and Stallone had completely digitally remastered and produced the DVD, uncut and completely uncensored, and meticulously curated all the numerous bonus materials. In order to receive a wider audience, filmmaker and distributor Quentin Tarantino lent his name to the finished DVD and it was re-released through a division of Tarantinos Rolling Thunder Production Company and Miramax Films. The Beyond played throughout the U.S. as a midnight movie feature and was the highest earning film for Rolling Thunder at that time. The film has since been continuously re-released solely by Grindhouse Releasing, the official licensed distributor of the film in North America.

===DVD and Blu-ray releases===
On 10 October 2000, Grindhouse Releasing co-distributed the film in collaboration with Anchor Bay Entertainment on DVD in both a limited edition tin-box set, and a standard DVD. There were only 20,000 limited edition sets released for purchase. The limited edition set was packaged in a tin box with alternative cover artwork, including an informative booklet on the films production as well as various miniature poster replications.

The film has since been continuously re-released by Grindhouse Releasing, the sole official licensed distributor of the film in North America. Grindhouse Releasing is currently working on an upcoming Blu-ray release of the film.

In March 2011, a Blu-ray Disc#Region codes|region-free Blu-ray of the film was released in the UK by the distributor Arrow Films, followed by a limited edition steelbook re-release on October 21, 2013. 
 Music Box Theatre in Chicago, Illinois. 

== Critical reception ==
 Time Out, on the other hand, called it "a shamelessly artless horror movie whose senseless story – a girl inherits a spooky, seedy hotel which just happens to have one of the seven doors of Hell in its cellar – is merely an excuse for a poorly connected series of sadistic tableaux of torture and gore."  Film critic Roger Ebert gave the film half a star out of four, writing, "The movie is being revived around the country for midnight cult showings. Midnight is not late enough." 
 Bravo Televisions Time Out conducted a poll with several authors, directors, actors and critics who have worked within the horror genre to vote for their top horror films.  The Beyond placed at number 49 on their top 100 list. 

==References==
 

Thrower, Stephen. Beyond Terror: The Films of Lucio Fulci, Fab Press, 2002. ISBN 0-9529260-6-7

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 