Roadside Ambanis
 
{{Infobox film
| name           = Roadside Ambanis
| image          = Roadside Ambanis.jpg
| alt            =
| caption        =
| director       = Kamal Sethu
| producer       = Kamal Sethu
| writer         = Kamal Sethu 
| starring       = D. Praveen Himaj Kumar Vijay Kumar Pandha Pasupathi Maha Laxmi Rajagopal Mani Kandan Deepak Yuva Rani
| music          = Mamiboys
| cinematography = Richard Prasad
| editing        = B. Lenin
| studio         = Grape Pictures
| released       =  
| runtime        = 19 minutes
| country        = India
| language       = Tamil
| budget         = Rs.65000
}}
Roadside Ambanis (English: Roadside Entrepreneurs) is an Indian live action short film. The films run time is approximately 19 minutes. It was written and directed by first-timer Kamal Sethu, and produced by Grape Pictures.

==Plot==
Kannan and Rajesh are two young and impoverished school dropouts who aspire to start their own Cycle Repair Shop. Both plan to conduct a Cricket Tournament to make the money required to rent deposit a premises for the shop. Fate intervenes and their friendship is tested. Does Kannan get to start the business or does Rajesh continue his studies? This forms the climax of this film.

==Awards & Official Selections==
* Best Short Film at the 3rd Norway Tamil Film Festival.  On 27 April 2012. Oslo, Norway.
* Official Selection at the 3rd Winter Film Awards Festival.  On 27 February 2014. New York, USA.
* Official Selection at the 6th CMS International Childrens Film Festival.  On 7 April 2014. Lucknow, India.
* Official Selection at the 11th Seoul International Agape Film Festival.  On 22 May 2014. Seoul, South Korea.
* Official Selection at the 3rd Figari Film Festival.  On 4 July 2014. Golfo Aranci, Italy.
* Official Selection at the 9th Harlem International Film Festival.  On 13 September 2014. New York, USA.
* Official Selection at the 8th International Childrens Film Festival.  On 24th January 2015. Dhaka, Bangladesh.

==References==
 

==External links==
*  

 
 
 


 