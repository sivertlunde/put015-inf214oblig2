Left Luggage (film)
{{Infobox film
| name           = Left Luggage
| image          = Left luggage cover.jpg
| caption        = DVD cover of Left Luggage
| director       = Jeroen Krabbé
| producer       = Edwin de Vries Craig Haffner
| writer         = Edwin de Vries Carl Friedman (book)
| starring       = Laura Fraser Adam Monty Isabella Rossellini Jeroen Krabbé
| music          = Keith Allison Henny Vrienten
| cinematography = Walther van den Ende
| editing        = Edgar Burcksen
| distributor    = Polygram Filmed Entertainment, Buena Vista International
| released       =  
| runtime        = 100 minutes
| country        = Netherlands Yiddish
| budget         =
}}
Left Luggage is a 1998 Dutch film directed by Jeroen Krabbé.

==Plot==
While escaping from Nazis during World War II, a Jewish man buries two suitcases full of things dear to his heart in the ground. The war deprived him of his family, and afterwards he endlessly turns over the soil of Antwerp to find the suitcases, an obsessive compulsion. He keeps checking old maps and keeps digging, trying to find what he lost. His daughter Chaya is a beautiful modern girl looking for a part-time job. She finds a place as a nanny in the strictly observant hassidic family with many children, although her secular manners clearly fly in the face of their beliefs. One of the reasons she is accepted is that mother of the family is absolutely overburdened by the household, so Chaya stays despite the resistance of the father, who is normally an indisputable authority in the family.

She develops a special bond with the youngest of the boys, four-year-old Simcha, who seems incapable of speaking. She encourages him to speak while walking in the park, and it appears that, after some coaching from Chaya (who needs coaching herself) during the upcoming Passover Seder, Simcha will be able to chant the section of the Haggadah usually reserved for the youngest speaking participant - the Four Questions.

At first, Simchas nerves prevent him from chanting, and his brothers begin to chant instead. Simcha finally lifts his voice. The entire family, including Chaya, applauds his efforts, but his judgemental father does not recognize this great step, but instead criticizes the boy for a mistake. Chaya confronts the father, and in the process, discovers his own pain as a Holocaust survivor, and begins to understand her own parents grief.

The anti-Semitic superintendent of the building is a constant problem for the entire family and now for Chaya. However, as opposed to the observant Jews, she refuses to be a victim and does not put up with his anti-Semitic tricks. She fights him, thus exciting the childrens admiration and fathers wrath.

Unfortunately, walks with Simcha end in a tragedy: after sneaking to the park, he drowns in the pond, while chasing the ducks he loved so much. Some in the community hold Chaya responsible for his death. However, in a scene where Chaya goes to the familys mourning service, the mother feels compassion for Chaya and realizes that Chaya felt a deep connection with Simcha. As an act of acceptance, his mother rips Chayas shirt, which is a sign of a mourner (a sibling, parent, child or spouse of the deceased) in Jewish tradition.

The boys father finally, albeit silently, acknowledges Chayas connection with Simcha when she observes the graveside service.

Chayas experience allows her to finally accept her parents past, and to embrace her own Jewishness.

The film is a commentary not only on external (gentile) anti-Semitism, but also on the lack of connection and self-acceptance of assimilated Jews.

==Cast==
*Laura Fraser - Chaya Silberschmidt
*Adam Monty - Simcha Kalman
*Isabella Rossellini - Mrs. Kalman
*Jeroen Krabbé - Mr. Kalman
*Chaim Topol - Yacov Apfelschnitt
*Marianne Sägebrecht - Mrs. Silberschmidt
*Maximilian Schell - Mr. Silberschmidt
*Koen De Bouw - Mr. Silberschmidt (at age of 20) David Bradley - Concierge
*Heather Weeks - Sofie
*Miriam Margolyes - Mrs. Goldman
*Lex Goudsmit - Mr. Goldman

==Awards==
The film was entered into the 48th Berlin International Film Festival, where the film won the Blue Angel Award and Isabella Rossellini won an Honourable Mention.   

==See also==
*List of Holocaust films

==References==
 

==External links==
* 
*  Clubcard TV
* 

 
 
 
 
 
 
 
 
 