Small Claims: The Reunion
{{Infobox film
| name           = Small Claims: The Reunion
| image          = 
| alt            = 
| caption        = 
| director       = Tony Tilse
| producer       = Rosemary Blight  Ben Grant Sue Masters
| writer         = Kaye Bendle Keith Thompson
| screenplay     = 
| story          = 
| based on       =  
| starring       = Claudia Karvan Rebecca Gibney  Lisa Chappell
| music          =  Anna Howard 
| editing        = Mark Perry
| studio         = Goalpost Pictures
| distributor    = Network Ten
| released       =  
| runtime        = 
| country        = Australia
| language       = English
| budget         = 
| gross          =  
}}

Small Claims: The Reunion is an   in 2006. The film was a co-production with subscription television and was also broadcast on the Foxtel, Austar, and Optus Television Subscription Television services.  The series was written by husband and wife team, Keith Thompson and Kaye Bendle. 

This is part one of a mystery series about two overworked young mums, de-skilled beyond their worst nightmares, who become a formidable pair of sleuths, directed by Cherie Nowlan.  Their cases are the murders, greed and dark passions that lurk behind the anonymous facade of the suburbs. 

==Cast==
* Rebecca Gibney, as Chrissy Hindmarsh
* Claudia Karvan, as Jo Collins
* Lisa Chappell, as Louise Page
* Simon Burke, as Jon
* Alison Whyte, as Pip
* Maggie Dence, as Roma
* Peter Phelps, as Paul Loyd Emma Booth, as Annie Steve Vidler, as Ross Page
* Daisy Betts, as Amber
* Gyton Grantley, as Detective Senior Constable Brett
* Wayne Blair, as Detective Senior Constable Lacey

==See also==
* Australian films of 2004
* Cinema of Australia
* List of films shot in Sydney
* List of Australian films
*  
*   Website

==References==
 

==External links==
*  


 
 