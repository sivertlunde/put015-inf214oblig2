Caliber 9
{{Infobox film
| name          = Caliber 9
| image         = Milano calibro 9.jpg
| image_size    =
| caption       = German poster for Milano calibro 9
| director      = Fernando Di Leo
| producer      = Armando Novelli
| writer        = Fernando Di Leo Frank Wolff Luigi Pistilli
| music         = Luis Enríquez Bacalov Osanna
| cinematography= Franco Villa
| released      =  
| runtime       = 100 minutes
| country       = Italy Italian
}} Italian crime film specialist Fernando Di Leo in 1972 in film|1972. The film is based on a novel of the same name written by Giorgio Scerbanenco.  The soundtrack for the film, Preludio Tema Variazioni e Canzona, is a collaboration album between Luis Enríquez Bacalov and the Italian progressive rock group Osanna. 
 1972 and by Il boss (The Boss) in 1973 in film|1973. 

==Plot==
Small-time gangster Ugo Piazza (Gastone Moschin) is released from prison. He tries to convince the police, the mafia, his psychotic ex-friend Rocco (Mario Adorf) and his girlfriend Nelly Bordon (Barbara Bouchet) that he wants to go straight, but everyone believes he has $300,000 of stolen money hidden somewhere.

==Cast==
* Gastone Moschin: Ugo Piazza
* Barbara Bouchet: Nelly Bordon
* Mario Adorf: Rocco Musco Frank Wolff: Police Commissioner
* Luigi Pistilli: Inspector Mercuri
* Ivo Garrani: Don Vincenzo Philippe Leroy: Chino
* Lionel Stander: The "American"
* Salvatore Aricò: Luca 
* Ernesto Colli: Alfredo Bertolon

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 


 
 