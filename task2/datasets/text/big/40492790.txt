Desire (2000 film)
{{Infobox film
 | name = Desire
 | image_size =
 | caption =
 | director = Colleen Murphy
 | producer =  
 | writer = Colleen Murphy
 | narrator =
 | starring =  
 | music =  
 | cinematography = Christophe Bonnière
 | editing =
 | studio =  
 | distributor =
 | released = 10 September 2000 (Canada)
 | runtime = 97 min. Canada
 English
 | budget =
 | gross =
 | preceded_by =
 | followed_by =
 | image =
}} 

Desire is a Canadian drama film, released in 2000. It was directed and written by Colleen Murphy.

==Plot==
The film stars Zachary Bennett as Francis Waterson, an aspiring concert pianist, and Katja Riemann as Halley Fischer, an elementary school teacher with whom Francis enters a romantic relationship.

==Awards and nominations==
 Best Actress Best Actor nomination for Zachary Bennett.

==External links==
*  

 
 
 
 

 