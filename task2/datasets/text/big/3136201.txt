Lion of the Desert
{{Infobox film
 | film name = Lion of the Desert
 | image = Lion_of_the_Desert_DVD_Cover.jpg
 | caption = Lion of the Desert DVD cover
 | director = Moustapha Akkad
 | producer = Moustapha Akkad
 | writer = H.A.L. Craig
 | music = Maurice Jarre
 | starring = Anthony Quinn Oliver Reed Rod Steiger Raf Vallone
 | distributor =Anchor Bay Entertainment
 | budget =  US$35 million 
 | released =  
 | runtime = 206 minutes
 | language = English
}} historical action government under Muammar Gaddafi.  Released in May 1981, the film was liked by critics and audiences   but performed poorly financially, bringing in just $1 million net worldwide.  . The film was forbidden in Italy in 1982 and was only shown on pay TV in 2009.

== Plot == Italian dictator Italian colonization The Fourth Libya with the might of the Italian Army. Tanks and aircraft are used in the desert for the first time. The Italians also committed atrocities: killing of prisoners of war, destruction of crops, and imprisoning populations in concentration camps behind barbed wire. 
 Libyan Arabs Berbers suffered heavy losses, their relatively primitive weaponry was no match for mechanised warfare; despite all this, they continued to fight, and managed to keep the Italians from achieving complete victory for 20 years. Graziani was only able to achieve victory through deceit, deception, violation of the laws of war and human rights, and by the use of tanks and aircraft. 

Despite their lack of modern weaponry, Graziani recognised the skill of his adversary in waging guerrilla warfare. In one scene, Mukhtar refuses to kill a defenseless young officer, instead giving him the Italian flag to return with. Mukhtar says that Islam forbids him to kill captured soldiers and demands that he only fight for his homeland, and that Muslims are taught to hate war itself.

In the end, Mukhtar is captured and tried as a rebel. His lawyer states that since Mukhtar had never accepted Italian rule, he cannot be tried as a rebel, and instead must be treated as a prisoner of war (which would save him from being hanged). The judge rejects this, and the film ends with Mukthar being publicly executed by hanging.

== Cast ==
  with Oliver Reed, on the set of Lion of the Desert.]]

*Anthony Quinn as Omar Mukhtar. General Graziani.
*Irene Papas as Mabrouka.
*Raf Vallone as Diodiece.
*Rod Steiger as Benito Mussolini|Mussolini. El Gariani.
*Andrew Keir as Salem.
*Gastone Moschin as Tomelli.
*Stefano Patrizi as Sandrini.
*Adolfo Lastretti as Sarsani.
*Sky Dumont as Prince Amedeo, Duke of Aosta|Amedeo.
*Takis Emmanuel as Bu-Matari.
*Rodolfo Bigotti as Ismail. Robert Brown as Al Fadeel.
*Eleonora Stathopoulou as Alis Mother.
*Luciano Bartoli as Captain Lontano.
*Claudio Goro as Court President.
*Giordano Falzoni as Judge at Camp.
*Franco Fantasia as Grazianis Aide.
*Ihab Werfaly as Ali.

==Music==

The musical score of Lion Of The Desert was composed and conducted by Maurice Jarre, and it was performed by the London Symphony Orchestra.

==Soundtrack==

Track Listing for the First Release on LP

SIDE One
# Omar the Teacher / Italian Invasion / Resistance / The Lion of the Desert (19:12)

SIDE Two
# The Displacement / The Concentration Camp / The Death/March of Freedom (19:33) 



Track Listing for the First Release on CD

# Omar the Teacher (04:26)
# Prelude: Libya 1929 (02:24)
# The Execution of Hamid (05:04)
# Desert Ambush (01:46)
# Omar Enters Camp (04:15)
# The Empty Saddle (01:49)
# March to Demination (05:19)
# Ismails Sacrifice (02:36)
# I Must Go (02:27)
# Grazianis Triumph (01:41)
# Entracte (02:19)
# Concentration Camp (03:15)
# Italian Invasion (01:32)
# Starvation (00:53)
# The Hanging (01:27)
# General Graziani (03:00)
# Charge (01:23)
# Phoney Triumph (04:38)
# Omars Wife (03:22)
# Omar Taken (02:38)
# The Death of Omar (01:38)
# March of Freedom (With Choir) (03:59)

== Censorship in Italy == Parliament to Chamber of Deputies. 

The movie was finally broadcast on television in Italy by Sky Italy on June 11, 2009 during the official visit to Italy of Libyas then leader Muammar Gaddafi.

== Reception ==
Cinema historian Stuart Galbraith IV writes about the movie: "A fascinating look inside a facet of Arab culture profoundly significant yet virtually unknown outside North Africa and the Arab world. Lion of the Desert is a Spartacus-style, David vs. Goliath tale that deserves more respect than it has to date. Its not a great film, but by the end it becomes a compelling one." 

The verdict of British historian Alex von Tunzelmann about the movie is: "Omar Mukhtar has been adopted as a figurehead by many Libyan political movements, including both Gaddafi himself and the rebels currently fighting him. Lion of the Desert is half an hour too long and hammy in places, but its depiction of Italian colonialism and Libyan resistance is broadly accurate." 
 Film critic Vincent Canby writes: "Spectacular… virtually an unending series of big battle scenes." 

Clint Morris comments the movie as: "A grand epic adventure thatll stand as a highpoint in the producing career of Moustapha Akkad." 

== See also ==
*Libyan resistance movement
*Italian Libya


== References ==
 

== External links ==
*  
*   ( )

 

 
 
 
 
 
 
 
 
 
 
 
 
 