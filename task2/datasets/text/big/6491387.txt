Dr. Giggles
{{Infobox Film |
   name        = Dr. Giggles |
   image       = Dr giggles poster.jpg |
   caption     = Theatrical Release Poster|
   writer      = Manny Coto | Keith Diamond Richard Bradford |
   director    = Manny Coto |
   producer    = Stuart M. Besser |
   writer      = Manny Coto and Graeme Whifler | Brian May |
   cinematography = Robert Draper |
   editing     = Debra Neil-Fisher |
   released    = October 23, 1992 | Largo Entertainment JVC Entertainment.inc  Dark Horse Entertainment |
   distributor = Universal Studios |
   runtime     = 96 minutes |
   budget      = Unknown |
   gross       = $8,403,433 }}

Dr. Giggles is a 1992 horror film directed by Manny Coto, and starring Larry Drake as the titular antagonist and Holly Marie Combs as the protagonist.  The film co-stars Cliff DeYoung and Glenn Quinn.  It was released on October 23, 1992. 

==Plot==

In the town of Moorehigh in 1957, the patients of Dr. Evan Rendell kept disappearing. After some investigation, the citizens of Moorehigh found that he and his son, Evan, Jr. (nicknamed "Dr. Giggles" for his hideous laugh), were ripping out patients hearts in an attempt to bring back the doctors dead wife.  The townspeople stone Dr. Rendell to death, but Evan, Jr. disappeared.

Meanwhile, Dr. Giggles breaks into his fathers abandoned office and starts going through the doctors old files, gathering a list of names.

Officer Hank Magruder goes to investigate Jennifers house, and finds her father there, lying in a pool of blood. Giggles attacks and kills Magruder, but not before Magruder seriously wounds him in the side with a bullet. Reitz arrives soon after, finding his partner dead and Jennifers father wounded but alive. Meanwhile, Giggles returns to his hideout, performing surgery on himself to remove the bullet.
 breaks the fourth wall, staring at the camera and asking, "Is there a doctor in the house?" before dying.

Recovering in the hospital, Jennifer is visited by her also-recovering father, and by Max.

==Cast==

Larry Drake as Doctor Evan Rendell Jr. (Dr. Giggles)

Holly Marie Combs as Jennifer Campbell

Cliff DeYoung as Tom Campbell

Glenn Quinn as Max Anderson
 Keith Diamond as Officer Joe Reitz
 Richard Bradford as Officer Hank Magruder
 Michelle Johnson as Tamara

John Vickery as Dr. Chamberlain

Nancy Fish as Elaine Henderson

Sara Melson as Coreen

Zoe Trilling as Normi

Darin Heames as Stu

Deborah Tucker as Dianne

Doug E. Doug as Trotter Denise Barnes as Leigh

Nick Joseph Mastrandrea as Young Evan Rendell Jr./Stus brother

==Release==
The original release was on October 23, 1992 and the re-release on December 12, 2009 at New Beverly Cinema in Los Angeles.  

==Reception==
Dr. Giggles earned poor reviews from critics and currently holds a 27% rating on Rotten Tomatoes.

Variety (magazine)|Variety gave the film a negative review calling it a "wildly uneven horror film" noting that "More care in scripting and fewer cheap yocks could have resulted in a viable new paranoid horror myth"   Vincent Canby also criticized the script in his review for The New York Times stating "The screenplay is stitched together from variations on cliches used by or about the medical community."   The Washington Post noted that "Manny Coto turns to co-writer Graeme Whifler time and again for punch lines in a desperate attempt to revive a script that begins in critical condition and ends up DOA." 

==Filming Locations== Metzger Oregon. 

==References==
 

==External links==
*  
*  
*  
*  
*  

 

  
 
 
 
 
 
 
 
 