Gordon's War
{{Infobox film
| name        = Gordons War
| image       = Gordons_war_poster_01.jpg
| writer      = Howard Friedlander, Ed Spielman
| starring    = Paul Winfield
| director    = Ossie Davis
| producer    = Robert Schaffel
| studio      = Palomar Pictures
| distributor = 20th Century Fox
| released    =  
| runtime     = 90 minutes
| country     = United States
| language    = English
| music       = Angelo Badalamenti (as Andy Badale), Al Elias
| cinematography= Victor J. Kemper
| budget      = $1.6 million 
| gross       = $1,250,000 (US/ Canada rentals) 
}} written by directed by Ossie Davis. It stars Paul Winfield as Gordon Hudson.

==Synopsis==
A Vietnam veteran returns home to find drug dealers and addicts now rule his old neighborhood, and that even his own wife has fallen victim to drugs. Together with three of his buddies from Vietnam, he fights back.

==Cast==
*Paul Winfield - Gordon Hudson Carl Lee - Bee Bishop
*David Downing - Otis Russell Tony King - Roy Green  Gilbert Lewis - Spanish Harry Carl Gordon - Luther the Pimp
*Nathan Heard - Big Pink
*Grace Jones - Mary 
*Jackie Page - Bedroom Girl 
*Chuck Bergansky - Caucasian killer Adam Wade - Hustler
*Hansford Rowe - Dog Salesman 
*Warren Taurien - Goose Ralph Wilcox - Black hit man  David Connell - Hotel Proprieter
*Rochelle LeNoir - Gordons wife
*Charles McGregor - Drug dealer on subway station platform

==Soundtrack== New Birth Public Enemy, Coldcut and Blade (rapper)|Blade.

==References==
 

==External links==
* 
*   at Rottentomatoes.com

 

 
 
 
 
 
 
 
 


 