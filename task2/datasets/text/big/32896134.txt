Fisher's Ghost (film)
 
 
{{Infobox film
| name           = Fishers Ghost
| image          =
| caption        =
| director       = Raymond Longford
| producer       = Lottie Lyell Charles Perry
| writer         = Raymond Longford  Lottie Lyell
| starring       =
| music          =
| cinematography = Arthur Higgins
| studio         = Longford-Lyell Productions
| editing        =
| distributor    = Hoyts
| released       = 4 October 1924
| runtime        = 55 minutes (5000 feet) "Raymond Longford", Cinema Papers, January 1974 p51 
| country        = Australia
| language       = Silent film  English intertitles
| budget         = ₤1,000 
}}

Fishers Ghost is a 1924 Australian silent film directed by Raymond Longford based on the legend of Fishers Ghost. It is considered a lost film.

==Synopsis==
The film is set in 1820s New South Wales. Two transported convicts,George Worrall and Frederick Fisher, are released and take up farms at Campbelltown. They are both successful and become friends. Worrall persuades Fisher to go on a trip to England and says he will manage Fishers farm. A few months later, Worrall goes to an estate agent with a letter from Fisher saying that he has decided to stay in England and has instructed Worrall to sell his farm.

In 1826, a settler called Farley sees an apparition who purports to be Fisher sitting on a three rail fence. This apparition claims he was murdered by Worrall and later indicates where Fishers body lays. Worrall is arrested at his wedding to a girl who does not return his affections. He is tried, convicted and sentenced to death. He eventually confesses to the crime.  

==Cast==
*Robert Purdie as George Worrall
*Fred Twitcham as Fisher
*Lorraine Esmond
*Percy Walshe
*William Ryan
*Ted Ayr
*William Coulter
*Charles Keegan
*Ruby Dellew
*Ada St. Claire
*Charlotte Beaumont
*Ike Beck

==Production==
Raymond Longford and Lottie Lyell, in association with Charles Perry, formed a new company together: Longford-Lyell Productions. Fishers Ghost marks the production companys first film. Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, 122.     It was shot on location in Campbelltown, and Longford sought the advice of Campbelltown residents and also explored the records on the subject from the local Mitchell Library.  The film was completed by August 1924.   Fishers Ghost, The Bushwhackers (1925), and Peter Vernons Silence (1925) were the only three films produced by Longford-Lyell Productions as the company had already entered liquidation in June 1924, even before the films release.   Although Lottie Lyell and Raymond Longford created many films together, Fishers Ghost and The Bushwhackers are the only films for which Lyell received credit as scriptwriter and assistant director before her death from tuberculosis in 1925. 

==Reception==
The film is attributed to being one of the earliest and influential Australian horror films,  paving the way for the resurgence of the genre in the 1970s after the Australian government began funding their movie industry. 

Union Theaters rejected the film be released in their Sydney theaters because their managing director, Stuart F. Doyle, claimed the film was "too gruesome" for the public. The film was shown in Hoyt theaters and yielded ₤1,300 in its first week of screenings. 

In 1934 Longford registered a script for a remake of the film.  However it was never made.

In 2010, Tony Buckley, a producer who helped find and restore the 1971 Australian film Wake in Fright, called for a Film Search program to locate the lost negatives of Fishers Ghost as well as other historic Australian films. 

==References==
 

==External links==
* 
*  at National Film and Sound Archive
*  at AustLit
* , a detailed summary of the films plot

 

 
 
 
 
 
 
 