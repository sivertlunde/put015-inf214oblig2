Shiva Sainya
{{Infobox film|
| name = Shiva Sainya
| image = 
| caption =
| director = Shivamani
| writer = Shivamani
| starring = Shivrajkumar  Nivedita Jain   Arundhati Nag
| producer = Y. S. Ramesh
| music = Ilaiyaraja
| cinematography = Krishna Kumar
| editing = K. Balu
| studio = Yashi Enterprises
| released =  
| runtime = 138 minutes
| language = Kannada
| country = India
| budgeBold textt =
}}
 Kannada action action drama film directed by Shivamani and produced by Y. S. Ramesh. The film features Shivarajkumar and Nivedita Jain in the lead roles.  

The films score and soundtrack was scored by Ilaiyaraja and the cinematography was by Krishna Kumar.

== Cast ==
* Shivarajkumar 
* Nivedita Jain
* Arundhati Nag
* C. R. Simha
* Doddanna
* Somashekhar Rao
* Mukhyamantri Chandru
* Lokanath
* Master Anand
* Dheerendra Gopal
* Satyabhama
* Mandeep Roy

== Soundtrack ==
The soundtrack of the film was composed by Ilaiyaraja. 

{{track listing 
| headline = Track listing
| extra_column = Singer(s)
| lyrics_credits = yes
| collapsed = no
| title1 = Chikkamagaloora Chikkamallige
| extra1 = Ilaiyaraja & K. S. Chithra
| lyrics1 = Shyamsundar Kulkarni
| length1 = 04:51
| title2 = O Mere Pyare Mano & K. S. Chithra
| lyrics2 = V. Manohar
| length2 = 05:44
| title3 = Raja Namma Rajyakke Mano
| lyrics3 = V. Manohar
| length3 = 04:32
| title4 = Om Shanthi Om Shanthi Mano 
| lyrics4 = V. Manohar
| length4 = 05:05
| title5 = Jailali Hutti Mano 
| lyrics5 = Ilaiyaraja
| length5 = 05:16
| title6 = Are Laila Laila Mano 
| lyrics6 = Sriranga
| length6 = 04:59
| title7 = Yuva Kranthiya
| extra7 = Chorus
| lyrics7 = Doddarangegowda
| length7 = 04:57
}}

== References ==
 

== External links ==
* 
*  

 
 
 
 
 
 


 

 