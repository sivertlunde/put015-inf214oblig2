Nobody, Nobody But... Juan
{{Infobox film
| name           = Nobody, Nobody But... Juan
| image          = 
| alt            =  
| caption        =  Enrico S. Quizon
| producer       = 
| writer         = 
| screenplay     = 
| story          =  Gloria Romero Eugene Domingo Pokwang
| music          = 
| cinematography = 
| editing        = 
| studio         =  RVQ Productions  Kaizz Ventures Inc. and Joe Aldeguer Productions 
| distributor    = 
| released       =  
| runtime        = 
| country        = Philippines
| language       = Filipino   Tagalog
| budget         = 
| gross          = 
}}
Nobody, Nobody But... Juan is a Philippine comedy film released on December 25, 2009 as an entry to the 2009 Metro Manila Film Festival.    The title is a take on the Wonder Girls song "Nobody (Wonder Girls song)|Nobody".

==Plot== Gloria Romero), Japanese occupation of Manila. Wowowee is Juans way of coping with homesickness and reliving the past. He usually creates alarms and scandals if he never watches Wowowee every day. He also has a son, who is a womanizer and has many children out of wedlock. 

When watching Wowowee is banned in the home, Juan takes drastic measures to watch his favorite TV program, from riots to hunger strikes. The last straw comes when he left the home and was caught by federal officers. He left the home and arrives in the Philippines with only his passport,plane tickets and pocket money. He arrives in Philippines, became a victim of a "fraudulent" taxi driver Leo Martinez, meets an American who loves Wowowee too and his wife, Chariz Solomon. In his quest,he crosses paths with his old friend Tu (Eddie Garcia) who used to be his partner in the vaudeville duo Juan Tu,that plays satiric, slapstick and prison comedy not only for rich Filipinos, but also for Japanese troops, one of them, an officer, Ya Chang, became a victim of a cream pie throwing joke. Tu now works with Lolay to embezzle money from audiences, especially foreigners by giving them "tickets" for a fee. He then meets Wowowee host Willie Revillame when he was dragged by the dancers, after he was tricked by Long Mejia and Brod Pete to fool the chasing guards. He tells Willie about the things that happened in his old age. After he told the story, he then sees Lolay (Pokwang) shouting his name. Lolay introduces him to Willie again and introduces Tu. The guards see Tu and chase him down. Juan and Lolay also chase Tu. Tu hides in a branch of Mang Inasal and orders a Jumbo Roasted Chicken. Juan finds him and tells him why he was being chased down. Tu confesses that they were scalpers and he knows where Aida is, but refuses to call him Tu,but Ribio. The guards and policemen eventually find Tu. Tu tells the guards that he would go to his family before going to jail.

Juan is then reunited with his long-lost love Aida, whom he found out had married Tu and Juan have a daughter Aida was pregnant during their last show before the Americans bombed Manila, including the theater they performed.The film ends when Juan decided to stay in Philippines for good, along with his daughter and oldest son,who works in the Philippines, fulfilling his promise on Tu, who was imprisoned due to estafa and Willie Revillame giving a message to Juan and to all his loyal watchers and fans. 

==Cast==
* Dolphy† as Juan
* Eddie Garcia  as Tu/Torribio Gloria Romero as Aida
* Pokwang as Lolay/Lolita 
* Eugene Domingo as Julie 
* Eric Quizon as Mario
* Vandolph as young Tu
* Epi Quizon as young Juan
* Heart Evangelista as young Aida
* G. Toengi as Jane
* Joe Vincent Aldeguer as himself/Joe
* Willie Revillame as himself
* Ya Chang as Japanese Officer
* Sahlee Quizon as Juans daughter, Juana
* Long Mejia as himself
* Caloy Alde as himself
* Chariz Solomon as TFC Subscriber
* (uncredited American man) as TFC Subscribers husband
* Keanna Reeves as the gossip monger maid
* Bearwin Meily as Security Guard

==References==
 

==External links==
*  

 
 
 
 
 