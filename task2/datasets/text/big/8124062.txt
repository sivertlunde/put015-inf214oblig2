The Noose Hangs High
{{Infobox film
| name           = The Noose Hangs High
| image          = noosehangshigh.jpg
| caption        = Theatrical release poster Charles Barton
| producer       = Charles Barton Howard Harris Charles Grayson
| starring       = Bud Abbott Lou Costello Cathy Downs Joseph Calleia
| music          = Irving Friedman
| cinematography =
| editing        = Harry Reynolds
| Studio         = Abbott & Costello Productions Inc
| distributor    = Eagle-Lion Films (1948, original) MGM (1993, VHS) & (2005, DVD)
| released       =  
| runtime        = 77 min.
| language       = English
| budget         = $610,933 
}}
The Noose Hangs High is a 1948 film starring the comedy team of Abbott and Costello. The film is a remake of the Universal Pictures film For Love or Money (1939). 

==Plot==
Ted Higgins (Bud Abbott) and Tommy Hinchcliffe (Lou Costello) work for the Speedy Service Window Washing Company.  They run into a bookie named Nick Craig (Joseph Calleia), who, after mistaking them for employees of the Speedy Messenger Service, sends them to Mr. Stewarts (Ben Weldon) office to collect $50,000 owed to him.  But Stewart has plans of his own: he hires two thugs to rob Ted and Tommy of the money he has just paid.  Tommy flees from the robbers and takes refuge in a room with a gaggle of women who are mailing face powder samples.  He hides the money in an envelope and addresses it to Craig, but it is accidentally switched with an envelope containing a powder sample.  Ted and Tommy return to Craigs office and explain what happened; they assure him that the cash will arrive in the mail the next day.

When face powder (instead of cash) arrives in the mail, an irate Craig gives Ted and Tommy 24 hours to return his money.  The boys attempt to contact everyone on the mailing list until they finally locate the recipient, Carol (Cathy Downs), who informs them that she spent most of the money and has only about $2,000 left.  The three of them go to the race track hoping to gamble the remaining cash to win enough money to pay back Craig.  They encounter a strange fellow named Julius Caesar (Leon Errol), who claims to have never lost a bet. They refuse to follow his betting advice, only to see his horse win, and they are left with nothing.  Ted, abandoning hope, decides that they would be safest in jail, so they run up a huge tab in a nightclub. Just as they are about to be arrested, Craig and his henchmen show up and demand the money. After Ted and Tommy reply that they do not have it, the thugs take them to a nearby construction warehouse and begin pouring cement in which to dump them.  Meanwhile, Carol and Caesar have been sitting at the bar, betting large amounts on fish at the clubs aquarium.  Caesar loses and hands her the $50,000 that she has just won, to her amazement. It turns out Caesar is actually an eccentric millionaire named J.C. MacBride, and they all arrive at the warehouse in time to pay back Craig.

==Production==
The Noose Hangs High was filmed from November 13 through December 10, 1947 under the terms of a new deal signed with Universal after completion of The Wistful Widow of Wagon Gap.  Those terms specified that Abbott and Costello were permitted to make one film a year with another company.  Universal intended to make this film with Abbott and Costello. However, under the terms of their contract, the duo decided to purchase the story from the studio and made it themselves at Eagle-Lion Films|Eagle-Lion, which had taken over the studios of Poverty Row studio Producers Releasing Corporation. 

The film was budgeted at $652,933 but came $41,900 under. 

==Routines== Hit the Ice), with Abbott going back and forth about whether they should get away or not with Costello taking his pants off and putting it on again as instructed.
*Mudder and Fodder, where Costello is explained the meaning of different types of horse.  In this case he mistakes a horse who can run well in the mud (a mudder) as mother and the food that is fed to a horse (its fodder) as father.  This routine was first used in It Aint Hay.
*Phone Booth, where Costello attempts to call Craig from a phone booth.  He is given a number where Craig can be reached and he calls it, unaware it is the phone booth next to him.  Craig answers the phone and they have an argument with each other, unaware that they are right next to each other. This routine was first used in Keep Em Flying.
*Youre 40, Shes 10 (previously used in Buck Privates), where Abbott tries to explain to Costello how a girl younger than him can get closer in age to him as they get older.
*Mustard (previously used in One Night in the Tropics)

==Home media==
Although filmed for Eagle-Lion, MGM Pictures currently owns the rights to this film and it is through them that this film was  released on DVD on May 17, 2005.

==References==
  

==External links==
* 

 
 

 
 
 
 
 
 
 
 