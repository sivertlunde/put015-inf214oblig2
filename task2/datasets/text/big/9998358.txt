Nazareno Cruz and the Wolf
{{Infobox film
| name           = Nazareno Cruz y el lobo
| image          = NazarenoCruzYElLobo.jpg
| caption        = Theatrical release poster
| director       = Leonardo Favio
| producer       = Leonardo Favio
| writer         = Leonardo Favio Jorge Zuhair Jury Juan Carlos Chiappe (radio program)
| starring       = Juan José Camero Marina Magali Alfredo Alcón Lautaro Murúa
| music          = Juan José García Caffi
| cinematography = Juan José Stagnaro
| editor         = Cristián Kaulen Antonio Ripoll
| studio         = Choila Producciones Cinematográficas
| distributor    = Producciones del Plata S.A.
| released       = 1975
| runtime        = 92 min.
| country        = Argentina Quechua
| budget         =
| gross          = 
}} Argentine fantasy film directed by Leonardo Favio and starring Juan José Camero and Alfredo Alcón. The story works as an adaptation of the classical myth of the Luison|Lobizón, and it has become a classic film. It is also widely known as the most successful of all time in its country. With 3.4 million viewers it holds the national record ahead of El secreto de sus ojos.  
 Best Foreign Language Film at the 48th Academy Awards, but was not accepted as a nominee.  It was also entered in the 9th Moscow International Film Festival.   

== Synopsis == the Devil) presents himself to Nazareno and explains that his curse is real. Mandinga makes Nazareno a proposition: if Nazareno gives up his love, he will receive in exchange his freedom and many riches. Nazareno refuses the deal and eventually turns into a werewolf, becoming involved in a series of tragedies.

== Cast ==
* Juan José Camero ... Nazareno Cruz
* Marina Magali ... Griselda The Powerful
* Lautaro Murúa ... Julián
* Nora Cullen ... The Lechiguana
* Elcira Olivera Garcés ... Damiana
* Saul Jarlip ... The Old Man Pancho
* Juanita Lara ... Fidelia
* Yolanda Mayorani ... The Powerfuls Godmother
* Marcelo Marcote ... The Child
* Josefina Faustín
* Augusto Kretschmar

== See also ==
* List of submissions to the 48th Academy Awards for Best Foreign Language Film
* List of Argentine submissions for the Academy Award for Best Foreign Language Film

== References ==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 

 
 