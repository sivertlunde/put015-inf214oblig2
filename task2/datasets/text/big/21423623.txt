Glorious 39
 
 
{{Infobox film
| name           = Glorious 39
| image          = Glorious thirty nine ver2.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Stephen Poliakoff
| producer       = {{Plainlist|
* Barney Reisz
* Martin Pope}}
| writer         = Stephen Poliakoff
| starring       = {{Plainlist|
* Romola Garai
* Bill Nighy
* Julie Christie
* David Tennant
* Christopher Lee}}
| music          = Adrian Johnston Danny Cohen
| editing        = Jason Krasucki
| studio         = {{Plainlist|
* BBC Films
* Magic Light Pictures
* Quickfire Films
* Screen East Content Investment Fund
* Talkback Thames
* UK Film Council}}
| distributor    = Momentum Pictures
| released       =  
| runtime        = 129 minutes  
| country        = United Kingdom
| language       = English
| budget         = Pound sterling|£3.7 million
}} war Thriller thriller film written and directed by Stephen Poliakoff. Starring Romola Garai, Bill Nighy, Julie Christie, Jeremy Northam, Christopher Lee, David Tennant, and Jenny Agutter, the film was released on 20 November 2009.

On the eve of World War II, as the formidable Keyes family tries to uphold its traditional way of life, daughter Anne (Garai) sees her life dramatically unravel when she stumbles upon secret recordings of the pro-appeasement movement. 

==Plot== House of Commons, Alexander (Bill Nighy) and mother, Maud (Jenny Agutter) had adopted her, however, Maud then gave birth to Ralph (Eddie Redmayne) and Celia. Michael is curious to learn what happened to Anne, which leads Walter to reminisce of the summer of 1939, at the Keyes estate in Norfolk.
 MP Hector (David Tennant) and lover, the reserved Lawrence (Charlie Cox) are present for the festivities. When Alexander arrives that night, he also brings a guest, the quiet government employee, Joseph Balcombe (Jeremy Northam). During dinner, Hector rants about Britains lack of action against Nazi Germany, noting that while his view is unpopular, he feels that it needs to be said. It is later revealed that he has been one of those calling out for a new prime minister.

The next day, while looking for a missing cat, Anne finds her in one of the propertys sheds, which had been off-limits due to them containing Alexanders manuscript papers. She finds gramophone records labelled "Foxtrot," which upon listening, actually contain recorded meetings and telephone conversations. Alexander reveals that he has allowed Balcombe to store government documents in the shed.

Two weeks later, Anne is notified that Hector has been found dead, of an apparent suicide. Anne wonders if Balcombe had anything to do with Hectors death. Alexander brushes off the idea, but does offer to ask Balcombe to remove the records from the shed, something he promises to do the next day during a picnic. While there, the picnic-goers take off for a walk, including Aunt Elizabeth (Julie Christie), leaving Anne to watch over baby Oliver. Anne awakens to find Oliver and his pushchair missing. She follows his cries to no avail, and when the family returns, they search, too, until they find him in his pushchair on a lane. Anne vehemently denies moving the baby, but the incident plants roots of doubt about Annes word.
 September 1, Anne gives a second record to her fellow actor and friend, Gilbert (Hugh Bonneville), who is subsequently found dead from an apparent suicide.

Anne travels back to Norfolk to keep Aunt Elizabeth company, where she listens to the second recording. On it, she recognizes Balcombes voice, along with another, her brother, Ralph. Ralph is heard suggesting the name "thin man dancing" for a covert operation.  The name he suggested is a reference to a childhood toy with which the siblings played. This confirmed to Anne that Ralph is not to be trusted. At a party in London, Anne attempts to tell Lawrence of Ralphs involvement, but he already knows. Lawrence convinces Anne to bring him the recording at a rendezvous at a suburban veterinary surgery. After Anne finds Lawrences body in a shed filled with euthanised pets, she is drugged by her father and taken prisoner in Aunt Elizabeths house which is close to St Pauls Cathedral. Balcombe pays her a visit, and brings with him the second recording, which was intercepted by him. He informs her that the recording had been made for her father, and that was why they were stored at the house in Norfolk. He also informs her that their house in London is being used for series of pro-appeasement meetings which her father is chairing.

Alexander later admits to her that he believes that Britain will be completely destroyed unless it secures a peace treaty with Germany, and that nothing should disturb that. Alexander tells her that she is the only member of the family that does not share his beliefs, which is why they are keeping her locked away and sedated. After some time, Maud unlocks the door to release her while the rest of the family is at the park. She goes past them, and when they act as though nothing wrong had happened, Anne runs away.

Back in the present, Walter tells Michael that Anne died in Canada 20 years ago, and that he was just doing what his family and Balcombe had wanted. It is then revealed that Balcombe convinced Walter to move Olivers pushchair into the lane. Michael asks Oliver and Walter to accompany him to meet his mother. They travel to the same park where Anne had last seen her family. A woman, Michaels mother, wheels an elderly woman towards them. That elderly woman is revealed to be Anne, and Michael tells them that that he knew the truth all along but wanted to hear it from them.

==Cast==
* Romola Garai as Anne Keyes, an actress and eldest Keyes child. Ralph refers to her as "Glorious".
* Bill Nighy as Sir Alexander, Annes father, Member of Parliament and World War I veteran.
* Julie Christie as Elizabeth, Annes aunt.
* David Tennant as Hector, a family friend and Member of Parliament
* Christopher Lee as Walter Page
** Sam Kubrick-Finney as young Walter Page
* Eddie Redmayne as Ralph, Annes brother
* Juno Temple as Celia, Annes sister 
* Jenny Agutter as Maud, Annes mother
* Hugh Bonneville as Gilbert Williams, an actor friend of Anne
* Charlie Cox as Lawrence, Annes lover
* Corin Redgrave as Oliver Page
* Jeremy Northam as Joseph Balcombe, a shady government operative
* Toby Regbo as Michael Walton, Annes grandnephew
* Muriel Pavlow as Old Anne Keyes

==Production==
 ]] Walsingham Abbey are featured prominently as a favourite haunt of the Keyes siblings. Other locations used include the Cley Marshes, Holkham Hall and Houghton Hall.

Filming began in late October 2008 and concluded in December 2008.    Much of the filming took place in Norfolk where the film is set. 

==Reviews==
* The Daily Mail: "For a thriller, the pace is much too sedate, and its easy to lose patience with a heroine who is this slow on the uptake. As a director of child actors, Poliakoff is hopeless. Worst of all, he seems not to have seen many movie thrillers in the past 20 years, for the pacing is pedestrian and the camerawork static, more redolent of bad episodes of Midsomer Murders than modern cinema. Verdict: Poor script and laboured direction sink fine leading performance." 
* The Guardian: "Stephen Poliakoffs pre-second world war conspiracy thriller never zips the way it should, but its still a solid, old-school entertainment." 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 