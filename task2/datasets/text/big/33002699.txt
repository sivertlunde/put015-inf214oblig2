Kis Kis Ki Kismat
{{Infobox film
| name           =Kis Kis Ki Kismat
| image          = Kis_Kis_Ki_Kismat.jpg
| image_size     = 
| caption        = DVD Cover
| director       = Govind Menon
| producer       = Vivek Nayak
| writer         = Govind Menon
| narrator       = 
| starring       =Dharmendra Mallika Sherawat
| music          = D. Imman
| cinematography = Uday Devare
| editing        = Ashmith Kunder
| distributor    = 
| released       = 22 October 2004
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

Kis Kis Ki Kismat  is a 2004 Bollywood sex comedy film directed by Govind Menon starring Dharmendra and Mallika Sherawat.  Supporting roles were played by Satish Shah, Tinnu Anand, Jagdeep, Viju Khote, Dinesh Hingoo and Shivaji Satam.  Tamil composer D. Imman composed the music, making his Bollywood debut.  The film received negative reviews.   

==Cast==
* Dharmendra as Hasmukh Mehta
* Mallika Sherawat as Meena Madhok
* Rati Agnihotri as Kokila Hasmukh Mehta Siddharth Makkar as Harish Mehta
* Satish Shah as Ahmed Rafsanjani
* Kurush Deboo as Khaled Mahmud
* Tinnu Anand as Aditya Sanganeria
* Dinesh Hingoo as Meenas maternal uncle
* Jagdeep as Dipankar Chattopadya
* Geeta Khanna as Lily
* Viju Khote as Gogi Dhanda / Inspector Dhandekar
* Tiku Talsania as Nimesh Popley
* Suresh Menon as Ramalingam
* Shivaji Satam as Minister
* Ajit Kulkarni as Mehtas partner
* Chetan Pandit as Fatman
* Manmauji

==References==
 

==External links==
*  

 
 
 


 