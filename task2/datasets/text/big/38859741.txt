Third Person (film)
Third romance film directed and written by Paul Haggis and starring Liam Neeson, Mila Kunis, Adrien Brody, Olivia Wilde, James Franco, Moran Atias, Kim Basinger, and Maria Bello.  The film premiered at the 2013 Toronto Film Festival. 

==Plot==
The film tells three inter-connected love stories that take place in Paris, New York and Rome.

Paris: Michael, a writer who recently left his wife Elaine, receives a visit from his lover Anna. The story explores their very complicated on/off relationship due to her inability to commit because of a terrible secret. 

New York: Julia, an ex-soap opera actress turned hotel maid is accused of harming her young naive child, a charge which she firmly denies. As a result of these charges, he is now in the custody of her husband Rick who is trying everything in his power to take the boy away from her. Julia, herself, is trying at all costs to regain custody of her son.
 gypsy (Roma) woman, Monika. Scott is inevitably drawn into a plot where he tries to free Monicas daughter who has been kidnapped by a Russian gangster and is being held for ransom. Emotions run high as the viewer and Scott question whether this is a set up or not.

==Cast==
* Liam Neeson as Michael
* Olivia Wilde as Anna 
* James Franco as Rick
* Mila Kunis as Julia
* Adrien Brody as Scott
* Moran Atias as Monika
* Maria Bello as Theresa
* Kim Basinger as Elaine
* Caroline Goodall as Dr. Gertner
* David Harewood as Jake Long
* Riccardo Scamarcio as Marco
* Loan Chabanol as Sam
* Patrick Duggan as NYC Hotel Bell Man

==Release==
The first international trailer of the film was released on April 15, 2014,  followed by a domestic poster on the next day.  The first US trailer was released on April 18.  The film was released in the U.S. on June 20, 2014.

==Reception==
Third Person received negative reviews from critics.  The film has a 23% approval rating on the   gave the film a rating of 37/100, based on 27 reviews. 

==Home media==
The film was released on DVD and Blu-ray Disc on September 30, 2014. 

== References ==
 

==External links==
 
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 