The Miracle Woman
{{Infobox film
| name           = The Miracle Woman
| image          = The Miracle Woman 1931 Poster.jpg
| image_size     = 267px
| border         = yes
| alt            = 
| caption        = theatrical release poster
| director       = Frank Capra
| producer       = Harry Cohn
| writer         = Dorothy Howell  
| screenplay     = Jo Swerling
| based on       =  
| starring       = {{Plainlist|
* Barbara Stanwyck
* David Manners
* Sam Hardy
}}
| music          =  Joseph Walker
| editing        = Maurice Wright
| studio         = Columbia Pictures
| distributor    = Columbia Pictures
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 John Meehan and Robert Riskin, the film is about a preachers daughter who becomes disillusioned by the mistreatment of her dying father by his church. Having grown cynical about religion, she teams up with a con man and performs fake miracles for profit. The love and trust of a blind man, however, restores her faith in God and her fellow man. The Miracle Woman was the second of five film collaborations between Capra with Stanwyck. Produced and distributed by Columbia Pictures, the film was reportedly inspired by the life of Aimee Semple McPherson.   

==Plot==
Florence Fallon (Barbara Stanwyck) is outraged when her minister father is fired after many years of selfless service to make way for a younger man. She tells the congregation what she thinks of their ingratitude. Her bitter, impassioned speech impresses Bob Hornsby (Sam Hardy), who convinces her to become a phony preacher for the donations they can squeeze out of gullible believers. She builds up a devoted national following. Then she meets a blind John Carson (David Manners), falls in love, and the sham comes tumbling down.

==Cast==
*Barbara Stanwyck as Florence Fallon
*David Manners as John Carson
*Sam Hardy as Bob Hornsby
*Beryl Mercer as Mrs Higgins
*Russell Hopton as Bill Welford Charles Middleton as Simpson
*Eddie Boland as Collins
*Thelma Hill as Gussie

==Themes==
The film shares themes with other Capra films, namely Mr. Smith Goes to Washington and Mr. Deeds Goes to Town in that the central character gives up power and fortune for the sake of their principles. What is different here is the gender roles are reversed, with the main character being a woman who is supported by the man who loves her.   

==References==
Notes
 

Bibliography
*  
*  

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 