Summer of Sam
 
{{Infobox film
| name = Summer of Sam
| image = Summer Of Sam (movie).jpg
| image_size = 220px
| border = yes
| alt = 
| caption = Theatrical release poster
| director = Spike Lee
| producer = Jon Kilik Spike Lee
| writer = Victor Colicchio Michael Imperioli Spike Lee
| starring = John Leguizamo Adrien Brody Mira Sorvino Jennifer Esposito Anthony LaPaglia
| music = Terence Blanchard
| editing = Barry Alexander Brown	 	
| cinematography = Ellen Kuras
| studio = Touchstone Pictures 40 Acres and a Mule Filmworks Buena Vista Pictures
| released =  
| runtime = 142 minutes  
| country = United States
| language = English Italian
| budget = $22 million
| gross = $19,288,130
}} crime Thriller thriller film Son of Sam serial murders. It was directed and produced by Spike Lee and stars John Leguizamo, Adrien Brody, Mira Sorvino, and Jennifer Esposito.

==Plot== Son of Sam who has shot several women in parked cars. In the Throgs Neck section of the Bronx hair dresser Vinny is married to waitress Dionna although he continues to carry on affairs with multiple women. One night, Dionna and Vinny are returning from a date when they pass by the police at one of the crime scenes. Vinny realizes that he was near the victims earlier that night while having sex with Dionnas cousin, and that the killer may have seen him.

Vinny is approached by Ritchie, an old friend. Ritchie has become part of the punk rock scene and his unusual dress and hairstyle concerns Vinny and others in the neighborhood. The only one who seems accepting of Ritchie is Ruby, Vinnys half-sister, who thinks he looks good. Ruby is initially shocked when she learns that Ritchie makes money by dancing and prostituting himself at a gay theater but the two become closer.

As the Son of Sam killings continue, tension rises in the neighborhood. Detective Petrocelli of the New York Police Department asks local mobster Luigi to help find the killer. Luigi and his men begin assembling a list of possible suspects. A group of men in the neighborhood also make a list of possible suspects, including Ritchie who they regard as "a freak".

Ruby joins a band with Ritchie and they get a gig at CBGB. They invite Vinny and Dionna but the couple are intimidated by the punks at the club and leave. Vinny and Dionna try to get into Studio 54 but are denied and instead end up at Platos Retreat where they engage in an orgy. On the ride home, Vinny berates his wife for her participation in the orgy, calling her a "freaky slut". Dionna tells Vinny that she knows about his prior infidelities including his having sex with her cousin. She angrily storms off and goes to stay at her parents house.

After another gig, Ritchie sees Vinny sitting in his car outside CBGB and sneaks up on him, pretends to be Son of Sam and startles Vinny. Returning to the neighborhood an increasingly tired and unstable Vinny meets with Dionna and apologizes to her, promising never to cheat again. However, she doesnt believe him and says she wants a divorce. The police publish an eyewitness sketch of the murderer and some of the local men think it looks like Ritchie. Vinny, distraught over his impending divorce, gets drunk.  A group of local men come by his house asking for his help to lure Ritchie into a trap so they can deliver him to the police. Vinny reluctantly goes along with it.

Unknown to Vinny and the men, the police have arrested David Berkowitz for the shootings. Vinny goes to Ritchies place and finds Ritchie and Ruby there. He lures Ritchie out of the house on the pretext of talking about his marriage but changes his mind and warns him to run. The other men emerge and attack Ritchie as Ruby look on horrified. Ritchies stepfather, Eddie, comes out of the house and holds the attackers at gunpoint. When they say that Ritchie is the Son of Sam, he tells them that the killer has already been captured. Unable to face Ritchie, Vinny walks away.

==Cast==
 
* John Leguizamo as Vinny
* Adrien Brody as Ritchie
* Mira Sorvino as Dionna
* Jennifer Esposito as Ruby
* Anthony LaPaglia as Detective Petrocelli
* Michael Rispoli as Joey
* Saverio Guerra as Woodstock
* Bebe Neuwirth as Gloria
* Patti LuPone as Helen Mike Starr as Eddie
* Roger Guenveur Smith as Detective Atwater
* Ben Gazzara as Luigi
* Arthur Nascarella as Mario
* Michael Imperioli as Midnite John Savage as Simon
* Michael Badalucco as David Berkowitz
* Spike Lee as John Jeffries
* John Turturro as Voice of Harvey the Dog
 

==Production== Country Club, Morris Park and Throggs Neck sections of the Bronx which David Berkowitz terrorized in 1977, with some scenes filmed in Brownsville, Brooklyn. Specifically, Maries Beauty Lounge, the salon where Vinny works, is a real, still active salon located on Morris Park Avenue, between Williamsbridge Road and Bronxdale Avenue. Most of Berkowitzs killings actually took place in Queens. The real CBGB was used, the band playing on stage, L.E.S. Stitches, is a contemporary punk band operating out of New Yorks Lower East Side.

Brodys nose was broken during the final climatic fight scene in which his character Richie is brutally beaten by his friends.  After they are refused entry into Studio 54, the sex scene between Dionna and Vinny included more explicit shots in the original cut. This scene was edited after the MPAA threatened the film with an "NC-17" rating. 

The role of Dionna was originally written with Jennifer Esposito in mind. The role of Ruby was originally offered to Sarah Michelle Gellar.  However a cast reshuffle ended with Mira Sorvino as Dionna and Esposito as Ruby. 

==Reception==
  Scorsese territory!" 

Summer of Sam has an overall approval rating of 50% on  , the film has a 67/100 rating, indicating "generally favorable reviews". 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 