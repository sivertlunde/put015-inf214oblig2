Trading Mom
{{Infobox film
| name = Trading Mom
| image = 
| image_size = 
| alt = 
| caption = 
| director = Tia Brelis
| producer = Raffaella De Laurentiis
| screenplay = Tia Brelis
| based on =  
| starring = Sissy Spacek Anna Chlumsky Aaron Michael Metchik Asher Metchik Maureen Stapleton
| music = David Kitay
| cinematography = Buzz Feitshans IV
| editing = Isaac Seyahek
| studio = First Look International
| distributor = Trimark Pictures
| released =  
| runtime = 85 minutes
| country = United States
| language = English
| budget = 
| gross = $319,123 
}}
Trading Mom (also released as The Mommy Market) is a 1994 Fantasy film|fantasy/comedy film written and directed by Tia Brelis, based on her mothers book. Starring Sissy Spacek and Anna Chlumsky, it features the final acting role of André the Giant, who died a year before it was released.

==Plot==
Elizabeth, Jeremy, and Harry Martin are three children who have had up to the edge with their mother, a nagging workaholic. Soon, they meet a mysterious gardener named Mrs. Cavour, who tells them of an ancient spell that will make her disappear. After saying the incantation one night, the next morning, she has disappeared along with their memories of her. Mrs. Cavour tells them of a place in town called the Mommy Market, where they have about every kind of mother one can think of. It has a policy, however, that one receives three tokens to take one of the mothers home, and if they do not find a suitable one by their third token, they can not return. Unfortunately for them, the mothers they pick: a snappy French woman, a nature-hiker, and a circus performer, do not sit well with them. Feeling lost without a guardian to look after them, the children soon learn that their real mother has been sent to the mommy market herself, where she has been all along. In order to break the spell and see their mother returned, they must remember something about her.

==Cast==
* Sissy Spacek as Mrs. Martin/Mama, the snappy French woman/Mom, the nature-hiker/Natasha, the circus performer
* Anna Chlumsky as Elizabeth Martin
* Aaron Michael Metchik as Jeremy Martin
* Asher Metchik (Aarons real-life brother) as Harry Martin
* Maureen Stapleton as Mrs. Cavour
* André the Giant as the Circus Strongman
* Merritt Yohnka as Mr. Leeby, the school principal
* Sean MacLaughlin as Edward, the Mommy Markets manager
* Schuyler Fisk (Sissys real-life daughter) as Suzy
* Anne Shannon Baxter as Lily
* Nancy Chlumsky (Annas real-life mom) as Dr. Richardson, the social worker

==Reception==
The film received negative reviews; as of July 2012, it holds a 29% rating on Rotten Tomatoes,  as well as being mentioned in Siskel and Eberts "Worst of 1994" episode.

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 


 
 