Anatomy of Hell
 
{{Infobox film
| name           = Anatomy of Hell
| image          = 
| alt            =  
| caption        = 
| director       = Catherine Breillat
| producer       = Jean-François Lepetit
| screenplay   = Catherine Breillat
| based on     =  
| starring       = {{Plainlist|
* Amira Casar
* Rocco Siffredi
}}
| narrator       = Catherine Breillat
| music          = 
| cinematography = {{Plainlist|
* Giorgos Arvanitis
* Guillaume Schiffman
}}
| editing        = Pascale Chavance
| studio         = {{Plainlist|
*CB Films
* Canal+
*Flach Film
}}
| distributor    = {{Plainlist|
*Rézo Films
*Sharada Distribuzione
}}
| released       =  
| runtime        = 77 minutes
| country        = France
| language       = French
| budget         = 
| gross          = 
}}

Anatomy of Hell ( ) is a film directed by Catherine Breillat in 2004. The sexually explicit film stars Amira Casar and Rocco Siffredi.

== Background ==
 porn star Rocco Siffredi as "the man". Leonard Maltin summarizes: "After attempting suicide in the bathroom of a gay disco, a woman hires the man who rescues her to spend four nights in her company, challenging him to watch me where Im unwatchable." 

== Reception ==

Movie critic   stated: "I remember when hard-core first became commonplace, and there were discussions about what it would be like if a serious director ever made a porn movie. The answer, judging by Anatomy of Hell, is that the audience would decide they did not require such a serious director after all."   November 11, 2004 

BBC film critic Jamie Russell gave the film four stars out of five:

: "The plot is hardcore thin: a woman (Amira Casar) cruises a gay club and pays broody stud (porn star Rocco Siffredi) to spend four nights with her. A challengingly explicit delve into the female body (often quite literally), its a unique cinematic example of feminist existential porn.... Yet perversely, its also one of the most groundbreaking films in recent memory in terms of both the explicitness of its sexuality and its commitment to such an austere intellectual discourse. No wonder Rocco looks so shell-shocked: this is sex not as comedy, but as the deepest, darkest male nightmare." Russell, Jamie.  , BBC, 22 November 2004 

The film went on to win "Best Feature Film" at the Philadelphia Film Festival.   

== References ==
 

== External links ==

*  

 

 
 
 
 
 
 
 
 
 
 
 