Doctor at Sea (film)
{{Infobox film
| name           = Doctor at Sea
| image          = Doctor at Sea orig UK poster.jpg
| border         = yes quad film poster
| director       = Ralph Thomas
| producer       = Betty E. Box Richard Gordon Jack Davies
| based on       =    
| starring       = Dirk Bogarde Brigitte Bardot James Robertson Justice Brenda De Banzie Joan Sims Bruce Montgomery
| cinematography = Ernest Steward
| editing        = Frederick Wilson
| studio         =  Group Film Productions  
| distributor    = Rank Film Distributors   Republic Pictures  
| released       =    
| runtime        = 93 minutes
| country        = United Kingdom
| language       = English
| budget         = gross = 1,111,404 admissions (France) 
}} Richard Gordons Jack Davies, and once again Dirk Bogarde played the lead character Dr Simon Sparrow. The cast also includes James Robertson Justice and Joan Sims from the first film, but this time playing different characters. This was Brigitte Bardots first English-speaking film.

==Plot==
With a view to escaping his employers daughter, who has amorous designs on him, Dr Simon Sparrow (Bogarde) signs on as medical officer on a cargo ship, "SS Lotus". The ship is commanded by hot-tempered and authoritarian Captain Wentworth Hogg.

Sparrow overcomes initial seasickness and settles into life on board. After arriving in a South American port (unspecified, but possibly in Ecuador),  Sparrow meets Hélène Colbert (Bardot), a sexy young French nightclub singer.

The misogynist Captain Hogg is forced to take on two female passengers, Muriel Mallet (De Banzie), the daughter of the chairman of the shipping company, and her friend, Hélène, for the return trip. Romance blossoms between Simon and Hélène, and when they reach home, Helene receives a telegram offering her a job in Rio de Janeiro. She and Sparrow return together.

Throughout the trip, Hogg has been romanced by Muriel and eventually becomes engaged to her - with almost certain promotion to Commodore (rank)|Commodore.

==Main cast==
 
* Dirk Bogarde as Dr. Simon Sparrow
* James Robertson Justice as Captain Hogg
* Brenda De Banzie as Muriel Mallet
* Brigitte Bardot as Hélène Colbert
* Maurice Denham as Steward Easter
* Michael Medwin as Third Officer Trail
* Hubert Gregg as Second Officer Archer
* James Kenney as Fellowes
* Raymond Huntley as Capt. Beamish
* Geoffrey Keen as Chief Officer Hornbeam 
* George Coulouris as Ships Carpenter  Noel Purcell as Corbie 
* Jill Adams as Jill 
* Joan Sims as Wendy
* Cyril Chamberlain as Whimble
* Toke Townley as Jenkins 
* Thomas Heathcote as Wilson
* Felix Felton as Dr George Thomas 
 

==Reception== The Dam White Christmas. 

==Awards==
*Nominated, 1956 BAFTA Film Award, Best British Screenplay, Nicholas Phipps and Jack Davies

==Sequels==
  Doctor series of films, with Bogarde featuring in the first three. Doctor at Sea was Brigitte Bardots first ever role in an English-speaking film.

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 