Walking on Sunshine (film)
 
 
{{Infobox film
| name           = Walking on Sunshine
| image          = 
| alt            = 
| caption        = 
| director       = {{Plainlist|
* Max Giwa
* Dania Pasquini}}
| producer       = James Richardson
| writer         = Joshua St Johnson
| starring       = {{Plainlist|
* Annabel Scholey
* Giulio Berruti
* Hannah Arterton
* Katy Brand
* Leona Lewis}}
| music          = Anne Dudley
| cinematography = Philipp Blaubach
| editing        = Robin Sales
| studio         = IM Global
| distributor    = Vertigo Films
| released       =  
| runtime        = 97 minutes  
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = $4,558,820 
}} romantic Musical musical comedy-drama film directed by Max Giwa and Dania Pasquini. The film features covers of hit songs from the 1980s and was released on 27 June 2014. It is also a debut role for singer-songwriter Leona Lewis.

==Plot==
The opening scene shows Taylor and her boyfriend Raf by the sea. Raf wants Taylor to stay with him in Italy, but Taylor wishes to go to university and start her "real life".
 How Will I Know?") before falling right in front of him. Raf recognizes her and is about to greet her when Maddie runs over and kisses Raf, revealing that Raf is Maddies fiancé. Raf, Elena, Mikey and Enrico are all very surprised that Maddie and Taylor are sisters as they dont think the pair have anything in common. Taylor is shocked that Maddie is marrying Raf, but pretends she doesnt know him, Elena, Enrico, or Mikey, despite Elena telling her she should tell Maddie the truth. Taylor declines, and tells her she would prefer to keep it a secret as not to worry Maddie.
 The Power of Love"). Taylor goes outside to the bar and Raf follows her, saying that the relationship between them is "weird", which Taylor denies at first but then agrees with. Raf wants to tell Maddie about their relationship three years ago but Taylor refuses, wanting to protect Maddie and to stop her from feeling like a fool.

The next day, Raf and Taylor are sent by Maddie to go and sort out their clothes and also the rings. Raf and Taylor argue about their past relationship and Taylor says she doesnt believe people should "sing from the rooftops", referring to peoples feelings. Raf argues that if they do, they really mean it. Elena, Mikey and Enrico overhear and leave. However, when Raf and Taylor step out of the changing cubicles in their wedding outfits it is hinted that they still have feelings for each other. 

Whilst this is happening, Doug turns up and surprises Maddie at the market. He tells her he made a mistake by breaking up with her, and wants her back. She declines, but Doug tries to persuade her ("Dont You Want Me"). He follows Maddie back to her house, where she agrees to have dinner with him before her hen party as a "final farewell". Elena and Enrico are discussing Taylors secret by the pool and Maddie walks in, finding out Elena has a secret. Elena panics and tells her she is pregnant.
 Walking on Sunshine"). Maddie is left behind to be pampered for her hen night.
 Eternal Flame"). Raf overhears Taylor and the two go to kiss, but Taylor runs away, still thinking about Maddie. Raf chases her as she runs back to the house, and he demands to know if she has any feelings for him and to discuss what happened between them on the beach. Whilst they are arguing, Maddie walks in and her hen party guests led by Elena and Lil surprise them. Taylor is forced to reveal that Raf was her old love from her past Puglia holiday but lies, saying that she has no feelings for him. Raf is hurt, but says the same, to keep his relationship with Maddie strong. Lil kicks Raf out of the hen night and Maddie tells Taylor to change for the party.
 The Wild Boys"). Taylor leaves the nightclub that the girls are visiting, and sings It Must Have Been Love, along with Raf who is in a different part of Puglia at his stag do. Taylor realizes she still loves Raf and decides she cant stay in case she destroys Maddie and Rafs relationship. She leaves for the airport in a taxi, throwing a picture of her and Raf out of the taxi window. 

Whilst Taylor is leaving, Maddie goes back to her bedroom where Doug is waiting. He says he does not want anything from her but he wants to give his blessing, which Maddie says is "sweet" before she realizes what Doug is doing. She ties Doug up, pretending she is going to have sex with him ("Faith (George Michael song)|Faith"); instead, she pushes him into the pool. Everything goes silent and Maddie goes to see if he is alright, and Doug is on one knee with an engagement ring, which confuses Maddie.
 White Wedding"). Maddie and Taylor reconcile before heading inside.

At the altar, the priest asks if anybody objects to Raf and Maddies marriage, to which Doug stands up, but Maddie tells him to sit down. Maddie then surprises everyone, telling Raf she is in love with love itself rather than him specifically and that he deserves better. Raf leaves the church but Taylor pursues him to apologize ("If I Could Turn Back Time"). When it doesnt work, Taylor runs to the roof and sings to Raf from there, as earlier on in the film Raf said if somebody sings from the rooftops you can tell they mean their feelings. Raf joins Taylor on the roof where Taylor tells Raf she loves him and the two kiss.

Later at the beach, Elena admits to Enrico that she actually is pregnant and Enrico is overjoyed. Lil hints at having feelings for Mikey by kissing him. Maddie and Taylor discuss how Maddie is now alone, but she says she will be fine. Doug interrupts the girls, and Lil and Taylor strap him into a hang glider whilst Maddie distracts him. Lil gives a signal to a boat offshore and Doug is flown up into the air, screaming. The entire cast then perform "Wake Me Up Before You Go-Go" and the film ends.

==Cast==
* Hannah Arterton as Taylor
* Annabel Scholey as Maddie
* Giulio Berruti as Raf
* Leona Lewis as Elena
* Katy Brand as Lil
* Greg Wise as Doug
* Danny Kirrane as Mikey
* Giulio Corso as Enrico

==Musical numbers==
# "Holiday (Madonna song)|Holiday" (Madonna (entertainer)|Madonna) - Taylor
# "Venus (Shocking Blue song)#Bananarama version|Venus" (Bananarama) - Lil, Maddie, and Taylor
# "How Will I Know" (Whitney Houston) - Taylor, Mikey, Enrico, and Elena The Power of Love" (Huey Lewis and the News) - Taylor, Maddie, Lil, Mikey, Enrico, Raf, and Elena
# "Dont You Want Me" (The Human League) - Doug and Maddie Walking on Sunshine" (Katrina and the Waves) - Elena, Taylor, Lil, Mikey, Enrico, and Raf Eternal Flame" (The Bangles) - Taylor The Wild Boys" (Cyndi Lauper/Duran Duran) - Taylor, Maddie, Elena, and Lil / Mikey, Enrico, and Raf
# "It Must Have Been Love" (Roxette) - Taylor and Raf
# "Faith (George Michael song)|Faith" (George Michael) - Maddie and Doug White Wedding" (Billy Idol) - Lil, Mikey, Maddie, Enrico, Doug, Taylor, and Raf
# "If I Could Turn Back Time" (Cher) - Taylor and Raf
# "Wake Me Up Before You Go-Go" (Wham!) - Elena, Mikey, Lil, Taylor, Maddie, Enrico, Doug, and Raf

==Release==
===Box office===
Walking on Sunshine opened in 360 theaters on 27 June 2014 and earned $687,345 in its opening weekend.  As of 23 July, it has grossed $3,248,671 worldwide.   

===Critical reception===
The film received negative reviews, currently holding a 30% rating on review aggregator website Rotten Tomatoes based on 20 reviews. 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 