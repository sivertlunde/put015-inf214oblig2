Trópico de Sangre
{{Infobox film
| name      = Trópico de Sangre
| image     = Tropico de Sangre.jpg
| director  = Juan Delancer
| starring  = Michelle Rodriguez Juan Fernandez César Évora Sergio Carlo Claudette Lali Celines Toribio
| music     = Manuel Tejada
| country   = Dominican Republic
| released  =  
| language  = Spanish
}}

Trópico de Sangre ( ) is a 2010 drama film based on the true story of the Dominican Republics historic Mirabal sisters.

==Plot== Minerva Mirabal and tells the true story of how she and her sisters came to represent the greatest threat to dictator Rafael Trujillo and his regime.  The Mirabal sisters were involved in an underground movement against the government.  They were assassinated in 1960 by men under the instruction of the Trujillo regime according to Pupo Roman, although their death was made to appear as an automobile accident.  Many citizens were outraged and a few months later Trujillo was assassinated by an ambush led by Antonio de La Maza, who was played by actor Cesar Evora.
Antonio de la Maza was killed in turn by a death squad led by Ramfis Trujillo.

==Cast==
*Michelle Rodriguez as Minerva Mirabal
*Juan Fernandez as Rafael Trujillo
*César Évora as Antonio de la Maza
*Claudette Lali as Emilia Bencosme
*Sergio Carlo as Manuel "Manolo" Aurelio Tavárez Justo
*Sharlene Taulé as Maria Teresa Mirabal
*Liche Ariza as Tomas Ovando
*Celines Toribio as Dedé Mirabal
*Luchi Estevez as Patria Mirabal
*Héctor Then as Priest Luis Peña González
*Johnnié Mercedes as Dr. Tejada Florentino

==Filming information==
The film debuted at the New York International Latino Film Festival on July 29, 2009. 

The surviving sister, Mirabal sisters|Dedé Mirabal, consulted on and participated in the production of the film; she was portrayed by actress Celinés Toribio.

==Controversy==
In July 2008, the president of the Minerva Mirabal Foundation, Carlos Leiter, publicly criticized the film, specifically the involvement of actress Michelle Rodriguez due to her past legal issues. Leiter threatened to sue Rodriguez and her co-producers citing illegal use of the Mirabal name, unless charitable organizations of his choice, including his own, were given all revenues, including Rodriguezs entire personal salary, from the film. 
 Battle in Seattle) and James Cameron (in Avatar (2009 film)|Avatar)" would even participate in such a small production, let alone show such "undeniable" dedication to it as both an actress and producer. 

==The Mirabal sisters in other media==
 film with Dominican dictator Rafael Leónidas Trujillo.

*Dedé Mirabal still lives in the same household where they all grew up.  She has been responsible for keeping their legend alive.  At 85 years of age, she has recently released her book about her sisters and the events called Vivas en su jardin (Alive in their garden), published by Vintage Español/Random House Publishing.

*Another film project by the Dominican director Etzel Baez, called Crimen, has been left unfinished since 2006.

==References==
 

==External links==
* 
* 
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 