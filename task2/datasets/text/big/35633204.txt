The Eve of St. Mark
{{Infobox play
|name=The Eve of St. Mark
|image=Teosmppos.jpg
|caption=Original Broadway cast poster
|director=Lem Ward
|playwright=Mazwell Anderson
|starring=Aline MacMahon William Eythe
|productions=1942 Broadway   1943 London 
|awards=
}}

The Eve of St Mark is a 1942 play by Maxwell Anderson set during World War II. It later became a 1944 film by 20th Century Fox that featured some of the same actors who repeated their roles in the film.  The title is derived from the legend of St Marks Eve and the title of an uncompleted 1819 poem by John Keats.

==Plot== conscripted into the United States Army in late 1940. Prior to being shipped out first to San Francisco, then the Philippines, Quizz and his hometown girlfriend Janet discuss their future plans.
 against overwhelming odds. Quizz communicates with his mother and Janet through dreams, where he asks them whether he and his friends should stay with their gun to sacrifice themselves by covering the withdrawing American troops or leave by boat for a chance of survival.

==Play==
Anderson dedicated the play to his nephew Sgt. Lee Chambers who was killed in an accident in the military.  When researching the contemporary US Army for the play at Fort Bragg, North Carolina, Anderson was assigned a soldier from the Army’s public relations named Marion Hargrove.  The two became friends with Anderson basing one of the characters in the play, Private (rank)|Pvt. Francis Marion on him and recommending Hargrove’s stories of army life to his publisher.  When published as See Here Private Hargrove, Anderson wrote a forward to the book. 

The play opened on October 7, 1942 and closed June 26, 1943 with 307 performances.  
 John Sweet Michael Powell chose for the lead in his A Canterbury Tale. 

==Film==
  Michael OShea, George Matthews, Joann Dolan, Joven E. Rola and Toni Favor repeating their original stage roles in the film. The film featured Anne Baxter, Harry Morgan and Vincent Price as Pvt. Marion.

The film version avoided censorship problems with some of the sexual references and language with a modified screenplay.  The film featured the original ending of the play that was overwhelmingly rejected by the war weary test screening audience.  A new ending giving hope to those who had loved ones in the Philippines was filmed.

The film cost $1.4 million. HOLLYWOOD JOTTINGS: No Strings on Paramounts Purse -- Metros Record Budget for Western -- Addenda
By FRED STANLEYHOLLYWOOD.. New York Times (1923-Current file)   05 Sep 1943: X3. 

Screenwriter George Seaton recalled that he was unable to meet with the director before or during the filming and was unsatisfied with the end result. Though Seaton’s screenplay remained intact, Stahl had emphasised different things then Seaton had intended with Stahl recommending that Seaton become a director himself. 

==Notes==
 

==External links==
*  
*  
*’’Life’’ magazine review http://books.google.com.au/books?id=UUEEAAAAMBAJ&pg=PA51&dq=%22the+eve+of+st+mark%22&hl=en&sa=X&ei=-C2aT-_iO83tmAXs47nQDg&ved=0CEIQ6AEwAg#v=onepage&q=%22the%20eve%20of%20st%20mark%22&f=false

 
 
 
 
  
 
 
 
 
 
 
 
 
 