The Siege of the Alcazar
{{Infobox film
| name           = The Siege of the Alcazar
| image          =
| image_size     =
| caption        =
| director       = Augusto Genina
| producer       =
| writer         = Augusto Genina Alessandro De Stefani Pietro Caporilli
| narrator       =
| starring       = Fosco Giachetti Mireille Balin Maria Denis
| music          = Antonio Veretti
| cinematography = Francesco Izzarelli Vincenzo Seratrice Jan Stallich
| editing        = Fernando Tropea
| distributor    =
| released       = Kingdom of Italy (1861-1946)|Italy: 20 August 1940|
| runtime        = 99 (cut) minutes 119 (restored) minutes Italy Spain
| language       = Italian
| budget         =
}}
 Italian war film directed by  Augusto Genina about the famous episode Siege of the Alcázar during the Spanish Civil War set in Toledo, Spain. The film won the Mussolini Cup in Venice Film Festival for being the Best Italian Film. The film runs more in the Spanish dubbed version, it was restored by Filmoteca Española and released in DVD in Spain by Divisa Home Video. The film was shot in Cinecittà with Italian, French and Spanish actors. In the Italian version all three non-Italian actors (Mireille Balin, Rafael Calvo and Carlos Muñoz) spoke their lines in Italian. They were dubbed by Italian actors afterwards.

==Plot==
The Alcázar of Toledo is a historical fortification taken by Franquist troops during the Spanish Civil War. The Republicans invest the Alcazar and besiege it for months against determined Nationalist resistance, before the siege was lifted by Franco with the Army of Africa.

==Cast==
*Fosco Giachetti : Captain Vela
*Mireille Balin : Carmen Herrera
*María Denis : Conchita Alvarez
*  Carlos Muñoz : Colonel Moscardòs son
* Aldo Fiorelli: Francisco 
* Andrea Checchi: Perro
* Carlo Tamberlani: Captain Vincenzo Alba
* Sivio Bagolini: Paolo Montez
* Checco Rissone: radiotelegrafista

==Awards==
The film won the Mussolini Cup in Venice Film Festival for being the Best Italian Film.

==Reception==
The Italian version was released in Italy in August 1940. The Spanish version of the film, known as "Sin novedad en El Alcazar!" was released in Spain in October 1940. After the war, the film was re-released in Italy under the new title Alcazar with significant cuts. All references to the involvement of Italy in the Spanish Civil War as well as the cruelty of the Republicans were excised to avoid any political debate.

The film was preceded by a prologue in the Italian, Spanish and German versions, all introducing the subject and setting the scenes. While the Italian and Spanish prologues are almost identical and only praised the courage of the besieged, the German prologue is decidedly more sinister in tone and, not surprisingly, openly condemns Bolshevism as the source of all evils

==External links==
*  
* http://worldcinemadirectory.co.uk/index.php/component/film/?id=751

 

 
 
 
 
 
 
 
 
 

 

 