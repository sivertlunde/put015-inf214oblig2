Through the Forests and Through the Trees
{{Infobox film
| name           = Through the Forests and Through the Trees
| image          = 
| caption        = 
| director       = Georg Wilhelm Pabst
| producer       = Herbert O. Horn Walter Forster
| starring       = Eva Bartok
| music          = 
| cinematography = Kurt Grigoleit
| editing        = Herbert Taschner
| distributor    = 
| released       =  
| runtime        = 98 minutes
| country        = Germany
| language       = German
| budget         = 
}}

Through the Forests and Through the Trees (  ) is a 1956 German comedy film directed by Georg Wilhelm Pabst. It was Pabsts final film.

== Cast ==
* Eva Bartok as Caroline Brand
* Peter Arens as Carl Maria von Weber
* Joe Stöckel as Kutscher Treml
* Rudolf Vogel as Valerian
* Karl Schönböck as Graf Enzio von Schwarzenbrunn Michael Cramer as Konrad
* Carolin Reiber as Marie (as Caroline Reiber)
* Maria Stadler as Anna
* Heinz Kargus as Züngenhorn
* Rolf Weih as Seifensieder
* Erik Frey as Graf Geza Esterhazy (as Eric Frey)
* Fritz Lafontaine as Mesner
* Johannes Buzalski as Schnapsnase

== References==
 

== External links ==
*  

 

 
 
 
 
 


 
 