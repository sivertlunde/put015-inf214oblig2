Zombie Girl: The Movie
{{Infobox film
| name           = Zombie Girl: The Movie
| image          = ZombieGirl2009documentaryposter.jpg
| alt            = 
| caption        = 
| film name      = 
| director       = Justin Johnson  Aaron Marshall  Erik Mauck
| producer       = 
| writer         = 
| starring       = Emily Hagins Meghan Hagins Harry Jay Knowles Jerry Hagins
| music          = Christopher Thomas
| cinematography = Justin Johnson  Erik Mauck
| editing        = Aaron Marshall
| studio         =  Bob B. Bob Productions, Part Olson Pictures, Vacdoomed Productions
| distributor    = 
| released       =  
| runtime        = 89 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} 2006 zombie film Pathogen (film)|Pathogen that was directed by Emily Hagins, who was twelve years-old at the time.  

== Plot ==
The documentary chronicles the making of Emily Hagins feature-length zombie film Pathogen (film)|Pathogen. The film, which took Hagin two years to complete and screen, was met with several setbacks during its filming due to elements such as theft of property. During the filming of Zombie Girl, the directors noted Hagins growth as a fledgling director, as she was twelve at the time Pathogen was made. 

==Cast==
*Emily Hagins	
*Megan Hagins
*Jerry Hagins
*C. Robert Cargill
*Tiger Darrow
*Kate Dawson
*Rebecca Elliott
*Alec Herskowitz
*Kirk Hunter
*Rose Kent-McGlew
*Harry Jay Knowles
*Tim League
*Phillip Thomas Martinez
*Jay Giovanni Ramirez
*Neil Reece

== Reception==
Critical reception for Zombie Girl: The Movie has been mostly positive and the film currently holds a rating of 100% "fresh" on Rotten Tomatoes based upon 8 reviews.  IndieWire gave a positive review, saying that while they had their doubts about the documentarys running time, "the directors manage to craft an intermittently entertaining chronicle of Haginss attempts to navigate the usual filmmaking hurdles — in addition to a few unique ones."  The documentary also received positive reviews from Fearnet and Film Threat.   

===Awards===
*Spirit of Slamdance Award from the Slamdance Film Festival (2009)

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 


 