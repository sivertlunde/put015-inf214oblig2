Kangalal Kaidhu Sei
 
{{Infobox film
| name           = Kangalal Kaidhu Sei
| image          = 
| caption        = 
| director       = Bharathiraja
| producer       = K. Muralitharan V. Swaminathan
| writer         = Sujatha Rangarajan (dialogues)
| story          = S. Premnath
| starring       = Vaseegaran Priyamani
| music          = A. R. Rahman
| cinematography = B. Kannan
| editing        = K. Pazhanivel
| studio         = Lakshmi Movie Makers
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Tamil
| budget         = 
| gross          = 
}}
Kangalal Kaidhu Sei (English: Arrest by eyes) is a 2004 Tamil language film, written and directed by Bharathiraja. It stars Vaseegaran in the protagonists role and marks actress Priyamani|Priyamanis debut in the Tamil film industry. Music was scored by A. R. Rahman, which was the major highlight of this film. The film received mixed reviews and became a box office failure.  

==Plot==
John Vaseegaran is a young man who ranks fifth richest millionaire in India and 30th on the international level. The plot opens in a graveyard where Vaseegrans parents funeral takes place. He is not disturbed by the demise of his parents as he had not been showered with any affection by them instead concentrated only on making money. A lady of his community expresses her condolence to him and leaves. While getting into her car she finds her diamond ring is missing and screams; the lost ring is in Vaseegarans pocket.

Vasee inherits the huge property of his parents and becomes the chairman of 32 companies in his twenties. Though rich he seeks true motherhood affection. He imagines his dream girl and names her Cinderella. He is a Kleptomaniac. He consults his family doctor for a solution. The doctor instructs him to get into love.

Vidhya, a young woman, sees Vasee stealing a crystal from a shopping mall. She tries to report it to mall authorities. A diamond exhibition is organised and one of the millionaires is exhibiting his special diamond which is worth 100&nbsp;million. It is different from other diamonds, with 16 reflecting faces. This tempts Vasee. He visits the diamond exhibition. The special diamond is under the responsibility of Vidhya. She sees Vaseegaran at the exhibition and becomes alerted. Despite special tight security Vaseegaran manages to steal the diamond and happily leave. Vidhya is about to be arrested. She pleas to cancel the arrest. She remembers Vasees presence and relates the old incident of crystal theft. She immediately understands Vasee is the thief and tells police. But cops dont believe her as he is a wealthy man. The owner of the diamond is ready to believe her words as he relates the diamond ring theft in the graveyard.

With no evidence, a drama is organised by Vidhya to get back the diamond and fetch evidence. She enters Vasees home as his personal secretary. Vasee wants her to be dressed with modern attire and she does so. He looks sees imaginary dream girl Cinderella in Vidhya and starts develops a love for her. She is not interested in him. Meanwhile Vidhya searches for diamond and spots it in a big fish tank. She attempts to get back while Vasee is away home and does so when he leaves for a meeting. Vasee rushes back to home when suddenly Vidhya disappears and when he was informed by his family doctor at airport that Vidhya and a police officer who is her fiancé met him for an inquiry about Vasee. At home he sees the broken fish tank and understands that Vidhya took the diamond. Vidhya was happy as she is relieved from the first charge, but she is charged again for the theft.

The diamond taken from Vasee by Vidhya is not the original one, and she is remanded. The owner of the diamond demands an illegal relation with her in return for discharge from the case. She rushes to Vasee and pleas to give back the diamond as it destroys her future and life. She expresses that she cannot marry him and mother his children. Her fiancé warns Vasee that soon he will be caught and jailed. Vasee bails her from police custody. He demands Vidhya to be his Cinderella for five days in Switzerland as a punishment for attacking psychologically with love as a weapon. He assures no physical contact but only love. Vidhya initially refuses but, due to the pressure by her fiancé to save his position, she goes with Vaseegaran.

At the Swiss airport she dashes an officer who introduces himself a person to help her. Vasee takes her to his home, introduces her to all of his friends, teachers etc. She constantly imagines Vasee misbehaving to her and is annoyed. Vasee one day sees Vidhya talking to a stranger whom she dashed in the airport, from a distance. She breaks a photograph jointly posed by her and Vasee. On seeing broken pieces Vasee screams at her that it is his family photo and she has no rights to break it. Meanwhile the officer dashed by Vidhya with some officers who follow Vasee and Vidhya find him alone after third day. They see him speaking to himself at the top of the hill, at boating, at church, etc. They immediately rush to Vasees home to rescue Vidhya. They could see only the dead bodies of Vasee and Vidhya dressed in groom and bride costumes.  Vasee actually killed Vidhya by slapping her on the day for she breaking the photograph. Unexpectedly, due to the attack, she died by dashing into a heavy stone. Not willing to be jailed, Vasee kills himself with his precious golden gun as soon as cops arrive. The cops see a letter which instructs to spend his property in the name of Cindrella trust to help poor and needy and a will of it.

==Cast==
* Vaseegaran as Vasi 
* Priyamani as Vidhya
* Sanath Gunatillake
* Aakash as Premkumar
* G.K.
* Ilavarasu
* Kaanchana
* Shanthi Kumar
* Chitra Lakshmanan
* Mayilsamy
* Prem Nath
* Panduga
* Laxmi Ratan
* Tanay Chheda ... Salam Rama

== Crew ==
* Screenplay, Direction: Bharathiraja
* Production: K. Muralitharan, V. Swaminathan
* Story: S. Premnath
* Cinematography: B. Kannan
* Music: A. R. Rahman
* Editing: K. Pazhanivel
* Art direction: G. K.
* Lyrics: Pa. Vijay, Kabilan, Thenmozhi
* Visual effects: Zameer Hussain

==Production==
Priyamani who worked as a model for various advertisements was selected as heroine and it marked her debut in Tamil cinema.  Fazal Ahmed from Palladam was rechristined as Vaseegaran made his debut in this film in lead role. 

The climax scene was shot at Switzerland and so were a couple of songs. One of the songs was picturised at the airport, where Vaseegara and Priyamani took part. Earlier, a 36 days shooting schedule was held at Sri Lanka. After this the unit shifted to locations in Chennai and Ooty. 

==Soundtrack==
{{Infobox album |  
 Name = Kangalal Kaithu Sei
| Type = Soundtrack
| Artist = A.R.Rahman
| Writer = Paa Vijay, Kabilan, Thenmozhi
| Cover = KHSAudio.jpg
| Released = 
| Recorded = Panchathan Record Inn Feature film soundtrack
| Length = 27:35
| Label = Hit Music
| Producer = A.R. Rahman
| Reviews = 
| Last album = Enakku 20 Unakku 18  (2003)
| This album = Kangalal Kaithu Sei (2003)
| Next album = Tehzeeb (2003 film)|Tehzeeb   (2003)| 
}}
The soundtrack has 5 tracks composed by A. R. Rahman and an introductory speech by the films director Bharathiraja. Lyrics were written by Paa Vijay, Thenmozhi and Kabilan. The soundtrack got high critical acclaim and immediate praise and was fairly noticed. In fact, this soundtrack is often termed as one of the most under-rated works of A. R. Rahman. All songs got great appreciation. One of the tracks features vocals by noted Malayalam film score composer Johnson (composer)|Johnson. It is perhaps the only song sung by Johnson.
 
{| class="wikitable"
|-  style="background:#ccc; text-align:center;"
! Song !! Singer(s)!! Duration !! Lyrics
|-
|"Azhagiya Cinderella"  Hariharan (singer)|Hariharan
|4:58
|Pa. Vijay
|-
|"Anarkali"  Karthik (singer)|Karthik, Chitra Sivaraman, Kadhir, Murtuza
|6:47
|Pa. Vijay
|-
|"Aaha Thamizamma"
|A. R. Reihana, Mathangi, Blaaze
|5:21 Kabilan Vairamuthu
|-
|"Ennuyir Thozhi"  Unni Menon, Chinmayee
|5:55
|Pa. Vijay
|-
|"Theekuruvi" Johnson (composer)|Johnson, Harini, Mukesh
|4:32 Thenmozhi
|}

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 