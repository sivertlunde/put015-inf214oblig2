Cleopatra Jones and the Casino of Gold
{{Infobox film
| name           = Cleopatra Jones and the Casino of Gold
| image          = Cleopatra Jones and the Casino of Gold.jpg
| caption        = Promotional film poster
| director       = Charles Bail
| producer       = 
| writer         = William Tennant "based on characters created by Max Julien"
| starring       = Tamara Dobson
| music          = 
| cinematography = 
| editing        = 
| distributor    = Warner Bros.
| released       = 1975
| runtime        = 
| country        = United States
| awards         = 
| language       =  English
| budget         = 
}}

Cleopatra Jones and the Casino of Gold is a 1975 Adventure film|action-adventure Blaxploitation movie starring Tamara Dobson as Cleopatra     It is the sequel to the 1973 film  Cleopatra Jones.

==Plot==

The story begins with two government agents — Matthew Johnson and Melvin Johnson — being captured by the Dragon Lady Stella Stevens. Cleopatra Jones then travels to Hong Kong to rescue the agents. Jones pairs up with Tanny (Ni Tien) and ends up in the Dragon Lady’s casino, which in actuality, is the headquarters for her underground drug empire. Jones and Tanny use their combat skills to battle the Dragon Lady’s henchmen and rescues the agents.   

==Cast==

*Tamara Dobson as Cleopatra Jones 
*Stella Stevens as Dragon Lady
*Norman Fell as Stanley Nagel

Max Julien, author of the source story for, and a co-producer of, the films predecessor, Cleopatra Jones, refused to participate in the production, and instead got token credit for the story and script having been "based on characters created by" him.

Stuntmen:

Eddy Donno,
Leonard William Laybourne,
Alan Gibbs,
Frank Orsatti

==Reception==

The film was not as well received as its predecessor, Cleopatra Jones, due mainly to the decline in the popularity of the blaxplotation genre. 

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 