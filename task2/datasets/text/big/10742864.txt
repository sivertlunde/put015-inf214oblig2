The Dark Redemption
 

{{Infobox film
| name        = The Dark Redemption
| image       = DarkRedemption-Poster.jpg
| director    = Peter Mether
| producer    = Warren Duxbury
| writer      = Peter Mether Dwight Bonecki Derek Curtis David Wheeler Martin Grelis Peter Sumner
| distributor = Atomfilms
| released    =  
| runtime     = 35 min.
| language    = English
| budget      =
}}

The Dark Redemption is a Star Wars fanfilm written by Dwight Steven-Boniecki, Derek Curtis, Warren Duxbury, and directed by Peter Mether, and originally released in 1999.

Set just before the events of  , the film tells of how Mara Jade (a character from the Star Wars Expanded Universe) worked with other Rebels to capture the plans for the first Death Star. While on the mission, though, Mara hears the Palpatine|Emperors call to turn to the Dark Side of the The Force (Star Wars)|Force.

Other crucial moments in Star Wars history are explained in this film, such as how Han Solo got in trouble with Jabba the Hutt.

The Dark Redemption is unique among Star Wars fanfilms in that it features an actual actor from the original film reprising his role - that is, Peter Sumner returns to the role of Imperial Officer Lt. Pol Treidum.

==Cast==
*Damian Rice ...  Zev Senesca
*Jason Chong ...  Klaus Vanderon
*Martin Grelis ...  Boba Fett
*Leah McLeod ...  Mara Jade
*Peter Sumner ...  Lieutenant Pol Treidum David Wheeler ...  Garrock
*Drew Sneddon ... Darth Sidious
*William Bowden... Voice of Darth Sidious
*Nathan Harvey ... Kyle Katarn
*Uncredited voice actor ... Han Solo
*Ben Craig ... Darth Vader
*Alan Cinis ... Voice of Darth Vader

==External links==
*  
*  
*  

 

 
 
 
 
 
 