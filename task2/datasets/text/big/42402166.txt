Vice (2015 film)
 
{{Infobox film
| name           = Vice
| image          = Vice poster.jpg
| alt            = Promo poster
| caption        = Promo poster
| director       = Brian A. Miller
| producer       = Steven Saxton   Randall Emmett   George Furla   Adam Goldworm
| writer         = Andre Fabrizio   Jeremy Passmore
| starring       =  Thomas Jane Bruce Willis Ambyr Childers Hybrid
| cinematography = Yaron Levy
| editing        = Rick Shaine
| studio         = Grindstone Entertainment Group   Emmett/Furla/Oasis Films   Aperture Entertainment   K5 International
| distributor    = Lionsgate
| released       =  
| runtime        = 96 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 science fiction film directed by Brian A. Miller and written by Andre Fabrizio and Jeremy Passmore. The film stars Thomas Jane, Bruce Willis and Ambyr Childers.

==Plot==
Businessman Julian Michaels ( ) becomes self-aware and escapes, she finds herself caught in the crossfire between Julians mercenaries and a cop (Thomas Jane) who is hell-bent on shutting down the corruption of Vice, and stopping the violence once and for all.

The plot bears great resemblance to Michael Crichtons 1973 film Westworld.

== Cast ==
* Thomas Jane  as Roy
* Bruce Willis  as Julian Michaels
* Ambyr Childers  as Kelly
* Johnathon Schaech as Chris
* Bryan Greenberg as Evan
* Charlotte Kirk as Melissa
* Ryan ONan as Det. Matthews
* Colin Egglesfield as Reiner Don Harvey as Kasansky
* Jesse Pruett as Officer Pullman
* Lydia Hull as Stacey
* Tyler J Olson as Steve

== Production ==
On January 21, 2014, Emmett/Furla/Oasis Films was set to produce and finance an action thriller film Vice, Bruce Willis will star in the $15 million budgeted film, which Grindstone/Lionsgate Films will distribute.    On February 5, 2014, it was announced that K5 International will be pre-selling the film at Berlin International Film Festival|Berlin’s European Film Market. The film also stars Thomas Jane and Ambyr Childers.    Later on February 13, 2014 it was revealed that the film had been sold in  37 different countries in just under a week. 

=== Filming ===
The shooting of the film had begun on April 3, 2014 in Mobile, Alabama|Mobile, Alabama. 

== Release ==
A promo poster was revealed on February 18, 2014. 

This film was released on DVD and Blu-ray on March 17, 2015.

== Critical reception ==
Vice was met with extremely negative reviews.  , the film has a 4% approval rating on Rotten Tomatoes, based on 26 reviews.  It also has a Metacritic score of 17 out of 100, based on 14 reviews. 

== References ==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 