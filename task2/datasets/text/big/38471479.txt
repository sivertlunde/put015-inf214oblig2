Gangs of Tooting Broadway
 
{{Infobox film
| name = Gangs of Tooting Broadway
| image = Tooting Broadway.jpg
| alt =
| caption = Theatrical release poster
| director = Devanand Shanmugam 
| producer =
| writer = Tikiri Hulugalle Terence Anderson
| music =
| cinematography =
| editing =
| distributor = Tooting Broadway Films
| released =  
| runtime = 86 minutes 
| country = United Kingdom
| language = English
| budget =
| gross =
}}
Gangs of Tooting Broadway is a 2013 British Tamil|British-Tamil crime drama film directed by Devanand Shanmugam. The film features Nav Sidhu and Kabelan Verlkumar in lead roles. It released on 4 February 2013. The story is based on gang wars between Tamil people and Black people living in Tooting Broadway.

==Plot==
The main protagonist Arun (Nav Sidhu) is a disillusioned former member of the Wolf Pack, now returned to Tooting four years after going to Cambridge and his father’s death. His mother tells him that his younger brother Ruthi has turned into a gangster and that he is planning a big war to take place in Tooting Broadway. Arun is sent to stop and protect his brother. The film then turns to a flashback four years ago. Arun was a popular gangster who was taken by a police officer (Oliver Cotton) to be an undercover informer. Now back in reality, Arun meets Karuna, his old boss. Karuna tells him that the Black people want war with the Tamils, and the only way Arun can protect his brother, is by finishing the war by killing all the Black people in Tooting. Persuaded, Arun & Karuna go to finish off the war, however a completely unexpected twist takes place leading to Karunas death. An unexpected villain turns up at the end of the film, leaving you very surprised.

==Production==
Film production began in 2011. A teaser of the film was uploaded to YouTube in February 2012, however the film was then delayed. It finally was released on 4 February 2013.

==References==
 

==External links==
*   at the Internet Movie Database