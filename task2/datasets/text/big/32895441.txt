The Silence of Dean Maitland (1914 film)
 
{{Infobox film
| name           = The Silence of Dean Maitland
| image          = 
| image_size     =
| caption        = 
| director       = Raymond Longford
| producer       = 
| writer         = Raymond Longford Lewis Scott the novel by Maxwell Gray
| narrator       =
| starring       = Harry Thomas Arthur Shirley
| music          =
| cinematography = Tasman Higgins
| editing        = Tasman Higgins
| studio      = 	Fraser Film Release and Photographic Company
| distributor    = 
| released       = 13 June 1914
| runtime        = 3,800 feet "Raymond Longford", Cinema Papers, January 1974 p51 
| country        = Australia
| language       = Silent film  English intertitles
| budget         = ₤400 
| gross  = £5,000 
| preceded_by    =
| followed_by    =
}}
 of the same name by Maxwell Gray which was later filmed by Ken G. Hall in The Silence of Dean Maitland|1934. It is considered a lost film.

==Plot==
The Reverend Dean Maitland (Harry Thomas) falls for Alma Lee (Nellie Brooks) and impregnates her, despite being engaged to another woman. Almas father attacks Maitland and Maitland accidentally kills him in the struggle. His best friend, Dr Henry Everard (Arthur Shirley) is convicted of the crime and sentenced to twenty years in gaol. When Everard is released he comes to Maitlands church. The Dean confesses his guilt publicly and collapses and dies.

==Cast==
*Harry Thomas as Dean Maitland
*Gwil Adams as Lilian, Cyrils sister
*Nellie Brooks as Alma Lee
*Ada Clyde as Mrs. Lee
*Jack Goodall
*Rebe Grey as Marion Maitland
*Charles Keegan as Cyrill
*Nellie Kemberman as Cyrils mother
*Lottie Lyell as Marion Everhard
*James Martin as Ben Lee
*Arthur Shirley	as Doctor Henry Everard
*Little Tuppeny	as Everard Maitland
* Charles Villiers as Judkins
* Ellen Blood as blind boy

==Production==
The movie was based on a play adaptation of the novel which had last appeared in Sydney in the 1890s.    Entirely set in England, it was shot on location in the grounds of Gladesville Mental Asylum in Sydney. 

Harry Thomas, who played the leading role, was a noted elocutionist.  Raymond Longford later alleged that The Combine (Australian film industry)|"the Combine" refused to let him film in their Rushcutters Bay Studio. Graham Shirley and Brian Adams, Australian Cinema: The First Eighty Years, Currency Press 1989 p 35 

The movie was reportedly one of the first to feature the close-up shot.  However this is not true. 

==Release== the Combine threatened to cancel its supply of further films to the theatre, and other theatres, if they showed the film. Despite this the film was a considerable success at the box office.  It also enjoyed a popular release in the UK. 

The critic from the Sydney Morning Herald said that "the picture is well taken and the various roles are cleverly portrayed."  The Daily News said "the photographic and histrionic qualities of the production are excellent, the producer having not only kept closely to the text of the novel, but carefully selected his artists with a view to preserving the facial characteristics of the dramatis personae."  Other reviews were positive. 

==Legal Dispute==
Longford signed a two-year contract with Fraser Film Company to write films from May 1914 at £1,000 a year. This adaptation of The Silence of Dean Maitland was the first script Longford submitted. Afterwards however Fraser elected to pull out of the contract at the suggestion of exhibitor Henry Gee of Australasian Films.  Longford tried to sue Gee for ₤1,000 for helping procure breach of contract but was not successful. He appealed the decision, but the court found against him again.  The battle hurt Longfords career for a time – he made two short films, then had to leave for New Zealand to get finance.

==References==
 

==External links==
*  at IMDB
*  at AustLit
 

 
 
 
 
 
 
 