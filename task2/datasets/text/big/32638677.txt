List of horror films of 1967
 
 
A list of horror films released in 1967 in film|1967.

{| class="wikitable" 1967
|-
! scope="col" | Title
! scope="col" | Director
! scope="col" | Cast
! scope="col" | Country
! scope="col" | Notes
|-
!   | Berserk!
| Jim OConnolly || Joan Crawford, Ty Hardin, Judy Geeson ||   ||  
|-
!   | Blood of the Virgins
| Emilio Vieyra || Ricardo Bauleo ||   ||  
|-
!   | Castle of the Walking Dead
| Harald Reinl || Lex Barker ||   ||  
|-
!   | El coleccionista de cadaveres
| Santos Alcocer || Boris Karloff, Viveca Windfors, Jean-Pierre Aumont ||    ||  
|-
!   | Corruption (1968 film)|Corruption
| Robert Hartford-Davis || Peter Cushing, Sue Lloyd, Noel Trevarthen ||   ||  
|-
!   | Creature of Destruction
| Larry Buchanan || Aron Kincaid, Roger Ready, Les Tremayne ||   ||  
|- Even the Wind is Scared
| Carlos Enrique Taboada || Marga López, Maricruz Olivier, Alicia Bonet ||   ||  
|-
!   | Eye of the Devil
| J. Lee Thompson || Deborah Kerr, David Niven, Donald Pleasence, Edward Mulhare ||   ||  
|-
!   | The Fearless Vampire Killers
| Roman Polanski || Roman Polanski, Jack MacGowran, Alfie Bass ||    ||  
|-
!   | Frankenstein Created Woman Robert Morris ||   ||  
|-
!   | The Frozen Dead
| Herbert J. Leder || ||   ||  
|- The Gruesome Twosome Elizabeth Davis, Michael Lewis ||   ||  
|-
!   | Hillbillys in a Haunted House
| Jean Yarbrough || Ferlin Husky, Joi Lansing, Lon Chaney, Jr. ||   ||  
|- Maneater of Hydra Cameron Mitchell, George Martin ||    ||  
|-
!   | The Mummys Shroud John Phillips, David Buck ||   ||  
|- Night of the Big Heat
| Terence Fisher || Peter Cushing, Christopher Lee ||   ||  
|- Quatermass and the Pit
| Roy Ward Baker || James Donald, Barbara Shelley, Andrew Keir ||   || Science fiction horror 
|-
!   | She Freak
| Byron Mabe || William Bagdad, Claire Brennan, Lynn Courtney ||   ||  
|-
!   | The Shuttered Room David Greene || Gig Young, Carol Lynley, Oliver Reed ||   || Science fiction horror 
|- Something Weird
| Herschell Gordon Lewis || Elizabeth Lee, Tony McCabe ||   ||  
|- The Sorcerers
| Michael Reeves || Boris Karloff, Ian Ogilvy, Elizabeth Ercy ||   ||  
|-
!   | The Spirit is Willing
| William Castle || Sid Caesar, Vera Miles, Barry J. Gordon ||   ||  
|-
!   | Succubus Jack Taylor, Howard Vernon ||    ||  
|-
!   | A Taste of Blood Bill Rogers ||   ||  
|-
!   | Theatre of Death
| Samuel Gallu || Christopher Lee, Lelia Goldoni, Jenny Till ||   ||  
|-
!   | This Night Ill Possess Your Corpse
| José Mojica Marins || José Mojica Marins ||   ||  
|- Torture Garden
| Freddie Francis || Jack Palance, Burgess Meredith, Beverly Adams ||   ||  
|-
!   | The Touch of Her Flesh
| Michael Findlay || Angelique, Michael Findlay ||   ||  
|-
!   | Viy (1967 film)|Viy
| Konstantin Yershov || Leonid Kuravlev, Natalya Varley, Alexei Glazyrin ||   ||  
|-
!   | Zinda Laash
| Khwaja Sarfraz || Deeba, Habib ||    ||  
|}

==References==
 

==Citations==
 
*  <!--
-->
*  <!--
-->
* {{cite book |editor-last=Krafsur |editor-first=Richard P. |year=1997 |title=The American Film Institute Catalog of Motion Pictures Produced in the United States: Feature Films, 1961–1970, Part 2 |url=http://books.google.ca/books?id=s1k1RsGvFwwC |publisher=University of California Press |isbn=0-520-20970-2 |ref=harv
}}
 

 
 
 

 
 
 
 