San Yuan Li
{{Infobox film
| name           = San Yuan Li
| image          =
| director       = Ou Ning, Cao Fei
| distributor    = dGenerate Films
| released       =  
| runtime        = 45 minutes
| country        = China
| language       = Mandarin
| film name = {{Film name| jianti = 三元里
| fanti = 三元里
| pinyin = sān yuán lǐ}}
}} independent Chinese documentary directed and produced by artists Ou Ning and Cao Fei.

==Plot synopsis==
Armed with video cameras, twelve artists present a highly stylized portrait of San Yuan Li, a traditional village besieged by Chinas urban sprawl. Chinas rapid modernization literally traps the village of San Yuan Li within the surrounding skyscrapers of Guangzhou, a city of 12 million people. The villagers move to a different rhythm, thriving on subsistence farming and traditional crafts. They resourcefully reinvent their traditional lifestyle by tending rice paddies on empty city lots and raising chickens on makeshift rooftop coops.

==External links==
* 
*  
*  

 
 
 
 
 
 
 


 
 