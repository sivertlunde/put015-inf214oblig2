The Flying Serpent
{{Infobox film
| name           = The Flying Serpent
| image          = Poster of the movie "The Flying Serpent".jpg
| image_size     = 150px
| alt            =
| caption        = Theatrical release poster
| director       = Sam Newfield
| producer       = Sigmund Neufeld
| screenplay     = John T. Neville
| story          = John T. Neville
| narrator       = Ralph Lewis Hope Kramer Eddie Acuff
| music          = Leo Erdody
| cinematography = Jack Greenhalgh
| editing        = Holbrook N. Todd
| distributor    = Producers Releasing Corporation
| released       =   
| runtime        = 59 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} Ralph Lewis, Hope Kramer and Eddie Acuff. 

The film is also known as Killer with Wings (American recut version).

==Plot==
Insane archaeologist Professor Andrew Forbes (George Zucco) uses a beast he unearthed to kill his enemies. The creature is the Aztec god Quetzalcoatl. Slowly those who know this try to stop the maniac and his monster.

==Cast==
* George Zucco as Prof. Andrew Forbes Ralph Lewis as Richard Thorpe
* Hope Kramer as Mary Forbes
* Eddie Acuff as Jerry Jonsey Jones
* Wheaton Chambers as Louis Havener
* James Metcalf as Dr. Lambert
* Henry Hall as Sheriff Bill Hayes
* Milton Kibbee as Hastings
* Budd Buster as Head of Inquest Terry Frost as Vance Bennett

==Remake==
The film was loosely remade in the 1980s as director Larry Cohens 1982 film Q (film)|Q, starring Michael Moriarty, Candy Clark, and Richard Rountree. 

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 