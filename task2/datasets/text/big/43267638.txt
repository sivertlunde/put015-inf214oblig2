Golmaal (1998 film)
{{Infobox film
| name           = Golmaal
| image          = 
| image_size     =
| caption        =  Selva
| producer       = K. S. Ravikumar C. Sudhakar
| writer         = Selva N. Sakthivel  (dialogues) 
| starring       =  
| music          = Bala Bharathi
| cinematography = M. R. Hareekanth
| editing        = B. Ramesh
| distributor    =
| studio         = North East Pictures
| released       =  
| runtime        = 145 minutes
| country        = India
| language       = Tamil
}}
 1998 Tamil Tamil comedy film directed by Selva (actor)|Selva, making his directorial debut. The film features Selva himself and newcomer Monica Nerukkar in lead roles, with Rajan P. Dev, K. S. Ravikumar, Dhamu, Sathya Prakash, Mahanadi Shankar, Bhanu Prakash playing supporting roles. The film, produced by director K. S. Ravikumar and C. Sudhakar, had musical score by Bala Bharathi and was released on 5 June 1998.  

==Plot==

Ganesh (Selva (actor)|Selva), a good-for-nothing youth, falls in love with Aishwarya (Monica Nerukkar). Her father Karnal Rajappa (Rajan P. Dev) is a very strict father who hates love. Ganesh and Aishwarya decide to elope but her father later has a heart attack and Aishwarya refuses to marry him. Karnal Rajappas state of health deteriorates, therefore he arranges his daughters wedding as soon as possible. Three terrorists (Sathya Prakash, Mahanadi Shankar and Bhanu Prakash) escape from jail. While preparing Aishwaryas wedding in Karnal Rajappas house, Ganesh enters in her room and tries to convince her. In the meantime, the three terrorists enter in their house and begin to sequester them. These terrorists are ultimately Ganeshs friends and it was Ganeshs plan to marry his lover Aishwarya. What transpires later forms the crux of the story.

==Cast==
 Selva as Ganesh
*Monica Nerukkar as Aishwarya
*Rajan P. Dev as Karnal Rajappa
*K. S. Ravikumar as ACP Bike Pandian (Periya Pandi) and Chinna Pandi
*Dhamu as Dhamu
*Sathya Prakash
*Mahanadi Shankar as Bangaru
*Bhanu Prakash as Gautham
*Venniradai Moorthy
*Delhi Ganesh as Muthukrishnan, Ganeshs father Thyagu as Viswanath
*Kumarimuthu as Dhamus father
*Typist Gopu Pallavi as Aishwaryas sister-in-law
*Abitha as Reshma, Aishwaryas sister Sangeeta as Aishwaryas mother Sheela as Aishwaryas niece
*Rajasekhar as Home Minister
*Jenisha
*Chitti
*Baboos as Tyson
*Kalidoss
*Raadhika Sarathkumar in a guest appearance

==Soundtrack==

{{Infobox Album |  
| Name        = Golmaal
| Type        = soundtrack
| Artist      = Bala Bharathi
| Cover       = 
| Released    = 1998
| Recorded    = 1998 Feature film soundtrack |
| Length      = 17:39
| Label       = 
| Producer    = Bala Bharathi
| Reviews     = 
}}

The film score and the soundtrack were composed by film composer Bala Bharathi. The soundtrack, released in 1998, features 4 tracks with lyrics written by Arivumathi, Vasan and Thirumaran.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Duration
|- 1 || Hey Paappa || Suresh Peters || 4:24
|- 2 || Nee Pesum || Hariharan (singer)|Hariharan, K. S. Chithra || 4:22
|- 3 || Twinkle Twinkle || Anuradha Sriram || 4:24
|- 4 || Chandrabose || 4:29
|}

==References==
 

 
 
 
 