Lost in the Dark (1947 film)
 
{{Infobox film
| name           = Lost in the Dark
| image          =
| caption        =
| director       = Camillo Mastrocinque
| producer       = Fortunato Misiano
| writer         = Giovanni Bracco Vittorio De Sica Camillo Mastrocinque Fulvio Palmieri Aldo Vergano Cesare Zavattini
| starring       = Vittorio De Sica
| music          =
| cinematography = Renato Del Frate
| editing        = Gino Talamo
| distributor    =
| released       =  
| runtime        = 105 minutes
| country        = Italy
| language       = Italian
| budget         =
}}
 play of a 1914 silent film. The films sets were designed by the futurist architect Virgilio Marchi.

==Cast==
* Vittorio De Sica - Nunzio
* Fiorella Betti - Paolina
* Jacqueline Plessis - Livia
* Enrico Glori - Paolo Nardone
* Olga Solbelli - Emilia
* Pietro Bigerna
* Anna Corinto - Lolotta
* Leo Dale
* Primo Di Gennaro
* Augusto Di Giovanni
* Claudio Ermelli
* Pupella Maggio - (as Giustina Maggio)
* Annielo Mele
* Luigi Pavese - Frantz Cardillo
* Tina Pica
* Giuseppe Porelli - Giovanni
* Maria Luisa Reda
* Alfredo Rizzo
* Sandro Ruffini
* Agostino Salvietti
* Domenico Serra
* Ettore Zambuto

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 

 