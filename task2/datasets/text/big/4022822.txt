Cane Toads: An Unnatural History
{{Infobox film| name     = Cane Toads: An Unnatural History image          = Cane Toads film.jpg director  Mark Lewis writer         = Mark Lewis music          = Martin Armiger cinematography = Jim Frazier Wayne Taylor editing        = Lindsay Frazer starring       = released       = 9 June 1988 (Australia), 11 September 1988 (Canada), 30 September 1988 (USA, limited) runtime        = 47&nbsp;minutes country = Australia language = English
}}  the introduction Cairns and Gordonvale in Queensland.
 BAFTA Film Award for Best Short Film.    It is distributed in the United States by Radio Pictures.

Unusual for a film considered a cult classic, Cane Toads performed very well during its theatrical release. For almost 20&nbsp;years Cane Toads: An Unnatural History held the title of top grossing non-IMAX documentary for the Australian box office. It remains easily in the top ten today even with the IMAX films included. Released in March 1988, it is recording as bringing in $613,910 Australian dollars (not adjusted for inflation). 

==Production elements==

One of the film’s aesthetic and storytelling “innovations” was to try and tell much of the story from the cane toads’ point of view. This was achieved by a number of extremely low angle shots. Lewis’ goal was to give a voice to the toads who were at the center of so much controversy, and “create some sympathy for this animal that was so widely reviled”.   

==Funding and distribution==

The film was funded by Film Australia. It was shown in theatres and currently still ranks high in Australian box office records for documentaries. The film was also released on DVD or VHS in Australia, the USA, and the UK (IMDb, 2011a). Excerpts of the film are also available online as teaching aids. Based on Internet comments, the film is still shown in middle school, high school, and college classrooms and is well received.
Production Company: Film Australia. Distributors: First Run Features (1999) (USA) (VHS); First Run Features (2001) (USA) (DVD); Umbrella Entertainment (2003) (Australia) (DVD); Unique Films (UK) (VHS) (IMDb, 2011a). 

==Sequel==
 Mark Lewis. Their cultural impact and moral complexity were explored in an essay by Elizabeth Farrelly.   

==References==
 

Australian Broadcasting Corporation (ABC). (n.d.). Cane Toads – An Unnatural History (1987). ABC.net.au. Retrieved from http://www.abc.net.au/aplacetothink/html/cane_toads.htm

IMDb. (2011a). Cane Toads: An Unnatural History (1988): Company credits. The Internet Movie Database (IMDb). Retrieved 30 November 2011, from http://www.imdb.com/title/tt0130529/companycredits

IMDb. (2011b). Cane Toads: An Unnatural History (1988): Filming locations. The Internet Movie Database (IMDb). Retrieved 30 November 2011, from http://www.imdb.com/title/tt0130529/locations

IMDb. (2011c). Mark Lewis. The Internet Movie Database (IMDb). Retrieved 30 November 2011, from http://www.imdb.com/name/nm0507509/

Lewis, M. (2010). The making—and the meaning—of Cane Toads: The Conquest. Cane toads and other rogue species (pp.&nbsp;19–32). New York: PublicAffairs.

Mitman, G. (1999). Epilogue. Reel nature: Americas romance with wildlife on films (pp.&nbsp;203–208). Cambridge, Mass.: Harvard University Press.

Morris, E. (n.d.). Eye Contact: Interrotron. Errol Morris. Retrieved 20 November 2011, from http://errolmorris.com/content/eyecontact/interrotron.html

Murray, R. L., & Heumann, J. K. (2009). Ecology and popular film: Cinema on the edge. Albany: State Univ. of New York Press.

Petterson, P. B. (2011). Cameras into the wild: a history of early wildlife and expedition filmmaking, 1895–1928. Jefferson, N.C.: McFarland.

Screen Australia. (n.d.). Australia: Top documentaries all time. Screen Australia. Retrieved 30 October 2011, from http://www.screenaustralia.gov.au/research/statistics/mrboxtopdoco.asp

SEWPaC. (n.d.). Invasive species in Australia – Fact sheet. Australian Government Department of Sustainability, Environment, Water, Population and Communities (SEWPaC). Retrieved 30 November 2011, from http://www.environment.gov.au/biodiversity/invasive/publications/species.html

Weber, K. (2010). Cane toads and other rogue species. New York: PublicAffairs.

West, Jackson. (2007). Top Ten Online Filmmaking Techniques. Gigaom. Retrieved from http://gigaom.com/video/top-ten-online-filmmaking-techniques/

==External links==
* 
* 

 
 
 
 
 
 
 
 