After the Rehearsal
 
{{Infobox film
| name           = After the Rehearsal
| image          = Efter_Repetitionen.jpg
| writer         = Ingmar Bergman
| starring       = Erland Josephson Ingrid Thulin Lena Olin Nadja Palmstjerna-Weiss Bertil Guve
| director       = Ingmar Bergman
| producer       = Jörn Donner
| cinematography = Sven Nykvist
| distributor    =
| released       = 9 April 1984
| runtime        = 70 min
| language       = Sweden
| language       = Swedish
| budget         =
}}

After the Rehearsal ( ) is a television film, written and directed by Ingmar Bergman in 1984. The script contains numerous quotes from August Strindberg|Strindbergs A Dream Play|Drömspel. The film was screened out of competition at the 1984 Cannes Film Festival.   

==Plot summary==
Rational, exacting, and self-controlled theater director, Henrik Vogler, often stays after rehearsal to think and plan. On this day, Anna comes back, ostensibly looking for a bracelet. She is the lead in his new production of Strindbergs A Dream Play. She talks of her hatred for her mother (now dead), an alcoholic actress who was Voglers star and lover. Vogler falls into a reverie, remembering a day Annas mother, Rakel, late in life, came after rehearsal to beg him to come to her apartment. He awakes and Anna reveals the reason she has returned: she jolts him into an emotional response, rare for him, and the feelings of a young woman and an older man play out.

==Cast==
*Erland Josephson – Henrik Vogler (older)
*Ingrid Thulin – Rakel Egerman
*Lena Olin – Anna Egerman (older)
*Nadja Palmstjerna-Weiss – Anna Egerman (younger)
*Bertil Guve – Henrik Vogler (younger)

(Tartan DVD release of this film erroneously lists "Liv Ullmann" as one of the three main stars.  Ullmann is NOT in this film)

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 

 