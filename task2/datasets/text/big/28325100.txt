Submarine I-57 Will Not Surrender
{{Infobox film
| name           = Submarine I-57 Will Not Surrender
| image          =
| image_size     = 
| caption        = 1959 Japanese movie poster
| director       = Shūe Matsubayashi
| producer       = Shiro Horie, Toho
| writer         = Takeshi Kimura Katsuya Susaki
| narrator       = 
| starring       = 
| music          = Ikuma Dan
| cinematography = Taiichi Kankura
| editing        = Yoshitami Kuroiwa
| distributor    = Toho
| released       = 1959 (Japan)
| runtime        = 104 minutes
| country        = Japan
| language       = Japanese
| budget         = 
}}
 1959 cinema Japanese war film directed by Shūe Matsubayashi.

Special effects were made by Eiji Tsuburaya.

==Plot==

In World War II, the commanding officer of a sub, against his will takes on board two Western diplomats, to take them to the Canaries and arrange an armistice. When they get there, peace has been declared, but the subs crew dont know as their radio has failed. They send their passengers ashore and go out to face a final battle.

==Cast==
* Ryō Ikebe
* Tatsuya Mihashi
* Akihiko Hirata
* Akira Kubo
* Susumu Fujita
* Minoru Takada
* Maria Laurenti Andrew Hughes
* Hisaya Ito

== See also ==
* Imperial Japanese Navy submarines B3 type submarine

==References==
 

==External links==
* 

 
 
 
 
 
 


 
 