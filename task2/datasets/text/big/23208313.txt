Lightning (film)
 
{{Infobox film
| name           = Lightning 稲妻 Inazuma
| image          = Inazuma poster.jpg
| caption        = Japanese movie poster featuring Hideko Takamine
| director       = Mikio Naruse
| producer       =  Fumiko Hayashi (novel) Sumie Tanaka
| starring       =
| music          = Ichirō Saitō
| cinematography =
| editing        =
| studio         = Daiei Film
| distributor    =
| released       =  
| runtime        = 87 min.
| country        = Japan
| awards         =
| language       = Japanese
| budget         =
}} Japanese film Fumiko Hayashi.

The film won 1953 Blue Ribbon Awards for best director (Mikio Naruse), best film (Mikio Naruse) and for best supporting actress Chieko Nakakita. It also won Mainichi Film Concours for best film score by Ichirō Saitō and again for best supporting actress Chieko Nakakita. 

== Cast ==
* Hideko Takamine as Kiyoko
* Mitsuko Miura as Mitsuko
* Chieko Nakakita as Ritsu
* Kenzaburo Uemura as Ryuzo
* Kyōko Kagawa as Tsubomi
* Jun Negami as Shuzo
* Sakae Ozawa as Tsunakichi

== References ==
 

== External links ==
 
*  

 
 

 
 
 
 
 
 
 


 