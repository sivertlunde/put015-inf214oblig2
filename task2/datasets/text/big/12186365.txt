God's Gun
{{Infobox film
| name           = Gods Gun
| image          = GodsGun.jpg
| caption        = DVD cover for Gods Gun
| director       = Gianfranco Parolini
| producer       = {{plain list|
*Yoram Globus (associate producer)
*Menahem Golan (producer)
}}
| writer         = {{plain list|
*John Fonseca (screenplay)
*Gianfranco Parolini (screenplay)
}}
| starring       = {{plain list|
*Lee Van Cleef
*Jack Palance
*Richard Boone
*Sybil Danning
*Leif Garrett
}}
| music          = Sante Maria Romitelli
| cinematography = Sandro Mancori
| editing        = Manlio Camastro
| distributor    = Troma Entertainment
| released       =  
| runtime        = 94 minutes
| country        = {{plain list|
*Italy
*Israel
}}
| language       = {{plain list|
*Italian
*English
}}
}}

Gods Gun (also known as Diamante Lobo) is a 1975 Italian–Israeli Spaghetti Western filmed in Israel directed by Gianfranco Parolini (credited as Frank Kramer) and starring Lee Van Cleef. Jack Palance plays the head of a malicious group of bandits and Van Cleef plays a double-role of brothers: a priest and a reformed gunfighter determined to stop them.

Leif Garrett also plays a vital part in the film, as a fatherless kid who brings the reformed gunfighter to town.

==Plot==
One day Sam Clayton (Jack Palance) and his gang arrive in the small town of Juno City where Father John (Lee van Cleef) is the priest of the town church. After having a little bit of fun that involves raping a woman and knifing a man in the back, the gang leaves town, only to be caught by the fearless but unarmed Father John. That night the murderer is broken out of gaol.  Vowing revenge, the gang guns him down on the steps of his church and then set about taking control of the town while waiting for the stagecoach. However, little Johnny OHara (Leif Garrett) manages to escape with a couple of their horses and rides off to Mexico in the hope of finding the priests gunfighter brother (also played by Lee van Cleef). They soon meet and set off back across the border to clean up the town. Meanwhile Clayton discovers that he is Johnnys father. Also, some fifteen years prior during the Civil War, Jenny OHara (Sybil Danning) had been one of Claytons victims, adding to the mystique of the situation — as well as to the question of little Johnnys paternity — and now, enhancing the plot, Clayton takes to the idea of being a father.

==Cast==
*Lee Van Cleef as Father John / Lewis
*Jack Palance as Sam Clayton
*Richard Boone as The Sheriff
*Sybil Danning as Jenny
*Leif Garrett as Johnny
*Robert Lipton as Jess Clayton
*Cody Palance as Zeke Clayton
*Ian Sander as Red Clayton
*Pnina Rosenblum as Chesty
*Zila Carni as Juanita Lewis
*Heinz Bernard as Judge Barrett
*Didi Lukov as Rip
*Ricardo David as Angel George
*Chin Chin as Willy
*Rafi Ben Ami as Mortimer

==Production==
Richard Boone walked off the film before it was completed leaving his role to be dubbed by another actor.  In an interview with Cleveland Amory in Israel in May 1976 Boone told Amory, "Im starring in the worst picture ever made. The producer is an Israeli and the director is Italian, and they dont speak. Fortunately it doesnt matter, because the director is deaf in both ears." 

==Notes==
 

==External links==
* 
*  (English version)
*  – at the Troma Entertainment movie database

 
 
 
 
 
 
 
 
 
 
 