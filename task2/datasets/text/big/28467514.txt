Accidental Life
{{Infobox film
| name           = Accidental life
| image          =
| alt            =
| caption        =
| film name      = Slučajni život
| director       = Ante Peterlić 
| producer       =
| writer         =
| screenplay     = Petar Krelja Zoran Tadić Ante Peterlić 
| story          =
| based on       = 
| starring       = Dragutin Klobučar Ivo Serdar Ana Karić Zvonimir Rogoz Helena Buljan Stjepan Bahert Fabijan Šovagović Dragan Milivojević Martin Sagner Branko Špoljar 
| narrator       =
| music          = Boško Petrović 
| cinematography = Ivica Rajković 
| editing        = Katja Majer 
| studio         = Film Authors Studio 
| distributor    =
| released       =  
| runtime        = 66 minutes  (81 minutes, according to some sources )
| country        = Yugoslavia
| language       =
| budget         =
| gross          =
}} Croatian drama film directed by Ante Peterlić, starring Dragutin Klobučar, Ivo Serdar, Ana Karić and Zvonimir Rogoz.

Accidental Life, an existential study of ordinary lives led by two alienated urban white collar workers, was the only feature film of Ante Peterlić, Croatian film theorist and film critic. The film received mediocre reviews and went largely unnoticed after its release, but has been reevaluated decades later as one of the best Croatian films ever made.

== Plot ==
Filip (Dragutin Klobučar) and Stanko (Ivo Serdar) are two young clerks who share an office in a nondescript company. The two are also amateur rowers who train together. Their personalities are quite different: while Filip is fastidious, serious in relationships with women, and somewhat introverted and sensitive, Stanko is a womanizer and prone to shirking his duties at work. Still, they spend most of their time together, rowing on the Sava river in the morning, collaborating in the office during the day, and going out in the evening looking for female company - all trying desperately to escape from the tedium of everyday life. They see their senior colleague Jurak (Zvonimir Rogoz) as a dinosaur, dreading the possibility of becoming like him as they grow older.

Filip falls in love with an attractive female coworker and, as the events gradually unfold, differences in character between the two men create a simmering conflict...   

== Background and production == Faculty of Humanities and Social Sciences in Zagreb. In the 1960s he directed his first short TV drama, and was active as an assistant director in several feature films and documentaries, working also as a script doctor. 

The role of Stanko was initially intended for Zvonimir Črnko, but he was busy shooting a TV series, so Peterlić opted for Dragutin Klobučar. However, he felt that Klobučar was better suited for the character of Filip, the more introverted protagonist, so Stankos role ultimately went to Ivo Serdar. 

Due to budget constraints, the film was shot quickly, with a small crew. Peterlić likened the near-amateur filming conditions to those prevalent in the French New Wave, his main influence at the time. Still, he saw Accidental Life as a work that, rather than being a New Wave film per se, projected a New-Wave-like atmosphere by "striving to tell the truth about its times and about my generation by using a more modern way of storytelling". 

== Themes and style ==
Croatian film critic Jurica Pavičić describes Accidental Life as typical in many respects for the Croatian cinema of the late 1960s, which was increasingly moving away from historical, rural settings and adopting urban themes and sensibilities. The two protagonists are members of the "lost generation" who are urban loners with no ambition or direction in life. They are also - as noted by film critic Slaven Zečević - uprooted people with no past, which makes them an oddity in a still traditional society which highly values family background and kinship. 

The films portrayal of a faceless, dehumanized workplace is highly reminiscent of Il Posto, a 1961 film by Ermanno Olmi. Accidental Life thus largely reflected the modernist sensibilities of its era, but opted for a classicist visual approach. As a fan of Howard Hawks and John Ford, Peterlić preferred the subtle, "invisible" style of directing - using simple, functional shots - over the more extravagant modernist film techniques. 

== Reception ==
Accidental Life was shown at the Pula Film Festival, receiving mediocre reviews from the critics. During its five-week theatrical run in Grič Cinema in Zagreb, the films earnings recovered its budget.  It went largely unnoticed in Yugoslavia, despite receiving multiple recommendations in Cahiers du cinéma, the renowned French film magazine. 

A possible reason for the films tepid reception is because it did not really belong to any of the established movements in cinema of the era:  it was far from populist storytelling, yet - with its absence of pronounced symbolism, original motifs, or more extravagant camera and editing work - seemed too bland for the proponents of modernist cinema.  Peterlićs student and fellow film theorist Hrvoje Turković found some reactions to the film to be malicious, arguing that Peterlićs earlier work as a film critic motivated many to use the opportunity to "get even". 

Peterlić did not return to directing, and Accidental Life remained his only feature film. As his academic career was taking off, he felt that it made no sense to pursue film directing, a field in which his contributions were not appreciated.  Ultimately, Peterlić came to be best known for his scholarly work, which earned him the title of the "father of Croatian film studies". 

Largely forgotten for decades, Accidental Life has been reevaluated in a much more favorable light in the 1990s, when it began to appear in several critics all-time top lists of Croatian films.  In a 1999 poll among 44 Croatian film critics and film historians, Accidental Life placed 17th in the list of all-time best Croatian films.  In 2008 the film was released on DVD. 

==References==
{{Reflist|refs=

 {{cite web
| url=http://www.dnevnikulturni.info/vijesti/film/221/umro_ante_peterlic/
| title=Umro Ante Peterlić
| date=2007-07-14
| work=Dnevni Kulturni Info Radio 101
| language=Croatian
| accessdate=2014-08-02
}} 

 {{cite web
| url=http://www.filmski-programi.hr/baza_film.php?id=12
| title=Slučajni život
| work=filmski-programi.hr
| publisher=Croatian Film Association
| language=Croatian
| accessdate=2014-07-28
}} 

 {{cite web
| url=http://www.filmski.net/vijesti/dugometrazni-film/4835/umro_ante_peterlic
| title=Umro Ante Peterlić
| work=filmski-programi.hr
| date=2007-07-14
| publisher=Croatian Film Association
| language=Croatian
| accessdate=2014-08-03
}} 

 {{cite journal
| url=http://www.hfs.hr/nakladnistvo_zapis_detail.aspx?sif_clanci=32490#.U9ZdDrGX-M8
| title=Novo viđenje nevidljivog filma
| journal=Zapis
| publisher=Croatian Film Association
| issue=63
| year=2008
| last=Pavičić
| first=Jurica
| authorlink=Jurica Pavičić
| language=Croatian
| accessdate=2014-07-28
}} 

 {{cite web
| url=http://www.hfs.hr/hfs/dvd_detail_e.asp?sif=24&counter2=0
| title=Accidental life
| work=hfs.hr
| publisher=Croatian Film Association
| accessdate=2014-07-28
}} 

 {{cite web
| url=http://www.glasistre.hr/vijesti/kultura/-slucajni-zivot-a-peterlica-nakon-44-godine-u-puli-426186
| title=Slučajni život A. Peterlića nakon 44 godine u Puli
| work=Glas Istre
| date=2013-10-12
| language=Croatian
| accessdate=2014-08-04
}} 

 {{cite journal
| url=http://hrcak.srce.hr/index.php?show=clanak&id_clanak_jezik=108735&lang=en
| title=Izazovi »Amerike« u hrvatskom filmu i pisanju o filmu – esej o postupnoj realizaciji naslovne metafore
| journal=Hvar City Theatre Days
| volume=33
| issue=1
| date=May 2007
| last=Kragić
| first=Bruno
| language=Croatian
| accessdate=2014-08-03
}} 

 {{cite news
| url = http://arhiv.slobodnadalmacija.hr/19991128/kultura.htm
| language = Croatian
| newspaper = Slobodna Dalmacija
| title = "Tko pjeva, zlo ne misli" najbolji hrvatski film svih vremena!
| date = 1999-11-28
| accessdate = 2013-02-08
}} 

 {{cite journal
| url=http://www.matica.hr/vijenac/345/Bitno%20je%20zasukati%20rukave/
| title=Bitno je zasukati rukave
| work=Vijenac
| date=2007-05-24
| issue=345
| publisher=Matica hrvatska
| language=Croatian
| accessdate=2014-07-29
}} 

 {{cite web
| url=http://www.zagrebfilmfestival.com/arhiva/2007/index_en.php?page=d0&iz=fpf&fid=3071
| title=Accidental Life / Slučajni život 
| work=zagrebfilmfestival.com
| publisher=Zagreb Film Festival
| accessdate=2014-07-28
}} 

}}

==External links==
*  

 
 
 
 
 
 
 
 
 