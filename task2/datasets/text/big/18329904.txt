Jarum Halus
{{Infobox Film name           = Jarum Halus image          = image_size     = caption        = Theatrical release poster director       = Mark Tan producer       = Eleanor Low Mark Tan starring       = Christien New Juliana Ibrahim Razif Hashim Dato Rahim Razali Farah Putri  Justin Chan Megat Sharizal music          = San Weng Onn
                  Rabbit distributor    = Sparky Pictures released       = January 10, 2008 runtime        = 133 min country        = Malaysia awards         =  language       = Malay English budget         = USD 40000
}}
Jarum Halus is a 2008 Malaysian drama film. It is a modern-day adaptation of William Shakespeares Othello. The film’s title is derived from a Malay idiom meaning web of deceit or conspiracy (political)|conspiracy, which is a major theme in the plot of the film.

 The plot remains faithful to the source material, with the cast all taking their Shakespearian counterparts either in name, character or both (see Cast section)

It is the directorial debut of Mark Tan, who originally envisioned the project as a short film for his final project when studying film at Warwick University in the United Kingdom. When returning to his native Malaysia, the short film was used as a means to attract funding for a feature length project. When a deal was agreed and a script finalised, the quality attracted Malaysian veteran actor Dato Rahim Razali, who agreed to a role on a significantly reduced salary.

==Reception==

The film received a limited release in January 2008 was released to favourable reviews, with several references to its visual boldness and dynamic mixture of eastern values with western storytelling. It combines both the English and Malay languages in a style typical of urban Malaysian speech.

Jarum Halus won the "Best Digital Film" in FFM 2008.

==Cast== Othello
*Juliana Desdemona
*Razif Hashim - Iskandar - Iago
*Dato Rahim Razali - Datuk Kalel - Lodovico Cassio
*Farah Emilia
*Megat Sharizal - Rahim - Roderigo

The film features cameo appearances by Douglas Lim, Colin Kirton and Yasmin Yusuff.

==References==
 
==External links==
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 

 

 
 
 
 
 