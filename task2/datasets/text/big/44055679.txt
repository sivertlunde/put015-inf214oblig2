Do Diwane
{{Infobox film
| name           = Do Diwane
| image          = 
| image_size     = 
| caption        = 
| director       = Chimanlal Luhar 
| producer       = Sagar Movietone
| writer         = K. M. Munshi 
| narrator       =  Motilal Shobhana Yakub Aruna Devi
| music          = Pransukh Nayak 
| cinematography = Keki Mistry
| editing        = 
| distributor    =
| studio         = Sagar Movietone
| released       = 1936
| runtime        = 171 min
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}} 1936 Hindi comedy film directed by Chimanlal Luhar based on K. M. Munshis famous play Be Kharab Jaan.    The film was produced by Sagar Movietone and had music composed by Pransukh Nayak with cinematography by Keki Mistry.  The cast included Shobhana Samarth, Motilal (actor)|Motilal, Yakub (actor)|Yakub, Aruna Devi, Rama Devi, Kamalabai, Kayam Ali, Pande and Pesi Patel.   

Based on the famous Gujarati language writer K.M. Munshi’s "acclaimed comedy", the film dealt with the traditional values of the older generation in conflict with the western values adopted by the youngsters.      

==Plot==
Motilal in the role of a doctor, wants to join the revolutionaries along with his girlfriend played by Shobhana Samarth. Both are opposed by their parents. The parents find it difficult to come to terms with what they consider as western influences on their children in clothes and thinking. Several humorous situations arise in the process with everything finally ending to the satisfaction of both generations.

==Cast==
* Shobhana Samarth Motilal
* Yakub
* Rama Devi
* Aruna Devi
* Kamala Devi
* Kayam Ali
* Pande
* Temuras
* Pesi Patel
* Mehndi Raza
* Kantilal Nayak

==Shobhana Samarth and Motilal==
Shobhana Samarth started her career after marriage with Nigah-e-Nafrat (1935), but Do Diwane was the first film to have the popular pair of Shobhana Samarth and Motilal starring together and it was cited as one of her best films.        She came into prominence with her roles in two of Sagar Movietone films, Do Diwane and Kokila (1937 film)|Kokila (1937) both with Motilal.     

==Music==
The music was composed by Pransukh M. Nayak with lyrics written by Raghunath Brahmbhatt.  The singers were Motilal, Shobhana Samarth and Kamala Devi. 
===Songlist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer 
|-  
| 1 
| Dhoondat Hoon Jeevan Dor Ghor Ghata Chal
| Motilal
|-
| 2
| Gore Gore Paan Ka Beeda Banaun
|
|-
| 3
| Kha Ke Zehar Mar Jaana Sanam
|
|-
| 4
| Kya Baat Thi Maze Ki Dil Ko Lubha Ke Mere
| Motilal
|-
| 5
| Pa Laagoon Kar Jori Kanhaiya
| Kamala Devi
|-
| 6
| Sarowar Se Har Hans Ko Milne Ki Hai Aas
| Motilal
|-
| 7
| Paan Ka Dhandha Hi Achha Tha
| Motilal
|-
| 8
| Muhabbat Lene Aaye Thay Muhabbat Leke Jaate Hain
|
|-
| 9
| Khilne Bhi Nahin Pai Murjha Gai Phulwari
| Shobhana Samarth
|-
| 10
| Hum Ban Gaye Sab Bade Doctor
|
|-
| 11
| Dil Jaani Main Diwani
|
|}


==References==
 

==External links==
* 



 
 
 
 
 