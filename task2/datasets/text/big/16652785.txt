Flawless (2007 film)
 
 
{{Infobox Film 
| name = Flawless
| image = Flawless ver2.jpg
| caption = Theatrical release poster
| director =  Michael Radford
| producer = Carola Ash Jimmy de Brabant Stephen Margolis Albert Martinez Martin Michael A. Pierce Richard Pierce Charles Salmon  Mark Williams
| writer = Edward Anderson
| starring = Demi Moore Michael Caine Joss Ackland Jonathan Aris Simon Day Josef dBache-Kane  Lambert Wilson 
| music = Stephen Warbeck  
| cinematography = Richard Greatrex Peter Boyle
| distributor = Magnolia Pictures
| released = 11 February 2007
| runtime = 108 min English
| budget = $20,000,000
| gross = $6,819,587	
}}
 British fictional  crime film directed by Michael Radford, written by Edward Anderson, and starring Michael Caine and Demi Moore. It premiered 11 February 2007 in Germany. The film had a limited release in the United States on 28 March 2008. 

==Plot==
The film is set in London and in the early 1960s. Mr. Hobbs, a janitor played by Michael Caine, is about to retire but does not want to leave empty-handed. He asks Laura Quinn (Demi Moore), a disgruntled executive victimized by the glass ceiling, to help him steal from the company for which they both work: the London Diamond Corporation.

The film opens with the camera focusing on women in various positions of power, from different backgrounds, most of them handling some kind of business on PDAs or cellular phone.  The camera follows one woman, a writer for a newspaper who is doing a piece on "Women Who Led."  As she enters a restaurant to interview a woman to be included in this piece, she is talking on her cell phone to an associate who has guaranteed her a front page spot for a story, of which the writer seems to be very proud.  The woman she is meeting with is Laura Quinn, the only woman to ever have been a manager at the London Diamond Corporation during the late 50s into 1960.  Quinn in a very nonchalant manner places a box on the table, and out of this box pulls a 168 carat, 58 facet diamond. (33.6g) The writer is astounded and eyes the piece as Quinn says, "I stole it."

The movie flashes back to 1960, when Quinn was still employed as a manager at London Diamond Corporation.  She is the first person to arrive at work, and is always the last to leave.  Despite her having proved herself intellectually superior to her male co-workers, she is passed over for a promotion for the sixth time.  She pretends to brush it off, but it is plain to see that it severely bothers her. The janitor, Mr. Hobbs, says small supportive things to her when he cleans her office at night and then proceeds on his way, obviously an admirer of her strength and resolve to accomplish more than the men at London Diamond.  When the company is in danger of losing a crucial contract with the Russians, Quinn saves the company with a simple solution, but the Russians will not proceed unless the arrangements are kept secret from everyone except senior management, which excludes Quinn. Quinn is to be terminated, but Mr. Hobbs finds out and warns her.  Additionally, he offers her a place in a plot against the company that will guarantee him a hearty pension and will make her departure from the company a little more satisfying: stealing enough diamonds to make them rich, but not enough so that anyone will notice they are missing.

The plan is almost ruined when the company installs cameras to monitor the hallways, but Quinn discovers that the cameras change screens every 60 seconds, just enough time for Mr. Hobbs to steal the diamonds, and the plan proceeds.  However, instead of Mr. Hobbs only filling his coffee thermos with diamonds, he steals every single diamond, almost two tons worth, and holds them for ransom.  For 100 million pounds, the London Diamond Corporation will get all of its diamonds back.  Quinn never agreed to this and now finds herself trapped.  The company hires a private investigator to keep the matter from going public.  If the world were to know that the single supplier of diamonds to six continents had lost its supply, the diamond market would crash.  Mr. Finch keeps a close eye on Mr. Hobbs and Miss Quinn, but still does not have sufficient evidence against either of them. Quinn says they have to turn themselves in and perhaps avoid jail time if they give the diamonds back.  Mr. Hobbs confesses that his cause is worth spending one hundred lifetimes in jail, and refuses to disclose the location of the diamonds.  She begins to realize this was never about his pension.

Mr. Finch tries to press Miss Quinn for more information, but she does not crack.  He threatens that he has evidence against her, but she keeps her cool blood in grip and still does not reveal any information to him or admits any involvement. When the president of the company has a heart attack over the stress of the situation once leaked to Press, Mr. Finch is called away.  Miss Quinn runs to the bathroom and cries uncontrollably, almost breaking.  She pulls a handkerchief from her purse which held one of her earrings she had taken apart earlier in the film.  The diamond from the earring falls down the drain of the sink and into the trap underneath.  As she takes it apart and retrieves it, she gets an idea as to how it could have been pulled and where the diamonds could be.  She goes down into the sewer under the company and finds Mr. Hobbs guarding a passage.  He pulls a gun on her to prevent her from finding the diamonds, but she finds the one from the beginning of the movie on the floor. Mr. Hobbs confesses that she is correct, it was never about the pension.  Then, Michael Caines character delivers in a moving hindsight to miss Quinn (and the public) the Anagnorisis point of the film, which has a larger projection over the fictionalized tale into the real life Social events which affected many working class Britons  – and internationally, to many others too from the larger issues of Health insurance policies failures   –   at one point or another, and the soddy dealings which individuals in places of responsibility did have causing, however indirectly all the suffering and deaths many went through, so unwarranted. This will have a deep effect in miss Quinns own life, beyond the learning experience of the grand theft he involved her to, at great life fear for self, and the crucial moment went she changes in her contempt for him into one of disarming admiration, and from a self seeking person into a greater one following in his abnegated and devoted footsteps. When his wife was diagnosed with cancer, the doctors told him that it was totally operable and treatable, since the tumor was caught early.  But when they sought coverage for the treatment through their insurance policy with a private hospital corporation, they were told it was not an emergency and the medical insurers refused to pay for it. By the time his wife was admitted to the hospital, the cancer had reached an advanced stage and was incurable. The plot was purely revenge, and stemming from the excruciating frustration and life shattering impotence of turning back tragic irreversible events, that were purely an almost anonymous social betray part of a larger scheme but with personal costs to him and his wife, unknown to the Hobbs but which after his wife death he came to find out.  The Chairman of the Board of London Diamond died of a heart attack, and the head of the insurance syndicate from Kings Row was forced to pay the ransom demanded with own personal funds, leaving him financially ruined and pushing him to take his own life also to avoid further investigation from the National Audit Office (United Kingdom)| Exchequer and Audit Dpt. (now known as N.A.O.) which would have revealed his Embezzlement| Fraud, bankrupting the Insurance safety deposits. He was Mr. Hobbs main target all along, as he formerly headed the medical insurance company, which for the purposes of skimming expenses to provide the funds he would invest to build his own personal fortune, followed the policies he scripted and which ultimately prevented Mr. Hobbs wife from getting the medical help she needed.

Once the deadline for the ransom has passed, Mr. Hobbs leaves.  Miss Quinn proceeds through the tunnel and finds the mountain of diamonds, she calls Mr. Finch.  She almost decides to give up the huge diamond as well, but decides against it.  As the film returns to the present, the reporter is more than intrigued by her story.  Quinn states that later on, she was passed over for a promotion once again, but tendered her resignation the very next day.  After she quit, she received a letter from a bank in Switzerland stating that a certain amount had been deposited into a numbered account on her behalf, 100 million pounds sterling.  Mr. Hobbs had given her all of it.  Quinn tells the reporter that when she received that money, that is when the real story began.  She gets up and walks away leaving a manuscript with the reporter.  The manuscript details how Quinn spent the rest of her life donating all of the money from the plot to different organizations from cancer research to African charities, especially in the blood diamond region.  But she did confess to keeping the diamond, perhaps as a symbol of the last little piece of vanity she had left. The reporter runs out of the cafe and watches Quinn walk down the street, as she looked in 1960.

==Cast==
* Michael Caine as Mr. Hobbs
* Demi Moore as Laura Quinn
* Lambert Wilson as Finch
* Joss Ackland as Milton Kendrick Ashtoncroft
* Constantine Gregory as Dmitriev
* Ahmed Ayman as Bondok
* Natalie Dormer as Cassie, the writer

==Critical reception==

The film received generally positive to average reviews from critics. As of 24 April 2008, the review aggregator Rotten Tomatoes reported that 56% of critics gave the film positive reviews, based on 93 reviews.  Metacritic reported the film had an average score of 57 out of 100, based on 21 reviews. 

==Box office performance==

The film was released 5 October 2007 in Spain and has grossed $2.2 million there as of 6 January 2008. 

==References==

 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 