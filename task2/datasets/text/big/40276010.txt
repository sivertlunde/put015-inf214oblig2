Red Nights
{{Infobox film
| name           = Red Nights
| image	         = Red_Nights_Poster.jpg
| image size     =
| alt            = 
| caption        = 
| director       = Julien Carbon Laurent Courtiaud
| producer       = Alexis Dantec Rita Wu
| writer         = Julien Carbon Laurent Courtiaud
| screenplay     = 
| story          =
| based on       = 
| narrator       = 
| starring       = Frédérique Bel Carrie Ng Jack Kao Alex Cortés Willie Cortés
| cinematography = Man-Ching Ng
| editing        = Sébastien Prangère
| studio         = The French Connection Red East Pictures
| distributor    = 
| released       = 
| runtime        = 98 minutes
| country        = Hong Kong France Belgium French Standard Cantonese
| budget         = 
| gross          = 
}}
Red Nights (Les Nuits rouges du Bourreau de Jade) is a French-Hong Kong film by  Julien Carbon and Laurent Courtiaud. It is a thriller and a tale of erotic horror. The filmmakers call it a Hong Kong giallo with mystery, (sadistic) murders, fetishism and women. 

==Plot==
There is a legend of an executioner who created a poison which brought death through absolute pleasure. This legends repeats nowadays when a French woman (Bel) flees to Hong Kong after killing her lover and stealing from him an old artifact containing this poison. She meets a mobster from Taiwan and an epicurean and sadistic woman killer (Ng), who all want to get the precious poison.

==Cast==
*Frédérique Bel: Catherine Trinquier
*Carrie Ng: Carrie Chan
*Carole Brana: Sandrine Lado
*Stephen Wong Cheung-hing: Patrick
*Kotone Amamiya: Tulip
*Maria Chen: Flora
*Jack Kao: Mister Ko

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

 