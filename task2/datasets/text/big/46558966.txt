Star Without Light
{{Infobox film
| name = Star Without Light
| image =
| image_size =
| caption =
| director = Marcel Blistène
| producer = Eugène Tucherer
| writer =  André-Paul Antoine   Marcel Blistène
| narrator =
| starring = Édith Piaf   Marcel Herrand   Jules Berry
| music =   Guy Luypaerts 
| cinematography =   Paul Cotteret
| editing =  Ginou Bretoneiche 
| studio = Société Universelle de Films 
| distributor = Société Universelle de Films 
| released =   3 April 1946   
| runtime = 88 minutes
| country = France  French 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Star Without Light (French:Étoile sans lumière) is a 1946 French drama film directed by Marcel Blistène and starring Édith Piaf, Marcel Herrand and Jules Berry.  The films art direction was by Jean dEaubonne. The film is set in 1929.

==Main Cast==
*  Édith Piaf as Madeleine  
* Marcel Herrand as Roger Marney  
* Jules Berry as Billy Daniel 
* Serge Reggiani as Gaston Lansac  
* Mila Parély as Stella Dora 
* Yves Montand as Pierre 
* Colette Brosset as Lulu  
* Renée Dennsy as La script-girl  
* Jean Raymond as Paul 
* Pierre Farny 
* Paul Frankeur as Le reporter 
* Pierre Mindaist 
* Ginette Cantrin 
* Juliette Cransac 
* Georges Yvon 
* Jean Rozemberg 
* Georges Vitray as Le producteur Darnois 
* Mady Berry as Mélanie  

== References ==
 

== Bibliography ==
* Gorlinski, Gini. The 100 Most Influential Musicians of All Time. Britannica Educational Publishing, 2009. 

== External links ==
*  

 
 
 
 
 
 
 

 