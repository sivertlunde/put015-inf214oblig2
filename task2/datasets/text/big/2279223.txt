Alvin Purple
 
 
 
{{Infobox film 
| name           = Alvin Purple
| image          = Alvin Purple (film).jpg
| director       = Tim Burstall
| producer       = Tim Burstall
| writer         = Alan Hopgood
| starring       = Graeme Blundell Lynette Curran Jill Forster Jacki Weaver Dina Mann
| music          = Brian Cadd
| cinematography = Robin Copping
| editing        = Edward McQueen-Mason
| studio         = Hexagon Productions Roadshow Entertainment (Australia) Columbia-Warner (UK)
| released       =  
| runtime        = 97 minutes
| country        = Australia
| language       = English
| budget         = A$202,000 Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998 p274   gross           = A$4,720,000
}}
Alvin Purple is an 1973 Australian comedy film  starring Graeme Blundell, written by Alan Hopgood and directed by Tim Burstall.

It received largely negative reviews from local film critics. Despite this it was a major hit with Australian audiences. Alvin Purple became the most commercially successful Australian film released to that time, breaking the box office record set by Michael Powells pioneering Anglo-Australian comedy feature Theyre a Weird Mob (film)|Theyre a Weird Mob (1966).

The score and title theme were composed by iconic Australian singer-songwriter Brian Cadd.

A 1974 film sequel Alvin Rides Again toned-down the sex scenes and nudity, adding more camp comedy.
 Australian Broadcasting Alvin Purple. Blundell reprised the title role in both, as well as in the 1984 movie Melvin, Son of Alvin.

==Story==
The film is a bedroom farce|sex-farce which follows the misadventures of a naïve young Melbourne man Alvin Purple, whom women find irresistible. Working in door to door sales, Alvin (unsuccessfully) tries to resist legions of women who want him.

Alvin is so worn-out he seeks psychiatric help to solve his problems. His psychiatrist is, of course, a woman. Alvin ultimately falls in love with the one girl who doesnt throw herself at him. She becomes a nun, and Alvin ends up a gardener in the convents gardens.

==Cast==
 
*Graeme Blundell as Alvin Purple Abigail as Girl in See-Through
*Lynette Curran as First Sugar Girl
*Jacki Weaver as Second Sugar Girl
*Christine Amor as Peggy
*Dina Mann as Shirley
*Penne Hackforth-Jones as Dr. Liz Sort George Whaley as Dr. McBurney
*Dennis Miller as Mr. Horwood
*Jill Forster as Mrs. Horwood
*Frederick Parslow as Alvins Father
*Valerie Blake as Alvins Mother
*Alan Finney as Spike Dooley
*Elli Maclure as Tina Donovan
*Noel Ferrier as the Judge
*Elke Neidhardt
*Gary Down as Roger Hattam
*Peter Aanensen as Ed Cameron
*Jenny Hagen as Agnes Jackson
*Kris McQuade as Samantha
*Shara Berriman as kinly lady
*Stan Monroe as Mrs Warren
*Eileen Chapman as patient
*Jan Friedl as Miss Guernsey
*Barbara Taylor as Mrs Phillips
*Anne Pendlebury as woman with pin
*Boguslawa Alicia Chojnowska 
*Danny Webb as newsreader
*Jon Finlayson as Lizs lawyer
*John Smythe as Alvins lawyer
*Brian Moll as clerk of the court
*Lynne Flanagan as foreman of jury
*Peter Cummins as cab driver
*Sally Conabere as second nun
*Carole Skinner as Mother Superior
 

==Background==
 La Mama Theatre in Melbourne, established by his wife Betty Burstall. La Mama was a major focus for the new wave of Australian drama that was emerging at that time, showcasing many new plays, performance pieces and films by people such as Jack Hibberd, Alex Buzo, David Williamson, Bert Deling and Burstall himself.

Burstalls first feature film, 2000 Weeks was an ambitious contemporary drama about a writer, starring Scots-born actor Mark McManus (of Taggart fame) and Australian actress Jeannie Drynan, which was very notable at the time, being the first all-Australian feature film produced since Charles Chauvels Jedda (film)|Jedda in 1954. Although it was reportedly well-received overseas, 2000 Weeks was panned by local critics and it failed disastrously at the box office. The experience affected Burstall strongly and also influenced other directors and producers, including John B. Murray and Phillip Adams, who observed the hostile reaction to 2000 Weeks and who as a result took their film-making in a more populist direction, as Burstall soon did himself.
 Getting Back to Nothing (1970). His second feature, the contemporary comedy Stork (film)|Stork (1972) was much more successful. As well as launching the cinema career of actor Bruce Spence, who played the title role, it was the first of many successful film adaptations of plays by renowned Australian dramatist David Williamson. Stork was adapted from his play The Coming of Stork, which had premiered at La Mama.

===Development===
In 1972 Burstall became a partner in a new film production company,  .   accessed 14 October 2012 

Burstall considered 26 stories from writers such as Bob Ellis, Williamson, Barry Oakley and Frank Hardy before settling on Alvin Purple by Alan Hopgood. Scott Murray, Tim Burstall, Cinema Papers Sept-Oct 1979 p494-495 
 And the Big Men Fly and he was well-known to TV audiences at the time for his long-running role as the town doctor in the Australian Broadcasting Corporation|ABCs Bellbird (TV series)|Bellbird. He originally wrote Alvin Purple for the English company Tigon Films. 

Hopgoods story was originally half comic, half serious, and Burstall originally envisioned it as a 20-minute section of a multi story picture. However he then decided to make the story strictly comic and expand it to feature length. Burstall says he rewrote much of Hopgoods script, adding many chases and the water bed sequence, and turning McBurney figure into a sex maniac. The original script played more emphasis on the relationship between Alvin and his virginal girlfriend but this was cut in the final film. 

The budget was provided entirely by Hexagon - half from Roadshow, half from Burstall, Bilcock and Copping - apart from a short term loan from the Australian Film Development Corporation, which was repaid before the films release.   Burstall cast Graeme Blundell in the lead:
 I remember Bourkie   saying, Youve got to cast somebody like Jack Thompson. I said, Absolutely not. Youve got to cast somebody who wouldnt, on the surface, seem a stud or even particularly attractive. I actually thought that Alvin wasnt, that the comic element was connected with having a Woody Allen or a Dustin Hoffman figure who is not very obviously sexually attractive, and the girls rushing him. This becomes much funnier than if he was a stud figure.  
Blundell was paid $500 a week for the role. 

===Production===
The film was shot over five and a half weeks in March and April 1973.

==Reception==
The film was a massive success and took $4,720,000 at the box office in Australia,  which is equivalent to $36,721,600 in 2009 dollars. This is 7th highest grossing Australian film of all time when adjusted for inflation.

In 1979 Burstall said the film had returned $2.4 million to the exhibitors, $1.6 million to the distributors, who took $500,000, leaving Hexagon with $1.1 million. The movie sold to television for $40,000 in 1977. 

In 2008 Catharine Lumby wrote a book about the film in the Australian Screen Classic Series. 

The film was released in the US as The Sex Therapist. 

===Inside Alvin Purple===
It was accompanied on release by a 48-minute promotional documentary Inside Alvin Purple directed by Brian Trenchard-Smith.  This film was pulled from screening due to censorship concerns but was passed after some cuts had been made. 

==Home media==
Alvin Purple was released on DVD by Umbrella Entertainment in April 2011. The DVD is compatible with region codes 2 and 4 and includes special features such as the theatrical trailer, a picture gallery, the Inside Alvin Purple documentary and interviews with Tim Burstall, Alan Finney, Robin Copping, Graeme Blundell, Jacki Weaver, and Ellie MacLure.   

==References==
 

==External links==
* 
*  
*  at Australian Screen Online
*  
*  at Oz Movies
* , The Age, Melbourne, 1 June 2001
*   at  

 
 

 
 
 
 
 
 
 