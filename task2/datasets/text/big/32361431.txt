Bin Bulaye Baraati
 
 

{{Infobox film
| name           = Bin Bulaye Baraati
| image          = Bin_Bulaye_Baraati_poster_1.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Chandrakant Singh
| producer       = Dhanraj Jethani, Dhanraj Films
| screenplay     = Praful Parekh M.Salim
| starring       = Aftab Shivdasani Priyanka Kothari Rajpal Yadav Sanjai Mishra Om Puri Vijay Raaz
| music          = Anand Raj Anand Sanjoy Chowdhury (background music) 
| cinematography = Johny Lal
| editing        = Pranav Dhiwar, pankaj sharma
| studio         = 
| distributor    = Dhanraj Films
| released       =  
| runtime        = 
| country        = India subtitles
| budget         = 
| gross          =
}} song numbers. The film was released in India on 17 June 2011   and received mixed reviews.

==Plot==
Criminals Hazari (Sanjay Mishra) and Murari (Rajpal Yadav) rob the Police Commissioners home, and Chetta Singh (Vijay Raaz) steal the car belonging to the Commissioners wife, Kusum (Rati Agnihotri).  Meanwhile, AD (Aftab Shivdasani) elopes with Shreya (Priyanka Kothari), the niece of Police Sub-Inspector Pralay Pratap Singh (Om Puri). With each group is on the run for different reasons, they all end up in a vehicle stolen from crime boss Durjan Singh/Black Cobra (Gulshan Grover).  In the vehicle is a suitcase filled with Durjans loot.

On the run from both gangsters and police, they group disguises themselves using fake police uniforms, and arrives in Madhavgarh village just in time to stop Loha Singh (Manoj Joshi) from committing a crime.  Believing them to be real law officials, the towns residents host a congratulatory feast in their honor. The group settles in comfortaby among their new-found friends, but the Black Cobra tracks them down and plots their capture and death.

==Cast==
* Aftab Shivdasani as AD
* Priyanka Kothari as Shreya
* Shweta Tiwari as Rajjo/Rosie
* Rati Agnihotri as Kusum / Kuki
* Shakti Kapoor as Ajay Prakash
* Rajpal Yadav as Murari
* Sanjai Mishra as Hazari
* Gulshan Grover as Durjan Singh / Black Cobra
* Om Puri as Sub Inspector Pralay Pratap Singh
* Vijay Raaz as Chetta Singh
* Manoj Joshi as Loha Singh
* Mukesh Tiwari as Gajraj
* Hemant Pandey as Ranjeet
* Johnny Lever as Sajjan Singh / Badi Bi
* Dinesh Lamba as Havaldar
* Rocky Verma as Teeka Singh
* Neeraj Vora as Police Commissioner
* Shweta Keswani as ginni
* Razak Khan as Sajjans Dance Teacher Item song "Shalu Ke Thumke"

==Production==
It was announced February 2011 that Anand Raj Anand would be composing songs for the film. {{cite news 
| url=http://articles.timesofindia.indiatimes.com/2011-02-19/news-interviews/28614755_1_mallika-sherawat-anand-raj-anand-numbers 
| title=Who is Anand Raj Anand wooing? 
| work=Times of India 
| date=19 February 2011  
| accessdate=10 July 2011 
| author=Mallika Sherawat}}  By March 2011 principle filming had commenced. {{cite news 
| url=http://articles.timesofindia.indiatimes.com/2011-03-02/news-interviews/28646491_1_priyanka-kothari-s-homepage-yacht-fleet-water-baby
| title=Priyanka Kotharis a water baby! 
| work=Times of India 
| date=2 March 2011 
| accessdate=10 July 2011 
| author=TNN}}  In April 2011, it was released that actress Shweta Tiwari would have a lead role in the film, that she would replace Mallika Sherawat in singing some songs for the film, and that Sherawat would until be in the film, but as a dancer and not a singer. {{cite news 
| url=http://articles.timesofindia.indiatimes.com/2011-04-11/tv/29405803_1_post-bigg-boss-reliable-source-song
| title=Shweta Tiwari starts afresh 
| work=Times of India 
| date=11 April 2011 
| accessdate=10 July 2011 
| author=Kunal M Shah and Vickey Lalwani}}  One of Tiwaris song pieces was shot a scene solo with the other actors not present, but the others were added to the scene in post-production. {{cite news 
| url=http://movies.ndtv.com/movie_story.aspx?section=Movies&Id=ENTEN20110176500&keyword=bollywood&subcatg=MOVIESINDIA&nid=108471 
| title=Mallika Throwing Starry Tantrums? 
| work=NDTV / MiD DAY 
| date=27 May 2011 
| accessdate=10 July 2011}} 

===Soundtrack===
{{Track listing
| extra_column = Singer(s)
| title1 = Shalu ke thumke | extra1 = Anupama Raag | length1 = 3:36
| title2 = Dil Ka Achar | extra2 = Dimple Balhotra and Mamta Sharma | length2 = 3:19
| title3 = Kismat | extra3 = Ritu Pathak | length3 = 3:29
| title4 = Bin Bulaye Baraati | extra4 = Anand Raj Anand | length4 = 3:06
| title5 = Sawan Ka Tha Mahina | extra5 = Shaan and Gorisha | length5 = 3:42
}}

==Reception==

===Critical response===
The film received mixed response from critics. Mid-Day noted that "with a line-up of seasoned character actors known for their comic timings", the film could have been a "laugh ride", writing that the filmmakers "felt their mere presence was enough to prop the film" allowing them to work with a "meandering script" that jumped "from one character to another" in a manner "bound to test the audiences patience".  They also chided the inane dialogues and referred to the film as "balderdash", and a "no-brainer... ...made purely for the aam janata of the hinterland". {{cite news 
| url=http://www.mid-day.com/entertainment/2011/jun/180611-Bin-Bulaye-Baraati-Movie-review.htm 
| title=Bin Bulaye Baraati - Movie review 
| work=MiD DAY 
| date=18 June 2011 
| accessdate=10 July 2011 
| author=Shaheen Parkar}} 

IndiaGlitz noted that through consideration of cast of established comic actors, there were certain "set expectations" for the film, in that viewers would not expect "a classic in the making" and would appreciate surrendering "to the antics of a dozen odd comic players on the screen."  They also noted that the entire first half of the film is spent establishing its characters and that the second half "is an unabashed copy of Sholay with a little bit of Tees Maar Khan added as well."  They concluded that "despite very low expectations" the film itself "turns out to be a barely passable affair". {{cite web 
| url=http://www.indiaglitz.com/channels/hindi/review/13459.html 
| title=Bin Bulaye Baarati Movie Review 
| publisher=IndiaGlitz 
| date=18 June 2011 
| accessdate=10 July 2011 
| author=Joginder Tuteja}} 

Glamsham noted that with the films multiple comedic actors and special songs it "clearly aims to be a complete masala film."  They also noted that with "a plethora of actors who are known to tickle your funny bone, one would have at least expected a few laughs" from the film, but that it instead "turns out to be a tedious watch and a test of your patience."  They wrote that the plot was unoriginal, the treatment was poor and the films climax was both predictable and "totally uninspiring." They concluded that "Anand Raj Anands music is passable. Johny Lalls cinematography is unimpressive. Pranav Dhiwars editing is tacky." {{cite web 
| url=http://www.glamsham.com/movies/reviews/18-bin-bulaye-baraati-movie-review-061106.asp 
| title=Bin Bulaye Baraati Movie Review 
| publisher=Glamsham 
| date=18 June 2011 
| accessdate=10 July 2011 
| author=Pankaj Sabnani}} 

===Box office===
Bin Bulaye Baraati had an average opening commercially, and collected an poor gross of 65&nbsp;million in its opening weekend. Facing competition with the film Ready (2011 film)|Ready, Always Kabhi Kabhi and Double Dhamaal seems to be the official reason to why the film failed to do well at the box office. Its total gross was 130&nbsp;million worldwide, and was declared a Disaster grosser worldwide by Box office India.

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 