The Caddy
:For the Seinfeld episode, see "The Caddy (Seinfeld)"
{{Infobox film
| name = The Caddy
| image = thecaddy.jpg
| director = Norman Taurog
| writer = Danny Arnold Edmund Hartmann
| starring = Dean Martin Jerry Lewis Donna Reed Barbara Bates Paul Jones
| studio   = York Pictures Corporation
| distributor = Paramount Pictures
| released =  
| runtime = 95 minutes
| country = United States
| language = English budget = $1,864,112 Michael A. Hoey, Elvis Favorite Director: The Amazing 52-Film Career of Norman Taurog, Bear Manor Media 2013 
| gross = $3.5 million (US)  1,008,197 admissions (France)   at Box Office Story 
}}
The Caddy is a 1953 American film starring the comedy team of Martin and Lewis.

It was filmed from November 24, 1952 through February 23, 1953 and was released by Paramount Pictures on August 10, 1953. It was later re-released in 1964 on a double bill with another Dean Martin and Jerry Lewis picture, Youre Never Too Young. 

This was the teams first film since 1950s At War with the Army to be produced by their own production company, York Pictures Corporation.  It is also notable for cameo appearances by some of the leading professional golfers of the era (all playing themselves), including Ben Hogan, Sam Snead, Byron Nelson, and Julius Boros.

==Plot==
The story centers around Harvey Miller (Jerry Lewis), whose father was a famous golf pro.  He wanted Harvey to follow in his footsteps, but poor Harvey is afraid of crowds.  Instead, at the advice of his fiancée Lisa (Barbara Bates), Harvey becomes a golf instructor. Lisas brother Joe (Dean Martin) becomes Harveys first client and becomes good enough to start playing in tournaments, with Harvey tagging along as his caddy.  Donna Reed plays the wealthy socialite who Dean wins over. 

Joes success goes to his head and he begins to treat Harvey poorly. They begin to quarrel and cause a disruption at a tournament, so Joe is disqualified. However, a talent agent witnesses the comical spectacle and advises that they go into show business.

Harvey conquers his fear and they become successful entertainers. At the end, Harvey and Joe meet up with another comedy team who look just like them: Martin and Lewis!

==Music==
The score for the film includes the hit "Thats Amore", sung by Dean Martin. It was nominated for an Academy Award for best song, but it did not win.

==Cast==
*Dean Martin as Joe Anthony
*Jerry Lewis as Harvey Miller, Jr.
*Donna Reed as Kathy Taylor
*Barbara Bates as Lisa Anthony
*Joseph Calleia as Papa Anthony
*Fred Clark as Mr. Baxter aka Old Skinhead
*Clinton Sundberg as Charles, Butler Howard Smith as Golf Official
*Marshall Thompson as Bruce Reeber
*Marjorie Gateson as Mrs. Grace Taylor
*Frank Puglia as Mr. Spezzato Lewis Martin as Mr. Taylor
*Argentina Brunetti as Mama Anthony William Edmunds as Caminello Henry Brandon as Mr. Preen

==Production==
During shooting, on 8 January 1953 production was suspended for 23 days when Lewis entered Cedars of Lebanon Hospital with a fever. The movie became Martin and Lewis most expensive to date. 

==Promotion==
The team made a promotional radio message for the movie.  Several outtakes, available on The Golden Age of Comedy: Dean Martin and Jerry Lewis CD, feature Dean and Jerry trying to get through five lines of dialogue. When either one of them messed up a line, they exchanged several lines of profanity.

==Home media release==
The film was included on an eight-film DVD set, the Dean Martin and Jerry Lewis Collection: Volume One, released on October 31, 2006.

==See also==
* List of American films of 1953

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 