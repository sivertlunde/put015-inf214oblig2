Neethipeedam
{{Infobox film
| name           = Neethipeedam
| image          =
| caption        =
| director       = Crossbelt Mani
| producer       =
| writer         =
| screenplay     = Madhu Sheela KP Ummer Maniyanpilla Raju
| music          = G. Devarajan
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       =  
| country        = India Malayalam
}}
 1977 Cinema Indian Malayalam Malayalam film, directed by Crossbelt Mani. The film stars Madhu (actor)|Madhu, Sheela, KP Ummer and Maniyanpilla Raju in lead roles. The film had musical score by G. Devarajan.   

==Cast== Madhu
*Sheela
*KP Ummer
*Maniyanpilla Raju
*Thikkurissi Sukumaran Nair

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Yusufali Kechery and Bharanikkavu Sivakumar. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Daivam Manushyanaay || K. J. Yesudas || Yusufali Kechery || 
|-
| 2 || Poovinu Vannavano || P. Madhuri || Yusufali Kechery || 
|-
| 3 || Pularkalam || K. J. Yesudas || Yusufali Kechery || 
|-
| 4 || Viplava Gaayakare || P Jayachandran || Bharanikkavu Sivakumar || 
|}

==References==
 

==External links==
*  

 
 
 

 