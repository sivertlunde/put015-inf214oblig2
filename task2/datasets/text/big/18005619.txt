Chathurangam (2002 film)
{{Infobox film|
  name      = Chathurangam|
  image     = Chathurangam.jpg|
  director  = K. Madhu|
  script    = Babu Janardanan |
  starring  =  Mohanlal Lalu Alex Navya Nair Nagma  Saikumar (Malayalam actor)|Saikumar|
  released  = 2002|
  runtime   =|
  country   = India |
  language  = Malayalam |
}}

Chathurangam is a Malayalam movie released in 2002 directed by K. Madhu and featuring  Mohanlal, Navya Nair, Lalu Alex  and Nagma.

== Plot ==

The story is set in Kottayam. Atiprackal Jimmy Jacob is everyones favourite. Something turns around his life.

== Cast ==

* Mohanlal as Atiprackal Jimmy Jacob
* Navya Nair as Sherin Mathew
* Nagma as Nayana Pillai IPS Saikumar as K.C. Korah
* Lalu Alex as Methikkalam Thommichan
* Jagadish as Mathen Chethimattom
* Jagathi Sreekumaras Kochausepp Vijayaraghavan as P.P. Paulose
* Manianpilla Raju as Alex (Brother-in-law of Jimmy)
* Nedumudi Venu as Mathew Varghese (father of Sherin Mathew)
* Hariprasad R as Lawyer 
*Valsala Menon Augustine
* K.P.A.C. Lalitha as Therutha
* Bindu Panikkar as Annie (Sister of Jimmy)
* Radhika Menon as Liza

== Crew ==

*Direction - K. Madhu

 
 
 


 