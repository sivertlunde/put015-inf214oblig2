The Happiness Cage
 

{{Infobox Film
| name           = The Happiness Cage
| image          = Poster of the movie The Happiness Cage.jpg
| image_size     = 
| genre          = Drama   Sci-Fi
| director       =  
| writer         = Dennis Reardon Ron Whyte
| starring       = Christopher Walken Joss Ackland Ralph Meeker
| cinematography = 
| editing        = 
| distributor    = Cinerama Releasing Corporation
| released       = 1972
| runtime        = 94 min.
| country        = United States Denmark English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
The Happiness Cage (1972) is a science fiction film directed by Bernard Girard. The film stars Christopher Walken in his first starring role and Joss Ackland. The film was also known as The Mind Snatchers and it is the first starring role for Walken.

==Plot==
Dr. Frederick (Joss Ackland) is trying to find a way to ease the aggressive nature of soldiers by developing a microchip to access the pleasure centers of their brains.

==Cast==
*Christopher Walken as Pvt. James H. Reese
*Joss Ackland as Dr. Frederick
*Ralph Meeker as The Major
*Ronny Cox as Sgt. Boford Miles
*Marco St. John as Lawrence Shannon
*Bette Henritze as Anna Kraus Susan Travers as Nurse Schroeder
*Birthe Neumann as Lisa
*Claus Nissen as Army Psychiatrist

==External links==
* 

 
 
 
 
 

 