Mr. Big (film)
 
{{Infobox film
| name           = Mr. Big
| image          = 
| caption        = 
| director       = Tiffany Burns
| producer       = Tiffany Burns
| writer         = Tiffany Burns
| starring       = Glen Sebastian Burns Tiffany Burns
| music          = Jeff Tymoschuk
| cinematography = 
| editing        = Alec MacNeill Richardson
| studio         = 
| distributor    = 
| released       =  
| runtime        = 89 minutes
| country        = Canada
| language       = English
| budget         = 
| gross          = 
}}
Mr. Big is a 2007 documentary directed and produced by Tiffany Burns and edited by Alec MacNeill Richardson. The documentary examines the Mr. Big (police procedure)|"Mr. Big" undercover methods used by the Royal Canadian Mounted Police (RCMP). In these operations, RCMP officers pose as gang criminals and develop a relationship with the target in the hope of eventually determining what, if any, knowledge the target has of the crime being investigated. "Mr. Big" operations have been credited with securing difficult convictions in a large number of cases, but concerns have been raised that they involve a risk of false confessions and wrongful convictions.

Tiffany Burns is the sister of Sebastian Burns who, along with Atif Rafay, was convicted of murdering Rafays family in Bellevue, Washington in 1994. The major evidence presented at the high profile 2003 trial of Burns and Rafay was a confession that occurred in the context of a "Mr. Big" operation.  The Burns / Rafay case is one of those featured in Mr. Big.

Mr. Big includes interviews with targets of "Mr. Big" operations and their families &ndash; including the Burns family, interviews with various professionals who have an interest in the "Mr. Big" tactics, and RCMP footage of "Mr. Big" operations.

==Cast==
*Glen Sebastian Burns  as Himself
*Tiffany Burns  as Herself
*Donna Larsen as Herself
*Atif Rafay  as Himself

== References ==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 
 
 


 
 