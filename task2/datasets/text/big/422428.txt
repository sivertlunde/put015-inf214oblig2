Johnson Family Vacation
{{Infobox film
| name = Johnson Family Vacation| image = Family-vaca.jpg
| caption = Theatrical release poster
| director = Christopher Erskin
| producer = Andrew Sugerman, Cedric the Entertainer, Earl Richey Jones, Eric Rhone, Paul Hall, Todd R. Jones, Wendy Park
| writer = Earl Richey Jones Todd R. Jones Bow Wow Vanessa Williams
| music = Richard Gibbs
| cinematography = Jeff Barnett Shawn Maurer
| editing = John Carter
| distributor = Fox Searchlight Pictures
| released =  
| runtime = 97 minutes
| country = United States
| language = English
| budget = $12 million   
| gross = $31,462,753
}} Vanessa Williams, Bow Wow, Gabby Soleil, Shannon Elizabeth, Solange Knowles, and Steve Harvey. The film is director Erskins first feature film directorial project.

==Plot==
  Bow Wow), Los Angeles to Missouri. Between his wary wife, arguing kids, angry police officers, a bizarre hitchhiker (Shannon Elizabeth), bad diner food, and an unfortunate run-in with a cement mixer, getting from point A to point B proves to be just short of a nightmare.

==Cast==
* Cedric the Entertainer as Nate Johnson/Uncle Earl Bow Wow as Divirnius James D.J. Johnson Vanessa Williams as Dorothy Johnson
* Christopher B. Duncan as Stan
* Solange Knowles as Nikki Johnson
* Shannon Elizabeth as Chrishelle Rene Boudreau
* Gabby Soleil as Destiny Johnson
* Steve Harvey as Mack Johnson
* Aloma Wright as Glorietta Johnson Godfrey as Motorcycle Cop
* Jason Momoa &mdash; Navarro
* Jennifer Freeman &mdash; Jill
* Jeremiah "J.J." Williams Jr. &mdash; Cousin Bodie
* Lee Garlington &mdash; Betty Sue
* Lorna Scott &mdash; Gladys
* Philip Daniel Bolden as Mack Jr.
* Rodney Perry &mdash; Cousin Lump
* Shari Headley &mdash; Jacqueline
* Tanjareen Martin &mdash; Tangerine

==Reception==
Johnson Family Vacation received generally negative reviews. It scored a 6% rating on Rotten Tomatoes based on 90 reviews,  making it one of the sites worst-reviewed movies of 2004. The film has a 29/100 rating on Metacritic, based on 24 reviews.  The critics gave it a C− rating at Yahoo! Movies. 

 

==Awards & nominations==
2004 BET Comedy Awards
* Outstanding Lead Actor in a Box Office Movie &mdash; Cedric the Entertainer (nominated) Vanessa Williams (nominated)
* Outstanding Writing for a Box Office Movie (nominated)

==References==
 

==External links==
*  
*   – Official site
*  
*  
*  
*  

 
 
 
 
 
 
 
 