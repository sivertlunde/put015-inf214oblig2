Fair Game (1995 film)
{{Infobox film
| name           = Fair Game
| image          = fair game.jpg
| caption        = Theatrical release poster
| alt            =
| director       = Andrew Sipes
| based on       =  
| screenplay     = Charlie Fletcher
| producer       = Joel Silver
| starring       = {{Plain list|
* William Baldwin
* Cindy Crawford
* Steven Berkoff
* Christopher McDonald
* Salma Hayek
}}
| music          = Mark Mancina
| cinematography = Richard Bowen
| editing        = David Finfer Steven Kemper Christian Wagner
| studio         = Silver Pictures
| distributor    = Warner Bros.
| released       =  
| runtime        = 91 minutes
| country        = United States
| language       = English
| budget         = $50 million (estimated) 
| gross          = $11,534,477   
}} directed by attorney Kate McQuean and William Baldwin as Max Kirkpatrick, a Florida police officer. Kirkpatrick ends up on the run to protect McQuean when she is targeted for murder by ex-members of the KGB with interests in a ship owned by a Cuban man who may lose it in a divorce case being pursued by McQuean.

The film is based on Paula Goslings novel of the same name, which was previously adapted into the 1986 Sylvester Stallone film Cobra (1986 film)|Cobra.
 Miami Beach, and the Florida Keys National Marine Sanctuary.

==Plot ==

Kathryn "Kate" McQuean (Cindy Crawford) is a Miami lawyer who—in the course of a divorce proceeding—attempts to seize a 157-foot freighter docked off the Florida coast in lieu of unpaid alimony.

The freighter which is owned by criminal Emilio Juantorena (Miguel Sandoval), is the current base of operations of Ilya Pavel Kazak (Steven Berkoff), a former KGB agent who has become an international money laundering expert, and he has also become the leader of a group of rogue KGB members, including Stefan (Gustav Vintas), Leonide "Hacker" Volkov (Paul Dillon), Navigator (Marc Macaulay), Rosa (Jenette Goldstein) and Zhukov (Olek Krupa).

When Kate is unintentionally hit by a stray bullet, Miami detective Max Kirkpatrick (William Baldwin) is assigned to the case. Then an attempt is made on Kates life by Kazak who after killing Juantorena, assembles his team into tracking and killing Kate.

Max becomes her protector, as it turns out that Kazak wants Kate dead. Kate, Max and two of his colleagues stay at a hotel. They order pizza, however Volkov traces the order and Rosa and two henchmen infiltrate the hotel and kill Maxs colleagues. Max manages to kill the whole hit squad (except Rosa) and he and Kate then leave. After Max contacts his superior Lt. Meyerson (Christopher McDonald), FBI agents are sent to escort them. The "agents" turn out to be henchmen, working for Kazak and Maxs partner and long-time friend Detective Louis Aragon (John Bedford Lloyd) is killed in the process. After killing some of Kazaks men, Max and Kate travel throughout Florida, trying to avoid Kazak while trying to find out why he wants Kate dead. 

Their car, however breaks down along a freeway and they hook it to truck. Volkov and Stefan show up to kill them while Kazak splits with them to deal with Maxs cousin who has been feeding them information, regarding Kazak and his past activities in Cuba and Kate and Max ae forced to run with the truck while their car is still hooked onto it. After a long chase and gunfight, Kate steps hard on the brakes while Max steers the wheel of the truck, unhooking their car and causing it to crash into Volkovs and Stefans SUV vehicle, killing both of them.

Regardless, Kazak has Kate kidnapped and taken to the freighter and Rosa and Zhukov are sent to kill Max. They accidentally kill the Navigator and Max shoots both of them. Rosa, however has a bulletproof vest and after a long fight, Max kills her. Max then boards the freighter in an attempt to rescue Kate. The film ends when Max and Kate blow up the freighter, killing Kazak. As the freighter sinks, Kate says to Max, "You owe me a new boat." The duo laughs as a rescue helicopter picks them up.

==Cast==
* William Baldwin as Max Kirkpatrick, a Miami police detective
* Cindy Crawford as Kate McQuean, a civil-law lawyer who becomes a target for murder
* Steven Berkoff as Col. Ilya Kazak, a rogue ex-KGB operative now heads a group of terrorists
* Christopher McDonald as Lt. Meyerson, Maxs boss
* Miguel Sandoval as Emilio Juantorena, the owner of the freighter where Kazaks base of operations is at
* Johann Carlo as Jodi Kirkpatrick, Maxs cousin who is a forensics specialist
* Salma Hayek as Rita, Maxs ex-girlfriend
* John Bedford Lloyd as Det. Louis Aragon, Maxs partner
* Jenette Goldstein as Rosa, Kazaks brutal henchwoman
* Paul Dillon as Leonide "the Hacker" Volkov, one of Kazaks henchmen
* Olek Krupa as Zhukov, Kazaks henchman
* Gustav Vintas as Stefan, one of Kazaks henchmen
* Marc Macaulay as Navigator, one of Kazaks henchmen
* Dan Hedaya as Walter Hollenbach (uncredited), Juantorenas attorney

==Production==
Geena Davis, Julianne Moore and Brooke Shields were all offered the role of Kate McQuean, but they all passed as they were busy with other projects, before Cindy Crawford was ultimately cast.

Initially, Fair Game ran for 95 minutes, but after re-edits and reshoots, the film came in four minutes shorter. After poor test screenings, Warner Bros. cut some scenes and reshot others. In the original version, Elizabeth Pena played the role of Rita, Maxs ex-girlfriend, hence her name was included on the poster and the trailer. But when test audiences thought that Pena didnt seem right for the role, she was fired and Salma Hayek was brought in to replace her, saying that the only reason she took that part was that she insisted that she rewrite the scenes she was in. Cindy Crawford and William Baldwin also shot additional scenes to help boost the relationship between their characters. Crawford also shot more scenes on her own in order to develop her character. Dan Hedaya had a bigger part in the original cut, but was shortened down to just one scene and he chose to go uncredited for that reason. The extra filming/additions and re-edit caused the film to be delayed by three months.

Theatrical trailer shows some deleted and alternate scenes from original cut of the movie before it was partially re-shot and re-edited; Alternate interrogation scene between McQuean and Kirkpatrick in the beginning where he asks her some questions and she says that nobody tried to kill her, another extended part of this scene where she asks him does he has problem with lawyers and he says that hes a cop and that its "written on the badge". Alternate dialogue between them while they are driving in car at night where he says that they cant trust the cops and when she asks him why she should trust him he says because he hasnt shoot her yet and adds a line "Nights still young". Additional scene where he gives her the gun and when she says that she doesnt know how to shoot he says its just like using a camera, just point and shoot. Additional scene where Kirkpatrick asks McQuean will she hit him but she says "Nights still young", same line he said to her in another deleted scene shown in trailer.

==Reception==

===Box Office===
Fair Game is considered to be a box office bomb, grossing only United States dollar|USD$11.5 million against production budget of $50 million. 

===Critical response===
Fair Game was panned by critics, with review aggregation website Rotten Tomatoes giving it a score of just 13% based on reviews from 24 critics, with an average score of 2.4/10. Most critics singled out Crawfords poor acting, with Liam Lacey of the Globe and Mail saying that "One could scavenge the thesaurus to find synonyms for awkward to describe Crawfords performance."   
 
It was nominated for three Razzie Awards, for Worst Actress (Crawford), Worst New Star (Crawford) and Worst Screen Couple (Crawford and Baldwin), where it lost all of these categories to Showgirls. 

==See also==
* Cobra (1986 film)|Cobra, a 1986 film based on the book of the same name.

==References==
 

== External links ==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 