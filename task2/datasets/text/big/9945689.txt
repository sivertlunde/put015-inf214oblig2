Louvre Come Back to Me!
 
{{Infobox Hollywood cartoon series = Looney Tunes (Pepé Le Pew) image =  caption =  director = Chuck Jones Maurice Noble (co-director) story_artist = John Dunn animator = Richard Thompson layout_artist =  background_artist = Phillip DeGuard Tom OLoughlin voice_actor = Mel Blanc Julie Bennett (uncredited) musician = Milt Franklyn producer = David H. DePatie. studio = Warner Bros. distributor = Warner Bros. release_date = August 18, 1962 (USA) color_process = Technicolor runtime = 6:28 preceded_by = A Scent of the Matterhorn followed_by = movie_language = English
}}
Louvre Come Back to Me! is a 1962 Looney Tunes cartoon directed by Chuck Jones. It is the last Pepé Le Pew cartoon of the "classic" Warner Bros. animation age.

==Plot== ginger cat and Pepes stink causes the ginger cat to faint and the female cat to spring in the air getting her back on a fresh white-painted flagpole before she falls right into Pepes arms. As Pepe introduces himself, the female cat scurries away.

Pepe chases the female cat into the Louvre, the ginger cat following. Pepes stench ruins a couple of sculptures (correcting one into the Venus de Milo) as well as thwarting the ginger cats ambush attempt and he terrifies the female cat in the sculpture galley, even as he paints her picture ("Dont move, darling. I want to remember you just as you are."), she scurries away again ("Aw, shucks... You moved!").

The ginger cat pumps himself with air in an attempt to hold his breath while he confronts Pepe. Pepe plays along the confrontation as a duel, miming a miss and a defeat. The ginger cat in the meantime suffocates and puffs out all the air he held in, launching himself into the Hall dArmour. Pepe wonders where everyone has gone to and immediately picks up on where Penelope went.

Pepe finds the female cat hiding in the Air Conditioning machine and traps her in it with himself. Pepes fumes spread through the Louvre spoiling various works of art, even causing the Mona Lisa to talk ("I can tell you chaps one thing. Its not always easy to hold this smile.").

==Edited versions==
* The whole sequence of the ginger cats challenge with Pepe is left out in Daffy Ducks Fantastic Island for unknown reasons.

==Availability==
* DVD -  
* VHS - Daffy Ducks Fantastic Island

==External links==
*  
*  

 
 
 
 
 


 