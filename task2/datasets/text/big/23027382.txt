Faktas
{{Infobox film
| name           = Faktas
| image          = 
| caption        = 
| director       = Almantas Grikevicius
| producer       = 
| writer         = Vytautas Zalakevicius
| starring       = Regimantas Adomaitis
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 91 minutes
| country        = Soviet Union
| language       = Lithuanian
| budget         = 
}}
Faktas ( ) is a 1981 Soviet war film directed by Almantas Grikevicius. It was entered into the 1981 Cannes Film Festival, where Yelena Solovey won the award for Best Supporting Actress.   

==Cast==
* Regimantas Adomaitis
* Donatas Banionis
* Juozas Budraitis
* Uldis Dumpis
* Aleksandr Kaidanovsky
* Irena-Marija Leonaviciute
* Arnis Licitis
* Algimantas Masiulis
* Laimonas Noreika
* Yelena Solovey
* Leonid Obolensky
* Eugenija Pleskyte

==References==
 

==External links==
* 
* 

 
 
 
 


 
 