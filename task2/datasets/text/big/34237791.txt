Zakhmi Sher
{{Infobox film
| name           =Zakhmi Sher
| image          = ZakhmiSherfilm.jpg
| image_size     =
| caption        = 
| director       = Dasari Narayana Rao
| producer       = 
| writer         =
| narrator       = 
| starring       = Jeetendra   Dimple Kapadia
| music          = Laxmikant-Pyarelal
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1984
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
 1984 Bollywood film directed by Dasari Narayana Rao. It stars Jeetendra and Dimple Kapadia. The film was a remake of directors own Telugu film Bobbili Puli.

==Cast==
*Jeetendra as Major Vijay Kumar Singh
*Dimple Kapadia as Advocate Anu Gupta
*Amrish Puri as Swami Kashinath Singh
*Shakti Kapoor 	as Arsonist
*Om Shivpuri as Gupta 

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Hari Aao Hari Aao"
| Lata Mangeshkar
|-
| 2
| "O Mere Honewale"
| Kishore Kumar, Asha Bhosle
|-
| 3
| "Dhaka Chiki Maza Aane Laga"
| Alka Yagnik, Hemlata
|-
| 4
| "Yeh Janmabhoomi Janma Devi Maa"
| Suresh Wadkar
|-
| 5
| "Dekha Jo Tujhko"
| Shabbir Kumar, Kavita Krishnamurthy
|}

==External links==
*  

 
 
 
 
 
 
 