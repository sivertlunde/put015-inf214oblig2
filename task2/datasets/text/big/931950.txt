Cocoon: The Return
{{Infobox film
| name           = Cocoon: The Return
| image          = Cocoonthereturn.jpg
| caption        = Promotional movie poster for the film
| director       = Daniel Petrie
| producer       = Richard Zanuck
| writer         = Stephen McPherson
| starring       = Don Ameche Wilford Brimley Courteney Cox Hume Cronyn Jack Gilford Steve Guttenberg Barret Oliver Maureen Stapleton Jessica Tandy Gwen Verdon Tahnee Welch
| music          = James Horner
| cinematography = Tak Fujimoto
| editing        = Mark Roy Warner
| distributor    = 20th Century Fox
| released       =  
| runtime        = 116 minutes
| country        = United States
| awards         =
| language       = English
| budget         = $17.5 million 
| gross          = $25,024,919 
}}
Cocoon: The Return is a 1988 science fiction film that is the sequel to the 1985 film Cocoon (film)|Cocoon. All of the starring actors from the first film reprised their roles in this film, although Brian Dennehy only appears in one scene at the end of the film. Unlike its predecessor, the film was neither a commercial nor a critical success at the time of release, though has increased in popularity in subsequent years.

== Plot==

Five years after they left, the Antareans return to Earth to rescue the cocoons that were left behind. Before they can be retrieved, one of the cocoons is discovered by a science research team and taken to a secure laboratory for testing. The aliens and their human allies must find a way to retrieve the cocoon in time for their rendezvous with the rescue ship, while the humans travelling with them must decide whether to return to Antarea or stay on Earth and become mortal again. 

Joe learns that his leukemia has returned, but he knows it will be cured again as soon as he and Alma leave Earth. When Alma is subsequently hit by a car while attempting to save a child, Joe gives up the last of his lifeforce, saving her life but sacrificing his. Before dying, he tells Alma to take the job and that he loves her. Art and Bess learn that Bess is pregnant, and decide to raise the child on Anterea so they will live long enough to see him grow up. Ben and Mary, reconnect with their family and friends, including Bernie who is shown to have found love with Ruby, alleviating his depression over Roses death. And although a lovelorn Jack once again attempts to woo Kitty, she instead grants him a vision of his future, showing him children and a wife with a small heart-shaped birthmark on her neck.

The next night, before Ben, Mary, Art and Bess leave to meet the Antareans, Alma tells them she is staying on Earth to work at the preschool. Art, Kitty, Ben and his grandson David then rescue the Antarean from the Oceanographic Institute. Sara, one of the scientists working at the institute becomes aware of the companys plans to hand the alien over to the military. Unhappy about this, when she discovers the rescuers she allows them to escape.

After the four get the Antarean on Jacks boat out at sea, Ben makes it known to everyone that he and Mary were going to stay on Earth as well, since family was more important than living forever and that they should not outlive their children. When the space ship arrives, they are met by Walter before the Antareans, Art, Bess, and the cocoons left behind from the previous trip are brought aboard the space ship which departs for their homeworld.

Back at port after he has said his goodbyes to Ben, Mary and David, Jack is approached by Sara asking if he knows of a place where she could get some gas. They walk and talk for a bit, where Sara tells him she just quit her job. He eventually notices the small heart-shaped birthmark on her neck.

==Cast==

*Don Ameche	 ...	Art Selwyn
*Wilford Brimley ...	Ben Luckett
*Hume Cronyn	 ...	Joe Finley
*Courteney Cox	 ...	Sara
*Brian Dennehy	 ...	Walter
*Jack Gilford	 ...	Bernie Lefkowitz
*Steve Guttenberg...	Jack Bonner 
*Maureen Stapleton ...	Mary Luckett
*Jessica Tandy	 ...	Alma Finley
*Gwen Verdon	 ...	Bess McCarthy-Selwyn
*Elaine Stritch	 ...	Ruby Feinberg
*Tahnee Welch	 ...	Kitty
*Barret Oliver	 ...	David Linda Harrison	 ...	Susan
*Tyrone Power Jr. ...	Pillsbury
*Clint Howard	 ...	John Dexter
*Charles Lampkin ...	 Pops

Brian Dennehy held out returning as alien leader "Walter" but finally agreed to a 3-minute scene at the films end. He accepted no salary and appeared only as a favor to his cast mates from the first film.

==Soundtrack==
{{Infobox album  
| Name        = Cocoon: The Return
| Type        = Film score
| Artist      = James Horner
| Cover       =
| Recorded    = 1988
| Released    = 23 November 1988
| Genre       = Soundtrack
| Length      = 9 at 53:26}}
{{Album ratings Filmtracks
| rev1Score =     
}}
The music to Cocoon: The Return was composed and conducted by James Horner who had scored Cocoon.  The score mostly consisted of recycled themes and material from the first film.  The soundtrack was released on 23 November 1988 through Varèse Sarabande and features nine tracks of score at a running time of just over fifty-three minutes. 

# "Returning Home" (6:05)
# "Taking Bernie to the Beach" (4:31)
# "Joes Gift" (8:06)
# "Remembrances/The Break-In" (8:24)
# "Basketball Swing" (6:58)
# "Jacks Future" (2:44)
# "Growing Old" (1:55)
# "Good Friend" (3:16)
# "Rescue/The Ascension" (11:29)

== Reception ==
The film had a generally negative reception.   Roger Ebert of the Chicago Sun-Times gave the film two and a half out of four stars saying "Yes, the performances are wonderful, and, yes, its great to see these characters back again. But thats about it. For someone who has seen Cocoon, the sequel gives you the opportunity to see everybody saying goodbye for the second time."  Rotten Tomatoes gave at the film 36% positive reviews based on 36 reviews.

The film brought $25 million worldwide, far less than the first films $85 million worldwide gross.

==References==
 

== External links ==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 