...à la campagne
{{Infobox film
| name           = ...à la campagne 
| image          = ...à_la_campagne_poster.jpg
| image size     = 
| alt            = 
| caption        = French Film Poster
| director       = Manuel Poirier 
| producer       = Maurice Bernart
| writer         = Manuel Poirier
| narrator       =  Judith Henry Sergi López Jean-Jacques Vanier
| music          = Charlélie Couture
| cinematography = Nara Keo Kosal
| editing        = Hervé Schneid
| studio         = 
| distributor    = 
| released       =   
| runtime        = 108 minutes
| country        = France
| language       = French
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}

...à la campagne is a French film directed by Manuel Poirier, released 5 April 1995 in film|1995.

== Starring ==
* Benoît Régent : Benoît Judith Henry : Lila Sergi López : Pablo
* Jean-Jacques Vanier : Gaston
* Serge Riaboukine : Emile
* Élisabeth Commelin : Mylène
* Laure Duthilleul : Françoise
* Élisabeth Vitali : Cathy
* Céline Poirier : Céline

== External links ==
*   at the Internet Movie Database
*   at the site of Diaphana, the distributor of the film

 
 
 
 
 


 