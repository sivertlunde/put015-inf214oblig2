11 Harrowhouse
{{Infobox film
| name           = 11 Harrowhouse
| image          = 11 Harrowhouse Poster.jpg
| caption        = 11 Harrowhouse promotional movie poster
| director       = Aram Avakian
| producer       = Elliott Kastner Denis Holt (associate producer) 
| writer         = Jeffrey Bloom Charles Grodin (adaptation)  Gerald A. Browne (novel)       
| starring       = Candice Bergen James Mason Charles Grodin
| cinematography = Arthur Ibbetson 
| music          = Michael J. Lewis 
| editing        = Anne V. Coates 
| distributor    = 20th Century Fox
| released       =  
| runtime        = 94 min 
| country        = UK
| awards         = 
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}

11 Harrowhouse is a 1974 British film directed by Aram Avakian. It was adapted by Charles Grodin based upon the novel by Gerald A. Browne with the screenplay by Jeffrey Bloom. It stars Charles Grodin, Candice Bergen, James Mason, Trevor Howard and John Gielgud.

==Plot synopsis==

In England, a small-time diamond merchant (Grodin) is unexpectedly offered the chance to supervise the purchase and cutting of an extremely large diamond to be named for its wealthy owner (Howard). When the diamond is stolen from him, he is blackmailed into pulling off a major heist at "The System," located at 11 Harrowhouse, with the help of his beautiful and wealthy girlfriend (Bergen). The key figure in the theft, however, is the inside man (Mason) who works in the vault at The System. He is dying of cancer and wants to leave his family financially secure.

==Cast==
* Charles Grodin as Howard R. Chesser
* Candice Bergen as Maren Shirell
* James Mason as Charles D. Watts
* Trevor Howard as Clyde Massey
* John Gielgud as Meecham

==Reviews==

Time Magazine reviewed the film positively, and described the cast as "poised and stylish." 

==Relevance==

A prominent theme of the film revolves around how the primary world diamond producer controls the price of diamonds by creating artificial scarcity, much as oil producers do today.

==Versions==

The film has been screened in two versions in the past - both with and without a retrospective commentary from Grodins character, H.R. Chesser. Only the version without commentary seems to be widely available in published form, and neither version seems to have been screened to great extent on TV, though the original version with commentary holds up very well today. The film was released on LaserDisc by Fox Video in Widescreen Format and with the commentary intact.

==DVD Release==

On February 2, 2011 Shout! Factory is to release the film for the first time on DVD, but only on Region 1 (USA) Disc. 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 


 
 