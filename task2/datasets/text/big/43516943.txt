Affluenza (film)
 
{{Infobox film
| name           = Affluenza
| image          = Affluenza (film) poster.jpg
| alt            =
| caption        = Theatrical release poster
| director       = Kevin Asch
| producer       = Morris S. Levy
| screenplay     = Antonio Macia
| story          = Kevin Asch Antonio Macia
| starring       = Ben Rosenfield Gregg Sulkin Grant Gustin Nicola Peltz
| music          = MJ Mynarski
| cinematography = Timothy Gillis
| editing        = Suzanne Spangler
| studio         = Lookbook Films Mega Films
| distributor    = Filmbuff
| released       =  
| runtime        = 84 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Affluenza is a 2014 American drama film directed by Kevin Asch and written by Antonio Macia. It is loosely based on The Great Gatsby by F. Scott Fitzgerald.   It was the first leading role for actor Ben Rosenfield, and also starred Gregg Sulkin, Nicola Peltz and Grant Gustin.

==Plot==
In 2008, Fisher Miller moves in with his aunt Bunny and uncle Philip in Great Neck, New York, where he comes into contact with the privileged children of rich families.

==Cast==
* Ben Rosenfield as Fisher Miller 
* Gregg Sulkin as Dylan Carson 
* Nicola Peltz as Kate Miller 
* Grant Gustin as Todd Goodman 
* Steve Guttenberg as Philip Miller 
* Samantha Mathis as Bunny Miller 
* Valentina de Angelis as Jody 
* Danny Burstein as Ira Miller 
* Adriane Lenox as Professor Walker 
* Patrick Page as Jack Goodman 
* Carla Quevedo as Gail
* John Rothman as Rabbi Cohen 
* Roger Rees as Mr. Carson 
* Kevin Asch as Photographer  Joe Cross as Shaman 
* Taylor Gildersleeve as Beth 
* Barry Rohrssen as Detective

==Production==
In July 2012, The Hollywood Reporter announced that Grant Gustin had landed the lead role.  The main role eventually went to Ben Rosenfield. It premiered at the SVA Theater in New York City on July 9, 2014, and was released in the U.S. on July 11, 2014.   

==Reception==
Review aggregator website Rotten Tomatoes reported that 23% of thirteen surveyed critics gave the film a positive review; the average rating was 3.5/10.   Metacritic rated it 30 out of 100 based on twelve reviews.  Andrew Barker of Variety (magazine)|Variety described it as "an empty recasting of The Great Gatsby among Long Island rich kids."  Frank Scheck of The Hollywood Reporter wrote that the film "attempts to tackle weighty themes but ultimately feels as shallow as the lives of most of its principal characters."  Ben Kenigsberg of The New York Times called it a "millennial-chiding takeoff on The Great Gatsby" whose "vapid moralizing owes more to Bret Easton Ellis than to F. Scott Fitzgerald." 

==References==
 

==External Links==
*  
*  
*  

 
 
 
 
 
 
 
 