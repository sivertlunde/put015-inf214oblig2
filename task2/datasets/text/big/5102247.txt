I Trust You to Kill Me (film)
{{Infobox film
| name           = I Trust You to Kill Me
| image          = 
| caption        = 
| director       = Manu Boyer
| writer         = 
| starring       = David Beste Rayan Carman Greg Velasquez
| music          = 
| cinematography = 
| editing        = 
| distributor    = 3DD Entertainment First Independent Pictures
| released       =  
| runtime        = 105 minutes
| country        = United States
| language       = English
| budget         = 
| producer       = MRB Productions
| gross          = $12,854 {{cite web 
|url=http://boxofficemojo.com/movies/?id=itrustyoutokillme.htm |title=I Trust You to Kill Me (2006) |publisher=Box Office Mojo |accessdate=8 August 2011}} 
}}
I Trust You To Kill Me, released in January 2006, is a documentary film directed by Manu Boyer and featuring the California band Rocco DeLuca and the Burden and Canadian actor Kiefer Sutherland, who acted as the bands tour manager during their 2006 European tour.  The band is currently signed to Sutherlands independent record label Ironworks (record label)|Ironworks.

Originally broadcast in the UK on Sky One in February 2006, it has also been released on DVD in North America (NTSC) and broadcast on the American cable television channel VH1.  Other DVD release dates are unknown.

==References==
 

==External links==
*  

 
 
 
 


 