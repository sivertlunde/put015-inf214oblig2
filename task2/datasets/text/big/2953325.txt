Snow Cake
{{Infobox film
| name        = Snow Cake
| image       = Snowcake.jpg
| director    = Marc Evans
| producer   =  
| writer      = Angela Pell
| starring    =  
| cinematography  = Steve Cosens
| editing        = Mags Arnold
| distributor =   | Momentum Pictures   }}
| released    =  
| runtime     = 112 minutes
| country     =  
| language    = English
| music       = Broken Social Scene
| budget      = 
| gross       = $1,381,665 
}}
 2006 British/Canadian indie romantic romantic comedy drama film directed by Marc Evans and starring Alan Rickman, Sigourney Weaver, Carrie-Anne Moss, Emily Hampshire, and Callum Keith Rennie. It was released on September 8, 2006 in the UK.
 autistic Linda (Weaver), and neurotic British tourist Alex (Rickman) who has a change of heart after a fatal car accident involving himself and Lindas daughter Vivienne (Hampshire).

The movie was screened and discussed at Autism Cymru 2nd international conference in May 2006 as well as the Edinburgh International Film Festival, Tribeca Film Festival, Toronto International Film Festival, Seattle International Film Festival, among others. It was the opening night screening for the Berlin Film Festival as well. 

==Plot==
When the eccentric drifter Vivienne Freeman gets a ride from a reluctant recluse Alex Hughes (Alan Rickman), she is killed by a transport truck side ramming the car, while Alex only gets a nosebleed. Everybody agrees that it is not Alexs fault. He visits Linda to deliver Viviennes gifts and to provide support. She has been informed about her daughters death a few hours before Alexs visit, but does not show any signs of grief. However, she has a cleanliness mania which involves her constantly making sure everything in her home is neat, and prevents her from touching garbage bags. Her problem is finding someone who will put the garbage outside to be collected, as this was always something done by her daughter. Linda insists that Alex stay a few days so that he can do it for her. He agrees and also arranges Viviennes funeral.

During his stay he begins a relationship with one of Lindas neighbors, Maggie (Carrie-Anne Moss), who Linda mistakenly thinks is a prostitute. A local policeman warns Maggie of Alexs intentions because he has just served time for a mans murder. Maggie does not confront Alex about the matter, but instead waits until he brings the subject up himself. Alex reveals that he killed the man who caused his sons death in a car crash while he was on his way to meet Alex for the first time — Alex had only recently learned about his existence, the result of an affair a long time ago.

Linda dislikes Maggie to the point where she initially refuses her help. But after Alex leaves to see the mother of his son, she allows Maggie to come into her home and help her.

==Awards and critical recognition==

The film was nominated in 4 categories for the 27th Genie Awards in 2007:

* Best actress: Sigourney Weaver
* Best supporting actress: Emily Hampshire
* Best supporting actress: Carrie-Anne Moss (Won)
* Cinematography: Steve Cosens

Snow Cake won the Zip.ca Peoples Choice Award at the 2007 Kingston Canadian Film Festival.

Rotten Tomatoes has certified the film 66% fresh based on 59 reviews.

==Production notes==
The screenwriter, Angela Pell, wrote the role of Alex Hughes with Rickman in mind.  It was also Rickman who read the script and made sure Weaver (fellow Galaxy Quest costar) was contacted about the role of Linda.  Both Rickman and Weaver were runners-up at the Seattle International Film Festival for the respective prizes of Best Actor and Best Actress.

During the course of making the movie, Weaver researched the subject of autism and was coached by Ros Blackburn, a woman with the condition who is also an author and speaker about autism and Aspergers syndrome. Alan Rickman chose not to research the subject of autism in order to make his character have an impact/shock when facing Linda. 

==References==

{{reflist|2|refs=

 
{{cite web
| url = http://www.boxofficemojo.com/movies/?id=snowcake.htm
| title = Snow Cake
| publisher = Box Office Mojo
| date = 2007-06-14
| accessdate = 2011-08-24
}}
 

 
{{cite news
| last = Smith
| first = Neil
| date = 2006-08-16
| title = Weaver, still standing tall at 56
| publisher = BBC
| url = http://news.bbc.co.uk/2/hi/entertainment/4795787.stm
| accessdate = 2011-08-24
}}
 

 
{{cite video
| people = Marc Evans
| title = Making of Snow Cake
| medium = DVD
| publisher = BBC
| date = 2006
}}
 

 
{{cite video
| people = Alan Rickman
| title = Making of Snow Cake
| medium = DVD
| publisher = BBC
| date = 2006
}}
 

 
{{cite web
| publisher = IndieLondon
| title = Snow Cake - Review
| url = http://www.indielondon.co.uk/DVD-Review/snow-cake-review
| accessdate = 2011-08-24
}}
 

}}

==External links==
*  
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 