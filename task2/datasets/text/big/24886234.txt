Tehran Has No More Pomegranates!
{{multiple issues|
 
 
}}
{{Infobox film
| name           = Tehran Has No More Pomegranates!
| image          = 
| image_size     = 
| alt            = 
| caption        = 
| director       = Massoud Bakhshi
| producer       = Massoud Bakhshi
| writer         = Massoud Bakhshi
| starring       = 
| music          =
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 2006
| runtime        = 
| country        = Iran
| language       = 
| budget         = 
| gross          = 
}} 2006 Musical comedy Iranian Crystal Simorgh 25th Fajr International Film Festival|award-winning film produced, directed and written by Massoud Bakhshi.

==Title==
The film starts with a written text chosen from an old Persian book named Asar-al-Bilad describing Tehran in the 13th century.

*Tehran is a large village near the city of Rey, Iran|Rey, full of gardens and fruit trees.  Its inhabitants live in anthill-like underground holes.  The village’s several districts are constantly at war.  Tehranis’ main occupations are theft and crime, though the king pretends they are subject to him.  They grow excellent fruits, notably an excellent pomegranate, which is found only in Tehran.

- Asar-al-Bilad, 1241

==External links==
 

 
 


 