Gangster Squad
 
 
{{Infobox film
| name           = Gangster Squad
| image          = Gangster Squad Poster.jpg
| image_size     = 
| alt            = 
| caption        = Theatrical release poster
| director       = Ruben Fleischer Kevin McCormick Jon Silk Michael Tadross
| screenplay     = Will Beall
| based on       =  
| starring       = Josh Brolin Ryan Gosling Sean Penn Nick Nolte Emma Stone
| music          = Steve Jablonsky
| cinematography = Dion Beebe
| editing        = Alan Baumgarten James Herbert
| studio         = Village Roadshow Pictures Lin Pictures Warner Bros. Pictures Roadshow Entertainment   
| released       =   }}
| runtime        = 113 minutes  
| country        = United States
| language       = English
| budget         = $60 million    
| gross          = $105.2 million   
}}
Gangster Squad is a 2013 American crime film directed by Ruben Fleischer,  from a screenplay written by Will Beall and starring Josh Brolin, Ryan Gosling, Nick Nolte, Emma Stone, and Sean Penn.
 Gangster Squad" which was created when Clemence B. Horrall was the LAPDs Chief of Police.  A similar theme is the basis of a 1996 film, Mulholland Falls, and a 2013 television miniseries, Mob City.

== Plot == LAPD Detective Bill Parker. Parker believes that more drastic measures need to be taken against men like Cohen, and tasks OMara to begin waging a guerrilla campaign against the mobsters. He tells OMara (a former OSS commando during World War II)  to use his special operations training, learned at Camp X during World War II, and to select a small team that will work without badges or official support from the police.

OMaras pregnant wife Connie suggests choosing unorthodox veterans like himself, as young high-performers would likely already be on Cohens payroll. With Connies help, OMara selects a small squad of cops: black street officer Coleman Harris, wiretap expert Conwell Keeler, gunslinger and sharpshooter Max Kennard, and Kennards Hispanic partner Navidad "Christmas" Ramirez. OMara also attempts to recruit his partner Sergeant Jerry Wooters, but Wooters has become lazy and complacent in his job and refuses. Wooters keeps in touch with childhood friend Jack Whalen, who provides him with information on Cohen. Wooters also meets and begins a secret relationship with Cohens girlfriend Grace Faraday.

The squads first mission is to bust up an illegal Cohen casino in Burbank, California, but things quickly go bad as OMara and Harris are captured by corrupt Burbank police who were guarding the casino. Wooters has a change of heart after witnessing the death of a young boy he had been helping out and attempts to shoot Cohen. Whalen stops him and tells him that OMara is going to be turned over to Cohen, prompting Wooters to rescue the men from the Burbank jail. Deciding that they need more information on Cohens operations, Wooters and Keeler break into Cohens house and place an illegal wiretap inside his TV. The men are seen sneaking out by Grace, who agrees to keep their secret.
 The Gangster Squad", and Cohen pushes his men to find out who they are. Keeler deduces that Cohen is building a large wire gambling business somewhere in town, and warns OMara that if they dont take it out before it becomes operational Cohen will become too big for even them to stop. Keeler uses wire transmissions to locate the building, and the squad wipes it out. An enraged Cohen realizes that the Gangster Squad must be honest cops when he discovers that none of his money was stolen.

Cohen suspects that his house is bugged and begins searching for the tap. Grace overhears Cohen and fears that he knows about her relationship with Wooters. With the help of a maid, Grace escapes Cohens house and meets Wooters, who takes her to Whalen and tasks him with getting her out of town. Cohen finds the bug and begins feeding false information to Keeler. Cohen lures the Gangster Squad into a trap in Chinatown, Los Angeles|Chinatown, but Wooters arrives in time to alert the men to the trap. While the men are distracted in Chinatown, Cohen hits several targets himself. Cohens bodyguard Karl Lockwood finds Keelers listening post and kills him while Cohen goes to Whalens looking for Grace. Cohen murders Whalen in front of Grace, who hides from him. OMaras house is hit by a drive-by shooting, the stress of which causes Connie to give birth to their son in their bathtub.  

Grace agrees to testify against Cohen for the murder of Whalen, and OMara uses her testimony to get a warrant for Cohens arrest. The squad arrives at Cohens hotel to arrest him and an intense firefight breaks out. Wooters and Kennard are wounded, while Cohen and Lockwood escape. OMara pursues them down the block, assisted by a mortally wounded Kennard and his sharpshooting skill. Kennard, with Ramirez help, shoots Lockwood just before he dies and OMara and Cohen engage in a brutal fistfight that ends with OMara beating Cohen to his knees. As a crowd gathers, a bloodied OMara walks away and Cohen is arrested for Whalens murder.  
 25 to life at Alcatraz, where he is welcomed violently by Whalens friends. Grace and Wooters stay together and he stays on the force, while Ramirez and Harris become partners on the beat. Ramirez is shown patrolling with Kennards signature Colt Single Action Army on his hip. OMara quits to live a quiet life in Los Angeles with Connie and their son.

== Cast ==
* Josh Brolin as Sergeant John OMara
* Ryan Gosling as Sergeant Jerry Wooters 
* Sean Penn as Mickey Cohen Chief Bill Parker
* Emma Stone as Grace Faraday
* Anthony Mackie as Officer Coleman Harris
* Giovanni Ribisi as Officer Conwell Keeler
* Michael Peña as Officer Navidad "Christmas" Ramirez
* Robert Patrick as Officer Max Kennard
* Mireille Enos as Connie OMara
* Sullivan Stapleton as Jack Whalen
* Holt McCallany as Karl Lockwood
* Josh Pence as Daryl Gates
* Austin Abrams as Pete
* Jon Polito as Jack Dragna 
* James Hébert as Mitch Racine
* John Aylward as Judge Carter
* Troy Garity as Wrevock
* James Carpinello as Johnny Stompanato
* Frank Grillo as Tommy Russo
* Jonny Coyne as Grimes Jack McGee as Lieutenant Quincannon
 

== Production ==
=== Filming === Los Angeles Tower Theater, Park Plaza Hotel, MacArthur Park and Cliftons Cafeteria were used as filming locations. Three days of production were spent in Chinatown, Los Angeles. The film was shot digitally using cameras with anamorphic lenses.  Filming wrapped on December 15, 2011. 

=== Re-shoots === trailer for mass shooting at a theater in Aurora, Colorado on July 20, the trailer was pulled from most theaters running before films and airing on television, and removed from Apple Inc.|Apples trailer site and YouTube due to a scene in which characters shoot submachine guns at moviegoers through the screen of Graumans Chinese Theatre.  

It was later reported that the theater scene from the film would be either removed, or placed in a different setting since it is a crucial part of the film, and the film would undergo additional re-shoots of several scenes to accommodate these changes. This resulted in the release of Gangster Squad being delayed.  About a week after the shootings in Aurora, Warner officially confirmed that the film would be released on January 11, 2013, bumped from the original September 7, 2012 release date.     Two weeks later, on August 22, the cast reunited in Los Angeles to completely re-shoot the main action sequence of the film. The new scene was set in Chinatown where the Gangster Squad comes into open conflict with the gangsters as they strike back. Josh Brolin said he was not sad the original movie theatre scene was cut, and admitted that this new version is just as violent.  

==Release==
=== Box office ===
Gangster Squad grossed $46 million domestically and $59.2 million in other countries, for a total gross of $105.2 million, against a budget of $60 million.   

The film grossed $17 million in its opening weekend, finishing  third at the box office, behind Zero Dark Thirty ($24.4 million) and A Haunted House ($18.1 million). 

=== Reception ===
Gangster Squad received mixed reviews from critics. On Rotten Tomatoes, the film has a rating of 32%, based on 193 reviews, with an average rating of 5/10. The sites consensus reads, "Though its stylish and features a talented cast, Gangster Squad suffers from lackluster writing, underdeveloped characters and an excessive amount of violence".  Another review aggregator, Metacritic, which assigns a rating based off top reviews from mainstream critics, calculated a score of 40 out of 100, based on 38 critics, indicating "mixed or average reviews".  

Mark Kermode on his BBC Radio 5 Live show with Simon Mayo, compared Gangster Squad unfavourably with the Rockstar video game L.A Noire which he thought had better character development than Gangster Squad.

The reviewers of Spill.com gave it a "Rental, " praising the stylish approach but criticizing the dialogue, Emma Stones under-developed "damsel-in-distress" character, and Sean Penns laughable makeup. Cyrus suggests that the romantic subplot between Sergeant Jerry Wooters and Grace Faraday is "a story you would care nothing about if it wasnt Ryan Gosling and Emma Stone". 
IGN editor Chris Tilly wrote "Gangster Squad looks great but frustrates because with the talent involved, it had the potential to be so much more, " thus rating the film 6.3 out of 10. 

Writing for Roger Ebert of the Chicago Sun-Times, Jeff Shannon gives the film 2 stars out of 4. He believes that director Fleischer, better known for his comedic work, is out of his element, and barely suppressing his urge to spoof the genre. He notes that Stone and Gosling had chemistry in Crazy, Stupid, Love but that here it "curdles into lukewarm mush". He further criticizes the stock characters, and the generally uneven tone of the film, but praises the action highlights such as the car chase, and occasional flashes of brilliance in the performance of Sean Penn. In conclusion he describes Christian Slaters 1991 film Mobsters as still a marginally better film than Gangster Squad. 

Owen Gleiberman of Entertainment Weekly gave the movie a "C" and wrote, When Penn is on screen, Gangster Squad is far from great, but it does crackle with a certain gutter fascination. The trouble is that the director, Ruben Fleischer (the Music video|music-video veteran who made Zombieland), lures us into wanting to see a thriller that runs on intrigue, but OMara and his team of cops never come up with a devious or even very coherent plan. They beat the hell out of folks, bomb storefronts, and race through the boulevards in their cool 40s cars. And the movie, as criminal drama, goes nowhere.  

Richard Roeper gave the film a B+, saying “Gangster Squad is a highly stylized, pulp-fiction period piece based on true events” and noted the strong performances.

=== Home media ===

Gangster Squad was released on DVD and Blu-ray on April 23, 2013. The Blu-ray includes directors commentary from Ruben Fleischer and several segments about the real life men and stories of the Gangster Squad and Mickey Cohen. 

== Depictions of reality ==
The film purports to be inspired by a true story, though much of the story is fabricated.

* The film portrays Cohen organizing the murder of opponent Jack Dragna, whereas in reality Dragna died of a heart attack in 1956. William Parker is portrayed as a no-nonsense Christian in the film, whereas in reality he was far more controversial. Parker was also only 45 years old in 1949, and not in his 70s like Nolte (Parker didnt live until his 70s; he died at age 61). Gangster Squad. The Squad was created by Chief Clemence B. Horrall in 1946.
* The film concludes with Cohen being arrested in 1949 for murder and sent to Alcatraz. In reality, he was imprisoned in 1951 and again in 1961 for tax evasion. He was, however, attacked with a lead pipe while in prison.
* Slapsy Maxies, a nightclub prominently featured in the story, was a real establishment owned by "Slapsy" Maxie Rosenbloom, a former light-heavyweight boxing champion.
* While it is possible Cohen murdered Jack Whalen in real life, it was not at Whalens home as depicted in the film. Whalen was shot in 1959 while at dinner with Cohen and three of his associates, although Cohen was not accused or convicted of the murder himself.
* Cohens bodyguard Johnny Stompanato was not shot like depicted in the film, but instead lived until 1958, when he was stabbed by Cheryl Crane, the daughter of his girlfriend, Lana Turner.
* The real life Max Kennard was the first of the Squad to die, however it was in a 1952 car crash after he had retired, and not shot in the line of duty like in the film.
* In the film, Conwell Keeler is the first member of the Squad to be killed. In real life, he outlived all other members of the Gangster Squad, dying of a stroke in 2012.
* John OMara enjoyed retirement alongside his wife Connie and their daughter (unlike the son they have in the film) until OMaras death in 2003 at the age of 86.

== References ==
 

== External links ==
*  
*  
*  
*  
*  
*  
* http://www.crimeanalystblog.net/2013/01/the-real-story-of-lapds-gangster-squad.html. Retrieved June 23, 2013
* http://www.latimes.com/news/local/la-me-gangster-sg,0,5506273.storygallery. Retrieved June 23, 2013

 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 