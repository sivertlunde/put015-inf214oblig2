Idi Katha Kaadu
{{Infobox film
| name = Idi Katha Kaadu
| image = Idi Katha Kaadu.jpg
| caption = Official DVD Box Cover
| director = Kailasam Balachander|K. Balachander   
| producer =
| writer = Kailasam Balachander|K. Balachander 
| starring = Kamal Haasan Chiranjeevi Jayasudha Sarath Babu Ramaprabha
| music = M. S. Viswanathan 
| cinematography = LokSingh 
| editing =
| distributor =
| released =   
| runtime =
| country = India
| language = Telugu
| budget =
| gross =
| preceded_by =
| followed_by =
}}
 Sujatha in the original. Kamal Haasan starred in the same role in both languages. This film also had Chiranjeevi as the villainous husband of Jayasudha (Rajinikanth starred as the husband in Tamil). The film was shot in Black and white.   

== Plot ==
The movie revolves around the trials and tribulations of Suhasini (Jayasudha). Suhasini is a happy-go-lucky girl head-over-heels in love with her boyfriend Bharani (Sarath Babu). Her life changes when her father gets transferred to Mumbai (then Bombay). Her love life falls apart as Bharani doesn’t respond to any of her letters. In addition, her father becomes seriously ill. Her father’s office colleague, Subanakar (Chiranjeevi), becomes a great source of strength for her in these tough times. Soon, he asks for her hand in marriage. She accepts gratefully, and confesses that she had a boyfriend, who has seemingly forgotten her.

However, she soon realizes the truth about Subanakar. He is a sadistic and jealous husband, who tortures her no end and, not unsurprisingly, she opts for a divorce. As a divorced woman, with an infant in her hands, she lands in Chennai (then Madras) to take up a new job and start a new life.

Her life takes a turn for the better in Chennai as she has a very supportive friend-group in her office, particularly a widower, Johnny (Kamal Hassan). Johnny, a talented ventriloquist who “talks” through his puppet, Junior. He falls in love with Suhasini, but is unable to muster up the courage to tell her about it.

In an interesting cinematic twist, her ex-husband’s mother discovers her presence in the city and takes up a job as a maid in her house.

Suhasini also stumbles upon Bharani in Chennai and discovers that her letters to him never reached as they were intercepted by his mentally-challenged sister. Soon, Suhasini renews her relationship with Bharani and life seems to be looking up for her. But the ghosts of the past continue to haunt her. Subanakar comes to Chennai in the role of her boss – a contrite and repentant Subanakar, who now wants to re-marry Suhasini and redress the wrongs he had done. The situation becomes piquant with 3 men vying for Suhasini – her ex-love, her ex-husband and a silent lover lurking on the sidelines. Which way will Suhasini go? K. Balachander takes the movie to its logical, yet completely radical and unexpected, end.

==Cast==
* Kamal Hassan as Johnny
* Jayasudha	as Suhasini
* Chiranjeevi as Subanakar
* Sarath Babu as Bharani
* J. V. Ramana Murthi		
* Ramaprabha		
* Saritha  as Gayatri

== Soundtrack ==

{{Infobox album  
| Name        = Idi Katha Kaadu
| Type        = soundtrack
| Artist      = M. S. Viswanathan
| Cover       =
| Caption     =
| Released    =    
| Recorded    =
| Genre       =
| Length      = Telugu
| Label       =
| Producer    =
| Reviews     =
| Compiler    =
| Misc        =
}}

* Gaali Kadupu Ledu Kadali Kanthu Ledu (Lyrics:  )
* Idi Katha Kadu
* Jola Paata Paadi Uyyala Oopana (Lyrics:  )
* Junior Junior Atu Itu Kaani Hrudayam Toni (Lyrics:   and Ramola)
* Sarigamalu Galagalalu (Lyrics: Acharya Atreya; Singer: S.P. Balasubramanyam and P. Susheela)
* Thakadhimi Thaka (Lyrics: Acharya Atreya; Singer: S. P. Balasubramanyam)

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 