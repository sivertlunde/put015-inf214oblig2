Varadhanayaka
 
 

{{Infobox film
| name = Varadhanayaka
| image = Varadhanayaka.jpg
| caption = Poster
| director = Ayyappa P. Sharma
| producer = Shankar Gowda
| writer = Srivas 
| starring = Sudeep Chiranjeevi Sarja Sameera Reddy Nikeesha Patel P. Ravi Shankar
| music = Arjun Janya
| cinematography = Rajesh Katta
| editing = Ishwar
| background score = 
| distributor =  Shankar Productions
| released =  
    
| runtime = 
| country = India
| language = Kannada
| budget =  
| gross =    +   {Satellite Rights}
}} Kannada action film starring Chiranjeevi Sarja and Nikeesha Patel in the lead roles. Actor Sudeep plays an important role of a police officer. Sameera Reddy is his pair in the film. This film marks the Kannada debut of Sameera Reddy, an established Bollywood and Kollywood actress. The film is directed by Ayyappa P. Sharma. Arjun Janya is the music director of the film. Shankare Gowda has produced the venture under Shankar Productions banner.It is also dubbed in Hindi as Ek Tha Nayak. 
The film is a remake of Telugu film Lakshyam (2007 film)|Lakshyam which starred Tottempudi Gopichand|Gopichand, Jagapathi Babu and Anushka Shetty. 

== Cast ==
* Sudeep
* Chiranjeevi Sarja
* Sameera Reddy
* Nikeesha Patel Dr Ambareesh ... (Guest Appearance) Sumalatha Ambareesh ... (Guest Appearance)
* P. Ravi Shankar
* Rockline Venkatesh ... (Guest Appearance)
* Mukhyamantri Chandru
* Jai Jagadish
* Shobaraj
* Sharath Lohitashwa
* J. Karthik Sharan
* Chitra Shenoy
* Padmaja Rao
* Bullet Prakash
* Dharma
* Satyajit
* Rajiv and others

==Production==

===Casting=== Gopichand in Sai Kumar and P. Ravi Shankar were also roped in to play the supporting characters. It was also announced that Arjun Sarja would be doing a cameo appearance in the film.

===Filming=== shooting to regularly commence from June 2011.  The filming began without even casting the leading ladies for the film.

==Release==
Initially the film was plan to release on 23, November 2012.  The film released in India on 25 January 2013 to mixed reviews.   and also Kannada Sangha in Canada released this film in Canada on 28 April 2013 

==Boxoffice==
Varadhanayaka box office collection is grossed more than  . The satellite rights of the film was sold to Suvarna Channel for  . 

==Soundtrack==
{{Infobox album  
| Name        = Varadhanayaka
| Type        = Soundtrack
| Artist      = Arjun Janya
| Cover       = 
| Alt         = 
| Released    =  
| Recorded    = Feature film soundtrack
| Length      = 
| Label       = 
}}
Arjun Janya has composed 5 songs to the lyrics of Kaviraj (Lyricist)|Kaviraj. 
{{Track listing
| extra_column	 = Singer(s)
| title1	= Baite Baite
| extra1        = Arjun Janya, Anuradha Bhat
| title2        = Yeno Kane
| extra2        = Vijay , Priya Himesh
| title3        = Ondhsari
| extra3 	= Arjun Janya, Anuradha Bhat
| title4        = Morningu
| extra4        = Chandan Shetty, Suma Shastry
| title5        = Theme
| extra5        = Ravishankar
}}

==Awards & Nominations==
{{Infobox actor awards name = Varadanayaka (2013) awards = 1 nominations = 2 award1 = South Indian International Movie Awards award1W = 1 award1N = 2
}}

3rd South Indian International Movie Awards :-
* Best Actor in a Negative Role - Nominated - P. Ravi Shankar  
* Best Music Director - Nominated - Arjun Janya  
* Best Playback Singer (Male) - Won - Arjun Janya for the song "Baite Baite"    

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 

 