BearCity 2: The Proposal
{{Infobox film
| name = BearCity 2: The Proposal
| image =
| caption =
| director = Doug Langway
| producer =
| writer = Doug Langway
| starring =
| music = Mark Andersen Peter Calandra
| cinematography = Michael Hauer
| editing = Doug Langway
| distributor =
| released =  
| runtime =
| country = United States
| language = English
| budget =
}} bear community) comedy-drama film written and directed by Doug Langway. "Behind the Scenes on BearCity 2". The Advocate, July 25, 2012.  It is a sequel to his 2010 film BearCity. 

In addition to the major characters from the original BearCity, additions to the cast include Richard Riehle and Susie Mosher as Tylers parents, and Kathy Najimy as Brents mother. 

==Plot== Joe Conti), and the gang embarks on a road trip to Provincetown, Massachusetts for Bear Week.

==Cast== Joe Conti as Tyler
*Jason Stuart as Scott
*Stephen Guarino as Brent Brian Keane as Fred
*Gerald McCullouch as Roger
*Richard Riehle as Gabe
*Kathy Najimy as Rose
*Kevin Smith as Himself
*Frank DeCaro as Himself

==Release== Los Angeles Gay and Lesbian Film Festival and Philadelphia QFest in July 2012, before going into general release on September 28. It was subsequently released on DVD and Blu-ray on December 5, 2012 by SharpLeft Studios, and was made available as a digital download on January 11, 2013.

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 


 
 