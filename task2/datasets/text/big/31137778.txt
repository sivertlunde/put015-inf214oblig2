The White Sheik (1928 film)
 
 
{{Infobox film
| name           = The White Sheik
| image          = 
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Harley Knoles
| producer       = 
| writer         = Violet E. Powell   Mary Murillo
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Lillian Hall-Davis   Warwick Ward, Jameson Thomas   Julie Suedo
| music          = 
| cinematography = René Guissart (director)|René Guissart
| editing        = 
| studio         = British International Pictures
| distributor    = Wardour Films
| released       = 17 January 1928
| runtime        = 95 minutes
| country        = United Kingdom
| language       = 
}} British silent silent adventure film directed by Harley Knoles and starring Lillian Hall-Davis, Jameson Thomas and Warwick Ward.  It was based on the novel Kings Mate by Rosita Forbes.

==Cast==
* Lillian Hall-Davis - Rosemary Tregarthen
* Jameson Thomas - Westwyn
* Warwick Ward - Martengo
* Clifford McLaglen - Manheebe
* Gibb McLaughlin - Jock
* Forrester Harvey - Pat
* Julie Suedo - Zarita

==Release==
For its December 1929 New York City premiere at the Little Carnegie Playhouse  it was accompanied by the Hal Roach Studios comedy Feed ’em and Weep  and the Universum Film AG documentary short Strange Prayers. 

==Reception==
Mordaunt Hall called the film "amateurish" and "boring", with characters he thought "as silly a lot as have ever darted to and fro on the screen." 

==References==
{{reflist|refs=
   
}}

==External links==
* 

 

 
 
 
 
 
 
 
 


 
 