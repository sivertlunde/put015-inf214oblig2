A Nightmare on Elm Street 5: The Dream Child
 
{{Infobox film
| name             = A Nightmare on Elm Street 5:  The Dream Child
| image            = nightmare5.jpg
| caption          = Theatrical release poster Stephen Hopkins
| screenplay       = Leslie Bohem
| story            = John Skipp Craig Spector Leslie Bohem
| based on         =  
| starring         = Robert Englund Lisa Wilcox 
| producer         = Robert Shaye Rupert Harvey Jay Ferguson Peter Levy
| editing          = Brent A. Schoenfeld Chuck Weiss
| studio           = Heron Communications Smart Egg Pictures
| distributor      = New Line Cinema
| released         =  
| runtime          = 90 minutes
| country          = United States
| language         = English
| budget           = $8 million  (estimated) 
| gross            = $22.1 million  (domestic) 
}}

A Nightmare on Elm Street 5: The Dream Child is a 1989 American   and is followed by  .
 filter lighting technique is used in most of the scenes. The films main titles do not display the "5" that was used in all of the promotional material, TV spots, trailers, and merchandise. The main titles simply say "A Nightmare on Elm Street: The Dream Child". Released on August 15th, 1989, the film grossed over $22.2 million at domestic box office to a generally negative critical reception.

==Plot==
Taking place almost a year after  , Alice and Dan have now started dating and there is no sign of Freddy Krueger. One day, while in the shower, she sees herself at a strange asylum. As she walks she finds that she is dressed in a nuns habit with a nametag saying Amanda Krueger. She is then attacked by patients at the hospital but wakes up before anything happens. The next day Alice is graduating from high school alongside her new friends consisting of Greta, an aspiring supermodel, Mark, a comic book geek, and Yvonne, a candy striper who is also a swimmer. She only confides her nightmare to Dan, after he tells her about a trip to Europe. He tells her she is all in control of her dreams, and she goes to work.

While on her way to work, Alice finds herself back at the asylum, where she witnesses Amanda giving birth. Amanda tries to collect the baby before it escapes but it gets out of the operating room and Alice follows it into the same church that she had defeated Freddy in the previous film. The baby quickly finds Freddys remains and grows into an adult, fully resurrecting before Alice who hints that hes found the key to coming back. Amanda approaches to help stop Freddys return, but Freddy closes the door on her. Alice emerges at the diner she works in, but four hours late. She phones Dan who immediately rushes to join her. En route though, Dan falls asleep and Freddy comes after him. After turning Dan into a very literal speed demon with a motorcycle, he causes him to run full speed into a tanker truck. Alice hears the crash and rushes out to see Dans body, which then comes to life, causing Alice to faint. At the hospital, being treated for shock, Alice is not formed that she is pregnant with Dans child. She also meets a boy named Jacob who visits her in the night, but Yvonne tells her that there is no childrens ward at the hospital, nor were there any children on her floor. She tells her friends about Freddys legend, Yvonne is completely dismissive while the others, still skeptical say that whatever is after Alice will have to go through them first (which is what Alice is afraid of).

Greta falls asleep at a dinner party, and fully rants on her mothers obsessive and smothering behavior before Freddy arrives after realizing hes black he becomes a waiter and kills Greta by forcing her to eat herself, then choke to death in front of the real dinner party. Mark, who starts to believe the stories falls asleep and is lured into Freddys house. But Alice draws herself into his dream and saves him before seeing Jacob again; she realizes that Jacob is her unborn baby but awakens before she can talk to him. She requests an early ultrasound and she learns that Freddy is feeding the spirits of her friends to her baby and using his dreams to kill them as well. The argument over Alices sanity causes a rift between her and Yvonne, but Yvonne falls asleep at the pool and is attacked by Freddy. Alice goes to find Amanda in the asylum, but she is drawn off the search by Freddy holding Yvonne hostage. Meanwhile Mark, standing vigil over Alice finds a comic book depicting the movies events up to that point. He realizes hes fallen asleep and is drawn into a nightmare. After a game of cat and mouse, Marks love for Greta helps him unlock his dream power; the comic hero version of himself the Phantom Prowler, which he uses to attack Freddy. But Freddy turns the tables on Mark and slashes him to pieces by turning him into a paper doll version of himself and shredding him apart.

After saving Yvonne from Freddy and discovering Marks fate, Alice pleads for Yvonne to find Amanda at the old asylum while she goes to sleep in search of Freddy and her son. After being slowed by an M.C. Escher type labyrinth, Alice eventually catches up to Freddy and Jacob and realizes that Freddy has been hiding inside of her since his original defeat. She draws him out, but the attack severely weakens her. Meanwhile, Yvonne finds Amandas remains and frees her from her earthly prison. She appears and informs Jacob to use Freddys own power against him and save Alice. He does so, causing the spirits of Dan, Mark and Greta to literally rip Freddys baby form out of him. Jacob returns to his infant form and is absorbed into Alice is now formed, while Freddy is absorbed into Amanda. As she leaves, Freddy tries to break himself free of Amanda, but she closes the church doors on him, sealing him away for good.

Months later, Alice has given birth to Jacob, who she has named "Jacob Daniel" and is sleeping peacefully now. She meets with her father and Yvonne in the park for a play date. All appears to be back to normal, but as the scene pans out it focuses on a group of ghostly school children humming Freddys familiar rhyme.

==Cast==
*Robert Englund as Freddy Krueger
*Lisa Wilcox as Alice Johnson
*Beatrice Boepple as Amanda Krueger Jacob Johnson Yvonne Miller Dan Jordan Greta Gibson
*Nicholas Mele as Dennis Johnson Mark Grey
*Valorie Armstrong as Doris Jordan
*Burr DeBenning as Mr. Jordan
*Clarence Felder as Edmund A. Gray

==Reception==

===Box office===
A Nightmare on Elm Street 5: The Dream Child was released on August 11, 1989 on 1,902 theatres in North America. On the first weekend, the film grossed $8,115,176, falling behind Parenthood (film)|Parenthood ($9,672,350) and James Cameron’s The Abyss ($9,319,797).    The film ranked No.8 at the second weekend box office with a box office performance of $3,584,320, and it dropped out from the Top 10 list ranked as No.11 and No.14 on the third and the fourth weekend. Overall, the film eventually grossed $22,168,359 at domestic box office. Though this makes it  the second lowest grossing Nightmare on Elm Street film, the box office result equals 4 times its estimated budget, making it a financial success. The film ranked No.43 of the Top 50 highest domestic grossing films released in 1989. It is also the highest grossing horror-slasher film of the year.

===Reception===
A Nightmare on Elm Street 5: The Dream Child received generally negative reviews from critics. The review aggregator website Rotten Tomatoes reported a 31% approval rating with an average rating of 4.2/10 based on 29 reviews. The film was widely criticized for its plot and for taking the series in a more comedic direction. 

However, the film was acclaimed for its more gothic and imaginative dream sequences, special effects and Robert Englunds portrayal of Freddy Krueger. Lisa Wilcox is praised for her darker, more emotional and touching performance. Robert Englund has said that it is his least favorite film in the series next to Freddys Revenge and Freddys Dead. 

In an interview posted on Nightmare on Elm Street Companion, Lisa Wilcox, praised Nightmare 5s gothic tone, but also pointed out that several scenes and parts of the films plot perhaps are too sensitive for the audiences: “...NOES 5 was much darker, literally and figuratively. The lighting was darker and the subject matters were heavier too... Like abortion, teen motherhood, drinking and driving, bulimia, anorexia... Perhaps it was too much for a NOES film to handle. Thus, the film hit nerves too close to society home and therefore not as entertaining…” and she also gave the film a positive review in the interview, “…5 brought up interesting issues regarding teen pregnancy and rights of a mother."

===Accolades===
;1990 Fantasporto Awards Stephen Hopkins (Won)
* International Fantasy Film Award Best Film – Stephen Hopkins (Nomination)
 
;10th Golden Raspberry Awards Razzie Award for Worst Original Song – Bruce Dickinson for "Bring Your Daughter... to the Slaughter" (Won)
* Razzie Award for Worst Original Song – Kool Moe Dee for "Greatest Hits (Kool Moe Dee album)|Lets Go" (Nominated)

;1990 Young Artist Awards
* Best Young Actor in a Supporting Role – Whit Hertford (Won)

==Deleted scenes==
The graduation sequence was considerably cut down, which included Alices father giving her the camera. As a result there are a number of minor continuity errors such as Alice holding airplane tickets moments before Dan gives them to her as a surprise gift.

An unrated version of the film was originally released on VHS and Laserdisc. This version contained longer, more graphic versions of Dan, Greta, and Marks death scenes.  In Dans scene, cables can be seen sliding under the skin of Dans arm, a large piece of the bike pierces his leg, and the skin on Dans head is much more graphically torn off while he screams in pain. In Gretas scene, Freddy slices open a doll that begins to bleed and Greta is shown to have a gaping wound in her stomach, from which Freddy starts to feed to her. In  .

==Soundtrack== heavy metal, rap and hip-hop.

# Bruce Dickinson – "Bring Your Daughter... to the Slaughter"
# Romeos Daughter – "Heaven in the Back Seat"
# W.A.S.P. – "Savage"
# Mammoth – "Cant Take the Hurt"
# Slave Raider – "What Do You Know About Rock n Roll"
# Whodini – "Any Way I Gotta Swing It"
# Samantha Fox – "Now I Lay Me Down"
# Kool Moe Dee – "Lets Go"
# Doctor Ice – "Word Up Doc!"
# Schoolly D – "Livin in the Jungle"
 heavy metal band Iron Maiden, wrote and performed the song "Bring Your Daughter... to the Slaughter" for this movies soundtrack. The song, later re-recorded by the band Iron Maiden itself, went on to be a #1 single in the UK.

==See also==
*List of ghost films

==References==
 

==External links==
 
 
* 
* 
* 
* 
*  at Nightmare on Elm Street Companion

 
 

 
 
 
 
 
 
 
 
 