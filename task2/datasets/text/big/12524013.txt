Yona Yona Penguin
{{Infobox film
| name           = Yona Yona Penguin
| image          = Yona yona penguin.jpg
| caption        = 
| director       = Rintaro
| producer       = Jungo Maruta Denis Friedman Tony S. Izumi
| writer         = Tomoko Konparu
| starring       = 
| music          = Toshiyuki Honda
| cinematography = 
| editing        =  Madhouse Dynamo Pictures Def2shoot
| distributor    = Shochiku
| released       =  
| runtime        = 85 minutes
| country        = Japan France
| language       = Japanese French
| budget         = 
}}
 animated film Madhouse and CGI film. Famed Madhouse staff director Rintaro, known for Galaxy Express 999 and Metropolis (anime)|Metropolis, helmed the project, while French production company Denis Friedman Productions collaborated and helped fund the film.

==Visual style==
Madhouse has announced this film as being a "3D anime", or a film which brings an anime sensibility and design structure into the 3D CGI world. Unlike in other Japanese CG productions, such as Final Fantasy VII Advent Children, photo-realism is not emphasised. Rather, attention has been focused on giving the feel of a traditionally-made anime to a completely computerized production. As Madhouses expertise has long been in the creation of traditional 2D animation, much of the actual 3D animation was done by the French animation studio Def2shoot, the Thai studio Imagimax, and the Japanese studio Dynamo Pictures, with Madhouse providing direction and storyboards.

==Release== former Soviet Next Entertainment World (NEW). 

==See also==
* List of animated feature films
* List of computer-animated films

==References==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 