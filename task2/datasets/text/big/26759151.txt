Neramu Siksha
 
{{Infobox film
| name           = Neramu Siksha
| image          = Neramu Siksha.jpg
| image_size     =
| caption        =
| director       = K. Viswanath
| producer       = M. Balaiah Samudrala and Modukuri Johnson (dialogues) K. Viswanath (screenplay)
| narrator       =
| starring       = Ghattamaneni Krishna Bharathi Vishnuvardhan M. Balaiah Kaikala Satyanarayana Kanta Rao
| music          = Saluri Rajeswara Rao
| cinematography =
| editing        = S. P. S. Veerappa
| studio         =
| distributor    =
| released       = July 18, 1973
| runtime        =
| country        = India
| language       = Telugu
| budget         =
}}
 Tamil and Hindi languages. 

==Plot==
Krishna is son of a rich businessman couple (Kanta Rao and Pandari Bai). Bharathi is also a daughter of another rich man (Rao Gopal Rao). Krishna is grown up without knowing or facing any problems and gets a new car. While driving in competition with his friend Satyam (Satyanarayana), he accidentally kills Balaiahs brother and blinds Balaiah. The consequences of this event is the story of the film.

==Credits==
===Cast===
* Ghattamaneni Krishna
* Bharathi Vishnuvardhan
* M. Balaiah
* Kaikala Satyanarayana    as Satyam
* Kanta Rao	  as Heros Father, Rajasekharam Krishna Kumari
* Murali Mohan
* Pandari Bai   as Heros Mother
* Rao Gopal Rao   as  Narayana
* Arja Janardhana Rao

===Crew===
* Screenplay and Direction: K. Vishwanath
* Producers: Alaparti Suryanarayana and Mannava Venkata Rao
* Story: M. Balaiah
* Dialogues: Samudrala Ramanujacharya and Modukuri Johnson
* Production Company: Amrutha Films
* Director of Photography: G. V. R. Yoganand and B. Ramachandraiah
* Film Editing: S. P. S. Veerappa
* Choreography: Pasumarthi Krishna Murthy and Vempati Satyam
* Lyrics: Devulapalli Krishnasastri, Samudrala Ramanujacharya, C. Narayana Reddy, Kosaraju, Dasaradhi, P. Ganapati Sastry
* Original Music: Saluri Rajeswara Rao
* Assistant Composer: Puhalendi
* Playback Singers: S. P. Balasubramaniam, G. Anand, S. Janaki and P. Susheela

==Soundtrack==
* Chesina Papam Needi Chitikina Bratukinkokaridi (Lyrics: Devulapalli Krishnasastri)
* Daagudu Muta Dandakor
* Emandi Saaru O Batlaru Doragaru
* One Two One Two
* Ramuni Bantunura
* Vesavu Bhale Veshalu

==2009 film==
Neramu Siksha is made into film in 2009 starring Krishna, Jayasudha and Vijaya Nirmala under the direction of Vijaya Nirmala.

==References==
 

==External links==
*  
*  

 

 
 
 
 
 