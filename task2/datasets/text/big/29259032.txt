Shakti (2011 film)
 
 

{{Infobox film
| name           = Shakti
| image          = Shakti Wallpaper HD.jpg
| alt            =  
| caption        = Threatical Movie poster
| director       = Meher Ramesh
| producer       = C. Ashwini Dutt
| writer         = Yandamoori Veerendranath J. K. Bharavi
| starring       = Jr. NTR Ileana DCruz Prabhu Ganesan Manjari Phadnis Jackie Shroff Pooja Bedi Vidyut Jamwal   
| music          = Mani Sharma
| cinematography = Sameer Reddy
| editing        = Marthand K. Venkatesh
| studio         = Vyjayanthi Movies
| distributor    = Sri Venkateswara Creations
| released       =  
| runtime        = 161 minutes
| country        = India Telugu
| budget         =   (official budget) 
| gross          =  
}}
 Telugu historical historical fantasy film directed by Meher Ramesh starring Jr. NTR and Ileana DCruz.   

The film, produced by C. Ashwini Dutt under the Vyjayanthi Movies banner, features an extensive supporting cast including Sonu Sood, Manjari Phadnis, S. P. Balasubrahmanyam, Jackie Shroff, Pooja Bedi and Tamil actor Prabhu Ganesan, was released on 1 April 2011.         A dubbed Tamil version was simultaneously released in Tamil Nadu as Om Shakthi.  flop as its box office earnings were considerably less than its budget. Shakti budget varies from   to   including marketing and print costs.

==Plot==
Aishwarya (Ileana DCruz) is the daughter of central union home minister Raya (Prabhu Ganesan). Due to some reason, he doesnt allow his daughter to go on a trip with friends. She nonetheless goes to the trip. She then meets a guide, Shakti (Jr. NTR), who takes the group on a trip to several places. At first, she hates him because he uses every chance to get money from her. But when he saves her, she starts liking him. Afterwards, a few people who are chasing her finally find her in Kashmir and try to kill Shakti as he hurtd them earlier. Their plan fails as Shakti turns out to be a Secret Agent and defends himself and catches the main person in this plan. When Aishwarya goes back to her father, he forgives her and asks whether she had taken a box in which there is a diamond. When she replies in the negative, he says that it is more important than his own daughter. Later, when Shakti goes to a temple, the diamond box falls off a basket his mother brought with flowers on top of it. They inform Mahadevaya who asks them to immediately come to Humpy with the box and Aishwarya. Their travel is blocked by a strong man who is then defeated by Shakti. He then kills one more person who tries to grab the box. Then Mahadeva Raya and a Swami (Nassar (actor)|Nassar) come to him and then the flashback is revealed.

It is said that Mahadeva Rayaa father (S. P. Balasubrahmanyam) was a royal king who was extremely generous and had the blessings of god which gave him the power that whenever he touched the diamond, it used to shine brightly emitting light. He reveals that apart from the 18 Shakti petas, there is a secret one which protects India and only their family knows of it. There is also a protector who guards it at all times. His name is Rudra (Jr. NTR). After every 27 years, they must go to the shakti peta where the protector will perform several spiritual rites which must be compulsorily performed. When he starts performing them, an Egyptian king who knows of this comes to destroy the shakti peta. The protector kills the king but a person who works under the Mahadeva Rayas father, Janaki Varma (Jackie Shroff) cheats him and kills both the protector and Mahadeva Raya as he sought money from the king. He also steals a sword (Maha Trishulam) which is necessary for the rites to be performed. When the protector is killed, his wife (Manjari Phadnis), at the same time, delivers a baby boy whom Rudra gives to his loyal servant. The servant tries his best to save the boy but circumstances force him to throw the boy into the river. The boy is then found by a childless couple who adopt him. After finding out his past, he sets out for revenge.

Shakti then goes to Janakis country where he is presently the underworld don and a multimillionaire. He kills Janaki and gets the sword. Finally he goes to perform the rites but is stopped by the Egyptian queen and her powerful son who comes back and cannot be defeated. The swami then tells him that the strong man can only be defeated by damaging his eyes. He then kills him and performs the rites.

==Cast==
  was selected as lead heroine marking her second collaboration with NTR Jr.]]
* NTR Jr. as Shakti Swaroop/Rudra
* Ileana DCruz as Aishwarya Mahadevaraya
* Prabhu Ganesan as Mahadevaraya
* Manjari Phadnis as Gauri (Rudras Wife)
* S. P. Balasubrahmanyam as Vijayaraya
* Jackie Shroff as Janaki Varma
* Pooja Bedi as Fakhtooni Nassar as Mahadevarayas Swamy
* Sonu Sood as Mukthar (Fakhtoonis husband) Ali
* Krishna Bhagavaan
* Reza Ashtarian
* Sayaji Shinde as Sivaji
* Brahmanandam as Avatar
* Dharmavarapu Subramanyam Venu Madhav as Sathi Babu
* M. S. Narayana
* Daniel as Raakha
* Vidyut Jamwal 

==Reception==
  | rev2 = | rev2Score =   | rev3 =  | rev3Score =   | rev4 =Times of India| rev4Score =   ||}}
The high-budget socio-fantasy film was panned by critics.  Sify stated "A major drawback of Shakti is that Meher Ramesh failed to club the periodic frames with the contemporary story. The flashback doesn`t suit the time factor. The story is nothing but the weird imagination of director (also story writer) Meher Ramesh. Technically speaking, there is no screenplay and editing at all, but the senseless play of the director`s wild and stoic imagination." 

Rediff, which gave a two star said, "Needless to say the story lacks clarity and there is no logical thread either. As a result, the audience has to bear the tedium and the meandering plot. Sadly Shakti does not seem to have much shakti to sustain." 

CNN-IBN noted "Shakthi, touted as the costliest film ever made in the Telugu film industry, may not appeal to the viewers because of illogical and contradictory sequences. There are many bloomers like this in the film, which has many plots and sub-plots to confuse the audience." 

Timesofindia also gave a negative review with two stars and said "The blame for this epic failure, however, should be taken by the director. In an attempt to make history, he ended up confusing the audience, thanks to the meagre story-line, poor narration and far-fetched imagination."   

===Box office===
The film opened to great response collected  4.25 crore (Share) in its first day.  On weekend collected  10 crore (Worldwide Share) which is very good though disastrous talk.  The film opened to very good response in USA collected $89,571 ( 55.57 lakhs)in 3 Days.  The film collected  14.4 crore during its first week.  The film collected  19.01 crore in its final worldwide Share at the box office.   The film declared Disaster at the box office. 

==Soundtrack==
{{Infobox album
| Name        = Shakti
| Type        = Soundtrack
| Artist      = Mani Sharma
| Cover       =
| Background  =
| Released    = 27 February 2011
| Genre       = Film soundtrack
| Length      = 33:03
| Label       = Aditya Music
| Producer    = Mani Sharma
| Reviews     =
| Last album  = Parama Veera Chakra   (2011)
| This album  = Shakti   (2011)
| Next album  = Teen Maar   (2011)
}}

The soundtrack, composed by Mani Sharma, was released on 27 February 2011.
{{Track listing
| lyrics_credits = 
| extra_column = Performer(s)
| total_length = 33:03
 Ranjith || length1 = 5:16 || lyrics1 =
| title2 = Prema Desam || extra2 = Vedala Hemachandra|Hemachandra, Saindhavi|| length2 = 4:36 || lyrics2 =
| title3 = Mathilega Pichhiga || extra3 = Ranjith, Chinmayi || length3 = 4:00 || lyrics3 =
| title4 = Surro Surra || extra4 = Javed Ali, Suchitra || length4 = 5:27 || lyrics4 =
| title5 = Yamaga Unde || extra5 = N.C. Karunya|Karunya, Malavika || length5 = 4:22 || lyrics5 =
| title6 = Mahishasura Mardhini || extra6 = Srivardhini || length6 = 5:52 || lyrics6 =
| title7 = Maharudra Shakti || extra7 = Muralidhar, Ranjith, Hemachandra, Hanumantha Rao, Rita, Saindhavi, Srivardhini || length7 = 3:30 || lyrics7 =
}}

==References==
 

==External links==
*  
*   at ReviewUmpire.com

 

 

 
 
 
 