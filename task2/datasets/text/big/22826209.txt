Cadavres
 
 Cadavre}}
 
{{Infobox film
| name           = Cadavres
| image          = Cadavres 2009.jpg
| caption        = DVD Cover
| director       = Erik Canuel
| producer       = Pierre Gendron Christian Larouche Richard Ostiguy
| writer         = Benoît Guichard
| based on       = Cadavres&nbsp;by  
| starring       = Patrick Huard Julie Le Breton Sylvie Boucher Christian Bégin Christopher Heyerdahl Marie Brassard Patrice Robitaille Hugolin Chevrette Gilles Renaud
| music          = Michel Corriveau
| cinematography = Jean-François Bergeron (editor)|Jean-François Bergeron
| studio         = Zoofilms
| distributor    =  
| runtime        = 117 minutes
| country        = Canada
| language       = French
}} 2009 film directed by Eric Canuel. It is a film adaptation of the 1998 novel Cadavres by François Barcelo, a tragicomedy about the deadly feelings that unite a brother and sister who have nothing in common. 

==Plot==
One Halloween, the alcoholic mother of good-for-nothing Raymond suddenly dies, and he throws her body in a ditch. Repenting, he calls his sister Angèle, an actress who he has not seen for ten years, to help find the corpse. But the corpse they bring back in the ruined family home is not that of their mother. The brother and the sister start a sinister adventure involving two gangsters in dire straits, two chilling dealers, a dishonest artist agent, a terribly stupid cop, and a horde of pigs.

==Cast==
* Patrick Huard as Raymond Marchildon
* Julie Le Breton as Angèle
* Sylvie Boucher as the mother
* Christian Bégin as agent Pilon
* Christopher Heyerdahl as Paulo
* Marie Brassard as Paulette
* Patrice Robitaille as Jos-Louis
* Hugolin Chevrette as Rocky
* Gilles Renaud as the producer

==Release==
The film was released February 20, 2009. It received mixed criticism and failed to create substantial revenue at the box-office. 

==References==
 

==External links==
 
*  
*  
*  

 
 
 
 
 