Maami
{{Infobox film
| name               = Maami
| image              = Maami poster.jpg
| alt                = 
| caption            = Theatrical release poster
| director           = Tunde Kelani
| based on        =  
| producer           = Tunde Kelani
| screenplay         = Tunde Babalola
| starring       =  
| music          = Adesiji Awoyinka
| cinematography = Sharafa Abagun
| editing        = Kazeem Agboola   Hakeem Olowookere
| studio         = Mainframe Film and Television Productions
| distributor   =  
| released       =  
| runtime        = 93 minutes
| country        = Nigeria
| language       =  
| budget         = ₦30 million 
| gross          = ₦11,928,600 
}}

Maami (English: My Mother) is a 2011 Nigerian drama film produced and directed by Tunde Kelani. It is based on a novel of the same name, written by Femi Osofisan, and adapted to screen by Tunde Babalola.  It stars Funke Akindele as Maami, along with Wole Ojo and Olumide Bakare.   Though the film was a commercial failure,  it was generally met with positive critical reviews.  
 Best Nigerian Achievement in Best Production Design and Best Child Actor. 

==Cast==
*Funke Akindele as Maami
*Wole Ojo as Kashimawo
*Ayomide Abatti as Young Kashimawo
*Tamilore Kuboye as Dolapo
*Olumide Bakare as Otunba Bamisaye
*Godwin Enakhena as Sports presenter
*Sanya Gold as Mr Ojo
*Peter Badejo as NFF scretary

The film features special guest appearances from Yinka Davies, Kayode Balogun, Fatai Rolling Dollar, and Biodun Kupoluyi.

==Release== June 12.  It was screened in film festivals,  before it went on general theatrical release on 3 February 2012. 

==Reception==

===Critical reception===
The film was met with generally positive critical reviews, mostly due to its powerful story and themes.   comments: "Any film that starts with a cacophony of vuvuzelas is unlikely to hold subtlety as a core value, and Maami certainly lives up to the reputation of bold filmmaking that Kelani is celebrated for. The plot is engaging and at points disturbing, fraught with theft, emotional blackmail and trickery".  Gbenga Adeniji of   of   concludes: "the overall experience is a superior one. It packs an emotional punch and you might just find yourself shedding a tear or two. We realize that good films cost money and have resigned ourselves to the product placements, but thankfully, they keep it tasteful and at a bare minimum here. It is not a perfect film but it is definitely one to watch".  9aijabooksandmovies gave 3 out of 5 stars and comments: "Maami is an emotionally intoxicating movie, where viewers are visually served with large pints of heart touching, tear jerking scenes, stemming from the unconditional love a poor mother has for her only son. It is a beautifully crafted story; Viewers swim laps in the pool of flash backs and stop intermittently to inhale fresh breaths of reality. Despite its short comings more on technical details, Maami is a must see movie and another good work from Mainframe productions".  Fola Akintomide comments: "Generally, the movie Maami successfully holds her audience bound with an amazing storyline, impressive technical display and exceptional performances by the actors; indeed once again, Nollywood veteran and multiple award winning Tunde Kelani stamped his name as one of the deserving icons in Africa". 

The film however still wasnt received well by some critics. Amarachukwu Iwuala of   gave a negative review; although he commended the picture quality and Funke Akindeles performance, he panned everyother aspect of the film, commenting: "I watched Magun and could not leave my seat when the movie was over. What struck me after watching Maami was like a car crash. Apart from the cinematic quality which cannot be taken away from him   every other thing was a wreck. The casting was below par, the scripting was hurried and it looked like something that was quickly put together." "I could honestly feel Funke trying to pull this Molue wreck out of the pits, the rest was just journey into the abyss of stupidity".  Itunu of The Movie Pencil panned the film and concluded: "Overall, the storyline had no solid foundation and was indecisive". 

===Box office===
The film opened strongly at the theatres.  However, earnings dropped considerably after the first week of release and the film was declared a commercial failure at the box office.  Although the film was very popular at the time of its release, it didnt translate to good business as it was heavily pirated. 

===Accolades=== Achievement in Best Production Design and Best Child Actor. It received six nominations at the 2013 Nollywood Movies Awards, including "Best Original Screenplay", "Best Actress in a Leading Role" for Akindele and "Best Indigenous Actor" for Wole Ojo; it won the award for "Best Indigenous Movie" and Akindele won "Best Indigenous Actress" award for her role. The film was also nominated for "Best Film Director" at the 2013 Nigeria Entertainment Awards. Maami also won awards at the 2013 Africa Magic Viewers Choice Awards and 2012 ZUMA Festival Awards.

{| class="wikitable sortable" style="width:100%"
|+Complete list of Awards
|-
!  Award !! Category !! Recipients and nominees !! Result
|- Africa Movie Africa Film Academy   (7th Africa Movie Academy Awards)  Best Nigerian Film
| Tunde Kelani
|  
|- Achievement in Cinematography
| Sharafa Abagun
|  
|- Best Production Design
| 
|  
|-
| Best Child Actor
| Ayomide Abatti
|  
|- Nollywood Movies|Nollywood Movies Network   (2013 Nollywood Movies Awards) 
| Best Actress in a Leading Role Funke Akindele
|  
|-
| Best Indigenous Actor
| Wole Ojo
|  
|-
| Best Indigenous Actress
| Funke Akindele
|  
|-
| Best Indigenous Movie
| Tunde Kelani
|  
|-
| Best Original Screenplay
| Tunde Babalola
|  
|-
| Best child Actor
| Ayomide Abatti
|  
|-
| Nigeria Entertainment Awards   (2013 Nigeria Entertainment Awards) 
| Best Film Director
| Tunde Kelani
|  
|- Multichoice   (2013 Africa Magic Viewers Choice Awards) 
| Best Actress in a Drama
| Funke Akindele
|  
|-
| Best Lighting Designer
| Oluwole Olawoyin
|  
|-
| Best Art Director
| Bola Bello
|  
|-
| Best Local Language Movie (Yoruba)
| Tunde Kelani
|  
|-
| Best Cinematographer
| Sharafa Abagun
|  
|- ZUMA Film Festival   2012 ZUMA Awards {{cite web | url=http://www.vanguardngr.com/2012/05/funke-akindele-tk-shine-at-zuma-awards/ | title=
Funke Akindele, TK shine at ZUMA awards | publisher=Vanguard | work=Vanguard Newspaper | date=12 May 2012 | accessdate=15 September 2014 }}  
| Best Director
| Tunde Kelani
|  
|-
| Best Nigerian Film
| Tunde Kelani
|  
|-
| Best Actress
| Funke Akindele
|  
|}

==Home media== VOD on 5 June 2013 through Dobox TV.   and was released on DVD by Ajimson company on 14 April 2014.     New scenes were shot and added to the Extended edition DVD; According to Kelani, the scenes were present in the original script but he initially decided not to shoot them.  The DVD was however heavily pirated in less than 48 hours of its release, leading to a huge loss for Mainframe Studios. 

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 