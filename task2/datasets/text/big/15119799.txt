On Golden Pond (1981 film)
{{Infobox film
| name = On Golden Pond
| image = On_golden_pond.jpg 
| caption = Movie poster by Bill Gold
| director = Mark Rydell
| producer = Bruce Gilbert
| writer = Ernest Thompson
| based on =  
| starring       = {{plainlist|
* Katharine Hepburn
* Henry Fonda
* Jane Fonda
* Doug McKeon
* Dabney Coleman
* William Lanteau
}}
| music = Dave Grusin Billy Williams
| editing = Robert L. Wolfe
| studio = ITC Entertainment
| distributor = Universal Pictures
| released =  
| runtime = 109 minutes
| country = United States
| language = English
| budget = $15 million  
| gross   = $119,285,432  
}}
 1979 play of the same title. Henry Fonda won the Academy Award for Best Actor in what was his final film role. Co-star Katharine Hepburn also received an Oscar, as did Thompson for his script, and there were a further seven Oscar nominations for the film, including Jane Fonda, who played the daughter. The film co-starred Dabney Coleman and Doug McKeon.

==Plot==
An aging couple, Ethel and Norman Thayer, continue the long tradition of spending each summer at their cottage on a lake in the far reaches on northern New England called Golden Pond. When they first arrive, Ethel notices the loons calling on the lake "welcoming them home." As they resettle into their summer home, Normans memory problems arise when he is unable to recognize several family photographs, which he copes with by frequently talking about death and growing old. They are visited by their only child, a daughter, Chelsea, who is somewhat estranged from her curmudgeon of a father. She introduces her parents to her fiance Bill and his thirteen-year-old son Billy. Norman tries to play mind games with Bill, an apparent pastime of his, but Bill wont hear of it, saying he can only take so much. In another conversation, Chelsea discusses with Ethel her frustration over her relationship with her overbearing father, feeling that even though she lives thousands of miles away in Los Angeles, she still feels like shes answering to him. Before they depart for a European vacation, Chelsea and Bill ask the Thayers to permit Billy to stay with them while they have some time to themselves. Norman, seeming more senile and cynical than usual due to his 80th birthday and heart palpitations, agrees to Billy staying. Ethel tells him that hes the sweetest man in the world, but she is the only one who knows it.

Billy is at first annoyed by being left with elderly strangers with no friends nearby and nothing to do. He resents Normans brusque manner, but eventually comes to enjoy their Golden Pond fishing adventures together. Billy and Norman soon grow obsessed with catching Normans fish rival, named "Walter," which leads to the accidental destruction of the Thayers motorboat.  Chelsea returns to find out her father has made good friends with her fiances, now husbands, son. But when she sees the change in her fathers demeanor, Chelsea attempts something Billy accomplished that she never could: a backflip. Chelsea successfully executes the dive in front of a cheering Norman, Billy, and Ethel. Chelsea and Norman finally fully embrace before she departs with Billy back home.

The final day on Golden Pond comes and the Thayers are loading the last of the boxes. Norman tries to move a heavy box, but starts having heart pain and collapses onto the floor of the porch. Ethel tries unsuccessfully to get the operator to phone the hospital. Norman then says the pain is gone and attempts to stand to say a final farewell to the lake. Ethel tells him she has always known about death but for the first time it felt real, thinking Norman was going to die on the spot. Ethel helps Norman to the edge of the lake where they see the loons and Norman notes how they are just like him and Ethel, that their offspring is grown and gone off on her own, and now it is just the two of them.

==Production notes==
 
Jane Fonda purchased the rights to the play specifically for her father, Henry Fonda, to play the role of the cantankerous Norman Thayer.  The father-daughter rift depicted on screen closely paralleled the real-life relationship between the two Fondas.

Screenwriter Thompson spent his summers along the shores of Great Pond, located in Belgrade, Maine, but the film was made on Squam Lake in Holderness, New Hampshire.  The house used in the film was leased from a New York physician and was modified significantly for the shoot: an entire second floor was added as a balcony over the main living area at the request of the production designer. After the shoot the production company was contractually obligated to return the house to its original state but the owner liked the renovations so much that he elected to keep the house that way and asked the crew not to dismantle the second story.  A gazebo and a small boathouse were also relocated during the shoot.
 loons on the lake, the speedboat that zoomed by and disturbed them was so forceful it overturned their canoe in one take; Fonda was immediately taken out of the water and wrapped up in blankets as his health was fragile by that time.  The speedboat was piloted by the screenwriter, Ernest Thompson. 

When visiting Holderness, New Hampshire, one can take a boat tour of Squam Lake and view the filming sites from the movie. There is also a restaurant called "Walters Basin," which is named after the trout called "Walter" that Billy catches with Norman. For filming, "Walter" was brought in from a trout pond at the nearby Castle in the Clouds estate. He was released after his capture back into Squam Lake. Leftover footage of Fonda and Hepburn driving through the New Hampshire countryside, as seen in the opening credits, was later used for the opening of the CBS television sitcom Newhart. 
 studio behind Lord Grade, the television and film Media mogul|mogul. It was Lord Grade who largely raised the finance for the film. 
 2nd highest grossing film of the year, following Raiders of the Lost Ark, which earned $209,562,121. 

==Cast==
*Katharine Hepburn - Ethel Thayer 
*Henry Fonda - Norman Thayer Jr. 
*Jane Fonda - Chelsea Thayer Wayne 
*Doug McKeon - Billy Ray Jr
*Dabney Coleman - Bill Ray
*William Lanteau - Charlie Martin
*Chris Rydell - Sumner Todd

==Critical reception==
Roger Ebert of the Chicago Sun-Times said "On Golden Pond was a treasure for many reasons, but the best one, I think, is that I could believe it. I could believe in its major characters and their relationships, and in the things they felt for one another, and there were moments when the movie was witness to human growth and change. I left the theater feeling good and warm, and with a certain resolve to try to mend my own relationships and learn to start listening better . . . watching the movie, I felt I was witnessing something rare and valuable." 
 cheddar . . . Mr. Fonda gives one of the great performances of his long, truly distinguished career. Here is film acting of the highest order . . . Miss Hepburn . . . is also in fine form. One of the most appealing things about her as an actress is the way she responds to - and is invigorated by - a strong co-star . . . she needs someone to support, challenge and interact with. Mr. Fonda is the best thing thats happened to her since Spencer Tracy and Humphrey Bogart . . . an added pleasure is the opportunity to see Dabney Coleman   a role that goes beyond the caricatures hes usually given to play . . . On Golden Pond is a mixed blessing, but it offers one performance of rare quality and three others that are very good. Thats not half-bad." 

TV Guide rates it 3½ out of a possible four stars, calling it "a beautifully photographed movie filled with poignancy, humor, and (of course) superb acting . . . there could have been no finer final curtain for   than this."  Channel 4 sums up its review by stating, "Henry Fonda and Katharine Hepburn both shine in an impressively executed Hollywood drama.   has its mawkish moments but theres a certain pleasure in that, and writer Thompsons analysis of old age is sensitive, thought-provoking and credible." 

Not all reviewers were impressed, however.  David Kehr of the Chicago Reader called the film "the cinematic equivalent of shrink-wrapping, in which all of the ideas, feelings, characters, and images are neatly separated and hermetically sealed to prevent spoilage, abrasion, or any contact with the natural world . . . Mark Rydells bright, banal visual style further sterilizes the issues. The film exudes complacency and self-congratulation; it is a very cowardly, craven piece of ersatz art."  Time Out London says, "Two of Hollywoods best-loved veterans deserved a far better swan song than this sticky confection."  Mad (magazine)|Mad magazine satirized the octogenarian-themed film in their typical unsubtle manner, titling it On Olden Pond.

American Film Institute recognition
* AFIs 100 Years... 100 Movies - Nominated 
* AFIs 100 Years... 100 Passions - #22
* AFIs 100 Years... 100 Movie Quotes:
** "Listen to me, mister. Youre my knight in shining armor. Dont you forget it. Youre going to get back on that horse, and Im going to be right behind you, holding on tight, and away were gonna go, go, go!" - #88
** "Come here, Norman. Hurry up. The loons! The loons! Theyre welcoming us back." - Nominated 
* AFIs 100 Years of Film Scores - #24
* AFIs 100 Years... 100 Cheers - #45
* AFIs 100 Years... 100 Movies (10th Anniversary Edition) - Nominated 

==Accolades==
;Academy Awards   
*Academy Award for Best Actor (Henry Fonda, winner)
*Academy Award for Best Actress (Katharine Hepburn, winner) Academy Award for Best Adapted Screenplay (Ernest Thompson, winner)
*Academy Award for Best Picture (nominee) 
*Academy Award for Best Supporting Actress (Jane Fonda, nominee)
*Academy Award for Best Cinematography (nominee)
*Academy Award for Best Director (nominee) Academy Award for Best Film Editing (nominee) Academy Award for Best Original Score (nominee)
*Academy Award for Best Sound (Richard Portman and David M. Ronne, nominee)

;BAFTAs
*BAFTA Award for Best Actress in a Leading Role (Katharine Hepburn, winner)
*BAFTA Award for Best Film (nominee)
*BAFTA Award for Best Actor in a Leading Role (Henry Fonda, nominee)
*BAFTA Award for Best Actress in a Supporting Role (Jane Fonda, nominee)
*BAFTA Award for Best Direction (nominee)
*BAFTA Award for Best Screenplay (nominee)

;Golden Globes
*Golden Globe Award for Best Motion Picture - Drama (winner)
*Golden Globe Award for Best Actor - Motion Picture Drama (Henry Fonda, winner) 
*Golden Globe Award for Best Screenplay (Ernest Thompson, winner)
*Golden Globe Award for Best Director - Motion Picture (nominee) 
*Golden Globe Award for Best Actress - Motion Picture Drama (Katharine Hepburn, nominee) 
*Golden Globe Award for Best Supporting Actress - Motion Picture (Jane Fonda, nominee)

;Others
*Directors Guild of America Award for Outstanding Directorial Achievement in Motion Pictures (nominee) Grammy Award for Best Album of Original Score Written for a Motion Picture or Television Special (nominee)
*Writers Guild of America Award for Best Drama Adapted from Another Medium (winner)

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 