The Moment After 2: The Awakening
{{Infobox Film
| name           = The Moment After 2: The Awakening
| image          = The Moment After 2- The Awakening.jpg
| image_size     = 
| caption        = 
| director       = Wes Llewellyn
| producer       = 
| writer         = Amanda Llewellyn
| narrator       =  John Gilbert David A.R. White Kevin Downes Tim Williams
| cinematography = Todd Barron
| editing        = Wes Llewellyn Mike Wech
| studio         = Signal Hill Pictures
| distributor    = ChristianCinema.com
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Christian sequel film to The Moment After. It is a post-Rapture film following the lives of two former FBI agents.  The film was featured at both the Merrimack Valley Christian Film Festival  and the Greater Orlando Christian Film Festival. 

== Synopsis ==
After escaping death, former FBI agent Adam Riley (David A.R. White) reunites with his friend and Christian mentor Jacob Krause (Brad Heller). They know not of the forces who are trying to destroy them. Global Alliance leader Colonel Fredericks (Monte Perlin) has forced Adams former partner Charles Baker (Kevin Downes) to hunt them down while a ragtag militia, led by "Captain" Jackson (Lonnie Colon), spy on them for their own gain. As events lead to an explosive confrontation, they are all forced to an awakening of the struggle for their souls.

== Cast == John Gilbert as Peter McCullum
* David A.R. White as Adam Riley
* Kevin Downes as Charles Baker
* Asad Farr as Global Chairman
* Logan White as Carissa
* Don Parker Decker	 as the President of the United States
* Brad Heller as Jacob Krause
* Bree Pavey as Audrey Thomas

== References ==
 

== External links ==
*  
*  

 
 
 
 
 