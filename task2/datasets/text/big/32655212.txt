On Our Selection (1932 film)
 
{{Infobox film
| name           = On Our Selection
| image          = On_Our_Selection_Poster.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = Ken G. Hall
| producer       = Bert Bailey
| writer         = Bert Bailey   Ken G. Hall play by Edmund Duggan   based on the stories of Steele Rudd
| narrator       = 
| starring       = Bert Bailey  Fred MacDonald Dick Fair
| music          = 
| cinematography = Walter Sully
| editing        = George Malcolm
| sound          = Arthur Smith Clive Cross
| studio         = Cinesound Productions
| distributor = British Empire Films (Australia) Universal (UK)
| released       = 22 July 1932 (Brisbane)  12 August 1932 (Sydney) 1933 (UK)
| runtime        = 99 minutes (Australia)
| budget         = £8,000 "Counting the Cash in Australian Films", Everyones 12 December 1934 p 19-20    
| gross          = ₤60,000 (end 1953)    ₤2,000 (UK) 
| country        = Australia
| language       = English
}} popular play Edmund Duggan in 1912, which formed the basis for the screenplay. Bailey repeats his stage role as Dad Rudd. He also wrote the script with director Ken G. Hall.

The movie was one of the most popular Australian films of all time. 
 Down on the Farm.

==Synopsis==
The movie opens with the title card "bushland symphony", followed by sounds and vision of the Australian bush. The subsequent action involves a series of various subplots centered around a "selection" in South West Queensland owned by Dad Rudd: he owes some money to his rich neighbour, old Carey, who is determined to break Dad financially; his educated daughter Kate is pursued by two men, the poor but devoted Sandy and Careys villainous son, Jim; one of his workers, Cranky Jack, has a mysterious background; comic visits from a parson and country dentist who removes Dads tooth; his dim son Dave proposes to his girlfriend, Lily; his other son, Joe, causes slapstick havoc; Dave gets married and moves out with his wife and tries to borrow money from his father; Dads daughter Sarah is pursued by the high-voiced Billy, who Dad doesnt like; Dad Rudd runs for parliament opposite Carey; and his horse wins a race.

The main story concerns a murder mystery. Jim Carey attempts to blackmail Kate into being with him by lying about what she did in the city, and Sandy knocks him out. Carey later turns up dead and Sandy is suspected of the murder. The Rudds hold a dance and a police officer turns up to arrest Sandy when Cranky Jack confesses he killed Carey because the dead man stole his wife. The film ends with Dad and Mum happily watching the sun come up.

==Cast==
*Bert Bailey as Dad Rudd
*Fred MacDonald as Dave Rudd
*Alfreda Bevan as Mum Rudd
*John McGowan as Maloney
*Molly Raynor as Kate Rudd
*Dick Fair as Sandy Graham
*John Warwick as Jim Carey
*Billy Driscoll as Uncle
*Lilias Adeson as Lily White
*Len Budrick as Old Carey
*Bobbie Beaumont as Sarah Rudd
*Ossie Wenban as Joe Rudd
*Fred Kerry as Cranky Jack
*Dorothy Dunkey as Mrs White
*Fred Browne as Billy Bearup
*Arthur Dodds as parson

==Production==
The film was the first full-length feature from Cinesound Productions. Ken G. Hall was reluctant to make the film as he thought it was old fashioned but the play had been enormously popular and Stuart Doyle thought an adaptation would be well received. 

Most of the cast had appeared in the stage version. Although Bert Bailey was clean shaven and generally wore a fake beard for his stage performances, Hall insisted he grow a real beard for the film version; Bailey did and ended up wearing a beard for the rest of his life. 

Bailey was paid ₤400 and received 60% of the profits. 

Shooting took place in mid 1931, partly in a makeshift studio at an ice skating rink in Bondi Junction, with location filming in Castlereagh, Penrith. Crucial to the movies success was a sound recording system invented by Tasmanian engineer Arthur Smith, which enabled the film to be made without having to hire sound equipment from Hollywood. (The equipment cost £2,000 – to have imported it would have cost ten times that. ) Hall later said Smiths contribution was so important "there would have been no Cinesound without him". 

The cinematographer, Walter Sully, was a newsreel cameraman for Cinesound. It was the only feature he made for that company as he soon left to go work for their competitor, Fox Movietone.

==Reception==
On Our Selection was an enormous success at the box office, being among the top four most popular films in Australian cinemas in 1932  had earned earning ₤46,000 in Australia and New Zealand by the end of 1933. Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, 158.  By the end of 1934 the film had made an estimated profit of £23,200 and Bailey had earned an estimate £14,000.   

On Our Selection was continually revived over the next two decades and by 1953 had earned an estimated £60,000. The profits were split 50-50 between Cinesound Productions and the theatrical partnership of Bert Bailey and James Grant.  (In 1938 Hall estimated that 70% of the films income had been earned in country regions. )

The movie was bought for release in the UK by Universal for £1,000 and was retitled Down on the Farm on the grounds that the British would not know what a "selection" was.  Critical response was poor, the Era claiming that "Australias idea of what constitutes good comedy an obvious caricature of a typical bush homestead, is only likely to please an unsophisticated audience over here."  It achieved 500 bookings throughout the country. 

==Legacy==
The movie launched Halls career as a director and led to three sequels, starting with Grandad Rudd (1935). All the sequels featured Bert Bailey as Dad and Fred MacDonald as Dave, but a variety of actors would play Mum and their other children.

==See also==
*Grandad Rudd (1935)
*Dad and Dave Come to Town (1938)
*Dad Rudd, MP (1940)

==References==
 

==External links==
* 
*  at Australian Screen Online
*  at Oz Movies
*  by Steele Rudd at Project Gutenberg

 
 

 
 
 
 
 