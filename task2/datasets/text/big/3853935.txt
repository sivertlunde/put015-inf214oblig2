A Fool There Was (1915 film)
 
 
{{Infobox film
| name           = A Fool There Was
| image          = Fooltherewas1915movieposter.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Frank Powell William Fox
| based on       =  
| writer         = Roy L. McCardell (scenario) Frank Powell (adaptation)
| narrator       =
| starring       = Theda Bara Edward José
| music          =
| cinematography = George Schneiderman
| editing        =
| distributor    = Box Office Attractions Company Fox Film Corporation (1918 re-release)
| released       =  
| runtime        = 67 minutes (1915 release)
| country        = United States
| language       = Silent film English intertitles
| budget         =
| gross          =
}} William Fox, and starring Theda Bara. The film was long considered controversial for such risqué intertitle cards as "Kiss me, my fool!"  

The film is one of the few movies with Theda Bara that still exist today. It is the origin of the term "vamp (woman)|vamp" (short for vampire), referring to a femme fatale who causes the moral loss of those she seduced, and about how a vampire fascinates then exhausts its victims.

==Plot== Vampire woman" (Theda Bara) who uses her charms to seduce men and leave after ruining their lives.

Completely under the influence of this woman, he loses his job and abandoned his family. All attempts by his family to get him back on the right path fail. And the life of the "idiot" degrades more.

==Cast==
 
* Theda Bara as the Vampire
* Edward José as the husband (the fool), John Schuyler
* Mabel Frenyear as Kate Schuyler (the fools wife)
* Runa Hodges as their daughter
* May Allison as the wifes sister
* Clifford Bruce as the friend, Tom
* Victor Benoit as one of her victims, Reginal Parmalee
* Frank Powell as the doctor, as Frank Fowell
* Minna Gale as the doctors fiancee

==Broadway Origins==
The film was based on a 1909 Broadway play titled A Fool There Was by Porter Emerson Browne, which in turn was based on Rudyard Kiplings poem The Vampire. On the stage Baras part was played by actress Katharine Kaelred and was simply referred to as "The Woman". The star of the play was actually a male, Victorian matinee idol Robert C. Hilliard, whose name featured prominently in some advertisements for the movie though he had no connection with the film.

==Production and legacy==
 
 
The producers were keen to pay tribute to their literary source, having a real actor read the full poem to the audience before each initial showing, and presenting passages of the poem throughout the film in intertitles. Baras official credit is even "The Vampire", and for this reason the film is sometimes cited as the first "vampire" movie. 

The film was also a watershed in early film publicity. At a press conference in January, the studio gave an elaborate fictional biography of Theda Bara, making her an exotic Arabian actress, and presented her in a flamboyant fur outfit. Then they made an intentional leak to the press that the whole thing was a hoax. This may have been one of Hollywoods first publicity stunts.

The film marked the first on-screen appearance of the popular World War I-era film actress May Allison.
 British Board of Film Censors under its policy of not passing films with illicit sexual relationships.  Although A Fool There Was never received a public showing in Great Britain, later Theda Bara films were allowed.

Though the film contains scenes set in England and Italy, the entire movie was filmed in St. Augustine, Florida. 
 The Stain East Lynne (1916), and two short comedies she made for Hal Roach in the mid-1920s. This film showcases Baras status as the original screen "vamp" (so named for her portrayal of a female vampire).

== References ==
:Notes
 

;Bibliography
*  
*  

==External links==
*  
*  
*  
*  
*  *note stage actor Robert Hilliards(star of the play) name used in the publicity
*  

 
 
 
 
 
 
 
 
 
 
 
 