Some Girls (film)
{{Infobox film name          = Some Girls image         = Poster of the movie Some Girls.jpg director  Michael Hoffman producer      = Robert Redford writer        = Rupert Walters starring      = Patrick Dempsey Jennifer Connelly Andre Gregory music         = James Newton Howard
|cinematography= Ueli Steiger editing       = David Spiers studio        =  distributor   = United Artists MGM released      =   runtime       =  country       = USA language      = English  budget        =  gross         =  $401,421 
}}
Some Girls is a 1988 film starring Patrick Dempsey and Jennifer Connelly. 

==Synopsis==
Michael, a college student, (Patrick Dempsey) goes to Quebec City, Canada for Christmas at the request of his college girlfriend Gabriella (Jennifer Connelly), after months of her stonewalling him after she left college in mid-semester. 

Michael starts the vacation full of hope and youthful enthusiasm because she has invited him to stay with her family. He arrives and is left waiting at the airport for hours. Gabriella (he calls her Gabi) eventually arrives to pick him up and explains that sometimes they just dont answer their phone. He brushes off his frustration because hes happy to see her.
 Sheila Kelley) and Simone (Ashley Greenfield), along with a handsome young handyman named Nick (Lance Edwards) who is romantically involved with Irenka.  

Although Michael enthusiastically attempts to join in the unusual habits of the family (an example is the apparent tradition of growling at ones dinner plate before eating) he quickly learns that fitting in will be a challenge. The situation is complicated by the illness of Gabis grandmother, the matriarch of the family, and the reason why Gabi left school in the first place. Almost immediately, Michael commits the apparently unforgivable gaffe of expressing his condolences to Gabis mother. Things go downhill for Michael from there. As soon as they have time alone, Gabi announces that she no longer loves him. His attempts to change her mind seem somewhat successful, but the fickle female spirit continues to thwart him. In the meantime, he is seductively teased first by Irenka, then by Simone, and possibly even by her mother, all with Gabis apparent complicity. At one point, Gabi appears to have a change of heart toward Michael, but then again decides that she doesn’t love him.

Gabis grandmother (Lila Kedrova), whom we know only as "Granny," is in a care facility apparently suffering from senile dementia. Upon meeting Michael, she acts as if he is her long-dead husband, who was also named Michael. In an attempt to find her husband, she escapes from the hospital and goes to the country estate which they shared when they were first married. The girls, Nick, and Michael frantically rush to the estate, hoping Granny hasnt frozen in the cold of winter. During their search, Michael gets lost in the woods and falls into a hole, unable to get out. He hears footsteps and finds it is Granny. Apparently, she helps him out and they go back to the house together where Michael lights a fire and gets Granny out of her wet clothes. She then proceeds to talk to him as if he were her husband, and they form an unexpected bond.

Granny falls ill on the return trip, and despite Nicks valiant attempt to reach a hospital in time, she dies. After the funeral, and a tryst with Irenka, Michael makes a solitary visit to her grave-site where he encounters a mysterious young woman and confesses to her that he deeply loved Granny. Later he recognizes the young woman from a photograph as Granny in her youth, and realizes their meeting may have been supernatural. Ultimately, he returns home and we are left to assume that he never sees Gabi or her family again, as she has decided not to return to college. But Michael does not seem disappointed at the end of their relationship; instead he is deeply touched by his unexpected spiritual bond with her grandmother.

==Cast==
*Patrick Dempsey - Michael
*Jennifer Connelly - Gabriella dArc Sheila Kelley - Irenka dArc
*Lance Edwards - Nick
*Lila Kedrova - Granny
*Florinda Bolkan - Mrs. dArc
*Andre Gregory - Mr. dArc
*Ashley Greenfield - Simone dArc
*Jean-Louis Millette - Father Walter
*Sanna Vraa - Young Granny

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 