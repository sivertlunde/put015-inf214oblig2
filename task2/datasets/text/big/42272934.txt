Road 47
{{Infobox film
| name           = Road 47
| image          = Road 47 Poster.jpg
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| director       = Vicente Ferraz
| producer       = {{Plain list|
*Isabel Martinez
*Matias Mariani
*Joana Mariani
*Danielle Mazzocca
*Leonel Vieira
}}
| screenplay     = Vicente Ferraz
| starring       = {{Plain list | Daniel de Oliveira
*Richard Sammel
*Sergio Rubini
*Julio Andrade
*Giorgio Vicenzotti
}}
| music          = Luiz Avellar
| cinematography = Carlos Arango De Montis
| editing        = Mair Tavares
| studio         = Primo Filmes Stopline Films Três Mundos Produções Verdeoro
| distributor    = Europa Filmes (Brazil)
| released       =   }}
| runtime        = 107&nbsp;minutes   
| country        = Brazil Portugal Italy
| language       = Portuguese Italian German
| budget         = 
| gross          = 
}} Daniel de Oliveira, Richard Sammel, Sergio Rubini and Julio Andrade.

The film follows the trajectory of the landmine clearance unit from the Brazilian Expeditionary Force who, after a panic attack, are trying to get tempers and defuse the mined road that separates them from a village monitored by the axis forces.   

== Plot ==
During the World War II, Brazil was an ally of the United States, Britain and France. At the time there were directed over 25&nbsp;thousand soldiers from the BEF (Brazilian Expeditionary Force) to fight the enemies, represented by the Axis: Germany, Italy and Japan. Almost all soldiers came from poor backgrounds, and were mostly unprepared for combat, they had to learn in practice to fight for survival.
 Daniel de Oliveira), Tenente (Julio Andrade), Piauí (Gaspar Francisco) and Laurindo (Thogum) try to get down the mountain, but end up missing the others. When they were reunited, they had to decide to return to the battalion and are at risk of facing court-martial for dereliction of duty, or return to the position the night before and at risk of facing a surprise attack of the enemy. Thats when the journalist Rui (Ivo Canelas), tells about an active minefield and they think this is a chance to redeem the mistake they committed, but much is yet to happen and the war is far from over.   

== Cast ==

*Sergio Rubini as Roberto Daniel de Oliveira as Guimarães Guima
*Thogun as Sergente Laurindo
*Francisco Gaspar as Piauí
*Júlio Andrade as Tenente Penha
*Ivo Canelas as Rui
*Richard Sammel as Colonnello Mayer
*Daniele Grassetti as Partigiano
*Giorgio Vicenzotti as Partigiano

== Production ==
In an interview, Vicente Ferraz said that he wanted to discuss Brazils history in World War II, which he said was "forgotten by the Brazilians and which is completely unknown abroad."  He used diaries, letters, and interviews as source material. 

== Release ==
Road 47 premiered at the 2013 Festival do Rio. 

== Reception ==
Jonathan Holland of The Hollywood Reporter wrote, "Competent but not inspiring, this is a simple and effective film which never does full justice to the stirring material it’s based on."   Mark Adams of Screen International called it an "impressively mounted war film" and "high quality drama". 

== References ==
 

== External links ==
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 