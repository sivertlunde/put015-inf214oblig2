Reykjavík-Rotterdam
{{Infobox film
| name           = Reykjavík-Rotterdam
| image          = Reykjavik-Rotterdam.jpg
| caption        = Reykjavík-Rotterdam promotional poster
| director       = Óskar Jónasson
| producer       = Agnes Johansen Baltasar Kormákur Dirk Rijneke Mildred van Leeuwaarden
| writer         = Arnaldur Indriðason Óskar Jónasson
| starring       =Baltasar Kormákur Ingvar E. Sigurðsson Lilja Nótt Þórarinsdóttir Þröstur Leó Gunnarsson  Victor Löw Ólafur Darri Ólafsson Jörundur Ragnarsson
| cinematography = Bergsteinn Björgúlfsson
| editing        = Elísabet Ronaldsdóttir
| music          = Barði Jóhannsson
| studio         = Blueeyes Productions Rotterdam Films
| distributor    =
| released       = 3 October 2008 
| runtime        = 88 minutes
| country        = Iceland
| language       = Icelandic
| budget         =
| gross          = 	
| followed_by    =
| preceded_by =
}} Icelandic film directed by Óskar Jónasson and starring Baltasar Kormákur.

==Plot synopsis==
Like a fish on a dry land, Kristofer is stuck in a dull everyday routine, working as a security guard. He was fired from the freight ship on which he worked when he was caught smuggling alcohol. Faced with money problems, he is tempted to accept the help of his friend, Steingrimur, who manages to pull some strings to get his old job back. He decides to take his chances one last time on a tour to Rotterdam.

==Release== ARD on 1 January 2010. 

==Reception== Icelandic cinema. The film garnered a four-star rating from Morgunblaðið,  and has a fresh rating of 8.2/10 at Rottentomatoes.com. 

The film won five Edda Awards, including best script, director, editing, sound, and music: 
* Director of the year – Óskar Jónasson
* Screenplay of the year – Arnaldur Indriðason and Óskar Jónasson
* Editor of the year – Elísabet Rónaldsdóttir
* Sound design of the year – Kjartan Kjartansson and Ingvar Lundberg
* Achievement in music for film or television – Barði Jóhannsson
 82nd Academy receive an Oscar nomination.
 Jar City" and an "uncommonly commercial item with brawny action, strokes of humor and a besieged rooting interest (played effectively by Kormákur)." 

==Hollywood remake==
Working Title Films released a U.S. remake entitled Contraband (2012 film)|Contraband starring and produced by Mark Wahlberg.  The original films lead actor, Baltasar Kormákur—a successful director in his home country of Iceland—took on the role of director. 

==References==
 

==External links==
*   from Blueeyes Productions in co-production with Rotterdam Films
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 