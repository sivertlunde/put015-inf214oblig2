A Man of Iron
{{infobox film
| name           = A Man of Iron
| image          =
| imagesize      =
| caption        =
| director       = Whitman Bennett
| producer       = Whitman Bennett
| writer         = Lawrence Marston
| starring       = Lionel Barrymore Mildred Harris
| music          =
| cinematography = Edward Paul
| editing        =
| distributor    = Chadwick Pictures Corporation
| released       = June 28, 1925
| runtime        = 5 reels
| country        = United States
| language       = Silent film (English intertitles)
}} lost  silent film produced and directed by Whitman Bennett and distributed through Chadwick Pictures. The film starred Lionel Barrymore. 

==Cast==
*Lionel Barrymore - Philip Durban
*Mildred Harris - Claire Bowdoin
*Winifred Barry - Martha Durban
*Dorothy Kingdon - Edith Bowdoin
*Alfred Mack - Hugh Bowdoin
*J. Moy Bennett - Denis Callahan
*Isobel De Leon - Maybelle Callahan
*Jean Del Val - Prince Novakian

==See also==
*List of lost films

==References==
 

==External links==
* 
* 
* 
* 

 
 
 
 
 
 
 


 