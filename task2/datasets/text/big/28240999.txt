Lord Jeff
{{Infobox film
| name           = Lord Jeff
| image          =
| caption        =
| director       = Sam Wood
| producer       = Frank Davis Sam Wood
| writer         = Bradford Ropes (story) Endre Bohem (story) Val Burton (story) James Kevin McGuinness Frank Davis (uncredited) Walter Ferris (uncredited) Sam Wood (uncredited)
| narrator       =
| starring       = Freddie Bartholomew Mickey Rooney Charles Coburn
| music          =
| cinematography =
| editing        =
| studio         = Metro-Goldwyn-Mayer
| distributor    =
| released       =  
| runtime        =
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Lord Jeff is a 1938 film starring Freddie Bartholomew as a spoiled orphan who gets mixed up with some crooks, but gets set straight by a stint in a school.

==Plot== mercantile marine school, one of many vocational schools run by Barnardos|Dr. Barnardos home for orphaned boys, with the warning that if he does not behave himself, he will be transferred to a reformatory.

The school is headed by Captain Briggs (Charles Coburn). Briggs assigns longtime model student Terry OMulvaney (Mickey Rooney) to take Geoff under his wing. However, Geoff is not interested in fitting in; he only wants to return to London to be reunited with Doris and Jim. He soon antagonizes all of the other boys, with the sole exception of the irrepressibly cheerful Albert Baker (Terry Kilburn).
 RMS Queen big chill", refusing to have anything to do with him.

The bleak isolation of not being spoken to by the other boys takes its toll on Geoff, although he doesnt want to show it. He learns several life lessons at the hands of kindly and wise instructor "Crusty" Jelks (Herbert Mundin). Geoff confesses his runaway attempt to Captain Briggs, so that Terry might possibly be reinstated for the Queen Mary. He asks Captain Briggs not to tell the boys that the information clearing Terry came from him. Briggs selects Terry and Geoff to join the crew of the Queen Mary.

When Doris and Jim finally manage to contact Geoff, he refuses to go back to his crooked life, and tells them he is going to sail on the Queen Mary. Since the stolen necklace is too well known in England, Jim sews it inside Geoffs coat when Geoff is not looking, and books passage aboard the Queen Mary, bound for America. The necklace is found at the school, forcing Geoff to choose between conflicting loyalties. He chooses wisely, but Doris and Jim are nowhere to be found. Geoff is taken in for questioning by the police, meaning he will miss the sea voyage. Luckily, one of his schoolmates recognizes the crooked couple on the Queen Mary, and they are arrested in time for Geoff to board the ship and join Terry.

==Cast==
*Freddie Bartholomew as Geoffrey Braemer
*Mickey Rooney as Terry OMulvaney
*Charles Coburn as Captain Briggs
*Herbert Mundin as Bosun "Crusty" Jelks
*Terry Kilburn as Albert Baker
*Gale Sondergaard as Doris Clandon
*Peter Lawford as Benny Potter
*Walter Tetley as Tommy Thrums
*Peter Ellis as Ned Saunders
*George Zucco as Jim Hampstead
*Matthew Boulton as Inspector Scott
*John Burton as John Cartwright
*Emma Dunn as Mrs. Briggs
*Monty Woolley as Jeweler
*Gilbert Emery as Magistrate
*Charles Irwin as Mr. Burke
*Walter Kingsford as Superintendent

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 