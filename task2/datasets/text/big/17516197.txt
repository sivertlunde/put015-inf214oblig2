The Adventures of Tarzan
{{Infobox film name  = The Adventures of Tarzan image  = Adventures of Tarzan - Elmo Lincoln.jpg image size = 175px writer  = Robert F. Hill Lillian Valentine based on novels by Edgar Rice Burroughs starring = Elmo Lincoln Louise Lorraine Scott Pembroke Frank Whitson Lillian Worth director  = Robert F. Hill Scott Sidney producer  Louis Weiss distributor = Numa Pictures Corporation released    = December 1, 1921 (first chapter) runtime     = ? mins. (15 chapters) language    = Silent English Intertitles
}}     Louis Weiss, written by Robert F. Hill and Lillian Valentine (partially based on the novels The Return of Tarzan and Tarzan and the Jewels of Opar by Edgar Rice Burroughs), and directed by Robert F. Hill and Scott Sidney. The first chapter was released on December 1, 1921. 

==Plot== Jane from Queen La of Opar (fictional city)|Opar, taken to that hidden city, and is to be made a sacrifice.  Tarzan rescues her and they escape.  Nikolas Rokoff and William Cecil Clayton, the usurper to Tarzans title of Lord Greystoke, learn that Jane has a map to the city (which contains fabulous riches in exotic jewels), tattooed onto her back.  They kidnap her and attempt to loot the city.  
Tarzan braves many perils, finally rescues Jane, defeats the villains and escapes Las amorous clutches.

==Cast==
* Elmo Lincoln as Tarzan 
* Louise Lorraine as Jane Porter 
* Scott Pembroke as William Cecil Clayton, cousin of Tarzan, usurper to title of Lord Greystoke
* Frank Whitson as Nikolas Rokoff, a villain
* Lillian Worth as Queen La of Opar, a villainess in love with Tarzan
* Charles Inslee as Professor Porter, Janes father 

==Production== The Son of Tarzan inspired Great Western Producing Company to approach Tarzans creator Edgar Rice Burroughs about making another Tarzan serial.  However, the rights for another Tarzan film were still retained by the Weiss brothers Numa Pictures Corporation, the makers of the feature film The Revenge of Tarzan.  When Numa discovered that Great Western had Elmo Lincoln, the first screen Tarzan, signed to play the lead, they agreed to a deal in which Great Western would produce the film while Numa would handle distribution. {{cite book
 |last=Essoe |first=Gabe
 |title=Tarzan of the Movies
 |origyear=1972 |publisher=Citadel Press
 |isbn=978-0-8065-0295-3 |pages=37–44
 }}   The story was based partially on two of the Tarzan novels, The Return of Tarzan and Tarzan and the Jewels of Opar, with the addition of some new material.  {{cite book
 |last=Harmon |first=Jim
 |author2=Donald F. Glut  
 |authorlink=Jim Harmon
 |title=The Great Movie Serials: Their Sound and Fury 
 |origyear=1973 |publisher=Routledge
 |isbn=978-0-7130-0097-9 |pages=123 and 125
 |chapter=6. Jungle "Look Out The Elephants Are Coming!"
 }}   The desert scenes were filmed in Arizona.

===Casting===
This serial marked Elmo Lincolns return to the part of Tarzan, whom he was the first to play, but it was also Lincolns last time as the character.   The serial was advertised as "Censor-proof."  Nevertheless, censorship forced the previously bare-chested Lincoln to cover up and wear an over-the-shoulder-styled costume for this production.   Jane was played by Louise Lorraine, who celebrated her sixteenth birthday during production.   As advertised "Joe Martin, famous screen ape, plays a leading part."   Production started 1 January and finished 13 August 1921.   The serials prologue featured Edgar Rice Burroughs himself. 

===Stunts===
Frank Merrill began doubling Lincoln about half way through the serial.  Lincoln was insured for $150,000 and the insurers were not happy with him doing his own stunts.  Seven years later, Merrill was cast as the Apeman in Tarzan the Mighty.   {{cite book
 |last=Stedman |first=Raymond William
 |title=Serials: Suspense and Drama By Installment
 |origyear=1971 |publisher=University of Oklahoma Press
 |isbn=978-0-8061-0927-5 |pages=56 |chapter=3. At This Theater Next Week
 }} 

==Release==
===Theatrical===
For marketing purposes The Adventures of Tarzan Serial Sales Corporation was formed in New York. The serial sold in half of all available markets without the use of a road man.  Within three months of the completion date it had sold out in most countries world wide.   Despite rumours circulated that the serial was not new material, but just a rehash of footage from previous Tarzan films, The Adventures of Tarzan was a successful film and one of the top four attractions of the year.   The film was reedited and released with sound effects twice—first in 1928, and a second time in 1935.

===Home media=== has not survived.  The version available on DVD is the 1928 ten chapter re-release.

==Critical reception==
The Exhibitors Herald wrote, "Elmo Lincoln as Tarzan is too well known to theater-goers to need further introduction. His red-blooded fights, staged in each episode, will evoke applause from the serial audience." Film Fun Magazine wrote, "There are enough wild animals introduced in each episode to keep the younger generation, which has shown a predilection for the serial form of entertainment, whooping her up.” 

==Influence==
The success of the serial inspired a Broadway show, Tarzan of the Apes, but critics attacked it as fit only for film and unsuitable for the stage. 

==Chapter titles==
# Jungle Romance
# The City of Gold
# The Sun Death
# Stalking Death
# Flames of Hate
# The Ivory Tomb
# The Jungle Trap
# The Tornado
# Fangs of the Lion
# The Simoon
# The Hidden Foe
# Dynamite Trail
# The Jungles Fury
# Flaming Arrows
# The Last Adventure

==Novel==
{{Infobox book
| name             = The Adventures of Tarzan
| image            = 
| author           = Maude Robinson Toombs
| cover_artist     = Jerry Schneider
| series           = Tarzan (book series)
| publisher        = ERBville Press
| pub_date         = 2006 (trade paper) 2008 (hardcover)
| pages            = 158
| isbn             =  
| oclc             = 
| preceded_by      = the Dark Heart of Time (1999)
| followed_by      =   (2011)
| exclude_cover= yes
}}
Originally written as a 15-part serial for newspapers in 1921, it was collected and published as a released as a trade-paperback (ISBN 978-1-4357-4973-3) by ERBville Press in January 2006. The book became available as a hardcover via Lulu.com in 2008.

===Chapters===
# Jungle Romance
# The City of Gold
# The Sun Death
# Stalking Death
# Flames of Hate
# The Ivory Tomb
# The Jungle Trap
# The Tornado
# Fangs of the Lion
# The Simoon
# The Slave Market
# Dynamite Trail
# The Jungles Prey
# The Flaming Arrow
# The Last Adventure

==References==
 

==See also== 
*List of incomplete or partially lost films

==External links==
 
*  

 
  
 

 

 
 
 
 
 
 
 
 
 
 
 