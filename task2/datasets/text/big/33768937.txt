Hit and Runway
{{Infobox film
| name           = Hit and Runway
| image          = Hit and Runway film poster.jpg
| image_size     =
| caption        =
| director       = Christopher Livingston
| producer       =
| writer         = Jaffe Cohen, Christopher Livingston
| narrator       =
| starring       = Michael Parducci Peter Jacobson Judy Prescott
| music          =
| cinematography =
| editing        =
| distributor    = Mirador Films
| released       =  
| runtime        = 110 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Hit and Runway is a 1999 American comedy film directed by Christopher Livingston. It won best screenplay at the  Los Angeles Independent Film Festival.

==Cast==
{| class="wikitable"
|-
! style="background:silver;"| Actor !! style="background:silver;"| Role
|-
| Michael Parducci || Alex Andero
|-
| Peter Jacobson || Elliot Springer
|-
| Judy Prescott || Gwen Colton
|-
| Kerr Smith || Joey Worciuekowski
|-
| Hoyt Richards || Jagger Stevens
|- John Fiore || Frank Anderro
|-
| J. K. Simmons || Ray Tilman
|-
| Teresa DePriest || Lana
|-
| Jonathan Hogan || Bob
|-
| Bill Cohen || Norman Rizzoli
|-
| Rosemary De Angelis || Marie Andero
|-
| Marisa Redanty || Barbara
|-
| John DiResta || Bruno
|-
| Marian Quinn || Eileen
|-
| Stephen Singer || Rabbi Pinchas
|-
| Nicholas Kepros || Jack Springer
|-
| Jaffe Cohen || Comic Actor
|- David Drake || Michael
|-
| Bryan Batt || Carlos
|-
| Adoni Anastassopoulos || Villain
|-
| Cindy Guyer || Model Hostage
|-
| Jo-Jo Lowe || Renee
|-
| Hillel Meltzer || Woody
|-
| Ali Marsh || Geraldine
|-
| Mike Arotsky || Harold the Weightlifter
|-
| Andersen Gabrych || Gay Bartender
|-
| William Severs || Father McWilliams
|-
| David Tumblety || Priest
|-
| Florence Anglin || Aunt Lois
|-
| Bobo Lewis || Cousin Rosalie
|-
| Andrew Polk || Gary Shapiro
|-
| Alicia Minshew || TV Actress
|-
| Julia Novack || Jennifer Andero
|-
| Bobby Dicrisci || Justin Andero
|-
| Elizabeth Muller || Zoe Springer
|-
| Arthur Jolly || Villain 2
|-
| David Filippi || Taxi Driver
|-
| David Juskow || Woody (voice)
|-
| Emilu Klagsbrun || Additional Vocal Talents (voice)
|-
| Radium Cheung || Deli owner
|-
| Susan Cicchino || Girl in Bar (uncredited)
|-
| Marni Lustig || Andrea (uncredited)
|}

==External links==
*  

 
 
 
 
 

 