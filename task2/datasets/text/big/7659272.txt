Anti-hero (film)
{{Infobox Film 
| name = Anti-hero
| image = 
| director = Matt Jaissle
| producer = Isaac Cooper Matt Jaissle
| writer = Matt Jaissle Isaac Cooper
| starring = Isaac Cooper Craig Smith Mark Arndt Dave Bramwell Todd Tjersland Steve Sheppard Sam Walt
| distributor = MetroCity Productions
| released = 1999
| runtime = 75 min.
| language = English
| editing = Matt Jaissle
| awards = 
| gross = 
| budget = < $10,000
}}
Anti-hero is a 1999 action film directed by Matt Jaissle and stars Isaac Cooper, Craig Smith, Mark Arndt, Dave Bramwell, Todd Tjersland, Steve Sheppard and Sam Walt. The film revolves around a second-rate TV actor, Ace Baldwin (Cooper), and his co-star, Rick Beverly (Smith), who together are the main stars of a low-budget, low-quality super-hero television series relatively unknown in the US, but is allegedly "#3 in Mexico."  The film was shot in and around Olympia, Washington.

==Plot== Magneto of X-Men fame), and Baldwin is the obligatory side-kick, Wonder Boy. Baldwin is not happy with his second-rate status in the series, nor in his own life, and struggles to overcome his despair. He becomes involved with Zamphir (Arndt) who has stolen the only sample of an experimental and potent type of synthetic heroin from a drug gang. After Arndt is killed by the gang attempting to recover their drug sample, Baldwin takes the sample himself, intending to sell it before the gang catches up with him. In short order he finds that the gang is on his trail. He must quickly find a way to sell the drug and make his escape before the gangs hitmen (Tjersland and Sheppard) can find him.

Baldwins first clue that the gang has found him, is in a scene is where the hitmen have brought in their star assassin, "Magic Fingers", whose expertise with sniper weapons is legendary. While an episode of Magna-Max is being filmed, the three gang members stake out the location, and, with Baldwin in the crosshairs, just as Magic Fingers squeezes the trigger on his scoped rifle, the director of the show, played by the films director Jaissle in a cameo, unluckily lunges into the path of the bullet and is killed. This then sets off a chase series where Baldwin and Beverly are running through the forest in costume trying to escape being shot to death. Of course, Beverly is not entirely surprised that somebody is upset with his co-star, but is aghast at the level of anger. His question to Baldwin is a classic: "Just who the Hell did you piss off?!"

Because Baldwin knew he was in trouble, just not how close trouble was to him, he had started carrying a pistol, and is able to return fire as he and Beverly flee through the woods. The three heavily-armed hitmen rush ahead and ambush the costumed pair. Unfortunately, Beverly is killed in the ambush, but the enraged Baldwin manages to somehow take out the three professionals, and in a climactic scene he then hunts down and executes a rough justice on the ganglord (Walt).

==References==
 

==External links==
* 
*   at the Zombie Movie Database

 
 


 