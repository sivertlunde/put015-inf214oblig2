Hi, Fidelity
 
 
{{Infobox film
| name = Hi, Fidelity
| image = Hi, Fidelity poster.jpg
| caption = 
| film name = {{Film name| traditional = 出軌的女人
| simplified = 出轨的女人}}
| producer = 
| director = Calvin Poon
| writer = Calvin Poon
| starring =  
| editing = 
| cinematography = 
| studio = 
| distributor = 
| released =  
| runtime = 
| country = Hong Kong
| language = Cantonese
| budget = 
}}
Hi, Fidelity is a 2011 Hong Kong dramatic film written and directed by Calvin Poon. The film stars Pat Ha, Michelle Ye, Carrie Ng and William Chan.

==Cast==
* Pat Ha
* Michelle Ye
* Carrie Ng
* William Chan
* George Lam
* Chapman To
* Lawrence Cheng
* Candice Yu

==Awards and nominations==
===31st Hong Kong Film Awards===
*Nominated - Best Original Song 
*Nominated - Best New Director (Calvin Poon)

==Release==
The film was released in Hong Kong on 24 March 2011. 

==References==
 

==External links==
*  
*  

 
 
 

 