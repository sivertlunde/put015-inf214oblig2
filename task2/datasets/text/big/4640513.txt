The Second Chance
 
{{Infobox Film
| name           = The Second Chance
| image          = SecChance.JPG
| image_size     = 
| caption        = The Second Chance movie poster
| director       = Steve Taylor
| producer       = J. Clarke Gallivan  Coke Sams  Steve Taylor Ben Pearson  Steve Taylor
| narrator       = 
| starring       = Michael W. Smith  Jeff Obafemi Carr  J. Don Ferguson  Lisa Arrindell Anderson  David Alford
| music          = 
| cinematography = 
| editing        = Matthew Sterling
| distributor    = Triumph Releasing Corporation
| released       = February 17, 2006
| runtime        = 102 min.
| country        = United States
| language       = English
| budget         = $1,200,000 
| gross          = $463,542
| preceded_by    = 
| followed_by    = 
}}
 2006 drama film, directed by veteran musician Steve Taylor. The film won Best Feature Film at the Christian WYSIWYG Film Festival.

The film was released in the United States on February 17, 2006 to a limited number of theaters; the widest release was 87 theaters.  As of its close date, March 5, 2006, the film had grossed $463,542. 

==Plot summary==
Ethan Jenkins (Michael W. Smith) is a pastor who enjoys working with his well-to-do congregation.  At the request of his father, Ethan takes an assignment at Second Chance Church, where he meets Jake Sanders (Jeff Obafemi Carr). Jake is a pastor who lives in a completely different world from that of Ethan, and spends much of his time dealing with poverty, drugs, and crime. The two different lifestyles of these pastors cause an inevitable conflict as these two men try to bridge the divide.

==Nominations and awards==
6th Annual Christian WYSIWYG Film Festival
* Winner, Best Feature Film

==Production notes==
Taylor did not intend this film to be considered a Christian film. He told Christianity Today that "One of the reasons weve avoided the tag "Christian film" is because its the kiss of death—its not an apocalyptic thriller or a conversion story. Its a redemption story, set in the world of these two churches, and we wanted to tell an authentic story deep in those settings."  This is the first film in which Michael W. Smith, a contemporary Christian composer, singer, and musician, has acted.

The film was shot mostly on location in Nashville, Tennessee.  The area was mainly in and around East Nashville between 2nd and 4th Avenues. One featured spot of note shows the front facade of the former Pearl Cohn Comprehensive High School at 17th Avenue and Jo Johnston.

==Soundtrack==
{{track listing
| headline = 
| writing_credits = yes
| extra_column = Performer
| collapsed = no
| total_length    = 
| title1          = Movin on Up
| writer1         = Bobby Gillespie, Andrew Innes
| extra1          = Third Day
| length1         = 3:31
| title2          = All in the Serve
| writer2         = Michael W. Smith
| extra2          = Michael W. Smith
| length2         = 2:53
| title3          = Follow Me
| writer3         = 
| extra3          = Andraé Crouch, Michael W. Smith
| length3         = 2:35
| title4          = Refuge (When Its Cold Outside)
| writer4         = Paul Cho, DeVon "Devo" Harris 
| extra4          = John Legend
| length4         = 3:49
| title5          = Nothing But the Blood
| writer5         = Jars of Clay, Robert Lowry, Traditional 
| extra5          = The Blind Boys of Alabama, Jars of Clay
| length5         = 4:11
| title6          = Total Praise
| writer6         = Richard Smallwood
| extra6          = Michael W. Smith
| length6         = 3:39
| title7          = Footwashing
| writer7         = Michael W. Smith
| extra7          = Michael W. Smith
| length7         = 2:03
| title8          = Homeless Child
| writer8         = Ben Harper
| extra8          = The Holmes Brothers
| length8         = 3:45
| title9          = I Surrender All
| writer9         = 
| extra9          = Ruben Studdard
| length9         = 3:56
| title10         = Hang On
| note10          = Pajam Remix
| writer10        = Matt Bronleewe, Wes King, Michael W. Smith 
| extra10         = Michael W. Smith, 21:03
| length10        = 3:59
| title11         = The Last Hallelujah 
| writer11        = Michael W. Smith
| extra11          = Michael W. Smith
| length11        = 3:35
| title12         = Im Glad About It
| writer12        = Fred Hammond, Juanita Wynn
| extra12          = Fred Hammond
| length12        = 3:52
| title13         = Ethan Testifies
| writer13        = Michael W. Smith
| extra13          = Michael W. Smith
| length13        = 1:20
| title14         = The Solid Rock
| writer14        = 
| extra14          = Michael W. Smith
| length14        = 2:52
| title15         = We Must Praise
| writer15        = James Moss
| extra15          = James Moss
| length15        = 5:10
| title16         = On the Rooftop
| writer16        = Michael W. Smith
| extra16          = Michael W. Smith
| length16        = 3:36
}}

==References==
 

==External links==
*  
*  
*  
 

 
 
 
 
 


 