Permissive (film)
Permissive British film released in 1970, directed by Lindsay Shonteff. Written by Jeremy Craig Dryden, it depicts a young girls progress through the rock music groupie subculture of the time.

==Synopsis==
 Forever More. At first Suzy is just one of many girls who follow the groups and make themselves sexually available to musicians and their hangers-on (a type represented by Forever Mores road manager Jimmy, played by Gilbert Wynne). When the band go on tour she is left behind. For some time she lives on the streets with Pogo (Robert DAubigny, credited as "Robert Daubigny"), a gentle hippie drifter who is eventually killed in a road accident.

After the accident Suzy meets Fiona again. She becomes accepted as part of Forever Mores entourage, and develops the glamorous style and hard attitude of an experienced groupie. She makes a play for Lee and ousts Fiona from her status as his old lady. In the final scene, the band are about to leave their hotel when Suzy finds Fiona in the bathroom with her wrists slashed. She walks out, abandoning her former friend.

==Music==
Forever More were a genuine performing band, although the band members play characters other than themselves in the film. Songs from the soundtrack appear on their album Yours – Forever More. Alan Gorrie went on to commercial success as a member of the Average White Band.
 Comus provided the films opening title theme and other incidental music and songs.
 Titus Groan.

== DVD & Blu-ray release ==
 Flipside series on 25 January 2010.  The disc also includes the feature film Bread (directed by Stanley Long, 1971) and the short   Ave You Got a Male Assistant Please Miss? (Graham Jones, Jon Astley, 1973).

== References ==
 
 

== External links ==
*  

 

 
 