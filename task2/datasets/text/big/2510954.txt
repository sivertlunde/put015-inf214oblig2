An Unfinished Life
 
{{Infobox film
| name           = An Unfinished Life
| image          = An Unfinished Life film.jpg
| caption        = Theatrical release poster
| director       = Lasse Hallström
| producer       = Leslie Holleran Alan Ladd, Jr. Joe Roth Bob Weinstein Harvey Weinstein
| based on       =  
| writer         = Virginia Korus Spragg
| starring       = Robert Redford Jennifer Lopez Morgan Freeman
| music          = Deborah Lurie
| cinematography = Oliver Stapleton
| editing        = Andrew Mondshein
| studio         = Revolution Studios The Ladd Company
| distributor    = Miramax Films
| released       =  
| runtime        = 108 minutes
| country        = United States
| language       = English
| budget         = $30 million
| gross          = $18,618,284
}}

An Unfinished Life is a 2005 drama film directed by Swedish director Lasse Hallström, and based on the Mark Spragg novel of the same name. The film stars Robert Redford, Jennifer Lopez, and Morgan Freeman. It is the story of a Wyoming rancher (Redford) who must reconcile his relationship with his daughter-in-law (Lopez) and granddaughter, after they show up unexpectedly at his ranch and ask to stay with him and his disabled best friend and neighbor (Freeman). 

==Plot==
One year ago, a wild bear stole a calf from Mitch (Morgan Freeman) and Einar’s (Robert Redford) ranch. The two friends attempted to save the calf, but the bear viciously attacked Mitch — and because Einar was drunk, he failed to save him from serious injury. The bear escaped into the mountains.

A year later, Mitch’s wounds still cause him constant pain. Einar cares for Mitch daily, giving him morphine injections, food and friendship. He leans his guilt on emotional crutches, while Mitch struggles to walk with a cane. The bear later returns to forage for food in town — and around the same time, Einar’s long-lost daughter-in-law Jean (Jennifer Lopez) shows up on his doorstep. 

Sheriff Crane Curtis (Josh Lucas) captures the bear and displays him in the town zoo. Jean and her daughter Griff (Becca Gardner) move in with Einar and Mitch. Einar’s son, Griffin, had moved away and married Jean years ago, causing a rift in the family that snapped when Griffin died in a car accident. Tension mounts between Einar and Jean because both are still grieving for Griffin. 

Since Griffin died, Jean has fallen into a series of abusive relationships. She moved in with Einar to escape her latest abusive boyfriend, Gary (Damian Lewis), but like the bear, her past will return to haunt her. Jean slowly falls in love with Sheriff Curtis, but little Griff can’t open up to him or trust him because of Jean’s bad experiences. 

Jean starts working at a local coffee shop. There she befriends Nina, another waitress (Camryn Manheim), who helps her understand Einar’s gruff ways and dark past.

Tensions in the family continue to build. One night, Gary arrives to harass Jean and Griff. He and Einar have an explosive confrontation that pushes Einar to the brink. 

Einar returns to the ranch and demands that Jean tells him how Griffin died. Jean says they flipped a coin to determine who would drive — she lost. At 3 a.m., the two tired souls set out for the last leg of their trip. Jean fell asleep at the wheel. The car flipped six times. Griffin died, but Jean survived and discovered she was pregnant with Griff. When he learns the truth about why his son was ripped from him too soon, Einar says they’ll have to talk about Jean moving out. Jean says she’s through talking. The next morning she takes Griff with her and leaves. Griff, who loves her grandfather as the first strong male figure in her life, runs back to the ranch alone. Einar and Mitch teach her to be a real Wyoming cowgirl. 

The movie ends with Mitch insisting that Einar set the bear who handicapped him free, although the plan does not go off without a hitch. Einar lands in the hospital, where he and Jean attempt to reconcile. Einar invites Jean to come back and live with him and Griff. Mitch survives a peaceful confrontation with the bear from his past. It flees to the mountains where it belongs.  

Gary makes one last attempt to drag Jean and Griff back “home” — this ends with Einar beating him up and tossing him on a bus out of town. In the final scene, Einar affectionately talks with one of his cats — who throughout the whole story he coldly ignored. Griff then invites Sheriff Curtis for lunch. All is well as Mitch narrates the last seconds of the story, describing to Einar, his dreams of flying above the earth and coming to understand things about life.

==Cast==
* Robert Redford as Einar Gilkyson
* Jennifer Lopez as Jean Gilkyson
* Morgan Freeman as Mitch Bradley
* Josh Lucas as Sheriff Crane Curtis
* Lynda Boyd as Kitty
* Damian Lewis as Gary Winston
* Camryn Manheim as Nina
* Becca Gardner as Griff Gilkyson
* Trevor Moss as Griffin Gilkyson (in flashbacks)

==Production==
While set in Wyoming, the movie was actually filmed in the Canadian towns of Ashcroft, British Columbia|Ashcroft, Savona, British Columbia|Savona, and Kamloops, British Columbia.  

==Release==
===Critical reception===
Reviews of the film were mixed, with many critics praising the acting rapport of Redford, Freeman and Lopez while panning the predictability of the well-trodden storyline. Movie review aggregate site Rotten Tomatoes shows that 53% of critics gave the film a positive review, giving it a "rotten" score. 

===Box office=== wider release, the film opened at number 11 with $2,052,066.  Despite its $30 million budget, the film made a meager $18,618,284 worldwide by the time of its closing.

===Awards===
The film won the best makeup award from the Canadian Network of Makeup Artists (Jayne Dancose), and it won the Genesis Award as best feature for 2005.

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 