The Ginger Bread Boy
{{Infobox Hollywood cartoon|
| cartoon_name = The Ginger Bread Boy
| series = Oswald the Lucky Rabbit
| image = OswaldGingerbread1934.jpg
| caption = Screenshot of Oswald, The boy beagle, and the baby dog.  Bill Nolan
| animator = Fred Avery Ray Abrams Cecil Surry Jack Carr Merle Gilson Victor McLeod
| voice_actor = 
| musician = James Dietrich
| producer = Walter Lantz
| distributor = Universal Pictures
| release_date = April 16, 1934
| color_process = Black and white
| runtime = 6 minutes English
| preceded_by = Wolf! Wolf!
| followed_by = Goldielocks and the Three Bears
}}
 one published in a magazine in 1875.

==Plot==
In a living room, Oswald and the boy beagle are listening to a radio, awaiting a program. Suddenly, they are interrupted by a baby dog from another room who is sucking a thumb very loudly. Not wanting to be bothered by the noise, the boy beagle approaches the small puppy and puts a boxing glove on the latters hand. The two friends resume their waiting at the radio, only to be disturbed again by the bawling baby dog. The boy beagle then comes back to help the little mutt burp as well as giving a diaper change.

While Oswald and the boy beagle are still anticipating at the living room, the baby dog comes to them, craving for chocolate pudding. This time, Oswald stands up and takes the little mutt to the dining room. The rabbit then provides a serving spoon and a bowl with the dessert. When the baby dog gets busy eating, Oswald and the boy beagle were finally listening to their sound system in peace. Their awaited program is a fairy tale about a live ginger bread boy.

Once there was a spinster who desperately wanted to have children but simply couldnt obtain any. Thus she decided to create a boy out of ginger bread. After baking one in the oven, it came to life somehow.

While the ginger bread boy is still on the table, getting used to his new life, the household cat sees and finds him delicious. The cat began running after the humanoid biscuit, much to the spinsters dismay.

A clever dodger, the ginger bread boy was unfazed by the cats ferocity and is able to keep himself at a safe distances. With a little help from the spinster, he eventually drove the fearsome feline away from household, resulting a favorable outcome for both him and his surrogate mother.

Back in the living room, Oswald and the boy beagle were enjoying themselves, listening to the rest of their radio program. Just then, the baby dog comes in again, wanting to have a hobby after such a meal. Still having a large supply of chocolate pudding, the small puppy decides to splatter it on the faces of the two friends, thus leading to a friendly food fight.

==Directing==
Unusual to most shorts of the series, the person who directed the cartoon is not credited, although some believe it was Fred "Tex" Avery. The reason to this remains undisclosed. {{cite web
|url=http://lantz.goldenagecartoons.com/1934.html
|title=The Walter Lantz Cartune Encyclopedia: 1934
|accessdate=2011-07-27
|publisher=The Walter Lantz Cartune Encyclopedia
}} 

==Differences from the original story==
In the original version of the fairy tale, the ginger bread boy ends up eaten by the animal chasing him, namely a fox. In this adaptation, he subdues his predator and remains alive. Also in the original story, the ginger bread would always try to run away from anyone (including his creator) just for pleasure. In the film, he cherishes his human mother and would not run off.

==See also==
*Oswald the Lucky Rabbit filmography

==References==
 

==External links==
*  at the Big Cartoon Database

 

 
 
 
 
 
 
 
 
 