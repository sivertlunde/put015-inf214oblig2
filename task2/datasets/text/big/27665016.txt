Mystery Submarine (1963 film)
{{Infobox film
| name           = Mystery Submarine
| image_size     = 
| image	         = Mystery Submarine FilmPoster.jpeg
| caption        = 
| director       = C.M. Pennington-Richards
| producer       = Bertram Ostrer
| writer         = 
| narrator       = 
| starring       = Edward Judd James Robertson Justice Laurence Payne
| music          = Clifton Parker
| cinematography = Stanley Pavey
| editing        = Bill Lewthwaite
| studio         = 
| distributor    = 
| released       = 1963
| runtime        = 92 mins
| country        = United Kingdom
| language       = English
}} British war film directed by C.M. Pennington-Richards and starring Edward Judd, James Robertson Justice and Laurence Payne.  A captured German submarine is used by the Royal Navy to trick a German force aiming to intercept a supply convoy. The film is based on a play by Jon Manchip White.

==Plot==

U153 is damaged during air attack in the Atlantic, and it’s crew abandon to escape Chlorine gas leakage due to  broken battery cells. Her commanding officer is overcome by fumes before he can jettison the ship’s papers. Aware of the intelligence windfall that this represents, the submarine is taken by a British prize crew to be examined and inspected (in much the same manner that befell the real German U-boat later renamed HMS Graph ).

It is not long before British intelligence suggest a new use for the submarine as a Trojan Horse. A picked crew of volunteers led by Commander Tarlton (Edward Judd) take the U153 back to war, to intercept and disable a German Wolf-pack; in this they succeed, even sinking the Wolf-pack leader in their subsequent escape. 

Her mission accomplished the U153 is attacked and sunk by a British Frigate whose crew is oblivious to the submarine’s mission or identity. Commander Tarlton orders his men to abandon ship, getting his crew off intact before she goes down. Their rescuers are astonished to learn that not only are the men they recover from the sea all British, but by attacking they have just sunk one of ‘His Majesty’s submarines…’

==Cast==
* Edward Judd - Lieutenant Commander Tarlton
* James Robertson Justice - Rear Admiral Rainbird
* Laurence Payne - Lieutenant Seaton
* Joachim Fuchsberger - Commander Scheffler
* Arthur OSullivan - Mike Fitzgerald
* Albert Lieven - Captain Neymarck
* Robert Flemyng - Vice Admiral Sir James Carver Richard Carpenter - Lieutenant Haskins
* Richard Thorp - Lieutenant Chatterton
* Jeremy Hawk - Admiral Saintsbury Robert Brown - Coxswain Drage
* Frederick Jaeger - Lieutenant Hence
* George Mikell - Lieutenant Remer
* Peter Myers - Telegraphist Packshaw Leslie Randall - Leading Seaman Donnithorne
* Ewen Solon - Lieutenant Commander Kirklees
* Nigel Green - Chief Lovejoy 

==References==
 

==External links==
* 

 

 
 
 
 
 
 


 