Animal (1977 film)
{{Infobox film
| name = LAnimal
| image = 
| caption = 
| director = Claude Zidi
| producer = Bernard Artigues Christian Fechner René Malor
| writer = Michel Audiard Dominique Fabre Claude Zidi
| starring = Jean-Paul Belmondo Raquel Welch Jane Birkin Dany Saval Johnny Hallyday Claude Chabrol Josiane Balasko
| music = Vladimir Cosma
| cinematography = Claude Renoir
| editing = Monique Isnardon Robert Isnardon
| distributor = Cerito Films
| released = 5 October 1977
| runtime = 100 minutes
| country = France
| language = French
| gross = $20,525,628  3,157,789 admissions (France) 
}}
LAnimal (1977 in film|1977) is an action-comedy film directed by Claude Zidi and starring Jean-Paul Belmondo and Raquel Welch

==Plot==
Mike (Jean-Paul Belmondo) is a stuntman who works with his girlfriend Jane (Raquel Welch). On their wedding day Mike and Jane are forced by producers to do a stunt for a film they are working on. Mike, annoyed doesnt look on the road and crashes the car causing them to end up in a hospital. After they come out Jane doesnt want to talk to Mike so he decides to get her a job in a film in which he is a stunt double for his double who is a star in action pictures but is in fact a wimp.

==Cast==

*Jean-Paul Belmondo as Mike / Bruno
*Raquel Welch as Jane
*Charles Gérard as Hyacinthe
*Julien Guiomar as Fechner
*Aldo Maccione as Sergio Campanese
*Dany Saval as Doris
*Raymond Gérôme as Count of Saint-Prix
*Henri Génès : as Camille, café owner
*Jane Birkin as The Female Star
*Johnny Hallyday as The Male Star
*Claude Chabrol as The Director Mario David as Santos
*Jacques Alric 
*Henri Attal 
*Josiane Balasko
*Maurice Auzel
*Maurice Bénichou 
*Edouard Bergara 
*Paul Bisciglia 
*Richard Bohringer
*Anne-Marie Coffinet 
*Raphaële Devins
*Isabelle Duby 
*Martine Ferrière
*Jean-Jacques
*Jean-Jacques Moreau  
*Didier Flamand 
*Gilles Kohler
*Yves Mourousi 
*Xavier Saint-Macary 
*Agathe Vannier

==References==

 

==External links==
 
*  

 

 
 
 
 
 
 
 
 

 