Báječná léta pod psa
{{Infobox Film
| name           = The Blissful Years of Lousy Living / Báječná Léta pod Psa
| image          =
| image_size     = 
| caption        = 
| director       = Petr Nikolaev
| producer       =  Jan Novák
| narrator       = 
| starring       = Libuše Šafránková Ondřej Vetchý Milan Dvořák Ivan Hlas
| cinematography = 
| editing        = 
| distributor    = Space Films
| released       = April 3, 1997
| runtime        = 109 minutes
| country        = Czech Republic
| language       = Czech
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 1997 movie, adapted from the book of the same title by Michal Viewegh. The film is directed by Petr Nikolaev.

== Plot ==
Its the beginning of the 1960s in Czechoslovakia where the heavy impact of the Soviet Union influences all the happenings in the country. A young mother, played by Libuše Šafránková and a father, played by Ondřej Vetchý, are expecting their first baby. They have already agreed on a name - Quido. The baby is due to be born on August 5, but because nothing happens as planned, Quido is born earlier, during the performance Waiting for Godot written by Samuel Beckett. This might have influenced his life because since that moment he seems to be a genius boy. Of course his intelligence makes him trouble during his teenage years at school and also during his attempts to get a girlfriend. Eventually he manages to pick the right one.

For Quido everything suddenly looks wonderful, when another disaster comes. His father starts to suffer from persecution mania after he has been degraded from his job and asked to come to a police interrogation. He changes completely and thinks that the situation becomes unbearable. Thats why he is making himself a coffin. Quidos mother feels desperate and comes with an idea which could save her husband. She wants Quido to have his own baby so that her husband could see the world from a better perspective again.

Eventually the whole situation is saved not only by Quidos child, but mainly by The Velvet Revolution in 1989.
As a result Quidos father starts to feel much better. The whole atmosphere is then interrupted by the fact that it becomes more and more obvious that the situation hasnt changed that much.

== Cast list ==
*Libuše Šafránková as Quidos mother
*Ondřej Vetchý as Quidos father
*Jan Zahálka as Quido - young boy
*Jakub Wehrenberg as Quido - young man
*Klára Botková as Jaruška - young girl
*Jitka Ježková as Jaruška - young woman
*Vladimír Javorský as Šperk
*Vilma Cibulková as Šperková
*Miriam Kantorková as grandmother Věra
*Vladimír Dlouhý (actor)|Vladimír Dlouhý as Zvára
*Květa Fialová as grandmother Líba
*Stanislav Zindulka as grandfather Josef
*Otakar Brousek st. as dědeček Jiří
*Viktor Preiss as playwright
*Jiří Schmitzer as Dr. Liehr
*Alice Bendová as Zita
*Miloň Čepelka as gateman

== References ==
*  

==External links==
*  

 
 
 
 
 
 
 
 
 