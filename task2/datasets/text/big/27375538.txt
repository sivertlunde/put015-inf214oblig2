Life Is Cheap... But Toilet Paper Is Expensive
{{Infobox film
| name           = Life Is Cheap... But Toilet Paper Is Expensive
| image          = Life_Is_Cheap..._But_Toilet_Paper_Is_Expensive_Poster.jpg
| image_size     = 
| caption        = 
| director       = Wayne Wang
| writer         = Spencer Nakasako Amir Mokri Wayne Wang
| narrator       = 
| starring       = Cheng Wan Kin
| music          = Mark Adler  
| cinematography = Amir Mokri
| producer = Winnie Mokri
| editing        = Sandy Nervig
| studio         = 
| distributor    = Lions Gate Entertainment Silverlight Entertainment
| released       = August 24, 1990
| runtime        = 83 min.
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}} John Chan. It won an award at the 1990 Rotterdam International Film Festival. 

The film was the subject of controversy when it originally received an X rating from the Motion Picture Association of America, the distributor, Silverlight Entertainment, chose to release it without this rating and with a self-anointed adults-only A rating.  On their TV show for the week of August 13-17, 1990, the late film critics Gene Siskel and Roger Ebert praised the decision to apply the A rating since it was a concept they had often discussed on "At the Movies" in the context of harshly criticizing the MPAAs standards of forcing serious films aimed at adult audiences to either undergo damaging edits to receive R ratings or be locked out of most theatrical and advertising outlets. Both critics, however, also said the film itself was not very good.

==Overview==
A man is hired, by people he believes to be gangsters, to deliver a briefcase from America to Hong Kong.

==Cast==
*Cheng Wan Kin as Duck Killer John Chan as The Son In Law
*Kwan-Min Cheng as Uncle Cheng
*Allen Fong as Taxi Driver
*Rocky Wing Cheung Ho as Punk #2
*Angela Yu Chien as Blue Velvet

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 


 
 