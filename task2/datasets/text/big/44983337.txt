She's Lost Control (film)
{{Multiple issues|
 
 
}}
{{Infobox film
| name           = Shes Lost Control
| image          = Shes_Lost_Control_poster.jpg
| alt            =
| caption        = Film poster
| director       = Anja Marquardt
| producer       = Anja Marquardt Kiara C. Jones Mollye Asher Oren Moverman
| writer         = Anja Marquardt
| starring       = Brooke Bloom Marc Menchaca Dennis Boutsikaris Laila Robins Robert Longstreet
| music          = Simon Taufique
| cinematography = Zachary Galler
| editing        = Anja Marquardt Nick Carew
| studio         = SLC Film Rotor Film
| distributor    = Monument Releasing
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| box office       = $3,740 

}}
Shes Lost Control is a 2014 American dark drama film written and directed by Anja Marquardt in her directorial debut. The film stars Brooke Bloom as Ronah, a sexual surrogate whose professional intimacy with her male clients has begun to take over her personal life. The film had its world premiere at the 2014 Berlin International Film Festival    in February where it won the Forum sections top prize, the CICAE International Confederation of Art Cinemas Award, being called a "masterpiece" by the jury.    Just one month later, in March 2014, Shes Lost Control had its North American premiere at the SXSW Film Festival, followed by a New York premiere at New Directors/New Films at Film Society of Lincoln Center and MoMa, where it was described as a "stylish, deeply unnerving, and profound film on an intangible modern issue."   

The film was released theatrically in the U.S. by Monument Releasing on March 20, 2015, and will be released theatrically in Germany May 14, 2015. 

==Plot==
Fiercely independent, Ronah works as a sexual surrogate in New York City, teaching her clients the very thing they fear most - to be intimate. Her life unravels when she starts working with a volatile new client, Johnny, blurring the thin line between professional and personal intimacy in the modern world.

==Cast==
* Brooke Bloom as Ronah
* Marc Menchaca as Johnny
* Dennis Boutsikaris as Dr. Alan Cassidy
* Laila Robins as Irene
* Tobias Segal as Christopher
* Roxanne Day as Claire
* Ryan Homchick as Andro
* Robert Longstreet as C.T.

==Production==
Initial production funds for Shes Lost Control were raised on the crowdfunding platform Kickstarter, earning a total of $50,081 by November 9, 2012.    The film was shot entirely in New York City, principal photography commenced June 28, 2013.

==Reception==
Shes Lost Control received positive reviews from critics. David Rooney in his review for The Hollywood Reporter described the film as "chilling" and "austerely elegant", "acted with naturalistic intensity", "arrestingly lit and shot, exhibiting a sharp eye for expressive compositions."  Marc Adams of Screen International said in his review that theres "much to admire in Marquardt’s control and precision as well as a well sustained lead performance by lead actress Brooke Bloom (...) Brooke Bloom is impressive... while Menchaca is equally impressive." Adams called the film "astutely cold and compelling", "unglossy and taut."  Eric Kohn of Indiewire called the film "a fascinating debut" and "engrossing" and graded the film A- by saying that "Shes Lost Control strikes a fascinating mood between slow-building angst and cold remove not unlike the Joy Division song that provides its title."  Steve Dollar of The Wall Street Journal featured the film saying its "wintry visual design and tone of isolation feels akin to dystopian science-fiction or a horror film, the kind in which high-rise apartment buildings leak paranoia. Against the chill, Ms. Bloom performs with flesh-and-blood urgency."  In her review for the LA Times, Katie Walsh finds that the "cinematic style and themes of urban alienation reflect almost a 1970s thriller quality, though the execution is decidedly modern. "Shes Lost Control" is a quiet triumph, a true herald of a distinctive and necessary voice in cinema." 

==Accolades==
{| class="wikitable "
|- style="background:#ccc; text-align:center;"
|+ List of accolades received by Shes Lost Control
! Year
! Award
! Category
! Recipients
! Result
|- 2014
| Berlin International Film Festival
| CICAE Award
| Anja Marquardt
|  
|-
| Best First Feature
| Anja Marquardt
|  
|-
| SXSW Film Festival
| Gamechanger Award
| Anja Marquardt
|  
|-
| Nantucket Film Festival
| Adrienne Shelly Award
| Anja Marquardt
|  
|-
| Thessaloniki International Film Festival
| Best Lead Actress
| Brooke Bloom
|  
|-
| Jeonju International Film Festival
| Best International Feature Film
| Anja Marquardt
|  
|-
| International Festival of Independent Cinema Off Plus Camera
| Best Feature Film
| Anja Marquardt
|  
|-
| Vladivostok International Film Festival
| Best Feature Film
| Anja Marquardt
|  
|-
| Festival du nouveau cinéma
| Best Feature Film
| Anja Marquardt
|  
|- 2015
| Créteil International Womens Film Festival
| Best Feature Film
| Anja Marquardt
|  
|- 30th Independent Independent Spirit Awards Best First Feature
| Anja Marquardt   Kiara C. Jones   Mollye Asher
|  
|- Best First Screenplay
| Anja Marquardt
|  
|-
|}

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 