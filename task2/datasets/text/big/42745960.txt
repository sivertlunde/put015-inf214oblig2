The Adventuress from the Floor Above
{{Infobox film
| name =The Adventuress from the Floor Above
| image =
| image_size =
| caption =
| director = Raffaello Matarazzo
| producer = Francesco Curato   Giorgio Genesi
| writer =  Edoardo Anton     Riccardo Freda    Margherita Maglione  Vittorio De Sica  Raffaello Matarazzo 
| narrator =
| starring = Vittorio De Sica   Clara Calamai   Giuditta Rissone   Olga Vittoria Gentilli 
| music = Giuseppe Anepeta     
| cinematography = Tino Santoni 
| editing = Riccardo Freda  
| studio =    Artisti Associati
| distributor =  Artisti Associati
| released = 21 October 1941 
| runtime = 80 minutes
| country = Italy Italian 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
The Adventuress from the Floor Above (Italian:Lavventuriera del piano di sopra) is a 1941 Italian comedy film directed by Raffaello Matarazzo and starring Vittorio De Sica, Clara Calamai and Giuditta Rissone. It was made at the Palatino Studios in Rome. The film was part of the popular White Telephone genre of comedies. 

==Cast==
*  Vittorio De Sica as Fabrizio Marchini  
* Clara Calamai as Biancamaria Rossi 
* Giuditta Rissone as Clara Marchini  
* Olga Vittoria Gentilli as La madre di biancamaria  
* Camillo Pilotto as Rossi  
* Carlo Campanini as Arturo  
* Ernesto Almirante as Il padre de Biancamaria 
* Giselda Gasperini as Lucrezia - la cameriera di Arturo  
* Jucci Kellerman as La cameriera 
* Dina Romano as Matilde -

== References ==
 

== Bibliography ==
* Moliterno, Gino. Historical Dictionary of Italian Cinema. Scarecrow Press, 2008.

== External links ==
* 

 
 
 
 
 
 
 

 