The Venetian (film)
 
{{Infobox film
| name           = The Venetian
| image          = 
| caption        = 
| director       = Ingmar Bergman
| producer       = Henrik Dyfverman
| writer         = Bertil Bodén Giacomo Oreglia
| starring       = Maud Hansson
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 21 February 1958
| runtime        = 56 minutes
| country        = Sweden
| language       = Swedish
| budget         = 
}}

The Venetian ( ) is a 1958 Swedish television drama film directed by Ingmar Bergman.   

==Cast==
* Maud Hansson – Nena, servant-girl
* Sture Lagerwall – Bernardo
* Gunnel Lindblom – Valeria
* Helena Reuterblad – Oria, Valerias servant-girl
* Eva Stiberg – Angela
* Folke Sundquist – Julio

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 