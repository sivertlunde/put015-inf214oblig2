Emanuelle Escapes from Hell
{{Infobox film
| name           = Emanuelle Escapes from Hell
| image          = 
| caption        = 
| director       = Bruno Mattei
| producer       = 
| writer         = Claudio Fragasso
| starring       = Laura Gemser
| music          =  
| cinematography =  
| editing        = 
| distributor    = 
| released       = 1983
| runtime        = 
| country        = Italy
| language       = Italian
| budget         = 
| preceded_by    = 
| followed_by    = 
}} Gabriele Tinti, Carlo De Mejo, Lorraine De Selle, and Franca Stoppi.  The film was written and co-directed by Claudio Fragasso (a longtime Mattei collaborator), and was shot in 1982 with a mostly French crew.  The film has achieved a certain level of cult status recently due to its exploitive nature, extreme violence, and cheesy dialog.

== Plot ==
Emanuelle is sent to a violent womens prison. While she is in prison, she comes into confrontation with the "top dog" inmate Albina, ending in a series of fights. Albina gets the worse of it, including a broken arm, a knife in her leg, and her wig pulled off.
Following a series of cat fights and arguments the womens lives are interrupted by the arrival of four male death row inmates led by "Crazy Boy" Henderson, who break into the prison. The male convicts proceed to rape, mutilate, and torture the female inmates (involving a sick game of Russian roulette), and executions.  One convict is killed when a SWAT team attempts to invade the prison.  Another is killed by a female inmate who hides a razor blade inside her vagina before enticing him to have his way with her.  Henderson and the remaining male con attempt to break out using the warden, Emanuelle, and a wounded sheriff as human shields.  After a gory finale, Emanuelle and the sheriff are the only characters left alive, and the sheriff promises to reopen her case. 

Although the film contains scenes of violence, these are portrayed in an over the top cartoonish manner, something Mattei did regularly with his other films throughout the 1980s.

== Other information ==
* Filmed back to back with Violence in a Womens Prison (1982) A.K.A. Caged Women (1982). 
* Shot on a budget of $60,000.  Gabriele Tinti) were married in real life at the time.  Tintis voice for the English version was supplied by Ted Rusoff, who was married to actress Carolyn De Fonseca.  Fonseca supplied the voice of Albina, the antagonist during the first third of the film. 
* The film was partially financed by a French Pantyhose company, which is why the female convicts seldom remove theirs.
* The last film in the hugely popular Black Emanuelle series.

== References ==
 

* Palmerini, Luca, Gaetano Mistretta (1996), Spaghetti Nightmares, Key West: Fantasma Books. pp.&nbsp;55. ISBN 0-9634982-7-4
* Internet Movie Database | http://www.imdb.com/title/tt0085250/

== External links ==
* 

 
 
 
 
 
 


 
 