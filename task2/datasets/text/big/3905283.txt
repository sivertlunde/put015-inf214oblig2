Zarak
 
 
{{Infobox film
| name           = Zarak
| image          = Zarak (movie poster).jpg
| image size     =
| caption        = Original film poster Terence Young
| producer       = Irving Allen Albert R. Broccoli
| writer         = Richard Maibaum
| narrator       = Michael Wilding Anita Ekberg
| music          = William Alwyn
| cinematography = Ted Moore
| editing        = Clarence Kolster
| distributor    = Columbia Pictures
| released       = 10 January 1957 (World Premiere, London)
| runtime        = 99 min.
| country        = UK
| language       = English
| budget         =
| gross = $1.4 million (US rentals) 
| preceded by    =
| followed by    =
}}
:For other meanings, see Zarak (disambiguation). Terence Young Northwest Frontier Michael Wilding, Anita Ekberg, and featured Patrick McGoohan in a supporting role.

==Plot==
Zarak Khan (Mature) is the son of a chief who is caught embracing one his fathers wives Salma (Ekberg). Zaraks father sentenced both to torture and death but they are saved by an Imam (Finlay Currie). The exiled Zarak becomes a bandit chief and an enemy of the British Empire.

==Production== Doctor Zhivago. From Russia, with Love, and Goldfinger (novel)|Goldfinger. Similarly, the Director, Terence Young and the Producer, Albert R. Broccoli went on to create the Bond movies.

The film version was announced in 1953. The producers initially wanted Errol Flynn for the lead but Victor Mature was cast instead. WARWICK ACQUIRES BEVAN SPY NOVEL: Irving Allen Plans Production of Zarak Khan – Seeking Errol Flynn for Title Role
By THOMAS M. PRYORSpecial to THE NEW YORK TIMES.. New York Times (1923–Current file)   14 May 1953: 33 
 Bob Simmons who performed and doubled several stars in the film noted that Victor Mature refused to ride a horse. When his stunt double Jack Keely was killed in a horse accident on the set Mature insisted on personally paying for his funeral. 

Patrick McGoohan portrays Moor Larkin, an Adjutant to Michael Wildings character who has a penchant for billiards, as well as offering sensible, albeit ignored, advice. This role was commented on in the British cinema magazine, Picturegoer. The critic Margaret Hinxman made Patrick McGoohan her "Talent Spot". She assured readers that this new face would be "really something", given a "half-decent" part.  She completely slated the film, however, describing it as "absurd".

The popular chanteuse Yana sang her hit song Climb Up the Wall in the film. 

Studio work was done at Elstree. "These Are the Facts", Kinematograph Weekly, 31 May 1956 p 14 

The original film poster was criticised by the House of Lords for "bordering on the obscene" and banned in the United Kingdom. 

The action sequences reappeared in John Gillings The Bandit of Zhobe (1958) and The Brigand of Kandahar (1965).

==Soundtrack==
Climb Up the Wall  
Music by Auyar Hosseini  
Lyrics by Norman Gimbel 
Sung by Yana

==The real Zarak Khan==
The book, written by A.J. Bevan, contained a foreword by Field Marshal William Slim. According to Bevan, the real Zarak Khan was an Afghan who spent most of his life fighting the British in the northwest frontier in the 1920s and 1930s. Among his crimes was the murder of a holy man. He eventually gave himself up and was sentenced to life imprisonment in the Andaman Islands. However when the Japanese occupied the islands he stayed in his cell.

Khan was eventually given a suspended sentence and decided to work for the British in Burma. In 1943 he was leading a patrol when its British officer was killed in an ambush. He watched another British patrol be attacked by the Japanese and sent messengers to summon a Gurkha force. To stop the Japanese from escaping with their prisoners before the Gurkhas arrived, he attacked them single-handed, and killed or wounded six soldiers before being overpowered. He refused to be beheaded and insisted on being flayed alive to buy time to enable the Gurkhas to arrive. 

Producer Irving Allen decided to make a fictional account set in the 19th Century. 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 