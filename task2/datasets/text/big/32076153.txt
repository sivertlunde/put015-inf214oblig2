Rakta Bandhan
{{Infobox film
 | name = Rakta Bandhan
 | image = 
 | caption = DVD Cover
 | director = Rajat Rakshit
 | producer = Tarachand Barjatya
 | writer = 
 | dialogue = 
 | starring = Mithun Chakraborty Rati Agnihotri Surinder Kaur Jagdeep
 | music = Usha Khanna
 | lyrics = 
 | associate director = 
 | art director = 
 | choreographer = 
 | released = June 22, 1984
 | runtime = 135 min.
 | language = Hindi Rs 2 Crores
 | preceded_by = 
 | followed_by = 
 }}
 1984 Hindi Indian feature directed by Rajat Rakshit, starring Mithun Chakraborty, Rati Agnihotri, Surinder Kaur, Dina Pathak and Jagdeep. 

==Plot==
Rakta Bandhan is a family drama/action film starring Mithun Chakraborty in a double role, well supported by Rati Agnihotri, Surinder Kaur, Dina Pathak and Jagdeep. He plays twin brothers Chandan and Trishul Singh who are separated at a young age and grow up in very different circumstances. Chandan grows up to become a simpleton villager and Trishul becomes a dacoit. How the brothers lives are intertwined and how they are reunited forms the plot of the film.

==Cast==
*Mithun Chakraborty
*Rati Agnihotri
*Surinder Kaur
*Jagdeep
*leela mishara

==References==
 
*http://ibosnetwork.com/asp/filmbodetails.asp?id=Rakta+Bandhan

==External links==
*  

 
 
 

 