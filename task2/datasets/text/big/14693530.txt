Last Words (film)
{{Infobox film
| name           = Last Words
| image          = last words.jpg
| image_size     =
| caption        =
| director       = Werner Herzog
| producer       = Werner Herzog
| writer         = Werner Herzog
| cinematography = Thomas Mauch
| editing        = Beate Mainka-Jellinghaus
| distributor    =
| released       = 1968
| runtime        = 13 min.
| country        = West Germany Greek
| budget         =
| preceded_by    =
| followed_by    =
}}
 Signs of Life, and edited in one day. {{cite book
  | last = Herzog
  | first = Werner
  | title = Herzog on Herzog
  | publisher = Faber and Faber
  | date = 2001
  | isbn = 0-571-20708-1 }} 

The film tells a story of the last man to leave the abandoned island of Spinalonga, which had been used as a leper colony. The man refused to leave, and so was forcibly removed. He now lives in Crete, where he plays the lyre at nights in a bar, and refuses to speak. The films narrative style is very unconventional, with most characters speaking their lines several times repeatedly in long takes. The man from the island has the most spoken lines of any character, as he repeatedly explains that he refuses to speak, even a single word. 

== Cast (uncredited)==
* Antonis Papadakis : the last leper
* Lefteris Daskalakis : the bouzouki player

==References==
 

== External links ==
*  

 
 

 
 
 
 