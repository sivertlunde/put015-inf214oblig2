Don't Bother to Knock (1961 film)
 
{{Infobox film
| name           = Dont Bother to Knock
| image          = 
| image_size     = 
| caption        = 
| director       = Cyril Frankel Frank Godwin
| based on       =  
| writer         = Denis Cannan Frederic Gotfurt Frederic Raphael
| narrator       = 
| starring       = Richard Todd Nicole Maurey Elke Sommer John Le Mesurier
| music          = Elisabeth Lutyens
| cinematography = Geoffrey Unsworth
| editing        = Anne V. Coates
| distributor    = Associated British Picture Corporation|Warner-Pathé Distributors (UK)
| released       = 29 May 1961 (London)
| runtime        = 89 minutes (UK)
| country        = United Kingdom
| language       = English
| budget         = 
}}
 British comedy film directed by Cyril Frankel and starring Richard Todd, Nicole Maurey, Elke Sommer and John Le Mesurier.

==Synopsis==
Bill Ferguson (Richard Todd), a Scottish romantically active travel agent gives several woman the keys to his flat, and ends up trying to prevent them from meeting each other. 

==Cast==
* Richard Todd as Bill Ferguson
* Nicole Maurey as Lucille
* Elke Sommer as Ingrid
* June Thorburn as Stella
* Rik Battaglia as Gulio
* Judith Anderson as Maggie
* Dawn Beret as Harry
* Scott Finch as Perry
* Eleanor Summerfield as Mother
* John Le Mesurier as Father
* Colin Gordon as Rolsom
* Kenneth Fortescue as Ian Ronald Fraser as Fred Tommy Duggan as Al
* Michael Shepley as Colonel
* Joan Sterndale-Bennett as Spinster

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 


 
 