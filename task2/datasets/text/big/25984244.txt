Sehnsucht 202
{{Infobox film
| name           = Arabian Love
| image          = 
| image size     = 
| caption        = 
| director       = Max Neufeld
| producer       = Arnold Pressburger Gregor Rabinovitch
| writer         = Emeric Pressburger Karl Farkas Irma von Cube
| narrator       = 
| starring       = Magda Schneider Luise Rainer
| music          = Richard Fall
| cinematography = Otto Kanturek Anton Pucher
| editing        = Else Baum UFA
| released       =  
| runtime        = 86 minutes
| country        = Weimar Republic Austria
| language       = German
| budget         = 
}} musical comedy film directed by Max Neufeld and distributed by Universum Film AG|UFA. Sehnsucht 202 was Luise Rainers film debut.

==Plot==
Set in Vienna, the film focuses on Magda and Kitty, two young women who reply to a newspaper advertisement and are contacted by the two young owners of a parfume store. Because their replies were confused with that of a flirtatious stenographer, the two men have different intentions than the girls and complications ensue. 

==Cast==
*Magda Schneider as Magda
*Luise Rainer as Kitty Fritz Schulz as Bobby
*Paul Kemp as Silber
*Rolf von Goth as Harry
*Attila Hörbiger as Paul, Magdas brother
*Mizi Griebl as Magdas mother
*Hans Thimig as Beamter

==Reception==
The film was received generally well.  The New York Times praised Magda Schneider, calling her "impersonally pleasing as ever".  The reviewer furthermore said: "Fritz Schulz did not let a comedy point get by and the cast was rounded smoothly by Rolf van Goth and Paul Kemp. Richard Fall has composed a song, "Mein Schatz, ich bin in Dein Parfüm verliebt" ("Sweetheart, Im in Love With Your Perfume"), which will have a bad break if it remains within Central European dance orchestra borders. I have spent many worse two hours with camera and microphone." 

Because of the films success, two alternate versions were made and released shortly later: Une jeune fille et un million (1932), a French version, and Milyon avcilari (1934), a Turkish version.

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 


 