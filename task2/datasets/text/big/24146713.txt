Welcome, Mr. Washington
{{Infobox film
| name = Welcome, Mr. Washington
| image =
| image_size =
| caption =
| director = Leslie S. Hiscott
| producer = Elizabeth Hiscott
| writer   = Jack Whittingham
| based on =  
| narrator = Donald Stewart   Peggy Cummins   Leslie Bradley
| music    = John Borelli Gerald Gibbs   Erwin Hillier
| editing  = Erwin Reiner
| studio   = British National Films
| distributor = Anglo-American Film Corporation 
| released =  
| runtime  = 91 minutes
| country  = United Kingdom English 
| budget   =
| gross    =
| preceded_by =
| followed_by =
| website  =
}} Donald Stewart and Peggy Cummins. Two sisters are left almost penniless by their fathers sudden death, and are forced to lease their estate as an airbase to the newly arrived American forces during the Second World War. 
 75 Most Wanted" lost films. 

The film was made by British National Films, based on a story by Noel Streatfeild.

==Cast==
* Barbara Mullen as Jane Willoughby  Donald Stewart as Lieutenant Johnny Grant 
* Peggy Cummins as Sarah Willoughby 
* Leslie Bradley as Captain Abbot 
* Roy Emerton as Selby 
* Martita Hunt as Miss Finch 
* Arthur Sinclair as Murphy 
* Graham Moffatt as Albert Brown 
* Shelagh Fraser as Millie 
* Beatrice Varley as Martha 
* George Carney as Publican 
* Louise Lord as Katherine Willoughby  Paul Blake as Vernon 
* Drusilla Wills as Mrs. Curley  
* Gordon Begg as Vicar 
* Julian DAlbie as Cox 
* Hal Gordon as Chauffeur  Danny Green as Hank
* Irene Handl as Mrs. Pidgeon Herbert Lomas as Blacksmith
* John McLaren as Corporal
* Thomas Palmer as Bud
* Tony Quinn as Sergeant McGinnis
* Johnnie Schofield as Butcher 
* Elsie Wagstaff as Miss Jones  Alexander Field as Yokel
* Victor Wood as  Yokel

==References==
 

==External links==
*  , with extensive notes
*  

 
 
 
 
 
 
 
 

 