Bird in a Cage
{{Infobox film
| name         = Bird in a Cage
| image        = 
| image_size   =
| caption      = Bird in a Cage
| director     = Antonio Zarro
| producer     = Bradford Carr Truman Anquoe Jr.
| executive producer = Terry Lindvall
| writer       = Antonio Zarro
| starring     =
| music        =
| cinematography =
| editing       =
| studio       = Christian Broadcasting Network University
| distributor  = Zarro Entertainment
| released     =  
| runtime      = 60 minutes
| country      = United States English
| budget       = United States|$ 15,000
}}
 American comedy comedic drama film written and directed by Antonio Zarro while he was attending the Christian Broadcasting Network University (now Regent University).

==Production==
Zarros original script for the film had the two leads dying, but this was changed when CBN University associate professor and executive producer of the film Terry Lindvall felt "so many deaths would make the movie too much like a Shakespearean tragedy."  He also recounted how a "blue tint applied at the film lab to turn a swimming scene from day to night made the heroines pink bathing suit seem to disappear", which compelled Lindvall to censor most of the swimming shot." 

==Plot==
The film follows a thief into the countryside where he is mistaken as preacher, which is transformational. 

==Reception==
Hal Erickson of All Movie Guide remarked that the director was a college student when he "lovingly assembled" the film, writing "The material is simple (and sometimes simplistic), but demands artistry. Novice filmmaker Antonio Zarro delivered that artistry." 

==Recognition==

===Awards and nominations=== Student Academy Award in 1987   Christian Broadcasting Network Universitys’s first Student Academy Awards. 

==References==
 

   

   

   

   

 

== External links ==
*  

 
 