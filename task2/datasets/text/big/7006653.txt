The Pink Blueprint
{{Infobox Hollywood cartoon
| cartoon_name = The Pink Blueprint Pink Panther
| image = Pinkblueprint.jpg
| caption = Title card
| director = Hawley Pratt Don Williams Norm McCabe Dale Case LaVerne Harding
| background_artist = Tom OLoughlin
| musician = William Lava
| producer = David H. DePatie Friz Freleng
| distributor = United Artists
| release_date =   Deluxe
| runtime = 6 24"
| country = United States
| movie_language = English
| preceded_by = Vitamin Pink
| followed_by = Pink, Plunk, Plink
}}
The Pink Blueprint is the 18th cartoon produced in the Pink Panther series. A total of 124 6-minute cartoons were produced between 1964 and 1980.

==Plot==
A frustrated building contractor battles with the Pink Panther over the design of a house being built. The original design has blue overtones and a more traditional shape; conversely, the panthers is rounded, sleek, and all pink. Several unsuccessful attempts are made at swapping the original house blueprint with the pink version. In conclusion, Pink gets the house he wants, but it is really the house the contractor was going to build in disguise. Beck, Jerry. (2006) Pink Panther: The Ultimate Guide to the Coolest Cat in Town!; DK ADULT, ISBN 0-7566-1033-8 

==Notes==
* The concept of substituting a pink version of something for the original color was borrowed from The Pink Phink. Both The Pink Blueprint and The Pink Phink were nominated for an Academy Award for Animated Short Film, with the latter winning. 
* Footage from The Pink Blueprint would be reused as a flashback in the made-for-television entry Pinkologist. 
* The Pink Panther Show contained a laugh track when the Pink Panther cartoons were broadcast on NBC-TV. When MGM released all 124 Pink Panther cartoons on DVD in 2006, the theatrical versions were, for the most part, utilized. However, several television prints made their way onto the DVD set, The Pink Blueprint being one of them. 
==See also== List of The Pink Panther cartoons

==References==
 

==Further sources==
* Meet the Pink Panther; by Hope Freleng and Sybil Freleng  , (Universe Publishing, 2005).
* The Pink Panther Classic Cartoon Collection   (2006).  . New York: MGM Home Video.
*  

 


 
 
 
 
 
 


 