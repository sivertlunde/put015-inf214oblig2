La caza
{{Infobox film
| name           = La caza
| image          = La caza.jpg
| caption        = Spanish film poster
| director       = Carlos Saura
| producer       = Elías Querejeta
| writer         = Angelino Fons Carlos Saura
| narrator       = 
| starring       = Alfredo Mayo Emilio Gutiérrez Caba
| music          = Luis de Pablo
| cinematography = Luis Cuadrado
| editing        = Pablo González del Amo  
| distributor    = 
| released       =  
| runtime        = 91 minutes
| country        = Spain
| language       = Spanish
| budget         = 
}} Spanish film Spanish Cinema.

==Plot==
José, Paco and Luis, three middle-aged men, veteran Falangist, reunite in a provincial village of Castile (historical region)|Castile, spending a hot summers day drinking, reminiscing and hunting rabbits. José instigates the hunt. He is in debt because of an impending divorce and is living beyond his means with a younger woman. His main objective at the reunion is to secure a loan from Paco, a shrewd businessman, also unhappily in love and looking for younger women. Paco brings with him Luis, now employed at his factory. Luis is a weak, forlorn individual, an alcoholic addicted to wine, women and science fiction rather than social conviviality or male camaraderie. A fourth member of the group, Enrique, a teenage relative of Pacos comes along for the thrill of the rabbit hunt.

Meeting at the local bar, the men proceed to a run down farm house and hire Juan and his young niece Carmen to aid them in the hunt, as well as several ferrets to rout the rabbits from their holes. As the hunters prepare their guns, they reminisce about the Civil War and the excitement of hunting men instead of animals. After a few drinks, José asks Paco for a loan; it will cement their relationship, he says. Paco, who has grudgingly been expecting this, refuses, but instead offers José a job.

During the hunt, the men kill several rabbits and eventually lunch on them. Their relationships become more enstranged as they fret over the past and rebuke each other in several ways. Luis becomes deranged and turns to practice-shooting with a mannequin; he also starts a fire that grows too large and has to be put down. Near the end, Paco kills a ferret; he claims he shot it accidentally, but José feels he did it maliciously. As the hunt gains in intensity, the gunfire becomes more rapid. The smoldering hatred and frustrations of the three men are triggered when Paco is hit by a blast from Josés shotgun and falls mortally wounded, into a stream. Luis, enraged by the killing, tries to kill José by running him down with a land rover. José retaliates, shooting at Luis, but the latter manages to survive long enough to shoot at the escaping José and kill him before going down himself. Enrique, unhurt, is left alone in the midst of this carnage, trying to fathom the inexplicable behavior of the three wartime comrades. The movie ends in a freeze-frame as he runs away from the carnage.

==Cast==	
*Ismael Merlo	 as	José
*Alfredo Mayo	as	Paco
*José María Prada	as Luis
*Emilio Gutiérrez Caba	as Enrique (credited as Emilio G. Caba)
*Fernando Sánchez Polack	as Juan
*Violeta García	as Carmen
*María Sánchez Aroca	as  La Madre de Juan

==Reception== Civil War battle similar to the one described in the dialogue. Saura won the Silver Bear for Best Director at the 16th Berlin International Film Festival in 1966.   

== References ==
 

==Notes==
*DLugo, Marvin, The Films of Carlos Saura, Princeton University Press, 1991, ISBN 0-691-03142-8
* Schwartz, Ronald, The Great Spanish Films: 1950- 1990,Scarecrow Press, London, 1991, ISBN 0-8108-2488-4

==External links==
*  
*   Suite101 (Spanish)

 

 
 
 
 
 
 
 
 