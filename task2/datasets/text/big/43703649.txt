Painted Desert (1938 film)
{{Infobox film
| name           = Painted Desert
| image          = File:Movie_poster_for_1938_western_film_Painted_Desert.jpg
| alt            = 
| caption        = Movie poster
| film name      =  David Howard
| producer       = Bert Gilroy
| writer         =  Oliver Drake Jack Cunningham
| based on       =  George OBrien Laraine Johnson Ray Whitley
| narrator       = 
| music          = Roy Webb Harry Wild
| editing        = Frederick Knudston
| studio         = RKO Pictures
| distributor    = RKO Pictures
| released       = 1938
| runtime        = 59 minutes
| country        = United States English
| budget         = 
| gross          = 
}}
 Western film David Howard George OBrien William Boyd Red Rock Canyon, a popular filming location during the 1930s and 1940s, with a multitude of Western (genre)|B-Westerns being filmed there. 

==Plot==
Bob, a young rancher, buys a mine on his leased land to prevent the working of it. However, surveys show a valuable mineral deposit, so he and Carol, the granddaughter of the discoverer of the mine, who has been forced to sell it to a crook, start operations on it. Fawcett, the crook, schemes to get the mine back, but is outwitted at every turn and loses his life in the dynamiting which he inspired.

==Cast== George OBrien - Bob McVey
*Laraine Day - Miss Carol Banning (billed as Laraine Johnson)
*Ray Whitley - Steve Stanley Fields - Bill
*Maude Allen - Yukon Kate
*Fred Kohler - Hugh Fawcett (billed as Fred Kohler, Sr.)
*Lloyd Ingraham - Charles M. Banning
*Harry Cording - Henchman Burke
*Max Wagner - Henchman Kincaid
*Lee Shumway - Bart Currie
*William V. Mong - Banker Heist

==References==
 

==External links==
  in the Internet Movie Database
 
 
 
 
 
 
 
 
 