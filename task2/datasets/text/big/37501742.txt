Evil Dead (2013 film)
{{Infobox film
| name           = Evil Dead
| image          = EvilDead2013Poster.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = Fede Alvarez
| producer       = {{Plainlist |
* Bruce Campbell
* Robert Tapert
* Sam Raimi
}}
| screenplay     = {{Plainlist |
* Fede Alvarez
* Rodo Sayagues
}}
| based on       = The Evil Dead by Sam Raimi
| starring       = {{Plainlist |
* Jane Levy
* Shiloh Fernandez
* Jessica Lucas
* Lou Taylor Pucci
* Elizabeth Blackmore
}}
| music          = Roque Baños
| cinematography = Aaron Morton
| editing        = Bryan Shaw
| studio         = Ghost House Pictures FilmDistrict 
| distributor    = TriStar Pictures
| released       =  
| runtime        = 92 minutes  
| country        = United States
| language       = English
| budget         = $17 million   
| gross          = $97.5 million 
}}
Evil Dead is a 2013 American supernatural horror film written and directed by Fede Alvarez. It is the fourth installment of the The Evil Dead (franchise)|Evil Dead franchise, serving as both a Reboot (fiction)|reboot, a remake and as a loose continuation of the series; the first neither to be directed by Sam Raimi, have Bruce Campbell as the main star, nor be scored by the original trilogys composer, Joseph LoDuca. Instead it was scored by Spanish composer Roque Baños.
 world premiere at the South by Southwest festival on March 8, 2013. On March 9, 2013, it was announced that the film will have a sequel, followed by a crossover with the original trilogy. Evil Dead was announced on July 15, 2013 to be adapted into a live experience as the first maze announced for Universal Studios Hollywoods and the second maze for Universal Orlando Resorts annual Halloween Horror Nights event for 2013.

==Plot==
An injured, terrorized girl is being chased down in the woods. The men who capture her takes her to some cabins basement. They believe she is possessed by an evil entity so they have her tied to a column. Her own father is among them, and as she tries to convince him that theres nothing wrong with her, she slowly loses control and eventually it becomes clear that she is in fact possessed. In response to this her father pours flammable liquid on her, sets her on fire, then shoots her in the head.

Some time later, David (Shiloh Fernandez) and his girlfriend Natalie (Elizabeth Blackmore) meet up at a cabin in the woods with his sister Mia (Jane Levy), and their friends Eric (Lou Taylor Pucci) and Olivia (Jessica Lucas), whos a nurse. The goal is to help Mia overcome her heroin addiction. David and Mias old dog Grandpa is there, too. David had moved to another city years before, leaving a younger Mia alone to care to their late, mentally ill mother.
 Naturom Demonto. Recognizing witchcraft practices, Eric starts reading the book out of curiosity. Mia, on the other hand, begins to think that there is something unnatural about the house, and asks them to leave, but the group refuses, believing that she is just suffering from Drug withdrawal|withdrawal. Furious that they dont believe her, she leaves alone in her car, but on the way out a mysterious, raggedy girl appears in the middle of the road, causing her to crash the car. Trying to return to the cabin on foot, she sees the girl again,   and ends entangled by the deformed branches of a possessed tree, which held her still and penetrate her with a black substance. Later, David and Olivia find Mia in shock, and take her back to the cabin. Mia tells David that whatever attacked her in the woods is now in the cabin with them, but he doesnt believe her, still convinced that she is hallucinating due to drug withdrawal symptoms. After finding Grandpa mortally wounded outside the cabin, David confronts Mia and finds her scalding herself in the shower. Eric recognizes the same scene depicted in the witch book. David tries to drive her to a hospital, but flood waters have blocked the road.

After returning to the cabin, the now fully possessed Mia shoots David with the shotgun and speaking in an altered voice tells the group that they will all die, then passes out. When Olivia tries to retrieve the gun, Mia overpowers her and vomits blood on her face. The group locks Mia in the cellar to contain her. Olivia retreats to the bathroom to clean herself, however she becomes possessed by the entity as well. Eric happens upon Olivia cutting her cheek off with broken glass, and she turns on him. In the struggle, Eric bludgeons her to death. While David tends to Erics wounds, a crying Mia lures Natalie into the cellar and bites her hand. Eric explains to David that according to the book, the demon called the Taker of Souls must consume five souls in order to unleash a being called the Abomination. Meanwhile, Natalie attempts to avoid possession from the bite wound infection by severing her arm. David and Eric tend to Natalies wounds, then discuss their next move. Eric explains that Mia must be "purified" either by live burial, dismemberment, or burning; this will end her possession and the demons assault. As they debate, a possessed Natalie attacks them with a nail gun. David shoots Natalies other arm off with the shotgun, and she bleeds to death.

David takes the injured Eric outside, planning to burn Mia with the whole cabin, but when she starts to sing their mothers lullaby, he cant bring himself to do it. He decides to bury her alive, instead. He digs a grave outside, then descends into the basement to capture Mia, but is caught by surprise. In the ensuing struggle, David is saved when Eric knocks Mia out, but not before she manages to stab him with a box cutter. As Eric dies from his wounds, he forgives David. David sedates Mia and puts her in the grave. After she dies, he digs her up and uses an improvised defibrillator to revive her, finding that the demon has been Exorcism|exorcised, and she is human again. However, returning to the cabin to retrieve the keys to his Jeep, David is mortally wounded by a now-possessed Eric. David locks Mia out of the cabin, engaging Eric by himself. David ignites a nearby gasoline can, engulfing the cabin in flames and killing both Eric and himself.

With their deaths, the prophecy is fulfilled. Blood rains from the sky as the Abomination, who looks like Mia, rises from Hell and starts chasing her. After a protracted fight that results in the loss of her hand, Mia manages to kill the Abomination with a chainsaw, and it sinks back into the ground. The blood rain stops and Mia walks away into the forest. The Naturom Demonto lies on the ground nearby and slams shut on its own.
 turning to the screen.

===Alternate ending=== 
An alternate ending shows Mia walking on a deserted road after leaving the cabin before collapsing. A man stops and picks her up. In the car, Mia wakes up with her eyes wide open but is not possessed. 

==Cast==
 
* Jane Levy as Mia Allen
* Shiloh Fernandez as David Allen
* Lou Taylor Pucci as Eric
* Jessica Lucas as Olivia
* Elizabeth Blackmore as Natalie
* Jim McLarty as Harold
* Phoenix Connolly as Teenager
* Sian Davis as Old Woman
* Stephen Butterworth as Toothless Redneck
* Karl Willetts as Long Haired Redneck
* Randal Wilson as Demonic/Abomination Mia
* Rupert Degas as the Demon (voice)
 

In addition, using audio from the original film, Bob Dorian plays Professor Raymond Knowby during the credits and Ellen Sandweiss plays a voice cameo as Cheryl Williams. Bruce Campbell plays Ash Williams in an uncredited post-credit cameo appearance.

The initial letters of the five main characters names (David, Eric, Mia, Olivia, Natalie) form an acrostic spelling out the word DEMON. 

==Production== Robert G. Tapert, who are the producers of the original trilogy.

Raimi and Campbell had planned a remake for many years, but in 2009, Campbell stated the proposed remake was "going nowhere" and had "fizzled" due to extremely negative fan reaction.   However, in April 2011, Bruce Campbell stated in an AskMeAnything interview on Reddit.com, "We are remaking Evil Dead. The script is awesome   The remakes gonna kick some ass — you have my word." 

On July 13, 2011, it was officially announced, via a press release, that Ghost House Pictures would team up with FilmDistrict to produce an Evil Dead remake, with Diablo Cody in the process of revising the script and Fede Alvarez directing.   Actor Shiloh Fernandez was cast in the lead male role of David.  Bloody Disgusting reported that Lily Collins would play the lead female role of Mia, but on January 24, 2012, she dropped out of the role.  

On February 3, 2012, it was announced that actress Jane Levy, star of the television series Suburgatory, would replace Collins in the lead female role as Mia.  Lou Taylor Pucci, Elizabeth Blackmore, and Jessica Lucas later joined the cast.  

In January 2013, Alvarez commented on the ambiguity of the films relationship to the original:
  Alvarez, who also has a background in computer-generated imagery|CGI, also confirmed in an interview that the film does not employ CGI (except for touch-ups): "We didnt do any CGI in the movie   Everything that you will see is real, which was really demanding. This was a very long shoot, 70 days of shooting at night. Theres a reason people use CGI; its cheaper and faster, I hate that. We researched a lot of magic tricks and illusion tricks." 

Sam Raimis 1973 Oldsmobile Delta 88 can be seen in an opening scene with David and Mia as they arrive at the cabin. The 1973 Oldsmobile Delta 88 has appeared in almost all of the movies that Raimi has been involved with over his career.

==Release== tweeted on BBFC for containing strong "bloody violence, gory horror and very strong language".     StudioCanal handled the release of Evil Dead in the United Kingdom. 

Evil Dead premiered at the SXSW Film Festival in Austin, TX on March 8, 2013.   The music for Evil Dead, composed by Roque Baños, was released by La-La Land Records in a 40-minute digital form and a 70-minute physical release, on April 9, 2013. 

===Home media===
Evil Dead was released on DVD and Blu-ray, on July 16, 2013.   The Blu-ray exclusives include commentary from three of the cast, and screenwriters Fede Alvarez and Rodo Sayagues, behind the scenes and a featurette, while the regular DVD will include three other featurettes. 

===Extended cut===
An extended version, that has yet to be released, featuring an alternative ending and various other deleted clips and dialogue, some of which were featured in the original trailer but subsequently removed from the theatrical version, was aired in the UK, on January 25, 2015.

No one has yet confirmed whether this was an intentional debut for the anticipated "extended cut", which fans of the film have asked about since the theatrical release, or whether StudioCanal had inadvertently supplied Channel 4 with the wrong copy of the film. 

Alvarez has confirmed on Twitter, that the version aired was not the extended cut. 

Channel 4 has subsequently confirmed that the wrong copy of the film was supplied to them and that they have sent it back. They added that they have no other information on the version which aired and since the "extended/incorrect" version has been returned to the Distributor they would not be airing it again. 

==Reception==
The film brought in $26 million in its opening weekend  and became a box office success, grossing over $54,239,856 domestically and $43,303,096 internationally, for a worldwide take of $97,542,952.

The film holds a score of 62% on Rotten Tomatoes, based on 172 reviews, with an average rating of 6.2/10. The sites consensus states, "It may lack the absurd humor that underlined the original, but the new-look Evil Dead compensates with brutal terror, gory scares, and gleefully bloody violence."   On Metacritic, the film has a score of 57 out of 100, based on 38 critics, indicating "mixed or average reviews".  Criticism of the film focused on the characters and some of the story being spoiled by the trailers.
 The List commented, "Evil Dead has ample cheap shocks and few bloodcurdling frights but it builds to something gorily bravura and, if thats your bag, youll come away satisfied. Its a while before anyone picks up a chainsaw, but boy is it worth it when they do."   Matt Singer called the film "an assault on the senses" and "a success, one that out-Evil Deads the original movie with even more gore, puke, blood, and dismembered limbs. It may not be wildly inventive, but it is effective, and plenty faithful to the spirit — and tagline — of the first Ultimate Experience in Grueling Terror." 

Richard Roeper rated the film one star out of four, criticizing the films unoriginality, the characters lack of intelligence, and the films reliance on gore for what he felt were cheap scares. He concluded his review by saying, "I love horror films that truly shock, scare and provoke. But after 30 years of this stuff, Im bored to death and sick to death of movies that seem to have one goal: How can we gross out the audience by torturing nearly every major character in the movie?" 

==Awards and nominations==
{| class="wikitable"
|-
! Year
! Award
! Category
! Recipient
! Result
|- 2013
| Golden Trailer Awards 
| Best Horror TV Spot
| TriStar Pictures and mOcean for "Everythings Fine"
|  
|-
| rowspan="2"| 2014 Empire Awards Best Horror
|
|  
|- Saturn Award Best Make-Up
| Patrick Baxter, Jane OKane and Roger Murray 
|  
|}

==Sequel==
At the SXSW premiere event, Alvarez announced that a sequel is in the works.    In addition, Sam Raimi confirmed plans to write Evil Dead 4 with his brother; it was later specified that this film would be Army of Darkness 2.   At a WonderCon panel in March 2013, Campbell and Alvarez stated that their ultimate plan was for Alvarezs Evil Dead 2 and Raimis Army of Darkness 2 to be followed by a seventh film which would merge the narratives of Ash and Mia.  On October 30, 2013, co-writer Sayagues confirmed to DeadHollywood that he and Alvarez will not return for the sequel.  That same month, Alvarez took to his Twitter that the rumor is not true.   It appears at this point based on interviews from Bruce Campbell and Jane Levy that a 2nd film in Alvarez Evil Dead series will not happen, they are instead choosing to continue with the original Evil Dead trilogy story in the new TV Series Ash Vs Evil Dead.  Fede Alvarez has already begun work on a 2nd film entitled Man In The Dark.  Sam Raimi and Rob Tapert are producing again.

==References==
 
 

==External links==
 
 
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 