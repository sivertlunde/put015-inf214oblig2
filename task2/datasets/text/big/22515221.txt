AmerAsian
 
 
{{Infobox film
| name           = AmerAsian (AmerAsian: Origin)
| image          = AmerAsian.jpg
| alt            =  
| caption        = Theatrical poster
| director       = Roger Lim
| producer       = Roger Lim
| writer         = Roger Lim
| starring       = Roger Lim Evlin Lake Bobby Dodge Shannon Gayle Andrew Plummer Jason Lombard
| music          = Chris Julian
| cinematography = David Doko
| editing        = Joseph Binetti
| studio         = 
| distributor    = 
| released       =  
| runtime        = 105 minutes
| country        = United States
| language       = English
| budget         = $500,000
| gross          = 
}}
AmerAsian is a 2009 feature length college relationship drama was initially completed, but re-entered post-production in January 2009 for additional shooting. Directed by Roger Lim, it is the first AmerAsian film of a trilogy and stars Lim as Eric Young (who takes on the identity of unknown Jimmy Lee) and Evlin Lake as Monica Donovan (Eric’s potential love interest). It also introduces Bobby Dodge as Coach Donovan (Monica’s dad and Eric’s baseball coach), Shannon Gayle as Kimberly (Monica’s best friend), Andrew Plummer as Sean (the catalyst who finds Eric’s new identity), and Jason Lombard as Daryl Walker (the team’s standout player).

==Plot==
The story, set in 2000, begins when Eric Young, stagnant and lifeless since his father’s death, is coerced by his best friend, Sean, to borrow an identity and take back a forgone year of baseball eligibility. Once in class, Eric immediately connects with Monica, the charming, All-American daughter of his coach, Coach Donovan. Although Eric works harder than anyone to earn playing time, the jealous Donovan permanently relegates him to the bench. Due to his own hidden motives, Sean warns Eric to steer clear of Monica and to focus strictly on baseball. But when Eric turns the other ear, he finds the closer he grows to Monica, the darker the secrets that surface from her abusive past. Eric is ultimately forced to the weigh the challenges of his own mental well-being against the psychological pain which causes Monica to spiral out of control.

==Cast==
*Roger Lim as Eric (aka Jimmy): An American- born Asian graduate, 28; Even after several years, he seems unable to move forward in life after the loss of his dad; Coerced by his best friend, Sean, into taking a younger student’s identity in order to play one last year of college baseball; Establishes a relationship with Monica, discovering too late that she is his Coach’s daughter; While hiding his true identity, he confronts mounting pressures from everyone around, while he tries to help Monica come to terms with her past; When relegated to the bench the entire year, her trauma forces him to re-evaluate the perspective of his own life.
*  her self-diagnosed manic-depression; Covers her tremendous emotional trauma with admirable warmth, optimism, and charisma—but for how long?
*Bobby Dodge as Coach Donovan: Eric and Sean’s unfair college baseball coach, 45; Also a health education instructor who feels threatened when Eric and Monica flirt with each other in class; The “adoptive” father of Monica whose jealousy, suspicion, and disapproval of all of Monica’s relationships lead to increasing alcoholic tendencies and sexually deviant behavior.
*Shannon Gayle as Kimberly: An All-American co-ed junior, 20, who is Daryl Walker’s off and on flame; Malicious and relentless, she uses Eric for revenge on both Monica and Daryl by way of quick-wits, deceit, and sexuality; Masks her jealousy and insecurity with calculating seduction; Cold-hearted and competitive, she even sets eyes on Monica’s father to stir the ultimate trouble.
*Andrew Plummer as Sean: A college senior and an extremely talented athlete suffering through a disappointing college baseball career, 22; Manages the convenience store where he talks Eric into stealing a younger student’s identity; When Eric hesitates, Sean must somehow cajole him into the classroom and onto the playing field due to hidden motives of his own.
*Jason Lombard as Daryl Walker: A college senior and the team’s best player, 22, who is always on Coach Donovan’s good side; The starting shortstop, who is the reason behind Sean’s limited playing time; Repeatedly disrespects Eric by pursuing Monica, all while playing with Kimberly’s emotions; Cocky, arrogant, and cutthroat, he plays the entire field.
*Additional cast members include Akiko Shima as Eric’s Mom, Kristin Pesceone as Sean’s girlfriend Lara, Kara Hyatt as high schooler Julie, Tracy Miller as cold-hearted Coach Martin Weiss, Sr., and Andy Rossi as racist umpire, Christian. 

==Production==
===Development===
After 2 years of developing his first feature length screenplay, Lim set out to produce, cast, and act in AmerAsian. Lim, who was at first reluctant to direct himself in his script, decided that doing so would be in the best interest of the most accurate storytelling, having written the script based on his own life. His casting process took a very tough 18 months before all the pieces began to fall into place. The film, originally scheduled to shoot in the Spring of 2004 with Travis Van Winkle attached as Sean, was pushed back a year for casting purposes. After over 1500 reads for the female lead, Lim found Evlin Lake, who was perfect for the role of Monica Donovan, and initiated intense rehearsals with her. After several hundred more reads, Bobby Dodge was attached to play Monica’s suspect father, Coach Donovan, and Jason Lombard was cast as the arrogant Daryl. Although it took just as many more auditions to find the perfect cold-hearted Kimberly, Shannon Gayle was attached late in the game, though Lim notes her performance as one of the most exciting elements of the entire film. Despite Van Winkle’s hard work, commitment, and dedication to his intense rehearsals, and everything set to go in mid-2005, Van Winkle was unfortunately lost to production. But just weeks before shooting, Lim cast Andrew Plummer, already on board for a smaller role in the film, as the new Sean. The remainder of the cast would come on board as much as a year later.
Attached even before any of the actors was cinematographer David Doko, whose tremendous support was invaluable to Lim. A month before principal photography was to begin, Lim set out to personally interview hundreds more to fill his 20 crew positions. As pre-production came to a wrap, Lim was able to continually fine-tune his script until AmerAsian was set to begin shooting.

===Filming===
Shooting on AmerAsian started on June 11, 2005 on the Los Angeles Pierce College baseball field in Woodland Hills, California, and concluded on July 17, 2005 at the Country Inn & Suites in Calabasas. Two additional sets of pickup weekends were shot each in 2006 and in 2007. Shooting locations also included Los Angeles, Encino, Los Angeles|Encino, Sherman Oaks, and Burbank, California. During 2007, Lim worked with picture editor Joseph Binetti while he was able to write and shoot the final pieces of his debut film. During 2008 and into 2009, the film’s sound design, foley, and music fell into the hands of Emmy Award Winner, Chris Julian. 

==Release==
AmerAsian will be released in North America in February, 2010. In addition to the release of AmerAsian, the film will be succeeded by its sequels, Great American Dream, followed by Young Again.

== References ==	
 

==External links==
* 
* 

 
 
 
 
 