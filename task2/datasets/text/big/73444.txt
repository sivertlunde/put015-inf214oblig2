Easy Rider
 
{{Infobox film
| name           = Easy Rider
| image          = EasyRider.jpg
| image_size     =
| caption        = Original poster
| director       = Dennis Hopper
| producer       = Peter Fonda
| writer         = Peter Fonda Dennis Hopper Terry Southern
| starring       = Peter Fonda Dennis Hopper Jack Nicholson Steppenwolf Smith Smith The Byrds The Holy Modal Rounders Fraternity of Man The Electric Flag The Jimi Hendrix Experience Little Eva The Electric Prunes Roger McGuinn
| cinematography = László Kovács (cinematographer)|László Kovács
| art direction  = Jerry Kay
| editing        = Donn Cambern
| studio         = Raybert Productions Pando Company Inc.
| distributor    = Columbia Pictures
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         = $360,000
| gross          = $60 million   Worldwide Box Office. Retrieved July 18, 2014. 
}} bikers (played American Southwest Library of Congress National Registry in 1998.
 counterculture film, drug use, communal lifestyle. marijuana and other substances. Interviews in  . A Making-of documentary. 

==Plot== American flag-adorned Native American-style buckskin pants and shirts and a bushman hat. The former is appreciative of help and of others, while the latter is often hostile and leery of outsiders.
 Los Angeles, Mardi Gras.

During their trip, Wyatt and Billy meet and have a meal with a rancher, whom Wyatt admires for his simple, traditional farming lifestyle.  Later, the duo pick up a hitch-hiker ( " appears to be practiced, with two women seemingly sharing the affections of the hitch-hiking communard  and then turning their attention to Wyatt and Billy. The hitch-hiker wants the two bikers to stay at the commune, saying, "the time is now", to which Wyatt replies "Im hip about time...but I just gotta go."  As the bikers leave, the hitch-hiker (known only as "Stranger on highway" in the credits) gives Wyatt some LSD for him to share with "the right people".

Later, while jokingly riding along with a parade in a small town, the pair are arrested by the local authorities for "parading without a permit" and thrown in jail. There, they befriend American Civil Liberties Union lawyer and local drunk George Hanson (Jack Nicholson) who has spent the night in jail after overindulging in alcohol. George helps them get out of jail, and decides to travel with Wyatt and Billy to New Orleans. As they camp that night, Wyatt and Billy introduce George to cannabis (drug)|marijuana. As an alcoholic and a square (slang)|"square", George is reluctant to try the marijuana (Gateway drug theory|"It leads to harder stuff", and "I dont want to get hooked"), but he quickly relents.
 Louisiana restaurant, parish line." As the waitress does not take their order Wyatt, Billy, and George leave without eating and make camp outside of town. The events of the day cause George to comment: "This used to be a hell of a good country. I cant understand whats gone wrong with it." He observes that Americans talk a lot about the value of freedom, but are actually afraid of anyone who truly exhibits it.

In the middle of the night, the local men find the trio asleep and brutally assault them with clubs. Billy manages to scare off the attackers by yelling and brandishing a knife. Wyatt and Billy suffer minor injuries, but George has been fatally beaten. Wyatt and Billy wrap Georges body up in his sleeping bag, gather his belongings, and vow to return the items to his parents.
 psychedelic bad trip infused with Catholic prayer represented through quick edits, sound effects, and exposure (photography)|over-exposed film.
 middle finger up at them, the hillbilly fires the shotgun at Billy, who immediately hits the pavement, seriously wounded in the side. As the truck then takes off past Wyatt down the road, Wyatt turns around and races back to put his jacket over his critically injured friend, who is already covered in blood, before riding off for help. By this time, the pickup truck has turned around and closes on Wyatt. The hillbilly fires at Wyatt as he speeds by the pickup, hitting the bikes gas tank and causing it to instantly erupt into a fiery explosion. Wyatt lands by the side of the road, apparently dead. As the murderous rednecks drive away, the film ends with a shot of the flaming bike in the middle of the deserted road, as the camera ascends to the sky.

==Cast==
 
 
*Peter Fonda – Wyatt, "Captain America"
*Dennis Hopper – Billy
*Antonio Mendoza – Jesus
*Phil Spector – Connection
*Mac Mashourian – Bodyguard
*Warren Finnerty – Rancher
*Tita Colorado – Ranchers Wife
*Luke Askew – Stranger on Highway
Commune
*Luana Anders – Lisa
*Sabrina Scharf – Sarah
*Sandy Wyeth – Joanne
*Robert Walker, Jr. – Jack
*Robert Ball - Mime #1
*Carmen Phillips - Mime #2
*Ellie Walker - Mime #3
*Michael Pataki - Mime #4
Jail
*Jack Nicholson – George Hanson
*George Fowler, Jr. - Guard
*Keith Green - Sheriff
 
Café
*Hayward Robillard - Cat Man
*Arnold Hess, Jr. - Deputy
*Buddy Causey, Jr. - Customer #1
*Duffy Lafont - Customer #2
*Blasé M. Dawson - Customer #3
*Paul Guedry - Customer #4
*Suzie Ramagos - Girl #1
*Elida Ann Hebert - Girl #2
*Rose LeBlanc - Girl #3
*Mary Kaye Hebert - Girl #4
*Cynthia Grezaffi - Girl #5
*Colette Purpera - Girl #6
House of Blue Lights
*Toni Basil – Mary
*Karen Black – Karen
*Lea Marmer - Madame
*Cathé Cozzi - Dancing Girl
*Thea Salerno - Hooker #1
*Anne McClain - Hooker #2
*Beatriz Monteil - Hooker #3
*Marcia Bowman - Hooker #4
Pickup Truck
*David C. Billodeau
*Johnny David
 

==Production==
 ) with Wyatt (Peter Fonda)]]
 ]]
 The Trip Beat relic into its hippie reincarnation as Easy Rider", and connected Peter Fondas characters in those two films, along with his character in The Wild Angels, deviating from the "formulaic biker" persona and critiquing "commodity-oriented filmmakers appropriating avant-garde film techniques."  It was also a step in the transition from independent film into Hollywood film|Hollywoods mainstream, and while The Trip was criticized as a faux, popularized underground film made by Hollywood insiders, Easy Rider "interrogates" the attitude that underground film must "remain strictly segregated from Hollywood."  Mills also wrote that the famous acid trip scene in Easy Rider "clearly derives from their first tentative explorations as filmmakers in The Trip." 

When seeing a still of himself and Bruce Dern in The Wild Angels, Peter Fonda had the idea of a modern Western film|Western, involving two bikers travelling around the country and eventually getting shot by hillbillies. He called Dennis Hopper, and the two decided to turn that into a movie, The Loners, with Hopper directing, Fonda producing, and both starring and writing. They brought in screenwriter Terry Southern, who came up with the title Easy Rider. The film was mostly shot without a screenplay, with ad-libbed lines, and production started with only the outline and the names of the protagonists. Keeping the Western theme, Wyatt was named after Wyatt Earp and Billy after Billy the Kid. 

During test shooting on location in New Orleans, Hopper fought with the productions ad hoc crew for control.  At one point he entered into a physical confrontation with photographer Barry Feinstein, who was one of the camera operators for the shoot. After this turmoil, Hopper and Fonda decided to assemble a proper crew for the rest of the film. 
 The Tonight Show, and during the interview, Hopper alleged that Torn had pulled a knife on him during the altercation, prompting Torn to sue Hopper successfully for defamation. 
 Malibu Canyon, Arroyo Hondo near Taos, New Mexico, did not permit shooting there.   
 Route 66 in Flagstaff, Arizona, passing a large figure of a lumberjack. That lumberjack statue—once situated in front of the Lumberjack Cafe—remains in Flagstaff, but now stands inside the Walkup Skydome|J. Lawrence Walkup Skydome on the campus of Northern Arizona University.    A second, very similar statue was also moved from the Lumberjack Cafe to the exterior of the Skydome.   

Most of the film is shot outside with natural lighting.  Hopper said all the outdoor shooting was an intentional choice on his part, because "God is a great Gaffer (motion picture industry)|gaffer." The production used two five-ton trucks, one for the equipment and one for the motorcycles, with the cast and crew in a motor home.  One of the locations was Monument Valley. 
 Krotz Springs, and the two other men in the scene—Johnny David and D.C. Billedeau—were Krotz Springs locals.
 Madonna as though it were Frances Ford Seymour|Fondas mother (who had committed suicide when he was 10 years old) and ask her why she left him. Although Fonda was reluctant, he eventually complied. Later, Fonda used the inclusion of this scene as leverage to persuade Roger McGuinn to allow the use of his cover of Bob Dylans "Its Alright, Ma (Im Only Bleeding)". 

Despite being filmed in the first half of 1968, roughly between  , one of Hoppers proposed cuts was 220 minutes long, including extensive use of the "  and Bert Schneider, Henry Jaglom was brought in to edit the film into its current form, while Schneider purchased Hopper a trip to Taos so he would not interfere with the recut. Upon seeing the final cut, Hopper was originally displeased, saying that his movie was "turned into a TV show", but he eventually accepted, claiming that Jaglom had crafted the film the way Hopper had originally intended. Despite the large part he played in shaping the film, Jaglom only received credit as an "Editorial Consultant". 

There are various reports about exact running time of original rough cut of the movie; Four hours, four and a half hours, or five hours. All deleted footage is believed to be lost. Some of the scenes which were in original cut but got deleted are:  Original opening showing Wyatt and Billy performing in a Los Angeles stunt show (their real jobs), two of them being ripped off by the promoter, getting in a biker fight, picking up women at a drive-in, cruising to and escaping from Mexico to score the cocaine they sell, elaborate police and helicopter chase that took place at the beginning after the dope deal with police chasing Wyatt and Billy over mountains and across the Mexican border, the road trip out of L.A. edited to the full length of Steppenwolfs Born to Be Wild with billboards along the way offering wry commentary, Wyatt and Billy being pulled over by cop while driving their motorcycles across highway, two of them encountering the black motorcycle gang, ten additional minutes for the volatile café scene in Louisiana where George deftly keeps the peace, Wyatt and Billy checking in hotel before going over to Madam Tinkertoys, extended and much longer Madam Tinkertoy sequence, extended versions of all the campfire scenes including the enigmatic finale in which Wyatt says "We blew it, Billy".

Easy Rider’s style — the jump cuts, time shifts, flash forwards, flashbacks, jerky hand-held cameras, fractured narrative and improvised acting — can be seen as a cinematic translation of the psychedelic experience. Peter Biskind, author of Easy Riders, Raging Bulls wrote: "LSD did create a frame of mind that fractured experience and that LSD experience had an effect on films like Easy Rider". 

==Motorcycles==
 .  ]] hardtail motorcycle frames and panhead engines, chopper builders—Cliff Ben Hardy—following ideas of Peter Fonda, and handled by Tex Hall and Dan Haggerty during shooting.   
 props became National Motorcycle Museum in Anamosa, Iowa. Many replicas have been built since the film’s release. 

Hopper and Fonda hosted a wrap party for the movie and then realized they had not shot the final campfire scene.  Thus, it was shot after the bikes had already been stolen, which is why they are not visible in the background as in the other campfire scenes.    

==Reception==
  review aggregate website Rotten Tomatoes, based on 44 reviews, with an average rating of 7.8/10.

===Awards and honors=== Best Actor Best Original Screenplay.
 100 Years, 100 Movies. In 1998, Easy Rider was added to the United States National Film Registry, having been deemed "culturally, historically, or aesthetically significant."

===Significance=== third highest Bonnie and Clyde and The Graduate, Easy Rider helped kick-start the New Hollywood phase during the late 1960s and early 1970s. {{cite news
  | title = Easy Rider (1969)
  | publisher = The New York Times
  | url = http://movies.nytimes.com/movie/15197/Easy-Rider/overview
  | accessdate = 2008-10-18
| first=Vincent Vice President Spiro Agnew criticized Easy Rider, along with the band Jefferson Airplane, as examples of the permissiveness the 1960s counterculture. 

The films success, and the new era of Hollywood that it helped usher in, gave Hopper the chance to direct again with complete artistic control.  The result was 1971s The Last Movie, which was a notable box office and critical failure, effectively ending Hoppers career as a director for well over a decade.

==Music==
 
The movies "groundbreaking" {{cite news
  | title = The greatest week in rock history
  | publisher = Salon
  | date = 2003-12-19
  | url = http://dir.salon.com/story/ent/feature/2003/12/19/rock/index.html CSN viewed a rough cut of the film, they assured Hopper that they could not do any better than he already had.

Bob Dylan was asked to contribute music, but was reluctant to use his own recording of "Its Alright, Ma (Im Only Bleeding)", so a version performed by Byrds frontman Roger McGuinn was used instead. Also, instead of writing an entirely new song for the film, Dylan simply wrote out the first verse of “Ballad of Easy Rider” and told the filmmakers, “Give this to McGuinn, he’ll know what to do with it.” McGuinn completed the song and performed it in the film.

==Home media==
The film was released by The Criterion Collection in November 2010 as part of the box set America Lost and Found: The BBS Story. It included two audio commentaries, one featuring actor-director-writer Dennis Hopper, the other Hopper, actor-writer Peter Fonda, and production manager Paul Lewis, Born to Be Wild (1995) and “Easy Rider”: Shaking the Cage (1999), documentaries about the making and history of the film, television excerpts showing Hopper and Fonda at the Cannes Film Festival, and a new video interview with BBS cofounder Steve Blauner. 

==Sequel==
In 2013, a revisionist history sequel to the movie was released titled  , directed by Dustin Rikert.  The film is about the family of Wyatt "Captain America" Williams from the 1940s to the present day.

==See also==
* List of films featuring hallucinogens
* American Dream
* Hippie exploitation films
* Method acting
* Outlaw biker film

==Notes==
{{Reflist|30em|refs=
   
 
   

    
}}

==References==
 
*{{cite book
  | last = Aldaz
  | first = Gabriel
  | authorlink =
  | title = Right Palm Up, Left Palm Down: The Log of a Cross-Country Scavenger Hunt
  | publisher = SparkWorks
  | year = 2010
  | isbn = 978-0-9703407-7-1
}}
*{{cite book
  | last = Carr
  | first = Jay
  | authorlink =
  |author2=National Society of Film Critics 
  | title = The A List: The National Society of Film Critics 100 Essential Films
  | publisher = Da Capo Press
  | year = 2002
  | isbn = 0-306-81096-4
}}
* 
*{{cite book
  | last = Hill
  | first = Lee
  | authorlink =
  | title = Easy Rider
  | publisher = British Film Institute
  | year = 1996
  | isbn = 0-85170-543-X
}}
*J. Hoberman|Hoberman, J..  
*{{cite book
  | last = Klinger
  | first = Barbara
  | authorlink =
  | editor =  Steven Cohan, Ina Rae Hark
  | chapter = The Road to Dystopia: Landscaping the Nation in Easy Rider
  | title = The Road Movie Book
  | location = New York
  | publisher = Routledge
  | year = 1997
  | isbn = 0-415-14936-3
}}
*{{cite book
  | last = Lev
  | first = Peter
  | authorlink =
  | title = American Films of the 70s: Conflicting Visions
  | publisher = University of Texas Press
  | year = 2000
  | url = http://www.utexas.edu/utpress/books/levame.html
  | isbn = 0-292-74716-0
}}
*{{cite book
  | last = Osgerby
  | first = Bill
  | authorlink =
  | title = Biker: Truth and Myth: How the Original Cowboy of the Road Became the Easy Rider of the Silver Screen
  | publisher = Globe Pequot
  | year = 2005
  | isbn = 1-59228-841-3
}}
* Phipps, Keith. (November 16, 2009) Slate.com.  
*Zoller Seitz, Matt.  
 

== Further reading ==
*  

==External links==
 
 
*  
 
 <!--      

       Please be cautious adding more external links.

Wikipedia is not a collection of links and should not be used for advertising.

     Excessive or inappropriate links will be removed.

 See   and   for details.

If there are already suitable links, propose additions or replacements on
the articles talk page, or submit your link to the relevant category at
the Open Directory Project (dmoz.org) and link there  using  .

-->

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 