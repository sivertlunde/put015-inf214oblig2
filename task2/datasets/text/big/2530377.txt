The Center of the World
 
{{Infobox film
| name           = The Center of the World
| image          = The Center of the World poster.jpg
| image_size     =
| caption        =
| director       = Wayne Wang
| producer       = Wayne Wang Peter Newman
| writer         = Wayne Wang Miranda July
| narrator       =
| starring       = Peter Sarsgaard Molly Parker
| music          =
| cinematography = Mauro Fiore
| editing        = Lee Percy
| distributor    =
| released       =  
| runtime        = 88 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 American film directed by Wayne Wang, which was digitally shot and  released in 2001. It stars Peter Sarsgaard as a Dot-com millionaire who hires a drummer/stripper (Molly Parker) to stay with him in Las Vegas for three days for US$10,000. The film was screened out of competition at the 2001 Cannes Film Festival.   

==Plot==
A couple checks into a suite in Las Vegas. In flashbacks we see that hes a computer whiz on the verge of becoming a dot.com millionaire (  Jerri (Carla Gugino); at night, at least after the first night, things seem to get complicated. When the three days are over, the stripper makes it clear that she was only there for the money and that the man she spent the time with was just a client. Upset that the feelings he had werent mutual he then rapes her, which she makes no attempt to stop. She then masturbates for him to achieve orgasm. The next day he returns home heartbroken. The movie ends with his return to the strip club to see the woman he fell in love with again. She greets him fondly and says that she is glad to see him again. Because the movie is shown in a non-linear format, it is left to the viewers interpretation of when this event occurred.

==Cast==
  
* Peter Sarsgaard as Richard Longman  
* Molly Parker as Florence
* Mel Gorham as Roxanne
* Shane Edelman as Porter
* Karry Brown as Lap dancer
* Alisha Klass as Pandora stripper 
* Lisa Newlan as Porn site woman
* Jason Calacanis as Pete
 
* Travis Miljan as Dog owner
* Jerry Sherman as Old man
* Carla Gugino as Jerri
* Pat Morita as Taxi driver
* Balthazar Getty as Brian Pivano
* Robert Lefkowitz as Motel manager
* John Lombardo as Gondolier
 

;Cast notes:
* Internet executive Jason Calacanis consulted on, and appeared in, the film.

==Reception==
The film opened to mixed reviews, and has been compared to movies with similar "hooker in love" storylines such as Exotica (film)|Exotica and Pretty Woman. The Center of the World is more explicit than those films, containing both female and male full frontal nudity, as well as a penetration shot of a lollipop in a vagina.

==Notes==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 