The Presence (film)
{{Infobox film
| name           = The Presence
| image          = ThePresenceFilm2010.jpg
| caption        = Dont Believe Everything You Hear.
| director       = Tom Provost
| producer       = Daniel Myrick Tom Provost Brandon Blake Tom Rice
| writer         = Tom Provost
| starring       = Mira Sorvino Justin Kirk Tony Curran Shane West
| music          = Conrad Pope
| cinematography = Collin Brink
| editing        = Cecily Rhett
| distributor    = Lionsgate
| released       =  
| runtime        = 87 minutes
| country        = United States English
| studio         = Khartoum Saturn Harvest Films Flatland Pictures
| budget         = $1 million {http://rocknrollghost.com/2011/11/07/film-interview-tom-provost-the-presence-lionsgate}
}}

The Presence is a 2010 horror thriller film written and directed by Tom Provost. This film is the directorial debut for Tom Provost. 

==Plot==
In this darkly romantic ghost story, a woman (Mira Sorvino) travels to an isolated cabin where she finds herself stalked by an apparition (Shane West) who has come to inhabit her space as his own. With the unexpected arrival of the womans boyfriend (Justin Kirk), the dark spirits haunting grows more obsessive. Soon the woman begins to exhibit weirdly irrational behavior as the thin line between sanity and possession begins to unravel.

==Cast==
* Mira Sorvino as The Woman
* Shane West as Ghost
* Justin Kirk as The Man
* Tony Curran as The Man In Black
* Muse Watson as Mr. Browman
* Deobia Oparei as Woodsman

==References==
{{Reflist|refs=
   
}}

== External links ==
*  
*  
*  

 
 
 
 
 
 
 
 
 


 