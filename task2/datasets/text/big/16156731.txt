Street of Shame
{{Infobox film
| name           = Street of Shame
| image          =
| image_size     =
| caption        = Theatrical release poster
| director       = Kenji Mizoguchi Yasuzo Masumura (assistant director)
| producer       = Masaichi Nagata (producer)
| writer         = Masashige Narusawa (writer) Yoshiko Shibaki (novel)
| narrator       =
| starring       = 
| music          = Toshiro Mayuzumi
| cinematography = Kazuo Miyagawa
| editing        = Kanji Sugawara
| studio         = Daiei Film
| distributor    = 
| released       = March 18, 1956
| runtime        = 87 minutes
| country        = Japan
| language       = Japanese
| budget         =
| preceded_by    =
| followed_by    =
}}
 1956 black-and-white Japanese film directed by Kenji Mizoguchi. It is the personal tales of several Japanese women of different backgrounds who work together in a brothel. It was Mizoguchis last film.

The film is based on the novel Susaki no Onna by Yoshiko Shibaki.

==Plot Summary==
 Diet considers a ban on prostitution, the womens daily dramas play out. Each has dreams and motivations. Hanae is married, her husband unemployed; they have a young child. Yumeko, a widow, uses her earnings to raise and support her son, whos now old enough to work and care for her. The aging Yorie has a man who wants to marry her. Yasumi saves money diligently to pay her debt and get out; she also has a suitor who wants to marry her, but she has other plans for him. Mickey seems the most devil-may-care, until her father comes from Kobe to bring her news of her family and ask her to come home.

==Cast==
* Machiko Kyō as Mickey
* Ayako Wakao as Yasumi
* Aiko Mimasu as Yumeko
* Michiyo Kogure as Hanae
* Kumeko Urabe as Otane
* Yasuko Kawakami as Shizuko
* Hiroko Machida as Yorie
* Eitarō Shindō as Kurazō Taya
* Sadako Sawamura as Tatsuko Taya
* Toranosuke Ogawa as Mickeys father
* Bontarō Miyake as nightwatch
* Daisuke Katō as president of Brothel Owners Association
* Kenji Sahara

The production designer was Hiroshi Mizutani.

==Legacy==
  (1974) directed by Tatsumi Kumashiro is a remake of this film. 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 


 