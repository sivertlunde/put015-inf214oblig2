Broken Lullaby
{{Infobox film
| name           = Broken Lullaby
| image          = BrokenLullabyPoster.jpg
| image_size     =
| caption        = Original poster
| director       = Ernst Lubitsch
| producer       = Ernst Lubitsch
| writer         = Samson Raphaelson Ernest Vajda Based on a play by Maurice Rostand
| narrator       =
| starring       = Lionel Barrymore Nancy Carroll Phillips Holmes
| music          = W. Franke Harling
| cinematography = Victor Milner
| editing        =
| studio         =
| distributor    = Paramount Pictures
| released       =  
| runtime        = 76 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} Reginald Berkeley. 

==Plot==
Haunted by the memory of Walter Holderlin, a soldier he killed during World War I, French musician Paul Renard (Holmes) confesses to a priest, who grants him absolution. Using the address on a letter he found on the dead mans body, Paul then travels to Germany to find his family.

Because anti-French sentiment continues to permeate Germany, Dr. Holderlin (Barrymore) initially refuses to welcome Paul into his home, but changes his mind when his sons fiancée Elsa identifies him as the man who has been leaving flowers on Walters grave. Rather than reveal the real connection between them, Paul tells the Holderlin family he was a friend of their son, who attended the same musical conservatory he did.

Although the hostile townspeople and local gossips disapprove, the Holderlins befriend Paul, who finds himself falling in love with Elsa (Carroll). When she shows Paul her former fiancés bedroom, he becomes distraught and tells her the truth. She convinces him not to confess to Walters parents, who have embraced him as their second son, and Paul agrees to forego easing his conscience and stays with his adopted family. Dr. Holderlin presents Walters violin to Paul, who plays it while Elsa accompanies him on the piano.

==Production==
The films original title, The Man I Killed, was changed to The Fifth Commandment to avoid giving "wrong impressions in the minds of the public about the character of the story." It ultimately was released as Broken Lullaby.  .Though it was screened on 27th Sept 1932 in London as The Man I Killed, https://debsdiaries.wordpress.com/  

The film was presented at the Venice International Film Festival. 

According to The Hollywood Reporter, Czechoslovakia banned the film due to its "pacifistic theme."  

The film was screened at the 2006 San Sebastián International Film Festival as part of an Ernst Lubitsch retrospective. 

==Cast (in credits order)==
*Lionel Barrymore as Dr H. Holderlin
*Nancy Carroll as Fraulein Elsa, Walters Fiancee
*Phillips Holmes as Paul Renard
*Louise Carter as Frau Holderlin
*Lucien Littlefield as Herr Walter Schultz
*Tom Douglas as Walter Holderlin, German Soldier Killed By Paul
*Zasu Pitts as Anna, Holderlins Maid
*Frank Sheridan as Priest
*George Bickel as Herr Bresslauer, Dress Shop Owner
*Emma Dunn as Frau Miller
*Reginald Pasch as Fritzs Father
*Rodney McLennan as War Veteran

==Critical reception== habiliments of the players and their make-up."   

Pauline Kael called Phillips Holmes "unspeakably handsome but an even more unspeakable actor," thought Nancy Carroll was "miserably miscast," and added, "Lubitsch cant entirely escape his own talent, and the film is beautifully crafted, but he mistook drab, sentimental hokum for ironic, poetic tragedy."  

Time Out London said, "The acting is overwrought; the dialogue is uniformly on-the-nose. Yet purity is the word that comes to mind: The movie is a nakedly sincere ode to the power of sympathy, and its not to be missed."  

==DVD release== fullscreen format and has an audio track in English and subtitles in English and Spanish.

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 