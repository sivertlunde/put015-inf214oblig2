Hercules (1958 film)
{{Infobox film
| name = Hercules (Le fatiche di Ercole)
| image = Hercules VHS cover.JPG
| image size = 
| caption = VHS cover
| director = Pietro Francisci
| producer = Federico Teti
| writer = Ennio De Concini Pietro Francisci Gaio Frattini
| starring = Steve Reeves Sylva Koscina Fabrizio Mioni
| music = Enzo Masetti
| cinematography = Mario Bava
| editing = Mario Serandrei
| released =  
| country = Italy
| distributor = Lux Film (1958, Italy) Warner Bros. Pictures (1959, USA, dubbed)
| runtime = 107 min. Italian
| gross = $4.7 million (est. US/ Canada rentals) 
}}
 Hercules myths Quest for the Golden Fleece.  The film stars Steve Reeves as the titular hero and Sylva Koscina as his love interest Princess Iole. Hercules was directed by Pietro Francisci and produced by Federico Teti. The film spawned a sequel, Hercules Unchained ( ), that also starred Reeves and Koscina.
 peplum (or "sword and sandal") films featuring oiled bodybuilders as mythological heroes and gladiators battling monsters, despots, and evil queens.

==Source material== Ulysses as Hercules protégé, and Hercules renunciation of his immortality in order to experience life as a mortal man.

==Plot and cast==
Hercules (Steve Reeves) is on the road to the court of King Pelias of Iolcus (Ivo Garrani) to tutor Pelias son Prince Iphitus (Mimmo Palmara) in the use of arms.  Pelias beautiful daughter Princess Iole (Sylva Koscina) updates Hercules on the history of her fathers rise to power and the theft of the kingdoms greatest treasure, the Golden Fleece.  Some suspect—and it eventually proves true—that King Pelias has acquired the throne through fratricide.  Hercules and Iole are attracted to each other and a romance eventually develops.
 Ulysses and his father Laertes (Gabriele Antonini and Andrea Fantasia), Argos (Aldo Fiorelli), the twins Castor and Pollux (Fulvio Carrara and Willi Colombini), the lyre-strumming Orpheus (Gino Mattera), the physician Aesculapius (Gian Paolo Rosmino) and others.

After weathering a tempest at sea, the Argonauts dally in a lush garden-like country with Antea, the Queen of the Amazons (Gianna Maria Canale) and her ladies (Gina Rovere and Lily Granado).  Jason falls in love with Antea, but, when the Amazons plot the deaths of the heroes, Hercules forces Jason to board the Argo and secretly set sail in the night.  On the shores of Colchis, the heroes battle hairy ape-men while Jason slays a dragon and retrieves the Golden Fleece.  The Argonauts embark for home with their prize.

In Iolcus, the populace greet the returning heroes but Pelias and his henchman Eurysteus (Arturo Dominici) steal the Golden Fleece, deny Jasons claim, and plot his destruction.  A tense battle between Pelias forces and the heroes follows.  Hercules halts Pelias cavalry dead in its tracks by toppling the portico of the palace upon them.   The defeated Pelias drinks poison.  Jason ascends the throne while Hercules and Iole set sail for new adventures.

Subplots involve the death of Pelias headstrong son Prince Iphitus, and exploits for Hercules resembling the Labors of the Nemean Lion and the Cretan Bull.  Cast includes (Lidia Alfonsi) as Ioles Maid, (Afro Poli) as Jasons mentor Chiron, Aldo Pini as Tifi, Spartaco Nale and Paola Quattrini in the uncredited role of Young Iole. 

==Production==
The film was shot in Eastmancolor, using the French widescreen process List of anamorphic format trade names|Dyaliscope. An American Bison served as the Cretan Bull. 
Sound effects for some action sequences were lifted from MGMs Forbidden Planet (1956) and the Japanese film Godzilla (1954).

==Distribution==
American producer Joseph E. Levine acquired the U.S. distribution rights to the film, and, due in part to his wide release (the film opened in 175 theaters in the New York City area alone) a long with an intensive promotional campaign, Hercules became a major box-office hit. 

Warners advanced him $300,000 for the privilege of distributing the film in the US. 

==Merchandise== Dell comic book adaptation with illustrations by John Buscema and a 33 RPM long-playing RCA Victor recording of the films soundtrack.  

==Gallery==
  Magazine advertisement Boston premiere Dell comic book
 
 

==Biography==
* 

==References==
 

==External links==
 
*   Retrieved 29 August 2008.
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 