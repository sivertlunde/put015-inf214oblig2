Mojados: Through the Night
{{Infobox film
| name           = Mojados: Through the Night
| image          = 
| caption        = Production Still Tommy Davis
| cinematographer       = Tommy Davis
| cinematography = Tommy Davis
| starring       = N/A
| music          = Sin Panache
| editing        = Tommy Davis
| studio         = Tdrand
| released       =  
| runtime        = 55 minutes
| country        = Mexico, United States
| language       = Spanish, English
| budget         = 
| gross          = 
}}
Mojados: Through the Night is a 2004 documentary film directed by Tommy Davis.  The film documents the journey of four men   as they trek 120 miles across the Texas desert.

==Summary==

Filmed over the course of ten days the film follows four men into the world of illegal immigration. Alongside Bear, Tiger, Handsome, and Old Man, director Davis takes a 120 mile cross-desert journey   that has been traveled innumerable times by nameless immigrants like these four migrants from Michoacán, Mexico. Davis tells the stories of these migrants as their dehydrated days evading the U.S. Border Patrol turn into sub-zero nights filled with barbed wire, storms and ever-present confrontation with death.  


==Awards ==
*SXSW Film Festival - Audience Award
*Arizona Intl. Film Festival - Best Documentary
*Santa Fe Film Festival - Best Documentary
*San Antonio Underground - Grand Prize
*Kansas International Film Festival - Audience Award

==References==
{{reflist|refs=
    

   

    
}}

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 
 

 