Pu-239 (film)
{{Infobox Film
| name           = Pu-239 / The Half-Life of Timofey Berezin
| image          = Pu-239 movie poster.jpg
| image_size     =
| caption        =
| director       = Scott Z. Burns
| producer       = Charlie Lyons Miranda de Pencier Guy J. Louthan
| writer         = Scott Z. Burns
| based on       =  
| starring       = Paddy Considine Radha Mitchell Oscar Isaac
| music          = Abel Korzeniowski
| cinematography = Eigil Bryld
| editing        = Tatiana S. Riegel Leo Trombetta
| distributor    = Beacon Pictures HBO Films
| released       =  
| runtime        = 97 mins
| country        = United Kingdom
| language       = English
| budget         = 
}}
Pu-239 is a 2006 film directed by Hollywood producer Scott Z. Burns based on the book PU-239 and Other Russian Fantasies written by Ken Kalfus. The film was shown twice at the 2006 Toronto International Film Festival under the title The Half Life of Timofey Berezin, before being distributed by HBO Films under its original working title. Pu-239 is the chemical symbol for plutonium-239 ( 239 Pu), a radioactive isotope  of the chemical element plutonium.

==Plot== criticality malfunction. The facilitys draconian managers maintain his exposure was a survivable 100 Röntgen equivalent man|rems, while accusing him of sabotage and suspending him without pay. Loyal coworkers, however, help Timofey discover the truth that he was exposed to 1,000 rems of radiation. Suffering from acute radiation poisoning, he has only days to live.

Before Timofeys adoring wife, Marina (Radha Mitchell), is fully aware of his fate, he leaves for Moscow, on a mission to secure a better future for her and their young son. He hooks up with a small-time gangster, Shiv (Oscar Isaac), in hopes of finding a buyer for a selfmade canister of a little over 100 grams of weapons-grade plutonium salt he has stolen. It is 1995, only a few years after the dissolution of the Soviet Union, and they spend their time hurtling through the hotels, nightclubs and private palaces of the new Moscow underworld, ricocheting between two rival crime lords (Nikolaj Lie Kaas and Steven Berkoff). However, what Timofey and Shiv never realize is that they are both caught in the same vise: trying to find a way free of a certain fate; hoping to do right by their loved ones before it is too late.

==Awards== Eddie - 2008, Best Edited Miniseries or Motion Picture for Non-Commercial Television Excellence in Production Design Award - 2008, Television Movie or Mini-Series

==See also==
* Radium Girls
* Walking ghost phase
* Half-life

==External links==
*  
*  
*  
*   at the TIFF 2006 archival site.

 
 
 
 
 
 
 