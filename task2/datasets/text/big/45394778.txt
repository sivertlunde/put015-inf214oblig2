Nightlight (2015 film)
{{Infobox film
| name           = Nightlight
| director       = Scott Beck Bryan Woods
| producer       = Michael London Norton Herrick Darren Brandl Janice Williams
| screenplay     = Scott Beck Bryan Woods
| starring       = Shelby Young Chloe Bridges Carter Jenkins Mitch Hewer Taylor Murphy
| cinematography = Andrew M. Davis
| editing        = Russell Andrew
| studio         = Herrick Entertainment Lionsgate
| released       =   
| runtime        = 85 minutes 
| country        = United States
| language       = English
}}

Nightlight is a 2015 American supernatural thriller film written and directed by Scott Beck & Bryan Woods. The film stars Shelby Young, Chloe Bridges, Carter Jenkins, Mitch Hewer, and Taylor Murphy. The film is also told from a singular point of view and is described as "A groundbreaking story device that has heightening suspense, while turning the found footage genre on its head."

== Plot ==

Five friends go into the woods of a mysterious forest for a fun night of flashlight games. But their night in the woods turns into a deadly experience when they disturb a demonic presence in the forest that has a long history of troubled young people contemplating suicide. 
   
== Cast ==
* Shelby Young as Robin
* Chloe Bridges as Nia
* Carter Jenkins as Chris
* Mitch Hewer as Ben
* Taylor Murphy as Amelia
* Kyle Fain as Ethan


==Home Media==
The film will be released on DVD courtesy of Lionsgate Home Entertainment on May 26, 2015. 

== References ==
 

== External links ==
*  
*  

 
 