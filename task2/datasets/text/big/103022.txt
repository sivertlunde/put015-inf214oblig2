An Unmarried Woman
{{Infobox film
| name           = An Unmarried Woman
| image          = Unmarried_woman.jpg
| director       = Paul Mazursky
| writer         = Paul Mazursky Michael Murphy Cliff Gorman
| music          = Bill Conti
| cinematography = Arthur J. Ornitz
| distributor    = 20th Century Fox
| released       =  
| country        = United States
| language       = English
| runtime        = 130 minutes
| budget = $2,515,000 
| gross          = $24,000,000 
}}
An Unmarried Woman is a 1978 American comedy-drama film written and directed by Paul Mazursky, and starring Jill Clayburgh.

The film was nominated for the Academy Award for Best Picture and Clayburgh was nominated for an Academy Award for Best Actress.

==Plot== Michael Murphy) leaves her for a younger woman. The film documents Ericas attempts at being single again, where she suffers confusion, sadness, and rage.
 British artist (Alan Bates).

==Cast==
* Jill Clayburgh as Erica
* Alan Bates as Saul Michael Murphy as Martin
* Cliff Gorman as Charlie
* Patricia Quinn as Sue (as Pat Quinn)
* Kelly Bishop as Elaine
* Lisa Lucas as Patti Linda Miller as Jeannette Andrew Duncan as Bob
* Daniel Seltzer as Dr. Jacobs
* Matthew Arkin as Phil
* Penelope Russianoff as Tanya
* Novella Nelson as Jean
* Raymond J. Barry as Edward
* Ivan Karp as Herb Rowan
 Paul Jenkins who taught Alan Bates his painting technique for his acting role.   

==Awards== Best Picture, Best Actress Best Writing, Screenplay Written Directly for the Screen. Mazurskys screenplay won awards from the New York Film Critics Circle and the Los Angeles Film Critics Association.
 Best Actress at the 1978 Cannes Film Festival.   

The film was also nominated for several 1978 New York Film Critics Circle Awards, including Best Film, Best Direction, Best Actress (for Jill Clayburgh) and Best Supporting Actress (for Lisa Lucas). 

==Reception==
Vincent Canby wrote "Miss Clayburgh is nothing less than extraordinary in what is the performance of the year to date. In her we see intelligence battling feeling — reason backed against the wall by pushy needs." 

  the popular success that his films Blume in Love, Harry and Tonto and Next Stop, Greenwich Village should have given him  -
Erica (Jill Clayburgh),  the heroine, sleeps in a T-shirt and bikini panties. There are so few movies that deal with recognizable people that this detail alone is enough to pick up ones spirits...Jill Clayburgh has a cracked , warbly voice - a modern polluted-city huskiness...When Ericas life falls apart and her reactions go out of control, Clayburghs floating, not-quite-sure, not-quite-here quality is just right." 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 