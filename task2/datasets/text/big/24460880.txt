The Beauty Prize (film)
 
{{Infobox film
| name           = The Beauty Prize
| image          =
| caption        =
| director       = Lloyd Ingraham
| producer       =
| writer         = Winifred Dunn and Nina Wilcox Putnam Pat OMalley Eddie Phillips
| cinematography =
| editing        =
| distributor    = Metro-Goldwyn-Mayer
| released       =   reels (approximately 60 minutes)
| country        = United States Silent English English intertitles
| budget         =
}}

The Beauty Prize is a 1924 film directed by Lloyd Ingraham and starring Viola Dana.

==Synopsis==
A manicurist wins a beauty contest while posing as a debutante, and reveals her deception via a new craze, the radio.

==Cast==
* Viola Dana - Connie Du Bois Pat OMalley - George Brady Eddie Phillips - Eddie Schwartz
* Eunice Murdock Moore - Madame Estelle (as Eunice Vin Moore)
* Edward Connelly - Pa Du Bois
* Edith Yorke - Ma Du Bois

==External links==
* 

 
 
 
 
 
 
 
 
 


 