The Truth (1960 film)
{{Infobox film
| name           = The Truth (La Vérité)
| image          = Verite.jpg
| caption        = Film poster
| director       = Henri-Georges Clouzot
| producer       = Raoul Lévy
| writer         = Henri-Georges Clouzot Véra Clouzot Simone Drieu Jérôme Géronimi Michèle Perrein Christiane Rochefort
| starring       = Brigitte Bardot Charles Vanel Paul Meurisse
| music          = 
| cinematography = Armand Thirard
| editing        = Albert Jurgenson
| studio         = Han Productions
| distributor    = Kingsley-International Pictures (US)
| released       = 2 November 1960 (France) 26 June 1961 (US)
| runtime        = 130 minutes
| country        = France
| language       = French
| budget         =  studio = C. E. I. A. P. Han Productions Ina Production 
| gross          = 5,694,993 admissions (France)   at Box Office Story 
}}

The Truth ( ) is a 1960 French film directed by Henri-Georges Clouzot and starring Brigitte Bardot. The film was nominated for the Academy Award for Best Foreign Language Film.   

==Plot==
Dominique Marceau is a young Frenchwoman on trial for killing her lover, Gilbert.

The prosecuting attorney, Eparvier, claims it was an act of premeditated murder that warrants the death penalty. The defense attorney, Guérin, maintains that it was an act of passion and not punishable by death.

During the course of the trial, we see the events that led up to the crime. Dominques parents let her move to Paris after she tried to kill herself when they initially refused. She had been living on the Paris Left Bank with her violinist sister Anne, partying and sleeping with men.

She meets Gilbert, her sisters boyfriend, a music student. Dominique seduces Gilbert and he falls for her and proposed but she turns him down. They live together for a time but Dominique struggles with domesticity and Gilbert is constantly worried she will cheat on him. Eventually they break up.

Over time Dominique becomes a prostitute while Gilbert becomes a famous conductor. Gilbert and Anne become engaged. Dominique realises that Gilbert was the only man she ever loved and they sleep together. However he kicks her out the next day.

She tries to kill herself to prove her love but when he mocks her she shoots him. She then attempts suicide but is found and rescued by the police.

At the end of the trial, Dominque realises the jury is unconvinced that her love for Gilbert was real. She returns to her prison cell and slashes her wrists with a piece of broken mirror.

== Cast ==
*Brigitte Bardot as Dominique Marceau
*Charles Vanel as Maître Guérin
*Paul Meurisse as Maître Éparvier
*Sami Frey as Gilbert Tellier
*Marie-José Nat as Annie Marceau
*Louis Seigner as Le président des assises
*André Oumansky as Ludovic
*Jacqueline Porel as La secrétaire de Me Guérin
*Jean-Loup Reynold as Michel
*Christian Lude as M. Marceau
*Suzy Willy as Mme Marceau
*Barbara Sommers as Daisy (as Barbara Sohmers)
*René Blancard as Lavocat général
*Fernand Ledoux as Le médecin légiste
*Claude Berri as Georges

==Production==
Yves Montand was originally meant to star. Monroe, Bardot--Such Is Yves Work
Louella Parsons:. The Washington Post, Times Herald (1959-1973)   24 Mar 1960: B10. 

Jean-Paul Belmondo, Jean-Pierre Cassell and Jean Louis Trintignant were all considered for the lead role - Trintignant was Bardots choice - before Clouzot decided to go with Sami Frey.   accessed 30 December 2014 

Philippe Leroy-Beaulieu, one of the male leads, was fired during shooting. It was rumoured this was done because he had an affair with Bardot. Leroy-Beaulieu then sued the producer for damages of 300,000 francs. Charrier had a nervous breakdown and was hospitalised for two months. Vera Clouzot had a nervous breakdown in July. In August Clouzot had a heart attack and filming was suspended for a week. Brigitte: Bardot
By MAURICE ZOLOTO. The Washington Post, Times Herald (1959-1973)   09 Oct 1960: AW8.  Also Bardots secretary of four years sold secrets about her to the press. 

During filming, Bardot had an affair with Sammi Frey which resulting in her breaking up with her then husband Jacques Charrier. In September 1960 Bardot had an argument with Charrier and then attempted suicide by slashing her wrist. (Charrier had earlier attempted suicide himself.)  BRIGITTE BARDOT TRIES VILLA HIDEOUT SUICIDE: Reported Out of Danger Incomplete Source
Los Angeles Times (1923-Current File)   29 Sep 1960: 1.  Brigitte Tries to End Her Life: Latest Love Spat Depresses Her
Chicago Daily Tribune (1923-1963)   30 Sep 1960: 5. 

==Reception==
In the words of the New York Times "probably no film in recent years - at least in France - has been subjected to so much advance attention. Two years in the planning, six months in the shooting, sets sealed to the press, and all culminating in the suicide attempt of the dramas star, Brigitte Bardot. The public had been told that Clouzot was turning B.B. into a real actress." REFLECTIONS ON THE PARISIAN SCREEN SCENE
By CYNTHIA GRENIER. New York Times (1923-Current file)   20 Nov 1960: X9. 

The film was a massive box office hit in France, Bardots biggest ever success at the box office. 

===Critical Reception===
The Los Angeles Times called the film "an amazing picture, a tour de force from all concerned. It is at once immoral, amoral and strangely moral." Bardot Film: Immoral, Amoral, Moral
Scheuer, Philip K. Los Angeles Times (1923-Current File)   19 Mar 1961: l3. 

==See also==
* List of submissions to the 33rd Academy Awards for Best Foreign Language Film
* List of French submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 
*  
*  at TCMDB
*  at New York Times
 
 

 
 
 
 
 
 
 
 
 