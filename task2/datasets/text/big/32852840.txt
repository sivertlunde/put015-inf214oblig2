Braveheart (1925 film)
{{Infobox film
| name           = Braveheart Mary OHara from the play by William C. deMille
| image	         = Braveheart (1925) - film poster.jpg
| starring       = Rod La Rocque Lillian Rich
| director       = Alan Hale, Sr.
| music          = 
| producer       = 
| distributor    = 
| released       =  
| country        = United States
| runtime        = 71 minutes
| language       = Silent film English intertitles
| budget         = 
| gross          = 
}}
Braveheart is a 1925 silent film western directed by Alan Hale, Sr. and starring Rod La Rocque. The story focuses on members of a tribe of Indians who are being intimidated by the owners of a canning company seeking to violate the treaty protecting the tribes fishing grounds.  

==History==
A project by Cecil B. DeMille, {{cite encyclopedia
 | author1 = Alexander Ewen | author2 = Jeffrey Wollock| title =Strongheart, Nipo | encyclopedia =Encyclopedia of the American Indian in the Twentieth Century
 | url = http://www.fofweb.com/History/HistRefMain.asp?iPin=ENAIT489&SID=2&DatabaseName=American+Indian+History+Online&InputText=%22scout%22&SearchStyle=&dTitle=Strongheart%2C+Nipo&TabRecordType=Biography&BioCountPass=227&SubCountPass=220&DocCountPass=7&ImgCountPass=13&MapCountPass=1&FedCountPass=&MedCountPass=14&NewsCountPass=0&RecPosition=191&AmericanData=&WomenData=&AFHCData=&IndianData=Set&WorldData=&AncientData=&GovernmentData= | volume =online
 | publisher = Facts On File, Inc.
 | year = 2014
 | accessdate = August 19, 2014}}  {{cite journal
 | last = Strongheart | first = Nipo T.
 | date = Autumn 1954
 | title = History in Hollywood
 | journal = The Wisconsin Magazine of History
 | volume = 38 | issue = 1 | pages = 10–16, 41–46
 | jstor = 4632754}}  initially it is named "Strongheart" after a play written by his brother William C. deMille circa 1904 and produced on Broadway in 1905  as his first major success.  A film had been developed in 1914 from it.    {{cite thesis
|author=Lori Lynn Muntz
|title=Representing Indians: The Melodrama of Native Citizenship in United States Popular Culture of the 1920s
 | pages = 265
 | publisher =Department of English, University of Iowa
 | location =
 | date =May 2006
| id = UMI3225654
 | url =http://books.google.com/books?id=pxKBdpjzbCwC&pg=PA26
|isbn=978-0-542-79588-6 Native American topics, was asked to rewrite the movie and he included elements referring to the Yakima Nation and had the hero succeed in preserving Indian fishing rights,  a topic of some recent interest. {{cite news
 | title =Yakima Indians see governor and get old fishing rights
 | newspaper =The Oregon Daily Journal
 | location =Portland, Oregon
 | page =1
 | date =9 January 1920
 | url =http://www.newspapers.com/clip/909750/yakima_fishing_rights/ a canine star. for more on the movie see      Subsequently the DeMille film was retitled and released as Braveheart. Nipo T. Strongheart played a roll in the film playing a Medicine Man and collaborating on the screenplay.   {{cite news
 | title =Film Actor works with Ty Jr, now
 | newspaper =The Deseret News
 | location =Salt Lake City, Utah
 | page =4
 | date =Aug 31, 1952
 | url =http://news.google.com/newspapers?nid=336&dat=19520831&id=JhYkAAAAIBAJ&sjid=oU0DAAAAIBAJ&pg=3355,5806967
| accessdate = August 25, 2014 }}  A news story covering the work is echoed a couple places – New York, {{cite news
 | title ="Braveheart" at the American
 | newspaper =The Troy Times,
 | location =Troy, N. Y,
 | page = ?, (left down from top)
 | date =February 20, 1926
 | url =http://fultonhistory.com/Newspaper%2018/Troy%20NY%20Times/Troy%20NY%20Times%201926/Troy%20NY%20Times%201926%20-%200741.pdf
| accessdate = August 25, 2014 }}  and California. {{cite news
 | title =Nipo Strongheart is "Braveheart" is Real Medicine Man
 | newspaper =Covina Argus
 | location =Covina, California
 | page =3
 | date =12 February 1926
 | url =http://www.newspapers.com/clip/881003/nipo_strongheart_in_show/
| accessdate = August 23, 2014 }}  Sometimes advertising for performance-lectures of Nipo Strongheart from then on would have him in Indian costume as well as a scene from the movie where he was dressed in normal attire. 

Nipo Strongheart was able to include Indians not dressed up in Indian costumes and succeeding in redressing wrongs done to them – however much the lead role was still a white man in Indian costume.    

One scholar said:  The court sequence is heavily and multiply textualized… conveying legal arguments and judgements that refer to treaties.… the judges decision parses the meaning of the treaty text itself: "We have examined the Federal treaty with the Indians and find that it gives them the right to fish where and when they please, without limitation by State tax or private ownership."  

A restoration of Braveheart was done by the "Washington Film Preservation Project" and the film shown at a Yakama Nation Native American Film Festival in 2006 {{cite news
| last =Nowacki
 | first = Kim
 | title =Native American Film Festival -- Preservation celebration
 | newspaper =Yakima Herald-Republic
 | location =Yakima Washington
 | page =?
 | date =November 10, 2006
 | url =
| accessdate = August 26, 2014}}  and 2007. {{cite news
| last =Nowacki
 | first = Kim
 | title =Indian filmmakers getting their stories out
 | newspaper =Yakima Herald-Republic
 | location =Yakima Washington
 | page =?
 | date =November 9, 2007
 | url =
 | accessdate = August 26, 2014}} 

==Cast==
*Rod La Rocque – Braveheart
*Lillian Rich – Dorothy Nelson
*Robert Edeson – Hobart Nelson
*Arthur Housman – Frank Nelson
*Frank Hagney – Ki-Yote
*Tyrone Power, Sr. – Standing Rock
*Jean Acker – Sky-Arrow
*Sally Rand – Sally Vernon
*Henry Victor – Sam Harris

Uncredited is Nipo T. Strongheart. {{cite encyclopedia
 | author1 = Alexander Ewen  | author2 = Ewen, Alexander| title =Strongheart, Nipo | encyclopedia =Encyclopedia of the American Indian in the Twentieth Century
 | url = http://www.fofweb.com/History/HistRefMain.asp?iPin=ENAIT489&SID=2&DatabaseName=American+Indian+History+Online&InputText=%22scout%22&SearchStyle=&dTitle=Strongheart%2C+Nipo&TabRecordType=Biography&BioCountPass=227&SubCountPass=220&DocCountPass=7&ImgCountPass=13&MapCountPass=1&FedCountPass=&MedCountPass=14&NewsCountPass=0&RecPosition=191&AmericanData=&WomenData=&AFHCData=&IndianData=Set&WorldData=&AncientData=&GovernmentData= | volume =online
 | publisher = Facts On File, Inc.
 | year = 2014
 | accessdate = August 19, 2014}} 

==Further reading==
 

{{cite thesis
|author=Lori Lynn Muntz
|title=Representing Indians: The Melodrama of Native Citizenship in United States Popular Culture of the 1920s
 | pages = 265
 | publisher =Department of English, University of Iowa
 | location =
 | date =May 2006
| id = UMI3225654
 | url =http://books.google.com/books?id=pxKBdpjzbCwC&pg=PA26
|isbn=978-0-542-79588-6
 | accessdate = August 26, 2014 }}

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 