Separate but Equal (film)
 

{{Infobox film
| name           = Separate But Equal
| image          = Separate But Equal.jpg
| image size     = 
| alt            = 
| caption        = VHS cover art
| director       = George Stevens, Jr.
| producer       = Stan Margulies   George Stevens, Jr.
| writer         = George Stevens, Jr.
| screenplay     = 
| story          = 
| narrator       = 
| starring       = Sidney Poitier   Gloria Foster  Tommy Hollis 
| music          = Carl Davis
| cinematography = Nick Knowland
| editing        = John W. Wheeler New Liberty Films Republic Pictures
| distributor    = 
| released       =  
| runtime        = 194 minutes (3 hours, 14 minutes)
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} American television Supreme Court desegregation case Brown v. Board of Education, based on the phrase "Separate but equal".
 Chief Justice 1924 US presidential election), Cleavon Little as lawyer and judge Robert L. Carter, and Lynne Thigpen as Ruth Alice Stovall.  It was Burt Lancasters final movie before his death. In 1991, the Academy of Television Arts and Sciences rewarded the film Outstanding Miniseries award. 

==Plot==
The issue before the United States Supreme Court is whether the equal protection clause of the 14th Amendment of the U.S. Constitution mandates the individual states to desegregate public schools; that is, whether the nations "separate but equal" policy heretofore upheld under the law, is unconstitutional.  The issue is placed before the Court by Brown v. Board of Education and its companion case, Briggs v. Elliott.  Many of the justices personally believe segregation is morally unacceptable, but have difficulty justifying the idea legally under the 14th Amendment.  Marshall and Davis argue their respective cases.  Marshall argues the equal protection clause extends far enough to the states to prohibit segregated schools.  Davis counters that control of public schools is a "states rights" issue that Congress never intended to be covered by the 14th Amendment when it was passed.

Taking the case under advisement, the stalemated justices agree to allow Marshall and Davis an opportunity to re-argue their respective cases as to whether the equal protection clause specifically extends to the desegregation of schools.  In the interim, Chief Justice Fred M. Vinson dies and is replaced by a non-jurist, Governor Earl Warren of California.

Meanwhile, Marshall and his staff are fruitless in finding any research showing the Civil-War era crafters of the 14th Amendment in 1866 intended for schools to be desegregated.  On the other hand, Davis and his Ivy League-educated staff find several examples of segregated schools having existed ever since the passage of the equal protection clause.  Finally, the NAACP staffers discover a quote by Thaddeus Stevens delivered on the floor of the Senate during the debate over the Amendment, which directly states segregation is constitutionally and morally wrong.  They place it at the front of their brief.  Marshalls argument is compelling.

As the case is taken under advisement a second time, new Chief Justice Warren is taken on a tour of Gettysburg by his black chauffeur.  He also realizes that his chauffeur must sleep in the car because there are no lodging places available for him because of his race. Warren discovers a majority of the Court agrees to strike down the "separate but equal" laws; however, it is important to him that the Court be unanimous.  He writes an opinion and takes copies to all of the dissenting justices trying to convince each one of the significance of unanimity.  They finally all agree.  Warren reads his opinion which states that segregation "has no place" in American society.  Even opposing counsel, John W. Davis, privately agrees it is time for society to change.

The film closing acknowledges Thurgood Marshalls own ascent to the Supreme Court in 1967 and explains that the plaintiff in the companion case, a black student named Briggs, never attended an integrated school.

==Casting==
* Sidney Poitier as Thurgood Marshall
* Burt Lancaster as John W. Davis
* Richard Kiley as Earl Warren Robert L. Bob Carter
* Gloria Foster as Buster Marshall
* John McMartin as Gov. James F. Byrnes
* Graham Beckel as Josiah C. Tulley
* Ed Hall as Rev. J.A. Delaine
* Lynne Thigpen as Ruth Alice Stovall
* Macon McCalman as W.B. Springer
* Randle Mell as Charles L. Black, Jr.
* Cheryl Lynn Bruce as Gladys Hampton
* Tommy Hollis as Harry Briggs
* John Rothman as Jack Greenberg
* Damien Leake as Kenneth and Mamie Clark|Dr. Kenneth Clark
* Mike Nussbaum as Justice Felix Frankfurter
* William Cain as Judge Julius Waties Waring|J. Waties Waring
* Michael Flippo as Tom
* Michael L. Nesbitt as Man In Audience
* Ric Reitz as Mr. Straight
* Edward Seamon as self
* Pearce Venning as Harry Briggs Jr.
* Laurens Moore as Judge
* Michael Noel as Businessman in train station
* I. Russell Weinstein as Reporter at phone booth

==See also==
* African-American Civil Rights Movement in popular culture

==References==
 

==External links==
* 

 

 
 
 
 
 