Jem and the Holograms (film)
 
{{Infobox film
| name           = Jem and the Holograms
| image          = File:Jem Movie Teaser Poster.png
| alt            = 
| caption        = Teaser poster
| director       = Jon M. Chu
| producer       = {{Plainlist|
* Jason Blum
* Scooter Braun
* Jon M. Chu
* Brian Goldner
* Stephen Davis
* Bennett Schneir}}
| writer         = Ryan Landels 
| based on       =  
| starring       = {{Plainlist|
* Aubrey Peeples
* Stefanie Scott
* Hayley Kiyoko Aurora Perrineau
* Juliette Lewis
* Ryan Guzman
* Molly Ringwald}}
| music          = Nathan Lanier
| cinematography = Alice Brooks
| editing        = Jillian Twigger Moul
| studio         = {{Plainlist|
* Hasbro Studios
* Allspark Pictures
* Blumhouse Productions SB Projects}} Universal Pictures
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} romantic Musical musical Fantasy fantasy comedy animated television series of the same name. Directed by Jon M. Chu and written by Ryan Landels, the film is scheduled to be released on October 23, 2015.

==Plot==
In a hyper-linked social media age, an orphaned teenage girl, Jerrica Benton (Aubrey Peeples), becomes an online recording sensation, and she and her sisters embark on a music-driven scavenger hunt - one that sends them on an adventure across Los Angeles - in an attempt to unlock a final message left by her father. 

==Cast==
* Aubrey Peeples as Jerrica Benton/Jem, the lead singer of the band.
** Isabella Rice as young Jerrica
* Stefanie Scott as Kimber Benton, Jerrica Benton/Jems younger sister and the keyboardist as well as the primary songwriter of the band.
* Hayley Kiyoko as Aja Leith, the lead guitarist of the band and right-hand woman to Jerrica/Jem. Aurora Perrineau as Shana Elmsford, the bass guitarist of the band.
** Wynter Perrineau as young Shana
* Juliette Lewis as Erica Raymond, the ruthless and corrupt co-owner of Starlight Records. 
* Ryan Guzman as Rio Pacheco, the road manager and engineer for the band and Jerrica/Jems love interest.
* Molly Ringwald as Mrs. Bailey, the caretaker of Starlight House.
* Nathan Moore as Zipper, the main antagonist in the film.
* Barnaby Carpenter as Emmet Benton, Jerrica and Kimbers late father.
* Nicholas Braun as Brad

Jem voice actresses Samantha Newark, Jerrica/Jems speaking voice,  and Britta Phillips, Jerrica/Jems singing voice, will have cameo appearances.

==Production==
Given the recent success of  .   

===Casting=== Aurora Perrineau as Shana.  On April 30, actor Ryan Guzman was cast as Rio.  On May 19, Juliette Lewis was added to the cast of the film.    On May 20, Molly Ringwald joined the film. 

===Filming=== Van Nuys,  later on May 19, shooting was underway in Los Angeles.  Shooting ended on May 24, 2014. 

==Release==
On October 16, 2014, Universal and Blumhouse announced the film to be released on October 23, 2015. 

===Marketing===
On February 25, 2015, the first official image from the film was released, featuring Peeples as Jem, Scott as Kimber, and Kiyoko as Aja performing on stage. 

==References==
 

==External links==
 
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 