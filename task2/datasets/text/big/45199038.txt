10 Rules for Sleeping Around
 
{{Infobox film
| name = 10 Rules for Sleeping Around
| image = 
| alt = 
| caption = 
| director = Leslie Greif
| producer = {{Plainlist|
* Harry Basil
* Leslie Greif
* Vince Maggio
* Herb Nanas}}
| writer = Leslie Greif
| starring = {{Plainlist|
* Jesse Bradford
* Chris Marquette
* Tammin Sursok
* Virginia Williams}}
| music = {{Plainlist|
* Nathan Matthew David
* Alan Ett}} Tom Priestley, Jr.
| editing = Richard Nord
| studio = Thinkfactory Media
| distributor = Screen Media Films
| released =  
| runtime = 94 minutes
| country = United States
| language = English
}}
10 Rules for Sleeping Around is a 2013 American romantic comedy film written, produced, and directed by Leslie Greif and starring Jesse Bradford, Chris Marquette, Tammin Sursok, and Virginia Williams.

==Cast==
 
* Jesse Bradford as Vince Johnson
* Chris Marquette as Ben Roberts
* Tammin Sursok as Kate Oliver
* Virginia Williams as Cameron Johnson
* Bryan Callen as Owen Manners
* Lucia Sola as Gabriella "Gabi" Jobim
* Wendi McLendon-Covey as Emma Cooney
* Michael McKean as Jeffrey Fields
* Reid Ewing as Hugh Fields
* Simone Griffeth as Barbara
* Corey Saunders as Matt
* Bill Bellamy as Dwayne
* Mills Allison as Duncan
* Zakiya Alta Lee as Tanisha Jamie Renee Smith as Nikki
* Molly McCook as Jaymee
* Leslie Greif as Foreman Joe Michael Corbett as himself
 

==Release==
10 Rules for Sleeping Around was first released via DVD in the Netherlands on August 13, 2013 before arriving in the United States on April 4, 2014.

===Critical reception=== inAPPropriate Comedy, Not Cool, and The Singing Forest.

The Hollywood Reporter calls the film "a numbingly unfunny sex farce."  1NFLUX Magazine s review was slightly more positive, giving the film a C+. 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 

 