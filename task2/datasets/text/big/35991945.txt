Many Rivers to Cross (film)
{{Infobox film
| name           = Many Rivers to Cross
| image          = Many Rivers to Cross - Film Poster.jpg
| caption        = Theatrical Film Poster Roy Rowland Jack Cummings Harry Brown Guy Trosper
| narrator       = Robert Taylor Eleanor Parker Victor McLaglen
| music          = Cyril J. Mockridge
| cinematography = John F. Seitz
| editing        =
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        =
| country        = United States
| language       = English
| budget         = $1,683,000  . 
| gross          = $3,832,000  
}} Robert Taylor.

==Plot==
Kentucky, the late 1700s: A traveling preachers coming to town, but Miles Henderson is upset because Cissie Crawford seems reluctant to marry him. She seems more interested in a handsome trapper whos just arrived in the territory, Bushrod Gentry.

Cissies life is saved by Bushrod after shes attacked by Shawnee tribesmen, but hes a confirmed bachelor who lets her down gently. While traveling on his own, Bushrod is wounded by the Indians and in danger until another woman, Mary Stuart Cherne, saves his life.

Feeling love at first sight, Mary takes him home to her Scottish-born father, Cadmus, and their Indian servant, Sandak, to heal. Her longtime suitor Luke Radford is unhappy about this interloper.

Bushrod again declines a chance to settle down, whereupon an angry Mary ends up keeping him there against his will. With her four brothers keeping a gun on him, Bushrod is forced to marry Mary.

He punches a Justice of the Peace and gets 30 days in jail. Another trapper, Esau Hamilton, has a sick child whose life Bushrod ends up saving. He slips away and intends to be on his own again, but Bushrod comes across Indians who are trying to scalp Mary. He saves her life this time, then accepts his fate as a man in love.

==Cast== Robert Taylor as Bushrod Gentry
* Eleanor Parker as Mary Stuart Cherne
* Victor McLaglen as Cadmus
* Alan Hale, Jr. as Luke Radford
* Darryl Hickman as Miles Henderson
* Betty Lynn as Cissie
* Ralph Moody as Sandak
* Russell Johnson as Banks
* Russ Tamblyn as Cherne
* James Arness as Esau Hamilton
* Richard Garrick as Preacher Ellis

==Reception==
According to MGM records the film earned $2,084,000 in the US and Canada and $1,748,000 elsewhere, resulting in a profit of $533,000. 
==References==
 

==External links==
*  at IMDB

 

 
 
 
 
 
 
 
 
 