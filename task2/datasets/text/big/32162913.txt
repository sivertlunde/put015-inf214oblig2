Wake (film)
 
{{Infobox film
| name           = Wake
| image          = 
| caption        = 
| director       = Ellie Kanner
| producer       = Hal Schwartz Bill Shraga Lennox Wiseley
| writer         = Lennox Wiseley
| narrator       =  Jane Seymour Sprague Grayden David Zayas
| music          = Brad Segal
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 97 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 comedy drama drama romance romance independent Jane Seymour, Danny Masterson, and Marguerite Moreau.

== Summary == Jane Seymour) to confront her past. And as she tries to open herself to the risks of love with Tyler, she realizes she may have more to fear than just a broken heart.

== Cast ==
* Bijou Phillips as Carys
* Ian Somerhalder as Tyler
* Danny Masterson as Shane
* Marguerite Moreau as Lila Jane Seymour as Mrs. Reitman 
* Sprague Grayden as Marissa
* David Zayas as Detective Grayson

== Information ==
The film was the Opening Night film at the 2009 Cinequest Film Festival. It later was a Spotlight Presentation at the Newport Beach Film Festival.  The film was released by E1 Entertainment on DVD and digital platforms on April 20, 2010.  It has played on both Showtime and Starz in the US.  It was released internationally by Moving Pictures.

==External links==
*  
*  

 
 
 
 

 