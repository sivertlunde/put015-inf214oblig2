Off Track
{{Infobox film
| name           = Off Track
| image          = OffTrack.jpg
| alt            = 
| caption        = DVD cover
| film name      = {{Film name
| traditional    = 馬路英雄
| simplified     = 马路英雄
| pinyin         = Mǎ Lù Yīng Xióng
| jyutping       = Maa2 Lou4 Jing1 Hung4 }}
| director       = Cha Cuen-yee
| producer       = Willie Chan Benny Chan
| writer         = 
| screenplay     = Rico Chung
| story          = 
| based on       =
| starring       = Jacky Cheung Max Mok Loletta Lee Ellen Chan
| narrator       = 
| music          = Lowell Lo
| cinematography = Ma Koon-wa
| editing        = Cheung Kwok-kuen
| studio         = Wing Chuen Film Production
| distributor    = Newport Entertainment
| released       =  
| runtime        = 90 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          = HK$5,038,551
}}
 Hong Kong action drama film directed by Cha Chuen-yee and starring Jacky Cheung, Max Mok, Loletta Lee and Ellen Chan. The film was followed by a sequel in 1995, titled Highway Man, which featured a different storyline. 

==Plot==
Joe (Max Mok) is a car fanatic and co-owns an auto repair shop. One time in an illegal street race, Joe collides with Lui (Jacky Cheung) and the two of them were arrested. Since then, Lui and Joe had become enemies. Luis father (Wu Ma) is a seasoned police officer who goes by his principles and detains his own son, and thus, their relationship had grown very sour, and Lui also increases his hatred towards Joe.. On the other hand, Joe meets Ann (Loletta Lee) and falls in love her. However, Joe discovers that Ann is actually Luis younger sister, and Lui tries to destroy their relationship. At first, Joe resisted Lui for the sake of Ann, but Lui becomes increasingly aggressive. Knowing that this all started because of her, Ann reluctantly breaks up with Joe. However, Joe refuses to step and negotiates with Lui to settle in a car race with a bet of HK$500,000. During the race, Lui dashes into the ocean. Luis underling, Bull (Karel Wong), who thinks that Lui is dead, takes Ann hostage to threaten Joe in order to avenge his boss. Joe rushes to save Ann, but was no match for Bull. In this critical occasion, Lui returns with a full body of blood.

==Cast==
*Jacky Cheung as Lui
*Max Mok as Joe
*Loletta Lee as Ann
*Ellen Chan as Katy
*Karel Wong as Bull
*Wu Ma as Luis father
*Lung Fong as Brother Bing
*Ma Kei
*Billy Ching
*James Ha
*Leung Sap-yat
*Hung Chi-ming
*Scott Kam
*Lam Chi-wa
*Ng Kwok-kin as Uncle Fei
*Chiu Jun-chiu as Policeman

==Theme song==
*Street Hero (馬路英雄)
**Composer: T. Clarkin, R. Bullard
**Lyricist: Keith Chan Siu-kei
**Singer: Jacky Cheung

==Reception==
===Critical===
  gave the film a mixed review and writes "Passably interesting triad/street racer drama which lifts clichés from many Hong Kong films before it." 

===Box office===
The film grossed HK$5,038,551 at the Hong Kong box office during its theatrical run from 19 January to 2 February 1991.

==See also==
*Jacky Cheung filmography 

==References==
 

==External links==
* 
*  at Hong Kong Cinemagic
* 

 
 
 
 
 
 
 
 
 
 
 
 