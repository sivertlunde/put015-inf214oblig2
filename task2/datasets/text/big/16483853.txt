The Spiral Staircase (1975 film)
{{Infobox film
| name = The Spiral Staircase
| image_size =
| image = The Spiral Staircase FilmPoster.jpeg
| caption = Peter Collinson
| producer = Josef Shaftel
| writer = Ethel Lina White (novel) Mel Dinelli (writer)
| narrator =
| starring = Jacqueline Bisset Michael Sarrazin John Phillip Law
| music = David Lindup
| cinematography = Ken Hodges
| editing = Raymond Poulton
| studio = Raven Films
| distributor = Warner Bros.
| released = 1975
| runtime = 86 min
| country = United Kingdom
| language = English
| budget =
| gross =
}} Peter Collinson, The Spiral Staircase.

==Plot== 
Helen Mallory is a beautiful young woman who has been unable to speak a word since seeing her husband and daughter die in a fire. 

==Argumento== 
She visits the home of her elderly, invalid grandmother, and meet her uncle, Joe Sherman, a respected psychiatrist, but the visit turns into a nightmare as she encounters his brash brother Steven, a pretentious Southern belle named Blanche, and a number of other mysterious characters in a house where everyones life seems to be in grave danger.

==Cast== 
* Jacqueline Bisset as Helen Mallory
* Christopher Plummer as Dr. Joe Sherman
* John Phillip Law as Steven Sherman
* Sam Wanamaker as Lieutenant Fields
* Mildred Dunnock as Mrs. Sherman
* Gayle Hunnicutt as Blanche
* Elaine Stritch as Nurse
* John Ronane as Dr. Rawley
* Sheila Brennan as Mrs. Oates
* Ronald Radd as Oates
* Heather Lowe as Heather
* Christopher Malcolm as Policeman

==References== 
 

==External links== 
*  

 

 
 
 
 
 
      