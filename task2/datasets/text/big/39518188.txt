Horrible Bosses 2
 
{{Infobox film
| name = Horrible Bosses 2
| image = Horrible_Bosses 2.jpg
| alt = 
| caption = Theatrical release poster
| director = Sean Anders
| producer = {{Plainlist|
* Brett Ratner
* Jay Stern
* John Morris
}}
| screenplay = {{Plainlist|
* Sean Anders
* John Morris
}}
| story = {{Plainlist|
* John Francis Daley Jonathan Goldstein
* Sean Anders
* John Morris
}}
| starring = {{Plainlist|
* Jason Bateman
* Charlie Day
* Jason Sudeikis
* Jennifer Aniston
* Jamie Foxx
* Chris Pine
* Christoph Waltz
}}
| music = Christopher Lennertz
| cinematography = Julio Macat
| editing = Eric Kissack
| studio = {{Plainlist|
* New Line Cinema
* Benderspink RatPac Entertainment
}} Warner Bros. Pictures
| released =  
| runtime = 108 minutes  
| country = United States
| language = English
| budget = $42 million   
| gross = $106.6 million  
}}
Horrible Bosses 2 is a 2014 American black comedy film directed by Sean Anders and a sequel to 2011s Horrible Bosses. Produced by New Line Cinema and distributed by Warner Bros., the film was released on November 26, 2014. The film earned mixed reviews, but was a moderate box office success, grossing over $106 million worldwide.

==Plot==
Nick Hendricks (Jason Bateman), Dale Arbus (Charlie Day), and Kurt Buckman (Jason Sudeikis) decide to start their own business after tiring of working for ungrateful bosses. Their idea is a car-wash-inspired shower head called the "Shower Buddy". They have trouble finding investors until they are approached by Burt Hanson (Christoph Waltz) and his son Rex (Chris Pine). Burt admires their commitment to manufacturing the product themselves, while Rex prefers to outsource to China, and agrees to invest if they can make 100,000 units. Taking out a business loan, the three rent a warehouse, hire employees, and manage to produce their output. However, Burt backs out of their deal at the last minute, claiming that he never signed an agreement, and he plans on taking their inventory in foreclosure and selling them (renamed the "Shower Pal") himself, while leaving the three in $500,000 debt with their outstanding loan.

Seeking financial advice, Nick, Dale, and Kurt visit Nicks old boss, Dave Harken (Kevin Spacey), in prison, who says the three have no feasible legal options to recover their losses. The three then resolve to kidnap Rex and hold him for ransom. They seek the help of "Motherfucker" Jones (Jamie Foxx), who says the best way to kidnap someone who knows them is to keep the victim unconscious for the duration of the kidnapping. The three create a ransom note asking for $500,000 and go to the office of Dales old boss, Dr. Julia Harris (Jennifer Aniston), to steal a tank of nitrous oxide. While there, Kurt and Dale are almost caught by Julias sex addiction group meeting; after the group leaves, Nick has sex with Julia, providing the distraction that allows Dale and Kurt to escape the building. The trio goes to Rexs house, but while they hide in the closet, Dale accidentally turns on the tank and they pass out. When they wake up in the morning, they find Rex gone.

When they arrive back at the warehouse, they find Rex tied up in the trunk of their car. Rex gets out and reveals he found them hiding in his closet, but decided to stage his own kidnapping with them due to his strained relationship with his dad. Rex sent the ransom note to his dad and increased the ransom to $5 million. The three are uncertain of Rexs plan, but Rex threatens to go to the police if they back out. They call Burt to inform him of Rexs kidnapping, threatening to kill Rex if Burt calls the cops. However, the police, led by Detective Hatcher (Jonathan Banks), subsequently arrive at their warehouse to question Nick, Dale, and Kurt due to their involvement with Burt. When the police leave, Rex breaks down, knowing Burt cares more about his money than his son. Now sympathetic to Rex, the trio agrees to work with him in the fake kidnapping, and all four devise a plan to outsmart the police and take the ransom money, utilizing untraceable phones, a basement garage to block out any tracking signal, and Kurt disguising himself as Burt.
 app and, believing Dale is cheating on her with Julia, storms off. Dale angrily locks Julia in the bathroom so the three can leave. In the basement garage, Nick, Dale, and Kurt, wearing masks, tell Burt to give back the cell phone. Burt is then killed by Rex, who reveals that, after seeing that his father did not care about him, he decided to kill Burt and frame Nick, Dale, and Kurt in order to inherit the family business. Rex forces Kurt to switch pants as Rexs pants have Burts blood on them.

As the trio are about to be cornered by the police, Jones arrives, as he anticipated that the three would be betrayed and killed and was seeking to claim the ransom money for himself. He attempts to help them get back to the warehouse where Rex is supposed to be tied up, with the police chasing them, before Rex does so they can prove their innocence. When they get back to the warehouse, Jones escapes with the money and the police arrive to find Rex tied up. Before the police arrest Nick, Dale, and Kurt, Kurts phone rings in Rexs pocket, and the police recognize the ringtone as the same phone that was left to Burt by the kidnappers. Rex tries to claim the phone is his, but when Hatcher asks why Rex did not bother to call the police if he had a phone, Rex takes Hatcher hostage. Dale attempts to attack Rex, but Rex shoots him, which distracts Rex long enough for Hatcher to subdue him.

A few days later, Dale wakes up to find out that the three did get in trouble, but because Dale helped save Hatchers life, the police dropped the charges. He also finds out that Julia helped make amends with Stacy, although she hints at having had sex with him during his coma and promises to sleep with his wife as well. In the aftermath, their business goes into foreclosure, but is subsequently purchased by Harken in prison, who allows the three of them to stay employed. Jones, meanwhile, uses the ransom money to invest in Pinkberry.

==Cast==
* Jason Bateman as Nick Hendricks
* Jason Sudeikis as Kurt Buckman
* Charlie Day as Dale Arbus
* Jennifer Aniston as Dr. Julia Harris, D.D.S
* Kevin Spacey as David Harken
* Jamie Foxx as Dean "Motherfuckah" Jones
* Chris Pine as Rex Hanson
* Christoph Waltz as Burt Hanson
* Jonathan Banks as Detective Hatcher
* Lindsay Sloane as Stacy Arbus
* Keegan-Michael Key as Mike
* Kelly Stables as Rachel
* Lennon Parham as Roz
* Suzy Nakamura as Kim

==Production== Jonathan Goldstein would be returning to write the script.    At this time, New Line Cinema was reported to be negotiating with Gordon to return as director as well as with Jason Bateman, Charlie Day, and Jason Sudeikis to return in the lead roles.    On February 27, 2012, it was confirmed that Goldstein and Daley were in the process of writing the new script.  In March 2013, Goldstein and Daley confirmed that they had submitted multiple draft scripts for the sequel, and that production had moved towards finalizing the budget.  Later in the same month Bateman, Day, and Sudeikis were confirmed to be reprising their roles, with Jamie Foxx negotiating to return.  The film was once again produced by Brett Ratner and Jay Stern. 

In August 2013, it was announced that Gordon would not be returning to direct because of scheduling conflicts and that the studio was actively searching for a replacement.   In September 2013, Sean Anders was announced as Gordons replacement, with John Morris joining the production as a producer.    The pair had previously performed a rewrite on Goldsteins and Daleys sequel script.  In September 2013, Jennifer Aniston signed on to reprise her role as Julia Harris. 

Principal photography took place in Burbank, California  between September 2013  and June 2014. 

==Release== trailer was released on September 30, 2014. 

On September 27, 2013, it was announced that the film would be released on November 26, 2014. 

===Box office===
Horrible Bosses 2 grossed $54.5 million in North America and $52 million in other territories for a total worldwide gross of $106.6 million worldwide, against a budget of $42 million.  This was almost half the predecessors total gross of $209 million.   
 Thanksgiving Day On Friday the film earned $6.2 million.   In its opening weekend it earned $23 million debuting at No. 5 at the box office. 

Outside North America, the film was released to 42 markets and earned $11.7 million from 4,900 screens. The highest debuts came from Russia ($2.3  million), the United Kingdom ($2 million), Mexico ($1.13 million) and Germany ($1 million). 

===Critical response===
Horrible Bosses 2 received  mixed  reviews from critics, with many criticizing its offensive jokes about rape, but praising the cast, with Pines performance in particular. On Rotten Tomatoes, the film holds a 35% rating, based on 135 reviews, with an average rating of 4.7/10. The sites consensus reads, "Horrible Bosses 2 may trigger a few belly laughs among big fans of the original, but all in all, its a waste of a strong cast that fails to justify its own existence."  On Metacritic, the film has a score of 40 out of 100, based on 36 critics, indicating "mixed or average reviews". 


Justin Lowe of The Hollywood Reporter said, "The jokes start growing stale well before the films midpoint."  Justin Chang of Variety (magazine)|Variety called the film an "inane and incredibly tasteless sequel."  Dan Callahan of TheWrap told that "the result is puerile, ugly and painfully unfunny."  Moira MacDonald of The Seattle Times gave the film one and a half stars out of four, saying "Lots of gags fly by, many of them in questionable taste (some downright offensive) and most of them unfunny."  Claudia Puig of USA Today gave the film one out of four stars, saying "This ill-conceived sequel to 2011s entertaining Horrible Bosses is base, moronic, insulting and vulgar. Its also cringingly unfunny."  Tom Russo of The Boston Globe gave the film two and a half out of four stars and said, "A new misadventure whose negligibly refined formula somehow ends up being more consistently entertaining."  Stephen Holden of The New York Times said that the film is "one of the sloppiest and most unnecessary Hollywood sequels ever made, isnt dirtier or more offensive than its 2011 forerunner. But it is infinitely dumber and not half as funny." 
 Joe Williams of St. Louis Post-Dispatch gave the film two out of four stars and said, "Horrible Bosses 2 is further proof that likable actors have to take an occasional sick day."  James Berardinelli gave the film one and a half stars out of four and wrote for ReelViews, "Horrible Bosses 2 (emphasis on "horrible") is an apt title for this repugnant, unnecessary sequel." 

===Home media===
The film was released via DVD and Blu-ray Disc on February 24, 2015. Like the first film, the Blu-ray release contains an extended version, which runs an additional eight minutes longer (116 minutes) than the 108-minute theatrical cut.

==See also==
 

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 