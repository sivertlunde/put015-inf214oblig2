The Spider's Web (1960 film)
{{Infobox film
| name           = The Spiders Web
| image          = "The_Spiders_Web"_(1960).jpg
| image_size     = 
| caption        = British quad poster
| director       = Godfrey Grayson
| producer       = Edward J. Danziger  Harry Lee Danziger
| writer         = Agatha Christie (play)   Albert G. Miller   Eldon Howard
| narrator       = 
| starring       = Glynis Johns John Justin Cicely Courtneidge   Jack Hulbert
| music          = Tony Crombie James Wilson  
| editing        = Bill Lewthwaite
| studio         = Danziger Productions
| distributor    = United Artists
| released       = November 1960
| runtime        = 88 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}} British mystery film directed by Godfrey Grayson and starring Glynis Johns, John Justin, Cicely Courtneidge and Jack Hulbert.  It was an adaptation of the play Spiders Web (play)|Spiders Web by Agatha Christie, and a rare Technicolor A feature from the The Danzigers|Danzigers.  It was remade as a television special and aired on December 26, 1982 starring Penelope Keith.

The poster tagline read, "DONT TELL YOUR FRIENDS THE ENDING ... THEY WONT BELIEVE IT!!!"  

==Plot==
The story of an ambassadors wife who must hide the corpse of her husband from his daughter. 

==Cast==
* Glynis Johns - Clarissa Hailsham-Brown 
* John Justin - Henry Hailsham-Brown 
* Jack Hulbert - Sir Rowland Delahaye 
* Cicely Courtneidge - Miss Peake  Ronald Howard - Jeremy  David Nixon - Elgin 
* Wendy Turner - Pippa 
* Basil Dignam -  Hugo 
* Joan Sterndale-Bennett - Mrs Elgin 
* Ferdy Mayne - Oliver 
* Peter Butterworth - Inspector Lord
* Anton Rodgers - Sergeant Jones

==Critical reception==
TV Guide wrote, "an entertaining film version of Agatha Christies 1954 stage play about a diplomats wife who hides the corpse of her stepdaughters father. Though there is no Miss Marple or Hercule Poirot to push this programmer along, it still moves at a lively pace."  

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 


 