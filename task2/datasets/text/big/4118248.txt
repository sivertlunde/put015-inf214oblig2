Brother Bear 2
 
 
{{Infobox film
| name           = Brother Bear 2
| image          = brotherbear2.jpg
| caption        = DVD cover of Brother Bear 2 Ben Gluck Jim Ballantine Carolyn Bates Dave Thomas Jack Weber Dana Gonzales 
| music          = Matthew Gerrard Dave Metzger Robbie Nevil
| distributor    = Buena Vista Home Entertainment
| released       =  
| country        = United States
| runtime        = 73 minutes
| language       = English
}}
Brother Bear 2 is a 2006 American animated comedy-drama/fantasy film and the direct-to-video sequel to the animated feature Brother Bear and was released on DVD and VHS on August 29, 2006. Melissa Etheridge contributed three songs to the film. In the film, the adventures of bear brothers Kenai and Koda continue. While the first film dealt with Kenais relationship with Koda, this one focuses more on his bond with a young human, Nita. Only five of the original characters return for the sequel including Kenai, Koda, Rutt, Tuke, and Tug. But only four of those actors came back to do their original roles which include Jeremy Suarez, Rick Moranis, Dave Thomas, and Michael Clarke Duncan.
 first film, but according to Reuters, Patrick Dempsey ultimately voiced Kenai. However, the end credits still note him as one of the additional voices. 

This is also Rick Moranis last role in a film before retiring from acting. 

== Plot ==
  Jeff Bennett). Dave Thomas). Koda cannot go to the village because they will kill him. Kenai makes it there, and gets into a fight with Atka. He falls off a cliff into shallow water, where the spirits come. Koda tells Kenai that he asked the spirits to change him back into a man. Kenai tells Nita that he cannot change because he cannot leave Koda, but Nita tells him that she can. So, she turns into a bear that has same coloring as Kenai, and they get married. The film ends with Kenai and Nita getting married and Rutt and Tuke finding mates, and the spirits changing the cave painting of young Nita and Kenai into two bear cubs, since neither are human any more.
 

== Voice cast ==
* Patrick Dempsey as Kenai
* Jeremy Suarez as Koda
* Mandy Moore as Nita
* Rick Moranis as Rutt Dave Thomas as Tuke
* Michael Clarke Duncan as Tug
* Andrea Martin as Anda Jeff Bennett as Atka
* Catherine OHara as Kata
* Wanda Sykes as Innoko
* Wendie Malick as Siqiniq
* Kathy Najimy as Taqqiq
* Tress MacNeille as Hoonah
* Jim Cummings as Bering and Chilkoot
* Jack Weber as Young Kenai
* Jessie Flower as Young Nita

== Reception ==
Brother Bear 2 received generally mixed reviews from critics, and the film currently holds a 50% in Rotten Tomatoes, and an average rating of 5.6/10 (based on 8 votes).  The film is the second direct-to-video sequel to have a higher rating on Rotten Tomatoes than its predecessor, with the first film being An Extremely Goofy Movie.

Enthusiastic reviews included those of 7M Pictures, which wrote "The kids will love “Brother Bear 2,” especially if they loved the first film. It has a good message and some decent scenes."  RealTalk Movie Reviews said "Although sequels -- even a few from Disney -- are often disappointing, this one is a keeper, mostly because of its charming story and extraordinary background music",  and DVDTalks Brian Orndorf said "As money-grabbing animated product goes, "Brother Bear 2" rests nicely on a lowered expectation level, and is hardly an offensive affront to the first film. The texture and polish is deeply missed, but the characters are so strong and engaging, it still entertains."  David Cornelius of DVDTalk wrote "The story fails to impress, but everything else adds up in all the right ways to make up for it. The makers of "Brother Bear 2" break the curse of the Disney sequel and turn in a welcome effort."  Movie Metropolis said "When you consider that Disney meant this production strictly for the home and it probably didnt cost nearly as much as the first film to make or market, its actually a superior product..."Brother Bear 2" may not be first-tier Disney filmmaking, but it is first-tier Disney animation, and that and the sweetness of the story line may be enough to keep even grown-ups entertained." 

Negative reviews came from Pablo Villaca of Cinema em Cena, who said "sad to say ... the magic of the first film and sensitivity were replaced by cliches, ridiculous story and cheap sentimentality."  Antagony & Ecstasy noted that it has a "weirdly adult-centered narrative" and concluded that "its such a big, fluffy, nothing of a movie, as pointedly blank and inoffensive as the lazy, anonymous songs contributed by a slumming Melissa Etheridge".  Reef Film Reviews said "Theres little doubt that Brother Bear 2, for the most part, comes off as an affable yet entirely needless piece of work, as filmmaker Ben Gluck, working from Rich Burns script, is generally unable to wholeheartedly capture and sustain the viewers interest - with the ongoing emphasis on stand-alone segments (eg the central trio run afoul of several violent raccoons) ensuring that the movie is only sporadically engaging." 

== Soundtrack ==
 
{{Infobox album
| Name        = Brother Bear 2
| Type        = soundtrack
| Artist      = Various artists
| Cover       = Brother Bear 2 OST.jpg
| Released    = August 15, 2006
| Recorded    = 2006
| Genre       = Pop music|Pop, Rock (music)|rock, soundtrack
| Length      = Walt Disney
| Producer    =
}}

The soundtrack to Brother Bear 2 was released August 15, 2006. It is available only on digital outlets such as iTunes and walmart.com. It includes the following tracks:

#Dave Metzger – "Opening: Brother Bear 2"  – 0:34 
#Melissa Etheridge – "Welcome to This Day"  – 2:40 
#Dave Metzger – "The Dream"  – 2:08 
#Dave Metzger – "Father and Daughter"  – 0:54 
#Dave Metzger – "Nita Confesses Her Fear"   – 0:55 
#Melissa Etheridge and Josh Kelley – "Feels Like Home"  – 3:30 
#Melissa Etheridge – "It Will Be Me"  – 3:35 
#Dave Metzger – "Kodas Wish to the Spirits"  – 1:38 
#Dave Metzger – "I Love You Too"  – 2:42 
#Dave Metzger – "Nitas Transformation"  – 1:23 
#Melissa Etheridge and Josh Kelley – "Welcome to This Day  "  – 1:33 

== References ==
 

== External links ==
 
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 