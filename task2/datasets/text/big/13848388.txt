The Impossible Kid
The Impossible Kid is a Kung Fu movie made in 1982 in film|1982, starring Filipino actor Weng Weng. This movie is a sequel to For Your Height Only. Part of the comedic premise of this film involves the viewer’s suspension of disbelief during the fight scenes. The 29" Weng Weng was featured beating up, knocking out, and holding down average-sized men.

==Synopsis==
Weng Weng plays an agent, code-named “00” who works for the Manila branch of Interpol. The Chief sends him in pursuit of an arch villain, Mr X, whose white sock covered head is reminiscent of the Ku Klux Klan’s pointed hoods. When Mr X holds the Philippines for ransom two businessmen, Maolo and Simeon, pay his demands. Weng Weng suspects foul play and goes deep undercover to reveal the true identity of the mysterious Mr X.

== In popular culture ==
In 2008, the Brazilian comedy show Hermes & Renato made a parody dubbing of The Impossible Kid, titled “Um Capeta em Forma de Guri” (lit. "A Demon in a Boy’s Body"), that portrayed the title character as a mischievous and wicked little boy. The dub is full of bad language and sexual innuendos.

The Impossible Kid was featured in 2010 on the  , hosted by Ed the Sock and Liana K.

==External links==
* 
* 

 
 
 
 
 
 


 
 