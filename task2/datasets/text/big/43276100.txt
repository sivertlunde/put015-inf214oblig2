Savage Pampas (1945 film)
{{Infobox film
| name = Savage Pampas 
| image = 
| image_size =
| caption =
| director = Lucas Demare   Hugo Fregonese
| producer = 
| writer =  Homero Manzi   Ulises Petit de Murat
| narrator = Enrique Muiño
| starring = Francisco Petrone   Luisa Vehil   Domingo Sapelli   Froilán Varela
| music = Lucio Demare   Juan Ehlert Bob Roberts
| editing =   Atilio Rinaldi   Carlos Rinaldi
| studio = Artistas Argentinos Asociados
| distributor = Artistas Argentinos Asociados 
| released = 9 October 1945   
| runtime = 98 minutes
| country = Argentina Spanish 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} under the same title.

==Synopsis==
A tough Captain of the Argentine Army doggedly battles a band of outlaws composed of a mixture of Indians and Argentine deserters.

==Cast==
* Francisco Petrone 
* Luisa Vehil
* Domingo Sapelli 
* Froilán Varela 
* María Esther Gamas 
* Judith Sulian 
* Roberto Fugazot 
* Margarita Corona 
* Juan Bono 
* María Concepción César 
* Pablo Cumo  Luis Otero 
* Jorge Molina Salas 
* Tito Alonso 
* René Múgica
* Pedro Codina 
* Aurelia Ferrer 
* Francisco García Garaba 
* Raúl Luar  
* José Ruzzo

== References ==
 

== Bibliography ==
* Rist, Peter H. Historical Dictionary of South American Cinema. Rowman & Littlefield, 2014.

== External links ==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 


 