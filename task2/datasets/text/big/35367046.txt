L'affaire Chebeya
{{Infobox film
| name           = LAffaire Chebeya (un crime dEtat ?)
| image          = Affaire Chebeya poster.JPG
| caption        =
| director       = Thierry Michel
| producer       =  
| writer         = Thierry Michel
| narrator       = Rachid Benbouchta
| music          = 
| cinematography = 
| editing        =
| distributor    =
| released       =  
| runtime        = 96 minutes
| country        = Belgium
| language       = French English Dutch
| budget         =
| gross          =
}}
LAffaire Chebeya (un crime dEtat ?) is a documentary directed by Thierry Michel that was released in February 2012.  
The award-winning film explores the assassination of a human rights activist in the Democratic Republic of the Congo (DRC) and the subsequent trial.

==Synopsis== civil war. On 2 June 2010, his body was found in a car near the capital, Kinshasa. The police quickly painted a scenario that made Chebeya the routine victim of a crime with sexual connotations. However, this version of the events broke down after an investigation in which the police were implicated. The government agreed to bring some of the highest police officials to justice in a military court, handing out stiff sentences to those found guilty and awarding compensation to the victims. 

The film exposes the rough edges of Chebeyas character. He was a tenacious, insensitive and annoying man, with an inflated view of his role as defender of human rights, sometimes willing to make strange alliances to achieve his goals.  The film covers more than just the police case, but provides a survey of life in a country that is between dictatorship and democracy, between opacity and justice. A recurring theme is the failure of the police chief, General John Numbi, to appear and offer an explanation. 

==Production==
Thierry Michel directed his first documentary on the Democratic Republic of the Congo in 1992 with Zaïre, le cycle du serpent. Since then he has released a series of other films covering different aspects of life and politics in the country. Taken together his films provide a unique overview of the social, economic and political life of the country. {{cite book |ref=harv |url=http://books.google.ca/books?id=akIpucUB97oC&pg=PA269 |pages=268–269
 |title=Télévision française, la saison 2011: une analyse des programmes du 1er septembre 2009 au 31 août 2010
 |first=Christian |last=Bosséno
 |publisher=Editions LHarmattan |year=2011 |ISBN=2296546781}} 

He flew to Kinshasa as soon as he heard of the death of Chebeya, whom he had known for many years, to film the trial.  This was the largest public trial there for over a decade, and the authorities were under strong pressure from the United States and Europe to ensure that justice was done.

Speaking of the film in an interview, the director said the trial did provide some degree of justice, if imperfect, but noted that the country may be sliding backwards in terms of protection of human rights. {{cite web
 |url=http://www.rue89.com/2012/04/04/laffaire-chebeya-au-congo-un-film-sur-un-crime-detat-230067
 |work=Rue89
 |title=" L’Affaire Chebeya ", un docu sur un " crime d’Etat " au Congo
 |date=2012-04-04
 |author=Sandra Titi-Fontaine
 |accessdate=2012-04-07}} 

==Reception==
Le Monde noted that because the analysis is subtle, the documentary is probably only accessible to those who already known something of the Congo. 

Another reviewer said Thierry Michel had perpetuated the memory of Chebeya and given him the stature he deserves, that of a modest hero, ruthlessly efficient, a man whose name is written in the pantheon of heroes of the Congo. 
The film won the Grand Prize at the International Film Festival of Human Rights in Paris in March 2012. {{cite web
 |url=http://blogs.lexpress.fr/afrique-en-face/2012/04/04/cinema-verite/
 |work=LExpress
 |title=Cinéma Vérité
 |date=4 April 2012 
 |author=VINCENT HUGEUX
 |accessdate=2012-04-07}} 

The film received a Magritte Award nomination in the category of Best Documentary in 2013. 

==References==
{{reflist |refs=
 {{cite web
 |url=http://www.courrierinternational.com/article/2012/04/04/l-affaire-chebeya-la-realite-plus-forte-que-la-fiction
 |title="LAffaire Chebeya" : la réalité plus forte que la fiction
 |work=Courrier International
 |date=2012-04-04
 |author=Colette Braeckman
 |accessdate=2012-04-07}} 
 {{cite web
 |url=http://www.lemonde.fr/cinema/article/2012/04/03/l-affaire-chebeya-un-crime-d-etat-une-chronique-de-la-vie-dans-un-pays-ambigu_1679233_3476.html
 |work=Le Monde
 |title="LAffaire Chebeya (un crime dEtat ?)" : une chronique de la vie dans un pays ambigu
 |date=2012-04-03
 |author=Thomas Sotinel
 |accessdate=2012-04-07}} 
}}

==External links==
* 

 
 
 
 
 
 