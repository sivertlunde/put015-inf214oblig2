Scarred (film)
{{Infobox Film |
|  name           = Scarred
|  image          =
|  writer         = Rose Marie Turko
|  starring       = Jennifer Mayo, Jackie Berryman, David Dean, Rico L. Richardson
|  director       = Rose Marie Turko
|  cinematography =
|  producer       =
|  distributor    =
|  released       = July 1984
|  music          =
|  runtime        = 85 minutes 
|  country        = United States
|  language       = English
}} 1984 independent film.

==Cast and crew==
Directed by: Rose Marie Turko

Starring: Jennifer Mayo, Jackie Berryman, David Dean, Rico L. Richardson

==Synopsis==
Shot in a cinema verite style, perhaps because this film about teenage prostitution began as a project while director Rose-Marie Turko was a student at UCLA, the format tends to hit home better than a more artificial approach. Although starting out as a story about how young Ruby Star (Jennifer Mayo) was forced into prostitution in order to support herself and her baby, the film quickly dips into the seamier side of life after Ruby meets a pimp nicknamed Easy (David Dean) and gets involved with a demi-monde of degenerates.

MPAA Rating: PG

Runtime: 1 hour, 25 minutes

Genre: Drama, Teenage Prostitution, Self-Discovery

==Soundtrack==
 Dennis Peters)

Recorded in Los Angeles, California at Westlake Studios, Soundcatsle Studio, Davlen Studios, and The Wilcox Studio.

----
 Boy
#Cant Boy
#Street Life  (Freddy Moore)  - The Nu Kats Boy
#Clam Up  (Grimbicus, Comer) - The Signals 
#Message Of The Heart  Kim Fields) - Kim Fields
#Sign Of The Times  (Webb - Read) - The Difference
#Adolescent  (The Plugz) - The Plugz
#Dont Let Go  (Merlind - Manciouso - Strum - DePompies) - Modern Design
#Oasis Of Love   (Kim Fields) - Kim Fields
#No Tomorrow Today  (Tim Timmermans ) - Tim Timmermans
#World Outside My Window  (Jacobson) - Second Language

==External links==
*  

 
 
 


 