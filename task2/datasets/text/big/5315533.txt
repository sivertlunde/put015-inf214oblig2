Autumn Tale
 
{{Infobox film
| name           = Autumn Tale
| image size     = 
| image	=	Autumn Tale FilmPoster.jpeg
| alt            = 
| caption        = 
| director       = Éric Rohmer
| producer       = 
| writer         = Éric Rohmer
| narrator       = 
| starring       = Béatrice Romand, Marie Rivière, Alain Libolt, Alexia Portal
| music          = 
| cinematography = Diane Baratier
| editing        = Mary Stephen
| studio         = 
| distributor    = October Films (USA)
| released       =   (France, Belgium) July 1999 (USA) 
| runtime        = 112 minutes France
| French
| budget         = 
| gross          = 
}}
Autumn Tale ( ) is a 1998 French film, directed by Éric Rohmer, starring Béatrice Romand, Marie Rivière, Alain Libolt, Didier Sandre, Alexia Portal, and Aurélia Alcaïs. It is the final film of Rohmers Contes des quatre saisons (Tales of the Four Seasons), which also includes A Tale of Springtime (1990), A Tale of Winter (1992) and A Summers Tale (1996).

==Synopsis==
Magali (Béatrice Romand), forty-something, is a winemaker and a widow: she loves her work but feels lonely. Her friends Rosine (Alexia Portal) and Isabelle (Marie Rivière) both want secretly to find a husband for Magali.

==Reception==

Sight & Sound called it a "beautiful, witty and serene film" which "never falls into the talking-heads trap. Encounters in cars, cafés, gardens and restaurants are visually dramatised, allowing the characters philosophies (the action of the film, as it were) to be expressed dynamically. And this literary emphasis on language, something of a cliché with Rohmer, and the simplicity of the mise en scène rest on tight plotting in the tradition of Rohmers master, Alfred Hitchcock|Hitchcock." 

Stephen Holden, in excerpts re-published after the films New York City opening but originally written after the films appearance as part of the 1998 New York Film Festival, called the film "as sublimely warming an experience as the autumn sun that shines benevolently on the vineyard owned by the films central character, Magali (Beatrice Romand)"; although the film has its "labored moments" and "except for a twist here and there, you know where the story is going to go", the film nevertheless "evokes such a sensuous atmosphere &mdash; bird song, wind and light and shadow that delineate the season and time of day with an astonishing precision &mdash; that you are all but transported into Magalis fields, where this years grapes promise to yield an especially fine vintage." 

The Boston Review said "The Autumn Tale... outshines its   predecessors....Throughout this film one senses that both the characters and the audience are in the hands of a great psychologist&ndash;if one knew more about the Rhône Valley, its old towns and its new factories, one would appreciate even more how Rohmers women are suited to their local social reality, which is filmed as carefully as they are."   
 Notting Hill, its like they wear galoshes compared to the sly wit of a movie like Autumn Tale.  They stomp squishy-footed through their clockwork plots, while Rohmer elegantly seduces us with people who have all of the alarming unpredictability of life." 

==Awards== Best Foreign Language Film by the National Society of Film Critics.

==DVD release==
In the United Kingdom, a region 2 DVD was released by Artificial Eye, with English subtitles and an interview with the writer/director. 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 