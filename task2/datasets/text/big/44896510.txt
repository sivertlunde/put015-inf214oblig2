For Goodness Sake
{{Infobox film
| name           = For Goodness Sake
| image          = 
| alt            = 
| caption        =  David Zucker 
| producer       = Micah Center for Ethical Monotheism 
| writer         =   
| screenplay     = 
| story          = 
| based on       =  
| starring       =   
| narrator       =  
| music          = 
| cinematography = 
| editing        = Michael L. Sale
| studio         =  
| distributor    =  
| released       = 1993 
| runtime        = 29 
| country        = U.S.
| language       = English
| budget         = 
| gross          =  
}} David Zucker vignettes that Ethical Monotheism, and Mentor Media Inc. marketed the film for ethics training at schools and corporations.   Directed by Zucker, it starred Prager and actors Jason Alexander, Scott Bakula, Bonnie Hunt, and Bob Saget. 

The film originally included O.J. Simpson, but Simpson was edited out of later showings after he was charged with murder.     

A sequel, For Goodness Sake II, was directed by Trey Parker and released in 1996.   It featured Dennis Prager and fellow radio talk show host Larry Elder. 

==References==
 

 

 
 
 