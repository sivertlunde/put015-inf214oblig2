Bandh Darwaza
 
 
{{Infobox film
| name           = Bandh Darwaza (Translation: "The Closed Door")
| image_size     = 
| image	=	Bandh Darwaza FilmPoster.jpeg
| caption        = 
| director       = Shyam Ramsay and Tulsi Ramsay
| producer       = Shyam Ramsay
| writer         = Dev Kishan and Shyam Ramsay
| narrator       = 
| starring       = Manjeet Kullar Kunika Aruna Irani Hashmat Khan Chetana Das
| music          = Anand-Milind (Soundtrack)
| cinematography = Gangu Ramsay
| editing        = Keshav Hirani
| distributor    = 
| released       =  
| runtime        = 145 minutes
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 Anirudh Agarwal and the Afghan actor Hashmat Khan. The film soundtrack was composed by the Anand-Milind brothers.

== Plot ==
The film opens in the ruins of Kali Pahari (Black Hills, a decrepit complex of caves), and introduces the vampire Neola.  Neola, a Dracula-like vampire, sleeps in a coffin by day, and transforms into a bat at night to hunt humans from the neighbouring villages. He craves a steady supply of human blood, and a supply of fresh young women (so he may seduce them and spread his evil seed). Neola is assisted by a ragtag bunch of servants who lure innocent humans (mostly women) to Kali Pahari so Neola may easily prey on them. His servants include Mahua (a witch-woman), Mahaguru (an evil priest), a tantrik (evil wizard) and various other henchmen that provide muscle for his evil deeds.

Mahua is employed as a maid in the household of a righteous Thakur (baron) Pratap. The Thakur spots her speaking with Baku one evening. Knowing of Bakus association with the evils of Kali Pahari, the Thakur threatens to fire Mahua if she is ever caught with any of the gang again. Mahua, however, is under orders to source a new maiden for Neola.

The upright Thakur is wealthy, well liked and happy, except for the fact that his wife Lajo is unable to have children. The local temple priest reassures her, but she is deeply perturbed when the Thakurs mausi (aunt) arrives and urges him, citing the necessity of progeny, to consider marrying again. Mahua moves in. She promises Lajo will bear children if Lajo will agree to come to Kali Pahari where Neola will grant her wish, on the condition that if she bears a son she can keep him but if she gives birth to a daughter the child will be Neolas property and should be handed over to him. Lajo refuses at first, but Mahua prevails over Lajos vulnerability and Lajo accepts the deal. Accordingly, she goes to Kali Pahari, where she is bathed in milk and Neola appears and hypnotises her and beds her. Thereafter Lajo gives birth to a daughter Kamya. But when Mahua claims the child, Lajo naturally refuses point-blank and drives her away. Mahua poisons Lajo on the instructions of Neola. Lajo confronts Thakur and tells him the truth and tells him to rescue Kamya who is abducted by Mahua and taken to Kali Pahari.  Lajo succumbs to the poison. Enraged, the Thakur enters Kali Pahari and, after a long fight, is able to drive a magic stake through Neola to put him down. Gravely wounded, Neola staggers back to his coffin deep within the caves (behind a bandh darwaza or closed doorway) and lays within it to rest and recover.

Some twenty years later, Kamya grows up to be a beautiful girl. She has her heart set on Kumar, her childhood crush, but he wards off her advances as childish tomfoolery. Kumar likes Sapna (the sister of his friend Anand). Sapna lives in Mumbai in a hostel and on her way back to her home she meets a woman who asks for a lift to Kali Pahari in the dead of night. Sapna drops her at Kali Pahari but unknowingly the woman forgets her book in Sapnas car. Its a black magic book. Sapna follows the woman to return the book where she sees the woman laying down on a ritual table where a priest surrounded by some strange people stabs her. Sapna screams seeings this. The priest orders his men to nab her but she flees and in a hurry she drops the book at Kali Pahari. Shaken by this event she reaches Kumars home and narrates everything to Anand and him. Next day the trio visit Kali Pahari and there is nothing at all there like what Sapna had described. Sapna says she was not dreaming and shows them the book she mistakenly dropped there last night. On the way they meet Kamya who invites the trio for her birthday party. Things get interesting when Kumar begins to portray affections towards Sapna at Kamyas birthday party. Shaken by this, Kamya taunts Sapna due to which Sapna leaves the venue. Kamya in the meanwhile makes a direct move, but Kumar bluntly snubs her. On her way back home Sapna encounters the same woman who was stabbed the previous night. She demands her book and sits in Sapnas car. Sapna reaches home and finds the book. In the meanwhile Kumar reaches Sapnas home searching her as she had left the party midway. Sapna tells him about that strange woman. Kumar throws the book out of her house and tells her to stop thinking about all this. Kamya finds the book as she was following Kumar. She reaches home and reads the book and finds ways how she can achieve Kumar through witchcraft. While driving that strange woman (of the Kali Pahari gang), crashes her car, and is taken by the gang to the altar of Neola. Kamya is cured of her injuries by the priest by black magic and they tell her she can achieve everything if she joins their gang, she agrees. 

Kamya practices witchcraft on Kumar at a graveyard. He reaches there and starts making love to her. Anand and Sapna reach there and rescue Kumar. Anand finds Kumars photograph at the graveyard and instantly believes that Kamya is behind all this as he has seen Kamya before with the strange people of Kali Pahari. One day Kamya is invited to Kali Pahari at night for a ritual. Its a trap to get Kamya to revive Neola, who was asleep all these years. He recognises Kamya bites her and beds with her. She is now forever trapped behind the bandh darwaza and is a sex slave to Neola. Kumar, Anand and Sapna enter Kali Pahari to rescue Kamya. They finally find her, barely conscious, deep within the catacombs in a glass coffin. They try to rescue her but the place gets filled with smoke and Neola abducts her. 

Kamya is hypnotised by the tantrik to serve Neola forever.Now Kamya gets orders to source more women for Neola. She approaches Bhanu (Anands wife) and lures her to Kali Pahari, but Kumar, Anand and some of his friends arrive there in time to rescue Bhanu before Neola can seduce her. Kamyas next target is Sapna. Sapna, too, is rescued in the nick of time, but Kumar and Anand discovers that Kamya is behind all this. They report back to the Thakur, and he decides to finish Neola once and for all.
Upon entering Kali Pahari, the Thakur finds Mahua, who informs him with evil glee of the bargain behind the birth of Kamya, and that Kamya is now entirely in Neolas power. Furious, the Thakur beheads Mahua. But Neola is unstoppable. He beckons Bhanu once again, and this time, he succeeds in biting her. Neola attacks Sapna yet again, and follows her to the Thakurs home. After a prolonged chase, Neola kills both Bhanu and Anand and nearly gets Sapna, but is thwarted in the nick of time by the Thakur. The Thakur reveals the tale of Kamyas birth to Kumar and Sapna.

The three of them enter Kali Pahari to recover Kamya. They finally find her, barely conscious, deep within the catacombs. They are immediately set upon by the gang, various henchmen, and Neola himself. They fight their way out and wound Neola once again, but are somehow unable to kill him. The evil woman and the tantrik carry away Neola in a brougham. Kumar gives chase, and captures the tantrik, and threatens to kill him unless he discloses Neolas Achilles heel. The tantrik reveals that Neolas soul is trapped within a statue at Kali Pahari and his weakness is sunlight. Kumar and the Thakur devise a two phase plan to finish Neola. Kumar and Sapna follow the brougham away from Kali Pahari, and round up the townsfolk to engage Neola. Neola attacks them in a frenzy. Just as he gets within reach of Sapna, he is involuntarily seized with pain and bursts into flames (as the Thakur sets the evil statue afire).

The film ends with Kumar, Sapna and the Thakur looking on as Neola is destroyed forever.


== Cast ==
* Manjeet Kullar as Sapna
* Hashmat Khan as Kumar
* Kunika as Kamya
* Aruna Irani as Mahua
* Anita Sareen as Bhanu
* Vijayendra Ghatge as Thakur Pratap Singh
* Raza Murad as evil priest
* Anirudh Agarwal as Neola
* Satish Kaul as Anand
* Beena Banerjee as Thakurayan Lajo
* Johnny Lever as Gopi( Thakurs servant )
* Shayamalee as the strange woman of Kali Pahari
* Jack Gaud as Neolas devotee
* Ashalata Wabgaonkar as Kumars mother
* Karunakar Pathak
* Gurinder Kohli
* Rajni Bala

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="centr"
! # !! Title !! Singer(s)
|-
| 1
| "Bheega Bheega Mausam Tadpaye"
| Suresh Wadkar, Sadhana Sargam
|-
| 2
| "Jalta Hai Kyun Tu"
| Alisha Chinai
|-
| 3
| "Main Ek Chingari Hoon"
| Anuradha Paudwal
|-
| 4
| "Tu Ek Chingari Hai"
| Suresh Wadkar
|}

==References==
 

== External links ==
*  
*  

 
 
 
 
 