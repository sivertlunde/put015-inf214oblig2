His and Hers (film)
His and Hers is a 1960 British comedy film directed by Brian Desmond Hurst and starring Terry-Thomas, Janette Scott and Wilfrid Hyde-White.  The film follows an eccentric author who tries to impose his lifestyle on his reluctant wife.

==Cast==
* Terry-Thomas - Reggie Blake
* Janette Scott - Fran Blake
* Wilfrid Hyde-White - Charles Dunton
* Nicole Maurey - Simone Rolfe
* Joan Sims - Hortense
* Kenneth Connor - Harold
* Meier Tzelniker - Felix McGregor
* Joan Hickson - Phoebe
* Oliver Reed - Poet
* Francesca Annis - Wanda
* Dorinda Stevens - Dora
* Kenneth Williams - Policeman
* Barbara Hicks - Woman
* Billy Lambert - Baby
* Colin Gordon - Television announcer
* Marie Devereux - Wife

==References==
 

==External links==
* 
*   at the website dedicated to Brian Desmond Hurst

 

 
 
 
 
 
 
 


 
 