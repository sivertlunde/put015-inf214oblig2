Balayogini
{{Infobox film
| name           = Balayogini
| image          = Balayogini_1937film_2.jpg
| image size     =
| caption        = K.R. Chellam and Baby Saroja in Balayogini
| director       = K. Subramaniam|K. Subramanyam
| producer       = K. Subramaniam|K. Subramanyam, Madras United Artists Corp
| writer         = K. Subramaniam|K. Subramanyam
| narrator       =
| starring       = Baby Saroja C. V. V. Panthulu K. B. Vatsal R. Balasaraswathi
| music          = Moti Babu Maruti Seetharammayya
| cinematography = Sailen Bose Kamal Ghosh
| editing        = Dharam Veer
| distributor    =
| released       = 5 February 1937
| runtime        = 210 min.
| country        = India Tamil 
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}
 Tamil and Telugu language|Telugu. It was directed by K. Subramaniam|K. Subramanyam. It is one of the earliest Tamil films to be set in a contemporary social setting and to advocate reformist social policies. This film is considered to be first childrens film of South India.               

==Production==
Subramanyam, was influenced by the reformist ideals of his father C.V. Krishnaswamy Iyer. He was moved by the social conditions around him to make reform oriented films. Balayogini (lit. Child Saint) was made to expose the plight and suffering of widows in middle class Brahmin communities in Tamil Nadu. Subramanyam produced this film under his "Madras United Artists Corporation" banner to express his criticism of the existing social norms and his disapproval of priesthood.     He wrote the story, screenplay & dialogues and directed it himself. He cast his niece Saroja as the titular character. The film was started in 1936 and released in 1937. The completed film was 19,000 feet (210 minutes runtime) in length.   

===Telugu film=== Telugu with dialogues written by B.T. Raghavacharya, under the banner of Mahalakshmi Studios.    	Baby Saroja, played the key role of Widows Daughter and the other main characters are played by Arani Satyanarayana, Vangara, Kamala Kumari, Thilakam and S. Varalakshmi.

==Plot==
Sarasas (K. R. Chellam) father is arrested by Police for being a debtor. She goes to the sub collectors (K. B. Vatsal) house to ask for help. The collectors widowed sister Janaki and her niece (Baby Saroja) take pity on her. They are driven out of the house by the angry collector. They take refuge in the house of their low-caste servant Munuswamy. Munuswamy dies and Janaki cares for his children as her own. This causes outrage in the conservative society and Munuswamys house is burned down by a mob. The child Saroja changes everyones mind with her arguments.

==Cast and crew==
 
*K. R. Chellam
*Baby Saroja
*C. V. V. Panthulu
*Bharathan - Gopalayyar
*K. B. Vatsal (K. Viswanathan) - District Collector
*Raavu Balasaraswathi
*"Baby" Rukmini
*Salem Sundaram
*K. Subramanyam - Story, Screenplay and Director
*Papanasam Sivan - Lyrics
*Moti Babu - Music
*Maruti Seetharamiah - Music
*Dharam Veer - Editor
*Sailen Bose - Cinematographer
*Kamal Ghosh - Assistant Cinematographer
*Batuk Sen - Art Director      

==Soundtrack==
* Kannae Paappa

==Reception==
The film was released on 5 February 1937 to critical and public acclaim. "Baby" Saroja was hailed as "Shirley Temple of India".  Many girls were named "Saroja" after her.  The films success inspired a number of socially themed films in South India.

Reviewing the film in the magazine Jaganmohini in February 1937, reviewer Vatsakumari wrote:

 

A similar review was written in Manikodi magazine in its February 1937 issue:
  }}
 Theodore Baskaran has called it the most significant Tamil film of its period.   

==References==
 

==External links==
*  
*  

 
 
 
 
 