Histoire(s) du cinéma
{{Infobox film
| name           = Histoire(s) du cinéma
| image          =Histoire(s)DuCinema01.jpg
| image_size     =
| caption        = An image quoted from Prison (1949 film) and overlapped text Histoire(s) du cinéma
| director       = Jean-Luc Godard
| producer       = Canal+, Centre National de la Cinématographie, France 3, Gaumont Film Company|Gaumont, La Sept, Télévision Suisse Romande, Vega Films
| writer         = Jean-Luc Godard
| narrator       = Jean-Luc Godard
| starring       = Juliette Binoche, Julie Delpy, Anne-Marie Miéville, André Malraux, Ezra Pound, Paul Celan David Darling
| cinematography = Pierre Binggeli, Hervé Duhamel
| editing        = Jean-Luc Godard Gaumont
| 1998
| runtime        = 266 minutes (total)
| country        = France Switzerland
| language       = French
| budget         =
| gross          =
| website        =
}}
  cinema and how it relates to the 20th century; in this sense, it can also be considered a critique of the 20th century and how it perceives itself. The project is considered the most important work of the late period of Godards career.

Histoire(s) du cinéma is always referred to by its   means both "history" and "story," and the s in parentheses gives the possibility of a plural. Therefore, the phrase Histoire(s) du cinéma simultaneously means The History of Cinema, Histories of Cinema, The Story of Cinema and Stories of Cinema. Similar double or triple meanings, as well as puns, are a recurring motif throughout Histoire(s) and much of Godards work.
 1997 Festival.   
 ECM record label.

==Episodes==
Histoire(s) du cinéma consists of 4 chapters, each one subdivided into two parts, making for a total of 8 episodes. The first two episodes, Toutes les histoires (1988) and Une histoire seule (1989) run 52 minutes and 48 minutes, respectively; the remaining 6 episodes, premiered 1997 - 1998, run under 40 minutes each.

*Chapter 1(a) : 51 min.
**Toutes les histoires (1988 in film|1988) - All the (Hi)stories
*Chapter 1(b) : 42 min.
**Une Histoire seule (1989 in film|1989) - A Single (Hi)story
*Chapter 2(a) : 26 min.
**Seul le cinéma (1997 in film|1997) - Only Cinema
*Chapter 2(b) : 28 min.
**Fatale beauté (1997 in film|1997) - Deadly Beauty
*Chapter 3(a) : 27 min.
**La Monnaie de l’absolu (1998 in film|1998) - The Coin of the Absolute
*Chapter 3(b) : 27 min.
**Une Vague Nouvelle (1998 in film|1998) - A New Wave
*Chapter 4(a) : 27 min.
**Le Contrôle de l’univers (1998 in film|1998) - The Control of the Universe
*Chapter 4(b) : 38 min.
**Les Signes parmi nous (1998 in film|1998) - The Signs Among Us

==Films referenced and quoted==
 ]]
Histoire(s) du cinéma is composed almost entirely of visual and auditory quotations from films, some famous and some obscure.
The sources of referenced films and literary quotations are delineated chronologically by the film critic Céline Scemama-Heard, the author of Histoire(s) du cinéma de Jean-Luc Godard. La force faible d’un art. 

This is a partial list of works Godard drew upon to create the project; a complete list would number hundreds of entries.

*The Barefoot Contessa   
*Bicycle Thieves 
*The Docks of New York 
*A King in New York Man Hunt
*Notorious (1946 film)|Notorious  The Night of the Hunter
*Only Angels Have Wings   
*Passion of Joan of Arc, which Godard had featured earlier in Vivre Sa Vie
*Rear Window
*Scarface (1932 film)|Scarface  Snow White and the Seven Dwarfs 
*Teorema (film)|Teorema
*Zéro de conduite

==References==
 

==Further reading==
*Scemama-Heard, Céline, Histoire(s) du cinéma de Jean-Luc Godard. La force faible d’un art, L’Harmattan, Paris, 2006. ISBN 2-296-00728-7  

==External links==
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*   of   at 0xDB

 

 
 
 
 
 
 
 
 
 
 
 
 
 