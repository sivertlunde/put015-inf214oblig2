Lost Indulgence
{{Infobox film
| name         = Lost Indulgence
| image        = Lost Indulgence.jpg
| caption      =
| director     = Zhang Yibai
| producer     = Tsui Siu-Ming Amy Li Zhang Yibai
| writer       = Zhao Tianyu
| starring     = Karen Mok Tan Jianci Jiang Wenli Eric Tsang Eason Chan Wang Yu
| editing      = Kong Jinglei
| distributor  = Sundream Motion Pictures
| released     =  
| runtime      = 104 minutes
| country      = China
| language     = Mandarin
| rating       =
| music        =
| budget       =
}} Curiosity Kills the Cat, Lost Indulgence takes place in the central Chinese city of Chongqing. 

The film is a family drama about the relationship between a mother and son (Jiang Wenli and Tan Jianci) after the sudden death of the father (Eric Tsang).

Lost Indulgence navigated a complicated series of schedules and cancellations before it was allowed to screen abroad, at the Tribeca Film Festival in April 2008.

== Plot ==
Upon the apparent death of his taxi-driver father (Eric Tseng), Xian-chuen (Tan Jianci) and his mother Li (Jiang Wenli) take in his fathers last fare, Su-Dan (Karen Mok), a bar girl, to fulfill the familys obligations. The father, Wu Tao drove his cab into the Yangtze River, breaking Su-Dans leg and potentially paralyzing her for life, though his body was not recovered.

The son, a bereaved and sullen teenager, does not take to the crippled Su-Dan at first, his thoughts preoccupied with a school crush and the sudden death of his father. As time goes on, however he warms to her presence (and her ever-exposed legs). Su-Dan, restricted to a wheelchair sees the boy as a substitute little brother.

Meanwhile, Li, who works as a medic at a factory, is forced to work double shifts at a veterinarian hospital to help make ends meet. There she meets a handsome stranger (Eason Chan) who brings in his injured dog, and a tentative romance begins to form.

== Cast ==
* Jiang Wenli as Li, the mother in the family. She takes on her husbands last passenger before his death.
* Tan Jianci as Xian-chuen, Li and Wu Taos sullen teenage son.
* Eric Tseng as the father, Wu Tao, a taxi-driver, Lis husband and Xian-chuens father, early in the film he is killed when his taxi plunges off a bridge into a river.
* Karen Mok as Su-Dan, a crippled bar girl who was Lis husbands last passenger.
* Eason Chan

== Release == Summer Palace at the Cannes Film Festival without permission.  Others saw the heightened concern as a result of public criticism and controversy surrounding Ang Lees sexually explicit Lust, Caution (film)|Lust, Caution and Feng Xiaogangs allegorical war story Assembly (film)|Assembly.  The withdrawal of the film was also seen as a sign of the growing influence that the Chinese Film Bureau has had on Hong Kong productions (or in this case co-productions). 

Lost Indulgence missed a second scheduled screening at the Udine East Asian Film Festival, when a screening was again canceled due to failure to obtain official permission.    Upon this second cancellation, critics speculated that producers were taking extra care before the Olympics rather than seeing the delays as a clear cut case of censorship. 

Despite these setback, the film eventually did get its international premiere at the 2008 Tribeca Film Festival on April 25, 2008. 

==Critical reception==
The film opened to mixed reviews.

Perry Lam of Muse (Hong Kong magazine)|Muse Magazine called the movie an object lesson in how bad casting can skew a movie irredeemably. 

== References ==
 

==External links==
* 
* 
*  at the Chinese Movie Database

 

 
 
 
 
 
 