Westbound (film)
{{Infobox film
| name           = Westbound
| image          = Westbound_1959_Poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Budd Boetticher
| producer       = Henry Blanke
| screenplay     = Berne Giler
| story          = {{Plainlist|
* Berne Giler
* Albert S. Le Vino
}}
| starring       = {{Plainlist|
* Randolph Scott
* Virginia Mayo
* Karen Steele
}}
| music          = David Buttolph
| cinematography = J. Peverell Marley
| editing        = Philip W. Anderson
| studio         = Warner Bros.
| distributor    = Warner Bros.
| released       =  
| runtime        = 72 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Western film directed by Budd Boetticher and starring Randolph Scott, Virginia Mayo, and Karen Steele. This is the sixth of seven films directed by Boetticher and starring Scott.

The movie was shot in September 1957 in Warnercolor. at cost of more than a half-million dollars.  The Laramie Street set at Warner’s Burbank was used for the setting of Julesburg, Colorado|Julesburg, Colorado. The Warner Ranch was used for other settings. David Buttolph composed the score. Westbound was released on April 25, 1959.

The movie was not a part of the Ranown cycle of Westerns for which Boetticher, Scott and Harry Joe Brown partnered; Scott owed Warners one picture from an old contract, so Boetticher volunteered to direct it himself so as to protect their brand. Although Boetticher never went so far as to disown the film, he felt it was not part of the series and would only discuss it outside of that context.

==Plot==
In 1864, Union army officer Captain John Hayes is asked to take charge of the Overland stagecoach line, which makes eastbound gold shipments from California that aid the Unions war effort.

Hayes travels to Overland headquarters in his hometown of Julesburg, Colorado. He meets a Union soldier, Rod Miller, who has lost an arm, and Millers wife, Jeannie.

Clay Putnam has quit his position with Overland and is now secretly working for the Confederacy. He has the support of a quick-draw bandit, Mace, and also has married Hayes former love, Norma.

Maces men pick a fight with the one-armed Miller, calling him "half a man" and raising Jeannies ire. Rod is distraught at his condition, unable to even cock a pistol now. Hayes decides to ask the Millers if they would agree to run the local Overland station out of their farm.

Mace wants to kill Hayes, but is talked out of it by Putnam, who fears the Unions response. He orders Maces men to destroy Overlands stations and property instead and steal its deliveries of gold.

Putnam is jealous of Hayes, though, believing Norma is still interested in him. He decides to kill him, but mistakes Rod for Hayes and shoots the wrong man.

Mace drives a stagecoach off a cliff, killing passengers, including women and children. A disgusted Norma decides to leave Putnam and warns she will see him hang if anything should happen to Hayes.

A final confrontation in town results in townspeople offering Hayes their help. Putnam also comes looking for Mace, but is shot, whereupon Mace is killed by Hayes.

Norma hopes to rekindle Hayes love for her, but he appears more likely to have a future with Jeannie.

==Cast==
* Randolph Scott as Hayes
* Virginia Mayo as Norma
* Karen Steele as Jeannie
* Michael Dante as Rod Miller 
* Andrew Duggan as Putnam
* Michael Pate as Mace
* Wally Brown as Stubby
* John Daheim as Russ
* Walter Barnes as Willis - Stage Depot Cook

==Reception==
Critical reaction was subdued despite the presence of Scott and Boetticher, with the two collaborating on a cycle that has received favorable criticism in recent years.  An article in American Cowboy in 2004 called Westbound "a forgotten potboiler that Boetticher directed simply to keep the collaboration going."  The Scarecrow Video Movie Guide in 2004 called this "a contractual obligation Boetticher directed out of friendship" and "otherwise forgettable." A book, Stagecoach to Tombstone, describes the favorable elements: " ... only the presence of Karen Steele at her most tomboyish as Jeannie…and a vicious turn by B-movie hardman Michael Pate as hired gun Mace, are of note."

==DVD release==
Warner Home Video released a DVD in June 2009 on the Warner Archives label.

==References==
* Hughes, Howard.  Stagecoach to Tombstone: the Filmgoers Guide to the Great Westerns.  I.B. Tauris, 2008, p.&nbsp;110.
* Nott, Robert.  Last of the Cowboy Heroes: the Westerns of Randolph Scott, Joel McCrea, and Audie Murphy.  Jefferson, N.C. : McFarland, 2000, pages 136-137.
* Scarecrow Video Movie Guide.  Sasquatch Books, 2004, page 24.
* Teachout, Terry.  “What Randolph Scott Knew” in American Cowboy September–October 2004, page 24.

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 