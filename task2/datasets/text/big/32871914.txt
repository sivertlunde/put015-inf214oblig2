Three in One (1957 film)
 
{{Infobox film
| name           = Three in One
| image          = 
| image_size     = 
| caption        =  Cecil Holmes
| producer       = Cecil Holmes Ralph Peterson (The City) based on = stories by Henry Lawson (Joe Wilsons Mates) Frank Hardy (The Load of Wood) John McCallum
| starring       = Reg Lye
| music          = Raymond Hanson
| cinematography = Ross Wood
| editing        = A. William Copeland
| studio         = Australian Tradition Films
| released       = 1957 (Australia)
| runtime        = 89 mins
| country        = Australia
| language       = English
| budget         = ₤28,000 Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998 
}} Cecil Holmes consisting of three separate stories, "A Load of Wood", "The City" and "Joe Wilsons Mates". 

==Plot== Joe Wilson dies alone in a small town during the 1890s without friend or family. But because he carries a union card the local union member give him a decent burial.

The Load of Wood is set during the 1930s. Two men are doing relief work but can not afford to buy enough fuel to keep their families warm. They steal a truck of wood from a rich mans estate and distribute it around to need families.

In The City a young factory worker and ship assistant plan to marry but cannot afford it. The argue an walk the streets but realise they love each other.

==Cast==
 
===Joe Wilsons Mates===
* Reg Lye as the swaggie
*Edmund Allison as Tom Stevens
*Alexander Archdale as Firbank
*Charles Tasman as the undertaker
*Don McNiven as Patrick Rooney
*Jerold Wells as Wally
*Chris Kempster as Longun
*Brian Anderson as Joe
*Kenneth Warren as Andy
*Evelyn Docker as Maggie
*Ben Gabriel as the priest
*the Bushwackers Band
===The Load of Wood===
*Jock Levy as Darkie
*Leonard Thiele as Ernie
*Ossie Wenban as Sniffy
*John Armstrong as Chilla
*Jim Doone as Joe
*Ted Smith as Coulson
*Edward Lovell as The
*Keith Howard as Shea
*Eileen Ryan as Mrs Johnson
===The City===
*Joan Landor as Kathie
*Brian Vicary as Ted
*Betty Lucas as Freda
*Gordon Glenwright as Alex
*Ken Wayne as first cab driver
*Stewart Ginn as second cab driver
*Alan Trevor as preacher
*PatMartin as customer
*Margaret Christensen as customer
*Alastair Roberts as bodgie
 

==Production==
Filming for tall three stories took place at Pagewood Studios before and after Smiley was shot there in 1955. Exteriors for the first two stories were shot in Camden and locations for the third were filmed on Sydney streets. 

==Reception==
The film was positively received by overseas critics and screened at a number of festivals (including one in Communist China ) but struggled to obtain commercial release in Australia. It failed to recover its cost and Australian Tradition Films was liquidated in 1959. 

==References==
 

==External links==
*  in the Internet Movie Database
*  at Oz Movies
 
 
 