Dick Tracy Returns
{{Infobox Film|
| name           = Dick Tracy Returns
| image          = DickTracyReturns.jpg
| image_size     = 
| caption        =  John English
| producer       = Robert M Beche
| writer         = Franklin Adreon Ronald Davidson Barry Shipman Sol Shor Rex taylor Chester Gould (comic strip) Charles Middleton David Sharpe Lee Ford William Nobles
| distributor    = Republic Pictures
| released       = USA 20 August 1938 {{cite book
 | last = Mathis
 | first = Jack
 | title = Valley of the Cliffhangers Supplement 
 | origyear = 1995
 | publisher = Jack Mathis Advertising
 | isbn = 0-9632878-1-8
 | pages = 3, 10, 32–33
 | chapter = 
 }}  USA 17 July 1948 (re-release) 
| runtime         = 15 chapters / 254 minutes 
| language       = English
| country        = United States
| budget          = $156,991 (negative cost: $170,940) 
| gross          = 
}} Republic Movie Dick Tracy, with Ralph Byrd reprising his role as the title character.  It was successful enough that two further sequels were released, in 1939 and 1941, and Byrd become so connected with the character he went on the play him in a subsequent television series.
 Charles Middleton).

==Synopsis==
Tracy and his group must battle saboteurs and spies in his effort to bring down the Stark gang, a major crime family syndicate led by the vicious and brutal Pa Stark...

==Cast==
*Ralph Byrd as Dick Tracy
*Lynne Roberts as Gwen Andrews Charles Middleton as Pa Stark.  Pa Stark was based on the real criminal Ma Barker. {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | origyear = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 20–21
 | chapter = 2. In Search of Ammunition
 }} 
*Jerry Tucker as Junior David Sharpe as Agent Ron Merton, a newly trained agent.
*Lee Ford as Mike McGurk Michael Kent as Agent Steve Lockwood
*John Merton as Champ Stark
*Raphael Bennett as Trigger Stark Jack Roberts as Dude Stark
*Ned Glass as Kid Stark Jack Ingram as Slasher Stark

==Production==
Dick Tracy Returns was budgeted at $156,991 although the final negative cost was $170,940 (a $13,949, or 8.9%, overspend).  It was the most expensive Republic serial of 1938 and the most expensive Republic serial until The Lone Ranger Rides Again in 1939.   It was the second most expensive of the four Dick Tracy serials (the most expensive was the last, Dick Tracy vs Crime Inc at $175,919 ).

It was filmed between 10 June and 18 July 1938 under the working title Return of Dick Tracy.   The serials production number was 791. 
 West Coast FBI agent rather than as the local police detective for a large Midwestern city he is in the original comic strip.
 Dick Tracy serial,  was permitted by an interpretation of the original contract, which allowed a "series or serial".  Therefore, Chester Gould was not paid again for the right to produce this serial. 

===Special effects===
The special effects were created by Republics in-house team, the Lydecker brothers

===Stunts===
*Earle D. Bunn
*Yakima Canutt
*George DeNormand as Dick Tracy (doubling Ralph Byrd)
*Duke Green
*George Magrill Eddie Parker
*Allen Pomeroy
*Loren Riebe Ted Wells
*Bud Wolfe

==Release==
===Theatrical===
Dick Tracy Returns official release date is 20 August 1938, although this is actually the date the seventh chapter was made available to film exchanges. 

The serial was re-released on 17 July 1948 between the first runs of Dangers of the Canadian Mounted and Adventures of Frank and Jesse James. 

==Critical reception==
Cline states that the Dick Tracy serials were "unexcelled in the action field," adding that "in any listing of serials released after 1930, the four Dick Tracy adventures from Republic must stand out as classics of the suspense detective thrillers, and the models for many others to follow." 

==Chapter titles==
# The Sky Wreckers (29min 51s)
# The Runway of Death (16min 34s)
# Handcuffed to Doom (16min 20s)
# Four Seconds to Live (15min 39s)
# Death in the Air (16min 35s)
# Stolen Secrets (15min 23s)
# Tower of Death (14min 34s)
# Cargo of Destruction (16min 12s)
# The Clock of Doom (16min 4s) - a clipshow|re-cap chapter
# High Voltage (16min 15s)
# The Kidnapped Witness/The Missing Witness (15min 45s) 
# The Runaway Torpedo (15min 33s)
# Passengers to Doom (16min 19s) - a clipshow|re-cap chapter
# In the Hands of the Enemy (16min 30s)
# G-Mens Drag-Net (16min 24s)
 Source:   {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | origyear = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | chapter = Filmography
 | page = 222
 }} 
 The Lone Ranger).  The other two were only 12 chapters long.

==References==
 

==External links==
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 