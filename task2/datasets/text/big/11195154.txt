A Christmas Tale
 
{{Infobox Film
| name           = A Christmas Tale
| image          = Achristmastaleposter.jpg
| caption        = Theatrical release poster
| director       = Arnaud Desplechin
| producer       = Pascal Caucheteux
| writer         = Arnaud Desplechin Emmanuel Bourdieu
| starring       = Catherine Deneuve Jean-Paul Roussillon Mathieu Amalric Anne Consigny Melvil Poupaud Emmanuelle Devos Chiara Mastroianni
| music          = Grégoire Hetzel
| cinematography = Eric Gautier
| editing        = Laurence Briaud
| studio         = Why Not Productions
| distributor    = Bac Films 
| released       =  
| runtime        = 150 minutes
| country        = France
| language       = French
| budget         = 
| gross          = $7,356,393 
}}

A Christmas Tale ( ) is a 2008 French comedy-drama film by Arnaud Desplechin, starring Catherine Deneuve, Jean-Paul Roussillon, Mathieu Amalric, Anne Consigny, Melvil Poupaud, Emmanuelle Devos and Chiara Mastroianni. It tells the story of a family with strained relationships which gathers at the parents home for Christmas, only to learn that their mother has leukemia. It was in competition for the Palme dOr at the 2008 Cannes Film Festival.

==Plot==
 
Junon Vuillard is Abel Vuillards wife, and the iron-willed matriarch of the Vuillard family. Junon held her family together through many tough times, and although her willpower helped the family survive and prosper, it also has left many bad feelings among her children. Junon is still a handsome woman, and though her husband (who owns a small fabric dying plant) has become obese and clearly aged, he retains a remarkable clarity, acceptance, tolerance, and unconditional love for his family, and it is clear that he and their love for each other is the lynchpin that holds an otherwise fragmented family together, albeit uneasily.

The couple has three children, all grown and in their 30s.  Their eldest daughter is Elizabeth, who is a successful playwright who is married to an equally successful man, Claude.  They have one child, 16-year-old Paul, who is mentally ill and taking powerful medication to control his psychiatric problems.  The couples middle son is Henri, who drinks too much and has always fought and argued with other members of the family.  He has a new girlfriend, Faunia.  Ivan is the couples youngest son.  He is married to Sylvia, and they have two sons, Basile and Baptiste.  Henri and Ivan are close friends with Simon, their cousin who was raised with them after the death of his parents. Simon works in Abels small fabric dying plant, but is an avid and skilled painter in his spare time.  He is also an alcoholic who has gotten in trouble many times for brawling in public.  All three men dated or were interested in Sylvia at one time, but they manipulated her to think that only Ivan loved her; she married him and grew to love him. The Vuillard familys other son, Joseph, never appears alive in this film but is its core throughout, and the presence around which everyones psyches revolve: he died of leukemia when he was six years old, despite a desperate effort to save his life by procreating another child who could be a bone marrow donor for him. It may be that part of the siblings poor relationship is the resentment they feel toward one another for not saving Josephs life.

Six years prior to the Christmas gathering that is the heart of this film, Henri faced bankruptcy.  Elizabeth paid off his debts, but demanded that he never see her again, meaning he was left out of family gatherings as well.  Henri kept his promise.  The specific reason (if there is one) for this banishment remains unanswered to the very end, but there is much speculation on the part of the family members throughout the film about what precipitated it, including incest.

Just before Christmas, Junon learns that she, too, has leukemia and does not have long to live, though she is offered the potential of a longer lifespan if she gets a bone marrow transplant.  Her family gathers at the Vuillard home in Roubaix, a small city in the north of France. The family immediately falls to bickering. Junon asks her children if one of them (or possibly Paul) will donate bone marrow to her that will allow her to survive.  Henri and Elizabeth cruelly fight with one another, and Henri begins drinking heavily and hides Pauls medication. Paul fears that the blood test he will have to undergo may also reveal that his father is not his biological parent.  Henri initially refuses to have the blood test, because, he claims, he has never loved his mother.  Faunia, who is Jewish, has agreed to spend some time with the Vuillards before leaving to spend the holiday with her own family.  Her honesty and gentleness have a moderating impact on Henri, and she manages to spend two days with the family before finally leaving.

On 23 December, Rosaimée visits the family for dinner and fireworks. Rosaimée was Abels mothers friend, although it is suggested that perhaps the two women were lesbian lovers rather than "just friends." Rosaimée tells Sylvia that Simon stopped seeing Sylvia because he believed that she would be happier if she fell in love with Ivan. This deeply upsets Sylvia, who feels betrayed and manipulated by Simon. Henri finally has the blood test without anyones knowledge, and discovers he can be a donor. He decides to do so despite his coldness toward his mother. Simon disappears on Christmas Eve and begins drinking heavily in local cafes, and the entire family rushes out into the snow to find him. Sylvia discovers him in a distant cafe, and she confesses that she knows Simon loves her.  She and Simon spend several hours talking, then return to the Vuillard home and make love.  Paul tries to tell Henri about his fears. Henri convinces him that he is not his father, a fact reinforced by the blood test, and reassures Paul that he is not a moral failure for being afraid.  The man and boy begin to bond, and Paul begins to improve almost overnight. On Christmas Day, Abel and Elizabeth discuss Elizabeths longstanding depression, and Abel reads to her from the prologue to Friedrich Nietzsches On the Genealogy of Morality about how well we know – or dont know – ourselves. Abel suggests that Elizabeth fears death, and that has led to her caution and depression.

The film ends with Ivan casually discovering that his wife has had sexual intercourse with Simon (they make no real effort to hide it, waking up together in bed the next day and greeting her children from there as they come bearing tea), but the effect on him is not revealed – he seems remarkably blase, almost as though he has expected that this would happen one day.  Sylvia seems to love Simon just as he loves her, and it is suggested that the two have agreed to become lovers.  Paul decides to stay behind with Henri, who is having a positive effect on his mental health.  Henri donates his bone marrow to Junon, but she announces, seemingly before there would be medical evidence of this, that her body will reject the transplant.  Elizabeth speculates that Junon will live, but Henri is shown flipping a coin in the hospital in front of his mother and not revealing the answer.

==Cast==
*Catherine Deneuve as Junon
*Jean-Paul Roussillon as Abel
*Anne Consigny as Elizabeth
*Mathieu Amalric as Henri
*Melvil Poupaud as Ivan
*Emmanuelle Devos as Faunia
*Laurent Capelluto as Simon
*Chiara Mastroianni as Sylvia
*Hippolyte Girardot as Claude
*Emile Berling as Paul
*Françoise Bertin as Rosaimée

==Themes==
 
The film explores family relationships, how some conflicts are resolved, and how relationship constantly evolve between mother and children, lovers, spouses, and siblings in the context of a Christmas family gathering. There are philosophical insights into the complexity of life, and we gain a deeper understanding of each person in the family, the dynamics of the Vuillard family, and how we deal with adversity.

==Critical reception==

===Awards and nominations=== Broadcast Film Critics (USA) Best Foreign Language Film
 Cannes Film Festival (France)
**Nominated: Golden Palm (Arnaud Desplechin)

*César Awards (France) Best Actor&nbsp;– Supporting Role (Jean-Paul Roussillon) Best Actress&nbsp;– Supporting Role (Anne Consigny) Best Cinematography (Eric Gautier) Best Director (Arnaud Desplechin) Best Editing (Laurence Briaud) Best Film Best Sound (Nicolas Cantin, Jean-Pierre Laforce and Sylvain Malbrant) Best Writing&nbsp;– Original (Emmanuel Bourdieu and Arnaud Desplechin) Most Promising Actor (Laurent Capelluto)
 Chicago Film Critics (USA) Best Foreign Language Film

*  (France)
**Won: Best Director (Arnaud Desplechin)

*Online Film Critics Society (USA) Best Foreign Language Film

*Satellite Awards (USA) Best Actress&nbsp;– Musical or Comedy (Catherine Deneuve)

===Reviews===
* 

===Top ten lists===
The film appeared on many critics top ten lists of the best films of 2008.     

 
 
*1st&nbsp;— Andrew OHehir, Salon.com|Salon  Dana Stevens, Slate (magazine)|Slate 
*1st&nbsp;— Josh Rosenblatt, The Austin Chronicle 
*1st&nbsp;— Rick Groen, The Globe and Mail 
*1st&nbsp;— Sean Axmaker, Seattle Post-Intelligencer  Shawn Levy, The Oregonian  The Class) 
*2nd&nbsp;— Kimberly Jones, The Austin Chronicle 
*2nd&nbsp;— Stephanie Zacharek, Salon.com|Salon 
  Michael Phillips, Chicago Tribune 
*4th&nbsp;— Lou Lumenick, New York Post  The Class) 
*6th&nbsp;— Dennis Harvey, Variety (magazine)|Variety 
*7th&nbsp;— Scott Foundas, LA Weekly (tied with The Secret of the Grain) 
*7th&nbsp;— Stephen Holden, The New York Times 
*8th&nbsp;— Liam Lacey, The Globe and Mail 
*10th&nbsp;— Robert Mondello, National Public Radio|NPR 
 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 