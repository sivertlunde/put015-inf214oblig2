Schooled (film)
{{multiple issues|
 
 
}}
{{Infobox film
| name           = Schooled
| image          = 
| caption        = 
| director       = Brooks Elms
| producer       = Brian Hennessy, Lorenda Starfelt
| writer         = Brooks Elms
| starring       = Daniel Kucan, Alysia Reiner, Vladimir Borges, Kelly Sheerin, Ashley Argota
| music          = Andrew Hollander
| cinematography = Sky Borgman
| editing        = Frederick Marx, Ken Scribner
| distributor    = 
| released       =  
| runtime        = 100 minutes
| country        = United States
| awards         =
| language       = English
| budget         = 
| gross          = 
}} independent drama East Coast school teacher (Daniel Kucan), who becomes involved with a non-traditional, alternative school in California, which leads to significant changes in his personal philosophy and lifestyle. The film co-stars Alysia Reiner, and was co-edited by Frederick Marx, the writer/producer/co-editor of the 1994 documentary Hoop Dreams.

==Production==
Schooled was shot on a limited budget in and around Los Angeles county in 2005-2006.

==Release==
Schooled premiered at the  .  The film had its East Coast premiere at the Queens International Film Festival.  

==Reception== democratic schools".  According to a press release for the film, Schooled  writer/director Brooks Elms studied film at New York University, where he received the Warner Bros. Production Grant, the NYU Film Fest Screenplay Award, and the Steven J. Ross Filmmaker’s Grant from the Hamptons Film Festival. 

==Impact==
Since its release in 2007, the film has generated debate amongst communities contemplating involvement in alternative education. 

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 