The Scar (film)
 
{{Infobox film
| name           = The Scar
| image          = The_Scar.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Krzysztof Kieślowski
| producer       = Zbigniew Stanek
| writer         = Krzysztof Kieślowski Romuald Karaś
| narrator       =
| starring       = Franciszek Pieczka Mariusz Dmochowski Halina Winiarska
| music          = Stanisław Radwan
| cinematography = Sławomir Idziak
| editing        = Krystyna Górnicka
| distributor    = Artificial Eye  (UK)  Kino Video  (USA) 
| released       =  
| runtime        = 112 minutes
| country        = Poland Polish
| budget         =
| gross          =
}} 1976 Polish film written and directed by Krzysztof Kieślowski and starring Franciszek Pieczka.    Filmed on location in Olechów, Poland, the film is about a man put in charge of the construction of a large chemical factory in his home town in the face of strong opposition from the townspeople who are concerned with their short-term needs. The film received the Polish Film Festival Special Jury Prize (Krzysztof Kieslowski) and Best Actor Award (Franciszek Pieczka) in 1976.    The Scar was Krzysztof Kieślowskis first theatrical feature film. 

==Plot==
After discussions and dishonest negotiations, a decision is made as to where a large new chemical factory is to be built. Stefan Bednarz (Franciszek Pieczka), an honest Party man, is put in charge of the construction. Bednarz used to live in the small town where the factory is to be built, and his wife used to be a Party activist there. Although he has unpleasant memories of the town, Bednarz sets out to build a place where people will be able to live well and work well. His intentions and convictions, however, conflict with those of the townspeople who are mainly concerned with their short-term needs. Disillusioned, Bednarz gives up his position.

==Cast==
* Franciszek Pieczka as Stefan Bednarz
* Mariusz Dmochowski as Vorsitzender
* Jerzy Stuhr as Bednarzs assistant
* Jan Skotnicki
* Stanislaw Igar as Minister
* Stanislaw Michalski
* Michal Tarkowski as TV editor
* Andrzej Skupien
* Halina Winiarska as Bednarzs Wife
* Joanna Orzeszkowska as Eva (Bednarzs Daughter)
* Jadwiga Bryniarska
* Agnieszka Holland as Secretary
* Malgorzata Lesniewska
* Asja Lamtiugina 
* Ryszard Bacciarelli
* F. Barfuss
* Bohdan Ejmont
* Henryk Hunko
* Jan Jeruzal
* Zbigniew Lesien
* Konrad Morawski
* Jerzy Prazmowski
* Jan Stawarz
* Wojciech Stockinger
* Kazimierz Sulkowski   

==Production==
;Filming locations
* Olechów, Lódz, Lódzkie, Poland   

==Reception==
;Awards and nominations
* 1976 Polish Film Festival Special Jury Prize (Krzysztof Kieslowski) 
* 1976 Polish Film Festival Award for Best Actor (Franciszek Pieczka) 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 


 