The Hamiltons
 
{{Infobox film
| name           = The Hamiltons
| image          = File:Hamiltonsposter.jpg
| caption        = 
| director       = The Butcher Brothers
| producer       = Mitchell Altieri Phil Flores
| writer         = The Butcher Brothers Adam Weis
| starring       = Cory Knauf Samuel Child Joseph McKelheer Mackenzie Firgens
| music          = Josh Myers
| cinematography = Michael Maley 
| editing        = Jesse Spencer
| distributor    = Lions Gate Films
| released       =  
| runtime        = 86 minutes
| country        = United States
| language       = English
| budget         = 
}}
 independent 2006 horror film directed by the Butcher Brothers (Mitchell Altieri and Phil Flores).  Cory Knauf stars as a teenager who must decide whether to help the victims that his older siblings have kidnapped.

==Plot==

A suburban family harbors a dark secret: When their parents are killed in a tragic accident, eldest Hamilton sibling David (Samuel Child) relocates the surviving family members to a quiet California suburb and assumes the responsibility of caring for his orphaned teenage siblings; Wendell (Joseph McKelheer), Darlene (Mackenzie Firgens), and Francis (Cory Knauf). While twins Wendell and Darlene seem to share a bizarre incestuous bond that separates them from the rest of the siblings, Francis acquires a video camera that previously belonged to his deceased parents and sets out preparing a school project about his family.

The all-seeing lens of Francis roving camera begins to reveal that something malevolent is going on inside the Hamiltons picturesque abode. David finds work in a meat-packing warehouse, and is shown to bring some stuff from work home with him, while it is also implied that he is interested in men as he regularly invites male co-workers to go with him to a nearby motel for an evening.

One evening, Wendell kidnaps two young girls whom he meets in a local bar and they are tied in their storeroom. Older brother David drains blood off one of them till she slowly dies. Francis gets attracted to the other girl who tries to get him to help her. But things come to a head when Wendell and Darlene murder one of her classmates and feed off her blood.

When David tries to kill the other girl, Francis hits him in the head and carries her off to a safe hideout at the meat packing plant which is closed for the night. Wendell asks David to follow Francis, as he is the only one capable of persuading him to return. At the barn, Francis, seeing an open wound on the girls hand, is unable to control his bloodlust and he kills the girl and feeds off her. When David appears, Francis is crying and we see his fangs. Though terribly sad at his actions, he finally seems to be accepting what he really is: a vampire.

With the death of Darlenes classmate, David decides to move somewhere else, and they go to the basement to retrieve the creature locked there, who turns out to be their little brother Lenny.

The family, the Hamilitons, is revealed to be a new breed of vampires who are born that way, not made. They move from place to place to hide their secret of their thirst for blood and to avoid detection from the authorities. In the final scene, we see them introducing themselves to their new neighbors, as the Thompsons, and Francis, now having embraced his newfound status as a vampire, is shown making a happy video of his now complete family.

==Cast==

* Cory Knauf as Francis Hamilton
* Samuel Child as David Hamilton
* Joseph McKelheer as Wendell Hamilton
* Mackenzie Firgens as Darlene Hamilton
* Rebekah Hoyle as Samantha Teal
* Brittany Daniel as Dani Cummings
* Al Liner as Paul Glenn
* Jena Hunt as Kitty Davies
* Tara Glass as Jenna Smith
* Larry Laverty as Larry Davies
* Joe Egender as Allen Davies
* Nicholas Fanella as Lenny Hamilton
* Jackie Honea as Mrs. Hamilton
* John Krause as Mr. Hamilton
* Nathan Parker as Hot Pants

==Release==
The Hamiltons was part of the 2006 After Dark Horrorfest.  It received a theatrical release on November 17, 2006.     It was released on DVD on March 27, 2007. 

In October 2011, the film was adapted into a play.   

===Sequel===
 The Thompsons, was released in 2012. 

==Reception==

In a positive review, Robert Koehler of Variety (magazine)|Variety states that The Hamiltons "refuses to play by most of the genre rules".   Joshua Siebalt of DreadCentral rated the film 3.5/5 stars and called it "a damn solid film."   Brad Miska of Bloody Disgusting rated it 3.5/5 stars and called it "a truly unique horror film, which leads up to one final moment that is sure to leave you in a state of shock."   In a mixed review, Bill Gibron of DVD Verdict described the film as amateurish but the best After Dark Horrorfest entry that year.   Scott Weinberg of DVD Talk rated the film 2.5/5 stars and described it as "a pretty amateurish affair."   Don R. Lewis of Film Threat rated the film 4.5/5 stars and states that The Hamiltons is "one of the best indie horror films in a great long while."   Scott Collura of IGN rated the film 5/10 and criticized the films production values and lack of gore, though he called it "a solid little horror film." 

===Awards===

The Hamiltons won the jury prize at the Santa Barbara International Film Festival and the Malibu International Film Festival. 

==See also==
*Vampire film

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 