The Ernie Game
 
{{Infobox film
| name           = The Ernie Game
| image          = 
| caption        =  Don Owen Robert Allen Gordon Burwash
| writer         = Don Owen
| starring       = Alexis Kanner
| music          = 
| cinematography = Jean-Claude Labrecque
| editing        = Roy Ayton
| studio         = National Film Board of Canada
| released       =  
| runtime        = 89 minutes
| country        = Canada
| language       = English
| budget         = 
}} Don Owen.

Called "One of the most innovative examples of personal cinema to come from English Canada in the Sixties" by the Cinematheque Ontario, The Ernie Game was part of a proposed trio of works intended to celebrate the Canadian Centennial. The film centres on Ernie Turner and his attempts to survive in the world after hes released from an insane asylum|asylum. He grows increasingly alienated and his fragile mental state declines, moving between two women, ex-girlfriend and current lover. "The Ernie Game provides a resonant portrait of mental illness," writes Steve Gravestock of the Cinematheque, "its pathologically narcissistic protagonist representing Owen’s most nightmarish vision of the artist as fraud and pariah."   

Owen risked his career at the NFB when he surreptitiously turned what was to have been a half-hour educational film into the of the few English-language dramatic features made in Canada during the 1960s.   

Produced by the National Film Board of Canada, The Ernie Game received the Etrog Awards, now known as Genie Awards, for Best Direction and Best Feature Film in 1968.    It was also entered into the 18th Berlin International Film Festival.   

==Cast==
* Jackie Burroughs as Gail
* Anna Cameron as Social worker
* Leonard Cohen as Singer
* Corinne Copnick as Landlady
* Rolland DAmour as Neighbour
* Judith Gault as Donna
* Alexis Kanner as Ernie Turner
* Derek May as Ernies accomplice
* Louis Negin as Ernies friend

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 


 
 