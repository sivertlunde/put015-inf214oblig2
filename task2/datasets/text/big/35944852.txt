Déjà Vu (1985 film)
{{Infobox film
| name           = Déjà Vu
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Anthony B. Richmond
| producer       =  
| writer         =  
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       =  
| music          = Pino Donaggio
| cinematography = 
| editing        = Richard Trevor
| studio         = Cannon Films
| distributor    = The Cannon Group
| released       =  
| runtime        = 90 min.
| country        = United Kingdom
| language       = English, Italian
| budget         = 
| gross          = 
}} 

Déjà Vu is a motion picture released in 1985, produced by Cannon Films. The movie, an adaptation of the novel Always, by Trevor Meldal-Johnsen, is a reincarnation love story, directed by Anthony B. Richmond, and written by Richmond, Ezra D. Rappaport, and Arnold Anthony Schmidt. The film stars Jaclyn Smith, Claire Bloom, Nigel Terry and Shelley Winters.   

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 

 