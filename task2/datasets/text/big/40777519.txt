Apenas o Fim
{{Infobox film
| name           = Apenas o Fim
| image          = Apenas o Fim.jpg
| caption        = 
| director       = Matheus Souza
| producer       = 
| writer         = Matheus Souza
| starring       = Gregório Duvivier Érika Mader Nathalia Dill Marcelo Adnet Álamo Facó
| music          = Pedro Carneiro
| cinematography = Julio Secchin
| editing        = Julio Secchin
| studio         = 
| distributor    = 
| released       =  
| runtime        = 80 minutes
| country        = Brazil
| language       = Portuguese
| budget         = BRL|R$ 8,000 
}}

Apenas o Fim is a 2008 Brazilian comedy film|comedy-romance film directed by Matheus Souza, his directorial debut, and starring Gregório Duvivier, Érika Mader,  Nathalia Dill and Marcelo Adnet.

The film is the result of a project by students of the film school of Pontifical Catholic University of Rio de Janeiro|PUC-Rio, and it was all filmed at the university, with the support of the Department of Social Communication. 

==Plot==
A girl decides to run away from her ordinary life leaving her parents, friends and her boyfriend Antonio. But before leaving, she resolves to spend the last hour with him, having a long conversation while walking in college. They speak about their relationship remembering the past, imagining the future and discussing a number of fears and issues involving their generation. 

==Cast==
* Gregório Duvivier
* Érika Mader
* Nathalia Dill
* Marcelo Adnet
* Álamo Facó

==Awards and nominations==
* Festival do Rio - 2008 
** Honorable Mention - Jury 
** Best Fiction Feature Film - The public choice award 32nd São Paulo International Film Festival - 2008 
** Best Brazilian Fiction Feature Film - The public choice award

==References==
 

==External links==
*  

 

 
 
 
 
 