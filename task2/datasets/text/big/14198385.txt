Genghis Khan: To the Ends of the Earth and Sea
 
{{Infobox film name = Genghis Khan: To the Ends of the Earth and Sea image = Genghis_Khan_2007_film_poster.jpg caption = Japanese poster director = Shinichirō Sawai producer = Minoru Ebihara Haruki Kadokawa Katsuhito Matsuura Yutaka Okada Akihiko Ōsugi Yoshiaki Tokutome writer = Takehiro Nakajima Shōichi Maruyama narrator = starring = Takashi Sorimachi Rei Kikukawa Mayumi Wakamura music = Taro Iwashiro cinematography = Yonezō Maeda editing = Akimasa Kawashima studio = Kadokawa Haruki, Avex Group|Avex, H.I.S., Tokyo FM, Shochiku, Yahoo! Japan, Japan FM Network, Yomiuri Shimbun, Japan Airlines) distributor = Shochiku (Japan) Funimation (United States|U.S.)  . Retrieved February 25, 2008.  released = March 3, 2007 (Japan) February 21, 2008 (U.S. limited)  runtime = 136 minutes country = Japan/Mongolia language = Japanese budget = US$30 million based on = Novels by Seiichi Morimura
}} 2007 Cinema Japanese - historical drama film depicting the life of Genghis Khan.

==Cast==
*Takashi Sorimachi as Genghis Khan|Temüjin/Genghis Khan
*Rei Kikukawa as Börte
*Mayumi Wakamura as Hoelun
*Kenichi Matsuyama as Jochi
*Yoshihiko Hakamada as Hasar Khulan
*Yūsuke Hirayama as Jamukha
*Naoki Hosaka as Yesugei|Yesügei Bagatur Toghrul Khan
*Eugene Nomura as Boorču
*Eri Shimomiya as Temulen
*Shōhei Yamazaki as Behter
*Kairi Narita as Belgutei
*Takuya Noro as Chilaun
*Sōsuke Ikematsu as young Genghis Khan|Temüjin
*Ami Takeishi as young Börte
*Takeshi Ōbayashi as Caravan Chief

==Plot==
Temujin (Sorimachi Takashi) is born the son of the chief of a Mongolian tribe, and grows up as the one who carries the blood of “blue wolf”. When he is grown up, he marries Borte (Kikukawa Rei), but one night, she is taken away by another tribe. When Temujin rescues her, she is pregnant, and a boy is born not long thereafter. As the boy may be a son of a stranger, Temujin names him Jochi (Matsuyama Kenichi), meaning outsider, and refuses to accept him as his son. The time goes on and Temujin is enthrowned as the King of Mongolia. He changes his name to Genghis Khan and pledges to avenge his long-time enemy, the Jin Dynasty. Genghis Khan finally acknowledges Jochi as his own son, and they decide to fight together, but Jochi is killed by the enemy. The lonely battle of Genghis continues without end. A historical drama about the life of Genghis Khan, a hero who united the Mongol Empire. 

==Production==
Takehiro Nakajima and Shōichi Maruyama adapted the screenplay from the historical-fiction novels Chi Hate Umi Tsukiru Made: Shōsetsu Chingisu Hān (Ue) and Chi Hate Umi Tsukiru Made: Shōsetsu Chingisu Hān (Shita) by Seiichi Morimura.

The film cost US$30 million to make, and was filmed over four months in 2006 in Mongolia, featuring more than 27,000 extras, as well as 5,000 Mongolian Army soldiers. 

==Release== Cannes Film Market, the Moscow International Film Festival and the 2007 Antalya Golden Orange Film Festival. It was the opening film of the 5th World Film Festival of Bangkok and the San Francisco Asian Film Festival.

Genghis Khan was released by The Bigger Picture in only 40 U.S. theaters on February 21, 2008. As of February 25, it has made only US$3,892 there. It grossed nearly US$11 million in Japan and Mongolia. 

Genghis Khan was released on DVD in the US in 2008.

==References==
 

==External links==
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 