Njan Ekananu
{{Infobox film 
| name           = Njan Ekananu
| image          =
| caption        =
| director       = P Chandrakumar
| producer       = Madhu
| writer         = Sudha P Nair
| screenplay     = Sudha P Nair Madhu Dileep Poornima Jayaram Sukumari
| music          = MG Radhakrishnan
| cinematography = Jayanan Vincent
| editing        = MV Natarajan
| studio         = Uma Arts
| distributor    = Uma Arts
| released       =  
| country        = India Malayalam
| Lyrics         =  Sathyan Anthikadu
}}
 1982 Cinema Indian Malayalam Malayalam film, directed by P Chandrakumar and produced by Madhu. The film stars Madhu (actor)|Madhu, Dileep, Poornima Jayaram and Sukumari in lead roles. The film had musical score by MG Radhakrishnan.   

==Cast== Madhu as Madhavankutty Menon IPS
*R. Dilip (Tamil actor)|R. Dilip as Dillan/Dileep
*Poornima Jayaram as Sindhu
*Sukumari as Vasanthy
*Jagathy Sreekumar as Sreekumaran
*Sankaradi as Ramettan Janardhanan as Raghu
*Sreerekha as Uma
*Srividya as Dr Seethalakshmi

==Soundtrack==
The music was composed by MG Radhakrishnan and lyrics was written by Sathyan Anthikkad. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || O Mridule (Sad) || K. J. Yesudas || Sathyan Anthikkad || 
|-
| 2 || O Mrudule || K. J. Yesudas || Sathyan Anthikkad || 
|-
| 3 || Pranaya vasantham || K. J. Yesudas, KS Chithra || Sathyan Anthikkad || 
|-
| 4 || Rajani parayu || KS Chithra || Sathyan Anthikkad || 
|}

==References==
 

==External links==
*  

 
 
 

 