The Mating of Marcus
 
 
{{Infobox film
| name           = The Mating of Marcus
| image          =
| caption        =
| director       = W.P. Kellino 
| producer       = 
| writer         = Mabel Grundy (novel)  David Hawthorne George Bellamy   Moore Marriott 
| music          = 
| cinematography = 
| editing        = 
| studio         = Stoll Pictures
| distributor    = Stoll Pictures
| released       = 1926
| runtime        = 6,000 feet  
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         = 
| preceded_by    =
| followed_by    =
}} British silent silent romance David Hawthorne, George Bellamy and Moore Marriott  It was based on a novel by Mabel Grundy.

==Cast== David Hawthorne – Marcus Netherby  George Bellamy – Mr. Chester 
* Moore Marriott – Reverend Cheffins 
* Mollie Johnson – Valerie Westmacott
* Pauline Cartwright – Vivi Chester 
* Beatrice Ford – Naomi Chester 

==References==
 

==Bibliography==
* Low, Rachael. History of the British Film, 1918–1929. George Allen & Unwin, 1971.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 

 