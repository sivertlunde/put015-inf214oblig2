Lorna Doone (1912 film)
{{Infobox film
| name           = Lorna Doone 
| image          =
| caption        =
| director       = Wilfred Noy
| producer       = 
| writer         = R. D. Blackmore (novel) 
| starring       = Dorothy Bellew
| music          = 
| cinematography = 
| editing        = 
| studio         = Clarendon Films
| distributor    = Gaumont British Distributors
| released       = December 1912 
| runtime        = 
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         =
| preceded_by    =
| followed_by    =
}} silent historical film directed by Wilfred Noy and starring Dorothy Bellew. The film is an adaptation of the 1869 novel Lorna Doone by R. D. Blackmore, set in Seventeenth century Devon. 

==Cast==
* Dorothy Bellew as Lorna Doone

==References==
 

==Bibliography==
* Klossner, Michael. The Europe of 1500-1815 on Film and Television: A Worldwide Filmography of Over 2550 Works, 1895 Through 2000. McFarland & Company, 2002.

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 


 
 