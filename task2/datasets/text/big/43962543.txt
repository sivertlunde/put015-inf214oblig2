Boy Friend (1975 film)
{{Infobox film
| name = Boy Friend
| image =
| caption =
| director = P. Venu
| producer = P. Venu
| writer = Sasikala Venu P. Venu (dialogues)
| screenplay = P. Venu
| starring = Sukumari Adoor Bhasi Pattom Sadan Peethambaran
| music = G. Devarajan
| cinematography = Vipin Das
| editing = G Kalyana Sundaram
| studio = Anupama Films
| distributor = Anupama Films
| released =  
| country = India Malayalam
}}
 1975 Cinema Indian Malayalam Malayalam film, directed and produced by P. Venu. The film stars Sukumari, Adoor Bhasi, Pattom Sadan and Peethambaran in lead roles. The film had musical score by G. Devarajan.   
 
==Cast==
  
*Sukumari 
*Adoor Bhasi 
*Pattom Sadan 
*Peethambaran
*Sreelatha Namboothiri 
*Surendran
*Girijan 
*Jameela Malik 
*K. P. Ummer 
*Kuthiravattam Pappu 
*Lissy
*Mallika Sukumaran 
*Rani Chandra 
*Ravi Menon  Reena 
*S. P. Pillai 
*Sadasivan  Sadhana  Sudheer 
*Swapna
*Thankappan 
*Vidhubala  Vincent 
 

==Soundtrack==
The music was composed by G. Devarajan. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Anuraagathin   || P Madhuri || P Venu || 
|- 
| 2 || Anuraagathin   || K. J. Yesudas || P Venu || 
|- 
| 3 || Jaatharoopini || Chorus, Sreekanth || Sreekumaran Thampi || 
|- 
| 4 || Kaalam Poojicha || Sreekumaran Thampi || Sreekumaran Thampi || 
|- 
| 5 || Maari Poomaari || Jayachandran || Sreekumaran Thampi || 
|- 
| 6 || Oh My Boy Friend || Jayachandran, P Madhuri, Padmanabhan || Sreekumaran Thampi || 
|}

==References==
 

==External links==
*  

 
 
 


 