Lutaru Lalna
{{Infobox film
| name           = Lutaru Lalna
| image          = Lutaru Lalna 1938.jpg
| image size     = 
| border         = 
| alt            = 
| caption        =
| director       = Homi Wadia
| producer       = Wadia Movietone
| writer         = JBH Wadia
| screenplay     = Homi Wadia
| story          = JBH Wadia
| based on       = 
| narrator       = 
| starring       = Fearless Nadia John Cawas Boman Shroff Sardar Mansoor
| music          = Master Mohammed
| cinematography = 
| editing        = 
| studio         = Wadia Movietone
| distributor    = Wadia Movietone
| released       = 1938
| runtime        = 155 minutes
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}} 1938 action adventure Hindi film directed by Homi Wadia and produced by Wadia Movietone.    The music was composed by Master Mohammed and Baldev Naik.  The film starred Fearless Nadia, Boman Shroff, Sardar Mansoor, Sayani Atish and Sarita Devi.  

==Plot==
Minister Durjan Singh (Sayani Atish) takes over the management of Ramnagar after the king’s death. The king’s daughter Indira (Fearless Nadia) has been studying abroad. On her return she hears from Vimala (Sarita Devi) and her father the pious Saibaba (Sardar Mansoor) that Durjan Singh and his men are oppressing the poor. Durjan Singh’s brother Randhir (Boman Shroff), the Army Commander, is kind-hearted and sympathises with them. Randhir and Indira are in love with each other. When Randhir opts to leave with Vimala and her father, Indira finds out the truth about Durjan Singh. She takes on the disguise of a dacoit wearing a mask and becomes Lutaru Lalna to help the victimised. She has the horse Punjab Ka Beta and two brothers Bhanu and Nanu who have the car Rolls Royce Ki Beti, to help her in her efforts. Soon Durjan Singh is out to catch her. Following some lengthy action scenes Durjan and his henchman Teesmar Khan are caught. Randhir eventually gets together with Indira.  

==Cast==
* Fearless Nadia as Indira
* Boman Shroff as Randhir
* Sardar Mansoor as Saibaba
* Sarita Devi as Vimala
* Atish Sayani as Durjan
* Mithu Miya
* Master Mohammed
* Master Chhotu
* Minoo The Mystic

==Music== Freedom Movement was effectively show-cased by composer Master Mohammed in the Wadia films. In Lutaru Lalna he wove in two songs with nationalistic lyrics "Jhandha Ooncha Rahe Hamaara" sung by Mohammed along with Sarita Devi and "Jug Jug Chamke Hind Ka Tara" sung by Sarita Devi. Mohammed had earlier composed patriotic songs for Veer Bharat (1934) and Jai Bharat (1936).    The music was composed by Master Mohammed and Baldev Nayak with lyrics by Pandit Gyan Chander.. 

===Songlist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer
|-
| 1
| Jagat Hai Sapna Koi Na Apna
| Master Mohammed, Sarita Devi
|-
| 2
| Jhanda Ooncha Rahe Hamaara
| Sarita Devi and chorus
|-
| 3
| Ishq Kehte hain Kise Puchhiye Diwane Se
| Ahmed Dilawar
|-
| 4
| Jug Jug Chamke Hind Ka Tara
| Sarita Devi
|-
| 5
| Khawab-e-gaflat Se Gavardhan Aaja Tu
| Sardar Mansoor
|-
| 6
| Maaro Na Bhar Pichkari
| Sarita Devi
|-
| 7
| Shyam Na aaye Tadpat Jiya Hamari
| Sarita Devi
|}



==References==
 

==External links==
* 

 

 
 
 
 
 
 
 