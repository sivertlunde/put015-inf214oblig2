Hakanlar Çarpışıyor
{{Infobox Film |
  name     = Hakanlar Çarpışıyor |
  writer         = Erdoğan Tünaş |
  starring       = Cüneyt Arkın Bahar Erdeniz Aytekin Akkaya Hüseyin Peyda |
  director       = Natuk Baytan |
  producer       = Memduh Ün |
  released   = 1977 | Turkish |
}} Khans Are Turkish historic Kyrgyz chief Chinese troops attack their tribe and kill Olcaytos father. After this event, he decides to take revenge from Chinese warlords. Later, he would find a chance to do so when Chinese form an alliance with a Sheikh from South Asia, who uses all his efforts to kill Olcayto because of the love affair between his daughter and Olcayto.

One of the main motives of the film is the "Bozkır Yasası" (Law of the Steppe). During many key scenes of the film, characters act according to this notion which praises the success of a warrior. This also gives the film a nationalistic flavour which can be considered as an "essential" point of the genre.

Writer Erdoğan Tünaş possibly got his inspiration from Karaoğlan, the popular character of Suat Yalaz. Both Olcayto and Karaoğlan are from Central Asia, they are talented warriors, they have noble black horses and they both use swords which have Grey Wolf figures on their handles. Another important detail of the film is that Cüneyt Arkıns partner in it is Aytekin Akkaya, the same actor who also stars in the Turkish cult film Dünyayı Kurtaran Adam with Arkın.

==See also==
*Cinema of Turkey

 
 
 
 
 


 
 