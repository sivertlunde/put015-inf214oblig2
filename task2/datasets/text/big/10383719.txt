Ghost Station (film)
{{Infobox film
| name           = Ghost Station
| image          = Ghost Station movie poster.jpg
| caption        = The Thai movie poster.
| director       = Yuthlert Sippapak
| producer       = 
| writer         = 
| narrator       = 
| starring       = Kerttisak Udomnak Nakorn Silachai
| music          = 
| cinematography = 
| editing        = 
| distributor    = Sahamongkol Film International
| released       = April 5, 2007
| runtime        =  Thailand
| Thai
| budget         = 
| preceded_by    = 
| followed_by    = 
}} 2007 Cinema Thai List of comedy horror films|comedy-horror film directed by Yuthlert Sippapak as a spoof of Brokeback Mountain.

==Plot== gay couple who love cowboy movies and move from the city to a rural area to have a more intimate, rustic setting for their relationship. They buy an abandoned filling station and look to settle down. Udd then finds that Yai is having an affair with Tangmo, a local woman who has a lesbian lover, Jenny. Neither Yai nor Tangmo are aware of either of their sexual histories, but Udd discovers the affair and plans to have anal sex with his grandfather out of revenge. However, none of them know that he is a zombie, and lives with some scary spirits.

==Cast==
* Kerttisak Udomnak as Udd
* Nakorn Silachai as Yai
* Achita Sik-kamana as Tangmo
* Supassra Ruangwong as Jenny

==Production==
After his previous film, Krasue Valentine, director-screenwriter Yuthlert Sippapak was in the process of developing his next project when he was on Khaosan Road in Bangkok when he had a chance meeting with comedian Nakorn Silachai and the two came up with the concept of Ghost Station.

"Ghost Station is a pure comedy," Yuthlert said in production notes for the film. "Most of my previous works have been mixed-genre films with comedy as a part of the overall concept ... Ghost Station will be my first-ever film using comedy as the main focal point and horror or romance for subplots." 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 


 
 