International House (1933 film)
{{Infobox film
| name           = International House
| image          = 
| image_size     = 
| caption        = 
| director       = A. Edward Sutherland
| producer       = Emanuel Cohen
| writer         = Neil Brant
| starring       = W.C. Fields Bela Lugosi George Burns Gracie Allen Cab Calloway Rose Marie Peggy Hopkins Joyce
| music          = Ralph Rainger Howard Jackson John Leipold Al Morgan Cab Calloway
| cinematography = Ernest Haller
| editing        = 
| distributor    = Paramount Pictures
| released       =  
| runtime        = 70 minutes
| country        = United States English
| budget         =
}}
 Grand Hotel of comedy".

==Synopsis== Big Broadcast pictures that were also released by Paramount during the 1930s.  In addition to the typical lunacy by the comic players, it also provides a snapshot of some popular stage and radio acts of the era.
 International Settlement in Shanghai, the setting moves over 200 miles away to the International House Hotel, a large metropolitan hotel in Wuhu City|Wuhu, China ("Wuhu" also being a pun on the greeting "Yoo hoo").

The ostensible plot line concerns a Chinese inventor (Edmund Breese as "Dr. Wong") premiering a "radioscope", an early version of television. Unlike actual television, the mechanism does not need a camera, but its monitor can focus in and pick up on acts around the world, relaying sound and visuals as though it were a combination of a radio and telescope. Dr. Wong has brought his device to the hotel in an attempt to attract a commercial buyer to develop it.
 Kansas City but went off-course in his autogyro. Plotlines also involve four-times-divorced American celebrity Peggy Hopkins Joyce playing herself, avoiding one of her ex-husbands, the jealous Russian General Petronovich (Bela Lugosi); Tommy (Stuart Erwin) as a prospective buyer from an American electric company hoping to buy Wongs invention and finally wed his sweetheart Carol (Sari Maritza); resident physician Dr. Burns (George Burns) and his goofy aide Nurse Allen (Gracie Allen) dealing with a quarantine on the hotel; and the exasperation of the hotels fussy and frustrated manager (Franklin Pangborn). All of this is broken up with transmissions Dr. Wong receives on his radioscope, as well as a short floor show featuring Lona Andre and Sterling Holloway in the hotels rooftop garden restaurant.

The film ends with Fields being chased as he drives his American Austin automobile through several rooms of the hotel and up and down stairs before driving back into the hold of his autogyro and taking off.

==Cast==

===Actors===
*Peggy Hopkins Joyce as herself
*W. C. Fields as Prof. Henry R. Quail
*Stuart Erwin - Tommy Nash
*George Burns - Doctor Burns
*Gracie Allen - Nurse Allen {{cite book
|last1=Sicherman
|first1=Barbara
|last2=Green
|first2=Carol Hurd
|title=Notable American women: the modern period : a biographical dictionary
|url=http://books.google.com/books?id=CfGHM9KU7aEC&pg=PA14
|accessdate=9 March 2011
|year=1980
|publisher=Harvard University Press
|isbn=978-0-674-62733-8
|page=14}} 
*Sari Maritza - Carol Fortescue
*Lumsden Hare - Sir Mortimer Fortescue
*Bela Lugosi - Gen. Nicholas Petronovich
*Franklin Pangborn - Hotel Manager
*Edmund Breese - Dr. Wong, Chinese inventor

===Performers===
*Stoopnagle and Budd as F. Chase Taylor and Budd Hulick
*Rudy Vallee as himself
*Cab Calloway as himself, with his band {{cite book
|author1=Aberjhani
|author2=Sandra L. West
|title=Encyclopedia of the Harlem Renaissance
|url=http://books.google.com/books?id=XP48QWTmjyUC&pg=PA55
|accessdate=9 March 2011
|date=September 2003
|publisher=Infobase Publishing
|isbn=978-0-8160-4539-6
|page=55}} 
*Rose Marie as herself ("Baby Rose Marie")
*Lona Andre as China Teacup
*Sterling Holloway - Coffee Mug

==Production==

===Pre-Code elements=== homosexual flirting with "Dont let the posy fool you!" referring to his own boutonniere, which he tears off and tosses away. Several of the revealing costumes of the female dancers in the "She Was a China Teacup" song-and-dance routine show the bare outline of breasts, something that the Code would also virtually eliminate.
 Austin - Postmaster General." This was a potshot at Will H. Hays, the diminutive former Postmaster General who was then trying to enforce his Hollywood Production Code.

===Earthquake=== actual earthquake was centered in nearby Long Beach, California. About 120 people were killed and most of the downtown section was destroyed.

==Music==
Lyricist Leo Robin and composer Ralph Rainger wrote three songs for the film, "She Was a China Tea-cup and He Was Just a Mug," performed offscreen by an unidentified male vocalist; "Thank Heaven For You," sung onscreen by Rudy Vallee; and "My Bluebirds Singing the Blues,"
sung onscreen by Baby Rose Marie. Cab Calloway and His Orchestra perform 1932s "Reefer Man," written by Andy Razaf (lyrics) and J. Russel Robinson (music).

==References==
 

== External links ==
 
* 
 

 

 
 
 
 
 
 
 
 
 
 