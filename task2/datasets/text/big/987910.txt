La Boum
{{Infobox film
| name           = La Boum
| image          = La Boum 1982 film poster.jpg
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| director       = Claude Pinoteau
| producer       = Marcel Dassault
| screenplay     = {{Plainlist|
* Danièle Thompson
* Claude Pinoteau|
}}
| starring       = {{Plainlist|
* Sophie Marceau
* Claude Brasseur
* Brigitte Fossey
}}
| music          = Vladimir Cosma
| cinematography = Edmond Séchan   
| editing        = Marie-Josèphe Yoyotte
| studio         = Gaumont Film Company
| distributor    = Gaumont Film Company
| released       =  
| runtime        = 110 minutes
| country        = France
| language       = French
| budget         = 
| gross          = 
}}
La Boum (English title: The Party or Ready for Love) is a 1980 French comedy film directed by Claude Pinoteau and starring Sophie Marceau, appearing in her film début. Written by Danièle Thompson and Claude Pinoteau, the film is about a thirteen-year-old French girl finding her way at a new high school and coping with domestic problems. The film was an international box-office hit, earning 4,378,500 admissions in France.   The music was written by Vladimir Cosma, with Richard Sanderson singing the song "Reality (Richard Sanderson song)|Reality". A sequel movie, La Boum 2, was released in 1982.

==Plot==
Thirteen-year-old Vic (Sophie Marceau) is new at her high school. She makes friends with Pénélope (Sheila OConnor) and together they check out the boys at their school, looking for true love. Vic is frustrated by her parents, who will not allow her to attend the "boum", a big party. Her great-grandmother, Poupette, helps her out, and Vic ends up falling in love with Matthieu (Alexandre Sterling). While Vic is busy finding her true love, her parents marriage faces a crisis when her fathers ex-lover demands a last night together.   

==Cast==
* Claude Brasseur as François Beretton
* Brigitte Fossey as Françoise Beretton
* Sophie Marceau as Vic Beretton
* Denise Grey as Poupette
* Jean-Michel Dupuis as Étienne
* Sheila OConnor as Pénélope Fontanet
* Alexandra Gonin as Samantha Fontanet
* Dominique Lavanant as Vanessa
* Bernard Giraudeau as Éric Thompson
* Jacques Ardouin as Père de Raoul
* Evelyne Bellego as Éliane
* Richard Bohringer as Guibert
* Jean-Claude Bouillaud as Père Boum 2
* Micheline Bourday as Journaliste VSD
* Florence Brunold as Femme enceinte
* Jean-Pierre Castaldi as Brassac   

==Production==

===Soundtrack===
# "Reality (Richard Sanderson song)|Reality" (Cosma-Jordan) by Richard Sanderson – 4:45
# "It Was Love" (Cosma-Jordan) by The Regiment – 4:30
# "Formalities (instrumental)" (Cosma-Jordan) by Orchestra Vladimir Cosma – 3:40
# "Gotta Get a Move On" (Cosma-Jordan) by Karoline Krüger – 2:58
# "Swingin Around" (Cosma-Jordan) by The Cruisers – 2:47
# "Gotta Get a Move On" (Cosma-Jordan) by The Regiment – 4:42
# "Formalities" (Cosma-Jordan) by The Regiment – 3:41
# "Gotta Get a Move On (instrumental)" (Cosma-Jordan) by Orchestra Vladimir Cosma – 3:00
# "Murky Turkey" (Cosma-Jordan) by Richard Sanderson – 3:48
# "Go On Forever" (Cosma-Jordan) by Richard Sanderson – 3:43   

==Reception==

===Box office===
La Boum was an international box-office hit,  earning 4,378,500 admissions in France, 1,289,289 admissions in Hungary, and 664,981 admissions in West Germany.   

===Critical response===
In his review for Allmovie, Hal Erickson called the film "disarmingly diverting" and a "real audience pleaser".   

==Sequel==
 
A sequel movie, La Boum 2, was released in 1982 in which Marceau reprised her role as Vic. In the sequel, Vic is without a boyfriend, but her parents are happily back together, and her great-grandmother is considering marriage to her long-term boyfriend. When Vic meets a young boy and is attracted to him, she faces the important decision of making love for the first time, as her girlfriend has already done.

==References==
 

==External links==
*  
*  

 
 
 
 
 