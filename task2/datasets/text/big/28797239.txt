The Song of the Sun
 
{{Infobox Film
| name           = The Song of the Sun
| image          = 
| image size     = 
| caption        = 
| director       = Max Neufeld
| producer       = Angelo Besozzi Alberto Giacalone
| writer         = Ferruccio Biancini Hans Fritz Köllner
| narrator       = 
| starring       = Vittorio De Sica
| music          = 
| cinematography = Eduard Hoesch
| editing        = Fernando Tropea
| distributor    = 
| released       = 1934
| runtime        = 92 minutes
| country        = Italy Germany
| language       = Italian
| budget         = 
| preceded by    = 
| followed by    = 
}}

The Song of the Sun ( ) is a 1934 Italian-German film directed by Max Neufeld and starring Vittorio De Sica.   

==Cast==
* Giacomo Lauri Volpi - Himself
* Vittorio De Sica - Paladino, il avvocato
* Lilliane Dietz - Frida Brandt
* Eva Magni - Signora Bardelli
* Livio Pavanelli - Il Giarnalista
* Umberto Melnati - Bardelli
* Celeste Almieri - Il Segretario
* Tina Zucchi

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 