Raman Abdullah
{{Infobox film
| name = Raman Abdullah
| image =
| caption =
| director = Balu Mahendra
| writer = Balu Mahendra Karan Vignesh Vignesh Eshwari Rudra Prithviraj Prithviraj Charle Amarasigamani Delhi Ganesh
| producer =
| music = Illayaraja
| cinematography = Balu Mahendra
| editing = Balu Mahendra
| released = 1997
| runtime =
| country = India Tamil
| budget =
}}
 1997 Tamil Indian feature directed by Balu Mahendra, starring Sivakumar, Karan (actor)|Karan, Vignesh (actor)|Vignesh, Eshwari Rao, Rudra (actress)|Rudra, Prithviraj (Tamil actor)|Prithviraj, Charle, Amarasigamani and Delhi Ganesh.

==Plot==
 Karan and Vignesh as friends, one Musilm and the other a Hindu, their misadventures to save their face from a strict Haji, played by Sivakumar.

==Cast==

* Sivakumar Karan as Abdullah Vignesh as Raman
* Easwari Rao Rudra
* Prithviraj
* Charle
* Amarasigamani
* Delhi Ganesh Kitty
* Aadukalam Naren

==Production==
Director Balu Mahendra had offered a leading role to actor Vikram (actor)|Vikram, who was unable to take up the offer as a result of schedule clashes with his work in Ullasam (1997). 

==Soundtrack==
The music was composed by Maestro Ilaiyaraaja.

* En Veettu Jannal - Arunmozhi, Bhavatharini
* Puthithai Ketkum - Chithra
* Sembaruthi Pennoruthi - SPB, Chithra
* Un Madhama - Nagore E. M. Hanifa
* Muthamizhe - SPB, Chithra

==Remake==
The film is the remake of the Malayalam film Malappuram Haji Mahanaya Joji.

==References==
 

==External links==
*  

 

 
 
 
 
 
 


 