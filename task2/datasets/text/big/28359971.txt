Anton (film)
{{Infobox film
| name           = Anton
| image          = 
| image size     =
| caption        = 
| director       = Per Blom
| producer       = 
| writer         = Per Blom
| narrator       =
| starring       = Bjørn Erik Jessen 
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 
| runtime        = 113 minutes 
| country        = Norway
| language       = Norwegian
| budget         = 
| gross          =
| preceded by    =
| followed by    =
}}
 Norwegian drama film written and directed by Per Blom, starring Bjørn Erik Jessen. 15-year-old Anton Olsson (Jessen) lives in a little village in rural Norway. As his father loses his grip on reality, Antons mother, who left home when Anton was little, returns.

==External links==
*  
*   at Filmweb.no (Norwegian)

 
 
 
 