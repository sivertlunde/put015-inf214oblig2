Dos pintores pintorescos
{{Infobox film
| name           = Dos pintores pintorescos
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = René Cardona Jr.|René Cardona, Jr.
| producer       = Miguel Zacarías
| writer         = 
| screenplay     = Roberto Gómez Bolaños
| story          = Alfredo Zacarías
| based on       = 
| narrator       = 
| starring       = Marco Antonio Campos Gaspar Henaine Regina Torné Gilda Mirós Miguel Córcega Emilia Stuart Eduardo Alcaráz
| music          = Raúl Lavista
| cinematography = Rosalío Solano
| editing        = Gloria Schoemann
| studio         = Producciones Zacarías
| distributor    = 
| released       =  
| runtime        = 87 minutes
| country        = Mexico
| language       = Spanish
| budget         = 
| gross          = 
}} Eastmancolor with cinematography by Rosalío Solano.

==Plot==
Viruta and Capulina are two painters who work painting skyscrapers. One day, while lunching on a scaffolding, Capulina accidentally spills paint onto a policeman down under and he decides to escape by walking off the scaffolding. He falls, but he rapidly holds on to Viruta as he tries to stand against a window. On the other side of that window, a man named Lorenzo stabs a certain woman meanwhile his lover Carmina is watching. As Capulina stands next to the window, Lorenzo and Carmina see him as he tells them that he has "never seen death from up close". Lorenzo, believing that Capulina has witnessed the crime, searches for his gun meanwhile Capulina achieves safety when Viruta hauls the scaffolding downward as he picks him up. As both Viruta and Capulina get out of Lorenzos reach, they encounter the policeman to whom they spilled paint who fines them two-hundred pesos as punishment. 

After another incident with a painting, Capulina decides to become an artist instead of a painter as he confirms his occupations danger. Viruta and Capulina also visit a nearby artists club where they meet two beautiful art students, Diana and Julia. Both Diana and Julia study art at the studio of Dianas father, an Italian art professor. Viruta and Capulina manage to negotiate a deal with the professor in order to receive art classes. As a result of the negotiation, Viruta and Capulina paint the walls of various rooms within the studio.

==Cast==
*Marco Antonio Campos as Viruta
*Gaspar Henaine as Capulina "Capuso"
*Regina Torné as Diana
*Gilda Mirós as Julia
*Miguel Córcega as Lorenzo
*Emilia Stuart as Carmina (credited as Emilia Suart)
*Eduardo Alcaráz as Professor
*Nathanael León as Policeman
*Mayte Carol as Murdered Woman
*Consuelo Monteagudo as Painting Buyer 
*Victor Eberg as Painting Vendor
*Enrique del Castillo as Paint Store Owner
*Los Hooligans as Musical Band

==Production==
Dos pintores pintorescos was filmed from December 14 through December 26, 1966 in Estudios Churubusco and on location in Mexico City.    

==Reception==
The film premiered on October 19, 1967 in the Cine Cuitláhuac during one week.  

==References==
 

==External links==
* 

 

 
 
 