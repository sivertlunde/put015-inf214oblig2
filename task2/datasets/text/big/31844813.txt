Charley's Aunt (1941 film)
{{Infobox film
| name           = Charleys Aunt
| image          = "Charleys_Aunt"_(1941).jpg
| image_size     = 
| caption        = 
| director       = Archie Mayo
| producer       = William Perlberg
| writer         = George Seaton Brandon Thomas (Original Play)
| narrator       = 
| starring       = Jack Benny Alfred Newman
| cinematography = J. Peverell Marley
| editing        = Robert Bischoff
| studio         = Twentieth Century Fox Film Corporation
| distributor    = Twentieth Century Fox Film Corporation
| released       = August 1, 1941 (USA)
| runtime        = 80 mins.
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
Charleys Aunt is a 1941 film directed by Archie Mayo. It stars Jack Benny and Kay Francis. It was the third filmed version of the stage farce of the same name by Brandon Thomas. It remained one of Bennys personal favourites among his own films.      

==Cast==
*Jack Benny as Babbs Babberly
*Kay Francis as Donna Lucia dAlvadorez  James Ellison as Jack Chesney
*Anne Baxter as Amy Spettigue
*Edmund Gwenn as Stephen Spettigue
*Laird Cregar as Sir Francis Chesney
*Reginald Owen as	Redcliff
*Arleen Whelan as	Kitty Verdun
*Richard Haydn as Charley Wyckham
*Ernest Cossart as Brasset
*Morton Lowry as Harley Stafford Will Stanton as Messenger
*Lionel Pape as Hilary Babberly
*C. Montague Shaw as Elderly Professor
*Maurice Cass as Octogenarian Professor
*Claud Allister as	Cricket Match Spectator William Austin as	Cricket Match Spectator

==Reception==
The film was the 8th most popular movie at the US box office in 1941. FILM MONEY-MAKERS SELECTED BY VARIETY:  Sergeant York Top Picture, Gary Cooper Leading Star
New York Times (1923-Current file)   31 Dec 1941: 21. 
*TV Guide wrote "Jack Benny was never better (with the possible exception of his classic To Be or Not to Be (1942 film)|To Be or Not to Be) and carries the film with a top-flight performance. This was his first role of any consequence other than his previous tailor-made parts with radio jokes flying thick and fast around his well-known persona."  
*The New York Times wrote "when the Benny physiognomy peered impishly from behind a lacy fan, the audience held its sides, and when in the final scene his wig vanished to leave his masculine coiffure stark naked, there was a roar of laughter that must have shaken the Roxy Theatre (New York City)| Roxys rococo ceiling."  

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 