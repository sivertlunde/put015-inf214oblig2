All My Loved Ones
{{Infobox film
| name           = All My Loved Ones
| image          = 
| image size     = 
| caption        = 
| director       = Matej Mináč
| producer       = 
| writer         = Matej Mináč Jirí Hubac
| narrator       = 
| starring       = Rupert Graves
| music          = 
| cinematography = Dodo Simoncic
| editing        = 
| distributor    = 
| released       = 21 October 1999
| runtime        = 91 min.
| country        = Czech Republic
| language       = Czech
| budget         = 
}} 1999 Czech Best Foreign Language Film submission at the 72nd Academy Awards, but did not manage to receive a nomination. 

==Plot==
It is the story of an upwardly-mobile Jewish-Czech family before Nazi invasion of Czechoslovakia. After initial denial about the looming danger, the family is unable to find a way out of the country upon realizing the reality of the imminent Nazi threat. An uncle in the family meets Nicholas Winton, the (real life) British humanitarian who, just before the start of the Second World War, organized the rescue of several hundred Jewish children from German-occupied Czechoslovakia and likely death in the Holocaust. The operation was later known as the Czech Kindertransport. The storyline focuses heavily on Silberstein family members, with Nicholas Winton (portrayed by Rupert Graves) appearing briefly in key scenes near the end of the film. 

==Development==
Mináč got the idea for "a film about what preceded the war from the perspective of a child" from stories recounted by his mother. For further inspiration, he visited  .   

==Cast==
* Rupert Graves as Nicholas Winton
* Josef Abrhám as Jakub Silberstein
* Jiří Bartoška as Samuel
* Libuse Safránková as Irma
* Hanna Dunowska as Eva Marie
* Krzysztof Kolberger as Leo
* Tereza Brodská as Hedvika
* Krzysztof Kowalewski as Rous
* Marián Labuda as Helmut Spitzer
* Agnieszka Wagner as Anna
* Jirí Pecha as Amavite Puel
* Grazyna Wolszczak as Angelika
* Ondrej Vetchý as Max
* Brano Holicek as David
* Lucia Culkova as Sosa

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 