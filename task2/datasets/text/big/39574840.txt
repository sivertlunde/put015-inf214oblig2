The Prince of Arcadia
{{Infobox film
| name           = The Prince of Arcadia
| image          = 
| image_size     = 
| caption        = 
| director       = Karl Hartl 
| producer       = Oskar Glück
| writer         = Walter Reisch
| narrator       = 
| starring       = Willy Forst   Liane Haid   Hedwig Bleibtreu   Albert Paulig
| music          = Robert Stolz
| editing        = Else Baum   Karl Hartl Franz Koch   Franz Planer
| studio         = Projektograph Film 
| distributor    = Süd-Film 
| released       = 18 May 1932
| runtime        = 89 minutes
| country        = Austria   Germany
| language       = German
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} German romance film directed by Karl Hartl and starring Willi Forst, Liane Haid and Hedwig Bleibtreu. It premiered on 18 May 1932. 

==Cast==
*  Willi Forst as Der Prinz von Arkadien 
* Liane Haid as Mary Mirana, eine Schauspielerin 
* Hedwig Bleibtreu as Die Ex-Fürstin Tante 
* Albert Paulig as Flügeladjutant Mölke zu Mölke 
* Ingeborg Grahn as Die Infantin 
* Edwin Jürgensen   
* Reinhold Häussermann   
* Alfred Neugebauer    Ernst Arndt Walter Brandt   
* Herbert Hübner    Gustav Müller  
* Paul Pranger

==References==
 

==Bibliography==
* Grange, William. Cultural Chronicle of the Weimar Republic. Scarecrow Press, 2008.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 