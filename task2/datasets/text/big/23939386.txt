Malibu (film)
{{Infobox film
| name = Malibu
| image = Malibutitlescreen.jpg
| image size =
| caption = Malibu title screen
| director = E.W. Swackhamer
| producer = Robert Hamner Jack Leewood Peter Thompson William Murray (novel)
| narrator = George Hamilton
| music =
| cinematography =
| editing = J. Terry Williams ABC Columbia Pictures Television
| released = January 23, 1983 (USA)
| runtime = 240 min (including commercials)
| country = United States English
| budget =
| gross =
| preceded by =
| followed by =
}}
 William Murray. Malibu beach community, and become involved with the lives of the various people living in the community.   

==Cast==
* William Atherton - Stan Harvey
* James Coburn - Tom Wharton
* Susan Dey - Linda Harvey
* Chad Everett - Art Bonnell Steve Forrest - Rich Bradley George Hamilton - Jay Pomerantz
* Jenilee Harrison - Cindy
* Ann Jillian - Gail Hessian
* Richard Mulligan - Charlie Wigham
* Anthony Newley - Wilson Mahoney
* Kim Novak - Billie Farnsworth
* Valerie Perrine - Dee Staufer
* Eva Marie Saint - Mary Wharton
* Bridget Hanley - Laura Bonnell
* Troy Donahue - Clint Redman
* Brad Maule - Lane Ponda Richard McKenzie - Hunnicutt Powell
* Rod McCary - Alex West
* Selma Archerd - Amanda Settles
* Floyd Levine - Mr. X
* Hansford Rowe - Dr. Ferraro
* Reid Smith - Tad
* Douglas Dirkson - Bascomb
* Diane Sommerfield - Leoni Steve Levitt - Goopy
* Peter Van Norden - Bumbo
* Carol Hamner - Mrs. Benedict
* Monique St. Pierre - Jane Dennison
* Tawny Kitaen - Mahoneys Girlfriend

==References==
 

==External links==
*  

 

 
 
 
 


 