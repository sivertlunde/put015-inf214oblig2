Harlan – In the Shadow of Jew Süss
{{Infobox film name        = Harlan – In the Shadow of Jew Süss image       = Poster_large_harlan.jpg caption     = Theatrical poster director    = Felix Moeller cinematography = Ludolph Weyer producer    =   distributor = Zeitgeist Films release     =   country     = Germany language    =   runtime     = 99 minutes
}} documentary film Nazi German filmmakers, Veit Harlan and his family. It focuses on the "wildly varying attitudes of Harlans children and grandchildren",  and how they struggle even today with the legacy of their ancestors work.
 Jew Süss, a film that The New York Times has called "perhaps the most notorious antisemitism|anti-Semitic movie ever made".  Veit Harlan was the only artist from the Nazi era to be charged with war crimes.

The film uses "never-before-seen archival footage, unearthed film excerpts, rare home movies and new interviews".   

It is distributed in the U.S. by Zeitgeist Films in 2010. 

==Crew==
*Written and directed by Felix Moeller
*Cinematography – Ludolph Meyer
*Sound – Martin Noweck
*Film editing – Anette Fleming
*Original music – Marco Hertenstein
*Narrator – August Zirner
*Producers – Amelie Larscha, Felix Moeller

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 

 
 