Eaux profondes
 
{{Infobox film
| name           = Eaux profondes
| image          = Eaux-profondes.jpg
| caption        = Film poster
| director       = Michel Deville
| producer       = Denis Mermet
| writer         = Michel Deville Florence Delay
| based on       =   Patricia Highsmith
| starring       = Isabelle Huppert
| music          = 
| cinematography = Claude Lecomte
| editing        = Raymonde Guyot
| distributor    = 
| released       =  
| runtime        = 94 minutes
| country        = France
| language       = French
| budget         = 
}}

Eaux profondes is a 1981 French thriller film directed by Michel Deville and starring Isabelle Huppert.   

==Cast==
* Isabelle Huppert - Melanie
* Jean-Louis Trintignant - Vic Allen
* Sandrine Kljajic - Marion
* Éric Frey - Denis Miller
* Christian Benedetti - Carlo Canelli
* Bruce Myers - Cameron
* Bertrand Bonvoisin - Robert Carpentier
* Jean-Luc Moreau - Joël
* Robin Renucci - Ralph
* Philippe Clévenot - Henri Valette
* Martine Costes - La maman de Julie
* Evelyne Didi - Evelyn Cowan
* Jean-Michel Dupuis - Philip Cowan
* Bernard Freyd - Havermal
* Anne Head - La directrice
* Maurice Jacquemont - Docteur Franklin
* Sylvie Orcier - Jeanne Miller
* Pierre Vial - Le juge
* Amélie Prévost - Marie Valette

==See also==
* Isabelle Huppert filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 

 
 