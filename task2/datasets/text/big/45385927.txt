Moran of the Marines
{{Infobox film
| name           = Moran of the Marines
| image          = Moran of the Marines poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Frank R. Strayer
| producer       = Jesse L. Lasky Adolph Zukor
| screenplay     = Ray Harris Agnes Brand Leahy George Marion Jr. Sam Mintz Linton Wells
| starring       = Richard Dix Ruth Elder Roscoe Karns Brooks Benedict E. H. Calvert Duke Martin
| music          = 
| cinematography = Edward Cronjager	
| editing        = Otho Lovering	
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 70 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 comedy silent film directed by Frank R. Strayer and written by Ray Harris, Agnes Brand Leahy, George Marion Jr., Sam Mintz and Linton Wells. The film stars Richard Dix, Ruth Elder, Roscoe Karns, Brooks Benedict, E. H. Calvert and Duke Martin. The film was released on October 13, 1928, by Paramount Pictures. {{cite web|url=http://www.nytimes.com/movie/review?res=9B04E5D8173EE73ABC4D52DFB6678383639EDE|title=Movie Review -
  The Wedding March - THE SCREEN; Mr. Von Stroheims Picture. - NYTimes.com|work=nytimes.com|accessdate=13 February 2015}}  
 
==Plot==
 

== Cast ==
*Richard Dix as Michael Moran
*Ruth Elder as Vivian Marshall
*Roscoe Karns as Swatty
*Brooks Benedict as Basil Worth
*E. H. Calvert as Gen. Marshall
*Duke Martin as The Sergeant
*Tetsu Komai as Sun Yat

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 