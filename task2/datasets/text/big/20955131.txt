Munje!
{{Infobox film
| name           = Munje!
| image          =
| caption        =
| director       = Radivoje Andrić
| producer       = Zoran Cvijanović
| writer         = Srđa Anđelić
| starring       = Boris Milivojević Sergej Trifunović Nikola Đuričko Maja Mandžuka Milica Vujović Zoran Cvijanović Nebojša Glogovac
| music          = Eyesburn
| cinematography = Dušan Joksimović
| editing        = Marko Glušac
| distributor    = Pro Vision
| released       = March 21, 2001 (Serbia and Montenegro) October 3, 2001 (Poland)
| runtime        = 161 minutes
| language       = Serbian
| country        = Serbia
| budget         =
| gross          =
}} football player of Belgrades Red Star FC Dušan Savić also appears as a special guest, portraying himself.

== Plot == recording studio.

Pop and Mare call on their friend from school days to help them release the record, but all this doesnt go so smoothly. Gojko hasnt forgotten his school days and now he is surrounded by bodyguards. On their way to the club, Mare and Pop meet false Santa Claus, "cool" cop and of course - girls.

== Cast ==
* Boris Milivojević as Mare
* Sergej Trifunović as Pop
* Nikola Đuričko as Gojko
* Maja Mandžuka as Kata
* Milica Vujović as Lola
* Zoran Cvijanović as false Santa Claus
* Nebojša Glogovac as the Police Officer

== Soundtrack ==
The soundtrack include the songs composed for the movie, but also the songs of the other authors. 

# "Munje!" by Mao
# "Rizlu imas licnu kartu nemas" by Meso
# "Fool Control" by Eyesburn
# "Superstar" by Darko Džambasov & Frtalj bukvara
# "Beg sa žura" by Darko Obradović and Branislav Kovačević
# "Gojko je moj dečko" by Darko Obradović and Branislav Kovačević
# "Congrats" by Dominator 
# "Muzika za ljubljenje" by Darko Obradović and Branislav Kovačević
# "Izlazim" by Kanda, Kodža i Nebojša
# "Nigde" by Urgh!
# "Hoću da džonjam" by Kolibriks
# "A ko matori" by Sova
# "Devojka iz drugog sveta" by Neočekivana Sila Koja Se Iznenada Pojavljuje i Rešava Stvar
# "Noon Chaka Superfly" by Noon Chaka Superfly
# "Piano Roll" by Noon Chaka Superfly
# "Zapremina tela" by Darkwood Dub
# "Novo svetsko čudo" by MC Flex (song of Beogradski sindikat)
# "Right Direction" by Kanda, Kodža i Nebojša
# "4 Hero Live in Belgrade" by 4 Hero
# "Prvi pogled tvoj" by E-Play
# "Manitua mi II" by Kanda, Kodža i Nebojša
# "Apokalipso" by Darko Rundek
# "Chica" by Sport i Reinkarnacija
# "Disaster" by Neočekivana Sila Koja Se Iznenada Pojavljuje i Rešava Stvar
# "Zašto ja" by Dr Iggy
# "Battle Hymn of the Republic" by Fife & Drum Band
# "Druga" by E-Play
# "Crossover" by 4 Tune

== References ==
 

== External links ==
*  
*  

 
 
 
 
 