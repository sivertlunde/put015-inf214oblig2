Gimme Shelter (2013 film)
{{Infobox film
| name = Gimme Shelter
| image = Gimme Shelter.jpg
| border = yes
| caption = Theatrical release poster
| director = Ron Krauss
| producer = Ron Krauss Dama Claire Jeff Rice Scott Steindorff
| writer = Ron Krauss
| starring = Vanessa Hudgens James Earl Jones Rosario Dawson Brendan Fraser
| music = Ólafur Arnalds
| cinematography = Eric Steven Kirkland
| editing = Marie-Hélène Dozo Mark Sult
| studio = Roadside Attractions Day 28 Films
| distributor = Roadside Attractions
| released =  
| runtime = 101 minutes
| country = United States
| language = English
| gross =  $1,359,910 
}}
Gimme Shelter is a 2013 American independent drama film written and directed by Ronald Krause based on a true story about a pregnant teenager, Apple (Vanessa Hudgens), who runs away from her abusive, drug-addicted mother (Rosario Dawson), and traces her father (Brendan Fraser). Apples difficult and abusive upbringing, unfortunate situation and her father and his wifes struggle to integrate her into their family result in conflicts. After crashing the car of a pimp who forced her into his vehicle, Apple awakes in hospital where she encounters a chaplain (James Earl Jones) who connects her with a shelter for young mothers.  

==Plot==
Agnes "Apple" Bailey (Vanessa Hudgens) has never had an easy life. Shes been in and out of foster care for years, and her mother June (Rosario Dawson) is an abusive addict who only wants her for the welfare money she provides. She decides to run away and go in search of her absent father, Tom Fitzpatrick (Brendan Fraser), whom she discovers is now a wealthy Wall Street broker with a family. He agrees to take her in, but shes quickly forced out again when he and his wife learn shes pregnant and dont agree with her decision to keep the baby.

When a pimp forces her into his vehicle to discuss "business", Apple jumps into the drivers seat, speeds away and crashes the car. Apple awakes in hospital where the chaplain Father McCarthy (James Earl Jones) is waiting to speak with her. After gaining her trust Father McCarthy arranges for her to stay in a shelter for young mothers run by a spiritual, formerly homeless woman named Kathy (Ann Dowd). June is informed that Apple will be staying on there and goes to the shelter in an attempt to take her home, but is forced to leave after she gets violent.

Apple bonds with the other girls at the shelter, beginning to open up and trust them. She gives birth to a baby girl and names her Hope. Tom comes to visit and the two reconcile. He offers to let Apple and Hope come live with him, but as theyre leaving Apple realizes shes already made a home for herself at the shelter and decides to stay.

==Cast==
*Vanessa Hudgens as Agnes "Apple" Bailey
*Rosario Dawson as June Bailey
*Brendan Fraser as Tom Fitzpatrick
*James Earl Jones as Father McCarthy
*Candace Smith as Marie Abeanni
*Shiela Tapia as Officer Sue
*Joseph Ferrante as Officer Cole
*Emily Meade as Cassandra
*Stephanie Szostak as Joanna Fitzpatrick
*Allen Holloway as Guest Motel
*Natalie Guerrero as Tina
*Laneya Wiles as Jasmine
*Rachel Mattila as Nicky Lotito
*Ann Dowd as Kathy
*Gena Bardwell as Afra
*Tashiana R. Washington as Destiny / Princess
*Karina Bonnefill as Maternity Nurse
*Susan Jayne Brown as Maternity Doctor
*Joe Mancini as Construction Foreman
*Jade Jackson as Tawana

==Production==
The film was filmed in New York, and New Jersey. Filming started June 9, 2011. 
 
To prepare for this film, director Ron Krauss lived for over a year in the Several Sources Shelter founded by Kathy DiFlore, and grew to know DiFiore, the shelter, and the two girls Apple Bailey is based on. The film was shot in the home of Kathy DiFiore and the original Several Sources Shelter.  
 
Vanessa Hudgens, in explaining why the story of Apple appealed to her, said, "... it’s very relevant to what’s going on around us and I think that it’s easy to block it out because it’s uncomfortable. It’s something that we don’t want to discuss, but the fact is it’s happening all around us. Young people are becoming homeless and don’t have anywhere to go. Young mothers who have no support and no love and no place to call their own… just so much. I mean, abuse and homelessness, it’s all happening around us way more than we are allowing ourselves to see." 

Discussing why her role as Apples drug-addicted mother June had special meaning for her, Rosario Dawson said, "My mom had me when she was 17. I grew up in a squat in the Lower East Side, and the dropout rate and teen pregnancy rate in my hood was very high. Luckily, my mom had some support and was a good role model. She told me I could be anything I wanted to be. But not everybody gets a happy ending. Some people make some bad choices, and they never, ever recover from them.”  

Brendan Fraser and James Earl Jones donated their salaries to the real-life shelters (Several Sources Shelters) portrayed in the film.  

==Reception==
Gimme Shelter received generally negative reviews from film critics. Film review aggregator Rotten Tomatoes gives a film critic score 22% "rotten"  based on 66 reviews, with an average score of 4.6/10.  The film received an audience score of 66% based on 7,489 reviews with an average score of 3.7/5.

On www.rogerebert.com, critic Sheila OMalley rated the film 2 1/2 stars out of 3, saying the "scenes between Dawson and Hudgens vibrate with pain and ugliness. The script is often obvious, with all feelings laid out too cleanly, but both actresses still manage to create a jagged relationship based on their characters codependence and shared traumas. One of the films strengths is its portrayal of the system and what it does to abused children, and the layers of bureaucracy that make it hard to bring about meaningful change in peoples lives."  

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 


 
 