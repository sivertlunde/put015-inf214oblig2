Repas de bébé
{{Infobox film
| name           = Le Repas de Bébé
| image          = Repas de bébé.jpg
| image_size     = 
| caption        =  
| director       = Louis Lumière
| producer       = Louis Lumière
| writer         = 
| narrator       = 
| starring       = Andrée Lumière
| music          = 
| cinematography = Louis Lumière
| editing        = 
| distributor    = 
| released       = February 20, 1896 (UK)
| runtime        = 
| country        = France Silent
| budget         = 
| gross          =
| preceded_by    = 
| followed_by    = 
}}
 1895 France|French short black-and-white silent documentary film directed and produced by Louis Lumière and starring Andrée Lumière.

The film formed part of the first commercial presentation of the Lumière Cinématographe on December 28, 1895 at the Salon Indien, Grand Café, 14 Boulevard des Capucines, Paris. 

==Production==
As with all early Lumière movies, this film was made in a  , an all-in-one camera, which also serves as a film projector and developer. 

The baby featured, Andrée Lumière, died in Lyon aged 24, as a result of the 1918 flu pandemic.

==Plot==
The film consists of one shot of Auguste Lumière, his wife and baby daughter having breakfast in the countryside.

==Cast==
* Andrée Lumière as Herself, Bébé 
* Auguste Lumière as Himself, Auguste Lumière 
* Mrs. Auguste Lumière as Herself, Marguerite Lumière

==Current status==
Given its age, this short film is available to freely download from the Internet. It has also featured in a number of film collections including Landmarks of Early Film volume 1 and The Movies Begin - A Treasury of Early Cinema, 1894-1913. It also forms part of Visions of Light a 1992 documentary film and the The Lumière Brothers First Films, a compilation produced in 1996. 

==References==
 

==External links==
*   (requires QuickTime)
*  
*   on YouTube

 

 
 
 
 
 
 
 