After Sex (1997 film)
{{Infobox film
| name           = After Sex
| image size     =
| image	=	After Sex FilmPoster.jpeg
| caption        =
| director       = Brigitte Roüan 
| producer       = Humbert Balsan
| writer         = Santiago Amigorena Philippe Le Guay Jean-Louis Richard Brigitte Roüan Guy Zilberstein
| narrator       =
| starring       = Brigitte Roüan  Patrick Chesnais
| music          =
| cinematography = Pierre Dupouey
| editing        = Laurent Roüan
| distributor    =
| released       = 29 August 1997
| runtime        = 97 minutes
| country        = France
| language       = French
| budget         =
| preceded by    =
| followed by    =
}} French film director Brigitte Roüan.

Roüan stars in the film as Diane Clovier, a married mother of two who has an affair with the friend of a young writer she is mentoring. Meanwhile, her husband, a lawyer, is defending a woman charged with murdering her unfaithful spouse.

The film was well received in its native France, but raised some eyebrows for its sympathetic treatment of a womans infidelity. The film also received positive notice in the United States, where it was released in 1998. It was screened in the Un Certain Regard section at the 1997 Cannes Film Festival.   

The Latinate provenance of the title is explained Post-coital_tristesse|here.

==Cast==
* Brigitte Roüan - Diane Clovier
* Patrick Chesnais - Philippe Clovier
* Boris Terral - Emilio
* Nils Tavernier - François Narou
* Jean-Louis Richard - Weyoman-Lebeau
* Françoise Arnoul - Madame LePluche
* Emmanuelle Bach - Caroline
* Carmen Chaplin - Copine Narou
* Gaëlle Le Fur - Isabelle
* Elodie Pong - Designer
* Roberto Plate - Miguel
* Olivier Lechat - Victor
* Félix Dedet-Roüan - Basile
* Jean Delavalade - Dedé
* Jean-Claude Chapuis - Musical Classes Player

==References==
 

==Sources==
* Riding, Alan. "When the Tables Are Turned in Adulterys Secret Rooms", New York Times, March 8, 1998

==External links==
*  

 
 
 
 
 
 


 