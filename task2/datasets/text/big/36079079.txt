A Glass of Beer
{{Infobox film
| name           = A Glass of Beer
| image          = 
| image_size     = 
| caption        = 
| director       = Félix Máriássy
| producer       = 
| writer         = Judit Máriássy
| narrator       = 
| starring       = Éva Ruttkai   Tibor Bitskey   Elma Bulla   Mária Sulyok
| music          = Imre Vincze 
| editing        = Sándor Boronkay   
| cinematography = István Eiben 
| studio         = Magyar Filmgyártó Állami Vállalat
| distributor    = 
| released       = 25 December 1955
| runtime        = 87 minutes
| country        = Hungary
| language       = Hungarian
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} Hungarian comedy film directed by Félix Máriássy and starring Éva Ruttkai, Tibor Bitskey and Elma Bulla. It is also known as A Half Pint of Beer.

==Main cast==
* Éva Ruttkai - Cséri Juli 
* Tibor Bitskey - Kincse Marci 
* Elma Bulla - Csériné 
* Mária Sulyok - Kincséné 
* János Görbe - Kincse 
* Kálmán Koletár - Kincse Öcsi 
* Éva Schubert - Gizus 
* Elemér Tarsoly - Juhász 
* Katalin Berek - Emmi 
* Imre Pongrácz - Laci 
* Sándor Peti - Jocó bácsi 
* József Kautzky - Bordás 
* József Horváth (actor)|József Horváth - Lala 
* József Petrik - Tatár
* Gyula Horváth - Dagadt 
* Lajos Öze - Seregély

==Bibliography==
* Brown, Karl William. Regulating Bodies: Everyday Crime and Popular Resistance in Communist. ProQuest, 2007.
* Burns, Bryan. World Cinema: Hungary. Fairleigh Dickinson University Press, 1996.

==External links==
* 

 
 
 
 
 
 

 