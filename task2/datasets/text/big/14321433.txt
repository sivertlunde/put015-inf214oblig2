Best Worst Movie
{{Infobox film
| name           = Best Worst Movie
| image          = Best-worst-movie.jpg
| alt            =  
| caption        = Theatrical release cover Michael Stephenson
| producer       = Lindsay Stephenson Brad Klopman Jim Klopman
| writer         = 
| starring       = Michael Stephenson George Hardy Jason F. Wright Darren Ewing Jason Steadman Claudio Fragasso
| music          = 
| cinematography = 
| editing        = 
| studio         = Magicstone Productions
| distributor    = Magicstone Productions
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Michael Stephenson, Epix Drive-In.

== Reception ==
 

Roger Ebert awarded Best Worst Movie three out of a possible four stars.  The film also currently holds a 95% approval rating, based on 56 reviews, on the website Rotten Tomatoes.  The AV Club gave the film a "B" rating. 

== References ==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 


 