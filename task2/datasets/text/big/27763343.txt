Kona Coast (film)
{{Infobox film
| name           = Kona Coast
| image          =
| image_size     =
| caption        =
| director       = Lamont Johnson
| writer         =
| narrator       =
| starring       = Richard Boone
| music          =
| cinematography = Joseph LaShelle
| editing        =
| studio         =
| distributor    =
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Kona Coast is a 1968 film directed by Lamont Johnson. It stars Richard Boone and Vera Miles. 

==Plot==
Sam Moran (Richard Boone) is a Honolulu charter-boat captain who leads fishing expeditions in the tropical paradise. When his daughter is found murdered at the party of a wealthy young playboy, he seeks the truth about the murder. Convinced the playboy is guilty, he enlists the help of his friend Kittibelle (Joan Blondell), who runs an alcohol abuse treatment center where Sams former love Melissa (Vera Miles) is a recovering alcoholic. 

Sam runs into a wall of silence obviously built by hush money and islanders fearful of reprisals from the rich and powerful family. The determined dad fights to uncover the information that will land the murderer in jail as he avenges the death of his daughter. 

==Cast==
* Richard Boone as Capt. Sam Morgan
* Vera Miles as Melissa Hyde
* Joan Blondell as Kitibelle Lightfoot
* Steve Ihnat as Kryder
* Chips Rafferty as Charlie Lightfoot

==References==
 

==External links==
*  

 

 
 
 