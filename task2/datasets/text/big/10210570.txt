UnCivil Liberties
 
{{Infobox film
| name           = UnCivil Liberties
| image          = Uncivil Liberties dvd cover.jpg
| caption        = DVD cover
| director       = Thomas Mercer
| producer       = Tony Grocki   Thomas Mercer
| writer         = Thomas Mercer Glenn Allen
| music          = Bryan Cady   Chad Lenig
| cinematography = Justin Maine
| editing        = Tony Grocki
| distributor    = 
| released       =  
| runtime        = 110 min
| country        = USA
| language       = English
| budget         = $570,000
| gross          = 
}} 2006 film Glenn Allen and Tony Grocki.

==Plot== Big Brother-type government that uses technology to spy on its citizens. A militia assassin, Mike Wilson (Glenn Allen), is hired to kill a government agent, Cynthia Porter (Penny Perkins), who is reluctantly helping develop the technology. Wilson, however, cant bring himself to assassinate Porter, and soon his partner, Sam Norton (Tony Grocki), is hired to kill Wilson.

After Wilson is dead and the militia headquarters are bombed, Norton decides to adopt Wilsons pacifism, and invites Porter to join him to change the future.

== External links ==
*  
*  

 
 
 
 


 