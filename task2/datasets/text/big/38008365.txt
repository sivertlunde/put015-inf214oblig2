In the Family (1971 film)
 
 
{{Infobox film
| name           = In the Family
| image          = 
| caption        = 
| director       = Paulo Porto
| producer       = Paulo Porto
| writer         = Helen Leary Noah Leary
| starring       = Iracema de Alencar
| music          = 
| cinematography = José Medeiros
| editing        = 
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = Brazil
| language       = Portuguese
| budget         = 
}}

In the Family ( ) is a 1971 Portuguese drama film directed by Paulo Porto. It was entered into the 7th Moscow International Film Festival where it won a Silver Prize.   

==Cast==
* Iracema de Alencar as Dona Lu
* Rodolfo Arena as Seu Souza
* Paulo Porto as Jorge
* Odete Lara as Neli
* Anecy Rocha as Corinha
* Procópio Ferreira as Afonsinho
* Fernanda Montenegro as Anita
* Antero de Oliveira as Roberto
* Elisa Fernandes as Suzana
* Álvaro Aguiar as Arildo

==References==
 

==External links==
*  

 
 
 
 
 
 
 