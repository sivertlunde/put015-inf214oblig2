True Lies
 
{{Infobox film
| name           = True Lies
| image          = True lies poster.jpg
| image_size     = 220px
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| director       = James Cameron
| producer       = James Cameron Stephanie Austin
| screenplay     = James Cameron
| story          = James Cameron Randall Frakes
| based on       =   Tom Arnold Bill Paxton Art Malik Tia Carrere Eliza Dushku Grant Heslov Charlton Heston
| music          = Brad Fiedel
| cinematography = Russell Carpenter
| editing        = Conrad Buff IV Mark Goldblatt Richard A. Harris
| studio         = Lightstorm Entertainment
| distributor    = 20th Century Fox   Universal Pictures  
| released       =  
| runtime        = 141 minutes  
| country        = United States
| language       = English
| budget         = $100–120 million
| gross          = $378,882,411   
}} Tom Arnold, Art Malik, Tia Carrere, Bill Paxton, Eliza Dushku, Grant Heslov and Charlton Heston. It is a loose remake of the 1991 French comedy film La Totale!.   

True Lies was the first Lightstorm Entertainment project to be distributed under Camerons multi-million dollar production deal with 20th Century Fox, as well as the first major production for the visual effects company Digital Domain, which was co-founded by Cameron.
 Academy Awards BAFTAs in the Best Visual Effect category, and also for seven Saturn Awards.

==Plot== Tom Arnold) cover company for Omega Sector) doing a lot of "corporate" travel. Harrys latest mission in Switzerland reveals the existence of a Palestinian terrorism organization known as the Crimson Jihad, led by Salim Abu Aziz (Art Malik). Harry suspects that antiques dealer Juno Skinner (Tia Carrere) has ties to Aziz. After visiting her, Harry is chased by Azizs men through a shopping mall and a large hotel, missing the birthday party that Helen and Dana have arranged for him.

The next day, Harry plans to surprise Helen at her office to take her out to lunch, but overhears her talking over the phone to a man named Simon (Bill Paxton). He uses his connections in Omega Sector to learn that Simon is a used car salesman, pretending to be a secret agent to seduce women, including Helen. Harry and other Omega agents, don masks and kidnap Helen while she is at Simons trailer while frightening Simon to keep him away from her. Harry, using a voice masking device, interrogates Helen and learns she sought adventure with Harry gone all the time. Harry arranges for Helen to participate in a staged mission, where she is to seduce an mysterious arms dealer in a hotel room (in reality, its Harry hiding in the shadows with a pre-recorded voice) and plant a tracking bug on him. However, Aziz men burst in and kidnap both of them, and take them to an island in the Florida Keys.
 tactical nuclear warheads hidden inside antique statues shipped by Juno and plans to detonate one to demonstrate his power to the United States, before ordering his men to torture the two. Harry, under a truth serum, reveals his double life to Helen, but then manages to break free and rescue Helen. They secretly watch as Aziz prepares to take one of the warheads into downtown Miami and sets another to go off on the island within the hour. Harry leaves Helen in a safe place as he starts to attack Aziz men, but Helen is captured by Aziz and Juno and taken with the convoy on the Overseas Highway. Harry is rescued by Gibson and other agents in helicopters following the tracking bug that Helen was to set, and they pursue the convoy along the Seven Mile Bridge, with two U.S. Marine Corps McDonnell Douglas AV-8B Harrier II|AV-8B Harrier jets firing and destroying part of the bridge to stop the convoy. Harry manages to rescue Helen from Junos limo before it falls over the destroyed section. The jets make it back to the mainland in time before the first bomb goes off, while Harry and Helen passionately kiss.
 downtown Miami. Harry takes one of the Harriers to reach the skyscraper quickly, leaving Helen standing on the road barefoot and worried. Faisal poses as part of a requested camera crew for Aziz to make his demands, providing enough distraction for Dana to steal the ignition key and flee the room. Aziz chases Dana onto an overhead crane while Harry arrives in the Harrier. He is able to rescue Dana while Aziz falls onto the plane, eventually becoming ensnared on the end of one of the AIM-9 Sidewinder missiles on the plane. Harry fires the missile through the now-empty skyscraper, destroying a helicopter with Aziz reinforcements and killing Aziz. Harry, Helen, and Dana are safely reunited.

A year later, the Taskers family integrity has been restored, and it is revealed that Helen now also works for Omega Sector. Harry and Helen are called to embark on a new mission together, where they encounter "Simon" again, who is a waiter serving champagne and telling a female attendee at the event that he is actually a secret agent on a mission. Using her new skills, Helen intimidates Simon into leaving, just before a tango begins with Harry and Helen taking their places to complete their mission.

==Cast==
* Arnold Schwarzenegger as Harry Tasker
* Jamie Lee Curtis as Helen Tasker Tom Arnold as Albert Gib Gibson
* Bill Paxton as Simon
* Tia Carrere as Juno Skinner
* Art Malik as Salim Abu Aziz
* Eliza Dushku as Dana Tasker
* Grant Heslov as Faisal
* Charlton Heston as Spencer Trilby
* Marshall Manesh as Jamal Khaled
* James Allen as Colonel

==Production==
Schwarzenegger stated that while filming a scene with a horse, a camera boom hit the horse and "it went crazy, spinning and rearing" near a drop of 90 feet. Schwarzenegger quickly slid off the horse and stuntman  ,   July 18. Available at    .  Kempley, R., 1994.  . The Washington Post,   July 15. Accessed July 24, 2010. –120   million to produce, True Lies was the first film with a production budget of over $100 million.  It was filmed over a seven-month schedule. 

==Release==

===Critical reception===
Upon its release in 1994, the film garnered mostly positive reviews. Based on 47 reviews collected by Rotten Tomatoes, True Lies has a 72% fresh rating and a weighted average of 6.5/10 with the consensus being "If it doesnt reach the heights of director James Camerons and star Arnold Schwarzeneggers previous collaborations, True Lies still packs enough action and humor into its sometimes absurd plot to entertain".  Website Metacritic, which assigns a weighted mean to various reviews, gave the film a 63 out of 100, indicating "generally favorable reviews". 

James Berardinelli from Reelviews gave the film 3.5 stars out of 4, saying,
 "I have yet to decide whether True Lies is a better comedy or action film. It contains heavy elements of both, and plays them equally well. Unlike such failed attempts as Hudson Hawk and Last Action Hero, however, True Lies is a big, grandiose movie that has an immense amount of fun while never taking itself too seriously... Speed (1994 film)|Speed and True Lies deliver a summer one-two punch that will leave viewers squirming with excitement and gasping for breath."  
 1994 Golden Globe for Best Actress in a Musical/Comedy. 

Despite the positive reviews, the film was criticized as sexist, cruel or even misogynistic, for its treatment of female characters, such as the hero (Schwarzenegger) using his agencys resources to stalk and frighten his wife.  Others perceived it as conveying a strong anti-Arab or anti-Muslim prejudice. 

Of the many locations that were used in the film, the Rosecliff Mansion  was used for the ballroom tango scenes in the beginning of the film and the exterior of the Swiss chalet that Harry Tasker infiltrates is the Ochre Court.  The ballroom dancing scene that closes the film, as well as the scenes in the lobby of the fictional Hotel Marquis in Washington, take place in the Crystal Ballroom of the Millennium Biltmore Hotel in downtown Los Angeles. 

===Box office===
Opening in 2,368 theaters in the United States, True Lies ranked #1 in its opening weekend, earning $25,869,770. True Lies was a box-office success, earning $146,282,411 in the United States and $232,600,000 in the rest of world, totaling $378,882,411 worldwide. 

===Accolades===
{| class="wikitable" style="width:99%;"
|-
! Award !! Category !! Recipient!! Result
|-
|rowspan="1"| Golden Globe Award Best Actress
| Jamie Lee Curtis
| 
|- BAFTA
| Best Special Visual Effects John Bruno, Thomas L. Fisher, Jacques Stroweis, Pat McClung, Jamie Dixon
| 
|-
|rowspan="7"| Saturn Award Best Actress
| Jamie Lee Curtis
| 
|- Best Direction
| James Cameron
| 
|-
| Best Special Effects
|
| 
|- Best Actor
| Arnold Schwarzenegger
| 
|- Best Supporting Actress
| Tia Carrere
| 
|- Best Supporting Actor
| Bill Paxton
| 
|- Best Action/Adventure/Thriller Film
| - 
| 
|-
|rowspan="1"| Academy Award (The Oscars) Best Visual Effects
| John Bruno, Thomas L. Fisher, Jacques Stroweis and Patrick McClung
| 
|-
|rowspan="5"| MTV Movie Awards Best Actress
| Jamie Lee Curtis
| 
|- Best Comedic Performance Tom Arnold
| 
|- Best Kiss
| Arnold Schwarzenegger and Jamie Lee Curtis
| 
|- Best Dance Sequence
| Arnold Schwarzenegger and Tia Carrere
| 
|- Best Action Sequence
| Bridge Explosion/Limo Rescue
| 
|-
|rowspan="1"| Eddie Award
| Best Editing
| Conrad Buff IV, Mark Goldblatt, Richard A. Harris
| 
|- Japanese Academy Awards Outstanding Foreign Language Film
| -
| 
|-
|rowspan="1"| Screen Actors Guild Award Best Actress in a Supporting Role
| Jamie Lee Curtis
| 
|}

==Possible sequel and television series adaptation==
 
In September 2010, multiple websites reported Cameron developing True Lies as a possible television series with Dark Angel producer René Echevarria acting as showrunner and producer.    In 2012, online news reports quoted Eliza Dushku as saying there would be a sequel reuniting the original cast with the writer/director James Cameron.  Cameron originally planned to make a sequel sometime in 2002, but he put his plans on hold once the September 11, 2001 attacks occurred in New York City, saying terrorism was no longer something to be taken lightly.  In an interview, James Cameron stated there are no plans for a True Lies sequel, but he and Schwarzenegger had spoken about possibly working on a new project together once Schwarzenegger leaves office.   
 2003 California recall election.

==Soundtrack==
{{Infobox album
| Name = True Lies
| Type = Film score
| Artist = Brad Fiedel and various artists
| Cover = True Lies (album).jpg
| Released = July 19, 1994
| Length =
| Label =  Lightstorm/Epic Soundtrax
| Reviews =
}}
{{tracklist
| headline = Track list
| collapsed = no
| title1 = Sunshine of Your Love
| music1 = Living Colour
| title2 = Darkness, Darkness
| music2 = Screaming Trees Alone in the Dark
| music3 = John Hiatt
| title4 = Entity
| music4 = Mother Tongue
| title5 = Sunshine of Your Love (The Adrian Sherwood & Skip McDonald Remix)
| music5 = Living Colour
| title6 = Main Title/Harry Makes His Entrance
| title7 = Escape from the Chateau
| title8 = Harrys Sweet Home
| title9 = Harry Rides Again
| title10 = Spying on Helen
| title11 = Junos Place
| title12 = Caught in the Act
| title13 = Shadow Lover
| title14 = Island Suite
| title15 = Causeway/Helicopter Rescue
| title16 = Nuclear Kiss
| title17 = Harry Saves the Day
}}

Songs appearing in the film not included on the soundtrack album: Sade
* More Than a Woman" – Bee Gees
* "The Blue Danube" – The Philadelphia Orchestra
* "Por una Cabeza" – Argentinean tango, performed by  

==References==
 

==External links==
 
 
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 