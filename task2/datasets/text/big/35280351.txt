Hemlock Society (film)
{{Infobox film
| name           = Hemlock Society
| image          = Poster of Hemlock Society (Film).jpg
| alt            =  
| caption        = Poster
| director       = Srijit Mukherji Shrikant Mohta Mahendra Soni
| writer         = Srijit Mukherji Jeet  Sabyasachi Chakrabarty
| music          = Anupam Roy
| lyrics         = Anupam Roy
| cinematography = Soumik Halder
| editing        = Bodhaditya Banerjee. Saikat Sengupta
| studio         =
| distributor    =
| released       =  
| runtime        =
| country        = India Bengali
| budget         = 
| gross          = 
}}
 Bengali Dark Autograph & Baishe Srabon. The cast of the movie consists of Parambrata Chatterjee, Koel Mallick and Soumitra Chatterjee.    The name is inspired by the erstwhile Hemlock Society, a society which was based in Santa Monica, US. The primary missions of this society included providing information to dying persons and supporting legislation permitting physician-assisted suicide.

==Plot==
The movie is a strong insight into psychological dilemma about the primal existence of life. It reiterates the fact that you can shock someone by letting her know that death is impending and inescapable.’  It is a dark comedy which successfully evokes an awareness of life. The comic element is used dexterously to bring out the fear of death. The protagonist Meghna (Koel Mallick),  misses her mother, hums the songs of her favorite Sidhartha Roy numbers, blames the new lady in her father’s life, shaken by the dejection by her fiancé. Yet, as an escape route from the world of pain and suffering she goes to her father doctor to get prescribed sleeping pills. Ananda Kar  (Parambrata Chatterjee|Parambrata) as the name suggests ‘make merry’ refrains her from ending her life. He comes with an unusual proposal of imparting her technical know how about committing suicide. He adds the training will save you from embarrassment if suicide becomes a vain attempt. Ananda Kar is the founder of the Hemlock Society,  that  teaches aspirants how to successfully commit suicide. The names of the professors are also significant of the tricks they will teach to execute the end. The society is a film studio. Meghna assembles with all other suicide aspirants being addressed by the over-enthusiastic professors, trainlet, jhulan, dhamani, raktim, shikha,  all specialists in different suicide methods. Meghna goes with him, leaving a goodbye message for her parents at her apartment door. Dr. Basu comes in search of his daughter, finds the note and shatters in grievance. Dr. Basu and Niharika start their search for Meghna and almost leave no stone unturned. Meanwhile at Hemlock Society, Meghna encounters with miscellaneous experiences related to humans lives and death. The three day workshop at the society makes her understand the importance of life and what difference her loss would mean to her near and dear ones. The metamorphosis of Meghna occurs when she realizes the miseries of other inmates and considers herself lucky. On the final day, Meghna confesses to Ananda that she wants to live and a vital reason which worked behind her changing of mind is that she had fallen in love with Ananda. Ananda informs her that he is suffering from Lymphocytopenia a medical condition for which he would survive only for two more years.  Meghna leaves Hemlock Society, sobbing and returns to her father. The very thought that someone near the cradle of death can ‘drink life to the lees’ in spite of complain and regret, surprises her. She learns from Ananda, that Hemlock society is an institute that refrains people from ending their life by making them aware of the importance of life.  Six months later, Meghna reconciles with Ananda while he is on a nursing home bed, after having a blood-transfusion. The film ends with a witty note where Meghna’s fiancé (Saheb Chatterjee), now a dejected soul and undoubtedly suicide aspirant,  being taken to Hemlock Society. 

==Cast==
* Koel Mallick as Meghna Sarkar
* Parambrata Chatterjee as Ananda Kar
* Rupa Ganguly as Niharika Basu (Meghnas Step Mother)
* Deepankar De as Dr. Chittaranjan Basu (Meghnas Father)
* Reeta Basu Meghnas Mother
* Silajit Majumder as Siddhartha Roy, the Singer
* Saheb Chatterjee as Shantanu
* Anindita Bose as Shreya
* Biswajit Chakraborty
* Soumitra Chatterjee as Colonel Samaresh Bagchi (Guest Appearance)
* Sabitri Chatterjee as Jhulon Gupto (Guest Appearance)
* Sabyasachi Chakrabarty as Dhomoni Ghosh (Guest Appearance)
* Srijit Mukherji Doyal Khashnobish (Guest Appearance)
* Bratya Basu as Raktim Ganguly (Guest Appearance)
* Barun Chanda as Trenlet Biswas (Guest Appearance)
* Raj Chakraborty as Setu Venkataraman (Guest Appearance)
* Akshay Kapoor as Invigilator/ Sign Language Interpreter
* Sudeshna Roy as Shikha Dhor (Guest Appearance)
* Sohag Sen as  Miss Cella Neous (Guest Appearance)
* Priyanka Sarkar as Hiya (Guest Appearance) Jeet (Special Appearance)

==Music==
The soundtrack is composed Anupam Roy.

{{Track listing
| headline        = Tracklist
| extra_column    = Singer(s)
| total_length    = 
| all_music       = 
| all_lyrics      = 
| title1          = Amar Mawte (Female)
| extra1          = Lopamudra Mitra
| length1         = 5:30
| title2          = Ekhon Anek Raat
| extra2          = Anupam Roy
| length2         = 3:46
| title3          = Ei To Ami Chai
| extra3          = Shreya Ghoshal & Anupam Roy
| length3         = 4:30
| title4          = Jawl Phoring
| extra4          = Silajit Majumder 
| length4         = 3:28
| title5          = Amar Mawte (Male)
| extra5          = Rupankar Bagchi
| length5         = 3:44
| title6          = Phiriye Dewar Gaan 
| extra6          = Rupam Islam 
| length6         = 5:52
| title7          = Hemlock Society Theme 
| extra7          = Composed by Indrajit Dasgupta
| length7         = 3:28
}}

==Reception and Recognition==
The film received generally positive reviews by critics. Parambrata Chatterjees performance was critically applauded, as was Koel Mulliks. It has received 21 awards till now. Srijit Mukherji and Koel Mallick got the Shoilojanando Mukherjee Memorial Awards for Direction and Acting respectively. Parambrata Chatterjee received the Anandalok Award for Best Actor, while Anupam Roy got the Anandalok Award for the Best Song (Ekhon Onek Raat). At the Mirchi Music Awards 2013, it got the Best Album and Best Song (Ekhon Onek Raat) in the Listeners Choice category as well as the Best Male Playback for Rupankar Bagchi for Aamar Mawte. At the 13th Telecine Awards, it got Parambrata Chatterjee the Special Jury Award for acting, and Anupam Roy, the Best Lyricist Award. It also got the Best Movie Poster at the Srijon Shawmman 2013. At the Bengal Youth Awards 2013, Srijit Mukherji and Anupam Roy got the Best Director and the Best Music Director Awards respectively. Anupam Roy also got the Best Male Playback for Ekhon Onek Raat in the ETV Shongeet Shawmman 2013. It also got the prestigious BFJA award, the oldest film award in India, for Best Actress, Best Actor (Jurys choice), Best Male Playback, Best Female Playback (Female) and Best Art Direction. It also fetched the Zee Banglar Gourab Samman for Anupam Roy for Best Lyricist and Male Playback for Ekhon Onek Raat and Best Actress for Koel Mullick.      

==Remake==
Umesh Ghadge, who has previously worked as an assistant director in films like Dhoom and Once Upon a Time In Mumbaai, will be remaking the film as Anandvan in Marathi. Urmila Matondkar has been approached for Koels role. Atul Kulkarni, who was offered Parambratas role, however, has turned down the project despite taking an immense liking to the script. 

==References==
 

 
 
 
 