The Martyrdom of Philip Strong
{{Infobox film
| name           = The Martyrdom of Philip Strong
| image          = The Martyrdom of Philip Strong.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Richard Ridgely 
| producer       = 
| screenplay     = Everett McNeil Francis Neilson Charles Sheldon
| starring       = Robert Conness Mabel Trunnelle Janet Dawley Bigelow Cooper Helen Strickland Frank A. Lyons
| music          = 
| cinematography = George W. Lane 	
| editing        = 
| studio         = Edison Company
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 drama silent film directed by Richard Ridgely and written by Everett McNeil, Francis Neilson and Charles Sheldon. The film stars Robert Conness, Mabel Trunnelle, Janet Dawley, Bigelow Cooper, Helen Strickland and Frank A. Lyons. The film was released on November 3, 1916, by Paramount Pictures.  

==Plot==
 

== Cast ==
*Robert Conness as Philip Strong
*Mabel Trunnelle as Sarah Strong
*Janet Dawley as Irma Strong
*Bigelow Cooper as Brother Man
*Helen Strickland as Mrs. Alden
*Frank A. Lyons as William Winter 
*William Wadsworth as Dunn
*Herbert Prior as Hikes
*Olive Wright as May Hikes
*Edith Wright as Loreen
*Brad Sutton as Hooks

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 

 