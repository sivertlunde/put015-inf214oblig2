The Miller and the Sweep
{{Infobox film
| name           = The Miller and the Sweep
| image          = TheMillerandtheSweep.jpg
| image_size     = 
| caption        = Screenshot from the film George Albert Smith
| producer       = George Albert Smith
| writer         =
| narrator       =
| starring       =
| music          =
| cinematography = George Albert Smith
| editing        =
| studio         = G.A. Smith
| distributor    = Warwick Trading Company
| released       =  
| runtime        = 49 secs
| country        = United Kingdom Silent
| budget         =
}}
 1898 UK|British short black-and-white silent comedy George Albert Smith, featuring a miller carrying a bag of flour fighting with a chimney sweep carrying a bag of soot in front of a windmill, before a crowd comes and chases them away. The film, according to Michael Brooke of BFI Screenonline, "was one of the first films made by G.A. Smith, shortly after he first acquired a camera," and is also, "one of the earliest films to show a clear awareness of its visual impact when projected."

==Plot==
A miller is carrying a sack of flour from his windmill, when he accidentally bumps into a chimney sweep who is carrying a sack of soot. The two start fighting, during the course of which the miller is covered with soot and the chimney sweep is covered with flour. The chimney sweep chases the miller off screen, and a small crowd of adults and children appears unexpectedly from the right of the screen to chase after them both. The film finishes when the last of the crowd exits the shot.

==Production and release== single shot on 24 July 1897 in front of Race Hill Mill on the South Downs, north of Brighton Racecourse, and then re-filmed on 24 September - it is probably this latter version that has survived.     These two versions are mentioned in Smiths cash book. There is no mention of the first print from July being sold.    It is likely that the film was rehearsed before filming, as this was Smiths usual way of working with fiction films.  The names of the actors are unknown, as with many films from the period, and it is not clear whether they are professional actors, music hall performers, comedians, or simply amateurs.   

The September version of the film was available for commercial sale within a fortnight of its filming, and two prints were sold in the first week of October. 

==Analysis== James Williamsons Washing the Sweep (1899) or Robert W. Pauls Whitewash and Miller (1898).    Often, "black" and "white" would be in competition with one another for the affection of a woman, although Smith avoided this romantic element in his film.  For example, a popular comic sketch in England in the 1880s was "The Sweep and the Miller", performed by Professor Daltrey and Corporal Higgins. In this, the two suitors attempt to woo a housemaid, prompting a fight between them.  More generally, comedic violence between working-class stereotypes was also a feature of Victorian music hall productions, which would become important in early film comedy. 

The film also provides an early example of an on-screen chase, of the type that would become particularly popular from 1903 onwards, in a trend prompted by British productions such as A Daring Daylight Burglary and Desperate Poaching Affray.       Film historian Stephen Bottomore has stated that with this work, "Smith helped invent the chase film", by offering a "model for the chases in numerous British and other films in the years that followed".   

==Current status==
The film has survived in its entirety, and the   in 2002.

==References==
 

==External links==
* 

 
 
 
 
 
 