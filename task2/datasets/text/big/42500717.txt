Who Cares (1925 film)
{{infobox film
| title          = Who Cares
| image          =
| imagesize      =
| caption        =
| director       = David Kirkland
| producer       = Harry Cohn
| writer         =   (scenario) Walter Anthony (intertitles)
| starring       = Dorothy Devore
| music          =
| cinematography = Allen Thompson
| editing        =
| distributor    = Columbia Pictures
| released       = February 1, 1925
| runtime        = 59 minutes; 6 reels
| country        = United States Silent (English intertitles)
}} Who Cares?

Real life husband and wife, actors Vera and Ralph Lewis, play grandparents.

==Cast==
*Dorothy Devore - Joan
*William Haines - Martin
*Lloyd Whitlock - Gilbert Palgrave
*Beverly Bayne - Mrs. Hosack
*Wanda Hawley - Irene
*Vola Vale - Tootles Charles Murray - Greaves (as Charlie Murry)
*Vera Lewis - Grandmother Ludlow Ralph Lewis - Grandfather Ludlow William Austin - Dr. Harry Oldershaw
*Carrie Clark Ward - Housekeeper

==References==
 

==External links==
*  
* 
* 

 
 
 
 
 


 