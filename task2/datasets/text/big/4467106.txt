Project A Part II
 
 
{{Infobox film
| name           = Project A Part II
| image          = Project-A-II-Poster.jpg
| caption        = Hong Kong film poster traditional    = A計劃續集
| simplified     = A计划续集
| pinyin         = A Jìhuà Xùjí
| jyutping       = A Gai3 Waak6 Zuk6 Zaap6}}
| director       = Jackie Chan
| producer       = {{plainlist|
* Raymond Chow
* Leonard Ho David Lam
* Edward Tang
}}
| writer         = {{plainlist|
* Jackie Chan
* Edward Tang
}}
| starring       = {{plainlist|
* Jackie Chan
* Maggie Cheung
* Rosamund Kwan
* Bill Tung
}}
| music          = Michael Lai
| cinematography = Cheung Yiu-tsou
| editing        = Peter Cheung Yiu-chung
| distributor    = {{plainlist| Golden Harvest Media Asia
}}
| released       =  
| runtime        = 106 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         =
}}
  Hong Kong action film written and directed by Jackie Chan, who also starred in the lead role. It is the sequel to the 1983 film Project A. Jackie Chan plays Sergeant Dragon Ma once again, Sammo Hung and Yuen Biao, stars from the original film, are absent. The film was released in the Hong Kong on 19 August 1987.

==Plot==
The sequel continues with runaway pirates, who vow that they must kill Dragon Ma to take revenge for their late captain. On recommendation of the Chief of Marine Force, Dragon Ma is transferred to be in charge of the district of Sai Wan after the Superintendent, Chun, is thought to be staging his arrests. Chun, however, has an excellent record and the "criminals" he has been engaging are shot and killed, so there is no evidence against him.

Dragon Ma and his subordinates later meet Yesan and her cousin, Carina, at a teahouse selling flowers to raise funds. He later learns that Carina is  a member of the Chinese revolutionaries headed by Dr Sun Yat-sen. Dragon identifies himself as the new superintendent of Sai Wai Police Station, after realizing that all of his policemen except one has been taking bribes. Ho, the only upright policeman around, tells them that a gangster named Tiger Ow with gambling dens and other illegal businesses is the kingpin of the town.

When his men are too cowardly to confront Tiger, Dragon is forced to confront him with only Ho and his three friends he brought with him from the Marine Police. After a big fight where the policemen are badly outnumbered, the Marine Police show up with guns and force the gangsters to surrender. After one last fight with Ow, all the gangsters are sent to prison, inspiring the police at the station to do a better job.
 Empress Dowager, who are working with Chun; they trap her in a wardrobe at Yesans house. Yesan, Li, Ma, and Ho arrive. Li hides but ends up threatened by one of the imperial agents. Ma and Ho, who are handcuffed together, enter the bathroom. While they are in the bathroom, the commissioner arrives.

As Yesan hangs up his uniform, Ma and Ho see the keys to the handcuffs. They break free and hide under the bed, losing the key to the handcuffs. They see the Imperial Agents with Carina in the wardrobe. Then Yesan and the commissioner sit down to talk. The Commissioner demonstrates how to handcuff two people by handcuffing himself to the armchair but can not break free. Chun arrives to visit Yesan, and the commissioner hides under the bed, where he sees Ma and Ho. Yesan finally gets Chun to get out of her house, when Ma, Ho, Tung and Yesan defeat the Imperial agents. Eventually the Imperial Agents are arrested, the revolutionary chief escapes, and Dragon is handcuffed by Chun so that he can be brought to the main prison. Carina flees town with the help of the revolutionaries. Chun arranges for Dragon to be killed by a prison warden.

The pirates attack both Dragon and Chun with axes, but they are eventually driven off after the police show up.  Dragon is handed over to the prison warden, tied up in a sack, and thrown into the sea. The revolutionaries save Dragon and take him back to their hideout above a medicine store, where they try to enlist him. Dragon refuses to actively help them, saying that he is just a Hong Kong cop. The head of the pirates falls sick, and the pirates enter the medicine store to ask for some herbs. Dragon intervenes and offers to pay for their medicine, causing the pirates to think much better of him.

The Imperial agents arrive and apprehend most of the revolutionaries, to gain possession of the black book.  Dragon helps Yessan and Miss Pak escape while safeguarding the book.  After a frantic run and fight scene, he defeats them with the help of the pirates.  The Police Commissioner arrives with a huge police cohort and orders the arrest of Superintendent Chun, now fully aware Chun is trying to murder Dragon. Chun tries to run, but a large bamboo-and-wood stage facade falls on Chun while he attempts to retrieve his moneybag. Dragon, on order of the Commissioner, takes charge of the police.

In the end, a music video featuring Chan singing the theme song, appears in a bubble above the credits roll.

==Cast==
* Jackie Chan – Sergeant Dragon Ma
* Maggie Cheung – Yesan
* Bill Tung – Police Commissioner
* Rosamund Kwan – Miss Pak
* Carina Lau – Carina
* Ray Lui – Mr. Man Tin-ching
* Regina Kent – Regina, Governor’s daughter Mars – Mars / Jaws
* Kenny Ho – Shi King
* Yao Lin Chen – Awesome Wolf
* Chris Li – Mr. D (Mas sidekick)
* Fan Mei-sheng – Black Bear
* Lee Hoi-san – Choy
* Tai Bo – Mr. B
* John Cheung – Bodyguard
* Chen Ti-ko – Python
* Benny Lai – Pirate No.1
* Rocky Lai – Pirate No.2
* Johnny Cheung – Pirate No.3
* Lai Sing-kong – Piarte No.4
* Frankie Poon – Pirate No.5
* Sun Wong – Sgt. Ching
* Wan Fat – Wan Sam Mun
* Len Wen-wei – Sung
* Ben Lam – Brawns Lo Wai-kwong – Brains Anthony Chan – Cop #365
* Nicky Li – Hotel Receptionist
* Kwan Hoi-san – Chi
* Lau Siu-ming – The Prince
* Isabela Wong - Winnie Chi (extra)
* Sammo Hung - Fei (Opening Credit Secuence)
* Yuen Biao - Captain Tzu (Opening Credit Secuence)
* Dick Wei - San Pau (cameo) (Opening Credit Secuence in film)

==Production notes==
* Sammo Hung and Yuen Biao did not appear in the Project A sequel because they were shooting the film Eastern Condors. Ben Lam, Kenny Ho and Chris Li appeared in both films. A Night at the Opera, where everyone was crammed into a ship cabin. The scene where the wall falls on Jackie, but he escapes injury because he is standing exactly where the window of the wall, is taken from Buster Keatons Steamboat Bill Jr.. The handcuff sequence was inspired by a segment Jackie saw on the news magazine 60 Minutes. 
* Isabella Wong is the only rare model character to appear in both Project A and Project A Part II and has not been seen in any other films.,       Later in 1988 on a cameo appearance in Police Story 2.

==Box office==
The film grossed HK $31,459,916 at the Hong Kong box office. 

==Reception==
Rotten Tomatoes, a review aggregator, reports that 67% of six surveyed critics gave the film a positive review; the average rating was 6.5/10.   David Beamish of DVDactive rated it 6/10 stars and recommended it to fans of the first fan.   J. Doyle Wallis of DVD Talk rated it 3.5/5 stars and called it "one of the better films in his resume, just not up to the likes of the original film that preceded it."   Mike Pinsky of DVD Verdict called it a "time filler" for Chan that does not live up to the previous film. 

===Awards and nominations===
*1988 Hong Kong Film Awards
**Won: Best Action Choreograghy 
**Nominated: Best Film Editing (Peter Cheung) 

==See also==
*Jackie Chan filmography
*List of Hong Kong films

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 