The Bowery Boys Meet the Monsters
{{Infobox Film
| name           = The Bowery Boys Meet the Monsters
| image          = 
| image_size     = 
| caption        = 
| director       = Edward Bernds
| producer       = Ben Schwalb
| writer         = Edward Bernds Elwood Ulman
| narrator       = 
| starring       = Leo Gorcey Huntz Hall David Gorcey Bernard Gorcey
| music          = Marlin Skiles
| cinematography = Harry Neumann William Austin Allied Artists
| distributor    = Allied Artists
| released       = June 6, 1954 (U.S. release)
| runtime        = 65 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Allied Artists and is the thirty-fourth film in the series.

==Plot==
The front window of Louies Sweet Shop is a frequent victim of the local neighborhood kids baseball games. The Bowery Boys think that a nearby vacant lot would be perfect for the kids to play ball, and keep out of trouble. Slip and Sach travel during a heavy rainstorm to visit the owners of the lot at their home on Long Island. As it turn out, the owners, all members of the same family, are completely insane. Dereck, a mad scientist, wants a brain for his gorilla. His brother Anton wants a brain for his robot, Gorog. Their sister Amelia needs fresh meat to give to her man-eating tree, while their niece Francine is a vampire. Feeling that Slip and Sach are perfect for their personal needs, the family asks the duo to spend the night. The boys soon catch on to the familys schemes, causing a frantic chase through the house. Louie, Butch, and Chuck visit the home to search for Slip and Sach, and its not long before they too get caught up in all the madness.

==Cast==

===The Bowery Boys===
*Leo Gorcey as Terrance Aloysius Slip Mahoney
*Huntz Hall as Horace Debussy Sach Jones
*David Gorcey as Chuck Anderson (Credited as David Condon)
*Bennie Bartlett as Butch Williams

===Remaining cast===
*Bernard Gorcey as Louie Dumbrowski
*Lloyd Corrigan as Anton Gravesend
*Ellen Corby as Amelia Gravesend
*John Dehner as Dr. Derek Gravesend
*Laura Mason as Francine Gravesend Paul Wexler as Grissom, the butler
*Norman Bishop as Gorog, the robot (uncredited)
*Paul Bryar as Officer Martin (uncredited)
*Steve Calvert as Cosmos, the gorilla (uncredited)
*Rudy Lee as Herbie Wilkins (uncredited)

==Home media== Warner Archives released the film on made to order DVD in the United States as part of "The Bowery Boys, Volume Two" on April 9, 2013.

==References==
 

==External links==
* 

 
{{succession box
| title=The Bowery Boys movies
| years=1946-1958
| before=Paris Playboys 1954
| after=Jungle Gents 1954}}
 

 

 
 
 
 
 
 
 


 