The ABC of Love
{{Infobox film 
| name           =  The ABC of Love
| image          = 
| caption        = 
| director       = Eduardo Coutinho Rodolfo Kuhn Helvio Soto
| producer       = Jose Luis Contereras Leon Hirszman Marcelo Simonetti Helvio Soto
| writer         = Roberto Arlt Eduardo Coutinho Carlos Del Peral César Fernández Moreno Rodolfo Kuhn Helvio Soto Francisco Urondo
| starring       = Jorge Rivera López Vera Vianna Reginaldo Faria Susana Rinaldi Federico Luppi
| music          = Tito Ledermann Óscar López Ruiz Sidney Waismann
| cinematography = Fernando Bellet Dib Lufti Juan José Stagnaro
| editing        = Nello Melli Antonio Ripoll
| distributor    = Difilm
| released       = Argentina: 7 September 1967 
| runtime        = 95 minutes
| country        = Argentina Brazil
| language       = Spanish
| budget         = 
| followed_by    = 
}} 1967 cinema Brazilian and Argentine drama film directed by Eduardo Coutinho and Rodolfo Kuhn.

The picture stars Jorge Rivera López and Bárbara Mujica.

==Plot==
The films involves a suicide pact and marriage.

==Release and acclaim==
The film premiered in June 1967 at the 17th Berlin International Film Festival and was nominated for a Golden Bear Award.    It premiered on 7 September 1967 in Argentina. Released in Brazil it was also released in Chile.

==Cast==
* Jorge Rivera López
* Vera Vianna
* Reginaldo Faria
* Susana Rinaldi
* Federico Luppi
* Jofre Soares
* Héctor Pellegrini
* Isabel Ribeiro
* Mário Petráglia
* María Luisa Robledo
* Corrado Corradi
* Marta Gam
* Cláudio MacDowell
* Gloria Pratt
* Adriana Aizemberg

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 
 


 
 