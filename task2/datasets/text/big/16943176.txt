Geng: The Adventure Begins
{{Infobox film
| name = Geng: The Adventure Begins
| image = Geng - Pengemberaan Bermula.jpg
| caption = Official poster
| director = Nizam Razak
| producer = H.J. Burhanuddin
| writer = Muhammad Anas Abdul Aziz
| starring = Nur Fathiah Diaz Amir Izwan Balqis Fadhullah Lee
| music = Mohamed Azfaren Aznam Yuri Wong
| studio = Les Copaque Production
| distributor = Grand Brilliance
| released =  
| runtime = 90 minutes
| country = Malaysia
| language = Malay RM 4 million
| gross = RM 6,314,526 
}} computer animated feature film. The film is produced by Les Copaque Production|Les Copaque and released in Malaysian cinemas beginning 12 February 2009.

Geng was launched in a ceremony held on 11 September 2007 together with short animated series Upin & Ipin that have connections with the film. Planning for the film began in late-2005 as Les Copaque commenced operations. It received financial support from ICT-related agencies such as Multimedia Development Corporation and MIMOS.

==Plot==
Bored with nothing to do for their school holidays, Badrol invites his best friend Lim to go on a camping trip at his old Kampung where he grew up, with intentions to experience some long-forgotten true Malaysian life in his Grandfather’s durian orchard. However their simple holiday trip is cut short by the discovery of a clue that leads them closer to the rumours of the mystery house deep in the forest. Together with their newly found friends, Raju the animal-whispering boy, Ros the village beauty, and her two cute twin brothers, Upin and Ipin, they work together to unravel the secrets surrounding the small Kampung. 

==Cast==
* Amir Izwan as Badrol
* Kee Yong Pin as Lim
* Nur Fathiah Diaz as Upin & Ipin
* Balqis Fadhullah Lee as Ros
* Kannan Rajan as Raju

==Production==
It took two years, RM 4.7 million    and 40 local animators to complete Geng, which director Mohd Nizam Abdul Razak compares as cheap against Western animation budgets worth "around RM15 million to RM40 million" and "about 130 to 150 animators".   
 Mimos Bhd Maya and MentalRay were rendering in only six to eight months compared to the usual over-one-year period, as well as saving costs. 
 Astro and Censorship Boards approval of their film without cuts and permission to screen by Finas. 

The music for Geng is mainly composed by Yuri Wong, who also composed music for the related animated series Upin & Ipin, along with Mohamed Azfaren Aznam, who contributed music as a second composer.

==Reception== Oscar nominees The Curious Case of Benjamin Button.

===Box office chronology===
{|class=wikitable
!Days of screening
!Cumulative sales
|- 4 days (15 February) RM 1.75 million  
|- 7 days (18 February )
|RM2.3 million 
|- 11 days (22 February)
|RM3.7 million 
|- 14 days (25 February) RM 4.09 million 
|- 18 days (1 March)
|RM4.8 million 
|- 26 days (9 March) RM 5.72 million 
|- 35 days (18 March) RM 6.011 million   
|- 39 days (22 March) RM 6.24 million 
|- 43 days (26 March) RM 6.28 million 
|- 49 days (1 April) RM 6.31 million 
|}

==Awards==
;2009
* International Film Festival for Children, Indonesia 
**Anugerah Pilihan Penonton (Peoples Choice Awards)
* 22nd Malaysian Film Festival 
**2 Jurys Choice Awards (box office, animated feature film with distinctive characters)
* MSC Malaysia Kre8tif! Industry Awards 
**Best Editor (Mohd Faiz Hanafiah)
**Best Music/Score (Yuri Wong)

==References==
 

==External links==
*  
*  
* 

 

 
 
 
 
 
 
 
 
 