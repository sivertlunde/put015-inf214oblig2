The World of Geisha
{{Infobox film
| name = The World of Geisha
| image = The World of Geisha.jpg
| image_size = 
| caption = Theatrical poster for The World of Geisha (1973)
| director = Tatsumi Kumashiro Infobox data from   
| producer = 
| writer = Tatsumi Kumashiro (screenplay) Kafū Nagai (original novel)
| narrator = 
| starring = Junko Miyashita Naomi Oka
| music = 
| cinematography = Shinsaku Himeda
| editing = Akira Suzuki
| distributor = Nikkatsu
| released =  
| runtime = 72 min.
| country = Japan
| language = Japanese
| budget = 
| gross = 
| preceded_by = 
| followed_by = 
}}
 1973 Japanese film in Nikkatsus Pink film#Second wave (The Nikkatsu Roman Porno era 1971–1982)|Roman porno series, directed by Tatsumi Kumashiro and starring Junko Miyashita. The mainstream film journal Kinema Junpo included it in their choices for best ten films of the year of 1973.   

==Synopsis==
In 1918, against the background of political events following the Russo-Japanese War, a geisha serves a new customer. Though he is about to be married, and it is against the rules, she falls in love with him. 

==Cast==
* Junko Miyashita: Sodeko   
* Naomi Ōka: Yuko
* Hideaki Ezumi: Shinsuke
* Hatsuo Yamaya: Pinsuke
* Go Awazu: Koichi
* Moeko Ezawa: Hanae
* Meika Seri: Hanamuru

==Critical reception==
The World of Geisha was very successful with the public and critics in Japan.    The mainstream film journal Kinema Junpo selected it as the 6th best Japanese film of 1973.  In Currents in Japanese Cinema, Japanese film critic Tadao Sato describes the film as a "masterful porno film rich in emotion, anarchy, and nihilism."  Director Nagisa Oshima took an opposite view of the film in a well-known essay written before his own In the Realm of the Senses (1976). Oshima called Kumashiros film "a little too refined," and took it to task for failing to "realise the effect of pornography." Oshima complained about the Roman porno films that they took "sex as their subject matter and not as their theme. The themes of their most highly regarded films tend to be something like adolescent rebellion; sex is merely the seasoning." He concludes, this "is precisely why these films are attractive to superficial critics, and young film buffs." 

Contemporary Western critics were also positive towards the film. A January 1974 Variety (magazine)|Variety review noted that Kumashiro showed a "definitely savvy directorial flair", but that there was "(n)othing banal, or pretentious" about the film. Further, the "love scenes... are handled with wit and insight", and the "sex bouts... all have a rightness in tone." The review continues, "It is technically fine and ranks with some well-known costumers." "Right placement," Variety concludes, "could have this gaining some following abroad and not for its sex scenes but its feeling for period, time and mores and social outlooks that are reflected from the sensual rather than the social side of its characters." Mosk. Yojohan Fusuma No Shitabari (film review). Variety (magazine)|Variety, 1974-01-02.  François Truffaut called The World of Geisha a "great movie," adding, "The acting is perfect, and the film is humorous. In its praise for female beauty and derision for male stupidity lies the generous spirit of Jean Renoir."   

Later commentators on Japanese cinema continue to hold the film in high esteem. The Weissers, in their Japanese Cinema Encyclopedia: The Sex Films (1998) comment, "The structure of the film is its most interesting aspect. Director Kumashiro is playing intentional games with linear storytelling, creating a narrative which flows unhampered, unrestricted by time."  In his Behind the Pink Curtain, Jasper Sharp calls The World of Geisha Kumashiros "most articulate attack" against censorship and state-imposed morality. Sharp, p. 138. 

== Themes and style ==
Kumashiros screenplay was based on   are another technique Kumashiro uses prominently in The World of Geisha. Most reviews of the film comment on the opening love-making session which lasts over one-third of the films total running time. This long scene is intercut with subplots involving the other geishas and their clients.  Kumashiros usage of inserts contrasts with that of traditional directors such as Yasujirō Ozu|Ozu. Inserts in an Ozu film have a  calming and transitional purpose, whereas Kumashiro uses them to shock the audience while also, in The World of Geisha, providing historical context. Historical events used as inserts in The World of Geisha include the Korean uprisings, Russias October Revolution, the Japanese Rice Riots of 1918, and a police order censoring the news of these riots.  
 fogging – against itself in an exaggerated form, as satirical commentary. In The World of Geisha, he does this while also "censoring" titles, such as Sodekos, "Im coming again!!", which is marked out with "X"s, as were reports of Japanese casualties in Siberia in the Russian Civil War.  By using these two forms of censorship in juxtaposition, Kumashiro draws a parallel between Taishō period censorship, and Eirins, and the governments contemporary forms of censorship. 

==Availability==
The World of Geisha was released theatrically in Japan on November 3, 1973.  Nikkatsu marketed the film to the English-speaking world with the ad, "The essence of the geisha world! The art of pleasing men! ...dainty butterflies... and how these colorful beauties entertain their diverse guests night after night to the strains of the three-stringed samisen." 

The World of Geisha was re-released on DVD in Japan on December 22, 2006, as part of Geneon Universal Entertainment|Geneons sixth wave of Nikkatsu Roman porno series.  In the U.S., The World of Geisha has been released on home video by both Image Entertainment and Kino International (company)|Kino. 

==Bibliography==

===English===
*    
* Mosk. Yojohan Fusuma No Shitabari (film review). Variety (magazine)|Variety, 1974-01-02.
*  
*  
*  
*  
*  

===Japanese===
*  
*  
*  
*  
*  

==Notes==
 

 

 
 
 
 
 
 
 