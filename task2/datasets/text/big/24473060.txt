Chirag Kahan Roshni Kahan
{{Infobox film
| name = Chirag Kahan Roshni Kahan
| image =Chiragkahan.jpg
| caption = DVD cover
| director = Devendra Goel
| producer = Devendra Goel
| writer = Devendra Goel story = Dhruva Chatterjee
| narrator =
| starring =Meena Kumari Rajendra Kumar Ravi
| cinematography =
| editing =
| distributor =
| released =
| runtime =
| country = India
| language = Hindi
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
 Indian Bollywood film directed and produced by Devendra Goel.    The film stars Meena Kumari as the protagonist Ratna, and Rajendra Kumar, Honey Irani and Madan Puri in pivotal roles.

==Plot==
Before giving birth to her child, Ratna (Meena Kumari) loses her husband, and eventually gives birth to a son, Raju. Four years later, Dr. Anand, who treated her while she was giving birth to Raju (Honey Irani) re-enters her life and soon becomes a frequent visitor, much to the displeasure of Ratnas mother-in-law (Mumtaz Begum) and her husbands sister Bela, both of whom start maltreating her and making her life miserable. Anand stops visiting thereafter. Anand soon marries a nurse called Maya Verma (Minu Mumtaz). Maya is a squanderer, and her relationship with Anand falters. Moreover, he later finds out that she cannot conceive. When Anands dad passes away, he leaves considerable wealth to Anands child, including a fair monthly allowance. Maya plots with an advocate, S. Prakash (Sunder), who happens to be Belas husband, to fabricate a story that Anands father was mentally unbalanced while writing the testament. This plan fails, and along with her aunt, Nurse Sarla Verma (Nilambai), make a false statement claiming that Ratnas son is actually Anands biological son, leading to their lawyer filing a custodial case in court. What will happen next?

==Cast==
*Meena Kumari ... Ratna
*Rajendra Kumar ... Dr. Anand
*Honey Irani ... Raju
*Minu Mumtaz ... Nurse Maya Verma
*Nilambai ... Nurse Sarla Verma Sunder ... Advocate S. Prakash
*Madan Puri ... Dr. Mehta Asit Sen ... solicitor Mumtaz Begum ... Ratnas mother-in-law

==Awards==
The film received two nominations at the 1960 annual Filmfare Awards: 
*Nominated, Filmfare Best Actress Award - Meena Kumari
*Nominated, Filmfare Best Story Award - Dhruva Chatterjee

== References ==
 
==External links==
* 

 
 
 