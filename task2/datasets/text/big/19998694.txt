Absolon
 
  
{{Infobox film
| name           = Absolon
| image          = Absolonposter.jpg
| alt            =  
| caption        = Promotional film poster
| director       = David Barto
| producer       = Jamie Brown
| writer         = Brad Mirman
| starring       = Christopher Lambert Kelly Brook Lou Diamond Phillips Ron Perlman Roberta Angelica
| music          = Gary Koftinoff
| cinematography = Unax Mendía   Curtis Petersen
| editing        = Evan Landis
| studio         = {{plainlist|
*GFT Absolon Films, Inc.
*Studio Eight Absolon Films, Ltd. }}
| distributor    = Lions Gate
| released       =  
| runtime        = 96 minutes
| country        = Canada United Kingdom 
| language       = English
| budget         = 
| gross          = 
}} science fiction thriller film. The plot concerns a future society where the only hope for survival from a deadly virus is a drug called Absolon. The film was directed by David Barto, and stars Christopher Lambert, Lou Diamond Phillips, and Kelly Brook.

==Plot==
In the year 2010, a virus infected everyone on the planet, wiping out half the population. Absolon is a drug regimen everyone must now take to stay alive. One corporation controls the drug and Murchison (Ron Perlman) is its leader.

A corporate scientist, who was researching the virus, is found murdered. Norman Scott (Christopher Lambert) is the policeman assigned to investigate the crime. He eventually uncovers a conspiracy involving the scientist. He is given a partial dosage of the cure the scientist had been working on, but soon realizes he is in over his head as he is being hunted by an assassination team. Scott goes on the run with Claire (Kelly Brook), one of the murdered scientists colleagues. They find out the assassins are employed by Murchison.

Scott discovers he is being chased down for the cure in his bloodstream. He is also able to kill the assassins chasing them. In the end, Scott finds out the cure he was carrying wasnt for the original virus, which had died out years ago, but a cure for the worldwide dependence on the addictive Absolon drug itself.

==Notes==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

 
 