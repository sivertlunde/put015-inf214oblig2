Santhanagopalam
{{Infobox film name           =Santhanagopalam director       = Sathyan Anthikkad producer       = Mathew George writer     = Raghunath Paleri starring  Innocent Sankaradi Jagathy Sreekumar Chippy editing        = K. Rajagopal cinematography = Vipin Mohan music  Johnson
|banner         = Central Productions distributor    = Central Productions released       = 1994 studio         = Central Productions runtime        = 130 minutes  country        = India language       = Malayalam
}}

Santhanagopalam is a 1994 Malayalam film starring Thilakan, Jagadheesh, Balachandra Menon, and Kaviyoor Ponnamma in the lead roles. Thilakan won the 1994 Kerala State Film Award for Best Actor for his roles in Gamanam and this film. 

==Plot==

Thilakan is a worker at a factory and his family consists of his wife(Kaviyoor Ponnamma), his dad(Krishnan Kutty Nair)and 4 children(2 sons and 2 daughters).Elder son(Balachandramenon) is trying hard to establish himself in Kerala after having had strong will to stay in Kerala rather than going abroad. Second son(Jagadeesh)is looking for a Government job. Balachandramenon finally starts a telephone booth and is inaugurated by Circle Inspector(Jagathi Sreekumar, which turns out to be an evergreen comedy scene in Malayalam. Thilakan however wants Jagadeesh to look out for better prospects in the interim of his test results. Jagadeesh also shows some plans to start a business of his own. Once he and Thilakan travelling to Mangalore for a job need of Jagadish, meets up with an accident when the lorry they were travelling fell into a river. Jagadeesh escapes and Thilakan is not found. He is believed to be killed in the accident. Thilakan, is an insurance holder for a huge amount and his sons make arrangements to claim it stating that their father is killed in the accident. Thilakans boss(Shankaradi), knowing that Thilakans family is getting a huge amount, tries to interfere with a fraud story. With the papers being arranged for insurance claim, Thilakan surprisingly returns home alive after being in an Ayurvedic Heritage home under treatment nursing his injuries of accident. His return, however does not please his sons, Jagadeesh in special, who by then had laid some business plans eyeing at his fathers insurance money. They force Thilakan to stay home enclosed in a room without roaming outside so as to claim the insurance amount. Thilakans return is witnessed by his neighbor(Innocent) who believes that its his ghost and he turns psychic. Thilakan, unable to bear the humiliation of being locked up in the house and seeing that his sons care money more than him, finally decides to leave home one night. He is accompanied by Kaviyoor Ponnamma who asks him whether he is leaving her alone behind. The climax scene shows Thilakan and Kaviyoor Ponnamma accompanied by Krishnan Kutty Nair also and the 3 travelling away from their children to lead a new life.

==Cast==
*Thilakan as Kurupp
*Jagadheesh ...   
*Balachandra Menon as Sudhakaran
*Kaviyoor Ponnamma ...  
*Krishnan Kutty Nair ... Innocent as Adiyodi
*Sankaradi ...   
*Jagathy Sreekumar ...   
*Chippy (actress) | Chippy...
*Prem Kumar...
*Indrans as Unnivasu

==Soundtrack==
{{Infobox album
| Name        = Santhanagopalam
| Type        = Soundtrack Johnson
| Language = Malayalam
| Genre       = Film
|}}
 Johnson and written by V Madhusoodanan Nair.

{| class="wikitable"
|-
! No. !! Song Title !! Singer
|-
| 1 || Pradosha Kumkumam || KJ Yesudas
|-
| 2 || Thaaram Thookum || Jayachandran | P Jayachandran, Sujatha Mohan, chorus
|-
| 3 || Thinkal Thudukkumbozhente Mukham ||  KJ Yesudas
|}

==References==
 

 
 
 