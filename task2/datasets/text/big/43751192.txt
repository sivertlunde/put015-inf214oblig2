Scarlet River
{{Infobox film
| name           = Scarlet River
| image          = Scarlet River poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Otto Brower
| producer       = David O. Selznick
| screenplay     = Harold Shumate
| story          = Harold Shumate  Tom Keene Dorothy Wilson Roscoe Ates Lon Chaney Jr. Edgar Kennedy
| music          = Max Steiner
| cinematography = Nicholas Musuraca
| editing        = Frederic Knudtson
| studio         = RKO Pictures
| distributor    = RKO Pictures
| released       =  
| runtime        = 54 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Western film Tom Keene, Dorothy Wilson, Roscoe Ates, Lon Chaney Jr. and Edgar Kennedy. The film was released on March 10, 1933, by RKO Pictures.   

== Cast == Tom Keene as Tom Baxter  Dorothy Wilson as Judy Blake
*Roscoe Ates as Ulysses 
*Lon Chaney Jr. as Jeff Todd 
*Edgar Kennedy as Sam Gilroy 
*Billy Butts as Buck Blake
*Hooper Atchley as Clink McPherson
*Betty Furness as Babe Jewel
*Jack Raymond as Benny James Mason as Dummy 
*Yakima Canutt as Yak

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 
 