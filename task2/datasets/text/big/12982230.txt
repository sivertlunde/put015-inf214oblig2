The Ugly Swans (film)
{{infobox film image = Ugly swans poster.jpg name = The Ugly Swans director = Konstantin Lopushansky producer = Catherine Dussart Dmitry Gerbachevsky Andrey Sigle writer =Konstantin Lopushansky Vyacheslav Rybakov Arkady and Boris Strugatsky (novel) starring = Catherine Dussart Gregory Hlady music = Andrey Sigle cinematography = Vladislav Gurtchin editing = Maxim Holodiuk Sergei Obukhov Aleksandr Zaretzky	 	 distributor = Proline Film runtime = 105 minutes country = Russia language = Russian released =   budget = 
}} novel of the same name by Arkady and Boris Strugatsky. The film is often compared to Andrei Tarkovskys Stalker (1979 film)|Stalker, also adapted from a Strugatsky book.  

==Plot==
The films plot is loosely based on the novel, with some superficial differences. The story has been adjusted slightly to contextualize it in the "near future," with the main character Victor Banev recast as a UN envoy to the town of Tashlinsk, where a mysterious group has taken the towns children to an isolated boarding school. The major departure from the novels plot is in the ending, in which the "Aquatters" ("Slimeys" from the novel) are all killed by the humans. The children are heroically rescued by Banev, but they are unable to reassimilate into society and are institutionalized.

A small role created for the film was a UN negotiator named Gennady Komov, a reference to a popular character from the Strugatskys other books.

==References==
 

==External links==
* 
*  at the London Film Festival

 
 
 
 
 
 
 
 
 
 


 
 