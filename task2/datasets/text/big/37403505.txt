Le Solitaire (film)
{{Infobox film
| name           =  Le Solitaire
| image          = Le solitaire.jpg
| image size   =
| alt            =
| caption        = 
| director       = Jacques Deray
| producer       = Alain Sarde
| writer         = Alphonse Boudard  Jacques Deray  Simon Michaël  Daniel Saint-Hamont
| narrator       =  Jean-Paul Belmondo
| starring       = Jean-Paul Belmondo
| music          = Danny Shogger
| cinematography = Jean-François Robin
| editing        = Henri Lanoë
| studio         = 
| distributor   = 
| released       = 18 March 1987
| runtime        = 100
| country        = France
| language       = French
| budget         =
| gross          =918,187 admissions (France) 
}}

Le Solitaire is a French crime film directed and partly written by Jacques Deray. 

== Cast ==

* Jean-Paul Belmondo: Commissaire Stan Jalard  
* Jean-Pierre Malo: Charly Schneider  
* Michel Beaune: inspector Pezzoli   Pierre Vernier: Maurin  
* François Dunoyer: René Pignon 
* Michel Creton: Simon Lecache
* Franck Ayas:Christian Lecache, Simons son

== Plot ==

The policeman Stan Jalard and his colleague Simon Lecache are rather fed up with police work. They are toying with the idea to quit police service in order to run a hotel on the Antilles. Single father Lecache has already asked his son Christian about it. But at the very evening when Lecache tells Jalard that his son approves of their plan, Lecache is murdered by the professional killer Charly Schneider. 

Jalard changes his mind. He dedicates his life all the more to police work. After two more years he has been promoted but he had no chance to get Schneider yet because Schneider disappeared. Eventually Schneider returns to France and commits crimes. Moreover he threatens Jalard on the phone and later devastates his flat. He even sends somebody to shoot Jalard and his godson Christian in the street. 

Jalard identifies Schneiders new accomplices and puts them under pressure. Step by step he closes in on him until he can confront him in his hide-out. Schneider refuses to show any regret, eludes and steals a car. He tries to run over Jalard who arrests him anyway. Now that Jalard has brought the murderer of Christians father to justice, he allows Christian to call him “Dad”. He, who has put his godson away into boarding schools all the time and lived only for his police work, now demonstrates a shift in priorities. When they drive home, Jalard puts a police siren on top of his car and drives wiggly lines just because that obviously amuses little Christian a lot.

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 


 
 