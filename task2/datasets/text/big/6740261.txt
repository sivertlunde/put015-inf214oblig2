Eternally Yours (film)
{{Infobox film
| name           = Eternally Yours 
| image          = Eternally-Yours-1939.jpg
| caption        = Film poster Charles Kerr (assistant)
| producer       = Tay Garnett Walter Wanger (uncredited executive producer)
| writer         = Charles Graham Baker|C. Graham Baker   Gene Towne
| starring       = Loretta Young   David Niven
| music          = Werner Janssen
| cinematography = Merritt B. Gerstad
| editing        = 
| distributor    = United Artists 
| released       = October 7, 1939
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         = $790,878 Matthew Bernstein, Walter Wagner: Hollywood Independent, Minnesota Press, 2000 p440 
| gross = $683,131 
| preceded_by    = 
| followed_by    = 

}} Academy Award for Best Music.

==Plot==
 
Anital Halstead (Loretta Young) goes to see a magic act performed by Tony (David Niven), the "Great Arturo", after her bridal shower for her wedding to Don Burns (Broderick Crawford). Anita and Tony are immediately attracted to each other and get married. She becomes his assistant in the act. 

One night, Tony becomes drunk in the company of a woman reporter and boasts he will jump out of an airplane at   with his hands handcuffed behind his back. When she prints his claim, he first tries to get out of it with a fake cast on his arm, but when he sees the thousands of fans, he goes through with it, freeing himself in mid-air and parachuting safely to the ground. He promises Anita that he will not attempt the dangerous stunt again, but soon breaks his word and performs it repeatedly all over the world.

Anita becomes weary of the constant travel and longs to settle down and start a family. Secretly, she sells her jewelry and has a house built in the Connecticut countryside. When it is completed, she shows Tony a picture of it, but his uninterested reaction stops her from telling him it is theirs. When he signs up for a two-year, round-the-world tour rather than take the vacation he had promised, she finally gives up. She leaves him and gets a divorce in Reno. Anitas grandfather, Bishop Peabody (C. Aubrey Smith), breaks the news to the distraught Tony.

On a sea cruise with her Aunt Abby (Billie Burke), Anita is surprised to run into her old fiance Don. She gets the ships captain to marry them. However, she spends their honeymoon night with her grandfather. The next night, Don insists on introducing her to his boss, Harley Bingham (Raymond Walburn), at a nightclub. The entertainment is none other than the Great Arturo, with his old assistant, Lola De Vere (Virginia Field). He soon persuades Bingham to let him perform at Binghams company retreat at a resort, much to Anitas discomfort. 

Mrs. Bingham (Zazu Pitts) has a dilemma though. They have not booked enough rooms to provide separate bedrooms for the unmarried Tony and Lola. Tony suggests he and Don share one room, while Anita and Lola take the other. During his stay, Tony tries unsuccessfully to persuade Anita to take him back. Meanwhile, the hapless Don becomes sick, and the doctor prescribes no physical activity of any sort for a month.

Bishop Peabody is told by his lawyer that Anitas divorce is not legal. Later, he informs his granddaughter that Tony will be doing his parachute stunt that day. She attends. Tony tells his valet and friend Benton (Hugh Herbert) that he hid a lockpick in the wrong airplane, but goes ahead with the trick anyway. He frees himself dangerously close to the ground. After he is pulled unconscious out of the water, Anita rushes to his side. When he regains consciousness, they are reconciled. In the final scene, they enter their Connecticut home.

==Cast==
*Loretta Young as Anita Halstead
*David Niven as Tony, "The Great Arturo"
*Hugh Herbert as Benton
*Billie Burke as Aunt Abby
*C. Aubrey Smith as Gramps, aka Bishop Peabody
*Raymond Walburn as Mr. Harley Bingham
*Zasu Pitts as Mrs. Cary Bingham
*Broderick Crawford as Don Burns
*Virginia Field as Lola De Vere
*Eve Arden as Gloria, a friend of Anitas
*Ralph Graves as Mr. Morrisey
*Lionel Pape as Mr. Howard
*Fred Keating as Master of Ceremonies

==Reception==
The film recorded a loss of $200,281. 

==Production== Trade Winds their previous collaboration. The film also featured footage from the 1939 New York Worlds Fair.  Paul LePaul, the magician who was a technical adviser to the film has a cameo doing card tricks with Hugh Herbert.

The film was Loretta Youngs first film after losing her contract at 20th Century Fox.  She had previously starred in Wangers Shanghai (1935 film)|Shanghai (1935).

Paul Mantz performed aerial stuntwork and photography for the film. 

==See also==
* List of films in the public domain

==References==
 

==External links==
 
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 