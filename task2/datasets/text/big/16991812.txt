Dayere Zangi
{{Infobox film
| name           = Dayere Zangi
| image          =
| caption        = Original film poster
| director       = Parisa Bakhtavar
| producer       = Jamal Sadatian
| writer         = Asghar Farhadi
| starring       = Baran Kowsari Saber Abar Mehran Modiri Mohammad Reza Sharifinia  Hamed Behdad Amin Hayai Bahareh Rahnama Omid Rohani Gohar Kheirandish Nima Shahrokh Shahi Niloofar Khoshkholgh Negar Foroozandeh Akram Mohammadi Amir Noori Mahdi Pakdel Afarin Chitsaz Sarina Farhadi Soroosh Goodarzi Kianoosh Gerami Mohsen Ghazi Moradi Bahareh Rahnama Melika Sharifinia
| music          = Amir Tavassoli
| cinematography = Morteza Poursamadi
| editing        = Hayede Safiyari
| distributor    = Boshra Film
| released       =  
| runtime        = 110 minutes
| country        = Iran
| language       = Persian
| budget         = Boshra Film
| gross          = Rial5,233,183,000 
}}
Dayere Zangi, (Dayereh zangi or Dayereh-e zangi) ( ) is a 2008 Iranian film directed by Parisa Bakhtavar and written by her director husband, Asghar Farhadi. It was released in Iran during Nowrooz holidays and was an average gross.

==Story==
Shirin (Baran Kowsari), a young woman, has only one day to make 3,000,000 rials to repair the broken car that she claims to be her fathers. She befriends a naive, young man named Mohammad (Saber Abar) who tries to help her to raise the money by entering an apartment building and installing satellites for its diverse residents &mdash; with diverging views on media &mdash; when they get into further trouble.

==References==
 

==External links==
*  

 
 
 