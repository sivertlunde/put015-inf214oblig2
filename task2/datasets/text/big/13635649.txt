Jaanam Samjha Karo
{{Infobox film
| name = Jaanam Samjha Karo जानम समझा करो  
| image = Jaanam_Samjha_Karo.jpg
| caption = DVD cover
| director = Andaleeb Sultanpuri
| producer = Bubby Kent
| writer = Rajkumar Santoshi
| narrator = 
| starring = Salman Khan Urmila Matondkar Jaspal Bhatti Shammi Kapoor
| music = Anu Malik Anand-Milind
| cinematography = Santosh Sivan
| editing = 
| distributor = 
| released = 2 April 1999
| runtime = 153 min
| country = India
| language = Hindi
| Movie result = Flop
| gross = 8.4 crore 
| preceded_by = 
| followed_by = 
}} Indian Bollywood film directed by Andaleeb Sultanpuri who is the son of veteran lyricist Majrooh Sultanpuri. It was released on 2 April 1999. The film stars Salman Khan, Urmila Matondkar in lead roles, with Shakti Kapoor, Shammi Kapoor and Monica Bedi in supporting roles

==Plot==
Chandni (Urmila Matondkar), a lower class talented night club singer and dancer, who happens to be religious, and well mannered, lives an unfortunate life with her three aunts, who dominate her life, and with a weak maternal grandmother.  She dances for a living because it is the only way she can support her poor family and she hopes to give her nieces an education.

One night while singing, she meets Rahul (Salman Khan), a young wealthy womanizer, and falls in love with him.  Rahul feels attracted to her but he does not take her seriously.  He intends to treat her just like any of his other girlfriends, and only wants to have a short affair.  They get into fights all the time, only to become friends again.  However after a mistaken impulse of kissing Chandni turns out bad for Rahul as he realizes Chandni is not like the girls he had met and he try to mend thing sending gift to Chandni, but the gift is rejected by Chandni as she feels as if Rahul keeps following her. 
Later on, she is stuck alone in London, being chased by an old Indian pervert who tricked her into getting in to his car since she was stuck outside the Indian embassy.  She runs out of the car after the old man tries molesting her and is coincidentally saved by Rahul. Rahul invites her to stay at his rather luxurious hotel. 
One day, however, his Dadaji (Shammi Kapoor), mentions that he wants to see Rahul married to Chandni.  In order to fool him, Rahul asks Chandni to pretend to be his  wife, which she accepts.  After Dadaji leaves, Rahul and Chandni have a big fight, where Rahul slaps Chandni, and both decide to go their separate ways.  However, Rahul realizes that he has fallen in love with her.  He follows her back to India from London.  He goes to her home and asks to speak to her, but she refuses to listen to him. So he yells out his apologies in front of her house and states that he loves her, while a crowd gathers.  His ex-friend arrives there with a gang and they get into a fight.  When Rahul is almost about to murder his ex friend, Chandni comes out and tells him to not ruin his life by killing Harry.  Rahul then proposes to her, and Chandni wants to accept, except that she had previously promised her cold-blooded manager that she would move to another city  and open a bar with him.  The manager feels as if he has witnessed true love and kindly lets Chandni go.  Chandni then accepts Rahuls proposal and the movie ends with them embracing each other.

==Cast==
*Salman Khan&nbsp;... Rahul 
*Urmila Matondkar&nbsp;... Chandini 
*Shakti Kapoor&nbsp;... Harry
*Sadashiv Amrapurkar&nbsp;... Daniel
*Jaspal Bhatti&nbsp;... Tubby, Rahuls secretary 
*Bindu (actress)|Bindu&nbsp;... Chandinis aunt 1
*Monica Bedi&nbsp;... Monica
*Shammi Kapoor&nbsp;... Rahuls Dadaji

== Music ==
The soundtrack of the film contains 8 songs. The music is conducted by Anu Malik, except for one song composed by the duo Anand-Milind (Kisi Ne Humse Kiya Hai Vaada). Lyrics are authored by Majrooh Sultanpuri. Music was very popular at that time.

{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- style="background:#3366bb; text-align:center;"
! # !! Song !! Singer(s) !! Length 
|-
| 1
|Chandni Aaya Hai Tera Deewana
| Udit Narayan
| 06:22
|-
| 2
| I Was Made For Loving You Anu Malik, Kamaal Khan, Ila Arun
| 06:36
|-
| 3
| Jaanam Samjha Karo
| Anu Malik, Hema Sardesai
| 06:35
|-
| 4
| Kisi Ne Humse Kiya Hai Vaada
| Alka Yagnik
| 06:14
|-
| 5
| Love Hua
| Kumar Sanu, Alka Yagnik
| 06:45
|-
| 6
| Main Ladki Akeli
| Anu Malik, Hema Sardesai
| 07:11
|-
| 7
| Sabki Baaraten Aayeen
| Alka Yagnik
| 05:46
|-
| 8
| Sabki Baaraten Aayeen
| Jaspinder Narula
|  05:38
|}

==References==
 

==External links==
* 

 
 
 