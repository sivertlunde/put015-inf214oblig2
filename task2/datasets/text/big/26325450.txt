7 Khoon Maaf
 
 

 

{{Infobox film
| name           = 7 Khoon Maaf
| image          = 7KhoonMaaf_poster_ver1.jpg
| caption        = Theatrical release poster
| director       = Vishal Bhardwaj 
| producer       = Ronnie Screwvala Vishal Bhardwaj Matthew Robbins Vishal Bhardwaj
| narrator       = Vivaan Shah
| based on       =   John Abraham Irrfan Khan Aleksandr Dyachenko Annu Kapoor Naseeruddin Shah Vivaan Shah Usha Uthup
| music          = Vishal Bhardwaj
| cinematography = Ranjan Palit
| editing        = A. Sreekar Prasad
| studio         = VB Pictures UTV Spotboy
| released       =  
| runtime        = 147 minutes 
| country        = India
| language       = Hindi
| budget         =     
| gross           =     
}} Indian black John Abraham, Aleksandr Dyachenko, Vivaan Shah and Usha Uthup in supporting roles.
 Best Female Playback Singer for Uthup and Rekha Bhardwajs song, "Darling".

==Plot==
7 Khoon Maaf tells the story of an Anglo-Indian woman, Susanna Anna-Marie Johannes (Priyanka Chopra), who murders all her husbands. Susanna tries to find love, but six of her seven husbands have a flaw which proves fatal. The killings (and her yearning for love) are explained by the loss of her mother at a young age. The story is told by a young forensic pathologist, Arun, (Vivaan Shah) to his wife (Konkona Sen Sharma). Arun has watched Susanna since their childhood, and has a secret crush on her. He owes his life to her; she funded his education, making him what he is today. Arun tells his wife that Susanna has committed suicide, leaving him a note congratulating him on his marriage. The doctor must now confirm that the body found is Susannas. Arun is unable to overcome his grief over her death and pours out his heart to his wife, who listens patiently.
 stableboy (Shashi Malviya) with a whip. Edwin is eliminated during a panther-hunting trip with the help of Susannas faithful maid (Usha Uthup), butler (Harish Khanna) and the mute stableboy.
 John Abraham), who renames himself Jimmy Stetson after their marriage—is a singer whose flaw is pride. The marriage begins well, but Jimmy becomes successful and misuses his new-found fame. He steals songs, dallies with other women and becomes dependent on drugs. Susanna wants to be lucky in love the second time; she tries to wean him from his addiction, but he continues in secret. She gives up, disposing of him with a heroin overdose. Police investigators find footprints near his body indicating that a person with six toes is the murderer.

Susannas third husband—Wasiullah Khan (also known as Musafir) (Irrfan Khan)—is a soft-spoken, thoughtful poet by day and a sado-masochist by night. Susanna tries to cover her bruises with makeup; her servants cannot bear to see her mistreated, and advise her to get rid of him. Khan is ultimately interred in a snowy Kashmiri grave.

Her fourth husband, Nicolai Vronsky (Aleksandr Dyachenko), is a Russian spy leading a double life. When Susanna discovers he has another wife and family, her rage knows no bounds and he meets his end with the aid of Susannas pet snakes. After Vronskys death, Susanna no longer agonizes over eliminating anyone of whom she disapproves.
 Viagra proves his undoing; one fateful night, Susanna mixes an overdose of the drug into Lals drink.
  epiphany she imagines Jesus Christ as her seventh husband, full of love and never hurtful; she dies, becoming a nun and finding the love she sought all her life. Arun and his wife return home after he tells her that Susanna is dead.

==Cast==
* Priyanka Chopra as Susanna Anna-Marie Johannes (also known as Saheb, Suzi, Sultana, Anna and Sunaina)
* Neil Nitin Mukesh as Major Edwin Rodriques (first husband) John Abraham as Jamshed Singh Rathod-Jimmy Stetson (second husband)
* Irrfan Khan as Wasiullah Khan (also known as Musafir) (third husband)
* Aleksandr Dyachenko as Nicolai Vronsky (fourth husband)
* Annu Kapoor as Inspector Keemat Lal (fifth husband)
* Naseeruddin Shah as Dr. Modhusudhon Tarafdar (also known as Modhu Daa) (sixth husband)
* Vivaan Shah as Arun Kumar
* Usha Uthup as Maggie Aunty (maid)
* Harish Khanna as Galib Khan (butler)
* Shashi Malviya as Goonga Chacha (stableboy)
* Konkona Sen Sharma as Nandini, Aruns wife (cameo appearance)
* Ruskin Bond as church father (cameo appearance)
* Radhika Arora as Nandinis friend (cameo appearance)

==Production==
After making Ruskin Bonds novel The Blue Umbrella into a The Blue Umbrella (film)|film, Vishal Bhardwaj adapted Bonds short story "Susannas Seven Husbands". Rekha Bhardwaj, who enjoys Bonds work, recommended the story to her husband Vishal.  He was intrigued by the four-page storys title, and thought it had potential for a novel and a film. Vishal Bhardwaj said, "I told myself why would a woman have seven husbands and then I came to know that she also kills them! I was immediately hooked to it. It reminded me of a very old film, Bluebeards Seven Wives".    Bhardwaj noted that he retained Bonds theme, but incorporated his own elements to make the film a dark comedy. He said, "I had previously taken liberties with Shakespeare. Naturally, when you adapt a story, your vision also comes in it. But I have remained honest to its essence".  He included Keemat Lal (who plays a police officer in all Bonds stories), although the character is not in the original story. Bhardwaj decided to include him in the film as an homage to Bond, explaining that he took liberties with characters names and traits.  Bond said that when Bhardwaj decided to adapt the story for the screen, he expanded it into an 80-page novella and began to think in terms of scenes. The novella became a 200-page, full-length Hindi script.    Bond also had to devise Indian methods of killing the husbands, which he found challenging: "The challenge was devising seven ingenious ways in which she could kill her husbands without being suspected. And she does it successfully, until towards the end". 
 Imtiaz Ali were approached to play two of the seven husbands, but they reportedly turned down the offer.   Usha Uthup was cast as Susannas maid in the film.  Konkona Sen Sharma was confirmed for a cameo appearance. 
 The Curious Case of Benjamin Button, to create seven looks for the character.  Chopra found the most challenging period was that of the 65-year-old woman.  The actress said, "prosthetics had to play a heavy part but I’m happy that I pushed myself as the result is really fab. I had to be very careful of not doing things that would damage the make-up". To make Chopra look authentic, the makeup team used Chopras mothers and grandmother’s photos to create her look.  She had to gain five kg weight to fit her aging character. 
 Shalimar Bagh Hyderabad and in Russia.    During filming Chopra was restricted from eating and drinking while donning the prosthetic makeup (which took five hours to apply). 

==Soundtrack==
{{Infobox album 
| Name        = 7 Khoon Maaf
| Type        = soundtrack
| Artist      = Vishal Bhardwaj
| Cover       = 7KhoonMaaf albumcover.jpg
| Released    =    (digital)     (CD) 
| Recorded    = 2010–2011 Studio Satya, Mumbai Empire Audio Center Pvt. Ltd., Mumbai Rajiv Menon Studios, Chennai
| Genre       = Soundtrack
| Length      =  
| Label       = Sony Music
| Producer    = Hitesh Sonik, Clinton Cerejo, Simaab Sen
| Last album  = Ishqiya (2010)
| This album  = 7 Khoon Maaf (2011)
| Next album  = Matru Ki Bijlee Ka Mandola (2012)
}}
The films score and songs were composed by Vishal Bhardwaj, with lyrics by Gulzar. Its soundtrack contains seven songs and two reprise versions. The soundtrack was digitally released on Ovi (Nokia) on 21 January 2011, and on CD 24 January 2011. The song "Darling" is based on the Russian folk song "Kalinka (song)|Kalinka", and contains several Russian words (one of Susannas husbands was Russian). "Kalinka" is credited on the album cover.  "Tere Liye", sung by Suresh Wadkar, was not used in the film.

{{track listing
| headline       = Track listing
| extra_column   = Singer(s)
| lyrics_credits = yes
| title1         = Darling
| extra1         = Usha Uthup, Rekha Bhardwaj
| music1         = Vishal Bhardwaj 
| lyrics1        = Gulzar
| length1        = 3:27
| title2         = Bekaraan
| extra2         = Vishal Bhardwaj
| music2         = Vishal Bhardwaj
| lyrics2        = Gulzar, Javed Sheikh
| length2        = 6:25
| title3         = O Mama
| extra3         = KK (singer)|KK, Clinton Cerejo
| music3         = Vishal Bhardwaj
| lyrics3        = Ajinkya Iyer
| length3        = 4:53
| title4         = Awaara
| extra4         = Master Saleem
| music4         = Vishal Bhardwaj
| lyrics4        = Gulzar
| length4        = 5:31
| title5         = Tere Liye
| extra5         = Suresh Wadkar
| music5         = Vishal Bhardwaj
| lyrics5        = Gulzar
| length5        = 5:42
| title6         = Dil Dil Hai
| extra6         = Suraj Jagan
| music6         = Vishal Bhardwaj
| lyrics6        = Gulzar
| length6        = 3:06
| title7         = Yeshu
| extra7         = Rekha Bhardwaj
| music7         = Vishal Bhardwaj
| lyrics7        = Gulzar
| length7        = 6:26
| title8         = Doosri Darling
| extra8         = Usha Uthup, Rekha Bhardwaj, Clinton Cerejo, Francois Castellino
| music8         = Vishal Bhardwaj
| lyrics8        = Gulzar
| length8        = 3:04
| title9         = O Mama (Acoustic)
| extra9         = KK
| music9         = Vishal Bhardwaj
| lyrics9        = Ajinkya Iyer
| length9        = 1:50
}}

The soundtrack received positive reviews from music critics. The   rated the album a 3 (out of 5): "7 Khoon Maaf is a good album with a couple of definite hits, couple of skip worthy ones and the remaining have the potential to grow. Since 7 Khoon Maaf is not a routine Bollywood affair, it cant be expecting a quick pick at the stands from Day One".  The Hindustan Times noted that the soundtrack takes the listener through a variety of moods: "Bhardwaj has certainly succeeded in adding new sounds to his catalogue. He continues to evade Bollywood monotony with this soundtrack   an innovative effort." 

==Marketing and release==
 
The films preview and trailer were released on 24 December 2010 to a positive response from critics, who praised the whole presentation and Chopras dialogues particularly: "duniya ki haar biwi ne kabhie na kabhie toh yeh zarur sochega, ki main apne pati se hamesha hamesha ke liye chutkara kaise paun" ("Every wife in the entire world must have once in her lifetime thought of how to get rid of her husband forever").  Following the preview-trailer launches, Chopra promoted the film by appearing with seven men (dressed as bridegrooms) at the Radio Mirchi FM studio.    The succession of marriages and funerals was illustrated at a promotional event for the film, where Chopra appeared as a Catholic bride in a wedding gown holding a bouquet. A short time later she reappeared as a widow in mourning, for her husbands funerals.  

Emphasizing the films theme, Chopra introduced a "seven ways to lose your Valentine" press kit for reporters at a Valentines Day promotion. The kit contained a rope, a syringe, a knife, a bottle of poison, a sachet of potassium cyanide, an ice pick and a blister pack of Viagra, which was based on Susannas way to kill her husbands in the film.  In February 2011, a book entitled Susannas Seven Husbands was released by Penguin Books as a collectors edition including the novella, the short story and the film’s screenplay. 
 Panorama section.    At the Friedrichstadt Palace (a Berlin theatre) an audience of some 2,500 people watched the film on the festivals final evening, giving the director and the nine cast and crew members present a standing ovation when they appeared onstage. 
 Dhobi Ghat. Reliance Home Entertainment released 7 Khoon Maaf on DVD in March 2011 across all regions in a one-disc NTSC format.  The Blu-ray and Video CD versions were released at the same time. 

==Reception==
7 Khoon Maaf was critically praised and Chopras performance was singled out in particular, with many reviewers describing it as "a role of a lifetime".  Aniruddha Guha of the   also rated the film four out of five: "Vishal Bhardwaj does it again. The maverick filmmaker has once again woven magic with his latest blockbuster Saat Khoon Maaf, which presents Priyanka Chopra in a never before character". 

  of Bollywood Hungama gave the film three stars, saying that "7 Khoon Maaf is a dark film that has its share of positives and negatives. However, the film will meet with diverse reactions – some will fancy it, while some will abhor it. The film will appeal more to the critics/columnists and the festival circuit". 

The film also received some criticism.   of NDTV gave the movie two-and-a-half stars (out of five), terming it a "disappointment" and saying "the film stumbles and fumbles. The episodic nature of the narrative makes the plot predictable." 

At the box office, the film opened to weak ticket sales across India (its release coincided with the 2011 Cricket World Cup).  It grossed   during its first week.  The film earned approximately   at the domestic box office.  Made on a budget of  ,  7 Khoon Maaf failed to do much at the box office, despite positive reviews; however, it made a profit for its producers by earning an additional   for its television-music-home-video rights. Siddharth Roy Kapur (CEO of UTV Motion Pictures) said, "7 Khoon Maaf has worked well for us commercially due to a combination of tight production budgeting, optimised spending on prints and publicity and a pre-sales strategy that helped us to de-risk the film via sales of home video, music, satellite and theatrical rights even before the release". 

==Awards and nominations==
  Best Female Best Villain.  7 Khoon Maaf won three Apsara Film & Television Producers Guild Awards (out of six nominations) in technical categories: Best Cinematography, Best Art Direction and Best Costume Design. 

==References==
 

==External links==
 
*  
*  

 

 
 

 
 
 
 
 
 
 
 
 
 
 
 