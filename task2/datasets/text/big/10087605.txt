Where There's a Will (1936 film)
{{Infobox film
| name           = Where Theres a Will
| image          = wheretheresawill.jpg
| image size     = 150px
| caption        =
| director       = William Beaudine
| producer       = Michael Balcon
| writer         = Robert Edmunds Will Hay William Beaudine
| narrator       =
| starring       = Will Hay H.F. Maltby Graham Moffatt Norma Varden
| music          = Bretton Byrd
| cinematography = Charles Van Enger
| editing        = Terence Fisher
| distributor    = Gainsborough Pictures
| released       = 10 August 1936
| runtime        = 80 minutes
| country        = United Kingdom English
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}
Where Theres a Will is a 1936 British comedy film directed by William Beaudine and starring Will Hay, Graham Moffatt and Norma Varden. It features an incompetent solicitor who unwittingly becomes party to a bank robbery.

==Plot outline==

Will Hay plays the pennyless, bungling solicitor Benjamin Stubbins, who arrives at his office to find his insolent office boy (Graham Moffatt) with his feet up on the desk, reading a wild west magazine, which Hay confiscates so that he can read it later.

Stubbins later takes a job from a group of Americans who claim they want him to track down some ancestors of theirs in Scotland.  In reality however they want to use his office so they can rob a safe in the room immediately below his office. Stubbins takes the job (which is designed to keep him out of the office).

In the end Stubbins realises his mistake and at a Christmas Eve fancy dress party he informs a group of carol singing policeman about the Americans nefarious activities.

==Cast==
*Will Hay as Benjamin Stubbins
*Graham Moffatt as Willie the office boy
*Norma Varden as Lady Margaret Wimpleton
*Hartley Power as Duke Wilson
*Gina Malo as Goldie Kelly
*H.F. Maltby as Sir Roger Wimpleton
*Peggy Simpson as Barbara Stubbins
*Gibb McLaughlin as Martin
*Eddie Houghton as Slug
*Hal Waters as Nick John Turnbull as Detective Collins
*Sybil Brooke as the Landlady
*Davina Craig as Lucie
*Mickey Brantford as Jimmy
*Henry Adnes as the Pawnbroker
*Frederick Piper as a Fingerprint expert

==External links==
*  
*  

 

 
 
 
 
 
 
 


 