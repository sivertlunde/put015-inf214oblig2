Why Not Me? (film)
{{Infobox film
| name = Why Not Me?
| image = PourquoiPasMoi.jpg
| image_size =
| caption =
| director = Stéphane Giusti
| producer = Caroline Adrian Marie Masmonteil
| writer = Stéphane Giusti
| narrator =
| starring = Amira Casar Julie Gayet Bruno Putzulu Alexandra London
| music =
| cinematography = Antoine Roch
| editing = Catherine Schwartz
| distributor = Cinema Mondo
| released =  : October 14, 1999 UK: 8 September 2000
| runtime = 95 mins.
| country = France Spain Switzerland French
| budget =
| gross =
| preceded_by =
| followed_by =
}} 1999 France|French come out to their parents. {{Cite news
  | last = French
  | first =  Philip
  | author-link = 
  | last2 = 
  | first2 = 
  | author2-link = 
  | title = Pourquoi Pas Moi?
  | newspaper = The Observer
  | pages = 
  | year = 
  | date = 2000-11-26
  | url = http://film.guardian.co.uk/News_Story/Critic_Review/Observer_review/0,,402998,00.html
  | accessdate =2009-09-25
}}
 

==Cast==
* Amira Casar as Camille
* Julie Gayet as Eva
* Bruno Putzulu as Nico
* Alexandra London as Ariane
* Carmen Chaplin as Lili
* Johnny Hallyday as José
* Marie-France Pisier as Irene
* Brigitte Roüan as Josepha
* Assumpta Serna as Diane
* Elli Medeiros as Malou
* Vittoria Scognamiglio as Sara
* Jean-Claude Dauphin as Alain
* Joan Crosas as Tony
* Montse Mostaza as Tina
* Marta Gil as Clara
* Cedric Dupuy as Frenchie  

==Awards==
In 1999, Why Not Me? won the Best Lesbian Feature at the Seattle Lesbian & Gay Film Festival. This was followed in 2000 with the Audience Award and the Jury Award at the 2000 Miami Gay and Lesbian Film Festival. {{cite web
 | title = Awards for Pourquoi pas moi?
 | work =Internet Movie Database
 | url = http://www.imdb.com/title/tt0162556/awards
 | accessdate =2007-07-13 }} 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 

 
 