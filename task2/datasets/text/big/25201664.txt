List of horror films of 2009
 
 
A list of horror films released in 2009 in film|2009.

{| class="wikitable sortable" 2009
|-
! scope="col" | Title
! scope="col" | Director
! scope="col" class="unsortable"| Cast
! scope="col" | Country
! scope="col" class="unsortable"| Notes
|-
!   | A Blood Pledge
| Lee Jong-yong || Son Eun-seo, Jang Kyeong-ah, Song Min-jeong, Oh Yeon-seo, Yoo Shin-ae ||   ||  
|-
!   | Antichrist (film)|Antichrist
| Lars von Trier || Willem Dafoe, Charlotte Gainsbourg ||             || 
|-
!   | Abduction
| John Orrichio || Tony Rugnetta, Roberto Lombardi, Christy Callas, Deana Demko ||   ||   
|-
!   | Abundant Sunshine
| || ||   ||  
|-
!   | Agyaat
| Ram Gopal Verma || Adesh Bhardwaj, Rasika Dugal, Joy Fernandes ||   ||  
|-
!   | Air terjun pengantin
| Rizal Mantovani || Tamara Blezinski, Marcel Chandrawinata, Tyas Miraish ||   ||  
|-
!   | Albino Farm
| Sean McEwen, Joe Anderson || Tammin Sursok, Sunkrish Bala, Chris Jericho ||   ||  
|-
!   | All About Evil Thomas Dekker, Cassandra Peterson, Mink Stole ||   ||  
|-
!   | Are You Scared 2
| John Lands, Russell Appling || Kathy Gardiner, Adrienne Hays, Chad Guerrero ||   ||  
|-
!   | Basement Jack
| Michael Shelton || Eric Peter-Kaiser, Michele Morrow, Sam Skoryna ||   ||  
|-
!   | Bedtime Ghost Tales
| J.R. Thomas || ||  ||  
|-
!   | Bigfoot
| Bob Gray || Brooke Beckwith, Van Jackson, Shawn Kipp ||   ||  
|-
!   | Bikini Frankenstein
| || Jayden Cole, Christine Nguyen, Brandin Rackley ||   || Comedy horror 
|-
!   |  
| James Nguyen || Whitney Moore, Janae Caster, Colton Osborne ||   ||  
|-
!   | Bikini Girls on Ice
| Geoff Klein || Cindel Chartrand, Danielle Doetsch, Suzi Lorraine ||   ||  
|-
!   | Black Devil Doll
| Jonathan Lewis || Heather Murphy, Natasha Talonz, Christine Svendsen ||   ||  
|-
!   | The Black Waters of Echos Pond
| Gabriel Bologna || Danielle Harris, Mircea Monroe, Adamo Palladino ||   ||  
|-
!   | Bleed with Me
| || ||   ||  
|- The Bleeding DMX ||   ||  
|-
!   | Blood Creek
| Joel Schumacher || Dominic Purcell, Henry Cavill, Michael Fassbender ||   ||  
|-
!   |  
| Chris Nahon || Masiela Lusha, Colin Salmon, Gianna Jun ||    ||  
|-
!   | Blood Moon Rising
| Brian Skiba || Ron Jeremy, David C. Hayes, Davina Joy ||   ||  
|-
!   | Blood Red Moon
| Scott Patrick || Sarah Lavrisa, Matthew Rogers, Mark Courneyea ||   ||  
|-
!   | Blood Ties Jason Carter, DJ Perry ||   ||  
|-
!   | Bloodbath in the House of Knives
| Ted Moehring || ||   ||  
|-
!   | Breaking Nikki
| Hernán Findling || Maria Ines Alonso, Maxime Seugé, Oliver Kolker ||   ||  
|- Burning Bright
| Carlos Brooks || Briana Evigan, Meat Loaf, Mary Rachel Dudley ||   ||  
|-
!   | The Burnt House
| Adam Ahlbrandt || Lily Ahlbrandt, Joe Hiate, Monica Knight ||   ||  
|-
!   |   Michael Bowen, Noah Segan ||   ||  
|-
!   | Carriers (film)|Carriers
| Alex Pastor, David Pastor || Lou Taylor Pucci, Chris Pine, Piper Perabo ||   ||  
|-
!   | Case 39
| Christian Alvart || Renée Zellweger, Ian McShane, Adrian Laster ||   ||   
|-
!   | Chaw (film)|Chaw Jung Yu-mi, Yoon Je-moon, Uhm Tae-woong ||   ||  
|-
!   | The Chosen One
| Theodore Collatos || Arthur Collins, Carolina Monnerat, Sam Porretta ||   ||  
|- Children of the Corn
| Donald P. Borchers || David Anders, Kandyse McClure, Preston Bailey ||   ||  
|- The Collector
| Marcus Dunstan || Josh Stewart, Michael Reilly Burke, Andrea Roth ||   ||  
|-
!   | Contagio
| Steve Sessions || Luc Bernier, Isabelle Stephen, Tim Tanner ||   ||  
|-
!   | Creature of Darkness
| Mark Stouffer || Siena Goines, Kevin Alejandro, Sanoe Lake ||   || Science fiction horror 
|- Dark and Stormy Night
| Larry Blamire || Jennifer Blaire, Daniel Roebuck, Dan Conroy ||   || Comedy horror 
|- Dark Mirror
| Pablo Proenza || Lisa Vidal, David Chisum, Lupe Ontiveros ||   ||  
|-
!   | Dark Moon Rising
| Dana Mennie || Chris DiVecchio, Sid Haig, Lin Shaye ||   ||  
|-
!   | Dead by Dawn 2: The Return
| || ||   ||  
|-
!   | Deadly Little Christmas
| Novin Shakiba || Felissa Rose, Monique La Barr, Leah Grimsson, Jed Rowen ||   ||  
|-
!   | The Devils Tomb
| Jason Connery || Cuba Gooding Jr., Ray Winstone, Ron Perlman ||   ||  
|-
!   | Dead at the Box Office
| Shawn Stutler || Michael Allen Williams, Isaiah Robinson, Jennifer Popagain ||   ||  
|-
!   | Deadlands 2: Trapped
| Gary Ugarek || ||   ||  
|-
!   | The Descent 2 Jon Harris || Shauna Macdonald, Natalie Jackson Mendoza ||   ||  
|-
!   |  
| Jason Stutter || Raybon Kan, Jessica Grace Smith, Suze Tye ||   ||  
|-
!   | The Disturbed
| Conor McMahon || Carla McGlynn, Stephen Murray, Clyde Mowlds ||   ||  
|-
!   | Doghouse (film)|Doghouse
| Jake West || Emily Booth, Danny Dyer, Lee Ingleby ||   ||  
|- Dorian Gray Ben Barnes, Rebecca Hall, Ben Chaplin ||   ||  
|-
!   | Drag Me to Hell
| Sam Raimi || Alison Lohman, Justin Long, Lorna Raver ||   ||  
|-
!   | Drifter: Henry Lee Lucas Antonio Sabato, Kostas Sommer ||   ||  
|-
!   | Earth Day
| Mister Ooh-la-la || Daphne Danger, Elizabeth Myers, Adrian Salge, ||   ||  
|-
!   | Edges of Darkness
| Blaine Cade, Jason Horton || Alonzo F. Jones, Shamika Ann Franklin, Lee Perkins ||   ||  
|-
!   | Evil&nbsp;– In the Time of Heroes
| Yorgos Noussias || ||  ||  
|-
!   | Evilution Guillermo Diaz, James Duval ||   || Science fiction horror 
|-
!   | The Familiar
| Miles Hanon || Laura Spencer, Stephanie Brehms, Ben Hall ||   ||  
|-
!   | Family Demons
| Ursula Dabrowsky || ||  ||  
|- The Fear Chamber
| Kevin Carraway || Richard Tyson, Rhett Giles, Steven Williams ||   ||  
|- The Final
| Joey Stewart || Marc Donato, Jascha Washington, Whitney Hoy ||   ||  
|-
!   | The Final Destination
| David R. Ellis || Bobby Campo, Shantel VanSanten, Haley Webb ||   ||  
|-
!   | First 7th Night
| Herman Yau Lai-To || Michelle Ye, Gordon Lam Ka-Tung, Tony Ho Wah-Chiu ||   ||  
|-
!   | Flesh, TX
| Guy Crawford || Joe Estevez, Kathleen Benner, Jose Rosete ||   ||  
|-
!   | The Fourth Kind
| Olatunde Osunsanmi || Milla Jovovich, Elias Koteas, Will Patton ||   || Science fiction horror 
|- Freeway Killer
| John Murlowski || Michael Rooker, Eileen Dietz, Tyler Neitzel ||   ||  
|- Friday the 13th
| Marcus Nispel || Derek Mears, Jared Padalecki, Amanda Righetti ||   ||  
|-
!   | Georges Intervention
| J.T. Seaton || ||   ||  
|-
!   | Gnaw
| Gregory Mandry || Julia Vandoorne, Rachel Mitchem, Gary Faulkner ||   ||  
|-
!   | Grace (film)|Grace
| Paul Solet || Jordan Ladd, Stephen Park, Samantha Ferris ||   ||  
|-
!   | Gravestoned
| Michael McWillie || Eryn Brooke, Lar Park-Lincoln, Hope Latimer ||   ||  
|-
!   | Grave Danger
| Jim Haggerty || Cathy St. George, Vic Martino, Jonathan Holtzman ||   ||  
|-
!   | Grotesque (2009 film)|Grotesque
| Koji Shiraishi || Hiroaki Kawatsure, Tsugumi Nagasawa, Shigeo Ōsako ||   ||  
|-
!   | Growth (film)|Growth
| Gabriel Cowan || Brian Krause, Mircea Monroe, Richard Riehle ||   ||  
|-
!   | The Grudge 3
| Toby Wilkins || Shawnee Smith, Marina Sirtis, Johanna E. Braddy ||   ||  
|- Halloween II
| Rob Zombie || Tyler Mane, Scout Taylor-Compton, Malcolm McDowell ||   ||  
|-
!   | Hanger (film)|Hanger
| Ryan Nicholson || Nathan Dashwood, Wade Gibb, Alastair Gamble ||   ||  
|-
!   | The Haunting
| Elio Quiroga || Ana Torrent, Héctor Colomé, Julio Perillán ||   ||  
|-
!   | The Haunting in Connecticut
| Peter Cornwell || Amanda Crew, Virginia Madsen, Kyle Gallner ||   ||  
|-
!   | Haunting of Winchester House
| Mark Atkins || Lira Kellerman, Patty Roberts, Tomas Boykin ||   ||  
|-
!   | He
| Creep Creepersin || Ariauna Albright, Matt Turek, Marlina Germanova ||   || Direct-to-video 
|-
!   | Heartless (2009 film)|Heartless
| Philip Ridley || Joseph Mawle, Jim Sturgess, Clémence Poésy ||   ||
|-
!   | Hellhounds
| Rick Schroder || Amanda Brooks, Andrew Howard, Scott Elrod ||   ||  
|-
!   | Hellphone
| Joven Tan || Rainier Castillo, Dexter Doria, Basty Alcances ||   ||  
|-
!   | Hidden (2009 film)|Hidden
| Pål Øie || Anders Danielsen Lie, Karin Park, Marko Iversen Kanic ||   ||  
|-
!   | Higanjima Kim Tae-kyun Koji Yamamoto, Miori Takimoto ||    ||  
|-
!   | Horrid
| James Pronath || Melissa Jo Murphy, Charles Ramsey, Donn Kennedy ||   ||  
|-
!   | Hotel Darklight
| Alan Brennan, Ciaran Foy, Conor McMahon, Brian OToole, Ewan Petit, James Phelan, Kian Pitit, Dolores Rice, Paul Walker, Ian Walker || Susan Loughnane, Carla Mooney, Noel Aungier, Aisling Bodkin ||   ||
|-
!   | The House of the Devil
| Ti West || AJ Bowen, Tom Noonan, Mary Woronov ||   ||  
|-
!   | House of the Wolf Man
| Eben McGarr || Ron Chaney, Dustin Fitzsimons, Jeremie Loncka ||   ||
|-
!   | Humains
| Jacques-Olivier Melon || Philippe Nahon, Sara Forestier, Dominique Pinon ||   ||  
|-
!   | Hunger
| Steven Hentges || Lori Heuring, Lea Kohl, Linden Ashby ||   ||  
|-
!   | The Human Centipede (First Sequence)
| Tom Six || Akihiro Kitamura, Dieter Laser, Andreas Leupold, Ashley C. Williams ||    ||  
|-
!   | Humanimal (film)|Humanimal
| Francesc Morales || Cristobal Tapia-Montt, Francisco Gormaz, Felipe Avello, Cecilia Levi, Jimena Nuñez ||   ||  
|-
!   | Hurt (2009 film)|Hurt
| Barbara Stepansky || Jackson Rathbone, Melora Walters, William Mapother ||   ||
|-
!   | Hush (2009 film)|Hush William Ash, Christine Bottomley, Andreas Wisniewski ||   ||
|-
!   | Hydra
| Andrew Prendergast || Polly Shannon, William Gregory Lee, Texas Battle ||   ||  
|-
!   | In a Spiral State
| Ramzi Abed || Gidget Gein, Bianca Barnett, Elissa Dowling ||   ||  
|-
!   | Incest Death Squad
| Cory Udler || Elske McCain, Lloyd Kaufman, Scarlet Salem ||   ||  
|-
!   | Infestation (film)|Infestation
| Kyle Rankin || Diane Gaeta, Kinsey Packard, Brooke Nevin ||   ||  
|-
!   | Interplanetary
| Chance Shirley || Melissa Bush, Chuck Hartsell, Kyle Holman ||   ||  
|-
!   | Intruder
| Gregory Caiafa || Brian Ish, Alexandra Grossi, Arda Itez ||   ||  
|- Invitation Only
| Kevin Ko || Kristian Brodie, Maria Ozawa, Jerry Huang ||   ||  
|-
!   | Jennifers Body
| Karyn Kusama || Megan Fox, Amanda Seyfried, Johnny Simmons ||   ||  
|-
!   | La horde
| Yannick Dahan, Benjamin Rocher || Eriq Ebouaney, Aurélien Recoing, Alain Figlarz ||   ||  
|- Laid to Rest Robert Hall Kevin Gage ||   ||  
|-
!   | The Landlord
| Emily Hyde || ||   ||  
|-
!   | The Last Lovecraft: The Relic of Cthulhu
| Henry Saine || Barak Hardley, Edmund Lupinski, Sujata Day ||   ||  
|- The Last House on the Left
| Dennis Iliadis || Sara Paxton, Martha MacIsaac ||   ||  
|-
!   | Late Fee
| Carl Morano, John Carchietta || Stephanie Danielson, Georgia Kate Haege, Sawyer Novak ||   ||  
|-
!   | Lesbian Vampire Killers
| Phil Claydon || Vera Filatova, Silvia Colloca, Lucy Gaskell ||   || Comedy horror 
|- Live Evil
| Jay Woelfel || Ken Foree, Åsa Wallander, Mark Hengst ||   ||  
|-
!   | Love Blade
| Jason Rudy || Tiffany Arscott, Rae Wright ||   ||  
|- The Loved Ones
| Sean Byrne || Xavier Samuel, Jessica MacNamee, John Brumpton ||   ||  
|-
!   | Macabre (2010 film)|Macabre
| Timo Tjahjanto & Kimo Stamboel || ||    ||  
|-
!   | Malibu Shark Attack David Lister || Peta Wilson, Mungo McKay, Sonya Salomaa ||   ||  
|-
!   | Maneater (2009 film)|Maneater
| Michael Emanuel || Robert R. Shafer, Sean Cain, Conrad Janis ||   ||  
|-
!   | Meadowood
| Robert Kurtzman || ||   ||  
|-
!   | Mega Shark vs. Giant Octopus
| Jack Perez || Lorenzo Lamas, Deborah Gibson, Vic Chao ||   ||  
|- Melancholie der Engel
| Marian Dora || Bianca Schneider, Martina Adora, Ulli Lommel ||   ||  
|-
!   | Mental Scars
| Mischa Perez || John Gearries, Sonny Landham, Theresa Alexandria ||   ||  
|-
!   | Methodic
| Chris R. Notarile|| Brandon Slagle, Niki Notarile, Tony Dadika, Roberto Lombardi||   ||   
|-
!   | Midnight Movie
| Jack Messitt || Rebekah Brandes, Mandell Maughan, Stan Ellsworth ||   || Direct-to-video 
|- Life Blood
| Ron Carlson || Scout Taylor-Compton, Danny Woodburn, Jennifer Tung ||   ||  
|-
!   | Must Love Death
| Andreas Schaap || Sami Loris, Manon Kahle, Lucie Pohl ||   ||  
|-
!   | Mutants (2009 film)|Mutants
| David Morlet || Dida Diafat, Hélène de Fougerolles, Nicolas Briançon ||   ||   
|-
!   | Mutilation Mile
| Ron Atkins || Lawrence Bucher, Susan Bull, Toni Ferrari ||   ||  
|-
!   | My Bloody Valentine 3D Tom Atkins, Kerr Smith ||   ||  
|-
!   | My Super Psycho Sweet 16
| Jacob Gentry || Chris Zylka, Juliana Guill, Lauren McKnight ||   ||  
|-
!   | My Wife Is a Vampire
| Paul Marrin || Teyama Alkamli, Morgan Jarvis, James Emmerson ||   ||  
|-
!   | Necrosis (film)|Necrosis
| Jason Robert Stephens || Penny Drake, George Stults, Robert Michael Ryan ||   ||  
|-
!   | Necromantia
| Pearry Teo || Chad Grimes, Layton Matthews, Santiago Craig ||   ||  
|-
!   | Neighbor
| Robert Angelo Masciantonio || America Olivo, Joe Aniska, Amy Rutledge ||   ||  
|-
!   | Neowolf
| Yvan Gauthier || Veronica Cartwright, Agim Kaba, Heidi Johanningmeier ||   ||  
|-
!   | Never on Sunday
| Francis Xavier || Elissa Dowling, Mari Cielo Pajares, Joe Ochman ||   ||  
|-
!   | Nine Dead
| Chris Shadley || Melissa Joan Hart, William Lee Scott, Lucille Soong ||   ||  
|-
!   | Nine Miles Down
| Anthony Waller || Kate Nauta, Meredith Ostrom, Adrian Paul ||   ||
|-
!   | Nun of That
| Richard Griffin || Sarah Nicklin, Alexandra Cipolla, Debbie Rochon ||   ||  
|-
!   | Occult (2009 film)|Occult
| Kôji Shiraishi || Mika Azuma, Horiken, Kôen Kondô ||   || Lovecraftian horror|Lovecraftian-derived 
|-
!   | Open Graves
| Álvaro de Armiñán || Mike Vogel, Eliza Dushku, Iman Nazemzadeh ||    ||  
|-
!   | Oral Fixation
| Jake Cashill || Emily Parker, Kerry Aissa, Chris Kies ||   ||  
|-
!   | Orphan (film)|Orphan
| Jaume Collet-Serra || Vera Farmiga, Peter Sarsgaard ||     ||  
|-
!   | Pandorum
| Christian Alvart || Dennis Quaid, Antje Traue, Wotan Wilke Möhring ||   ||  
|-
!   | Paranormal Entity
| Shane Van Dyke || Fia Perera, Norman Saleet, Erin Marie Hogan ||   || Mockbuster 
|- Patient X
| Yam Laranas || Richard Gutierrez, Cristine Reyes, TJ Trinida ||   ||  
|-
!   | Penance (film)|Penance
| Jake Kennedy || Marieh Delfino, Tony Todd, Michael Rooker ||   ||  
|-
!   | Perkins 14
| Craig Singer || Roxanna Ravenor, Michale Graves, Katherine Pawlak ||   ||  
|-
!   | The Pit and the Pendulum
| David DeCoteau || Amy Paffrath, Lorielle New, Tom Sandoval ||   ||  
|-
!   | Poker Run
| Julian Higgins || ||   ||  
|-
!   | Postman does not knock three times
| Hassan Fathi || Ali Nasirian, Pantea Bahram, Baran Kosari, Mohammad-Reza Foroutan ||   ||  
|-
!   | Ravage the Scream Queen
| Bill Zebub || Nikki Sebastian, Jordana Leigh, Elyse Cheri ||   ||  
|-
!   | Reborn Chris Cox ||   ||  
|-
!   | The Reeds
| Nick Cohen || Anna Brewster, Scarlett Alice Johnson, Will Mellor ||   ||  
|-
!   | Rise of the Gargoyles
| Bill Corcoran || Eric Balfour, Tanya Clarke, Ifan Huw Dafydd ||   ||  
|-
!   | Rotkappchen: The Blood of Red Riding Hood
| Harry Sparks || Stefanie Geils, Chris OBrocki, Angelina Leigh ||   ||  
|-
!   | Run! Bitch Run!
| Joseph Guzman || Cheryl Lyone, Peter Tahoe, Christina DeRosa ||   ||  
|-
!   | The Sacred
| Jose Zambrano Cassella || ||   ||  
|-
!   | Sand Serpents
| Jeff Renfroe || Jason Gedrick, Elias Toufexis, Sebastian Knapp ||   || Television film 
|-
!   | Satanic Panic
| Marc Selz || Victoria Vukovic Bradley, Melissa Chirello-Wood, Rosa Isela Frausto ||   ||  
|-
!   | Saw VI
| Kevin Greutert || Tobin Bell, Shawnee Smith, Costas Mandylor, Betsy Russell, Tanedra Howard ||   ||  
|-
!   | Scare Zone
| Jon Binkowski || Cynthia Murell, Michele Feren, Laura Alexandra Ramos ||   ||  
|- Sea of Dust
| Scott Bunt || Ingrid Pitt, Stuart Rudin, Tom Savini ||   ||  
|-
!   | Shadow
| Federico Zampaglione || Jake Muxworthy, Chris Coppola, Karina Testa ||   ||  
|-
!   | Shake, Rattle & Roll XI
| Jessell Monteverde, Rico Gutierrez, Don Michael Perez || Ruffa Gutierrez, Maja Salvador, Iya Villania ||   ||  
|-
!   | Shattered Lives
| Carl Lindbergh || Lindsey Leino, Skyler Caleb, Ellyse Deanna ||   ||  
|-
!   | Shes Crushed
| Patrick Johnson || Henrick Nolen, Caitlin Wehrle, Natalie Dickinson ||   ||  
|-
!   | Shiver
| Bob Shaye, Michael Lynne || ||   ||  
|-
!   | The Shock Labyrinth 3D
| Takashi Shimizu || Yūya Yagira, Misako Renbutsu, Ryo Katsuji ||   ||  
|- The Shrine
| Jon Knautz || Aaron Ashmore, Trevor Matthews, Cindy Sampson ||   ||  
|-
!   | The Shortcut
| Nicholaus Goossen || Shannon Woodward, Katrina Bowden, Wendy Anderson ||   ||
|-
!   | Silent Night, Zombie Night Vernon Wells, Lew Temple, Felissa Rose ||   ||  
|-
!   | Skull Heads Steve Kramer ||   ||  
|-
!   | Slaughtered Steven ODonnell, James Kerley, Cassandra Swaby ||   ||  
|-
!   | Smash Cut
| Lee Demarbre || David Hess, Sasha Grey, Jesse Buck ||   ||  
|-
!   | Someones Knocking at the Door
| Chad Ferrin || Noel Segan, Ezra Buzzington, Elina Madison ||   ||  
|-
!   | Sorority Row
| Stewart Hendler || Rumer Willis, Audrina Patridge, Leah Pipes & Margo Harshman ||   || Remake 
|-
!   | Spirit Camp
| Kerry Beyer || Marco Perella, Denise Williamson, Amy Morris ||   ||  
|-
!   | Splice (film)|Splice
| Vincenzo Natali || Sarah Polley, Adrien Brody, David Hewlett ||    ||   
|-
!   | Staunton Hill
| Cameron Romero || Kathy Lamkin, Cristen Coppen, David Rountree ||   || Direct-to-video 
|-
!   | Suck (film)|Suck
| Rob Stefaniuk || Alice Cooper, Jessica Paré, Iggy Pop ||   ||  
|-
!   | Summers Blood|Summers Moon
| Lee Demarbre || Ashley Greene, Stephen McHattie, Peter Mooney, Barbara Niven ||   || Direct-to-video 
|-
!   | Survival of the Dead
| George A. Romero || Alan Van Sprang, Kenneth Welsh, Kathleen Munroe ||    ||  
|-
!   | Surviving Evil
| Terence Daw || Billy Zane, Alexis Georgiou, Louise Bamber ||    ||  
|-
!   | Sutures
| Tammi Sutton || Carlos Lauchu, Azie Tesfai, Allison Lange ||   ||  
|-
!   | Sweatshop
| Stacy Davidson || Danielle Jones, Melanie Donihoo, Naika Malveaux ||   ||  
|-
!   | Taintlight
| Chris Seaver || Jesse Ames, Andrew Baltes, Miranda Bonetwig ||   ||  
|-
!   | Tannöd
| Bettina Oberli || Julia Jentsch, Monica Bleibtreu, Vitus Zeplichal ||   ||  
|-
!   | Tell-Tale (film)|Tell-Tale
| Michael Cuesta || Ulrich Thomsen, Josh Lucas, Dallas Roberts ||    ||  
|-
!   | Terror Overload&nbsp;– Tales from Satans Truck Stop
| || Scarlet Salem, Joe Knetter ||   ||  
|- The Telling
| Nicholas Carpenter || Bridget Marquardt, Holly Madison, Sara Jean Underwood ||   ||  
|- The Crypt
| Craig McMahon || Sarah Oh, Cristen Irene, Joanna Ke ||   || Direct-to-video 
|- The Thaw
| Mark A. Lewis || Val Kilmer, Martha McIsaac, Aaron Ashmore, Kyle Schmid ||   ||  
|-
!   | Thirst (2009 film)|Thirst
| Park Chan-wook || Song Kang-ho, Kim Ok-bin ||   ||  
|-
!   | Thirsty
| Andrew Kasch || Tiffany Shepis ||   ||  
|-
!   | Tony (2009 film)|Tony
| Gerard Johnson || Peter Ferdinando, Ricky Grover, George Russo ||   ||  
|-
!   | Tormented (2009 British film)|Tormented
| Jon Wright || Calvin Dean, Tuppence Middleton, Dimitri Leonidas ||   ||  
|-
!   | The Tortured
| Robert Lieberman || Jesse Metcalfe, Erika Christensen ||   ||  
|-
!   | Transylmania
| David Hillenbrand & Scott Hillenbrand || Patrick Cavanaugh, James DeBello, Paul H. Kim, Tony Denman, Jennifer Lyons ||   ||  
|-
!   | Triangle (2009 British film)|Triangle Christopher Smith || Melissa George, Liam Hemsworth, Holly Marie Combs ||   ||  
|-
!   | Twilight Vamps
| Fred Olen Ray || Jayden Cole, Christine Nguyen, Brandin Rackley ||   ||  
|-
!   | The Uh! Oh! Show
| Herschell Gordon Lewis || Brooke McCarter, Bruce Blauer, Krista Grotte ||   ||  
|- The Unborn
| David S. Goyer || Odette Yustman, Meagan Good ||   || Remake 
|-
!   | Uncharted
| Frank Nunez || Demetrius Navarro, Shana Montanez, Erlinda Orozco ||   ||  
|-
!   |  
| Patrick Tatopoulos || Michael Sheen, Bill Nighy, Rhona Mitra ||   ||  
|-
!   | The Undying
| Steven Peros || Robin Weigert, Anthony Carrigan ||   ||  
|- The Uninvited
| Charles Guard || Elizabeth Banks, Arielle Kebbel, Emily Browning ||   ||  
|-
!   |  
| Eric Bross || Agnes Bruckner, Arjay Smith, Beau Billingslea ||   ||  
|-
!   | Vampire Girl vs. Frankenstein Girl
| Naoyuki Tomomatsu, Yoshihiro Nishimura || Cay Izumi, Takumi Saito, Yukie Kawamura ||   ||  
|-
!   | Vampiro
| Jorge Ramirez Rivera || Damian Chapa, Leslie Garza, Vida Harlow ||   ||  
|-
!   | Vertige
| Abel Ferry || Fanny Valette, Johan Libéreau, Maud Wyler ||   ||  
|-
!   | Wake Wood
| David Keating || Aidan Gillen, Eva Birthistle, Timothy Spall ||   ||  
|-
!   | Witchmaster General
| Jim Haggerty || Tatyana Kot, Bud Stafford, Suzi Lorraine ||   ||  
|-
!   | Wolvesbayne
| Griff Furst || Jeremy London, Christy Carlson Romano ||   || Direct-to-video 
|-
!   | Won Ton Baby!
| James Morgart || Suzi Lorraine, Gunnar Hansen, Debbie Rochon ||   ||  
|-
!   |   Janet Montgomery ||   ||  
|-
!   | Zombies & Cigarettes
| Rafael Martinez, Iñaki San Román || Aroa Gimeno, Mónica Miranda, María San Miguel ||   ||  
|-
!   | Zombieland
| Ruben Fleischer || Woody Harrelson, Jesse Eisenberg, Emma Stone, Abigail Breslin ||   || Comedy horror 
|-
!   | Zone of the Dead
| Milan Konjević, Milan Todorović || ||     ||  
|}

==References==
 

 
 
 

 
 
 
 