Double X: The Name of the Game
 
{{Infobox film|
  name        =Double X: The Name of the Game|
  image       = |
  caption     = |
  writer      =  (Novel)  |
  starring    =Norman Wisdom William Katt Bernard Hill Gemma Craven Simon Ward Chloë Annett |
  director    =  |
  producer    =Noel Cronin   Edward Joffe|
  music       = |
  distributor =  (DVD)|
  released    =United Kingdom   June, 1992|
  runtime     = 95 minutes|
  gross       = |
  language    =English|
  budget      =|
}} thriller film. The screenplay by Shani S. Grewal  is based in part on Vengeance, a novel by David Fleming. 

The film features Norman Wisdom, William Katt, Gemma Craven, Simon Ward, Bernard Hill, & Chloë Annett

==Synopsis==
Expert safecracker Arthur Clutten (Norman Wisdom|Wisdom) masterminds heists for a criminal syndicate he belongs to. But after witnessing the brutal methods of persuasion being meted out by gang leader, Ignatius Smith (Bernard Hill|Hill) Clutten decides to quit. But he realises the gang would sooner see him killed than quitting. After stealing some documents incriminating Smith and his boss, Edward Ross (Simon Ward|Ward), Clutten then puts his family in hiding and goes on the run...but his daughter (Chloë Annett|Annett) has been kidnapped by the gang and a hitman (William Katt|Katt) has been hired and is close to finding Clutten.

==Cast==
*Norman Wisdom as Arthur Clutten
:
*William Katt as Michael Cooper
:
*Gemma Craven as Jenny Eskridge
:
*Simon Ward as Edward Ross
:
*Bernard Hill as Ignatious Iggy Smith
:
*Chloë Annett as Sarah Clutten

===Cast Notes===
*First film role for Norman Wisdom in 20 years.

*Chloë Annett makes her feature film début.

*Last film of Vladek Sheybal.

==Locations==
*Filmed in the UK at the following locations:

::Monkey Island, Bray, Berkshire, England

::Portpatrick, Dumfries and Galloway, Scotland

::Stranraer, Dumfries and Galloway, Scotland

==DVD Release== Region 2 DVD on 25 February 2007 in the UK under "The BEST of BRITISH Collection" tag by  .

==References==
 

==External links==
*  at  
* 
* 
*  

 
 
 
 
 