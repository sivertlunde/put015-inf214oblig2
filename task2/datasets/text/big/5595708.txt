The Eight Diagram Pole Fighter
 
 
 
{{Infobox film
| name           = The Eight Diagram Pole Fighter
| image          = Eight Diagram Pole Fighter movie poster.jpg
| caption        = The Hong Kong movie poster.
| film name = {{Film name|traditional=五郎八卦棍
 |simplified=五郎八卦棍 pinyin = Wǔ Láng Bā Guà Gùn  jyutping = Ng 5  Long 4  Baat 3  Gwaa 3  Gwan 3 }}
| director       = Lau Kar-leung
| producer       = Mona Fong   Run Me Shaw   Run Run Shaw	
| writer         = Lau Kar-leung   Kuang Ni
| starring       = Gordon Liu   Alexander Fu   Kara Hui  Ko Fei
| music          = Stephen Shing
| distributor    = Shaw Brothers Studio
| released       =  
| runtime        = 98 minutes
| country        = Hong Kong
| language       = Cantonese
}} 1983 Hong Kong film by Shaw Brothers, directed by Lau Kar-leung and starring Gordon Liu and Alexander Fu. It was released as The Invincible Pole Fighters outside of Hong Kong and Invincible Pole Fighter in North America.

Alexander Fu died in a car accident before the filming of The Eight Diagram Pole Fighter was finished and does not appear in the final showdown as originally written in the script. 

The film is based on the Generals of the Yang Family (Yeung family in Cantonese) legends.

== Plot == Buddhist monk. spears to eight diagram pole fighting technique. When he finally appeared to have put his anger and past behind him, news broke that the Khitans had captured his younger sister, Yeung Baat-mui, who was looking for him. Now he must break Buddhist vows (including not kill and not be bothered by worldly affairs) to save Baat-mui and exact his revenge.

==Cast==
: Note: The characters names are in Cantonese romanisation.
{|class="wikitable" 
|-
!Cast
!Role
!Description
|- Lily Li Se Choi-fa || "Taai-gwan", Yeung Yips wife
|- Wong Yue Yeung Ping || "Daai-long", Yeung Yips 1st son
|- Lau Kar-wing Yeung Ding || "Yi-long", Yeung Yips 2nd son
|- Mak Tak-law Yeung On || "Saam-long", Yeung Yips 3rd son
|- Hsiao Ho Hsiao Ho Yeung Fai || "Sei-long", Yeung Yips 4th son
|- Gordon Liu Yeung Dak || "Ng-long", Yeung Yips 5th son
|- Alexander Fu Yeung Chiu || "Luk-long", Yeung Yips 6th son
|- Cheung Chin-pang Yeung Zi || "Chat-long", Yeung Yips 7th son
|- Kara Hui Yeung Kei || "Baat-mui", Yeung Yips daughter and 8th child
|- Yeung Jing-jing Yeung Ying || "Gau-mui", Yeung Yips daughter and 9th child
|- Lam Hak-ming Pun Mei || Song Dynasty general
|- Wang Lung-wei || Ye-leut Lin || Liao Dynasty prince
|- Chu Tiet-woh || Gun Kwai || Liao Dynasty general
|- Ko Fei || || abbot of the Ching-leung Monastery
|- Ching Chu || Master Ji-hung || senior monk at the Ching-leung Monastery
|- Lau Kar-leung || || hunter
|}

==Awards and nominations==
1985 - 4th Hong Kong Film Awards Best Action Choreography

==External links==
*   at Hong Kong Cinemagic
*  
*  

 
 

==References==
 

 
 
 
 
 
 
 
 
 