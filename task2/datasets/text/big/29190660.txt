David Copperfield (1913 film)
 
 
  David Copperfield by Charles Dickens. It is the second-oldest known film adaptation of the novel.

The film was made by the Hepworth Manufacturing Corporation, was produced by   website]  In the United Kingdom it was released in August 1913, and in the United States it was released on 1&nbsp;December 1913. It ran at 67 minutes on seven reels. 

A review of the film in The Dickensian said:
 
"It occupies close upon two hours to exhibit and is divided into six parts, telling the story of Davids life from Blunderstone to his happy union with Agnes. Obviously the part of David is enacted by three different actors, Master Eric Desmond, Mr Len Bethel and Mr Kenneth Ware. The first-named being one of the cleverest child actors we have seen. We have no space to speak of all the characters presented. Each and all of them were Dickens creations to the life and not mere exaggerations as is often the case. 
 
The film not only includes all of the most prominent characters and all the necessary incidents of the book to make the story intelligible to the lay reader, but they have been enacted in the actual places in which the novelist laid them." 
 

The cast included Reginald Sheffield - billed as Eric Desmond  (David Copperfield as a boy), Len Bethel (David Copperfield as a youth), Kenneth Ware (David Copperfield as a Man), Edna May (Emly as a Child), Amy Verity (Emly as a Woman), Alma Taylor (Dora Spenlow), H Collins (Mr Micawber), Miss West (Mrs Micawber), Jack Hulcup (Uriah Heep), Jamie Darling (Daniel Peggotty), and Cecil Mannering (Steerforth).  Dickens Before Sound, British Film Institute booklet, pgs 22-25 

==References==
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 

 