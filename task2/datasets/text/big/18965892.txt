Lymelife
{{Infobox film
| name = Lymelife
| image = Lymelife ver2.jpg
| image_size = 215px
| alt = 
| caption = Promotional poster
| director = Derick Martini
| producer =  
| writer = Derick Martini Steven Martini
| starring = Rory Culkin Alec Baldwin Emma Roberts
| music = Steven Martini The Spaceship Martini
| cinematography = Frank Godwin
| editing = Derick Martini Steven Martini Mark Yoshikawa
| distributor = Screen Media Films
| released =  
| runtime = 94 minutes  
| country = United States
| language = English
| budget = $1.5 million
}} independent comedy-drama film written by brothers Derick Martini and Steven Martini, and directed by Derick Martini, depicting aspects of their life in 1970s Long Island from the perspective of a teenager. The film stars Alec Baldwin, Rory Culkin, and Emma Roberts.  Martin Scorsese served as an executive producer.

The film debuted at the 2008 Toronto International Film Festival,  in September 2008 and won the International Federation of Film Critics Award (FIPRESCI).  After its theatrical release in 2009, writer director Derick Martini was nominated for a Gotham Award for Breakthrough Director.

==Plot==
Set in 1979 Syosset, Long Island, New York, Lymelife follows two families, the Bartletts and the Braggs, who crumble when tangled relationships, real estate problems, and Lyme disease converge in the heart of suburbia. 15-year-old Scott Bartlett is a gentle boy, radically different from his blustery father Mickey and mother Brenda. An outbreak of Lyme disease, as well as the accompanying paranoia, hits their community hard.

When the Bartletts neighbor, Charlie Bragg, is diagnosed with the illness, Charlie is unable to work and his wife Melissa must keep the income flowing herself. She is hired by Mickey, a friendly favor motivated by lust. Mickeys history of philandering is one of the many things upsetting Brenda. Scott has been in love with the Braggs one year-older daughter Adrianna for all his life; she is starting to return his interest.

Charlie spends days hiding in his basement, while his wife believes he is in Manhattan on job interviews. He is obsessed with hunting deer. Scott and Charlie have a good relationship, one of the only ones Charlie is able to maintain throughout his illness. Things heat up when Jimmy, Scotts older brother, comes home from the army on their moms birthday. Brenda leaves early from Jimmys going-away party when it is clear that there is a relationship between Mickey and Melissa. Jimmy and Mickey have a confrontation.

Scott learns of the affair and confronts his mother. Adrianna helps him through this, but shuns him after a rumor spread from a lie he tells a friend. Brenda kicks Mickey out of the house and is once again able to act the role of an effective parent. Charlie also confronts Mickey after he inadvertently witnesses the affair; when his wife finds out that he has been letting her earn the familys keep, she packs to leave. Scott and Adrianna reconnect and lose their virginity to each other. Brenda and Mickey also reconcile that night.

The movie ends with a gunshot, revealing that Charlie shot Mickey.

==Cast==
* Rory Culkin as Scott Bartlett
* Alec Baldwin as Mickey Bartlett
* Emma Roberts as Adrianna Bragg
* Jill Hennessy as Brenda Bartlett
* Kieran Culkin as Jimmy Bartlett
* Timothy Hutton as Charlie Bragg
* Cynthia Nixon as Melissa Bragg
* Logan Huffman as Blaze Salado
* Brandon Thane Wilson as Stuart
* Adam Scarimbolo as Todd OLeary

;Uncredited
* Derick Martini as Photographer
* Steven Martini as Taxi driver
* Matthew Martini as Jimmys friend

==Production==
===Development=== Macaulay and Kieran Culkin|Kieran, with Kieran and his younger brother Rory Culkin going on to portray the Martini brothers in this film. It was during this period that Steven met his first true love, Adrianna, whose father had Lyme disease. This relationship is the basis of the love story between Rory Culkin and Emma Roberts in the film.

===Budget===
In early 2005, the film was plagued with insufficient funds and was shut down on the eve of principal photography. The funds to make the film were not raised until 2008. Despite the 3-year hiatus, nearly the entire original cast made time in their schedules to participate in the film. The total budget for the film was $1.5 million. The shooting schedule was a scant 22 days, made only more difficult to complete due to the directors insistence on using older, larger and heavier anamorphic lenses.

===Casting=== A Streetcar Named Desire, and it being an "eye opening" experience for him. Jill Hennessey was offered the role of Brenda Bartlett after Jennifer Jason Leigh left the film and her replacement, Anne Heche, insisted on wearing a wig. Martini chose to circumvent the conventional casting process, specifically "cold readings", and instead cast the actors based on viewing their previous work. Emma Roberts was the only main actor who wanted to audition for the director despite not having to.

===Shooting locations=== Montclair High School in Montclair, New Jersey.  Other locations were nearby, also in New Jersey.

==Release==
===Critical reception===
The film premiered at the 2008 Toronto International Film Festival and won the International Federation of Film Critics award (FIPRESCI). It landed on several of 2009s critics "Top Ten Lists" and was praised for its performances and directorial style, but criticized for its storys familiarity. The film received a 63% "fresh" rating on Rotten Tomatoes; the consensus states: "Lymelife features sharp performances, but the story lacks the emotional depth or focus worthy of its talented cast." 

===Box office===
The film began its North American theatrical release in April 2009. Initially, Lymelife was only shown on screens in New York and Los Angeles, but eventually expanded to screens in almost every major and minor U.S city. Foreign release was more fruitful, opening on screens in nearly every major foreign territory, which is rare for a smaller American independent. The film grossed $421,307 in the United States and an additional $1,104,938 internationally for a total worldwide gross of $1,526,245.    DVD, VOD and Cable sales brought the films gross up to $4,526,245, more than doubling its production budget

==References==
{{Reflist|refs=
   
   
   
   
}}

==External links==
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 