Three Faces East (1930 film)
{{infobox film
| name           = Three Faces East
| image          = Three Faces East 1930.jpg
| image_size     = 225px
| caption        = theatrical releaseposter
| director       = Roy Del Ruth
| producer       = Daryl Zanuck
| writer         =  Arthur Caesar Oliver H. P. Garrett
| based on       =  
| starring       = Constance Bennett
| music          = Paul Lamkoff
| cinematography = Barney McGill William Holmes
| distributor    = Warner Bros.
| released       =  
| runtime        =
| country        = United States
| language       = English
}}
 silent in 1926. 

==Plot==
The story takes place during World War I. The action opens on a French battlefield. After meeting with German spy Schiller Blacher (Erich von Stroheim), Z-1 (Constance Bennett) is sent on a mission to England. The action then moves into the London home of Sir Winston Chamberlain (William Holden- no relation to the 1950s star of the same name). Sir Winston does not know that that his supposedly faithful butler, Vardar, is actually Blacher. When Z-1, as Frances Hawtree, arrives at the home, Vardar, who is in love with her, believes her to be a loyal German agent, but things turn out otherwise when she prevents him from sending a stolen code back to Germany and thus reveals her true allegiance.

==Preservation==
The film survived complete. It was transferred unto 16mm film by Associated Artists Productions in the 1950s and shown on television. A 16mm copy is housed at the Wisconsin Center for Film & Theater Research.   Another print exists at the Library of Congress. 

The film is now available on video-on-demand from WB Archive, WB Shop and Amazon. 

==Cast==
*Constance Bennett as Frances Hawtree/Z-1
*Eric von Stroheim as Valdar/Schiller Blecher
*Anthony Bushell as Captain Arthur Chamberlain William Courtenay as Mr. Yates
*Crauford Kent as General Hewlett Charlotte Walker as Lady Catherine Chamberlain
*William Holden as Sir Winston Chamberlain Ullrich Haupt as Colonel
*Paul Panzer as "Kirsch" the Decoy
*Wilhelm von Brincken as Captain Kugler

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 