Alfie (1966 film)
 
 
 
{{Infobox film
| name = Alfie
| image = Alfie_original.jpg
| image_size = 220px
| border = yes
| alt = 
| caption = Original release poster
| director = Lewis Gilbert	
| producer = Lewis Gilbert
| screenplay = Bill Naughton
| based on =  	
| starring = Michael Caine Shelley Winters Millicent Martin Vivien Merchant Jane Asher Julia Foster
| music = Sonny Rollins
| cinematography = Otto Heller
| editing = Thelma Connell
| distributor = Paramount Pictures
| released =  
| runtime = 113 minutes  
| country = United Kingdom
| language = English
| budget = $800,000 
| gross = $18,871,300 
}} romantic comedy-drama adaptation by Bill Naughton of his own novel and play of the same name. The film was released by Paramount Pictures.
 uncaring behaviour and his loneliness. He cheats on numerous women, and despite his charm towards women, he treats them with disrespect and refers to them as "it", using them for sex and for domestic purposes. Alfie frequently breaks the fourth wall by speaking directly to the camera narrating and justifying his actions. His words often contrast with or totally contradict his actions.

This was the first film to receive the "suggested for mature audiences" classification by the Motion Picture Association of America in the United States,   PG rating.

The film had its World Premiere at the Plaza Theatre in the West End of London on 24 March 1966.

==Plot==
The film begins with Alfie Elkins (Michael Caine) ending a relationship with a married woman, Siddie (Millicent Martin), and getting the woman from another of his affairs, Gilda (Julia Foster) pregnant. The film then follows his life for a few years, documenting events that lead to the characters emotional growth, starting with the birth of his son, but his inability to commit to his childs mother leads to her marrying a bus conductor.
 hereditary diseases, tubercular shadows on his lungs; and this, combined with being banned from seeing his son, leads him to have a brief mental breakdown.

Alfie spends time in a convalescent home, where he meets Harry, who confronts him about his delusion that he is doing no harm, and Lily, Harrys wife, whom he gets pregnant in a one-night stand. The ensuing abortion is a turning point for the character, and the only time other than his passing out/breakdown where he exhibits real emotion: collapsing in tears at seeing the aborted fetus.

He meets Ruby, an older, voluptuous, affluent, and promiscuous American, while freelancing taking holiday photos of tourists near the Tower of London. Later, as a car-hire chauffeur, he picks up a young hitchhiker, Annie from Sheffield, looking to make a fresh start in London, who moves in with him. She proves preoccupied with a love left behind, scrubbing Alfies floor, doing his laundry, and preparing his meals to compensate. He grows resentful of the relationship and drives her out with an angry outburst, later regretting it.

Alfie decides to change his non-committal ways and settle down, albeit with the rich Ruby. The day he chooses to suggest this to her, he finds a younger man in her bed, leaving him on his own and disheartened at the films end and wondering, "Whats it all about? You know what I mean."

==Cast==
* Michael Caine as Alfred "Alfie" Elkins
* Shelley Winters as Ruby
* Millicent Martin as Siddie
* Vivien Merchant as Lily Clamacraft
* Jane Asher as Annie
* Julia Foster as Gilda
* Shirley Anne Field as Carla
* Eleanor Bron as the Doctor
* Denholm Elliott as the Abortionist
* Alfie Bass as Harry Clamacraft
* Graham Stark as Humphrey
* Murray Melvin as Nat
* Sydney Tafler as Frank
* Queenie Watts as the	Blonde Pub Singer (uncredited)

==Production==
The film is unusual in that it has no opening credits and the end credits feature photos of the principal actors, as well as of the main technical crew, including director Gilbert and cameraman Otto Heller. It was shot at   which is seen at the beginning and end of the film where the title character walks into the distance accompanied by a stray dog    and Tower Bridge which is the backdrop for the photography scene with Shelley Winters.  . Retrieved 6 November 2013 
Several well-known actors, including Richard Harris, Laurence Harvey, James Booth and Anthony Newley turned down the title role due to the then taboo subject matter, which was experimented with in both American and British films. Despite having played "Alfie" on Broadway, Terence Stamp categorically declined to reprise the role on film, so he and casting agents approached his good friend and then roommate Michael Caine: not one to then snub a role about a common man, Caine agreed to do it. He won huge acclaim for the breakthrough role of his career and continued to land better parts.

==Music==
 
The original film soundtrack featured jazz saxophonist Sonny Rollins with local musicians from London including Stan Tracey on piano, who improvised "Little Malcolm Loves His Dad" (although never credited), Rick Laird on bass, Phil Seamen on drums, Ronnie Scott on tenor sax. The released soundtrack album, recorded back in the States with orchestration by Oliver Nelson, featured Rollins, but with other musicians.

The title song, "Alfie (song)|Alfie", written by Burt Bacharach and Hal David, was sung by Cher over the films closing credits.  It became a hit for British singer Cilla Black (whose version was used for the original British soundtrack), and for Madeline Eastman and Dionne Warwick. Numerous jazz musicians have covered it and it has become a jazz standard.

==Reception==
Alfie garnered critical acclaim, currently holding a 100% "fresh" rating on review aggregator Rotten Tomatoes. 

==Awards and recognition== Special Jury Best Actor Best Actress Best Song Best Picture Best Writing (Screenplay Based on Material from Another Medium).

It was the second-most popular film at the British box office in 1966, after Thunderball (film)|Thunderball. 

In 2004, Total Film magazine named it the 48th greatest British film of all time.   

==Legacy== 2004 remake starred Jude Law in the title role.

===References in popular culture===
* Much dialogue from the film was sampled by the band Carter USM for their 1991 album 30 Something.
* The LP, "Nino Tempos Rock N Roll Beach Party" (1956 Liberty Records … LRP3023) can be seen hanging in Alfies apartment in several key scenes.
* New York post-hardcore band Polar Bear Club references "Alfie Elkins 66" in their song "Drifting Thing" off their 2009 release Chasing Hamburg.
* The soundtrack to Austin Powers in Goldmember (in which Caine co-stars) contains a song entitled "Alfie (Whats It All About Austin)". This song is a cover of the original films title song, with all occurrences of "Alfie" replaced with "Austin". The Divine Becoming More Like Alfie, which samples its opening dialogue in its introduction.

==See also==
* BFI Top 100 British films

==References==
 

==External links==
*  
*  
*  , Screenshots from the film

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 