A Chinese Ghost Story III
 
 
 
{{Infobox film
| name           = A Chinese Ghost Story III
| image          = ACGS3 Poster HK.gif
| caption        = Film poster
| director       = Ching Siu-Tung
| producer       = Tsui Hark
| writer         =
| starring       = Tony Leung Chiu-Wai Joey Wang Jacky Cheung Nina Li Chi
| music          =
| cinematography =
| editing        =
| distributor    =
| released       = 1991
| runtime        =
| country        = Hong Kong
| language       = Cantonese
| budget         =
}}
A Chinese Ghost Story III (Sinnui Yauwan III: Do Do Do) is a 1991 Hong Kong romantic comedy-horror film directed by Ching Siu-Tung and produced by Tsui Hark. It is the sequel to A Chinese Ghost Story and A Chinese Ghost Story II.

Though technically a distant sequel, the plot is more of a retelling of the original A Chinese Ghost Story. Joey Wong reprises her role as a ghostly beauty bound in servitude to the Tree Demon. The Tree Demons seal, as cast by the monk Yan (Wu Ma) in the original film, only lasts for 100 years. Now 100 years later, the Tree Demon awakens.

==Plot==
Two Buddhist monks, the master Bai Yun and his inept disciple Shi Fang are transporting a golden idol of Buddha. On their travels they meet the relatively honorable mercenary Yin. Beset by thieves and neer-do-wells in the villages, the monks opt to spend the night at the local temple, which is none other than the Orchid Temple of the first film. Over the next few nights, Shi Fang is visited by Lotus and the two become fond of each other.

However, Butterfly becomes suspicious of her sister Lotus. Being rivals for the favor of the Tree Demon, Butterfly plots to capture the monk and expose her sisters betrayal. When the Tree Demon attempts to take Shi Fang, the master intervenes and uses a spell to transport his disciple to safety. In the ensuing battle with the Tree Demon, the master himself is captured.

Shi Fang enlists Yin to help rescue his master, and feels he must also help Lotus by recovering her urn. Yin and the master do battle with the Tree Demon, destroying it. Though he disapproves of his disciples relationship with the ghost, the master helps to save Lotus as well. However, after the Tree Demons destruction the Black Mountain Demon takes up the pursuit of Shi Fang, Master Bai Yun and Lotus. He summons high pillars to block their escape route and darkens the sky so the sunlight cannot shine on earth.

Master Bai Yun then casts a spell on Shi Fang and covers his body with holy gold liquid, then Lotus takes Shi Fang up to the sky above the dark clouds so that Shi Fang can use his golden body to reflect the sunlight to earth. In the end, the Black Mountain Demon is killed by sunlight, and the sky clears. Shi Fang thinks Lotus may have been killed by the sunlight, and starts looking for her. He finds Lotus hiding under a pile of rocks, safe from the sun, and Lotus tells Shi Fang that her spirit will follow him when he takes her urn of ashes away from that place.

==Cast==
* Tony Leung Chiu-Wai as Monk Shi Fang
* Joey Wong as Lotus
* Jacky Cheung as Yin
* Nina Li Chi as Butterfly
* Lau Siu-Ming as Tree Devil (Lao Lao)
* Lau Shun as Shi Fangs Master/ Reverend Bai Yun
* Lau Yuk Ting as Jade
* Tommy Wong
* Sam Hoh

 
 

==References==
 

==External links==
*  

 

 
 
 
 
 
 