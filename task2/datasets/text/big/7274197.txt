The Return of the Vampire
 
{{Infobox film
| name = The Return of the Vampire
| image = Returnofthevampire.jpg
| caption =
| director = Lew Landers Sam White
| writer = Randall Faye Griffin Jay
| starring = Bela Lugosi Frieda Inescort Nina Foch Miles Mander Roland Varno Matt Willis
| music = Mario Castelnuovo-Tedesco
| cinematography = L. W. OConnell
| editing = Paul Borofsky
| distributor = Columbia Pictures Corporation
| released =  
| runtime = 69 min. English
| budget = $75,000 
| box office = $500,000 (estimated) 
}}
  1944 by Columbia Pictures. It is in black and white, and describes an Englishwomans two encounters with a vampire. The first encounter takes place during World War One, and the second during World War Two.

The film stars   writes: "Columbia Pictures hired Lugosi for Return of the Vampire, in which he played Dracula in all but name; for copyright purposes, the vampires name was Armand Tesla." 

==Plot==
 
The film begins with Sir Fredrick Fleet (Miles Mander): The following events are taken from the notes of Professor Walter Saunders of King’s College, Oxford...

The first scene takes place in a mist-shrouded cemetery at night. A werewolf (Matt Willis) enters a tomb and tells his vampire ‘Master’ that it is time for him to awake. A hand reaches out of the coffin and lifts the lid. A shadow appears on the wall, and the voice of Bela Lugosi asks what happened while he was asleep. The werewolf replies that his latest victim has been taken to Dr. Ainsley’s clinic. 

Baffled by her patient’s anemic condition, Lady Jane Ainsley (Frieda Inescort) has called in Professor Walter Saunders (Gilbert Emery). While they are discussing the patient, two children enter. They are Lady Jane’s son, John, and Professor Saunders’ granddaughter, Nikki. Lady Jane and the professor send the children to bed and return to their patient. The figure, finding that his victim is not alone, attacks Nikki instead. After the patient dies, Professor Saunders sits up the rest of the night, reading a book on vampires written two hundred years ago by Armand Tesla.   

The following morning, the professor shows Lady Jane the bite marks on their dead patient’s neck, and tells her that he believes they were caused by a vampire. Lady Jane is skeptical until they discover similar bite marks on Nikki’s neck. 
Professor Saunders and Lady Jane go to the cemetery and search for the vampire’s coffin. As they are about to drive a stake through its heart, the werewolf returns and tries to stop them; but once the vampire is staked, the werewolf returns to his human form.  

Twenty four years have passed and Professor Saunders has just died, though his account of these events was found among his effects. Sir Fredrick Fleet (Miles Mander) sits in his office at Scotland Yard, reading the professors manuscript. 
Sir Frederick tells Lady Jane that he intends to find the body of the man whom she and Professor Saunders staked. If the man really was alive when they staked him, Lady Jane is guilty of murder.
Lady Jane tells Sir Frederick that the man she and the professor staked was two hundred years old. He was none other than Armand Tesla, whose lifelong fascination with vampires ended with his becoming one himself. 

The scene moves to Lady Jane’s clinic. Her son, John, and Professor Saunders’ granddaughter, Nikki, are now adults and plan to marry. 
It is World War II, and Nikki (Nina Foch) is in military uniform. John (Roland Varno) is in civilian clothes, having been discharged from the RAF due to a war injury. When she and John are alone, Lady Jane tells him about her meeting with Sir Frederick. John asks if she is worried about being arrested for murder. Lady Jane says that, when Sir Frederick finds Tesla’s body, he will see that it hasn’t decomposed. That will prove Tesla was a vampire.  They agree not to tell Nikki about this. They dont want to remind her of her childhood trauma when she was bitten by the vampire.
While they are talking, Andréas enters. He used to be Tesla’s werewolf servant. Freed of the vampire’s power, he has become human again, and is Lady Jane’s assistant at the clinic. Andréas is visibly upset when he hears that the vampires body is going to be dug up.

During an air raid, a bomb falls on the cemetery. Gravediggers are assigned to rebury the disturbed coffins. They find Teslas body, assume the stake driven through his heart was part of a bomb, and pull it out.

Back at the clinic, Lady Jane tells Andréas that Hugo Bruckner, a famous scientist, has escaped a Nazi concentration camp and is coming to Britain to work with her. She sends Andréas to meet Dr. Bruckner’s boat and bring him back to the clinic.

On his way to meet Bruckner, Andréas fully sees the risen vampire face to face. Fixing Andréas with his hypnotic eyes, the vampire says that he was responsible for Professor Saunders death. Now he will take his revenge on Lady Jane. Andréas, once again under Tesla’s power, becomes a werewolf. Following the vampire’s orders, he kills Bruckner and Tesla takes his place.

The following morning, Sir Frederick and Lady Jane come to the cemetery to look for Tesla’s grave. When they find there is nothing left of it but a hole where the bomb fell, Sir Frederick declares the case is closed.

That evening, Lady Jane throws a party to celebrate John and Nikki’s engagement. Sir Frederick arrives, with Professor Saunders’ manuscript. He asks Lady Jane whether he should give the manuscript to Nikki, since she is the professor’s granddaughter and only living relative. Lady Jane takes the manuscript and locks it in a drawer because she doesnt want Nikki to be reminded of her childhood trauma.

Tesla arrives, pretending to be Bruckner. He charms everyone except Sir Frederick, who seems suspicious of him. Lady Jane discovers the drawer has been forced open and the professor’s manuscript stolen. She calls in Sir Frederick. He finds some hairs stuck to the drawer, and puts them in his pocket. Upstairs, Nikki finds the manuscript lying beside her bed and begins reading it. Later, she hears Tesla’s voice calling to her. She asks who he is, and he replies that she already knows.

The following morning, John and Lady Jane find Nikki lying unconscious on the floor of her bedroom. John is upset when he sees the bite marks on Nikki’s neck, but Lady Jane assures him that everything will be all right.

Lady Jane returns to the cemetery and speaks with the gravediggers. They tell her that they found a body with a stake in it. They pulled out the stake and reburied the body, but now its missing.
She tells this to Sir Frederick, but he dismisses it because he doesnt believe in vampires. Instead he assigns two plainclothes men to shadow Andréas.

While the two men are following him, Andréas changes into a werewolf. He runs away, dropping the bundle he was carrying. 
The two men take the bundle to Sir Frederick, who opens it and finds it contains the personal effects of the real Hugo Bruckner. Sir Frederick’s suspicions of Bruckner/Tesla are now confirmed. While Sir Frederick is examining the contents of the bundle, another man comes in. He says that a laboratory analysis of the hairs Sir Frederick found on the drawer show them to be wolfs hairs.   

That night, as Nikki sleeps, Tesla calls to her again. He tells her to go to John’s bedroom.

The following morning, Lady Jane finds John lying on the floor of his bedroom with bite marks on his neck. Nikki is convinced that she is becoming a vampire, but Lady Jane tells her that Tesla bit John, hoping to make Nikki believe she did it. Sir Frederick and Lady Jane question Andréas about the bundle. His hands become hairy and claw-like, but before he completes his transformation into a werewolf, he flees.

Sir Frederick assigns the same two plainclothes men to follow Bruckner/Tesla, but the vampire eludes them.
Bruckner/Tesla goes to the Ainsley house and stands in the shadows, watching Lady Jane as she plays the organ. He tells her that, now she knows who he really is, he will take his revenge. He will turn Nikki into a vampire, and she will then do the same to John.  Lady Jane pushes the sheet music aside, revealing a cross on the organ. The vampire disappears. Again Tesla calls to Nikki. She rises from her bed, leaves her bedroom and walks down the stairs.
Downstairs, Sir Frederick and Lady Jane are once again arguing the existence of vampires. When they see Nikki coming down the stairs, they stop arguing and follow her.

Nikki goes to the cemetery, where Tesla and Andréas (who has now completed his transformation into a werewolf) are waiting for her. The air raid siren goes off and bombs start falling. Nikki faints. The werewolf picks her up and is carrying her to safety when Sir Frederick shoots him. The wounded werewolf staggers into the tomb, still carrying the unconscious Nikki. He lays her down and asks Tesla for help. The vampire says that he no longer needs him, and tells Andréas to crawl into a corner and die. The werewolf obediently crawls into a corner, where he finds a crucifix. He picks it up, and returns to his human form.  An explosion fills the screen, indicating a bomb has hit the cemetery. When Nikki awakes, she sees Andréas dragging an unconscious Tesla out of the tomb. 

It is now dawn, and the vampire begins to decompose in the daylight. After Tesla dies, Andréas also dies of his bullet wound. Sir Frederick and Lady Jane had taken shelter from the bombs, and continued quarreling. They now rush back to the cemetery and find Nikki, who tells them that Andréas saved her. Lady Jane asks Sir Frederick if he now believes in vampires. He says that he is still an unbeliever.
He turns to the two plainclothes men and asks them ‘You two fellows dont believe in vampires, do you?’ To his surprise, they both reply that they do. Sir Fredrick then breaks the fourth wall and asks ‘Do you people?’

==Cast==

* Bela Lugosi as Armand Tesla / Dr. Hugo Bruckner
* Frieda Inescort as Lady Jane Ainsley
* Nina Foch as Nicki Saunders
* Miles Mander as Sir Fredrick Fleet
* Roland Varno as John Ainsley Matt Willis as Andreas Obry / The Werewolf

==Production==
Bela Lugosi was paid $3,500 dollars for his four weeks of film. 

Bela Lugosi filmed this Columbia feature August 21 September 1943, prior to his final two Monogram films. This was also the last time he would receive top billing for a major Hollywood studio. 

==In popular culture==

This movie is mentioned in the Sanford and Son episode #104 ("The TV Addict") where Fred G. Sanford is watching a television program with his friend Grady Wilson. The movie is mentioned and Grady says that there is a surprise ending at which point Fred says he likes surprise endings. Then Grady tells him "if you like surprise endings, boy youre going to really like this one", and he goes on to explain the surprise ending, which infuriates Fred. Fred at that point overturns a bowl of potato chips onto Gradys head to show his dismay at having the surprise ending of the movie spoiled for him.

==See also==
*Vampire film

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 