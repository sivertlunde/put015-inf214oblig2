Pistols for Breakfast
 
{{Infobox film
| name           = Pistols for Breakfast
| image	=	Pistols for Breakfast FilmPoster.jpeg
| caption        = 
| director       = Alfred J. Goulding
| producer       = Hal Roach
| writer         = 
| starring       = Harold Lloyd
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = United States  Silent English intertitles
| budget         = 
}}
 short comedy film featuring Harold Lloyd. A print of the film survives in the Museum of Modern Art film archive.   

==Plot==
A young man (Fay) goes out to eat breakfeast with his friend (Harrison). As a restaurant "regular" with a pistol threatens to eat everyones bacon, the two friends flee.

==Cast==
* Harold Lloyd 
* Snub Pollard 
* Bebe Daniels  
* Sammy Brooks
* Billy Fay
* Estelle Harrison
* Lew Harvey
* Bud Jamison
* Dee Lampton
* Gus Leonard
* Fred C. Newmeyer
* James Parrott
* Dorothea Wolbert
* Noah Young

==See also==
* List of American films of 1919
* Harold Lloyd filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 


 