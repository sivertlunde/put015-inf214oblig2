Round-Up Time in Texas
{{Infobox film
| name           = Round-Up Time in Texas
| image          = Round-Up_Time_in_Texas_Poster.jpg
| caption        = Theatrical release poster
| director       = Joseph Kane
| producer       = Nat Levine Oliver Drake
| starring       = {{Plainlist|
* Gene Autry
* Smiley Burnette
* Maxine Doyle
}}
| cinematography = William Nobles
| editing        = Lester Orlebeck
| studio         = Republic Pictures
| distributor    = Republic Pictures
| released       =  
| runtime        = 61 minutes
| country        = United States
| language       = English
}} Western film Oliver Drake.  The film stars Gene Autry, Smiley Burnette, and Maxine Doyle. Despite its title, the majority of the film takes place in South Africa.

The film is about a cowboy who delivers a herd of horses for his brother, a diamond prospector whose work has attracted the interest of a bunch of badguys.   

==Plot== Ken Cooper), grubstaked Tex and Barclay. Gene and his sidekick Frog (Smiley Burnette) quickly arrange for passage overseas and accompany the horses to South Africa.

Meanwhile Tex and his partner, while on their way back to Dunbar from their mine, are ambushed by Cardigans men. Barclay is killed and Tex wounded but able to escape. Cardigan wants sole control of the mine. Sometime later, Gene and Frog arrive at Dunbar and hire Barkey McCuskey, a small-time English con artist, as their auctioneer for when they sell their horses. Worried that Tex is not in Dunbar, Gene stoutly defends his brother when Cardigan tells him he is being sought for the murder of Barclay. Gene grows suspicious of Cardigan when he sees that the saloon owner tampered with another telegram from Tex. Cardigan in turn grows jealous as Gene who becomes friendly with saloon singer Gwen (Maxine Doyle), who is Barclays daughter. 

The next day Gene sees Cardigans servant Namba (Corny Anderson) at the auction wearing the belt buckle that Gene gave to Tex, and Gene becomes more suspicious of Cardigan, whose henchman, Craig Johnson (Dick Wessel), plans to kill the Texans that night. Johnson arranges to have Gene and Frog arrested after he gives uncut diamonds to Frog in return for a horse. The South African Constabulary explain that it is illegal to have uncut diamonds without a license. The next day, while the police take Gene and Frog to Kimberly for trial, the Texans manage to escape. Later they find Cardigan as he is making his way through the jungle with Johnson, Gwen, and Barkey in search of the diamond mine. 

The group is captured by natives and brought before Chief Bosuto (Buddy Williams). Just as they are about to be sacrificed, Frog wins the chief over by teaching his children to sing. The chief agrees to let the others go if Frog stays behind to teach his children more music. Frog and Cardigan stay with the natives while Gene, Barkey, and Gwen are led away by Namba. After the police arrive and free Cardigan, and head to Cardigans jungle hideout, where Tex is being held a slave. Gene arrives and frees his brother and the other workers. Cardigan and Johnson attempt to escape, taken Gwen as a hostage, but Frog catches up with Johnson and Gene fights with Cardigan, who falls from a cliff to his death. The police arrive and capture the rest of Cardigans gang. Later on the ship returning to Texas, while Frog sings along with Bosutos children, Gene and Gwen kiss.

==Cast==
* Gene Autry as Gene Autry
* Smiley Burnette as Frog
* Maxine Doyle as Gwen Barkley
* The Cabin Kids as Chief Bosutos Children
* Champion the Wonder Horse as Genes Horse
* LeRoy Mason as John Cardigan
* Earle Hodgins as Barkey McCusky
* Dick Wessel as Henchman Craig Johnson
* Buddy Williams as Chief Bosuto
* Elmer Fain as Chief Bosutos Son
* Corny Anderson as Namba
* Frankie Marvin as Cowboy / Soldier Ken Cooper as Tex Autry   

==Production==
===Stuntwork===
* Ken Cooper
* Joe Yrigoyen 

===Filming locations===
* Iverson Ranch, 1 Iverson Lane, Chatsworth, Los Angeles, California, USA   

===Soundtrack===
* "When the Bloom Is on the Sage" (Fred Howard, Nat Vincent) by Gene Autry, Smiley Burnette, and cowhands
* "When the Bloom Is on the Sage" (Fred Howard, Nat Vincent) by Gene Autry, Smiley Burnette, The Cabin Kids, and Buddy Williams at the end
* "Old Chisholm Trail (Come a Ti Yi Yippee Yippee Yay)" (Traditional) by Gene Autry and Smiley Burnette
* "Drink Old England Dry" (Traditional) by Maxine Doyle and bar patrons
* "Uncle Noahs Ark" (Gene Autry, Smiley Burnette, Nick Manoloff) by Gene Autry and Smiley Burnette
* "Prairie Rose" (Sam H. Stept, Sidney D. Mitchell) by Gene Autry
* "Jacob Drink" (Traditional Polish folksong) by an unidentified man in the bar
* "Silver Threads Among the Gold" (H.P. Danks) played on a record
* "African Chant" by natives
* "Moon of Desire" by Gene Autry and natives
* "Voice Improvisation" by Smiley Burnette and The Cabin Kids
* "Shell Be Coming Round the Mountain" (Traditional) 
* "On Revival Day" (Andy Razaf) by The Cabin Kids
* "Caveman" by Gene Autry
* "Dinah" (Harry Akst, Sam Lewis, Joe Young) by Smiley Burnette and The Cabin Kids   

==References==
;Citations
 
;Bibliography
 
*  
*  
*   

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 