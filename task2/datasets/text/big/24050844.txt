Mero Euta Saathi Cha
Mero Euta Saathi Chha ( : I Have One Friend)  is a Nepali movie released on September 11, 2009. This movie was a huge hit in 2009, despite some parts of the story appeared similar to that from the Korean hit A Millionaires First Love including the leaked sex tape of its lead actress with a married man.

==Plot summary==

Where jack and rose childhood friends Pair up at cornerstones of their lives. Shikha being the small town girl and Jay the raunchy heir to a family fortune, Jay comes to the realization of joy and happiness which he finds in Shikha during pursuit of his fortune. The relationship morphs Jay, whereas fate and destiny have their own ways with the budding pair.

==Cast==

* Aaryan Sigdel as Jay Shumsher JBR Namrata Shrestha as Shikha
* Jeewan Luitel
* Ashok solta
* Keshab Bhattarai
* Zenisha Moktan
* Sushma Karki
* Rekha Thapa in a cameo

==Crew==

* Produced By Prabhu S.J.B Rana,Prajwal S.J.B Rana
* Executive Producer: Pritam Singh 
* Directed By Sudarshan Thapa
* Special Appearance rekha thapa
* Music Director Sugam Pokhrel, Bipin Acharya
* Cinematographer Rajesh Singh,Puru Pradhan
* Fight Director Anil Rai
* Story By Suvas S. Basnyat
* Budget:1 crore
* Gross revenue: 123.32 rupees

==Release==
The film was released on September 11, 2009. After a year of production, the movie did not follow the queue system. With that provision, it did not get the screening dates at the Bishwa Jyoti Cinema and Ganga Hall but was screened at other cinema halls of both Kathmandu and outside.

==Reception==

 

==Critical Response==
Although, the film met with some criticism for the story resembling to the Korean movie A Millionaires First Love, it received positive reviews for its direction, cinematography, choreography and mostly for Aryan Sigdels performance.

Aryan Sigdel won the National Film Award for the Best Actor in Leading Role for his performance.

==Box office==

Many critics believe that the success of this movie is attributed to the leaked sex tape of its lead actress, Namrata Shrestha. It is also rumored that said tape was carefully timed for the success of this movie.

==Music==
{{Infobox album |  
| Name = Mero Euta Sathi Chha
| Type = soundtrack
| Artist = Sugam Pokharel
| Cover = Feature film soundtrack
| Lyrics = Sugam Pokharel   Bipin Acharya
}}

===Tracks===
The official track listing.
{{tracklist
| extra_column    = Artist(s)
| title1          = K Yo Maya Ho
| extra1          = Sugam Pokhrel
| length1         =
| title2          = Juni Juni
| extra2          = Sugam Pokhrel, Rejina Rimal
| length2         =
| title3          = Lajai Lajai
| extra3          = Soham, Mandira
| length3         =
| title4          = Mero Aankha ko Gajal
| extra4          = Sunidhi Chauhan
| length4         =
| title5          = Mero Euta Saathi Chha
| extra5          = Sugam Pokhrel
| length5         =
}}

==External links==
*  
*  
*  
*  
*  
*  
*  

 
 
 