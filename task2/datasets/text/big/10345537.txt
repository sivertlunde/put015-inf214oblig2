Dil Deke Dekho
{{Infobox film
| name           = Dil Deke Dekho
| image          = DilDekeDekho.jpg
| image_size     = 
| caption        = 
| director       = Nasir Hussain
| producer       = Sashadhar Mukherjee
| writer         = 
| narrator       = 
| starring       = Shammi Kapoor Asha Parekh
| music          = Usha Khanna
| cinematography = Dilip Gupta
| editing        = S.E Chandiwale
| distributor    = 
| released       =  October 2, 1959
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
}}
 Tumsa Nahin Dekha (1957), which became a big hit and which gave star Shammi Kapoor a new image as a comedic, dancing hero.  Husain and Kapoor reteamed here.  Asha Parekh, a former child actress, made her film debut as a heroine.  This was also the debut film of comedian Rajendranath (younger brother of Premnath.) This is also music composer Usha Khannas first film.

The film became a hit at the box office.   Husain, Kapoor, and Parekh would reteam again for Teesri Manzil (1966), which also became a big hit.  Husain would also cast Parekh as his leading lady in five other films.  Shammi Kapoor had said that he had initially wanted Waheeda Rehman to play his leading lady in the film, but the producer had already signed Asha Parekh, and he encouraged her during filming.   The film was released on Ashas 17th birthday. 

==Plot==

Neeta is an heiress, the only daughter of U.K. based Industrialist, Jagat Narayan. She is of marriageable age, and is being wooed by Kailash, Chandra, and Raja. She prefers Chandra over Kailash and Raja, but subsequently changes her mind and falls in love with Raja. Things take a dramatic turn when Jagat and Neeta find out that Raja is not who is claims he is. When Raja defends himself, and calls himself Roop, his very own mother denies this claim, and instead states that Chandra is Roop, her only son. Raja alias Roop must now make attempts to prove himself, and realizes that this is indeed an uphill task.

==Cast==
*Shammi Kapoor: Raja/Roop
*Asha Parekh: Neeta Narayan
*Sulochana Latkar: Jamuna (as Sulochna)
*Raj Mehra: Jagat Narayan
*Rajendra Nath: Kailash
*Kewal Kapoor: (as Keval Kapoor)
*B.K. Mukherjee:(as B.K. Mukerji)
*Surendra: Harichand

== Soundtrack ==
The Soundtrack of the movie was composed by Usha Khanna, with the lyrics written by Majrooh Sultanpuri. Although all the songs are memorable, they seem to have an OP Nayyaresque tinge to them.
{| class=wikitable
|-
!Song
!Singer(s)
|- Dil Deke Dekho Mohammad Rafi
|- Yaar Chulbhula Hai Mohammad Rafi & Asha Bhosle
|- Pyar Ki Kasam  Mohammad Rafi & Asha Bhosle
|- Bade Hai Dil Ke Kaale Mohammad Rafi & Asha Bhosle
|- Hum Aur Tum Aur Yeh Sama Mohammad Rafi
|- Bolo Bolo Kuch Toh Bolo Mohammad Rafi
|- Megha re Megha Mohammad Rafi
|}

==References==
 

== External links ==
*  

 

 
 
 
 
 
 


 