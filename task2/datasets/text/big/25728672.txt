All Under the Moon
{{Infobox film
| name           = All Under the Moon
| image          = 
| caption        = 
| film name = {{Film name| kana           = 月はどっちに出ている
| romaji         = Tsuki wa Dotchi ni Dete Iru}}
| director       = Yōichi Sai
| producer       = 
| writer         = 
| starring       = Masato Hagiwara Ruby Moreno
| music          = 
| cinematography = Junichi Fujisawa
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
}} Japanese film directed by Yoichi Sai.

==Awards and nominations==
18th Hochi Film Award  
*Won: Best Film
*Won: Best Director - Yōichi Sai
*Won: Best Actress - Ruby Moreno Japan Academy Prize 
*Nominated: Best Film
*Nominated: Best Director - Yōichi Sai
*Nominated: Best Screenplay
*Nominated: Best Actress - Ruby Moreno
15th Yokohama Film Festival  
*Won: Best Film
*Won: Best Director - Yōichi Sai
*Won: Best Cinematography - Junichi Fujisawa
*Won: Best Supporting Actor - Masato Hagiwara
*Won: Best Supporting Actress - Ruby Moreno

==References==
 

==External links==
* 

 
{{Navboxes  title = Awards  list =
 
 
 
 
 
}}

 
 
 
 
 
 


 