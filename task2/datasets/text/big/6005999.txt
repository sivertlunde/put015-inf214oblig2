Funny Ha Ha
{{Infobox film|
  name           = Funny Ha Ha |
  image          = Funny ha ha poster.jpg|
  director       = Andrew Bujalski |
  writer         = Andrew Bujalski |
  starring       = Kate Dollenmayer Mark Herlehy Christian Rudder Jennifer L. Schaper Myles Paige Marshall Lewy |
  | cinematography = Matthias Grunsky
  | producer       = Ethan Vogt Morgan Faust Hagai Shaham | Sundance Channel Goodbye Cruel Releasing Wellspring Media |
  released       =   |
  country        = United States |
  runtime        = 89 minutes |
  language       = English |
}} Allston neighborhood of Boston, Massachusetts.

==Reception==
The movie was largely successful with critics, who praised it for its realism. It received an 87% freshness score on Rotten Tomatoes  and a rating of 78 on Metacritic. 
 Daily Varietys Robert Koehler said the movie was "beautifully observant and wholly unpretentious".  

It was named to top 10 lists by A.O. Scott of The New York Times, Kimberley Jones of The Austin Chronicle, Mark Mohan of The Oregonian and Robert Koehler of Variety. 

The films widest release was three theaters. It grossed $82,620. 

==Awards==
Andrew Bujalski was the winner of the 2004 Someone to Watch Award at the Independent Spirit Awards.  The film won the featured film award at the 2004 Black Point Film Festival.  In 2005, Kate Dollenmayer was runner-up for the National Society of Film Critics Best Actress award. 

==References==
 

==External links==
* 
* 
* 
* 

 
 
 
 