The Buddy Holly Story
 

{{Infobox film
| name           = The Buddy Holly Story
| image          = Buddy_holly_story_cover.jpg
| caption        = The Buddy Holly Story DVD cover
| director       = Steve Rash
| producer       = Fred Bauer Edward H. Cohen Frances Avrut-Bauer Fred T. Kuehnert
| writer         = Novel: John Goldrosen Story: Alan Swyer screenplay: Robert Gittler Paul Mooney
| music          = Joe Renzetti
| cinematography = Stevan Larner
| editing        = David E. Blewitt James Seidelman
| distributor    = Columbia Pictures
| released       = May 18, 1978
| runtime        = 113 minutes
| country        = United States
| language       = English
| budget          = $2 million 
| gross          = $14,363,400 
}}
 rock musician Buddy Holly.  It features an Academy Award-winning musical score, adapted by Joe Renzetti and Academy Award|Oscar-nominated lead performance by Gary Busey.
 William Jordan, and Maria Richwine, who played Maria Elena Holly.

It was adapted by  .

==Plot==
Buddy Holly, a teenager from Lubbock, Texas, emerges into the world of rock and roll with friends and bandmates, drummer Jesse Charles and bass player Ray Bob Simmons, forming a trio known as The Crickets.

The bands first break comes when it is invited to Nashville, Tennessee to make a recording, but Buddys vision soon clashes with the producers rigid ideas of how the music should sound and he walks out. Eventually, he finds a more flexible producer, Ross Turner, who, after listening to their audition, very reluctantly allows Buddy and the Crickets to make music the way they want.

Turners secretary, Maria Elena Santiago, quickly catches Buddys eye. Their budding romance nearly ends before it can begin because her aunt initially refuses to let her date him, but Buddy persuades the aunt to change her mind. On their very first date, Maria accepts his marriage proposal and they are soon wed.

A humorous episode results from a misunderstanding at a New York booking. Sol Gittler signs up the Crickets sight-unseen for the famous Apollo Theater in Harlem, assuming from their music that theyre a black band. When three white Texans show up instead, he is stunned. Unwilling to pay them for doing nothing, and because Buddy and the Crickets have a contract specifying a weeks engagement for $1000.00, Gittler nervously lets them perform and prays fervently that the all-black audience doesnt riot at the sight of the first all-white band to play there. (In real life, that distinction belongs to Jimmy Cavallo and The House Rockers, who played at that venue in 1956.) After an uncomfortable start, Buddys songs soon win over the audience and the Crickets are a tremendous hit.

After two years of success, Ray Bob and Jesse decide to break up the band. They feel overshadowed by Buddy and do not want to relocate to New York City, which Buddy believes is necessary to stay on top. Initially, he is saddened by their departure, but he soldiers on. Maria announces that she is pregnant and Buddy is delighted.

On February 2, 1959, preparing for a concert at  ...and the rest is Rock and Roll.

==Cast==
{|class="wikitable"
! Actor !! Role
|-
| Gary Busey || Buddy Holly
|-
| Don Stroud || Jesse Charles (based on J.I. Allison)
|-
| Charles Martin Smith || Ray Bob Simmons (based on Joe B. Mauldin)
|- William Jordan || Riley Randolph (Lubbock D.J.)
|-
| John F. Goff || T. J. (Nashville Producer)
|-
| Amy Johnston || Cindy Lou
|-
| Conrad Janis || Ross Turner
|-
| Albert Popwell || Eddie Foster
|-
| Fred Travalena || Madman Mancuso (Buffalo, New York D.J., possibly based on Tom Clay or Danny Neaverth)
|-
| Jack Denbo || New York Cabbie
|-
| Maria Richwine || María Elena Holly
|-
| Dick ONeill || Sol Gittler
|-
| Freeman King || Apollo M.C.
|- Paul Mooney || Sam Cooke
|-
| Jerry Zaremba || Eddie Cochran
|-
| Gailard Sartain || The Big Bopper
|-  Joe Renzetti || first studio-violinist
|- Matthew Beard Matthew Beard || Luther (stage hand)

|}

==Production==
The actors did their own singing and played their own instruments, with guitarist Jerry Zaremba overdubbing the guitar parts. Busey, in particular, was admired by critics for recording the soundtrack music live and for losing a considerable amount of weight in order to portray the skinny Holly. According to Buseys biography, he lost 32 pounds to look more like Holly, who weighed 146&nbsp;pounds at the time of his death.

The actors accurate portrayal was aided by knowledge gained from a previous attempt to film part of the Holly life story, the ill-fated Three-Sided Coin, in which he played Crickets drummer Jerry Allison. (The film was cancelled by 20th Century Fox due to pressure from Fred Bauer and his company, who had made deals with the Holly Estate (law)|estate.) The screenplay of Three-Sided Coin (by Allison and Tom Drake) revealed many personal details about Holly, and Busey picked up more during off-set conversations with Allison.

While the story follows Buddy Holly from age 19 to 22 (1955 to early 1959), Busey was 33 when he played the role. Charles Martin Smith auditioned for the role of Buddy, but since Busey already had been cast, the producers cast Martin to play Ray Bob Simmons because they liked his audition. Simmons and Jesse Charles were character names used in place of Joe B. Mauldin and J.I. Allison, the actual Crickets. 

The incident in which a Buffalo disc jockey locked himself in a studio and repeatedly played the same song over and over was loosely based on a real-life stunt orchestrated by controversial disc jockey Tom Clay (and repeated a few years later by Danny Neaverth), who held up Buffalos Shelton Square by playing Bill Haley & His Comets "Rock Around the Clock" repeatedly from the top of a billboard; that incident, however, had no relation to Buddy Holly or his music.

==Reception==

The film received many positive reviews from critics and fans alike.  But Holly authorities and other music scholars criticized what they deemed gross inaccuracies in the plot, such as showing a physical confrontation at Hollys first Nashville session when nothing of the sort actually occurred.

===Crickets reaction===

The Crickets, after watching the film, appreciated it but gave a thumbs down on their portrayal, calling it "horrible" due to many reasons. 
 Bob Montgomery, Don Guess & Larry Welborn were written out of the film altogether and never depicted controversially. 
 racist attitudes and manners (the way the character says "nigger").

In 1984, former Beatles bassist Paul McCartney wanted to tell the actual story of Buddy Holly; with participation of Curtis, Montgomery, Allison, Mauldin, as well as family members of Hollys, participated in a documentary written by McCartney called The Real Buddy Holly Story.

==Awards== Academy Award Best Adaptation Best Actor Best Sound.   

==References==
 

== External links ==
*  
*  
*  
*  

 
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 