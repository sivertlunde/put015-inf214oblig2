The Humpty Dumpty Man
 
 
{{Infobox film
| name           = The Humpty Dumpty Man
| image          = 
| image size     =
| caption        = 
| director       = P.J. Hogan
| producer       = Miranda Bain
| writer         = P.J. Hogan Karl Zwicky
| based on = 
| narrator       =
| starring       = Frank Gallacher
| music          = 
| cinematography = Martin McGrath
| editing        = Murray Ferguson
| studio = Quantum Films Capital Productions Australian Film Commission
| distributor    = Home Cinema Group (video)
| released       = 1989 (video)
| runtime        = 92 mins
| country        = Australia English
| budget         = A$1,292,000 "Australian Productions Top $175 million", Cinema Papers, March 1986 p64 
| gross = 
| preceded by    =
| followed by    =
}}
The Humpty Dumpty Man is a 1986 Australian film which was the first feature directed by P.J. Hogan. It was based on the David Combe affair. David Stratton, The Avocado Plantation: Boom and Bust in the Australian Film Industry, Pan MacMillan, 1990 p241-242  

==Plot==
Shadlow is a trade consultant who accepts a Soviet trade contract and is accused of international espionage.

==Release==
The movie was not released theatrically. 

==References==
 

==External links==
*  at IMDB

 
 
 
 

 