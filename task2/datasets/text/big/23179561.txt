Phagun (1958 film)
 
{{Infobox film
| name           =  Phagun
| image          = Phagun, 1958 film.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Bibhuti Mitra
| producer       = bibhuti Mitra under the banner of Mitra Productions.
| writer         =Pranab Roy (Story)
| narrator       = 
| starring       =Madhubala,Bharat Bhushan,Jeevan,Kammo,Nishi,Cuckoo (Dancer in song "Shokh Shokh Aankhen",Dhumal,Mehmood (Palace Guard),Murad,Badri Prasad,Rajendra,Jagdish Kanwal and Ronak Singh.
| music          = O.P.Nayyar.Lyrics by: Qamar Jalalabadi.Playback Singers: Mohd.Rafi,Asha Bhosle.
| cinematography = D.N.Ganguly
| editing        = 
| distributor    = zEros
| released       = 1958
| runtime        = 2h 24mn 20s
| country        = India Hindi
| budget         = 
| gross          = 
}}

 Phagun is a 1958 Bollywood film directed by Bibhuti Mitra. It was the 6th highest grossing film in India of 1958. 

==Cast==
*Madhubala... Banani
*Bharat Bhushan... Bijan
*Jeevan... Madhal
*Kammo		 Nishi		
*Cuckoo... Dancer (in song "Shokh Shokh Aankhen")
*Dhumal (actor)|Dhumal... Mattu
*Mehmood Ali|Mehmood... Gatekeeper (as Mahmood)
*Murad (actor)|Murad... Tinkari
*Badri Prasad... Bijans Father (as Badriprasad) Rajendra (as Rajendar)
*Jagdish Kanwal (as Jagdish Kamal)

==Music==
It has a popular song "Ek Pardesi Mera Dil Le Gaya, Jaate Jaate Meetha Meetha Gham De Gaya".


{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track # !! Song !! Singer(s)!! Duration !! Picturized On
|-
| 1
| "Teer Yeh Chupke"
| 
| 3:07
| Madhubala, Bharat Bhushan
|-
| 2
| "Main Soya Ankhiyan Meeche"
| 
| 3:32
| 
|-
| 3
| "Barso Re Haye Bairi Badarwa Barso Re"
| 
| 3:15
| Madhubala, Bharat Bhushan
|-
| 4
| "Chhum Chhum Ghunghroo Bole"
| 
| 3:32
| Madhubala
|-
| 5
| "Piya Piya Na Lage Mora Jiya"
| 
| 3:19
| Madhubala, Bharat Bhushan
|-
| 6
| "Soon Ja Pukar"
| 
| 3:06
| Bharat Bhushan, Cuckoo, Jeevan, Kammo, Madhubala, Nishi
|-
| 7
| "Bana De Prabhuji"
| 
| 4:18
| Madhubala, Bharat Bhushan
|-
| 8
| "Ek Pardesi Mera Dil Le Gaya"
| 
| 3:53
| Madhubala, Bharat Bhushan
|-
| 9
| "Meri Chhod De Kalahi"
| 
| 3:12
| Mehmood, Dhumal
|-
| 10
| "Tum Rooth Ke Mat Jana"
| 
| 3:15
| Madhubala, Bharat Bhushan
|-
| 11
| "Shokh Shokh Aankhen"
| 
| 4:06
| Nishi, Cuckoo, Bharat Bhushan
|}

==References==
 

==External links==
*  

 
 
 
 

 