The Plague Dogs (film)
 
{{Infobox film
| name           = The Plague Dogs
| image          = Plaguedogsposter.jpg
| image_size     = 250px
| caption        = Martin Rosen Tony Guy (animation)
| producer       = Martin Rosen
| writer         = Martin Rosen (screenplay) Richard Adams (novel)
| narrator       = John Bennett John Franklyn-Robbins Bill Maynard Malcolm Terris Judy Geeson Patrick Stewart
| music          = Patrick Gleeson
| cinematography =
| editing        =
| studio         = Nepenthe Productions
| distributor    = United Artists (United Kingdom)   Embassy Pictures (USA)
| released       =  
| runtime        = 103 min (82 for cut version)
| country        = United Kingdom
| language       = English
| budget         =
}} 1982 United British adult adult animated animated drama novel of Martin Rosen, Watership Down, Nepenthe Productions; it was released by Embassy Pictures in the United States and by United Artists in the United Kingdom. The film was rated MPAA film rating system|PG-13 by the MPAA for violent images and thematic elements.

The films story is centered on two dogs named Rowf and Snitter, who escape from a research laboratory in Great Britain. In the process of telling the story, the film highlights the cruelty of performing vivisection and animal research for its own sake (though Martin Rosen said that this was not an anti-vivisection film, but an adventure), an idea that had only recently come to public attention during the 1960s and 1970s.

==Plot== smooth fox resuscitated repeatedly. One evening, Snitter squeezes under his cage and into Rowfs, where they discover his cage is unlatched. Eager to escape the tortures of life inside the facility, they explore, knocking over other cages and finally sneaking into the incinerator, where they are nearly killed before escaping.

Initially relieved and eager to   and murdering sheep and even humans.

The Tod parts with them as the pursuers close in, telling them hes got "a few more tricks" up his sleeve, but he is quickly wounded and killed. Snitter and Rowf hear his final scream from a train they board, but neither wants to believe that hes been killed: "Theyll never catch him, hes too crafty." As they go, Snitter smells the sea, and decide that it is their last hope. On the beach of Ravenglass, Snitter looks out over the water and claims to see the island. Without a second thought, Snitter strolls in the sea and begins to swim towards the island. Rowf on the other hand, is very hesitant to follow him. His intense fear of water/drowning has stranded him on the shore, all the while armed troops are quickly closing in. Desperate to escape the troops, he jumps in and catches up with Snitter. Two gunshots are fired at them but appear to miss; immediately a white mist appears and the humans all disappear. They begin to swim away into the mist towards the island Snitter claims to see but Rowf cant spot. The dogs swim and swim and swim, until at last, Snitter claims that the island was an illusion due to his operation and begins to let himself drown. Rowf however, claims to finally spot the island and urges Snitter to continue;"Just stay with me...Ill get you there."

The film ends with the dogs swimming into the fog, leaving it ambiguous as to whether they survived or not, even though an island is shown during the credits.

==Differences between the film and novel==
The differences between the films conclusion and the books are numerous and have substantial bearing on their respective outcomes. The most significant is that the survival of the dogs at the end of the film seems very unlikely; the book as originally submitted to the publisher concluded similarly, but in all known editions, the pair is rescued and returned to Snitters original owner, who in the book has survived. Other differences are linked to the alteration of certain characters and to an overall compression of the time sequences and series of events outlined in the book.

==Video release history==
 
There are two versions of the film: An 86 minute version and a 103 minute version. The only country that offered the full-length film on DVD was Australia {{cite web | work=07/06/2005 Archives: "Martin Rosens Plague Dogs comes to DVD - UNCUT!" | title=07/06/2005 Archives: "Martin Rosens Plague Dogs comes to DVD - UNCUT!" |url= archiveurl = archivedate = 25 October 2006}}  until it was released in the UK on 7 January 2008.   

The original VHS release of the full theatrical cut of the film was released by Thorn Productions in 1982. Only around 8,000 copies of this version were made.

In 1983, Embassy Pictures ordered cuts to be made to the film. This shortened cut was premiered in 1983. In 1984, it was given a limited release across the United States by Charter Productions. Strangely, the VHS cover states that the film is 99 minutes long.

In 2002, Anchor Bay Entertainment released a Region 2 DVD version of the film, but it contained the US cut. Soon afterwards, the Dutch budget label, Indies Home Entertainment, released a Region 2 disc which also contained the US cut but includes forced Dutch subtitles; this DVD has been out-of-print since November 2005. In 2004, a DVD version of the film was released by Trinity Home Entertainment; unable to obtain the full cut, they settled with using the truncated US version.
 Watership Down), Martin Rosens private print. This is probably the only full cut of the film in existence aside from the rare Thorn VHS and the original master. The same print was later released on Region 2 DVD in the UK by Optimum Releasing in 2008.

Many missing scenes are considered minor to the overall plot and were mostly removed to reduce running time. However, one scene taken from the book was removed because of its shocking content: After the hired gunman falls to his death from a steep crag from which he was attempting to shoot the dogs, a helicopter flies over the snow-covered crags and valleys, cutting to a close-up of his body, ripped to shreds, implying that the starving dogs had eaten the corpse.

==End theme==
The theme song, "Time and Tide", was composed and sung by Alan Price.
 sampled by industrial group Skinny Puppy for their anti-vivisection single (music)|single, "Testure", from their 1988 album VIVIsectVI.

==References==
 

==External links==
 
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 