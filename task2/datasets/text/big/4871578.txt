King Boxer
 
 
{{Infobox Film
| name           = King Boxer
| image          = 5 Fingers of Death.jpg
| caption        = Promotional Lobby card of the US release
| film name = {{Film name|traditional=天下第一拳
 |simplified=天下第一拳 pinyin = Tiān xià dì yī quán jyutping = Tin 1  haa 6  dai 6  jat 1  kyun 4 }}
| director       = Chang-hwa Chung, Korea
| producer       = Run Run Shaw
| writer         = Chiang Yang
| narrator       = 
| starring       = Lo Lieh
| music          = Yung-Yu Chen
| cinematography = Wang Yunglung
| editing        =  Shaw Brothers Warner Bros. (US)
| released       = 28 March 1972   21 March 1973  
| runtime        = 98 minutes
| country        = Hong Kong English
| budget         = 
| gross = $4,000,000 (US/ Canada rentals) 
}}

King Boxer aka Five Fingers of Death ( , lit. "Number One Fist in the World"), is a 1972 martial arts film directed by Chang-hwa Chung (鄭昌和 정창화) and starring Lo Lieh. Made in Hong Kong, it is one of many kung fu-themed movies with Lo Lieh in the lead. He appeared in many similar efforts from the 1960s, pre-dating the more internationally successful Bruce Lee.

Released in the USA by Warner Bros. in March 1973, the film was responsible for beginning the North American kung fu film craze of the 1970s,  though it is overshadowed by Enter the Dragon released later that same year. The film has a cult following in the U.S., and was referenced in Quentin Tarantinos film Kill Bill, which sampled the theme from the television series Ironside (TV series)|Ironside played during several of its fight scenes. When asked in 2002 by Sight & Sound Magazine to name his twelve favourite movies of all time, Tarantino placed "Five Fingers of Death" at number 11. 

==Plot==
A promising young martial arts student named Chi-Hao has spent most of his life studying under a master and has fallen in love with the masters daughter Yin-Yin. After the master fails to properly fight off a group of thugs, he sends Chi-Hao to study under a superior master, Shen Chin-Pei. He instructs Chi-Hao to learn from Chin-Pei and defeat the local martial arts tyrant, Ming Dung-Shun, in an upcoming tournament in order to earn Yin-Yins hand.

Chi-Hao meets a young female singer, Yen Chu Hung, on the road to the city and rescues her from Dung-Shuns thugs. She falls in love with him, but he resists her advances with difficulty. He reaches town and begins studying under Shen Chin-Pei. After an initial beating by Chin-Peis star pupil, Han Lung, Chi-Hao improves rapidly. One day, another thug of Dung-Shuns, Chen Lang, breaks into the school and beats all of Chin-Peis students. Chin-Pei finally arrives and fights him, but is struck by a dishonorable blow and severely wounded. Chi-Hao tracks Chen Lang down and defeats him. When Chin-Pei hears of this, he selects Chi-Hao to receive his most deadly secret, the Iron Fist.

Han Lung discovers that Chi-Hao has been chosen as Chin-Peis successor and becomes intensely jealous. He conspires with Dung-Shun to have Chi-Hao crippled. He lures Chi-Hao into the forest, where Dung-Shuns three new Japanese thugs ambush him. They overpower him and break his hands. Later, they visit his old masters school and kill him as well. Yen helps Chi-Hao recuperate and again tries to woo him, but he resists her. Finally, Chi-Haos fellow students locate him and encourage him to regain his fighting spirit. He begins training and soon overcomes his wounds. Yin-Yin arrives, but withholds the news of her fathers death. A rejuvenated Chi-Hao successfully defeats all the other students to become Chin-Peis representative for the upcoming tournament. Han Lung returns to Dung-Shun with the news, but Dung-Shuns son blinds him and casts him out.

On the day of the tournament, a conscience-stricken Chen Lang warns Chi-Hao of the three Japanese thugs lying in ambush on the road to the arena. Chi-Hao fights the thugs until Chen Lang arrives and holds them off so that Chi-Hao can get to the tournament on time. He arrives just in time and defeats Dung-Shuns son to win the tournament. Dung-Shun stabs Chin-Pei amongst the celebrations and departs. As Dung-Shun arrives back home, he discovers that all his lights are out. Han Lung appears in the darkened room and, guided by Yens direction, fights Dung-Shun and his son. Han Lung blinds the son, who is then stabbed by his father in the confusion. Dung-Shun bursts out of the dark room and summons his minions, who kill Han Lung and Yen Chu Hung.

Chi-Hao arrives at Dung Shuns house, but Dung-Shun flees and stabs himself before Chi-Hao can fight him. As he leaves, the chief Japanese thug arrives with Chen Langs head. He and Chi-Lao face off. Chi-Hao uses his Iron Fist power, causing his hands to glow red, and delivers several powerful blows that send the thug smashing into a brick wall. The thug defeated, Chi-Hao collects Yin-Yin and departs.

==Legacy==
The title of the movie was parodied in the "mockumentary" 18 Fingers of Death!, which pokes fun at martial arts movies.

==See also==
*List of Shaw Brothers films
*Hong Kong action cinema
*List of Dragon Dynasty releases

==References==
 

==External links==
* 
* 
* 
*  @ THE DEUCE: Grindhouse Cinema Database
* 

 
 
 
 
 
 
 
 
 