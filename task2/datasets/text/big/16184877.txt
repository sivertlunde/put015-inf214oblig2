Dark Honeymoon
{{Infobox film
| name           = Dark Honeymoon
| director       = David OMalley
| image          = Dark Honeymoon FilmPoster.jpeg
| producer       = Michael L. Meltzer Scott Vandiver
| writer         = David OMalley
| starring       = Lindy Booth Nick Cornish Roy Scheider Tia Carrere Daryl Hannah Eric Roberts
| music          = Juan J. Colomer
| cinematography = Matt Molitor
| editing        = Joe Pascual Michael Spence 
| studio         = Alpine Pictures
| released       =  
| runtime        = 89 minutes
| country        = United States
| language       = English
}}
Dark Honeymoon is a 2008 thriller film, starring Lindy Booth, Nick Cornish, Tia Carrere, Daryl Hannah, Roy Scheider and Eric Roberts.  It was directed by David OMalley and released direct-to-video on 22 July 2008. 

==Plot==
After a brief courtship, a man marries an enchanting woman, and then things begin to go terribly wrong. During their honeymoon on the foggy Oregon coast, he discovers her shocking secrets as those around them begin to die horrible and violent deaths, one by one. He soon learns that you really dont know someone until you marry them.

==Cast==
* Lindy Booth as Kathryn
* Nick Cornish as Paul
* Roy Scheider as Sam
* Tia Carrere as Miranda 
* Daryl Hannah as Jan
* Eric Roberts as L.A. Guy
* Wes Ramsey as Jay
* Robert R. Shafer as Sheriff Fields

==Production==
Shooting began May 2006 in Cambria, California. The DVD was released on July 22, 2008.

According to industry sources, new footage was shot for "Dark Honeymoon" without the input, permission or knowledge of the writer/director, a motion picture veteran named David OMalley.  The film was then re-edited, drastically changing the original story, characters and intent of the movie. As a result, OMalley reportedly disavowed the film and replaced his name with a pseudonym. 

==References==
 

==External links==
*  

 
 
 
 
 

 