Shakespeare in Love
:For the theatre adaptation, see Shakespeare in Love (play).
 
 
{{Infobox film
| name = Shakespeare in Love
| image = Shakespeare in Love 1998 Poster.jpg
| alt = 
| caption = Theatrical release poster John Madden
| producer = David Parfitt Donna Gigliotti Harvey Weinstein Edward Zwick Marc Norman
| writer = Marc Norman Tom Stoppard
| starring = Gwyneth Paltrow Joseph Fiennes Geoffrey Rush Colin Firth Ben Affleck Judi Dench 
| music = Stephen Warbeck
| cinematography = Richard Greatrex David Gamble Universal Pictures  
| released =  
| runtime = 123 minutes  
| country = United Kingdom United States
| language = English
| budget = $25 million 
| gross = $289.3 million 
}} romantic comedy-drama John Madden, written by Marc Norman and playwright Tom Stoppard. The film depicts an imaginary love affair involving Viola de Lesseps (Gwyneth Paltrow) and playwright William Shakespeare (Joseph Fiennes) while he was writing Romeo and Juliet. Several characters are based on historical people, and many of the characters, lines, and plot devices allude to Shakespeares plays.
 Best Picture, Best Actress Best Supporting Actress (Judi Dench).

==Plot== The Rose Theatre. Shakespeare is working on a new comedy, Romeo and Ethel, the Pirates Daughter. Suffering from writers block, he has barely begun the play, but starts auditioning players. Viola de Lesseps, the daughter of a wealthy merchant who has seen Shakespeares plays at court, disguises herself as "Thomas Kent" to audition, then runs away. Shakespeare pursues Kent to Violas house and leaves a note with the nurse, asking Thomas Kent to begin rehearsals at the Rose. He sneaks into the house with the minstrels playing that night at the ball, where her parents are arranging her betrothal to Lord Wessex, an impoverished aristocrat. While dancing with Viola, Shakespeare is struck speechless, and after being forcibly ejected by Wessex, uses Thomas Kent as a go-between to woo her. Wessex also asks Wills name, to which he replies that he is Christopher Marlowe.
 Christopher Kit Marlowe, completely transforming the play into what will become Romeo and Juliet. Viola is appalled when she learns he is married, albeit separated from his wife, and she knows she cannot escape her duty to marry Wessex. After Viola learns that Will is married, Will discovers that Marlowe is dead, and thinks he is the one to blame. Lord Wessex suspects an affair between Shakespeare and his bride-to-be. Because Wessex thinks that Will is Kit Marlowe, he approves of Kits death, and tells Viola the news. It is later learned that Marlowe had been killed by accident. Viola finds out that Will is still alive, and declares her love for him. Then, Viola is summoned to court to receive approval for her proposed marriage to Lord Wessex. Shakespeare accompanies her, disguised as her female cousin. There, he persuades Wessex to wager £50 that a play can capture the true nature of love, the exact amount Shakespeare requires to buy a share in the Chamberlains Men. Queen Elizabeth I declares that she will judge the matter, when the occasion arises.
 Edmund Tilney, the Curtain, boy actor as Juliet. Following her wedding, Viola learns that the play will be performed that day, and runs away to the Curtain. Planning to watch with the crowd, Viola overhears that the boy playing Juliet cannot perform, and offers to replace him. While she plays Juliet to Shakespeares Romeo, the audience is enthralled, despite the tragic ending, until Master Tilney arrives to arrest everyone for indecency due to Violas presence.
 Twelfth Night".
 Twelfth Night, or What You Will, imagining her as a castaway disguised as a man after a voyage to a strange land. "For she will be my heroine for all time, and her name will be...Viola (Twelfth Night)|Viola."

==Cast==
 
* Gwyneth Paltrow as Viola de Lesseps
* Joseph Fiennes as William Shakespeare
* Geoffrey Rush as Philip Henslowe
* Colin Firth as Lord Wessex Ned Alleyn
* Judi Dench as Elizabeth I of England Edmund Tilney Jim Carter as Ralph Bashford
* Martin Clunes as Richard Burbage
* Antony Sher as Dr. Moth
* Imelda Staunton as Nurse
* Tom Wilkinson as Hugh Fennyman Mark Williams as Wabash
* Daniel Brocklebank as Sam Gosse
* Jill Baker as Lady de Lesseps Will Kempe
* Joe Roberts as John Webster Christopher Kit Marlowe
 

==Production==
The original idea for Shakespeare in Love came to screenwriter Marc Norman in the late 1980s. He pitched a draft screenplay to director Edward Zwick. The screenplay attracted Julia Roberts who agreed to play Viola. However, Zwick disliked Normans screenplay and hired the playwright Tom Stoppard to improve it (Stoppards first major success had been with the William Shakespeare|Shakespeare-themed play Rosencrantz & Guildenstern Are Dead). Peter Biskind, "Down and Dirty Pictures: Miramax, Sundance and the Rise of Independent Film" (New York: Simon & Schuster, 2004), p. 327. 

The film went into production in 1991 at Universal Studios|Universal, with Zwick as director, but although sets and costumes were in construction, Shakespeare had not yet been cast, because Roberts insisted that only Daniel Day-Lewis could play the role.  Day-Lewis was uninterested, and when Roberts failed to persuade him, she withdrew from the film, six weeks before shooting was due to begin. The production went into Turnaround (filmmaking)|turnaround, and Zwick was unable to persuade other studios to take up the screenplay. 
 Miramax interested John Madden as director. Miramax boss Harvey Weinstein acted as producer, and persuaded Ben Affleck to take a small role as Ned Alleyn. 

The film was considerably reworked after the first test screenings. The scene with Shakespeare and Viola in the punt was re-shot, to make it more emotional, and some lines were re-recorded to clarify the reasons why Viola had to marry Wessex. The ending was re-shot several times, until Stoppard eventually came up with the idea of Viola suggesting to Shakespeare that their parting could inspire his next play. 

==References to Elizabethan literature== Zeffirelli Romeo and Juliet. 

Many other plot devices used in the film are common in Shakespearean comedies and other plays of the Elizabethan era: the Queen disguised as a commoner, the cross-dressing disguises, mistaken identities, the sword fight, the suspicion of adultery, the appearance of a "ghost" (cf. Macbeth), and the "play within a play". According to Douglas Brode, the film deftly portrays many of these devices as though the events depicted were the inspiration for Shakespeares own use of them in his plays. 

The film also has sequences in which Shakespeare and the other characters utter words that later appear in his plays, or in other ways echo those plays:
* On the street, Shakespeare hears a Puritan preaching against the two London stages: "The Rose smells thusly rank, by any name! I say, a plague on both their houses!" Two references in one, both to Romeo and Juliet; first, "A rose by any other name would smell as sweet" (Act II, scene ii, lines 1 and 2); second, "a plague on both your houses" (Act III, scene I, line 94).
* Backstage at a performance of The Two Gentlemen of Verona, Shakespeare sees William Kempe in full make-up, silently contemplating a skull, a reference to the gravediggers scene in Hamlet.
* Shakespeare utters the lines "Doubt thou the stars are fire, / Doubt that the sun doth move" (from Hamlet) to Philip Henslowe.
* As Shakespeares writers block is introduced, he is seen crumpling balls of paper and throwing them around his room. They land near props which represent scenes in several of his plays: a skull (Hamlet), and an open chest (The Merchant of Venice).
* Viola, as well as being Paltrows character in the film, is the name of the lead character in Twelfth Night who dresses as a man after the supposed death of her brother.
* At the end of the film, Shakespeare imagines a shipwreck overtaking Viola on her way to America, inspiring the second scene of his next play, Twelfth Night, a scene which also echoes the beginning of The Tempest.
* Shakespeare writes a sonnet to Viola which begins: "Shall I compare thee to a summers day?" (from Sonnet 18).
* Shakespeare tells Henslowe that he still owes him for "one gentleman of Verona", a reference to Two Gentlemen of Verona, part of which we also see being acted before the Queen later in the film.
* In a boat, Shakespeare tells Viola, who is disguised as Thomas Kent, of his lady’s beauty and charms, she dismisses his praise, as no real woman could live up to this ideal, this is a set up for Sonnet 130, “My mistress’ eyes are nothing like the sun”.


  in a scene wherein Marlowe (Rupert Everett) seeks payment for the final act of the play from Richard Burbage (Martin Clunes). Burbage promises the payment the next day, so Marlowe refuses to part with the pages and departs for Deptford, where he is killed.  The only surviving text of The Massacre at Paris is an undated octavo that is probably too short to represent the complete original play. It has been suggested to be a memorial reconstruction by the actors who performed the work. 

The child John Webster who plays with mice is a reference to the leading figure in the next, Jacobean, generation of playwrights. His plays (The Duchess of Malfi, The White Devil) are known for their blood and gore, which is humorously referred to by the child saying that he enjoys Titus Andronicus, and also saying of Romeo and Juliet, when asked his opinion by the Queen, "I liked it when she stabbed herself." 
 Will Kempe Seneca if you played it," a reference to the Roman tragedian renowned for his sombre and bloody plot lines which were a major influence on the development of English tragedy.

Will is shown signing a paper repeatedly, with many relatively illegible signatures visible. This is a reference to the fact that several versions of Shakespeares signature exist, and in each one he spelled his name differently.

==Plot precedents and similarities==
After the films release, certain publications, including Private Eye, noted strong similarities between the film and the 1941 novel No Bed for Bacon, by Caryl Brahms and S. J. Simon, which also features Shakespeare falling in love and finding inspiration for his later plays. In a foreword to a subsequent edition of No Bed for Bacon (which traded on the association by declaring itself "A Story of Shakespeare and Lady Viola in Love") Ned Sherrin, Private Eye insider and former writing partner of Brahms, confirmed that he had lent a copy of the novel to Stoppard after he joined the writing team,  but that the basic plot of the film had been independently developed by Marc Norman, who was unaware of the earlier work.
 Alexandre Duvals Richard III. 
 The Quality 1999 Academy Awards, as "absurd", and argued that the timing "suggests a publicity stunt".  

==Inaccuracies== Arthur Brookes 1562 narrative poem The Tragical History of Romeus and Juliet, which itself was rooted in an Italian original. 

==Reception==
 " and Mel Brooks. Then the movie stirs in a sweet love story, juicy court intrigue, backstage politics and some lovely moments from "Romeo and Juliet"...&nbsp;Is this a movie or an anthology? I didnt care. I was carried along by the wit, the energy and a surprising sweetness."    

The review aggregator Rotten Tomatoes reports that 92% of critics gave the film positive reviews based on 125 reviews and it has an average rating of 8.3 out of 10. The critics consensus states: "Endlessly witty, visually rapturous, and sweetly romantic, Shakespeare in Love is a delightful romantic comedy that succeeds on nearly every level." 

Shakespeare in Love was among  . IMDb. Retrieved 2012-02-19. 
 Prince Edward Sophie Rhys-Jones Queen Elizabeth II to be given the title of Earl of Wessex instead. 

===Accolades===
 

American Film Institute recognition:
* AFIs 100 Years...100 Passions – #50  
* AFIs 100 Years...100 Laughs – Nominated 
* AFIs 100 Years...100 Movies (10th Anniversary Edition) - Nominated 

{| class="wikitable"
|- style="background:#ccc; text-align:center;"
! Award
! Category
! Recipient(s)
! Outcome
|-
| rowspan="13" | 71st Academy Awards    Best Picture
| David Parfitt, Donna Gigliotti, Marc Norman, Harvey Weinstein and Edward Zwick
|  
|- Best Actress
| Gwyneth Paltrow
|  
|- Best Supporting Actress
| Judi Dench
|  
|- Best Art Direction
| Martin Childs and Jill Quertier
|  
|- Best Costume Design Sandy Powell
|  
|- Best Original Musical or Comedy Score
| Stephen Warbeck
|  
|- Best Original Screenplay
| Marc Norman and Tom Stoppard
|  
|- Best Director John Madden
|  
|- Best Supporting Actor
| Geoffrey Rush
|  
|- Best Cinematography
| Richard Greatrex
|  
|- Best Film Editing David Gamble
|  
|- Best Makeup
| Lisa Westcott and Veronica Brebner
|  
|- Best Sound Peter Glossop
|  
|-
| rowspan="15" | 52nd British Academy Film Awards
| BAFTA Award for Best Film
!
|  
|-
| BAFTA Award for Best Actress in a Supporting Role
| Judi Dench
|  
|-
| BAFTA Award for Best Editing
| David Gamble
|  
|-
| BAFTA Award for Best Direction
| John Madden
|  
|-
| BAFTA Award for Best Actor in a Leading Role
| Joseph Fiennes
|  
|-
| BAFTA Award for Best Actress in a Leading Role
| Gwyneth Paltrow
|  
|-
| BAFTA Award for Best Actor in a Supporting Role
| Geoffrey Rush
|  
|-
| BAFTA Award for Best Actor in a Supporting Role
| Tom Wilkinson
|  
|-
| BAFTA Award for Best Cinematography
| Richard Greatrex
|  
|-
| BAFTA Award for Best Original Screenplay
| Marc Norman and Tom Stoppard
|  
|-
| BAFTA Award for Best Makeup and Hair
| Lisa Westcott
|  
|-
| BAFTA Award for Best Sound
| Robin ODonoghue, Dominic Lester, Peter Glossop, and John Downer
|  
|- Anthony Asquith Award for Film Music
| Stephen Warbeck
|  
|-
| BAFTA Award for Best Costume Design Sandy Powell
|  
|-
| BAFTA Award for Best Production Design
| Martin Childs
|  
|-
| rowspan="2" | 49th Berlin International Film Festival   
| Golden Bear
!
|  
|- Silver Bear
| Marc Norman and Tom Stoppard
|  
|-
| Directors Guild of America Awards 1998
| Outstanding Directorial Achievement in Motion Pictures
| John Madden
|  
|-
| rowspan="6" | 56th Golden Globe Awards
| Golden Globe Award for Best Motion Picture – Musical or Comedy
!
|  
|-
| Golden Globe Award for Best Actress – Motion Picture Musical or Comedy
| Gwyneth Paltrow
|  
|-
| Golden Globe Award for Best Screenplay
| Marc Norman and Tom Stoppard
|  
|-
| Golden Globe Award for Best Director
| John Madden
|  
|-
| Golden Globe Award for Best Supporting Actor – Motion Picture
| Geoffrey Rush
|  
|-
| Golden Globe Award for Best Supporting Actress – Motion Picture
| Judi Dench
|  
|-
| rowspan="5" | 5th Screen Actors Guild Awards Outstanding Performance by a Cast in a Motion Picture
!
|  
|- Outstanding Performance by a Male Actor in a Leading Role
| Joseph Fiennes
|  
|- Outstanding Performance by a Female Actor in a Leading Role
| Gwyneth Paltrow
|  
|- Outstanding Performance by a Male Actor in a Supporting Role
| Geoffrey Rush
|  
|- Outstanding Performance by a Female Actor in a Supporting Role
| Judi Dench
|  
|-
| Writers Guild of America Awards 1998
| Best Original Screenplay
| Marc Norman and Tom Stoppard
|  
|-
| 1998 New York Film Critics Circle Awards
| Best Screenplay
| Marc Norman and Tom Stoppard
|  
|}

==Cultural influence==
* The film was spoofed and homaged, along with Star Wars, in the 1999 short film George Lucas in Love.
* The film was seen and frequently interrupted by Brenda Meeks in Scary Movie.

==Stage adaptation==
  Sonia Friedman Productions.  The production was officially announced in November 2013.   

The production opened at the Noël Coward Theatre in West End of London|Londons West End on 23 July 2014, receiving rave reviews from critics, calling it "A joyous celebration of theatre"  ,"Joyous"   and "A love letter to theatre"   
 Lee Hall. The production was directed by Declan Donnellan and designed by Nick Ormerod, the joint founders of Cheek by Jowl.

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 