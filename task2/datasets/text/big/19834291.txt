Children, Mother, and the General
 
{{Infobox film
| name           = Children, Mother, and the General
| image          = 
| caption        = 
| director       = László Benedek
| producer       = Erich Pommer
| writer         = Herbert Reinecker Laszlo Benedek
| starring       = Hilde Krahl
| music          = 
| cinematography = Günther Rittau
| editing        = Anneliese Artelt
| distributor    = 
| released       =  
| runtime        = 110 minutes
| country        = West Germany
| language       = German
| budget         = 
}}

Children, Mother, and the General    ( , and also released as Sons, Mothers, and a General)    is a 1955 West German war film directed by László Benedek and starring Hilde Krahl. 

==Cast==
* Hilde Krahl – Helene Asmussen
* Ewald Balser – General
* Therese Giehse – Elfriede Bergmann
* Ursula Herking – Dr. Behrens, Ärztin
* Alice Treff – Pastorin
* Beate Koepnick – Inge
* Marianne Sinclair – Näherin
* Adi Lödel – Harald Asmussen
* Dieter Straub – Leo Bergmann
* Holger Hildmann – Sohn der Ärztin
* Karl-Michael Kuntz – Edmund, Sohn der Pastorin
* Walter Lehfeld – Werner, Sohn der Näherin
* Peter Burger – Robert, Inges Bruder
* Bernhard Wicki – Hauptmann Dornberg
* Claus Biederstaedt – Verpflegungsgefreiter
* Rudolf Fernau – Stabsarzt
* Hans Christian Blech – Feldwebel mit den Orden
* Klaus Kinski – Leutnant
* Maximilian Schell – Deserteur
* Alfred Schieske – Fahrer mit der Flasche
* Otto Lüthje – Bahnhofsvorsteher

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 

 
 