The Ducktators
 
 

 

The Ducktators is a Looney Tunes black and white cartoon that was produced by Leon Schlesinger Productions and was released in 1942 by Warner Bros.. Directed by Norman McCabe, the cartoon satirizes various events of World War II. The title is a pun on dictator.

==Story==
The cartoon takes place on a farm, beginning with a few ducks, geese and chickens being shown. After a brief few moments, two ducks (who are expecting a duckling) go over to their unusually black-colored egg as it starts to hatch. Once the egg has hatched, a duckling with an Adolf Hitler appearance emerges and yells "Sieg Heil!" while giving the Nazi salute.

After time passes, the baby Hitler duck grows into adulthood, holding aggressive speeches toward other ducks and geese. One goose, representing  , grieves at what is happening ("Have they forgot? Tis love thats right, and naught is gained by show of might.").

A group of chickens and the Mussolini goose are then seen at what appears to be a Peace Conference, in which the Hitler duck slaps the Mussolini goose for dim-wittedly taking part in it and puts the newly signed peace treaty through a Paper shredder|shredder, whereupon everyone in the Peace Conference area brawl against each other.
 Japanese Mandate Island" written on the back of the flag) on a tiny island that turns out to be a turtle, who emerges from the water and chases the duck, beating him with the "Japanese Mandate Island" sign as he is now on the warpath against Tojo. The Japanese duck tries to get out of being beaten by flashing a badge that reads, "I Am Chinese people|Chinese," but to no avail. "And IM mock turtle soup!" shouts the infuriated turtle (a subtitle below the badge states that it was made in Japan, which may explain why the turtle didnt buy the ruse).

The three (Hitler Duck, Mussolini Goose and Tojo Duck) are then seen marching across a field while singing a parody of "One, Two, Buckle My Shoe".
 Jerry Colonna and possibly representing Joseph Stalin) wielding a large wooden hammer for a weapon and hiding in a barrel which he also uses for transportation and a human man carrying a sniper rifle that emerges from a poster that says "For Victory, Buy United States Victory Bonds" and fires at the fleeing axis powers, the Dove overthrows the Axis Powers and saves the day!
 trophies above his fireplace in beaten up appearances.
 states to United States Savings Bonds and Revenue stamp|Stamps.

==Political references== Italian accent, and the chick at the end of one of his speeches could be heard yelling "Duce!" multiple times, indicating that this goose is Benito Mussolini. The third who was also a duck was using the flag of the Empire of Japan, and openly singing "The Japanese Sandman". The caricature is a reference to Japan in gestalt, but can be interpreted as referring specifically to either Hideki Tojo or Hirohito.

==Changes in Sunset Productions version==
The ending where the dove is sitting with his kids and points out that his enemies have been defeated (and are now moose-heads above the fireplace), followed by a notice to buy war bonds, has been rarely seen since the short was sold to   DVD set and the World War II cartoons special on the Cartoon Network show ToonHeads, the full ending is shown uncut and uncensored. 

==Availability==
* The Ducktators is available (uncut) on  , Disc 2.

==See also==
* List of World War II short films
* List of films in the public domain

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 