A Moment of Romance
 
 
{{Infobox film
| name           = A Moment of Romance
| image          = AMomentofRomance.jpg
| caption        = Poster of A Moment of Romance
| film name = {{Film name| traditional    = 天若有情
| simplified     = 天若有情
| pinyin         = Tiān Ruò Yǒu Qíng
| jyutping       = Tin1 Jeok6 Jau5 Cing4}} Benny Chan
| producer       = Johnnie To
| writer         = James Yuen
| starring       = Andy Lau Jacklyn Wu Ng Man-tat
| music          = Lo Tayu Fabio Carli
| cinematography = Joe Chan Horace Wong Patrick Jim
| editing        = Wong Ming-Gong
| studio         = Movie Impact Paka Hill Film Production
| distributor    = Newport Entertainment
| released       =  
| runtime        = 88 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         =
| gross          = HK$12,899,353
}} 1990 Cinema Hong Kong action romance Benny Chan, Johnny To, Best Supporting Actor at the 10th Hong Kong Film Awards. 
 underworld rag and a rich heiress and is considered a major classic of Hong Kong cinema. Because of the film, Andy Lau was nicknamed "Wah Dee" (華Dee), the character he portrays in the film and is one of Laus representative works.

==Plot== triad gangster, getaway driver in a jewelry store robbery. When the raid goes wrong, he takes a young woman named Jo Jo (Jacklyn Wu) hostage. The head of Wah Dees gang, Trumpet, demands that she be killed, but Wah Dee resists and saves her.  Despite the fact that Wah Dee and Jo Jo come from different backgrounds, the two fall in love but still face many difficulties.

==Cast==
* Andy Lau as Wah Dee
* Jacklyn Wu as Jo Jo Huen
* Ng Man-tat as Rambo
* Wong Kwong-Leung as Trumpet
* Chu Tiet-Wo 
* Lam Chung 
* Sandra Lang
* Lau Kong 
* Yuen Bun 
* Liang Shan

 {{Cite web |url=http://www.imdb.com/title/tt0100777/ |title=A Moment of Romance 
 |accessdate=30 July 2010 |publisher=imdb.com}} 
   

==Crew==
*Presenter: Wallace Chung
*Planning: Ringo Lam, Wong Jing, Ise Cheng
*Action Director: Yuen Bun
*Car Stunts: Bruce Law, Joe Chu
*Art Director: Ringo Chueng
*Costume Designer: Lee Yuk Shing, Yam Kam Jan
*Assistant Director: Chu Yat Hung, Law Sai Kuen, Bosco Lam
*Makeup: Wong Lai Kuen
*Hair Stylist: Chan Tat Ming
 

==Songs==
===Theme song===
*If The World Had Romance (天若有情)   / No Regrets of Youth (青春無悔)  
**Composer: Lo Tayu
**Lyricist: Lee Kin Tat  , Lo Tayu  
**Singer: Shirley Yuen

===Insert theme===
*Gray Track (灰色軌跡)   / Dark Space (漆黑的空間)  
**Composer: Wong Ka Kui
**Lyricist: Gene Lau
**Singer: Wong Ka Kui

*Never Regretted (未曾後悔)   / Short Term Gentleness (短暫的溫柔)  
**Composer: Wong Ka Kui
**Lyricist: Wong Ka Keung  , Gene Lau   Paul Wong

*Regardless If Its Wrong (是錯也再不分)  / No Need to Understand So Much (不需要太懂)  
**Composer: Wong Ka Kui
**Lyricist: :Paul Wong  , Mike Lau  
**Singer: Wong Ka Keung

==Awards and nominations==
*10th Hong Kong Film Awards Best Supporting Actor (Ng Man-tat) Best New Performer (Jacklyn Wu)
**Nominated: Best Original Film Score (Lo Tayu, Fabio Carli)
**Nominated: Best Original Film Song (If The World Had Romance (天若有情) - Composer: Lo Tayu, Lyricist: Lee Kin Tat, Singer: Shirley Yuen)

==See also==
*Andy Lau filmography
*Johnnie To filmography
*Wong Jing filmography
*List of Hong Kong films
*List of biker films

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 