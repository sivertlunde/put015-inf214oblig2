Why Has Bodhi-Dharma Left for the East?
{{Infobox film
| name           = Why Has Bodhi-Dharma Left for the East?
| image          = Why Has Bodhi-Dharma Left for the East?.jpg
| caption        = Poster to Why Has Bodhi-Dharma Left for the East?
| director       = Bae Yong-kyun 
| producer       = Bae Yong-kyun Productions
| writer         = Bae Yong-kyun
| starring       = Lee Pan-yong Sin Won-sop Yi Pan-Yong
| music          = Jin Gyu-yeong
| cinematography = Bae Yong-kyun
| editing        = Bae Yong-kyun
| distributor    = Bae Yong-kyun Productions
| released       =  
| runtime        = 175 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          = 
| film name = {{Film name
 | hangul         =  ?
 | hanja          =  가  쪽으로 간 까닭은?
 | rr             = Dalmaga dongjjokeuro gan ggadalkeun?
 | mr             = Talmaka tongtchokŭro kan kkadalgŭn?
 | context        = }}
}}
Why Has Bodhi-Dharma Left for the East? (Hangul|달마가 동쪽으로 간 까닭은? - Dalmaga dongjjok-euro gan ggadakeun?) (1989 in film|1989) is an award-winning South Korean film written, produced and directed by Bae Yong-kyun, a professor at Dongguk University in Seoul. Known principally as a painter, {{cite web |url=http://wc03.allmovie.com/cg/avg.dll?p=avg&sql=1:120417|title=Why Has Bodhi-Dharma Left for the East?
|accessdate=2007-12-27|last=Brennan|first=Sandra|publisher=Allmovie}} 
  Bae spent seven years making this film with one camera and editing it by hand.  The film was screened in the Un Certain Regard section at the 1989 Cannes Film Festival.   

==Synopsis== Seon Buddhist views on life, Why Has Bodhi-Dharma Left for the East? follows the lives of three Buddhist monks: a child, an adult, and an old man. 

This film is largely about two Zen koans. The koans are: What is my original face before my mother and father were conceived?, and (In death,) where does the master of my being go?

The films title, although not explained literally in the film, is a reference to Bodhidharma, a 6th-century Buddhist monk from India who transmitted Zen to China. The question, "Why has Bodhi-Dharma left for the east," is echoed by the young boys question, "Why have we all left the world?" The entire film can be seen as an answering of that question, so that the film itself becomes a koan.

The three main protagonists are Haejin, an orphan boy, Kibong, a young monk, and Hyegok, a Zen master. 

Haejin injures a bird while bathing. Its mate does not leave the bird, but stays around, as if to see what becomes of its mate. Heijin takes the injured bird away to heal it, but it dies later in the film. 

The movie then cuts to a scene in which an ox breaks through a confining fence and escapes to the forest. The ox represents Kibongs need to escape, as well as his own blind passions. The ox, like the young man, escapes its confinement but isnt truly free. It seems free, but really is still subject to its own desires. The young man also escapes the confinement of the world, but is still plagued by his own blind passions. The ox can also be seen as representing the blind passions of the young boy, escaping the walls of his humanity and running amok.

The abbot instructs a young monk (Kibong) to assist a Zen master living alone in the mountains. The young monk has renounced his life of hardship in search of peace and perfection.

The Zen master is a recluse, living in a monastery on a high mountain, and has come to realise the vanity of knowledge. He also knows the secret of everlasting peace. Dogged by chilblains (inflammation of the hands and feet caused by exposure to cold and moisture), he tries to lead others to the same realisation, a realisation which comes only with ardent devotion, not simply through knowledge or worship. 

The old Master mainly tries to communicate his Way through the use of koans - Zen riddles with no absolute answers. The first koan is: What is my original face before my father and mother were conceived? The second koan is, When the moon takes over in your heart (that is, in enlightenment or in death), where does the master of my being go? He instructs the young monk to "hold the koan between his teeth" and to solve them. In solving the koans, the old Master tells the young monk that he will find an unshakeable peace.

The young monk takes leave of the old monk and goes to town, where he buys medicine for the old man with alms from begging. He also visits his blind mother, who is having a hard time tending to herself. The young monk returns to the monastery, disillusioned and appalled at his own selfishness in renouncing his destiny, which was to serve his mother and family. 

He returns to the old monk and communicates his desire to go back to life, to embrace the filth of humanity and face the turmoil of day-to-day existence in the swarm of people. He is severely reprimanded by the monk, who however does not prevent him. The young monk leaves the monastery to return to his old life, but is caught in a flash flood and nearly drowned. He is found sitting on a rock in the middle of rapids by Haejin. The young monk is then rescued by his Master.

When the young monk regains consciousness, he is informed by the boy that his Master had been in meditation for quite a while and is severely ill. Kibong realizes that the Master has traded his own life to save him. Deeply moved, he visits the old Master, who extracts a promise from Kibong to perform his last rites as the old man wishes. His wish is for his body to be burned on top of the hill so that he can return to his original place.

News of a festivity on the approaching full-moon day reaches Kibong, who wishes to attend it with the boy. Hyegok, apparently feeling better, gives them leave, assuring Kibong that he would manage well by himself. The old man also asks them to bring enough kerosene for him. 

At the festival, Kibong and Heijin watch the enthralling dance, while it is made known that the dancer is none other than the old monk himself, in another form. In his monastery, he is slowly making preparations himself. On a bright full-moon night, Heijin and Kibong make their way back. On returning to the monastery, the boy smells burnt medicine; they quickly discover that the old man is dead.

True to his promise, with the true meaning and meaninglessness of death, possession, desire and vanity dawning on him every passing instant, Kibong stuffs the dead body in a wooden chest and slowly starts a difficult trek up the hillside. He carries the chest on a firewood pack – presumably the same pack the young monk had used at the beginning of the film to haul firewood to the monastery. The meaning here is, in death, the body is no different from firewood. This idea was stated earlier by the old man when he told the little boy that the boys extracted tooth is no different from a pebble in the road. 

By nightfall, the young monk reaches the burial ground. He tries to light the pyre, but there is a light drizzle and he cant get it started. He suddenly remembers his masters words about the kerosene. He goes back to the monastery and collects the kerosene. He then returns to the burial ground and sets fire to the coffin. He spends the night by the side of the burning body, tortured by his feelings and coming to the full realization of death.

Looking around the burning body, we see the ox and the young boy. Both seem transfixed by the blaze. Even the blind passions and foolishness of youth are subdued in death.

In the morning, after the funeral pyre has burned down, we see the young monk kneeling and sifting his fingers through the ashes. In this scene, the young monk is fully encountering the true realization of death. He seems to be looking for something in the ashes, and finds the last few remaining bones of his former master. Presumably, these few small bones are the “master” of the old mans being. They are what is left after everything else is burned away. They are death. The young monk collects these bones and grinds them to powder with a stone. He then walks through the forest, scattering the powdered bones over water, earth, trees and plants.

When the young monk scatters the old mans powdered bones, he returns the old man to his original place, as the old man had said must happen. The old mans original place is everywhere, just as his original face is everything. With this realization, the young monk finally solves the koans and attains the unshakeable peace the old man had spoken about. He then returns to the monastery. Seeking out Heijin, he entrusts the boy with the Masters few remaining possessions. He then takes his leave.

In a magnificent final sequence, we see the boy coming of age: In a play scene, the young boy re-enacts the previous nights event by burning the old mans few personal belongings. The boy has done in miniature what the young monk had done the night before. In this act, the boy remembers the old mans teachings and comes to understand the nature of impermanence. He wakes up the next day and goes to the stream to collect water. As usual the dead birds companion chirps to distract him (the chirping bird represents the sound of death), but this time the boy does not even notice it. Understanding impermanence, his education is complete. He enters the Masters room and closes it after him. An old Master has died and a new Master makes his beginning. The bird flies away, liberated. The wandering ox (which had always been shown before in shadow) returns with a man (possibly Kibong), walking beside him in sunlight. It too has found peace. Rather than having to face the choice between confinement or escape, the blind passions now walk alongside the human. Both the human and the passions find their true place.

==Awards==
* 1989 Locarno International Film Festival, the Golden Leopard (Best Film) and Prize of the Ecumenical Jury 

The film enjoyed immense critical popularity in the early 1990s, which has since been on the wane. 

==Notes==
 

==Bibliography==
* {{cite web |url=http://wc03.allmovie.com/cg/avg.dll?p=avg&sql=1:120417|title=Why Has Bodhi-Dharma Left for the East?
|accessdate=2007-12-27|last=Brennan|first=Sandra|publisher=Allmovie}} 
* 
* 
* 

==External links==
*  

==See also==
*Korean Buddhism

 

 
 
 
 
 