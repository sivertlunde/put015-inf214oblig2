Take It Easy (2015 film)
{{Infobox film
| name= Take It Easy
| image= Take It Easy.jpg
| director=  Sunil Prem Vyas
| Producer= Dharmesh Pandit
| writer=  Sunil Prem Vyas
| story= Sunil Prem Vyas
| starring=  Vikram Gokhale  Dipannita Sharma Raj Zutshi Anang Desai Joy Sengupta Sulbha Arya Supriya Karnik
| studio         = Prince Productions
| distributor    = Hash Entertainment 
| released       =  
| country        = India
| language       = Hindi
}} Hindi childrens film written and directed by Sunil Prem Vyas and produced by Dharmesh Pandit. The film is starring Vikram Gokhale, Raj Zutshi, Supriya Karnik, Dipannita Sharma  and Anang Desai.    Upon release the film received mixed reviews. 

==Plot==
The protagonists are both almost ten-year-old children. The father of one of them is a player who wants hist son to become one too, but his son is more interested in Studying. The other boys story is contary. The Parents are imposing the dreams.

==Cast==
* Vikram Gokhale
* Dipannita Sharma
* Raj Zutshi
* Anang Desai
* Joy Sengupta
* Sulbha Arya
* Vijay Kashyap
* Supriya Karnik
* Jyoti Gauba
* Yash Ghanekar
* Prasad Reddy
* Smera Jadhav

==Soundtrack==
* Maa Sunn Le Zara
* Mushkil Hai – Male
* Mushkil Hai – Female 
* O Kaat
* Tu Bechaara Mei Bechaara 
* Nanhe Se (Theme Song)
* Background Theme Score 
* Take It Easy Yaa

==Critical reception==

Bollywood Flick.in gave it 3/5 stars and said,
 The movie is very good and has the potential to convince modern day families to try to understand their childs mentality and to not impose any unbearable burden on their child.Dharmendra Pandit and Narendra Singh want to convince every parent. They have produced a master work in the film industry.  Shubhra Gupta of The Indian Express gave it 1.5/5 stars, stating,  With some easy moments, this film could have been easier to watch, but not when there is no break from loud background music, loud melodrama, and loud dialogues. Even the children, who all try hard to be as natural as possible, are weighed down under all the preachiness. 
Renuka Vyavahare of The Times of India gave it 2/5 stars and opined,  Their message of how everything is judged on the basis of money than character today comes across clearly. However, the execution could have been crisper, more authentic. The story keeps beating around the bush for no reason, when the impact has already been made. The dramatization is uncalled for, especially towards the end. What could have been an otherwise heartrending climax, gets faltered when stretched for no rhyme or reason. Why beg for tears?"   Webduniya gave it 2/5 stars. Shakti Shetty of Mid-day gave it 3/5 stars and opined,  On the big screen, both Yash Ghanekar as well as Prasad Reddy put in a fine performance as rivals-turned-BFFs. Their more seasoned colleagues are remarkable but still, the story revolves around the two. They are the undisputed stars. Last year hardly witnessed a memorable children’s film. With this venture, one can now expect many more from 2015. 

==References==
 
==External links==
 
 
 
 
 
 