Hang Your Dog in the Wind
{{Infobox Film
| name           = Hang Your Dog in the Wind
| image          = hangyourdog.jpg
| image_size     =
| caption        = Screenshot of Hang Your Dog in the Wind
| director       = Brian Flemming
| producer       = Stann Nakazono
| writer         = Brian Flemming
| narrator       = 
| starring       = Steve Wilcox Dave James Keythe Farley
| music          = 
| cinematography = Brett Webster
| editing        = Jacob Craycroft
| distributor    = 
| released       = 
| runtime        = 
| country        =  English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

Hang Your Dog in the Wind was the first feature film of Brian Flemming.  It was shot in black and white and Super 16 in 1993 then blown up to 35mm.  Although it was not accepted by either the Sundance Film Festival or the Slamdance Film Festival in 1997, it was released as part of film festival created by Flemming and associates,  "The 1997 Slumdance Experience." 

==See also==
*Brian Flemming
*The God Who Wasnt There

==References==
 

==External links==
*  

 
 


 