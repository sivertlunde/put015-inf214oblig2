Whistling in Brooklyn
{{Infobox film
| name           = Whistling in Brooklyn
| image          = Whistling in brooklyn.jpg
| image size     = 
| caption        = Theatrical release poster
| director       = S. Sylvan Simon
| producer       = George Haight
| writer         = Nat Perrin Wilkie C. Mahoney (add. dialogue) Stanley Roberts (uncredited)
| starring       = Red Skelton Ann Rutherford Jean Rogers
| music          = George Bassman
| cinematography = Lester White Ben Lewis
| distributor    = MGM
| released       =  
| runtime        = 87 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Whistling in the Dark and Whistling in Dixie. Wally prepares to marry his girlfriend, but gets sidetracked when he is mistaken for a serial murderer. Leo Durocher makes his screen debut, playing himself.

==Cast==
* Red Skelton as Wally Benton
* Ann Rutherford as Carol Lambert
* Jean Rogers as Jean Pringle
* Rags Ragland as Chester Ray Collins as Grover Kendall
* Henry ONeill as Inspector Holcomb
* William Frawley as Detective Ramsey
* Sam Levene as Creeper
* Arthur Space as Detective MacKenzie
* Robert Emmett OConnor as Detective Leo Finnigan Steve Geray as Whitey
* Howard Freeman as Steve Conlon
* Mike Mazurki as thug

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 


 
 