The Indian Love Lyrics
{{Infobox film
| name           = The Indian Love Lyrics
| image          =
| caption        =
| director       = Sinclair Hill
| producer       =
| writer         = Lawrence Hope (poem) Sinclair Hill
| starring       = Catherine Calvert Owen Nares Malvina Longfellow
| cinematography = D.P. Cooper
| editing        = H. Leslie Brittain
| studio         = Stoll Pictures
| distributor    = Stoll Pictures
| released       =  
| runtime        =
| country        = United Kingdom
| awards         =
| language       = Silent English intertitles
| budget         =
}} silent romantic drama film directed by Sinclair Hill and starring Catherine Calvert, Owen Nares and Malvina Longfellow.  It is based on the poem The Garden of Karma by Lawrence Hope. The films sets were designed by art director Walter Murton.

In India, a Princess disguises herself as a commoner to escape an arranged marriage.

==Cast==
* Catherine Calvert as Queen Vashti 
* Owen Nares as Prince Zahindin  
* Malvina Longfellow as Princess Nadira  
* Shayle Gardner as Ahmed Khan  
* Fred Raynham as Ibrahim-bey-Israel  
* Roy Travers as Hassan Ali Khan   William Parry as Mustapha Khan   William Ramsey as Sultan Abdul Rahin 
* Daisy Campbell as Sultana Manavour 
* Fred Rains as Selim  
* Pino Conti as Youssef 
* Arthur McLaglen as Champion

==References==
 

==Bibliography==
* Low, Rachael. History of the British Film, 1918-1929. George Allen & Unwin, 1971.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 

 