Rob the Mob
{{Infobox film
| name           = Rob the Mob
| image          = Rob the Mob.jpg
| alt            = 
| caption        = 
| director       = Raymond De Felitta
| producer       = William Teitler   William Kay
| writer         = Jonathan Fernandez
| starring       = Michael Pitt   Nina Arianda   Ray Romano   Andy García  
| music          = 
| cinematography = Chris Norr
| editing        = David Leonard
| studio         = The Exchange
| distributor    = Millennium Entertainment
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = $208,693 
}}

Rob the Mob is a 2014 American crime drama film directed by Raymond De Felitta and written by Jonathan Fernandez. The film stars Michael Pitt, Nina Arianda, Andy García, Ray Romano, Aida Turturro, Frank Whaley, Michael Rispoli and Joseph R. Gannascoli. The film is based on a true story. 

== Plot == Tommy Uva (Pitt) and Rosie DeToma (Arianda) getting high and robbing a florist on Valentines Day. Tommy holds up the cashier with a pistol while Rosie waited in the car she inherited from her father. Tommy is arrested and sent to prison for 18 months. In the meantime, Rosie gets a job at a debt collection agency run by Dave Lovell (Dunne), who went to jail for bilking major companies out of $800,000 through fraudulent invoices. Lovell reformed his life and mainly hires ex-cons at his firm. He is happy to give Tommy a job and a second chance. Tommy is restless and instead of following the call script, he often counsels people on how to defraud Lovells company. 

When Tommy sees some mafiosos in his neighborhood, he grows enraged. He calls them fat old guys living off their reputation, and he resents the way the mafia treated his father. Over the course of the film, it is revealed that Tommys father started his shop with a mafia loan, and he suffered frequent beatings when he was late with payments. Tommys mother and brother still run the shop, but his mother blames Tommys criminal activity for breaking her husbands heart and leading to his death. Whereas, Tommy blames his fathers death completely on the humiliation he suffered at the hands of the mafia.
 Sammy the Bulls testimony, and he perks up when Sammy explains that no guns are allowed in mafia social clubs. He cases one of the clubs that Sammy mentioned, and then he pitches the idea to rob the club to Rosie. He explains that it will be safe since no one will have a gun, and that the mafia would never call the cops to report the crime. He gets an Uzi, which he does not know how to use. Rosie reluctantly shows him how to load it, and agrees to the plan. 

During the robbery, Tommy shoots the Uzi wildly in the air, eventually convincing the mafiosos to hand over their jewelry and cash rolls. He also humiliates them by messing with their hair and making them simulate sexual intercourse with each other. He leaves the club by shooting up the walls and saying, "This is for Frankie Uva". Rosie and Tommy are thrilled with their score, but the mafia are furious. Sal (Rispoli) consults with his boss, Big Al Fiorello (Garcia), about what to do. Big Al wants the duo found and scared, but not killed, because of the heavy scrutiny on the mob due to Gottis trial.

Tommy and Rosie eventually run out of money and rob another club. Tommy humiliates the mobsters this time by making them strip to their underwear. FBI Agents have the club under surveillance, and they are stunned at the sound of gunfire. When Tommy flees, the mobsters chase after him out of the club. Agent Frank Hurd (Whaley) photographs several of them standing on the street in their underwear looking bewildered. 

Hurd shares the story with New York Post columnist Jerry Cardozo (Romano), even reluctantly agreeing to let Cardozo publish one of the pictures. It becomes a front page story, and a deep humiliation to the mob. Tommy and Rosie convince Lovell to skip work one day and attend the trial with them. Lovell is as enthralled by Sammy the Bulls testimony as they are. Sammy mentions another social club (The Waikiki). On the way back to the office, Lovell notices an uzi under a blanket in the back seat. 

When Tommy robs the Waikiki club, he is dismayed that there are only three very old men in there playing cards. One of them, Joey D (Young) begs for his wallet back, claiming it is a gift from his dead wife. In the wallet, Tommy and Rosie discover a list of the entire mafia organization, including names, ranks, phone numbers, and addresses. They decide to warn the mafia that they have the list, because they feel it is insurance against retribution. Rosie even calls Big Al at home to warn him that they have the list.

Big Al is furious at Sal, who explains that Joey D had the list because Sal thought no one would think such an old man had such an important piece of evidence. The list is essentially a phone tree, meant to function as a pass by mobsters who get arrested. The mobster under arrest calls the guy with the list to signal he will not be cooperating with authorities, and the guy with the list notifies everyone on it so that they can know the authorities are bluffing if they say that the arrested mobster has turned into an informant. The FBI has always wanted such a document because it undermines the standard mafia defense that they are not a real organization. 

Big Al summons everyone to a meeting and puts out a contract on Tommy and Rosie, who have become known as "Bonnie and Clyde". Meanwhile, Rosie calls Cardozo to complain that he has underreported the number of clubs that they robbed. She sets up an interview with Cardozo, and after initially protesting, Tommy joins her in talking to Cardozo. His profile of the couple only humiliates the mafia further, but it draws far too much attention to Tommy and Rosie. After reading the article, Lovell warns Rosie about the danger she is in. 

One day when Sal is driving through the neighborhood, he nearly runs into Tommy and Rosie, who he recognizes because of their car. He follows them to their apartment, and he is stunned to find they live 3 blocks from the first club they robbed. He tells his underlings where they live, and orders them to kill them. 

The FBI infiltrates Tommy and Rosies apartment and makes a copy of the list, which they use to arrest Big Al. Cardozo is outraged to learn that the FBI will not give Tommy and Rosie any protection for the role that they played in the investigation. Cardozo tries to give airline tickets to Mexico to Tommy and Rosie. He warns them that they are in way over their heads. Tommy and Rosie claim they have a plan. They drive into Manhattan to view the Rockefeller Center Christmas tree and the holiday windows on Fifth Avenue. At the end of their day, they are ambushed in their car and killed.

A photo of Tommy and Rosemarie Uva is displayed over some final title cards, which claim that the list they stole lead to several mob convictions.

== Cast == Tommy Uva Rosie Uva
* Andy García as Big Al
* Ray Romano  as Jerry Cardozo
* Aida Turturro as Anna
* Frank Whaley as Agent Frank Hurd
* Michael Rispoli as Sal Dom
* Burt Young as Joseph Delmonico (aka Joey D)  
* Griffin Dunne as Dave Lovell
* Yul Vazquez as Vinny Gorgeous
* Aimee Mullins as Carrie
* Silvestre Rasuk as Homeless Man
* Luke Fava as Robbie
* Adam Trese as Assistant Director Ryan
* Garry Pastore as Sammy
* Samira Wiley as Agent Annie Bell
* Matthew Blumm as Marco
* Danielle Montezinos as Jumela
* Teddy Coluca as Joe Butch

== Production ==
The filming began in May 2013 in Manhattan, New York City. 

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 


 