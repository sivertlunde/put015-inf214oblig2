Navaratri (1966 film)
{{Infobox film
| name           = Navaratri
| image          =
| image_size     =
| caption        =
| director       = Tatineni Rama Rao
| producer       = Anumolu Venkata Subba Rao
| writer         = A. P. Nagarajan (story) Mullapudi Venkata Ramana (dialogues)
| narrator       = Savitri Kongara Suryakantam
| music          = T. Chalapathi Rao
| cinematography = P. N. Selvaraj
| editing        = J. Krishna Swamy
| studio         = Saradhi Studios
| distributor    =
| released       = 1966
| runtime        =
| country        = India Telugu
| budget         =
}}
 Tamil film same title, Girija make a guest appearance as patients admitted in mental hospital.

Lead Actor (Akkineni Nageswara Rao) got high critical acclaim for his portrayal of Nine Characters. This film is regarded to be having some of the finest ever performances by Nageswara Rao.

The plot of the movie is totally based on what happens during Navaratri (Nava means Nine and Ratri means Night).

==Plot==
The film opens with the narrator explaining that there are nine types of human behaviors (Navarasam) such as Wonder, Fear, Compassion,  Anger, Equanimity, Disgust, Elegance, Bravery and Bliss and that Nageswara Raos nine roles represents one character per role.

Radha (Savitri (actress)|Savitri) is the only daughter of a rich man. When she happily celebrates Navaratri festival at her home with her friends her father informs about the visit of a  groom and his parents for her wedding. Radha is reluctant to the proposal as she wants to marry her college mate Venu Gopal. After arguments with her father, she leaves away from home without her fathers knowledge at the first Navaratri night.

First Night (Wonder): She searches for her lover in the college hostel but finds that he has gone to get married. Radha felt cheated by Venu and attempts for suicide where she is stopped by a widower Anand (Akkineni Nageswara Rao). He takes her to his house and introduces her to his daughter. He urges her to tell her address to drop her safely. Unwilling to return home she leaves the place the next early morning.

Second Night (Fear): The next day she damages the pot of a vendor. When vendor fights with her she was rescued by a homely looking woman. The woman takes Radha to her home. Radha meets several women in her house. But the house is actually a brothel house. She is trapped to a drunkard (Akkineni Nageswara Rao). The drunkard justifies that he cannot seduce his own wife as she is a T.B patient. Though he does not want to betray his wife, he is not able to resist his feelings. Radha advises and warns him in order to escape from him but he does not want to hear. After much struggle the drunkard falls into the floor and faints. Radha escapes from the place.

Third night (Compassion): After escaping from the brothel house, Radha is caught by a patrol police for wandering into the road at unusual time. At police station she pretends to be a mentally ill woman. The police admits her at a mental hospital. The old aged lonely doctor Karunakar (Akkineni Nageswara Rao) understands that Radha is fine but pretends to escape from cops and so he helps her. She stays in the hospital the whole night. Meanwhile, Radhas father comes, in search of her, to the Doctor. Doctor, eventually understands the situation and asks one of the nurses to call Radha. But Radha escapes.

Fourth Night (Anger): Radha misunderstands the police jeep is coming for her. She dashes with a man with a gun (Akkineni Nageswara Rao) and faints. The man takes her to his place. Radha understands police is not looking for her but looking for the man as he is a killer who killed a rich businessman as a revenge for his brothers death. The gun man insists Radha to leave. But she doesnt as she feels he is a good person and convinces him to surrender to police. In an attack the gunman is killed by the businessmans henchmen. Radha escapes from the place.

Fifth night (Equanimity): Radha runs into a farmer group cheering and dancing while harvesting the crop. An innocent villager (Akkineni Nageswara Rao), takes her to his house and introduces to his elder sister. A local priest (Relangi) visits their home and tells that Radha is possessed by a spirit in order to cheat for money by performing some fake rituals. Radha gets irritated by their acts and escapes away that night.

Sixth Night (Disgust): Radha meets an old aged leper (Akkineni Nageswara Rao), who once upon a time was a rich man. The man lost all his money in the treatment and charity (hoping that will help him from disease). He is disgusted by everyone including his own son who discarded him when his money ran out. Radha helps him by taking him to a hospital. The doctor is surprised as he is one of the beneficiers who was benefitted with medicine degree by charity of the rich man. The doctor decides to stay with him until he is cured. Radha leaves the hospital.

Seventh Night (Elegance): Radha feels very tired and asks for water from one of the houses. People offer water for her. One of the men "Bhagavatha Rao" (Akkineni Nageswara Rao), is a director and actor of stage plays and road side plays. They have been committed for nine stage plays in the village on the account of Navaratri celebration. But the heroine elopes with a harmonium player and his whole troop are in critical position in search of a replacement for the seventh day play failing on which will make them to lose their money and reputation in the village. He asks  Radha to help by acting with him for the days play. Radha agrees on a condition that she should be let gone after the play is over. The play is successful that night. The agent tries to misbehave with Radha and the actor pulls him from it and warns him. But finds Radha has left.

Eight Night (Bravery): Radha disguises as a man and visits a house of a hunter (Akkineni Nageswara Rao). The hunter has been there for hunting a tiger and for another purpose. Radha introduces herself a secret agent in search of a criminal. The hunter seems to believe her and gives a her an earnest welcome, feast and hospitality. Radha finds that the hunter is actually a commissioner of police stayed in search of a criminal which actually faked by Radha. She tries to escape from the place but she is caught by the hunter.

The hunter introduces himself as the paternal uncle of the groom whom her father proposed for her and the groom is none other than her lover -  Venu Gopal (Akkineni Nageswara Rao). Radha has actually left her home before her father knew that she is in love with a person and the lover is the same man he has arranged for his daughter. Also Radha misunderstands that Venu Gopal actually is going to marry another girl but the girl is actually herself. Radha leaves for the Venu Gopals place the ninth day.

Ninth Day (Bliss): Venu Gopal (Akkineni Nageswara Rao), looks pale and dull after Radha left her home. He is neither interested to live usually nor to continue with his studies. His parents are worried and scolds him for wasting his life for a girl after all. Angered by this he shuts himself into a room. Radha reaches his home that time. Venus parents and Radha fears that he is attempting suicide. But suddenly the room opens and Radha runs inside.

Venu Gopal actually wanted to surprise Radha about their marriage and that is why he did not inform her about the engagement. Due to mis-communication Radha left home on the first Navaratri Night and comes back on ninth Navaratri night.

Venu Gopal and Radha happily marry. Except the dead gun man, her marriage is attended by the all the seven characters ( portrayed by Akkineni Nageswara Rao) she met during those eight days.

==Credits==

===Cast===
* Akkineni Nageshwara Rao   in Nine characters as
** The Widower (Wonder)
** The Drunkard (Fear)
** The Doctor (Compassion)
** The Gunman (Anger)
** The Villager (Equanimity)
** The Leper (Disgust)
** The Actor (Elegance)
** The Hunter/The Commissioner of police (Bravery)
** The Bridegroom (Bliss) Savitri	  as   Radha
* Chalam	as   Raju
* Kongara Jaggaiah
* Chittor V. Nagaiah
* Gummadi Venkateswara Rao	   as Father of Radha
* Relangi Venkataramaiah
* Ramana Reddy Raja Babu
* Chadalavada Kutumba Rao
* Hemalata
* M. Prabhakar Reddy as Police Insperctor
* Kutti Padmini
* Rushyendramani as Brothel-house Head
* Chhaya Devi	as Mental patient Girija	as Mental patient
* Jamuna Ramanarao	 as Mental patient
* J. Jayalalithaa as Mental patient Kanchana Mental patient Suryakantam as Mental patient

===Crew===
* Director: Tatineni Rama Rao
* Associate Director: A. Purnachandra Rao
* Story: A. P. Nagarajan
* Dialogues: Mullapudi Venkata Ramana
* Producer: Anumolu Venkata Subba Rao
* Production Company: Prasad Art Productions
* Lyrics: Dasaradhi, Kosaraju, Srirangam Srinivasa Rao
* Original Music: T. Chalapathi Rao
* Film Editing: J. Krishna Swamy
* Art Director: G. V. Subba Rao
* Cinematography: P. N. Selvaraj
* Choreography: Pasumarthi Krishna Murthy
* Playback singers: Jamuna (actress)|Jamuna, Ghantasala Venkateswara Rao, Savitri (actress)|Savitri, P. Susheela

==Character map of Navarathri and its remakes==
{| class="wikitable" style="width:50%; text-align:center;"
|- style="background:#ccc;" Navarathri (1964) Navarathri (1966) (Cinema of Andhra Pradesh|Telugu)|| Naya Din Nai Raat (1974) (Bollywood|Hindi)
|- Sanjeev Kumar
|- Savitri || Savitri ||Jaya Bhaduri
|-
| Director || A.P. Nagarajan || Tatineni Rama Rao ||A. Bhimsingh
|-
|}

==Soundtrack==
* Addala Meda Undi (Singers: P. Susheela, B. Vasantha, K. Jamuna Rani)
* Cheppana Katha Cheppana (Singer: P. Susheela; Cast: Savitri (actress)|Savitri) Ghantasala and P. Susheela)
* Navaratri Shubharatri (Singers: P. Susheela and group; Cast: Savitri) Akkineni and Savitri)
* Raju Vedale Sabhaku (Veedhi Natakam) (Singers: Ghantasala and others)

==External links==
*  

 
 
 
 
 
 