The Armchair
{{Infobox film
| name           = The Armchair
| image          = Le-fauteuil.jpg
| alt            = 
| caption        = 
| director       = Missa Hebié
| producer       = 
| writer         = Missa Hebié Noraogo Sawadogo
| narrator       = 
| starring       = 
| music          = 
| cinematography = Charles Baba Gomina
| editing        = Bertin B. Bado
| studio         = Faso Films
| distributor    = 
| released       =  
| runtime        = 92 minutes
| country        = Burkina Faso
| language       = French
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}
The Armchair ( ) is a 2009 Burkina Faso|Burkinabé film directed by Missa Hebié. It was written by Hebié and Noraogo Sawadogo. It won the Oumarou Ganda Prize at the 21st Panafrican Film and Television Festival of Ouagadougou. {{Cite web
  | title = 2009 Pan-African FESPACO Awards
  | work = Alt Film Guide
  | date = 10 March 2009
  | url = http://www.altfg.com/blog/film-festivals/fespaco-awards-2009/
  | accessdate =8 March 2010 }}
  {{Citation
  | last = Hegel
  | first = Goutier
  | author-link = 
  | title = Fespacos 40th anniversary&nbsp;– a mark of openness and excellence
  | newspaper = The Courier (ACP-EU)
  | date = March–April 2009
  | url = http://www.acp-eucourier.info/Fespaco-s-40th-anniversar.683.0.html
  | accessdate =8 March 2010
}}
  It was also screened at the 2009 Pusan International Film Festival in South Korea.

==Cast==
* Barthélémy Bouda
* Norah Kafando
* Justin Oindida
* Barou O. Ouédraogo

==References==
 

==External links==
*  
*    

 
 
 
 
 