Asuravamsam
{{Infobox film
| name           = Asuravamsham
| image          = Asuravamsam.jpg
| image size     = 
| caption        = 
| director       = Shaji Kailas
| producer       = R. Mohan Ranjith
| narrator       =  Siddique Narendra Saikumar
| music          = Rajamani
| editing        = L. Bhoominathan Manikandan
| distributor    = Shogun Release 
| released       = 1997
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}}
 Ranjith and directed by Shaji Kailas, with the cinematography by V. Manikandan|Manikandan. The film stars Manoj K. Jayan, Biju Menon, Siddique (actor)|Siddique, Narendra Prasad, Saikumar (Malayalam actor)|Saikumar, Priya Raman, and Chippy (actress)|Chippy. 

==Plot==

Palayam Murukan (Manoj K. Jayan) is the underworld goonda leader ruling Palayam market in Kozhikode City. He hails from an aristocratic family and enjoys a good academic background, but got into the crime world during his college days. He was expelled from his house by his sister (Bindu Panicker) after getting jailed for a murder. In alliance with Mayor Swamy (Narendra Prasad) and assisted by the muscle power of his trusted hench man Dosth Vishwan (Siddique (actor)|Sidhique), Murukan becomes the most powerful person of the city. His activities include mediating land deals, arranging hartals for political parties and murdering for a cause. On the personal side, he enjoys a deep rooted friendship with Dosth, and also he sponsors his adopted sister(Chippy (actress)|Chippy) on her medical studies and is loved by the people of Palayam market.

Things take a turn around when Murukan clashes with Mayor Swamy over a land deal. Murukan backs Moosa Settu (Rajan P. Dev) while Mayor Swamy supports the syndicate of Hussain Haji (Rizabawa) and the Thattel brothers Mani (C.I. Paul) and Bobby (Saikumar (Malayalam actor)|Saikumar). The plot thickens when Mayor Swamy arranges the murder of Moosa Settu. Nandita Menon(Priya Raman), a young industriallist also joins with them against Murukan, though she is not aware of their motive.

The syndicate manages to bring a new Police Commissioner for the city to tackle Murukan. Entering young, dynamic and quick tempered Jayamohan (Biju Menon), who is better known for his aggressive and arrogant ways of tackling crime. His direct fight with Murukan creates a lot of  problem in the Palayam market. He arrests each and every one belonging to Murukans gang, thus making Murukan to try for violent retaliations. But Jayamohan, who comes to know about the soft side of Murukan decides to turn him into a law abiding citizen. Mayor Swami and group, who were looking for the end of Murukan, gets upset by this move. In the mean time Nandita Menon also makes a truce with Murukan. Swami kills Dosth Vishwan, which tends Murukan again to take law into his hands. He turns violent, where by finishes the crime syndicate of the city by killing those who had masterminded the death of Vishwan. The film ends with Murukan holding a sword dipped in the blood of Bobby.

==Trivia==

*This was the second script by Ranjith for Shaji Kailas after Rudraksham.
*This was also the first Malayalam film of cinematographer V. Manikandan|Manikandan. 
*Kuthiravattam Pappu appears in a song.
*This film was shot in just 27 days at a budget of 78 lakhs and was completely shot in and around Kozhikode. 
 
==Cast==

*Manoj K. Jayan
*Biju Menon Siddique
*Saikumar Saikumar
*Narendra Prasad
*Priya Raman Chippy
*Rizabawa
* Lalithasree
*C.I. Paul
*K. B. Ganesh Kumar
*Mamukkoya Augustine
*Maniyanpilla Raju
*Rajan P. Dev
*Madhupal
* Sadiq
*Ajith
*Bindu Panicker
*Kunjandi
*Valsala Menon
*Kozhikode Narayanan Nair
*Kollam Thulasi

== External links ==
*  

 
 