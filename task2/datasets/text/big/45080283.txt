Manassiddare Marga
{{Infobox film 
| name           = Manassiddare Marga
| image          =  
| caption        = 
| director       = M. R. Vittal
| producer       = Srikanth Nahatha Srikanth Patel
| writer         = Remake
| screenplay     = Sadashiva Brahmam Rajkumar Rajashankar Narasimharaju K. S. Ashwath
| music          = M. Ranga Rao
| cinematography = Srikanth Rajaram
| editing        = S P N Krishna T P Velayudham
| studio         = Srikanth & Srikanth Enterprises
| distributor    = Srikanth & Srikanth Enterprises
| released       =  
| runtime        = 168 min
| country        = India Kannada
}}
 1967 Cinema Indian Kannada Kannada film, Narasimharaju and K. S. Ashwath in lead roles. The film had musical score by M. Ranga Rao.  

==Cast==
  Rajkumar
*Rajashankar Narasimharaju
*K. S. Ashwath
*M. P. Shankar
*Ranga
*Dinesh
*Rathnakar
*Shastry Jayanthi
*Udayachandrika
*Shailashree
*B. V. Radha
*Kamalamma
*Baby Padmashree
 

==Soundtrack==
The music was composed by M. Ranga Rao. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Madhura Mojina || L. R. Eswari || Vijaya Narasimha || 03.37
|-
| 2 || O Chandamama || L. R. Eswari || M. Marendra Babu || 03.26
|}

==References==
 

==External links==
*  
*  

 
 
 
 


 