The Journey to Melonia
{{Infobox Film
| name = The Journey to Melonia
| image = Melonia poster.gif
| caption =Theatrical release poster
| producer = 
| writer = Screenplay: Per Åhlin Karl Rasmusson Play: William Shakespeare Robin Carlsson Olle Sarri Tomas von Brömssen
| director = Per Åhlin
| distributor = Sandrew Metronome
| released = 15 December 1989
| runtime = 104 minutes
| country = Sweden Norway
| language = Swedish
| music = Björn Isfält
| editing = Per Åhlin SEK
| box office = 
}}
The Journey to Melonia ( ) is a 1989 Swedish-Norwegian animated fantasy adventure film directed by Per Åhlin, very loosely based on William Shakespeares The Tempest. It was Per Åhlins first fully animated feature film, since his earlier films Out of an Old Mans Head and Dunderklumpen! had both used a mix of animation and live action. The full English title is The Journey to Melonia: Fantasies of Shakespeares The Tempest.

It won two Guldbagge Awards for Best Creative Achievement (a category with three awards for technical achievements without their own categories). The first was to Åhlin for the animation and the second to Björn Isfält for the music. 

==Plot==
The beautiful paradise island Melonia is inhabited by the sorcerer Prospero with his daughter Miranda, the albatross Ariel, the good-natured vegetable-faced gardener Caliban and William the dog-nosed poet. They live a generally peaceful life, except for Caliban who has to work hard with the garden. A few miles away lies the dark island Plutonia, where the greedy capitalists Slug and Slagg rule. Once as green and flourishing as Melonia, Plutonia is now perceived as hell on earth, where children are forced to, under slave conditions, build weapons and tools of war, which Slug and Slagg believe is the way of the future. With Plutonias resources nearly exhausted, Slug and Slagg turn their gaze on the unexploited Melonia, scheming to take it over with a gigantic drill.

The movie begins with one of the child slaves, a boy named Ferdinand, escaping from Plutonia in a box, and ends up on Melonia, where Miranda and Prospero nurse him back to health. Prospero has just finished a magical growth elixir (humorously labelled "power soup" for the remainder of the movie), which Caliban is entrusted. Slug and Slagg kidnap Caliban and bring him to Plutonia. Ferdinand, Miranda, Prospero and some others journey to Plutonia in order to free Caliban, which eventually turns into a quest to free Ferdinands enslaved friends. Eventually, Miranda helps the children escape by transforming them into birds and transporting them into an old theater, where William the poet is making a less than successful attempt at staging William Shakespeare|Shakespeares The Tempest. After breaking out of his prison, the thirsty Caliban thoughtlessly drinks the elixir. Slug and Slagg, encouraged by Calibans growth, attempt to coax him into working for them, but their rants of superiority by arms falls upon deaf ears. They then attempt to destroy Caliban using the great drill, but he easily lifts it off the ground and plunges it into the floor, causing the island to sink to the bottom of the ocean in a gigantic maelstrom. Slug and Slagg are unable to escape the maelstrom and as they do not appear again, its safe to assume that they sink to the bottom and drown. The theater almost sinks as well, but is saved by Caliban. Prospero loses his magical powers, but accepts it readily, knowing that everybodys power will replace his magic. He frees Caliban and Ariel from his service, and the movie ends with a singing Ariel flying off into the sunset.

==Voice cast==
* Allan Edwall as Prospero Robin Carlsson as Miranda
* Olle Sarri as Ferdinand
* Tomas von Brömssen as Ariel
* Jan-Olof Strandberg as William
* Ingvar Kjellson as Kapten Julgransfot (Captain Tree-stand)
* Ernst Günther as Caliban
* Eva Rydberg as Kockan (Cookie)
* Jan Blomberg as Slug (Shrewd)
* Hans Alfredson as Slagg (Slag)
* Nils Eklund as Rorsman (Helmsman)

==Production==
With a budget of 22 million      When making the script, Per Åhlin and Karl Rasmussen would pick lines from The Tempest they thought they might have use of, and then put them together into a coherent synopsis. An influence for the visual style was Jules Vernes Propeller Island from 1895 in literature|1895. 

==Critical reception== A Midsummer The King and the Mockingbird. Some critique was given regarding the slow pace and ambiguous tone, making it hard to distinguish whether it was a childrens film or aimed for adults. Others complained about the somewhat naïve message.

The most negative review came from Variety (magazine)|Variety, where Keith Keller wrote: "The Voyage to Melonia by Per Åhlin, Swedens past master of animated films, probably has aimed over everybodys head with this go at The Tempest. Seven years in the making at a locally extraordinary cost $3.5-million, pic looks big but soon sags dangerously, and eventually ruptures." 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 