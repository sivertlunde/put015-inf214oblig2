Gamera: Guardian of the Universe
{{Infobox film name           = Gamera: Guardian of the Universe image          = File:Gamera Theatrical Poster.jpg caption        = Theatrical Poster director       = Shusuke Kaneko producer       = Hiroyuki Kato Shigeru Ohno Yasuyoshi Tokuma Tsutomu Tsuchikawa Seiji Urushido writer         = Kazunori Itō starring       = Shinobu Nakayama Ayako Fujitani Yukijiro Hotaru music          = Kow Otani cinematography = Junichi Tozawa editing        = Shizuo Arakawa
| studio        = Daiei Film distributor    = Toho (Japan) ADV Films (USA) released       =   country        = Japan runtime        = 95 minutes language  Japanese
|budget         = ¥5,000,000 box office     = ¥520,000,000
}}
 , is a 1995 science fiction kaiju film directed by Shusuke Kaneko and written by Kazunori Itō. It is a reboot of the Gamera film franchise, taking the character and franchise in a more serious and darker direction, away from the campy tone of the original films, and targeting a more mature audience. It was a co-production of Hakuhodo, Daiei Film and Nippon Television, and was the first Gamera film not to be released by Daiei Film.

The film follows Asagi Kusanagi and her father and colleagues, Yoshinari Yonemori and Mayumi Nagamine, as environmental pollution reawakens ancient giant monsters, the giant turtle Gamera and three carnivorous giant vampire bats Gyaos, to do battle for the salvation of the human race.

The film was released in Japan on March 11, 1995 and was a critical and financial success, earning ¥520,000,000 ($6,000,000 USD)  at the box office. It has been widely acclaimed by fans and critics and is regarded as a classic of the Kaiju genre.  The film spawned two sequels,   and  . It is the ninth entry in the Gamera film series and first in Shusuke Kanekos Gamera trilogy.

==Plot== Etrurian runes on the atoll. During the investigation, the atoll suddenly quakes, destroying the slab and throwing the scientists into the ocean. One member of the team, Marine Officer Yoshinari Yonemori (Tsuyoshi Ihara), sees the eye and tusk of a giant turtle. 

Meanwhile, ornithologist Mayumi Nagamine (Shinobu Nakayama) investigates a village in the Goto Archipelago reportedly attacked by a "giant bird." While Nagamine is initially skeptical of the claims, she is horrified upon discovering human remains in a giant bird pellet. Exploring the nearby forest, her team encounters and then successfully prevents three bird-like creatures from attacking another village. To prevent further attacks, Nagamine agrees to aid the government in capturing the giant birds. The creatures are lured the Fukuoka Dome baseball stadium, where two of the three are successfully captured. The last one escapes to the harbor, where she is killed by the giant turtle encountered by Yonemori and the scientists. The remaining birds escape before the turtle reaches the stadium.

After translating the runes, Kusangi explains to Yonemori and his daughter Asagi (Ayako Fujitani) that the giant turtle is called Gamera and the birds are Gyaos. When Asagi touches one of the stone amulets, she inadvertently forms a spiritual bond with Gamera. Kusanagi also tries to convince the government that Gyaos are the threat, but they remain focused on Gamera due to the destruction he caused. 

Now working together to investigate the creatures, Kusanagi, Yonemori, and Nagamine witness another Gyaos attack at the Kiso Mountain Range. Nagamine and Yonemori are nearly killed trying to rescue a child, but Gamera arrives in time to save them and kill another Gyaos. The last Gyaos, however, escapes. Meanwhile, Asagi discovers that she suffers the same wounds and fatigue as Gamera due to their shared bond. At Mount Fuji, she witnesses a military strike against Gamera. The attack attracts the final Gyaos to the scene, where she grievously wounds Gamera and forces the turtle to retreat into the ocean. Simultaneously, Asagi suffers a similar wound and passes out from the pain. Kusanagi visits his daughter at the hospital where Asagi falls into a coma after saying that she and Gamera must rest. 

After consulting with a biologist, Nagamine and Yonemori learn that the Gyaos are genetically engineered and reproduce asexually. They speculate on the origins and purpose of Gyaos and Gamera. Nagamine suggests that Gyaos were awakened by rampant pollution and Gamera was created to combat Gyaos. They approach Kusanagi with this information, explaining that the incident at Mount Fuji shows that Asagi is spiritually linked with Gamera. Kusanagi dismisses these claims until he witnesses the amulets power himself.

With Gamera recovering in the ocean, the last Gyaos grows unchecked, becoming a Super Gyaos. The creature attacks Tokyo, causing many civilian casualties and prompting the government to focus on Gyaos instead of Gamera. Attempts to kill Gyaos end in failure and she builds a nest in the ruins of Tokyo Tower.

Upon awakening from her sleep, Asagi warns the others that Gamera has recovered and will attack Gyaos. Gamera catches Gyaos by surpise, destroying her nest and eggs. A massive air battle ensues and Asagi, Kusanagi, Nagamine, and Yonemori follow closely in a helicopter. Initially, Gyaos overpowers Gamera, but Asagi uses her spiritual energy to revive Gamera, who kills Gyaos. Gamera then releases Asagi from their bond and returns to the sea.

==Cast==
* Tsuyoshi Ihara as Yoshinari Yonemori
* Akira Onodera as Naoya Kusanagi
* Shinobu Nakayama as Mayumi Nagamine, a gifted ornithologist who is also a friend of Asagis.
* Ayako Fujitani as Asagi Kusanagi, a young girl who forms a spiritual bond with Gamera after she received an ancient pendant found on Gameras hide by her father.
* Yukijirō Hotaru as Inspector Osako
* Hatsunori Hasegawa as Colonel Satake
* Hirotaro Honda as Mr. Saito, EPA
* Naoki Manabe and Jun Suzuki as Gamera, the films titular kaiju, Gamera is a giant fireball breathing turtle that was created by an advanced civilization to exterminate the invading Gyaos.
* Yuhmi Kaneyama as Gyaos, a species of malevolent man-eating bird creatures reawakened by environmental pollution.
* Akira Kubo as Captain of the Kairyumaru

==Awards==
* 1996 - Nominated Award of the Japanese Academy Best Supporting Actress - Shinobu Nakayama
* 1996 - Won Blue Ribbon Award Best Director - Shusuke Kaneko, Best Supporting Actress - Shinobu Nakayama
* 1996 - Won Festival Prize Best Director - Shusuke Kaneko, Best Screenplay - Kazunori Itô, Best Supporting Actress - Shinobu Nakayama, Best Technical - Shinji Higuchi (for his special effects)

==Reception==
Gamera: Guardian of the Universe received positive reviews from critics and fans.   

Peter H. Gilmore of MonsterZero.us said, "All in all, this is a vibrant and energetic film. The monster battles are full of physical grappling as well as energy weapon exchanges, and the excellent suitmation is well augmented by judiciously used computer-generated imagery|CGI."  Popcorn Pictures said, "This is just a great, fun kaiju film. ... Gamera finally has a film to rival Godzilla (but hes still second best to the Big G, though) and rid the infamous legacy that has dogged him throughout his motion picture life."  David Miller of CULT MOVIES praised the films special effects, calling the film, "one of the best of all the giant monster films".    Steve Biodrowski of CINEMAFANTASIQUE praised the films "money shot" moments, stating, "supplying the necessary oomph to push this over from being merely diverting to being outright exhilarating".  The New York Daily News praised the films action sequences, stating, "giant monster movie fans seeking a big-screen treat will find it here".  Roger Ebert gave the film three stars out of four, saying that, despite its flaws, "Gamera is more fun" than "megabudget solemnity like Air Force One", and that "Gamera is not a good movie but it is a good moviegoing experience".  {{cite web|url=https://www.youtube.com/watch?v=kHYxnujZwxw|title=
Siskel & Ebert review Gamera: Guardian of the Universe|work=Youtube|accessdate=December 3, 2014}} 

==Blu-ray and DVD releases==

Blu-ray:

Mill Creek Entertainment
* Released: October 12, 2010
* Note: Comes bundled with Gamera 2: Attack of the Legion on the same Blu-ray Disc. 

Gamera Trilogy: Guardian of the Universe / Gamera 2: Attack of the Legion / Gamera 3: Revenge of Iris 
* Released: Sep 27, 2011
* Aspect Ratio: 1.85:1 Codec: MPEG-4 AVC (1080P)
* Audio: Japanese: DTS-HD Master Audio, DTS-HD HR, Dolby Digital 5.1
* English: Dolby Digital 5.1
* Subtitles: English and Japanese
* Extras: Gamera: Guardian of the Universe: Behind the Scenes 
* Gamera Tests & Special Effects
* Gamera: Attack of the Legion: Action On Location 
* Creating the Legion 
* Gamera 3: Revenge of Iris: The Awakening of Iris remix 
* Deleted Scenes 
* G3 Trailer Reel 
* Note: All extras are on Gamera 3: Revenge of Iris 

DVD:

ADV Films
* Released: March 18, 2003
* Note: Extra features: Interview with special effects director Shinji Higuchi, Press Conference Footage, Behind the Scenes, Footage from Yabari International Fantastic Adventure Film Festival, Opening Night Footage, Trailers.

==References==
 

==External links==
* 
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 