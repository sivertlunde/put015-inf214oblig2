Aaranya Kandam
{{Infobox film
| name = Aaranyakaandam
| image =
| caption =
| director = J. Sasikumar
| producer = RS Prabhu
| writer = S. L. Puram Sadanandan
| screenplay = S. L. Puram Sadanandan
| starring = Prem Nazir Srividya Sukumari Adoor Bhasi
| music = A. T. Ummer
| cinematography = CJ Mohan
| editing = G Venkittaraman
| studio = Sree Rajesh Films
| distributor = Sree Rajesh Films
| released =  
| country = India Malayalam
}}
 1975 Cinema Indian Malayalam Malayalam film, directed by J. Sasikumar and produced by RS Prabhu. The film stars Prem Nazir, Srividya, Sukumari and Adoor Bhasi in lead roles. The film had musical score by A. T. Ummer.   
 
==Cast==
  
*Prem Nazir 
*Srividya 
*Sukumari 
*Adoor Bhasi 
*Thikkurissi Sukumaran Nair 
*Sankaradi 
*Sreelatha Namboothiri  Kunchan 
*MG Soman  Meena 
*Paravoor Bharathan 
*Philomina 
*S. P. Pillai 
*Thrissur Rajan 
*Usharani 
 

==Soundtrack==
The music was composed by A. T. Ummer. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Ee Vazhiyum Ee Marathanalum || K. J. Yesudas || P Bhaskaran || 
|- 
| 2 || Kalabhakkuriyitta || K. J. Yesudas || P Bhaskaran || 
|- 
| 3 || Kalayude Paalalacholayil || L. R. Eswari, Chorus || P Bhaskaran || 
|- 
| 4 || Manjalachaarthile || K. J. Yesudas || Bharanikkavu Sivakumar || 
|- 
| 5 || Naraayanaa hare naaraayanaa || Kamukara, Chorus || P Bhaskaran || 
|- 
| 6 || Njanoru Ponmani || K. J. Yesudas, P Madhuri || P Bhaskaran || 
|- 
| 7 || Yadunandana || P Madhuri || Bharanikkavu Sivakumar || 
|}

==References==
 

==External links==
*  

 
 
 


 