Black Silk
{{Infobox film
| name           = Black Silk
| image          = Black_Silk_poster.jpg
| caption        = Thai film poster
| director       = Rattana Pestonji
| producer       = Rattana Pestonji
| writer         = Rattana Pestonji
| narrator       =
| starring       = Ratanavadi Ratanabhand Senee Wisaneesarn Tom Wisawachart
| music          = Preecha Maetrai
| cinematography = Rattana Pestonji Adele Pestonji
| editing        = Rattana Pestonji
| distributor    = Hanuman Film
| released       = 1961
| runtime        = 129 minutes
| country        = Thailand
| language       = Thai
| budget         =
}} Thai crime drama film written and directed by Rattana Pestonji.

Considered the first Thai film noir, Black Silk was also among the first Thai films to be exhibited at overseas film festivals, screening at the 11th Berlin International Film Festival in 1961.   

==Plot==
Seni, a club owner, is under pressure by a rival, Wan, to pay an outstanding loan. Upon hearing that he has a long-lost brother named Sema, Seni sends his loyal lieutenants, Tom and Pon, to visit Sema, only to find that Sema has died. Seni decides that he can use the situation to his advantage, and buries Semas body in a forest.

Seni and Tom orchestrate the death of Wan, by setting fire to Wans car, pushing it off a cliff and framing Wans assistant Sin, also dead, for the murder. Seni then assumes the identity of his dead brother, free and clear of debts and Wans meddling.

Tom, meanwhile, has fallen in love with Phrae, a widowed mother who has worn black silk since her husband died two years before. Seni sees Phrae as a threat to his scheme and orders Tom to stop seeing her. Wan further plots to use Tom to psychologically manipulate Phrae.
 Buddhist temple. Tom tries to stand up to his boss, with disastrous consequences.

==Cast==
* Ratanavadi Ratanabhand as Phrae
* Senee Wisaneesarn as Senee/Sema
* Tom Wisawachart as Tom
* Seni Utsanisan
* Thawin Worawiboon
* Sarinthip Siriwan
* Jameunmanphonrit
* Phichit Saliphan
* Jurai Kasemsuwan

==Production==
Black Silk was the second color film made by Rattana, shot in the Cinemascope format at a time when most other Thai filmmakers were using 16-mm film.

With most of his resources put into the film stock and equipment, Rattana made sacrifices, performing most of the key jobs himself as writer, director, producer, cinematographer and editor. His daughter, Pannee Trangkasombat (using the stage name Ratanavadi Ratanabhand) was cast as Prae. 
 sound effects. Nonetheless, the performances are praised as generally good, especially Senee Wisaneesarn as Senee/Sema. There are even some musical numbers. 

==Release== Thai cinema, being among the first Thai films to be chosen for a major international film festival, in competition at the 11th Berlin International Film Festival in 1961. 

In the 2000s, the film was featured in retrospective programs at the Bangkok International Film Festival, the Singapore International Film Festival and the 2005 Pusan International Film Festival.

==References==
 

==External links==
* 
*   at the Pusan International Film Festival

 
 
 
 
 
 