The Chances of the World Changing
{{Infobox Film | name = The Chances of the World Changing
| image          =DVD cover of the movie The Chances of the World Changing.jpg
| caption        = 
| director       = Eric Daniel Metzgar
| producer       = Eric Daniel Metzgar
| writer         = 
| starring       = 
| music          = Eric Liebman
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| language       = English
| budget         =  
}}
 endangered turtle species from extinction. It was directed and produced by Eric Daniel Metzgar (Merigold Moving Pictures) and produced by Nell Carden Grey (Pigeon Post Pictures). Other credits include Eric Liebman (original music), Noe Venable (additional music and voice-over engineer), and Faun Fables (contributor of the song "Live Old").
 ecological destruction and poaching - the problem that  conservation biologists have dubbed "the Asian turtle crisis".  :
filmmaker Eric Daniel Metzgar, the creator of the film The Chances of the World Changing, talks to George Amato, the director of conservation genetics at the American Museum of Natural History  about turtle conservation and the relationship between evolution and extinction 
 Truer Than Fiction Award as part of the 2007 Independent Spirit Awards.

==Notes==
 

==External links==
*  at Independent Spirit Awards
*  at Full Frame Documentary Film Festival
*  at Public Broadcasting Service
* 

 
 
 
 
 
 

 