Sunshine Dad
{{infobox film
| title          = Sunshine Dad
| image          = Sunshine_Dad_(1916)_1.jpg
| imagesize      =
| caption        = Fay Tincher and Chester Withey Edward Dillon
| producer       = Fine Arts Film (aka D. W. Griffith)
| writer         = Tod Browning (story) Chester Withey (story & scenario) F. M. Pierson (scenario)
| cinematography = Alfred G. Gosden
| editing        =
| distributor    = Triangle Film Corporation
| released       = April 23, 1916
| runtime        = 5 reels
| country        = United States Silent (English intertitles)
}}
 Edward Dillon, written by Tod Browning and Chet Withey and starred stage comedian De Wolf Hopper. Hoppers year old son, William Hopper, appears in the film as a baby.  

The film is also known as A Knight of the Garter.

A print is preserved at the Library of Congress. 

==Cast==
*De Wolf Hopper - Alfred Evergreen
*Fay Tincher - Widow Marrimore
*Chester Withey - Count Kottschkoiff
*Max Davidson - Mystic Seer
*Raymond Wells - Mystic Doer
*Eugene Pallette - Fred Evergreen
*Jewel Carmen - Charlotte
*William Hopper - Baby
*Leo - A Lion

==References==
 

==External links==
* 
* 

 
 
 
 
 
 


 