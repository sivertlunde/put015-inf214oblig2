It's a Free World...
{{Infobox film
| name           = Its a Free World...
| image          = 
| image_size     = 
| caption        = 
| director       = Ken Loach
| producer       = Ken Loach  Rebecca OBrien
| writer         = Paul Laverty
| narrator       = 
| starring       = Kierston Wareing Juliet Ellis Frank Gilhooley
| music          = George Fenton
| cinematography = 
| editing        = 
| distributor    = 
| released       = 24 September 2007
| runtime        = 96 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = $6,645,036 
| preceded_by    = 
| followed_by    = 
| website        = 
}} 2007 Venice Film Festival. An ambitious working class British woman, played by Kierston Wareing, tries to improve her lot by setting up her own business.

==Plot== recruitment agency illegal immigrants.

Meanwhile, Angie becomes romantically involved with Karol (Lesław Żurek), an English-speaking Pole who is in the same predicament as those Angie recruits. She also helps Mahmoud, his wife and two young daughters, much to Roses distress. Mahmoud has been ordered deported, but he has gone into hiding to avoid a likely jail sentence back home in Iran.

Despite Roses misgivings, Angie becomes increasingly eager to do whatever it takes to build the business. When Angie anonymously informs the government about a camp of immigrants, that is the final straw for Rose. She quits.

Disaster strikes when one employer refuses to pay twenty of Angies workers the £40,000 they are owed. They blame her, and some of them take drastic action. They first kidnap her son Jamie (Joe Siffleet), then tie her up. After searching her flat, they take her profits (about a quarter of what they are due) and leave, but not before warning her that they want the rest or she will never see her son again. Soon after, Jamie shows up, unaware that the "policemen" he was talking to were fake. In the final scene, Angie abandons her scruples completely; she travels to the Ukraine to knowingly recruit illegal workers, offering to obtain forged papers for them.

==Critical response==
Though the film was not widely reviewed, it received generally positive reviews from critics. Review aggregation website Rotten Tomatoes gives the film a score of 83% based on 18 reviews, 15 of 18 were judged to be positive review, with an average rating of 6.6 out of 10. 

Peter Howell wrote in the Toronto Star, "Newcomer Wareing delivers an award-worthy performance as the steely Angie, who is impossible to hate even as she descends deeper into the moral abyss." 

==Awards== 2008 BAFTA Television Awards, losing out to Dame Eileen Atkins. 
The film received the "White Camel" (best film prize) at the fifth edition of the Sahara International Film Festival, the only film festival in the world celebrated in a refugee camp.

==See also==
* Ghosts (2006 film)|Ghosts, a 2006 film dealing with the much the same topic from the point of view of an illegal immigrant
* Gangmaster

==References==
 

== External links ==
*  
* London Citizens  

 

 
 
 
 
 
 
 
 
 
 
 
 
 