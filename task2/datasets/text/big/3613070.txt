The Man Who Would Be King (film)
{{Infobox film
  | name = The Man Who Would Be King
  | image = The Man Who Would Be King.jpg
  | caption = Theatrical release poster by Tom Jung
  | director = John Huston John Foreman
  | writer = Rudyard Kipling (story) John Huston Gladys Hill
  | starring = Sean Connery Michael Caine Christopher Plummer  Saeed Jaffrey
  | music = Maurice Jarre
  | cinematography = Oswald Morris
  | editing =
  | distributor = USA:&nbsp; 
  | released =  
  | runtime = 123 min
  | country = United States  United Kingdom
  | language = English
  | budget = $8 million 
  | gross = $11 million  (rentals)  
}} novella of Indian Army who set off from late 19th-century British India in search of adventure and end up as kings of Kafiristan. 

==Plot== Daniel "Danny" Dravot (Sean Connery) traveled to remote Kafiristan (in modern-day Afghanistan, the province is now known as Nuristan), became gods, and ultimately lost everything.

Three years earlier, Dravot and Carnehan had met Kipling under less than auspicious circumstances- Carnehan, a former Colour sergeant of the Queens Own Royal Loyal Light Infantry, pickpocketed Kiplingss pocketwatch but was forced to return it as he was a fellow Freemason. Despite being accomplished gun smugglers, swindlers, fencers of stolen goods, conmen, and blackmailers, both of them are bitter that after fighting to make India part of the Empire, they will have little to return home to except dead-end jobs. Feeling that India is too small for men such as themselves they intend to purchase twenty Martini Henry rifles, travel to Kafiristan, help a local king overcome his enemies, overthrow him, and become rulers themselves before stealing various riches and returning to England in triumph. After signing a contract pledging mutual loyalty and forswearing drink and women until they achieved their grandiose aims, Peachy and Danny set off on an epic overland journey north beyond the Khyber Pass, fighting off bandits, blizzards, and avalanches, into the unknown land of Kafiristan (literally "Land of the Kafir|Infidels").

They chance upon a Gurkha soldier who goes by the name Billy Fish (Saeed Jaffrey), the sole survivor of a mapping expedition sent several years earlier. Billy speaks English as well as the local tongue. Acting as translator and interpreter of customs and manners, he smooths the path of Peachy and Danny as they begin their rise, offering their services as military advisors, trainers, and war leaders to the chief of the much-raided village of Er-Heb. Peachy and Danny muster a force to attack the villagers most-hated enemy, the Bashkai. During the battle, Danny is struck by an arrow, but is unharmed, leading the natives to believe that he is a god. In fact, the arrow was stopped by a bandolier hidden beneath his clothing. As victory follows victory, the defeated are recruited to join the swelling army.
 Masonic jewel (given to him for luck by Kipling, a fellow Mason). By coincidence, the symbol on the jewel matches one known only to the highest holy man, the symbol of "Sikander" (Alexander the Great), who had conquered the country thousands of years before and promised to return. The holy men are convinced Danny is the son of Sikander. They hail him as king and lead the two men down to storerooms heaped with treasure that belonged to Sikander, which now belongs to Danny.
 delusions of Queen Victoria "as an equal." Disgusted, Peachy decides to take as much loot as he can carry on a small mule train and leave alone.
 Roxanne (Shakira Caine), despite Peachys strong warnings. Roxanne, having a superstitious fear that she will burst into flames if she consorts with a god, tries frantically to escape, biting Danny during the wedding ceremony. The bite draws blood, and when everyone sees it, they realize that Danny is human after all.
The angry natives pursue Danny and Peachy. When it becomes clear that the battle is lost, Peachy and Danny offer Billy a horse to escape, but Billy refuses and wishes them luck before courageously charging into the mob with a kukri singlehandedly. Nevertheless, Billy is killed amidst the mob and Peachy and Danny are soon captured. Danny apologizes to Peachy for spoiling their plans, and Peachy forgives him. Now resigned to his fate, Danny is forced to walk to the middle of a rope bridge over a deep gorge as the ropes are cut. Peachy is crucified between two pine trees, but he is cut down the next day when he survives the ordeal. Eventually, he makes his way back to India, but his mind has become unhinged by his sufferings. As Peachy finishes his story, he presents Kipling with Dannys head, still wearing its crown, thereby confirming the tale.

==Cast==
*Sean Connery as Daniel Dravot
*Michael Caine as Peachy Carnehan
*Christopher Plummer as Rudyard Kipling
*Saeed Jaffrey as Billy Fish
*Doghmi Larbi as Ootah
*Jack May as District Commissioner
*Karroom Ben Bouih as Kafu Selim
*Mohammad Shamsi as Babu
*Albert Moses as Ghulam
*Paul Antrim as Mulvaney
*Graham Acres as Officer
*The Blue Dancers of Goulamine as Dancers
*Shakira Caine as Roxanne

Huston had planned to make the film since the 1950s, originally with Clark Gable and Humphrey Bogart in the roles of Daniel and Peachy. He was unable to get the project off the ground before Bogart died in 1957; Gable followed in 1960. Burt Lancaster and Kirk Douglas were then attached to play the leads, followed by Richard Burton and Peter OToole. In the 1970s, Huston approached Robert Redford and Paul Newman for the roles. Newman advised Huston that British actors should play the roles, and it was he who recommended Connery and Caine. 

Karroom Ben Bouih was said to have been 103 years old when he played Kafu Selim, although there is no record of his birth.

The film was shot at Pinewood Studios  and at locations in France and Morocco. 

Though based on Kiplings work, the actual comedy and relationship between the two characters is high influenced by the Bing Crosby and Bob Hope Road to... movies of the 1940s and 50s.

==Reception==
John Simon of  ." Jay Cocks of Time commented "John Huston has been wanting to make this movie for more than 20 years. It was worth the wait." 

Roger Ebert gave the film 4 out of a possible 4 stars and wrote: "Its been a long time since theres been an escapist entertainment quite this unabashed and thrilling and fun." 

Some critics felt that the film was too long, and that Caine had overplayed his part. A review in Variety (magazine)|Variety was critical of the film mostly because of Caines performance, stating "Whether it was the intention of John Huston or not, the tale of action and adventure is a too-broad comedy, mostly due to the poor performance of Michael Caine." 

==Award nominations==
The film was nominated for four Academy Awards:   
 Best Art Peter James Best Writing&nbsp;– John Huston, Gladys Hill Best Costume Design&nbsp;– Edith Head Best Editing&nbsp;– Russell Lloyd (film editor) | Russell Lloyd

Maurice Jarre was nominated for the Golden Globe Award for Best Original Score.

Head was nomiated for the BAFTA Award for Best Costume Design, and Oswald Morris for the BAFTA Award for Best Cinematography.

==Music==
Maurice Jarre scored the film and invited classical Indian musicians to participate in the recording sessions with a traditional European symphony orchestra, blending the musical styles for the melodies, based around the Irish song "The Minstrel Boy" (although the lyrics are those of Reginald Hebers "The Son of God Goes Forth to War"), which figures in the plot. The fine playing of The Minstrel Boy is by William Lang, late of the Black Dyke Band and the London Symphony Orchestra.

==See also==

* White savior narrative in film

==References==
Notes
{{reflist|30em|refs=

 
 
 

}}

Bibliography
 
* 
 

==External links==
 
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 