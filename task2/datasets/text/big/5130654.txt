Big Helium Dog
{{Infobox film
|  name     = Big Helium Dog|
  image          = Big Helium Dog.jpg| Brian Lynch| Brian Lynch|
  starring       = Matt Kawczynski Michael Linstroth| View Askew|
  released   = 1999| 89 min
  music          = |
  language = English|
  budget         = |
}}
 View Askew Kevin Heffernan) and supporting (Jay Chandrasekhar, Steve Lemme, and Erik Stolhanske) roles.

The film has long been a favorite amongst fans who attended screenings at festivals such as the Smith sponsored "Vulgarthon" which led to an anticipated DVD release.

==Story description==
A sketch comedy which spoofs its own production: A guy trying to cope with the losing his friend is pulled against his will into a lucrative TV deal that later falls through, nonetheless he saves the world from destruction.

==Cast==
*Matt Kawczynski ....  Charlie Osgood
*Michael Linstroth ....  Ray Cross
*Kevin Smith ....  Director Kevin Heffernan ....  Phil
*Michael Ian Black ....  Martin Huber
*Lorene Scafaria ....  Chastity
*Pete Capella ....  High School Jock
*Jay Chandrasekhar ....  Movie Producer (scenes deleted)
*Damien Furey ....  Autograph Seeker
*Ralph Lambiase ....  BHD Interviewer
*Alyce LaTourelle ....  Law Bitch
*Dicky Barrett ....  Mr. Blocko
*Steve Lemme ....  Kendrick Brian Lynch ....  Directors Assistant
*Vincent Pereira ....  Cigarette Fairy
*Richard Perello ....  Bartender Brian Quinn ....  Vance
*Blanchard Ryan ....  Beautiful Dancer
*Erik Stolhanske ....  Father Joeb
*Bryan W. Strang ....  Carl Dunlop
*Kelli Strang ....  Little Susie
*Kris VanCleave ....  Immense Gay Trucker Bryan Johnson ....  Undercover Jesus
*Ming Chen .... Club Patron

==See also==
* List of American films of 1999 Brian Lynch

==References==
 
 

==External links==
*  
* 

 
 
 
 
 


 