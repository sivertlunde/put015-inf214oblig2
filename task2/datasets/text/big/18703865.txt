Swarnakamalam
{{Infobox film
| name           = Swarnakamalam
| image          = Swarnakamalam poster.jpg
| caption        = Theatrical Release poster
| director       = K. Viswanath
| producer       = Ch.V. Appa Rao
| writer         = K. Viswanath (story) Sainath Thotapalli (dialogue)
| screenplay     = K. Viswanath Venkatesh Bhanupriya Sharon Lowen
| music          = Ilayaraja
| cinematography = Lok Singh
| editing        = G.G. Krishna Rao
| studio         = Bhanu Art Creations
| distributor    =
| released       = 15th July 1988
| runtime        = 2:23:06
| country        = India
| language       = Telugu
| budget         =
| preceded by    =
| followed by    =
}} Telugu dance film, written and directed by K. Viswanath.  The film starred Daggubati Venkatesh|Venkatesh, Bhanupriya in the lead roles and music composed by Ilayaraja. 
 South Filmfare Awards, including Nandi Award for Best Feature Film and Filmfare Best Film Award (Telugu). 

==Plot==
Meenakshi (Bhanupriya) and Savitri (Devilalita) are daughters of a Kuchipudi doyen, Seshendra Shastry. While a highly accomplished artiste in his field, Seshendra Shastry is not well-off and has not been able to afford his daughters a conventional education. Both of them have achieved a respectable degree of proficiency — Savitri in Carnatic classical music and Meenakshi in classical dance.

Savitri is grateful for her knowledge and interest, and looks forward to a life that will require her to hone her skills in the same art. Meenakshi, on the other hand, is bitter about the lack of opportunity that she feels in the field of classical dance in India and resolves to make a simpler and more pleasurable life for herself as soon as possible, while confiding her ambitions only to her sister.

Chandrasekhar (Daggubati Venkatesh|Venkatesh) is a tenant who has just moved in next door. He is a painter and is shown to be handling movie promotions as a large chunk of his work. He develops an interest in the neighbours and tries to help them in whatever way he can, partly because of his (unconfessed) interest in Meenakshi and partly because of his interest in the art which seems to be slowly fading away from public life.

The rest of the film is largely built around Meenakshis journey from skepticism to devotion in her pursuit of dance. Chandrasekhar is shown to be an important catalyst in this transformation. Meenakshi becomes an accomplished dancer through the direction of renowned Odissi dancer Sharon Lowen, and gets an opportunity to go abroad and perform. She learns of Chandrasekhars love for her. She doesnt go abroad and finally unites with Chandrasekhar, confessing her love for him as well.

==Cast== Venkatesh as Chandu / Chandra Shekar
* Bhanupriya as Meenakshi
* Sharon Lowen as herself

==Soundtrack==
{{Infobox album
| Name        = Swarnakamalam
| Tagline     =
| Type        = film
| Artist      = Ilaiyaraaja
| Cover       =
| Released    = 1988
| Recorded    =
| Genre       = Soundtrack
| Length      = 44:39
| Label       = Echo Music
| Producer    = Ilaiyaraaja
| Reviews     =
| Last album  = Rudra Veena  (1988) 
| This album  = Swarnakamalam   (1988)
| Next album  = Varasudochhadu   (1988)
}}

The music for the film was composed by Ilaiyaraaja and released on ECHO Music Company.
{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 44:39
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = yes
| music_credits =

| title1  = Ghallu Ghallu
| lyrics1 = Sirivennela Sitaramasastri
| extra1  = S. P. Balasubrahmanyam|S. P. Balu, P. Susheela
| length1 = 5:02

| title2  = Aakasamulo
| lyrics2 = Sirivennela Sitaramasastri
| extra2  = S. Janaki
| length2 = 4:29

| title3  = Kothaga Rekka
| lyrics3 = Sirivennela Sitaramasastri SP Balu, S. Janaki
| length3 = 4:29

| title4  = Koluvai Vunnade
| lyrics4 = Sirivennela Sitaramasastri
| extra4  = S. P. Balu, P. Susheela
| length4 = 4:57

| title5  = Andela Ravamidhi
| lyrics5 = Sirivennela Sitaramasastri
| extra5  = S. P. Balu, Vani Jairam
| length5 = 6:59

| title6  = Shiva Poojaku
| lyrics6 = Sirivennela Sitaramasastri
| extra6  = S. P. Balu, P. Susheela
| length6 = 6:04

| title7  = Cheri Yasodaku
| lyrics7 = Annamayya Keerthana
| extra7  = S. P. Sailaja
| length7 = 4:35

| title8  = Aathmathvam
| lyrics8 = Siva Manasa Puja
| extra8  = S. Janaki
| length8 = 3:07

| title9  = Sakhihey
| lyrics9 = Bhuvaneswar Misra
| extra9  = Jayadev Ashtapadhi
| length9 = 1:19
}}
 

==Locations==
The high quality of the choreography in the song sequences demanded locations that were spread across the country. They include Puri in Orissa, the Himalayas, the stupa at Sanchi, Visakhapatnam, and others. 

==Awards==
;Nandi Awards- 1988 Best Feature Film - (Gold) - V. Appa Rao Best Actress - Bhanupriya Special Jury Award - Daggubati Venkatesh

;Filmfare Awards South- 1988 Best Film – Telugu - V. Appa Rao  Best Actress – Telugu - Bhanupriya

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 