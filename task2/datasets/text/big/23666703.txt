Stranger in Sacramento
{{Infobox film
| name           =Uno straniero a Sacramento
| image          =Stranger in Sacramento.jpg
| image_size     =
| caption        =
| director       =Sergio Bergonzelli
| producer       =
| writer         = Sergio Bergonzelli, Jim Murphy (novel)
| narrator       =
| starring       =
| music          =
| cinematography =
| editor       =
| distributor    =
| released       = 1965
| runtime        =
| country        = Italy Italian
| budget         =
}}
 1965 Italy|Italian Spaghetti Western film directed by Sergio Bergonzelli.

==Cast==
*Mickey Hargitay	... 	Mike Jordan
*Enrico Bomba	...	Chris (as Steve Saint-Claire)
*Mario Lanfranchi	...	Mill (as Johnny Jordan)
*Giulio Marchetti	...	Sheriff Joe (as James Hill)
*Florencia Silvero	... 	Lisa (as Flo Silver)
*Barbara Frey       ...     Rona Barret
*Big Matthews		(as Big I. Matthews)
*Gia Sandri
*Luciano Benetti    ...	Deputy sheriff (as Lucky Bennett)
*Gabriella Giorgelli	... 	Rosa (saloon dancer)
*Ariel Brown
*Renato Chiantoni
*Franco Gulà
*Romano Giomini
*Julián Marcos

==Plot==
During a cattle drive Mike Jordan finds his father and brothers murdered and the herd stolen. He is met with suspicion by the local sheriff and population. Eventually, Mike is able to expose the rancher Barret as the perpetrator. He is assisted by Lisa, Barrets former sister-in-law, and Chris, a wanted horse thief.

==Reception==
In his investigation of narrative structures in Spaghetti Western films, Fridlund discusses Stranger in Sacramento among films that mix some characters, motifs and plots well known from American traditional Westerns with others usual in  Spaghetti Westerns. For example, Mike Jordan is close to the typical American Western hero, while Chris is more like Joe in A Fistful of Dollars. 

==References==
 

==External links==
*  

 
 
 
 
 
 

 
 