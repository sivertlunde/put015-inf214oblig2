Cockpit (film)
{{Infobox film
| name           = Cockpit
| image          = 
| image_size     = 
| caption        = 
| director       = Mårten Klingberg
| producer       = Malte Forssell (executive producer) Rebecka Lafrenz (producer) Mimmi Spång (producer)
| writer         = Erik Ahrnbom
| narrator       = 
| starring       = See below
| music          = Mathias Nille Nilsson
| cinematography = Simon Pramsten
| editing        = Lars Gustafson
| studio         = 
| distributor    = 
| released       = 13 July 2012
| runtime        = 
| country        = Sweden
| language       = Swedish
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

Cockpit is a 2012 Swedish film directed by Mårten Klingberg.

== Plot summary ==
 

== Cast ==
*Jonas Karlsson as Valle
*Björn Gustafsson as Albin
*Björn Andersson as Harald
*Mårten Klingberg as Jens
*Chatarina Larsson as Susanna
*Sofia Ledarp as Annika
*Gustav Levin as Peter
*Karin Lithman as Caroline
*Tanja Lorentzon
*Ellen Mattsson as Maria
*Marie Robertson as Cecilia
*Måns Westfelt as Gunnar
*Carina Söderman as Rakel

== Soundtrack ==
 

== External links ==
* 
* 

 
 
 
 
 


 
 