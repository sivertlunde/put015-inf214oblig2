The Dead Girl
 
{{Infobox film name       = The Dead Girl image      = Dead girlposter.jpg caption    =Theatrical release poster director   = Karen Moncrieff producer   = Eric Karten Gary Lucchesi Tom Rosenberg Kevin Turen Henry Winterstern writer     = Karen Moncrieff starring   = Toni Collette Brittany Murphy Rose Byrne Marcia Gay Harden Mary Beth Hurt Kerry Washington music      = Adam Gorgoni
|cinematography= Michael Grady editing    = Toby Yates
|distributor= First Look International released   =   runtime    = 93 minutes country    = United States language   = English budget     = gross      = $905,291 
}} AFI Film limited US theatrical release on 29 December 2006. It was generally well received.  It only ran for two weeks in US first-run theaters, and earned nearly all its revenue from overseas release. 

==Plot==
The movie is presented in five segments, each bearing a title:
*The Stranger: Arden (Colette) lives with her child abuse|abusive, invalid mother (Laurie). One day she discovers the naked body of a dead woman on the property. Arden becomes a minor celebrity, drawing the attention of Rudy (Ribisi), who tells her the dead woman is the victim of a serial killer preying on young women in the area. Rudy asks Arden out. Arden gets into a fight with her mother, which compels her to finally leave home. Arden has her date with Rudy. In the morning, Arden calls the police to report a woman (her mother) left alone at home.
*The Sister: Leah (Byrne) is prepping the dead woman in the morgue when she notices a certain birthmark. Leah suspects that the dead woman is her sister, Jenny, who was taken from a nearby park fifteen years ago. She asks the sheriff to match the dental records. She is certain the dead woman is her missing sister, but Leahs parents (Steenburgen, Bruce Davison) refuse to believe that Jenny is dead. The sheriff delivers the test result: the dead woman is not Jenny.
*The Wife: Ruth (Hurt) is angry that her husband, Carl (Nick Searcy), constantly leaves her alone night after night. The next morning, she discovers clothing, wallets, and Identification card|IDs in ziplock bags inside a storage unit that is supposedly empty. She matches the IDs to names in the newspapers of the serial killers victims. Carl comes back late at night with scratches on his neck. After another argument, he leaves the house to go to the car outside. He pulls a trash bag out of the trunk and goes to the storage unit. Ruth asks Carl if he knows anything about the victims, and he says no. Later that night, Ruth enters the unit and pulls everything out. She drives to the police station, but doesnt follow through with her plan to present the evidence she found to the authorities; instead Ruth returns home to burn the evidence. sexually abused her. The following day, Melora takes Kristas daughter home with her.
*The Dead Girl: Krista (Murphy) buys her daughter a stuffed animal for her third birthday. One of her johns, Tarlow (Brolin), promises to drive her to Norwalk, where her daughter is, but he backs out at the last minute. Krista goes back to her room and finds her room-mate Rosetta &mdash; implied to be her lover &mdash; severely beaten. Krista believes the culprit to be a man named Tommy (Dennis Keiffer). Krista vandalizes Tommys car and, when he tries to stop her, she beats him up and leaves on a borrowed motorcycle. Krista calls Rosetta, and tearfully asks if Rosetta cares about her; Rosetta doesnt reply, and Krista hangs up, saddened and hurt. When the motorcycle breaks down on the highway, Krista hitches a ride from Carl. The film ends with Krista smiling, and happily talking about her daughter.

==Cast==
* Toni Collette – Arden
* Brittany Murphy – Krista Kutcher
* Rose Byrne – Leah
* Marcia Gay Harden – Melora
* Mary Beth Hurt – Ruth
* Kerry Washington – Rosetta
* Giovanni Ribisi – Rudy
* Josh Brolin – Tarlow
* Piper Laurie – Ardens mother
* Nick Searcy – Carl
* Mary Steenburgen – Beverley, Leahs mother
* Bruce Davison – Leahs father

==References==
 

==External links==
*  
*  
*  
*  
* 

 
 
 
 
 
 
 
 
 