Panchama Veda (film)
{{Infobox film|
| name = Panchama Veda
| image = 
| caption =
| director = P. H. Vishwanath
| writer = P. H. Vishwanath Ramakrishna  Geetha
| producer = Rajkumar   P. R. Prabhakar   Sarojamma
| music = Sangeetha Raja
| cinematography = R. Manjunath
| editing = Suresh Urs
| studio = Sri Sathyasai Combines
| released =  
| runtime = 139 minutes
| language = Kannada
| country = India
| budgeBold textt =
}}
 Kannada romantic Ramakrishna in the lead roles.  The film had musical score by Sangeetha Raja. 

Upon release, the film was critically acclaimed and won multiple awards at the Karnataka State Film Awards for the year 1989-90.
 
== Cast ==
* Ramesh Aravind as Ravi
* Sudharani as Rukmini Ramakrishna
* Geetha
* Kashi
* Krishne Gowda
* Nagendra Shah
* Sujatha
* Geethanjali
* Sundaramma


== Soundtrack ==
The soundtrack of the film was composed by Sangeetha Raja.

{{track listing 
| headline = Track listing
| extra_column = Singer(s)
| lyrics_credits = yes
| collapsed = no
| title1 = Aase Holeye Ukki Haridaithe
| extra1 = K. S. Chithra
| lyrics1 = Doddarangegowda
| length1 = 
| title2 = Nee Thanda Preethi
| extra2 = S. P. Balasubrahmanyam & K. S. Chithra
| lyrics2 = M. N. Vyasa Rao
| length2 = 
| title3 = Aase Holeye Batthi Hogaithe
| extra3 = K. J. Yesudas
| lyrics3 = Doddarangegowda
| length3 = 
}}

==Awards==
* Karnataka State Film Awards
 Best Actress - Sudharani Best Dialogue writer  - T. N. Seetharam Best Editing - Suresh Urs

== References ==
 

== External links ==
*  


 
 
 
 
 



 