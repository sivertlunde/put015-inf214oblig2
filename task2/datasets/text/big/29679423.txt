Will You Merry Me?
 
{{Infobox film
| name                 = Will you Merry Me?
| starring             = Wendie Malick Cynthia Stevenson
| released             =  
| runtime              =
| country              = United States
| language             = English
}}

Will you Merry Me?, a Christmas film, stars Wendie Malick and Cynthia Stevenson. It was filmed in 2007/2008, and was released in 2008.

== Plot ==

The Fines must survive Christmas with the Kringles. Rebbecca, Jewish, and her fiancé Henry, Christian, must get to know their in-laws, put up with fighting parents, and survey their familys traditions as they prepare to share a life together.

== Cast ==

* Wendie Malick as Suzie Fine
* Cynthia Stevenson as Marilyn Kringle
* Vikki Krinsky as Rebbeca Fine
* Tommy Lioutas as Henry Kringle
* Davis Eisner as Marvin Fine
* Patrick McKenna as Hank Kringle
* Reagan Pasternak as Kristy Easterbrook
* Martin Doyle as Tom Schultz

 
 


 