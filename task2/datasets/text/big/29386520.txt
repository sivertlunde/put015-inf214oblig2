The Terror (1928 film)
{{Infobox film
| name           = The Terror
| image          = "The_Terror"_(1928).jpg
| image size     = 
| alt            = 
| caption        = 
| director       = Roy Del Ruth Joseph Jackson
| producer       = Darryl F. Zanuck Jack L. Warner
| based on       =  
| starring       = May McAvoy Louise Fazenda Edward Everett Horton Alec B. Francis
| cinematography = Chick McGill  Thomas Pratt Jack Killifer 
| distributor    = Warner Bros. Pictures
| released       =  
| runtime        = 80 mins. (Sound version)   85 mins. (Silent version)  
| country        = United States
| language       = English  (Sound version) 
}} Lights of New York) This film was also the first all-talking horror film made, using the Vitaphone sound-on-disc system. 

==Plot==
"The Terror", a killer whose identity is unknown, occupies an English country house that has been converted into an inn.  Guests, including the spiritualist Mrs. Elvery and detective Ferdinand Fane, are frightened by strange noises and mysterious organ music.  Connors and Marks, two men just released from jail, have sworn revenge upon "The Terror".  Following a night of mayhem that includes murder, the identity of "The Terror" is revealed.   

==Cast==
*May McAvoy as Olga Redmayne
*Louise Fazenda as Mrs. Elvery, a spiritualist 
*Edward Everett Horton as Ferdinand Fane, a Scotland Yard detective 
*Alec B. Francis as Dr. Redmayne
*Matthew Betz as Joe Connors, a just-released criminal 
*Otto Hoffman as Soapy Marks, a just-released criminal  Holmes E. Herbert as Goodman Joseph Gerard as Supt. Hallick
*John Miljan as Alfred Katman Frank Austin as Cotton

The credits are spoken by a caped and masked Conrad Nagel

==Reception== The Lion and the Mouse,   all-talk picture of which May McAvoy, Alec Francis, two of the terrorized, are veterans."  Three months later, John MacCormac, reporting from London for The New York Times upon the films UK premiere, wrote: 
:"The universal opinion of London critics is that The Terror is so bad that it is almost suicidal. They claim that it is monotonous, slow, dragging, fatiguing and boring, and I am not sure that I do not in large measure agree with them. What is more important, Edgar Wallace, who wrote the film, seems to agree with them also. "Well," was his comment, "I have never thought the talkies would be a serious rival to the stage."   

==Remake==
The Terror was partially re-made by First National as Return of The Terror (1934).    Some films from the Warner Archive Collection has not been announced on DVD. 

==Preservation status== lost though the Vitaphone sound disc still exists and is preserved at the UCLA Film and Television Archive.    

Warner Bros. records of the films negative have a notation, "Junked 12/27/48" (i.e., December 27, 1948). Warner Bros. destroyed many of its negatives in the late 1940s and 1950s due to nitrate film pre-1933 decomposition. No prints of the film are known to currently exist, though rumors that private collectors who own foreign prints have continued to surface as late as 1999.

When in February 1956, Jack Warner sold the rights to all of his pre-December 1949 films; inclusing (The Terror) to Associated Artists Productions (which merged with United Artists Television in 1958, and later was subsequently acquired by Turner Broadcasting System in early 1986 as part of a failed takeover of MGM/UA by Ted Turner).

But in October 2002, in IMDb user  , who permitted me to screen it on a hand-cranked Movieola.

==See also==
*Films based on Edgar Wallace works
*List of incomplete or partially lost films

==Notes==
 

==References==
 
 

==External links==
*  
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 