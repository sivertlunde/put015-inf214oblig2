La Visa Loca
La Visa Loca is a 2005 Filipino comedy-drama film directed by Mark Meily and produced by Sharon Cuneta. Starring Robin Padilla, Rufa Mae Quinto and Johnny Delgado, the film is about a taxi driver who dreams of going to the USA.

==Plot==
Jess Huson (Robin Padilla) is a Filipino taxi driver who dreamed of obtaining a visa to be able to go to the USA. Jess meets his ex-girlfriend (Rufa Mae Quinto) and her young son Jason (Kurt Perez) who might actually be his. However, his visa application was denied. Jesss aging but randy diabetic father (Johnny Delgado) spends his days watching TV. One day, the famous TV host Nigel Adams came to the Philippines and Jess becomes his guide. He learns Nigels brother was the one who operates one of the biggest nursing aid agencies in the US East Coast.

== Film name ==
Name choice is a reference to one of Ricky Martins famous songs, "Livin la Vida Loca".

==Cast==
*Robin Padilla -  as Jess Huson
*Johnny Delgado -  as Papang
*Rufa Mae Quinto -  as Mara
*Paul Holme -  as Nigel Adams
*David Shannon - as David, Nigels Cameraman
*Kurt Perez- as Jason
*Rj Padilla- as Young Jess

== External links ==
*  

movie review: http://www.amazon.com/gp/aw/cr/rRSHV4DCL7T7H2/ref=aw_cr_i_1

 
 
 
 
 
 

 
 
 