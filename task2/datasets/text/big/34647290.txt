Love (2012 Taiwanese film)
{{Infobox film
| name = Love
| image = Love 2012 poster.jpg
| caption = Taiwan poster
| director = Doze Niu
| producer = Jimmy Huang
| writer = Doze Niu Tseng Li-ting Wang Qi-nan
| starring = Zhao Wei Shu Qi Mark Chao Ethan Juan Eddie Peng Doze Niu
| music = Chen Chien-Chi
| cinematography = Mark Lee Ping Bin
| editing = Tseng Li-ting
| distributor = Warner Bros. (Taiwan) Huayi Brothers (China) China Lion Films (U.S.)
| released =  
| runtime = 150 minutes
| country = Taiwan
| language = Mandarin
| budget = 180 million Taiwan Dollar
}} 2012 Taiwanese film, directed by Doze Niu. It stars mainlands celebrity actress Zhao Wei and Taiwans award winning actress Shu Qi,  Mark Chao, Ethan Juan, Eddie Peng, Amber Kuo, Ivy Chen. Love premiered at "Panorama" of the 62nd Berlin International Film Festival.  Similar to the British film Love Actually, Love is a romantic comedy featuring an ensemble cast of actors whose romantic stories are revealed to be interwoven as the plot progresses.

==Cast==
*Shu Qi as Rou Yi
*Ethan Juan as Xiao Kuan
*Ivy Chen as Yi Jia
*Mark Chao as Mark
*Amber Kuo as Xiao Min
*Eddie Peng as Ah Kai
*Zhao Wei as Xiao Ye
*Doze Niu as Chao Ping

== Summary ==
Love focuses on many different types of relationships: family, friends, lovers, co-workers, and even strangers are shown to express the different types of love people have every day. Through the three sub-plots of eight interwoven characters, the ups and downs of love, what love has to offer and what love has to take, the movie shows the relative situations in which love can take hold.

=== Sub-Plot 1: Yi Jia, Xiao Min, Ah Kai  ===
Yi Jia (Ivy Chen) is a simple, ordinary girl that goes along with everybody around her. She is in the Cycling Team of her university; she plays the supportive role called the domestique. Like her role in the cycling team, her real life is just the same. Yi Jias character portrays a person with no characteristic, no specialist and not very outstanding in general. When she is with her best friend - Xiao Min she is the supportive role like in the cycling team. Yi Jia is there to support and help Xiao Min. Xiao Min (Amber Kuo) is best friends with Yi Jia, yet the complete opposite of Yi Jia. Xiao Min is assertive, talented cold, confident, has a good family background and got everybodys attention. She has a boyfriend - Ah Kai. Ah Kai (Eddie Peng) is interested in filming and dreams to be a successful director in the future. But his dream is often joked around by people around him, with the exception of Xiao Min. 
These three best friends complement each other, forming a strong friendship but this all comes to a halt when Yi Jia finds herself pregnant.

=== Sub-Plot 2: Mark, Xiao Ye  ===
Mark (Mark Chao) is a young and successful entrepreneur. He is self-indulgent, cool, modern and casual. His motif about woman is, "Just need to conquer" and has an attitude of "If never have, then will never lost". Behind this train of thought a lacking childhood. On a trip to Beijing, Mark meets Xiao Ye. Xiao Ye (Zhao Wei) is a single mother with a seven-year-old son - Dou Dou and Marks realty estate agent. But their meeting did not go well as it ended in the police office, with Xiao Ye suffering a broken leg and losing her son along the way. Eventually, with the help of Mark, Dou Dou was found. Through this experience, his time with the mother and son, Mark found himself developing intimate feelings that he never had with people. 

=== Sub-Plot 3: Xiao Kuan, Rou Yi, Chao Ping  ===
Xiao Kuan (Ethan Juan) has a simple, pure, boy next door personality. Rou Yi (Shu Qi) is a materialistic woman, dependent on rich men, mistress of Chao Ping which leads to her life under the eyes of the public. Chao Ping (Doze Niu) is an aging man,and CEO of a well-known company.After an unhappy night of partying, Rou Yi and Chao Ping argues on the way home. Rou Yi gets out of the car and their fight is seen by Xiao Kuan. In a sense of heroism, Xiao Kuan grabs Rou Yi, running away from Chao Ping and brings her back to his house which is quiet and serene. From then on, they became acquaintances and Xiao Kuans place serves a place of escape for Rou Yi - a place away from the lights and attention. Through Xiao Kuans simple life, Rou Yi contemplates on her life and thinks it is time to change and take control.
 

==Reception==
After its preview screening, the film received positive reviews in Taiwan. In the preview investigation, the last scene of Shu Qi and Ethan Juan was selected as "the most romantic scene", and the solo performance of Zhao Wei as "the most amazing scene".   ifeng.com February 03, 2012
 

==Boxoffice==
In mainland China, the films gross was 130 million Chinese yuan. In Taiwan, the films gross was 160 million Taiwan Dollars. 

==References==
 

==External links==
* 
* 
* 
* 
* 
*  

 
 
 
 
 