Uuno Turhapuro – This Is My Life
{{Infobox film
| name          = Uuno Turhapuro &ndash; This Is My Life
| image         = This Is My Life poster.jpg
| caption       = Promotional poster
| director      = Ere Kokkonen
| writer        = Ere Kokkonen
| starring      = Vesa-Matti Loiri Marjatta Raita Tapio Hämäläinen Marita Nordberg
| producer      = Anna-Maija Kokkonen
| distributor   = Ere Kokkonen Oy
| released      = September 10, 2004
| runtime       = 100 min.
| language      = Finnish
}} 2004 Finland|Finnish comedy film, made as a tribute to Spede Pasanen. It is the twentieth and last installment in the Uuno Turhapuro film series.

==Plot==
Uuno disguises as an old man and infiltrates a nursing home for rich old people, where his father-in-law also lives. The ever-hungry Uuno is seduced by the table groaning with food, but as it happens, he never manages to be there at dinnertime.

Meanwhile Sörsselssön enters Uuno for a TV competition named This Is My Life, where contestants tell about their life as viewers vote them either to continue or out of the show. The nursing home elderly watch on TV as Uuno tells the shows host his life story.

==Production==
After Spede Pasanen, father of Uuno Turhapuro films, died in 2001, a group led by Ere Kokkonen decided to make a last Turhapuro movie for remembrance of him. The plot is based on Spedes and Eres common idea of Uuno in a nursing home for the elderly. Plenty of archive material is seen in the movie, so Härski Hartikainen, played by Spede Pasanen, is also in the movie in the form of archive material.
 cameo roles, such as Ilkka Lipsanen|Danny, Mika Häkkinen, Tanja Karpela and Ben Zyskowicz.

== External links ==
*  

 

 
 
 
 
 
 


 
 