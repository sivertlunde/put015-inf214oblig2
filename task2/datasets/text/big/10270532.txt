Shower (film)
{{Infobox film
| name           = Shower
| image          = Shower DVD cover.jpg
| image_size     =
| caption        = DVD cover Zhang Yang
| producer       = Peter Loehr Zhang Yang Liu Fendou Huo Xin Diao Yinan Cai Shangjun
| starring       = Zhu Xu Pu Cunxin Jiang Wu
| music          = Ye Xiaogang Zhang Jian Bieru
| editing        = Yang Hongyu
| studio         = Imar Film
| distributor    = Sony Pictures Classics (United States)
| released       =      
| runtime        = 92 minutes
| country        = China
| language       = Mandarin Chinese
| budget         = $350,000
}} Zhang Yang and starring Zhu Xu, Pu Cunxin and Jiang Wu. It premiered at the Toronto International Film Festival on 14 September 1999 and won the FIPRESCI Prize. Though only the second directorial work by Zhang and the third production of Imar Film, Shower was selected for numerous film festivals, including San Sebastian Film Festival, Sundance Film Festival, and Seattle International Film Festival, where it received many awards.
 bathhouse in Beijing. An aged father and his younger, mentally challenged son have been working hard every day to keep the bathhouse running for a motley group of regular customers. When his elder son, who left years ago to seek his fortune in the southern city of Shenzhen, abruptly returns one day, it once again puts under stress the long-broken father-son ties. Presented as a light-hearted comedy, Shower explores the value of family, friendship, and tradition.

==Plot==
 bathhouse for men in an old district in Beijing. The bathhouse provides a variety of peripheral services, including haircut, massage, shaving, fire cupping, even old-style pedicure, to a motley group of regular customers, many of whom are retired old Beijingers. The patrons usually spend their entire day, day after day, in the bathhouse, engaging in a game of Chinese chess or cricket fighting. As such, they have formed close bonds not only with one another, but also with Old Liu, who is manager, staff, mediator in squabbles, and marriage consultant all in one. One of the patrons is a man who sings O Sole Mio in the shower, but when he goes to sing it in public he loses his ability, until Erming provides him with a shower from a hose. Another patron is Mr. Zhang, whose marriage has been in trouble ever since his wife ran after a thief while naked, and Old Liu arranges a reconciliation by having the man find his wife in the bath. After the bathhouse closes in the evening, Old Liu and Erming go for their daily jog around the neighbourhood, after which they engage in a contest to hold their breath in water as they bathe. Even simple tasks such as cleaning up the bathhouse are tremendous fun for the duo, both of whom behave like little children at these times.

One day, Old Lius elder son Liu Daming (Pu Cunxin), who left many years ago to seek fortunes in the southern city of Shenzhen, abruptly returns home. Now a successful businessman, Daming had received a postcard from Erming with a drawing of their father lying in bed. Thinking that his father has died, Daming rushed home, only to find it a misunderstanding. However, the father-son ties between Old Liu and Daming appear to be strained, and Daming plans to return to Shenzhen three days later.

When Erming accompanies Daming downtown to reserve a plane ticket, Erming mindlessly wanders off from the area and is nowhere to be seen. After a futile search, Daming returns home to his distressed father, who lashes out at him for not being able to take care of his younger brother. Old Liu laments that having already lost one son (referring to Daming), he cannot afford to lose the other. The next morning, however, Erming is able to find his own way home, to the relief of everyone. Having already postponed his flight, Daming decides to stay one more night.

It rains heavily overnight. While up on the roof fixing leaks, Old Liu catches a cold and falls sick the next morning. Daming volunteers to take over the work in the bathhouse, delaying his return once more. In the next few days, Daming stays on to help in the bathhouse. He even joins in the daily evening jog with his father and Erming. One evening, after coming back from jogging, Old Liu passes away peacefully whilst bathing in the bathtub. 

Erming refuses to accept the reality of his fathers death and insists on opening the bathhouse every day at the usual time. In the meantime, Daming is planning to bring Erming back to Shenzhen to live with him, but he is worried about his wifes reaction regarding the mentally challenged Erming. He decides to leave Erming in a mental hospital for a couple of weeks whilst he goes back to Shenzhen to make preparations. This is not received well by Erming, however, as he struggles to cope at the mental hospital after fighting with the hospital attendants. In response to the situation, Daming decides to bring Erming back to the bathhouse again and temporarily resume operations.

Meanwhile, the entire district is due to be demolished to make way for commercial development. As removalists begin to move old furniture out of the bathhouse, Erming furiously attempts to stop the removalists from moving out the furniture in an attempt to keep the bathhouse. Eventually, he is convinced by Daming to accept the reality of the situation. The film concludes with a group of old customers gathering in the bathhouse for the last time, removing old paintings and portraits from the walls. Simultaneously, Erming begins nostalgically singing the tune of O Sole Mio as a final farewell to the bathhouse.

==Cast== bathhouse in Beijing, father to Daming and Erming
* Pu Cunxin as Liu Daming, elder son of Old Liu, left for Shenzhen many years ago
* Jiang Wu as Liu Erming, younger and mentally challenged son of Old Liu, helper in the bathhouse

==Production==
  Shaanxi province (near Yanan) and Tibet. According to producer Peter Loehr, he had "to go out and beg for a cast" because of the tight budget. He was even considering hiring a real mentally challenged amateur actor for the role of Liu Erming when Jiang Wu approached him and requested to have the role. The Beijing shoot took 43 days in total, with 18 to 20-hour days and one day off. The actors took naps between shots on the beds available in the bathhouse on set. Shower wrapped up in late May 1999.

==Reception==

Having wrapped up in late May 1999, producer Peter Loehr held back the release of Shower due to poor domestic market in Mainland China. Instead, the film premiered at the Toronto Film Festival on 14 September, winning the Fipresci Prize. It was soon invited to many more film festivals and distribution rights were sold to 56 countries. Imar Film had recouped double of its budget of US dollar|US$350,000 in advances alone. Having won international acclaim, Shower finally opened at home on 6 November.

===Awards and nominations===
* Toronto Film Festival, 1999
** International Critics Award (Fipresci Prize)
* San Sebastian Film Festival, 1999
** Best Director Ecumenical Award
* Thessaloniki Film Festival, 1999
** Golden Alexander Award
* Seattle International Film Festival, 2000
** Golden Space Needle Award
** Best Director
* Rotterdam International Film Festival, 2000
** Audience Award
* Satellite Awards, 2001
** Golden Satellite Award (nominated)
* Hong Kong Film Awards, 2002
** Best Asian Film (nominated)

==DVD release== Spanish and French was released on 12 December 2000 and distributed by Sony Pictures Classics in the United States.

==References==
*  

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 