Bad Guy (1937 film)
{{Infobox film
| name           = Bad Guy
| image          = Bad Guy (1937 film) poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Edward L. Cahn
| producer       = Tom Reed 
| screenplay     = Earl Felton Harry Ruskin 
| story          = J. Robert Bren Kathleen Shepard Hal Long 
| starring       = Bruce Cabot Virginia Grey Edward Norris Jean Chatburn Cliff Edwards Edward Ward
| cinematography = Lester White 
| editing        = Ben Lewis 
| studio         = Metro-Goldwyn-Mayer
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 69 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Bad Guy is a 1937 American crime film directed by Edward L. Cahn and written by Earl Felton and Harry Ruskin. The film stars Bruce Cabot, Virginia Grey, Edward Norris, Jean Chatburn and Cliff Edwards. The film was released on August 27, 1937, by Metro-Goldwyn-Mayer.  

==Plot==
 

== Cast ==
*Bruce Cabot as Lucky Walden
*Virginia Grey as Kitty
*Edward Norris as Steve Carroll
*Jean Chatburn as Betty
*Cliff Edwards as Hi-Line
*Charley Grapewin as Dan Gray
*Warren Hymer as Shorty John Hamilton as Warden
*Clay Clement as Bronson 

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 