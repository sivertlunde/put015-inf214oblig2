Bonobo (film)
{{Infobox film
| name           = Bonobo
| image          = Bonobo online poster.jpg
| alt            = Bonobo poster
| caption        = 
| director       = Matthew Hammett Knott
| producer       = Farhana Bhula 
| writer         = Matthew Hammett Knott, Joanna Benecke James Norton, Eleanor Wyld
| music          = 
| cinematography = 
| editing        = 
| studio         =       
| distributor    = 
| released       =  
| runtime        = 
| country        = United Kingdom
| language       = English
| budget         = 
| gross          =  
}}
 James Norton.

The film is about middle-aged widow Judith (Tessa Peake-Jones), and her attempts to convince her daughter Lily to leave an alternative commune and return to university. Bonobo premiered at the Raindance Film Festival  where it was nominated for Best British Feature. 

==Plot==
  Middle-aged widow Judith is worried about her 23-year-old daughter Lily, who has joined what she thinks is a sect. In fact, a group of youngsters overseen by middle-aged Anita have set up a commune based on the lifestyle of the Bonobo chimpanzee (Pan paniscus), in which all social conflicts are resolved by having sex. 

Deciding that she’ll ‘rescue’ Lily, Judith turns up at the commune, only to be told that she’ll have to wait until she’s in the right state of mind to talk to her daughter. Judith herself starts opening up, but at a party rejects Anita’s advances. Back home, Lily tells her mother that she has to be true to herself. Judith admits her formerly hidden desires and tells Lily she is going to let her decide her future for herself. 

Meanwhile, sex itself becomes a problem for the group, and it dissolves. When mother and daughter return to the commune, everyone hugs and kisses farewell.

==Cast==
* Tessa Peake-Jones as Judith
* Josie Lawrence as Anita James Norton as Ralph
* Eleanor Wyld as Lily
* Will Tudor as Toby
* Orlando Seale as Malcolm
* Carolyn Pickles as Celia
* Patricia Potter as Sandra
* Milton Lopes as Peter
* Harriet Kemsley as Helen

==Production==
Bonobo was privately financed and shot on location in Wimborne Minster, Dorset.

==Reception== The Telegraph called it "frisky, liberated, funny, alive".

==References==
 

==External links==
*  

 
 
 
 
 