La Boîte noire
 
 
{{Infobox film
| name           = La Boîte noire
| image          = Boite noire.jpg
| image_size     =
| caption        = Theatrical poster Richard Berry
| producer       = Michel Feller
| writer         = Richard Berry and  
| narrator       =
| starring       = José Garcia (actor)|José Garcia Marion Cotillard
| music          =  
| cinematography = Thomas Hardmeier
| editing        = Lisa Pfeiffer
| distributor    = EuropaCorp Distribution
| released       = 2 November 2005
| runtime        = 90 minutes
| country        = France
| language       = French
| budget         = $8,390,000
| gross          = $3,103,386 
}}
 Richard Berry, written by Berry and  , adapted from a novella by Tonino Benacquista, and starring José Garcia (actor)|José Garcia and Marion Cotillard.

==Plot== does not remember what happened before the crash, and he does not know the meaning of the words he pronounced while unconscious. The nurse who assisted him, Isabelle Kruger (Marion Cotillard), recorded them in a notebook, which she gives to him. Arthur then tries to understand what happened, what those sentences mean, and begins to lose his grasp of reality.

==Cast==
*José Garcia (actor)|José Garcia as  Arthur Seligman
*Marion Cotillard as Isabelle Kruger / Alice
*Michel Duchaussoy as Mr. Seligman, Arthurs father
*Bernard Le Coq as  Walcott / Doctor Granger
*Helena Noguerra as Soraya
*Gérald Laroche as  Commissioner Koskas / Marc Koskas
*  as  Mrs. Seligman, Arthurs mother
*  as  Dr. Brenner
*  as  La gardienne
*Pascal Bongard as Clovis
*Steve Campos as Arthur Seligman (6 years old)
*Hugo Brunswick as Yvan Seligman (teenager)
*Dominique Bettenfeld as Policeman 1
*Alexandre Donders as Customer in the café
*Arnaud Maillard as Little friend of Koskas

==References==
 

==External links==
* 

 
 
 
 
 


 