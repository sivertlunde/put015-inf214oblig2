Born to Love You (film)
{{Infobox film
| name             = Born to Love You
| image            = Born_To_Love_You_poster.jpg
| caption          = Theatrical release poster
| director         = Jerome Pabocan
| producer         =  
*Charo Santos-Concio
*Malou Santos
*Coco Martin
*Julie Anne Benitez
*Charyl Chan
*Mhalouh Crisologo
*Tess Fuentes
*Rondel Lindayag
*Carmi Raymundo
 
| writer           = Rondel Lindayag Coco Martin Deo Endrinal
| starring         =  
*Coco Martin
*Angeline Quinto
*Albert Martinez
*Tonton Gutierrez Eula Valdes
*Al Tantay
*Malou de Guzman
*Kiray Celis
 
| music            = Francis Concio
| screenplay       = Rondel Lindayag
| cinematography   = Hermann Claravall Gary Gardoce 
| editing          = Renewin Alano
| studio           =  
*Star Cinema
*CineMedia
 
| distributor      = Star Cinema
| released         =  
| runtime          = 
| country          = Philippines
| language         =  
| budget           = 
| gross            = PHP 52,369,650 
}} Filipino romantic film directed by Jerome Pabocan, starring Coco Martin and Angeline Quinto. The film was released under CineMedia and distributed by Star Cinema on May 30, 2012.

The film is Quintos acting debut.   

== Plot ==
The film centers around Joey Liwanag (Angeline Quinto), a poor lass who works as a part-time Korean tourist guide and likes to join amateur singing contests with her two younger siblings to help her family who struggles with financial problems. Despite the hardship and low probability, Joey worked hard to save up money to go to Korea and meet her father she has always longed to meet ever since she was born.

Rex Manrique (Coco Martin) is a frustrated, very arrogant and hot-tempered photographer who is out to prove the world that he can stand on his own feet and succeed in life independently. However, the ladder of success seems to be impossible for him as he faces different complications and rejections in life and as well as his career.

The moment Rex and Joey encounters each other in a Korean wedding, these two grown a big misunderstanding and hatred towards each other making it hard for the two of them to get along in the first place. Things gets more out of hand when Rex was hired in an advertisement company where Joey was also working for&mdash;this time, as a translator. As they got to know each other, they found solace and comfort in each others company. But their security they found in each other soon starts to shake when life takes another course and drives them in a complicated situation. Joey finally meets her biological father, while Rex struggles finding acceptance and forgiveness for her Mother who abandoned her for another man when he was still a young boy. Although Rex cuts off their relationship, Joey tries everything that she could to help him go through his own challenges and promises Rex that she will never leave him. But when Rex finally figures out the answers to his questions and his purpose in life, he and Joey gets into a car accident making Rex decide to leave Joey and his family behind and start a new life.

After a couple of years of investigation, Joey finally finds Rex in an island in Batangas and confronts him about their relationship which they broke off unofficially. At the same time, Joey finds out that Rex turned blind after saving her life in the accident and Rex himself found forgiveness in his heart. The movie ends during Rex and Joeys wedding celebration.

== Cast ==
*Coco Martin as Rex Manrique
*Angeline Quinto as Joey Liwanag
*Albert Martinez as Charles Ronquillo
*Roman Solito as Travis Dela Fuente
*Eula Valdez as Sylvia/Rexs mom 
*Al Tantay as Mario Liwanag/Joeys dad
*Tonton Gutierrez as Rexs dad
*Malou de Guzman as Ampie Liwanag/Joeys mom
*Kiray Celis as Sampaguita Liwanag
*Izzy Canillo as Young Rex Manrique
*Jojit Lorenzo
*Eda Nolan as Jam
*Mickey Ferriols as Bianca
*Amy Nobleza as Corrita Liwanag
*Louise Abuel as Brian
*Ryan Bang
*Manuel Aquino as Ku Aquino
*David Chua
*Aloy Noranda
*Chris Pasturan
*Jenny Kim
*Richard Quan
*Amy Nobleza
*Andre Tiangco

== Box office ==
"Born to Love You" earned P52.37 million.

== References ==
 

== External links ==
* 
* 


 
 
 
 
 
 
 