Manjina There
{{Infobox film
| name           = Manjina There
| image          =
| alt            =
| caption        =
| film name      = ಮಂಜಿನ ತೆರೆ
| director       = Bangalore Nagesh
| producer       = K. R. Narayana Murthy
| writer         = Chi. Udaya Shankar (dialogues)
| screenplay     = M. D. Sundar Bangalore Nagesh
| story          = Sahyadri
| based on       = Manjula Thoogudeepa Srinivas Dinesh
| narrator       =
| music          = Upendra Kumar
| cinematography = R. Madhusudan
| editing        = B. S. Prakash Rao
| studio         = G K Productions
| distributor    = G K Productions
| released       =  
| runtime        = 133 min
| country        = India Kannada
| budget         =
| gross          =
}}
 1980 Cinema Indian Kannada Kannada film, directed by Bangalore Nagesh and produced by K. R. Narayana Murthy. The film stars Srinath, Manjula (Kannada actress)|Manjula, Thoogudeepa Srinivas and Dinesh in lead roles. The film had musical score by Upendra Kumar.  

==Cast==
 
* Srinath Manjula
* Thoogudeepa Srinivas
* Dinesh
* Shakthi Prasad
* Chethan Ramrao Narasimharaju
* Sampath
* Vajramuni
* Shashikala
* Mamatha Shenoy
* Saroja
* Rajakumari
* Vathsala
* Raghunath
* Thipatur Siddaramaiah
 

==Soundtrack==
The music was composed by Upendra Kumar. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Janaki || Chi. Udaya Shankar || 03.26
|-
| 2 || O Nanna Cheluve || K. J. Yesudas || Chi. Udaya Shankar || 02.53
|-
| 3 || Balasiruva || K. J. Yesudas, Vani Jayaram || Chi. Udaya Shankar || 03.04
|- Janaki || Chi. Udaya Shankar || 03.07
|}

==References==
 

==External links==
*  
*  

 
 
 

 