Parwane
{{Infobox film
| name           = Parwane
| image          = Parwane film.jpg
| image_size     =
| caption        = Film poster
| director       = Ashok Gaikwad
| producer       = Vijay K. Ranglani
| writer         = Tanveer Khan
| narrator       =
| starring       = Avinash Wadhavan Siddharth Shilpa Shirodkar Paresh Rawal Gulshan Grover
| music          = Anand-Milind
| cinematography = 
| editing        = 
| distributor    = Shalimar Films
| released       = 12 November 1993
| runtime        = 
| country        = India Hindi
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Parwane is a Bollywood action film released in 1993 starring Avinash Wadhavan, Siddharth, Shilpa Shirodkar and Paresh Rawal.The story is written by Tanveer Khan and music is composed by Anand-Milind 

==Cast==

*Siddharth-Henry DSouza
*Shilpa Shirodkar-Mona S. Saxena
*Avinash Wadhavan-Avinash Malhotra
*Varsha Usgaonkar-Suzie
*Paresh Rawal-Naag Reddy
*Gulshan Grover-Rama N. Reddy
*Ajinkya Deo-Aslam
*Tinnu Anand-DSouza 

==Soundtrack==
The music of the film is composed by Anand-Milind and lyrics are penned by Sameer. 

{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Ye Ladki Badi Magroor Hai"
| Kumar Sanu
|-
| 2
| "Jee Chahta Hai Tujhe Kiss Karoon"
| Kumar Sanu, Kavita Krishnamurthy
|-
| 3
| "Jis Baatse Darte Thhe Woh Baat Ho Gai"
| Kumar Sanu, Kavita Krishnamurthy
|-
| 4
| "Ab aaye hai"
| Amit Kumar, Sadhana Sargam, Udit Narayan
|-
| 5
| "Dunk Maare Bichuda"
| Kavita Krishnamurthy,Purnima
|-
| 6
| "Koi nahi mita"
| Amit Kumar, Anand Bakshi
|}

==Release==
The film was well received by critics. 

==References==
 
==External links==
* 

 
 
 

 