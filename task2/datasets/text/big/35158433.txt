Radiance: The Experience of Light
{{Infobox film
| name           = Radiance:  The Experience of Light
| image          =  
| caption        = 
| director       = Dorothy Fadiman
| producer       =  Michael Wiese
| music          = Stephen Hill (broadcaster)
| editor         =  John Victor Fante
| photography    =  Steven P. Mangold  Barry Brukoff
| released       =  
| runtime        = 22 minutes
| country        = United States
| language       = English
}}
Radiance:  The Experience of Light is a 1978 short debut film created and narrated by filmmaker, Dorothy Fadiman.  The film documents the presence of light as a universal symbol for “Spirit”, to people from many cultures. The film weaves together images, music and a poetic narration, revealing how light continues to ignite inspiration in religion, philosophy, art, and architecture throughout human history.

==Reception==

Colin Higgins who directed and wrote the screenplay for the film,  Harold and Maude, called it "Beautifully crafted, personal and profound. It was for me a totally spiritual experience."

The film was featured as part of the evening with the theme, "Art of Film," at the  16th Annual Independents Film Festival in Tampa, Florida in 2009. 

Radiance: The Experience of Light received the following awards:
*Columbus International Film & Video Festival:  Chris Award
*Religious Educators Media Award
*Whole Life Expo Media Festival/San Francisco
*Virgin Islands/Miami International Film Festival

==References==
 

== External links ==
*  
*   at vimeo.com
*    at archive.org
*   at cultureunplugged.com
*   on Turn Toward Life TV
*   
*   from SF Gate
*  
*  
*  

 
 
 
 
 
 
 
 
 


 