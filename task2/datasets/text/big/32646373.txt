Nasi Lemak 2.0
{{Infobox film
| name           = Nasi Lemak  2.0
| image          = Nasi Lemak 2.0 DVD Cover.png
| caption        = Film poster
| film name      = {{Film name| simplified   = 辣死你妈
| pinyin         = làsǐnǐmā}} Namewee
| producer       = Fred Chong Sylvia Lim
| writer         =  Namewee Karen Kong Adibah Noor Afdlin Shauki David Arumugam Reshmonu Kenny & Chee
| music          = 
| cinematography = 
| editing        = 
| studio         = Prodigee Media Sdn Bhd
| distributor    = 
| released       =  
| runtime        = 108 minutes
| country        = Malaysia
| language       = Mandarin Malay English Tamil Hokkien Teochew Cantonese Hakka Japanese
| budget         = lower than MYR1 mil. MYR7 Million
}}
 National Censorship Board. The film premiered on 8 September 2011 in 60 cinemas throughout Malaysia. In an interview with Agence France-Presse|AFP, co-producer Fred Chong said that the movie made more than MYR1.5 million during the first four days of release. 

==History==
On 17 March 2010, Wee Meng Chee, nicknamed Namwee, announced at a small media event that he was going to make a movie.  He applied for funds from the Malaysian government, which he recorded on film. After multiple unsuccessful attempts, he vowed to meet the Prime Minister of Malaysia.  Instead, he filmed the movie himself with a low budget and requested the government censorship board approval.  On 26 June 2011, he announced that the movie would be in theatres on 8 September.  Soon after, the trailer was released on his YouTube channel.  The movie premiered in Melbourne, Australia, on 31 August, which is also Hari Merdeka|Malaysias National Day. 

The films executive producer, Fred Chong, said that the movie had the full support from Minister Datuk Seri Nazri Aziz, a member of the Prime Ministers office, who had issued an official letter endorsing it as a "1Malaysia film".  However, protesters objected to the film and its maker, which prompted Wee to post on his Facebook page about his concerns that the movie would be banned in Malaysia.  

==Plot==
A young chef, Huang Da Xia (Wee Meng Chee) struggles to get his restaurant business going. His restaurant is unpopular because he cannot adapt to the "localized" cooking his patrons are looking for. However, contradicting his unpopular cuisine, he is also well known as "Hero Huang" in the local neighborhood, because he carries out good deeds in helping the community, including filming videos and putting them on his YouTube account. One day, he meets Xiao K (Karen Kong), who asks for his help. Xiao Ks father, owner of a famous Chinese restaurant, is fighting with her aunt for the ownership of the restaurant. After some complicated discussions, they decided to hold a contest to see who can cook the best Chinese dish. Desperate to get his life and the restaurant business back on track, Chef Huang decided to help Xiao K. Huang seeks help from a mysterious hawker stall lady (Adibah Noor), who summons him to embark on an extraordinary journey of his life. During this self-enlightening experience, he will also meet many "local heroes", each lending their support to help him rediscover his roots and the real hidden message of "Nasi Lemak".

==Social impact==
 
2.0 is the first film in Malaysia to portray the countrys three major races in an ensemble manner. The response was mixed. This is the first Malaysian film that did not target one of the three population groups as its audience. A survey conducted by major bloggers and entertainment tabloids claimed that most of its audience loved the style of acting, because they could relate to the characters.

The only protest against the movie took place in Ipoh by a group called Pertubuhan Gagasan Rakyat Didahulukan Negeri Perak.

2.0 is not without controversy. an article by Fauziah Arof dated 21 September 2011 condemned Namewee for insulting the national anthem Negaraku, Islam and Malay race as a whole. Namewee responded that the author had passed judgement on his movie without looking at it as a whole, for misunderstanding it and for missing its intent to unify the races across Malaysia. Namewee’s response led protestors to push for Utusan Malaysia to lodge an official police report against him.

The National Film Development Corporation Malaysia (Finas) declared that the movie is not entitled to a 20% tax rebate under its new incentive for local films, because it did not qualify for mandatory screening status. 

==Cast==
{{columns-list|3| Namewee as Chef Huang
* Karen Kong as Xiao K
* Adibah Noor as Nor
* David Arumugam as Curry Master
* Afdlin Shauki as Fisherman
* Kenny & Chee as Nyonya & Baba
* Dennis Lau as Lan Qiao
* Reshmonu as Hang Tu Ya
* Nadine Ann Thomas as Curry Daughter
* Nur Fathia as Fisherman 2nd Wife
}}

==See also==
* Nasi lemak

==References==
 

== External links ==
*  
*  

 

 
 
 
 