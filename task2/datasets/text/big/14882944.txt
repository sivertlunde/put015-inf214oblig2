Brothers (2009 film)
{{Infobox film
| name           = Brothers
| image          = Brothersposter.jpg
| caption        = Promotional film poster
| director       = Jim Sheridan
| producer       = Michael De Luca Sigurjón Sighvatsson Ryan Kavanaugh
| screenplay     = David Benioff
| based on       =  
| starring       = Tobey Maguire Jake Gyllenhaal Natalie Portman Sam Shepard Mare Winningham Bailee Madison
| music          = Thomas Newman
| cinematography = Frederick Elmes
| editing        = Jay Cassidy Michael De Sighvatsson Films Lionsgate Relativity Media
| released       =  
| runtime        = 105 minutes  
| country        = United States
| language       = English
| budget         = $26 million   
| gross          = $43,318,349 
}} psychological war Afghanistan and Denmark. Golden Globe Best Actor in a Motion Picture Drama for his performance.

==Plot== Marine captain prisoner in a mountain village. 

With Sam gone, Tommy attempts to redeem himself in the eyes of his family by wrangling old friends to help with kitchen repairs for Grace and the kids. Grace slowly sheds her previous resentment towards her brother-in-law. As months pass, Grace and Tommy bond over their mutual mourning, culminating in a passionate fireside kiss. They regret it afterward, and do not take this attraction any further, though Tommy continues to win the favor of his nieces. Meanwhile, Sam and Joe are tortured by their captors, forced to make videotaped dismissals of the military and their mission, though only Joe cracks. The captors eventually deem him useless and force Sam, at gunpoint, to beat Joe to death with a lead pipe. 

Sometime later, Sam is rescued. Once Sam returns home, he drifts through encounters in a cold, paranoid daze, showing signs of severe   on Tommy who arrives and tries to calm his brothers violent breakdown. 

The police arrive, and after a frantic confrontation in which Sam holds the gun up against his head and nearly commits suicide, he reluctantly surrenders after a plea from Tommy and Grace. Sam is admitted to a mental hospital. Grace visits him and tells him that if he does not tell her what is tormenting him, he will lose her forever. Faced with this decision, Sam finally opens up about the source of his pain, confiding in her that he killed Joe and they embrace. A letter between husband and wife is read aloud, with Sam wondering if he will be able to continue living a normal life.

==Cast==
* Tobey Maguire as Capt. Sam Cahill
* Jake Gyllenhaal as Tommy Cahill
* Natalie Portman as Grace Cahill
* Sam Shepard as Hank Cahill
* Mare Winningham as Elsie Cahill
* Bailee Madison as Isabelle Cahill
* Taylor Geare as Maggie Cahill
* Patrick Flueger as Pvt. Joe Willis
* Carey Mulligan as Cassie Willis
* Clifton Collins, Jr. as Maj. Cavazos Jenny Wade as Tina
* Omid Abtahi as Yusuf
* Navid Negahban as Murad
* Enayat Delawary as Ahmed
* Ethan Suplee as Sweeney
* Arron Shiver as A.J.
* Ray Prewitt as Owen

==Reception==

===Box office=== New Moon The Blind Side.  Since its box office debut the film has grossed $43,318,349 worldwide. 

===Critical response=== rating average of 6.2 out of 10. The sites consensus is that "It plays more like a traditional melodrama than the Susanne Bier film that inspired it, but Jim Sheridans Brothers benefits from rock-solid performances by its three leads." 
{{cite web
|url=http://www.rottentomatoes.com/m/1213999-brothers/
|title=Brothers (2009)
|publisher=IGN Entertainment, Inc
|work=Rotten Tomatoes
|accessdate=December 25, 2009}}
  On Metacritic the film has a rating of 58 out of 100 based on 30 reviews. 
{{cite web
|url=http://www.metacritic.com/film/titles/brothers2009
|title=Brothers (2009): Reviews
|work=Metacritic
|publisher=CNET Networks, Inc
|accessdate=December 25, 2009}}  Maguire in particular received critical acclaim for his dramatic performance. Roger Ebert said that Brothers is "Tobey Maguires film to dominate, and Ive never seen these dark depths in him before."  Richard Roeper of the Chicago Sun Times named Brothers as the Best Film of 2009.

===Accolades===
Of the Golden Globe nomination, Tobey Maguire said "I had no expectation about getting a nomination, but I was watching nonetheless. My wife and my son got really excited. I was sort of surprised — I was like, Oh, wow. And I couldnt hear the latter part of my name." The Edge of U2 described how the band planned to celebrate the nomination. "I think we might have a pint of Guinness and eat a potato in honor of (director) Jim (Sheridan) and his great piece of work." 

{| class="wikitable sortable"
|- Year
!align="left"|Ceremony Category
!align="left"|Recipients Result
|- 2009
|15th Critics Choice Awards Best Young Actor / Actress Bailee Madison
| 
|- 36th Saturn Awards Best Action or Adventure Film
|Brothers
| 
|- Best Actor Tobey Maguire
| 
|- Best Actress Natalie Portman
| 
|- Best Performance by a Younger Actor Bailee Madison
| 
|- 67th Golden Globe Awards Best Actor - Drama Tobey Maguire
| 
|- Best Original Song
|"Winter" by U2
| 
|- 2010 Teen Choice Awards Choice Movie Actor - Drama Tobey Maguire
| 
|- Jake Gyllenhaal
| 
|- Chicago Film Critics Association Awards 2009 Best Supporting Actress Natalie Portman
| 
|- Denver Film Denver Film Critics Society Awards 2009 Best Original Song
|"Winter" By U2
| 
|}

==Home media==
Brothers was released on DVD and Blu-ray on March 23, 2010.

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 