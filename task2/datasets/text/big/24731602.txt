Extraordinary Measures
 
{{Infobox film
| name           = Extraordinary Measures
| image          = Extraordinary measures poster.jpg
| caption        = Theatrical poster Tom Vaughan
| producer       =  
| writer         = Robert Nelson Jacobs
| based on       =  
| starring       =   Andrea Guerra
| editing        = Anne V. Coates Andrew Dunn
| studio         = Double Feature Films
| distributor    = CBS Films
| released       =  
| runtime        = 106 minutes 
| country        = United States
| language       = English
| budget         = $31 million   
| gross          = $15,134,293 
}} John and Aileen Crowley, whose children have Pompes disease. The film was shot in St. Paul, Oregon; Portland, Oregon; the Corner Saloon in Tualatin, Oregon; Manzanita, Oregon; Beaverton, Oregon, and Vancouver, Washington. It is the first film to go into production for CBS Films, the film division of CBS Corporation.

==Plot==
  John Crowley, Pompe disease or acid maltase deficiency. In the film his children are aged 8 and 6.

Along with his wife Aileen (Keri Russell), he raises money for research scientist Robert Stonehill (Harrison Ford), forming a company to develop a drug to save his childrens lives.

== Cast == John Crowley
* Harrison Ford as Dr. Robert Stonehill
* Keri Russell as Aileen Crowley
* Courtney B. Vance as Marcus Temple
* Meredith Droeger as Megan Crowley
* Diego Velazquez as Patrick Crowley
* Sam M. Hall as John Crowley, Jr.
* Patrick Bauchau as Eric Loring
* Jared Harris as Dr. Kent Webber
* Alan Ruck as Pete Sutphen
* David Clennon as Dr. Renzler
* Dee Wallace as Sal
* Ayanna Berkshire as Wendy Temple
* P. J. Byrne as Dr. Preston
* Andrea White as Dr. Allegria
* G. J. Echternkamp as Niles
* Vu Pham as Vinh Tran Derek Webster as Cal Dunning
 John Crowley makes a cameo appearance as a venture capitalist.

==Production==
  
Adapted by Robert Nelson Jacobs from a nonfiction book "The Cure: How a Father Raised $100 Million&mdash;and Bucked the Medical Establishment&mdash;in a Quest to Save His Children" by the Pulitzer Prize journalist Geeta Anand, the film is also an examination of how medical research is conducted and financed.

 
  in Portland, Oregon]] OHSU Doernbecher Nike campus in Beaverton, Oregon.  This was the first time Nike allowed filming on their campus and they donated the location payment to Doernbecher Children’s Hospital.  During filming, the working title was The Untitled Crowley Project. 

In the film, the children are 9 and 7 years old. Their non-fiction counterparts were diagnosed at 15 months and 7 days old and received treatment at 5 and 4, respectively. Roger Ebert (January 20, 2010).  . Sun Times. Accessed 2011-08-01. 

==Inspiration== Pompe disease, was simultaneously approved for sale by the US Food and Drug Administration and the European Medicines Agency. Henceforth, more than 1000 infants born worldwide every year with Pompe disease will no longer face the prospect of death before reaching their first birthday for lack of a treatment for the condition.
 The Cure (ISBN 9780060734398).   Parts of the book first appeared as a series of articles in the Wall Street Journal.

The small start-up company Priozyme was based on Oklahoma City-based Novazyme. The larger company, called Zymagen in the film, was based on Genzyme in Cambridge, MA. {{cite journal|url=http://www.the-scientist.com/blog/print/57091/|journal=The Scientist NewsBlog|title=
A review of Extraordinary Measures|author=Jef Akst|date=January 22, 2010}}  Novazyme was developing a protein therapeutic, with several biological patents pending, to treat Pompe Disease, when it was bought by Genzyme. The patent portfolio was cited in the press releases announcing the deal. 

According to Genzyme, Dr. Robert Stonehills character is based upon scientist and researcher William Canfield,  who founded Novazyme.  Roger Ebert, in his review, says the character is based on Yuan-Tsong Chen,  a scientist and researcher from Duke University  who collaborated with Genzyme in producing Myozyme, the drug which received FDA approval.

==Reception==

===Critical response===
The film opened to mixed reviews. Review aggregation website Rotten Tomatoes gives the film a score of 27% based on reviews from 136 critics, with an average rating of 4.8 out of 10. The sites general consensus is that "Despite a timely topic and a pair of heavyweight leads, Extraordinary Measures never feels like much more than a made-for-TV tearjerker." 
{{cite web
| url = http://www.rottentomatoes.com/m/extraordinary_measures/
| title=Extraordinary Measures (2010)
| work = Rotten Tomatoes
| publisher = Flixster
| accessdate = July 8, 2010
}}
   weighted average score out of 0–100 reviews from film critics, has a rating score of 45 based on 33 reviews. {{cite web
| url = http://www.metacritic.com/film/titles/extraordinarymeasures
| title= Extraordinary Measures: Reviews
| publisher= CNET Networks
| work = Metacritic
| accessdate = January 25, 2010
}}
 

Richard Corliss of Time magazine wrote: "Fraser keeps the story anchored in reality. Meredith Droeger does too: as the Crowleys afflicted daughter, shes a smart little bundle of fighting spirit. So is the movie, which keeps its head while digging into your heart. You have this critics permission to cry in public."  The New York Times  A. O. Scott said in his review: "The startling thing about Extraordinary Measures is not that it moves you. Its that you feel, at the end, that you have learned something about the way the world works." 
 Pompe disease.  Peter Rainer from The Christian Science Monitor mentions that Big Pharma got a surprisingly free pass in the film and that it will come as a surprise to all those sufferers struggling to get orphan drugs developed. 

Jef Akst, writing for the journal, The Scientist, stated that the film is good depiction of the "hard to swallow fiscal issues of drug development". 

===Box office===
The film opened at #8 on its opening weekend, taking in $6 million. The film experienced sharp declines and only remained in theaters for four weeks as it only earned $12 million, making it a box office bomb.

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 