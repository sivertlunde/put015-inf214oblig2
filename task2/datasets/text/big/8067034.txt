Hukkle
 
{{Infobox Film
| name           = Hukkle
| image          = Hukkle-dvd.jpg
| caption        = DVD cover
| director       = György Pálfi
| producer       = Csaba Bereczki András Böhm
| writer         = György Pálfi
| starring       = Ferenc Bandi Józsefné Rácz
| music          = Balázs Barna Samu Gryllus
| cinematography = Gergely Pohárnok
| editing        = Gábor Marinkás
| distributor    = Arsenal
| released       = 2002
| runtime        = 78 minutes
| country        = Hungary Hungarian Czech Czech 
| budget         = $100,000
| gross         = 
| preceded_by    = 
| followed_by    = 
}}
 2002 cinema Hungarian film directed by György Pálfi about the daily life of people in a random village.  The story is based on the Angel Makers of Nagyrév.

== Characters ==
* Ferenc Bandi - Csuklik bácsi
* Józsefné Rácz - Bába
* József Forkas - Rendõr
* Ferenc Nagy - Méhész
* Ferencné Virág - A méhész felesége
* Mihályné Király - Nagymama
* Mihály Király - Nagypapa
* Eszter Ónodi - Városi anya
* Attila Kaszás - Városi papa
* Szimonetta Koncz - Városi kislány
* Gábor Nagy - Városi kisfiú
* Jánosné Gyõri - Postás
* Edit Nagy - Pásztorlány
* János F. Kovács - Vízhordó fiú

== Plot ==
The story takes place in an ordinary village in Hungary. It begins with an old man who has hiccups, and takes place in front of his house near a can of milk. He observes the daily habits of the villagers, and the viewer is shown many sequences about different events: A young man drives his horse and cart filled with milk cans. Normally he would clean the cans, but hes distracted by a girl sitting in the sun. A threshing-machine is harvesting. A cat becomes poisoned and eventually dies. A mole is killed by an old lady ploughing the ground and she gives the mole to her dog. A farmer takes his pig to a sow for fertilization and the two owners watch with satisfaction when the pigs copulate. The men of the village bowl to kill time. The old man is still having hiccups.

The village seems idyllic, but there are mysterious things happening. During these events, there are sequences about women trading bottles with unknown liquids. From time to time a man dies and the collective village walks up with a chest and comforts the widow. The postwoman also shows up from time to time and gives the widow her dead mothers pension. It all seems harmless and normal life continues after the burials.
When a fisherman disappears, a local policeman is determined to find out what happened to the fisherman and eventually finds out at the end when he sees the mailman appear with a package for the widow. 

With (almost) no dialogue in the movie, it seems the events around the villagers, animals, and plants have no meaning. However, at the end of the movie there is a wedding where some girls sing an old folksong which reveals the murder-mystery.

== Awards ==
===Acquired===
* Cottbus Film Festival of Young East European Cinema, Audience Award: (György Pálfi)
* Cottbus Film Festival of Young East European Cinema, FIPRESCI Prize - Special Mention: (György Pálfi)
* Cottbus Film Festival of Young East European Cinema, First Work Award of the Student Jury: (György Pálfi)
* Cottbus Film Festival of Young East European Cinema, Special Price: Feature Film Competition (György Pálfi), Gergely Pohárnok
* European Film Awards, European Discovery of the Year: (György Pálfi)
* Hong Kong International Film Festival, Golden Firebird Award: (György Pálfi)
* Hungarian Film Critics Awards, Film Critics Award:Best Cinematography (Gergely Pohárnok)
* Hungarian Film Critics Awards, Film Critics Award: Best Sound (Tamás Zányi)
* Hungarian Film Critics Awards, László B. Nagy Award: (György Pálfi)
* Hungarian Film Week, "Gene Moskowitz" Critics Award: (György Pálfi)
* Hungarian Film Week, Best Debut Film: (György Pálfi)
* Molodist International Film Festival, Festival Diploma: Best Full-Length Fiction Film (György Pálfi)
* Paris Film Festival, Special Mention: (György Pálfi)
* San Sebastián International Film Festival, Best New Director - Special Mention: (György Pálfi), Csaba Bereczki, András Böhm
* Santa Fe Film Festival, Luminaria: Best Feature (György Pálfi)
* Sochi International Film Festival, Golden Rose (György Pálfi)

== References ==
 

== External links ==
* 

 

 
 
 
 
 