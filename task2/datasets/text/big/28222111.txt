Il domestico
 
{{Infobox film
| name           = Il domestico
| image          = Il domestico 1974.jpg
| caption        = Film poster
| director       = Luigi Filippo DAmico Ary Fernandes
| producer       = 
| writer         = Sandro Continenza Raimondo Vianello
| starring       = Lando Buzzanca
| music          = Piero Umiliani
| cinematography = Alessandro DEva
| editing        = Renato Cinquini
| distributor    = 
| released       =  
| runtime        = 94 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}

Il domestico is a 1974 Italian comedy film directed by Luigi Filippo DAmico and Ary Fernandes. It was shown as part of a retrospective on Italian comedy at the 67th Venice International Film Festival.   

==Cast==
* Lando Buzzanca - Rosario Cabaduni, called Sasa
* Martine Brochard - Rita
* Arnoldo Foà - Engineer Ambrogio Perigatti
* Silvia Monti
* Femi Benussi - Lola Mandragali
* Leonora Fani - (as Eleonora Fani)
* Paolo Carlini - Andrea Donati
* Enzo Cannavale - Salvatore Sperato
* Nanda Primavera
* Renzo Marignano
* Antonino Faa Di Bruno
* Erika Blanc - Silvana
* Luciano Salce - The director
* Gordon Mitchell - General Von Werner

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 