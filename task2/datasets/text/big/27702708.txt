Real Steel
 
{{Infobox film
| name           = Real Steel
| image          = Real Steel Poster.jpg
| alt            = Hugh Jackman in character in a boxing pose in front of a large boxing robot in a similar pose.
| caption        = Theatrical release poster
| director       = Shawn Levy
| producer       = Shawn Levy Susan Montford Don Murphy
| screenplay     = John Gatins
| story          = Dan Gilroy Jeremy Leven
| based on       =  
| starring       = {{Plain list |
* Hugh Jackman
* Dakota Goyo
* Evangeline Lilly
* Anthony Mackie
* Kevin Durand 
}}
| music          = Danny Elfman
| cinematography = Mauro Fiore
| editing        = Dean Zimmerman
| studio         = {{Plain list |
* Touchstone Pictures DreamWorks Pictures
* Reliance Entertainment
}} Walt Disney Studios Motion Pictures 
| released       =  
| runtime        = 127 minutes 
| country        = United States
| language       = English
| budget         = $110 million 
| gross          = $299.3 million   
}} American Science science fiction sports drama DreamWorks Pictures. a 1963 Animatronic robots were built for the film, and motion capture technology was used to depict the brawling of computer-generated robots and animatronics.

Real Steel was theatrically released by Touchstone Pictures in Australia on October 6, 2011, and in the United States and Canada on October 7, 2011, grossing nearly $300 million at the box office and received mixed reviews; with criticism for its similarities to Rocky, but yet praise to the visual effects, action sequences and acting performances. The film was nominated for the Academy Award for Best Visual Effects at the 84th Academy Awards, but lost to Hugo (film)|Hugo.

==Plot==
 
In 2020, human boxers are replaced by robots. Charlie Kenton (Hugh Jackman), a former boxer, owns "Ambush", but loses it in an arranged fight against a bull belonging to promoter and carnival owner, Ricky (Kevin Durand), who sees Charlie as a joke, partially because he beat Charlie up the last time they competed in the ring. Having made a bet that Ambush would win, Charlie now has a debt to Ricky — which he runs out on.

After the fight, Charlie learns that his ex-girlfriend died and he must attend a hearing deciding the future of their son, Max (Dakota Goyo). There, Maxs aunt, Debra (Hope Davis), and her wealthy husband, Marvin (James Rebhorn), request full custody, which Charlie concedes for $100,000; half in advance, on the condition that Charlie retains Max for three months. Thereupon, Charlie and Max, and Bailey Tallet (Evangeline Lilly), the daughter of Charlies former boxing coach, acquire the once-famous "Noisy Boy", arrange a fight but is destroyed by "Midas". Attempting to scavenge parts of a new robot from a junkyard, Max discovers "Atom", an obsolete but intact sparring robot designed to withstand severe damage, and capable of mirroring its handlers movement.

At Maxs behest, Charlie pits Atom against "Metro", whom Atom overcomes. Max convinces Charlie to control Atom, resulting in a series of victories and culminating with the defeat of national champion, "Twin Cities". Elated by their success, Max challenges global champion, "Zeus". After the fight, Ricky and his two henchmen attack and rob Charlie of their winnings, which prompts him to return Max to Debra. Persuaded by Bailey, Charlie arranges the challenge offered by Max and convinces Debra to allow Max to witness the fight. Ricky bets $100,000 that Atom will not last the first round against Zeus, but loses and is cornered by the fights bookmakers. In the penultimate round, Atoms vocal controls are damaged, whereupon Charlie guides the robot through shadow mode to weaken and overwhelm Zeus, but is unable to win within the allotted time. Zeus is declared the winner by number of blows inflicted but the near-defeat leaves Zeus team humiliated, and Atom is labelled the "Peoples Champion".

==Cast==
* Hugh Jackman as Charles Charlie Kenton
* Dakota Goyo as Max Kenton
* Evangeline Lilly as Bailey Tallet
* Anthony Mackie as Finn
* Olga Fonda as Farra Lemkova
* Karl Yune as Tak Mashido
* Kevin Durand as Ricky
* Hope Davis as Debra
* James Rebhorn as Marvin
* John Gatins as Kingpin

==Production==
  DreamWorks Pictures, Reliance Entertainment, 21 Laps Entertainment, and Montford/Murphy Productions.  The original screenplay was written by Dan Gilroy and was purchased by DreamWorks for $850,000 in 2003 or 2005 (sources differ).       The project was one of 17 that DreamWorks took from Paramount Pictures when they split in 2008.  Director Peter Berg expressed interest in the project in mid-2009 but went no further.  Levy was attached to the project in September 2009,    and Jackman was cast in the starring role in November for a $9 million fee.  In the same month, Steven Spielberg and Stacey Snider at DreamWorks greenlit the project.   Les Bohem and Jeremy Leven had worked on Gilroys screenplay, but in 2009 John Gatins was working on a new draft.  When Levy joined the project, he worked with Gatins to revise the screenplay,    spending a total of six weeks fine-tuning the script. Advertising company FIVE33 did a two-hundred page "bible" about robot boxing. Levy said he was invited by Spielberg and Snider while finishing Date Night, and while the director initially considered Real Steel to have "a crazy premise," he accepted after reading the script and feeling it could be "a really humanistic sports drama." Shawn Levy audio commentary, Real Steel Blu-Ray 
 Cobo Arena, Belle Isle Zoo, and the Highland Park Ford Plant. 
 Jurassic Park, where Winstons animatronic dinosaurs "got a better performance from the actors, as they were seeing something real, and gave the visual effects team an idea of what it would look like." As Real Steel was not based on a toy, Meyer said that "there was no guideline" for the robots, and each was designed from scratch, with an attempt to put "different personality and aesthetics," according to Levy. In Atoms case, it tried to have a more humanizing design to be an "everyman" who could attract the audiences sympathy and serve as a proxy to the viewer, with a fencing mask that Meyer explained served to show "his identity was a bit hidden, so you have to work harder to get to see him."  Executive producer Robert Zemeckis added that the mask "became a screen so we can project what we want on Atoms face." Damage was added to the robots decoration to show how they were machines worn out by intense battles. 

For scenes when computer-generated robots brawl, "simulcam" motion capture technology, developed for the film Avatar (2009 film)|Avatar, was used. As Levy described the process, " oure not only capturing the fighting of live human fighters, but youre able to take that and see it converted to   robots on a screen instantaneously. Simulcam puts the robots in the ring in real time, so you are operating your shots to the fight, whereas even three, four years ago, you used to operate to empty frames, just guessing at what stuff was going to look like."    Boxing hall-of-famer Sugar Ray Leonard was an adviser for these scenes  and gave Jackman boxing lessons so his moves would be more natural. 

==Release==
Real Steel had its  .  The film was released in 3,440 theaters in the United States and Canada,  including 270 IMAX screenings. There were also over 100 IMAX screenings in territories outside the United States and Canada, with 62 screening on October 7. 
===Marketing===
 .  In May 2011, DreamWorks released a second trailer. While the film features boxing robots, Levy said he wanted to show in the trailer "the father-son drama, the emotion Americana of it". He said, "We are very much the robo-boxing movie, but thats one piece of a broader spectrum."  In addition to marketing trailers and posters, DreamWorks enlisted the British advertising company Five33 to build large physical displays representing the film as it had done for  .  The studio also collaborated with Virgin America to name one of their Airbus A320s after the film, and one of the films robots is pictured on its fuselage.  On September 19, Jackman appeared on the weekly sports entertainment program WWE Raw to promote the film.  In addition to Jackman making an appearance on the show, WWE named Crystal Methods "Make Some Noise" from the films soundtrack as the official theme song for their returning Pay-per-view|PPV, Vengeance (2011)|Vengeance.

Jakks Pacific released a toy line with action figures based on Atom, Zeus, Noisy Boy, Midas and Twin Cities.  The company has also released a one-on-one, playset fighting game with robots in a ring. 
ThreeA released a line of high-end sixth-scale figures, as adapted by Australian artist Ashley Wood, based on Ambush, Atom, Midas, and Noisy Boy.

Jump Games released a fighting video game based on the film for Android and iOS devices,  and Yukes has made a game for the PS3 and Xbox 360 

===Home media=== deleted and extended scenes with introductions by director Levy; and a profile of film consultant Sugar Ray Leonard.      

==Reception==

===Box office=== The Ides of March) and all holdovers.  It managed first-place debuts in 11 countries including Hugh Jackmans native Australia ($4.2 million). 

===Critical response===
Real Steel received mixed reviews from critics. Review aggregation website  , which assigns a weighted average score out of 100 to reviews from mainstream critics, gives the film a rating score of 56, based on 34 reviews.  Audiences surveyed by CinemaScore during the opening weekend gave the film a grade A, on scale from A plus to F. 

Roger Ebert of the Chicago Sun-Times rated the film three out of four stars, saying, "Real Steel is a real movie. It has characters, it matters who they are, it makes sense of its action, it has a compelling plot. Sometimes you go into a movie with low expectations and are pleasantly surprised."  Lisa Schwarzbaum of Entertainment Weekly gave the film an A-, saying director Levy "makes good use of his specialized skill in blending people and computer-made imaginary things into one lively, emotionally satisfying story". 

Conversely, Claudia Puig of USA Today said that, "Though the premise of fighting robots does seem a plausible and intriguing extension of the contemporary WWE world, Real Steel is hampered by leaden, clichéd moments in which a stubborn boy teaches his childish father a valuable lesson."  James White of the UK magazine Empire (magazine)|Empire gave the film 3 of 5 stars, saying, "Rocky with robots? Its not quite in Balboas weight class, but Real Steel at least has some heft. Theres barely a story beat among the beat-downs that you wont expect, and sometimes the saccharine gets in the way of the spectacle, but on the whole this is enjoyable family entertainment." 

===Accolades===
{| class="wikitable" rowspan=5; style="text-align: center; background:#ffffff;"
! Award !! Nominee !! Category!! Result 
|- Hugh Jackman||Favorite Action Movie Star||  
|- Young Artist Dakota Goyo||Young Best Performance in a Feature Film - Leading Young Actor||  
|- Saturn Award||Saturn Best Performance by a Young Actor||  
|- 84th Academy Academy Awards||Erik Academy Award Best Visual Effects||  
|}

==Soundtrack==
Real Steel soundtrack consists of 13 tracks featuring artists including Tom Morello, Eminem, Royce da 59" (Bad Meets Evil), Yelawolf, 50 Cent, Limp Bizkit and Foo Fighters. Levy, a fan of The Crystal Method, invited that duo to contribute to the soundtrack; they recorded two new songs for it after viewing a rough cut of the film. 

{{Track listing
| collapsed       = yes
| headline        = Real Steel - Music From The Motion Picture 
| total_length    = 0:48:53
| music_credits   = yes Fast Lane 
| music1 = Bad Meets Evil | length1 = 4:12
| title2 = Heres A Little Something For Ya 
| music2 = Beastie Boys | length2 = 3:09
| title3 = Miss The Misery 
| music3 = Foo Fighters | length3 = 4:32
| title4 = The Enforcer 
| music4 = 50 Cent | length4 = 3:25
| title5 = Make Some Noise (Put em Up) 
| music5 = The Crystal Method | length5 = 3:27
| title6 = Till I Collapse 
| music6 = Eminem | length6 = 4:59
| title7 = One Man Army 
| music7 = The Prodigy | length7 = 4:15
| title8 = Give It A Go 
| music8 = Timbaland, Veronica | length8 = 4:20
| title9 = The Midas Touch 
| music9 = Tom Morello | length9 = 3:28
| title10 = Why Try 
| music10 = Limp Bizkit | length10 = 2:53
| title11 = Torture 
| music11 = Rival Sons | length11 = 3:37
| title12 = All My Days 
| music12 = Alexi Murdoch | length12 = 4:56
| title13 = Kenton 
| music13 = Danny Elfman | length13 = 1:40
}}

The score album of "Real Steel: Original Motion Picture Score" consists of 19 tracks composed by Danny Elfman; released  November 1, 2011, in the US. Levy considered Elfman one of the few composers who could do a score similar to that of the Rocky franchise, alternating guitar-based ambient music and songs with a full orchestra. 

{{Track listing
| collapsed       = yes
| headline        = Real Steel 
| total_length    = 0:42:19
| all_music       = Danny Elfman

| title1 = Charlie Trains Atom 
| length1 = 1:59
| title2 = On The Move 
| length2 = 2:39
| title3 = Into The Zoo 
| length3 = 1:02
| title4 = Why Were Here (feat. vocal by Poe (singer)|Poe) 
| length4 = 0:55
| title5 = Meet Atom 
| length5 = 3:17
| title6 = Its Your Choice 
| length6 = 1:28
| title7 = Safe With Me 
| length7 = 2:57
| title8 = Atom Versus Twin Cities 
| length8 = 3:12
| title9 = ...For A Kiss 
| length9 = 0:56
| title10 = Get In The Truck 
| length10 = 1:12
| title11 = Bonding 
| length11 = 2:01
| title12 = Twin Cities Intro 
| length12 = 1:20
| title13 = Parkway Motel (feat. vocal by Poe (singer)|Poe) 
| length13 = 1:47
| title14 = This Is A Brawl 
| length14 = 1:48
| title15 = You Deserve Better 
| length15 = 4:02
| title16 = Into The Ring 
| length16 = 1:12
| title17 = Taking A Beating 
| length17 = 1:33
| title18 = Final Round 
| length18 = 6:53
| title19 = Peoples Champion 
| length19 = 2:06
}}

==Sequel==
In April 2011, DreamWorks announced it was developing a sequel, and that John Gatins, who wrote the screenplay for the first film, was hired to script the second.  Touchstone Pictures, which distributed the first, will co-produce and co-finance, with DreamWorks, and distribute the film. Director Shawn Levy said in September 2011 a sequel depended on the success of the first film and that he would also direct it. The key actors—Jackman, Lilly, and Goyo—would reprise their roles if the studio proceeded with a sequel, though the production schedule would need to match Jackmans crowded schedule. 

==See also==
* Steel (The Twilight Zone)|"Steel" (The Twilight Zone), a 1963 episode of The Twilight Zone also based on Richard Mathesons short story List of sports films – Boxing
* Pluto (manga)|Pluto, Naoki Urasawas manga adaptation of an Astroboy arc which also incorporates robot tournament fighting.

==References==
{{Reflist|30em|refs=
   
   
}}

==External links==
 
 
*  
*  
*  
*  
*  
*  
*  
*   at  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 