Femme Fatale (2002 film)
 
{{Infobox film 
| name = Femme Fatale
| image = Femme fatale poster.jpg
| image_size = 215px
| alt = 
| caption = French release poster
| director = Brian De Palma
| producer = Tarak Ben Ammar Marina Gefter
| writer = Brian De Palma Rebecca Romijn-Stamos Antonio Banderas Peter Coyote
| music = Ryuichi Sakamoto
| cinematography = Thierry Arbogast
| editing = Bill Pankow
| studio = Epsilon Motion Pictures Quinta Communications
| distributor = Warner Bros.
| released =  
| runtime = 114 minutes
| country = France
| language = English French
| budget = $35 million
| gross = $16.8 million
}}
Femme Fatale is a 2002 French erotic thriller/mystery film directed by Brian De Palma. The film stars Rebecca Romijn and Antonio Banderas. It was screened out of competition at the 2002 Cannes Film Festival.   

==Plot==
Mercenary thief Laure Ash (Rebecca Romijn) participates in a diamond heist in Cannes Film Festival|Cannes.  The plan is for Laure to steal valuable diamonds from the ensemble of a female attendant named Veronica (Rie Rasmussen) while in the middle of seducing her, during which her accomplices "Black Tie" (Eriq Ebouaney) and Racine (Édouard Montrouge) provide various support.  However, Laure instead double-crosses her accomplices and escapes to Paris with the diamonds.  In Paris, a series of events causes Laure to be mistaken for her own doppelgänger, a missing Parisian woman named "Lily" (also portrayed by Romijn) who had recently disappeared.  While Laure luxuriates in a tub in Lilys home, the real Lily returns and commits suicide while Laure secretly watches, leaving Laure the opportunity to take her identity for good and leave the country to United States|America. 

Seven years later, Laure (in her identity as "Lily") resurfaces in Paris as the wife of Bruce Watts, the new American ambassador to France (Peter Coyote).  After arriving in France, a Spanish paparazzo named Nicolas Bardo (Antonio Banderas) takes her picture.  The picture is displayed around Paris, and Black Tie spots Bardos photo while in the middle of killing Veronica by throwing her into the path of a speeding truck.  With Laure exposed to her vengeful ex-accomplices, she decides to frame Bardo for her own (staged) kidnapping.  Bardo is further manipulated by Laure into following through with the "kidnapping", and in the process, begin a sexual relationship.  The pair eventually meet with Bruce for an exchange, however, Bardo has a crisis of conscience at the last moment and sabotages the scheme.  In retaliation, Laure executes both Bardo and Bruce, only to be surprised by her ex-accomplices afterward, who promptly throw Laure off a bridge to her seeming death.

In an extended twist ending, the entirety of the movies events after Laure enters the tub in Lilys home are revealed to be a dream.  Laure spies Lily entering the home as before, but this time stops her from committing suicide.  Seven years later, Laure and Veronica, who is revealed to have been Laures partner all along, chat about the success of their diamond caper.  Black Tie and Racine arrive seeking revenge, but they are killed by the same truck that killed Veronica in Laures dream.  Bardo, witnessing all these events, introduces himself to Laure, swearing that he has met her before, with Laure replying "Only in my dreams".

==Cast== Rebecca Romijn-Stamos as Laure Ash / Lily Watts
* Antonio Banderas as Nicolas Bardo
* Peter Coyote as Ambassador Bruce Watts
* Eriq Ebouaney as Black Tie
* Rie Rasmussen as Veronica
* Thierry Frémont as Inspector Serra
* Gregg Henry as Shiff
* Éva Darlan as Irma
* Fiona Curzon as Stanfield Phillips
* John Stamos as Cheesy Agent

==Reception==

===Critical reception===
The film received mixed reviews; however, it was praised by several high profile critics, notably Roger Ebert, who gave it a 4 star review and called it one of De Palmas best films.  
The film has since developed a cult status amongst cinephiles.  Femme Fatale currently holds a 48% rating on Rotten Tomatoes based on 134 reviews.

===Box office===
The film was a box office bomb, taking in less than its production costs worldwide.

{| class="wikitable"
|-
| US domestic gross || style="text-align:right;"| US$6,630,252
|-
| International gross || style="text-align:right;"| $10,208,658
|-
| Worldwide gross || style="text-align:right;"| $16,838,910
|}

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 