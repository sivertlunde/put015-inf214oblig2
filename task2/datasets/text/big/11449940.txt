His Favourite Pastime
{{Infobox film
  | name = His Favourite Pastime
  | image = His Favourite Pastime 1914.jpg
  | imagesize= 
  | caption = Film still George Nichols
  | producer = Mack Sennett
  | writer = Craig Hutchinson
  | starring = Charles Chaplin   Roscoe Fatty Arbuckle   Viola Barry
  | music =  Frank D. Williams
  | editing = 
  | distributor = Keystone Studios
  | released =  
  | runtime = 16 minutes Silent (English intertitles)
  | country = United States
  | budget = 
  | preceded by = 
  | followed by = 
}}
  1914 American comedy film starring Charlie Chaplin.

==Synopsis==
Charlie gets drunk in the bar. He steps outside, meets a pretty woman, tries to flirt with her, only to retreat after the womans father returns. Returning to the bar, Charlie drinks some more and engages in rogue behaviors with others. He finally leaves the bar, sees the woman leaving, follows the woman home, and proceeds to make a nuisance of himself, eventually getting kicked out of the house.

==Cast==
* Charles Chaplin - Drunken masher
* Roscoe Fatty Arbuckle - Shabby drunk
* Peggy Pearce - Beautiful lady

==See also==
* List of American films of 1914
* Charlie Chaplin filmography
* Fatty Arbuckle filmography

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 


 