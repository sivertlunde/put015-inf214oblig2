Bars and Stripes
 
{{Infobox Hollywood cartoon|
| cartoon_name = Bars and Stripes
| series = Krazy Kat
| image = KrazyBarsnstripes.jpg
| caption = Screenshot
| director = Ben Harrison Manny Gould
| story_artist = Manny Gould
| animator = Allen Rose Jack Carr
| voice_actor = 
| musician = Joe de Nat
| producer = Charles Mintz
| studio = 
| distributor = Columbia Pictures
| release_date = October 15, 1931
| color_process = Black and white
| runtime = 6:13 English
| preceded_by = Weenie Roast
| followed_by = Hash House Blues
}}

Bars and Stripes is a 1931 short film from Columbia Pictures, and part of the Krazy Kat theatrical cartoons.

==Plot== bow and shoves him off. Krazy then picks up another one, and the two engaged in a bow fight. The duel ends with the feline tossing the big violin out the window. Krazy goes on to discard his other instruments in a similar fashion for their disloyalty. Down on the pavement outside, the double bass tells the cat that they will return in a larger number and get back at him.

To build up his battalion, the double bass and his associates parade on the street, calling for other instruments to join them. For every house they pass by, at least one comes out to go with the pack. In such a short period, a considerable number is reached, and they are ready to take on their feline foe.

The army of musical instruments arrive at the vicinity of Krazys place, and began firing their guns at the house. Despite the significant odds against him, Krazy takes a machine gun, and is able to shoot down much of the outside forces. Although they suffer numerous casualties, the instruments refuse to concede. They continue their gunfire until Krazys house begins to crumble, but the feline is, nonetheless, unscathed. Closing in on the cat, one of the incoming attackers go for the coup de grace, only to receive a backfire. Instead of landing on Krazy, the projectile pierces a barrel of whiskey, causing it to spray its contents onto the last instruments still on the offensive. As a conclusion to the battle, the instruments are intoxicated, and they play a blues tune before finally succumbing. Krazy remains on his feet and therefore gets the last laugh.

==Availability==
The short is available in the Attack of the 30s Characters collection.

==External links==
*  at the Big Cartoon Database

 

 
 
 
 
 
 
 
 
 
 


 