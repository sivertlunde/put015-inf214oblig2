The Bandit Trail
{{Infobox film
| name           = The Bandit Trail
| image          =
| image_size     =
| caption        =
| director       = Edward Killy
| producer       = Bert Gilroy
| writer         =
| starring       = Tim Holt
| music          =
| cinematography = Harry J. Wild
| editing        = RKO Radio Pictures
| released       =  
| runtime        =
| country        = United States
| language       = English
| budget         = $45,000 Richard Jewell & Vernon Harbin, The RKO Story. New Rochelle, New York: Arlington House, 1982. p163. 
| gross          =
}}
The Bandit Trail is a 1941 Western film. 

==Plot==
A cowboy helps rob a bank to get revenge on an unscrupulous banker.

==Cast==
* Tim Holt as Steve Haggerty
* Ray Whitley as Smokey
* Janet Waldo	as Ellen Grant
* Lee Lasses White as Whopper
* Morris Ankrum as Red Haggerty
* Roy Barcroft as Joel Nesbitt

==References==
 

==External list==
*  
*  

 
 
 
 
 


 