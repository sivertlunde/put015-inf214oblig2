Arsene Lupin, Detective
{{Infobox film
| name           = Arsène Lupin détective 
| image          =
| caption        =
| director       = Henri Diamant-Berger
| producer       = Henri Diamant-Berger
| writer         = Henri Diamant-Berger (  and  ) Jean Nohain ( )
| based on       =  
| starring       =  Jules Berry  Suzy Prim   Gabriel Signoret Jean Lenoir
| cinematography = Maurice Desfassiaux  André Dantan
| editing        = Marthe Leroux
| studio         = Le Film dArt
| distributor    = Distribution Parisienne de Films
| released       =  29 April 1937 (France) 
| runtime        = 98 min 
| country        = France
| awards         = French
| budget         = 
}}
 French crime film directed by Henri Diamant-Berger and starring Jules Berry, Gabriel Signoret and Suzy Prim.

==Plot==
Arsène Lupin decides to run a detective agency in addition to being a gentleman thief. As a detective he happens to cooperate with police in order to unveil the criminal activities of a villain. When he succeeds the villain returns the favour. The unmasked Arsène Lupin manages to escape with the villains gangster moll as his new companion.

==Cast==
* Jules Berry - Barnett, alias Arsène Lupin 
* Gabriel Signoret - Inspector Béchoux 
* Suzy Prim - Olga Vauban 
* Rosine Deréan - Germaine Laurent 
* Aimé Simon-Girard - the journalist
* Thomy Bourdelle - Cassire 
* Mady Berry - Victoire 
* Abel Jacquin - Brémond

==Bibliography==
* Oscherwitz,Dayna & Higgins, MaryEllen. The A to Z of French Cinema. Scarecrow Press, 2009.

==External links==
* 

 

 
 
 
 
 
 
 


 