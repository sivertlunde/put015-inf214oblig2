Landmandsliv
{{Infobox film
| name           = Landmandsliv
| image          = 
| caption        = 
| director       = Erik Balling
| producer       = Bo Christensen Henning Karmark
| writer         = Erik Balling Fritz Reuter Paul Sarauw
| starring       = Lone Hertz
| music          = 
| cinematography = Jørgen Skov
| editing        = 
| distributor    = ASA Film
| released       = 1 October 1965
| runtime        = 108 minutes
| country        = Denmark
| language       = Danish
| budget         = 
}}

Landmandsliv is a 1965 Danish comedy film directed by Erik Balling and starring Lone Hertz.

==Cast==
* Lone Hertz - Marie Møller
* Morten Grunwald - Frits Triddlefitz
* Ellen Winther Lembourn - Louise Havermann
* Pouel Kern - Karl Havermann
* Frits Helmuth - Grev Frantz
* Holger Juul Hansen - Grev Axel von Rambow
* Helle Virkner - Grevinde Frida von Rambow
* Helge Kjærulff-Schmidt - Zakarias Bræsig
* Poul Reichhardt - Fukse Frederiksen
* Hans Kurt - Markus Mukke Mackenfeldt
* Kirsten Walther - Elvira Victoria Cornelia Hønse Hansen Ernst Meyer - Jokum
* Lise Thomsen - Kvinde der danser med Frederiksen
* Kirsten Søberg - Kokkepigen
* Henny Lindorff Buckhøj - Gæst på godset

==External links==
* 

 
 
 
 
 
 
 


 
 