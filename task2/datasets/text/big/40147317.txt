The Tattooed Stranger
{{Infobox film
| name           = The Tattooed Stranger
| image          = Tattooed stranger 1950 poster.jpg
| image_size     =
| alt            =
| caption        = Theatrical release poster
| director       = Edward Montagne
| producer       = Jay Bonafield
| screenplay     = Philip H. Reisman Jr.
| narrator       =
| starring       = {{plainlist| John Miles
* Patricia Barry Walter Kinsella
* Frank Tweddell
}}
| music          = Alan Shulman
| cinematography = William O. Steiner
| editing        = David Cooper
| studio         = RKO Pictures
| distributor    = RKO Pictures
| released       =   |ref2= }}
| runtime        = 64 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} John Miles, Walter Kinsella and Frank Tweddell.  The picture was one of the first movies featuring Jack Lord, who went on to star in the television series Hawaii Five-O.   The film was John Miles last film.

==Plot== John Miles) leads the investigation of a series of brutal murders.

==Cast== John Miles as Detective Tobin
* Patricia Barry as Mary Mahan Walter Kinsella as Lieutenant Corrigan
* Frank Tweddell as Captain Lundquist
* Rod McLennan as Captain Gavin
* Henry Lasko as Joe Canko
* Arthur L. Jarrett as Johnny Marseille
* Jim Boles as Fisher
* William Gibberson as Aberfoyle
* Jack Lord as Det. Deke Del Vecchio

==Reception==
The New York Times wrote, "The thrills are few and far between in this manhunt but its authenticity is obvious." 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 