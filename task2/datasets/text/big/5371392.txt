Varavelpu
{{Infobox film
| name           = Varavelpu
| image          = Varavelppu.jpg
| caption        = The DVD Cover
| director       = Sathyan Anthikkad
| producer       = K. Rajagopal Sreenivasan
| Sreenivasan Murali Murali Innocent Innocent Thilakan Johnson
| cinematography = Vipin Mohan
| editing        = K. Rajagopal
| distributor    = K. R. Gangadharan|K. R. G. Enterprises
| released       =  
| runtime        = 145 minutes
| country        = India
| language       = Malayalam
}}

Varavelpu (  black comedy film directed by Sathyan Anthikkad and written by Sreenivasan (actor)|Sreenivasan, starring Mohanlal, Revathi, Sreenivasan (actor)|Sreenivasan, Thilakan, Murali (Malayalam actor)|Murali, Innocent (actor)|Innocent, and Jagadish. The film was produced by K. Rajagopal under the banner of K. R. G. Movie International and has been distributed by K. R. G. Enterprises. 

The film depicted trade union problems plaguing Kerala in a satirical dark comedy and some stellar performances from the lead actors, this movie went onto become a commercial and critical success.

==Plot summary==
The movie is about a young Keralite who returns to his home state of Kerala after 7 long years of toil in the Gulf. He now plans to settle in his native town, with whatever savings he managed over the years. The movie traces his plight as he ventures to spend the rest of his life running a bus in a state where employee strikeouts are a common day-to-day event. How he finally has to return to the Gulf losing everything, for another coming (welcome) forms the crux of the movie.

==Cast==
*Mohanlal as Murali
*Revathi as Rama
*Oduvil Unnikrishnan as Narayanan
*Jagadeesh as Valsan Janardhanan as Kumaran
*KPAC Lalitha as Santha
*Meena as Rukmini
*Mamukoya as Hamza Innocent as Chathukutty Sreenivasan as Vehicle Inspector
*Thikkurissy as Apal bandavan Govindan Nair Murali as Prabhakaran
*Sankaradi as Muralis uncle
*Bobby Kottarakkara as Pappan
*Thilakan as Labour Officer Ramakrishnan
*Praseetha Menon

==Songs== Johnson and has been written by Kaithapram Damodaran Namboothiri.

# Doore doore sagaram thedi: K. J. Yesudas
# Doore doore sagaram thedi: K. S. Chithra
# Vellara poomala mele: K. J. Yesudas

==External links==
*  
* http://popcorn.oneindia.in/title/5978/varavelppu.html

 
 
 
 
 
 


 