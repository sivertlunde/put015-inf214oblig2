Scrap Happy Daffy
 
{{Infobox Hollywood cartoon|
| cartoon_name = Scrap Happy Daffy
| series = Looney Tunes (Daffy Duck)
| image = ScrapHappyDaffy.jpg
| image size = 250px
| caption = Title card
| director = Frank Tashlin
| story_artist = Don Christensen
| animator = Credited: Art Davis Uncredited: Cal Dalton Phil Monroe I. Ellis
| voice_actor = Mel Blanc
| musician = Carl W. Stalling
| producer = Leon Schlesinger The Vitaphone Corporation
| release_date = August 21, 1943 (USA)
| color_process = Black-and-white
| runtime = 8 min (one reel)
| movie_language = English
}}

Scrap Happy Daffy is a Warner Bros. World War II short featuring Daffy Duck, directed by Frank Tashlin and released in 1943. Daffy is the guard of a scrap yard, doing his part to help America win the war against the Nazis, but the Nazis decide to destroy his scrap pile by sending a billy goat out to eat everything in sight.
 black and white.

==Plot==
Daffy is a guard at a scrap pile, encouraging Americans to "Get the tin out", "Get the iron out" and especially " ". Singing Were in to Win, Daffy goes over the various things Americans can send to help with the war effort. However, this doesnt go down well with Adolf Hitler, who reads about Daffys scrap pile helping to beat Benito Mussolini, and responds to this by giving his men the following order: "Destroy that scrap pile!"

With the word out, a Nazi submarine sends a torpedo to the scrap pile — which has a billy goat inside, who immediately starts eating everything in sight. Daffy, hearing the noise, tries to find out whats making the noise. After temporarily pointing a rifle at a reflection of himself (thinking that he cornered someone else), Daffy finds the goat hiccuping with the garbage inside him and amiably offers him a glass of Alka-Seltzer. However, when Daffy sees the swastika that the goat (whom he derides as a "tin termite") is wearing, he starts messing with the goat. Temporarily getting the better of the goat, Daffy is almost undone when he tries to whack the goat with a mallet - but the mallet gets stuck in the goats horns and the goat knocks Daffy around.
 include us out!"

==Availability==
This cartoon was colorized in 1995, with a computer adding color to a new print of the original black and white cartoon. This preserved the quality of the original animation.  However, this new colorized version was never broadcast on American television (due to its subject matter). A clip of this cartoon (which, oddly, was featured colorized and restored) was shown on a documentary about World War II-era cartoons (" ,  and is featured independently on the   (shown in its original black and white format). The original black and white cartoon is also available as a special feature for the DVD release of the Warner Bros. 1943 movie Air Force. This cartoon has now fallen into the public domain because the copyright was not renewed after 1971.

==External links==
* 
* .

 

 
 
 
 
 
 
 
 