Tokyo Park
{{Infobox film
| name           = Tokyo Park
| image          = Tokyo Koen Movie Poster.jpg
| caption        = Theatrical release poster
| director       = Shinji Aoyama
| producer       = Hiroaki Saito Yasushi Yamazaki
| writer         = Shinji Aoyama Masaaki Uchida Norihiko Goda
| screenplay     = 
| story          = 
| based on       =  
| starring       = Haruma Miura Nana Eikura Manami Konishi Haruka Igawa
| music          = Isao Yamada Shinji Aoyama
| cinematography = Yuta Tsukinaga
| editing        = Hidemi Ri
| studio         = 
| distributor    = Showgate
| released       =  
| runtime        = 119 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
}} 2011 Japanese drama film directed by Shinji Aoyama. It is based on the novel Tokyo Koen by Yukiya Shoji.     It was released in Japanese cinemas on 18 June 2011.   

==Plot==
Miura Haruma takes on the role of Koji, a college student aiming to become a professional photographer. One day, he receives an unusual request to shadow the clients girlfriend and take pictures of her; this assignment leads to subtle changes in his relationships with the women around him. Nana Eikura plays the ex-girlfriend of Kojis childhood friend, while Manami Konishi plays Kojis sister after one of her parents remarries, and Haruka Igawa plays the woman that Koji is photographing.

==Cast==
* Haruma Miura as Koji Shida
* Nana Eikura as Miyu Tominaga
* Manami Konishi as Misaki Shida
* Haruka Igawa as Yurika Hatsushima
* Shota Sometani as Hiro Takai
* Yo Takahashi as Takashi Hatsushima 
* Takashi Ukaji as Kenichi Haraki

==Release==
Tokyo Koen was showcased at the 64th Locarno International Film Festival to compete for the Golden Leopard award in August 2011.    The film was awarded a special Golden Leopard to honor Aoyamas career. 

==Reception==
Mark Shilling of The Japan Times gave the film a rating of 3.5 stars out of five. He noted that the film was "a new outlook and approach" by the director Shinji Aoyama. He described the character Koji as "an amiable, passive empty slate on which the more aggressive, knowing types around him can write their own dramas".    Neil Young of The Hollywood Reporter said Tokyo Park was Aoyamas most mainstream, conventional film. He cited "the insipid blandness of a daytime soap-opera, lacking anything resembling urgency, edge or originality", saying that the cinematography was "picturesquely banal" and the score was "jauntily intrusive".    Jay Weissberg of Variety (magazine)|Variety felt that the film existed in an airless state despite its outdoor sequences.   

==References==
 

==External links==
*    
*  

 

 
 
 
 
 
 


 