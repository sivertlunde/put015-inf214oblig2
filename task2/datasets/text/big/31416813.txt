Bombay March 12
 
 
{{Infobox film
| name           = Bombay March  12
| image          = 1993 Bombay, March 12.jpg
| alt            =
| caption        = Promotional poster
| director       = Babu Janardhanan
| producer       = Haneef Muhammed
| writer         = Babu Janardhanan Roma
| music          = Songs:  
| cinematography = Vipin Mohan
| editing        = Vijay Shankar
| studio         = Red Rose Creations
| distributor    = Red Rose Release & PJ Entertainments
| released       =   
| runtime        = 
| country        = India
| language       = Malayalam
| budget         =
| gross          =
}} Indian drama 1993 Bombay Roma and hyperlink format. It opened on 30 June in theatres and, despite receiving positive critical reviews, did not fare well at the box office.

==Plot==

The story is about Sameer (Mammootty), a sweeper with the Alappuzha Municipality, who is always on tenterhooks because he is on the police radar whenever there is trouble or an act of terror occurs. Later it is revealed that his brother-in-law Shahjahan (Unni Mukundan) was suspected to be part of the terror team that had planted bombs in Bombay in 1993 and was later killed in an encounter with the Army in Andhra Pradesh.

The narrative moves back and forth, slowly connecting Sameer with Shahjahan who had gone to Bombay as an IT professional and from whom much was expected by his family. The innocent youngster gets entangled in the web of a terror group and goes into hiding fearing the safety of his loved ones.

==Cast==
* Mammootty as Sameer / Sadananda Bhatt
* Unni Mukundan as Shahjahan Roma as Abidha
*Sudheer Karamana  as Anti Terrorism Squad Officer Shari
* Sadiq
* Anil Murali Lal
* Rajeev Govinda Pillai
* Baby Diya
* Akhil Dev

==Production==

===Casting===
Sameer, one of the two characters played by Mammooty in the film, is inspired by the Peoples Democratic Party (India)|Peoples Democratic Party leader Abdul Nasser Madanis long incarceration as an under-trial in a Tamil Nadu jail. Likewise, Sameer spends nine and a half years in jail as an accused in a blast case and is finally acquitted of all charges. The director says that the film transmits not just the trauma of Madani but of every under-trial who had to undergo long imprisonment. "Only the core idea is taken from real life and the rest is fiction. Madani is a political leader. But Sameer is presented as an ordinary person", he said. 

Unni Mukundan (Krishna), who plays a major supporting role in Bombay March 12, was formerly working as an assistant to veteran director A. K. Lohithadas.   Model-turned-child artiste Babydiya makes her film debut with Bombay March 12. 

===Filming=== Hyderabad and Tamil and Telugu language|Telugu.    There was uzz that Bombay March 12 might be shot in Hindi as well.   

==Music== Lijo Jose City of God in December 2010. Babu Janardhanan was really amazed at how the soundtrack had shaped up for City of God, which was scripted by him. In January, Babu Janardhanan approached Prashant and asked him to score the background music for the movie. Prashant was initially apprehensive about scoring for the film after learning how sensitive it is and given there is a superstar the music would demand a commercial tone. But Babu Janardhan was kind enough to let him come up with his own creativity. After working on a few templates over a few days, he decided on one that really set the tempo. He completed the score on 15 June 2011. 

The soundtrack to Bombay March 12 is composed by Babu Janardhanans long-time friend Afzal Yusuf. Two popular Bollywood singers, Sonu Nigam and Sadhana Sargam, have recorded Malayalam songs for this film. While Sadhana Sargam earlier sang "Ponmuzham Thandil" in the same year for Oru Nuna Kadha, Sonu Nigam, who already marked his debut in Malayalam in Major Ravis Kandahar (2010 film)|Kandahar by singing a Hindi song, is singing this Malayalam number for the first time. The song titled "Chakkaramavin Kombathu" has lyrics penned by Rafeeq Ahmed. 

; Track listing
{{tracklist
| headline        =
| extra_column    = Singer(s)
| total_length    =
| all_lyrics      = Rafeeq Ahmed
| title1          = Chakkara Maavin
| extra1          = Sonu Nigam, Ganesh Sundaram
| length1         =
| title2          = Viriyunnu
| extra2          = Sadhana Sargam
| length2         =
| title3          = Onaveyil	
| extra3          = M. G. Sreekumar, Soney Sai, Sudheesh
| length3         =
| title4          = Maula Mere
| extra4          = Kailash Kher
| length4         =
| title5          = Chakkara Maavin
| extra5          = Sonu Nigam, Soney Sai, Ganesh Sundaram
| length5         =
| title6          = Viriyunnu
| extra6          = Usha Uthup
| length6         =
}}

==Critical reception== City of God. But there is always a niggling thought at the back of our minds that the point he is trying to make – that one bad egg can spoil the whole basket – could have been conveyed just as well in a more simple and linear narrative."  , Rediff, 
July 1, 2011  Mammoottys performance is appreciated as "one of his most subdued roles in recent times". According to Palicha, Unni Mukundan as Shahjahan "does full justice to the confidence that the director has reposed in him." 

==References==
 

 
 
 
 
 
 
 
 
 
 
 
 
 