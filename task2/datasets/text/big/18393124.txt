The Witness (1969 Hungarian film)
{{Infobox film
| name           = A tanú
| image          = A_tanu_dvd_cover.jpg
| caption        = DVD cover
| director       = Péter Bacsó
| writer         = Péter Bacsó, János Újhegyi
| starring       = Ferenc Kállai, Lajos Őze, Béla Both
| producer       =
| distributor    = Mafilm
| released       = 
| runtime        = 103 min.
| country        = Hungary
| music          = György Vukán
| cinematography =
| editing        =
| language       = Hungarian
| budget         =
| gross          =
}} satire film, 1956 Revolution was still taboo. Although it was financed and allowed to be made by the communist authorities, it was subsequently banned from release. As a result of its screening in foreign countries, the communist authorities eventually relented and allowed it to be released in Hungary. It was screened at the 1981 Cannes Film Festival in the Un Certain Regard section.    A sequel was made in 1994 named "Megint tanú" (English: Witness Again).

==Plot== communist movement of Hungary, but is now working at a dike.  He meets an old friend from the underground communist movement, Zoltán Dániel, now a government official who fishes at the Danube, near the dike.  Dániel falls in the river, and Pelikán rescues him and invites him to his home.  The ÁVH receive a "serious anonymous report" stating Pelikán committed an illegal act of slaughtering a pig for food.  Dániel tries to save him, but accidentally opens a hidden door to the basement, where all the pork had been hidden.  Pelikán is taken to prison and later released, due to the "will of the higher command".  Comrade Virág gives various assignments to Pelikán such as being the CEO of a swimming pool, an amusement park, and an Magyar Narancs|orange-research facility; all to cause Pelikán to be the witness in a show trial against Zoltán Dániel.  Before the trial they present Pelikán with testimony he must memorize, but Pelikán decides to tell the truth.  He is thrown back in prison as a reprisal.  While awaiting hanging, the political climate changes by reason of Stalins death and he is released and meets Comrade Virág who has lost all his former power and influence.

==Cast==
* Ferenc Kállai - József Pelikán, Dive Keeper
* Lajos Őze - Árpád Virág
* Béla Both - Comrade Bástya
* Zoltán Fábri - Zoltán Dániel
* Lili Monori - Gizi
* Károly Bicskey - Elemér Gulyás
* György Kézdi - Virágs Detective
* József Horváth (actor)|József Horváth - Railwayman
* Róbert Rátonyi - Operetta Actor

==References==
 

==Notes==
*Janet Maslin,  , September 26, 1981

==External links==
* 
* 

 
 
 
 
 