Little Comrade
{{Infobox film
| name           = Little Comrade
| image          = 
| alt            = 
| caption        =
| director       = Chester Withey
| producer       = Jesse L. Lasky
| screenplay     = Alice Eyton Juliet Wilbur Tompkins 
| starring       = Vivian Martin Niles Welch Gertrude Claire Richard Henry Cummings Larry Steers Elinor Hancock
| music          = 
| cinematography = Frank E. Garbutt 	
| editor         =	
| studio         = Famous Players-Lasky Corporation
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 comedy silent film directed by Chester Withey and written by Alice Eyton and Juliet Wilbur Tompkins. The film stars Vivian Martin, Niles Welch, Gertrude Claire, Richard Henry Cummings, Larry Steers and Elinor Hancock. The film was released on March 30, 1919, by Paramount Pictures.  

==Plot==
 

==Cast==
*Vivian Martin as Genevieve Rutherford Hale
*Niles Welch as Bobbie Hubbard
*Gertrude Claire as Mrs. Hubbard
*Richard Henry Cummings as Mr. Hubbard
*Larry Steers as Lieutenant Richard Hubbard 
*Elinor Hancock as Mrs. Hale 
*Nancy Chase as Isabel Hale
*Pearl Lovici as Bertha Bicknell

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 

 