Over the Odds
{{Infobox Film
| name           = Over the Odds
| image          = 
| image_size     = 
| caption        = 
| director       = Michael Forlong
| producer       = Alec Snowden
| writer         = Ernest Player   Rex Arundel
| music          = 
| cinematography = Norman Warwick
| editing        = Reginald Beck
| narrator       = 
| starring       = Marjorie Rhodes   Glenn Melvyn   Cyril Smith   Esma Cannon
| distributor    = Rank Organisation 1962
| runtime        =   65 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}} British comedy Cyril Smith, Wilfrid Lawson.  A bookmaker struggles to cope with his two mother-in-laws.  It was based on a play by Rex Arundel

==Cast==
* Marjorie Rhodes - Bridget Stone
* Glenn Melvyn - George Summers Cyril Smith - Sam
* Esma Cannon - Alice
* Thora Hird - Mrs Carter Wilfrid Lawson - Willie Summers
* Frances Cuka - Hilda Summers
* Gwen Lewis - Mrs Small
* Rex Deering - Butcher
* Patsy Rowlands - Marilyn
* Fred Griffiths - Fruit Vendor
* Leslie Crowther - Fishmonger

==References==
 

 
 
 
 
 

 