Candyjam
{{Infobox film
| name =
| image =
| image size =
| alt =
| caption =
| director = Joanna Priestley Joan Gratz
| producer = Joanna Priestley Joan Gratz
| writer =
| starring =
| music = Dave Storrs
| cinematography = Joanna Priestley
| editing = Joanna Priestley
| studio = Priestley Motion Pictures (1988)
| distributor =
| released =
| runtime = 7 minutes
| country = United States English
| budget =
| gross =
| preceded by =
| followed by =
}}
 1988 7 short animated film animated collaboration by ten animators from four countries produced and directed by Joanna Priestley and Joan Gratz. The animation uses drawings, puppets and object animation.
 David Anderson Paul Driessen (The Hague, Holland), Tom Gasek (Cambridge, MA, USA), Joan Gratz (Portland, OR, USA), Marv Newland (Vancouver, BC, Canada), Christine Panushka (Valencia, CA, USA) and Joanna Priestley.

==Awards and recognition==
*First Prize, Chicago International Film Festival
*Certificate of Merit, National Independent Film Competition
*Honorable Mention, Sinking Creek Film Festival
*Cash Award Winner, Northwest Film and Video Festival
*Honorable Mention, Espinjo International Animation Festival, Portugal, Tourn ée of Animation

==References==
 

==External links==
*  at the Internet Movie Database

 
 


 