City Island (film)
{{Infobox film
| name           = City Island
| image          = City island.jpg
| alt            = 
| caption        = Promotional poster
| director       = Raymond De Felitta Andy Garcia Lauren Versel Zachary Matz
| writer         = Raymond De Felitta
| starring       = Andy Garcia Julianna Margulies Steven Strait Emily Mortimer Ezra Miller Dominik Garcia-Lorido Alan Arkin
| music          = Jan A.P. Kaczmarek
| cinematography = Vanja Cernjul
| editing        = David Leonard CineSon Entertainment Medici Entertainment NovaStar Digital Anchor Bay Films
| released       =  
| runtime        = 104 minutes  
| country        = United States
| language       = English
| budget         = $6 million 
| gross          = $7,875,862 
}} Andy Garcia, City Island, where the film is set. 

==Plot== City Island, illegitimate son platonic bond with Molly (Emily Mortimer), an aspiring actress. 

Meanwhile, Vinces 20-year-old daughter, Vivian (Dominik Garcia-Lorido, the real-life daughter of Andy García, her on-screen father), has not told her family that she has been suspended from college, lost her scholarship, gotten breast implants, and become a stripper to try to pay for her next semester; their youngest teenage child, Vinnie (Ezra Miller), has a secret sexual fetish for feeding women (feederism), and fantasizes about their fat next-door neighbor; and Vinces wife, Joyce (Julianna Margulies), thinking she has lost all marital intimacy, sexually pursues Tony without realizing that he is her stepson. 

Vince successfully auditions for a part in a Martin Scorsese film, while his wife and Tony seek each others sexual attention. Vince, Jr. befriends his fat neighbor, who helps to bring him closer to an overweight girl whom he has been attracted to at school.

Tensions rise as the familys many dysfunctions come to a head. Tony, finally deciding to escape the insanity of the Rizzo household, steals their car to find Vivian working at the strip club. Just before the group is nearly torn apart in a violent outburst, Vince reveals the truth about everything, with Tony discovering in amazement that the dysfunctional family he sought to escape is actually his own. Vivian and the others admit their faults and Vince acknowledges the familys problems with the desire to work them out. The finally relieved group reunites in forgiveness toward one another, welcoming the overwhelmed Tony as a new member of their bizarre but loving family.

==Cast== Andy Garcia as Vince Rizzo
* Julianna Margulies as Joyce Rizzo
* Steven Strait as Tony Nardella
* Emily Mortimer as Molly Charlesworth
* Ezra Miller as Vincent "Vinnie" Rizzo, Jr.
* Dominik Garcia-Lorido as Vivian Rizzo
* Alan Arkin as Michael Malakov
* Carrie Baker Reynolds as Denise
* Hope Glendon-Ross as Cheryl

==Reception==
City Island received generally positive reviews, holding an 81% "fresh" rating on Rotten Tomatoes, with the consensus "Raymond De Felitta combines warmth, humanity, and a natural sense of humor, and is abetted by Andy Garcia and an excellent ensemble cast."  On Metacritic, which uses an average of the critics reviews, the film has a 66/100, indicating "generally favorable reviews". 

As for box office, the film was a minor success, grossing $6,671,036 domestically and $7,875,862 worldwide based on a $6 million budget.   

==Awards==
* Received the First Place Audience Award at the Tribeca Film Festival.

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 