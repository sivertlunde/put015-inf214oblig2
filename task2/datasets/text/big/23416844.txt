Détective (1985 film)
 
{{Infobox film
| name           = Détective
| image	         = Détective-poster.jpg
| caption        = Film poster
| director       = Jean-Luc Godard
| producer       = Christine Gozlan Alain Sarde
| writer         = Alain Sarde Philippe Setbon Jean-Luc Godard
| starring       = Laurent Terzieff
| music          = 
| cinematography = Louis Bihi
| editing        = Marilyne Dubreuil
| distributor    = 
| released       =  
| runtime        = 95 minutes
| country        = France
| language       = French
| budget         = 
}}

Détective is a 1985 French crime film directed by Jean-Luc Godard. It was entered into the 1985 Cannes Film Festival.   

==Cast==
* Laurent Terzieff as William Prospero
* Aurelle Doazan as Arielle
* Jean-Pierre Léaud as Inspector Neveu
* Nathalie Baye as Françoise Chenal
* Claude Brasseur as Emile Chenal
* Johnny Hallyday as Jim Fox Warner
* Alain Cuny as Old Mafioso
* Xavier Saint-Macary as Accountant Pierre Bertin as Young Son
* Alexandra Garijo as Young Daughter
* Stéphane Ferrara as Tiger Jones
* Emmanuelle Seigner as Princess of the Bahamas
* Eugène Berthier as Old manager
* Julie Delpy as Wise young girl
* Cyrille Dajinckourt as La fille
* Ann-Gisel Glass as Anne

==See also==
* Jean-Luc Godard filmography

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 