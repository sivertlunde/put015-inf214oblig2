Outcast (1922 film)
{{Infobox film
| name           = Outcast
| image          = Elsie Ferguson in Outcast.jpg
| caption        = Outcast lobby card Chester "Chet" Withey
| producer       = Famous Players-Lasky
| writer         = Hubert Henry Davies (play Outcast) Josephine Lovett (scenario) David Powell William Powell
| cinematography = Ernest Haller
| editing        =
| distributor    = Paramount Pictures
| released       =  
| runtime        = 70 minutes
| country        = United States Silent (English intertitles)
| budget         =
}}
 1922 American silent drama David Powell. William Powell has a small supporting part in this which was his third film.
 The Girl From 10th Avenue (1935) starring Bette Davis and released by Warner Brothers.

==Plot==
As described in a film publication,  Valentine Moreland (MacLaren) has married her husband for money and jilted Geoffrey Sherwood (Powell). He feels her rejection keenly, and his friend Tony Hewlitt (David) finds Geoffrey alone in his apartment drinking. Miriam, abandoned by her husband and whose child had died from neglect, has been forced out on the street when her rooms rent becomes overdue. Tony sprays some soda water out the window and hits Miriam. Miriam is invited in and Tony offers to compensate her for her hat. She tells Geoffrey her story, and he sees that he is no worse off than other people. Miriam works with Geoffrey in his South American business and he helps her get an apartment. She loves him and tries to win his affection, but he is held back by his memory of Valentine. Valentine has become weary of her elderly husband and seeks Geoffrey at his apartment. Miriam arrives and, seeing that Geoffrey is infatuated with Valentine, resolves to leave. Valentine glances out the window and, seeing her husband John (Wellesley) entering the house, panics. Miriam hides Geoffrey and Valentine in the next room, and, when John comes in, convinces him that she is Geoffreys mistress and the only woman there. After John leaves, Valentine decides to rejoin her husband. Miriam leaves and, after sending a letter to Geoffrey indicating an intent to kill herself, leaves on a boat for South America. Geoffrey pursues her in a seaplane and rescues her when she jumps of the ship. They are married and honeymoon in Rio.

==Cast==
*Elsie Ferguson - Miriam David Powell - Geoffrey Sherwood
* William David - Tony Hewlitt
*Mary MacLaren - Valentine Moreland
*Charles Wellesley - John Moreland Teddy Sampson - Nellie Essex
*William Powell - DeValle

==Preservation status==
The 1922 film is now considered a lost film.  However, one source claims a print may exist in Milan, Italy at the Cineteca Italiana. 

==References==
 

==External links==
* 
* 
* 
* 
* 
* 
* 


 
 
 
 
 
 
 