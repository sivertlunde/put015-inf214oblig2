Silent Rage
 
{{Infobox film 
| name = Silent Rage
| image =  Silent rage poster.jpg
| caption = Theatrical release poster Michael Miller
| writer = Edward Di Lorenzo   Joseph Fraley
| producer = Anthony B. Unger
| starring       = {{plainlist|
* Chuck Norris
* Ron Silver
* Steven Keats
* Toni Kalem William Finley
* Brian Libby
* Stephen Furst
}}
| cinematography = Robert C, Jessup Neil Roach
| editing = Richard C. Meyer Peter Bernstein Mark Goldenberg
| studio = Topkick Productions
| distributor = Columbia Pictures
| released =  
| runtime = 103 min. English
| country = United States
| budget =
| gross = $10,490,791 (United States) 
}} science fiction/horror horror movie starring Chuck Norris and directed by Michael Miller. 

==Plot== Brian Libby) goes insane and kills two members of a family he was staying with. Sheriff Daniel "Dan" Stevens (Chuck Norris) and his deputy Charlie (Stephen Furst) respond and eventually arrest Kirby, but Kirby breaks out of the handcuffs, overpowers other officers and grabs one of their guns, forcing the officers to open fire and shoot Kirby.  Kirby suffers severe gunshot wounds and is near death.
 William Finley respectively), who are also genetic engineers.  In an attempt to save Kirby, Dr. Spires suggests that they use the formula they created. However, Dr. Halman objects in light of Kirbys psychosis. Dr. Spires first decides that Halman is right, but then ignores the consequences and proceeds to use the formula once Dr. Halman leaves. The formula revives Kirby and renders him nearly invulnerable. Kirby then escapes from the institute and tracks Dr Halman at his home. Halman attempts to kill Kirby, but Kirby kills him first. Nancy, Halmans wife, discovers Halmans body and tries to run away from Kirby but is killed as well. Allison arrives at the Halmans to pick up some gear for a trip she and Stevens are going on only to discover the Halmans corpses. Kirby escapes and Stevens and the police arrive at the scene and Allison is taken to the Institute by Stevens and Charlie. Kirby returns to the institute to get his wounds treated by Dr Spires and Dr Vaughn. However, Spires and Vaughn realize the situation is out of control.  Spires leaves to go and look through some samples leaving Vaughn with Kirby. Vaughn injects Kirby with a fatal dose of poison. However Kirby survives and catches up with Vaughn and in a brief struggle Kirby kills Vaughn by stabbing him in the neck with the same syringe. Spires then discovers Vaughns body and goes to his office where Kirby tracks him down and kills him by breaking his neck. Meanwhile, Charlie is watching Allison while Stevens is out. Charlie and Allison leave the office and come across Kirby killing one of the workers. Charlie tries to make Kirby surrender but Kirby attacks Charlie and kills him by breaking Charlies back in a bear hug. Stevens arrives and finds Charlie fatally injured. Stevens saves Allison From Kirbys attack in Spires office.      
Sheriff Stevens tracks down Kirby in the hope that he can put an end to the carnage.
He finally finds Kirby and is able to crash his car, light him on fire, throw him into a lake, roundhouse kick him several times, and eventually throws him down a well. As Stevens leaves, however, Kirbys head comes out of the water, at which point the film ends.

==Cast==

Chuck Norris - Sheriff Dan Stevens

Ron Silver - Dr Tom Halman

Steven Keats - Dr Phillip Spires

Toni Kalem - Alison Halman
 William Finley - Dr Paul Vaughn

Brian Libby - John Kirby

Stephen Furst - Charlie

Stephanie Dunnam - Nancy Halman

==Release==

The film was released theatrically in the United States by Columbia Pictures in April 1982. It grossed $10,490,791 at the box office.    

The film was released on DVD by Sony Pictures Home Entertainment in 2001. 

RiffTrax released Silent Rage on demand on September 6, 2013. 
==Reception==
 
==Remake==
The film was remade in 2009 as Indestructible. 

==See also==
* List of American films of 1982

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 