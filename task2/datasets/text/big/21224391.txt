Guest from the Future
{{Infobox television film name = Guest from the Future image =  image_size =  caption =  runtime = 5 hours genre = Science fiction creator = Kir Bulychov writer = Pavel Arsenov Kir Bulychov director = Pavel Arsenov producer = Gosteleradio USSR starring = Natalya Guseva Aleksei Fomkin Maryana Ionesyan Ilya Naumov editing = Tatyana Malyavina music = Yevgeni Krylatov budget =  country = Soviet Union language = Russian
|network = First Program released =  first_aired = March 25, 1985 last_aired = March 30, 1985 num_episodes = 5 preceded_by = The Mystery of the Third Planet followed_by = 
}} Soviet television miniseries, made at Gorky Film Studio, first aired in 1985. It is based on the novel One Hundred Years Ahead (Сто лет тому вперёд) by Kir Bulychov.
 Natalya Guseva as Alisa Selezneva, a girl from the future that travels to the present and Aleksei Fomkin as Kolia Gerasimov, a boy from the present who travels to the future.

The series was a great success in the Soviet Union and is often reaired to this day.

==Plot==

===Part 1===

Two schoolboys, Kolya and Fima, follow a mysterious strange lady to an abandoned house. When they enter the house, they find no trace of the stranger, but in the empty basement Kolya discovers a secret door that leads to a room with a technical device in it. Curious Kolya starts pressing some buttons and activates the device that "transfers" him to another place. We learn that this place is the institute for temporal research. Employees return from time travels to different periods and deliver artifacts that are inventoried by Werther, an android who holds a secret crush on the stranger whose name is Polina. Kolya sneaks around in the corridors but is ultimatively caught by Werther who starts inventorying Kolya as well, assuming that he was brought in by Polina. On this occasion Kolya learns that he has actually travelled through time into the late 21st century. Werther ponders putting Kolya in the museum but then decides to send him back in time in order to cover up what he thinks to be Polinas mistake. Kolya convinces him to let him catch some sights of the future first. On his exploration, Kolya learns about teletransportation, humanoid aliens, antigravity and space flight. He also makes contact with a grandpa Pavel, a 130-year-old man who is intrigued by his 1980s school uniform.

===Part 2===

Having reached the space port, Kolya unsuccessfully tries to get a ticket for an interplanetary flight. However, by joining a group of pupils who escort their project satellite to the launch, he manages to enter the transit area. There he witnesses how two service workers are stunned and then impersonated by two shape-shifting aliens that have emerged from a crate. Back in the waiting hall, Kolya meets grandpa Pavel again (who turns out to be Polinas future father-in-law) and tells about what he just saw, but the old man dismisses it as childs fantasies. Grandpa Pavel introduces Kolya to Prof. Seleznyov, director of the interplanetary zoo. Kolya spots the shape-shifters again. They lure grandpa Pavel away from the group, stun and impersonate him. The fake grandpa Pavel introduces the other shape-shifter to Prof. Seleznyov as a fellow scientist. The "scientist" shows conspicuos interest in a device called "myelophone", a mind-reader. Seleznyov mentions that the device is with his daughter Alisa in the interplanetary zoo. Kolya spots and awakens the real grandpa Pavel who tells him that the aliens are space pirates and that Alisa is in danger. He asks Kolya to find Alisa. In the zoo, the pirates in new disguise steal the myelophone from Alisa who tries to read animal minds. However, with the help of a man and his talking goat, Kolya can recover the device and temporarily escapes from the pirates in an air chase. He returns to the institute. While Kolya activates the time machine, Werther sacrifices himself trying to stop the pirates. Yet they use the machine to follow Kolya into the past. Meanwhile Alisa, chasing the pirates, has reached the institute and uses the time machine as well. In the 20th century, Kolya escapes while Alisa, not accustomed to 20th-century traffic, runs into a car. Kolya and Fima discuss what to do with the myelophone. The pirates install themselves in the abandoned house and wait.

===Part 3===

Alisa is in hospital, recovering after the accident and faking amnesia in order to hide her origin. However, she talks about dolphin languages and exotic fruits. Her roommate Yulya thinks she just makes up those stories but likes her anyway. In the night, Kolya and Fima enter the basement of the abandoned house but the entrance to the time machine is blocked. On their way back out they barely evade the pirates. Alisa finds out that Yulya attends the same school as Kolya (whom she never saw but whose data she got from the talking goat). She confesses to Yulya that she is from the future and that Kolya has taken something from her. They are interrupted by the shape-shifters posing as the doctor and "Alisas father". When they cannot find the myelophone, the pirates leave. At dawn the girls run away from the hospital to Yulyas place. When they meet Fima, Yulya introduces Alisa to him as a friend from far away. Alisa tells Yulya that now Kolya is in danger because he has the myelophone. Yulya has their escape covered up by her grandmother. Meanwhile the boys are turning more and more paranoid and start acting like in spy movies. Fima has correctly guessed that Alisa is the Alisa from the future. One pirate comes too late to take Alisa from the hospital but manages to retrieve Yulyas address. Yulyas grandmother arranges for Alisa to be temporarily schooled in Yulyas school in order to find the right Kolya (as there are three and Alisa does not know his family name). She also brushes off the pirate who comes to ask for the girls. The pirates decide to surveil both Yulyas place and her school.

===Part 4===

Alisa is admitted to school, excels in English class and also receives love letters from boys. Fima, triumphant that his predictions have come true so far, wonders whether Alisa will try to eliminate Kolya as witness if he gives her the myelophone. He explains to Kolya the danger of creating alternative timelines by using knowledge from the future. The pirates and the girls spot each other in a pedestrian precinct. A chase ensues but a neighbour with a big dog scares away the pirate. In class, the girls start interrogating the three Kolyas. The right Kolya is scared but is saved by Fima distracting the girls with a story of being in love. In a park, Kolya hides from a pirate. At home, the girls wonder why none of the Kolyas seemed to be the right one. Later the pirates observe the children during sports. Alisa exceeds in long jump, sparking interest not only in the pirates but also in a talent scout. The pirates decide to kidnap Alisa but the girls see their reflection in a shop window. They run away but are stopped by the talent scout who demands to see more of Alisas abilities. When the girls finally arrive home, they find themselves sieged by the pirates. They disguise as an adult to leave the house on the next morning. In the park, the pirates approach one of the Kolyas but he annoys them by being overly talkative. They trick another classmate into showing them the schools back entrance. Meanwhile Kolya has picked up the myelophone from home and sneaks it in Yulyas schoolbag. A pirate enters the class disguised as teacher and calls out Alisa to check her schoolbag. When the real teacher encounters her copy, she drops unconscious. Kolya confesses to Alisa that he is the one who took the myelophone and runs away, distracting the pirates attention from Alisa. After a long chase, Kolya is caught, stunned and taken away. A witness is frightened into silence.

===Part 5===

Alisa has run after the pirates and Kolya but has lost their track. The witness leads her in the wrong direction. The other children arrive to help searching, as Yulya has told them Alisas secret. They worry that Kolya may be tortured in order to reveal where he keeps the myelophone. Fima believes that the device is still at Kolyas place and runs there in order to pick it up before the pirates arrive. The other children search the building where Alisa lost Kolyas track. In the abandoned house, the pirates start interrogating Kolya. Meanwhile Yulya reaches for a snack in her schoolbag and discovers the myelophone. The children use it to scan the remaining flats in the building. Alisa spots the witness and uses the myelophone on him. His thoughts tell that two men carried an uncoscious boy to the abandoned house. The children approach the house and are noticed by the pirates who also realise that Alisa carries the myelophone. As Kolya has fallen unconscious from the torture, the pirates assume his shape. One distracts most children, the other lures Alisa into the house. While she cares for Kolya, the pirate takes the myelophone. However, the talent scout has also entered the house. When the pirate threatens her, she throws him out of the window. The other pirate takes the myelophone from him and tries to leave him behind in the 20th century. However, the access to the time machine is still blocked. The children rush in but one pirate fires his blaster, forcing the children to retreat. The door to the time machine opens and Polina enters in order to arrest the pirates. They fire on her as well but she is protected by a force field and paralyses them. Alisa says goodbye and tells the children what they are going to become in the future. While she enters the secret chamber and the door closes, we hear the song "Beautiful Tomorrow".

==Cast==
* Natalya Guseva as Alisa Seleznyova
* Aleksei Fomkin as Kolya Gerasimov
* Maryana Ionesyan as Yulya Gribkova
* Ilya Naumov as Fima Korolyov
* Vyacheslav Nevinny as Veselchak U (lit. Jolly Man U)
* Mikhail Kononov as Krys (lit. Tom-rat)
* Georgi Burkov as Alik Borisovich (doctor) 
* Yevgeni Gerasimov as Robot Werther
* Valentina Talyzina as Mariya Pavlovna
* Natalya Varley as Marta Erastovna
* Yelena Metyolkina as Polina
* Vladimir Nosik as old man Pavel
* Yuri Grigoryev as Professor Seleznyov
* Mariya Sternikova as Shurochka
* Andrei Gradov as Ishutin Inna Churkina as schoolgirl

== Company & Channels ==
 
{| class="wikitable"
|-
! Country
! Studio
|-
|  
| First Program
|-
|  
| BBC One
|-
|  
| TF1
|-
|  
| Rai Tre
|-
|  
| Fuji TV
|-
|  
| Seoul Broadcasting System
|-
|  
| Pogo TV, ETV Bengali, Saam TV, Kushi TV
|}

==Soundtrack==
;Wonderful Far-away (Prekrasnoe dalyoko)
The song was performed by the Grand Children chorus of Central Television and Gosteleradio USSR as the theme song for the film. The composer Yevgeny Krylatov, words Yuri Entin ( ). The songs popularity soon separated from the miniseries, and it became iconic in its own right; it was widely performed by various choirs and sung in everyday life.

==Cultural influence== Natalya Guseva, who had got a lot of letters from fans. This phenomenon was called as "Alicemania" (Алисомания) and had a large scale.
* Popular in early 2000s Russian dance group Гости из Будущего (Gosti iz Budushchego; Guests from the Future) got their name from the film title.

The game S.T.A.L.K.E.R. has a secret room that is a reference to this movie.
It is a room that has the same time-travel machine in it. It is not reachable for the player and can only be seen from outside the room.

==External links==
* 
* 
* 
*  
 
 
 
 
 
 
 
 
 
 
 
 
 