The Beast of Borneo
{{Infobox film
| name           = The Beast of Borneo
| image          = Poster of the movie The Beast of Borneo.jpg
| image_size     =
| caption        =
| director       = Harry Garson
| producer       = Harry Garson (producer)
| writer         = Alfred Hustwick (screenplay) Alfred Hustwick (story) Frank J. Murray (story)
| narrator       =
| starring       = See below
| music          =
| cinematography = Lewis W. Physioc
| editing        = William Faris
| distributor    =
| released       = 1934
| runtime        = 63 minutes
| country        = USA
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
 Universal Studios 1931 East of Borneo. A couple of added dialogue scenes were spliced into what was essentially a travelogue and a series of close-ups of an enraged orangutan.

==Plot==
A noted big game hunter, Bob Ward (John Preston), is visited in the jungles of Borneo by Russian scientist Boris Borodoff (Eugene Sigaloff) and his lovely assistant Alma Thorne (Mae Stuart), who want to prove the evolutionary link between man and beast. Ward at first declines to lead the scientists to a tribe of orangutans, but Almas charms finally convince him. Along with Wards pet orangutan, Borneo Joe, they track the apes and actually manage to capture a male orangutan, whom Dr. Borodoff anaesthetizes with a shot of whiskey. Borodoff, it soon appears, is quite insane -- and Bob, in an effort to calm him down, is knocked unconscious and dragged into the jungle by the tormented orangutan. He is rescued by Alma and Borneo Joe, but the trio can only watch as the enraged simian kills the evil Dr. Borodoff.

==Cast== John Preston as Bob Ward
*Mae Stuart as Alma Thorne
*Eugene Sigaloff as Dr. Boris Borodoff
*Val Durand as Darmo
*Doris Brook as Nahnda
*Alexander Schoenberg as Controller Derrick van de Mark John Peters as Mr. Kruger

==Soundtrack==
 

==External links==
* 
* 

 
 
 
 
 
 


 