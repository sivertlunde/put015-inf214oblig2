Judaai (1997 film)
{{Infobox film
| name           = Judaai
| image          = Judaai 1997.jpg
| caption        = DVD Cover
| director       = Raj Kanwar
| producer       = Surinder Kapoor Jainendra Jain
| narrator       =
| starring       = Anil Kapoor Sridevi Urmila Matondkar
| music          = Nadeem-Shravan Harmeet Singh
| editing        = Waman Bhonsle
| distributor    = S.K. Film Enterprises Eros Entertainment
| released       = 1 February 1997
| runtime        =
| country        = India
| language       = Hindi
| budget         =   
| gross          =   
}}
 1997 Bollywood Telugu film Shubhalagnam. The film itself was remade into a television series, Main Lakshmi Tere Aangan Ki.

==Synopsis==
When Kajal (Sridevi) marries Raaj (Anil Kapoor), an Engineer by profession, she and her dad (Kader Khan), automatically assume that he is rich, wealthy, and corrupt. But that is not the case. Raaj does not have an air-conditioner, a car, not even a refrigerator. This infuriates Kajal, and in her mind she creates a separate world, where she is rich, wealthy, has several cars, and lives lavishly in a big bungalow. Even the birth of two children (a young son and a little daughter), and several years of marriage, does not change Kajal. An example of this occurs when Kajal meets her old friend Nisha (Poonam Dhillon) who Kajal feeds a pack of lies such that her husband is a business magnate, her family own several cars, live in a big bungalow etc. Nisha who is more wealthy than Kajal then offers to give her a lift home, only for Kajal to have to lie about the directions to her home according to the story she has fabricated. Kajal then realises her mistake when the bungalow she points out as her own turns out to be Nishas. Nisha explains to Kajal that no matter how much and how little you have, your own house is your own home and that fabricating such stories only adds insult to her husband, family and home. When Janhvi (Urmila Matondkar) Raajs bosss (Saeed Jaffrey) niece comes from overseas into their lives, she falls for the honest and humble natured Raaj and asks him to marry her; despite knowing that he is already married with children. After meeting the greedy Kajal, Janhvi offers two crore rupees in exchange for marriage to Raaj. And Kajal, seeing this as her opportunity to gain wealth and an easy life, accepts, not knowing that she is making a big mistake and this decision will change her life forever.
Kajal forces Raaj into the agreement & gets a reluctant Raaj & Janhvi married and divorces Raaj herself, to comply with the Hindu Marriage Act; thinking that she and Janhvi will happily live together and share Raaj.

She then uses the money she got in the bargain to buy a huge mansion & cars. Ironically, the bungalow she buys ends up being the bungalow that she pointed out as her own in front of her old friend Nisha. Nisha briefly returns to her old home to collect a picture of her husband left behind. Kajal then proceeds to taunt her that she has fulfilled her dreams whereas Nishas dreams are now shattered having lost everything. Nisha reminds Kajal that she sold her house and possessions to pay for the treatment of her ill husband, whereas Kajal has sold her husband for riches; and that to some extent, Kajal is still all the more poorer for that. Kajal moulds her self like the page three socialites & this leads to Kajal neglecting her family. Raaj initially feels rejected by Kajal and does not get close to Janhvi, feeling objectified at the thought of being sold and bought between his two wives. Eventually, the children & Raj find companionship with Janhvi who showers them with love and affection. The children call Janhvi Maa; something that Kajal would previously reprimand her children for calling her; insisting that they call her Mummy as this sounded high society.

Soon, Kajal realises how far she has drifted from her family. She forgets her wedding anniversary and throws a party for her daughters birthday not realising that Raaj has never been attracted to a lavish lifestyle and thus further pushing him away from her and towards Janhvi. After frequent reminders from her mother Kajal tries to make amends. When nothing works, Kajal shocks her husband when she slaps Janhvi and accuses her of stealing her husband and tries to make Janhvi leave. Kajal offers Janhvi all of her money back in exchange for Raaj again, only to be told by Raaj that she is the poorest relation despite having all the riches she ever dreamed of. On advice of her friend, Kajal seeks legal advice which also states that the only way she can legally marry Raaj is to get Janhvi and Raaj to divorce - an impossible task. She then forcibly tries to throw Janhvi out of the house, but to no avail as Raaj decides to leaves with Janhvi claiming that in the present circumstances, this is the fairest thing to do. The kids decide to stay with Janhvi and their father - heartbroken as they learn that their father was sold, prompting the son to ask his mother Kajal about the prospect of them being sold to another family at an agreed price.

Kajal on the other hand, disraught by her family abandoning her, donates all her riches, in order to pay her dues. When she learns that Raaj & the kids are leaving for the US, she runs to the airport for one final visit. She finds them, ready to depart. But in a last minute twist in the plot, Janhvi turns to Raaj and tells them that she is leaving for the US alone, but she is not literally alone. She is expecting Raajs child. Janhvi accepts that although Kajal sold her husband, but it was her who offered to buy him and thus she too should pay her dues. So, the movie ends with Kajal getting her family back, learning of the importance of family over money and Janhvi leaving for New York.

== Cast ==
* Anil Kapoor .... Raaj Varma
* Sridevi .... Kajal Varma
* Urmila Matondkar .... Janhvi Sahni
* Paresh Rawal .... Hasmukhlal (Landlord)
* Saeed Jaffrey .... Mr. Sahani (Jahnvis uncle)
* Farida Jalal .... Kajals mom
* Omkar Kapoor .... Romi (Kajal and Raajs son)
* Alisha Baig .... Preiti (Kajal and Raajs daughter)
* Johnny Lever .... Harry (Kajals brother)
* Upasna Singh .... Vaani Hasmukhlals Daughter and Harrys Wife
* Kader Khan .... Kajals father
* Poonam Dhillon ....Special Appearance (Kajals friend Nisha)
* Dinesh Hingoo .... as Doctor (Special appearance)
* Mehmood Jr. .... as Champak

== Box office ==
The film had low opening but picked up initially due to strong word of mouth. It then did well worldwide. In its full run it grossed Rs. 67 Crores   and was declared a "Semi Hit". It was also the Eleventh highest grossing film of the year 1997.

== Soundtrack ==
{| class="wikitable"
|-
! Track #
! Song
! Singer(s)
|-
| 1
| "Mujhe Pyaar Hua" Abhijeet
|-
| 2
| "Judaai Judaai"
| Hariharan (singer)|Hariharan, Alka Yagnik & Jaspinder Narula
|-
| 3
| "Main Tujhse Aise Milun"
| Alka Yagnik & Abhijeet
|-
| 4
| "Meri Zindagi Ek Pyaas"
| Jaspinder Narula & Shankar Mahadevan
|-
| 5
| "Pyaar Pyaar Karte Karte"
| Alka Yagnik, Abhijeet & Sapna Mukherjee
|-
| 6
| "Raat Ko Neend Aati Nahin"
| Amit Kumar & Priya Mayekar
|-
| 7
| "Judaai Judaai"
| Hariharan, Alka Yagnik & Jaspinder Narula
|-
| 8
| "Shaadi Karke Fass Gaya"
| Bali Bramhabhatt, Alka Yagnik & Shankar Mahadevan
|}

== Awards ==
===1998 Filmfare Awards===
* Nominated Filmfare Best Actress Award - Sridevi
* Nominated Filmfare Best Supporting Actress Award - Urmila Matondkar
* Won- Best Scene
Nominated filmfare best comedian award - Johnny lever

===1998 Screen Awards===
* Nomination Best Actress - Sridevi
* Nomination Best Supporting Actress - Urmila Matondkar

===1998 Zee Cine Awards===
* Nomination Best Actress - Sridevi
* Nomination Best Supporting Actress - Urmila Matondkar

== References ==
 

==External links==
*  


 
 
 
 
 
 