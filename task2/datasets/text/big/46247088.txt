Dark Remains
{{Infobox film
| name           = Dark Remains
| image          =
| alt            =
| caption        =
| director       = Brian Avenet-Bradley
| producer       = Laurence Avenet-Bradley
| writer         = Brian Avenet-Bradley
| starring       = {{plainlist|
* Cherri Christian
* Greg Thompson
* Scott Hodges
}}
| music          = Benedikt Brydern
| cinematography = Laurence Avenet-Bradley
| editing        = Brian Avenet-Bradley
| studio         = Avenet Images
| distributor    =
| released       =  
| runtime        = 88 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Dark Remains 2005 American horror film written and directed by Brian Avenet-Bradley.  It stars Greg Thompson, Cheri Christian, and Scott Hodges.  A couple believes that their dead daughter may be attempting to contact them.

== Plot ==
After their daughter dies, Julie and Allen move to a rural location.  When Julie begins taking photographs of the area, she sees images of her dead daughter in them, and she begins to believe that her daughter may be attempting to communicate with them.

== Cast ==
* Greg Thompson as Allen Pyke
* Cheri Christian as Julie
* Scott Hodges as Jim Payne
* Jeff Evans as Sheriff Frank Hodges
* Rachel Jordan as Emma Pyke
* Michelle Kegley as Mrs. Payne
* Rachael Rollings as Rachel Roberts
* Karla Droege as Marianne Shore

== Release ==
Dark Remains was released on home video on December 26, 2006. 

== Reception ==
Dennis Harvey of Variety (magazine)|Variety called it "a genuinely creepy ghost story that packs maximum dread per reel".   Bloody Disgusting rated it 4/5 stars and wrote, "All in all, unbelievable. Dark Remains is a suspense film the like of which hasn’t been released in a long time."   Joshua Siebalt of Dread Central rated it 3.5/5 stars and wrote, "All in all, Dark Remains is a very solid ghost story with enough attention to detail and characterization to set it aside from its contemporaries."   Mitchell Wells of Horror Society wrote, "If you are a horror fan, I would really encourage you to watch it and see how the director is greatly influenced by old ghost films as well as new age ones."   Mike Long of DVD Talk rated it 2.5/5 stars and wrote that though the films "contains some creepy moments", the film was often boring, confusing, or both. 
 
It was awarded best picture at the 2005 Rhode Island International Horror Film Festival. 

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 