Ponga Pandit
{{Infobox film
| name           = Ponga Pandit
| image          = 
| image_size     = 
| caption        = 
| director       = Prayag Raj
| producer       = Surinder Kapoor
| writer         = 
| narrator       = 
| starring       = Randhir Kapoor Neeta Mehta Danny Denzongpa
| music          = Laxmikant Pyarelal
| cinematography = Sharad Kadwe
| editing        = Kamlakar
| distributor    = 
| released       = 1975
| runtime        = 
| country        = India
| language       = Hindi 
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
Ponga Pandit is 1975 Hindi movie produced by Surinder Kapoor and directed by Prayag Raj. The film stars Randhir Kapoor, Neeta Mehta, Danny Denzongpa, Nirupa Roy, Madan Puri, Shreeram Lagoo and Ranjeet. The music is by Laxmikant Pyarelal.

==Plot==
Haridwar-based Neelkanth Pandey and Shambhu Nath get their children, Bhagwantiprasad and Parvati respectively, married. While Shambhu re-locates to live in Bombay, Neelkanth continues to reside in the same residence. Years later both children have grown up, so Neelkanth invites the Naths to finalize the nuptials. Very soon the Pandeys will face humiliation and ridicule at the hands of Parvati, who now calls herself Pamela, and Shambhu, who wants his daughter to marry someone sophisticated and wealthy, while Parvati is in love with a singer named Rocky. Bhagwanti is determined to fulfill his parents wishes and decides to stay wed to Parvati, re-locates to Bombay, to try and woo her back - with results that will end up changing his life forever

==Cast==
* Randhir Kapoor as Bhagwati Prasad Neelkanth Pandey/Prem
* Neeta Mehta as Parvati Pamela S. Nath
* Danny Denzongpa as Rockey
* Prema Narayan as Lalita
* Nirupa Roy as Janki S. Nath
* Madan Puri as Shambhu Nath
* Shreeram Lagoo as Professor
* Ranjeet as Master
* Pinchoo Kapoor as Neelkanth Prasad Pandey
==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Tere Milne Se Pehle"
| Lata Mangeshkar
|-
| 2 
| "Ganga Ghat Ka Pani Piya Hai"
| Kishore Kumar
|-
| 3
| "Woh Mere Peechhe Padi Huyi Hai"
| Kishore Kumar
|-
| 4
| "Jijaji Meri Didi Hai Anadi"
| Kishore Kumar, Asha Bhosle, Usha Mangeshkar
|-
| 5
| "Main Jab Chhedoonga Dil Ka"
| Kishore Kumar
|-
| 6
| "Yeh Birha Ki Aag Aesi" 
| Manna Dey
|}

== External links ==
*  

 
 
 
 

 