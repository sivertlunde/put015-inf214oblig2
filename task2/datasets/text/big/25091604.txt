Crazy Mama
 
{{Infobox film
| name           = Crazy Mama
| image          = Crazy Mama.jpg 
| image size     = 
| border         = 
| alt            = 
| caption        = Theatrical release poster
| director       = Jonathan Demme
| producer       = Julie Corman
| writer         =  Robert Thom
| story          = Frances Doel
| starring       = Cloris Leachman
| music          = Snotty Scotty and The Hankies
| cinematography = Bruce Logan
| editing        = Allan Holzman Lewis Teague
| studio         = 
| distributor    = New World Pictures
| released       = June 1975
| runtime        = 83 mins.
| country        = United States
| language       = English
| budget         = $2.3 million 
| gross          =
}}

Crazy Mama is a 1975 American Action film|action/comedy film directed by Jonathan Demme and produced by Julie Corman. The film stars Cloris Leachman. Dennis Quaid and Bill Paxton are also seen briefly in two of their earliest roles.

==Plot== Long Beach, California beauty parlor run by Melba Stokes (Leachman), her mother Sheba (Ann Sothern) and daughter Cheryl (Linda Purl), is repossessed. They flee when landlord Mr. Albertson comes to demand the back rent.
 Las Vegas next. In pursuit of pregnant Cheryl is her boyfriend, Shawn, while Melba gets reacquainted with an old lover, Jim Bob. Further battles with the law along the way eventually lead to a shootout in which Jim Bob and others are killed. Melba is left alone, on the lam, but begins life again in a new town with a new look.

==Cast==
* Cloris Leachman as Melba
* Stuart Whitman as Jim Bob
* Ann Sothern as Sheba
* Linda Purl as Cheryl
* Jim Backus as Albertson
* Donny Most as Shawn
* Tisha Sterling as Young Sheba
* Sally Kirkland as Ella Mae
* Harry Northup as FBI Man
* The Goodyear Blimp as Himself

==Production==
The film is a follow up to   gave birth to her first child during production. Christopher T Koetting, Mind Warp!: The Fantastic True Story of Roger Cormans New World Pictures, Hemlock Books. 2009 p 86-87 

==Soundtrack==
*"Transfusion" - Performed by Nervous Norvus
*"Sleepwalk" - Written by Johnny Farina, Santo Farina & Ann Farina, Performed by Santo & Johnny
*"Money (Thats What I Want)" - Written by Berry Gordy and Janie Bradford, Performed by Barrett Strong
*"Devoted To You" - Written by Boudleaux Bryant, Performed by The Everly Brothers
*"Black Slacks" - Written by Joe Bennett and Jimmy Denton, Performed by Joe Bennett and The Sparkletones
*"Running Bear" - Written by The Big Bopper (as J.P. Richardson), Performed by Johnny Preston
*"All I Have To Do Is Dream" - Written by Boudleaux Bryant, Performed by The Everly Brothers
*"Ive Had It" - Performed by The Bell Notes
*"Western Movies" - Written by Cliff Goldsmith and Fred Smith, Performed by The Olympics
*"Lollipop" - Written by Beverly Ross and Julius Dixon, Performed by The Chordettes

==Home media== The Lady In Red as part of the Roger Corman Cult Classics collection. 

==See also==
*List of American films of 1975

==Notes==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 