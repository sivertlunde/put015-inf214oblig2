The Dinner (2013 film)
 
{{Infobox film
| name           = The Dinner
| image          = 
| caption        = 
| director       = Menno Meyjes
| producer       = 
| writer         = Menno Meyjes Herman Koch (novel)
| starring       = Thekla Reuten
| music          = 
| cinematography = Sander Snoep
| editing        = 
| distributor    = 
| released       =  
| runtime        = 88 minutes
| country        = Netherlands
| language       = Dutch
| budget         = 
}}

The Dinner ( ) is a 2013 Dutch drama film written and directed by Menno Meyjes. It was screened in the Contemporary World Cinema section at the 2013 Toronto International Film Festival.      

The film is based on Herman Kochs novel Het diner.

==Cast==
* Thekla Reuten as Claire
* Kim van Kooten as Babette
* Daan Schuurmans as Serge
* Jacob Derwig as Paul
* Sabine Soetanto
* Reinout Bussemaker as Rector
* Gusta Geleynse as Dakloze
* Wil van der Meer as Tonio
* Jonas Smulders as Michel
* Harpert Michielsen as Gerant
* Andre Dongelmans as Beau
* Serge Mensink as Rick

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 