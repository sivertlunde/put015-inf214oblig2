Restless Natives
 
{{Infobox film
| name = Restless Natives
| image = Restless_natives_poster.jpg
| caption = Film Poster Robert Urquhart Bernard Hill Mel Smith
| writer = Ninian Dunnett Michael Hoffman 
| producer = Rick Stevenson  Andy Paterson Mark Bentley
| music = Stuart Adamson
| cinematography = Oliver Stapleton
| studio = EMI Films
| distributor = Thorn EMI Screen Entertainment  Orion Classics
| released = June 1985 (UK) September 12, 1986 (USA)
| runtime = 90 minutes
| country = Scotland
| language = English
| budget = £1.2 million Alexander Walker, Icons in the Fire: The Rise and Fall of Practically Everyone in the British Film Industry 1984-2000, Orion Books, 2005 p35 
| gross = £300,000 
}} Michael Hoffman Joe Mullaney, and Ned Beatty.
 highwaymen become local folk heroes as well as a tourist attraction in themselves.

The soundtrack features music by Big Country.  This music was not released on an album but was combined into two lengthy tracks, each featuring various pieces of music and clips of actors from the films audio, which appeared on limited edition formats of two Big Country 12" singles.  It was released on CD for the first time on the 1998 Big Country collection Restless Natives & Rarities, where it is presented as a single 35-minute track.

The film performed well at the box office in Scotland but struggled to make an impact elsewhere. 

==See also==
* Restless Nation

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 


 
 