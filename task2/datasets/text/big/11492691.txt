Rumors (film)
{{Infobox film
| name           = Rumours
| image          = Private SNAFU.JPG
| image_size     =
| caption        =
| director       = Friz Freleng
| producer       = Leon Schlesinger (producer) Dr. Seuss (supervising producer)
| writer         = Dr. Seuss   Phil Eastman Frank Graham   Tedd Pierce   Michael Maltese 
| animator       =  Jack Bradbury   Ken Champin   Gerry Chiniquy   Manuel Perez
| music          = Carl W. Stalling
| cinematography =
| editing        = Treg Brown
| distributor    =
| released       = December 13, 1943
| runtime        = 3 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

Rumors is part of the Private Snafu series of animated shorts produced by Warner Bros. during World War II. Released in 1943, the cartoon was directed by Friz Freleng.

==Plot==
  
Private Snafu and his buddies begin talking about a recent bombing, but the story grows more exaggerated with each passing turn. Eventually, a panic breaks out on his base that a bombing is imminent. In the end, nothing happens, but the base is quarantined and Snafu is locked up.

==References==
* Friedwald, Will and Jerry Beck. "The Warner Brothers Cartoons." Scarecrow Press Inc., Metuchen, N.J., 1981. ISBN 0-8108-1396-3.

==External links==
* 

 
 
 


 