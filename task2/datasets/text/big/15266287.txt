Blast of Silence
{{Infobox Film
| name           = Blast of Silence
| image          = Blastofsilence.jpg
| image size     =
| caption        =
| director       = Allen Baron
| producer       = Merrill Brody
| writer         = Allen Baron
| narrator       = Lionel Stander Larry Tucker Peter Clume
| music          = Meyer Kupferman
| cinematography = Merrill Brody
| editing        = Peggy Lawson
| distributor    = Universal Studios
| released       = 1961
| runtime        = 77 min.
| country        = United States
| language       = English
| budget         =
| gross          =
}} American crime thriller film released in 1961 in film|1961. It was written and directed by Allen Baron and produced by Merrill Brody who was also the cinematographer.

==Characters and story==
Frankie Bono (Allen Baron), a hitman from Cleveland, Ohio|Cleveland, comes to New York City during Christmas week to kill a middle-management mobster, Troiano (Peter H. Clune). First he follows his target to select the best possible location, and orders a gun from rat-loving dealer Big Ralph (Larry Tucker). One night, he meets a former friend from the orphanage he grew up in, rattling his cool and putting his life in danger.

==Production notes==
According to Turner Classic Movies web site, the "fist fight" scene was filmed on Long Island during Hurricane Donna (September 10–12, 1960), the only hurricane of the 20th century to blanket the entire East Coast from south Florida to Maine. 

==DVD release==
  photos, and images of locations as they existed in 2008. Also included is a booklet featuring an essay by film critic Terrence Rafferty and a four-page graphic novel by Sean Phillips (Criminal (comics)|Criminal, Sleeper (comics)|Sleeper, Marvel Zombies).

==References==
 

==External links==
* 
* 
* 
* 
*  at Trailers From Hell

 
 
 
 
 
 
 