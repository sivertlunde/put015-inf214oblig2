His Last Twelve Hours
 
{{Infobox film
| name           = His Last Twelve Hours
| image          = 
| caption        = 
| director       = Luigi Zampa
| producer       = Carlo Civallero
| writer         = Cesare Zavattini Suso Cecchi dAmico Vitaliano Brancati
| starring       = Jean Gabin
| music          = Nino Rota
| cinematography =  Carlo Montuori
| editing        = Eraldo Da Roma
| distributor    = 
| released       =  
| runtime        = 81 minutes
| country        = Italy 
| language       = Italian
| budget         = 
}}

His Last Twelve Hours ( ) is a 1951 Italian drama film directed by Luigi Zampa and starring Jean Gabin.   

==Cast==
* Jean Gabin - Carlo Bacchi
* Mariella Lotti - Margot, Bacchis wife
* Julien Carette - Amedeo Santini
* Elli Parvo - Lidia Guidi, Bacchis friend
* Antonella Lualdi - Maria
* Antonietta Pietrosi - Anna, Carlo bacchis daughter
* Paola Borboni - Luisa
* Carlo Sposito - The Duke Sorino (as Carletto Sposito)
* Marga Cella - Mrs. Gigliosi
* Fausto Guerzoni - A worker
* Piero Pastore - A working trade unionist
* Dante Maggio - The cobbler
* Enrico Luzi - Witness of the accident
* Dino Raffaelli - Giuseppe
* Bella Starace Sainati - Luisa, Bacchis aunt
* Bice Valori - Witness of the accident

==References==
 

==External links==
* 
 
 
 
 
 
 
 
 
 
 
 
 
 