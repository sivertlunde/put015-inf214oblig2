Theatre of Death
{{Infobox film
| name           = Theatre of Death
| image          = "Theatre_of_Death".jpg
| caption        = 
| director       = Samuel Gallu
| producer       = E. M. Smedley-Aston (producer) William J. Gell (executive producer)
| writer         = Ellis Kadison Roger Marshall
| based on       = 
| starring       = Christopher Lee Julian Glover Lelia Goldoni
| music          = Elisabeth Lutyens
| editing        = Barrie Vince
| studio         = Pennea Productions Ltd.
| distributor    = London Independent Producers (UK) Hemisphere Films (USA)
| released       = 1967
| runtime        = 91 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
 1967 British horror movie.  It stars Christopher Lee as a theatre director whose Grand Guignol theatre is thought to be linked to a series of murders. John Hamilton, The British Independent Horror Film 1951-70 Hemlock Books 2013 p 167-170 

==Cast==
*Philippe Darvas -	Christopher Lee
*Dani Gireaux -	Lelia Goldoni
*Charles Marquis -	Julian Glover
*Inspector George Micheaud -	Ivor Dean
*Karl Schiller -	Joseph Furst
*Andre, Patron of Cafe -	Steve Plytas
*Colette -	Betty Woolfe
*Joseph -	Leslie Handford
*Patrons Wife -	Miki Iveria
*Pierre -	Fraser Kerr
*Heidi -	Dilys Watling
*Voodoo Dancer -	Lita Scott
*Madame Angelique -	Evelyn Laye
*Nicole Chapelle -	Jenny Till
*Ferdi -	Terence Soall Esther Anderson
*Jean, Stage Manager -	Peter Cleall
*Girl on Scooter -	Suzanne Owens
*Belly Dancer -	Julie Mendez
*Voodoo Dancer -	Evrol Puckerin
*Voodoo Drummers -	The Tony Scott Drummers

==References==
 
==External links==
*  

 
 


 