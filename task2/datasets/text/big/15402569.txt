Kiss the Bride (2007 film)
{{Infobox film
| name           = Kiss the Bride
| image          = KissTheBride-750px.jpg
| caption        = Kiss the Bride film poster
| director       = C. Jay Cox
| producer       = C. Jay Cox Richard Santilena Bob Schuck
| writer         = Ty Lieberman
| starring       = Tori Spelling Philipp Karner James OShea
| music          = Ben Holbrook
| cinematography = Carl Bartels
| editing        =
| distributor    = Regent Releasing
| released       =  
| runtime        = 100 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $32,033 
}}
Kiss the Bride is a 2007 romantic comedy film directed by C. Jay Cox, which had a limited release in April 2008. It stars Tori Spelling, Philipp Karner and James OShea.

==Plot== James OShea) were best friends in high school, but ten years later, Matt receives an invitation to Ryans wedding, he is surprised - especially that Ryans intended, whose name is Alex, is a woman (Tori Spelling). Matt and Ryan had a gay relationship in high school, and Matt has held a torch for Ryan for the past ten years. Described by a co-worker as "so My Best Friends Gay Wedding," Matt races off to rescue his former love from this woman who must have trapped him into marriage. Matt and Alex hit it off, Matt and Ryan have some things to work out, and there is a cast of character in-laws (Joanna Cassidy, Tess Harper, Robert Foxworth and Amber Benson).

==Cast==
*Tori Spelling as Alex James OShea as Ryan
*Philipp Karner as Matt
*Amber Benson as Elly
*Joanna Cassidy as Evelyn
*Garrett M. Brown as Gerald
*Tess Harper as Barbara
*Robert Foxworth as Wayne
*E.E. Bell as Dan
*Steve Sandvoss as Chris
*Michael Medico as Sean
*Jane Cho as Stephanie
*Ralph Cole Jr. as Barry
*Brooke Dillman as Virginia
*Dean McDermott as Plumber
*Elizabeth Kell as Monica
*Dean Nolen as Reverend
*Connie Sawyer as Aunt Minnie Les Williams as Larry
*Charlie David as Joey
*Paul Meacham as Waiter Worthie Kyle Davis as Officer Harley
*Mary Gillis as Saleslady

==Critical reception==
Nick Pinkerton of LA Weekly called the film "the most ignoble outing in bi-curious screen hijinks since France produced Poltergay." 

==Soundtrack==
*"U Found Me" (Levi Kreis, Darci Monet) - Levi Kreis
*"Hardly A Hero" (Kreis, Monet) - Levi Kreis
*"Were Okay" (Kreis) - Levi Kreis (end credits)
*"Drive" - Brian Kent 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 
 
 
 