Love Me and the World Is Mine
{{Infobox film
| name           = Love Me and the World Is Mine
| image          =
| caption        =
| director       = Ewald André Dupont
| producer       = Carl Laemmle
| writer         = Rudolph Hans Bartsch (novel Die Geschichte von der Hannerl und ihren Liebhabem) Imre Fazekas Albert DeMond Walter Anthony Paul Kohner
| narrator       =
| starring       = Mary Philbin Norman Kerry Betty Compson
| music          = Sidney Jones
| cinematography = Jackson Rose
| editing        = Edward L. Cahn Daniel Mandell
| distributor    = Universal Pictures
| released       =  
| runtime        = 60 minutes
| country        = United States Silent English English intertitles
| budget         =
}}

Love Me and the World Is Mine (1928 in film|1928) is an American romantic film directed by Ewald André Dupont and released by Universal Pictures. This film is unknown to survive and might be a lost film.

==Plot==
Hannerl (Philbin) is a young woman growing up in Old Vienna. She falls in love with two men: A young army officer who can provide her love and security and an old wealthy man who can provide her a high-class life. She doesnt know who she wants to spend her life with, but must make her decision.

==Cast==
* Mary Philbin - Hannerl
* Norman Kerry - Von Vigilatti
* Betty Compson - Mitzel
* Henry B. Walthall - Van Denbosch
* Mathilde Brundage - Mrs. Van Denbosch
* Charles Sellon - Mr. Thule
* Martha Mattox - Mrs. Thule
* George Siegmann - Porter

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 


 
 