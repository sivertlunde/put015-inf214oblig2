Dressed to Kill (1928 film)
{{infobox film
| name           = Dressed to Kill
| image          =
| imagesize      =
| caption        =
| director       = Irving Cummings  William Fox
| writer         = Malcolm Stuart Boylan
| starring       = Mary Astor
| music          =
| cinematography = Conrad Wells
| editing        =  Frank E. Hull
| distributor    = Fox Film Corporation
| released       = March 18, 1928
| runtime        = 70 minutes
| country        = United States
| language       = Silent (English intertitles) 
}}

Dressed to Kill is a 1928 silent film drama produced and distributed by Fox Film Corporation and starring Mary Astor and Edmund Lowe. Astor was "on loan" from Warner Bros. for the film. The New York Times review stated - "Edmund Lowe is capital as the well-tailored Barry. Mr. Barry likes a good round of golf on the day following a fruitful burglary. Mary Astor is charming as Jean and R. O. Pennell makes the most of the "Professors" rôle.".
 Samuel L. Roxy Theatre.

==Plot==
The gang of a mob boss get suspicious his new girlfriend. Because shes a beautiful young girl they dont believe she would actually associate with the mob and wonder if shes really a police "plant". The mobsters dress nattily to not appear "out of place" in the ritzy neighborhoods prior to a heist.

==Cast==

* Edmund Lowe - Mile-Away Barry
* Mary Astor - Jeanne
* Ben Bard  - Nick
* Bob Perry - Ritzy Logan
* Joe Brown - as himself  
* Tom Dugan - Silky Levine
* John Kelly - Biff Simpson
* Robert Emmett OConnor - Detective Gilroy
* R. O. Pennell - Professor
* Ed Brady - singing waiter   Charles Morton - Jeannes sweetheart
* Harry Dunkinson - uncredited
* Florence Wix - dressmaker (uncredited)

==References==
* 

==External links==
*  

 
 
 
 
 
 
 
 
 
 


 
 