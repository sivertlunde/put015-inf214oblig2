Enter Madame (1935 film)
{{Infobox film
| name           = Enter Madame
| image          = Enter-madame-1935.jpg
| image_size     =
| caption        = movie poster
| director       = Elliott Nugent
| producer       =
| writer         = Gilda Varesi Archibald (play Enter Madame) Charles Brackett (screenplay)
| narrator       =
| starring       = Cary Grant Elissa Landi
| music          =
| cinematography = Theodor Sparkuhl
| editing        =
| distributor    = Paramount Pictures
| released       =  
| runtime        = 83 minutes
| country        = United States
| language       = English
| budget         =
}} American romantic comedy film directed by Elliott Nugent, starring Elissa Landi and Cary Grant, and released by Paramount Pictures.
 Garrick Theatre in New York City for a total of 350 performances. The stage version was directed by Brock Pemberton . The 1935 movie was a remake of a 1922 silent film starring Clara Kimball Young and  Louise Dresser.

==Film cast==
*Elissa Landi	... 	Lisa Della Robbia
*Cary Grant	... 	Gerald Fitzgerald
*Lynne Overman	... 	Mr. Farnum
*Sharon Lynn	... 	Flora Preston (*as Sharon Lynne)
*Michelette Burani	... 	Bice
*Paul Porcasi	... 	Archimede
*Adrian Rosley	... 	Doctor
*Cecilia Parker	... 	Aline Chalmers
*Frank Albertson	... 	John Fitzgerald
*Wilfred Hari	... 	Tamamoto
*Torben Meyer	... 	Carlson
*Harold Berquist	... 	Bjorgenson
*Diana Lewis	... 	Operator
*Richard Bonelli	... 	Scarpia in La Tosca
*Ann Sheridan	... 	Floras Shipboard Friend (as Clara Lou Sheridan)

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 


 