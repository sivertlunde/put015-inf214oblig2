Max Havelaar (film)
 
{{Infobox film
| name           = Max Havelaar of de koffieveilingen der Nederlandsche handelsmaatschappij
| image          = 
| caption        = 
| director       = Fons Rademakers
| producer       = Fons Rademakers
| writer         = Gerard Soeteman, from the novel by Multatuli Peter Faber, Sacha Bulthuis, Rutger Hauer, Krijn ter Braak, Adendu Soesilaningrat
| music          =   
| cinematography = Jan de Bont
| editing        = Pieter Bergema
| distributor    = 
| released       =  
| runtime        = 163 minutes
| country        = Netherlands  Dutch and Indonesian
| budget         = 
}} Best Foreign Language Film at the 49th Academy Awards, but was not accepted as a nominee. 

==Cast== Peter Faber as Max Havelaar
* Sacha Bulthuis as Tine
* Adendu Soesilaningrat as Regent (as E.M. Adenan Soesilaningrat)
* Maruli Sitompul as Demang
* Krijn ter Braak as Verbrugge
* Carl van der Plas as Resident
* Rima Melati as Mevrouw Slotering
* Rutger Hauer as Duclari
* Joop Admiraal as Slotering
* Frans Vorstman as Gouverneur-generaal
* Piet Burnama as Djaska (as Pitradjaja Burnama)
* Herry Lantho as Saïdjah
* Leo Beyers as Droogstoppel
* Nenny Zulaini as Adinda

==See also==
* List of submissions to the 49th Academy Awards for Best Foreign Language Film
* List of Dutch submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 

 
 
 
 
 
 
 