100 Days (2013 film)
{{Infobox film
| name           = 100 Days
| image          = 100 Days 2013 film poster.png
| alt            = 
| caption        = Film poster
| director       = Henry Chan
| producer       = {{Plainlist |
* Stacy Fan
* Weiko Lin 
}}
| writer         = Weiko Lin 
| screenplay     = Megi Hsu
| starring       = {{Plainlist |
* Johnny Lu
* Tracy Chou 
* Soda Voyu
* Akira Chen 
* Julianne Chu
* Ming-Hsui Tsai 
}}
| music          =  {{Plainlist | 
* Mira Lin
* Christopher Wong
}}
| cinematography = Randy Che
| editor = Kenji Chen
| studio         = Unison Company
| distributor    = Vie Vision Pictures
| released       =   
| runtime        = 104 minutes
| country        = Taiwan
| language       = Mandarin
}}
100 Days (  directed by Henry Chan, marking his second film since Gas (2004 film)|Gas.

== Plot ==

The main character is career-obsessed Bo Dan Wu (played by Johnny Lu), a rising star at a telecom company in Taipei. When he learns the news of his estranged mother’s death, he reluctantly returns to his hometown (set in Matsu’s picturesque Qinbi Village (芹壁村) to pay his respects. When he arrives, he learns of a tradition which requires him to either marry within 100 days or wait for three years.

There is just one catch: Bo Dan does not plan on getting married any time soon. Fortunately, his step-brother Zhen Fong (played by Soda Voyu), decides to marry his long-time fiancé Xiao Wei (played by Tracy Chou) in three days. It turns out that Xiao Wei is actually Bo Dan’s childhood sweetheart, and when a typhoon prevents him from leaving the island, the two are forced with the possibility of rekindling their romance. Xiao Wei is forced to choose: her doting fiancé who is the “right” choice, or the man of her dreams.

== Cast ==

*Akira Chen (陳文彬) as Mo Shu
*Tracy Chou (周采詩) as Xiao Wei
*Julianne Chu (朱蕾安)
*Johnny Lu (路斯明) as Bo Dan Wu 
*Tsai Ming Hsiu (蔡明修 ) as Liu Ching
*Soda Voyu (蘇達) as Zhen Fong

== Production ==
 Beigan Township  (北竿) island because the village looked like “it hasn’t been touched by time.”
“I wanted the village to be a character. If I came back to discover my own village and fell in love with it, it had to be attractive and beautiful,” Chan says.
But Matsu was an expensive choice. Lacking the infrastructure necessary for filmmaking, the crew had to ship everything to the island, including 14 trucks, cranes, generators and extras. And similar to the protagonist in the movie, a typhoon stranded the production team on the island a few times. 
Additional scenes were shot in Taipei, Taiwan. 

== Release ==
 33rd Hawaii International Film Festival on October 13, 2013 with English subtitles.   Following this, the film saw a nationwide release in Taiwan on November 1, 2013. 

== References ==

 

== External links ==

*  
*  

 
 
 
 