The Mexican
 Mexican}}
 
{{Infobox film
| name           = The Mexican
| image          = Themexicanposter.jpg
| caption        = Theatrical release poster
| director       = Gore Verbinski
| producer       = Christopher Ball John Baldecchi Lawrence Bender
| writer         = J.H. Wyman
| starring       = Brad Pitt Julia Roberts James Gandolfini J. K. Simmons
| music          = Alan Silvestri
| cinematography = Darius Wolski Craig Wood DreamWorks Pictures
| released       =  
| runtime        = 123 minutes
| country        = United States
| language       = English
| budget         = $57 million
| gross          = $147,845,033  (total)  
}} thriller and road movie.

The script was originally intended to be filmed as an independent production without major motion picture stars, but Roberts and Pitt, who had for some time been looking for a project they could do together, learned about it and decided to make it. The movie was then advertised as a typical romantic comedy star vehicle, somewhat misleadingly, as the script does not focus solely on the Pitt/Roberts relationship and the two share relatively little screen time together. Ultimately, the film earned $66.8 million at the U.S. box office. 

==Plot==
The story follows Jerry Welbach (Brad Pitt) as he travels through Mexico to find a valuable antique gun, The Mexican, and smuggle it into the United States. Five years earlier, Welbach had caused a traffic accident in which he hit the car of local mobster Arnold Margolese (Gene Hackman), who was jailed for five years after the police searched his car following the crash, finding someone tied up in his trunk. In compensation for the jail time, Welbach has been sent on various errands by Margoleses second-in-command, Bernie Nayman (Bob Balaban). Retrieving the gun will be his final errand. Welbach has a girlfriend, Samantha (Julia Roberts), whom he argues with constantly and who leaves Jerry prior to the trip over his lack of commitment to their relationship.

Jerry arrives in Mexico and makes his way to pick up Beck (David Krumholtz), the Margolese employee now in possession of the gun.  There, a drunk Beck tells Jerry about the guns history as a suicide weapon used as part of a jilted love-triangle between a woman, a nobleman, and the son of the gunsmith who forged the weapon, as well as its curse to misfire.  Jerry helps Beck to his car, only for the man to be killed by celebratory gunfire from a nearby festival.

Panicked but determined, Jerry buries the body and then calls Bernie to report on the situation, only for his vehicle to be stolen while he makes the call, the gun still inside.  Jerry briefly has a non-compliant donkey as transportation to follow the thief, then buys an old, damaged truck. Meanwhile, Samantha gets kidnapped by a hit man named Leroy who tells her that Jerry hired him to make sure she is safe from anybody that wants her dead. It is then revealed that Leroy is gay, which doesnt bother Sam after they pick up Postal worker named Frank, whom then has a relationship with Leroy.

Another hitman follows them to Las Vegas and kills Frank to make it look like a suicide. Leroy kills him in an act of vengeance, and flees with Sam. Jerry finds out that Ted wanted to kill him and take the pistol to Margolese, but hesitated and ends up getting handcuffed to a pawn shop owner. Jerry then finds out he accidentally grabbed Teds Passport and tries to retrieve it, but Ted has already fled. Jerry decides to pick up Sam and Leroy at the airport, where Leroy recognizes him, but Jerry doesnt recognize Leroy. After their car has a blowout, Jerry kills Leroy and finds out that he is one of Margolese men as Winston Baldry, and that the real Leroy was the man Winston killed. Shocked by this, Sam decides to go home, but gets kidnapped by Bernie in order to get the pistol from Margolese. Sam gets the upper hand and kills Bernie with the pistol he desperately needed. Jerry and Sam rekindle their romance and get married in Mexico, while Margolese finally gets his prize. The film ends with Jerry and Sam arguing over the story of The Mexican.


Cast
* Brad Pitt as Jerry Welvach
* Julia Roberts as Samantha 
* James Gandolfini as WInston Baldry (Leroy)
* J.K. Simmons as Ted Slocum
* Bob Balaban as Bernie Nayman
* Sherman Augustus as Well Dressed Black Man (the actual Leroy)
* Michael Cerveris as Frank
* David Krumholtz as Beck
* Gene Hackman as Arnold Margolese
* Castulo Guerra as Joe

==Production== Las Vegas, Nevada and Los Angeles, California.

==Box office==
The film opened at #1 at the North American box office making $20,108,829 USD in its opening weekend, although the film had a 39% decline in earnings the following week, it was enough to keep the film at the top spot for another week.

==Reception==
The film holds a 56% rating on Rotten Tomatoes. The critical consensus states that "Though The Mexican makes a good attempt at originality, its ponderous length makes it wear out its welcome. Also, those looking forward to seeing Roberts and Pitt paired up may end up disappointed, as they are kept apart for most of the movie." 

"The scenes between Roberts and Gandolfini make the movie special. ... Their dialogue scenes are the best reason to see the movie." 

"Pitt and Roberts are good too – maybe better like this than if they were together. ... If it had been a Pitt/Roberts two-hander, there wouldnt have been room for Gandolfinis wonderful character, and that would have been a shame." 

 The Mexican is sporadically entertaining. It works when Gandolfini is on screen; when he leaves, he takes the movie with him. ... From here, director Gore Verbinski, intercuts between two road movies, one of which (the one with Pitt) is downright boring" 

"Roberts and Pitt are generally terrific. In The Mexican they are horrid. ... Gandolfini is a star on the rise. His work in The Mexican is solid. Frankly, hes the only bright spot in this dark and pointless movie." 

"Moviegoers who have seen The Mexican arent coming out of cinemas talking about the romantic chemistry between Brad Pitt and Julia Roberts. Theyre talking about the presence of tough guy James Gandolfini in the unlikely role of a gay hit man named Leroy." 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 