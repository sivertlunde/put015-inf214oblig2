Bewitched (1945 film)
{{Infobox film
| name           = Bewitched
| image          = Bewitched poster.jpg
| image_size     =
| alt            =
| caption        = Theatrical release poster
| director       = Arch Oboler Jerry Bresler
| writer         = Arch Oboler
| screenplay     =
| based on       =  
| narrator       =
| starring       = Phyllis Thaxter Edmund Gwenn
| music          = Bronislau Kaper
| cinematography = Charles Salerno Jr.
| editing        = Harry Komer
| studio         =
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 65 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Bewitched is a 1945 American film noir directed and written by Arch Oboler.  The drama features Phyllis Thaxter and  Edmund Gwenn. 

==Plot==
On the eve of her engagement, a demure young woman becomes the victim of a split personality and murders her fiance.

==Cast==
* Phyllis Thaxter as Joan Alris Ellis
* Edmund Gwenn as Dr. Bergson
* Henry H. Daniels Jr. as Bob Arnold
* Addison Richards as John Ellis
* Kathleen Lockhart as Mrs. Ellis
* Francis Pierlot as Dr. George Wilton
* Sharon McManus as Small Girl
* Gladys Blake as Glenda Will Wright as Mr. Herkheimer
* Stephen McNally as Eric Russell (billed as "Horace McNally")
* Oscar OShea as Capt. OMalley
* Minor Watson as Governor
* Virginia Brissac as Martha—Governors Wife

==Critical reception==
When the film was first released, the staff at Variety (magazine)|Variety magazine liked the film, and wrote, "Produced on a low budget, with a sterling cast of actors actors, this picture just oozes with class because of the excellent adaptation and direction it has been given by radios Arch Oboler, author of the story, "Alter Ego", on which the film is based. Climax follows climax, strong performance follows strong performance in this thrilling psychopathic study of a girl obsessed by an inner voice that drives her to murder." 

==References==
 

==External links==
 
*  
*  
*  
*  
*   of Phyllis Thaxter (illustrates her split personality)
*  

 
 
 
 
 
 
 
 
 