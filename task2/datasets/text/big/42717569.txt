Miley Cyrus: Tongue Tied
{{Infobox film
| name = Miley Cyrus: Tongue Tied
| image =  
| alt = A black-and-white title card reading "Miley by Quentin Jones".
| caption = 
| director = Quentin Jones
| producer = {{plainlist|
* Michael Angelos
* Neil Cooper
* Nina Qayyum
}}
| starring = Miley Cyrus
| music = {{flatlist|
* 30s
* Zoee
}}
| distributor = Nowness
| released =  
| runtime = 2 minutes
| country = United States
| language = English
}}

Miley Cyrus: Tongue Tied (alternatively titled Miley) is an American   and the erotic novel Fifty Shades of Grey (2011), and agreed that it continued to solidify the sexually-explicit public image she had cultivated in 2013.

==Background and production==
{{Quote box
| quote  = "Stripping the color from our heroine’s cartoonish persona, Jones goes in on popular symbols of sex: the fishnets; the beauty mark made darker like Marilyn’s; the black collar fit for a Bunny. To Miley, as to a little kid, even the most dangerous object is a toy. And like a kid’s, her near-nudity is hardly erotic. She’s just happier naked. Her poses are jokes, even that infamous tongue—stuck out—remains firmly in cheek. She holds up the cardboard cut-out and winks. Get it? She’s playing with herself."
| source = — Sarah Nicole Prickett of Adult describing the concept of Miley Cyrus: Tongue Tied. 
| width  = 25%
| align  = left
}}

Miley Cyrus: Tongue Tied was first premiered during Cyrus headlining Bangerz Tour in February 2014; it was used as an introductory interlude for her performances, and was musically accompanied by the track "Fitzpleasure" by the English band Alt-J.  It was later re-released on May 1, 2014, during which time Cyrus was recovering from an allergic reaction that consequently delayed the tour.   

Miley Cyrus: Tongue Tied was created by the production and representation agency Cadence, headquartered in New York City. Neil Cooper and Nina Qayyum were enlisted as its executive producers, while Michael Angelos is recognized as a third producer. In addition to directing the project, Quentin Jones was responsible for creating its animations. Peter Duffy and Johnny Langer collaborated for the music and sound design.  Diane Martel was secured as the creative director of the project;  she had previously worked with Cyrus for her music video for the track "We Cant Stop" and her controversial performance at the 2013 MTV Video Music Awards.  Paul Laufner and Sandra Winther were respectively employed as the director of photography and artwork assistant. Simone Harouche is acknowledged as a stylist; Cyrus hair was done by Andy Lecompte, while her makeup was done by Pati Dubrogg. 

==Synopsis==
 

A two-minute short film, Miley Cyrus: Tongue Tied features the track "Stockholm Syndrome" by 30s featuring Zoee. Heavily inspired by electronic music, its lyrics "hold me down so its hard to breathe" contribute to the overtly sexual nature of the video.  The film is alternatively named Miley in its title card; the black-and-white film itself is contained within a square, and is bordered by excess white space on either side. 

Cyrus is dressed in black latex underwear throughout the clip, with electrical tape covering her nipples,    and is occasionally seen in fishnets.  The video frequently alternates between footage of Cyrus making facial expressions with cartoon-styled eyes and noses layered on her face and Cyrus wearing latex bands commonly associated with sadomasochism (S&M). She is additionally seen suspended mid-air by ribbons in an acrobatic fashion, hanging from a rod with her legs provocatively spread, and suggestively showing her buttocks to the camera. As the clip progresses, Cyrus cries as she spreads black oil across her face, which she later smears across her body in a sexual manner. 

==Critical reception== Idolator opined that the sadomasochistic inspiration "  things up"; he elaborated that the video was "stylish and beautifully shot" and restored Cyrus position as "the queen of shock" from recording artist Rihanna, who had generated media attention for a topless magazine spread in Lui. 

While stating that the project was "sexy", Oscar Lopez admitted that its concept was unoriginal considering its similarities to the visuals for "  thought that the project "was done in the name of art", whereas Cyrus earlier endeavors come across as being provocative "in a blatant bid to boost record sales".  After classifying the clip as not safe for work, Lauren Valenti from Marie Claire acknowledged it as the "latest viral reminder that Mileys Disney days are far behind her.  Ryan Reed from Rolling Stone called the video "jaw-dropping" and felt that it was an extension of the "psychedelic side" that Cyrus had recently displayed through on-stage collaborations with Wayne Coyne and Steven Drozd of The Flaming Lips. 

Amiya Moretta opined that the clip "boldly confronts double standards that exist between socially acceptable expressions of sexuality" and wondered "if this   the spawn of a sexual revolution." She described the video as "incredibly sexualized and aesthetically captivating, as the black and white combinations of fluttering eyelashes, slathered black paint and eye masks appeal to the limited attention spans of today’s generation. The mixed-media music video is short and surreal, using layers of photography, illustration, and animation to create a story." 

==Credits and personnel==
Credits adapted from Miley Cyrus: Tongue Tied.   
 
 
* Michael Angelos&nbsp;– producer
* Neil Cooper&nbsp;– executive producer
* Pati Dubrogg&nbsp;– makeup
* Peter Duffy&nbsp;– music and sound design
* Simone Harouche&nbsp;– stylist
* Quentin Jones&nbsp;– director, animation
* Johnny Langer&nbsp;– music and sound design
* Paul Laufer&nbsp;– director of photography
* Andy Lecompte&nbsp;– hair
* Diane Martel&nbsp;– creative director
* Nina Qayyum&nbsp;– executive producer
* Sandra Winther&nbsp;– artwork assistant
 

==References==
 

==External links==
*  
*  , full-length (3:14) audio version

 

 
 
 