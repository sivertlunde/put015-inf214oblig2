Ek Bura Aadmi
{{Infobox film
| name= Ek Bura Aadmi 
| image=
| image_size = 
| alt=
| caption=
| director=Ishraq Shah
| producer=Surendra Rajiv Zenaida Mastura
| story=Ishraq Shah
| based on= true incidents
| screenplay= Yashwant Shekhawat, Risho Oberoi and Ishraq Shah
| starring=Arunoday Singh Kitu Gidwani Raghuveer Yadav Yashpal Sharma Angira Dharw Deepak Singh
| music= Band of Bandagi
| cinematography= 
| editing= Meghna Ashchit
| studio=
| distributor=Philind Motion Picture
| released= unreleased
| runtime= 105 minutes
| country=India
| language=Hindi
}}

Ek Bura Aadmi {{cite web|title=EK BURA AADMI on location shoot, Releasing 26 July 2013, ★ Arunoday Singh, Kitu Gidwani  2013 
Hindi Political, Thriller film directed by Ishraq Shah and produced by Surendra Rajiv and Zenaida Mastura. This is a work of fiction inspired by real life stories of well-known criminals turned politicians from the North Indian states of UP and Bihar. The film is a realistic portrayal of power politics where money and muscle play very prominent roles.The film is to be released on July 26, 2013.
The film features Arunoday Singh, Kitu Gidwani, Raghuveer Yadav and Yashpal Sharma as main characters.

==Cast==

*Arunoday Singh as Munna Siddiqui
*Kitu Gidwani as Rukmi Devi 
*Raghubir Yadav as Prabhunath Pandey
*Yashpal Sharma as Rajnath Chowdhary
*Deepak Singh as Sajid Siddiqi 
*Angira Dhar
*Vishal O Sharma
*Vinod Nahardih

==Plot==
The film revolves around the power politics of the Hindi heartland of UP and Bihar.

==Soundtrack==
The music as well as the background score has been composed by Tochi Raina and his Band of Bandagi. Lyrics have been penned by Nasir Faraaz, Arafat Mehmood & Ishraq. The film features six songs:

#Suraj Sehra Mein Akela Hai (Romantic Fusion)
#Yeh Tamasha Kya Hai (Sufi / Jazz / Rock Fusion)
#Uchal Gayi Chamiya (Item Number / Nautanki)
#Gaon Ki Radha Ka Rukh (Folk - Holi)
#Uth Ja Tham Le Tiranga (Patriotic / Call to Action)
The audio was released by Times Music (Junglee) in Delhi on June 14 and has been made available on all the digital platforms.

==References==
 

==External links==
*  Full All Songs Lyrics, Movie Information & Reviews.
*  at Official Facebook Page
*  at IMDB
*http://articles.timesofindia.indiatimes.com/2013-06-19/news-interviews/40069157_1_ek-bura-aadmi-arunoday-singh-music-launch
*http://www.indianexpress.com/news/ek-bura-aadmi-music-launched/1134099/
*  on Times Music

 
 
 
 
 
 
 


 