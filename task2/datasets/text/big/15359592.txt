Open Season 2
{{Infobox film
| name = Open Season 2
| image = Open Season 2 poster.jpg
| caption = Film poster
| director = Matthew OCallaghan Todd Wilderman
| producer = Kirk Bodyfelt Matthew OCallaghan Executive producers: Michelle Murdocca Andrea Miloro
| writer = David I. Stern Steve Moore
| starring = Joel McHale Mike Epps Cody Cameron Jane Krakowski Billy Connolly Crispin Glover Danny Mann Steve Schirripa Fred Stoller Olivia Hack
| music = Ramin Djawadi
| editing = Steven Liu Jimmy Sandoval
| studio = Sony Pictures Animation Reel FX Creative Studios
| distributor = Sony Pictures Home Entertainment
| released =  
| runtime = 76 minutes
| gross = $8,716,950
| country = United States
| language = English
}} Open Season, produced by Sony Pictures Animation. It was directed by Matthew OCallaghan, co-directed by Todd Wilderman, and produced by Kirk Bodyfelt and Matthew OCallaghan.

Most of the supporting cast reprised their voice roles, however Martin Lawrence, Ashton Kutcher, and Patrick Warburton do not reprise their roles as Boog, Elliot, and Ian from the first film; instead, Mike Epps, Joel McHale, and Matthew W. Taylor voice Boog, Elliot, and Ian respectively.

==Plot==
The film takes place one year after the events of the first film.

Elliot (Joel McHale) has grown giant new antlers and is getting married to Giselle (Jane Krakowski). But during a mishap, Elliots new antlers are cracked off and now look like they did in the first film, which upsets him. Luckily, Boog (Mike Epps) and the others manage to cheer Elliot up by having a rabbit fight. But Elliot has new emotions about his marriage and feels reluctant to marry Giselle. Mr. Weenie (Cody Cameron) finds a dog biscuit trail that his previous owners left behind and uncontrollably follows it. At the climax of the wedding, Elliot witnesses Mr. Weenie being taken away by his old owners, Bob and Bobbie (Georgia Engel). Elliot tells the story to the other forest animals (a little overreacting) and decides to make a rescue mission to save him. The other ones that go on Elliots rescue mission are Boog, Giselle, McSquizzy (Billy Connolly), Buddy, (Matthew W. Taylor), and Serge (Danny Mann) and Deni (Matthew W. Taylor).

Meanwhile, the other pets meet. There is Fifi (Crispin Glover), a toy poodle and his basset hound companion Roberto (Steve Schirripa), two cats named Stanley (Fred Stoller), whose companion is a mentally retarded cat named Roger (Sean Mullen), and a Southern dog named Rufus (Diedrich Bader), whose companion is his girlfriend Charlene (Olivia Hack). Fifi discusses his hatred for wild animals as one night he goes into the bushes to retrieve his chew toy and is shocked by the wild animals inside, and accidentally gets his tail touched by the bug lighter. He then tries to maul a nearby rabbit, until stopped by his owner. Meanwhile, the wilds find Weenie, much to Elliots dismay, who does not want to marry Giselle. They try to free him while his owners are in a gas station. They free him from his chains, but accidentally leave him stuck on the RV along with Buddy. Elliot and Giselle get in a feud, and eventually leaves Elliot to search for Mr. Weenie himself, while Serge and Deni fly to look for him.

The owners reach the pet camp with Mr. Weenie and Buddy, unbeknownst to them. The other pets meet with Weenie, and Fifi tries to change Weenie back into a pet, but fails. Buddy helps Weenie escape and Buddy tries to free Weenie from his shock collar. During the chase, Fifi gets shocked by the collar and gets his forehead burned, which causes him to lose most of his sanity. Meanwhile, Serge and Deni return and explain they found Weenie and Buddy at a pet camp, which they now escaped from. Boog and the others set camp at a human camp, and Boog tries to convince Giselle that Elliot is a good person and they are good together, but fails. Elliot, meanwhile, is having a horrible time, following his own tracks that he confuses for Mr. Weenies and gets his head stuck on a trash can lid with gum on it, but misses his friends and becomes a mess. The wilds reach the pet camp, but the pets and their owners have already left, but realizes that they have gone to Pet Paradiso, a vacation spot for pets. Elliot finds Weenie and Buddy and is convinced to go to Pet Paradiso to save his friends.

The wilds reach Pet Paradiso and try to sneak in by disguising themselves as pets, with Giselle as a dalmatian and McSquizzy as a chihuahua. Boog attempts to sneak in as a cat, but gets the idea to be a sheepdog. Elliot also disguises himself as Boogs female human owner. Giselle and McSquizzy walk around Pet Paradiso looking for Mr. Weenie, but their cover is blown and are kidnapped by Fifi and the other pets. Elliot, Boog, Buddy and Mr. Weenie attempt to go inside to save Giselle and McSquizzy, but are captured by Fifi as well. Fifi tries to kill them with a pile of shock collars. As Boog tries to stop Mr. Weenie from going down a waterslide, his cover is blown as well and the security try to tranquilize him after he causes a rampage after people confuse his weight. Before Fifi shocks the wilds into submission, Elliot tries to profess his love for Giselle, but fails. As Fifi is about to fry the wilds, Boog, is was chasing Weenie down a waterslide, enters the pets lair, and the water that was rushed from the waterslide forces everyone out of the lair.

A battle between the wilds and the pets ensues, with the Pet Paradiso security focused on tranquilizing the wilds. Elliot saves Giselle and accidentally places all the shock collars on himself. He also wrestles Fifi in the pool for the shock collar remote. Fifi eventually grabs the remote and activates all the shock collars, but does not realize that Elliot put all the shock collars on him. Fifi survives, but the explosion caused him to lose his hair and become bald. The pets and the wilds settle their differences and decide to become friends. Mr. Weenie decides to join the pets and returns to his owners in rejoice. Elliot finally professes his true feelings for Giselle, and they get married.

==Production==
Sony announced a sequel to Open Season in September 2007. Although, the original grossed $85 million and $105 million outside the United States, Sony felt the film performed much better on DVD, thus, making a direct-to-DVD sequel. Sony Pictures Digital president said that "the studio will keep Open Season 2s costs low by utilizing Sony Pictures Imageworks|Imageworks satellite facilities in India and New Mexico". 

==Cast==
*Joel McHale as Elliot. He was supposed to marry Giselle, but abandoned her due to new emotions. In the end, he and Giselle end up getting married anyway after he finally manages to profess his love for Giselle.
*Mike Epps as Boog. He also leads the rescue mission to save Mr. Weenie.
*Cody Cameron as Mr. Weenie. He is a German dachshund. He was captured by Fifi and the pets, leaving Boog, Elliot, and the other wilds to go rescue him.
*Jane Krakowski as Giselle. She is Elliots fiance who is scared of getting married. However, in the end, she and Elliot end up getting married after Elliot finally professes his love for Gisele.
*Billy Connolly as McSquizzy. He joins in the mission to save Weenie.
*Crispin Glover as Fifi. He is an ill-tempered toy poodle who is often annoyed by his companion, Roberto. Fifi has a deep hatred over wild animals and slowly begins to lose his sanity after his forehead gets burned during a chase.
*Danny Mann as Serge. He joins in the ue mission to save Weenie with Deni.
*Matthew W. Taylor as Deni/Buddy/Ian, one of the main protagonist. They join in the mission to save Mr. Weenie.
*Steve Schirripa as Roberto. He is Fifis dim-witted companion who usually annoys him. In the end, he is reformed and becomes friends with the wilds.
*Fred Stoller as Stanley. He is a pet cat who is constantly annoyed by Roger. In the end, he is reformed and becomes friends with the wilds.
*Sean Mullen as Roger. He is a mentally retarded cat who often annoys his best friend Stanley. In the end, he is reformed and becomes friends with the wilds.
*Diedrich Bader as Rufus. He is a Southern dog who is Charlenes boyfriend. In the end, he is reformed and becomes friends with the wilds.
*Olivia Hack as Charlene, a Western dog who is Rufus girlfriend. In the end, she is reformed and becomes friends with the wilds.

==Release==
Although Open Season 2 was as direct-to-video in the United States and the United Kingdom, it was released theatrically in some countries. In South Africa on its opening day, the film grossed $84,244 from 26 screens with an $2,081 average. In Russia, it opened with $2,835,600 from 360 screens with a $7,877 average. In Poland, it opened with $194,339 from 75 screens with an $2,591 average. In total, the film grossed $8,716,950.   

Theatrical releases: 
*South Africa - September 24, 2008
*Russia - October 16, 2008
*Croatia - November 6, 2008
*United Arab Emirates - November 13, 2008
*Czech Republic - December 18, 2008
*Slovakia - December 18, 2008
*Lebanon - December 18, 2008
*Turkey - January 16, 2009
*Poland - January 23, 2009
*Iceland - January 30, 2009
*South Korea - March 12, 2009

==Reception==
Overall, Open Season 2 received mixed reviews from critics. Critic at DVD Verdict claimed, "Open Season 2 is no classic (neither was the original), but its a competent check-your-brain-at-the-door comedy for children of all ages. The animation and storytelling may not stack up against Pixars (whose does?), but the flick offers something that Pixar movies generally dont: old cartoon slapstick and sight gags in the mold of Bugs Bunny".   

==Home release== PSP UMD on January 27, 2009. 

==Sequel==
 

The sequel Open Season 3 premiered in theaters in Russia on October 21, 2010 and was released on DVD and Blu-ray in United States on January 25, 2011. 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 