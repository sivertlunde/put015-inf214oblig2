Aadharsam
{{Infobox film 
| name           = Aadharsam
| image          =
| caption        =
| director       = Joshiy
| producer       = MD George
| writer         = Pappanamkodu Lakshmanan
| screenplay     = Menaka
| Shyam
| cinematography =
| editing        =
| studio         = Amala Arts
| distributor    = Amala Arts
| released       =  
| country        = India Malayalam
}}
 1982 Cinema Indian Malayalam Malayalam film, Menaka in lead roles. The film had musical score by Shyam (composer)|Shyam.   

==Cast==
*Prem Nazir
*Jayabharathi
*Srividya Menaka
*Sukumaran
*MG Soman

==Soundtrack== Shyam and lyrics was written by Bichu Thirumala. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Jeevan pathanju pongum || K. J. Yesudas, S Janaki, Chorus || Bichu Thirumala || 
|-
| 2 || Kannu pothalle || S Janaki, Chorus || Bichu Thirumala || 
|-
| 3 || Laharikal nurayumee || S Janaki || Bichu Thirumala || 
|-
| 4 || Swapnangal than chithayil || K. J. Yesudas || Bichu Thirumala || 
|}

==References==
 

==External links==
*  

 
 
 

 