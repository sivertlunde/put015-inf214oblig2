The Canadian (film)
{{Infobox film
| name           = The Canadian
| image          =
| imagesize      =
| caption        =
| director       = William Beaudine
| producer       = Adolph Zukor Jesse Lasky
| based on       =   Arthur Stringer (adaptation, scenario) Julian Johnson(intertitles)
| starring       = Thomas Meighan
| music          =
| cinematography = Alvin Wyckoff
| editing        =
| distributor    = Paramount Pictures
| released       =   reels (7,753 feet)
| country        = US
| language       = Silent (English intertitles)
}}
The Canadian is an extant 1926 silent film drama produced by Famous Players-Lasky and distributed by Paramount Pictures. It is based on a 1913 Broadway play, The Land of Promise, by W. Somerset Maugham. The film was directed by William Beaudine and starred Thomas Meighan. Meighan had costarred with Billie Burke in a 1917 silent film based on the same story, The Land of Promise. In both films he plays the same part. This film is preserved in the Library of Congress.   

==Cast==
*Thomas Meighan - Frank Taylor
*Mona Palma - Nora
*Wyndham Standing - Ed Marsh Dale Fuller - Gertie
*Charles Winninger - Pop Tyson
*Billy Butts - Buck Golder

==References==
 

==External links==
* 
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 


 
 