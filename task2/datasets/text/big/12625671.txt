Lady for a Night
 
{{Infobox film
| name           = Lady for a Night
| image          = Lady for a Night FilmPoster.jpeg
| caption        = Film poster
| director       = Leigh Jason
| producer       = Albert J. Cohen
| writer         = Garrett Fort
| starring       = John Wayne Joan Blondell
| music          = 
| cinematography = Norbert Brodine
| editing        = Ernest J. Nims
| distributor    = 
| released       =  
| runtime        = 87 minutes
| country        = United States 
| language       = English
| budget         = 
}}
 Memphis Belle is named after a steamboat in this film. It is also known as The Lady From New Orleans, Memphis Belle and Lady of New Orleans.

==Plot==
Social climber Jenny Blake owns of the casino steam boat Memphis Belle, together with the influential Jack Morgan. Most of the customers are from the upper layers of the citys social life, but they have little respect for Jenny and her - in their opinion - vulgar occupation. 
Jack is secretly in love with Jenny. To show her what it really is she aspires for he arranges for her to be queen of the high society ball at the Mardi Gras festivities. Her crowning angers many of the established members of society and she is mocked in public. However, she doesnt give up her dream.

She decides to use one of the old plantation owners, Alan Alderson, as a  leverage. Alan is burdened with debt and manages to lose his plantation, "The Shadows" when gambling at the casino. Jenny makes Alan an offer, to strike his debts at the casino if he agrees to marry her.

Alan sees no other alternative than to agree to the proposition, and Jenny is secured a respectable position in society. They marry in a hurry, and Jack is informed of the bond soon after.

Jack is devastated by Jennys marriage to Alan, and doesnt try to save the  Memphis Belle when it catches fire. Everyone in Alans family has a hard time accepting Jenny, except his aunt Katherine, who is suffering from mental illness.

Jack goes on to sabotage the relationship between Jenny and Alan. He gets help from Alans aunt Julia, who goes to lengths to ruin the marriage. Julia starts off with insinuations that Jenny has an improper relation with Jack, and goes on to ruining a ball Jenny is hosting.

Jack saves Jenny and the ball by using his political influence to make the guests attend even though Julia has tried to keep them away. Jenny is almost killed when Julia goes on to let her ride in a carriage pulled by a blind horse.

Jenny retaliates by ordering Julia to leave the plantation. The infuriated Julia then mixes a poisoned drink meant for Jenny, but Alan beats her to it and dies from drinking it. Jenny is accused of murdering her husband, and there is a criminal trial. Aunt Katherine is the only person who knows that Julia mixed the drink, but she is forced by her sister to testify against Jenny in court.

Jenny is convicted of murder, but Katherine soon confesses that it was Julia who mixed the drink, and a few years earlier also killed Katherines fiancée in a fit of jealousy. Jenny is released and cleared of all charges. She meets Jack and they start working together again. She finally gives up her dream to climb the social ladder and accepts Jacks proposal to marry him. 

==Cast==
* Joan Blondell as Jenny "Jen" Blake Alderson
* John Wayne as Jackson Morgan
* Ray Middleton as Alan Alderson
* Philip Merivale as Stephen Alderson
* Blanche Yurka as Julia Anderson
* Edith Barrett as Katherine Alderson
* Leonid Kinskey as Boris, Jacks Bodyguard
* Hattie Noel as Chloe, Jennys Maid
* Montagu Love as Judge
* Carmel Myers as Mrs. Dickson, the Mayors Wife
* Dorothy Burgess as Flo
* Guy Usher as Governor
* Ivan Miller as Mayor Dickson
* Patricia Knox as Mabel
* Lew Payton as Napoleon, Aldersons Servant

==See also==
* John Wayne filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 