Zangiku monogatari (1956 film)
{{Infobox Film
| name           = Zangiku monogatari
| image          = Zangiku monogatari 1956 poster.jpg
| caption        = Japanese movie poster featuring Chikage Awashima (on the left) and Kazuo Hasegawa (on the right)
| director       = Koji Shima
| producer       = Masaichi Nagata
| studio = Daiei Motion Picture Company
| writer         =
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| distributor    = Daiei Motion Picture Company
| released       =   
| runtime        = 112 min.
| country        = Japan
| awards         = 
| language       = Japanese
| budget         = 
| preceded_by    = 
| followed_by    = 
| 
}}
  is a 1956 black and white Japanese film directed by Koji Shima.

== Cast ==
* Kazuo Hasegawa
* Chikage Awashima
* Tamao Nakamura
* Mitsuko Yoshikawa

== See also ==
* The Story of the Last Chrysanthemum (残菊物語 Zangiku monogatari) (1939) by Kenji Mizoguchi

== References ==
 

== External links ==
 
*   http://search.varietyjapan.com/moviedb/cinema_24799.html
*   http://www.allcinema.net/prog/show_c.php?num_c=137212

 

 
 
 
 
 
 


 

 