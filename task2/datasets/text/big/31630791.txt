He's on Duty
{{Infobox film
| name           = Hes on Duty
| image          = Banga banga.jpg
| director       = Yook Sang-hyo
| producer       = Kim Bok-geun   Lee Se-young
| writer         = Yook Sang-hyo
| starring       = Kim In-kwon   Kim Jung-tae
| music          = Shin Hyung
| cinematography = Jeon Dae-sung
| editing        = Park Gok-ji
| distributor    = 
| released       =  
| runtime        = 110 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          = 
}}
 2010 South Korean film that comically yet incisively depicts racial issues in Korea. Tae-sik finds it difficult to get a job due to his odd appearance and impatient character. After failing repeatedly, he disguises himself as a foreigner and finally lands a job. Tae-sik, however, witnesses the cruel treatment migrant workers face in Korea.    

The Korean title is a pun on the lead characters name and is an abbreviated form of 반갑습니다 (bangabseumnida) or 반가워요 (bangawoyo) which translates to "Delighted? Delighted!" or "Nice to meet you."  

==Plot==
Bang Tae-sik is perennially unemployed as he drifts from one job to another, from manual labor to serving coffee. His appearance (dark skin and short height), being rather atypical for a Korean is to blame it seems, but best buddy Yong-cheol persuades Tae-sik to make better use of these disadvantages: Desperate and having nothing better to do, he adopts a strange accent and ethnic hat and is reborn as Bang-ga (a twist on his family name) from Bhutan, and immediately lands a job at a chair manufacturing factory.

Despite a shaky beginning ― due to his unredeemable clumsiness, rather than doubts about his alleged Bhutani roots that are all too convincing ― Tae-sik gets along with his co-workers, and even starts romancing the lovely Jang-mi from Vietnam. He is even voted to become president of a migrant workers labor union and competent Korean language instructor, and joins in a harmonious effort to win a local singing competition for foreigners.

Tae-sik begins to truly bond with his co-workers but his loyalties are put to the test when Yong-cheol finds a way to swindle their money.  

==Cast==
*Kim In-kwon - Bang Tae-sik / Bang-ga
*Kim Jung-tae - Yong-cheol 
*Shin Hyun-bin - Jang-mi 
*Khan Mohammad Asaduzzman - Ali 
*Nazarudin - Rajah 
*Peter Holman - Charlie 
*Eshonkulov Parviz - Michael 
*Jeon Gook-hwan - Boss Hwang 
*Kim Kang-hee - Miss Hong 
*Kim Bo-min - Hye-young 
*Park Yeong-soo - Park Kwan-sang
*Jung Tae-won - Dan-poong

==Awards and nominations==
2010 Korean Film Awards
*Nomination - Best Supporting Actor: Kim Jung-tae
*Nomination - Best New Actress: Shin Hyun-bin
*Nomination - Best Screenplay: Yook Sang-hyo
*Nomination - Best Music: Shin Hyung
 2011 Baeksang Arts Awards
* Best New Actress: Shin Hyun-bin
* Best Screenplay: Yook Sang-hyo

2011 Buil Film Awards
* Best Screenplay: Yook Sang-hyo

==References==
 

== External links ==
*    
*    
*   at Naver  
*  
*  
*  

 
 
 