Welcome Danger
{{Infobox film
| name           = Welcome Danger
| image          = File:Welcome Danger poster.jpg
| image_size     = 
| caption        = Film poster Malcolm St. Clair (uncredited)
| producer       =  Felix Adler Lex Neal Clyde Bruckman 
| starring       = Harold Lloyd Barbara Kent 
| music          = 
| cinematography = Henry O. Kohler Walter Lundin
| editing        = Bernard W. Burton Carl Himm
| studio         = Harold Lloyd Corporation
| distributor    = Paramount Pictures
| released       =  
| runtime        = 115 minutes (sound version) 10,796 feet (silent version)
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
Welcome Danger (1929) is a comedy film directed by Clyde Bruckman and starring Harold Lloyd in his first talkie. A sound version and silent version were filmed. Ted Wilde began work on the silent version, but became ill and was replaced by Bruckman. 

==Plot==
A student (Lloyd) helps the San Francisco police investigate a crime wave in that citys Chinatown district.

==Cast==
*Harold Lloyd
*Barbara Kent
*Noah Young
*Charles B. Middleton| Charles Middleton
*Will Walling
*Edgar Kennedy

==Preservation status==
Both the silent and sound versions have been restored by the UCLA Film and Television Archive.

==References==
 

==See also==
*Harold Lloyd filmography

==External links==
* 
* 


 
 

 
 
 
 
 
 
 
 

 