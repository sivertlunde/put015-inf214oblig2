Mad Bastards
 
{{Infobox film
| name           = Mad Bastards
| image          = 
| caption        = 
| director       = Brendan Fletcher
| producer       = Brendan Fletcher David Jowsey Alan Pigram Stephen Pigram
| writer         = Brendan Fletcher
| starring       = Dean Daley-Jones
| music          = 
| cinematography = Allan Collins
| editing        = Claire Fletcher
| distributor    = 
| released       =  
| runtime        = 94 minutes
| country        = Australia
| language       = English
| budget         = $3.2 million   accessed 24 May 2014  gross = $203,598 (Australia) 
}}

Mad Bastards is a 2011 Australian drama film written and directed by Brendan Fletcher.  It is Fletchers debut film and it premiered at the 2011 Sundance Film Festival.   

==Plot==
Years ago, TJ (Dean Daley-Jones) abandoned his wife and son, and as time passes his conscience tells him its time he began facing up to his responsibilities as a father. TJ is an Aboriginal man living in Western Australia and has a weakness for alcohol and a habit of getting into fights. As it happens, TJs son Bullet (Lucas Yeeda) is nearly as troubled as he is; at the age of thirteen, hes already been arrested for arson and instead of serving a sentence in a juvenile detention home, he is released to the custody of his Elders. Bullet isnt anxious to reacquaint himself with TJ, but both realize they need to settle their scores with one another, and Bullets Grandfather Texas (Greg Tait) steps in to help.

==Cast==
* Dean Daley-Jones as TJ
* Karla Hart as TJs sister
* Alex Lloyd as Musician
* Douglas Macale as Uncle Black
* Patrick McCoy-Geary as Bullets mate
* Kelton Pell as Mad Dog
* Alan Pigram as Musician
* Ngaire Pigram as Nella
* Stephen Pigram as Musician
* Greg Tait as Texas
* John Watson as Bush Camp Elder
* Lucas Yeeda as Bullet

==Reception==

Mad Bastards received positive reviews from critics and audiences, earning an approval rating of 88% on Rotten Tomatoes.
 SBS gave the film three stars out of five. She observed that the "over-reliance on score sets up an avoidant rhythm that begins to feel like a lack of narrative confidence." However she also points out that "Fletcher’s atmospheric approach is not without moments of emotional power, and the raw, unyielding landscapes of Northwestern Australia are framed to resonant effect." 

===Awards and nominations===

{| class="wikitable"
|-
! Award
! Category
! Subject
! Result
|- AFI Members Choice Award Brendan Fletcher, David Jowsey, Alan Pigram, Stephen Pigram
| 
|- AACTA Award  (1st AACTA Awards|1st)  AACTA Award Best Film
| 
|- AACTA Award Best Original Screenplay Brendan Fletcher
| 
|- AACTA Award Best Young Actor Lucas Yeeda
| 
|- AACTA Award Best Sound Phil Judd
| 
|- Nick Emond
| 
|- Johanna Emond
| 
|-
|}

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 