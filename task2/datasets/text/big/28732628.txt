Hanamizuki
{{Infobox film
| name           = Hanamizuki
| image          = 425px- Hanamizuki Poster.jpg
| caption        = Poster
| border         = yes
| director       = Nobuhiro Doi
| producer       = Yasuo Yagi Kazuya Hamana Jun Nasuda
| writer         = Noriko Yoshida
| starring       = Yui Aragaki Toma Ikuta
| music          = 
| cinematography = Yasushi Sasakibara
| editing        = 
| studio         =
| distributor    = Toho
| released       =  
| runtime        = 128 minutes
| country        = Japan
| language       = Japanese
| budget         = $5,500,000 
| gross          = $32,990,888   
}}
  is a 2010 Japanese romance drama film inspired by the lyrics of a love song of the same name by Yō Hitoto.    The film is directed by Nobuhiro Doi, and its script was written by Noriko Yoshida. The film spans the ten-year period from 1996 to 2006, and it stars Yui Aragaki as Sae, a high school student who later grows into a young adult.  Toma Ikuta also stars in this film, playing the role of Kouhei, Saes boyfriend and an aspiring fisherman. 

Sae and Kouhei met by accident on a train when they were both high school students, and a relationship starts to form. Later, when Sae needs to leave Hokkaido to go to Tokyo for studies, the two of them attempted to have a long distance relationship. However, their bond is tested by multiple challenges facing the two of them.
 30th Hawaii International Film Festival.  The film received a mixed reception from critics, and grossed over US Dollar|US$32,990,888, making it the 12th highest grossing Japanese film of 2010.   

==Plot==
This film spans the years of 1996 to 2006.

It begins in the year 2005 when Sae is traveling to her birthplace of Peggys Cove, Nova Scotia in Canada. On the bus, she looks at a photo, and the scene immediately goes back to 1996. Sae and Kouhei met on a train ride to their respective college entrance examinations. The train hit a deer, causing Sae to worry about being late for the examinations. They went to a nearby house to ask if the owners could give them a lift. Sae spotted a key in the truck parked at the porch. She asked Kouhei if he would "borrow" the truck to take her to the exam center. However, when Kouhei tried to overtake a slow cow truck, he narrowly avoided an incoming crane, and went off the road into a ditch. They were brought to a police station and Sae was disqualified from her examinations.

Sae worked hard to get into a university while Kouhei always supported her. However, he had mixed feelings about her going to Tokyo, because that would mean that they would be separated. Hence, when Sae managed to get into Waseda University, Kouhei at first refused to see her off. However, at his friends urging, they got onto a boat and chased after Sae, and when they saw her, they rolled out a banner reading, "Good Luck Sae!".

At Waseda University, Sae met Kitami Junichi, a senior who likes taking pictures of children in third world countries. He helps Sae find a night job teaching English at a cram school and became good friends with her. When Kouhei visited Sae in Tokyo, upon seeing Sae and Kitami talking together, Kouhei got jealous. During the dinner date with Sae, Kouhei refused to eat anything and stormed out of the restaurant. On his way, a group of delinquent youths knocked down a box that contained Kouheis present for Sae, and mocked him. A fight ensured, and Kouhei was injured. Sae brought Kouhei back to her apartment, where they made up. Kouhei then gives Sae the ship, which was similar to the one Kouhei was on when he saw off Sae. Sae worries that their relationship will not last longer.

Four years later, a graduating Sae is unable to find a job in Tokyo. She met Junichi, who asks her to go to New York City together with him. Kouhei was also told by his father that their fishing boat was about to be repossessed by the bank, and he must find another job. Kouhei then contacts Sae, telling her that he plans to go to Tokyo to find her. However, on the fishing boats last trip, Kouheis father had a heart attack and died. Kouhei is then unable to leave for Tokyo, as he had to take care of his mother and younger sister.

Later, Sae leaves for New York and meets up with Junichi, and they worked together in the same company. Junichi proposed to Sae later on. Sae returned to Kushiro to attend her friend Minamis wedding, and she found out that Kouhei was married to Ritsuko. However, Ritsuko was jealous of the way Sae and her husband were interacting. Kouhei then met Sae at the lighthouse, and Sae tells him that this might be the last time she visits Japan. When Kouhei returned, he found Ritsuko waiting for him on the steps with bad news- the bank might make them bankrupt. Kouhei manages to settle the problem, but he found Ritsukos divorce papers on the table when he returned. The scene ends with a news report stating that Junichi was killed in Iraq.

A year later, Sae visits her hometown. When she was walking, she chanced upon the ship that Kouhei had given her in a shop window and found out that Kouhei was part of a ships crew that had docked in port. She rushed to see Kouhei, but just missed him.

In 2006, Sae had moved back to her hometown, and set up a school for children in her house. The film ends when Sae sees Kouhei under a flowering dogwood tree, and Sae welcomes Kouhei back home.

After the credits there is a cutscene with a little girl, looking at that same tree Sae always was. Her father comes in behind her and lifts her up. If you look close enough, you can see the childs father is Kouhei. Putting the pieces together: Sae and Kouhei get married and have a daughter, they live in Saes childhood home.

==Cast==
* Yui Aragaki as Sae Hirasawa.  This will be Aragakis last high school character, though she will be playing the role of a career women later in the film.   This is a translated version of the original article by Sports Hoshi   She had also undergone special language training to cope with the English lines in this film.  Sae is a Hokkaido high school student who lives with her mother and dreams of attending a Tokyo university in the beginning of the film. She met Kouhei on a train ride, and slowly their relationship blossomed. Saes childhood was played by Runa Matsumoto.   
* Toma Ikuta as Kouhei Kiuchi,  the only son of a fisherman who dreams of following in his fathers footsteps one day. When he first saw Sae, he immediately fell in love with her. He was later married Ritsuko, but was divorced from her a little later due to a falling out.
* Osamu Mukai as Junichi Kitami,    a photographer who loves to go around the world taking pictures of children in war zones. He and Sae met at Waseda University, where Kitami was her senior. He proposed to Sae, but was killed by a terrorist attack in Iraq before their wedding.
* Hiroko Yakushimaru as Ryoko, Saes mother.  She worked as a nurse during the day and worked at a Karaoke Lounge as a hostess during the night to support Sae. She was widowed when her husband died because of cancer.
* Misako Renbutsu as Ritsuko Watanabe, Kouheis wife.  She liked Kouhei since she saw him working at the docks where she works as an accountant. She later married him, but divorced him and ran away. She later became a cashier at a supermarket. Arata as Kemimichi, Saes father.  He is an avid photographer, and frequently leaves Sae and her mom alone. He planted the Dogwood tree in Saes house garden.
* Yuichi Kimura as Makato Endo,  Saes neighbor and her mothers friend. He owned a ranch.
* Yutaka Matsushige  Eri Tokunaga as Minami Nakamura,  Saes best friend in high school. She later married one of Kouheis friends.
* Yuta Kanai 
* Yu Koyanagi 
* Tsutomu Takahashi 
* Mamatsu Hayashi  is playing the role of Kouheis younger sister.
* Kaori Mizushima 

==Production==

===Development===
The plot of the film is based on the song Hanamizuki, a song by Yō Hitoto released in 2004.   This popular song got to 4th in the Oricon charts. 

===Casting=== Nada Soso. 

===Filming===
 

In Japan, it was filmed in Waseda University in Tokyo and in the Shinjuku district, which is also in Tokyo. Next, the filming moved to Kushiro, Hokkaido in September 2009, completing the filming in Japan itself. 
 Brooklyn Heights Union Square. Peggys Cove Lighthouse.  On 23 April 2010, Yui Aragaki and Toma Ikuta was greeted at their filming site there.    Aragaki described this filming location as "quite distant (except for the scene at sea)" and added that "Luckily, there have not been a major accident during the filming."  Hanamizuki director Nobuhiro Doi said that after completing the filming across three countries, he felt "nothing but relief" and thanked the cast for having gone through the long journey with him. 

===Theme song===
The films theme song was  , which was sung by Yō Hitoto and on which this film was based on. 

==Soundtrack==
{{Infobox album
| Name        = Hanamizuki Original Soundtrack
| Type        = Soundtrack Takefumi Haketa
| Cover       =
| Length      = 41 minutes
| Recorded    =
| Released    =     
| Label       = Columbia Music Entertainment (COCP-36327) 
}}
The soundtrack for this film was released before the film on 10 August 2010. 

Track list: 

#  
# "Opening Title"
# "Jig for the Fiddle #1"
# "Lighthouse on the Hill -Main Theme-"
# "Jig for the Fiddle #2"
#  
# "New York"
#  
#  
# "Irish Reel"
#  
# "Fiddle and Guitar -Main Theme-"
#  
#  
#  
#  
#  
#  
#  
# Lighthouse on the Hill -reprise-
#  

==Release==
Hanamizuki was first released in Japan nationwide on 21 August 2010.  Next, it debuted in Thailand under the Thai name title ฮานามิซึกิ เกิดมาเพื่อรักเธอ on 23 December 2010.  In Taiwan, Hanamizuki was released on 1 January 2011.    In Hong Kong, Hanamizuki was released under the Chinese name of "花水木" on 9 April 2011. 

Hanamizuki was also screened at the 30th Hawaii International Film Festival under the category of "Spotlight on Japan" in 2010, marking its international debut.   

===Home media===
The Hanamizuki home video was released on 4 March 2011. It was released in three different versions: DVD (region 2) Normal Edition, DVD (region 2) Special 2-Disc Edition and in Blu-ray Format. 

==Reception==

===Box office===
Hanamizuki debuted on 310 cinema screens around Japan, where it became the highest grossing film on the weekend of 21–22 August 2010 with a record gross of over 400 million yen, and at the same time breaking Studio Ghiblis film The Secret World of Arrietty four consecutive week hold on the top position in the Japanese box office.    During this weekend, over 301,000 people went to watch this film.  Hanamizuki managed to cling on to its top position for two consecutive weekends,  managing to break the 1 billion yen mark and having over 1 million viewers during the weekend.   

In total, Hanamizuki gross of $32,891,241 in Japan cinemas.  The Motion Picture Producers Association of Japan put its gross at 2.83 billion yen, making it the 12th highest grossing Japanese film of 2010. 

In Thailand, Hanamizuki debuted at the ninth position on its debut weekend.    It was screened there for two weekends, garnering a total gross of $22,782.  Hanamizuki managed to achieve the 10th position on its Taiwanese debut weekend of January 1–2.  Over its run of three weeks, it grossed a total of $56,268 in Taiwanese cinemas, making it the second-highest grossing territory after Japan. 

===Critical reception===
Before the film release, Asahi Shinbun wrote in an article that Hanamizuki was a "modest masterpiece of the Yō Hitotos work", and that it makes the audience want to cry.    The article ended by saying, "Now to see more and more." 

Derek Elley, reviewing for Film Business Asia, criticized the screenplay of the film saying that it was "the weakest element in Hanamizuki".    He did, however, praised actress Yakushimaru Hiroko, calling it "the best performance (in the film)".  Overall, the reviewer concluded that the film is a "workmanlike story better suited to afternoon TV." and gave the film a rating of 5 out of 10. 

===Accolades===
{| class="wikitable"
|-
! Year
! Award
! Category
! Result
! Recipient
|- 2010
| 32nd Yokohama Film Festival
| Best Newcomer Award
|  
| Osamu Mukai      Also for his role in BECK 
|-
| 53rd Blue Ribbon Awards
| Best Newcomer Award
|  
| Toma Ikuta        Also for his role in Ningen Shikkaku 
|}

==Merchandise==

===Illustrated book===
The book The Three Little Pigs, which was used as a prop in the film and it was used in the scene where Sae when she taught her young students English, was put up for sale as one of the films publicity move.    The book was illustrated by Yui Aragaki, and includes a CD containing the narration of the story in Japanese with Aragaki as one of the narrators.  The book was published by mpi and was released on 25 June 2010, before the release of the actual film. 

===Film manga===
A manga version of Hanamizuki was published by Shueisha in Japan.    It was released in a tankōbon volume on 15 February 2011. 

====Volume list====
{{Graphic novel list/header
 | Language = Japan
}}
{{Graphic novel list
 | VolumeNumber    = 1
 | OriginalISBN    = 978-4088671031
 | OriginalRelDate = 15 February 2011 
 | LicensedISBN    = -
 | LicensedRelDate = - 
}}
 

==References==
 

==External links==
*    
*  
*  

 

 
 
 
 
 
 