The Adventures of Ford Fairlane
 
{{Infobox film
| name         = The Adventures of Ford Fairlane
| image        = Fordfairlaneposter.jpg
| caption      = Theatrical release poster
| director     = Renny Harlin
| producer     = Joel Silver Steve Perry Daniel Waters James Cappe David Arnott
| story        = James Cappe David Arnott
| based on     = characters by Rex Weiner
| starring     = Andrew Dice Clay Wayne Newton Priscilla Presley Lauren Holly Maddie Corman David Patrick Kelly Morris Day with Robert Englund and Ed ONeill
| music        = Yello  (original score) 
| editing      = Michael Tronick Oliver Wood
| studio       = Silver Pictures
| distributor  = 20th Century Fox
| released     =  
| runtime      = 104 minutes
| country      = United States English
| budget       =
| gross        = $21,413,502  (domestic) 
}}

The Adventures of Ford Fairlane is a 1990 American action film|action/comedy film directed by Renny Harlin. It stars comedian Andrew Dice Clay as the title character, Ford Fairlane, a private detective|"Rock n Roll Detective," whose beat is the music industry in Los Angeles. True to his name, Ford drives a 1957 Ford Fairlane 500 Skyliner in the film.

The movies main character was created by writer Rex Weiner in a series of stories that were published as weekly serials 1979–80 by the New York Rocker and the LA Weekly. The stories have since been published as an e-Book.

==Plot==
Ford Fairlane is seen sitting on a beach smoking as the film opens. A flashback initiates, showing a roaring crowd at a concert given by fictional popular heavy metal band The Black Plague. Lead singer Bobby Black (Vince Neil) makes an eccentric entrance down a zip-line onto the stage and begins performing. Shortly into one of the bands songs, Bobby Black collapses on stage and dies.

After The Black Plague singer is murdered onstage, shock-jock Johnny Crunch (Gilbert Gottfried), an old friend who came west with Fairlane, hires Ford to track down a mysterious teenage groupie named Zuzu Petals who may have a connection to Bobby Blacks death.

Soon after hiring Fairlane, Crunch is electrocuted on the air. The worlds hippest detective soon finds himself trading insults with a ruthless record executive (Wayne Newton), a clueless cop and former disco star (Ed ONeill), a merciless hit man (Robert Englund) and countless ex-girlfriends out for his blood. Aiding and abetting Fairlane in this whodunnit is loyal assistant Jazz (Lauren Holly) and a hip record producer (Morris Day) at the head of a bizarre lineup of suspects, victims, beautiful women and a koala as he finds himself hip-deep in the case of his life.

The macguffin of the film is three data CDs which, when read simultaneously, detail the illegal dealings of Julian Grendel, who murdered Bobby Black when he found out Black had acquired them.

The first disc was with Colleen Sutton, the second with Zuzu Petals, and the third disc was hidden under the star for Art Mooney on the Hollywood Walk of Fame.

==Cast==
*Andrew Dice Clay&nbsp;— Ford Fairlane
*Wayne Newton&nbsp;— Julian Grendel
*Priscilla Presley&nbsp;— Colleen Sutton
*Lauren Holly&nbsp;— Jazz
*Gilbert Gottfried&nbsp;— Johnny Crunch
*Maddie Corman&nbsp;— Zuzu Petals
*David Patrick Kelly&nbsp;— Sam the Sleaze Bag
*Brandon Call&nbsp;— The Kid
*Robert Englund&nbsp;— Smiley
*Ed ONeill&nbsp;— Lt. Amos
*Vince Neil&nbsp;— Bobby Black
*Morris Day&nbsp;— Don Cleveland
*Sheila E.&nbsp;— Club singer
*Lala Sloatman&nbsp;— Sorority Girl (credited as Lala)
*Kari Wuhrer&nbsp;— Melodi
*Linda Atkinson&nbsp;— Nona
*William Shockley - Punk Gunslinger
*Mark Zuelke - Punk Gunslinger

==Soundtrack==
Music being central to the plot of a film about a private detective who specializes in cases arising from the music industry, the soundtrack featured a diverse group of artists.  The official soundtrack release had the following tracks:

# "Cradle of Love"&nbsp;— Billy Idol Dion
# "Funky Attitude"&nbsp;— Sheila E.
# "Glad to Be Alive"&nbsp;— Lisa Fisher, Teddy Pendergrass
# "Cant Get Enough"&nbsp;— Tone Loc
# "Rock N Roll Junkie"&nbsp;— Mötley Crüe
# "I Aint Got You"&nbsp;— Andrew Dice Clay
# "Last Time in Paris"&nbsp;— Queensrÿche
# "Unbelievable"&nbsp;— Yello Wind Cries Mary"&nbsp;— Richie Sambora
 Charmed Life. In the video, a young woman, played by Betsy Lynn George, taunts an uptight neighbor with her advances as she dances to the music.  The video also featured footage from the film playing on a television in the neighbors home, although none of the footage features Clay (at least not his face).  This may be due to the infamous ban of Clay from appearing on the music network. Alternate versions of the "Cradle of Love" video eliminates the film footage when the video is usually aired on MTV.

Samboras contribution to the soundtrack was a cover of the Jimi Hendrix song. Yellos "Unbelievable" samples bits of dialogue from the film, with one notable dialog switch - where in the movie a phone number is said as "1-800-Unbelivable" and in the song, the phone number is said as "1-800-Perfect".  A number of the musicians featured on the soundtrack also appeared in the film itself, including Morris Day, Sheila E., Tone Loc (as Slam the Rapper), former Ozzy Osbourne bassist Phil Soussan & drummer Randy Castillo appear playing the Black Plague concert during the flashback at the beginning of the film and Vince Neil, the lead singer of Mötley Crüe (who appeared as Bobby Black, the lead singer of the fictitious band, Black Plague). Black Plagues lead guitarist was played by Quiet Riots axeman Carlos Cavazzo. Not appearing on the soundtrack is "Booty Time", the song that Ed ONeills character performs during the film.
 Baby is used as the films incidental soundtrack.

==Reception==

===Critical response===
The film received mostly negative reviews upon release and holds a "rotten" 26% rating at Rotten Tomatoes.  Critic Roger Ebert called the film "loud, ugly and mean-spirited". 

It was not a financial success during its original theatrical release, making just over $21 million in the U.S.  Clay has claimed in interviews that the movie had a successful first week before being pulled from theaters under pressure from the politically correct.  The film played on more screens during its second week than its first but still suffered a 53.5% drop in box office gross. 
 Razzies at 1990 Golden Raspberry Awards including Worst Actor (Andrew Dice Clay), Worst Picture (Joel Silver & Steve Perry&nbsp;— tied with Ghosts Cant Do It) and Worst Screenplay (Daniel Waters, James Cappe and David Arnott).  It was also nominated for Worst Director and twice for Worst Supporting Actor (for both Gilbert Gottfried and Wayne Newton). 

Billy Idols recording of "Cradle of Love" was named one of the "Most Performed Songs from Motion Pictures" by the ASCAP. 

===Reception abroad===

Contrary to the negative reviews in the US, the film was probably the biggest cult phenomenon of the video tape era in Hungary in the early 90s. Countless illegal copies of the movie were (and still are) circulating in the country.

The main cause for the popularity of the Hungarian release was the Hungarian dub, which was of extremely high quality. It starred iconic Hungarian actors, including Feró Nagy, rock singer and cult icon, in the role of Ford Fairlane. The translated script contained a lot of hilarious and over the top swearing, and catchphrases from the movie became engrained in local urban slang very rapidly.

In Norway, after the movie was released on VHS in 1992, Ford Fairlane soon became a phenomenon. The catchphrases became hugely popular and the movie received cult status during the 90s. After huge demand from Norwegian audiences the movie was released on DVD in the early 2000s.

In Spain. it also became quite popular, especially due to the dubbing of the movie by the popular singer, actor and comedian Pablo Carbonell.

==See also==
 
* List of American films of 1990

==References==
 

==External links==
 
* 
* 
* 
   
{{Succession box
| title=Golden Raspberry Award for Worst Picture  (tied with Ghosts Cant Do It)
| years=11th Golden Raspberry Awards
| before= 
| after=Hudson Hawk
}}
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 