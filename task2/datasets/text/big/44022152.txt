Sue, Mai & Sawa: Righting the Girl Ship
{{Infobox film
| name           = Sue, Mai & Sawa: Righting the Girl Ship
| image          = 
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Osamu Minorikawa
| producer       = Yoshitaka Takeda Kasumi Yao 
| writer         = 
| screenplay     = Sachiko Tanaka 
| story          = 
| based on       =  
| narrator       = 
| starring       = 
| music          = Shin Kono Kasarinchu
| cinematography = Gen Kobayashi 
| editing        = Hidemi Ri 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 106 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
}}

  is a 2012 Japanese drama film directed by Osamu Minorikawa and based on the manga series Su-chan by Miri Masuda. It premiered at the 25th Tokyo International Film Festival on October 24, 2012 and was released in Japan on March 2, 2013.   

==Cast==
*Kō Shibasaki as Yoshiko Morimoto 
*Yōko Maki (actress)|Yōko Maki as Maiko Okamura
*Shinobu Terajima as Sawako Hayashi 
*Shōta Sometani as Kosuke Chiba 
*Arata Iura as Seichiro Nakata 
*Hana Kino as Yoko Koba
*Poon-chaw Guin as Nobuko Hayashi
*Akiko Kazami as Shizue Hayashi  Megumi Sato as Mika Iwai
*Mio Uema as Chika Takeda
*Aoi Yoshikura as  Minami Koba
*Ai Takabe as Chigusa Maeda

==Reception==

===Critical response===
On Film Business Asia, Derek Elley gave the film a 6 out of 10, calling it "a mature chick flick|chick-flick with a fine array of actresses but rather over-slack pacing." 

===Accolades===
{| class="wikitable sortable" width="90%"
|- style="background:#ccc; text-align:center;"
! Award
! Date
! Category
! Recipients and  nominees
! Result
|-
| rowspan="1"| Kinema Junpo Awards
| rowspan="1"| 2014  Best Actress
| Yōko Maki (actress)|Yōko Maki
|  
|-
|}

==References==
 

==External links==
*   
* 

 
 
 
 
 


 
 

 