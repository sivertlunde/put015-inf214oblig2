Secuestro
Secuestro is an action film about three rookie kidnappers who scramble as their ill-fated plan falls apart. About the terrible wages of violence and crime.
 Juan Castillo.

==Awards==
NYU Graduate Film Program awards: Screenwriting, Production, Ensemble Cast, Cinematography, Editing, Directing – the only foreign language NYU graduate thesis film to garner all key category awards.
*Showtime Network Latino Filmmakers Showcase.
*NYU Spike Lee Fellowship.
*Global Foundation for Democracy and Development (FUNGLODE) Maria Montez short film first prize presented by Dominican president Leonel Fernández.

==Cast==
*Leidy Luna, Segunda
*Elvis Nolasco, Jefe
*Tony Pascual, Novato
*Robert Santana, Victim

==Other==
Genre: Action-Suspense-Thriller

Duration: 30 minutes

Color: color

Format: 35mm

Locations: Dominican Republic

==External links==
*  

 
 