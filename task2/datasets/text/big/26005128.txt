Revati (film)
{{Infobox film
| name = Revati
| image =  
| caption = Theatrical release poster
| director = Faroogh A. Siddique
| producer = Vikas M. Kate
| writer = Sunil Akhtar Tapan A.Bhatt  (Dialogue) 
| screenplay = Faroogh A. Siddique
| story = Faroogh A. Siddique Ayub Kha Vijay Kadam
| music = Jatin-Lalit Nazakat Shujat
| cinematography = Akram Khan
| editing = Suresh Chaturvedi	
| studio = Victory International
| distributor = Ultra Worldwide release
| released =  
| runtime = 142 minutes
| country = India
| language = Hindi Marathi
}} 2005 Hindi Indian film written and directed by Farogh Siddique. The film stars Kashmira Shah.

== Plot ==
The story of a rebel trash picker living in the slums of Bombay India. She dreams of being able to have simple luxuries like a bath or decent clothes to cover her body. One day she gets her opportunity through a drug smuggler of Bombay and quickly learns all that glitters is not gold. Whether it be a drug lord, a murder, rapist or the legal system; Revati is a fighter that keeps a sense of humor about her plight. Revati shows the lifes struggles of a woman struggling to keep her morality and dignity in a place that tries hard to strip her of all she has in many extreme circumstances. Revati dares to be different and live by her own rules. Will she win and yet keep her morality? Will she survive?

== Cast ==
* Kashmira Shah
* Kiran Kumar Ayub Kha
* Vijay Kadam

==External links==
* 

 
 
 


 