Heidi Fleiss: Hollywood Madam
 
{{Infobox film
| name           = Hollywood Madam
| image          = 
| caption        = 
| director       = Nick Broomfield
| producer       = Nick Broomfield
| writer         =  Ivan Nagy Madam Alex Daryl Gates  
| music          = David Bergeaud
| cinematography = Paul Kloss 
| editing        = Susan J. Bloom
| distributor    = InPictures
| released       =    
| runtime        = 106 min
| country        = United States England
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

Heidi Fleiss: Hollywood Madam (1996) is a feature-length documentary film by Nick Broomfield.  It is an examination of the woman herself, Broomfield attempting to uncover if she truly is a horrible person, and what made her that way.
 Ivan Nagy, himself allegedly involved in prostitution.  Furthermore the destructive relationship between Fleiss and Nagy is discussed, a sort of mutual aggressiveness that both seem to find appealing.

==Box office==

Despite positive reviews (Roger Ebert called it one of the ten best films of the year), the film did not achieve box office success.  Opening at two North American theatres on February 9, 1996, it grossed $14,321 ($7,160 per screen) in its opening weekend, on its way to a final gross of just $34,402.

==External links==
*  
*  
*  

 

 
 
 
 

 