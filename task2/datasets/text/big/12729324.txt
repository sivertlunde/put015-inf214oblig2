The ChubbChubbs Save Xmas
{{Infobox Film
| name = The ChubbChubbs Save Xmas
| image = The ChubbChubbs Save Xmas poster.jpg
| caption = Poster for The ChubbChubbs Save Xmas
| director = Cody Cameron
| producer = Kirk Bodyfelt
| writer = David Feiss  Cody Cameron   Jurgen Gross
| starring = Bradford Simonsen  Jeff Wolverton  Jeffrey Tambor  Zachary Gordon  Brad Spoylt
| music = Ramin Djawadi
| studio = Sony Pictures Animation Sony Pictures Imageworks Prana Studios
| distributor =   (2007, with Daddy Day Camp.)
| released = August 8, 2007
| runtime = 5 minutes
| country = United States
| language = English
| budget =
}}
 short by Sony Pictures Animation. It was produced by Kirk Bodyfelt and directed by Cody Cameron. It was theatrically released on August 8, 2007 along with Daddy Day Camp {{cite web | author=PIMPJOE_ESB | url=http://www.countingdown.com/movies/3171870/news?item_id=3984136 | title=
Catch the ChubbChubbs Saving Xmas With "Daddy Day Camp" | publisher=CountingDown.com | date=2007-08-08 | accessdate=2010-11-10}}  and is a sequel to the Academy Award-winning animated short film The ChubbChubbs!.

On October 9, 2007, both The ChubbChubbs! and The ChubbChubbs Save Xmas animated shorts were released as special features on the Surfs Up (film)|Surfs Up DVD and Blu-ray.

==Plot==
Meeper and the ChubbChubbs want a home for Christmas, but when they land on earth, they fall on Santa. The sake of the holiday was in Meepers hands.

Meanwhile, a mean little boy named Brad hacks the www.IveBeenGood.com website and puts him on Santas nice list when he finds out he is naughty. This causes Meeper to think that he was good. When Meeper gets to the house, one of the ChubbChubbs is taken by Brad. Puzzled, Brad tries to find the opening for batteries when puts it in the wrong place.

Meeper finds out and picks up the biggest ChubbChubb. The creatures finally get there wish....a trailer, but the ChubbChubbs eat the house, causing them to go back home.

==Cast==
* Brad Simonsen as Meeper the Alien
* Jeff Wolverton as the ChubbChubbs
* Jeffrey Tambor as Santa Claus
* Zachary Gordon as Brad Spoylt

==References==
 

== External links ==
*   at Sony Pictures Animation
*  
*  

 

 
 
 
 
 
 
 
 
 
 


 