Today You Die
 
{{Infobox film 
| name = Today You Die
| image = Todayyoudie dvd.jpg
| caption = DVD cover
| director = Don E. Fauntleroy
| producer = Randall Emmett George Furla Danny Lerner Steven Seagal
| writer = Kevin Moore
| story = Danny Lerner Sarah Buxton Mari Morrow Nick Mancuso Robert Miano
| music = Steve Edwards
| cinematography = Don E. Fauntleroy
| editing = Robert Ferretti Millennium Films Nu Image Emmett/Furla/Oasis Films|Emmett/Furla Films
| distributor = Sony Pictures Home Entertainment
| released =  
| runtime = 90 minutes
| country = United States
| language = English
}} Sarah Buxton, Mari Morrow, Nick Mancuso and Robert Miano. The film was released on direct-to-video|direct-to-DVD in the United States on September 13, 2005.

==Plot==
Harlan Banks (Steven Seagal) is a Robin Hood kind of thief who has always picked his own jobs and tried to pull heists that would leave him room to help out others. The work keeps getting riskier, and the urging of his girlfriend Jada (Mari Morrow), Banks has decided to pull one final job, going in with some men who are planning a $20 million robbery.

After the heist goes bad, Banks heads to Las Vegas, where Jada wants him to get a real job. On the way to town, Banks and Jada passed a childrens hospital displaying a going out of business sign. Banks gets a job driving an armored car for a man, Max (Kevin Tighe). The job is not exactly legitimate, and Bruno (Robert Miano), Banks partner for the job, and shoots a security guard, resulting in a chase through the Vegas strip in the armored van.

However, Banks is stopped and sent to prison, where he befriends an inmate known as Ice Kool (Treach). With Ices help, Banks escapes, determined to hunt Max down. Along the way, Banks meets a federal agent named Saunders (Nick Mancuso), and it turns out that Saunders, who is in league with Max, is the man behind the setup. However, Banks sets out to take down both Saunders and Max with a job.

==Cast==
* Steven Seagal as Harlan Banks
* Treach as Ice Kool Sarah Buxton as Agent Rachel Knowles
* Mari Morrow as Jada
* Nick Mancuso as Agent Saunders
* Robert Miano as Bruno
* Kevin Tighe as Max
* Jamie McShane as Vincent
* Lawrence Turner as Garret
* Brett Rice as Taggert
* Lance J. Mancuso as Casino Guard
* Chloë Grace Moretz as St. Thomas Hospital Girl
* Elayn J. Taylor as Old Tarot Reader
* Hawthorne James as Derrick
* David Fryberger as Cop
* Morann Peri as Cop Partner
* John Gulino as Marshall
* Smalls as Dinky-D
* Darren Ting as Ming Lee
* John Wister as Rusty
* Lesley-Anne Down as Bank Manager
* J. Anthony Pena as Hispanic
* Lisa Guerrero as Reporter
* Brian Jay as Bartender
* Jerry Trimble as Garrets Gang #1
* J.J. Perry as Thug
* Les Weldon as Helicopter Pilot
* Randy Couture as Vincents Bodyguard #1 (uncredited)

==Production==
It is set and filmed at Sofia, Bulgaria and Las Vegas, Nevada in 56 days on October 4 and November 29, 2004.

==Legal troubles==
The producers of Today You Die filed lawsuits against Seagal because of experiences during the filming. Producers at Nu Image and Kill Master Productions alleged that Seagal, while filming this and Mercenary for Justice, arrived at the set late, left early, and re-wrote scripts without their permission, among other things. However, Seagal disputed the suit, and he countersued against producers, claiming fraud and breach of contract. As of January 2006, it appears the legal issues have not yet been resolved.

==Home media== Region 1 in the United States on September 13, 2005, and also Region 2 in the United Kingdom on 10 July 2006. It was distributed by Sony Pictures Home Entertainment.

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 