Beyond the Sierras
{{Infobox film
| name           = Beyond the Sierras
| image          = Beyond the Sierras poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Nick Grinde
| producer       = 
| screenplay     = Robert Lord
| story          = John T. Neville 
| starring       = Tim McCoy Sylvia Beecher Roy DArcy Polly Moran Richard Neill J. Gordon Russell
| music          =
| cinematography = Arthur Reed	
| editing        = William LeVanway 
| studio         = Metro-Goldwyn-Mayer
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 60 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 action silent film directed by Nick Grinde and written by Robert Lord. The film stars Tim McCoy, Sylvia Beecher, Roy DArcy, Polly Moran, Richard Neill and J. Gordon Russell. The film was released on September 15, 1928, by Metro-Goldwyn-Mayer.  

==Plot==
 

== Cast ==	
*Tim McCoy as The Masked Stranger
*Sylvia Beecher as Rosa del Valle
*Roy DArcy as Owens
*Polly Moran as Inez
*Richard Neill as Don Carlos del Valle 
*J. Gordon Russell as Wells 

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 

 