Kasam Suhaag Ki
{{Infobox film
| name           =Kasam Suhaag Ki
| image          = 
| image_size     = 
| caption        = 
| director       =Mohan Segal	 
| producer       =N.P. Singh
| writer         =Ali Raza
| narrator       = 
| starring       =Dharmendra Rekha
| music          = Laxmikant-Pyarelal
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1989
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

Kasam Suhaag Ki  is a 1989 Bollywood film directed by Mohan Segal and starring Dharmendra and Rekha. 

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Aa Gale Lag Ja"
| Anuradha Paudwal
|-
| 2
| "Chiraiya Ko Baaz Liye Jaye"
| Anuradha Paudwal
|-
| 3
| "Idhar Bhi Bijlee"
| Kavita Krishnamurthy
|-
| 4
| "Main Ho Gayee Deewani"
| Hemlata
|}

==External links==
*  

 

 
 
 
 