Andrea Chénier (film)
{{Infobox film
 | name =Andrea Chénier
 | image = Andrea Chénier (film).jpg
 | image_size = 265px
 | caption =
 | director =  Clemente Fracassi
 | writer =  
 | starring =  Antonella Lualdi
 | music =  Giulio Cesare Sonzogno 
 | cinematography =   Piero Portalupi
 | producer =  
 | released = 1955
 | language =  Italian
 }} 1955 Cinema Italian drama film directed by Clemente Fracassi.

It is loosely based on the eponymous 1896 opera by Umberto Giordano.   

== Cast ==
*Antonella Lualdi: Madeleine de Coigny
*Raf Vallone: Gérard
*Michel Auclair: Andrea Chénier
*Rina Morelli: mother of Andrea
*Sergio Tofano: Luigi Chénier
*Denis dInès: Countess of Coigny 
*Piero Carnabuci: Count of Coigny
*Maria Zanoli: housekeeper
*Nando Cicero

==References==
 

==External links==
* 

 
 
 
 
 


 
 