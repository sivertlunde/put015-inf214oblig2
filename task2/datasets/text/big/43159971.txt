Love Is Duty Free
{{Infobox film
| name = Love is Duty Free
| image =
| image_size =
| caption =
| director = E.W. Emo
| producer =
| writer =  Fritz Koselka  
| narrator = Hans Moser   Susi Peter   Theodor Danegger   Hans Olden
| music = Hanns Elin   Max Niederberger 
| cinematography = Georg Bruckbauer  
| editing =  
| studio = Wien-Film 
| distributor = 
| released = 17 April 1941 
| runtime =
| country = Austria   (Greater Germany)  German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} Hans Moser, Greater Germany following the 1938 Anschluss. The film was intended to mock the First Austrian Republic and its democratic system of government as incompetent. 

==Synopsis==
The financially hard-pressed Austrian government have arranged a secret deal with the Swiss, but an officious Austrian customs officer is unaware of this and arrests the Swiss representatives in the belief that they are wanted criminals.

==Cast== Hans Moser as  Laurenz Hasenhüttl
* Susi Peter as Hasenhüttls Tochter
* Theodor Danegger as Schweizer Gesandter
* Hans Olden as Finanzminister
* Maria Eis as Frau des Finanzminister
* Else Elster as Geliebte des Finanzministers
* Hans Unterkircher as Minister Bouvier
* Oskar Sima as Bundeskanzler
* Heinrich Heilinger as Ministerialrat
* Erik Frey as Ministerialsekretär
* Fritz Imhoff as Pförtner
* Josef Egger as Amtsdiener
* Alfred Neugebauer as Amerikanischer Millionär
* Karl Skraup as Chefredakteur Grimsky
* Gisa Wurm as Stubenmädchen
* Robert Freitag as Schweizer Zollbeamter
* Hermann Erhardt as Polizeiwachtmeister
* Josef Eichheim as Wirt in Feldkirch
* Gertrud Wolle as Englische Touristin

== References ==
 

== Bibliography ==
* Hake, Sabine. Popular Cinema of the Third Reich. University of Texas Press, 2001.

== External links ==
*  

 
 
 
 
 
 
 
 

 