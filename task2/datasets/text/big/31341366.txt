Velli Vizha
{{Infobox film
| name           = Velli Vizha
| image          =
| caption        =
| director       = K. Balachander
| producer       = B. Duraisami
| writer         = 
| screenplay     = 
| story          =  Jayanthi Vanisri S. Varalakshmi
| music          = V. Kumar
| cinematography = 
| editing        = 
| studio         = Kalaakaendraa
| distributor    = Kalaakaendraa
| released       =  
| runtime        = 170 minutes
| country        = India
| language       = Tamil language|Tamil}}
 1972 Cinema Indian Tamil Tamil film,  directed by K. Balachander and produced by B. Duraisami. The film stars Gemini Ganesan, Jayanthi (actress)|Jayanthi, Vanisri and S. Varalakshmi in lead roles. The film had musical score by V. Kumar.   

==Cast==
* Gemini Ganesan Jayanthi
* Vanisri
* S. Varalakshmi
* V. Gopala Krishnan

==Soundtrack==
The music was composed by V. Kumar.  
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|-  Vaali || 03.13 
|-  Vaali || 03.11 
|-  Susheela || Vaali || 02.48 
|-  Vaali || 03.14 
|-  Vaali || 03.20 
|}

==References==
 

==External links==
*  
*  

 

 
 
 
 
 


 