A Christmas Carol (2009 film)
 
{{Infobox film
| name           = A Christmas Carol
| image          = ChistmasCarol2009-Poster.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Robert Zemeckis
| producer       = Jack Rapke Steve Starkey Robert Zemeckis
| screenplay     = Robert Zemeckis
| based on       = A Christmas Carol&nbsp;by   Robin Wright Penn Cary Elwes
| music          = Alan Silvestri
| cinematography = Robert Presley
| editing        = Jeremiah ODriscoll
| studio         = Walt Disney Pictures ImageMovers Digital Walt Disney Studios Motion Pictures
| released       =  
| runtime        = 95 minutes 
| country        = United States
| language       = English
| budget         = $175-200 million    
| gross          =  $325.3 million   
}} Robin Wright Penn, and Cary Elwes.
 The Polar Express (2004), and Beowulf (2007 film)|Beowulf (2007). 

A Christmas Carol began filming in February 2008, and was released on November 3, 2009 by Walt Disney Pictures.  It received its world premiere in London, coinciding with the switching on of the annual Oxford Street and Regent Street Christmas lights, which in 2009 had a Dickens theme.  
 How the Grinch Stole Christmas (2000).

==Plot==
In 1843, Ebenezer Scrooge, a bitter and miserly old moneylender at a London counting house holds everything that embodies the joys and spirit of Christmas in contempt. He refuses to visit his cheerful nephew, Fred, at his Christmas dinner party with his family, and he forces his underpaid employee Bob Cratchit to beg to take the day off for his own family. On Christmas Eve, Scrooge is visited by the ghost of his former business partner Jacob Marley, who had died seven years prior and is now forced to spend his afterlife carrying heavy chains forged from his own greedy ways. Marley warns Scrooge that he will suffer an even worse fate if he does not repent. He then tells Scrooge he will be visited by three more spirits that will help guide him.

The first spirit is the Ghost of Christmas Past, which shows Scrooge visions of his own past that take place around the Christmas season, reminding Scrooge of how he ended up the avaricious man he is now. In the visions, Scrooge spends much of his childhood neglected by his father over the holidays at a boarding school until hes finally brought home by his loving sister, Fan, who dies prematurely after giving birth to her son, Fred. Scrooge later begins a successful career in business and money lending, and he becomes engaged to a woman named Belle, though she later breaks off the engagement when his obsession with wealth drives her away. The elderly Scrooge is unable to bear witnessing these events again and extinguishes the spirit with its candle snuffer cap. This causes Scrooge to be rocketed thousands of feet into the air while clinging onto the snuffer, only to have it disappear, resulting in Scrooge falling down to earth, back into his bedroom for the next visitation.

The second spirit is the Ghost of Christmas Present, which shows Scrooge the happiness of his fellow men on Christmas Day. Among them are Fred, who playfully makes jokes with his family at Scrooges expense, and the Cratchit family, who are barely able to make do with what little pay Scrooge gives them. Scrooge is touched by the Cratchits sickly young son Tiny Tim and his commitment to the spirit of Christmas, and he is dismayed to learn from the spirit that Tim may not have much longer to live. Before dying, the spirit warns Scrooge about the evils of "Ignorance" and "Want", Big Ben begins tolling the hour, as "Ignorance" and "Want" manifest themselves before Scrooge as two wretched children who grow into violent, insane individuals, leaving the spirit dying and wither into dust as Big Ben finishes its hour toll, in front of Scrooge.
 fence named Old Joe; and the men who attended his funeral had only gone for a free lunch. Tiny Tim is also shown to have died, leaving the Cratchit family to mourn him on Christmas. The horrified Scrooge asks the spirit whether the images he has seen are sure to happen or can be changed. To little response, the spirit reveals Scrooges own grave, showing his own date of death as December 25 of a forthcoming year (or perhaps the very next morning, as the year is never shown), and forces Scrooge to fall into his empty coffin sitting in a deep grave atop the fires of Hell.

Scrooge suddenly awakens to find it is Christmas Day, and all three spirits have visited him over the course of one night. He joyously gives a child on the street some money to buy a prize turkey and have it delivered to the Cratchits. He then attends his nephews dinner, giving money to the poor and celebrating with his fellow men along the way. When Bob Cratchit comes to work, Scrooge grants him the day off and raises his salary after he has him deliver the money to the bank. As he steps out, Bob Cratchit affirms with the audience that Scrooge has become a kinder man and a second father to Tiny Tim, who survives thanks to Scrooges charity.

==Cast==
 
* Jim Carrey as:
** Ebenezer Scrooge, an ill-tempered, stingy, selfish man, who despises Christmas and all things which engender happiness.
** Ghost of Christmas Past, the first of the three spirits that haunt Scrooge in order to prompt him to repent. He is depicted as a young, androgynous human with a waxy, candle-like body and a flickering flame for a head, who speaks in a dreamy, slow voice with an Irish accent, and sways about. ermine robe who ages rapidly while he is with Scrooge. He has a tendency to laugh heartily, even as he dies, and carries the sins of Ignorance and Want upon his person, in the forms of horrifying, savage children. Grim Reaper cast across the ground or a wall, and occasionally emerges into three dimensions to point at something or to chase Scrooge in a large, shadow-like horse-drawn hearse.
* Gary Oldman as:
** Bob Cratchit, Scrooges unappreciated, underpaid clerk.
** Jacob Marley, The ghost of Scrooges former business partner, bound in chains and damned to walk the earth due to his cold-hearted, wasted life. Tiny Tim, Cratchits youngest son. While Gary Oldman provided the motion capture, Tiny Tims voice is provided by Ryan Ochoa.
* Colin Firth as Fred, Scrooges affable nephew and only living relative.
* Bob Hoskins as:
** Fezziwig|Mr. Fezziwig, the proprietor of a warehouse business for whom Scrooge worked as an apprentice. fence who buys the belongings of the deceased Scrooge from Mrs. Dilber. Robin Wright Penn as:
** Belle, Scrooges neglected fiancée.
** Fan Scrooge, Scrooges late sister who died prematurely after giving birth to Fred.
* Cary Elwes as:
** Dick Wilkins, Scrooges old roommate.
** Mad Fiddler
** Businessman #1
** Guest #2
** Portly Gentleman #1, a man who requests from Scrooge a donation to those less fortunate.
** Destitute Man #2
* Steve Valentine as:
** Funeral director|Undertaker, a funeral worker that Scrooge meets with revolving around Jacob Marleys death.
** Topper
* Julene Renee-Preciado as Adult Want
* Fionnula Flanagan as Mrs. Dilber, Scrooges charwoman.
* Kerry Hoyt as Adult Ignorance
* Molly C. Quinn as Belinda Cratchit.
* Ryan Ochoa as:
** Tattered Caroler
** Beggar Boy
** Young Cratchit Boy
** Ignorance Boy
** Young Boy with Sleigh
* Daryl Sabara as:
** Funeral director|Undertakers Apprentice
** Tattered Caroler
** Beggar Boy
** Peter Cratchit
** Well-Dressed Caroler
* Sammi Hanratty as:
** Beggar Boy
** Young Cratchit Girl
** Want Girl
* Lesley Manville as Bob Cratchits wife.
* Fay Masterson as:
** Martha Cratchit
** Guest #1
** Caroline
* Ron Bottitta as:
** Tattered Caroler
** Well-Dressed Caroler
* Jacquie Barnbrook as:
** Mrs. Fezziwig
** Freds Sister-in-Law
** Well-Dressed Caroler
* Paul Blackthorn as:
** Guest #3
** Businessman #2
* Julian Holloway as:
** Fat Cook
** Portly Gentleman #2
** Businessman #3 
* Michael Hyland as Guest #4
* Leslie Zemeckis as Freds Wife

==Production== The Polar Express, which was also directed by Robert Zemeckis served as the basis for Ebeneezer Scrooge in this movie. 

==Release==

===Box office===
The film opened at #1 in 3,683 theaters, grossing $30,051,075 its opening weekend, with an average of $8,159 per theater.  The film has come to gross an estimated $137,481,366 in the United States and Canada and $181,000,000 in other territories, for a worldwide total of $318,481,366.  In the UK, A Christmas Carol topped the box office on two separate occasions; the first was when it opened, the second was 5 weeks later when it leapfrogged box office chart toppers   and Paranormal Activity despite family competition from Nativity! (film)|Nativity!, another Christmas-themed movie.

===Home media=== 2D Blu-ray Disc|Blu-ray/DVD combo and in a four-disc combo pack that includes a Blu-ray Disc#Blu-ray 3D|Blu-ray 3D, a Blu-ray 2D, a DVD and a digital copy. This marked the first time that a film was available in Blu-ray 3D the same day as a standard Blu-ray 2D,  as well as Disneys first in the Blu-ray 3D market.  The DVD contains deleted scenes and two featurettes called "On Set with Sammi Hanratty|Sammi" and "Capturing A Christmas Carol". The Blu-ray 2D also has a "Digital Advent Calendar" and the featurette "Behind the Carol: The Full Motion Capture Experience". The Blu-ray 3D has an exclusive 3D game called "Mr. Scrooges Wild Ride".

==Reception==
  normalized rating out of 100 to reviews from film critics, has a rating score of 55 based on 32 reviews.   

In his review, Roger Ebert of the Chicago Sun-Times gave the film four stars (out of four), calling it "an exhilarating visual experience".  Owen Gleiberman of Entertainment Weekly gave the film an A, applauding the film as "a marvelous and touching yuletide toy of a movie". {{cite news|url= Wall Street Journal wrote in his review that the films "tone is joyless, despite an extended passage of bizarre laughter, several dazzling flights of digital fancy, a succession of striking images and Jim Carreys voicing of Scrooge plus half a dozen other roles." 

  praised the film for sticking to Dickens original dialogue but also questioned the technology by saying, "To an extent, this Christmas Carol is a case of style – and stylisation – overwhelming substance." 

===Awards and nominations===
{| class="wikitable" style="font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Award
! Category
! Recipients and nominees
! Result
|-
|rowspan="2"| 2010 Kids Choice Awards Favorite Voice from an Animated Movie
| Jim Carrey
|  
|-
| Favorite Animated Movie
|rowspan="2"| A Christmas Carol
|  
|- 36th Saturn Awards Best Animated Feature 
|  
|}

==See also==
* List of ghost films
* A Christmas Carol (score)|A Christmas Carol (score) List of A Christmas Carol adaptations
* List of 3D films

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 