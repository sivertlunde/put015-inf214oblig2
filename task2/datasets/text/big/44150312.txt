Radhayude Kamukan
{{Infobox film
| name           = Radhayude Kamukan
| image          =
| caption        =
| director       = Hassan
| producer       = Areefa Enterprises
| writer         =
| screenplay     = Rohini Seema Seema
| music          = A. T. Ummer
| cinematography =
| editing        =
| studio         = Nalanda Enterprises
| distributor    = Nalanda Enterprises
| released       =  
| country        = India Malayalam
}}
 1984 Cinema Indian Malayalam Malayalam film, Rohini and Seema in lead roles. The film had musical score by A. T. Ummer.   

==Cast==
*Ratheesh Rohini
*Seema Seema

==Soundtrack==
The music was composed by A. T. Ummer and lyrics was written by Ramachandran. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Punchirithookunnen || S Janaki || Ramachandran || 
|-
| 2 || Punchirithookunnen || Sujatha Mohan || Ramachandran || 
|}

==References==
 

==External links==
*  

 
 
 

 