June 17th, 1994
 

{{Infobox film
| name           = June 17th, 1994
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = 
| director       = Brett Morgen
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| based on       = 
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} police chase of O. J. Simpson.  Morgen says the diversity of the events provide an opportunity "to look at the soul of America." 

The documentary features no narration and consists simply of music set to clips from news sources during the day. 

== Events occurring in the day ==
The events detailed in the documentary that occurred during the chase of Simpson are as follows.
* Arnold Palmer playing his final round at the 1994 U.S. Open (golf)|U.S. Open. FIFA World Cup. Stanley Cup Finals with a ticker-tape parade on Broadway (Manhattan)|Broadway. NBA Finals between the Houston Rockets and the New York Knicks.

== References ==
 
 

 
 
 