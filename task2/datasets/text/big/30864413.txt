Zaalim
 
 
{{Infobox film
| name = Zaalim
| image =
| writer = Madhan Joshi (Story, Screenplay and Dialogues)
| starring = Akshay Kumar Madhoo
| director = Ramesh Sippy
| producer = G.P. Sippy
| music = Anu Malik, Ashok Sharma
| lyrics = Anand Bakshi
| released = 1994
| language = Hindi
}}

Zaalim is an Indian film directed by Sikander Bharti and released in 1993. It stars Akshay Kumar, Madhoo, Vishnuvardhan, Ranjeet, and Alok Nath.

==Plot==
Judge Somnath lives with his wife, three sons, and a daughter. Two of his sons, Vikram and Mohan are married, while his daughter, Kaamna, and youngest son, Ravi, are of marriageable age. Somnath had wanted his sons to be a doctor, police officer, and a Judge. While Vikram is a surgeon, Mohans a Police Inspector, & Ravi is now studying law and on his way to become a lawyer and then a Judge like his dad. This family does have a dark secret. Ravi is prone to losing his temper, so much so that he gets out of control, and has killed someone in his childhood. When Kaamna gets raped, the family are reluctant to tell Ravi. When they do they do convince him to control himself, while Mohan gets an arrest warrant for Vinod, Kaamnas molester. Things do not go smoothly in court as the matter is put off for several months and then Somnath and Kaamna are killed in a bomb explosion. Now Mohan and Vikram handcuff Ravi on their balcony while they finalize the funeral arrangements, and when they return Ravi is no longer there. And then the killings begin...

==Characters==
* Akshay Kumar ... Ravi
* Madhoo ... Madhu
* Vishnuvardhan ... Inspector Mohan 
* Alok Nath ... Judge Somnath
* Ranjeet ... Ranjit
* Mohan Joshi ... Jaikaal
* Arun Bakshi ... Dr. Vikram

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Bombay Se Rail Chali"
| Anu Malik, Alisha Chinai
|-
| 2
| "Ai Ai Ai Sorry Sorry"
| Abhijeet, Alisha Chinai. 
|-
| 3
| "Mubarak Ho Mubarak Ho"
| Kumar Sanu, Alka Yagnik
|-
| 4
| "Mubarak Ho Mubarak Ho"
| Kumar Sanu
|-
| 5
| "Pehle Hi Qayama"
| Vinod Rathod , Alka Yagnik
|-
|}

==External links==
*  

 
 
 


 