Maneye Manthralaya
{{Infobox film 
| name           = Maneye Manthralaya
| image          =  
| caption        = 
| director       = H. R. Bhargava
| producer       = Anuradha Singh Dushyanth Singh Amritha Singh
| story          = 
| writer         = Chi. Udaya Shankar
| screenplay     = H. R. Bhargava Bharathi Jai Jagadish Mukhyamantri Chandru
| music          = M. Ranga Rao
| cinematography = D V Rajaram
| editing        = Goutham Raju
| studio         = Rohini Pictures
| distributor    = Rohini Pictures
| released       =  
| runtime        = 128 min
| country        = India Kannada
}}
 1986 Cinema Indian Kannada Kannada film, directed by H. R. Bhargava and produced by Anuradha Singh, Dushyanth Singh and Amritha Singh. The film stars Anant Nag, Bharathi Vishnuvardhan|Bharathi, Jai Jagadish and Mukhyamantri Chandru in lead roles. The film had musical score by M. Ranga Rao.  

==Cast==
 
*Anant Nag Bharathi
*Jai Jagadish
*Mukhyamantri Chandru
*Dingri Nagaraj
*Ramesh Aravind Tara
*Umashree
*Uma Shivakumar
*Shanthamma
*Nagesh
*Kavya Krishnamurthy
*Sangram Singh
*Jairaj Singh
*Pranaya Murthy
*Manchayya
*Mysore Lokesh
*Dayanand
*Janardhan
*Bemel Somanna
*Master Sanjay
*Master Chethan
*Baby Pramodhini
*Radha
*Sapna
 

==Soundtrack==
The music was composed by M. Ranga Rao. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Manjula || Chi. Udaya Shankar || 04.21
|- Manjula || Chi. Udaya Shankar || 04.05
|-
| 3 || Baarisu Baarisu || S. Janaki|Janaki, Ramesh || Chi. Udaya Shankar || 04.43
|-
| 4 || Maneye Baridayithu || S. Janaki|Janaki, K. J. Yesudas || Chi. Udaya Shankar || 03.58
|-
| 5 || Happy Birth Day || S. Janaki|Janaki, K. J. Yesudas || Chi. Udaya Shankar || 08.01
|}

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 


 