Bullet Scars
 
{{Infobox film
| name           = Bullet Scars
| image	=	Bullet Scars FilmPoster.jpeg
| caption        = Film poster
| director       = D. Ross Lederman
| producer       = 
| writer         = Sy Bartlett (story "False Faces") Charles Belden (story "False Faces") Robert E. Kent
| starring       = See below Howard Jackson William Lava  (uncredited) Heinz Roemheld  (uncredited)
| cinematography = Ted D. McCord
| editing        = James Gibbon
| studio         = 
| distributor    = 
| released       =  
| runtime        = 59 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Bullet Scars is a 1942 American film directed by D. Ross Lederman.

== Cast ==
*Regis Toomey as Dr. Steven Bishop
*Adele Longmire as Nora Madison
*Howard Da Silva as Frank Dillon
*Ben Welden as Pills Davis
*John Ridgely as Hank OConnor
*Frank Wilcox as Mike
*Tod Andrews as Joe Madison
*Hobart Bosworth as Dr. Sidney Carter
*Roland Drew as Jake
*Walter Brooke as Trooper Walter Leary
*Creighton Hale as Jess, the Druggist
*Hank Mann as Gilly
*Sol Gorss as Dude
*Don Turner as Mitch

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 
 