Umrika
 
Umrika is a 2015 Indian drama film directed and written by Prashant Nair and produced by Swati Shetty and Manish Mundra. The film premiered at the 2015 Sundance Film Festival, where it won the World Cinema Dramatic Audience Award.  Director Nair said that the film is "about the mythology of America and, more generally, how cultures perceive each other: the stereotypes, assumptions, misunderstandings and labeling as “exotic” of all things unfamiliar". 

==Plot==
The film begins with Udai (Prateik Babbar) leaving his village for the United States. Following the death of father, Udais younger brother Ramakant (Suraj Sharma) realises that the letters his family have been receiving from Udai have been forged by his father and uncle, and learns that Udai vanished when he reached his port city, Mumbai. He embarks upon a journey to locate his brother.

==Reception==

Kenneth Turan of The Los Angeles Times called Umrika a "warmly intelligent film"  {{cite web|last=Turan|first=Kenneth|title=
Sundance preview: Diverse lineup unspools in Park City|url=http://www.latimes.com/entertainment/movies/la-et-mn-sundance-curtain-raiser-20150122-column.html#page=1|website=Los Angeles Times}}  and included it in his 25 Films of note at Sundance 2015. 

Variety praised the film, giving kudos to "Nair’s soundly constructed script and deft handling of a very good cast."  

The Hollywood Reporter stated that Nair has managed to incorporate several big and abstract topics — including what ties us to our families and place of birth and the extent to which these things are important — into a story in which they become highly personal for the characters. 

Indiewire criticized the plot for being underwritten, and described the film as "ultimately a non sequitur story that, at worst, holds no weight, and, at best, makes little sense to someone born outside of Indian values and traditions." 

==References==
 

==External links==
*  on the Internet Movie Database

 
 
 
 
 
 


 