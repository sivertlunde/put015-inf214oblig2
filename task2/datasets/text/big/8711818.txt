The Next Man
{{Infobox film
| name           = The Next Man
| image          = TheNextManposter.jpg
| caption        = Original poster
| director       = Richard C. Sarafian
| producer       = Martin Bregman
| writer         = David M. Wolf Morton S. Fine Richard C. Sarafian Alan Trustman
| starring       = Sean Connery Adolfo Celi Cornelia Sharpe
| music          = Michael Kamen Michael Chapman
| editing        = Aram Avakian Robert Q. Lovett
| distributor    = Allied Artists Pictures Corporation
| released       =  
| runtime        = 108 min.
| country        = United States English
| budget         =
}}
The Next Man (also known as The Arab Conspiracy or Double Hit) is a 1976 American political action thriller film starring Sean Connery, Adolfo Celi, Cornelia Sharpe and Charles Cioffi. Critical reaction at its opening was not positive. Music for the film features New York guitarist Frederic Hand.

==Plot==
The film is set during the Arab oil embargo of 1976. Khalil Abdul-Muhsen (Connery), is the Saudi Arabian minister of state who proposes to recognize Israel, support Israeli membership in OPEC and sell Saudi oil to needy nations. The object of his plan is to protect third-world nations from the threat of Cold War ideology. Khalils radical agenda and idealism however finds few friends and he is soon the target of multiple assassination attempts by Arab terrorist groups.

They send Nicole Scott (Sharpe) to infiltrate Abdul-Muhsens entourage, seduce him and await further instructions. However, she develops strong feelings for him in reality and the completion of the plan is jeopardized.

==Cast==
* Sean Connery as Khalil Abdul-Muhsen
* Cornelia Sharpe as Nicole Scott
* Albert Paulsen as Hamid
* Adolfo Celi as Al Sharif
* Marco St. John as Justin
* Ted Beniades as Frank Dedario
* Charles Cioffi as Fouad Jaime Sánchez as New York security 

==Reception==
The film on its release was not received particularly well by critics. Roger Ebert on reviewing the film on November 17, 1976 criticized the understanding of the plot remarking that "When good directors work with bad material, Pauline Kael once said, what happens is that they shove art into the crevices of dreck. That would do as a description of "The Next Man," a movie with an impenetrable plot".  , November 17, 1976, Retrieved on July 7, 2008 

However he commented that the film has some scenes that worked and were positive, particularly those between Sean Connery, as the minister of state for Saudi Arabia, and Cornelia Sharpe, who plays a professional international assassin, although the plot lacked details to understand these characters more fully.  Ebert was most impressed by Cornelia Sharpes performance as Nicole Scott in which she plays an attractive character that Connery can easily fall in love with, describing her as a "cool beauty". 

Vincent Canby of The New York Times described the film as a "suspense melodrama made by people whose talent for filmmaking and knowledge of international affairs would both fit comfortably into the left nostril of a small bee".    Like Ebert, he identified flaws in the plot understanding, remarking that "The Next Man is obsessed with political assassination but it never really identifies its villains, preferring, instead, to cop out by playing on natural paranoia that assumes that everyone everywhere is on the take from someone somewhere. This attitude is too easy to represent true cynicism".  He also criticised the movement of locations in the film which added to the confusing nature of the plot commenting "The Next Man moves rootlessly around the world like a fretful tourist, from New York to the Middle East, the south of France, London, Ireland, Bavaria, and the Bahamas, though nothing much happens in any one of these places that couldnt as easily happen somewhere else". 

Variety (magazine)|Variety were not impressed by the film either commenting that "The Next Man emerges more a slick travesty with political overtones than the cynical suspense meller it was designed to be". 

==References==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 
 