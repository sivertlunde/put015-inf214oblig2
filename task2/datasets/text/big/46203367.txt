Big Business (1930 film)
{{Infobox film
| name = Big Business 
| image =
| image_size =
| caption =
| director = Oscar M. Sheridan
| producer =Oscar M. Sheridan 
| writer =  Hubert W. David = Oscar M. Sheridan
| narrator = Anthony Ireland
| music = Oscar M. Sheridan   H.W. David
| cinematography = Basil Emmott 
| editing = 
| studio = Oscar M. Sheridan Productions 
| distributor = Fox Film Company
| released = September 1930
| runtime = 76 minutes
| country = United Kingdom English
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Big Business is a 1930 British musical film directed by Oscar M. Sheridan and starring Frances Day, Barrie Oliver and Virginia Vaughan. It was made at the Twickenham Film Studios in London. 

==Cast==
*   Frances Day as Pamela Fenchurch 
* Barrie Oliver as Barnie  
* Virginia Vaughan as Kay   Anthony Ireland as Jimmy 
* Ben Welden as Fenchurch 
* Jimmy Godden as Oppenheimer  
* Billy Fry as Augustus  
* Lewis Keezing as Miggs
* Leslie Hutch Hutchinson as Pianist  
* Elsie Percival
* Arthur Roseberry as Himself - Orchestra Leader  

==References==
 

==Bibliography==
*Low, Rachael. Filmmaking in 1930s Britain. George Allen & Unwin, 1985.
*Wood, Linda. British Films, 1927–1939. British Film Institute, 1986.

==External links==
* 

 
 
 
 
 
 
 
 

 