Be My Wife (1921 film)
 
{{Infobox film
| name = Be My Wife
| image =
| caption =
| director = Max Linder
| producer = Max Linder
| writer = Max Linder
| starring = Max Linder
| cinematography = Charles Van Enger
| editing =
| distributor = Goldwyn Pictures
| released =  
| runtime = 57 minutes  English (Original intertitles)
| country = United States
| budget =
}}
 1921 silent silent comedy film written, directed and starring Max Linder.

== Plot summary ==
Max and Mary are in love, but Marys Aunt Agatha dislikes Max, and instead prefers the unappealing Simon. So Max has to resort to a series of ruses to try to get Simon out of the way, and to be able to spend time with Mary.

Finally, Max comes up with a scheme that might allow him to prove to Aunt Agatha that he is more worthy than Simon.

==Cast==
* Max Linder – Max, the Fiancé
* Alta Allen – Mary, the Girl
* Caroline Rankin – Aunt Agatha
* Lincoln Stedman – Archie
* Rose Dione – Madame Coralie
* Charles McHugh – Mr. Madame Coralie
* Viora Daniel – Mrs. Du Pont
* Arthur Clayton  – Mr. Du Pont

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 


 