Little Man, What Now? (film)
 
{{Infobox film
| name           = Little Man, What Now?
| image          = Little-man-what-now-1934.jpg 
| caption        = Film poster
| director       = Frank Borzage
| producer       = Carl Laemmle Jr.
| writer         = 
| screenplay     = William Anthony McGuire
| story          = 
| based on       =  
| narrator       = 
| starring       = Margaret Sullavan Douglass Montgomery Arthur Kay (uncredited)
| cinematography = Norbert F. Brodin
| editing        = Milton Carruth Universal Pictures - Studio
| distributor    = Universal Pictures - Theatrical Distributor
| released       =  
| runtime        = 98 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 same name by Hans Fallada.

==Plot==
Germany in the 1930s, a young couple struggling against poverty. Hans is a small business agent. He is happily married to Emma that he affectionately calls Lämmchen (small lamb). They must keep their marriage a secret in order for Hans to keep his job, as his boss wants him to marry his daughter. Hans loses his job when the truth comes out. They move in with his stepmother in bustling Berlin to find success. Hans gets a small job in a department store. Things are okay until they find out that his stepmother is really a notorious madame and runs an exclusive brothel.

==Cast==
* Margaret Sullavan as Emma Lämmchen Pinneberg
* Douglass Montgomery as Hans Pinneberg Alan Hale as Holger Jachman
* Catherine Doucet as Mia Pinneberg
* DeWitt Jennings as Emil Kleinholz
* G. P. Huntley, Jr. as Herr Heilbutt
* Muriel Kirkland as Marie Kleinholz
* Fred Kohler as Karl Goebbler
* Mae Marsh as Wife of Karl Goebbler
* Donald Haines as Emil Kleinholz Jr.
* Christian Rub as Herr Puttbreese
* Alan Mowbray as Franz Schluter

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 