LD 50 Lethal Dose
 
{{Infobox film
| name           = LD 50 Lethal Dose
| image          = Movie poster of the movie LD 50 Lethal Dose 2003.jpg
| image_size     = 
| caption        = DVD cover
| director       = Simon De Selva
| producer       = Alistair MacLean-Clark Basil Stephens
| writer         = Matthew McGuchan
| screenplay     = 
| starring       = Katharine Towne Melanie Brown Tom Hardy Ross McCall Michael Price
| cinematography = Robin Vidgeon
| editing        = Kant Pan
| studio         = Four Horsemen Films Isle of Man Film Crossfire Productions Buena Vista Touchstone
| released       =  
| runtime        = 97 minutes
| country        = United Kingdom
| language       = English
| budget         = $8 million
| gross          = 
}}

LD 50 Lethal Dose (aka "Lethal Dose" or "LD50") is a 2003 horror film directed by Simon De Selva, produced by Alistair MacLean-Clark and Basil Stephens and written by Matthew McGuchan. A group of animal rights activists set off to free an imprisoned colleague from a terrifying ordeal but their rescue mission turns into a series of twisted and mind bending incidents. Starring Tom Hardy, Katharine Towne and Melanie Brown.

==Plot==
Animal activists break into an animal research facility, when Gary (McCall) gets caught in a bear trap. Unable to free Gary, the rest of the group flees, leaving Gary to take the blame. A year later, the group has since disbanded until an encrypted e-mail from Gary arrives asking for help. Danny (Bill), who has been visiting Gary in prison, tells the group that Gary has traded his body for experiments in exchange for a reduced sentence.
 stoner and subterranean level full of labyrinth hallways and experiment chambers. They also discover that a powerful electrical force is stalking them, and it has full control of the complex. The force plays havoc with the would-be rescuers, turning their righteous cause into a death trap. They must work together to outsmart and defeat the unseen evil.  

==Cast==
*Katharine Towne as Helen
*Melanie Brown as Louise
*Tom Hardy as Matt
*Ross McCall as Gary
*Toby Fisher as Justin
*Leo Bill as Danny
*Philip Winchester as Vaughn
*Stephen Lord as Spook 

==Production==
Filmed on location in London, England and Isle of Man. Filming started on October 27, 2002 and went until January 31, 2003.

==References==
 

==External links==
*  
* 

 
 
 
 
 