Piano Mover
{{Infobox Hollywood cartoon|
| cartoon_name = Piano Mover
| series = Krazy Kat
| image = Krazy in Piano Mover.jpg
| caption = Screenshot Krazy Kat (left), the spaniel (right)
| director = Manny Gould Ben Harrison
| story_artist = Manny Gould 
| animator = Allen Rose
| voice_actor = 
| musician = Joe de Nat
| producer = Charles Mintz
| distributor = Columbia Pictures
| release_date = January 4, 1932
| color_process = Black and white
| runtime = 6:27 English
| preceded_by = Taken for a Ride
| followed_by = Love Krazy
}}

Piano Mover is a short animated cartoon released by Columbia Pictures, starring Krazy Kat.

==Plot==
Krazy is driving a truck, delivering a piano to someone. The place of his delivery is an enormously tall condominium. Because the piano could not fit through the small doors, Krazy has no choice but to pass it through a window. To do so, his fellow deliverer lifts the instrument with a crane. While he stands on the elevating load, the feline has to deal with his acrophobia as the targeted window is in one of the high floors. He also has to handle some hostile residents.

Following some trouble going up and down, Krazy finally reaches the window of his recipient. The recipient is none other than Krazys spaniel girlfriend who pops out of the window. Overjoyed in getting her delivery, the spaniel plays the piano and sings a song. She then dances on the balcony, and Krazy joins her. They are dancing so merrily that they become oblivious to where they are. In this, the spaniel suddenly stumbles off the edge, pulling Krazy with her. Fortunately, the canine girl grabs hold of the piano while her boyfriend hangs onto her legs. When they got themselves on top of the instrument, Krazy and the spaniel attempt to go down safely with it. But their troubles arent over when a hostile bird bites the rope carrying the piano. The musical keyboard then plummets into the sidewalk along with the couple. On the wreckage, Krazy and the spaniel are dazed but unhurt.

==Availability==
The Krazy Kat shorts from the Columbia era were released for television by Samba Productions and Screen Gems. For home video, they were distributed by Excel Home Movies, Official Films, and Columbia Home Movies. The cartoon Piano Mover is available in the Columbia Cartoon Collection: Volume 2. {{cite web
|url=http://theshortsdepartment.webs.com/columbiacartoons.htm
|title=The Columbia Cartoons
|accessdate=2012-06-17
|publisher=the shorts development
}} 

==References==
 

==External links==
*  at the Big Cartoon Database

 

 
 
 
 
 
 
 
 
 


 