Zhizn i priklyucheniya chetyrekh druzei 3/4
{{Infobox Film
| name = Zhizn i priklyucheniya chetyrekh druzei 3/4
| originalname = Жизнь и приключения четырех друзей 3/4 
| director = Oleg Yeryshev
| writer = Yusef Printsev
| starring = Katya Kishmereshkina   Petr Shelokhonov   Boris Sokolov Mikhail Svetin   Yefim Kamenetsky   Lev Lemke
| music = Gennadi Gladkov Igor Tsvetkov
| cinematography = Roman Chernyak
| editing = G. Saidakovskaya
| distributor = -Russia- Ochakovo Film - non-Russia - Enio Film 1981
| runtime = 60 min.
| country = Soviet Union Russian
}}
 1981 USSR|Russian directed by Oleg Yeryshev. This is a second film in the popular family series about the adventures of three dogs and one cat, who are following their owner and are having lots of fun together. Film runs 60 minutes, and has two parts: 3. Igra s ognyom (aka.. Playing games with fire), and 4. Kot v meshke (aka.. Cat in the bag).

== Synopsis ==
  where Christmas trees are grown. There four animals help people to stop the fire and save Christmas trees. Then, together with their owner, the four friends join children for celebration of the New Year.

== Production ==
* Filming dates 1980 - 1981
* Filming locations: Lentelefilm studios, Leningrad, Leningrad oblast, Russia.
* Five additional animal tamers took part in filming.
* Three dogs and one cat were selected for filming after auditioning hundreds of dogs and cats with their owners.
* Dogs and cat "improvised" a lot before the camera, so many takes were made for each scene before getting good results for congruent editing.
* Dogs and cat made some unexpected moves during the filming, so the writer, Yusef Printsev, had to create additional lines for human actors and for translator of the animals thoughts (Lev Lemke).

==Release== Central Television, as well as in theatres in Leningrad, Moscow, and across Russia, as a sequel to Zhizn i priklyucheniya chetyrekh druzei 1/2. Both films also ran in theatres across the former Soviet Union and in Eastern Europe. In 1994 the film was re-edited in St. Petersburg, Russia for video release. That same year the film was released on VHS and DVD, having several editions during the 1990s, and has been a popular family entertainment for all ages.

* The film and its prequel were also released in Germany in the 2000s (decade) under the title "Die Abenteuer der vier Strolche (1/2)" and the sequel "Die Abenteuer der vier Strolche (3/4)."

* This film is the second in a series of 8 episodes made between 1980 and 1994. Each episode has a different plot and variations in the cast of actors and crew, but with the same three dogs remaining in the first 4 episodes. The first 4 episodes are paired (1/2 and 3/4) and edited as two movies for video release, now available on VHS and DVD in Europe and in the countries of the former USSR. It remains a popular family movie for all ages.

== Interesting Fact ==
* This is a second film in the popular Russian childrens series about three dogs and a cat becoming friends. In real life, three dogs, named Toshka, Bubrik and Fram, and a cat Svetofor became really friendly and cooperative with actors and director in the course of filming. Owners of dogs and cat also became extras in the film.

* Cat Vaska was too independent, and eventually walked away from the project. New cat, named Barsik, was cast as "Cat Svetofor" and remained in the cast for parts 3 and 4.

==Main cast==
* Katya Kishmereshkina
* Petr Shelokhonov
* Tatyana Samarina
* Boris Sokolov
* Mikhail Svetin
* Yefim Kamenetsky
* Lev Lemke - as Translator of animals thoughts.
* Vladimir Bobin
* Valeri Bychenkov
* Valeri Doronin
* Andrei Khilko
* Mikhail Matveyev
* Yevgeni Tilicheyev
* Nikita Yeryshev

==Animal actors==
* Cat Svetofor - played by cat Barsik (no owner mentioned)
* Dog Bubrik - played by terriere Chingiz (dog owner - E. Sokolova)
* Dog Fram  - played by shepherd Yanko (dog owner - L. Ostretsova)
* Dog Toshka - as herself (dog owner - S. Pavlova)

==Crew==
* Director: Oleg Yeryshev
* Production director: Yu. Goncharov
* Writers:    Yusef Printsev, Oleg Yeryshev
* Cinematographers: Roman Chernyak, V. Ivanets
* Composers: Gennadi Gladkov, Igor Tsvetkov
* Art director: Konstantin Dmitrakov
* Editing: G. Saidakovskaya, L. Burtsova
* Second unit director: A. Ratnikov
* Sound: Iosif Minkov
* Music editor: A. Aristov
* Light: A. Zakharov, L. Yudina
* Animal trainers: L. Ostretsova, S. Pavlova, E. Sokolova.
* Production managers: E. Grigorenko, O. Krakhovskaya, E. Snyatovskaya

==Production Company==
* Gosteleradio
* Lentelefilm
* Ochakovo-Films

==Distributors==

* Gosteleradio (all formats, from 1981–1991) in the Soviet Union.
* Ochakovo-Films (all formats, after 1992) in Russia.
* Enio Film (after 1994, VHS video) (non-Russia, non-USA).

== External links ==
 
*  
* Zhizn i priklyucheniya chetyrekh druzei in German "Die Abenteuer der vier Strolche"   (German)
* Cast and synopsis for "Жизнь и приключения четырех друзей" (1980)   (Russian)
* Cast, crew, plot and other data for "ЖИЗНЬ И ПРИКЛЮЧЕНИЯ ЧЕТЫРЕХ ДРУЗЕЙ"   (Russian)
* Zhizn i priklyucheniya chetyrekh druzei/Жизнь и приключения четырех друзей on Film.ru   (Russian)

 
 
 
 
 
 
 
 
 
 
 