Manhandled
{{Infobox film
| name           = Manhandled
| image          = Manhandled movie poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Lewis R. Foster
| producer       = {{plainlist|
* William H. Pine
* William C. Thomas
}}
| screenplay     = {{plainlist|
* Whitman Chambers
* Lewis R. Foster
}}
| story          = 
| based on       =  
| starring       = {{plainlist|
* Dorothy Lamour
* Sterling Hayden
}}
| cinematography = Ernest Laszlo
| music          = Darrell Calker
| editing        = Howard A. Smith
| studio         = Pine-Thomas Productions
| distributor    = Paramount Pictures
| released       =  
| runtime        = 97 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Manhandled is a 1949 film noir directed by Lewis R. Foster, and starring Dorothy Lamour, Dan Duryea, and Sterling Hayden, and based on the 1945 novel The Man Who Stole a Dream by L. S. Goldsmith.

==Plot==
Struggling writer Alton Bennet explains to psychiatrist Dr. Redman how he has nightmares about murdering his wealthy wife, Ruth, who owns very valuable jewels.

A private eye, Karl Benson, steals the office keys of Redmans private secretary, Merl Kramer, who is then framed after Ruth is found murdered. Karl has planted some of the dead womans jewels in Merls apartment.

Insurance investigator Joe Cooper is on the case, along with the police. Karl is confronted by Dr. Redman, who confesses that it was he who murdered Ruth, his own patient. Karl happened upon the murder scene, knocked out Redman and stole the jewels.

Redman proposes a split, but Karl kills him. He then knocks Merl unconscious and intends to throw her from a roof, but her cries alert police and Joe, who come to her rescue.

==Cast==
* Dorothy Lamour as Merl Kramer
* Sterling Hayden as Joe Cooper
* Dan Duryea as Karl Benson
* Irene Hervey as Ruth, Mrs. Alton Bennet
* Phillip Reed as Guy Bayard
* Harold Vermilyea as Dr. Redmond
* Alan Napier as Alton Bennet Art Smith as Detective Lt. Bill Dawson
* Irving Bacon as Sgt. Fayle

==Reception==
Audiences and critics found the plot too confusing, and the film was not popular on its release.  Lisa Mateas of Turner Classic Movies said that "contemporary audiences ... will find that Manhandled does not disappoint". 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 