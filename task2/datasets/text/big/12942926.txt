Avenging Bill
 
{{Infobox film
| name           = Avenging Bill
| image          =
| image size     =
| caption        =
| director       = John A. Murphy
| producer       = Arthur Hotaling
| writer         = E.W. Sargent
| narrator       =
| starring       = Oliver Hardy
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        =
| country        = United States English intertitles
| budget         =
}}

Avenging Bill is a 1915 American film featuring Oliver Hardy.

==Cast==
* Mabel Paige - Lucy
* Royal Byron - Mr. Grouch
* Eloise Willard - Mrs. Grouch
* Oliver Hardy - Bill, the Grocers Boy (as Babe Hardy)

==See also==
* List of American films of 1915
* Filmography of Oliver Hardy

==External links==
* 

 
 
 
 
 
 
 
 