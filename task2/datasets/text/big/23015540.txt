Prince Nezha's Triumph Against Dragon King
 
{{Infobox film
| name           = Prince Nezhas Triumph Against Dragon King
| image          = 
| caption        = 
| director       = Yan Dingxian Wang Shuchen Xu Jingda
| producer       = 
| writer         = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 65 minutes
| country        = China
| language       = Mandarin Chinese
| budget         = 
}}
Prince Nezhas Triumph Against Dragon King ( , and also known as Nezha Conquers the Dragon King) is a 1979 Chinese animated film. It was screened out of competition at the 1980 Cannes Film Festival.   

==Plot==
The film is an adaptation of a story in Chinese mythology (in particular, the epic fantasy novel Fengshen Bang) about the warrior deity Nezha (deity)|Nezha, who became a student of the immortal Taiyi Zhenren. The main part of the story revolves around Nezhas feud with Ao Guang, the Dragon King of the Eastern Sea, whom he angers through the killing of Ao Bing, Ao Guangs third son. Through bravery and wit, Nezha finally breaks into the Dragon Kings underwater palace and successfully defeats him.

==Japanese version cast==
* Banjô Ginga as Li Gen (voice: Japanese version)
* Daisuke Gôri as Li Jing - Nezhas father (voice: Japanese version)
* Junko Hori as Little boy 1 (voice: Japanese version)
* Ichirô Nagai as Master Taiyi / Li Jings chancellor (voice: Japanese version)
* Masako Nozawa as Nizha (voice: Japanese version)
* Nachi Nozawa as Ao Guangs chancellor (voice: Japanese version)
* Mari Okamoto as Little girl (voice: Japanese version)
* Chikao Ōtsuka as Dragon King (voice: Japanese version)
* Tomiko Suzuki as Little boy 2 (voice: Japanese version)
* Norio Wakamoto as Ao Bing (voice: Japanese version)

==Other appearances==
 On 30 May 2014, Nezha Conquers the Dragon King was featured on the Google search engines homepage as an animated Google Doodle|doodle.  

==See also==
* Nezha (deity)
*  

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 