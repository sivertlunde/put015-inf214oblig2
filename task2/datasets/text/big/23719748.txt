Sofia (film)
{{Infobox film
| name           = Sofia
| image          =
| image size     =
| caption        =
| director       = Alejandro Doria
| producer       = Diana Frey Miguel Rodríguez
| starring       = Dora Baret
| music          =
| cinematography = Miguel Rodríguez
| editing        = Silvia Ripoll
| distributor    =
| released       = 16 April 1987
| runtime        = 98 minutes
| country        = Argentina
| language       = Spanish
| budget         =
}}

Sofia is a 1987 Argentine drama film directed by Alejandro Doria. It was screened in the Un Certain Regard section at the 1987 Cannes Film Festival.   

==Cast==
* Dora Baret - Sofía
* Héctor Alterio - Pedros father
* Graciela Dufau - Pedros mother
* Alejandro Milrud - Pedro
* Nicolas Frei - Silvio Núñez
* Alberto Busaid - Man in train station
* Lito Cruz - Pimp
* Mónica Villa - Prostitute
* Rafael Rodríguez
* Ana Sadi
* Damian Canavezzio
* Marcelo Serre
* Fabián Gianola
* Walter Peña

==References==
 

==External links==
* 

 
 
 
 
 
 
 