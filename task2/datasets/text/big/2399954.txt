The Scarlet and the Black
 

{{Infobox film|
| name           = The Scarlet and the Black
| image          = TheScarletandtheBlack.jpg
| producer       = Bill McCutchen
| director       = Jerry London
| writer         = J.P. Gallagher (novel)
| based on       = The Scarlet Pimpernel of the Vatican David Butler
| starring       = Gregory Peck Christopher Plummer John Gielgud Barbara Bouchet Fabiana Udenio
| music          = Ennio Morricone
| cinematography = Giuseppe Rotunno
| editing        = Benjamin A. Weissman
| studio         = ITC Entertainment
| distributor    = Columbia Broadcasting System
| released       = February 2, 1983
| runtime        = 143 min. English
| budget         = 
}}
 Scarlet and Black, which starred Ewan McGregor and Rachel Weisz.

Based on J. P. Gallaghers book The Scarlet Pimpernel of the Vatican (published in 1967), this movie tells the story of Monsignor Hugh OFlaherty, a real life Irish-born Roman Catholic priest who saved thousands of Jews and escaped Allied POWs in Rome. It was directed by Jerry London.
 bishops in the Roman Catholic Church, but also to the dominant colors of Nazi Party regalia.

==Plot==
In 1943, the Nazi military occupies Rome. Pope Pius XII (John Gielgud) is approached by General Max Helm and SS Head of Police for Rome Lieutenant Colonel Herbert Kappler (Christopher Plummer). The Colonel expresses concern that escaped Allied prisoners may attempt to seek refuge in the Vatican, and requests permission to paint a white line across St. Peters Square in order to mark the extent of Vatican sovereignty. The Pope grants his permission, and when the SS officers leave, he sees out of the window that the white line has already begun to be painted.

Kapplers main antagonist is Monsignor OFlaherty ( .

Met with continuous failure, Kappler begins to develop a personal vendetta against OFlaherty. Despite OFlahertys efforts, Kappler manages to recapture many escaped POWs, deport many Jews to death camps, and exploit and oppress the general population; a number of OFlahertys friends are also arrested or killed. OFlaherty is himself the target of an assassination attempt instigated by Kappler, which fortunately fails due to the monsignors boxing skills. The rescue organization continues operating, and succeeds in saving many lives.

As the war progresses, the Allies succeed in landing in Italy and begin to overcome German resistance, eventually breaking through and heading towards Rome itself. Colonel Kappler worries for his familys safety from vengeful partisans, and, in a one-to-one meeting with OFlaherty, asks him to save his family, appealing to the same values that motivated OFlaherty to save so many others. The Monsignor, however, refuses, refusing to believe that, after all the Colonel has done and all the atrocities he is responsible for, he can expect mercy and forgiveness automatically, simply because he asks for it, and walks away in disgust.

As the Allies enter Rome in June, 1944, Monsignor OFlaherty joins in the celebration of the liberation, and somberly toasts those who did not live to see it. Although the Pope had officially cautioned OFlaherty about his activities, on the day of the liberation he bestows his personal blessing upon the Monsignor, who then goes into a chapel to pray.

Kappler is captured in 1945 and questioned by the Allies. In the course of his interrogation, he is informed that his wife and children were smuggled out of Italy and escaped unharmed into Switzerland. Upon being asked who helped them, Kappler realizes who it must have been, but responds simply that he does not know.

The film epilogue states that OFlaherty was decorated by several Allied governments after the war. Kappler was sentenced to life imprisonment, but was frequently visited in prison by OFlaherty, his only regular visitor. Eventually, the former SD officer converted to the Roman Catholic faith, and was baptized by the Monsignor in 1959.

==Actors==
Vatican Officials
* Gregory Peck as Monsignor Hugh OFlaherty
* John Gielgud as Pope Pius XII 
* Raf Vallone as Father Vittorio
* Angelo Infanti as Father Morosini
* Marne Maitland as Papal Secretary
* Stelio Candelli as OFlahertys Secretary
* Gabriella DOlive as Mother Superior

SS Personnel
* Christopher Plummer as SS-Obersturmbannführer Herbert Kappler
* Kenneth Colley as SS-Hauptsturmführer Hirsch (Erich Priebke) (as Ken Colley)
* Walter Gotell as SS-Obergruppenführer Max Helm (Karl Wolff) Michael Byrne as Reinhard Beck
* T. P. McKenna as Reichsführer Heinrich Himmler
* David Brandon as SS officer

Allied Personnel
 John Terry as Lt. Jack Manning
* Phillip Hatton as Lt. Harry Barnett
* Mark Lewis as Cpl. Les Tate
* William Berger as U.S. Intelligence Officer (as Bill Berger)
* Edmund Purdom as British Intelligence Officer / Epilogue Narrator (as Edmond Purdom)

Civilians
* Barbara Bouchet as Minna Kappler
* Julian Holloway as Alfred West (John May)
* Olga Karlatos as Francesca Lombardo (Chetta Chevalier)
* Vernon Dobtcheff as Count Langenthal
* Peter Burton as Sir DArcy Osborne
*Fabiana Udenio as Guila Lombardo
* Remo Remotti as Rabbi Leoni
* Giovanni Crippa as Simon Weiss
* Billy Boyle as Paddy Doyle
* Itaco Nardulli as Franz Kappler
* Cariddi Nardulli as Liesel Kappler (as Carridi Nardulli)
* Alessandra Cozzo as Emilia Lombardo
* Cesarina Tacconi as Pregnant Woman
* Sergio Nicolai as Firing Squad Officer
* Bruno Corazzari as Coalman
* Francesco Carnelutti as Cameriere Segreto

==Historical accuracy== Supreme SS and Police Leader of Italy.  The film was unable to use Wolffs real name, since the SS General was still living when the film was in production; he died in 1984.

Actor Christopher Plummer was 53 years old during the production of the film.  Herbert Kappler was only 36 when he served as SS Security Chief in Rome.

Monsignor Hugh OFlaherty was a real Irish-born priest and Vatican official, credited with saving 6,500 Jews and Allied war prisoners.

Herbert Kappler was sentenced to life imprisonment, and did convert to Roman Catholicism in 1959, partly under the influence of his war-time opponent Hugh OFlaherty, who visited Kappler in prison every month, discussing religion and literature with him. He was eventually transferred to a prison hospital on account of poor health. It was there that he escaped imprisonment by being smuggled out in a suitcase by his wife (Kappler weighed less than 105 pounds at the time). He escaped to West Germany, where he eventually died at age 70 in 1978.

==Awards==
In 1983 The Scarlet and the Black was nominated for an Emmy in the category Outstanding Film Editing for a Limited Series or a Special.

==References==
 	

==External links==
* 
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 