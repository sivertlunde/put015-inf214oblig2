Barood (2010 film)
{{Infobox film
| name           = Barood : (The Fire) - A Love Story
| image          =
| director       = Jagdish C. Pandey
| producer       = Sadhan Kumar Das Ulhas Mhatre
| screenplay     = Chintoo Bihani
| story          = 
| starring       = Sunil Shetty Shilpa Shirodkar
| music          = Bhoopi Ratan Kanak Raj
| cinematography = Gautam Bose  
| editing        = Deepak Joul
| studio         =
| distributor    = 
| runtime        = 134 mins
| country        = India
| language       = Hindi
| released       = November 12, 2010
| budget         = 
| gross          = 
}}
Barood : (The Fire) - A Love Story is a 2010 Bollywood action film directed by Jagdish C. Pandey. It Stars Sunil Shetty and Shilpa Shirodkar.

==Cast==
* Sunil Shetty as Shakti
* Shilpa Shirodkar as Sona
* Mukesh Khanna as Abhay Kumar
* Kiran Kumar as Ajmaira
* Anant Mahadevan as Amrit Vohra
* Shiva Rindani
* Moon Moon Sen
* Raju Srivastav

==Goofs==
Sunil Shetty and Shilpa Shirodkar play the lead pair but the they look a lot younger than their actual age in 2010. Sunil Shetty and Shilpa Shirodkar voice is apparently dubbed by some artists.

==References==
 

==External links==
*  

 
 
 


 