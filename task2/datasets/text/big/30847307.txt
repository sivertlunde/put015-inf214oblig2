White Woman
 
{{Infobox Film
| name           = White Woman
| image_size     = 
| image	=	White Woman FilmPoster.jpeg
| caption        =  Stuart Walker
| producer       = E. Lloyd Sheldon (uncredited) Frank Butler (play) Samuel Hoffenstein Gladys Lehman
| narrator       = 
| starring       = Carole Lombard Charles Laughton Charles Bickford
| music          = 
| cinematography = 
| editing        = 
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       = November 10, 1933
| runtime        = 68 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} Stuart Walker Frank Butler. 

One of hundreds of Paramount films held in limbo by Universal Studios. Universal gained ownership of Paramount features produced between 1929 and 1949.
 Kurt Neumann. 

==Cast==
*Carole Lombard as Judith Denning
*Charles Laughton as Horace H. Prin
*Charles Bickford as Ballister
*Kent Taylor as David von Elst
*Percy Kilbride as Jakey James Bell as Hambly Charles Middleton as Fenton (as Charles B. Middleton) Claude King as C. M. Chisholm
*Ethel Griffies as Mrs. Chisholm
*James Dime as Vaegi
*Marc Lawrence as Connors

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 


 