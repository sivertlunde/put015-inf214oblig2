The Sea (2013 film)
 
 
{{Infobox film
| name = The Sea
| image = the-sea-poster.jpg
| caption = Promotional Poster
| alt =
| director = Stephen Brown	 	
| producer = David Collins Michael Robinson Luc Roeg
| screenplay = John Banville
| based on = novel The Sea (novel)|The Sea by John Banville	
| starring = Rufus Sewell Natascha McElhone Ciarán Hinds Sinéad Cusack Bonnie Wright
| music = Andrew Hewitt
| cinematography = John Conroy
| editing = Stephen OConnell	
| studio = Rooks Nest Entertainment Samson Films
| distributor = Independent
| released =   
| runtime = 87 minutes 
| country = Ireland United Kingdom
| language = English
| budget =
| gross =
}} novel of the same name by John Banville, who also wrote screenplay for the film.    The film premiered in competition at Edinburgh International Film Festival on 23 June 2013.    The film had its North American premiere at the 2013 Toronto International Film Festival.  

==Plot==
The story of a man who returns to the sea where he spent his childhood summers in search of peace following the death of his wife.

==Cast==
*Rufus Sewell as Carlo Grace
*Natascha McElhone as Connie Grace
*Ciarán Hinds as Max Morden
*Sinéad Cusack as Anna Morden
*Bonnie Wright as Rose
*Charlotte Rampling as Miss Vavasour
*Ruth Bradley as Claire
*Karl Johnson as Blunden
*Mark Huberman as Jerome
*Missy Keating as Chloe
*Amy Molloy as Shop Girl (Sadie)
*Stephen Cromwell as Young Tough (Mick)

==Production==
Producer of the film Luc Roeg said that "Ive wanted to make a film of John Banvilles haunting and soulful novel for several years and its been worth the wait. Im excited to introduce a new film maker, Stephen Brown, to world cinema and I couldnt be more delighted with the cast and crew weve assembled together with our producing partners at Samson Films." 

Filming started in September 2012 and finished by the January 2013.  

==Reception==
The sea premiered at the 2013 Edinburgh International Film Festival and received mixed reviews. Rating it at 7/10,the Screenkicker website said "intimate, superbly acted meditation on grief and abandonment that will make you think about how we cope with tragedy".  Marc Adams, chief film critic of Screen Daily wrote, "the film’s emotional still waters run deep and the film is gently watchable as a series of fine actors deliver nuanced and powerful performances."  Guy Lodge from Variety wrote "This good, middlebrow adaptation of John Banvilles Booker Prize-novel sacrifices structural intricacy for Masterpiece-style emotional accessibility." And added "Afforded the least, but most searing, screen time are Anna’s final days, which economically imply longer-running problems in Max’s marriage. In a uniformly strong cast, a superbly terse Cusack cuts that little bit deeper as a dying woman who understandably has no time for her husband’s hovering pain."  

Local response was less favourable. Niki Boyle of Film List a Scottish web magazine gave the film two out of five stars and said that "Hinds and Rampling are suitably low-key, and character actor Karl Johnson puts in a decent turn as a more poignant version of The Major from Fawlty Towers, but the whole thing feels utterly derivative, from the contrast between the muted-palette and light-saturated flashbacks, to the spare, mournful piano-and-violin score."  Rob Dickie of "Sound on Sight", praised the performance of cast but criticise the pace and climax of the film by saying that " the pace is lethargic, there are no surprising revelations and the ending is horribly anticlimactic, meaning the strong performances and flashes of visual flair go to waste."  Ross Miller of Thoughts on Film gave it 1 out of 5 stars, saying that, "What could have been a fascinating and melancholic look at memory, regret and loss is actually a boring and monotonous character drama... a pretentious mess thats a chore to sit through."  Emma Thrower of The Hollywood News also gave film a negative review by saying that "A frustrating blend of wooden and naturalistic, it is a surprise to realise author John Banville is responsible for a screenplay that often unfolds like an overblown television drama. Rufus Sewell and Bonnie Wright also suffer in these laborious and often unwelcome instagram-filtered interludes, Sewell an incongruous pantomime villain and Wright an underused but ultimately ineffective screen presence." 

The sea also served as the closing film at "25th Galway Film Fleadh", at 14 July 2013.   IconCinema listed the sea at its Top 200 most anticipated films of 2013. 

==Accolades==

 
{| class="wikitable sortable"
|-
! Year
! Award
! Category
! Recipient
! Result
|-
| 2013
| Edinburgh International Film Festival
| Audience Award Nominee
|
|   
|}
 

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 