Camouflage (1977 film)
 
{{Infobox film
| name           = Camouflage
| image          = 
| caption        = 
| director       = Krzysztof Zanussi
| producer       = 
| writer         = Krzysztof Zanussi
| starring       = Piotr Garlicki
| music          = 
| cinematography = Edward Klosinski
| editing        = 
| distributor    = 
| released       =  
| runtime        = 106 minutes
| country        = Poland
| language       = Polish
| budget         = 
}}
 Best Foreign Language Film at the 50th Academy Awards, but was not accepted as a nominee. 

==Cast==
* Piotr Garlicki as Jaroslaw Kruszynski
* Zbigniew Zapasiewicz as Jakub Szelestowski
* Christine Paul-Podlasky as Nelly Livington-Pawluk (as Christine Paul)
* Mariusz Dmochowski as Vice Dean
* Wojciech Alaborski as Kiszewski
* Mieczyslaw Banasik as Jozef
* Krystyna Bigelmajer as Zofia
* Jadwiga Colonna-Walewska as Deanery Manager
* Alfred Freudenheim as Official
* Marian Glinka as Resort Manager
* Hanna Grzeszczak as Girl in Kitchen
* Iwona Sloczynska as Ania

==See also==
* List of submissions to the 50th Academy Awards for Best Foreign Language Film
* List of Polish submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 