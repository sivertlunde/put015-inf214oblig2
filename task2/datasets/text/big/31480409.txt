Love Comes Lately
{{Infobox film
| name           = Love Comes Lately
| image          = Love Comes Lately.jpg
| alt            = 
| caption        = DVD cover
| director       = Jan Schütte
| producer       = Alex Gibney Martin Hagemann W. Wilder Knight II
| screenplay     = Jan Schütte
| based on       =  The Briefcase, Alone and Old Love    by Isaac Bashevis Singer
| starring       = Otto Tausig Rhea Perlman Olivia Thirlby Tovah Feldshuh
| music          = Henning Lohner
| cinematography = Edward Kłosiński Chris Squires
| editing        = Katja Dringenberg Renate Merck
| studio         = Zero Fiction Film Kino International
| released       =  
| runtime        = 86 minutes
| country        = Austria Germany United States
| language       = English
| budget         = 
| gross          = $77,458 
}}
Love Comes Lately is a 2007 film written for the screen and directed by Jan Schütte. The film is based on the short stories of Isaac Bashevis Singer.

==Plot==
Elderly Jewish writer Max Kohn (Otto Tausig) is an Austrian émigré whose mind is constantly working causing a state of perpetual confusion. Hes a successful author of short stories who lives in New York City and is so stuck in his old ways that he believes that the only proper way to write is by using a typewriter. Max has several women interested in seducing him, but he spends most of his time with fellow worrier Reisel (Rhea Perlman). During a trip to speak in nearby Hanover Max begins editing his latest story—a wild tale of a Miami retiree who gets himself into various kinds of trouble. It doesnt take Max long to lose himself in his own writings, and pretty soon, hes mixed up in two sexy romances and an unsolved murder.

Upon returning to reality, Max begins to feel as if his own written words have begun to manifest themselves. A meeting with burned out former student Rosalie (Barbara Hershey), with whom he shares a mutual attraction, follows, and later while heading to Springfield for another unwanted speaking engagement Max discovers that he has lost the speech he prepared. After a series of small adventures, Max decides to start writing a new story based on his recent life and featuring a protagonist named Harry—a thinly veiled stand-in for himself.

==Principal cast==
{| class="wikitable" style="width:50%;"
|- "
! Actor !! Role
|-
| Otto Tausig || Max Kohn
|-
| Rhea Perlman || Reisel
|-
| John C. Vennema || Dr. Grosskopf
|-
| Olivia Thirlby || Sylvia Brokeles
|-
| Tovah Feldshuh || Ethel
|-
| Bunny Levine || M.N.
|-
| Brian Doyle-Murray || Boss
|-
| Elizabeth Peña || Esperanza
|-
| Barbara Hershey || Rosalie
|-
| Gurdeep Singh || H.P.
|-
| Lee Wilkof || Professor Meyer
|}

==Reception==
The film received a score of 70% on Rotten Tomatoes, indicating pretty good reviews.

From Nathan Lee at The New York Times:
 

From Wesley Morris at The Boston Globe:
 

==References==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 