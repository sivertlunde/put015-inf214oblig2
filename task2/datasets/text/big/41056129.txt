Century of the Dragon
 
 
{{Infobox film
| name           = Century of the Dragon
| image          = CenturyoftheDragon.jpg
| image_size     =
| border         = 
| alt            = 
| caption        = Film poster
| film name      = {{Film name
| traditional    = 龍在邊緣
| simplified     = 龙在边缘
| pinyin         = Lóng Zài Biān Yuán
| jyutping       = Lung4 Zoi6 Bin1 Jyun4 }} 
| director       = Clarence Fok
| producer       = Wong Jing
| writer         = 
| screenplay     = Wong Jing
| story          = 
| based on       = 
| narrator       =  Patrick Tam Anthony Wong Joey Meng
| music          = Sammy Ha Wai Hin Kwan
| cinematography = Edmond Fung
| editing        = Marco Mak
| studio         = Jings Production China Star Entertainment
| released       =  
| runtime        = 109 minutes Hong Kong
| language       = Cantonese
| budget         = 
| gross          = HK$8,313,482
}}
 1999 Cinema Hong Kong crime drama Patrick Tam.

==Plot== Anthony Wong) was fighting for territories with another tiad boss Loan Shark Ko (Lee Ka Ting), which happened on the day of Fei Lungs mothers (Paw Hee-ching) birthday party. Unknown to Pau, the police have planted undercover cop Big Head Man (Eric Wan) in his gang for many years. On that night, Pau was arrested and hospitalized due to heavy injuries.
 Patrick Tam), a ruthless man, takes the opportunity for vengeance and makes a deal with the elders that whoever is able to find the mole and kill Superintendent Ko and Loan Shark Ko, will get the opportunity to be the new leader. When Sing hears this he decides to immediately inform Big Head Man. Since Man lost his pager, Sing was unable to call him and tries to find Man in his sons school. When Sing makes it there, Man has left with his son and while trying to find him everywhere, he finds Man and his son killed by Chun and his henchmen.

Chun is actually desperate to take his fathers position as Hung Hings leader and pretends to feel sad for his father and tells Sing to kill Superintendent Ko, which Sing neglects. Fei Lung does not want to deal with the underworld and tells Sing to beware of moles around him, which makes Sing feel guilty. Chun calls Sing to negotiate with Loan Shark Man, while on the other hand orders his henchmen to sweep Kos business and rape his wife and daughter. While Ko was negotiating with Sing, he finds out that his business were all swept and his wife and daughter raped and decides to kill Sing before he was rescued by Fei Lung, which makes Ko believe that Fei Lung is behind all this and decides to take revenge on him. Later, Superintendent Ko tells Sing to immediately assist in arresting Fei Lung, but Sing explains to Ko that Chun is the mastermind behind this and persuades him to arrest Chun instead, which leads Ko to misunderstand that Sing has renegaded and detains him.

When Sings girlfriend Judy (Joey Meng) learns that Chun sent his top henchmen Ma Wong (Frankie Ng) and Fa Fot (Lung Fong) to kidnap Fei Lungs wife Daisy (Suki Kwan), Judy immediately informs Daisy; while on the other hand, Loan Shark Ko has captured Lungs mother to threaten Lung to surrender and Chun unexpectedly arrives and kills Ko while trapping Lung, his mother and son in a farm. When Superintendent Ko finds Loan Shark Ko dead in Lungs house, he lists Lung as a wanted criminal.

When Sing rescues Daisy, he learns that Chun has captured Fei Lung and his family and goes to find Chun in his company. While also with the help of Sing, Daisy was able to avoid being rapped by Chun and holds him hostage to the farm to rescue Fei Lung, his mother and son. However, after rescuing Fei Lung where a big gunfight occurs, Daisy was killed by Chun in the process.

In the end, Fei Lung decides to take revenge. When Sing learns of this, he immediately rushes to stop him from this mistake. There, Fei Lung uses Sings gun to resolve Chun. Sing shoots Chun for self-defense where Chun also falls off the building and dies.

==Cast==
*Andy Lau as Lung Yat Fei / Fei Lung Gor
*Louis Koo as Wong Chi Sing Patrick Tam as Tong Man Chun
*Suki Kwan as Daisy Anthony Wong as Tong Pau
*Joey Meng as Judy
*Paw Hee-ching as Fei Lungs mother
*Lawrence Lau as Ken
*Frankie Ng as Ma Wong
*Lee Siu-kei as Sam
*Eric Wan as Big Head Man
*Lung Fong as Fa Fit
*Joe Lee as Superintendent Ko
*Chung Yeung as Cole
*Lee Ka Ting as Loan Shark Ko
*Yu Man Chun
*Fan Chin Hung as thug
*Lee To Yue as triad at meeting
*Chan Sek
*Yee Tin Hung
*Adam Chan
*Lam Kwok Kit
*Chu Cho Kuen
*Hon Ping

==Box office==
The film grossed HK$8,313,482 at the Hong Kong box office during its theatrical run from 15 October to 4 November 1999 in Hong Kong.

==See also==
*Andy Lau filmography
*Wong Jing filmography

==External links==
* 
*  at Hong Kong Cinemagic
* 
*  at LoveHKFilm.com

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 