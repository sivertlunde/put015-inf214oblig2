Koi Tujh Sa Kahaan
 
{{Infobox film
| name           = Koi Tujh Sa Kahan
| image          = Koi Tujh Sa Kahaan CD Cover.jpg
| image_size     =
| caption        =
| director       = Reema Khan
| producer       = Reema Khan
| writer         = Khalil-ur-Rehman Qamar Nadeem Veena Malik Babrak Shah
| music          = Amjad Bobby Wajid Ali Nashad
| cinematography = Waqar Bokhari
| editing        = Akiv Ali Aqeel Ali Asghar
| released       = August 12, 2005
| runtime        = 163 minutes
| country        = Pakistan
| language       = Urdu
| budget         =
| domestic gross =
| preceded_by    =
| followed_by    =
}}
 2005 Pakistani Reema which was released in theaters across Pakistan and UK in August 2006.

==Synopsis==
Koi Tujh Sa Kahan revolves around a loving husband and wife. Their lives become bumpy and rough because of greed, selfishness and self-centered approaches of a handful of so-called friends of the family. As after many thrills and sequences full of suspense, the life of this couple is on a smooth track with the efforts of a very thoughtful individual.

==Film business==
In the box office the film was a hit.  Lahore: Golden Jubilee (Metropole 22 weeks) Karachi: Silver Jubilee

==Filming locations==
* Bangkok, Thailand
* Kuala Lumpur, Malaysia
* Singapore

==Cast==
* Reema Khan
* Veena Malik
* Moammar Rana
* Bebrik Shah Simran
* Nadeem
* Irfan Khusat

==Songs==
1.Koi Tujh Sa Kahan (Title Song) 
2.Sohni Sohni Sohni 
3.Meri Aankhain 
4.Aaja Karle Pyar 
5.Ek Bewafa Ne 
6.Wada Hai Mera 
7.Pyar Main Kis Ne

==External links==
* 
* 

 

 
 
 
 
 
 


 
 