Fist Power
 
 
{{Infobox film name = Fist Power image = Fist Power.jpg caption = DVD cover traditional = 生死拳速 simplified = 生死拳速 pinyin = Shēng Sǐ Quán Sù jyutping = Sang1 Sei2 Kyun4 Cuk1}} director = Aman Chang producer = Wong Jing starring = Chiu Man-cheuk Anthony Wong Sam Lee music = Tommy Wai Samuel Leung cinematography = Choi Shung-fai editing = Marco Mak studio = Jings Production Limited Wins Entertainment Ltd. distributor = China Star Entertainment Group released =   runtime = 92 minutes country = Hong Kong language = Cantonese English gross = HK$771,235 
}} Hong Kong Chiu Man-cheuk, Anthony Wong, Sam Lee. Shooting for the film took place in Hong Kong between February and March 1999. The film was released in Hong Kong theatres on 21 January 2000.

==Plot==
Brian Cheuk is a mainland Chinese security specialist and martial arts teacher. He visits his family in Hong Kong. Shortly after he sends his nephew to school, Charles, a former British marine, takes the students hostage and plants bombs around the school. Charles is unhappy because his son was taken away by his ex-wife and her new husband, and they are on their way to America. He demands that his son be brought to him in exchange for the hostages. Cheuk agrees to help Charles and he rushes to the airport with Simna (Charless brother-in-law) and Hung (a reporter). Even after they find the boy, all is not over yet because their journey back is filled with danger — both the police and the thugs are out to get them.

==Cast== Chiu Man-cheuk as Brian Cheuk Anthony Wong as Charles Chau
* Gigi Lai as Hung Sam Lee as Simna
* Cheng Pei-pei as Brians mother
* Lau Kar-wing as Brians father
* Kara Hui as Brians sister
* Lung Fong as Chiu Chung-tin
* Jewel Lee as Killer
* Mak King-ting as Charles Chaus ex-wife
* Wai Tin-chi as Killer
* Mak Tak-law as Killer
* Lam Suet as Fatty
* Jude Poyer as Killer
* Wu Chi-lung as Brian Cheuks brother-in-law

==Home media== Region 2. DVD was released in Region 1 in the United States on 16 October 2001, it was distributed by Tai Seng.

==See also==
* List of Hong Kong films

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 