Echoes of Paradise
 
 
{{Infobox film
| name           = Echoes of Paradise
| image          = Shadows of the Peacock.jpg
| image_size     =
| caption        =
| director       = Phillip Noyce
| writer         = Jan Sharp Anne Brooksbank
| narrator       =
| starring       = Wendy Hughes John Lone
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = 92 mins.
| country        = Australia
| language       = English
| budget         = A$2.6 million "Australian Productions Top $175 million", Cinema Papers, March 1986 p64 
| gross          = A$19,851 (Australia) 
| website        =
| amg_id         =
}}
Echoes of Paradise (also known as Shadows of the Peacock) is a 1989 Australian film directed by Phillip Noyce.

==Production==
The film was written by Jan Sharp who was then married to the director Phil Noyce. Sharp was going to produce but was held up making The Umbrella Woman so producing duties went to Jane Scott. The story was originally set in Bali, where the movie was to be shot, but before filming began the Sydney Morning Herald ran a series of articles suggesting the wife of the Indonesian president was corrupt. All Indonesian visas for Australians were cancelled and the script was rewritten to be set in Thailand, with several keys scenes shot in Sydney. David Stratton, The Avocado Plantation: Boom and Bust in the Australian Film Industry, Pan MacMillan, 1990 p112-113 

Phil Noyce later said the story was so distorted by this change they probably would have been better off not making the movie:
 The original story was very different. It was really about the Balinese characters alienation and his coming to terms with it, coming to terms with a western influence and his traditional obligations, trying to work it all out. Wendy Hughes character went through a very similar journey in the original story. Its just that the setting and the Balinese character were very different once we moved to Thailand.  

==Box Office==
Echoes of Paradise grossed $19,851 at the box office in Australia. 

==See also==
*Cinema of Australia

==References==
 

==External links==
* 

 

 
 
 

 