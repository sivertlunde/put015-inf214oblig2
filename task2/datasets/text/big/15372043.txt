The Sea Knows
{{Infobox film name           = The Sea Knows image          = The Sea Knows.jpg caption        = Poster for The Sea Knows (1961) director       = Kim Ki-young  producer       = Kim Ki-young writer         = Kim Ki-young starring       = Kim Wun-ha Gong Midori music          = Han Sang-ki cinematography = Choe Ho-jin editing        = O Yeong-geun distributor    = Korean Art Movies released       =   runtime        = 117 minutes country        = South Korea language       = Korean budget         =  gross          = 
| film name      = {{Film name hangul         =       hanja          =  은 알고 있다 rr             = Hyohaetaneun alko itta mr             = Hyŏnhaet‘anŭn algo itta context        = }}
}} 1961 South Korean film directed by Kim Ki-young.

==Synopsis==
A wartime melodrama about Aroun, a Korean living in Japan and conscripted into the army. He endures cruel treatment at the hands of the Japanese soldiers, and objections from the mother of his Japanese girlfriend. The film concludes with a U.S. bombing which kills all the Japanese soldiers, but leaves Aroun alive. 

==Cast==
* Kim Wun-ha as Aroun 
* Gong Midori as Hideko
* Lee Ye-chun as Mori
* Lee Sang-sa as Inoue Kim Jin-kyu as Nakamura
* Kim Seok-hun
* Ju Jeung-ryu as Hidekos mother
* Kim Seung-ho
* Park Am
* Park Nou-sik

==Notes==
 

==Bibliography==
*  
*  
*  

 

 
 
 
 
 


 