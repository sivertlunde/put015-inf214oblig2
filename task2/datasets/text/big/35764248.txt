Yeh Jo Mohabbat Hai
 
{{Infobox film
| name = Yeh Jo Mohabbat Hai - Movie
| image =
| alt =
| caption = Theatrical release poster
| director = Shree Narayan Singh
| producer = Ashim Samanta
| screenplay =Dilip Shukla
| starring = Aditya Samanta Nazia Hussain Mohnish Behl Rati Agnihotri Farida Jalal Mukesh Tiwari Anooradha Patel Preet Kaur Madhan Siddharth arora
| music = Anu Malik
| cinematography = Fuwad Khan
| released =  
| country = India
| language = Hindi
}}
Yeh Jo Mohabbat Hai is a dramatic love story, releasing in India and other countries on 3 August 2012.   Yeh Jo Mohabbat Hai is a story of two young people who are caught between love for each other and the generations old enmity between their respective Rajput families. 

== Synopsis ==
The film begins with the birth of Karishma (Nazia) and Karan (Aditya) on the same day in the same hospital. 23 years later, they dont know  their family backgrounds, when they meet each other in summer at Kraków University Poland and they  fall in love.

When Karan finds out her family background, he starts avoiding Karishma. What happens thereafter is a succession of interesting events that you would get to see in this musical extravaganza from the house of Shakti Samanta which made some memorable romantic films like Aradhana, Kati Patang, Amar Prem, Kashmir Ki Kali and others.

 

== Cast ==
* Aditya Samanta as Karan
* Nazia Hussain as Karishma
* Rati Agnihotri
* Farida Jalal
* Mukesh Tiwari
* Anuradha Patel
* Preet Kaur Madhan
* Mohnish Behl
* Siddharth Arora
* Manasvi Vyas

== Soundtrack ==
The soundtrack of Yeh Jo Mohabbat Hai  is composed by Anu Malik.The album consist of seven songs.

===Tracklist===
{| border="2" cellpadding="3" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
| Track || Song || Singers || Duration 
|-
| 01 || Pyaar Karna Na Tha(Male) || Mohit Chauhan ||   07:38
|-
| 02 || Pyaar Karna Na Tha(female || Shreya Ghoshal ||   07:12
|-
| 03 || Tere Bina jee Na Laage || Mohit Chauhan, Suzanne DMello   || 6:16
|-
| 04 || Naina Thak Thak Haare || Roop Kumar Rathod   || 7:09
|-
| 05 || Kyon Kyon || Shaan (singer)|Shaan, Shreya Ghoshal   || 5:43
|-
| 06 || Big Fat Indian Wedding || Neeraj Shridhar, Anmol Malik  ||05:48
|-
| 07 || Angel My Angel|| Sonu Nigam, Alisha Chinai   || 4:04
|}

== References ==
 
 

== External links ==
*  
*  
*  

 
 
 
 


 
 