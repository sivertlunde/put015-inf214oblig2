Havinck
{{Infobox film
| name           = Havinck
| image          = 
| image_size     = 
| caption        = 
| director       = Frans Weisz
| producer       = Gijs Versluys
| writer         = Marja Brouwers Ger Thijs
| narrator       = 
| starring       = Willem Nijholt
| music          =   
| cinematography = Giuseppe Lanci
| editing        = Ton Ruys
| distributor    = 
| released       = 15 October, 1987
| runtime        = 101 minutes
| country        = Netherlands
| language       = Dutch
| budget         = 
}} 1987 cinema Dutch drama film directed by Frans Weisz. It was screened in the Un Certain Regard section the 1988 Cannes Film Festival.   

==Cast==
* Willem Nijholt - Lawyer Havinck
* Will van Kralingen - Havincks wife Lydia
* Carolien van den Berg - Havincks mistress Maud
* Anne Martien Lousberg - Havincks daughter Eva
* Maarten Wansink - Greve
* Coen Flink - Bork
* Max Croiset - Havincks father-in-law
* Dora van der Groen - Havincks mother-in-law
* Kenneth Herdigein - Kenneth
* Eric van Heyst - Noordwal
* Ella van Drumpt - Havincks secretary
* Lieneke le Roux - Borks secretary
* Lex de Regt - Detective
* Dorijn Curvers - Policewoman
* Han Kerkhoffs - Policeman
* Ger Thijs - Probation officer

==References==
 

== External links ==
*  

 
 
 
 
 
 
 