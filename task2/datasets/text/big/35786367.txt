Bugles in the Afternoon
{{Infobox film
| name        = Bugles in the Afternoon
| image       = Bitapos.jpg
| caption     = Original film poster
| name        = Roy Rowland
| producer    = Harry Brown Ernest Haycox (novel)
| starring    = Ray Milland
| music       = Dimitri Tiomkin
| distributor = Warner Bros.
| released    =  
| runtime     = 85 minutes
| country     = United States
| gross       = $1.5 million (North America) 
}} Western feature film starring Ray Milland, based on the novel by Ernest Haycox.   The story features the Battle of the Little Big Horn. It was filmed in Technicolor and released by Warner Bros..

==Plot==
A rivalry between U.S. Cavalry captains results in Kern Shafter being demoted and disgraced for striking Edward Garnett with a saber. Kern claimed to be defending the honor of his fiancee.
 Bismarck in the Dakota territory, Kern is welcomed to join the 7th Cavalry by an old friend, Capt. Myles Moylan, and assigned the rank of sergeant. He is pleased until he learns that Capt. Garnett is there at Fort Lincoln as well, and now will be his commanding officer.

Kern makes a friend named Donovan, a private. The two of them are assigned to investigate the murder of local miners by Sioux tribesmen, leading to a dangerous encounter. When these risky missions continue, Capt. Moylan begins to realize that Garnett is deliberately putting Kern at risk.

The feud escalates when Garnett makes romantic advances toward Josephine, who is angered by Kern striking him, unaware of their history or Garnetts true character.

The soldiers leave with General George Armstrong Custer to do battle with the Sioux. After his friend Donovan is fatally wounded, Kern is able to get back to the fort. He accompanies Garnett back to the battlefield at Little Big Horn just in time to see Custers men being massacred.

After the rivalry between themselves comes to a head, Kerns reputation and rank of captain are restored thanks to Moylan, and he is now seen by Josephine as the man she wants.

==Cast==
Ray Milland  ...  Kern Shafter   
Helena Carter  ...  Josephine Russell   
Hugh Marlowe  ...  Capt. Edward Garnett   
Forrest Tucker  ...  Donovan   
Barton MacLane  ...  Capt. Myles Moylan  
George Reeves  ...  Lt. Smith

==References==
 

==External links==
*  at IMDB

 

 
 
 
 
 
 
 
 
 
 
 
 


 