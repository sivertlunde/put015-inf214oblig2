Laborer's Love
{{Infobox film
| name           = Laborers Love (aka Romance of a Fruit Peddler)
| image          = Laborers Love (1922).webm
| image_size     =
| caption        =
| director       = Zhang Shichuan
| producer       =
| writer         = Zheng Zhengqiu
| starring       = Zheng Zhegu Zheng Zhengqiu Yu Ying
| cinematography = Zhang Weitao
| editing        =
| studio         = Mingxing Film Company
| released       =  
| runtime        = 30 minutes
| country        = China English intertitles
| budget         =
}}
 1922 short film produced in China, and premiered October 5, 1922 at the Olympic Theater in Shanghai.  It is also known as Romance of a Fruit Peddler or Romance of a Fruit Pedlar ( ) and as "Cheng the Fruit Seller".  It constitutes the earliest complete film from Chinas early cinematic history that survives today. {{cite web | url = 
http://people.cohums.ohio-state.edu/denton2/courses/c505/temp/history/chapter2.html publisher = Ohio State University}}  The film was also one of the earliest productions of the soon-to-be prolific Mingxing Film Company and was directed and written by Mingxing co-founders Zhang Shichuan and Zheng Zhengqiu, respectively.

Notably, the film had both Chinese and English intertitles; a clear indication that at this early point in its history, cinema in Shanghai was made not only for the Chinese, but for the many Westerners residing there as well. {{cite web | url = http://www.univie.ac.at/Sinologie/repository/ueLK110_ChinFilmgesch/filmgeschichteSkript.pdf publisher = The University of Vienna- Sinologie Program}} 

In 2007, Australian ensemble Blue Grassy Knoll wrote a new score for Labourers Love, and performed it live in Shanghai and Beijing as part of the Australian Theatre Festival.

==Plot==
	An unemployed carpenter finds temporary work selling fruit from a pushcart.  He meets and falls for a doctors daughter, but her father opposes the relationship due to the young mans humble station, but he will approve a suitor who helps to rescue his failing medical practice.  The carpenter restructures the staircase to a gambling club outside the doctors office so that it collapses whenever anyone walks on it.  The resulting number of injured seeking treatment so increases the doctors income that he gladly accepts the carpenter as his son-in-law.

==Cast==
*Zheng Zhegu ... Zheng the carpenter
*Zheng Zhengqiu ... Doctor Zhu
*Yu Ying ... Miss Zhu

==Notes==
 

==External links==
 
* 
 

 
 
 
 
 
 


 
 