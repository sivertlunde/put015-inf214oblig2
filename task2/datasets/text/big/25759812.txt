The Aviator (1929 film)
 
{{Infobox film
| name           = The Aviator  
| image          = The_Aviator_1929_Poster.jpg
| image_size     = 275px
| caption        = Theatrical poster
| director       = Roy Del Ruth  
| producer       = Irving Asher
| based on       = The Aviator (play) by James Montgomery
| writer         =   Arthur Caesar Robert Lord De Leon Anthony (titles)
| narrator       = 
| starring       = Edward Everett Horton Patsy Ruth Miller
| music          = Rex Dunn (uncredited)
| cinematography = Chick McGill (aka Barney McGill) William Holmes
| distributor    = Warner Bros. Pictures
| released       =  
| runtime        = 75 minutes
| country        = United States
| language       = English
}}

The Aviator is a 1929 talkie|all-talking Pre-Code Hollywood|pre-code Vitaphone comedy film produced and released by Warner Bros. Directed by Roy Del Ruth, the film was based on the play of the same name by James Montgomery and starred Edward Everett Horton and Patsy Ruth Miller. The Aviator is similar to the silent comedy The Hottentot (1922), where a hapless individual has to pretend to be a famous steeplehorse jockey.The Aviator today is considered a lost film. 

==Plot== William Norton Bailey), a publisher and his publicist (Lee Moran) decide to  boost the sales of a wartime book of flying experiences. They credit the book to popular author Robert Street (Edward Everett Horton), who is completely ignorant about aviation. Robert gets into all sorts of trouble in attempting to carry on the ruse, saving his friends business but also attracting the attention of aviation-mad Grace Douglas (Patsy Ruth Miller). At first, he is able to carry out simple publicity events, but when he accidentally starts up an aircraft, his incredible aerobatics end with a landing in a haystack. When a race is staged between him and French ace Major Jules Gaillard (Armand Kaliz), it ends with Robert confessing he is no pilot, but still winning Graces heart.

==Cast==
 
 
* Edward Everett Horton as Robert Street
* Patsy Ruth Miller as Grace Douglas
* Johnny Arthur as Hobart William Norton Bailey as Brooks
* Armand Kaliz as Major Jules Gaillard
* Edward Martindel as Gordon
* Lee Moran as Brown
* Kewpie Morgan as Sam Robinson
* Phillips Smalley as John Douglas
 

==Production==
The Aviator appeared at a time when aviation films were extremely popular, with only western films being made in larger numbers. It was typical to even have aircraft show up in a western. Blending comedy in The Aviator with aerial thrills seemed to be an ideal way to satisfy the publics appetite for aviation subjects.  

==Reception== Hal Erickson wrote, "The ensuing wild ride through the air is the best part of the picture, with Robert trying to maintain his equilibrium and dignity throughout." 
 Joe E. Going Places, starring Dick Powell (impersonating a famous horseman), with Ronald Reagan appearing in an early role.

==Preservation==
No film elements of The Aviator are known to survive. The soundtrack, which was recorded on Vitaphone disks, may survive in private hands.  In the late 1949s and 1950s, Warner Bros. destroyed many of its negatives due to nitrate film decomposition. Studio records indicate that the negative of The Aviator and filmography pre-1931 was marked "Junked 12/27/48" (December 27, 1948).

==See also==
*List of lost films

==References==
Notes
 
Bibliography
 
* Wynne, H. Hugh. The Motion Picture Stunt Pilots and Hollywoods Classic Aviation Movies. Missoula, Montana: Pictorial Histories Publishing Co., 1987. ISBN 0-933126-85-9.
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 