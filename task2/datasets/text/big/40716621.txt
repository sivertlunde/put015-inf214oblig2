Jet Set (film)
{{Infobox film
| name           = Jet Set
| image          = Jet Set (film).jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = Jet Set
| director       = Fabien Onteniente
| producer       = Eric Altmayer, Nicolas Altmayer, Javier Castro
| writer         = Caroline Saillo
| screenplay     = Fabien Onteniente, Bruno Solo, Emmanuel de Brantes, Olivier Chavarot
| story          = 
| starring       = Bruno Solo Samuel Le Bihan Lambert Wilson Ornella Muti
| music          = Loïc Dury, Christophe Minck 		
| cinematography = Franco Di Giacomo
| editing        = Nathalie Hubert
| studio         = Bac Films
| distributor    = Vega Distribution, Bac Films
| released       =  
| runtime        = 105 minutes
| country        = France
| language       = French
| budget         = $5,6 million
| gross          = $14,346,712  
}}
Jet Set is a 2000 French comedy film, directed by Fabien Onteniente. A sequel, People (film)|People, was released in 2004.

==Plot==
Jimmy (Bruno Solo), desperate to save his suburban bar from bankruptcy, conceives a plan to attract the "jet set", the rich and glamorous celebrities of France. He sends his friend Mike (Samuel Le Bihan), a down-at-the-heels unemployed actor, to infiltrate French high society and garner contacts with prestigious personalities to invite. Hijinks ensue.

==Cast==
* Samuel Le Bihan : Mickaël Gonzalvez aka « Mike »
* Lambert Wilson : Artus de Poulignac
* Ornella Muti : Camilla
* Ariadna Gil : Andréa
* Bruno Solo : Jimmy
* José Garcia (actor)|José Garcia : Mellor de Silva
* Aurore Clément : Nicole
* Estelle Larrivaz : Lydia
* Loránt Deutsch : Fifi
* Elli Medeiros : Danielle Joubert
* Antoinette Moya :   Gonzalvez
* Guillaume Gallienne : Evrard Sainte Croix  
* Armelle : Frénégonde
* Adel Kachermi : Himself

==References==
 

==External links==
* 
*  at AlloCiné  

 
 
 
 