Alibaba Aur 40 Chor
 
{{Infobox Film
| name = Alibaba Aur 40 Chor
| image = Alibaba40film.jpg
| image_size = 
| caption = Vinyl Record Cover
| director       = Latif Faiziyev  Umesh Mehra
| producer       = F.C. Mehra
| writer         = 
| narrator       = 
| starring       = Dharmendra Hema Malini Zeenat Aman Prem Chopra Madan Puri Rolan Bykov Frunzik Mkrtchyan Sofiko Chiaureli
| music          = Rahul Dev Burman Anand Bakshi (lyrics)
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 30 May 1980
| runtime        = 153 mins
| country        = India, USSR Russian
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 Choreographer P. L. Raj. 

Plot

The story is about a poor lad named Ali Baba (Dharmendra) who lives in the town of Gulabad, somewhere in central Asia, with his mother and elder brother Qasim who owns a small petty shop. Ali Babas father Yousuf is a merchant in a faraway land who has never returned since he last left when Ali Baba was born. So poor Ali Baba makes a living out of selling timber cut from the hills. Gulabad is terrorized by a band of 40 dacoits. They hide their loot in a magical cave in the deserted hills. When the bandit leader recites the magical spell it opens and when he says another spell it closes.  When news reaches them that his father has gone missing, Ali Baba goes in his search and not only finds his father, but also rescues princess Marjeena (Hema Malini) from the guards of the king who murdered her father to become king. Both Marjeena and Ali Baba fall in love with each other. Then they are attacked, Marjeena is taken captive, and his father is killed. After burying his father, Ali Baba finds out that Marjeena is being sold in the slave market, he borrows money from Qasim, and uses that to pay for Marjeena, and brings her home. Qasim wants to recover his money, and as a result decides to evict Ali Baba from their family home. Ali Baba and his mother leave the home. It is then the Khazi of the region announces a reward for the capture of notorious bandit Abu Hassan. A young girl named Fatima (Zeenat Aman) whose father has been murdered by the dacoits has a score to settle with Abu Hassan (Rolan Bykov). Fatima pledges her support to Ali Baba in killing Abu Hassan. Shortly, thereafter Ali Baba comes to know the secret hideout of Abu Hassan and its magic spells to open it.  He also gets some gold and jewelry from there, which he distributes amongst villagers for diverting some water to their parched land. Ali Baba’s greedy brother Qasim lures Ali Baba into telling him where the cave is and those magic spells.  Out of greed Qasim takes so much gold jewelry and coin as a result of which he forgets the spell to reopen the door and gets stuck inside. When the dacoits find him they kill him. Ali Baba then informs the Khazi about Abu Hassans hideout. What Ali Baba does not know is that the Khazi and Abu Hassan is the same person, and that the Khazi has given instructions to his men to ensure that Ali Baba is killed, so that no one can get their hands on his treasure. Abu Hassan hides the 40 thieves in large urns to kill Ali Baba. Ali Baba comes to know of this and kills them all with the help of Fatima. He brings to light the startling truth that their own ruler heads the dacoits.

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Jadugar Jadoo Kar Jayega"
| Kishore Kumar, Asha Bhosle
|-
| 2
| "Khatouba"
| Asha Bhosle 
|-
| 3
| "Sare Shahar Mein"
| Lata Mangeshkar, Asha Bhosle 
|-
| 4
| "Aaja Sar-E-Bazar"
| Lata Mangeshkar 
|-
| 5
| "Qayamat"
| Lata Mangeshkar 
|}

==External links==
* 
 

 
 
 
 
 
 
 