Morgan's Last Raid
{{Infobox film
| name           = Morgans Last Raid
| image          = Morgans Last Raid poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Nick Grinde
| producer       = 
| screenplay     = Harry Braxton Bradley King
| story          = Madeleine Ruthven Ross B. Wills 
| starring       = Tim McCoy Dorothy Sebastian Wheeler Oakman Al Ernest Garcia Hank Mann
| music          = 
| cinematography = Arthur Reed
| editing        = William LeVanway 	
| studio         = Metro-Goldwyn-Mayer
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 60 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Western silent film directed by Nick Grinde and written by Harry Braxton and Bradley King. The film stars Tim McCoy, Dorothy Sebastian, Wheeler Oakman, Al Ernest Garcia and Hank Mann.   The film was released on January 5, 1929, by Metro-Goldwyn-Mayer.

==Plot==
 

== Cast ==	 
*Tim McCoy as Capt. Daniel Clairbourne
*Dorothy Sebastian as Judith Rogers
*Wheeler Oakman as John Bland
*Al Ernest Garcia as Morgan
*Hank Mann as Tex
*C. Montague Shaw as Gen. Rogers

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 