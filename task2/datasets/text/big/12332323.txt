Jeevan Dhaara
{{Infobox film
| name           = Jeevan Dhaara
| image          = JeevanDhaara.jpg
| image_size     =
| caption        =
| director       =Tatineni Rama Rao
| producer       =A V Subba Rao
| writer         = Dr. Rahi Masoom Reza
| narrator       =
| starring       = Rekha Raj Babbar Amol Palekar Sulochana Latkar Simple Kapadia Rakesh Roshan
| music          = Laxmikant-Pyarelal
| cinematography =
| editing        = Bhaskar
| distributor    =
| released       = 6 February 1982
| runtime        =
| country        = India
| language       = Hindi
| budget         =
}} Indian Bollywood film directed by Tatineni Rama Rao. The film is a remake of the 1974 Tamil film Aval Oru Thodar Kathai.  The film stars Rekha, Raj Babbar, Amol Palekar, Sulochana Latkar, Simple Kapadia and Rakesh Roshan. The movie belongs to Art Cinema genre, also known as Parallel Cinema. Rekha received a nomination for Filmfare Best Actress Award, the only nomination for the film.   She is credited with the films box office success. 

==Plot==
Sangeeta (Rekha) is a young, strong and idealistic girl. She is 25 years old, but unlike her contemporaries, she is still not married. The reason for this is her being a member of a poor family. Her father left the family; Her mother is an old homemaker; Her younger sister Geeta (Madhu Kapoor) is a young widow; Her nephews have to go to school while their father, her brother (Raj Babbar) is an inebriated and unemployed man. All the members of this family live in one little house. She is the only one who takes care of them. She is the only one who works to support the family. She is concerned for her nephews future and makes her best to bring them up and educate them.

However, secretly, she dreams of the day when she could also have her own family, husband and children. Three men enter her life, namely Amol Palekar, Kanwaljeet Singh and Rakesh Roshan, but destiny has something different for her. Will this day come ever?

==Cast==
* Rekha
* Raj Babbar
* Amol Palekar
* Simple Kapadia
* Rakesh Roshan
* Sulochana Latkar

==References==
 

==External links==
*  

 
 
 
 
 
 

 