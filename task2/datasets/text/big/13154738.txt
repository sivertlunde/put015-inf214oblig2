The Gentle Cyclone
 
{{Infobox film
| name           = The Gentle Cyclone
| image          =File:Gentle Cyclone lobby card.jpg
| caption        =Lobby card
| director       = Woody Van Dyke|W.S. Van Dyke
| producer       =
| writer         = Frank R. Buckley Thomas F. Dixon Jr.
| starring       = Oliver Hardy
| music          =
| cinematography = Chester A. Lyons Reginald Lyons
| editing        =
| distributor    = Fox Film Corporation
| released       =  
| runtime        =
| country        = United States
| language       = Silent English intertitles
| budget         =
}}
 short comedy film featuring Oliver Hardy.

==Cast==
* Buck Jones - Absolem Wales
* Rose Blossom - June Prowitt
* Will Walling - Marshall Senior
* Reed Howes - Marshall Junior
* Stanton Heck - Wilkes Senior
* Grant Withers - Wilkes Junior
* Kathleen Myers - Mary Wilkes Jay Hunt - Judge Summerfield
* Oliver Hardy - Sheriff Bill
* Marion Harlan

==See also==
* List of American films of 1926
* Oliver Hardy filmography

==External links==
* 

 

 
 
 
 
 
 
 
 
 


 