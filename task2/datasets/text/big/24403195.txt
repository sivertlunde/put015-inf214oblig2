Road, Movie
{{Infobox film
| name           = Road, Movie
| image          = RoadMovie.jpg
| caption        = Poster
| director       = Dev Benegal
| producer       =  
| writer         = Dev Benegal
| starring       =  
| music          = Michael Brook
| cinematography = Michel Amathieu
| editing        = Yaniv Dabach
| distributor    = 
| released       =  
| runtime        = 95 minutes
| country        = India
| language       = Hindi
| budget         =   8 Crores
| gross          = 
|}}
Road, Movie ( ) is a 2009 Indian road movie directed by Dev Benegal, and starring Abhay Deol, Tannishtha Chatterjee, and Satish Kaushik. It premiered at the 2009 Toronto Film Festival   and opened the section Generation 14plus at the 60th Berlin International Film Festival in February 2010.  Road, Movie was released in India on 5 March 2010. 

==Plot==
Vishnu (Abhay Deol), a restless young man, itches to escape his fathers faltering hair oil business.

An old truck beckons, which, Vishnu sees as his ticket to freedom. He offers to drive the 1942 Chevy across the desert to the sea, where it has been sold to a local museum. As he sets off across the harsh terrain of desert India, he discovers hes not merely transporting a battered vehicle but an old touring cinema.

Along the way, Vishnu reluctantly picks up a young runaway (Mohammed Faizal Usmani), a wandering old entertainer (Satish Kaushik) and a striking gypsy woman (Tannishtha Chatterjee). Together they roam in the barren land, searching for water and an elusive fair. The journey turns dire when they are waylaid by corrupt cops and a notorious water lord. 

The key to their freedom is the eccentric collection of films and the two forty-year-old film projectors in the back of the truck. As in 1001 Nights, if the films are good, they live and move on. If the films are boring, they face death in the outback.

The journey proves transformative for each of the travelers, but especially for Vishnu who discovers life, love and laughter on the Indian highway.

==Cast==
* Abhay Deol as Vishnu
* Tannishtha Chatterjee as the Woman
* Mohammed Faisal as the Boy
* Satish Kaushik as Om
* Yashpal Sharma as the Waterlord
* Veerendra Saxena as Police Chief
* Amitabh Srivastava as Father
* Suhita Thatte as Mother
* Hardik Mehta as Masseur
* Shradha Shrivastav as Sister
* Roshan Taneja as OPJ

==Production==
Road, Movie began in 2004 as an original idea by Dev Benegal. To do research for the film, he travelled for a year with a transient cinema troupe that showcased different films in rural Rajasthan villages. Benegal completed the first draft of the films script in only 10 days. The script was written at a difficult period in his career as his other projects had been unsuccessful in finding financial backers.  He successfully submitted his screenplay to the LAtelier screenwriting portion of the 2006 Cannes Film Festival.  Abhay Deol signed on to play the role of Vishnu after reading the script which he found "really nice, dreamy and humorous."    Filming took place in desert areas of the Kutch District of Gujarat and Jaisalmer in Rajasthan. 

==Release== Tokyo Sakura Grand Prix Award and at the 2009 Tribeca Film Festival.

===Box office===
Road, Movie released in theatres on March 5, 2010 to a poor turnout at the cinemas. The film grossed a poor INR 11.1 million in its first week. In its second week, the films collections saw a drop of a massive 90%, and netted INR 1.2 million. It was declared as a disaster at the box office.  
<!--==Music==
{{Infobox Album 
| Name        = Road Movie
| Type        = Soundtrack
| Artist      = Michael Brook
| Cover       = 
| Released    = 
| Recorded    =
| Genre       = 
| Length      = 
| Label       = 
| Producer    = 
| Reviews     =
|  Last album  = 
|  This album  = 
|  Next album  = 
}}-->

==Awards==
 2011 Star Screen Awards
Won 
* Star Screen Award for Best Sound – Vikram Joglekar

==References==
 

==External links==
*  
*  
*  , Rediff.com
* 

 
 
 
 
 
 
 
 