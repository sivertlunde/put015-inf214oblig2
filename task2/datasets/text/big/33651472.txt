Two Little Drummer Boys
{{Infobox film
| name           = Two Little Drummer Boys
| image          =
| caption        =
| director       = G.B. Samuelson 
| producer       = John E. Blakeley
| writer         = Walter Howard (play)  Georgie Wood  Derrick De Marney   Alma Taylor   Paul Cavanagh
| music          = 
| cinematography = 
| editing        = 
| studio         = John E. Blakeley Productions
| distributor    = Victoria Films
| released       = October 1928
| runtime        = 7,500 feet 
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         = 
| preceded_by    =
| followed_by    =
}} British silent silent comedy Georgie Wood, Derrick De Marney and Alma Taylor.  The film was based on the 1899 play Two Little Drummer Boys by Walter Howard and was shot at Southall Studios. It was produced by Mancunian Films. 

==Cast== Georgie Wood - Eric Carsdale
* Derrick De Marney - Jack Carsdale
* Alma Taylor - Alma Carsdale
* Paul Cavanagh - Captain Darrell Walter Byron - Captain Carsdale
* Julie Suedo - Margaret

==References==
 

==Bibliography==
* Wagg, Stephen. Because I tell a Joke or Two: Comedy, Politics and Social Difference. Routledge, 1998.

==External links==
* 

 

 
 
 
 
 
 
 
 
 


 
 