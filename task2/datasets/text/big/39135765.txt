Boomerang Family
{{Infobox film
| name           = Boomerang Family
| image          = Boomerang_Family_poster.jpg
| caption        = 
| film name      = {{Film name
| hangul         = 고령화 가족
| rr             = Goryeonghwa Gajok
| mr             = Koryŏnghwa Kachok}}
| director       = Song Hae-sung
| producer       = Na Gyeong-chan   Kim Dong-hyun
| screenplay     = Kim Hae-gon   Kim Jae-hwan   Song Hae-sung
| based on       =  
| starring       = Park Hae-il   Yoon Je-moon   Gong Hyo-jin   Youn Yuh-jung   Jin Ji-hee
| music          = Lee Jae-jin
| cinematography = Hong Kyung-pyo
| editing        = Park Gok-ji
| studio         = Invent Stone Corp.
| distributor    = CJ Entertainment
| released       =  
| runtime        = 112 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          =   
}}
 
Boomerang Family ( ; lit. "Aging Family") is a 2013 South Korean comedy-drama film directed by Song Hae-sung,  and starring Park Hae-il, Yoon Je-moon, Gong Hyo-jin, Youn Yuh-jung, and Jin Ji-hee.   Based on the 2010 novel Aging Family by CHEON Myeong-kwan,  the film is about three misfit siblings with a mean average age of 39, who all decide to move back in with their mother.    

==Plot==
Film director In-mo ( ) arrives with her bratty, rebellious 15-year-old daughter Min-kyung (Jin Ji-hee). 35-year-old Mi-yeon announces that she is leaving her second husband, and that she needs to stay with them for the immediate future. In this way, fate has reunited this dysfunctional and rather quirky family, with their petty conflicts, sibling rivalries and largely unexpressed affection, as they struggle with the challenges of middle age.    

==Cast==
* Park Hae-il as Oh In-mo
* Yoon Je-moon as Oh Han-mo
* Gong Hyo-jin as Oh Mi-yeon 
* Youn Yuh-jung as Mom
* Jin Ji-hee as Shin Min-kyung, Mi-yeons daughter
* Ye Ji-won as Han Soo-ja, hairdresser Kim Young-jae as Jung Geun-bae, Mi-yeons boyfriend
* Yoo Seung-mok as drug peddler
* Park Young-seo as Ki-soo, Han-mos thief friend Lee Young-jin as Seo Mi-ok, In-mos wife 
* Jung Young-gi as Jang, Mi-yeons husband
* Kim Hae-gon as Kim, porno producer
* Kim Dae-jin as Choi
* Yoo Yeon-mi as Min-kyungs school friend
* Han Sung-yong as restaurant customer
* Oh Min-ae as landlady
* Woo Hye-jin as Mi-yeons wedding friend
* Hwang Byung-gook as wedding photographer
* Park Geun-hyung as Mr. Gu, Moms male friend Kim Ji-young as Sang-geuns woman
* Yang Hee-kyung as Jung-hyuns woman
* Kim Kyung-ae as Jung-boks woman

==Awards and nominations==
2013 50th Grand Bell Awards
*Nomination - Best Director - Song Hae-sung
*Nomination - Best Actress - Youn Yuh-jung
*Nomination - Best Supporting Actress - Jin Ji-hee

==References==
 

==External links==
*    
*  
*  
*  

 

 
 
 
 
 
 
 