Aw Aaakare Aa
{{Infobox film name = Aw Aaakare Aa image = caption = Movie poster for Aw Aaakare Aa starring = Adyasha Mohapatra Dipti Panda director = Subash Das producer = Subash Das  writer = Subash Das editing = Gadadhar Puty  music = Bidyadhar Sahu cinematography = Jugal Debata  distributor = Om Films released       =   runtime        =   90 minute  country = India language = Oriya
}}
Aw Aaakare Aa ( ) is a 2003 Indian Oriya film directed by Subash Das,This film reflects  change in the present education system and projected another very sensitive issue — the drudgery and defeatism of modern education system  
 

== Synopsis ==
This is the story of a school teacher, Mini. Her efforts to change the conventional pattern of education creates hurdles in her own career. Without being able to compromise, she is frequently transferred from one school to another. Her only solace is her childhood memory which surfaces time and again. Finally Mini does not reconcile with the present educational system, resigns and starts her own school where there are no four-walls and no regimentation.

==Cast==
*Adyasha Mohapatra as Mini (Childhood)
*Dipti Panda as   Mini (Adulthood)

== Crew ==
*Subas Das - Director
*Subas Das - Story & Screenplay
*Subas Das -   Producer
*Gadadhar Puty - Editor
*Debdas Chotray - Dialogue
*Jugala Debata - Director of Photography
*Bidyadhar Sahu - Music
*Subas Sahu - Art Director
*Barik - Sound
*Ambika Das - Costume Design
*Kunal Patnaik - Makeup

== Music ==
Bidyadhar Sahu has arranged music for this film

==Review==
Subas Das has proved his excellence as a thoughtful filmmaker in his first person narrative debut film Aw Aaakare Aa  wherein he has expressed his concern over the flaw full primary education system prevalent in Orissa and many other States of India. However this is the only system of education in rural Orissa.
In this 90 minutes movie Mr. Das has tried to suggest an alternative to the age-old system through his protagonist ‘Mini’ who always wanted a teacher of her choice and finally discovered in her self only. The form itself is the novelty of the film wherein the childhood and the adulthood of the protagonist are presented physically together in the cinema.

==Awards & Participation== National Film Oriya film
*Orissa State Film Awards, (2003) - Best film,Best Direction, Best Child actress and best editing
*13th Golden Elephant International Children’s Film Festival
*International Film Festival at Jamshedpur (Children’s section).
 

== References ==
 

==External links==
* 
* 
* 
*  

 
 
 