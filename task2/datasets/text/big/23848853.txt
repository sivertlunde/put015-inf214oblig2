Daddy Cool (2009 Malayalam film)
{{Infobox film
| name           = Daddy Cool
| image          = Daddy Cool Malayalam.jpg
| director       = Aashiq Abu
| producer       = Bose Kurian Alwyn Antony
| writer         = Aashiq Abu  Dialogues  Bipin Chandran
| starring       =  
| cinematography = Sameer Thahir
| editing        = V. Sajan
| released       =  
| runtime        = 
| studio         = Ananya Films Marikar Films
| distributor    = Marikar Release
| budget         =  6 crore
| gross          = 
 
| pcountry        = India
| language       = Malayalam
| music          = Bijibal

}}
Daddy Cool is a 2009 Malayalam film starring Mammootty, Richa Pallod and Master Dhananjay. The film was the directorial debut of Aashiq Abu.This movie got average  hit status. The locations are Kochi and Hong Kong. The film was dubbed in Tamil and became a superhit.

==Plot==
The story line of the film is the escapades of a cricket crazy kid, all of eight years and his father, a cop. Antony Simon (Mammootty) is a crime branch CI, but prefers to spend his time fooling around with his son Aadi (Dhananjay). And being a Crime Branch police officer, his rather laid-back attitude is not appreciated by his peers and superiors. Richa Pallod portrays his wife who finds this irritating.

The father-son duo becomes friends with the famous national cricketer Sreekanth (Govind Padmasurya) when they save him from some street fight. Once while the father-son duo was out for shopping, Adhi saw a man with the gun and screams at his father. Simon turns back and shoots him but he kills the wrong person. Simon now faces murder charges and is suspended from duty.

Later that day, Aadhi is found missing. Simon being a cop, the entire police force in the district is in search. They find that the one who was killed by Simon was part of the gang who were trying to get Sreekanth into a match-fixing deal and now they are trying to get revenge on Simon for killing one of their gang members. The rest of the film is how Simon fights the gang and gets his kid back from them with idiotic ways.

==Cast==
*Mammootty as CI Antony Simon aka Daddy Cool
*Master Dhananjay as Adi Simon
*Govind Padmasoorya   as  Sreekant
*Richa Pallod as Annie Simon
*Biju Menon as Roy
*Daniel Balaji as Shiva Vijayaraghavan
*Saikumar Saikumar
*Baburaj Baburaj
*Suraj Venjaramood
*Babu Swamy as Annies father
*Rajan P. Dev as Roys father
*Vinayakan
*Ashish Vidyarthi as Bheem Bhai Radhika as Annies sister
*Gopakumar
*Bipin Chandran as News reader (guest appearance)
* Ambika Mohan as Annies mother
*Lena Abhilash as Adis teacher

==Critical reception==
The film has received mixed  reviews in kerala. It was considered as an average at box office.

==Soundtrack==
The film features a successful soundtrack, consisting of 6 songs, composed by Bijibal.

{{Track listing
| extra_column = Artist(s)
| lyrics_credits = yes
| title1 = Daddy Cool
| extra1 = Gayatri Suresh
| lyrics1 =R. Venugopal
| title2 = Daddy My Daddy
| extra2 = Swetha Mohan, Sanand Unnithan, Aditya Krishna, Amrutha Krishna, Anju Bhaskaran
| lyrics2 = Santhosh Varma
| title3 = Kadha
| extra3 = Bijibal
| lyrics3 = Anil Panachooran
| title4 = Kadhayoru Hariharan
| lyrics4 = Anil Panachooran
| title5 = Samba
| extra5 = Jassie Gift
| lyrics5 = Santhosh Varma
| title6 = Samba Salsa
| extra6 = Anuradha Sriram, Jassie Gift
| lyrics6 = Santhosh Varma
}}

==References==
 

==External links==
*    
*  
 
 
 
 
 
 
 