Body & Soul: Diana & Kathy
{{Infobox film
| name           = Body & Soul: Diana & Kathy
| image          =
| image size     =
| caption        =
| director       = Alice Elliott
| producer       = Alice Elliott   Simone Pero Audi
| writer         =
| narrator       =
| starring       =
| music          = Rick Baitz
| cinematography = Alice Elliott
| editing        = Rose Rosenblatt
| distributor    = New Day Films
| released       =
| runtime        =
| country        =
| language       =
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}

Body & Soul: Diana & Kathy, directed by Alice Elliott, is a 2007 documentary film that focuses on the symbiotic relationship between Diana Braun, who has Down Syndrome, and Kathy Conour, who has cerebral palsy. The two activists for the rights of people with disabilities met three decades ago and vowed to fight to live independent lives together.

Body & Soul: Diana & Kathy made its television debut on PBS in October 2009.

Kathy Conour died in September 2009. 

== Nominations and Awards ==
The film has screened at over 30 film festivals, including The Heartland Film Festival, Superfest, and The Mill Valley Film Festival. The film was recipient of the 2007 AAIDD Media Award and the Council on Foundations 2008 Henry Hampton Award for Excellence in Film and Digital Media.

==References==

 

==External links==
*  
*  
*  
*  
*  

 
 
 
 

 