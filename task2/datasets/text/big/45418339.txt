Outpost 37
{{Infobox film
| name           = Outpost 37
| image          = 
| alt            = 
| caption        = 
| film name      = 
| director       = Jabbar Raisani
| producer       = 
| writer         = Jabbar Raisani
| screenplay     = {{Plainlist|
* Blake Clifton
* Jabbar Raisani }}
| story          = 
| based on       =  
| starring       = {{Plainlist|
* Adrian Paul
* Reiley McClendon
* Rick Ravanello }}
| narrator       = 
| music          = Theo Green
| cinematography = Blake Clifton
| editing        = 
| studio         = {{Plainlist|
* Altitude Film Entertainment 
* Bigscope Films 
* Out of Africa Entertainment 
* Ravens Nest Entertainment 
* Rooks Nest Entertainment }}
| distributor    = {{Plainlist|
* IFC Films
* IFC Midnight
* Viva Films
* IPA Asia Pacific }}
| released       =  
| runtime        = 90 Minutes
| country        = {{Plainlist|
* UK
* South Africa }}
| language       = English
| budget         = 
| gross          =  
}}

Outpost 37 (also known as Alien Outpost) is a 2014 science fiction action thriller film. The film follows a documentary crew sent to record the daily lives of the soldiers of Outpost 37, 12 years after the initial Alien invasion.

==Plot==
The film opens with a documentary style brief history recap of the past 12 years. "2021: Earth was invaded. 2022: The Heavies retreated, leaving thousands of their soldiers behind. 2023: The USDF Outpost were created to seek and destroy all Heavies left on Earth. 10 years later, the Outposts that once defended us have been reduced or dismantled. The few that remain are undermanned and underfunded."

In 2033, a documentary crew along with a pair of soldiers are sent to Outpost 37 in the Middle East. Upon arriving at Outpost 37, not receiving a response from inside the base and hearing gunfire, they storm in. Seeing the base soldiers firing at the nearby terrain, they join in, in an attempt to support them. This proves to be a joke played on the new arrivals shortly later. 

The film continues to follow the lives of the soldiers, their interactions (both off duty and during sporadic combat with local combatants). The film also cuts to short documentary clips of the years after the initial alien invasion (governments collapsing, new orbital defenses being built, etc.). 

 

==Cast==
* Adrian Paul as General Dane
* Reiley McClendon as Ryan Andros  new soldier -->
* Rick Ravanello as Spears
* Douglas Tait as The Heavy
* Joe Reegan as Alex Omohundro  
* Andy Davoli as Savino (voice)
* Nic Rasenti as Harty
* Matthew Holmes as North
* Sven Ruygrok as Frankie Forello  
* Brandon Auret as Savino
* Scott E. Miller as John Wilks  
* Jordan Shade as The PMC Soldier
* Kenneth Fok as Zilla
* Darron Meyer as Roger Hollis  
* Stevel Marc as Righty
* Justin Munitz as Hans
* Michael Dube as Brick
* Lemogang Tsipa as Mac
* Khalil Kathrada as Saleem
* Tyrel Meyer as Duke
* Tapiwa Musvosvi as Tyrone "Bones" Ridell  
* Edwin Jay as Soldier
* Craig Macrae as Lefty
* Sherwyn Budraj as Soldier

==Critical reception==
Outpost 37 received generally unfavorable reviews from critics. According to review aggregator Metacritic, the film has a score of 26 out of 100. 

==References==
 

==External Links==
*  
* https://www.facebook.com/pages/Outpost-37-aka-Alien-Outpost/371504959622108

 