The Eddy Duchin Story
{{Infobox film
| name           = The Eddy Duchin Story
| image          = Eddyduchinstorymp.jpg
| caption        = DVD cover
| director       = George Sidney
| producer       = Jerry Wald Jonie Taps
| writer         = Samuel A. Taylor Victoria Shaw   James Whitmore  Shepperd Strudwick
| music          = George Duning
| cinematography = Harry Stradling
| editing        = Viola Lawrence Jack Ogilvie
| distributor    = Columbia Pictures
| released       = June 21, 1956
| runtime        = 123 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $5.3 million (US)  
}}
 Academy Award nomination for his cinematography in the CinemaScope film. The film received four nominations in total and was one of the highest-grossing films of 1956. Incorporating signature elements of Duchins style into his own original style, Carmen Cavallaro performed the piano music for the film.

Soundtrack recordings: In 1956 and 1957 respectively, two musical "soundtrack" recordings, that is, studio recordings of songs from the film, were issued.  Twelve of the films songs were released in The Sound Track Album, The Eddy Duchin Story, with Carmen Cavallaro at the piano. This recording was issued by Decca in 1956 (mono) as DL 8289 and reissued in stereo in 1965 as Decca DL 78289 (which was also issued in Mexico and Canada).  In 1957, Capitol Records issued an LP album entitled,  Selections from The Eddy Duchin Story (Capitol T-716), featuring nine of the original albums twelve soundtracks. Accompanied by the Harry Geller Orchestra, pianists George Greeley and Harry Sukman performed the selections. Somewhat ironically, both pianists imitated (usually quite closely and rather ably) Cavallaro’s beloved interpretations of the songs rather than Duchin’s. Given the extraordinary commercial success of the original soundtrack, it is no wonder. 

Some of the films box office success can be attributed to the appearance of Novak in ads for No-Cal diet soda. Novak became one of the first celebrities to be featured in advertisements for soft drinks, and each ad also featured a reminder to see Novak in The Eddy Duchin Story.

Musician Peter Duchin, whose relationship with his father is a major subject of the film, has written very negatively about the script, saying there was too much unnecessary fictionalization of his parents lives and deaths.

==Plot==
Fresh out of pharmacy school, young Eddy Duchin travels to New York in the 1920s to take a job playing piano for bandleader Leo Reismans orchestra. But upon arrival, Eddy learns there is no such job.

A wealthy socialite, Marjorie Oelrichs, overhears his playing and takes a personal interest in Eddy. When he is invited to the home of her wealthy aunt and uncle, the Wadsworths, for a party, Eddy is disappointed to discover that he has been asked there merely to entertain.

Having fallen in love, Marjorie goes so far as to propose marriage to Eddy rather than the other way around. She has secret fears that she expresses on their wedding night, and tragedy strikes when Marjorie dies giving birth to their child.

An anguished Eddy abandons his baby boy, Peter, leaving him in the Wadsworths care, and goes away from New York for many years. He serves on a warship in the war. Finally persuaded to visit his son, he meets Peters governess, a British woman named Chiquita, who grows on him after an uneasy start. Peter is learning to play the piano.

Eddy has an engagement at the Waldorf-Astoria hotel, but his hand freezes while at the keyboard. He eventually is diagnosed with a fatal illness and has no more than a year to live. After he marries Chiquita, he cant bring himself to tell Peter about his illness, so he simply says that soon hell be "going away." Peter ultimately learns the truth.

==Cast==
* Tyrone Power as Eddy Duchin
* Kim Novak as Marjorie Oelrichs Victoria Shaw as Chiquita Wynn
* James Whitmore as Lou Sherwood
* Larry Keating as Leo Reisman

==Other soundtrack recordings== Al Lerner, A Tribute to Eddy Duchin. Released by Tops Records in 1957, this release featured the following tunes: 
* "Manhattan (song)|Manhattan"
* "Nocturne in E Flat"
* "Starlight Concerto"
* "Gee, Baby, I Aint Too Good to You"
* "Shine"
* "Night Dreams"
* "My Heart Belongs to Daddy"
* "It Must Be True"
* "I Cant Give You Anything But Love, Baby"
* "Bésame Mucho"
* "Love Walked In" 
* "Whispering"
(A special detail from the vinyl record of this tribute is that it is not black, but is made of yellow translucent material, with some brown figures in veneer)

==Awards==
The film was nominated for four Academy Awards.   
* Cinematography (Color) - Harry Stradling
* Music (Scoring of a Musical Picture) - Morris Stoloff, George Dunning Sound Recording - Columbia Studios Sound Department -John Livadary
* Writing (Motion Picture Story) - Leo Katcher

==References==
 

==Further reading==
*  

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 