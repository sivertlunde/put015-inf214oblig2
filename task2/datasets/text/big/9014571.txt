At Midnight I'll Take Your Soul
 
{{Infobox film
| name           = At Midnight Ill Take Your Soul
| image          = Marins_poster5.jpg
| caption        = Theatrical release poster.
| director       = José Mojica Marins
| producer       = Arildo Iruam Geraldo Martins Simões Ilídio Martins Simões
| writer         = Waldomiro França José Mojica Marins Magda Mei
| starring       = José Mojica Marins
| music          = Salatiel Coelho Hermínio Gimenez
| cinematography = Giorgio Attili
| editing        = Luiz Elias
| studio         = Indústria Cinematográfica Apolo
| distributor    = N.T.M. 
| released       = November 9, 1964
| runtime        = 84 minutes
| country        = Brazil
| language       = Portuguese
| budget         =
}}
 Brazilian horror film directed by José Mojica Marins. Marins is also known by his created alter ego Coffin Joe (Zé do Caixão). It is also Brazils first horror film,       and it marks the first appearance of Marins character Zé do Caixão (Coffin Joe). The film is the first installment of Marins "Coffin Joe trilogy", and is followed by This Night Ill Possess Your Corpse (1967 in film|1967), and Embodiment of Evil (2008 in film|2008).

==Plot==
 
The film is set in an unnamed Brazilian small town. Zé do Caixão (Coffin Joe), the local undertaker who disdains religion and emotion and who believes the only thing that matters is the "continuity of the blood" (specifically his own), is looking for the "perfect woman" to bear him a superior child who will be immortal. Since his wife Lenita has been found to be unable to bear children, Coffin Joe begins to make advances with Terezinha, the fiancée of Joes friend Antonio. Terezinha scolds him by telling him that Antonio is the only man in her life. During a Catholic holiday, Joe kills his wife Lenita (because of her infertility) by tying her up and having a venomous spider bite her. The local authorities cannot find a clue to arrest him and he remains free to do whatever he wants.
 gypsy who will tell the fortune of Antonios marriage with Terezinha. The gypsy reveals, however, that there is going to be a tragic disaster, and the two will never get married. Joe, in response, calls her a fraud and states that the supernatural is a hoax. She warns him not to mock the supernatural forces, lest they make him pay. That night, Joe and Antonio go to Antonios house, where Antonio tells Joe that he really didnt believe the witchs words, and that he expects to marry Terezinha and have a happy life together. Fulfilling the witchs prophecy, Coffin Joe brutally bludgeons and then drowns Antonio in a bathtub.
 canary for kill herself, then return to take his soul to hell. Joe laughs at her, but the next day she is found hanging in her home. To his surprise, she doesnt blame him in her suicide note. Meanwhile, the villages Dr. Rudolfo begins to suspect Joe for the recent outbreak of violent deaths that has occurred. When Joe becomes aware of the doctors suspicions, Coffin Joe appears at Dr. Rudolfos home, gouges his eyes with his long fingernails and sets him on fire.

Time passes, and Coffin Joe remains unpunished for his crimes. On the Day of the Dead he meets Marta, a young woman who is visiting her relatives, and decides to choose her as his next perfect woman. Joe escorts Marta home late at night, only to be confronted by the gypsy who predicted the doom of Antonio and Terezinha. She tells Joe that his soul shall be claimed by the ghosts of those he murdered and Satan at midnight. Joe threatens the gypsy, but after leaving Marta at her destination, he is soon visited by ghostly apparitions that test his courage. Realizing he cannot face the apparitions, Joe runs away, and coincidentally arrives at the mausoleum where Antonio and Terezinha were buried. Finally at the edge of his sanity, Joe opens the coffins to prove to himself his victims are really dead, but instead sees that their eyes are open, as if they are still alive, faces crawling with maggots. Some time later, the villagers come to the mausoleum after hearing Coffin Joes blood-curdling screams and find him lying on his back, horribly disfigured, his eyes bulging open similar to the eyes of the two corpses. At that same time, the bells of the local church ring, announcing the stroke of midnight.

==Cast==
*José Mojica Marins as Zé do Caixão
*Magda Mei as Terezinha
*Nivaldo Lima as Antônio
*Valéria Vasquez as Lenita
*Ilídio Martins Simões as Dr.Rodolfo
*Eucaris Moraes as Velha Bruxa
==Production==
 

==Release==
 

==Reception==
 
==References==
 

== External links ==
*   
* 
* 
*  on    
*   

 

 
 
 
 
 
 
 
 