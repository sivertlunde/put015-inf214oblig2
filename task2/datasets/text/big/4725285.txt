De Schippers van de Kameleon
{{Infobox film
| name           = De Schippers van de Kameleon
| image          = Film poster De Schippers van de Kameleon.jpg
| caption        = Film poster
| director       = Steven de Jong Marc Willard
| producer       =
| writer         = Hotze de Roos (novel) Jean Ummels (screenplay)
| narrator       =
| starring       = Koen van der Donk Jos van der Donk Maarten Spanjer Saar Koningsberger Joep Sertons
| music          = Ronald Schilperoort
| cinematography = François Perrier (cinematographer)|François Perrier Anton Stoelwinder
| editing        = Paul Bruisjens
| distributor    =
| released       =  
| runtime        = 100 Mins
| country        = Netherlands
| language       = Dutch
| budget         =
}}
De Schippers van de Kameleon (2003) is a Dutch family film with as main characters identical twins Koen van der Donk (Hielke Klinkhamer) and Jos van der Donk (Sietse Klinkhamer). The actors are also identical twins, born March 6, 1988. The film is based on the books of Hotze de Roos about the adventures of the twins with their boat, the opduwer De Kameleon.

The film received a Golden Film (100,000 visitors) and a Platinum Film (400,000 visitors) and with 750.000 viewers in cinemas. It was the best viewed Dutch film production in 2003. In 2006 it was chosen as "Best Childrens Movie" at the 4th International Film Festival in Eilat, Israel. {{cite news 
   | title = De Kameleon beste kinderfilm in Israël
   | trans_title = De Kameleon best childrens movie in Israel
   | url = http://www.volkskrant.nl/kunst/article317779.ece/De_Kameleon_beste_kinderfilm_in_Israel
   | agency = ANP
   | quote = De Schippers van de Kameleon is bekroond als beste kinderfilm op het Filmfestival van Israël. Dat heeft regisseur Steven de Jong donderdag gemeld. De jury bestond uit Israëlische kinderen. (De Schippers van de Kamelon was crowned best childrens movie at the Film Festival of Israel. This was reported by director Steven de Jong on Thursday. The jury consisted of Israeli children.)
   | work = Volkskrant
   | language = Dutch
   | date = 2006-06-08
   | accessdate = 2010-03-20
  }}  {{cite news
   | script-title=he:פסטיבל הסרטים באילת - פרסים והוקרות
   | trans_title = Film Festival in Eilat - Awards and Honors
   | url = http://www.news1.co.il/ArticlePrintVersion.aspx?docId=101952&subjectID=1
   | work = News1
   | first = Shosh
   | last = Maimon
   | quote = קברניטי הקאמיליון ההולנדי, בבימויו של סטיבן דה יונג, הוא סרט הילדים הטוב ביותר. (De Schippers van de Kameleon, directed by Steven de Jong, is the best childrens movie.)
   | language = Hebrew
   | date = 2006-05-29
   | accessdate = 2009-05-15
  }}  {{cite news
   | quote = סרט הילדים ההולנדי "קברניטי הקאמליון", בבימויו והפקתו של סטיבן דה יונג, זכה בפרס "סרט הילדים הטוב ביותר" על פי בחירת ילדי אילת, אשר שפטו בתחרות. (The Dutch childrens movie De Schippers van de Kameleon, directed and produced by Steven de Jong, won the prize of The Best Childrens Movie, upon the choice of the children of Eilat, who judged the competition.)
   | title =  5000 איש צפו בפסטיבל הקולנוע הבינלאומי באילת
   | trans_title = 5000 people watched the international film festival in Eilat
   | url = http://www.eilatredc.co.il/article.asp?id=2707
   | work = Red Sea Communications
   | first = Ronit
   | last = Zilberstein
   | date = 2006-05-28
   | accessdate = 2009-05-15
   | language = Hebrew
  }} 
   
The film also has a sequel, Kameleon 2, with the same characters and actors.

==References==
 

==External links==
*   
* 

 
 
 
 
 
 
 


 
 