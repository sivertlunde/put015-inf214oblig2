Mega Mendoeng
 
 
{{Infobox film
| name           = Mega Mendoeng
| image          = Poster mega mendoeng.jpg
| image_size     = 
| border         = 
| alt            = An advertisement
| caption        = Advertisement
| director       = Boen Kim Nam
| producer       = Ang Hock Liem
| screenplay     = 
| narrator       = 
| starring       ={{plain list|
*Rd Soekarno
*Oedjang
*Boen Sofiati
*Soehaena
}}
| music          = 
| cinematography = 
| editing        = 
| studio         = Union Films
| distributor    = 
| released       =  
| runtime        = 
| country        = Dutch East Indies
| language       = Indonesian
| budget         = 
| gross          = 
}}
Mega Mendoeng ( ;   (now Indonesia) directed by Boen Kin Nam and produced by Ang Hock Liem for Union Films. Starring Rd Soekarno, Oedjang, Boen Sofiati, and Soehaena, it follows two young lovers who are separated by lies but ultimately reunite at the village of Mega Mendoeng in Bogor. This film, Unions seventh and final production, was shot concurrently with Soeara Berbisa and completed over a period of three months. It was released in early 1942 and screened as late as July of that year, but may now be lost film|lost.

==Plot== marry his cousin Fatimah, on threat of being disowned. To spare her husband from this fate, Retnaningsih decides to abandon him and moves from Bandung to Batavia (now Jakarta), where she lives a life of poverty. Unknown to her, this decision is used by Raden Koesoema as proof of her that she was not trustworthy. Winanta falls into despair, but eventually marries Fatimah.

Eighteen years later, Winanta and Fatimah have had a daughter named Koestini (Boen Sofiati), who is studying in Batavia. Educated, graceful, and beautiful, she is popular with young men but only returns the affections of Soedjono (Rd Soekarno), a young assistant pharmacist who is polite and refined. One of her spurned suitors, Soekatma, decides to ruin their relationship by telling Winanta that Koestini has spent all of her time chasing boys rather than studying. Believing these lies, Winanta recalls Koestini to Bandung.

Koestini falls ill, and her death is reported. Driven mad by the news, Soedjono begins wandering aimlessly. Ultimately, as if guided by some unseen force, he discovers Koestini alive in a village called Mega Mendoeng, near Bogor. This discovery brings him back to his senses, and the two are able to live happily together.  and  .}}

==Production==
  Batavia (now Jakarta) which was run by Tjoa Ma Tjoen and financed by Ang Hock Liem. A sound technician by training, Mega Mendoeng was Boens only full directorial credit. Liem served as producer. The film was announced in September 1941 and produced concurrently with Soeara Berbisa (Venomous Voice), a film on which Boen acted as assistant director.  Mega Mendoeng was completed by December 1941, after Soeara Berbisa. 
 realist lines and aimed at educated viewers,  owing to the inclusion of stage actors&nbsp;– people who usually entertained lower class audiences&nbsp;– the Indonesian film historian Misbach Yusa Biran suggests that this was untrue. 

==Release and legacy== Japanese invasion looming, the February 1942 edition of the film magazine Pertjatoeran Doenia dan Film reported that several studios would move away from the colonial capital of Batavia or go on a production hiatus. Union, though already beginning production of a film set in the Majapahit era titled Damar Woelan, was forced to close shop;  it never reopened.  Soekarno returned to the film industry in the 1950s and was active until the 1970s, mostly credited as Rendra Karno. 

Mega Mendoeng was screened as late as July 1942,  but may now be lost film|lost. Movies in the Indies were recorded on highly flammable nitrate film, and after a fire destroyed much of Produksi Film Negaras warehouse in 1952, old films shot on nitrate were deliberately destroyed.  As such, the American visual anthropologist Karl G. Heider suggests that all Indonesian films from before 1950 are lost.  However, J.B. Kristantos Katalog Film Indonesia records several as having survived at Sinematek Indonesias archives, and Biran writes that several Japanese propaganda films have survived at the Netherlands Government Information Service. 

==Explanatory notes==
 

==References==
 

==Works cited==
 
*{{Cite book
 |title=Apa Siapa Orang Film Indonesia 1926–1978
 |trans_title=What and Who: Film Figures in Indonesia, 1926–1978
 |year=1979
 |oclc=6655859
 |ref=harv
 |location=Jakarta
 |language=Indonesian
 |publisher=Sinematek Indonesia
 |editor-last=Biran
 |editor-first=Misbach Yusa
}}
* {{cite book
  | title =  
  | trans_title = History of Film 1900–1950: Making Films in Java
  | language = Indonesian
  | last = Biran
  | first = Misbach Yusa
  | authorlink = Misbach Yusa Biran
  | location = Jakarta
  | publisher = Komunitas Bamboo working with the Jakarta Art Council
  | year = 2009
  | isbn = 978-979-3731-58-2
  | ref = harv
  }}
*{{cite book
 |title=Indonesia dalam Arus Sejarah: Masa Pergerakan Kebangsaan
 |trans_title=Indonesia in the Flow of Time: The Nationalist Movement
 |language=Indonesian
 |last=Biran
 |first=Misbach Yusa
 |chapter=Film di Masa Kolonial
 |trans_chapter=Film in the Colonial Period
 |publisher=Ministry of Education and Culture
 |year=2012
 |volume=V
 |pages=268–93
 |isbn=978-979-9226-97-6
 |ref=harv
}}
*{{cite book
 |url=http://books.google.ca/books?id=m4DVrBo91lEC
 |title=Indonesian Cinema: National Culture on Screen
 |isbn=978-0-8248-1367-3
 |author1=Heider
 |first1=Karl G
 |year=1991
 |publisher=University of Hawaii Press
 |location=Honolulu
 |ref=harv
}}
*{{Cite news
 |url=http://niod.x-cago.com/maleise_kranten/article.do?code=Niod061&date=19420708&id=061-19420708-005014&words=Mega%20Mendoeng%20mega%20mendoeng
 |title=Film2 yang Dipertoendjoekkan oleh Bioscoop2 di Djakarta Ini Malam (8 Djoeli 2602)
 |trans_title=Films Showing in Theatres in Jakarta Tonight (8 July 2602)
 |language=Indonesian
 |work=Pembangoen
 |page=5
 |date=8 July 1942
 |location=Jakarta
 |ref= 
}}
*{{Cite journal
 |title=Mega Mendoeng
 |language=Indonesian
 |work=Pertjatoeran Doenia dan Film
 |date=December 1941
 |pages=19
 |volume=1
 |issue=7
 |location=Batavia
 |ref= 
}}

* {{cite web
  | title = Mega Mendoeng
  | language = Indonesian
  | url = http://filmindonesia.or.id/movie/title/lf-m012-41-863083_mega-mendoeng#.UBCYuqDE_Mw
  | work = filmindonesia.or.id
  | publisher = Konfiden Foundation
  | location = Jakarta
  | accessdate = 26 July 2012
  | archiveurl = http://www.webcitation.org/69QvqhyFh
  | archivedate = 26 July 2012
  | ref =  
  }}
* {{cite web
  | title = Oedjang
  | language = Indonesian
  | url = http://filmindonesia.or.id/movie/name/nmp4b99ab644c6ce_oedjang/filmography
  | work = filmindonesia.or.id
  | publisher = Konfiden Foundation
  | location = Jakarta
  | accessdate = 20 May 2014
  | archiveurl =http://www.webcitation.org/6Phf9z8dy
  | archivedate = 20 May 2014
  | ref =  
  }}
*{{Cite journal
 |title=Soeara Berbisa
 |trans_title=Venomous Voice
 |language=Indonesian
 |work=Pertjatoeran Doenia dan Film
 |date=October 1941
 |pages=29
 |volume=1
 |issue=5
 |ref=harv
 |location=Batavia
 |ref= 
}}
*{{Cite journal
 |title=Warta dari Studio
 |trans_title=Reports from Studios
 |language=Indonesian
 |work=Pertjatoeran Doenia dan Film
 |date=September 1941
 |pages=26–28
 |volume=1
 |issue=4
 |location=Batavia
 |ref= 
}}
*{{Cite journal
 |title=Studio Nieuws
 |trans_title=Studio News
 |language=Indonesian
 |work=Pertjatoeran Doenia dan Film
 |date=February 1942
 |pages=18–20
 |volume=1
 |issue=9
 |location=Batavia
 |ref= 
}}
*{{Cite journal
 |title=Tanja Djawab
 |trans_title=Questions and Answers
 |language=Indonesian
 |work=Pertjatoeran Doenia dan Film
 |date=February 1942
 |page=25
 |volume=1
 |issue=9
 |location=Batavia
 |ref= 
}}
*{{Cite journal
 |title=Tirai Terbentang
 |trans_title=Open Curtains
 |language=Indonesian
 |work=Pertjatoeran Doenia dan Film
 |date=December 1941
 |pages=28–29
 |volume=1
 |issue=7
 |location=Batavia
 |ref= 
}}
 

 
 

 
 
 