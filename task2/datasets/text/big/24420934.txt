Das Herz der Königin
{{Infobox film
| name           = Das Herz der Königin
| director       = Carl Froelich UFA (Berlin)
| writer         = Harald Braun
| starring       = Zarah Leander
| music          = Theo Mackeben
| cinematography = Franz Weihmayr
| editing        = Gustav Lohse
| distributor    = 
| released       = 1940
| runtime        = 112 minutes
| country        = Germany
| language       = German
}}
 English and Scottish propaganda purposes, in the context of the Second World War going on at the time.

==Plot==
The film starts with Mary Stuart, Queen of Scots, held prisoner in Fotheringhay Castle, awaiting the final judgement in her case, which is expected within a few hours. Soon she finds out that the Royal Court has sentenced her, with the assent of Queen Elizabeth I, to be executed on the scaffold on the following day. She breaks down and remembers the events leading to her now imminent death. The bulk of the film consists of this Flashback (narrative)|flashback.
 Jacob Stuart, Lord Bothwell, face Mary Stuart critically. Moreover, immediately upon her arrival Mary faces an assassination attempt with poisoned wine, by Johanna (Jean) Gordon, whose Clan Gordon was at feud with the Stuarts for many years.
 Privy Council, whom Mary summons to deal with the first political decisions to be announced, do not show up. The only one present is Jacob Stuart, who tears up the document presented for his signature. When the Queen is alone again Lord Bothwell arrives and confesses that he had fallen in love with her. Mary Stuart orders him arrested for insulting the Queen, even though she is impressed by his demeanour.

Meanwhile Queen Elizabeth I, angered that with a lawful Catholic queen taking over Scotland will limit her influence there and even threaten her right to the throne in England, sends to Scotland her confidant Henry Darnley - who is both an English peer and a Scottish lord - to spy for her and to raise the population against Mary Stuart.

Nevertheless, Darnley himself falls in love with Queen Mary and leads her to lord Bothwells Castle, where the Scottish lords meet secretly to plot the Queens deposition. Mary ventures alone into the meeting,  and is imprisoned.

She is released the next day, but only to after having been made to swear an oath that she would  marry a Scot. Mary Stuarts choice falls on lord Bothwell, whom she believes to be still in captivity. It turns out, however, that he had fled together with Jean Gordon, and has married her. The two of them raise an army with the aim of overthrowing Mary –  Jean wants revenge on the Queen and the Stuarts, while Lord Bothwell wants power.
 King James I.

A troupe of itinerant actors stages a play hinting broadly that Riccio is the Queens lover and might be the father of her child. Lord Darnley feels mocked and indignant and therefore authorises a plot in which Riccio is assassinated.

At this time Lord Bothwells army appears, which The Queen allows to enter Lord Bothwells Castle and temporarily take over power in the land "for the Queens own protection". Lord Darnley has meanwhile fallen ill with smallpox. On the advice of Lord Bothwell, now her lover, Mary has her ill husband brought to Edinburgh, where he dies in an explosion at his home.

Now Queen Elisabeth sends an army to Scotland to release Mary Stuart from the power of Bothwell and to offer to her refuge in England, which is actually a trap meant to imprison Mary and keep her away from the throne of Scotland.

Meanwhile, Mary Stuart had married Lord Bothwell, who had separated from Jean Gordon. The wedding is interrupted when the English army appears under the guidance of Jacob Stuart, the Queens half-brother, who presents secret love letters which Mary Stuart had sent to Bothwell while still married to her previous husband. Olivier, the Queens page, is killed while attempting to hide the letters.

Lord Bothwell is faced with the choice of standing by Mary Stuart and dying, or denying her. He turns away from her, but the treacherous Jacob Stuart still sentences him to death by dragging (in actual history he fled to Denmark whose King treated him cruelly and where he eventually died in prison; all of which is not mentioned in the film). When Jacob Stuart takes from his sister her child James, to protect him against Queen Elizabeth, Mary Stuart accepts the offer of the English Queen and goes into exile in England, which sets her path to execution.

The frame story from the beginning is now taken up again. Mary Stuart, in a stunning bejeweled red gown, is led to the scaffold and kneels down in prayer as she awaits the sharp hatchet to fall.

==Cast==
The cast included
*  
*  )
*  )
*  
*  )
*   (Henry Stuart, Lord Darnley)
*  
* Will Quadflieg: Page Olivier
* Hubert von Meyerinck: Sir John
* Erich Ponto: Itinerant actor
* Ursula Herking: Member of the itinerant actors troupe
* Hans Hessling: Nelson, Henry Darnleys Companion
*  
*  )
* Rudolf Klein-Rogge: Ruthven, General of the Scottish Palace Guard
* Eduard von Winterstein: An English General
* Josef Sieber: Paris, a Scottish prison guard
*  

==The heart as a theme==
The theme of the hearts emotions is repeated throughout the whole film. The cold Elizabeth I who had obviously edged any love out of her own life is contrasted with the extremely emotional Mary Stuart, whose life is filled (as presented in the film) by love. When Queen Elizabeth learns of the birth of the Scottish heir to the throne gets, she exclaims: "I rule and she loves!". One of the last lofty expressions Mary are in the film: "Kings win in life - hearts, in eternity".

Mary Stuart appears guided exclusively by emotions. She says: "My heart has guided me; it is the only thing that leads me." The biggest mistake of her life - the marriage with Lord Darnley, shows the exception: "It is about the crown, not about your heart" Darnley says in the film. Nevertheless, it is noted that their common son, the future King James I would be a good ruler but - as one of the courtiers says to Queen Mary -  he will also "have a big heart".

==Production==

"Das Herz der Königin" was filmed from November 1939 to March 1940 in the Babelsberg film studio.

On October 29, 1940, the film censorship board classified the film as "unsuitable for minors" but otherwise "valuable" both "artistically" and "culturally".

The premiere took place on November 1, 1940 in the München Ufa-Palace ("Luitpold-Theater")

The film is punctuated by numerous songs, mostly sung by Zarah Leander:

* "A black stone, a white stone" (Zarah Leander)
* "Where is your heart" (Zarah Leander)
* "Slumber song" (Zarah Leander)
* "Once a foreign guest comes to you" (Zarah Leander, Friedrich Benfer)
* "If only you do not think of it, nothing will hurt you" (Erich Ponto)

The text was written by Harald Braun, the music by Theo Mackeben.

*In 1941 Harald Braun published a novelization, also titled Das Herz der Königin.

==Criticism==
Das Herz der Königin became a failure in its time and also today counts as one of the weakest of  Zarah Leanders films.

The International Film Encyclopedia criticized the film as a work from the Nazi era with a strong anti-British bias:  "The depiction of the cold ruler Elizabeth I was aimed at presenting the early history of British imperialism and its  striving for world domination, which would have consequences for all parts of the Earth during centuries up to the present “, thus implicitly justifying Germanys war with Britain at the time. 

Moreover, critics noted that "The extreme miscasting of Zarah Leander in the leading role added to the films kitsch an unintended comic element", that "The film is superficial and pseudo-historical" and that "In her stiff splendid evening gowns as Mary Stuart, Leander could hardly move". 

==References==

 

 
 
 
 
 
 
 
 
 
 
 
 