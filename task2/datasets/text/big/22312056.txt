Symmoria eraston
{{Infobox film
| name           = Symmoria eraston Συμμορία εραστών
| image          =
| image size     =
| caption        =
| director       = Vangelis Serdaris
| producer       =
| writer         = Assimakis Gialamas Kostas Pretenteris
| starring       = Dinos Iliopoulos Maro Kontou Nikos Rizos Kaiti Panou Aliki Zografou Vasos Andrianos Panos Velias Thanos Martinos Andreas Tsakonas Vasos Andronidis Elsa Rizou Gogo Antzoletaki
| music          =
| cinematography =
| editing        =
| distributor    = Klearchos Konitsiotis 1972
| runtime        = 96 min
| country        = Greece Greek
| cine.gr_id     = 201221
}}
 1972 Greece|Greek black-and-white film starring Dinos Iliopoulos, Maro Kontou, Nikos Rizos and Kaiti Panou.

==Cast==
*Dinos Iliopoulos
*Maro Kontou
*Nikos Rizos
*Kaiti Panou
*Aliki Zografou
*Vasos Andrianos
*Panos Velias
*Thanos Martinos
*Andreas Tsakonas
*Vasos Andronidis
*Elsa Rizou
*Gogo Antzoletaki

==See also==
*List of Greek films

==External links==
* 
* 

 
 
 
 

 