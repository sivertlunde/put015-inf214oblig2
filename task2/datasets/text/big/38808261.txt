The Legend of Sarila
 
 
{{Infobox film
| name = The Legend of Sarila
| film name = La Légende de Sarila
| image =
| director = Nancy Florence Savard
| producer = {{plainlist|
* Marie-Claude Beauchamp
* Paul Risacher
* Nancy Florence Savard
}}
| writer = {{plainlist|
* Roger Harvey
* Paul Risacher (English adaptation)
* Pierre Tremblay
}}
| starring =
| music = Olivier Auriol
| cinematography =
| editing = {{plainlist|
* Arthur Tarnowski
* Robert Yates
}}
| production companies = {{plainlist|
* 10th Ave Productions
* CarpeDiem Film & TV
}}
| distributor = Phase 4 Films
| released =  
| runtime = 82 minutes
| country = Canada
| language = French
| budget = Canadian dollar|$8.5 million
}} Inuit shaman, shaman at home repeatedly attempts to kill him and undermine their quest.
 the Finnish mimic Walt Disney Animation Studios|Disneys Frozen (2013 film)|Frozen, which resulted in a lawsuit with Disney.

== Plot ==

The young orphan Markussi lives with his little sister Mipoulok in an Inuit clan. Markussi can talk to animals and has some special powers unique to shamans. He does not want to be a shaman though and mainly keeps his powers a secret because he fears being a shaman would make him like the clans shaman Croolik. Croolik grew selfish and unjust after the deaths of his sons, who died along with Markussis father in an accident on a hunting trip. Croolik blames his sons deaths on his now-separated wife Saya (whom he accuses of having let their sons go hunting at too young an age) and Markussis father; by extension, Croolik also hates Markussi.
 Sedna and tries to call on the spirit of darkness. In punishment, Sedna takes all animals away from the clans lands, provoking a dangerous food shortage and lack of tradable pelts.

Desperate, the clan decides to search for Sarila, a legendary land where animals are said to be plenty. Under Crooliks influence—who intends Markussi to die on the quest—three young Inuit are selected: Markussi, along with his two friends, Poutulik (son of the clan chief), and Apik, who were promised to each other when they were children. Saya, who now lives on her own as a healer, offers to take care of Markussis young sister and her dog Kajuk in his absence.

The three friends leave the clan with sled dogs and Apiks pet lemming Kimi. On their quest, they face several dangers, repeatedly brought on by Croolik through magic; this includes possessing Poutulik by means of twin amulets (called "medallions"), so that Poutulik attacks Markussi. The three young travellers can overcome all dangers, repeatedly saved by Markussis powers. In the meantime, Croolik frames his former wife Saya for stealing food from the clan and has her banned from the clan.

Eventually the three friends reach Sarila, a warm place full of life, where they can quickly hunt the much-needed food for the clan. Besides, Apik realizes she loves Markussi, who had already shown affection for her before. Markussi is told by Sedna that he has passed all tests, but needs to do one "small thing" for her upon return to the clan, for the animals to be released; when asking what she requires, she only responds that "a true shaman knows what to do" and to "do what you must." Briefly afterwards, Poutulik is again possessed by Croolik and tries to kill Markussi, but the lemming Kimi realizes the role of Poutuliks amulet and rips it off, freeing Poutulik.

Upon return to the clan, Apik tells that she wants to marry Markussi, and her parents consent. Poutulik had already agreed earlier. He now wishes to travel and learn more about others ways before becoming the clans chief one day.

Croolik is enraged that Markussi is still alive and challenges him. Markussi accepts the magical fight, understanding that this is Sednas last wish and thereby proving himself a true shaman. Croolik is defeated and, dying, confesses that he framed Saya and asks for her forgiveness.

On behalf of the clan, Markussi formally asks Sedna for forgiveness, and she lets the animals return. Crooliks crow offers his services to Markussi. When he declines, the crow flies away; in the last shot it is seen carrying one of the twin amulets, which lights up in the same way as it did when Poutulik was possessed.

==English voice cast==
* Dustin Milligan as Markussi, an orphan with Shamanic powers
* Rachelle Lefevre as Apik, promised to Poutulik when she was a small girl
* Tim Rozon as Poutulik, the son of chief Itak; promised to Apik when he was a small boy
* Christopher Plummer as shaman Croolik
* Geneviève Bujold as healer and Crooliks former wife Saya
* Tyrone Benskin as chief Itak and Dawn Ford as his wife Tayara, who is too sick to do anything but moan
* Sonja Ball as Kimi, Apiks lemming
* James Kidnie as Kwatak, Crooliks crow
* Angela Galuppo as Mipoulok, Markussis sister
* Elias Toufexis as Kauji and Holly OBrien as Jiniak, Apiks parents
* Robert Higden as the tall hunter Arlok
* Natar Ungalaaq as Markussis "guardian spirit" Ukpik (the Inuinnaqtun word for snowy owl )
* Elisapie Isaac as the goddess Sedna
* Harry Standjofski as Uliak, another hunter of the clan
* Kajuk, Mipouliks dog

==Production==
The $8.5 million production was promoted as Canadas first 3D animated feature film. 

==International Distribution==
The international distribution rights are being licensed by Cinema Management Group. 

== Reception ==
On the website Rotten Tomatoes, the film counted two favourable and one unfavourable reviews, too few to compute an average rating.  Similarly, the site Metacritic includes a single favourable review (score of 70). 
 Bujold and Christopher Plummer|Plummer", but criticized that the " arratively muddled and visually undistinguished" "film hyperactively lurches from one frenzied action sequence to the next, insuring that young viewers attention spans wont be too sorely tested. But the one-note characterizations and predictable animation tropes quickly prove wearisome, and despite the exoticism of its setting The Legend of Sarila ultimately fails to enchant." 

==Awards==
The Academy of Motion Picture Arts and Sciences set the film on its "list of eligible films" for the Academy Award for Best Animated Feature  (not to be confused with an award nomination).
 Canadian Screen Award nomination for "Achievement in Music - Original Song" for writer and performer Elisapie Isaac and composer Olivier Auriol at the 2nd Canadian Screen Awards in 2014. 

== Trademark infringement lawsuit == redesigned the artwork, packaging, logo, and other promotional materials for its newly (and intentionally misleadingly) retitled film to mimic those used by   for Frozen and related merchandise."    While film titles cannot be trademarked by law, Disney cited a number of alleged similarities between the new Phase 4s Frozen Land logo and Disneys original one.     By late January 2014, the two companies had settled the case; the settlement stated that the distribution and promotion of The Legend of Sarila and related merchandise must use its original title and Phase 4 must not use trademarks, logos or other designs confusingly similar to Disneys animated release.      Phase 4 was also required to pay Disney $100,000 before 27 January 2014, and make "all practicable efforts" to remove copies of Frozen Land from stores and online distributors before 3 March 2014.  

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 