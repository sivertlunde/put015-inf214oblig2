Dancing in the Dark (1949 film)
{{Infobox film 
| name           = Dancing In the Dark
| image          = Dancing in the Dark FilmPoster.jpeg
| image_size     = 225px
| caption        = Theatrical Film Poster
| director       = Irving Reis George Jessel
| writer         = Howard Dietz Jay Dratler
| narrator       =  Mark Stevens Betsy Drake Adolphe Menjou
| music          =  Harry Jackson
| editing        = Louis R. Loeffler
| distributor    = 20th Century Fox
| released       =  
| runtime        = 92 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}
 musical comedy Mark Stevens. Betsy Drakes singing voice was dubbed by Bonnie Lou Williams.

==Plot==

This musical comedy stars William Powell as Emery Slade, an unlikeable actor who was once a major film star, but who has not worked in ten years. Slade tries to convince studio chief Melville Crossman (Adolphe Menjou) to give the female lead in the film version of a Broadway musical to an unknown, rather than the actress he was sent to New York to sign. 

==Cast==
* William Powell as Emery Slade Mark Stevens as Bill Davis
* Betsy Drake as Julie Clarke
* Adolphe Menjou as Melville Crossman
* Randy Stuart as Rosalie Brooks
* Lloyd Corrigan as John Barker
* Hope Emerson as Mrs. Schlaghammer
* Walter Catlett as Joe Brooks
* Don Beddoe as Barney Bassett
* Jean Hersholt as Jean Hersholt

==References==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 


 
 