Battling Jane
{{infobox film
| name = Battling Jane
| image size =
| image = Battling Jane FilmPoster.jpeg
| caption =
| director = Elmer Clifton
| producer =
| writer = Arnold Bernot George Nichols
| music = Karl Brown
| editing =
| distributor = Paramount Pictures
| released =  
| runtime = 5 reel#Motion picture terminology|reels, 4458 feet
| country = United States
| language = Silent film (English intertitles)
}} survival status is unknown.

==Plot==
Jane, a waitress at a small town Maine hotel, assumes guardianship of a baby whose mother has died. The babys rakish father, Dr. Sheldon, conspires to steal prize money won by Jane after she enters the child in a baby show. Jane manages to hold the doctor and his accomplice at bay until help arrives, then uses the prize money to help the war effort by purchasing Liberty Bonds and donating the rest to the Red Cross. 

==Release==
The film was released in the United States in September, 1918, and in Canada shortly before the new year. In some venues it was accompanied by the comedy film The Goat with Fred Stone.  It would be one of the most financially successful films made by Gish for Paramount. 

Battling Jane received good reviews for its performances and its scenario: "(T)hough by turns pure comedy and pure melodrama, (it) is logically and consistently told."  A reviewer for the Toronto World thought Dorothy Gish "a marvel of cleverness... as charming as in any of her previous parts. Director Elmer Clifton leaves nothing to be desired, either in point of acting or investiture. The support is unusually capable."   The Evening Post of Wellington, New Zealand, found the lead role "quaint"; nonetheless it praised both the "cleverly depicted" story and the leading lady: "There are hundreds of Janes in the world, no doubt... Ms. Gish portrays the character realistically." 

==See also==
*The House That Shadows Built (1931 promotional film by Paramount)

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 