Paradise Murdered
{{Infobox film name           = Paradise Murdered image          = Paradise Murdered film poster.jpg caption        = director       = Kim Han-min producer       = Choi Du-young   Shin Hye-yeon writer         = Kim Han-min starring       = Park Hae-il Park Sol-mi Seong Ji-roo Lee Dae-yeon Park Won-sang Choi Joo-bong music          = Bang Jun-seok cinematography = Kim Yong-heung editing        = Shin Min-kyung distributor    = MK Pictures released       =   runtime        = 112 minutes country        = South Korea language       = Korean film name      = {{Film name hangul         = 극락도 살인사건  hanja          =     rr             = Geukrakdo Salinsageon  mr             = Kŭngnakdo Sarinsakŏn}} budget         = gross          =   
}} 2007 Cinema South Korean film starring Park Hae-il and Park Sol-mi, and is the directorial debut of filmmaker Kim Han-min.

==Synopsis==
Off the southern coast of the Korean Peninsula, an island of 17 inhabitants exists. The so-called Paradise Island holds up to its name with its breath-taking mountains and sea coupled with good-natured people. No worries or stress holds for anyone who comes to visit this beautiful oasis. But this peace doesn’t last long as every single one of the inhabitants disappear one day without a single trace. Chaos initially breaks out when a blood-drenched corpse is found and everyone becomes a suspect. The furious sea allows them no boat ride to the mainland and their only existing radio communication device has been smashed. Trapped together on the island, everyone is suspicious of each other and even the unseen could be a possible suspect. As hideous secrets get revealed day by day, an island of paradise slowly turns into an island of death.

==References==
 

==External links==
*  
*  
*  
* Joshua Pettigrew,  . Kung Fu Cult Cinema. 20 March 2007. Retrieved 29 March 2007.
* Kim Tae-jong,  . The Korea Times. 13 March 2007. Retrieved 29 March 2007.

 
 
 
 
 


 