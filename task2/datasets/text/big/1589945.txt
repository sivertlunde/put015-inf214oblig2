The Care Bears Adventure in Wonderland
{{Infobox film
| name         = The Care Bears  Adventure in Wonderland
| image        = CareBearsMovieIII_AIW.jpg
| caption      = Original theatrical poster
| director     = Raymond Jafelice Michael Hirsh Patrick Loubert Clive A. Smith
| writer       = Peter Sauder
| screenplay   = Susan Snooks John de Klein Keith Knight John Stocker Colin Fox
| music        = Patricia Cullen
| editing      = Evan Landis Nelvana Limited
| distributor  = Cineplex Odeon Films
| released     =  
| country      = Canada
| runtime      = 78 minutes
| language     = English
| budget       = US$5 million    (adjusted by inflation$: )    the   company earned an international reputation in 1984, after American director   and  . A year later, Ohios American Greetings Corp. and Kenner Parker Toys Inc. commissioned Nelvana to produce the animated Care Bears Movie. Earning $34 million in 1985, it became at the time the worlds most profitable non-Disney animated movie. Buoyed by that success, Nelvana made two sequels. But the last of the trilogy, the 1987 Care Bears Adventure in Wonderland, which Nelvana produced for just under $5 million, only broke even. Conceded Hirsh: It was just one   too many.}}   |group=nb}}
| gross        = North America: US$2,608,000    (adjusted by inflation$: )     Worldwide: US$6,000,000    (adjusted by inflation$: )     |
}} Michael Hirsh, Keith Knight, Bob Dermer, Jim Henshaw, Tracey Moore and Elizabeth Hanna.
 rapping Cheshire Cat and the Jabberwocky.
 Region 1 premiere has yet to occur.

==Plot==

In Care-a-lot, the Care Bears are visited by the White Rabbit, the uncle of Swift Heart Rabbit. The White Rabbit gives the Care Bears the task of finding the missing Princess of Heart, who is to be crowned queen in Wonderland (fictional country)|Wonderland, otherwise the villainous Wizard of Wonderland will gain the throne. Tenderheart, Grumpy, Good Luck, Brave Heart, Lotsa Heart, Swift Heart and the White Rabbit search all over the world for the Princess, but to no avail. Grumpy is pointed to a girl who resembles the Princess, Alice (Alices Adventures in Wonderland)|Alice. The Care Bears decide that Alice could act as the Princess until the real one is found. The group is separated by the power of the Wizard, forcing Grumpy, Swift Heart and the White Rabbit to use a rabbit hole to reach Wonderland.
 Dim and rap artist. Queen of Hearts throne room, where the Queen accepts Alice as her daughter whilst knowing she is not. Brave Hearts group locate the Mad Hatter who takes them to the lair of the Jabberwocky, where the Princess is. Grumpy rescues the princess, but the Jabberwocky gets a thorn in his foot which is removed by the Care Bears. In gratitude, the Jabberwocky (or "Stan" as he prefers to be called) decides to help them back to Heart Palace.

As the Princess coronation day arrives, the Wizard decides to expose Alices identity to the court via the Princess Test, to prove that she is not the princess. Alice climbs a mountain to retrieve some water from a spring, aided secretly by Tenderheart and Lotsa Heart; however, Alice gives the water to an injured unicorn. Angered by this, the Wizard demands that Alice make the flowers in the palace garden bloom magically. The princess secretly steps in and makes the flowers bloom. The Wizard, who had not been anticipating the Princess return, suddenly exclaims he had her kidnapped, exposing his crime. The Care Bears, Alice, and the Wonderland characters confront the Wizard but the appearance of the Jabberwocky drives the villain insane, and he is arrested. The princess is crowned the new queen, and she helps Alice and the Care Bears return home. At Care-A-Lot, Grumpy raps with the Cheshire Cat who joins the Care Bears for dinner as the film ends.

==Cast==
The cast of the film: 
 
{| class="wikitable" style="font-size:90%; text-align:center; margin: 5px;"
|-  Name
! Character
|-
| Bob Dermer || Grumpy Bear
|-
|  Eva Almos || Swift Heart Rabbit
|-
| Dan Hennessey || Brave Heart Lion / Dum
|-
| Jim Henshaw || Tender Heart Bear
|-
| Marla Lukofsky || Good Luck Bear
|-
| Luba Goy || Lots-a-Heart Elephant
|- Keith Knight || White Rabbit
|-
| Tracey Moore || Alice
|- Colin Fox || Wizard
|- John Stocker || Dim / Cheshire Cat
|-
| Don McManus || Caterpillar
|-
| Elizabeth Hanna || Queen of Wonderland
|-
| Alan Fawcett || Flamingo
|-
| Keith Hampshire || Mad Hatter / Jabberwocky ("Stan")
|-
| Alyson Court || Princess of Wonderland
|-
|}

==Production== a Care ABC network Global in Michael Hirsh, Patrick Loubert and Clive A. Smith—were once again producers.   Jack Chojnacki, the co-president of American Greetings licensing division Those Characters from Cleveland,  served as a creative consultant on this instalment;  for the previous ones, he was an executive producer.      In December 1986, Toronto actor Colin Fox recorded his lines for the film at the Nelvana studios. 

==Release==

===North America=== Cineplex Odeon Sign o Family Channel.   Although the first two Care Bears films have received DVD releases,     a Region 1 DVD premiere has not yet occurred for the third one,  although it did receive a DVD release in Australia.

Adventure in Wonderland only managed to break even with worldwide earnings of US$6,000,000,  |0}} in   dollars. |group=nb}} which led Nelvana co-founder Michael Hirsh to say, "It was just one sequel too many."   The Bears would return for a television special,  .     Their next theatrically released film,  , was screened in the U.S. in August 2007. 

===Overseas===
As with the original Care Bears Movie,      Adventure in Wonderland made an appearance at 1987s Cannes Film Festival.     The film was released by Germanys Warner Bros.    on November 27, 1987    as Bärchis Abenteuer im Wunderland.   It sold 104,478 tickets and ranked 96th place among the years releases in that market (excluding re-issues),  and grossed approximately   (the equivalent of Deutsche Mark|DM788,750, or US$570,000).    .   |name=de-ticket|group=nb}}  The film was released on video by Virgin Group|VCL/Virgin on March 22, 1988. 

Adventure in Wonderland was released in the Netherlands on December 17, 1987, as Troetelbeertjes in Wonderland.   Starting in March 1988, it was screened in matinees across the United Kingdom {{cite news|last=Robinson|first=David|title= Virgin Films,  as part of an agreement with the local branch of 20th Century Fox.   As late as 1992, distribution rights in France were held by NDP, who released it as Les Bisounours au pays des merveilles.   In Spain, the film is entitled Los Osos Amorosos en el País de las Maravillas (among Castillan speakers)  and Els Óssos Amorosos al país de les meravelles (among Catalan speakers).     Elsewhere in Europe, it is known as As Aventuras dos Ursinhos Carinhosos (in Portugal),    Krambjörnarna i Underlandet (in Sweden),  and Troskliwe Misie w Krainie Czarów (in Poland). 

In Australia, the film was released theatrically in December 1988,  and on video in July 1989 by Virgin;    it was airing on that countrys Nine Network by February 1996.   South African video rights were held by Ster Kinekor in 1989.   On February 23 that same year, it was released in Mexico as Aventuras de Alicia en el país de las maravillas y los ositos cariñositos.   As of 2010, the film has been sold on DVD in several European countries;    an Australian edition treats it as an episode rather than a feature-length film. 

==Reception==
{{quote box|width=35%|quote=
"Its always been hard to care for these bears. These adventures do not change them for the better."|source=International Film Guide, 1989 
}} Sunday Mail wrote of the film positively,  while another Australian critic, Rob Lowing of Sydneys The Sun-Herald, gave it two and a half stars out of four and noted that there was "Nothing original here, although that also means nothing to shock".  Similarly, the Christian Science Monitor wrote that "The animated action holds few surprises for grown-ups, but the cute characters and fetching designs should enthrall young children." 
 New York Times review, Caryn James said, "  movie is paced so it wont strain the attention span of a 6-month-old, but there is nothing to spark a childs imagination."  As noted Hal Hinson of The Washington Post, "Watching   is like being pelted mercilessly for 75 minutes with Lucky Charms. Its nonfatal (unless you have a sugar problem, in which case youre likely to lapse into a coma), but its not exactly my idea of fun either."   Rick Groen of Torontos The Globe and Mail expressed disappointment over the way the villain was handled. 

Comparing this installment to its predecessors, Newsday  s Joseph Gemlis declared that Wonderland "aspires to be more than a merchandising gimmick. It has a story to tell". However, he criticised the plot and treatment of the fictional lands denizens.  Film critic   to Lewis Carroll fans."  In a 1988 issue, the Video Librarian labelled it a "dud". 

Adventure in Wonderland was nominated for Best Music Score (by Patricia Cullen) and Best Original Song ("Rise and Shine" by Maribeth Soloman) at the 1987 Genie Awards in Canada.  At the Young Artist Awards, it was also nominated for Best Motion Picture in the Animated category. 

==Allusions== Androcles and 1937 and 1952 respectively.   According to Johanna Steinmetz of the Chicago Tribune, Wonderland borrowed some elements from The Wizard of Oz (1939 film)|The Wizard of Oz. 

==Music==
{{quote box|width=35%|quote=
"First we hear the song Everything Is Wonderful in Wonderland. That contrasts with the villains song, when he tells how hes going to bring order to Wonderland by taking the color out and turning everything right side up. I dont think he is really scary. Just mean."
|source=Michael Hirsh 
}}
The songs in The Care Bears Adventure in Wonderland were composed and performed by John Sebastian,  who performed "Nobody Cares like a Bear" in the first Care Bears Movie.   For this instalment, Sebastian sang "Have You Seen This Girl?" and the "Wonderland" song, while singer and songwriter Natalie Cole performed the film’s opening song, "Rise and Shine".   The score was written by Patricia Cullen  (who had previously scored the first two films),   and orchestrated and conducted by Milton Barnes.   Todd Sussman of The Miami News said, "A musical score of seven forgettable songs is ornamental and does little to advance the plot. ... Even the opening number   is unmemorable. Its hookless."   

{| class="wikitable" border="1"
|- Song
!align="left"|Writer
!align="left"|Performer(s)
!align="left"|Producer(s)
|-
| "Rise and Shine" || Maribeth Soloman   Arr.   Micky Erbe || Natalie Cole || David Greene
|-
| "Have You Seen This Girl?" || John Sebastian || John Sebastian || John Sebastian David Greene
|-
| "Wonderland" || John Sebastian || John Sebastian || John Sebastian David Greene
|-
| "Mad About Hats" || John Sebastian || Keith Hampshire || John Sebastian David Greene
|-
| "The King of Wonderland" || John Sebastian || Colin Fox || John Sebastian David Greene
|-
|}

==Notes==
 

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 