Kantheredu Nodu
{{Infobox film 
| name           = Kantheredu Nodu
| image          =  
| caption        = 
| director       = T V Singh Takur
| producer       = A K Velan
| writer         = A K Velan
| screenplay     = G V Iyer Rajkumar Leelavathi Leelavathi Balakrishna Balakrishna Rajasree
| music          = G. K. Venkatesh
| cinematography = B Dorairaj
| editing        = Venkataram V N Raghupathi
| studio         = Aranachelam Studios
| distributor    = 
| released       =  
| country        = India Kannada
}}
 1961 Cinema Indian Kannada Kannada film, Balakrishna and Rajasree in lead roles. The film had musical score by G. K. Venkatesh.  

==Cast==
  Rajkumar
*Leelavathi Leelavathi
*Balakrishna Balakrishna
*Rajasree Narasimharaju
*Ramadevi
*G V Iyer
*B. Jayashree
*Bharadwaj
*Papamma
*Eshwarappa
*Vijayakumari
*Ganapathi Bhat
*Vasanthi
*H Krishna Shastry
*Kuppuraj
*Veerabhadrappa
*Revathi
*Shakunthala
 

==Soundtrack==
The music was composed by GK. Venkatesh. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Bangaradodave || PB. Srinivas || GV. Iyer || 01.50
|-
| 2 || Bangaradodave Beke || PB. Srinivas || GV. Iyer || 02.42
|-
| 3 || Hagalu Irulu || PB. Srinivas || GV. Iyer || 01.29
|- Janaki || GV. Iyer || 03.24
|-
| 5 || Kallu Sakkare || PB. Srinivas || Purandaradasa || 02.54
|-
| 6 || Kannadada Makkalella || Venkatesh || GV. Iyer || 03.17
|-
| 7 || Ninagidu Nyayave || PB. Srinivas || GV. Iyer || 01.26
|-
| 8 || Sharanu Kaveri || PB. Srinivas || GV. Iyer || 01.27
|-
| 9 || Sigadhanna || PB. Srinivas || GV. Iyer || 01.39
|-
| 10 || Siriye Karunada || PB. Srinivas || GV. Iyer || 01.25
|-
| 11 || Yedavidare Naa || S. Janaki|Janaki, Rajeshwari || GV. Iyer || 04.50
|}

==References==
 

==External links==
*  
*  
*  

 
 
 
 


 