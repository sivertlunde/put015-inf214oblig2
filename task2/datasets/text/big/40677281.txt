Consuming Spirits
{{Infobox film
| name = Consuming Spirits
| image = Consuming_Spirits_Poster.jpg
| caption = 
| director = Chris Sullivan
| producer = Chris Sullivan
| writer = Chris Sullivan
| starring = 
| music = 
| cinematography = Chris Sullivan
| editing = Chris Sullivan
| studio = 
| released =  
| runtime = 136 minutes
| country = United States
| language = English
| budget = 
| gross = 
}} animated drama film directed by Chris Sullivan and his directorial debut. Partially autobiographical, the movie released on December 12, 2012 in New York City and employs a diverse mixture of different animation styles to tell the stories of three different characters and the anguish they face in their everyday lives.

==Synopsis==
The movie follows the lives of three residents of the fictional town of Magguson, all of whom work at the towns newspaper The Daily Suggester. While at first the interactions between the three people appear to show a superficial working relationship between them, as the movie progresses the viewer learns that there is a deeper relationship between them all and that all of them have hidden secrets.

==Cast== Nancy Andrews as Gentian Violet (voice) Chris Sullivan as Victor Blue (voice)
*Judith Rafael as Mother Beatrice Elastica (voice)
*Mary Lou Zelazny as Ida Blue (voice) Chris Harris as Peabody Shampling (voice)
*Robert Levy as Earl Gray (voice)

==Reception== Fantoche animated film festival in Baden, Switzerland.  A reviewer for NPR recommended the film and called it "emotionally raw, thoroughly original". 

===Awards===
*Chicago Award at the  Chicago International Film Festival (2012, won) 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 