Alvin and the Chipmunks: The Road Chip
{{Infobox film
| name           = Alvin and the Chipmunks: The Road Chip
| image          = 
| alt            = 
| caption        =
| director       = Walt Becker
| producer       = Ross Bagdasarian, Jr. Janice Karman
| writer         = Randi Mayem Singer
| based on       =   Jason Lee Justin Long Bella Thorne Matthew Gray Gubler Jesse McCartney Kimberly Williams-Paisley Tony Hale
| music          = 
| cinematography = 
| editing        =  Bagdasarian Company Sunswept Entertainment TSG Entertainment
| distributor    = 20th Century Fox
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Alvin and Jason Lee, Justin Long, Matthew Gray Gubler, Bella Thorne, Jesse McCartney, Kimberly Williams-Paisley and Tony Hale. The film is scheduled to be released on December 23, 2015, by 20th Century Fox.

== Cast == Jason Lee as David Seville    
* Justin Long as Alvin  
* Matthew Gray Gubler as Simon 
* Bella Thorne as Ashley   
* Jesse McCartney as Theodore  
* Kimberly Williams-Paisley  
* Tony Hale   

==Production==
In June 2013,   joined the cast.  On March 10, 2015, Kimberly Williams-Paisley joined the cast.  On March 23, 2015, it was confirmed that Bella Thorne had joined the cast.  Principal photography began on March 16, 2015. 

== References ==
 

== External links ==
*  

 
 

 
 
 
 
 
 
 
 
 
 
 

 