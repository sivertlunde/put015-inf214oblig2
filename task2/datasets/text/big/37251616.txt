Pondatti Thevai
{{Infobox film
| name           = Pondatti Thevai
| image          = Pondatti Thevai DVD cover.jpg
| image_size     =
| caption        = DVD cover Parthiban
| producer       = A. Sundaram Parthiban
| starring       =  
| music          = Ilaiyaraaja
| cinematography = K. Rajpreeth
| editing        = M. N. Raja
| distributor    = Vivek Chithra International
| studio         = Vivek Chithra International
| released       =  
| runtime        = 150 minutes
| country        = India
| language       = Tamil
}}
 1990 Tamil Tamil drama Ashwini in lead roles. The film, produced by  A. Sundaram, had musical score by Ilaiyaraaja and was released on 14 April 1990.  

==Plot==

Kannan (Parthiban), a bus conductor, falls in love with Shanthi (Ashwini), a Brahmin girl. The rest of the story is he will get married with her or not.

==Cast==

*Parthiban as Kannan Ashwini as Shanthi
*Sindhu  MRK as Naidu
*Ra. Sankaran as Sankar, Shanthis father
*Kamala Kamesh as Kamala, Shanthis mother
*K. Janaki as Kannans mother
*Kumari Muthu as Kumari Muthu
*Idichapuli Selvaraj
*Thalapathi Dinesh

==Soundtrack==

{{Infobox Album |  
| Name        = Pondatti Thevai
| Type        = soundtrack
| Artist      = Ilaiyaraaja
| Cover       = 
| Released    = 1990
| Recorded    = 1990 Feature film soundtrack |
| Length      = 28:30
| Label       = 
| Producer    = Ilaiyaraaja
| Reviews     = 
}}

The film score and the soundtrack were composed by film composer Ilaiyaraaja. The soundtrack, released in 1990, features 10 tracks with lyrics written by Vaali (poet)|Vaali, Pulamaipithan, Gangai Amaran, Piraisoodan and Ilaiyaraaja.   

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Duration
|-  1 || Aararo Paada || Arunmozhi, K. S. Chithra || 5:02
|- 2 || Amma Naan || Malaysia Vasudevan || 5:30
|- 3 || Enathu Raagam || Arunmozhi, S. Janaki || 4:34
|- 4 || Ladies Special || Chorus || 0:20
|- 5 || Naan Nenaicha || Chorus || 0:20
|- 6 || Oru Raaja || S. Janaki || 4:42
|- 7 || Thottavudan || K. S. Chithra || 1:00
|- 8 || Varuga Varuga || Chorus || 0:21
|- 9 || Yaaradi  (Male)  || Ilaiyaraaja || 4:55
|- 10 || Yaaradi  (Female)  || Chorus || 1:46
|}

==References==
 

 

 
 
 
 
 