Master Minds
{{Infobox Film
| name           = Master Minds
| image          = Master Minds.jpg
| image_size     = 
| caption        = Theatrical poster to Master Minds
| director       = Jean Yarbrough
| producer       = Jan Grippo
| writer         = Charles Marion
| narrator       = 
| starring       = Leo Gorcey Huntz Hall Gabriel Dell David Gorcey William Benedict
| music          = Edward J. Kay
| cinematography = Marcel LePicard William Austin
| distributor    = Monogram Pictures
| released       =  
| runtime        = 64 minutes
| country        = United States
| language       = English
| budget         = 
}}
Master Minds is a comedy film starring The Bowery Boys. The film was released on November 29, 1949 by Monogram Pictures and is the sixteenth film in the series.

==Plot==
Sach, after eating too much candy develops a toothache which allows him to predict the future.  Slip and Gabe come up with an idea to make money off of this and put him in a side show carnival.  A mad scientist sees Sachs photo in the newspaper and reads about his ability and decides to visit the carnival.  After seeing Sach in action he decides to kidnap him so he can transfer his brain into the brain of Atlas, a humanoid creature.

The boys attempt to rescue Sach, but are captured themselves.  Meanwhile Sach and Atlas have had their brains swapped temporarily and Louie has arrived in the hopes of rescuing all of them.  He dons a knights armor and temporarily outwits the scientists, but is eventually captured as well.  However, the police, who Louie tried to alert earlier, arrive and arrest the scientists.  Slip then tries to put Sach back on display at the carnival, but Sach says he no longer has a toothache...because he swallowed it!

==Production==
Bennie Bartlett temporarily left the series after this film. He would be replaced by Buddy Gorman for the next seven films.

==Home media== Warner Archives released the film on made to order DVD in the United States as part of "The Bowery Boys, Volume One" on November 23, 2012.

==Cast==
===The Bowery Boys===
*Leo Gorcey as Terrance Aloysius Slip Mahoney
*Huntz Hall as Horace Debussy Sach Jones
*William Benedict as Whitey
*David Gorcey as Chuck
*Bennie Bartlett as Butch

===Remaining cast===
*Gabriel Dell as Gabe Moreno
*Bernard Gorcey as Louie Dumbrowski
*Glenn Strange as Atlas
*Alan Napier as Dr. Druzik
*William Yetter as Otto Jane Adams as Nancy Marlowe

==References==
 

==External links==
* 

 
{{succession box
| title=The Bowery Boys movies
| years=1946-1958 Angels in Disguise 1949
| after=Blonde Dynamite 1950}}
 

 
 

 
 
 
 
 
 
 