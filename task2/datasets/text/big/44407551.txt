Divorce in the Family
{{Infobox film
| name           = Divorce in the Family
| image          = 
| alt            = 
| caption        = 
| director       = Charles Reisner
| producer       = Harry Rapf
| screenplay     = Delmer Daves
| story          = Maurice Rapf Delmer Daves Lois Wilson Jean Parker Maurice Murphy
| music          = 
| cinematography = Oliver T. Marsh  William S. Gray
| studio         = Metro-Goldwyn-Mayer
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 81 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Lois Wilson, Jean Parker and Maurice Murphy. The film was released on August 27, 1932, by Metro-Goldwyn-Mayer.  

==Plot==
 

== Cast ==
*Jackie Cooper as Terry Parker
*Conrad Nagel as Dr. Shumaker
*Lewis Stone as John Parker Lois Wilson as Mrs. Shumaker
*Jean Parker as Lucile
*Maurice Murphy as Al Parker
*Lawrence Grant as Kenny
*Richard Wallace as Snoop
*David Newell as Interne
*Oscar Rudolph as Spike
*Louise Beavers as Rosetta
*Edith Fellows as a little girl with a kite (uncredited)

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 