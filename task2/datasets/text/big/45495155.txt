Uncharted Seas
{{Infobox film
| title          = Uncharted Seas
| image          = Uncharted Seas (1921) - Ad 2.jpg
| imagesize      =
| caption        =
| director       = Wesley Ruggles
| producer       =
| writer         = John Fleming Wilson
| starring       =
| cinematography = John F. Seitz
| editing        =
| distributor    = Metro Pictures
| released       =  
| runtime        =
| country        = United States Silent English intertitles
}}
Uncharted Seas is American 1921 silent era romance drama film directed by Wesley Ruggles and starring Alice Lake, Carl Gerard, Rudolph Valentino.

==Cast==

* Alice Lake	 ... Lucretia Eastman
* Carl Gerard	 ... Tom Eastman
* Rudolph Valentino	 ...	Frank Underwood
* Robert Alden	 ...	Fred Turner
* Charles Hill Mailes	 ...	Old Jim Eastman (as Charles Mailes)
* Rhea Haines	 ...	Ruby Lawton

==References==
 

==External links==
* 
 
 
 
 
 
 
 

 