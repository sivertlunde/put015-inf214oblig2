Surrogate Valentine
{{Infobox film
| name           = Surrogate Valentine
| image          = 
| alt            = 
| caption        = 
| director       = Dave Boyle 
| producer       = Duane Andersen Dave Boyle   Gary Chou   Alex Cannon Paul Cannon Michael Lerman
| screenplay     = 
| writer         = Dave Boyle Joel Clark Goh Nakamura
| starring       = Goh Nakamura Chadd Stoops Lynn Chen Parry Shen Calpernia Addams Joy Osmanski Eric M. Levy Mary Cavett
| music          = Goh Nakamura
| cinematography = Bill Otto
| editing        = Duane Andersen Dave Boyle Michael Lerman
| studio         = 
| distributor    = Tiger Industry Films Brainwave
| released       =  
| runtime        = 1 hour 14 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}

Surrogate Valentine  is an independent comedy film directed and produced by Dave Boyle about a musician named Goh Nakamura.

==Synopsis==
San Francisco indie musician Goh Nakamura lives a life on the road, navigating friendships and relationships.

==Cast==
* Goh Nakamura as Himself
* Chadd Stoops as Danny Turner
* Lynn Chen as Rachel
* Parry Shen as Bradley
* Mary Cavett as Valerie
* Joy Osmanski as Amy
* Calpernia Addams as Tammi
* Eric M. Levy as Arthur
* Dan Damage as Mark
* Di Quon as Emily

==Reception==
Critical response to the film was generally positive. Rotten Tomatoes reports a 60% approval rating based on 5 reviews.    

John DeFore of The Hollywood Reporter called the film "a slight, but amiable buddy comedy" as well as saying that it "offers a certain mild slacker charm".  Michelle Orange of Village Voice also gave a positive review of the film saying that it "cultivates a sweet, shucksy tone that wears thin in some of the early scenes, but ultimately deepens into genuine heart". 

==References==
 

 
 
 
 