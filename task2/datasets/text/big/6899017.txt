Funny Farm (film)
{{Infobox film
| name           = Funny Farm
| image          = funny_farm_(film_poster).jpg
| image_size     =
| caption        = Promotional poster for Funny Farm
| director       = George Roy Hill
| producer       = Robert L. Crawford Funny Farm) Jeffrey Boam (screenplay)
| starring       = Chevy Chase Madolyn Smith Joseph Maher Jack Gilpin Brad Sullivan MacIntyre Dixon
| music          = Elmer Bernstein
| cinematography = Miroslav Ondrícek
| editing        = Alan Heim
| distributor    = Warner Bros.
| released       =  
| runtime        = 101 minutes
| country        = United States English
| budget         = $19 million
| gross          = $25,537,221
}} adapted from same name by Jay Cronley. The movie was filmed on location in Vermont, mostly in Townshend, Vermont. It was the final film directed by George Roy Hill.

==Plot==
Andy Farmer (Chase) is a New York City sports writer who moves with his wife, Elizabeth (Smith) to the seemingly charming town of Redbud, Vermont, so he can write a novel. They do not get along well with the residents, and other quirks arise such as being given exorbitant funeral bills for a long-dead man buried on their land years before they acquired the house. Marital troubles soon arise from the quirkiness of Redbud as well as the fact that Elizabeth was critical of Andys manuscript, while secretly getting her ideas for childrens books published. They soon decide to divorce and sell their home, enticing the towns residents with a $15,000 donation to Redbud, as well as a $50 cash bonus to whoever made a friendly impression toward their prospective home buyers. To that end, the citizens remake Redbud into a perfect Norman Rockwell-style town. Their charade dazzles a pair of prospective buyers, who make the Farmers an offer on the house; however, Andy declines to sell, realizing that he genuinely enjoys small-town living. He and Elizabeth decide to stay together in Redbud, much to the chagrin of the locals, who are now angry that they lost their promised money. Though the mayor does not hold the Farmers liable for the $15,000, as the sale of their house did not occur, Andy decides to pay everyone in Redbud their $50, which helps improve his standing among the townspeople. The film ends with Andy taking a job as a sports writer for the Redbud newspaper, and Elizabeth, now pregnant with their first child, has written multiple childrens stories.

==Cast==
*Chevy Chase as Andy Farmer
*Madolyn Smith as Elizabeth Farmer
*Kevin OMorrison as Sheriff Ledbetter Mike Starr and Glenn Plummer as Crocker and Mickey, the movers Kevin Conway as Crumb Petree, the mailman (uncredited)
*Joseph Maher as Michael Sinclair, the publisher Nicholas Wyman as Lon and Dirk, the Criterion brothers William Newman as Gus Lotterhand
*Sarah Michelle Gellar as Elizabeths Student (scenes deleted)

==Production==
The film was mostly shot in Southeastern Vermont, including the towns of Townshend, Vermont|Townshend, Windsor, Vermont|Windsor, Grafton, Vermont|Grafton, and Hartland, Vermont using locals as extras. The Townshend Common today has an unusual souvenir from the production of Funny Farm: to make the trees on the Common look as if it was mid autumn, the film crew dyed the leaves, which killed all of the trees, except for one in the middle of the Common. Today, the ring of newly planted trees around the edge of the Common are significantly shorter than the much larger one that survived the filming of Funny Farm. 

==Reception==
The movie received mixed to positive reviews at the time of its release.    However, film critics Roger Ebert and Gene Siskel were strong champions of the film, praising it on The Oprah Winfrey Show and The Tonight Show Starring Johnny Carson. Ebert described the film as a "small miracle.",  while Siskel called it "the best film Chase has made" and compared it to the films of Preston Sturges.  Funny Farm holds a 67% rating on Rotten Tomatoes based on 18 reviews.

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 