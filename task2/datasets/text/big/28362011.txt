Betrayal (2009 film)
{{Infobox film
| name           = Betrayal
| image          = Svik.jpg
| caption        = 
| director       = Håkon Gundersen
| producer       = 
| writer         = Håkon Gundersen
| starring       = Fridtjov Såheim   Lene Nystrøm   Kåre Conradi   Götz Otto
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 103 minutes
| country        = Norway
| language       = Norwegian
| budget         = 
| gross          =
}} Norwegian historic action film directed by Håkon Gundersen, starring Fridtjov Såheim, Lene Nystrøm, Götz Otto and Kåre Conradi. The film is based on a true story.

==Plot==
During the German occupation of Norway, nightclub owner Tor Lindblom (Såheim) makes a fortune by collaborating with the Germans. With the help of Schutzstaffel|SS-Sturmbandfürer Krüger (Otto), he plans to exploit the construction of a new aluminium plant for his own benefit. At the same time he is also romantically involved with Eva Karlsen (Nystrøm), a singer at the nightclub, who is a British double agent.

==Reception== dice throw of two and called the film an "awkward amateur night".    Ingunn Økland of Aftenposten gave it three points, calling it a "missed opportunity" to make what could have been "an important Norwegian war film".   

==References==
 

==External links==
*  
*   at Filmweb.no (Norwegian)
*   at the Norwegian Film Institute

 
 
 
 


 
 