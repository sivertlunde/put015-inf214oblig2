Rose Hobart (film)
Rose experimental collage film created by the artist Joseph Cornell, who cut and re-edited the Universal  film East of Borneo (1931) into one of Americas most famous surrealist short films. Cornell was fascinated by the star of East of Borneo, an actress named Rose Hobart, and named his short film after her. The piece consists of snippets from East of Borneo combined with shots from a documentary film of an eclipse.

== Creation == 16mm print of East of Borneo at a junk shop. To make the 77-minute film less tedious for repeated viewings by himself and his brother, Cornell would occasionally cut some parts, rearrange others, or add pieces of nature films, until it was condensed to its final-length of 19 minutes, mostly featuring shots of the lead actress, whom Cornell had become obsessed with. As such, it might be classified as one of the earliest fanvids, which often feature character studies from stock footage from popular films and television programs.

== Screenings ==
When Cornell screened the film, he projected it through a piece of blue glass and slowed the speed of projection to that of a silent film. Cornell removed the original soundtrack and added "Porte Alegre" and "Belem Bayonne", two songs from Nestor Amarals album Holiday in Brazil, a record that Cornell had also found at a junk shop.
 surrealist art at the Museum of Modern Art.

Salvador Dalí was in the audience, but halfway through the film, he knocked over the projector in a rage. “My idea for a film is exactly that, and I was going to propose it to someone who would pay to have it made,” he said. "I never wrote it down or told anyone, but it is as if he had stolen it." Other versions of Dalís accusation tend to the more poetic: "He stole it from my subconscious!" or even "He stole my dreams!" 

After the Dalí incident, Cornell did not show the film again until the 1960s, when, at the behest of Jonas Mekas, it was screened again for a public audience. When the first print was made from Cornells original in 1969, Cornell chose a rose tint instead of the normal blue.

== Honors ==
In 2001, Rose Hobart was selected for preservation in the United States National Film Registry by the Library of Congress as being "culturally, historically, or aesthetically significant".

==See also==
*Treasures from American Film Archives
*Blonde Cobra

==References==
 

==External links==
*  
*  
*  

==References==
*Deborah Solomon, Utopia Parkway: The Life and Work of Joseph Cornell (New York: Farrar, Straus and Giroux, 1997)
*Julien Levy, Memoir of an Art Gallery  (Boston:  MFA Publications, 2003)
* 
*Brian Frye,  , 2001

 
 
 
 
 
 
 