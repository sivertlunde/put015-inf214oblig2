American Milkshake
{{Infobox film
| name           = American Milkshake
| image          = American Milkshake.jpg
| caption        = 
| director       = David Andalman Mariko Munro
| producer       = David Andalman Mariko Munro
| writer         = David Andalman Mariko Munro
| starring       = Tyler Ross Shareeka Epps Georgia Ford Eshan Bay Leo Fitzpatrick Danny Burstein
| cinematography = Ian Bloom
| music          = Kieran Magazul
| studio = 
| distributor   = Phase 4 Films 
| released       =  
| runtime        = 82 minutes
| country        = United States
| language       = English
| budget         = $1.4 Million
| gross          = $44.687
}}
American Milkshake is a 2013 American dark comedy film written and directed by David Andalman and Mariko Munro, starring Tyler Ross, Shareeka Epps, Georgia Ford, Eshan Bay, Leo Fitzpatrick and Danny Burstein.

==Plot==
In the mid-90s, Jolie Jolson, a white high school student (the great-grandson of  . Jolie gets on the basketball team due to a large donation from his well-off father, and ends up dating one of the cheerleaders for the team, and accidentally impregnates her.

==Cast==
* Tyler Ross as Jolie
* Shareeka Epps as Henrietta
* Georgia Ford as Christine
* Leo Fitzpatrick as Mr. McCarty
* Eshan Bay as Haroon
* Danny Burstein as Coach
* Nuri Hazzard as Arius
* Anna Friedman as Jeanette

==Release== Kevin Smith Movie Club for theatrical and day and date release and   it also won the producers award at US in Progress. 

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 


 