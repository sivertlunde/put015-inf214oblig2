Grosse Fatigue
 
{{Infobox film
| name           = Grosse Fatigue
| image          = Grosse Fatigue.jpg
| caption        = Film poster
| director       = Michel Blanc
| producer       = Daniel Toscan du Plantier
| writer         = Michel Blanc Bertrand Blier
| starring       = Michel Blanc Carole Bouquet Philippe Noiret Josiane Balasko
| music          = 
| cinematography = Eduardo Serra
| editing        = Maryline Monthieux
| distributor    = 
| released       =  
| runtime        = 92 minutes
| country        = France
| language       = French
| budget         = $8.4 million
| gross = $13.2 million 
}}

Grosse Fatigue is a 1994 French comedy film directed by Michel Blanc. It was entered into the 1994 Cannes Film Festival.   

==Plot==
Michel Blanc is a great film actor. However, he was accused of sexually abusing Josiane Balasko, Charlotte Gainsbourg and Mathilda May, had behaved like a cad at Cannes and accept poor seals, such as animations in supermarkets, secretly his agent. The evidence is obvious, but White knows he is innocent. He is assisted by Carole Bouquet to shed light on this matter, and he discovers that he has a perfect double, Patrick Olivier, who, having suffered in his life to be like Michel Blanc, decided to enjoy.

==Cast==
* Michel Blanc as Michel Blanc / Patrick Olivier
* Carole Bouquet as Carole Bouquet
* Philippe Noiret as Philippe Noiret
* Josiane Balasko as Josiane Balasko
* Marie-Anne Chazel as Marie-Anne Chazel
* Christian Clavier as Christian Clavier
* Guillaume Durand as Guillaume Durand
* Charlotte Gainsbourg as Charlotte Gainsbourg
* David Hallyday as David Hallyday
* Estelle Lefébure as Estelle Hallyday (as Estelle Hallyday)
* Gérard Jugnot as Himself
* Dominique Lavanant as Herself
* Thierry Lhermitte as Thierry Lhermitte
* Mathilda May as Mathilda May
* Roman Polanski as Himself
* Philippe du Janerand as Inspector
* François Morel (actor)|François Morel as Inspectors assistant
* Jean-Louis Richard as Psychiatrist
* Raoul Billerey as Michel Blancs father
* Dominique Besnehard as Michel Blancs agent

==Awards==
* 1994 Cannes Film Festival: Best Screenplay 
** Technical Grand Prize 

==References==
 

==External links==
* 

 
 
 
 
 
 
 

 
 