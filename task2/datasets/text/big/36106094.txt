Under the Mountains
{{Infobox film
| name           = Under the Mountains
| image          = 
| image_size     = 
| caption        = 
| director       = Béla Balogh
| producer       = 
| writer         =  József Pakots
| narrator       = 
| starring       = Oszkár Dénes   Ila Lóth   László Mihó
| music          = 
| editing        = 
| cinematography = Dezsö Nagy
| studio         = Star Filmgyár
| distributor    = 
| released       = 7 July 1920
| runtime        = 
| country        = Hungary Silent  Hungarian intertitles
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 Hungarian silent silent drama film directed by Béla Balogh and starring Oszkár Dénes, Ila Lóth and László Mihó.

==Cast==
* Oszkár Dénes - Sebastiano, földbirtokos 
* Ila Lóth - Marta, molnárlány 
* László Mihó - Maruccio, molnárinas 
* Iván Petrovich - Pedro, a pásztor  
* Magda Posner - Lujza 
* Soma Szarvasi - Tomasso apó 
* Leóna Szarvasiné  

==Bibliography==
* Cunningham, John. Hungarian Cinema: From Coffee House to Multiplex. Wallflower Press, 2004.

==External links==
* 

 

 
 
 
 
 
 
 