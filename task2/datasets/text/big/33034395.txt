Eli Sjursdotter
{{Infobox film
| name           = Eli Sjursdotter
| image          = 
| image size     =
| caption        = 
| director       = Arne Bornebusch   Leif Sinding
| producer       = 
| writer         = Henning Ohlson   Johan Falkberget (novel)
| narrator       =
| starring       = Sonja Wigert   Ingjald Haaland   Sten Lindgren
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 31 October 1938
| runtime        = 79 minutes
| country        = Norway
| language       = Norwegian
| budget         = 
| gross          =
| preceded by    =
| followed by    =
}}
 Norwegian drama film directed by Arne Bornebusch and Leif Sinding, starring Sonja Wigert, Ingjald Haaland and Sten Lindgren. The film is based on Johan Falkbergets 1913 novel by the same name.

In 1719, during the Great Northern War, a Swedish army is making its way home through Trøndelag during a particularly cold winter. The Swedish soldier Per Jönsa (Lindgren) seeks refuge in a mountain cottage, where he is cared for by the peasant daughter Eli Sjursdotter (Wigert). This is not well received by Elis father (Haaland), who hates the Swedes.

==External links==
* 
* 

 
 
 
 