We Can't Go Home Again
{{Infobox Film name           = We Cant Go Home Again image          = We Cant Go Home Again.jpg caption        =  director       = Nicholas Ray producer       =  writer         = Nicholas Ray Tom Farrell Susan Ray starring       = Richie Bock Tom Farrell Nicholas Ray Danny Fisher Jill Gannon Jane Heymann Leslie Levinson Stanley Liu Luke Oberle Phil Weisman music          =  cinematography = Richie Bock Peer Bode Danny Fisher Mark Goldstein Stanley Liu Steve Maurer editing        = Richie Bock Charles Bornstein Tom Farrell Danny Fisher Mark Goldstein Nicholas James Carol Lenoir	 distributor    =  released       =  runtime        = 88 min. (1976 version) country        = United States awards         =  language       = English budget         =  preceded_by    = 
}}
 experimental feature film directed by Nicholas Ray in collaboration with his film students at Binghamton University. Ray and the students play fictionalized versions of themselves.
 Rough versions festivals as Cannes premiere in 1973    ), and the most well-known cut was completed in 1976. 

Ray was still making alterations to it at the time of his death in 1979.

==Production==

In 1971, Nicholas Ray received an invitation to lecture at   and the experimental filmmaker Ken Jacobs. Ray became close with his students, and together with them moved into a house off-campus where the group creating a filmmaking commune.   There they began work on We Cant Go Home Again, sharing all filmmaking duties.
 Super 8, 16mm, 35mm and a video synthesizer which had been donated to the project by Nam June Paik.  

== References ==
 

== External links ==
*  
*  
*  
* , Jonathan Rosenbaum

 

 
 
 
 
 
 
 