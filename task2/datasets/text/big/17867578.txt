Sarvam
{{Infobox film
| name = Sarvam
| image = Sarvam Poster.jpg
| image_size =
| caption = Promotional Poster Vishnuvardhan
| producer = K. Karunamoorthy C. Arunpandian Vishnuvardhan A. Gokul Krishna Rajkannan Arya Trisha Indrajith Rohan Shiva Anu Hasan Prathap Pothan
| music = Yuvan Shankar Raja
| cinematography = Nirav Shah
| editing = Sreekar Prasad
| studio = Ayngaran International
| distributor = Ayngaran International
| released =  
| runtime = 143 minutes
| country = India
| language = Tamil
| gross =  14 crore
}}
 Indian Cinema Tamil romantic Arya and Indrajith in other prominent supporting roles,  the film, which had been under production since end-2006, is produced and distributed by K. Karunamoorthy and C. Arunpandian under the banner of Ayngaran International, filmed by Nirav Shah and edited by Sreekar Prasad, whilst, the film score was composed by Yuvan Shankar Raja, who composed the soundtrack as well, which was released on 14 February 2009, coinciding with Valentines Day.

The film revolves around five individual characters and the events in their life, that, on the one hand, changes everything in their respective lives, which the films title refers to, and, on the other hand, also brings together these people. It is an anthology film, that contains two parallel moving stories, which are connected by a freak accident in Chennai. A young architect, falls in love with a young beautiful pediatric doctor, runs after her and tries to convince her to marry her, while, simultaneously, a morose ex-football coach troubles a software engineer by vowing to kill his eight-year-old son. How these characters get connected and confronted through the accident forms the crux of the story.
 Telugu under Hindi as Wardaat- The Revenge.
 Vishnuvardhan made lot of changes on original story line for Indian Cinema.

==Plot== Upanishad quote: "Death is just the beginning of another life". It follows a flashback

Karthik (Arya (actor)|Arya) is a young happy-go-lucky carefree architect, who, one day, comes across a young beautiful girl, Sandhya (Trisha Krishnan) in a kart racing|go-karting race. Karthik immediately falls in love with Sandhya and, having decided, that she is the right girl for him, wants to convince her to marry him. Sandhya is a pediatric doctor and became so due to her love for children. Karthik, after finding out several information about her, first arranges a meeting with her, by pretending to her, that she had caused a car accident and damaged his car. When that approach, however, fails, he eventually goes to the hospital, where Sandhya works, becoming a regular visitor there and making all kinds of possible efforts to woo her. Sandhya, however, has other interests and shows no signs of reciprocating his love, since she believes, Karthik is too carefree and not serious about life, who, nonetheless, keeps following her and trying still to convince her. When, one day, Sandhyas parents tell her about her to-be-fixed marriage, she, finally recognizes, that Karthik is apparently the right boy and gradually falls for him, slowly developing a deep love for him. She wholeheartedly agrees with the marriage.

Sid, that he was undergoing, and threatens him, that he will feel the same pain only, when his own son dies. When a workmate of Naushad informs him, that Eashwar had visited him and requested information about Naushad, and tells him to be aware, he and his son escape to a place of his friend, which is located at the Elliots beach in Chennai.

Meanwhile, Karthik and Sandhya, who do spend time together, happily enjoying life, are ready for the marriage and make plans for the future. Karthik, then, suddenly requests Sandhya to postpone their marriage for a year, but in which he meets with disapproval, since Sandhya persists on marrying at the earliest possible date. To solve this problem, they decide to hold a cycling race across the Elliots beach road, dealing, that the winner may make the decision, which has to be accepted by the other person. At the same time, Imaan and his friend fly a kite on the terrace of Naushad’s friend. Suddenly Imaan faints and falls, which distracts the boys, who were around him, and they let the kite slip and fly away, the thread cuts loose. Sandhya, who is ahead of Karthik and leading the race, rides straightly into the kite thread, which cuts her neck, injuring her fatally. She eventually dies in the hospital because of severe blood loss.

Six months later, Sandhyas father visits Karthik, who is devastated, grief-stricken, trying to convince himself in vain. Sandhya’s father then informs Karthik, that even after death Sandhya has saved the life of a child, since her heart has been transplanted into a boy, who incidentally happens to be Naushad’s son Imaan. Karthik becomes emotional and is overjoyed, that at least one part of his love is alive and goes to meet Naushad and Imman. He finds out, that they’ve left their home in Chennai and moved to Munnar in Kerala. He travels to Munnar to meet them there and finally gets to see them and feels happy again.
 car mechanic Sajid, who played professionally football with Eashwar, a few years ago and who allows Eashwar to stay at his home.

Karthik a last time goes to see father and son, before he wants to leave for Chennai, when then suddenly a car tries to hit Imman to death. Karthik, who was just readying to go home, comes back and is able to save Imman. Shocked by that incident, Karthik doesn’t believe, that it was merely an accident and wants Naushad to tell the truth, who has been admitted to hospital after having been injured in the accident. Karthik then gets to know about Eashwar, who was the man in the car, and his intentions. Eashwar has bought the same car, which “killed” his family members and seeks revenge for their death by killing Naushad’s son Imaan in return to “equal it”. Karthik then decides to take Imaan along him to a nearby place, where he had stayed before, promising Naushad, that he will protect and look after Imaan.
 cat and mouse chase, whereas Eashwar wants to dispose of Karthik to get to Imaan and to kill him. Sajid sees, Eashwar chasing Karthik and offers to help by hiring people, who will kill both silently without mentioning Eashwar’s and Sajid’s names.

How Karthik protects Imaan and saves him from Eashwar as a tribute to Sandhya and his love forms the crux of the story.

==Cast and crew==

===Cast=== Arya as Karthik. Karthik is a young happy-go-lucky architect, who falls in love with Sandhya and tries to convince her to marry him. Through Sandhya, he gets confronted with Naushad and Imaan and later with Eashwar. Sarvam was Aryas first film after Naan Kadavul, with which he was engaged for more than two years and for which he had grown a long, thick beard.    In contrary to that, Sarvam features him as a romantic lover hero, whereby he had to remove the beard and hence, the "Old Arya returned".   Actually, Vishnuvardhan said, Aryas transition from Naan Kadavul to Sarvam was very difficult, since Naan Kadavul was a "lengthy process", where Arya had "forgotten the real in him".    This happens to be the third film, in which Vishnuvardhan and Arya worked together after Arinthum Ariyamalum and Pattiyal with Arya citing, that Vishnuvardhan knows him "in and out" and that he was able to bring out the "best and finer aspects" of him, knowing his strengths and weaknesses,    while Vishnuvardhan confessed, that working with Arya was comfortable.  Arya later also mentioned, that the role of Karthik was the best role he had ever played in his film career, since the character had "many shades" as romantic, serious and emotional and there was "so much of variation".  pediatric doctor, Nila and Ileana DCruz|Ileana, were considered and opted out. According to Vishnuvardhan, Trisha, though she is not a new entrant, looks "very fresh" in the film, which, he believes, is because of her co-star Arya.  Besides, she has an "interesting quality", which is her "innocent smile irrespective of her mischievousness", Vishnuvardhan states, which was "very essential" for the character and the reason for giving her the role.  football coach Rottweiler dog, Mani Ratnams 2002 film Kannathil Muthamittal. He got the role of Eashwar, replacing Upendra, who again was signed after Arjun Sarja|Arjun, Mohan Lal and Nana Patekar had refused the offer. Vishnuvardhan once mentioned, that Chakravarthy has a "spaced out look about him", which was needed for his character in the film and which made him to rope him in.  Indrajith as Naushad. Naushad is a software engineer, who had hit Eashwars wife, Gheetha, and son, Naveen, to death in a road accident and is, therefore, threatened by Eashwar, who wants to kill Naushads son in the same way in return. Sarvam happens to be the comeback vehicle to the Tamil film industry for Indrajith, too, who had lastly appeared in a Tamil film in 2001, in En Mana Vaanil along with Jayasurya and Kavya Madhavan.
* Master Rohan Shiva as Imaan. Imaan is the eight-year-old son of Naushad and the target of Eashwar, who wants to kill him. Imaan, is actually, the person, who "causes" the accident, which eventually brings together Karthik, Naushad and himself, and, hence, causes, indirectly, the conflict between Karthik and Eashwar. Rohan Shiva was signed for the role of Imaan, after 100-150 boys were tested for eight months. Vishnuvardhan finally chose Imaan, citing, that "he is not handsome, but there is something very cute about him". 
* Krishna as Krishna. Krishna is a friend of Karthik and works with him. He provides the comic relief in the first half of the film. Krishna has become a regular in Vishnuvardhans film, having acted already in Arinthum Ariyamalum and Pattiyal. As in the earlier films, Krishnas character in this film is again named Krishna. car mechanic and Eashwars friend and ex-football teammate, who meets him in Munnar, Kerala, where ha has moved and lives with his family. When finding out about Eashwars intentions, he decides to help him. Wasim is another boy, who was introduced by Vishnuvardhan in this film, who, he cites, is "so natural" as the other actors as well that you "tend to forget that it is cinema". 
* Anu Hasan as Gheetha in a cameo role. Gheetha is Eashwars wife, who, along with her son, Naveen, dies in a car accident.
* Prathap Pothan as a psychiatrist in a cameo role. He plays the psychiatrist, whose advice is sought by Karthik, who himself is advised to do so after behaving "strangely".

Other supporting artists in cameo roles include Raviprakash and Sriranjani as Trishas father and mother respectively, Geetha Agarwal, Minnale Ravi and Utthara Krishna Dass among others.

===Crew=== Vishnuvardhan
* Production: K. Karunamoorthy & C. Arunpandian Vishnuvardhan & A. Gokul Krishna
* Dialogues: Rajkannan
* Music: Yuvan Shankar Raja
* Cinematography: Nirav Shah
* Editing: Sreekar Prasad
* Art direction: Man Jagadh
* Sound designer: A. S. Laxmi Narayanan Dinesh & Geetha Kapoor
* Stunts: Thiyagarajan
* Lyrics: Pa. Vijay
* Costume design: Anu Vardhan
* P.R.O.: Mounam Ravi & Diamond Babu
* Stills: Sittarasu
* Production manager: Sundarraj
* Executive producer: K. Vijayakumar

==Production==

===Development=== Vishnuvardhan announced, Surya in Saran    to produce the film, who all opted out, though. Later, Surya opted out of the project, too, and after Vishnuvardhan tried to cast R. Madhavan in his film, which also failed for unknown reasons, the film was finally shelved in December 2006 and Vishnuvardhan instead started to film the remake of the 1980 Rajinikanth-starrer Billa (1980 film)|Billa, titled Billa (2007 film)|Billa as well, starring Ajith Kumar in the lead role. 
 Arya in the lead role.    It was also rumored, that music composer Yuvan Shankar Raja would produce the film,   which turned out be wrong with K. Karunamoorthy and C. Arunpandian taking over the project and producing it under the banner of Ayngaran International. The film was finally kicked off in May 2008 with Arya (actor)|Arya, Trisha Krishnan and J. D. Chakravarthy in the lead roles.

The film was first reported to be a remake of the 1986 Hollywood thriller Hitchhiking|Hitcher  and later to be a remake of another Hollywood film, the 2003 Academy Award-nominated drama 21 Grams. 

===Casting=== model and Nila and Anushka were Gautham Menons Vaaranam Aayiram.  By November 2006, R. Madhavan was announced as the hero of the film, before the film was finally shelved in December 2006.
 Arjun was Arya playing Arya and Trisha Krishnan, who were teaming up for the first time.   Reports indicated that the script has been changed several times, to suit the lead actors respectively,  Vishnuvardhan, however, confirmed, that the script did not change, stating that there are similarities between Surya and Arya and the script suited both and therefore a change was not necessary.
 Tamil film Sathyam (2008 film)|Sathyam at the same time. Eventually Vishnuvardhan roped in Telugu actor J. D. Chakravarthy for the same role replacing Upendra.

Apart from these three actors, it is said, that Malayalam actor Indrajith Sukumaran, brother of well-known actor Prithviraj Sukumaran, who acts in his second direct Tamil film after En Mana Vaanil, a child actor, named Rohan Shiva, playing the 8-year-old Imaan, and a Rottweiler dog play important roles in the film.
 Dinesh and Geetha joined Kalyan as the choreographer and Manu Jagadh replaced as Remiyan as the art director.

===Filming===
Shooting for Sarvam was held for nearly nine months in various locations, including exotic places, where never before a film was shot. It started finally in June 2008, after Vishnuvardhan returned from the Cannes Film Festival 2008 Cannes Film Festival|2008, where his film Billa was screened, and after Arya had completed Naan Kadavul, for which filming was held for nearly one and a half year.  The first half of the film was shot in and around Chennai, while the second half was shot in Kerala, mostly in deep forests,   hence, the first half will be very "colourful", while the mood in the second half completely changes, getting "rainy, misty and completely green and dark". 
 Gemini flyover, the Elliots beach in Besant Nagar,  the flyover at Kathipara Junction and at the San Thome Basilica in Santhome in Chennai,  where scenes for the first half of the film as well as some song sequences were filmed.
 climax were shot. For the climax scene, a 90-feet-tall dilapidated church was built up and was made to look like 150 years old,   while the remaining part of the climax was shot back in Chennai at Prasad Studio, where again a huge set was erected by Manu Jagadh.   Other filming locations in Kerala include Cochin,  Kannoor and Chalakudy. 
 Gemini flyover in Chennai. A lift, which was installed to carry materials to the eighth floor of the building, where filming was to be held, collapsed suddenly, killing two technicians, who were working on the lift.   After actor J. D. Chakravarthy, too, was injured in a stunt sequence, it was reported, that Vishnuvardhan had decided to change the film title,  which, however, didnt happen.

The songs were filmed at several places across India, like in Tamil Nadu, Goa, Kerala and Gujarat.
A folk song ("Sutta Suriyana") was shot in a Chennai studio, choreographed by Dinesh and picturized on the lead couple, Arya and Trisha.  Another song ("Neethane"), which was choreographed by Kalyan, was filmed on the new bridge at the Kathipara Junction, Chennai and the Indian Air Force base at Cholavaram.  A fourth song ("Kaatrukulle"), picturized again on Arya and Trisha, was shot in late September and October at the beaches in Goa and a lake in Ladakh, that was choreographed by Geetha, a student of the renowned choreographer Farah Khan.     In the first weeks of February, the last song, a romantic duet, choreographed by Dinesh, involving the lead couple once more ("Siragugal"), was shot at the Rann of Kutch, a saline clay desert in Gujarat, close to the Pakistan border,   according to Vishnuvardhan, a location that was never before used or seen in a Tamil film. 

With this song, the shooting was eventually wrapped up in February 2009 and post-production works began. 

==Soundtrack==
{{Infobox album |  
| Name = Sarvam
| Type = soundtrack
| Artist = Yuvan Shankar Raja
| Cover = Sarvam.jpg
| Released =  14 February 2009 (India)
| Recorded = 2006 / 2008 Feature film soundtrack
| Length = 24:00
| Producer = Yuvan Shankar Raja
| Label = Ayngaran Music An Ak Audio
| Reviews =
| Last album = Siva Manasula Sakthi (2009)
| This album = Sarvam (2009)
| Next album = Vaamanan (2009)
}}

The film score and the soundtrack were composed by Yuvan Shankar Raja, who renewed his previous association with director Vishnuvardhan, who have earlier created very successful musical albums in Arinthum Ariyamalum, Pattiyal and Billa (2007 film)|Billa. The soundtrack was formally released on 14 February 2009, coinciding with Valentines Day at the Inox Complex in Chennai. 
 Aayirathil Oruvan, for which Yuvan Shankar Raja was first signed to score the music, but which he left in the midst, after he had a fall out with director Selvaraghavan.  However, Aayirathil Oruvan has one song Un Mele Aasadhan that was based on and sounds similar to Adada Vaa. 

The album received very positive reviews. Behindwoods.com reviewer Malathy Sundaram gave three stars out of five and wrote: "For the second time (after Aegan,) Yuvan has attempted to break free of the general norms of music here and the result is rather pleasing. The album feels fresh and breezy. With relevant picturization, the songs could make an impact".  Rediff.com reviewer Pavithra Srinivasan too gave three stars out of five and wrote: "Sarvam is appealing" and "It looks like Yuvan, after a couple of not-so-memorable efforts, is on his way to finding his groove again. Sarvam manages to snag your attention, in a good way".  Indiaglitz.com reviewer wrote: "On the whole, Yuvans melodies would turn to be the chartbusters of the season and any guesses about best picks. Adada Vaa Asathala would go straight onto everyones music library on their I-pods and mobiles. Sutta Suriyana is one more song that enlivens the feel while Signature Music would rock on the air", giving verdict as "effectually brilliant".  Milliblog.com reviewer Karthik wrote: "After two average soundtracks, Yuvan gets his groove back"   and later listed the album as the Top Tamil OST of 2009 and the song Neethane as the Top Tamil song of 2009 in his annual music round-up. 

{| class="wikitable"
|- style="background:#ccc; text-align:center;"
! Track !! Song !! Singer(s)!! Duration !! Notes Sarvam#Songs picturization|* 
|- 1 || Adada Vaa || Ilayaraaja, Andrea Jeremiah, Suvi Suresh || 4:31 || Picturized on Arya, shot in sets
|- 2 || Neethane ||  
|- 3 || Sutta Suriyanae || Vijay Yesudas || 4:32 || Picturized on Arya, featuring Indrajith and Rohan as well
|- 4 || Kaatrukulle || Yuvan Shankar Raja || 4:23 || Picturized on Arya and Trisha in a huge Glass-box with beach, forest and desert as backdrops
|- 5 || Siragugal ||  
|}

==Release==
Sarvam sold 3.3 million tickets worldwide.

===Marketing=== interval of Pongal 2009 cover story to Sarvam. 

===Theatrical run=== Raghava Lawrences Rajadhi Raja, 2009 Indian 2009 IPL Indian censor board had certified the film on 21 April 2009, giving it a clean "U" certificate without any cuts.    

===Reception=== Vishnuvardhan for Nirav Shahs Yuvan Shankar Rajas music and background score, Manu Jagadhs art work, while Thyagarans "close combat action scenes" were cited as "spellbinding".  Overall, the film was described as a "decent thriller". 
 Arya and Sreekar Prasads editing "fits the bill".  Overall, she cites, that the film is worth a watch for the "stunning visuals", "musical score" and "Trishas sequence", who, she claims, to be "perhaps the only shining part of the movie itself". 

2 out of 5 stars were given by Behindwoods.com as well, which said, that Sarvam "rides on its visuals throughout" and that "an enjoyable first half is negated by a slack second".    It blamed, Vishnuvardhan to be a director, who "got lots of flair and style", but whose writing "lets him down". Arya was cited as "top notch", while Trisha "looks fresh and beautiful and carries off her role in style" and "the pair has worked exceedingly well".  According to Behindwoods, the real hero, however, was Nirav Shah, praising, that "every single frame is a treat to watch" and that it is "the visuals" that keep the audience "engrossed at many points in the second half", appreciating Manu Jagadh art works as well.  Overall, it is cited, that you can enjoy the "frames", "beautiful visuals", the "Arya-Trisha chemistry" and forget the "thrills and suspense", which "are missing the film". 

In comparison, the film received very positive reviews as well. Indiaglitz described the film as "Grand, glitzy and glossy", citing, that Vishnuvardhan has "mastered the art of making a trendy, sophisticated and chic film",  while Zonkerala called it "Vishnus skilled piece of work", 

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 