Something in the Wind
{{Infobox film
| name           = Something in the Wind
| image          = Something in the Wind 1947 Poster.jpg
| border         = yes
| caption        = Theatrical release poster
| director       = Irving Pichel
| producer       = Joseph Sistrom
| screenplay     = {{Plainlist|
* Harry Kurnitz
* William Bowers
}}
| story          = {{Plainlist|
* Fritz Rotter
* Charles ONeal
}}
| starring       = {{Plainlist|
* Deanna Durbin
* Donald OConnor
* John Dall
}}
| music          = 
| cinematography = Milton R. Krasner Otto Ludwig
| studio         = Universal Pictures
| distributor    = Universal Pictures
| released       =  
| runtime        = 94 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Something in the Wind is a 1947 American musical comedy film directed by Irving Pichel and starring Deanna Durbin, Donald OConnor, and John Dall.    Based on a story by Fritz Rotter and Charles ONeal, the film is about the grandson of a recently deceased millionaire who mistakes a beautiful female disc jockey for her aunt, who once dated his grandfather. This was OConnors first film after he returned from military service in World War II. The film includes the famous "I Love a Mystery" number performed by OConnor.

==Cast==
* Deanna Durbin as Mary Collins
* Donald OConnor as Charlie Read
* John Dall as Donald Read
* Charles Winninger as Uncle Chester Read
* Helena Carter as Clarissa Prentice
* Margaret Wycherly as Grandma Read
* Jean Adair as Aunt Mary Collins
* The Williams Brothers as Singing Quartet
* Jacqueline deWit as Fashion Show Saleslady 
* Jan Peerce as Tony, the Policeman
* Bess Flowers as Woman in Audience (uncredited)
* Andy Williams as Himself (uncredited)   

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 


 