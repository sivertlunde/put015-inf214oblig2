One Big Holiday (film)
{{Infobox film
| name = One Big Holiday
| caption = 
| image =
| alt =
| director = Michael Feld
| producer = Christopher Guetig
| cinematography = Robert M. Edgecomb
| writer =
| music = 
| editing = Jack Price
| studio = 
| distributor = 
| released =  
| runtime = 29 minutes
| country = United States
| language = English
| budget =
}}
One Big Holiday is a short documentary film about the band My Morning Jacket, and their relationship with their hometown of Louisville, Kentucky. Shot over a period of one week in October 2010, it chronicles the preparations of the band as they readied for a homecoming concert in the newly constructed downtown arena. Executive Producer Christopher Guetig was the bands drummer from 2000-2002.

==Screenings==
The film held its world theatre premiere at the LA Film+Music Weekend on March 23–25, 2012 in Los Angeles, CA. 

It also was an official selection of the Chicago International Movies and Music Festival, April 12–15, 2012, in Chicago, IL. 

The film had its Louisville premiere on opening night of Flyover Film Festival on June 7, 2012, with special guest Jim James of My Morning Jacket. 

The film is part of My Morning Jackets exclusive deluxe box set of Circuital, released May 31, 2011. 

==References==
 

==External links==
*  
* , 26 May 2011

 
 
 
 
 
 
 
 


 