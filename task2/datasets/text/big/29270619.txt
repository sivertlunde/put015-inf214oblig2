Rhythm in the Air
{{Infobox film
| name           = Rhythm in the Air
| image          =
| image_size     =
| caption        =
| director       = Arthur B. Woods
| producer       = Michael Balcon
| writer         = Jack Donohue Vina de Vesci Jack Donohue Tutta Rolf
| music          =
| cinematography = Roy Kellino
| editing        = Fox British
| released       = 1936
| runtime        = 72 minutes
| country        = United Kingdom
| language       = English
}} 1936 British Jack Donohue and Norwegian Tutta Rolf.  The film was a quota quickie production, written by Donohue in collaboration with Vina de Vesci, and was reportedly largely autobiographical, as the events in the film closely mirrored Donohue own experience of coming to be a dancer.

==Plot==
Jack Donovan (Donohue), a riveter working on the construction of a high-rise building, is distracted from his work by spying through a nearby window on a lissom young woman Mary (Rolf) as she rehearses her tap-dancing routines.  When she finishes, he pauses to give the unsuspecting Mary an ovation of cheers and wolf-whistles, but in the process loses his balance and falls to the ground, breaking both ankles.

The sympathetic Mary, who witnessed his fall, later visits him in hospital.  Finding him very attractive, she claims that as his bones start to mend, tap-dancing is a wonderful way to strengthen his muscles and joints.  He laughs at the absurdity of the suggestion.

Fully recovered, Jack goes back to his job, only to find that he has developed a new and severe fear of heights and it is quite impossible to continue in his line of work.  He meets up again with Mary, and now takes her up on her suggestion of learning to tap.  He finds he has a natural aptitude, and soon takes up dancing professionally.  The couple fall in love, and are soon married. 
 
==Cast== Jack Donohue as Jack Donovan
* Tutta Rolf as Mary
* Vic Oliver as Tremayne
* Leslie Perrins as Director
* Kitty Kelly as Celia
* Tony Sympson as Alf
* Terry-Thomas as Frankie

== External links ==
*  
*   at BFI Film & TV Database

 

 
 
 
 
 
 
 
 


 