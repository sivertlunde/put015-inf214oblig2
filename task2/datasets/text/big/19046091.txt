Two Crowded Hours
{{Infobox film
| name           = Two Crowded Hours
| image          = Two Crowded Hours.jpg
| image_size     = 
| caption        = John Longden (right) and Jerry Verno (centre)
| director       = Michael Powell Jerome Jackson Henry Cohen
| writer         = Joseph Jefferson Farjeon
| narrator       = 
| starring       = John Longden Jane Welsh Jerry Verno
| music          = 
| cinematography = Geoffrey Faithfull
| editing        = Arthur Seabourne    Fox Film Twentieth Century-Fox (USA)
| released       = 8 July 1931 (London) 28 December 1931 (UK)
| runtime        = 43 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} Quota quickie and is the first film where Powell is credited as the director.

==Plot==
A murderer is on the run from prison and is out to get everyone, especially the girl (Jane Welsh), who put him there. The detective (John Longden) gives chase with the help of a London cabbie (Jerry Verno) who has aspirations of becoming a policeman himself.

==Production== Jerry Jackson for the Film Engineering Company and distributed by the British arm of Fox Pictures. With accomplished players John Longden (star of Blackmail (1929 film)|Blackmail) and Cockney character actor Jerry Verno, shooting was completed in 12 days in April 1931 in and around Londons Soho. "It was played for laughs and thrills", Powell said, "and we were paid £1 per foot by Fox. We got £4,000 on delivery so obviously we had to make it for £3,000".  Although a few stills survive, there is no known print of Two Crowded Hours in existence.

==Cast==
* John Longden as Harry Fielding
* Jane Welsh as Joyce Danton
* Jerry Verno as Jim Michael Hogan as Scammell
* Edward Barber as Tom Murray

==Status== 75 Most Wanted" lost films, along with two later Powell films The Price of a Song (1935) and The Man Behind the Mask (1936). 

==References==

===Notes===
 

===Bibliography===
 
* Chibnal, Steve. Quota Quickies : The Birth of the British B Film. London: BFI, 2007. ISBN 1-84457-155-6
* Powell, Michael. A Life in Movies: An Autobiography. London: Heinemann, 1986. ISBN 0-434-59945-X.
 

==External links==
* 
* 
*  reviews and articles at the  
* , including extensive notes

 

 
 
 
 
 
 
 
 
 