The Battle of Algiers
 
{{Infobox film
| name = The Battle of Algiers
| image = The Battle of Algiers poster.jpg
| caption = Theatrical release poster
| director = Gillo Pontecorvo
| writer = Gillo Pontecorvo Franco Solinas
| music = Ennio Morricone Gillo Pontecorvo
| starring = Jean Martin Saadi Yacef Brahim Haggiag Tommaso Neri
| cinematography = Marcello Gatti
| editing = Mario Morra  Mario Serandrei
| distributor = RCS MediaGroup|Rizzoli, Rialto Pictures
| released =  
| runtime = 120 minutes
| language = Arabic French
| country = Italy Algeria
| budget = $800,000
}} French government Battle of Algiers. An Italo-Algerian production, it was directed by Gillo Pontecorvo and shot on location. The film, which was shot in a Roberto Rossellini|Rossellini-inspired newsreel style—in black and white with documentary-type editing—is often associated with Italian neorealism cinema. 

The film has been critically celebrated and often taken, by insurgent groups and states alike, as an important commentary on urban guerrilla warfare. It occupies the 48th place on the Critics Top 250 Films of the 2012 Sight & Sound poll  as well as 120th place on Empire (magazine)|Empire magazines list of the 500 greatest movies of all time. 

Algeria gained independence from the French, a matter which Pontecorvo portrays in the films epilogue. The film concentrates on the years between 1954 and 1957 when the guerrilla fighters regrouped and expanded into the Casbah, which was met by French paratroopers attempting to regain territory. The highly dramatic film is about the organization of a guerrilla movement and the methods used by the colonial power to contain it.

A subject of socio-political controversy, the film wasnt screened for five years in France, where it was later released in 1971. 

==Subject== events that National Liberation coda depicting nationalist demonstrations and riots, suggesting that although France won the Battle of Algiers, it lost the Algerian War.

The tactics of the FLN guerrilla insurgency and the French counter insurgency, and the uglier incidents of the war, are depicted. Colonizer and colonized commit atrocities against civilians. The FLN commandeer the Casbah via summary execution of Algerian criminals and suspected French collaborators and use terrorism, including bombings, to harass Europeans. The security forces resort to lynch mobs and indiscriminate violence against the opposition. French paratroops are depicted as routinely using Torture during the Algerian War|torture, intimidation, and murder. Pontecorvo and Solinas have several protagonists, based on historical war figures. The story begins and ends from the perspective of Ali la Pointe (Brahim Haggiag), a petty criminal who is politically radicalized while in prison and then recruited by FLN commander El-hadi Jafar (Saadi Yacef, dramatizing a character based on himself ).
 Petit Omar, a street urchin who is an FLN messenger; Larbi Ben Mhidi, a top FLN leader and the films political rationale for the insurgency; and Djamila Bouhired|Djamila, Zohra Drif|Zohra, and Hassiba Benbouali|Hassiba, three FLN women urban guerrillas who effect a terrorist attack. The Battle of Algiers also features thousands of Algerian extras; Pontecorvos intended effect was the "Casbah-as-Greek chorus|chorus", communicating with chanting, wailing, and physical effect. 

==Production and style==
===Screenplay===
The Battle of Algiers was inspired by Souvenirs de la Bataille dAlger, by Saadi Yacef, the campaign account of an FLN military commander.  The book, written by Yacef while a prisoner of the French, was FLN morale-boosting propaganda for militants. After independence, Yacef was released and became part of the new government. The Algerian government backed a film of Yacefs memoir; exiled FLN man Salash Baazi approached the Italian director Gillo Pontecorvo and screenwriter Franco Solinas with the project.

Yacef wrote his own screenplay, which does not have any conversations or plot,  which the Italian producers rejected as too biased towards the Algerians. Although sympathetic to Algerian nationalism, the Italian businessmen insisted on dealing with events from a neutral perspective. The final screenplay of The Battle of Algiers has an Algerian protagonist and depicts the cruelty and suffering of French and Algerians. 
 , where the film was shot]]

Despite its basis in true events, The Battle of Algiers uses composite characters and changes the names of certain persons. For example, Colonel Mathieu is a composite of several French counterinsurgency officers, especially Jacques Massu.  Saadi Yacef has stated that Mathieu was actually more based on Marcel Bigeard, although the character is also reminiscent of Roger Trinquier.  Accused of portraying him as too elegant and noble, screenwriter Solinas denied that this was his intention; the Colonel is "elegant and cultured, because Western civilization is neither inelegant nor uncultured". 

===Visual style===
For The Battle of Algiers, Pontecorvo and cinematographer Marcello Gatti filmed in black and white and experimented with various techniques to give the film the look of newsreel and documentary film. The effect was convincing enough that American releases carried a disclaimer that "not one foot" of newsreel was used. 

Pontecorvos use of fictional realism enables the movie to operate along a double-bind as it consciously addresses different audiences. The film makes special use of television in order to link western audiences with images they are constantly faced with and always depicting the truth. The film seems to be filmed through the point of view of a western reporter, as telephoto lenses and hand-held cameras are used, whilst depicting the struggle from a safe distance with French soldiers placed between the crowds and camera.  

===Cast===
Pontecorvo chose to cast non-professional Algerians he met, picking them mainly on appearance and emotional effect (as a result, many of their lines were dubbed).  The sole professional actor of the movie was Jean Martin, who played Coloniel Mathieu; Martin was a French actor who had worked primarily in theatre. Pontecorvo wanted a professional actor, but one with whom audiences would not be too familiar, which could have interfered with the movies intended realism. Martin had been dismissed several years earlier from the Théâtre National Populaire for signing the manifesto of the 121 against the Algerian War. Martin had also served in a paratroop regiment during the Indochina War as well as the French Resistance, thus giving his character an autobiographical element. The working relationship between Martin and Pontecorvo was not always easy, as the director, unsure that Martins professional acting style would not contrast too much with that of the non-professionals, kept arguing with his acting choices. 

===Sound and music===
Sound—both music and effects—performs important functions in the movie. Indigenous Algerian drumming, rather than dialogue, is heard during a scene in which female FLN militants prepare for a bombing. In addition, Pontecorvo used the sounds of gunfire, helicopters and truck engines to symbolize the French methods of battle, while bomb blasts, ululation, wailing and chanting symbolize the Algerian methods. Gillo Pontecorvo had written the music for The Battle of Algiers, but because he was classed as a "melodist-composer" in Italy, it required him to work with another composer which was undertaken by his good friend Ennio Morricone. 

==Post-release history==
===Critical reception=== Best Screenplay Best Director Best Foreign Language Film in 1967.    Other awards include The City of Venice Cinema Prize (1966), the International Critics Award (1966), the City of Imola Prize (1966), the Italian Silver Ribbon Prize (director, photography, producer), the Ajace Prize of the Cinema dEssai (1967), the Italian Golden Asphodel (1966), Diosa de Plata at the Acapulco Film Festival (1966), the Golden Grolla (1966), the Riccione Prize (1966), Best Film of 1967 by Cuban critics (in a poll sponsored by Cuban magazine Cine), and the United Churches of America Prize (1967). In 2010, the movie was ranked sixth in Empire (magazine)|Empire magazines "The 100 Best Films Of World Cinema". 

Not all reception was positive, in France, "Cahiers du cinéma devoted a special feature to the film consisting of five articles by critics philosophers, and film scholars, wherein the negative assessment of the film was cast in such strong terms that it undermined, on moral grounds, the legitimacy of any critic or analyst who did not condemn the film, let alone anyone who dared consider it worthy of filmic attention."  

===Political controversies in the 1960s=== banned there for five years.  The topic was controversial because there were differing views on whose side the film was on. Many in France felt the film was too sympathetic to the Algerian view and may be why it was not screened for many years. "A moving film that presented events from the Algerian point of view." 

Pontecorvo said "The Algerians put no obstacles in our way because they knew that Id be making a more or less objective film about the subject. The French authorities, who were very sensitive on the Algerian issue, banned the film for three months." Threats from fascist groups prevented screenings of the film for four years despite Pontecorvos attempt to make a politically neutral film. 

===The Battle of Algiers and guerrilla movements=== national liberation Black Panthers, the Provisional Irish Republican Army, and the Jammu Kashmir Liberation Front.  The Battle of Algiers was apparently also Andreas Baaders favourite movie. 

==Screenings worldwide==
===1960s screening in Argentina===
 , 28 July 2005, extract from El Silencio transl. in English by openDemocracy.  Anibal Acosta, one of the ESMA cadet interviewed 35 years later by French journalist Marie-Monique Robin described the session:

 
They showed us that film to prepare us for a kind of war very different from the regular war we had entered the Navy School for. They were preparing us for police missions against the civilian population, who became our new enemy. 
 

===2003 Pentagon screening===
During 2003, the film again made the news after the Directorate for Special Operations and Low-Intensity Conflict at the Pentagon offered a screening of the movie on August 27, regarding it as a useful illustration of the problems faced in Iraq.  A flyer for the screening read:

:"How to win a battle against terrorism and lose the war of ideas. Children shoot soldiers at point-blank range. Women plant bombs in cafes. Soon the entire Arab population builds to a mad fervor. Sound familiar? The French have a plan. It succeeds tactically, but fails strategically. To understand why, come to a rare showing of this film."  , The New York Times, 7 September 2003. 
 Defense Department official (Directorate for Special Operations and Low-Intensity Conflict) in charge of the screening, "Showing the film offers historical insight into the conduct of French operations in Algeria, and was intended to prompt informative discussion of the challenges faced by the French." 

===2003–2004 theatrical re-release===
At the time of the 2003 Pentagon screening, legal and  "Copyright infringement|pirate" VHS and DVD versions of the movie were available in the United States and elsewhere, but the image quality was degraded. An Italian film restoration had been done in 1999. The restored print allowed Rialto Pictures to acquire the distribution rights for a December 1, 2003, theatrical re-release in the United Kingdom, a January 9, 2004, theatrical re-release in the United States, and a May 19, 2004, re-release in France. The film was shown in the Espace Accattone rue Cujas in Paris from November 15, 2006, to March 6, 2007. 

===2004 Criterion edition===
On October 12, 2004, The Criterion Collection released the movie, transferred from a restored print, in a three-disc DVD set. The extras include former US counter-terrorism advisors Richard A. Clarke and Michael A. Sheehan discussing The Battle of Algiers s depiction of terrorism and guerrilla warfare and directors Spike Lee, Mira Nair, Julian Schnabel, Steven Soderbergh, and Oliver Stone discussing its influence on film. Another documentary includes interviews with FLN members Saadi Yacef and Zohra Drif.

==Further reading==
* Aussaresses, General Paul. The Battle of the Casbah: Terrorism and Counter-Terrorism in Algeria, 1955-1957 (New York, Enigma Books, 2010). ISBN 978-1-929631-30-8.

==See also==
 

*Fang of the Sun Dougram
* Dallos

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*  

 
 
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 