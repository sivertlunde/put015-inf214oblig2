Saagar (1985 film)
{{Infobox film
| name           = Saagar
| image          = Saagar.jpg
| caption        = Official DVD Cover
| director       = Ramesh Sippy
| producer       = G.P. Sippy
| writer         = Javed Akhtar
| narrator       =
| starring       = Rishi Kapoor Kamal Hassan Dimple Kapadia Nadira Saeed Jaffrey Satish Kaushik
| music          = R.D. Burman
| cinematography = S.M. Anwar
| editing        = M. S. Shinde
| distributor    = Sippy Films
| released       =  
| runtime        = 187 minutes
| country        = India
| language       = Hindi
}}

Saagar (  film directed by Ramesh Sippy. The film stars Rishi Kapoor, Kamal Hassan and Dimple Kapadia.
Saagar was a comeback film for Kapadia, and contained a fleeting topless shot of her. Saagar was List of Indias official entries to the Oscars|Indias official entry for the Academy Award for Best Foreign Language Film in 1985. 

This was the second instance in the history of Filmfare Awards where an actor has been nominated for both Best Actor as well as Best Actor in Supporting role, the previous nominee for both awards was Ashok Kumar (for Aashirwad in 1970). Kamal Hassan ultimately won the Best Actor award, his first and only award in that category for a Hindi film.

==PLOT ==
the film begins with Raja(Kammal Hassan) a fisher man living in a village in goa.Mona (dimple kapadia) is rajas long time friend and the daughter of dislvia.Raja spends most of his time in Monas bar-restaurant.raja who had lost his parents at a young age was taken umder the protective wings of ms.Joseph, who is like his mother.it is later revealed that raja loves Mona and is willing to marry her,while Mona does not share such a feeling for raja.raja plans to confess his true feeling to Mona only after setting a bright tommorow.mean while wealthy industrialist Kamla Devis only grandson RAvi has returned to assist her in her business.Ravi father had gone against Kamla Devis wishes by marrying a poor girl,leaving everything.While RAvi was 7 years old his parents died in a car accident,leaving RAvi with Kamla Devi.RAvi meets a old man baba.In a turn of events RAvi meets Mona and soon both falls for each other.What follows in these 3 lives forms the major crux of the movie.

== Soundtrack ==
The music was composed by R. D. Burman and the lyrics were by Javed Akhtar.

*01.   Jaane Do Naa   –   Asha Bhosle, Shailendra Singh
*02.   O Maria   –   Asha Bhosle, S. P. Balasubrahmanyam
*03.   Chehra Hai Ya   –	Kishore Kumar 
*04.   Saagar Kinare   –   Kishore Kumar, Lata Mangeshkar
*05.   Saagar Kinare (Sad)   –   Lata Mangeshkar
*06.   Sach Mere Yaar Hai   –   S. P. Balasubramaniam
*07.   Yoon Hi Gaate Raho	–   S. P. Balasubramaniam, Kishore Kumar

==Reception==
According to Asiaweek, "Saagar offers a skimpy eternal-triangle plot, but it is remarkable for its polished narration and masterly technique. The romance is subdued, symbolised by waves gently caressing the shore." It further praised the performances, calling Kapadia "a delight" and claiming that Hassan "steals the show with his subtle performance," and the direction by Sippy, who "has succeeded in injecting vitality, beauty and deep insight into a gossamer-thin story."  India Today wrote, "Like Sholay, and only like Sholay, Saagar is purely a directors film." 

==Awards==
;Won
1986 Filmfare Awards (India) 
* Filmfare Award – Best Actor – Kamal Hassan
* Filmfare Award – Best Actress – Dimple Kapadia
* Filmfare Award – Best Cinematography – S.M. Anwar
* Filmfare Award – Best Male Playback Singer – Kishore Kumar (for Saagar Kinare)

;Nominated
* Filmfare Award for Best Supporting Actor – Kamal Hassan

==See also==
* List of submissions to the 58th Academy Awards for Best Foreign Language Film
* List of Indian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 

 
 
 
 
 