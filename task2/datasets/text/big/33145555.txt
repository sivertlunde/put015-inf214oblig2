L'Amour braque
 
{{Infobox film
| name           = LAmour braque
| image          = Lamour braque.jpg
| caption        = Theatrical release poster
| director       = Andrzej Zulawski
| producer       = Antoine Gannagé Alain Sarde
| writer         = Etienne Roda-Gil Andrzej Zulawski Fyodor Dostoevsky (The Idiot)
| starring       = Sophie Marceau Francis Huster Tchéky Karyo
| music          = Stanislas Syrewicz
| cinematography = Jean-François Robin
| editing        = Marie-Sophie Dubus
| distributor    = AMLF (France)
| released       =  
| runtime        = 101 minutes
| country        = France
| language       = French
}} romantic drama film directed by Andrzej Zulawski and starring Sophie Marceau, Francis Huster, and Tchéky Karyo. Inspired by Dostoyevskys novel The Idiot the film is about a bank robber who meets a neurotic dreamer on his way to Paris whom he considers to be an idiot. The dreamer follows him everywhere and soon falls in love with his girlfriend, resulting in a tragic ending.    The film received a Fantasporto International Fantasy Film Award Nomination for Best Film in 1986.   

==Plot==
Following a successful bank robbery, Micky (Tchéky Karyo) tries to take back his girlfriend Mary (Sophie Marceau) who had been taken from him by the brothers Venin. On his way to Paris, Micky meets Leon (Francis Huster), a neurotic dreamer who is considered an idiot by Micky and his associates. Uncertain about Mickys actions, Leon follows him everywhere and eventually falls in love with Mary. This strange love triangle leads to a tragic ending. 

==Cast==
* Sophie Marceau as Mary 
* Francis Huster as Léon 
* Tchéky Karyo as Micky 
* Christiane Jean as Aglaé 
* Jean-Marc Bory as Simon Venin 
* Michel Albertini as André,  
* Saïd Amadis as Le caïd 
* Roland Dubillard as Le commissaire 
* Ged Marlon as Gilbert Venin 
* Serge Spira as Le baron 
* Julie Ravix as Gisèle 
* Marie-Christine Adam as Maries Mother
* Azeddine Bouayad as Harry Cleven   

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 


 