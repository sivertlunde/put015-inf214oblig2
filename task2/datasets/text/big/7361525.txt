The Hidden Blade
{{Infobox film
| name = The Hidden Blade
| caption = DVD cover
| image	= The Hidden Blade FilmPoster.jpeg
| image size = 190px
| director = Yōji Yamada 
| producer = Hiroshi Fukazawa
| writer = Yoshitaka Asama Yōji Yamada Shūhei Fujisawa (story)
| starring = Masatoshi Nagase Takako Matsu Yukiyoshi Ozawa
| music = Isao Tomita
| cinematography = Mutsuo Naganuma
| editing = Iwao Ishii
| distributor = Shochiku
| released =  
| runtime = 132 minutes
| country = Japan Japanese
| budget =
| gross = $8,043,781 
}} 1860s Japan, directed by Yoji Yamada. The plot revolves around several samurai during a time of change in the ruling and class structures of Japan. The film was written by Yamada with Yoshitaka Asama and, like its predecessor The Twilight Samurai, based on a short story by Shūhei Fujisawa. The soundtrack is an original composition by Isao Tomita.

==Plot==
The film takes place in Japan in the 1860s, a time of cultural assimilation.  Two samurai, Munezo Katagiri (Masatoshi Nagase) and Samon Shimada (Hidetaka Yoshioka), bid farewell to their friend, Yaichiro Hazama (Yukiyoshi Ozawa), who is to serve in Edo (present-day Tokyo) under the shogunate of that region.  Though the position is desirable, Katagiri voices his concern that a man of Yaichiro’s character is likely to run into trouble.  His doubts are assured when the married Yaichiro expresses an intention to indulge in Edo’s sensual pleasures while stationed there. 
 ritual suicide after a construction job gone wrong).  She desires a match between Samon and Shino (Tomoko Tabata), Katagiri’s sister.  Also present is Kie (Takako Matsu), the Katagiri’s housekeeper, who is schooled in etiquette and literacy.  In a voiceover, Katagiri hints at an affection for Kie, but then mentions that around the same time Shino married Samon, Kie married a man of the merchant class and left the Katagiri household.

Three years go by during which Katagiris mother passes away.  While walking through town, he sees Kie in a kimono shop where she assures him that she is well.  Weeks later, however, Shino tells Katagiri that from the start of her marriage, Kie has been forced to perform all manner of duties to the point that she is little more than a slave to her new family.  Concerned, Katagiri visits Mrs. Iseya (Sachiko Mitsumoto), Kie’s mother-in-law, and finds her incoherent with illness.  Outraged, he demands that Kie’s husband file divorce papers then carries her to his own house to recover.

In the midst of everything, the changing times have forced Katagiri and his fellow samurai to learn the art of Western weaponry, a thing that the elder members of the clan treat with disdain.  Word arrives from Edo that government officials thwarted an uprising against the shogun and that Yaichiro, Katagiri’s friend, was involved.  After being brought back to the village in a prisoners cage, Yaichiro is denied the honor of ritual suicide and must live out the remainder of his days in a cell.  Believing that Yaichiro’s friends are complicit, Hori (Ken Ogata), the clan’s chief retainer, demands that Katagiri identify them, but he refuses, citing his honor as a samurai, and is violently dismissed.

Meanwhile, Kie has since recovered and is once again Katagiri’s housekeeper.  Though their fondness for one other is evident, both are keenly aware of their difference in social class and act accordingly.  Despite this, gossip prompts Katagiri to send Kie back to the countryside to live with her father.  Shortly after, Yaichiro breaks out of prison and takes a family hostage.  Hori demands that Katagiri dispatch him.  

Knowing that Yaichiro is the better swordsman, Katagiri visits their former teacher (Min Tanaka), who is now a farmer, and learns a dangerous maneuver that involves turning ones back on the enemy.  The next day, Katagiri arrives on the outskirts of the village and attempts to persuade Yaichiro to surrender.  When the latter refuses (accusing Hori and the other leaders of incompetence), the two engage in single combat during which Katagiri uses the new technique to deliver a mortal blow.  A dying Yaichiro attempts the same, but is gunned down by foot soldiers hiding in the woods.  Knowing that this is a blatant dishonor to a samurai, Katagiri is dismayed.  On returning to the village, he runs into Yaichiro’s wife (Reiko Takashima) who reveals that she paid a visit to Hori the night before and exchanged sexual favors for his promise to keep Yaichiro alive (a promise that was never fulfilled).  Bound by an oath to commit suicide should Yaichiro die, she takes her own life.

Unsure of his fealty, Katagiri approaches Hori with his treachery, to which he crudely admits.  Realizing that the Hazamas were victims of a corrupt system, Katagiri avenges them both by stabbing Hori in the heart with a thin blade (the technique known as “the hidden blade” which leaves almost no trace of blood (in the original Japanese version the technique is actually called "the demons claw/scratch", the wound was so small it appeared to be caused by nonhuman sources) ).  Katagiri buries the blade at the Hazama’s grave as a form of atonement and relinquishes his samurai status.  Resolved to become a tradesman, he leaves the village for a distant island, but not before seeing Kie.  With custom no longer a breach, Katagiri proposes marriage and Kie accepts.  The film ends with them sitting on a hill holding hands, pondering a future together.

==Cast==
*Masatoshi Nagase - Munezo Katagiri
*Takako Matsu - Kie
*Hidetaka Yoshioka - Samon Shimada
*Yukiyoshi Ozawa - Yaichiro Hazama
*Tomoko Tabata - Shino Katagiri
*Reiko Takashima - Hazamas wife
*Kunie Tanaka - Katagiri Kanbee
*Chieko Baisho - Mrs. Katagiri
*Min Tanaka - Kansai Toda
*Nenji Kobayashi - Ogata
*Ken Ogata - Chief Retainer Hori
*Hiroshi Kanbe - Naota
*Sachiko Mitsumoto - Mrs. Iseya
*Nana Saito - Bun

==Awards==
In addition to 16 nominations,  the film received the following awards:
* The Japanese Academy Award for "Best Art Direction" to Mitsuo Degawa and Yoshinobu Nishioka
* Hochi Film Award for "Best Actress" to Takako Matsu
* Mainichi Film Concours for "Best Supporting Actress" to Tomoko Tabata

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 