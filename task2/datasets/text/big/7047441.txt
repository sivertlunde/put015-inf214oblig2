Billy's Dad Is a Fudge-Packer!
{{Infobox film
| name = Billys Dad is a Fudge-Packer!
| image = Billys Dad Is a Fudge-Packer.jpg
| image size =
| caption =
| director = Jamie Donahue
| producer = Steak House Juli Vizza
| writer = Jamie Donahue
| narrator =
| starring = Spencer Daniels Robert Gant
| music = Rob Cairns
| cinematography = Thomas L. Callaway
| editing = Juli Vizza Power Up Films
| released =  
| runtime = 10 minutes
| country = United States
| language = English
| budget =
| gross =
}} short comedy film written and directed by Jamie Donahue in her first non-acting effort. It is a parody of the 1950s social guidance films, and depicts the life of a boy learning about adulthood in a traditional family. The apparently innocent account of family life in the 1950s is loaded with sexual innuendo. It was made by production company POWER UP.

==Plot==
Billy is a boy who has to decide what he will do with his life. His father works at the candy factory as a "fudge packer" (which is modern slang for homosexual anal sex) and "has several men under him". His mother is visited by a short, mannish woman who knows how to please the local housewives. His sister is preparing to be a good wife, and in almost every shot is moving a phallic object to her mouth.

==Cast==
* Spencer Daniels as Billy
* Robert Gant as Billys dad
* Cady Huffman as Billys mom
* Alex Borstein as Betty Henderson
* Gina Rodgers as Billys sister
* D. C. Douglas as 50s announcer

==Film festivals==
Film festivals in which Billys Dad Is a Fudge-Packer! appeared include:

===2005===
* Sundance Film Festival
* Tribeca Film Festival
* PlanetOut Film Festival COMEDY FINALIST
* Austin Gay & Lesbian International Film Festival
* Boston Gay & Lesbian Film Festival
* Brisbane Queer Film Festival
* Cineffable – Paris Lesbian Film Festival
* Cinequest
* Closet Cinema - Southwest Gay and Lesbian Film Festival
* Comedia - Just for Laughs
* Connecticut Gay & Lesbian Film Festival
* Fairy Tales International Gay & Lesbian Film Festival
* FilmOut San Diego Film Festival
* Frameline
* Fresno Reel Pride Film Festival
* Image + Nation – Montreal Gay & Lesbian Film Festival
* Inside Out - Toronto Lesbian & Gay Film and Video Festival
* London Lesbian Film Festival
* Martha’s Vineyard Independent Film Festival
* Melbourne Queer Film Festival
* Miami Gay & Lesbian Film Festival
* Nashville Film Festival
* Newport Beach Film Festival
* North Carolina Gay & Lesbian Film Festival
* OUT Loud Long Beach Art & Film Festival
* Oslo Gay & Lesbian Film Festival
* Outfest
* Palm Springs International Festival of Short Films
* Philadelphia Gay & Lesbian Film Festival
* PlanetOut Film festival
* Q Cinema-Fort Worth Gay & Lesbian Film Festival
* Queer Screen-Mardi Gras Film Festival, Darlinghurst, Australia
* Queersicht – Bern, Switzerland Gay & Lesbian Film Festival
* Reel Affirmations – Washington DC GLBT Film Festival
* Reel Queer Syracuse
* Rhode Island International Film Festival
* Sacramento International Gay & Lesbian Film Festival
* Seattle Lesbian and Gay Film festival

==Reviews==
*  

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 

 
 