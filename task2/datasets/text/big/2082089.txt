Trixie (film)
{{Infobox film
| name           = Trixie
| image          = Trixie-film.jpg
| image_size     = 175px
| caption        = Official DVD Cover
| director       = Alan Rudolph
| producer       = Robert Altman
| writer         = John Binder Alan Rudolph
| starring       = Emily Watson Nick Nolte Brittany Murphy Lesley Ann Warren Nathan Lane
| music          = Mark Isham Roger Neill
| cinematography = Jan Kiesser
| editing        = John Helde Michael Ruscio
| studio         = Pandora Cinema Sandcastle 5 Productions Sony Pictures Classics
| distributor    = Columbia TriStar Home Video Sony Pictures Classics Sony Pictures Television
| released       =  
| runtime        = 116 minutes
| country        = United States English
| budget         = $20 million
| gross          = $295,683
}}
Trixie is a 2000 American Mystery film|mystery-crime film directed by Alan Rudolph and starring Emily Watson, Nick Nolte, Will Patton and Brittany Murphy.

== Plot ==
Trixie Zurbo is an eccentric woman who longs to quit her job as a security guard in a department store and become a private detective. She finally gets her wish when she takes a job in security at a casino. She accidentally becomes involved in a murderous plot and Trixie takes her first case; however, her unschooled command of the English language and comedy intervenes and the mess begins.

==Cast==
* Emily Watson as Trixie Zurbo
* Dermot Mulroney as Dex Lang
* Nick Nolte as Senator Drummond Avery
* Nathan Lane as Kirk Stans
* Brittany Murphy as Ruby Pearli
* Lesley Ann Warren as Dawn Sloane
* Will Patton as W. Red Rafferty
* Stephen Lang as Jacob Slotnick
* Mark Acheson as Vince Deflore
* Vincent Gale as Sid Deflore
* Jason Schombing as Ramon
* Robert Moloney as Alvin
* Troy Yorke as Cleavon Arris
* Wendy Noel as Coffee Shop Waitress David Kopp as Bell Boy
* Ken Kirzinger as Averys Bodyguard
* Jonathon Young as Gas Attendant
* Terry Chen as Waiter Gary Jones as Lobbyist Zak Santiago Alam as Gang Member
* Tyler Labine as Gang Member
* Norman Armour as Dr. Gold
* Alvin Sanders as Capitol Building Custodian
* Peter Bryant as Cop #1
* Kate Robbins as Tourist Wife
* Michael Puttonen as Tourist
* Brendan Fletcher as CD Thief
* Terence Kelly as Mr. Lang
* Maria Herrera as security guard

==Release==
The film premiered on June 28, 2000 in New York and Los Angeles, California.

==External links==
 
*  
* 

 

 
 
 
 
 
 
 
 
 
 


 