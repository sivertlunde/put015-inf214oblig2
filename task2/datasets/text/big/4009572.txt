Voice (film)
 
{{Infobox film name           = Voice image          = Voice film poster.jpg caption        = Theatrical poster writer         = Choi Ik-hwan starring       = Kim Ok-bin Seo Ji-hye Cha Ye-ryun director       = Choi Ik-hwan producer       = Lee Choon-yun   Lee Mi-yeong editing        = Kim Sun-min cinematography = Kim Yong-heung music          = Lee Byung-hoon   Jang Young-gyu distributor    = Cinema Service released       =   country        = South Korea runtime        = 104 minutes language       = Korean gross          =    . Box Office Mojo. Retrieved March 4, 2012.  film name      = {{Film name hangul         = 여고괴담 4: 목소리 hanja          =  rr             = Yeogogoedam 4: moksori mr             = Yŏgogoedam 4: moksori}}
}} 2005 South horror film, first film.   

It was screened at the 2006 San Francisco Korean American Film Festival. 

== Plot ==
Young-eon (Kim Ok-bin), the top singer at an all-girls school, is murdered by a music sheet cutting her throat in the opening scene. The next day nobody can see or hear her except her friend Seon-min (Seo Ji-hye) who is able to hear her. After Young-eon convinces Seon-min that it is her voice that can be heard the two attempt to find out what happened to Young-eon. Seon-min begins speculating that the music teacher must have killed Young-eon. The mystery behind her death is slowly unraveled as Young-eon has flashbacks of life before her death.
 ghosts only remember what they want to, revealing that Young-eons memory may be incorrect. There seems to be another ghost haunting the school, one that the two friends suspect must be Young-eons murderer. Later, Young-eons body is found in the elevator, the same elevator that a previous student died in. We learn that Young-eon was alive all this time in a sleeping state but died when a student accidentally dropped a cart down the elevator shaft onto her neck.

Young-eon is running out of time to discover who the other ghost is; if her friend forgets about her then she will lose her voice. Some of the flashbacks include her mother, who is shown to be in a hospital. In one flashback, she talks to her mother about how as soon as she is old enough she will learn to drive. Another features how her mother committed suicide by jumping off the top of the hospital. In the end it is revealed that Young-eon drove her mother to suicide. It is also shown that Young-eon might have multiple personality disorder due to the trauma and guilt of encouraging her mother to kill herself. We see Young-eon bring tears to her music teachers eyes when she asks the teacher to sing, leading the teacher to contemplate suicide.

It turns out that Young-eon had been hearing the voices of her teacher and of the ghost, Hyo-jung (Im Hyeon-kyeong), the whole time, and Young-eon wanted the teacher dead to get rid of Hyo-jungs voice. Hyo-jung was a student and a former top singer at the school. She fell in love with the music teacher. Hyo-jung has the same voice as Young-eon and they are both recorded in a song.

We come to the conclusion that Hyo-jung shot the music sheet at Young-eon`s throat in the first scene out of anger at losing her voice. Seon-min thinks Young-eon should move on and "cross over". This angers Young-eon, since she wants nothing more than to live again. Not long after, Young-eon kills Cho-ah and takes over Seon-mins body. The last scene is of Young-eon in Seon-mins body talking to her reflection at her locker, and then walking alongside Seon-mins mother, talking about how as soon as she is old enough she will learn to drive.

At the very end of the movie (when the credits are rolling) we can see Cho-ah shouting but voiceless.

==Cast==
*Kim Ok-bin as Park Young-eon
*Seo Ji-hye as Seon-min
*Cha Ye-ryun as Cho-ah
*Kim Seo-hyung as Hee-yeon
*Im Hyeon-kyeong as Hyo-jung
*Jeon Ji-ae
*Nah Eun-kyeong

==References==
 

== External links ==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 