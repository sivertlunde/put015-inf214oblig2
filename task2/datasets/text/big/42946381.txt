Badlapur (film)
 
{{Infobox film
| name           = Badlapur
| image          = Badlapur Poster.jpg
| alt            =
| caption        = Theatrical release poster
| director       = Sriram Raghavan
| producer       = Dinesh Vijan Sunil Lulla
| writer         = Sriram Raghavan Arijit Biswas
| starring       =  
| editor         = Pooja Ladha Surti
| music          = Sachin-Jigar
| cinematography = Anil Mehta
| editing        = Pooja Ladha Surti
| studio         = Maddock Films
| distributor    = Eros International
| released       =  
| runtime        = 117 minutes
| country        = India
| language       = Hindi
| budget =    
| gross =     
}} Huma Qureshi, Yami Gautam, Vinay Pathak, Divya Dutta and Radhika Apte in supporting roles.  The film was released on 20 February 2015.  Box Office India reported that Badlapur grossed approximately   worldwide.   

==Plot==
 
 
Misha (Yami Gautam) and her son Robin become the unfortunate victims of a bank robbery getaway during a shopping trip. Liak (Nawazuddin Siddiqui) and his friend Harman (Vinay Pathak) are on the run after robbing a bank and use Mishas car to flee. Robin falls out of the moving vehicle during the struggle, while Misha is shot by Liak. Both mother and son later succumb to their injuries in hospital. Harman jumps out of the car to protect the money while Liak is arrested.
 Huma Qureshi), a prostitiute who is Liaks girlfriend, and visits her to enquire about the missing partner. However, he gets angry at Jhimlis trust in Liak and ends up torturing and raping her. After Liaks sentence, Raghu exiles himself to a reclusive life at a chance station where he gets down from a train and unsuccessfully attempts to commit suicide - Badlapur.
 
15 years later, a cancer-ridden and terminally ill Liak is paroled from prison on compassionate grounds. Unknown to Liak, Raghu has facilitated the release by providing a letter of pardon for Liak to the court, in lieu of knowing the elusive partners name from Liaks mother. Bent on vengeance, Raghu starts for Pune and finds Harman. Harman now runs a well-established restaurant in Pune and is, for all intents and purposes, a respected member of the society.

After his parole, Liak is kept under watch by police as they believe Liak will lead them to the unknown partner. Liak nevertheless manages to contact Harman, and arranges to meet Harman and take his share of the money. However, Raghu has already met and interrogated Harman along with Harmans wife Kanchan (Radhika Apte). Harman pleads with Raghu not to reveal his name to the cops. He tells Raghu that while he was involved in the robbery, it was Liak and not him who shot and killed his son and wife. Raghu, however, does not seem convinced. Harman promises to give Raghu Liaks share of money. Raghu takes the money but kills both Harman and his wife. Raghu meets Shobha (Divya Dutta), the NGO worker who was instrumental in arranging Liaks parole, to create an alibi. He pretends to be in love with Shobha and sleeps with her. Later he returns and buries the bodies of Harman and Kanchan on a lonely hilltop.

Police now turn suspicious and start investigating Harman, who seems to have disappeared with his wife.

A despondent Liak gets to know from his mother that Raghu came to know Harmans name from her. Liak breaks into Raghus house in search of the money and gets into a scuffle with Raghu. But Raghu soon overpowers Liak and beats him senseless. When Liak regains consciousness, Raghu tells him in detail how he killed Harman and his wife and disposed of their bodies.Liak tells Raghu that it was him, and not Harman who killed Raghus wife and child, but he did it in a moment of panic, unlike Raghu who, Liak points out, planned his murders with a cool mind, and without any certain knowledge of guilt. Liak tells Raghu that there is no difference between them, and that Raghu has become mad and should go for treatment.

Liak meets his former girlfriend Jhimli for one last time. Jhimli, who is now a keep of a local businessman, breaks down knowing that Liak is now near death.

The police inspector investigating the case now has circumstantial evidence that Raghu murdered Harman and his wife. He meets Raghu and offers a deal, the money which Raghu has got from Harman for removing the evidence.

Liak returns home in a distraught state knowing he has caused misery all around and possibly his friends death too. He finds his mother does not have a single good word to tell about his father and realizes that soon he will also be spoken of in the same manner by people - a complete no-gooder without a single good deed to speak of.

Raghu refuses the policeman and prepares for a final showdown, Liak walks into the police station and confesses to killing Harman and his wife and also reveals where to find the dead bodies. Liak takes Raghus blame, giving Raghu a second chance to live his life, and also doing one good thing in his own life before dying.

After seven months, Liak succumbs to cancer in jail. Jhimli meets Raghu and tells him that he has a second chance thanks to Liak, that very few get such chances - Liak never did - and that he should not waste it. She also asks Raghu that, now that his revenge is complete and all the guilty are dead, what good it did to him. Raghu is silent as Jhimli leaves him standing in the rain.

== Cast ==
* Varun Dhawan  as Raghav (Raghu)
* Nawazuddin Siddiqui  as Liak Tungrekar Huma Qureshi as Jhimli  
* Yami Gautam as Misha
* Vinay Pathak as Harman
* Divya Dutta as Shobha
* Radhika Apte as Kanchan (Koko)
* Ashwini Kalsekar as Mrs. Joshi
* Murali Sharma as Michael
* Pratima Kazmi as Liaks Mother  Zakir Hussain as Patil
* Kumud Mishra as Inspector Govind Mishra

== Production ==
The film began shooting in May 2014.   

==Critical reception ==
Raja Sen from Rediff rated it 4 out of 5 and said "Badlapur is a dark, unflinching, fantastic film"  Sudhish Kamath from The Hindu wrote that "Badlapur Darkly ambitious and very well made"  Rachit Gupta from Filmfare stated that "Badlapur to have Exhilarating performances, stellar storytelling"  Saibal Chatterjee of NDTV|NDTV.com noted pervasive contemptful treatment of women in the film, writing, "If one can ignore the overt misogyny on show all through the film, Badlapur throws up enough surprises to hold the viewers interest right until the bitter end."  Mohar Basu of Times of India rated Badlapur 4 out of 5.  Shubhra Gupta of The Indian Express rated the movie 2 stars out of 4, describing it as riveting, but also noting that the film "comes off too contrived in many places, and leaves us hanging in others."  Rajeev Masand of IBNLive rated it 3.5 out of 5 and wrote: "The pace slackens post-intermission, plot contrivances are many, and you might say the film is misogynistic in its treatment of women ...  , the film keeps you on your toes, curious to see where its twists and turns will lead." 

==Box office==

According to Koimoi, the film collected   in five days at the domestic box-office, with   on the first day.  
By the end of the third weekend, Badlapur grossed  .  By the end of its third week run, the movie crossed   nett at India box offices,  leading Koimoi to estimate the film has taken in double its expenses.     Box Office India reported that Badlapur grossed approximately   worldwide. 

==Soundtrack==

{{Infobox album
| Name = Badlapur
| Type = Soundtrack
| Artist = Sachin-Jigar
| Cover = 
| Released =   Feature film soundtrack
| Length =   Eros Music Happy Ending (2014)
| This album = Badlapur (2015)
| Next album = ABCD 2 (2015)
| Misc = {{Singles
 | Name = Badlapur
 | Type = Soundtrack
 | Single 1 = Jee Karda
 | Single 1 date =  
 | Single 2  = Jeena Jeena
 | Single 2 date =   
 | Single 3  = Judaai
 | Single 3 date =  
}}
}}
The soundtrack is composed entirely by Sachin-Jigar, while the lyrics were written by Dinesh Vijan and Priya Saraiya. The first song, "Jee Karda", was released as a single on 9 December 2014. The song "Jeena Jeena" was released on 14 February 2015. Jeena Jeena reached number one on the Indian iTunes Charts,  Radio Mirchi Charts,  and Bollywood Planet charts  for several weeks.

{{Track listing
| extra_column = Singer(s)
| total_length = 26:14
| title1 = Jee Karda
| note1 =  Divya Kumar
| length1 = 4:01
| title2 = Jeena Jeena
| extra2 = Atif Aslam
| length2 = 3:49
| title3 = Jee Karda
| note3 = Rock Version Divya Kumar
| length3 = 4:00
| title4 = Judaai
| extra4 = Rekha Bhardwaj, Arijit Singh
| length4  = 4:32
| title5 = Jeena Jeena
| note5 = Remix
| extra5 = Atif Aslam
| length5 = 3:39
| title6 = Badla Badla
| extra6 = Vishal Dadlani, Jasleen Royal, Suraj Jagan
| length6 = 3:13
| title7 = Sone Ka Pani
| extra7 = Priya Saraiya
| length7 = 3:00
}}

== References ==
 

== External links ==
*  

 
 
 