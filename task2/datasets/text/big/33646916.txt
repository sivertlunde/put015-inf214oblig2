Russian Holiday
{{Infobox film
| name           =Russian Holiday
| image          = 
| image_size     = 
| caption        = 
| director       = Greydon Clark
| writer         = 
| narrator       = 
| starring       = Jeff Altman
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 1992
| runtime        = 89 mins.
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
Russian Holiday is a 1992 film directed by Greydon Clark. It stars Jeff Altman and Victoria Barrett.  

==Cast==
*Jeff Altman as Milt Holly
*Victoria Barrett as Viktoria
*Susan Blakely as Susan Dennison
*Barry Bostwick as Grant Ames
*E.G. Marshall as Joe Meadows

==References==
 

==External links==
* 

 

 
 