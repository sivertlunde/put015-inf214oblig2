Shortbus
{{Infobox film
| name = Shortbus
| image = ShortbusPoster1.jpg
| image_size = 225px
| alt = Groups of different people look up behind the title and credits
| caption = Theatrical release poster
| director = John Cameron Mitchell
| producer = Howard Gertler John Cameron Mitchell Tim Perell Alexis Fish
| writer = John Cameron Mitchell Paul Dawson Lindsay Beamish PJ DeBoy Raphael Barker Peter Stickles Jay Brannan
| music = Yo La Tengo
| cinematography = Frank G. DeMarco
| editing = Brian A. Kates Q Television
| distributor = THINKFilm
| released =  
| runtime = 101 minutes  
| country = United States
| language = English
| budget = $2 million
| gross = $5,433,051
}}
Shortbus is a 2006 American film written and directed by John Cameron Mitchell. The plot revolves around a sexually diverse ensemble of colorful characters trying desperately to connect in New York City. The characters converge in a weekly Brooklyn artistic/sexual salon loosely inspired by various underground NYC gatherings that took place in the early 2000s. According to Mitchell, the film attempts to "employ sex in new cinematic ways because its too interesting to leave to porn."  Shortbus includes a variety of explicit scenes containing Unsimulated sex|non-simulated sexual intercourse with visible penetration and male ejaculation.

==Plot== prostitute James Paul Dawson), open up drag performance artist Justin Bond (playing himself). Sofia slowly opens up to new experiences; this includes a friendship with a dominatrix who goes by the name Severin (Lindsay Beamish). Sofias inability to achieve orgasm begins to cause conflict with Rob, who in turn begins attending Shortbus with Sofia.
 stalker neighbor, Caleb (Peter Stickles). Caleb fears the inclusion of Ceth in James and Jamies relationship might break them up and thus destroy his ability to live vicariously through them, so he attends Shortbus, where he confronts Ceth. Sofia begins to go daily to a spa with a sensory deprivation tank to meet with Severin, and the two begin to have intense conversations. Severin begins to help Sofia loosen up sexually; Sofia helps Severin achieve a deeper human connection than she had experienced before. One evening at Shortbus, Severin discusses with Sofia the idea of giving up sex work to pursue her dream of being an artist. The two then have an unplanned sexual experience, and once again Sofia is left unsatisfied. Throughout the film, James is seen making a film about himself and his relationship. It turns out to be a suicide note. He attempts to take his own life and is rescued by Caleb, who calls for help, but is too embarrassed to wait with James for the help to arrive. He writes his phone number and email address on James face while he is unconscious. When James wakes in the hospital, he calls Caleb. James goes to Calebs home to be consoled, but does not contact Jamie or Ceth, neither of whom can understand why he wouldnt call them or come home.

There follows an interlocking trio of scenes showing connections between the characters emotional problems and their sexual lives.
* At Calebs house, he and James have sex, and James allows Caleb to penetrate him, something he has never allowed anyone to do before. Afterward, in a dramatic revelation, James is seen in the window of Calebs apartment by Jamie, who realizes in that moment that James is alive and okay.
* Rob and Severin have a paid encounter where Rob asks to be flogged, something he couldnt ask Sofia to do. As this progresses, Severin loses control and Rob tries to comfort her. the lights go off across the city, seemingly caused by the simultaneous and collective frustration of the characters.

The film ends with a song by Justin Bond at Shortbus during the blackout. Sofia arrives and finds Rob with Severin and after acknowledging him sits down by herself. James and Jamie also arrive followed by Ceth and Caleb. Justins song starts on a wistful note, but as it progresses it becomes more energetic and positive thanks to the arrival of the Hungry March Band. This is mirrored in the actions and emotions of the actors. Jamie and James make out on the floor, and Ceth and Caleb start to hit it off. Rob seems to find a friend, and Severin progresses from nervous anxiety to happy elation upon the arrival of the band. Sofia engages in a threesome with a couple she has seen several times before and who appear to meet her prerequisite of "just beginning to experiment sexually" (Nick and Leah, played by Jan Hilmer and Shanti Carson), and finally achieves an orgasm, and the blackout affecting New York ends, as does the film.

==Cast==
  and PJ DeBoy.]]
* Sook-Yin Lee as Sofia Lin Paul Dawson as James
* Lindsay Beamish as Severin 
* PJ DeBoy as Jamie
* Raphael Barker as Rob
* Peter Stickles as Caleb
* Jay Brannan as Ceth
* Alan Mandell as Mayor Tobias
* Jan Hilmer and Shanti Carson as Nick and Leah
* Justin Vivian Bond as self / Mistress of Shortbus
* The Hungry March Band as Themselves

;Cameos
* Mitchell himself is featured in two shots in the Sex-Not-Bombs Room: the first, kissing a dark-haired man; and the second, performing cunnilingus on a woman sitting on a couch.
* Jonathan Caouette, director of Tarnation (film)|Tarnation, as the man who steals some "blondies" off a plate in the Sex-Not-Bombs Room. Mitchell met him when Caouette auditioned for Shortbus and subsequently became executive producer of Tarnation. Polaroid of her).
* Singer-songwriter Reginald Vermue (aka Gentleman Reg) has a song on the soundtrack and appears as "The Albino"
* Tristan Taormino, a sex columnist and porn film director/occasional actress, is one of the participants in the orgy sequence. "How to Shoot Sex: A Docu-Primer" (2007): Shortbus Region 1 DVD release (Th!nk Film) 
* Comedian Bradford Scobie (Doctor Donut) cameo as the woman being eaten out by Mitchell (on the couch next to Rob). Murray Hill (on the swing)
* Acrobatics|Acrobat-comedians The Wau Wau Sisters (on the trapeze) Dirty Martini (doing the strip tease)
* Burlesque star The World-Famous *Bob* (sitting with the Mayor in the Truth-Or-Dare Room)
* Ray Rivas, like his character Shabbos goy, is known for his blood-inclusive brand of extreme performance art which he performed at DUMBA.
* Bitch (performer)|Bitch, of the bands Bitch and Animal and "Bitch and the Exciting Conclusion," plays herself.
* Daniela Sea, queercore musician and former circus juggler of Cypher in the Snow and cast member of The L Word, also plays herself as Little Prince, her Radical Faerie nickname.
* JD Samson of the band Le Tigre (who actually played their first show in DUMBA) appears in the "Bitchs Bedroom" scene.
* Justin Tranter, lead singer of the band Semi Precious Weapons, is a spectator in Shortbus.

==Production==
The audition website elicited half a million hits and 500 audition tape submissions. 40 people were called in for improv auditions and nine actors were cast, all before there was any story in mind. The films characters and story were created collaboratively over 2.5 years through improvisation workshops with the cast. Mitchell wrote the screenplay from the raw material generated by the workshops and rehearsals.

Much of the sex in the film is unsimulated. Mitchell says:

 
 Mitchell also participated in the latter scene, performing oral sex on a woman for the first time "as a gesture of solidarity". 

The panoramic cityscape interspersed through the film was completely Computer-generated imagery|computer-generated and designed by John Bair.

===Inspiration=== Radical Faerie DUMBA (where the films salon was actually shot), and the weekly CineSalon film gathering, both of which were organized, in part, by Stephen Kent Jusick who plays Creamy in the film. The still-running underground Rubulad party was also an inspiration.

===Casting=== Hedwig and the Angry Inch, including:
* Sook-Yin Lee previously played Kwang-Yi, the electric guitarist in Hedwigs original band in Junction City, Kansas.
* Sofias therapy client, Cheryl, is played by Miriam Shor, who portrayed Yitzhak, Hedwigs back-up singer/lover.
* Mandell, who played Tobias, appeared as a restaurant customer who receives a "car wash" from Hedwig. Mandell met Mitchell when they performed together wearing radiation burn makeup in a 1987 Los Angeles stage production about Chernobyl. Mandell was a great friend and collaborator of playwright Samuel Beckett and was directed by him in Waiting for Godot and Endgame (play)|Endgame. They met after Mandell co-founded the San Quentin Prison Drama Workshop in the mid-60s which produced works of Beckett, among other playwrights. Mandell introduced Mitchell to Beckett in Paris in 1988, the year before Beckett died.
* PJ DeBoy appeared as the Hedhead with dreads in the scene where Hedwig recounts her Tommy Gnosis stories atop a big pile of tires.

===Jokes===
There is a plot device involving a vibrating egg with the brand name, In the Realm of the Senses. This is a wink to the explicit 1976 Nagisa Oshima film of the same name in which a man urges his lover to insert a hard-boiled egg in her vagina and "lay it".

Justin Bonds line: "As my dear, departed friend Lotus Weinstock used to say, I used to want to change the world. Now I just want to leave the room with a little dignity" refers to a stand-up line from real-life comedian Lotus Weinstock, a Los Angeles-based performer who was Lenny Bruces last girlfriend. Mitchell was friendly with Weinstock and her daughter, singer-songwriter Lili Haydn, before Weinstock died in 1997.

The film playing in the first Shortbus salon scene is, in effect, an elaborate "erotic" joke. Its entitled "Saverio" and was directed by Mitchell as a tribute to a 70s-era short film called "Calma". The score song is "Kids" by John LaMonica which appears on the film soundtrack.

==Release== premiering in film festivals. Howard Gertler and Tim Perell received an Independent Spirit Award as Producers of the Year.

===Critical reception===
Shortbus received mixed to positive reviews, currently holding a 66% "fresh rating on   based on 27 reviews. 

Lou Lumenick from New York Post wrote “Mitchell’s adventurous, big-hearted, pansexual mosaic of New Yorkers looking for love and orgasms (not necessarily in that order), is a rare example of a nonporn film that doesn’t exploit graphic sex as a gimmick.”    Peter Travers from Rolling Stone commented “If there is such a thing as hard-core with a soft heart, this is it.” 

===Public reaction===
Some have branded the film "Pornography|pornographic". In response, Mitchell says that the dictionary defines porn as "material created and viewed for the primary purpose of sexual arousal," and argues that the sex in Shortbus is often purposefully "de-eroticized" to "remove the cloud of arousal to reveal emotions and ideas that might have been obscured by it".

 

Sook-Yin Lee was nearly fired by the Canadian Broadcasting Corporation, for which she hosts a radio program, Definitely Not the Opera, because of her participation in several unsimulated sex scenes in the film. Ultimately, she retained her job as the CBC relented in the face of support for Lee from the public,  as well as from celebrities such as Gus Van Sant, Atom Egoyan, David Cronenberg, Francis Ford Coppola, Michael Stipe, Moby, Julianne Moore and Yoko Ono. 

The Korea Media Rating Board banned the film from public screenings in South Korea in 2007 for its sex scenes and gay content, although it still screened at film festivals not subject to the Boards authority. Sponge ENT, the films South Korean distributor, filed suit and in 2009, the Supreme Court of South Korea ordered the ban lifted, declaring the national film censorship law unconstitutional for its ambiguity. 

===Home media===
The film was released to  -style "How to Shoot Sex: A Docu-Primer", deleted scenes (including a dropped subplot about a character who is the Bush twins personal assistant), as well as a filmmaker/cast audio commentary.

==Soundtrack== Bright Eyes First Day V2 with the Anita ODay song replaced by "What Matters To Me" by Tiebreaker (John LaMonica).

# Scott Matthew – "Upside Down"
# Azure Ray – "If You Fall"
# Yo La Tengo – "Wizard’s Sleeve"
# Animal Collective – "Winter’s Love"
# Scott Matthew – "Surgery"
# Lee & Leblanc (with Sook-Yin Lee) – "Beautiful"
# Gentleman Reg – "It’s Not Safe"
# John LaMonica – "Kids"
# Scott Matthew – "Language"
# Jay Brannan – "Soda Shop"
# Anita ODay – "Is You Is or Is You Aint My Baby" The Ark – "Kolla Kolla (Nationalteatern Tribute Version)"
# Jasper James and the Jetset – "This House" The Ark – "This Piece of Poetry Is Meant To Do Harm"
# The Hidden Cameras – "Boys of Melody"
# Scott Matthew – "Little Bird"
# Justin Bond and the Hungry March Band – "In the End (Long Film Version)"
# Scott Matthew – "In the End (Acoustic)"
# Chimaira - "Taste My..."

==See also== Hedwig and the Angry Inch, Mitchells first film (2001) Rabbit Hole, Mitchells latest film (2010)
* Transvestism
* Unsimulated sex

==References==
 

==External links==
*  
*  
*  
*  
*  
*   at Fortissimo Films

 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 