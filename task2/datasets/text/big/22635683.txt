St Trinian's 2: The Legend of Fritton's Gold
 
 
{{Infobox film
| name           = St Trinians 2:  The Legend of Frittons Gold
| image          = St trinians.jpg
| image_size     = 215px
| alt            =
| caption        = British release poster
| director       = Oliver Parker Barnaby Thompson
| producer       = Oliver Parker Barnaby Thompson
| writer         = Jamie Minoprio Jonathan M. Stren  
| screenplay     = Piers Ashworth Nick Moorcroft
| based on       =  
| starring       = Rupert Everett Colin Firth David Tennant Talulah Riley Sarah Harding Tamsin Egerton Gemma Arterton
| music          = Charlie Mole
| cinematography = David Higgs
| editing        = Emma E. Hickox
| studio         = Ealing Studios Fragile Films
| distributor    = Entertainment Film Distributors
| released       =  
| runtime        = 106 minutes
| country        = United Kingdom
| language       = English
| budget         =
| gross          = £7,088,097 
}} adventure comedy previous film in the series.    It is the seventh in a long-running series of films based on the works of cartoonist Ronald Searle, and the second film produced since the franchise was rebooted in 2007.

==Plot==
One night, an unknown man telephones St. Trinians and bribes a girl, Celia, to find an old ring in the school library. She is caught by the other girls, and forced to explain that the man offered her £20,000 for the ring. Annabelle, the new head girl of St. Trinians, bartered for £100,000 from the man when he called back, but he refused and then proceeded to threaten them. Afterwards, the girls ask the headmistress, Camilla Fritton, about why the ring might be so valuable. She tells them a story about Pirate Fritton, her ancestor, who supposedly hid a great treasure somewhere in the world. He had two rings made, which when fitted together would reveal the location of the treasure.

The man on the telephone who wanted the ring is Sir Piers Pomfrey, a member of a secret society, and descendant of a man robbed by Pirate Fritton in 1589. Sir Piers manages to steal the ring from headmistress Camilla. The girls decide to search for the second ring, and find a clue left by another ancestor of the Frittons, who found the first ring (left in the library) and believed he knew where to look for the second. After a difficult deciphering of the clue, a midnight hunt in a graveyard, dealing with demonic possession, and disguising themselves as boys to infiltrate another school, the girls find the elusive second ring at last.

Now, all that is left is to steal back the ring that Sir Piers Pomphrey took.

They begin by researching Sir Piers Pomphrey (who runs a secret society known as AD1 who are a masculinist brotherhood), who has a seemingly flawless reputation, and discover that he knows Geoffrey Thwaites – an old flame of Camilla Fritton from the previous film. Camilla tracks him down to ask for help, and he informs her that Sir Piers is the head of a secret society known only as AD1. Geoffrey is persuaded to join forces with the girls of St. Trinians against Piers.

They organize an undercover attempt to break into the headquarters of AD1, recruiting former head girl Kelly (now an M.I.7 agent). They retrieve the first ring, only to discover that it is not in the secret societys vault. Luckily, Geoffrey realises where the ring really is, on Pomfreys finger, and manages to get it back to St. Trinians.

At last the two rings are together, and they reveal the treasures location; Shakespeares Globe. After organising a flash mob to keep AD1 from following them, a small group of the girls, their headmistress, and Geoffrey head for their new destination. They reach a secret room, and open a chest, only to find that there are no golden riches waiting for them. But what they do find is another treasure entirely: two surprising secrets held by Pirate Fritton. First; that he wrote plays under the name William Shakespeare, and second; that he was in fact a she, a secret the AD1 could never let be revealed.

Sir Piers Pomfrey reaches the girls and takes the evidence they have found, threatening them with a gun. Just as he is about to make a getaway down the Thames aboard his private boat, destroying the evidence, Celia (played by Juno Temple) has an idea. The girls sail the reconstruction of the Golden Hind down the Thames, while shooting cannons at Pomfreys boat. Then, Miss Fritton swings down and swipes the evidence from Pomfreys hands. The end shows St Trinians throwing a wild party.

==Cast==
  during filming in August 2009]]
* Rupert Everett as Miss Camilla Dagey Fritton, St Trinians headmistress
** Everett also plays Archibald Fritton, Camillas ancestor, and:
** Reverend Fortnum Fritton, Camillas ancestor
* David Tennant as Sir Piers Pomfrey, leader of secret society AD1
** Tennant also plays Lord Pomfrey, Sir Piers ancestor
* Colin Firth as Geoffrey Thwaites, the former Education Minister
* Talulah Riley as Annabelle Lealla Fritton, the Head Girl of St Trinians and Camillas niece
* Sarah Harding as Roxy, the rebellious Rock Star
* Tamsin Egerton as Chelsea Parker, Posh Totty No. 1
* Clara Paget as Bella, Posh Totty No. 2
* Gabriella Wilde as Saffy, Posh Totty No. 3
* Juno Temple as Celia, the Eco Ella Smith as Lucy, the Geek
* Montserrat Lombard as Zoe, the Emo
* Zawe Ashton as Bianca, the Rude Girl
* Jessica Agombar as Jessica, the Rude Girl
* Cloe Mackie & Holly Mackie as Tania & Tara, the Twins
* Gemma Arterton as Kelly Opossum Jones, the former Head Girl, now an MI7 agent
* Celia Imrie as St Trinians Matron
* Toby Jones as St Trinians Bursar
* Jodie Whittaker as Beverly, the Receptionist
* Christian Brassington as Peters, Sir Piers Assistant
* Oscar the dog as Heathcliff, the new school dog Ricky Wilson as Roxys boyfriend

==Production== Guildford Cathedral Choir.  On 16 August 2009 hundreds of extras, along with the main characters, filmed a mass dance scene in the style of a flash mob at Liverpool Street station|Londons Liverpool Street Station.

The manor house used as the girls school is Knebworth House in Hertfordshire.

==Release==
It was announced at the 2008 Cannes Film Festival that St Trinians II: The Legend of Frittons Gold was to be released on 18 December 2009.

===Box office=== Sherlock Holmes, and Avatar (2009 film)|Avatar.

===Critical reception===
The film received overwhelmingly negative reviews, holding a 10% rotten rating on Rotten Tomatoes. 

==Home media== region 2 region 1 DVD release occurred on 23 March 2011.

==Sequel==
It has been confirmed   that there will be a St Trinians 3: Versus the World.  There was also a competition held in 2010 for the chance for a girl to have a walk on role in the film.

As of March 2015 no cast confirmation or release date has been confirmed for the third instalment.

==See also==
* St Trinians
* Cross-dressing in film and television
*  

==References==
 

==External links==
*  
*  
*  
*  
*  
*   Movie and Character pictures

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 