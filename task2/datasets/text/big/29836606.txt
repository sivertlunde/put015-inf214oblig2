Night Slaves
{{Infobox film
| name           = Night Slaves
| image          =
| image_size     =
| caption        =
| director       = Ted Post Everett Chambers Everett Chambers Robert Specht Jerry Sohl (novel)
| narrator       =
| starring       = James Franciscus Lee Grant Andrew Prine Leslie Nielsen
| music          = Bernardo Segall
| cinematography =
| editor       =
| distributor    =
| released       = 1970
| runtime        =
| country        = United States English
| budget         =
}} American television The Outer The Night Stalker, Killdozer! (film)|Killdozer)and starred film and TV actor James Franciscus and Lee Grant. 
 The Outer The Immortal.

Jerry Sohl the author of the original novel noted that he was "very pleased with the whole thing...as a matter of fact, it interested me. They did a marvelous job.". 
 Planet of The Apes film series and had high regard for Franciscus as an actor. Post worked as a director-for-hire on TV series, TV movies and theatrical films but brought more than the usual "director- for- hire" ethos often working on improving scripts or working with actors to refine their performances to meet the needs of the material. 

==Plot== estranged married couple, take a vacation together while Clay recuperates from a serious auto accident. They end up in a sleepy little town which seems to be normal, except at night when the townspeople (and Marjorie) turn into zombies, file into trucks and head out of town. They always return by morning, and no one has any memories of the night before. Only Clay is unaffected, and no one believes his story.

==Cast==
*James Franciscus as Clay Howard
*Lee Grant as Marjorie Howard
*Scott Marlowe as Matt Russell
*Andrew Prine as Fess Beany / Noel
*Tisha Sterling as Annie Fletcher / Naillil
*Leslie Nielsen as Sheriff Henshaw
*Morris Buchanan as Mr. Hale John Kellogg as Mr. Fletcher

==Release==
The film originally aired on September 29, 1970	on the American Broadcasting Company (ABC).

The film has never officially been released on VHS or DVD.

==See also==
* They Came from Beyond Space

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 