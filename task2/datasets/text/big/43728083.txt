Frozen Fever
 
{{Infobox film name           = Frozen Fever image          = Frozen Fever poster.jpg border         = no alt            = caption        = Poster director       = {{Plainlist|
* Chris Buck Jennifer Lee
}} producer       = {{Plainlist|
* Peter Del Vecho
* Aimee Scribner
}} story         = {{Plainlist|
* Chris Buck
* Jennifer Lee
* Marc E. Smith
}} based on       =   starring       = {{Plainlist|
* Kristen Bell
* Idina Menzel
* Jonathan Groff
* Josh Gad
}} music          = Christophe Beck  cinematography = editing        = studio         = {{Plainlist|
* Walt Disney Animation Studios
* Walt Disney Pictures
}} distributor  Walt Disney Studios Motion Pictures  released       =   runtime        = 7 minutes    country        = United States language       = English budget         = gross          =  
}}
 Jennifer Lee. It is a sequel to the 2013 Walt Disney Animation Studios film Frozen (2013 film)|Frozen, and stars Kristen Bell, Idina Menzel, Jonathan Groff, and Josh Gad. The film debuted in theaters alongside Walt Disney Pictures Cinderella (2015 film)|Cinderella on March 13, 2015.

==Plot== Elsa plans to throw her the perfect surprise party with the help of Kristoff (Disney)|Kristoff, Sven, and Olaf (Disney)|Olaf. However, after planning the huge party and as Anna is being led on a "party treasure hunt," by following a string that winds through the kingdom, Elsa finds that she has caught a Common cold|cold. She starts sneezing and unknowingly produces a group of small snowmen much like Olaf with each sneeze, who proceed to wreak havoc with the birthday partys decorations, much to Kristoff and Olafs chagrin as they have been left by Elsa to watch the birthday decorations to make sure Annas birthday is perfect. While Elsa takes Anna on the hunt the two try to get the little snowmen under wraps and fix back the decorations in time for Anna and Elsas return to the palace.
 Hans is first film by scooping horse manure. The snowball crushes him against the wagon of manure much to the amusement of the nearby horses. Meanwhile, Elsa rests in bed while Anna takes care of her with Anna saying to  Elsa that allowing her the chance to take care of her was the best birthday present of them all. Olaf takes the tiny snowmen under his wing and brings them to Elsas ice castle with the help of Kristoff and Sven, where they stay with a disconcerted Marshmallow.

==Cast== Anna  Elsa  Kristoff  Olaf  Chris Williams as Oaken  Hans 
* Paul Briggs as Marshmallow 

Non-speaking characters include Sven, Kristoffs reindeer companion, and the Snowgies, the tiny snowmen. 

==Production==

On September 2, 2014, during the  ,   revealed that the cast had already recorded their vocal tracks, stating "We just worked on a short for Frozen."  On December 3, 2014, it was announced that Aimee Scribner would be a co-producer and that Frozen Fever would debut in theaters alongside Walt Disney Pictures Cinderella (2015 film)|Cinderella on March 13, 2015.     In late December, the co-directors told the Associated Press "There is something magic about these characters and this cast and this music. Hopefully, the audiences will enjoy the short were doing, but we felt it again. It was really fun."  Around the same time, Dave Metzger, who worked on the orchestration for Frozen, disclosed he was already at work on Frozen Fever. 

The short features the song "Making Today a Perfect Day", by Anderson-Lopez and Lopez.    At the premiere of Cinderella and Frozen Fever at the El Capitan Theatre in Hollywood, California, on March 1, 2015, Josh Gad told USA Today, "I want to apologize to parents everywhere for the fact that children are going to be singing a whole new Frozen song "    Gads wife noticed he was still humming it two days after he recorded his lines. 
 Big Hero 6 and before the studios animators had to start working on subsequent features (i.e., Zootopia).  The short was rendered using Hyperion, the new rendering system developed by Walt Disney Animation Studios for Big Hero 6. 

==Reception==
 Let It Go". 

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 