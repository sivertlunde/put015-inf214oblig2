Muppets from Space
{{Infobox film
| name           = Muppets from Space
| image          = Muppets from space.jpg
| image_size     = 215px
| alt            =
| caption        = Theatrical release poster Tim Hill
| producer       = Brian Henson Martin G. Baker
| writer         = Jerry Juhl Joey Mazzarino Ken Kaufman Hollywood Hogan Ray Liotta Andie MacDowell 
| music          = Jamshied Sharifi
| cinematography = Alan Caso Richard Pearson Michael A. Stevenson
| studio         = Jim Henson Pictures
| distributor    = Columbia Pictures 
| released       =  
| runtime        = 87 minutes   
| country        = United States
| language       = English
| budget         = $24 million 
| gross          = $22.3 million 
}} science fiction Tim Hill, produced by Jim Henson Pictures, and released to theaters on July 14, 1999. The film is a deviation from other Muppet films as it is the only non-musical film, as well as the first film in the series that focuses mainly on a character other than Kermit the Frog. The film was shot in Wilmington, North Carolina at EUE/Screen Gems.

==Plot==
The Great Gonzo has always been classified as a "whatever". But after he begins to have disturbing dreams of abandonment and rejection, he begins to realize just how alone he is in the world. One of his nightmares involves him being denied entry onto Noahs Ark by Noah (F. Murray Abraham). The next morning, Gonzo tells Kermit the Frog that he is getting tired of being called a "whatever." After an alien race appears to be trying to send him a message through bowls of cereal, Gonzo realizes that he may not be so alone after all and climbs to the rooftop to start watching the sky. Using a bolt of lightning, Gonzo communicates with a pair of cosmic fish, revealing to him that he is an alien from outer space.
 Animal spring Beaker which Miss Piggy uses on a gate guard (Ray Liotta).
 impounded due to the parking tickets that Agent Rentro forgot to take care of, Singer and Agent Rentro end up taking the company car which happens to be a cement truck.

The Muppets go to Cape Doom after rescuing Gonzo and, along with a crowd of alien-happy spectators, await their arrival. The ship comes to Earth and the aliens, who all resemble Gonzo, explain that many years ago they lost him but welcome him back into the fold. Singer turns up and tries to kill the Aliens, but thanks to Agent Rentro (who has disabled his Subatomic Neutro-Destabilizer by taking the part that fires the weapon), he cannot and is laughed at. Gonzo considers going into space with his long-lost family, but chooses not to. While grateful for his family for going through the trouble of locating and visiting him on Earth, he feels cant go with them, as he wants to stay with his fellow Muppet Show castmates. Singer is invited by the aliens to go with them, and leaves as Earths ambassador.

The film ends with the Muppets watching the stars on the roof. Gonzo tells Kermit he wonders why his family asked him to build a Jacuzzi. Pepe chuckles because he and Rizzo had pretended to be them and asked him to do it.

==Cast==
* Jeffrey Tambor as K. Edgar Singer, the head of C.O.V.N.E.T. A self-explained paranoid, delusional psychopath.
* Pat Hingle as General Luft, a military official who K. Edgar Singer reports to.
* Rob Schneider as Martin, the TV producer of UFO Mania. anchorwoman

===Muppet performers=== Bunsen Honeydew, Waldorf and Birdman
* Steve Whitmire as Kermit the Frog, Rizzo the Rat, Beaker, Bean Bunny, and Cosmic Fish #1
* Bill Barretta as Pepé the King Prawn, Bobo the Bear, Johnny Fiama, Bubba the Rat, and Cosmic Fish #2
* Jerry Nelson as Robin the Frog|Robin, Statler and Waldorf|Statler, Floyd Pepper, and Ubergonzo
* Brian Henson as Dr. Phil Van Neuter, Sal Minella, and the Talking Sandwich
* Kevin Clash as Clifford (Muppet)|Clifford, Carter
* Frank Oz as Miss Piggy, Fozzie Bear, Animal (Muppet)|Animal, and Sam Eagle Scooter
* John Henson as Sweetums
* John Kennedy as Dr. Teeth and the Electric Mayhem|Dr. Teeth
 John Kennedy, Peter Linz, and Drew Massey made cameos as beach hippies.

===Cameos===
* F. Murray Abraham as Noah
* David Arquette as Dr. Tucker, a sadistic scientist that works at C.O.V.N.E.T. and is in charge of the medical research that involves the rats.
* Josh Charles as Agent Baker, an operative of C.O.V.N.E.T.
* Kathy Griffin as Female Armed Guard, an operative of C.O.V.N.E.T. Hollywood Hogan as Man in Black/Himself, an operative of C.O.V.N.E.T.
* Ray Liotta as Gate Guard, the gate guard at C.O.V.N.E.T.
* Joshua Jackson as Pacey Witter, from Dawsons Creek
* Katie Holmes as Joey Porter, from Dawsons Creek

==Production== John Kennedy, and Rickey Boyd performed his characters. Their voices can still be heard in several scenes used in the films theatrical trailer, as well as the blooper reel.
 Scooter since Adam Hunt Richard Hunt).

===Writing===
An earlier draft of the story was written by Kirk Thatcher called "Muppets in Space." In the screenplay, aliens abducted Kermit because they believed him to be their leader, leading the other Muppets to attempt to save him. A set of Welchs Jelly Glasses were produced based around this theme. According to the production notes featured on the DVD, the film was inspired by Gonzos song in The Muppet Movie (1979), "Im Going to Go Back There Someday". 

===Music===
{{Infobox album   Name = Muppets from Space: Original Motion Picture Score Type = soundtrack Artist = Jamshied Sharifi Cover =  Released = August 10, 1999 Genre = Soundtrack
|Label = Sony Chronology  = The Muppets Last album =   (1996) This album = Muppets from Space: Original Motion Picture Score (1999) Next album Best of Muppets featuring The Muppets Wizard of Oz (2005)
}}
 Flash Light" George Clinton as a duet with Pepe the King Prawn named "Starlight". Two soundtracks were released featuring music from the film. One was an album containing the classic soul and funk tracks, while the other was an album containing the films score. The films score was composed by Jamshied Sharifi, and released by Varèse Sarabande.

Earlier drafts of the film had more original music, including the song "Eye 2 the Sky", written and recorded by Ween, which was not included on the soundtrack. This song was intended to be sung by Gonzo. Dave Goelz had also recorded a new rendition of "Im Going to Go Back There Someday" for this film, a song which had originally appeared in The Muppet Movie. This song was also dropped, but was included on the Muppets from Space soundtrack, also sung by Gonzo.

==Release== the Henson Walt Disney Inspector Gadget. 

===Box office===
Muppets from Space was considered a box office bomb theatrically, grossing only $22.3 million worldwide against its $24 million budget. 

===Critical reception===
Reviews for the film were mixed to positive, with a 63% "fresh" rating on Rotten Tomatoes based on 56 reviews (though this is lower than any of the previous Muppet films, all of which have reached at least 68% favorability on that site). The sites consensus stated that "if Muppets from Space lacks the magic and wit of its cinematic predecessors, this pleasingly silly space romp is funny and clever enough to make for better-than-average family entertainment."   

Roger Ebert of the Chicago Sun-Times gave the film a two star rating (out of four) and concluded his review by saying that "maybe Muppets from Space is just not very good, and theyll make a comeback. I hope so. Because I just dont seem to care much anymore."    On the other hand, Robin Rauzi of the Los Angeles Times gave the film a positive review stating that "twenty years after The Muppet Movie and 30 after the beginning of Sesame Street, there is still life in these creations of felt, foam rubber and fake fur. With care, they will easily entertain and educate a third or fourth generation of children. The magic is back."   

In a 2000 interview, Frank Oz described the film as not "up to what it should have been," and "not the movie that we wanted it to be." 

==References==
 

==External links==
*  
*  
*  
*   at Muppet Wiki
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 