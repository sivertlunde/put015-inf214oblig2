Man to Man (1930 film)
{{Infobox film name = Man to Man image = Man_to_Man_1930_Poster.jpg producer  = director = Allan Dwan writer = Joseph Jackson based on =   starring = Grant Mitchell Lucille Powers Otis Harlan Dwight Frye music = Erno Rapee Louis Silvers cinematography = Ira H. Morgan editing = George Marks distributor = Warner Bros. released =   runtime = 68 minutes language = English
|country = United States
}}
Man to Man is an Sound film|all-talking drama film produced by Warner Bros. in 1930. The film was directed by Allan Dwan and stars Phillips Holmes. The film is based on the story "Barber Johns Boy" by Ben Ames Williams. 

==Synopsis==
Phillips Holmes plays as the son of a barber, played by Grant Mitchell, who killed a man who had murdered his brother. Holmes, who is ashamed at being the son of a murderer, is working at a bank when his father is paroled. Although Mitchell is eager to establish a relationship with his son, Holmes wants nothing to do with his father. Feeling that people are judging him because of his father, Holmes decides to leave town and take his girl friend, played by Lucille Powers, with him. There is only one problem, Holmes needs to make some money quickly in order to marry Powers. Dwight Frye, who works at the same bank as Holmes, is also in love with Powers and figures out a way to prevent Holmes from taking her. Frye steals two thousand dollars from Holmes drawer so that he will be accused of stealing money. When Holmes realizes that two thousand dollars is missing from his drawer he assumes his father has stolen the money as he visited him at the bank earlier in the day. This leads Holmes to confess to stealing the money to prevent his dad from going to prison. At the same time, his father confesses to stealing the money to prevent Holmes from going to prison. Powers, who suspects that Fyre has stolen the money, tricks him into confessing his crime and father and son are happily reunited.  

==Cast==
*Phillips Holmes as Michael Bolton Grant Mitchell as Barber John Martin Bolton
*Lucille Powers as Emily
*Otis Harlan as Rip Henry
*Dwight Frye as Vint Glade Russell Simpson as Uncle Cal
*George F. Marion as Banker Jim McCord Paul Nicholson as Ryan
*Robert Emmett OConnor as the Sheriff

==Preservation== Warner Archive on DVD.

==External links==
*  

 

 
 
 
 
 
 
 
 
 