Stay Cool
{{Infobox film
| name         = Stay Cool
| image        = Staycoolnewposter.jpg
| caption      = Theatrical poster
| director     = Ted Smith Ken Johnson Mark Polish Mark Polish
| starring     = Winona Ryder   Mark Polish   Hilary Duff   Sean Astin   Josh Holloway   Jon Cryer   Chevy Chase
| distributor  = Initiate Productions
| released     =  
| first screening   = Cannes Film Festival 2010
| rating       = 
| Status       = 
| runtime      = 94 minutes
| country      = United States
| language     = English
| budget       = $4.5 million
}}
Stay Cool is a 2011  American comedy film directed by Ted Smith, and written by Mark Polish. The film stars Winona Ryder, Mark Polish, Hilary Duff, Sean Astin, Josh Holloway, Jon Cryer, and Chevy Chase.

==Cast and characters==
* Mark Polish as Henry McCarthy
* Winona Ryder as Scarlet Smith
* Hilary Duff as Shasta O Neil
* Josh Holloway as Wino 
* Max Thieriot as Luke 
* Sean Astin as Big Girl
* Chevy Chase as Principal Marshall
* Jon Cryer as Javier
* Marc Blucas as Brad Nelson
* Frances Conroy as Mrs. Looch

==Production==
Stay Cool was filmed between July and September 2008 in Santa Clarita, Saugus and Valencia (California). 

==Release==
A film festival version of Stay Cool was shown at the Tribeca Film Festival on April 23, 2009 and received favorable reviews.  It was presented under the category for "World Narrative Film Festival".  On the official Tribeca Film Festival it was described as a film that "...reminds us that time certainly does fly and old flames are hard to put out."  A version of the film was also premiered in May 2010 at the Marché du Film of Cannes (France).

It had a September 16, 2011 theatrical release in the United States. 

==Reception==
MTV ranked the film #5 on the "Top 10 Movies That Will Have You Screaming Oh My God!" calling it "the most adult-minded movie on this list". 

The film currently holds a 20% rating on the RottenTomatoes.com site. 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 