Beethoven's 3rd (film)
{{Multiple issues|
 
 
}}

{{Infobox film
| name           = Beethovens 3rd
| image          = BeethovensthirdDVD.jpg
| image size     =
| caption        = DVD cover
| director       = David Mickey Evans
| producer       = David Bixler Kelli Konop
| writer         = Jeff Schechter
| narrator       =
| starring       = Judge Reinhold Julia Sweeney Joe Pichler Jamie Marsh
| music          = Philip Giffin John Aronson
| editing        = Harry Keramidas Universal Family & Home Entertainment
| released       =   ( USA )     ( UK )
| runtime        = 99 minutes
| country        = United States English
| budget         =
}}
Beethovens 3rd is the second sequel to the 1992 film, Beethoven (film)|Beethoven. Additionally, this is the third installment in the Beethoven film series. It is the first film to be released directly to video. The film marks the onscreen introduction of Judge Reinhold as George Newtons younger brother Richard. Julia Sweeney as Richards wife Beth. Joe Pichler as Richards son Brennan. and Michaela Gallo as Richards daughter Sara.

== Plot ==
The movie takes place a few years after the events of the second film and the event at the mountains. The Newton family is on a cross country vacation in the USA to join a Newton family reunion in California, and in an unexplained move, Beethoven must travel there with Georges brother Richard (Judge Reinhold) and his family of a nagging wife Beth (Julia Sweeney) and two bratty kids Sara (Michaela Gallo) and Brennan (Joe Pichler) as they hit the road to California in a huge, shiny and expensive RV, equipped with a DVD player. Following them are two bumbling criminals who have hidden some secret codes on a DVD that they figure no one in the world will buy (The Shakiest Gun in the West), but someone does: Richard. So now theyve got a DVD holding top secret information and the criminals must get it back. They are not only in trouble with the criminals but with Beethoven also. But after he saves Sara from the criminals the family becomes very sad knowing they have to give him back. After they get to California they find out from Richards Uncle Morrie that George, Alice, and the kids will not be coming because of business reasons in Botswana which means they need to extend their visit for another year and will have to watch Beethoven longer which made everyone happy.

== Cast ==
{|class="wikitable"
 ! Actor/Actress || Character
|-
| Judge Reinhold || Richard Newton
|-
| Julia Sweeney ||  Beth Newton
|-
| Joe Pichler ||  Brennan Newton
|-
| Michaela Gallo ||  Sara Newton
|-
| Mike Ciccolini ||  Tommy
|-
| Jamie Marsh || Bill (William)
|-
| Danielle Keaton (as Danielle Weiner)||  Penny
|-
| Frank Gorshin ||  Morrie Newton
|-
| Holly Mitchell ||  Kennel Employee
|}

== References ==
 

== External links ==

* 

 
 

 
 
 
 
 
 
 
 
 