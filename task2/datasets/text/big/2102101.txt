Mary Reilly (film)
{{Infobox film
| name =Mary Reilly 
| image =Mary_Reilly.jpg
| border = yes
| caption =Theatrical Release Poster
| director = Stephen Frears		
| producer = 	Norma Heyman Ned Tanen Nancy Graham Tanen
| writer = Christopher Hampton 
| based on =    George Cole Michael Gambon Glenn Close
| music = George Fenton
| cinematography = Philippe Rousselot
| editing = Lesley Walker
| distributor = TriStar Pictures 
| released =  
| runtime = 108 minutes
| country = United States
| language = English 
| budget = $47 million (estimated)    
| gross = $12,379,402  
}} Mary Reilly by Valerie Martin (itself inspired by Robert Louis Stevensons Strange Case of Dr Jekyll and Mr Hyde). This was the re-teaming of director Frears, screenwriter Hampton, and actors Malkovich and Glenn Close, all of whom were involved in the Oscar-winning Dangerous Liaisons (1988).

==Plot==
Mary Reilly is a servant in the home of Dr. Henry Jekyll.  A friendship between Mary and the doctor develops, which becomes an attraction. The doctor takes an interest in Mary, confiding in her and she in him; specifically she tells him of abuse she suffered at her fathers hands. After Mary and the Doctor spend a considerable time talking one morning as she brings him his breakfast; the head of the household staff, Mr Poole challenges Marys story that the two of them were discussing the planting a garden, thinking that she lied. Upon being discreetly questioned by Poole, the doctor deftly covers for Mary and says that the garden they talked about is "the very thing" needed in the house. However, the household is thrown into turmoil when the master announces he will be getting an assistant. The staff speculate on his employment and origins, as he is never fully seen and remains a mystery.
 blood money. She then hides in the lab because the exit door is locked. She is terrified as Hyde discovers her hiding place, but he merely throws her a key. The next morning, she finds Jekyll in the yard with a sprained ankle.

The following morning, Jekyll wakes her with another letter for Mrs. Farraday. When she arrives at the brothel, the madam is furious. She shows Mary the room that has been let to Hyde, which is covered in blood. Mary returns to the house with a blood stained handkerchief of Jekylls, as well as a message from Farraday promising to do what is necessary to conceal the bloody event. Mary finally meets the enigmatic Edward Hyde (Who has a strong resemblance to Jekyll) and finds herself drawn towards his passionate nature. However, she is also upset when he reveals that he knows intimate details about her conversations with the Doctor. In turn, Mary, challenges the Doctor about this and the doctor claims that while he made notes about their conversations out of habit. He asserts that Hyde looked at the Doctors notes without his knowledge. The next day, when delivering Jekylls breakfast, he asks her to accompany Hyde on an errand. They visit the slaughterhouse yard to collect organs for the doctors research. Upon their return, Hyde torments Mary, asking if she is aware of how much Jekyll longs to touch her. 

While fetching tea for Hyde, she answers the door to find Mrs. Farraday, who insists on seeing Jekyll. Jekyll is not pleased to see Farraday, who demands more money for her continued silence. Mary leaves them alone, but while watering the garden, she notices the lights in the lab go out. Investigating, she discovers a small pool of blood on the theater table. She does not see Hyde, who is hidden and has killed Mrs Farraday.

Mary receives a letter informing her that her mother has died. Intending to give her mother a proper burial, she goes out into the fog to find a funeral parlor. She is grabbed in an alley by Hyde, who is being chased by mounted police. He hides behind her as they rush by. He thanks her for being in the right place at the right time, and kisses her before leaving.  When she returns to the house, she questioned by the police, in connection to the murder of Jekylls school friend, Sir Danvers Carew, a Member of Parliament. Asked if she has seen Hyde in the last 24 hours, Mary lies that she has not. Jekyll later tells her she should not have risked so much by lying for Hyde. Danvers may have been "corrupt and frivolous," the doctor says, but he never deserved to be murdered. Jekyll has dismissed Hyde and given him money to disappear.

When delivering breakfast, she is surprised to discover Hyde in the doctors bed. She tries to raise the alarm, but he stops her. Hyde then reveals to her his true nature. He explains that the doctor injects himself with a serum; a cure for his depression, and that Hyde is the resulting cure. He in turn injects the antidote to resume being Jekyll. He said that he now has the ability to appear without the aid of Jekylls serum.  Hyde then tries to persuade her to have sex with him. Shocked, Mary wishes to leave. He releases her, and she joins her colleagues in the kitchen. They are interrupted by Jekyll. who orders Poole to take a sample of a potion to the chemists and ask them to analyze it. He is to wait until they are successful, as this is a matter of life and death. Unfortunately, Poole returns unsuccessfully.

Mary packs her things to leave, but on her way out, she decides to visit the lab. Hyde attacks her, smashing bottles all around. He holds a knife to her throat, but does not kill her. He says that he always knew that Mary "would be the death of us." He then injects himself with the antidote, and Mary is forced to witness the horrific transformation between one man to the other. Jekyll reveals that Hyde has mixed a poison with the antidote. He then dies in Marys arms. In the morning, Jekyll, although dead, has transformed into Hyde one last time, awake and smiling.

==Cast==
*Julia Roberts: Mary Reilly
*John Malkovich: Dr. Henry Jekyll / Mr. Edward Hyde
*Michael Sheen: Bradshaw George Cole: Mr. Poole
*Kathy Staff: Mrs. Kent
*Bronagh Gallagher: Annie
*Glenn Close: Mrs. Farraday
*Michael Gambon: Mr. Reilly
*Ciarán Hinds: Sir Danvers Carew

==Production== Mary Reilly Ed Wood,  but Burton dropped out in May 1993 over his anger against Guber for putting Ed Wood in turnaround. Stephen Frears was TriStars first choice to replace Burton, and Di Novi was fired and replaced with Ned Tanen. Daniel Day-Lewis was TriStars first choice for the role of Dr. Jekyll and Uma Thurman for the role of Mary.   

==Critical and commercial reception==
 
Reports of alleged production delays and animosity between the two leads helped fuel the poor word-of-mouth preceding the films release. Upon release, the reviews were decidedly negative, with few critics finding anything to praise about the production. Many found fault with Roberts, calling her miscast (though Malkovich, too, received his fair share of ill mention). The film did not do well at the box office. It earned a paltry $5.6 million domestically on a budget of $47 million and grossed only $12.3 million worldwide.  Mary Reilly currently holds a 26% rating on Rotten Tomatoes based on 42 reviews with the consensus stating: "Mary Reilly looks good and has its moments but overall, the movie borders on boredom."

Roberts was nominated for Worst Actress by the Razzie Awards, with Stephen Frears also being nominated for Worst Director, but lost to Striptease (film)|Striptease.  The film was also entered into the 46th Berlin International Film Festival.   

==See also==
*Strange Case of Dr Jekyll and Mr Hyde

==References==
 

==External links==
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 