Bauerntanz zweier Kinder
{{Infobox film
| name           = Bauerntanz zweier Kinder
| image          = 
| image_size     = 
| caption        = 
| director       = Max Skladanowsky
| producer       = 
| writer         = 
| narrator       = 
| starring       = 
| music          = 
| cinematography = Max Skladanowsky
| editing        = 
| distributor    = 
| released       =    
| runtime        = 
| country        = German Empire Silent
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 1895 Germany|German short black-and-white silent documentary film directed by Max Skladanowsky. The film captures two children from Ploetz-Lorello, performing a dance. 

It was one of a series of films produced to be projected by a  , Serpentinen Tanz, Der Jongleur Paul Petras, Das Boxende Känguruh, Akrobatisches Potpourri, Kamarinskaja, Ringkampf and Apotheose. Each film lasted approximately 6 seconds and would be repeated several times. 

==References==
 

== External links ==
*  

 
 
 
 
 
 
 
 


 
 