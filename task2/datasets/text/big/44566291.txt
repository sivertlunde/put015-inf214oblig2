Kenner (film)
{{Infobox film
| name           = Kenner
| image          = Kenner poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Steve Sekely
| producer       = Mary P. Murray 
| screenplay     = Robert L. Richards Harold Clemins 
| story          = Mary P. Murray 
| starring       = Jim Brown Madlyn Rhue Robert Coote Ricky Cordell Charles Horvath Prem Nath
| music          = Prem Dhawan Piero Piccioni
| cinematography = Dieter Liphardt 
| editing        = Richard V. Heermance 	
| studio         = M & M Productions
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 92 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Kenner is a 1969 American drama film directed by Steve Sekely and written by Robert L. Richards and Harold Clemins. The film stars Jim Brown, Madlyn Rhue, Robert Coote, Ricky Cordell, Charles Horvath and Prem Nath. The film was released on April 23, 1969, by Metro-Goldwyn-Mayer.  

==Plot==
 

== Cast ==
*Jim Brown as Roy Kenner
*Madlyn Rhue as Anasuya
*Robert Coote as Henderson
*Ricky Cordell as Saji
*Charles Horvath as Tom Jordan
*Prem Nath as Sandy
*Kuljit Singh as Young Sikh
*Sulochana Latkar as Mother Superior 
*Ursula Prince as Sister Katherine
*Tony North as American Friend
*Ming Hung as Ring Referee
*R.P. Wright as Gym Owner
*Nitin Sethi as Customs Officer
*Mahendra Jhaveri as Young Hindu
*G.S. Aasie as Shoe Merchant
*Ravikant as Bald Disciple
*Hercules as Robed Man
*Khalil Amir as Robed Man 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 