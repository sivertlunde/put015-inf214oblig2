Mammy (1930 film)
{{Infobox film name           = Mammy image          = Mammy1930.jpg producer       = Walter Morosco director       = Michael Curtiz  writer  Joseph Jackson Gordon Rigby starring       = Al Jolson Lois Moran Lowell Sherman Noah Beery music          = Irving Berlin cinematography = Barney McGill editing        = Owen Marks distributor    = Warner Bros. released       =   budget         =  country        = United States runtime        = 95 minutes language       = English
}}

Mammy (1930 in film|1930) is an Talkie|All-Talking musical drama film with Technicolor sequences, released by Warner Bros. The film starred Al Jolson and was a follow-up to his previous film, Say It With Songs (1929). Mammy became Al Jolsons fourth feature, following earlier screen efforts as The Jazz Singer (1927), The Singing Fool (1928) and Say It With Songs (1929). The movie relives Jolsons early years as a minstrel man. The songs were written by Irving Berlin, who is also credited with the original story titled Mr. Bones.

==Synopsis== minstrel troupe take a freight train out of town. Eventually, Lewis confesses to the crime and Jolson is thereby proven to be innocent.

==Cast==
*Al Jolson as Al Fuller
*Lois Moran as Nora Meadows
*Lowell Sherman as Billy West / Westy
*Louise Dresser as Mother Fuller
*Hobart Bosworth as Meadows
*Tully Marshall as Slats
*Mitchell Lewis as Hank Smith / Tambo Jack Curtis as Sheriff Tremble

==Songs==
*"Let Me Sing and Im Happy" 
*"Here We Are"
*"Who Paid the Rent for Mrs. Rip Van Winkle?" 
*"The Knights of the Road" (missing on surviving prints)
*"The Call of the South" (missing on surviving prints)
*"Yes, We Have No Bananas" 
*"Miserere" 
*"Across The Breakfast Table, Looking At You" 
*"In the Morning" 
*"Night Boat to Albany"  Pretty Baby"
*"When You and I Were Young, Maggie"
*"Mammy (song)|Mammy"

==Preservation==
The original Technicolor sequences were found in a Dutch print (a copy of the International Sound Version) which had Dutch titles inserted in several places. This print was restored by the UCLA Film and Television Archive, and released on DVD from the Warner Archive Collection, along with its Overture and Exit Music. Unfortunately, sections of those Technicolor sequences were lost when Dutch titles were inserted, and some of the cuts from color to sepia tinted black and white are not smooth. Additionally, two songs are missing from all existing prints that were in the original release: "The Call of the South" and "Knights of the Road". They were written by Irving Berlin and sung by Al Jolson.

==See also==
*List of early color feature films

==References==
;Notes
 
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 