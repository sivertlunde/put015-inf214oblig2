Now and Forever (1934 film)
{{Infobox film
| name           = Now and Forever
| image          = Now and Forever 1934 Poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Henry Hathaway
| producer       = Louis D. Lighton
| screenplay     = {{Plainlist|
* Vincent Lawrence
* Sylvia Thalberg
}}
| story          = {{Plainlist|
* Jack Kirkland
* Melville Baker
}}
| starring       = {{Plainlist|
* Gary Cooper
* Carole Lombard
* Shirley Temple
}}
| music          = {{Plainlist|
* Harry Revel
* Mack Gordon
}}
| cinematography = Harry Fischbeck
| editing        = Ellsworth Hoagland
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 83 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Now and Forever is a 1934 American drama film directed by Henry Hathaway.  The screenplay by Vincent Lawrence and Sylvia Thalberg was based on a story by Jack Kirkland and Melville Baker. The film stars Gary Cooper, Carole Lombard, and Shirley Temple in a story about a criminal going straight for his childs sake. Temple sang "The World Owes Me a Living".  The film was critically well received.  Temple adored Cooper who nicknamed her Wigglebritches (Windeler 140). This is the only film in which Lombard and Temple appeared together.

==Cast== Gary Cooper as Jerry Day
*Carole Lombard as Toni Carstairs
*Shirley Temple as Penelope Day
*Sir Guy Standing as Felix Evans
*Charlotte Granville as Mrs. Crane
*Gilbert Emery as James Higginson
*Henry Kolker as Mr. Clark
*Tetsu Komai as Mr. Ling

==Production==

Temple was loaned out to Paramount by Fox Films for $3,500 a week in what would be her second movie at Paramount. It would also be the first movie in which a stand-in (Marilyn Granas) was hired for Temple. Temple had a good rapport with the adult crew, especially Gary Cooper, who bought her several toys and made a number of sketches for her. During the making of the movie, Dorothy Dell, who costarred with Temple in Little Miss Marker and developed a close personal friendship with her, died in an automobile accident. Temple was not told about this until filming was started on the crying scene in the movie in which her character finds out her father was lying to her about stealing the jewelry. The tears she was crying in that scene were in effect real tears. 

==Reception==
The film was popular at the box office. 

The New York Times thought the film "a sentimental melodrama" and "a pleasant enough entertainment." Temple was highly praised for her performance. 

Temple sang "The World Owes Me a Living",  a version of which also featured in a Silly Symphonies animation of The Ant and the Grasshopper  in the same year. Louella Parsons was amazed "at the ease with which   reels off her lines, saying big words and expressions.  There is nothing parrot-like about Shirley. She knows what she is talking about."   Temple-fever spread with the release of the film.  Her fan mail (which numbered 400&ndash;500 letters a day) was delivered in huge mail sacks to the studio and a secretary was hired to manage it (Edwards 66).

==See also==
* Gary Cooper filmography
* Shirley Temple filmography
* Carole Lombard filmography

==References==
;Notes
 
;Bibliography
 
*  
*  
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 