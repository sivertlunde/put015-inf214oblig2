The White Balloon
{{Infobox film 
 | name = The White Balloon
 | image = Whiteballoon82.jpg
 | caption =
 | director = Jafar Panahi
 | producer = Kurosh Mazkouri
 | writer = Abbas Kiarostami
 | starring = Aida Mohammadkhani  Mohsen Kafili Fereshteh Sadr Orfani
 | music =
 | cinematography = Farzad Jadat
 | editing = Jafar Panahi
 | distributor = October Films
 | released =  
 | runtime = 85 minutes
 | country =Iran
 | language =Persian
 | budget =
 }}

 The White Balloon ( , Badkonake sefid) is a 1995 Iranian film directed by Jafar Panahi, with a screenplay by Abbas Kiarostami. It was Panahis feature-film debut as director. The film received many strong critical reviews and won numerous awards in the international film fairs around the world including the Prix de la Camera dOr at the 1995 Cannes Film Festival. The Guardian has listed this movie as one of the 50 best family films of all time. 

==Plot==
 Iranian New Year. The film opens in a Teheran market where seven-year-old Razieh (Aida Mohammadkhani) and her mother are shopping. Razieh sees a goldfish in a shop and begins to nag her hurrying mother to buy it for the festivities instead of the skinny ones in her familys pond at home.  Almost all of the films major characters are briefly seen in this market scene, though they wont be introduced to the viewer until later. On their way home, mother and daughter pass a courtyard where a crowd of men has gathered to watch two snake charmers. Razieh wants to see what is happening but her mother pulls her daughter away, telling her that it is not good for her to watch these things.

Back home, Razieh is upset about her mothers refusal to let her buy a new goldfish, but continues  her campaign of nagging. Her older brother Ali (Mohsen Kalifi) returns from a shopping errand for their father who, although remains unseen has a tangible presence causing tension in the family. He complains that he asked Ali to buy shampoo, not soap, then throws the soap at him. Ali sets off to buy the shampoo and when he returns Razieh enlists his help in changing her mothers mind about the goldfish, bribing him with a balloon. Insisting that she can buy the goldfish in the market for 100 tomans ("Youre crazy," Ali tells her, remarking that he can see two films with that money), Razieh finally gets her wish. Her mother gives her the familys last 500-toman banknote and asks her to bring back the change. Razieh sets off with an empty glass jar to the fish shop a few blocks away.

Between their home and the fish store, Razieh manages to lose the money twice, first in an encounter with the snake charmer, and then when she drops the money through the grate at the entrance to a store which has been closed for the New Year celebration.

Razieh and Ali make several attempts to retrieve the money and receive assistance from many people, including the owners of nearby shops and an Iranian soldier. The money, however, is always just out of reach. Finally, the siblings receive help from a young Afghan street vendor selling balloons. He carries all of his balloons on a wooden stick, and has only one balloon, a white one, left. The group attaches a piece of gum to one end of the balloon stick, and with it, they reach down through the grate and pull the money out.

The film ends, not with Ali and Razieh, but with the young Afghan boy, who has become an important character only at the very end of the film.

==Accolades==
*Prix de la Camera dOr, 1995 Cannes Film Festival
*Gold Award, Tokyo International Film Festival, 1995
*Best International Film, Sudbury Cinéfest, 1995
*International Jury Award, São Paulo International Film Festival, 1995

==Notes==
 

==External links==
* 
*  

 
 

 
 
 
 
 
 
 
 