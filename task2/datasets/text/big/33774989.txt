Cold Heaven (film)
{{Infobox film name = Cold Heaven image = ColdHeaven1991.jpg caption =
|director= Nicolas Roeg Allan Scott based on Cold Heaven Brian Moore released = 13 September 1991 (Toronto International Film Festival, Canada); 29 May 1992 (USA) country = United States language = English
|music = Stanley Myers producer = Allan Scott Eric Parkinson cinematography = Francis Kenny editing = Tony Lawson
}} Allan Scott, novel of Northern Irish-Canadian Brian Moore, which was published in 1983.

==Plot==
The plot concerns a lapsed Catholic, Marie Davenport (played by Theresa Russell), who is about to leave her husband Alex (played by Mark Harmon) for her lover, Daniel (played by James Russo), when Alex is apparently killed in a boating accident and then seems to have risen from the dead. The film deals with Maries dilemma in confronting this apparent miracle.

==Cast==
* Theresa Russell as Marie Davenport 
* Mark Harmon as Dr. Alex Davenport
* James Russo as Daniel Corvin
* Julie Carmen as Anna Corvin 
* Seymour Cassel as Tom Farrelly
* Diana Douglas as Mother St. Agnes 
* Talia Shire as Nun
* Will Patton as Priest
* Cástulo Guerra as Dr. DeMencos

==Critical review== Time Out said that "Sadly, for all its technical brilliance and narrative assurance, the films climactic scenes require an act of faith that no film-maker – Christian, agnostic or atheist – has any right to ask".   

Neil Sinyard in Reference Guide to British and Irish Film Directors described it as "disappointing... unusually dour and dry in its treatment of guilt and paranoia".   

==References==
 

==External links==
*  
*  
* 
 
 
 

 
 
 
 
 
 


 