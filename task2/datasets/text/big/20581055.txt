Door Ka Raahi
{{Infobox film
| name           = Door Ka Raahi
| image          = 
| image_size     = 
| caption        = 
| director       = Kishore Kumar
| producer       = 
| writer         = Kishore Kumar
| narrator       = 
| starring       =Tanuja, Kishore Kumar and Ashok Kumar
| music          = Kishore Kumar
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1971
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

Door Ka Raahi is a 1971 Bollywood drama film directed by Kishore Kumar. The film stars Tanuja, Kishore Kumar and Ashok Kumar. Film is a finest art of Kishore Kumar and his vision of iternal world.
Film strives to deliver very strong message of humanity in simplest language. Door Ka Rahi is story of a person named "Prashant" who is on the travel for the wellness of society, and the end of Journey is to watch....., the angle of the movie does not seems to be commercial rather focused on expressing the internal feeling.

==Cast==
*Kishore Kumar
*Tanuja
*Ashok Kumar
*Padma Khanna
*Amit Kumar
*Abhi Bhattacharya Asit Sen
*Hiralal Ganga

==Songs==
{| border="2" cellpadding="5" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track # !! Song !! Singer(s)
|-
| 1
| Beqarar Dil Gaaye Jaa
| Kishore Kumar, Sulakshana Pandit
|-
| 2
| Khushi Do Ghadi Ki
| Kishore Kumar
|-
| 3
| Jeevan Se Na Haar
| Kishore Kumar
|-
| 4
| Chalti Chale Jaaye Zindagi
| Hemant Kumar
|-
| 5
| Panthi Hoon Main Us Path Ka
| Kishore Kumar
|-
| 6
| Main Ek Panchhi Matwala Re Amit Ganguly
|- 
| 7
| Mujhe Kho Jane Do
| Kishore Kumar
|-
| 8
| Ek Din Aur Aa Gaya
| Manna Dey
|}

==External links==
*  

 
 
 
 


 
 