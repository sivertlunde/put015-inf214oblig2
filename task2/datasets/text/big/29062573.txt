The Fall of Berlin (film)
 
{{Infobox film
| name           = The Fall of Berlin
| image          = The Fall of Berlin poster.jpg
| alt            = 
| caption        = A 1950 poster of the film.
| director       = Mikheil Chiaureli
| producer       = Viktor Tsirgiladze 
| writers        = Pyotr Pavlenko
| screenplay = Pyotr Pavlenko Mikheil Chiaureli
| narrator       =  Boris Andreyev Marina Kovaliova
| music          = Dmitri Shostakovich 
| cinematography = Leonid Kosmatov
| editing        = Tatiana Likhacheva 
| studio         = Mosfilm
| distributor    = Amkino (US)
| released       =  
| runtime        = 167 minutes (original)  151 minutes (post-1953 version)
| country        = Soviet Union
| language       = Russian
| budget         = 
| gross          = 
}} Soviet film in two parts directed by Mikheil Chiaureli, released in 1950 by the Mosfilm Studio. The script was written by Pyotr Pavlenko, and the musical score composed by Dmitri Shostakovich. It starred Mikheil Gelovani as Joseph Stalin.

Portraying the history of the Second World War with a focus on a highly positive depiction of the role the Soviet leader played in the events, it is considered one of the most important representations of Stalins cult of personality.

==Plot==
 
===Part 1=== greatly surpasses invade the Soviet Union.
 Soviet slaves-laborers, attack Stalingrad. In the meanwhile, Göring negotiates with British capitalist Bedstone, who supplies Germany with needed materials. After the Soviet victory in Stalingrad, Vasily Chuikov tells Ivanov that Stalin is always with the Red Army. The storyline leaps to the Yalta Conference, where Stalin and his Western Allies debate the future of the war. The treacherous Winston Churchill intends to deny the Soviets access to Berlin and almost manages to convince the gullible Franklin Delano Roosevelt to accept his plans.

===Part 2=== Hans Krebs Reichstag and the three hoist the banner atop of it. The Germans surrender and Red Army soldiers from throughout the USSR celebrate victory. Stalins plane lands in Berlin, and he is greeted by an enthusiastic crowd of soldiers and liberated slave-laborers, holding posters with his picture and waving red flags. Stalin carries a speech in which he calls for world peace. Standing in the crowd, Alexei and Natasha recognize each other and are reunited. Natasha asks Stalin to let her kiss him on the cheek, and they hug while prisoners praise Stalin in numerous languages. The film ends with Stalin wishing all peace and happiness.

==Cast==
 
*Mikheil Gelovani as Joseph Stalin. Boris Andreyev as Alexei Ivanov.
*Marina Kovaliova as Natasha Rumyantseva.
*Alexey Gribov as Kliment Voroshilov. Nikolay Bogolyubov as Khmelnitsky.
*Tamara Nosova as Katia.
*Ruben Simonov as Anastas Mikoyan.
*Boris Livanov as Marshal Konstantin Rokossovsky.
*Andrei Abrikosov as General Aleksei Antonov.
*Jan Werich as Hermann Göring.
*Veriko Anjaparidze as Hans mother.
*Georgy Tatishvili as Meliton Kantaria.
*Dmitry Dubov as Mikhail Yegorov.
*Fedor Blasevich as Marshal Georgy Zhukov.
*Nikolai Ryzhov as Lazar Kaganovich.
*Gavriil Belov as Mikhail Kalinin.
*Maxim Strauch as Vyacheslav Molotov.
*Konstantin Bartashevich as General Vasily Sokolovsky.
*Sergei Blinnikov as Marshal Ivan Konev. Alexander Vasilevsky.
 
*Boris Tenin as General Vasily Chuikov.
*Victor Stanitsyn as Winston Churchill.
*Oleg Frohlich as Franklin Delano Roosevelt.
*Leonid Pirogov as James F. Byrnes.
*Vladimir Savelyev as Adolf Hitler. Hans Krebs.
*Nikolai Plotnikov as Field Marshal Walther von Brauchitsch.
*Vladimir Pokrovsky as General Alfred Jodl. Vasily Kuznetsov.
*Michael Sidorkin as General Sergei Shtemenko.
*Maria Novakova as Eva Braun.
*N. Petrunkin as Joseph Goebbels.
*V. Renin as Field Marshal Gerd von Rundstedt.
*K. Gomola as Heinz Linge.
*A. Urazaliev as Yusuf.
*K. Roden as Charles Bedstone.
*Ivan Solovyov as Johnson
*Yuri Tymoshenko as Zaichenko.
*Yevgeniya Melnikova as Director.
*Dmitry Pavlov as Tomashevich.
*Sofia Giatzintova as Alexeis mother.
*Willi Narloch as SS officer captured by Alexei.
 

==Production==

===Background===
Stalins cult of personality, which began to manifest itself already in the late 1930s, was marginalized during World War II; to mobilize the population against the enemy, Soviet films focused on historical heroes who defended Russia or on the feats of the people themselves.  The premiers character appeared in only two pictures during the war.    However, as victory seemed secure, Stalin tightened his control over every aspect of the Soviet society, including cinema. After 1945, his cult returned to the screen with greater intensity than ever before, and he was credited as the sole architect of Germanys defeat. Denise J. Youngblood wrote that shortly afterwards, there remained only three kinds of war heroes: "the dead, the maimed and Stalin."   

===Inception=== The Vow. eponymous series of Eastern Front campaigns. The project was only partially fulfilled until Stalins death. 

===Development===
As with all films in which his character made an appearance, Stalin took a keen interest in the work on The Fall of Berlin.    The premier intervened in Pavlenkos writing, read the screenplays manuscript and corrected several grammatical mistakes; he also deleted a short sequence during which a German civilian in Berlin exhorted his family to hasten and flee as the Red Army approaches. German historian Lars Karl believed this signaled his resolution to demonstrate that the civilian populace had nothing to fear from the Soviets.  The picture was the first feature film about the Battle of Berlin and the events in Hitlers bunker, preceding Der letzte Akt by five years.   
 Odessa Steps from The Battleship Potemkin, a gesture intended to mock Sergei Eisenstein. 

According to the memories of Svetlana Aliluyeva, Chiaureli approached her father with the idea to combine the fate of his son, Yakov Dzhugashvili, in the plot. Stalin promptly rejected this.    Soviet actor Artyom Karapetian claimed Chiaurelis wife, actress Veriko Anjaparidze, told him Stalin was so outraged when he heard of this that Lavrentiy Beria – who was standing nearby –  reached into his trousers pocket, "presumably, for his gun."    The directors daughter, Sofiko Chiaureli, recounted that her father "knew he was saved" when Stalin wiped tears from his eyes as he watched Gelovani descend from the plane and muttered "If only I have went to Berlin."   

===Principal photography===
Chiaureli brought some 10,000 Soviet extras to Berlin for the filming, and also hired many local residents for the tunnel flooding sequence; He wasnt able to work in the Reichstag – it was located in the British Zone of West Berlin – and conducted the photography mainly in the Babelsberg Studios.    However, most of the episodes set in the German capital were filmed in ruined cities in the Baltic region.    In addition, a scale model of Berlin, over one square kilometer in size,    was built in Mosfilms studios; this miniature was to create the urban combat scenes in the end of Part II.  

The Soviet Army provided five divisions, their supporting artillery formations, four tank battalions, 193 military aircraft and 45 captured German tanks to recreate the open field battles portrayed in The Fall of Berlin. They consumed 1.5 million liters of fuel during the filming.   

The Fall of Berlin was one of the first colored films made in the Soviet Union. The producers used Agfacolor reels, taken from Universum Film AG|UFAs studios in Potsdam-Babelsberg|Neubabelsberg. 

===Music=== Formalism during Wedding March, heard during the scene in which Hitler marries Eva Braun; the march was banned in Nazi Germany. According to Riley, it is unclear whether Chiaureli intended to mock the Nazis by portraying them as unable to recognize an item they have forbidden, or he has simply been ignorant of the ban.   

Soundtrack for Part I

1. Main Title Part 1 (2:44)

2. Beautiful Day (2:14)  ; lyrics by Yevgeniy Dolmatovsky.]

3. Alyosha By the River	(1:39)

4. Stalinss Garden (2:04)

5. Alyosha and Natalia in the Fields / Attack (3:55)	

6. Hitlers Reception (1:31)

7. In the Devastated Village (2:39)		

8. Forward! (0:58)

Soundtrack for Part II

1. Main Title Part 2 (2:06)

2. The Roll Call / Attack at Night (3:02)

3. Storming the Seelov Heights (6:26)

4. The Flooding of the Underground Station (1:11)

5. The Final Battle for the Reichstag / Kostyas Death	(4:06)		

6. Yussufs Death / The Red Banner (3:41)

7. Stalin at Berlin Airport (4:28)

8. Finale / Stalins Speech / Alyosha and Natasha Reunited (2:43)  ; lyrics by Yevgeniy Dolmatovsky.]

==Reception==

===Contemporary response=== Boris Andreyev and Vladimir Kenigson were all awarded the Stalin Prize, 1st Class for their work.    In Czechoslovakia, The film also won the Crystal Globe in the 5th Karlovy Vary International Film Festival.   

On the day of the films release,   in which he described the film as "wonderful... a truthful portrayal of the relations between the people and the leader... and the love of all people to Stalin."   A day after,   praised it as an authentic representation of history. 

The publics reaction to the film was monitored by the government: in a memorandum to  .   
 Barracked Peoples Police were obliged to watch it. However, The Fall of Berlin was received with little enthusiasm by the populace. Years later, in an article he wrote for the Deutsche Filmkunst magazine on 30 October 1959, Sigfried Silbermann – director of the state film distributor Progress-Film Verleih – attributed this response to the effect years of anti-Soviet propaganda had on the German people. 

French critic  ... Some aesthetics today advocate American film noirs, but in the future only specialists will be interested in these museums of horror, the remains of a dead epoch... While the majority of people will applaud The Fall of Berlin." Sadoul, Georges. La Geste Grandiose et Inoubliable de Staline. Les Lettres Françaises, 25 May 1950. Quoted in:    In France, it sold 815,116 tickets. 
 Members of Parliament in Westminster and for the Prime Minister in his Chartwell residence. Churchill wrote to historian Hugh Trevor-Roper in May, asking about the veracity of the Berlin underground flooding by Hitler, and the latter replied it was "mythologizing" history. After the Foreign Office concluded the picture was "too one-sided to serve as an effective communist propaganda", it was released without cuts, with a long disclaimer that stressed "the advantages of living in the free British society" and that the Soviet screenwriters completely ignored the Western Allies role in the war. The film became the most successful Soviet or foreign picture about World War II screened in the UK during the 1940s and 1950s. Tony Shaw noted The Fall of Berlin enjoyed mostly positive reviews during its six-week run in London and its subsequent showings in the country, though some also commented it was overblown propaganda; the critics of The Sunday Times and the Evening Standard both opined that the Soviets obliteration of the Anglo-American contribution to victory was akin to the same treatment received by the Red Army in Western productions about the war.   

The film was one of the few foreign-language pictures to be presented in the BBCs program Current Release; former war correspondent Matthew Halton was invited to comment on it. The American magazine Variety (magazine)|Variety described it as "The Russian answer to the many American and British films about the war... having some contemporary significance, in the light of the tensions between the West and the Soviet Union."    The New York Times critic dubbed it as a "deafening blend of historical pageantry and wishful thinking... directed as if his (Chiaurelis) life depended upon it" and – in what author David Caute claimed was the worse condemnation which could be leveled at it in the day  – that it had a "Hollywood-style plot". He also disapproved of the historical veracity of the Yalta Conference scene,  while John Howard Lawson, recently released from prison, praised it as an authentic depiction of events.  Officials in Nicola Napoli|Artkino, the pictures American distributor, claimed the film was "already witnessed by 1.2 million people" in the United States by 9 June 1952, a day after its release there. {{cite web |url=http://movies.nytimes.com/movie/review?res=9A0DE4DD143AE23BBC4153DFB0668389649EDE |title=Padeniye Berlina (1950)
|author= H.H.T.|date=9 June 1952|work=nytimes.com |accessdate=30 April 2011}} 

===De-Stalinization===
Stalins death in March 1953 signaled a sharp turn in the politics of the Eastern Bloc. After Beria was arrested, Chiaureli was instructed by the new rulers to leave Moscow.    The Fall of Berlin was withdrawn from circulation. A directive of the Soviet Film Export Bureau to halt its screening reached East Germany in July.  During the summer of 1953, the scene featuring Alexei Ivanov dining with Stalin and the other Soviet leaders in Moscow was edited out from all available copies; author Richard Taylor attributed this to the appearance Berias character had there.  In the post-1953 version, Ivanov is introduced to Stalin, and is then shown strolling with Natasha in the wheat field.
 speech condemning Stalins cult of personality in front of the 20th Congress of the Communist Party of the Soviet Union. In the midst of it, he told the audience:

"Let us recall the film, The Fall of Berlin. Here only Stalin acts. He issues orders in a hall in which there are many empty chairs. Only one man approaches him to report something to him – it is Alexander Poskrebyshev|Poskrebyshev... And where is the military command? Where is the politburo? Where is the government? What are they doing, and with what are they engaged? There is nothing about them in the film. Stalin acts for everybody, he does not reckon with anyone. He asks no one for advice. Everything is shown to the people in this false light. Why? To surround Stalin with glory – contrary to the facts and contrary to historical truth."   
 opposed Khrushchevs Georgian demonstrations included a request to hold showings of the film in their list of demands.   

===Critical analysis===
Historian Nikolas Hüllbusch viewed The Fall of Berlin as a representation of Stalins strengthening political power. He compared it to the first fiction film to feature the premier, the 1937 Lenin in 1918, which depicted Stalin as Vladimir Lenins most devout disciple; and to The Vow, in which he is chosen as Lenins heir and takes an oath to keep his legacy. In The Fall of Berlin, Lenin has no impact on the plot. Instead of being the state founders successor, Stalins legitimacy was now based on his leadership of the USSR during the war.   

Denise Youngblood wrote that although not the first to portray Stalin as "war hero in chief" – this was already done in pictures like  , who was portrayed in accordance with his political status in the late 1940s, after he was shunned by Stalin: Zhukov was not even among the generals who received Stalin in Berlin. Beside this, most characters – from Natasha to   commented the leader played the part of the "magician and the matchmaker who wisely leads the couple to reunion."   
Alexeis character was not intended to be perceived as an individual, It is interesting to note that Alexei Ivanov served as the third man in the group carrying the   and  , a junior political officer. His part in the operation was silenced until years after Stalins death.  but rather, a symbol to the entire Soviet people: he was depicted as an archetypal worker; his date of birth is given as 25 October 1917 by the Julian Calendar, the day of the October Revolution. 
 Fulton Speech, was portrayed in a highly negative fashion. 

The film is regarded by many critics as the epitome of Stalins cult of personality in cinema: Denise Youngblood wrote "it was impossible to go further" in the "veneration" of him;  Philip Boobbyer claimed the cult reached "extraordinary proportions" with its release;  Lars Karl opined it "stood above any other part of the cult";  Slavoj Žižek regarded it as the "supreme case of the Stalinist epic"  Nikolas Hüllbusch commented that it was the "zenith" of the representation of Stalins screen "alter-ego";  and Richard Taylor believed it was "the apotheosis of Stalins cult of Stalin". 

===Restoration===
In 1991, after the dissolution of the Soviet Union The Fall of Berlin had its first public screening in thirty-five years, during the 48th Venice International Film Festival. 

In 1993, Dušan Makavejev included footage from the film in his picture Gorilla Bathes at Noon. {{cite web |url=http://movies.nytimes.com/movie/review?res=990CEED71738F93AA15750C0A963958260|title=Film Festival Review; A Russian Expatriate Adrift in Berlin
|author= Holden, Stephen|date=29 March 1995|work=nytimes.com |accessdate=30 April 2011}} 

In 2003, the film was remastered by a company from Toulouse, in a relatively poor quality. In 2007, it was re-released by the American group International Historical Films. No available version contains the Beria scene, though several old uncensored copies of the film appear to exist.   

==References==
 

==Annotations==
 

==External links==
 
*  and   of The Fall of Berlin, on Mosfilms YouTube channel.
*  for direct free viewing on the Mosfilm studios official site.
*  and   on the IMDb.
*  on kino-teatr.ru.
*  on Mubi.
*  on the Princeton University Department of Slavistics site.
* .
* .
*  on ostfilm.de.

 
 
 

 
 
 
 
 
 
 
 
 
 
 