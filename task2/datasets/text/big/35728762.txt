Full Moon in Blue Water
{{Infobox film
| name           = Full Moon in Blue Water
| image          = 
| image_size     = 
| caption        = 
| director       = Peter Masterson
| writer         = Bill Bozzone David Foster (producer) Dennis Stuart Murphy (associate producer) Eduard Sarlui (executive producer) John Turman (producer) Lawrence Turman (producer)
| narrator       = 
| starring       = Gene Hackman Phil Marshall Fred Murphy
| editing        = Jill Savitt
| studio         = Trans World Entertainment Turman-Foster Company
| distributor    = Trans World Entertainment
| released       = November 23, 1988
| runtime        = 95 mins.
| country        = United States
| language       = English
| budget         = 
| gross          = $450,726 (USA)
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
Full Moon in Blue Water is a 1988 film directed by Peter Masterson. It stars Gene Hackman and Teri Garr. 

==Plot==
Floyd owns a bar called the Blue Water Grill in a town of that name on an island off the gulf coast of Texas. He has lost interest in almost everything in the year since the mysterious disappearance of his wife, neglecting his business and staying home to watch old home movies of their life.

Floyds father-in-law, known to all as the General, is confined to a wheelchair and trapped in the throes of dementia. A mentally challenged local man called Jimmy comes by to look after the General at times when Floyd cant be there.

Into their lives comes Louise, a school bus driver who is falling for Floyd and trying to get him to come out of his stupor. Land opportunists are trying to seize his property and taxes need to be paid. A crisis on the home front forces Floyd to finally confront his future.

==Cast==
*Gene Hackman as Floyd
*Teri Garr as Louise Taylor
*Burgess Meredith as The General
*Elias Koteas as Jimmy
*Kevin Cooney as Charlie ODonnell David Doty as Virgil
*Gil Glasgow as Baytch
*Becky Ann Baker as Dorothy (as Becky Gelke)
*Marietta Marich as Lois
*Alexandra Masterson as Annie Gorman (as Lexie Masterson)

==References==
 

==External links==
* 

 

 
 
 
 
 

 