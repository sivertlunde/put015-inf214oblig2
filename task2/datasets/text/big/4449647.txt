Stag Night
:Stag night is the UK, Canadian, Irish and New Zealand term for a Bachelor party.
{{Infobox film
| name           = STAG NIGHT
| image          = stagnightdvd.jpg
| caption        = 
| director       = Peter A. Dowling
| producer       = Arnold Rifkin Christopher Eberts Michael Philip
| writer         = Peter A. Dowling
| starring       = Kip Pardue Breckin Meyer Vinessa Shaw Karl Geary
| music          = Benedikt Brydern
| cinematography = Toby Moore
| editing        = Vanick Moridian
| studio         = Film Tiger Instinctive Film Rifkin-Eberts
| distributor    = Arsenal Pictures
| runtime        = 84 minutes
| released       =  
| country        = United States
| language       = English
| budget         = United States dollar|$4,000,000 (estimated)
}}

Stag Night is a 2008 American horror film,  written and directed by Peter A. Dowling. 

==Plot==
Four men on a bachelor party in New York ride the subway and, along with two strippers from the club, accidentally get off at a station that closed down in the 1970s.  Trapped in the tunnels beneath New York, they witness the murder of a transit cop by three transients and find themselves on the run for their lives.

==Cast==
* Kip Pardue as Mike
* Vinessa Shaw as Brita
* Breckin Meyer as Tony
* Scott Adkins as Carl
* Karl Geary as Joe
* Sarah Barrand as Michele
* Rachel Oliva as Claire
* Luca Bercovici as Tunnel Rat #1
* Genadii Gancheva as Tunnel Rat #2
* Radoslav Parvanov as Tunnel Rat #3
* Suzanna Urszuly as Woman
* Itai Diakov as Kid Tunnel Rat
* Nikolai Sotirov as Old Man
* Phil Jackson as rain Driver
* Vencislav Stojanov as Cop
* Jo Marr as Young Man
* Amy Mihailova	as Young Woman
* Yordan Zahariev as Bouncer #1
* Gabriel Balanica as Bouncer #2
* Maya Andreeva	as Old Woman
* Dennis Andreev as Child
* Lyubomir Yonchev as Shanty Town Bo

==Production==
Produced by Arnold Rifkin and Chris Eberts, under their Rifkin/Eberts label, and Michael Philip and Jo Marr of Film Tiger.  The movie was shot in 2007 in Sofia, Bulgaria and in New York. The post-production took place at "The Post Group", Hollywood, Los Angeles in 2008. Chris Ouwinga and Darryn Welch financed the film via their production company, Instinctive Film.

Starring Kip Pardue, Vinessa Shaw, Breckin Meyer, Karl Geary, Scott Adkins, Sarah Barrand, Rachel Oliva and Luca Bercovici. 

Closing Credits Song, "Dont Hide Away From the Sun" by Alphanaut.

===Inspiration===
Running in almost real time, Stag Night is a throwback to the early movies of John Carpenter and 70s thrillers such as Deliverance while the plot bears a certain resemblance to that of the classic British horror film Death Line.

==Release==
The film was first time released as Direct-to-DVD on 26 June 2008 in Brazil as Fuga Sobre Trilhos. It was part of the Screamfest Horror Film Festival on 8 February 2010.  Ghost House Pictures released the DVD in the United States on 15 February 2011. 

==Notes==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 