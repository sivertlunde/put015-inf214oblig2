Babes a GoGo
{{Infobox film
| name           = Bébés à gogo
| image          = 
| caption        = 
| director       = Paul Mesnier
| producer       = Taurus Films (France)
| writer         = Marcel Franck Paul Mesnier
| starring       = Jane Sourza Louis de Funès
| music          = Louis Gasté
| cinematography = 
| editing        = 
| distributor    = Corona
| released       = 8 June 1956 (France)
| runtime        = 86 minutes
| country        = France
| awards         = 
| language       = French
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 French Comedy comedy film from 1956, directed by Paul Mesnier, written by Marcel Franck, starring Jane Sourza and Louis de Funès. The film is known under the title:  "Babes a GoGo" (International English title). 

== Cast ==
* Jane Sourza : Isabelle Petitbourgeois, wife
* Raymond Souplex : Stéphane Petitbourgeois, husband
* Louis de Funès : M. Célestin Ratier, representative of the industries of childhood
* Jean Carmet : Hubert, the husband of Stalemate
* Andréa Parisy : Pat, daughter of Isabelle and Stéphane
* Andrée Servilange : Geneviève, dite Mademoiselle, la nurse
* Marthe Alycia : Daphné, mother Hubert
* Arlette Massart : Jeannette, la bonne
* Florence Blot : lemployée de létat civil à la mairie
* Saint-Granier : lui-même, en présentateur
* Valérie Vivin
* Pierre Vernet
* Max Desrau
* Max Revol
* Cécile Eddy
* Bernard Revon

== References ==
 

== External links ==
*  
*   at the Films de France

 
 
 
 
 
 
 


 