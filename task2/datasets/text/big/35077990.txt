Puthiya Thiruppangal
{{Infobox film
| name           = Pudhiya Tiruppangal
| image          = 
| caption        =
| director       = Sharada Ramanathan
| producer       = TCS
| story          = Saradha Ramanathan
| screenplay     =  Nandha Andrea Jeremiah Darani Surveen Chawla Vidyasagar
| cinematography = Madhu Ambat
| editing        = Rajalakshmi
| studio         = Sri Sivaselvanayagi Amman Movies 
| distributor    = 
| released       = 
| runtime        = 
| country        = India
| language       = Tamil
| budget         = 
| gross          = 
| preceded_by    =
| followed_by    =
}} Tamil thriller Nandha and Andrea Jeremiah.   The film deals with the subject of child trafficking.   

==Cast== Nandha as Adithya
* Andrea Jeremiah as Mallika
* Surveen Chawla as Anupama
* Darani
* Gautam Kurupkkl;lBold text

==Production==
 National Film Vidyasagar agreed to compose the films music.  The director will be introducing Rajalakshmi as editor in this film. 

==Soundtrack==
{{Infobox album|  
| Name        = Puthiya Thiruppangal
| Longtype    = 
| Type        = Soundtrack Vidyasagar
| Cover       = 
| Caption     = 
| Released    =  
| Recorded    =  Feature film soundtrack
| Length      =  Tamil
| Label       = 
| Producer    = Vidyasagar
| Last album  = 3 Dots (2013)
| This album  = Puthiya Thiruppangal (2013)
| Next album  = Pullipulikalum Aattinkuttiyum (2013)
}}

The films score and soundtrack were composed by Vidyasagar (music director)|Vidyasagar. The soundtrack received positive reviews from music critics. Behindwoods quoted "Vidyasagar in fine form". 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Lyrics!! Singer(s)!! Notes 
|-  1 || Bigg Nikk Bigg Nikk, Ranjith  || 
|- 2 || Karthik || 
|- 3 || "Oruthuzhi Iru Thuzhi" || Na. Muthukumar || Chinmayi, Haricharan ||
|- 4 || "Yaar Intha" || Na. Muthukumar || Andrea Jeremiah  ||  
|}

==References==
 

 
 
 
 


 