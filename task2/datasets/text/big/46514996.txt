Samson (1915 film)
{{Infobox film
| name = Samson 
| image =
| caption =
| director = Edgar Lewis
| producer =
| writer =  Henri Bernstein (play)   Paul Sloane 
| starring = William Farnum   Maude Gilbert   Edgar L. Davenport
| music = 
| cinematography = 
| editing =
| studio = Box Office Attractions Company
| distributor = Box Office Attractions Company
| released = January 1915 
| runtime = 50 minutes
| country = United States English intertitles
| budget =
| gross =
}}
Samson is a 1915 American silent drama film directed by Edgar Lewis and starring William Farnum, Maude Gilbert and Edgar L. Davenport. It is an adaptation of Henry Bernsteins play Samson (play)|Samson. Farnum later appeared in a second adaptation Shackles of Gold, although the setting was switched from France to America. 

==Cast==
*  William Farnum as Maurice Brachard  
* Maude Gilbert as Marie DAndolin  
* Edgar L. Davenport as Marquis DAmdprom  
* Agnes Everett as Marquise DAndolin  
* Harry Spingler  as Max DAndolin  
* Charles Guthrie as Jerome Govaine  
* Carey Lee as Elise Vernette  
* George De Carlton as M. Deveraux  
* Elmer Peterson as M. Fontenay  
* Edward Kyle as Baron Hatzfeldt

== References ==
 

==Bibliography==
* Goble, Alan.  The Complete Index to Literary Sources in Film. Walter de Gruyter, 1999.

== External links ==
*  

 
 
 
 
 
 
 
 

 