Eettappuli
{{Infobox film
| name = Eettappuli
| image =
| caption =
| director = Crossbelt Mani
| producer =
| writer = Cheri Viswanath
| screenplay = Cheri Viswanath Shankar Ambika Ambika Balan K Nair Silk Smitha
| music = G. Devarajan
| cinematography = EN Balakrishnan
| editing = Chakrapani
| studio = Rose Enterprises
| distributor = Rose Enterprises
| released =  
| country = India Malayalam
}}
 1983 Cinema Indian Malayalam Malayalam film, directed Crossbelt Mani. The film stars Shankar Panicker|Shankar, Ambika (actress)|Ambika, Balan K Nair and Silk Smitha in lead roles. The film had musical score by G. Devarajan.   

==Cast==
   Shankar  Ambika 
*Balan K Nair 
*Silk Smitha 
*Vijayalalitha 
*Raveendran 
 

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Poovachal Khader. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Arimullappoovin || K. J. Yesudas, P. Madhuri || Poovachal Khader || 
|- 
| 2 || Padachonte srishtiyil || K. J. Yesudas || Poovachal Khader || 
|- 
| 3 || Ponnin kaadinu || P. Madhuri || Poovachal Khader || 
|}

==References==
 

==External links==
*  

 
 
 


 