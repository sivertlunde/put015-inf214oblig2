Carrie (2013 film)
 
 
{{Infobox film
| name           = Carrie
| image          = Carrie_Domestic_One-sheet.jpg
| image_size     = 220px
| border         = yes
| alt            =
| caption        = Theatrical release poster 
| director       = Kimberly Peirce
| producer       = Kevin Misher
| screenplay     = Lawrence D. Cohen Roberto Aguirre-Sacasa
| based on       =   Alex Russell Gabriella Wilde Ansel Elgort Julianne Moore
| music          = Marco Beltrami
| cinematography = Steve Yedlin
| editing        = Lee Percy
| studio         = Misher Films
| distributor    = Metro-Goldwyn-Mayer Screen Gems
| released       =  
| runtime        = 99 minutes  
| country        = United States
| language       = English
| budget         = $30 million  . Box Office Mojo. Retrieved October 17, 2013. 
| gross          = $85,039,284 
}} 1974 novel of the same name, though Metro-Goldwyn-Mayer and Screen Gems, who produced the film, employed a script that was reportedly more faithful to Kings original novel. The film stars Chloë Grace Moretz as the titular Carrie White, and Julianne Moore as Carries mother, Margaret White.

Following the initial announcement of March 15, 2013 as the release date, the films public launch was later postponed to October 18, 2013.

==Plot==
 
 Carrie (Chloë Grace Moretz), is a shy, lonely girl, who nears her graduation from Ewen High School in Maine.
 first menstrual Miss Desjardin (Judy Greer) comforts Carrie and sends her home with Margaret, who believes menstruation is a sin. Margaret demands that Carrie abstain from showering with the other girls. When Carrie refuses, Margaret hits her with a Bible and locks her in her "prayer closet". As Carrie screams to be let out, a crack appears on the door, and the crucifix in the closet begins to bleed.

Miss Desjardin informs the girls who teased Carrie that they will endure boot-camp style detention for their behavior. When Chris refuses, she is suspended from school and banned from the prom. She storms out, vowing revenge.
 the ability to move things with her mind. She researches her abilities, learning to harness them. Sue Snell (Gabriella Wilde) attempts to make amends to Carrie by asking her boyfriend, Tommy Ross (Ansel Elgort), to take Carrie to the prom. Carrie accepts Tommys invitation. When she tells her mother, Margaret forbids Carrie to attend. Asking her mother to relent, Carrie manifests her telekinesis. Margaret believes this power comes from the Devil in Christianity|Devil.
 Alex Russell), and his friends plan revenge on Carrie. They kill a pig and drain its blood into a bucket. Margaret tries to prevent Carrie from going to the prom, but Carrie uses her powers to lock her mother in the closet. At the prom, Carrie is nervous and shy, but Tommy kindly puts her at ease. As part of Chris and Billys plan, Chriss friend, Tina Blake (Zoë Belkin), slips fake ballots into the voting box, which name Carrie and Tommy prom queen and king.

At home, Sue receives a text from Chris taunting her about her revenge on Carrie. Sue drives to the prom, arriving just as Carrie and Tommy are about to be crowned. Sue sees a bucket of pigs blood dangling above Carrie but, before she can warn anyone, Miss Desjardin hustles her out, suspecting that Sue is planning to humiliate Carrie.

Chris dumps the bucket of pigs blood onto Carrie and Tommy. Chris shower video appears on large screens above the stage, inciting laughter from some in the audience, until the bucket falls onto Tommys head, killing him. Enraged, Carrie takes her revenge telekinetically, killing dozens of people. A fire breaks out and, as the school burns to the ground, Carrie walks away, leaving a trail of fire and destruction in her wake. Chris and Billy attempt to flee in Billys car. Chris urges Billy to run Carrie over, but Carrie flips the car into a gas station, killing them.

Carrie arrives home and she and Margaret embrace. Margaret tells Carrie about the night of Carries conception: Carries father "Marital rape|took" Margaret, who enjoyed the experience. Margaret attacks Carrie, who kills her with several sharp tools. Carrie becomes hysterical and makes stones rain from the sky to crush the house. When Sue arrives, a furious Carrie attacks her with her powers, but senses something inside Sue, and tells her that her baby is a girl. Carrie pushes a stunned Sue out of the house to safety as the house collapses and apparently kills the Whites.

As a voice-over gives her testimony in court regarding the prom incident, Sue visits Carries grave and places a single white rose by the headstone. As she leaves, the gravestones surface begins to break.

===Blu-ray alternate opening and ending===
In the alternate opening, a young Carrie has a discussion with her teenage neighbor, who is sun-bathing, over the fact that Margaret believes that women with breasts are sinful. Margaret catches them in the conversation and believes that the neighbor is offending Carrie, not before the neighbors mother disagrees with her. Suddenly, stones begin to rain only on the White household. Margaret, believing it is a sign from God, takes shelter as she carries Carrie inside.

The gravestone crack and the court speech are not present in the alternate ending. Sue is in the hospital trying to deliver her baby. When the baby is about to come out, a bloody hand bursts out from her vagina and grabs Sue. She wakes up; screaming in fear and her mother tries to comfort her since the nightmare is over. A subliminal image reveals Carrie, bloody in her prom dress, holding Sues infant daughter.

==Cast==
 
* Chloë Grace Moretz  as Carrie White Miss Desjardin
* Portia Doubleday as Chris Hargensen Alex Russell as Billy Nolan
* Gabriella Wilde as Sue Snell
* Julianne Moore as Margaret White
* Ansel Elgort as Tommy Ross
* Barry Shabaka Henley as Principal Henry Morton
 
* Zoë Belkin as Tina Blake 
* Karissa Strain as Nicki Watson  
* Katie Strain as Lizzy Watson
* Samantha Weinstein as Heather
* Demetrius Joyette as George Dawson
* Mouna Traore as Erika

==Planning and casting==
In May 2011, representatives from MGM and Screen Gems announced that the two companies were producing a film remake of Carrie. The two studios hired   playwright Roberto Aguirre-Sacasa to write a screenplay that delivers "a more faithful adaption" of Kings novel—Aguirre-Sacasa previously adapted King’s work The Stand into a comic book in 2008. 

Upon hearing of the new adaptation, King remarked, "The real question is why, when the original was so good?" He also suggested  -ABC News. 
 Alex Russell Miss Desjardin.   

==Release==
The original release date was March 15, 2013,  but in early January 2013 the release date was moved to October 18, 2013.  

Sony held a "First Look" event at the New York Comic Con on October 13, 2013 that allowed attendees to view the film prior to the release date. The event was followed by a panel session with several members of the cast and crew. 

Trailers for the film included a phone number that offered promotions to the caller, as well as a recording of a simulated encounter with characters from the film. 

===Home video===
The film was released on DVD and Blu-ray on January 14, 2014. The Blu-ray features an alternate ending and 9 deleted scenes.

==Reception==
  On Metacritic, it scored a 53 out of 100 based on 34 reviews, indicating "mixed or average reviews." 

Kevin C. Johnson of the   gave the film a positive review, stating: "The actings strong; in addition to Moretz and Moore, Judy Greer is a welcome presence in the   gave the film a C- rating, criticizing Moretzs Carrie as "too adjusted, coming across less like the very peculiar girl King described in his novel and more like the stealth babe of some nottie-to-hottie teen romance." Dowd lamented on the film as a whole, "Its a strange thing to say about a movie so obsessed with the red stuff, but this Carrie is bloodless." 

A lot of fans were disappointed in the movie and are trying to release an extended director´s cut because of it. 

===Box office===
Sony estimated the revenue for the opening weekend of Carrie as between $16 million and $18 million, while others estimated a bigger margin of $24 million to $28 million due to the Halloween season. However, the final takings totaled $16,101,552 and the film was ranked at number 3 behind Gravity (film)|Gravity and Captain Phillips (film)|Captain Philips, both of which were in their second and third weeks, respectively. By the end of the week, the film managed to gross $20,121,355.  In week two, the film slipped 62.8% to sixth place with $5,900,000 and 43.2% to ninth place in its third week with $3,400,000.   

At the end of its run, the film has grossed $35,266,619 in North America and $49,524,059 in other countries for a worldwide gross of $84,790,678. It is the 67th highest-grossing film of 2013 in the United States. 

===Accolades===
 
{| class="wikitable" style="width:100%;"
|-
! Year
! Award
! Category
! Recipient(s)
! Result
|-
|rowspan=3| 2014
| 40th Peoples Choice Awards|Peoples Choice Awards  Favorite Horror Movie
|Carrie
|  
|- Saturn Awards    Saturn Award Best Horror Film
|Carrie
|  
|- Saturn Awards  Saturn Award Best Young Actor/Actress
|Chloë Grace Moretz
|  
|-
|}

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 