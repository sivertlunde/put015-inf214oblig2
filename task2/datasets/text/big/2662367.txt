Two for the Money (film)
{{Infobox film
| name        = Two for the Money
| image       = Two for the Money Poster.jpg
| caption     = Theatrical release poster
| director    = D. J. Caruso 
| writer      = Dan Gilroy
| starring    = Al Pacino Matthew McConaughey Rene Russo Armand Assante Jeremy Piven Jaime King Carly Pope
| music = Christophe Beck
| editing = Glen Scantlebury
| cinematography = Conrad W. Hall
| producer    = James G. Robinson Jay Cohen Guy McElwaine
| studio      = Morgan Creek Productions 
| distributor = Universal Pictures
| released    =  
| runtime     = 122 min.
| language    = English
| country     = United States
| budget      = $35 million
| gross       = $30,526,509 
}} sports gambling.

==Plot== football star who, after sustaining a career-ending injury, takes a job handicapping football games. His success at choosing winners catches the eye of Walter Abrams (Pacino), the slick head of one of the biggest sports consulting operations in the United States. Walter takes Brandon under his wing, and soon they are making tremendous amounts of money.

Langs in-depth knowledge of the game, leagues and players brings in big winnings and bigger clients. Abrams cable television show, The Sports Advisors, skyrockets in popularity when he adds Langs slick "John Anthony" persona to the desk, infuriating Jerry Sykes (Jeremy Piven), who up to now has been Walters in-house expert. Langs total image is remade &mdash; new car, new wardrobe and a new look with the assistance of Walters wife, Toni (Russo), a hair stylist.

Things suddenly go south, however, when Lang begins playing his hunches instead of doing his homework. He loses his touch and is even physically assaulted by the thugs of a gambler (Armand Assante) who lost a great deal of money following Langs advice. Lang and Abrams once-solid relationship sours.

Langs new high-rolling lifestyle depends entirely on his ability to predict the outcomes of the games. Millions are at stake by the time he places his last bet, and Abrams grows increasingly unstable, secretly gambling all of his own money on Langs picks and becoming suspicious that Lang is having an affair with his wife.
 coach of a junior league football team.

==Cast==
* Al Pacino as Walter Abrams
* Matthew McConaughey as Brandon Lang
* Rene Russo as Toni Abrams
* Armand Assante as C.M. Novian
* Carly Pope as Tammy
* Jeremy Piven as Jerry Sykes
* Jaime King as Alexandria
* Ralph Garman as Reggie Charles Carroll as Chuck Adler

==Release and reception==
The film itself was released on October 8, 2005 to generally bad reviews and lackluster box office returns. It scored only 22% overall rating at Rotten Tomatoes. Two for the Moneys North American receipts came to only $22,991,379 and $30,526,509 worldwide, against a production budget of $35 million.

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 