Nirjan Saikate
Nirjan Bengali film directed by Tapan Sinha. It was released in 1963. This movie is an adaptation of the novel with the same name by eminent Bengali writer Samaresh Basu, who wrote this travelogue under his pen name Kalkut.

==Plot==
The writer is himself the protagonist. He is on his way to Puri by train where he gets acquinted with a group of 4 Bengali widows accompanying their niece, also going to Puri. The writer was going to just get out of his day to day Kolkata life whereas these ladies were actually going to calm down their niece who had just suffered a break up with her lover. The rest of the story revolves around these people and their attraction and involvement with the writer. The writer is amused with them and for his part tries to calm the girl. The story guides us around the different relationships the writer develops with each of the character he meets. The movie tries to show the social taboos for widows and teaches a wise philosophy that life is ahead of us and not before. There is a pretty good description of Puri, Konark and Rambha. At the time this movie was shot it was possible for Tapan Sinha to shot inside the Konark sun temple, also the bullock cart way to Konark is an extra for the viewer.

==Cast==
* Anil Chatterjee as the protagonist -the writer
* Sharmila Tagore as the Niece
* Chhaya Devi, Ruma Guha Thakurta, Renuka Devi, Bharati Devi&mdash;as the 4 widows.
* Rabi Ghosh as a Panda of Puri
* Jahar Ganguly as the Hotel Owner.
* Nripati and Pahari Sanyal also had important roles in the film.

==Awards==
The leading 4 ladies of this film -the 4 widows, shared the National Film Award for Best Actress in 1963.

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 

 