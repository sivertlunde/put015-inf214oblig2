Rana (film)
 
{{Infobox film
| name           = Rana
| image          = Rajinikanth_Rana.jpg
| alt            =  
| caption        = Early promotional poster of the film
| director       = K. S. Ravikumar
| producer       =  
| writer         = S. Ramakrishnan
| screenplay     = K. S. Ravikumar
| story          = Rajinikanth
| starring       =  
| music          = A. R. Rahman
| cinematography = R. Rathnavelu Anthony
| studio         =  
| distributor    = 
| released       = 
| runtime        = 
| country        = India
| language       = Tamil
| budget         = 
| gross          = 
}} historical action Ocher Picture Eros International. The film will feature cinematography by R. Rathnavelu and music composed by A. R. Rahman. 

Despite the occurrence of principal photography, which took place on 29 April 2011, casting is still incomplete. During the first schedule of filming, Rajinikanth fell ill on sets and was admitted to hospital for over a month, resulting in an indefinite postponement of filming. Further attempts for the film to continue production throughout the year failed. 

In April 2014, it was announced that Ranas script would be written, and would be ready in six months 

==Cast==
* Rajinikanth as Rana, Sena and Veera
* Deepika Padukone as Vadhana
* Vadivelu
* Ganja Karuppu

==Production==

===Development=== Eros International and Soundaryas Ocher Picture Productions. 
 AVM Studios went through subsequent hospital visits, after which he was eventually obliged to be admitted for treatment of an undisclosed form of nephropathy, extensively delaying the commencement of Rana.  Following the delay, the team considered making Rana as an animation film but Rajinikanth was insistent that the film would be a live action film. Film producer Murali Manohar then suggested that the animation film could potentially be a sequel to Rana and thus the team began work on Kochadaiiyaan. 

===Casting=== 2011 legislative assembly elections in Tamil Nadu.  But, on Rajinis insistence, Vadivelu was brought back into the film.  

R. Rathnavelu was specially chosen by Rajinikanth as the films cinematographer,  after the two worked together in their prior successful venture Enthiran (2010).    Rajeevan was signed up as art director. 

==References==
 

 

 
 
 
 
 
 