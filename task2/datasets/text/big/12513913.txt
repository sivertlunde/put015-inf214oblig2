Custer's Last Stand (serial)
{{Infobox film
| name           = Custers Last Stand
| image          = Custers Last Stand FilmPoster.jpeg
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Elmer Clifton Louis Weiss
| screenplay     = George Arthur Durlam Eddie Granemann William Lively
| story          = George Arthur Durlam Eddie Granemann William Lively
| starring       = Rex Lease Lona Andre William Farnum Ruth Mix Jack Mulhall
| music          = Hal Chasnoff
| cinematography = Bert Longenecker
| editing        = George M. Merrick Holbrook N. Todd
| studio         = 
| distributor    = Stage & Screen Productions
| released       = January 2, 1936 April 3, 1936 (film)
| runtime        = 15 chapters (328 min) 84 min (film)
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 film serial based on the historical Custers Last Stand at the Little Bighorn River. It was produced by the Poverty Row studio Stage & Screen Productions, which went bust shortly afterwards as a victim of the Great Depression. This serial stars many famous and popular B-Western actors as well as silent serial star Helen Gibson.

In April of the same year, the serial was edited into an 84-minute feature film, which was released under the same name.

==Plot overview==
The serial follows multiple plot threads, centering on a Medicine Arrow taken in battle and a secret gold mine, in the lead up to the Battle of Little Big Horn.
 

==Cast==
*Rex Lease as Kit Cardigan and his father John C. Cardigan
*Lona Andre as Belle Meade
*William Farnum as James Fitzpatrick
*Ruth Mix as Elizabeth Custer
*Jack Mulhall as Lieutenant Cook
*Nancy Caswell as Barbara Trent
*George Chesebro as Lieutenant Frank Roberts
*Dorothy Gulliver as Red Fawn General George A. Custer
*Helen Gibson as Calamity Jane
*Josef Swickard as Major Henry Trent, MD
*Chief Thundercloud as Young Wolf
*Allen Greer as Wild Bill Hickok
*High Eagle as Crazy Horse Howling Wolf as Sitting Bull

==Production==
Commenting on the plot, Cline notes that this serial contains several historical characters in a purely fictitious setting. "The story rambled through a series of loosely connected plots and sub plots" leading to Little Big Horn. {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | year = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | page = 39
 | chapter = 3. The Six Faces of Adventure
 }} 

===Stunts===
*Yakima Canutt Ken Cooper
*Carl Mathews Mabel Strickland (riding double) Wally West

==Release==

===Theatrical===
Custers Last Stand was well received by action fans, regardless of its historical inaccuracies. 

==Chapter titles==
# Perils of the Plains
# Thundering Hoofs sic| 
# Fires of Vengeance
# The Ghost Dancers
# Trapped
# Human Wolves
# Demons of Disaster
# White Treachery
# Circle of Death
# Flaming Arrow
# Warpath
# Firing Squad
# Red Panthers
# Custers Last Ride
# The Last Stand
 Source:  {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | year = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | page = 217
 | chapter = Filmography
 }} 

==See also==
* List of film serials by year
* List of film serials by studio

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 

 