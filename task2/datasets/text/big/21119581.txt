Inuk (film)
{{Infobox film
| name           = Inuk
| image          = Inuk_Poster.jpg
| border         = yes
| caption        = 
| director       = Mike Magidson
| producer       = Ann Andreasen Mike Magidson Marc Buriot Sylvie Barbe  http://www.inuk-lefilm.com/EN/crew.html 
| writer         = Mike Magidson Ole Jørgen Hammeken Jean-Michel Huctin 
| starring       = Ole Jørgen Hammeken Gaaba Petersen Rebekka Jørgensen Sara Lyberth Inunnguaq Jeremiassen Elisabeth Skade
| music          = Justin La Vallee Karina Moller Robert Peary HIVSHU  
| editing        = Cecile Coolen
| studio         = 
| distributor    = 
| released       =  
| country        = Greenland France Greenlandic
| budget         = 
| gross          = 
}} Best Foreign Language Oscar at the 85th Academy Awards, but it did not make the final shortlist.   

==Background==
The "Childrens Home Uummannaq" is situated 500&nbsp;km north of the  )   "On Thin Ice - Le Voyage dInuk - the film", accessed 01-20-2009    (French language|French)   "Le Voyage dInuk", accessed 01-20-2009 

==Plot==
Inuk covers a journey from Greenlands south to its north as an homage to the origins of the Inuit people. It is the coming-of-age story of 16-year-old Inuk (Gaaba Petersen), who was raised in the south in Greenlands capital Nuuk, and who is torn between the violence of his alcoholic parents and his dreams of creating an Inuit rock band. He is sent to a foster home in the north, where his foster guardian and teacher, Aviaaja (Rebekka Jørgensen) sends him to the bear hunter Ikuma (Ole Jørgen Hammeken) so that he may learn wisdom. But Ikuma had begun to doubt himself after his own world began to decline due to the effects of global warming. This begins Inuks difficult initiation into manhood through a journey by dogsled where the seal hunt replaces video games. On his journey he meets and is attracted to the rebellious Naja (Sara Lyberth). He finally reconciles his life, but in doing so re-awakens the old injury that had affected the life of Ikuma.   

==Cast==
* Ole Jørgen Hammeken as Ikuma
* Gaaba Petersen as Inuk
* Rebekka Jørgensen as Aviaaja
* Sara Lyberth as Naja
* Inunnguaq Jeremiassen as Minik
* Elisabeth Skade as Inuks Mother

==Release==
A preliminary screening was at salon du cinema de Paris 2009 while the film was still being edited. The French film team was there to speak about the difficulties of shooting in  )   "From 16 to 18 January 2009 - Grande Halle de la Villette", accessed 01-20-2009    (French language|French)   "Salon du Cinema 2009: ask for the full program! (Samedi) (Saturday)", accessed 01-20-2009 

The Official Theatrical release was done on the 11 May 2012 in Nuuk Greenland with all the Greenlandic officials where invited by Børnehjemmet UPI the Greenlandic producer of the film.  

==See also==
* List of submissions to the 85th Academy Awards for Best Foreign Language Film
* List of Greenlandic submissions for the Academy Award for Best Foreign Language Film

== References ==
 

==External links==
*    
*    
*  

 
 
 
 
 
 
 
 