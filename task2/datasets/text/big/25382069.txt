El Misterio de Huracán Ramírez
{{Infobox Film
| name           = El Misterio de Huracán Ramírez 
| image          = Misteriodehuracan poster1962.jpg
| caption        = 
| director       = Joselito Rodríguez
| producer       = Juan Rodríguez Mas (credited as Juan R. Mas)
| writer         = Fernando Méndez Joselito Rodríguez Juan Rodríguez Mas
| starring       = David Silva Totina Jackson Titina Romay Pepe Romay Carmelita González Freddy Fernández Carlos Agosti
| music          = Sergio Guerrero
| cinematography = Manuel Gómez Urquiza
| editing        = Carlos Savage     
| distributor    = Cinematográfia Romá Películas Rodríguez Estudios Churubusco Azteca S.A.
| released       =  
| runtime        = 86 minutes
| country        = Mexico
| language       = Spanish
| budget         = 
}}
 David Silva Freddy Fernández. Daniel García makes his film début as Huracán Ramírez, replacing Spanish-born wrestler Eduardo Bonada, who would make only a cameo appearance.

==Cast and characters==
*  and character to someone else.
*Tonina Jackson as Señor Torres/Tonina Jackson: Fernandos father
*Carmelita González as Laura: Fernandos wife. Freddy Fernández as Pichí: Lauras younger brother and Fernandos best friend.
*Titina Romay as Margarita Torres: Fernandos little sister. Daniel García was the man under the Huracán Ramírez mask in all of the action sequences. would also play the role in wrestling rings all over Mexico for decades after the movie was released.
*The film also featured a number of real-life luchadores wrestling with and against Huracán Ramírez in the action sequences.

==Plot==
The film continues the story of Fernando Torres, now seemingly retired from lucha libre, having sold his former identity to a rival luchador. The new Huracán has proved himself to be a savvy businessman, running his own wrestling arena. The lives of Huracán and the Torres family are soon threatened however, by a local gangster known as El Príncipe (Carlos Agosti), who will stop at nothing until he discovers the true identity of Huracán Ramírez.

==External links==
* 

 
 
 
 
 
 