Haunter (film)
{{Infobox film
| name           = Haunter image           = Haunter poster.jpg caption         = Theatrical release poster
| director       = Vincenzo Natali
| producer       = Steve Hoban
| writer         = Brian King
| starring       = {{Plainlist |
* Abigail Breslin
* Peter Outerbridge
* Michelle Nolden
* Stephen McHattie
* Samantha Weinstein
}}
| editing        = Michael Doherty
| cinematography = Jon Joffin
| music          = Alex Khaskin
| distributor    = {{Plainlist | Wild Bunch
* IFC Midnight
}}
| released       =   }}
| runtime        = 97 minutes
| country        = Canada
| language       = English
| budget         =
| gross          =
}}
Haunter is a 2013 Canadian supernatural horror  film directed by Vincenzo Natali, written by Brian King, and starring Abigail Breslin. The film premiered at the 2013 South by Southwest Film Festival, and was picked up for U.S. distribution there by IFC Midnight. 

==Plot==
Lisa Johnson, the ghost of a teenage girl who becomes aware that she is dead, haunts a house somewhere in northern Ontario.  Along with her parents and brother, who are unaware that they are dead, she is stuck on the same day they were murdered in 1985. As she becomes more aware of her circumstances, she realizes that she can make contact with people in other timelines.  As she explores this ability, a pale man appears and warns her to stop. Undeterred, Lisa uses personal items from other people killed in the house to make a connection with Olivia, part of a family living in the house in the future who will become the next set of victims.  

With the help of Olivia and the spirits of other murdered girls, Lisa is transported into the timelines of other victims and unravels the mystery of the house.  She causes her family to come to terms with the knowledge that they are dead, and thus "awakened" they become able to assist her. After her family escapes to the afterlife, Lisa stays behind to stop the murderous evil spirit who haunts the house. With help from the evil spirits family (the original victims), Lisa overcomes him and rescues another family from suffering the same fate. Thus breaking the cycle of possessions and murder-suicides, Lisa awakes, no longer reliving the same day.

==Cast==
* Abigail Breslin as Lisa Johnson
* Peter Outerbridge as Bruce Johnson
* Michelle Nolden as Carol Johnson
* Stephen McHattie as Pale Man - Edgar Mullins
** David Knoll as Young Edgar
* Peter DaCunha as Robert "Robbie" Johnson
* Samantha Weinstein as Frances Nichols
* Eleanor Zichy as Olivia
* David Hewlett as David - Olivias Father
* Sarah Manninen as Olivias Mother
* Martine Campbell as Olivias Sister
* Michelle Coburn as Mary Brooks
* Tadhg McMahon as Edgars Father
* Marie Dame as Edgars Mother

==Production==
Haunter was filmed at Toronto and Brantford, Ontario, Canada.     Production took 25 days.   Natali said that he was drawn to the film because, unlike Splice (film)|Splice, which took him twelve years to complete, Haunter only needed to be shot. 

==Release==
Haunter premiered at South by Southwest film festival on March 9, 2013,  and received a limited US theatrical release on October 18, 2013.   It was released on home video on February 11, 2014,  and made $129,447 on domestic video sales. 

==Reception==
Rotten Tomatoes, a review aggregator, reports that 53% of 32 surveyed critics gave the film a positive review; the average rating was 5.3/10.   Metacritic rated it 49 out 100.   Linda Barnard of the Toronto Star rated it 2/4 stars and called the script "ill-focused and juvenile".   Joe Leydon of Variety (magazine)|Variety called it "a modestly inventive variation on genre conventions".   John DeFore of The Hollywood Reporter wrote that the film is "sufficiently novel to uphold   reputation as a filmmaker not content telling conventional fanboy stories."   Nicolas Rapold of The New York Times wrote, "the films frazzled thought experiment becomes an adequate yarn."   Annlee Ellingson of the Los Angeles Times wrote that Natali "brings cool visuals and a punk attitude to Brian Kings cleverly layered script."   Josh Modell of The A.V. Club rated it C and wrote, "The occasionally intriguing, but ultimately middling Haunter is caught in some kind of gauzy haunted-house purgatory between a girl-powered YA story and a ghostly serial-killer mystery."   Nav Qateel of Influx Magazine rated it B and called it "better than it perhaps should have been". 

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 