The High Sign
{{Infobox film name            = The High Sign image           = The High Sign (1921) - Ad 1.jpg caption         = Buster and the Blinking Buzzards give the titular "high sign" director        = Edward F. Cline Buster Keaton producer        = Joseph M. Schenck writer          = Edward F. Cline Buster Keaton starring        = Buster Keaton Bartine Burkett Charles Dorety Al St. John music           = cinematography  = Elgin Lessley editing         = Buster Keaton distributor     = Metro Pictures released        =   runtime         = 21 min. language  English (original) intertitles country         = United States budget          =
|}}
 1921 American One Week The Haunted House (1921), that the film was released.  The title refers to the secret signal used by the underworld gang in the film.

Guitarist Bill Frisell released a soundtrack to the movie in 1995 on his album The High Sign/One Week.  The Rats & People Motion Picture Orchestra premiered its new score for the movie in 2008.

== Plot ==
Buster plays a drifter who cons his way into working at an amusement park shooting gallery.  Believing Buster is an expert marksman, both the murderous gang the Blinking Buzzards and the man they want to kill end up hiring him.  The film ends with a wild chase through a house filled with secret passages.

==Cast==
* Buster Keaton - Our Hero (as Buster Keaton)
* Bartine Burkett - Miss Nickelnurser (uncredited)
* Charles Dorety - Villain (uncredited) Al St. John - Man in during target practice (uncredited)

==See also==
* List of American films of 1921
* Buster Keaton filmography

==References==
{{cite book
  | last = Keaton
  | first = Eleanor
  | authorlink = 
  |author2=Jeffrey Vance
  | title = Buster Keaton Remembered
  | publisher = Harry N. Abrams, Inc.
  | year = 2001
  | isbn = 0-8109-4227-5 }}

==External links==
 
*   
*  
*   on YouTube

*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 


 