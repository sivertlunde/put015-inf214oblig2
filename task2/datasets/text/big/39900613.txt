Red Wedding
 
 
{{Infobox film
| name           = Red Wedding
| image          = Red Wedding.jpg
| caption        = Red Wedding poster
| director       = Lida Chan Guillaume Suon
| producer       = Rithy Panh
| starring       = 
| music          = Benjamin Bleuez Etienne Lechuga
| cinematography = Guillaume Suon Ambroise Boussier Lida Chan
| editing        = Guillaume Suon Lida Chan Narin Saobora
| distributor    = Women Make Movies Bophana Production Tipasa Production
| released       =  
| runtime        = 58 minutes
| country        = Cambodia France
| language       = Khmer
| budget         = 
| gross          = 
}}
Red Wedding (French: Noces rouges) is a 2012 documentary film co-directed by Lida Chan and Guillaume Suon, which portrays a victim of forced marriage under the Khmer Rouge regime.

The film premiered at the 2012 International Documentary Filmfestival Amsterdam (IDFA) and won the Award for Best Mid-Length Documentary.

== Synopsis ==

Between 1975 and 1979, at least 250,000 Cambodian women were forced into marriages by the Khmer Rouge. Sochan was one of them. At the age of 16, she was forced to marry a soldier who raped her.
 the international tribunal set up to try former Khmer Rouge leaders.

Red Wedding is the story of a survivor who pits her humanity against an ideology and a system designed to annihilate people like her.

== Production ==

Produced by Rithy Panh, Red Wedding is a Cambodian-French co-production by  ,   and  .

Red Wedding was produced with the support of  ,  ,  ,  ,  ,   and  .

The film was shot in Pursat province, in Cambodia, between 2010 and 2012.

== Distribution ==

Red Wedding is distributed by Women Make Movies,    and  .

== Reception ==

Red Wedding is the first film about a victim of forced marriage and rape under the Khmer Rouge. Lida Chan and Guillaume Suons survey began in 2010 when forced marriages were qualified crimes against humanity by the Khmer Rouge tribunal. Breaking a 30-years-old taboo and releasing the word of many victims, the film was received with emotion by the public and critical acclaim, both in Cambodia and abroad.             

"Red Wedding combines all the elements of a great documentary: a powerful historical episode, retold from an unexpected angle, and a first-rate photography. A truly poetic documentary." — IDFA jury 2012.

== Awards ==

* Best Mid-Length Documentary - International Documentary Filmfestival Amsterdam (IDFA) 2012      
* Golden Award (Mid-length competition) - Aljazeera International Documentary Film Festival 2013 (Doha, Qatar)  
* Jury Prize - Gdansk DocFilm Festival 2013 (Poland)  
* Special Jury Prize - HRHDIFF 2013 (Yangon, Burma)
* Best South East Asian Human Rights Film - FreedomFilmFest (Kuala Lumpur, Malaysia)  
* Special Mention Award - Salaya International Documentary Film Festival (Thailand)  

== References ==
 

== External links ==
* 
* 
* .

 
 
 
 
 