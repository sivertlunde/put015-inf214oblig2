Gold Dust Gertie
{{Infobox film name = Gold Dust Gertie image = GoldDustGertie.jpg producer = writer = Screenplay:   director = Lloyd Bacon starring = Ole Olsen Chic Johnson Claude Gillingwater music = Herbert Taylor David Mendoza cinematography = James Van Trees editing = Harold McLernon distributor = Warner Bros. released =   runtime = 65 minutes language = English
}} Ole Olsen, Chic Johnson and Claude Gillingwater. 

==Synopsis==
Winnie Lightner plays the part of a gold digger who married men only to divorce them and collect alimony. She marries Ole Olsen in 1927, divorces him, and then marries his friend Chic Johnson and also divorces him. By 1930, both Olson and Johnson have remarried but they continue paying alimony to Lightner without the knowledge of their current wives. When Olson and Johnson are late with their payment, Lightner shows up at their work. Olson and Johnson work at a firm that designs womens sportwear. Olson and Johnsons boss, played by Claude Gillingwater, is very old fashioned and insists on designing womens bathing suits that are so modest that they end up resembling the models from twenty years ago. Because of this, the business is doing poorly and Olson and Johnson are low on cash at the moment. Lightner, seeing that she currently has no chance of collecting from her ex-husbands at the moment, decides to vamp their boss. Lightner eventually convinces Gillingwater to liberalize his views. Lightner designs a new modern bathing suit which ends up winning an award. Gillingwater falls in love with Lightner and proposes marriage. Lightner accepts but problems soon arise. As they are celebrating their engagement another ex-husband shows up and, taking pity on Gillingwater, he attempts to warn him. Furthermore, the minister who Gillingwater has chosen to officiate at their wedding knows all about Lightners ex-husbands and her gold digging schemes.

==Cast==
*Winnie Lightner as Gertrude Gertie Dale Ole Olsen as George Harlan
*Chic Johnson as Elmer Guthrie
*Dorothy Christy as Mabel Guthrie
*Claude Gillingwater as John Aberdeen Arnold
*Arthur Hoyt as Dr. Rodman Tate, the Minister
*George Byron as Captain Osgood
*Vivien Oakland as Lucille Harlan
*Charley Grapewin as Nicholas Hautrey
*Charles Judels as Monsieur Pestalozzi
*Virginia Sale as Secretary Modelling Skimpy Bathing Suit

==Preservation==
Only the cut print released in the United States seems to have survived at the Library of Congress.,   The complete film was released intact in countries outside the United States where a backlash against musicals never occurred. It is unknown whether a copy of this full version still exists.

==References==
 

== External links ==
*  

 

 
 
 
 
 
 