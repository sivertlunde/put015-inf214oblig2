Na Ghar Ke Na Ghaat Ke
 
{{Infobox film
| name           = Na Ghar Ke Na Ghaat Ke
| image          = NaGharKeNaGhaatKe2010Poster.jpg
| border         = yes
| caption        = Film poster Rahul Aggarwal
| produce        = T. P. Aggarwal
| writer         = Alok Upadhyay
| starring       =  
| music          = Lalit Pandit
| editing        = Vinod Pathak
| country        = India
| language       = Hindi
| released       =  
}} Rahul Aggarwal as a Uttar Pradesh migrant to Mumbai. The film was released on 12 March 2010.

==Plot== Rahul Aggarwal) is a simple, rustic man who tries his luck in the city of dreams, Mumbai. He gets himself employed at the Mausam Vibhaag, his only means to a lucrative and thriving future. The plot thickens and so do Devki’s circumstances, for he pursues something that he had never meant to. He finds himself doing everything except dedicating time to his vocation. He comes across an array of city people who often find his innocence amusing and comical, but Devki realises that they stick with him even in the thickest of bogs that his life hauls him in. From the village folk to the corrupt cop to the impish goon, Devki finds himself cared for and aided even while he feels entrenched in a Na Ghar Ke Na Ghaat Ke state of affairs. The film depicts the everyday struggle of common man in a ‘rural meets urban’ set-up.

Director Aggarwal plays the lead opposite Narayani Shastri, while one meets iconic characters played by Om Puri, Paresh Rawal, Ravi Kishan and Neena Gupta.

==Cast== Rahul Aggarwal	 ...	Devakinandan Tripathi
* Neena Gupta      	 ...	Devkinandans mother
* Ehsan Khan	 ...	Special appearance
* Ravi Kishan    	 ...	Madan Khachak
* Anant Mahadevan	 ...	V. G. Chunawala
* Manmauji      	 ...	Special appearance
* Lalit Pandit   	 ...	Special appearance
* Om Puri       	 ...	Sankata Prasad Tripathi
* Paresh Rawal     	 ...	Inspector Khote
* Shweta Salve	 ...	Special appearance
* Ruma Sengupta	 ...	Devkis Dadi
* Narayani Shastri	 ...	Mithilesh
* akshita arora 	 ...	Mithilesh  mother 
* Rakesh Shrivastav	 ...	Devkinandans father-in-law
* Shriya Saran     	 ...	Special appearance
* Neeraj Vora	       ...	Mr. R. K. Khanna
* Giaa Manek         ...   Daughter of Sankata Prasad Tripathi

==External links==
*  

 
 
 
 


 