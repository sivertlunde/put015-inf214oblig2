The Mask (1988 film)
 
{{Infobox film
| name           = The Mask
| image          = 
| caption        = 
| director       = Fiorella Infascelli
| producer       = Ettore Rosboch Lilia Smecchia
| writer         = Adriano Aprà Ennio De Concini Fiorella Infascelli Enzo Ungari
| starring       = Helena Bonham Carter
| music          = Luis Enríquez Bacalov
| cinematography = Acácio de Almeida
| editing        = Francesco Malvestito
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}

The Mask ( ) is a 1988 Italian romance film directed by Fiorella Infascelli. It was screened in the Un Certain Regard sectiona at the 1988 Cannes Film Festival.   

==Cast==
* Helena Bonham Carter - Iris
* Michael Maloney - Leonardo
* Feodor Chaliapin, Jr. - Leonardos father
* Roberto Herlitzka - Elia
* Alberto Cracco - Viola
* Michele De Marchi - Theatre Company Manager
* Valentina Lainati - Maria
* Saskia Colombaioni - Saskia
* Arnaldo Colombaioni - Nani
* Valerio Colombaioni - Ercolino
* Walter Colombaioni - Acrobata
* Maria Tedeschi - Talia
* Massimo Fedele - Don Gaetano

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 