Fading Gigolo
 
{{Infobox film
| name           = Fading Gigolo
| image          = Fading Gigolo poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = John Turturro Jeffrey Kusama-Hinte
| writer         = John Turturro
| starring       = John Turturro Woody Allen Sharon Stone Sofia Vergara Vanessa Paradis Liev Schreiber
| music          = Abraham Laboriel Bill Maxwell
| cinematography = Marco Pontecorvo
| editing        = Simona Paggi
| studio         = Antidote Films
| distributor    = Millennium Entertainment  
| released       =  
| runtime        = 90 minutes  
| country        = United States
| language       = English
| budget         = 
| gross          = $13,369,873 
}} premiered in flopped at the box office.

==Plot== Hassidic rabbi, for treatment.

Murray tells her Fioravante is a massage healer who can help her and takes her to see him. Too observant to even shake hands with him, she nonetheless allows Fioravante to massage her back and that touch, the first since well before her marriage to her husband, brings her to tears. Meanwhile, Dovi, who works for Shomrim (neighborhood watch group)|Shomrim, a Williamsburg, Brooklyn neighborhood patrol, becomes suspicious and follows Murray. Dovi is in love with Avigal, but she does not encourage him. Fioravante and Avigal meet several more times, culminating in a kiss in the park. 
 Rabbinic Court, laws of modesty, but nothing more, explaining she was lonely. Avigal now accepts Dovi, but has him drive her to Fioravante to say good bye. Fioravante tells Murray he is leaving, but reconsiders after an encounter with a beautiful woman.

==Cast==
 
* John Turturro as Fioravante
* Woody Allen as Murray
* Sharon Stone as Dr. Parker
* Sofía Vergara as Selima
* Vanessa Paradis as Avigal
* Liev Schreiber as Dovi
* Tonya Pinkins as Othella
* Max Casella as Guy at counter
* Aida Turturro as Drivers wife
* Bob Balaban as Sol
* Michael Badalucco as Burly driver
* David Margulies as Chief Rebbe
* Aurélie Claudel as Tai Chi woman
* Loan Chabanol as Loan
 

==Release==
Fading Gigolo screened in the Special Presentation section at the 2013 Toronto International Film Festival on September 8, 2013.  It received a limited release in the United States on April 18, 2014.  However, the film performed poorly at the box office, only grossing $13,369,873 worldwide.

==Reception==
Fading Gigolo received mixed reviews. On film aggregation website  , it has a 58/100 score (indicating "mixed or average"), based on reviews from 38 critics. 

Geoffrey Macnab of The Independent gave the film 3 out of 5 stars, noting that "Turturros trick is to take stereotypical characters and to portray them in an offbeat and surprising way."  Peter Debruge of Variety (magazine)|Variety gave it a favorable review. "It certainly benefits from having Allen aboard, though only Turturro would fight to bring such a warm and disarming experiment to the screen", he said.    Catherine Shoard of The Guardian praised Woody Allens performance, stating that "Turturro has given Allen his biggest and best on-screen turn in years".  Stephanie Zacharek of Village Voice called the film "a breeze, enjoyable both for its sweetness and its unapologetic silliness". 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 