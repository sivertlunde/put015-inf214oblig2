Fireball (film)
{{Infobox film
| name           = Fireball
| image          = Fireball-thai-film.jpg
| alt            =  
| caption        = 
| director       = Thanakorn Pongsuwan
| producer       = Adirek Wattaleela
| writer         = Thanakorn Pongsuwan, Kiat Sansanandana, Taweewat Wantha, Adirek Wattaleela
| starring       = Preeti Barameeanat, Khanutra Chuchuaysuwan, Kumpanat Oungsoongnern
| music          = Giant Wave
| cinematography = Teerawat Rujintham, Wardhana Vunchuplou, Suntipong Waiwong
| editing        = Saknakorn Neteharn, Thanakorn Pongsuwan, Adirek Wattaleela
| studio         = 
| distributor    = Icon Film Distribution (2011)
| released       =  
| runtime        = 88 minutes
| country        = Thailand
| language       = Thai
| budget         = Unknown
| gross          = Unknown
}}
 Thai martial arts  action film. The film  directed by Thanakorn Pongsuwan combines Muay Thai and basketball.

==Plot==
Tai, a young man arrested on a crime charge, is discharged thanks to his twin brother Tans dogged help. After being set free, he finds Tan in a coma with severe injuries. Tans girlfriend, Pang, tells Tai that his brother got involved in some risky business to raise money to fight Tais case. Tai feels guilty that his problems brought his brother trouble. He then traces what happened to Tan, which ultimately leads him into illegal basketball gambling. Tai wants to find out who is behind this gambling and why his brother was beaten unconscious. He finally joins the "Fireball" team, a team which belongs to Hia Den and whose players include Singha, Kay, Ik, and Muek. In order to uncover the truth, Tai trades many things-possibly even his life.

==Cast==

*Preeti Barameeanat as Tai - Tan
*Khanutra Chuchuaysuwan	 as Pang
*Kumpanat Oungsoongnern	 as Muk
*Phutharit Prombandal as Den
*9 Million Sam	 as Zing
*Rattanaballang Tohssawat as Tun (credited as Arucha Tosawat)
* Kannut Samerjai-Yi

==Reception==
The films director won the Guru Prize for Most Energetic Film at the Fant-Asia Film Festival, and was tied with Vampire Girl vs. Frankenstein Girl.

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 


 