Love Ke Chakkar Mein
{{Infobox film
| name           = Love Ke Chakkar Mein
| image          = Bm lovekechakkarmein.gif
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = B. H. Tharun Kumar
| producer       = Dev Goradia
| screenplay     = Rajeev Kaul Praful Parekh
| story          = Rajeev Kaul Praful Parekh Dialogue: Raghuvir Shekhawat
| starring       = Rishi Kapoor Akshat Bhatia Namitha Satish Shah Shoma Anand
| music          = Anand Raj Anand Daboo Malik Nikhil Nikhil Vinay
| cinematography = Neelaabh Kaul
| editing        = 
| studio         = Manash Productions
| distributor    = 
| released       =  
| runtime        =  India
| Hindi
| budget         = 
| gross          = 
}}

Love Ke Chakkar Mein is a 2006 Hindi language comedy film written by Rajeev Kaul and Praful Parekh, and directed by B. H. Tharun Kumar.  The film stars Rishi Kapoor, Akshat Bhatia, Satish Shah, and Shoma Anand,   and marks the debut of Akshat Bhatia. 

==Background==
When Parmita Katkar, Miss India Asia Pacific 2003, signed with the production, it was referred to as a hot project.   And as soon as December 2004, actor Rishi Kapoor reported that he had completed his part in the film. 

==Plot==
Unemployed Vicky (Akshat Bhatia) meets Neha (Namita) on the internet, and when they meet in person, they fall in love and wish to get married. Nehas father, Vishal ( ), contacts the police and accuses Vicky of the crime.

==Cast==
* Rishi Kapoor as Armaan Kochar
* Akshat Bhatia as Vicky
* Satish Shah as Vishal Batra
* Shoma Anand as Kaajal A. Kochar
* Namitha as Neha V. Batra
* Parmita Katkar as Bijlee
* Shashi Kiran as Dr. Subhash

==Reception==
IndiaGlitz makes note that small budget films with "relatively" unknown actors occasionally do well due to promotion and word-of-mouth.  In noting a dry spell for director B. H. Tharun Kumar, "may just about make it happen again at the box office."   Times of India noted the film as one of the many Summer comedies "hoping to tickle their way to box-office success". 

==References==
 

==External links==
*   at the Internet Movie Database

 
 
 
 
 