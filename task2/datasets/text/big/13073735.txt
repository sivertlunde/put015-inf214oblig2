Forest of Death (film)
 
 
{{Infobox film
| name           = Forest of Death
| image          = Forest of Death film poster.jpg
| caption        = The Hong Kong theatrical poster. Danny Pang
| producer       = The Pang Brothers Danny Pang
| narrator       =
| starring       = Shu Qi Ekin Cheng
| music          = Payont Permsith
| cinematography = Choochart Nantitanyatada
| editing        = Curran Pang
| distributor    = Universe Films
| released       = Hong Kong: 22 March 2007
| runtime        = 85 min.
| country        = Hong Kong
| language       = Cantonese
| budget         =
| preceded_by    =
| followed_by    =
| website        =
| amg_id         =
}}
 2007 Cinema Hong Kong Danny Pang.

==Plot==
Police detective Ha Chun-Chi (Shu Qi) is investigating a rape and murder that took place in a mysterious forest that has also been the scene of many suicides. The main suspect in the murder case is Patrick Wong (Lawrence Chou), but he denies committing the crime.

Has investigation leads her to botanist Shum Shu-Hoi (Ekin Cheng), who has been experimenting with plants from the forest. Shums girlfriend, May (Rain Li), feigns interest in the forest to gain information for a tabloid television show she works for.

Shums experiments reveal that the plants can act as witnesses in the murder case, and sets up a re-enactment of the crime in the forest, where the plants will act as lie detectors.

==Cast==
*Shu Qi as Detective Ha Chun-Chi
*Ekin Cheng as Shum Shu-hoi
*Rain Li as May
*Lau Siu-Ming as Mr. Tin, the forest ranger
*Lam Suet as Commissioner Wong
*Tommy Yuen as Shu-hois assistant
*Lawrence Chou as Patrick Wong
*Cub Chin as Producer

==Release== Region 3 DVD in Hong Kong by Universe Laser on 10 May 2007. It also screened out of competition at the 2007 Bangkok International Film Festival.

==External links==
*  

 

 
 
 
 
 
 
 


 
 