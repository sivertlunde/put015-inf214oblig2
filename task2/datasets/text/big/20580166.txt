Thunder Birds (1942 film)
{{Infobox film
| name           = Thunder Birds
| image          = Poster - Thunder Birds 01.jpg
| caption        = Theatrical poster
| director       = William A. Wellman
| producer       = Lamar Trotti
| writer         = Lamar Trotti Darryl F. Zanuck (as Melville Crossman) John Sutton Jack Holt Dame May Whitty
| music          = David Buttolph Ernest Palmer
| color          = Technicolor Walter Thompson Twentieth Century-Fox Film Corporation
| distributor    = Twentieth Century-Fox Film Corporation
| released       =  
| runtime        = 78 minutes
| country        = United States English
| budget         =
| preceded_by    =
| followed_by    =
}}
 John Sutton.  It features aerial photography and location filming at an actual Arizona training base of the United States Army Air Forces named Thunderbird Field No. 1 during World War II.

The film was made as a propaganda vehicle to boost civilian morale,   Turner Classic Movies. Retrieved: September 21, 2014.  while at the same time providing a look at training activities and promoting airpower as a means of winning the war. Wellman was himself a veteran of the United States Army Air Service|U.S. Air Service as a World War I fighter pilot.

==Plot== Jack Holt), Reginald Denny), who is in charge of the Royal Air Force cadets at the base.
 George Barbier), also a close friend of Steves.
 flying coveralls. When he lands, she seems miffed, but responds to his passionate kiss of greeting. Kay is still very fond of him, but no longer deeply in love.
 conditional reflex", and asks for more time.

Peter reveals the reason why he wants to fly. His brother was killed on a bombing mission and their grandmother, Lady Jane Stackhouse (Dame May Whitty), summoned Peter, then an intern at a London hospital, home to show him the cheque she is sending Winston Churchill for the purchase of a new bomber to carry on the fight in Toms memory. Since no male is left in the family to do so, Peter leaves his hospital service to enlist in the RAF to learn to fly. After hearing his story, Steve agrees to keep Peter in training.

On his first leave, Peter meets Kay Saunders and is immediately infatuated. She dates Peter, but warns him that she might still be in love with Steve. Still, her instincts warn her that Steve would make a poor husband, as he is a carefree nomad not interested in settling down. Peter admires Steve and is grateful to him, so he warns Steve that he is in love with Kay and intends to propose marriage. Steve promises that he will not wash Peter out because of their rivalry. His judgment tells him that Peter will one day be a fine pilot. When Squadron Leader Barrett gives Peter a check flight, he gets sick again. Steve stands by Peter in a showdown, threatening to resign.

Gramps throws a Fourth of July party for the cadets and, to help Steve win Kay, tricks Peter into riding a bucking bronco. This backfires when Peter proves to be an adept horseman. Steve sees that Kay has fallen in love with Peter, even before she realizes it herself.
 sandstorm and is blown along the ground toward a cliff. Peter lands nearby and saves Steve, but the wind flips his aircraft over. Mac believes that Peters incompetence caused the damage, washes him out, and fires Steve. Kay convinces Mac and Barrett to giving them one more chance. She tells Steve that she has decided to marry Peter, and reminds him of his own words about where the war will be won. Peter makes good on the faith shown in him, making a deadstick landing when his engine fails during his solo flight. Soon after, Steve, hobbling on a cane, greets an incoming class of new RAF cadets.

==Cast==
* Gene Tierney as Kay Saunders 
* Preston Foster as Steve Britt  John Sutton as Peter Stackhouse  Jack Holt as Lt. Col. "Mac" MacDonald 
* Dame May Whitty as Lady Jane Stackhouse  George Barbier as Col. Cyrus P. "Gramps" Saunders 
* Richard Haydn as RAF cadet George Lockwood  Reginald Denny as Squadron Leader Barrett 
* Ted North as Cadet Hackzell  
* Janis Carter as Blonde 
* C. Montague Shaw as Doctor 
* Viola Moore as Nurse 
* Nana Bryant as Mrs. Blake 
* Joyce Compton as Saleswoman 
* Bess Flowers as Nurse

==Production==
 
Thunder Birds was intended by Fox studio chief Darryl F. Zanuck to be a follow-up to his popular A Yank in the R.A.F., given the working title of A Tommy in the U.S.A. Using the pen name "Melville Crossman," Zanuck himself wrote the original story. The studio also purchased rights to a magazine story entitled "Spitfire Squadron," written by Arch Whitehouse, but did not use it as part of the screenplay.   Turner Classic Movies. Retrieved: September 20, 2014. 

Hollywood Reporter wrote that Dana Andrews would play the lead in Thunder Birds opposite Gene Tierney and that either Bruce Humberstone or Archie Mayo would direct. 
 The Ox-Bow Incident, which Wellman began immediately after production ended for Thunder Birds.   Turner Classic Movies. Retrieved: September 20, 2014. 
 Stearman PT-17 primary trainer, but also featured many live action formation flights of Vultee BT-13 Valiant and North American AT-6 trainers. Hardwick and Schnepf 1983, p. 62.  Filming coincided with the time frame of the story. Additional sequences were filmed in the first week of June 1942 at the Falcon Field Training Facility in Mesa, Arizona, with retakes during July 1942.  Stunt pilot Paul Mantz flew the live action flying scenes. 

==Home video release==
20th Century Fox released Thunder Birds on June 6, 2006, as a Region 1 DVD, while in Region 2, it is available as part of a DVD box set of Gene Tierneys films for TCF.

==Reception==
Thunder Birds was received with decidedly mixed reviews.  , October 29, 1942. 

Audiences, however, were thrilled by the aerial scenes, which Crowther noted: " (contained) ... many shots of basic trainers rolling and zooming on yellow wings against the blue. Those are the only exalting glimpses in the whole film."  Film historians consider Thunder Birds a classic aviation film. 

==References==
===Notes===
 
===Citations===
 
===Bibliography===
 
* Hardwick, Jack and Ed Schnepf. "A Buffs Guide to Aviation Movies". Air Progress Aviation, Vol. 7, No. 1, Spring 1983.
* Orriss, Bruce. When Hollywood Ruled the Skies: The Aviation Film Classics of World War II. Hawthorne, California: Aero Associates Inc., 1984. ISBN 0-9613088-0-X.
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 