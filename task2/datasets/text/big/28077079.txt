Hurricane at Pilgrim Hill
{{Infobox film
| name           = Hurricane at Pilgrim Hill
| image          =
| image_size     =
| caption        =
| director       = Richard L. Bare
| producer       = Hal Roach Jr. (producer)
| writer         = James Charles Lynch (magazine story "The Battle at Pilgrim Hill")
| narrator       =
| starring       = See below
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =
| runtime        = 51 minutes
| country        = USA
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Hurricane at Pilgrim Hill is a 1950 American film directed by Richard L. Bare.

==Plot summary==
 

==Cast==
*Clem Bevans as Sam "Bigmouth" Smedley
*Cecil Kellaway as Jonathan Huntoon Smith David Bruce as Tom Adams, Smiths Attorney
*Virginia Grey as Janet Smedley Adams
*Robert Board as Steve Terhune
*Leslie Banning as Debbie Smith, Jonathans Daughter
*Syd Saylor as Sheriff Luke Arundle
*Frank Lackteen as Broken Head (Running Deer in Credits) Oliver Blake as Running Deer (Broken Head in Credits) Billy Gray as Johnny, Bigmouths Grandson
*Ann Doran as Katie, his mother
*Harry Hayden as Man on Train

==Soundtrack==
 

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 


 