Demonicus
{{Infobox film |
 name = Demonicus|
 image = |
 caption = |
 director = Jay Woelfel |
 producer = David S. Sterling Johnnie J. Young (co-producer) Charles Band (executive producer) | Tim Sullivan Jay Woelfel |
 starring = Gregory Lee Kenyon Venesa Talor Brannon Gould Kyle Tracy Jennifer Capo Allen Nabors Candace Kroslak Dominic Joseph Val Perez |
 cinematography = Jeff Leroy |
 editing = Jonathan Ammon Jay Woelfel |
 distributor = Full Moon Entertainment |
 released =   |
 runtime = 72 minutes | English |
 }} 2001 horror Tim Sullivan & Jay Woelfel and starred Gregory Lee Kenyon, Venesa Talor, Brannon Gould, Kyle Tracy, Jennifer Capo, Allen Nabors, Candace Kroslak, Dominic Joseph and Val Perez.

==Plot==
A group of young students lost in the Italian Alps become victims of an ancient Gladiator curse. One of the students becomes possessed and hunts down the rest.

==Cast==
* Gregory Lee Kenyon ..... James / Tyranus
* Venesa Talor ..... Gina
* Brannon Gould ..... Dino
* Kyle Tracy ..... Joe
* Jennifer Capo ..... Maria
* Allen Nabors ..... Anthony
* Candace Kroslak ..... Teresa
* Dominic Joseph ..... Frankie
* Val Perez ..... Charlene

==External links==
*  
*  
*  

 
 
 
 
 
 