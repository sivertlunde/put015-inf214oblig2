Halloween H20: 20 Years Later
{{Infobox film
| name           = Halloween H20: 20 Years Later
| image          = HalloweenH20poster.jpg
| caption        = Canadian Theatrical release poster
| director       = Steve Miner Kevin Williamson 
| writer         = Robert Zappia Matt Greenberg
| story          = Robert Zappia
| based on       =   Michelle Williams Adam Hann-Byrd Jodi Lyn OKeefe Janet Leigh Josh Hartnett LL Cool J Joseph Gordon-Levitt  Chris Durand
 
| music          = John Ottman
| cinematography = Daryn Okada
| editing        = Patrick Lussier
| released       =  
| studio         = Miramax Films Dimension Films Nightfall Productions Buena Vista Pictures
| runtime        = 82 minutes
| country        = United States
| language       = English
| budget         = United States dollar|$17 million Halloween: H20 at   
| gross          = $73 million  
}} Michelle Williams. original Halloween.
 Michael Myers, who attempted to kill her all those years ago. When Michael eventually appears, Laurie must face evil one last time, while the life of her teenage son hangs in the balance.
 original film, original Halloween Kevin Williamson original Halloween.

The film received mixed reviews from critics, praising the script, direction of Steve Miner and the performances of Jamie Lee Curtis and Josh Hartnett, but received criticism for certain plot points (such as ignoring some of the previous films) and its short running time.

==Plot== Michael Myers (Chris Durand), appears and attacks Marion before slitting her throat, killing her. The police arrive as Michael leaves the house with the file on Laurie Strode.

On Halloween two days later, Laurie Strode (Jamie Lee Curtis) awakens from a nightmare. Since 1978, she has tried to get her life together with the hope that Michael would never come after her again. She faked her death in an auto accident and then relocated to Northern California under the assumed name "Keri Tate". She has a seemingly perfect life with her teenage son John (Josh Hartnett), boyfriend Will (Adam Arkin), and a job as headmistress at Hillcrest Academy High School, a private boarding school. However, Laurie is far from happy, as the tragic events from 1978 still haunt her.
 Michelle Williams) go looking for their classmates. They find Sarahs body hanged in the pantry and are chased by Michael through the school grounds. At one point in the chase, John is stabbed in the thigh by Michael. Just as Michael is about to get Molly and John, they are saved by Laurie and Will, who open the door for them just in time. Just as the door closes behind them, Laurie and Michael come face to face for the first time since their last encounter twenty years ago. Laurie and Will hide Molly and John and decide to try to kill Michael.

When Will sees a shape approaching from the far end of the hall, he takes Lauries pistol (which she had secretly kept under her pillow at home during all this time) and shoots the shape five times, only to discover that it was the schools security guard, Ronny (LL Cool J). The real Michael then appears and kills Will by stabbing him in the back with his knife. Laurie helps John and Molly escape but she tells them to go for help while she chooses to go back to the school with a fire ax. She finds Michael and attempts to kill him several times, and finally after stabbing him multiple times, he topples over a balcony. She approaches his body and pulls one of the knives out of his chest. She slowly raises the knife high above her head, preparing to bring it down on Michael and kill him, but before she can deliver the final blow, Ronny suddenly appears, having survived the accidental shooting and grabs her. He restrains her from attacking Michael and drags her out of the cafeteria.

The police come and put Michaels corpse in a body bag, loading it into a coroners van. Laurie, knowing that Michael is incredibly hard to kill and not believing that he is really dead, grabs the ax from earlier and an officers pistol, and she steals the van. While driving away, Michael sits up and escapes the body bag. She slams on the brakes, throwing him through the windshield. She then tries unsuccessfully to run him over. The vehicle tumbles down a cliff but she escapes, while Michael gets pinned between the van and a tree. Laurie recovers the axe and approaches him. He reaches out to her, apparently seeking forgiveness and compassion. At first it seems she will accept this, and begins reaching out to him, but then she slowly pulls her hand back and with one swing decapitates Michael, finally killing him. Michaels head rolls down the hill and Laurie exhales.

==Cast==
* Jamie Lee Curtis as Laurie Strode/Keri Tate
* Josh Hartnett as John Tate Michelle Williams as Molly Cartwell
* Adam Arkin as Will Brennan
* LL Cool J as Ronald "Ronny" Jones
* Jodi Lyn OKeefe as Sarah Wainthrope
* Adam Hann-Byrd as Charlie Deveraux
* Janet Leigh as Norma Watson
* Nancy Stephens as Marion Chambers
* Joseph Gordon-Levitt as Jimmy Howell
* Lisa Gay Hamilton as Shirley "Shirl" Jones Michael Myers
* Tom Kane as Samuel Loomis|Dr. Sam Loomis (voice only)

==Production== Curtis wanted original to have active involvement in it. It was believed that Carpenter opted out because he wanted no active part in the sequel; however, this is not the case. Carpenter agreed to direct the movie, but his starting fee as director was $10 million. Carpenter rationalized this by believing the hefty fee was compensation for revenue he never received from the original Halloween, a matter that was still a bit of contention between Carpenter and Akkad even after twenty years had passed. When Akkad balked at Carpenters fee, Carpenter walked away from the project.
 Kevin Williamson was involved in various areas of production. Although not directly credited, he provided rewrites in character dialogue, which is seen heavily throughout the teen moments. Miramax/Dimension Films felt his involvement as a co-executive producer merited being credited. The original working title for the film was Halloween 7: The Revenge of Laurie Strode.

===Music===
The original music score was composed by  s scores from Scream, Scream 2, and Mimic (film)|Mimic by a team of music editors as well as new cues written by Beltrami during the final days of sound mixing on the film. Dimension Films chief Bob Weinstein demanded the musical changes after being dissatisfied with Ottmans score. Halloween: H20 score at   
 Creed was featured in the film during a party sequence and is also heard during the credits of the film.

The theme from  , Jamie Lee Curtis real life mother). (In addition Janet Leigh stands in front of a 1957 Ford Sedan, license plate NFB 418, which was her car in the movie Psycho.)

===Masks===
 
As said on Halloween: 25 Years of Terror, Halloween H20 had scenes re-shot due to complaints of the Myers mask used in the film. Scenes that could not be re-shot had a CGI mask replace them frame by frame. Four masks were made for the film.

==Alternate Television Version== FX network premiered an alternate version of the film, adding and extending footage not seen in the original release. 

==Reception==

===Box office=== 2007 remake of the original. It was released on August 5, 1998 in the US and later in many other countries. H20 cost $17 million to produce and returned over $55 million in domestic box office sales with an opening weekend of $16,187,724.  As for video/DVD rentals, the film grossed over $21 million.

===Critical response===
Halloween H20 received mixed reviews. with a rating of 51% on   and Josh Hartnett.   

==Continuity==
* As originally conceived, the plot device in which Laurie had faked her death was written explicitly to account for her reported "death" in  , and the original story treatment for H20 acknowledged the events depicted in the fourth through sixth films in the series, including the existence and death of Lauries daughter, Jamie Lloyd; however, the filmmakers ultimately chose to ignore the continuity of the previous three sequels to focus more on Jamie Lee Curtiss character, Laurie Strode. Although Lauries faked death remained in the script, the scenes mentioning Jamie were removed from the story, and the films dialogue was adjusted to indicate that Michael Myers had not been heard from in the twenty years since the night depicted in the first two films.

* Michaels 20 missing years are explained in the comic book series Halloween: Sam, which also explains what happened to Dr. Loomis in the new continuity and further goes on to explain that Loomis and Laurie both knew he would return and she was placed in a witness protection program. The new continuity explains that Michaels body was never recovered from the hospital.
 Halloween II.

* The Halloween comic book series, published by   and Halloween H20, but in doing so made the plot of   (unreleased at the time) impossible.

* Some scenes that were dropped from the other three movies were placed in H20. For example, the scene where Laurie is hiding beneath a table in the dining hall, Michael starts flipping the tables over. This was originally going to be placed in  , where Michael chases Jamie Lloyd through the elementary school. It was written that she would hide under a desk and Michael was going to flip the desks over. This was dropped due to time constraints. However, Moustapha Akkad remembered and filmed it as part of H20.

* Jamie Lee Curtis mother, Janet Leigh, appears in this film as Mrs. Watson. Janet Leigh is best known for her role as Marion Crane in Psycho (1960 film)|Psycho (1960). There is a scene where we see Mrs. Watsons car behind her. It is a 1957 Ford Custom 300. This is the same car that Marion trades her car for in Psycho when she is on the run. It is rumored to be the same exact car.

* Judith Myers death is briefly mentioned just like in previous Halloween films. It is brought up by Laurie Strode when she reveals her true identity to her boyfriend, Will; and Laurie says Judith was 17 years old at the time of her death. According to John Carpenters Halloween (1978 film)|Halloween (1978), however, the dates on Judiths tombstone are November 10, 1947 – October 31, 1963 making her 15 years old at the time of death. It is believed that the writers of Halloween: H20 changed Judiths age so that it would correspond with the character Laurie Strodes age from the original Halloween film of 1978.

* The yearbook shown in the beginning of the film lists Laurie as being part of the Class of 1978. However, if she was still in school in October 1978, she would have been part of the Class of 1979.

==Home media== Buena Vista Home Video. In the United Kingdom, the film was released on VHS in 1998, a re-release was made on September 1, 2000.

On DVD, the film was first released by   and  . 

Halloween H20 was released in Canada for the first time ever on Blu-ray by Alliance released along with Halloween: The Curse of Michael Myers and   on January 12, 2010.  On May 3, 2011 it was released by Echo Bridge Home Entertainment in the US but with an aspect ratio of 1.78:1 (not cropped from the original aspect ration of 2.35:1, but rather open-matte due to the film being shot in   in one Blu-ray collection. 

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 