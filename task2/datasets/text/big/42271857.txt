Chowky
{{Infobox film
| name           = Chowky
| image          =
| alt            =  
| caption        =
| director       = Shekhar (Kitu) Ghosh  
| producer       = Bunty Walia    Jaspreet Singh Walia  
| writer         = 
| starring       = Sandhya Mridul   Raja Chaudhary Yudhishtir Urs
| music          = Sonu Nigam   Bickram Ghosh   Arjit Dutta   Bapi Tutul
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       =  
| runtime        =
| country        = India
| language       = Hindi
| budget         =
| gross          =
}}
Chowky is an upcoming Hindi suspense thriller directed by Shekhar (Kitu) Ghosh. The film is produced by Bunty Walia and Jaspreet Singh Walia under the banner G S Entertainment Pvt Ltd.  The film features Sandhya Mridul, Raja Chaudhary and Yudhishtir Urs as the main characters and is slated to release in October 2015. 

== Plot ==
Chowky, a suspense thriller, is a story of one night between two strangers with a lot of twists and turns. The film revolves around a criminal and a sub-inspector, over an eventful night, in a Chowky (police station) where fate has got the two together. 

== Cast ==
* Sandhya Mridul   
* Raja Chaudhary  
* Yudhishtir Urs  

== Music ==
Composer duo Sonu Nigam - Bickram Ghosh have composed a promotional song for Chowky.  They have previously composed music for Jal (film)|Jal and Sooper Se Ooper.

Music composer duo Bapi Tutul have composed 2 songs in the film and composer Arjit Dutta has composed 1 song in the film.  

Singer Sunidhi Chauhan has sung a song in the film titled Khanjar Ki Dhar. 

==Soundtrack==
{| class="wikitable"
|-  style="background:#ccc; text-align:center;"
| Track # || Title || Singer(s) || Lyrics || Composer(s)
|- Chowky (Title Sonu Nigam - Bickram Ghosh   
|- Khanjar Ki Sunidhi Chauhan||????? ||?????
|}

== References ==
{{reflist|refs=
 {{cite news
|url=http://www.indicine.com/bollywood/chowky/
|title=Chowky
|date=
|work=Indicine
|accessdate=21 March 2014}} 

 {{cite news
|last=Kansara
|first=Krupa
|url=http://timesofindia.indiatimes.com/Entertainment/India_Buzz/I_have_one-pack_abs_Yudi_/articleshow/3172648.cms
|title=I have one-pack abs: Yudi
|date=28 June 2008
|work=Times of India
|accessdate=21 March 2014}} 

 {{cite news
|url=http://www.bollyvista.com/article/p/32/8102
|title=Bunty Walia plans 8 films in 3 years
|date=
|work= Bolly Vista
|archiveurl=https://web.archive.org/web/20070701045826/http://www.bollyvista.com/article/p/32/8102
|archivedate=1 July 2007
|accessdate=21 March 2014}} 

 {{cite web
|url=http://www.youtube.com/watch?v=22NvZQgl0-M
|title=CHOWKY - PROMO (Online Exclusive)
|date=
|work=YouTube
|accessdate=21 March 2014}} 

 {{cite news
|url=http://www.bollywoodhungama.com/moviemicro/cast/id/504592
|title=Bollywood Hungama
|date=
|work=Bollywood Hungama
|accessdate=21 March 2014}} 

 {{cite news
|url=http://news.desitvforum.net/2014/04/kitu-has-three-films-in-his-kitty/
|title=Kitu has three films in his kitty
|date=4 April 2014
|work=Desi TV News
|accessdate=7 April 2014}} 

 {{cite news
|last=Adarsh
|first=Taran
|url=http://www.filmibeat.com/bollywood/news/2007/bunty-walia-eight-films-190907.html
|title=Bunty Walia takes up eight projects
|date=19 September 2007
|work=Filmi Beat
|accessdate=7 May 2014}} 

 {{cite web
|url=http://www.facebook.com/photo.php?v=10151625879500289
|title=Chowky FB
|date=
|work=Chowky FB
|accessdate=7 May 2014}} 

 {{cite news
|url=http://www.gomolo.com/chowky-movie/21005
|title=Gomolo
|date=
|work=Gomolo
|accessdate=13 June 2014}} 

 {{cite news
|url=http://entertainment.chennaipatrika.com/post/2014/07/22/Shekhar-Ghosh-Chowky-takes-Kolkata-Youth-by-Storm.aspx
|title=Shekhar (Kitu) Ghoshs Chowky takes Kolkata Youth by Storm
|date=22 July 2014
|work=ChennaiPatrika
|accessdate=23 July 2014}} 

}}

 
 
 


 