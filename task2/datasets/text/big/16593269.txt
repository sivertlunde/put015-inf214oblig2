Moscow-Cassiopeia
{{Infobox Film
| name           = Moscow-Cassiopeia
| image          = 
| image_size     = 
| caption        = 
| director       = Richard Viktorov
| producer       = 
| writer         = Isai Kuznetsov, Avenir Zak
| narrator       = 
| starring       = Innokenti Smoktunovsky, Lev Durov
| music          = 
| cinematography = 
| editing        = 
| distributor    = Gorky Film Studio   Telecinco   BBC One   Polonia 1   France 4   ZDF   Rai Movie, Rai Fiction   ABS-CBN   TV3 (Malaysia)   Asianet, Nickelodeon-India
| released       =  
| runtime        = 85 min
| country        = Soviet Union
| language       = 
| budget         = 
| gross          = 
}} Soviet 1973 film directed by Richard Viktorov based on a script by Isai Kuznetsov and Avenir Zak. Followed by Otroki vo vselennoy (second part, 1974). Runtime - 85 min.

==Synopsis==
From the depths of the universe Earth can hear the radio signals of intelligent beings from a planet of the star system Shedar (Alpha Cassiopeia constellation). A project is set up, proposed by the young inventor Vitya Sereda, to send a spaceship to reach the planet - but the flight will last for decades, so the crew of the spaceship "Dawn " (Starship relativistic nuclear annihilation), is to be recruited from teenage students.

The project is all carefully thought out but student Fyodor Lobanov stows away aboard the starship and unwittingly causes it to transcend the speed of light and so reaching its target 27 years ahead of schedule...

== Cast ==
* Innokenti Smoktunovsky as I.O.O. (Special Service Executive)
* Vasili Merkuryev as academician Blagovidov
* Lev Durov as academician Filatov Yuri Medvedev as academician Ogon-Duganovsky
* Pyotr Merkuryev as academician Kurochkin

=== Space ship DAWN crew === Mikhail Yershov as Sereda
* Aleksandr Grigoryev as Kozelkov
* Vladimir Savin as Kopanygin
* Vladimir Basov Jr. as Lobanov
* Olga Bityukova as Kuteishchikova
* Nadezhda Ovcharova as Sorokina
* Irina Popova as Panfyorova

=== Other cast ===
* Anatoli Adoskin
* Natalya Fateyeva
* Nikolai Figurovsky
* Artyom Karapetyan
* Valentina Kutsenko
* Sergei Radchenko
* Raisa Ryazanova
* Nadezhda Semyontsova
* Natalya Strizhenova
* Anna Viktorova
* Nikolai Viktorov
* Mikhail Yanushkevich
* V. Zolotaryov

== Trivia == Relativistical Nuclear (Yaderniy)

==Awards==
* Premio for the Best Film for Kids of the All-Union Cinema Festival, Baku, 1974
* Special Premio of the International Cinema Festival of Science Fiction Films, Triest, 1975
* Special Prize of the International Cinema Festival (in the Children films category), Moscow, 1975
* Platero Prize of the International Cinema Festival as the film for the Kids and Youth, Gijón, 1975.
* Diploma of the Moscow Technical Contest of the Films, UNIATEK congress, Moscow, 1976
* State Premio of RSFSR in the honour of Vasilyiev Brothers, 1977.

==External links==
* 
* 

 
 
 
 
 


 
 