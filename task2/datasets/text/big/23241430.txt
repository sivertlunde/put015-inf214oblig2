Girl in the Cadillac
{{Infobox Film
| name           = Girl in the Cadillac
| image          = Girl-in-the-cadillac.jpg
| caption        = 
| director       = Lucas Platt
| producer       = Thomas Baer
| writer         = James M. Cain (novel, The Enchanted Isle) John Warren (screenplay)
| narrator       =  Michael Lerner William Shockley
| music          = Anton Sanko
| music cues     = Philip Cacayorin
| cinematography = Nancy Schreiber
| editing        = Norman Hollyn
| distributor    = Columbia Tristar Home Video
| released       = 1995
| runtime        = 89 minutes
| country        = United States English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

Girl in the Cadillac is a crime drama film released in 1995. The film stars Erika Eleniak and William McNamara. 

==Film synopsis==
A woman called Mandy (Eleniak) runs away from her home. She meets Rick (McNamara) at a bus station who takes her with him to a rendezvouz with a pair of bank robbers. After listening to their plan, Mandy agrees to drive the getaway car for $5,000, when they convince her everything will go smoothly. However, the robbery goes awry resulting in several bank employees killed, and Mandy flees with Rick and $75,000. As a result, she is chased by the police and the other two robbers. During their escape, Mandy and Rick buy a red Cadillac convertible. Ultimately, she decides she wants to give the money back, but Rick disagrees. 

==Filming==
The film was shot in locations in Bakersfield and Los Angeles, California, and Corpus Christi, Texas.

==Cast==
*Erika Eleniak as Mandy 
*William McNamara as Rick Michael Lerner as Pal
*Bud Cort as Bud
*Valerie Perrine as Tilly William Shockley as Lamar
*Leland Orser as Used car salesman

==External links==
* 
*  at Rotten Tomatoes

 
 