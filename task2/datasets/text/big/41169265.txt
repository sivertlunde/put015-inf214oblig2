Copenhagen (2014 film)
 
{{Infobox film
| name           = Copenhagen
| image          = Copenhagen film.jpeg
| director       = Mark Raso (director)
| producer       = Mauro Mueller Mette Thygesen
| writer         = Mark Raso Olivia Grant Mille Dinesen Baard Owe Tamzin Merchant
| music          = Agatha Kaspar
| cinematography = Alan Poon
| editing        = Mark Raso
| studio         = Fidelio Films Scorched Films
| runtime        = 
| country        = USA, Canada, Denmark
| language       = English
| budget         = 
| gross          = 
}}
Copenhagen is an independent US-Canadian coming-of-age story|coming-of-age adventure film. It had its world premiere as the opening narrative feature at the 20th anniversary edition of the Slamdance Film Festival 2014.  The film won the Grand Jury Prize at the Florida Film Festival  and Gasparilla Film Festival.

==Plot==
After weeks traveling through Europe, the immature William (Gethin Anthony) and his co-travelers arrive in Copenhagen. Copenhagen is not just another European destination for William; it is also his father’s birthplace, and William is set on finding his grandfather and delivering him a letter addressed from his own late dad. After Williams co-travelers abandon him to elope in London, a charming waitress at the hotel where William is staying named Effy (Frederikke Dahl Hansen) becomes interested in his search. Together they embark on a local, two-wheeled tour of the city, uncovering bit by bit his familys sordid past.

Effys youthful exuberance and wisdom challenge William, and he begins to fall for her and reconsider his antagonistic relationship with women. That is, however, until she confesses to being fourteen years old. That afternoon, William also learns, through an uncle Effy helps him find in Copenhagen, that his grandfather was a Nazi. When Williams co-traveler and best friend Jeremy returns from his unsuccessful trip to London, he scolds William over Effy and warns her against William. As the mutual attraction builds and William connects with someone for the first time in his life, he must deal with the fact that the first person with whom he shares love and understanding is half his age. William, a man whose growth was stunted by his fathers abandonment of him as a child, must learn to grow up by understanding his past, while the mature Effy must learn to be a child again.

In the end, William turns Effy away. She returns to school, and he takes the train to Skagen to find his grandfather.

==Cast==
*Gethin Anthony – William
*Frederikke Dahl Hansen – Effy
*Sebastian Armesto – Jeremy Olivia Grant – Jennifer
*Baard Owe – Uncle Mads
*Mille Dinesen – Effys Mother
*Martin Hestbæk – Henrik
*Tamzin Merchant – Sandra
*Preben Ravn – Thomas Vinter
*Sebastian Bull Sarning - Albert
*Gordon Kennedy – Uncle Peter
*Sune Kofoed – Receptionist Madsen
*Silja Eriksen Jensen – Signe
*Julie Christiansen – Berlin Girl
*Asbjørn Krogh Nissen – Ivan
*Zaki Nobel Mehabil – Bartender Markus
*Thomas Buttenschøn - Thomas Buttenschøn
*Miriam Yeager - School Teacher
*Kåre Fjalland - Priest
*Jane Pejtersen - Dane on Bridge	 
*Hélène Kuhn - Heather
*Mads Korsgaard - Hostel Bartender	

==Production==
The film was produced by Fidelio Films and Scorched Films. The film was shot in Copenhagen. It is the first feature film by Student Academy Award winner Mark Raso. The film was produced by Mauro Mueller and Mette Thygesen.

==Reception==
The film received generally positive reviews, and holds a 93% positive rating on Rotten Tomatoes.  Sheila OMalley, writing for Roger Ebert, gave it three out of four stars, saying "the film doesnt entirely work, but sometimes things that dont entirely work are interesting, bringing tension and unresolved issues that start to take over the film, almost by stealth. Its nice to watch something that isnt neat, that doesnt play it safe...  Copenhagen knows life is complex." 

==References==
 
 
http://filmmakermagazine.com/48072-marc-rasos-microbudget-production-diary-part-1/ 
http://filmmakermagazine.com/50735-mark-rasos-microbudget-production-diary-wrap-reflections/

==External links==
* 
* 

 
 
 
 
 
 
 


 