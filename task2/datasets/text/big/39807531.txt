Tenaliraman (film)
 
{{Infobox film
| name           = Tenaliraman
| image          = Tenaliraman 2014.jpg
| caption        = Theatrical release poster
| director       = Yuvaraj Dhayalan Kalpathi S. Aghoram Kalpathi S. Ganesh Kalpathi S. Suresh
| writer         = Yuvaraj Dhayalan
| starring       = Vadivelu Meenakshi Dixit
| music          = D. Imman
| cinematography = Ramnath Shetty
| editing        = Raja Mohammed
| studio         = AGS Entertainment
| distributor    = AGS Entertainment
| released       =  
| runtime        = 146 mins
| country        = India Tamil  Mandarin
| budget         =   
| gross          =   
}} Tamil historical fiction political satire comedy film directed by Yuvaraj Dhayalan of Potta Potti fame. Vadivelu stars in this film in dual roles. Meenakshi Dixit plays the heroine in the film. 

== Plot summary ==
The movie opens to a prologue, where King Parasbaram  (Radha Ravi), ruler of a rival kingdom and the nine ministers of the Kingdom of Vikata Nagaram are on their way to a Chinese consulates mansion. They are warmly welcomed by General Wang (Wilson Ng) and courtesan Hong (Vivienne Tseng) once they reach there. The Chinese consulate, headed by Chao Gong Gong (Nickson Cheng), wishes to engage in trade with the people of the Kingdom of Vikata Nagaram and reap the benefits. Thus, the ministers strike a deal with Parasbaram and the ministers to help them, promising them a huge share of the benefits. One of the ministers, Nandivarma Rayar (Joe Malloori) objects to this, saying that he doesnt want to betray the King and his people for money. Chao Gong Gong realising that he is of no use to the plan, has courtesan Hong kill him with a poisoned needle from her hair.

The film then introduces Tenaliraman (Vadivelu as the common man) who captures a gang of thieves (led by "Besant Nagar" Ravi) and teaches them a lesson. He then takes off for Vikata Nagaram where he meets the feisty Princess Maadhulai (Meenakshi Dixit), daughter of the King of Vikata Nagaram, and defeats her in an arm wrestling competition.

The films plot then shifts to the King (Vadivelu again, as the King of Vikata Nagaram), who is ruling over his kingdom along with his nine ministers, whom he claims to be the Navarathnams (nine jewels) of his court. The most interesting aspect is that the King has a rather large family comprising 36 wives and 52 children and  is confined mostly to his palace. He believes his people are happy based on the sayings and reports of his ministers. In reality, the people are being robbed and cheated by the corrupt ministers and are living in poverty. During his administration, he realizes the absence of Nandivarma Rayar and asks about it to his ministers, who reply that he had passed away. The King then declares that a new minister be appointed in Nandivarma Rayars stead. The guards then announce the news in public. Tenaliraman overhears the news and goes to the palace as a candidate for Nandivarma Rayars post.

After a display of his all-round talents (singing, dancing, wit and humour), Tenaliraman impresses the King and soon becomes an integral part of the Kings court, even closer to the King than the presiding ones. In the meantime, love blossoms between Tenaliraman and Princess Maadhulai. Tenaliraman, in reality, is a rebel who is sent by his comrades to kill the King, whom he believes at first, is responsible for the poverty of the people. Later, while having dinner with the King and the ministers, he discovers that it is the ministers who are at fault and not the King. He tries to warn his comrades of this, but his pigeon who carried the warning message, is captured by one of the ministers (Manobala). He informs the other ministers that Tenaliraman is a rebel and they twist Tenaliramans message to the rebels, saying that the King will be killed that night. Tenaliramans comrades believe the twisted message sent to them and raid the palace, only to have them and Tenaliraman get captured by the King. On learning of the conspiracy, the King banishes Tenaliraman from his kingdom.

After a few months, many changes have taken place in Vikata Nagaram. The Chinese have entered the fray and are trading with the Kingdom. Unbeknownst to the King, they are reaping the rewards for themselves, leaving the people even poorer. Parasbaram and the ministers get their share and are enjoying themselves. The King is now very sad thinking of Tenaliraman and misses him. Princess Maadhulai then tells him that Tenaliraman could not have done such a thing and comforts her father. He learns from Tenaliramans comrade, Kaaliappa (Bose Venkat) that the rebels were wrongly informed and that Tenaliraman was not at fault. The King goes and successfully brings back Tenaliraman to his Kingdom.

Tenaliraman then informs the King in private, that all the rules and regulations and schemes organised by the King on the trade with the Chinese are being carried out in such a way that it only benefits the Chinese and the ministers and not the people, who live in utter poverty, and since the King confines himself mostly to his palace and believes the words of his ministers and that he has lived a rich and prosperous life all along, he cannot mingle and live with the people of his Kingdom even for one day. The King then bets Tenaliraman that he can live with his people incognito for 10 days, and at the same time, find concrete evidence that the people are being robbed and cheated by the Chinese and the ministers. Leaving the care of his Kingdom to Tenaliraman and lying to his wives that he will be going to a nearby Kingdom for administrative purposes and for attending a feast, the King begins his mission.

During his mission, the King learns about the plight of his people from various persons. Soon, he comically finds himself in a hunger strike that is organised against him. When Tenaliraman meets the King in private and converses with him, one of the citizens (Devadarshini) overhears the conversation and discovers that it is the King himself who is among them incognito and reveals it to the citizens of Vikata Nagaram.

In the meantime, Tenaliraman is ruling in the Kings state with an Iron grip and because of this, Chao Gong Gong and Parasbaram fear that they might get caught in the process. Parasbaram learns from his spies that the King is in Vikata Nagaram and not elsewhere. He reveals it to Chao Gong Gong and they issue orders to find the King and kill him.

The King, with his citizens, have started a riot in Vikata Nagaram, but unfortunately, the King is captured by the Chinese. Parasbaram, who wanted the throne of Vikata Nagaram for himself, purposefully does not divulge to the Chinese that the King was among those captured until the corrupt ministers came to the consulate to say that the King is in Vikata Nagaram. The King revealed himself and everyone are dumb founded. Chao Gong Gong orders courtesan Hong to kill Parasbarm, because he purposefully failed to realise earlier that the person they caught was the King. The King is then imprisoned in a carriage and taken to a secluded place to be executed. But the leader of a Cannibal tribe stops them midway and orders the King to be handed over to them. Surrounded by Cannibals, Chao Gong Gong reluctantly releases the King. The King, initially frightened, asks the Cannibal who he is. The Cannibal is revealed to be none other than Tenaliraman. Chao Gong Gong uses his telescope and spots something fishy and the ministers and the Chinese traders find out, after General Wang fired at the "men" on the hilltop, that the "men" were just haystacks and that they have been duped. They then to go after the pair, but Tenaliraman, in his style, chases them out of Vikata Nagaram forever using the same bag of tricks that he used on the gang of thieves earlier.

The film then ends with new people replacing the posts of the corrupt ministers. Coincidentally, most of these new people were involved in comical encounters with the King during his 10-day plan. In the end, the King selects a post for himself, on Tenaliramans suggestion, as the child welfare minister and Princess Maadhulai is selected as the extra curricular activities minister.

The film signs off with the tagline "Iniya Payanam Thodarattum.....Dum!".

== Cast ==
* Vadivelu - Tenaliraman (The common man)/Maamannar (The King of Vikata Nagaram)   
* Meenakshi Dixit - Princess Maadhulai  
* Radha Ravi - King Parasbaram, ruler of a rival kingdom and the main antagonist   
* G. M. Kumar - Chief Minister, the head of the navarathnams (nine jewels)   
* Bala Singh - Naganadha, One of the ministers 
* Manobala - Education Minister      
* Shanmugarajan - One of the Ministers   
* Namo Narayana - Environmental Minister
* Krishnamoorthy - Health Minister
* Joe Malloori – Nandivarma Rayar  (Cameo Appearance)
* Jangiri Madhumitha - Kongana Valli, one of the 36 queens (Cameo Appearance) Rajesh - Eating Joint Owner
* Devadarshini – Eating Joint Owners Sister  
* Santhana Bharathi - Ex Chettiar/Man carrying bags of rice on cart  Mansoor Ali Khan – Cannibal tribe leader 
* Bose Venkat - Kaaliappa, Tenaliramans comrade
* Minnal Ravi - Royal Barber
* Kingkong - Street dancer/Beggar
* Scissor Manohar - Beggar who acts like a blind person
* Alwa Vasu - Candidate for Nandivarma Rayars post
* Muthu Kaalai - Vendor in the street
* "Besant Nagar" Ravi - Leader of the thief gang
* Baby Neha - One of Maadhulais Sisters
* Nickson Cheng - Chao Gong Gong, leader of the Chinese traders
* Vivienne Tseng - Courtesan Hong
* Wilson Ng - General Wang

== Production ==
Principal photography began on 20 May 2013 under the working title Jagajala Pujabala Thenaliraman.  Meenakshi Dixit was selected to play the female lead in this film.  The films first look was shown via stills on October 2013, much to the delight of Vadivelus fans, confirming that the shooting of the film was underway.  Portions of the film were also shot in China and Hyderabad,  but the news of shooting the film in China was however clarified by Vadivelu as untrue, "We had plans to go to China and shoot. But that didnt materialize because of time constraints. We, however, overcame that issue by getting Chinese actors from Singapore to come down here and shoot."  The scenes in the film have extensive CGI (computer-generated imagery) work. The work is about 95 minutes of the running time of the film. 

== Music ==
{{Infobox album
| Name        = Tenaliraman
| Type        = Soundtrack
| Genre       = Film soundtrack
| Artist      = D. Imman
| Producer    = D. Imman Tamil
| Sony Music
| Released    = 1 April 2014
| Last album  = Yennamo Yedho   (2014)
| This album  = Tenaliraman   (2014)
| Next album  = Oru Oorla Rendu Raja   (2014)
}}

D. Imman composed the music for the film and singer Shreya Ghoshal had sung a track penned by Viveka. The song was one of the high points in the film.  Apparently, Vadivelu himself had crooned a number as well and that he had done a terrific job in singing the song.  Singer Krishnaraj had also delivered a song.  Singer and actress Andrea Jeremiah had sung a Middle Eastern flavoured number and furthermore, Imman had confirmed that the soundtrack of the film will consist of 5 songs and 3 theme songs and that a 70s kind of feel would be present in each track.  Talking about the music of the film Imman posted on his Twitter page "Tenaliraman audio is designed to achieve the sound of early 60s and 70s. Melody making and orchestration is set to the period with the mix and master of todays digital sound. This films music did take me for a time travel and hope it does with the listeners too."  The audio of the film was launched on April Fools Day. Imman had tweeted, "The background score is in full swing, and nearing completion. Im sure u guys gonna love Vadivelu sir back on screen! Audio on April 1".  In addition to Viveka, Puthumaipithans lyrics were also used.   

The album received mixed reviews from critics, many of whom felt that the songs would look better on screen. Behindwoods gave 2/5 saying, "Immans usual magic is missing".  Only Kollywood gave it 2.75/5 praising Immans experimental songs, particularly "Aanazhagu", but finally stating that the songs overall were mediocre by Immans standards.  Therarefield gave a mixed review stating, "Tenaliraman captures the yesteryear ambience in its sounds to mixed results".  Cinemagrind gave 2.5/5 saying, "Staying true to the script, Imman has no freedom to experiment. Average vintage album!"  Nevertheless, songs like "Aanazhagu" and "Rampappa" became instant hits among the masses.  Indiaglitz wrote, "The usual Imman touch is missing". 

{{track listing
| headline        = Tracklist
| extra_column    = Singer(s)
| lyrics_credits  = yes

| title1        = Aanazhagu
| extra1        = Shreya Ghoshal
| lyrics1       = Viveka
| length1       =

| title2        = Aey Vaayadi
| extra2        = Krishnaraj
| lyrics2       = Viveka
| length2       =

| title3        = Udal Vaangalaiyo
| extra3        = Andrea Jeremiah
| lyrics3       = Viveka
| length3       =

| title4        = Nenje Nenje
| extra4        = Hariharasudan
| lyrics4       = Viveka
| length4       =

| title5        = Rampappa
| extra5        = Mukesh, Vadivelu
| lyrics5       = Pulamaipithan
| length5       =

| title6        = Aanazhagu (Karaoke)
| extra6        = 
| lyrics6       = 
| length6       =

| title7        =  Udal Vaangalaiyo (Karaoke)
| extra7        = 
| lyrics7       = 
| length7       =

| title8        =  Nenje Nenje (Karaoke)
| extra8        = 
| lyrics8       = 
| length8       = 
}}

== Release == United Kingdom. Sun TV. 

=== Promotion ===
The official 3-minute trailer for the film was released on 27 March 2014.     In its trailer review, Behindwoods wrote "However, for now, Vadivelu is back. Dum.". 

=== Marketing === Sun TV interview on Tamil New Years Day, which fell on 14 April 2014, the idea of the King having 36 wives and 52 children was based on a person called Ziona Chana, who lives in Baktawang village of the Mizoram state of India and who holds the world record for the worlds biggest family with 39 wives, 94 children, 14 daughters-in-law and 33 grandchildren, 181 in total.  Commenting on the film, the films director Yuvaraj spoke of how much faith Vadivelu has put in him and he efforts he made in the film, "There are several reasons for viewers to watch this film. The first is Vadivelu sir. My film will mark his return to cinema after a two-year break. He will be seen in dual roles and both roles will make you smile. To differentiate between the two characters he plays in the film, Vadivelu sir created a different body language for one of the characters. For instance, he created a different walking and talking style for a particular character. In fact, he would blink several times more while playing this character. It was amazing to see the way he transformed himself. Vadivelu sir received several offers, but he chose my film to mark his comeback and gave me the opportunity to work with him. I am just 27 years old. When some top heroes are scared to trust youngsters, he has shown that he has immense faith in the youth of this country." he said  Actress Meenakshi Dixit was aware of the fact that the film did not feature a regular hero, yet she agreed to work in the film because she didnt want to miss an opportunity to work with Vadivelu, "I was told by many people not to do this film but my heart was all for it. I was sure about one thing that maybe Im not working with a regular hero but I will not get an opportunity to work with a legendary comedian. Moreover, the movie concentrates more on the content rather than just the hero. Im very proud to be a part of such a prestigious project by a great production house. Working with him was an experience of a lifetime. I have learnt a lot from him. He is the most humble and grounded person I have ever met in my life. It was a challenge to match the comic timing of Vadivelu because he would improvise randomly on sets. I would have practiced my lines for hours and then when we do the scene, everything would have been improvised by Vadivelu. So, catching up with him was a challenge." she said  Furthermore, Vadivelu stated, "This is a very good film, one people must watch," he said and added, "I am sure they will like it. I wanted to mark my return with a film that would stand out. And I chose this script after listening to several others. We have added some imaginary events to short stories we know and have shot the film in a manner that will not hurt anybodys feelings or sentiments," he said. Pointing out to him that his director Yuvaraj was grateful that he had been entrusted with the task of directing a film that marked his return to cinema and he replied, "Todays youngsters are full of passion. This youngster was like camphor. If I began narrating a scene, he would complete it for me. He is sharp and has a bright future. He might be 26, but has the wisdom of a 62-year-old. Ive realized that one must never dismiss or underestimate anybody just because they are young. In fact, I have worked as an assistant director to him in this film. My producer has placed a lot of faith in me. Despite several people trying to dissuade him from making this film, he has backed me. I am grateful for the support he has extended." he said.  

=== Allegations of depiction of historical figures ===
Certain Telugu associations had written to the Censor Board seeking the removal of any scene that degrades Tenali Raman, the legendary minister in King Krishnadevarayas court. The organisations had petitioned the censor board to appoint an expert committee of Andhra historians to check the movie.  It was understood that he plays the roles of Tenali Raman and his King Krishnadevaraya. In his interview to a leading National newspaper, the films director, Yuvraj Dhayalan had clarified that Vadivelu had not acted as King Krishnadevaraya in his film. He also added that his film was not based on any real incidents and it was a summation of a few moral stories about Tenaliraman.  Commenting on the issue, the director of the film, Yuvaraj Dhayalan had said that he was ready to screen his film to any committee that had raised concerns and that they could very well ask him to show the film to them. He confirmed that there will be no scenes that will degrade Tenali Raman and that the Censor Board had looked into all the concerns posed by the Telugu associations and cleared the film with a U certificate without any cuts. He furthermore said that the film has been in the making for a very long time and people knew very well that Vadivelu was acting in it and that they should have raised this concern some six months ago and not around the time of the release of the film and that the intentions of the entire team of the film was not to hurt anyones sentiments.  Addressing the concerns raised by the Telugu groups, Vadivelu himself also stated that there was no mention of the Krishnadevaraya in the film and that the word "Manna" only was used to address the King throughout the film.  An official statement signed by R. Rangarajan, director of AGS entertainment, which produces the film, stated that the film is an imaginary story conceived on the basis of Tenaliramans moral stories and that the film doesnt reflect neither the life history of Tenaliraman nor that of any person with whom Tenaliraman was associated with/lived with and that the story of the film is an imaginary one reflecting the lifestyle of people who lived in the 15th century in the backdrop of the present happenings and events and that all the characters appearing in the film are fictitious.  The Telugu groups moved to the High Court on Friday and also sought the intervention of the Tamil Nadu government. On Friday, two Telugu organisations filed a writ petition in the Madras High Court seeking a direction to the Tamil Nadu Government to pass appropriate orders on their representation with regard to the film. The petitioners, Tamil Nadu Telugu Makkal Peravai and Tamil Nadu Telugu Yuva Sakthi, said that the films producer had not considered the facts about King Krishnadevaraya and that they had produced the movie with its own ideology. "History should not be twisted;" the petitioners said. At a press meet in the city, representatives of various Telugu organisations said they were also attempting to make a representation to the Chief Secretary seeking a halt to the release of the film. "Tenaliraman is a historic character who was a poet in the court of Krishnadevaraya, a great king. But the trailer of this film shows the king as a person who has many wives and children, and ignored his subjects." Thangavel Swamy of Sillavar Rajakambalam Naicker Sangam, one of the protesting groups, said.  The petition also stated to the films producers to screen the movie to its representatives or authorities appointed by the court before the movies release so as to be sure of what the movie contained.  The petition was taken up for hearing on 11 April 2014 by Jusitce Satish Kumar Agnihotri (acting Chief Justice) and Justice M. M. Sundaresh. Justice Sathish Kumar Agnihotri said that since he did not know Tamil, he could not judge about the contents of the film. Hence he had posted the petition to another division bench. 

=== Clearing of the film ===
On 16 April 2014, a division bench consisting of Justices N. Paul Vasanthakumar and M. Sathyanarayanan refused to stall the release of the film but asked its producers to prominently display a disclaimer at the beginning of the film stating that it was a fictional work and that it was not intended to hurt the sentiments of religious or any other groups. Earlier, the producer had filed an affidavit stating that there was no mention of King Krishnadevaraya anywhere in the movie and pointed out that the Madurai bench of the high court had already dismissed a similar PIL.   

=== Support for the film ===
Director and political activist Seeman (director)|Seeman,   director V. Gowthaman,  and director Bharathiraja supported the film,  terming the allegations as systematic attacks to prevent the films release.  

== Reception ==

=== Critical reception ===
Tenaliraman received mixed reviews. Hindustan Times gave 2/5 and wrote, "Vadivelu does make an effort to look and sound different in the two roles, varying his body language – to appear dignified as the king and comical as the jester" and concluded that, "Tenaliraman is meant for Vadivelu fans and is a good movie for the summer."  S. Saraswathi of Rediff gave 2/5 and wrote, "Performance wise, there is little scope for anyone other than Vadivelu. Though the film is set in the 15th century, the issues dealt with are quite contemporary such as foreign direct investment, rampant corruption and bribe-giving and receiving. Director Yuvaraj has tried to present a socially relevant issue in a humorous manner."  Sify wrote, "On the whole, Tenaliraman is a perfect family outing this summer, as some laughs are guaranteed. Watch it for Vadivelu!".  The Deccan Chronicle gave 3/5 stars and wrote, "Clearly the film rests entirely on Vadivelus shoulders. He delivers a noteworthy performance both as the king and as Tenali, but it is the character of Maamannar that draws the most laughs from the audience."  Oneindia Entertainment gave 3/5 stars, stating, "Tenaliraman is a complete family entertainer. It is a paisa-vasool film.".  The Times of India gave 2.5/5 stars and wrote, "Tenaliraman is amusing, especially if you like loud comedy."  The New Indian Express wrote, "Vadivelu uses suitable body language and expressions to demarcate the two roles – of a simpleton king, unaware of the real state of his people and of the scheming by his ministers; and of the witty shrewd Tenali who brings the king to his senses. What makes the film watchable is Vadivelus screen presence."  Baradwaj Rangan of The Hindu wrote, "Vadivelus timing is intact. He has a terrific scene where he spouts gibberish, unable to put together words to describe his plight."  Siddarth Srinivas of Cinemalead gave 2.5/5 stars and wrote, "Copacetic period comedy highlighted by Vadivelus performance."  Indiaglitz gave 2.5/5 stars and concluded, "Tenaliraman is an enjoyable light-hearted entertainer."  Behindwoods gave 2.5/5 stars and wrote, "In all, Tenaliraman, as a standalone film, may find patrons in family audiences and children and may come across as a welcome break for this summer vacation."  Moviecrow gave 2.75/5 and concluded, "Tenaliraman is mounted on a political platform and successfully gets away without setting off land mines. However, Tenaliraman looks like a poor cousin of Imsai Arasan and lacks some genuinely interesting moments and necessary sting that is quintessential to political satires. Nevertheless, Tenaliraman is watchable only for Vadivelu!". 

=== Box office ===
The film collected 7.6&nbsp;million in Chennai alone during its first week of release (from 18 April 2014 to 20 April 2014).  It collected 9.5&nbsp;million in its second week (from 21 April 2014 to 27 April 2014).  The film collected 3.2&nbsp;million in its third week (from 28 April 2014 to 4 May 2014).  The films collected 0.6&nbsp;million in its fourth week (from 5 May 2014 to 11 May 2014), therefore taking the box office tally in Chennai to 20.9&nbsp;million.  The final verdict by Behindwoods was "Average" and that the movie has been accorded a decent welcome. 

== References ==
 

== External links ==
*  
*  

 
 
 
 
 