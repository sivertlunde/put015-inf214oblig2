His Private Life (1928 film)
{{Infobox film
| name           = His Private Life
| image          = His Private Life poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Frank Tuttle
| producer       = Jesse L. Lasky Adolph Zukor
| screenplay     = Ethel Doherty George Marion Jr. Keene Thompson Ernest Vajda 
| starring       = Adolphe Menjou Kathryn Carver Margaret Livingston Eugene Pallette André Cheron Sybil Grove
| music          = 
| cinematography = Henry W. Gerrard 
| editing        = Verna Willis 	
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 comedy silent film directed by Frank Tuttle and written by Ethel Doherty, George Marion Jr., Keene Thompson and Ernest Vajda. The film stars Adolphe Menjou, Kathryn Carver, Margaret Livingston, Eugene Pallette, André Cheron and Sybil Grove. The film was released on November 17, 1928, by Paramount Pictures. {{cite web|url=http://www.nytimes.com/movie/review?res=950CE0DF133FE33ABC4A52DFB7678383639EDE|title=Movie Review -
  Show People - THE SCREEN; Hollywood Life Satirized. The Alluring Blonde. Fun and Drama. A Spy Yarn. LORELEI ON THE SCREEN. "Women Without Men" a Story With Suicide as the Climax. Other Photoplays. - NYTimes.com|work=nytimes.com|accessdate=13 February 2015}}  
 
==Plot==
 

== Cast ==
*Adolphe Menjou as Georges St. Germain
*Kathryn Carver as Eleanor Trent
*Margaret Livingston as Yvette Bérgere
*Eugene Pallette as Henri Bérgere
*André Cheron as Maurice
*Sybil Grove as Maid
*Paul Guertzman as Stupid boy
*Alex Melesh as Salesman
*Alex Woloshin as Hotel clerk 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 