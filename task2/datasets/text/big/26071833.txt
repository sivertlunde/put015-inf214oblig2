Ghost Parade
 

{{Infobox film
| name           = Ghost Parade
| image          =
| image_size     =
| caption        =
| director       = Mack Sennett
| producer       = Mack Sennett (producer)
| writer         = Harry McCoy (writer) Stuart E. McGowan (writer) Arthur Ripley (writer) Earle Rodney (writer) Sydney Sloan (writer) Gene Towne (writer) G. Trano (writer) John A. Waldron (writer)
| narrator       =
| starring       =
| music          =
| cinematography = Frank B. Good George Unholz
| editing        = William Hornbeck
| distributor    =
| released       = 24 May 1931
| runtime        = 18 minutes 20 minutes (American 2005 DVD release)
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}

Ghost Parade is a 1931 American short subject film directed by Mack Sennett.

==Plot summary==
A disparate group of characters are menaced in a haunted house.

==Cast==
*Harry Gribbon as The Constable
*Andy Clyde as Mr. Martin
*Marjorie Beebe
*Frank Eastman
*Marion Sayers
*Babe Stafford
*Charles Gemora as Gorilla

==Soundtrack==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 


 