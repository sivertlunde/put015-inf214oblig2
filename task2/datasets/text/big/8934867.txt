Love Is a Fat Woman
{{Infobox film
| name           = Love Is A Fat Woman
| image          = Gorda87poster.jpg
| image_size     =
| caption        =
| director       = Alejandro Agresti
| producer       = Cesar Maidana
| writer         = Alejandro Agresti
| narrator       =
| starring       = Elio Marchi Sergio Poves Campos
| music          = Paul M. van Brugge
| cinematography = Néstor Sanz
| editing        = René Wiegmans
| distributor    =
| released       = September 23, 1987
| runtime        = 82 min.
| country        = Argentina Netherlands
| language       = Spanish
| budget         =
}} Argentine drama film written and directed by Alejandro Agresti.

== Synopsis ==
José is a young journalist who gets fired over refusing to write an article about an American film crew, overdramatizing the situation, in Argentina. When he goes looking for his old girlfriend, he runs into serious difficulties with the crew again.

== Cast ==
* Elio Marchi ... José
* Sergio Poves Campos ... Caferata
* Carlos Roffé
* Mario Luciani
* Enrique Morales
* Harry Havilio
* Tito Haas
* Christian Cardozo
* Federico Peralta Ramos
* Theodore McNabney (as Theo McNabey)
* Sergio Lerer
* Ernesto Ciliberti
* Silvina Chaine ... Woman on the Train
* Stella Fabrizzi
* José Glusman
* Norma Graziosi
* Silvana Silveri

== Planned sequel == Irish men, Darren ONeill and Colm Rowan. It is said that after a week socialising and drinking with the two, he became inspired to make "Loving Fat Women" a sequel to his original movie. In it, he recaps the exploits of the Irish pair on his visit to Ireland and the many hefty women they encountered on their soirees. Already rated "R" it has captivated audiences in pre-screening for its explicitness and the braun of the two lead characters (Colm and Darren) in their search of the next "big" prize (I.e. Fat woman). Original titles considered for the film were "Fat Loving Criminals" and "Larger Than Life".

== External links ==
*  
*  

 

 
 
 
 
 
 
 

 
 