Hurry, Charlie, Hurry
{{Infobox film
| name           = Hurry, Charlie, Hurry
| image          = 
| alt            = 
| caption        = 
| director       = Charles E. Roberts
| producer       = Howard Benedict 
| screenplay     = Paul Girard Smith Luke Short
| starring       = Leon Errol Mildred Coles Kenneth Howell Cecil Cunningham George Watts
| music          = 
| cinematography = Nicholas Musuraca
| editing        = George Hively
| studio         = RKO Pictures
| distributor    = RKO Pictures
| released       =  
| runtime        = 65 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Hurry, Charlie, Hurry is a 1941 American comedy film directed by Charles E. Roberts and written by Paul Girard Smith. The film stars Leon Errol, Mildred Coles, Kenneth Howell, Cecil Cunningham and George Watts. The film was released on June 13, 1941, by RKO Pictures.   

==Plot==
 

== Cast ==
*Leon Errol as Daniel Jennings Boone
*Mildred Coles as Beatrice Boone
*Kenneth Howell as Jerry Grant
*Cecil Cunningham as Mrs. Diana Boone
*George Watts as Horace Morris
*Eddie Conrad as Wagon Track
*Noble Johnson as Chief Poison Arrow Douglas Walton as Michael Prescott
*Renee Godfrey as Josephine Whitley 
*Georgia Caine as Mrs. Georgia Whitley
*Lalo Encinas as Frozen Foot

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 


 