20 Years of Dysfunction
{{Infobox film
| name           = 20 Years Of Dysfunction
| image          = 
| alt            =  
| caption        = 
| director       = Billy Milano
| producer       = Billy Milano
| starring       = Stormtroopers Of Death
| music          = Stormtroopers Of Death
| editing        = 
| studio         =  Nuclear Blast America
| released       =  
| runtime        = 183 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
20 Years Of Dysfunction is a DVD/CD released in 2005 by crossover thrash band Stormtroopers Of Death. Mainly created by singer Billy Milano, the material for the 2-disc DVD/CD is mostly culled from fan-shot amateur footage. The relative scarceness of available material has resulted in some hilarious decisions by the production team; for instance, Milk is sung by Danny Lilker but the video footage inappropriately shows vocalist Milano singing. Also notable is that the DVD does not include any material from the second Stormtroopers Of Death studio album Bigger Than The Devil.

The DVD extras include five mini concerts from various performances, "backstage shenanigans", interviews with S.O.D. members, several never before heard ballads and a for-this-DVD-made video for the track Fuck The Middle East. Also included is the 1985 bootleg Pussywhipped in its entirety.

==Soundtrack==

===DVD===
#"Intro - March of the S.O.D."
#"Sgt D & The S.O.D."
#"Shenanigans 1"
#"Kill Yourself"
#"About Billy Milano"
#"Milano Mosh"
#"Shenanigans 2"
#"Speak English or Die"
#"Shenanigans 3"
#"Chromatic Death"
#"Fist Banging Mania"
#"About Scott Ian"
#"Pi Alpha Nu"
#"Shenanigans 4"
#"Ballads"
#"No Turning Back"
#"Shenanigans 5"
#"Fuck the Middle East"
#"Douche Crew"
#"About Danny Lilker"
#"Milk"
#"Shenanigans 6"
#"Freddy Krueger"
#"About Charlie Benante"
#"Pussywhipped"
#"Shenanigans 7"
#"United Forces"

===Audio CD: Pussywhipped, aka. Bootlegging the Bootlegger===
#"Diamonds and Rust" (0:31)
#"Whats That Noise" (1:18)
#"March of the S.O.D." (4:32)
#"Fist Banging Mania" (2:20)
#"Milano Mosh" (2:08)
#"Diamonds and Rust" (0:43)
#"Milk" (2:25)
#"Pi Alpha Nu" (1:43)
#"Ram It Up Your Cunt" (2:21)
#"United Forces" (2:11)
#"No Turning Back" (1:26)
#"Kill Yourself" (3:45)
#"Momo" (0:24)
#"Speak English or Die" (2:55)
#"Anti-Procrastination Song" (0:26)
#"Fuck the Middle East" (0:25)
#"Douche Crew" (2:27)
#"Vitality (Milk, Pt. 2)" (0:49)
#"Pre-Menstrual Princess Blues" (1:43)
#"Diamonds and Rust" (0:24)
#"Chromatic Death" (1:32)
#"Ballad of Jimi Hendrix" (0:16)
#"Ballad of Jimi Hendrix" (0:15)
#"Ballad of Jimi Hendrix" (0:39)
#"Freddy Kreuger" (3:07)
#"Not!" (0:38)

==The Mini-concerts==
#"Best of Sod Live in: European Festivals"
#"Best of Sod Live in: USA"
#"Best of Sod Live in: Germany"
#"Best of Sod Live in: Holland"
#"Best of Sod Live in: Japan"

==Extra Features==
#"9/11 Tribute"
#"Alex Perialass Interview"
#"Billys Interview With Liz From Metal Maniacs"
#"Fuck The Middle East Video"
#"March Of The S.O.D./Sgt D & The S.O.D. Video"

==References==
 

 
 

 