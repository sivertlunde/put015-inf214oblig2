The Strange Case of Mary Page
 
{{Infobox film
| name           = The Strange Case of Mary Page
| image          = The Strange Case of Mary Page.jpg
| caption        = Film poster
| director       = J. Charles Haydon
| producer       =
| writer         =
| starring       = Henry B. Walthall Edna Mayo
| cinematography =
| editing        = Essanay Company
| released       =  
| runtime        = 15 episodes
| country        = United States
| language       = Silent (English intertitles)
| budget         =
}}
 drama film serial directed by J. Charles Haydon. The film is considered to be lost film|lost.   

==Cast==
   and Edna Mayo]]
* Henry B. Walthall as Phil Langdon, Attorney
* Edna Mayo as Mary Page
* Sidney Ainsworth as David Pollock
* Harry Dunkinson as E.H. Daniels, Show Manager
* John Cossar as Prosecuting Attorney
* Frank Dayton as Dan Page
* Frances Raymond as Mrs. Page
* John Thorn as Jim Cunningham
* Arthur W. Bates as Young Gambler
* Edmund Cobb
* Frank Hamilton
* Frank Benedict (as Francis Benedict)
* William Chester
* Virginia Valli (as Miss Valli)
* Thomas Commerford as Judge
* Jessie Jones
* Honus Smith Edward Arnold as Dr. Foster (Ch. 5)
* Richardson Cotton as Juror (Ch. 5)
* Ernest Cossart

==See also==
* List of film serials
* List of film serials by studio
* List of lost films

==References==
 

==External links==
 
*  
*  

 
 
 
 
 
 
 
 
 
 


 