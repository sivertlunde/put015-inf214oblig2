Harriet the Spy (film)
 
{{Infobox film
| name           = Harriet the Spy
| image          = Harriet the Spy (1996 film) poster.jpg
| caption        = Theatrical release poster
| director       = Bronwen Hughes
| producer       = Marykay Powell
| screenplay     = Douglas Petrie Theresa Rebeck Greg Taylor Julie Talen
| based on       =  
| starring       = Michelle Trachtenberg Rosie ODonnell 
| music          = Jamshied Sharifi
| cinematography = Francis Kenny
| editing        = Debra Chiate
| studio         = Nickelodeon Movies Rastar
| distributor    = Paramount Pictures
| released       =  
| runtime        = 102 minutes
| MPAA rating    = PG
| country        = United States
| language       = English
| budget         = $13 million
| gross          = $26,570,048 (domestic)
}}
 novel of the same name by Louise Fitzhugh, and starring Michelle Trachtenberg (in her film debut) as the title character.
 directing debut), produced by Paramount Pictures, Nickelodeon Movies and Rastar. It was the first film that was produced under the Nickelodeon Movies banner, and the first of two film adaptations of the Harriet the Spy books. In theaters, the remake pilot episode of Hey Arnold! from 1996 was shown before the film and received a PG rating from the MPAA.

The film was shot in Fort Lauderdale and Miami, Florida, and Toronto, Ontario, Canada. 

==Plot== Gregory Smith) and Janie Gibbs (Vanessa Lee Chester). She lives a privileged life with her parents and her nanny, Katherine "Ole Golly" (Rosie ODonnell), whos the only person who knows all the things that Harriet has been snooping on. Harriet and her friends are enemies with mainly Marion Hawthorne (Charlotte Sullivan). For a while, Harriet lives life very well with being a spy and having fun with Golly.

One night, being home alone with Harriet, Golly invites a friend, Mr. George Waldenstein, over. And after Golly accidentally burns their dinner, the three go out to dinner and a movie instead, where things turn into a disaster. Mrs. Welch fires Golly for letting Harriet stay out late, but then she realizes that she still needs her to look after Harriet and begs her to stay. Golly tells her, however, that she was planning to leave soon since she believes that Harriet is old enough to take care of herself, much to everyones protests. Shortly before she leaves, Golly encourages Harriet to never give up on her love for observing people just because shell no longer be with her, and promises her that she will be the first to buy her very own autographed copy of Harriets first novel she sells in the future. After Harriet bids Golly goodbye, she becomes depressed and withdrawn. She even gets caught when investigating the home of Agatha K. Plummer (Eartha Kitt).

The next day, she plays with her friends at the park, and disaster strikes. Marion finds Harriets private notebook and begins reading all of Harriets vindictive comments on her friends out loud, such as how she suspects Janie "will grow up to be a nutcase", and mocking Sports father for barely earning any money. Everyone finds that theyre all cruel and hurtful, and even Sport and Janie turn their backs on Harriet. The kids then create a Spy-Catcher club and torment Harriet on her spy routes.  

After running into a police officer, and then getting zeroes on her schoolwork, Harriet gets her notebook taken away by her parents. Her parents tell Harriets teacher Miss Elson (Nancy Beatty) to search Harriet every day for notebooks, much to Harriets embarrassment. One day, during art, Marion Hawthorne "accidentally" pours blue paint all over Harriet, who does things to get back at everyone individually, then exposes to them that Marions father left her because he never loved her.

Harriets parents find out what she has done to her classmates and send her to be evaluated by a psychologist, who assures them that Harriet is fine. Then things start to get better again. Harriet gets her notebook back, and she even gets a surprise visit from Golly, who tells her that in order to make things right again, she has to do two things: apologize and lie. Harriet then tries to apologize to Sport and Janie, even though they reject her at first (they later, however, get tired of being treated unfairly in Marions bully group and quit). She also shares her opinion with Miss Elson about how getting appointed as the editor of the sixth grade paper wasnt being done in a fair way, and she get selected herself by her classmates, who get Marions occupation as the editor voted out. Through one article, she apologizes to everyone, including Marion, and all (except Marion) accept her apology. All is well. On opening night of the 6th grade pageant, Janie, Sport and Harriet light off a stink bomb as revenge on Marion and dance until the end of the film.

==Cast==
* Michelle Trachtenberg as Harriet M. Welsch
* Rosie ODonnell as Katherine "Ole Golly"  Gregory Smith as Simon "Sport" Rocque
* Vanessa Lee Chester as Janie Gibbs
* J. Smith-Cameron as Mrs. Welsch
* Robert Joy as Mr. Welsch
* Eartha Kitt as Agatha K. Plummer
* Charlotte Sullivan as Marion Hawthorne
* Teisha Kim as Rachel Hennessy
* Cecilley Carroll as Beth Ellen Hansen
* Dov Tiefenbach as Boy with Purple Socks
* Nina Shock as Carrie Andrews
* Connor Devitt as Pinky Whitehead
* Alisha Morrison as Laura Peters
* Nancy Beatty as Miss Elson
* James Gilfillan as Archie Simmons
* Gerry Quigley as Sports Dad
* Jackie Richardson as Janies Mother
* Roger Clown as Dr. Wagner

==Box office and release== US theaters on July 10, 1996, and the film grossed $6,601,651 on its opening weekend, averaging about $3,615 per each of the 1,826 screens it was shown on.  The film went on to gross a total of $26,570,048 by November 10, 1996, and was considered a box office success, earning back double its $13,000,000 budget.

===Home media release===
Harriet the Spy was released onto VHS on February 25, 1997. The film was later released on DVD on May 27, 2003, with no special features whatsoever.

==Reception==
The film has received mixed reviews from critics and it currently has a 48% "rotten" rating on Rotten Tomatoes.

==Awards and nominations==
{| class="wikitable"
|-
! Year !! Award !! Category !! Winner/Nominee !! Result
|- 1997
|1997 Kids Choice Awards Favorite Movie Actress Rosie ODonnell
| 
|- Young Artist Awards Best Performance in a Feature Film - Leading Young Actress Michelle Trachtenberg
| 
|- Best Performance in a Feature Film - Supporting Young Actress Vanessa Lee Chester
| 
|- Best Family Feature - Drama
|
| 
|- Best Performance in a Feature Film - Supporting Young Actor Gregory Smith Gregory Smith
| 
|-
|}

==Sequel==
A direct-to-TV sequel was released in 2010 entitled  , with Jennifer Stone from Wizards of Waverly Place replacing Michelle Trachtenberg in the title role.

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 