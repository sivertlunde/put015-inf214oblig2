Requiescant
 
{{Infobox film
| name           = Requiescant
| image          = Requiescant.jpg
| caption        = Original Italian Poster
| director       = Carlo Lizzani
| producer       = Carlo Lizzani Ernst R. von Theumer
| writer         = Lucio Battistrad Andrew Baxter Adriano Bolzoni Armando Crispino Denis Greene Edward Williams
| starring       = Lou Castel Mark Damon Pier Paolo Pasolini Barbara Frey
| music          = Riz Ortolani
| cinematography = Sandro Mancori
| editing        = Franco Fraticelli 
| distributor    =
| released       =  
| runtime        =
| country        = Italy Italian
| gross          =
}}

Requiescant (aka Kill and Pray) is a 1967 spaghetti western directed by Carlo Lizzani. 

==Synopsis==
After surviving his family being massacred, a young boy is taken in and raised by a preacher. Years later he comes face to face with the man that killed his family and he is tempted back into violence.

==Selected cast==
* Lou Castel: Requiescant
* Mark Damon: George Bellow Ferguson
* Pier Paolo Pasolini: Don Juan
* Ninetto Davoli: El Nino
* Franco Citti: Burt
* Barbara Frey: Princy
* Lorenza Guerrieri: Marta

==Releases==
Wild East released a limited edition region 0 NTSC DVD on 1 November 2004, preserving the films original widescreen aspect ratio. The DVD has the English title Kill and Pray on the box art but the title on the print used for the DVD transfer is the original Italian Requiescant title. The 2004 DVD is currently out of print, but the film was re-released under the title Kill and Pray in another limited edition R0 NTSC DVD in 2011 alongside Dead Men Dont Count, also starring Mark Damon.

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 

 
 