The Sons of Great Bear
{{Infobox film
| name           = The Sons of Great Bear
| image          = Sons of Great Bear.jpg
| image size     = 250px
| border         = 
| alt            = 
| caption        = 1966 poster of The Sons of Great Bear
| director       = Josef Mach
| producer       = Hans Mahlich	
| screenplay     = Margot Beichler Hans-Joachim Wallstein 
| based on   =  
| starring       = Gojko Mitić
| music          = Wilhelm Neef
| cinematography = Jaroslav Tuzar
| editing        = Ilse Peters
| studio         = Deutsche Film-Aktiengesellschaft Bosna Film Sarajevo
| distributor    = PROGRESS-Film Verleih
| released       =  
| runtime        = 92 minutes
| country        = German Democratic Republic
| language       = German
| budget         = 
| gross          = 4,800,000 East German Mark (GDR; 1966)
}}
 Western film, Czechoslovak filmmaker Yugoslav actor Native Americans, Whites as antagonists. It is one of the most successful pictures produced by the DEFA studio.

==Plot==
In 1874, the United States Government|U.S. government encroaches on the lands of the Lakota people. Mattotaupa, an Oglala Lakota man, gambles with Red Fox, a White criminal, in a saloon. When seeing he has gold, Red Fox demands to know its origin. Mattotaupa refuses, and Red Fox murders him. Mattotaupas son, the young and fierce warrior Tokei-ihto who mistrusts the Whites and never drinks their Brandy|"Firewater", witnesses the murder.

Two years later, Tokei-ihto is the war chieftain of the Oglalas Bear Band and one of Crazy Horses commanders in the Great Sioux War. He raids a resupply column sent to a United States Army|U.S. Army fort, but brings the commanders daughter Katie Smith to her father unharmed, requesting to negotiate peace. Major Smith turns him down, and one of his officers tries to shoot the chieftain, who then surprises the soldiers and single-handedly destroys their munitions depot.

The warriors return to their camp victorious. Red Fox arrives, offering a peace treaty. Tokei-ihto is sent to negotiate. Smith demands they settle in a reservation. When Tokei-ihto rejects the offer, he is imprisoned. The Bear Band are brutally forced to resettle.

After several months, the war has ended. Red Fox enters Tokei-ihtos cell, trying to force him to reveal the location of the gold mine. Katie Smith and her friend Adams stop him before he begins to torture the captive. They recommend that after being released, he should take his people to Canada. The chieftain accepts the governments terms and returns to the reservation.

Tokei-ihto ventures to the sacred caves of the Great She-Bear, the Bands Totem animal, which are also the golds source, to seek the blessing of the spirits to their departure. Red Foxs henchman Pitt goes after him, hoping to find gold. He stumbles upon a she-bear which kills him, though not before he fatally wounds her with a gunshot. Tokei-ihto finds the she-bears cub. He takes it back to the tribe.

The Bear Band leave. They raid an army column, and General George Crook offers a United States dollar|$200 reward on the chieftains head. Red Fox, still seeking the gold, sets after them with a large group of ruffians. They gain on the Lakota as they cross the Missouri River. Red Fox agrees to let the people move on undisturbed if Tokei-ihto would remain behind. The chieftain agrees. After a prolonged fight, he manages to kill Red Fox. The band settle on the other bank, finding refuge.

==Cast==
 
* Gojko Mitić as Tokei-ihto (voiced by Karl Sturm)
* Jiří Vršťala as Jim Fred Red Fox Clark (voiced by Fred Düren)
* Hans Finohr as Hawandschita
* Günter Schubert as Feldger
* Hannjo Hasse as Pitt
* Horst Kube as Thomas
* Henry Hübchen as Hapedah
* Adolf Peter Hoffmann as Mattotaupa
* Sepp Klose as Crazy Horse|Tȟašúŋke Witkó
* Franz Bonnet as Crazy Horse the elder
* Kati Székely as Uinonah (Winúŋna)
* Rolf Römer as Tobias
* Hans Hardt-Hardtloff as Major Samuel Smith
* Gerhard Rachold as Lieutenant Roach
* Horst Jonischkan as Adams
* Jozef Adamovič as Chapa (voiced by Klaus Bergatt)
* Blanche Kommerell as Eenah
 
* Milan Jablonský as Mountain Thunder (voiced by Lothar Schellhorn) 
* Helmut Schreiber as Ben
* Rolf Ripperger as Joe
* Brigitte Krause as Jenny
* Karin Beewen as Cate Smith
* Ruth Kommerell as Tashina
* Zofia Słaboszowska as Mongshongsha (voiced by Ursula Mundt)
* Slobodanka Marković as Sitopanaki (voiced by Gertrud Adam) 
* Jozef Majerčík as Chetansapa (voiced by Ezard Haußmann)
* Martin Ťapák as Shonka (voiced by Horst Manz)
* Walter E. Fuß as Theo
* Jozo Lepetić as Bill (voiced by Horst Schön)
* Herbert Dirmoser as Old Crow
* Willi Schrade as Tatokano
* Dietmar Richter-Reinick as Lieutenant Warner
 

==Production==

===Background=== Old West Native Americans while glorifying the White settlers, making him a promoter and a precursor of an expansionist ideology of "blood and soil" and   in the words of Klaus Mann   the "Cowboy mentor of the Adolf Hitler|Führer". Lehman‏, Grieb. p. 120. 

East Germany had its own writers who dealt with the subject, though. The Munich-born Liselotte Welskopf-Henrich composed a first draft of a novel set in the land of the Sioux in 1918, when she was 17 years old. After the end of the Second World War, Henrich joined the Communist Party of Germany and settled in East Berlin, where she held a tenure as a Classics professor in the Humboldt University of Berlin. Mcnab, Lischke. p. 288.  She finished her book only in 1951. In the following year, it was published under the title The Sons of Great Bear, gaining wide success among children and youth. It became the basis for an eponymous hexalogy of novels about the adventures of Harka, an Oglala Lakota who would eventually be given the name Tokei-ihto. Henrichs books were written from the Native American perspective, and she studied Lakota culture extensively to convey an authentic depiction of them, and even conducted several tours to the United States and Canada to live among their tribes. Until 1996, her books were translated to 18 languages and sold 7.5 million copies worldwide. 

===Inception===
The attitude of the DEFA studio the countrys state-owned cinema monopoly   to Westerns changed in the early 1960s, with the release of a wave of Karl May films produced in West Germany, especially the series directed by Harald Reinl which began with Apache Gold in 1963. While not distributed in the GDR, East German tourists could view them when vacationing in the neighboring Czechoslovakia. The extensive popularity of those pictures convinced DEFA officials to make their own version.  Producer Hans Mahlich one of the veteran members of the studio who participated in the making of some of DEFAs most widely known pictures, like Castles and Cottages and The Sailors Song was the first to promote the idea, arguing that such films would become a great commercial success. Habel. p. 7.  While seeking to exploit the popularity of the subject, the studio directors had to consider the ideological demands of the establishment, which would have viewed a typical Western as a reactionary and a "bourgeoisie" form of art Bergfelder. p. 203.  – indeed, Der Spiegel reported that several years earlier, an attempt to turn Henrichs novels into a film was "vetoed by the party, which negatively deemed the Westerns as a capitalistic  influence that would encourage criminality."  Therefore, they sought to imbue the planned picture with themes which would suit state officials and turn it into "politically correct entertainment": Gemünden. p. 244.  Henrichs novels, with their emphasis on the negative portrayal of the white colonists and their already established popularity with the audience, were selected to be adapted to the screen.  Studio dramaturgue Günter Karl wrote: "We knew we had to set ourselves apart from the capitalist movies of the genre... But we were nevertheless forced to use at least part of the elements that make this genre so effective."  Lieselotte Welskopf-Henrich told an interviewer that "in their themes, the West German Karl May films lag far behind good American pictures in which the Native American and his right to self-defense were already recognized. We attempted to make a new sort of an Indian film." 

===Development=== McCarthy era.  Major Samuel Smith clearly wears the rank of a colonel; a regimental flag in his headquarters carries the inscription "United States of Amerika", and a map shows the U.S.-Mexican Border as it was before the Treaty of Guadalupe Hidalgo, with Mexico stretching all the way to Oregon.  Henrich, who served as the producers adviser, was "fanatic about details": when several horses refused to be mounted without a saddle and had to be fitted with one, she resigned her post, insisting that Native Americans rode bareback.   The author later demanded that her name be removed from the credits, though it remained there.  She refused to allow any more of her books to be adapted for the screen after The Sons of Great Bear, insisting "too many liberties were taken". 

Principal photography for The Sons of Great Bear was conducted in summer 1965, and took place in the Socialist Republic of Montenegro and in the Elbe Sandstone Mountains.  It took about two and half months, and the sum expended on making the picture was "over 2 million East German Marks". 

==Reception==

===Distribution===
The Sons of Great Bear turned into an instant success upon its release, owing also to two external factors: the first was its appeal to young audiences, as many East German children were already acquainted with Henrichs books;  and another was the low supply of new pictures for 1966: over half of DEFAs productions intended to be released during the year   12 out of 21, most prominently Kurt Maetzigs The Rabbit Is Me and Frank Beyers Trace of Stones were banned as a result of the XI Plenum of the Socialist Unity Party of Germany which took place between the 16th and 18 December 1965,   in which the allegations of rising Politburo member Erich Honecker, who blamed the cinema industry for promoting values incongruous with Marxism, were widely accepted by state functionaries. Machs film was one of those deemed innocuous enough to remain unscathed by censures. 
 Federal Republic rapprochement with the East German government. König. p. 60.  In the Czechoslovak Socialist Republic, it was seen by 1,737,900 viewers during its original run there in summer 1966.  In the Soviet Union, where it was released in January the following year, it had 29.1 million admissions,  making it to the top ten in the 1967 box office.  It became popular throughout the Eastern Bloc. Walter Ulbricht requested a private screening "to understand why everyone were going to cinemas", and after the film ended he told the studio representative "to keep it up!" (weiter so!)  

===Critical response===
The film critic of the   reviewer added: "under the pro-Indian direction of Josef Mach, the first Western of the German Democratic Republic is a shoot em up (Knall und-Fall) picture inlaid with a message: the athletic Red men shoot the Whites down from the saddle, and then stand between the tents, unemployed; the palefaces, on the contrary, drink and gamble in some fort." 

===Analysis===
  form of model citizen". Gemünden. p. 251.  In addition to all this, while he already worked with Western filmmakers, he chose to emigrate to East Berlin  a welcome contrast to the constant flow of local actors defecting to the Federal Republic. 

David T. McNab‏ and Ute Lischke stressed that while purporting to present an authentic depiction of Native Americans, it also delivered orthodox East German political messages: the films ending, in which Tokei-ihto declares his people will settle on the "rich fertile land" to "raise tame buffalo, forge iron, make ploughs" champions "no Indian philosophy" but sounds more like "a new way for a worker and collective farming state". The use of religion, in the form of the bear cub saved from the cave, is an "improbable narrative... The spirit world is not understood but interpreted for the propaganda of the former East. Once again, the film exploits the Indians." 

===Legacy=== East German cultural nostalgia. 

==References==
 

==Bibliography==
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 

==External links==
* 
*  at icestorm.de.
*  at the Czech Film Database.  
*  at progress-film.de.

 
 
 
 
 
 
 
 
 
 