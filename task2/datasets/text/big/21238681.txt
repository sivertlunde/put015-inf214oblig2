Encirclement – Neo-Liberalism Ensnares Democracy
{{Infobox film
| name           = Lencerclement - La démocratie dans les rets du néolibéralisme
| image          = 
| alt            =  
| caption        = 
| director       = Richard Brouillette
| producer       = {{Plainlist|
*Esteban Bernatas
*Richard Brouillette}}
| writer         = {{Plainlist|
*Richard Brouillette
*Kathleen Fleming}}
| starring       = {{Plainlist|
*Noam Chomsky
*Ignacio Ramonet
*Normand Baillargeon Susan George}}
| music          = Éric Morin
| cinematography = Michel Lamothe
| editing        = Richard Brouillette
| studio         = 
| distributor    = 
| released       =  
| runtime        = 160 minutes
| country        = Canada
| language       = French English
| budget         = 
| gross          = 
}}
Encirclement – Neo-Liberalism Ensnares Democracy (French: LEncerclement - La démocratie dans les rets du néolibéralisme) is a 2008 Canadian   by Richard Brouillette which was awarded the Robert and Frances Flaherty Prize (Grand Prize) at the 11th Yamagata International Documentary Film Festival (Yamagata, Yamagata|Yamagata, Japan, 2009), the Grand Prize at the 15th Visions du réel festival (Nyon, Switzerland, 2009), the Audience Award for Best feature film, along with a Special Jury Mention for the Amnesty International Award, at the 6th IndieLisboa festival (Lisbon, Portugal, 2009), the Pierre and Yolande Perrault Award for Best first or second documentary at the 27th Les Rendez-vous du cinéma québécois|Rendez-vous du cinéma québécois (Montreal, Canada, 2009), and the La Vague Award for Best documentary film (ex aequo with Hommes à louer, by Rodrigue Jean) at the 23rd Festival international du cinéma francophone en Acadie (Moncton, Canada, 2009).

The world premiere took place at the 11th   on November 20, 2008. The international premiere took place at the 59th Berlinale, in the International Forum of New Cinema section, on February 7, 2009.

== Synopsis ==

Drawing upon the thinking and analyses of renowned intellectuals, the documentary sketches a portrait of neo-liberal ideology and examines the various mechanisms used to impose its dictates throughout the world.

== Technical details ==

* Producer, Director, Editor: Richard Brouillette
* Scriptwriter and Researcher: Richard Brouillette
* Cinematography: Michel Lamothe
* Sound recording: Simon Goulet
* Additional sound recording: Alexandre Gravel
* Original music:  
* Sound mix: Éric Tessier
* Production company: Les films du passeur
* International sales: Andoliado Producciones and Cinema Esperança International
* Distribution in Canada: Amoniak Films Distributions and Cinema Esperança International
* Format : Black and White -    ) or   ( )
* Country: Quebec (Canada)
* Language: original version in   subtitles
* Genre:  
* Running time: 160 minutes

== Starring ==
* Noam Chomsky
* Ignacio Ramonet
*  
*  
*  
*  
* Michel Chossudovsky
* François Denord
* François Brune
* Martin Masse
* Jean-Luc Migué
* Filip Palda
*  

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 
 
 