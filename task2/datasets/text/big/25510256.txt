Atithi Tum Kab Jaoge?
 
 
{{Infobox film
| name           =
| image          = Atithi Tum Kab Jaaoge.jpg
| caption        = Theatrical release poster
| director       = Ashwni Dhir
| producer       = Amita Pathak
| writer         = 
| screenplay     = Tushar Hiranandani Robin Bhatt
| story          = Ashwni Dhir
| starring       = Ajay Devgn Paresh Rawal Konkona Sen Sharma
| music          = Songs:  
| cinematography = Aseem Bajaj
| editing        = Dharmendra Sharma
| studio         = 
| distributor    = Warner Bros. Pictures Wide Frame Pictures
| released       =  
| runtime        = 115 minutes
| country        = India
| language       = Hindi
| budget         =  
| gross          =   
}}
Atithi Tum Kab Jaoge (Dear Guest, When Will You Leave) is a 2010 Bollywood comedy film directed by Ashwni Dhir, and it was produced by Amita Pathak. The film stars Ajay Devgan, Paresh Rawal and Konkona Sen Sharma in the lead roles. It was released on 5 March 2010.The film was an average grosser and made nearly double of its budget (150&nbsp;million) at the box office. It received numeorous nominations at award ceremonies.    The film was declared average at Indian boxoffice.

==Plot==
Chastised by their son, Aayushs Hindi school-teacher for not conversing in one of the national languages, as well as ignoring the relevance of the word Atithi, Mumbai-based Bollywood writer, Puneet (Ajay Devgn), and his wife, Munmun (Konkona Sen Sharma), go out of their way to welcome an hitherto unknown paternal uncle, Lambodar (Paresh Rawal), from Gorakhpur. Hoping that he will depart soon, the couple find their lives turned upside when he not only refuses to leave but interferes with their professional and personal lives – so much so that Puneet gets fired from his movie project Pachaas, and Munmun almost loses her job. The loaded with mayhem ways they try to get Lambodar out of their house leads to hilarious results!The climax of the story is that when Lambodhar decides to go back to his village Pappu(his original nephew whom he had mistaken as Ajay Devgn) comes and invites him over.The confusion was that Puneet was living in A wing and Pappu was in the B wing of the apartment.

==Cast==
* Ajay Devgn as Punit Bajpai

* Konkona Sen Sharma as Munmun Bajpai
* Paresh Rawal as Lambodar Chacha
* Satish Kaushik as Ranjeet Taneja
* Sanjai Mishra as Building Watchman
* Akhilendra Mishra as Suleman Bhai
* Viju Khote as Kaalia
* Mukesh Tiwari as Inspector
* Rohitash Gaud as Niranjan Tripathi
* Hrishikesh Joshi

==Production==
The film was shot at Yashraj Studios, Filmistan, Film City and other locations in Mumbai. 

==Reception==

===Critical===
The film received average to good ratings from the critics. Taran Adarsh from Bollywood Hungama rated it 3.5 out of 5, and claimed that the film was "fun" and "frolic" for all. Komal Nahta gave it 3/5 and explained how it should be acclaimed for its family love, and the great message at the end.

===Box office===
Atithi Tum Kab Jaoge had an "average" opening at the box office, and took about a 30–40% opening, which is a typical trend for films in the family comedy/drama genre. The collections picked up through good word of mouth and sustained well at the box office for eight weeks despite competition from other films and IPL cricket tournament held during the same time. The film worked well with family audiences but did not find a large youth audience. The film did a domestic gross of   390&nbsp;million.    It was declared an average grosser by Box Office India.

==Music==
{{Infobox album
| Name = Atithi Tum Kab Jaoge
| Cover = Atithi Tum Kab Jaoge?.music.jpg
| Type = soundtrack
| Artist = Amit Mishra & Pritam Chakraborty
| Released =  
| Recorded = Feature film soundtrack
| Length = 39:22
| Label = Junglee Music
| Producer = Amita Pathak
}}

The music is composed by Amit Mishra and Pritam Chakraborty while the lyrics are by Irshad Kamil. The soundtrack was released in 20 February 2010.     "Aaja Aaja" and its remix are composed by Pritam while the rest were by Amit Mishra

# "Atithi Tum Kab Jaoge" – Amit Mishra
# "Jyoti Jalaile" – Sukhwinder Singh
# "Aaja Aaja" – Raghuvir Yadav, Ajay Jhingran & Rajneesh
# "Dohe" – Amit Mishra
# "Sukhakarta" – Amit Mishra
# "Aaja Aaja (remix)" – Raghuvir Yadav, Ajay Jhingran & Rajneesh

==Adaptation==
The film is based on short story Tum kab jaoge,Athithi? by Sharad Joshi, famous satirist of Hindi, whose stories are being aired on SAB TVs Lapataganj, series directed by Ashwini Dhir, The Director of the film. The film is a hilarious version of Agantuk (Written and Directed by Satyajit Ray).
 Gujarati novel Bauter Varas No Babo published in 1976, which was later translated in Hindi as Bahatar Saal Ka Baccha. He has filed a case with The Film Writers Association (FWA) and has demanded Rs 11.5&nbsp;million as compensation, the case is still pending judgment.   

== Awards and nominations ==

* Won: Apsara Award for Best Performance in a Comic Role – Paresh Rawal Best Comedian – Paresh Rawal 

==References==
 

==External links==
*  
*  
*   Bollywood Hungama
*  

 
 
 
 
 