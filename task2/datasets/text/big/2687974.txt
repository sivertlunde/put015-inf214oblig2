Drunken Master II
 
 
 
{{Infobox film
| name           = Drunken Master II
| image          = DrunkenMasterII.jpg
| caption        = Drunken Master II film poster
| film name      = {{Film name
| traditional    = 醉拳二
| simplified     = 醉拳二
| pinyin         = Zuì Quán Èr
| jyutping       = Zeoi3 Kyun4 Ji6}}
| director       = Lau Kar-Leung Jackie Chan  (uncredited) 
| producer       = Eric Tsang Edward Tang Barbie Tung
| writer         = Edward Tang Tong Man-Ming Yun Kai-Chi
| starring       = Jackie Chan Ti Lung Anita Mui Ken Lo Lau Kar-Leung Andy Lau Felix Wong
| music          = William Hu
| cinematography = Jingle Ma Tony Cheung Cheung Yiu Jo Nico Wong
| editing        = Peter Cheung Golden Harvest Paragon Films
| released       =  
| runtime        = 102&nbsp;minutes
| language       = Cantonese
| country        = Hong Kong
| budget         = $10&nbsp;million
| gross          = Hong Kong: Hong Kong dollar|HK$40,971,484 United States: US$11,555,430
 }} Chinese folk hero, Wong Fei-hung. It was Chans first traditional style martial arts film since The Young Master (1980) and Dragon Lord (1982). The film was released in North America as The Legend of Drunken Master in 2000.
 1978 film best films of all time by Time (magazine)|Time magazine.

==Plot== consul to avoid the tax.

When the train makes a stop, Fei Hung and Tso create a diversion to sneak into the first class section (filled with members of the British Consulate and the British Ambassador) to retrieve the ginseng. However, when Fei Hung gets to the first class luggage, he spots a Manchurian officer (Lau Kar-Leung) apparently stealing an unknown item that is in a similar package as the ginseng. Fei Hung confronts him but the officer hits Fei Hung. However, Fei Hung retrieves the ginseng, and pursues the officer in revenge for hitting him. A long fight between them ensues under the train, with the officer gaining the upper hand. He warns Fei Hung that the next time he will kill him and calls him a traitor. Puzzled, Wong Fei Hung angrily tells the officer that he is not a traitor and challenges him to a hand-to-hand Kung Fu fight. Fei Hung uses his Zui Quan (Drunken Boxing) style of martial arts on him, but it proves to be ineffective. After the fight, the officer tells Fei-Hung that his drunken boxing has no power and cant kill. When Fei Hung returns to the train, the Manchurian officer opens the box he stole from the train, only to realize that it is actually the ginseng.

Meanwhile, on the train, guards of the British Consulate search for a stolen item and they ask the Wongs to show them their items. Fei Hung discovers that he accidentally stole a valuable Chinese antique, instead of the ginseng. But before the officers discover it, a sympathetic son of a Northeast Chinese general (Andy Lau) uses his influence to intervene (in both English dubbed versions, the man is actually a counter-intelligence officer). In a later scene at the British consulate, the British ambassador (Louis Roth), who is apparently involved in a corrupt operation to smuggle ancient Chinese artifacts and sell them to the London Museum of Arts, tells his henchman that they wont buy the collection of antiques without the missing Emperors Jade Seal (the artifact now accidentally in Fei Hungs possession). Then he sends his henchman, John and Henry (Ken Lo and Ho Sung Pak) to make the workers at a local steel factory work overtime. When the workers refuse, Henry viciously beats them with his martial arts skills, and forces them to get back to work.

When the Wongs return home from their train ride, trouble brews for Fei Hung when his fathers client, Mr. Chan, comes to retrieve the ginseng root. Fei Hung takes the root of his fathers prized ancient bonsai tree, discreetly gives it to Mr. Chan and tells him that it is the ginseng. Knowing that the bonsai tree root could be deadly for Mr. Chan if he decides to brew it, Fei-Hungs step-mom, Ling (Anita Mui) decides to temporarily loan her necklace to one of her friends in exchange for some money to buy ginseng. This leads some of Master Wongs friends to believe that the Wongs are having financial troubles, and they offer him a collection, which a confused Master Wong declines. Meanwhile, Fei Hung and Ling do not realize that Henry and his men are following them. Assuming that what Ling and Fei Hung are carrying is the stolen artifact (although its actually Lings necklace), they attempt to steal the bag, which starts a fight between Fei-Hung vs. Henry and his men. During the fight, Ling encourages Fei Hung to use drunken boxing against them to impress the crowd and gain publicity for the Wongs school, Po Chi Lam. She and her friends take a bunch of alcohol from a country club and give it to Fei Hung, therefore giving him the speed and power he needs to do drunken boxing properly, and then he impressively defeats Henry and his henchmen. However, Master Wong Kei Ying arrives as the fight ends, and Fei Hungs drunken behavior embarrasses the family. Master Wong takes his son and wife home and lectures them, saying they are destroying his reputation by fighting and drinking in public, and for making others believe that they are broke. He beats Fei Hung for fighting and using drunken boxing (which Master Wong forbids). To make matters worse, Mr. Chans wife comes by to tell Wong Kei Ying that her husband is very sick from the bonzai tree root, which is poisonous if consumed. An infuriated Master Wong beats Fei Hung even more and disowns him, kicking him out of the house.

Fei Hung goes to a restaurant and drinks heavily in sorrow. John arrives with a beaten Henry and the rest of the henchman from earlier to confront him. Fei Hung is now clearly too drunk to fight, and John beats him. Fishmonger Tsang (Felix Wong), a fellow Kung-Fu instructor and friend of Fei-Hung, arrives and tries to intervene, but is unable to when a vat of hot liquid he was carrying spills on him. The next morning, Fei Hung and Tsang are found knocked out beaten, with Fei Hung stripped with a banner hanging from him that says "King of Drunken Boxing." Master Wong brings Fei Hung back into the home, and explains that the reason why he forbids drunken boxing is because it is difficult for drunken boxers to find the right balance of alcohol consumption. The following night, the Manchurian officer from the train arrives at the Wongs residence to speak to Fei Hung. Master Wong recognizes him as Master Fu Wen-Chi, the "last decorated Manchu officer." The next day at a restaurant, Master Fu explains to Fei Hung that the artifact that ended up in Fei-Hungs possession (and what Master Fu meant to steal from the train) was the Emperors Jade seal. He tells him about the theft of precious ancient Chinese artifacts by foreigners and asks him to join him in stopping it. Moments later, an enormous gang of axe-wielding thugs (known as the Axe Gang), apparently paid for by the British Consulate, try to kill them. After a long fight, Fei Hung and Master Fu make an escape, and Fishmonger Tsang, Fun, and Tsangs student Marlon (Lau Ga-Yeung) join the fight, as they recognize Master Fu. But a British consulate guard fatally shoots Master Fu when he runs down an alley, and they take back the Jade seal. Fu Wen Chi pleads with them to get it back before he dies.

The following night, both Tsang and Fei Hung break into the consulate disguised as consulate guards to retrieve the Jade seal, but are both eventually caught. They are jailed, beaten, and held for ransom by the British Ambassador, who demands that Wong Kei Ying sells the land where Po Chi Lam and Fishmonger Tsangs schools are. Master Wong reluctantly agrees to do so and the Consulate releases Fei Hung and Tsang. Then the ambassador orders the steel mill to be closed down and for all of the steel shipments to be sent to Hong Kong. Angry, steelworkers Fo Sang (Chin Kar-lok) and a man named Uncle Hing (Hon Yee Sang) break into the steel mill later that night to find out what the British are up to, and they discover the steel shipment boxes are filled with ancient Chinese artifacts. However, they are caught and they fight the consulates henchman. Fo Sang escapes and informs Fei-Hung and Ling about what is happening.

Later, Fei Hung, Tsang, Fun, and Marlon arrive at the factory where the workers are staging a protest that becomes violent against the Consulates abuses. Once inside the factory, Fei Hung takes on all of the henchmen until only Henry and John are left. Fei Hung easily fights off Henry but John proves to be a tough opponent due to his strong, fast, and flexible kicks. When John and Henry gain the upper hand and are about to finish him off, Fei Hung uses the industrial alcohol in the steel mill to light Henry on fire, and then drinks it. Disposing of Henry, Fei Hung then drinks enough industrial alcohol and beats John in a wild fight scene with his drunken boxing.

Later, the Wongs are rewarded by a Chinese general for their help in stopping the British Consulates crimes, which includes a sum of money and Po Chi Lam and Tsangs schools back.

==Cast and crew== Golden Harvest studio founder Leonard Ho.
The film was directed by Lau Kar-leung, although Jackie Chan is credited with directing the final fight scene. The villain in the scene is played by Ken Lo, a Jackie Chan Stunt Team member and Chans former personal bodyguard. The action direction was by Lau Kar-leung in co-operation with the Jackie Chan Stunt Team.

===Cast===
*Jackie Chan – Wong Fei Hung
*Anita Mui – Ling
*Ti Lung – Wong Kei Ying
*Felix Wong – Fishmonger Tsang
*Lau Kar-leung – Master Fu Wen-Chi (credited as Lau Ka Leung) Cheung Hok Leung, Son of Northeast General on train (HK version) / Counter Intelligence Officer (U.S. version) (cameo)
*Hoh Wing Fong – Fun
*Cheung Chi Gwong – Chang Tsan
*Ken Lo – Jon / John (as Low Houi Kang)
*Ho Sung Pak – Henry
*Cheung Chi-Gwong - Tso (credited as Tseung Chi Kwong)
*Chin Kar-lok – Ho Sang / Fo Sang (credited as Chin Ka Lok)
*Bill Tung – General rewarding Wong Kei Ying
*Hon Yee Sang – Uncle Ho / Hing
*Lau Ga-Yung – Marlon (Fishmonger Tsans student)
*Lau Siu Ming – Mr. Chu / Chiu
*Suki Kwan – Mrs. Chu
*Pak Yan – Mrs. Chan
*Szema Wah Lung - Senior in Restaurant #2
*Yvonne Yung Hung – Lings friend  (credited as Evonne Yung)
*Vindy Chan – Lings friend (credited as Chan Wai Yee)
*Louis Roth – British Consul (credited as Louis C. Roth)
*Po Tai – Moe (as Tai Bo)
*Alan Chan - Fight Spectator in the Crowd (credited as Chan Kwok Kuen) Mark Houghton – British military officer Smith
*Ho Pak Kwong – Uncle Ho (uncredited)
*Sandy Chan – Lily (as Chan Kui Ying)
*Vincent Tuatanne – Bruno
*Therese Renee – Terese Mars - Fight Spectator in the Crowd/Thug in Final Fight (uncredited)

==Home media==
The original region 0 DVD release of Drunken Master II is the only version which features the entire Hong Kong version. However, the aspect ratio is cropped to 1.78:1 from the original theatrical 2.35:1 aspect ratio.
 Mei Ah VCD and LaserDisc, Tai Sengs VHS (itself a recording of the Mei Ah LaserDisc) and the Australian VHS from Chinatown Video (a sub label of Siren Visual). These prints have "burnt-in" Chinese/English subtitles. An uncut release with good picture quality, the original audio track, and the original aspect ratio is considered a "holy grail" by many Hong Kong cinema fans.
 industrial alcohol during the films ultimate fight. Played for laughs, the scene was considered to be in bad taste by the American distributor, Dimension Films. 

In addition to the cut, however, there were other significant changes made to the US release including the change of title (to Legend of Drunken Master), an English-language dub (Chan dubbed himself), and a new musical score. The re-dubbed soundtrack also meant that sound effects were different, in some instances completely altering the rhythm of the fight scenes.   

The Australian (region 4) and Japanese (region 2) release featured the same cuts and re-scoring as the US release. The region 3 releases for Hong Kong and Korea contains the English export version with the original 2:35:1 non-anamorphic aspect ratio. This cut of the film ends almost immediately after Fei-Hung defeats John. The audio tracks include an abridged Cantonese and Mandarin soundtracks, and the original Golden Harvest English dub different than that of Dimensions. It contains the original score and sound effects, but there are no English subtitles.

A Blu-ray version was released on 15 September 2009, in the United States, which features the cut US version in the original 2.35:1 aspect ratio.   

In the UK it was released on Blu-ray 16 April 2012 under the title The Legend Of Drunken Master. 

==Reception==

=== Critical reception ===
  several years ago, comparing the physical comedy of Chan and Buster Keaton, martial arts fans brought in their bootleg Hong Kong laser discs of this film and told me that I had to see the final 20-minute fight sequence. They were correct. Coming at the end of a film filled with jaw-dropping action scenes, this extended virtuoso effort sets some kind of benchmark: It may not be possible to film a better fight scene."    

In   made Jackie Chan a star in Hong Kong, The Legend of Drunken Master may be the most kick-ass demonstration yet, for the majority of American moviegoers, of what the fuss is all about: To many aficionados (who know the video as Drunken Master II), this 1994 favorite, remastered and dubbed in "classic" bad Chinese-accented English, showcases Chan in his impish glory, dazzling in his ability to make serious, complicated fighting look like devil-may-care fun."      

 , and his early screen days as "the next Bruce Lee" to create his own genre of martial-arts comedies   Jackie starred in, and directed, many wonderful action films in his pre-Hollywood days. This one can stand at the peak".    

James Berardinelli was one of the less fervent reviewers: "The Legend of Drunken Master is pretty typical Hong Kong Chan fare – five superior action sequences with a lot of failed comedy and mindless drivel padding out the running length. Most of the expository and character-building scenes fall into one of three categories: (1) inane, (2) incomprehensible, or (3) dull. The tone is also wildly inconsistent. Some sequences are laced with slapstick comedy while others are acutely uncomfortable as a result of torture and the nearly-abusive disciplining of a grown child by a parent. (Differences in culture make the latter seem more incongruous to American viewers than to Chinese movie-goers.) So its up to the action to redeem the film – a feat it succeeds at, at least to a point."    

===Track listing===
#The Drunken Master (sung by Jackie Chan & other artists) (03:09)
#Provocation (02:12)
#Circled On All Side (02:13)
#Conspiracy (01:39)
#Stealing The Jade Seal (00:34)
#The First Fight (02:22)
#Bayonet (01:08)
#To Try Out (01:21)
#The Car Chase (00:50)
#Searching (01:22)
#Wonderful Moment (01:25)
#Not Thinking Others (04:22)(sung by unknown artist)
#Free (01:29)
#Mistake (01:27)
#Mother And Son (01:35)
#The Wild Strong Man (01:45)
#Ending Love (sung by unknown artist) (04:22)
#Gathering Of City (01:35)
#The Discussion (01:09)
#The Robbery Of Countrys Fortune (00:56)
#The Hidden Wiseman (01:10)
#Marching Forward (01:12)
#Real Dragon Does Not Belong To Small Pond (02:59)
#Play Around (01:04)
#Fan Hons Lesson (02:07)
#Public Insult (01:16)
#Regret (01:49)
#A Drunken Journey (01:20)
#The Hand-To-Hand Combat (02:02)
#To Come On Proudly (02:48)
#The Drunken Master (sung by Jackie Chan & other artists) (03:06)

===Box office===
Drunken Master II was a notable success in Hong Kong, grossing Hong Kong dollar|HK$40,971,484 during its theatrical run.  The success was somewhat surprising, considering reports of tension on the set between Chan and Lau Kar Leung , and that the 90s vogue for kung fu films had more or less passed.

Six years later, Drunken Master II was released in 1,345 North American theaters as The Legend of Drunken Master By Dimension Films.   This re-edited version made US$3,845,278 ($2,865 per screen) in its opening weekend, on its way to a US$11,555,430 total.

===Awards and nominations=== 1995 Hong Kong Film Awards
**Winner: Best Action Choreography (Lau Kar-leung, Jackie Chan Stunt Team)
**Nomination: Best Film Editing (Peter Cheung)
 Golden Horse Film Festival
**Winner: Best Martial Arts Direction (Lau Kar-leung, Jackie Chan Stunt Team)

*1997 Fantasia Festival|Fant-Asia Film Festival
**Winner: Best Asian Film (Jackie Chan, Lau Kar-leung) Tied with Perfect Blue (1998)

==See also==
*Jackie Chan filmography
*Andy Lau filmography

==References==
 

==External links==
* 
* 
* 
*  at Hong Kong Cinemagic

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 