Ballad of a Bounty Hunter
{{Infobox film
  | name = Ballad of a Bounty Hunter
  | image =
  | caption = VHS cover for Ballad of a Bounty Hunter
  | director = Joaquín Luis Romero Marchent
  | producer = José Luis Jerez Aloza Giovanni Simonelli
  | starring = James Philbrook Norma Bengell Simón Andreu
  | music = Piero Piccioni
  | cinematography = Fulvio Testi
  | editing = Mercedes Alonso
  | distributor = Troma Entertainment
  | released = 1968
  | runtime = 81 minutes
  | country = Spain
  | language = Spanish
  | budget =
  | preceded by =
  | followed by =
  }}
 Spanish western film directed by Joaquín Luis Romero Marchent and distributed by Troma Entertainment.

==Cast==
* James Philbrook: Don Ramón
* Norma Bengell: Fedra
* Simón Andreu: Stuart
* Emilio G. Caba: José
* María Silva: Isabelle Alvárez
* Alfonso Rojas: Julio Alvárez
* Luis Induni: Fedras Bruder
* Maria Cumani Quasimodo: María
* Giancarlo Bastianoni: Outlaw
* Álvaro de Luna (actor)|Álvaro de Luna: Outlaw

==External links==
* 

 
 
 
 
 
 
 

 
 