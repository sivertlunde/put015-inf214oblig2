Lake Placid (film)
{{Infobox film
| name = Lake Placid
| image = Lake placid ver2.jpg
| caption = Theatrical release poster
| director = Steve Miner
| producer = David E. Kelley Michael Pressman Peter Bogart
| writer = David E. Kelley
| starring = Bill Pullman Bridget Fonda Oliver Platt Brendan Gleeson Betty White Meredith Salenger Mariska Hargitay
| music = John Ottman
| cinematography = Daryn Okada Paul Hirsch Marshall Harvey
| studio = Phoenix Pictures Stan Winston Studios
| distributor = 20th Century Fox
| country = United States
| released =  
| runtime = 82 minutes
| language = English
| budget = $27 million http://www.imdb.com/title/tt0139414/business 
| gross = $56,870,414 
}}
 horror comedy film. The film was written by David E. Kelley and directed by Steve Miner, starring Bill Pullman, Bridget Fonda, Oliver Platt, Brendan Gleeson, Betty White, Meredith Salenger and Mariska Hargitay. The plot revolves around a giant, 30-foot-long man-eater|man-eating crocodile which terrorizes the fictional location of Black Lake, Maine, United States, and also follows the dysfunctional group who attempt to capture or destroy the creature.

The film was produced by Fox 2000 Pictures and Stan Winston Studios (which did the special effects for the creatures) and principal photography was shot in British Columbia, Canada. The film was distributed by 20th Century Fox and released in cinemas in the United States on July 16, 1999, and in the United Kingdom on March 31, 2000. 

The film was a financial success at the box office and was followed by three made-for-television sequels,   (2012), aswell as a made-for-television spin-off crossover-film with the Anaconda (film)|Anaconda-franchise, titled Lake Placid vs. Anaconda (2015).

==Plot==
In Aroostook County, Maine, marine Fish and Game officer Walt Lawson is attacked and bitten in half by something unseen in Black Lake. Sheriff Hank Keough (Brendan Gleeson), Fish and Game officer Jack Wells (Bill Pullman), American Museum of Natural History paleontologist Kelly Scott (Bridget Fonda), and mythology professor/crocodile enthusiast Hector Cyr (Oliver Platt) go to the lake to investigate.

A series of strange events occurs, including Kelly and Hanks canoe mysteriously flying into the air and flipping, the discovery of a severed toe and a severed moose head, and the decapitation of Burke, one of Hanks deputies.

Later, as Hank and Hector argue, a bear attacks them, but a giant saltwater crocodile then leaps out of the water and drags it into the lake.  Later, after finding Burkes severed head, Jack, Kelly, and Hank witness Mrs. Delores Bickerman (Betty White), one of few people living on the lake, feeding a blindfolded cow to the enormous crocodile. Mrs. Bickerman reveals that she has been feeding the crocodile for years after it followed her husband home. It eventually killed him. She is placed under house arrest for initially lying to the police.

Hector decides to take Deputy Sharon Gare (Meredith Salenger) on a trip in his helicopter, and lands it in the cove where the crocodile lives. While he is diving, it targets him, but he and Gare escape. Jack and Hank plan to allow Florida Fish and Game to kill the crocodile when they arrive, but Hector suggests instead that he lure it out of the water and drug it. Jack reluctantly accepts the proposal, and they use one of Mrs. Bickermans cows, dangled from the helicopter, as bait. After a few hours, the crocodile lunges at the cow. Hector pulls up, loses the cow, and crashes the helicopter into the lake. The crocodile comes on land and attacks Jack and Kelly. Kelly is knocked into the lake, but she makes it to the helicopter just in time.

The crocodile then gets trapped in the helicopter. Despite Hector and Kellys protests to let the animal live, Jack grabs a gun and shoots it. The gun is revealed to be a tranquilizer rifle. As Hector comes out of the water, a second crocodile attacks him, but Hank blows it up with his grenade launcher. Florida Fish and Game officers arrive seconds later. They load the crocodile on a truck and take it to Portland, Maine to figure out what to do with it. The last scene shows Mrs. Bickerman feeding bread crumbs to many baby crocodiles, implying the two adults were a mating pair. During the end credits, the surviving adult crocodile is seen tied to the back of a flat-bed truck, speeding down a road.

==Cast==
* Bill Pullman as Jack Wells
* Bridget Fonda as Kelly Scott
* Oliver Platt as Hector Cyr
* Brendan Gleeson as Sheriff Hank Keough
* Betty White as Mrs. Delores Bickerman
* Meredith Salenger as Deputy Sharon Gare David Lewis as Walt Lawson
* Tim Dixon as Stephen Daniels
* Natassia Malthe as Janine
* Mariska Hargitay as Myra Okubo
* Jed Rees as Deputy Burke
* Richard Leacock as Deputy Stevens
* Jake T. Roberts as Officer Coulson
* Ty Olsson as State Trooper
*Adam Arkin as Kevin (uncredited)

==Production==
The film was produced by Fox 2000 Pictures, Phoenix Pictures, and Rocking Chair Productions.  The   long crocodile was created by  .

Almost the entire film was shot on location in remote locations in Lincoln, Maine|Lincoln, Maine, which stood in for the fictional locations of the film in the American state of Maine. Some scenes were shot in Vancouver and Surrey, British Columbia|Surrey, B.C. Three different lakes stood in for the fictional "Black Lake"; these were Shawnigan Lake, Vancouver Island, B.C.; Buntzen Lake, Anmore, B.C.; and Hayward Lake, Mission, British Columbia|Mission, B.C. 
 PETA would be interested to learn of her alleged mistreatment of her cows. In reality, Betty White is a major on-air spokesperson for PETA. 

==Reception==
Although Lake Placid was a financial success at the box office, critical reception was mixed. Rotten Tomatoes gave the film a 39% rating with the critical consensus being "Faux horror schtick fails to elicit any laughs or scares."  Roger Ebert described it as "completely wrong-headed from beginning to end".  Empire (magazine)|Empire gave the film four out of five stars, saying "You can enjoy Placid as a straightforward camping-holiday nightmare, or as a sly, ironic take on the same. It works deliciously as both." 

==Sequels==
The film was followed by three sequels and a crossover-film, and all are television/Syfy films and were not as successful as the original.
 Sony Pictures Sci Fi Sci Fi Channel original movie on April 28, 2007. Changes from the original film include a completely different cast, filming locations in Bulgaria and a severely reduced budget. The unrated DVD release of the film was distributed by 20th Century Fox and released on January 29, 2008.
 Colin Ferguson. Aired on August 21, 2010  and was released as a DVD on October 26, 2010. Lake Placid 3 have two versions; The TV version was rated R, the DVD version was unrated and contains nudity. 

Its final sequel,  , was released on September 29, 2012. Yancy Butler, who starred in the third film, reprised her role as Reba who survived being attacked by a crocodile at the end of the third movie.

A crossover-film with the Anaconda (film)|Anaconda-franchise, titled Lake Placid vs. Anaconda, premiered on April 25, 2015, on Syfy.

==See also==
* List of killer crocodile films

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 