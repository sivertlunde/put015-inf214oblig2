God for Sale
 
{{Infobox film
| name           = God for Sale
| image          = Godforsalemovieposter.jpg
| alt            = 
| caption        = Theatrical Release Poster
| director       = Babu Janardhanan
| producer       = Salim P. T.
| writer         = Babu Janardhanan
| based on       = 
| starring       = Kunchacko Boban Jyothi Krishna Suraj Venjaramoodu Anumol Tini Tom Thilakan Afsal Yousuf
| cinematography = Sinu Sidharth
| editing        = Sobhin K. Soman
| studio         = Green Advertising
| distributor    = Saldal
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          =
}} Malayalam satirical satirical drama drama film written and directed by Babu Janardhanan and starring Kunchacko Boban in the lead role with Jyothi Krishna, Suraj Venjaramoodu, Anumol, Tini Tom and Thilakan plays other pivotal roles.    The film focusses on the fraudulent spiritual leaders and demigods who are rising in numbers and tackles it with a satirical tinge.  This was the last movie in which Thilakan acted before his death.

==Plot==
God for Sale is a satirical take on what all determines the faith of a person. The film opens with the arrest of Poornananda Swami (Kunchacko Boban) who is alleged of child sacrifice. His story is unveiled through versions put forth by the accused and his brother (Suraj Venjaramoodu). 

The plot of the movie shifts to the seventies. Attingal village is home to Kamalasanan Pillai (Suraj Venjaramoodu), a tailor. One fine morning, after a Kadhaprasangam performance, Pillai was found dead under suspicious circumstances. From there, the plot shifts to a more contemporary period. Prasannan (Kunchakko Boban), son of Kamalasanan Pillai, is now a hardworking daily wages labourer, who has a lady love (Jyothi Krishna). Again, the backdrop changes, and the protagonist is seen as a brilliant law student in a famous college. Prasannan confronts some bitter truths in his early life that alters his personality. When he joins the college, he is attracted to Leftist thinking. Later he falls in love with a rich woman, and eventually turns into an affluent person, and an emotional downfall turns him into an alcoholic. The journey of Prasannan from there to the retreat centre and ultimately to a person who claims to be the avatar of God is the storyline of the film.

== Cast ==
* Kunchacko Boban as Prasannan nair
* Jyothi Krishna as Kamala
* Suraj Venjaramoodu as Kamalahasanan pillai
* Anumol as Anupama
*Kochu Preman
*Kalaranjini
* Mala Aravindan 
* Tini Tom
* Thilakan
* LaKshmipriya

==Production==
The first schedule of the film was taken in mid-2012. Actor Thilakan appears in an important scene taken in this schedule. This was the last movie in which Thilakan acted before his death. The major locales of the film were Government Engineering College, Thrissur and Vadakkencherry. 

==References==
 

 
 
 