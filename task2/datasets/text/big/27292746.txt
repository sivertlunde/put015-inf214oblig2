The Shift (film)
{{Infobox film
| name           = The Shift
| image          =
| image size     =
| caption        =
| director       = David Trumble
| screenplay     =  
| story          = Greg Lock
| producer       = David Trumble
| starring       =  
| released       =  
| runtime        = 15 minutes
| country        = United Kingdom
| language       = English
| budget         = £3,000
}}
The Shift is a short film directed by David Trumble, starring Greg Lock and Graham Hornsby. The film, written by David Trumble, Matt Brothers and Greg Lock focuses on a Paramedic and Emergency Medical Technician (EMT) working for the London Ambulance Service on a night shift.

==Plot==
The protagonist Damon Yorke, played by Greg Lock is a young but highly valued and experienced London based Paramedic. Damon is having some relationship problems with his girlfriend Clare, and is being forced to attend couples counselling. Damon is told that he should try phoning Clare just before she goes to bed whilst he is on shift work, just to reassure her that he is okay and that he loves her. However on the first night shift of trying this new system out he accidentally leaves his phone at home. 

His crewmate who is EMT Joe Greene, played by Graham Hornsby claims to have also left his phone back at the ambulance station. So a personal quest for Damon begins to try and find a phone so he can call Clare. The only problem is Damon is confronted by the many incidents any ambulance would encounter on a night shift ranging from the trivial to the serious and these keep getting in Damons way. This results in Damons anger and frustrations with his personal life coming to surface, and somethings got to give. 

==Cast==

* Greg Lock as Damon Yorke
* Graham Hornsby as Joe Greene
* Debbie Wicks as Clare
* Liam H. Dempsey as Security Guard
* Victoria Eldon as Sister
* Charlotte Eldon as Electrocuted Girl
* Tommy Egerton as Old Lady
* Kathryn Ritchie as Emma
* Eifion Robert Melnyk-Jones as Llewelyn

==Production==
 
The film was shot over the course of 5 nights with an ambulance hired for 2 of them. The majority of the filming took place in Bournemouth, England. Even though the film takes place in London, the only locations used that were actually in London was Damons flat and the riverside with Canary Wharf in the background.

The shooting of the film was done on a Canon 7D by Adam Scarth as Director of Photography, he was assisted by Noorganah Robertson and Thomas Saville.

==Charity gala premiere== David Williams David Williams and Helen Baxendale attended the event. The Shift was shown as the first part of the evening. 

==References==
 

==External links==
*  

 

 
 
 