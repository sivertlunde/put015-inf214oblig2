Candy (1968 film)
{{Infobox film
| name           = Candy
| image          = Candy_movieposter.jpg
| image_size     =
| caption        = original film poster
| director       = Christian Marquand
| producer       = Robert Haggiag Selig J. Seligman Peter Zoref
| screenplay     = Buck Henry
| based on       =  
| starring       = Marlon Brando Ewa Aulin John Astin Charles Aznavour Richard Burton James Coburn John Huston Ringo Starr Elsa Martinelli Enrico Maria Salerno
| music          = Dave Grusin
| cinematography = Giuseppe Rotunno
| editing        = Giancarlo Cappelli Frank Santillo Cinerama Releasing
| released       =  
| country        = France Italy United States
| runtime        = 124 minutes
| language       = English
| budget         = $2.72 million "ABC Motion Pictures|ABCs 5 Years of Film Production Profits & Losses", Variety (magazine)|Variety, 31 May 1973, pg 3. 
| gross          = $16,408,286 
}}
 the 1958 novel by Terry Southern and Mason Hoffenberg, from a screenplay by Buck Henry.  The film satirizes pornographic stories through the adventures of its naive heroine, Candy, played by Ewa Aulin. It stars Marlon Brando, Ewa Aulin, Richard Burton, James Coburn, Walter Matthau, Ringo Starr, John Huston, John Astin, Charles Aznavour, Elsa Martinelli and Enrico Maria Salerno. Popular figures such as Sugar Ray Robinson, Anita Pallenberg, Florinda Bolkan, Marilù Tolo, Nicoletta Machiavelli and Umberto Orsini also appear in cameo roles.

==Plot==
High school student Candy (former Miss Teen Sweden Ewa Aulin) seemingly descends to Earth from space. In the relatively simple plot, she naively endures an escalating series of situations in which her oblivious allure triggers satirical porn-film-like encounters.  Roger Ebert wrote, "Candy caroms from one man to another like a nympho in a pinball machine, and the characters she encounters are improbable enough to establish Terry Southerns boredom with the conventions of pornography." Ebert, Roger (December 26, 1968)   Chicago Sun Times Last accessed 2010-03-23. 
 psychedelic journey hunchback (Charles Aznavour), an obsessed underground filmmaker (Enrico Maria Salerno) and a fake Indian guru (Marlon Brando). As the film ends, she meets a wise guru in an Indian temple (who turns out to be her brain-damaged father in disguise), revisits some of the characters she met in the film, then wanders off into the desert before returning to outer space.

==Screen debut== Beatle Ringo A Hard Magical Mystery The Magic Christian — while he continued his music career.

==Reception== psychedelic movies Yellow Submarine, The Trip, 18th highest grossing film of 1968.

According to Variety the film earned North American rentals of $7.3 million, but because of costs (including over $1 million paid out in participation fees), recorded an overall loss of $25,000.  It was the 12th most popular movie at the UK box office in 1969. 

Reviews were generally positive with a few misgivings: the film rates 80% at the   found it "a lot better than you might expect" but missed the "anarchy, the abandon, of Terry Southerns novel".  Renata Adler decried "its relentless, crawling, bloody lack of talent".  Rock Me", Steppenwolf which became a Top 10 hit for the band in the Winter of 1969.

==Cast==
*Ewa Aulin - Candy  
*Charles Aznavour - The Hunchback 
*Marlon Brando - Grindl  
*Richard Burton - McPhisto
*James Coburn - Dr. Krankheit  
*John Huston - Dr. Dunlap  
*Walter Matthau - General Smight  
*Ringo Starr  - Emmanuel  
*John Astin - Daddy / Uncle Jack  
*Elsa Martinelli - Livia  
*Sugar Ray Robinson - Zero  
*Anita Pallenberg - Nurse Bullock  
*Florinda Bolkan - Lolita  
*Marilù Tolo - Conchita  
*Nicoletta Machiavelli - Marquita
*Enrico Maria Salerno - Jonathan J. John
*Umberto Orsini - The Big Guy  
*Joey Forman - The Cop (Charlie)
*Lea Padovani - Silvia Fontegliulo 
*Buck Henry - Mental Patient

==See also==
* Candy (Southern and Hoffenberg novel)|Candy (Southern and Hoffenberg novel)

==References==
 

==External links==
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 