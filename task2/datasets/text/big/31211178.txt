Sthithi
{{Infobox film
| name           = Sthithi
| image          = 
| image size     =
| caption        =
| director       = R. Sarath
| producer       = T. P. Abdul Khader
| writer         = R. Sarath
| narrator       =
| starring       =  
| music          = Unni Menon M. Jayachandran Sunny Viswanath
| cinematography = M. J. Radhakrishnan
| editing        =
| studio         = Sak Films
| distributor    = 
| released       =  
| runtime        =
| country        = India
| language       = Malayalam
| budget         =
| gross          =
}}
Sthithi ( ) is a 2002 Malayalam feature film written and directed by R. Sarath. The film tells the tale of a couple employed in the Secretariat. It stars Unni Menon, Nandini Goswal, Master Achuth, Master Anand and Mallika Sukumaran. It is playback singer Unni Menons debut film as an actor. He also composed a couple of songs for the film. Bengali actress and Odissi dancer, Nandini Goswal plays the female lead.

==Plot==
Vivek and Vani who are Secretariat employees. They live in a modest house on the outskirts of Thiruvananthapuram together with their two children. Their lives change when under threat of job losses, the trade unions call for strike action whilst at the same time their landlord asks them to leave.

==Cast==
* Unni Menon as Vivek
* Nandini Goswal as Vani
* Master Achuth
* Master Anand
* Mallika Sukumaran
* C. K. Basu
* Vellayambalam Gopalakrishnan
* Sreelatha Namboothiri
* Remya Nambeesan
* Vinu Abraham
* Kollam Devarajan
* Ratna Purushothaman
* Jayaraj Warrier
* Ajayan

==References==
*  
*  

 
 
 

 