Devil's Mile
{{Infobox film
| name = Devils Mile
| image = File:Devils Mile film poster 2014.jpg
| director = Joseph OBrien
| producer = Joseph OBrien Mark Opausky Motek Sherman
| writer = Joseph OBrien Maria del Mar Casey Hudecki Amanda Joy Lim Frank Moore Adrienne Kress Samantha Wan Shara Kim Craig Porritt Chris Alexander
| cinematography = Brian Chambers
| editing = Joyce Poon
| studio = Grovers Mill Productions Swim Pictures
| distributor = Phase 4 Films
| released =  
| runtime = 88 minutes
| country = United States Canada
| language = English
}}

Devils Mile is a 2014 American-Canadian horror film and the feature film directorial debut of Joseph OBrien, who also co-wrote the script and created the films special effects.  The film premiered at the Fantasia Film Festival on July 26, 2014 and is scheduled for release on DVD and VOD on August 12, 2014. It stars David Hayter as a mob enforcer that finds himself embroiled in strange supernatural occurrences while transporting hostages through a detour.   

Of the films casting, OBrien chose to bring on del Mar due to having worked with her in the past and chose Hayter due to being familiar with his work. 

==Synopsis== Maria del Mar), and Jackie (Casey Hudecki) are kidnappers trying to deliver their two captives to Mr. Arkadi (Frank Moore), their employer and a powerful mob boss. The trip is long and things arent helped out by Toby being easy to rile up. They decide to take a detour in the hopes of making it to their destination that much quicker, but things take a turn for the worse when one of their captives is accidentally killed and the group finds that they have stumbled into something supernatural.

==Cast==
* David Hayter as Toby McTeague Maria del Mar as Cally
* Casey Hudecki as Jacinta "Jackie"
* Amanda Joy Lim as Kanako Kobayashi
* Frank Moore as Mr. Arkadi
* Adrienne Kress as Callys Girlfriend
* Samantha Wan as Suki Tsuburaya
* Shara Kim as Demon
* Craig Porritt as The Caretaker

==Reception==
Shock Till You Drop panned the film overall, criticizing the films script and direction, as they felt that OBrien "goes overboard with the tricks and gimmicks."  Bloody Disgusting was also highly critical of the film and commented that they saw the film as being "weird for weirdness sake, without any real thematic or pulpy endgame in mind" and wrote that "It’s like filmmaker Joseph O’Brien is pounding you over the head with his alleged self-importance without having anything to back it up. It’s a frustrating experience."  In contrast, Twitch Film gave an overly favorable review for Devils Mile, praising the films visual elements, script, and post effects, stating that "OBrien displays the right amount of patience to get the right amount of tension out of his audience." 

== References ==
 

== External links ==
*  

 
 
 