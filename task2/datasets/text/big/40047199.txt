How to Use Guys with Secret Tips
{{Infobox film
| name           = How to Use Guys with Secret Tips 
| image          = File:How_to_Use_Guys_with_Secret_Tips_poster.jpg
| director       = Lee Won-suk
| producer       = Kim Hyun-shin   Eo Ji-yeon   Yoon Chang-sook 
| writer         = Lee Won-suk   Noh Hye-young   Ha Su-jin
| starring       = Lee Si-young   Oh Jung-se   Park Yeong-gyu
| music          = Mowg
| cinematography = Kim Sun-ryung
| editing        = Kim Chang-ju   Kim Woo-hyeon
| distributor    = Showbox
| released       =  
| runtime        = 116 minutes
| country        = South Korea
| language       = Korean
| budget         =  
| gross          =   
| film name      = {{Film name
 | hangul         = 남자사용설명서 
 | hanja          = 
 | rr             = Namja Sayongseolmyungseo 
 | mr             = }}
}}

How to Use Guys with Secret Tips ( ; lit. "A Manual on How to Use Men" ) is a 2013 South Korean romantic comedy film starring Lee Si-young and Oh Jung-se, and directed by Lee Won-suk.

==Plot==
For the past five years, Choi Bo-na (Lee Si-young) has been overworked and underpaid as a second assistant director for TV commercials. Sorely lacking social skills, self-confidence, fashion sense, and aesthetic polish, she is casually disregarded by her work colleagues and knows that her career is going nowhere. One night, she hits rock bottom. Stranded on a beach in the middle of nowhere after a grueling outdoor shoot, she comes across an eccentric middle-aged man selling inspirational videos. He urges her to buy his masterwork, "A Manual on How to Use Men," telling her that it will change her life and give her the skills she need to find success and happiness. She initially had doubts but buys the video eventually. After reluctantly following the step-by-step instructions of self-styled guru Dr. Swarski (Park Yeong-gyu), Bo-nas life takes a drastic turn. She begins to experience professional success and men start flocking to her (including top hallyu star, Lee Seung-jae (Oh Jung-se), who initially treated Bo-na with indifference).

==Cast==
* Lee Si-young as Choi Bo-na 
* Oh Jung-se as Lee Seung-jae
* Park Yeong-gyu as Dr. Swarski
* Kim Jung-tae as Woo Sung-chul, Bo-nas ex
* Lee Won-jong as Yook Bong-ah, director
* Bae Sung-woo as CEO Jin, Seung-jaes manager
* Jun-seong Kim as Oh Ji-hoon
* Kim Min-jae as assistant director Jo Seung-hwan, assistant director
* Kyung Soo-jin as Kim Mi-ra, office cutie
* Ahn Yong-joon as Sung-jae
* Cheon Jin-ho as Jong-seok, Seung-jaes assistant
* Yang Yoon-young as Yoon Ji-eun, actress
* Hwang In-chung as Seung-jaes stylist
* Anton as model in video
* Tanya as model in video
* Kim Young-woong as director of photography
* Kim Kyung-jin as man trying to get into taxi
* Lee Mu-young as morning TV program MC
* Ryu Hyun-kyung as woman who spots Seung-jae near Bo-nas apartment
* Sa-hee as Yoon-hee
* Ji Chang-wook as Hong-joon
* Park Sung-taek as advertising agency PD
* Yoon Seok-joo as photographer
* Cha Jong-ho as cash replacement

==Production==
This was the feature directorial debut of Lee Won-suk, a graduate of the American Film Institute. Lee said the film was initially a black comedy, but during its seven-year pre-production, he made compromises in order to make the film more appealing commercially. Nevertheless he retained the satirical bent and issues he wanted to highlight, such as gender inequality, and the societal practice where shrewd people are more likely to succeed than those who simply work hard. Though classified as a romantic comedy, Lee called it more of a "fantasy," saying, "In the end, she gets everything she wanted. But that does not happen in real life no matter how earnestly people live their lives." 
 screen graphics and animation in the films backgrounds and visual patterns.  

==Reception== New World and Miracle in Cell No. 7. 

Koreanfilm.org praised the films "great comic timing," the "charismatic" performances by its lead actors and its "multitude of gags that are genuinely funny." The review said it felt "fresh and new, but it is also simply a very well executed film. In a genre that looks easy, but is actually quite challenging, this is a significant accomplishment."   

Twitch Film particularly noted the films "unique, vibrant and endlessly creative aesthetic," and director Lees "keen sense of style." It also said "one of the films great strengths is its excellent soundtrack, which runs the gamut of indie music, pop and club beats, not to mention Maurice Ravel|Ravel." 

In April 2013, the film won the Audience Award at the 15th Far East Film Festival in Udine, Italy, and director Lee Won-suk also received the Golden Mulberry Award.   In August 2013, it won the Bronze Prize for Best Asian Feature at the Fantasia Festival. 

==References==
 

==External links==
*    
*    
*    
*  
*  
*  

 
 
 