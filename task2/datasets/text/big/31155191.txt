Sreerama Pattabhishekam
{{Infobox film 
| name = Sreeraama Pattaabhishekam
| image =
| caption =
| director = GK Ramu
| producer = P Subramaniam
| writer = Mythology Nagavally R. S. Kurup (dialogues)
| screenplay = Nagavally R. S. Kurup
| starring = Prem Nazir Kaviyoor Ponnamma Thikkurissi Sukumaran Nair Prem Nawas
| music = Br Lakshmanan
| cinematography =
| editing = N Gopalakrishnan
| studio = Neela
| distributor = Neela
| released =  
| country = India Malayalam
}}
 1962 Cinema Indian Malayalam Malayalam film, directed by GK Ramu and produced by P Subramaniam. The film stars Prem Nazir, Kaviyoor Ponnamma, Thikkurissi Sukumaran Nair and Prem Nawas in lead roles. The film had musical score by Br Lakshmanan.   

==Cast==
*Prem Nazir as Sree Raman
*Kaviyoor Ponnamma  as Kaushalya
*Thikkurissi Sukumaran Nair as Dasaradhan
*Prem Nawas as Lakshmanan
*Vasanthi as Ahalya GK Pillai as Vishwamithran
*Miss Kumari as Kaikeyi
*K. V. Shanthi as Surpanakha/Kamavalli
*T. K. Balachandran as Bharathan
*Aranmula Ponnamma as Kausalya
*Jose Prakash as Janakaraja
*Kaduvakulam Antony as Badan
*Kottarakkara Sreedharan Nair as Ravanan
*Vanchiyoor Radha as Mallika
*Adoor Pankajam as Mandara Hari as Shatruganan
*SP Pilla as Vaidyar
*Sukumari dance appearance in song sequence

==Soundtrack==
The music was composed by Br Lakshmanan and lyrics was written by Thirunayinaarkurichi Madhavan Nair and Thunchathezhuthachan. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Chollu Sakhi || P Susheela || Thirunayinaarkurichi Madhavan Nair || 
|- 
| 2 || Lankesha || P. Leela || Thirunayinaarkurichi Madhavan Nair || 
|- 
| 3 || Mama Tharuni || Kamukara || Thirunayinaarkurichi Madhavan Nair || 
|- 
| 4 || Mohini Njaan || S Janaki || Thirunayinaarkurichi Madhavan Nair || 
|- 
| 5 || Naaduvaazhuvaan || K. J. Yesudas, P Susheela, Kamukara, AP Komala, Chorus || Thirunayinaarkurichi Madhavan Nair || 
|- 
| 6 || Ninne Piriyukil || PB Sreenivas || Thirunayinaarkurichi Madhavan Nair || 
|- 
| 7 || Parannu Parannu || P Susheela, Kamukara || Thirunayinaarkurichi Madhavan Nair || 
|- 
| 8 || Pokunnitha || PB Sreenivas, Chorus || Thirunayinaarkurichi Madhavan Nair || 
|- 
| 9 || Ponnittu Porulittu || P Susheela, Kamukara || Thirunayinaarkurichi Madhavan Nair, Thunchathezhuthachan || 
|- 
| 10 || Pookkaatha Kaadukale (Theyyare) || K. J. Yesudas, Chorus || Thirunayinaarkurichi Madhavan Nair || 
|- 
| 11 || Raajaadhi Raaja || AP Komala, Jikki, Vaidehi || Thirunayinaarkurichi Madhavan Nair || 
|- 
| 12 || Raamaraama Seetha || K. J. Yesudas, Chorus || Thirunayinaarkurichi Madhavan Nair || 
|- 
| 13 || Sooryavamshathin || P. Leela, Kamukara || Thirunayinaarkurichi Madhavan Nair || 
|- 
| 14 || Thaathan Nee Mathavu || PB Sreenivas || Thirunayinaarkurichi Madhavan Nair || 
|- 
| 15 || Vatsa Soumithre || Kamukara || Thunchathezhuthachan || 
|}

==References==
 

==External links==
*  

 
 
 


 