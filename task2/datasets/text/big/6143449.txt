You Can't Win 'Em All
{{Infobox film
| name           = You Cant Win Em All
| image          = You Cant Win Em All - 1970 - Poster.png Frank McCarthy Peter Collinson
| producer       = Gene Corman
| writer         = Leo Gordon
| narrator       =
| starring       = Tony Curtis Charles Bronson Fikret Hakan Salih Güney Michèle Mercier
| music          = Bert Kaempfert
| cinematography = Ken Higgins
| editing        =
| studio         = SRO
| distributor    = Columbia Pictures
| released       = 24 July 1970
| runtime        = 97 min.
| country        = United Kingdom
| language       = English
| budget         =
}}
 1970 war Peter Collinson, starring Tony Curtis and Charles Bronson as two American soldiers in 1922 Turkey who protect the three daughters of a Turkish governor while thwarting a Turkish army colonels attempt to take gold on a train the two soldiers happen to be on. The setting is the time of the Greco-Turkish War (1919–1922).

== Cast ==
* Tony Curtis: Adam Dyer
* Charles Bronson: Josh Corey
* Michèle Mercier: Aila
* Grégoire Aslan: Osman Bey
* Fikret Hakan: Colonel Elci 
* Salih Güney: Captain Enver Patrick Magee: Mustapha Kayan (really Mustapha Kemal Atatürk)
* Tony Bonner: Reese
* John Acheson: Davis Horst Janson: Wollen 
* Leo Gordon: Bolek
* Reed De Rouen: US naval officer
* Paul Stassino: Major

==Production notes==
Aircraft sequences were flown and coordinated by Charles Boddington and Lewis Benjamin. The aircraft were owned by ex-RCAF pilot Lynn Garrison who shipped several of his SE-5 replicas from Ireland to Turkey for the production. They were previously featured in The Blue Max and Darling Lili and would go on to star in Von Richthofen and Brown, Zeppelin, The Great Waldo Pepper, and numerous TV commercials.

==See also== Vera Cruz, a 1954 film with a similar plot set in the Mexican Revolution.

==Bibliography==
 
* Benjamin, Lewis. "Turkish Delight!". Aeroplane Monthly. October 2008, Vol. 36, Issue 426, ISSN 0143-7240, pp.&nbsp;32–37.
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 

 