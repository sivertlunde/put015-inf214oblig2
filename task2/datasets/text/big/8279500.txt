The Phantom Rider (Republic serial)
 
 
{{Infobox film
| name           = The Phantom Rider
| director       = Spencer Gordon Bennet Fred C. Brannon
| producer       = Ronald Davidson
| writer         = Albert DeMond Basil Dickey Jesse Duffy Lynn Perkins Barney Sarecky Robert Kent Peggy Stewart LeRoy Mason George J. Lewis Kenne Duncan Hal Taliaferro Chief Thundercloud Roy Barcroft
| cinematography = Bud Thackery
| distributor    = Republic Pictures
| released       = {{Film date|1946|01|26|U.S. serial|ref1= {{cite book
 | last = Mathis
 | first = Jack
 | title = Valley of the Cliffhangers Supplement 
 | year = 1995
 | publisher = Jack Mathis Advertising
 | isbn = 0-9632878-1-8
 | pages = 3, 10, 88–89
 | chapter = 
 }} |1954|10|11|U.S. re-release|ref2= }}
| runtime         = 12 chapters (167 minutes) 
| country         = United States English
| budget          = $140,207 (negative cost: $138,925) 
| awards          =
}}
 Republic Movie serial.  It was later re-released under the new title Ghost Riders of the West.

==Plot==
Dr Jim Sterling attempts to create a police force on the Big Tree Indian Reservation. However, his efforts face sabotage, secretly directed by the apparently friendly Indian Agent Fred Carson, whose gang is currently able to rob stagecoaches wagons without opposition.  In order to defeat his enemies, Sterling adopts the name and costume of the legendary "Phantom Rider."

==Cast== Robert Kent as Dr Jim Sterling and The Phantom Rider Peggy Stewart as Doris Shannon, school teacher
*LeRoy Mason as Fred Carson, villain secretly sabotaging the reservation to maintain his interests
*George J. Lewis as Blue Feather, Sterlings sidekick
*Kenne Duncan as Ben Brady
*Hal Taliaferro as Nugget, miner
*Chief Thundercloud as Chief Yellow Wolf
*Tom London as Ceta
*Roy Barcroft as The Marshal
*Monte Hale as Cass
*Hugh Prosser as Keeler

==Production==
The Phantom Rider was budgeted at $140,207 although the final negative cost was $138,925 (a $1,282, or 0.9%, under spend).   It was filmed between 25 July and 22 August 1945.   The serials production number was 1499. 

===Stunts===
*Wayne Burson
*Tommy Coats Fred Graham Cliff Lyons
*Ted Mapes
*Eddie Parker
*Post Park (coach) Tom Steele (fights)
*Duke Taylor (horseback)
*Dale Van Sickel (horseback) Henry Wills (horseback)
*Bill Yrigoyen (horseback/fights)
*Joe Yrigoyen (horseback/fights)

===Special Effects===
Al special effects by the Lydecker brothers.

==Release==

===Theatrical===
The Phantom Riders official release date is 26 January 1946, although this is actually the date the sixth chapter was made available to film exchanges.   The serial was re-released on 11 October 1954, under the new title Ghost Riders of the West, between the first runs of Man with the Steel Whip and Panther Girl of the Kongo. 

==Chapter titles==
# The Avenging Spirit (20min)
# Flaming Ambush (13min 20s)
# Hoofs of Doom sic|  (13min 20s)
# Murder Masquerade (13min 20s)
# Flying Fury (13min 20s)
# Blazing Peril (13min 20s)
# Gauntlet of Guns (13min 20s)
# Behind the Mask (13min 20s) - a clipshow|re-cap chapter
# The Captive Chief (13min 20s)
# Beasts at Bay (13min 20s)
# The Death House (13min 20s)
# The Last Stand (13min 20s)
 Source:   {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | year = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | chapter = Filmography
 | page = 242
 }} 

==See also==
* List of film serials
* List of film serials by studio

==References==
 

==External links==
* 
* 

 
{{succession box  Republic Serial Serial 
| before=The Purple Monster Strikes (1945 in film|1945)
| years=The Phantom Rider (1946 in film|1946)
| after=King of the Forest Rangers (1946 in film|1946)}}
 

 

 
 
 
 
 
 
 
 