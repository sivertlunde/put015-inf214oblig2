Anbe Vaa (2005 film)
{{Infobox film
| name = Anbe Vaa (2005 film)
| image = Anbe vaa 2005.jpg
| caption = Theatrical poster
| director = K. Selva Bharathy
| writer =K. Selva Bharathy Vivek Rekha Rekha 
| producer =Dragon Movie Productions 
| music = D. Imman
| editor = Suresh Urs
| released =  
| runtime = 
| country = India
| language = Tamil
}} Vivek and Rekha in the lead roles. Produced by Dragon Movie Productions, the film released on October 18, 2005 to average reviews and had a decent run at the box office. 

== Plot Summary ==
The story is about a rich heiress (Rekha) whose son (Thendral) is a good-for-nothing guy who spends his time drinking and making merry with friends and his uncle, Nandha (Vivek). In fact, it is Nandha who sorts out all his problems. Together they go to a village to see a girl under the orders of the rich heiress, who feels that marriage would make her son a responsible man.

They meet the heroine (Sridevika) and it is love at first sight for the rich heiresss son and soon he gets married to her. But post-marriage, the couple starts fighting as they find that they are not compatible. In the end, Nandha sorts out everything and unites the couple.

== Cast ==
* Thendral
* Sridevika Vivek as Nandha Rekha

== Soundtrack ==
Music is composed by D. Imman. 

{{Track listing
| extra_column  = Singer(s)
| all_lyrics    = 
| total_length  = 26:44

| title1     = Alek, Alek
| extra1     = Suchitra, D. Imman
| length1    = 04:28

| title2     = Kaal Koluse
| extra2     = Harish Raghavendra, Balaram
| length2    = 04:32

| title3     = Loyola
| extra3     = Balaram
| length3    = 04:46

| title4     = Naan Unnai
| extra4     = Shobha Chandrasekhar
| length4    = 04:05

| title5     = Olib Laila
| extra5     = Kalpana, Timmy
| length5    = 04:40

| title6     = Pidikavilaida
| extra6     = Karthik (singer)|Karthik, Srilekha Parthasarathy
| length6    = 04:13
}}

== Reception ==
The film received average reviews from critics.

Sify wrote, "Anbe Vaa depends largely on Vivek’s scintillating comedy show. His antics spice up the proceedings and the punch lines make you smile. Sreedevika is not bad for a first timer. The film drags towards the end and music of D.Imman is a big let down. On the whole, this Selvabharathy directed film is funny in parts thanks to the one-man laugh brigade called Vivek." 

Thiraipadam.com wrote, "Anbe Vaa is one of those movies that benefits from low expectations. The mug of its debutant hero doesnt inspire much confidence and its only Viveks name on the credits that gives the film some credibility. But the movie avoids the easy route(of setting the movie in a college and loading the movie with vulgarity) that most movies with newcomers take and aims higher. It doesnt reach that high in the end but the effort deserves some credit." 

== References ==
 

 
 
 
 