Captains Courageous (1937 film)
{{Infobox film
| name = Captains Courageous
| image = File:Captains Courageous poster.jpg
| image_size =
| caption = Film poster
| director = Victor Fleming
| writer = Rudyard Kipling (novel)
| starring = Freddie Bartholomew Spencer Tracy
| music = Franz Waxman
| editing = Elmo Veron
| cinematography = Harold Rosson
| studio   = Metro-Goldwyn-Mayer
| distributor = Metro-Goldwyn-Mayer
| released =  
| runtime = 115 minutes
| country = United States English
|budget = $1,645,000  .  gross = $3,133,000 
}}
 1937 Metro-Goldwyn-Mayer the novel by Rudyard Kipling, it had its world premiere at the Carthay Circle Theatre in Los Angeles. The movie was produced by Louis D. Lighton and directed by Victor Fleming. Filmed in black-and-white, Captains Courageous was advertised by MGM as a coming-of-age classic with exciting action sequences.

==Plot==
Harvey Cheyne (Freddie Bartholomew) is the spoiled son of business tycoon Frank Burton Cheyne (Melvyn Douglas). He is shunned by his classmates at a private boarding school, and eventually suspended for  bad behavior. His father therefore takes his son with him on a business trip to Europe via a trans-Atlantic steamship. En route, Harvey falls overboard in the Grand Banks of Newfoundland. He is rescued by a Portuguese-American fisherman, Manuel Fidello (Spencer Tracy), and taken aboard the fishing schooner Were Here. Harvey fails to persuade captain Disko Troop (Lionel Barrymore) to take him ashore, nor can he convince him of his wealth; but the captain offers him a temporary membership in the crew until they return to port, and Harvey eventually accepts. Befriended by Captain Troops son, Dan (Mickey Rooney), he becomes acclimated to the fishing lifestyle. In the climactic race back to the Gloucester, Massachusetts port against a rival schooner, the Jennie Cushman, Manuel climbs to the top of the mast to furl the sail, but is mortally injured when the mast cracks and he is plunged into the water, caught irreversibly in the tangled rope and the topsail canvas, and drowns. Eventually, the schooner returns to port and Harvey is reunited with his father, whom he surprises by his own greater maturity. Harvey himself mourns Manuels death, until later. 

 

==Cast==
*Freddie Bartholomew as Harvey Cheyne
*Spencer Tracy as Manuel Fidello
*Lionel Barrymore as Captain Disko Troop
*Melvyn Douglas as Frank Burton Cheyne
*Charley Grapewin as Uncle Salters
*Mickey Rooney as Dan Troop
*John Carradine as Long Jack
*Oscar OShea as Captain Walt Cushman
*Jack La Rue as Priest (as Jack LaRue)
*Walter Kingsford as Dr. Finley
*Donald Briggs as Bob Tyler
*Sam McDaniel as "Doc" (as Sam McDaniels)
*Bill Burrud as Charles Jamison (as Billy Burrud)

==Box Office==
According to MGM records the film earned $1,688,000 in the US and Canada and $1,445,000 elsewhere resulting in a profit of $355,000. 

==Awards==
Spencer Tracy won the Academy Award for Best Actor for his work in this film.  The movie was also nominated for three other Academy Awards: Best Picture - Louis D. Lighton, producer Best Film Editing - Elmo Veron Best Writing, Screenplay - Marc Connelly, John Lee Mahin and Dale Van Every
 MGM Home Warner Home Videos DVD of the film on January 31, 2006.

==In popular culture==
Holden Caulfield, protagonist of the 1951 novel The Catcher in the Rye, is thought to look like Harvey Cheyne, as in the book a prostitute tells Caulfield that he looks like the boy who falls off a boat in a film starring Spencer Tracy, though the film is not mentioned by name.

==See also==
*Lionel Barrymore filmography

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 