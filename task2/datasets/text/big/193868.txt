Sunrise at Campobello
 
{{Infobox film
| name           = Sunrise at Campobello
| image          = Sunrise at Campobello film.jpg
| caption        = VHS cover
| screenplay     = Dore Schary
| based on       =  
| starring       = Ralph Bellamy Greer Garson Hume Cronyn
| music          = Franz Waxman
| cinematography = Russell Harlan
| editing        = George Boemler
| director       = Vincent J. Donehue
| producer       = Dore Schary
| distributor    = Warner Bros.
| country        = United States
| released       =  
| runtime        = 144 minutes
| language       = English
| budget         = 
}}
 Broadway Play play of the same name, the film was directed by Vincent J. Donehue and starred Ralph Bellamy, Greer Garson, Hume Cronyn and Jean Hagen.

Produced with the cooperation of the Roosevelt family, Eleanor Roosevelt was present on the set during location shooting at the Roosevelt estate in Hyde Park, New York.

==Plot==
Beginning at the Roosevelt familys vacation home on Campobello Island, New Brunswick, Canada (on the border with Maine), in the summer of 1921, Franklin is depicted in early scenes as vigorously athletic, enjoying games with his children and sailing his boat.
 James pushed his fathers wheelchair near to the podium.

The play and film both omit any mention of Warm Springs, Georgia and of Roosevelts stay there, and of Roosevelts creation of a rehabilitation center at Warm Springs.
==Historical context==
 
Before and during Franklin D. Roosevelts Presidency of Franklin D. Roosevelt|presidency, the extent of his disability was carefully concealed from the public. Sunrise at Campobello depicts the debilitating effects of FDRs illness to a greater extent than had been previously disclosed by the media.

FDRs attending physician, Dr. William Keen, believed it was polio and commended Eleanors devotion to the stricken Franklin during that time of travail. "You have been a rare wife and have borne your heavy burden most bravely," he said, proclaiming her "one of my heroines". 

==Cast==
  
* Ralph Bellamy – Franklin Delano Roosevelt
* Greer Garson – Eleanor Roosevelt
* Hume Cronyn – Louis Howe Marguerite "Missy" LeHand Sara Roosevelt
* Alan Bunce – Governor Alfred E. Smith
* Tim Considine – James Roosevelt Anna Roosevelt
 
* Frank Ferguson – Dr. Bennett
* Pat Close – Elliott Roosevelt Franklin Roosevelt, Jr. Johnny Roosevelt
* Lyle Talbot – Mr. Brimmer David White – Mr. Lassiter
* Walter Sande – Captain Skinner
 

==Awards==
Greer Garson won the Golden Globe Award for Best Actress - Motion Picture Drama.    

The film was also entered into the 2nd Moscow International Film Festival.   
 Academy Awards===
;Nominations  Best Actress: Greer Garson
* ; Set Decoration: George James Hopkins
* 
* , Sound Director

==See also==
* Franklin D. Roosevelts paralytic illness

==References==
 

==External links==
 
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 