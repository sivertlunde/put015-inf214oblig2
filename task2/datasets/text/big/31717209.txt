Durgaa
 
{{Infobox film
| name = Durgaa
| image =
| image_size =
| caption = 
| director       = Shibu Mitra
| producer       = S. K. Kapoor
| writer         = Faiz Saleem
| narrator       = Pran Raj Babbar Ashok Kumar Aruna Irani
| music          = Sonik-Omi
| cinematography = Jal Mistry
| editing        = 
| studio         =
| distributor    = Kapur Films
| released       = 1985
| runtime        =
| country        = India
| language       = Hindi
| country        = India
| revenue        = 2,00,00,000
| preceded_by    =
| followed_by    =
| website        =
}}
Durgaa (  Hindi film directed by Shibu Mitra, released on 12 July, Bollywood films of 1985|1985.  The film stars Hema Malini in the title role. This action-crime genre film revolves around Durgaa, a soft spoken girl who is exploited by various men in her life. At first, Durgaa falls in love with a young man (Raj Babbar) and even marries him. But she realises post marriage that her husband had married her only as an excuse to sleep with her. Durgaa sees her husband romancing and sleeping with other girls. Her husband tries to get Durgaa into trouble by hatching a plan to get her name tagged as a prostitute. She is then accused of murdering an innocent girl but seeks the help of a senior good looking man (Pran) to bear her court expenses so that she can fight the injustice against her. She is introduced to Mohan Babu (Rajesh Khanna), who has the reputation of winning every case he handles and so is hired as the advocate representing Durgaa. But Durgaa is not aware of the motive of the old man in helping her. The rest of the story is why Mohan was hired as the advocate by that old man, what the motive of the old man helping Durgaa was, will Durgaa ever forgive her husband, who murdered the girl etc. In the end Durgaa takes revenge on the devils in her life. Rajesh Khanna plays the savior of Durgaa in the last 50 minutes of the film, playing the Advocate representing Durgaa in court, and marries Durgaa in the end.The music is by Sonik Omi. 

==Cast==
*Rajesh Khanna ....  Advocate
*Hema Malini ....  Durgaa
*Ashok Kumar ....
*Pran (actor)|Pran....
*Raj Babbar ....
*Aruna Irani ....

==Track list==
#  Kuch Kahne Wala Tha Main  : Mohammad Rafi, Lata Mangeshkar
#  Ab Naya Tamasha : Asha Bhosle
#  Chinak Chinak Chu : Asha Bhosle
#  Hai Chanji Kar Deo Ki  : Alka Yagnik
#  O Nari Dukhiyaari  : Mahendra Kapoor
# Sasural Mein To Hogi : Dilraj Kaur, Chandrani Mukherjee
# Sheram Wali Tera Darbar : Asha Bhosle

==References==
 

==External links==
*  

 
 
 
 