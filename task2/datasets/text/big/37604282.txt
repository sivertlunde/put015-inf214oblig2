The Castanet Club
 
{{Infobox film
| name           = The Castanet Club
| image          =
| caption        =
| director       = Neil Armfield
| producer       = Glenys Rowe
| writer         =
| starring       = Steve Abbott
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        =
| country        = Australia
| language       = English
| budget         =
| gross          = A$25,100 (Australia) 
}} Newcastle cabaret troupe, The Castanet Club. The Castanet Club started life in the gritty steel town of Newcastle, NSW, Australia, in 1982.

The town was like a kind of artistic blast furnace as well. Precious metal was separated from the slag by audiences who demanded no-bull entertainment. It was a matter of either “do your business, or get off the pot”. This earthy ethos which dominated the Second City of NSW also produced such talents as Silverchair, The Screaming Jets and The Marching Koalas.

Great original work often comes from the regions. Cities like Newcastle punch above their weight, because talent there has the licence to fail for a while, as it develops and grow. Audiences forgive initial failures, because frankly there’s not much else going on and, besides, they know everyone up on stage.

And so it was with The Castanet Club. Newcastle was the proving ground and before long a successful Comedy-Cabaret-Big-Band had crystallized around the nine core players: Stephen Abbott aka Johnny Goodman, the-MC, Glenn Butcher, doing lead vocals as the incomparable lounge lizard Lance Norton Angela Moore, as Betty B-Plate & Shirley Purvis, Russell Cheek, as Tron Wexler& Barnsey’s roadie Doug "Gargoyle" Ormerod, Warren Coleman as Bowling Man and the sexually repressed Pastor Noel Anderson, Rodney Cambridge on drums, Penny Biggins, better known as Doris Crawley on accordion, Kathy Bluff, the Kid Kalamai on violin, Maynard as, well, just Maynard and that was enough thank you very much, and Peter Mahony as Urman Erstwhile up the back.

The Castanet Club became the toast of Newcastle. Thick-aired venues were packed with audiences dancing, screaming and laughing. Ready to look the world in the eye, they stormed the Edinburgh Festival where audiences could not quite believe the joy, energy and camaraderie of these refugees from the antipodes.

The players had an ironic 60s take on everything, sporting velvet suits, plastic Carnaby Street skirts or 60s bowling shirts. They were an hilarious kaleidoscope of colour, music, movement, satire and goodwill.

As the years passed, the band developed a swathe of unique comic characters, (half the band had its background in theatre performance) and the material became a unique fusion of character, song and celebration.

The Castanets were young, they were free… and they got better and better: wrote better songs, got funnier, made up better dance routines. Then they made their film! …then, in 1991 they broke up.

==References==
 

==External links==
* 
 
 
 
 
 