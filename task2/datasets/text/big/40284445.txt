Lithivm
{{Infobox film
| name           = Lithivm
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = 
| director       = David Flamholc
| producer       = Leon Flamholc
| writer         = David Flamholc
| screenplay     = 
| story          = 
| based on       = 
| narrator       = 
| starring       = Fredrik Dolk Johan Widerberg Björn Granath
| music          = Kenneth Cosimo
| cinematography = Mårten Nilsson
| editing        = Leon Flamholc
| studio         = Caravan Film AB
| distributor    = Sonet Film
| released       = August 28, 1998  
| runtime        = 127 min 
| country        = 
| language       = 
| budget         = $500 000 (estimated)
| gross          = 
}}
 horror thriller thriller from 1998 directed by David Flamholc and Swedish voice acting veteran Fredrik Dolk and Johan Widerberg. The title of the film is a stylised spelling of Lithium which is used to treat people with schizophrenia.

==Plot==
 bipolar Hanna gets an internship at a newspaper but is not satisfied with her duties that involve reading letters. She is pestered by her psychopathic on and off lover Martin and comes to believe that the divorced schizophrenic Dag is a serial killer.

==Cast==

*Agnieszka Koson - Hanna (as Agnieszka)
*Fredrik Dolk - Dag Tingström
*Johan Widerberg - Martion
*Yvonne Lombard - Hasse
*Björn Granath - Lt. Henrik Laurentsson
*Marika Lagercrantz - Margareta
*Göran Forsmark - Werner
*Ola Wahlström - Greidner
*Måns Westfelt - Birger
*Kent-Arne Dahlgren - Sten (Stone)
*Simon Paulsson - Filip
*Finn Kronsporre - Police

===Crew===

*Production Designer Erika Ökvist
*Assistant Director Gita Mallik
*Carpenter Ayden Demirkiran	
*Sound Effects by Monir Eriksson
*Stunt Cordinator Lars Lundgren
*Best girl Rebecka Liljeberg
*Grip operated by Peter Pettersson
*Sound Mixer Berndt Frithiof
*First Assistant Camera Mikaël Meisen-Dietmann
*Stunts by Joachim von Rost
*Production Assistant Martin Goldberg
*Production Assistant Marie Eklinder

==About the film==

The film was shot in Stockholm using 16&nbsp;mm reverse film that was later blown up to 35mm.   This created much grain. The cinematography Flamholc and Mårten Nilsson was going for, was a further stylisation of the techniques they had used in Nightbus 807. Lithvim was the third and final film Flamholc made with Fredrik Dolk and his final fiction film to date. The film did receive support from the Swedish Film Institute.  The film makes heavy use of Air by Johann Sebastian Bach.

==Response==

The film bombed on the Swedish box office and was declared one of the worst Swedish films of all time.  The film did on the other hand win an award at the Hollywood Film Festival. 

==References==
 

 
 