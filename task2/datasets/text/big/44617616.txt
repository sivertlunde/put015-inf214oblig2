Getting Even (1909 film)
 
{{Infobox film
| name           = Getting Even
| image          = 
| caption        = 
| director       = D. W. Griffith
| producer       = 
| writer         = D. W. Griffith
| starring       = Billy Quirk Mary Pickford
| music          = 
| cinematography = Billy Bitzer
| editing        = 
| distributor    = 
| released       =  
| runtime        = 6 minutes
| country        = United States
| language       = Silent
| budget         = 
}} silent short short comedy film directed by D. W. Griffith. A print of the film exists in the film archive of the Library of Congress.   

==Cast==
*Billy Quirk - Bud
*Mary Pickford - Miss Lucy James Kirkwood - Jim Blake
*Edwin August Florence Barker
*Kate Bruce - Party Guest
*Verner Clarges - Party Guest
*John R. Cumpson - miner
*Arthur V. Johnson - Party Guest
*Florence La Badie George Nichols - miner
*Anthony OSullivan - miner
*Lottie Pickford - Party Guest
*Gertrude Robinson - Party Guest
*Mack Sennett - miner
*Henry B. Walthall - miner

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 