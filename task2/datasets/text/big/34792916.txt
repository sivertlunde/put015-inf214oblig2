Romeo and Juliet (2013 film)
 
 
{{Infobox film
|name=Romeo & Juliet
|image=Romeo and Juliet 2013 film.png
|image_size=220px
|border=yes
|alt=
|caption=US theatrical release poster
|director=Carlo Carlei
|producer=Ileen Maisel Nadja Swarovski Julian Fellowes
|screenplay=Julian Fellowes based on= 
|starring=Douglas Booth Hailee Steinfeld Damian Lewis Kodi Smit-McPhee Ed Westwick Paul Giamatti
|cinematography=David Tattersall
|music=Abel Korzeniowski
|editing=Peter Honess Swarovski Entertainment
|distributor=Icon Productions Entertainment Film Distributors   Relativity Media      Universal Studios  
|released= 
|runtime=118 minutes  
|country=United Kingdom Italy Switzerland
|language=English
|budget=Pound sterling|£15 million (United States dollar|$24 million)
|gross=$1,162,635   
}} romantic tragedy Franco Zeffirellis adaptation of Shakespeares tragedy, this film uses the traditional setting of Renaissance Verona,     but, unlike previous major film adaptations,  this film only follows the plot and only uses some of Shakespeares traditional dialogue. This has led to a controversy, with several critics denouncing the films advertising as misleading, and losing the essence of the play. 

==Plot==
During the Late Middle Ages in Verona, two wealthy families, the Montagues and Capulets, have been feuding for centuries. One day at the market place, the feuding families start a brawl which infuriates the Prince and he threatens that if the peace of Verona is disturbed again, he shall take their lives. Meanwhile, Romeo is a young Montague who is in love with Lord Capulets niece, Rosaline. 
Romeos friend, Benvolio persuades him to forget Rosaline but Romeo rebuffs him.

Later that night, there is a party held by Lord Capulet. Romeo sneaks in with Benvolio and Mercutio hoping to meet Rosaline. Instead, Romeo sees Juliet who is Lord Capulets daughter and falls in love with her. Juliet feels the same and they dance. They go together to a quiet place and share a passionate kiss. Juliets Nurse interrupts and when Romeo talks to the nurse, he discovers that Juliet is a Capulet.

After the party ends, Romeo sneaks into Juliets garden secretly where he witnesses Juliet expressing her love for Romeo. They have a romantic time and decide to get married the next day. Romeo seeks help from Friar Laurence to wed them and the Friar agrees thinking that their love may end the violent war between Capulets and Montagues. They perform the ceremony and afterwards Juliet returns home. Romeo catches up with Mercutio and Benvolio but they meet Tybalt and his men on the way. This starts another violent brawl during which Tybalt stabs Mercutio. Romeo is enraged and runs after Tybalt seeking revenge. They fight and Romeo slays Tybalt. As the result of this loss, the Prince banishes Romeo from Verona.

Meanwhile both families are filled with grief over their losses, especially Juliet. The Friar sends Romeo to Juliet so he can spend one last night with her. Romeo goes to Juliet and they consummate their marriage. Romeo leaves in haste the next morning. But Juliet is shocked when her father brings news of planning to wed Juliet with Count Paris. Juliet is resistant but her father threatens to disown her if she does not wed Paris. Juliet goes to Friar Laurence for help, threatening to take her life if the Friar does not have a solution. The Friar in return, gives her a potion that will put her in a deathlike sleep temporarily while he will tell Romeo about this and they shall run away together. Juliet is overjoyed and drinks the potion that night. Her parents are devastated and instead of her marriage, her funeral is planned. During the funeral, Benvolio sees Juliet and thinks she is dead and immediately runs off to tell Romeo.

Friars letter however, does not reach Romeo. And Benvolio tells Romeo of what he saw. Romeo is shocked and devastated and plans to 
take his life. He buys poison and goes to Juliet. He kisses Juliet one last time and drinks the poison and that is exactly when Juliet awakens. Romeo dies in her arms. And the Friar arrives but he is too late. He hears some guards coming and goes to hold them off. While Juliet is filled with grief and stabs herself with Romeos dagger. The Friar arrives and finds them both dead.

Their funeral is held together and the feud between Capulets and Montagues is finally ended. In the end, the Prince narrates "For there never was a story of more woe than this of Juliet and her Romeo."

==Cast==
*Douglas Booth as Romeo Montague 
*Hailee Steinfeld as Juliet Capulet 
*Damian Lewis as Lord Capulet
*Kodi Smit-McPhee as Benvolio Montague
*Ed Westwick as Tybalt
*Paul Giamatti as Friar Lawrence Nurse
*Christian Cooke as Mercutio
*Stellan Skarsgård as Prince Escalus of Verona
*Natascha McElhone as Lady Capulet
*Tom Wisdom as Count Paris
*Leon Vitali as Apothecary
*Laura Morante as Lady Montague
*Nathalie Rapti Gomez as Rosaline Capulet
*Tomas Arana as Lord Montague
*Simona Caparrini as a female guest Anton Alexander Abraham (House of Montague)

==Production==
Principal photography started on 3 February 2012 in Italy.  The film was shot at the grotto Sacro Speco in Subiaco, Lazio|Subiaco; Mantua;  Caprarola, Lazio; Cinecittà, Rome; and in Verona. The first pictures of the set were posted on Italian newspaper Gazzetta di Mantova on 14 February 2012.   Steinfeld finished filming her scenes on 7 March 2012. 

===Casting===
Ed Westwick was the first actor to read the script.   In April 2011, Hailee Steinfeld was said to be in talks for the lead role as Juliet in this adaptation.  Owing to Steinfelds young age, there was some concern she would be asked to appear nude in the film. Director Carlo Carlei explained "there was a lovemaking scene that included nudity for the married Romeo and Juliet. This script was written with a 20-year-old actress in mind. As soon as Hailee Steinfeld was cast, all nudity and lovemaking have been excised from the script. It will be romantic and age-appropriate for a 14-year-old."  Julian Fellowes added, "We did feel it would be nice to have romantic, married love, and that purity was an important part of the film. They dont make love until they have been married."  The role of Romeo was found in June 2011 when Douglas Booth was cast, beating out 300 other actors who were interested in the part.  Paul Wesley had been offered the role of Count Paris,  but it was announced in February 2012 that Tom Wisdom would play him. 

==Release== premiere was held in Hollywood on 24 September 2013 at the ArcLight Hollywood.  It was released in Australia on 13 February 2014.

===Critical reception===
The film has received negative reviews from critics, as it currently holds a 22% approval rating on aggregate review site  , based on 30 critics, indicating "mixed reviews".  Tony Howard, in his Around the Globe review writes that the film is "unique in its respect for the play, the actors, and young people".

==References==
 

==External links==
* 
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 