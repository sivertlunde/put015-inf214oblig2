The Pirates (2014 film)
{{Infobox film
| name           = The Pirates
| image          = 
| director       = Lee Seok-hoon
| producer       = Im Young-ho   Chun Sung-il
| writer         = Chun Sung-il   Choi Yi-young 
| starring       = Kim Nam-gil   Son Ye-jin 
| music          = Hwang Sang-jun
| cinematography = Kim Young-ho  
| editing        = 
| distributor    = Lotte Entertainment
| released       =  
| runtime        = 130 minutes
| country        = South Korea
| language       = Korean
| budget         =  
| gross          =   (international)
| film name      = {{Film name
| hangul         = 해적: 바다로 간 산적 
| hanja          = 
| rr             = Haejeok: Badaro Gan Sanjeok 
| mr             = }}
}}
 period adventure film starring Kim Nam-gil and Son Ye-jin.  

==Plot==
On the eve of the founding of the Joseon Dynasty, a whale swallows the Emperors Seal of State being brought to Joseon by envoys from China. With a big reward on whoever brings back the royal seal, mountain bandits led by Jang Sa-jung go out to sea to hunt down the whale. But he soon clashes with Yeo-wol, a female captain of pirates, and unexpected adventure unfolds.   

==Cast==
 
*Kim Nam-gil as Jang Sa-jung   
*Son Ye-jin as Yeo-wol  
*Yoo Hae-jin as Cheol-bong
*Lee Geung-young as So-ma
*Oh Dal-su as Han Sang-jil Kim Tae-woo as Mo Heung-gap 
*Park Chul-min as Monk
*Shin Jung-geun as Yong-gap
*Kim Won-hae as Chun-seop
*Jo Dal-hwan as San-man
*Jo Hee-bong as Oh Man-ho
*Jung Sung-hwa as Park Mo
*Choi Sulli as Heuk-myo
*Lee Yi-kyung as Cham-bok
*Ahn Nae-sang as Jeong Do-jeon
*Kim Kyeong-sik as Subforeman  Yi Seong-gye
*Jeon Bae-soo as Baek Seon-gi 
*Park Hae-soo as Hwang Joong-geun 
*Lee Do-yeon as young Yeo-wol 
*Kim Won-joong as Catfish 
*Song Yong-ho as Snakehead 
*Moon Seong-bok as Eel 
*Lee Gyu-ho as Mool-gom 
*Jeon Won-gyu as Pine cone 
*Lee Jae-ho as Cudgel 
*Kim Jae-hak as Straw 
*Kim Ian as Baek Chi
 

==Box office==
The Pirates sold 272,858 tickets during its first two days of release, placing second on the box office chart behind  .  After 17 days in theaters, it became the third Korean film in 2014 to reach 5 million admissions.  At the end of its run, the film reached List of highest-grossing films in South Korea|8,665,269 admissions, and also grossed   internationally. 

==International release== Cannes Film Market.  

==Awards and nominations==
{| class="wikitable"
|-
! Year
! Award
! Category
! Recipient
! Result
|- 2014
| rowspan=7|   51st Grand Bell Awards  
| Best Actress
| Son Ye-jin
|  
|-
| Best Supporting Actor
| Yoo Hae-jin
|  
|-
| Best Cinematography
| Kim Young-ho
|  
|-
| Best Art Direction
| Kim Ji-a
|  
|-
| Best Costume Design
| Kwon Yu-jin
|  
|-
| Best Lighting
| Hwang Soon-wook
|  
|-
| Technical Award (sound)
| Lee Seung-yup
|  
|-
| rowspan=1|   15th Busan Film Critics Awards
| Best Actor
| Lee Geung-young
|  
|-
| rowspan=2|   22nd Korea Culture and Entertainment Awards
| Best Film
| The Pirates
|  
|-
| Best Director
| Lee Seok-hoon
|  
|-
| rowspan=4|   35th Blue Dragon Film Awards 
| Best Director
| Lee Seok-hoon
|  
|-
| Best Supporting Actor
| Yoo Hae-jin
|  
|-
| Best Art Direction
| Kim Ji-a
|  
|-
| Technical Award
| Kang Jong-ik (visual effects)
|  
|- 2015
|   6th KOFRA Film Awards 
| Best Supporting Actor
| Yoo Hae-jin
|  
|-
| rowspan=2|  10th Max Movie Awards
| Best Actress
| Son Ye-jin
|  
|-
| Best Supporting Actor
| Yoo Hae-jin
|  
|-
|   20th Chunsa Film Art Awards 
| Technical Award
|
|  
|- 9th Asian Film Awards 
| Best Visual Effects
| Kang Jong-ik
|  
|- 51st Baeksang Arts Awards
| Best Actress
| Son Ye-jin
|  
|-
| Best Supporting Actor
| Yoo Hae-jin
|  
|-
|}

==References==
 

==External links==
*    
*   at Lotte Entertainment
*  
*  
*  

 
 
 
 
 
 
 
 