Hunting Flies
{{Infobox film
| name           = Hunting Flies
| image	=	Hunting Flies FilmPoster.jpeg
| caption        = Film poster
| director       = Andrzej Wajda
| producer       = 
| writer         = Janusz Glowacki
| starring       = Zygmunt Malanowicz
| music          = 
| cinematography = Zygmunt Samosiuk
| editing        = Halina Prugar-Ketling
| distributor    = 
| released       =  
| runtime        = 104 minutes
| country        = Poland
| language       = Polish
| budget         = 
}}

Hunting Flies ( ) is a 1969 Polish comedy film directed by Andrzej Wajda. It was entered into the 1969 Cannes Film Festival.   

==Cast==
* Zygmunt Malanowicz - Wlodek
* Malgorzata Braunek - Irena
* Ewa Skarzanka - Hanka, Wlodeks wife
* Hanna Skarzanka - Hankas mother
* Józef Pieracki - Hankas father
* Daniel Olbrychski - Sculptor
* Irena Dziedzic - Journalist
* Leszek Drogosz - Militiaman
* Jacek Fedorowicz - Director
* Marek Grechuta - V.I.P.s Son
* Irena Laskowska - Editors Wife
* Julia Bratna - Girl
* M. Ziólkowski - Boy
* Leon Bukowiecki - Editor
* Krzysztof Burnatowicz

==References==
 

==External links==
* 

 

 
 
 
 
 


 
 