Damansky Island Year 1969
 
Damansky Island Year 1969 (Russian: Остров Даманский. 1969 год) is a 2004 Russian documentary film about two battles that took place on Damansky Island in 1969 between the Soviet Union and the Peoples Republic of China (erstwhile Cold War allies). The film consists of interviews with participants and leaders from both sides of the conflict.

Directed by Andrey Semenov, 2004

Issued: Studio GALAKON, 2004
Duration: 39 min. 22 sec

==Documentary Summarized Transcript==
As an aid to non-Russian speakers, here is an English description of the majority of the narrative, with special attention paid to the accuracy of all interviews.  All names are shown in Roman and Cyrillic spellings:

00:00 – 02:10
This island is so small, that during the Spring overflow it hides under the Ussuri river. The Damansky Island in Russian language, Zhenbao in Chinese, meaning valuable, precious. March 1969 – conflict. Sunday, 2 March 1969 – planned military training, Russian forces were practicing on how to defend from an aggressor. The Dalnerechensky (Дальнереченский) unit of Border Guard took part in the exercise, defending the Russian-Chinese border on Ussuri, close to Damansky Island. Most of the people and equipment were moved deeper (50&nbsp;km) into USSR. The border perimeters were manned with only 30 people each.  Close to noon, a message came from Damansky Island, saying that the battle with the Chinese was in progress.

02:10 – 02:50
Oleg Losik (Олег Лосик), Commander of the Far Eastern (Дальневосточный) military unit.
They often had a place of  provocation…In the summer, on Amur and Ussuri rivers, Chinese fisherman used to step into our waters. And what they did in the winter…They stepped on ice and yielded anti-Soviet slogans. And our border protection soldiers would push them off the ice back to the shore without the use of weapons.

02:50 – 03:36
A firm order from Moscow was issued – don’t get dragged in by those provocations, don’t use weapons, even though the border is being crossed every day.

03:36 – 03:55
March 2, 1969 – for the first time in 25 years, from the end of the 2nd World War there was a military assault at the Soviet border.

03:55 – 04:08
Oleg Losik  (Олег Лосик), Commander of the Far Eastern (Дальневосточный) military unit
  asked me: what is going on there on your side? I said that this and that happened. I took precautions.

04:08 – 04:44
He said, that at 10:40AM, the head of the Lower Mikhaylovka (Нижне-Михайловка), Lieutenant Ivan Strelnikov (Иван Стрельников) along with his soldiers headed out to those who breached the borders. The Chinese started to move away. The second group from the border perimeter under the command of Yuri Babansky, tried to cut through the way out of the Chinese. The border patrol soldiers did not realize that they were being dragged into the trap.

04:44 – 05:06
Yuri Babansky (Юрий Бабанский), the “Hero of USSR”
I ran first, there was  about 25 meters left to reach Strelnikov (Стрельников). On the island where Strelnikov was stationed, there was the main ambush, where the first shot was heard, followed by the rattle of automatic rifles.

05:06 – 05:56	
There was this military photographer Nikolai Petrov (Николай Петров). He managed to take 3 photos before he was killed.
The Strelnikov group was executed in a few seconds.
Immediately – the Chinese started the assault. They had artillery.
Yuri Babansky became the commander.

05:56 – 06:09
Yuri Babansky (Юрий Бабанский), the “Hero of USSR”
“I ordered – ‘Fire!’ And we were holed up on the island for 1 hour and 40 minutes. 5 men stayed alive.”

06:09 – 05:19
The Border Guard soldiers were fighting with the Chinese military, but the Far Eastern military unit could not enter and provide help. Orders from the Kremlin were needed for that, but they did not want to drag in the military into this conflict.

05:19 – 06:51
Oleg Losik  (Олег Лосик), Commander of the Far Eastern (Дальневосточный) military unit
We had a directive from the Defense Minister. It said: not only defend our border. Not only. Do not allow it to escalate. The conflict was to be limited to the actions of the Border Guard units

06:51 – 07:20
Both countries had nuclear capabilities. Brezniev was waiting for the meeting with Nixon and events at the border could have weakened the USSR during those negotiations. Brezniev knew, that it reminded people of the Prague Spring.

07:20 – 08:40
 

08:40 – 09:49
  were there to sign an agreement regarding the limitations on strategic weapons.

09:49 – 10:03
Georgi Arbatov Георгий Арбатов, the advisor to the Central Committee of the Party, Director of the Institute for U.S. and Canadian Studies
Leonid Ilyich wanted the relations with the US to get better. That is why he accepted the visit of Kissinger.

 

17:30 – 18:11
Oleg Losik  ordered to throw tanks, artillery and rocket batteries at the border. But at this time the military could not have been added to the conflict, only Border Patrol. SG Soldiers from the Kulebyakiny hills (Кулебякины Сопки) have joined. Their commander was Lieutenant Vitaly Bubenin (Виталий Бубенин).

18:11 – 18:51
Lieutenant Vitaly Bubenin (Виталий Бубенин), the Hero of USSR
There were 300 of them,   maybe more and 20-21 of us. The distance between us was 150–200 meters. Obviously, it was horrible. When we got on the snow, and laid on it, digged in it. A sniper started shooting. I felt that I’ll be killed next. Then it got horrible. Not Ivanov, Petrov, Sidorov, but me. How I got over it I don’t remember. Then, anger came. I was lying, and the anger was arising within me.

18:51 – 19:15
A directive came to take the people out of an unequal fight. But Bubenin (Бубенин) made a different decision. With an armored vehicle   he got in back of the Chinese.

19:15 – 19:41
Lieutenant Vitaly Bubenin (Виталий Бубенин), the Hero of USSR
From the Chinese side, there were reinforcements. About one unit. The unit of Chinese jumped out from the banks of the river, about 60 meters, right under the BTR vehicle. They weren’t expecting me. We killed almost the entire unit there.

19:41 – 20:37
Bubenin stopped the vehicle to pick up wounded, Chinese got into the BTR.

19:51 – 20:37
Lieutenant Vitaly Bubenin (Виталий Бубенин), the Hero of USSR
We got into another BTR. I made a decision to strike from the side. I was lucky to be on the flank of a battalion. When we showed up, it was a surprise to the Chinese. They panicked and all started running away from the island. Then we noticed how many Chinese were in front of us. We shot almost everybody.

20:37 – 20:58
From the other bank, the Chinese started shooting at the BTR. Only Bubenin and 3 others could have still fought further. The Chinese did not realize, the ammunition was running out quickly.

20:58 – 21:32
Lieutenant Vitaly Bubenin (Виталий Бубенин), the Hero of USSR
We were holding ourselves together and started the offensive. I’ve come to realize, that this was the last fight. The last round. And that nobody would come back. Soldiers were screaming, waving their hands. I could’t hear correctly, I was injured (shell shocked). But I understood that everything was over.

21:32 – 23:51
The Andropov report.
248 Chinese and 32 Soviet soldiers have been killed.
Bubenin received the title (Hero of USSR), the first since World War II.
In 1964 the USSR wanted to return Damansky island (and other islands) to the Chinese, but Mao had rejected the offer.

23:51 – 24:24
Georgi Arbatov  , the advisor to the Central Committee of the Party, Director of the Institute for U.S. and Canadian Studies
Everybody understood, that the Chinese were trying to take advantage of the inconsistency between the USSR and USA. I have to admit, that I was surprised, that the Chinese relationship to the USA was better than to us.

24:24 – 25:10
Reasons of Soviet-Chinese misunderstandings.
Crisis
Vietnam.

25:10 – 25:48
Georgi Arbatov Георгий Арбатов, the advisor to the Central Committee of the Party, Director of the Institute for U.S. and Canadian Studies
He had a lot of common sense. I recommended him to consider this patiently – who can win more? Who has got the interest? Are we interested in limiting the weapons? Yes. Americans? As well. We may be interested even more, because they are richer. But they also want it.

25:48 – 27:05
China didn’t criticize the Prague Spring.
Hockey again, hockey like politics.

27:05 – 27:24
Vladimir Petrov (Владимир Петров), sports champion USSR
You can say that sport is war during peacetime. So our leadership ordered us not to lose. So the way to win is not only through tanks. They had to achieve victory.

27:24 – 27:43
Anna Shatilova (Анна Шатилова), director of central television
I remember, that with regards to events on Damansky Island, it was said, that USSR must solve 2 problems – Damansky Island and Nedomansky Недоманский   – Czech hockey player, who kept winning.

27:43 – 29:18
USSR – America 17:2
Second attack on Damansky Island by the Chinese
5000 Chinese soldiers
About 40 Border Patrol soldiers from USSR
4 secret tanks from the Soviet side and the border unit commander Leonov  (Леонов).

29:18 – 19:18
Oleg Losik (Олег Лосик), Commander of the Far Eastern (Дальневосточный) military unit.
They started the tank and moved on. Dismissed the directive. And all other tanks followed. Around the island. And stopped at the river. The tanks waited about 100 meters from the Chinese bank, where the Chinese were waiting with anti tank weapons. They hit the Leonov’s tank. Leonov started getting out of the tank. He was killed. Too bad.

30:18 – 31:46
About 5PM – the Chinese were getting ready to attack.

Moscow was silent. But Losik individually had decided to include the artillery and “Grad” rockets. One thousand Chinese died.

31:46 – 32:00
Oleg Losik (Олег Лосик), Commander of the Far Eastern (Дальневосточный) military unit.
The HQ found out about it. The Defense Minister asked: “Why did this occur? We should have saved the ammunition.”

32:01 – 32:15
The defenders of Damansky Island lost 58 men (in two conflicts).

32:15 – 33:33
Anna Shatilova (Анна Шатилова), director of central television
The heroes on air that evening were participants of the conflict on Damansky Island and their relatives. I remember when I got to studio A, and there were lots of people and equipment. Wife of Strelnikov. I remember this name, that Strelnikov was killed… She announces the song from Pakhmutova (Пахмутовой), a wonderful song, “The tenderness”. And when Maya Kristalinskaya (Майя Кристалинская) started singing, I’m sitting, looking and... Strelnikov’s wife fainted.

34:02 –
Again about sport, hockey, Czech Republic
About Czech & Soviet hockey players fighting.

Nixon did not expect an aggression of China on USSR.
Kissinger found himself in this situation and understood – Mao was provoking the USSR – is he going to use the USSR against China to defend a small island. Is he really going to limit the amount of weapons, as it was declared?

Meeting of Nixon and Mao.
Nixon did not fully understand Mao’s motivation.

==External links==
*Link to Documentary:   

 
 
 
 
 
 
 