Bachelor Party 2: The Last Temptation
{{Infobox Film
| name           = Bachelor Party 2: The Last Temptation
| image_size     = 190px
| image	=	Bachelor Party 2- The Last Temptation FilmPoster.jpeg
| caption        = 
| director       = James Ryan
| producer       = 20th Century Fox Home Entertainment
| writer         = Jay Longino
| narrator       = 
| starring       = Josh Cooke Sara Foster Greg Pitts Warren Christie Emmanuelle Vaugier Audrey Landers  James Dooley
| cinematography = Roy H. Wagner
| editing        = Robert Komatsu
| distributor    = 
| released       = 
| runtime        = 98 minutes (R-rated), 103 minutes (unrated) http://www.videobusiness.com/article/CA6531114.html  
| country        = 
| language       = 
| budget         = $6 million 
| gross          = 
}}
 Bachelor Party), James Ryan, best known as a finalist to direct Feast (2005 film)|Feast, the film made as part of the final season of Project Greenlight. 

Principal photography occurred in Miami, Florida starting in March 2007. 

==Cast==
As billed,  the cast also includes 
Greg Pitts,
Harland Williams,
Warren Christie,
Danny A. Jacobs,
Max Landwirth,
Emmanuelle Vaugier,
Karen-Eileen Gordon,
Mauricio Sanchez, Steven Crowley, and 
Audrey Landers.

==Plot==

A guy named Ron finally settled up and decided to marry his girlfriend Melinda, whom he met just two months ago. Her brother, Todd, hates Ron and tries to persuade Melinda that he is just marrying her to use her money for himself. He tries to talk Melinda out of the wedding, but before that, he tries to show who Ron really is so he decided to make a wild bachelor party, in hope of catching Ron and canceling the wedding.

==Reception==
The trade publication Video Business said the film "moves at a fast clip, but the jokes are lackluster, leaving only the female cast members’ topless scenes as a potential selling point." 

A reviewer for The Observer, an independent student newspaper|student-run newspaper covering Rutgers University, called it "one of the worst movies I have ever seen" and noted that it was only Sara Fosters appearance in the film that kept him watching. 

IGN in its review of the DVD said "A film that probably shouldnt have been made, yet there are going to be loads of people out there who will enjoy this disaster because, as the old advertising motto states, "sex sells."  

==References==
 

== External links ==
*  

 
 
 
 