My Generation (film)
 
My Generation is a 2000 film by documentary filmmaker Barbara Kopple that looks at the links between young people who attended the original 1969 Woodstock music festival and those who attended the 1994 and 1999 Woodstock festivities.  Despite the wide generational and cultural gaps between the original attendees and the newer ones, the film finds that there are more commonalities than there are differences. The movie also captured both the notorious mud-slinging performance of the band Green Day at 1994s festival and the chaotic fires at 1999s concert.  The film features performances by such acts as Joe Cocker, Blues Traveler, DMX (rapper)|DMX, and Limp Bizkit.  My Generation premiered as a work-in-progress at the 2000 Sundance Film Festival. 

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 

 