Way...Way Out
{{Infobox film
| name     = Way...Way Out
| image    = waywayout.jpg Gordon Douglas
| writer   = William Bowers Laslo Vanday
| starring = Jerry Lewis Connie Stevens Robert Morley Dick Shawn Anita Ekberg James Brolin Malcolm Stuart
| distributor = 20th Century Fox
| released =  
| runtime  = 106 minutes
| language = English
| budget   = $2,955,000 
| gross    = $1.2 million (est. U.S./Canada rentals)  361,933 admissions (France)   at Box Office Story 
}}
 1966 American comedy film starring Jerry Lewis and released by 20th Century Fox on October 21, 1966. The film was both a critical and commercial flop. 

==Plot==
The year is 1989, and the United States continues to be engaged in a Space Race with the Soviet Union.

The two male astronauts currently manning the U.S. weather station on the Moon, Hoffman (Dennis Weaver) and Schmidlap (Howard Morris), are suffering the effects of their long stay in space and need to be relieved, as Schmidlap regularly ties up Hoffman and has even knocked out his two front teeth.  The sex-starved Schmidlap sits around drawing lewd pictures of naked women.  

Mr. Quonset (Robert Morley), the head of NAWA, is concerned that the situation with Hoffman and Schmidlap threatens to become an embarrassment to NAWA.  Furthermore, the Soviets have taken a step forward in the space race by placing the first (unmarried) male/female couple on the Moon.  Quonset decides the United States should place the first married couple in space.
 Linda Harrison) gantry for their space launch.

When they arrive on the Moon, they receive regular visits from the Russian astronauts, Anna Soblova (Anita Ekberg) and Igor Baklenikov (Dick Shawn), living at the nearby Soviet lunar station.  Antics ensue with vodka pill parties and the men preening for their beautiful female companions.  The Soviets are suspected of trying to sabotage the American space station, but they are soon vindicated.  

Anna tricks Igor into marrying her by declaring she is pregnant, having gotten the idea for this from Eileen.  The wedding is broadcast via satellite to the entire planet, with Peter acting as best man and Eileen as Maid of Honor.  There is talk of the Soviets having achieved the first baby on the Moon.  Mr. Quonset declares his unhappiness that the Soviets have scored this crucial first to Peter and Eileen, insinuating that this is because Peter is less virile than Igor and Eileen less sexy than Anna.  Stung by this, Eileen declares she is just as pregnant as Anna, delighting Mr. Quonset.  Eileen then tells the startled Peter shes just as pregnant as Anna because Anna isnt pregnant at all, and then suggests they could make her assertion retroactively true.  They are just initiating this project as the movie ends.

==Production==
Way...Way Out was filmed from January 24-March 30, 1966.  
 John "Shorty" Powers, who had been NASAs mission commentator for Project Mercury.

The title song is performed by Gary Lewis & the Playboys led by Lewis son, Gary.

Brian Keith has a cameo as General "Howling Bull" Hallenby.

==Cast==
* Jerry Lewis as Peter
* Connie Stevens as Eileen
* Dick Shawn as Igor
* Anita Ekberg as Anna
* Brian Keith as General "Howling Bull" Hallenby
* Dennis Weaver as Hoffman
* Howard Morris as Schmidlap
* Robert Morley as Quonset
* James Brolin as Ted Linda Harrison as Peggy

==References==
 

== External links ==
 

 

 
 
 
 
 
 
 