Cookie (film)
{{Infobox film
| name           = Cookie
| image          = cookieposter.jpg
| director       = Susan Seidelman
| writer         = Nora Ephron Alice Arlen
| starring       = Peter Falk Dianne Wiest Emily Lloyd Jerry Lewis Bob Gunton Ricki Lake Joy Behar
| producer       = Laurence Mark
| music          = Thomas Newman
| cinematography = Oliver Stapleton
| editing        = Andrew Mondshein
| distributor    = Lorimar Productions Warner Bros. Pictures
| released       =  
| runtime        = 94 min.
| language       = English gross = 191,940 admissions (France)   at Box Office Story 
| budget         = 
}}
Cookie is a 1989 comedy film released on August 23, 1989 by Warner Brothers.

==Plot==
Cookie Voltecki (Emily Lloyd) is the illegitimate daughter of mobster Dino Capisco (Peter Falk), who has just finished thirteen years in prison. Since being released from jail, all that Dino wants is to settle some old scores, and make up for lost time with his daughter.

Cookies mother, Lenore Voltecki (Dianne Wiest), has been Dinos longtime mistress. Dinos actual wife Bunny (Brenda Vaccaro) has, he thinks, been kept in the dark about Dinos mistress and his daughter. Dino decides that the best way to get to know Cookie is to hire her as his chauffeur. With her ears to the conspiracies floating around Dino, she quickly discovers that her fathers old crony, Carmine (Michael V. Gazzo), has been swindling him and that Dinos life is in jeopardy.

==Main cast==
* Peter Falk - Dominick "Dino" Capisco
* Dianne Wiest Lenore Voltecki
* Emily Lloyd - Carmela "Cookie" Voltecki
* Michael V. Gazzo - Carmine
* Brenda Vaccaro - Bunny Capisco
* Adrian Pasdar - Vito
* Lionel Stander - Enzo Della Testa
* Jerry Lewis - Arnold Ross
* Bob Gunton - Richie Segretto
* Joe Mantello - Dominick
* Ricki Lake - Pia
* Joy Behar - Dottie

==Reception==
The film received negative reviews from critics upon its release and has 18% on Rotten Tomatoes.

==Home media==
Warner Archives released the film on made to order DVD in the United States on May 4, 2010.

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 


 