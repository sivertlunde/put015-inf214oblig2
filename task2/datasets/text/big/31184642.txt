These Girls Are Missing
{{Infobox film
| name           = These Girls Are Missing
| image          = These_Girls_Are_Missing.jpg
| image_size     = 
| caption        = Still from the film
| director       = Shari Robertson Michael Camerini
| producer       = Shari Robertson Michael Camerini
| narrator       = Kagendo Murungi
| music          = Epizo Bangoura
| cinematography = Michael Camerini
| editing        = Jay Freund
| studio         = The Epidavros Project, Inc.
| distributor    = 
* The Epidavros Project, Inc.
* Filmakers Library
| released       = Beijing, Sept. 1995 The UN Fourth World Conference on Women 
| runtime        = 60 minutes
| country        = United States
| language       = English, French, Maninka language|Malinké, Susu language|Susu, Pular language|Pular, Kiswahili, Chichewa
}}

These Girls Are Missing is a 1995 documentary film from directors Shari Robertson and Michael Camerini about the gender gap in education in Africa. Its world premiere was at the UN Fourth World Conference on Women.  The film grew out of an initiative by the FAWE, The Forum for African Women Educationalists, with additional support from the Rockefeller Foundation and UNICEF.

The films purpose was to address, as Robertson puts it, "the elephant in the room" about girls in school and allow for discussion of attitudes about the effect of education on African girls and their societies. It is currently used for training within the Peace Corps who, in statements, have described the film as "rich in its learning opportunities as well as being beautifully filmed."

== Synopsis ==

"A film about men and women, about marriage... families... having babies, about tradition, and the modern world... about who goes to school in Africa... and why girls are missing." - Official tag-line.

Every year girls in African schools "go missing," from school classrooms. This film analyzes the stories of five schoolgirls and examines the causes and consequences of the high dropout rates among girls. The stories are told directly by Malawian and Guinean girls and their families. 
* Part I: Nadouba and Bintu in the West-African village of Gbonko, Guinea.
* Part II: Ethel and her mother, in Muluma village, Malawi.
* Part III: Taz and Patricia from elite St. Mary’s Secondary School in Zomba, Malawi.
Also presented is a group of elders from a Malinké village conversing about the issue.

==Release==

These Girls Are Missing was initially commissioned by  , French language|French, Malinké, Susu, Poular, Ki-Swahili and Chichewa.

Since its release, the film has been distributed widely by the Ministries of Education in both Guinea and Malawi, and used in focus groups and workshops promoting girls’ education. FAWE also distributed the film unto associate members in Ministries of Education in Zanzibar, Angola, Cameroon, Namibia, Tanzania, Kenya, Senegal, Mauritius, Seychelles, Botswana, São Tomé e Principe, Mozambique,  Uganda and Ghana. 
 Cinema du CIES (Comparative and International Education Society) Conference, Buffalo, NY; Marymount Manhattan College, New York City|NY; and the Woodrow Wilson School of Public and International Affairs, Princeton University. It has also been screened for the U.S. State Department, U.S. Agency for International Development, Inter-American Development Bank,  World Council for Comparative Education Societies and the United States National Academy of Sciences|U.S. National Academy of Sciences.

==The Girls Know it==

The filmmakers also produced a shorter version of the film called The Girls Know It, released in 1997. It is hosted, on screen, by The Honorable Aicha Bah, Minister of Education in Guinea. The film is shorter, at 47 minutes and was released in 1997. It is available in seven languages as well. This 47-minute cut is available in the same seven languages as the 60-minute film.

==Educational Use==

UNICEF, USAID, and the Peace Corps have made extensive use of These Girls Are Missing as a training and education tool.

==Critical reception==

The film received the Cine Golden Eagle under the Documentary Feature category  and a Silver Plaque as part of the 31st annual Chicago International Film Festival.

March, 1998 Moving Pictures Bulletin reviewed the film as  analyzing and contrasting situations "represent  a new generation who may finally break the cycle of girls in Africa forced to miss out on their education."

==See also==
*Shari Robertson
*Michael Camerini
*Well-Founded Fear
* 

==External links==
*   for the film
*  
*  
*  

==References==
 

 

 
 
 
 
 
 
 