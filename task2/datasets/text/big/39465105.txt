The Devil's in the Details
 

{{Infobox film
| name           = The Devils in the Details
| image          = The Devils in the Details poster.png
| caption        = DVD cover
| director       = Waymon Boone
| producer       = 
| writer         = Waymon Boone
| starring       = Ray Liotta  Emilio Rivera  Joel Mathews  Raymond J. Barry  Noel Gugliemi  Lane Garrison  Jake Jacobson 
| music          =
| cinematography = 
| editing        = 
| studio         = Dark Details   Hollywood Media Bridge
| distributor    = Image Entertainment   Eagle Films
| released       =  
| runtime        = 100 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
The Devils in the Details is a 2012 American thriller film directed and written by Waymon Boone. The film encircles an Arizona military veteran suffering post-traumatic stress disorder|post-trauma from a military experience when he gets caught up in a Mexican cartels drug mule plot. It stars Ray Liotta, Emilio Rivera, Joel Mathews, Raymond J. Berry, Noel Gugliemi, Lane Garrison and Jake Jacobson.

==Plot==
Thomas Conrad (Joel Mathews), a supermarket manager and military ex-soldier, has trouble re-adjusting to a normal life in Nogales, Arizona. For over 6 months, he has been tormented by post-traumatic stress disorder from a terrifying experience serving his country overseas. Enduring issues with his family, he has also separated himself from his wife Selina (Jenny Lyng) and daughter Chloe (Ava Acres). Recommended to undergo rehab, he visits ex-Navy Seal and psychiatrist, Dr. Robert Michaels (Ray Liotta), who helps him identify the moments leading up to his condition.
 booze between drug smuggling border patrol officer, Thomas must have his estranged family commit the crime in a timely manner.

During a series of phone calls that detail the action sequences, Thomas succeeds to get his family into action. He comes close to becoming fatally wounded on many occasions where his time nearly expires to complete a task and is also threatened with being injected by a deadly chemical compound. Many of Bills associates check in to keep watch, making Thomas believe he had been taken to Mexico. On one instance, he pleads help from a young man named Trevor (Lane Garrison) who enters the room, but he is revealed to be working with the others, soon bringing in Thomass employee Olivia (Shi Ne Nielson) to shoot and kill her which infuriates Thomas. Eventually Thomass sister Claire secures a large amount of drugs and money from a designated house, but when she transports it to the drop-off spot, she is apparently shot and killed. Soon, his father Richard calls Dr. Robert Michaels for help. Around this time, it’s revealed in a flashback that during his military duty with his comrade Hutchens (Albert Thakur), who was shot, Thomas inadvertently cornered and gunned down several armed kids in a home, causing his mental anguish. Robert arrives at Thomass wifes house pretending to be looking for a plumber. Taking action, an armed Robert sneaks to the back and enters the house, ultimately killing two men to free the house of captivity.

Released from restraint and left weakened on the floor, Thomas finds the chance to stab and kill an armed Trevor with a sharp object. Taking his gun, he also shoots Corbin in the head. Escaping the torture room after killing them both, Thomas walks down a corridor noticing audio equipment on a table. Exiting out, he realizes he was kept behind the same bar the entire time. He approaches Bill at the counter and shoots dead the provoking bartender Frank (Arturo del Puerto). Bill reveals there was no cartel and everything was his elaborate plan – they spied on him, set up the minor car accident, got him drunk in the bar, held him against his will and used certain audio clips to play tricks on him, all to goad Thomas into getting his family to commit the robbery of a drug dealer, something Bill admittedly did to other victims previously. Also Thomass sister Claire was killed because his father involved the police. Exacting revenge, Thomas injects a lethal chemical compound into Bill, who dies as a result. Leaving the bar and relieved to see outside again, Thomas drives home to see his wife and daughter waiting to greet him at the door.

==Cast==
*Ray Liotta as Dr. Robert Michaels
*Emilio Rivera as Bill Duffy
*Joel Mathews as Thomas Conrad
*Raymond J. Barry as Richard Conrad
*Noel Gugliemi as Guzzo
*Lane Garrison as Trevor
*Jake Jacobson as Corbin
*Arturo del Puerto as Frank
*Albert Thakur as Hutchens
*Jenna Lyng as Selina
*Ava Acres as Chloe
*Justin Finney as Justin
*Shi Ne Nielson as Olivia
*Nikki Deloach as Claire
*Wes McGee as Kyle Conrad

==Distribution==
The Devils in the Details was released in the United States on Blu-ray and DVD on March 12, 2013. 

==Reception==
The film has garnered mixed to negative reviews. Reviewed by the DVD Verdict, writer Tom Becker observed the film as "overwrought" and "under-thought", believing Ray Liotta contributed positively, the location and effects were "lite", and the suspense was scarce, but felt "the lack of a significant pay-off is just a tremendous cheat."    Brendon Surpless of Blu-ray Definition called it "a rather mundane film" that "has some entertaining moments thanks to a solid performance by veteran Ray Liotta."    In a less critical review, Michael Reuben of blu-ray.com argued that the film "keeps you guessing" and credited Joel Mathews as a credible victim and Emilio Rivera to be the "stuff of nightmares".   

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 