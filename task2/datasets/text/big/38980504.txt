Skagerrak (film)
 
{{Infobox film
| name           = Skagerrak
| image          = 
| caption        = 
| director       = Søren Kragh-Jacobsen
| producer       = 
| writer         = Anders Thomas Jensen Søren Kragh-Jacobsen
| starring       = Iben Hjejle
| music          = 
| cinematography = Eric Kress
| editing        = 
| distributor    = 
| released       =  
| runtime        = 104 minutes
| country        = Denmark
| language       = Danish
| budget         = 
}}

Skagerrak is a 2003 Danish drama film directed by Søren Kragh-Jacobsen. It was entered into the 25th Moscow International Film Festival.   

==Cast==
* Iben Hjejle as Marie
* Bronagh Gallagher as Sophie
* Martin Henderson as Ian / Ken
* Ewen Bremner as Gabriel Gary Lewis as Willy
* Simon McBurney as Thomas
* Helen Baxendale as Stella
* James Cosmo as Robert
* Scott Handy as Roman
* Kay Gallie as Housekeeper

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 

 
 