The Last Judgment (1961 film)
{{Infobox film name            = Il giudizio universale image           = Giudizio universale.jpg caption         =  director        = Vittorio De Sica producer        = Dino De Laurentiis writer          = Cesare Zavattini starring        = Alberto Sordi Nino Manfredi Vittorio Gassman Jack Palance Ernest Borgnine Paolo Stoppa Fernandel Renato Rascel Silvana Mangano Franco Franchi Ciccio Ingrassia Anouk Aimée Lino Ventura Melina Mercouri Vittorio De Sica Paolo Stoppa music           = Alessandro Cicognini cinematography  = Gábor Pogány editing         =  distributor     =  released        = October 26, 1961 runtime         = 92 minutes country         = Italy language        = Italian budget          =
|}} 1961 commedia allitaliana film by Italian director Vittorio De Sica. It was coproduced with France.

It has an all-star Italian and international cast, including Americans Jack Palance, Ernest Borgnine; Greek Melina Mercouri and French Fernandel, Anouk Aimée and Lino Ventura.

The film was a huge flop, massacred by critics and audiences when it was released. It was filmed in black and white, but the last sequence, the dance at theatre, is in color.

==Plot==
At the morning of a normal day of a Naples that begins to hear complex and not always positive effects of the economic boom, a stentorian voice (Nicola Rossi-Lemeni) which seems to come down from heaven announces that "At 18 begins the Last Judgement!".
The announcement is repeated with increasing insistence, first treated with disdain and then more and more frightening. The plot is fragmented into a series of scenarios and stories intertwined: the preparation of the great ball of the Duke to whom all Naples is requested, the struggle to get dressed up in the poorest districts, bored rich you are courting, a husband who accidentally discovers his wife with her lover, a cynical imagine that ekes out a living selling children in America, a young man of good company made the subject of sneers from fierce populace, the unlikely defense of a lobbyist by a wordy lawyer (the by De Sica), and the impact of the increasingly mysterious voice shaking innovation of this human variety. Those who repent too late, he who gives himself to the mad joy, who flaunts a false indifference.
Announced time, the city is impacted by a terrible flood (the mysterious voice that has already passed the stage of sanctions?) After which, with great solemnity, the Last Judgment begins and ends, however, as mysteriously as it is announced. The sun came out, people rushed to the ball of the Duke and soon everything is forgotten, the sound of an ironic "Lullaby", coined shortly before by a hypocritical and false slavery.

==Cast==
* Alberto Sordi - Merchant of children
* Vittorio Gassman - Cimino
* Anouk Aimée - Giorgios wife Irene
* Fernandel - The widower
* Nino Manfredi - Waiter
* Silvana Mangano - Letizia Matteoni
* Paolo Stoppa - Giorgio
* Renato Rascel - Coppola
* Melina Mercouri - Foreign lady
* Jack Palance - Matteoni
* Lino Ventura - Giovannas father
*Elisa Cegani - Giovannas mother
* Vittorio De Sica - Defense lawyer
*Ernest Borgnine -  the pickpocket
*Eleonora Brown -  Giovanna
* Jimmy Durante -  the big nose man  
* Franco Franchi and Ciccio Ingrassia -  the unemployed men
* Domenico Modugno -  the singer
* Marisa Merlini -  a mother
* Mike Bongiorno -  himself
* Akim Tamiroff -  the director
* Maria Pia Casilio -  the waitress
* Alberto Bonucci -  the guest in Matteonis house
*Lamberto Maggiorani -  a poor man

== External links ==
*  

 

 
 
 
 
 
 
 
 
 


 
 