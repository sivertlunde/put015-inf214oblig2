Puthariyankam
{{Infobox film
| name = Puthariyankam
| image =
| caption =
| director = PG Vishwambharan
| producer =
| writer = Purushan Alappuzha
| screenplay = Purushan Alappuzha Seema
| music = A. T. Ummer
| cinematography = Anandakkuttan
| editing = NP Suresh
| studio = Umamini Movies
| distributor = Umamini Movies
| released =  
| country = India Malayalam
}}
 1978 Cinema Indian Malayalam Malayalam film, Seema in lead roles. The film had musical score by A. T. Ummer.       

==Cast==
*Sreelatha Namboothiri 
*Unnimary 
*Poojappura Ravi  Seema 
*Roja Ramani  Sudheer 
*Vijayalalitha  Vincent

==Soundtrack==
The music was composed by A. T. Ummer and lyrics was written by Yusufali Kechery. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Aarum Kothikkunna Poove || B Vasantha, Gopalakrishnan || Yusufali Kechery || 
|- 
| 2 || Aathiraponnoonjaal || Ambili, Chorus, Jolly Abraham || Yusufali Kechery || 
|- 
| 3 || Chanchalaakshimaare || Sujatha Mohan || Yusufali Kechery || 
|- 
| 4 || Kaalidaasa Kavyamo || K. J. Yesudas || Yusufali Kechery || 
|}

==References==
 

==External links==

 
 
 


 