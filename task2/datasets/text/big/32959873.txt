The Soul Market
{{Infobox film
| name           = The Soul Market
| image          =
| caption        =
| director       = Francis J. Grandon
| producer       = Popular Plays and Players Inc
| writer         = Aaron Hoffman
| starring       = Olga Petrova
| music          =
| cinematography = George Peters
| editing        = 
| distributor    = Metro Pictures
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = Silent with English intertitles
| budget         =
| gross          =
}}
 silent American drama film directed by Francis J. Grandon. The film is considered to be lost film|lost. 

==Plot==

Olga Petrova plays Elaine Elton, a famous actress who is engaged to a powerful producer.  She meets a millionaire, who poses as a chauffeur to conquer her because he knows she dislikes rich men.  She falls in love with him but cannot accept his proposal of marriage because of her engagement to the producer.     

== Cast ==
* Olga Petrova as Elaine Elton (as Olya Petrova)
* Arthur Hoops as Oscar Billings
* John Merkyl as Jack Dexter (as Wilmuth Merkyl)
* Fritz De Lint as Dick Gordon
* Evelyn Brent as Vivian Austin
* Fraunie Fraunholz as Griggs
* Charles Brandt as Sam Franklin
* Charles Mack as Harvey Theugh
* Bert Tuey as Joe Burrows
* Grace Florence as Mrs. Wilson
* Cora Milholland as Susan
* Al Thomas as James Austin
* Gypsy OBrien as Billie Simpson
* Claire Lillian Barry

==References==
 

== External links ==
* 
* 

 
 
 
 
 
 
 
 
 
 
 