Fiorile
{{Infobox film
| name           = Fiorile
| image          = Fiorile93poster.jpg
| image_size     =
| caption        = Italian-language poster
| director       = Paolo Taviani Vittorio Taviani
| producer       = Grazia Volpi
| writer         = Sandro Petraglia Paolo Taviani Vittorio Taviani
| narrator       =
| starring       = Claudio Bigagli Galatea Ranzi Michael Vartan
| music          = Nicola Piovani
| cinematography = Giuseppe Lanci
| editing        = Roberto Perpignani
| distributor    = Fine Line Features (US)
| released       =  
| runtime        = 118 minutes
| country        = Italy France Germany Italian French French
| budget         =
| gross          =
}} Paolo and Vittorio Taviani, and stars Claudio Bigagli, Galatea Ranzi, and Michael Vartan. It was entered into the 1993 Cannes Film Festival.   

The title Fiorile allegedly is derived from the month of Floréal (April–May) in the French Republican Calendar. The film is also known as Wild Flower.

==Cast==
* Claudio Bigagli - Corrado / Alessandro
* Galatea Ranzi - Elisabette / Elisa
* Michael Vartan - Jean / Massimo
* Lino Capolicchio - Luigi
* Constanze Engelbrecht - Juliette
* Athina Cenci - Gina
* Giovanni Guidelli - Elio
* Norma Martelli - Livia
* Pier Paolo Capponi - Duilio
* Chiara Caselli - Chiara
* Renato Carpentieri - Massimo as an old Man
* Carlo Luca De Ruggieri - Renzo
* Laurent Schilling
* Fritz Müller-Scherz - University Professor
* Laura Scarimbolo - Alfredina

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 


 
 