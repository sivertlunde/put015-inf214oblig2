Matri-Phony
{{Infobox Film |
  | name           = Matri-Phony |
  | image          = Matriphony42.jpg |
  | image size     = 185px | Harry Edwards |
  | writer         = Monte Collins Elwood Ullman
  | producer      = Del Lord Hugh McCollum |
  | starring       = Moe Howard Larry Fine Curly Howard Vernon Dent Monte Collins Marjorie Deanne Cy Schindell Eddy Chandler Max Wagner|
  | cinematography = George Meehan | 
  | editing        = Paul Borofsky |
  | distributor    = Columbia Pictures |
  | released       =   |
  | runtime        = 17 10" |
  | country        = United States
  | language       = English
}}

Matri-Phony is the 63rd short subject starring American slapstick comedy team the Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959.

== Plot ==
The Stooges live in "Ancient Erysipelas," where they run Ye Olde Pottery shop. The powerful Emperor Octopus Grabus (Vernon Dent) is in search for a new wife again, with his sights set on redheads. Lovely Diana (Marjorie Deanne), who has kindled Grabus interest, hides out in the Stooges shop. A palace guard catches onto the scheme and all are brought to Grabus. The boys help Diana escape, while Moe and Larry convince Curly to dress up as Octopuss prospective bride. These two then destroy the glasses of the nearly-blind Grabus, who cannot see past his nose. The Stooges make a rapid escape by jumping out a palace windows, but end getting caught, upside-down, on the spears of three guards.

== Production notes ==
The title Matri-Phony is a pun on the word "Marriage|matrimony."   

Matri-Phony is the first Stooge film to employ the accordion-based, driving version of "Three Blind Mice" over the opening credits. This faster theme would be used until the end of 1944 in film|1944. 
 ) in Matri-Phony.]]
 Harry Edwards developed a reputation at Columbia Pictures as the studios worst director. After his next directoral effort with the Stooges (Three Little Twirps), the trio requested that they never work with him again. His poor directing skills are apparent in the closing scene When the Stooges are hanging upside down from the guards spears. Edwards voice is loudly heard directing Larry Fine: "Larry, grab the.....". Larry was the only one who was not holding onto his guards trousers: after receiving his direction, Larry quickly grabbed the pants. This type of exchange should have been silenced during post-production.   

==Quotes==
Curly: "Oh, food!"  (beholds the spread before him)  "Vitamans A B C D E F GEE, I like food!"

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 