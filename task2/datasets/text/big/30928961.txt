Abraham Lincoln: Vampire Hunter
 
{{Infobox film name           = Abraham Lincoln: Vampire Hunter image          = Abraham Lincoln - Vampire Hunter Poster.jpg alt            =   caption        = Theatrical release poster director       = Timur Bekmambetov producer       = Tim Burton Timur Bekmambetov Jim Lemley screenplay     = Seth Grahame-Smith based on       =   Benjamin Walker Dominic Cooper Anthony Mackie Mary Elizabeth Winstead Rufus Sewell Marton Csokas music          = Henry Jackman cinematography = Caleb Deschanel editing        = William Hoy studio  Tim Burton Productions   Dune Entertainment distributor    = 20th Century Fox released       =   runtime        = 105 minutes  country        = United States language       = English budget         = $69 million  . Boxoffice.com (2012-06-22). Retrieved on 2013-08-25.  gross          = $116.4 million  . Box Office Mojo (2012-09-20). Retrieved on 2013-08-25. 
}} action horror mashup novel of the Benjamin Walker 16th President of the United States (1861–1865), is portrayed in the novel and the film as having a secret identity as a vampire hunter.
 3D on June 20, 2012 in the United Kingdom and June 22, 2012 in the United States. The movie received mixed reviews, praising the action sequences and originality, but criticized for its lack of story, CGI over-reliance, and pacing.

==Plot==
<!--
PLEASE DO NOT add more detail to this summary, but instead adjust what is already present. Wikipedias guidelines for film plot summaries state that they should be no more than 700 words; this summary is 666 words long as of June 26, 2012. 
--> Benjamin Walker) Nancy (Robin Thomas (Joseph plantation owned William Johnson (Anthony Mackie), and intervenes when he sees Johnson being beaten by a slaver. Because of his sons actions, Thomas is fired. That night, Lincoln sees Barts break into his house and attack his mother. Nancy falls ill the following day, and dies shortly afterwards. Thomas tells Lincoln that Barts poisoned her.

Nine years later, a vengeful Lincoln attacks Barts at the docks, but Barts, who is actually a vampire, overpowers him. However, before Barts can kill him, Lincoln is rescued by Henry Sturges (Dominic Cooper). Sturges explains that vampires exist, and offers to teach Lincoln to be a vampire hunter. Lincoln accepts and, after a decade of training, travels to Springfield, Illinois armed with a modified silver-bladed axe. During his training, Sturges tells Lincoln that the vampires in America descend from Adam (Rufus Sewell), a vampire who owns a plantation in New Orleans with his sister, Vadoma (Erin Wasson). Sturges also tells Lincoln of the vampires weakness, silver, and presents him with a silver pocket watch.
 Joshua Speed Mary Todd (Mary Elizabeth Winstead) for whom he develops romantic feelings, despite the fact that she is engaged to Stephen A. Douglas (Alan Tudyk).

Lincoln successfully finds and defeats Barts. Before dying, Barts reveals that Sturges is also a vampire. Lincoln confronts Sturges, who reveals that, centuries ago, he was attacked and bitten by Adam. Because Sturges soul was impure, he became a vampire, and that prevented him from harming Adam or any other vampire (since "Only the living can kill the dead"). Sturges has since been training vampire hunters, hoping to destroy Adam.

Disappointed, Lincoln decides to abandon his mission. However, Adam learns of his activities and kidnaps Johnson to lure Lincoln into a trap at his plantation. Adam captures Lincoln and tries to recruit him, revealing his plans to turn the United States into a nation of the undead. Speed rescues his friends, and they escape to Ohio.

Lincoln marries Mary and begins his political career, campaigning to abolish slavery. Sturges warns Lincoln that the slave trade keeps vampires under control, as vampires use slaves for food, and if Lincoln interferes, the vampires will retaliate. After Lincolns election as President of the United States of America, he moves to the White House with Mary, where they have a son, William Wallace Lincoln (Cameron M. Brown). However, tragedy strikes when William is later bitten by a disguised Vadoma and dies.

Confederate President Jefferson Davis (John Rothman) convinces Adam to deploy his vampires on the front lines, and their appearance on the first day of the Battle of Gettysburg terrorizes the Union Army as their weapons prove ineffective on them.

Lincoln orders the confiscation of all the silverware in the area and has it melted to produce silver weapons. Speed, believing that Lincoln is tearing the nation apart, defects and informs Adam that Lincoln will transport the silver by train.

On the train, Adam and Vadoma, who have set fire to the upcoming trestle, attack Lincoln, Sturges, and Johnson. During the fight Adam learns that the train holds only rocks, not the silver. Speed then reveals that his betrayal was a ruse planned by him and Lincoln to lure Adam into a trap, and Adam kills Speed for this. The bridge begins to collapse, and Lincoln, with Johnson make an escape.
 assault and are met head on by the Union. Armed with their silver weapons, the Union Infantry destroy the vampires and eventually win the war.
 April 14, 1865, Sturges tells Lincoln that the remaining vampires have fled the country. Sturges tries to convince Lincoln to allow him to turn Lincoln into a vampire, so that he can become immortal and continue to fight vampires, but Lincoln declines before going to Fords Theatre with his wife. In modern times, Sturges approaches an unknown man at a bar in Washington, D.C., as he hopes that the man, who is in a situation identical to Lincolns before he went to kill Barts, will become the next vampire hunter.

==Cast==
 , who plays the titular role in this film, gets into character before touring the  .]] Benjamin Walker 16th President of the United States and the main protagonist. 
** Lux Haney-Jardine as Young Abraham Lincoln
* Dominic Cooper as Henry Sturges, Lincolns mentor in vampire hunting, a former vampire hunter, and a vampire who lost his wife and humanity to vampires. William Johnson, Lincolns earliest and closest friend. 
** Curtis Harris as Young Will
* Mary Elizabeth Winstead as Mary Todd Lincoln, Lincolns wife. 
* Rufus Sewell as Adam, the powerful leader of an order of vampires. 
* Marton Csokas as Jack Barts, a plantation owner and the vampire who killed Lincolns mother. Joshua Speed, Lincolns friend and assistant. 
* Joseph Mawle as Thomas Lincoln, Lincolns father.
* Robin McLeavy as Nancy Lincoln, Lincolns mother. 
* Erin Wasson as Vadoma, Adams sister.
* John Rothman as Jefferson Davis
* Cameron M. Brown as William Wallace Lincoln, Abraham and Marys third son. 
* Frank Brennan as Senator Jeb Nolan
* Jaqueline Fleming as Harriet Tubman
* Alan Tudyk as Stephen A. Douglas, an American politician from Illinois.   

==Production==
The film Abraham Lincoln: Vampire Hunter was first announced in March 2010 when Tim Burton and Bekmambetov paired to purchase film rights and to finance its development themselves. The books author, Seth Grahame-Smith, was hired to write the script.  Fox beat other studios in a bidding war for rights to the film the following October. 

In January 2011, with Bekmambetov attached as director, Walker was cast as Abraham Lincoln. He beat Adrien Brody, Josh Lucas, James DArcy, and Oliver Jackson-Cohen for the role.    Additional actors were cast in the following February.          Filming began in March 2011 in Louisiana.   The film had a budget of   and was produced in 3D.   

==Release==
Abraham Lincoln: Vampire Hunter was originally scheduled to be released in 2D and 3D on October 28, 2011, but was later pushed back to  , 2012.   The movie premiered in  , which is stationed in the Middle East. Several of the films stars attended the screening, including Anthony Mackie, Erin Wasson and Benjamin Walker, who dressed in character as Abraham Lincoln. The screening marks the first time that a major motion picture made its debut for United States servicemen and women. 

===Reception===
The film received generally mixed reviews. As of November 9, 2014, Rotten Tomatoes reports a "rotten" approval score of 35%, based on 181 reviews, with an average score of 4.9/10. The consensus reads that the film "has visual style to spare, but its overly serious tone doesnt jibe with its decidedly silly central premise, leaving filmgoers with an unfulfilling blend of clashing ingredients." Emanuel Levy of EmanuelLevy.com wrote that "Though original, this is a strenuous effort to combine the conventions of two genres."  The movie also garnered a "mixed or average" score of 42 out of 100 on Metacritic, based on 35 reviews. 

Richard Corliss of Time (magazine)|Time magazine elaborates, saying that "The historical epic and the monster movie run on parallel tracks, occasionally colliding but never forming a coherent whole."  Christy Lemire of Associated Press meanwhile, comments on the films tenor and visual effects, saying "What ideally might have been playful and knowing is instead uptight and dreary, with a visual scheme thats so fake and cartoony, it depletes the film of any sense of danger," awarding the film a rating of 1.5 out of 4.  Joe Morgenstern of The Wall Street Journal agrees, saying, "Someone forgot to tell the filmmakers ... that the movie was supposed to be fun. Or at least smart." 

Joe Neumaler of New York Daily News gives the film a rating of 1 out of 5, writing, "This insipid mashup of history lesson and monster flick takes itself semi-seriously, which is truly deadly."  The title is praised by Manohla Dargis of The New York Times, who adds, "its too bad someone had to spoil things by making a movie to go with it."  The title is further commented on by Barbara VanDenburgh from the Arizona Republic, who says, "The problem with movies based on a single joke is that a single joke is rarely funny enough to sustain the running time of a feature-length film".

Tony Kushner, the script writer of the actual Abraham Lincoln biopic released that same year, has stated that he thought the film was a "godforsaken mess", although this opinion had nothing to do with a historical perspective.
 Joe Williams of St. Louis Post-Dispatch, who calls it, "The best action movie of the summer," and praising the film for presenting "a surprisingly respectful tone toward American values and their most heroic proponent", calling "the battlefield scenes   suitably epic" and finally commending leading star Benjamin Walker, "a towering actor who looks like a young Liam Neeson and never stoops to caricature." 

===Box office===

Abraham Lincoln: Vampire Hunter grossed $37,519,139 at the domestic box office and $78,952,441 in International Markets.  It received a worldwide total of $116,471,580. 

===Home media===
Abraham Lincoln: Vampire Hunter was released on DVD and Blu-ray in the United States and Canada on October 23, 2012. 

==Accolades==
{| class="wikitable" rowspan="5;" style="text-align:center; background:#fff;"
|-
!Year!!Award!!Category!!Recipient(s)!!Result!!Ref.
|- 34th Young 2013
|Young 34th Young Best Performance Cameron M. Brown|| ||   
|}

==Soundtrack==
{{Infobox album  
| Name        = Abraham Lincoln: Vampire Hunter (Original Motion Picture Soundtrack)
| Type        = Soundtrack
| Artist      = Henry Jackman
| Cover       = Abraham-Lincoln-Vampire-Hunter-Soundtrack-List.jpg
| Released    =    (Australia) 
| Recorded    = 2011-2012
| Genre       = Soundtrack, film score
| Length      = 45:32
| Label       = Fox Music
| Producer    = Tim Burton Timur Bekmambetov Jim Lemley
{{Singles
 | Name = Abraham Lincoln: Vampire Hunter
 | Type = soundtrack Powerless
 | single 1 date = October 12, 2012
}}
}} Living Things premiered in the official trailer to Abraham Lincoln and was the first song to be played over the closing credits, followed by "The Rampant Hunter".  However, the song was not featured in the soundtrack,  but still the song was released as a single under the name of soundtrack in Japan. 

{{Track listing
| collapsed       = no
| headline        = 
| extra_column    = Artist
| total_length    = 45:32

| title1          = Childhood Tragedy
| note1           = 
| writer1         = 
| lyrics1         =
| music1          = 
| extra1          = Henry Jackman
| length1         = 0:54

| title2          = Vampires
| note2           = 
| writer2         = 
| lyrics2         = 
| music2          = 
| extra2          = Henry Jackman
| length2         = 3:06

| title3          = What Do You Hate?
| note3           = 
| writer3         = 
| lyrics3         = 
| music3          = 
| extra3          = Henry Jackman
| length3         = 1:15

| title4          = Power Comes from Truth
| note4           = 
| writer4         = 
| lyrics4         = 
| music4          = 
| extra4          = Henry Jackman
| length4         = 2:29

| title5          = You Are Full of Surprises
| note5           = 
| writer5         = 
| lyrics5         = 
| music5          = 
| extra5          = Henry Jackman
| length5         = 1:15

| title6          = Mary Todd
| note6           = 
| writer6         = 
| lyrics6         = 
| music6          = 
| extra6          = Henry Jackman
| length6         = 1:56

| title7          = The Horse Stampede
| note7           = 
| writer7         = 
| lyrics7         = 
| music7          = 
| extra7          = Henry Jackman
| length7         = 3:15

| title8          = Henry Sturges
| note8           = 
| writer8         = 
| lyrics8         = 
| music8          = 
| extra8          = Henry Jackman
| length8         = 0:55

| title9          = Adam
| note9           = 
| writer9         = 
| lyrics9         = 
| music9          = 
| extra9          = Henry Jackman
| length9         = 1:28

| title10         = Rescue Mission
| note10          = 
| writer10        = 
| lyrics10        = 
| music10         = 
| extra10         = Henry Jackman
| length10        = 1:15

| title11         = Inauguration
| note11          = 
| writer11        = 
| lyrics11        = 
| music11         = 
| extra11         = Henry Jackman
| length11        = 1:52

| title12         = All Slave to Something
| note12          = 
| writer12        = 
| lyrics12        = 
| music12         = 
| extra12         = Henry Jackman
| length12        = 2:49

| title13         = Emancipation
| note13          = 
| writer13        = 
| lyrics13        = 
| music13         = 
| extra13         = Henry Jackman
| length13        = 0:45

| title14         = Haunted by the Past
| note14          = 
| writer14        = 
| lyrics14        = 
| music14         = 
| extra14         = Henry Jackman
| length14        = 3:00

| title15         = Battle at Gettysburg
| note15          = 
| writer15        = 
| lyrics15        = 
| music15         = 
| extra15         = Henry Jackman
| length15        = 0:49

| title16         = Forging Silver
| note16          = 
| writer16        = 
| lyrics16        = 
| music16         = 
| extra16         = Henry Jackman
| length16        = 1:40

| title17         = 80 Miles
| note17          = 
| writer17        = 
| lyrics17        = 
| music17         = 
| extra17         = Henry Jackman
| length17        = 1:52

| title18         = The Burning Bridge
| note18          = 
| writer18        = 
| lyrics18        = 
| music18         = 
| extra18         = Henry Jackman
| length18        = 3:41

| title19         = Not the Only Railroad
| note19          = 
| writer19        = 
| lyrics19        = 
| music19         = 
| extra19         = Henry Jackman
| length19        = 1:38

| title20         = The Gettysburg Address
| note20          = 
| writer20        = 
| lyrics20        = 
| music20         = 
| extra20         = Henry Jackman
| length20        = 2:22

| title21         = Late to the Theater
| note21          = 
| writer21        = 
| lyrics21        = 
| music21         = 
| extra21         = Henry Jackman
| length21        = 2:00

| title22         = The Rampant Hunter
| note22          = iTunes exclusive
| writer22        = 
| lyrics22        = 
| music22         = 
| extra22         = Henry Jackman
| length22        = 5:30
}}
{{Track listing
| headline = Promotional single 
| collapsed = yes
| extra_column    = Artist Powerless 
| note223          = 
| writer23        = 
| lyrics23        = 
| music23         = 
| extra23         = Linkin Park
| length23        = 3:44
}}

==See also==
*Cultural depictions of Abraham Lincoln Hansel & Gretel
*Jesus Christ Vampire Hunter, a cult film about Jesus Christs life as a vampire hunter
* 
*Lincoln (2012 film)|Lincoln
*Vampire film

==References==
 

==External links==
 
 
*  
*  
*  
*  
*  
*  .  Academic Quarter, Spring 2013

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 