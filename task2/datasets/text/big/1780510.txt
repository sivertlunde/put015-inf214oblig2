Live a Little, Love a Little
{{Infobox film
| name           = Live a Little, Love a Little
| image          = Live a Little Love a Little Poster.jpg
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| director       = Norman Taurog
| producer       = Douglas Laurence
| screenplay     = Michael A. Hoey
| based on       =  
| starring       = {{Plainlist|
* Elvis Presley
* Michele Carey
* Rudy Vallee
}}
| music          = Billy Strange
| cinematography = Fred J. Koenekamp
| editing        = John McSweeney, Jr.
| studio         = Metro-Goldwyn-Mayer
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Live a Little, Love a Little is a 1968 American musical comedy film starring Elvis Presley.    It was directed by Norman Taurog, who had directed several previous Presley films.  This was to be Taurogs final film. Shortly thereafter, he went blind.  The film introduced the song "A Little Less Conversation"; an alternative take of which would form the basis of a remix that returned Presley to international music sales charts in 2002.

==Cast==
* Elvis Presley as Greg Nolan  
* Michele Carey as Bernice  
* Don Porter as Mike Lansdown  
* Rudy Vallee as Mr. Penlow  
* Dick Sargent as Harry  
* Sterling Holloway as The Milkman  
* Celeste Yarnall as Ellen  
* Eddie Hodges as The Delivery Boy  
* Joan Shawlee as Robbies Mother  
* Mary Grover as Miss Selfridge
* Emily Banks as RKC&P Receptionist

==Plot==
Greg Nolan (Presley) is a newspaper photographer who lives a carefree life—that is, until he encounters an eccentric, lovelorn woman named Bernice (Carey) on the beach. Bernice assumes different names and personalities whenever the mood hits her. (She introduces herself to Greg as "Alice" but shes known to the delivery boy as "Susie" and to the milkman as "Betty.")

After having her Great Dane dog, Albert (which was reportedly Presleys real-life dog Brutus, although Priscilla Presley has stated that it was a trained dog used for the film), chase Greg into the water when he insults her after a kiss, Bernice invites him to stay at her beachfront home. She later manages to make him lose his job and apartment after drugging him, which leaves him in a deep sleep for days.

However, Bernice also manages to find Greg another home. He wants to repay her so he gets two full-time photographer jobs: one for a Playboy Magazine|Playboy-like magazine owned by Mike Lansdown (Porter), the other for a very conservative advertising firm co-owned (with other partners) by Mr. Penlow (Vallee). The two jobs are in the same building, forcing Greg to run from one to the other (up and down the stairwell) without being detected. He also must deal with Bernice and her eccentric ways.

==Background==
Based on the 1965 novel Kiss My Firm But Pliant Lips by Dan Greenburg, and with a screenplay co-written by Greenburg, Live a Little, Love a Little was a departure from the standard Presley film of the period.  It had a more mature tone than other Presley musicals with strong language, drug references, and an implied sexual encounter. 

Elvis was paid $850,000 plus 50% of the profits. Michael A. Hoey, Elvis Favorite Director: The Amazing 52-Film Career of Norman Taurog, Bear Manor Media 2013 
 Malibu coast, at Marineland of the Pacific|Marineland, and at the Los Angeles Music Center. 
 Joe Esposito, also appeared. 

Released on October 23, 1968, the film failed to impress most critics.  Due to a very poor performance on US release, the film was not released at all in many regions, including the UK. 

==Soundtrack== Frank and Nancy Sinatra, and attuned to current trends in popular music brought in a group of musicians outside of Presleys usual stable and written arrangements that went afield from Presleys usual sound.  "Almost in Love" was given a late-night cocktail-jazz quality, "Edge of Reality" was a piece of pseudo-acid rock, and "A Little Less Conversation" written by Strange and his new discovery who would write several more songs for Presley, Mac Davis, bordered on funk.

"A Little Less Conversation" was released as a single with "Almost in Love" on the  .

===Track listing=== Guy Fletcher)
# "Edge of Reality" (Bernie Baum, Bill Giant, Florence Kaye)
# "A Little Less Conversation" (Billy Strange, Mac Davis)
# "Almost in Love" (Luiz Bonfá, Randy Starr)

===Personnel===
* Elvis Presley – vocals
* B. J. Baker, Sally Stevens, Bob Tebow, John Bahler – backing vocals
* Joseph Gibbons – electric guitar
* Neil Levang – electric guitar Alvin Casey – electric guitar
* Charles Britz – electric guitar
* Don Randi – piano
* Charles Berghofer – bass
* Larry Knechtel – bass
* Hal Blaine – drums
* Gary Coleman – drums

==Quotes==
Celeste Yarnall, who played Ellen, recalled the making of the film and her impressions of Elvis Presley:
 

==DVD==
Live a Little, Love a Little was released to DVD by Warner Home Video on August 7th, 2007 as a Region 1 widescreen DVD.

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 