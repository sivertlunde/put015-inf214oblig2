The Winslow Boy (1999 film)
 
{{Infobox film
| name           = The Winslow Boy
| image          = The Winslow Boy (1999 film) original poster.jpg
| image_size     = 200px
| caption        = Theatrical release poster
| director       = David Mamet
| producer       = Sarah Green
| writer         = Terence Rattigan (play) David Mamet (screenplay)
| starring       = Nigel Hawthorne Rebecca Pidgeon Jeremy Northam Guy Edwards Gemma Jones
| music          = Alaric Jans
| cinematography = Benoît Delhomme
| editing        = Barbara Tulliver
| studio         =
| distributor    = Sony Pictures Classics
| released       = 16 April 1999 (U.S.) 29 October 1999 (UK)
| runtime        = 104 minutes
| country        = United Kingdom United States
| language       = English
| budget         =
| gross          =
}} 1999 period drama   film directed by  David Mamet. Starring Nigel Hawthorne, Rebecca Pidgeon, Jeremy Northam and Gemma Jones. Set in London before World War I, it depicts a family defending the honour of its young son at all cost. The screenplay was adapted by Mamet based on  Terence Rattigans  dramatic play The Winslow Boy.

It was screened in the Un Certain Regard section at the 1999 Cannes Film Festival.   

==Plot== Royal Naval College, Osborne, is unexpectedly home. Ronnie has been accused of the theft of a postal order. An internal enquiry, conducted without notice to his family and without benefit of representation, finds him guilty and Mr. Winslow is "requested to withdraw" his son from the college (the formula of the day for expulsion). Ronnie proclaims his innocence and his father believes him—enough so that he demands an apology from the College. When the college refuses to reinstate Ronnie, Arthur decides to take the matter to court. With the help of his daughter and Desmond Curry, a solicitor and friend of the family, Mr. Winslow decides to hire the most highly sought after barrister in England at the time, Sir Robert Morton, known also to be a shrewd opposition Member of Parliament. 
 House of Commons, the government yields, and the case does come to court.

Catherine had expected Sir Robert to decline the case, or at best to treat it as a political tool; instead, he is coolly matter-of-fact about having been persuaded of Ronnies innocence by his responses to questioning (in fact, a form of cross-examination, to see how young Ronnie would hold up in court) in the presence of his family. Catherine, a left-wing suffragette, is not enthusiastic about Morton, whom she considers cold and heartless. Catherine is also disturbed by Sir Roberts establishment views: he is a conservative opponent of women’s suffrage. "He always speaks out against what is right," she observes to her father, though she admires his legal skills. Morton, for his part, obviously is quite taken with Catherine from the moment he meets her in his offices; later he stares at her as she arrives at the House of Commons and as she watches the proceedings from the ladies gallery.
 Lord Chief Justice, rather than drop the case. The least affected is Ronnie, who happily has been transferred to a new school.

At trial, Sir Robert (working together with Desmond Curry and his firm) is able to discredit much of the supposed evidence.  The Admiralty, certainly embarrassed and presumably no longer confident of Ronnies guilt, abruptly withdraws all charges against him, proclaiming the boy entirely innocent.

When their resounding victory arrives, not a single member of the  Winslow’s family is present at court. It is Violet, the maid, who tells Mr. Winslow and Catherine what has happened at court. Shortly after, Sir Robert appears in the Winslows’ home to share the good news. The film ends with a suggestion that romance might yet blossom between Sir Robert and Catherine, who acknowledges that she had misjudged him all along.

==Cast==
* Nigel Hawthorne - Arthur Winslow
* Rebecca Pidgeon - Catherine Winslow
* Jeremy Northam - Sir Robert Morton
* Guy Edwards - Ronnie Winslow
* Gemma Jones - Grace Winslow
* Matthew Pidgeon - Dickie Winslow
* Aden Gillet - John Waterstone
* Colin Stinton - Desmond Curry
* Sarah Flind - Violet
* Neil North - First Lord

==Production==
Shot in London, the film was directed by David Mamet who originally had thought of making a theatrical production of Rattigans dramatic play, which had been previously filmed in 1948. After failing to find backers for the project, Mamet decided to adapt the play for the big screen. It was produced by Sarah Green with Sally French, Michael Barker and Tom Bernard as associate producers. The screenplay was adapted by Mamet based on Rattigans play. The music score was by Alaric Jans and the cinematography by Benoît Delhomme.

Neil North who played the title role in the 1948 version also appears in this version (as the First Lord of the Admiralty). In the audio commentary for the DVD release, Mamet says he had already cast North in the film before he was told that North also appeared in the 1948 version.
 Mike Newell originated the role of Ronnie Winslow in the original stage production. However, this is mistaken; Northam has confused another actor who is also named Michael Newell, who is now a real-estate agent. Mike Newell (the director) was born in 1942, and since the play premiered in 1946, it is not possible for a four-year-old to have played that role. Rebecca Pidgeon, who plays Catherine, is the wife of playwright and director David Mamet. Matthew Pidgeon, who plays Dickie Winslow (Catherines brother), is Rebecca Pidgeons real-life brother.

==Overview==
Set against the strict codes of conduct and manners of the age, The Winslow Boy is based on a fathers fight to clear his sons name after the boy is expelled from Osborne Naval College for stealing a postal order.  To clear the boys name was imperative for the familys honour; had they not done so, they would have been shunned by their peers and society.  The boys life would have been wrecked by the stain on his character.
 Solicitor General accepted that Archer-Shee was innocent, and ultimately the family was paid compensation. George Archer-Shee died in the First World War and his name is inscribed on the war memorial in the village of Woodchester in Gloucestershire where his parents lived.

==Awards==
*Winner Best British Performance (Jeremy Northam) - Edinburgh International Film Festival

== DVD release ==
The film was released on DVD in the U.S.A. on February 2000. The special features include an audio commentary by director/screenwriter David Mamet and cast members, Jeremy Northam, Nigel Hawthorne and Rebecca Pidgeon. The DVD also includes theatrical trailers for The Winslow Boy and The Spanish Prisoner. There is a short "making of" feature.

==References==
 

== External links ==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 