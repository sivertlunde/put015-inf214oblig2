Sons of Matthew
 
 
{{Infobox film name           = Sons of Matthew image          =  caption        =  producer       = Charles Chauvel director  Charles Chauvel writer         = Charles Chauvel Elsa Chauvel Maxwell Dunn
| based on = novels by Bernard OReilly narrator = Wilfred Thomas music = Henry Krips cinematography = Carl Kaiser Bert Nicholas editor = Terry Banks starring  Tommy Burns
| studio = Greater Union Theatres Universal-International distributor    =  Universal-International released       = 16 December 1949 (Australia) 26 January 1950 (UK) 5 January 1950 (USA) runtime        = 107 min. (Australia) 97 min. (USA) country        = Australia language       = English budget         = ₤120,000 Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, 209.  or £500,000   
| gross = over £50,000 (Australia) 
}} Charles Chauvel. The film was shot in 1947 on location in Queensland, Australia and the studio sequences in Sydney. Sons of Matthew took 18 months to complete, but it was a great success with Australian audiences when it finally opened in December 1949.

Sons of Matthew is a legendary film in the history of Australian cinema, partly because of the adverse conditions in which it was made. Maxwell Dunn wrote later in his book How they Made Sons of Matthew that, during filming, it was the wettest season in 80 years in Queensland. For UK and US release Universal-International cut the film by 30 minutes, added some American narration and renamed it The Rugged ORiordans. 

==Plot summary==
Irishman Matthew ORiordan and his English wife Jane raise a family of five sons and two daughters on their farm in the valley of Cullenbenbong in northern New South Wales. They battle drought, flood and fire. The wife of neighbour Angus McAllister dies and they help raise their daughter Cathy.

Years go by and the children grow up. Eldest brother Shane is inspired by his uncle Jack, who tells them about virgin rainforest on Lamington plateau in southern Queensland. They decide to move there and establish a farm. They are accompanied by Angus and Cathy McAllister. By this stage Cathy is engaged to the second son, Barney.

The ORiordan brothers clear the land and start building a slab hut. Cathy realises she is in love with Shane and he falls for her. A huge storm his the farm and the brothers fight. Barney knocks out Shane, hurting his spine.

Shane recovers, Barney earns his forgiveness by working hard. Shane and Cathy are married.

==Cast==
 
*Michael Pate as Shane ORiordan
*Ken Wayne as Barney ORiordan Tommy Burns as Luke ORiordan
*John Unicomb as Terry ORiordan
*John Ewart as Mickey ORiordan
*Wendy Gibb as Cathy McAllister 
*John OMalley as Matthew ORiordan
*Thelma Scott as Jane ORiordan
*Dorothy Alison as Rose ORiordan
*Diane Proctor as Mary ORiordan
*Tom Collins as young Shane
*Max Lemon as young Barney
*Rodney Fielder as young Luke
*Doug Smith as young Terry
*Jimmy White as young Mickey
*Marion Dickson as young Rose
*Baby Lawson as young Mary John Fegan as Jack Farrington
*Robert Nelson as Angus McAllister
*Barbara Armstrong as young Cathy
*Laurel Young as Bessie Benson
*Nonnie Peifer as Molly Benson
*Betty Orme as Selina Benson
*John Fleeting as doctor
*Carrie Moore as midwife
*Alan Poolman as Dan McGregor
 

==Development== Grant Taylor was mentioned as a possible star. 

Chauvel commissioned Maxwell Dunn and Gwen Meredith to write a script about the OReillys and Bernard OReillys rescue of survivors from the crash of a Stinson aeroplane in 1937. (An event filmed in 1987 as The Riddle of the Stinson.)  James Bancks also worked on the script. Eventually Chauvel decided to make an original story of pioneers.

Chauvels normal backer, Herc McIntyre of Universal Pictures, agreed to invest in the movie. He persuaded Norman Rydge of Greater Union to join him in partnership. The budget was originally announced as being ₤100,000.  The Queensland government contributed ₤3,000 to production costs. 

Casting took several months, with most of the actors being unknowns.  It was the first lead role for Michael Pate, Wendy Gibb and Ken Wayne. Boxer Tommy Burns was given an important support role. 

==Production==
In March 1947 a unit of about 70 people set off for the main location near Beaudesert, Queensland|Beaudesert.  Filming coincided with near-constant rain – the first three months of shooting saw only three weeks of weather suitable for filming.  Locations sometimes had to be reached by pack horse and foot. A second unit under Carl Kayser was brought out to location to assist production. 

After six months on location, the unit moved to the studios of Cinesound Productions in Bondi. They filmed there for two months then returned to Queensland for a further five months. In March 1948 they returned to the Bondi studio and reshot several scenes. Shooting took eighteen months in total. Charles Chauvel then shot an alternative ending in the Blue Mountains. This ending was eventually discarded.

While Chauvel was filming in Sydney, his home was robbed. 

==Reception==
The movie had cost so much money it needed to be successful in Australia and overseas. The film was very popular at the local box office being seen by an estimated 750,000 Australians. 

The movie was cut for overseas release, with narration added and thirty minutes removed, including a scene where Wendy Gibb bathes nude. 

===Overseas Release===
Initial response in the UK and USA was poor, with the film being pulled from a cinema in New York after only a week.  

The New York Times called the film "a dismally disappointing effort, cut along the most crude and conventional lines". THE SCREEN IN REVIEW: Dream No More, Story About Israel in Documentary-Type Picture, of Ambassador At the Park Avenue At the Palace
By BOSLEY CROWTHER. New York Times 06 Jan 1950: 25. 

However after a slow start the film took off commercially in the UK, helped by a competition with a prize of a trip to Australia, which over half a million people entered.  Eventually the movie made a small profit. Overseas reviews were mixed. 

Ken G. Hall later blamed the difficulties involved in making this film on scaring off Norman Rydge from investing in feature film production, contributing to Halls failure to make another feature after 1946.

Several of the cast attempted to forge careers overseas, including Michael Pate, Tommy Burns,  and Wendy Gibb. 

==References==
 
* 

==External links==
* 
*  at Australian Screen Online
*  at National Film and Sound Archive
* 
*  at Oz Movies
 

 
 
 
 
 