The Meteor Man (film)
 
 
{{Infobox film
| name           = The Meteor Man
| image          = Meteor man.jpg
| image_size     =
| caption        = Theatrical one-sheet for The Meteor Man Robert Townsend 
| producer       = Robert Townsend Loretha C. Jones
| writer         = Robert Townsend
| narrator       =
| starring       = Robert Townsend Marla Gibbs Eddie Griffin Robert Guillaume James Earl Jones Bill Cosby Another Bad Creation
| music          = Cliff Eidelman
| cinematography = John A. Alonzo
| editing        = Adam Bernardi Richard Candib Robaire W. Estel Andrew London Pam Wise
| distributor    = Metro-Goldwyn-Mayer
| released       = August 6, 1993 (USA)
| runtime        = 100 minutes
| country        = United States
| language       = English
| budget         = $30,000,000 (estimated)
| gross          = $8,023,147 (USA)
}} superhero comedy Robert Townsend Reservoir Hill neighborhood of Baltimore, Maryland|Baltimore. Robert Townsend named the films protagonist Jefferson Reed, after one of his childhood heroes, his favorite teacher.

==Plot== Robert Townsend) vagrant named Marvin (Bill Cosby). Reed awakens several days later in the hospital, but when his bandages are taken off, he is miraculously healed of all injuries.

Jeff soon discovered that the meteorite had left him with spectacular abilities such as flight, x-ray/laser vision, superhuman strength, speed, and hearing, invulnerability, healing powers, absorb a books content by touch, super breath, telepathy with dogs (which he uses to communicate with his own dog, Ellington), and telekinesis. Confiding this to his parents Ted (Robert Guillaume) and Maxine (Marla Gibbs), they convince him to use his powers to help the community. His mother designs a costume and as the Meteor Man, he takes on the Golden Lords. He shuts down 15 crack houses, stops 11 robberies, brings peace between the police, the Crips (Cypress Hill), and the Bloods (Naughty by Nature) where they begin to work together to rebuild the community they destroyed, and plants a giant garden in the middle of the ghetto.
 superpowers and engage in a brawl. When Simon is about to throw a dumpster at Jeff, he hears Ellington barking telling Jeff that he can win and throws the dumpster at Ellington instead, fatally injuring Ellington. This angers Jeff and he disappears and returns as Meteor Man. They continue with their brawl with Meteor Man winning and draining Simon of his powers by absorbing them. The locals all gather around Ellington who is now lying on the street, whimpering in pain. Jeff uses his x-ray vision to see that Ellingtons ribs are broken. Before Jeff can do anything, his powers fade away, again. But just then, Marvin comes over and uses the last of his powers from the meteor fragment to heal Ellingtons injuries, thus saving Ellingtons life. The locals all applaud. 

Later, Anthony Byers confronts Meteor Man, but is outnumbered by the Bloods and the Crips who show up to protect Meteor Man. Anthony Byers and his gang are then arrested by the police after attempting to "take a vacation to the Bahamas".

==Cast==
  Robert Townsend as Jefferson Reed/Meteor Man
* Marla Gibbs as Maxine Reed, Jeffersons mother
* Eddie Griffin as Michael Anderson, Jeffersons friend.
* Robert Guillaume as Ted Reed, Jeffersons father
* James Earl Jones as Earnest Moses, Jeffersons neighbor.
* Bill Cosby as Marvin, a mute vagrancy (people)|vagrant.
* Marilyn Coleman as Mrs. Walker, Jefferson Reeds Landlord|landlady.
* Roy Fegan as Simon Caine, the leader of the Golden Lords.
* Don Cheadle as Goldilocks, member of the Golden Lords.
* Big Daddy Kane as Pirate, member of the Golden Lords.
* Frank Gorshin as Anthony Byers, a drug lord. Sinbad as Malik, Stacys boyfriend Nancy Wilson as Principal Laws
* Tommy Tiny Lister as Digit
* Wallace Shawn as Mr. Little
* Faizon Love as Maurice
;Cameos
* Naughty by Nature as The Bloods
* Cypress Hill as The Crips
* Biz Markie as Himself
* Luther Vandross as Jamison
* Another Bad Creation as Jr. Lords, the children members of the Golden Lords. John Witherspoon as Clarence James Carter III
 

==Soundtrack==
# "Cant Let Her Get Away" - Michael Jackson
# "Its for You (Shanice song)|Its for You" - Shanice Lisa Taylor
# "You Turn Me On" - Hi-Five
# "Who Can"
# "Your Future Is Our Future" - Daryl Coley & Frank McComb
# "I Say a Prayer" - Howard Hewett
# "Is It Just Too Much" - Keith Washington
# "Somebody Cares for You" - Frank McComb
# "Good Love" - Elaine Stepter
# "Aint Nobody Bad (Like Meteor Man)" - Big Hat Ray Ray

==Comic== Meteor Man.

==Reception==
Rotten Tomatoes gives the film a score of 29% based on 14 reviews. 

Peter Rainer of The Los Angeles Times compares the film to "a fairly clunky sitcom" with its sense of righteous do-goodism, and although the film intends to inspire, it instead sends the message that it would take a superhero to clean up inner-city gang violence.      
Roger Ebert writes, "The movie contains big laughs and moments of genuine feeling, but it seems to be put together out of assorted inspirations that were never assembled into one coherent story line." 

==References==
 

==External links==
*  
*  
*   at Superheroes Lives

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 