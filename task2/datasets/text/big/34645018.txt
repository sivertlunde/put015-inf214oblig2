The Idle Rich
 
{{infobox film
| name           = The Idle Rich
| image          =
| imagesize      =
| caption        =
| director       = William C. deMille
| producer       = Louis B. Mayer Irving Thalberg
| based on       =  
| writer         = Clara Beranger (scenario)
| starring       = Conrad Nagel Bessie Love Leila Hyams
| music          = Leonard Smith
| editing        = Conrad A. Nervig
| distributor    = MGM
| released       = June 15, 1929
| runtime        = 80 minutes
| country        = United States
| language       = English
}}
 Edith Ellis.    
 Robert Young and Ruth Hussey.

==Plot==
Wealthy businessman William "Will" Van Luyn (Conrad Nagel) proposes to his secretary, Joan Thayer (Leila Hyams). She accepts, but worries how her family will react. When he meets them for the first time, little sister Helen (Bessie Love) is delighted by the advantages he will bring, but cousin Henry starts lecturing him on the virtues of the downtrodden middle class. The rich can afford medical treatment, and the poor can rely on the government to pay the bills, but the middle class has to shoulder the financial burden on its own. Nevertheless, Will marries Joan.

A month later, their honeymoon is interrupted when he has to return to the city to deal with a lawsuit. Joan insists that they stay in her familys apartment for a few weeks. Her father (James Neill) turns down Wills offer to buy them a luxurious twelve-room apartment in a much better neighborhood, and the rest of the family supports his decision. Will gamely goes along with Joans plan, but insists her bedroom is too small for the two of them, and sleeps on the living room couch. This does not contribute to the couples marital bliss; he has insisted that she quit working, so she does not see much of him during either the day or the night.

Wills good intentions only cause trouble. He secretly arranges for Henry to be offered a good job in South America, but Henry finds out about his hand in it and turns the opportunity down. When Will gives Helen expensive jewelry as a belated birthday present, her fiancé, truck driver Tom Gibney, is furious. He picks a fight, but Will easy knocks him down with a couple of punches.

To teach his in-laws a lesson, Will announces that he is going to become one of them by giving away his fortune to charity. Joans father has just lost his job, and the entire family admit they have been wrong and try to talk Will out of it. Finally, he reveals that it was all a ploy to get them to realize that his wealth is not something to be ashamed of. They no longer object when he showers them with gifts.

==Cast==
* Conrad Nagel as William van Luyn
* Bessie Love as Helen Thayer
* Leila Hyams as Joan Thayer
* Robert Ober as Henry Thayer
* James Neill as Mr. Thayer
* Edythe Chapman as Mrs. Thayer
* Paul Kruger as Tom Gibney
* Kenneth Gibson as Frank Thayer, Helens brother

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 