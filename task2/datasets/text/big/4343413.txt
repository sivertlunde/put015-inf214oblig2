Pulse (1988 film)
{{Infobox film|
  name           = Pulse |
  image          = |
  caption        = |
  director       = Paul Golding |
  producer       = Patricia A. Stallone|
  screenplay     = Paul Golding | Joseph Lawrence Matthew Lawrence | Jay Ferguson |
  cinematography = Peter Lyons Collister |
  editing        = Gib Jaffe | Aspen Film Society |
  distributor    = Columbia Pictures |
  released       = March 4, 1988 |
  runtime        = 95 min. |
  language       = English |
  gross          = $40,397  |
}} 1988 science Joseph Lawrence, and Matthew Lawrence.  The films title refers to a highly aggressive and intelligent pulse of electricity that terrorizes the occupants of a suburban house in Los Angeles, California.  The film was produced through Columbia Pictures and the Aspen Film Society and distributed by Columbia Pictures.  The titular Pulse and its accompanying elements were designed by Cinema Research.

==Plot==
A highly aggressive, paranormal intelligence thriving within the electrical grid system of Los Angeles, California is moving from house to house.  It terrorizes the occupants by taking control of the appliances, killing them or causing them to wreck the house in an effort to destroy it.  Once this has been accomplished, it travels along the power lines to the next house, and the terror restarts.  Having thus wrecked one household in a quiet, suburban neighborhood, the pulse finds itself in the home of a boys divorced father whom he is visiting.  It gradually takes control of everything, injuring the stepmother, and trapping father and son, who must fight their way out.

==Release==
The film was promoted by the taglines "It traps you in your house...then pulls the plug," "In every second of every day, it improves our lives.  And in a flash, it can end them," and also "the ultimate shocker."

==Reception==
Pulse has a 58% approval rating at the online review aggregator Rotten Tomatoes based on 12 reviews.   

==Casting==

* Cliff De Young as Bill Rockland.
* Roxanne Hart as Ellen Rockland.
* Joey Lawrence as David Rockland.
* Matthew Lawrence as Stevie.
* Charles Tyner as Old Man Holger.
* Dennis Redfield as Pete.
* Robert Romanus as Paul.
* Myron Healey as Howard.
* Michael Rider as Foreman.
* Jean Sincere as Ruby.
* Terry Beaver as Policeman.
* Greg Norberg as Policeman.
* Tim Russ as Policeman.

==Music==
The musical score for Pulse was composed by  .

==References==

 

== External links ==
*  

 
 
 
 
 
 
 
 


 
 