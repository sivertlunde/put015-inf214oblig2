Life and Lyrics
{{Infobox Film
| name           = Life and Lyrics
| image          = Lifelyrics.jpg
| caption        = CD cover for the Life and Lyrics soundtrack album
| director       = Richard Laxton
| producer       = Esther Douglas Fiona Neilson
| writer         = Ken Williams
| starring       = Ashley Walters Louise Rose Chris Steward
| music          = Various John Daly
| editing        = Tracey Wadmore-Smith
| distributor    = United International Pictures
| released       = 29 September 2006
| runtime        = 99 mins
| country        = United Kingdom English
| budget         = 
| preceded_by    = 
| followed_by    = 
}} 2006 United British film directed by Richard Laxton and starring Ashley Walters, a former member of the garage band So Solid Crew.

A variation on the Romeo and Juliet theme, it is set in modern day South London, it tells the story of two rival crews of DJs and what happens when a member of one crew falls for a member of another.

The Motion Crew are a group of south London rappers led by DJ Danny D-Biz Lewis (Ashley Walters). For Danny, his music is everything and his crew are like family. But loyalties are put to the test when Danny falls for the beautiful Carmen, whom he soon finds out is related to a member of their most hated rivals, the violent and arrogant Hard Cash Crew.

Both crews seem certain to face each other in the rap battle finals of the prestigious Mic Masters competition, a thrilling event that will offer a final chance for Danny to beat his rivals and make it big.

== See also == 
*Bullet Boy
*Kidulthood Adulthood
*West 10 LDN
*Top Boy

== External links ==
*  

 

 
 
 
 