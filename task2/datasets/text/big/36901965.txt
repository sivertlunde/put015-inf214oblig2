Captain Lash
{{infobox film
| name           = Captain Lash 
| image          =
| imagesize      =
| caption        =
| director       = John G. Blystone  William Fox
| writer         = Daniel Tomlinson
| starring       = Victor McLaglen
| music          = 
| cinematography = Conrad Wells
| editing        = James Kevin McGuinness 
| distributor    = Fox Film Corporation
| released       =  
| runtime        = 
| country        = USA
| language       = English  
}}

Captain Lash (1929) is a film adventure drama produced and distributed by Fox Film Corporation starring Victor McLaglen and Claire Windsor. The screenplay was written by Daniel Tomlinson.   

==Plot== stoker on a steam ship whose shipmates have nicknamed "Captain". Lash somehow grabs the attention of society dame passenger Cora Nevins. Nevins is actually a jewel thief whos lifted diamonds from wealthy passenger Arthur Condrax. She needs Lash to aid in sneaking the "ice" ashore at Singapore. Cocky is Lashs concertina-playing buddy and uses it to signal Lash.

==Cast==

* Victor McLaglen - Captain Lash 
* Claire Windsor - Cora Nevins
* Jane Winton - Babe
* Clyde Cook - Cocky Arthur Stone - Gentleman Eddie  Albert Conti - Alex Condrax
* Jean Laverty - Queen
* Frank Hagney - Bull Hawks
* Boris Charsky - Condaxs servant
* Marcelle Corday - Mrs. Koenig

==References==
 

==External links==
*  
* 

 
 
 
 
 
 


 