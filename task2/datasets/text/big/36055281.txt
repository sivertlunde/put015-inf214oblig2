Radar Secret Service
 
{{Infobox film
| name           = Radar Secret Service
| image          =
| producer       = Robert L. Lippert Barney A. Sarecky
| director       = Sam Newfield
| writer         = Baryl Sachs John Howard Adele Jergens Russell Garcia Richard Hazard	 Ernest Miller
| editing        = Carl Pierson
| distributor    =
| released       =  
| runtime        = 60 min English
}} John Howard, Samuel Newfield. The movie was featured on the American television show Mystery Science Theater 3000.

==Plot==
Set in post-World War Two, stolen Uranium-238 goods are tracked by the US Government, using new radar technology; the girlfriend of a gang member is also recruited as an informant. Though the radar tracking device helps, in the end, it still takes an undercover girl to get back the looted shipment from the criminal masterminds.

==Cast== John Howard as Bill Travis
* Adele Jergens as Lila
* Tom Neal as Mickey Moran
* Myrna Dell as Marge
* Sid Melton as Pill Box
* Ralph Byrd as Static Robert Kent as Henchman Benson
* Pierre Watkin as Mr. Hamilton (credited as Pierre Watkins)
* Tris Coffin as Michael
* Riley Hill as Henchman Blackie Robert Carson as Tom, Radar Operator
* Kenne Duncan as Michaels Henchman
* Holly Bane as Truck Radio Operator, OX-2
* Bill Crespinel as Helicopter Operator
* Bill Hammond as Henchman Joe #2
* Jan Kayne as Myrtle the Maid
* John McKee as Second Bruiser
* Marshall Reed as First Bruiser
* Boyd Stockman as Henchman Joe #1 Bob Woodward as Henchman Gus

==MST3K== railroad crossing Lost Continent Hercules Against the Moon Men.

==External links==
*  

 
 
 
 

 