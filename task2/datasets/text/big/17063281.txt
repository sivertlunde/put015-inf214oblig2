The Simple-Minded Murderer
{{Infobox film
| image          = The Simple-Minded Murder.jpg
| name           = The Simple-Minded Murderer
| original name  = Den Enfaldige Mördaren
| caption        = Swedish cover.
| director       = Hans Alfredson
| producer       = 
| writer         = Hasse Alfredson
| narrator       = Stellan Skarsgård Maria Johansson
| music          = Rolf Sersam, Giuseppe Verdi
| cinematography = Rolf Lindström, Jörgen Persson (cinematographer)|Jörgen Persson
| editing        = Jan Persson
| distributor    = Svensk Filmindustri (SF)
| released       =  
| runtime        = 108 minutes
| country        = Sweden
| language       = Swedish
| budget         = 
}}

The Simple-Minded Murderer ( ) is a 1982 Swedish drama film directed by Hans Alfredson, starring Stellan Skarsgård, as the feeble-minded Sven Olsson.

==Plot==
The story takes place in 1930s Skåne, Sweden, and focuses on Sven, who is Cleft lip and palate|hare-lipped and thus cant speak correctly. Most people consider him stupidity|stupid, and call him an Idiot (usage)|idiot.

The film begins with Sven and a woman, who we later learn is called Anna, driving an old car across the landscape. The sun is setting, and in the sky Sven sees three angels. He and Anna hide in an old house, and while Anna makes herself comfortable, Sven throws a huge, bloodstained blade into a Water well|well. He lies down beside Anna and starts his inner monologue about how it all began.

When Svens mother died, he was "taken care of" by Höglund (Hans Alfredsson), an evil factory owner who is a member of the local Nazi party, and lives on a farm. Sven must work on Höglunds farm without pay, and sleep among cows in the stables, where he is tormented by a rat. Being very goodhearted, Sven cannot make himself drown the animal once he has caught it, because he simply cant take another life.

==Cast==
*Stellan Skarsgård  as Sven Maria Johansson  as Anna
*Hans Alfredson  as Höglund
*Per Myrberg  as Andersson
*Lena-Pia Bernhardsson  as Mrs. Andersson
*Nils Ahlroth  as Månsson
*Lars Amble  as Bengt
*Carl-Åke Eriksson  as Wallin
*Cecilia Walton  as Vera
*Wallis Grahn  as Mrs. Höglund
*Else-Marie Brandt  as Svens mother
*Gösta Ekman  as The new driver
*Carl Billquist  as Flodin
*Lena Nyman  as Woman without legs
*Björn Andrésen  as Angel

==Awards and reception== Best Director Best Film Best Actor (Stellan) at the 18th Guldbagge Awards.    Stellan also won the Silver Bear for Best Actor at the 32nd Berlin International Film Festival for his role as Sven.   

The well-known Swedish  . Hasse Alfredssons resources seems unlimited and my admiration for his creativity and the wealth of his ideas are absolute". 

==Trivia==
*The story is based on a short chapter in Alfredsons book En Ond man (An Evil Man). The chapter was called "Idiotens berättelse" ("The Idiots Story") and was an inner monologue held by the then nameless narrator.
*Another inspiration for the movie came when Alfredson first heard Requiem (Verdi)|Requiem by Giuseppe Verdi. A piece of music, he found very powerful and he knew he wanted to use that in a movie curse and stamp on the ground in order to get really angry.
*Alfredson has stated in an interview that the character of Sven is loosely based on a real person, named Hans, whom he once knew. Höglund is also based on a real person and a scene from the movie, when Höglund throws a poor farmers money into the fireplace on christmas eve is based on a true story.

==References==
 
* Staffan Schöier & Stefan Hermelin (2005). Hasse & Tage: Svenska Ord & co: Saga & Sanning ISBN 978-91-0-011600-2

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 