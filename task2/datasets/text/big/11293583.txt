How High Is Up?
{{Infobox film|
  | name           = How High Is Up? |
  | image          = HowighUpTITLE.jpg |
  | caption        =  |
  | director       = Del Lord |
  | writer         = Elwood Ullman | Bert Young Bruce Bennett Edmund Cobb|
  | producer       = Del Lord Hugh McCollum |
  | cinematography = Allen G. Siegler |
  | editing        = Art Seid |
  | distributor    = Columbia Pictures |
  | released       =   |
  | runtime        = 16 26"|
  | country        = United States |
  | language       = English |
}}

How High is Up? is the 48th short subject starring American slapstick comedy team The Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959. 

==Plot==
The Stooges are menders who drum up business at a construction site by poking holes on the bottom of the workers lunch boxes, then offering to repair the holes. When their ruse is discovered, they are chased onto the site and blend in with a crowd of men seeking employment. Curly states that they are "the best riveters that ever riveted," and the hiring workman (Edmund Cobb) sends them to work on the 97th floor, despite Curlys debilitating fear of heights.  In order to calm his fears, he wears a parachute.

While riveting, Larry also heats sausage for Moe and Curly. The foreman discovers Larry, who proceeds to toss Curly an actual rivet, who claims, "Its a weenie, but its kind of tough." Curly later uses a hard hat with a screwhead to engage the rivets while Moe drills them. The Stooges do a lousy job riveting and part of the building collapses when head foreman Mr. Blake (Vernon Dent) leans against a beam.  

Just before he leans against the beam,  he says that it looked like "good solid construction".  After it collapses he demands to see the Stooges.  The manager points to the section where the trio had been working.  The Foreman and manager see the convoluted mess of beams that the Stooges managed to assemble and keep standing.  The Forman and several men chase the stooges, who escape by parachuting off the building and landing in their wagon below. 

==Production notes==
Filming for How High is Up? was completed May 7-11, 1940.    The aerial shots of the scene, straight down from the building the Stooges are working on, are from the then newly built Empire State Building in New York City.   

The sweater removal scene is one of the best examples of the Stooges tendencies to use unorthodox methods to get the simplest job done. Since Moe and Larry cannot pull the sweater off of Curly, they figure the only way to do so is through the use of tools, such as mallets, chisels, and eventually a pair of scissors.  Larry can be seen breaking character and laughing, particularly when Curly yells, "Dont mind me, dont mind me!!" 

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 