Happy Flight
 
{{Infobox film
| name           = Happy Flight
| image          = HappyFlgiht Poster.jpg
| caption        = Film poster
| writer         = Shinobu Yaguchi, Junko Yaguchi
| starring       = 
| director       = Shinobu Yaguchi
| producer       = Shintaro Horikawa, Daisuke Sekiguchi
| distributor    = Toho
| released       =  
| runtime        = 102 min
| country        = Japan Japanese
| budget         = 
}}  is a Japanese comedy film directed by  . Friday November 14, 2008. Retrieved on February 19, 2010. 

==Story==
The film Happy Flight is about Kazuhiro Suzuki, a copilot who is trying to qualify as a pilot, and Etsuko Saitō, a young flight attendant going on her first international flight, who service an All Nippon Airways 747-400 as Flight 1980 to Honolulu, Hawaii, United States. Suzuki feels stressed when Captain Noriyoshi Harada becomes his evaluator, while Saitō becomes stressed when she learns she is working under Chief Purser Reiko Yamazaki. The story also tells the story of Natsumi Kimura, a ground staffer who deals with issues regarding the flight.

The plot revolves around the logistics of preparing Flight 1980 for a timely departure. The aircraft serving has been reported with malfunctioning heated pitot tubes, but the captain decides to postpone the repair to avoid delays, relying on redundant instruments available. Bird patrol has been dispatched to fend off pigeons. Immediately after takeoff, an instrument alarm prompts the captain in command to switch to the backup pitot tube as their primary indication. The captain mistakenly transmits the cabin announcement to the air traffic channel.

Inflight services commence shortly after the seat belt sign has been turned off. Due to the overbooked flight, not all passengers could receive their first choice of lunch between beef and fish; the cabin purser gives her stewardess a lesson in dealing with demand imbalance: promoting the less wanted fish. The stewardess follows through with her first mishap, describing the beef as "plain and ordinary." She then fumbles orders for white wine, apple juice, and motion sickness drugs. She corrects the drink orders; but the ill passenger vomits onto her uniform.
 indication of air speed until they descend to below 22,000 feet, where air temperature is above freezing point.
 Haneda airport, now suffering from severe weather conditions. The flight crew and ground controllers then have to work together to take the plane back home, win the cooperation of a "difficult" passenger, fly around adverse weather, and determine if the maintenance crew was at fault for the aircrafts failure.

==Cast==
* Haruka Ayase ( )
* Tomoko Tabata ( )
* Seiichi Tanabe ( )
* Shinobu Terajima ( )
* Saburo Tokito ( )
* Kazue Fukiishi
* Ittoku Kishibe

==Reception==
Mark Schilling of The Japan Times reviewed the film, giving it three of five stars. Schilling said that he "felt somewhat like a convict watching a prison film whose heroes are the trustees and guards — and feeling the filmmakers arent getting the whole story." 

==References==
 

==External links==
*    
*  

 

 
 
 
 
 
 