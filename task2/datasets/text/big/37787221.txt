Lilting (film)
{{Infobox film
| name           = Lilting
| image          = Lilting UK poster in portrait mode.jpg
| alt            =
| caption        = Theatrical release poster
| director       = Hong Khaou
| producer       = Dominic Buchanan
| writer         = Hong Khaou
| starring       = Ben Whishaw Cheng Pei-pei Andrew Leung Morven Christie Naomi Christie Peter Bowles
| music          = Stuart Earl
| cinematography = Urszula Pontikos
| editing        = Mark Towns Film London Microwave BBC Films Skillset
| distributor    = Artificial Eye   Strand Releasing
| released       =  
| runtime        = 91 minutes
| country        = United Kingdom
| language       = English
| budget         = £120,000
| gross          = $197,588 
}}
Lilting is a 2014 British drama film written and directed by Cambodian-born British director Hong Khaou, whose short film, Spring, was selected for Sundance and Berlinale film festival 2011.  It is produced by Dominic Buchanan, whose debut film Gimme The Loot had its World Premiere at SXSW in March 2012 and went on to win the Grand Jury Prize for Best Narrative Feature. 

The film had its world premiere on 16 January 2014, on Day One of the Sundance Film Festival, at which it competed in the World Cinema Dramatic Competition.  It won the Cinematography Award: World Cinema Dramatic at the festival.   The film had a theatrical release in UK on 8 August 2014   and released on September 26, 2014 in United States. 

== Plot ==
Lilting tells the story of a mother’s attempt at understanding who her son is after his untimely death. Her world is suddenly disrupted by the presence of his lover. Together, they attempt to overcome their grief whilst struggling against not having a shared language. 

== Cast ==
* Ben Whishaw as Richard
* Andrew Leung as Kai
* Cheng Pei-pei as Junn
* Morven Christie as Margaret
* Naomi Christie as Vann
* Peter Bowles as Alan

== Production ==
The script, original titled Lilting the Past, won third spot in the 2011 Brit List, a list of the best unproduced British screenplays. 

The film was one of three films greenlit by Microwave in early 2012.   A casting call was later released for the three of the lead roles,  later filled by Cheng Pei-pei and Andrew Leung.

Filming began in November 2012   and completed principal photography in December 2012.   Director Khaou has said the film will be visually inspired by Wong Kar-wais In the Mood for Love. 

During production, as part of the Microwave scheme, Michael Winterbottom mentored writer/director Khaou, while producer Buchanan was mentored by Ken Marshall, producer of London to Brighton, Filth (film)|Filth and Song for Marion. As with all Microwave films, the budget was £120,000.  It is the first bi-lingual film to be made under the Microwave scheme. 

== Reception ==
 .]]

=== Critical response ===
Lilting was met with positive reviews from critics. Review aggregator   the film has 61 rating from 23 reviews.

Justin Chang, in his review for Variety (magazine)|Variety, said that the film "Hong Khaou makes a fine debut with this quietly resonant cross-cultural chamber piece."  David Rooney of The Hollywood Reporter praised the film, saying, "Delicate and unhurried almost to a fault, though also hauntingly sexy and even humorous at times."  Amber Wilkinson from The Daily Telegraph|Telegraph gave the film three out of five stars and praised the lead actor that "Whishaw is magnetic as a man pushed to the edge of fragility by mourning, but who still suggests a quiet strength."  Dominic Mill of We Got This Covered gave a positive review and said, "The subject matter is powerful, and the performances are wonderful – in a world of big and showy dramatism, Lilting gets its point across without feeling the need to shout about it." 

=== Accolades ===
 
 
{| class="wikitable sortable"
|-
! Year
! Award
! Category
! Recipient
! Result
|-
| rowspan="2"| 2014 Sundance Film Festival
| World Cinema Grand Jury Prize: Dramatic
| Hong Khaou
|  
|-
| Cinematography Award: World Cinema Dramatic
| Urszula Pontikos
|  
|}
 

== References ==
 

== External links ==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 