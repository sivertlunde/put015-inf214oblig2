Zaza (1915 film)
{{infobox film
| name           = Zaza
| image          = Zaza-newspaperadvert-1915.jpg
| imagesize      = 
| caption        = Contemporary newspaper advertisement. Hugh Ford
| producer       = Adolph Zukor Charles Frohman
| writer         = David Belasco
| based on       =  
| starring       = Pauline Frederick
| music          =
| cinematography =
| editing        =
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 mins.
| country        = United States Silent English intertitles
}} silent romantic Hugh Ford of the same name that starred Mrs. Leslie Carter. 
 Famous Players studio on September 11, 1915 caused the films release to be delayed until November 11.  The film is now considered Lost film|lost.  

==Cast==
* Pauline Frederick - Zaza
* Julian LEstrange - Bernard Dufrene
* Ruth Sinclair - Madame Dufrene
* Maude Granger - Aunt Rosa
* Blanche Fisher - Louise
* Helen Sinnott - Nathalie
* Mark Smith - Cascart
* Charles Butler - Duc de Brissac
* Walter Craven - Dubois
* Madge Evans - Child (uncredited)

==Other adaptations== in 1923 in 1939, starring Claudette Colbert. 

==See also==
*List of lost films

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 


 
 