Aatadista
 
{{Infobox film
| name           = Aatadista
| image          = Aatista.JPG
| caption        =
| director       = Ravikumar Chowdary
| producer       =  C Kalyan and S Vijayanand
| writer         = Ravikumar Chowdary Nitin Kajal Jayalalitha Naramalli Sivaprasad
| music          = Chakri
| cinematography = Jawahar Reddy
| editing        =
| distributor    =
| released       =  
| runtime        =
| country        = India
| language       = Telugu
| budget         =
| gross          =
}}
 Telugu film, starring Nitin Kumar Reddy|Nitin, Kajal Aggarwal, Jayasudha, Nagababu and others. Directed by A S Ravi Kumar, it was produced by C Kalyan and S Vijayanand, and scored by Chakri (music director)|Chakri.  The film was released on 20 March 2008 to mixed reviews. RKD Studios have bought the rights of the film in Hindi.

==Story==
Jagan alias Chinna is industrialist Lion Rajendras jobless son. He falls in love with Sunanda. Meanwhile he suggests that his father and his rival Raghunath get into a partnership to double up their efficiency. Without his knowledge his marriage is fixed to Raghunaths daughter, who obviously turns out to be Sunanda, but the families are not on great terms even after they fix the match. Raghunath is dealing in tobacco businesses that is actually owned by Bonala Shankar, a brusque and notorious MLA. Rajendra doesnt want to deal in that area, but Bonala threatens the board of directors too. Now Jagan has to deal with this corrupt politico and plays a game with him that shakes him from his very roots-spreads a rumor that he hit Shankar.

==Cast==
*Nitin
*Kajal Aggarwal
*Nagababu
*Jayasudha Jayalalitha
*Raghuvaran
*Narsing Yadav
*Raviprakash
*Chalapathi Rao
*Narra Venkateswara Rao Sivaprasad
*Amit Venu Madhav
*Mumaith Khan

==Music==

===Songs===
* Style Style
* Pappeede Chummade
* Vachindiro Silaka
* Mila Mila
* Regipoy e
* Sorry Sorry

== References ==
 

 
 
 


 