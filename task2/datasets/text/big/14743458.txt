Transcendent Man
 
{{Infobox film
| name           = Transcendent Man
| image          = Transcendent man poster.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Barry Ptolemy
| producer       = Barry Ptolemy Felicia Ptolemy
| starring       = Ray Kurzweil
| editing        = Meg Decker Doobie White
| music          = Philip Glass
| cinematography = Shawn Dufraine
| studio         = Ptolemaic Productions Therapy Studios
| country        = United States
| released       =  
| runtime        = 83 minutes
| distributor    = Docurama
| language       = English
}}
Transcendent Man is a 2009 documentary film by American filmmaker Barry Ptolemy about inventor, futurist and author Ray Kurzweil and his predictions about the future of technology in his 2005 book, The Singularity is Near. In the film, Ptolemy follows Kurzweil around his world as he discusses his thoughts on the technological singularity, a proposed advancement that will occur sometime in the 21st century when progress in artificial intelligence, genetics, nanotechnology, and robotics will result in the creation of a human-machine civilization.

William Morris Endeavor distributed the film partnership with Ptolemaic Productions and Therapy Studios, using an original model involving a nationwide screening tour of the film (featuring Q&A sessions with Ptolemy and Kurzweil), as well as separate digital and DVD releases. The film was also released on iTunes and On-Demand on March 1, 2011, and on DVD on May 24, 2011. Stewart, A. (January 11, 2011).  . Variety. Retrieved September 10, 2011. 

==Synopsis==
Raymond Kurzweil, noted inventor and futurist, is a man who refuses to accept the inevitability of physical death. He proposes that the  " and provide a human-computer interface within the brain; robotics, or artificial intelligence, will make superhuman intelligence possible, including the ability to back up the mind.

Most of the movie has an implication of a religious background, and is applying technology to accomplish the goals with what is considered to be "god like" powers, through interdependent connection. Kurzweil has been criticized as being a modern day prophet, however the film describes a detailed list of his inventions. Rays dedication to improving the blinds quality of life is displayed in the climax of the film, with his miniature blind reading tool. Ray speaks of emailing someone a blouse, or printing out a toaster utilizing nanotechnology. Eventually swarms of our nanotechnology will be sent by us into the universe to, as Kurzweil puts it, "wake up the universe". 
  human and machine evolution, concerns about Kurzweils predictions are raised by technology experts, philosophers, and commentators. Physician William B. Hurlbut warns of tragedy and views Kurzweils claims as lacking a more moderate approach necessitated by biological science. AI engineer Ben Goertzel champions the transhumanist vision, but acknowledges the possibility of a dystopian outcome. AI researcher Hugo de Garis warns of a coming "Artilect War", where god-like artificial intellects and those who want to build them, will fight against those who dont. Kevin Warwick, professor of Cybernetics at University of Reading, advocates the benefits of the singularity, but suggests the Terminator scenario could also occur, where humans become subservient to machine and live on a farm, and the singularity is the point where humans lose control to the intelligent machines. Warwick basically spells doom for anyone who is human after the singularity. Dean Kamen observes that advances in technology have finally made immortality a reasonable goal. At the end of the film, Kurzweil states, "if I was asked if god exists, I would say not yet."

==Cast==
* Tom Abate, Technology Reporter, San Francisco Chronicle.
* Hugo De Garis, Professor of Computer Science and Mathematical Physics, Xiamen University.
* Peter Diamandis, Chairman, X Prize Foundation.
* Neil Gershenfeld, Director, Center for Bits and Atoms, Massachusetts Institute of Technology|MIT.
* Ben Goertzel, Artificial Intelligence Engineer. William Hurlbut, Consulting Professor in the Neuroscience Institute at Stanford University. Kevin Kelly, Co-founder, Wired (magazine)|Wired.
* Aaron Kleiner, Kurzweil Technologies
* Hannah Kurzweil, mother of Ray Kurzweil
* Ray Kurzweil
* Sonya R. Kurzweil, wife of Ray Kurzweil
* Robert Metcalfe, co-inventor of Ethernet, founder of 3Com
* Chuck Missler, Technologist/Koinonia Institute
* Colin Powell, retired Four-star rank|four-star General in the United States Army.
* Steve Rabinowitz, college friend from MIT.
* Philip Rosedale, creator of Second Life
* William Shatner
* Kevin Warwick, Professor of Cybernetics, University of Reading.
* Stevie Wonder

==Music==
American composer  Philip Glass scored the original soundtrack  for the film. In addition to the Transcendent Man score, other music from Glasss collection was included in the soundtrack.

*"A Brief History of Time"
*"Koyaanisqatsi"
*"Kyokos House" (from Mishima)
*"Religion" (from Naqoyqatsi)
*""Satyagraha Act III" (Conclusion)
*"Symphony No. 3"
*"The Thin Blue Line"
*"Tirol Concerto for Piano and Orchestra"

==Release==
The Transcendent Man tour visited five major cities in the U.S., as well as London. These screenings featured question and answer sessions with director Barry Ptolemy and Ray Kurzweil following the film, as well as V.I.P. receptions.  

Ptolemaic Productions and Therapy Studios have pursued an alternative distribution strategy for Transcendent Man, going through the Global and Music departments of agency William Morris Endeavor to partner with iTunes and Media-on-Demand for a March 1, 2011 digital release and with New Media for a May 24, 2011 DVD release.  Marketing made use of social media and emerging technologies like QR codes to appeal to a tech-savvy audience. 

 

==Reception==
 Charlie Rose. Additionally, Kurzweil went on to discuss the film on The Colbert Report, Jimmy Kimmel Live!, and Real Time with Bill Maher.

==Film festivals==
*April 28, 2009 - Tribeca Film Festival, World Documentary Feature Competition. 
*November 5, 2009 - American Film Institute film festival, Los Angeles. 
*November 24, 2009 - International Documentary Film Festival Amsterdam (IDFA), Amsterdam, Netherlands, screened in competition. 
*March 2010 -  . 

==Criticism==
One common criticism of Kurzweils final prediction is that he does not consider that new technologies are never universally and immediately adopted due to the laws of economics. Start-up costs and economies of scale mean that initially transhumanist technology would be prohibitively expensive for most people. This would cause the wealthy, first adopters of brain enhancing technology to be transcendental above the less fortunate. One response to this criticism uses the technology of the automobile as an example. Even though a rich person  might drive an expensive List of Rolls-Royce motor cars|Rolls-Royce, cheaper alternatives are available that perform the same task. In other words, no matter how much two cars differ in price, their function is virtually identical. One important element of Kurzweils singularity is that the cost will come down to virtually nothing. 

Kurzweil readily defends AI as being controllable against malicious behavior, which he accepts is a definite threat. He never, on the other hand, confronts the obvious dangers of AI fusing with the first humans.  

==References==
 

==Further reading==
*Barker, A. (November 23, 2009). Transcendent Man. Variety. 417 (2), 34.
*Gefter, A. (May 8, 2009).  . New Scientist. 202 (2707), 27.  
*Shermer, M. (April 1, 2011). The Immortalist. Science. 332 (6025), 40.  
*Tucker, P. (2009). The Cinematic Singularitarian. The Futurist. 43 (5), 60.

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 