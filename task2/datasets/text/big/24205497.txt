Donald Duck and the Gorilla
 
{{Infobox Hollywood cartoon|
|cartoon_name=Donald Duck and the Gorilla
|series=Donald Duck Jack King
|story_artist=
|animator=
|layout_artist=
|background_artist=
|voice_actor=Clarence Nash
|musician=
|producer=Walt Disney Walt Disney Productions RKO Radio Pictures
|release_date=March 31, 1944
|color_process=Technicolor
|runtime=6:57 English
|preceded_by=Trombone Trouble
|followed_by=Contrary Condor
}}

Donald Duck and the Gorilla is a Donald Duck Cartoon originally released in 1944. It stars Donald Duck along with his three nephews; Huey, Dewey and Louie and Ajax (Disney)|Ajax, the Gorilla. The 1930 Mickey cartoon The Gorilla Mystery has a similar plot involving a gorilla named Beppo who kidnaps Minnie Mouse.

==Plot==
One stormy night, Donald and his nephews overhear the radio announcer, Breckenridge, state a terrible killer gorilla named Ajax (Disney)| Ajax has escaped from the local zoo. As a joke, Donald scares his nephews with fur gloves to make it seem he is Ajax. To get revenge on their uncle, the nephews dress up in a gorilla suit and frighten Donald, just before Ajax breaks into the house. After the gorilla chases Donald through his old house, with the help of the radio announcer, the nephews use tear gas to stop Ajax, also making Donald cry.

==Releases==
VHS

*Cartoon Classics : First Series : Volume 3 : Scary Tales
*Disneys Halloween Treat
*Cartoon Classics : Second Series : Volume 13 : Donalds Scary Tales

Laserdisc

*Cartoon Classics : Scary Tales
*Donalds Scary Tales
*Halloween Haunts

DVD

*Walt Disney Treasures : The Chronological Donald Volume 2
*Mickeys House of Villains

Television

*The Ink and Paint Club : #34 : Donalds Nephews

==Technical specifications==
*Animation type : Standard
*Sound mix : Mono
*Aspect ratio : 1.37 : 1
*Negative format : 35mm
*Print format : 35mm
*Cinematographic process : Spherical

==Notes==
 

==External links==
* 
*  at  
 
 
 
 
 
 
 
 

 