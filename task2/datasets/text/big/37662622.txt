Bade Dilwala
{{Infobox film
| name           = Bade Dilwala
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Shakeel Noorani	
| producer       = Shakeel Noorani	
| writer         = Shakeel Noorani	
| screenplay     = 
| story          = 
| based on       =  
| starring       = Sunil Shetty Priya Gill
| music          = Aadesh Shrivastava
| narrator       = Sachin Khedekar
| cinematography = Dinesh Telkar
| editing        = Sudhir Verma	
| studio         = 
| distributor    = 
| released       =  
| runtime        = 152 mins
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}}
 Indian Bollywood film produced and directed by Shakeel Noorani. It stars Sunil Shetty and Priya Gill in pivotal roles. 
 
There is another movie named Bade dilawala released in 1982, which has a song sung by Kishore Kumar "Jeevan Ke Din Chhote Sahi".
 It Could Happen to You which was released in 1994, starring Nicolas Cage and Bridget Fonda.
Later it was also remade in Telugu as Bahumati in 2007.

==Cast==
* Sunil Shetty...Police Inspector Ram Prasad
* Priya Gill...Piya Verma
* Paresh Rawal...Mannubhai Rajnikant Shroff
* Archana Puran Singh...Manthara, Rams wife
* Satish Kaushik...	Police Inspector Iqbal Shaikh
* Ranjeet...Prosecuting Attorney
* Raju Srivastava...Actor
* Rashid Khan...Sanju Baba
* Guddi Maruti...Zarine
* Raju Kher...Motilal Bihari

==Plot==

A police inspector officer Ram (Sunil Shetty), a man of his word. His wife Manthara (Archana Puran Singh) is obsessed with money and neglects her family in pursuit of getting rich quick. She forces Ram to buy a lottery ticket and he obliges. When he goes into a restaurant for a snack, he finds that he has no money to tip the waitress Piya (Priya Gill). He promises her half the money if he ever wins the lottery. And to his surprise, he does!! But Manthara is not about to give up half of this windfall so easily....and as Ram and Piya come closer to each other, they discover a growing attraction.

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Apne Mehboob Ki"
| Udit Narayan, Alka Yagnik
|-
| 2
| "Baant Raha Tha"
| Udit Narayan, Alka Yagnik, Shankar Mahadevan
|-
| 3
| "Bhadke Aag Judaai Ki (Female)"
| Jaspinder Narula
|-
| 4
| "Bhadke Aag Judaai Ki (Male)"
| Shankar Mahadevan
|-
| 5
| "Jawan Jawan Hai"
| Hariharan (singer)|Hariharan, Alka Yagnik
|-
| 6
| "Mujhe Aisa Ladka"
| Alka Yagnik
|-
| 7
| "Tu Tu Ru Tu Ru"
| Udit Narayan, Sunidhi Chauhan
|}

==References==
 

==External links==
* 

 
 
 

 