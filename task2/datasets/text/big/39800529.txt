The Mystery of Bangalore
{{Infobox film
| name           = The Mystery of Bangalore
| image          = 
| image_size     = 
| caption        = 
| director       = Alexander Antalffy   Paul Leni 
| producer       =  Rudolf Kurz   Paul Leni 
| narrator       = 
| starring       = Conrad Veidt   Gilda Langer   Harry Liedtke 
| music          = 
| editing        =
| cinematography = 
| studio         = Pax Film 
| distributor    = 
| released       = January 1918
| runtime        = 
| country        = Germany
| language       = Silent   German intertitles
| budget         =  
| gross          =
| preceded_by    = 
|  German silent film directed by Alexander Antalffy and Paul Leni and starring Conrad Veidt, Gilda Langer and Harry Liedtke. 

==Cast==
* Conrad Veidt as Dinja 
* Gilda Langer as Elles, die Tochter des Gouverneurs 
* Harry Liedtke as Archie Douglas 

==References==
 

==Bibliography==
* Isenberg, Noah William. Weimar Cinema: An Essential Guide to Classic Films of the Era. Columbia University Press, 2009.

==External links==
* 

 

 
 
 
 
 
 
 

 