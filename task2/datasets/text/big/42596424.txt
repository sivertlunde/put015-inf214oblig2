Green Ice
{{Infobox film
| name           = Green Ice
| image          = Green_Ice_(film).jpg
| image_size     =
| caption        = French poster
| director       = Ernest Day 
| producer       =
| writer         = Edward Anhalt Ray Hassett
| starring       = Ryan ONeal Omar Sharif Anne Archer
| music          = Bill Wyman
| cinematography = Gilbert Taylor
| editing        = John Jympson studio = ITC Entertainment
| distributor    =
| released       =1981
| runtime        =
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
Green Ice is a 1981 adventure film starring Ryan ONeal. It was also released under the name Operation Green Ice.

==Plot==
A down on his luck engineer gets involved in an adventure with a mysterious woman and an emerald magnate.

==Cast==
* Ryan ONeal as Joseph Wiley
* Anne Archer as Holbrook
* Omar Sharif as Meno Argenti
* John Larroquette as Claude
* Michael Sheard as Jaap

==Production==
The original director left the project during filming in Mexico and was replaced by Ernest Day. The film was financed by Lew Grade who called it "quite a nice little film, but in the end, too much like a TV movie." Lew Grade, Still Dancing: My Story, William Collins & Sons 1987 p 251 

==Music==
  Green Ice. The soundtrack contains 18 original songs.

==References==
 

==External links==
*  at IMDB
*  at TCMDB

 
 
 
 


 