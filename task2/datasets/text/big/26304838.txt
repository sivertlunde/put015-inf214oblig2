Mockery
{{Infobox film
| name           = Mockery
| caption        =
| image          = Mockery FilmPoster.jpeg
| director       = Benjamin Christensen
| producer       = Erich Pommer
| writer         = Stig Esbern (story) Joseph Farnham (titles) Bradley King (continuity) Lon Chaney Barbara Bedford Ricardo Cortez Emily Fitzroy
| music          =
| cinematography = Merritt B. Gerstad John W. English
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 75 minutes
| country        = United States
| language       = Silent (English intertitles)
| budget         =
| gross          =
}} American film Russian Revolution Lon Chaney. Danish director Barbara Bedford) who is threatened by the encroaching insurgency. 

==Release==
Mockery received mixed reviews when it was first released,  and is regarded as one of Chaneys weaker films of his MGM period (1924-1930).  The film was thought to be lost until the mid-1970s. 

==Cast==
*Lon Chaney - Sergei Barbara Bedford - Countess Tatiana Alexandrova 
*Ricardo Cortez - Capt. Dimitri 
*Mack Swain - Vladimir Gaidaroff 
*Emily Fitzroy - Mrs. Gaidaroff 
*Charles Puffy - Ivan, the Gatekeeper 
*Kai Schmidt - Butler 
*Johnny Mack Brown - Russian Officer 
*Albert Conti - Military Commandant at Novokursk (uncredited) 
*Jules Cowles - Peasant who robs Tatiana (uncredited) 
*Frank Leigh - Outlaw Peasant in Cabin (uncredited) 
*Russ Powell - Man taking Sergei to Ivan (uncredited) 
*Buddy Rae - Russian Soldier (uncredited) 
*Michael Visaroff - Cossack whipping Sergei (uncredited)

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 


 