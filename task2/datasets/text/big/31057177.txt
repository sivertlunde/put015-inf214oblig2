Rizal sa Dapitan
 
 
Rizal sa Dapitan (lit. Rizal in Dapitan), is a 1997 film adaptation of life of the Filipino national hero Dr. José Rizal, starring Albert Martinez as José Rizal and Amanda Page as Josephine Bracken, the screenplay is by Pete Lacaba.

==Plot==
José Rizal (Martinez) was exiled in Dapitan in 1892, and he began adapting to his new home. He helped the local residents by offering free education to all children, befriending his student Jose Asiniero (Hernando), and rendering his services as a doctor, including treating his mother, Doña Teodora Alonzo (Carpio), who visited him with his sisters Maria (Pangilinan) and
Narcisa (Dumpit).

He met his fiancée Josephine Bracken (Page) who brought her blinded stepfather George Taufer (Holmes) but later on she left him for her beloved Rizal. They decide to marry, but are refused a Church wedding on political grounds. The couple settles for a common-law marriage despite initial opposition from Rizals family, and have a stillborn son Rizal names Francisco. The film closes with Rizal leaving Dapitan as the locals mourn him. An epilogue explains Rizals intent to work in Cuba and subsequent arrest, his execution and its birthing the Philippine Revolution.

==Cast==
*Albert Martinez - Dr. José Rizal
*Amanda Page - Josephine Bracken
*Roy Alvarez - Captain Ricardo Carnicero
*Jaime Fabregas - Fr. Franciso de Paula Sanchez
*Candy Pangilinan - Maria Rizal
*Tess Dumpit - Narcisa Rizal
*Rustica Carpio - Teodora Alonzo
*Noni Buencamino - Pío Valenzuela
*Carelle Manuela - Manuela Orlac
*Soliman Cruz - Pablo Mercado
*Junell Hernando - Jose "Josielito" D. Aseniero - student of Rizal later governor of Zamboanga del Norte.
*Cris Michelena - Father Obach (as Chris Michelena)
*Paul Holmes - George Tauffer

==References==
*Israel, Lorna (2011) "A Body in Permanent Transit. José Rizals Exile as Spatial Performance", in: manycinemas 2, 54-66.  ,  

 
 
 


 