Katanga Business
{{Infobox film
| name           = Katanga Business
| image          = Affiche katanga business.jpg
| caption        = 
| director       = Thierry Michel
| writer         = Thierry Michel
| narrator       = Jacques Dubuisson 
| starring       = 
| music          = Marc Hérouet
| cinematography = Michel Téchy
| editing        = Marie Quinton 
| studio         = Les Films de la Passerelle Les Films dIci
| distributor    = 
| released       =  
| runtime        = 120 minutes
| country        = Belgium
| language       = French English Dutch
| budget         = 
| gross          = 
}}
Katanga Business is a 2009 film by Belgian director Thierry Michel that explores the mining industry in Katanga Province, Democratic Republic of the Congo.

== Plot ==
The province of Katanga in the south east of the Democratic Republic of the Congo (DRC) has huge mineral riches including uranium, zinc, copper and cobalt.
These were exploited in colonial times by the Union Minière du Haut Katanga, which was nationalized in 1966 by President Mobutu Sese Seko.  Mobutu lavished wealth on his clan, but the mines were soon abandoned, forcing the miners into illegal artisanal workings.
More recently new investors have bought into the industry including Indians and Chinese. 
 George Forrest, Indian and Chinese investors and state officials.  It questions who benefits from the mining operations.  Katanga Business highlights the charismatic governor of the province, the populist Moses Katumbi. 
It shows the great wealth of the province and the abject poverty of the miners. 

== Technical ==
Thierry Michel had produced works on the DRC over a period of seventeen years before making Katanga Business.  Earlier works were Zaire, le cycle du serpent (1992), Les Derniers Colons (1995), Mobutu roi du Zaïre (1999) and Congo River, Beyond Darkness (2005).   Taken together his films provide a unique overview of the social, economic and political life of the country. He made the television documentary film Katanga, la guerre du cuivre (Katanga, the copper war) earlier in 2009, the basis for the film.  A book was published with the same title including text and photographs by Thierry Michel. 

Michels Congolese assistant director Guy Kabeya Muya received death threats for having worked on the film.
The filmmaker Monique Mbeka Phoba quickly mobilized an international campaign to ensure that the Congolese authorities extended their protection. 

== Reception ==
A reviewer in Le Monde said of the film that Thierry Michel is not trying to make an indictment, that he approaches the subject with "eyes wide open", and that the film is "bursting with life, in which one will find, as easily as copper in Kolwezi, reason for hope".  Another reviewer criticized the decision to let the images and people in the film tell the story, as in a drama, rather than providing explanations of causes and effects. As a result, the film therefore is mostly a simple depiction of the impact of globalization and neo-colonialism.  The film was nominated for a Magritte Award in the category of Best Documentary in 2011. 

== References ==
  
;Sources
 
*{{cite book |ref=harv |url=http://books.google.ca/books?id=akIpucUB97oC&pg=PA269
 |title=Télévision française, la saison 2011: une analyse des programmes du 1er septembre 2009 au 31 août 2010
 |first=Christian |last=Bosséno
 |publisher=Editions LHarmattan |year=2011 |ISBN=2296546781}}
*{{cite book |ref=harv |url=http://books.google.ca/books?id=thfrCtR2w3kC&pg=PA197 title	=Congo RDC: puissance et fragilité
 |first=Pierre |last=Cappelaere
 |publisher=Editions LHarmattan |year=2011 |ISBN=2296541623}}
*  |url=http://www.excessif.com/cinema/critique-katanga-business-4708805-760.html
 |title=Katanga Business
 |work=Excessif |accessdate=2012-04-05}}
* 
 |url=http://www.africultures.com/php/index.php?nav=personne&no=10949
 |work=Africultures
 |title=Guy Kabeya Muya
 |accessdate=2012-04-03}}
*{{cite book |ref=harv
 |title=Katanga business: un livre
 |first1=Thierry |last1=Michel |first2=Elikia |last2=MBokolo
 |publisher=Luc Pire |year=2009 |ISBN=2507002751}}
* 
 |url=http://ks29982.kimsufi.com/passerelle/pres-equipe.php?m=4
 |publisher=Les Films de la Passerelle SPRL
 |title=THIERRY MICHEL
 |accessdate=2012-04-05}}
*{{cite web |ref=harv
 |url=http://www.lemonde.fr/cinema/article/2009/04/14/katanga-business-du-coeur-des-tenebres-au-coeur-de-la-mondialisation_1180487_3476.html
 |title="Katanga Business" : du coeur des ténèbres au coeur de la mondialisation
 |work=Le Monde |date=2009-04-14 |first=Thomas |last=Sotinel
 |accessdate=2012-04-05}}
 

== External links ==
*  
*  

 
 
 
 
 