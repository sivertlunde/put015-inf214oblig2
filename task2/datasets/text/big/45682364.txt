A Bachelor Husband
{{Infobox film
| name           = A Bachelor Husband
| image          =
| caption        =
| director       = Kenelm Foss
| producer       = H.W. Thompson   Frank E. Spring
| writer         = Ruby M. Ayres   Kenelm Foss 
| starring       = Lyn Harding   Renee Mayer   Hayford Hobbs 
| cinematography = 
| editing        = 
| studio         = Astra Films 
| distributor    = Astra Films
| released       = October 1920
| runtime        = 
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         =
| preceded_by    =
| followed_by    =
}} silent romance film directed by Kenelm Foss and starring  Lyn Harding, Renee Mayer and Hayford Hobbs. It was based on a story by Ruby M. Ayres, originally published in the Daily Mirror.  

==Cast==
*   Lyn Harding as Feather Dakers  
* Renee Mayer as Marie Celeste  
* Hayford Hobbs as Chris Lawless  
* Irene Rooke as Aunt Madge 
* Lionelle Howard as Atkins   Gordon Craig as Chris, as a child  
* Margot Drake as Mrs. Chester  
* Will Corrie as George Chester  
* R. Heaton Grey as Aston Knight  
* Phyllis Joyce as Mrs. Heriot

==References==
 

==Bibliography==
* Low, Rachael. History of the British Film, 1918-1929. George Allen & Unwin, 1971.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 

 