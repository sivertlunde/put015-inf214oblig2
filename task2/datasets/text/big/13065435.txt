The Gown Shop
{{Infobox film
| name           = The Gown Shop
| image          = 
| image_size     = 
| caption        = 
| director       = Larry Semon
| producer       = Larry Semon
| writer         = Larry Semon
| starring       = Oliver Hardy
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = August 14, 1923
| runtime        =  2 reels; 600m
| country        = United States
| language       = Silent with English intertitles
| budget         = 
}}
 silent comedy film featuring Oliver Hardy.

==Cast==
* Larry Semon - Larry, a salesman
* Kathleen Myers - Head saleslady
* Oliver Hardy - Store manager (as Babe Hardy)
* Fred DeSilva - Dressmaker (as F.F. DeSylva)
* Pete Gordon - Worker
* William Hauber Frank Hayes - Wife in audience
* James Donnelly - Husband Spencer Bell - Janitor
* Harry DeRoy - Audience member
* Dorothea Wolbert - Audience member
* Otto Lederer - Audience member

==See also==
* List of American films of 1923
* Oliver Hardy filmography

==External links==
* 

 
 
 
 
 
 
 
 
 
 