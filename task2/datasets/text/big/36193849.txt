Death Carries a Cane
{{Infobox film
| name           = Death Carries a Cane
| image          = Passi di danza su una lama di rasoio poster.jpg
| alt            = 
| caption        = Italian theatrical release poster
| director       = Maurizio Pradeaux
| writer         = Alfonso Balcázar Arpad DeRiso George Martin Maurizio Pradeaux
| starring       = Robert Hoffmann 
| music          = Roberto Pregadio 
| cinematography = Jaime Deu Casas
| editing        = Eugenio Alabiso
| studio         = Balcázar Producciones Cinematográficas Società Europea Films Internazionali Cinematografica (SEFI)
| released       = 5 January 1973
| runtime        = 84 min.
| country        = Italy Spain
| language       = Italian ESP 11,919,640  (Spain) 
}}

Death Carries a Cane (Italian title: Passi di danza su una lama di rasoio; UK title: Maniac At Large) is a 1973 Italian giallo film directed by Maurizio Pradeaux. The Italian title translates as Dance Steps on a Razors Edge, but the film is listed in reference books as Death Carries a Cane.

== Cast ==
* Robert Hoffmann: Alberto Morosini 
* Nieves Navarro: Kitty (as Susan Scott)  George Martin: Inspector Merughi 
* Anuska Borova: Lidia Arrighi/ Silvia Arrighi 
* Simón Andreu: Marco 
* Salvatore Borgese: Asdrubale Magno 
* Luciano Rossi: Richard

== Critical reception == AllMovie wrote of the film, "a complex plot and stylish visuals dont necessarily combine to instantly produce a good giallo", but that "the film is good for a few chuckles and has a pair of memorable murder sequences." 

== References ==

 

== External links ==

*  

 
 
 
 


 