Feroz (film)
{{Infobox film
| name           = Feroz
| image          = 
| caption        = 
| director       = Manuel Gutiérrez Aragón
| producer       = Elías Querejeta
| writer         = Manuel Gutiérrez Aragón Elías Querejeta
| starring       = Fernando Fernán Gómez
| music          = 
| cinematography = Teodoro Escamilla
| editing        = Pablo González del Amo
| distributor    = 
| released       = May, 1984
| runtime        = 115 minutes
| country        = Spain
| language       = Spanish
| budget         = 
}}

Feroz is a 1984 Spanish fantasy film directed by Manuel Gutiérrez Aragón. It was screened in the Un Certain Regard section at the 1984 Cannes Film Festival.   

==Cast==
* Fernando Fernán Gómez - Luis
* Frédéric de Pasquale - Andrés (as Frederic de Pasquale) Javier García
* Elene Lizarralde - Ana
* Julio César Sanz - Pablo
* Pedro del Río
* José Antonio Gálvez - (as Antonio Gálvez)
* Matilde Grange
* Margarita Calahorra
* José Rodríguez (actor)|José Rodríguez
* María José Parra
* Mercedes Marfil
* Agustín Arranz
* Valeriano de la Llama
* Carlos Cano Francisco Menéndez
* Marta Suárez

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 