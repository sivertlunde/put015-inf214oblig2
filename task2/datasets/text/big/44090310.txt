Chathurvedam
{{Infobox film 
| name           = Chathurvedam
| image          =
| caption        =
| director       = J. Sasikumar
| producer       = SSR Kalaivaanan
| writer         = S. L. Puram Sadanandan
| screenplay     = S. L. Puram Sadanandan
| starring       = Prem Nazir Adoor Bhasi Thikkurissi Sukumaran Nair Sankaradi
| music          = G. Devarajan
| cinematography = CJ Mohan
| editing        = K Sankunni
| studio         = Manikantan Productions
| distributor    = Manikantan Productions
| released       =  
| country        = India Malayalam
}}
 1977 Cinema Indian Malayalam Malayalam film, directed by J. Sasikumar and produced by SSR Kalaivaanan. The film stars Prem Nazir, Adoor Bhasi, Thikkurissi Sukumaran Nair and Sankaradi in lead roles. The film had musical score by G. Devarajan.   

==Cast==
 
*Prem Nazir
*Adoor Bhasi
*Thikkurissi Sukumaran Nair
*Sankaradi
*Sreelatha Namboothiri
*Alummoodan
*Bahadoor Meena
*Padmapriya
*Paravoor Bharathan
*Philomina
 

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Sreekumaran Thampi. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Chaaru sumaraaji mukhi || K. J. Yesudas || Sreekumaran Thampi || 
|-
| 2 || Chiriyude Poonthoppil || K. J. Yesudas, P. Madhuri || Sreekumaran Thampi || 
|-
| 3 || Padaan Bhayamilla || K. J. Yesudas || Sreekumaran Thampi || 
|-
| 4 || Udayaasthamaya Pooja || K. J. Yesudas || Sreekumaran Thampi || 
|}

==References==
 

==External links==
*  

 
 
 

 