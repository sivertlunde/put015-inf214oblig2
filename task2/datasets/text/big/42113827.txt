Bellini and the Devil
{{Infobox film
| name           = Bellini and the Devil
| image          = Bellini and the Devil.jpg
| caption        = Cover of the DVD release
| director       = Marcelo Galvão
| producer       = Theodoro Fontes
| writer         = Marcelo Galvão
| based on       =  
| starring       = Fabio Assunção
| music          = Andreas Kisser Eduardo Queiroz Marina de La Riva
| editing        = Theodoro Fontes Eduardo Queiroz
| cinematography = Rodrigo Tavares
| studio         = Santa Fé 1900 Filmes
| distributor    = Santa Fé 1900 Filmes Imagem Filmes  
| released       =  
| runtime        = 90 minutes
| country        = Brazil
| language       = Portuguese
| budget         = R$3,552,991  
| gross          = R$14,122 
}}
Bellini and the Devil ( ) is 2008 Brazilian crime film directed by Marcelo Galvão. Based on Tony Bellottos homonymous novel, it stars Fabio Assunção as Remo Bellini, a São Paulo-based detective who investigates mysterious murders revolving around the The Book of the Law.  A sequel to Bellini and the Sphynx, it premiered as the opening film of the 1st Los Angeles Brazilian Film Festival,  where Assunção won the award of Best Actor. 

==Cast==
*Fábio Assunção as Remo Bellini
*Rosanne Mulholland as Gala
*Nill Marcondes as Zanqueta
*Mariana Clara as Rita
*Beto Coville as Mariano
*Luíza Curvo as Clarice
*Caroline Abras as Silvia
*Marco Luque as Alex
*Christiano Cochrane as Malta
*André Bubman as Odilon
*Neuza Romano as Odilons mother
*Javer Monteiro as bartender
*Malu Bierrenbach as Érica
*Marília Gabriela as Letícia
*Jatir Eiró as journalist
*Deto Montenegro as pauper

==Production==
In 2002, after the theatrical release of the Roberto Santuccis Bellini and the Sphynx, the production team cogitate to adapt the second book, Bellini e o Demônio.  Malu Mader wanted to direct the film but due to other projects, she did not it.  Instead, Galvão was actually hired for it; he cited Aleister Crowley as the main influence, as well as the filmmakers David Lynch and David Cronenberg, and quimbanda and black magic as influences for the work.  He also wrote the screenplay, but he felt he was betrayed on the films theatrical version as the producer Theodoro Fontes modified it.  Galvão, however, was satisfied to know that the version released on DVD would contain an extra with a directors cut version. 

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 


 