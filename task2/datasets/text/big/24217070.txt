Oi kyries tis avlis
{{Infobox film
| name           = Oi kyries tis avlis
| image          = 
| caption        = 
| director       = Dinos Dimopoulos
| producer       = 
| writer         = Dimitris Giannoukakis
| starring       = Dinos Iliopoulos Alekos Alexandrakis Nora Valsami Dionyssis Papayannopoulos Eleni Prokopiou Floreta Zana Costas Prekas Antigoni Koukouli Katerina Yioulaki Tasos Giannopoulos Babis Anthopoulos Mairi Lalopoulou Evita Iliopoulou
| cinematography = Nikos Dimopoulos
| choreographer   = Giannis Fleury 
| music          = Mimis Plessas 
| editing        = 
| distributor    = Finos Films
| released       =  
| runtime        = 104 minutes
| country        = Greece
| language       = Greek
| cine.gr_id     = 
}}
Oi kyries tis avlis ( ) is a 1966 Greek film based on the theatrical play To ekto patoma (Το έκτο πάτωμα = The Next Step).  King Constantine II came to congratulate the leading actor in his little chamber, after the premiere of the theater production.

The movie was filmed in 1966 by Finos Films and sold 338,081 tickets. The film was directed by Dinos Dimopoulos, a student of Iliopoulos at the Dramatic school. Dinos Iliopoulis was rewarded 90,000 drachmas for his role, a high fee at the time.

==See also==
*List of Greek films

==External links==
*  

 
 
 
 


 
 