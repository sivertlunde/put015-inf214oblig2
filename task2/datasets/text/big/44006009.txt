Panchami (film)
{{Infobox film
| name = Panchami
| image =
| image_size =
| caption = Hariharan
| producer = Hari Pothan
| writer = Malayattoor Ramakrishnan Hariharan
| starring = Prem Nazir Jayan Jayabharathi Adoor Bhasi
| music = M. S. Viswanathan
| cinematography = Melli Irani
| editing = G Venkittaraman
| studio = Supriya
| distributor = Supriya
| released =  
| country = India Malayalam
}}
 1976 Cinema Indian Malayalam Malayalam film, Hariharan and produced by Hari Pothan. The film stars Prem Nazir, Jayan, Jayabharathi and Adoor Bhasi in lead roles. The film had musical score by M. S. Viswanathan.  
  

==Cast==
  
*Prem Nazir 
*Jayan 
*Jayabharathi 
*Adoor Bhasi 
*Sankaradi 
*Sreelatha Namboothiri 
*Francis
*Bahadoor 
*Balan K Nair 
*Chandran
*KPAC Sunny 
*Kottarakkara Sreedharan Nair  Master Raghu as Chinnan Meena 
*N. Govindankutty 
*Nedumangad Krishnan
*Nellikode Bhaskaran 
*Paravoor Bharathan 
*Usharani 
 

==Soundtrack==
The music was composed by M. S. Viswanathan. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Anuraaga Surabhila || K. J. Yesudas || Yusufali Kecheri || 
|- 
| 2 || Panchami Paalaazhi || Jayachandran || Yusufali Kecheri || 
|- 
| 3 || Rajanigandhi || Jolly Abraham || Yusufali Kecheri || 
|- 
| 4 || Theyyathom || Vani Jayaram || Yusufali Kecheri || 
|-  Susheela || Yusufali Kecheri || 
|- 
| 6 || Vannaatte Oh My Dear || Jayachandran || Yusufali Kecheri || 
|}

==References==
 

==External links==
*   
*  

 
 
 


 