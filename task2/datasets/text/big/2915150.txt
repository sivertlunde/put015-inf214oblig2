Super Hero Central
{{Infobox film
| name           = Super Hero Central
| image          = Super_Hero_Central.jpg
| caption        = Super Hero Central DVD Box Cover
| director       = Scott Shaw
| producer       = Scott Shaw
| writer         = Scott Shaw
| starring       = Scott Shaw   Hae Won Shin  Kevin Thompson   Conrad Brooks   Donald G. Jackson   Linnea Quigley
| music          = 
| cinematography =  Jake Dharma
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Super Hero Central  is a 2004 Martial arts oriented Cult film. This film was directed by and stars Scott Shaw. The Co-Stars of this films include Donald G. Jackson, Conrad Brooks, and Kevin Thompson.

==Plot==
This film follows two primary characters, Ace X and Kid Velvet, played by Shaw and Thompson.  These two characters are members of a Rock n Roll band, named Super Hero Central, who live on the skids of both society and the music industry.  They transform into masked superheroes to takedown a Hollywood crime lord, named Rinaldi. 

The third primary character of this film is an unnamed character, played by Donald G. Jackson.  This character goes through the film, reporting on the band through telephone conversations and while driving through Hollywood. The fourth, also unnamed,  primary character is played by Hae Won Shin. This character is a spiritual teacher who sends Shaw on his mission to fight crime.

The other primary characters who appear in this film are a reporter, Linda Marshall and a police commissioner, Commissioner Beckman. The reporter is seen throughout the film, reporting on the crime activities taking place in Hollywood, interviewing the police commissioner, and also interacting with the band members. She is eventually captured by the crime lord, which set the climax of this film into motion.

==Locations==
Super Hero Central was filmed in Hollywood, California, Macau, Taipei, Taiwan, Tokyo, Japan, and Vancouver, British Columbia. This film follows two crime-fighting Superheros, played by Shaw and Thompson, who disguise their Superhero identities as members of a Rock n Roll band.

==Hollywood==
Similar to many Scott Shaw films Super Hero Central utilities Hollywood, California as a cinematic backdrop. Though his films are often shot in multiple locations, utilizing various geographic locations in the United States and Asia to tell the story, the primary storyline of his films generally originate in Hollywood. Hollywood landmarks such as the Hollywood sign and images of Hollywood Blvd. are commonly seen in his films. This is also the case with Super Hero Central.

As is the case with virtually all Scott Shaw films, the story for this movie is presented in a non-linear fashion and there are many unexpected and seemingly unrelated events presented throughout this film.  This film also possesses the references to Blacksploitation Cinema apparent in virtually all Scott Shaw films and this feature is full of Music video style edits where the central characters leave behind the storyline and interact solely by the presentation of visual images in association with Techno music. As this film was created soon after the death of his longtime filmmaking partner Donald G. Jackson Shaw designed the story line of Super Hero Central to pay homage to some of the previous films he had created in association with Jackson."

The final credit of this movie is, "In Memory of Dinosaur Filmmaking."  This is the same final used in many Scott Shaw films.

==External links==
*  
* 
* 

 
 