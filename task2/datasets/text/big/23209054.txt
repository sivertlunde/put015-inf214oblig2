Poova Thalaiya
{{Infobox film
| name           = Poova Thalaiya
| image          =  
| caption        = 
| director       = K. Balachander
| producer       = Rama. Arangakannal M. L. A.
| writer         = K. Balachander
| starring       = Gemini Ganesan Jaishankar Nagesh S. Varalakshmi Rajasree Vennira Aadai Nirmala
| music          = M. S. Viswanathan
| cinematography = N. Balakrishnan
| editing        = N. R. Kittu
| studio         = Arul Films
| distributor    = Arul Films
| released       =  
| runtime        = 150 mins
| country        =   India Tamil
}}
 1969 Tamil language drama film directed by K. Balachander. The film features Gemini Ganesan and Jaishankar in lead roles. The film, produced by M. L. A. Rama. Arangakannal, had musical score by M. S. Viswanathan and was released on May 1969.  The film was remade in Telugu as Bomma Borusa and in Malayalam as Balapareekshanam.

==Plot==
Gemini Ganesan is a widower and S. Varalaxmi is his mother in law. Jaishankar is the younger brother of Gemini Ganesan and he falls in love with S. Varalaxmis youngest daughter Venniradai Nirmala. A healthy competition takes place between evil minded S. Varalaxmi and Jai Shankar so as to overthrow one another.

==Cast==
* Gemini Ganesan
* Jaishankar
* Nagesh
* S. Varalakshmi
* Rajasree
* Sreekanth
* Vennira Aadai Nirmala Manorama
* M. R. R. Vasu
* S. Rama Rao
* J. Balayya
* Sachu

==Soundtrack==
The music composed by M. S. Viswanathan. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Vaali (poet)|Vaali || 03:50
|-
| 2 || Madhurayil Parantha || T. M. Soundararajan || 03:58
|-
| 3 || Paladai Meni || P. Susheela, L. R. Eswari || 04:28
|-
| 4 || Poda Chonnal Pottu || T. M. Soundararajan, Manorama (Tamil actress)|Manorama, A. L. Raghavan || 05:43
|-
| 5 || Poova Thalaiya || T. M. Soundararajan, Seerkazhi Govindarajan || 04:21
|}

==References==
 

==External links==
 
 

 
 
 
 
 
 


 