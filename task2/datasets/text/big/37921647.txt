Many Wars Ago
{{Infobox film
 | name =Many Wars Ago
 | image =  Many Wars Ago.jpg
 | caption =
 | director = Francesco Rosi
 | writer = Francesco Rosi Raffaele La Capria Tonino Guerra
 | based on = Emilio Lussu (memoir)
 | starring = Gian Maria Volonté Alain Cuny Pier Paolo Capponi
 | music = Piero Piccioni
 | cinematography = Pasqualino De Santis
 | editing =  Ruggiero Mastroianni
 | producer = Francesco Rosi Marina Cicogna
 | distributor = Dubrava Film
 | released = 1970
 | runtime = 101 min
 | awards =
 | country = Italy Italian
 | budget =
 }} 1970 Cinema Yugoslav war anti war drama film directed by  Francesco Rosi.    It is based on the memoir by Emilio Lussu "Un anno sullaltopiano"  The Italian title is (most likely) ambiguous and difficult to translate ("Men against" is a word by word translation). However "Un anno sullaltopiano" means "A year on the plateau".

== Plot == Trentino Front during World War One around 1916-17, Italian army officers demand far too much of their men.

Time after time the soldiers are forced to leave their trenches in attempts to storm the enemy positions, always with the same horrific result. The Austria-Hungary|Austro-Hungarian machine guns inevitably mow them down. In one attack a major is killed, and subsequently every sixth man of his platoon is chosen to be executed by a firing squad of his comrades, in some bizarre kind of compensation for the killed officer. And it gets only worse...

== Cast ==
* Gian Maria Volonté: corporal Ottolenghi
* Pier Paolo Capponi: corporal Santini
* Alain Cuny: general Leone
* Franco Graziosi: major Ruggero Malchiodi
* Mark Frechette: vice corporal Sassu
* Nino Vingelli: private Ferito
* Mario Feliciani: medical officer (colonel and doctor)
* Daria Nicolodi: red cross soldier
* Giampiero Albertini: captain Abbati

==References==
 

==External links==
* 
 

 
 
 
 
 
  
 
 
 

 