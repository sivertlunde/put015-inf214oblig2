The Idiot (1946 film)
{{Infobox film
| name =The Idiot  
| image =
| image_size =
| caption =
| director = Georges Lampin
| producer = Sacha Gordine
| writer =  Fyodor Dostoevsky  (novel)  Georges Raevsky   Charles Spaak
| narrator =
| starring = Edwige Feuillère   Lucien Coëdel   Jean Debucourt
| music =   V. de Butzow   Maurice Thiriet Christian Matras  
| editing =     Léonide Azar
| studio = Films Sacha Gordine 
| distributor = Les Films Osso
| released = 7 June 1946 
| runtime = 101 minutes
| country = France French 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
The Idiot (French:Lidiot) is a 1946 French drama film directed by Georges Lampin and starring Edwige Feuillère, Lucien Coëdel and Jean Debucourt.  It is an adaptation of Fyodor Dostoevskys novel The Idiot. The films sets were designed by the art director Léon Barsacq.

==Cast==
*   Edwige Feuillère as Nastasia Filipovna 
* Lucien Coëdel as Rogogine  
* Jean Debucourt as Totsky   Sylvie as Madame Ivolvine  
* Gérard Philipe as Le prince Muichkine 
* Nathalie Nattier as Aglaé Epantchine  
* Jane Marken as Naria  
* Maurice Chambreuil as Le général Epantchine  
* Michel André as Gania Ivolvine  
* Elisabeth Hardy as Sophie Ivolvine  
* Roland Armontel as Louliane Timofeievitch Lebediev livrogne  
* Mathilde Casadesus as Adélaïde Epantchine  
* Janine Viénot as Alexandra Epantchine  
* Tramel as Ivolvine 
* Marguerite Moreno as La générale Elisabeth Prokovievna Epantchine 
* Danielle Godet 
* Rodolphe Marcilly
* Maurice Régamey 
* Victor Tcherniavsky
* Charles Vissières 
* Georges Zagrebelsky

== References ==
 

== Bibliography ==
* Dayna Oscherwitz & MaryEllen Higgins. The A to Z of French Cinema. Scarecrow Press, 2009.

== External links ==
*  

 
 
 
 
 
 
 
 
 
 
 
 

 

 