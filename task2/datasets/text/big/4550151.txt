Keetje Tippel
{{Infobox film
| name = Katie Tippel
| image = keetje_tippel_movie_poster.jpg
| caption = movie poster for Katie Tippel
| director = Paul Verhoeven
| producer = Rob Houwer
| writer = Neel Doff Gerard Soeteman
| starring = Monique van de Ven Rutger Hauer
| music = Rogier van Otterloo
| cinematography = Jan de Bont Jane Sperr
| distributor =
| released = 1975
| runtime = 107 min. Dutch
| budget =
}}

 

Katie Tippel (  film by Paul Verhoeven. The film is based on the memoirs of Neel Doff (1858–1942) and was the most expensive Dutch film produced up to that time. The film was a box office success  taking in 1,829,116 admissions in the Netherlands.

==Plot==
The film begins in 1881 at Stavoren, a small town on the outskirts of the Netherlands, and follows the journey of Katies family to Amsterdam, where they hope to escape the grinding poverty of their native town by finding work in the capital. Upon their arrival Katie manages to secure employment at a dye-works, but is fired when she refuses to have sex with the company director. She manages to find a job at a hat shop, where, during a business trip to a brothel, she discovers her older sister Mina is working as a prostitute. Later that evening, back at the hat shop, she is brutally raped by the owner and, as an act of revenge, smashes the shop window.

Weeks go by, during which Katies father struggles to make ends meet working in a factory and Mina begins a slow descent into alcoholism. While attempting to steal some bread from a market cart, Katie is knocked unconscious by a policeman and taken to a sanitorium. It soon becomes apparent that everywhere she goes her body is her only asset as one of the doctors diagnoses her with tuberculosis, but refuses her treatment unless she sleeps with him. It is unclear whether this occurs, but on her discharge Katie rejoins her family and discovers her father has been made redundant and her sister is too drunk to sleep with her clients. Katies mother decides that, rather than let the family starve, Katie must go into prostitution.
 Peter Faber), he takes her back to his studio to pose for a painting he is working on depicting a Socialist revolution. He pays her the rate they negotiated on the street for her services, but tells her he only wants her to model for him. The next day he introduces Katie to Hugo (Rutger Hauer), a banker, and Andre (Eddy Brugman), a wealthy socialist, and they head to a nightclub for a meal. Katies social re-education has begun. Andre is immediately attracted to Katie, but she seems more enamoured with Hugo, with whom she goes home and sleeps. That next morning Hugo provides Katie with money to buy herself a new dress and the two arrange to meet in the park. It is here that Katies former life threatens to catch up with her when the gentleman she approached a few days earlier recognises her and informs Hugo he knew Katie when she only cost fifty pence. Hugo punches him, refusing to believe his story and promises Katie that she can move in with him "forever." Katie goes to see her family, informing them she intends to leave. Her mother asks how she will feed the children if Katies not there to provide. Katie tells her mother she "should have fucked less" and storms out, kicking her hysterical mother out of the way.

Once at Hugos, Katie settles into the life of the bourgeoisie but is soon troubled at having to go spying on some of Amsterdams less affluent shop owners to see who Hugo should refuse credit to. After being exposed in a coffee shop and having her face rubbed in hot chocolate as a result, Katie tells Hugo she will not do his dirty work anymore. Very soon Hugo informs Katie that he intends to marry his fiancee and Katie must leave, stressing that their relationship was just a bit of fun that went too far. Katie, with nowhere to go, runs out onto the street and joins in a socialist march. The police arrive to break it up, opening fire on the marchers. During the confusion, Katie is reunited with George and Andre who are also present. Andre is shot in the arm and collapses, hitting his head. George takes them to a carriage waiting nearby and Andre and Katie board it, taking them to Andres country mansion. Katie stays with him and, when he awakes, it becomes apparent that romance will blossom between the two. They discuss the subject of money, Katie telling Andre that "money turns people into bastards." When Andres headwound begins to bleed, Katie informs Andre that the best cure is to suck the blood. "Thats better," she says, a sliver of blood running from the corner of her mouth. The picture freezes and a roller-caption appears informing us that the film is based on true events from Nell Doffs life and that "her indomitable spirit lives on in this film."

==Cast==
*Monique van de Ven - Keetje Tippel
*Rutger Hauer - Hugo Peter Faber - George
*Andrea Domburg - Keetjes moeder
*Hannah de Leeuwe - Mina, Keetjes zus
*Jan Blaaser - Keetjes vader
*Eddie Brugman - André

==Original ending==
The film was to feature an epilogue set several years in the future where Katie, now married to Andre, would be sat reading. As the noise from those starving in the street becomes unbearable, Katie rises and closes the open window to shut out the sound. Her journey is now complete, Katie becoming one of the "bastards" she despises. However, producer Rob Houwer decided this was too downbeat as an ending, so it was never filmed.

==Production==
Gerard Soetemans original script for the film was substantially longer, Katies journey mirroring the growth of Socialism in the Netherlands during the late 19th Century. Owing to budgetary restrictions from the Dutch Government and Rob Houwer many of the more lavish scenes were cut, placing the emphasis less upon the period the film is set than upon Katie herself.

==Intertextuality== Black Book (2006) also features a strong-willed female character whose quest for identity becomes blurred.

== References ==
 

==External links==
* 

 

 
 
 
 
 
 
 
 