The Temptations (miniseries)
{{multiple issues|
 
 
}}
{{Infobox television film
| name           = The Temptations
| image          =  
| image_size     = 
| caption        = 
| genre          = Miniseries
| creator        = 
| director       = Allan Arkush
| producer       = Jay Benson   Otis Williams  Shelly Berger
| writer         = 
| screenplay     = Robert Johnson Kevin Arkadie
| story          = 
| based on       =  
| narrator       = Charles Malik Whitfield Leon Tina Christopher Reid Mel Jackson Smokey Robinson Alan Rosenberg
| music          = Smokey Robinson Jamie Anderson
| editing        = John Duffy Neil Mandelberg Babelsberg International Film Produktion
| distributor    = Hallmark Entertainment Lions Gate (2001 home video) Vivendi Entertainment (2001 home video)
| budget         = 
| country        = United States
| language       = English
| network        = NBC
| released       =  
| first_aired    =  
| last_aired     =  
| runtime        = approx. 88 min. per episode
| num_episodes   = 2
| preceded_by    = 
| followed_by    = 
| website        = 
}}

The Temptations is a four-hour television miniseries broadcast in two-hour halves on NBC, based upon the history of one of Motowns longest-lived acts, The Temptations. Executive produced by former Motown executive Suzanne de Passe, produced by Otis Williams and Temptations manager Shelley Berger, and based upon Williams’ Temptations autobiography, the miniseries was originally broadcast on November 1 and November 2, 1998. It was filmed on location in Pittsburgh, PA in the spring of 1998. Allan Arkush was the miniseries’ director.

==Overview== Leon as Paul Williams. Also featured were Charles Ley as Dennis Edwards, J. August Richards as Richard Street, Obba Babatundé as Berry Gordy, Vanessa Bell Calloway as Johnnie Mae Matthews, and Mel Jackson as Norman Whitfield.

As the miniseries was based upon Otis Williams’ book, it came from his perspective: the focus of the story tended to be on Williams and Melvin Franklin, with David Ruffin and Eddie Kendricks seen as antagonists for much of the second half (although Kendricks was still given a more sympathetic portrayal than Ruffin). Dennis Edwards was not heavily focused upon, nor was much said of the problems he later had with Otis Williams. Nevertheless, the miniseries gave a general overview of both the history of the group and that of Motown, and, thanks to de Passes connection, the film was able to use authentic props and locations.

A number of liberties were taken with factual events for dramatization purposes. For example, in the film, Melvin Franklin apparently dies outside of the kitchen in his mothers house. In reality, he died from heart failure at the Cedars-Sinai Medical Center after being admitted following a series of Epileptic seizure|seizures.  As Franklins death was still fresh in the minds of the miniseries creators (he died in 1995), it was decided that the miniseries would not present Franklins death as it actually occurred. Also, David Ruffin was not found dead near a hospital, and then taken to a morgue where he was properly identified. Instead, he had suffered a drug overdose, and was taken to the hospital by his chauffeur where he died there.   Another inaccuracy is the depiction of Ruffin, Kendricks and Edwards performing together before the 1982 reunion tour, when in reality Ruffin and Kendricks did not start performing together until 1985, with Edwards joining them in 1989 after the group was inducted into the Rock and Roll Hall of Fame. 

Although the movie is set mostly in Detroit Michigan and Los Angeles, the producers chose to shoot the film in Pittsburgh, presumably to take advantage of the many different architectural and geographical looks that Pittsburgh offers. de Passe Entertainment had, some six years earlier, shot   in Pittsburgh as well.

==Plot==

===Part one (November 1, 1998)=== singer lock eyes, which he credits as the moment he devoted his life to music. Otis stepfather Edgar is less than pleased with Otis plans to become a singer instead of an assembly line worker, but his mother Haze is supportive.
 bass singer, Melvin Franklin, singing with the Voice Masters on a street corner. Once Melvin gets permission from his mother, Rose, he becomes the bass signer for the Siberians.

On the way home from school one day, Otis and the group follow a group of female classmates while singing "Earth Angel". Otis takes a liking to one of the girls, Josephine, whom he begins dating. The next Saturday, the Siberians hear a radio DJ requesting them by name to come to the radio station. Arriving at the station, the DJ leads the group to a run-down recording studio downstairs. Johnnie Mae Matthews, who owns the studio, reveals herself as the person whod summoned the Siberians. She declares herself their new manager and producer, changes their name to "Otis Williams & the Distants"‘‘, and records their first single, "Come On".
 Paul Williams The Primettes Mary Wilson, Diane Ross. Eager to meet Motown founder Berry Gordy, Otis and Melvin corner Gordy in the bathroom after their performance and get his business card.

Once the party ends, Johnnie Mae shows up in a new car emblazoned with the Distants name, bought with the money earned from sales of "Come On". Awestruck, the Distants innocently ask about their share of the money, which angers Johnnie Mae to the point that she fires them on the spot, keeping the money, car, and group name for herself. Al, Richard, and Pee-Wee promptly quit as well.

Shortly after, Melvin approaches Otis and tells him that Eddie Kendricks and Paul Williams recently left The Primes and are interested in joining with them. Otis is reluctant to let them join, as he finds them arrogant and cocky, but ultimately accepts when Al rejoins the group. With this new line-up, the group renames themselves "The Elgins". Eddie and Paul prove to be valuable members; Paul teaches them how to dance and becomes their unofficial choreographer, while Eddie becomes their falsetto singer. In March 1961, The Elgins go to Motowns Hitsville USA in hopes of a record deal, which Berry Gordy offers - with the stipulation that they come up with a better group name while they wait for an audition.

After sitting outside the studio for hours waiting to be called in and thinking up a new name, a secretary named Martha Reeves finds them outside and calls them in to meet Berry, who approves of their new name, "The Temptations". After hearing them perform "Oh Mother of Mine" (which would become their debut single for Motown), Berry enthusiastically signs them under the Motown label. Leaving Hitsville in high spirits, reality hits when Otis arrives home and Josephine informs him she is pregnant, leading them to get married before Josephine gives birth to their son Lamont.

For several years, The Temptations career at Motown suffers from a string of underperforming records, though Gordy assigns Smokey Robinson to write and produce for the group and Cholly Atkins to revise Pauls choreography. The group start to doubt their talents and Al starts to lose his passion for singing, becoming more negative and volatile. This comes to a head during the 1964 Motown New Years party, where Al and Paul get into a fight backstage after a performance and Al breaks a bottle across Pauls face. Otis fires Al on the spot, and the Temptations go on as a quartet to perform "Shout (The Isley Brothers song)|Shout" as an encore. Two of their Motown colleagues, Jimmy Ruffin and his brother David Ruffin|David, join the Tempts onstage, with Davids performance skills particularly impressing the group. David is invited to join the group, which he readily accepts.

A few days after the new year, the group head to Hitsville to record a song written for them by Smokey Robinson. The song, "The Way You Do the Things You Do" becomes an instant hit and puts the Temptations on the map. Shortly after the song hits the charts, the group, along with labelmates such as The Miracles, The Supremes, Mary Wells, Martha & the Vandellas, and Marvin Gaye, tour the eastern United States and the Jim Crow-era southern United States as part of Motowns Motor Town Revue. While on tour, Otis begins an affair with Florence Ballard of The Supremes, putting strain on his marriage with Josephine.
 My Girl". The song becomes a massive success in 1965, reaching number one on the charts and leading to appearances on television. The group enjoys their newfound success and wealth, spending money on themselves and their loved ones.

With this success, however, soon comes trouble. By early 1966, David starts to develop an ego, thinking himself to be solely responsible for the Temptations success. He also begins using drugs and starts showing up late for rehearsals and meetings, if at all. When Otis and Melvin go to Davids house to urge him to straighten up, David declares that the group instead should change its name to "David Ruffin & the Temptations".

In the meantime, Berry Gordy hires a new manager for both the Temptations and The Supremes, a white man named Shelly Berger. Despite some reluctance from the group - especially Paul - Shelly works to expand the Temptations fanbase to the mainstream white audience.
 Copacabana nightclub in New York City, but David is nowhere to be found at showtime and the group goes on as a foursome. David shows up midway through their first number, and before they perform their song, "(I Know) Im Losing You", he introduces himself by declaring to the Copa audience "Im David Ruffin...and these are the Temptations." Fed up, Otis, Paul, and Melvin vote David out of the group, with only Eddie voting to keep David. After David is formally fired in mid-1968, Melvin turns to his groupmates and rhetorically asks, "so now what"?

===Part two (November 2, 1998)=== Cloud Nine". Cloud Nine" Copacabana angered that Paul is still declared not ready to tour with the group again and another singer, the returning former Distants member Richard Street, is put in his place. Later, Otis and his son Lamont visit Paul at his house. Paul asks to be back in the Temptations, while demonstrating his dancing, almost falling over. Otis tells him that he will be back when he gets better. In June 1972, Whitfield writes another song called "Papa Was a Rolling Stone", which the group are initially against recording due to Otis feeling its not a ballad and Dennis because his father died on the third of September. Eventually, the group goes along with it. In a montage set to the song, David and Eddie are seen performing and Paul is seen struggling with his addiction, while fighting with his wife, and later driving around town, ending with Paul committing suicide in a parking lot in 1973. Eddie reunites with the others at the funeral, with Melvin telling him that while hes out of the group, they will always be family.

By 1977, the Temptations have moved from Detroit to Los Angeles and have been hit with a dry spell in their career. The group, now with Otis and Melvin as the only remaining original members, fire Shelly as their manager, leave the Motown label and start recording under the Atlantic Records label. Eddie is still under the Motown label and has made two major hits, while David, who has had some hits after the Temptations, is also under a dry spell. One day, while Melvin is helping a woman with her grocery bags, a thief gets in his car and tries to start it. When Melvin tries to stop him, the thief shoots him in both of his legs, kicks him out, and drives off in his car. Melvin tells Otis to go on tour without him, as they need the money. After the tour, Otis goes back to Detroit with Lamont to visit his mother, who tells him that she has cancer. They then have a heart-to-heart on the porch.
 Keep On Truckin", brings David on stage and they sing "Youre My Everything" together. After everyone leaves the club, Eddie and David have a drink at the bar and agree to start their own faction of the Temptations, along with Dennis Edwards, who was fired from the Temptations in 1978. Otis and Melvin move back to Detroit and go back under the Motown label and Shelly becomes their manager again. Not long after, Motown becomes interested in setting up a reunion tour between both sets of the Temptations.

By 1982, the tour is officially underway and both sets of Temptations come together to rehearse and become reacquainted. While on tour, Josephine calls and informs Otis that Lamont died in a construction accident. After Otis gets back from the funeral, the tour starts to fall apart, as Davids drug addiction starts to trigger his destructive nature.

In 1989, the Temptations are inducted into the Rock and Roll Hall of Fame. At the ceremony, Otis and Melvin are reunited with David, Eddie, and Dennis. And despite their past squabbles and rivalries, for one moment, they are friends again.

In June 1991, a dead body is found in front of a hospital. After a week in the morgue, the body is finally identified as that of David Ruffin, dead of an apparent drug overdose. Eddie dies soon after of lung cancer in October 1992, but that is not shown.

In February 1995, Otis and Melvin, now reduced to a wheelchair, visit Melvins mother. While preparing to eat dinner, Melvin, despite being in a wheelchair, volunteers to get short ribs from the kitchen. While hes gone, Melvins mother thanks Otis for taking care of Melvin and keeping the Temptations together through all the good and bad times. The two then call for Melvin, but he doesnt respond. They go into the kitchen and find him dead. Many people show up at the funeral, including Smokey Robinson, who sings his song "Really Gonna Miss You".

The film ends with the "classic five" Temptations (Otis, Melvin, Eddie, Paul, and David) in their youth, singing "My Girl" on a stage. At the end of the song, they take a bow, with Otis saying in a voice-over "Temptations, forever."

==Afterwards==
The miniseries was a ratings success, and Arkush won a 1999 Emmy award for Outstanding Directing for a Miniseries or a Movie. The miniseries has been subsequently rerun on the VH-1 cable television network and released to VHS and DVD. The VHS release notably omitted a few scenes which had previously aired on the television premiere. One such scene includes David Ruffin, clearly under the influence of drugs and his ego, becoming belligerent during a picnic celebration with the other members of the group. The removal of this scene is possibly due to the ensuing suit.
 suit against Williams, Shelly Berger, David V. Picker, Motown, De Passe Entertainment, Hallmark Entertainment, and NBC for use of their likenesses in the film, defamation of character, and emotional distress because of the inaccurate depictions of events.  They also alleged that the miniseries misportrayed them and/or their relatives and twisted facts. The judges ruled in favor of the defendants, and the ruling was upheld when the plaintiffs appealed in 2001.  Otis Williams later claimed that while his book was the source material for the film, he did not have a great deal of control over how the material was presented.

==Cast==
*Charles Malik Whitfield as Otis Williams, founder and leader of Otis Williams & the Distants and later The Temptations. He becomes the only singer to remain with the group from its inception, and conflicts with some of his group mates, particularly David Ruffin and Eddie Kendricks, over group leadership. Melvin "Blue" Franklin, Otis best friend, and a member of both Otis Williams & the Distants and The Temptations for over four decades. Shy and soft-spoken, he secretly struggles with arthritis over the years. Eddie "Cornbread" Kendricks, a member of The Primes and The Temptations original first tenor/falsetto and co-lead singer. After quitting the group in 1971, Eddie becomes a solo singer and later joins forces with fellow ex-Temptation David Ruffin. Paul Williams, a member of The Primes and the Temptations original lead singer, who succumbs to alcoholism and poor health, forcing his retirement from the act in 1971. He commits suicide in 1973. Leon as David Ruffin, The Temptations lead singer from 1964 to 1968, whose ego leads the others to force him out of the group.
*Alan Rosenberg as Shelly Berger, the Temptations manager.
*Tina Lifford as Haze, Otis mother.
*Jenifer Lewis as Mama Rose, Melvins mother.
*Gina Ravera as Josephine, Otis wife during the 1960s and the mother of their son Lamont.
*Obba Babatundé as Berry Gordy, Jr, founder of Motown Records.
*Erik Michael Tristanas a young Smokey Robinson, the lead singer of The Miracles and The Temptations primary songwriter and producer during the early 1960s. The real-life Smokey Robinson appears as himself in a cameo at the end of Part Two.
*J. August Richards as Richard Street, a member of Otis Williams & the Distants, who later replaces Paul Williams in The Temptations
*Harold Surratt as Edgar, Otis stepfather.
*Charles Ley as Dennis Edwards, The Temptations lead singer in the late 1960s and 1970s. He later joins forces with David and Eddie when they form a Temptations splinter group in the 1980s.
*Vanessa Bell Calloway as Johnnie Mae Matthews, producer and manager for Otis Williams & the Distants.
*Mel Jackson as Norman Whitfield, The Temptations primary producer and songwriter during the late 1960s and early 1970s.
*Rhonda Ross Kendrick as Maxine, Pauls wife Elbridge "Al" Bryant, Otis high school friend and a member of both Otis Williams & the Distants and The Temptations. Having a day job as a milk man, Al becomes restless and moody as the Temptations struggle to find their big break, and is fired in December 1963.
*Adam Lazarre-White as Flynn, Davids driver and self-appointed manager. Christopher Reid as Joltin Joe, a radio DJ who summons The Distants for Johnnie Mae Matthews
*Stevland Parks as Lamont, Otis and Josephines son, age 12
*Chrystal Bates as Mrs. Rogers, Josephines mother.
*Bianca Lawson as Diana Ross of The Primettes/The Supremes.
*Melissa Mercedes Cardello as Florence Ballard of The Primettes/The Supremes. She begins an affair with Otis while on the Motor Town Revue in 1964. Mary Wilson of The Primettes/The Supremes.
*NTasha A. Pierre as Martha Reeves, a Motown performer and secretary.
*Lamman Rucker as Jimmy Ruffin, Davids older brother and a Motown performer.
*Nyjah Moore as Tammi Terrell, Davids girlfriend and a Motown performer.
*Ricky Fante as Marvin Gaye, a Motown performer. Russell Clark as Cholly Atkins, Motowns in-house choreographer.
*Vincent A. Ponder as James Pee-Wee Crawford, a member of Otis Williams & the Distants.
*Benjamin J. Cain Jr. as Glenn Leonard, the Temptations first tenor in the 1980s.
*Jonnie Brown as Damon Harris, the Temptations first tenor in the 1970s after Eddie quits the act.

==Awards==
{| class="wikitable"
|-  style="background:#b0c4de; text-align:center;" Year
! Award
! Result
! Category
! Recipient
|- 1999 || Motion Picture Motion Picture Sound Editors || rowspan=4|Won || Best Sound Editing - Television Mini-Series - Music || Kevin Crehan (music editor) and Tom Villano (music and scoring editor)  (For episode "Night One") 
|-
| Best Sound Editing - Television Mini-Series - Dialogue & ADR || Suzanne Angel, Mark Friedgen, G. Michael Graham, Anton Holden, Kristi Johns, Mark R. La Pointe, Michael Lyle, Scott A. Tinsley, and Tim Terusa  (For episode "Night One") 
|- Outstanding Television Movie or Mini-Series ||  - 
|- Outstanding Directing for a Miniseries or a Movie || Allan Arkush
|}

==References==
 

==External links==
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 