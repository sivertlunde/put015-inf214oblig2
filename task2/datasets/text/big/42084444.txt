If I Were Rich
{{Infobox film
| name =  If I Were Rich
| image =
| image_size =
| caption =
| director = Randall Faye
| producer =Randall Faye
| writer =  Horace Annesley Vachell (play)   Brandon Fleming
| narrator =
| starring = Jack Melford   Kay Walsh   Clifford Heatherley   Minnie Rayner
| music = 
| cinematography = Geoffrey Faithfull
| editing = 
| studio = Randall Faye Productions
| distributor = RKO Pictures
| released = May 1936
| runtime = 59&nbsp;minutes
| country = United Kingdom English
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
If I Were Rich is a 1936 British comedy film directed by Randall Faye and starring Jack Melford, Kay Walsh and Clifford Heatherley.

==Plot summary==
A Socialist barber inherits an Earldom.

==Cast==
*Jack Melford as Albert Mott
*Kay Walsh as Chrissie de la Mothe
*Clifford Heatherley as General de la Mothe
*Minnie Rayner as Mrs. Mott
*Henry Carlisle as Puttick
*Frederick Bradshaw as Jack de la Mothe
*Ruth Haven as Nancy
*Quentin McPhearson as Higginbotham

==Production==
The film was made at Nettlefold Studios as a quota quickie for release by the Hollywood company. RKO Pictures.  It was based on the play Humpty-Dumpty by Horace Annesley Vachell.

==References==
 

==Bibliography==
*Chibnall, Steve. Quota Quickies: The Birth of the British B Film. British Film Institute, 2007.
*Low, Rachael. Filmmaking in 1930s Britain. George Allen & Unwin, 1985.
*Wood, Linda. British Films, 1927–1939. British Film Institute, 1986.

==External links==
* 

 
 
 
 
 
 
 
 
 


 