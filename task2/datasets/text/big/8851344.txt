Premonition (2007 film)
{{Infobox film
| name           = Premonition
| image          = Premonitionmp.jpg
| caption        = Theatrical release poster
| director       = Mennan Yapo
| producer       = Ashok Amritraj Jon Jashni Adam Shankman Jennifer Gibgot Sunil Perkash Nick Hamson Bill Kelly
| starring       = Sandra Bullock Julian McMahon Nia Long Kate Nelligan Amber Valletta Courtney Taylor Burness Shyann McClure
| music          = Klaus Badelt
| cinematography = Torsten Lippstock
| editing        = Neil Travis Offspring
| distributor    = TriStar Pictures Metro-Goldwyn-Mayer
| released       =  
| runtime        = 96 minutes
| country        = United States
| language       = English
| budget         = $20 million
| gross          = $84,146,832   
}} supernatural thriller thriller film directed by Mennan Yapo and starring Sandra Bullock, Julian McMahon, and Amber Valletta. The films plot depicts a housewife named Linda who experiences the days surrounding her husbands death in a non-chronological order, and how she attempts to save him from his impending doom.

Contrary to popular belief, the film is not a remake of the 2004 Japanese horror film Premonition (2004 film)|Premonition, and is its own original story.
 

==Plot==
 
After Jim (Julian McMahon) surprises his wife Linda Hanson (Sandra Bullock) with a new house, they have two daughters, Megan (Shyann McClure) and Bridgette (Courtney Taylor Burness). While Jim is away on a business trip, Linda is left to take care of the girls.

Linda talks with her friend Annie (Nia Long), who asks how Linda and Jim are, since their relationship had gotten into a rut. Then Linda goes through various chores. When she checks the answering machine there is a message from Jim, and he says that he meant everything he said in front of the kids the other night and he wants her to know that he was sincere. He spots a call waiting on his phone and mutters "Is that you?" before hanging up. Linda calls him back and leaves a message.

The doorbell rings and Linda opens the door to Sheriff Reilly (Marc Macaulay), who tells Linda that her husband died in a car accident on the previous day. After picking her girls up from school, Linda sadly tells them that their father won’t be coming home. Linda’s mother, Joanne (Kate Nelligan), is over as well and occupies Lindas daughters. After putting the kids to bed, Joanne talks with her daughter. She tells Linda that although it is difficult, they should start making funeral arrangements and look over Linda and Jims insurance plans. Linda tells her mother that she is not ready yet. Linda falls asleep on the living room couch,
Linda wakes up in bed, dressed in a nightgown and covered with sheets and blankets. Confused, she gets up and knocks on the guest room door. No one is inside. Linda walks downstairs, where Jim is at the counter drinking coffee and watching TV. Linda is shocked.

Still in a daze, Linda drops off the kids at school. As she drives on, she makes an abrupt stop that angers drivers around her, and she is approached by Sheriff Reilly, who warns her to be more careful. The sheriff acts as if hes never met her before. Linda returns home to her chores but notices the rainbow sweater again. She walks outside to hang laundered bed sheets. While she tries to spread out a sheet, she trips on a toy and falls backwards on a dead crow. Some of the blood gets on her hand, and she gets blood all over the glass door (which now has no stickers on it) and on the sink. The day ends with a family dinner, the girls reporting a boring day,
Linda wakes up the next day and finds an empty prescription bottle of lithium with pills scattered in the sink. The label on the bottle states the pills were prescribed by  Doctor Norman Roth. She finds the mirrors in the house covered in sheets while she makes her way downstairs. She encounters family and friends wearing mourning garb. Annie is there and assures Linda that the kids are safe outside. Linda  finds the girls on the swing set. Bridgette has terrible red gashes all over her face. Megan tells her mom that she does not see any scars and her sister looks perfect.

At the burial, a young priest is giving the eulogy for Jim, describing him as a wonderful man who always put his family first.

Back at the house, Linda frantically flips through the phone book, but finds the correct page in the trash rather than the book. She calls Dr. Roths office, and a recorded message states that the office is only open during weekdays. Later, several men come to the house. One introduces himself as Dr. Norman Roth (Peter Stormare), and with him are his assistants and Sheriff Reilly. Joanne apologizes but tells Linda that she needs help, Linda wakes up in her own bed again and finds Jim in the shower. At breakfast, she is happy to see that Bridgettes face is wound-free. After dropping the girls off at school, she stops in her driveway and opens the garbage to discover the crows body inside. She goes to Dr. Roths Office. He doesnt recognize her and initiates a session where she tells him of seeing Jim alive and then dead again. At the end of the session he prescribes lithium.

Back at the house, Linda sees Bridgette running toward the glass doors and urges her to stop. Bridgette cant tell the door is closed and runs through it, breaking the glass and hurting her face and hands. Linda hurries the girls to the emergency room and Jim arrives.

Linda feels the crumpled phone book paper in her back pocket. She throws it in the trash. But the sight of the paper in the basket triggers her memory and she makes a timeline of the weeks events.  She hasn’t experienced Friday yet, but Saturday is the funeral. She hides the paper under the tablecloth and goes to talk to Jim. She begs him not to go, but seeing that its no good, requests that "If tomorrow is Wednesday, please, please wake me up before you leave." He promises.

Linda wakes up on the living room couch, understands it is Friday. She meets with Annie who gives sympathy for the weeks events. Linda tells Annie that maybe all of this was meant to happen.

Linda then visits the insurance agent, who tells her that Jim came in the morning of his accident and tripled his life insurance plans, and she makes the funeral arrangements. Later that night, she asks Joanne, if letting Jim die would be the same as killing him. Joanne simply confirms that he is already dead.

Linda wakes up on the previous Sunday. She sends Jim off with the kids while she drives to the church. There, she speaks with Father Kennedy (Jude Ciccolella) and explains that she’s scared. Kennedy has an entire book detailing incidents similar to hers. In the 20th century a woman had a vision that there was going to be a hurricane and kill all the people, she was later burned at the stake as a witch, two days later the hurricane happened. He explains that the faithless are more susceptible to greater forces. He says that she must have faith and that life itself can be a miracle.

Eventually, Linda visits the site of the accident. She stares at the "Mile 220" sign, which is where Sheriff Reilly told her the accident took place. Images of her family flash before her eyes as Linda tries to decide what to do.

That night, Linda puts pressure on Jim and the kids to show more affection for each other. She continues acting strange, so Jim tries to comfort her. She tells him that she’s had a dream where he died. He tells her that it is just a dream and that everything will be fine, Linda wakes up in her bed once again. She finds a note from Jim saying that he’s taking the kids to school and will be back tomorrow. Linda begins to search for Jim.

Jim then calls the house and leaves the message from the beginning of the film. The two have a reconciliation. Then Linda tries to save Jim from the car accident over the phone; however, her attempt fails, The movie ends with Linda pregnant with another child.

==Cast==
*Sandra Bullock as Linda Hanson
*Julian McMahon as James "Jim" Hanson
*Courtney Taylor Burness as Bridgette Hanson
*Shyann McClure as Megan Hanson
*Nia Long as Annie
*Kate Nelligan as Joanne
*Amber Valletta as Claire Francis
*Marc Macaulay as Sheriff Reilly
*Jude Ciccolella as Father Kennedy
*Peter Stormare as Dr. Norman Roth
*Mark Famiglietti as Doug Caruthers
*E.J. Stapleton as Model Home Salesman
*Matt Moore as Priest
*Irene Zeigler as Mrs. Quinn
*Marcus Lyle Brown as Bob

==Production== Minden and Shreveport, Louisiana|Shreveport, Louisiana.

==Release==
The film was first released in the U.S. on March 16, 2007. The film was not released theatrically in Norway, it was released direct-to-video on January 2, 2008 in that country.

===Home media===
The film was released on Blu-ray and DVD on July 17, 2007.

==Reception==

===Box office===
Premonition opened in 2,831 theaters and came in third place behind 300 (film)|300 and Wild Hogs, opening with $17,558,689 with a $6,202 average. The film stayed in theaters for 7 weeks and grossed $47,852,604 in the United States and $84,146, 832 worldwide. 

===Critical reception=== rating average Groundhog Day." normalized rating out of 100 to reviews from film critics, the film is considered to have "generally unfavorable reviews" with a rating score of 29 based on 30 critics.  Despite the weak reviews, several critics, including Rex Reed, commended Sandra Bullock for her performance. 

==References==
 

==External links==
* 
* 
* 
* 
* 
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 