Charlie Bartlett
 
{{Infobox Film
| name = Charlie Bartlett
| image = Charlie_bartlett_ver4.jpg
| image_size = 
| alt = 
| caption = Theatrical release poster
| director = Jon Poll
| producer = Sidney Kimmel Barron Kidd Jay Roach David Permut
| writer = Gustin Nash
| starring = Anton Yelchin Kat Dennings Robert Downey Jr. Tyler Hilton Hope Davis
| music = Christophe Beck
| cinematography = Paul Sarossy
| editing = Alen Baumgarten SKE
| distributor = Metro-Goldwyn-Mayer
| released = February 22, 2008
| runtime = 97 minutes
| country = United States
| language = English
| budget = United States dollar|$12 million   
| gross = $5,254,986    
}} therapeutic advice and prescription drugs to the student body at his new high school in order to become popular.
 Cannes Film Maui Film Festival, and the Cambridge Film Festival before going into theatrical release in the United States and Canada on August 3, 2007.

==Plot==
The son of a depressed but doting mother (Hope Davis) and a father who is serving time for tax evasion, wealthy teenager Charlie Bartlett (Anton Yelchin), - after being expelled from several private academies for various infractions - enrolls in a public school run by embittered alcoholic Principal Nathan Gardner (Robert Downey, Jr.). Unable to fit-in with most of his fellow students, Charlie forms an alliance with school bully Murphy Bivens (Tyler Hilton) and offers him half the proceeds from the sale of a variety of prescription drugs Charlie obtains by feigning physical and emotional symptoms during sessions with different psychiatrists.

Before long, his natural charm and likability positions him as the schools resident therapist, who offers advice within the confines of the boys bathroom. Charlies social life noticeably improves as he gains the confidence and admiration of the student body and begins to date the principals rebellious daughter, Susan (Kat Dennings).

Complications arise when seriously depressed Kip Crombwell (Mark Rendall) attempts suicide by swallowing a handful of anti-depressants provided by Charlie. Charlie befriends Kip after having an in-depth conversation with Principal Gardner. Charlie discovers Kip is writing a play about adolescent issues and pitches it to Gardner who is, at first, unsure but agrees when Kip says that it would make him less inclined to attempt suicide again. The students then have trouble with the student lounge; security cameras get installed, and they start a riot.

One day, Charlie comes by Susans house to pick her up for a date, and he gives her a pharmacy bag. Mr. Gardner comes out, thinking the bag is full of "inappropriate items", and pushes Charlie, who then punches Gardner as a reflex. He tries to apologize, but the principal is not forgiving. Susan and Charlie drive off, and the bag turns out to contain nicotine gum, to help Susan to quit smoking cigarettes.

That night, the students of the public school are all at the student lounge, when the cops arrive. They arrest Charlie for assault, Principal Gardner is fired, and the kids trash the lounge building. Charlie is released, and before the play he stops by Mr. Gardners house to invite him. Charlie finds Mr. Gardner heavily intoxicated and waving a revolver. They get into a heated argument which culminates in Charlie falling into a pool after attempting to tackle Mr. Gardner, thinking he is about to commit suicide. Mr. Gardner, appearing sober, comes to his senses and dives in the pool to save Charlie and states that he was not attempting to commit suicide and that he has too many responsibilities to do so. They talk their problems over about Charlies father and Susan, and then go to the play. Mr. Gardner becomes a history teacher, and Charlie is now able to visit his father.  The film ends with Charlie applying for a summer internship at a psychiatric institute.

==Cast==
 
* Anton Yelchin as Charlie Bartlett
* Kat Dennings as Susan Gardner
* Robert Downey, Jr. as Nathan Gardner
* Tyler Hilton as Murphey Bivens
* Hope Davis as Marilyn Bartlett
* Mark Rendall as Kip Crombwell
* Jake Epstein as Dustin Lauderbach
* Megan Park as Whitney Drummond
* Lauren Collins as Kelly
* Derek McGrath as Superintendent Sedgwick Stephen Young as Dr. Stan Weathers
* Abby Zotz as Mrs. Crombwell Drake as A/V Jones
* Sarah Gadon as Priscilla
 Aubrey Graham (Jimmy Brooks), and Ishan Davé (Linus).

==Production==
  
The film was shot on location in Toronto as well as at Parkwood Estate in Oshawa, Ontario and Trafalgar Castle School in Whitby, Ontario. It was also filmed at Western Technical-Commercial School, where parts of Billy Madison, Ice Princess, Alley Kids Strike, "Fever Pitch" and Being Erica were shot.

==Soundtrack==
The soundtrack to Charlie Bartlett was released on January 22, 2008.

{{Track listing
| collapsed       = no
| extra_column    = Artist
| total_length    = 47:11 

| title1          = Charlies Monologue
| length1         = 1:22
| extra1          = Monologue

| title2          = Charlies Theme
| length2         = 0:51
| extra2          = Christophe Beck

| title3          = Tennis
| length3         = 1:01
| extra3          = Christophe Beck

| title4          = Unnecessary Trouble
| length4         = 1:01
| extra4          = Hard-Fi

| title5          = Visiting Hours
| length5         = 1:17
| extra5          = Christophe Beck

| title6          = Selling Dvds
| length6         = 0:55
| extra6          = Christophe Beck

| title7          = Charlie & Shrinks
| length7         = 0:50
| extra7          = Dialouge

| title8          = Pusherman
| length8         = 5:02
| extra8          = Curtis Mayfield

| title9          = Jazz It Up
| length9         = 1:58
| extra9          = Christophe Beck

| title10         = Prescription Flush
| length10        = 0:58
| extra10         = Christophe Beck

| title11         = Cameras Going Up
| length11        = 0:38
| extra11         = Christophe Beck

| title12         = First Kiss
| length12        = 1:16
| extra12         = Christophe Beck

| title13         = Oh Yeah
| length13        = 2:57
| extra13         = The Subways

| title14         = Kip Overdoses
| length14        = 2:14
| extra14         = Christophe Beck

| title15         = Voodoo
| length15        = 3:26
| extra15         = Spiral Beach

| title16         = Passing Notes
| length16        = 1:07
| extra16         = Christophe Beck

| title17         = This Is a School, Not a Prison
| length17        = 1:19
| extra17         = Christophe Beck

| title18         = New Clouds, Not Clouds
| length18        = 3:41
| extra18         = Spiral Beach

| title19         = Gardner Hits Bottom
| length19        = 1:19
| extra19         = Christophe Beck

| title20         = Day Ok
| length20        = 2:18
| extra20         = Spiral Beach

| title21         = Seat on This Train
| length21        = 4:22
| extra21         = Tom Freund

| title22         = Youre Not Alone
| length22        = 3:26
| extra22         = Christophe Beck

| title23         = Dr. Bartlett
| length23        = 1:44
| extra23         = Christophe Beck

| title24         = If You Want to Sing Out, Sing Out
| length24        = 2:09
| extra24         = Cat Stevens (Performed by Kat Dennings)

}}

==Critical reception==
Stephen Holden of The New York Times wrote "If the attention span of Charlie Bartlett didn’t wander here and there, the movie might have been a high school satire worthy of comparison with Alexander Payne’s Election (1999 film)|Election. But as it dashes around and eventually turns soft, it loses its train of thought ...   never coalesces into the character-driven, serious comedy with heart that you want it be." 
 American Pie route. Instead, the film grinds on with only a few bright moments. The big problem, though, isnt the script but rather the direction and, specifically, the plodding pace of the film. Thats surprising, given that first-time director Jon Poll has a background in film editing. It may have something to do with knowing pretty much what will happen from one moment to the next, but you keep wanting Poll and his cast to get on with things, or at least, energize the film some way or another. The tone is often just turgid ... Yet, for all its problems, the film is often sincere, often earnest ... Youll find yourself rooting for the filmmakers in spite of yourself, and, more to the point, in spite of the mistakes theyve made." 
 Pump Up the Volume, but still feels like a shot in the arm and is full of irreverent energy." He added, "Despite an ineffectual subplot about the heros absent father, there are some good satirical riffs here on adult hypocrisies (with Robert Downey Jr. especially good as the beleaguered, alcoholic school principal), a few echoes of the underrated Mumford (film)|Mumford, and lots of high spirits." 

Darrell Hartmann of the New York Sun said, "John Polls rebellious-teen comedy falls well below the high bar set by recent genre hits Juno (film)|Juno and Superbad (film)|Superbad. An anything-goes kookiness pervades the first half, but the film then takes a trite turn that only serves to highlight its unlikely premise." 

David Balzer of Toronto Life, rating it three out of five stars, called it "a cool trip down teen dramedy lane, but one senses the film could be a lot smarter. Bartlett’s drug selling, it turns out, is not the main subject of the movie; messed-up people are, and this causes Charlie Bartlett to lean on psychobabble about disaffection that it initially tries so hard to mock. The film’s use of Cat Stevens’ anthem from Harold and Maude, “If You Want to Sing Out, Sing Out,” encapsulates its problems; instead of acting as a wry expression of Bartlett’s dark philosophy, the song becomes the kind of pat message of self-empowerment that drives teens to Prozac in the first place." 

==Home media==
   fullscreen (with optional commentary by Jon Poll and Gustin Nash) or anamorphic widescreen (with optional commentary by Poll, Anton Yelchin, and Kat Dennings) format. It has audio tracks and subtitles in English and Spanish. Bonus features include Restroom Confessionals and a music video by Spiral Beach.

==References==
 
 

==External links==
 
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 