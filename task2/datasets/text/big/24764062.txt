Miss Malini
{{Infobox film
| name = Miss Malini
| image = Miss Malini.jpg
| caption  = A still from the film
| director = Kothamangalam Subbu
| writer =  R. K. Narayan 
| starring = Pushpavalli Kothamangalam Subbu Sundari Bai Javar Seetharaman Gemini Ganesan
| producer = Gemini Studios
| music = Saluri Rajeswara Rao Parur S. Anantharaman
| distributor =
| cinematography = 
| editing = 
| released =  1947 
| runtime =  
| country = India Tamil
| budget =
}}
 Tamil social print of the film is known to exist presently, so it is considered it a lost film. 

==Plot==
Life is a constant struggle for Malini, a poor young woman with an ailing father. She reluctantly accepts stage actor-friend Sundari’s suggestion to go on stage, and joins her theatre group, Kala Mandhiram. Success smiles on her and she soon becomes an idol of the masses. Sampath, a suave swindler, befriends Malini and takes control of her life. He persuades her to start her own theatre company. A puppet in his hands, Malini severs ties with those who have helped her in the past such as Sundari. Soon she is in debt and back at the bottom of the ladder. Sampath abandons her. Sundari and others come to her rescue and Malini goes back to Kala Mandhiram and begins her life anew, sadder but certainly wiser.

==Cast==
* Pushpavalli as Malini
* Kothamangalam Subbu as Sampath
* Sundari Bai as Sundari
* Javar Seetharaman
* Gemini Ganesan (credited on-screen as ‘R.G.’, for Ramaswamy Ganesan) 

==Production==
Produced by Gemini Studios, the film featured Pushpavalli in the title role and Sundari Bai as Malinis friend. Kothamangalam Subbu wrote and directed the film and appeared as Sampath, a cheat, in a portrayal that was then considered "exceptional" in South Indian cinema and his performance was praised for being "suave villainy".
 Tamil and Telugu cinema — Gemini Ganesan". 

==Soundtrack==
Saluri Rajeswara Rao and Carnatic musician Parur S. Anantharaman composed the films score. 

;Tracklisting 
{| class="wikitable"
|-
! Song
|-
| Jegame Oru Chiththira Salai
|-
| Kulikkanum Kalikkanum
|-
| Mylapore Vakkeelathu
|-
| Paadum Radio
|-
| Senthamizh Nadu Sezhiththidave
|-
| Sree Saraswadhae
|}
==Reception==
Randor Guy, a critic from The Hindu described it as "one of the finest social satires to have been made in South Indian cinema" and noted that it "did not receive the appreciation it so richly deserved" because it was ahead of its time. He however concluded that the film would be "remembered for its music, Gemini Ganesan’s debut and as a film that appealed to the intellectuals in South India." The film was not successful at the box-office, but was well received by intellectuals.   

==References==
 

==External links==
*  

 

 
 
 
 
 
 