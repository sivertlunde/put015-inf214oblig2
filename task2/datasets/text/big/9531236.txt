18-J
{{Infobox film
| name       = 18-j
| image      = 18Jposter.jpg
| caption    = Theatrical release poster
| producer   = Fernando Blanco Cecilia Bossi Diego Dubcovsky Noemí Fuhrer Marcelo Pavan Jorge Rocca Pablo Rovito Luis A. Sartor Fernando Sokolowicz Guillermo Szelske Juan Vera
| narrator    = Norma Aleandro
| distributor = INCAA
| released    =  
| runtime     = 100 minutes
| country     = Argentina
| language    = Spanish
| budget      =
}} July 18, 1994 bombing of the AMIA Building in Buenos Aires, where 86 people were killed and 300 others wounded. The perpetrators were never caught. AMIA is the Argentine Israelite Mutual Association, a charity, and the attack is considered the largest single incident of terrorism against Jews since World War II.

The film is a tribute to the memory of the victims from the Argentine cinema community, producers, and directors, and released on the ten-year anniversary of the attack. Each director portrays his or her memory and impressions of the event in their own way. The ten short films are shown in a sequence. The picture was co-produced by the INCAA (Instituto Nacional de Cine y Artes Audiovisuales) and ten of the most active Argentine producers. The film opens with a brief introduction by Argentine actor Norma Aleandro.

==Background==
Because the picture involved numerous explosions it was shot on an Argentine Army base. The short films capture Argentine life at several social levels, both Jewish and non-Jewish. The ten directors use a variety of cinematic styles: the sentimental, hard-hitting, and the abstract.

==Distribution==
The film opened in Argentina on August 19, 2004. In Brazil it opened October 4, 2004, at the Rio de Janeiro International Film Festival. The film has been screened at various film festivals, including: the Palm Beach Jewish Film Festival, Palm Beach, Florida; the Washington Jewish Film Festival, Washington DC; the Haifa International Film Festival, Haifa, Israel; the San Francisco Jewish Film Festival, San Francisco; the Hong Kong Jewish Film Festival, Hong Kong; the Boston Jewish Film Festival, Boston; and others.

==Production companies==
Each of the following Argentine film production companies chose one director for the film: 
* BD Cine (Daniel Burman)
* Audiovisual Production Center from the University of Tres de Febrero (Adrián Caetano)
* Cinetauro (Lucía Cedrón)
* Patagonik Film Group (Alejandro Doria)
* Zarlek Producciones (Alberto Lecchi)
* Kaos (Marcelo Schapces)
* Guacamole Films (Carlos Sorín)
* Aleph Media (Juan Bautista Stagnaro)
* Pol-Ka (Adrián Suar)
* Cinema Digital (Mauricio Wainrot)

==Ten shorts==

===(1) 86===
* Synopsis: The pain of the victims showing the effects of a bomb on objects: flowers, books, a birthday cake; as recalled by a blinded man.
* Directed by: Israel Adrián Caetano
* Written by: Roberto Gispert
* Editing: Israel Adrián Caetano
* Cinematography: Julián Apezteguia

----

===(2) La Memoria===
aka The Memory Handel aria.
* Directed by: Carlos Sorín
* Written by:
* Editing: Alejandro Alem, Alejandro Parysow Hugo Colace

----

===(3) Untitled=== Once neighborhood, where the attack took place.
* Directed by: Daniel Burman
* Written by: Daniel Burman
* Editing: Alejandro Brodersohn
* Cinematography: Alejandro Giuliani

----

===(4) La Llamada===
aka The Call
* Synopsis: Set in Quebrada de Humahuaca, a village far from Buenos Aires, a woman experiences anguish. Her son lives in Buenos Aires and she is desperately waiting to hear from him.
* Directed by: Alberto Lecchi
* Written by: Santiago Giralt
* Editing: Alejandro Alem Hugo Colace

----

===(5) La vergüenza===
aka Shame
* Synopsis: A survivor of the attack, Ana, revives her memories while preparing in her mind the testimony she will present in Court. Also covers the political cover-up following the bombing.
* Directed by: Alejandro Doria
* Written by: Alejandro Doria and Aída Bortnik
* Editing: Sergio Zóttola
* Cinematography: Willi Behnisch

----

===(6) Mitzvah===
* Synopsis: Centered on an elderly Jewish couple as they prepare for a bar mitzvah. Their daughter lives in Israel and they plan to visit her soon.
* Directed by: Lucía Cedrón
* Written by: Victoria Galardi
* Editing: Rosario Suárez
* Cinematography: José Luis García (cinematographer)|José Luis García

----

===(7) La comedia divina===
aka The Divine Comedy
* Synopsis: The pain of the victims.
* Directed by: Juan Bautista Stagnaro
* Written by: Juan Bautista Stagnaro
* Editing: Alejandro Alem
* Cinematography: Andrés Mazzon

----

===(8) Lacrimosa===
aka The Tearful
* Synopsis: In purely artistic fashion four dancers from the San Martín Theater perform.
* Directed by: Mauricio Wainrot Carlos Gallardo and Mauricio Wainrot
* Editing: Marcela Sáenz
* Cinematography: Abel Peñalba

----

===(9) La ira de Dios===
aka The Wrath of God
* Synopsis: The pain of the victims.
* Directed by: Marcelo Schapces
* Written by: Paula Romero Levit and Pablo Fidalgo
* Editing: Miguel Schverdfinger
* Cinematography: José Guerra

----

===(10) Sorprensa===
aka Surprise
* Synopsis: The pain of the victims, and shows the arbitrariness of terrorism in selecting its victims.
* Directed by: Adrián Suar
* Written by: Josefina Trotta, Sebastián Noejovich, Lucía Victoria Roux, María Laura Meradi, Francisco Sánchez Azcárate, Damián Fraticelli and Mariano Vera
* Editing: Alejandro Alem, Alejandro Parysow
* Cinematography: Miguel Abal

==Reception==

===Critical response===
Jonathan Holland, film critic for Variety (magazine)|Variety, liked the various stories and how they provide a "perceptive overview of Argentinian society". He wrote, "This worthy and affecting homage features styles from abstract to hard-hitting. Political fest sidebars are the pics likeliest destination, along with arthouses in territories with a cultural interest in the tragedy... As a byproduct, pic reps an often perceptive overview of Argentine life at several social levels. Though many of the dead were Jewish, most helmers have significantly emphasized the universality of the tragedy rather than focusing on Jewish victimization." 

==References==
 

==External links==
*  
*   at the cinenacional.com  
*   film review at Cineismo by Silvina Rival  
*  

 
 
 
 
 
 
 
 
 