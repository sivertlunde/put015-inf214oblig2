Navy Born
{{Infobox film
| name           = Navy Born
| image          =
| image_size     =
| caption        =
| director       = Nate Watt
| producer       = Nat Levine (producer) Burt Kelly (associate producer)
| writer         = Mildred Cram (story) Albert DeMond (screenplay) and Olive Cooper (screenplay) Marcus Goodrich (screenplay) Claire Church (additional dialogue)
| narrator       =
| starring       = See below
| music          = Ernest Miller
| editing        = Richard Fantl
| distributor    =
| released       = 2 June 1936
| runtime        =
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Navy Born is a 1936 American film directed by Nate Watt.

The film is also known as Mariners of the Sky (American original script title).

== Plot summary ==
 

The aircraft carrier appearing at the start is the USS Lexington (CV-2).

== Cast ==
*William Gargan as Lt. Red Furness
*Claire Dodd as Bernice Farrington
*Douglas Fowley as Lt. Steve Bassett George Irving as Adm. Kingston
*Dorothy Tree as Daphne Roth William Newell as Lt. Bill Lyons
*Addison Randall as Lt. Tex Jones
*Georgia Caine as Aunt Minnie
*Claudia Coleman as Mrs. Farrington Douglas Wood as Mr. Strickland
*Paul Fix as Joe Vezie
*Hooper Atchley as Seeley

== Soundtrack ==
 

== External links ==
* 
* 
 

 
 
 
 
 
 
 


 