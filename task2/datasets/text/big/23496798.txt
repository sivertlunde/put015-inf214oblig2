The Sultan's Wife
 
{{Infobox film
| name           = The Sultans Wife
| image          = 
| caption        = 
| director       = Clarence G. Badger
| producer       = Mack Sennett (Keystone Studios)
| writer         = 
| starring       = Gloria Swanson
| music          = 
| cinematography = 
| editing        = 
| distributor    = Triangle Film Corporation
| released       =  
| runtime        = 18 minutes; 2 reels
| country        = United States  Silent English intertitles
| budget         = 
}}
 silent comedy film directed by Clarence G. Badger and starring Gloria Swanson.   

==Plot==
Gloria follows sailor boyfriend Bobby to India. After she is kidnapped by the sultan, who wants her for his harem, Bobby must come to the rescue. 

==Cast==
* Gloria Swanson as Gloria
* Bobby Vernon as Bobby Joseph Callahan
* Teddy the Dog
* Gonda Durand
* Phyllis Haver
* Roxana McGowan
* Vera Steadman
* Edith Valk
* Blanche Payson as Harem Matron (uncredited)

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 

 