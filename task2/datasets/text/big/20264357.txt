Blue Gap Boy'z
{{Infobox film
| name           = Blue Gap Boyz
| image          = 
| caption        = Theatrical release poster
| director       = Holt Hamilton
| producer       = Travis Hamilton Rebekah Hamilton José Montoya
| writer         = Holt Hamilton Ernest "Ernie" David Tsosie III Vincent Craig James Bilagody Beau Benally Sallie Glaner Braden Horst Aschmann Jana The Plateros
| cinematography = Holt Hamilton
| editing        = Derek Natzke
| storyboard     = 
| distributor    = Better World Distribution
| released       =  
| runtime        = 90 minutes
| country        = United States Navajo
| website        = 
}}
Blue Gap Boyz is a 2008 independent comedy film written and directed by Holt Hamilton. Blue Gap Boyz was filmed primarily in Phoenix, Arizona, as well as on the Navajo Nation in Blue Gap, Arizona.

==Cast== Ernest "Ernie" David Tsosie III as James Nez
* Vincent Craig as Jessie Nez
* James Bilagody as Jodie Nez
* Beau Benally as Frankie B.
* Sallie Glaner Braden as Sara
* Horst W. Aschmann as Rolf
* Guilty Wilson as Mystic Love
* Jana Mashonee as herself
* Deshava Apachee as Mr. Big Extra Tough Guy

==See also==
* Turquoise Rose
* Pete & Cleo Tuscarora singer and actress
* James and Ernie, a Navajo comedy duo

==External links==
*  

 
 
 
 
 
 
 
 
 


 