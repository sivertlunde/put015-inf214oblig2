Blue Sky Bones
{{Infobox film
| name           = Blue Sky Bones
| image          = Blue Sky Bones poster.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Cui Jian
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       =
| music          = 
| cinematography = 
| editing        = 
| studio         = Antaeus Film Group Beijing Dongxi Music Art Production Co.,Ltd Liaoning Ougu Digital Technology Co.,Ltd Shenzhenshi Zhongshu Wenchuang Capital Management Co.,Ltd Dongyue International Media（Beijing）Co.,Ltd Defang Wealth management（Dalian）Co.,Ltd
| distributor    = 
| released       =   
| runtime        = 100 minutes
| country        = China
| language       = Mandarin
| budget         = 
| gross          = chinese yuan|¥3.02 million (China)
}}

Blue Sky Bones ( ), also known as The Blue Bone, is a 2013 Chinese music film directed by Cui Jian. It was released in China on October 17, 2014. 

==Cast==
*Zhao Youliang
*Ni Hongjie
*Yin Fang
*Huang Xuan
*Huang Huan
*Guo Jinglin
*Lei Han
*Tao Ye
*Mao Amin

==Reception==
By October 20, the film had earned chinese yuan|¥3.02 million at the Chinese box office.   

==References==
 

==External links==
* 

  

 

 