McHale's Navy Joins the Air Force
 
 Joe Flynn and Tim Conway are the leads for this sequel to first movie made in 1964 also named McHales Navy (1964 film)|McHales Navy. Most of the movie is based on their two characters particularly Ensign Parker. Series star Ernest Borgnine was unavailable due to a scheduling conflict while he appeared in the 1965 movie The Flight of the Phoenix (1965 film)|The Flight of the Phoenix. However, in a Cinema Retro interview, Borgnine said the producer Edward Montagne wanted to make the film cheaply, without him and would not show him the script.  Carl Ballantine also doesnt appear in the movie and the PT-73 crew is not seem in large portions of the film. The movie, which also features Ted Bessell and Gavin MacLeod, was directed by series producer Edward Montagne.
 Joe Flynn) is sent to a staff meeting in Brisbane, Australia and is forced to use the PT-73 to get there after Fuji (Yoshio Yoda) sabotages Lt. Carpenters PT-116 (Bob Hastings). While in Brisbane, Binghamton orders the PT-73 crew to remain on board, but they switch uniforms with Russian crewmen on the adjoining docked ship so they can leave the ship without being noticed. In a mix up Parker (Tim Conway) switches uniforms with Lt. Harkness (Ted Bessell) who then is mistakenly arrested by the Russian NKGB and put on the Russian ship, but he later escapes and spends the entire movie trying to get back. Parker impersonates Lt. Harkness with Binghamton helping him until Harkness comes back. Because of Harknesss reputation as a lady killer women are drawn to the very shy Parker. Parker also has to avoid General Harkness (Tom Tully) who is Lt Harknesss father for fear of being found out. In the meantime Harkness (or rather Parker) is promoted three times to Lt. Colonel by unwittingly scoring three military victories. Even after being found out, Parker is by then too big a hero and the military brass decide to sort of cover up the whole mix up of Parker pretending to be someone else. At the end of the film Parker does an impressive impersonation of then President Franklin D. Roosevelt.

The movie is titled "McHale Navy Joins the Air Force" and similar designations on the TV show are U.S.A.F. Since the movie is situated "Somewhere in the South Pacific 1943" this would be historically inaccurate. Until 1947 the Air Force was part of the U.S. Army and was known as the U.S. Army Air Forces (U.S.A.A.F) beginning in 1941.

The movie was released to VHS on March 31, 1998. 

==Cast== Joe Flynn	 ...	
Captain Wally Binghamton 
Tim Conway	 ...	
Ensign Charles Parker 
Bob Hastings	 ...	
Lt. Elroy Carpenter 
Gary Vinson	 ...	
George Christopher 
Billy Sands	 ...	
Motor Machinist Mate Harrison Bell 
Edson Stroll	 ...	
Virgil Edwards 
Bobby Wright	 ...	
Willy Moss 
Yoshio Yoda	 ...	
Takeo Fuji Fugiwara 
Gavin MacLeod	 ...	
Seaman Joseph Haines 
Tom Tully	 ...	
Gen. Harkness 
Susan Silo	 ...	
Cpl. Smitty Smith 
Henry Beckman	 ...	
Col. Platt 
Ted Bessell	 ...	
Lt. Wilbur Harkness 
Jean Hale	 ...	
Sgt. Madge Collins 
Cliff Norton	 ...	
Maj. Bill Grady 
Jacques Aubuchon   ...
Dimitri

==References==
 

==External links==
*   in the Internet Movie Database

 
 
 
 
 
 
 
 