The Newburgh Sting
{{multiple issues|
 
 
}}

{{Infobox film
| name = The Newburgh Sting
| image =The Newburgh Sting poster.jpg
| alt = The Newburgh Sting
| caption = The Newburgh Sting (2014) documentary film
| director = Kate Davis, David Heilbroner
| producer = Kate Davis, David Heilbroner
| writer =David Heilbroner
| starring = 
| music =
| cinematography =
| editing =
| studio = Q-Ball Productions
| distributor = 
| released =  
| runtime = 80 minutes
| country = United States
| language = English
| budget =
| gross = 
}}
The Newburgh Sting (2014) is a documentary film about the Federal Bureau of Investigations sting operation on four Muslim men involved in the 2009 Bronx terrorism plot. Beginning in 2008, an FBI informant, Shaheed Hussain, recorded hours of conversations with the men who were ultimately arrested and convicted of planting three non-functional bombs next to two synagogues in Riverdale, Bronx and for planning to use Stinger missiles to shoot down United States military cargo planes near Newburgh (town), New York|Newburgh, New York. The point of view of the documentary is that it was later brought to light that the plot with the four men who were coaxed into participating was created by the FBI. The men argue that this was a case of entrapment. In April, 2014, the film was shown at the Tribeca Film Festival.                        

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 


 