The Margin (film)
{{Infobox film
 | name =  The Margin
 | image =  The Margin (film).jpg
 | caption =
 | director =  Walerian Borowczyk
 | writer = 
 | starring = Sylvia Kristel Joe Dallesandro
 | music = 
 | cinematography =  Bernard Daillencourt	
 | editing =     Louisette Hautecoeur 
 | producer =   
 | released =  
 | country = 
 | language = French
 }} The Margin by André Pieyre de Mandiargues.

It had admissions of 725,502 in France. 

==Cast==

* Sylvia Kristel : Diana
* Joe Dallesandro : Sigismond
* André Falcon : Antonin Pons 
* Mireille Audibert : Sergine 
* Louise Chevalier : Féline
* Denis Manuel : Le moustachu 
* Dominique Marcas 

==References==
 
==External links==
*  at IMDB

 
 
 