Ek Mutho Chabi
{{Infobox film
| name = Ek Mutho Chabi
| image = Ek mutho chhabi.jpg
| caption =
| director = Arghyakamal Mitra Partha Sen Indranil Roychowdhury Prabhat Roy Anjan Dutt Kaushik Ganguly
| writer =
| starring =
| producer = Roopa Ganguly
| distributor =
| cinematography =
| editing =
| released = September 2005
| country = India
| budget =
| gross =
| runtime = Bengali
| music = Debjyoti Mishra Neel Dutt
}}
 Bengali feature-film released in September 2005. As the name suggests, the movie does not tell one continuous story over 90 minutes; rather its a compilation of six different stories combined together, all of around 20–25 minutes duration. All directed by different directors. The movie is a kind of experiment with short stories after success of telefilm industry.Argyakamal Mitra’s Janmodin, Partha Sen’s Pakshiraj, Indranil Roychowdhury’s Tapan Babu, Prabhat Roy’s Ragun Babur Galpo, Anjan Dutt’s        Tarpor Bhalobasha and Kaushik Ganguly’s ProgressReport make up the package of six. 
{{cite news
|url=http://www.telegraphindia.com/1051126/asp/calcutta/story_5521576.asp
|title=Film and fashion fiesta
|publisher=www.telegraphindia.com
|accessdate=2008-10-24
|last=Jawed
|first=Zeeshan
|location=Calcutta, India
|date=2005-11-26
}}
 

== Movie Synopsis ==

=== Story 1 - Janmodin ===
*Cast: Rajatava Dutta, Sreelekha Mitra, Dilip Roy
*Director: Argyakamal Mitra
*Plot: Rajatava and Srilekha live in a flat in Kolkata. They have moved to their new flat which they bought with bank loan. Rajat is a personal secretary to a wealthy businessman. One day Rajat got a call from bank that his EMI cheque has bounced because of lack of funds in his bank account. He was called to make the payment immediately else his flat will be confiscated by the bank. The only way he can manage the money is to arrange for a woman for his boss on his bosss birthday. Circumstances leads to the fact that the woman is none other than Srilekha, Rajatavas wife, unaware to him. The film ends at a juncture where Rajat smells his wifes perfume and knows that it smells exactly to the perfume smell that was coming out from his bosss woman.

=== Story 2 - Pakshiraj ===
*Cast: June Maliah, Arunima Ghosh
*Director: Partha Sen
*Plot: Arunima is a tea-stall owner. Came to Kolkata from a village, she is illiterate, her tea-cum-shop is just beside a corporate building. She has a dream to live like a queen. The inspiration she bought from June, an employee of the company, whose office is in the said building. Arunima fantasizes about June. Thinks that one day even she will be coming to office and do all the other stuffs that her didi (June) is doing now. For that she drops 2-3 coins in a tin-can everyday, thinking that one day this assembling together will make her rich. But one night her tin-can got stolen. She suspected a male-friend of her has done that. She went to him, screamed for her money and in anger busted a brick on his head. The last scene shows that June got scared when she saw police arresting Arunima.

=== Story 3 - Tapan Babu ===
*Cast: Supriya Devi, Arindam Sil
*Director: Indranil Roychowdhury
*Plot: A mediocre doctor in Kolkata being unsatisfied with his marriage life, keeps a secret affair with a woman, who is the maid of Supriya Devi. and the doctor is a family friend of Supriya devi, Thus their (between the doctor and the servant) relationship gets stronger. One day, supriya shows the doctor an old necklace of her. Her estimation says that necklace is worth Rs 40 lacs that time. Somehow the doctor managed a photograph of the necklace and ordered a Jewellers shop to make a verbatim copy of that of aluminium. One day, he went to Supriya Devis bungalow for a formal visit. No one was there. She gave her extra overdose of Morfin, a drug used for insomnia patients. But overdose of it can lead to the patients death. So the owner of the necklace perished away. The doctor replaced the original necklace with the duplicate. He also issued the death certificate of her. But somehow, the maid-servant came to know about his misdeed and started blackmailing him. The story ended here. After that he found himself to change his character.

=== Story 4 – Ragun Babur Galpo ===
*Cast: Biplab Chatterjee, Indrani Halder
*Director: Prabhat Ray
*Plot: Biplab, better known as Ragunbabu among the local boys, is always angry with everyone, starting from his wife, son and the boys of the locality. He scolds his son every time for not studying. Biplabs wife Indrani tries to defend her son. Indrani tolerates her husband for all his scolding, his misdeed, his forgetfulness, everything. But one day, even she succumbs to the pressure and gets into a quarrel with her husband. Biplab slaps her in front of her son. His son gets angry and throws his cricket bat all of a sudden to his dad, beats his dad randomly. Indrani asks for a taxi and takes her husband to a nursing home. The last scene shows the boy committing his mistake to everyone, saying that he has beaten his dad. But he is not at all sorry for that.

=== Story 5 - Tarpor Bhalobasha ===
*Cast: Rupa Ganguly, Bikram Ghosh, Kunal Mitra, Pallavi Chatterjee
*Director: Anjan Dutt
*Plot: Rupa and Kunal both works for the film industry. They live together in a flat. Rupa is more famous than her boyfriend. She thinks that her boyfriend is jealous about her. All of a sudden, she met a car accident one day while going to the studio with Bikram and lost her one leg. Consequently, she lost all her jobs. Kunal prospers. This story tells about some forceful but untrusted relationship and then about the repentance of the couple.

=== Story 6 - ProgressReport ===
*Cast: Sudipa Basu, Kaushik Sen, Santu Mukherjee
*Director: Kaushik Ganguly
*Plot: Two girls from same family appears for the West Bengal Higher Secondary Exam the same year. One of them (Payel) has made a hype that she might made it into the top 10 in the merit-list. She wears a spectacle of high-power. Her sister (Ritu) is not so meritorious. Rather she aspires for being a film-star. Thats why even their teacher Kaushik pays more attention to Payel rather than to her sister. In the exam hall, circumstances leads to an uncanny situation. Payels specs gets broken. Her spare specs is not with her, its at home. Their parents will come at lunch break and their phone dead. So no way to bring the other specs from home. Ritu sacrifices her exam for her elder sister, runs to home and brings back the other specs for Payel. This shows the level of commitment for her sister.

== See also == Jackpot
* Waarish

== References ==
 

 

 
 
 
 
 