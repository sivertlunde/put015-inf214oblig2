Children Full of Life
{{Infobox film
| name           = Children Full of Life
| image          = 
| image_size     =
| caption        =
| director       = Noboru Kaetsu
| producer       = Naoaki Hinohara Junichi Nogami
| writer         = 
| narrator       = 
| starring       = Toshiro Kanamori
| music          = 
| cinematography = 
| editing        = 
| studio         = NHK
| distributor    = Gravitas Ventures
| released       =  
| runtime        = 59 minutes (Japanese),     48&nbsp;min (English) 
| country        = Japan, Canada
| language       = Japanese, English
| gross          = 
| preceded_by    =
| followed_by    =
}}
  is a 2003 Japanese documentary film directed by Noboru Kaetsu that follows a 4th grade teacher in Japan.

==Synopsis==
Children Full of Life follows the life and teaching of Mr. Kanamori, a 4th grade primary school teacher in Kanazawa, Japan. He gives his students lessons on what he considers to be the most important principles in life: to be happy and to care for other people.      His lessons include discussion around teamwork, community, the importance of openness, how to cope, and the harm caused by bullying.   

==Awards==
Children Full of Life was awarded the Global Television Grand Prize at the 25th Anniversary Banff Television Festival, the festival’s highest honour and the first time Japan took the top prize.   

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 


 