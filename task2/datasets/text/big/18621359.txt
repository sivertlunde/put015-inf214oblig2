Jay and Seth versus the Apocalypse
{{Infobox film
| name           = Jay and Seth versus the Apocalypse
| image          = 
| image_size     = 
| alt            = 
| caption        = 
| director       = Jason Stone
| producer       = Rachel Robb Kondrath Jason Stone
| writer         = Evan Goldberg Jason Stone 
| starring       = Seth Rogen Jay Baruchel
| music          = Mark Petrie
| cinematography = Jay Visit
| editing        = Neel Upadhye
| studio         = Catastrophic Films
| distributor    = Mandate Films Sony Pictures Home Entertainment
| released       =   (trailer)   (full short)
| runtime        = 9:51
| country        =
| language       = English
| budget         = 
| gross          = 
}} short comedy movie trailer for YouTube but conceived as a 9-minute short, stars actors Seth Rogen and Jay Baruchel. It was adapted into a feature film, This Is the End, and was finally released in full on its full-length counterparts Blu-ray release.

Jay and Seth versus the Apocalypse follows the two friends and actors who have shut themselves in their apartment and argue over their predicament during some unspecified end-of-the-world event.   

==Production== The Orphanage Scott Stewart. After the trailer they put up on YouTube in June 2007 got over 50,000 hits in the first two weeks, Stone and Goldberg began shopping the project around. Variety (magazine)|Variety reports that several production companies vied for the rights for production. 
 Craig Robinson, and Danny McBride.  The plot centers around this group, having been at a party at Francos house when the apocalypse commences. Many celebrity cameos occur during the film, including Jason Segel, Emma Watson, Michael Cera, Rihanna, Aziz Ansari and Paul Rudd. On October 1, 2013, Jay and Seth Versus the Apocalypse was finally released in full as a Blu-ray exclusive feature on the home video release on This Is the End.

==Reception== trailer because when it was first posted, the filmmakers intended to publicly screen the short at festivals.  However, when the film was picked up by Mandate, the short was shelved from festival consideration. This frustrated many fans, who expected to see a feature closer to the 2007 release of the trailer online.  The trailer had over 200,000 views in its first 14 months. 

==References==
 

==External links==
*  
*  

 
 
 
 