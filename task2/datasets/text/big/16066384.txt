Sparrow (2008 film)
{{Infobox film name           = Sparrow image          = Sparrowmovieposter.jpg caption        = Promotional poster director       = Johnnie To producer       = Johnnie To writer  Milkyway Creative Team starring       = Kelly Lin Simon Yam Lam Ka-Tung Law Wing-Cheong Kenneth Cheung music          = Xavier Jamaux Fred Avril cinematography = Cheng Siu-Keung editing        = David M. Richardson studio         = Universe Entertainment Milkyway Image distributor  China Film Group released       =   runtime        = 87 minutes country        = Hong Kong language       = Cantonese budget         = gross          = $3,547,972 
}}
Sparrow ( ) is a 2008 Hong Kong caper film produced and directed by Johnnie To.

Sparrow remained in pre-production for three years from 2005 to 2008, with To shooting the film in between other projects.  The film was selected in competition at the 58th Berlin International Film Festival, premiering during the festival in February 2008. It was released in Hong Kong on 19 June 2008.

==Plot==
 
Kei (Simon Yam) is the experienced leader of a team of Pickpocketing|pickpockets.(Pickpockets are also known as "Sparrows" in Hong Kong slang). He enjoys a carefree lifestyle taking photos with his vintage Rolleiflex. One day a dashing beauty, Chun-Lei (Kelly Lin), suddenly appears in Keis viewfinder. Kei is mesmerized. Every member of his team has an encounter with her.... But behind Chun-Leis attractive facade lies a mysterious past and a mission to set herself free.

==Cast==
{| class="wikitable"
|- bgcolor="#CCCCCC"
! Role || Actor
|-
| Chung Chun-Lei || Kelly Lin
|-
| Kei || Simon Yam
|-
| Bo || Lam Ka-Tung
|-
| Sak || Law Wing-Cheong
|-
| Mac || Kenneth Cheung
|-
| Fu Kim-Tong || Lo Hoi-pang
|-
| Lung || Lam Suet
|}

==Production==
Director Johnnie To shot the film in Hong Kong over a three-year period. In an interview, To said he and his crew would shoot every three or four months between projects. 

==Awards and nominations==

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 2px #aaa solid; border-collapse: collapse; font-size: 90%;"
|- bgcolor="#CCCCCC" align="center"
! colspan="4" style="background: LightSteelBlue;" | Awards
|- bgcolor="#CCCCCC" align="center"
! Award
! Category
! Name
! Outcome
|- 45th Golden Horse Film Awards Best Original Film Score Xavier Jamaux, Fred Avril Nominated
|- Best Cinematography Cheng Siu-Keung Won
|-
|rowspan=5|28th Hong Kong Film Awards Best Director Johnnie To Nominated
|- Best Actor Simon Yam Nominated
|- Best Cinematography Cheng Siu-Keung Nominated
|- Best Film Editing David M. Richardson Nominated
|- Best Original Film Score Xavier Jamaux, Fred Avril Nominated
|-
|rowspan=1|3rd Asian Film Awards Best Cinematography Cheng Siu-Keung Nominated
|- 2008 Asia Pacific Screen Awards Asia Pacific Best Feature Film
| Nominated
|- Asia Pacific Best Achievement in Directing Johnnie To Nominated
|- Best Performance by an Actor Simon Yam Nominated
|- Best Achievement in Cinematography Cheng Siu-Keung Nominated
|}

{| class="wikitable" Berlin International Film Festival
|-
! width="33"| Year
! width="100"| Award
! width="200"| Result
|-
| align="center" rowspan="1"| 2008
| align="center" rowspan="1"| Golden Bear
| align="center" rowspan="1"| In Competition
|}

==See also==
* Johnnie To filmography

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 

 

 
 
 
 
 
 