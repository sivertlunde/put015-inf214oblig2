A Daughter of the Wolf
{{Infobox film
| name           = A Daughter of the Wolf
| image          = A Daughter of the Wolf (1919) - 1.jpg
| alt            = 
| caption        = Film still
| director       = Irvin Willat
| producer       = Jesse L. Lasky
| screenplay     = Marion Fairfax Hugh Pendexter
| starring       = Lila Lee Elliott Dexter Clarence Geldart Raymond Hatton Richard Wayne Minnie Devereaux
| music          =  	
| cinematography = J.O. Taylor 
| editor         =	
| studio         = Famous Players-Lasky Corporation
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = Silent (English intertitles)
| budget         = 
| gross          =
}}
 silent Drama drama film directed by Irvin Willat and written by Marion Fairfax and Hugh Pendexter. The film stars Lila Lee, Elliott Dexter, Clarence Geldart, Raymond Hatton, Richard Wayne, and Minnie Devereaux. The film was released on June 22, 1919, by Paramount Pictures.  

==Plot==
 

==Cast==
*Lila Lee as Annette Ainsworth
*Elliott Dexter as Robert Draly
*Clarence Geldart as Wolf Ainsworth
*Raymond Hatton as Doc
*Richard Wayne as Sgt. Tim Roper
*Minnie Devereaux as Mrs. Beavertail
*Jim Mason as Roe 
*Jack Herbert as Jacques
*Marcia Manon as Jean Burroughs
*James Neill as Judge Burroughs
*Clyde Benson as M. Pomgret
*Roy Diem as Clerk
*Charles Ogle as Doc 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 