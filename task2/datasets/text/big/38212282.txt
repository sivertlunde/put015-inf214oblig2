The Newest Pledge
{{Infobox film
| name           = The Newest Pledge
| image          = The Newest Pledge Movieposter.jpg
| image_size     = 
| alt            = 
| caption        = 
| director       = Jason Michael Brescia
| producer       = Bob Burton  Renee De Ponte  Bryson Pintard
| writer         = Jason Michael Brescia
| starring       = Rob Steinhauser   Joseph Booton Mindy Sterling Jason Mewes Kevin Nash
| distributor    = Lionsgate
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = English
}}
The Newest Pledge is a 2012 comedy film written and directed by Jason Michael Brescia. The film is about the hard-partying Omega fraternity and their struggles to raise a baby that they find on their doorstep.  The film was released in North America by Lionsgate.

==Plot==
The morning after a night of binge drinking and patriotic partying, the Omega fraternity awaken to find a baby on their doorstep.  After little deliberation, the fraternity members, lead by their president Hodges, decide to raise the baby and name him Kegston. In a short period of time raising a baby runs the fraternities finances dry, leading Hodges to seek advice from former Omega president, and current professor at the university Professor Street (played by Jason Mewes).

On the verge of losing their baby due to bankruptcy, Pledge, a freshman fraternity member, suggests that the Omegas throw a party to raise funds. Despite Phi Tau Tau, the rival fraternities best attempt to ruin the party, the Omegas raise the funds necessary to keep Kegston only to be hit with the news that no non-students can live in Greek housing. This leads Hodges, along with his fraternity brothers Night Train, 39, and Renato to visit President Dumervile (played by Mindy Sterling). Despite schmoozing the President to the best of their abilities, President Dumervile informs the Omegas that its too late in the year for her to make any changes, and that "rules are rules". 

Days later, with hope dwindling, Night Train, the schools championship running back gets drafted into the NFL and decides to use his money to start a scholarship for babies. The school and President Dumervile then allow Kegston to be a student at the university, foiling Phi Tau Taus second attempt to rid the Omegas of their newest member.

Later in the semester, at the Omegas Spring Formal, Hodges declares Pledge the candidate to succeed him as President of the Omegas. The Omegas hadnt lost a Greek Board election in almost a century, but Rico (played by Joseph Booton), the leader of the Phi Tau Taus, creates a campaign slandering the Omegas, stating that they kidnapped the baby. Rico and Pledges presidential contest culminates at the Greek Board debate where Pledge presents a paternity test to President Dumervile stating who the real father of the baby is.

==Release==
The Newest Pledge premiered on October 30, 2010 at the New Beverly Cinema in Los Angeles, California. Before its wide release on August 28, 2012, the film screened at the Houston Comedy Film Festival, the Wet Your Pants Comedy Film Festival in Indianapolis, and Indie Fest USA in Garden Grove, California. The film had several private or limited engagement theatrical screenings including a New York City premiere at Cinema Village, a screening at the Dodge College of Film and Media Arts where several key crew members graduated, and a screening at the art house theater in Jason Michael Brescias home town of Malverne, New York. 

The film was released in 16x9 widescreen 1.78:1 and Dolby 5.1 digital audio by Lionsgate DVD. The DVD also includes commentary with writer director Jason Michael Brescia, and actors Kevin Myers, Rob Steinhauser, and Beth Lepley. 

==References==
 

 

 
 