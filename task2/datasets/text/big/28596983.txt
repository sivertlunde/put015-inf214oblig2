Matching Jack
 
 
{{Infobox film
| name           = Matching Jack
| image          = MatchingJack2010Poster.jpg
| image size     = 
| alt            = 
| caption        = Australian Poster
| director       = Nadia Tass
| producer       =  
| screenplay     =  
| story          = Lynne Renew
| starring       =  
| music          = Paul Grabowsky
| cinematography = David Parker Mark Warner
| studio         = Cascade Films
| distributor    = 20th Century Fox
| released       =  
| runtime        = 103 minutes
| country        = Australia
| language       = English
| budget         = 
| gross          = $258,011
}} David Parker, based on an unfilmed script by Renew entitled Love and Mortar. 

== Plot ==
Life seems idyllic for Marisa (Jacinda Barrett) and her son, Jack (Tom Russell), until a poor performance at a school soccer match ends with Jack in hospital and Marisa trying to find her husband, David (Richard Roxburgh), who is interstate at a conference. In fact, David is planning to leave Marisa for his current mistress (Yvonne Strahovski), with his phone off and not a care in the world.
 
Jack is diagnosed with Leukaemia and the only possibility of a cure is if David has had a child from one of his many flings who could be a bone marrow donor. Marisa looks back through his diaries, figures when he could have been having affairs, and goes out door knocking. Unsuspecting women face a desperate mother as Marisa searches high and low for possibilities and the full scale of David’s infidelity is revealed.
 
Meanwhile, Jack befriends Finn (Kodi Smit-McPhee), a young Irish boy in the next bed. He has been travelling the world with his father, Connor (James Nesbitt). Initial disdain turns to mutual respect as both Marisa and Connor find their own ways to deal with their respective sons’ illnesses.

==Production==

Matching Jack was produced by Tass and Parkers studio Cascade Films, in association with the film investment agencies Film Victoria and Screen Australia. It was filmed on location in Melbourne in 2009, and premiered at the 2010 Melbourne International Film Festival. 20th Century Fox distributed it to 185 national screens in August 2010, the widest Fox release of an Australian film since 2008s Australia (2008 film)|Australia.

The film won Best Picture, Best Director and Best Screenplay at the 2011 Milan International Film Festival. Matching Jack was also honoured with Cannes Cinephile Prix De Jury Bel Age at the 2011 Cinephiles festival at Cannes in May 2011.

Over recent months, the film has screened at: Palm Beach International Film Festival, Cleveland International Film Festival, Belfast Film Festival, Internationales Filmwochenende Wurzburg (Germany), Tiburon International Film Festival and Newport Beach Film Festival. Recently it was also screened in out of competition section of the 10th Pune International Film Festival.

== Reviews ==
It holds a score of 69% on Rotten Tomatoes. 

Leonard Maltin: “A deeply emotional film, clearly made with tender loving care. Kodi Smit-McPhee gives an utterly remarkable performance in a cast full of fine actors.”

David Stratton, The Australian: “Wonderfully acted and scripted… Smit-McPhee, particularly, brings grace, dignity and child-like optimism to his character, and he is heart-breakingly impressive as Finn.”

David Tiley, Screenhub: “A terrific, passionate, sobby, craftful heartgrabber, a grown up film about a vast and honest emotional arc.”

Jim Schembri, The Sydney Morning Herald: "Its a no-holds-bar emotional slam dunk as a young mother (superbly played by Jacinda Barrett) desperately searches for a bone marrow match for her son from among the illegitimate children fathered by her promiscuous husband (Richard Roxburgh). A beautiful, moving, unashamedly melodramatic film that rouses tears and joy in equal measure."

== Cast ==
*Jacinda Barrett as Marissa
*James Nesbitt as Connor, an Irish sailor Tom Russel l as Jack, Marissas son
*Kodi Smit-McPhee as Finn, Connors son, who befriends Jack
*Richard Roxburgh as David, Marissas unfaithful husband
*Julia Harari as Young Doctor
*Yvonne Strahovski as Veronica

== Release ==
Matching Jack opened at number eight at the Australian box office on its opening weekend, taking $258,011.

== References ==

 
 
*Murphy, Damien (14 August 2010). " ". The Sydney Morning Herald (Fairfax Media). Retrieved on 28 August 2010.
*Mathieson, Craig (19 August 2010). " ". The Sydney Morning Herald (Fairfax Media). Retrieved on 28 August 2010.
*Gonzalez, Miguel (24 August 2010). " ". Encore Magazine (Focal Attractions). Retrieved on 28 August 2010.
*Gonzalez, Miguel (24 August 2010). " ". Encore Magazine (Focal Attractions). Retrieved on 28 August 2010.
*Industry News (18 May 2011). " ". Encore Magazine (Focal Attractions). Retrieved on 30 May 2011.
*NIXCO (25 May 2011). " ". Inside Film. Retrieved on 30 May 2011.
*Newsflash (12 May 2011)  " ". Urban Cinephile. Retrieved on 5 June 2011.
*Schembri, Jim (19 August 2010). " ". The Sydney Morning Herald. Retrieved on 5 June 2011.
 

== External links ==
* 
* 

 

 
 
 
 
 
 
 