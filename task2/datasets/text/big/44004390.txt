Anchu Sundarikal
{{Infobox film 
| name           = Anchu Sundarikal
| image          =
| caption        =
| director       = M. Krishnan Nair (director)|M. Krishnan Nair
| producer       = Kasim
| writer         = Jagathy NK Achari
| screenplay     = Jagathy NK Achari
| starring       = Prem Nazir Jayabharathi Adoor Bhasi Pappukutty Bhagavathar
| music          = MS Baburaj
| cinematography =
| editing        =
| studio         = Sony Pictures
| distributor    = Sony Pictures
| released       =  
| country        = India Malayalam
}}
 1968 Cinema Indian Malayalam Malayalam film, directed by M. Krishnan Nair (director)|M. Krishnan Nair and produced by Kasim. The film stars Prem Nazir, Jayabharathi, Adoor Bhasi and Pappukutty Bhagavathar in lead roles. The film had musical score by MS Baburaj.   

==Cast==
*Prem Nazir
*Jayabharathi
*Adoor Bhasi
*Pappukutty Bhagavathar
*T. S. Muthaiah GK Pillai
*Pankajavalli
*Paravoor Bharathan
*Rani Chandra
*Udayachandrika

==Soundtrack==
The music was composed by MS Baburaj and lyrics was written by Yusufali Kechery. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Amrithum Thenum || K. J. Yesudas || Yusufali Kechery || 
|-
| 2 || Anju Sundarikal || K. J. Yesudas || Yusufali Kechery || 
|-
| 3 || Maayaajaalacheppinullile || K. J. Yesudas || Yusufali Kechery || 
|-
| 4 || Paattupadi || P Susheela || Yusufali Kechery || 
|-
| 5 || Pathinezhilethiya || S Janaki || Yusufali Kechery || 
|-
| 6 || Sindooracheppilum Kandilla || K. J. Yesudas || Yusufali Kechery || 
|}

==References==
 

==External links==
*  

 
 
 

 