Matzor
{{Infobox film
| name           = Matzor
| image          = 
| caption        = 
| director       = Gilberto Tofano
| producer       = Yaackov Agmon Michael J. Kagan
| writer         = Dahn Ben Amotz Gilberto Tofano
| starring       = Eran Agmon
| music          = 
| cinematography = David Gurfinkel
| editing        = Danny Shick
| distributor    = 
| released       =  
| runtime        = 100 minutes
| country        = Israel
| language       = Hebrew
| budget         = 
}}  Best Foreign Language Film at the 42nd Academy Awards, but was not accepted as a nominee. 

==Cast==
* Eran Agmon
* Gila Almagor as Tamar
* Yael Aviv
* Dahn Ben Amotz
* Yehoram Gaon as Eli
* Omna Goldstein
* Anni Grian
* Micha Kagan
* Raviv Oren
* Amir Orion
* Baruch Sadeh
* Uri Sharoni

==See also==
* List of submissions to the 42nd Academy Awards for Best Foreign Language Film
* List of Israeli submissions for the Academy Award for Best Foreign Language Film

==References==
 

== External links ==
*  
*  on The New York Times

 
 
 
 
 
 
 
 