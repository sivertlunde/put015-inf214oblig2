Cheaper by the Dozen (2003 film)
{{Infobox film
| name           = Cheaper by the Dozen
| image          = Cheaper by the Dozen 2003 film poster.jpg
| caption        = Promotional poster
| director       = Shawn Levy
| producer       = Robert Simonds Michael Barnathan Ben Myron Joel Cohen Alec Sokolow
| story          = Craig Titley
| based on       =  
| narrator       = Bonnie Hunt
| starring       = Steve Martin
| music          = Christophe Beck
| cinematography = Jonathan Brown
| editing        = George Folsey, Jr. Robert Simonds Productions
| distributor    = 20th Century Fox
| released       =  
| runtime        = 98 minutes
| country        = United States
| language       = English
| budget         = $40 million
| gross          = $190,212,113 
| rating         = PG
}} autobiographical book original film adaptation, although it is mentioned that the mothers maiden name is Gilbreth. The film was directed by Shawn Levy, narrated by Bonnie Hunt, and starring Steve Martin. The film was released on December 25, 2003 by 20th Century Fox, ultimately grossing just over $190 million worldwide.   

==Plot==
Tom Baker (Martin) is a football coach at a small rural college in Midland, Indiana, where he raised twelve children, and his wife, Kate (Hunt), has written about her story in a book and hopes to send it to her friend to publish it. At the films beginning, Tom unexpectedly receives an offer from his old friend and football teammate, Shake McGuire (Jenkins), to coach at his alma mater in Evanston, Illinois. Tom accepts the offer, and he and Kate begin making their plans on moving to Evanston. All twelve children find out and demand the move be put to a vote, even though Tom says it would not have any power. Tom loses the vote, even after he and Kate join, but goes ahead with the move anyway, claiming there will be more money and that they will be a "happier and stronger family". However, the atmosphere at the Bakers new house is tense, and the situation at school for the children is even worse.

When her book is ready for publication, Kate is recommended to do a national book tour to promote it. Tom thinks that he will handle everything around the Bakers house while Kate is away, so he decides to hire the Bakers oldest child, Nora (Perabo), and her boyfriend, Hank (Kutcher), to manage the children. When Nora and Hank arrive at the Bakers house to manage them, the children plans to make Hank the target of their prank by soaking his underwear in meat and assisting the Bakers pet dog, Gunner, to attack him by chewing his bottom, prompting him to refuse to assist in baby-sitting. As a result, Nora is angry at the children and later drives off with Hank, while Tom berates the children for their prank and cuts off their allowance within a month. Moments after Kate departs for her national book tour, Tom realizes that he cannot handle the children on his own after a chaotic night. In reply to this revelation, Tom tries to hire a housekeeper, but nobody is willing to work with a family as large as the Bakers, so Tom decides to bring the football players from work into the Bakers house for game practicing in the living room to prepare for the Saturday night football game as the children perform chores and their household games. However, the children start causing trouble at school, in which Kyle and Nigel hurt their kindergarten teacher, and the others stand-up against a group of bullies by fighting them. Because of these incidents, Tom grounds them all and forbids them from going to Dylans birthday party, although they already have presents for him. Without Tom noticing them, the children sneak out of the house to give Dylan his presents. While discussing the plays, Tom overhears party-goers screaming from Dylans house as one of the gifts turns out to be a snake. In all of the commotion, Dylan gets injured by Tom and is later sent to the hospital. Tom is forced to cancel the game after Shake advises him not to bring the players in the Bakers house or the children in the department again. 
 Chicago to Midland. Reuniting with the rest of their family, the Bakers begin to address their issues with each other. Tom ultimately resigns from his position at his alma mater and settles for a less-time consuming job. The film ends when Kate narrates that the Bakers are becoming close.

==Cast==
===The Bakers===
 
* Steve Martin as Tom Baker, patriarch of the family
* Bonnie Hunt as Kate Gilbreth Baker, matriarch of the family and narrator of the film
* Piper Perabo as Nora Baker, 22 years old
* Tom Welling as Charlie Baker, 18 years old
* Hilary Duff as Lorraine Baker, 16 years old
* Kevin Schmidt as Henry Baker, 14 years old  Jacob Smith as Jake Baker, 12 years old
* Alyson Stoner as Sarah Baker, 11 years old
* Forrest Landis as Mark Baker, 10 years old
* Liliana Mumy and Morgan York as Jessica and Kim Baker, 8-year-old fraternal twins
* Blake Woodruff as Mike Baker, 6 years old
* Brent and Shane Kinsman as Kyle and Nigel Baker, 5-year-old identical twins
 

===Others===
 
* Steven Anthony Lawrence as Dylan Shenk
* Paula Marshall and Alan Ruck as Tina and Bill Shenk, Dylans parents
* Richard Jenkins as Shake McGuire, Toms old friend and football teammate
* Ashton Kutcher as Hank, Noras boyfriend
* Tiffany Dupont as Beth, Charlies girlfriend
* Cody Linley as Quinn
* Jared Padalecki as an unnamed bully 
* Joel McCrary as Gil
* Dax Shepard as a member of the camera crew
* Regis Philbin as himself
* Kelly Ripa as herself
* Frank Welker as Gunner (voice), the Bakers pet dog
* Wayne Knight as Electrician (uncredited)
 

==Sequel==
A sequel, Cheaper by the Dozen 2, was released in the United States on December 21, 2005.

==Soundtrack==
 
{{Tracklist
| headline        = "Cheaper by the Dozen" Soundtrack
| title1          = Im Just a Kid
| length1         = 1:24
| title2          = Help! (song)|Help!
| length2         = 1:12 In Too Deep
| length3         = 2:46
| title4          = What Christmas Should Be
| length4         = 3:10
| title5          = Life Is a Highway
| length5         = 
| title6          = These Are Days
| length6         = 
| title7          = Rockin Robin (song)|Rockin Robin
| length7         = 
| title8          = Rockin Around the Christmas Tree
| length8         = 
}}

Other compositions used in the movie are "Classical Gas" by Mason Williams and Carl Orffs "O Fortuna", among others.

==Reception==

===Awards and nominations===
Cheaper by the Dozen has been nominated and won several awards. The following is a list of these:
{| class="wikitable"
|-
! Result
! Type of award
! Category
! Year
|-
| Nominated
| Teen Choice Award
| Choice movie blush (Hilary Duff), Choice breakout movie star (male) (Tom Welling), Choice movie liplock 2004 
|-
| Won Young artist award
| Best Ensemble Cast
|-
| Nominated
| Best performance in feature film (Alyson Stoner and Forrest Landis)
|}

===Critical reception===
The film received mixed to negative reviews from film critics, with 23% of critics giving a positive review and an average score of 4.6 out of 10 according  , went on to gross $190,212,113 worldwide.  Ashton Kutcher was nominated for a Golden Raspberry Award for Worst Actor for his performance in this, and two other films, but lost to Ben Affleck for his performances in Daredevil (film)|Daredevil, Gigli and Paycheck (film)|Paycheck.   

==Home media==
The film was released on VHS and DVD on April 6, 2004.

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 