Envelope (film)
 

{{Infobox film
| name           = Envelope
| caption        =
| image          = 
| director       = Aleksey Nuzhny
| producer       = 
| writer         = Aleksey Nuzhny
| starring       = Kevin Spacey Alanna Ubach David Meunier Anthony Skordi Cody Caira Mike Dirksen
| cinematography = 
| released       =   
| country        = USA
| language       = English
}} Yevgeny Petrov. Actor Kevin Spacey supports young Russian director Aleksy Nuzhny by starring in his film. 

== Cast ==
{| class="wikitable"
|-
! Cast
! Role
|-
| Kevin Spacey || Evgeniy Petrov
|-
| Alanna Ubach || Tanya
|-
| David Meunier || Lieutenant
|-
| Anthony Skordi || Pavel
|-
| Cody Caira || Lieutenants Assistant
|-
| Mike Dirksen || Lieutenants Assistant
|}

== Story ==
Evgeniy Petrov, The Soviet Union writer and journalist has an unusual hobby. He is writing fake letters to his imaginary friends since he was 6. Every time he chooses different fake name for his addressee. The envelopes come back, but beautified with colorful foreign stamps and postmarks. Throughout his lifetime, he has accumulated letters from many countries around the world. One time he actually receives a letter from New Zealand from someone who claims to know him.

==References==
Notes
 

== External links ==
*  
 