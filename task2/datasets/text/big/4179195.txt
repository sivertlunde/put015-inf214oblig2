Overnight
 
 
 
 documentary by Tony Montana and Mark Brian Smith. The film details the rise and fall of filmmaker and musician Troy Duffy, the writer-director of The Boondock Saints, and was filmed at his request.   

Duffy is presented as a victim of his own ego, and as the film progresses and his fortunes fade Duffy becomes increasingly abusive to his friends, relatives and business partners. According to co-director Montana, "Troy seemed to revel in the attention of Hollywoods lights and our cameras. Only three times during the production did he ask not to be filmed. It was on those occasions that he threatened us."

== Plot ==
Overnight is the story of Troy Duffy, a Boston bartender and aspiring screenwriter who is also a musician in a band called "The Brood," along with his brother Taylor.

At the beginning of the film, Troy is riding high: his script for The Boondock Saints has just been picked up by Miramax chief Harvey Weinstein for $300,000 and Duffy has been taken on by the William Morris Agency. Duffy, who had never made a movie or attended film school, will also direct the $15 million film. Moreover, his band will produce the soundtrack and get a recording contract from Maverick Records, and Weinstein will buy the bar Duffy works in and hire Duffy to run it.

Duffy initially enjoys his new success, entertaining celebrities in his bar, dining at hotel restaurants, and moving into a production office where he holds teleconferences with producers. The movie deal, however, quickly turns sour, partially due to Duffys own arrogant behavior. Believing himself to be the next power-player in Hollywood, Duffy insults actors who are in consideration for Boondock (including Ethan Hawke, Keanu Reeves,  and Kenneth Branagh, whose name Duffy repeatedly mispronounces before simply calling him "cunt"). Duffy threatens to leave William Morris in favor of a rival agency, and generally alienates both Weinstein and his own production team through his abrasive behavior. Ultimately, Duffy receives word of rumors that Weinstein, one of the most powerful producers in Hollywood, has had him blacklisted; Miramax puts the film in Turnaround (filmmaking)|turnaround, conference calls are refused, and soon Duffy is without any movie industry contract at all. 

Duffys musical efforts are equally ill-fated. Famed guitarist Jeff "Skunk" Baxter expresses interest in producing The Brood, singling out lead vocalist Taylor Duffy for particular praise. However, in recording sessions with Baxter and producer Ron St. Germain, Troy Duffy is filmed behaving dictatorially and refusing any advice that contradicts his opinion. Baxter also expresses concern about the bands heavy alcohol consumption. Renaming themselves The Boondock Saints, their debut CD sells only 690 copies, and the band is dropped from their contract.

Duffy is finally able to obtain independent financing for the film, although it totals less than half of its original production budget. Boondock Saints is promoted at the Cannes Film Festival, but all the major American distribution companies pass on the film. The film manages to receive a limited release in five cities, but performs poorly and is pulled after a week, and is released on DVD and VHS. Although positive reviews of the movie begin to spread via word-of-mouth and the film becomes a financial success, Duffys contract with Miramax stipulates he cannot profit from the films video sales.

==Reception==
Overnight received positive reviews. On Rotten Tomatoes, it has a 78% fresh rating, based on 77 reviews. The consensus says, "This absorbing but wince-inducing documentary is a cautionary tale about the costs of hubris in the world of indie film."  Roger Ebert gave Overnight 3-out-of-4 stars, writing, "  family, we sense during one scene, has been listening to this blowhard for a lifetime, and although they are happy to share his success, theyre sort of waiting to see how he screws up. ... So are we." 

Comedian Adam Carolla mentions Overnight in his book In 50 Years Well All Be Chicks (2010), describing the documentary as a cautionary tale, as well, and suggesting that Troy Duffys behavior is an example of how to not behave upon attaining success. In November 2011, Carolla released a podcast interview with Troy Duffy. Admitting that he was not on his best behavior during the time the documentary was filmed, Duffy nonetheless insists that Overnight was unfairly slanted to make him appear like a "boorish asshole." *  

==References==
 

== External links ==
* 
* 

 
 
 
 
 
 