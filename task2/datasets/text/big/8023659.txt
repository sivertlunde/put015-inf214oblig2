Unaccompanied Minors
{{Infobox Film
| name = Unaccompanied Minors
| image = Unaccompanied minors poster.jpg
| image_size = 215px
| alt = 
| caption = Promotional poster
| director = Paul Feig
| producer = Lauren Shuler Donner Michael Aguilar
| writer = Jacob Meszaros Mya Stark Brett Kelly Gia Mantegna Quinn Shephard Michael Andrews
| cinematography = Christopher Baffa
| editing = George Folsey, Jr. Brad E. Wilhite
| studio = Village Roadshow Pictures Donners Company Warner Bros. Pictures
| released = December 8, 2006
| runtime = 90 minutes
| country = United States
| language = English
| budget = $26 million
| gross = $21,994,214
}} Christmas comedy Brett Kelly, Gia Mantegna, and Quinn Shephard in supporting lead roles. It is based on a true story by Susan Burton first told on the public radio show This American Life under the title "In the Event of an Emergency, Put Your Sister in an Upright Position". 

==Plot== Gina Mantegna), Brett Kelly).

Charlie, Spencer, Beef, Grace and Donna sneak out, and proceed to enjoy themselves around the airport. When they are caught by the airport security guards and returned to the UM room, they find that the other minors, Katherine included, have been sent to a lodge down the road, and that the grouchy head of passenger relations, Oliver Porter (Lewis Black) — whose trip to Hawaii is among the canceled flights — intends for the kids to spend Christmas Eve in the UM room. Knowing that it will break Katherines faith in Santa Claus if she does not receive a present by the next morning, Spencer asks the others to help him get a present for his sister in return for a plan to escape.

With Spencers plan, the minors give Zach Van Bourke (Wilmer Valderrama), the friendly clerk watching them, the slip, but Mr. Porter grows desperate to get the kids back, and sends all the airport guards to search for them. After Donna and Grace get into a fight, Spencer decides that theyre going to have to work together as a team, and Beef leaves to go and get a Christmas tree, along the way, he reflects on how his step-father, Ernie, hoped to make him stronger by saying men are made, not born. Meanwhile, Spencer and Katherines father tries to drive to the airport in his biodiesel fueled car, but it eventually breaks down at a gas station. Fortunately, the owner lets him borrow a Hummer.

The minors head to a thinly secured exit in the back of the airport, letting a dog loose to distract the guards. While they hide from Mr. Porter in the baggage claim, Charlie, who is hiding in a suitcase, gets placed on a conveyor transport. Donna goes to save him, putting herself on a wild ride. Spencer and Grace follow them to the unclaimed baggage warehouse, where they find many wonderful presents, including a set of walkie-talkies, and a doll for Katherine.

However, they are seen dancing to Lee Morgans The Sidewinder on security cameras, and Mr. Porter and the guards chase the minors through the warehouse. Using a canoe, the minors take Zach captive and sled to the lodge while pursued by the guards, and manage to elude Mr. Porter long enough to find Katherine asleep in the lobby, and place the doll in her arms, after which Mr. Porter recaptures them. While running around the lodge, Grace has to remove her contact lenses and switch to glasses, which made her look like a dork in the past (but not to Spencer). With their mission completed, the minors go back to the airport with Mr. Porter quietly. They are placed under surveillance in separate rooms.

Using the walkie-talkies, the minors tamper with the security cameras and escape through air ducts. They find the Christmas decorations Mr. Porter confiscated, and Beef returns with a huge Christmas tree that he traded his prized Aquaman action figure for. With Zachs help, the minors decorate the airport, and take items from the unclaimed baggage warehouse to use as presents for the rest of the stranded passengers. Mr. Porter finds Spencer to admit defeat and reveals that the source of his unhappiness is he never really gets to spend time with his family during the holidays. Spencer inspires some holiday spirit in the man with some friendly words and the gift of a snow globe. On Christmas morning, Mr. Porter dresses up as Santa Claus to hand out presents to the passengers, Spencer and Katherines father arrives to pick up his children, and Beef tells a girl about his trek to find a Christmas tree, Charlie and Donna exchange phone numbers and share a kiss, and Grace accepts Spencers invitation to spend Christmas with him and his family.

==Cast==
*Dyllan Christopher as Spencer Davenport
*Lewis Black as Oliver Porter Gina Mantegna as Grace Conrad
*Tyler James Williams as Charles "Charlie" Goldfinch
*Quinn Shephard as Donna Malone
*Wilmer Valderrama as Zach Van Bourke Brett Kelly as Timothy "Beef" Wellington
*Dominique Saldaña as Katherine Davenport
*Paget Brewster as Valerie Davenport
*Rob Corddry as Samuel "Sam" Davenport
*Wayne Federman as Airport Attendant
*Mario Lopez as Part-Time Substitute minors watcher
*Jessica Walter as Cindi
*Rob Riggle as Head Guard Hoffman
*David Koechner as Ernie
*Tony Hale as Alan Davies
*Cedric Yarbrough as Melvin "Mel" Goldfinch
*Kristen Wiig as Carole Malone
*Al Roker as Himself
*Teri Garr as Aunt Judy (uncredited)
with a cameo by The Kids in the Halls Kevin McDonald, Bruce McCulloch, and Mark McKinney as Mr. Porters men

==Deleted scenes==
Original opening: At a mall, Spencer meets a girl from his school named Ashley. They talk and are about to kiss, but this is revealed to be a daydream. His mom interrupts him, and tells him to take his sister to see Santa. The daydream sequence was commonly seen in TV promotions for the film, despite not making the final cut.

Snotty Beef: During the sledding sequence, there is a shot of Beef wiping his nose on his coat.

Extended dance sequence: Alternate takes of Charlie dancing.

Peach Puma: Upon arriving at the Peach Festival, Beef enters a jar of preserves in the canning competition, but is bitten by a puma when he tries to ride the teacups. This scene is often included in the television cut to round out a full two-hour time block.

Dr. Charlie and Donna Dance: While Donna and Charlie are dancing, Charlie asks Donna if she can teach him to be tough like her. Donna explains that, in truth, shes angry about her life and about her parents. Charlie encourages Donna to try to be happy more often. Donna snorts and tells to Charlie to shut up and dance.

Mistletoe: When Grace sees that Spencer looks nervous about something, he tells her that hes just going to go check on the others. Grace looks up and sees a mistletoe. Realizing that Spencer was a little nervous about kissing her, Grace snorts and calls him a dork.

==Soundtrack==
Tyler James Williams performed a song for the film with the same name as the film, but was not used in the film, but in a television spot to promote the film.

==Reception==
Unaccompanied Minors was not an initial success at the box office, having earned only $16,655,224 domestically against its $26 million budget, not counting DVD sales. Its worldwide box office gross was $21,994,214. The film received generally negative to mixed reviews from critics, It has a "Rotten" rating of 30% on Rotten Tomatoes, with the general consensus being: "Unaccompanied Minors, while featuring credible performances by its mostly young cast, is simply a rehash of other, funnier movies" This has a 43 out of 100 on Metacritic, indicating "mixed or average reviews". 

==References==
 

==External links==
 
 
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 