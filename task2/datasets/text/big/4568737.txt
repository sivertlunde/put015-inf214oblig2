Guddi (1971 film)
{{Infobox film
| name = Guddi
| image = Guddi_Movie_Poster.jpg
| caption =Guddi Movie Poster
| director = Hrishikesh Mukherjee
| producer = Hrishikesh Mukherjee
| writer = 
| story = Gulzar
| screenplay = Hrishikesh Mukherjee, Gulzar (lyricist)|Gulzar, D. N. Mukherjee
| starring = Dharmendra Jaya Bhaduri Utpal Dutt A. K. Hangal Asrani Keshto Mukherjee
| music = Vasant Desai Gulzar (Lyrics)
| lyrics = Gulzar
| cinematography = Dwarka Divecha
| released =  
| runtime = 188 minutes 204 minutes  (directors cut, India)  162 minutes  (USA) 
| country = India Hindi
| Budget = 
| preceded_by =
| followed_by =
| awards =
| Gross = 
}}
 Pran gave Tamil as Cinema Paithiyam (1975) starring Jayachitra and Kamal Haasan. 

==Plot==
Kusum (aka Guddi) (Jaya Bachchan) is a spunky and carefree schoolgirl who lives with her father (A. K. Hangal), brother and sister-in-law (Sumita Sanyal). Guddi has a crush on film star Dharmendra, who she regards as a superman who can do no wrong, unable to distinguish between his on screen image and the real person behind the star. 

Nobody knows the extent of her crush until she visits Bombay, where her sister-in-laws brother, Navin (played by Bengali film actor Samit Bhanja), proposes to her, only to be taken aback when Guddi discloses that she is in love with Dharmendra. Navin discusses the matter with his uncle (Utpal Dutt), who decides that the only solution is to make Guddi realise the difference between illusion and reality.

The uncle contacts Dharmendra through a mutual friend. With his help, they show Guddi the difference between the real world and the make believe world of cinema. Exposed for the first time to the grime, the cruel and heartless world behind the glamour of cinema, Guddi realises that nothing is true in the reel world. While her respect for Dharmendra grows, Guddi comes to realise the fact that he is just as human as anyone around and lives with the same fears and insecurities as anyone else.

The film ends with Guddi agreeing to marry Navin.

==Cast==
*Dharmendra  as  Himself
*Jaya Bhaduri  as  Guddi
*Sumita Sanyal  as  Bhabhi
*Utpal Dutt  as  Professor Gupta Samit  as  Navin Hangal  as  Guddis father
*Asrani  as  Kundan
*Keshto Mukherjee  as  Kader Bhai
*Vijay Sharma  as  Kishan
*Lalita Kumari  as  Teacher
*Arti  as  Tara

==Crew==
*Director - Hrishikesh Mukherjee
*Story - Gulzar
*Screenplay - Hrishikesh Mukherjee, Gulzar, D. N. Mukherjee
*Dialogue - Gulzar
*Editor - Das Dhaimade
*Cinematographer - Jaywant Pathare
*Art Director - Ajit Banerjee
*Producers - Hrishikesh Mukherjee, N. C. Sippy
*Executive Producer - Romu N. Sippy
*Production Executives - T. S. Ganesh, K. G. Nair
*Associate Director - Anil Ghosh
*Assistant Directors - Saroj Banerjee, Susheela Kamat, Nitin Mukesh
*Assistant Art Directors - Ibrahim Mamoo, Ram Gopal Sharma
*Assistant Cameramen - Prakash Anandkar, Shashikant Kabre, G. D. Mushtaq
*Assistant Editors - Khan Zaman Khan, Shridhar Mishra, J. F. H. Van der Auwera (color consultant)
*Wardrobe - M. R. Bhutkar, Mohan Pardesi
*Music Assistant - Vasant Achrekar, Sebastian DSouza

==Cameos==
Guddi provides a rare behind-the-scenes look at the movie industry in Bombay during the late 1960s and early 1970s and featured cameos from a lot of the major actors and industry figures at that time including:

* Amitabh Bachchan
* Biswajeet
* Rajesh Khanna
* Vinod Khanna
* Ashok Kumar
* Dilip Kumar
* Hrishikesh Mukherjee
* Naveen Nischol
* Om Prakash Pran
* Shashikala
* Mala Sinha
* Shatrughan Sinha
* Deven Verma

==Soundtrack==
The song "Bole Re Papihara" was listed at #21 on Binaca Geetmala annual list 1972.
{{Track listing
| headline        = Songs
| extra_column    = Singer(s)
| all_lyrics      = Gulzar
| all_music       = Vasant Desai

| title1          = Bole Re Papihara
| extra1          = Vani Jairam
| length1         = 3:35

| title2          = Hari Bin Kaise Jeeun
| extra2          = Vani Jairam
| length2         = 3:40

| title3          = Humko Man Ki Shakti Dena
| extra3          = Vani Jairam & chorus
| length3         = 4:30
}}

==Awards and Nominations==
 
|-
| 1972 Jaya Bhaduri (Only nomination)  
| Filmfare Award for Best Actress
|  
|}

==References==
 

==External links==
* 
*  , rediff.com, 28 August 2006. Retrieved 2 October 2009.

 

 
 
 
 
 
 