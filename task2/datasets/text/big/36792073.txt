San Lazaro (film)
{{Infobox film
| name           = San Lazaro
| image          = San Lazaro Movie Poster.png
| alt            = Official movie poster.
| caption        = Theatrical release poster
| director       = Wincy Aquino Ong
| producer       = Wincy Aquino Ong, Dan Gil and Ramon Bautista
| writer         = Wincy Aquino Ong and Ramon Bautista
| starring       = Ramon Bautista
| music          = Jazz Nicolas and Mikey Amistoso
| cinematography = Dan Gil
| editing        = Wincy Aquino Ong
| distributor    = GMA Records
| released       = July 2011
| runtime        = 98 minutes
| country        = Philippines
| language       = Filipino
| budget         = Php 600,000
}}

San Lazaro is a Philippine horror film written and directed by Wincy Aquino Ong, starring Ramon Bautista, Wincy Aquino Ong, Nicco Manalo, Bianca King, Ely Buendia, Julia Clarete, Earl Ignacio, Kean Cipriano and Allan Forte. http://www.imdb.com/title/tt1837650/  It is produced by Watusi Productions and Ramon Bautista Films.  The film premiered in Cinemalaya 2011 as one of the finalists for the NETPAC (Network for the Promotion of Asian Cinema) Category. 

== Overview ==

=== Production ===

The production was funded through personal funds and deferrals. Principal shooting started in September 2010. The fictional town in the title was shot in Lubao, Pampanga; Antipolo, Rizal; and parts of Quezon City. The opening chapters where the two main characters first meet were shot in various locations around Malate, Manila and Makati City. The final screen cut was delivered on August 2011. 

In April 2012, GMA Records bought the DVD distribution rights to the film. 

=== Plot ===
Sigfried (Wincy Aquino Ong), an unemployed accountant, gets into trouble with his landlord. He needs money to pay for his rent.

Sigfried, in a stroke of luck, gets a call from an estranged childhood friend, Limuel (Ramon Bautista). Limuel tells Sigfried that his brother Biboy (Nicco Manalo) has been exhibiting psychotic behavior after his wife left him. Limuel, knowing that Sigfried’s uncle (Allan Forte) is the renowned Singing Exorcist of San Lazaro, asks Sigfried to accompany him in bringing his brother Biboy to the remote town of San Lazaro, ten hours from the city by car. 

They reach the exorcist just in time, but the possessed Biboy eats the vocal cords of the Singing Exorcist, thereby rendering the exorcist incapable of performing the ritual. In the end, the injured exorcist explains that the spirit can only be defeated by feeding it a dark secret. Limuel tells the spirit that he has been trysting with his brother’s wife.

== Cast ==

*   as Limuel
* Wincy Aquino Ong	as Sigfried
*  	as Biboy
* Bianca King as Cheska
* Ely Buendia as Rex
* Allan Forte as Dante
*   as Berhilyo
* Julia Clarete as Kathy
* Kean Cipriano	 as Carlo Benedicto

== Soundtrack ==
 The Itchyworms Indiana Jones And The Temple of Doom. Nicolas created the moody opening credits music that matched the award-wining animation sequence on the evening before the first day of principal photography. The theme music from Bianca Kings scenes were inspired by the score from the TV movie The Karen Carpenter Story.

== Reception ==

The film garnered positive reviews upon its initial festival screenings. Erwin Romulo of The Philippine Star describes the film as "wickedly fun to watch".  Oggs Cruz of Twitchfilm.com brands the film as "a product of tilted genius".  Renowned critic Philbert Dy writes in Esquire: "There are so many ideas in San Lazaro that it occasionally sags under its own weight. (But) this is a film destined to find a cult, drawing people in with its unique brand of insanity."  Armando dela Cruz of Film Police Reviews sums up the film as, "a euphoria distinct of its own; it is relentlessly bizarre (and sometimes just too much so), but no less bracing." 

== Awards ==

The film won Best Opening Titles for the 2011 Philippine Graphic Design Awards, Motion Graphics Category. http://pgdawards.com/2011_Winners/?portfolio=juanito-quiccs-maiquez  The jurors praised the title sequence as: "art that sketches the Filipino Culture as it establishes the mood for the film itself: creepy and dark, yet humorous." 

== References ==

 

== External links ==
*  
*  

== San Lazaro ==

 

 