Finding Nemo
 
 
 
{{Infobox film
| name           = Finding Nemo
| image          = Finding Nemo.jpg
| alt            =
| caption        = Theatrical release poster
| director       = Andrew Stanton
| producer       = Graham Walters Bob Peterson David Reynolds
| story          = Andrew Stanton
| starring       = {{Plainlist |
* Albert Brooks
* Ellen DeGeneres
* Alexander Gould
* Willem Dafoe  
}}
| music          = Thomas Newman
| cinematography = Sharon Calahan  Jeremy Lasky
| editing        = David Ian Salter
| studio         = Walt Disney Pictures Pixar Animation Studios Buena Vista Pictures
| released       =    
| runtime        = 100 minutes
| country        = United States
| language       = English
| budget         = $94 million 
| gross          = $936.7 million 
}} Pixar Animation clownfish named regal tang named Dory, searches for his abducted son Nemo all the way to Sydney Harbour. Along the way, Marlin learns to take risks and let Nemo take care of himself.
 3D on Best Original second highest-grossing 27th highest-grossing 5th highest-grossing 10 Top 10 lists.  A #Sequel|sequel, Finding Dory, is in production, set to be released on June 17, 2016.   

==Plot==
 
Two ocellaris clownfish, Marlin and his wife Coral, admire their new home in the Great Barrier Reef and their clutch of eggs when a barracuda attacks, knocking Marlin unconscious. He wakes up to find Coral and all but one of the eggs have disappeared. Marlin names this last egg Nemo, a name that Coral liked.
 regal blue tang with Anterograde amnesia|short-term memory loss.  Marlin and Dory meet three sharks&nbsp;– Bruce, Anchor and Chum&nbsp;– who are trying to make friends with fish in an old minefield.  Marlin discovers the divers mask and notices an address written on it. When he argues with Dory and accidentally gives her a nosebleed, the blood scent causes Bruce to enter an uncontrollable feeding frenzy. The pair escape from Bruce but the mask falls into a trench in the deep sea.
 school of jellyfish that sea turtle named Crush, who takes Marlin and Dory on the East Australian Current. Marlin tell the details of his long journey with a group of sea turtles, and his story is spread across the ocean. He also sees how Crush gets on well with his son Squirt.
 puffer fish; ochre starfish; pacific cleaner blacktailed humbug.

The fish learn that Nemo is to be given to Shermans niece, Darla, who killed a fish by constantly shaking its bag. Gill then reveals his plan to escape, jamming the tanks Filter (aquarium)|filter, forcing the dentist to remove the fish to clean it.  The fish would be placed in plastic bags, and then they would roll out the window and into the harbor. After an attempt at the escape goes wrong, a brown pelican, Nigel, brings news of Marlins adventure. Inspired by his fathers determination, Nemo successfully jams the filter, but the dentist installs a new high-tech filter before they can escape.

After leaving the East Australian Current, Marlin and Dory are engulfed by a blue whale. Inside the whales mouth, Dory communicates with the whale, which carries them to Port Jackson and expels them through his blowhole. They meet Nigel, who recognizes Marlin from the stories he has heard, and he takes them to Shermans office. Darla has just arrived and the dentist is handing Nemo to her. Nemo plays dead to save himself as Nigel arrives. Marlin sees Nemo and believes he is dead before Nigel is thrown out. In despair, Marlin leaves Dory and begins to swim home.  Gill then helps Nemo escape into a drain that leads to the ocean.  Dory loses her memory and becomes confused, and meets Nemo, who reached the ocean. Eventually, Dorys memory is restored after reading the word "Sydney" on a drainpipe. She directs Nemo to Marlin and they reunite, but then Dory is caught in a fishing net with a school of grouper. Nemo enters the net and orders the group to swim downward to break the net, enabling them to escape. After returning home, Nemo leaves for school, with Crushs son Squirt, and Marlin, no longer overprotective, proudly watches Nemo swim away with Dory at his side.

At the dentists office, the high-tech filter breaks down and the Tank Gang escapes into the harbor, belatedly realizing they are still confined in plastic bags.
 

 

==Voice cast==
 
* Albert Brooks as Marlin
* Ellen DeGeneres as Dory
* Alexander Gould as Nemo
* Willem Dafoe as Gill
* Brad Garrett as Bloat
* Allison Janney as Peach
* Austin Pendleton as Gurgle
* Stephen Root as Bubbles
* Vicki Lewis as Deb (and her sister, "Flo")
* Joe Ranft as Jacques
* Geoffrey Rush as Nigel moonfish
* Andrew Stanton as Crush Bob Peterson as Mr. Ray
* Barry Humphries as Bruce
* Eric Bana as Anchor
* Bruce Spence as Chum
* Erik Per Sullivan as Sheldon Bill Hunter as Dr. Philip Sherman
* Elizabeth Perkins as Coral
* Rove McManus as a crab
* Nicholas Bird as Squirt
* Jordy Ranft as Tad
* Erica Beck as Pearl
* LuLu Ebeling as Darla
 

==Production==
The inspiration for Nemo sprang from multiple experiences, going back to when director Andrew Stanton was a child, when he loved going to the dentist to see the fish tank, assuming that the fish were from the ocean and wanted to go home. Finding Nemo, 2004 DVD, commentary  In 1992, shortly after his son was born, he and his family took a trip to Six Flags Discovery Kingdom (which was called Marine World at the time).  There, after seeing the shark tube and various exhibits, he felt that the underwater world could be done beautifully in computer animation.  Later, in 1997, he took his son for a walk in the park, but realized that he was over protecting him and lost an opportunity to have a father-son experience that day. 

In an interview with National Geographic (magazine)|National Geographic magazine, he said that the idea for the characters of Marlin and Nemo came from a photograph of two clownfish peeking out of an Sea anemone|anemone:

 

In addition, clownfish are colourful, but do not tend to come out of an anemone often.  For a character who has to go on a dangerous journey, Stanton felt a clownfish was the perfect type of fish for the character.  Pre-production of the film began in early 1997.  Stanton began writing the screenplay during the post-production of A Bugs Life.  As a result, Finding Nemo began production with a complete screenplay, something that co-director Lee Unkrich called "very unusual for an animated film".   The artists took scuba diving lessons to study the coral reef. 

  co-wrote and directed the film.]]
The idea for the initiation sequence came from a story conference between Andrew Stanton and   and Gerald being scruffy and sloppy.  The filmmakers could not find an appropriate scene for them that did not slow the pace of the picture, so Geralds character was minimized. 
 story reel, and assumed they would find an actor later.  When Stantons performance became popular in test screenings, he decided to keep his performance in the film.  He recorded all his dialogue while lying on a sofa in co-director Lee Unkrichs office.  Crushs son Squirt was voiced by Nicholas Bird, the young son of fellow Pixar director Brad Bird.  According to Stanton, the elder Bird was playing a tape recording of his young son around the Pixar studios one day.  Stanton felt the voice was "this generations Thumper (Bambi)|Thumper" and immediately cast Nicholas. 
 Karen Walker on the television show Will & Grace was not her natural speaking voice.  The producers hired her anyway, and then strongly encouraged her to use her Karen Walker voice for the role.  When Mullally refused, she was dismissed.  To ensure that the movements of the fish in the film were believable, the animators took a crash course in fish biology and oceanography.  They visited aquariums, went diving in Hawaii and received in-house lectures from an ichthyologist.  As a result, Pixars animator for Dory, Gini Cruz Santos, integrated "the fish movement, human movement, and facial expressions to make them look and feel like real characters." 31st Annual Annie Awards,  , Accessed June 12, 2014, "... Character Animation... Gini Santos Finding Nemo..."  Oct 23, 2009, Sean Howell, Yahoo! News,  , Accessed June 12, 2014, "...Pixar ... Finding Nemo, she integrates the fish movement, human movement, and facial expressions to make them look and feel like real characters...." 

The film was dedicated to Glenn McQueen, a Pixar animator who died of melanoma in October 2002.    Finding Nemo shares many plot elements with Pierrot the Clownfish, a childrens book published in 2002, but conceived in 1995.  The author, Franck Le Calvez, sued Disney for infringement of his intellectual rights.  The judge ruled against him, citing the color differences between Pierrot and Nemo. 

==Video game==
 
A video game based on the film was released in 2003, for Personal computer|PC, Xbox, PlayStation 2|PS2, GameCube and Game Boy Advance. The goal of the game is to complete different levels under the roles of film protagonists Nemo, Marlin or Dory. It includes cutscenes from the movie, and each clip is based on a level. It was also the last Disney/Pixar game developed by Travellers Tales. Upon release, the game received mixed reviews.       A Game Boy Advance sequel, titled Finding Nemo: The Continuing Adventures, was  released in 2004. 

==Reception==

===Critical response===
The  , which assigns a normalized rating out of 100 top reviews from mainstream critics, calculated a score of 90 out of 100 based on 38 reviews, indicating "universal acclaim." 

  gave the film a positive review, saying "As gorgeous a film as Disneys ever put out, with astonishing qualities of light, movement, surface and color at the service of the best professional imaginations money can buy."    Jeff Strickler of the Star Tribune gave the film a positive review, saying "Proves that even when Pixar is not at the top of its game, it still produces better animation than some of its competitors on their best days."  Gene Seymour of Newsday gave the film three and a half stars out of four, saying "The underwater backdrops take your breath away. No, really. Theyre so lifelike, you almost feel like holding your breath while watching."  Rene Rodriguez of the Miami Herald gave the film four out of four stars, saying "Parental anxiety may not be the kind of stuff childrens films are usually made of, but this perfectly enchanting movie knows how to cater to its kiddie audience without condescending to them." 

Kenneth Turan of the   gave the film three and half stars out of four, saying "Finding Nemo is an undersea treasure. The most gorgeous of all the Pixar films&nbsp;— which include Toy Story 1 and 2, A Bugs Life and Monsters, Inc.&nbsp;—Nemo treats family audiences to a sweet, resonant story and breathtaking visuals. It may lack Monsters, Inc.s clever humor, but kids will identify with the spunky sea fish Nemo, and adults will relate to Marlin, Nemos devoted dad."  Bruce Westbrook of the Houston Chronicle gave the film an A-, saying "Finding Nemo lives up to Pixars high standards for wildly creative visuals, clever comedy, solid characters and an involving story."  Tom Long of The Detroit News gave the film an A-, saying "A simple test of humanity: If you dont laugh aloud while watching it, youve got a battery not a heart." 

Lou Lumenick of the New York Post gave the film four out of four stars, saying "A dazzling, computer-animated fish tale with a funny, touching script and wonderful voice performances that make it an unqualified treat for all ages."  Moira MacDonald of The Seattle Times gave the film four out of four stars, saying "Enchanting; written with an effortless blend of sweetness and silliness, and animated with such rainbow-hued beauty, you may find yourself wanting to freeze-frame it."  Daphne Gordon of the Toronto Star gave the film four out of five stars, saying "One of the strongest releases from Disney in years, thanks to the work of Andrew Stanton, possibly one of the most successful directors youve never heard of."  Ty Burr of The Boston Globe gave the film three and a half stars out of four, saying "Finding Nemo isnt quite up there with the companys finest work -- theres finally a sense of formula setting in -- but its hands down the best family film since Monsters, Inc.."  C.W. Nevius of The San Francisco Chronicle gave the film four out of four stars, saying "The visuals pop, the fish emote and the ocean comes alive. Thats in the first two minutes. After that, they do some really cool stuff."  Ann Hornaday of The Washington Post gave the film a positive review, saying "Finding Nemo will engross kids with its absorbing story, brightly drawn characters and lively action, and grown-ups will be equally entertained by the films subtle humor and the sophistication of its visuals."  David Ansen of Newsweek gave the film a positive review, saying "A visual marvel, every frame packed to the gills with clever details, Finding Nemo is the best big-studio release so far this year." 

Richard Corliss of   gave the film a positive review, saying "The latest flood of wizardry from Pixar, whose productions, from Toy Story onward, have lent an indispensable vigor and wit to the sagging art of mainstream animation."  The 3D re-release prompted a retrospective on the film nine years after its initial release.  Stephen Whitty of the Newark Star-Ledger described it as "a genuinely funny and touching film that, in less than a decade, has established itself as a timeless classic."  On the 3D re-release, Lisa Schwarzbaum of Entertainment Weekly wrote that its emotional power was deepened by "the dimensionality of the oceanic deep" where "the spatial mysteries of watery currents and floating worlds are exactly where 3D explorers were born to boldly go". 

===Box office===
Finding Nemo earned $380,843,261 in North America, and $555,900,000 in other countries, for a worldwide total of $936,743,261.  It is the  .  Worldwide, it was the highest-grossing Pixar film, up until 2010 when Toy Story 3 surpassed it. 

In North America, Finding Nemo set an opening weekend record for an animated feature, making $70,251,710 (first surpassed by Shrek 2) and ended up spending 11 weeks in the top 10 domestically, remaining there until August 14th.  It became the highest-grossing animated film in North America ($339.7 million), outside North America ($528.2 million) and worldwide ($867.9 million), in all three occasions out-grossing The Lion King.  In North America, it was surpassed by both Shrek 2 in 2004, and Toy Story 3 in 2010.   After the re-release of The Lion King in 2011 and after Despicable Me 2  and Frozen (2013 film)|Frozen passed it in 2014, it stands as the fifth highest-grossing animated film in these regions. Outside North America, it stands as the fifth highest-grossing animated film. Worldwide, it now ranks fourth among animated films. 

The film had impressive box office runs in many international markets. In Japan, its highest-grossing market after North America, it grossed ¥11.2 billion ($102.4 million), becoming the highest-grossing foreign animated film in local currency (yen).    It has only been surpassed by Frozen (2013 film)|Frozen (¥12.1 billion).  Following in biggest grosses are the U.K., Ireland and Malta, where it grossed £37.2 million ($67.1 million), France and the Maghreb region ($64.8 million), Germany ($53.9 million) and Spain ($29.5 million). 

====3D re-release====
After the success of the 3D re-release of  .  In total, it earned $41.1 million in the United States, and $31.0 million outside the U.S. 

===Accolades===
 
Finding Nemo won the   for Favorite Movie and Favorite Voice from an Animated Movie (Ellen DeGeneres) and Saturn Award for Best Supporting Actress (Ellen DeGeneres). 

The film was also nominated for two Chicago Film Critics Association Awards for Best Picture and Best Supporting Actress (Ellen DeGeneres), a Golden Globe Award for Best Motion Picture&nbsp;– Musical or Comedy and two MTV Movie Awards for Best Movie and Best Comedic Performance (Ellen DeGeneres). 

In June 2008, the   and Shrek).   

American Film Institute recognition:
* AFIs 100 Years...100 Movies (10th Anniversary Edition)&nbsp;– Nominated 
* AFIs 10 Top 10&nbsp;– #10 Animated film 

===Environmental concerns and consequences=== Australian Tourism Commission (ATC) launched several marketing campaigns in China and the United States to improve tourism in Australia, many of them utilizing Finding Nemo clips.   Queensland used Finding Nemo to draw tourists to promote its state for vacationers.  According to National Geographic, "Ironically, Finding Nemo, a movie about the anguish of a captured clownfish, caused home-aquarium demand for them to triple."   
 introduced species that are harmful to the indigenous environment, a practice that is harming reefs worldwide.  

==Home media==
Finding Nemo was released on DVD and VHS on November 4, 2003.   The DVD release included an original short film, Exploring the Reef, and a 1989 short animated film, Knick Knack.  The film had a home video release on both Blu-ray and Blu-ray 3D on December 4, 2012, with both a 3-disc and a 5-disc set. 

==Soundtrack==
Finding Nemo, the original soundtrack album,  was the first Pixar film not to be scored by  . 

==Theme park attractions== Disneyland Park, The Seas with Nemo & Friends which opened in 2007 at Epcot, Finding Nemo&nbsp;– The Musical which opened in 2007 in Disneys Animal Kingdom, and Crushs Coaster which opened in 2007 at Walt Disney Studios Park.   

===Finding Nemo&nbsp;– The Musical===
 
{{multiple image
| direction = vertical
| image1=
| caption1= Puppets of Dory, Marlin and turtles 
| width1=250
| image2=
| caption2= Puppets of Gill, Peach, Bubbles, Nemo, Bloat and Gurgle 
| width2=250
| header=Finding Nemo&nbsp;– The Musical
| header_align=center
}}

The stage musical Tarzan Rocks! occupied the Theater in the Wild at  -winning Avenue Q composer Robert Lopez and his wife, Kristen Anderson-Lopez, would "combine puppets, dancers, acrobats and animated backdrops" and open in late 2006.  Years later, Anderson-Lopez explained that she had written a compact 15-minute a cappella version of the story of Oedipus; someone at Disney read it and recognized her talent for condensing material, and offered her the opportunity to make a pitch for the Finding Nemo project. 
 Michael Curry, The Lion King, serving as leading puppet and production designer.   

Anderson-Lopez said that the couple agreed to write the adaptation of "one of their favorite movies of all time" after considering "the idea of people coming in   at 4, 5 or 6 and saying, I want to do that....So we want to take it as seriously as we would a Broadway show".    To condense the feature-length film to 30 minutes, she and Lopez focused on a single theme from the movie, the idea that "the worlds dangerous and beautiful". 
 Thea Award for Best Live Show from the Themed Entertainment Association. 

==Sequel==
 
 
In 2005, after disagreements between Disneys Michael Eisner and Pixars Steve Jobs over the distribution of Pixars films, Disney announced that they would be creating a new animation studio, Circle 7 Animation, to make sequels to the seven Disney-owned Pixar films (which consisted of the films released between 1995 and 2006).  The studio had put Toy Story 3 and Monsters University|Monsters, Inc. 2 into development, and had hired screenwriter Laurie Craig to write a draft for Finding Nemo 2.  Circle 7 was subsequently shut down after Robert Iger replaced Eisner as CEO of Disney and arranged the acquisition of Pixar.
 Chicken Little? Everyone calm down. Dont believe everything you read. Nothing to see here now. #skyisnotfalling".  According to the report by The Hollywood Reporter published in August 2012, Ellen DeGeneres was in negotiations to reprise her role of Dory.  In September 2012, Stanton confirmed, saying, "What was immediately on the list was writing a second John Carter (film)|Carter movie.  When that went away, everything slid up.  I know Ill be accused by more sarcastic people that its a reaction to Carter not doing well, but only in its timing, but not in its conceit".  In February 2013, it was confirmed by the press that Albert Brooks would reprise the role of Marlin in the sequel. 

In April 2013, Disney announced the sequel, Finding Dory, confirming that Ellen DeGeneres and Albert Brooks would be reprising their roles as Dory and Marlin, respectively. It was scheduled to be released on November 25, 2015,   but the films ending was revised after Pixar executives viewed Blackfish (film)|Blackfish.   On September 18, 2013, it was announced that the film would be pushed back to a June 17, 2016, release.  Pixars The Good Dinosaur was moved to the November 25, 2015 slot to allow more time for production of the film. 

==See also==
 

==References==
 

==External links==
 
 
*   from Disney
*   from Pixar
*  
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 