Veronica Mars (film)
 
{{Infobox film
| name           = Veronica Mars
| image          = Veronica Mars Film Poster.jpg
| image_size     = 220px
| border         = yes
| alt            = 
| caption        = Theatrical release poster Rob Thomas
| producer       = Rob Thomas Dan Etheridge Danielle Stokdyk
| screenplay     = Rob Thomas Diane Ruggiero
| story          = Rob Thomas
| based on       =  
| starring       = Kristen Bell Jason Dohring Krysten Ritter Ryan Hansen Francis Capra Percy Daggs III Chris Lowell Tina Majorino Enrico Colantoni
| music          = Josh Kramon
| cinematography = Ben Kutchins
| editing        = Daniel Gabbe
| studio         = Warner Bros. Digital  Spondoolie Productions Warner Bros. Pictures 
| released       =  
| runtime        = 107 minutes 
| country        = United States
| language       = English
| budget         = $6 million      
| gross          = $3.5 million   
}} mystery comedy-drama Rob Thomas CW television same name title character. Its executive producers are Joel Silver, Bell, and Jenny Hinkey. Warner Bros. Pictures opened the film in the United States theatrically and on video-on-demand on March 14, 2014.   

==Plot== the shows Veronica Mars has left the fictional town of Neptune, California and moved to New York City, where she is in a stable relationship with Stosh "Piz" Piznarski and has a job offer from the prestigious law firm Truman-Mann and Associates. She is contacted by her ex-boyfriend Logan Echolls, now a Lieutenant in the United States Navy, who has been accused of murdering his girlfriend Carrie Bishop, a fellow Neptune High student who became a successful but self-destructive pop star under the stage name "Bonnie DeVille". He is being bombarded for offers of representation from lawyers, and Veronica agrees to return to Neptune and help Logan find one who will best represent him. She is reunited with her father Keith Mars, Neptunes former sheriff-turned-private investigator, who shows her how corruption and classism is rife under Sheriff Dan Lamb.

Despite her claims that her stay will be brief and she will not get involved, Veronica begins to investigate the circumstances of Carries death. During her investigation, Veronica is dragged to her ten year high school reunion by friends Wallace Fennell and Cindy "Mac" MacKenzie. There, she learns that former outlaw biker Eli "Weevil" Navarro is now a reformed family man. During the reunion, Veronica realizes Carries murder is connected to the death of Carries best friend, Susan Knight, who disappeared off a boat at sea nine years earlier. After Veronicas nemesis Madison Sinclair plays a copy of Veronicas college sex tape with Piz, a fight breaks out. The reunion comes to an abrupt end as Veronica sets the sprinklers off, with Veronica punching out Madison after Madison verbally harasses Veronica further. Veronica attends an after party and speaks with Dick Casablancas, Luke Haldeman and his fiancée Gia Goodman, and Stu "Cobb" Cobbler, all of whom were with Susan and Carrie on the boat the night Susan disappeared. Meanwhile, while driving home from the reunion, Weevil stops to help a driver being harassed by bikers, only to be shot by the driver, a nervous Celeste Kane. The sheriffs department plants a gun so that Celeste can claim self-defense, and Keith agrees to prove Weevils innocence.

Veronica concludes that those on Susans boat nine years ago covered up the circumstances of her death, and that someone killed Carrie because she threatened to confess. Compromising videos of Carrie are posted online and Veronica traces them back to Vinnie Van Lowe, who has been planting spyware on celebrities and selling the footage. Veronica uses Vinnies footage to prove Gia lured Logan out to Carries home the night of her murder, suggesting she and Luke killed Carrie and framed Logan. Lamb blatantly ignores her evidence and refuses to follow up, but unbeknownst to him Veronica records the conversation. Having stayed in Neptune longer than planned, Veronica calls Piz in New York to explain that she cannot return yet, and Piz breaks off their relationship. Truman-Mann rescinds their job offer, which results in an argument between Keith and Veronica about what shes doing with her life.

Keith has a clandestine meeting with Deputy Sacks about Weevils case, but they are attacked by an unknown driver in a truck who slams into Sacks car, killing him and leaving Keith in critical condition. Veronica and Logan sleep together, reaffirming their relationship, and the next day she sends bugged flowers to Gias apartment and calls her, playing recordings of Carries voice to scare Gia into confessing to being the mastermind behind Carries death. Gia panics and calls Cobb, her partner-in-crime. Veronica goes to Gias apartment to confront her, where Gia reveals that Cobb is the mastermind: Susan overdosed, and he took photos of a panicked Carrie, Gia, and Luke dumping Susans body and has been blackmailing them ever since. Veronicas bug broadcasts everything via a radio frequency which Veronica believed to be unused, but which is actually that of a local radio station. Cobb hears their conversation over the radio from his apartment in the building opposite, then shoots and kills Gia through the window before coming after Veronica. She calls the police and lures Cobb down to the basement before beating him unconscious with a golf club.

Logan returns to active duty in the Navy, but promises to come back to Veronica. Cobbs photo and the secret recording of Lamb refusing to investigate Veronicas claims leak online, forcing Lamb to arrest Cobb, with calls to oust Lamb from office. Both Keith and Weevil recover from their injuries, but Weevil returns to the criminal lifestyle he left behind. Veronica takes over her fathers private investigator business with Mac as her assistant, resolved to help fight Neptunes corruption.

==Cast==
 
  Veronica Mars   
* Jason Dohring as Logan Echolls 
* Krysten Ritter as Gia Goodman 
* Ryan Hansen as Dick Casablancas   
* Francis Capra as Eli "Weevil" Navarro 
* Percy Daggs III as Wallace Fennel 
* Chris Lowell as Stosh "Piz" Piznarski 
* Tina Majorino as Cindy "Mac" Mackenzie 
* Enrico Colantoni as Keith Mars 
* Gaby Hoffmann as Ruby Jetson  Sheriff Don Lamb from the series. 
* Brandon Hillock as Deputy Jerry Sacks 
* Martin Starr as Stu "Cobb" Cobbler 
* Ken Marino as Vinnie Van Lowe   
* Max Greenfield as Leo DAmato 
* Amanda Noret as Madison Sinclair 
* Daran Norris as Cliff McCormack  Andrea Estella as Bonnie DeVille/Carrie Bishop, replacing Leighton Meester in the role.   
* Sam Huntington as Luke Haldeman 
* Duane Daniels as Principal Van Clemmons   
* Jonathan Chesner as Corny 
* Kevin Sheridan as Sean Friedrich 
* Lisa Thornhill as Celeste Kane
* Christine Lakin as Susan Knight 
* Jamie Lee Curtis as Gayle Buckley 

;Cameo appearances
* Ryan Lane as Slick Fellow Applicant   
* Ira Glass as himself 
* Harvey Levin as himself
* Justin Long as Drunken Wingman 
* Kyle Bornheimer as Heliskier
* Dax Shepard as Overconfident Club Boy 
* James Franco as himself 
* Eden Sher as Penny  Dave "Gruber" Allen as 60 Year-Old Rocker 
* Alejandro Escovedo as a busker.
 

==Production==

===Development=== Rob Thomas Double Fine Adventure.    Afterwards, the film earned a greenlight from Warner Bros. Digital Distribution. 

The Kickstarter campaign ended on April 13, with 91,585 donors raising $5,702,153. 
 Andrea Estella, of whom Thomas is a fan. 

===Filming=== Arts District of Los Angeles and at Long Beach, California.  Following a test screening in October 2013, Warner Bros. agreed to pay for an additional day of shooting in order to clarify a plot point. As a result, two scenes were shot during the following December. 

===Music=== musical score for Veronica Mars was composed by Josh Kramon, who previously wrote the original background music to the television series. A soundtrack album containing Kramons score was released on March 14, 2014. 

===Soundtrack===
A compilation album of music featured in the film was released digitally by WaterTower Music on March 4, 2014.  The album features the television series original title song "We Used to Be Friends" by The Dandy Warhols, as well as a new acoustic version by Alejandro Escovedo and other songs by Emperor X, Twin Sister, ZZ Ward, Sufjan Stevens, Max Schneider, Mackintosh Braun, Typhoon (American band)|Typhoon, Lou Rawls, and Gregory Alan Isakov. 

{{Track listing
| collapsed       = no
| extra_column    = Artist
| total_length    = 51:18 

| title1          = We Used to Be Friends
| length1         = 3:30
| extra1          = Alejandro Escovedo

| title2          = Go Captain and Pinlighter
| length2         = 4:13
| extra2          = Emperor X

| title3          = Holding My Breath
| length3         = 4:12
| extra3          = Twin Sister

| title4          = All Around and Away We Go
| length4         = 4:36
| extra4          = Twin Sister

| title5          = Criminal
| length5         = 3:51
| extra5          = ZZ Ward featuring Freddie Gibbs
 Chicago
| length6         = 6:05
| extra6          = Sufjan Stevens

| title7          = Stick Up
| length7         = 2:55
| extra7          = Max Schneider

| title8          = Never Give In
| length8         = 3:25
| extra8          = Mackintosh Braun

| title9          = Prosthetic Love
| length9         = 4:03 Typhoon

| title10         = Youll Never Find Another Love Like Mine
| length10        = 4:25
| extra10         = Lou Rawls

| title11         = Second Chances
| length11        = 3:50
| extra11         = Gregory Alan Isakov

| title12         = We Used to Be Friends
| length12        = 3:19
| extra12         = The Dandy Warhols

| title13         = Mug Shot
| note13          = bonus track itunes only
| length13        = 2:54
| extra13         = Max Schneider

}}

==Distribution== wider theatrical six major studios.  The film was released in DVD and Blu-ray Disc|Blu-ray formats on May 6, 2014. 
 world premiere PaleyFest on March 13, 2014. 

===Marketing=== trailer was released on January 2, 2014. 

The revival of the series has also sparked continuations through different mediums. On July 15, 2013, it was announced that Veronica Mars creator/director Thomas and Alloy Entertainment signed a two-book deal with Vintage Books.  Picking up where the film ends, the first book in the deal will be published on March 25, 2014.  In addition to the book series, The CW announced plans for a web series centered on Ryan Hansen as a version of himself trying to make a Dick Casablancas spin-off of Veronica Mars. 

==Reception==

===Box office===
Preliminary box office tracking reports were initially up in the air due to the unprecedented financial nature of Veronica Mars.   The film earned $260,000 from its Thursday night showings (in 95 theaters),  and reached a $1 million 1.25-day total after expanding to 291 theaters on Friday.  
In its opening weekend, the film grossed $1,988,351 in 291 theaters in the United States, ranking #11 at the box office.   Analysts at the time noted that the unusual financial history and distribution of the film made it difficult to interpret these numbers or to compare them to those of other films. {{cite web|first=Scott|last=Mendelson|title=Weekend Box Office: Veronica Mars Earns $2M, Mr. Peabody And Sherman Tops|url=http://www.forbes.com/sites/scottmendelson/2014/03/16/weekend-box-office-veronica-mars-earns-2m-need-for-speed-bombs|date=
March 16, 2014|publisher=Forbes|quote=Again, this is a strange release, completely without precedent in box office history.|accessdate=March 24, 2014}} 

The film grossed $3,322,127 in domestic box office and $163,000 in foreign box office (Austria, Germany and United Kingdom) for a worldwide total of $3,485,127. 

===Critical response===
Veronica Mars has received positive reviews from critics, with many praising Bells performance as the title character. On   the film has a score of 62 out of 100, based on 35 critics, indicating "generally favorable reviews". 

Sheri Linden of The Hollywood Reporter gave the film a positive review, citing it as "a solid cinematic turn for the Nancy Drew of the new millennium,   sure to delight crowdfunding backers and other fans of the source series." 

===Awards===
 
|-
| style="text-align:center;" rowspan="5"| 2014
|Veronica Mars
| mtvU Fandom Awards Fandom of the Year
|  
|- Kristen Bell MTV Movie Award for Favorite Character
|  
|-
|Veronica Mars
| Teen Choice Award for Choice Movie - Drama
|  
|- Kristen Bell
| Teen Choice Award for Choice Movie Actress: Drama
|  
|- Jason Dohring
| Teen Choice Award for Choice Movie Actor: Drama
|  
 

===Home media performance===
Veronica Mars made $2.2 million from Blu-ray and DVD sales in its first two weeks of release. 

==Sequel==
On March 13, 2014, Variety (magazine)|Variety reported that actress Kristen Bell is interested in reprising her role as Veronica Mars in a sequel; however, one has not yet been officially announced. Thomas revealed that he and Warner Bros. had a "magic number" in mind for the first film to earn in order to make a sequel possible.  In an interview with Michael Ausiello published on July 29, 2014, Rob Thomas stated: "Im optimistic. The   movie made money which was key to maybe seeing a second one, so yeah, Id love to do it." 

=== Novels ===
A series of novels, written by series creator   on March 25, 2014, as a  , and would not be negated by a future film. 

==References==
 

== Further reading ==
*  

==External links==
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 