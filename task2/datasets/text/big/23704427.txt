Hud (1986 film)
{{Infobox film
| name           = Hud
| image          = 
| caption        = 
| director       = Vibeke Løkkeberg
| producer       = Terje Kristiansen Vibeke Løkkeberg
| writer         = Vibeke Løkkeberg Terje Kristiansen
| starring       = Vibeke Løkkeberg
| music          = 
| cinematography = Paul René Roestad
| editing        = Terje Kristiansen
| distributor    = 
| released       =  
| runtime        = 115 minutes
| country        = Norway
| language       = Norwegian
| budget         = 
}}

Hud (Skin) is a 1986 Norwegian crime film directed by and starring Vibeke Løkkeberg. It was screened in the Un Certain Regard section at the 1987 Cannes Film Festival.   

==Cast==
* Vibeke Løkkeberg - Vilde
* Keve Hjelm - Sigurd - Vildes Stepfather
* Terence Stamp - Edward - an artist
* Elisabeth Granneman - Vildes Mother
* Frank Audun Kvamtrø - Antonies
* Per Jansen - Sjur
* Tonje Kleivdal Kristiansen - Malene
* Per Oscarsson - The Vicar
* Thale Svenneby - Vilde som barn

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 