The Wild and the Brave
{{Infobox film
| name           = The Wild and the Brave
| image          =
| caption        =
| director       = Eugene S. Jones
| producer       = Eugene S. Jones Natalie R. Jones
| writer         =
| narrator       =
| starring       = Ian Ross and wife, Paul Ssali Naluma, wife and family, Kidepo rangers
| music          =
| cinematography = Tony Mander
| editing        = Stephen Milne
| distributor    =
| released       =  
| runtime        = 102 minutes
| country        = United States
| language       = English
| budget         =
}}

The Wild and the Brave is a 1974 American documentary film directed by Eugene S. Jones. The film portrays the relationship between Iain Ross, the outgoing British Chief Warden of Kidepo Valley National Park and his Ugandan replacement Paul Ssali. It portrays the racial and cultural tensions and amity of the postcolonial handover from 1970 to 1972.
 Best Documentary Feature.   

The film also carried the alternative title, Two Men of Karamoja.  

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 