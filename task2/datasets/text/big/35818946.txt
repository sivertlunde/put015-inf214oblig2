Machete Kills
 
{{Infobox film
| name = Machete Kills
| image = Machete Kills.jpg
| border = yes
| alt = 
| caption = Theatrical release poster
| director = Robert Rodriguez
| producer = {{plainlist|
*Robert Rodriguez
*Rick Schwartz
*Sergei Bespalov
*Alexander Rodnyansky
*Aaron Kaufman
*Iliana Nikolic
}}
| screenplay = Kyle Ward
| story = {{plainlist|
*Robert Rodriguez
*Marcel Rodriguez
}}
| starring = {{plainlist|
*Danny Trejo
*Michelle Rodriguez
*Sofía Vergara
*Amber Heard
*Antonio Banderas
*Lady Gaga
*Cuba Gooding Jr. Walt Goggins William Sadler
*Demián Bichir
*Mel Gibson
 
}}
| music = Carl Thiel
| cinematography = Robert Rodriguez
| editing = {{plainlist|
*Robert Rodriguez
*Rebecca Rodriguez
}} Quick Draw Productions Troublemaker Studios 
| distributor = Open Road Films 
| released =    
| runtime = 108 minutes  
| country = United States
| language = English Spanish
| budget = $20 million 
| gross = $15,008,161 
}} William Sadler, Marko Zaror and Charlie Sheen (credited by his real name of "Carlos Estévez").  The film follows the titular ex-federal (Trejo) as he is recruited by the President of the United States|U.S. President (Sheen) to stop an arms dealer (Gibson) and a revolutionary (Bichir).    

The film was released on October 11, 2013, failing to recoup its budget of $20 million and receiving negative reviews. Critics cited the overuse of plot points, poorly produced CGI, and the blend with science fiction genre.

==Plot== Machete Cortez Mexican drug William Sadler) President of the US, Rathcock (Charlie Sheen), intervenes. Machete is brought to the White House, where the president offers him US citizenship if he eliminates Marcos Mendez (Demián Bichir), a psychopath who is threatening to fire a nuclear missile at Washington, D.C. if the American government does not intervene to stop the rampant drug cartels in Mexico and the corruption of its government.

Machete agrees and travels to San Antonio, where he meets his handler Blanca Vasquez (Amber Heard), an undercover beauty pageant competitor. She sends him to Acapulco to meet a young woman, Cereza (Vanessa Hudgens), who can lead him to Mendez. Machete finds her in a brothel run by her mother, Madame Desdemona (Sofía Vergara), who attempts to kill Machete before he escapes with Cereza. She takes him to Mendezs associate, Zaror (Marko Zaror), who kills Cereza before taking Machete to Mendezs base of operations.

There, Machete learns that Mendez has wired the missiles launch device to his heart and triggered its launch in 24 hours. If he dies, the missile fires. After killing Zaror he captures Mendez, intending to escort him to US and find a way to disarm the missile. Machete learns that Mendez is an ex-secret agent who tried to expose his corrupt superiors, only to be betrayed and forced to watch his family being tortured. The trauma drove him insane, creating his split personalities and leading him to join forces with the missiles creator.

Shortly thereafter, a hit is put out on them. Machete is targeted by Madame Desdemona and her prostitute assassins, including a shapeshifting hitman called El Camaleón (Lady Gaga), as well as Doakes. Machete and Mendez manage to reach the US and kill Doakes and Clebourne only to be caught by a reborn Zaror and the same mercenaries who killed Sartana. Zaror decapitates Mendez and Machete is riddled with bullets.

Machete wakes up to find himself in a healing tank. He is taken to meet Zarors benefactor—corrupt businessman, inventor and Star Wars fan Luther Voz (Mel Gibson). He shows Machete Mendezs beating heart, preserved in a jar, and informs him of his plan to manipulate extremists throughout the world to detonate nuclear weapons while planning to escape in a spaceship to rebuild society in space. Machete then escapes with help from Luz (Michelle Rodriguez), who had heard about the hit on Machete. She informs him that the only one who can disarm Mendezs heart is Machetes old enemy, Osiris Amanpour (Tom Savini). Machete contacts Vasquez, who instructs him to meet her at a rendezvous point.

Arriving there, Machete is betrayed and ambushed by Vasquez, who is in league with Voz. As she is escaping into the desert, Machete gives chase and jumps onto the top of her vehicle but falls off after gunfire comes through the roof. Machete is then given a ride by El Camaleón, who tries to kill him one last time. But he escapes and El Camaleón ends up being shot to death by a group of racist rednecks just inside the US border. Machete then reunites with Luz and her group, the Network. They infiltrate a fundraiser at Vozs base of operations. Machete realizes Voz was the one who killed Sartana and fights him. He severely burns Vozs face, disfiguring him to the extent that Voz is forced to wear a metallic, silver mask. Meanwhile, Vasquez shoots Luz in her good eye, completely blinding her. Luz kills Vasquez in return but is captured by Voz, frozen in Carbonite and taken aboard his ship.

Machete jumps on the missile as it launches and disarms it in mid-air, while Voz boards the ship and departs with the Zaror clones, his supporters, as well as Luz. The disarmed missile then plunges into the Rio Grande and Machete is rescued by President Rathcock, who asks him to follow Voz into space and kill him. Machete agrees and uses a SpaceX Falcon 9 v1.0 rocket to depart to Vozs Station in Earths orbit, where he is given a laser machete to start his mission.

In a post-credits scene, an outtake from the Luz and Blanca fight scene is included followed by a shot of President Rathcock in front of a space background inquisitively brandishing two of Vozs guns (the molecular disruptor and the same pistol used to kill Sartana) before firing wildly at an off-screen target.

==Cast==
{{Columns-list|2| Machete Cortez
* Michelle Rodriguez as Luz / Shé   
* Mel Gibson as Luther Voz   
* Sofía Vergara as Madame Desdemona   
* Amber Heard as Blanca Vasquez / Miss San Antonio   
* Demián Bichir as Marcos Mendez   
* Charlie Sheen (credited by his real name of "Carlos Estévez") as President Rathcock   
* Walton Goggins as El Chameleón 1      
* Cuba Gooding Jr. as El Chameleón 2    
* Lady Gaga as La Chameleón
* Antonio Banderas as El Chameleón 4   
* Jessica Alba as Sartana Rivera
* Vanessa Hudgens as Cereza Desdemona      
* Alexa Vega as KillJoy 
* Marko Zaror as Zaror
* Tom Savini as Osiris Amanpour William Sadler as Sheriff Doakes   
* Julio Oscar Mechoso as Chepo 
* Billy Blair as Billy Blair 
* Samuel Davis as Clebourne
* Vincent Fuentes as Cabrito Cook
* Elle Lamont as Dollface
* Felix Sabates as Doc Felix
* Electra and Elise Avellan as Nurse Mona and Nurse Lisa 
* Jessica Moreno as Lady D Girl
* Vicky Solis as Lady D Girl
* Marci Madison as Nurse Fine
* Elon Musk as Himself
}}

==Production==
On June 10, 2012, Rodriguez announced that principal photography for Machete Kills had begun.  Principal photography took only 29 days, as shooting wrapped on July 28, 2012. 

The film was produced by Robert Rodriguez, as well as Aaron Kaufman and Iliana Nikolic through their QuickDraw Productions, Sergei Bespalov of Aldamisa Films, Alexander Rodnyansky of AR Films and Rick Schwartz of Overnight Productions. 

Lindsay Lohan, who played April Booth in the first film, did not appear in this installment. Rodriguez said that he liked Lohans character but she "didnt fit into the story". 

The film has  .com, June 6, 2013 

On June 20, 2013, the films release date was pushed back from September 13, 2013, to October 11, 2013, to avoid competition with  . 

==Promotion==
On October 9, 2013, Lady Gagas Vevo released a lyric video for "Aura (song)|Aura", a song from her fourth studio album Artpop, to promote the film. 

==Reception==
  normalized rating based on reviews from mainstream critics, the film has received a score of 41 out of 100, based on 33 critics, indictating "mixed or average reviews". 

Gaga was nominated for a  . 

==Sequel==
At the end of the first films theatrical release, two sequels are mentioned, Machete Kills and Machete Kills Again. The trailer for the third and final film, Machete Kills Again... In Space,  precedes the second film as a "Coming Attraction".      

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 