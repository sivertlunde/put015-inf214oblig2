The Frog Prince (1986 film)
{{Infobox Film | name = The Frog Prince
  | image =The Frog Prince (1986 film).jpg 
  | genre = Fantasy, Musical
  | director = Jackson Hunsicker
  | producer = Patricia Ruben
  | writer = Jackson Hunsicker
  | starring = Helen Hunt   Amanda Pays   Aileen Quinn  
  | cinematography = Amnon Salomon Jim Clark
  | distributor = Cannon Group
  | released = UK:   5 October 1986   USA:   1 June 1988
  | runtime = 86 minutes
  | country = USA Israel English
 The Frog Prince by The Brothers Grimm
  }}
 classic fairytale. It is rated "G" and was filmed in Tel Aviv, Israel. The tagline was "Cannon Movie Tales: Lavish, Feature length new versions of the worlds best loved storybook classics."

==Cast==
*Aileen Quinn - Princess Zora
*Helen Hunt - Princess Henrietta
*John Paragon - Ribbit / Prince Of Freedly
*Clive Revill - King William
*Seagull Cohen - Dulcey
*Eli Gorenstein - Cook
*Shmuel Atzmon - Baron Von Whobble
*Jeff Gerner - Emissary
*Aaron Kaplan - Page
*Moshe Ish-Kassit - Sleeping Guard

== Reception ==
=== Critical reception === AMG gave the film a 2-star rating.

== Soundtrack ==
* "Theme Song"
Written by Kenn Long

* "Lucky Day"
Written by Kenn Long 
Performed by Aileen Quinn

* "A Promise Is A Promise"
Written by Kenn Long 
Performed by Clive Revill & Aileen Quinn

* "Too Tall Frog"
Written by Kenn Long 
Performed by Nick Curtis

* "Music Box Waltz"
Written by Kenn Long

* "Friendship"
Written by Kenn Long 
Performed by Aileen Quinn & Nick Curtis

* "Have You Forgotten Me?"
Written by Kenn Long  
Performed by Aileen Quinn

==References==
*  
*  

 
 
 
 
 

 