Stepping Out (1991 film)
{{Infobox film
| name           = Stepping Out
| image          = SteppingOutFilmPoster.jpg
| image_size     = 200px
| caption        = One-sheet promotional poster
| director       = Lewis Gilbert
| producer       = John Dark Lewis Gilbert Richard Harris 
| starring       = Liza Minnelli Shelley Winters Julie Walters Robyn Stevan Bill Irwin Ellen Greene Jane Krakowski Sheila McCarthy Andrea Martin Nora Dunn Eugene Robert Glazer Carol Woods
| music          = Peter Matz
| cinematography = Alan Hume
| editing        = Humphrey Dixon
| distributor    = Paramount Pictures
| released       =  
| runtime        = 106 min.
| country        = United States English
| budget         =
| gross          = $246,000 
}} musical comedy Richard Harris, based on a play also written by Harris, and starring Liza Minnelli. 

Most of the actors are Broadway theatre|Broadway-level actors and performers, several of whom have won Tony Awards, specifically Minnelli, Krakowski, Martin, Irwin and Greene (nominee). Winters and Minnelli are Academy Award winners, and Walters is an Academy Award nominee as well as being a BAFTA and Golden Globe Award winner. Woods was the only member of the Broadway cast to appear in the film. 

== Synopsis == Buffalo and starts teaching tap dance lessons to a group of misfits who, through their dance classes, bond and realize what they can achieve. Their newfound self-confidence changes their lives forever.

== Cast ==
* Liza Minnelli as Mavis Turner, a former Broadway dancer and singer who teaches a tap class in Buffalo, New York
* Bill Irwin as Geoffrey, a shy insurance salesman, and the only man in the group
* Ellen Greene as Maxine, a cute, funny, and slightly wild mom
* Robyn Stevan as Sylvia, a loud-mouthed buddy of Rose.
* Jane Krakowski as Lynne, a sensitive Registered Nurse, one of the best dancers battered wife
* Andrea Martin as Dorothy, a naive but sincere housewife with an allergy problem
* Julie Walters as Vera, a wealthy but inadvertently abrasive woman
* Carol Woods as Rose, a vibrant woman with a teenage son who gets into trouble
* Shelley Winters as Mrs. Fraser, a dour rehearsal pianist
* Luke Reilly as Patrick, Maviss conceited, insensitive boyfriend
* Nora Dunn as Pam, director of the upscale dance center, a rival to Mavis
* Eugene Robert Glazer as Frank, Andis husband
* Géza Kovács as Jerry
* Raymond Rickman as Alan

== Differences between play and film ==
In order to appeal to a broader audience, and to justify the hiring of headliner Minnelli, the setting of the play was transferred from a gritty North London suburb to gritty Buffalo, New York. Maviss backstory was expanded to allow her a chance to sing more (in the play she has a role equal to those of the students).

The students personalities are generally the same on stage and screen, although minor plot strands have been omitted. For example, Rose was initially a Trinidadian, a significant immigrant population in London. Any character outside the class is an addition for the film, as well as any scene outside the classroom plus the final production number.

John Kander and Fred Ebb wrote a new song for the film, the title number, which is given a rousing (and lengthy) presentation at films end, in which the formerly awkward troupe reveal themselves to now be polished performers.

== Reception ==
The film was designed as a motion picture comeback for Liza Minnelli. In the 1970s, after the great success of her film,  . Both films were financial flops. In order to salvage Minnellis by-now tarnished film career, there was great anticipation in the hopes that Stepping Out would re-establish her as a film star. Originally Stepping Out was to be released in the spring of 1991 to coincide with Minnellis New York concert stage show Live from Radio City Music Hall, but because of a corporate restructuring at Paramount Studios, the films opening was delayed until October 4, 1991 when it was released only in a very limited format. Despite decent reviews and quite a bit of publicity (Minnelli and the cast of Stepping Out - with the exception of Ellen Greene and Julie Walters - even appeared on the nationally syndicated television talk program The Sally Jessy Raphael Show on the day of the premiere to promote the film), it only grossed $246,000 total in the United States and within a short while, went straight to video. 

Variety (magazine)|Variety, in its 1991 review described the film as "Its Liza-as-you-love-her in Stepping Out, a modest heartwarmer about a bunch of suburban left-feeters getting it together for a charity dance spot. Fragile ensemble item often creaks under the Minnelli glitz, but results are likeable enough." Roger Ebert gave the film a moderate review stating "The   contained more dancing and was generally more engaging, maybe simply because it was on the stage. As a song-and-dance picture, it talks too much. As a drama, its superficial and locked into a formula." 

== Awards == Best Supporting Actress category.

== References ==
 

== External links ==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 