Two Days in April
 
{{Infobox film
| name           = Two Days in April
| image          =
| image size     =
| alt            =
| caption        =
| director       = Don Argott
| producer       = Sheena M. Joyce Dave Broome Liesl Copland Ted Sarandos Jack Tiernan
| writer         =
| screenplay     =
| story          =
| narrator       = Travis Wilson
| music          =
| cinematography = Don Argott
| editing        = Demian Fenton
| studio         = 25/7 Productions 9.14 Pictures
| distributor    = Red Envelope Entertainment
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = United States|$550,000 (est)
| gross          =
}}
 2007 documentary Travis Wilson) NFL Draft Weekend, and focuses on the intense training leading up to the NFL Draft and the emotional roller coaster of draft day.  Three of the players were selected within six picks of each other in the third round. {{cite news
| url=http://sportsillustrated.cnn.com/2007/writers/the_bonus/12/20/documentary/index.html
| title=Lights, camera...  How a film about the NFL Draft got stuck in red zone
| work=Sports Illustrated
| date=December 21, 2007
| accessdate=June 8, 2011
| author=L. Jon Wertheim}} 
 Bruce Allen, Michael Johnson, Travis Wilson, and Wendy Wilson.

==Background== IMG training academy in Bradenton, Florida to meet the players and begin filming. 
 IMG to join Creative Artists Agency, taking his clients with him.  One of the contractual conditions for filming IMG athletes at the IMG Academy, was that the film would be subject to IMGs artistic approval.  With Condon leaving the project, Broome and Argott were worried that IMG would invoke this clause to shut down the project. 

After completion of principal filming, Argott spent several months editing more than 150 hours of video to create the 92-minute film. Red Envelope Entertainment promoted the film to home video and television distribution contacts, and even considered trimming the film to a 42-minute length for ESPN to include as part of their 2007 NFL Draft coverage. However, during the films post-production, Netflix learned that Condon refused to sign a release to appear on film. 

According to his attorney, Condon was unhappy after seeing a screening of the film, believing that it did not reflect his clients or the draft processes properly, and asserting that the production company had verbally promised him the final right to artistic sign-off.  Broome contended that Condon wished to sabotage the project because he did not do well in the draft and was worried other sports agents might use this as leverage against him. Broome further denied that he or any producer would ever verbally give away final approval to a subject in a documentary.  In 2007, the NFL and NFL Properties expressed concerns about whether the filmmakers had obtained rights to certain used footage, and shortly thereafter ESPN broke off negotiations. 

Broome felt this was because ESPN would rightly question why an executive producer would disavow a project and why the NFL would question rights to air footage.  And when the 2007 NFL Draft passed without the film being able to be aired, Broomes production company, First and Ten Productions, filed a lawsuit against Condon and NFL Productions.  Included among their charges were "intentional interference with a contractual relationship," alleging that "Condon willfully and maliciously interfered ... by falsely representing to the National Football League that Plaintiff had not obtained all necessary rights to the combine footage or to the use of Condons name, image and likeness."  The suit against the NFL was settled for undsclosed terms and they were subsequently removed as defendants in the lawsuit, leaving Condon as sole defendant. 

==Critical reception==

 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 