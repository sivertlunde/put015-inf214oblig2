Sentenced for Life
 
 
{{Infobox film
  | name     = Sentenced for Life
  | image    = 
  | caption  = 
  | director = E. J. Cole		
  | producer = 
  | writer   = 
  | based on = play Sentenced for Life
  | starring = Bohemian Dramatic Company
  | music    = 
  | cinematography = 
  | editing  =  studio = Australian Biograph Company 
  | distributor = Pathes Freres 
  | released = 29 May 1911 
  | runtime  = 2,000 feet 
  | language = Silent film English intertitles
  | country = Australia
  | budget   = 
  }}

Sentenced for Life is an Australian film directed by E. J. Cole. It was an adaptation of a play performed by Cole and his Bohemian Dramatic Company as early as 1904. 

It is considered a lost film. 

==Plot==
A man is wrongly convicted and sentenced as a convict.  According to a contemporary report, "Vivid convict scenes are enacted, ending with a revolt by the prisoners. There is a happy ending of wedding bells."    It turns out the young mans rival was responsible and he is punished. 

Chapter headings were:
*the Favourite;
*it did look suspicious
*the Blackmailer, 
*Outlaw and the Child, 
*Slight Breeze, 
*Malaysia, 
*General Commotion, 
*Blighted Hopes, 
*Manufacture of Almonds   

==Cast of theatre production==
In 1911 the cast of a theatre production of the play in Geelong was listed as follows:
*E. I. Cole as Mr. Bertram, 
*Mr. Frank Mills as Richard Hayward, 
*W. S. Marshall as Jabez Ooh
*J. R. Wilson as Sammy Traddles
*Vene Linden as Mary Bertram 
It is highly likely at least some of these actors repeated their performance in the film.

==References==
 

==External links==
*  
*  at AustLit
 

 
 
 
 


 