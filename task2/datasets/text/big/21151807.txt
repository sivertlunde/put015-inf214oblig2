Renzo Gracie: Legacy
{{Infobox Film
| name           = Renzo Gracie: Legacy
| image          = DVD cover of the movie Renzo Gracie- Legacy.jpg
| caption        = DVD cover
| director       = Gethin Aldous
| producer       = Gethin Aldous
| writer         = Gethin Aldous Steve Allen Adrian Miller 
| starring       = Renzo Gracie Robson Gracie Ryan Gracie Ralph Gracie Daniel Gracie B.J. Penn Pat Miletich Oleg Taktarov Andre Gusmão Kyra Gracie
| music          = Adrian Miller
| cinematography = Gethin Aldous
| editing        = Gethin Aldous
| studio         = 
| distributor    = 
| released       =  
| runtime        = 78 minutes
| country        = United States Portuguese
| budget         = 
| gross          = 
}}
Renzo Gracie: Legacy is a   from its bare knuckle days to the explosion of the sport in both Japan and United States|America.   

==Background==
Renzo is from the notorious Gracie family of Brazil and is the third generation of Brazilian Jiu Jitsu fighters from that family.   In 1993 his uncle Rorion Gracie joined forces with Art Davie and Bob Meyrowitz to create the perfect proving ground to display the brilliance of his families fighting art Ultimate Fighting Championship.   The sport is now considered one of the fastest growing in the world. 

==Production== directed by Gethin Aldous.   He followed Renzo over a 7-year period and another student of Renzos, Alex Shum, filmed Renzo for 4 years before that.   The filmmakers chose to shun a normal distribution route and decided to self-distribute, desiring that the money for all the "blood, sweat, and tears" put into the film went straight to its makers. It was self-financed by the filmmakers on a shoestring budget.

==Release== 2008 at the United States Sports Film Festival in Philadelphia, Pennsylvania.,     and was released on DVD with little or no marketing on November 14, 2008, through its own website and Amazon.com. 

==Reception==
There are 2 on-line reviews written about the film at present: Paul Larkin of MMAjunkie spoke of the director saying, "Gethin Aldous did a fantastic job with Legacy. He does not bore you to death with narration or beat you over the head with information.", and of the film,  he wrote it "...will no doubt be considered a masterpiece in the world of documentaries".   Elias Cepeda of Inside Fighting wrote, "Gethin Aldous’ new documentary film Renzo Gracie: Legacy should be considered an instant classic simply on the basis of its scope and breadth", and the film "...accomplishes the rare feat of being able to both satisfy the most ardent and informed sports fan’s desire for fresh, new perspective and knowledge and serve as an understandable primer for the uninitiated." 

==Cast==
(as themselves)
*Renzo Gracie
*Robson Gracie
*Ryan Gracie
*Ralph Gracie
*Daniel Gracie
*B.J. Penn
*Pat Miletich
*Oleg Taktarov
*Andre Gusmão
*Kyra Gracie

==Additional sources==
*MMA Mania, "Renzo Gracie: Legacy to premiere at the inaugural U.S. Sports Film Festival on October 25" 
*Five Ounces of Pain, "New Renzo Gracie documentary to debut during film festival" 

== References ==
 

==External links==
*  

 
 
 
 
 