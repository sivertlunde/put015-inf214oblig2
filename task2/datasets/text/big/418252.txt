One-Eyed Jacks
 
{{Infobox film
| name           = One-Eyed Jacks
| image          = One Eyed Jacks poster.jpg
| caption        = One Eyed Jacks theatrical poster
| director       = Marlon Brando
| producer       = George Glass Walter Seltzer Frank P. Rosenberg
| screenplay     = Guy Trosper Sam Peckinpah (uncredited) Calder Willingham Rod Serling (early draft) 
| based on       =   Ben Johnson Slim Pickens
| cinematography = Charles Lang
| music          = Hugo Friedhofer
| editing        = Archie Marshek
| distributor    = Paramount Pictures
| released       =  
| runtime        = Theatrical cut: 141 minutes Directors cut: 300 minutes (Has been destroyed, cut footage does not exist)
| country        = United States
| language       = English
| budget         = $6 million 
| gross          = $4.3 million (US/ Canada rentals)  
}}
 1961 western Ben Johnson, and Slim Pickens.

==Plot==
Rio (Marlon Brando) (also called "The Kid"), a shameless philanderer, his mentor Dad Longworth (Karl Malden), and a third man named Doc rob a bank of two saddlebags of gold in Sonora, Mexico. The robbery is successful, but Mexican Rurales track them and catch them celebrating in a cantina, killing Doc. Dad and Rio manage to escape, followed by the Rurales.

After getting cornered on a high ridge, with Rios horse dead, Rio figures the Rurales will be "swarming all over us inside an hour". Deciding that one partner might take the remaining pony and ride to a little jacalito down the canyon about five miles and return with fresh mounts, they shake for it, with Rio fixing the deal so his pal Dad can be the one to go.

Dad gets to a corral, strapping the swag bag onto a fresh pony, but he gets second thoughts. He casts one eye towards a point on the ridge sure to be taken by the Rurales, and with the other he gazes off in the opposite direction out past a low-lying treeline towards the border and safety. One way leads to danger and a poor chance at surviving with half the booty, the other towards a virtual certainty with all of it. After a decidedly short moment of reflection, he takes the latter and leaves his friend to be taken by Rurales. Rio is arrested, and is transported to prison by way of the jacalito where he learns firsthand of Dads betrayal from the owner.

Rio spends five hard years in a Sonora prison, giving him ample time to mull over Dads betrayal before escaping with new partner Chico Modesto (Larry Duran) and going hunting for him. When he locates him, Longworth has used his wealth to become the sheriff of Monterey, California. Instead of ambushing Dad, Rio gives him a chance to explain why he left him back in Mexico, pretending he had never been captured to put him off-guard. Longworths awkward self-serving story is easily seen through.
 Ben Johnson) (who used his knowledge of Dads whereabouts to force their partnership), but his plans are sidetracked when he falls in love with Longworths beautiful virginal stepdaughter Louisa (Pina Pellicer), and takes advantage of a fiesta to spend the night with her on the beach. When Dad finds out, he tries to make Louisa confess to being deflowered, but after intervention by his wife Maria (Katy Jurado) he backs down. He then traps Rio in town and administers a vicious beating with a whip in front of the entire town, smashing Rios gun hand with the butt of a shotgun in an attempt to end his gun-slinging days.

While recovering from his wounds near the ocean, Rio struggles with his conflicting desires to love the girl and to kill her stepfather for revenge. He decides to forgo vengeance, fetch Louisa and leave, but Emory, having decided that Rio will never be fast enough to challenge him again kills Chico and pulls off the bank job without his knowledge. Worse, the heist goes wrong and a young girl is killed. Rio is falsely accused without evidence and locked up by Longworth after whipping the town up into a lynch mob state. Dad desperately wants to kill Rio in an attempt to absolve his own guilt over the earlier betrayal. Knowing that the trials outcome is certain, and Rio is sure to be hanged in two days, Dad has one last private talk with him, again attempting to excuse himself, to which Rio replies, "You may be a one-eyed jack around here, but I’ve seen the other side of your face." Rio confesses to being imprisoned for the last five years in a Sonora prison, but Dad disbelieves him, saying that it is a lie.

After Louisa visits Rio in jail to confess that she is going to have his baby, he is beaten by sadistic deputy Lon Dedrick (Slim Pickens) out of jealousy and envy for Louisas affection. Maria faces Dad about telling her the truth, which she learned from Louisa, stating that she knew something was wrong since the moment Rio arrived in the town and that he is going to hang him out of pure guilt. Longsworth leaves in a fit of anger.

Louisa visits the jail again in an attempt to smuggle a Derringer pocket pistol, but she is discovered by Lon, who leaves the gun on the table. While Lon takes Louisa out, Rio seizes the opportunity to get hold of the pistol, which he accomplishes, but without ammunition. Rio bluffs his way out of jail with the unloaded pistol, helping himself to the revolver of Lon after a tense scene. As he is making his escape, he is spotted by Dad in the center of town. Under fire and left with no choice, Rio kills Longworth in a final showdown.

In the closing scene, Rio and Louisa ride out to the dunes and say a sentimental farewell. Rio will now be a hunted man and is already wanted in Mexico, and tells Louisa that hes going to Oregon, but to look for him in the spring, when he will return for her.

This film was one of the first Westerns to portray a bad man as somebody the audience can identify with, leading to the Spaghetti westerns of Sergio Leone.

==Cast==
* Marlon Brando ... Rio
* Karl Malden ... Dad Longworth Ben Johnson ... Bob Emory
* Katy Jurado ... Maria Longworth
* Pina Pellicer ... Louisa
* Slim Pickens ... Lon Dedrick
* Larry Duran ... Chico Modesto
*Sam Gilman	...	Harvey Johnson
*Timothy Carey	...	Howard Tetley
*Miriam Colon	...	Redhead
*Elisha Cook, Jr.	...	Carvey (billed as Elisha Cook)
*Rodolfo Acosta	...	Mexican Rurale Captain (billed as Rudolph Acosta)
*Tom Webb	...	Farmers Son
*Ray Teal...	Barney
*John Dierkes	...	Chet
*Philip Ahn	...	Uncle
*Margarita Cordova	...	Nika, Flamenco Dancer
*Hank Worden	...	Doc
*Clem Harvey	...	Tim
*William Forrest	...	Banker
*Mina Martinez	...	Margarita
*P. Casteneda	...	Townswoman
*L. Gohl	...	Girl
*J. Olivetti	...	Vendor
*Mary Dagher Turbay	...	Spanish Speaking Inn Keeper
*Eric Alden	...	Townsman (uncredited)
*Nesdon Booth	...	Townsman (uncredited)
*Sheryl Deauville	...	Marina (uncredited)
*Maria Deglar	...	Townswoman (uncredited)
*Rosita Delva	...	Townswoman (uncredited)
*Joe Dominguez	...	Corral Keeper (uncredited)
*Mickey Finn	...	Blacksmith (uncredited)
*Duke Fishman	...	Townsman (uncredited)
*Nacho Galindo	...	Mexican Townsman (uncredited)
*John George	...	Townsman (uncredited)
*Augie Gomez	...	Townsman (uncredited)
*Al Haskell	...	Townsman (uncredited)
*Jimmie Horan	...	Townsman (uncredited)
*Miyoshi Jingu	...	Aunt (uncredited)
*Fenton Jones	...	Square-Dance Caller (uncredited)
*Lisa Lu	...	Mei-Mei (uncredited)
*Margarita Martín	...	Mexican Vendor (uncredited)
*Donald McGuire	...	Townsman (uncredited)
*Jorge Moreno	...	Bouncer in Shack (uncredited)
*Charles Morton	...	Townsman (uncredited)
*Joan Petrone	...	Flower Vendor (uncredited)
*Snub Pollard	...	Townsman (uncredited)
*John Michael Quijada	...	Mexican Rurale Sergeant (uncredited)
*Francy Scott	...	Cantina Girl (uncredited)
*Bert Stevens	...	Townsman (uncredited)
*Shichizo Takeda	...	Cantina Owner (uncredited)
*Marie Tsien	...	Townswoman (uncredited)
*Felipe Turich	...	Cardsharp (uncredited)
*Glen Walters	...	Townswoman (uncredited)
*Henry Wills	...	Ephraim the stableman (uncredited)

==Adaptation and development==
  The Twilight Zone series, wrote an adaptation of the novel The Authentic Death of Hendry Jones by Charles Neider (1956), at the request of producer Frank P. Rosenberg.  The book was a fictional treatment of the familiar Billy the Kid story, relocated from New Mexico to the Monterey Peninsula in California.  The adaptation was rejected.

Rosenberg next hired Sam Peckinpah, who finished his first script on 11 November 1957. Marlon Brandos Pennebaker Productions had paid $40,000 for the rights to Authentic Death and then signed a contract with Stanley Kubrick to direct for Paramount Pictures. Peckinpah handed in a revised screenplay on 6 May 1959. Later, Brando fired Peckinpah and hired Calder Willingham, but he and Brando stalled, so both Willingham and Kubrick were fired. Guy Trosper became the new screenwriter and worked on the story with Brando, who volunteered to serve as director.

The movie had very little resemblance to the Neider novel, and what remains has much more resonance with history than fiction. At various times, the two credited screenwriters and the uncredited Peckinpah have claimed (or had claimed for them) a majority of the responsibility for the film. When Karl Malden answered the query about who really wrote the story he said: "There is one answer to your question — Marlon Brando, a genius in our time." 

==Production== Academy Award nomination in the Best Cinematography, Color category that year. Upon release, it made little money, leading to a string of unsuccessful films for Brando.

Marlon Brando shot a total of five hours of additional footage, some of which was later destroyed. Later, other directors worked on the rest of the film after Brando walked away from the production.    He did not direct another film in his later years, but he did continue to act. In a 1975 Rolling Stone interview Brando said of directing, "You work yourself to death. Youre the first one up in the morning... I mean, we shot that thing on the run, you know, You make up the dialogue the scene before, improvising, and your brain is going crazy". 

==Release==
The film was released on March 30, 1961 in New York City. 

==Critical reception==
One-Eyed Jacks received mixed reviews from critics. Review aggregator  , on the other hand, wrote: "It is an oddity of this film that both its strength and its weakness lie in the area of characterization. Brandos concept calls, above all, for depth of character, for human figures endowed with overlapping good and bad sides to their nature."  Dave Kehr of The Chicago Reader wrote: "There is a strong Freudian pull to the situation (the partners name is “Dad”) that is more ritualized than dramatized: the most memorable scenes have a fierce masochistic intensity, as if Brando were taking the opportunity to punish himself for some unknown crime." 

== In popular culture ==
One-Eyed Jacks is the name of a brothel in the TV series Twin Peaks created by David Lynch and Mark Frost. That it shares the same name as this film is acknowledged in dialogue between Donna Hayward and Audrey Horne, where Audrey asks Donna if she has heard of "One-Eyed Jacks" and Donna responds "Isnt that that Western with Marlon Brando?" 

Johnny Burnette released a tie-in song The Ballad of the One-Eyed Jacks.

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 