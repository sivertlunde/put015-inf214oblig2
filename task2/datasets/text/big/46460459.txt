Akeli (film)
{{Infobox film
| name           = Akeli
| image          = 
| caption        = 
| director       = Vinod Pande
| producer       = Vinod Pande
| screenplay     = 
| narrator       = 
| starring       = R. Madhavan Nandini Ghosal Anupam Shyam
| music          = Jeetu-Tapan Nilesh Bhatia 
| cinematography = Sudhir Gowda
| editing        = Bobby Bose
| studio         = Cine Images Production
| distributor    = 
| released       =  
| runtime        = 165 mins
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}}

Akeli is a 2014 Hindi film produced, written and directed by Vinod Pande. The film featured R. Madhavan in his first full-length role in a Hindi film, alongside danseuse Nandini Ghosal and Anupam Shyam. Despite being completed and censored in August 1999, the film remained unreleased before the makers chose to release it via YouTube in August 2014. 

==Cast==
 
* R. Madhavan as Avinash
* Nandini Ghosal as Meera
* Anupam Shyam
* Sujata Sanghamithra
* Prerna Agarwal
* Asha Sharma
* Aashish Kaul Raj Kiran
* Vinod Pande
* Brij Gopal Shakti Singh
 

==Production==
The project materialised in late 1997, with R. Madhavan agreeing to make his acting debut in Hindi films with the project. He signed the film while he was shooting for the Kannada venture, Shanti Shanti Shanti (1998), as well as a series of television dramas.   Danceuse Nandini Gopal was selected to play the leading female role, while Anupam Shyam was picked to play another supporting role. The film was censored on 10 August 1999, but was unable to find a distributor for a theatrical release.  In 2000, a corporate group considered purchasing the first copy of the film, but Vinod Pande decided to delay the venture until Madhavans Rehnaa Hai Terre Dil Mein (2001) was released. The film then geared up for release in 2002 through Yash Raj Films, but plans were suddenly cancelled at the final moment and the project remained indefinitely on hold. 

In 2014, Vinod Pande chose to release the film directly on to YouTube citing he did not want the film to get "lost", and that another of his unreleased films, Chaloo Movie, had found itself online illegally. It was later played at the Festival du Film dAsie du Sud Transgressif in France in February 2015, with Vinod Pande in attendance.

==References==
 

==External links==
*  

 
 
 
 

 