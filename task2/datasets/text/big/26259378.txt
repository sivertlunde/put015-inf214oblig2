Karthika Deepam (film)
{{Infobox film
| name           = Karthika Deepam
| image          = Karthika Deepam.jpg
| image_size     =
| caption        =
| director       = Laxmi Deepak
| producer       = G. Radhadevi Gupta A. Krishnaiah
| writer         = M. Prabhakar Reddy   (story)  Laxmi Deepak   (screenplay) 
| narrator       = Sharada Geetha Geetha Gummadi Venkateswara Rao M. Prabhakar Reddy
| music          = Chellapilla Satyam
| cinematography = J. Satyanarayana
| editing        = K. Balu Vauhini and Prasad Studios
| distributor    =
| released       = May 4, 1979
| runtime        =
| country        = India
| language       = Telugu
| budget         =
}}
Karthika Deepam is a 1979 Telugu film directed by Laxmi Deepak and starring Shobhan Babu, Sridevi and Sharada (actress)|Sharada. The songs are highly popular particularly Aaraneekuma Ee Deepam Karteeka Deepam penned by Devulapalli Krishnasastri. The music score by Chellapilla Satyam are memorable. Shobhan Babu won Filmfare Best Actor Award (Telugu)

The film is remade as Maang Bharo Sajana (1980) in Hindi directed by Tatineni Rama Rao and in Kannada as Sowbhagya Lakshmi (1987).

==Credits==

===Cast===
* Shobhan Babu   ...   Sridhar Rao and Raja Sharada 
* Sridevi   ...    Radha Geetha   ...     Geetha
* Gummadi Venkateswara Rao Leelavathi
* M. Prabhakar Reddy   ...    Dhananjaya Rao Rajababu
* Allu Ramalingaiah
* K. V. Chalam   ...    Driver Suryakantham
* Ramaprabha
* Potti Prasad
* Mada

===Crew===
* Director: Laxmi Deepak
* Assistant Director: Gadiraju Keshava Rao
* Screenplay : Laxmi Deepak
* Producers: G. Radhadevi Gupta and A. Krishnaiah
* Production company: Kavitha Films
* Dialogues: Madhupatla Suri
* Cinematography: J. Satyanarayana
* Operative Cameraman: J. Phani Prasad
* Art director: B. N. Krishna
* Film Editing: K. Balu
* Lyrics: Devulapalli Krishna Sastry, C. Narayana Reddy, M. Gopi and Narala Ramareddy
* Music Director: Chellapilla Satyam
* Playback singers: S.P. Balasubrahmanyam, S. Janaki, P. Susheela

==Songs==
* "Aaraneekuma Ee Deepam Karteeka Deepam" (Lyricist:  , S. Janaki; Cast: Sridevi and Sharada)
* "Chilakamma Palikindhi Chiguraaku Pilichindi" (Lyricist: Narala Rami Reddy; Singers: S. P. Balasubrahmanyam, S. Janaki; Cast: Shobhan Babu and Sridevi)
* "Chooda Chakkani Daana" (Lyricist: Gopi; Singers: S. P. Balasubrahmanyam, P. Susheela; Cast: Shobhan Babu and Geetha)
* "Muvvalemo Nedemo Naalona Mroginchenu Mohana Geetam" (Lyricist: Dr. C. Narayana Reddy; Singer: S. Janaki; Cast: Sridevi)
* "Nee Kowgililo Taladachi Nee Chetulalo Kanumoosi" (Lyricist: Gopi; Singers: S. P. Balasubrahmanyam, S. Janaki; Cast: Shobhan Babu and Sridevi)
* "Ye Maata Aha Telusu Adi Kaadu" (Lyricist: Gopi; Singers: S. P. Balasubrahmanyam, P. Susheela; Cast: Shobhan Babu and Sharada)

==External links==
*  

 
 
 
 
 


 