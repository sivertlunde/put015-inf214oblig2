Mr. America (film)
{{Infobox film
| name            = Mr. America 
| image           = 
| border          = 
| caption         = 
| director        = Leonardo Ferrari Carissimi
| producer        = Velvet Movie Azteca Produzioni Redigital
| writer          = Leonardo Ferrari Carissimi Fabio Morgan
| starring        = Anna Favella Marco Cocci
| music           = The Niro
| cinematography  = 
| editing         =
| studio          =
| distributor     = Stemo Production 
| released        =  
| runtime         = 93 minutes
| country         = Italy - Canada  English - Italian
| budget          = 
|accessdate=
| gross           = 
}} thriller directed by Leonardo Ferrari Carissimi, and screenplay by the director himself and Fabio Morgan. It stars Anna Favella and Marco Cocci.

==Plot==
Penny Morningstar (Anna Favella) is a young and successful art gallery director, while Andy is a failed artist, obsessed with Andy Warhol. 
Andy Warhol was a serial killer, without piety, who stole peoples soul, letting them die slowly. Valerie Solanas, Edie Sedgwick or Jean-Michel Basquiat would probably share this theory, but they cannot do it, because they are all death. These are not the only suicides connected to the controversial figure of Andy Warhol, or as David Solanas called him, of "Mr. America". 
In the movie Penny is a sort of Andy Warhol. Shes beautiful and talented, but shes also evil. When she wants to do something she doesnt care if she hurst people. Every artist of the gallery is in love with her and she will destroy them. Then theres a killer, who thinks to be Andy Warhol and kills all the artists who compose Penny Morningstars factory.

==Cast==
* Anna Favella as Penelope (Penny) Morningstar
* Marco Cocci as Adrian 
* Eliud Luciani asNathan Briac
* Luca Mannocci as Seven
* Michael Schermi as Roy

==Premiere==
The film premiered on October 17, 2013 in Trento and the press conference and other official premiere was in Rome, on November 5, 2013. The movie has been released in all the cinemas on November 7, 2013.

===Accolades===
{| class="wikitable"
!Year
!Ceremony
!Award
!Result
|- 2013
|rowspan="2"|Terra di Siena International Film Festival Best Actor - Marco Cocci 
| 
|-
|}

 
 
 