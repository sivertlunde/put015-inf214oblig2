Kandagawa Pervert Wars
{{Infobox film
| name = Kandagawa Pervert Wars
| image = 
| image_size =
| caption = 
| director = Kiyoshi Kurosawa
| producer = 
| writer = Kiyoshi Kurosawa
| narrator = 
| starring = Usagi Aso
| music = 
| cinematography = 
| editing = Junichi Kikuchi
| studio = Directors Company
| distributor = Million Film
| released = August 1983
| runtime = 60 minutes
| country = Japan
| language = Japanese
| budget = 
| gross = 
}}

  is a 1983 Japanese pink film directed by Kiyoshi Kurosawa who would later go on to a career directing mainstream horror films.

==Synopsis==
A young girl, Akiko, in a tenement block in Tokyos Kandagawa area, uses a telescope to spy on her neighbors in between lovemaking sessions with her boyfriend. When she finds what seems to be an incestuous relationship between a mother and son, she decides, with her boyfriend and her friend Masami, to rescue the son from this predicament and introduce him to a "healthy sex life".

==Cast==
* Usagi Asō ( ) as Akiko
* Makoto Yoshino / Makoto Mino ( ) as Masami
* Houen Kishino ( ) as the Son
* Miiko Sawaki / Mimi Sawaki ( ) as the Mother
* Tatsuya Mori as Ryō (Akikos Boyfriend)
* Masayuki Suo as the Apartment Manager

==Background==
Kiyoshi Kurosawa was one of a number of young Japanese filmmakers, several associated with Nikkatsu, who belonged to a production organization called the Directors Company which had been founded in 1982. Through the influence of fellow Directors Company member Banmei Takahashi, Kiyoshi was offered a chance to direct a pink film for Million Film. This film became Kandagawa Pervert Wars with its references to Alfred Hitchcocks Rear Window, inventive directorial devices, playful mannerisms and in-joke allusions to Kurosawas favorite western films.    Jasper Sharp suggests that the studio was less than delighted with the result and Million shelved his second pink film effort College Girl: Shameful Seminar as not sexy enough. Kurosawa was able to buy the footage and reworked it into the 1985 non-pink film The Excitement of the Do-Re-Mi-Fa Girl. 

The cast and staff of Kandagawa Pervert Wars present a kaleidoscope of figures who would become an important part of film making in Japan in the 1990s and later.  These include Chief Assistant Director Toshiyuki Mizutani, Second Assistant Director Masayuki Suo who also had a minor role in the film, Third Assistant Director Akihiko Shiota, and actor Tatsuya Mori.    

Thomas and Yuko Mihara Weisser praise the "witty dialogue and very likable characters" and give the film a 3 star rating (out of four). 

==Release==
Kandagawa Pervert Wars was released theatrically in Japan in August 1983 by Million Film  and published as a DVD on April 26, 2004 by AceDeuce ( ). 

==Further reading==
*  
*  

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 