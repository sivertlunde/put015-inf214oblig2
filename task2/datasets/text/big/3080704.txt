Cronos (film)
 
 
{{Infobox film
| name           = Cronos
| image          = Cronos.jpg
| alt            =
| caption        = Theatrical release poster
| director       = Guillermo del Toro
| producer       = Arthur H. Gorson Bertha Navarro Alejandro Springall
| writer         = Guillermo del Toro
| narrator       = Jorge Martínez de Hoyos
| starring       = Federico Luppi Ron Perlman Claudio Brook Margarita Isabel Tamara Shanath Javier Álvarez
| cinematography = Guillermo Navarro
| editing        = Raúl Dávalos
| studio         = Fondo de Fomento Cinematográfico Instituto Mexicano de Cinematografía Universidad de Guadalajara Iguana Producciones Ventana Films
| distributor    = Prime Films S.L.   October Films  
| released       =  
| runtime        = 92 minutes  
| country        = Mexico
| language       = Spanish English
| budget         = $2 million 
| gross          = $621,392   
}} vampire horror first feature film, and the first of several films on which he collaborated with Luppi and Perlman. 

== Plot ==
In the year 1536, an alchemist in Veracruz developed a mechanism that could give eternal life. In 1937, an old building collapsed and the alchemist with marble white skin is killed when his heart is pierced by the debris. Investigators never revealed what else was discovered in the building: basins filled with blood from a corpse.

In the present, an old antique dealer, Jesús Gris, notices that the base of an archangel statue is hollow. He opens it and finds a 450-year-old mechanical device in the base. After winding the ornate, golden, scarab-shaped device, it suddenly unfurls spider-like legs that grip him tightly, and it inserts a needle into his skin which injects him with an unidentified solution.

A living insect — entombed within the device and meshed with the internal clockwork — produces the solution. However, Gris is unaware of this detail until later. Eventually, he discovers that his health and vigor are returning in abundance, as is his youth. His skin loses its wrinkles, his hair thickens and his sexual appetite increases. He also develops a thirst for blood. This at first disgusts him, but he eventually succumbs to the temptation.

Meanwhile, a rich, dying businessman, Dieter de la Guardia, who has been amassing information about the device for many years, has been searching for the archangel statue with the cronos device. He has appropriated several archangels already. He sends his thuggish nephew, Angel, to purchase the archangel at the antique shop.

During a party, Gris sees blood on a mens room floor and decides to lick it. Angel then finds Gris and tries to beat him into giving up the device. When Gris faints, Angel places his body inside a car and pushes it off a cliff. Gris dies but later revives and escapes from an undertakers establishment before he can be cremated. He later reads the program for his funeral and opens his mouth which had been sewn shut. He returns to his home where his granddaughter, Aurora, lets him in. He works on a letter to his wife where he comments on the changes that his body has made and tells her that after completing some unfinished business he will return to her. He notices that his skin burns in the presence of sunlight and sleeps in a box to avoid it.

Eventually, he and Aurora bring the device to Dieters business headquarters, where the businessman offers him a "way out" in exchange for the device. Gris comments on his damaged skin and the businessman tells him to peel it off because he has new skin underneath, which is marble white like the dead alchemist. Gris agrees to hand it over in exchange for knowing the "way out", whereupon Dieter stabs him. Before being able to strike the killing blow to the chest, Dieter is incapacitated by Aurora. The mortally wounded Dieter is found and killed by his nephew, Angel, who is tired of waiting for his inheritance. Angel confronts Gris on the rooftop of the building and beats him severely. Gris throws them both off the roof, killing Angel. Upon awakening, Gris destroys the device after he is tempted to feed off his granddaughter and returns home. Gris now with marble white skin, lies dying in bed. Aurora and his wife are by his side.

== Cast ==
* Federico Luppi as Jesús Gris
* Ron Perlman as Angel de la Guardia
* Claudio Brook as Dieter de la Guardia
* Tamara Shanath as Aurora Gris
* Margarita Isabel as Mercedes We Are What We Are.
* Mario Iván Martínez as Alchemist
* Farnesio de Bernal as Manuelito
* Jorge Martínez de Hoyos as Narrator

== Reception ==
=== Critical response ===
The film was very well received by critics;  .   
 Time Out conducted a poll with several authors, directors, actors and critics who have worked within the horror genre to vote for their top horror films.  Cronos was placed at number 96 on their top 100 list. 

=== Box office ===
In North America, the film was given limited release to 2 theaters where it grossed $17,538 its opening weekend and grossed a total of $621,392 playing at a total of 28 screens. 

== Home media == audio commentaries by cast and crew, a video tour of del Toros home office, several interviews, and Geometria (film)|Geometria, a short film written and directed by del Toro. 

== See also ==
* Vampire film

== References ==
 

== External links ==
*  
*  
*  
*  
*   by Maitland McDonagh

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 