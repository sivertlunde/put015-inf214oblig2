Amba (film)
{{Infobox film
| name           = Amba
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Mohan Kumar
| producer       = Mohan Kumar
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Anil Kapoor Meenakshi Seshadri
| music          = Laxmikant-Pyarelal
| cinematography = K.K. Mahajan
| editing        = 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
}}

Amba is a 1990 Indian Bollywood film produced and directed by Mohan Kumar. It stars Anil Kapoor and Meenakshi Seshadri.

==Cast==
* Anil Kapoor as Suraj Sarju B. Singh
* Meenakshi Seshadri as Lajjo
* Shabana Azmi as Amba Bhanupratap Singh
* Kiran Juneja as Prabha R. Singh Kanwaljit Singh as Rajendra Raja B. Singh 
* Rajan Sippy as Kunwar Ranvir J. Singh
* Mangal Dhillon as Thakur Shamsher Singh

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Sheron Wali Mata Ka Jab Naam"
| Suresh Wadkar, Kavita Krishnamurthy
|-
| 2
| "Maa Ka Man Mamta Ka Mandir"
| Mohammed Aziz, Anuradha Paudwal
|-
| 3
| "Ek Swarg Hai Aasman Par"
| Suresh Wadkar, Mohammed Aziz, Kavita Krishnamurthy, Anuradha Paudwal
|-
| 4
| "Laga Ragda To Mit Gaya Jhagda"
| Mohammed Aziz, Alka Yagnik
|-
| 5
| "Sun Meri Shehzadi"
| Mohammed Aziz, Alka Yagnik
|-
| 6
| "Aurat Zaat Mard Se Jhagda"
| Kishore Kumar
|}

==External links==
* 

 
 
 
 


 