Inhale (film)
 
{{Infobox film
| name           = Inhale
| image          = Inhale.jpg
| caption        = 
| director       = Baltasar Kormákur
| writer         = 
| starring       = Dermot Mulroney Diane Kruger Sam Shepard
| music          = James Newton Howard
| cinematography = Óttar Guðnason
| editing        = Elísabet Ronaldsdóttir
| studio         = 
| distributor    = IFC Films
| released       =  
| runtime        = 83 minutes
| country        = United States
| language       = English Spanish
| budget         = $10,000,000 
| gross          = $55,089 
}}
Inhale is a 2010 thriller film directed by Baltasar Kormákur. It stars Dermot Mulroney and Diane Kruger. 

==Plot==

Paul Stanton (Mulroney) and his wife  Diane (Diane Kruger) are a couple whose daughter is dying from a rare degenerative lung condition. The only thing that can save her life is a lung transplant from an organ donor. The movie starts when the donated lung fails to arrive correctly. The movie goes back and forth between the present and the events leading up to it. In the present Day, Paul goes to Juarez Mexico to find a man called Dr. Navarro. After leaving his cell number at many local hospitals, somebody finally contacted Paul. 
In the past, Dr. Rubin (Arquette) whos treating their daughter (Stallard) had given them information regarding Pauls associate, Harrison (Shepard), who somehow managed to get an organ transplant for himself, possibly from the black market. Paul confronted Harrison and finally managed to get Dr. Navarros name from him. Harrison told him that he was contacted by Navarro and he didnt know how to get a hold of him.

Back to the Present Day, Paul is ambushed by a couple of Navarros guys that tell him to stay away. After an arduous search, Paul finally finds out that Dr. Navarro is actually Dr. Martinez (Perez) from a local hospital that he visited previously for his wounds. Paul confronts Martinez and asks him to find a lung donor for his daughter, who only has one week to live. Martinez agrees to help him for a $250,000 cash payment. 

The next day, Martinez tells Paul that he has found a matching donor for his daughter. Paul tells Diane to go to Juarez and prepare their daughter for the transplant operation. Little does Paul know that the donor is actually a live person. Martinez and his men have planned a hit-and-run accident to get the lung implant from a local street boy. Paul discovers the fact and tells Diane about this. Diane tells him that she does not want to know where the donor comes from. Paul confronts Martinez at the operating table. Martinez tells Paul to make a choice: to go ahead with the operation or disband the operation knowing that they wont be able to find another match in time to save his daughters life. The next scene shows Paul and Diane at their daughters funeral. Diane gives Paul an accusing look blaming him for their daughters death. Meanwhile, elsewhere, the local boy that was supposed to be the organ donor is alive and playing soccer with his friends.

==Cast==
*Dermot Mulroney as Paul Stanton
*Diane Kruger as Diane Stanton
*Sam Shepard as James Harrison
*Jordi Mollà as Aguilar
*Vincent Pérez as Dr. Martínez
*Rosanna Arquette as Dr. Rubin
*Kristyan Ferrer as Miguel
*Mia Stallard as Chloe
*Abraham Chaidez as Luis Ruiz

==Release==
Inhale was featured at the Hamptons International Film Festival and had a limited wide release in the United States in October 2010.

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 