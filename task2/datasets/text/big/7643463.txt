Double Dragon (film)
{{Infobox film
| name          = Double Dragon
| image         = Double Dragon 1994 movie poster.jpg
| image_size =
| caption       =
| director       = James Yukich
| producer     =
| writer          = Michael Davis Peter Gould Mark Brazill (uncredited)
| narrator      =
| starring       = Mark Dacascos Scott Wolf Robert Patrick Alyssa Milano Julia Nickson Kristina Wagner John Mallory Asher Leon Russom Jay Ferguson
| cinematography = Gary B. Kibbe
| editing        = Florent Retz
| studio         = Imperial Entertainment Group
| distributor   = Gramercy Pictures Universal Pictures
| released     =  
| runtime       = 95 minutes
| country       = United States
| language    = English
| budget        = $7.8 million 
| gross          = $2.3 million
}} 1994 live-action same name punk environment.

== Plot summary ==
Koga Shuko, a crime lord and businessman, explains to his underlings about a powerful, magic medallion called the Double Dragon, which has been split into two pieces. He obtains one half and orders his henchmen to find him the other.

Teenage brothers Billy and Jimmy Lee, and their guardian Satori head home after citywide curfew from a martial arts tournament. On their way, they are accosted by gang members, who rule the streets after dark due to an uneasy pact made with the police department to keep them from running amok during the day.  They get away thanks to help from the Power Corps, a group of vigilantes headed by their friend Marian, daughter of the police chief. Unfortunately, Abobo, a gang leader, discovers Satori holds the second medallion half and reports this to Shuko.  For his failure in securing it, he is mutated into a hulking giant.
 possession and a shadow form,  by temporarily possessing Satori.  Billy and Jimmy successfully incapacitate Abobo, but Shuko has the place doused in gasoline and lit on fire. Satori sacrifices herself so the brothers can escape with the Dragon.

Unable to find the brothers on his own, Shuko unites and takes over the gangs by displaying his power and sends them after the Lees.  Billy and Jimmy narrowly get away, and seek refuge in the Power Corps hideout. Marian agrees to help them, using this as an opportunity to get rid of the gangs once and for all, and the three of them decide to go to Shuko’s office building to steal his medallion. They ultimately fail and are forced to flee, but Jimmy is captured in the process.

Billy and Marian return to the Power Corps base, and lament about how none of them have been able to figure out how to use their Dragon piece.  Marian points out a discovery they made that the wearer of the medallion is immune to the powers of its counterpart, meaning Shuko is not able to possess him as long as he has it.  Suddenly, the gangs attack the hideout.  In the mélêe, Jimmy reappears.  Billy is elated; however, Jimmy is merely being controlled by Shuko as he tries to pummel his brother into submission.  Billy then accidentally activates his medallion’s ability, which is the power of the body and effectively makes him invulnerable to harm.  Knowing this, Shuko threatens to kill Jimmy instead.  This doesn’t succeed either, so he releases Jimmy to distract Billy long enough to get the medallion.

Shuko unites the halves and turns into a pair of shadow warriors with katanas that disintegrate anything they slice through.  The Lee brothers fight, but cannot beat him. Abobo, who had previously been taken prisoner and since reformed, reveals to Marian that Shuko’s weakness is light.  Marian reactivates the hideout’s generator, and the shadow warriors are rendered powerless.  Billy and Jimmy attack, forcefully recombining the shadows into Shuko, and are given the Double Dragon halves.  They combine the two pieces, granting them matching uniforms and the medallion powers, and they briefly see a vision of Satori’s spirit as she gives them encouraging words.  The brothers pummel Shuko, and Jimmy possesses him to make him do embarrassing things. During this, Marian’s father shows up, intending to bring Shuko to justice and take care of the gangs once and for all.  Jimmy has Shuko write a check to the police department for $129 million before encouraging the police chief to arrest him.

Shuko is sent to jail, the police department has renewed strength to fight the gangs instead of compromising with them, and Billy and Jimmy can now keep both halves of the medallion safe.

== Cast ==
* Scott Wolf as Billy Lee, the younger Lee brother. Wears a blue outfit in the end. Originally the Player 1 character in the video games.
* Mark Dacascos as Jimmy Lee, the elder Lee brother. Wears a red outfit in the end. Originally the Player 2 character in the video games.
* Alyssa Milano as Marian Delario, the leader of the Power Corps. Originally the kidnapped woman in the arcade game, the film version of Marian is a more active heroine compared to her video game counterpart. 1995 Double Dragon fighting game based on the film.
* Julia Nickson as Satori Imada, the adoptive mother of Billy and Jimmy.
* Kristina Wagner as Linda Lash, Shukos henchwoman. Linda was originally an enemy character from the video game.
* Nils Allen Stewart as Bo Abobo, the leader of a street gang known as Mohawks. Abobo was another enemy character from the video game.
* Henry Kingi plays the mutated Bo Abobo during the later part of the film, who reforms and tries to befriend the Lee brothers and Marian at the end. George Hamilton appears as an anchorman.
* Vanna White appears as an anchorwoman.
* Andy Dick appeared as a weatherman who deals with the "fogcast", giving warnings over (implied acidic and radioactive) black rain.
* Cory Milano as Marc Delario, Marians younger brother.

==Production==
First-time director Jim Yukich summarized his approach to the film: "Our characters are like normal kids - three kids on an adventure, so we didnt want to make something that kids would almost be too afraid to see. ... Id like to make it in a funnier, light-hearted vein."   

The boat chase sequence was filmed on the Cuyahoga River in Northeast Ohio, and climaxes with an explosion which used 700 gallons of gasoline combined with 200 gallons of alcohol.  Despite warnings the night before on several news channels, the explosion caused residents of the nearby city to panic, leading to 210 phone calls to emergency services over ten minutes. 

==Reception==
Reviews by critics, such as the review of the movie by the Washington Post, were not favorable.  Reviewbiquity gave the film one out of five stars, stating the movie "won’t satisfy even the most fervent of fans."  On review aggregate website Rotten Tomatoes, it received a negative score of 9% from 11 reviews, with the consensus "Double Dragon s clever use of special effects cannot mask the films overly simplistic storyline and cheesy dialogue", making it one of the lowest-rated video game movie adaptions of all time.   

In 2009, Time (magazine)|Time listed the film on their list of top ten worst video game movies. 

== Box office ==
According to Box Office Mojo, The film grossed $1,376,561 in its opening weekend at 1,087 theaters and $2,341,309 in its finished theatrical run.

== Home video == Universal released the film on VHS and Laserdisc in April 1995 in the United States, while CFP released the film on video in Canada. GoodTimes Entertainment made another VHS in late 1997, released it on DVD in 2001, and another DVD on August 31, 2004. Both DVDs are currently out of print.  

== Related media == Double Dragon produced by Technos Japan. This includes the transformation that the Lee brothers go through during the films climax, which appear in the game as a special move for both characters; and the use of footage of the film in the games introduction and Marians stage.

== References ==
 

== External links ==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 