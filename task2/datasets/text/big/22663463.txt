Young Widow
{{Infobox film
| name           = Young Widow
| image          = Young Widow.jpg
| caption        = Theatrical release poster
| director       = Edwin L. Marin
| producer       = Hunt Stromberg
| writer         = Clarissa Fairchild Cushman (novel) Richard Macaulay (writer)
| narrator       =
| starring       = Jane Russell Louis Hayward
| music          = Carmen Dragon
| cinematography = Lee Garmes
| editing        = John M. Foley
| distributor    = Hunt Stromberg Productions
| released       = March 4, 1946
| runtime        = 100 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Young Widow is a 1946 drama film directed by Edwin L. Marin, starring Jane Russell and Louis Hayward. It focuses on Joan Kenwood, a young journalist who cant get over her husbands death in World War II. Kenwood is reminded in large ways and small of her late husband during every one of her assignments. 

With The Outlaw still being withheld from general release, Young Widow represented the first time that most filmgoers ever saw a 25-year-old Jane Russell on the screen.

==Plot== Air Corps  photographer husband was killed on an air mission, returns to New York  City from England. The managing editor of the newspaper for which she worked, Peter Waring (Kent Taylor),  offers Joan work, but she despondently rejects it and instead stays with two aunts on their farm in Virginia.  Unable to stop thinking about the death, however, she decides to return to New York. 
 Marie Wilson), a show girl who has just returned from entertaining the troops.  A number of military men drop in on the apartment as Joan arrives, all invited by the scatter-brained Mac.  Jim learns where Joan is staying, shows up too, and sees opportunity in the situation. Later, everyone goes out to a café. While Jim and Joan are dancing, her husband’s favorite song is played, and a distraught Joan leaves. Jim follows and takes her home. When he bluntly suggests that she get over the man she is in love with, Joan explains that the man is her husband, who was killed over Berlin. Ashamed, Jim returns to his base at Mitchel Field on Long Island, where he is awaiting orders for the Pacific.

The next day, as Joan is leaving the apartment, she encounters a remorseful Jim. After she accepts his apology, Jim accompanies her to the Rapid transit|subway. While waiting for the train, Jim saves the life of an elderly woman who falls on the tracks. Joans reporter instincts take over, and she investigates the story and offers it to the paper. Delighted, Peter promptly puts her on the payroll. She and Jim pursue an easy-going courtship when he receives a 72-hour pass.

Jim receives a telegram ordering him to report for cholera inoculation|shots. He proposes to Joan, but still haunted by her husband, she rejects him saying that "it will always be this way." A few days later, Peg’s husband returns after losing his leg in combat, and moved by seeing them together, Joan decides to tell Jim that she will wait for him. Peter drives her to the airfield, but Jims outfit is already taking off. She waves frantically at him from outside the gate as he takes off, and as he passes by, mouths the words that she loves him and will wait for him.

==Cast==
 
 
* Jane Russell as Joan Kenwood
* Louis Hayward as Lt. Jim Cameron
* Faith Domergue as Gerry Taylor Marie Wilson as Mac McCallister
* Kent Taylor as Peter Waring
* Penny Singleton as Peg Martin
* Connie Gilchrist as Aunt Cissie
* Cora Witherspoon as Aunt Emeline Norman Lloyd as Sammy Jackson
  Steve Brodie as Willie Murphy
* Richard Bailey as Bill Martin
* Robert Holton as Bob Johnson
* Peter Garey as Navy Lieutenant Smith
* Bill Moss as Marine Lieutenant Pearson (as William Moss) William Murphy as Army Lieutenant Hope (as Bill Red Murphy)
 

==Production==
The film went over budget by $600,000 and was a box office failure. {{cite book
 | last = Balio
 | first = Tino
 | authorlink =
 | coauthors =
 | title = United Artists: The Company Built by the Stars
 | date = 2009
 | publisher = University of Wisconsin Press
 | isbn = 978-0-299-23004-3
 }} p203 
==References==
 

==External links==
*  
*  
*  
 

 
 
 
 
 