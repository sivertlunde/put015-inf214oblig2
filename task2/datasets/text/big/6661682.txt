Swayamvaram
 
 

 
 
{{Infobox film
| name = Swayamvaram
| image = Swayamvaram.jpg
| alt = The films poster showcasing the lead actress, Sharada, and films title in Malayalam
| caption = Theatrical release poster
| director = Adoor Gopalakrishnan
| producer = Chitralekha Film Co-operative
| writer = Adoor Gopalakrishnan K. P. Kumaran
| screenplay =
| story = Adoor Gopalakrishnan
| starring =  
| music = M. B. Sreenivasan
| cinematography = Mankada Ravi Varma
| editing = Ramesan
| distributor = Chitralekha Film Co-operative
| released =  
| runtime = 131 minutes
| country = India
| language = Malayalam
| budget =     
}} Madhu and Sharada in the lead roles. Notable smaller roles were played by Thikkurisi Sukumaran Nair, Adoor Bhavani, K. P. A. C. Lalitha, and Bharath Gopi. The film depicts the life of a couple—Vishwam (Madhu) and Sita (Sharada)—who have married against their parents wishes and want to start a new life at a new place. The title is an allusion to the ancient Indian practice of a girl of marriageable age choosing a husband from among a list of suitors.

Swayamvaram marked several debuts—directorial of Gopalakrishnan, acting of the Malayalam star Bharath Gopi, and film producing of the Chitralekha Film Cooperative, an organisation cofounded by Gopoalakrishnan himself. The film features an original score by M. B. Sreenivasan, camerawork by Mankada Ravi Varma, and film editing by Ramesan. Writer-director K. P. Kumaran co-scripted the film with Gopalakrishnan. It took seven years for Gopalakrishnan to get the project rolling when his initial proposal for a loan to make a film was turned down by the Film Finance Corporation (FFC). The FFC later partially financed the film when Chitralekha provided the rest.
 new wave National Film Best Film, Best Director Best Actress for Sharada.

==Plot==
A newly wed couple, Vishwam (Madhu (actor)|Madhu) and Sita (Sharada (actress)|Sharada), have married against the preference of their families, and left their hometown. Both want to start a new life at a new place. Initially, they stay in a decent hotel but soon due to financial reasons they move to another, ordinary hotel.

Vishwam, an educated, unemployed youth, is an aspiring writer and had some of his short stories published in the newspapers earlier. He dreams of having his novel, titled Nirvriti (Ecstasy), published in the newspaper. He meets one of the newspaper editors (Vaikom Chandrasekharan Nair), who agrees to read his novel but declines to publish it as Vishwam does not have many writings to his credit. Sita is offered a job as a sales girl but cannot accept it because she is unable to pay the required security deposit of   1,000. With several unsuccessful attempts to get a job, the increasing financial pressure forces them to shift to a slum. With an old lady named Janaki (Adoor Bhavani) and a prostitute named Kalyani (K. P. A. C. Lalitha) as their neighbours, things do not work as desired for the couple and they end up selling Sitas jewellery.
 Sita Swayamvara and a closed door.

==Cast== Madhu as Vishwam Sharada as Sita
* Adoor Bhavani as Janaki
* K. P. A. C. Lalitha as Kalyani, a prostitute
* Thikkurissy Sukumaran Nair as a college principal
* P. K. Venukuttan Nair as Vasu, a small time smuggler
* Vaikom Chandrasekharan Nair as an editor
* Karamana Janardanan Nair
* Bharath Gopi as a dismissed employee

==Production==

===Title===
 
The title refers to the ancient Indian practice of a girl of marriageable age choosing a husband from among a list of suitors.  It was also an affirmation to one of Mohandas Karamchand Gandhis beliefs about an individuals right to make own choices.  The films English title for international release was mainly Ones Own Choice, however it was shown at the Moscow International Film Festival under the title, On Own Will.  Other translations of the Malayalam title have also been used, such as By Choice,  Her Own Choice,  Marriage by Choice,  Betrothal by Choice,  The Betrothal,  and The Selection. 

===Development=== New Wave movement of global cinema and formed a film society in Kerala, named "Chitralekha Film Cooperative", with his classmates in 1965.  Gopalakrishnan had initially submitted a romantic script Kamuki to the Film Finance Corporation (now National Film Development Corporation of India or NFDC) which they declined to finance.   Later, he submitted the script for Swayamvaram, which Film Finance Corporation accepted and approved the loan of a  . However, it took seven years for him to get the film rolling, after he passed out of the Film and Television Institute of India.  Gopalakrishnan co-scripted the film with writer-director K. P. Kumaran.  The film was produced by Chitralekha Film Co-operative, Keralas first film co-operative society for film production, with Swayamvaram being their first feature film production.   The films total budget was   and Gopalakrishnan used the money he had collected from his documentary productions. Initially, Chitralekha Film Co-operative, the producer of the film had trouble distributing the film, so they decided to do it by themselves. 

===Casting===

Gopalakrishan wanted fresh faces for both the lead roles and he had wrote letters to various heads of colleges and universities for the auditions. However, he did not receive any response from anywhere.  For the female lead, Gopalakrishnan approached Sharada (actress)|Sharada, one of the most successful actresses of her time. She was acting in commercial films when Gopalakrishnan asked her to star in Swayamvaram. Sharada was initially reluctant to commit herself to an art film, but agreed when Gopalakrishnan narrated the complete story to her at Prasad Studios, in Chennai.    The male lead for the film, Madhu (actor)|Madhu, was an old friend of Gopalakrishnan and had expressed a desire to act in one of his films. By the time Gopalakrishnan finished his studies and returned from Film and Television Institute of India|FTII, Madhu was already a star in Malayalam cinema. Gopalakrishnan then decided to cast him opposite Sharada. Mentioning about his experience working with Gopalakrishnan and Swayamvaram, Madhu recollected in an interview that "  when Gopalakrishnan narrated the story of Swayamvaram, I knew it was going to be different."  Years later he also mentioned that he "sometimes wished Prem Nazir had acted in Adoors Swayamvaram. He might have won a Bharath award. But he was very busy those days."   
 Best Actor award, then known as "Bharat Award", for the role at the 25th National Film Awards in 1977.   

K. P. A. C. Lalitha, who would later play notable characters in many of Gopalakrishnans films, played a small role as a prostitute in Swayamvaram.  Noted Malayalam writer and journalist Vaikom Chandrasekharan Nair played a newspaper editor in the film. Gopalakrishnan praised him for his performance, expressing the difficulties of performing as oneself onscreen. 

===Filming===
Due to financial crises, it took more than one and a half years for Gopalakrishnan to finish the film.  Swayamvaram was one of the first Malayalam films to use synchronised sound and to be filmed in outdoor locales, for which Gopalakrishnan used his Nagra audio recorder.    The film was shot in two schedules. It was delayed due to scheduling conflicts of the lead actress, Sharada. She was working in several films at that time, so Gopalakrishnan had to arrange the schedule to suit her convenience.  The lead actor, Madhu, mentioned in an interview that Gopalakrishnan was clear about his characters and their behaviour. Gopalakrishnan also used to discuss the shoot with his crew before the shooting.   
 original score by M. B. Sreenivasan. The editing of the film was done by Ramesan, whereas S. S. Nair and Devadathan worked together on the production design. Sound mixing was done by P. Devadas. The total budget of the film was  , where the Film Finance Corporation provided   150,000 as a loan. 

==Reception==

===Theatrical=== National Film Awards, the film was re-released in theatres and gathered better response this time, which also helped Gopalakrishnan repay the loan to FFC, the main producer of the film.  The film participated in the competition section of the 8th Moscow International Film Festival in 1973.   

===Critical=== National Award winning film, Chemmeen (1965), Swayamvaram marked a turning point in his career. 

Although most of the reviews were positive, some film experts were critical about the film. Amaresh Datta, in his book The Encyclopaedia of Indian Literature, criticised the film for "following the neo-realistic style" and showcasing "same old love story without any freshness added".  Poet and journalist C. P. Surendran called the film "disturbing" in one of his articles, criticising Gopalakrishnan and his films.  Shyam Benegal, a film director and a well-known admirer of Gopalakrishnans films, also mentioned that he was not particularly pleased with Swayamvaram.  Some critics have pointed out its resemblance to Ritwik Ghataks Subarnarekha (film)|Subarnarekha (1965). Gopalakrishnan agreed on influences of Ghatak and Satyajit Ray but pointed that Swayamvarams treatment is different from Ghataks Subarnarekha as Swayamvaram is more about the trip. 

The film received little critical response in Kerala, which Gopalakrishnan referred to as "more of a question of insensitivity rather than personal enmity." However, some noted critics like Moorkoth Kunhappa and T. M. P. Nedungadi praised the movie, with Nedungadi writing a response titled "Swayamvaram over, what next in Malayalam cinema?" 

==Legacy==
Swayamvaram pioneered the new wave cinema movement in Malayalam cinema.     The film did not feature any dance numbers, comedy or melodramatic scenes, which were "usual ingredients" of films at that time, but it introduced viewers to then unknown techniques of film presentation, in which it was not merely used for "story-telling".   It was an inevitable development for Malayalam cinema, as the film focused mainly on cinema rather than its story. The film also introduced film-goers to a new cinematic art through the impulses generated by the film, which in turn were more important than the film itself.  The film is also said to have divided Malayalam films into three different categories, "uncompromising art films", "compromising films" which aimed at commercial success but tried maintaining a good deal of the artistic qualities, and the "commercial films" which purely aimed at box office success.  Through Swayamvaram, Gopalakrishnan became the first Indian director to use sound as a leitmotif (a recurring musical theme).  The film provided a new experience to Indian cinema-goers, as it used ample amounts of natural sounds with a minimalistic background score.   

==Digital restoration==
The National Film Archive of India has digitally restored the film, and the restored version with improved subtitles in English was screened at the International Film Festival of India in November 2012. Also, the University of Wisconsin–Milwaukee has acquired all of Gopalakrishnans features, including Swayamvaram, to restore and preserve. 

==Awards== 1973 Moscow International Film Festival (Russia) 
* Adoor Gopalakrishnan - Nominated
 1973 National National Film Awards (India)   
* National Film Award for Best Feature Film
*  
*  
*  

;1973 Kerala State Film Awards (India) 
*   Kerala State Film Award for Best Art Direction: Devadathan

==References==
 

==Bibliography==
 
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 
 