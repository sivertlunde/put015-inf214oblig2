Colpo di luna
{{Infobox film
| name           = Colpo di luna
| image          = Colpo di luna.jpg
| alt            =
| caption        =
| director       = Alberto Simone
| producer       =
| writer         = Alberto Simone Gioia Magrini Dido Castelli
| starring       = Tchéky Karyo Nino Manfredi Isabelle Pasco Jim van der Woude Johan Leysen Mimmo Mancini Paolo Sassanelli
| music          = Vittorio Cosma
| cinematography = Roberto Benvenuti Romolo Eucalitto
| editing        = Enzo Meniconi Kohout
| studio         =
| distributor    =
| released       =  
| runtime        = 86 minutes
| country        = Italy
| language       = Italian
| budget         =
| gross          =
}}
Colpo di Luna is a 1995 Italian drama film written and directed by Alberto Simone. It won a David di Donatello for Best New Director. It was released in the US in 2000 under the name Moon Shadow. It was entered into the 45th Berlin International Film Festival where it won an Honourable Mention.   

==Plot==
Lorenzo, an astrophysicist, has inherited a house in Sicily. He has it restored by Salvatore, an introverted carpenter. Salvatore starts the job with help of his handicapped son. Since work seems to be slow Lorenzo goes and checks on them. He finds out Salvatore has turned the house into a therapeutic community for troubled people. At first Lorenzo is angry but then his good heart prevails.

==Cast==
* Tchéky Karyo: Lorenzo
* Nino Manfredi: Salvatore
* Isabelle Pasco: Luisa
* Jim van der Woude: Agostino
* Johan Leysen: Titto
* Mimmo Mancini: Filippo
* Paolo Sassanelli: Michele

==References==
 

==External links==
 

 
 
 
 
 
 


 