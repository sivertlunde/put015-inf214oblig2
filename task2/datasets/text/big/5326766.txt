Les Rendez-vous de Paris
 
{{Infobox film
| name           = Les Rendez-vous de Paris
| image_size     = 
| image	=	Les Rendez-vous de Paris FilmPoster.jpeg
| caption        = 
| director       = Éric Rohmer
| producer       = 
| writer         = 
| narrator       = 
| starring       = Clara Bellar Serge Renko
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1995
| runtime        = 94 min.
| country        = France French
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 portmanteau French film directed by Éric Rohmer.
 Picasso painting, and centres on an artist who is attracted by a stranger. The three stories of the film are linked by a girl singing in the streets to an accordion accompaniment - a homage to René Clairs Sous les toits de Paris.

== Cast ==

=== Le Rendez-vous de 7 heures ===
* Clara Bellar : Esther
* Antoine Basler : Horace
* Mathias Mégard : Flirt
* Judith Chancel : Aricie
* Malcolm Conrath : Félix
* Cécile Parès : Hermione
* Olivier Poujol : Boy at Café

=== Les Bancs de Paris ===
* Aurore Rauscher : Elle
* Serge Renko : Lui

=== Mère et enfant, 1907 ===
* Michael Kraft : The Painter
* Bénédicte Loyen : The Young Woman
* Veronika Johansson : Swedish Woman

=== Mouffetard-Musette ===
* Florence Levu
* Christian Bassoul

== External links ==
*  
*  

 

 
 
 
 
 
 


 
 