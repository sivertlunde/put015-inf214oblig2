I Could Read the Sky
{{Infobox film
| name = I Could Read the Sky
| image =
| caption =
| director = Nichola Bruce
| producer = Janine Marmot
| writer = Nichola Bruce
| starring = Dermot Healy Maria Doyle Kennedy Stephen Rea
| music = Iarla Ó Lionáird
| distributor = Artificial Eye
| budget =
| released =  
| runtime = 84 minutes Ireland
| language = English
}}
 Irish  film directed by Nichola Bruce.  It is based upon a photographic novel by Timothy OGrady and Steve Pyke, which concerns the Irish experience of emigration and exile. It has been described as an "innovative, melancholic, and deeply moving film". 

==Plot==
The film concerns an old Irish immigrant living in London who is looking back over his life. He recalls his early life in the west of Ireland, his first love, emigrating to England, searching for his brother Joe, who disappeared after he emigrated several years previously. His marriage and wifes later depth is also remembered.

==Cast==
*Dermot Healy as the old man
*Maria Doyle Kennedy as Maggie
*Stephen Rea as PJ Doran
*Brendan Coyle as Francie
*Liam OMaonlai as Joe

Also
*Pádraic Breathnach
*Mick Lally

==Production==
The film was based on a photographic novel of the same name by Timothy OGrady and Steve Pyke. OGrady lived for some time in the west of Ireland and was interested in the stories of Irish immigrants. In the authors words:
 "I Could Read The Sky is a novel which tells its story through words and photographs. The story is that of an Irish emigrant struggling to possess his life in acts of memory. He is old. He is alone. He is lying in bed at night in the darkness remembering a life of dislocation, of loss, of descent into madness and of redemption through music and through the love of a woman. Some of the time he is remembering on prose and some of the time in pictures. Neither is meant to illustrate the other. They are distinct acts of memory in their own right. The act of remembering itself becomes a way of completing his life."  
 sean nós Noel Hill and Liam OMaonlai also contributed to the soundtrack.

==References==
 

 
 
 