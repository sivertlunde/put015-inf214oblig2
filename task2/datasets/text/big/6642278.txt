The Life and Times of Judge Roy Bean
 
 
{{Infobox film
| name = The Life and Times of Judge Roy Bean
| image = Life and Times of Judge Roy Bean.jpg
| image_size     = 225px
| caption = Theatrical release poster by Richard Amsel
| director = John Huston John Foreman
| writer = John Milius
| starring = Paul Newman Jacqueline Bisset Anthony Perkins Victoria Principal
| music = Maurice Jarre Richard Moore
| editing = Hugh S. Fowler
| studio = First Artists
| distributor = National General Pictures
| released =  
| runtime = 120 minutes
| country = United States
| language = English
| budget =
| gross = $16,530,578 
}}
The Life and Times of Judge Roy Bean is a 1972 American western film written by John Milius, directed by John Huston, and starring Paul Newman (at the height of his career, between Butch Cassidy and the Sundance Kid and The Sting). It was loosely based on the Judge Roy Bean|real-life, self-appointed frontier judge. 

==Plot==
An outlaw, Roy Bean, rides into a West Texas border town called Vinegaroon by himself. The customers in the saloon beat him, rob him, toss a noose around him and let Beans horse drag him off.

A young woman named Maria Elena finds and helps him. Bean promptly returns to town and shoots all those who did him wrong. With no law and order, he appoints himself judge and "the law west of the Pecos" and becomes the townspeoples "patrone."

Bean renames the saloon The Jersey Lilly and hangs a portrait of a woman he worships but has never met, Lillie Langtry, a noted actress and singer of the 1890s. When a band of thieves come to town (Big Bart Jackson and gang members Nick the Grub, Fermel Parlee and Whorehouse Lucky Jim), rather than oppose them, Bean swears them in as lawmen. The new marshals round up other outlaws, then claim their money after Bean sentences them to hang.

Dispensing his own kind of frontier justice, Bean lets the marshals hang Sam Dodd and share his money. When a drunk shoots up a saloon, Bean doesnt mind, but when Lilys portrait is struck by a bullet, the fellow is shot dead on the spot. Prostitutes are sentenced to remain in town and keep the marshals company.

Maria Elena is given a place to live and fine clothes ordered from a Sears Roebuck catalog. A mountain man called Grizzly Adams gives her and Bean a bear as a pet. As soon as lawyer Frank Gass shows up claiming the saloon is rightfully his, Bean puts him in a cage with the bear.

Bean goes off to San Antonio, leaving a pregnant Maria Elena behind and promising her a music box that plays "The Yellow Rose of Texas." In his absence, Gass and the prostitutes conspire to seize control of the town from the judges hard rule. A dapper Bean tries to see Lily Langtrys show, but it is sold out. He is deceived by men who knock him cold and steal his money.

Upon his return, Bean finds that Maria Elena is dying following a difficult childbirth. He names the baby Rose after the music boxs song. He also plans to hang the doctor, but Gass, who has been elected mayor, overrules him. Bean is sorrowful about losing Maria Elena and rides away. Gass brings in hired guns to get rid of Beans marshals.

Years go by. Oil rigs have been built around the prospering town. A grown-up Rose is surprised one day to look up and find Bean has returned. A shootout follows, Gass is killed and a fire engulfs the saloon, where the burning roof collapses on Bean.

A train pauses by the town, a considerable time later. Out steps Lily Langtry. She is told the story of Judge Roy Bean and his feelings toward her. She concludes that he must have been quite a character.

==Cast==
* Paul Newman as Judge Roy Bean
* Victoria Principal (film debut) as Maria Elena
* Anthony Perkins as Reverend LaSalle
* Ned Beatty as Tector Crites
* Jacqueline Bisset as Rose Bean
* Tab Hunter as Sam Dodd Grizzly Adams
* Ava Gardner as Lillie Langtry
* Richard Farnsworth as Outlaw
* Stacy Keach as Bad Bob
* Michael Sarrazin as Roses husband
* Roddy McDowall as Frank Gass
* Anthony Zerbe as Opera House hustler who mugs Bean
* Mark Headley as Billy the Kid
* Frank Soto as Mexican leader
* Jim Burk as Big Bart Jackson Matt Clark as Nick the Grub
* Bill McKinney as Fermel Parlee
* Steve Kanaly as Lucky Jim
* Francesca Jarvis as Mrs. Jackson
* Karen Carr as Mrs. Grub
* Lee Meza as Mrs. Parlee
* Dolores Clark as Mrs. Lucky Jim
* Neil Summers as Snake River Rufus Krile
* June Towner as Dorothy Pilsbury
* Jack Colvin as Pimp
* Howard Morton as Photographist
* Billy Pearson as Billy the Station Master
* Stan Barrett as Killer
* Dean Casper as Hotel desk clerk
* Don Starr as San Antonio Opera House manager
* Alfred G. Bosnos as Opera House clerk
* John Hudkins as Man at Opera House stage door
* Ken Freehill as Bedfellow
* Duncan Inches as Man at Vinegaroon
* Rusty Lee as Tuba player
* Roy Jenson as Outlaw
* Gary Combs as Outlaw
* Fred Brookfield as Outlaw
* Bennie E. Dobbins as Outlaw
* Leroy Johnson as Outlaw
* Fred Krone as Outlaw
* Terry Leonard as Outlaw
* Dean Smith as Outlaw
* Margo Epper
* Jeannie Epper
* Stephanie Epper
* Barbara J. Longo

==Production==
The film was based on an original script by John Milius, who hoped to direct. The script was sent to  , Uni of California 2006 p 287 

==Reception==
The film earned estimated North American rentals of $7 million in 1973. 

==Awards==
* 1973 Academy Award for Best Original Song, nomination for the song "Marmalade, Molasses and Honey" (Maurice Jarre, Marilyn Bergman, Alan Bergman)
* 1973 Golden Globe Award for Best Original Song, nomination for the song "Marmalade, Molasses and Honey" Most Promising Newcomer, Female, nomination for Victoria Principal

==References==
 

== External links ==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 