Story of a Beloved Wife
 
{{Infobox film
| name           = Story of a Beloved Wife
| image          = Aisai Monogatari.jpg
| caption        = Screenshot
| director       = Kaneto Shindo
| producer       = 
| writer         = Kaneto Shindo
| starring       = Nobuko Otowa
| music          = 
| cinematography = Yasukazu Takemura
| editing        = 
| distributor    = 
| released       =  
| runtime        = 97 minutes
| country        = Japan
| language       = Japanese
| budget         = 
}}

  is a 1951 Japanese drama film written and directed by Kaneto Shindo. It was Shindos debut film as a director.    It is an autobiographical work based on Shindos first marriage. Jūkichi Uno stars as a struggling screenwriter, and Nobuko Otowa stars as the wife who supports him through his early struggles.

==Plot==
Yamazaki, an aspiring screenwriter, played by Jūkichi Uno, is boarding with a couple and their daughter. He and the daughter become involved romantically and the father asks him to leave, and tells the daughter not to marry Yamazaki because of his insecure line of work.

In the end the wife dies of tuberculosis.

==Cast==
* Nobuko Otowa as Takako Ishikawa
* Jūkichi Uno as Keita Numazaki
* Ichirō Sugai as Masuda, director of the film studio
* Ryōsuke Kagawa as Kōzō Ishikawa
* Masao Shimizu as Masuda
* Yuriko Hanabusa as Yumie Ishikawa
* Osamu Takizawa as Film Director Sakaguchi
* Saburō Date as Sakunosuke Ōta

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 