This Movie Is Broken
{{Infobox film
| name           = This Movie Is Broken
| image          = This-movie-is-broken.jpg
| alt            =  
| caption        = Theatrical release poster Bruce McDonald
| producer       = Rhombus Media Shadow Shows
| screenplay     = Don McKellar Bruce McDonald Kevin Drew
| based on       =  
| starring       = Broken Social Scene Greg Calderone Georgina Reilly Kerr Hewitt
| music          = Broken Social Scene
| cinematography = John Price
| editing        = Matthew Hannam Gareth C. Scales
| studio         = 
| distributor    = Alliance Films (Canada) E1 Entertainment (International)
| released       =  
| runtime        = 85 minutes
| country        = Canada
| language       = English
| budget         = 
| gross          = 
}}
  Bruce McDonald. A cross between a romantic comedy and a concert film, the film stars Greg Calderone as Bruno, a young man hoping to convince his longtime crush Caroline (Georgina Reilly) to become his girlfriend by taking her to a Broken Social Scene concert at Harbourfront Centre|Harbourfront. 
 2009 Toronto strike.

The films theatrical premiere was held at Torontos NXNE festival in 2010. 

==Plot==
Bruno wakes up in bed next to Caroline, his long-time crush. But tomorrow shes off for school in France, and maybe she only granted this miracle as a parting gift for her long-time friend. So tonight—tonight is Brunos last chance. And tonight, as it happens, Broken Social Scene, her favorite band, is throwing a big outdoor bash. Maybe if Bruno, with the help of his best pal Blake, can score tickets and give Caroline a night to remember, he can keep this miracle alive. 

==Cast==
*Georgina Reilly as Caroline
*Greg Calderone as Bruno
*Kerr Hewitt as Blake
*Lyndie Greenwood as Blakes Girlfriend
*Tracy Wright as Box Office Woman
*Stephen McHattie as Security Guard
*Mayko Nguyen as Publicist
*Bobby Sahni as Concert MC

==Reception==

Liam Lacey of The Globe and Mail remarked of the film, "The drama floats by like a bubble of yearning and delight, and then ends with a slightly silly pop. Contrary to our expectations, this isnt really a version of Before Sunset set in the T.Dot. Its a Broken Social Scene movie, with a sweet tease of a story that pretends for a moment that Toronto and Paris could share an affair." 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 