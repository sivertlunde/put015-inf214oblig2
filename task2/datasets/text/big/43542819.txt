Only the Young Die Good
{{Infobox film
| name           = Only The Young Die Good
| image          = Only The Young Die Good (theatrical release poster).jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Dominic Deacon
| producer       = Anna Young
| writer         = Dominic Deacon
| starring       = Jasper Jewel Rod Lara Mark Casamento Emily Paddon-Brown Becky Lou Rowan Francis Strawberry Siren Haydn Evans Ashleigh Southam Kelly Ann Doll Evan "Moxie" Kitchener
| cinematography = Matthew Scott
| editing        = Dominic Deacon	
| studio         = Southside Productions
| released       =  
| runtime        = 90 minutes
| country        = Australia
| language       = English
}}
 noir thriller Bad Habits and Burlesque (2010 Australian film)|Burlesque. This film was shot in Melbourne, Victoria. 

==Plot==
Set in Melbourne, 1949: private detective, Jack Mitchum, is a decent man trying to make a living in a dirty town. Jack witnesses a brutal murder and he has only 2 days to try and prove his innocence before he takes the rap for a crime he didnt commit. He has to play their own rough and dirty game to stay alive as he discovers that in this town, Only The Young Die Good. 

==Cast==

Jasper Jewel as Gloria Monroe.

Rod Lara as Raul Estana.

Mark Casamento as Jack Mitchum.

Emily Paddon-Brown as Valerie Richmond.

Becky Lou as Lucy Raymond.

Rowan Francis	as Dennis Richmond.

Strawberry Siren as Stephanie Wells.

Haydn Evans as Walter Sopwith.

Ashleigh Southam as Lucas.

Kelly Ann Doll as Ruby.

==Soundtrack==
 Evan "Moxie" Bad Habits and Burlesque (2010 Australian film)|Burlesque. The score has been described as sinister, sensual, foreboding, dark, and passionate. An official soundtrack release date has been set for late 2014. 

Additional music.
#"The Other One" (Written and Performed by Jasper Jewel and Ashleigh Southam)
#"Sex And Cigarettes" (Written and Performed by Ilana Charnelle)

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 