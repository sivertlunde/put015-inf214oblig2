Tuneega Tuneega
{{Infobox film
| name = Tuneega Tuneega 
| image = Tuneega Tuneega film poster.jpg
| alt =
| caption =
| director = M. S. Raju
| producer =  Maganti Ramchandran Dil Raju  (presenter)  
| writer = Paruchuri Brothers   (dialogues) 
| starring = Sumanth Ashwin Rhea Chakraborty Mrinal Dutt Vasundhara Kashyap
| music = Karthik Raja
| cinematography = S. Gopal Reddy 
| editing = K. V. Krishna Reddy
| studio = Padmini Arts Sri Venkateswara Creations  (presents) 
| distributor = Sri Venkateswara Creations 14 Reels Entertainment  (Overseas)  
| released =     
| runtime =
| country = India
| language = Telugu
| budget = 
| gross = 
}}

Tuneega Tuneega (Telugu:తూనీగ తూనీగ) is a Telugu film written and directed by M. S. Raju. It stars Sumanth Ashwin and Rhea Chakraborty and was produced by Maganti Ramji  under Padmini Arts banner.  The film is being presented and distributed by Dil Raju under Sri Venkateswara Creations Banner. It was released worldwide on July 20, 2012. 

==Story==
Rama Swamy (Prabhu) and Ravindra Babu (Nagababu) are childhood friends who stay together. Their kids Karthik (Sumanth) and Nidhi (Rhea) are of the same age but they hate each other and torment each other by playing pranks. After a while, Nidhi is sent abroad for her further studies.

After a gap of 12 years, Nidhi comes back and meets Karthik at a family function. It’s love at first sight for Karthik, but he hides his identity. Slowly Nidhi comes to know the truth, and in the process, falls for Karthik’s charms. Just when everything seems rosy, Nidhi’s family wants her to marry someone else. The rest of the story is about how Karthik solves all the issues and wins his love. 

==Cast==
* Sumanth Ashwin as Karthik
* Rhea Chakraborty as Nidhi Prabhu as Ramasamy Naga Babu as Nagendra Babu
* Manisha Yadav as Maithri
* Vasundhara Kashyap as Neethu
* Abinaya as Kavya
* Sayaji Shinde as James Paruchuri Venkateswara Rao
* Vijayachander Seetha
* Vinod Kumar Geetha
* Jyothi
* MS Narayana AVS

==Production== Teja was announced.  It was announced that the movie would be made under M. S. Rajus banner Sumanth Art Productions. It was launched on 14 December 2007.  Due to undisclosed reasons, it never went onto sets and the film was shelved. In April 2010, it was reported that V. N. Aditya was directing Sumanths debut film under their home banner.  In May 2010, it was announced that the film was titled Mirchi with Maganti Ramji producing and Aditya directing.  

In January 2011, it was announced that V. N. Aditya was no longer part of the project and M. S. Raju would direct.  In March 2012, it was announced that the filming had been completed and the film was titled Tunnega Tuneega.  It was also announced that Dil Rajus Sri Venkateswara Creations will present and distribute the film. In April 2012, the first look was released, and it was revealed that former MTV VJ Rhea Chakraborty is paired alongside Sumanth.  It was censored on July 16 and received a U certificate. 

==Critical reception==
This film got negative reviews. Greatandhra.com wrote Zero Buzz with rating 2.25/5 and better to watch real Tuneega in park than going to the theater as a bottom line.

==Soundtrack==
{{Infobox album
| Name = Tuneega Tuneega
| Longtype =
| Type = Soundtrack
| Artist = Karthik Raja
| Cover =
| Released = January 10, 2012
| Recorded = 2012 Feature film soundtrack
| Length = 26:28 Telugu
| Label = Aditya Music
| Producer = Karthik Raja
}}

The soundtrack was composed by   and three by Sirivennela Sitaramasastri. The soundtrack album was released on June 10, 2012 at Lalitha Kala Thoranam in Hyderabad, India|Hyderabad.  It was released through Aditya Music label.

{{Tracklist
| collapsed       =
| headline        = Tracklist
| extra_column    = Artist(s)
| total_length    = 26:28
| writing_credits = 
| lyrics_credits  = yes
| music_credits   = 
| title1          = Mike Testing
| note1           =  Krishna Chaitanya Ranjith
| length1         = 03:50
| title2          = Tuneega Tuneega
| note2           =  
| lyrics2         = Krishna Chaitanya
| extra2          = MK Balaji
| length2         = 03:05
| title3          = Dhigu Dhigu Jabilee
| note3           = 
| lyrics3         = Sirivennela Sitaramasastri
| extra3          = Karthik (singer)|Karthik, Rita
| length3         = 04:01
| title4          = Hatsoff Oyi Brahma
| note4           = 
| lyrics4         = Sirivennela Sitaramasastri Tippu
| length4         = 03:39
| title5          = Pedavanchullo Prema
| note5           = 
| lyrics5         = Krishna Chaitanya
| extra5          = Rahul Nambiar
| length5         = 01:42
| title6          = Ahista Ahista
| note6           = 
| lyrics6         = Sirivennela Sitaramasastri
| extra6          = Karthik, Rita
| length6         = 05:11
| title7          = Dhoodi Pinja Lanti Pilla
| note7           = 
| lyrics7         = Krishna Chaitanya
| extra7          = Rahul Nambiar
| length7         = 04:01
| title8          = Merise Ninge
| note8           = 
| lyrics8         = Bhuvanachandra
| extra8          = 
| length8         = 00:59
}}

==Awards==
===CineMAA Awards===
* Best Male Debut (2013): Sumanth Ashwin 

==References==
 

 
 
 