The Names of Love
{{Infobox film
| name           = The Names of Love
| image          = The Names of Love.jpg
| alt            =  
| caption        = 
| director       = Michel Leclerc
| producer       = Antoine Rein Fabrice Goldstein Caroline Adrian
| writer         = Michel Leclerc Baya Kasmi
| starring       = Sara Forestier Jacques Gamblin Zinedine Soualem Carole Franck
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Music Box Films (US)
| released       =  
| runtime        = 100 minutes
| country        = France
| language       = French
| budget         = 
| gross          = 
}}
The Names of Love ( ) is a 2010 French film, directed by Michel Leclerc, written by  Leclerc and Baya Kasmi, and produced by Antoine Rein, Fabrice Goldstein and  Caroline Adrian. The film recorded 764,821 admissions in Europe.  

==Production background== best female best writing.   

The former French Prime Minister Lionel Jospin makes a cameo appearance.

==Plot==
The film is semi-biographical, documenting the life of a young woman who uses sex as a weapon to influence right-wing individuals and conservative Muslims. Baya Benmahmoud (Sara Forestier), a scatter-brained, free-spirited, young left-wing activist, sleeps with her political opposites in order to manipulate them to her cause, until she finds her match in Arthur Martin (Jacques Gamblin).

==Cast==
* Jacques Gamblin as Arthur Martin
* Sara Forestier as Baya Benmahmoud
* Zinedine Soualem as Mohamed Benhmamoud
* Carole Franck as Cécile Delivet Benmahmoud
* Jacques Boudet as Lucien Martin
* Michèle Moretti as Annette Martin
* Zakariya Gouram as Hassan Hassini
* Lionel Jospin as himself
* Antoine Michel as photographer

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 

 