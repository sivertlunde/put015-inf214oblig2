The Rape of the Sabine Women (film)
The Rape of the Sabine Women is an art film by Eve Sussman, which had its world premiere on 2006-11-26 at the 47th International Thessaloniki Film Festival.   

Eve Sussman, an artist and movie producer, was born in England, to American parents, in 1961. She was educated at Robert College of Istanbul, University of Canterbury and Bennington College. Besides the United States, and the Whitney Museum of American Art amongst other institutions her work has been exhibited in Turkey, Austria, United Kingdom, Ireland, Germany, Italy, Spain, Croatia, France, Poland and Canada. {{cite web
| url = http://www.union-gallery.com/content.php?page_id=1112
| title = Union
| accessdate = 27 February 2007
}} 
 eponymous classical story in a variety of modern contexts.   

== See also ==
* 89 seconds at Alcázar
* Las Meninas
* Tableau vivant

==References==
 

==External links==
*  An early cut of the film was exhibited at the Nasher Museum of Art at Duke University, July 6, 2006 – Sept. 24, 2006.
* ". Marquette University. Retrieved 7 December 2007

 
 
 


 