The Reenactment
{{Infobox film
| name           = The Reenactment Reconstituirea
| image          =
| caption        =
| director       = Lucian Pintilie
| producer       = Lucian Pintilie
| writer         = Horia Pătraşcu Lucian Pintilie
| starring       = George Constantin George Mihăiţă Vladimir Găitan
| music          =
| cinematography = Sergiu Huzum
| editing        =
| distributor    = Filmstudio Bucureşti
| released       = 1968
| runtime        = 100 minutes
| country        = Romania
| language       = Romanian
| budget         =
}} 1968 black-and-white communist regime, militiaman Dumitrescu alcoholic Paveliu (Emil Botta).
 Romanian cinema, censorship apparatus Romanian Revolution toppled communism.

==Production and plot==
Both Horia Pătraşcus novel and the screenplay (co-authored by Pătraşcu and Pintilie) are closely based on real-life events. The incident was witnessed by Pătraşcu during the early 1960s, and took place in his native town of   site; retrieved May 28, 2008  The militiamen involved had detained two youths with no prior criminal record, accusing them of having been drunk and disorderly, and had decided to make them reenact the scene in order to educate the public about the perils of alcohol. In a 1999 interview, Pătraşcu acknowledges that, as a university student and part-time activist at a local culture house, he was a member of the original crew. 

Like in the film, the militiamens decision seems to have outweighed the punishment proscribed for such offences: they made the youths film the same scene over and over, and, in the process, exposed them to public humiliation.   Pătraşcu, who credits  , September 2008 

There was a significant gap between the storys publishing and the start of production. Filmmaker  , Nr. 332, October 2008  Poet and actor Emil Botta was fifty-seven at the time, while George Constantin was thirty-five and Ernest Maftei forty-eight. Despite the marked age difference, Mihăiţă remembers, the communication on the set was smooth and the atmosphere playful. 
 Southern Carpathian resort of Sinaia, but preserves some elements from the intended setting, including the restaurants name of Pescăruş ("Seagull"), which had been borrowed from its Caransebeş model. 

==Themes==
In addition to its factual content, The Reenactment stands as a metaphor for the peoples inability to control their own destinies under the grip of a  , Nr. 753, August 2004 
In 2004, Lucian Pintilie wrote that his decision to shoot the film was also motivated by his disgust in respect to the invasive practices of communist authorities, having previously been informed that one of his friends, a closeted gay actor, was denounced for breaking Article 200|Romanias sodomy law, and, in order to avoid the prison sentence, was forced to have intercourse with his wife while investigators watched. 

Pintilie also stated his objection to the very notion of an inquiry, noting that such a procedure "is the most effective way of veiling reality", and indicated that the film was in part an allusion to the tradition of torture and repeated interrogation, enforced by the Securitate secret police in the previous decade.  George Constantins character was thus supposed to be a Securitate officer, but, Pintilie claims, the institution was scandalized by the possibility of an exploration into its past, and appealed to Nicolae Ceauşescu personally to prevent this from happening; as a consequence, Pintilie turned the protagonist into a prosecutor.  Although the reference to Militia practices was the result of such pressures, it became one of the most valued attributes of the film. The portrayal of militiamen as brutal and irresponsible contrasted with their sympathetic portrayal in films approved of by the Ceauşescu regime, and especially with the post-1970 series Brigada Diverse.  In a 2007 article for Gândul, journalist Cristian Tudor Popescu writes that, by exposing the torment which could be caused even by routine Militia interventions, The Reenactment "had discarded the urban legend of militiamen so stupid that they make one laugh with tears when seeing how stupid they are." 

"Harshness" was identified by Mircea Săucan as a main characteristic of the film.  Stressing that he does not find this trait to be a defect, he states that, had he directed the film, he would have insisted more on the "cold" aspect of the inquiry, to stand in contrast with the  y evil on which Romanian communist authorities were relying."    , Nr. 174, June 2007 (originally published in Gândul, June 5, 2007) 

In 1965, Pintilie had directed the film Duminică la ora şase, which dealt in part with similar themes, but, as the director indicates, only hinted in that direction.  It was showcased and acclaimed at the Pesaro Film Festival in Italy. He recalls: "The films prologue clearly placed the plot in the years of   socialism.   Who are the youths from the films prologue? I was asked directly by the audience   Arent todays youths these youths in the film? No, I shamelessly lied, for there was clearly a possibility for prevarication, no, I said in order to be able to return to Romania and make The Reenactment."  George Mihăiţă recounts not realizing at first the importance of his role: "The best proof of   sublime unawareness was that, when shooting was over, I asked Pintilie—joking more or less—when he was going to give me a more important part to play... He smiled and said just this: Wait and see the movie! " 

==Impact and legacy==

===Censorship===
The Reenactment s release coincided with the peak of  , where "the projectionist was driven out of his mind" because it ran as the main feature for two months on end.  Mihăiţă recalls: "the films presentation was stripped of all ceremony. Better put, the film was introduced through the back door at Luceafărul... It stood there, without any comments, for about a month, before being withdrawn as discreetly as it had appeared."  The Reenactment was also shown in Timişoara, but, Pătraşcu indicates, no program or promotional material given approval for publishing; it was only shown with discretion in several other main cities, "until people had heard about it", then withdrawn.  The writer also remembers being "glad" upon learning that the film managed to raise public awareness, and that it incited viewers to engage in rioting against Militia forces. 

Early in 1969, the authorities took the decision to withdraw the film from cinemas, a ban which lasted until the regime came to an end two decades later.     Marina Kaceanov,  , in  , Nr. 25, March 2008, Section 3    Doinel Tronaru,  , at   in general, and intervened to stop filming on Pragul albastru, which was based on a screenplay by  , May 12, 2008  For instance, it describes how, moments after having seen the film in Bucharest, the avant-garde author and former communist Geo Bogza scribbled in the snow set on the directors car the words: "Long live Pintilie! The humble Geo Bogza." 

In May 1970, the  , where he was celebrated with a retrospective and a special trophy. Pintilie confesses that the ceremony failed to impress him at the time, due to his feelings of dissatisfaction and his determination to continue filming in Romania.    , Nr. 753, August 2004  Such events were also organized in other cities, among them London and Bologna, but Pintilie refused to attend them. 

Another clash between Pintilie and the communist system occurred in 1972, when he satirized officials by staging a subversive version of Gogols The Government Inspector, which was suspended soon after its premiere. Annette Insdorf, "A Romanian Director Tells a Tale of Ethnic Madness", in The New York Times, November 4, 1994  In an interview with The New York Times, he records a meeting he had with the censors: "I was told, If you want to continue working here, you have to change your conception of the world. I answered, But Ive just started formulating it.   All I can do is develop it. " 

During the following period, Pintilie only worked sporadically in Romania and was pressured to seek employment abroad    (notably, in the United States, where he served as artistic director for Minneapolis Guthrie Theater and Washington, D. C.s Arena Stage).  His only other film released at home before 1989, the 1981 De ce trag clopotele, Mitică?, was loosely based on stories by Ion Luca Caragiale. Noted for subtly criticizing the Ceauşescu regime at a time when it had return to a hard-line stance (see July Theses), it was itself censored by the officials.   Film critic Doinel Tronaru argues that both productions are equally accomplished.  Pătraşcu, dissatisfied by the new restrictive guidelines, avoided contributing screenplays, and instead focused on cultural activities with Adrian Păunescus Cenaclul Flacăra, whose shows still maintained a degree of artistic independence. 

===Recovery=== Romanian Revolution, Ministry of Too Late (nominated for Palme dOr at the Cannes Festival, the same year).  Many of them revisit Romanias communist past, and, in his later production După-amiaza unui torţionar, he focuses on a more obvious treatment of the Securitate and its repression tactics. 
 The Contest). sensationalist trend in Romanian television, which sees stations competing for ratings by closely following cases of suicide and murder. 

At the   (originally published in   ( ),   (May 2007)  and Toronto (February 2008). 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 