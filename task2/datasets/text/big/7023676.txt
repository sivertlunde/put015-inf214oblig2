The Pride and the Passion
 
{{Infobox film
| name           = The Pride and the Passion
| image          = The Pride and the Passion - Poster.jpg
| image_size     = 250
| caption        = 1957 theatrical poster
| director       = Stanley Kramer
| producer       = Stanley Kramer
| screenplay     = Edna Anhalt Edward Anhalt The Gun by C.S. Forester
| starring       = Cary Grant Frank Sinatra Sophia Loren
| music          = George Antheil
| cinematography = Franz Planer
| editing        = Frederic Knudtson Ellsworth Hoagland Stanley Kramer Pictures
| distributor    = United Artists
| released       =  
| runtime        = 132 min.
| country        = United States
| language       = English
| budget         = $3.7 million Tino Balio, United Artists: The Company That Changed the Film Industry, Uni of Wisconsin Press, 1987 p 101 
| gross          = $5.5 million (US rentals) 
}}
 Pride and Prejudice.
 1957 war film produced and directed by Stanley Kramer and starring Cary Grant, Frank Sinatra and Sophia Loren. Set in the Napoleonic era, it is the story of a British officer (Grant) who has orders to retrieve a huge cannon from Spain and take it to the British forces by ship. But first the leader of the Spanish guerrillas (Sinatra) wants to transport the cannon 1,000&nbsp;km across Spain to help in the capture of Ávila, Spain|Ávila from the French before he releases the cannon to the British.  Most of the movie deals with the hardships of transporting the cannon across rivers and through mountains while evading the occupying French forces and culminates in the final battle for Ávila.  A sub-plot is the struggle for the affections of Loren by the two officers.
 The Gun by C.S. Forester. Earl Felton did an uncredited re-write. George Antheil composed the score. Saul Bass designed the opening title sequence. The film co-starred Theodore Bikel and Jay Novello. The picture was filmed in Technicolor and VistaVision, and released by United Artists.

==Plot==
During the Peninsular War, Napoleons armies overrun Spain.  An enormous cannon, belonging to a Spanish army, is abandoned when it slows down the armys retreat.   French cavalrymen are dispatched to retrieve it.

Britain, Spains ally, sends Royal Navy captain Anthony Trumbull (Cary Grant|Grant) to find the cannon and see that it is handed over to British forces. However, when Trumbull arrives at the Spanish headquarters, he finds that it has been evacuated and is now occupied by a guerrilla band led by the French-hating Miguel (Frank Sinatra|Sinatra). Miguel agrees to help Trumbull search for the cannon, although the two men come to dislike each other. One cause of their enmity is Miguels mistress Juana (Sophia Loren), who falls in love with Trumbull.

Meanwhile, sadistic General Jouvet (Theodore Bikel), the French commander in Ávila, Spain|Avila, orders the execution of Spaniards who do not give information of the cannons whereabouts. The cannon has in fact undergone an arduous journey in the direction of Avila, which Miguel is obsessed with capturing.

The guerrilla band, whose ranks have swelled considerably, almost loses the cannon when General Jouvet deploys artillery near a mountain pass that they need to use to get to Avila. With help from the local populace, they get the cannon through, although it rolls down a hillside and is badly damaged.

The cannon is hidden in a cathedral while it is repaired, once having to be disguised as an ornamental piece during a religious celebration. However, French officers are informed about the cannons presence, but the cannon has been moved by the time the officers arrived and they scorn the informant.

When the cannon finally arrives at the guerrillas camp near Avila, Trumbull and Miguel prepare to attack the city. However, Avila is defended by strong walls and eighty cannons, Trumbull going so far to estimate that half of the guerrillas will be killed during the assault. He tries to convince Juana not to participate in the attack, but, the next day, she goes with the men.

The cannon is used to breach the walls, and, despite suffering heavy losses (including Juana and Miguel), the guerrillas get inside the city. Jouvet is killed and the remaining French troops are overrun in the town square. After the battle, Trumbull places Miguels body in front of the statue of Avilas patron saint.

==Cast==
 
* Cary Grant - Capt. Anthony Trumbull
* Frank Sinatra - Miguel
* Sophia Loren - Juana
* Theodore Bikel - General Jouvet
* John Wengraf - Sermaine
* Jay Novello - Ballinger
* José Nieto (actor)|José Nieto - Carlos
* Philip Van Zandt - Colonel Vidal
 

==Troubled production==
  The Sun Also Rises in various locales around Europe, including Spain. When there was to be no reconciliation, Sinatra hurriedly left the production, asking director Stanley Kramer to condense all of his scenes into an as brief as possible shooting schedule; Kramer obliged. Conversely, Cary Grant was happy to get away from his failing marriage to Betsy Drake.

Despite the films problems, Kramer was nominated by the Directors Guild of America for Outstanding Directorial Achievement.
 Mike Walker, Steven Weber as Earl Felton, Greg Itzin as Cary Grant, Kate Steele as Sophia Loren, Jonathan Silverman as Frank Sinatra and Jonathan Getz as Stanley Kramer.

The cannon appears to have been based on the Jaivana Cannon, a real prototype from Jaipur, India, one of the largest cannons ever built.

==Box office and critical reception==
Opening to mixed reviews on July 10, 1957, The Pride and The Passion would prove to be successful at the box office, spurred no doubt by the popularity of the leading actors. With box office rentals of $4.7 million from a gross of $8.75 million, this would be one of the 20 highest grossing films of 1957. Variety (magazine)|Variety praised the films production values, stating "Top credit must go to the production. The panoramic, longrange views of the marching and terribly burdened army, the painful fight to keep the gun mobile through ravine and over waterway - these are major pluses." However Ephraim Katz in The Film Encyclopedia describes it as "overblown empty epic nonsense". 

However the high production cost meant the film lost $2.5 million. 

The films musical score was the last important work by George Antheil, once famous as the "bad boy of music" in the 1920s. It is the only one of Antheils many film scores to have been preserved on a commercial soundtrack recording.

==References==
 

==External links==
*  
*  
* Varietys Review: http://www.variety.com/review/VE1117794130.html?categoryid=31&cs=1&p=0
* BBC Radio 4s The Afternoon Play - The Gun Goes to Hollywood: http://www.bbc.co.uk/programmes/b00zdhzs

 

 
 
 
 
 
 
 
 
 
 
 
 
 