Les Truands
{{Infobox film
| name           = Les Truands  
| image          = 
| image size     =
| alt            =
| caption        = 
| director       = Carlo Rim
| producer       = Robert Sussfeld	
| writer         =  Carlo Rim
| narrator       =
| starring       = Eddie Constantine  Noël-Noël Jean Richard
| music          = Georges Van Parys
| cinematography = Maurice Barry
| editing        = Monique Kirsanoff
| studio         = Franco London Films  
| distributor    = Gaumont  
| released       = 27 April 1956
| runtime        = 106
| country        = France
| language       = French
| budget         =
| gross          =
}}

Les Truands  is a French comedy film starring Eddie Constantine directed by Carlo Rim. For English-speaking audiences it was renamed as Lock Up Your Spoons respectively The Gangsters. 

== Synopsis ==
This is the story of racketeer Amédée who recalls the story of his life when he is already more than 100 years old. He tells his family about his rise and his rivals. This is all to explain why he couldnt help but nick the watch of the mayor who came to congratulate him.

== Cast ==
*  
*  
* Jean Richard : Alexandre Benoit, Amédées son
* Yves Robert : Amédée Benoit (and his father) Sylvie : Clarisse Benoit
* Lucien Baroux : the priest
* Mireille Granelli : Clarisse at the age of 18 years
* Denise Provence : La Païva
* Line Noro : Chiffon
*  
* Héléna Manson : Nana
* Robert Dalban : Pépito Benoit
* Gaston Modot : Justin Benoit
* Cora Vaucaire : a singer
* Claude Borelli : a wife of Jim
* Antonin Berval : Benoits third-born son
* Martine Alexis : a wife of Jim
* Irène Tunc : a wife of Jim
* Ariane Lancell : a saloon girl
* Béatrice Arnac : Mme Léonce
* Guy Tréjean : Bobby, the lover of Cahuzacs daughter
* Albert Rémy : policeman
* Nadine Tallier : a wife of Jim
* Claude Godard : a wife of Jim
* Françoise Delbart : the twins mother
* André Bervil : Ange
* Daniel Sorano : the barkeeper
* Robert Vattier : the Duke of Morny
* André Dalibert : a police officer
* Michel Nastorg : town mayor
* Léon Larive : photographer
* Pascal Alexandre : Jim as child
* Christian Denhez : Amédée as child
* Jean dYd : the grandfather
* Nelly Vignon : Madeleine Cahuzac
* Jacques Mancier : police inspector
* Henri Cogan : a cowboy

== References ==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 


 
 