Lovelife
 
{{Infobox film
| name           = Lovelife
| image          = Lovelife.jpg
| image_size     =
| caption        =
| director       = Jon Harmon Feldman
| producer       = Fuller French Todd Hoffman
| writer         = Jon Harmon Feldman
| narrator       =
| starring       = Saffron Burrows Sherilyn Fenn Carla Gugino Matt Letscher Jon Tenney Bruce Davison
| music          = Adam Fields
| cinematography = Tony C. Jannelli
| editing        = Samuel Craven
| distributor    = Trimark Pictures
| released       = April 1997
| runtime        = 96 minutes
| country        = United States
| language       = English
| budget         =
| preceded_by    =
| followed_by    =
}}
Lovelife is a 1997 romantic comedy film written and directed by Jon Harmon Feldman. The ensemble cast includes Matt Letscher, Sherilyn Fenn, Saffron Burrows, Carla Gugino, Bruce Davison, Jon Tenney and Peter Krause.

Lovelife was nominated for a Feature Film Award at the 1997 Austin Film Festival, and won an Audience Award at the Los Angeles Independent Film Festival. The film was winner of the screenplay award at the L.A. Indie fest.

==External links==
* 
*  
*  at Variety.com
*  at Qwipster.net
*  at Cybergraffiti

 
 
 
 
 
 
 
 


 