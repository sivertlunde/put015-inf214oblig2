Mana Voori Pandavulu
{{Infobox film
| name           = Manavoori Pandavulu
| image          = Manavoori Pandavulu.jpg
| image_size     = 
| caption        =  Bapu
| producer       = Jaya Krishna
| writer         = Mullapudi Venkata Ramana
| narrator       = 
| starring       = Krishnam Raju Chiranjeevi Rao Gopal Rao
| music          = K. V. Mahadevan
| cinematography = Balu Mahendra
| editing        = 
| studio         = 
| distributor    = 
| released       = 9 November 1978
| runtime        = 
| country        = India
| language       = Telugu
| budget         = 
}} Bapu and Hindi as Hum Paanch starring Mithun Chakraborty.    

== Synopsis ==
The film revolves around Krishna (played by Krishnam Raju) who brings together an unlikely group of 5 youngsters to help in creating awareness among their villagers to revolt against the cruel village head, Rambhoopal (Rao Gopal Rao) and his henchmen. Even though being a revolutionary theme, the movie was wonderfully made without the usual noise that accompanies the leftist movies and fashioned on the lines of the great epic Mahabharata. Amazing performances by all the actors (Krishnam Raju, Raogopal Rao, Allu Ramalingaiah etc.) and wonderful technical backup make this movie another classic from Bapu (film director)|Bapu. The film also has Chiranjeevi as a very young actor (his second movie to be released) in the role of Parthu.

== Cast ==
*Krishnam Raju .... Krishna
*Rao Gopal Rao  ....  Rambhoopal (Dora vaaru)
*Murali Mohan  ....  Ramudu
*Prasad Babu  ....  Bheemanna
*Chiranjeevi ....  Parthu
*Allu Ramalingaiah  ....  Kannappa
*Shoba  ....  Sundari Geetha  ....  Sinni
*Kanta Rao  ....  Dharmaiah
*Sarathi ....  Rambhoopals son
*Jhansi  .... Santhamma Halam  ....  Oorvasi
*Bhanuchander
*Jayamalini
*Vijaya Bhaskar

== Crew == Bapu
*Screenplay &ndash; Mullapudi Venkata Ramana
*Dialogue &ndash; Mullapudi Venkata Ramana
*Producer &ndash; Jaya Krishna, Krishnam Raju
*Cinematography &ndash; Balu Mahendra
*Music &ndash; K. V. Mahadevan Kosaraju
*Playback singers &ndash; S. P. Balasubrahmanyam, S.P. Sailaja, P. Susheela, G. Anand, M. S. Rama Rao

== Soundtrack ==
{{Track listing
| collapsed       =
| headline        =
| extra_column    = Singer(s)
| total_length    =

| all_writing     =
| all_lyrics      =
| all_music       = K. V. Mahadevan

| writing_credits =
| lyrics_credits  = yes
| music_credits   =

| title1          = Jendaa Pai Kapiraju
| note1           =
| writer1         =
| lyrics1         = Arudra
| music1          =
| extra1          = S. P. Balasubrahmanyam|S. P. Balu, G. Anand, M. S. Ramarao
| length1         =

| title2          = Manchiki Cheddaki
| note2           =
| writer2         =
| lyrics2         = Arudra
| music2          =
| extra2          = S. P. Balu
| length2         =

| title3          = Nalla Nallani
| note3           =
| writer3         = Kosaraju
| music3          =
| extra3          = G. Anand, S. P. Sailaja
| length3         =

| title4          = Orey Pichchi Sannaasee
| note4           =
| writer4         =
| lyrics4         = Arudra
| music4          =
| extra4          = S. P. Balu
| length4         =

| title5          = Pandavulu
| note5           =
| writer5         =
| lyrics5         = Arudra
| music5          =
| extra5          = S. P. Balu, G. Anand, M. S. Ramarao
| length5         =

| title6          = Piriki Mandhu Thaagi
| note6           =
| writer6         =
| lyrics6         = Arudra
| music6          =
| extra6          = S. P. Balu
| length6         =

| title7          = Sitralu Seyakuro
| note7           =
| writer7         =
| lyrics7         = Arudra
| music7          =
| extra7          = S. P. Balu
| length7         =

| title8          = Swagatham Suswagatham
| note8           =
| writer8         =
| lyrics8         = Arudra
| music8          =
| extra8          = Rao Gopal Rao, P. Susheela
| length8         =
}}

==Box-office==
Katakataala Rudraiah and Mana Voori Pandavulu were released within a gap of 10 days and both the films became blockbusters. 

==Awards==
*Filmfare Best Film Award (Telugu) - Krishnam Raju & Jaya Krishna
*Nandi Award for Best Cinematographer - Balu Mahendra

==References==
 

 

 
 
 
 
 
 
 