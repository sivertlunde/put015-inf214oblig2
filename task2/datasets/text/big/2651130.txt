Evita (1996 film)
{{Infobox film
| name = Evita
| image = Evita poster.jpg
| image_size = 220px
| border = yes
| alt = 
| caption = United States Theatrical release poster
| director = Alan Parker
| producer = {{plain list|
*Alan Parker
*Robert Stigwood
*Andrew G. Vajna
}}
| screenplay = {{plain list|
*Alan Parker
*Oliver Stone
}}
| based on =  
| narrator = Antonio Banderas
| starring = {{plain list| Madonna
*Antonio Banderas
*Jonathan Pryce
*Jimmy Nail
}}
| music = Andrew Lloyd Webber
| cinematography = Darius Khondji
| editing = Gerry Hambling
| studio = Hollywood Pictures Cinergi Pictures
| distributor = Buena Vista Pictures
| released =  
| runtime = 134 minutes  
| country = United States
| language = English
| budget = $55 million 
| gross = $141,047,179 
}} musical drama musical of the same name about Eva Perón. Directed by Alan Parker and written by Parker and Oliver Stone, the film starred Madonna (entertainer)|Madonna, Antonio Banderas, and Jonathan Pryce. The film was released on December 25, 1996 by Hollywood Pictures and Cinergi Pictures. The film received a mixed critical reception. However, the movie had a powerful run at the box office, grossing $141,047,179 against a budget of $55 million.

==Plot== Eva Duarte First Lady and Spiritual Leader of the Nation of Argentina, with Ché assuming many different guises throughout Evas story.   

At the age of 15, Eva lives in the provincial town of Junín, and longs to seek a better life in Buenos Aires. She persuades a tango singer, Agustín Magaldi, with whom she is having an affair, to take her to the city. After Magaldi leaves her, she goes through several relationships with increasingly influential men, becoming a model, actress and radio personality. She meets with the older and handsome Colonel Juan Perón at a fundraiser following the recent 1944 San Juan earthquake. Peróns connection with Eva adds to his populist image, since she is from the working class (as he is). Eva has a radio show during Peróns rise and uses all her skills to promote Perón, even when the controlling administration has him jailed in an attempt to stunt his political momentum. The groundswell of support Eva generates forces the government to release Perón, and he finds the people enamored of him and Eva. Perón wins election to the presidency and marries Eva, who promises the new government will serve the descamisados.

At the start of the Perón government, Eva dresses glamorously, enjoying the privileges of being the first lady. Soon after, Eva embarks on what was called her "Rainbow Tour" to Europe. While there she had mixed receptions; the people of Spain adore her; the people of Italy call her a whore and throw things (such as eggs) at her, while the Pope gives her a small, meager gift; and the French, while kind to her, were upset that she was forced to leave early. There are hints of the illness that eventually caused her death. Upon returning to Argentina, Eva establishes a foundation and distributes aid; the film suggests the Perónists otherwise plunder the public treasury. The military officer corps and social elites despise Evas common roots and affinity for the poor.

Eva is hospitalized and they learn that she has cancer. She declines the position of Vice President because she is too weak, and makes one final broadcast to the people of Argentina. She understands that her life was short because she shone like the "brightest fire," and helps Perón prepare to go on without her. A large crowd surrounds the Casa Rosada in a candlelight vigil praying for her recovery when the light of her room goes out, signifying her death. Evas funeral is shown again. Ché is seen at her coffin, marveling at the influence of her brief life. He walks up to her glass coffin, kisses it, and walks into the crowd of passing mourners.

==Cast== Madonna as Evita Perón
* Antonio Banderas as Ché
* Jonathan Pryce as Juan Perón
* Jimmy Nail as Agustín Magaldi Dona Juana Ibarguren Juancito Duarte Bianca Duarte Elisa Duarte Erminda Duarte
* Peter Polycarpou as Domingo Mercante
* Gary Brooker as Juan Atilio Bramuglia
* Andrea Corr as Juan Peróns mistress

==Production==

===Casting===
Discussion of the film began soon after the original production was staged in London in 1978. Several actresses were considered for the role of Eva Perón.

Director Ken Russell has said that his own first choice for the film lead was Karla DeVito, who had come to fame in rock tours and on Broadway, where she had impressed the wife of Andrew Lloyd Webber. DeVito had a screen test for the role while in England shooting music videos for her solo album Is This A Cool World or What?  DeVitos performance of "Dont Cry for Me, Argentina" in the screen test caused much positive buzz. Russell wrote that she brought viewers to tears—except Tim Rice, who wanted Elaine Paige, with whom he was romantically involved. Although Russell rejected the idea, Paige was screen tested twice.

Russells biography indicates that he met with Barbra Streisand, who dismissed the idea of the role immediately. He wrote that he then suggested Liza Minnelli. A year had passed between the first screen tests and Minnellis, which Russell reports was "amazing." Russell approached Stigwood with Minnellis test, convinced she had the necessary talent and star quality, but he was soon after told the role was going to Paige. Having already protested that idea, Russell quit the film. (Years later when he saw DeVito again, Russell addressed her as "My Evita.")

Rumors through the years include Lloyd Webber considering  , the final directorial choice. Years later, Pfeiffer reportedly recorded a number of demonstration musical tracks, and its been said she almost was cast for the role by Parker, but he wanted to shoot the picture on location, not in Pfeiffers preferred Hollywood studio.

  campaigned to play Eva Perón. Madonna eventually wrote a letter to Alan Parker, explaining how she would be perfect for the part. Parker took note of Madonnas genuine fervor for the role:

 

After Madonna was cast as Eva Perón, LuPone was then approached to play the role of Evas mother, but she declined. 

Upon securing the role, Madonna underwent intense vocal training and studied the history of Argentina, as well as Eva Perón.

English singer/actress Billie Piper and Irish singer Andrea Corr had minor parts in the film at what was the start of both of their careers.

===Filming===
Principal photography began in February 1996 and was finished in May.  Madonna was paid a salary of $1 million for her role in the project. She personally lobbied then-Argentine president Carlos Menem for permission to film at the Casa Rosada, the executive mansion.   Upon arrival in Argentina, the cast and crew faced protests over fears that the project would tarnish Eva Peróns image.  They filmed in Buenos Aires for five weeks before moving to Budapest for a month. 

Madonna related the difficulties in changing locations: 
 "We went from 100-degree weather in Argentina, the Latin culture, very embracing, warm, passionate, to a country where people are just learning to be expressive without being afraid. Everybody has a sad expression on their face. And its difficult to work in an environment where there is no joy. It was the toughest experience of my life."  , TIME, 30 December 1996, pp. 1-2  
 Vanity Fair. 

Madonna said of the experience, "This is the role I was born to play. I put everything of me into this because it was much more than a role in a movie. It was exhilarating and intimidating at the same time&nbsp;... And I am prouder of Evita than anything else I have done." 

Midway through production, Madonna discovered she was pregnant. Her daughter Lourdes Maria Ciccone Leon was born on October 14, 1996. 

===Music===
 
The music for the film was completed in a London recording studio in the fall of 1995. 

The soundtrack album was released in two versions; a two-disc edition titled Evita: The Motion Picture Music Soundtrack and a single-disc containing highlights from the soundtrack titled Evita: Music from the Motion Picture. The double-disc edition includes the entire soundtrack to the film, except for two instrumental pieces heard during the film: a short instrumental medley of "Id Be Surprisingly Good for You" and "You Must Love Me" used during the marriage scene (which happens in the middle of "A New Argentina"), and a longer  medley of the same two songs in reversed order, segueing into the chorus of "Dont Cry For Me Argentina", used for the end credits.
;CD 1
{{tracklist
| extra_column    = Performer(s)
| writing_credits = no
| all_lyrics = Tim Rice
| all_music = Andrew Lloyd Webber
| title1          = A Cinema in Buenos Aires, 26 July 1952
| extra1          = John Mauceri
| length1         =  1:20
| title2          = Requiem for Evita
| extra2          = John Mauceri
| length2         =  4:16
| title3          = Oh What a Circus
| extra3          = Madonna (entertainer)|Madonna, Antonio Banderas
| length3         =   5:44
| title4          = On This Night of a Thousand Stars
| extra4         = Jimmy Nail
| length4         =   2:24
| title5          = Eva and Magaldi / Eva Beware of the City
| extra5         = Madonna, Jimmy Nail, Antonio Banderas
| length5         =   5:20
| title6          = Buenos Aires
| extra6         = Madonna
| length6         =   4:09
| title7          = Another Suitcase in Another Hall
| extra7         = Madonna
| length7         =   3:33
| title8          = Goodnight and Thank You
| extra8         = Madonna, Antonio Banderas
| length8         =   4:18
| title9          = The Ladys Got Potential
| extra9         = Antonio Banderas
| length9         =   4:24
| title10          = Charity Concert / The Art of the Possible
| extra10         = Jimmy Nail, Jonathan Pryce, Antonio Banderas
| length10         =   2:33
| title11          = Id Be Surprisingly Good for You
| extra11         = Madonna, Jonathan Pryce
| length11         =   4:18
| title12          = Hello and Goodbye
| extra12         = Madonna, Andrea Corr, Jonathan Pryce
| length12         =   1:46
| title13          = Perons Latest Flame
| extra13         = Madonna, Antonio Banderas 
| length13         =   5:17
| title14          = A New Argentina
| extra14         = Madonna, Jonathan Pryce
| length14         =   8:09
}}

;CD 2
{{tracklist
| extra_column    = Performer(s)
| writing_credits = no
| all_lyrics = Tim Rice
| all_music = Andrew Lloyd Webber
| title1          = On the Balcony of the Casa Rosada (Part 1)
| extra1          = Jonathan Pryce
| length1         =  1:28
| title2          = Dont Cry for Me Argentina#Madonna version|Dont Cry for Me Argentina
| extra2          = Madonna
| length2         =  5:31
| title3          = On the Balcony of the Casa Rosada (Part 2)
| extra3         = Madonna
| length3         =  2:00
| title4          = High Flying, Adored
| extra4         = Madonna, Antonio Banderas
| length4         =  3:32
| title5          = Rainbow High
| extra5         = Madonna
| length5         =  2:26
| title6          = Rainbow Tour
| extra6         = Antonio Banderas, Jonathan Pryce, Gary Brooker, Peter Polycarpou
| length6         =  4:50
| title7          = The Actress Hasnt Learned the Lines (Youd Like to Hear)
| extra7         = Madonna, Antonio Banderas
| length7         =  2:31
| title8          = And the Money Kept Rolling In (and Out)
| extra8         = Antonio Banderas
| length8         =  3:53
| title9          = Partido Feminista
| extra9         = Madonna
| length9         =  1:40
| title10          = She Is a Diamond
| extra10         = Jonathan Pryce
| length10         =  1:39
| title11          = Santa Evita
| extra11         = John Mauceri
| length11        =   2:30
| title12          = Waltz for Eva and Che
| extra12         = Madonna, Antonio Banderas
| length12        =   4:31
| title13          = Your Little Bodys Slowly Breaking Down
| extra13         = Madonna, Jonathan Pryce
| length13        =   1:24
| title14          = You Must Love Me
| extra14         = Madonna
| length14        =   2:50
| title15          = Evas Final Broadcast
| extra15         = Madonna
| length15        =   3:05
| title16          = Latin Chant
| extra16         = John Mauceri
| length16        =   2:11
| title17         = Lament
| extra17         = Madonna, Antonio Banderas
| length17        =   5:17
}}

==Awards and nominations==
;Academy Awards    Best Original Song ("You Must Love Me") (Won) Best Art Direction (Nomination) Best Cinematography (Nomination) Best Film Editing (Nomination) Best Sound Andy Nelson, Anna Behlmer and Ken Weston – Nomination)

;MTV Movie Awards Madonna (Nomination)

;Golden Globe Awards
* Best Motion Picture – Musical or Comedy (Won)
* Best Actress – Motion Picture Musical or Comedy (Madonna (entertainer)|Madonna) (Won)
* Best Actor – Motion Picture Musical or Comedy (Antonio Banderas) (Nomination)
* Best Director – Motion Picture (Alan Parker) (Nomination)
* Best Original Song ("You Must Love Me") (Won)

;BAFTA Awards
* Best Cinematography (Nomination)
* Best Costume Design (Nomination)
* Best Editing (Nomination)
* Best Makeup and Hair (Nomination)
* Best Production Design (Nomination)
* Best Adapted Screenplay (Nomination)
* Best Sound (Nomination)
* Best Film Music (Nomination)

;Other awards
* Los Angeles Film Critics Association Award – Best Production Design (Won)
* Satellite Award – Best Film – Musical or Comedy (Won)
* Satellite Award – Best Costume Design (Won)
* Satellite Award – Best Original Song ("You Must Love Me") (Won)
* Broadcast Film Critics Association Award – Best Picture (Nomination)
* Satellite Award – Best Art Direction (Nomination)
* Satellite Award – Best Cinematography (Nomination)

==Release==

===Critical reception===
Evita had received mixed reviews from film critics. It currently holds a 64% rating on Rotten Tomatoes,  On Metacritic, it assigned the film a score of 45/100 (indicating "mixed or average reviews") based on 23 critics.  Roger Ebert gave the film 3-and-a-half stars (out of 4), praising the work of director Parker and the stars Madonna and Banderas.  Critic Zach Conner commented "Its a relief to say that Evita is pretty damn fine, well-cast, and handsomely visualized. Madonna once again confounds our expectations. She plays Evita with a poignant weariness and has more than just a bit of star quality. Love or hate Madonna-Eva, she is a magnet for all eyes."  Time  s Richard Corliss wrote, "But this Evita is not just a long, complex music video; it works and breathes like a real movie, with characters worthy of our affection and deepest suspicions."  On the other hand, Newsweek s David Ansen wrote, "Its gorgeous. Its epic. Its spectacular. But two hours later, it also proves to be emotionally impenetrable." 

Evita was nominated for five Academy Awards and won the award for "Best Original Song" with "You Must Love Me", which Lloyd Webber and Rice re-teamed after a gap of 20 years to write especially for the film. Evita had five Golden Globe nominations and three wins (Best Picture – Comedy or Musical; Best Original Song, "You Must Love Me"; and Best Actress in a Comedy or Musical, Madonna (entertainer)|Madonna). It was one of the National Board of Reviews Top Ten Films of the Year.

The government of Argentina undertook its own film project, treating Peróns biography as a drama;   was released in October 1996 in Argentina and in the US in December 1996, around the same time as the American film.

===Box office=== wide opening The Relic. The film made $50,047,179 in the United States and an additional $91 million overseas, for a total of $141,047,179 worldwide.   

===Home media===
The film has no overall worldwide distributor, but was released on VHS, Laserdisc, and DVD. Some DVD versions contain special features such as a making-of, the "You Must Love Me" music video, etc. Evita was one of the first films ever to be released on the DVD format. A Blu-ray Disc|Blu-ray 15th Anniversary Edition was released on May 22, 2012 in the United States. 

==World record==
The film earned Madonna a Guinness World Record title, "Most costume changes in a film".  In Evita, Madonna changes costumes 85 times (which included 39 hats, 45 pairs of shoes, and 56 pairs of earrings). The record was previously held by Elizabeth Taylor for the 1963 film Cleopatra (1963 film)|Cleopatra (65 costume changes).

==See also==
* Evita (musical)|Evita – the original 1978 musical production by Andrew Lloyd Webber and Tim Rice
* Eva Perón
* Juan Perón

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 