Achuvettante Veedu
{{Infobox film
| name = Achuvettante Veedu
| image =  Achuvettante_Veedu.jpg
| caption = 
| alt = 
| director = Balachandra Menon
| producer = A. V. Govindankutty
| writer =  Balachandra Menon
| starring = Nedumudi Venu,  Balachandra Menon,  Rohini Hattangadi
| music = Vidyadharan
| cinematography =  
| editing = Balachandra Menon
| studio =  
| distributor =  Safe Release
| released =   
| runtime =   
| country =  India Malayalam
| budget =  
| gross = 
}}

Achuvettante Veedu (  in 1987. The movie is directed by Balachandra Menon starring Nedumudi Venu, Balachandra Menon and Rohini Hattangadi in leading roles. Lyrics of the movie is written by S. Ramesan Nair and back ground music by Mohan Sithara.

==Cast==
*Nedumudi Venu as Achuthankutty Nair
*Rohini Hattangadi as Rugmini Kunjamma
*Balachandra Menon as Vipin
*Rohini as Aswathy Nair
*Sukumaran as Prabhakaran
*Jagannatha Varma as Varma
*Meenakumari as Sarada
*Thilakan as Damodaran Nair
*Aranmula Ponnamma as Achuthankuttys Mother
*T. P. Madhavan as Achuthankuttys Brother
*Sukumaran as Achuthankuttys Brother
*Sankaradi as Jacob
*Kaviyoor Ponnamma as Vipins Mother
*Adoor Bhawani as Mary

==Plot==
The story is about Achuthankutty Nair (Nedumudi Venu) and his family. Achuthan relocates to Trivandram to a rented apartment. His dream is to build his own house and struggles for it. In his new neighborhood there is a mens hostel which creates a nuisance to his life. Vipin (Balachandra Menon) stays in the mens hostel creates trouble for Achuthankutty and his family.

==Soundtrack==
{| class="wikitable"
|-
! Track
! Singer
! Lyrics
! Music
|-
| Chandanam Manakkunna
| K. J. Yesudas
| S. Ramesan Nair
| Vidyadharan
|-
| Chandanam Manakkunna
| K. S. Chitra
| S. Ramesan Nair
| Vidyadharan
|}

==References==
 

==External links==
* 

 

 
 
 
 


 