Tui Jodi Aamar Hoiti Re
{{Infobox film
| name   = Tui Jodi Aamar Hoiti Re
| image    =Tui Jodi Amar Hoitire.jpg
| caption     = 
| director      = Uttam Akash
| producer    = 
| writer         = Uttam Akash Ferdous Moushumi Pobir Mitro Rina Khan Kabila
| music          = Imon Saha
| cinematography = Akash
| editing        = Mizno
| distributor    =Anupom
| released       = 2007
| runtime        = 
| country        = Bangladesh Bengali
| budget         = 
| gross          = 
}}
Tui Jodi Aamar Hoiti Re (  meaning of  i Bengali language romantic family drama film. It released in 2007 all over Bangladesh. It was directed and written by Uttam Akash. Tui Jodi Aamar Hoiti Re stars Shakib Khan, Ferdous Ahmed|Ferdous, Moushumi and many more. It marks the first and so far, the only, time in Dhallywood film history with Moushumi and Shakib Khan as a pair on screen together. It was considered a moderate success at the box office.

==Plot==
Shakib Khan is a young landlord in an area. One day he saw Moushumi and fell love with her. But Moushumi loves a simple village man named Ferdous. Shakib Khan really desires Moshuimi. Moushumi tried to understand that he is junior to her and she loves another. But Shakib Khan is determined to get her.

For this Shakib Khan picks up Ferdous and destroys his eyes. He then takes Mouishumi to him palace. Moushumi acted love with the landlord Shakib Khan. After time, he understood that it is impossible to own one’s heart by force. The story ends when Shakib Khan gives his eyes to Ferdous and shoots himself. After getting his sight back, Ferdous asks who gave him the eyes and want to see him. The doctor brings him to the dead body of Shakib Khan. 

==Cast==
* Shakib Khan  Ferdous  
* Moushumi 
* Pobir Mitro 
* Rina Khan 
* Kabila 
* Siba Shanu

==Crew==
* Director: Uttam Akash
* Story: Uttam Akash
* Script: Uttam Akash
* Music: Imon Saha
* Lyrics: Kabir Bokul
* Cinematography: Akash
* Editing: Mizno
* Distributor: Anupom

==Technical details==
* Format: 35 MM (Color)
* Real: 13 Pans Bengali
* Country of Origin: Bangladesh
* Date of Theatrical Release: 2007
* Technical Support: Bangladesh Film Development Corporation (BFDC)

==Music==

===Soundtrack===
{| border="3" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singers !! Performers !! Notes
|- 1
|"Tui Jodi Aamar Hoiti Re"  Monir Khan Shakib Khan and Mousumi Title song
|- 2
|"Bhalobashi bole tumi"  Monir Khan, Konok Chapa and Andrew Kishore Shakib Khan, Ferdous and  Mousumi
|
|- 3
|"Bondhu amar premer dacter" MoMotaz & Raza Ferdous Ahmed|Ferdous and  Mousumi
|
|- 4
|"Amar preyotoma"  Monir Khan and Shakila Zafar
| Shakib Khan and Mousumi
|
|- 5
|"Chadmukher hashi" Polas and Shoshi Ferdous Ahmed|Ferdous and Mousumi
|
|-
|}

==References==
 

==External links==

 
 
 
 
 