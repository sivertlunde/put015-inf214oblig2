Black Journal
{{Infobox film
 | name = Black Journal
 | image =   Black Journal.jpg
 | caption =
 | director = Mauro Bolognini
 | writer =  
 | starring = Shelley Winters
 | music =   Enzo Jannacci
 | cinematography = Armando Nannuzzi
 | editing =Nino Baragli
 | producer =
 | distributor =
 | released = 1977
 | runtime =
 | awards =
 | country =
 | language = Italian
 | budget =
 }}
Black Journal (originally titled Gran Bollito) is a 1977 Italian drama film directed by Mauro Bolognini.   It is based on the real life events of Leonarda Cianciulli, the Italian serial killer best known as the "Soap-Maker of Correggio".  

== Cast ==
* 
*Mario Scaccia: Rosario, husband of Lea
*Max von Sydow: Lisa Carpi / Police Chief
* 
*Alberto Lionello: Berta Maner / Banker
*Laura Antonelli: Sandra
*Rita Tushingham: Maria 
*Adriana Asti: Palma 
*Milena Vukotic: Tina  
*Franco Branciaroli: Don Onorio, the priest
*Antonio Marsina: Michele, son of Lea
*Maria Monti

==References==
 

==External links==
* 
 

 
 
 
 
 
 


 
 