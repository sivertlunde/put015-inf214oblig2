Monster Dog
{{Multiple issues|
 
 
}}

{{Infobox Film
| name           = Monster Dog
| image          = Monsterdog.jpg
| caption        = Video box cover
| director       = Claudio Fragasso
| producer       = Carlos Aured
| writer         = Claudio Fragasso
| starring       = Alice Cooper Victoria Vera Carlos Santurio
| music          = 
| cinematography =
| editing        =	 
| distributor    = Continental Motion Pictures Corp.
| released       = 
| runtime        = 84 min.
| country        = Italy English
| box office     =
}} Italian horror film written and directed by Claudio Fragasso and starring Alice Cooper and Victoria Vera. It was filmed in Torrelodones, Spain.

==  Plot summary ==
The story begins with rock star Vince Raven (Cooper) performing in the music video for his new song, "Identity Crisis." Later, Vince, Vinces girlfriend Sandra, and Vinces film crew (Jordan, Marilou, Frank, and Angela) drive in a van to Vinces old childhood home to shoot a music video.

While waiting for the crew, Jos, the care taker of the house, prepares a welcome home party for Vince. He is interrupted when he begins hearing strange noises. After searching around the house, he walks outside to find a pack of wild canines growling outside his door. The canines outnumber him and attack him.

The next morning the crew continue their drive to the house. Along the way, they run into two police officers, Sheriff Morrison and Deputy Dan, who are standing at a barricade. The police warn the crew that there has been another "attack." After the crew leave, the sheriff and his deputy are both killed by the Monster Dog in the woods.

The drive comes to a halt when Vince hits a German Shepherd with the van. The crew cannot stand to watch it suffer in pain, so Vince puts it to rest by killing it with a large rock. While the crew mourns the dogs death, an old man in blood-stained clothing attacks them. He warns them that they "will all die," except for Vince. The old man then runs into the woods, Vince and Sandra chase after him to get him to a hospital. While searching through the woods, the Vince and Sara run into the Monster Dog. They escape the monster and get back to the van.

When the crew finally arrive at the house, Jos is nowhere to be found. Vince is worried what happened to Jos so he takes a shotgun and searches around the house. While the crew waits for Vince to return, they discover the food for the party. After searching the house, Vince gives up and wanders into a room where he discovers a book about werewolves.

Later that night Angela has a nightmare that the bloody old man murders everyone in the house. She runs from him and tries to get to Vince. She finds him reading a book in a rocking chair, his back toward her. She slowly walks behind him until he gets up, revealing that he is the Monster Dog. Angela wakes up screaming and the crew tries to calm her down. She tells them about her dream and how Vince was a "werewolf."

Vince is later found reading in the same rocking chair as in Angelas nightmare. Sandra calms to talk to him, and he tells her the story of his fathers death. He says that his father had lycanthropy (the werewolf curse) and that he was blamed for many deaths. He was stabbed with pitchforks, doused with gasoline, and burnt alive. Vince tells Sandra to go back to bed. When returning to her room, Sandra notices a Vinces family portrait. In the portrait there is a werewolf face in a bush next to the family and that Vinces father has demonic red eyes.

The next day, the crew decides to begin filming their next music video for Vinces song, "See Me in The Mirror." Angela is dressed as a bride and Vince sings to his reflection in a mirror. As Angela walks down the stairs, she notices the shadow of a body that is resting against the upstairs window. The light outside flashes and the body crashes through the window. It is revealed to be Joss corpse. Angela leaves the house in shock while the others search the roof to find out what happened. Vince runs after Angela as a mysterious car pulls up to the house. Four armed men step out of the car and talk the crew into letting them into the house. The men hold the crew hostage until Vince comes back, so they can kill him just like they killed his father.

When Vince returns with Angela the house is locked. Vince runs to the car but as the house door is opened Angela is shot by one of the armed men. Vince runs off and the armed men follow.

Vince heads to the roof where he has a shoot off with the some of the armed men. Meanwhile, a pack of wild dogs break into the house and attack the crew and one of the armed men. The armed man tries to kill a dog by burning it but he is set on fire as well. The Monster Dog waits at the open doorway, shrouded in fog, and Jordan tries to fight it but he is killed and dragged away. Sandra and Marilou run upstairs, with the dogs chasing them. The girls run and lock themselves in a room but the Monster Dogs head breaks through the wall, then disappears.

The two girls find Vince, who accused by Marilou to be the Monster Dog. The three escape the house and hide in the armed mens car, but they dont have the keys. Sandra and Vince leave Marilou to get the keys.

Vince and Sandra return to the car, after another run in with a gunman. As they drive off, Marilous corpse falls on Sandra and the Monster Dog attacks Vince from the back seat. Sandra jumps out and hears the car get destroyed with the sound of a gunshot. She finds the old man, who tells her about how he was attacked by Vinces father, which resulted in him becoming a "lycanthrope." The old man dies after telling Sandra that he has bitten Vince and that Vince will now become a werewolf.  Sandra leaves to find Vince, who tells her to kill him before he turns into a werewolf. As Vince begins his transformation into the Monster Dog, Sandra shoots him. She begins to nervously laugh and cry, being the only one left alive.

The movie ends with a reprise of "Identity Crisis."

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 