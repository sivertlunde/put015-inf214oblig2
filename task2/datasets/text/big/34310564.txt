My Cactus Heart
{{Infobox film
| name           = #MyCactusHeart
| image          = My_Cactus_Heart_(High-Res).jpg
| caption        = Theatrical movie poster
| director       = Enrico Santos
| producer       = Charo Santos-Concio   Malou Santos 
| writer         = Enrico Santos
| screenplay     = Enrico Santos Ricardo Fernando III Raz de la Torre Jerry Gracio
| music          = Francis Concio
| starring       = Maja Salvador Matteo Guidicelli Xian Lim
| cinematography = Sherman So Manuel Teehankee
| editing        = Vanessa De Leon
| distributor    = Star Cinema
| studio         = Skylight Films
| released       =  
| runtime        = 100 minutes
| country        = Philippines
| language       =  
| budget         =
| gross          =    
}}

My Cactus Heart (stylized as #MyCactusHeart) is a 2012 Filipino romantic comedy film directed by Enrico Santos, starring Maja Salvador, Matteo Guidicelli, and introducing Xian Lim.   

It is the first independent film produced by Skylight Films which premiered inside the PBB house on January 24. 2012. It was released nationwide on January 25, 2012.  

== Premise ==
Sandy (Maja Salvador) is a hopeless romantic who has dumped many boyfriends and also rejected a number of suitors. Her life changes when she meets Bene (Xian Lim), a handsome heartthrob, at work. However, her friend Carlo (Matteo Guidicelli) will do everything to have her reciprocate his affection.

== Plot ==
My Cactus heart, A thorny love story revolves on Sandy (Maja Salvador), a hopeless romantic girl whose heart and mind is very optimistic believing there is only one great perfect love. After discovering the infidelity of her father, she sworn off love, she thinks men are heels. Now she is known as heart breaker, BASTED-ERA - thats Sandy who always find reasons to reject men who hangs around her. She feels she will end up being hurt, just what like her father did to her mother. She has dumped every man she has been in a relationship with, ignored and denied suitors who would love to ask her out. Now, who might be the lucky guy to enter her thorny heart? Will it be her friend Carlo (Matteo Guidicelli), a waiter of a family business, who is repeatedly rejected by Sandy, but doesnt take NO for an answer, or her handsome new co-worker Benedict (Xian Lim)? 

==Cast==

===Main cast===
*Maja Salvador  as Sandy
*   as Carlo
*Xian Lim  as Benedict

===Supporting cast===
*Ramon Christopher Gutierrez as Facundo
*Rosanna Roces as Margaret
*Ricky Davao as Dan
*Joy Viado as Tita Au
*Pinky Amador as Mother of Carlo
*Bettina Carlos as Anna
*Kristel Moreno as Lucy
*Franzen Fajardo as Tommy
*Beatriz Saw as Arianna
*Lemuel Pelayo as Kale
*Niña Dolino as Helena
*Gee Canlas as Johanna
*Ervic Vijandre as Romeo
*JM De Guzman as Val
*Cecil Paz as Lindsay

==Box Office== Top Grossing Filipino films of 2012. 

It is one of the most successful indie films of 2012 beating other mainstream films of GMA Films, Viva Films, and Regal Films.

==Soundtrack==
Star Records released the soundtrack of the movie, containing 12 tracks.  Sam Milby made a cover of "Kaba", which was originally sung by Tootsie Guevarra   for the films soundtrack album, however, in the official music video, it was sung by Matteo Guidicelli.  

==References==
 

==External links==
* 

 
 
 
 
 
 
 