Urumi (film)
 
 
 

{{Infobox film
| name = Urumi
| image = Urumi2011.jpg
| alt =  
| caption = Teaser poster
| director = Santosh Sivan
| producer = Shaji Nadesan Santosh Sivan Prithviraj Sukumaran
| screenplay     = Shankar Ramakrishnan
| story          = Shankar Ramakrishnan KPAC Lalitha Arya  Alexx ONell
| music = Deepak Dev
| cinematography = Santosh Sivan A Sreekar Prasad
| studio = August Cinema
| distributor = August Cinema V Creations  (Tamil Nadu) 
| released =  
| runtime = 160 minutes
| country = India
| language = Malayalam
| budget =    c
}}
  Arya and Vidya Balan in extended cameos. The soundtrack features songs by Kaithapram Damodaran Namboothiri and Rafeeq Ahammed, as well as composer Deepak Dev.

The film is set in the early 16th century, when the Portuguese sailors dominated the Indian ocean. The story follows Kelu (Prithviraj) - determined to avenge death his father at the hands of the Portuguese - and his cohorts such as Vavvali of Nagapattinam (Prabhu Deva), princess Ayesha of Arackel (Genelia DSouza) and princess Bala of Chirakkal (Nithya Menon). The plot intervenes with the royal intrigues of the Chirakkal Royal House - where Kelu serves as the commander - in - chief - and the assassination of puppet ruler Bhanu Vikraman (Ankur Khanna). Historical figures such as Estêvão da Gama (16th century)|Estêvão da Gama, Vasco da Gama and Chenichery Kurup play pivotal roles in the film.
 Pazhassi Raja Telugu with Tamil as Urumi: Padhinaintham Nootrandu Uraivaal.  http://www.thehindu.com/features/cinema/sivan-in-focus/article3480385.ece 
 Best Background Best Sound Recordist (M. R. Rajakrishnan).  

== Plot ==
From the executives of Nirvana Group, Krishna Das (Prithviraj Sukumaran) learns that his ancestral property in Kerala, leased out to a non-governmental organisation (NGO) by his late-grandfather, is rich in minerals and is free to be sold as the lease period has expired. The multinational mining company   offers him a large sum of money as advance. The NGO currently runs a tribal school in the land property, which is situated inside the Kannadi Forest Range. When Krishna Das and his friend Thanseer (Prabhu Deva) reach the plot, they are kidnapped by the local tribal men to a cave deep in the forest. Krishna Das meets their chief Thangachan (Arya (actor)|Arya) in the cave. Thangachan explains to him that he is the descendant of certain Chirakkal Kelu Nayanar.

In early 16th century, the Portuguese sailors under Vasco da Gama (Robin Pratt) captured a Muslim pilgrim ship and took all the passengers as prisoners. The general/Kothuwal of the Chirakkal Kingdom (northern Kerala) sent a Brahmin negotiator and his own son, Kelu, to the captured ship for negotiations on the pilgrims. But, da Gama rejected all peace talks and barbarously cuts off the Brahmins ears and sewed ears of his dog on the same spot. He then ordered the hundreds of passengers be locked in the hold and the ship to be set on fire. The general of Chirakkal, Chirakkal Kothuwal, storms ship to rescue his son Kelu. But, Chirakkal Kothuwal loses his life in this attempt, and only Kelu escapes. Vavvali, a Tamil Muslim boy, takes Kelu with him to his hut, and treats him as his younger brother. Kelu crafts an urumi, specially made with the leftover ornaments of the dead women and children of the pilgrim ship. He takes an oath that he will murder Vasco da Gama.

Then it shows a foreshadowing of Kelu and Vavvali as adults hunting rabbits in a forest somewhere in Chirakkal. They save the princess of Chirakkal Bala (Nithya Menon) from a group of abductors, apparently organized by her cousin Bhanu Vikraman (Ankur Khanna). Under the orders of Bhanu Vikraman, Kelu and Vavvali are arrested by the Chirakkal guards and is presented in trial before Chirakkal Thampuran (Amole Gupte). In the trial, it is revealed it was the duo who saved the princess. The king grants them an audience. 

During the audience, Kelu manages to get the support of the king to capture da Gama who is coming back to India as the Viceroy. They plan to conduct a secret raid and capture da Gama during the hanging of Balia Hasan in Arackal Fort. However they instead capture Estêvão da Gama (Alexx ONell) and bring him as a prisoner to Chirakkal. In the process, Kelu comes across Princess Ayesha (Genelia DSouza), a fiery warrior princess of Arackal Palace. With the help of Aysha, Balia Hasan is also freed from the gallows.

Back in Chirakkal, the king bestows Kelu with the honour of being the new general ("Kothuval"). Vavvali however is somewhat unhappy as the king neglected to acknowledge his role in the capture of Estêvão.

Princess Ayesha is among captives from a raid in Arackal, which was secretly undertaken by the Chirakkal royals in the midst of Kelus abduction plan, and she is presented to the spoiled prince Bhanu Vikraman as a concubine. However, she tries to kill the prince and Kelu saves him. Later, Kelu helps Ayesha to escape from Chirakkal. Kelu tells Vavvali that they must be planning to capture Vasco da Gama rather than spending days in Chirakkal Palace. They set out to all the nearby villages, with Princess Ayesha joining their effort, and succeed in amassing support from the villagers against the Portuguese. A large number of people join with Kelu, and they get ready to strike any time.

Meanwhile, with the help of minister Chenichery Kurup (Jagathy Sreekumar), Bhanu Vikraman conspires against his uncle and join hands with Estêvão da Gama. He assassinates his uncle with a Portuguese pistol. Kelu returns to Chirakkal Palace to discuss with Bhanu Vikraman, now king of Chirakkal, what actions to take against de Gama, but Bhanu hesitantly states that the army will not longer take orders from Kelu. Chirakkal Bala, the princess of Chirakkal, now joins with Kelu and his cohorts.

Meanwhile, da Gama - accompanied by Estêvão da Gama among others - arrives at the Chirakkal Palace. Chenichery Kurup, whom da Gama remembers at first site, welcomes him. During the audience, Bhanu Vikraman is killed by Estêvão da Gama. As a mark of respect for his allegiance to the Portuguese crown, the empire offers Kurup the post of the Governor General of Laccadives. 

The Chirakkal army lead by Angadan Nambi attacks the rebel hideout. Rebels under Kelu, Vavvali, Ayesha and defected Chirakkal general Kaimal resists the army. Soon an Estêvão da Gama-lead Portuguese unit arrives in the village as reinforcement for the Chirakkal force. The rebels manages to defeat the combined forces and plan to march to Chirakkal. However, Vavvali is killed in action. The rebels now launch an attack on the Chirakkal Palace. A terrible battle rages on. The rebels are immediately put on the back-foot by the Portuguese cannon guns. Kelu manages to breach the perimeter set up by Estêvão and enters the palace. He manages to attack da Gama and is soon killed by the musketeers.

Listening to the story of his ancestors, Krishna Das decides not to sell his land to the multinational mining corporation and plans to start a new life in Kerala.

== Main cast ==
* Prithviraj Sukumaran - Kelu/Krishna Das ("K.D.")
* Prabhu Deva - Vavvali of Nagapattinam/Thanseer ("Tarzan")
* Genelia DSouza - Ayesha of Arackel/Nominee of the plot
* Nithya Menon - Bala of Chirakkal/Daisy da Cunha
* Alexx ONell - Estêvão da Gama (16th century)|Estêvão da Gama/Executive of Nirvana
* Robin Pratt - Vasco Da Gama
* Amole Gupte - King of Chirakkal
* Ankur Khanna - Bhanu Vikraman of Chirakkal
* Jagathy Sreekumar - Chenichery Kurup/Kerala Minister
* K. P. A. C. Lalitha - Narrator
* Shaji Nadesan - MLA Raveendra Kaimal/Kaimal
* Sasi Kalinga - Chakrathali Hasim Pakir

=== Extended cameos === Arya - Kothuwal of Chirakkal/Thankachan
* Vidya Balan - Makkom/Teacher Bhumi Tabu - Dancing woman

==Soundtrack==
The songs and the background score for the film have been composed by Deepak Dev with lyrics penned by Kaithapram Damodaran Namboothiri, Rafeeq Ahammed and Engandiyur Chandrasekharan. The music album consists of nine songs. According to Deepak Dev, composing songs for Urumi was a challenge as Santosh Sivan had proscribed all electronic music, as the film is set in the sixteenth century. It was Prithviraj who suggested Deepak Dev to Sivan. The songs encompasses many genres – folk, lullaby, ballad etc. The vocalists range from the most experienced K. J. Yesudas to newcomers Job Kurian and Reshmi Sathish. The song "Chimmi Chimmi" is done as a tribute to M. G. Radhakrishnan, a composer Deepak Dev said to have admires.

Music is an organic part of the film as it takes the narrative along. Deepak Dev composed the five songs for the film, the lyrics of which have been written by Rafeeq Ahmed, Kaithapram, Prashanth Narayan and Chandru.   

{| class="wikitable"
|-  style="background:#ccc; text-align:center;"
! Track #
! Title
! Singer(s)
! Length
|-
| 1|| "Aaranne Aarane" || Job Kurian, Rita || 4:19
|-
| 2|| "Aaro Nee Aaro" || K. J. Yesudas, Swetha Mohan|| 6:20
|-
| 3|| "Chimmi Chimmi" || Manjari Babu || 2:44
|-
| 4|| "Appaa" || Reshmi Sathish || 3:19
|-
| 5|| "Vadakku Vadakku -Friendship Remix" || Guru Kiran, Shaan Rahman || 2:52
|-
| 6|| "Thelu Thele" || KR Renji || 3:51
|- Prithviraj || 3:27
|-
| 8|| "Chalanam Chalanam" || Reshmi Sathish || 3:47
|-
| 9|| "Theme Music" || Mili (Humming) || 3:05
|}

===Plagiarism accusations===
The song "Aaro Nee Aaro" in the film is alleged to be plagiarised from Loreena McKennitts "Caravanserai" of the album An Ancient Muse. The track also uses major hooks from Loreenas famous track "The Mummers Dance". Loreena McKennit filed a plagiarism suit against composer Deepak Dev and the makers of Urumi in Delhi High Court. On 21 September 2011, Justice Manmohan Singh of Delhi High Court passed an order on a copyright infringement claim preventing the makers from releasing the soundtrack in English, Hindi and Tamil. Since the producers failed to appear in court, Delhi High court Judge Hema Kohli passed an arrest warrant against actor Prithviraj, Santhosh Sivan and Shaji Natesan.   

==Production==
 , Pune district|Pune]]

"Whenever you travel to Goa or Fort Kochi or such places, you will always find a suite in the name of Vasco da Gama who is revered as a discoverer of India. But when you delve deeper into the history, you will realize that he discovered India for the Western world but he is the conqueror, the first colonial ruler in the world as they all came to trade in pepper but instead of trading they decided to conquer the place. Hence I thought it would be interesting to make a film that would show the small peppercorn changing the entire history of India. I think for every Indian it would be interesting." Santosh Sivan, the cinematographer-cum-director, explained his initial thoughts on a film on Vasco da Gama. http://www.videos.behindwoods.com/videos-q1-09/director-interview/santosh-sivan-14-08-2011.html 

"Santosh   and I used to keep discussing a historical film during the making of Raavanan. We roped in Shaji Nadesan, a friend of ours, and thus was born August Cinema.", Prithviraj Sukumaran later explained to the press about his production company.  "I like the idea of recreating the bygone era. It is interesting to think of the characters you have heard as real. Also, it excited me to have cannons, swords and urumi (curling blades) in a film.", Santosh Sivan was highly inspired about the concept. 
 Ranjith Balakrishnans associate director for some time, had scripted a tale for a competition based on the medieval history of Kerala. “Called Chekavar, it was on the gallant warriors of Malabar and the pageant of the Mamankam. I had shown it to Prithviraj   during the shooting of Thirakkatha. He was quite taken up with the script and mentioned it to Santhosh   when the two were working on Raavanan. That is how Santosh   got in touch with me,” explains Shankar Ramakrishnan.     "I was nearly imprisoned in Santosh Sivans flat in Mumbai for about two months when I was writing the story of Urumi. Finally I told him the one-line story of a boy who wanted to kill Vasco da Gama and the movie took off smoothly from then on. I did not see it as a period film as I felt that the issues it dealt with were contemporary", reveals Shankar Ramakrishnan.    Shankar spent two years gathering the material for his script and doing research to flesh out his characters, some of whom are familiar names in Indian history. He went to Kannur and read old ballads and stories of the region. 
 Alexx O’Neill, who hails from Connecticut, was cast as Estêvão da Gama. "When I had signed Urumi, it was basically for a single character Estavio Da Gama, but later I played Vasco Da Gama’s role too   and had to speak only in Portuguese", O’Neill said. "I wanted to be very true to the accent and the way the character would speak at that time. So I hired a person in Mumbai before the shooting began. Even during the dubbing I had someone to assist me, so that I don’t go wrong with the pronunciation", he added. 

Some changes were made in the dresses worn by the characters from original historical attires, particularly those worn by the women. "You cannot re-create exactly how it was then, as women were topless in those days. So, you stylise the kind of dresses they wore in that era.", Sivan said to Rediff.  Shooting for the film started on 17 August 2010.  The main locations were Kerala and the forests of Malshej Ghat in Maharashtra.  Most of the scenes were painstakingly captured by Santosh Sivan in mostly natural light with the modest of budgets and minimal visual effects.    "It was tedious and the terrain was difficult. On screen, it looks beautiful but we shot standing in slush almost 24/7. People got foot infections. It was laborious.", says Nithya Menon, who played a Chirakkal princess.  The film was shot with a combination of various formats. Canon EOS 5D was extensively used, especially for the sensuous song featuring Vidya Balan.  The shooting lasted a period of seven months in the states of Kerala, Karnataka and Maharashtra. http://www.indianexpress.com/news/urumi-is-my-comment-on-globalisation-santosh-sivan/769710/0 
 Harishchadragad in Lakshya and Ananthabhadram is the art director of the film. 

Santosh Sivan was heavily involved in bringing out a "tailored version" of the film in Telugu 2011, and in Tamil in 2012.     Many scenes in the film were re-shot to avoid lip-sync problems. http://www.thehindu.com/features/cinema/sivan-in-focus/article3480385.ece 

=== The English version ===
The English version is expected to be just 110-minute-long, which is 55 minutes short when compared to the Malayalam film. The director had also earlier confirmed that the English version will showcase the brutal side of the Portuguese invader. The English version, however, will be released only in 2015. "The English flick, titled "Vasco Da Gama", is totally different from the Malayalam version in terms of structure and story. "We will be retaining only 30% of the scenes from the original.", says Santosh Sivan, adding that the extra sequences are already shot. The script is being sponsored by a forum of the Hong Kong International Film Festival. 

== Themes ==
According to cinematographer and filmmaker Santosh Sivan film is his comment on globalization. He adds that the film resonates with modern times where corporate lobbies are causing displacement of indigenous people across the world. "I have traveled across the world while shooting for films and documentaries and I have seen first hand the displacement and exploitation, the side effects of globalization being suffered by the people who live in close contact to nature. The film centres around a similar situation, but it is removed by a few centuries," said Sivan. The film also focuses on a new perspective of storytelling. "History is written by the victors, the powerful who won. So was Vasco da Gama a brave explorer or a invader after gold." he added. 

"The film is designed in such a way that it talks about the present and the past. In the past some people came and exploited our land and it is happening even now. Perhaps the people who lived then are the people who live now. Still we are not united and our progress is not uniform. The film has also portrayed this aspect in a different manner.", Sivan pointed out Jyothsna Bhavanishankar of Behind the Woods. 

Shankar Ramakrishnan, who has written the story and screenplay for Urumi after doing an extensive research, said that the film presents history from a different perspective. "Even a small child in Kerala perceives Vasco da Gama as an explorer, who made the first-ever colonial invasion in any part of the world. But theres more to him than that. Urumi is an attempt to portray or rather discuss the many realities that could have affected the course of our history," he said. Shankar Ramakrishnan added that the title is not just suggestive of Kelus Urumi, but the feeling of vengeance that we carry in our hearts. 

==Reception==
The first exclusive sneak preview of the film was held exclusively for Mani Ratnam. After watching the film, Mani Ratnam was all praise for Santosh Sivan, who was away in Trivandrum promoting the film. Says Mani, "Urumi is huge. It is entertaining and the performances are very convincing. Its simply Santosh magic." 
 Best Sound Recordist for his work in Urumi]]

The film opened well with positive reviews from both critics and viewers.   It earned accolades as arguably one of the best historical fantasies Malayalam cinema has ever seen and was critically acclaimed at film festivals around the world. 

"The Hindu" described the film as "a landmark film in Malayalam".  Sarswathy Nagarajan describes, "It is a decisive turn for Malayalam cinema because Urumi, while broadening the horizons of Mollywood, is also an attempt to reach out to a global audience. The lavishly made Urumi brings together a host of talented actors and technical personnel from Indian cinema."  "Nowrunning" gave the film 3/5 rating and comments the story as "timeless, the images magical, the acting solid, the script first-rate, the romance delightful, the action deadly and the energy raw – in short, the kind of film that one loves to see, and then animatedly write about."  "Rediff" gave a 3/5 rating for the movie.  "Sify" also gave 4/5 rating with movie verdict being "Very Good". According to Sify, "Urumi is a fairy tale fantasy film that has a heart and technical artistry."  Indiaglitz rated the movie as a "Must see" and commented on the movie as follows : "All in all, Urumi is a must watch for all those who love quality cinema. Redefining the traditional qualities of period dramas, Urumi is sure to become a movie that will be respected and adored by Mollywood for its creative panache, tremendous performances and great technical wizardry." 

The film was also one of the seven Malayalam films selected to be screened at the Indian Panorama section of International Film Festival of India. 
 Kalaipuli S Dhanus V Creations opened to positive reception by critics as well as audience. http://www.indiaglitz.com/channels/tamil/review/13090.html
  http://www.indiaglitz.com/channels/tamil/article/82492.html  "The Hindu" wrote of the Tamil version, "From the very first scene of this film to the last, director-cum-cinematographer Santosh Sivan proves to be on top of this technical masterpiece. The songs, waterfall sequences and the war scenes evoke a feel of realism due to its stunning visuals and brilliant re-recording. Certain portions in the film become very melodramatic to suit the Indian audiences thereby derailing the pace. The brilliantly choreographed action sequences seem to be a tad too loud for the ears."  Notable websites and Tamil magazines praised the dialogs of the Tamil version. 

The film has also caught the eye of Academy award winning filmmaker Oliver Stone. "I had met Stone while he was in Mumbai a few months ago and he was curious when he came to know that I was doing a film on Da Gama. We had a discussion about it and he wanted me to send him a copy once the film was done," said Santosh Sivan. 

==Awards==
; Kerala State Film Award
* Best Background Music - Deepak Dev
* Best Sound Recordist - M. R. Rajakrishnan

; Imagineindia International Film Festival (Madrid)   . Imagineindia International Film Festival. 30 May 2012. Retrieved 8 June 2012. 
* Best Film – Urumi
* Best Director – Santosh Sivan

; Nana Film Awards
* Second Best Actor – Jagathy Sreekumar 
* Best Cinematographer – Santosh Sivan
* Best Music Director – Deepak Dev
* Best Lyricist – Rafeeq Ahammed
* Best Female Playback Singer – Manjari Babu
* Best Makeup Artist – Ranjith Ambady
* Best actor – Prithviraj
; Reporter Film Awards 
* Best Cinematographer – Santosh Sivan
* Best Art Director – Sunil Babu
* Best Visual Effects – Karthik Kotamraju
* Best Background Score – Deepak Dev
* Best Sound Mixing & Recording – M. R. Rajakrishnan & Anand Babu

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 