Drive, He Said
{{Infobox Film
| name           = Drive, He Said
| image          = Drive he said.jpg
| caption        = Theatrical release poster
| director       = Jack Nicholson Steve Blauner Jack Nicholson
| writer         = Jeremy Larner Jack Nicholson Terrence Malick (uncredited)
| starring       = William Tepper   Karen Black   Bruce Dern   Robert Towne   Henry Jaglom
| music          = David Shire Bill Butler
| editing        = Donn Cambern   Christopher Holmes   Pat Somerset   Robert L. Wolfe
| studio         = BBS Productions Drive Productions Inc.
| distributor    = Columbia Pictures
| released       =   June 13, 1971     June 30, 1971     July 24, 1971
| runtime        = 90 mins
| country        = United States
| language       = English
| budget         = $800,000
}} The Monkees, Easy Rider) and its successor, BBS Productions. Based upon the 1964 novel of the same title by Jeremy Larner, the film is mainly notable as the directorial debut of Jack Nicholson (who also wrote the screenplay) following his breakthrough as an actor in Easy Rider (1969) and Five Easy Pieces (1970).
 Bill Butler One Flew Over The Cuckoos Nest. Original music was composed by David Shire (then married to Coppolas sister Talia Shire) and the screenplay included uncredited contributions from future director Terence Malick. 
 Michael Warren (Hill St Blues), who (like Tepper) was also a former collegiate basketball player.

It was filmed on the campus of the University of Oregon and other locations in Eugene, Oregon. The film is also notable for its controversial (for the time) use of profanity, its depictions of sex and drug use, and for several scenes of male frontal nudity, including a locker-room shower scene, and the mental breakdown scene in which Gabriel (Margotta) is shown frontally nude, which led to an attempt by the censor to give the film an X rating.

==Synopsis==
The film is an examination of libidinous basketball star Hector Bloom ( , and Vietnam War|anti-war sentiments.

==Cast==
*William Tepper as Hector
*Karen Black as Olive
*Michael Margotta as Gabriel
*Bruce Dern as Coach Bullion
*Robert Towne as Richard
*Henry Jaglom as Conrad Michael Warren as Easly
*June Fairchild as Sylvie
*Don Hanmer as Director of Athletics
*Lynette Bernay as Dance Instructor Joseph Walsh as First Announcer
*Harry Gittes as Second Announcer Charles Robinson as Jollop
*Bill Sweek as Finnegan
*David Ogden Stiers as Pro Owner
*B.J. Merholz as Pro Lawyer
*I.J. Jefferson as Secretary
*Kenneth Payne as President Wallop
*Cathy Bradford as Rosemary
*Eric Johnson as Private First Class Johnson
*Cindy Williams as team managers girlfriend

==Critical reception==
The film was entered into the 1971 Cannes Film Festival.   

Contemporary reviews of the film were mixed. Steven Scheuer found the film "utterly downbeat, and unfortunately dated".   Roger Ebert found the film "disorganized", but also said it was "occasionally brilliant" with the performances being "the best thing in the movie", including the "laconic charm" of Tepper. 

In contrast, Leonard Maltin found the film "confusing", and while he also praised the acting performances, he found that the film "loses itself in its attempt to cover all the bases".  Vincent Canby was complimentary when he lauded the film as being "so much better than all of the rest of the campus junk Hollywood has manufactured in the last couple of years" but felt that the lead male performance was a let-down for the film as a whole. 

==References==
 

==Notes==
* , June 14, 1971. (accessed 9 January 2008).
*Ebert, Roger (1972)  , Chicago Sun-Times, January 1, 1972. (accessed 9 January 2008).
*Maltin, Leonard (1991) Leonard Maltins Movie and Video Guide 1992, Signet, New York.
*Scheuer, Steven H. (1990) Movies on TV and Videocassette, Bamtam Books, New York.

==External links==
* 
 

 
 
 
 
 
 
 
 