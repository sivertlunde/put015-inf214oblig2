Inang Yaya
{{Infobox film
| image               = 
| caption             =
| name                = Inang Yaya
| director            = Pablo Biglang-awa (as Pablo B. Biglang-awa Jr), Veronica Velasco (as Veronica B. Velasco)
| producer            =
| music               =
| cinematography      =
| story               =
| screenplay          =
| writer              = Veronica Velasco
| editing             =
| starring            = Maricel Soriano   Tala Santos   Erika Oreta   Sunshine Cruz   Zoren Legaspi
| studio              =
| distributor         =
| released            =  
| country             = Philippines
| language            =  
| runtime             = 
| budget              =
| gross               = 
}}

Inang Yaya (Mother Nanny) is a 2006 movie directed by "Pablo Biglang-awa" and "Veronica Velasco" and stars Maricel Soriano as Norma, a nanny who has to choose between Ruby (Tala Santos, her daughter) or Louise (Erika Oreta, the girl who she takes care of). The film won Best Film and Best Performance, awards given by the Young Critics Circle. 

==Casts==
*Maricel Soriano as Norma
*Erika Oreta as Louise
*Tala Santos as Ruby
*Sunshine Cruz as May 
*Zoren Legaspi as Noel
*Liza Lorena as Lola Toots
*Marita Zobel as Lola Tersing
*Matthew Mendoza as Mon
*Kalila Aguilos as Luz (as Kalila Agiloz)
*J.M Reyes as Carlo (as John Michael Reyes)
*Jessu Trinidad as Cocoy
*Erica Dehesa as Alexa
*Julia Buencamino as Margarita
*Mary Agustin as Nana
*Roence Santos as Tess

==References==
 

==External links==
* 

 
 
 
 
 


 
 