Men Who Have Made Love to Me
 
{{Infobox film
| name           = Men Who Have Made Love to Me
| image          = Men Who Have Made Love to Me (1918 film).jpg
| image_size     =
| border         =
| alt            =
| caption        =
| director       = Arthur Berthelet
| producer       = George K. Spoor
| writer         = Edward T. Lowe, Jr.|E. C. Lowe Mary MacLane
| screenplay     =
| story          =
| based on       =  
| narrator       = Paul Harvey
| music          =
| cinematography =
| editing        =
| studio         =
| distributor    = Essanay
| released       =  
| runtime        = 70 minutes (7 reels)
| country        = United States Silent English intertitles
| budget         =
| gross          =
}}

Men Who Have Made Love to Me is a 1918 silent movie starring Mary MacLane, based on her book I, Mary MacLane (1917), and directed by Arthur Berthelet.   

The film was produced by early American film-maker, George K. Spoor.

==Cast==
*Mary MacLane ... Herself
*Ralph Graves ... The Callow Youth Paul Harvey ... The Literary Man (as R. Paul Harvey)
*Cliff Worman ... The Younger Son
*Alador Prince ... The Prize Fighter
*Clarence Derwent ... The Bank Clerk
*Fred Tiden ... The Husband of Another

==Plot==
The story of six affairs of the heart, drawn from controversial feminist author Mary MacLanes 1910 syndicated article(s) by the same name, later published in book form in 1917. None of MacLanes affairs - with "the bank clerk," "the prize-fighter," "the husband of another," and so on - last, and in each of them MacLane emerges dominant. Re-enactments of the love affairs are interspersed with MacLane addressing the camera (while smoking), and talking contemplatively with her maid on the meaning and prospects of love. 

==Preservation status==
This film is now thought to be a lost film.

==References==
 

==External links==
*  
*  

 
 
 
 
 
 


 