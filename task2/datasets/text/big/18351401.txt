Beautiful Memories
{{Infobox film
| name           = Se souvenir des belles choses
| image          = 
| caption        = 
| director       = Zabou Breitman
| producer       = Stéphane Marsil
| writer         = 
| screenplay     = Zabou Breitman Jean-Claude Deret
| story          = 
| based on       =  
| narrator       = 
| starring       = Isabelle Carré Bernard Campan Bernard Le Coq Zabou Breitman
| music          = Ferenc Javori
| cinematography = Dominique Chapuis
| editing        = Bernard Sasia
| studio         = Hugo Films France 3 Cinéma Les Films de la Colombe Les Productions de la Guéville
| distributor    = Wild Bunch Distribution
| released       =  
| runtime        = 110 minutes
| country        = France
| language       = French
| budget         = $3,7 millions
| gross          = $4,603,327 
}}
 Best Debut, Best Actress Best Supporting Actor, and was nominated for Best Actor. Also, the French Syndicate of Cinema Critics named it best debut film.

== Plot ==
Claire Poussin, a young woman in her early 30s whose mother has recently died from Alzheimers, has been having memory loss problems since being struck by lightning. She believes she is showing the first signs of the disease, but her sister Nathalie thinks the problem is temporary. Claire seeks help by entering a clinic for people with memory-loss problems, which is located in a big country house and run by Prof. Christian Licht. Prof. Licht is having an affair with therapist Marie Bjorg, which he thinks is hidden from his patients, but isnt. At the clinic, Claire meets Philippe, a noted wine expert who is traumatized following a car accident which killed his wife and child, and they fall in love. When both of them are released, they move in together, but find that their condition severely affects their lives. Bernard recovers his memory, and is pained when he remembers the tragic accident, while Claires condition becomes worse.

== Cast ==
*Isabelle Carré as Claire Poussin
*Bernard Campan as Philippe
*Bernard Le Coq as Prof. Christian Licht
*Zabou Breitman as Marie Bjorg
*Anne Le Ny as Nathalie Poussin
*Dominique Pinon as Robert 
*Aude Briant as Corinne 
*Denys Granier-Deferre as Toto 
*François Levantal as Daniel 
*Jean-Claude Déret as Léo Finkel 
*Celine Lomez|Céline Léger as Sarah
*Julien Courbey as Stéphane

==References==
 

== External links ==
* 

 

 
 
 
 
 
 


 