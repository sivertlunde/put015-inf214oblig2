The Woman in the Septic Tank
{{Infobox film
| name           = The Woman in the Septic Tank
| image          = Babaesaseptictankposter.png
| caption        = Theatrical movie poster
| director       = Marlon Rivera
| producer       = Chris Martinez  Marlon Rivera Josabeth Alonso John Victor Tence
| writer         = Chris Martinez
| screenplay     = Chris Martinez
| story          = 
| based on       =  
| starring       = JM De Guzman Kean Cipriano Cai Cortez Eugene Domingo
| music          = Vincent De Jesus Albert Michael Idioma Addiss Tabong
| cinematography = Larry Manda
| editing        = Ike Veneracion
| studio         = Cinemalaya Martinez Rivera Films  Quantum Films Straight Shooters Media, Inc.
| distributor    = Star Cinema
| released       =  
| runtime        = 
| country        = Philippines
| language       = Tagalog
| budget         = 
| gross          = P38.4 million 
}} 2011 Cinema Filipino comedy comedy independent film directed by Marlon N. Rivera, written by Chris Martinez, and starring JM De Guzman, Kean Cipriano, Cai Cortez and Eugene Domingo.   accessed on July 20, 2011 via www.pep.ph   The film follows three aspiring filmmakers who are set out to make a film for the sole purpose of receiving international recognition and awards. 

The Woman in the Septic Tank was submitted as the Philippines official entry for the 2011 Academy Awards for Best Foreign Film, and was an entry for the 2011   and grossed 38.4 million pesos and was the highest grossing Filipino independent film at the time until it was overtaken in 2015 by That Thing Called Tadhana.   

==Overview==
 7th Cinemalaya Independent Film Festival. The festival was held from July 15 to the 24th at the Cultural Center of the Philippines.

After the mass critical success of the movie at the film festival, Star Cinema purchased the rights to release the film for a wider release.   retrieved on July 31, 2011 via www.pep.ph  The film was released on August 3, 2011 to over 50 theaters nationwide and grossed almost 40 million pesos becoming the highest grossing Philippine independent film at the time. 

===Synopsis=== Payatas dumpsite. They believe they have a winning script, and the energy and drive to make their dreams come true, no matter what the cost.

==Cast==
*Eugene Domingo plays a fictionalized version of herself, whom the filmmakers want to star in their movie Walang Wala (With Nothing) 
**Domingo also portrays Mila, the protagonist of With Nothing.
*JM De Guzman as Bingbong, the producer of With Nothing, who is focused on adjusting the film to help them gain recognition from festivals and win awards.
*Kean Cipriano as Rainier de la Cuesta, the director of With Nothing.
*Cai Cortez as Jocelyn, the mostly silent production manager of Bingbong who visualizes several scenarios of With Nothing presented by Rainier and Bingbong.

Cherry Pie Picache and Mercedes Cabral appear in cameo appearances as themselves, who are the other options of Bingbong, Rainier and Jocelyn in casting the part of Mila. 

==Reception==

===Box office===
The film broke box office records for the Cinemelaya Independent Film Festival. It is the first full-length film to have its audience tickets sell to its maximum capacity within ten days of showing at the festival. The film was a commercial as well as critical success.   The film earned a total of P20 million on its first five days.  The total gross of the film amounted to P38.4 million, making it the highest grossing independent film in the history of Philippine cinema.   accessed on September 9, 2011 via www.pep.ph 

===International=== Groundhog Day of pilot filmmaking."   
 Far East Best Foreign Language Film category of the 84th Academy Awards,       but it did not make the final shortlist.   

===Awards and nominations===
 
|- 2011
|rowspan="5"| 7th Cinemalaya Independent Film Festival * Best Screenplay (Chris Martinez)
| 
|- Audience Choice Awards 
| 
|- Best Performance of an Actress (Eugene Domingo)
| 
|- Best Director (Marlon N. Rivera)
| 
|- Best Picture
| 
|- 2012
|rowspan="2"| 6th Asian Film Awards  Best Screenwriter (Chris Martinez)
| 
|-
|Peoples Choice Awards - Favorite Actress (Eugene Domingo)
| 
|-
| German Federal Ministry for Economic Cooperation and Development (BMZ)
| Cinema Fairbindet Award
| 
|-
|rowspan="4"| 10th Gawad TANGLAW for Films  Best Director (Marlon Rivera)
| 
|- Best Screenplay (Chris Martinez)
| 
|- Best Actress (Eugene Domingo)
| 
|- Best Film
| 
|- 
|rowspan="10"| 28th PMPC Star Awards for Movies 
| Digital Movie of the Year
| 
|-
| Digital Movie Director of the Year (Marlon Rivera)
| 
|- Movie Actress of the Year (Eugene Domingo)
| 
|- Digital Movie Original Screenplay of the Year (Chris Martinez)
| 
|- Digital Movie Cinematographer of the Year (Larry Manda)
| 
|- Digital Movie Editor of the Year (Ike Veneracion)
| 
|- Digital Movie Production of the Year (Norma Regalado)
| 
|- Digital Movie Musical Scorer of the Year (Vincent de Jesus)
| 
|- Digital Movie Sound Engineer of the Year (Albert Michael Idioma & Addiss Tabong)
| 
|- Digital Movie Theme Song of the Year ("Sabaw")
| 
|}

 

==See also==
* List of submissions to the 84th Academy Awards for Best Foreign Language Film
* List of Philippine submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  on Cinemalaya.com
*  
*  Official Website

 
 
 
 
 
 