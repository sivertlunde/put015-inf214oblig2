The Bodybuilder and I
{{Infobox Film
| name           = The Bodybuilder and I
| image          = 
| image_size     = 
| caption        = 
| director       = Bryan Friedman
| producer       = Julia Rosenberg Anita Lee
| writer         = Bryan Friedman
| narrator       = 
| starring       = Bill Friedman Jim Guthrie
| cinematography = Alan Poon
| editing        = Seth Poulin
| distributor    = January Films
| released       = 2007
| runtime        = 92 minutes
| country        = Canada English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
The Bodybuilder and I is a feature-length documentary film written and directed by Bryan Friedman, taking viewers on a journey into the subculture of geriatric bodybuilding as the filmmaker tries to reconnect with his father.  The film is co-produced by January Films and the National Film Board of Canada.

==Synopsis==
59-year-old Bill Friedman is a competitive bodybuilder - a former world champ in the age 50-60 category, determined to win his title back. Documentary director Bryan Friedman is 26. Bill - Bryans dad - hasnt been around since Bryan was a baby. In The Bodybuilder and I, Bryan chronicles his fathers attempt to make it back to the top. In the process of making the film, the two men get to know one another and maybe begin to understand each other.

==Awards==
Winner of the Best Canadian Feature Documentary Award at Hot Docs and co-winner of Best Canadian Documentary
Award at the Atlantic Film Festival.  

==Theatrical release==
The Bodybuilder and I began a limited theatrical release in Toronto, Vancouver and Ottawa on November 2, 2007. 

==See also==
*Afghan Muscles

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 

 
 