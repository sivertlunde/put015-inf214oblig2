Jungle 2 Jungle
{{Infobox film
| name           = Jungle 2 Jungle
| image          = Jungle two jungle ver1.jpg
| caption        = Theatrical release poster
| director       = John Pasquin
| producer       = Richard Baker Brad Krevoy
| writer         = Bruce A. Evans Raynold Gideon
| starring       = Tim Allen Martin Short Sam Huntington Lolita Davidovich David Ogden Stiers JoBeth Williams
| music          = Michael Convertino
| cinematography = Tony Pierce-Roberts
| editing        = Michael A. Stevenson TF1 International Buena Vista Pictures
| released       =  
| runtime        = 105 minutes
| country        = United States
| language       = English
| budget         = $32 million
| gross          = $59.9 million
}} TF1 Films New York businessman who discovers he has a teenage son from a South American tribe of Amerindians. Upon arriving in New York, they get taste of each others respective cultures. The film performed modestly at the box office and was poorly received by critics.

==Plot==
Michael Cromwell (Tim Allen) is a self-absorbed, successful commodities broker living in New York City.  Wanting to marry his new fiancée Charlotte (Lolita Davidovich), he needs to obtain a final divorce from his first wife, Patricia (JoBeth Williams), who left him some years earlier.  Patricia now lives with a semi-Westernised tribe in Canaima National Park, Venezuela.  Michael travels there to get her signature on divorce papers, but upon arriving, discovers that he has a 13-year-old son named Mimi-Siku (Sam Huntington).
 building 7.

Michaels fiancée, Charlotte, is less than pleased about the unexpected visitor in a loin cloth outfit, who tries to urinate in front of her at a fake tree (as is usual in his tribe), suggests eating her cat, and Maitika, his enormous pet tarantula escapes from his box and into her apartment. Mimi-Siku wears traditional dress during much of his stay in New York. As Michael attempts to adapt Mimi-Siku to city life, cross-cultural misunderstandings occur when Mimi-Siku reverts to customs considered acceptable by his tribe. On climbing the Statue of Liberty to reach the flame, Mimi-Siku is disappointed when he sees that the fire is not real.
 Poecilia latipinna fish.  Richard freaks out when he sees Mimi and Karen together in a hammock and threatens to send her to an all-girls summer camp.

The Kempsters and Michael are being targeted by Alex Jovanovic (David Ogden Stiers), a Russian mobster and caviar dealer, who believes that they have cheated him in a business deal. Jovanovic arrives at the Kempsters, and tortures Richard for info. Since he refuses, he tries to amputate Richards fingers in revenge. By fighting together and utilizing Mimi-Sikus hunting skills (and Maitika), the two families defeat the mobsters.
 blowpipe and poisoned darts, telling Michael to practice and come to see him when he can hit flies.

Shortly afterwards, Michael finds himself disheartened by the rat-race and realizes that his relationship with Charlotte is not working for him anymore.  He attempts to kill a fly with his blowpipe on the trading floor of the New York Board of Trade.  He hits the fly, but also Langston, his boss, who collapses asleep on the trading floor.

Michael returns to Lipo-Lipo to see his son and ex-wife, bringing the Kempster family with him for a vacation. Karen and Mimi are reunited, and it is implied that Michael and Patricia also resume their relationship.

The film ends with Michael undergoing the rite of passage as Mimi did earlier.

==Cast==
* Tim Allen as Michael Cromwell
* Martin Short as Richard Kempster 
* Sam Huntington as Mimi-Siku Cromwell
* Jobeth Williams as Dr. Patricia Cromwell
* Lolita Davidovich as Charlotte
* David Ogden Stiers as Alexei Jovanovic
* Bob Dishy as George Langston
* Valerie Mahaffey as Jan Kempster
* Leelee Sobieski as Karen Kempster
* Frankie J. Galasso as Andrew Kempster
* Luis Avalos as Abe
* Carole Shelley as Fiona Glockman
* Dominic Keating as Ian
* Rondi Reed as Sarah
* Oni Faida Lampley as Madeleine

==Reception==
 
Jungle 2 Jungle was the 7th highest grossing PG-rated movie of 1997, taking in about $59.9 million in the US. The financial result is widely considered to be lower than expected, and the film was overall panned by audiences. 

  Little Indian, Siskel and Ebert, Ebert had said Jungle 2 Jungle was not as bad as Little Indian, Big City because it was "far too mediocre to be terrible." He also described it as "lamebrained, boring, predictable, long, and slow" and said while the French version was memorably bad Jungle 2 Jungle was "just forgettable". Eberts colleague Gene Siskel mildly disagreed saying that he felt Jungle 2 Jungle was just as bad as Little Indian, Big City. He also said he felt embarrassed for Tim Allen and Martin Short as he felt they were used far better in other television programs and films.  Siskel later went on to declare this as the worst film of 1997. 

Review aggregation website Rotten Tomatoes gives a rating of 20% based on reviews from 41 critics.  
 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 