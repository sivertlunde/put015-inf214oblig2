Toki o Kakeru Shōjo (1997 film)
{{Infobox film
| name = Toki o Kakeru Shōjo
| image = Toki o Kakeru Shōjo 1997 film DVD cover.jpg
| size = 200px
| caption = DVD cover
| screenplay = Ryoji Ito Chiho Katsura Haruki Kadokawa
| narrator = Tomoyo Harada Shunsuke Nakamura
| director = Haruki Kadokawa
| producer =
| distributor =
| released   = November 8, 1997
| runtime = Japanese
| budget =
}} novel of the same name.  The film was released in Japan on November 8, 1997, directed by Haruki Kadokawa, with a screenplay by Ryōji Itō, Chiho Katsura and Haruki Kadokawa, starring beginner Nana Nakamoto in the main role. The film is narrated by the previous 1983 films lead-actress Tomoyo Harada, and is set in 1965, when the novel was published for the first time. The film poster was used as the new cover for the 1997 edition of the novel.

==Cast==
* Nana Nakamoto as Kazuko Yoshiyama Shunsuke Nakamura as Kazuo Fukamachi
* Mitsuko Baisho
* Takaaki Enoki
* Mariko Hamatani
* Yu Hayami
* Masatô Ibu
* Yoshiko Kuga
* Hironobu Nomura
* Tsunehiko Watase
* Itsumi Yamamura

==Theme songs==
"Yume no Naka de ~We are not alone, forever~" and "Toki no Canzone", a remake of the 1983 films theme song, written and sung by Yumi Matsutoya.

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 

 