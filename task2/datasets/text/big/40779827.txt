The Boy and the World
{{Infobox film
| name           = The Boy and the World
| image          = The Boy and The World Film Poster.jpg
| caption        = Theatrical release poster
| director       = Alê Abreu
| producer       = Tita Tessler Fernanda Carvalho
| writer         = Alê Abreu
| starring       = Vinicius Garcia Lu Horta Marco Aurélio Campos
| music          = Ruben Feffer   Gustavo Kurlat
| cinematography = 
| editing        = Alê Abreu
| studio         = Filme de Papel
| distributor    = Espaço Filmes 
| released       =  
| runtime        = 80 minutes
| country        = Brazil
| language       = Portuguese
| budget         =
| gross          = 
}} animated film directed by Alê Abreu. 

==Plot==
Cuca is a boy who lives in a distant world, in a small village in the interior of his mythical country. One day, he sees his father leaving in search of work, embarking on a train towards to an unknown capital. The weeks that follow are of anguish and confusing memories. Until then, one night, a breath of wind breaks into the bedroom window and takes the boy to a distant and magical place. 

==Cast==
*Vinicius Garcia
*Lu Horta
*Marco Aurélio Campos

==Reception==
The films worldwide premiere occurred at the Ottawa International Animation Festival  where it won the an Honourable Mention for Best Animated Feature "Because it was full of some of the most beautiful images weve ever seen".  It also earned an honorable mention at the Festival do Rio and won the Youth Award at the Mostra de Cinema de São Paulo.  At the Festival de Cinema de Animação de Lisboa, it won the Best Film Award.  At the Annecy International Animated Film Festival, it won the Cristal Award for Best Feature Film, and was voted the favorite film by the audience. 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 

 
 