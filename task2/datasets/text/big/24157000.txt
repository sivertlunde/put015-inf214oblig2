City Dump: The Story of the 1951 CCNY Basketball Scandal
{{Infobox film|
  name           = City Dump: The Story of the 1951 CCNY Basketball Scandal|
  image          =  City Dump (CCNY).jpg|
  producer       =  |
  director       = George Roy  Steven Hilliard Stern |
  writer         =   |
  starring       =   |
  music          =  |
  cinematography =  |
  editing        =  |
  distributor    =  |
  released       = 1998 |
  runtime        =  |
  country        = United States | English |
  followed_by    =
}}
City Dump: The Story of the 1951 CCNY Basketball Scandal is a 1998 documentary film by George Roy and Steven Hilliard Stern, produced by Black Canyon Productions, and HBO Sports about the CCNY Point Shaving Scandal. The documentary was shown on HBO. 

==Selected Cast==
*Liev Schreiber as himself - narrator
*Maury Allen as himself Dave Anderson as himself
*Marty Glickman as himself
*Nat Holman as himself (archive footage)
*Marvin Kalb as himself
*Al McGuire as himself
*Burt Young as himself - opening narration
*Sidney Zion as himself

==Awards==
*Won &ndash; Gold Apple at the National Educational Media Network Awards (1999)
*Won &ndash; Certificate of Merit Television - History for directors George Roy and Steven Hilliard Stern at the San Francisco International Film Festival (1999)

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 


 
 