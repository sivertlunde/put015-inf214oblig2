The Best Mouse Loses
 

{{Infobox Hollywood cartoon|
| cartoon_name = The Best Mouse Loses
| series = Krazy Kat
| image = 
| caption = 
| director = Vernon Stallings
| story_artist = 
| animator = 
| voice_actor = 
| musician = 
| producer = J.R. Bray
| studio = Bray Productions
| distributor = Famous Players Lasky Corporation
| release_date = March 3, 1920
| color_process = Black and white
| runtime = 3:22 English
| preceded_by = Loves Labor Lost (cartoon)|Loves Labor Lost
| followed_by = Kats is Kats
}}

The Best Mouse Loses is a silent short theatrical cartoon by Bray Productions, and features Krazy Kat.

==Plot==
A boxing match is about to begin, and Ignatz Mouse is one of the featured fighters. But Ignatz has no intention of winning when he tells a canine buddy he will go down purposely, and therefore asks the latter to wager his sack of cash on it. The rodent and the dog agree on the idea before entering the venue.

Momentarily, Krazy Kat arrives just outside the venue. He then meets a girl mouse who happens to be a sister of Ignatz. The girl mouse says to Krazy shell be wagering her familys fortune that her brother would emerge victorious. She also requests him to make sure Ignatz prevails which Krazy accepts with pleasure.

The boxing match begins, and Krazy is the referee. Ignatz and the opponent, however, start with some bizarre acts like dancing which prompts Krazy to tell them to actually fight. After the opponent makes a feeble contact, Ignatz drops deliberately, pretending to be knocked cold. Not wanting to disappoint the girl mouse, Krazy counts extremely slow until Ignatz is saved by the bell, much to the mouses surprise. The second round starts but the fighters still hardly engage. After the contenders make a few moves, Krazy strikes and knocks down the opponent using his tail. He then counts out the opponent, and declares Ignatz the winner. Ignatz is infuriated by the bouts outcome, and therefore pummels Krazy with punches.

==External links==
*   at the Big Cartoon Database

 

 
 
 
 
 
 
 
 
 
 


 