The End of Agent W4C
  1967 Czechoslovakia|Czechoslovak film parodying the James Bond secret agent genre. Directed by Václav Vorlíček based on the story by Oldřich Daněk. Runtime 87 min. Mono. Produced by Filmové Studio Barrandov and distributed by Central Office of Film Distribution, Prague.

Produced by Věra Kadlecová. Composer - Svatopluk Havelka, musical director - František Belfín. Cinematography by František Uldrich. Editor - Jaromír Janáček.

Film had a great success in USSR theatrical distribution of the 1960s.

== Plot summary == James Bond movies.
Main hero - Cyril Juan Borguette alias W4C is superspy. Equipped by last invention - Alarm clock (with knife, gun, gas, geiger-muller, microphone-jammer and small atombomb - all inside) is he sent in Prague. There is saltbox with plans of military use  of Venus. Agent must play with international spy network. And on his steps is going accountant Foustka.

==Cast==
* Jan Kačer as Cyril Juan (Agent W4C)
* Květa Fialová as Alice
* Jiří Sovák as Foustka
* Jan Libíček as Resident
* Jiří Pleskot as Chief
* Jiří Lír as Stern
* Josef Hlinomaz as Agent 1
* Walter Taub as Big Chief
* Otto Šimánek as Inventor
* Zdeněk Braunschläger as Agent 2
* Ivo Gubel as Agent 3
* Alena Kreuzmannová
* Lubomír Kostelka
* František Husák
* Petr Čepek
* Vlastimil Hašek
* Jaroslav Kepka
* Svatopluk Skládal
* Antonin Sura
* Oldřich Velen
* Isabela Tylinkova
* Zdenek Blazek
* Vera Bauerova
* Jan Cmíral
* Jaroslav Cmíral
* Jaroslav Heyduk
* Oldřich Hoblik
* Václav Kotva
* Helena Růžičková
* Miloš Vavruška
* Hana Talpová

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 

 
 