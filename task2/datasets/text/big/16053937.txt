The Face on the Bar-Room Floor (1923 film)
 
{{Infobox film
| name           = The Face on the Bar-Room Floor
| image          =
| caption        =
| director       = John Ford William Fox
| writer         = G. Marion Burton Eugene B. Lewis
| based on       =  
| starring       = Henry B. Walthall Ruth Clifford
| music          =
| cinematography = George Schneiderman
| editing        = Fox Film Corporation
| released       =  
| runtime        = 60 minutes
| country        = United States
| language       = Silent   English intertitles
| budget         =
}}

The Face on the Bar-Room Floor is a 1923 American drama film directed by John Ford. The film is considered to be lost film|lost.       It was adapted from the poem of the same name by Hugh Antoine dArcy.

==Cast==
* Henry B. Walthall as Robert Stevens
* Ruth Clifford as Marion Trevor
* Ralph Emerson as Dick Von Vleck (as Walter Emerson) Frederick Sullivan as Thomas Waring
* Alma Bennett as Lottie
* Norval MacGregor as Governor
* Michael Dark as Henry Drew
* Gus Saville as Fisherman

==See also==
*List of lost films

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 


 