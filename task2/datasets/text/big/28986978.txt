L'avare (film)
{{Infobox Film
| name           = Lavare
| image          = 
| caption        = 
| director       = Louis de Funès Jean Girault
| producer       = Christian Fechner  Bernard Artigues
| writer         = Louis de Funès Jean Girault
| starring       = Louis de Funès
| music          = Jean Bizet
| cinematography = Edmond Richard
| editing        = Michel Lewin
| distributor    = A.M.L.F
| released       = 5 March 1980
| runtime        = 125 min.
| country        = France
| awards         =  French
| budget         = 
| gross          = $18,250,890  
| preceded_by    = 
| followed_by    = 
}}
 French comedy movie from 1980, directed by Louis de Funès and Jean Girault, written by Louis de Funès and Jean Girault, and starring by Louis de Funès. The English title of the film is The Miser. It is an adaptation of Molière|Molières famous comedy The Miser|LAvare ou LÉcole du mensonge (The Miser).

De Funès tried to draw out the unhappy side of the character. Harpagon, unloved by humanity, is driven to an obsessive love of money. 

== Cast ==

* Louis de Funès: Harpagon
* Frank David: Cléante
* Hervé Bellon: Valère
* Michel Galabru: Maître Jacques
* Georges Audoubert: Anselme
* Guy Grosso: Brindavoine
* Claude Gensac: Frosine
* Henri Génès: commissioner
* Claire Dupray: Élise, Misers daughter
* Anne Caudry: Marianne, Cléantes girlfriend
* Bernard Menez: La Flèche, Cléantes valet
* Henri Génès: the commissioner
* Michel Modo: la Merluche, Harpagons attendant
* Max Montavon: Maître Simon
* Micheline Bourday: Dame Claude, the housemaid
* Madeleine Barbulée: Mariannes mother

== Reception ==
The film is still unrated by Rotten Tomatoes. 

== References ==
 

==External links==
*  
*  
*   Films de France

 
 
 
 
 
 
 


 