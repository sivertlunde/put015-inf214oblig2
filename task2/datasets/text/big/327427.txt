The Greatest Story Ever Told
 
{{Infobox film
| name           = The Greatest Story Ever Told
| image          = GREATSTO.jpg
| caption        = Theatrical release poster
| director       = George Stevens
| producer       = George Stevens
| screenplay     = James Lee Barrett George Stevens
| starring       = Max von Sydow Dorothy McGuire Charlton Heston Jose Ferrer Telly Savalas Alfred Newman
| cinematography = Loyal Griggs William C. Mellor
| editing        = Harold F. Kress Argyle Nelson, Jr. Frank ONeil
| studio         = George Stevens Productions
| distributor    = United Artists
| released       =  
| runtime        = 260 minutes
| language       = English
| country        = United States
| budget         = $21 million Tino Balio, United Artists: The Company The Changed the Film Industry, Uni of Wisconsin Press, 1987 p 133 
| gross          = $15,473,333 
}}
 Nativity through the Resurrection of Jesus|Resurrection. This film is notable for its large ensemble cast and for being the last film appearance of Claude Rains.

==Cast==
The major roles in the movie were following: 
* Max von Sydow as Jesus the Virgin Mary
* Charlton Heston as John the Baptist
* Claude Rains as Herod the Great
* Jose Ferrer as Herod Antipas
* Telly Savalas as Pontius Pilate
* Martin Landau as Caiaphas
* David McCallum as Judas Iscariot
* Donald Pleasence as "The Dark Hermit" (a personification of Satan)
* Michael Anderson, Jr. as James the Just Matthew
* Joanna Dunham as Mary Magdalene
* Joseph Schildkraut as Nicodemus
* Ed Wynn as "Old Aram"
 Robert Blake, John Considine, Paul Stewart, John Wayne and Shelley Winters.  

==Pre-production== the Gospels. The series was adapted into a 1949 novel by Fulton Oursler, a senior editor at Readers Digest. Medved, Harry & Michael. "The Hollywood Hall of Shame." Perigree Books, 1984. ISBN 0-399-50714-0  Darryl F. Zanuck, the head of 20th Century Fox, acquired the film rights to the Oursler novel shortly after publication,  but never brought it to pre-production.   
 The Diary of Anne Frank at 20th Century Fox, he became aware that the studio owned the rights to the Oursler property. Stevens created a company, "The Greatest Story Productions", to film the novel. 

  as the Centurion.]]
It took two years to write the screenplay. Stevens collaborated with Ivan Moffet and then with James Lee Barrett. It was the only time Stevens received screenplay credit for a film he directed.  Ray Bradbury and Reginald Rose were considered but neither participated. The poet Carl Sandburg was solicited though it is not certain if any of his contributions were included. Sandburg, however, did receive screen credit for "creative association."  

Financial excesses began to grow during pre-production. Stevens commissioned French artist André Girard (1901-1968)|André Girard to prepare 352 oil paintings of Biblical scenes to use as storyboards. Stevens also traveled to the Vatican to see Pope John XXIII for advice. 

In August 1961, 20th Century Fox withdrew from the project, noting that $2.3 million had been spent without any footage being shot. Stevens was given two years to find another studio or 20th Century Fox would reclaim its rights. Stevens moved the film to United Artists. 
 King of Kings  starring Jeffrey Hunter as Jesus. Filmed in Spain, it was critically panned and flopped. King of Kings, when released, turned out to be nearly an hour shorter than The Greatest Story Ever Told.

==Casting==
For The Greatest Story Ever Told, Stevens cast Swedish actor Max von Sydow as Jesus. Von Sydow had never appeared in an English-language film and was best known for his performances in Ingmar Bergmans dramatic films.  Stevens wanted an unknown actor free of secular and unseemly associations in the mind of the public. 

The Greatest Story Ever Told featured an ensemble of well-known actors, many of them in brief, even cameo, appearances. Some critics would later complain that the large cast distracted from the solemnity, notably in the appearance of John Wayne as the Roman centurion who comments on the Crucifixion, in his well-known voice, by stating: "Truly this man was the son of God."  

==Production== Pyramid Lake in Nevada represented the Sea of Galilee, Lake Moab in Utah  was used to film the Sermon on the Mount, and Californias Death Valley was the setting of Jesus 40-day journey into the wilderness.    

Stevens explained his decision to use the U.S. rather than in the Middle East or Europe in 1962. "I wanted to get an effect of grandeur as a background to Christ, and none of the Holy Land areas shape up with the excitement of the American southwest," he said. "I know that Colorado is not the Jordan, nor is Southern Utah Palestine. But our intention is to romanticize the area, and it can be done better here." 

Forty-seven sets were constructed, on location and in Hollywood studios, to accommodate Stevens vision.   

To fill location scenes with extras, Stevens turned to local sources – R.O.T.C. cadets from an Arizona high school played Roman soldiers (after 550 Navajo Indians from a nearby reservation allegedly did not give a convincing performance; other sources claim they werent on set long enough and left early to take part in a tribal election )  and Arizona Department of Welfare provided disabled state aid recipients to play the afflicted who sought Jesus healing. 

Principal photography was scheduled to run three months but ran nine months or more  due to numerous delays and setbacks (most of which were due to Stevens insistence on shooting dozens of retakes in every scene).  Joseph Schildkraut died before completing his performance as Nicodemus, requiring scenes to be rewritten around his absence. Cinematographer William C. Mellor had a fatal heart attack during production; Loyal Griggs, who won an Academy Award for his cinematography on Stevens’ 1953 Western classic Shane (film)|Shane, was brought in to replace him. Joanna Dunham became pregnant, which required costume redesigns and carefully chosen camera angles. 

Much of the production was shot during the winter of 1962-1963, when Arizona had heavy snow. Actor David Sheiner, who played James the Elder, quipped in an interview about the snowdrifts: "I thought we were shooting Nanook of the North."  Stevens was also under pressure to hurry the John the Baptist sequence, which was shot at the Glen Canyon area – it was scheduled to become Lake Powell with the completion of the Glen Canyon Dam, and the production held up the project. 

Stevens brought in two veteran filmmakers. Jean Negulesco filmed sequences in the Jerusalem streets while David Lean shot the prologue featuring Herod the Great.  Lean cast Claude Rains as Herod. 

By the time shooting was completed in August 1963, Stevens had amassed six million feet of Ultra Panavision 70 film (about 1829&nbsp;km or 1136 miles, roughly the radius of the Moon). The budget ran to an astounding $20 million – 2010 equivalent: approximately $142 million – plus additional editing and promotion charges), Hollywood Hall of Shame, p. 142  making it the most expensive film shot in the U.S.  

The film was advertised on its first run as being shown in Cinerama. While it was shown on an ultra-curved screen, it was with one projector. True Cinerama required three projectors running simultaneously. A dozen other films were presented this way in the 1960s.

==Release and reception==
The Greatest Story Ever Told premiered 15 February 1965, 18 months after filming wrapped, at the Warner Cinerama Theatre in New York City. Critical reaction toward the movie was divided once it premiered. In its favor, Variety (magazine)|Variety called the film "a big, powerful moving picture demonstrating vast cinematic resource." The Hollywood Reporter stated: "George Stevens has created a novel, reverent and important film with his view of this crucial event in the history of mankind." 

However,   in   – later notorious as the frequently scathing theater and film critic of  , likewise called the movie "a big windy bore." 

Brendan Gill wrote in The New Yorker:
:If the subject matter werent sacred in the original, we would be responding to the picture in the most charitable way possible by laughing at it from start to finish; this Christian mercy being denied us, we can only sit and sullenly marvel at the energy for which, for more than four hours, the note of serene vulgarity is triumphantly sustained. quoted in Hollywood Hall of Shame, p. 141 

Stevens told a New York Times interviewer: "I have tremendous satisfaction that the job has been done – to its completion – the way I wanted it done; the way I know it should have been done. It belongs to the audiences now…and I prefer to let them judge."  Reviews to the film continue to be mixed, as it currently holds a 37% rating on Rotten Tomatoes based on 19 reviews. 

The original running time was 4 hr 20 min (260 min).  The time was revised three times, to 3 hr 58 min (238 min); to 3 hr 17 min (197 min) for the United Kingdom, and finally 2 hr 17 min (137 min) for general U.S. release.  Commercially, the film was not successful (by 1983 it had grossed less than $8 million, perhaps 17 percent of the amount required to break even),  and its inability to connect with audiences discouraged production of biblical epics for years. 

The Greatest Story Ever Told was nominated for five Academy Awards:    Best Musical Score Best Cinematography (color) Art Direction Richard Day, David S. posthumous nomination), Ray Moyer, Fred M. MacLean, Norman Rockett) Costume Design (color) Special Visual Effects

For the 2001 DVD release, a 3 hr 19 min (199 min) version was presented along with a documentary called He Walks With Beauty, which detailed the film’s tumultuous production history. Its Blu-ray release appeared in 2011. 

==See also== King of Kings – an earlier film about the life of Jesus, released in 1961.
* Cultural depictions of Jesus

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 