Boys of the City
{{Infobox film
| name           = Boys of the City
| image          = Boysofthecity.jpg
| caption        = Theatrical poster
| image size     = 180px
| director       = Joseph H. Lewis
| producer       = Sam Katzman
| writer         = William Lively
| starring       = Bobby Jordan Leo Gorcey
| music          = Lew Porter
| cinematography = Robert E. Cline  Harvey Gould
| editing        = Carl Pierson 
| distributor    = Monogram Pictures Corporation
| released       =  
| runtime        = 68 mins.
}}

Boys of the City is a 1940 black-and-white comedy/thriller film directed by Joseph H. Lewis. It is the second East Side Kids film and the first to star Bobby Jordan, Leo Gorcey, and Ernest Morrison.

==Plot== Dennis Moore), Dave OBrien), the boys guardian, whom he had unjustly sentenced to death, only to have his verdict reversed and Knuckles exonerated. Later that night, when Louise is kidnapped and the Judge found strangled, Giles and Simp (Vince Barnett), the Judges bodyguard, accuse Knuckles of the murder, but the boys capture Simp and Giles and determine to find the murderer themselves. Muggs (Leo Gorcey) and Danny (Bobby Jordan) discover a secret panel in the library wall and enter a passage where they find Louises unconscious body and glimpse the figure of a fleeing man. Knuckles captures the man, who identifies himself as Jim Harrison (Alden Stephen Chase) of the district attorneys office. Amid the confusion, the real killer takes Louise captive, but the boys track him down and unmask Simp. Harrison then identifies the bodyguard as the triggerman seeking revenge on the Judge. With the crime solved, the boys can finally leave for their summer camp.

==Production== East Side Kids.

After completing the pilot film for the series, producer Sam Katzman was able to convince former Dead End Kids Bobby Jordan and Leo Gorcey to join the series. Katzman also brought in Gorceys younger brother David Gorcey|David, and former Our Gang star and Vaudeville entertainer Ernest Morrison|"Sunshine Sammy" Morrison. Morrison had already known Katzman prior to joining the series.

While this film is technically a sequel to the previous film, some unexplained changes are made (namely the addition of "Muggs", "Scruno", and "Buster").

Most of the cast from the previous film did not return. Bobby Jordan replaced Harris Berger in the role of "Danny", and would retain the role for a large portion of the series run. Jack Edwards was originally slated to return as "Algernon Wilkes", but immediately declined after being offered a part 
in another movie. Eugene Francis took his place.

Hal E. Chester returned, but as his character was killed off in the previous film, he plays a different character here. This would be his last East Side Kids film. 
 Dave OBrien all returned, and each reprised their role from the previous film. This would be Burkes last East Side Kids film. After his departure, the character of "Skinny" was given to Haines, while "Peewee" was given to David Gorcey.

==Cast and characters==
===The East Side Kids===
*Bobby Jordan as Danny Dolan
*Leo Gorcey as Muggs
*Hal E. Chester as Buster
*Frankie Burke as Skinny Sunshine Sammy as Scruno
*Donald Haines as Peewee
*David Gorcey as Pete
*Eugene Francis as Algy Wilkes

===Other cast===
*Vince Barnett as Simp
*Inna Gest as Louise Mason Dave OBrien as Knuckles Dolan
*Minerva Urecal as Agnes Dennis Moore as Giles
*Forrest Taylor as Judge Malcolm Parker
*Alden Stephen Chase as Jim Harrison
*Jerry Mandy as Cook
*George Humbert as Tony

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 