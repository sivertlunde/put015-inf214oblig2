Pepe (film)
{{Infobox film
| name = Pepe
| image = Pepe movie poster.jpg
| caption = movie poster
| director = George Sidney
| producer = George Sidney
| writer = Leslie Bush-Fekete (play Broadway Zauber aka Broadway Magic) Claude Binyon Dorothy Kingsley
| starring =Mario Moreno ("Cantinflas") Dan Dailey Shirley Jones
| cinematography = Joseph MacDonald
| music = Al Clark
| distributor = Columbia Pictures
| released =  
| runtime = 180 minutes 
| country = United States
| language = English
| budget =
| gross = $4.8 million (US/ Canada rentals)  
}}
 Mario Moreno Around the World in Eighty Days, produced by Mike Todd in 1956.

The film failed to achieve the success of Cantinflas previous American film and was roundly criticized by film critics.  A VHS tape of the film was released on December 7, 1998.  

== Plot == Zsa Zsa Gabór, Bing Crosby, Maurice Chevalier and Jack Lemmon in drag as Daphne from Some Like It Hot.  He is also surprised by things that were new in America at the time, such as automatic swinging doors. When he finally reaches the man who bought the horse, he is led to believe there is no hope of getting it back. However, the last scene shows both him and the stallion back at the ranch with several foals.

== Cast ==
* Mario Moreno ("Cantinflas") as Pepe
* Dan Dailey as Ted Holt
* Shirley Jones as Suzie Murphy
* Carlos Montalbán as Rodríguez (auctioneer)
* Vicki Trickett as Lupita
* Matt Mattox as Dancer
* Hank Henry as Manager
* Suzanne Lloyd as Carmen
* William Demarest as Movie Studio Gateman

=== Cameos ===
* Maurice Chevalier
* Bing Crosby
* Richard Conte
* Tony Curtis
* Bobby Darin Schultzy
* Sammy Davis Jr.
* Jimmy Durante Zsa Zsa Gabór
* Judy Garland (voice only)
* Greer Garson
* Hedda Hopper
* Joey Bishop
* Ernie Kovacs
* Peter Lawford
* Janet Leigh
* Jack Lemmon Dennis the Menace
* Kim Novak
* André Previn
* Donna Reed
* Debbie Reynolds
* Edward G. Robinson
* Cesar Romero
* Frank Sinatra
* Dean Martin
* Charles Coburn
* Billie Burke

==Awards==
The film was nominated for seven Academy Awards:       Best Art Direction (Ted Haworth, William Kiernan) Best Cinematography (Joseph MacDonald) Best Costume Design (Edith Head) Film Editing (Viola Lawrence, Al Clark) Best Original Song ("Faraway Part of Town") Best Scoring Best Sound Charles Rice)

==References==
 

== External links ==
*  
*  
*   on the New York Times

 

 
 
 
 
 
 
 
 