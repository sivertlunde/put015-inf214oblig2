The Bite
{{Infobox film
| name           = The Bite
| image          = The Bite.jpg
| image_size     = 
| caption        = Poster to the 1960s US release of The Bite Kan Mukai   
   
| producer       = Teruo Yamoto   
| writer         = Miki Yamada 
| narrator       = 
| starring       = Senjo Ichiriki Michiko Shiroyami
| music          = 
| cinematography = Jiro Suzuki 
| editing        = 
| studio         = Kokuei / Tōkyō Geijutsu Pro 
| distributor    = Olympic International Films (USA 1960s) Cinema Epoch (USA 2008) 
| released       = August 1966   
| runtime        = 62 minutes  
| country        = Japan Japanese English English (film exists only in dubbed version)
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}

  aka Bait (1966) is a Japanese pink film directed by Hiroshi Mukai, credited as "Kan Mukai" in the international English-dubbed version, and starring Senjo Ichiriki and Michiko Shiroyami.

==Synopsis==
A gigolo is paid by a rich older woman to seduce young girls while she and friends watch his love-making behind a two-way mirror. He tries to break free of the older womans control, but when his sick mother needs an operation, he returns to the rich woman, making love to her for money. Humiliated by his dependency, he takes the womans daughter to the love-show, where he rapes her in front of her mother.    

==Cast==
* Michiko Shiroyami 
* Senjo Ichiriki
* Machiko Matsumoto
* Keisuke Senda
* Natsue Hanaha

==Availability==
Olympic International Films released The Bite to the U.S. grindhouse circuit soon after its original release in Japan.   The film was released in Britain in 1967 as Bait.    

Since its first run, The Bite had been considered a lost film.    Jasper Sharp notes that had it not been for the overeas release of films like The Bite, these films would likely not be available today, as many early pink films were not saved in Japan.  The film was rediscovered and released in an English-dubbed version on DVD region code|region-0 DVD by Gregory Hatanakas Cinema Epoch label on May 13, 2008.  Preceding the DVD release, Portland, Oregons Clinton Street Theater played The Bite on a double feature with Mamoru Watanabes Slave Widow (1967) on May 9 and May 11, 2008. 
 

==References==
 

==Bibliography==
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 


 
 