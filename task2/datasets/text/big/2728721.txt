Tanguy (film)
{{multiple issues|
 
 
}}
Tanguy is a 2001 French black comedy by Étienne Chatiliez.

== Plot ==
When he was a newborn baby, Edith Guetz thoughtlessly told her son Tanguy : "If you want to, you can stay at home forever. 28 years later, the over-educated university teacher of Asian languages and womanizer leads a successful and wealthy life... while still living in his parents home. Father Paul Guetz longs to see his son finally leave the nest, a desire that his wife does not share. Edith finally agrees and the pair unite to make home Tanguys life miserable. But they dont know that Tanguy isnt the type of guy who easily gives up.

==Cast==
* Sabine Azéma: Edith Guetz
* André Dussollier: Paul Guetz
* Éric Berger: Tanguy Guetz
* Hélène Duc: Grandmother Odile
* Aurore Clément: Carole
* Jean-Paul Rouve: Bruno Lemoine
* André Wilms: the psychiatrist
* Richard Guedj: Patrick
* Roger Van Hool: Philippe
* Nathalie Krebs: Noëlle
* Delphine Serina: Sophie
* Sachi Kawamata: Kimiko
* Annelise Hesme: Marguerite
* Jacques Boudet: the judge
* Philippe Laudenbach: Badinier (the lawyer)

==In popular culture==
The word Tanguy became the usual term to designate an adult still living with his parents.

== External links ==
* 

 
 
 
 
 


 
 