A Symposium on Popular Songs
{{Infobox film
| name           = A Symposium on Popular Songs
| image          = 
| caption        = 
| director       = Bill Justice
| producer       = Walt Disney
| writer         = Xavier Atencio
| screenplay     = 
| story          = 
| based on       =  
| starring       = Paul Frees Gloria Wood Billy Storm Skip Farrell
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Academy Award Best Animated The Wonderful World of Color, Disneys NBC Sunday evening anthology series. It was released on DVD in 2005 as part of the Walt Disney Treasures set Disney Rarities.

==Songs==

===The Rutabaga Rag===
"The Rutabaga Rag", performed by Paul Frees as Ludwig Von Drake, was not written as a parody of ragtime, but rather as an authentic ragtime song. Disney Rarities - Audio Commentary for A Symposium on Popular Songs with Leonard Maltin and Richard M. Sherman, 2005  In the course of the films narration, Von Drake claims to have invented ragtime music and, specifically, this song.

===Charleston Charlie=== flapper era iconic song "Hes So Unusual", which was co-written by the Sherman Brothers tin pan alley songwriting father, Al Sherman in 1929 (see 1929 in music). The subject of both songs is a male college student whom the singer desires. "Charleston Charlie" begins with the Betty Boop-esque lyric "Boop boop be doop".

In the film, Ludwig Von Drake claims he wrote the song when traveling below the Mason–Dixon line. "Mr. Dixon" approached Drake and asked him to put "Dixie" on the map. For this reason, he wrote a song originally entitled, "Louisville Ludwig", but later changed the name to "Charleston Charlie" in order to protect the innocent, namely himself.

===Although I Dropped $100,000===
Also known as "Although I Dropped a Hundred Thousand in the Market, Baby (I Found a Million Dollars in Your Smile)", and performed by Frees, this song makes a subtle reference to the singing style exemplified by  .  Chapter 1; "Als Time", Pages 17, 231.  Santa Clarita: Camphor Tree Publishers, 1998 

According to film critic, Leonard Maltin, this song as well as "Charleston Charlie" are homages to Al Sherman and his songs.  In the context of the film, Ludwig Von Drake claims he wrote the song and it became all the rage at the beginning of the Great Depression.

===Im Blue for You, Boo-Boo-Boo-Boo-Boo=== crooning style.  Crooning elements, such as the repetition of "Boo boo boo boo boo", the whistling of the melody, and over-rhyming of the word "heart", are placed throughout the song. 

The Sherman Brothers father, Al Sherman, wrote several songs which were sung by Bing Crosby in the 1930s and 1940s.  In 1970, Robert & Richard Sherman had a chance, in their own right, to work with the legendary Crosby on the made-for-television musical production of Goldilocks (short film)|Goldilocks.

===The Boogie Woogie Bakery Man===
"The Boogie Woogie Bakery Man", performed by Betty Allan, Diane Pendleton and Gloria Wood, had a structure and arrangement closely styled after the Andrews Sisters hit song, "Boogie Woogie Bugle Boy". The song makes direct and indirect references to the singing style exemplified by the Andrews Sisters, as well as numerous songs from the swing era which was the heyday of their career.  The very first line of the song references several swing era songs:
 

The song itself is about an "oriental" baker of fortune cookies. At the time the song was written, the use of the term "oriental" was quite common. However, in recent decades, the term has increasingly been seen to be offensive when used to describe an individual from the far east.

In 1974, twelve years after A Symposium on Popular Songs was first released, the Sherman Brothers worked with the Andrews Sisters on the Tony Award winning show, Over Here!, which was also an homage to the 1940s swing era music of the day.

===Puppy Love Is Here to Stay=== Blue Moon" Puppy Love". 

===Rock, Rumble and Roar===
"Rock, Rumble and Roar" is the final song from the film and sung by Paul Frees, Gloria Wood, Skip Farrell, Betty Allan, and Diane Pendleton. The song is meant to be the most modern of the songs from the film, and also revisits the six previous songs. This song is a homage to the popular, early rock and roll song, "Shake, Rattle and Roll".  It is the second song in the featurette to be sung by Ludwig Von Drake.

==References==
 

==External links==
*  at  
* 
 

 
 
 
 
 
 
 
 
 