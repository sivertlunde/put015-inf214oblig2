Cela s'appelle l'aurore
{{Infobox film
| name        = Cela sappelle laurore
| image       = Cela sappelle laurore poster.jpg
| caption     = Theatrical poster
| writer      = Jean Ferry Luis Buñuel Based on a novel by Emmanuel Roblès
| starring    = Georges Marchal Lucia Bosé Julien Bertheau Giani Esposito
| director    = Luis Buñuel
| producer    = Edmond Ténoudji Claude Jaeger
| movie_music = Joseph Kosma
| distributor = Les Films Marceau  Laetitia Film
| released    = 9 May 1956 (France)
| runtime     = 102 min. French
| music       =
| awards      =
| budget      =
}}

Cela sappelle laurore (English: This is Called Dawn) is a 1956 Franco-Italian film, directed by Luis Buñuel. It was written by Buñuel and Jean Ferry, based on a novel by Emmanuel Roblès.

==Synopsis==
In a small town in the south of France, Dr. Valerio is committed to caring for the poor. His young wife, Angela, cannot stand the place and wants to move to Nice, but the doctor does not want to leave before finding a replacement. Valerio is sympathetic to the working poor of the area, particularly Sandro, a farm worker who maintains the trees belonging to Gorzone, a rich industrialist and the primary employer in the town.  Angela becomes ill and wanders the towns slum quarters in a delirium, ultimately leaving the town for a holiday without her husband. The Commissioner of Police takes Valerio to a mountain village in order to sign medical reports for a little girl who had been raped by her grandfather.  There, Valerio meets Clara, a rich young widow.  He considers a menage a trois, but is rebuffed.  Meanwhile, Sandro is dismissed by Gorzone, forcing Sandro and his tubercular wife, Magda, to leave their home, a move that results in Magdas death.  Distracted by grief, Sandro shoots Gorzone.  Valerio hides Sandro from the police, a decision that results in Angela leaving the doctor for good.  Sandro leaves Valerios protection and, when cornered by the authorities, shoots himself.  Valerio refuses to shake hands with the Commissioner, and the last scene shows Valerio and Clara walking away at night, arm in arm on the quayside, followed by some of Sandros cronies.

==Political theme== La Mort en ce jardin and La fièvre monte à El Pao: "Each of these films is, openly, or by implication, a study in the morality and tactics of armed revolution against a right-wing dictatorship."   

==Cast==
{| class="wikitable"
|- Georges Marchal||Doctor Valerio
|- Lucia Bosé||Clara
|- Julien Bertheau||Commissioner Fasaro
|- Nelly Borgeaud||Angela
|- Giani Esposito||Sandro Galli
|- Brigitte Elloy||Magda
|- Gorzone
|- Simone Paris||Mrs. Gorzone
|- Gaston Modot ||Sandros replacement
|}

==References==
 

==External links==
*   at the Internet Movie Database

 

 
 