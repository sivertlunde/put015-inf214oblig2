Chinnadana Nee Kosam
{{Infobox film
| name           = Chinnadana Nee Kosam
| image          = Chinnadana Nee Kosam poster.jpg
| caption        = Film poster
| director       = A. Karunakaran
| writer         = A. Karunakaran, Mukund Pandey (Script doctor), Harsha Vardhan (Dialogues)
| producer       = N. Sudhakar Reddy, Nikita Reddy
| starring       = Nithiin, Mishti
| music          = Anoop Rubens
| cinematography = I. Andrew
| editing        = Prawin Pudi
| distributor    = Asian Movies & CineGalaxy Inc., (Overseas)   
| studio         = Sresht Movies
| released       =  
| language       = Telugu
| budget         =   
| share          =   
}} romantic comedy film written and directed by A. Karunakaran. The film features Nithiin and Mishti in the lead roles marking the latters debut in Telugu cinema. The film was produced by Nithiins father N. Sudhakar Reddy and sister Nikhita Reddy under the banner Sresht Movies. Harsha Vardhan wrote the films dialogues. Anoop Rubens composed the films soundtrack and background score. I. Andrew and Prawin Pudi were the films cinematographer and editor respectively.

Production began on 21 May 2014. Principal photography began on 2 June 2014. The film opened to mixed reviews.

== Cast ==
*Nithiin as Nithiin
*Mishti as Nandini Reddy
*Nassar Naresh
*Sithara Sithara
*Ali Ali
*Madhunanda

== Production == Heart Attack Telugu debut. She said that the film will be a romantic entertainer with a thriller element in it.  The film had its official launch on 21 May 2014 at Hyderabad, India|Hyderabad.  I. Andrew was signed as the cinematographer, Anoop Rubens was signed as the music director and Harsha Vardhan penned the films dialogues. 

Principal photography began on 2 June 2014 at Hyderabad.  The first schedule came to an end on 12 June 2014.  On 31 July 2014, the films second schedule was wrapped up with which 50% of the films shoot was complete.  The films title was announced as Chinnadana Nee Kosam on 5 August 2014 which happens to be the name of a song from the film Ishq (2012 film)|Ishq (2012).  In early September 2014, the films unit traveled to Barcelona for a 35 days schedule where crucial scenes and songs on the lead pair were planned to be shot.  The film was predominantly shot in Switzerland and the films unit returned to Hyderabad nearly on 11 October 2014.  The first look poster along with a teaser was unveiled on 24 October 2014 on the occasion of Diwali.  
 dubbing works were carried out simultaneously.  On 27 November 2014, Nithiins father Sudhakar Reddy stated that the filming has been completed and post-production activities began.    The theatrical trailer was unveiled on 28 November 2014.  Pawan Kalyan was reported to make a cameo appearance in the film which both Karunakaran and Nithiin denied those reports as rumors but added that Nithiin will be seen as Pawan Kalyans fan in the film.   Mishti described her character as a witty and intelligent girl named Nandini and said that her co-star Dhanya Balakrishna and the assistant directors helped her in pronouncing Telugu correctly. 

== Music ==
{{Infobox album
| Name       = Chinnadana Nee Kosam
| Longtype   = To Chinnadana Nee Kosam
| Type       = Soundtrack
| Artist     = Anoop Rubens
| Cover      = Chinnadana Nee Kosam soundtrack.jpg
| Released   = 27 November 2014
| Recorded   = 2014 Feature film soundtrack
| Length     = 22:50 Telugu
| Label      = Aditya Music
| Producer   = Anoop Rubens
| Last album = Pilla Nuvvu Leni Jeevitham (2014)
| This album = Chinnadana Nee Kosam (2014)
| Next album = Temper (film)|Temper  (2015)
}}
 Krishna Chaitanya and composed by Rubens. The release date was confirmed as 27 November 2014 by Nithiin.  Aditya Music acquired the audio rights.  Akkineni Nagarjuna was invited as the chief guest for the audio launch event held at Shilpakala Vedika. 
 Tirupati on 19 December 2014 when Nithiin visited the Sri Venkateswara Swamy temple to pray for the films success. 

{{tracklist
| headline        = 
| extra_column    = Artist(s)
| total_length    = 22:50 Krishna Chaitanya
| title1          = Chinnadana Nee Kosam
| extra1          = Raja Hasan
| length1         = 3:59
| title2          = Ooh La La
| extra2          = Anoop Rubens
| length2         = 3:54
| title3          = Everybody Chalo All I Wanna Say
| extra3          = Jaspreet Jasz, Anudeep, Dhanunjay, Hymath
| length3         = 3:19
| title4          = Dil Dil Dil
| extra4          = Rahul Sipligunj
| length4         = 4:02
| title5          = Mundhugane
| extra5          = Ramya Behra
| length5         = 3:29
| title6          = Albeli
| extra6          = Simha, Jhanvi Narang
| length6         = 4:07
}}

== Release ==
The film was initially planned for a release on 19 December 2014.  Later it was postponed to 25 December 2014 as a Christmas release.  Asian Movies & CineGalaxy Inc., acquired the overseas theatrical rights. 

==References==
 

 

 
 
 
 