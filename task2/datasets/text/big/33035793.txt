Erstwhile Susan
{{infobox film
| name           = Erstwhile Susan
| image          =Erstwhile Susan (1919) - 3.jpg
| imagesize      =200px
| caption        =film still
| director       = John S. Robertson  Shaw Lovett (assistant director)
| producer       = Realart Pictures
| based on       =  
| writer         = Kathryn Stuart (writer)
| starring       = Constance Binney Mary Alden
| music          =
| cinematography = Roy Overbaugh
| editing        =
| distributor    = Realart Pictures
| released       =   reels (1639.02 meters)
| country        = United States Silent (English intertitles)
}}
Erstwhile Susan is a 1919 American silent drama film directed by John S. Robertson, produced and distributed by Realart Pictures. It is based on a 1914 novel Barnabetta by Helen Reimensnyder Martin and later Broadway play Erstwhile Susan by Marian De Forest. Minnie Maddern Fiske starred on Broadway in 1916. This film version stars Mary Alden and Constance Binney, then an up-and-coming young actress. This film version, once thought to be lost film|lost, survives at the Museum of Modern Art.   

First film by Realart Films, Adolph Zukors offshoot affiliate of his Famous Players-Lasky enterprise.

==Plot==
As described in an adoption in the November 1919 issue of the film magazine Shadowland, {{cite book
 | last =Ward
 | first =Jane
 | authorlink =
 | title =Erstwhile Susan
 | publisher =M. P. Publishing Co.
| journal =Shadowland
 | volume =1
 | issue =3
 | date =November 1919
 | location =New York
 | pages =35–38, 67, 72
 | url =https://archive.org/details/shadowland01mppu}}  Barnabetta (Constance Binney) dreams of furthering her education, but her Mennonite father Jacob (Bradley Barker) disapproves. Jacob later marries Erstwhile Susan (Mary Alden), who has money and changes the family relationships, and sends Barnabetta to college. After graduation, she helps David Jordan (Jere Austin) run for the Senate, who then professes his love for her.

==Cast==
*Constance Binney - Barnabetta Dreary
*Jere Austin - David Jordan
*Alfred Hickman - Dr. Edgar Barrett
*Mary Alden - Erstwhile Susan
*Anders Randolf - Barnaby Dreary
*Georges Renavent - Emanuel Dreary
*Bradley Barker - Jacob Dreary
*Leslie Hunt - Albert Buchter
*Clare Verdera -   ?  (*uncredited)

==Preservation== lost save First Love (1921). 

==References==
 

==External links==
 
*  
*  

 

 
 
 
 
 
 
 
 
 
 


 