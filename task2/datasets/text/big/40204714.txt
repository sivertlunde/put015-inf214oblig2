Those Happy Years
 
{{Infobox film
| name           = Those Happy Years (Anni felici)
| image          = Those Happy Years.jpg
| caption        = Film poster
| director       = Daniele Luchetti
| producer       = 
| writer         = Daniele Luchetti, Sandro Petraglia, Stefano Rulli, Caterina Venturini
| starring       = Kim Rossi Stuart, Micaela Ramazzotti
| music          = Franco Piersanti
| cinematography = Claudio Collepiccolo
| editing        = Mirco Garrone
| distributor    = Cattleya
| released       =  
| runtime        = 100 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}

Those Happy Years ( ) is a 2013 Italian drama film directed by Daniele Luchetti. It was screened in the Special Presentation section at the 2013 Toronto International Film Festival.   

==Cast==
* Kim Rossi Stuart as Guido
* Micaela Ramazzotti as Serena
* Martina Gedeck as Helke
* Samuel Garofalo as Dario
* Niccolò Calvagna as Paolo
* Benedetta Buccellato as Nonna Marcella
* Pia Engleberth as Nonna Marina
* Angelique Cavallari as Michelle

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 