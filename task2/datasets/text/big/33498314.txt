Le Pont du Nord
{{Infobox film
| name           = Le Pont du Nord
| image          =
| caption        =
| director       = Jacques Rivette
| producer       = Barbet Schroeder Martine Marignac Margaret Ménégoz Jean-Pierre Mahot	
| writer         = Bulle Ogier Pascale Ogier Suzanne Schiffman Jacques Rivette Jérôme Prieur 
| starring       = Bulle Ogier Pascale Ogier
| music          = 
| cinematography = William Lubtchansky Caroline Champetier
| editing        = Nicole Lubtchansky Catherine Quesemand   		 	
| distributor    = 
| released       =USA 7 October 1981 France 24 March 1982
| runtime        = 129 minutes
| country        = France
| language       = French
| budget         =
| gross          =
}} Paris Goes Away was made as a rehearsal in preparation for this film.

==Plot==
Le Pont du Nord stars Bulle and Pascale Ogier as two women who randomly meet and investigate a strange and surreal mystery together involving a strange package and several characters all named Max.

==Cast==
*Bulle Ogier as Marie 
*Pascale Ogier as Baptiste 
*Pierre Clémenti as Julien 
*Jean-François Stévenin as Max 
*Benjamin Baltimore as Le Max au couteau 
*Steve Baës as Le Max au manteau 
*Joe Dann as Le joueur de bonneteau 
*Mathieu Schiffman as Un Hongrois 
*Antoine Gurevitch as Trois garçons 
*Julien Lidsky as Trois garçons 
*Marc Truscelli as Trois garçons

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 

 