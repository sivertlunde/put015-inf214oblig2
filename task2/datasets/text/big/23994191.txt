Plots with a View
{{Infobox film
| name           = Plots with a View (Undertaking Betty)
| image          = Undertaking Betty.jpg
| caption        = International theatrical poster
| director       = Nick Hurran
| producer       = Michael Cowan Suzanne Lyons Jason Piette Kate Robbins
| writer         = Frederick Ponzlov Lee Evans Christopher Walken
| music          = Rupert Gregson-Williams
| cinematography = Kevin Rudge James Welland John Richards
| studio         = Great British Films Snowfall Films Spice Factory
| distributor    = Miramax
| released       = 2002
| runtime        = 88 minutes
| country        = United Kingdom United States Germany
| language       = English
| budget         = 
| gross          = 
}} Lee Evans and Christopher Walken. The film began filming in Caldicot, Monmouthshire, Wales, UK in 2002, and was released in the U.S. on November 12, 2005, with a DVD release on March 7, 2006.

==Plot==
Boris Plots,is director of Plots Funeral Home in the fictional Welsh village of Wrottin Powys, His rival Frank Featherbed, an American, is determined to revolutionise the undertaking business in Britain through the innovation of "themed funerals".
Boris Plots dreamed of only two things as a young boy: dancing and Betty Rhys-Jones. Betty secretly loved Boris, but could not fight her fathers wishes, so she was married off to a gold digger. Boris gave up his dreams and took over the familys undertaking business.When Bettys mother-in-law dies, Boris and Betty are thrust together again and as they discuss the funeral arrangements for Bettys mother-in-law, the old spark is rekindled.Boris discovers that the only obstacle between their love for one another, is her marriage to the adulterous Councilor Hugh Rhys-Jones. In a desperate bid for happiness Boris and Betty decide to stage her death and run away together.

==Cast==
* Brenda Blethyn as Betty Rhys-Jones
* Robert Pugh as Hugh Rhys-Jones
* Alfred Molina as Boris Plots
* Naomi Watts as Meredith Mainwaring Lee Evans as Delbert Butterfield
* Christopher Walken as Frank Featherbed

==Production==
The movie was filmed on location in Monmouthshire, Wales, South Glamorgan, Wales, Vale of Glamorgan, Wales, Rhondda Cynon Taff, Wales, and Denbighshire, Wales.

The dance sequences were choreographed by Peter Darling and the costumes were designed by Ffion Elinor.

==Reception==
The 2002 Variety (magazine)|Variety review noted its "near-miraculous balance between the silly and the morbid" and referred to it as "enjoyable and entertainingly cast" even though it was "unlikely to bury the competition".  In 2005, The Los Angeles Times described it as succeeding in sustaining its "deliberate silliness" though it might work better for home viewing than in theatres, and described its cast as "first rate."  DVD Talk was less enthusiastic: "though kind of a mess, and not really all that funny, Undertaking Betty still feels barely recommendable, thanks to a colorful cast of true characters."  Reel Film gave it 3 of 4 stars, and described it as "charming, low-key romantic comedy", and though theres "nothing especially groundbreaking or even memorable about Undertaking Betty, but the undeniably sweet vibe is ultimately quite difficult to resist." 

===Recognition===
The film was nominated for a Golden Hitchcock at the 2002 Dinard British Film Festival, and won a Cymru Award at the 2003 BAFTA Awards, Wales.

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 