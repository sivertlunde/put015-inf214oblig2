Mutt Boy
 
{{Infobox film name           = Mutt Boy  image          = File:Mutt_Boy_poster.jpg
| film name = {{Film name hangul         =    rr             = Ddong gae mr             = ttonggae}} director       = Kwak Kyung-taek  producer       = Jung Jong-seob writer         = Kwak Kyung-taek Kim Jang-woo  starring       = Jung Woo-sung Kim Kap-soo Uhm Ji-won music          =  Yun Min-hwa cinematography = Hwang Ki-seok editing        = Park Yu-kyeong distributor    = Show East released       =   runtime        = 103 minutes country        = South Korea language       = Korean budget         =  gross          = 
}}
Mutt Boy ( ; lit. "Shit dog") is a 2003 South Korean film starring Jung Woo-sung, Kim Kap-soo and Uhm Ji-won. A developmentally-disabled boy grows up in a rural town, and eventually attracts a small gang of followers due to his fighting skills. However, he keeps getting into trouble with the law, and getting picked up by his father, who works as a police officer.

==Plot==
Chul-mins family consists of himself, his police detective father, his sick mother, and a dog. Chul-min calls the dog "Mutt" or "Stray Dog," but his father calls Chul-min "Mutt" too. The dog is Chul-mins best friend. He doesnt cry when his mom dies, but he does get furious when his dog is eaten by the older boys in his soccer club. He takes his revenge, but his fathers patrol car awaits him. Although he now must live on his own, Chul-min wins renown as the one who beat up 21 people all by himself. Many of the towns thugs start to hang around Chul-min, fascinated by him, and he becomes a leader of small-time gangsters. One day his father takes in a young orphan named Jung-ae who was a thief. Eventually Chul-min and Jung-ae develop feelings each other and Chul-min confronts the gang who killed his beloved dog.

==Cast==
*Jung Woo-sung ... Cha Chul-min
*Uhm Ji-won ... Kim Jung-ae
*Kim Kap-soo ... Chul-mins father
*Kim Jung-tae ... Jin-mook
*Hong Ji-young ... Soon-ja
*Yang Joon-kyung ... Oh Deok-man
*Lee Soo-ho ... Dae-deok Kim Sang-ho ... Jang-son
*Park Sang-gyu ... Detective Park
*Heo Wook ... Gye-hwan

==External links==
*    
*  
*  
*  

 
 
 
 
 
 
 
 


 
 