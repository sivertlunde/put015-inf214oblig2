Ripa Hits the Skids
{{Infobox film
| name           = Ripa Hits the Skids
| image          = 
| caption        = 
| director       = Christian Lindblad
| producer       = 
| writer         = Christian Lindblad
| starring       = Sam Huber
| cinematography = Ilkka Ruuhijärvi
| editing        = 
| distributor    = 
| released       =  
| runtime        = 80 minutes
| country        = Finland
| language       = Finnish
| budget         = 
}}

Ripa Hits the Skids ( ) is a 1993 Finnish comedy film directed by Christian Lindblad. It was entered into the 18th Moscow International Film Festival.   

==Cast==
* Sam Huber as Ripa
* Mari Vainio as Tiina
* Merja Larivaara as Pirjo
* Leena Uotila as Irma
* Leo Raivio as Antti
* Kari Väänänen as Lynkkynen
* Jussi Lampi as Lindgren
* Vesa-Matti Loiri as Father
* Christian Lindblad as Keränen
* Minna Pirilä as Beauty
* Jukka Pitkänen as Jaatinen

==References==
 

==External links==
*  

 
 
 
 
 


 
 