The Little Hero of Holland
 
 
{{Infobox film
| name           = The Little Hero of Holland
| image          = New York Dramatic Mirror June1910.jpg
| caption        = Advertisement for the film in The New York Dramatic Mirror
| director       = 
| producer       = Thanhouser Company
| writer         = 
| narrator       =
| starring       = Marie Eline
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        =
| country        = United States English inter-titles
}} silent short short drama dike with his finger to prevent it from bursting. The role of the boy was played by Marie Eline, but little else is known about the production and cast of the film. The director may have been Barry ONeil and the writer may have been Lloyd Lonergan. Parts of the film were shot on Glen Park Island in New Rochelle, New York. The film was released on June 17, 1910 and saw a wide release, including the United Kingdom. The film is presumed lost film|lost.

== Plot ==
Though the film is presumed lost film|lost, a synopsis of the film was published in The Moving Picture World on June 18, 1910. It states: "Hans is a little Dutch boy, the son of a poor fisherman. He, like all children of Holland, is early impressed with the fact that the safety of the whole community depends on the strength of the dike. He is sent upon an errand to his aunt late one evening. Walking along the foot of the dike, he discovers a tiny hole, through which the water is pouring. Realizing that if left to flow through even this small aperture, the water would soon break down the dike and flood the village, he sticks his tiny finger in the hole, and waits for someone to come to his assistance. Night comes on. His family decided he has stayed with his aunt, and do not worry about him. In the morning he is found almost unconscious from fatigue and exposure, by his little sister who has been searching for him. She runs back and notifies the villagers, who come to his assistance, repair the dike, and carry the little hero back to the village in triumph. He is received by the burgomaster and praised by the minister. By his nights heroism he has made himself a place in history, and will always be known as the little hero of Holland."   

== Production ==
The writer of the scenario is unknown, but it was most likely Lloyd Lonergan. Lonergan was an experienced newspaperman employed by The New York Evening World while writing scripts for the Thanhouser productions. He was the most important script writer for Thanhouser, averaging 200 scripts a year from 1910 to 1915.  The film was an adaptation of the fictional story that was popularized in Hans Brinker, or The Silver Skates. The name of the boy, Hans, may be a reference to the erroneous association to unnamed character in the book.  The film director is unknown, but it may have been Barry ONeil. Film historian Q. David Bowers does not attribute a cameraman for this production, but two possible candidates exist. Blair Smith was the first cameraman of the Thanhouser company, but he was soon joined by Carl Louis Gregory who had years of experience as a still and motion picture photographer. The role of the cameraman was uncredited in 1910 productions.    Many of the scenes for the film were shot at Glen Park in New Rochelle, New York. 

Only Marie Elines credit as Hans, the young boy, is credited.  Eline had previously played an Italian boy in The Two Roses.    The other members cast may have included the leading players of the Thanhouser productions, Anna Rosemond and Frank H. Crane. Rosemond was one of two leading ladies for the first year of the company.  Crane was also involved in the very beginnings of the Thanhouser Company and acted in numerous productions before becoming a director at Thanhouser. 

== Release ==
The single reel drama, approximate 650 feet long, was released on June 17, 1910.  Some trade publications list the length of the film as 1000 feet though.  The United Kingdom release of the film may have had the name of the hero changed to Peter. A synopsis in The Bioscope on October 27, 1910 refers to the hero by the name Peter.  This alternative name for the hero of the story can be found in the 1910 publication of Reader 1st-4th by D. Appleton and Company.  A review in The Moving Picture World was neutral because it lacked specific praise or criticism of the production.  The film had a wide release, advertisements for the showing in theaters include those in Pennsylvania,   Oklahoma,  Indiana,  and Missouri.  The film is presumed lost film|lost.

== References ==
 

 
 
 
 
 
 
 
 
 