Trippin' (film)
 
{{Infobox Film
| name           = Trippin
| image          = Trippin dvd cover.jpg
| image_size     = 
| caption        = 
| director       = David Raynr
| producer       = Marc Abraham Caitlin Scanlon Thomas Bliss
| writer         = Gary Hardwick 
| starring       = Deon Richmond Countess Vaughn Maia Campbell Donald Faison Guy Torry
| music          = Michel Colombier
| cinematography = John B. Aronson
| editing        = Earl Watson
| studio         = Beacon Pictures
| distributor    = Rogue Pictures
| released       = May 12, 1999
| runtime        = 94 min
| country        = United States
| language       = English
| budget         = 
| gross          = $9,017,070
}}

Trippin is a 1999 comedy film starring Deon Richmond, Maia Campbell, Donald Faison, and Guy Torry. The film provided one of Anthony Andersons earliest film roles.  It was directed by David Raynr.

==Plot==
Greg (Deon Richmond) is nearing the end of his high school days as graduation slowly approaches.  He is also anxiously awaiting prom and has the hopes of going with Cinny (Maia Campbell), the schools local beauty.  Along with these wants, Greg is also an avid daydreamer and daydreams (trippin) over everything. Most of his "trips" are reversals of real world events, such as visualizing himself as a military commando when confronted by bullies, or as a super genius when in fact he struggles academically. Since he is about to graduate his mother and a teacher encourage him to apply for college.  He finally realizes asking for help with the college applications is a great way for him to get in good with Cinny.  So they slowly start a friendship but Greg wants it to blossom into a romance so he begins to lie about things. Things go smoothly and are believable until they fall apart one day.  Afterward, Greg realizes he needs to stop daydreaming ("trippin") and focus.

==Production notes== Long Beach Harbor City in Los Angeles was also a location used for filming as some of the films scenes take place on the  .  The title of the film, while it was in production, was Gs Trippin that was later shortened to its current title before release. 

==Reception==
Trippin was poorly received by critics, and currently holds an 18% "rotten" rating on the review aggregated site Rotten Tomatoes based on 28 reviews. The film made $2,527,909 its opening weekend and grossed a total of $9,017,070 during its theatrical run. 

==Releases== The Best Man, and How to Be a Player.  The sole DVD was released with a different cover art featuring both Deon Richmond and Donald Faison.

==References==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 
 
 