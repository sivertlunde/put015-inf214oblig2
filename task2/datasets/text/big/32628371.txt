List of horror films of 1977
 
 
A list of horror films released in 1977 in film|1977.

{| class="wikitable sortable" 1977
|-
! scope="col" | Title
! scope="col" | Director
! scope="col" class="unsortable"| Cast
! scope="col" | Country
! scope="col" class="unsortable"| Notes
|- Audrey Rose John Beck ||   ||  
|-
!   | The Car
| Elliot Silverstein || James Brolin, Kathleen Lloyd, John Marley ||   ||  
|-
!   | The Child
| Robert Voskanian || Laurel Barnett, Rosalie Cole, Frank Janson ||   ||  {{hidden|multiline=y|fw1=normal
|1=Alternative titles
|2=Also known as Kill and Go Hide , Zombie Child}}
|- Count Dracula Mark Burns ||   || Television film 
|-
!   | Day of the Animals
| William Girdler || Leslie Nielsen, Lynda Day George, Jon Cedar ||   ||  
|-
!   |   George Barry || Linda Bond, Demene Hall, Julie Ritter ||   ||  
|-
!   | The Demon Lover
| Donald G. Jackson, Jerry Younkins || Val Mayerik, Gunnar Hansen, Carol Lasowski ||   ||  
|-
!   | Demon Seed
| Donald Cammell || Julie Christie, Fritz Weaver, Gerritt Graham ||   ||  
|- Empire of the Ants
| Bert I. Gordon || Joan Collins, Robert Lansing, John David Carson ||   ||  {{hidden|multiline=y|fw1=normal
|1=Alternative titles
|2=Also known as H.G. Wells Empire of the Ants (USA) (complete title).}}
|-
!   |  
| John Boorman || Linda Blair, Richard Burton, Louise Fletcher ||   ||  
|- Full Circle
| Richard Loncraine || Anna Wing, Mia Farrow, Sophie Ward ||    ||  
|-
!   | Good Against Evil
| Paul Wendkos || Kim Cattrall, Elyssa Davalos ||   || Television film 
|-
!   | Haunts Cameron Mitchell, Aldo Ray ||   ||  {{hidden|multiline=y|fw1=normal
|1=Alternative titles
|2=Also known as The Veil.}}
|- The Hills Have Eyes Robert Houston, Virginia Vincent ||   ||  
|-
!   | House (1977 film)|House
| Nobuhiko Obayashi || Kimiko Ikegami, Kumiko Ohba, Yôko Minamida ||   ||  
|-
!   | It Happened at Lakewood Manor
| Robert Scheerer || Suzanne Somers, Lynda Day George, Myrna Loy ||   ||  {{hidden|multiline=y|fw1=normal
|1=Alternative titles
|2=Also known as Ants! and Panic at Lakewood Manor}}
|-
!   | Kingdom of the Spiders
| John Cardos || William Shatner, Tiffany Bolling, Woody Strode ||   ||  
|-
!   | Last Cannibal World
| Ruggero Deodato || Massimo Foschi, Ivan Rassimov ||   ||  {{hidden|multiline=y|fw1=normal
|1=Alternative titles
|2=Also known as Cannibal, Carnivorous, The Last Survivor and Jungle Holocaust}}
|-
!   | Lisa, Lisa (film)|Lisa, Lisa
| Frederick R. Friedel || Leslie Lee, Jack Canon, Ray Green ||   ||  {{hidden|multiline=y|fw1=normal
|1=Alternative titles
|2=Also known as Axe (USA) (video box title) , California Axe Massacre , California Axe Murders , The Axe Murders , The Virgin Slaughter (USA)}}
|-
!   |   Michael Anderson || Richard Harris, Charlotte Rampling ||   ||  
|- The Pack
| Robert Clouse || Joe Don Baker, Hope Alexander-Willis, R.G. Armstrong ||   ||  
|- The Possessed
| Jerry Thorpe || James Farentino, Joan Hackett, Harrison Ford, Claudette Nevins ||   || Television film 
|-
!   | Rabid Frank Moore ||   ||  
|-
!   | Ruby (1977 film)|Ruby Roger Davis ||   ||  
|-
!   | Satans Cheerleaders
| Greydon Clark || Yvonne De Carlo, Robin Greer, John Carradine ||   ||  
|- The Sentinel
| Michael Winner || Chris Sarandon, Cristina Raines, Martin Balsam ||   ||  
|-
!   | Sette note in nero
| Lucio Fulci || Jennifer ONeill, Gabriele Ferzetti, Marc Porel ||   || {{hidden|multiline=y|fw1=normal
|1=Alternative title
|2=Also known as The Psychic, "Murder to the Tune of the Seven Black Notes".}} 
|-
!   | Shock (1977 film)|Shock
| Mario Bava || David Colin, Jr., Daria Nicolodi ||   ||  {{hidden|multiline=y|fw1=normal
|1=Alternative titles
|2=Also known as Beyond the Door II and Suspense}}
|- Shock Waves Brooke Adams, Fred Buch ||   ||  {{hidden|multiline=y|fw1=normal
|1=Alternative titles
|2=Also known as Almost Human (UK) ,Death Corps}}
|-
!   | Suspiria
| Dario Argento || Jessica Harper, Stefania Casini, Joan Bennett ||   ||  
|-
!   | Tentacles (film)|Tentacles
| Ovidio G. Assonitis || John Huston, Shelley Winters, Bo Hopkins ||  ,   ||  {{hidden|multiline=y|fw1=normal
|1=Alternative titles
|2=Also known as Tentacoli}}
|- The Uncanny
| Denis Heroux || Catherine Begin ||    ||  
|-
!   | Watch me when I Kill
| Antonio Bido || Franco Citti, Fernando Cerulli, Sylvia Kramer ||   ||  
|}

==References==
 

==Citations==
 
*  <!--
-->
 

 
 
 

 
 
 
 