Alias the Deacon
{{infobox_film
| name           = Alias the Deacon
| image          =
| imagesize      =
| caption        =
| director       = Edward Sloman
| producer       = Carl Laemmle
| writer         = John B. Hymer(play) Leroy Clemens(play) Charles Kenyon(scenario) Walter Anthony(scenario, intertitles)
| starring       = Jean Hersholt
| cinematography = Jackson Rose Gilbert Warrenton
| editing        = Byron Robinson
| distributor    = Universal Pictures
| released       =   reels (2,093.67 meters)
| country        = United States
| language       = Silent film (English intertitles)
}}
Alias the Deacon is a 1928 silent film produced and released by Universal Pictures. Based on a stage play, it was directed by Edward Sloman and is preserved at the Library of Congress.    

==Cast==
*Jean Hersholt - George Caswell or The Deacon
*June Marlowe - Phyllis/Mrs. Nancy Blythe
*Ralph Graves - Jim Adams
*Myrtle Stedman - Mrs. Betsy Clark
*Lincoln Plumer - John Cunningham
*Ned Sparks - Slim Sullivan Tom Kennedy - Bull Moran
*Maurice Murphy - Willie Clark
*George West - George

;uncredited
*Wilson Benge - Minister
*Walter Brennan - Cashier at Cunninghams Rink
*Joseph W. Girard - Sheriff of Morton County
*Annabelle Magnus - Nancy as a Child
*George Periolat - Giuseppi Padroni aka Tony the Dip

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 