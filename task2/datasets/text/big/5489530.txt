A Real Young Girl
{{Infobox film
| name           = A Real Young Girl
| image          = ARealYoungGirl1976Poster.jpg
| caption        = French Poster
| director       = Catherine Breillat
| producer       = Pierre Richard Muller   André Génovès
| writer         = Catherine Breillat
| screenplay     = 
| story          = 
| based on       =  
| starring       = Charlotte Alexandra Hiram Keller Rita Maiden Bruno Balp
| music          = Mort Shuman
| cinematography = Patrick Daert Pierre Fattori
| editing        = Annie Charrier Michèle Quevroy
| studio         = 
| distributor    = Rézo Films
| released       =   (produced in 1976)
| runtime        = 93 minutes (Uncut)
| country        = France
| language       = French
| budget         = 
| gross          = 
}}

A Real Young Girl ( ) is a 1976 French drama about a 14-year-old girls sexual awakening, written and directed by Catherine Breillat. The film, Breillats first, was based on her fourth novel, Le Soupirail.

This film is notable for its graphic depiction of sexuality, which includes Charlotte Alexandra exposing her vulva. This led to it being banned in many countries. It was not released to theaters until 2000.

Breillats films and novels are often about the "erotic and emotional lives of young women, as told from the womans perspective," typically using "blunt language and open depiction of sexual subject matter." Many of Breillats films and novels, including A Real Young Girl have led to controversy and hostile press coverage. For example, Breillats film 36 Fillette, about the "burgeoning sexuality of a 14-year-old girl, and a middle-aged man intent on seducing her" led to "storms of controversy." 

==Plot== masturbated out of boredom. Her father (Bruno Balp) hires a young man named Jim (Hiram Keller), with whom Alice immediately becomes infatuated. Alice has a graphic sexual fantasy in which Jim ties her to the ground with barbed wire, and attempts to insert an earthworm into her vagina. When the earthworm will not fit, Jim tears it into small pieces and puts them in Alices pubic hair.

At a carnival, a middle-aged man exposes himself to her on a ride. She then arrives home and imagines seeing her fathers penis. She exposes herself to Jim, and the two masturbate in front of each other, to Alices chagrin. She discovers her father is having an affair, and Jim tries pressuring her into having sex. He is then shot and killed by a trap Alices father set up to keep wild boar out of his maize field.

==Critical response==
Critic Brian Price refers to A Real Young Girl a "transgressive look at the sexual awakening of an adolescent girl," an "awkward film" which "represents Breillat at her most Bataillesque, freely mingling abstract images of female genitalia, mud, and rodents into this otherwise realist account of a young girls" coming of age. Price argues that the films approach is in line with Linda Williamss defense of literary pornography, which Williams describes as an “elitist, avant-garde, intellectual, and philosophical pornography of imagination" versus the "mundane, crass materialism of a dominant mass culture.” Price argues that "there is no way ... to integrate this film into a commodity driven system of distribution," because it "does not offer visual pleasure, at least not one that comes without intellectual engagement, and more importantly, rigorous self-examination." As such, Breillat has insisted that "sex is the subject, not the object, of her work." 

Reviewer Lisa Alspector from the Chicago Reader called the film’s “theories about sexuality and trauma ... more nuanced and intuitive than those of most schools of psychology," and noted the films use of a blend of dream sequences with realistic scenes. Lisa Alspector Chicago Reader.
http://onfilm.chicagoreader.com/movies/capsules/21135_REAL_YOUNG_GIRL.html 
John Petrakis from the Chicago Tribune noted that Breillat “has long been fascinated with the idea that women are not allowed to go through puberty in private but instead seem to be on display for all to watch, a situation that has no parallel with boys.” Petrakis points out that Breillat’s film “seems acutely aware of this paradox.” Dana Stevens from The New York Times called the film “crude, unpolished, yet curiously dreamy.”  Maitland McDonagh from TV Guide also commented on the film’s curious nature in her review: “neither cheerfully naughty nor suffused with gauzy prurience,   evokes a time of turbulent (and often ugly) emotions with disquieting intensity.” Other reviewers, such as The Christian Science Monitor’s David Sterritt, view the film as a waypoint in the director’s early development toward becoming “a world-class filmmaker.”

Several reviewers have commented on the film’s frank treatment of unusual sexual fantasies and images. Filmcritic.com’s Christopher Null pointed out that the film was “widely banned for its hefty pornographic content,” and called it one of Breillat’s “most notorious” films. Null says “viewers should be warned” about the film’s “graphic shots” of “sexual awakening ... (and) sensory disturbances”, such as the female lead vomiting all over herself and playing with her ear wax. While Null rates this “low-budget work ... about a 3 out of 10 on the professionalism scale” and admits that “it barely makes a lick of sense,” he concedes that “theres something oddly compelling and poetic about the movie.”  The Village Voice’s J. Hoberman called the film a “philosophical gross-out comedy rudely presented from the perspective of a sullen, sexually curious 14-year-old.” The New York Post’s Jonathan Foreman called the film a “test of endurance, and not just because you need a rather stronger word than "explicit" to describe this long-unreleased, self-consciously provocative film.”

==Miscellaneous==
This film has no closing credits; instead, an instrumental version of the song "Suis-je une petite fille (Am I a little girl)" plays over a black screen.

==See also==
* 36 Fillette
* Fat Girl List of mainstream movies with unsimulated sex

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 