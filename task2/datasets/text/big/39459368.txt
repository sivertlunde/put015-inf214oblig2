It Felt Like Love
{{Infobox film
| name           = It Felt Like Love
| image          = 
| alt            = 
| caption        = 
| director       = Eliza Hittman
| producer       = Eliza Hittman   Shrihari Sathe   Laura Wagner
| writer         = Eliza Hittman
| starring       = Gina Piersanti   Giovanna Salimeni   Ronen Rubinstein   Jesse Cordasco   Nicolas Rosen   Richie Folio   Kevin Anthony Ryan   Case Prime
| music          = 
| cinematography = Sean Porter
| editing        = Carlos Marques-Marcet   Scott Cummings
| studio         = 
| distributor    = 
| released       =  
| runtime        = 82 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

It Felt Like Love is a 2013 independent drama film, the first feature film directed by Eliza Hittman.

==Plot==
 

Lila wants to emulate the sexual exploits of her more experienced best friend. She fixates on a tough older guy who will "sleep with anyone" and tries to insert herself into his world, putting herself in a dangerously vulnerable situation.

==Cast==
*Gina Piersanti as Lila
*Giovanna Salimeni as Chiara
*Ronen Rubinstein as Sammy
*Jesse Cordasco (Nyck Caution of Pro Era) as Patrick
*Nicolas Rosen as Devon
*Case Prime as Nate

==Reception==
It Felt Like Love premiered at the 2013 Sundance Film Festival, and subsequently screened at such festivals as International Film Festival Rotterdam, Maryland Film Festival and Giffoni Film Festival. It was acquired by Variance Films in November 2013, with an expected theatrical release in 2014. 

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 