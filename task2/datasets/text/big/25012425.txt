The Great Scout & Cathouse Thursday

{{Infobox Film
| name           =  The Great Scout & Cathouse Thursday
| image          =   
| image_size     = 13 KB
| caption        =  Don Taylor Richard Alan Shapiro
| narrator       = 
| starring       = Lee Marvin Oliver Reed John Cameron
| cinematography = 
| editing        = 
| distributor    =  American International Pictures
| released       = June 23, 1976
| runtime        = 102 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}} Don Taylor starring Lee Marvin, Oliver Reed, Sylvia Miles and Kay Lenz. 

==Plot==
Sam Longwood, a frontiersman who has seen better days, has spent the last 15 years looking for his ex-business partner Jack Colby, who ran off with all the gold from a mine they were prospecting but also took the love of his life, Nancy Sue, as well. Sam, along with his two other partners- Indian Joe Knox and Billy, has finally found Colby and along the way they pick up a young prostitute nicknamed Thursday. But getting their money isnt going to be as easy as they think.

==Cast==
* Lee Marvin as Sam Longwood
* Oliver Reed as Joe Knox
* Robert Culp as Jack Colby
* Kay Lenz as Thursday
* Elizabeth Ashley as Nancy Sue
* Sylvia Miles as Mike
* Strother Martin as Billy

==References==
 

==External links==
* 

 

 
 
 
 
 