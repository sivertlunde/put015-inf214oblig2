Postcards from Leningrad
{{Infobox Film
| name           = Postcards from Leningrad
| image          = Poster posdelen.jpg 
| caption        = Postcards from Leningrad film poster 
| director       = Mariana Rondon  
| writer         = Mariana Rondon 
| starring       = Laureano Olivares, Greisy Mena 
| distributor    = Sudaka Films 
| released       = September 7, 2007 
| country        = Venezuela 
| language       = Spanish
}}
 guerrilla groups in the 1960s in Venezuela.
 Best Foreign Language Film of the 80th Academy Awards.

==Cast==

Laureano Olivares - Teo  
Greisy Mena - Marcela/Clara/Mercedes   
William Cifuentes - Teo (Child)  
Haydee Faverola - Grandmother   
María Fernanda Ferro - Marta   
Ignacio Marquez - Tio Miguel   
Oswaldo Hidalgo - Grandfather   
Claudia Usubillaga  - The Girl

==Synopsis==

During the leftist uprising in the 1960s in Venezuela, a young guerrilla-girl, living in secrecy, gives birth to her first daughter during Mothers Day. Due to that, her photos appear on the newspaper, since that moment they would have to run away.

Hidden places, false disguises and names are the daily life of The Girl, the narrator of the story. Alongside with her cousin (Teo), they re-live the adventures of their guerrilla parents, building up a labyrinth with superheroes and strategies, in which nobody knows where the reality (or madness) begins. However, this childrens game does not hide the deaths, tortures, denunciations and treason within the guerrillas.
 The Invisible Man, in order to escape from the danger. However, they know that their parents might never comeback and therefore, theyll only receive Postcards from Leningrad.

==Awards and honors==

*Best Director (Rajatha Chakoram)at International Film Festival of Kerala(IFFK), 2008 
*Golden Sun Award at Biarritz International Festival of Latin American Cinema, 2007 http://www.imdb.com/title/tt1085491/awards 
*Golden India Catalina Award for best film at Cartagena Film Festival, 2008 
*Feature Film Trophy for best film at Cine Ceara National Cinema Festival, 2008 
*International Jury Award (Revelation Category) at São Paulo International Film Festival, 2007 

==References==
 

==External links==
*  - Official site. (in Spanish)
*  (in Spanish)
*  

 
 
 
 