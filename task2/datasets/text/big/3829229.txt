Good Morning, Boys
{{Infobox Film
| name           = Good Morning, Boys
| image          = "Good_Morning_Boys"_(1937).jpg
| image_size     = 
| caption        = 
| director       = Marcel Varnel Edward Black
| writer         = Leslie Arliss Marriott Edgar
| narrator       = 
| starring       = Will Hay Graham Moffatt
| music          = Louis Levy
| cinematography = Arthur Crabtree
| editing        = R.E. Dearing   Alfred Roome
| studio         = Gainsborough Pictures
| distributor    = Gaumont British Distributors
| released       = January 1937  
| runtime        = 79 minutes
| country        = United Kingdom English 
}}
Good Morning, Boys is a 1937 British comedy film directed by Marcel Varnel and featuring Will Hay, Martita Hunt, Lilli Palmer and Peter Gawthorne. It was made at the Gainsborough Studios in Islington. 

==Plot outline==
 Dr Twist, horses with his pupils and teaches them little. Colonel Willoughby-Gore attempts to sack the incompetent Twist but is foiled when he and his boys, after fraudulently gaining resounding success in a French examination, are invited to Paris by the French ministry of education.

In Paris they become involved with a gang of criminals, including escaped convict Arty Jones, father of one of the boys, and Yvette, a night club singer, who are attempting to steal the Mona Lisa from the Louvre and replace it with a duplicate.

==Cast==
*Will Hay as Dr Benjamin Twist
*Martita Hunt as Lady Bagshott
*Peter Gawthorne as  Col. Willoughby-Gore
*Graham Moffatt as Albert Brown
*Fewlass Llewellyn as The Dean Mark Daly as  Arty Jones
*Peter Godfrey as Cliquot
*C. Denier Warren as Minister of Education
*Lilli Palmer as Yvette Charles Hawtrey as  Septimus
*George Ravenscroft as one of the boys

==Critical reception==
Allmovie wrote, "the magnificent Will Hay re-creates his vaudeville characterization of a supercilious schoolmaster...But the inimitable, toothless Moore Marriott (aka "Harbottle") is conspicuous by his absence."  

==References==
 

==Bibliography==
* Low, Rachael. Filmmaking in 1930s Britain. George Allen & Unwin, 1985.
* Wood, Linda. British Films, 1927-1939. British Film Institute, 1986.

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 


 