Masterminds (2015 film)
 
{{Infobox film
| name           = Masterminds
| image          = Masterminds (2015 film) poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Jared Hess
| producer       = John Goldwyn Lorne Michaels
| writer         = Chris Bowman Jody Hill Danny McBride Hubbel Palmer Emily Spivey
| starring       = Zach Galifianakis Owen Wilson Kristen Wiig Jason Sudeikis
| music          = 
| cinematography = Erik Wilson 	 David Rennie
| studio         = Michaels-Goldwyn
| distributor    = Relativity Media
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 heist comedy film based on the 1997 Loomis Fargo Robbery in North Carolina, directed by Jared Hess and written by Chris Bowman, Jody Hill, Danny McBride, Hubbel Palmer and Emily Spivey. The film stars Zach Galifianakis, Owen Wilson, Kristen Wiig and Jason Sudeikis. The film is scheduled to be released on August 14, 2015, by Relativity Media. 

==Cast==
*Zach Galifianakis  as David Scott Ghantt
*Kristen Wiig    as Kelly Campbell
*Owen Wilson  as Steve Chambers 	
*Mary Elizabeth Ellis  as Michelle Chambers	
*Jason Sudeikis   	 as Mike McKinney	
*Ken Marino    as Doug
*Devin Ratray 	as Runny		
*Kate McKinnon 		 Leslie Jones  as Detective	 Jon Daly 
*Ross Kimball  as Eric
*Jordan Israel
*Njema Williams as Ty

== Production == Leslie Jones, Jon Daly joined the cast of the film to play the FBI agent.   

=== Filming === Asheville area.   On July 29, Galifianakis was spotted in prisoners costume, during the filming in the streets of downtown Asheville, which were transformed.    The BB&T Center building, also the location of the production office, was transformed into the "Park Street Citizens Bank", with a Loomis Fargo burgundy truck parked outside of the entrance. The crew also shot the film on the steps of Buncombe County Courthouse, and in front of the Mediterranean Restaurant.  

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 