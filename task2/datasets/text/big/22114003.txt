Frisco Jenny
{{Infobox film
| name           = Frisco Jenny
| image          = Poster - Frisco Jenny 09.jpg
| image_size     =
| caption        =
| director       = William A. Wellman
| producer       = Raymond Griffith
| writer         = Gerald Beaumont (story) Lillie Hayward (story) John Francis Larkin (story) Robert Lord Wilson Mizner
| narrator       =
| starring       = Ruth Chatterton Louis Calhern
| music          =
| cinematography = Sidney Hickox
| editing        =
| studio         = First National Pictures
| distributor    = First National Pictures
| released       =  
| runtime        = 71-73 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Frisco Jenny is a 1932 Pre-Code Hollywood|pre-Code drama film starring Ruth Chatterton and directed by William A. Wellman.

==Plot== Tenderloin district, James Murray), earthquake kills both men and devastates the city. In the aftermath, Jenny gives birth to a son, whom she names Dan.

With financial help from crooked lawyer Steve Dutton (Louis Calhern), who himself came from the Tenderloin, she sets herself up in the vice trade, providing women on demand. Jenny has one loyal friend, the Chinese woman Amah (Helen Jerome Eddy), who helps take care of the baby.

At a party in Steves honor, he catches gambler Ed Harris (an uncredited J. Carrol Naish) cheating him in a back room. In the ensuing struggle, Steve kills him, with Jenny the only eyewitness. The pair are unable to dispose of the body before it is found and are questioned by the police. However, neither is charged. The scandal forces Jenny to temporarily give up her baby to a very respectable couple who owe Steve a favor to keep the child from being taken away from her. 
 bootlegging in the city.

When Dan runs for district attorney, his opponent is Tom Ford (an uncredited Edwin Maxwell), who does Jennys bidding. Against her best interests, she frames Ford so that Dan can win.

When Steve tries to bribe Dan to free some of his men, he is arrested. Out on bail, Steve asks Jenny to blackmail Dan into dropping the charges, but she refuses to jeopardize her sons future. In fact, she intends to retire to France with Amah. When Steve threatens to reveal that Jenny is Dans real mother, she shoots and kills him at Dans office.

She is quickly arrested and prosecuted by Dan. Refusing to defend herself, she is condemned to death by hanging. Amah pleads with her to tell Dan the truth in the hope that he can help her, but when he comes to see her, she remains silent.

==Cast (in credits order)==
*Ruth Chatterton as Frisco Jenny Sandoval
*Louis Calhern as Steve Dutton
*Helen Jerome Eddy as Amah Donald Cook as Dan Reynolds James Murray as Dan McAllister
*Hallam Cooley as Willie Gleason Pat OMalley as Policeman Pat OHoolihan
*Harold Huber as George Weaver
*Robert Emmett OConnor as Jim Sandoval
*Willard Robertson as Police Captain Tom

According to Robert Osborne of Turner Classic Movies, George Brent was originally slated for the role of Dan Reynolds. However, Chatterton and the younger Brent were soon to be married, and she did not like the idea of playing his mother.

==External links==
 
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 