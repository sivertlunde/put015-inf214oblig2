Lumberjack (film)
{{Infobox film
| name           = Lumberjack
| image_size     =
| image	=	Lumberjack FilmPoster.jpeg
| caption        =
| director       = Lesley Selander
| producer       = Lewis J. Rachmil (associate producer) Harry Sherman (producer)
| writer         = Clarence E. Mulford (characters) Norman Houston (screenplay) and Barry Shipman (screenplay)
| narrator       =
| starring       = See below
| music          = Paul Sawtell
| cinematography = Russell Harlan
| editing        =
| distributor    =
| released       = 28 April 1944
| runtime        = 65 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Lumberjack is a 1944 American film directed by Lesley Selander.

== Cast == William Boyd as Hopalong Cassidy
*Andy Clyde as California Carlson Jimmy Rogers as Jimmy Rogers
*Douglass Dumbrille as Daniel J. Keefer
*Ellen Hall as Julie Peters Jordan
*Francis McDonald as Clyde Fenwick
*Ethel Wales as Aunt Abbey Peters
*Hal Taliaferro as Henchman Taggart Charles Morton as Big Joe Williams
*Herbert Rawlinson as Buck Peters
*Frances Morris as Mrs. Williams John Whitney as Ben Jordan
*Jack Rockwell as Sheriff Miles

== Soundtrack ==
* "The Place Your Heart Calls Home" (Written by Ozie Waters and Forrest Johnson)

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 


 