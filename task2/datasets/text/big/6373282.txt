The Golden Girls (film)
 
 
{{Infobox film
| name           = The Golden Girls
| image          = 
| alt            = 
| caption        = 
| film name      = 山水有相逢 Joe Ma
| producer       = Cheung Chi-Sing Fok Hoi-Bo Charles Heung
| writer         = Cheung Chi Sing Joe Ma
| screenplay     = 
| story          = 
| based on       =  
| starring       = Lau Ching-Wan Anita Yuen Ada Choi
| narrator       =  
| music          = Clarence Hui Joseph Hwang Ronald Ng
| cinematography = Kwan Pak-Suen
| editing        = Kwong Chi-Leung
| studio         = Wins Entertainment
| distributor    = China Star Entertainment Group
| released       =  
| runtime        = 94 min
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          =  
}} Hong Kong Joe Ma. sitcom of the same name.

==Cast==
* Cheung Chi-Sing
* Cheung Tat-ming - Cousin J.P.
* Perry Chiu
* Ada Choi - Lulu Shum
* Allen Fong
* Vincent Kok - Kent
* Nancy Kwan
* Lau Ching-Wan - Chun Wei
* Lee Lik-Chi
* Francis Ng - Wong Siu-Yi
* Paulyn Sun - May Chu (credited as Pauline Suen)
* Michael Tse - Fey Leading Man
* Manfred Wong - Director of Cleopatra
* Wong Yut Fei - Lo Kim-Choi
* Anita Yuen - Mei-Ball

==See also==
* List of Hong Kong films

==External links==
*  
*  

 

 
 
 
 

 