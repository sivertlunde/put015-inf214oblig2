Rhino!
{{Infobox film
| name           = Rhino!
| image          = Rhino! poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Ivan Tors
| producer       = Ben Chapman 
| screenplay     = Art Arthur Arthur Weiss
| story          = Art Arthur
| starring       = Harry Guardino Shirley Eaton Robert Culp Harry Makela George Korelin
| music          = Lalo Schifrin
| cinematography = Lamar Boren Sven Persson 
| editing        = Warren Adams
| studio         = Ivan Tors Films
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 91 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Rhino! is a 1964 American action film directed by Wolf Rilla and written by Art Arthur and Arthur Weiss. The film stars Harry Guardino, Shirley Eaton, Robert Culp, Harry Makela and George Korelin. The film was released on May 20, 1964, by Metro-Goldwyn-Mayer.  

==Plot==
 

== Cast ==
*Harry Guardino as Alec Burnett
*Shirley Eaton as Miss Arleigh
*Robert Culp as Dr. Hanlon
*Harry Makela as Jopo 
*George Korelin as Haragay

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 

 