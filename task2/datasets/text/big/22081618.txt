The Worst Years of Our Lives
{{Infobox film
| name           = Los peores años de nuestra vida
| director       = Emilio Martínez Lázaro
| writer         = David Trueba
| starring       = Gabino Diego Agustín González
| music          = Michel Camilo
| editing        = Iván Aledo
| released       = 9 September 1994 (Spain) 3 March 1995	(USA)
| runtime        = 100 minutes
| country        = Spain French
}}
Los peores años de nuestra vida (  comedy film directed by Emilio Martínez Lázaro and written by David Trueba. The film was nominated for four Goya Awards, winning one.

==Awards and nominations==

===Won=== Goya Awards
**Best Sound (Polo Aledo, José Antonio Bermúdez, Carlos Garrido and Gilles Ortion)

===Nominated=== Goya Awards
**Best Actor in a Leading Role (Gabino Diego)
**Best Actor in a Supporting Role (Agustín González)
**Best Screenplay &ndash; Original (David Trueba)

==External links==
*  

 
 
 
 
 
 


 