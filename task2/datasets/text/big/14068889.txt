The Chemist (film)
 
{{Infobox film
| name           = The Chemist
| image          = 
| caption        = 
| director       = Al Christie
| producer       = Al Christie E. W. Hammons
| writer         = David Freedman
| starring       = Buster Keaton
| music          = 
| cinematography = Dwight Warren George Webber
| editing        = 
| distributor    = 
| released       =  
| runtime        = 19 minutes
| country        = United States 
| language       = English
| budget         = 
}}
 short comedy film featuring Buster Keaton.

==Plot==
Elmer Triple (Buster Keaton) needs to invent something new and amazing.  He succeeds not only in creating the "next big thing", but also in catching the eyes of robbers who would like to steal it.

==Cast==
* Buster Keaton - Elmer Triple
* Marlyn Stuart
* Earle Gilbert
* Donald MacBride
* Herman Lieb

==See also==
* List of American films of 1936
* Buster Keaton filmography

==External links==
* 

 
 
 
 
 
 
 
 
 


 