Sowing the Wind (1916 film)
{{Infobox film
| name           = Sowing the Wind 
| image          =
| caption        =
| director       = Cecil Hepworth
| producer       = Cecil Hepworth
| writer         = Sydney Grundy  (play)   Blanche McIntosh 
| starring       = Henry Ainley   Alma Taylor   Stewart Rome
| cinematography = 
| editing        = 
| studio         = Hepworth Pictures
| distributor    = Hepworth Pictures
| released       = May 1916 
| runtime        = 
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         =
| preceded_by    =
| followed_by    =
}} Sowing the Wind by Sydney Grundy. 

==Cast==
*   Henry Ainley as Tom Brabazon 
* Alma Taylor as Rosamond  
* Stewart Rome as Ned Annersley  
* Violet Hopson as Helen Gray  
* Chrissie White as Maude Fretwell  
* Lionelle Howard as Bob Watkin   Charles Vane as Lord Petworth  
* Percy Manton as Sir Richard Cursitor

==References==
 

==Bibliography==
* Goble, Alan. The Complete Index to Literary Sources in Film. Walter de Gruyter, 1999.

==External links==
* 

 
 
 
 
 
 
 
 
 

 