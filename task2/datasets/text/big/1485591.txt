Perfect Strangers (2004 film)
{{Infobox film
| name = Perfect Strangers
| director =Robin Shepperd
| producer =
| writer =Simon Booker
| starring =Rob Lowe Anna Friel Khandi Alexander
| music =Adrian Johnston
| cinematography =Michael McMurray
| editing =Ralph Brunjes
| distributor =Working Title Films
| released =  
| runtime = 120 min.
| country =United Kingdom
| language = English
}}

Perfect Strangers is a 2004 romantic comedy film starring Rob Lowe and Anna Friel.

==Plot==
Lloyd and Susie in London work for the same ad agency, but in different cities. An arrangement is made where they switch jobs and  homes for a month. After settling into each others place they consult each other by phone, its not long before they fall in love.

==Cast==
*Rob Lowe as Lloyd Rockwell
*Anna Friel as Susie Wilding
*Khandi Alexander as Christie Kaplan
*Sarah Alexander as Alix Mason
*Jennifer Baxter as Betsy
*Gabriel Hogan as Harvey Truelove
*Jane Luk as Dee Dee Colin Fox as Sir Nigel
*Katie Bergin as Kathy

==External links==
*  

 
 
 
 
 


 
 