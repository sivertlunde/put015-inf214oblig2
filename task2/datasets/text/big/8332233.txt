The Keeper: The Legend of Omar Khayyam
 
{{Infobox film 
| name = The Keeper: The Legend of Omar Khayyam
| image = Thekeeper.jpg
| caption = 
| director = Kayvan Mashayekh
| producer = Belle Avery Kayvan Mashayekh Sep Riahi
| writer = Belle Avery Kayvan Mashayekh Christopher Simpson Andranik Madadian
| music = Elton Ahi Shani Rigsbee
| cinematography = Matt Cantrell Dusan Joksimovic
| editing = Duncan Burns
| distributor = 
| released =  
| runtime = 95 minutes
| country = United States
| language = Persian English
| budget = 
}}
 Persian intellectual Omar Khayyám. It was directed by Kayvan Mashayekh and stars Vanessa Redgrave and Moritz Bleibtreu. It was released in 2005.

==Plot==
Kamran is a 12-year-old boy in the present day who discovers that his ancestor is the 11th-century mathematician, astronomer, and poet of Persia, Omar Khayyam. The story has been passed down in his family from one generation to another, and now it is his responsibility to keep the story alive for future generations. The film takes us from the modern day to the epic past where the relationship between Omar Khayyam, Hassan Sabbah (the original creator of the sect of Assassins) and their mutual love for a beautiful woman separate them from their eternal bond of friendship.

Filmed almost entirely on location in Samarkand and Bukhara, Uzbekistan.

==Cast==
*Vanessa Redgrave as The Heiress Malikshah
*Rade Serbedzija as Imam Muaffak (Nizam al-Mulk)
*Bruno Lastra as Omar Khayyam Christopher Simpson as Hassan Sabbah
*Andranik Madadian as Ali Ben-Sabbah
*David McColloch as Soccer Kid #15 (Kid Hit In Chest)
*Jahan Ghashghaei Vocals / Musicsian

==Release==
The film commenced its theatrical run in movie theaters across the US beginning in June 2005 and ended up playing in 14 US cities for nearly one year. In addition to its lengthy theatrical run, the film raised nearly $200,000 for Children’s and Cancer Charities in Houston, New York and London, England as a direct result of its non-profit screenings.

The film was an Official Selection at the 2005 Moscow International Film Festival.  In 2006, it received two prestigious Golden Lioness Awards for Excellence in Directing and Costume Design from the World Academy of Arts, Literature and Media in Budapest, Hungary.  “The Keeper” screenplay has also been selected for the permanent script collection of the Academy of Motion Picture Arts and Sciences.  

Additionally, the Mayor of the City of Houston proclaimed July 26, 2005 “Keeper Day” in a public ceremony at City Hall.  This unique honor was in commemoration of Kayvan Mashayekh’s outstanding contribution to the cultural diversity of the City as direct result of its charitable gift to Texas Children’s Cancer Center of $91,000 from the screening of the film at its Premiere.

==External links==
*  

 
 
 
 
 
 
 