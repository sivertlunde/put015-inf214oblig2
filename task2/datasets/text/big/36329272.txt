Zuster Theresia
{{Infobox film
| name      = Zuster Theresia
| image     = Zuster Theresia.jpg
| image size   = 
| border     = 
| alt      = 
| caption    = Theatrical poster
| director    = {{plain list|
*M. H. Schilling
*Wong brothers
}}
| producer    =
| writer     = 
| starring    = {{plain list|
*M. H. Schilling
*Daisy Diephuis
*Henk Maschhaup
*Alle Heymann
}}
| music     = 
| cinematography = 
| editing    = 
| studio     = Halimoen Film
| distributor  = 
| released    =  
| runtime    = 
| country    = Dutch East Indies
| language    = 
| budget     = 
| gross     = 
}}
Zuster Theresia (English: Sister Theresia) is a 1932 film from the Dutch East Indies (modern-day Indonesia) directed by M. H. Schilling with the help of the Wong brothers. The film, starring Henk Maschhaup, Daisy Diephuis, and Alle Heymann, follows a young man and his relationship with two women. A commercial failure, the film was the last made by Schilling and led the Wongs to take a two-year hiatus.

==Plot==
Bob (Hugo de Rode) dies not long after finishing his studies in the Netherlands, and his classmate Henk (Henk Maschhaup) goes to the Dutch East Indies, where Bobs father Gelder (M. H. Schilling) has a small orchard outside Bandung. Henk begins helping the family financially, and marries Bobs sister Daisy (Daisy Diephuis). However, Daisy enjoys going to parties and dances, while their child Baby (Carl Schilling) is left alone.

Henk calls his cousin Flora (Alle Heymann) to help with the child, and begins to fall in love with her. When Baby is taken ill, Flora treats him. This closeness drives Daisy mad with jealousy, and she goes to nearby Pelabuhan Ratu and unsuccessfully tries to commit suicide by throwing herself into the sea. Flora and Henk marry, but when Daisy returns home Flora is forced to annul her marriage and leave the house. She joins a nunnery, where she becomes Sister Theresia.

==Production== native and Indo culture in his films, something he was unable to distance himself from for Zuster Theresia. 

Active direction was done by the Wong brothers, ethnic Chinese who generally made Chinese-oriented films. As Zuster Theresia was directed at Dutch audiences, the Indonesian film historian Misbach Yusa Biran suggests that the Wongs had little creative input. 

The film was the last to be produced by Halimoen Film.  It was a sound film, but the sound was of low quality;  the first talkie in the Dutch East Indies, Karnadi Anemer Bangkong, had only been released in 1930.  The films poster emphasised its Dutchness and depicted only Flora, as Sister Theresia, praying, with text above and below the image. Ade Irwansyah, compiling a list of the best Indonesian film posters for Tabloid Bintang, selected the poster for Zuster Theresia as the twentieth-best of all time, noting that its simplicity strengthened the image of a nun praying. 

==Release and reception==
The film was released in the Dutch East Indies in 1932,  where it was a commercial failure; it was not circulated in the Netherlands. Although its main audience was the Dutch, well-educated natives such as Armijn Pane are also recorded as seeing it. The film was Schillings last  and the Wong Brothers retired from filmmaking until 1934, when they joined Albert Balink to produce Pareh (Rice; 1936). 
==See also==
*List of films of the Dutch East Indies

==References==
Footnotes
 

Bibliography
 
*{{cite book
 |title= 
 |trans_title=History of Film 1900-1950: Making Films in Java
 |language=Indonesian
 |last=Biran
 |first=Misbach Yusa
 |author-link=Misbach Yusa Biran
 |publisher=Komunitas Bamboo working with the Jakarta Art Council
 |year=2009
 |isbn=978-979-3731-58-2
 |ref=harv
}}
*{{cite web
 |url=http://www.tabloidbintang.com/extra/top-list/9752-20-poster-film-indonesia-terbaik-sepanjang-masa-imho.html
 |title=20 Poster Film Indonesia Terbaik Sepanjang Massa (IMHO)
 |trans_title=20 Best Indonesian Movie Posters of All Time (IMHO)
 |last=Irwansyah
 |first=Ade
 |date=8 March 2011
 |work=Tabloid Bintang
 |language=Indonesian
 |archivedate=5 July 2012
 |archiveurl=http://www.webcitation.org/68uxBYoEM
 |ref= 
 |accessdate=19 August 2011
}}
 

==External links==
* 

 
 
 

 
 