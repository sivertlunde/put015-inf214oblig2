Mr. Cinderella
{{Infobox film
| name           = Mr. Cinderella
| image          = 
| alt            = 
| caption        = 
| director       = Edward Sedgwick
| producer       = Edward Sedgwick Hal Roach
| screenplay     = Richard Flournoy Arthur V. Jones 
| story          = Jack Jevne
| starring       = Jack Haley Betty Furness Arthur Treacher Raymond Walburn Robert McWade Rosina Lawrence
| music          = Marvin Hatley
| cinematography = Milton R. Krasner
| editing        = Jack Ogilvie 
| studio         = Hal Roach Studios
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 75 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Mr. Cinderella is a 1936 American comedy film directed by Edward Sedgwick and written by Richard Flournoy and Arthur V. Jones. The film stars Jack Haley, Betty Furness, Arthur Treacher, Raymond Walburn, Robert McWade and Rosina Lawrence. The film was released on October 23, 1936, by Metro-Goldwyn-Mayer.  

==Plot==
 

== Cast ==
*Jack Haley as Joe Jenkins
*Betty Furness as Patricia Pat Randolph
*Arthur Treacher as Watkins
*Raymond Walburn as Peter Randolph
*Robert McWade as Mr. J.J. Gates
*Rosina Lawrence as Maizie Merriweather
*Monroe Owsley as Aloysius P. Merriweather
*Kathleen Lockhart as Aunt Penelope Penny Winfield
*Edward Brophy as Detective McNutt
*Charlotte Wynters as Martha
*Tom Dugan as Spike Nolan
*Iris Adrian as Lil
*Toby Wing as Lulu
*Morgan Wallace as Mr. Emmett Fawcett
*Arthur Aylesworth as Mr. Simpson
*John Hyams as Mr. Wilberforce
*Leila McIntyre as Mrs. Wilberforce 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 