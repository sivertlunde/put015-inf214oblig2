Haunted (2007 film)
{{Infobox film name           = Haunted image          = MusallatFilmPoster.jpg image size     = alt            =  caption        = Film poster director       = Alper Mestçi producer       = Banu Akdeniz Murat Toktamisoglu writer         = Güray Ölgü Alper Mestçi narrator       =  starring       = Bigkem Karavus Burak Özçivit Kurtulus Sakiragaoglu music          = Resit Gözdamla cinematography = Feza Caldiran editing        = Alper Mestçi studio         = Dada Film Mia Yapim distributor    = Özen Film Maxximum Film und Kunst GmbH released       =   runtime        = 90 min   country        = Turkey language       = Turkish budget         = $1,500,000 (estimated) gross          = $2,170,188 preceded by    = followed by    = 
}}

Haunted (  directed by Alper Mestçi about a young Turkish guest-worker who it is haunted by dark visionsin Berlin. The film, which opened nationwide on   was one of the highest-grossing Turkish films of 2007.

==Production==
The film was shot on location in Istanbul, Turkey and Berlin, Germany.   

==Plot synposis==
Suat (Burak Özcivit) is a young Turkish man who leaves his new bride Nurcan (Bigkem Karavus) behind in Turkey and joins his childhood friend Metin (İbrahim Can) in Berlin where he can earn some money. Here he is haunted by dark visions that eventually drive him to attempt suicide. When the doctors in Germany can find nothing wrong Suat and Metin return to Istanbul to seek the advice of spiritual healer Haci Burhan Kasavi (Kurtuluş Şakirağaoğlu). Suat and family of Haci except a daughter of Haci was killed by a demon angry about him for being killed when born as the bady of Nurcan.
Musallat 2: Lanet (2011) by the same company is about a young woman find she is a relative of a jinn and being born after a couple asked a witch to ask for a child for a jinn through the cut animal legs sorcerery ritual.

==Cast==
*Burak Özçivit as Suat
*Biğkem Karavus as Nurcan
*Kurtuluş Şakirağaoğlu as Haci Burhan Kasavi
*İbrahim Can as Metin

==Release==
The film opened in 125 screens across Turkey on   at number 3 in the box office chart with an opening weekend gross of $372,573.    

{| class="wikitable sortable" align="center" style="margin:auto;"
|+ Opening weekend gross
|-
!Date!!Territory!!Screens!!Rank!!Gross
|-
|  
| Turkey
| 125
| 3
| $372,573
|-
|  
| Germany
| 23
| 24
| $69,936
|-
|  
| Austria
| 2
| 21
| $11,999
|-
|  
| Belgium
| 2
| 23
| $18,878 
|}

==Reception==
===Box Office=== Turkish film of 2007 with a total gross of $1,802,504. 

The worldwide total gross rose to $2,170,188 following ineternational release in 2008. 

=== Reviews ===
Todd Brown, writing for Twitch Film, claims to have been, drawn in by the simple but compelling story of a young family haunted and possessed, with, a pretty compelling blend of pulp with legitimate tension, which looks to ride that same split in influences, of technical proficiency and just plain pulp that has long been the mark of Turkish genre films, though the technical end looks stronger than most, with, truly moody and unsettling imagery. If the film comes close to living up to the trailer this thing is EASILY going to be the best Turkish genre picture of the past five to ten years, he concludes.         

Chris Churchill, also writing for Twitch Film, adds that, The film also scored Theatrical releases in a few European countries earlier in the year, something quite rare for Turkish genre films in the past, so there could be something that makes Musallat stand out from the recent crop of Turkish Horror films produced.     

==See also==
*2007 in film
*Turkish films of 2007

==External links==
* 
* 
* 

==References==
 

 
 
 
 
 
 