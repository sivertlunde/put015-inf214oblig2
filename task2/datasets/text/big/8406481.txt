El Apóstol
{{Infobox film
| name = El Apóstol
| image        =
| caption      =
| director     = Quirino Cristiani
| producer     = Federico Valle
| writer       = Quirino Cristiani
| starring     =
| distributor  =
| released     = November 9, 1917
| country      = Argentina
| runtime      = 70 minutes (14 frame/s) Spanish (Castellano) intertitles
| budget       =
| music        =
}}
El Apóstol (Spanish: "The Apostle") was a 1917 Argentine animated film utilizing cutout animation, and the worlds first animated feature film.  

==Production background==
The film was written by Alfonso de Laferrere    and directed by Quirino Cristiani. The film consisted of a total of 58,000 frames played over the course of 70 minutes (at 14 frames per second). 

The film was a satire, with President Hipólito Yrigoyen ascending to the heavens to use Jupiter (mythology)|Jupiters thunderbolts to cleanse Buenos Aires of immorality and corruption. The result is a burnt city. The film was well received by critics at the time and a commercial success. A fire that destroyed producer Frederico Valles film studio incinerated the only known copy of El Apóstol, and so it is now considered a lost film.

A 2007 documentary Quirino Cristiani: The mystery of the first animated movies, directed by animator Gabrielle Zuchelli researches the history of the studio and recreates the look and technique used in El Apóstol.

==See also==
 
*List of animated feature-length films
*List of lost films

== References ==
 

== External links ==
*  
*  
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

 
 