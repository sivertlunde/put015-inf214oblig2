Dark Skies (film)
For Dark Skies (TV Series).

{{Infobox film
| name           = Dark Skies
| image          = Dark Skies Poster.jpg
| image_size     = 215px
| alt            =
| caption        = Theatrical release poster Scott Stewart
| producer       = Jason Blum Couper Samuelson Jeanette Brill
| screenplay     = Scott Stewart Josh Hamilton Dakota Goyo Kadan Rockett J. K. Simmons
| music          = Joseph Bishara David Boyd
| editing        = Peter Gvodas
| studio         = Entertainment One Blumhouse Productions
| distributor    = Dimension Films
| released       =  
| runtime        = 97 minutes  
| country        = United States
| language       = English
| budget         = $3.5 million 
| gross          = $26.4 million   
}} science fiction Scott Stewart Josh Hamilton and Dakota Goyo.  

The film was released on February 22, 2013.

==Plot== Josh Hamilton), The Sandman, which frightens Sammy.

Lacy wakes up at night and checks on her boys before heading down to the kitchen where she discovers the fridge door open with its contents spread out all over the floor. The backdoor is also wide open. The next day, she and Daniel believe an animal broke in, although Lacy questions why the vegetables were eaten while the meat remained untouched.

Daniel has a job interview which doesnt go well, and he receives a notice in the mail that their mortgage payment is late. That night, Lacy wakes up and heads down to the kitchen. She discovers all of their canned and packaged foods stacked up in towers all over the kitchen counters, floor, and table. The chandelier above the dining table also appears to project a strange sign on the ceiling. Lacy brushes it off as a nightmare, and in the morning the police come to check all of the doors and windows to see if there was a break in, but find nothing. The police officer suggests Daniel reconnect his home security system despite the cost. The next night, the alarm is set off, waking up the entire family. Daniel shuts off the alarm and notices that all doors and windows are still locked. The alarm company calls and believes it to be a malfunction in the system because it shows that all entry points into the house were breached at exactly the same time. Lacy and Daniel then notice all of their photographs have disappeared from their frames. The alarm company sends over a technician in the morning who determines that nothing was breached.

The number of strange events begins to increase in frequency and danger. While Sammy is playing soccer with the other kids, he wets himself, stares into space, and screams. Lacy, who is at home, witnesses hundreds of birds crashing into the house. At night, Lacy is awoken by a sound and hears Sammy talking. When she goes to check on him, she sees a figure standing over his bed. She screams and turns on the light only to find an empty room. Daniel and Lacy run around the house trying to find Sammy, when Daniel sees him in the front yard walking away from the house. They retrieve him, but Sammy has no idea why he is outside. Immediately after this experience, Daniel installs security cameras around the house.

The next day, Lacy has a viewing at a house for sale. As shes pitching the house to a couple, she becomes unable to speak and enters a trance. She walks towards the glass door and begins to bash her head repeatedly against it before waking up in her own bed. She finds a bruise forming on her forehead and notices that she blacked out for six hours. The scientist investigating the birds flying into her home calls Lacy and tells her the birds came from three different flocks migrating from three different directions, and it was as if something was "drawing" the birds to her home. Lacy begins to search online for answers and finds articles on UFOs along with pictures other children have drawn that are very similar the ones Sammy has drawn of a child and three grey figures holding hands beside a house.
 catatonic state. His nose begins to bleed heavily. He wakes up with no memory of what happened. Lacy and Daniel watch the video camera footage of Daniel getting out of bed and walking outside and Lacy tells him about the things she read of UFOs, though Daniel dismisses her.

Sammy goes to the pool with the Jessops the next day but refuses to go swimming. Shelly (Annie Thurman), attempts to take off his shirt and discovers a strange formation of bruises all over his chest. Jesse and his friend Ratner (L. J. Benet) sneak into the woods behind a golf course where Ratner shoots at Jesse with a pellet gun. Jesse freezes in a standing position and has a seizure that causes his eyes to roll back into his head. He is taken to the hospital, where the doctors inform Lacy and Daniel that Jesse is covered in strange symbols that had to have been branded onto his body. Due to Jesses age, the doctor informs Lacy and Daniel that child protective services are going to be notified. When Daniel hears that Ratner and Jesse were together just prior to the incident, Daniel believes that Ratner is the one who hurt Jesse; he attacks Ratner in his front yard before being knocked out by Ratners dad, drawing the ire of Jesse. Sammy is returned, and the Jessops now also believe that the Barretts are abusing their children.
 The Grays". Pollard informs them that there are 3 of these aliens, and that many have suffered the same fate as the Barretts, with most cases ending in a child abduction. Lacy finds a wall in Edwins apartment covered in newspaper articles about missing children. Edwin warns the Barretts that they should be hyper-protective of their child, who has been "chosen" and that they must work together in order to save their son in hopes the aliens will move on to another family. Edwin states that sickness and nosebleeds are signs, as is the drawing that Lacy discovered.

Daniel goes out and buys a shotgun and boards up the windows of the house while Lacy buys an aggressive guard dog. The family spend July 4 inside their home eating dinner, recounting earlier July 4ths for the boys. While watching fireworks on the TV, the transmission is cut to static, all the lights begin to flicker and the dog starts barking ferociously at the boarded up front door. Lacy sends Jesse into his room with Sammy and tells him not to take his eyes off of his brother. She stands guard outside the door with a knife in hand. Downstairs, Daniel has his gun pointed at the front door as bright lights shine around it. The screws holding the boards in place unscrew on their own, and then all the power in the house cuts out.

Upstairs Lacy hears the TV in her bedroom turn on. She walks towards it, unaware of a Gray behind her, and becomes trapped in her room. Daniel fires into the bright light, hoping to hit one of the Grays. He then runs upstairs and gets Jesse and Sammy into his and Lacys room where they barricade themselves in and huddle together on the bed. The TV begins to flicker again, and the Grays can be seen in the room surrounding the bed. Lacy sees them and screams and Jesse blacks out. Jesse awakes in the foreclosed house from earlier in the film. He finds his mother dead in the bloodied kitchen before witnessing his father commit suicide. Seeing his brother, he chases after Sammy before reawakening in the upstairs hallway of his house. Daniel, Lacy, and Sammy are all together, staring at Jesse in concern and bewilderment. The Grays appear in front of Jesse, there is flash of light, and he is abducted.

Three months later, Lacy and Daniel are suspects in Jesses disappearance, and theyve moved into an apartment. Pollard cuts up a newspaper article about Jesses disappearance and hangs it on his wall with all the other pictures of missing children. As Lacy is going through old things, she finds pictures that Jesse drew as a child that involved three grey figures holding his hand in front of a house. Lacy realizes that illness was one of the symptoms of the Grays and remembers that Jesse was sick for the longest time as a child. She comes to understand that Jesse was the one at risk, and not Sammy. As she realizes this, the walkie-talkie begins to go off with feedback, and she hears Jesses voice saying "Sam".

==Cast==
* Keri Russell as Lacy Barrett Josh Hamilton as Daniel Barrett
* Dakota Goyo as Jesse Barrett
* Kadan Rockett as Sam Barrett
* J. K. Simmons as Edwin Pollard
* L. J. Benet as Kevin Ratner
* Rich Hutchman as Mike Jessop
* Myndy Crist as Karen Jessop
* Annie Thurman as Shelly Jessop
* Jack Washburn as Bobby Jessop
* Ron Ostrow as Richard Klein
* Cary Quattrocchi as Martin Haldeman
* Brian Stepanek as Security system technician
* Judith Moreland as Janice Rhodes
* Trevor St. John as Alex Holcombe
* Alyvia Alyn Lind as Young Daughter

==Production== Scott Stewart  and produced by Jason Blum,    Jeanette Brill and Couper Samuelson.  The films screenplay was written by Stewart;    the original script by Stewart took about six weeks to finish writing. 

==Release==
Dark Skies was released in the United States on February 22, 2013     and in the United Kingdom on April 5, 2013. 

==Reception==
Dark Skies received mostly mixed reviews from film critics. It holds a 40% "rotten" rating on   based on 18 reviews. Michael OSullivan of The Washington Post wrote that " he movie builds a moderate, if less than monumental, level of spookiness, regardless of your ignorance. It’s a workmanlike piece of suspense." He gave the film 2 out of 4 stars.  In a moderately favourable review for The New York Times, Andy Webster praised the film for the "consummate dexterity" with which it employed worn-out horror devices. 

==Home media==
The film was released on DVD & Blu-Ray on May 28, 2013.

==See also==
 

==References==
 

==External links==
*  
*  
*  
*  
*   at filmfutter.com

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 