Hilary and Jackie
 
 
{{Infobox film
| name = Hilary and Jackie
| image = Hilary-and-jackie-poster.jpg
| caption = US poster for the film
| director = Anand Tucker
| producer = Nicolas Kent Andy Paterson
| screenplay = Frank Cottrell Boyce
| based on =   
| starring = Emily Watson Rachel Griffiths James Frain David Morrissey
| music = Barrington Pheloung
| cinematography = David Johnson Martin Walsh Channel 4 Films (UK) October Films (US)
| released =  
| runtime =
| country = United Kingdom
| language = English
| budget =
| gross = $4,912,892 (US) 
}}
Hilary and Jackie is a 1998 British biographical film directed by Anand Tucker. The screenplay by Frank Cottrell Boyce is often claimed to have been based on the memoir A Genius in the Family (later republished under the title Hilary and Jackie) by Piers and Hilary du Pré,  which chronicles the life and career of their late sister, cellist Jacqueline du Pré. 

In fact it was not, as the book had not been written,  an Inside Film programme, handed out at early showings of the film quotes screenwriter Frank Cottrell Boyce as saying,"Hilary was working on the book at the same time as I was working on the film ... it was at a very early stage when we were doing the script."  The film was in fact based on conversations with Hilary and Piers, and unlike the book does not claim to be the true story and contains some made-up incidents. This it important, as the film attracted controversy and criticism for allegedly distorting details in Jacquelines life, although Hilary du Pré publicly defended her version of the story.      

Emily Watson was nominated for the Academy Award for Best Actress and Rachel Griffiths was nominated for the Academy Award for Best Supporting Actress. 

==Plot==
The film is divided into two sections, the first telling events from Hilarys point of view and the second from Jackies. It opens with Hilary and Jackie as children being taught by their mother to dance and play musical instruments, the cello for Jackie and the flute for Hilary. Jackie does not take practising seriously at first, but when she does, she becomes a virtuoso, quickly rising to international prominence. Marriage to pianist and conductor Daniel Barenboim follows.

Hilary, on the other hand, plays in a community orchestra and marries Christopher Finzi, the son of composer Gerald Finzi. The film, though focused primarily on Jacqueline, is ultimately about the relationship between the two sisters and their dedication to one another; to help Jacqueline through a nervous breakdown and in the interest of therapy, Hilary consents to Jacqueline having an affair with her (Hilarys) husband.

The last quarter of the movie chronicles in detail the last fifteen years of Jacquelines life: she is diagnosed with multiple sclerosis, loses control of her nervous system, becomes paralysed, goes deaf and mute, and finally dies. The film ends with Jacquelines spirit standing on the beach where she used to play as a child, watching herself and her sister frolicking in the sand as little girls.

==Cast==
* Emily Watson as Jacqueline du Pré
* Rachel Griffiths as Hilary du Pré
* James Frain as Daniel Barenboim
* David Morrissey as Christopher Finzi
* Charles Dance as Derek du Pré
* Celia Imrie as Iris du Pré
* Rupert Penry-Jones as Piers du Pré Bill Paterson as William Pleeth
* Auriol Evans as Young Jackie
* Keeley Flanders as Young Hilary
* Nyree Dawn Porter as Dame Margot Fonteyn
* Vernon Dobtcheff as Professor Bentley
* Helen Rowe as Instrumentalist

==Production== Blue Coat County Sessions House, Georges Dock, St. Georges Hall, Liverpool|St. Georges Hall, and the Walker Art Gallery in Liverpool. Additional scenes were filmed at the Royal Academy of Music and Wigmore Hall in London, and most interiors were shot at the Shepperton Studios in Surrey. Brithdir Mawr, an ancient house in North Wales, was used for location shots of Hilarys house. 

Classical pieces performed in the film include compositions by Edward Elgar, Joseph Haydn, Johann Sebastian Bach, Johannes Brahms, César Franck, Matthias Georg Monn, Georg Friedrich Händel, Robert Schumann, Ludwig van Beethoven, and Antonín Dvořák. Jacqueline du Prés cello in the movie was played and synchronised to Emily Watsons movements by Caroline Dale.

==Critical reception==
In his review in The New York Times, Stephen Holden called the film "one of the most insightful and wrenching portraits of the joys and tribulations of being a classical musician ever filmed" and "an astoundingly rich and subtle exploration of sibling rivalry and the volcanic collisions of love and resentment, competitiveness and mutual dependence that determine their lives." He went on to say "Hilary and Jackie is as beautifully acted as it is directed, edited and written." 

Roger Ebert of the Chicago Sun-Times described it as "an extraordinary film   makes no attempt to soften the material or make it comforting through the cliches of melodrama." 

In the San Francisco Chronicle, Edward Guthmann stated, "Watson is riveting and heartbreaking. Assisted by Tuckers elegant direction and Boyces thoughtful, scrupulous writing, she gives a knockout performance." 

Anthony Lane of The New Yorker said, "The sense of period, of ungainly English pride, is funny and acute, but the movie mislays its sense of wit as the girls grow up. The nub of the tale... feels both overblown and oddly beside the point; it certainly means that Tucker takes his eye, or his ear, off the music. The whole picture, indeed, is more likely to gratify the emotionally prurient than to appease lovers of Beethoven and Elgar." 

Entertainment Weekly rated the film A− and added, "This unusual, unabashedly voluptuous biographical drama, a bravura feature debut for British TV director Anand Tucker, soars on two virtuoso performances: by the rightfully celebrated Emily Watson . . . and by the under-celebrated Rachel Griffiths." 

Rana Dasgupta wrote in an essay about biographical films that "the films tagline – The true story of two sisters who shared a passion, a madness and a man – is a good indication of its prurient intent. The books moving account of love and solidarity, whose characters are incomplete and complex but not "mad", is rejected in favour of a salacious account of social deviance." 

==Controversy and protests==
Although the film was a critical and box-office success, and received several Academy Award nominations, it ignited a furore, especially in London, centre of du Pres performing life. A group of her closest colleagues, including fellow cellists Mstislav Rostropovich and Julian Lloyd Webber, sent a "bristling" letter to The Times in February 1999.      

Clare Finzi, Hilary du Prés daughter, charged that the film was a "gross misinterpretation, which I cannot let go unchallenged."  Daniel Barenboim said, "Couldnt they have waited until I was dead?"    

Hilary du Pré wrote in The Guardian, "At first I could not understand why people didnt believe my story because I had set out to tell the whole truth. When you tell someone the truth about your family, you dont expect them to turn around and say that its bunkum. But I knew that Jackie would have respected what I had done. If I had gone for half-measures, she would have torn it up. She would have wanted the complete story to be told."  Jay Fielden reported in The New Yorker that shed said, "When you love someone, you love the whole of them. Those who are against the film want to look only at the pieces of Jackies life that they accept. I don’t think the film has taken any liberties at all. Jackie would have absolutely loved it."   Subscription required. 

==Awards and nominations==
* Academy Award for Best Actress (Emily Watson, nominee)
* Academy Award for Best Supporting Actress (Rachel Griffiths, nominee)
* Golden Globe Award for Best Actress - Motion Picture Drama (Watson, nominee) BAFTA Alexander Korda Award for Best British Film (nominee)
* BAFTA Award for Best Actress in a Leading Role (Watson, nominee)
* BAFTA Award for Best Adapted Screenplay (nominee)
* BAFTA Award for Best Sound (nominee) BAFTA Anthony Asquith Award for Film Music (nominee)
* British Independent Film Awards for Best Actress (Watson, winner)
* British Independent Film Awards for Best Director (winner)
* British Independent Film Awards for Best Actress (Griffiths, nominee)
* British Independent Film Awards for Best British Film (nominee)
* London Film Critics Circle ALFS Award for British Actress of the Year (Watson, winner)
* Satellite Award for Best Actress - Motion Picture Drama (Watson, nominee)
* Satellite Award for Best Adapted Screenplay (nominee)
* Screen Actors Guild Award for Outstanding Performance by a Female Actor in a Leading Role - Motion Picture (Watson, nominee)
* Screen Actors Guild Award for Outstanding Performance by a Female Actor in a Supporting Role - Motion Picture (Griffiths, nominee)

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 