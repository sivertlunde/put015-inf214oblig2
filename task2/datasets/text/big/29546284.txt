Kanmadam
{{multiple issues|
 
 
}}

{{Infobox film
| name           = Kanmadam
| image          = Kanmadam (1998).jpg
| image_size     = 
| alt            = 
| caption        = DVD cover
| director       = A. K. Lohithadas
| producer       = Suchithra Mohanlal 
| writer         = A. K. Lohithadas
| narrator       =  Lal  Mala Aravindan
| music          = Raveendran S. P. Venkatesh  (background score) 
| cinematography = Ramachandra Babu  
| editing        = A. Sreekar Prasad  Pranavams International
| distributor    = Pranavam Arts
| released       =  
| runtime        = 162 minutes
| country        = India
| language       = Malayalam
| genre          = Drama
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 1998 Malayalam Malayalam musical Siddique appear in guest roles. The music was composed by Raveendran while the background score was by S. P. Venkatesh.  The cinematography was handled by Ramachandra Babu and the editing was by A. Sreekar Prasad. The film was produced and distributed by Mohanlals production company Pranavam Arts.

==Plot==
A thug, Vishwanathan, leaves the city to the village where a deceased man named Damodaran lived. Damodarans grandmother was waiting for her grandsons return, unaware of his death. Vishwanathan falls in love with a high-tempered young woman named Bhanu before his best friend Johny came for him. But would Vishwanathan reveal the truth to Bhanu that, he and his friend Johny accidentally killed Damodaran.

==Cast==
*Mohanlal ...  Vishwanathan
*Manju Warrier ...  Bhanu
*Lal ...  Johnny
*Mala Aravindan ...  Swami Velayudhan
*Sreejaya ...  Suma
*Dhanya Menon ... Bhanus younger sister Siddique ...  Damodaran
*K. P. A. C. Lalitha ...  Yasodha Gopalakrishnan
*Cochin Hanifa ...  Police officer
*Usharani
* Gayathri
* Mahima

==Soundtrack==

This film includes 5 songs written by Gireesh Puthenchery and composed by   Raveendran . The background music is done by S. P. Venkatesh.

{| class="wikitable"
! Song !! Singers !! Raga
|-
| Doore Karalilurukumoru || K. J. Yesudas|Dr. K. J. Yesudas ||
|-
| Kaathoram Kannaaram || Sudeep Kumar, K. S. Chithra ||
|-
| Manjakkiliyude || K. J. Yesudas|Dr. K. J. Yesudas ||  Kharaharapriya
|-
| Manjakkiliyude || Radhika Thilak || Kharaharapriya
|-
| Moovanthi Thazhvarayil || K. J. Yesudas|Dr. K. J. Yesudas || Abheri
|-
| Thiruvaathira || M. G. Sreekumar, Radhika Thilak ||Arabhi, Shuddha Saveri
|-
| Thiruvaathira || Sudeep Kumar, Radhika Thilak || Arabhi, Shuddha Saveri
|}

==References==
 

==External links==
*  

 
 
 
 
 


 