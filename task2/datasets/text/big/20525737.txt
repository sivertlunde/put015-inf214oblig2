Garfield's Pet Force
{{Infobox film
| name        = Garfields Pet Force
| image       = Garfields Pet Force Coverart.png
| caption     = DVD cover
| director    = Mark A.Z. Dippé Kyung Ho Lee Jim Davis
| narrator   = Frank Welker
| starring    = Frank Welker Gregg Berger Audrey Wasilewski Jason Marsden Wally Wingert Vanessa Marshall John Davis Ash R. Shah
| music       = 
| editing     = Rob Neal Tom Sanders
| studio      = Paws, Inc. The Animation Picture Company Davis Entertainment
| distributor = 20th Century Fox Home Entertainment Lionsgate Home Entertainment (UK)
| released    =  
| runtime     = 76 minutes
| country     = United States South Korea
| language    = English
| budget      = 
}} Jim Davis comic strip Garfield and loosely based on the Pet Force novel series. It is the sequel to Garfields Fun Fest (2008). It is the final installment in the Garfield CGI Cartoon films. It was released on DVD and Blu-ray Disc on June 16, 2009. It was written by Garfield creator Jim Davis. In 2010 it was released in 3-D.

==Summary==
When Vetvix comes to Comic Strip World to go after Garzooka. Garzooka enlists Garfield, Nermal, Arlene, and Odie to help him stop Vetvix by becoming the Pet Force. However, Garfield would rather eat and sleep than help save the world. When Vetvix starts threatening Garfields world, Garfield realizes whats really at stake. Will Garfield stop being lazy and help the Pet Force, and save the world?

==Plot==
On the planet Dorkon,  s superhero counterpart), Odious (Odies superhero counterpart), Abnermal (Nermals superhero counterpart) and Starlena (Arlene (Garfield)|Arlenes superheroine counterpart).

The situation was revealed to be a comic book Nermal was reading during a cookout with the gang. Nermal is really excited about getting the next 100th edition issue. Garfields friends go to the Comic studio to work their new strip, except Garfield who wants to finish all the hot dogs. Nermal gets the new Pet Force issue from a news stand, with Garzooka jumping out of a comic book afterwards. Garzooka heads for Jons house and is told where Odie, Arlene, and Nermal are by Garfield. Garzooka gives Garfield the Klopman Crystal as well telling him to protect it. Curiously, the pages of the comic book show exactly what is going on in Comic Strip World with Garfield & his group, but pages of future incidents are blank until the incidents occur.

In the break room at the Comic Studio, Nermal, Arlene, and Odie notice Garzooka behind them. Garzooka hands them the serums asking them to help him stop Vetvix, but they dont change immediately after they drink the serums. However its time for Odie, Arlene, and Nermal to go to work and Garzooka follows. Meanwhile, the real Garfield is enjoying a relaxing day all to himself, but is captured by Vetvix (who appeared in Comic Strip World earlier) who tortures him for the Klopman Crystal, but to no avail (because Garfield is a cartoon character). Garfield tries to protect the Klopman Crystal, but it is taken by Vetvix and orders her guards to get rid of Garfield.
  
The real Garzooka and the Pet Force go to the antenna and use it to bring down Vetvixs ship. Meanwhile, the zombies chase Garfield and Wally to the Comic Studio, and Eli opens the pit in the filming area, in which the zombies fall into. Meanwhile at the tower, Vetvix shoots the Moscram Ray Gun using the super scramble mode at the Pet Force.

Emperor Jon and Professor Wally break free (by the Professors monocle that Garfield put on the windowsill earlier), and take over the ship, flying into the air, making Vetvix fall off. However, she makes a giant monster by shooting the ray gun at most of the buildings in the Comic Strip World, and decides to use the monster to destroy everything and get back her ship. Meanwhile, Vetvixs ship lands near the back alley of Comic Studio, and the Crazy Crew meet them, who let Garfield enter the ship. The ship flies above the monster and Garfield jumps off. By using the power of the Super Scrambled Pet Force and the dropped Moscram Ray Gun, Garfield defeats the monster along with unscrambling the Pet Force.

==Cast==
* Frank Welker as Garfield, Garzooka, Computer Voice, Dog, Keith, Kung Fu Guard, Lawyer, Monster, Spike Guard, and the Narrator
* Gregg Berger as Odie
* Audrey Wasilewski as Arlene
* Jason Marsden as Nermal
* Vanessa Marshall as Vetvix
* Wally Wingert as Jon Arbuckle and Emperor Jon
* Fred Tatasciore as Billy Bear, Horned Guard
* Greg Eagles as Eli
* Jennifer Darling as Betty, Bonita Stegman
* Stephen Stanton as Randy Rabbit, Skinny Guard, Newspaper Stand Guy
* Neil Ross as Wally, Professor Wally, Charles
 Jeff Fischer, David Michie, Paige Pollack and Ruth Zalduondo.

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 