'Blue Blazes' Rawden
{{Infobox film
| name           = Blue Blazes Rawden
| image          = File:Movie_poster_for_1918_silent_film_Blue_Blazes_Rawden.jpg|thumb|right|
| alt            = 
| caption        = Movie poster
| director       = William S. Hart
| producer       = Thomas H. Ince
| screenplay     = J.G. Hawks Robert McKim Robert Gordon Jack Hoxie
| music          = 
| cinematography = Joseph H. August
| editing        = 
| studio         = Artcraft Pictures Corporation William S. Hart Productions
| distributor    = Paramount Pictures
| released       =  
| runtime        = 65 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 drama silent Robert McKim, Robert Gordon and Jack Hoxie. The film was released on February 1, 1918, by Paramount Pictures.   The survival status of this film is preserved and is available in various viewing formats. 
 
==Plot==
 

==Cast==
*William S. Hart as Blue Blazes Rawden
*Maude George as Babette DuFresne Robert McKim as Ladyfingers Hilgard
*Gertrude Claire as Mother Hilgard Robert Gordon as Eric Hilgard
*Jack Hoxie as Joe La Barge

==Background==
 
Hart published a letter in the Motion Picture News (March 30, 1918), titled A straight-from-the shoulder letter, advising exhibitors that he was not connected with a company called "W.H. Productions Co.". Hart claimed that the company was changing titles of his films and showing previously made features of him without his permission. The letter states that "the old titles under which they were previously exhibited have been changed and new titles substituted...they are old pictures with new labels". Hart went on to say that, "if your patrons...are misled by new titles into a belief that these pictures are my latest productions, it will not only injure my reputation but also seriously affect the reputation of your theatre". Blue Blazes Rawden is listed at the bottom of the letter, along with his other films that bear the "Artcraft" trademark. 

==Reviews and reception==
A reviewer in Photoplay (1918) said that "Hart gives the best exhibition of his acting ability that I have ever yet seen". They also noted that the story "has no actual ending", but rather "is something of a slice of life...its interest lies in the struggle that goes on in the heart of the naturally ferocious, brutal Rawden, turned gentle by sheer determination".  A review in Variety (magazine)|Variety (1918) praised Hart saying, "  was a good actor long before he thought of going into pictures...he now reveals one more artistic accomplishment - that of an intelligent, careful, painstaking director. The detail in the preparation of the feature goes a long way toward enhancing its value".  The Motion Picture Magazine (1918), said that "one celluloidic thing is as inevitable as fates and taxes-that William S. Hart starts each new picture career as a bad man and ends it by being completely reformed...like the excess tax, it is an excess reformation". 

== References ==
 

== External links ==
*  
* 
 
 
 
 
 
 
 
 
 