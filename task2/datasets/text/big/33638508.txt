The Paper Man (film)
{{Infobox film
| name           = The Paper Man
| image          = 
| caption        = 
| director       = Ismael Rodríguez
| producer       = Ismael Rodríguez
| writer         = Luis Spota Ismael Rodríguez
| starring       = Ignacio López Tarso
| music          = 
| cinematography = Gabriel Figueroa
| editing        = 
| distributor    = 
| released       =  
| runtime        = 110 minutes
| country        = Mexico
| language       = Spanish
| budget         = 
}}
 Best Foreign Language Film at the 36th Academy Awards, but was not accepted as a nominee. 

==Cast==
* Ignacio López Tarso as Adán
* Alida Valli as La Italiana
* Susana Cabrera as La Gorda
* Guillermo Orea as Tendero
* Alicia del Lago as María
* José Ángel Espinoza as Torcuato (as Ferrusquilla)
* Famie Kaufman as Prostituta (as Vitola)
* Mario García Harapos as El Gorgojo (as Harapos)
* Dolores Camarillo
* Raúl Castell as Don Trini
* Tizoc Rodríguez
* Jana Kleinburg
* Carlos Ancira as Comisario

==Awards== Best Motion Golden Globes.   

==See also==
* List of submissions to the 36th Academy Awards for Best Foreign Language Film
* List of Mexican submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 