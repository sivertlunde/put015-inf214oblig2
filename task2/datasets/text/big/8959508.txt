Hardly a Criminal
{{Infobox film
| name           = Apenas un delincuente
| image          = Apenasundelincuente.jpg
| image_size     =
| caption        =
| director       = Hugo Fregonese
| producer       =
| writer         = Raimundo Calcagno  Israel Chas de Cruz
| narrator       =
| starring       = Tito Alonso
| music          =
| cinematography =
| editing        =
| distributor    =
| released       = 1949
| runtime        =
| country        = Argentina
| language       = Spanish
| budget         =
| preceded_by    =
| followed_by    =
}}
 Argentine crime drama directed by Hugo Fregonese and written by Raimundo Calcagno and Israel Chas de Cruz. Starring Tito Alonso.

==Cast==
*Tito Alonso Jorge Salcedo
*Homero Cárpena
*Guillermo Casali
*Sebastián Chiola
*Mario Cozza
*Rodolfo Crespi
*José De Angelis ....Jails Director
*Alfonso Ferrari Amores
*Josefa Goldar
*Tito Grassi ....Inspector
*Jacinto Herrera
*Gaby Jerrico
*Linda Lorena
*Raúl Luar
*Fausto Padín
*Alberto Peyrou
*Nathán Pinzón
*Anibal Romero
*Orestes Soriani ....Companys manager
*José A. Vázquez

==External links==
*http://www.imdb.com/title/tt0143110/

 
 
 
 
 
 
 


 
 