Little Hiawatha
 
{{Infobox Hollywood cartoon
| cartoon name = Little Hiawatha
| series = Silly Symphonies
| image =
| image size =
| alt =
| caption = David Hand
| producer = Walt Disney
| story artist =
| narattor = Sterling Holloway
| voice actor =
| musician = Albert Hay Malotte Frank Thomas
| layout artist =
| background artist =
| studio = Walt Disney Productions
| distributor = United Artists
| release date =   (USA)
| color process = Technicolor
| runtime =
| country = United States
| language = English
| preceded by = Woodland Café
| followed by = The Old Mill
}} 1937 animated cartoon produced by Walt Disney, inspired by the poem The Song of Hiawatha by Henry Wadsworth Longfellow. The film is currently seen on the 2012 Blu-ray of Walt Disneys Pocahontas (1995 film)|Pocahontas. It is the last Silly Symphony to be released by United Artists.

==Synopsis==
Over opening narration, Little Hiawatha is seen paddling his canoe down a river—at one point backwards—on his way to hunt game. Upon reaching land, he steps out and immediately falls down a hidden hole in the water, bringing about the laughter of the animals in the forest. Hiawatha gives chase to them—with his pants occasionally falling down in what is the cartoons running gag. He gives chase to the grasshopper, but is foiled when it spits in his face, which earns him more laughter from the animals. He chases them again and manages to corner a small rabbit, but finds he cannot bring himself to kill it after it gives him repeated sad glances, even after he has armed it with a spare bow and arrow. Frustrated, he shoos it back to its family and breaks his bow and arrow, to the animals delight.

Later Hiawatha comes across a set of bear tracks, which leads him to a face-to-face encounter with a bear cub. He chases after it, but runs right into the cubs hungry mother, who is enraged and pursues him through the forest. The other animals band together to keep Hiawatha out of the bears clutches, including scenes of opossums flinging Hiawatha through the air, and beavers cutting down trees in the bears path. Returned safely to his canoe, Hiawatha rows off into the sunset as the animals wave him farewell.

==External links==
*  

 

 
 
 
 
 
 
 

 