Theeraatha Bandhangal
{{Infobox film 
| name           = Theeraatha Bandhangal
| image          =
| caption        =
| director       = Dr Joshua
| producer       =
| writer         =
| screenplay     =
| starring       = Babu Joseph Salini
| music          = K. Raghavan
| cinematography =
| editing        =
| studio         = GJ Arts
| distributor    = GJ Arts
| released       =  
| country        = India Malayalam
}}
 1982 Cinema Indian Malayalam Malayalam film, directed by Dr Joshua. The film stars Babu Joseph and Salini in lead roles. The film had musical score by K. Raghavan.   

==Cast==
*Babu Joseph
*Salini

==Soundtrack==
The music was composed by K. Raghavan and lyrics was written by Poovachal Khader. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Edi Enthedi Raajamme || S Janaki, Kanakambaran || Poovachal Khader || 
|-
| 2 || Enthe Oru Naanam || P Susheela || Poovachal Khader || 
|-
| 3 || Saayam Sandhya || S Janaki || Poovachal Khader || 
|-
| 4 || Udayam Namukkiniyum || K. J. Yesudas || Poovachal Khader || 
|}

==References==
 

==External links==
*  

 
 
 

 