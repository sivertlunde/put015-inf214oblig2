Dust in the Wind (film)
{{Infobox film
|  name               = Dust in the Wind
|  original_name      = 戀戀風塵
|  image              = Dust in the wind(1986 film) poster.jpg
|  caption            = Theatrical release poster
|  director           = Hou Hsiao-Hsien 
|  producer           = 
|  writer             = Chu Tien-wen Wu Nien-jen
|  starring           = Wang Chien-wen Xin Shufen Li Tian-lu
|  music              = Chen Ming-Chang
|  movie_music        = 
|  cinematography     = Mark Lee Ping-Bin
|  editing            = 
|  production_company = Central Motion Pictures Corporation
|  distributor        = Central Motion Pictures Corporation
|  released           = 1986
|  runtime            = 109 minutes
| country        = Taiwan Taiwanese
}} 1986 critically acclaimed but commercially unsuccessful movie by Taiwanese filmmaker Hou Hsiao-Hsien.

This film is based on co-screenwriter Wu Nien-jens own experiences, and the final part of Hous coming-of-age trilogy, the others being A Summer at Grandpas (1984) and The Time to Live and the Time to Die (1985).

==Synopsis==
A love story about a young couple from a village in the northern-east part of Taiwan. The boy, Ah-yuan goes to Taipei to work after graduating from junior high school so he can earn money to send home. The girl, Ah-yun follows him the next year and they work hard to earn enough money to marry. Then Ah-yuan must spend three years in the military and the girl marries someone else. Although Ah-yuan regrets what happened he does not blame Ah-yun.

==Cast==
* Wang Chien-wen as Ah-yuan
* Xin Shufen as Ah-yun
* Li Tian-lu as Ah-yuans grandfather

==Production==
The film features Li Tian-lu in the role of the grandfather.  Li became a central part of Hous major films.

==Critical reception==
 

== External links ==
*  

 

 
 
 
 
 
 

 
 