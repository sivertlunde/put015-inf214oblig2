Woman of the Port (1991 film)
{{Infobox film
| name           = Woman of the Port
| image          =
| caption        =
| director       = Arturo Ripstein Michael Donnelly Allen Persselin
| writer         = Guy de Maupassant Paz Alicia Garciadiego
| starring       = Damián Alcázar
| music          =
| cinematography = Ángel Goded
| editing        = Carlos Puente
| distributor    =
| released       =  
| runtime        = 110 minutes
| country        = Mexico
| language       = Spanish
| budget         =
}}

Woman of the Port ( ) is a 1991 Mexican drama film directed by Arturo Ripstein. It was screened in the Un Certain Regard section at the 1991 Cannes Film Festival.   

==Cast==
* Damián Alcázar as Marro
* Alejandro Parodi as Carmelo
* Juan Pastor as Simón
* Patricia Reyes Spíndola as Tomasa
* Evangelina Sosa as Perla
* Ernesto Yáñez as Eneas

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 
 