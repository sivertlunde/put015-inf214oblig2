On the Nose (film)
{{Infobox film name           =  On the Nose image          =  OnTheNosels10.jpg image_size     =  caption        = Theatrical poster producer       = Scott Kennedy Tristan Lynch director       = David Caffrey writer         = Tony Philpott starring       = Dan Aykroyd Robbie Coltrane music          = James Jandrisch cinematography = Paul Sarossy editing        = Roger Mattiussi			 distributor    = Highwire Entertainment released       = September 21, 2001 runtime        = 104 minutes country  Ireland Canada language  English
|imdb           = 
}} Canadian comedy film|comedy/fantasy film released in 2001. It was written by Tony Philpott and stars Dan Aykroyd and  Robbie Coltrane in a magical comedy where a long dead Australian aboriginals head is used to predict winners in horse races.  The movie was filmed in Dublin, Republic of Ireland|Ireland.

On the Nose won the Audience Award in 2002 at the Newport Beach Film Festival.

==Summary==
Robbie Coltrane plays a rehabilitated gambler who is tested beyond all limits when a head stored in the university turns out to accurately predict race horse winners. Robbie Coltrane finds he must hold off a representative from the university of Australia, who has come to reclaim the head and take it back to Australia, and the mob in order to raise money for his daughters tuition.

==Cast==
* Dan Aykroyd as Dr. Barry Davis
* Robbie Coltrane as Delaney
* Brenda Blethyn as Mrs.Delaney
* Tony Briggs as Michael Miller Jim Norton as Patrick Cassidy
* Sinead Keenan as Sinead Delaney Don Baker as Barclay
* Glynis Barber as Anthea Davis Francis Burke as Nana
* Mark Anthony Byrne as Anto (Uncredited)
* Simon Delaney as Grogan
* Peadar Flanagan as Race Commentator
* Una Kavanagh as Receptionist
* Laurence Kinlan as Kiaran Delaney
* Alvaro Lucchesi as Foley
* Eanna MacLiam as Seamus Boyle
* Una Minto as Woman
* Cathy Murphy as Dolores
* Owen OGorman as Teller
* Sean OShaughnessy as Liam
* Peter OSullevan as Grand National commentator
* Myles Purcell as Egert
* June Rogers as Eileen Casey
* Des Scahill as Race Commentator
* Zara Turner as Carol Lenahan

== External links ==
*  

 
 
 
 
 
 
 
 
 


 
 