Rascal (film)
{{Infobox film
| name           = Rascal
| image          = Rascal poster.jpg
| caption        = Theatrical release poster
| director       = Norman Tokar
| producer       = James Algar
| writer         = Harold Swanton
| based on       = Sterling North
| narrator        = Walter Pidgeon Steve Forrest Pamela Toll Buddy Baker
| cinematography = William E. Snyder
| editing        = Norman R. Palmer Walt Disney Productions Buena Vista Distribution
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = English
| gross          =
}}
 film adaption Walt Disney Productions based on a book Rascal (book)|Rascal by Sterling North about a young man and his pet raccoon set in California.

==Synopsis==
The movie is a dramatization of Sterling Norths 1963 "memoir of a better era." Born near Edgerton, Wisconsin, North was a former literary editor for newspapers in Chicago and New York City.  The movie relates a year in the life of young Sterling North (portrayed by Bill Mumy) and his "ringtailed wonder" pet raccoon, Rascal. Although set in Wisconsin, circa 1917, the movie was filmed in California.

==Cast==
* Bill Mumy - Sterling North
* The voice of Walter Pidgeon - Sterling North as an adult  Steve Forrest - Sterlings father, Willard
* Pamela Toll - his sister, Theo
* Elsa Lanchester - Mrs. Satterfield Henry Jones - Garth Shadwick
* Bettye Ackerman - Miss Whalen
* Johnathan Daly - Rev. Thurman
* John Fiedler - CY Jenkins
* Richard Erdman - Walt Dabbett
* Herbert Anderson - Mr. Pringle
* Robert Emhardt - Constable
* Steve Carlson - Norman Bradshaw
* Maudie Prickett - Miss Pince-Nez
* David McCallum - Ice Cream Man

==The book==
As is often the case, this film differs from its source material, the award-winning book Rascal (book)|Rascal. Norths sister, the poet and editor Jessica Nelson North, is not a character in the film.

==See also==
* Araiguma Rascal - a Japanese anime based on Rascal

==External links==
*  
* 
* 
* 

 

 
 
 
 
 
 


 