Gangster Exchange
 
{{Infobox film
| name           = Gangster Exchange
| image          = Gangster Exchange.jpg
| caption        = 
| director       = Dean Bajramovic
| producer       = Dean Bajramovic David Krae
| writer         = Dean Bajramovic
| starring       = Christopher Russell Nobuya Shimamoto Aaron Poole
| music          = Whitney Baker Dan Elliot Senad Senderovic
| cinematography = Kevin C.W. Wong
| editing        = Robert James Spurway
| studio         = Aquila Pictures Gothic Raygun Pictures
| distributor    = Cinemavault Releasing Peace Arch Entertainment Group
| released       =  
| runtime        = 
| country        = Canada
| language       = English
| budget         = 
| gross          = 
}}
Gangster Exchange is a 2009 film written and directed by Dean Bajramovic. The film debuted at the Beverly Hills Film Festival on April 3, 2009,  where it won the Audience Choice for Best Film. 

==Plot==
Japanese yakuza team up with Bosnian mobsters to import a toilet made of pure heroin into New York City.

==Cast==
*Christopher Russell as Marco 
*Nobuya Shimamoto as Hiro 
*Aaron Poole as Big Dave 
*Sarain Boylan as Kendra 
*Jasmin Geljo as Gogo Wolf 
*Zeljko Kecojevic as Dragan Wolf 
*Walter Alza as Sasha 
*Steven P. Park as Ozaki 
*Dennis Akayama as Takayama 
*David Krae as Snowy
*Daniel Park as Kitano

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 


 