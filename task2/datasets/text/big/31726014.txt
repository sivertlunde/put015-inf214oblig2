List of Ollywood films of 1992
 
A list of films produced by the Ollywood film industry based in Bhubaneshwar and Cuttack in 1992:

==A-Z==
{| class="wikitable"
|-
! Title !! Director !! Cast !! Genre !! Notes
|-
|   | 1992 in film|1992
|- Badshah || Prasenjit Chatterjee || || ||
|- Ghara Mora Sriram Panda, Mahasweta Roy, Mihir Das, Bijay Mohanty || ||
|- Paradesi Chadhei || Rabi Kinnagi || Uttam Mohanty, Mihir Das, Angelaa Chriss|| ||
|- Maa Jhara Saha || Kanhu Mohanty ||  || ||
|- Hisaba kariba Kalia || Sumanta Dey || || ||
|- Uttam Mohanty, Aparajita Mohanty || ||
|- Agni Sankat || Santunu Misra || || ||
|- Anti Churi Tanti Kate || Basant Sahu || || ||
|- Mukti Tirtha || Himanshu Das || || ||
|- Maa (1992 Maa || Prashanta Nanda ||Shrikant, Ritu Das, Rakhi Guljar || ||
|- Naga Panchami Naga Panchami || Bijay Bhaskar || Uttam Mohanty, Prasanjeet, Rutupurna Ghosh, Debashree Roy|| ||
|- Panjur Bhitare Avtar Singh || || ||
|- Preeti Ra Iti || Raghu Misra || || ||
|- Sukha Sansara|| Biresh Chatterji || || ||
|- Tara (1992 film)|Tara|| Bijaya Jena || Bijaya Jena, Sunil Chourasia, Anita Das|| ||
|- Udandi Sita|| Avatar Singh ||  || ||
|- Bhinna Samaya|| Man Mohan Mahapatra ||  || ||
|-
|}

==References==
 

 
 
 

 
 
 
 