The Woman in Green
 
{{Infobox film
| name           = The Woman in Green
| image          = The Woman in Green - 1945 - Poster.png
| image_size     =
| caption        = 1945 Theatrical Poster
| director       = Roy William Neill
| producer       = Roy William Neill
| screenplay     = Bertram Millhauser
| based on       = characters created by Sir Arthur Conan Doyle
| narrator       = 
| starring       = Basil Rathbone Nigel Bruce
| music          = Mark Levant
| cinematography = Virgil Miller
| editing        = Edward Curtiss
| distributor    = Universal Studios
| released       =  
| runtime        = 68 min
| country        = United States
| language       = English
}}

 
The Woman in Green is a 1945 American Sherlock Holmes film starring Basil Rathbone as Holmes and Nigel Bruce as Dr. Watson, with Hillary Brooke as the woman of the title and Henry Daniell as Professor Moriarty. The film is not credited as an adaptation of any of Sir Arthur Conan Doyles Holmes tales, but several of its scenes are taken from "The Final Problem" and "The Adventure of the Empty House." The Woman in Green is the eleventh film of the Sherlock Holmes (1939 film series)|Rathbone/Bruce series of 14.

==Plot==
When several women are murdered and their forefingers severed, Holmes and Watson are called into action, but Holmes is baffled by the crimes at the start. Widower Sir George Fenwick (Paul Cavanagh), after a romantic night alone with his girlfriend Lydia Marlowe (Hillary Brooke), is hypnotized into believing that he is responsible for the crimes. He is certain that he is guilty after he awakes from a stupor and finds a womans forefinger in his pocket. His daughter comes to Holmes and Watson without realizing that Moriartys henchman is following her. She tells Holmes and Watson that she found her father burying a forefinger under a pile of soil. She has dug up the forefinger and shows it to them.

Fenwick is then found dead, obviously murdered by someone to keep him from talking. Holmes theorizes that Moriarty, who was supposed to have been hanged in Montevideo, is alive and responsible for the crimes. Watson is then called to help a woman who fell over while feeding her pet bird. He leaves, and minutes later, Moriarty appears and explains that he faked the phone call so he could talk to Holmes. When Moriarty leaves, Watson arrives. Holmes explains what Moriarty did, notices that a window shade that was shut in the empty house across the street is now open, and tells Watson to investigate.

Inside the empty house Watson, looking through the window, believes that he sees a sniper shoot Holmes in his apartment. Holmes then appears at the house and explains that he put a bust of Julius Caesar there because of the busts resemblance to his own face (Holmes realized that as soon as he sat there, Moriarty would have him killed). Inspector Gregson takes the sniper, a hypnotized ex-soldier, away, but the sniper is later killed on Holmess doorstep.

Holmes now realizes that Moriartys plan involves:
:1) killing women and cutting off their forefingers,
:2) making rich, single men believe they have committed the crime,
:3) using this fake information to blackmail them, and
:4) counting on the victims being too terrified to expose the scheme.

He befriends Lydia, whom he had seen with Sir George at a restaurant, suspecting that she is in cahoots with Moriarty. She takes him to her house, where he is apparently hypnotized. Moriarty enters and has one of his men cut Holmes with a knife to verify that he is hypnotized. He then tells Holmes to write a suicide note (which he does), walk out of Lydias apartment onto the ledge, and jump to his death.

Watson and the police then appear and grab the criminals. Holmes then reveals he was never really hypnotized, but secretly ingested a drug to make him appear as if he had been hypnotized and also insensitive to pain. Moriarty then escapes from the hold of a policeman and jumps from the top of Lydias house to another building. However, he hangs onto a pipe which becomes loose from the building, causing him to fall to his death.

==Cast==
* Basil Rathbone as Sherlock Holmes
* Nigel Bruce as Doctor Watson
* Hillary Brooke as Lydia Marlowe
* Henry Daniell as Professor Moriarty
* Paul Cavanagh as Sir George Fenwick Matthew Boulton as Inspector Tobias Gregson
* Eve Amber as Maude Fenwick
* Frederick Worlock as Doctor Onslow 
* Tom Bryson as Corporal Williams
* Sally Shepherd as Crandon, Marlowes maid Mary Gordon as Mrs. Hudson

==See also== Hypnosis in popular culture

==References==
 

==External links==
 
*  
*  
*  
*  
*  
{{ external media align  = right width  =  video1 =  
}}

 
 

 
 
 
 
 
 
 
 
 
 
 
 