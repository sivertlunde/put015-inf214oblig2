Dante's Inferno: Abandon All Hope
  }}
{{Infobox film
| name           = Dantes Inferno: Abandon All Hope
| image          = Dantes Inferno- Abandon All Hope.jpg
| alt            =  
| caption        = 
| director       = Boris Acosta
| producer       = Boris Acosta
| based on       =  
| narrator       = Jeff Conaway
| starring       = Vittorio Matteucci Vincent Spano Armand Mastroianni Jenn Gotzon Lalo Cibelli Nia Peeples Diane Salinger Martin Kove Jsu Garcia  Stella Stevens
| music          = Aldo De Tata
| cinematography = 
| editing        = Angelo Acosta
| studio         = 
| distributor    = Dantes Inferno Documentary, LLC
| released       =  
| runtime        = 40 minutes
| country        = United States
| language       = English Italian
| budget         = 
| gross          = 
}}
Dantes Inferno: Abandon All Hope is a black and white film produced and directed by Boris Acosta. The story is based on the first part of Dante Alighieris Divine Comedy - Inferno (Dante)|Inferno.

==Plot==
This film is a narrative journey from Dantes own hand, through the worst of the afterlife, Inferno. It is a chronological descent to the deepest of Hell, circle by circle to the exit into Purgatory. It features most of Gustave Dores lithograph illustrations and some excerpts of the 1911 feature film "LInferno".

==Cast==
*Dino Di Durante - Introduced by
*Vittorio Matteucci - Dante
*Lalo Cibelli - Virgil
*Jenn Gotzon - Beatrice
*Gianmario Pagano - Charon, The Ferryman
*Jeff Conaway - Circles Introduction
*Zan Calabretta - 2nd Circle, The Lustful
*Amy Lucas - 3rd Cirble, The Gluttons
*Diane Salinger - The Furies
*Vanna Bonta - 6th and 7th Circles
*Vincent Spano - 7th Circle, The Murderers
*Jsu Garcia - 7th Circle, The Blasphemers
*Leslie Garza - 8th Circle, The Hypocrites
*Stella Stevens - 8th Circle, The Thieves
*Paul Lynch (director) - 8th Circle, The Falsifiers
*Martin Kove - 8th Circle, The Sowers of Discord
*Armand Mastroianni - 9th Circle, The Traitors
*Nia Peeples - Exit into Purgatory

==References==
*An early version of this film was introduced as a work-in-progress at the Market of the Cannes Film Festival in 2008

==External links==
*  
*  

 
 
 
 
 
 
 
 


 