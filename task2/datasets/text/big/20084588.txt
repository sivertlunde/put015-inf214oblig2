Poliche
{{Infobox film
| name           = Poliche
| image          = 
| image_size     = 
| caption        = 
| director       = Abel Gance
| producer       = Maurice Orienter
| writer         = Henry Bataille Henri Decoin Abel Gance
| narrator       = 
| starring       = Constant Rémy
| music          = 
| cinematography = Roger Hubert
| editing        = Lothar Wolff
| distributor    = 
| released       = 1934
| runtime        = 
| country        = France 
| language       = French
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

Poliche is a 1934 French drama film directed by Abel Gance.      

==Cast==
* Constant Rémy - Didier Méreuil, dit Police
* Marie Bell - Rosine
* Edith Méra - Mme Laub
* Violaine Barry
* Romain Bouquet - Boudier
* Alexander DArcy - Saint-Wast (as Alexandre Darcy)
* Betty Daussmond - Augustine
* Marcel Delaître - Prosper Méreuil
* Pierre Finaly - Laub
* Catherine Fonteney - Louise Méreil (as Catherine Fontenay)
* Marguerite Mahé - Laccordéoniste

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 