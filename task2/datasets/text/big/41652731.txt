Kanithan
{{Infobox film
| name           = Kanithan
| image          = Kanithan poster.jpg
| alt            =  
| caption        = First look poster
| director       = T. N. Santhosh
| producer       = S. Thanu
| writer         = T. N. Santhosh
| starring       = Atharvaa Catherine Tresa Tarun Arora
| music          = Sivamani
| cinematography = Arvind Krishna
| editing        = Bhuvan Srinivasan
| studio         = S. Thanu|V. Creations
| distributor    = V. Creations
| released       =  
| runtime        = 
| country        = India
| language       = Tamil
| budget         =  
| gross          =
}}
Kanithan (English: Mathematician) is an upcoming Tamil film directed by Santhosh and produced by S. Thanu. The film features Atharvaa and Catherine Tresa in the lead roles, while Sivamani composes the films music. The film is currently in production.      

==Cast==
*Atharvaa
*Catherine Tresa
*Tarun Arora
*K. Bhagyaraj
*Manobala Karunakaran
*Sunder Ramu
*Aadukalam Naren
*Sathyaraj
*Nassar
* Aadhira
*Y. G. Mahendra

==Production==
The project materialised in October 2013, with producer S. Thanu revealing that Atharvaa and Catherine Tresa were signed on by his production house to feature in a film directed by Santhosh, with music by Sivamani and camera work by Arvind Krishna.    The shoot began progressing in December 2013, with a first look poster being revealed the following month.   

== References ==
 

 
 
 


 