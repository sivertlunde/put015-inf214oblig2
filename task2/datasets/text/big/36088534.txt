Western Jamboree
{{Infobox film
| name           = Western Jamboree
| image          = Western_Jamboree_Poster.jpg
| border         = yes
| caption        = Theatrical release poster
| director       = Ralph Staub
| producer       = Harry Grey (associate)
| screenplay     = Gerald Geraghty
| story          = Patricia Harper
| starring       = {{Plainlist|
* Gene Autry
* Smiley Burnette
* Jean Rouverol
}}
| music          = Raoul Kraushaar (supervisor)
| cinematography = William Nobles
| editing        = Lester Orlebeck
| studio         = Republic Pictures
| distributor    = Republic Pictures
| released       =  
| runtime        = 54 minutes
| country        = United States
| language       = English
| budget         = $55,462 
}} Western film directed by Ralph Staub and starring Gene Autry, Smiley Burnette, and Jean Rouverol. Magers 2007, p. 124.  Based on a story by Patricia Harper, the film is about a singing cowboy who goes up against a gang of outlaws who are looking to steal the valuable helium gas beneath the cowboys ranch.    Magers 2007, p. 125. 

==Plot==
Ranch foreman Gene Autry (Gene Autry), his sidekick Frog Milhouse (Smiley Burnette), and the rest of the men at the Circle J ranch are anxious about the new owner, Van Fleet, who recently purchased the property. While awaiting his arrival, they learn that outlaws are searching for a helium gas floe on the property—gas they can sell to foreign powers for use in Airship|dirigibles, which are banned by the United States government. After one of the ranch hands is attacked, Gene and Frog go after the outlaws. At an abandoned house near the trail they find old Dad Haskell (Frank Darien), who tells Gene that his daughter Betty (Jean Rouverol) believes that he is the owner of the Circle J and is due to arrive from the east. Gene agrees to help maintain his charade and turns the Circle J into a dude ranch until the real owner arrives.

When Betty arrives with her fiancé, Walter Gregory (George Walcott), and his mother (Margaret Armstrong), Gene pretends to save her from Frog who poses as an Indian. She gets angry at Gene when she finds out it is just a gag. That night, Betty tells her father that Mrs. Gregory allowed her to charge $1,500 worth of clothing to her account, and she needs to pay the money back soon.

The next day, while continuing to track the outlaws, Gene decides to follow Betty when she goes for a ride and they accidentally discover the source of the helium. The outlaws, who have been following Gene, also now know the location of the gas floe. One of the gang tries to purchase the ranch from Dad, thinking that he is the owner, he refuses, making Walter and his mother suspicious. Mrs. Gregory then decides to get the $1,500 directly from Dad, who writes her a bad check to settle the debt, thinking he can win the money back from her at cards.

Meanwhile, Gene grows suspicious of Randolph Kimball (Ben Hewlett), the chemist who once told the previous owner of the Circle J that the gas on the property was worthless. When Van Fleet finally arrives at his ranch, he is taken and tied up by Frog and ranch hand Joseph Frisco who want to keep him out of the way. Not knowing about their intentions, Gene comes across the incensed owner and frees him. After Doc Trimble (Harry Holman) wins enough money at cards to cover Dads check, Van Fleet shows up and angrily throws everyone off of his ranch, including Walter and his mother, who want nothing more to do with Betty and Dad.

While watching Frog blowing up balloons, Gene gets an idea and decides to fill an aeronautical balloon with helium and send Frog up in a basket. From the sky, Frog discovers the pipeline to the outlaws cave, and after Gene uses a bullet to puncture Frogs balloon, they head towards the hideout. Following a shootout with the gang, Gene captures most of the outlaws and apprehends Kimball as he attempts to escape in a truck. Impressed with Genes initiative, Van Fleet offers him the position of ranch manager, and Betty decides that Gene would make a much better husband than her former fiancé. 

==Cast==
* Gene Autry as Gene Autry
* Smiley Burnette as Frog Millhouse
* Jean Rouverol as Betty Haskell
* Esther Muir as Duchess
* Joe Frisco as Frisco
* Frank Darien as Dad Haskell
* Margaret Armstrong as Mrs. Gregory
* Harry Holman as Doc Trimble
* Edward Raquello as Don Carlos
* Ben Hewlett as Randolph Kimball
* Kermit Maynard as Ranch Hand Slim
* George Walcott as Walter Gregory
* Ray Teal as McCall
* Champion as Genes Horse (uncredited)    Magers 2007, p. 124–125. 

==Production==
===Filming and budget===
Western Jamboree was filmed October 18–27, 1938. The film had an operating budget of $55,462 (equal to $ }} today). 

===Stuntwork===
* Ken Cooper (Gene Autrys stunt double) 
* Duke Green
* Kermit Maynard
* Bill Yrigoyen 

===Soundtrack===
* "The Cowboys Dream (Roll On, Little Dogies, Roll On)" (D.J. OMalley) by Gene Autry and Cowboys in the opening scene
* "The Darktown Strutters Ball" (Shelton Brooks) by Joe Frisco (piano and dance) and Edward Raquello (violin) in the saloon
* "Cielito Lindo" (Traditional Mexican Ballad) by Gene Autry in Spanish, and Cowboys in the saloon
* "Balloon Song" (Gene Autry, Johnny Marvin, Fred Rose) by Smiley Burnette
* "My Bonnie (Lies Over the Ocean)" (H.J. Fuller)
* "Old Folks at Home (Swanee River)" (Stephen Foster)
* "Old November Moon" (Gene Autry, Johnny Marvin) by Gene Autry with backup by Cowboys
* "I Love the Morning" (Gene Autry, Johnny Marvin, Fred Rose) by Gene Autry, Smiley Burnette, Frankie Marvin and other Cowboys
* "When the Bloom Is on the Sage (Round Up Time in Texas)" (Fred Howard, Nat Vincent) by Gene Autry and saloon patrons, with Joe Frisco (piano) and Edward Raquello (guitar)
* "Paradise in the Moonlight" (Gene Autry, Fred Rose)    

==References==
;Citations
 
;Bibliography
 
*  
*  
*   

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 