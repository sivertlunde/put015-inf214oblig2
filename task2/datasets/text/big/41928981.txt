Bringing Up Father (1928 film)
{{Infobox film
| name =  Bringing Up Father 
| image =
| image_size =
| caption = Jack Conway
| producer =
| writer = Frances Marion   George McManus   Ralph Spence 
| narrator =
| starring = Marie Dressler   Polly Moran   J. Farrell MacDonald   Gertrude Olmstead 
| music = 
| cinematography = William H. Daniels 
| editing = Margaret Booth  
| studio = Metro-Goldwyn-Mayer 
| distributor = Metro-Goldwyn-Mayer 
| released = March 17, 1928
| runtime = 70 minutes
| country = United States English intertitles
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} silent comedy Jack Conway remade in Jiggs and Maggie films to be made. 

==Cast==
*   Marie Dressler as Annie Moore  
* Polly Moran as Maggie  
* J. Farrell MacDonald as Jiggs  
* Jules Cowles as Dinty Moore  
* Gertrude Olmstead as Ellen 
* Grant Withers as Dennis  
* Andrés de Segurola as The Count  
* Rose Dione as Mrs. Smith  
* David Mir as Oswald 
* Tenen Holtz as Ginsberg Feitelbaum

==References==
 

==Bibliography==
* Drew, Bernard A. Motion Picture Series and Sequels: A Reference Guide. Routledge, 2013.

 

==External links==
* 

 
 
 
 
 
 
 
 
 

 