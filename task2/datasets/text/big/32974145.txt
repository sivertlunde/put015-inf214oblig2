Silks and Saddles
 
{{Infobox film
| name           = Silks and Saddles
| image          = Queen of the turf - 1922 - newspaperad.jpg
| image_size     = 
| caption        = Newspaper advertisement for the American release.
| director       = John K. Wells
| producer       = John K. Wells 
| writer         = John K. Wells John Cosgrove
| narrator       = 
| starring       = Brownie Vernon Robert MacKinnon
| music          = 
| cinematography = Alfred William Burne
| editing        = Robert Bates John K. Wells
| studio         = Commonwealth Pictures
| distributor    = The Carroll Brothers
| released       = 5 March 1921 
| runtime        = 5,500 feet
| country        = Australia
| language       = Silent (English intertitles)
| budget         = ₤5,600   
| gross          = £50,000 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
Silks and Saddles is a 1921 Australian film set in the world of horse racing directed by John K. Wells.

The film is also known as Queen o Turf or Queen of the Turf in the USA.

== Plot summary ==
On the stud farm of Kangarooie, squatters daughter Bobbie wants her weak brother Richard to come home for her birthday, but her prefers the charms of the city, in particular the high society adventuress, Mrs Fane. Tubby Dennis OHara, who is in love with Bobbie, persuades Richard to come home and he brings Mrs Fane with him. OHara gives Bobbie his horse, Alert, as a present. Bobbie enters it in a race and Mrs Fane tries to stop her winning. Bobbie falls in love with a handsome man and rides Alert to victory. 

== Cast == Brownie Vernon as Bobbie Morton
*Robert MacKinnon as Richard Morton Jr John Cosgrove as Dennis OHara John Faulkner as Richard Morton Sr
*Tal Ordell as Phillip Droone
*Evelyn Johnson as Myra Fane Raymond Lawrence as Jeffrey Manners
*Gerald Harcourt as Toby Makin
*Tommy Denman as Dingo
*Kennaquhair (horse) as Alert

== Production ==
The film is one of the rare Australian movies to survive today almost in its entirety. It was made by Commonwealth Pictures, a company formed in October 1920 with Eric Griffin as managing director at a value of £10,000.  They hired John K. Wells to direct; he was an American who moved to Australia with Wilfred Lucas to work as an assistant director.

The movie was shot in and around Sydney, including at Randwick racecourse and at Camden, with interiors at E. J. Carrolls studio at Palmerston in Waverly.  Footage was taken involving an aeroplane, one of the first Australian movies to do so.  The champion race horse Kennaquhair appears.

==Reception==
Le Maistre Walker, who helped set up Commonwealth Pictures, later claimed the film earned £17,000 in Australasia, of which only £3,900 was returned to the company. The UK rights were sold for £3,000 and the American rights for $16,000. Walker says that the US distributors made £30,000 out of the film. However because of the associated costs, Commonwealth could only return 23/ of every £1 invested, and soon went out of business. 

The film was the victim of block booking in the US so was retitled and edited to make it seem as if it was set in Virginia. 

Wells appears to have never directed another feature film. Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, 103. 

== References ==
 

== External links ==
* 
* 
*  at National Film and Sound Archive

 
 
 
 
 
 
 