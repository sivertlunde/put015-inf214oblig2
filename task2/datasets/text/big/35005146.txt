Navrang
{{Infobox film
| name           = Navrang
| image          = Navrang.jpg
| image_size     =
| border         =
| alt            = 
| caption        = Film poster
| director       = V. Shantaram
| producer       =
| writer         =
| screenplay     = V. Shantaram
| story          = G.D. Madgulkar| based on       =   Mahipal Sandhya Sandhya
| music          = C. Ramchandra Bharat Vyas (lyrics)
| cinematography = Tyagraj Pendharkar
| editing        = Chintamani Borkar
| studio         =
| distributor    =
| released       = 1959 
| runtime        =
| country        = India
| language       = Hindi
| budget         =
| gross          =
}}

Navrang is a 1959  , 28 September 2008. 

== Plot ==
Diwakar is a poet and loves his wife Jamuna on everything. But Jamuna does not agree that Diwakar lives out of sheer poetry in a fantasy world and the real world less and less responsible. Diwakar goes so far that he is a fantasy woman in his wifes body creates what he calls Mohini.
Diwakar will soon become a recognized poet and Jamuna gives birth to a boy. Unfortunately, the happiness does not last long: Diwakar loses his job because of a critical songs against the British. Now he can no longer feed his sickly father nor his son, who is starving. All this makes Jamuna angry, but above all Diwakars growing obsession with Mohini.

As Jamuna decides to live apart from Diwakar, it is destroyed internally and no longer capable of proof. Jamuna slowly realizes that she can not live without Diwakar and forgives him.

==Cast and Crew ==

=== Cast === Sandhya  as  Jamuna/Mohini Mahipal  as  Divakar
*Keshavrao Date
*Chandrakant Gaur
*Vandana
*Ulhas
*Vatsala Deshmukh Agha
*Baburao Pendharkar
*Jeetendra

=== Crew ===
*Director: V. Shantaram
*Editor : Chintamani Borkar
*Banner : Rajkamal Kalamandir
*Cinematography : Tyagraj Pendharkar	
*Choreographer : Shyam Kumar
*Music Director : C. Ramchandra
*Lyrics : Bharat Vyas
*Audiographer : A. K. Parmar

== Music ==
{{Track listing
| collapsed       =
| headline        =
| extra_column    = Singer(s)
| total_length    =

| all_writing     =
| all_lyrics      = Bharat Vyas
| all_music       = C. Ramchandra

| writing_credits =
| lyrics_credits  =
| music_credits   =

| title1          = Aa Dil Se Dil Mila Le
| note1           =
| writer1         =
| lyrics1         = 
| music1          =
| extra1          = Asha Bhosle
| length1         =

| title2          = Adha Hai Chandrama
| note2           =
| writer2         =
| lyrics2         = 
| music2          =
| extra2          = Asha Bhosle, Mahendra Kapoor 
| length2         =

| title3          = Are Ja Re Hat Natkhat
| note3           =
| writer3         =
| lyrics3         =
| music3          =
| extra3          = Asha Bhosle, Mahendra Kapoor
| length3         =

| title4          = Kari Kari Kari Andhiyari
| note4           =
| writer4         =
| lyrics4         = 
| music4          =
| extra4          = Asha Bhosle, C. Ramchandra
| length4         =

| title5          = Kaviraaja Kavita Ke Mat Ab Kaan Marode
| note5           =
| writer5         =
| lyrics5         =
| music5          =
| extra5          = Bharat Vyas
| length5         =

| title6          = Rane De Re
| note6           =
| writer6         =
| lyrics6         = 
| music6          =
| extra6          = Asha Bhosle, Manna Dey, C. Ramchandra (Stimme)
| length6

| title7          = Shyamal Shyamal Baran
| note7           =
| writer7         =
| lyrics7         = 
| music7          =
| extra7          = Mahendra Kapoor
| length7         =

| title8          = Tum Mere Main Teri
| note8           =
| writer8         =
| lyrics8         = 
| music8          =
| extra8          = Asha Bhosle
| length8         =

| title9          = Tum Paschim Ho Hum
| note9           =
| writer9         =
| lyrics9         = 
| music9          =
| extra9          = C. Ramchandra
| length9         =

| title10         = Tum Saiyan Gulab Ke
| note10          =
| writer10        =
| lyrics10        = 
| music10         =
| extra10         = Asha Bhosle
| length10        =

| title11         = Tu Chhupi Hai Kahan
| note11          =
| writer11        =
| lyrics11        =
| music11         =
| extra11         = Asha Bhosle, Manna Dey
| length11        =

| title12         = Ye Mati Sabhi Ke Kahani
| note12          =
| writer12        =
| lyrics12        = 
| music12         =
| extra12         = Mahendra Kapoor
| length12        = 
}}

== Awards and Nominations ==
{| class="wikitable"
|-
! Year !! Category !! Cast/Crew member !! Status
|-
| colspan="4" |
*Filmfare Award
|- Best Editing || Chintaman Borkar ||  
|- 1960 || Best Sound || A. K. Parmar ||  
|- Best Director || V. Shantaram ||  
|-
|}

==References==
 

== External links ==
*  
*  

 

 
 
 
 