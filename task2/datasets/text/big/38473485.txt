Asad (film)
{{Infobox film
| name           = ASAD
| image          = ASAD filmstill1-1.jpg
| director       = Bryan Buckley
| producer       =  
| co-producer       = Matt Lefebvre
| writer         = Bryan Buckley
| starring       =  
| music         = Marcel Khalife
| cinematography = Scott Henriksen
| distributor = ShortsHD  
| editing       = Chris Franklin
| country        = United States South Africa
}}
 2012 Academy Award for Best Live Action Short Film. 
 Somali refugees living in South Africa, none of whom had any acting experience prior to production.   

After being nominated for an Academy Award the film was released along with all the other 15 Oscar-nominated short films in theaters by ShortsHD.      

==Plot==
Asad is a fisher boy in Somalia. His friends, led by Laban, are pirates; Asad feels as if he is suited for the pirate life. An old fisherman named Erasto would rather he stick to an honest fishing life as opposed to piracy. Asad has never been able to catch anything on his fishing trips and is beginning to give up hope. Erasto tries to lift his spirits with his largest catch of the day.

When Asad is taking home the fish with his friend various people on the street ask if he has caught it and he angrily tells them that he has not. His friend suggests that he should lie but Asad says that if he does he will be cursed forever and wont catch anything. As they are talking they run into a group of Somali rebels that ask where all the beautiful women in their town are. When Asads friend says he does not know they threaten to kill him; Asad offers them the fish to save his friends life.

The next day Asad goes out to the sea to meet with Erasto for a fishing trip and sees the old man injured by the boat. Since Asad had mentioned where the fish came from, the Somali rebels had come for him. Erastos arm was so badly injured that he could not fish. He tells Asad that the boys luck will change today and he will finally catch something.
 Persian cat, which he brings back to Erasto. Neither of them know what a cat is, but they say that it looks like a white lion. Erasto marks that Asads name means "lion". Asad names the cat Lionfish and takes it back to his village to take care of, his luck changed.

==Awards==

{| class="wikitable" style="text-align: center"
|----- bgcolor="#94bfd3"
| colspan=4 align=center | Awards for ASAD
|----- bgcolor="#94bfd3" 
!width="100"|Year
!width="250"|Association
!width="300"|Award Category
!width="150"|Status
|- 2013 in 2013
| Academy Awards
| Best Short Film - Live Action
|  
|- 2012 in 2012
| Tribeca Film Festival
| Best Narrative Short
|  
|-
| Los Angeles Film Festival
| Audience Award
|  
|-
| rowspan="2" | Austin Film Festival
| Audience Award
|  
|-
| Best Narrative Short
|  
|-
| New Orleans Film Festival
| Best Narrative Short Film
|  
|-
| Rhode Island International Film Festival
| Grand Prize – Best Short Film
|  
|-
| Traverse City Film Festival
| Special Jury Prize
|  
|-
| HollyShorts Film Festival
| Best Short Film
|  
|-
| One Lens Film Festival
| 1st Place and Social Justice Award
|  
|-
| Mill Valley Film Festival
| BAFTA/LA Award - Best Short Film
|  
|-
| San Jose International Film Festival
| Best of Fest
|  
|-
| Denver Film Festival
| Starz Peoples Choice Award for Short Film
|  
|-
| Boulder International Film Festival
| Best Short Film
|  
|}

==Festival Selections==
 
 
*Tribeca Film Festival
*Seattle International Film Festival
*Los Angeles Film Festival
*Palm Springs International Film Festival
*Traverse City Film Festival
*Rhode Island Film Festival
*HollyShorts Film Festival
*One Lens Film Festival
*Montreal World Film Festival
*Telluride Film Festival
*1 Reel Film Festival
*Atlantic Film Festival
*Raindance Film Festival
*Hamptons International Film Festival
*Mill Valley Film Festival
 
*New Orleans Film Festival
*Austin Film Festival
*Ellensburg Film Festival
*San Jose International Film Festival
*Foyle Film Festival
*Denver Film Festival
*Evolution Film Festival
*Santa Fe Film Festival
*Ashbury Shorts Film Festival
*Irvine Film Festival
*Boulder International Film Festival
*Mainly British Film Festival
*Sedona Film Festival
*San Diego Film Critics Society
*Boulder International Film Festival
 

== References ==
 

==External links==
* 

 

 
 