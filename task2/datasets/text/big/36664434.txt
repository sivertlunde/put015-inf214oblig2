Ride, Tenderfoot, Ride
{{Infobox film
| name           = Ride, Tenderfoot, Ride
| image          = Ride_Tenderfoot_Ride_Poster.jpg
| border         = yes
| caption        = Theatrical release poster Frank McDonald
| producer       = William Berke
| screenplay     = Winston Miller
| story          = {{Plainlist|
* Betty Burbridge
* Connie Lee
}}
| starring       = {{Plainlist|
* Gene Autry
* Smiley Burnette
* June Storey
}}
| music          = Raoul Kraushaar (supervisor)
| cinematography = Jack A. Marta
| editing        = Lester Orlebeck
| studio         = Republic Pictures
| distributor    = Republic Pictures
| released       =  
| runtime        = 65 minutes
| country        = United States
| language       = English
| budget         = $74,965 Magers 2007, p. 170. 
}} Western film Frank McDonald and starring Gene Autry, Smiley Burnette, and June Storey. Written by Winston Miller, based on a story by Betty Burbridge and Connie Lee, the film is about a singing cowboy who inherits a meat-packing plant and must face stiff competition from a beautiful business rival.    

==Plot==
Singing cowboy Gene Autry (Gene Autry) and his sidekick Frog Millhouse (Smiley Burnette) work on a ranch owned by Ann Randolph (June Storey). Gene is unaware that he has just inherited the Belmont Packing Company. While Gene and Frog take the cattle to market, Gene has an argument with Ann who fires them both, giving them one of the steers as back pay. Later the local sheriff, seeing a Randolph steer in the possession of the two cowboys, arrests Gene and Frog on suspicion of cattle rustling. Attorney Henry Walker (Forbes Murray), who has been searching for the singing cowboy, finally locates Gene at the jail and informs him of his inheritance.

After being released from jail, Gene takes possession of the Belmont Packing Company. Ann, who owns a rival packing company, had plans to merge the two companies under her ownership. Now she is dismayed to learn that the man she just fired is now her main business competitor. Anns conniving general manager and fiancé, Donald Gregory (Warren Hull), convinces her to feign romantic interest in Gene and sweet talk him into selling his company to her. At first the plan appears to work, and Gene agrees to Anns offer and signs a contract of sale. Later, when he learns that Gregory plans to close the plant putting all his employees out of work, Gene tears up the contract and decides to stay in the packing business.

Gene soon learns that his biggest business challenge is having enough cattle to fill the distribution demands. He initiates a campaign to convince the ranchers to sell their stock to his Belmont Packing Company, and soon the contracts start coming in. Ann responds with her own campaign, however, appealing to ranchers with a "helpless woman" routine. When he notices her success, Gene changes tactics and starts a new campaign, singing to the ranchers and organizing parades in an effort to win their business, and the campaign succeeds.
 Mary Lee), who has a crush on Gene, overhears Gregorys men plotting to dynamite the dam and flood the valley. After she warns Gene of Gregorys scheme, Gene rides off and intercepts Gregorys henchmen before they can plant their explosives. Soon after, Gregory is indicted for sabotage, and Gene and Ann form a business alliance as well as a romantic relationship. 

==Cast==
* Gene Autry as Gene Autry
* Smiley Burnette as Frog Millhouse
* June Storey as Ann Randolph Mary Lee as Patsy Randolph
* Warren Hull as Donald Gregory
* Forbes Murray as Attorney Henry Walker
* Joe McGuinn as Henchman Martin
* Joe Frisco as Haberdasher
* Isabel Randolph as Miss Spencer
* Herbert Clifton as Butler Andrews
* Mildred Shay as Stewardess
* Si Jenks as Sheriff
* Cindy Walker as Singer with The Pacemakers
* The Pacemakers as Singers
* Bob Burns as Deputy (uncredited)
* Fred Burns as Rancher Allen (uncredited)
* Fred Snowflake Toones as Train Porter (uncredited)
* Slim Whitaker as Rancher (uncredited)
* Hank Worden as Henry Haggerty (uncredited)
* Champion as Genes Horse (uncredited)    

==Production==

===Filming and budget===
Ride, Tenderfoot, Ride was filmed June 28 to July 12, 1940. The film had an operating budget of $74,965 (equal to $ }} today), and a negative cost of $74,443. 

===Stuntwork===
* Joe Yrigoyen (Gene Autrys stunt double)
* Jack Kirk (Smiley Burnettes stunt double)
* Nellie Walker (June Storeys stunt double) 

===Filming locations===
* Chatsworth Railroad Station, Chatsworth, Los Angeles, California, USA
* Corriganville Movie Ranch, Simi Valley, California, USA

* Lake Hemet, Riverside County, California, USA
* Palmdale, California, USA 

===Soundtrack===
* "Ride, Tenderfoot, Ride" (Richard A. Whiting, Johnny Mercer) by Mary Lee
* "Ride, Tenderfoot, Ride" by Gene Autry, Mary Lee, and June Storey
* "When the Works All Done This Fall" (D.J. OMalley) by Gene Autry
* "Eleven More Months and Ten More Days" (Fred Hall, Arthur Fields) by Gene Autry and Smiley Burnette in jail
* "Woodpecker Song" (Eldo Di Lazzaro, Bruno Cherubini, Harold Adamson) by Gene Autry and Mary Lee
* "That Was Me by the Sea" (Smiley Burnette) by Smiley Burnette
* "Leanin on the Ole Top Rail" (Charles Kenny, Nick Kenny) by Gene Autry
* "Oh! Oh! Oh!" (Gene Autry, Johnny Marvin) by The Pacemakers, Mary Lee, and Others
* "On the Range" (Gene Autry, Johnny Marvin) by Gene Autry and Cowhands Magers 2007, pp. 169–170.    

==References==
;Citations
 
;Bibliography
 
*  
*  
*   

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 