The File on Thelma Jordon
{{Infobox film
| name           = The File on Thelma Jordon
| image          = ThelmaJordonPoster.JPG
| alt            =
| caption        = Theatrical release poster
| director       = Robert Siodmak
| producer       = Hal B. Wallis
| screenplay     = Ketti Frings
| story          = Marty Holland Paul Kelly
| music          = Victor Young George Barnes
| editing        = Warren Low
| studio          = Hal Wallis Productions
| distributor    = Paramount Pictures
| released       =  
| runtime        = 100 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
The File on Thelma Jordon is a 1950 film noir directed by Robert Siodmak from a screenplay by Ketti Frings. It stars Barbara Stanwyck and Wendell Corey. 

==Plot==
Thelma Jordon late one night shows up in the office of married assistant district attorney Cleve Marshall with a story about prowlers and burglars. Before Cleve can stop himself, he and Thelma are involved in a love affair. But Thelma is a mysterious woman, and Cleve cant help wondering if she is hiding something.

When her rich Aunt Vera is found shot,  Jordon calls not the police but Marshall, who helps her cover up evidence that may incriminate her. When she emerges as the prime suspect, Marshall sabotages the prosecution. Thelma Jordon is acquitted. Her past, however, has begun to catch up with her.

Jordon acknowledges a relationship with Tony Laredo, who conceived the scheme for her to commit murder and inherit Veras jewels and money. Having a guilty conscience, Jordon causes a car accident that results in her accomplices death. As she lay dying herself, Jordon confesses the truth to the district attorney. She does not incriminate Marshall, who nevertheless tenders his resignation.

==Cast==
* Barbara Stanwyck as Thelma Jordon
* Wendell Corey as Cleve Marshall Paul Kelly as Miles Scott
* Joan Tetzel as Pamela Marshall
* Stanley Ridges as Kingsley Willis
* Richard Rober as Tony Laredo Gertrude W. Hoffmann as Aunt Vera Edwards

==Reception==

===Critical response===
When the film was released, the staff at Variety (magazine)|Variety magazine praised the film, and wrote, "Thelma Jordon unfolds as an interesting, femme-slanted melodrama, told with a lot of restrained excitement. Scripting   is very forthright, up to the contrived conclusion, and even that is carried off successfully because of the sympathy developed for the misguided and misused character played by Wendell Corey ... Robert Siodmaks direction pinpoints many scenes of extreme tension." 
 Time Out Double Indemnity, but which takes its tone, unlike Wilders film, not from Stanwycks glittering siren who courts her own comeuppance ("Judgement day, Jordon!"), but from the nondescript assistant DA she drives to the brink of destruction." 

The New York Times, in a 1950 review, gave a mixed review and noted "Thelma Jordon is, for all of its production polish, adult dialogue and intelligent acting, a strangely halting and sometimes confusing work." 

==References==
 

==External links==
*  
*  
*  
*  
*  

===Streaming audio===
*   on Screen Directors Playhouse: March 15, 1951

 

 
 
 
 
 
 
 
 
 
 
 
 