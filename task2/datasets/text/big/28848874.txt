Vektor (film)
 
{{multiple issues|
 
 
 
}}

 

Vektor is a postapocalyptic Croatian thriller produced and directed by Ivan Rogar in 2010.
The script was written by Josip Fackovic. Starring Danijela Tepic, Josip Fackovic,
Alan Hrehoric and Tamara Kefelja. Co-produced by Vedad Basic and Ivan Blazun.
The film favors drama, suppressed dark emotions and atmosphere over violence, and is presented in almost black & white, industrial band Kult Of Red Pyramid.

==Synopsis==
The world is in chaos...
A war for energents has emerged between the East and the West, creating massive climate changes by nuclear and biological warfare. After receiving a distressed phone call, Christine returns to her childhood home, hoping to find her father alive, accompanied by a lifetime friend and a secretive scientist.
Strange things start to happen after they find a woman named Elena, amnesic and spattered with blood...

==Production==
The film was produced with a very limited budget. It was shot on Canon XL2.
Production period lasted more than one year to complete. The film is scheduled to be released on VOD and DVD in fall 2010.
This is the third film directed by Ivan Rogar, among  .

==Premiere and Release==
Vektor premiered in Zagreb, September/October 2010. 
From May 2011., the film can be watched on YouTube in its entirety, on the official channel.

==Critical Reception==
 

==Sequel==
On 10 July 2011. the sequel to the film was announced, under the name Vektor: Terminal Apocalypse.

 
 
 

==References==
 

 
 
 