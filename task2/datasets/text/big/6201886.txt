The Winslow Boy (1948 film)
 
 
 
{{Infobox film
| name           = The Winslow Boy
| image          = Winslow Boy 1 .jpeg|image_size=250px
| caption        = Video cover
| director       = Anthony Asquith
| producer       = Anatole de Grunwald
| writer         = Terence Rattigan Anatole de Grunwald Anthony Asquith Sir Cedric Hardwicke Basil Radford Neil North Jack Watling Kathleen Harrison Hugh Dempster
| music          = William Alwyn (comp.) Dr. Hubert Clifford (dir.)
| cinematography = Osmond H. Borradaile Freddie Young
| editing        = Gerald Turney-Smith
| studio         = London Films
| distributor    = British Lion Films
| released       = 1948
| runtime        = 112 min.
| country        = United Kingdom
| language       = English
| budget         =
| gross = £211,383 (UK) 
| preceded by    =
| followed by    =
}}
The Winslow Boy is a 1948 film adaptation of Terence Rattigans play The Winslow Boy. It was made by De Grunwald Productions and distributed by the British Lion Film Corporation. It was directed by Anthony Asquith and produced by Anatole de Grunwald with Teddy Baird as associate producer. The screenplay was written by de Grunwald and Rattigan based on Rattigans play. The music score was by William Alwyn and the cinematography by Freddie Young.
 Sir Cedric 1999 film adaptation directed by David Mamet.

==Background== House of Commons. The play focuses on a refusal to back down in the face of injustice – the entire Winslow family, and the barrister who represents them (Sir Robert Morton), make great sacrifices in order that right be done.
 Edward Carson Solicitor General accepted that Archer-Shee was innocent, and ultimately the family was paid compensation. George Archer-Shee died in the First World War and his name is inscribed on the war memorial in the village of Woodchester in Gloucestershire where his parents lived. There is no real world counterpart to the character of Catherine, although she is central to the plot of the play and films.

==Plot== Royal Naval Sir Cedric Hardwicke), is requested to remove his son from the college. Unwilling to accept the verdict, Winslow and his daughter Catherine institute their own enquiries and engage a friend and family solicitor, Desmond Curry (Basil Radford) to assist them, including the briefing of the best barrister in England at the time, Sir Robert Morton (Robert Donat), should the case come to court.

The government is unwilling to allow the case to proceed, but after heated debates in the  ) has been forced to leave Oxford due to the lack of money, Catherine (Margaret Leighton) loses her marriage settlement and subsequently her fiancé, John Watherstone (Frank Lawton), and Arthur Winslow loses his health.

==Cast==
 
 
* Robert Donat as Sir Robert Morton
* Cedric Hardwicke as Arthur Winslow
* Basil Radford as Desmond Curry
* Margaret Leighton as Catherine Winslow
* Kathleen Harrison as Violet
* Francis L. Sullivan as Attorney General
* Marie Lohr as Grace Winslow
* Jack Watling as Dickie Winslow
* Walter Fitzgerald as First Lord
* Frank Lawton as John Watherstone
* Neil North as Ronnie Winslow Nicholas Hannen as Colonel Watherstone
* Hugh Dempster as Agricultural Member
* Evelyn Roberts as Hamilton MP
 
* W.A. Kelley as Brian ORourke
* Edward Lexy as First Elderly Member Gordon McLeod as Second Elderly Member
* Marie Michelle as Mrs. Curry
* Mona Washbourne as Miss Barnes
* Ivan Samson as Captain Flower
* Kynaston Reeves as Lord Chief Justice
* Charles Groves as Clerk of the Court
* Ernest Thesiger as Mr. Ridgeley Pierce
* Vera Cook as Violets friend
* Stanley Holloway as Comedian
* Cyril Ritchard as Music Hall Singer
* Noel Howlett as Mr. Williams (uncredited)
* Wilfrid Hyde-White as Wilkinson (uncredited)
 

==Differences from the play==
Unlike the play and the David Mamet The Winslow Boy (1999 film)|remake, the 1948 film shows the actual trial, while in other versions, the trial occurs offstage and the audience is told (but not shown) what occurred during it.

==Production==
The film was shot in early 1948. 

==Reception==
It was one of the most popular films at the British box office in 1948.   UN award for 1948.

== References ==
 

== External links ==
* 
* 
*  at BFI Screenonline

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 