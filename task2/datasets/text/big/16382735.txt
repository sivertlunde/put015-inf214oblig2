An Imaginary Tale
{{Infobox film
| name           = Une histoire inventée
| image          =
| image_size     =
| caption        =
| director       = André Forcier
| producer       = Robin Spry Claudio Luca (producer and executive producer) Jamie Brown (executive producer) Louise Abastado (associate producer)
| writer         = André Forcier Jacques Marcotte
| narrator       =
| starring       = Tony Nardi Jean Lapointe Louise Marleau Charlotte Laurier
| music          = Serge Fiori
| cinematography = Georges Dufaux
| editing        = Aube Foglia François Gill
| distributor    = Astral Films
| released       = October 4, 1990   September 6, 1991   January 30, 1992  
| runtime        = 100 minutes
| country        =   French
| budget         =
| gross          = Canadian dollar|$ 487,809
| preceded_by    =
| followed_by    =
}}
An Imaginary Tale ( ) is a 1990 Canadian drama film.

==Plot==
Toni (Nardi) is the director of a staged rendition of Othello in Montreal. It is a pet project of his, financed by his mafia uncle. Unbeknownst to him, the audiences are also rounded up and paid by the same uncle. Some of them have seen every performance of this tragic play, and are understandably bored, so when the backstage romantic events of the actors result in absurd situations onstage, the audience is delighted. There are a huge number of romantic situations going on in this film at the same time. One of them involves Gaston (Lapointe), a somewhat world-weary jazz musician, and Florence (Marleau), a glamorous middle-aged woman who has been pining for him for years. Another involves to members of the musicians jazz trio. Yet another involves the plays Desdemona, Soledad (Laurier), the girlfriend of the man playing Othello, who cant keep his hands off his dresser. She is also Florences niece.

==Recognition==
* 1991
** Genie Award for Best Achievement in Direction - André Forcier - Nominated
** Genie Award for Best Motion Picture - Claudio Luca, Robin Spry - Nominated
** Genie Award for Best Original Screenplay - Jacques Marcotte, André Forcier - Nominated
* 1990
** Montreal World Film Festival Most Popular Film - André Forcier - Nominated - Won
** Sudbury Cinefest Best Canadian Film - André Forcier - Won

==External links==
*  
*  

 
 
 
 
 
 
 
 
 


 