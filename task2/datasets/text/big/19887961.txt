Agora (film)
{{Infobox film
| name           = Agora
| image          = Agoraposter09.jpg
| alt            =  
| caption        = Teaser poster
| director       = Alejandro Amenábar
| producer       = Fernando Bovaira Álvaro Augustin
| writer         = Alejandro Amenábar Mateo Gil
| starring       = Rachel Weisz Max Minghella
| music          = Dario Marianelli
| cinematography = Xavi Giménez
| editing        = Nacho Ruiz Capillas
| studio         = 
| distributor    = Focus Features Newmarket Films Telecinco Cinema
| released       =      
| runtime        = 126 minutes
| country        = Spain
| language       = English Spanish
| budget         = United States dollar|US$75 million (Euro|€ 50 million)
| gross          = $37,828,420
}} Roman Egypt, heliocentric model that challenges it. Surrounded by religious turmoil and social unrest, Hypatia struggles to save the knowledge of classical antiquity from destruction. Max Minghella co-stars as Davus, Hypatias fathers slave, and Oscar Isaac as Hypatias student, and later prefect of Alexandria, Orestes (prefect)|Orestes.

The story uses historical fiction to highlight the relationship between religion and science amidst the decline of Greco-Roman polytheism and the Christianization of the Roman Empire. The title of the film takes its name from the agora, a gathering place in ancient Greece, similar to the Roman Forum (Roman)|forum. The film was produced by Fernando Bovaira and shot on the island of Malta from March to June 2008. Justin Pollard, co-author of The Rise and Fall of Alexandria (2007), was the historical advisor for the film.

Agora was screened out of competition at the 2009 Cannes Film Festival in May, and opened in Spain on October 9, 2009 becoming the highest grossing film of the year for that country. Although the film had difficulty finding distribution, it was released country by country throughout late 2009 and early 2010. The film received a 53% overall approval rating from Rotten Tomatoes and seven Goya Awards in Spain, including Best Original Screenplay. It was awarded the Alfred P. Sloan Foundation Feature Film Prize at the Hamptons International Film Festival.

==Plot== Alexandria is Platonic school, Theon (Michael Orestes (Oscar Isaac) and Synesius (Rupert Evans), are immersed in the changing political and social landscape. She rejects Orestess love (she offers him her bloody menstrual towel, to show him that love has its drawbacks, while studying has none): she prefers to devote herself to science. Davus assists Hypatia in her classes and is interested in science, and is also secretly in love with her.
 Library of sexually assaulting her, but quickly begins to sob and offers his sword to her. However, she removes his slave collar and tells him he is free.
 the Earth is a sphere, by arguing that people far from the top would fall off the Earth. When they ask Davus his opinion he avoids conflict by saying that only God knows these things.
 heliocentric model religious objections Jews come into conflict, committing violent acts against each other.
 Cyril (Sami stone her instead. When everyone goes outside to collect stones, Davus secretly suffocates her to spare her the pain of being stoned to death and tells the mob that she fainted. Davus leaves as they begin to stone her.

==Cast==
*Rachel Weisz as Hypatia of Alexandria. Weisz was already a fan of Amenábars work when she received the script, and was very interested in the role.   See also:    Although she had not heard of Hypatia before, she felt that her history was still relevant to the contemporary world: "Really, nothing has changed. I mean, we have huge technological advances and medical advances, but in terms of people killing each other in the name of God, fundamentalism still abounds. And in certain cultures, women are still second-class citizens, and they’re denied education."  Weisz wanted to delve more into Hypatias sexuality and her desires, but Amenábar disagreed. She also received science lessons to help inform her depiction of the character.  At the 2009 Cannes Film Festival, Weisz spoke about her style and approach: "Theres no way we could know how people behave in the 4th century. I imagine they were still human beings with the same emotions as we have now. There are cultural customs, I guess, which were different. We approach the acting style to make the people flesh and blood and to make the acting incredibly naturalistic." 
*Max Minghella as Davus, Hypatias fathers slave. Davus is in love with Hypatia, but it is an unrequited love, and Davus turns towards Christianity instead. When Hypatia is about to be stoned to death at the end of the film, he decides to suffocate her because he still loved her and didnt want her to suffer any physical pain. The character of Davus was invented as "eyes for the audience" and is not based on any historical account.    Minghella grew up in Hampstead, near the same area of North London as Weisz, and found it very easy to work with her. 
*Oscar Isaac as Orestes (prefect)|Orestes. Student of Hypatia, Orestes is an aristocrat, who like Davus, falls in love with her, and has a strong friendship with Hypatia.  Isaac was familiar with the history of early Christianity during the period represented in the film, but like Weisz, he had not heard of Hypatia before joining the project. 
*Sami Samir as Saint Cyril of Alexandria
*Manuel Cauchi as Theophilus of Alexandria, uncle of Cyril
*Ashraf Barhom as Ammonius (monk)|Ammonius, a parabalani monk
*Michael Lonsdale as Theon of Alexandria, father of Hypatia Synesius of Cyrene
*Homayoun Ershadi as Aspasius the old slave. He acts as Hypatias research assistant.

==Production==

===Development===
  }}

After Amenábar completed  , by American astronomer  , Copernicus, Johannes Kepler and Galileo, {{cite news 
|last=Goldstein |first=Patrick |date=2009-05-17 |title=At Cannes: Alejandro Amenabars provocative new historical thriller |url=http://latimesblogs.latimes.com/the_big_picture/2009/05/from-cannes-agora-alejandro-amenabars-provocative-new-historical-thriller.html |publisher= , a 4th-century Greek astronomer whose history, he felt, was still relevant in the 21st century: "We realized that this particular time in the world had a lot of connections with our contemporary reality. Then the project became really, really intriguing, because we realized that we could make a movie about the past while actually making a movie about the present." 
 The Ten Commandments (1956), Ben-Hur (1959 film)|Ben-Hur (1959), and Pharaoh (film)|Pharaoh (1966).  A year before the start of pre-production, designer Guy Hendrix Dyas spent three weeks with Amenábar in Madrid to do some preliminary work on the set designs and the recreation of the ancient city of Alexandria so that previs animations could be generated.

The film was produced by Fernando Bovaira, with Telecinco Cinema as the primary producer along with Mod Producciones, Himenoptero, and Sogecable as co-producers. 

===Filming===
Principal photography began on March 17, 2008, on the island of Malta, and was scheduled to last 15 weeks.  Production designer Guy Hendrix Dyas used large sets on location instead of computer generated imagery at Amenábars direction.    The construction of the set employed almost 400 people, and was the largest ever designed on the island. Actor Charles Thake (Hesiquius) suffered minor facial injuries on the set when he collided with extras running during a scene.  Filming ended in June. 

==Release==
Agora premiered at the   and at Regals Westpark 8 in Irvine, California|Irvine. 

===Home media=== Region 2-locked DVD and Blu-ray formats.  A Region 1-locked DVD was released October 2010. 

==Reception==

===Critical response===
 

British writer and film critic Peter Bradshaw of The Guardian praised Alejandro Amenábar and his film, describing Agora as "an ambitious, cerebral and complex movie.... Unlike most toga movies, it doesnt rely on CGI spectacle, but real drama and ideas." Bradshaw also applauded Rachel Weiszs role as Hypatia, calling it "an outstanding performance". 
American screenwriter and critic Roger Ebert liked the film and gave it three stars out of four. He said: "I went to see Agora expecting an epic with swords, sandals and sex. I found swords and sandals, some unexpected opinions about sex, and a great deal more." 

The film holds a 52% Rotten rating on Rotten Tomatoes based on 87 reviews. 

===Response from Christians===
The Religious Anti-Defamation Observatory (Observatorio Antidifamación Religiosa), a   acknowledges that the film has been criticized for "perceived slights against Christians" but that "its lack of condemnation of specific dogma makes the films target seem to be fundamentalism in general". 

In contrast, the New York-based Rev. Philip Grey wrote a positive review of the film and strongly recommended it: "Christians who see themselves in the fanatic, murderous monks of the film and feel offended need to do some serious soul-searching.(...) Hypatia as depicted in the film is firmly opposed to what, in her time and at her city, is offered&mdash;or rather, imposed by brute force&mdash;under the name of Christianity. Nevertheless, she seems to me far more a follower of the precepts of Christianity than are her persecutors and tormentors.(...) In particular, in watching the deeply moving final scene, her going calmly to her death amidst the jeering mob, I could not help but strongly recall Jesus Christ on His own way to Golgotha". 

===Box office===
Agora was cinema of Spain|Spains highest grossing film of 2009, earning over $10.3 million within four days of its release on October 9.   
 Rentrak Theatrical, indieWIRE reported that Agora "scored the highest per-theater-average of any film in the marketplace" during the Memorial Day holiday weekend from May 28 through May 31, just after its U.S. limited release.  Despite this very high per-theater average, Agora was never widely released in the USA. According to Box Office Mojo, its widest release in the USA was just 17 theaters. 

The film grossed over $32.3 million (€ 21.4 million) by 1 December 2009, and about $35 million by 1 February 2010. As of 10 January 2011, Agoras worldwide box office earnings were approximately $39 million.  DVD and Blu-ray sales numbers are not publicly available.

===Accolades===
Agora was nominated for 13 Goya Awards, winning 7.  The film won the Alfred P. Sloan Foundation Feature Film Prize ($25,000 USD) in 2009 at the Hamptons International Film Festival. 

==Historical accuracy==
Before its release, the distribution company insisted on screening the film at the  . 

Antonio Mampaso, a Spanish astrophysicist and one of Agoras scientific advisors, stated in an interview that "we know that Hypatia lived in Alexandria in the IV and V centuries CE, until her death in 415. Only three primary sources mention Hypatia of Alexandria, apart from other secondary ones". He added that none of Hypatias work has survived but it is thought, from secondary sources, that her main fields of study and work were geometry and astronomy. Mampaso claimed that Hypatia invented the hydrometer, an instrument still in use today, and that probably her father Theon of Alexandria, together with Hypatia, invented the astrolabe.  However, it is generally accepted that the astrolabe had already been invented a couple of centuries earlier, and that the instrument was known to the Greeks before the Christian era.   Similarly, the hydrometer was invented before Hypatia, and already known in her time. Synesius sent Hypatia a letter describing a hydrometer, and requesting her to have one constructed for him.   
 Robert Barron, an American Catholic priest, writes in an article: "Hypatia was indeed a philosopher and she was indeed killed by a mob in 415, but practically everything else about the story that Gibbon and Sagan and Amenábar tell is false".  Irene A. Artemi, a doctor of theology at Athens University, states that "the movie—albeit seemingly not turning against the Christian religion—is in fact portraying the Christians as fundamentalist, obscurantist, ignorant and fanatic".   Similarly, the atheist Armarium Magnus blog said: "Over and over again, elements are added to the story that are not in the source material: the destruction of the library, the stoning of the Jews in the theatre, Cyril condemning Hypatias teaching because she is a woman, the heliocentric "breakthrough" and Hypatias supposed irreligiosity." 

==See also==
*Hypatia (novel)|Hypatia &ndash; an 1853 novel by Charles Kingsley
*Destruction of the Alexandrian Serapeum
*List of historical drama films
*List of films set in ancient Rome Other articles titled "Agora"
*The Name of the Rose, also about destruction of classical writings

==References==
 

==Further reading==
* 
* 
* 
* 
*Hart, David B.   First Things, June 4, 2010.
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 

==External links==
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 