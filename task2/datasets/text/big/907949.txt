Bulworth
{{Infobox film name                 =      Bulworth image                =      Bulworth.jpg alt                  = caption              =      Promotional poster director             =      Warren Beatty
|producer=Warren Beatty Lauren Shuler Donner Pieter Jan Brugge writer               = Warren Beatty Jeremy Pikser Aaron Sorkin James Toback starring             = Warren Beatty Halle Berry Oliver Platt Don Cheadle Paul Sorvino Jack Warden Isaiah Washington Christine Baranski Amiri Baraka music                = Ennio Morricone
|cinematography=Vittorio Storaro editing              = Billy Weber Robert C. Jones distributor          = 20th Century Fox  released             =   runtime              = 108 minutes   country              = United States language             = English budget               = $30 million gross                = $29,202,884   
}} political comedy comedy film Senator Jay Billington Bulworth (Beatty), as he runs for re-election while trying to avoid a hired assassin.

==Plot== Democratic Senator, conservative politics and to accepting donations from big corporations. In addition, though he and his wife have been having affairs with each others knowledge for years, they must still present a happy façade in the interest of maintaining a good public image.

Tired of politics, unhappy with his life in general, and planning to commit suicide, Bulworth negotiates a $10 million life insurance policy with his daughter as the beneficiary in exchange for a favorable vote from the insurance industry. Knowing that a suicide will void his daughters inheritance, he contracts to have himself assassinated within two days time.

Turning up in California for his campaign extremely drunk, Bulworth begins speaking his mind freely at public events and in the presence of the C-SPAN film crew following his campaign. After dancing all night in a club and smoking marijuana, he even starts rapping in public. His frank, potentially offensive remarks make him an instant media darling and re-energize his campaign. Along the way he becomes romantically involved with a young black activist named Nina, who tags along with him on his campaign stops. Along the way he is pursued by the paparazzi, his insurance company, his campaign managers and an increasingly adoring public, all the while fearful of his impending assassination. 

After a televised debate where Bulworth drinks out of a flask on air and derides insurance companies and the American healthcare system, he decides to hide at Ninas familys home, located in the ghetto of South-Central Los Angeles. While hiding at Ninas he wanders around the neighborhood, where he witnesses a group of kids selling crack, and buys the group ice cream. After saving the group from a racially motivated encounter with a cop, he finds out they are "soldiers" of L.D., a local drug kingpin whom Ninas brother owes money to. Bulworth eventually makes it to a television appearance arranged earlier by his campaign manager, during which he raps and repeats truths Nina and L.D. told him about the lives of poor black people and their opinions of various American institutions, like education and employment. Eventually he offers the solution that "everybody should fuck everybody" until everyone is "all the same color" stunning the audience and his interviewer.

After Bulworths TV appearance he escapes with Nina and goes with her back to her house, where she reveals that she is the assassin he indirectly hired (ostensibly to make the money needed to pay off the debt her brother owes to L.D.) and will now not carry out the job. Bulworth, finally relieved that he is not in danger of being killed, falls asleep, having not slept for the past several days. 

The next morning the press and Bulworths campaign managers converge on Ninas house, all eager to talk to him. L.D. also comes to Ninas house, and having had a change of heart says he will let Ninas brother work off his debt instead of hurting or killing him. Bulworth emerges from the bedroom looking rested, and as he steps outside he invites Nina to go with him, who eventually joins him after some hesitation. Bulworth and Nina embrace and begin to kiss as people cheer. As Bulworth happily accepts a new campaign for the presidency, he is suddenly shot in front of the crowd of reporters and supporters by an agent of the insurance company lobbyists, who were fearful of Bulworths recent push for single-payer health care.

Bulworths fate is left ambiguous. The final scene shows an elderly vagrant, that Bulworth met previously, standing alone outside a hospital who urges the audience that they cannot be ghosts, and must be spirits instead... implying that even if Bulworth dies, his new values will live on.

==Cast==
* Warren Beatty as Senator Jay Billington Bulworth
* Halle Berry as Nina
* Oliver Platt as Dennis Murphy
* Don Cheadle as L.D.
* Paul Sorvino as Graham Crockett
* Jack Warden as Eddie Davers
* Isaiah Washington as Darnell
* Christine Baranski as Constance Bulworth
* Amiri Baraka as Rastaman
* Joshua Malina as Bill Feldman
* Sean Astin as Gary
* Barry Shabaka Henley as the bartender
* Helen Martin as Momma Doll
;Cameos
* William Baldwin as Constances lover
* Larry King as himself
* Michael Clarke Duncan as Bouncer George Hamilton as himself

==Production==
  Primary Colors with her former comedy partner, the director Mike Nichols.
 Dick Tracy, Beatty used the leverage of a lawsuit to wangle unprecedented artistic freedom," disclosing only the barest outline of the story and essentially duping Fox into bankrolling the project.

==Soundtrack==
 
The soundtrack was released on April 21, 1998 by Interscope Records.

==Reception==
The film generated a great deal of controversy but received a positive reception from film critics.      It currently holds a 75% approval rating at Rotten Tomatoes.

In 2013, the New York Times reported that President Barack Obama had, in private, "talked longingly of going Bulworth," in reference to the film. {{Citation
| last    = Baker
| first   = Peter
| date     = May 15, 2013
| title    = Onset of Woes Casts Pall Over Obama’s Policy Aspirations
| newspaper  = The New York Times
| url = http://www.nytimes.com/2013/05/16/us/politics/new-controversies-may-undermine-obama.html?pagewanted=2&smid=tw-share&pagewanted=all
| accessdate  = 2013-05-18
}} 

===Box office===
The Los Angeles Times commented that Bulworth did "extremely well" on a limited release.   The film grossed United States Dollar|$29,202,884 worldwide at the box office.

==Awards==
{| class="wikitable"
|-
! Award
! Category
! Recipients and nominees
! Result
|-
| 71st Academy Awards Best Original Screenplay
| Warren Beatty and Jeremy Pikser 
|  
|-
| rowspan="3"|  56th Golden Globe Awards Best Screenplay
| Warren Beatty and Jeremy Pikser
|  
|- Best Picture
| Bulworth
|  
|- Best Actor
| Warren Beatty
|  
|-
| 1998 Satellite Awards Best Actor
| Warren Beatty
|  
|-
| 1998 Writers Guild of America Awards Best Screenplay
| Warren Beatty and Jeremy Pikser
|  
|-
| Chicago Film Critics Association Awards 1998
| Best Screenplay
| Warren Beatty and Jeremy Pikser
|  
|-
| Los Angeles Film Critics Association Best Screenplay (1998)
| Warren Beatty and Jeremy Pikser
|  
|-
|- Golden Lion Awards
| Best Film
| Bulworth
|  
|-
| rowspan="2"|  1999 NAACP Image Awards Outstanding Actress
| Halle Berry
|  
|- Outstanding Supporting Actor
| Don Cheadle
|  
|-
| American Film Institute 100 Years...100 Laughs
| Bulworth 
|  
|-
|}

==References==
 

==External links==
 
*  
*  
*  
*  
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 