Brigadoon (film)
{{Infobox film
| name           = Brigadoon
| image          = Brigadoon (french poster).jpg
| image_size     = 
| alt            = 
| caption        = French film poster
| director       = Vincente Minnelli
| producer       = Arthur Freed
| writer         = Alan Jay Lerner
| narrator       = 
| starring       = Gene Kelly  Van Johnson  Cyd Charisse
| music          = Alan Jay Lerner (Lyrics)  Frederick Loewe (Music)  Conrad Salinger (orchestrator)
| cinematography = Joseph Ruttenberg
| editing        = Albert Akst
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 108 minutes
| country        = United States
| language       = English
| budget         = $3,019,000  . 
| gross = $2.25 million (US) 
| preceded_by    = 
| followed_by    = 
}} MGM musical musical feature Ansco Color Broadway Brigadoon musical of the same name by Alan Jay Lerner and Frederick Loewe.  The film was directed by Vincente Minnelli and stars Gene Kelly, Van Johnson, and Cyd Charisse.  Brigadoon has been broadcast on American television and is available in VHS and DVD formats.  

The 1954 film is not to be confused with the 1966 television film of the same title and essentially the same plot.  The 1966 film was directed by Fielder Cook  and stars Robert Goulet, Peter Falk, and Sally Ann Howes.   It won five Primetime Emmy Awards. 

==Plot== Americans Tommy Albright (Gene Kelly) and Jeff Douglas (Van Johnson) are on a hunting trip in Scotland and become lost in the woodlands. They happen upon Brigadoon, a miraculously blessed village that rises out of the mists every hundred years for only a day. (This was done so that the village would never be changed or destroyed by the outside world.)  If any villager ever leaves Brigadoon, the spell will be broken and the village will vanish forever, and any outsider who wishes to stay must love someone in the village strongly enough to accept the loss of everything he or she knew in the outside world.  Tommy falls in love with village lass Fiona Campbell (Cyd Charisse), whose younger sister Jean (Virginia Bosler), is about to be married to Charlie Dalrymple (Jimmy Thompson). When Tommy and Jeff happen upon clues about the village and its people that make no sense, Fiona takes them to see Mr. Lundie, the village schoolmaster, who tells them the story of Brigadoon and the miracle. That evening, Mr. Lundie officiates at the wedding of Jean and Charlie, which Tommy and Fiona attend. Interrupting the wedding, the jealous Harry Beaton (Hugh Laing) announces he is leaving Brigadoon to make everything disappear, since the girl he loves, Jean, is marrying another man.   Harrys words cause mass chaos among the townspeople and they all rush to stop him. Harry almost crosses the bridge but is stopped short by Tommy, who is knocked unconscious. With men closing in on him, Harry climbs up a tree to hide but is soon shot accidentally by Jeff, who skipped the wedding to hunt and shot at a bird that flew by Harry. Harry falls dead to the ground and is soon found by the men. Fiona frantically searches to find Tommy.  Confessing their love for each other, they decide to marry, allowing Tommy to stay in Brigadoon for good. But while Fiona goes off to find Mr. Lundie, Tommy tells Jeff about his plan. Jeff, drunk and remorseful of accidentally killing Harry, tells Tommy he cant just leave everything in the real world behind for this girl hes only known a day. Fiona returns with Mr. Lundie, but Tommy confesses that he cannot stay. Fiona says she understands but is heartbroken and they say good-bye before Brigadoon completely disappears. Tommy and Jeff cross the bridge and walk away.

Back in New York City, Tommy can think only of Fiona.  Unable even to talk with his fiancee, Tommy ends his relationship with her and calls Jeff, telling him to get the first flight back to Scotland. He and Jeff return to the same spot where they were lost, though Jeff reminds him again the village will not be there.  But suddenly Tommy sees lights start to appear thrrough the mist and runs toward them.  Brigadoon reappears and Tommy gets to the foot of the bridge to see Mr. Lundie half-awake on the other side saying: "Tommy, lad, you! My, my, you must really love her. You woke me up." Tommy seems stunned that Brigadoon has been brought back, but Mr. Lundie reminds him: "I told ye, if you love someone deeply enough, anything is possible ... even miracles."

Tommy then runs across the bridge and reunites with Fiona as the village fades back into the mist.

==Cast==
* Gene Kelly as Tommy Albright
* Van Johnson as Jeff Douglas
* Cyd Charisse as Fiona Campbell
* Elaine Stewart as Jane Ashton Barry Jones as Mr. Lundie
* Hugh Laing as Harry Beaton
* Albert Sharpe as Andrew Campbell
* Virginia Bosler as Jean Campbell Jimmy Thompson as Charlie Chisholm Dalrymple Tudor Owen as Archie Beaton
* Owen McGiveney as Angus
* Dee Turnell as Ann
* Dodie Heath as Meg Brockie (as Dody Heath)
* Eddie Quillan as Sandy Michael Dugan as Townsman
* Barrie Chase as Dancer
* George Chakiris as Dancer

==Production==
Producer J. Arthur Rank, 1st Baron Rank|J. Arthur Rank acquired the rights of the official play in February 1951.    According to the press, Metro-Goldwyn-Mayer "paid a fortune" for the rights, "Gene Kelly, Kathryn Grayson Named Stars For Movie, Brigadoon  by Louella Parsons, Deseret News, March 12, 1951, p. 3  and Gene Kelly and Kathryn Grayson were named in the leads a month later.  By the time they were cast, a script was not written yet, although it was reported that Alan Jay Lerner was expected to start on the script a week later.  Furthermore, Alec Guinness was also set for a role and David Wayne, Moira Shearer and Donald OConnor were under consideration for one. 
 Bill Hayes for the role of Jeff.  Cyd Charisse replaced Grayson in March 1953. Elaine Stewart was cast in the fourth lead in November 1953, and it was reported that she was more enthusiastic about working with Minnelli than with Kelly. 

Because of Kellys commitments to other film projects, production was delayed for a while, and it did not begin until 1953.    Minnelli and Kelly considered shooting the film on location in Scotland, but due to its unpredictable climate, high production costs, and MGM President Dore Scharys eagerness of films being made on a low budget, the idea was canned.  Kelly and producer Arthur Freed traveled to Scotland to confirm for themselves if the weather was too unreliable, and they agreed with the studio.  In Kellys biography it was stated that "the weather was so bad that we had to agree with the studio. So we came back to the United States and started looking for locations here. We found some highlands above Monterey   that looked like Scotland. But then the studio had an economy wave, and they clamped the lid on that idea."  Much to the disappointment of the cast and crew and the delight of Dore Schary, filming had to take place on the sound stages at MGM instead.

In addition, rather than being filmed in the expensive original three-strip   and utilized   process to add clarity and presence to the picture. 

==Musical numbers==
 
# "Once in the Highlands/Brigadoon/Down on MacConnachy Square"&nbsp;– Eddie Quillan, Villagers, and Offscreen M-G-M Chorus
# "Waiting for My Dearie"&nbsp;– Cyd Charisse (dubbed by Carol Richards) and Dee Turnell (dubbed by Bonnie Murray)
# "Ill Go Home with Bonnie Jean"&nbsp;– Jimmy Thompson (dubbed by John Gustafson), Gene Kelly, Van Johnson and Chorus
# "The Heather on the Hill"&nbsp;– Gene Kelly, Danced by Gene Kelly and Cyd Charisse
# "Almost Like Being in Love"&nbsp;– Sung and Danced by Gene Kelly
# "The Wedding Dance"&nbsp;– Danced by Jimmy Thompson and Virginia Bosler
# "The Chase"&nbsp;– Sung by men pursuing Hugh Laing
# "The Heather on the Hill"&nbsp;– Danced by Gene Kelly and Cyd Charisse
# "Ill Go Home with Bonnie Jean" (reprise)&nbsp;– Sung offscreen by Jimmy Thompson, Carol Richards and Chorus
# "The Heather on the Hill" (reprise) &nbsp;– Sung offscreen by Jimmy Thompson, Carol Richards and Chorus
# "Waitin for My Dearie" (reprise)&nbsp;– Sung offscreen by Jimmy Thompson, Carol Richards and Chorus
# "Finale: Brigadoon" &nbsp;–  M-G-M Chorus
: Source:  
 Breen office refused to allow the use of the two songs the Meg Brockie character sang in the stage version ("The Love of My Life" and "My Mothers Wedding Day" ), as the lyrics were considered too risqué for general audiences. With the omission of these songs, the supporting role of Meg Brockie was reduced in the film to scarcely more than a bit part. The minor song "Jeannies Packin Up" was also omitted. Some of this was done because, after listening to Gene Kellys pre-recordings of "There But For You Go I" and "From This Day On",  the makers of the film felt that the results did not show his voice to its best advantage, but some was done because producer Arthur Freed wanted to shape the two-and-a-half hour stage musical into a film that ran 108 minutes.

Complete sound and picture footage of three of the deleted musical numbers has survived, and it is included in the latest DVD release of the film.

The 1954 original motion picture soundtrack was originally incomplete, but was re-released with deleted songs, alternate takes, and undubbed vocals.

==Reception==
Bosley Crowther in the New York Times of September 17, 1954, described the film as "curiously flat and out-of-joint, rambling all over creation and seldom generating warmth or charm."  Crowther admired the costumes, sets, and decor but deplored the omission of several musical numbers.  He found fault with the films two stars and its director: "the personable Gene Kelly and Cyd Charisse have the lead dancing roles. Even so, their several individual numbers are done too slickly, too mechanistically. What should be wistful and lyric smack strongly of trickery and style&nbsp;... Mr. Kellys   is as thin and metallic as a nail; Miss Charisses is solemn and posey&nbsp;... Vincente Minnellis direction lacks his usual vitality and flow." He concluded by noting the film was "pretty weak synthetic Scotch." 

Leonard Maltin in his reappraisal feels this adaptation was unfairly overlooked when it first appeared and particularly praises the lovely score, orchestrated mainly by Conrad Salinger, and the performance of Van Johnson as Jeff Douglas.

==Box Office==
According to MGM records the film earned $1,982,000 in the US and Canada and $1,293,000 and resulted in a loss of $1,555,000. 

==Awards and nominations==
The film was nominated for three Academy Awards in 1955:      
* Best Art Direction-Set Decoration, Color&nbsp;– (Cedric Gibbons, E. Preston Ames, Edwin B. Willis, F. Keogh Gleason)
* Best Costume Design, Color&nbsp;– (Irene Sharaff)
* Best Sound, Recording&nbsp;– Wesley C. Miller (MGM)

The film won a 1955 Golden Globe:
* Best Cinematography, Color&nbsp;– (Joseph Ruttenberg)

;American Film Institute Lists:
*AFIs 100 Years...100 Passions&nbsp;– Nominated 
*AFIs 100 Years...100 Songs:
**Almost Like Being in Love&nbsp;– Nominated 
*AFIs Greatest Movie Musicals&nbsp;– Nominated 
*AFIs 10 Top 10&nbsp;– Nominated Fantasy Film 

==References==
 

==External links==
*  
*  

{{Navboxes|list1=
 
 
}}

 
 
 
 
 
 
 
 
 
 
 
 
 
 