The Psycho Legacy
{{Infobox film
| name           = The Psycho Legacy
| image          = The Psycho Legacy.jpg
| alt            = DVD cover art
| caption        = DVD cover art
| director       = Robert Galluzzo
| producer       =  
| writer         = Robert Galluzzo
| narrator       = Paul Ehlers
| starring       =  Mick Garris
| music          = Jermaine Stegall
| cinematography = John Torrani
| editing        = Jon Maus
| studio         = Masimedia
| distributor    = Shout! Factory
| released       =   |
| runtime        = 87 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

The Psycho Legacy is a 2010  independent  .  It also features interviews with current horror filmmakers who are fans of the Psycho series. 

The documentary was in production for 3 years and was released on DVD on October 19, 2010 in the United States and Canada. In April 2012, it was announced on the documentarys official Facebook page that a full length book based on the documentary is being written by director, writer and producer Robert V. Galluzzo.

==Cast==
*Anthony Perkins – Norman Bates, Psycho I–IV (archive footage) 
*Janet Leigh – Marion Crane, Psycho (archive footage)
*Vera Miles – Lila Crane, Psycho I–II (archive footage)
*Alfred Hitchcock – Director, Psycho (archive footage)
*Hilton A. Green – Assistant Director, Psycho/Producer, Psycho II–IV  Tom Holland – Writer, Psycho II Richard Franklin – Director, Psycho II (archive footage)
*Robert Loggia – Dr. Bill Raymond, Psycho II
*Chris Hendrie – Deputy Pool, Psycho II
*Andrew London – Editor, Psycho II
*Kurt Paul – Mother, Psycho II–III/Raymond Linette, Psycho IV
*Diana Scarwid – Maureen Coyle, Psycho III
*Lee Garlington – Myrna, Psycho II–III
*Jeff Fahey – Duane Duke, Psycho III
*Juliette Cummins – Red, Psycho III
*Brinke Stevens – Body Double, Psycho III
*Katt Shea – Patsy, Psycho III 
*Donovan Scott – Kyle, Psycho III
*Charles Edward Pogue – Writer, Psycho III
*Mick Garris – Director, Psycho IV
*Henry Thomas – Young Norman Bates, Psycho IV Norma Bates, Psycho IV
*Sharen Camille – Holly, Psycho IV
*Cynthia Garris – Ellen Stevens, Psycho IV
* 
*Stuart Gordon – Director Adam Green – Writer/Director
*Rolfe Kanefsky – Writer/Director Joe Lynch – Director
*Jason Allentoff – Webmaster, The Psycho Movies.com
*David J. Schow – Writer
*Dave Parker – Writer
*Ryan Turek – Journalist, Shock Til You Drop.com
*John Murdy – Creative Director, Universal Studios: California
*Tony Timpone – Former Editor, Fangoria

==Development==
The documentary was originally developed in late 2006.  Robert Galluzzo stated "I got the idea for the documentary when the  . But Universal wasnt interested in these ideas. However, the studio did allow Galluzzo to shoot footage on the Bates Motel and Psycho house sets on the backlot and use pictures and footage from all the films.

==Production==
Filming began in January 2007. Director Robert Galluzo shot interviews with Psycho cast and crew members over the next three years. Galluzo personally financed the production as he described it as a "labour of love project". Different types of digital video cameras were used to film the interviews over the three-year-long production, thus giving the documentary an   alumni as possible to arrange interviews. Most of them are based out of California, so I’d work at my dayjob for 3 months straight, then take 2 weeks off to go shoot some interviews. And then come back and do it all over again. That’s the main reason it’s taken so long is because I had to go back to work and save up the money to continue shooting. This was always a labor of love, so I did anything I could to make it work and get just a tiny bit further along."

==Promotion== Fangoria horror Tom Holland, San Diego Comic Con 2010.

==Release==
The documentary premiered at the 2010 Screamfest Horror Film Festival on October 16, 2010, followed by a Q&A with Mick Garris, Cynthia Garris and Katt Shea.
The documentary is set for a DVD release on October 19, 2010 over   in Germany on February 14, 2012. Releases in other countries is yet to be decided.

==Critical reception==
The documentary has received mostly positive reviews. Staci Layne Wilson of Horror.com says "Overall, I think this doc is a great way for Shout! Factory and Masi Media LLC to help us all celebrate the 50th birthday of Alfred Hitchcocks beloved Psycho". Jason Allentoff of The Psycho Movies.com says "RobGs look at this film series is incredible. Its a shame Universal hasnt taken notice." Matt Fini of Dread Central.com says " As far as Psycho is now concerned, we’ve thankfully got this documentary so that we won’t forget." Mike Gencarelli of Movie Mikes says "If you add the amazing feature film to these special features you have the ultimate collection for all Psycho fans." William David Lee of DVD Town.com says "The retrospective may not be as in-depth as some would like, there is enough to sate the appetites of the most die-hard "Psycho" followers." Horror Talk.com claims "Director Galluzzo does his best with a lot of fine material and keeps things moving at a decent clip, but repeat viewings will reveal the limitations that keep this from being the ultimate experience that it should be."
 Bates Motel 1998 remake were not mentioned. However originally in the deleted scenes section of the DVD there was supposed to be two short segments about the TV pilot and remake but for unknown reasons they were left off of the DVD when it was released. Some critics also criticized the low-budget production values of the film. Citing issues with the sound and aspect ratio of the documentary. The documentary has a 7.0 out of 10 rating on IMDb and a 62% audience score on Rotten Tomatoes.

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 