My Pal Trigger
{{Infobox film
| name           = My Pal Trigger
| image          = Mypaltripos.jpg
| image_size     =
| caption        = Original film poster Frank McDonald
| producer       = Armand Schaefer (associate producer)
| writer         = Screenplay: Jack Townley John K. Butler Story: Paul Gangelin Jack Holt Trigger Bob Nolan
| music          = R. Dale Butts Mort Glickman Charles Maxwell William Bradford  
| editing        = Harry Keller
| distributor    = Republic Pictures
| released       =  
| runtime        = 79 minutes
| country        = United States
| language       = English
}}
 1946 American Western musical Frank McDonald. George “Gabby” Jack Holt, Trigger in a story about the origin of Rogerss mount, and their deep and faithful bond. The film features several musical numbers for Rogers, Evans, and Bob Nolan and the Sons of the Pioneers.

==Plot==
Roy Rogers, a horse dealer and a peddler of leather goods, is hoping to mate his mare named "Lady" with Golden Sovereign, a stallion owned by rancher Gabby Kendrick (George "Gabby" Hayes).  Kendrick refuses, preferring to mate the stallion with his own stock. Brett Scoville (Jack Holt), a wealthy rancher and nightclub owner, wants to mate the stallion with his mare, as well, and to that end, steals the stallion. Golden Sovereign breaks free, finds Lady, and has a romantic interlude with her in the hills.

A search is raised to find Golden Sovereign, but Scoville accidentally shoots and kills the horse when Golden Sovereign is attacked by a wild stallion. Roy arrives on the scene and chases Scoville off with a bullet, but cannot later identify him as the killer. To all appearances, it looks like Roy is responsible for the death of Golden Sovereign, and he is arrested. He jumps bail, and takes off on Lady. Months later, Lady gives birth to Golden Sovereigns son. Roy names him Trigger and, in time, the horse becomes his mount.

Roy returns to the Golden Horse Ranch to present Trigger as a gift to Kendrick. The gift is declined, and Roy is jailed as a fugitive from justice. Trigger is auctioned to pay Roys debts. Scoville acquires the horse and, when the case against Roy is dropped, hires him to train Trigger.
 race at State Fair. Roy is riding Scovilles Trigger in the race, and Scoville promises him ownership of the horse should he win the race. Roy is reluctant to race after learning Scoville killed Golden Sovereign. However, Roy races and aids Susan when Scovilles men hold her horse in a pocket on the track. Susan wins the race on Golden Empress, and, consequently, Roy loses possession of Trigger. Later, Scoville is arrested for killing Golden Sovereign, and, Roy finally comes into possession of Trigger through Kendrick. Eventually, Roy is employed at the Kendrick ranch, and Trigger becomes the father of twin sons born to Susans Golden Empress.

==Cast==
* Roy Rogers as Roy Rogers, a horse dealer and peddler of leather goods
* Trigger (horse) as Trigger, the son of Kendrick’s Golden Sovereign and Roy’s Lady
* George "Gabby" Hayes as Gabby Kendrick, owner of the Golden Horse Ranch and Golden Sovereign, a stallion
* Dale Evans as Susan Kendrick, his daughter
* Bob Nolan as Bob, a Kendrick ranch hand 
* Sons of the Pioneers as musicians and Kendrick ranch hands Jack Holt as Brett Scoville, a wealthy rancher, horse breeder, and owner of the El Dorado casino and nightclub
* LeRoy Mason as Carson, Scoville’s henchman
* Roy Barcroft as Hunter, Scoville’s henchman
* Kenne Duncan as a croupier at Scoville’s nightclub
* Sam Flint as the Sheriff of El Dorado County
* Ralph Sanford as Al, an auctioneer
* Francis McDonald as Pete, a storekeeper
* Harlan Briggs as Dr. Bentley, a veterinarian
* William Haade as Davis

==Music==
* “She’s Havin’ Too Much Fun” (sung by Rogers and Evans)
* “Old Faithful” (sung by Rogers)

== External links ==
*  
*  
*  
*  
*  


 
 
 
 
 
 