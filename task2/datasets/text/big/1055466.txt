Primal Fear (film)
{{Infobox film
| name = Primal Fear
| image = Primal Fear.jpg
| caption = Theatrical release poster
| director = Gregory Hoblit
| producer = Gary Lucchesi Howard W. Koch, Jr.
| screenplay = Steve Shagan Ann Biderman
| based on =  
| starring = Richard Gere Laura Linney John Mahoney Alfre Woodard Frances McDormand Edward Norton
| music = James Newton Howard Michael Chapman
| editing = David Rosenbloom
| studio = Rysher Entertainment
| distributor = Paramount Pictures
| released =  
| runtime = 130 minutes
| country = United States
| language = English
| budget = $30 million
| gross = $102,616,183 
}}
 same name and directed by Gregory Hoblit. 

The film tells the story of Chicago defense attorney who believes his altar boy client is not guilty of murdering an influential Catholic Archbishop. 

Primal Fear was a box office success and earned mostly positive reviews, with Edward Norton making a strong showing in his film debut. He was nominated for an Academy Award for Best Supporting Actor and won a Golden Globe Award for Best Supporting Actor – Motion Picture. 

==Plot==
Martin Vail (Richard Gere) is a prominent Chicago defense attorney who loves the public spotlight and does everything he can to get his high-paying clients acquitted on legal technicalities. One day, he sees a news report about the arrest of Aaron Stampler (Edward Norton), a young altar boy from Kentucky with a severe stutter, who is accused of brutally murdering the beloved Archbishop Rushman (Stanley Anderson). Vail jumps at the chance to represent the young man pro bono.

At first, Vail is interested primarily in the publicity that the case will bring. During his meetings at the county jail with Aaron, however, Vail comes to believe that his client is truly innocent, much to the chagrin of the prosecutor (and Vails former lover), Janet Venable (Laura Linney).

As the murder trial begins, Vail discovers that powerful civic leaders, including the corrupt States attorney John Shaughnessy (John Mahoney), have lost millions of dollars in real estate investments due to a decision by the Archbishop not to develop on certain church lands. The Archbishop received numerous death threats as a result. Vail makes a search of the Archbishops apartment and finds a videotape of Stampler being forced to perform in a sexual act with another altar boy and a girl named Linda (Azalea Davila). Vail is now in a bind: Introducing this evidence would make Stampler more sympathetic to the jury, but it would also give his client a motive for the murder, which Venable has been unable to establish.
 sociopath who multiple personality abuse at insanity plea during an ongoing trial.

Vail sets up a confrontation in court by dropping hints about the Archbishops pedophilia, as well as Stamplers multiple personalities. He also has the sex tape delivered to Venable, knowing she will realize who sent it and—since she is under intense pressure from both Shaughnessy and her boss Bud Yancy (Terry OQuinn) to deliver a guilty verdict—will use it as proof of motive.

At the climax, Vail puts Stampler on the witness stand and gently questions him about his troubled dealings with the Archbishop and all about the sexual abuse that the Archbishop put him through. During cross-examination, after Venable questions him harshly for several minutes, Stampler turns into "Roy" in open court and attacks her, threatening to snap her neck if anyone comes near him. He is subdued by courthouse marshals and rushed back to his holding cell.
 not guilty mental hospital. Venable is fired for allowing the Archbishops crimes, which both the Catholic Church and the city council had been trying to hide for the past ten years, to come to public light.

In the final scene, Vail visits Stampler in his cell to tell him of the dismissal. Stampler says he recalls nothing of what happened in the courtroom, having again "lost time". However, just as Vail is leaving, Stampler asks him to "tell Miss Venable I hope her neck is okay," which he could not have been able to remember if he had "lost time". When Vail confronts him, Stampler reveals that he has been pretending to be insane the whole time. No longer stuttering, he brags about having murdered Archbishop Rushman for the sexual abuse inflicted upon him, as well as Linda. When Vail says, "So there never was a Roy," Stampler corrects him and says, "There never was an Aaron, counselor!" Stunned and disillusioned at how he was so easily fooled and manipulated by his own client, Vail walks away with Stampler taunting him from his cell.

==Cast==
 
* Richard Gere as Martin Vail
* Laura Linney as Janet Venable
* John Mahoney as John Shaughnessy
* Alfre Woodard as Judge Shoat
* Frances McDormand as Dr. Molly Arrington
* Edward Norton as Aaron Stampler / Roy
* Reg Rogers as Jack Connerman
* Terry OQuinn as Bud Yancy 
* Andre Braugher as Tommy Goodman 
* Steven Bauer as Joey Pinero
* Joe Spano as Abel Stenner 
* Tony Plana as Martinez
* Stanley Anderson as Archbishop Rushman
* Maura Tierney as Naomi Chance
* Jon Seda as Alex
* Kenneth Tigar as Weil
 

==Reception==
Primal Fear garnered positive reviews from critics and has a  74% "Fresh" rating on Rotten Tomatoes based on 43 reviews with an average score of 6.7 out of 10. The consensus states "A straightforward, entertaining thriller with a crackerjack performance by Edward Norton".   According to Janet Maslin, the film has a "good deal of surface charm", but that the film is "pared down to a farfetched plot and paper-thin motives,   the story relies on an overload of tangential subplots to keep it looking busy." 
Roger Ebert wrote, "the plot is as good as crime procedurals get, but the movie is really better than its plot because of the three-dimensional characters." Ebert awarded Primal Fear three-and-a-half stars out of a possible four, described Geres performance as one of the best in his career, praised Linney for rising above what might have been a stock character, and applauded Edward Norton for offering a "completely convincing" portrayal. 
 three weekends at the top of the U.S. box office.

Primal Fear inspired the 2002 Bollywood movie Deewangee. 

===Accolades===
Nortons depiction of Aaron Stampler garnered him multiple awards and nominations.
{| class="wikitable plainrowheaders" style="font-size: 100%;"
|-  style="background:#ccc; text-align:center;"
! scope="col" | Award
! scope="col" | Category
! scope="col" | Nominee(s)
! scope="col" | Result
|-
! scope="row" | Academy Award Best Supporting Actor
| Edward Norton
|  
|-
! scope="row" | American Society of Composers, Authors and Publishers
| Top Box Office Films
| James Newton Howard
|  
|-
! scope="row" | Boston Society of Film Critics Best Supporting Actor  
| rowspan="2" | Edward Norton
|  
|-
! scope="row" | British Academy Film Awards Best Actor in a Supporting Role
|  
|-
! scope="row" | Casting Society of America
| Best Casting for Feature Film, Drama
| Deborah Aquila Jane Shannon-Smith
|  
|-
! scope="row" rowspan="2" | Chicago Film Critics Association Best Supporting Actor
| rowspan="8" | Edward Norton
|  
|-
| Most Promising Actor  
|  
|-
! scope="row" | Florida Film Critics Circle Best Supporting Actor  
|  
|-
! scope="row" | Golden Globe Award Best Supporting Actor - Motion Picture
|  
|-
! scope="row" | Kansas City Film Critics Circle Best Supporting Actor
|  
|-
! scope="row" | Los Angeles Film Critics Association Best Supporting Actor  
|  
|-
! scope="row" | MTV Movie Awards Best Villain
|  
|-
! scope="row" | National Society of Film Critics Best Supporting Actor  
|  
|-
! scope="row" | Satellite Award
| Best DVD Extras
| Primal Fear - Hard Evidence Edition
|  
|-
! scope="row" | Saturn Award Best Supporting Actor
| rowspan="3" | Edward Norton
|  
|-
! scope="row" | Society of Texas Film Critics Awards
| Best Supporting Actor  
|  
|-
! scope="row" | Southeastern Film Critics Association Best Supporting Actor  
|  
|}

American Film Institute recognition:
*AFIs 100 Years...100 Heroes and Villains:
** Aaron Stampler - Nominated Villain 

==See also==
* Trial movies

==References==
 

==External links==
 
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

 