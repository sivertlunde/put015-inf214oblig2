Warming Up (1983 film)
{{Infobox film
| name           = Warming Up
| image          = 
| image size     =
| caption        = 
| director       = Bruce Best
| producer       = James Davern
| writer         = James Davern
| based on = 
| narrator       =
| starring       = Henri Szeps Barbara Stephens
| music          = 
| cinematography = 
| editing        = 
| studio = 
| distributor    = 
| released       = 1983
| runtime        = 
| country        = Australia English
| budget         = 
| gross = 
| preceded by    =
| followed by    =
}}
Warming Up is a 1983 Australian film about a divorced woman who moves to a country town and teaches the local football team to dance ballet. It was made by the team responsible for the TV show A Country Practice. David Stratton, The Avocado Plantation: Boom and Bust in the Australian Film Industry, Pan MacMillan, 1990 p318 

==References==
 

==External links==
*  at IMDB

 
 

 