Botched (film)
{{Infobox film
| name           = Botched
| image          = Botched poster.jpg
| caption        = Poster art
| director       = Kit Ryan
| producer       = Alan Balladur Thomas Fischer Steve Richards Terence Ryan Ken Tuohy
| writer         = Raymond Friel Derek Boyle Eamon Friel Geoff Bell
| music          = Tom Green
| cinematography = Bryan Loftus
| editing        = Jeremy Gibbs
| studio         = ApolloProMovie & Co. 1. Filmproduktion Arcade Films Barraboy Films Madigan Film Productions Opix Films Zinc Entertainment Inc.
| distributor    = Optimum Releasing   Darclight Films  
| released       =  
| runtime        = 91 minutes
| country        = Ireland
| language       = English
| budget         =
}}
Botched is a 2007 horror comedy film starring Stephen Dorff.

==Plot== Russian accomplices, Russell Smith), recover the cross; but their elevator becomes stuck on the uncompleted 13th floor. Believing the police have stopped the elevator, the thieves take their fellow passengers hostage to negotiate an escape. They agree to send one hostage down; but, when the elevator doors open on the ground floor, the hostage has mysteriously been Decapitation|beheaded. 
 Geoff Bell).

In the confusion, the Christian group seizes firearms and takes control, shooting Peter. Katerina is sent to stand guard over Richie and the remainder of the hostages, while Sonya takes Yuri to a chamber where he is to sacrificed by a wild man in armor named Alex (Edward Baker-Duly). Yuri manages to escape, but he is impaled by one of Alexs traps, complete with disco music and lighting. In his rage, Alex kills Katerina, allowing Richie and the hostages to escape. They discover Alexs lair, where he has been watching everyone using security cameras. A photo reveals that Alex is the twin brother of Sonya, and they both believe themselves to be descendants of Ivan the Terrible. The group splits when Richie and Anna decide to chase after Alex, while Boris and Dmitry prefer to rig the room with various traps they have constructed and wait for Alex to come to them.
 cauterize his arm, but is also injected with a serum that restricts facial movement.

Alex drags Peters body into his lair for a sacrifice. In doing so, he finds the cross and takes it for himself. Richie and Anna then unsuccessfully search Peter for the cross, only to discover that Peter miraculously survived the shooting. Alex is then attacked from three sides by Peter, Richie and an increasingly incoherent Boris. Though Alex easily kills Peter, Richie manages to impale him on his disco spikes, recovers the cross, then sets him on fire. As they are fleeing the building, Richie is strangled by Sonya, but Anna saves him by stabbing Sonya with her nail file. Security is alerted by Alexs body falling onto the floor below, allowing Richie and Anna to escape unencumbered.

Outside, they are met by Groznyi, who pays Richie handsomely for his work. Richie is enraged, having discovered that Grozniy is the brother of Sonya and Alex, and tells him that hell never work for him again, but Grozniy replies that Richie will be back. Richie and Anna then depart for Los Angeles.

The working title for this film was 13.

==Cast==
* David Heap as Auctioneer
* Alan Smyth as Hugo
* Stephen Dorff as Ritchie
* Sean Pertwee as Mr. Groznyi
* Igor Chistol as Thug 1
* Greg Jeloudov as Thug 2
* Jamie Foreman as Peter Russell Smith as Yuri
* Bronagh Gallagher as Sonya
* Norma Sheahan as Helena
* Gene Rooney as Katerina
* Jaime Murray as Anna
* Hugh OConor as Dmitry Geoff Bell as Boris
* Zak Maguire as Alex
* Edward Baker-Duly as Killer (credited as Edward Duly Baker)
* Luke Hayden as Security Guard

==Reception==
Critical reception for Botched has been negative and the film holds a rating of 25% on Rotten Tomatoes, based on 8 reviews. 

The film has received praise from horror review websites like Dread Central and Shock Till You Drop,  and Dread Central praised it for "  its audience with intelligence". 

===Awards===
* Best Feature Film at the 2007 New York City Horror Film Festival   
* Best Actor for Stephen Dorff at the 2007 New York City Horror Film Festival 

==References==
 

==External links==
*  
*  

 
 
 
 
 