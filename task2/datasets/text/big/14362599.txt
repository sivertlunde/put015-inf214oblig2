Cactus Makes Perfect
 
{{Infobox Film |
  | name           = Cactus Makes Perfect |
  | image          = CactusMakesPerfect1942lobbycard.jpg |
  | caption        =  |
  | director       = Del Lord |
  | writer        = Monte Collins Elwood Ullman | Ernie Adams|
  | cinematography = Benjamin H. Kline | 
  | editing        = Jerome Thom |
  | producer       = Del Lord Hugh McCollum |
  | distributor    = Columbia Pictures |
  | released       = February 26, 1942 (United States|U.S.) |
  | runtime        = 17 18" |
  | country        = United States
  | language       = English
}}

Cactus Makes Perfect  is the 61st short subject starring American slapstick comedy team the Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959.

==Plot==
 
The film opens with the Stooges mother (played by male actor Monte Collins) attempting to wake up her three boys without success. "Get out of bed you lazy loafers!" she screams to no avail. Finally, she yanks a rope that leads from the kitchen to the bed where the trio is sleeping soundly. This causes the bed to spin vertically until they are expelled.  
 Ernie Adams) after Curly fires an arrow from his Gold Collar Button Retriever. The two then try to rob the boys out of their dough. Moe and Larry flee to an abandoned hotel where Curly hid the gold in a safe. ("Its safe in the safe".) The miners show up, and they all take refuge in the safe room. The miners drill through the door, which Curly attributes to termites, and throw a stick of dynamite in. After a little back and forth, the stick fizzles out. Believing it to be a dud, the boys burst out laughing and Curly chucks the dynamite, causing it to actually explode.

==Production notes==
Filmed on August 7-11, 1941,    the title Cactus Makes Perfect parodies the proverb "practice makes perfect." 

Curlys remark, "I shoot an arrow into the air, where it lands I do not care: I get my arrows wholesale!"  parodies Henry Wadsworth Longfellows poem "The Arrow and the Song," which begins, "I shot an arrow into the air/It fell to earth, I knew not where..." 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 