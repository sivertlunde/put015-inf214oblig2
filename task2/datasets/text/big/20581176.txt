Elaan (1971 film)
 
{{Infobox film
| name           = Elaan 
| image          = Elaan1971.jpg
| image_size     = 
| caption        = 
| director       = K. Ramanlal
| producer       = 
| writer         =
| narrator       = 
| starring       =Vinod Khanna, Rekha and Madan Puri
| music          = Shankar-Jaikishan
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1971
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
 1971 Bollywood thriller film directed by K. Ramanlal. The film stars Vinod Khanna, Rekha and Madan Puri .

==Plot==
Naresh Kumar Saxena lives with his widowed mom and sister, Seema. He works as a freelance photographer and journalist. One day he meets with Mala Mehta and her dad, who is the Editor of a Newspaper. Mr. Mehta hires Naresh and assigns him to go to a remote island to investigate and expose some illegal activities there. Naresh goes there in the company of his friend, Shyam. Unfortunately, they are caught by the island guards and Lily tries to convince them to work for the gang . On refusal Naresh is lodged in a cell along with two others, one a scientist and the other is Ram Singh, a hoodlum. The scientist confides in Naresh that he has invented an atomic ring that when inserted in someones mouth will turn that person invisible, and subsequently passes away. Naresh puts the ring in his mouth, takes off his clothes, turns invisible and escapes. The news of his escape creates waves in the underworld and the Boss and Mr. Verma join forces to find Naresh, kill him, and keep the ring for themselves. For this they send Ram Singh and Lily to Bombay . Naresh and Shyam return back to Bombay and are given Police protection . Meanwhile the goons kill Mr. Mehta. Mala decided to take revenge for her dads murder and joins the CBI. Naresh and Shyam also start working for the CBI to catch the gang. Shyam identifies Lily in a newspaper ad for a club and they go in disguise to find the truth. The remaining story focuses on Naresh, Shyam and Malas struggle against Boss , Verma, Ram Singh and the gang.

==Cast==
*Vinod Khanna ...  Ram Singh 
*Rekha ...  Mala Mehta / Mary 
*Vinod Mehra ...  Naresh Kumar Saxena 
*Rajendra Nath ...  Shyam 
*Madan Puri ...  Mr. Verma 
*Jankidas ...  Professor  Helen ...  Lily  Dulari ...  Mrs. Saxena (Nareshs mom) 
*Brahm Bhardwaj ...  Mr. Mehta (Malas dad) 
*Birbal ...  Traffic cop 
*Hercules ...  Wrestler 
*Iftekhar ...  Police Chief  Rashid Khan ...  Jockey 
*Jagdish Raj ...  Police Inspector 
*Sanjana ...  Seema Saxena 
*Shetty ...  Boss
*Sabeena...  Vamp.

==External links==
*  

 
 
 
 


 
 