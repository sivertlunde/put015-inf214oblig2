Gopalapuranam
{{Infobox film
| name           = Gopalapuranam
| image          = Gopalapuranam.jpg
| caption        = 
| director       = K. K. Haridas
| producer       =  C P Rafeeq, Zakheer Hussain
| writer         =  Mukesh Ramana
| music          = Younaceo
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}} Mukesh and Ramana (actress)|Ramana.

== Plot ==
Gopalakrishnan is the son of Gopalan Nair, who rears cows and sells milk for a living. Gopalan Nair, now in his sixties, finds it hard to continue with his job and calls for the assistance of Remanan, the president of milkmen association and the only friend of his son Gopalakrishnan. Even though Gopalakrishnan could not pursue his studies after his sixth standard, he is not ready for any hard work and is only interested in making money the easy way.

Gopalakrishnans only sister Nandini comes across a person named Vishnu and they soon get married. Vishnu is a gold smuggler who works under Mathew (Saikumar (Malayalam actor)|Saikumar). Gopalakrishnan too joins smuggling business with Vishnu and proves himself as too good in that. But because of his greed he makes a big mistake and now the different gangs are behind him and Vishnu who wants their gold and money back.

== Cast == Mukesh
* Saikumar
* Ramana
* Sujibala
* Rajan P. Dev
* Indrans
* Jagannatha Varma
* Tony
* Kalashala Babu
* Salim Kumar
* Zakker Hussein
* Thesni Khan

== External links ==
* http://www.indiaglitz.com/channels/malayalam/preview/9244.html
* http://www.nowrunning.com/movie/4092/malayalam/gopalapuranam/index.htm
* http://popcorn.oneindia.in/title/503/gopalapuranam.html

 
 
 


 