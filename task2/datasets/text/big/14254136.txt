Nineteen Eighty-Four (film)
 
 
{{Infobox film 
| name           = 1984
| image          = Nineteen Eighty Four.jpg
| image_size     = 220px
| border         = yes
| alt            = 
| caption        = UK theatrical release poster
| director       = Michael Radford
| producer       = Simon Perry
| screenplay     = Michael Radford
| based on       =  
| starring       = {{plainlist|
* John Hurt
* Richard Burton
* Suzanna Hamilton
* Cyril Cusack
}}
| music          = Dominic Muldowney Eurythmics
| cinematography = Roger Deakins
| editing        = Tom Priestley
| studio         = Virgin Films Umbrella-Rosenblum Films
| distributor    = 20th Century Fox  (UK)  Atlantic Releasing  (US) 
| released       =  
| runtime        = 110 minutes  Canbys New York Times review lists the films running time as 117 minutes.  
| country        = United Kingdom
| language       = English 
| budget         = Pound sterling|£5.5 million 
| gross          = $8,430,492
}} novel of the same name. Starring John Hurt, Richard Burton, Suzanna Hamilton, and Cyril Cusack, the film follows the life of Winston Smith in Nations of Nineteen Eighty-Four|Oceania, a country run by a totalitarian government.

The film, which features Burtons last screen appearance, is dedicated to him "with love and admiration."   

==Plot== Oceania under the constant surveillance of the Thought Police. The story takes place in London, the capital city of the territory of Airstrip One (formerly "either England or Britain").  
 the Party Big Brother. A man haunted by painful memories and restless desires, Winston is an everyman who keeps a secret diary of his private thoughts, thus creating evidence of his thoughtcrime. 
 Julia — and they begin an illicit affair. Their first meeting takes place in the remote countryside where they exchange subversive ideas before having sex. Shortly after, Winston rents a room above a pawn shop (in the supposedly safe proletarian area) where they continue their liaison. Julia — a sensual, free-spirited young woman — procures contraband food and clothing on the black market, and for a brief few months they secretly meet and enjoy an idyllic life of relative freedom and contentment together.   

It comes to an end one evening, with the sudden raid of the Thought Police. They are both arrested and its revealed that there is a telescreen hidden behind a picture on the wall in their room, and that the proprietor of the pawn shop, Mr. Charrington, is a covert agent of the Thought Police. Winston and Julia are taken away to be detained, questioned and brutally "rehabilitated" separately. Winston is brought to the Ministry of Love, where  OBrien (Nineteen Eighty-Four)|OBrien, a high-ranking member of the Inner Party whom Winston had previously believed to be a fellow thought criminal and agent of the resistance movement led by the archenemy of the Party, Emmanuel Goldstein, systematically tortures him.
  
OBrien instructs Winston about the states true purpose and schools him in a kind of catechism on the principles of doublethink — the practice of holding two contradictory thoughts in the mind simultaneously. For his final rehabilitation, Winston is brought to Room 101, where OBrien tells him he will be subjected to the "worst thing in the world", designed specifically around Smiths personal phobias. When confronted with this unbearable horror — which turns out to be a cage filled with wild rats — Winstons psychological resistance finally and irretrievably breaks down, and he hysterically repudiates his allegiance to Julia. Now completely subjugated and purged of any rebellious thoughts, impulses, or personal attachments, Winston is restored to physical health and released.

In the final scene, Winston returns to the Chestnut Tree Café, where he had previously seen the rehabilitated thought criminals Jones, Aaronson and Rutherford (themselves once prominent but later disgraced members of the Inner Party) who have since been "vaporized" and rendered List of Newspeak words#Unperson|unpersons. While sitting at the chess table, Winston is approached by Julia, who was similarly "rehabilitated". They share a bottle of Victory Gin and impassively exchange a few words about how they have betrayed each other. After she leaves, Winston watches a broadcast of himself on the large telescreen confessing his "crimes" against the state and imploring forgiveness from the populace.

Upon hearing a news report declaring the Oceanian armys utter rout of the enemy (Eurasia (Nineteen Eighty-Four)|Eurasian)s forces in North Africa, Winston looks at the still image of Big Brother that appears on the telescreen, then turns away and almost silently says "I love you" - a phrase that he and Julia repeatedly used during their relationship, indicating the possibility that he still loves Julia. However, he could also be declaring his love for Big Brother instead, like in the book. The novel unambiguously ends with the words: "He loved Big Brother," whereas the movie seems to deliberately allow for either interpretation. Earlier, during Winstons conversation with Julia in the rented room, he stated that "if they can make me change my feelings, they can stop me from loving you, that would be real betrayal". In the final scene, the "real betrayal" has therefore either been committed or averted, depending on whether the "you" that Winston loves is Big Brother or Julia. 

==Cast==
 
* John Hurt as Winston Smith
* Richard Burton as OBrien (Nineteen Eighty-Four)|OBrien Julia
* Cyril Cusack as Mr. Charrington
* Gregor Fisher as Parsons James Walker Syme
* Andrew Wilde as Tillotson
* Corina Seddon as Mrs. Smith
* Rupert Baderman as Young Winston Smith
* John Boswall as Emmanuel Goldstein
* Phyllis Logan as The Telescreen Announcer (voice)
* Roger Lloyd Pack as Waiter
 
 Big Brother, though only as a still image.

==Production==
In winter 1983, Radford asked his producer to try for the rights to Orwells novel, with few expectations that they were available.  It turned out that the rights were held by Marvin Rosenblum, a Chicago lawyer who had been trying on his own to get such a film produced.    Rosenblum agreed to become an executive producer, and while producer Simon Perry raised the production money from Richard Branson, Radford wrote the script, inspired by his idea to make a "science fiction film made in 1948."   The script was finished in three weeks. 

For the role of OBrien (Nineteen Eighty-Four)|OBrien, Paul Scofield was originally contracted to play the part, but had to withdraw after sustaining a broken leg while filming The Shooting Party.   Anthony Hopkins, Sean Connery and Rod Steiger were all then considered.  Richard Burton, who was living in Haiti, joined the production six weeks into its shooting schedule  and insisted on his costume of a boiler suit being hand-made for him in Saville Row.  

Principal photography began on 19 March 1984, and ended in October 1984.   Some scenes were shot on the actual days noted in Winstons diary (for example: April 4, 1984) as well as at some of the actual locations and settings mentioned in Orwell’s novel. 

The budget was originally £2.5 million but this rose during filming and additional funds were required. 
 cinematographer Roger processing technique called bleach bypass to create the distinctive washed-out look of the films colour visuals. The film is a very rare example of the technique being applied to every release print, rather than the internegative or interpositive; as the silver is retained in the print and cannot be reclaimed by the lab, the cost is higher, but the retained silver gives a "depth" to the projected image.

===Filming===
The opening scenes of the film showing the Two Minutes Hate were filmed in a grass-covered hangar at RAF Hullavington near Chippenham in Wiltshire.  Some scenes set in Victory Square were also filmed at Alexandra Palace in London.   Senate House (University of London) was used for exterior shots of the Ministry of Truth.
 Wandsworth served Docklands of Newham was used as the setting for the proletarian zones. The pawnshop exterior, a pub scene and a scene with a prostitute were filmed in Cheshire Street, in Londons East End, an area Orwell had visited and commented on in his first book, Down and Out in Paris and London. The canteen interiors were filmed in a disused Co-op grain mill at Silvertown.

In contrast, the idyllic, dreamlike "Golden Country", where Winston and Julia repair for their first tryst and which recurs in Winstons fantasies, was filmed in the southwest county of Wiltshire at a natural circle of hills called "The Roundway", near the town of Devizes. The scenes on the train were shot on the Kent and East Sussex Railway.

The film shared a number of locations  with Brazil (1985 film)|Brazil, which was filmed the same year.   An epigram in the closing credits claims that the film was shot during the very months and in the very locales when and where Orwells novel was set.

==Reception==
Upon its U.S. premiere, Vincent Canby said the film was "admirable, bleakly beautiful", though "not an easy film to watch"; according to Canby, "Mr. Burton is fine as the suave, avuncular OBrien - his last film role - and Miss Hamilton, with her little-girl prettiness combined with a steely self-assurance, would seem to be a major find as Julia. Mr. Hurts performance, however, is the films center of gravity. He is splendid, and if his Winston Smith never seems truly tragic, thats in the nature of the Orwell work, which has a journalistic dryness to it that denies tragic possibilities. Most stunning of all are the films production design, by Allan Cameron, and Roger Deakinss photography, from which all the colors of sunlight have been drained."   

Roger Ebert awarded the film 3.5/4 stars, writing that it "penetrates much more deeply into the novels heart of darkness" than previous adaptations, and describing Hurt as "the perfect Winston Smith." 

Review aggregation website Rotten Tomatoes gives the film a 79% "fresh" rating, based on 14 reviews. 

==Controversy over the musical score== electronic Eurythmics score originally intended for the film had been composed entirely by Dominic Muldowney a few months earlier.

Against Radfords wishes, Virgin exercised their right of final cut and replaced Muldowneys musical  ", was released as a single just prior to the album and became one of Eurythmics biggest hits, peaking at number 4 and was awarded a Silver disc for sales in excess of 200,000 copies.  The music video for the single featured clips from the film. The track "Julia" was also released as a single though peaked just outside the Top 40. 
 BAFTA awards in protest of Virgins decision to change the musical score. Eurythmics responded with a statement of their own claiming no knowledge of prior agreements between Virgin and Radford/Muldowney.

In 1999, Muldowneys complete orchestral score (24 tracks in total) was released on a special limited edition CD album under the title Nineteen Eighty-Four: The Music of Oceania, to commemorate the films 15th anniversary. The CD booklet featured previously unseen production photographs and artwork as well as liner notes by Radford.  

On the subsequent MGM DVD release in North America in 2003, the films color is restored to a normal level of saturation and the Eurythmics contributions to the score were removed entirely and replaced with Muldowneys musical cues as Radford had originally intended—although both Eurythmics and Muldowney are still jointly credited in the opening and closing titles. This DVD release was quickly discontinued and currently remains out of print. This version had previously been shown by Channel 4 in the UK in the late-1980s. However, the MGM DVD release of the film in the UK in 2004 features the mixed Eurythmics/Muldowney soundtrack on the English language|English- and French-language audio tracks as well as the original desaturated visuals.

==Awards==
The film won the Best British Film of the Year award at the Evening Standard British Film Awards.
Also It won The Golden Tulip Award at Istanbul International Film Festival in 1985.

==Notes==
 

==See also==
* Nineteen Eighty-Four (1949 English novel) Nineteen Eighty-Four (1954 BBC television adaptation)
* 1984 (1956 film)|1984 (1956 film version)
* 1984 (opera)|1984 (opera)
* 1984 (advertisement)|"1984" (advertisement) for the Apple Macintosh
*Brazil (1985 film)|Brazil, which was released a few months later (and during production was initially known as 1984 and a ½) 
* List of films featuring surveillance

==References==
 

==External links==
 
*  
*  
*  
*  , ITN Source, 8 October 1984
*   from film.virtual-history.com

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 