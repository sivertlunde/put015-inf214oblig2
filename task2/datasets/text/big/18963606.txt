Amarilly of Clothes-Line Alley
{{Infobox film
 | name = Amarilly of Clothes-Line Alley
 | image = File:Amarilly of Clothes-line Alley poster.jpg
 | caption = Film poster
 | director = Marshall Neilan
 | writer = Belle K. Maniates (novel)  Frances Marion William Scott Kate Price
 | producer =
 | cinematography = Walter Stradling
 | distributor = Artcraft Pictures Corporation
 | budget =
 | released =  
 | country = United States Silent English intertitles
 | runtime = 67 minutes 
}}
Amarilly of Clothes-Line Alley is a 1918 silent film directed by Marshall Neilan, written by Frances Marion and based on a Belle K. Maniates novel. 

==Plot== William Scott), who gets her a job as a cigarette girl in his cafe after a fire unfairly causes her to lose her job as a theater scrubwoman. While working as a cigarette girl, she meets Gordon Phillips (Norman Kerry), a handsome and wealthy but frivolous young man, who is a society sculptor.

Terry becomes jealous when Amarilly starts hanging out with Gordon, and he breaks off the engagement. Gordon offers Amarilly a job with his wealthy and snobbish aunt, Mrs. Phillips (Ida Waterman). When the neighborhood is quarantined after a breakout of scarlet fever, Mrs. Phillips decides to take the time to teach Amarilly high class manners in a Pygmalion (play)|Pygmalion-like experiment. However, once she discovers her nephew has fallen in love with Amarilly, she turns against her. Mrs. Phillips tries to humiliate Amarilly by inviting her family over for a social party.

Amarilly is outraged and returns to her old home.  She sees Terry and invites him for supper.  He is delighted, and on the way to her house, he stops to buy expensive 50 cent violets, even though he had earlier passed up violets at 15 cents. He is shot by accident, and barely makes it to Amarillys house before collapsing. Fortunately, Terry survives. Amarilly visits him in the hospital and tells him that when he gets out, they have a date at City Hall.

The final scene is five years later. Amarilly is in a side car on Terrys motor bike; they both are nicely dressed and seem to be doing well. Then it is revealed under the blanket she has a baby, and behind Terry is a little boy.

==Cast==
* Mary Pickford - Amarilly Jenkins William Scott - Terry McGowen Kate Price - Mrs. Americus Jenkins
* Ida Waterman - Mrs. David Phillips
* Norman Kerry - Gordon Phillips
* Fred Goodwins - Johnny Walker
* Margaret Landis - Colette King Tom Wilson - Snitch McCarthy

unbilled
*Wesley Barry - Amarillys Brother
*Frank Butterworth - Amarillys Brother
*George Hackathorne - Amarillys Brother
*Marcia Manon - Woman in Dance Hall
*Antrim Short - Amarillys Brother
*Gertrude Short - Gossip
*Herbert Standing - Father Riordan
*Larry Steers - One of Gordons Friends
*Gustav von Seyffertitz - Surgeon

==References==
 

==External links==
* 
* 
* , available for free download at  

 
 

 
 
 
 
 
 
 
 
 
 
 