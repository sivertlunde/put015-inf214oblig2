Up in Central Park (film)
{{Infobox film
| name           = Up in Central Park
| image          = Up_in_Central_Park_1948_Poster.jpg
| caption        = Theatrical release poster
| border         = yes
| director       = William A. Seiter
| producer       = Karl Tunberg
| screenplay     = Karl Tunberg
| based on       =  
| starring       = {{Plainlist|
* Deanna Durbin
* Dick Haymes
* Vincent Price
}}
| music          = Johnny Green (director)
| cinematography = Milton R. Krasner Otto Ludwig
| studio         = Universal Pictures
| distributor    = Universal Pictures
| released       =  
| runtime        = 84 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Up in Central Park is a 1948 American musical comedy film directed by William A. Seiter and starring Deanna Durbin, Dick Haymes and Vincent Price. Based on the play Up in Central Park by Herbert Fields with a screenplay by Karl Tunberg, the film is about a newspaper reporter and the daughter of an immigrant maintenance man who help expose political corruption in New York City in the 1870s.      

==Plot==
In New York City in the 1870s, as the city prepares for the upcoming election, corrupt political boss William Tweed (Vincent Price) and his Tammany Hall political machine are working hard to re-elect their candidates, including Mayor Oakley (Hobart Cavanaugh), in order to continue exploiting the coffers of the city and state. The one voice opposing Boss Tweeds organization is John Matthews (Dick Haymes), a young naïve reporter for The New York Times.

When Irish immigrant Timothy Moore (Albert Sharpe) and his singing daughter Rosie (Deanna Durbin) arrive in New York City hoping for a better life, they are set upon immediately by Rogan (Tom Powers), one of Boss Tweeds men. The illiterate Timothy agrees to vote twenty-three times for the Tammany ticket, and is rewarded with $50 and an invitation to Boss Tweeds victory party. At the party, Rosie inadvertently overhears Boss Tweeds latest plan to embezzle the citys coffers through the unnecessary renovation of Central Park. Fearing that Rosie may know about his plan, Boss Tweed appoints the unknowing Timothy to the post of Park Superintendent.

Sometime later, John meets Timothy, the new Park Superintendent. Unaware that John is a reporter, Timothy reveals that some of the parks zoo animals are actually being raised for Boss Tweeds consumption. After Johns story appears in the paper, Timothy is fired, but when Rosie appeals to an infatuated Boss Tweed to give her father another chance, he agrees. Also smitten with Rosie, John offers Timothy a job with his newspaper. Soon after, John tries to convince Rosie of Boss Tweeds dishonesty, but is unsuccessful. Later that night, Rosie almost discovers Boss Tweeds true character when he makes numerous, lecherous advances toward her during dinner, but is interrupted by Timothy, who mistakenly believes that he was invited.

After Rosie arranges a meeting between John and Boss Tweed, the political boss offers to sponsor Johns proposed novel if he agrees to quit his job at The New York Times. John refuses the bribe. Later, John discovers Timothy attending grammar school classes; with the help of a schoolteacher named Miss Murch, the old man learns of Boss Tweeds corruption. When Timothy tries to tell his daughter about Boss Tweeds true character, she refuses to listen, having become romantically involved with the married man.

Through Boss Tweeds influence, Rosie soon auditions for an opera company, and though she is offered a role in an upcoming production, Tweed insists that she be cast in the current show. Meanwhile, Timothy, upset over his daughters involvement with Tweed, approaches John and offers to help him gain evidence against the political boss by breaking into city hall and examining the citys financial records. The two men are discovered by a drunken Mayor Oakley when he wanders into his office, but they trick him into giving his copies of Boss Tweeds financial dealings to the newspaperman.

After their corruption is exposed in the newspapers, Boss Tweed and his associates prepare to flee the country, but Tweed offers no apologies to Rosie for his actions, stating his belief in the rights of the strong over the weak. After he leaves her, Rosie wanders through Central Park, where she is discovered by Timothy and John. After requesting her fathers forgiveness, Rosie is reunited with John.

==Cast==
* Deanna Durbin as Rosie Moore
* Dick Haymes as John Matthews
* Vincent Price as Boss Tweed
* Albert Sharpe as Timothy Moore
* Tom Powers as Rogan
* Hobart Cavanaugh as Mayor Oakley
* Thurston Hall as Governor Motley
* Howard Freeman as Myron Schultz
* Mary Field as Miss Murch
* Tom Pedi as OToole
* Moroni Olsen as Big Jim Fitts
* William Skipper as Dancer
* Nellie Fisher as Dancer    

==Production==
===Soundtrack===
* "When She Walks in the Room" (Sigmund Romberg and Dorothy Fields)
* "Carousel in the Park" (Sigmund Romberg and Dorothy Fields)
* "Oh Say, Can You See (What I See)" (Sigmund Romberg and Dorothy Fields)
* "Pace, pace mio Dio" from the opera La forza del destino (Giuseppe Verdi, Francesco Maria Piave)   

==Reception==
In his 1948 review in The New York Times, T.M.P. wrote that the film was "somewhat less successful as entertainment than the play."    The producers decision to reduce the number of songs does not help matters.
 
Regarding the casting, the reviewer wrote, "Durbin is fresh looking in a nice girlish way and displays a convincing amount of naïveté, and Mr. Havimes is agreeable enough, though he looks and acts more like a professional juvenile than a seasoned reporter. Albert Sharpe contributes some mil dhumor as Miss Durbins doting parent."  Regarding the casting of Vincent Price in the role of Boss Tweed, the reviewer wrote, "a more inappropriate choice could hardly be imagined."  Finally, the film fails to exploit the obvious filming location choices in Central Park, restricting them to a few shots of the zoo, the carousel, and a bit of greenery around the superintendents house. According to the reviewer, the director should have "moved his camera out onto the meadows instead of focusing so much on plush, stuffy interiors." 

In his review for Rovi, Hal Erickson wrote that the best scene of the film was Currier & Ives ballet, one of the few holdovers from the stage version. 

==See also==
* Up in Central Park
* William M. Tweed
* Tammany Hall

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 