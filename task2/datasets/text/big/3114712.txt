We Jam Econo
{{Infobox film name     = We Jam Econo image          = wejamecono.jpg caption  = promo poster director       = Tim Irwin producer       = Rocket Fuel Films Productions Keith Schieron editing         =  starring       = D. Boon George Hurley Mike Watt distributor    =  released   =    runtime        = 91 minutes language = English
}} documentary about Tim Irwin San Pedro, California, after two years in production.

Poignant recent interviews with the bands two surviving members Mike Watt and George Hurley, as well as first-person anecdotes from notable musicians including Ian MacKaye, Flea (musician)|Flea, Henry Rollins and Thurston Moore, complement the archival concert and interview footage of the band, creating an informative and moving film for those interested in the band or punk rock in general.

The title is a lyric from their song "The Politics of Time." Its also referred to in a comment made near the end of the film by Mike Watt, in a 1985 interview, when the band is asked if they have anything else to say.  He answers for them: "We jam econo."  Econo was local slang for economic and described the bands dedication to low-cost record production and touring. It also describes the bands (and burgeoning underground independent music scenes) do-it-yourself attitude and philosophy.

== Interviews ==

Film includes interviews with the following individuals (in alphabetical order):

*Milo Aukerman
*Joe Baiza Kevin Barrett
*Scott Becker
*Jello Biafra
*Richard Bonney
*Jack Brewer
*Dez Cadena
*Joe Carducci
*Nels Cline
*Byron Coley
*Ed Crawford
*Brother Dale
*Richard Derrick John Doe 
*Chuck Dukowski
*Ray Farrell Flea
*Michael C. Ford
*Carlos Guitarlos
*Grant Hart
*Richard Hell
*Pat Hoed
*Rob Holzman
*Randall Jahnson Kjehl Johansen
*Curt Kirkwood
*Martin Lyon
*Ian MacKaye David Markey
*Mike Martt
*J Mascis
*Brother Matt
*Stephen McCllellan
*Vince Meghrouni
*Richard Meltzer
*Mike Mills
*Thurston Moore
*W.T. Morgan
*Chris Morris 
*Keith Morris
*Brendan Mullen
*Colin Newman
*Greg Norton
*Raymond Pettibon
*Tony Platon
*Lee Ranaldo David Rees
*Lisa Roeland
*Nanette Roeland
*Kira Roessler
*Henry Rollins
*Kurt Schellenbach Spot
*Urinals John Talley-Jones Tom Watson
*Jean Watt

==DVD==

The 2-disc DVD (with 16-page booklet) was released on June 27, 2006 on Plexifilm.

DISC 1: 
Feature "We Jam Econo - The Story of the Minutemen"
* Original music videos  for: 
**"This Aint No Picnic" (directed by Randall Jahnson) John Talley-Jones)
**"King of the Hill" (directed by Randall Jahnson)
* 19 Deleted Scenes and Interviews
* Uncut Bard College Interview (56min)

DISC 2:
Three live performances:
 The Starwood, Los Angeles, CA - November 18, 1980, songs include:
#Swing To The Right
#Fascist
#Joe McCarthys Ghost
#Paranoid Chant
#Tension
#Contained
#Fanatics
#Art Analysis
#Issued
#Validation
#Definitions
#Warfare
#Sickles & Hammers
#Hollering
#On Trial

*  , Washington, D.C. - 1984, songs include:
#Big Foist
#Retreat
#Toadies
#Anxious Mo-fo
#Love Dance
#Static
#Search
#Cut
#Plight
#Working Men Are Pissed
#Ack Ack Ack
#Life As A Rehearsal
#Beacon Sighted Through Fog
#The Only Minority
#Mutiny In Jonestown
#Maybe Partying Will Help
#Political Song For Michael Jackson To Sing
#The Roar Of The Masses Could Be Farts
#Mr. Robots Holy Orders
#One Reporters Opinion
#God Bows To Math
#Please Dont Be Gentle With Me
#Joe McCarthys Ghost
#The Punch Line
#Definitions
#The Anchor
#Bob Dylan Wrote Propaganda Songs
#This Aint No Picnic
#There Aint Shit On TV Tonight
#No Exchange
#Self-Referenced
#Dream Told By Moto Corona
#I Felt Like A Gringo
#Do You Want New Wave Or Do You Want The Truth?
#Little Man With A Gun In His Hand

* Acoustic Blowout (Public-access television Show) - Hollywood, CA - 1985, songs include:
#Corona
#Themselves
#The Red And The Black "The Red and the Black" by Blue Öyster Cult and "Green River" by Creedence Clearwater Revival appear on Disc One as the extra "Copy Songs." Also, "Aint Talkin Bout Love" was part of the acoustic performance but has been removed due to licensing issues. 
#Badges
#I Felt Like A Gringo
#Time Green River  
#Lost
#Ack Ack Ack
#History Lesson Pt. II
#Tour Spiel
#Little Man With A Gun In His Hand

== See also == Minutemen
* Plexifilm

== References ==
 

== External links ==
*   features video excerpts
*   from the  
*  
*  
*  

 
 

 
 
 