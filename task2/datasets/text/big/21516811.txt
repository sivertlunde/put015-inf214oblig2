Big Fella
{{Infobox film
| name           = Big Fella
| image          =
| image size     =
| caption        =
| director       = J. Elder Wills
| producer       = Henry Passmore   J. Elder Wills
| writer         = Claude McKay (novel) Ingram DAbbes   Fenn Sherie James Hayter
| music          = G. H. Clutsam   Will Grosz   Eric Ansell   Jack Beaver
| cinematography = Cyril Bristow George Stretton H.A.R. Thomson		 	 	
| editing        = Brereton Porter Douglas Robertson
| distributor    =
| released       = 8 April 1935
| runtime        = 85 minutes
| country        = United Kingdom English
}}
 musical drama film directed by J. Elder Wills and starring Paul Robeson, Elisabeth Welch and Roy Emerton. It is loosely based on the novel Banjo by Harlem Renaissance writer Claude McKay.

==Plot==
Big Fella is set on the docks and streets of Marseilles. Paul Robeson stars in the leading role, as a street-wise but honest dockworker who struggles with deep issues of integrity and human values. Elisabeth Welch plays opposite him as a café singer in love with him. Robeson’s wife, Eslanda Robeson, appears as the café owner. 

==Reception==
The movie received high praise, particularly for the music, featuring Robeson and Welch, Robesons performance.

==Cast==
* Paul Robeson as Banjo
* Elisabeth Welch as Amanda Manda
* Roy Emerton as Spike James Hayter as Chuck
* Lawrence Brown as Corney
* Eldon Gorst as Gerald Oliphant 
* Marcelle Rogez as Marietta
* Eric Cowley as Ferdy Oliphant 
* Joyce Kennedy as Mrs. Oliphant  
* Dino Galvani as Gendarme Anthony Holles as Gendarme 
* Margaret Rutherford as Nanny

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 


 