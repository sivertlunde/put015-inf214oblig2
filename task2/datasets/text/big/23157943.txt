Sweet Inquest on Violence
{{Infobox film
| name           = Sweet Inquest on Violence
| image          =
| image size     =
| caption        =
| director       = Gérard Guérin
| producer       = Gérard Guérin Gérard Mordillat
| writer         = Gérard Guérin Jacques Leduc
| narrator       =
| starring       = Michael Lonsdale
| music          =
| cinematography = François Catonné
| editing        = Marc Bodin-Joyeux
| distributor    =
| released       = 27 October, 1982
| runtime        = 98 minutes
| country        = France
| language       = French
| budget         =
| preceded by    =
| followed by    =
}}

Sweet Inquest on Violence ( ) is a 1982 French drama film directed by Gérard Guérin. It was entered into the 1982 Cannes Film Festival.   

==Cast==
* Michael Lonsdale - Ash, le financier
* Elise Caron - France
* Jeanne Herviale - La vieille dame
* Albert Marcoeur - Musicien
* Emmanuelle Debever - Marianne
* Claude Duneton - Paulo laveugle
* Nada Strancar - La soeur dun terroriste / Visiteuse de lappartement
* Robert Kramer - Le biologiste
* Prune Bergé - Madame Ash
* Mustapha Ami - Terroriste Caro - Terroriste
* Eva Hiller - Terroriste
* Zarah Lorelle - Terroriste
* Claude Hébert - Terroriste Walter Jones - Walter

==References==
 

==External links==
* 

 
 
 
 
 

 