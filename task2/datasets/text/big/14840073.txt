Kiss Kiss (Bang Bang)
 

{{Infobox film
| name           = Kiss Kiss (Bang Bang)
| image          =
| image_size     =
| caption        =
| director       = Stewart Sugg James Richardson
| writer         = Stewart Sugg
| starring       = Martine McCutcheon Stellan Skarsgård Chris Penn Paul Bettany
| music          = John Dankworth
| cinematography = Tony Pierce-Roberts
| editing        = Jim Clark
| distributor    = DVision Europa Filmes GAGA Communications Millennium Storm
| released       =  
| runtime        = 97 min. UK
| language       = English
| budget         = Pound sterling|£3,000,000 
| gross          =
}}
 2001 England|English comedy film written and directed by Stewart Sugg. It features Stellan Skarsgård, Chris Penn, and Paul Bettany.

== Plot ==

Felix is a hit-man who wants out of the business. He takes up a job to look after a reclusive man named Bubba who is like a giant kid, he does not understand people and does not know much, never having seen the outside world before. But Felix has bigger problems. He wants to patch up his relationship with his ex-girlfriend, and hes being targeted by his old colleagues who are not letting him get away from his past so easily.

==Cast==
*Stellan Skarsgård  as Felix
*Chris Penn  as Bubba
*Paul Bettany  as Jimmy
*Allan Corduner  as Big Bob
*Jacqueline McKenzie  as Sherry
*Martine McCutcheon  as Mia
*Sienna Guillory  as Kat
*Ashley Artus  as Mick Foot
*Belinda Stewart-Wilson as Camilla

== External links ==
*  
*  

 


 