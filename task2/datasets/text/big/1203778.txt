Titanic (1953 film)
 
{{Infobox film
| name           = Titanic
| image          = Titanic 1953 film.jpg
| image_size     = 225px
| caption        = film poster
| director       = Jean Negulesco 
| producer       = Charles Brackett
| writer         = Charles Brackett Richard L. Breen Walter Reisch
| starring       = Clifton Webb Barbara Stanwyck Robert Wagner Audrey Dalton Harper Carter Thelma Ritter Brian Aherne Richard Basehart
| music          = Sol Kaplan
| cinematography = Joseph MacDonald
| editing        = Louis R. Loeffler
| distributor    = 20th Century Fox NBC (TV) MGM (Austria)
| released       =  
| runtime        = 98 minutes
| country        = United States
| language       = English
| budget         = $1,805,000  
| gross          = $2,250,000 (US) 
}}
Titanic is a 1953 American drama film directed by Jean Negulesco. Its plot centers on an estranged couple sailing on the maiden voyage of the  , which took place in April 1912.

==Plot== Basque immigrant. Once aboard he seeks out his runaway wife, Julia (Barbara Stanwyck). He discovers she is trying to take their two unsuspecting children, 18-year-old Annette (Audrey Dalton) and ten-year-old Norman (Harper Carter), to her hometown of Mackinac, Michigan, to raise as down-to-earth Americans rather than rootless elitists like Richard himself.

As the ship prepares for departure, her captain, E. J. Smith (Brian Aherne), receives a hint from the shipping company representative that a record-setting speedy passage would be welcomed.
  defrocked for alcoholism.

When Annette learns her mothers intentions, she insists on returning to Europe with her father on the next ship as soon as they reach America. Julia concedes that her daughter is old enough to make her own decisions, but she insists on keeping custody of Norman. This angers Richard, forcing Julia to reveal that Norman is not his child, but rather the result of a one-night stand after one of their many bitter arguments. Upon hearing that, he agrees to give up all claim to Norman. Richard joins Maude, Earl, and George Widener in the lounge to play contract bridge with them. The next morning, when Norman reminds Richard about a shuffleboard game they had scheduled, Richard coldly brushes him off. 

Meanwhile Giff falls for Annette at first glance. At first she repulses his brash attempts to become better acquainted, but eventually she warms to him. That night, Giff, Annette and a group of young people sing and play the piano in the dining room, while Captain Smith watches from a corner table. 

Second Officer Lightoller (uncredited Edmund Purdom) expresses his concern to Captain Smith about the ships speed when they receive two messages from other ships warning of iceberg sightings near their route. Smith, however, assures him that there is no danger. 

That night, however, a lookout spots an iceberg dead ahead. Although the crew tries to steer clear of danger, the ship is gashed below the waterline and begins taking on water. When Richard finds the captain, he insists on being told the truth: the ship is doomed. He tells his family to dress warmly but properly; then they head outside.
 
Richard and Julia have a tearful reconciliation on the boat deck, as he places Julia and the children into a lifeboat. Unnoticed by Julia, Norman gives up his seat to an older woman and goes looking for his nominal father. When one of the lines becomes tangled, preventing the lifeboat from being lowered, Giff climbs down and fixes the problem, only to lose his grip and fall into the water. His unconscious body is dragged into the boat.

Meeker disguises himself as a woman to get aboard a lifeboat but Maude Young notices his shoes and unmasks him in front of the others in the lifeboat. At the other end of the spectrum of courage and unselfishness, George Healey heads down into one of the boiler rooms to comfort trapped crewmen.

As the Titanic is in her final moments, Norman and Richard find each other. Richard tells a passing steward that Norman is his "son" and then tells the boy that he has been proud of him every day of his life. Then they join the rest of the doomed passengers and the crew in singing the hymn "Nearer, My God, to Thee". The Titanic s bow plunges, forcing her stern high in the air, then the ship rapidly slides into the icy water.

==Cast==
*Clifton Webb as Richard Ward Sturges
*Barbara Stanwyck as Julia Sturges
*Robert Wagner as Gifford "Giff" Rogers
*Audrey Dalton as Annette Sturges
*Thelma Ritter as Maude Young Edward J. Smith
*Richard Basehart as George S. Healey
*Allyn Joslyn as Earl Meeker
*James Todd as Sandy Comstock
*Frances Bergen as Madeleine Astor
*William Johnstone as John Jacob Astor IV
*Harper Carter as Norman Sturges (uncredited)
*Edmund Purdom as Second Officer Charles Lightoller (uncredited)
*Mae Marsh as Woman to whom Norman gives his seat (uncredited)

==Production== Terry Moore was set to play the role of Annette Sturges, on condition that she would finish production of Man on a Tightrope on time. 

==Reception==
According to the film aggregator website, Rotten Tomatoes, Titanic holds an 88% "Fresh" rating, based on 8 reviews. 

Variety Magazine reviewed the film positively stating, "but by the time the initial 45 or 50 minutes are out of the way, the impending disaster begins to take a firm grip on the imagination and builds a compelling expectancy." 

Pauline Kael was not impressed with the pictures special effects.  She wrote:  "the actual sinking looks like a nautical tragedy on the pond in Central Park." 

==Awards and nominations== Directors Guild of America Award.

==References==
 

==External links==
 
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 