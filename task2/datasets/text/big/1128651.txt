Not One Less
 
 
{{Infobox film
 | name               = Not One Less
 | image              = Not One Less.jpg
 | alt                = DVD cover divided into three panels. The first depicts a serious-looking young Chinese woman with braided hair; she is standing, surrounded by blurred faces. The second panel shows a group of laughing children, all looking forward. The third panel shows a seated laughing boy, surrounded by the words, Not One Less. Other writing on the cover says, "From Zhang Yimou, award-winning director of Raise the Red Lantern", and the tagline "In her village, she was the teacher. In the city, she discovered how much she had to learn."
 | caption            = DVD release cover
 | film name = {{Film name| simplified         = 一个都不能少
  | traditional        = 一個都不能少
  | pinyin             = yí ge dōu bù néng shǎo}}
 | writer             = Shi Xiangsheng
 | producer           = Zhang Yimou
 | distributor        = Columbia Tristar
 | released           =  
 | starring           = Wei Minzhi  Zhang Huike
 | director           = Zhang Yimou
 | music              = San Bao Hou Yong
 | editing            = Zhai Ru
 | runtime            = 106 minutes
 | language           = Mandarin
 | country            = China
 | budget             =
}}
 |alt=A middle-aged Chinese man standing at a podium, wearing a Hawaiian shirt and lei]]
Not One Less ( ) is a 1999 drama film by Chinese director Zhang Yimou, adapted from Shi Xiangshengs 1997 story "A Sun in the Sky" ( ).   .  It was produced by Guangxi Film Studio and released by China Film Group Corporation in mainland China, and distributed by Sony Pictures Classics and Columbia Tristar internationally.
 Chinese countryside. education reform documentary style with a troupe of non-professional actors who play characters with the same names and occupations as the actors have in real life, blurring the boundaries between drama and reality.

The domestic release of Not One Less was accompanied by a Chinese government campaign aimed at promoting the film and cracking down on Copyright infringement|piracy. Internationally, the film was generally well-received, but it also attracted criticism for its ostensibly political message; foreign critics are divided on whether the film should be read as praising or criticizing the Chinese government. When the film was excluded from the 1999&nbsp;Cannes Film Festivals competition section, Zhang withdrew it and another film from the festival, and published a letter rebuking Cannes for politicization of and "discrimination" against Chinese cinema. The film went on to win the Venice Film Festivals Golden Lion and several other awards, and Zhang won the award for best director at the Golden Rooster Awards.

== Background ==
In the 1990s, primary education reform had become one of the top priorities in the Peoples Republic of China. About 160&nbsp;million Chinese people had missed all or part of their education because of the Cultural Revolution in the late 1960s and early 1970s,  .  and in 1986 the National Peoples Congress enacted a law calling for nine years of compulsory education.  .  By 1993, it was clear that much of the country was making little progress on implementing nine-year compulsory education, so the 1993–2000&nbsp;seven-year plan focused on this goal. One of the major challenges educators faced was the large number of rural schoolchildren dropping out to pursue work.  .  Another issue was a large urban–rural divide: funding and teacher quality were far better in urban schools than rural, and urban students stayed in school longer. 

== Production and cast == Keep Cool).    For this film, he cast only amateur actors  whose real-life names and occupations resembled those of characters they played in the film—as The Philadelphia Inquirer s Steven Rea described the performances, the actors are just "people playing variations of themselves in front of the camera".  For instance, Tian Zhenda, who played the mayor, was the real-life mayor of a small village,  .  and the primary actors Wei Minzhi and Zhang Huike were selected from among thousands of students in rural schools.   .  (The names and occupations of the films main actors   are listed in the table below.) The movie was filmed on location at Chicheng Countys Shuiquan Primary School, and in the city of Zhangjiakou; Not One Less, credits.  both locations are in Hebei province.

The movie was filmed in a Documentary film|documentary-like, "Neorealism (art)|neorealist"  style involving hidden cameras and natural lighting.       .  There are also, however, elements of heavy editing—for example, Shelly Kraicer noted that many scenes have frequent, rapid cut (transition)|cuts, partially as a result of filming with inexperienced actors.  . 

Zhang had to work closely with government censors during production of the film. He related how the censors "kept reminding   not to show China as too backward and too poor", and said that on the title cards at the end of the movie he had to write that the number of rural children dropping out of school each year was one million, although he believed the number was actually three times that.  Not One Less was Zhangs first film to enjoy government support and resources.  . "也是从那部影片开始，张艺谋将自己的创造能力与政府资本和主导媒体的宣传资源结合起来。" 

=== Cast ===
 
{| class="wikitable"
|-
! Name !! Role !! Real-life occupation
|-
| Wei Minzhi || Teacher Wei || middle school student
|-style="background:#D3D3D3"
| Zhang Huike || class troublemaker, school dropout || primary school student
|-
| Tian Zhenda || Mayor Tian || mayor of a village in Yanqing county
|-style="background:#D3D3D3"
| Gao Enman || Teacher Gao || village teacher in Yanqing county
|-
| Sun Zhimei || helps Wei search for Zhang Huike in the city || middle school student
|-style="background:#D3D3D3"
| Feng Yuying || TV station receptionist || ticket clerk
|-
| Li Fanfan || TV show host || TV show host
|-style="background:#D3D3D3"
| Zhang Yichang || sports recruiter || sports instructor
|-
| Xu Zhanqing || brickyard owner || mayor of a village in Yanqing county
|-style="background:#D3D3D3"
| Liu Hanzhi || Zhang Huikes sick mother || villager
|-
| Ma Guolin || man in bus station || clerk
|-style="background:#D3D3D3"
| Wu Wanlu || TV station manager || deputy manager of a broadcasting station
|-
| Liu Ru || train station announcer || announcer for a broadcasting station
|-style="background:#D3D3D3"
| Wang Shulan || stationery store clerk || stationery store manager
|-
| Fu Xinmin || TV show director || TV station head of programming
|-style="background:#D3D3D3"
| Bai Mei || restaurant owner || restaurant manager
|-
|}
 

== Plot == yuan bonus if all the students are still there when he returns.

 
When Wei begins teaching, she has little rapport with the students: they shout and run around instead of copying their work, and the class troublemaker, Zhang Huike, insists that "shes not a teacher, shes Wei Chunzhis big sister!" After putting the lesson on the board, Wei usually sits outside, guarding the door to make sure no students leave until they have finished their work. Early in the month, a sports recruiter comes to take one athletic girl, Ming Xinhong, to a special training school; unwilling to let any students leave, Wei hides Ming, and when the village mayor (Tian Zhenda) finds her, Wei chases after their car in a futile attempt to stop them.

One day, after trying to make the troublemaker Zhang apologize for bothering another student, Wei discovers that Zhang has left to go find work in the nearby city of Zhangjiakou. The village mayor is unwilling to give her money for a bus ticket to the city, so she resolves to earn the money herself, and recruits the remaining students to help. One girl suggests that they can make money by moving bricks in a nearby brickyard, and Wei begins giving the students mathematical exercises centered on finding out how much money they need to earn for the bus tickets, how many bricks they need to move, and how much time it will take. Through these exercises and working to earn money, her rapport with the class improves. After earning the money, she reaches the bus station but learns that the price is higher than she thought, and she cannot afford a ticket. Wei ends up walking most of the way to Zhangjiakou.

{{Location map+|China|float=right|caption=Location of Zhangjiakou, relative to Beijing|places=
 
 
|alt=Map of China, with Zhangjiakou and Beijing marked. The cities are close together in northeast China; Zhangjiakou is slightly northwest of Beijing.}}

In the city, Wei finds the people that Zhang was supposed to be working with, only to discover that they had lost him at the train station days before. She forces another girl her age, Sun Zhimei, to help her look for Zhang at the train station, but they do not find him. Wei has no success finding Zhang through the public address system and "missing person" posters, so she goes to the local television station to broadcast a missing person notice. The receptionist (Feng Yuying) will not let her in without valid identification, though, and says the only way she can enter is with permission from the station manager, whom she describes as "a man with glasses". For the rest of the day, Wei stands by the stations only gate, stopping every man with glasses, but she does not find the station manager, and spends the night asleep on the street. The next day the station manager (Wu Wanlu) sees her at the gate again, through his window, and lets her in, scolding the receptionist for making her wait outside.

Although Wei has no money to run an ad on TV,  the station manager is interested in her story and decides to feature Wei in a talk show special about rural education. On the talk show, Wei is nervous and hardly says a word when the host (Li Fanfan) addresses her, but Zhang—who has been wandering the streets begging for food—sees the show. After Wei and Zhang are reunited, the station manager arranges to have them driven back to  village, along with a truckload of school supplies and donations that viewers had sent in. Upon their return, they are greeted by the whole village. In the final scene, Wei presents the students with several boxes of colored chalk that were donated, and allows each student to write one character on the board. The film ends with a series of title cards that recount the actions of the characters  after the film ends, and describe the problem of poverty in rural education in China.

== Themes ==
While most of Zhangs early films had been historical epics, Not One Less was one of the first to focus on contemporary China.   .  The films main theme involves the difficulties faced in providing rural education in China. When Wei Minzhi arrives in Shuiquan village, the teacher Gao has not been paid in six months and the school building is in disrepair,  .  and chalk is in such short supply that Gao gives Wei specific instructions limiting how large her written characters should be.  Wei sleeps in the school building, sharing a bed with several female students. The version of the film released overseas ends with a series of title cards in English, the last of which reads, "Each year, poverty forces one million children in China to leave school. Through the help of donations, about 15% of these children return to school."

 .|alt=Black-and-white head portrait of an older man with short hair and sunglasses]] Iranian directors Abbas Kiarostami and Mohsen Makhmalbaf,   .   and Zhang has openly acknowledged the influence of Kiarostami in this film.    Zhang Xiaoling of the University of Nottingham argues that Zhang Yimou used the documentary perspective in order to suggest that the story is an accurate reflection of most rural areas in China,  .  while Shelly Kraicer believes that his "simultaneous presentation of seemingly opposing messages" is a powerful artistic method in of itself, and that it allows Zhang to circumvent censors by guaranteeing that the movie will include at least one message that they like.  Jean-Michel Frodon of Le Monde maintains that the film was produced "in the shadow of two superpowers"  and needed to make compromises with each.   

The film  addresses the prominent place that bureaucracy, and verbal negotiation and struggle, occupy in everyday life in China. Many scenes pit Wei against authority figures such as the village mayor, the announcer in the train station, and the TV station receptionist who also acts as a "gatekeeper".  .   .  Aside from Wei, many characters in the film show a "blind faith" in authority figures.  While she lacks money and power, Wei overcomes her obstacles through sheer obstinacy and ignorant persistence,  .    suggesting that speech and perseverance can overcome barriers.  Wei becomes an example of "heroic obstinacy"  .  and a model of using determination to face "overwhelming odds".  For this reason, the film has been frequently compared to Zhangs 1993 The Story of Qiu Ju, whose heroine is also a determined, stubborn woman; likewise, Qiu Ju is also filmed in a neo-realistic style, set partially in contemporary rural China and partially in the city,     .  and employs mostly amateur actors.   

Not One Less  portrays the mass media as a locus of power: Wei discovers that only someone with money or connections can gain access to a television station, but once someone is on camera she or he becomes part of an "invisible media hegemony" with the power to "manipulate social behavior", catching peoples attention where paper advertisements could not and moving cityfolk to donate money to a country school.  .  The power of television within the films story, according to Laikwan Pang of the Chinese University of Hong Kong, reflects its prominent place in Chinese society of the late 1990s, when domestic cinema was floundering but television was developing quickly; Pang argues that television-watching forms a "collective consciousness" for Chinese citizens, and that the way television unifies people in Not One Less is an illustration of this.  . 

 , where the "urban" half of the film takes place|alt=Aerial view of a city, surrounded mostly by desert. In the foreground and background are hills with sparse vegetation.]] Confucian values capitalist and individualistic society.  . 
 connections to avoid becoming an "outsider" in the city.  Frequent cuts show Wei and Zhang wandering aimlessly in the streets, Zhang begging for food, and Wei sleeping on the sidewalk; when an enthusiastic TV host later asks Zhang what part of the city left the biggest impression, Zhang replies that the one thing he will never forget is having to beg for food.  A.O. Scott of The New York Times compared the "unbearable" despair of the films second half to that of Vittorio De Sicas 1948 Bicycle Thieves. 

== Reception ==

=== Cannes withdrawal ===
  The Road Home was selected for the 1999 Cannes Film Festivals Official Selection, the most prestigious competition in the festival, where several of Zhangs earlier films had won awards.  .   .  The rationale is uncertain; Shelly Kraicer and Zhang Xiaoling claim that Cannes officials viewed the Not One Less  happy ending, with the main characters conflicts resolved by the generosity of city dwellers and higher-up officials, as pro-China propaganda,  .   .  while Zhu Ying claims that the officials saw it and The Road Home as too anti-government.  .  Rather than have his films shown in a less competitive portion of the festival, Zhang withdrew them both in protest, stating that the movies were apolitical.    In an open letter published in the Beijing Youth Daily, Zhang accused the festival of being motivated by other than artistic concerns, and criticized the Western perception that all Chinese films must be either "pro-government" or "anti-government", referring to it as a "discrimination against Chinese films". 

=== Critical response ===
 ,     and Philip Kemp of   s Desson Howe called the ending "flag-waving",    and   and Leigh Paatsch of the Herald Sun each pointed out that, while the film is "deceptive " positive    at face value, it has harsh criticism "bubbling under the surface".    Chinese critics Liu Xinyi and Xu Su of Movie Review recognized the dispute abroad over whether the film was pro- or anti-government, but made no comment; they praised the film for its realistic portrayal of hardships facing rural people, without speculating about whether Zhang intended to criticize or praise the governments handling of those hardships.  Hao Jian of Film Appreciation, on the other hand, was more critical, claiming that the movie was organized around a political message and was intended to be pro-government. Hao said that Not One Less marked the beginning of Zhangs transformation from an outspoken independent director to one of the governments favorites.  . "从《一个都不能少》开始，张艺谋开始按照政治上的主导意识来安排自己影片中人物的动机发展和行动的走向与结局。该片剧作的转折点力量（爱民的电视台台长）的编排具有中国语境内的政治保险性。" 
 Hou Yongs Red Sorghum. Italian neo-realists."  Another well-received part of the film was the segment in which Wei teaches math by creating practical examples out of her attempt to raise money for the bus to Zhangjiakou; in the Chinese journal Teacher Doctrines, Mao Wen wrote that teachers should learn from Weis example and provide students with practical exercises.  . 

Wei Minzhis character received mixed reactions: Scott described her as a "heroic" character who demonstrates how obstinacy can be a virtue,  whereas Richard Corliss of Time (magazine)|Time says she is "no brighter or more resourceful than  ".    Reactions to the city portion of the movie were also mixed: while Zhang describes the second half of the film as an eloquent commentary on Chinas urban-rural divide  .  and Kevin Lally calls it "startling",  Kemp criticizes it for being a predictable "Victorian cliché". 

=== Box office and release ===
 
Rights to distribute the film were purchased by the China Film Group Corporation, a state-sponsored organization, and the government actively promoted the film.  It was officially released in mainland China in  ,  although there were showings as early as mid-February.  Sheldon H. Lu reports that the film grossed Renminbi|¥18&nbsp;million, an average amount, in its first three months of showing; by the end of its run in November, it sold ¥40&nbsp;million at the box office.  .  (In comparison, Zhangs 2002 film Hero (2002 film)|Hero would earn ¥270&nbsp;million three years later.)  .  Nevertheless, Not One Less was the highest-grossing domestic film of 1999, and Laikwan Pang has called it a "box office success".  .  In the United States, the film was released in theaters on   2000, and grossed $50,256 in its first weekend and $592,586 overall;    The release was handled by Sony Pictures Classics,  and home video distribution by Columbia Tristar;   Not One Less was Columbias first Chinese film.   

Lu warns that domestic box office sales are not reliable indicators of a films popularity in mainland China, because of piracy and because of state or social group sponsorship;  many workers were given free tickets to promote the film, and a 1999 report claimed that more tickets were purchased by the government than by individuals.  .  The film was more popular than most government-promoted films touting the party line and Lu claims that it had "tremendous social support",  but Pang points out that its success was "not purely egalitarian, but partly constructed."  . 
 China Copyright Office issued a notice forbidding unauthorized production or distribution of the film. This was the first time China had enacted special copyright protections for a domestic film.     . "这是我国第一次对国产影片的版权实行如此正式的保护。"  On   1999, Hubei provinces Culture Office issued an "Urgent Notice for Immediate Confiscation of Pirated Not One Less VCDs", and two days later the Culture Office and movie company joined forces to conduct raids on ten audio-video stores, seizing pirated discs from six of them. 

=== Awards === Golden Rooster, mainland Chinas most prestigious award ceremony,  and the film was voted one of the top three of the year in the Hundred Flowers Awards.  Awards the film won or was nominated for are listed below.

 
{| class="wikitable"
|-
! Awards Year
! Category
! Result
! Notes
|- Venice Film Festival    1999
|Golden Lion
| 
|
|- Lanterna Magica award
| 
|
|- Sergio Trasatti award
| 
|
|- UNICEF award
| 
|
|- Golden Rooster Awards     1999
|Best Director
| 
|
|- Hundred Flowers Awards     1999
|Best Picture
|  with two other films 
|- Shanghai Film Critics Awards  1999
|Film of Merit
| 
|
|- Best Director
| 
|
|- Beijing Student Film Festival  1999
|Jury Award: Best Film
| 
|
|- China Obelisk Film Awards  1999
|Outstanding Film Director
|  with two other directors
|- Outstanding Feature Film
|  with nine other films
|-
|São Paulo International Film Festival  1999
|Audience Award: Best Feature
| 
|
|- European Film Awards  1999
|Screen International Award
| 
|
|- Golden Bauhinia Awards   *    2000
|Top 10 Chinese films
|  with nine other films
|- Young Artist Awards  2000
|Best International Film
| 
|
|- Best Performance in an International Film (Young Performer)
| 
|
|- Kinema Junpo Awards  2001
|Best Foreign Language Film Director
| 
|
|- Isfahan International Festival of Films for Children & Young Adults  2001
|Golden Butterfly
| 
|
|- Changchun Film Festival  2008
|Golden Deer: Outstanding Film in Rural Theme
|  with four other films
|-
|}
 

== References ==

=== Notes ===
 

=== Bibliography ===
 
*  
*  
*  
*   
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
 

== External links ==
 
*  
*  
*  
*   at the Chinese Movie Database
*   at Sony Pictures Classics

 
 

 

 
 
 
 
 
 
 
 
 