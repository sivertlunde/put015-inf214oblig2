Angadi Theru
{{Infobox film
| name =  Angaadi Theru
| image = Angadi-theru-poster.jpg
| director = Vasanthabalan
| producer = K. Karunamoorthy C. Arunpandian
| writer = Vasanthabalan Jeyamohan Mahesh Anjali Anjali A. Pandi
| music = Vijay Antony G. V. Prakash Kumar
| cinematography = Richard M. Nathan
| editing = A. Sreekar Prasad
| released =  
| runtime = 157 minutes
| language = Tamil
| country = India
| studio = Ayngaran International
| distributor = Ayngaran International
| budget =
| gross =  18 crore
}} Tamil romantic Mahesh and Anjali in Indian submissions for the Academy Award for Best Foreign Language Film but lost to Peepli Live. It released in Telugu as Shopping Mall.

==Plot==
Jyothi Lingam (Mahesh) is a bright student and son of a mason who leads a happy life in his village near Tirunelveli. One day tragedy strikes as his father the only earning member dies in an accident while crossing an unmanned railway gate, and young boy now has to look after his mother and two sisters.

Due to circumstances, he is forced to abandon his studies, though he comes first in his school in the board exam. Through a canvassing agent, he and his friend Marimuthu, get jobs as sales boys in a textile showroom in Ranganathan Street in Chennai.

Jyothi, along with hundreds of others, are employed at the Senthil Murugan Stores run by the big businessman. In each floor at the textile showroom, there are around 50 to 60 sales boys and girls who work in pitiable conditions from early in the morning to late night, without any rest.

He meets Kani (Anjali), a fiery independent girl. The difficult and harrowing times in the store bring them together as they face up to a cruel and lewd store supervisor Karungali (director A Venkatesh), who beats up the boys and molests the girls when they play around during duty hours. As Jyothi says there is no escape from the “Jail like” atmosphere in the shop where employees are treated more like slave labour in a concentration camp than without any human dignity. Angadi Theru is about how these two survive in a concentration camp like condition and what happens when fate perpetually smiles very cruelly at them.

Sneha lights up the happenings in a small cameo as herself by doing a commercial advertisement for that Stores.
This film is considered to be a milestone in Tamil cinema due its raw content. This movie is ironic to the real happenings at the two Popular Stores in Ranganathan Street in Chennai.

==Cast== Mahesh as Jyothi Lingam Anjali as Kani
* A. Venkatesh (director)|A. Venkatesh as Karungali Pandi as Marimuthu Sneha as herself (Cameo appearance)
* John Vijay as Director

==Reception==
 
The film met with widespread critical acclaim. Behindwoods mentioned, "Angadi Theru is an eye opener to all those who are on the rosier side of life,it’s an overdose of emotions, but you don’t mind it: simply because the characters have handled the scenes so skilfully". Parvathi Srinivasan from Rediff described the film as a, "kind of cinema you keep hoping for and only rarely get," labeling it, "A must watch." 
Many reviewers appreciated Anjalis performance with Behindwoods commenting that, "The girl looks every bit her role with an impressive gamut of emotions running through her face."  Chennaionline.com gave credit to the theme of the film by stating that, "Vasanthabalan must be applauded for courageously presenting us a film that looks into the darker side of the glittery world of massive show rooms. 

==Awards==
Angaadi Theru won the 2010 Best Film Award at the Chennai International Film Festival on 24 December 2010.  It also won accolades in the following award ceremonies.

;2010 Vikatan Awards Anjali
* Best Story - Vasanthabalan

;Norway Tamil Film Festival
* Best Film - K. Karunamoorthy, C. Arunpandian
* Best Actress - Anjali

;Vijay Awards Best Film - K. Karunamoorthy, C. Arunpandian Best Director - Vasanthabalan Best Actress - Anjali

;Filmfare Awards South Best Director - Tamil - Vasanthabalan Best Actress - Tamil - Anjali Best Female Playback Singer - Tamil - Shreya Ghoshal

==Soundtrack==
{{Infobox album 
| Name = Angaadi Theru
| Longtype = to Angaadi Theru
| Type = Soundtrack
| Artist = G. V. Prakash Kumar, Vijay Antony
| Genre = Film soundtrack
| Label = Ayngaran Music An Ak Audio Tamil
| Year = 2009
| Producer = G. V. Prakash Kumar   Vijay Antony
}}

The soundtrack album was composed by G. V. Prakash Kumar, while Vijay Antony composed two songs and also scored the background music . Lyrics were penned by Na. Muthukumar. Shreya Ghoshal won Filmfare Award for Best Female Playback Singer – Tamil for the song Un Perai Sollum Podhe. 

Tracklist
{{track listing
| extra_column = Singer(s) | music_credits = yes | all_lyrics = Na. Muthukumar Ranjith | music1 = Vijay Antony
| title2 = Kannil Theriyum   | extra2 = G. V. Prakash Kumar | music2 = G.V. Prakash Kumar Pandi | music3 = G.V. Prakash Kumar Hamsika | music4 = G.V. Prakash Kumar
| title5 = Unn Perai Sollum  | extra5 = Naresh Iyer, Shreya Ghoshal, Haricharan | music5 = G.V. Prakash Kumar
| title6 = Yenge Poveno Enn  | extra6 = Benny Dayal, MK Balaji, Janaki Iyer | music6 = Vijay Antony
}}

==References==
 

==Further reading==
* Christopher, Michael & Helen Staufer (2011). “Urban. Village. Urban-Village. Angaditheru and its mofussil department store society in Chennai”, in: manycinemas 1, 24-37

==External links==
*  

 
 

 
 
 
 