The Skin I'm In (film)
{{Infobox film
| name           = The Skin Im In
| image          = The Skin Im In Poster 1.jpg
| alt            = Official poster for THE SKIN IM IN (2012), a feature-length documentary by Broderick Fox
| caption        = Official Poster
| director       = Broderick Fox
| producer       = Lee Biolos Broderick Fox
| starring       = Broderick Fox   Rande Cook   Zulu
| music          = Ronit Kirchman
| cinematography = Broderick Fox
| editing        = Broderick Fox
| released       =  
| runtime        = 86 minutes
| country        = United States
| language       = English
}}
 American filmmaker, scholar, and professor Broderick Fox. The film had its world premiere at the 2012 Byron Bay Film Festival and had its United States premiere at the Arizona International Film Festival,  where it was awarded a Special Jury Award for Best Personal Filmmaking.  The Skin Im In will be released for stream and download on October 1, 2013 through The Orchard.

== Cast ==
*Rande Cook 
*Broderick Fox
*Zulu

== Production ==
Fox was inspired to create the film after an incident in 2005 where Fox was discovered on some subway tracks in the Berlin subway with head trauma and a blood alcohol level of 0.47.    The incident prompted Fox to document his personal journey to explore himself, spirituality, and his art.  The film was shot in five countries and includes collaborations with Canadian first nations artist Rande Cook and African-American artist Zulu to create a full back tattoo to memorialize the journey. 

== Reception ==
The Times Colonist gave a favorable review for The Skin Im In, praising Fox for his honesty in the film and stating that the collaboration with Cook and Zulu was one of the films highlights. 

== Soundtrack ==
{{Infobox album |  
  Name        =  The Skin Im In |
  Type        = film |
  Artist      = Ronit Kirchman and Broderick Fox|
  Cover       = |
  Released    = 2012 |
  Recorded    =  |
  Genre       = Soundtrack, instrumental|
  Length      =  |
  Label       = Wild River Records|
  Producer    = |
Chronology  =  |
}}

The soundtrack for the film was composed by Ronit Kirchman. In order to assist her with the creation of the score, Kirchman was named a 2012 Sundance Institute|Sundance/Time Warner Fellow in film composition, which gave her some financial support towards the soundtracks recording and performance.  Kirchman later performed a live remix set of the score for The Skin Im In as part of the 2012 Sundance Film Festival. 

=== Track listing ===

{{tracklist
| headline1       = 
| extra_column    = Performer

| writing_credits = yes

| title1          = 0.47
| writer1         = Ronit Kirchman
| extra1          = 
| length1         = 1:12

| title2          = Future Ancestors (Suite)
| writer2         = Ronit Kirchman
| extra2          = 
| length2         = 2:34

| title3          =  Ether Body
| writer3         = Ronit Kirchman
| extra3          = 
| length3         = 1:02

| title4          = OCD
| writer4         = Ronit Kirchman
| extra4          = 
| length4         = 1:52

| title5          = The Unbelievable Lightness of Boyhood
| writer5         = Ronit Kirchman
| extra5          = 
| length5         = 1:25

| title6          = Silence = Death
| writer6         = Ronit Kirchman
| extra6          = 
| length6         = 0:39

| title7          = Talking Around Pictures
| writer7         = Ronit Kirchman
| extra7          = 
| length7         = 1:39

| title8          = Back and Blueprint
| writer8         = Ronit Kirchman
| extra8          = 
| length8         = 1:20

| title9          = Pain Game
| writer9         = Ronit Kirchman
| extra9          = 
| length9         = 1:48

| title10         = Disappearing Body
| writer10        = Ronit Kirchman
| extra10         = 
| length10        = 1:13

| title11         = From Berlin With Love (Suite)
| writer11        = Ronit Kirchman
| extra11         = 
| length11        = 3:33

| title12          = Thrill Me
| writer12         = Ronit Kirchman
| extra12          =
| length12         = 2:17

| title13         = Intimacy, Meet Rick
| writer13         = Ronit Kirchman
| extra13          = 
| length13         = 2:03

| title14          = Whatever (feat. Broderick Fox)
| writer14         = Ronit Kirchman
| extra14          = 
| length14         = 3:03

| title15          = The End in the Beginning
| writer15         = Ronit Kirchman
| extra15          = 
| length15         = 2:28

| title16          = The Power of Choice
| writer16         = Ronit Kirchman
| extra16          = 
| length16         = 2:00

| title17          = Numinous Newness
| writer17         = Ronit Kirchman
| extra17          = 
| length17         = 0:50

| title18          = A Woman Knows
| writer18         = Ronit Kirchman
| extra18          = 
| length18         = 1:02

| title19          = I See Myself 
| writer19         = Ronit Kirchman
| extra19          = 
| length19         = 1:21

| title20          = The Big Party (feat. Broderick Fox)
| writer20         = Ronit Kirchman
| extra20          =
| length20         = 5:07

| title21         = Thirsty for Heroes
| writer21        = Ronit Kirchman
| extra21         = 
| length21        = 1:44

| title22         = Now!!!
| writer22        = Ronit Kirchman
| extra22         = 
| length22        = 1:04

| title23         = True Mirrors
| writer23         = Ronit Kirchman
| extra23          = 
| length23         = 0:48

| title24          = Embodied (to Live Life Is to Be Free)
| writer24         = Ronit Kirchman
| extra24          = 
| length24         = 2:01

| title25          = Fuck It
| writer25         = Broderick Fox
| extra25          = 
| length25         = 1:43

}}

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 