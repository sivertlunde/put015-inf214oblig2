Sontham
 

{{Infobox film
| name           = Sontham
| image          =
| alt            =  
| caption        =
| director       = Srinu Vaitla
| producer       = S.Sompally V.R.Kanneganti
| writer         = Kona Venkat Sunil Namitha Aryan Rajesh Rohit M. S. Narayana
| music          = Devi Sri Prasad
| cinematography =
| editing        =
| studio         = JD Arts
| distributor    =
| released       = August 23, 2002
| runtime        =
| country        = India
| language       = Telugu
| budget         =
| gross          =
}} Telugu language film released on August 23, 2002. It was directed by Srinu Vaitla. Sunil (actor)|Sunil, Aryan Rajesh, Rohit and Namitha played the lead roles. Devi Sri Prasad composed the score. It performed moderately well at the box office. 

== Plot ==

Vamsy (Aryan Rajesh), Nandu (Namitha) and Bose (Rohit) are childhood friends. Vamsy and Nandu grew up very close. Bose falls in love with a beautiful girl named Neha. When Bose tells Vamsy about her, she deems it one more love affair that is bound to fail. Vamsy feels that friendship is more valuable than love and that love spoils a friendship. Nandu, who wanted to share her love, goes back to the shelter as she feels Vamsy might get offended.

Vamsy leaves for New Zealand to supervise the overseas operations of his fathers company. In New Zealand, Vamsy realizes that he is in love with Nandu. When Vamsy returns to India, he finds out that Nandu is already engaged to someone else. Just before her marriage, Nandu receives a bouquet from Vamsys friend addressing both Vamsy and Nandu. Realizing that Vamsy loves her, Nandu runs after him at the Airport.

== Cast ==

* Aryan Rajesh as Vamsy
* Rohit as Bose Sunil as Sheshagiri or Shesham
* Namitha as Nandu
* M. S. Narayana as Bogeswar Rao
* Chitram Seenu
* Ramachandra
* Jhansi as Venkata Lakshmi
* Dharmavarapu Subramanyam as Subbu
* Adivi Sesh as Venkat (Namithas Fiancé)

== Soundtrack ==

{{Infobox album
| Name = Sontham
| Longtype = to Sontham
| Type = Soundtrack
| Artist = Devi Sri Prasad
| Cover =
| Released =
| Recorded = Feature film soundtrack
| Length = Telugu
| Label =
| Producer = Devi Sri Prasad
| Reviews =
| Last album = Thotti Gang (2002)
| This album = Sontham (2002)
| Next album = Varsham (2004 film)|Varsham (2003)
}}

The music and background score was composed by Devi Sri Prasad and lyrics for all songs were penned by  Sahiti.

{{Track listing
| extra_column = Singer(s) Tippu | length1 =04:46
| title2 = Telusuna  | extra2 = Chitra | length2 =04:36
| title3 = Yeppudu | extra3 = Mallikarjun | length3 =03:05
| title4 = Enati Varaku  | extra4 = Shaan (singer)|Shaan, Sumangali | length4 =04:37
| title5 = Akkado Ekkado  | extra5 = Malgudi Subha | length5 =05:08
| title6 = Naayudo Naayudo  | extra6 = Devi Sri Prasad | length6 =05:30
| title7 = Yeppudu(Female) | extra7 = Sumangali | length3 =02:46
}}

== External links ==
 

 
 
 

 
 