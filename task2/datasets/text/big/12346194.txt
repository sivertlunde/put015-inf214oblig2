The Dark Side of the Sun (film)
 
{{Infobox film
| name           = The Dark Side of the Sun
| image          =
| caption        = 
| director       = Božidar Nikolić
| producer       = Anđelo Aranđelović  Mile Citaković  Susan Haken  Duško Mihailov  Boris Nitchoff
| writer         = Andrew Horton  Nikola Jovanović  Željko Mijanović
| starring       = Brad Pitt
| music          = Vojislav Kostić  Kornelije Kovač
| cinematography = Bozidar Nikolić
| editing        = Petar Jakonić
| distributor    = Avala Film  Cinequanon Pictures International Inc
| released       =  
| runtime        = 101 minutes
| country        = United States  Yugoslavia
| language       = English
| budget         =
| gross          =
}}
 drama film skin disease. The film was directed by Božidar Nikolić.

The footage for the film was shot in 1988 but due to the outbreak of Croatian War of Independence (which started in 1991) it had to be abandoned and much footage was lost.   

The missing pieces were eventually recovered and it was released officially in 1997, by which time Brad Pitt was a major star. The film was later featured in an episode of Cinema Insomnia.   

==Plot==
Rick is a young American who suffers of a rare skin disease which prevents him from exposing himself to any kind of light, especially sunlight. After having tried several cures without success, his father takes him to a village in Yugoslavia where they meet a healer, who is supposed to save him. But the treatment does not work and Rick decides to forget about his illness and enjoy life, feeling the sun on his skin for the first time. In the short time he has left a young American actress enters his life. 

==Main cast== Guy Boyd ...  Father
*Brad Pitt ...  Rick
*Cheryl Pollak ...  Frances
* Constantin Nitchoff
* Milena Dravić ...  Mother
* Gorica Popović ...  Nina
* Sonja Savić
* Nikola Jovanović
* Boro Begović
* Milan Sretenović  (as Milan Sretenović-Globus)
* Andjelo Arandjelović  (as Angelo Arangelović)
* Sreten Mitrović
* Stole Aranđelović ...  Vidar (the healer) (as Stole Arangelović)
* Ras Rastoder
* Vladan Banović
* Milorad Novaković
* Pedja Rolović
* Vanja Vasić
* Tatjana Vadnjal
* Tanja Petrić

== References ==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 