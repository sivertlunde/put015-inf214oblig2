The Five Pennies
{{Infobox film
| name           = The Five Pennies
| image          = The Five Pennies.jpg
| image_size     =
| caption        =
| director       = Melville Shavelson Jack Rose
| writer         = Robert Smith Jack Rose Melville Shavelson
| narrator       =
| starring       = Danny Kaye Barbara Bel Geddes Harry Guardino Bob Crosby Louis Armstrong Tuesday Weld
| music          = Thorton W. Allen Sylvia Fine M.W. Sheafe Leith Stevens
| cinematography = Daniel L. Fapp
| editing        = Frank P. Keller
| distributor    = Paramount Pictures
| released       =  
| runtime        = 117 mins.
| country        = United States
| language       =
| budget         =
| gross          = $3 million (est. US/ Canada rentals) 
}}
The Five Pennies was a semi-biographical 1959 film starring Danny Kaye as cornet player and bandleader Loring Red Nichols. Other cast members included Barbara Bel Geddes, Harry Guardino, Bob Crosby, Louis Armstrong, Susan Gordon, and Tuesday Weld. The film was directed by Melville Shavelson.

The film received four  ), Best Original Song (Danny Kayes wife Sylvia Fine), Best Cinematography (Daniel L. Fapp), and Best Costumes (Edith Head).

The real Red Nichols recorded all of Kayes cornet playing for the film soundtrack. The other musicians in Reds band were not asked to provide their musical contributions and the sound of his "band" was supplied by session players.

==Plot summary==
 nickel equals five penny|pennies). As their popularity peaks, their young daughter Dorothy (Susan Gordon) contracts polio and the family leaves the music business, moving to Los Angeles. When Dorothy becomes a teen (Tuesday Weld) she learns of her fathers music career and persuades him go on a comeback tour. The tour borders on failure until several notable musicians from Nichols past appear to save the day.

==Trivia==
 
The drummer in the scenes with Louis Armstrong was Vel Tino, Sr.

Features an energetic Charleston and an exaggerated Tango with Lizanne Truex and Danny Kaye, who hammed up the Tango during the rehearsal to such a degree that the director added it to the scene.

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 