Life Express (2010 film)
 
{{Infobox film
| name           = Life Express
| image          = LifeExpress2010Poster.jpg
| alt            =  
| caption        = Film poster
| director       = Anup Das
| producer       = Sanjay Kalate
| story          = Anup Das
| screenplay     = Mandira Chakraborty   Shailendra Tyagi
| starring       = Rituparna Sengupta Kiran Janjani Yashpal Sharma Alok Nath Nandita Puri Anjan Srivastav Daya Shankar Pandey Vijayendra Ghatge
| music          = Roop Kumar Rathod
| cinematography = Anil Chandel
| editing        = Sanjib Datta
| studio         = Sky Motion Pictures
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}}
Life Express is a 2010 Indian drama film about a career-minded woman who aborts her first child, then decides to have a baby through a surrogate mother.

== Plot ==
Mumbai-based Tanvi Sharma, married to Financer, Nikhil, for 3 years, is thrilled when she is not only promoted as Assistant Vice-President with her employer, ICBI Bank, but also tests positive for motherhood. But her joy will be short-lived when she must abort the child as it comes in the way of her promotion as Vice-President - the venue to power, position and wealth - as well as incur the wrath of Nikhil. The duo do make up, and decide to have a child through a surrogate who will live with them during the pregnancy period. A broker, Shukla, arranges a surrogate, Gauri, an impoverished village-based mother of two children, and the wife of an unemployed idol-artist, Mohan. While dealing with pressures of the stigma attached of nursing a child sired through another woman from family and friends, she must also deal with the fact that a naive Gauri may decide to keep the child.
 

==Cast==
*Rituparna Sengupta as Tanvi N. Sharma
*Kiran Janjani as Nikhil Sharma
*Yashpal Sharma as Mohan
*Alok Nath as Mr. Singh - Tanvis dad
*Nandita Puri as Mrs. Shanti Singh - Tanvis mom
*Anjan Srivastav as Paresh
*Daya Shankar Pandey as Shukla
*Vijayendra Ghatge as Tanvis boss
*Divya Dutta as Gauri
*Sapan Saran as Soni

==Soundtrack==
{{Infobox album |  
| Name       = Life Express
| Type       = Soundtrack
| Artist     = Roop Kumar Rathod
| Cover      = 
| Released   = 2010
| Recorded   = 
| Genres     = World Music
| Length     =
| Label      = 
| Producer   = Roop Kumar Rathod
}}
Roop Kumar Rathod was the music director. Shakeel Azmi wrote the lyrics.
The album has 3 tracks.
{|class="wikitable" width="60%"
! Song Title !! Singers
|-
| Phiki Phiki Si Lage Zindagi Tere Pyaar Ka Namak Jo Na Ho|| Udit Narayan, Shreya Ghoshal
|-
| Thodisi Kami Rahey Jaati Hai ||
|-
| Phool Khilade Sakhon Par ||
|-
|}

==References==
 

== External links ==
*  

 
 
 


 