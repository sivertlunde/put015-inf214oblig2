Love in the Wilderness
{{Infobox film
| name           = Love in the Wilderness
| image          =
| caption        =
| director       = Alexander Butler 
| producer       = G.B. Samuelson
| writer         = Gertrude Page (novel) 
| starring       = Madge Titheradge   C.M. Hallard   Campbell Gullan   Maudie Dunham
| music          = 
| cinematography = 
| editing        = 
| studio         = G.B. Samuelson Productions
| distributor    = General Film Distributors 
| released       = May 1920
| runtime        = 5,000 feet  
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         =
| preceded_by    =
| followed_by    =
}} silent drama novel of Southern Rhodesia. The film was shot in California. 

==Cast==
* Madge Titheradge as Enid Davenport 
* C.M. Hallard as Keith Meredith 
* Campbell Gullan as Hon. Dicky Byrd 
* Maudie Dunham as Nancy Johnson 
* Hubert Davies as George Whiting  Frances Griffiths as Marion Davenport 
* Lenore Lynard as Mrs. Meredith

==References==
 

==Bibliography==
* Bamford, Kenton. Distorted Images: British National Identity and Film in the 1920s. I.B. Tauris, 1999.
* Low, Rachael. History of the British Film, 1918-1929. George Allen & Unwin, 1971.

 

==External links==
* 
 
 
 
 
 
 
 
 
 
 
 

 