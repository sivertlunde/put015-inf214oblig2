Matana MiShamayim
{{Infobox film |
  name     =       A Gift from Above|
  image          =  |
  caption        =  מתנה משמיים|
  director       = Dover Kosashvili|
  producer       = Marek Rozenbaum|  
  writer         = Dover Kosashvili   (Dubi Rubinstein)|
  starring       = Yuval Segal Rami Heuberger Moni Moshonov Lior Ashkenazi Ronit Yudkevitz Dover Kosashvili Menashe Noy|
  music          = Ioseb Bardanashvili |
  cinematography = Laurent Dailland|
  editing        = Yael Perlov|
  distributor    = Israel Film Fund Transfax  Keshet Broadcasting|
  released       =   |
  runtime        = 108 minutes|
  country        = Israel| Hebrew Judaeo-Georgian language|Judaeo-Georgian |
  budget         = 12.7 M NIS|
}}
 
A Gift from Above (2003,  i drama film, directed by Dover Kosashvili.  Like two of Kosashvilis other films, Late Marriage (2001) and Im Hukim (1998), the dialogue on this film is partly in the Judaeo-Georgian language and partly in Hebrew. And since the Judaeo-Georgian language is a dialect, spoken by a small community,  most of the cast had to learn it for this production.

The movie was a nominee to the Ophir Award in 11 categories. 

==Plot==

The characters on this movie live like a closed tribe. Most of them live on the same block.  Among themselves they speak a rare language. They put a lot of pressure on each other to get married only within their community. They are not much concerned about obeying the countrys laws. And many of them work in the same place, Ben Gurion Airport luggage department,  or help their community members, who do work there, to steal passengers suitcases.

The community does not have a lot of money, but its folklore is rich and its life is full of parties, sex, violence and excitement.  The most exciting happening there is the operation theyre plotting, step by step, for stealing cargoes of diamonds from the airplanes. The plotting includes putting all the blame on two volunteers from within the community itself.

==References==
 

 
 
 
 

 