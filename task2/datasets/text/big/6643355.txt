LOL (2006 film)
{{Infobox film
| name = LOL
| image = LOL film poster.jpg
| director = Joe Swanberg 
| producer = Joe Swanberg
| writer   = Kevin Bewersdorf Joe Swanberg C. Mason Wells
| starring = Kevin Bewersdorf Joe Swanberg C. Mason Wells Tipper Newton Brigid Reagan Greta Gerwig Kate Winterich
| music    = Kevin Bewersdorf
| cinematography = Joe Swanberg
| editing  = Joe Swanberg
| distributor =  Washington Square Films
| released =  
| runtime  = 81 minutes
| language = English
| budget   = 
}}
LOL is a 2006 independent mumblecore film by Joe Swanberg that examines the impact of technology on social relations.

==Plot==
The movie follows three recent college graduates in   as he makes out with his girlfriend. Chris conducts relationships by cellphone. Alexs preoccupation with chat rooms sabotages a potential face-to-face relationship with a girl he meets at a party.

==Cast==
* Kevin Bewersdorf — Alex
* C. Mason Wells — Chris
* Joe Swanberg — Tim
* Tipper Newton — Walter
* Brigid Reagan — Ada
* Greta Gerwig — Greta
* Kate Winterich — Tessa

==External links==
*  
*  
*  

 

 
 
 


 