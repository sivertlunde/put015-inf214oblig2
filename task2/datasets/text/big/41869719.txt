Chikkejamanru
{{Infobox film
| name = Chikkejamanru
| image =
| caption =
| director = V. Ravichandran
| writer = R. Selvaraj
| starring = V. Ravichandran  Gouthami   Jai Jagadish
| producer = Anam Gopalkrishna Reddy
| music = Hamsalekha
| cinematography = Jhonny Lal
| editing = K. Narasaiah
| studio = Sri Venkatakrishna Films
| released = 1992
| runtime = 140 minutes
| language = Kannada
| country = India
| budget =
}}
 drama film Tamil film Telugu as Venkatesh and Vijayashanti.

The film featured original score and soundtrack composed and written by Hamsalekha and was produced by Anam Gopalkrishna Reddy for Sri Venkatakrishna Films banner.

== Cast ==
* V. Ravichandran
* Gouthami
* Jai Jagadish
* Mukhyamantri Chandru
* Shivakumar
* Tennis Krishna
* Srilalitha
* Kaminidharan
* Vijay

== Soundtrack ==
The music was composed and written by Hamsalekha. 

{{track listing
| headline = Track listing
| extra_column = Singer(s)
| lyrics_credits = no
| collapsed = no
| title1 =  Rama Rama Rama
| extra1 = S. P. Balasubrahmanyam, S. Janaki
| lyrics1 =
| length1 =
| title2 = Nammura Nyaya Devaru
| extra2 = S. P. Balasubrahmanyam, K. S. Chithra
| lyrics2 =
| length2 =
| title3 = Buguri Buguri
| extra3 = S. P. Balasubrahmanyam, K. S. Chithra
| lyrics3 =
| length3 =
| title4 = Premada Hoogara
| extra4 = S. P. Balasubrahmanyam
| lyrics4 =
| length4 =
| title5 = Sobane Enniramma
| extra5 = S. P. Balasubrahmanyam, S. Janaki
| lyrics5 =
| length5 =
| title6 = Rama Rama Rama (sad)
| extra6 = S. P. Balasubrahmanyam, S. Janaki
| lyrics6 =
| length6 =
}}

==See also==
* Chinna Gounder
* Chinarayudu

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 


 