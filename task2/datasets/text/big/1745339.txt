A Lot like Love
 
{{Infobox film
| name =  A Lot like Love
| image = A_Lot_Like_Love_poster.JPG
| caption = Original poster
| director = Nigel Cole
| producer = Armyan Bernstein Kevin J. Messick
| writer = Colin Patrick Lynch
| starring =Ashton Kutcher Amanda Peet
| music = Alex Wurman
| cinematography = John de Borman
| editing = Susan Littenberg
| studio  = Touchstone Pictures Buena Vista Pictures
| released =      
| runtime = 107 minutes
| language = English
| budget = $30 million    
| gross = $42,886,719 
| country = United States
}}
A Lot like Love is a 2005 romantic comedy-drama film directed by Nigel Cole and starring Ashton Kutcher and Amanda Peet. The screenplay by Colin Patrick Lynch focuses on two individuals whose relationship slowly evolves from lust to friendship to romance over the course of seven years. The films tag line was: theres nothing better than a great romance to ruin a perfectly good friendship.

==Plot==
Constructed as a series of chapters that take place at a turning point in each characters life, the story moves from seven years in the past to three years to two and finally arrives in the present day. Emily Friehl (Amanda Peet) and Oliver Martins (Ashton Kutcher) first encounter is on a flight from Los Angeles to New York City, during which they join the mile high club. He has hopes of becoming an Internet entrepreneur and, certain of his future success, gives her his mothers phone number and suggests she call him in six years to see if his prediction came true.

Three years later, facing the prospect of spending New Years Eve alone, Emily finds Olivers number and calls him, and the two meet for dinner. Thus starts a series of reunions with the passing of time, as each drifts in and out of relationships with others, Oliver and his business partner Jeeter start an on-line diaper service, and Emily becomes a successful photographer. Each time they meet, one appears to be settled and content while the other is struggling to make headway in both life and career. Eventually they come to the realization that each is exactly the person the other one needs for fulfillment.

==Cast==
*Ashton Kutcher as Oliver Martin
*Amanda Peet as Emily Friehl
*Kathryn Hahn as Michelle
*Kal Penn as Jeeter
*Ali Larter as Gina
*Taryn Manning as Ellen Martin
*Tyrone Giordano as Graham Martin
*Amy Aquino as Diane Martin Gabriel Mann as Peter
*Jeremy Sisto as Ben Miller
*Moon Bloodgood as Bridget

==Production==
The film was shot on location in New York City, Los Angeles, and Antelope Valley.

===Music===
{{Infobox album  
| Name      = A Lot like Love (Music from the Motion Picture)
| Type      = soundtrack
| Artist    = Various artists
| Cover     = A Lot Like Love OST.jpg
| Released  =  
| Genre     =
| Length    =
| Label     = Columbia Records
| Producer  =
}}
A soundtrack album for the film was released by Columbia Records on April 12, 2005.    

====Track list====
{{tracklist
| extra_column = Artists
| title1       = Semi-Charmed Life
| extra1       = Third Eye Blind
| length1      = 4:27
| title2       = Walkin on the Sun
| extra2       = Smash Mouth
| length2      = 3:25
| title3       = Save Tonight
| extra3       = Eagle Eye Cherry
| length3      = 3:52
| title4       = Mint Car
| extra4       = The Cure
| length4      = 3:29 Mad About You
| extra5       = Hooverphonic
| length5      = 3:43 Trouble
| extra6       = Ray Lamontagne
| length6      = 3:59
| title7       = Know Nothing Travis
| length7      = 2:59
| title8       = If You Leave Me Now Chicago
| length8      = 3:56
| title9       = Brighter Than Sunshine Aqualung
| length9      = 4:01
| title10      = Hands of Time
| extra10      = Groove Armada
| length10     = 4:21
| title11      = Look What Youve Done Jet
| length11     = 3:50
| title12      = Breathe (2 AM)
| extra12      = Anna Nalick
| length12     = 4:39
| title13      = Maybe Its Just Me
| extra13      = Butch Walker
| length13     = 3:20
}}

==Critical reception==
A Lot like Love received mixed reviews from critics and currently holds a 41% rating on Rotten Tomatoes.

Manohla Dargis of The New York Times said the film "isnt half bad and every so often is pretty good, filled with real sentiment, worked-through performances and a story textured enough to sometimes feel a lot like life. If nothing else, A Lot Like Love is a pleasant reminder of a Hollywood time, seemingly long gone, when boy met girl in a midlevel romantic comedy without arty aspirations . . . or low-brow yucks." 

Roger Ebert of the Chicago Sun-Times observed, "The movie is 95 minutes long, and neither character says a single memorable thing. Youve heard of being too clever by half? Ollie and Emily are not clever enough by three-quarters . . . To call A Lot like Love dead in the water is an insult to water.".  Ebert awarded the film one out four stars, and ultimately included it in his list of "Most Hated Films".
 Kennedy offspring, makes Kutcher fade into the background, and youre left fantasizing what the movie might have been if Peter Sarsgaard had co-starred . . . Still, the movie is rich in the kind of details often left out because of a lack of budget or imagination." 
 Christopher Orr Second Law of Thermodynamics, which says that in a closed system (an apt description of Hollywood if ever there was one) there is a tendency toward entropy - in this case, from acknowledged classic to memorable cable-television staple to dim, flabby dud . . . If one thing saves A Lot Like Love from disaster - and Im not sure it does - its an easy chemistry between the leads, though one that owes little to Mr. Kutchers performance . . . Fortunately, Ms. Peet carries off the role of Emily with enough spark that even Mr. Kutcher gradually lights up . . . Since running away with The Whole Nine Yards five years ago, Ms. Peet has had altogether too little opportunity to showcase her sexy comic appeal. A Lot Like Love might have provided that opportunity, had it only featured something a little like a script." 

Peter Travers of Rolling Stone rated the film two out of four stars, calling it "a lot like a lot of other romantic comedies that make two lovers of friends (When Harry Met Sally, Serendipity (film)|Serendipity) and a lot not like the two witty and wise Richard Linklater movies - Before Sunrise and Before Sunset - that span the relationship between Ethan Hawke and Julie Delpy for nearly a decade and leave you wanting more . . . About A Lot Like Love leaving you wanting a lot less, I am right." 

==Box office== The Amityville Horror, and Sahara (2005 film)|Sahara. It eventually grossed $21,845,719 in the US and $21,041,000 in foreign markets for a total worldwide box office of $42,886,719. 

==DVD release== fullscreen editions The Proposal.

==See also==
*Same Time, Next Year (film)

==References==
 

==External links==
* 
* 
 

 

 
 
 
 
 
 
 
 
 
 