Lad, A Dog (film)
 
 
{{Infobox film
| name           = Lad: A Dog 
| image          = Lad A Dog Poster.jpg
| image_size     =
| alt            = An artists drawing of tricolored collie is jumping up while looking backwards with an open book in the noting the films being based on the novel by Albert Payson Terhune. along the bottom edge, a man is seen holding a crying woman, the collie howling along with the woman playing guitar, a man carrying a woman, and two men facing each other.
| caption        = Original theatrical poster
| director       = Leslie H. Martinson Aram Avakian
| producer       = Max Rosenberg
| writer         = Screenplay: Lillie Hayward Roberta Hodes Original novel Albert Payson Terhune
| starring       = Peter Breck Peggy McCay Carroll OConnor Angela Cartwright
| music          = Heinz Roemheld
| cinematography = Bert Glennon
| editing        = Tom McAdoo
| studio         = Vanguard Productions Warner Brothers
| released       =  
| runtime        = 98 minutes United States English
| budget         = 
| gross          = 
}}
Lad: A Dog is a 1962 American   written by Albert Payson Terhune. Starring Peter Breck, Peggy McCay, Carroll OConnor, and Angela Cartwright, the film blends several of the short stories featured in the novel, with the heroic Lad winning a rigged dog show, saving a handicapped girl from a snake, and capturing a poacher who killed his pups and injured one of his owners. Warner Brothers purchased the film rights for the novel from Vanguard Productions, and acquired the film rights for the other two Lad novels from the late Terhunes wife.

Aram Avakian was initially selected to be the films director, but when he continually refused to do a sentimental-type dog story, he was replaced by Leslie H. Martinson. Lillie Hayward and Roberta Hodes wrote the screenplay for the film, adapting several of the short stories from the novel to create a single narrative, and adding in an all-purpose villain. The film was released on June 6, 1962. The studio hoped it would be successful enough to be followed by a second film and a television series. Though praised by fans and modern reviewers, contemporary critiques felt Terhunes work did not translate well to film and it was considered a low budget, B-movie. It was released to home video in 1995.

==Plot==  dog show. However, Glure is jealous of Lads success and has rigged one event to have such specialized rules that he believes only his recently purchased high-priced, English-trained collie can win. During the competition, which involves directing the dogs through a tricky set of a maneuvers, Lad is able to complete the course, while Glures champion does not recognize the hand signals Glure makes while holding a cigar.

Later, Lad saves Glures daughter Angela from a poisonous snake by knocking her backwards to get her out of harms way, then fighting and killing the snake, getting bitten in the process. Her nurse (Alice Pearce) initially does not see the snake, and begins beating Lad for "attacking" the little girl. Distraught, Angela stands and walks for the first time since her illness to stop the nurses abuse of her friend. Lad disappears for three days, reappearing covered in mud but cured of the poison.

After his return, Lad is bred with another prize-winning collie, Lady, and they have two male puppies which are named Little Lad & Wolf. Angela is allowed her choice of one as a present to her when they are old enough to leave their mother & she chooses Little Lad. However, Jackson White (Jack Daly), a poacher Lad fought and chased off the property before, sets fire to barn out of vengeance. Elizabeth is injured and Little Lad is killed but not Wolf. Lad later aids in capturing White when he breaks into the house to try to steal Lads gold trophy from the dog show. Angela is initially inconsolable over the loss of her puppy Little Lad, and refuses to have anything to do with Wolf. After he is nearly lost in another accident, she changes her mind and accepts Wolf as her new dog.

==Production==
Max J. Rosenberg, of   from publisher E.P. Dutton, then later sold the rights to Warner Brothers. When the studio learned of the other two Lad novels, Further Adventures of Lad and Lad of Sunnybank, they were concerned to learn that Dutton had the rights to only the first novel, as they were hoping the film would be successful enough to develop a sequel and possibly a television series. Executive Bruce Chapman negotiated for the film rights for the other two novels with Anice Terhune, the late wife of the author. She set up a foundation, Terhune Lad Stories, Inc, to negotiate the rights for other two novels. All told, Warner paid $25,000 for the rights to all three books, though the bulk of the funds went to Dutton.   

Warner Brothers initially hired Aram Avakian, a "talented, aggressive young ex-film editor" known for his avant-garde tendencies, to direct the film.  The studio wanted a sentimental dog story that played true to the novel, which Avakian opposed.  In a 1969 Life (magazine)|Life interview, Avakian stated that he "wanted to make a kind of pop, camp thing that wouldnt be a complete ordeal for parents" while everyone else involved in the production wanted "Dick and Jane|Dick, Jane and Doggie".    Jack Warner, then head of the studio, eventually fired him, bringing the more conventional Leslie H. Martinson to complete the film.    Peter Breck and Peggy McCay were cast as Lads owners, renamed to Stephen and Elizabeth Tremayne. Veteran actor Carroll OConnor was hired to play the pompous and newly wealthy Hamilcar Q. Glure, with the role of his daughter played by Angela Cartwright, a noted young actress who had starred in The Danny Thomas Show. To cast the role of Lad, a talent search was conducted, eventually resulting in the selection of a collie from the San Fernando Valley who was credited simply as "Lad". The film is set in a modified version of the Place, with Pompton Lake reduced to a river with a large dock. The house, though similar to the real Terhune home, even including duplicates of the stone lions on the veranda, was built on a scale three times larger than the original. 

Well-known screenwriter Lillie Hayward, and newcomer Roberta Hodes, were hired to adapt the novel for film. They combined several of the stories from the novel, modifying characters to create a single flowing narrative. For example, the crippled girl who was a neighbor girl in the original novel became Glures daughter. Noting Terhunes frequent disdain for the Ramapough Mountain Indians, they named the films villain Jackson White, a play on the nickname used to refer to those people. White became the catch all villain of the film, who poaches deer, sets fire to the Tremayne barn, and break into the house to try to steal a gold trophy won by Lad. 

Lad: A Dog was released to theaters on June 6, 1962.    The 98 minute film was released to VHS format on January 31, 1995. 

==Reception==
The film was considered to be a "B-movie" with its low production budget. Terhune biographer Irving Litvag praised OConnors performance as Glure, feeling his talent made him "seem human" and "a person of dignity and love" versus Terhunes "overdrawn and exaggerated" silly character. He felt the setting and cinematography was "typical Hollywood overstatement", and that as a whole found that Terhunes "sentimental writing" did not do well in film form, calling the resulting film "cloying" and a "minor, unsung film." However, he noted that fans of the novels seemed to thoroughly enjoy the film, suspecting part of it was the "handsome collie" found to play the titular role, and seeing the names of the beloved collies they knew on the screen.  A New York Times reviewer called it a "bucolic drama of no discernible merit".   The Monthly Film Bulletin praised the dog actors, but felt the film was "stultifyingly mawkish, with a touch of supposed humor contributed by an allegedly English chauffeur".  Bob Ross, of the Tampa Tribune, considered it to be an "earnest, well-acted story".  Leonard Maltin felt it was a "genuine if schmaltzy" adaptation of the novel. 

==References==
 

==External links==
*  
*  
*  

 
 
 

 
 
 
 
 
 