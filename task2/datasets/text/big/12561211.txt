Fate Is the Hunter (film)
{{Infobox film
| name           = Fate Is the Hunter
| image_size     =
| image	         = Fate Is the Hunter FilmPoster.jpeg
| caption        = Theatrical release poster
| director       = Ralph Nelson
| producer       = Aaron Rosenberg
| writer         = Ernest K. Gann (book) Harold Medford
| narrator       =
| starring       = Glenn Ford Nancy Kwan Rod Taylor
| music          = Jerry Goldsmith
| cinematography = Milton R. Krasner Robert L. Simpson
| distributor    =
| released       =  
| runtime        = 106 minutes
| country        = United States
| language       = English
| budget         = $2,525,000 
| gross          = $2.2million 
}}
Fate Is the Hunter is a 1964 film about the crash of an airliner and the subsequent investigation: it was directed by Ralph Nelson and released by 20th Century Fox. It was nominally based on the bestselling 1961 memoir Fate Is the Hunter by Ernest K. Gann, but the author was so disappointed with the result that he asked to have his name removed from the credits.  In his autobiography, A Hostage to Fortune, Gann wrote, "They obliged and, as a result, I deprived myself of the TV Residual (entertainment industry)|residuals, a medium in which the film played interminably."
 flashback sequence), Mark Stevens; it also includes an uncredited appearance by Dorothy Malone.  It also features an early film score by prolific composer Jerry Goldsmith. 
 

==Plot==
Pilot Jack Savage (Taylor) is suspected of drinking and causing an airliner crash that kills 53 people and leaves only a single survivor, Martha Webster (Pleshette), a flight attendant. The captains wartime buddy, airline executive Sam C. McBane (Ford), is convinced of his friends innocence and investigates doggedly. Flashbacks deal with both Jacks past and Sam meeting him, plus others they used to know.

Eventually, a test flight recreates the events of the ill-fated flight. They find that the crash was caused by a fateful malfunction of the planes warning systems.

==Cast==
* Glenn Ford as Sam McBane
* Nancy Kwan as Sally Fraser
* Rod Taylor as Jack Savage
* Suzanne Pleshette as Martha Webster
* Jane Russell as Guest Star  
* Constance Towers as Peg Burke
* Wally Cox as Ralph Bundy
* Nehemiah Persoff as Ben Sawyer Mark Stevens as Mickey Doolan
* Max Showalter as Dan Crawford
* Dorothy Malone as Lisa Bond (uncredited)
* Howard St. John as Mark Hutchins
* Robert J. Wilke as Stillman
* Bert Freed as Charles J. Dillon
* Dort Clark as Ted Wilson
* Mary Wickes as Mrs. Llewlyn
* Robert F. Simon as Proctor

  used in the film was an amalgam of two airframes.]]

==Production==
The "Consolidated Airways" jet aircraft used in the film was one of two fabricated from DC-7(B) donors, the second was used to create the crash scene (on the beach). The wings were reportedly removed and reversed, a Boeing 707 nose cone along with "supersonic spike" were also added in order to achieve the appearance of a modern jet airliner. Modifications to the rear section of the aircraft included the addition of two nacelles to accommodate the simulated jet engines. A rear-mounted Boeing 707 spike-styled HF antenna isolator, and antenna were also added to the tail section. An area of the Twentieth Century Fox back lot was converted into the tarmac, taxiway, and runway seen in the film.  Because of the fear of litigation, it was reported that no airframe manufacturer or airline was willing to cooperate in the production of the film, making these steps necessary. The "Fate" aircraft was later used in the filming of an episode of the ABC television series "Voyage to the Bottom of the Sea" (1964-1968), and remained parked for several years on an overpass used for movie prop storage by the adjacent 20th Century Fox Studios.

==Reception==
Releasing a film about aircraft accidents, especially done in the melodramatic manner that the film employed, led to a curious reception from both critics and public. Bosley Crowther of The New York Times, simply called it, "a stupid, annoying film." 

===Awards and honors===
Fate Is the Hunter was nominated for a 1964 Academy Award in Best Cinematography (Black-and-white). 

===Cultural references===
An excerpt from the film was used in the 1980 comedy film, Airplane!. The film is also mentioned several times in the 1995 JAG (TV series)|JAG episode "Pilot Error"; it provides the protagonist with a clue in solving a fighter jet crash.

==References==
===Notes===
 

===Bibliography===
 
* Solomon, Aubrey. Twentieth Century Fox: A Corporate and Financial History (The Scarecrow Filmmakers Series). Lanham, Maryland: Scarecrow Press, 1989. ISBN 978-0-8108-4244-1.
* Vagg, Stephen. Rod Taylor: An Aussie in Hollywood. Duncan, Oklahoma: BearManor Media, 2010. ISBN 1-59393-511-0.
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 