Star Hunter
{{Infobox film
| name           = Star Hunter
| image          = Cover of Star Hunter a 1998 movie.jpg
| border         = yes
| caption        = 
| alt            = Star Hunter cover - A robotic like guy who looks a bit like predator.
| director       = Cole S. McKay Fred Olen Ray
| producer       = Sam Newfield Raymond Reynolds
| screenplay     = M.B.Dick Mark Litton R.J Robertson
| starring       = Roddy McDowell Stella Stevens
| cinematography = Gary Graver Mark Melville
| editing        = Robert L. Goodman Vaniak Monadian
| Production Company = Concorde-New Horizons
| released       =  
| runtime        = 89 minutes
| country        = United States
| language       = English
}}
 science fiction film starring Roddy McDowell, and Stella Stevens. The eponymous character is an alien who travels through space seeking species to hunt for pleasure. He arrives on Earth, landing in Los Angeles, and is soon in pursuit of a teacher and her students, whose bus broke down on the way home from a football game, which they lost.

This move has a one star rating on IMDB and no score on Rotten Tomatoes. The movie is allegedly a thriller but contains no actual thrills.

 


 