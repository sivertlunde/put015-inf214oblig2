List of Malayalam films of 1998
 

The following is a list of Malayalam films released in the year 1998.
{| class="wikitable"
|- style="background:#000;"
! colspan=2 | Opening !! Sl. No. !!Film !! Cast !! Director !! Music Director !! Notes
|-
| rowspan="3" style="text-align:center; background:#ffa07a; textcolor:#000;"| J A N 
| rowspan="1"  style="text-align:center; background:#ffdacc; textcolor:#000;"| 5
|valign="center" |  1 
| Sreekrishnapurathe Nakshathrathilakkam || Sudheesh, Nagma || Rajasenan || Berny Ignatius ||
|-
| rowspan="1"  style="text-align:center; background:#ffdacc; textcolor:#000;"| 8
|valign="center" |  2  Innocent || Ali Akbar || Berny-Ignatius ||
|-
| rowspan="1"  style="text-align:center; background:#ffdacc; textcolor:#000;"| 26
|valign="center" |  3 
| Kanmadam || Mohanlal, Manju Warrier || A. K. Lohithadas || Raveendran ||
|-
| rowspan="2" style="text-align:center; background:#dcc7df; textcolor:#000;"| F E B 
| rowspan="1"  style="text-align:center; background:#ede3ef; textcolor:#000;"| 6
|valign="center" |  4 
| Nakshathra Tharattu || Kunchako Boban, Shalini || K. Shankar || Mohan Sithara ||
|-
| rowspan="1"  style="text-align:center; background:#ede3ef; textcolor:#000;"| 23
|valign="center" |  5  Vidyasagar ||
|-
| rowspan="4" style="text-align:center; background:#d0f0c0; textcolor:#000;"| M A R 
| rowspan="1"  style="text-align:center; background:#edefff; textcolor:#000;"| 8
|valign="center" |  6  Rajeev || Vidyasagar ||
|-
| rowspan="1"  style="text-align:center; background:#edefff; textcolor:#000;"| 13
|valign="center" |  7  Innocent || Johnson ||
|-
| rowspan="1"  style="text-align:center; background:#edefff; textcolor:#000;"| 19
|valign="center" |  8  Rambha || Jomon || Vidyasagar ||
|-
| rowspan="1"  style="text-align:center; background:#edefff; textcolor:#000;"| 26
|valign="center" |  9  Mukesh || Sandhya Mohan || M. S. Viswanathan ||
|-
| rowspan="1" style="text-align:center; background:#ffa07a; textcolor:#000;"| A P R 
| rowspan="1"  style="text-align:center; background:#ffdacc; textcolor:#000;"| 8
|valign="center" |  10  Dileep || Johnson ||
|-
| rowspan="3" style="text-align:center; background:#dcc7df; textcolor:#000;"| M A Y 
| rowspan="1"  style="text-align:center; background:#ede3ef; textcolor:#000;"| 6
|valign="center" |  11  Reena || PG Vishwambharan ||  ||
|-
| rowspan="2"  style="text-align:center; background:#ede3ef; textcolor:#000;"| 21
|valign="center" |  12  Kamal || Kaithapram ||
|-
|valign="center" |  13  Vidyasagar ||
|-
| rowspan="4" style="text-align:center; background:#d0f0c0; textcolor:#000;"| J U N 
| rowspan="1"  style="text-align:center; background:#edefff; textcolor:#000;"| 3
|valign="center" |  14 
| Anuragakottaram || Dileep (actor)|Dileep, Suvalakshmi || Vinayan || Ilayaraja ||
|-
| rowspan="1"  style="text-align:center; background:#edefff; textcolor:#000;"| 9
|valign="center" |  15 
| Kallu Kondoru Pennu || Vijayshanti, Suresh Gopi || Shyamaprasad || Ilaiyaraaja ||
|-
| rowspan="1"  style="text-align:center; background:#edefff; textcolor:#000;"| 13
|valign="center" |  16 
| Manjeeradhwani || Sakshi Sivanand, Vineeth || Bharathan || Ilayaraja ||
|-
| rowspan="1"  style="text-align:center; background:#edefff; textcolor:#000;"| 19
|valign="center" |  17  Vidyasagar ||
|-
| rowspan="1" style="text-align:center; background:#ffa07a; textcolor:#000;"| J U L 
| rowspan="1"  style="text-align:center; background:#ffdacc; textcolor:#000;"| 18
|valign="center" |  18 
| Oro Viliyum Kathorthu || Mukesh (actor)|Mukesh, Sukumari || VM Vinu || Berny-Ignatius ||
|-
| rowspan="4" style="text-align:center; background:#dcc7df; textcolor:#000;"| S E P 
| rowspan="1"  style="text-align:center; background:#ede3ef; textcolor:#000;"| 4
|valign="center" |  19  Mohini || Rafi Mecartin || Suresh Peters ||
|-
| rowspan="1"  style="text-align:center; background:#ede3ef; textcolor:#000;"| 16
|valign="center" |  20  Fazil || Ouseppachan ||
|-
| rowspan="1"  style="text-align:center; background:#ede3ef; textcolor:#000;"| 19
|valign="center" |  21  Mangalya Pallakku || Sreenivasan (actor)|Sreenivasan, Jagadish || Vinod Roshan || BalabhaskarRajamani ||
|-
| rowspan="1"  style="text-align:center; background:#ede3ef; textcolor:#000;"| 30
|valign="center" |  22  Sreenivasan || Kamal || Raveendran ||
|-
| rowspan="4" style="text-align:center; background:#d0f0c0; textcolor:#000;"| O C T 
| rowspan="1"  style="text-align:center; background:#edefff; textcolor:#000;"| 1
|valign="center" |  23 
| British Market || Vijayaraghavan (actor)|Vijayaraghavan, Jagathy Sreekumar || Nizar || Rajamani ||
|-
| rowspan="2"  style="text-align:center; background:#edefff; textcolor:#000;"| 15
|valign="center" |  24  Sreenivasan || Sreenivasan || Johnson ||
|-
|valign="center" |  25  Dileep || Kalabhavan Ansar || Berny-Ignatius ||
|-
| rowspan="1"  style="text-align:center; background:#edefff; textcolor:#000;"| 23
|valign="center" |  26 
| Rakthasakshikal Zindabad || Mohanlal, Suresh Gopi || Venu Nagavalli || MG Radhakrishnan ||
|-
| rowspan="6" style="text-align:center; background:#ffa07a; textcolor:#000;"| N O V 
| rowspan="1"  style="text-align:center; background:#ffdacc; textcolor:#000;"| 3
|valign="center" |  27 
| Meenathil Thalikettu || Dileep (actor)|Dileep, Sulekha || Rajan Sankaradi || Ouseppachen ||
|-
| rowspan="2"  style="text-align:center; background:#ffdacc; textcolor:#000;"| 6
|valign="center" |  28 
| Mayajalam || Mukesh (actor)|Mukesh, Vineetha || Balu Kiriyath || S. P. Venkatesh ||
|-
|valign="center" |  29 
| Mayilpeelikkavu || Kunchako Boban, Jomol || Anil || Berny Ignatius ||
|-
| rowspan="1"  style="text-align:center; background:#ffdacc; textcolor:#000;"| 29
|valign="center" |  30 
| Sneham (1998 film)|Sneham || Jayaram, Jomol || Jayaraj || Perumbavoor G Raveendranath ||
|-
| rowspan="2"  style="text-align:center; background:#ffdacc; textcolor:#000;"| 30
|valign="center" |  31  Shruti || Rajasenan || Berny-Ignatius ||
|-
|valign="center" |  32 
| Sundarakilladi || Dileep (actor)|Dileep, Shalini || Muralikrishnan ||  ||
|-
| rowspan="2" style="text-align:center; background:#dcc7df; textcolor:#000;"| D E C 
| rowspan="2"  style="text-align:center; background:#ede3ef; textcolor:#000;"| 25
|valign="center" |  33  Mohini || Jose Thomas || Nadirsha ||
|-
|valign="center" |  34 
| Dravidan (1998 film)|Dravidan || Jagathy Sreekumar, Rajan P Dev || Mohan Kupleri || S. P. Venkatesh ||
|-yet to sort Ayushman Bhava|| || || ||
|-
| || || ||Thalolam|| || || ||
|- Manthri Kochamma|| || || ||
|- Magician Mahendralal from Delhi|| || || ||
|- Manjukalavum Kazhinju|| || || ||
|-
| || || ||Manthrikumaran|| || || ||
|- Ilamura Thamburan|| || || ||
|- Chenapparambile Aanakkariyam|| || || ||
|- Ennu Swantham Janakikutty|| || || ||
|-
| || || ||Samaantharangal|| || || ||
|-
| || || ||Ormacheppu|| || || ||
|-
| || || ||Kalapam|| || || ||
|- Graama Panchaayathu|| || || ||
|-
| || || ||Thirakalkkappuram|| || || ||
|- Daya (film)|Daya|| || || ||
|- Kattathoru Penpoovu|| || || ||
|-
| || || ||Thattakam|| || || ||
|- Mattupetti Machan|| || || ||
|- Achaammakkuttiyude Achaayan|| || || ||
|-
| || || ||Aaghosham|| || || ||
|-
| || || ||Kulirkaattu|| || || ||
|-
| || || ||Chitrashalabham|| || || ||
|-
| || || ||Meenthoni|| || || ||
|- Aalibabayum Aarara Kallanmarum|| || || ||
|- Poothiruvathira Ravil|| || || ||
|-
| || || ||Sooryavanam|| || || ||
|- The Truth The Truth|| || || ||
|-
| || || ||Sooryaputhran|| || || ||
|- Malabaril Ninnoru Manimaaran|| || || ||
|- Aaraam Jaalakam|| || || ||
|-
| || || ||Harthaal|| || || ||
|-
|}

==Dubbed films==

{| class="wikitable sortable"
!  width="25%" | Title
!  width="18%" | Director
!  width="18%" | Music
!  width="18%" | Cast
|- valign="top"
| Dravidam Bhanu Chander Bhanu Chander
|
|}

 
 
 

 
 
 
 
 