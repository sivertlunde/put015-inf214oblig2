Chaahat (1996 film)
{{Infobox film 
| name = Chaahat चाहत  
| image = CHAAHAT.jpg 
| director       = Mahesh Bhatt
| producer       = Robin Bhatt Viral Lakhia
| writer         = Robin Bhatt Akash Khurana Javed Siddiqui
| narrator       = 
| starring       = Shah Rukh Khan Pooja Bhatt Naseeruddin Shah Anupam Kher Ramya Krishna
| music          = Anu Malik
| cinematography = Ashok Behl
| editing        = Bharat Singh
| distributor    = Bhatt Productions
| released       = 21 June 1996
| runtime        = 146 mins
| country        = India
| language       = Hindi
  gross          = INR 87,500,000
}}
 romantic musical musical film by director Mahesh Bhatt. The cast of this film is Shahrukh Khan, Pooja Bhatt, Naseeruddin Shah, Ramya Krishna and Anupam Kher. Shahrukh Khan acquired right of this film from Mahesh Bhatt in October 2013.  

==Synopsis==

Roop Rathore (Shahrukh Khan) is a singer in Bombay just like his father (Anupam Kher), who is now sick and needs immediate medical care. One day, while he is singing at Ajay Narangs (Naseeruddin Shah) hotel, Ajays sister Reshma (Ramya Krishnan) falls in love with Roop. Reshma is a spoiled girl and Ajay looks through all of her wishes. Unfortunately, Roop is in love with a doctor named Pooja (Pooja Bhatt). Meanwhile, Reshma is obsessed with Roop, and asks her brother to call him again to sing in their hotel. But when she sees that all the girls are flattered over Roop, she gets very angry, and asks him to sing only for her now onwards. But Roop would rather work for rival Patel (Shri Vallabh Vyas) than take this offer. Ajays obsession with keeping his sister happy at all costs comes into play and Patel is brutally beaten by him until he agrees to throw Roop out of his hotel. Desperately in need of money for his fathers operation, Roop has no option but to agree to Reshma. Roop breaks up with Pooja.

The operation of his father is successful, but his father is saddened by his situation and decides to leave Bombay. Roop later tries to leave Bombay himself, along with his father and Pooja, but his plans are interrupted when Reshma tries to commit suicide. Nevertheless, Pooja and Roop get married. Frustrated with such turn of events, Reshma and her brother Ajay devise many plans to make their life miserable. At the end of the movie, Roops father is killed by Ajay, and when Roop tries to save Pooja, he and Ajay get involved in a fight. Reshma threatens to kill Pooja if Roop wont stop fighting with her brother.
==Cast==
*Shahrukh Khan... Roop Rathore
*Pooja Bhatt... Pooja
*Naseeruddin Shah... Ajay Narang 
*Anupam Kher... Shambunath Rathore
*Ramya Krishna... Reshma Narang
*Avtar Gill... Poojas uncle {Friendly Appearance}

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)!! Length
|-
| 1
| "Dil Ki Tanhai Ko"
| Kumar Sanu
| 07:28
|- 
| 2
| "Chaahat Na Hoti "
| Vinod Rathod, Alka Yagnik
| 08:05
|- 
| 3
| "Daddy Cool"
| Sudesh Bhosle, Devang Patel
| 08:23
|- 
| 4
| "Kabhi Dil Se Kam Mohabbat"
| Kumar Sanu, Sadhana Sargam
| 05:23
|- 
| 5
| "Nahi Lagta"
| Udit Narayan, Alka Yagnik
| 06:57
|- 
| 6
| "Tumne Dikhaye Aise Sapne"
| Vinod Rathod
| 06:40
|- 
| 7
| "Bole Mora Kangna"
| Kumar Sanu, Alka Yagnik
| 05:49
|- 
| 8
| "Doob Ke Dariya Mein" Poornima
| 04:03
|- 
| 9
| "Nahin Jeena Yaar Bina"
| Udit Narayan, Kavita Krishnamurthy
| 05:55
|}

==References==
 

==External links==
* 
 

 
 
 