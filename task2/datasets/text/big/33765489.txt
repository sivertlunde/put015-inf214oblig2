Imperiale
 
{{Infobox film
| name           = Imperiale
| image          = 
| caption        = 
| director       = George Skalenakis
| producer       = Thodoros Roubanis
| writer         = Giannis Tziotis
| starring       = Kostas Karras
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = Greece
| language       = Greek
| budget         = 
}}
 Best Foreign Language Film at the 41st Academy Awards, but was not accepted as a nominee. 

==Cast==
* Kostas Karras as Emperor
* Betty Arvaniti as Zoi
* Thodoros Roubanis as (as Theodoros Roubanis)
* Yanis Alexandridis
* Giorgos Oikonomou
* Venia Palliri
* Christos Parlas
* Yannis Totsikas
* Nikos Tsachiridis
* Giorgos Zaifidis
* Hristoforos Zikas
* Christos Zorbas

==See also==
* List of submissions to the 41st Academy Awards for Best Foreign Language Film
* List of Greek submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 