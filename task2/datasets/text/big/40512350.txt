From Bedrooms to Billions
{{Infobox film
| name           = From Bedrooms to Billions
| image          = 
| alt            = 
| caption        = 
| director       =  
| producer       = 
| writer         =  Matthew Smith David Darling David Perry Chris Anderson  Mel Croucher  Jon Hare 
| music          = Rob Hubbard Ben Daglish   
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 3 October 2014
| runtime        = 124 minutes
| country        = United Kingdom
| language       = English
}}
From Bedrooms to Billions is a 2014 documentary film by British filmmakers Anthony Caulfield and Nicola Caulfield that tells the story of the British video games industry from 1979 to the present day. The film focuses on how the creativity and vision of a relatively small number of individuals allowed the UK to play a key, pioneering role in the shaping of the billion dollar video games industry which today dominates the modern worlds entertainment landscape.  The film features interviews with major British game designers, journalists and musicians from across the last 30 years.
 crowd funding.

The Caulfields raised approximately £80,000 through two rounds of crowd funding with Indiegogo  and Kickstarter  generating donations of $37,000 and £60,000 respectively. Further funds have been raised through direct donations, pre-orders for DVDs and soundtrack CDs via the From Bedrooms To Billions website which has allowed them to produce the film independently. 

From Bedrooms to Billions was originally scheduled for release on May 30, 2014. The digital version was eventually launched on October 3, 2014. 

==References==
{{reflist|refs=
 
{{cite web
  |title=From Bedrooms To Billions on Kickstarter
  |publisher=Kickstarter
  |url=http://www.kickstarter.com/projects/1195082866/from-bedrooms-to-billions
}}
 

 
{{cite web
  |title= From Bedrooms To Billions on Indiegogo
  |publisher=Indiegogo
  |url=http://www.indiegogo.com/projects/from-bedrooms-to-billions
}}
 

 
{{cite news
  |url=http://metro.co.uk/2013/03/07/from-bedrooms-to-billions-uk-gaming-the-movie-3530510/
  |title=From Bedrooms to Billions – UK Gaming: The Movie Metro
  |date=7 March 2013
  |first=David|last=Jenkins
}}
 
}}

==External links==
* 
* 
*  . Eurogamer.net (22 March 2013)
*  . Edge Online
*  . MCV UK
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 

 