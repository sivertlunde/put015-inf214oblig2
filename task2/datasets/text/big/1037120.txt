The Hand That Rocks the Cradle (film)
{{Infobox film
| name = The Hand That Rocks the Cradle
| image = Handrockscradle.jpg
| caption = Theatrical release poster
| director = Curtis Hanson
| producer = David Madden, Ferdinand Abardo Amanda Silver Matt McCoy Julianne Moore John de Lancie Kevin Skousen Madeline Zima
| music = Graeme Revell
| cinematography = Robert Elswit
| editing = John F. Link
| studio = Hollywood Pictures Interscope Communications Nomura Babcock & Brown Buena Vista Pictures
| released = January 10, 1992
| runtime = 110 minutes
| country = United States
| language = English
| budget = $11,700,000 (estimated)  . IMDb. Retrieved 10 October 2013
 
| gross = $88,036,683
}}

The Hand That Rocks the Cradle is a 1992 American psychological thriller. The film was directed by Curtis Hanson, and stars Annabella Sciorra and Rebecca De Mornay. The tale follows a vengeful, psychopathic  nanny out to destroy a naive woman and steal her family. The original music score was composed by Graeme Revell.

==Plot== Matt McCoy), who reassures her and encourages her to report him to the police. More women come forward with similar complaints, and multiple charges are prepared against Dr. Mott, who decides to escape the forthcoming arrest and trial by shooting himself.

Later, Dr. Motts pregnant widow (Rebecca De Mornay) meets with her lawyers and is told that her husbands assets have been frozen because of the lawsuits and that she will lose her luxurious home. Mrs. Mott goes into early labor, loses her baby and, after hemorrhaging, is given an emergency hysterectomy. While recovering in the hospital, she sees a news story featuring Claire as the woman who alerted the police, leading Mrs. Mott to swear vengeance.
 alias "Peyton Flanders", responds to the ad and tricks the Bartels into hiring her. A close friend, Marlene (Julianne Moore), warns Claire, quoting the poem by William Ross Wallace.

Mrs. Mott wages a campaign to undermine Claire. She begins breastfeeding Joey in secret (which causes him to no longer take Claires milk) and encourages her daughter Emma (Madeline Zima) to keep secrets from her mother and tries to turn her against Claire. She also secretly destroys Michaels office proposal, tempts Michael sexually, and does things that lead Claire to believe that Michael is cheating on her with Marlene, fueled by the fact that Marlene had been an ex-girlfriend before he married Claire.

Mrs. Mott also suggests to Michael that he arrange a surprise party for Claire, leading Marlene and Michael to meet in secret. Claire accuses Michael of having an affair with Marlene, only to find the party-goers, including a very hurt Marlene and her husband, waiting in the next room.

Solomon (Ernie Hudson), an intellectually disabled handyman that has been assisting the Bartels, discovers Mrs. Mott breast-feeding Joey. Mrs. Mott then plants a pair of Emmas panties in Solomons toolbox, leading Claire to believe that Solomon molested her. Solomon is then fired, and returned to a rest home as a result. Emma tells Claire that Solomon never did anything bad to her, but Claire refuses to believe Emma, causing her to turn against her mother as Mrs. Mott had planned.

Claire begins to doubt "Peyton" and tells Michael so. Mrs. Mott overhears, then sets a trap for Claire in the greenhouse, rigging the glass skylights in her greenhouse in such a way that, when the door is opened, the skylights will come crashing down and the broken glass will kill Claire. 

Meanwhile, Marlene, a realtor by trade, comes across Dr. Motts house, which is still up for sale. She notices that there are wind chimes outside the nursery and remembers a similar gift from Mrs. Mott to Claire. Marlene then finds a newspaper clipping that reveals Mrs. Motts identity. Marlene plans to warn Claire, but she ends up confronting Mrs. Mott, who decides to use the trap on Marlene instead, and tricks her into going into the greenhouse. Marlene pushes open the greenhouse door and is killed by the falling glass. 

Mrs. Mott, who knows that Claire suffers from asthma, empties all of her inhalers. When Claire finds Marlenes body, she has a severe asthma attack and passes out, resulting in a brief hospitalization. Michael is distraught over Marlenes death and his wifes condition. Mrs. Mott attempts to seduce him. However, he rejects her advances and tells her that "theres only one woman for me." Unknown to the family, except for Emma, Solomon begins to keep a watchful eye over them.

Claire eventually uncovers the truth about Mrs. Mott and confronts her, punching Mrs. Mott in the face. Claire reveals the truth about "Peyton" to Michael as Mrs. Mott claims that she and Michael are having an affair, repeating his "one woman" comment. Michael declares that he was talking about his wife and kicks Mrs. Mott out. Claire then tells Michael to call the police as she realizes that Mrs. Mott rigged the green house to kill her and not Marlene, while Michael tells Claire to get Emma and Joey so that they can head to a hotel to be safe.

Mrs. Mott breaks into the house later and lures Michael down to the basement where she hits him with a shovel, knocking him down the stairs and breaking his legs. Michael orders Claire to hide the children and call the police. Mrs. Mott then hunts for Claire and the children. She attempts to take Emma and Joey, but after seeing Mrs. Mott assault her mother, Emma locks Mrs. Mott in the nursery. 

Using a fire poker, Mrs. Mott escapes, and hears Joey in the attic. She enters and sees Solomon aiding the childrens escape. Claire enters and Mrs. Mott attempts to kill her, but stops after Claire appears to be having another asthma attack, allowing Mrs. Mott to mock her. As Mrs. Mott tries to take Joey, Claire gets back up, having faked her asthma attack, and the two women fight, culminating in Claire pushing Mrs. Mott out of the window, where she falls to her death, hitting the fence that Solomon built.

Touched that Solomon risked his life to protect her family, Claire allows Solomon to handle Joey. Claire, Emma and Solomon then leave the attic as the police and paramedics arrive at the house.

==Cast==
* Annabella Sciorra as Claire Bartel
* Rebecca De Mornay as Mrs. Mott
* Ernie Hudson as Solomon Matt McCoy as Michael Bartel
* Julianne Moore as Marlene Craven
* John de Lancie as Dr. Victor Mott
* Kevin Skousen as Marty Craven
* Madeline Zima as Emma Bartel

==Filming locations==
Filming locations were Issaquah, Washington; Seattle, Washington (Motts residence at 2502 37th Ave W in Seattle); and Tacoma, Washington. 

==Reception== Medicine Man. By the end of its run, the film earned a domestic total of $88,036,683.   It was also placed at #24 in Bravo (US TV channel)|Bravos special 30 Even Scarier Movie Moments. Cybill Shepherd was reported by the Chicago Tribune to have turned down the role of the nanny because "the story preyed on the worst fears of women" and conflicted with her feminist beliefs.  The film currently holds a rating of 63% on Rotten Tomatoes based on 45 reviews.

==Home media==
The Hand That Rocks the Cradle was released on DVD on December 8, 1998. The film was presented in its original widescreen aspect ratio, approximately 1.85.1. The only special feature included on the DVD is the films original theatrical trailer. It was released on Blu-ray Disc|Blu-ray on September 4, 2012.

==Reproductions== Hindi film Telugu film Charmi and The Sitter had a similar plot.

== See also ==

* List of films featuring home invasions

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 