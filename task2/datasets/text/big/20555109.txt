Small Town Rivals
{{Infobox film
| name           = Small Town Rivals
| image          = Small Town Rivals film poster.jpg
| caption        = Theatrical poster
| film name      = {{Film name
 | hangul         =  
 | rr             = Yijang-gwa gunsu
 | mr             = Yichanggwa kunsu}}
| director       = Jang Jyu-sung
| producer       = Kim Mi-hee Cha Seung-jae Im Chung-ryeol Yun Sang-o
| writer         = Jang Jyu-sung
| starring       = Cha Seung-won Yoo Hae-jin Byun Hee-bong
| music          = Choi Seung-hyun
| cinematography = Kim Yun-soo
| editing        = Ko Im-pyo
| distributor    = CJ Entertainment
| released       =  
| runtime        = 113 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          =  
}} 2007 South Korean film.

== Plot ==
Cho Chun-sam and Noh Dae-gyu, now both in their thirties, are old friends who went to the same elementary school. In their school days, Chun-sam was always the ambitious class president, while Dae-gyu had to settle for a role as vice president. Twenty years later their roles are reversed: Chun-sam is now a humble farmer who has assumed the post of village chief in his hometown, while Dae-gyu is the newly elected county magistrate. At first, Chun-sam asks his old friend for favours regarding the development of his village, but these requests are turned down. Later, when Dae-gyu proposes building a nuclear waste disposal facility in the county, Chun-sam leads demonstrations against the plan, turning old friends into bitter rivals.

== Cast ==
* Cha Seung-won ... Cho Chun-sam
* Yoo Hae-jin ... Noh Dae-gyu
* Byun Hee-bong
* Choi Jung-won
* Bae Il-jib
* Nam Il-woo
* Jeon Won-joo
* Lee Jae-goo Kim Eung-soo ... Mr. Kim
* Lee Won-jong (cameo)
* Yum Jung-ah (cameo)

== Release ==
Small Town Rivals was released in   on its opening weekend with 440,516 admissions.  The film went on to receive a total of 1,269,142 admissions nationwide,  with a gross (as of May 27, 2007) of  . 

== Critical response ==
Yang Sung-jin of The Korea Herald was critical of the films blend of comedy and politics, saying, "Director Jang has incorporated a political satire into the film, weakening its already fragile comic underpinnings."  Kim Tae-jong of The Korea Times made similar comments, saying, "the funny moments often sidetrack from the storyline and do not successfully intermingle with the heavy sarcasm placed on political issues", but also noted, "The two actors deliver impeccable performances of the slapstick variety in the wacky situations they act in." 

== References ==
 

== External links ==
*  
*  
*  

 
 
 
 