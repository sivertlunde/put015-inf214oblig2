Kaze no Matasaburo
 
 
{{Infobox film
| name = Kaze no Matasaburō
| image = 
| caption = 
| director = Koji Shima
| producer = 
| writer = Nagami Ryūzō Ike Shintarō
| starring = Akihiko Katayama Akira Oizumi Akiko Kazami
| music = Sugihara Taizō
| cinematography = Aisaka Sōichi
| editing = Tsujii Masanori 
| choreography = 
| studio      =  Nikkatsu
| distributor = 
| released =  
| runtime =  96 minutes
| country = Japan  
| language = Japanese
| budget = 
}}
 fantasy family family drama film directed by Koji Shima,     based on Kenji Miyazawas 1934 short story of the same name.

== Plot ==
Saburō Takada transfers from the city to a very small school. The village children suspect that Saburō is actually Matasaburō, the wind sprite.   

== Cast ==
* Akihiko Katayama as Saburo Takada
* Akira Oizumi as Ichiro (Koichi)
* Akiko Kazami
* Ryūji Kita
* Hiroshi Hayashi as Grandfather of Ichiro
* Bontaro Miake

== See also ==
*Kenji Miyazawa
*   – short story by Kenji Miyazawa

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 


 