Charlie Chan's Chance
{{Infobox Film
| name           = Charlie Chans Chance
| image_size     = 
| image	=	Charlie Chans Chance FilmPoster.jpeg
| caption        = 
| director       = John G. Blystone
| producer       = Joseph H. August
| writer         = Earl Derr Biggers (novel and suggestions) Barry Conners Philip Klein
| narrator       = 
| starring       = Warner Oland Alexander Kirkland H. B. Warner
| music          = 
| cinematography = 
| editing        = 
| studio         = Fox Film Corporation
| distributor    = Fox Film Corporation
| released       = January 22 or 24, 1932
| runtime        = 73 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Charlie Chans Chance is a 1932 murder mystery film, the third to star Warner Oland as detective Charlie Chan. It is based on the novel Behind That Curtain by Earl Derr Biggers, who also contributed to the film. The film is considered to be Lost film|lost. 

==Plot==

Charlie Chan is in New York City attending a police convention when he is the intended murder victim here, and he avoids death only by chance. To find his would-be-killer (or would-be-killers), Charlie must outguess police reps from both Scotland Yard and New York City police.

==Cast==
*Warner Oland as Charlie Chan
*Alexander Kirkland as John R. Douglas
*H.B. Warner as Inspector Fife
*Marian Nixon as Shirley Marlowe
*Linda Watkins as Gloria Garland James Kirkwood as Inspector Flannery
*Ralph Morgan as Barry Kirk
*James Todd as Kenneth Dunwood
*Herbert Bunston as Garrick Enderly
*James Wang as Kee-Lin Joe Brown as Doctor
*Charles McNaughton as Paradise
*Edward Peil Sr. as Li Gung

 

Cast notes:
* Thomas A. Curran the early American silent film star plays an uncredited bit part.

==References==
 

==See also==
*List of lost films

==External links==
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 

 