Killjoy Goes to Hell
{{Multiple issues|
 
 
}}

{{Infobox film
| name        = Killjoy Goes to Hell
| image       = 
| director    = John Lechago
| producer    = Charles Band
| writer      = John Lechago Ian Roberts Jim Tavaré and Jessica Whitaker
| music       = Michael Sean Colin
| cinematography = ?
| editing     = John Lechago
| distributor = Full Moon Features
| released    = October 6, 2012
| budget      =
| runtime     = 92 min.
| country     = United States
| language    = English
}} Killroy was here with Killroy scratched out. This film is 256 in the Full Moon Catalog, the same number as Sandys patient number is 256, and Killjoys convict outfit

==Plot== Essex County Mental Asylum and is still questioned by Detective Grimley and Detective Ericson about the missing bodies of Zilla, the Professor, Rojer and Erica. She only responds with laughter, and Dr. Simmons informs the detectives that her brain is always in the stage of laughter, and cannot figure out what is causing her to do this, but from the evidence in the Professors house, and his relationship with Michael from the first film, Detective Ericson believes that the legend of Killjoy might be real after all. Meanwhile, the Bailiff takes Killjoy into an elevator down to Hell, where the courtroom is under Beelzebubs control. Killjoy is on trial for the crime of being too soft and not scary, as he let his last victim, Sandie, escaped from his realm. The accuser is Jezabeth, the Devils Advocate, who was once in a relationship with Killjoy a while ago before dumping her.
 Hell Jail, demonic clown who idolizes Killjoy, but has a secret agenda to take his position. He offers to become Killjoys attorney, and with what little left of human blood he has left, conjures up Punchy, Freakshow who is missing his little brother, and Batty Boop, who still has a grudge at Killjoy for vaporizing her. She recognizes Skid Mark, but cant remember him. In the first court hearing, the trio are brought to the stands and questioned by Jezabeth, but everything goes to hell as Punchy only speaks polari, Freakshows a mime, and Batty Boop gets Killjoy to apologize to her in front of everyone, not to mention Skid Marks failure as an attorney. Because of this, less that half of Killjoys fifty three names are crossed out by Scribe, the courts stenographer, which makes Killjoy weaker and erases them from existence. Back on earth, Detective Ericson and Dr. Simmons begin to notice that the Professors evidence on Killjoy is disappearing, and they cant remember them either. They decide to meet up with Detective Grimley and Sandie back at the Asylum to discuss whats going on.

Meanwhile in hell, Punchy begins to organize a revolt against the court with the rest of the demonic clowns to help Killjoy, and Freakshow goes to the Old Hags to find materials for a new bionic brother when he notices she has a magic mirror, but is only granted access to it unless he sleeps with her. He brings the materials back to Batty Boop, who conjoins him a new brother and tells her about the mirror, and Batty Boop decides to help Killjoy by entering the mirror to Earth and bringing Sandie to hell prove Killjoy guilty. Batty tracks down Sandies location and she and Freakshow enter the mirror and end up in the asylum, where Freakshow kills the Security Guard, Jim, and Batty kills Det. Grimley. Sandie tries to escape but Batty catches her and pushes her into the mirror. Dr. Simmons finds Jim dead along with Sandies straight jacket and thinks she did it. Freakshow is about to kill Det. Ericson, but is summoned back to Hell, leaving Ericson behind scared and wounded. Back in Hell, Beelzebub takes Killjoy to the elevator and takes him to Oblivion, the Final Circle of Hell, an area of nothing, to show him where hell be for the rest of eternity if he loses. He also leaves out a box on the desk in the courtroom for Killjoy, which he claims is insurance.

The next court hearing, Killjoy fires Skid Mark out of anger and decides to represent himself. Batty brings Sandie to the stands, in clown mode, and Sandie recaps the events to Jezabeth. Killjoy asks Sandie how she felt afterwards, and goes into vivid detail on what Killjoy did, convincing Beelzebub that he is in fact evil. The only thing left to convince him is the Trial of Combat, where Killjoy has to fight to the death with an opponent, which is Skid Mark. Batty Boop recognizes Skid Mark as one of her victims, who roofied her and raped her, leaving behind only one love bite on him: an infection. Skid Mark turns into a monster, and claims whoever wins gets to keep Batty. An unnamed Clown Observer hands Killjoy a bag of tricks, compliments of Punchy and the clowns, which is ineffective. Batty tells Sandie if Killjoy loses, she wont be able to get out of hell, so Sandie hands Killjoy his malice and crushes Skid Marks head off. The Clowns revolt in the courtroom, where Batty kills Jezabeth and Punchy kills Bailiff. Beelzebub, in a fit of anger, incinerates everyone but Punchy, Freakshow, Sandie, Batty and Killjoy before disappearing. Killjoy opens the box, and presses a button, which will self-destruct Hell in a minute. The group escapes in the elevator up to Earth, where the clown posse pursue Sandie in the streets.

==Cast==

===Evil clowns===
* Trent Haaga as Killjoy The clown
* Victoria De Mare as Batty Boop
* Al Burke as Punchy
* Tai Chan Ngo as Freakshow
* John Karyus as Skid Mark
* Daniel Del Pozza as Dirty Clown
* Derek Jacobsen as Dreadlock Clown
* Vincent Bilancio as Tramp Clown
* Nakai Nelson as Switchblade Sinister Clown/Voice of Destruction David Cohen as Clown Observer
* Leroy Patterson as Hillbilly Hobo/Monster Skid Mark
* Tim Chizmar as White Face
* Denzil Meyers as Harlequin

===Devils===
* Stephen F. Cardwell as Beelzebub
* Samantha Holman as Court Observer
* Mindy Robinson as Red Devil Girl
* Jenny Allford as Blue Devil Girl
* Aqueela Zoll as Jezabeth
* Jim Tavaré as Scribe
* Juan Patricia as Exploded Observer Ian Roberts as Bailiff
* Lisa Goodman as Old Hag

===People from Earth===
* Jessica Whitaker as Sandie
* Cecil Burroughs as Detective Grimley
* Jason Robert Moore as Detective Ericson
* Randy Mermell as Dr. Simmons
* Raymond James Calhoun as Security Guard

==Release==
The film was released on October 6, 2012 in the USA, and was eventually released under the title Killer Clown In the United Kingdom in 2013.

==References==
 

==External links==
*  

 

 
 
 
 
 
 