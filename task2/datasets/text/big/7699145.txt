Romie-0 and Julie-8
{{Infobox television |
 name = Romie-0 and Julie-8 |
 director = Clive A. Smith | Michael Hirsh Patrick Loubert |
 writer = William Shakespeare (play) |
 screenplay = (adaptation) Ken Sobol Elaine Pope |
 story = Michael Hirsh Clive A. Smith |
 starring = Greg Swanson Donann Cavin Nick Nichols | Nelvana Limited | CBC (1979, TV) Warner Home Video (1980s, VHS) |
 released = April 14, 1979 |
 country = Canada |
 language = English |
 preceded_by = The Devil and Daniel Mouse (1978 in television|1978) |
 followed_by = Intergalactic Thanksgiving (1979) |
}} androids who fall in love despite a taboo against their kind having such relationships.

The special is also known as Runaway Robots! Romie-0 and Julie-8.

==Plot==
In the future, two rival robotics firms are hard at work trying to create the next major leap in robotics. Both tout their wares at the latest robotics convention. The Mega Stellar Company has released their Romie-0 model of robot, while Super Solar Cybernetics has released Julie-8. However, unforeseen by their creators, Romie-0 and Julie-8 begin to fall in love, harboring feelings for the other.

After the convention, Romie-0 comes to Julie-8, and admits that due to their companys rivalry, they most likely cannot be together. The two decide to run away in order to keep from being broken up. Unsure where to go, they come across a rather shifty individual named Gizmo, who agrees to help them find a safe haven.

Meanwhile, both of the two robots creators (Mr Thunderbottom and Ms Passbinder) find that their creations are gone. At first they blame the other for stealing their creation, but then agree to work in tandem to find their robots when it turns out neither knows what has become of them.

Unknown to the two creators, Gizmo has transported Romie-0 and Julie-8 to a planet of junk named Trash-0-Lot, where the two come face-to-face with an enormous Junk Monster named Sparepartski. The monster has Romie-0 transported to the other side of the planet, but imprisons Julie-8 for his own purposes. Gizmo appears to her shortly afterward, and suggests that she offer to marry the Junk Monster in exchange for Romie-0s freedom off the planet. Julie-8 decides to try this offer, and Sparepartski accepts, much to the girls displeasure. While happy for Romie-0s release, Julie-8 is saddened over her fate, and removes a necessary circuit, causing her to die. In the meantime Sparepartski has found Thunderbottom and Passbinders ship nearby, and taken them prisoner as well.

Unknown to Julie-8, Romie-0 has managed to escape from the other side of the planet, and has made his way to her chambers. Upon finding her with her circuit removed, Romie-0 reinstalls it, and the two attempt to escape. Along their way out, they encounter their creators trussed up, and set about freeing them.

However, their escape does not go unnoticed, and Sparepartski soon starts to chase them across the junkscape. The two robot lovers carry their creators in hopes to get them to safety, but end up locking up when they run into a rust storm. The immobilization of the two robots causes their creators to carry them out of harms way.

The rust storm also claims Sparepartski, who it is then revealed was a giant scrap concoction made by Gizmo. Gizmo reveals his love for machines, and after seeing Julie-8 at the robotics convention, wanted to make her his bride. Romie-0 and Julie-8 then convince Gizmo that with the amount of trash on the planet, he could very well fashion his own sweetheart. Meanwhile, Mr Thunderbottom and Ms Passbinder (whove fallen in love with each other) have reconciled their differences, deciding to unite their robotics houses in a merger, much to the delight of Romie-0 and Julie-8.

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 