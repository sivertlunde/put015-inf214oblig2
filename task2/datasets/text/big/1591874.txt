Executive Suite
 

{{Infobox film
| name           = Executive Suite
| image          = Exec suite.jpeg|
| director       = Robert Wise
| producer       = John Houseman
| based on       =  
| writer         = Ernest Lehman
| starring       = William Holden Barbara Stanwyck Fredric March Walter Pidgeon
| music          = no musical score
| cinematography = George J. Folsey
| editing        = Ralph E. Winters
| studio         = 
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| country        = United States
| runtime        = 104 minutes
| language       = English budget = $1,383,000  . 
| gross          = $3,585,000 
}}
 MGM drama film depicting the internal struggle for control of a furniture manufacturing company after the unexpected death of the companys CEO. The film stars William Holden, Barbara Stanwyck, Fredric March, and Walter Pidgeon.   It was directed by Robert Wise and produced by John Houseman from a screenplay by Ernest Lehman based on the novel of the same name by Cameron Hawley.
 West Side Story, and other significant films. Over his career he would be nominated for the Academy Award six times.

While the film is one of few in Hollywood history without a musical score, the song "Singin in the Rain (song)|Singin in the Rain" is sung by young Mike Walling while he is off-camera taking a shower. The song appears in many MGM films during the period when its lyricist Arthur Freed was a producer at the studio.

==Plot==
While in New York City meeting with investment bankers at Steigel & Co., Avery Bullard, president and driving force of the Tredway Corporation, a major furniture manufacturing company in the town of Millburgh, Pennsylvania, drops dead in the street of a stroke moments after telegraphing his secretary with orders to call a meeting of the board of directors that evening. A meeting at which, its implied and assumed, he is likely to appoint an Executive Vice President who would be his immediate successor. As he collapses in the street he drops his wallet which is picked up by a bystander, emptied of its cash, and shoved into a wastebasket. Without the wallet, theres no way to immediately identify the body as Bullard.

The clock in the Western Union office on the ground floor of the fictional "Chippendale Building" reads 2:42 PM EDT when Bullard enters it to send the telegram dated (Friday) 6/19 (June 19), 1953 to his secretary, Erica Martin, stating that he will arrive in Millburgh on the "5:49" train from New York and calls for the executive committee to meet at six oclock. He collapses a few minutes later as he exits the building located on the west side of Broad Street just south of Exchange Place while attempting to hail a taxi. The rest of the film takes place over the subsequent 28 hours ending in Treadway Buildings executive suite shortly before 7 PM on Saturday evening.

George Caswell, one of the investment bankers with whom Bullard had been meeting and a member of the Tredway board of directors sees what he believes is Bullards body in the street and decides to profit from the information. He engages a broker to make a short sale of as much Tredway stock as he can in the few remaining minutes of exchange trading left on that Friday afternoon. The broker manages to sell 3700 shares of the stock and Caswell assumes hell be able to cover that sale by buying Tredway stock at a 10-point discount on Monday morning when news of Bullards death is released and exchange trading resumes.

But Bullards body remains unidentified and Caswell begins to entertain doubt that it was Bullard at all who died in the street. He spends that Friday night going through the evening papers trying to confirm that it was Bullard and calling Tredways corporate headquarters to ascertain if Bullard had shown up there. Eventually, while at the Stork Club, he reads a mention in one paper that a man with the initials "A.B." on his clothes and cufflinks has died in the street and remains unidentified. This convinces Caswell that it was in fact Bullard. So he calls the police to tip them off to the bodys identity, and orders a magnum of champagne in celebration.

The news of Bullards death travels to Millburgh and the executives at Tredway rapidly. Its Loren Shaw, the company controller, who takes the initiative in arranging Bullards funeral and coordinating the companys reaction to the news. In so doing, he effectively diminishes the stature and confidence of Treasurer Frederick Alderson, who had been one of Bullards closest friends though never a formal successor. Shaw also shrewdly releases the upcoming quarterly report immediately as the good news of big profits is likely to counter the news of Bullards death and bolster the stock price when the market opens—likely even sending the price higher.

Its the struggle among the five vice presidents to succeed Bullard as company president that makes up the substance of the films plot. With no formal heir apparent, the field is wide open.

The initial frontrunner is the ambitious but narrowly focused Shaw, who is concerned more with profitability and satisfying the stockholders than the quality of the companys products and long-term company growth. He gains the proxy of the daughter of the companys founder, who is still a major shareholder and board member, Julia Tredway. She had also been hopelessly in love with Bullard for many years.
 
Shaw also gains the vote of Caswell in return for being allowed to purchase 4,000 shares of stock at the Friday closing price to cover his short sale. If Caswell doesnt get those shares from the company reserve, he will be ruined when the exchange demands he pay the higher price for the stock he has already sold.

Alderson and idealistic Vice President for Design and Development, Don Walling, together try to come up with a viable alternative to Shaw. After going through all the contenders, they conclude that its Walling himself who is the only reasonable second candidate.. Thats despite that Walling would rather spend his time developing new products and more efficient manufacturing methods, and his wife Mary is strongly against his giving up that dream. Veteran Vice President of Manufacturing Jesse Grimm is opposed to Walling because of his youth and advanced education, while back-slapping Vice President of Sales Walt Dudley is being blackmailed by Shaw, having been caught having an affair with his secretary.

The machinations, bargaining and maneuvering leading up to the election propel the plot. Wallings enthusiasm and vision, and a stirring boardroom speech, eventually carry the day, and he is unanimously elected the new company president.

==Cast==
* William Holden as McDonald "Don" Walling, Vice President for Design and Development
* Fredric March as Loren Phineas Shaw, Vice President and Controller
* Walter Pidgeon as Frederick Y. Alderson, Vice President and Treasurer Paul Douglas as J. Walter Dudley, Vice President for Sales
* Barbara Stanwyck as Julia O. Tredway, daughter and heir of the founder of Tredway
* Louis Calhern as George Nyle Caswell, board member
* Dean Jagger as Jesse Q. Grimm, Vice President for Manufacturing
* Nina Foch as Erica Martin, secretary to the late CEO, Avery Bullard
* Shelley Winters as Eva Bardeman, secretary to Walter Dudley, as well as his mistress
* June Allyson as Mary Blemond Walling, wife of Don Walling
* Tim Considine as Mike Walling, son of Don Walling William Phipps as Bill Lundeen, a manager reporting to Don Walling
==Reception==
According to MGM records the film earned $2,682,000 in the US and Canada and $903,000 outside, resulting in a profit of $772,000. 
==Awards and nominations==
The film received four Academy Award nominations:    Best Actress in a Supporting Role (Nina Foch) Best Art Direction (Cedric Gibbons, Edward Carfagno, Edwin B. Willis, Emile Kuri) Best Cinematography Best Costume Design

The film also received two BAFTA Awards nominations: Best Film from any source Best Actor (Fredric March)

The film won the Special Jury Prize at the Venice Film Festival for best ensemble acting for the entire cast.

==TV series==
 
  television series Stephen Elliott, Paul Lambert, Richard Cox, Trisha Noble, Carl Weintraub, Maxine Stuart, and Ricardo Montalban.
 ABC and then The Rockford Files on NBC doomed the show to poor ratings, and it was canceled after one season.

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 