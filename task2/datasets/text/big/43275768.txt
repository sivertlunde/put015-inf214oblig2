Iracema (1949 film)
{{Infobox film
| name =  Iracema 
| image = 
| caption = 
| director = Vittorio Cardineli   Gino Talamo
| producer =Henrique Ferrari 
| writer =  Mário Silva (writer)|Mário Silva   Vittorio Cardineli
| based on =   Carlos Machado
| music = Concordio Donicelli   
| cinematography = Amleto Daissé 
| editing = Gino Talamo 
| studio =  Nova Terra Filmes  
| distributor = 
| released = 1949
| runtime = 
| country = Brazil Portuguese
| budget =
| gross =
| website =
}} historical drama novel of the same title.  The story is set against the early contacts between European and Native Americans in what became Brazil.

==Cast==
*    Ilka Soares as Iracema  
* Mário Brasini as Martins  
* Luís Tito as Poty  
* Nicolai Jartulary as Irapuã   Carlos Machado as Araken 
* Coaracy Pereira as Iracemas son

==References==
 

==Bibliography==
* Sadlier, Darlene Joy (ed.) Latin American Melodrama: Passion, Pathos, and Entertainment. University of Illinois Press, 2009.

==External links==
* 

 
 
 
 
 
 
 
 
 
 

 