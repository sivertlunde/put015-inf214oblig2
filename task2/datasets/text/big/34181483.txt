Nee Premakai
 
 
{{Infobox film
| name           = Nee Premakai
| image          = Nee_Premakai.jpg
| image_size     =
| caption        =
| director       = Muppalaneni Shiva
| producer       = D. Ramanaidu|Dr. D Rama Naidu
| writer         = V. Satish
| narrator       = Abbas Laya Laya Sonia Agarwal
| music          = S. A. Rajkumar
| cinematography = V. Jayaram
| editing        = K.V. Krishna Reddy
| studio         = Suresh Productions
| distributor    =
| released       =  
| runtime        =
| country        = India
| language       = Telugu
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}
 Telugu Love Laya and Sonia Agarwal played the lead roles.  Muppalaneni Shiva received Nandi Award for Best Screenplay Writer for this film. 

==Story== MCA education for Anjali. Prabhu takes the credit by pretending to be that anonymous well wisher. Prabhu exploits Srinivas to the maximum and uses it to attract Anjali. At the same time, Prabhu creates such a situation where Anjali start hating Srinivas. A relative of Srinivas (Brahmanandam) talks with the parents of Anjali and makes them agree to marry off Anjali to Srinivas. But when Anjali comes to know about this, she says that she is in love with Prabhu. Anjali parents alter their decision and decide to marry off Anjali to Prabhu.

==Cast== Abbas
* Vineeth Laya
* Sonia Agarwal
* D. Ramanaidu
* Kaikala Satyanarayana Chandramohan
* Brahmanandam Sudhakar
* AVS
* Ali
* MS Narayana
* KPV Prasad Manorama
* Kavitha
* Sana
* Anitha Chowdary
* Vimalasri

==Audio==
 

{{Infobox album
| Name        = Nee Premakai
| Tagline     = 
| Type        = film  
| Artist      = S. A. Rajkumar
| Cover       = 
| Released    = 2002
| Recorded    = 
| Genre       = 
| Length      = 27:30
| Label       = 
| Producer    = S. A. Rajkumar
| Reviews     =
| Last album  = 
| This album  =
| Next album  = 
}}
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 90%;"
|- align="center"
! style="background:#B0C4DE;" | No
! style="background:#B0C4DE;" | Song title
! style="background:#B0C4DE;" | Singers
! style="background:#B0C4DE;" | Duration
|-
| 1||"Vendi Mabbula Pallakilo" || Rajesh, K. S. Chithra|Chithra||04:55
|-
| 2||"Kalalu Kanna Nekai" ||Sanjay, Srilekha Parthasarathy ||05:02
|-
| 3||"O Prema Swagatham" || Rajesh, Chithra||04:41
|-
| 4||"Koti Taralaa " || Rajesh, Singer Usha|Usha||04:22
|-
| 5||"Manasannade Ledu" || S. P. Balasubrahmanyam||04:58
|-
| 6||"Mandakini Mandakini " || Rajesh, Chithra||04:52
|}

==Awards==
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 90%;"
|- align="center"
! style="background:#B0C4DE;" |  Award
! style="background:#B0C4DE;" | Category
! style="background:#B0C4DE;" | Nominee
! style="background:#B0C4DE;" | Outcome
|-
| Nandi Awards
| Nandi Award for Best Screenplay Writer
| Muppalaneni Shiva
|  
|}

== References ==
 

 

 
 
 

 