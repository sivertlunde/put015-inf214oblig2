Mr. Fix-It (1918 film)
{{Infobox film
| name           = Mr. Fix-It
| image          = Mrfixit-newspaperadvert-1918.jpg
| caption        = Newspaper advertisement.
| director       = Allan Dwan
| producer       = Douglas Fairbanks Jesse L. Lasky
| writer         = Ernest Butterworth (story) Allan Dwan Joseph Henabery Marjorie Daw Katherine MacDonald Frank Campeau Ida Waterman Fred Goodwins
| music          = 
| cinematography = 
| editing        = 
| studio         = Famous Players-Lasky/Artcraft
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 minutes 
| country        = United States  Silent English intertitles
| budget         = 
}}
 Marjorie Daw, and Wanda Hawley, directed by Allan Dwan at Artcraft Pictures, and released by Paramount Pictures. 

==Preservation status==
On July 16, 2011 at the Castro Theatre in San Francisco, the San Francisco Silent Film Festival presented a restored print of the film from George Eastman House. 

==References==
 

==See also==
*List of rediscovered films

==External links==
*  
* 
* 

 

 
 
 
 
 
 


 