Re-Animator
 
{{Infobox film
| name = Re-Animator
| image = Reanimator poster.png
| image_size = 215px
| alt = 
| caption = Theatrical release poster
| director = Stuart Gordon
| producer = Brian Yuzna
| screenplay = {{plainlist|
* Stuart Gordon
* William J. Norris
* Dennis Paoli
}}
| based on =  
| starring = {{plainlist|
* Bruce Abbott
* Barbara Crampton David Gale
* Robert Sampson
* Jeffrey Combs
}}
| music = Richard Band
| cinematography = Mac Ahlberg
| editing = Lee Percy
| studio = Re-Animator Productions
| distributor = Empire Pictures
| released =  
| runtime = 86 minutes
| country = United States
| language = English German
| budget = $900,000 
| gross = $2 million 
}} science fiction horror comedy film loosely based on the H. P. Lovecraft episodic novella "Herbert West–Reanimator."  Directed by Stuart Gordon, it was the first film in the Re-Animator series. The film has since become a cult film, driven by fans of Jeffrey Combs (who stars as Herbert West) and Lovecraft, extreme gore, and the combination of horror and comedy.

==Plot==
At University of Zurich Institute of Medicine in Switzerland, Herbert West brings his dead professor, Dr. Hans Gruber, back to life. There are horrific side-effects, however; as West explains, the dosage was too large. When accused of killing Gruber, West counters: "I gave him life!"

West arrives at Miskatonic University in New England in order to further his studies as a medical student. He rents a room from fellow medical student Dan Cain and converts the buildings basement into his own personal laboratory. West demonstrates his reanimating reagent to Dan by reanimating Dans dead cat Rufus.  Dans fiancee Megan, who already thinks West is creepy, walks in on this experiment and is horrified.

Dan tries to tell Dr. Alan Halsey, who is Megans father and dean of the medical school, about Wests success in reanimating the dead cat, but the dean does not believe him.  When Dan insists, the dean infers that Dan and West have gone mad. Barred from the school, West and Dan sneak into the morgue to test the reagent on a human subject in an attempt to prove that the reagent works, and thereby salvage their medical careers.  The corpse they inject comes back to life, but in a frenzied, violent, zombie-like state. Dr. Halsey stumbles upon the scene and, despite attempts by both West and Dan to save him, he gets killed by the reanimated corpse, which West then kills with a bone-saw.  Unfazed by the violence and excited at the prospect of working with a freshly dead specimen, West injects Dr. Halseys body with his reanimating reagent. Dr. Halsey returns to life, but in a zombie (fiction)|zombie-like state.

Dr. Halseys colleague Dr. Carl Hill, a research-oriented brain surgeon, takes charge of Dr. Halsey, whom he puts in a padded observation cell adjacent to his office.  He carries out a surgical operation on him, lobotomizing him.  During the course of this operation, he discovers that Dr. Halsey is not sick, but dead and reanimated.

Dr. Hill goes to Wests basement lab and tries to blackmail him into turning his reagent and notes over to Dr. Hill so he (Dr. Hill) can take credit for Wests discovery. West offers to demonstrate the reagent, and puts a few drops of it onto a microscope slide with some dead cat tissue on it.  While Dr. Hill is peering through the microscope at this slide, West decapitates him with a shovel, snarling "plagiarism|Plagiarist!" as he drives the blade of the shovel through Dr. Hills neck.  West then reanimates Dr. Hills head and body separately. While West is questioning Dr. Hills head and taking notes, Dr. Hills body sneaks up behind him and knocks him unconscious. The body carries the head back to Dr. Hills office, with Wests reagent and notes. 

Exercising mind control over Halsey, Dr. Hill sends him out to kidnap Megan from Dan.  While being carried to the morgue by her reanimated father, Megan faints.  When she arrives, Dr. Hill straps her unconscious body to a table, strips her naked, and sexually abuses her, shoving his bloody, severed head between her legs.  She wakes up in the middle of this experience.

West and Dan track Halsey to the morgue. West distracts Dr. Hill while Dan frees Megan. Dr. Hill reveals that he has reanimated and lobotomized several corpses from the morgue, rendering them susceptible to mind control. However, Megan manages to get through to her father, who fights off the other corpses long enough for Dan and Megan to escape. In the ensuing chaos, West injects Dr. Hills body with a lethal overdose of the reagent. Dr. Hills body mutates rapidly and attacks West, who screams out to Dan to save his work.

Dan retrieves the satchel containing Wests reagent and notes. As Dan and Megan flee the morgue, one of the reanimated corpses attacks and kills Megan. Dan takes her to the hospital emergency room and tries to revive her, but she is quite dead. In despair, he injects her with Wests reagent.  As the scene fades to black, Megan returns to life and screams.

==Cast==
* Jeffrey Combs as Herbert West
* Bruce Abbott as Dan Cain
* Barbara Crampton as Megan Halsey David Gale as Dr. Carl Hill
* Robert Sampson as Dean Alan Halsey
* Al Berry as Dr. Hans Gruber
* Carolyn Purdy-Gordon as Dr. Harrod
* Ian Patrick Williams as Swiss professor
* Gerry Black as Mace
* Peter Kent as Melvin the Re-Animated

==Production==
The idea to make Re-Animator came from a discussion Stuart Gordon had with friends one night about vampire films.    He felt that there were too many Dracula films and expressed a desire to see a Frankenstein film. Someone asked if he had read "Herbert West-Reanimator" by H.P. Lovecraft. Gordon had read most of the authors works, but not that story, which had been long out of print. He went to the Chicago Public Library and read their copy. 
 Dark Star, repeatedly told Gordon that the only market for horror was in feature films, and introduced him to producer Brian Yuzna. Gordon showed Yuzna the script for the pilot and the 12 additional episodes. The producer liked what he read and convinced Gordon to shoot the film in Hollywood because of all the special effects involved. Yuzna made a distribution deal with Charles Bands Empire Pictures in return for post-production services. 
 Evil Dead The Howling." lividities and different corpses". Fischer 1985, p. 45.  He and Gordon also used a book of forensic pathology in order to present how a corpse looks once the blood settles in the body, creating a variety of odd skin tones. Naulin said that Re-Animator was the bloodiest film he had ever worked on. In the past, he never used more than two gallons of blood on a film; on Re-Animator, he used 24 gallons. 
 David Gale could bend over and stick his head through so that it appeared to be the one that the walking corpse was carrying around. 

==Release==
The film was re-released with a premiere on May 21, 2010, as part of Creation Entertainments Weekends of Horror. 

===R-rated version===
When Re-Animator was originally released on VHS and Beta by Vestron Video, two versions were available: the unrated theatrical cut and an edited R-rated version, for those video stores whose rental policies would not allow them to rent unrated films that would be considered films with an MPAA X rating.

In the R-rated version, all extreme gore was edited out, but edited back into the film was a deleted scene: Before going to the Hospital morgue to confront Dr. Hill, Dan discovers West injecting himself with a watered down version of his reagent in order to fight off fatigue.

==Reception==
Re-Animator was released on October 18, 1985, in 129 theaters and grossed USD$543,728 on its opening weekend. It went on to make $2,023,414 in North America, above its estimated $900,000 budget.   

The film was well received by critics, earning mostly positive reviews, and today has a 94% "Fresh" rating on   in the Anthony Perkins mold. West is a figure of fun, but Combs doesnt spoof him".    In his review for the Los Angeles Times, Kevin Thomas wrote, "The big noise is Combs, a small, compact man of terrific intensity and concentration".    David Edelstein, writing for Village Voice, placed the film in his year-end Top Ten Movies list.

In their book  ,  , and even spawned a short-lived series of comic books. Even though it was a hit with audiences, the film generated a huge amount of controversy among Lovecraft readers. Fans thought the film a desecration of Lovecraft; their literary hero would never write such obvious exploitation! But the final criticism of the film might have been a bit more muted if these fans had actually read the "West" stories, which are pure exploitation. Lovecraft himself acknowledged as much, and female love interest and black sex humor aside, Re-Animator really is one of the more faithful and effective adaptations." 

Entertainment Weekly ranked the film #32 on their list of "The Top 50 Cult Films".    and also ranked it #14 on their "The Cult 25: The Essential Left-Field Movie Hits Since 83" list.   

==Legacy== From Beyond; though this film featured a story unrelated to Re-Animator, it was also directed by Stuart Gordon and starred both Jeffrey Combs and Barbara Crampton.

In the book  , producer-director Brian Yuzna mentions an idea that he had for a fourth Re-Animator. This version would have been titled Isle of Re-Animator, and would have been strongly influenced by the H. G. Wells novel The Island of Doctor Moreau. 
 musical adaptation opened on Broadway, which director Gordon participated in. 
 Army of Darkness Vs. Re-animator was released by Dynamite Entertainment in which Ash Williams of the Evil Dead series is admitted to Arkham Asylum and there confronts Herbert West of the Re-Animator series.

In 2015, graphic designer samRAW08 released an animated Film poster. 

==Comic==
Dynamite Comics will release in April 2015 the Comic adaption of the film. 

==See also==
* List of zombie films

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 