Itha Ivide Vare
{{Infobox film 
| name           = Itha Ivide Vare
| image          =
| caption        =
| director       = IV Sasi
| producer       = Hari Pothan
| writer         = P Padmarajan
| screenplay     = P Padmarajan Madhu Sharada Sharada Jayabharathi MG Soman
| music          = G. Devarajan
| cinematography = K Ramachandrababu
| editing        = K Narayanan
| studio         = Supriya 
| distributor    = Supriya 
| released       =  
| country        = India Malayalam
}}
 1977 Cinema Indian Malayalam Malayalam film, directed by IV Sasi and produced by Hari Pothan. The film stars Madhu (actor)|Madhu, Sharada (actress)|Sharada, Jayabharathi and MG Soman in lead roles.After the huge success of this film MG Soman achieved the Super Star status.This is the first commercial successful script of P.Padmarajan. The film had musical score by G. Devarajan.   

==Cast==
 

*MG Soman as Vishwanathan Madhu as Tharavukaran Paili
*Jayan as Ferryboat Man
*K. P. Ummer as Vasu,Vishwanathans father Sharada as Janu
*Jayabharathi as Ammini
*Kaviyoor Ponnamma as Kamalakshi
*Adoor Bhasi as Nanu
*Sankaradi as Shivaraman Nair
*Sreelatha Namboothiri as Shankari
*Master Raghunath as Young Vishwanathan
*Bahadoor as Vakkachan
*KPAC Sunny as Thug Meena as Janus Mother
*Vidhubala as Susheela
 

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Yusufali Kechery. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Entho Etho || P. Madhuri || Yusufali Kechery || 
|-
| 2 || Itha Itha Ivide Vare || K. J. Yesudas || Yusufali Kechery || 
|-
| 3 || Naadodippaattinte || P Jayachandran, P. Madhuri || Yusufali Kechery || 
|-
| 4 || Raasaleela || K. J. Yesudas || Yusufali Kechery || 
|-
| 5 || Vennayo || K. J. Yesudas || Yusufali Kechery || 
|}

==References==
 

==External links==
*  

 
 
 

 