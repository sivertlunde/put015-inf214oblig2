The Curse of the Living Corpse
{{Infobox film
| name           = The Curse of the Living Corpse
| image          = Poster of the movie The Curse of the Living Corpse.jpg
| image size     = 
| alt            = 
| caption        = 
| director       = Del Tenney
| producer       = Alan V. Iselin Del Tenney
| writer         = Del Tenney
| narrator       = 
| starring       = Helen Warren Roy R. Scheider Margot Hartman Robert Milli Hugh Franklin Candace Hilligoss
| music          = Bill Holmes
| cinematography = Richard L. Hilliard
| editing        = Gary Youngman Jack Hirshfeld
| studio         = Deal Films
| distributor    = 20th Century Fox
| released       = April 1964
| runtime        = 83 mins.
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}

The Curse of the Living Corpse is a 1964 Low-budget film|low-budget American horror film, written, directed, and produced by Del Tenney.  In the film, a series of murders haunt the family of a man who died leaving extensive instructions in his will to avoid his being buried alive. The film marked the feature film debut of actor Roy Scheider. It was originally co-billed with The Horror of Party Beach (1964).  Both movies were filmed in black-and-white in Stamford, Connecticut by Iselin-Tenney Productions, a short-lived production company the director formed with Alan V. Iselin, the owner of a chain of drive-in theaters. 

==Plot==
 
Rufus Sinclair suffers from catalepsy and lives in fear of being prematurely pronounced dead and buried alive. To prevent this he leaves detailed instructions to the family and his staff in case he is believed to be dead. But when he is found seemingly dead one day, his greedy family - eager to claim their inheritance - have him quickly interred. Rufus leaves specific instructions on how to be buried, which are violated by all involved. The family lawyer, while reading the will, lets them all know they will die from what they fear most.

Bruce (Robert Milli), will have his face disfigured. The widow Abigail (Helen Warren) will die by fire; asthmatic and alcoholic son Philip (Roy Scheider) will suffocate; Philips frustrated wife Vivian (Margot Hartman) will drown; faithful manservant Seth (J. Frank Lucas) will "join me in my tomb"; and nephew- and-all-around-nice-guy James (Robert Benson) will lose that which is most dear to him - obviously, his pretty wife, Deborah.

After Abigail reveals she left a diamond broach on his coffin, Bruce, needing the money, and Family maid and lover Lettie recover it, though Bruce is perturbed to find it on the floor. He leaves Lettie at the crypt, saying they need to return separately and against her wishes, and she is beheaded by a masked killer, seemingly Rufus returned from the grave. He leaves the head to be discovered by Bruce and others on a dinner tray, and vows to stop Rufus while forcing Vivian to help him.

Bruce is later maimed in the face by the masked killer, who then drags him to his death behind a horse. Vivian reveals that Lettie was murdered, and Seth tells the remaining family members that Bruces corpse is at the stable. Phillip finds Abigails diamond pin on Bruces body, and Abigail runs away in tears. The family lawyer sends Robert into town for the police. Phillip is named the new family patriarch now that Bruce is dead, and vows to do his best. Abigail, distraught, says shes glad Rufus is alive so he can pay for the two murders. Seth feels hes in the clear, until Phillip reminds Seth that he violated the terms of the will, and is "One of them, now."

The police arrive, and Phillip informs him that the family believes he was buried alive. The lawyer agrees, and says that Rufus will now be totally insane. Phillip and the rest of the family give the police Rufus description, and the police vow to find him. After Abigail has a panic attack, Phillip stays with her on her request, but then ducks out to get a drink, which he shares with one of the policemen. They get drunk, and Phillip tells him she has a morbid dislike of fire. The masked figure later enters Abigails room, chloroforms her, binds her to the bed, and sets her aflame. The family is unable to save her. The men then search with dogs, leaving Vivian and Deborah at the house.

Seth enters the crypt, apologizing for failing in his task of lighting the torches. He attempts to do so, and is murdered when the figure pulls a sword from the cane, fulfilling the wills threat he would die in the crypt. Meanwhile, Deborah convinces Vivian that they should dress up for Phillip and Robert. Phillip and Robert wander from the search party, and Robert finds Seths corpse.

The masked killer knocks out the policeman on guard and then strangles Vivian while shes bathing. Deborah walks in and the masked killer drags her away to the bog. Robert chases after them. The killer is revealed to be Phillip, upset with his treatment by his father and the rest of the family, the forgotten child. Phillip says he did not wish to kill her but must now. Robert arrives and the two fight. Robert gains the upper hand and Phillip sinks into the bog, dying as the will foretold.

Robert and Deborah leave happily, and the two policemen decide to share Phillips remaining booze, while lamenting over his ability to commit all the murders. They discover that the alcohol was merely tea, and that Phillip was not an alcoholic at all, just playing one so he could get away with the crimes.

==Cast==
*Helen Warren as Abigail Sinclair Roy R. Scheider as Philip Sinclair
*Margot Hartman as Vivian Sinclair
*Robert Milli as Bruce Sinclair
*Hugh Franklin as James Benson
*Jane Bruce as cook
*Candace Hilligoss as Deborah Benson
*Dino Narizzano as Robert Harrington
*Linda Donovan as Letty Crews
*J. Frank Lucas as Seth Lucas
*Paul Haney as Constable Barnes
*George Cotton as Constable Winters
*William Blood as minister

==Production==
The film was shot in Stamford, Connecticut. Robert Verberkmoss served as art director.

==References==
 

== External links==
* 

 
 
 
 
 
 