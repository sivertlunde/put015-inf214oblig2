The Luck of the Irish (1948 film)
{{Infobox film
| name           = The Luck of the Irish
| image          = LuckIrish1948.jpg
| image_size     = 
| caption        = Theatrical poster
| director       = Henry Koster
| producer       = Fred Kohlmar Philip Dunne
| narrator       = 
| starring       = Tyrone Power Anne Baxter Lee J. Cobb
| music          = Cyril Mockridge
| cinematography = 
| editing        = 
| distributor    = 20th Century Fox
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} 1948 film with Tyrone Power, Anne Baxter, Lee J. Cobb, Cecil Kellaway, and Jayne Meadows.

==Plot==
Stephen Fitzgerald (Tyrone Power), a newspaper reporter from New York, meets a leprechaun (Cecil Kellaway) and a beautiful young woman (Anne Baxter) while traveling in Ireland. When he returns to his fiancée (Jayne Meadows) and her wealthy father (Lee J. Cobb) in the midst of a political campaign in New York, he finds that the leprechaun and the young woman are now in the big city as well. Stephen is torn between the wealth he might enjoy in New York or returning to his roots in Ireland.

==Cast==
* Tyrone Power as Fitzgerald
* Anne Baxter as Nora
* Cecil Kellaway as Horace
* Lee J. Cobb as David
* Jayne Meadows as Frances

==Awards and nominations== Best Supporting Actor (nomination) - Cecil Kellaway

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 

 