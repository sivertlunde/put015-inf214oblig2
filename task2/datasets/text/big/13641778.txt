The Animal World (film)
{{Infobox film
|name= The Animal World image = TheAnimalWorld.jpg director = Irwin Allen producer = Irwin Allen writer = Irwin Allen narrator = Theodore Von Eltz
|music= Paul Sawtell distributor = Warner Bros. released =   runtime = 82 minutes
}}
The Animal World is a 1956 documentary film that was produced, written and directed by Irwin Allen. The film includes live-action footage of animals throughout the world, along with a ten-minute stop motion animated sequence about dinosaurs.

Irwins intention was to show the progression of life over time, although he noted, "We dont use the word evolution. We hope to walk a very thin line. On one hand we want the scientists to say this film is right and accurate, and yet we dont want to have the church picketing the film." 

==Dinosaur sequence==
The special effects in the films dinosaur sequence were produced by Ray Harryhausen and Willis OBrien. Irwin originally planned to film the scenes as a series of static dioramas with plastic models, but Harryhausen suggested that the scenes would be more memorable if they were animated. Roy P. Webber. The Dinosaur Films of Ray Harryhausen. McFarland & Company, 2004. p. 87-88.  The dinosaurs that appear include an Allosaurus, a Stegosaurus, a pair of Ceratosaurus|Ceratosaurs, a Triceratops, a Tyrannosaurus, and a family of Apatosaurus|Apatosaurs. 
 The Black Scorpion. 

==Notes==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 


 