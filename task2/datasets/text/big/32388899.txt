The Sergeant (film)
{{Infobox film
| name = The Sergeant
| image = File:Sergeant poster.jpg
| image_size =
| caption = John Flynn
| producer = Robert Wise Richard Goldstone Dennis Murphy (screenplay) Dennis Murphy (novel)
| starring = Rod Steiger John Phillip Law Ludmilla Mikael
| music =
| cinematography =
| editing =
| distributor = Warner Bros.-Seven Arts
| released =  
| runtime = 108 min
| country = United States
| language = English
| budget =
| gross = $1.2 million (US/ Canada rentals) 
}} John Flynn, and released by Warner Bros.-Seven Arts. 

==Plot==
A dedicated, decorated war veteran, Sgt. Callan (Rod Steiger), is posted in France at a fuel supply depot in 1952. Finding a lack of discipline under the frequently drunk Capt. Loring, he takes charge in a tough, no-nonsense manner.

But distracting the sergeant is a physical attraction to one of his men, Private First Class Swanson (John Phillip Law), that seems at odds with everything in Callans personality. He makes Swanson his orderly and befriends him socially, but behind his back scares off Solange, the privates girlfriend (Ludmila Mikaël).

Callans confusion and depression grows and he begins to drink. Unable to resist the urge, the sergeant attempts to kiss Swanson and is violently warded off. He turns up for morning formation hungover and Loring relieves him of duty. Callan goes off to a nearby woods alone, rifle in hand, and commits suicide.

==Cast==
*Rod Steiger: MSgt. Albert Callan
*John Phillip Law: Pfc. Tom Swanson
*Ludmila Mikaël: Solange
*Frank Latimore: Capt. Loring 
* Elliott Sullivan: Pop Henneken

==Production==
In 1966, Wise set up a company to produce low-budget films that others would direct. He optioned Dennis Murphy’s novel The Sergeant and hired his former assistant, John Flynn, to direct. Flynn says Simon Oakland badly wanted to play the lead but so did Rod Steiger who was in much demand at the time, and Steiger played it for less than his usual fee.   accessed 16 February 2015 

==Legacy==
The film was excerpted in the documentary film The Celluloid Closet (1996).

==See also== Reflections in a Golden Eye (1967)

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 


 