Stranger on My Land
{{multiple issues|
 
 
}}
{{Infobox television film
| name           = Stranger on My Land
| image          =
| caption        =
| genre          = Drama Western
| director       = Larry Elikann
| producer       = Edgar J. Scherick Gary Hoffman Michael Barnathan Mitch Engel
| writer         = Edward Hume I.C. Rapoport
| narrator       =
| starring       = Tommy Lee Jones Jeff Allin Richard Anderson
| music          = Ron Ramin
| cinematography = Laszlo George
| editing        = Peter V. White
| studio         = Taft Entertainment Edgar J. Scherick Associates Republic Pictures (Viacom) ABC
| network        = ABC
| released       =  
| runtime        = 94 minutes
| country        = United States
| language       = English
}} made for television western film, premiered on American Broadcasting Company, directed by Larry Elikann and is also Joseph Gordon-Levitts screen debut. 

==Plot==
Vietnam veteran rancher Bud Whitman (Jones) tries to stop the Air Force from building an air base on his land. 

==Cast==
*Tommy Lee Jones as Bud Whitman
*Jeff Allin as Marine Captain
*Richard Anderson as Maj. Walters
*Michael Paul Chan as Eliot
*Joseph Gordon-Levitt as Rounder
*Dee Wallace as Annie Whitman
*Barry Corbin as Gil 
*Terry OQuinn as Connie Priest 
*Pat Hingle as Judge Munson Michael Flynn as Brewer  Ben Johnson as Vern Whitman

== References ==
 

 
 
 
 
 

 
 