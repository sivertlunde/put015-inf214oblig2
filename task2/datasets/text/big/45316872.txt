The Little French Girl
{{Infobox film
| name           = The Little French Girl
| image          = 
| alt            = 
| caption        =
| director       = Herbert Brenon
| producer       = Jesse L. Lasky Adolph Zukor
| screenplay     = John Russell Anne Douglas Sedgwick Neil Hamilton Julia Hurley Jane Jennings
| music          = 
| cinematography = Harold Rosson
| editing        = 
| studio         = Famous Players-Lasky Corporation
| distributor    = Paramount Pictures
| released       =  
| runtime        = 62 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 drama silent Neil Hamilton, Julia Hurley and Jane Jennings. The film was released on May 31, 1925, by Paramount Pictures.  

==Plot==
 

==Cast== 
*Mary Brian as Alix Vervier
*Maurice de Canonge as Jerry Hamble
*Paul Doucet as Andre Valenbois
*Maude Turner Gordon as Lady Mary Hamble Neil Hamilton as Giles Bradley Julia Hurley as Mme. Dumont
*Jane Jennings as Mother Bradley
*Anthony Jowitt as Owen Bradley
*Alice Joyce as Madame Vervier
*Mario Majeroni as DeMaubert
*Esther Ralston as Toppie Westmacott
*Mildred Ryan as Ruth Bradley
*Eleanor Shelton

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 