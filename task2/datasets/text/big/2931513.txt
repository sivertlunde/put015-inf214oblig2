Volcanoes of the Deep Sea
{{Infobox film
| name           = Volcanoes of the Deep Sea
| image          = 
| caption        = 
| director       = Stephen Low
| producer       = James Cameron Alex Low   Pietro L. Serapiglia
| writer         = Alex Low   Stephen Low
| narrator       = Ed Harris
| music          = Michel Cusson
| cinematography = William Reeve
| editing        = James Lahti
| distributor    = 
| released       =  
| runtime        = 40 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Volcanoes of the Deep Sea is a 2003 documentary film directed by Stephen Low in the IMAX format about undersea volcanoes.

==Production==
Richard Lutz served as Principal Investigator and Lutz and Peter A. Rona served as Science Directors of the film, which was funded by the National Science Foundation and co-produced by Rutgers University. {{cite news
| title       = Richard A. Lutz - Professor
| first       = 
| last        = 
| url         = http://marine.rutgers.edu/main/IMCS-People-Details/People-Details-Richard-A.-Lutz.html
| newspaper   = 
| publisher   = Rutgers
| accessdate  = 2011-05-28
}} 

==Content==
The film included footage, research, and stories from the deep-sea DSV Alvin|Alvin expeditions of Lutz and his colleagues. {{cite news
| title       = Noted Marine Biologist to Head Marine and Coastal Sciences Institute at Rutgers
| first       = Paula
| last        = Walcott-Quintin
| url         = http://news.rutgers.edu/medrel/news-releases/2011/02/noted-marine-biologi-20110228
| publisher   = Rutgers
| date        = February 28, 2011
| accessdate  = 2011-05-28
}} 

Scientists use the deep-water submersible DSV Alvin to search for an animal, the paleodictyon nodosum, that produces a honeycomb patterned fossil called a Paleodictyon, near volcanic vents that lie 3500 meters (12,000 feet) underwater in the Mid-Atlantic Ridge.

==Reception==
The film received the award for "best IMAX film of the Year" at the Paris Film Festival, {{cite news
| title       = Exploration on the Origin of Life - Exciting Discoveries at Deep-sea Hydrothermal Vents
| first       = 
| last        = 
| authorlink  = 
| url         = http://www.tsinghua.edu.cn/publish/szen/5071/2010/20101217121520157959485/20101217121520157959485_.html
| newspaper   = 
| publisher   = Tsinghua University
| date        = 
| accessdate  = 2011-05-28
| quote       = 
}}  and in 2005 Richard Lutz received the Scientific Literacy Achievement Award from the New Jersey Association for Biomedical Research for his contributions to the film. 
 US theatres because it was feared that the films mention of evolution would provoke a negative reaction from creationist patrons. In particular, the film discussed the similarities in bacterial and human DNA.   The films distributor reported that the only U.S. states with theaters which chose not to show the film were Texas, Georgia (U.S. state)|Georgia, North Carolina, and South Carolina: "Weve got to pick a film thats going to sell in our area. If its not going to sell, were not going to take it," said the director of an IMAX theater in Charleston that is not showing the movie. "Many people here believe in creationism, not evolution."  

==See also==
* 9° North

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 


 