La Soufrière (film)
{{Infobox film
  | name = La Soufrière
  | image = Soufriere_Screen_Shot.jpg 
  | caption =
  | director = Werner Herzog
  | producer =
  | writer =
  | starring = Werner Herzog
  | narrator = Werner Herzog
  | music =
  | cinematography = Edward Lachman Jörg Schmidt-Reitwein
  | editing = Beate Mainka-Jellinghaus
  | studio = Werner Herzog Filmproduktion
  | distributor =
  | released = 1977
  | runtime = 30 min
  | language = German French
  | budget =
  }}
 German director sublime Herzog seeks to conjure in his films. Herzog converses in French with three different men he finds remaining on the island: one says he is waiting for death, and demonstrates his posture for doing so; another says he has stayed to look after the animals. In the end, the volcano did not erupt, thus sparing the lives of those who had remained on the island, including Herzog and his crew. 
 egocentric picture madness or complete harmony with nature, the subjects of Herzogs films exemplify life at its most spiritually extreme. The people who remained on the island had accepted their fate, and had given their lives to God. One man seemed surprised that Herzog would even question his resignation to die at the hands of the volcano. Others simply did not see the point in evacuating the island. Death (and the eruption of the volcano) cannot be escaped, only temporarily avoided. Those who remained had given their lives to God and did not see the point in trying to run away from that which cannot be conquered, and should not be feared.

==See also==
* La Grande Soufrière for information on the Guadeloupean volcano.

==References==
* Paul Cronin,  , 2002, ISBN 0-571-20708-1) p.&nbsp;148. 
*   Through the 1970s, edited by  , 1984, hardcover: ISBN 0-8044-2688-0 and paperback: ISBN 0-8044-6648-3) p.&nbsp;168.

==External links==
* 
*  at Allmovie
*  (Video interview from  ) 

 

 
 
 
 
 
 
 
 


 
 