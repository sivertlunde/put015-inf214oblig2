The Mark of the Wolfman
{{multiple issues|
 
 
 
}}

{{Infobox film
| name           = La Marca del Hombre Lobo
| image          = La Marca del Hombre Lobo.jpg
| caption        = Original poster
| director       = Enrique López Eguiluz
| producer       = Maximiliano Pérez-Flores
| writer         = Jacinto Molina
| starring       = Paul Naschy, Manuel Manzaneque, Dyanik Zurakowska
| music          = Ángel Arteaga
| cinematography = Emilio Foriscot
| editing        = Francisco Jaumandreu
| distributor    = D.C. Films  (Spain) , Independent International Pictures  (USA, theatrical) 
| released       =  
| runtime        = 88 min
| country        = Spain Spanish
| budget         =
}}
 1968 3-D_film|3-D Spanish horror film, the first in a long series about the werewolf Count Waldemar Daninsky, played by Paul Naschy. The film was also known as Hells Creatures: Dracula and the Werewolf, The Nights of Satan and Frankensteins Bloody Terror (despite the fact that there is no Frankenstein monster in this film).
Naschy followed up this film with his 1968 Nights of the Wolfman (which is today a lost film) and his 1969 Monstruos del Terror.

==Plot==
A drunken Gypsy couple spending the night in the abandoned Wolfstein castle accidentally resurrect the werewolf Imre Wolfstein when they remove the silver cross from his corpse. Once alive, he not only kills the Gypsy couple, but also wreaks havoc on a nearby village. The villagers attribute the attack to ordinary wolves, and in response, form a hunting party to kill off the animals. While on the hunt, Count Waldemar Daninsky is attacked by Imre Wolfstein and is afflicted with lycanthropy. After killing innocent victims in the midst of his transformation, he seeks help from specialists, Dr. Janos de Mikhelov and his wife, who turn out to be two vampires, who then prey on both Janice and Rudolph, Waldemars friends. The vampires revive the first werewolf, Imre, from the dead, and force the two werewolves to battle each other. Waldemar kills Imre Wolfstein with his fangs and then destroys the two vampires, only to be killed in turn by a bullet fired by Janice, the woman who loved him most.

==Production== Paul VI, and “Naschy” was inspired by a well-known Hungarian Olympic athlete, Imre Nagy. La Marca del Hombre Lobo was the first in a long line of werewolf films that would make Paul Naschy world famous.

In the United States, the film was called Frankensteins Bloody Terror, solely to satisfy the distributors need for a second "Frankenstein film" to pad out a planned double feature release. To justify this odd choice of title, a specially created animated opening sequence explains that a branch of the Frankenstein family became cursed with lycanthropy and took the name Wolfstein. American producer Sam Sherman needed to fill 400 playdates for his film Dracula vs. Frankenstein, which at that time was entangled in a legal stand-off with an unscrupulous film lab contracted to produce the release prints. The 400 theaters in question had been promised a Frankenstein film, and Sherman was determined to give them one. Both films ran together in 1974 in American theaters.

La Marca del Hombre Lobo was filmed in Jan Jacobsen’s Hi-Fi Stereo 70 3-D format. When Sam Sherman learned this, he was persuaded by other investors to hire optical effects maestro Linwood Dunn to create single-strip, over-and-under 35mm prints for American release. The final results were reportedly beautiful to look at when projected through high-quality 3-D lenses (such as those created by Robert V. Bernier for Space-Vision). But a celebrity-studded Hollywood premiere was completely undone when Sherman’s fellow investors provided shoddy acrylic lenses for the projectors!

==External links==
* 

 

 
 
 
 
 
 