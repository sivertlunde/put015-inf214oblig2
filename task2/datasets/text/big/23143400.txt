My Year Without Sex (film)
{{Infobox film
| name           = My Year Without Sex
| caption        =
| image      	 = My Year Without Sex FilmPoster.jpeg
| director       = Sarah Watt
| producer       = Bridget Ikin
| writer         = Sarah Watt
| starring       = Sacha Horler Matt Day
| music          =
| cinematography =
| editing        =
| distributor    = Hibiscus Films
| released       =  
| runtime        = 92 minutes
| country        = Australia
| language       = English
| budget.        = $4 million
| gross.          = $1,125,871
}}
 
 Australian drama Altona (suburban Melbourne), it is about a 30-something couple, Ross and Natalie, and their children Ruby and Louis, after Natalie suffers a ruptured brain aneurysm and is advised not to have sex for 12 months.

Watt has said {{
cite news
 | url=http://www.theage.com.au/news/entertainment/film/no-sex-watts-the-fuss-about/2009/05/23/1242498930379.html Age
 Fairfax
 | title= No sex? Watts the fuss about?
 | date= 23 May 2009
 | first=Paul
 | last=Kalina
 | accessdate =13 May 2010 }}  that after her first film Look Both Ways, she wanted to make a film "without a sex scene":
{{quote I didnt want to be coy and just avoid it, but once we started playing with that title, the whole thing became about sex.

My ideas were so broad, about anxiety and non-sustainable consumerism and how a non-ruling-class family were coping with how to save for the future and the uncertainty in the work force.

And sex seemed a good way to corral it. A lot of our consumerism is about looking sexually attractive or being anxious.
}}

My Year Without Sex received strongly favourable reviews, and was touted by The Sydney Morning Herald as "possibly the best" Australian film of 2009 {{
cite news
 | url=http://www.smh.com.au/news/entertainment/film/film-reviews/my-year-without-sex/2009/05/28/1243456658728.html?page=fullpage#contentSwap1
 | newspaper = Sydney Morning Herald Fairfax | title=My Year Without Sex | first=Paul | last=Byrnes | date=28 May 2009
 | accessdate=13 May 2010 }}  as well as "the most accomplished" local film of 2009 by The Age. {{cite news
 | first=Jake | last=Wilson
 | title= Most accomplished local film of the year
 | url= http://www.theage.com.au/news/entertainment/film/film-reviews/most-accomplished-local-film-of-the-year/2009/05/27/1243103589703.html Age
 Fairfax
 | date=23 May 2009
 | accessdate=13 May 2010 }}   

As with Look Both Ways, My Year Without Sex deals with the impact that serious illness has on individuals and relationships. The two films are reportedly part of a "proposed trilogy". {{
cite news
 | first= Paul | last= Kalina
 | url=http://www.theage.com.au/news/entertainment/film/getting-by-among-the-sharks/2009/05/21/1242498866569.html?page=fullpage#contentSwap1 Age
 Fairfax
 | title=Getting by among the sharks | date=23 May 2009 | accessdate=13 May 2010}} 

==Box office==
My Year Without Sex grossed $1,125,871 at the box office in Australia,. 

==See also==
* Cinema of Australia
* List of Australian films

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 


 