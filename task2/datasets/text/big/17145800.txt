All the Way Home (film)
{{Infobox film
| name           = All the Way Home
| image          = All_the_way_home_poster.jpg
| image_size     = 
| caption        = Theatrical Poster
| director       = Alex Segal
| producer       = David Susskind
| writer         = James Agee (novel) Tad Mosel (play) Philip H. Reisman Jr. Robert Preston Pat Hingle
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =   
| runtime        = 97–107 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} Robert Preston, All the Way Home.

==Cast==
*Jean Simmons as Mary Follett Robert Preston as Jay Follett
*Pat Hingle as Ralph Follett
*Aline MacMahon as Aunt Hannah
*Thomas Chalmers as Joel
*John Cullum as Andrew
*Helen Carew as Marys mother
*Ronnie Claire Edwards as Sally
*John Henry Faulk as Walter Starr
*Mary Perry as Great-aunt Sadie
*Lylah Tiffany as Great-great-grandmaw
*Edwin Wolfe as John Henry
*Michael Kearney as Rufus Follett

==References==
 

==External links==
* 
*  

 
 
 
 
 
 
 


 