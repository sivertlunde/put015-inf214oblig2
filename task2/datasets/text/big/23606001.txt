Beck – Monstret
{{Infobox film
| name           = Beck – Monstret
| image          = Beck 06 - Monstret.jpg
| image_size     = 
| alt            = 
| caption        = Swedish DVD-cover
| director       = Harald Hamrell
| producer       = Lars Blomgren Thomas Lydholm
| writer         = Rolf Börjlind
| narrator       = 
| starring       = Peter Haber Mikael Persbrandt Stina Rautelin
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 1998
| runtime        = 84 min
| country        = Sweden Swedish
| budget         = 
| gross          = 
}}
Beck – Monstret is a 1998 film about the Swedish police detective Martin Beck directed by Harald Hamrell.

== Cast ==
*Peter Haber as Martin Beck
*Mikael Persbrandt as Gunvald Larsson
*Stina Rautelin as Lena Klingström
*Per Morberg as Joakim Wersén
*Ingvar Hirdwall as Martin Becks neighbour
*Rebecka Hemse as Inger (Martin Becks daughter)
*Fredrik Ultvedt as Jens Loftsgård
*Peter Hüttner as Oljelund
*Björn Gedda as Sture Andersson
*Magnus Roosmann as Gert Ahlgren Hans Jonsson as Ove Lundin
*Anna Lindholm as Anita
*Sten Ljunggren as Anitas father

== References ==
* 

== External links ==
* 

 

 
 
 
 
 


 
 