Aandhali Koshimbir
{{Infobox film
| name = Aandhali Koshimbir
| image = Aandhali-Koshimbir.jpg
| alt = Story of a lonely Zagdalu father Bapu and his Nikamma son Ranga. 
| caption = Theatrical release poster
| director = Aditya Ingale
| story    = Pratap Deshmukh
| starring = Ashok Saraf   Vandana Gupte   Anand Ingale
| producer = Sudha Productions
| editing = Pravin Jahagirdar
| music = Narendra Bhide
| released =  
| runtime = 148 minutes
| language = Marathi
| country = India
}}
 Marathi comedy film directed by Aditya Ingale and starring Ashok Saraf, Vandana Gupte and Anand Ingale.

==Plot==
Aandhali Koshimbir is about a lonely Zagdalu father Bapu and his Nikamma son Ranga.
The story also tells about the lonely Zagdalu woman Shanti and her landlord Marne,  a lawyer and a Bakwaas poet who is madly in love with her. 
For monetary reasons, Ranga and his friend, Vashya, decides to bring home Shanti to meet Bapu to get them to start a quarrel. Because she is more powerful in Zagadaa, Ranga thinks she will win over Bapu in the quarrel, and that Bapu will lose his confidence, and they will therefore have their problem solved by getting money from Bapu. They tell each of Bapu and Shanti both that the other one is mentally out of control and that their lost spouses were exactly same looking as Bapu and Shanti, and if they come and have a big quarrel, the other patient might get cured.
As planned, Shanti comes home to have Zagadaa with Bapu, and Bapu get ready to do the same. But instead of having quarrels, the two lonely souls Bapu and Shanti falls in love. 
Bapu invites Shanti to stay at his place for a few days, and the situation becomes more complicated for Ranga and Vashya, and  Marne too, as he can not give his bakwaas Poems to Shanti.
At the Bapu house, all the lonely characters come together eventually and become a family. But Marne gets more and more lonely with his poems and becomes a villain.
As he comes to know the plan of Ranga, he blackmails him and charges a court case against Bapu and Shanti. With the opposite side of the same confusion, Ranga helps Bapu and Shanti to get rid of Marne, who in the end becomes insane.

==Reception==
{{Album ratings
| rev1 = Book My Show    
| rev1Score = 
| rev2 = Marathimovieworld.com    
| rev2Score = 
}}

Aandhali Koshimbir was met with favorable reviews. Marathimovieworld gave it 3.5 out of 5 stars, calling it "a neat and clean comedy".   Bookmyshow.com gave it 3 out of 5 stars and had the verdict "A quarreling contest gone wrong."   

==Cast==
* Ashok Saraf as  Bapu Sadavarte 
* Vandana Gupte as Shanti Chitnis
* Anand Ingale as Adv. Marne
* Aniket Vishwasrao as Ranga   
* Hemant Dhome as Vashya
* Hrishikesh Joshi as Goraksha
* Priya Bapat as Manju
* Mrunmayee Deshpande as Radhika 

==References==
 

==External links==
*  

 