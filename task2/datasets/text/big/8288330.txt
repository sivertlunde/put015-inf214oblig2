Skatetown, U.S.A.
{{Infobox Film
| name           = Skatetown, U.S.A.
| image          = Poster of the movie Skatetown, USA.jpg
| image_size     = 
| caption        = Katherine Kelly Lang and Greg Bradford are shown on the theatrical release poster
| director       = William A. Levey
| producer       = Lorin Dreyfuss (producer) William A. Levey (producer) Peter E. Strauss (executive producer)
| writer         = Nick Castle (Screenplay & story) Lorin Dreyfuss (story) William A. Levey (story)
| narrator       = 
| starring       = Scott Baio, Patrick Swayze, Flip Wilson, Maureen McCormick, Katherine Kelly Lang
| music          = Miles Goodman Dave Mason
| cinematography = Donald M. Morgan
| editing        = Gene Fowler Jr.
| studio        = Rastar
| distributor    = Columbia Pictures
| released       = October 1979
| runtime        = 98 min
| country        = USA English
| budget         = 
| gross          = $2.35 million Richard Nowell, Blood Money: A History of the First Teen Slasher Film Cycle Continuum, 2011 p 259 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

Skatetown, U.S.A. is a 1979 American comedic feature film produced to capitalize on the short-lived fad of roller disco. allmovie.com,  , retrieved 25 September 2010 

The film features many TV stars from the 1960s and 1970s, among them Scott Baio, Flip Wilson, Maureen McCormick, Ron Palillo and Ruth Buzzi. Patrick Swayzes leading role as the skater "Ace" was his first movie performance. Also in the cast are Sydney Lassick, Billy Barty and Playboy centerfold model Dorothy Stratten.

==Plot==  chicken played on motorized roller skates, the two rivals become friends.

==Production notes== gag having to do with itching powder) set between long roller skating sequences and musical performances.
 soap bubbles blown by a machine from the Lawrence Welk Show can be seen in sundry scenes. Some exteriors were shot on Santa Monica Pier and at nearby Venice Beach. Patrick Swayze, who had roller skated competitively as a teenager and was a trained dancer, did his own skating and stunts in the film. April Allen, Swayzes uncredited roller skating partner in the movie, had won the world championship in womens free skating seven years earlier. Swayze, Patrick and Niemi, Lisa,  , Simon and Schuster, 2009, pp 74-75, ISBN 978-1-4391-5858-6, retrieved 1 October 2010  susan-a-miller.com,  , retrieved 2 october 2010 

Twenty-nine years after filming,  , pp 123-124. 14 October 2008, ISBN 978-0-06-149014-9 

Scott Baio later recalled:
 I have blocked that movie from my memory, it was so bad... That was that whole time where Xanadu and Roller Boogie and all that crap was coming out. That was one of those things where they sent me the script and I said “no,” but they just kept calling and offering more money! I mean, they offered me a lot of money. And finally I said, “Well, hell. What is it? Two weeks’ work? Whatever, okay, fine.” And it was… You know, sometimes money isn’t everything.   It was just bad. I mean, it was bad shooting it. I’m trying to think of any real stories that I have, but it was just insanity. When was that? ’79? It was just a guy making a film who didn’t know how to make a film, and I don’t even know what the story was!. Skatetown, U.S.A., that was crapola.   accessed 7 April 2014  

== Cast ==
* Scott Baio - Richie
* Flip Wilson - Harvey Ross
* Patrick Swayze - Ace Johnson
* Maureen McCormick - Susan Nelson
* Greg Bradford - Stan Nelson
* Ron Palillo - Frankey
* Judy Landers - Teri
* Ruth Buzzi - Elvira
* Dorothy Stratten - customer at snack bar (girl who orders pizza)
* Joe E. Ross - rent-a-cop
* Dave Mason - himself
* Billy Barty - Jimmy
* Katherine Kelly Lang - Allison
* David Landsberg - Irwin
* Sydney Lassick - Murray
* Murray Langston - the drunk
* Bill Kirchenbauer - Skatetown doctor
* Denny Johnston - the wizard (club DJ)
* Vic Dunlop - Ripple
* Len Bari - Alphonse
* April Allen - Charlene (Aces girlfriend and skating partner, uncredited)   

== Soundtrack ==
{{Infobox album
| Name        = Skatetown, U.S.A.
| Type        = Soundtrack
| Artist      = Various Artists
| Cover       = Skatetown USA soundtrack coverart.jpg
| Released    = 1979
| Recorded    = Disco
| Length      = 39:00
| Label       = Columbia Records
| Producer    = 
| Reviews     =
| Last album  =
| This album  =
| Next album  =
}}
 DJ or Traffic hit Shake Your Boogie Nights" John Beal) during the End Credits. 

A soundtrack album was released in 1979 by Columbia Records.

Side A:
#Skatetown - Dave Mason (3:11)
#Boogie Wonderland - Earth, Wind & Fire (4:49)
#Shake Your Body (Down to the Ground) - The Jacksons (3:45) Boogie Nights Heatwave (3:38)
#Born to be Alive - Patrick Hernandez (3:23)

Side B:
#Roller Girl - John Sebastian (3:10)
#Perfect Dancer - Marilyn McCoo & Billy Davis Jr. (6:28)
#I Fell in Love - Dave Mason (2:21)
#Under My Thumb - Hounds (4:17)
#Feelin Alright - Dave Mason (4:30) 
 

==Reception== Marsha   eating drugged pizza with a bearded Horshack#Arnold_Horshack|Horshack." oddculture.com,  , retrieved 28 September 2010 

It was later shown on cable television from time to time. There have been no known licenced VHS or DVD releases. This may be owing to home video licencing woes over the soundtracks many major label recordings.   35mm and 16mm full frame prints of the movie (which was shot in 35mm and cropped to widescreen for theatrical release) have been exhibited at film revivals  and low quality video copies made from a much faded full frame 16mm print have been in commercial circulation. 

==References==
 

== External links ==
* 
*  
*  
*   at  
* Go Gos blog page about   roller rink in West Hollywood

 
 
 
 
 
 
 
 