Atlantic City (1980 film)
{{Infobox film
| name = Atlantic City
| image = Atlantic City (1980 film).jpg
| caption = Theatrical release poster
| director = Louis Malle John Kemeny
| writer = John Guare
| starring = Burt Lancaster Susan Sarandon Kate Reid Robert Joy Hollis McLaren Michel Piccoli Al Waxman
| music = Michel Legrand
| cinematography = Richard Ciupka
| editing = Suzanne Baron
| studio = Selta Films
| distributor = Paramount Pictures
| released =  
| runtime = 104 minutes
| country = Canada France
| language = English French
| budget = $7.2 million
| gross = $12,729,675  
}} Canadian romantic crime film directed by Louis Malle. Filmed in late 1979, it was released in France and Germany in 1980 and in the United States in 1981. The script was written by John Guare. It stars Burt Lancaster, Susan Sarandon, Kate Reid, Robert Joy, Hollis McLaren, Michel Piccoli, and Al Waxman.
 Big Five" Best Picture, Best Director, Best Actor, Best Actress Best Screenplay, On Golden Pond.

==Plot== runs numbers in poor areas of the city; he also acts as a caretaker for Grace (Kate Reid), an elderly invalid. Dave convinces Lou to sell the cocaine for him, but as Lou sells the first batch, Dave is attacked and killed by the mobsters whom he had stolen the drugs from.

Lou is left with the remaining cocaine and continues to sell to impress Sally, whom he has long pined for, with money. Sally and Lou make love one day, but she returns to her apartment to find it trashed; she has been tracked down by Daves killers, who beat her to find out if she has the drugs. They leave, but Lou laments not being able to protect her. Sally is fired from the casino when her late husbands criminal record is discovered. Lou sells the remainder of the cocaine, while both Sally and the mobsters discover Lous affiliation with Dave. The mobsters corner them one night, but are killed when Lou produces a gun and shoots them. He and Sally then steal their car and leave the city.

At a motel during the night, Lou takes the phone to the bathroom to call Grace and brag about the killings. Sally also wakes and steals the money with the intention of sneaking off; Lou witnesses this, but allows her to leave and escape to France now that she has been identified as an accomplice in the murders. Lou returns to Atlantic City to be with Grace and continue selling a portion of the cocaine that he had stashed away.

==Cast==
* Burt Lancaster as Lou Pascal
* Susan Sarandon as Sally Matthews
* Kate Reid as Grace Pinza
* Robert Joy as Dave Matthews
* Hollis McLaren as Chrissie
* Michel Piccoli as Joseph
* Al Waxman as Alfie
* Robert Goulet as himself
The film features a cameo by Wallace Shawn as a waiter in a restaurant; Malles next film was My Dinner with Andre, where Shawn is waited on as a customer.

==Production== New York. Although filmed in the United States, the film was a co-production (filmmaking)|co-production between companies based in France and Canada. Aside from Burt Lancaster, Susan Sarandon, and local extras, most of the cast originated from Canada or France. The film allowed Canadian actors such as Kate Reid and Al Waxman to successfully transition into American film and television roles.
 Six Degrees of Separation. Guare suggested that the story take place in Atlantic City, which was still for the most part suffering from the urban deterioration that prompted the legalization of gambling as a solution to save the city. The three met over dinner in early 1979 to work out quirks in the script and began shooting within a few months.
 Resorts and Caesars Atlantic City|Caesars; Ballys Park Place would open on December 30, toward the end of the principal photography). Most of the citys old resorts and entertainment piers were still standing, albeit in a severe state of disrepair. Within a couple of years of the filming, most of the these old hotels would fall victim to the wrecking ball as they were replaced with new casinos. To frame the picture, Malle foreshadows the great transition of the famous resort town in the opening credits by featuring footage of the implosion of the once-grand and historic Traymore Hotel on the Atlantic City Boardwalk.

Louis Malle hired composer Michel Legrand to write a score for the film, which he did. In the end, however, Malle decided against using a score for the film, and opted for all the music in the film to be ambient music|ambient: the only music used is that which exists in the world of the characters (i.e. radios, musical instruments, etc.).

===Filming locations===
The opening shot of the old Traymore Hotel being demolished is shown to convey the notion that the citys old hotels were being demolished to make way for the new casinos. However, the Traymore was in fact demolished in 1972,  years before the gambling referendum passed in New Jersey. The referendum passed in 1976 and the first hotel to open up was Resorts, formerly the Chafonte-Haddon Hall, in 1978.
 large model Lucy still stands in Margate and is on the National Register of Historic Places.

The club where Dave and Lou meet was the famed Club Harlem which opened in 1935 on Kentucky Avenue, and was the premier nightclub for black tourists visiting Atlantic City. The club would open and close frequently from 1968 on, and eventually closed for good by the end of the eighties. It was torn down in 1992.
Scenes were also shot in the Knife and Fork Restaurant, another famous Atlantic City landmark.

==Awards==
The film won the Golden Lion at the Venice Film Festival in 1980 in a tie with John Cassavetes Gloria (1980 film)|Gloria.
 Best Actor Best Actress Best Director, Best Picture Best Writing, Screenplay Written Directly for the Screen. In 2003, Atlantic City was selected for preservation in the United States National Film Registry by the Library of Congress as being "culturally, historically, or aesthetically significant".

American Film Institute Lists
*AFIs 100 Years...100 Movies - Nominated 
*AFIs 100 Years...100 Movie Quotes:
**"Yes, it used to be beautiful -- what with the rackets, whoring, guns." - Nominated 
*AFIs 100 Years...100 Movies (10th Anniversary Edition) - Nominated 
*AFIs 10 Top 10 - Nominated Gangster Film 

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 