Erskineville Kings
 
{{Infobox film
| name           = Erskineville Kings
| image          =
| image_size     =
| caption        =
| director       = Alan White
| producer       = Alan White Julio Caro
| writer         = Marty Denniss
| starring       = Hugh Jackman
| music          = Don Miller-Robinson Louis Tillett
| cinematography = John Swaffield
| editing        = Jane Moran
| studio         = Radical Media Palace Films Southern Star Entertainment Umbrella Entertainment
| released       = 1 January 1999
| runtime        = 90 minutes
| country        = Australia
| language       = English
| budget         =
| gross          = $183,691 
}} Australian drama film directed and produced by newcomer Alan White. The film was produced by Radical Media made for Palace Films on a minimal budget. It was released on 1 January 1999.   

The lead actor, Hugh Jackman, won the Film Critics Circle of Australia award for Best Male Actor,  for his performance as Wace, the older brother, who stayed home.

==Plot== Central station at dawn, seeking the whereabouts of his brother, Wace (Hugh Jackman). We learn from flashbacks that he left home two years ago to escape the clutches of his father’s violent rages. Wace, the older brother, is not too happy about Barky’s prolonged absence, having been left to manage looking after the father in his last years of life. After walking through the streets he finds an old mate of his, Wayne (Joel Edgerton), who assures him of the location of his brother. He succeeds in finding his brother through the help of Wayne and friends, who all end up at a pub where it is revealed that Barky and Waces mother left the family fifteen years earlier and that Wace hastened his fathers death after he was struck down by a stroke. Barky also crosses paths with his ex-girlfriend, Lanny, and manages to rekindle the relationship.

==Cast==
* Hugh Jackman as Wace
* Marty Denniss as Barky
* Joel Edgerton as Wayne
* Andrew Wholley as Coppa
* Leah Vandenberg as Lanny
* Aaron Blabey as Trunny
* Paul Dawber as The Father

==Production== Newtown and Erskineville, New South Wales|Erskineville, including inside Goulds Bookstore in Newtown. The title of the movie refers to the Kings Hotel, a fictional hotel in which most of the movie takes place.

==Box office==
Erskineville Kings grossed $183,691 at the box office in Australia. 

==Reception==
As of May 2012, Erskineville Kings does not have any critics reviews catalogued on the film review aggregator Rotten Tomatoes. 

==See also==
* Cinema of Australia

==References==
 

==External links==
*  

 
 
 
 
 
 

 
 