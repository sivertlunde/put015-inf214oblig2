The Tale of the Wonderful Potato
{{multiple issues|
 
 
}}

{{Infobox film
| name           = The Tale of the Wonderful Potato
| image          = File:Vidunderlige Kartoffel- DVD cover.png
| image_size     = 
| border         = 
| alt            = 
| caption        = DVD Cover
| film name      = 
| director       = Anders Sørensen
| producer       = 
| writer         = Anders Sørensen
| screenplay     = 
| story          = 
| based on       =  
| narrator       = Kai Løvring
| starring       = 
| music          = 
| cinematography = 
| editing        = Jette Michaelsen
| studio         = 
| distributor    = 
| released       =  
| runtime        = 22 minutes
| country        = Denmark
| language       = Danish
| budget         = 
| gross          = 
}}
The Tale of the Wonderful Potato ( ) is a 1985 animated film by Anders Sørensen. Sponsored by the Danish Film Institute, it tells the history of the potato through the ages—with a focus on European history and a twinkle in its eye. Potatos humorous and slightly self-deprecating presentation belies the detailed and insightful understanding of human history that carries through from the Incan potato creation myth, to the feisty tubers heighday in 18th-century haute cuisine.

==Educational value==
 
 

===Appropriate audience===
A mere 24 minutes long, The Tale of the Wonderful Potato is a schoolroom favorite in Europe, and has won the acclaim of teachers organizations worldwide.  It has been translated (dubbed) into English, German and Dutch, and is recommended for grades 5 through 8 due to the versatile nature of its presentation, coupled with a detailed narrative voice.

===Topics===
Topics covered include:
* History of the potato (South America and Europe)
* World leaders involved in the history of the potato
* Social climate during the 16th to 18th centuries
* Major world events in the 16th to 18th centuries
* Nutrition, storage and preparation of potatoes

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 