Tora-san's Matchmaker
{{Infobox film
| name = Tora-sans Matchmaker
| image = Tora-sans Marriage Proposal.jpg
| caption = Theatrical poster
| director = Yoji Yamada
| producer = Tomiyuki Maruyama Kiyoshi Shimizu
| writer = Yoji Yamada Yoshitaka Asama
| starring = Kiyoshi Atsumi Keiko Matsuzaka
| music = Naozumi Yamamoto
| cinematography = Hideyuki Iketani
| editing = Iwao Ishii
| distributor = Shochiku
| released =  
| runtime = 103 minutes
| country = Japan
| language = Japanese
| budget = 
| gross = 
}}

  is a 1993 Japanese comedy film directed by Yoji Yamada. It stars Kiyoshi Atsumi as Torajirō Kuruma (Tora-san), and Keiko Matsuzaka as his love interest or "Madonna".  Tora-sans Matchmaker is the forty-sixth entry in the popular, long-running Otoko wa Tsurai yo series.

==Cast==
* Kiyoshi Atsumi as Torajirō 
* Chieko Baisho as Sakura
* Keiko Matsuzaka as Yoko Sakaide
* Hidetaka Yoshioka as Mitsuo Suwa
* Shimojo Masami as Kuruma Tatsuzō
* Chieko Misaki as Tsune Kuruma (Torajirōs aunt)
* Hisao Dazai as Boss (Umetarō Katsura)
* Gajirō Satō as Genkō
* Keiroku Seki as Ponshū

==Critical appraisal== Japan Academy Prize awarded Yoji Yamada for Best Director and Screenplay, Yoshitaka Asama for Best Screenplay, and Isao Suzuki for Best Sound. Also nominated at the Japan Academy Prize were Yutaka Yokoyama and Mitsuo Degawa for Best Sound and Iwao Ishii for Best Editing.  The German-language site molodezhnaja gives Tora-sans Matchmaker three and a half out of five stars.   

==Availability==
Tora-sans Matchmaker was released theatrically on December 25, 1993.  In Japan, the film has been released on videotape in 1994 and 1996, and in DVD format in 2000 and 2008. 

==References==
 

==Bibliography==
===English===
*  

===German===
*  

===Japanese===
*  
*  
*  
*  

==External links==
*   at www.tora-san.jp (Official site)

 
 

 
 
 
 
 
 
 
 
 

 