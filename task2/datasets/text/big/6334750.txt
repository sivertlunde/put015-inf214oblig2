Abraham Lincoln's Clemency
{{Infobox film
| name           = Abraham Lincolns Clemency
| image          = Abraham Lincolns Clemency.jpg
| caption        = Theatrical poster to Abraham Lincolns Clemency
| director       = Theodore Wharton
| producer       =
| writer         =
| starring       =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = 1 reel 
| country        = United States
| language       = English
| budget         =
}}

Abraham Lincolns Clemency was a 1910 American film directed by Theodore Wharton and produced by Pathé Films.  The plot revolves around US President Abraham Lincoln pardoning a hapless sentry who had fallen asleep while on duty during the height of the Civil War.  Due to the soldiers incompetence he is due to face the firing squad.  However, his mother pleads with the President to save her son, which, as the title suggests, he does.   The film was a single reel in length.

==See also==
* List of American films of 1910

==References==
 
* 

== External links ==
* 

 
 
 
 
 
 
 


 