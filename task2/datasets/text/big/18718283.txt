Mortimer Griffin and Shalinsky
 
{{Infobox Film
| name           = Mortimer Griffin and Shalinsky
| image          = Shalinsky screenshot.jpg
| image_size     =
| caption        = 
| director       = Mort Ransen
| producer       = 
| writer         = Mort Ransen and Mordecai Richler
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        =
| distributor    =
| released       = 1985 
| runtime        = 24 min.
| country        = Canada English
| budget         = 
| preceded_by    = 
| followed_by    = 
}} eccentric Jewish intellectual that accuses his teacher of hiding his Jewish identity. The film is based on Richlers 1978 short story, "Mortimer Griffin, Shalinsky and How they Settled the Jewish Question".

==Summary==
Shalinsky, a Jewish university student, is convinced that his non-Jewish professor Mortimer Griffin is guilty of changing his name, suspecting the instructor of hiding his Jewish identity.

After listening attentively to Griffin’s analysis of Kafka’s The Metamorphosis, all the other students file out of the classroom, but Shalinsky lingers to compliment his teacher. After offering words of praise, between puffs of his cigarette, he asks pointedly, “Why did you change your name? You’re a Jew.”

Of course, Griffin is not Jewish and he tells Shalinsky this fact. Griffin is amused at first, and tells his Jewish friends about the incident.  However, Shalinsky persists, and simply wont accept that Griffin is not Jewish.  Eventually, Griffin gets more and more irritated by Shalinskys refusal to accept the truth.  Finally, irritation gives way to anger and Griffin blurts out comments that sound anti-Semitic. Griffin has a nervous breakdown.

In the satirical aftermath, we learn that Griffin has converted to Judaism and become religious.

==Reception==

 

==See also==
Other Jewish comedic-dramas:

*The Witch from Melchet Street
*Autumn Sun
*Saint Clara
*You Dont Say
*Alan and Naomi
*Left Luggage

==References==
*{{cite web
  | last =
  | first =
  | authorlink =
  | coauthors =
  | title =Mortimer Griffin and Shalinsky (1985)
  | work =
  | publisher =IMDB
  | year = 2008
  | url =http://www.imdb.com/title/tt0227079/
  | format =
  | doi =
  | accessdate = 4 August}}

*{{cite web
  | last = 
  | first =
  | authorlink =
  | coauthors =
  | title =Mortimer Griffin and Shalinsky
  | work =
  | publisher =The Jewish Channel
  | year = 2008
  | url =http://www.tjctv.com/movies/mortimer-griffin-and-shalinsky/
  | format =
  | doi =
  | accessdate = 4 August}}

==External links==
* 
* 
*  

 
 
 
 
 
 
 