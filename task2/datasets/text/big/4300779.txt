The Formula (2002 film)
 
{{Infobox film
| name        = The Formula
| image       =
| director    = Chris Hanel Stephen Phelan
| writer      = Stephen Phelan Chris Hanel
| starring    = Chris Hanel Abe Peterka Justin Whitlock Rebecca Peterka
| distributor = TheForce.net
| released    =  
| runtime     = 53 min.
| language    = English
| budget      = 2,000USD
}}

The Formula is a fan film that premiered online in 2002. Made in Iowa by a group of friends over 18 months, the movie documents a Star Wars fans attempt to make an epic fan film of his own, only to become disillusioned by the process and turn on his friends. The movie centers on the motivations of fan filmmakers, and proved popular for encouraging other would-be filmmakers to not give up their own projects and enjoy the process. 

The Formula has been featured as a leading representative of the fanfilm genre, with print references in   and  . It eventually screened theatrically, showing as an official selection at the 2003  . Film Threat gave it a five-star (out of five)  , calling it "a great surprise to those seeking out a good Star Wars fanfilm thats not made up exclusively of angst-filled Jedi, stormtroopers, over the top villains, or whatever the norm is nowadays.".

The Formula was the first film for directors Chris Hanel and Stephen Phelan, and both have gone on to filmmaking careers.  Besides working as a video game designer, Chris Hanel also co-wrote and served as the VFX Supervisor for the series Return of Pink Five.

The film is also notable because it was the first visual effects collaboration of Ryan Wieber and Michael Scott, who went on to create the popular lightsaber fight Ryan vs. Dorkman.

==Plot== High Fidelity, with Star Wars in the place of rock music, Tom reflects back a few months to the genesis of the fanfilm project.

Tom, Greg, Jenny, and Zarth are seen hanging out at Excalibur Games, a hobby shop, playing the  . Tom is revealed to be an employee at the store, working for store credit while he has his money saved up for film school. He claims the only downside to the job is the trekkies--specifically, two irate, Star Wars-hating ones named James (James Kropa) and Stewart (Michael Mulherin). The two enter the store, arguing over a discrepancy between the film Star Trek Generations and the Technical Manual. While waiting for Tom to get their new Star Trek cards, Stewart accidentally spills soda on a set of comics that Gregs boss ordered. Tom demands they pay for the damaged goods, and the Trekkies overpay him before leaving the store in a rush. Tom realizes that they can sell the undamaged books to Gregs boss for full price and pocket the money from the Trekkies. At Gregs suggestion, Tom combines the money with his remaining store credit and buys model lightsabers, which prompts the idea in his mind of making a Star Wars fanfilm.

Tom, Greg, Jenny, and Zarth sit around discussing ideas for a plot for their fanfilm, deciding against a story about a mischievous not-good-but-not-evil Jedi, a story about a post-Return of the Jedi Boba Fett (sitting on his ass watching Jerry Springer all day), a story about all the sith in the galaxy competing for the master-and-apprentice positions, and a rock musical. Despite Toms protests, everybody resigns to simply doing a lightsaber duel film. Tom approaches the reluctant Trekkies to do the visual effects, offering them ten percent of his store credit (which was all already spent on the lightsabers). Tom ends up spending all his money on preproduction for the film, and finds himself no longer able to afford film school. He has a nightmare about his film being terrible, with poorly-rotoscoped lightsabers and a soundtrack by Meco.

Back in the present, Zarth tells Tom he needs to "pull his head out". An irate Tom grabs a prop lightsaber, and the two enter a The Matrix|Matrix-inspired lightsaber duel fantasy sequence. When Zarth beats Tom, he asks him, "Do you think thats a real lightsaber youre holding?" Tom realizes hes been taking himself and the film too seriously, and has lost any sense of fun.

On their way back to Excalibur Games, Tom and Zarth are stopped by  , until Zarth interrupts and points out that the gag has already been done--in  . Back at Excalibur, the gang shows Greedo the scene in question. He flips out, complaining that a lightsaber fight is so overdone, but one Greedo scene is done before and suddenly its off-limits. Jenny explains that a lightsaber duel is a formula, and can be personalized, but they cant add anything to a Greedo scene, because "in the end, you always wind up dead," which Greg then proves by shooting Greedo in the head.

Later that night, the gang is eating pizza, and Tom apologizes to the others for his behavior. When it is revealed that Tom had dressed up as Darth Maul for the midnight showing of The Phantom Menace, Tom recounts the story in a soliloquy reminiscent of Jaws (film)|Jaws. Tom then reveals how much he hated the movie, and how mad he was at George Lucas. Later, when he met his friends, he realized that no movie would have met his expectations. Now, he just wants to finish his fanfilm.

The gang finishes shooting the duel, and holds the premiere in the store. Everybody is excited watching the film. After everybody else goes home, Tom reflects on the point of making a Star Wars fanfilm--to have fun. Your film may not be perfect, but you shouldnt care what others think, because in the end, "its only a movie."

==External links==
*   at TheForce.net
* 
*   at Film Threat
*   at The Fan Film Menace

 

 
 
 
 
 
 
 
 