3 Bad Men
 
{{Infobox film
| name           = 3 Bad Men
| image          = 3_bad_men_poster.jpg
| caption        = Theatrical release poster
| director       = John Ford
| producer       = John Ford John Stone Malcolm Stuart Boylan Ralph Spence George OBrien Olive Borden
| cinematography = George Schneiderman
| editing        = 
| studio         =  Fox Film Corporation
| released       =  
| runtime        = 92 minutes
| country        = United States 
| language       = Silent with English intertitles
}}
 Western film directed by John Ford.    Bob Mastrangelo has called it "One of John Fords greatest silent epics." 

==Cast== George OBrien as Dan OMalley
* Olive Borden as Lee Carlton
* Lou Tellegen as Sheriff Layne Hunter
* Tom Santschi as "Bull" Stanley
* J. Farrell MacDonald as Mike Costigan
* Frank Campeau as "Spade" Allen
* Priscilla Bonner as Millie Stanley
* Otis Harlan as Editor Zach Little
* Phyllis Haver as Lily (prairie beauty)
* Georgie Harris as Joe Minsk
* Alec B. Francis as Rev. Calvin Benson (as Alec Francis) Jay Hunt as Nat Lucas (old prospector)
* Grace Gordon as Millies pal (uncredited) George Irving as Gen. Neville (uncredited)
* Bud Osborne as Hunters henchman (uncredited)
* Vester Pegg as Henchman shooting Lucas (uncredited)
* Walter Perry as Pat Monahan (uncredited)

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 

 
 