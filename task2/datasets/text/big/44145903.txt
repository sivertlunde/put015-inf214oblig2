Muthodumuthu
{{Infobox film
| name           = Muthodumuthu
| image          =
| caption        =
| director       = M. Mani
| producer       = M Mani
| writer         = John Alunkal Thoppil Bhasi (dialogues)
| screenplay     = Thoppil Bhasi Shankar Menaka Menaka
| Shyam
| cinematography = CE Babu
| editing        = VP Krishnan
| studio         = Sunitha Productions
| distributor    = Sunitha Productions
| released       =  
| country        = India Malayalam
}}
 1984 Cinema Indian Malayalam Malayalam film, Shankar and Menaka in lead roles. The film had musical score by Shyam (composer)|Shyam.   

==Cast==
*Sukumari
*Adoor Bhasi Shankar
*Menaka Menaka
*Prathapachandran Baby Shalini
*Sabitha Anand
*Sreenath Sumithra

==Soundtrack== Shyam and lyrics was written by Chunakkara Ramankutty. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Dhanumaasakkaate vaayo || K. J. Yesudas || Chunakkara Ramankutty || 
|-
| 2 || Kannil ne thenmalaraay || K. J. Yesudas, S Janaki || Chunakkara Ramankutty || 
|}

==References==
 

==External links==
*  

 
 
 

 