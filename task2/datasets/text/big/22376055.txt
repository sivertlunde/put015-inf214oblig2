Die Pratermizzi
{{Infobox film
| name           = Die Pratermizzi
| image          = 
| image_size     = 
| caption        = 
| director       = Gustav Ucicky
| producer       = Alexander Kolowrat
| writer         = Walter Reisch
| narrator       = 
| starring       = Anny Ondra Igo Sym Nita Naldi
| music          = 
| cinematography = Gustav Ucicky   Eduard von Borsody
| editing        = 
| distributor    = Sascha-Film
| released       = January 1927
| runtime        = 50 minutes
| country        =   
| language       =  
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 Austrian silent silent drama film directed by Gustav Ucicky in 1926, released in January 1927, and starring Anny Ondra, Igo Sym and Nita Naldi. The film was long believed lost until its rediscovery in 2005. The films art direction was by Artur Berger and Emil Stepanek.

==Plot== tunnel of love Zum Walfisch  on the Prater in Vienna, and Baron Christian von B. fall in love, but their relationship is disrupted by the wilful involvement of the dancer Valette, who always wears a mask. Christian eventually follows Valette to Paris. When he tears the golden mask from her face he is shocked to discover that she is disfigured by a disease. He returns to Vienna with the intention of putting an end to his life, but at the last minute Marie is able to save him.

The ride through the tunnel of love is associated in this film with the journey into ones own self.

==Cast==
* Igo Sym: Freiherr Christian von B.
* Anny Ondra: Marie
* Nita Naldi: Valette, the dancer with the mask
* Hedy Pfundmayr: the dancers double
* Carl Götz: Herr von Z.
* Ferdinand Leopoldi: Adam Lorenz Stingl, owner of the tunnel of love
* Hugo Thimig: Matthias Veitschberger

This was the last major film role of Nita Naldi, whose career did not survive the advent of the talkies.

==History of the film==
In 2005 a print of the Pratermizzi on a base of the inflammable cellulose nitrate was discovered in the archives of the Centre national de la cinématographie. It was successfully copied and restored in time to be shown at the opening of Prater Film Festival the same year.

Excerpts from the film were published by the Filmarchiv Austria on the DVD "Der Wiener Prater im Film" in July 2005. 

==References==
 

==Sources and external links==
*  
*    
*    

 

 
 
 
 
 
 
 
 
 
 
 
 