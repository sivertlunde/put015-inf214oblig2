Ek Var Piyu Ne Malva Aavje
{{Multiple issues
| 
 
 
}}
{{Infobox film
| name           = Ek Var Piyu Ne Malva Aavje
| image          = Ek_Var_Piyu_Ne_Malva_Aavje.jpg
| caption        = Theatrical release poster
| director       = Harsukh Patel
| producer       = Gunvant Thakor Ramesh Patel
| story          = Dhiren Randheja
| screenplay     = Mukesh Malavankar
| writer         = Gunvant Thakor
| starring       = Hiten Kumar Vikram Thakor Firoz Irani Minaxi Mamta Soni Jayendra Mehta Bhaskar Nayak Jaimini Trivedi Mayur Vankani
| music          = Appu
| studio         = Jay Shree Mahaveer
| released       =  
| country        = India
| runtime        = 
| language       = Gujarati
}}
Ek Var Piyu Ne Malva Aavje ( ) is a 2006 Dhollywood romantic action film produced by Gunvant Thakor, Ramesh Patel, directed by Harsukh Patel, starring Hiten Kumar, Vikram Thakor, Firoz Irani, Minaxi, Mamata Soni, Jayendra Maheta, Jaimini Trivedi and Mayur Vankani in the lead roles, released in Gujarati cinema in 2006.

== Plot ==
This romantic film is about of Vikram and Radha. Though the pair are in love, Radhas father (Vajesang Thakor) does not like Vikram because he is a poor stage programmer (singer), while Vajesang Thakor is rich.So, Vajesang Thakor thinks that Vikram is not a boy who can married with his daughter Radha.When Vikram listen that he is a not a perfect man to marrie Radha he loss his confidence.At that time his big brother Suraj gave confidence to him, and finally Vikram get Radha.

==Cast==
* Hiten Kumar as Suraj
* Vikram Thakor as Vikram
* Firoj Irani as Roopsinh
* Minaxi as Tejal
* Mamata Soni as Radha
* Jayendra Maheta as Vajesang Thakor
* Jaimini Trivedi as Kadavi Foi
* Bhaskar Nayak as Mangalsinh
* Mayur Vakani as Popat

== Soundtrack ==
{{Infobox album
| Name       = Ek Var Piyu Ne Malva Aavje
| Type       = soundtrack
| Length     = 54:11
| Artist     = Vikram Thakor and group
| Cover      =
| Released   = 1 October 2006
| Genre      = Film soundtrack	
| Label      = Jay Shree Mahaveer Audio
| Producer   = Gunvant Thakor, Ramesh Patel
| Last album = Bewafa Sanedo (2006)
| This album = Ek Var Piyu Ne Malva Aavje  (2006)
| Next album = Paradeshi Aashique (2006)
}}
The music of the film is by Appu and all the songs were composed and written by Gunvant Thakor.Gunvant Thakor wrote many songs before his first movie Ek Var Piyu Ne Malva Aavje.  The music album was released by Jay Shree Mahaveer Audio.

{{track listing
| headline        = Tracklist
| extra_column    = Artist(s)
| title1          = Ek Var Piyu Ne Malva Aavje
| note1           = Happy version
| extra1          = Vikram Thakor, Dipali Saumaiya
| length1         = 4:55
| title2          = Hu Rangilo Hu Mojilo
| extra2          = Vikram Thakor, Nayan Rathod
| length2         = 4:56
| title3          = Gori Taro Latakore
| extra3          = Arvind Barot, Dipali Saumaiya
| length3         = 4:58
| title4          = Kadaju Kapi Jivado Kadhu
| extra4          = Vikram Thakor, Dipali Saumaiya
| length4         = 8:06
| title5          = Ek Var Piyu Ne Malva Aavje
| note5          = Sad version
| extra5          = Vikram Thakor
| length5         = 5:20
| title6          = Prem Gori Taro Kem Kari Bhulay
| extra6          = Vikram Thakor
| length6         = 3:52
| title7          = Khili Khili Chandarmani Raat
| extra7          = Vikram Thakor, Shilpa Thakor
| length7         = 2:20
| title8          = Rajwadi Chhiye Ame Maan Bher Rahiye
| extra8          = Vikram Thakor
| length8         = 2:43
| title9          = Doshino Sanedo
| extra9          = Vikram Thakor
| length9         = 2:27
| title10         = Gori Taro Latakore
| extra10         = Nayan Rathod, Dipali Saumaiya
| length10        = 5:00
| title11         = Sajani Morire…
| extra11         = Vikram Thakor, Dipali Saumaiya
| length11        = 5:49
| title12         = Ek Var Piyu Ne Malva Aavje
| note12        = Instrumental
| length12        = 3:45
}}

== References ==
 

==External links==
*  
* 

 
 


 