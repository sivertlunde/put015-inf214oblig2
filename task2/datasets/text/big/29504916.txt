Grabbers
 
 
{{Infobox film
| name           = Grabbers
| image          = Grabbers.jpg
| alt            =  
| caption        = Theatrical film poster
| director       = Jon Wright |
| producer       = Tracy Brimm Kate Myers Martina Niland
| writer         = Kevin Lehane |
| starring       = Richard Coyle Ruth Bradley Russell Tovey
| music          = Christian Henson
| cinematography = Trevor Forrest
| editing        = Matt Platts-Mills
| studio         = Forward Films UK Film Council Irish Film Board Limelight High Treason Nvizible  Samson Films
| distributor    = IFC Midnight   Sony Pictures (UK) Ascot Entertainment (Germany)   Element Pictures (Ireland) Monster Pictures (Aus/Nz)
| released       =  
| runtime        = 94 minutes   
| country        = {{plainlist|
*Ireland
*United Kingdom }}
| language       = English
| budget         = £3.5 million
| gross          =
}} monster film directed by Jon Wright and written by Kevin Lehane. The film stars Richard Coyle, Ruth Bradley, Bronagh Gallagher and Russell Tovey among an ensemble cast of Irish actors. 

==Plot==
 Garda Ciarán OShea (Richard Coyle), an alcoholic, initially resents his new partner, Garda Lisa Nolan (Ruth Bradley), a workaholic seeking to impress her superiors by volunteering for temporary duty in a remote Irish island. After discovering mutilated whale corpses, the quiet community slowly comes to realise that theyre under attack by enormous bloodsucking tentacled creatures from the sea, dubbing them "Grabbers". When Paddy (Lalor Roddy), the town drunk, inexplicably survives an attack, the local marine ecologist, Dr. Smith (Russel Tovey), theorizes that his high blood alcohol content proved toxic to the Grabbers, who survive on blood and water. OShea contacts the mainland, but an oncoming storm prevents any escape or help. The group also realizes the rain will allow the remaining large male Grabber to move about the island freely. Seeking to keep calm in the town, Nolan and OShea organize a party at the local pub, intending to keep the islands residents safe but unaware of the danger. Initially hesitant to join in a celebration when no good reason can be offered, the people enthusiastically agree when Brian Maher (David Pearse), the pub owner, offers free drinks. OShea volunteers to stay sober so that he can coordinate the towns defenses, and everyone else becomes drunk.

In a drunken stupor, Nolan reveals that she has come to the island to escape the shadow of her more-favoured sister. When they are alone in a squad car, Nolan confesses to OShea that she has feelings for him despite turning down his advances earlier. Smith wanders outside the pub and tries to get a picture of the beast, reasoning that his inebriated state will protect him from being eaten. Instead, the monster throws him into the air and kills him. Nolan and OShea escape to the pub, where they try to protect the townspeople. Nolan drunkenly reveals the danger they are in while trying to reassure everyone that nothing is trying to kill them. Panicked, they retreat to the second level of the pub, and baby sea monsters take over the first floor. Nolan accidentally sets the pub on fire while trying to sneak out, but she and OShea manage to draw the attention of the adult.

OShea and Nolan drive to a construction site, and the monster follows them. There, they hope to strand the monster on dry land, as it needs water to survive. Before they can successfully set a trap, the monster arrives and attacks OShea. Although wounded, OShea survives the attack, and Nolan uses the heavy construction equipment to mount a counter-attack, pinning it at the base of a pit. The monster grabs OShea, but before it can eat him he dumps a bottle of Paddys moonshine into its mouth, sickening it and causing it to release OShea. Nolan then ignites nearby explosives with a flare gun, killing the Grabber. As the storm clears up, they return to the town and OShea throws away his flask. The film ends with a shot of more sea monster eggs hatching.

==Cast==
*Richard Coyle as Garda Ciarán OShea
*Ruth Bradley as Garda Lisa Nolan
*Russell Tovey as Dr Smith
*Lalor Roddy as Paddy
*David Pearse as Brian Maher
*Bronagh Gallagher as Una Maher
*Pascal Scott as Dr Jim Gleeson
*Ned Dennehy as Declan Cooney
*Clelia Murphy as Irene Murphy
*Louis Dempsey as Tadhg Murphy Stuart Graham as Skipper
*Micheál Ó Gruagáin as Father Potts

==Release==
The film premièred at the 2012 Sundance Film Festival  and played at the Edinburgh International Film Festival in June 2012.   The film continued its festival run across the world screening at Karlovy Vary International Film Festival, Taormina Film Fest, Fantasia Film Festival, PIFAN, Sitges, Toronto After Dark Film Festival, Strasbourg European Fantastic Film Festival, London FrightFest Film Festival and held its Irish première in July 2012 as the opening film of the 24th Galway Film Fleadh. 

==Reception==
 s smart script is canny enough to sidestep the expectations of fans who might think they know how films like this are supposed to play out, while the monsters are as well-realised as anything in far more costly productions."     of The Guardian rated the film 3/5 stars and described it as "a likable and technically impressive comedy-horror" that is "fantastically silly, often funny".   Sam Adams of The A.V. Club rated the film B- and criticized the films climax as "a letdown" and "cheap imitation" compared to the first halfs "sharp-edged parody".   Marc Mohan of The Oregonian rated it C- and called it "a one-dimensional, one-joke film."    In a negative review for Variety (magazine)|Variety, Dennis Harvey called the film polished and watchable, but criticized the writing as "pretty tepid, middlebrow stuff".   Nigel Andrews of the Financial Times rated it 1/5 stars and said, "For a horror comedy it needed some comedy and some horror." 

===Awards===
At the Edinburgh International Film Festival, it was announced as one of the "Best of the Fest" of the 2012 line-up.  At the Strasbourg European Fantastic Film Festival, it won the Audience Prize for Best Film, and at Neuchâtel International Fantastic Film Festival|NIFFF, it won two awards: the Audience Award for best film and the Titra Film Award. 
 IFTAs at the 2013 Irish Film and Television Awards. Bronagh Gallagher for Best Supporting Actress, Kevin Lehane for Best Feature Script, producers David Collins and Martina Niland of Samson Films, alongside Forward Films and High Treason Productions were nominated for Best Film and Ruth Bradley was nominated and won for Best Actress. 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 