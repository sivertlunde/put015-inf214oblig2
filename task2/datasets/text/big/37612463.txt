Terminal Bliss
{{Infobox film
| name     = Terminal Bliss
| image    =
| caption  =
| writer   = Jordan Alan
| starring =
| director = Jordan Alan
| producer = Brian Cox
| music    = Frank Becker
| released =  
| runtime  = 79 minutes
| country  = United States
| language = English
}}

Terminal Bliss is a 1992 film starring Luke Perry. 

==Plot==
Two adolescent children of wealthy parents deal with the emotional travails of spoiled youth by indulging in self-destructive behavior including but not limited to drugs, parties, and teen sex. Friends John (Luke Perry) and Alex (Timothy Owen) deal with issues of betrayal involving Alexs girlfriend Stevie (Estee Chandler).

==Cast==
*Luke Perry as John Hunter
*Estee Chandler  as Stevie Bradley  
*Sonia Curtis as Kirsten Davis  
*Micah Grant as Bucky OConnell  
*Alexis Arquette as Craig Murphy

==Reception==
The movie was poorly received by critics.  

It debuted at number 17 at the domestic box office. 

== References ==
 

==External links==
* 

 
 
 
 