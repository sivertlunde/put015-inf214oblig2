L'enfant Roi
{{multiple issues|
 
 
}}

{{Infobox film
| name           = Lenfant Roi 
| image          = 
| caption        = 
| director       = Mohamed Houssine Grayaâ
| producer       = Audimage, 5 dimensions, La Méditerranéenne
| writer         = 
| starring       = 
| distributor    = 
| released       =  
| runtime        = 30 minutes
| country        = Tunisia
| language       = 
| budget         = 
| gross          = 
| screenplay     = Souleyman Djigo Diop, Ameur Mathlouthi
| cinematography = 
| sound          = Kaïs Ben Mabrouk
| editing        = Julien Hecker
| music          = Kaïs Ben Mabrouk
}}

Lenfant Roi  is a 2009 film.

== Synopsis ==
Once upon a time on the African continent, there was a village called Nidiobina that was ravaged by flames. And Foudi Mamaya and his wife had no choice but to leave their village in search of food, leaving behind their only son, Bouba, with his grandfather Wali. The old man teaches the boy how to become a man, and Bouba turns out to be a gifted pupil.

== Awards ==
* Jornadas del Cine Europeo de Túnez 2008

== References ==
 

 
 
 


 