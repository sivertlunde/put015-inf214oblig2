Aaynate
 
{{Infobox film
| name           = Aaynate
| image          = Aaynate.jpg
| image_size     =
| caption        = The poster for the film
| director       = Dulal Dey
| producer       =
| writer         =
| narrator       =
| starring       = Rituparna Sengupta  Firdous Ahmed  Rati Agnihotri
| music          =
| cinematography =
| editing        =
| distributor    =
| released       = 28 November 2008
| runtime        =
| country        = India Bengali
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}
 Bengali drama Bengali cinema. 
{{cite news
|url=http://www.telegraphindia.com/1080606/jsp/entertainment/story_9370390.jsp
|title=Aaynate
|publisher=www.telegraphindia.com
|accessdate=2008-11-22
|last=Nag
|first=Kushali
|location=Calcutta, India
|date=6 June 2008
}}
  
{{cite news
|url=http://www.telegraphindia.com/1080514/jsp/entertainment/story_9266497.jsp
|title=Aaynate
|publisher=www.telegraphindia.com
|accessdate=2008-11-22
|last=Nag
|first=Kushali
|location=Calcutta, India
|date=14 May 2008
}}
  
{{cite web
|url=http://calcuttatube.com/2008/08/18/aaynate-rituparna-sengupta/
|title=Aaynate
|publisher=calcuttatube.com
|accessdate=2008-11-22
|last=
|first=
| archiveurl= http://web.archive.org/web/20081203145229/http://calcuttatube.com/2008/08/18/aaynate-rituparna-sengupta/| archivedate= 3 December 2008  | deadurl= no}}  
  
{{cite news
|url=http://www.telegraphindia.com/1081129/jsp/entertainment/story_10178998.jsp
|title=Mirror cracked
|publisher=www.telegraphindia.com
|accessdate=2008-11-29
|last=Das
|first=Madhuparna
|location=Calcutta, India
|date=29 November 2008
| archiveurl= http://web.archive.org/web/20081205061112/http://www.telegraphindia.com/1081129/jsp/entertainment/story_10178998.jsp| archivedate= 5 December 2008  | deadurl= no}}
 

==Plot==
The story about Malini (Rituparna Sengupta) who recapitulating her childhood memories while recollecting her family attachments she remembers the detachment from her inner self grounded by the molestation in her teenage.Malini is now a well-versed photographer working in some magazine. Her photographic skills explores through the various special expressions of the people when they are dead and seeks to find out the emotions of the pale and white faces. During these ongoing course of actions she comes in contact with Urmila Sanyal (Rati Agnihotri), the leading social activist who has dedicated her life to the service of the exploited and distressed women of the society. The story then takes a turn to her romantic journey with her boyfriend, Rajat (Firdous Ahmed) who is the lavish brat of a big businessman.On one hand there is the family expectations tied to her and on the other there is urge for preserving her self-identity. Though she gets romantically involved with Rajat along with her family support but then nobody is ready to bear its outcomes. It is then when she becomes pregnant, she realizes the real faces of the people whom she has been respecting and admiring so much. In a moment she is being out cast and constantly pressurized to surrender to the patriarchal will. But revolting against it she comes to discover the illegitimacy of her own birth and finds Urmila Sanyal as her escapist mother, who was scared of the social accusations.
<!-- ==Cast==
==Crew==
* Producer(s):
* Director:
*Story:
*Production Design:
*Dialogue:
*Lyrics:
*Editing :
*Cinematography:
*Singer:
==Music==

 
==Critical reception==
==Awards== -->

==References==
 

==External links==
* 
* 

 
 
 


 