Money Talks (1926 film)
{{Infobox film
| name           = Money Talks
| image          =
| caption        =
| director       = Archie Mayo
| producer       =
| writer         = Jessie Burns and Bernard Vorhaus, based on the story by Rupert Hughes
| narrator       =
| starring       = Claire Windsor Bert Roach Owen Moore Ned Sparks
| music          =
| cinematography = William H. Daniels
| editing        =
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 70 minutes
| country        = United States Silent English English intertitles
| budget         =
}}
 1926 comedy film directed by Archie Mayo. The film stars Claire Windsor, Bert Roach, Owen Moore and Ned Sparks. It is written by Jessie Burns and Bernard Vorhaus, based on the story by Rupert Hughes. The film is considered partially lost. 

==Plot==
Sam Starling (Owen Moore) is deep in debt, his wife Phoebe (Claire Windsor) is leaving him and still he is confident. When Phoebe boards a luxury yacht and is wooed by the captain, Sam comes aboard as a woman and tries to seduce the captain (in fact, a liquor smuggler), away from his wife.

==Cast==
* Claire Windsor - Phoebe Starling 
* Owen Moore - Sam Starling 
* Bert Roach - Oscar Waters 
* Ned Sparks - Lucius Fenton 
* Phillips Smalley - J.B. Perkins 
* Dot Farley - Mrs. Chatterton 
* Kathleen Key - Vamp 
* George Kuwa - Ah Foo

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 


 