The Three Caballeros
{{Infobox film
| name           = The Three Caballeros
| image          = Three caballeros poster.png
| caption        = Original theatrical release poster Norman Ferguson (supervising director), Clyde Geronimi, Jack Kinney, Bill Roberts, Harold Young (sequence directors)
| producer       = Walt Disney Roy Williams William Cottrell Del Connell James Bodrero
| starring       = Clarence Nash José Oliveira Joaquin Garay Walt Disney Productions RKO Radio Pictures
| released       =  
| runtime        = 72 minutes Paul J. Smith Charles Wolcott
| country        = United States
| language       = English Spanish Portuguese
}} Walt Disney Walt Disney Animated Classics series, the film plots an adventure through parts of Latin America, combining live-action and traditional animation|animation. This is the second of the six package films released by Walt Disney Animation Studios in the 1940s.

The film is plotted as a series of self-contained segments, strung together by the device of Donald Duck opening birthday gifts from his Latin American friends. Several Latin American stars of the period appear, including singers Aurora Miranda (sister of Carmen Miranda) and Dora Luz, as well as singer and dancer Carmen Molina.
 a severe case of white specks and excess grain.)
 Never a Dull Moment.

== Film segments ==

 
 film projector, which shows him a documentary about birds. During the documentary, he learns about the Aracuan Bird|Aracuan Bird, who received his name because of his eccentric song. The Aracuan also makes several appearances throughout the film.
 pining for one girl, but fails. After the journey, Donald and José leave the book.

Upon returning, Donald realizes that he is too small to open his third present. José shows Donald how to use magic to return himself to the proper size. After opening the present, he meets Panchito, a native of Mexico. The trio take the name "The Three Caballeros" and have a short celebration. Panchito then presents Donalds next present, a piñata. Panchito tells Donald of the tradition behind the piñata. José and Panchito then blindfold Donald, and have him attempt to break open the piñata, which eventually reveals many surprises. Donald ends the celebration by being fired away by firecrackers in the shape of a ferocious toy bull (with which the firecrackers are lit by José with his cigar).

Throughout the film, the Aracuan Bird appears at random moments. He usually taunts everyone with his madcap antics sometimes stealing Josés cigar trying to make José jealous. His most famous gag is when he re-routes a train that Donald and José ride on by drawing new tracks, making the train disassemble. He returns three or four years later in the 1948 Disney film Melody Time.

The film consists of seven segments:

=== The Cold-Blooded Penguin ===
This segment involves a penguin named Pablo, told by Sterling Holloway, reproducing images of the penguins of Punta Tombo in Argentina along the coast of Patagonia, "Pablo the penguin" is shown to be so fed up with the freezing conditions of the South Pole that he would much rather leave for warmer climates.

=== The Flying Gauchito ===
This segment involves the adventures of a little boy from Uruguay in the English version,  and from  Argentina in the Spanish version,  and his winged donkey, who goes by the name of Burrito (Spanish for little donkey).

=== Bahia|Baía ===
This segment involves a pop-up book trip through the Brazilian state of Baía, as Donald and José meet up with some of the locals who dance a samba and Donald pining for one of the women, portrayed by singer Aurora Miranda.

=== Las Posadas ===
This is the story of a group of Mexican children who celebrated Christmas by re-enacting the journey of Mary, the mother of Jesus and Saint Joseph searching for room at the inn. "Posada" meant "inn", or "shelter", and their parents told them "no posada" at each house until they came to one where they were offered shelter in a stable. This leads to festivities including the breaking of the piñata, which in turn leads to Donald Duck trying to break his piñata as well.

=== Mexico: Pátzcuaro, Veracruz and Acapulco ===
Panchito gives Donald and José a tour of Mexico on a flying sarape. Several Mexican dances and songs are learned here. A key point to what happens later is that Donald is pining for some more ladies again, tries to hound down every single one he saw, and gain return affections, but once more fails and ends up kissing José while blindfolded.

=== You Belong To My Heart ===
The skies of Mexico City result in Donald falling in love with singer Dora Luz. The lyrics in the song itself play parts in the scenarios as to what is happening as well.

=== Donalds Surreal Reverie === state of La Zandunga". cacti appear in many different forms while dancing to "Jesusita en Chihuahua", a trademark song of the Mexican Revolution. This is a notable scene for live action and cartoon animation mixing, as well as animation among the cacti.

The scene is interrupted when Panchito and José suddenly spice things up for the finale, and Donald ends up battling the same toy bull with wheels on its legs from earlier. The catch is that it is again loaded with firecrackers and other explosives following with a fireworks finale with the words "The End" exploding from the fireworks first in Mexican Spanish (Fin) in the colors of the Mexican Flag, then in Brazilian Portuguese (Fim) in the colors of the flag of Brazil and finally in English in the colors of the flag of the United States of America.

== Production ==

=== Influence ===
Agustín Laras song "You Belong to My Heart" was featured in a Disney short called Plutos Blue Note (1947). It was later recorded by Bing Crosby. The Ary Barrosos song "Bahia" and the title song became popular hit tunes in the 1940s. The complete "Bahia" sequence was cut from the 1977 theatrical reissue of the film.

Some clips from this film were used in the "Welcome to Rio" portion of the Mickey Mouse Disco music video.

Don Rosa wrote two comic book sequels in 2000 and 2005 titled The Three Caballeros Ride Again and The Magnificent Seven (Minus 4) Caballeros respectively.

In September 2006, Panchito and José returned at Walt Disney World where they  appear for meet and greets. They can only be found outside the Mexico pavilion in World Showcase at Epcot. Donald also appears with them.

The 2011 Mickeys Soundsational Parade at Disneyland features all three Caballeros and the Aracuan Bird in one parade unit.

== Cast and characters ==
* Clarence Nash - Donald Duck (also dubbed the Spanish, Portuguese and Italian versions)
* José Oliveira - José Carioca (also dubbed the Spanish and Italian version)
* Joaquin Garay - Panchito Pistoles (also dubbed the Italian version and the songs in the Spanish version)
* Aurora Miranda
* Dora Luz
* Carmen Molina
* Sterling Holloway - Narrator (The Cold-Blooded Penguin) Frank Graham - Narrator
* Fred Shields&nbsp;— Narrator
* Francisco "Frank" Mayorga&nbsp;— Mexican Guitarist
* Nestor Amaral
* Trío Calaveras
* Trío Ascencio del Río
* Padua Hills Player
* Carlos Ramírez - Mexico

== Soundtrack == Paul J. Smith, and Charles Wolcott.
 Mexican song composed by Manuel Esperón with lyrics by Ernesto Cortázar. "Ay, Jalisco, no te rajes!" was originally released in a 1941 film of the same name, starring Jorge Negrete. After seeing Manuel Esperóns success in the Mexican movie industry, Walt Disney called him personally to ask him to participate in the movie. New English lyrics were written to the song by Ray Gilbert.

*"Baía" based its melody off of the Brazilian song "Na Baixa do Sapateiro" which was written by Ary Barroso and first released in 1938. New English lyrics were written by Ray Gilbert. Another Ary Barroso song, "Aquarela do Brasil", was featured in "The Three Caballeros prequel "Saludos Amigos", with its original Portuguese lyrics.

*"Have You Been to Bahia?" was written by Dorival Caymmi and was originally released in 1941. The song was translated into English with no major changes, other than replacing the word "nega" (A woman of African descent) with "Donald", who the song is addressed to in the film. Parts of the song are still sung in its original Portuguese.
 Dave Smith that the piece was not written originally for the film, but was instead licensed to Disney; however he is unaware of any evidence that proves this opinion. The piece was developed by Charles Wolcott, and Lacerda went uncredited in the film. {{cite web
|url=http://d23.disney.go.com/news/2012/06/d23-presents-ask-dave-june-12-2012/
| title =D23 Presents Ask Dave: June 12, 2012
| author =Dave Smith
| publisher =Disney D23
| accessdate =June 14, 2012
| archiveurl=http://www.webcitation.org/68PzPAjHF
| archivedate =June 14, 2012
| quote =While written by Lacerda (1903-1958) and licensed by Disney, it was developed by Charles Wolcott and Lacerda was uncredited. The piece appears at the end of the Baia train sequence and just before the “Os Quindins de Ya-Ya” sequence. A pandeiro is a Brazilian version of a tambourine.
}}  {{cite web
|url=http://d23.disney.go.com/news/2012/07/d23-presents-ask-dave-july-19-2012/
| title =D23 Presents Ask Dave: July 19, 2012
| author =Dave Smith
| publisher =Disney D23
| accessdate =July 22, 2012
| archiveurl=http://www.webcitation.org/69M2A19A3
| archivedate =July 22, 2012
| quote =It is the flute piece played during the train sequence, according to the film’s music cue sheet, running for one minute, three-and-two-thirds seconds. It is followed by silence, then “Os Quindins de Ya-Ya.” I have assumed it was not written for the film, but was simply licensed, though I have not seen evidence to back up that assumption.
}} 

*"Os Quindins de Yayá" was written by Ary Barroso and first released in 1941. Unlike Barrosos other song to be featured in this film, "Os Quindins de Yayá" was left in its original Portuguese. The song is sung by Aurora Miranda in the film.
 Braguinha in 1931. This song was first recorded under the name "Cena Carioca" and came to be known as "Pregões Cariocas" in 1936.
 Carlos Ramírez. It is the only song in the film to be completely original.

*The "Jarabe Pateño" was written by Jonás Yeverino Cárdenas in 1900. It is considered one of the most famous compositions from the Mexican state of Coahuila. {{cite web
|url=http://www.zocalo.com.mx/seccion/articulo/137491
| author =Ernesto Acosta
| title =Distingue a Coahuila el "Jarabe Pateño"; es reconocido a nivel mundial
| work =zocalo.com
| date =August 19, 2009
| accessdate =March 22, 2012
}} 

*"Lilongo" was written by Felipe "El Charro" Gil and copyrighted in the U.S. in 1946, {{cite web
|url=http://d23.disney.go.com/archives/
| title =Ask Dave Lilongo
| author =Dave Smith
| publisher =D23
| accessdate =January 13, 2012
| archiveurl=http://www.webcitation.org/64bIlby4X
| archivedate =January 13, 2012
| quote =“Lilongo” was written by Felipe “El Charro” Gil, and copyrighted in the U.S. by the music publisher Peer International Corp. in 1946. It is in the Son Jarocho style, a traditional musical style of the southern part of the Mexican state of Veracruz. Gil was born in Misantla, Veracruz, in 1913, into a family of musicians, and he made a study of the music of the area.
}}  though it was first recorded in the U.S. in 1938. It is performed by Trío Calaveras in the film.

*"You Belong to My Heart" based its melody off of the Mexican song "Solamente una vez", which was written by Agustín Lara. Like "Ay, Jalisco, no te rajes!" and "Na Baixa do Sapateiro", new English lyrics were written to the song by Ray Gilbert.

*"La Zandunga" (also spelt "La Sandunga") is a traditional Mexican song and the unofficial anthem of the Isthmus of Tehuantepec, in the Mexican state of Oaxaca. The melody is believed to have originated from Andalusia and was rearragned by Andres Gutierrez. Lyrics were written to it by Máximo Ramó Ortiz in 1853. It was arranged for this film by Charles Wolcott.

*The instrumental composition which plays while the cacti are dancing is "Jesusita en Chihuahua", a trademark of the Mexican Revolution which was written by Quirino Mendoza y Cortés in 1916. Overtime this piece has also come to be known under the names "J.C. Polka", "Jesse Polka", and "Cactus Polka".

*The instrumental composition "Sobre las olas (Over the Waves)" written by Mexican songwriter Juventino Rosas and first published in 1888 can be heard in the films score during The Cold-Blooded Penguin segment while Pablo the penguin is sailing to the Galapagos Islands. A small portion of "Jingle Bells" is briefly sung by Donald Duck.

== Nominations == 1944    
{| class="wikitable"
|- "
! Award
! Result
|- Best Musical Score
|  
|- Best Sound Recording  C. O. Slyfield 
|  
|}

== Release ==

=== Critical response ===
The Three Caballeros received mixed reviews when it was released and currently holds an 82% rating on Rotten Tomatoes. Most critics were relatively perplexed by the "technological razzle-dazzle" of the film, thinking that, in contrast to the previous feature films up to this time, "it displayed more flash than substance, more technique than artistry."    Bosley Crowther for one wrote in The New York Times, "Dizzy Disney and his playmates have let their technical talents run wild."  Other reviewers were taken aback by the sexual dynamics of the film, particularly the idea of Donald Duck lusting towards flesh-and-blood women. As The New Yorker put it in a negative review of the film, such a concept "is one of those things that might disconcert less squeamish authorities than the Hays office. It might even be said that a sequence involving the duck, the young lady, and a long alley of animated cactus plants would probably be considered suggestive in a less innocent medium."   

=== Television ===
For the films television premiere, The Three Caballeros aired as the ninth episode of the first season of American Broadcasting Company|ABCs Walt Disney anthology television series|Disneyland television series. Edited, shortened, and re-titled A Present For Donald for this December 22, 1954, broadcast and subsequent re-runs, Donald receives gifts from his friends for Christmas, instead of for his birthday as in the original. 

=== Theatrical Re-releases === Never a Dull Moment. 

=== Home video ===
* 1982 (VHS and Betamax)
* 1987 (VHS and Betamax)
* October 28, 1994 (VHS and Laserdisc - Walt Disney Masterpiece Collection)
* 1995 (Laserdisc&nbsp;— Exclusive Archive Collection)
* May 2, 2000 (VHS and DVD - Walt Disney Gold Classic Collection)
* April 29, 2008 (DVD&nbsp;— Classic Caballeros Collection)
* August 11, 2015 (Blu-ray/DVD Combo Pack - Special Edition Double Feature)

== Other media ==
One of the scenes of the former Mickey Mouse Revue features Donald, Jose and Panchito in the show, performing the movies theme song. In the queue for Mickeys PhilharMagic, there is a poster for "Festival de los Mariachis," which also features the three protagonists.

They also appear in some of Disneys themed resorts, such as Disneys Coronado Springs Resort where one can find topiaries of the trio, and Disneys All-Star Music Resort where a fountain depicting the trio is the centrepiece of the Guitar-shaped Calypso Pool.

Fictional music group Alvin and the Chipmunks covered the title song, "The Three Caballeros," for their 1995 Disney-themed album When You Wish Upon a Chipmunk; however, The Walt Disney Company neither sponsored nor endorsed the album the song was featured on. 
 House of Mouse series, voiced by Carlos Alazraqui (Pistoles) and Rob Paulsen (Carioca).

In April 2007, the film became the basis for a ride at the Mexican pavilion at Walt Disney Worlds Epcot named Gran Fiesta Tour Starring The Three Caballeros. 
 Alice and the White Rabbit, and others, Panchito, Jose, and Donald appear in the reopening of Disneylands Its a Small World in the Mexican segment of the ride.

== See also ==
* List of animated feature films

== References ==
 

== External links ==
*  
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 