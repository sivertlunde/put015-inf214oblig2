Beyond Borders
 
{{Infobox film
 | name = Beyond Borders
 | image = Beyond Borders film.jpg
 | caption = Theatrical poster for Beyond Borders
 | director = Martin Campbell
 | writer = Caspian Tredwell-Owen
 | starring = Angelina Jolie, Clive Owen
 | producer = Dan Halsted Lloyd Phillips
 | music = James Horner
 | cinematography = Philip Meheux
 | editing  = Nicholas Beauman
 | studio  = Mandalay Pictures
 | distributor = Paramount Pictures 
 | released =  
 | runtime = 127 minutes
 | country = United States
 | language = English
 | budget = $35 million
 | gross = $11,705,002 (worldwide)
}}

Beyond Borders is a 2003 Romantic drama|romantic-drama film about aid workers, directed by  Martin Campbell and starring Angelina Jolie and Clive Owen.   The original music score was composed by James Horner.

Reflecting Jolies real-life interest in promoting humanitarian relief,  the film was critically and financially unsuccessful. The film was marketed with the tagline "In a place she didnt belong, among people she never knew, she found a way to make a difference." 
 Goodwill Ambassador for the United Nations High Commissioner for Refugees (UNHCR) &mdash; similar to her character in the film.

==Plot==
While attending a fund-raising gala, Sarah Jordan (Angelina Jolie), a naive, married American socialite living in England, witnesses a fiery plea delivered by an intruder – a renegade humanitarian, Dr. Nick Callahan (Clive Owen). His plea made on behalf of impoverished children under his care turns Sarahs life upside down. Attracted to Nick and his cause, she impulsively abandons her job at an art gallery and sheltered life in England to work alongside him in his effort to aid the refugee camps. She travels to Ethiopia.

As Sarahs work takes her to these volatile areas, where few people have traveled and even fewer have survived, she discovers that the harsh realities she encounters, and her growing romantic attraction to the charismatic, unpredictable doctor, ignite in her a passion for saving lives, while risking her own in the process. She works for humanitarian and human rights organizations for ten years after she first travels to Ethiopia, and eventually works for the U.N.H.C.R. and acts as a regional representative for the United Kingdom.

==Cast==
*Angelina Jolie as Sarah Jordan Beauford
*Clive Owen as Dr. Nick Callahan
*Teri Polo as Charlotte Jordan
*Linus Roache as Henry Beauford
*Noah Emmerich as Elliott Hauser
*Yorick van Wageningen as Jan Steiger
*Timothy West as Lawrence Bauford
*Kate Trotter as Mrs. Bauford Jonathan Higgins as Philip
*John Gausden as Jimmy Bauford
*Isabelle Horler as Anna Beauford
*Iain Lee as Master of Ceremonies
*Keelan Anthony as Jojo
*John Bourgeois as Rolly
*Kalyane Tea as Steigers Girlfriend
*Julian Casey as Police Officer
*Norman Mikeal Berketa as Police Officer
*Aidan Pickering as TV Reporter
*Nambitha Mpumlwana as Tula
*Fikile Nyandeni as Gemilla
*Tony Robinow as Art Dealer
*Andrew French as Meles
*Jamie Bartlett as Joss  
*Tumisho Masha as Hamadi
*Kate Ashfield as Kat
*Faye Peters as Monica
*John Matshikiza as Dawit Ningpopo
*Zaa Nkweta as Titus
*Sahajak Boonthanakit as Port Official
*Dennis Tan as Port Official
*Doan Jaroen-Ngarm Mckenzie as Tao
*Burt Kwouk as Colonel Gao
*Teerawat Mulvilai as Ma Sok
*Bertrand A. Henri as Speaker
*Jasmin Geljo as Truck Driver
*Francis X. McCarthy as Strauss
*Manuel Tadros as Chechen Mobster
*Elizabeth Whitmere as Beatrice

==Production==
The film was shot on location in Thailand, Namibia and Canada.

==Reception==
Beyond Borders received negative reviews from critics, as the movie currently holds a 14% rating on Rotten Tomatoes based on 103 reviews. Critical consensus on the film has it that "Beyond Borders is good-intentioned, but the use of human suffering as a backdrop for a romance comes across as sanctimonious and exploitative."

It was nominated for the  , but lost to Jennifer Lopez for Gigli. 

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 