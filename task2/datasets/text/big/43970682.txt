Oka Chinna Maata
{{Infobox film
| name           = Oka Chinna Maata
| image          =
| caption        =
| writer         = Diwakar Babu  
| story          = Bhupathi Raja
| screenplay     = Muthyala Subbaiah 
| producer       = B. Shiva Rama Krishna
| director       = Muthyala Subbaiah Indraja
| music          = Ramani Bharadwaj
| cinematography = V. Srinivasa Reddy
| editing        = Cola Bhaskar
| studio         = Sri Venkateshwara Art Films
| released       =  
| runtime        = 2:27:57
| country        = India
| language       = Telugu
| budget         =
}}  
 Indraja in lead roles and music composed by Ramani Bharadwaj. The film recorded as Super Hit at box office.  

==Cast==
{{columns-list|3|
* Jagapati Babu as Chandu/Chanti Indraja as Geeta
* Raksha as Sireesha Satyanarayana as Abbayigaru
* Rajeev Raj as Rajeev
* Chalapathi Rao as Chalapati Rao
* Brahmanandam
* Babu Mohan as Moorti Mallikarjuna Rao as Chanchal Rao
* Amanchi Venkata Subrahmanyam|A.V.S. as Chanchal Raos friend
* Suthivelu as Parandamaiah
* Raghunatha Reddy as Abbayis brother-in-law
* Gundu Hanumantha Rao as Malokam
* Kallu Chidambaram as photographer
* Tirupathi Prakash as photographer
* Gowtham Raju as Auto Driver
* Junior Relangi as Relangi Jenny
* Annapoorna as Shireeshas mother Sri Lakshmi as Chanchal Raos wife
* Bangalore Padma as Abbayis wife
* Ragini as Geetas friend
* Padma Jayanthi
* Ramyasri
* Y. Vijaya as Relangi
}}

==Soundtrack==
{{Infobox album
| Name        = Oka Chinna Maata
| Tagline     = 
| Type        = film
| Artist      = Ramani Bharadwaj
| Cover       = 
| Released    = 1997
| Recorded    = 
| Genre       = Soundtrack
| Length      = 29:24
| Label       = Supreme Music
| Producer    = Ramani Bharadwaj
| Reviews     =
| Last album  =  
| This album  = 
| Next album  = 
}}

Music composed by Ramani Bharadwaj. All Songs are hit tracks. Music released on Supreme Music Company.
{|class="wik{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 29:24
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = yes
| music_credits =


| title1  = O Manasaa Thondhara Sirivennela Sitarama Sastry SP Balu,K. Chitra 
| length1 = 5:00

| title2  = Kurrakaaru Poojinche  
| lyrics2 = Bhuvanachandra
| extra2  = SP Balu,Chitra
| length2 = 5:07

| title3  = Abbo Oyabbo  
| lyrics3 = Bhuvanachandra
| extra3  = Mano (singer)|Mano,Srilekha
| length3 = 5:06 

| title4  = Madhuramu Kaadhaa
| lyrics4 = Sirivennela Sitarama Sastry 
| extra4  = Chitra
| length4 = 4:07

| title5  = Prathi Okariki Tholi Valapuna 
| lyrics5 = Bhuvanachandra 
| extra5  = SP Balu,Chitra
| length5 = 5:09

| title6  = Yevarini Choosthu Vunnaa  
| lyrics6 = Bhuvanachandra 
| extra6  = SP Balu,Chitra
| length6 = 4:55
}}

==External links==
 

==References==
 

 

 
 
 
 

 