Far North (1988 film)
{{Infobox film|
| name = Far North 
| image = Far North.JPG
| caption = 
| director = Sam Shepard
| writer = Sam Shepard
| producer = Carolyn Pfeiffer Malcolm R. Harding
| starring = Jessica Lange Charles Durning Tess Harper Donald Moffat Ann Wedgeworth Patricia Arquette Nina Draxten
| music = The Red Clay Ramblers
| cinematography = Robbie Greenberg
| editing = Bill Yahraus
| studio = Alive Films
| distributor = Nelson Entertainment
| runtime = 89 minutes
| released = September 10, 1988
| awards = 
| preceded_by =
| followed_by =
| country = United States
| language = English
}}
Far North is a 1988 drama film written and directed by Sam Shepard and starring Jessica Lange, Charles Durning, Tess Harper, Donald Moffat, Ann Wedgeworth and Patricia Arquette.

==Cast==
*Jessica Lange as Kate
*Charles Durning as Bertrum
*Tess Harper as Rita
*Donald Moffat as Uncle Dane
*Ann Wedgeworth as Amy
*Patricia Arquette as Jilly
*Nina Draxten as Gramma

==References==
 
* 
* 
 

==External links==
 
 

 
 
 
 
 
 


 