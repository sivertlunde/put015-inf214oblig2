Indradhanush (film)
{{Infobox film|
| name = Indradhanush
| image = 
| caption =
| director = V. Manohar
| based on = 
| screenplay = V. Manohar
| story = Ramesh Rao Abhisaarika   Akanksha Tripathi
| producer = Poornima S. Babu
| music = V. Manohar
| cinematography = Sundarnath Suvarna
| editing = T. Shashikumar
| studio = Sumanth Productions
| released =  
| runtime = 153 minutes
| language = Kannada
| country = India
| budget =
}} Kannada action action drama film directed, written and scored by V. Manohar and produced by Poornima S Babu. The film features Shiva Rajkumar, Monal (actress)|Abhisaarika, Akanksha Tripathi and Hari Nayar in the lead roles.   The film released on 31 March 2000 to mixed and negative reviews and performed poorer at the box-office.

== Cast ==
* Shiva Rajkumar  Abhisaarika
* Akanksha Tripathi
* Gurukiran
* Hari Nayar
* Avinash
* M.N Lakshmi Devi
* Sudhakar Banannje
* Charulatha...special appearance

== Soundtrack ==
The soundtrack of the film was composed by V. Manohar. 

{{track listing 
| headline = Track listing
| extra_column = Singer(s)
| lyrics_credits = yes
| collapsed = no
| title1 = Indradhanush Anupama
| lyrics1 =V. Manohar
| length1 = 
| title2 = Namma Prapancha
| extra2 = Shiva Rajkumar, B. R. Chaya, Rithish
| lyrics2 = V. Manohar
| length2 = 
| title3 = Nightingale Nightingale Mano
| lyrics3 = V. Manohar
| length3 = 
| title4 = Olden Days
| extra4 = Ramesh Chandra, Shanbog
| lyrics4 = V. Manohar
| length4 = 
| title5 = Chendavo Chendavo
| extra5 = Sangeetha Katti
| lyrics5 = V. Manohar
| length5 = 
| title6 = Maayaya Yaa Maayaya
| extra6 = V. Manohar
| lyrics6 = V. Manohar
| length6 = 
| title7 = Yara Kanasalla Rajkumar
| lyrics7 = V. Manohar
| length7 = 
| title8 = Kelu Kelavva Kelavva Rajkumar
| lyrics8 = V. Manohar
| length8 = 
}}

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 


 

 