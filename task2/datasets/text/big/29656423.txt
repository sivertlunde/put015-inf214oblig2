Chasing Ghosts (film)
{{Infobox film
| name = Chasing Ghosts
| image = Chasingghostsposter.JPG
| director = Kyle Dean Jackson
| producer = Brian Hartman Ari Palitz Alan Pao Corey Large Alan Pao
| starring = Michael Madsen Shannyn Sossamon Gary Busey Meat Loaf Michael Rooker
| music = Scott Glasgow
| cinematography = Andrew Huebscher
| editing = Kyle Dean Jackson
| distributor = Wingman Productions Sony Pictures Home Entertainment
| released =  
| runtime = 119 minutes
| country = United States
| language = English
| budget = $2 million
| gross = $1,102,000
}}
Chasing Ghosts is a 2005 mystery film starring Michael Madsen.

==Plot==
NYPD Detective Kevin Harrison (Michael Madsen), a cop who used to be on the take from mobster Marcos Alfiri (Gary Busey), has since turned good. Harrison is close to retirement when the son (James Duval) of a big-time gangster is killed; and no evidence is left behind. Together with his partner Cole (Corey Large), Harrison goes into the deep of the New York mob world, focused on finding his killer.

==Cast==
*Michael Madsen as Kevin Harrison Corey Large as Cole Davies
*Shannyn Sossamon as Taylor Spencer
*Meat Loaf as Richard Valbruno (as Michael Meat Loaf Aday)
*Gary Busey as Marcos Alfiri
*Lochlyn Munro as John Turbino
*Michael Rooker as Mark Spencer
*Danny Trejo as Carlos Santiago
*James Duval as Dmitri Parramatti
*Jeffrey Dean Morgan as Detective Cole Davies
*Mark Rolston as Frank Anderson

==Production==
Filming took place in Los Angeles, California and New York City, New York a budget of roughly $2,000,000. The film grossed only a bit more than half of its budget. It was released direct to DVD on March 14, 2006, and holds a 35% approval rating on Rotten Tomatoes.

==References==
*  
*  
*  

 
 
 
 
 
 
 
 

 