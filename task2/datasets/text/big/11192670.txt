Al's Lads
 

{{Infobox film
| name           = Als Lads
| image          =
| image_size     =
| caption        =
| director       = Richard Standeven
| producer       =
| writer         = Marc Gee
 | narrator       =
| starring       = Marc Warren Ralf Little Al Sapienza
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =
| runtime        =
| country        = United Kingdom
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
 British crime/drama film.

==Plot==
Three Englishmen working as waiters on a cruise ship in 1927 are given a chance to work for the Al Capone gang.

==Cast==
*Marc Warren as Jimmy
*Kirsty Mitchell as Edith
*Peter Pedrero as Brendan
*Al Sapienza as Georgio
*Ralf Little as Dan
*Julian Littman as Al Capone
*Scott Maslen as Sammy
*Stephen Lord as Eddy
*Richard Roundtree as Boom Boom
*Ricky Tomlinson as Billy

==References==
 
 

 