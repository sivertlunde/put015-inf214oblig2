Children of Nature
{{Infobox film
| name           = Children of Nature
| image          = ChildrenOfNature1991Poster.jpg
| caption        = Film poster
| director       = Friðrik Þór Friðriksson
| producer       = Friðrik Þór Friðriksson Vilhjálmur Ragnarsson
| writer         = Friðrik Þór Friðriksson Einar Guðmundsson
| starring       = Gísli Halldórsson Sigríður Hagalín Baldvin Halldórsson Björn Karlsson
| music          = Hilmar Örn Hilmarsson
| cinematography = Ari Kristinsson
| editing        = Skafti Gudmundsson
| distributor    = 
| released       =  
| runtime        = 82 minutes
| country        = Iceland
| language       = Icelandic English
| budget         = 
| gross          = 
}} 1991 Cinema Icelandic film Best Foreign Language Film Oscar at the 64th Academy Awards.

==Plot==
A man becomes too old to run his farm and he is made unwelcome in his daughter and son-in-laws urban dwelling. Dumped in a home for the elderly, he meets an old girlfriend from his youth, and they elope to the wilds of Iceland to die together. 

==See also==
* List of submissions to the 64th Academy Awards for Best Foreign Language Film
* List of Icelandic submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 
* 

 

 
 
 
 
 


 
 