Paranoia (1967 film)
{{Infobox Film
| name           = Paranoia
| image          = Paranoia (1967 film).jpg
| caption        = 
| director       = Adriaan Ditvoorst
| producer       = 
| writer         = Adriaan Ditvoorst Willem Frederik Hermans
| starring       = Kees van Eyck
| music          = 
| cinematography = Jan de Bont
| editing        = Jan Bosdriesz
| distributor    = 
| released       =  
| runtime        = 102 minutes
| country        = Netherlands
| language       = Dutch
| budget         = 
}}

Paranoia is a 1967 Dutch drama film directed by Adriaan Ditvoorst. It was entered into the 17th Berlin International Film Festival.   

==Cast==
* Kees van Eyck - Arnold Cleever
* Pamela Koevoets - Anna (as Pamela Rose)
* Rudolf Lucieer - Reclametekenaar
* Paul Murk - Cleevers oom
* Mimi Kok - Zijn maîtresse
* Ab van Ieperen
* Ton Vos - Annas vader
* Rob du Mee - Priester
* Max Kok
* Helene Kamphuis
* Ko Roblin
* Jan ter Oever

==References==
 

==External links==
* 

 
 
 
 
 
 
 