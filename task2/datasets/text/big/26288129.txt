Let the Game Begin
{{Infobox film
| name           = Let the Game Begin
| image          = LetTheGameBeginPoster.jpg
| director       = Amit Gupta
| producer       =  
| writer         = Amit Gupta
| starring       =  
| editing        =   
| music          =  
| studio         =  
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = English
}}
Let the Game Begin is a 2010 American film romantic comedy from Twisted Light Productions,  directed by   actor Adam Rodriguez  and Stephen Baldwin.  The film was released in Australia  on May 5, 2010, and was released in other countries in 2010 and 2011.

==Cast==
* Adam Rodriguez as Rowan
* Stephen Baldwin as David Carroll
* Lisa Ray as Eva
* Michael Madsen as Dr. Turner
* Diora Baird as Kate
* Thomas Ian Nicholas as Tripp
* Jesse Jane as Temptation
* Jennifer Rhodes as Hope
* Lochlyn Munro as Gary Johnson
* Ken Davitian as Eric Banks
* Natasha Henstridge as Angela James Avery as Hanley
* Caroline Neron as Samantha
* Sid Curtis as Younger Rowan
* James Matador as Matador PUA
* Jasmine Dustin as Nadine wing woman
* Lisa Jay as Scarlet wing woman
* Cristina Rosato as Francine social proof
* Ashleigh Harrington as Anna dancer

==Development== Las Vegas, Montreal, and Philadelphia. 
International release was in April 2010.  U.S and Canada release is expected in 2011.

==References==
 

==External links==
*  

 
 
 
 
 
 
 