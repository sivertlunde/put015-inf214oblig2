The Hunchback of Notre Dame (1939 film)
{{Infobox film
| name           = The Hunchback of Notre Dame
| image          = 1939-The-Hunchback-of-Notre-Dame.jpg
| caption        = Theatrical poster
| producer       = Pandro S. Berman
| director       = William Dieterle
| based on       =  
| screenplay     = Sonya Levien Bruno Frank Thomas Mitchell Edmond OBrien Alfred Newman
| cinematography = Joseph H. August William Hamilton Robert Wise RKO Radio Pictures
| released       =  
| runtime        = 116 minutes
| country        = United States
| language       = English
| budget         = $1,826,000 Richard Jewel, RKO Film Grosses: 1931-1951, Historical Journal of Film Radio and Television, Vol 14 No 1, 1994 p56  
| gross          = $3,155,000 
}}
 American film 1831 novel 1923 silent film version starring Lon Chaney.
 RKO Radio Pictures built on their movie ranch a massive medieval city of Paris and Notre Dame Cathedral, one of the largest and most extravagant sets ever constructed.

==Plot==
The film opens with King Louis XI and his close advisor, Claude Frollo|Frollo, the Kings Chief Justice of Paris, France|Paris, in a printing shop. Frollo is determined to do everything in his power to rid Paris of the printing press and Romani people|gypsies, who at the time are persecuted and prohibited from entering Paris. That day is Paris annual celebration, the Festival of Fools. Esméralda (The Hunchback of Notre Dame)|Esmeralda, a young gypsy girl, is seen dancing in front of an audience of people, including the King and Frollo. Quasimodo, the hunchback and bell ringer of Notre Dame Cathedral, is crowned the King of Fools until Frollo catches up to him and takes him back to the church.

Esmeralda is caught by Paris guards and is been chased after until she seeks safety in Notre Dame, to which the Archbishop of Paris, Frollos brother, protects her. She prays to the Virgin Mary to help her fellow gypsies only to be confronted by Frollo who accuses her of being a heathen. At this point she asks King Louis to help her people, to which he agrees. Frollo then takes her to Quasimodo, whom she is frightened of. She tries to run away from the hunchback until he catches up to her and physically carries her away. Pierre Gringoire, a poor street poet, witnesses all this, calls out to Phoebus and his guards, who capture Quasimodo just in time. Esmeralda is then saved and starts falling in love with Phoebus. Gringoire later trespasses the Court of Miracles but is saved by Esmeralda from hanging by marrying him.

The next day, Quasimodo is sentenced to be lashed in the square. Frollo, seeing this, realizes that he cant stop the sentence because it already happened, and abandons him instead. However, Esmeralda arrives and gives him water, and this awakens Quasimodos love for her.

Later that night, Esmeralda is invited by the nobles to their party. Frollo shows up to the party, where he confesses to Esmeralda his lust for her. Afterwards she dances in front of the nobles and moves away from the crowd with Phoebus to a garden where they share a moment between each other. Frollo then kills Phoebus out of jealousy, and Esmeralda is wrongly accused of his death. Afterwards, Gringoire visits Esmeralda in her prison cell to console her. Meanwhile, Frollo confesses his crime to his brother, and is in charge of Esmeraldas trial and sentences her to death. After she is about to be hanged in the gallows, Quasimodo saves her by taking her to the cathedral.

When Gringoire and Clopin realize that the nobles will revoke Notre Dames right of sanctuary, they both try different methods in order to save Esmeralda from hanging. Gringoire writes a pamphlet that will prevent this from happening, and Clopin leads the beggars to storm the cathedral. After the King reads the poets pamphlet, Frollo confesses his crime to him and Gringoire comes in to talk to Louis. After Quasimodo and the guards fight off the beggars, he sees Frollo in the bell tower seeking to harm Esmeralda, and throws him off the cathedral top. Later that morning, Esmeralda is pardoned and freed from hanging, and her Gypsy people are also finally freed. She leaves with Pierre Gringoire and a huge crowd out of the public square. The film makes it clear that in the end Esmeralda truly loves Gringoire. Quasimodo sees all this from above the cathedral and says sadly, "Why was I not made of stone, like thee?"

==Cast==
* Charles Laughton as Quasimodo Esmeralda
* Frollo
* Thomas Mitchell Clopin
* Gringoire
* Alan Marshal Phoebus
* Walter Hampden as Archdeacon Harry Davenport as King Louis XI
* Katharine Alexander as Madame de Lys
* George Zucco as Procurator Fritz Leiber as Old Nobleman
* Etienne Girardot as Doctor
* Helene Whitney as Fleur de Lys Mina Gombell as Queen of Beggars Olivier
* Curt Bois as Student
* George Tobias as Beggar
* Rod La Rocque as Phillippe
* Spencer Charters as Court Clerk Kathryn Adams as Fleurs Companion
* Dianne Hunter as Fleurs Companion
* Sig Arno as Tailor

==Censorship== 1923 film: 
instead of being the bad archdeacon as in the novel, Claude is good, and his original position as a villain was given to his younger brother, Jehan, who was a drunken student in the novel and again portrayed as an older man instead of a teenager. The only difference in this film is that Claude is portrayed as an archbishop and Jehan is portrayed as a judge.

==Legacy== 1996 animated film version of the story,  which borrowed several of its ideas.

==Award nominations==
The film was nominated for two Academy Awards:    Alfred Newman)
* Academy Award for Best Sound  (John Aalberg)

==Reception==
The movie was very popular but because of its cost only made a profit of $100,000. 

E. H. Harvey of The Harvard Crimson said that the film "in all is more than entertaining." He said that "the mediocre effects offer a forceful contrast to the great moments" in the film. 

==Home video==
The Hunchback of Notre Dame was released on DVD on January 6, 1998.

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 