A Family Secret
 
 
{{Infobox film
| name           = A Family Secret
| image          = AFamilySecret2006Poster.jpg
| image_size     =
| caption        = Canadian Poster
| director       = Ghyslaine Côté
| producer       = Julien Remillard (executive producer) Maxime Rémillard André Rouleau (producer)
| writer         = Ghyslaine Côté Martin Girard
| starring       = Ginette Reno Céline Bonnier Clémence DesRochers Marie-Chantal Perron
| music          = Normand Corbeil
| cinematography = Pierre Mignot
| editing        = Richard Comeau
| distributor    = Remstar Distribution
| released       =  
| runtime        = 86 minutes
| country        = Canada
| language       = French
| budget         = Canadian dollar|$ 5,450,000 (estimated)
| gross          =
| preceded_by    =
| followed_by    =
}}
A Family Secret is a 2006 Canadian comedy film|comedy-drama film. The French language title for the film is Le Secret de ma mère (My Mothers Secret).

== Plot ==

Set on New Years Day, Jos (David Boutin) family and friends gather together at a funeral parlour where chaos ensues.

== Awards and nominations ==
A Family Secret earned two Genie Award nominations (Genie Award for Best Performance by an Actress in a Leading Role for Ginette Reno and Genie Award for Best Original Screenplay) and won a Stony Brook Film Festival award for Best Feature.

== External links ==
*  
*  

 
 
 
 
 
 
 
 