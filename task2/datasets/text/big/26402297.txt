The Bloody Brood
{{Infobox film
| name           = The Bloody Brood
| image_size     =
| image	=	The Bloody Brood FilmPoster.jpeg
| caption        =
| director       = Julian Roffman
| producer       = Julian Roffman (producer)
| writer         = Anne Howard Bailey (story) Ben Kerner (writer) Elwood Ullman (writer)
| narrator       =
| starring       = See below
| music          = Harry Freedman
| cinematography = Eugen Schüfftan
| editing        =
| distributor    =
| released       =
| runtime        = 80 minutes 88 minutes (American DVD)
| country        = Canada, USA
| language       = English
| budget         =
| gross          =
}}

The Bloody Brood is a 1959 Canadian film directed by Julian Roffman.

==Plot==
A man begins to investigate on his own the death of his brother, who died from eating a hamburger laced with ground glass. With the police case stalled because of ineptness, the mans own investigation leads him toward a beatnik hang-out frequented by Nico (Peter Falk), a shady character who supplies drugs to the patrons and philosophizes about the ills of the world.

==Cast==
*Jack Betts as Cliff
*Barbara Lord as Ellie
*Peter Falk as Nico Robert Christie as Detective McLeod
*Ron Hartmann as Francis Anne Collins as A Model
*Bill Bryden as Studs
*George Sperdakos as Ricky Ron Taylor   as Dave
*Michael Zenon as Weasel
*William R. Kowalchuk as Roy
*Sammy Sales as Louis
*Kenneth Wickes as Paul the Poet
*Carol Starkman as Blonde Neighbor
*Rolf Colstan as Stephanex

== External links ==
* 
* 

 
 
 
 
 
 
 
 
 
 


 
 