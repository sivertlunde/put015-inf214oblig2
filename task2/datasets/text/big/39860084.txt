Subconscious Password
{{Infobox film
| name           = Subconscious Password
| image          = 
| border         = 
| alt            = 
| caption        = 
| film name      = 
| director       = Chris Landreth
| producer       = Marcy Page Mark Smith
| writer         = Chris Landreth
| story          = 
| based on       =  
| narrator       = 
| starring       = Chris Landreth John Dilworth Don McKellar Ron Pardo Patrice Goodman Ray Landry
| music          = Daniel Janke
| cinematography = 
| editing        = 
| studio         = National Film Board of Canada
| distributor    = 
| released       =  
| runtime        = 11:15 minutes
| country        = Canada
| language       = English
| budget         = 
| gross          = 
}}
Subconscious Password is a 2013 3-D film|3-D animated film by Chris Landreth offering an imaginary, comedic look at the inner workings of Landreths mind, as he tries to remember someones name at a party.

The film was produced the National Film Board of Canada (NFB) with the participation of Copperheart Entertainment and the Seneca College Animation Arts Centre.    The film is made with computer animation, as well as pixilation sequences at the beginning and end of the film, featuring animation director John R. Dilworth as the friend whose name Landreth cannot recall. Subconscious Password is Landreths first 3-D film and third with the NFB, Copperheart Entertainment and Seneca College. The idea for the film came to Landreth after watching a rerun of Password (game show)|Password in 2010.   

==Synopsis==
The film presents the mental process of remembering a friends name as it were an episode of the old Password game show, with various celebrities attempting to assist Landreth. 

==Production==
More than fifteen Seneca College graduating students worked on Subconscious Password, supported by five faculty members, as part of the Seneca Summer Animation Institute. After graduation, a number of these students were hired to work on the project until it was completed in February 2013. Most of the film was produced at Senecas Animation Arts Centre in Toronto, with three Seneca students also working on the film at the NFBs Animation Studio in Montreal. Subconscious Password was the first stereoscopic 3D film for Seneca College. 
 Imax that allows artists to create hand-drawn animation in 3D space, and which has been licensed to the NFB to develop creative applications.   
 stereoscopic depth to immerse audiences as the film progresses. 

==Release== Canadian Screen Awards. 

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 