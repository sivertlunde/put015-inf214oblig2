Deep Blood
{{Infobox film
| name           = Deep Blood
| image          = Deep Blood.jpg
| image_size     =
| caption        =
| director       = Raffaele Donato Joe DAmato (uncredited)
| producer       = Joe DAmato
| writer         = George Nelson Ott
| starring       = Frank Baroni Allen Cort Keith Kelsch James Camp
| music          =
| cinematography = Joe DAmato
| editing        =
| distributor    = Filmirage
| released       =  
| runtime        = 90 minutes
| country        = Italy Italian English English
| budget         =
}} Italian shark shark attack film directed by Raffaele Donato and Joe DAmato and written by George Nelson Ott. It was edited by Kathleen Stratton, and contained original music by Carlo Maria Cordio. The film was made in Italy by Filmirage S.r.l. and Variety Film Production. 

== Synopsis == Native American, hoodoo spirit in the form of a killer shark.

== Cast ==
* Frank Baroni
* Allen Cort
* Keith Kelsch
* James Camp
* Tody Bernard
* John K. Brune
* Margareth Hanks
* Van Jensens
* Don Perrin
* Claude File
* Charlie Brill
* Mitzi McCall

== Release ==
As of 2011, the film has still not been officially released in the United States in any home video format. It was released on DVD in the Czech Republic in 2009. 

==See also==
*List of killer shark films

== References ==
 

== External links ==
 

 

 
 
 
 
 
 
 
 
 
 
 
 



 
 