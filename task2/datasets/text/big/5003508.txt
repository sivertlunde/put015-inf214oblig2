The Syrian Bride
{{Infobox Film 
 | name = The Syrian Bride
 | image = The Syrian Bride film.jpg
 | caption = The Syrian Bride film poster
 | director = Eran Riklis 
 | writer = Suha Arraf Eran Riklis
 | starring = Hiam Abbass Makram Khoury Clara Khoury 
 | producer = Bettina Brokemper 
 | distributor = Koch-Lorber Films 
 | budget = 
 | released = 2 December 2004 ( Israel ) 
 | runtime = 97 min.
 | country = Israel, France, Germany
 | language = Arabic, English, Hebrew, Russian, French
  | }}
 2004 film directed by Eran Riklis. The story deals with a Druze wedding and the troubles the politically unresolved situation creates for the personal lives of the people in and from the village. The movies plot looks at the Arab-Israeli conflict through the story of a family divided by political borders, and explores how their lives are fractured by the regions harsh political realities.

The film has garnered critical acclaim and has won or been nominated internationally for several notable awards.

==Plot==
 occupied Golan and Syria observed by United Nations staff. Crossing of the zone is extremely rare as it is only granted by both sides under special circumstances. It has taken 6 months to obtain permission from the Israeli administration for Mona to leave the Golan. When Mona crosses she will not be able to return to her family on the Golan even to visit. Mona is a bit hesitant also because she doesnt know her husband-to-be.

Her father Hammed (Makram Khoury) openly supports the reunification with Syria and has just been released on bail from an Israeli prison. For this personal sacrifice he is respected by the elders of the village — yet when there is word that his son Hattem (Eyad Sheety) who has married Evelyna (Evelyn Kaplun), a Russian doctor, breaking with Druze tradition, is returning to see his sister off, they make it clear to the father that they will shun him too if he allows Hattem to come to the wedding.

Monas sister Amal (Hiam Abbass) is unhappily married and has two daughters who are almost grown up. She is considered a somewhat free spirit as she and her daughters wear trousers. Now she is even considering training as a social worker. Her elder daughter wants to marry the son of a pro-Israeli villager. Amals husband feels put in an awkward position, as tradition demands that the male head of the family controls the other members of the family to act in a socially acceptable manner. Amal is seen advising her daughter not to give up her studies irrespective of whatever pressures she may face from the family or society. This gives insight into Amals persona.

The second brother, Marwan (Ashraf Barhom), is a shady merchant doing deals in Italy and obviously a womanizer. Yet nobody seems to object to his slightly unsettled lifestyle — quite a contrast to his brother who is only greeted by his mother and siblings.

Then, after the wedding feast, the bride is escorted to the border where her emigration runs into trouble, as the Israeli government has just decided to stamp the passports of Golan residents bound for Syria as leaving Israel. The Syrian officials still regard the Golan as part of Syria under foreign occupation, and a stamp like that is viewed as an   ploy by the Israelis to force the Syrian side to implicitly acknowledge the annexation. 

So the UN liaison officer Jeanne goes back and forth until the Israeli official who put the stamp into the passport in the first place finally agrees to erase it with some correction fluid. 
Yet just as the problem seems to have been peacefully resolved, the solution is threatened by a change of position on the Syrian side.

In the end when it looks as if the wedding will be delayed at least for some days (which is regarded as a bad omen), the bride takes matters into her own hands. In our final view of her, she is walking with energy and determination toward the Syrian border; at the same time Amal walks away from the group with a determined face as if she would shatter the invisible fences which prevent her from pursuing her dreams.

==Awards==
* 2004  " (Best Film Award) - Eran Riklis
* 2004 Flanders International Film Festival, "Best Screenplay"
* 2004 Locarno International Film Festival, "Audience Award"
* 2005 Bangkok International Film Festival, "Golden Kinnaree Award" (Best Film Award)
* 2005 European Film Award nomination, "Best Actress" - Hiam Abbass

==See also==
*Cup Final (film) (1991)
*Lemon Tree (film) (2008)

==Notes==
 

==External links==
*Jonathan Cook,  , electronicIntifada,  21 July 2005.
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 