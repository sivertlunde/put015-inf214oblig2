Peaches (film)
 
{{Infobox film
| name           = Peaches
| image          = peaches-movie-poster.jpg
| caption        = DVD cover
| director       = Craig Monahan
| producer       = Judith McCann Margot McDonald Craig Monahan Don Reynolds Nicholas Stiliadis Roslyn Walker
| writer         = Sue Smith
| starring       = Hugo Weaving Jacqueline McKenzie Emma Lung
| music          = David Hirschfelder Ernie Clark
| editing        = Suresh Ayyar
| distributor    = Hopscotch Films
| released       =  
| runtime        = 109 minutes
| country        = Australia
| language       = English
| budget         = A$5,500,000
| gross          = A$407,000
}} Ernie Clark Australian Cinematographers Society|A.C.S., editing by Suresh Ayyar and art direction by Paula Smith.

==Plot==
Steph (Emma Lung) lost her parents in a car accident while still a baby. She was raised by her parents over-protective best friend, Jude (Jacqueline McKenzie).   She receives her dead mothers locked diary on her 18th birthday, the same day she starts work at the local peach cannery, and begins dual journeys, one pushing into the mysterious past and the other pursuing romantic complications in the present. The diary "reveals the colourful and sexy past of those close to her."   

Steph learns about her mother Jass (Samantha Healy), her father Johnny (Tyson Contor), and about the difficulties of love with her boss Alan Taylor (Hugo Weaving).

"Peaches is a love story that deals with accepting loss and change, and learning to move on." 

The director said of the characters: "They’re all just people. In fact Sue (Smith, writer) wanted more bonking in it, so that was never an issue,” he adds gamely. “There’re different journeys for different people; I spent a lot of time making that a reality. Men over 40 will go with Hugo’s journey; I find women around 30 plus will go with Jacqui; younger people go with Emma, but young men don’t go with the film at all... I’ve seen it in three countries   with many different audiences, and I do find there are different journeys for different people." 

==Cast==
  
*Hugo Weaving as Alan
*Jacqueline McKenzie as Jude
*Emma Lung as Steph
*Matthew Le Nevez as Brian
*Samantha Healy as Jass
*Tyson Contor as Johnny
*Catherine Lambert as Kath
*Giang Le Huy as Thuy
*Felicity Electricity as Sandy
*Ling Yeow as Chen Poh
*Caroline Mignon as Maria
*Duncan Hemstock as Kenny Carter
*Ed Rosser as Grandpa
*Peter Michell as Dave

Limited Edition Standees of the Cast were also made available

==Release==
The film premiered at the Montreal Film Festival on	30 August 2004, at the Boomerang Australian Film Festival (Hungary) on 24 September 2004, at the Hollywood Film Festival in the USA on 17 October 2004, at the 2005 Adelaide Film Festival on 26 February 2005, and at the Cannes Film Market on 11 May 2005.

==Classification== Australian Government MA 15+ for its strong themes, strong sex scenes, and strong coarse language.

==Critical reception==
Julie Rigg at Radio National wrote that: "This is not a bad little movie ... perhaps a little well-mannered in the way it has its actors front the screen, but at least these are not ocker caricatures we’re seeing here." 

Margaret Pomeranz objected to the central sexual relationship of the film. "I know I sound like a Victorian aunt, but I really hated that betrayal of that relationship between Alan and Steph. I mean, its not the older man, younger woman thing, it isnt, its almost like hes her father and its almost like an incestuous relationship. A film takes a step like that and it takes me where I really dont want to go. I reacted against everybody, I felt alienated from those characters at that moment." 

Sandra Hall of the Sydney Morning Herald took a (slightly) different line. "Then the unlikely but predictable happens. As Stephs romance with the past intensifies, she and 42-year-old Alan have an affair. You can see it looming, yet wish you couldnt for, once it hits, the resulting subterfuges and secret meetings rip all credibility out of the storyline. From then on, its up to the actors to keep it from falling apart - something they do by generating such goodwill that it seems mean-spirited not to stay with them."   

Sarah Barnett of the Sydney Anglican Network said, "Moody and absorbing, Peaches avoids creating clichéd or overly eccentric characters opting for more believable, richly drawn men and women. Audiences should note that the film does contain explicit sex scenes. While the relationship these scenes depicts is key to the plot, the level of nudity does seem somewhat exploitative of Emma Lung.  Elegantly written and filmed Peaches is compelling but not entirely satisfying as a drama. Despite strong performances, a haunting score and good production values its ending is somewhat bittersweet." 

Fr Richard Leonard SJ of the Catholic Church in Australia, said that "What many Catholic viewers will not care for is the number and style of the sexual encounters between an 18 years old innocent and her 42 year old married boss, especially when we know that Alan was for the first year or so of Stephs life Judes partner and her surrogate father. Whatever of the theme of Steph recreating history, this worrying suggestion of incest and work place harassment highlights how dysfunctional the relationships are between the three main players." 

==Box office==
Peaches grossed $407,088 at the box office in Australia,. 

==See also==
 
* Australian films of 2004
* Cinema of Australia
* List of films shot in Adelaide
* List of Australian films

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 