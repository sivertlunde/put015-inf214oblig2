Jyeshta (film)
{{Infobox film
| name = Jyeshta
| image =
| caption = 
| director = Suresh Krissna Ranjith
| producer = D. Kamalakar   M. B. Babu Vishnuvardhan   Ashima Bhalla   Devaraj
| music = S. A. Rajkumar
| cinematography = Ramesh Babu
| editing = Shyam Yadav
| studio  = Sri Lakshmi Ganapathi Productions
| released = December 17, 2004
| runtime = 135 minutes
| language = Kannada  
| country = India
}}
 Malayalam film Valliettan (2004), is directed and written by director Suresh Krissna and features soundtrack from S. A. Rajkumar. The original version was directed by Shaji Kailas and featured Mammootty and Shobhana in lead roles. 

Released on 17 December 2004, the film met with average and critical response at the box-office. 

== Cast == Vishnuvardhan 
* Ashima Bhalla 
* Devaraj 
* Aniruddh
* Sindhu Menon in a guest appearance
* Ramesh Bhat Tara
* Avinash
* Rangayana Raghu
* Sharath Lohitashwa
* Shobharaj
* Anand
* Sourav

== Soundtrack ==
All the songs are composed and scored by S. A. Rajkumar. 

{|class="wikitable"
! Sl No !! Song Title !! Singer(s) || Lyrics
|-
| 1 || "Naaleya Nambikeyu" || K. S. Chithra || K. Kalyan
|-
| 2 || "Mannigu Manujanigu" || S. P. Balasubramanyam || K. Kalyan
|-
| 3 || "O Kanchana" || Rajesh Krishnan, K. S. Chithra || K. Kalyan
|-
| 4 || "Nennegala Nenapugale" || Rajesh Krishnan || K. Kalyan
|-
| 5 || "Lolakku Jhumiki" || Karthik (singer)|Karthik, Rajesh Krishnan, Kalpana|| K. Kalyan
|- Srinivas || K. Kalyan
|-
| 7 || "Khalaas Hudugi" || Malathi || K. Kalyan
|}

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 


 