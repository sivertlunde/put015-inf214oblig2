Towards Evening
 
{{Infobox film
| name           = Towards Evening
| image          = Towards Evening.jpeg
| caption        = Film poster
| director       = Francesca Archibugi
| producer       = Guido De Laurentiis Leo Pescarolo
| writer         = Francesca Archibugi Gloria Malatesta Claudia Sbarigia
| starring       = Marcello Mastroianni
| music          = 
| cinematography = Paolo Carnera
| editing        = Roberto Missiroli
| distributor    = 
| released       =  
| runtime        = 99 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}
 Best Supporting the same category.    The film also won the David di Donatello for Best Film and the Nastro dArgento for Best Actor (to Marcello Mastroianni). 

==Cast==
* Marcello Mastroianni as Prof. Bruschi
* Sandrine Bonnaire as Stella
* Zoe Incrocci as Elvira
* Giorgio Tirabassi as Oliviero
* Victor Cavallo as Pippo
* Veronica Lazar as Margherita
* Lara Pranzoni as Papere
* Paolo Panelli as Galliano, the hairdresser
* Giovanna Ralli as Pina
* Gisella Burinato as Stellas mother
* Pupo De Luca as Judge
* Dante Biagioni as Architect

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 