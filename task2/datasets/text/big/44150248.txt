Thennal Thedunna Poovu
{{Infobox film
| name           = Thennal Thedunna Poovu
| image          =
| caption        =
| director       = Rama Arankannal
| producer       = Rama Arankannal
| writer         =
| screenplay     = Rajkumar Balaji Sumithra
| music          = M. S. Viswanathan
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       =  
| country        = India Malayalam
}}
 1984 Cinema Indian Malayalam Malayalam film, Sumithra in lead roles. The film had musical score by M. S. Viswanathan.   

==Cast==
*Revathy Rajkumar
*Balaji Sumithra

==Soundtrack==
The music was composed by M. S. Viswanathan and lyrics was written by Mankombu Gopalakrishnan. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || En manassil || K. J. Yesudas, Vani Jairam || Mankombu Gopalakrishnan || 
|-
| 2 || Hridayathin madhuramadhurageetham || Vani Jairam || Mankombu Gopalakrishnan || 
|-
| 3 || Nin neela nayanangal || K. J. Yesudas, S Janaki || Mankombu Gopalakrishnan || 
|-
| 4 || Oreyoru thottathil || P Susheela || Mankombu Gopalakrishnan || 
|-
| 5 || Phanam virichaal || SP Balasubrahmanyam || Mankombu Gopalakrishnan || 
|}

==References==
 

==External links==
*  

 
 
 

 