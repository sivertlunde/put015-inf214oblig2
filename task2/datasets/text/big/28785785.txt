Beautiful (2009 film)
 
{{Infobox film
| name           = Beautiful
| image          = Beautiful Movie Poster 2009.jpg
| caption        = Original poster
| director       = Dean OFlaherty Kent Smith Kate Butler
| writer         = Dean OFlaherty
| starring       = Deborra-Lee Furness Peta Wilson Sebastian Gregory Tahyna Tozzi Erik Thomson
| music          = Kym Green Bryce Jacobs
| cinematography = Kent Smith
| editing        = Marty Pepper Dale Roberts
| distributor    = Jump Street Films (Australia)
| released       =  
| runtime        = 97 minutes
| country        = Australia
| language       = English
| budget         = A$1,500,000
| gross          = A$56,101
}} 
Beautiful is a 2009 Australian independent film, written and directed by Dean OFlaherty, which was released by Adelaide-based Kojo Pictures on 5 March 2009. The film marked the feature filmmaking debut of both OFlaherty and Kojo Pictures.  The South Australian Film Corporation provided approximately 10 per cent of the $1.5m budget, while the rest came from private investors.  

Beautiful was the first film in Australia to receive the new (at the time) 40% Producers Rebate from the Federal Government.   The film received a poor response, taking only $56,000 at the Australian box office in its short cinema release. 

It was invited to screen at the 12th Shanghai International Film Festival, in June 2010, and later sold "to France and all French-speaking European territories ... Japan ... Poland, Middle East, Russia, Mexico and HBO Eastern Europe." 

==Plot==
In the Adelaide suburb of Sunshine Hills, three teenage girls have disappeared: Jenny Wells (found in a dumpster with her body cut open), Teresa Fields (found impaled and hanging from a clothesline), and Amanda Howatt, who disappeared 3 days earlier.

14-year-old Danny Hobson (Sebastian Gregory) is a loner, who lives with his police officer father Alan (Aaron Jeffery) and stepmother Sherrie (Peta Wilson). He is obsessed with his 17-year-old neighbour, Suzy Thomson (Tahyna Tozzi). They become friends and she tells him about the girls and the mysterious owners of number 46. She sends him to take some photos with his camera. They walk to number 46, where Suzy tells Danny that a woman is always staring out the window. 

While walking down the street, Dannys bouncy ball lands in the backyard of number 46. He introduces himself to the houses owner, a mysterious woman named Jennifer (Asher Keddie), who asks him to leave her alone. At night, he hides in a bush and sees a car approach the house. The driver, Jennifers boyfriend, gets out and stares at Danny, who takes a photo of him.

Danny develops the photos and takes them to Suzy, who recognises the boyfriend from a police newsletter. They read through several newsletters and Suzy identifies the man as Max Forster (Socratis Otto), a convicted rapist. Danny begins to suspect Jennifer is being held against her will in the house by Max. Danny goes to meet Jennifer and she shows him a bracelet which is important to her. He immediately recognises the bracelet as one belonging to his mother, whom he never met. He accuses her of being his mother.

Danny has a dream where Suzy is abducted. He wakes up to learn that she has been kidnapped from her bedroom. Alan interrogates Danny, who says he and Suzy only talked about the girls and number 46. Alan and some fellow officers investigate number 46, but find nothing suspicious. Danny receives a phone call from Suzy, who tells him that Max kidnapped her and that he knows Danny sent the police to his house. Danny must come to the house if he wants to see Suzy alive.

Danny steals Alans gun and goes to the house, where Jennifer takes him to the backroom, where Max is waiting for him. He tells him that he got out of jail and doesnt want to be sent back. He threatens to keep Danny in the house and take pornographic photos of him, but Danny shoots him in the head. Jennifer runs in and Danny asks her why she left. She denies being his mother and during a scuffle, he shoots her. Alan, who suspected Danny of going to the house, arrives and finds the mess. He tells Danny that Jennifer isnt his mother: his mother died when he was a baby, presumably in an accident caused by Alan. He demands that Danny leave. As Danny leaves, Alan shoots himself.

Danny returns home and begins to mourn Alans death with Sherrie. He sees Suzy in her front yard and Sherrie says that Suzy was never kidnapped. He realises she set him up and suspects her of making up the rumours about the girls, therefore causing everything. Danny and Sherrie leave Sunshine Hills forever.

The final scene is narrated by a monologue from Suzys mother (Deborra-Lee Furness), who says that Alan killed Dannys mother. She goes onto mention a rumour that Alan, Max and Jennifer were Satanists and killed the girls as sacrifices. It is revealed that Suzy pretended she was raped in number 46, but refuses to discuss it. The audience realise that most people believe Suzys story, but not the truth. She mentions another rumour about Danny being found dead in a nearby paddock. She says that she only stays in Sunshine Hills because she talks. The movie ends with her saying that she is going to protect Sunshine Hills.

==Cast==
*Deborra-Lee Furness as Mrs Thomson
*Peta Wilson as Sherrie
*Sebastian Gregory as Danny Hobson
*Tahyna Tozzi as Suzy Thomson
*Erik Thomson as Mr Thomson
*Aaron Jeffery as Sergeant Alan Hobson
*Asher Keddie as Jennifer
*Socratis Otto as Max Forster

==Critical reception==

Beautiful divided critics in Australia. 

While praising the "strong visuals" and "strong soundtrack", Fiona Williams of SBS Films said Beautiful was "another disappointing footnote in a submission for better script development in Australian filmmaking. It’s a good idea that’s been undercooked and overdone." 

Jim Schembri of The Age panned the film as a "dreadfully limp thriller". He diagnosed the problem as "the age-old one that bedevils too many Australian films - the movie cannot decide what type of movie it wants to be." 
 Blue Velvet, American Beauty, Happiness and Donnie Darko. He also took issue with its "horribly written dialogue". 

Annette Baille of Filmink Magazine praised the film for being "a truly transportive cinema experience - beautifully photographed, cleverly written and performed with precision - the only thing more intriguing than Beautifuls plot is what its gifted writer/Director will do next." 

David Griffiths of Mediaresearch.com said: "Beautiful is a stunning film that should silence the critics who are ringing the death bell for the Australian film industry". 

==Box Office==
Beautiful grossed $56,101 at the box office in Australia. 

==Trivia==
The iconic lawn chair scene, pictured above, is a homage to Stanley Kubricks Lolita (1962 film)|Lolita. According to footage in the DVD extras, the scene was shot at a house near the corner of Greenwood Grove and Meadowbank Rise ( ), Urrbrae, South Australia.

==See also==
 
* Australian films of 2009
* Cinema of Australia
* List of films shot in Adelaide
* List of Australian films
* South Australian Film Corporation

==References==
 

==External links==
*  
*  


 
 
 
 
 
 
 
 
 
 