Follow Me (film)
{{Infobox film
| name           = Follow Me
| image          = 
| caption        = 
| director       = Maria Knilli
| producer       = 
| writer         = Ulrich Weiß Maria Knilli
| starring       = Pavel Landovský
| music          = 
| cinematography = Klaus Eichhammer
| editing        = 
| distributor    = 
| released       =  
| runtime        = 104 minutes
| country        = West Germany
| language       = German
| budget         = 
}}

Follow Me is a 1989 West German drama film directed by Maria Knilli. It was entered into the 16th Moscow International Film Festival.   

==Cast==
* Pavel Landovský as Professor Pavel Navrátil
* Marina Vlady as Ljuba
* Rudolf Wessely as Frisör
* Ulrich Reinthaller as Milos
* Katharina Thalbach as Judith
* Mark Zak as Soldat Hans Jakob as Geiger
* Sylva Langova as 1. alte Frau
* Hana Maria Pravda as 2. alte Frau
* Renata Olarova as 3. alte Frau

==References==
 

==External links==
*  

 
 
 
 
 
 
 