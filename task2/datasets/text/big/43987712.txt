Yogamullaval
{{Infobox film 
| name           = Yogamullaval
| image          =
| caption        =
| director       = CV Shankar
| producer       = U Parvathibhai
| writer         = CV Shankar
| screenplay     = CV Shankar Baby CV Shankar
| music          = R. K. Shekhar
| cinematography = Chandran
| editing        = K Sankunni
| studio         = UPS Productions
| distributor    = UPS Productions
| released       =  
| country        = India Malayalam
}}
 1971 Cinema Indian Malayalam Malayalam film, Baby and CV Shankar in lead roles. The film had musical score by R. K. Shekhar.   

==Cast==
 
*Muthukulam Raghavan Pillai
*Shobha Baby
*CV Shankar
*T. S. Muthaiah
*Prem Navas
*Abbas
*Bahadoor GK Pillai
*K. P. Ummer Khadeeja
*Meena Meena
*Ramankutty
*Thodupuzha Radhakrishnan
*Vanchiyoor Radha
*Vijayalatha
*Saroja
*Vijayalakshmi
 

==Soundtrack==
The music was composed by R. K. Shekhar and lyrics was written by Sreekumaran Thampi. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Kaattumullapenninoru || LR Eeswari || Sreekumaran Thampi || 
|-
| 2 || Neelasaagara Theeram || S Janaki, SP Balasubrahmanyam || Sreekumaran Thampi || 
|-
| 3 || Omanathaamara poothathaano || M Balamuralikrishna || Sreekumaran Thampi || 
|-
| 4 || Padarnnu Padarnnu || S Janaki, SP Balasubrahmanyam || Sreekumaran Thampi || 
|}

==References==
 

==External links==
*  

 
 
 

 