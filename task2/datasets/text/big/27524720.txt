A Fish in the Bathtub
{{Infobox film
| name           = A Fish in the Bathtub
| image          = AFishInTheBathtub1999Cover.jpg
| image_size     = 
| caption        = DVD Cover
| director       = Joan Micklin Silver
| writer         = 
| starring       = Jerry Stiller
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = March 1999
| runtime        = 96 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
A Fish in the Bathtub is a 1999 comedy film directed by Joan Micklin Silver. It stars real life couple Jerry Stiller and Anne Meara. 

==Plot==
Jerry Stiller and Anne Meara play Sam and Molly, a married husband and wife whose marriage has been stretched to the brink after 40 years of incessant bickering over the smallest of things, not the least of which is Sams inexplicable decision to keep a fish in the bathtub. This, along with a daily harangue from the cantankerous Sam, forces Molly to finally pack a bag and go to son Joels home, which sets the stage for the family to fight through this bump in the road and get life back on track. 

==Cast==
*Jerry Stiller as Sam
*Anne Meara as Molly
*Mark Ruffalo as Joel Jane Adams as Ruthie
*Doris Roberts as Freida
*Louis Zorich as Morris
*Phyllis Newman as Sylvia Rosen 
*Val Avery as Abe
*Bob Dishy as Lou Moskowitz 
*Mordecai Lawner as Bernie
*Jonathan Hogan as Eldon Krantz
*Elizabeth Franz as Bea Greenberg

==Reception==
The film has been generally poorly received. Rotten Tomatoes has given the film a score of 43%.  Contact Music referred to it as "Just not that humorous".  The New York Times criticized the films dialogue, stating that "...the bickering goes too far..."  Killer Movies referred to its plot as "...nothing particularly new or significant or eye-popping..." 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 

 