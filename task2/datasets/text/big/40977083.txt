Anesthesia (film)
{{Infobox film
| name           = Anesthesia
| image          = 
| alt            = 
| caption        = 
| director       = Tim Blake Nelson
| producer       =  
| writer         = Tim Blake Nelson
| starring       = Sam Waterston Glenn Close Kristen Stewart Tim Blake Nelson Gretchen Mol Corey Stoll
| music        = Jeff Danna
| cinematography = Christina Voros
| studio        = Hello Please
| editing        = Mako Kamitsuna
| distributor    = 
| released       =   
| runtime = 90 minutes
| country = USA
| language       = English
| budget         = 
| gross          = 
}}
 independent drama film written, produced and directed by Tim Blake Nelson. Nelson will appear in the film along with Sam Waterston, Kristen Stewart, Glenn Close, Gretchen Mol, and Corey Stoll.

The film premiered at the Tribeca Film Festival on April 22, 2015.     

==Plot==
When Professor Walter Zarrow (Sam Waterston) gets violently mugged, it brings together the lives of people hes affected in the past.

==Cast==
*Sam Waterston  as Prof. Walter Zarrow   
*Tim Blake Nelson as Adam Zarrow  
*Kristen Stewart as Sophie
*Glenn Close  as Marcia Zarrow
*Gretchen Mol
*K. Todd Freeman as Joe
*Mickey Sumner as Nicole
*Rob Morgan as Parnell
*Lee Wilkof as Ray Eisenberg 
*Hannah Marks as Ella Zarrow
*Katie Chang as Amy 
*Corey Stoll 
*Michael K. Williams 
*Gloria Reuben  Scott Cohen as Dr. Laffer
*Yul Vazquez as Dr. Edward Barnes

==Production==
Filming took place in November 2013 in Manhattan.   

==Release==
On March 5, 2015, it was announced that the film had been selected to premiere at the 2015 Tribeca Film Festival.  On the same day the first image of the film was released.  

==Reception==
Anesthesia premiered at the 2015 Tribeca Film Festival to generally positive reviews. Dan Callahan of   avoids sentimentality in a contemporary drama suffused with anger and vitality" and "It is the anger that runs through “Anesthesia” that gives it its flavor, its mood, and its ultimate gravity. This film demands to be taken very seriously, and it earns that right. The woebegone despair that is ever-present in Nelson’s face on screen also suffuses the best of his writing here as well as in his direction of Stewart, with whom he joins forces very dynamically." 

John DeFore of The Hollywood Reporter wrote: "Any viewer entering the film without wanting to hug Waterston will have a crush by the pictures end, with the actor perfectly embodying a flavor of learned humanism that carries us through a couple of more abstractly angst discussions of societys decay." 

David DArcy of Screen International praised the film as well: "This finely acted, tender, drama is one of the surprises of the Tribeca Film Festival" and "No surprise, acting is the film’s most obvious strength. Probably due to a low budget and the many schedules that Nelson had to juggle, the style is remarkably natural." 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 


 
 