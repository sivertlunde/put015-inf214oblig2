Mini (film)
{{Infobox Film
| name           = Mini
| image          = 
| caption        = 
| director       = P. Chandrakumar Madhu
| writer         = Iskantar Mirsa
| based on     = 
| starring       = Aarati Ghanashyam Chandrahasan Kuckoo Parameswaran Babu G. Nair Malini Nair Ramachandran Nair 
| music          = Vishnu Bhatt
| cinematography = Sukumar
| editing        = Madhu Kainakari
| studio         = 
| distributor    =  
| released       =  
| runtime        = 76 minutes
| country        = India
| language       = Malayalam
| budget         =  
| gross          = 
}} Madhu and directed by P. Chandrakumar. The film handles the problem of alcoholism through the determined efforts of a young girl to save her father from self-destruction. It stars Aarati Ghanashyam, Chandrahasan, Kuckoo Parameswaran, Babu G. Nair and Malini Nair in pivotal roles. It won the National Film Award for Best Film on Family Welfare.

==Plot==
Mini is a 10-year old school girl from a middle-class family whose father is a habitual drunkard who beats up his wife as a rule and throws tantrums into the early hours of the morning. The mother and daughter suffer in silence; but the neighbours find the daily antics a nuisance. Despite their vehement protests things go from bad to worse.
 hunger strike. She goes on a hunger strike and when she collapses she is admitted to the hospital. Her father realises his fault and breaks down. He then promises never to touch alcohol again. Minis non-violent approach brings her victory and joy.

==Cast==
* Aarati Ghanashyam as Mini
* Chandrahasan as Minis father
* Kuckoo Parameswaran as Minis mother
* Babu G. Nair
* Malini Nair
* Ramachandran Nair

 
 
 

 