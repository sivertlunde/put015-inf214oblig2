Licántropo
{{Infobox Film
| name           = Licántropo
| image          = Licantropo-film.jpg
| caption        = 
| director       = Francisco Rodríguez Gordillo
| producer       = 
| writer         = Jacinto Molina
| starring       = Paul Naschy, Amparo Muñoz, Antonio Pica
| music          = José Ignacio Cuenca, Tomky de la Pena
| cinematography = Manuel Mateos
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = Spain  Spanish
| budget         = 
}} 1996 Spanish horror film that is the eleventh in a long series about the werewolf Count Waldemar Daninsky, played by Paul Naschy. Following his near-fatal heart attack in 1991, Naschy made this film as an intended comeback, but it was poorly distributed and received bad reviews. 
This misfire was followed by the 12th, and final, film in the series, a 2004 direct-to-video movie entitled Tomb of the Werewolf (filmed in Hollywood).

==Synopsis==
Waldemar Daninsky is an aging writer still suffering from his werewolf curse, and seeking the pure woman that is the only permanent cure. A serial killer pops up in Naschys neighborhood, and the two compete for victims.

==External links==
* 

 

 
 
 
 
 
 


 
 