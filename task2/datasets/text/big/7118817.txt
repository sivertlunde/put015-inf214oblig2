A Summer in St. Tropez
{{Infobox film
| name           = A Summer in St. Tropez
| image          = ASummerInStTropez1983Poster.jpg
| caption        = French Poster David Hamilton
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| starring       = 
| music          = Benoit Wiedemann
| cinematography = Alain Casanova
| editing        = Pauline Leroy
| studio         = 
| distributor    = 
| released       =   
| runtime        = 59 minutes
| country        = France
| language       = None
| budget         = 
| gross          = 
}} David Hamilton. 

==Principal photography==
The film was shot at and around David Hamiltons own house in Saint Tropez, which is 800 years old.

==Summary==
The film contains no dialogue at all, although the characters occasionally laugh and giggle. The soundtrack is the music of Benoit Wiedemann. Stills from the film can be seen in Hamiltons book The Dance. Some shots in the film are taken from his books Sisters and Dreams of a Young Girl.

==Availability==
This film is available on DVD in the UK. 

==Book==
A book of images from the film, A Summer in St. Tropez was released in 1983.

==External links==
* 

==References==
 

 
 
 
 
 
 
 

 