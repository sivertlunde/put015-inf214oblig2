Police Story 2
 
 
{{Infobox film
| name           = Police Story 2
| image          = Police-Story-2-poster.jpg
| caption        = Hong Kong film poster
| film name = {{Film name| traditional    = 警察故事續集
 | simplified     = 警察故事续集
 | pinyin         = Jǐngchá Gùshì Xùjí
 | jyutping       = Ging2 Chaat3 Gu3 Si6 Zuk6 Zaap6}}
| director       = Jackie Chan
| producer       = {{plainlist|
* Raymond Chow
* Leonard Ho
}}
| writer         = {{plainlist|
* Jackie Chan
* Edward Tang
}}
| starring       = {{plainlist|
* Jackie Chan
* Maggie Cheung
* Charlie Cho
}}
| music          = {{plainlist|
* Michael Lai
* Tang Siu Lam
}}
| cinematography = {{plainlist|
* Cheung Yiu Cho
* Danny Lee Yau Tong
}}
| editing        = Peter Cheung
| distributor    = {{plainlist| Golden Harvest Media Asia
}}
| released       =  
| runtime        = 101 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         = 
}} action crime crime comedy Police Story, continuing the storyline of Chans character, "Kevin" Chan Ka-kui.

==Plot==
Inspector Chan Ka-kui has been demoted to highway patrol as the result of his handling of his previous case, which involved the violent arrest of crime lord Chu Tao and heavy property damage. The new duty pleases his girlfriend, May, who is glad that her boyfriend is no longer taking difficult cases and has more time to see her.
 terminally ill with only three months left to live, so he has been released from prison, and while he is still alive he vows to make life difficult for Ka-Kui. John Ko and some henchmen show up at Ka-Kuis apartment and intimidate him, baiting the policeman to attack. Later, May and her mother are beaten by John Ko and his men. Ka-Kui can no longer hold back, and he lashes out against John Ko and his men at a restaurant.

Ashamed of his behavior, Ka-Kui resigns from the Royal Hong Kong Police Force. He plans to take a trip to Bali with May, but while he is at a travel agency in a shopping mall, some police officers see him and report that the mall is under a bomb threat. Unable to resist the urge to get involved in police work, Ka-Kui tells the officers to sound the fire alarm and have the mall cleared, and agrees to take responsibility for the decision. A bomb does indeed explode, and the entire mall is leveled by the blast.

Ka-Kui is praised for his efforts, and he is reinstated and assigned to solve the case of the bombing. Ka-Kui plants a covert listening device in the mall property companys office to try and learn more about the bombers. This leads to a suspect who is a deaf-mute and is a fierce martial artist and explosives expert.

The bombing gang, aware that the police are on to them, plan a simultaneous bombing of the property company and the police headquarters. They double their ransom demand to $20 million and kidnap May, luring Ka-Kui into a trap in which he is strapped with an explosive vest and forced to pick up the extortion money from the property company. However, after picking up the money, Ka-Kui tells the gang that they are being followed and split up. Ka-kui, still holding the ransom, is able to drive his car into a tunnel so that the bomb he is wearing cannot be activated and he strips it off. He then goes to rescue May, who is being held in a warehouse full of fireworks. Ka-Kui again faces the deaf-mute man, who throws small firebombs at him. Ka-Kui then gains the upper hand and brutally retaliates against the mute man, finally throwing him off a third story catwalk onto a pile of plastic drums below. Ka-Kui then rescues May and departs the warehouse, just as it explodes in a huge ball of fire.

==Cast==
* Jackie Chan as Sergeant "Kevin" Chan Ka-kui aka Jackie Chan (New Line Cinema & Fortune Star Media Dubs)
* Maggie Cheung as May
* Chor Yuen as Chu Tu aka Tom Koo (New Line Cinema English Dub)
* Charlie Cho as John Koo aka John Chow (New Line Cinema English Dub)
* Lam Kwok-Hung as Superintendent Raymond Li
* Bill Tung as "Uncle" Bill Wong Mars as Inspector Kim
* Benny Lai as Dummy / Deaf Criminal/ Gabby (New Line Cinema Dub)
* Johnny Cheung as Cheung
* Ben Lam as Tall Pau Hung
* Chi Fai Chan as Ngor
* Shan Kwai as President Fung
* Isabella Wong as Secretary of President Fung
* Ann Mui as Karen

==Release==
This film grossed HK $34,151,609 in Hong Kong. 

===Home media===
The Hong Kong version is 101 minutes long, while the Japanese release is 122 minutes long. The IVL Police Story Trilogy DVD boxed set version is the Japanese cut with Hong Kong blooper footage. Hong Kong-based company Kam & Ronsom Enterprise released the first three Police Story films on Blu-ray Disc in June 2009.
 Police Story and Police Story 2 as a double feature on DVD and Blu-ray Disc on 16 April 2013. 

==Reception==
The film received a rating of 86% on review aggregator Rotten Tomatoes.   Marc Savlov of The Austin Chronicle rated it 3/5 stars and called it "a fine introduction to the Jackie Chan phenomenon" that is less emotionally charged than the first film and less outlandish than the sequel.  TV Guide rated it 4/5 stars and wrote that the film makes up for its lack of story and eye-opening stunts with better pacing and more action.  They concluded that this film "remains among Chans best".   In a review of the Shout! Factory double-bill, Calum Marsh of Slant Magazine rated it 3.5/5 stats and wrote, "This isnt so much exemplary filmmaking as it is bravura stunt work, but Police Story is a veritable case study in the value of the latter." 
 Time Out polled several film critics, directors, actors and stunt actors to list their top action films.  Police Story 2 was listed at 61st place on this list. 

==Awards and nominations==
* 1989 Hong Kong Film Awards
** Won: Best Action Choreography

==See also== The Protector Crime Story
* List of Hong Kong films
* List of Dragon Dynasty releases

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 