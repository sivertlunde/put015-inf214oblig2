English Without Tears
{{Infobox film
| name           = English Without Tears
| image          = 
| image_size     =
| caption        = 
| director       = Harold French
| producer       = Anatole de Grunwald
| writer         = Terence Rattigan Anatole de Grunwald
| starring       = 
| music          = Nicholas Brodszky
| cinematography = Bernard Knowles
| editing        = Alan Jaggs
| studio         = Two Cities Films 
| released       = 28 July 1944
| runtime        = 89 minutes
| country        = United Kingdom
| language       = English
}}
 Michael Wilding, Penelope Dudley-Ward and Lilli Palmer.   It was also released under the title Her Man Gilbey.

==Plot== Michael Wilding), to the point of falling head over heels for him. When the Great War starts, Lady Christabel has to give up her quest for the birds rights, and instead focus on the war at home. The butler, who was quite indifferent to Joans affections for him, finds himself falling in love with her. Lady Christabel gets her hands full sorting out this unwanted love affair, when the affections of the young woman is no longer unrequited. 

==Cast== Michael Wilding ...  Tom Gilbey
* Penelope Dudley-Ward ...  Joan Heseltine
* Lilli Palmer ...  Brigid Knudsen Claude Dauphin ...  François de Freycinet
* Albert Lieven ...  Felix Dembowski
* Peggy Cummins ...  Bobbie Heseltine
* Margaret Rutherford ...  Lady Christabel Beauclerk Martin Miller ...  Schmidt
* Roland Culver ...  Sir Cosmo Brandon
* Paul Demel ...  M. Saladoff
* Beryl Measor ...  Miss Faljambe
* Guy Middleton ...  Captain Standish
* Esma Cannon ...  Queenie
* Ivor Barnard ...  Mr. Quiel
* Paul Bonifas ...  Monsieur Rolland
* Richard Turner ...  Delivery Man
* André Randall ...  Dutch Officer
* Gerard Heinz ...  Polish Officer

==References==
 

==External links==
* 

 
 


 
 
 
 
 
 
 
 
 
 


 
 