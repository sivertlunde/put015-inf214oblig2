The Terrorizers (film)
{{Infobox film
| name           = The Terrorizers
| image          = 
| caption        = 
| director       = Edward Yang
| producer       = 
| writer         = Edward Yang Hsiao Yeh
| narrator       = 
| starring       = Cora Miao Lichun Lee Bao-Ming Gu Shi-Jye Jin
| music          = 
| cinematography = Wei-Han Yang 
| editing        = Ching-song Liao	 
| distributor    = Central Motion Pictures
| released       = 1986
| runtime        = 109 minutes
| country        = Taiwan Mandarin Taiwanese Taiwanese
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
The Terrorizers ( ) is a 1986 film by Taiwanese filmmaker Edward Yang.

==Plot==

 

==Cast==
*Cora Miao as Zhou Yufeng
*Lichun Lee as Li Lizhong
*Shi-Jye Jin as Xiao Shen
*Bao-Ming Gu as Old Gu (A cop)

==Critical reception== Mainlander doctor and his novelist wife; and a young photographer who observes the life of the city unfolding around him, in an echo of the protagonist of Michelangelo Antonionis Blowup.  "Famously characterized by Marxist scholar Fredric Jameson as the postmodern film,  the film was likened by Yang himself to a puzzle where the pleasure lies in rearranging a multitude of relationships between characters, spaces, and genres." 

==Awards and nominations==
*1986 Golden Horse Film Festival
** Won: Best Film
** Nominated: Best Actress – Cora Miao

*1987 Locarno International Film Festival
** Won: Silver Leopard

*1987 British Film Institute Awards
** Won: Sutherland Trophy

*1987 Asia-Pacific Film Festival
** Won: Best Screenplay – Edward Yang, Hsiao Yeh

==References==
 

==External links==
* 

 

 
 
 
 
 


 