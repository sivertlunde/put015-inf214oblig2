Richard III (1955 film)
 
 
{{Infobox film
| name           = Richard III
| image          = Richardiii poster original.jpg
| image_size     =
| caption        = Theatrical re-release poster
| director       = Laurence Olivier
| producer       = Laurence Olivier Alexander Korda (uncredited)
| writer         = Laurence Olivier (uncredited)
| based on       =  
| starring       = Laurence Olivier Ralph Richardson Claire Bloom Cedric Hardwicke John Gielgud Laurence Naismith Norman Wooland
| music          = William Walton
| cinematography = Otto Heller
| editing        = Helga Cranston
| distributor    = London Films
| released       =  
| runtime        = 161 minutes
| language       = English
| country        = United Kingdom
| budget         = pound sterling|£6 million
| gross          = US$2,600,000 (USA) £400,000 (GB)
}} film adaptation historical Richard play of the lead Richard plotting King Edward IV, played by Sir Cedric Hardwicke. In the process, many are killed and betrayed, with Richards evil leading to his own downfall. The prologue of the film states that history without its legends would be "a dry matter indeed", implicitly admitting to the artistic licence that Shakespeare applied to the events of the time.
 Best Picture at the Academy Awards, though Academy Award for Best Actor|Oliviers acting performance was nominated. The film gained popularity through a US re-release in 1966, which broke box office records in many cities. {{cite web
|url=http://www.criterion.com/current/posts/316-richard-iii
|publisher=Criterion
|author=Bruce Eder
|title=Richard III
|accessdate=8 July 2006}}  Many critics now consider Oliviers Richard III his best screen adaptation of Shakespeare. The British Film Institute has pointed out that, given the enormous TV audiences it received when shown in the USA in 1955, the film "may have done more to popularize Shakespeare than any other single work". {{cite web
|url=http://www.screenonline.org.uk/film/id/467017/index.html
|publisher=British Film Institute
|author=Michael Brooke
|title=Richard III (1955)
|accessdate=12 July 2006}} 

==Plot== King Edward hunched back and a withered arm. He goes on to describe his jealousy over his brothers rise to power in contrast to his lowly position.
 her husband and Richard Neville, 16th Earl of Warwick|father, she cannot resist and ends up marrying him.
 Elizabeth (Mary Edward V Duke of York (Andy Shine), to have a protracted stay at the Tower of London.
 The Duke of Buckingham (Sir Ralph Richardson) to alter his public image, and to become popular with the people. In doing so, Richard becomes the peoples first choice to become the new King.

Buckingham had aided Richard on terms of being given the title of Earl of Hereford and the income from the accompanying land grant, but balks at the idea of murdering the two princes. Richard then asks a minor knight, Sir James Tyrrel (Patrick Troughton), eager for advancement, to have young Edward and the Duke of York killed in the Tower of London.  Buckingham, having requested his earldom at Richards coronation, fears for his life when Richard (angry at Buckingham for not killing the princes) shouts "I am not in the giving vein today!"  Buckingham then joins the opposition against Richards rule.
 Bosworth Field. Before the battle, however, Buckingham is captured and executed.

On the eve of the battle, Richard is haunted by the ghosts of all those he has killed in his bloody ascent to the throne, and he wakes screaming. Richard composes himself, striding out to plan the battle for his generals, and gives a motivational speech to his troops.
 Lord Stanley (Laurence Naismith), whose loyalties had been questionable for some time, betrays Richard, and allies himself with Henry. Richard sees this and charges into the thick of battle, side-by-side with his loyal companion Sir William Catesby (Norman Wooland) to kill Richmond and end the battle quickly. Eventually Richard spots Richmond and they briefly engage in a duel before being interrupted by Stanleys men. Richard and Catesby are able to escape the oncoming forces, but, in doing so, Richard is knocked off his horse, loses his cherished crown, and becomes separated from Catesby, who is off seeking rescue. Searching desperately for Richmond, whom he has lost sight of, Richard cries out: "A horse! A horse! My kingdom for a horse!" Catesby finds the king and offers him withdrawal, but Richard refuses to flee. Catesby is then killed by Richmonds men without Richard noticing. Richard then spots Lord Stanley, and engages him in single combat. Before a victor can emerge, the Lancastarian troops charge Richard, and fatally wound him. The wounded murderous king convulses in several spasms and offers his sword to the sky, eventually after doing so Richard dies of his wounds. Stanley orders Richards body to be taken away (discovered 528 years later) and then finds Richards crown in a thorn bush. He then proceeds to offer it to Henry, leaving the crown of England in safe hands once again.

==Cast==
Olivier cast only British actors. Since the film was financed by Alexander Korda and produced by his London Films, obtaining the required actors was not difficult, as many actors were contractually obliged to London Films. As with most films with ensemble casts, all the players were billed on the same tier. However, Olivier played the title character and occupies the majority of screen time and therefore could be considered the lead actor.
 Gielgud and Cedric Hardwicke|Hardwicke) and used this as a selling point.  The four members of the cast who had already achieved British knighthood were all listed as "Sir...." in the film credits.

===The House of York=== Richard III), the malformed brother of the King, who is jealous of his brothers new power, and plans to take it for himself. Olivier had created his interpretation of the Crookback King in 1944, and this film transferred that portrayal to the screen.  This portrayal earned Olivier his fifth Oscar nomination, and is generally considered to be one of his greatest performances; some consider it his best performance in a Shakespeare play.
 King Edward Henry VI of the House of Lancaster. This marked his only appearance in a film version of a Shakespeare play. He was 62 at the time of the film, whereas Edward died at the age of 40.
 1944 adaptation of Henry V. 
 Duke of Buckingham, a corrupt official, who sees potential for advancement in Richards plans and eventually turns against him when Richard ignores his wishes. Richardson was a lifelong friend of Oliviers. At first, Olivier wanted Orson Welles as Buckingham, but felt an obligation towards his longtime friend. (Olivier later regretted this choice, as he felt that Welles would have added an element of conspiracy to the film.) 
 King Edward V), the eldest son of the King, who holds many strong beliefs, and wishes one day to become a Warrior King.
 Duke of York, the younger son of the King.
 Duchess of York, the mother of the King. Haye worked regularly for Alexander Korda. Her characters role is severely cut in the film from the play.
 Pamela Brown Mistress Shore, the Kings mistress. Her character is only mentioned in Shakespeares play, never seen.
 The Lord Hastings (Lord Chamberlain), a companion and friend of Richard who is accused of conspiracy by Richard and is abruptly executed.
 The Lord Stanley. Stanley has a certain dislike for Richard and is not totally willing in his co-operation with him. Stanley eventually betrays Richard at Bosworth and engages him in a one-on-one duel.
 John Phillips Sir William Sir Richard Lord Francis Sir James Tyrrell, and John Howard, 1st Duke of Norfolk|John, Duke of Norfolk, loyal companions of Richard.

===The House of Lancaster=== Queen Elizabeth, Queen Consort of Edward. Kerridge did not make many screen appearances, though she did sometimes work for Alexander Korda. Her role has also been reduced from Shakespeares original.
 The Lord Rivers, brother of the Queen Consort. Morton was a British actor who mainly played supporting roles on screen.
 The Lord Grey, youngest son of the Queen Consort and stepson of the King. Cunninghams role in Richard III was one of his few screen appearances.
 Marquess of Dorset, eldest son of the Queen Consort and stepson of the King.
 The Lady Anne, a widow and an orphan thanks to the acts of Richard, though she cannot resist his charms and eventually becomes his wife.
 Henry VII, first of the House of Tudor). Henry, who is Richards enemy, and Lord Stanleys stepson, claims his right to the throne, and briefly duels Richard at Bosworth.

==Production==

===Background===
  Henry V The Three Little Pigs. {{cite book
 | last = Coleman
 | first = Terry
 | authorlink =
 | coauthors =
 | year = 2005
 | title = Olivier
 | publisher = Henry Hilt and Co
 | location =
 | isbn = 0-8050-7536-4
}}, Chapter 20  Alexander Korda, who had given Olivier his initial roles on film, provided financial support for the film. 

===Screenplay===
Most of the dialogue is taken straight from the play, but Olivier also drew on the 18th century adaptations by Colley Cibber and David Garrick, including Cibbers line, "Off with his head. So much for Buckingham!". Like Cibber and Garrick, Oliviers film opens with material from the last scenes of Henry VI, Part III, to introduce more clearly the situation at the beginning of the story.

A key change in the story involved the seduction of Anne. It is split into two scenes instead of one, and an element of perversity is added—whereas in the original play she is following a coffin with the corpse of her father-in-law, in this film the coffin contains the corpse of her husband. John Cottrell has been quoted as saying this makes "the young widows seduction even more daring and revolting than it is in the original, and giving Annes capitulation" in the second part after a passionate kiss "a new and neurotic twist"     This is accomplished by cutting lines, changing lines, and changing the sequence of some lines.

Olivier makes other small and subtle additions in the stage direction. When Richards nephew makes a joke about his uncles hunchback ("you should bear me on your shoulder"), Richard spins round and gives the boy a malevolent glare making the boy stagger back.  This bit of stage direction is original to Olivier. Olivier also silently mimes some actions spoken of in his soliloquies such as when he whispers insinuations about Clarence into the ear of King Edward.

In general the lengthy play is heavily cut. In an interview with Roger Manvell Olivier discussed how unwieldy and complex the play is.
  Queen Margaret is cut entirely, the role of the Duchess of York (Helen Haye) is significantly reduced, the role of Edward IVs wife Elizabeth is also reduced, and the execution of Clarence and other scenes are abridged.    These cuts were made to maintain the pace of the film and to cut down the running time, as a full performance of the play can run upwards of four hours. Richard is made more directly responsible for the death of Edward IV than in the play, as Edward has his fatal attack only moments after Richard informs the assembled nobles that Clarence is dead.

===Filming===

Gerry OHara was Oliviers assistant director, on hand to help since Olivier was acting in most of the scenes. 

Olivier was very precise in getting many of the visual details of the period correct. Actor Douglas Wilmer (Dorset) recounts that when he casually told Olivier that one piece of heraldry on the set was incorrect, that Olivier started pumping as much information out of him as possible as if he was "drilling for oil". 
 directly addressing the film audience, something not often done before in film. Near the beginning of the film Richards herald drops his coronet, a mistake that Olivier decided to keep in, as part of the motif of accidental loss of the crown continued in the final battle.

Most of the film was shot in at Shepperton Studios, but the climactic Battle of Bosworth Field abruptly opens up the setting, as it was shot outdoors, in the Spanish countryside. During one sequence therein, Olivier suffered an arrow wound to the shin when his horse jerked forwards. Fortunately, it was on the leg Richard was supposed to limp on, allowing the scene to continue. {{Expand section
cite web
|url=http://www.criterion.com/asp/release.asp?id=213&eid=344&section=essay&page=1
|author=Bruce Eder
|publisher=Criterion
|title=Richard III
|accessdate=8 July 2006
}} 

Wilmer also notes  

During filming, Oliviers portrait was painted by Salvador Dalí. The painting remained one of Oliviers favourites until he had to sell it to pay for his childrens school fees. 

===Cinematography===
The cinematography for the film was by Otto Heller, who had worked on many European films before coming to the UK in the early 1940s. The film uses the Technicolor process, which Olivier had earlier rejected for his Hamlet after a row with the company.  The use of Technicolor resulted in bright, vibrant colours. Korda had suggested that Olivier also use the new extreme widescreen format, CinemaScope, but Olivier thought it was nothing more than a gimmick designed to distract the audience from the true quality of the film, and chose the less extreme VistaVision format instead.  To this day, Richard III remains the only Shakespeare film made in VistaVision. 

===Music=== score described audio CD featuring John Gielgud. {{cite web | url=http://www.amazon.com/gp/product/B000000AKM/104-7370788-9975120?v=glance&n=5174 |
title=Walton: Richard III, Macbeth | publisher=amazon.com | accessdate=8 July 2006}}  The Chandos catalogue notes that Walton used the main theme throughout the film, especially towards the closing scenes.

==Reception==
  Leicester Square Queen Elizabeth II and Prince Philip attending the premiere.  Alexander Korda had sold the rights to the film to NBC in the US for $500,000 (about $ }} in todays dollars) and the film was released in the US on Sunday, 11 March 1956. The release was unique in that the film had its US premiere on the same day both on television and in cinemas, the first instance of this ever being done.  It was not shown during prime time, but rather in the afternoon, so prime time ratings for that day were not affected by any pre-emptions for a special program. It is quite likely that it was the first three-hour telecast of a film or a Shakespeare play ever to be shown.

The film, although slightly cut for television, was generally well received by critics, with Oliviers performance earning particular notice, but due to its simultaneous release through television and cinemas in the US, it was a box office failure, and many critics felt at the time that it was not as well-made as Oliviers previous films. However, the airing on US television received excellent ratings, estimated at between 25 and 40 million.    In addition, when the film was reissued in 1966, it broke box office records in many US cities.  Its critical reputation has since grown considerably, and many critics now consider it Oliviers best and most influential screen adaptation of Shakespeare.
 proposed film of Macbeth, which had been intended to go into production during 1957,  in the end finally failed to gain financing.   

===Awards=== The King Best Film Best British David di Donatello Award for Best Foreign Production. The Jussi Award was given to Olivier for Best Foreign Actor.   

Oliviers performance as Richard III was ranked 39th in Premiere (magazine)|Premiere magazines "100 Greatest Performances of All Time"   

==Reputation==
The review aggregator Rotten Tomatoes records that 79% of its collected reviews of the film are positive.    However, the reviewer for the AllMovie website criticises Oliviers direction for being far more restricted in its style in comparison to the bold filming of Henry V, or the moody photography of Hamlet, and Olivier the actor for dominating the production too much {{cite news |
url=http://www.allmovieportal.com/m/1955_Richard_III77.html|
publisher=UGO|
author=Jeffrey M. Anderson  |
title=Richard III Review}}  (although the character of Richard certainly dominates Shakespeares original play). There were some complaints about geographical inaccuracies in the film (the Battle of Bosworth Field was filmed in a region of Spain that does not resemble any locations in England). In response, Olivier wrote in   and the Tower of London to be practically adjacent. I hope theyll agree with me that if they werent like that, they should have been."   

The British film institute suggests Oliviers Richard III may have done more to popularise Shakespeare than any other piece of work.  According to the British Film Institute, the 25–40 million viewers during its airing on US television, "would have outnumbered the sum of the plays theatrical audiences over the 358 years since its first performance." 

==Home media==
The film has been released outside the US on DVD several times, but these releases are copies of the unrestored and cropped film. In 2004,  , production stills and two trailer (film)|trailers. 

==Notes and references==
 

==See also==
 
* 
*Chronology of stage, film and television performances given by Laurence Olivier

==External links==
 
 
*  
*  
*  
*  
*  
*  
*  
*  

 
 
{{Succession box| Best Foreign Language Film
  | after = Woman in a Dressing Gown
  | title = Golden Globe Award for Best English-Language Foreign Film
  | years = 1957
|}}
 

 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 