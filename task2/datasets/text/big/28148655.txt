Grapes (film)
 
{{Infobox film
| name           = Grapes
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Tomás Barina
| producer       = Adam Dvorák
| writer         = 
| narrator       = 
| starring       = Krystof Hádek
| music          = 
| cinematography = Martin Preiss
| editing        = Adam Dvorák
| studio         = 
| distributor    = 
| released       = 27 March 2008
| runtime        = 90 minutes
| country        = Czech Republic
| language       = Czech
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}} Czech comedy film directed by Tomás Barina. It was released in 2008.

==Cast==
* Krystof Hádek - Honza
* Lukas Langmajer - Jirka
* Lubomír Lipský - Deda Adámek
* Tereza Vorísková - Klárka
* Václav Postránecký - Michalica
* Marian Roden - Frantisek
* Miroslav Táborský - Kozderka
* Tomás Matonoha - Kája
* Lucie Benesová - Markétka
* David Strnad - Malý Honzík
* Robert Jaskow - Mácha (as Robert Jasków)
* Ctirad Götz - Poslanec Boucek
* Jirí Bábek - Prazsky policajt
* Kamil Svejda - Prazsky policajt
* Martin Sitta - Jozífek

==External links==
*  

 
 
 
 