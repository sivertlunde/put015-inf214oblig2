Sunset Limousine
{{Infobox film
| name           = Sunset Limousine
| image          = Sunset Limousine VHS cover.jpg
| alt            = 
| caption        = Sunset Limousine VHS cover
| film name      =  Terry Hughes
| producer       =  
| writer         =  
| screenplay     = 
| story          = 
| based on       =     
| starring       =  
| narrator       = 
| music          =  
| cinematography = Dennis Dalzell
| editing        = Michael J. Lynch
| production companies =  
| distributor    = Columbia Broadcasting System
| released       =  
| runtime        = 92 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 American comedy comedy television Terry Hughes Audrie Neenan, Martin Short, and George Kirby in a story about a limo driver who aspires to be a stand-up comic.  
 Witzend Productions signed to produce the project for CBS.     Filming took place during May 1983 with Lainie Kazan,    Martin Short, and Audie Neenan joining the cast that month.     The film debuted October 12, 1983 on CBS.     

==Plot==
Alan OBlack (John Ritter) is an aspiring stand-up comic who takes on a job as limousine driver in order to prove to his girlfriend Julie (Susan Dey) after she has kicked him out of their shared home, that he can be a responsible adult. Her standing complaint about Alan as a boyfriend has been that he sees life as one long rehearsal. This is exacerbated by the fact that, even with a now-steady job and dealing with strange passenger/clients, Alan rehearses his comedy at every opportunity and deals with bizarre situations with good-natured aplomb. He and his buddy Jay (Paul Reiser) become involved the shady dealings of businessman Bradley Coleman (Martin Short), which results in a chase through Los Angeles with both sides of the law in pursuit.

==Principle cast==
 
* John Ritter as Alan OBlack
* Susan Dey as Julie Preston
* Paul Reiser as Jay Neilson Audrie Neenan as Karen 
* Martin Short as Bradley Z. Coleman
* George Kirby as Elmer
* James Luisi as Angel
* Louise Sorel as Dolores Chase
* Lainie Kazan as Jessie Durning
* Michael Ensign as Gavrik Charles Lane as Reinhammer
* Stacey Nelkin as Stacey
* John Snee as Steven
* Darrell Zwerling as Janczyn
* Tom Dreesen as Comic Himself
* Eleanor Mondale as Secretary
* Martin Mull as Mel Shaver
* Joyce Little as Lauren
* Dick Patterson as Howard Chase
 

==Reception== People Magazine bemoaned John Ritters comedy ability being "drowned out" within the "cruder context of Threes Company", and wrote that his ability "shines in this fanciful TV-movie."  It was expanded that the films story line is silly, but as Ritters character "gamely rehearses his   act through it all", he shows himself as "delightful."   

The Age wrote that Sunset Limousine stood out from most American television comedies because it actually was funny.  In praising star John Ritter, it was offered that his timing and charm made a story that was otherwise rubbish into something entertaining.  As the story progresses Ritters character "handles the most impossible situations with politeness and good humor," becoming "increasingly endearing."   

Ocala Star-Banner praised the film, writing the films "magic ingredient is John Ritter," and that "Ritters style is what makes Sunset Limousine a welcome bit of light entertainment."   

Pittsburgh Post-Gazette panned the film, offering that CBS reliance on names over substance could not keep the film from being silly, and that even appearances by Lannie Kazan and George Kirby could not save prevent the film from being mindless.   

==References==
 

==External links==
* 
* .


 
 
 
 
 
 
 
 
 