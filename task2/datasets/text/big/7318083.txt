Enjō
{{Infobox Film
| name           = Enjo
| image          =
| image_size     = 
| caption        = Theatrical release poster
| director       = Kon Ichikawa
| producer       = Masaichi Nagata
| writer         = Kon Ichikawa Keiji Hasebe Natto Wada  Yukio Mishima (novel)
| narrator       = 
| starring       = Raizo Ichikawa Ganjiro Nakamura Tatsuya Nakadai
| music          = Toshiro Mayuzumi
| cinematography = Kazuo Miyagawa
| editing        = Shigeo Nishida
| distributor    = Daiei Film
| released       = August 19, 1958
| runtime        = 99 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 1958 Cinema Japanese film directed by Kon Ichikawa and adapted from the Yukio Mishima novel The Temple of the Golden Pavilion. It stands as one of his better known films.  Also known as Conflagration, the film has come to be best known by its Japanese title. 

==Synopsis==
 flashback structure, Enjo dramatizes the psychological collapse of Goichi (Raizo Ichikawa), a young Buddhist acolyte from a dysfunctional family who arrives at a Kyoto temple - the Golden Pavilion - for further study.

Goichi is haunted by two events - the discovery of his psychologically abusive mothers infidelity, and the effect of the revelation upon his father, who suddenly falls ill and dies shortly thereafter.  Shy and idealistic - and hindered by a stuttering problem - Goichi arrives at the temple haunted by his dying fathers sentiment that "the Golden Pavilion of the Shukaku Temple is the most beautiful thing in the world." 

In the wake of entering into his studies, Goichi is visited by his now-widowed mother, who unexpectedly states her wish that he strive to succeed in his studies, so that he might one day become the head priest at the temple.  Under unexpected pressure from his irresponsible surviving parent, Goichi then must face a challenge to his own ideals upon discovery of the head priests greed (the temple is being run as a tourist attraction, though an appearance of piety must be presented to outsiders) and his indiscreet pairings with a local geisha.

A flashback (one of many within the entire films greater structure) to the funeral of Goichis father introduces the idea of a cleansing inferno; with an escalating sense of desperation, Goichi sets fire to the pavilion. He is subsequently repudiated by his mother, and ultimately commits suicide before he can be taken to prison.

<!-- 
==Production==
==Response==
==Themes== 
-->

==Notes==
 

==References==
* Mellen, Joan.  The Waves At Genjis Door: Japan Through Its Cinema, 1976.  Pantheon, New York.  ISBN 0-394-49799-6
* Quandt, James.  Kon Ichikawa, 1982.  Cinematheque, New York.  ISBN 0-9682969-3-9
* Richie, Donald.  A Hundred Years of Japanese Cinema, 2001.  Kodansha America, New York & Tokyo.  ISBN 4-7700-2995-0
* Svensson, Arne.  Japan (Screen Series), 1971.  A.S. Barnes, New York.  ISBN 0-498-07654-7

==External links==
*  
*     at the Japanese Movie Database

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 


 