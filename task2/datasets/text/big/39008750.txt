Rangeelay
{{multiple issues|
 
 
}}
{{Infobox film
| name = Rangeelay
| image = Official movie poster for Rangeelay.jpg
| image_size =
| caption = 
| director = Navaniat Singh
| producer = Sunil Lulla Jimmy Shergill
| story = Dheeraj Rattan
| narrator =
| starring = Jimmy Shergill Neha Dhupia
| music = Jaidev Kumar
| cinematography = Yashaswi Bhargava
| editing = 
| distributor = 
| released = May 16, 2013
| runtime = 130 minutes
| country = India
| language = Punjabi
| budget = 
| gross =
}}

Rangeelay is a 2013 Punjabi movie directed by Navaniat Singh, featuring Jimmy Shergill and Neha Dhupia in the lead roles. The movie is produced by Jimmy Shergill in association with Eros International, and it marks Neha Dhupias debut in Punjabi cinema. The movie was released on May 16, 2013. The film opened to average opening in India and poor opening overseas. Rangeelay was also a critical failure. 

After Dharti(2011) and Taur Mittran Di(2012), this film is the third production of Jimmy Sheirgill Productions.   

==Cast==
*Jimmy Shergill as Sunny
*Neha Dhupia as Simmi
*Diljit Singh as dj
*Pankesh Mann as Maan
*Jaswinder Bhalla as DSP
*Shivinder Mahal as Major Saab
*Binnu Dhillon as Titli
*Rana Ranbir as Shotgun
* Jassi Kaur as Jassi

==Songs==
*Rangeelay Title Song Babbal Rai
*Headache Mika Singh
*Tere Bina Din Mere	Feroz Khan
*Yaara Tu Ashim Kemson and Shipra Goyal
*Dil De Kutte Jashan Singh
* Boloyaan Nishawn Bhullar and Simran-Tripat
*Dil De Kutte Sonu Nigam

Bollywood celebrity Neha Dhupia made her debut in Punjabi films with Rangeelay.

==References==
 

==External links==
*  
* http://www.dodear.com/index.php/en/movie/14705

 
 
 


 