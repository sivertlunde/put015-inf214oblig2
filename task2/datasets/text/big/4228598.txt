The Devil Thumbs a Ride
{{Infobox film
| name           = The Devil Thumbs a Ride
| image          = The Devil Thumbs a ride DVD cover.jpg
| alt            =
| caption        = Theatrical release poster
| director       = Felix E. Feist
| producer       = Herman Schlom
| screenplay     = Felix E. Feist
| based on       =   
| starring       = Lawrence Tierney Ted North Nan Leslie
| cinematography = J. Roy Hunt
| music          = Paul Sawtell
| editing        = Robert Swink
| distributor    = RKO Radio Pictures 
| released       =  
| runtime        = 62 minutes
| language       = English
| budget         =
| gross          =
}}
The Devil Thumbs a Ride is a 1947 film noir directed by Felix E. Feist and featuring Lawrence Tierney and Ted North. 

==Plot== sociopath who has just robbed and killed a cinema cashier.  Seeking to escape, he hitches a ride to Los Angeles with unsuspecting Jimmy Fergie Ferguson (North). Part way the pair stops at a gas station and picks up two women. Encountering a roadblock, Morgan persuades the party to spend the night at an unoccupied beach house. The police close in as one by one Morgan begins killing the threesome.

==Cast==
* Lawrence Tierney as Steve Morgan
* Ted North as Jimmy "Fergie" Ferguson
* Nan Leslie as Beulah Zorn, alias Carol Demming
* Betty Lawford as Agnes Smith
* Andrew Tombes as Joe Brayden, Night Watchman Harry Shannon as Detective Owens, San Diego Police
* Glen Vernon as Jack Kenny, Gas Station Attendant
* Marian Carr as Diane Ferguson William Gould as Police Capt. Martin, San Diego Police
* Josephine Whittell as Dianes mother
* Phil Warren as Pete, Roadblock Motorcycle Cop
* Robert Malcolm as Deputy Sheriff Grover

==Reception==

===Critical response===
When the film was released The New York Times film critic dismissed the film, "The Devil Thumbs a Ride, which came to the Rialto yesterday, is a distinctly pick-up affair ... In the role of the thug Lawrence Tierney, who played Dillinger a couple of years back, behaves with the customary arrogance of all gunmen in cheap Hollywood films. It is pictures like this which give the movies a black eye and give us a pain in the neck." 
 The Threat) directs and writes this ugly hitchhiker crime drama that has little entertainment value, the characters other than the main protagonist are too incredibly dull to ring true and it has no redeeming social value. The low-budget programmer is helped only by its noir look, fast-pace, the manic performance by Lawrence Tierney and the offbeat nature of its story ... Feist fills both the police car and the hitchers car with noir characters, but it ends up as a ride to nowhere. 

==See also==
* Detour (1945 film)|Detour (1945)
* The Hitch-Hiker (1953) The Desperate Hours (1955)

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 