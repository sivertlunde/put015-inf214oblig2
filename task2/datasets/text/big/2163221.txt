Sarfarosh
 
 
 
 
{{Infobox film
| name           = Sarfarosh
| image          = Sarfarosh_(Poster).jpg
| caption        = Theatrical release poster
| director       = John Matthew Matthan
| producer       = John Matthew Matthan
| writer         = John Matthew Matthan
| narrator       =
| starring       = Aamir Khan Naseeruddin Shah Sonali Bendre Shri Vallabh Vyas Mukesh Rishi
| music          = Songs:  
| cinematography = Vikas Sivaraman
| editing        = Jethu Mundul
| studio         = Cinematt Pictures Eros Entertainment
| released       =  
| runtime        = 129 minutes
| country        = India Hindi
| budget         = 
| gross          = 
}}
 Kannada as Telugu as Astram (2006) with Vishnu Manchu and Anushka Shetty.

==Plot==
Arms trade is taking place in India. An arsenal enters Indian territory via the state of Rajasthan, which makes its way to interior of India with help of many middlemen. Bala Thakur, a gun handler in Chandrapur, provides the arms to Veeran, a forest dwelling brigand. Later, Veeran and his gang attack a wedding bus, mercilessly gunning down every person in it which includes women and children. The government appoints a Special Action Team in Mumbai to trace the roots behind the attack. The team learns about Bala Thakur, but thanks to his informants, Thakur flees before he can be apprehended.

Meanwhile, ACP Ajay Singh Rathod, a resident of Mumbai, is attending a concert by the famed ghazal singer Gulfam Hassan (a Muhajir people|Muhajir), where he spots a familiar face, Seema. Ajay had a crush over Seema when they were studying in Delhi, but never got the courage to propose her. The duo are happy to meet each other again and gradually fall in love. Gulfam, on the other hand, is Indian by birth, but had to move to Pakistan as a child during partition. Deeply scarred due to the experience, he is still happy that the Government allows him to live in his palatial residence whenever he comes to India. Gulfam finds a huge fan in Ajay, who used to attend his programs as a child. Despite the age difference, the two bond.

Inspector Salim a Muslim inspector is taken off from the Special Action Team when a notorious gangster Sultan escapes his clutches and he is severely reprimanded his superior for this failure and for cuing the death of two other officers in the attempt. Despite being an honest and upright police officer with the best intelligence gathering network in the force, Salim is rueful that he is being given second class treatment because he is a Muslim and is being perceived as having let Sultan escape because he was a Muslim as well. His anger at the system doesnt lessen when Ajay, who was his junior, is told to head the team. Ajay wants Salim on the team, but Salim refuses. It is revealed that Ajays father was going to testify against some terrorist who had threatened him not to testify, but in an attempt to stop them from doing so, Ajays elder brother was killed. Ajays father was kidnapped and by the time the terrorists spared him, the man had lost his voice. This motivated Ajay to join the police force.

Unknown to Ajay, Gulfam is also working for the Pakistani intelligence which is attempting to create havoc in India and create a proxy war between both sides. Since Gulfam likes Ajay, he sees to it that nothing untoward happens to him. Here, Salim finds the location of Bala Thakur along with the information that Sultan, the man who escaped from his clutches, will also be there. Salim gives the information to Ajay who makes peace with Salim and convinces him to join the team again. An encounter at the criminals rendezvous results in the death of Bala Thakur and Ajay is seriously injured. Though Sultan and his right-hand man Shiva manage to escape, the operation is deemed a success as the team is able to intercept a large consignment of lethal arms and ammunition meant for terrorist Veeran and his terrorist acts around the country. Sultan is later assassinated on Gulfams orders because of his failure and false information is leaked that he has escaped to Pakistan.

While recuperating, Ajay has an epiphany which leads the team to Bahid in Rajasthan and to "Mirchi Seth" Rambandhu Gupt. The investigative team save for Ajay and Salim camp in Bahid and gather all possible information on Mirchi Seth. Ajay comes over to Bahid to investigate and meets Gulfam who is temporarily staying in his ancestral manor near Bahid. Gulfam attempts to derail the investigation through political means and ordering an assault on Ajay to subvert the investigation, but of no avail. The repeated failures on Gulfams part displease the senior officers in Pakistani Intelligence, who dispatch Major Aslam Baig to take care of the business.

Here, Ajay goes through the information collected so far and realises that he has seen many key suspects near Gulfam, but fails to make the connection. The investigative teams prepares for a final assault on the gun-running operation and in the pursuit of Mirchi Seth land up at Gulfams mansion. Ajay feels betrayed when he learns of Gulfams treachery, but is aware of lack of substantial evidence to indict Gulfam for his crimes. He goads and tricks Gulfam into killing Baig and arrests him for the offence. After realising what happened, Gulfam reveals how the incidents that happened during the partition embittered him and why he did those deeds.

In retaliation, Ajay makes him realise that his actions are not benefiting people of any religion. After Ajay makes him see the error in his ways, Gulfam, unable to stand the humiliation and guilt, commits suicide. Gulfams suicide is hushed up and the team returns triumphant to Mumbai to much accolades for busting the terrorist racket. At Mumbai Airport, Salim is tipped on the whereabouts of Veeran and Ajay embarks on another investigation with his team.

==Cast==
* Aamir Khan as A.C.P. Ajay Singh Rathod 
* Naseeruddin Shah as Gulfam Hassan
* Sonali Bendre as Seema
* Vallabh Vyas as Major Aslam Baig
* Dinesh Kaushik as Roshan, Seemas brother
* Mukesh Rishi as Inspector Saleem Pradeep Rawat as Sultan
* Govind Namdeo as Veeran
* Akhilendra Mishra as Rambandhu Gupt aka Mirchi Seth
* Makrand Deshpande as Shiva Rajesh Joshi as Bala Thakur
* Salim Shah as Inspector Yadav Manoj Joshi as SI Bajju
* Smita Jaykar as Ajays mother
* Akash Khurana as Ajays father Ahmed Khan as Haji Seth
* Nawazuddin Siddiqui as Criminal / arrested through victoria house raid
* Surekha Sikri as Sultans Mother
* Upasana Singh as Mala
* Ashok Lokhande as Chandrapur Hawaldar
* Sunil Shende as D.C.P.
* Sunil Rege as Minister
* Rajendra Mehra as Ajays Kaka
* Sukanya Kulkarni as Ajays Bhabhi
* Varun Vardhan as Ajays Brother
* Ayush Morarka as Chintu Ali Khan as Shafi
* Khodus Wadia as I.S.I. General
* Anil Upadhyay as Fatka
* Ramdas Jadhav as Chandrapur Inspector

==Critical Reception==
Sarfarosh was praised by the critics. Naseeruddin Shah was specially praised for his role of a Muhajir people|Muhajir(Muslim Indians who migrated to Pakistan after the Partition of India).   Aamir was also praised for the role of ACP.

==Accolades==
===Won===
* Filmfare Critics Award for Best Movie – Cinematt Pictures – John Matthew Matthan
* Filmfare Award for Best Editing – Jethu Mundul
* Filmfare Award for Best Dialogues - Pathik Vats & Hriday Lani.
* National Film Award for Best Popular Film Providing Wholesome Entertainment

===Nominated===
* Filmfare Award for Best Film – Cinematt Pictures – John Matthew Matthan
* Filmfare Award for Best Director – John Matthew Matthan
* Filmfare Award for Best Supporting Actor – Mukesh Rishi

==Soundtrack==
{{Infobox album |  
 Name = Sarfarosh
| Type = Album
| Artist = Jatin Lalit
| Cover =
| Released = 1999
| Recorded = Feature film soundtrack
| Length = 30:47 Tips
| Producer = Jatin Lalit
| Last album = Sangharsh (1999 film)|Sangharsh (1999)
| This album = Sarfarosh (1999)
| Next album = Silsila Hai Pyar Ka (1999)
}}

The films music was composed by Jatin Lalit. Lyrics are penned by Israr Ansari, Nida Fazli, Sameer, and Indeevar.

===Track listing===
{{Track listing
| extra_column = Singer(s) Sameer
| all_music = Jatin Lalit
| lyrics_credits = yes
| title1 = Hoshwalon Ko Khabar Kya
| extra1 = Jagjit Singh
| lyrics1 = Nida Fazli
| length1 = 05:02
| title2 = Is Deewane Ladke Ko
| extra2 = Alka Yagnik, Aamir Khan Sameer
| length2 = 04:40
| title3 = Jo Haal Dil Ka
| extra3 = Kumar Sanu, Alka Yagnik
| lyrics3 = Sameer
| length3 = 05:26
| title4 = Meri Raaton Ki Neendein Uda De
| extra4 = Alka Yagnik
| lyrics4 = Indeevar
| length4 = 04:37
| title5 = Yeh Jawani Hadh Kar De
| extra5 = Kavita Krishnamurthy
| lyrics5 = Sameer
| length5 = 04:44
| title6 = Zindagi Maut Na Ban Jaye
| extra6 = Roop Kumar Rathod, Sonu Nigam
| lyrics6 = Israr Ansari
| length6 = 06:18
}}

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 