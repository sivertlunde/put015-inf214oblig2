Scooby-Doo! Stage Fright
{{Infobox film name = Scooby-Doo! Stage Fright image = Scooby-Doo! Stage Fright.jpg caption = DVD cover director = Victor Cook	 	 producer = {{Plainlist|
*Victor Cook
*Alan Burnett}} writer = Doug Langdale Candie Langdale starring = Frank Welker Matthew Lillard Grey DeLisle Mindy Cohn Troy Baker Wayne Brady Isabella Acres music = Robert J. Kral edtior = Vincent Guisetti
| studio   = Warner Bros. Animation distributor = Warner Premiere released =   runtime = 80 minutes country = United States language = English
}}
Scooby-Doo! Stage Fright is the twentieth film in the direct-to-video series of Scooby-Doo films. It was released on August 20, 2013 by Warner Premiere.   

==Plot==
The Mystery Inc. crew head to Chicago for a talent show, called Talent Star, hosted by Brick Pimiento, where songwriting duo Fred and Daphne are finalists with some high hopes. Upon arrival, they learn that the opera house which the show will be held in is being terrorized by a Erik (The Phantom of the Opera)|Phantom, who is intensely lauding one of the finalists to win, Christine, a spoiled girl whose noisy parents are no more polite than her. Fred and Daphne also befriend one of the finalists, Emma Gale, a violinist. Not to be left out, Scooby and Shaggy decide to show Pimiento a juggling act, which they are betting will take the contest by storm. However, after Pimiento states his belief that ‘juggling stinks, throughout the film, the two continually approach Pimiento and show him numerous terrible impromptu acts.

Not long after checking in for the talent show, the Phantom appears in the opera house and Fred, Daphne and Velma attempt to catch him using the surveillance cameras, but are unsuccessful due to the Phantom’s ability to seemingly appear in multiple places at once. While being chased briefly by the Phantom, Shaggy and Scooby notice the Phantom had a strange lemon scent and afterwards, the gang split up to search for clues. Fred and Daphne meet the owner of the opera house, Mel Richmond. They learn from Richmond that a Phantom once terrorized the opera house thirty-five years ago when it was a disco. 

The dress rehearsal commences the next day. Fred and Daphne meet Emma’s parents, who are hoping to get the prize money to save their home from bankruptcy. During the dress rehearsal, the Phantom sabotages most of the finalists’ acts,  leaving Christine, Emma, and Fred and Daphne as the only contestants left. Due to these attacks, Fred and Daphne decide to use themselves as bait to lure the Phantom out. The gang manage to track down a Phantom, only to find it is the original Phantom from the 70’s, a man named Steve Trilby. Steve admits to the gang that he vandalized the opera house during the 70’s, due to his hatred over disco music, but that now he only goes into the opera house to get food. The gang return to the stage to find the Phantom setting fire to the opera house. With the help of Steve, they catch the Phantom, who is revealed to be Mel Richmond. However, the gang learn that Richmond isnt the Phantom they are after when they hear the Phantom threatening to destroy the place unless Christine wins.

Later that night, using Emma as bait, the gang manage to capture the Phantom, who turns out to be Christine’s father, Lance. Afterwards, however, Shaggy and Scooby find out that Ottoman is also the Phantom, when they see him putting on lemon-scented hand sanitizer. They rush to Ottoman’s office and find a magazine about the Soap Diamond, which is on display at a mineralogical center nearby. They quickly realize Ottoman ordered all the police to patrol the opera house so that the mineralogical center would be unguarded. They rush to the mineralogical center and find Ottoman running out of the building. Ottoman trips and drops the Soap Diamond, which Scooby catches. Ottoman pursues the gang toward a drawbridge, where Fred tricks him into jumping into a barge filled with garbage, by replacing the diamond with a dog bone. Ottoman is arrested and the gang goes back to the opera house, after being informed that Fred and Daphne are doing a tie-breaking performance against Emma.

The gang arrives back in time for Fred and Daphne to do their tie-breaker performance. The performance takes them in the lead of the voting chart, however, when they realize what it would mean if Emma lost, they decide to tell some of Shaggy and Scooby’s cheesy jokes, which enables them to lose enough votes, allowing Emma to win. At the end of the show, Velma plays the footage which shows Pimiento putting on the Phantom costume. Pimiento admits that he used the Phantom to boost the show’s ratings. After Pimento is taken away, Steve wraps up the show.

==Voice Cast== Fred Jones
* Matthew Lillard as Shaggy Rogers
* Grey DeLisle as Daphne Blake
* Mindy Cohn as Velma Dinkley
* Isabella Acres as Emma Gale
* Troy Baker as Erik (The Phantom of the Opera)|Phantom, Lance Damon
* Eric Bauza as K.J.
* Jeff Bennett as Mike Gale, Mel Richmond
* Wayne Brady as Brick Pimiento
* Vivica A. Fox as Lotte Lavoie
* Kate Higgins as Meg Gale, Cathy
* Peter MacNicol as Dewey Ottoman
* Candi Milo as Barb Damon
* John OHurley as The Great Pauldini
* Cristina Pucelli as Colette Security Guard #1, Hotel Clerk
* Paul Rugg as Steve Trilby
* Tara Sands as Nancy News Anchor
* Travis Willingham as Waldo
* Ariel Winter as Chrissy Damon

==Critical response==
DVD Verdict offered that the plot was an unoriginal "rip off rip-off of the classic The Phantom of the Opera" mixed with "overtones of American Idol" as well as other "reality based music shows". They also offered that while Blu-ray quality gives the film a ghoulish quality, it would be scary to only the youngest viewers. They noted Frank Welker as the voice of Fred, being the only holdover from the original series, and Matthew Lillards return as the voice of Shaggy. They also offered that Mindy Cohn "fits in well as nerdy Velma " and that Vivica A. Fox, Peter MacNicol, and Wayne Brady in supporting roles "make the film slightly more interesting for adults who want to play the is that so-and-sos voice? game."  They concluded that for children, the film "will probably be a lot of fun and somewhat thrilling", due to the car and foot chases and its various adventures, but that "adults will find it to be a bit tedious, but if that surprises you, clearly you havent spent much time around childrens entertainment."   

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 