Bangistan
 
 
{{Infobox film
| name           = Bangistan 
| image          = Bangistan First Look.jpg
| caption        = First Look
| director       = Karan Ashuman
| producer       = Ritesh Sidhwani and Farhan Akhtar
| starring       = Riteish Deshmukh Pulkit Samrat Chandan Roy Sanyal
| screenplay     = Puneet Krishna Sumit Purohit Karan Anshuman
| music          = Ram Sampath
| cinematography = Szymon Lenkowski
| studio         = Excel Entertainment
| Distributor    = AA Films
| released       =  
| country        = India
| language       = Hindi
}}
Bangistan is an upcoming Bollywood comedy film starring Riteish Deshmukh and Pulkit Samrat.  The film will be directed by Karan Anshuman. Ritesh Sidhwani and Farhan Akhtar will produce it. The film is scheduled to release on 31 July 2015.  

== Plot ==
The film follows the lives of two blundering terrorists on a mission to change the world.
Riteish Deshmukh and Pulkit Samrat play the young guns with lofty ideologies but ordinary talent who keep pace with a series of serendipitous high-energy, fast-paced, and exceptionally entertaining events that has a huge impact on world peace. 

== Cast ==
* Riteish Deshmukh  as Pulkit Pareek / Mitesh Sharma
* Pulkit Samrat as Mitesh Pareek / Pulkit Sharma
* Chandan Roy Sanyal Thakur Baldev Rao
* Cezary Pazura as Dancer
* Tomasz Karolak as Fredrick Dcosta
* Aakash Pandey as Trilok Nath Tripathi (Chief Minister)
* Arya Babbar as Gabbar Rao (Antagonist)
* Jacqueline Fernandez (Cameo)

== References ==
 

== External links ==
*  

 
 
 
 


 