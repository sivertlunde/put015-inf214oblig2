Lankadahanam
{{Infobox film 
| name           = Lankadahanam
| image          =
| caption        =
| director       = J. Sasikumar
| producer       = KP Kottarakkara
| writer         = KP Kottarakkara
| screenplay     = KP Kottarakkara
| starring       = Prem Nazir Adoor Bhasi Jose Prakash Sankaradi
| music          = M. S. Viswanathan
| cinematography = JG Vijayam
| editing        = K Sankunni
| studio         = Ganesh Pictures
| distributor    = Ganesh Pictures
| released       =  
| country        = India Malayalam
}}
 1971 Cinema Indian Malayalam Malayalam film, directed by J. Sasikumar and produced by KP Kottarakkara. The film stars Prem Nazir, Adoor Bhasi, Jose Prakash and Sankaradi in lead roles. The film had musical score by M. S. Viswanathan.   

==Cast==
 
*Prem Nazir
*Adoor Bhasi as Mathayi
*Jose Prakash
*Sankaradi as Adiyodi
*Sreelatha Namboothiri
*Friend Ramaswamy
*K. P. Ummer Khadeeja
*N. Govindankutty Ragini
*Vijayasree
 

==Soundtrack==
The music was composed by M. S. Viswanathan and lyrics was written by Sreekumaran Thampi. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Eeshwaranorikkal || K. J. Yesudas || Sreekumaran Thampi || 
|-
| 2 || Kilukile Chirikkum || LR Eeswari || Sreekumaran Thampi || 
|-
| 3 || Nakshathra Raajyathe || K. J. Yesudas || Sreekumaran Thampi || 
|-
| 4 || Panchavadiyile || P Jayachandran || Sreekumaran Thampi || 
|-
| 5 || Sooryanennoru Nakshathram || K. J. Yesudas || Sreekumaran Thampi || 
|-
| 6 || Swarganandini || K. J. Yesudas || Sreekumaran Thampi || 
|-
| 7 || Thiruvaabharanam Chaarthi || P Jayachandran, Chorus || Sreekumaran Thampi || 
|}

==References==
 

==External links==
*  

 
 
 

 