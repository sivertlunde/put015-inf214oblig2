Unnudan
{{Infobox film
| name           = Unnudan
| image          = 
| image_size     =
| caption        = 
| director       = R. Balu
| producer       = Aroma Mani
| writer         = R. Balu
| screenplay     =  Murali Kausalya Kausalya  Manivannan Deva
| cinematography = Thangar Bachan 
| editing        = B. Lenin V. T. Vijayan
| distributor    = 
| studio         = 
| released       = 18 October 1998
| runtime        =
| country        = India
| language       = Tamil
| budget         =
| preceded_by    =
| followed_by    =
| website        =
}}
 1998 Tamil Murali alongside Vivek and Manivannan play supporting roles. The project saw the director and cast come together after the success of their previous film Kaalamellam Kadhal Vaazhga (1997).  The film, which had music composed by Deva (music director)|Deva, opened in October 1998.  

==plot==
santhosh(Murali (Tamil actor)|Murali) runs a medical store along with his uncle(Manivannan) and he lives with his mother. Gowri(Kausalya (actress)|Kausalya) is a medical student in government hospital living with her grandfather, both become friends and they love to be in company of each other. santhosh falls for gowri but is unable to express his feelings for her likewise gowri falls for him but failed to reach him out through her letter. gowris grandfather is supportive to her love and tells her that he will help her out and he promises to tell santhosh about this but unfortunately he passes away. As gowri completed her final exam her parents are ready to take her back to cochin which is her home town. A letter she sends before reaches his hands by that time and he rushes to railway station to meet her but train moves but a patient in train gets abdominal pain due to pregnancy and gowri is called in for a treatment, she treats the girl and a boy child is born which she names as santhosh. As santhosh reaches her that time and they both join together and gets permission from her parents to be united forever.

==Cast== Murali  Kausalya
*Manivannan Vivek
*Kovai Sarala
*Chinni Jayanth Alphonsa

==Soundtrack==
*Vaanam Tharaiyil - Hariharan
*Kobama - Hariharan
*Kandupidi - SPB, Harini
*Cochin Madapura - Unnikrishnan, Swarnalatha
*Paalaaru - Sabesh
*Dil Dil - Mano, Anuradha Sriram

==Release==
The film had a low key release compared to the teams previous venture Kaalamellam Kadhal Vaazhga (1997) and received average reviews from critics.  

==References==
 

 
 
 
 


 