Seven Times Seven
 
{{Infobox film
| name           = Seven Times Seven
| image          = 7volte7.jpg
| caption        = Film poster
| director       = Michele Lupo
| producer       = Marco Vicario
| writer         = Sergio Donati
| starring       = Gastone Moschin
| music          = Armando Trovajoli
| cinematography = Franco Villa
| editing        = Sergio Montanari
| distributor    = 
| released       =  
| runtime        = 92 minutes
| country        = Italy 
| language       = Italian
| budget         = 
}}
 crime comedy caper directed by Michele Lupo and starring Gastone Moschin.   

==Plot==
A gang of prison inmates escape and rob the Royal Mint. They then sneak back to prison.

==Cast==
* Gastone Moschin - Benjamin Burton
* Lionel Stander - Sam
* Raimondo Vianello - Bodoni
* Gordon Mitchell - Big Ben
* Paolo Gozlino - Bingo (as Paul Stevens)
* Nazzareno Zamperla - Bananas
* Teodoro Corrà - Briggs
* Erika Blanc - Mildred
* Terry-Thomas - Police Inspector
* Turi Ferro - Bernard
* Adolfo Celi - Warden
* Paolo Bonacelli Ray Lovelock - Mildreds Lover
* Gladys Dawson - Miss Higgins Neil McCarthy - Mr. Docherty
* Adalberto Rossetti

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 