Ghost Patrol
 
{{Infobox film
| name           = Ghost Patrol
| image          =
| image_size     =
| caption        =
| director       = Sam Newfield
| producer       = Sigmund Neufeld Leslie Simmonds Joseph ODonnell (story)
| narrator       = Walter Miller
| music          =
| cinematography = Jack Greenhalgh Jack English
| distributor    =
| released       =  
| runtime        = 60 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Ghost Patrol is a 1936 American film directed by Sam Newfield.

==Plot==
A scientific genius has invented a machine capable of causing planes to crash. He uses it on planes loaded with valuables. Various characters become involved in conspiracies and double crosses in an attempt to stop him.

==Cast==
*Tim McCoy as Tim Caverly
*Claudia Dell as Natalie Brent Walter Miller as Ted Dawson
*Wheeler Oakman as Kincaid
*James P. Burtis as Henry Brownlee
*Lloyd Ingraham as Prof. Jonathan Brent
*Dick Curtis as Henchie Charlie

==External links==
* 
* 

 
 

 
 
 
 
 
 
 
 
 

 