The Mummy and the Hummingbird
{{Infobox film
| name           = The Mummy and the Humming Bird
| image          = 
| alt            = 
| caption        = 
| director       = James Durkin 
| producer       = Charles Frohman
| screenplay     = Isaac Henderson
| starring       = Charles Cherry Lillian Tucker Arthur Hoops William Sorelle Claire Zobelle Charles Coleman
| music          = 
| cinematography = 
| editing        = 
| studio         = Charles Frohman Company Famous Players Film Company
| distributor    = Paramount Pictures
| released       =  
| runtime        =
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 drama silent film directed by James Durkin and written by Isaac Henderson. The film stars Charles Cherry, Lillian Tucker, Arthur Hoops, William Sorelle, Claire Zobelle and Charles Coleman. The film was released on November 11, 1915, by Paramount Pictures.  

==Plot==
 

== Cast == 	
*Charles Cherry as Lord Lumley
*Lillian Tucker as Lady Lumley
*Arthur Hoops as Signor DOrelli
*William Sorelle as Giuseppe
*Claire Zobelle as Emma
*Charles Coleman as	Ronalds
*Nina Lindsey as Ruth 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 