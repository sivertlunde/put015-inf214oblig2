The After Party: The Last Party 3
{{Infobox film
| name           = The After Party: The Last Party 3
| director       = Michael Schiller
| producer       = Marc Levin, Caty Borum, Bret Burgess, Susan Chainey, Jon Fine, Donovan Leitch, Daniel Levin, Zachary David Medow, Michael Schiller
| writer         = Michael Schiller, Jon Fine, Steve Marcus
| cinematography = Mark Benjamin, Jeremiah Crowell, Tevo Diaz, Jon Fine, Michael Schiller
| editing        = Eric Bruggemann, Diana DeCilio, Michael Schiller Don King, Donovan Leitch, Bob Leonard, Daniel Levin, Marc Levin, Donna Lieberman, Austin Long, Tom Martinez, Spongebob Squarepants, Shana Rigby, Linda Sarsour, Michael Schiller, Al Sharpton, Anthony Stone, Joseph Villapaz, Antonella Vitale, Cornel West
| released       =  
| runtime        = 64 minutes
| country        = United States
| language       = English
}}
The After Party: The Last Party 3 is a 2011 documentary feature film about a cinematographer (Michael Schiller) whose life is changed forever when he is suddenly caught in a mass arrest while filming 2004 Republican National Convention protest activity near Ground Zero. His ordeal and eventual release compels him to seek answers, but doing so uncovers a bizarre world he never imagined existed.

==Cast==
* André Benjamin
* Mark Benjamin
* Caty Borum
* Barbara Bush
* Jenna Bush
* Eileen Clancy
* Hillary Rodham Clinton
* Jeremiah Crowell
* Chris Dunn
* Jon Fine Don King Donovan Leitch
* Bob Leonard
* Daniel Levin
* Marc Levin
* Donna Lieberman
* Austin Long
* Tom Martinez
* Spongebob Squarepants
* Shana Rigby
* Linda Sarsour
* Michael Schiller
* Al Sharpton
* Anthony Stone
* Joseph Villapaz
* Antonella Vitale
* Cornel West

==Reception==
The After Party: The Last Party 3 has won several awards and has screened across the United States. It has also received favorable reviews.    

== Accolades ==
{| class="wikitable"
|-
! Competition !! Award !! Category
|-
| New York Los Angeles International Film Festival || Winner || Best Documentary Feature 
|-
| Duke City DocFest || Winner || Freedom of Speech Award 
|-
| Los Angeles Cinema Festival of Hollywood || Winner || Best Documentary Feature 
|-
| ThrillSpy International Film Festival || Winner || Best Documentary 
|-
|}
{| class="wikitable"
|-
! Official Selection !! Screening Date !! Venue
|-
| Manhattan Film Festival  || July 24, 2011  || The Producers Club
|-
| Global Peace Film Festival || Sept. 23, 24, 2011  || Rollins College, Plaza Cinema Café
|- Atlanta DocuFest || Sept. 25, 2011  || The Goat Farm
|-
| Duke City DocFest || Oct. 15, 2011  || Kimo Theater
|-
| SF Documentary Film Festival || Oct. 15, 17, 19, 2011  || Roxie Cinema, Shattuck Cinema, Roxie Cinema
|-
| Film Columbia Festival || Oct. 22, 2011  || Morris Memorial
|-
| Red Rock Film Festival || Nov. 12, 2011  || St. George Arts Center
|-
|}

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 

 