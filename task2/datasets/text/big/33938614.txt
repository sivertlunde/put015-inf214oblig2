Stephen Steps Out
{{infobox film
| name           = Stephen Steps Out
| image          =
| imagesize      =
| caption        =
| director       = Joseph Henabery
| producer       = Jesse Lasky William Elliott
| writer         = Richard Harding Davis (short story) Edfrid Bingham (scenario)
| starring       = Douglas Fairbanks, Jr. Harry Myers
| music          =
| cinematography = Faxon Dean
| editing        =
| distributor    = Paramount Pictures
| released       = November 18, 1923
| runtime        = 6 reels;(5,652 feet)
| country        = United States Silent (English intertitles)
}} lost film.   
 
With this film the young Fairbanks Jr. opted for a screen career despite opposition from his famous father, Douglas Fairbanks.

==Cast==
*Douglas Fairbanks, Jr. - Stephen Harlow Jr.
*Theodore Roberts - Stephen Harlow Sr.
*Noah Beery, Sr. - Muley Pasha
*Harry Myers - Harry Stetson
*Frank Currier - Doctor Lyman Black
*James O. Barrows - Professor Gilman
*Fannie Midgley - Mrs. Gilman (*aka Fanny Midgley)
*Bertram Johns - Virgil Smythe George Field - Osman
*Maurice Freeman - Rustem
*Fred Warren - the sultan
*Pat Moore - the sultans son
*Jack Herbert - secretary
*Frank Nelson - hotel proprietor

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 


 