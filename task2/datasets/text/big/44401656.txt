When a Feller Needs a Friend
{{Infobox film
| name           = When a Feller Needs a Friend
| image          = When a Feller Needs a Friend poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Harry A. Pollard
| producer       = Harry A. Pollard  Frank Butler Sylvia Thalberg 
| based on       =  
| starring       = Jackie Cooper Charles "Chic" Sale Ralph Graves Dorothy Peterson Andy Shuford Helen Parrish
| music          = 
| cinematography = Harold Rosson
| editing        = William LeVanway 	
| studio         = Cosmopolitan Productions
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 74 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Frank Butler and Sylvia Thalberg. The film stars Jackie Cooper, Charles "Chic" Sale, Ralph Graves, Dorothy Peterson, Andy Shuford and Helen Parrish. The film was released on April 30, 1932, by Metro-Goldwyn-Mayer.  {{cite web|url=http://www.nytimes.com/movie/review?res=9F07E1D7153EE633A25757C1A9639C946394D6CF|title=Movie Review -
  When a Feller Needs a Friend - Limpy." - NYTimes.com|publisher=|accessdate=14 November 2014}} 

==Plot==
Eddie (Cooper) wears a leg brace and his mother will not let him play like the other boys. His hope is that a German doctor will be able to operate and fix his leg. When his cousin Froggie comes to live with his family, he is nice to Mr. and Mrs. Randall, but mean to Eddie. 

Uncle Jonas (Sale) sees what is happening, but Eddies parents do not believe him as Froggie seems so nice. Uncle Jonas tries to make Eddie tougher by teaching him boxing and baseball, but all it does is get Jonas thrown out of the house.

== Cast ==
*Jackie Cooper as Edward Haverford Eddie Randall
*Charles "Chic" Sale as Uncle Jonas Tucker
*Ralph Graves as Mr. Tom Randall
*Dorothy Peterson as Mrs. Margaret Randall
*Andy Shuford as Frederick Froggie
*Helen Parrish as Diana Manning
*Donald Haines as Fatty Bullen
*Gus Leonard as Abraham
*Oscar Apfel as Doctor 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 

 