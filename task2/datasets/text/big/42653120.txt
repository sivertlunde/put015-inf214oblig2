Lucky Carson
{{Infobox film
| name           = Lucky Carson
| image          = Lucky Carson (1921) - 1.jpg
| alt            = 
| caption        = Newspaper ad
| film name      = 
| director       = Wilfrid North    
| producer       = 
| writer         = {{Plain list| 
*Aquila Kempster
*Bradley J. Smollen 
}}
| screenplay     = 
| story          = 
| based on       =   
| narrator       = 
| starring       ={{Plain list| 
*Earle Williams 
*Earl Schenck
*Betty Ross Clarke
*Gertrude Astor
*Collette Forbes 
*James Butler
*Loyal Underwood 
}}
| music          = 
| cinematography = 
| editing        = 
| studio         = Vitagraph Company of America 
| distributor    = Vitagraph Company of America
| released       =  
| runtime        = 5 reels 
| country        = United States Silent (English intertitles)
| budget         = 
| gross          =  
}}
 silent film,    directed by Wilfrid North. It features Earle Williams,  Earl Schenck, Betty Ross Clarke, Gertrude Astor, Collette Forbes, James Butler, and Loyal Underwood in the lead roles.

==Plot==
John Peters (Earle Williams) is a ruined man who lost all his wealth in betting on horses. He attempts suicide but does not do so after hearing two men conversing with each other. He exchanges clothes with one of the men Rudolph Kluck (Earl Schenck) and goes to America where he amasses great fortune through betting on horses and speculating on Wall Street under the sobriquet "Lucky" Carson.

==Cast==
*Earle Williams as John Peters
*Earl Schenck as Rudolph Kluck
*Betty Ross Clarke as Doris Bancroft
*Gertrude Astor as Madame Marinoff
*Collette Forbes as Edith Bancroft
*James Butler as Tommy Delmaer
*Loyal Underwood as "Runt" Sloan

==References==
 

==External links==
* 
 

 
 
 
 
 


 