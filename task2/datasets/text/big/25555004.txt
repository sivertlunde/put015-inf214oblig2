Taboo (2002 film)
{{Infobox film
|  name           = Taboo
| image = Taboo.jpg
| caption = Official theatrical poster
|  director       = Max Makowski
|  producer       = Ash R. Shah Chris Fisher
|  writer         = Chris Fisher
|  starring       = Nick Stahl Eddie Kaye Thomas January Jones
|  music          = Ryan Beveridge
|  cinematography = Viorel Sergovici Jr.
|  editing        = M. Scott Smith
|  studio         = Creative Entertainment Group Imperial Fish Company Silver Bullet
|  distributor    = Columbia TriStar Home Video
|  released       =  
|  runtime        = 80 minutes
|  country        = United States
|  language       = English
|  gross          =
}} Mystery Thriller film directed by Max Makowski and stars Nick Stahl, Eddie Kaye Thomas and January Jones.

==Plot==
A lone castle, three women, three men and a game taboo, Elizabeth initiates it. Everyone has to answer a tricky question with "yes" or "no", for example, admit to a vice erotic nature.  Everything is anonymous so it seems, a year later they meet again in the castle, which is now owned by Christian, became the sole heir of the assets of his family and has become engaged to Elizabeth. They want to celebrate New Years Eve, it looks like a happy New Years night, but then Elizabeth is unexpectedly confronted with the Taboo game, but this time, each scene ends with the supposed death of one of their guests! Who is the murderer? Those who survived the deadly game?  Or is it all a horrible nightmare?  

==Cast==
* Nick Stahl as  Christian Turner
* Eddie Kaye Thomas	as Adam
* January Jones as Elizabeth
* Lori Heuring as Katie
* Derek Hamilton as Benjamin
* Amber Benson as Piper

==Release==
The film premiered on 14 January 2002 at Sundance Film Festival. 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 


 