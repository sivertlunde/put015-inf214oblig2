Boccaccio (1936 film)
{{Infobox film
| name           = Boccaccio
| image          = 
| alt            =  
| caption        = 
| director       = Herbert Maisch
| producer       = Max Pfeiffer
| writer         = Emil Burri Walter Forster
| starring       = Albrecht Schoenhals Gina Falckenberg Willy Fritsch Heli Finkenzeller
| music          = Franz Doelle
| cinematography = Konstantin Irmen-Tschet
| editing        = Carl Otto Bartning
| studio         = 
| distributor    = 
| released       =  
| runtime        = 88 minutes
| country        = Nazi Germany
| language       = German
| budget         = 
| gross          = 
}}

Boccaccio is a 1936 German musical film directed by Herbert Maisch and starring Albrecht Schoenhals, Gina Falckenberg and Willy Fritsch. 

==Plot==

Boccaccio is an operetta that relates how  Nazis conceived the Italian Renaissance. The Ferrara’s residents  are carried up in a tide of emotion and physical passion. Before long, the town is in chaos. The film is in German with English subtitles.

==Cast==
* Albrecht Schoenhals  ...  Cesare dEste - Herzog von Ferrara  
* Gina Falckenberg ...  Francesca - seine Frau  
* Willy Fritsch ...  Petruccicio - Schreiber am Stadtgericht  
* Heli Finkenzeller ...  Fiametta - seine Frau   Paul Kemp ...  Calandrino - Verleger und Buchdrucker  
* Fita Benkhoff ...  Bianca, seine Frau  
* Albert Florath ...  Bartolomeo - Hauptmann  
* Tina Eilers ...  Pia - seine Frau  
* Ernst Waldow ...  Ricco - Vertrauter des Herzogs  
* Hans Hermann Schaufuss ...  First Judge

==References==
 

==External links==
*  
*  

 
 
 
 
 
 


 
 
 