Aadhi
 
 
{{Infobox film
| name           = Aathi
| image          = Aadhi DVD Cover.jpg
| caption        = DVD Cover Ramana
| Vijay
| writer         = Surender Reddy Vakkantham Vamsi Vivek Sai Sai Kumar Adithya Menon Vidyasagar
| cinematography = S. Soundar Rajan
| editing        = Suresh Urs
| studio         = Sooriyan Arts
| released       = 15 January 2006
| runtime        = 167 min
| language       = Tamil Telugu
| country        = India
| budget         =  120&nbsp;million
| box office     =  1.35 &nbsp;billion

}}
 Vijay and Trisha in Telugu film Ramana  Telugu as Neneraa Aadhi. 

==Plot==
The opening scene shows Anjali (Trisha Krishnan) in white, like an angel, sitting on a white bench feeding a white pigeon by a calm ocean and a police officer (Devan) coming and sitting by her side and exchanging pleasantries and suddenly she whips out a knife and kills him saying that she has been waiting for this moment for many years.

After that, the movie moves to Aadhi (Vijay (actor)|Vijay) who lives with his foster parents (Manivannan & Seetha (actress)|Seetha) in New Delhi. He takes up a course in a Chennai college against his parents wishes, while he is actually on mission to eliminate the people behind the murder of his blood family.

Similarly Anjali studying in the same college has her own agenda to seek revenge on her parents killers, and she is assisted by her uncle (Nassar (actor)|Nassar). Soon Aadhi falls in love with Anjali, as she brings back memories of his childhood sweetheart and cousin who loved to dance in the rain.
 Sai Kumar). To strike back at Aadhis father who refuses to release his henchmen, he pays them a visit with some of his henchmen and the police officer Anjali murdered earlier in the film and murders their whole extended family. Only Anjali, her uncle and Aadhi survived the blast that annihilated their family.

Together, they combine forces to get revenge on RDX. The climax shows a battle between Aadhi and RDX, in which Aadhi kills RDX.

==Cast== Vijay as Aadhi
* Trisha Krishnan as Anjali Sai Kumar as RDX 
* Prakash Raj as Aadhis father Vivek as Bullet
* Subbaraju as RDXs brother Adithya Menon as Abdullah
* Nassar as Ramachandra
* Manivannan as Mani Livingston as Anjalis father Vijayakumar as Aadhis grandfather
* Malavika Avinash as Ramachandras Wife Amritha as Aadhis mother
* Sneha Nambiar as Anjalis Mother
* Karate Raja
* Madan Bob
* Adithya as RDXs henchman Raj Kapoor
* Rajan P. Dev as Pattabhi
* Santhana Bharathi
* Dhamu
* Five Star Krishna Devan as Inspector Shankar
* Sampath Ram
* Nandha Saravanan

==Production==
Aathi was directed by Ramana, who had teamed up with Vijay earlier for the film Thirumalai. Aathi is the remake of the successful Telugu film, Athathanokkade, which had Kalyanram, grandson of NTR, playing the hero.

Vijay is paired with Trisha in the Tamil film, this coming after their pairing for Gilli and Thirupachi. The others in the cast are Nasser, Sneha Nambiar, Livinston, Malavika, Rajan P. Dev, Manivannan, Rajkapur, Damu and Sita. Vivek has been roped in for comedy.

A romantic song was picturised on Vijay and Trisha on a set designed at a cost of Rs 40 lakh. Accompanying them were 50 dancers and a crowd comprising about 75 children and some junior artistes. 

==Reception & box office==
The film received positive reviews from critics but was average at box office. Sify stated that "It is only Vijays strong screen presence that makes Aathi, somewhat credible. At best, this hit-and-run revenge flick adds up to a action feast for the festival audience".   Nowrunning rated 4 out of 5 stars and called the film "Though not bracingly pleasurable, the movie is worth for its well etched out dialogues, racy action sequences and Vijays brisk acting" 

==Soundtrack==
{{Infobox album |  
| Name       = Aathi
| Type       = soundtrack Vidyasagar
| Cover      =
| Released   = 2006
| Recorded   =
| Genres     = Soundtrack
| Length     =
| Label      = Five Star Audio Vidyasagar
| Last album = Paramasivan (2006)
| This album = Aathi (2006)
| Next album = Mozhi (2007)
}} Vidyasagar with lyrics by Palani Barathi, Pa. Vijay & Yuga Barathi.
{| class="wikitable" style="width:60%;"
|-
! Song Title !! Singers
|- Karthik (singer)|Karthik, Anuradha Sriram
|- Sujatha Mohan
|-
| "Athi Athikka" || S. P. Balasubrahmanyam, Sadhana Sargam
|- Tippu
|- KK (singer)|KK, Sujatha Mohan
|}

The film includes two more songs not included in the audio soundtrack which are:
* “Varran Varran” sung by Tippu
* “Iruvar Vazhvum” sung by Karthik & Kalyani Nair

==References==
 

==External links==
*  

 

 
 
 
 