Victoria (2013 film)
Victoria is a 2013 Norwegian drama film directed by Torun Lian, starring Jakob Oftebro, Iben Akerlie and Bill Skarsgård. It tells the story of the love between the daughter of a landowner and the son of a local miller. The film is based on the novel Victoria (novel)|Victoria by Knut Hamsun. It was released in Norway on 1 March 2013.  Fridtjov Såheim received the Amanda Award for Best Actor in a Supporting Role. 

==Cast==
* Iben Akerlie as Victoria
* Jakob Oftebro as Johannes
* Bill Skarsgård as Otto
* Fridtjov Såheim as Victorias father

==References==
 

 
 
 
 
 
 
 
 