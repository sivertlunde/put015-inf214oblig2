Toto, Peppino and the Fanatics
 
{{Infobox Film
| name           = Toto, Peppino and the Fanatics (Totò, Peppino e le fanatiche)
| image          = Totò, Peppino e le fanatiche.jpg 
| caption        = Film poster
| director       = Mario Mattoli
| producer       = Isidoro Broggi Renato Libassi
| writer         = Agenore Incrocci Ruggero Maccari Furio Scarpelli Stefano Vanzina Mario Mattoli
| narrator       = 
| starring       = Totò, Peppino De Filippo, Johnny Dorelli
| music          = Johnny Dorelli, Renato Carosone
| cinematography = Anchise Brizzi
| editing        = Gisa Radicchi Levi
| distributor    = Titanus
| released       =  
| runtime        = 90 minutes
| country        = Italy 
| language       = Italian
| budget         = 
}}

Toto, Peppino and the Fanatics ( ) is a 1958 Italian comedy film directed by Mario Mattoli and starring Totò.   

==Plot==
The poor Antonio Vignanelli and Peppino Caprioli are exacerbated by hobby and foibles of their respective families, the two that cause many problems. The two are taken for fools and taken to the asylum, and in fact the director of the asylum (Aroldo Tieri) tells how the various misunderstandings that led to their hospitalization are due in reality the foibles of their families.

==Cast==
*Totò: Antonio Vignanelli
*Peppino De Filippo: Peppino Caprioli
*Johnny Dorelli: Carlo Caprioli
*Alessandra Panaro: la figlia dei Vignanelli
*Diana Dei: la moglie del capoufficio
*Mario Riva: Peppinos boss
*Rosalia Maggio: Anita Vignanelli
*Aroldo Tieri: il direttore del manicomio
*Enzo Garinei: il giornalista
*Giacomo Furia: il cugino di Giovanni
*Peppino De Martino: Giovanni
*Yvette Masson: Trude, the German tourist
*Fanfulla: Giacinti
*Renato Carosone: himself
* Anna Campori
* Virna Lisi

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 