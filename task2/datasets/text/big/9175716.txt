Once (film)
 
 
{{Infobox film
| name           = Once
| image          = Once (2006 film)poster.jpg
| caption        = US release poster John Carney
| producer       = Martina Niland
| writer         = John Carney
| starring       = Glen Hansard Markéta Irglová Interference
| cinematography = Tim Fleming
| editing        = Paul Mullen
| studio         = Samson Films BVI  Fox Searchlight   Icon Film Distribution  
| released       =   
| runtime        = 86 minutes   
| country        = Ireland
| language       = English
| budget         = $150,000 
| gross          = $20,710,513   
}}
 John Carney. Set in Dublin, Republic of Ireland|Ireland, the naturalistic drama stars musicians Glen Hansard and Markéta Irglová. Collaborators prior to making the film performing under the stage moniker The Swell Season, Hansard and Irglová composed and performed all of the original songs in the film.

Shot for only €112,000 ( . Hansard and Irglovás song "Falling Slowly" won the 2007 Academy Award for Best Original Song and the soundtrack as a whole also received a Grammy Award nomination.

Once spent years in development with the Irish Film Board.  It was during a period where the film board had no chief executive (for about 6 months) that the film was given the go-ahead by a lower level executive on the provision that the producers could make it on a budget of approximately €112,000 and not the initial higher budget.

==Plot== Czech immigrant Hoovers (a brand name that became synonymous with vacuum cleaners in the United Kingdom and Ireland), she insists that he fix her broken vacuum cleaner.

Next day she comes back with her vacuum cleaner, and soon tells him that she is a musician, too. At a music store where she regularly plays piano, he teaches her one of his songs ("Falling Slowly"); they sing and play together. He invites her and her ailing vacuum back to his fathers shop, and on the bus home musically answers her question as to what his songs are about: a long-time girlfriend who cheated on him, then left ("Broken Hearted Hoover Fixer Sucker Guy").

 
At the shop, she meets his father (Bill Hodnett). The Guy takes the Girl up to his room, but when he asks her to stay the night, she feels insulted and leaves. The next day, they quickly patch things up, as over the course of a week they excitedly write, rehearse and record songs.  The Girl rehearses her lyrics for one of the Guys songs (which she entitles "If You Want Me"), singing to herself while walking down the street, or when at a party, people perform impromptu (including "Gold").
 demo of his songs to take with him and asks the Girl to record it with him. They secure a bank loan and reserve time at a recording studio.

He learns she is married, with a husband in the Czech Republic. When Guy asks if she still loves her husband, she answers in Czech language|Czech, "Miluju tebe",  but coyly declines to translate what she said. (Although the translation is not included in the movie, in the Czech language it means "It is you I love.") After recruiting a band among other buskers (Gerard Hendrick, Alaistair Foley, Hugh Walsh), they go into the studio to record. They quickly impress Eamon, the jaded studio engineer (Geoff Minogue) with their first song ("When Your Minds Made Up"). On a break in the wee hours of the morning, the Girl finds a piano in an empty studio and plays the Guy one of her own compositions ("The Hill"). He asks her to come with him to London, which she jokingly goes along with before they come to terms with reality.

After the all-night session wraps up, they walk home. Before they part ways, the Girl reveals that she spoke to her husband and he is coming to live with her in Dublin. The Guy asks her to spend his last night in Dublin with him; she says that it would only result in " ", which is a "bad idea", but after the Guys pestering she ultimately agrees to come over. In the end, she stands him up and he cannot find her to say goodbye before his flight. He plays the demo for his father, who gives him money to help him get settled in London. Before leaving for the airport, the Guy buys the Girl a piano (a Petrof) and makes arrangements for its delivery, then calls his ex-girlfriend, who is happy about his imminent arrival. The Girls husband (Senan Haugh) moves to Dublin and they reunite.

==Main cast==
 
* Glen Hansard as Guy
* Markéta Irglová as Girl 
* Hugh Walsh as Timmy Drummer
* Gerard Hendrick as Lead Guitarist 
* Alaistair Foley as Bassist
* Geoff Minogue as Eamon
* Bill Hodnett as Guys Dad
* Danuse Ktrestova as Girls Mother
* Darren Healy as Heroin Addict
* Mal Whyte as Bill
* Marcella Plunkett as Ex Girlfriend
* Niall Cleary as Bob
* Wiltold Owski as Man watching TV
* Krzysztos Tlotka as Man watching TV
* Tomek Glowacki as Man watching TV
* Keith Byrne as Guy in Piano Shop
 

==Production==
 .]]
The two leads, Hansard and Irglová, are both professional musicians. Scott, A. O.  , The New York Times, 16 May 2007. Accessed 21 April 2008.  Director Carney, former bassist for Hansards band  , the story of a Dublin soul music cover band. Hansard was initially reluctant, fearing that he wouldnt be able to pull it off, but after stipulating that he had to be fully involved in the filmmaking process and that it be low-budget and intimate, he agreed. 

Produced on a shoestring, about 75% of the budget was funded by Bord Scannán na hÉireann (The Irish Film Board), plus some of Carneys own money. The director gave his salary to the two stars, and promised a share of the back-end for everyone if the film was a success. Shot with a skeleton crew on a 17-day shoot, the filmmakers saved money by using natural light and shooting at friends houses.  The musical party scene was filmed in Hansards own flat, with his personal friends playing the partygoers/musicians    &mdash;his mother, Catherine Hansard, is briefly featured singing solo. The Dublin street scenes were recorded without permits and with a long lens so that many passersby didnt even realize that a film was being made. The long lens also helped the non-professional actors relax and forget about the camera, and some of the dialogue ended up being improvised. 
 Bogart and Lauren Bacall|Bacall. Hansard and Irglová did become a couple in real life, getting together while on a promotional tour across North America, and living together in Dublin, in Hansards flat. du Lac, J. Freedom.  , The Washington Post, 17 February 2008. Accessed 21 April 2008.  Entertainment Weekly reported:  

Subsequently, Hansard indicated that that they were no longer a romantic couple.  He said, "Of course, we fell into each others arms. It was a very necessary part of our friendship but I think we both concluded that that wasnt what we really wanted to do. So were not together now. We are just really good friends."

Yet Hansard and Irglová were quite happy with the unrequited ending for their onscreen characters. In an interview, Hansard states that "Had the US distributor changed the end and made us kiss, I wouldnt be interested in coming and promoting it, at all."  Hansard says that ad-libbing produced the moment where Irglovas character tells the Guy in unsubtitled Czech, "No, I love you", but when it was shot, he didnt know what shed said, just like his character.   Accessed 14 January 2008. 

Both Hansard and Irglova give the impression in interviews that they are unlikely to pursue further acting. Irglova has spoken about being nervous in front of a crew, saying "I dont think I would be a good actress, overall",    and Hansard generally refers to the movie as a one-off, talking of "moving on... living a different life". 

As a result of the film, Hansard and Irglová have been releasing music and touring together as The Swell Season.

Glen Hansard and Markéta Irglová reprised their roles in The Simpsons episode "In the Name of the Grandfather".

==Reception==

===Box office performance and awards===

A rough cut of the film was previewed on 15 July 2006 at the   on 20 January 2007 and the Dublin Film Festival in February 2007, and received the audience awards at both events.   Re-linked 2013-05-07. 

The film was first released on cinema in Ireland on 23 March 2007, followed by a limited release in the United States on 16 May 2007. After its second weekend in release in the United States and Canada, the film topped the 23 May 2007 indieWIRE box office chart with nearly $31,000 average per location.   from 23 May 2007.  As of 28 March 2009, Once has grossed nearly $9.5 million in North America and over $20 million worldwide.  After 2007s box office success and critical acclaim, it won the Independent Spirit Award for Best Foreign Film.   Steven Spielberg was quoted as saying "A little movie called Once gave me enough inspiration to last the rest of the year". When informed of Spielbergs comments, director John Carney told Sky News, "in the end of the day, hes just a guy with a beard". At the time of this interview, Carney himself was also wearing a beard. Breznican, Anthony.  , USA Today, 7 August 2007. Accessed 9 August 2007. 
 The Cost The Swell AMPAS music committee satisfied themselves that the song had indeed been written for the film and determined that, in the course of the films protracted production, the composers had "played the song in some venues that were deemed inconsequential enough to not change the song’s eligibility". 

===Critical response===

Once was met with extremely high positive reviews from critics. Upon its March 2007 release in Ireland, RTÉs Caroline Hennessy gave the film 4 out of 5 stars and termed it "an unexpected treasure". About the acting, this Irish reviewer commented, "Once has wonderfully natural performances from the two leads. Although musicians first and actors second, they acquit themselves well in both areas. Irglová, a largely unknown quantity alongside the well-known and either loved or loathed Hansard, is luminous."  Michael Dwyer of The Irish Times gave the film the same rating, calling it "irresistibly appealing" and noting that "Carney makes the point - without ever labouring it - that his protagonists are living in a changing city where the economic boom has passed them by. His keen eye for authentic locations is ... evident". 

In May,    , RottenTomatoes.com. Accessed 1 March 2008.  and scored a grade of 88 ("universal acclaim") according to Metacritic.  , Metacritic.com. Accessed 1 March 2008. 
 The Telegraphs Sukhdev Sandhu said, "Not since Before Sunset has a romantic film managed to be as touching, funny or as hard to forget as Once. Like Before Sunset, it never outstays its welcome, climaxing on a note of rare charm and unexpectedness." 

The film appeared on many North American critics top ten lists of the best films of 2007:
 
  Michael Phillips, The Chicago Tribune 
* 1st - Nathan Rabin,  . Accessed 5 January 2008. 
* 2nd - David Germain, Associated Press Germain, David and Lemire, Christy.  , Associated Press via Columbia Daily Tribune, 27 December 2007. Accessed 31 December 2007. 
* 2nd - Kevin Crust, Los Angeles Times 
* 2nd - Kyle Smith, New York Post  Shawn Levy, The Oregonian 
* 2nd - Roger Moore, The Orlando Sentinel 
* 2nd - Robert Butler, Kansas City Star 
* 2nd - Paste Magazine 
* 3rd - Christy Lemire, Associated Press 
* 3rd - Tasha Robinson, The A.V. Club 
* 3rd - Andrew Gray, Tribune Chronicle 
 
* 3rd - Sean Means, Salt Lake Tribune 
* 4th - Keith Phipps, The A.V. Club 
* 4th - Christopher Kelly, Star Telegram 
* 5th - Ann Hornaday, The Washington Post 
* 5th - Desson Thomson, The Washington Post 
* 5th - Noel Murray, The A.V. Club 
* 6th - Ella Taylor, LA Weekly 
* 6th - Nick Digilio, WGN-AM 
* 7th - Claudia Puig, USA Today  Dana Stevens, Slate (magazine)|Slate 
* 7th - Scott Tobias, The A.V. Club 
* 7th - Scott Mantz, Access Hollywood 
 
* 7th - Craig Outhier, Orange County Register 
* 8th - Liam Lacey and Rick Groen, The Globe and Mail 
* 8th - Owen Gleiberman, Entertainment Weekly 
* 8th - Stephanie Zacharek, Salon.com|Salon 
* 9th - Joe Morgenstern, The Wall Street Journal 
* 9th - Michael Rechtshaffen, The Hollywood Reporter 
* 9th - Richard Roeper, At the Movies with Ebert & Roeper 
* 9th - Kenneth Turan, Los Angeles Times 
* 9th - Carina Chocano, Los Angeles Times 
* 9th - James Verniere, Boston Herald 
* 10th - Bob Mondello, NPR  , MovieCityNews.com. Accessed 1 May 2008. 
* 10th - Peter Vonder Haar, Film Threat 
 

In 2008, the film placed third on Entertainment Weekly  s "25 Best Romantic Movies of the Past 25 Years". 

==DVD and Blu-ray  ==
Once was released on DVD in the US on 18 December 2007,  and in the UK on 25 February 2008,  followed by a British Blu-ray release on 16 February 2009.  Once was released on Blu-ray in the US as an Amazon-exclusive on April 1, 2014. 

==Soundtrack==
{{Infobox album  
| Name        = Once
| Type        = soundtrack Interference
| Cover       = Once Soundtrack Cover.jpg
| Cover size  =
| Released    =  
| Recorded    =
| Genre       = Folk rock
| Length      = 43:37 Sony Music Soundtrax
| Producer    = Glen Hansard
}}

The soundtrack album was released on 22 May 2007 in the U.S. and four days later in Ireland.

A collectors edition of the soundtrack was released on 4 December 2007 in the US with additional songs and a bonus DVD with live performances and interviews about the film.  The additional songs were two previously unreleased  ", and Hansard and Irglovás "Into the Mystic".    
 The Cost and on Hansard and Irglovás The Swell Season (both released in 2006). An early version of the last track, "Say It to Me Now", originally appeared on The Frames 1995 album Fitzcarraldo (1995 album)|Fitzcarraldo. "All the Way Down" first appeared on the self-titled album from musician collective The Cake Sale, with Gemma Hayes providing vocals. The song "Gold" was written by Irish singer-songwriter Fergus OFarrell and performed by Interference.   

;Track listing
{{Track listing
| extra_column    = Performer(s)
| all_writing     = Glen Hansard, except where noted 
| title1          = Falling Slowly
| note1           = Glen Hansard, Markéta Irglová
| extra1          = Glen Hansard and Markéta Irglová
| length1         = 4:04
| title2          = If You Want Me
| note2           = Irglová
| extra2          = Irglová and Hansard
| length2         = 3:48
| title3          = Broken Hearted Hoover Fixer Sucker Guy
| extra3          = Hansard
| length3         = 0:53
| title4          = When Your Minds Made Up
| extra4          = Hansard and Irglová
| length4         = 3:41
| title5          = Lies
| note5           = Hansard, Irglová
| extra5          = Hansard and Irglová
| length5         = 3:59
| title6          = Gold
| note6           = Fergus OFarrell Interference
| length6         = 3:59
| title7          = The Hill
| note7           = Irglová
| extra7          = Irglová
| length7         = 4:35
| title8          = Fallen from the Sky
| extra8          = Hansard
| length8         = 3:25
| title9          = Leave
| extra9          = Hansard
| length9         = 2:46
| title10         = Trying to Pull Myself Away
| extra10         = Hansard
| length10        = 3:36
| title11         = All the Way Down
| extra11         = Hansard
| length11        = 2:39
| title12         = Once
| extra12         = Hansard and Irglová
| length12        = 3:39
| title13         = Say It to Me Now
| extra13         = Hansard
| length13        = 2:35
| title14         = And the Healing Has Begun
| note14          = Van Morrison, Collectors Edition only
| extra14         = Hansard
| length14        = 5:19
| title15         = Into the Mystic
| note15          = Morrison, Collectors Edition only
| extra15         = Hansard and Irglová
| length15        = 4:21
}}

===Accolades=== Best Compilation Best Song Written for Motion Picture, Television or Other Visual Media.  It won the Los Angeles Film Critics Association Award for Best Music,  , Variety, 9 December 2007.  and it was ranked at number two on the Entertainment Weekly 25 New Classic Soundtrack Albums list (1983–2008). 

===Charts success===
The soundtrack album reached #20 on the Irish Albums Chart in its first week, peaking at #15 a few weeks later.  Following the Oscar win, the album reached the top of the chart, while "Falling Slowly" reached a new peak of #2. 

In the United States, it ranked as the #10 soundtrack on 1 June.   from the 9 June 2007 issue  As of 11 July 2007, the album has sold 54,753 copies in the US. The album reached #27 on the Billboard 200|Billboard 200 according to Allmusic. It also reached #2 on the Soundtracks Chart and #4 on the Independent Chart. 

==Stage adaptation==
 
The film has been adapted for the stage as the musical (Once (musical)|Once). It first opened at the New York Theatre Workshop on 6 December 2011. The screenplay was adapted by Enda Walsh and the production directed by John Tiffany. 

In February 2012 the musical transferred to Broadway theatre|Broadways Bernard B. Jacobs Theatre. It began in previews on 28 February 2012 and opened on 18 March 2012.    Directed by John Tiffany, the cast features Steve Kazee as Guy and Cristin Milioti as Girl with sets and costumes by Bob Crowley. The music is from the film with two additional songs, and the cast is also the orchestra. The musical opened up to generally positive reviews.  Since its opening,  Once has been named Best Musical by The Outer Critics Circle, Drama League, The New York Drama Critics Circle, and The Tony Awards.
 Best Musical, Best Actor Best Actress Best Featured Best Direction Best Book of a Musical and Best Actor in a Musical. 

==See also==
* Busking
* Once (musical)
* Cinema of Ireland
* Musical films

==References==
 

==External links==
*  
*   (plays complete soundtrack - music starts when page loads)
*  
*  
*  
*  
*  
*  
*  

;Interviews
*   at  
*   at  
*   at Janakis Musings

;Reviews
*  , Entertainment Weekly review by Owen Gleiberman (15 May 2007)
*  , Rolling Stone review by Peter Travers (17 May 2007)
*  , stv.tv

 
{{Succession box
|title=Academy Award for Best Original Song
|before="I Need to Wake Up" by Melissa Etheridge Jai Ho" by A. R. Rahman 80th Academy Awards
}}
 

 

 
 
 
 
 
 
 
 
 
 
 
 