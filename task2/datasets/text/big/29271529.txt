3 Stories About Evil
{{Infobox Film
| name           = 3 Stories About Evil
| image          = 
| caption        = 
| director       = Michael Frost
| producer       = Jay Frank Walter Reuben Michael Frost Andrew Sachs Edgar Varela
| writer         = Walter Reuben
| starring       = Mink Stole Erica Gavin Joe Dallesandro Billy Drago Suzete Belouin Barry Brisco Joey Krebs Kimmy Robertson Laurence Tolhurst
| music          =
| cinematography = Andrew K. Sachs
| editing        = Michael Frost
| distributor    = Helsinki Productions
| released       =  
| runtime        = 22&nbsp;minutes
| country        = United States
| language       = English
| budget         =
}}
3 Stories About Evil is a 2008 short, experimental narrative film directed by Michael Frost and photographed by Andrew K. Sachs. It was written by Walter Reuben and stars Mink Stole, Erica Gavin, Joe Dallesandro, and Billy Drago. 

==Premise== black comedies about family, sexuality, the media, and beauty pageants.     

==Plot==
Three interrelated stories comprise the plot structure of this film. In the first, "The Story of Johnnie & Laurie," Johnnie (Barry Brisco) is betrayed by his sister (Suzette Belouin) when she finds out his sexual proclivities. When she informs her parents (Billy Drago & Erica Gavin) about his homosexuality, their estrangement leads Johnnie through a series of unsavory incidents. In "The Story of Pat & Pepper," Pat (Mink Stole) is a Christian conservative and her five-year-old daughter Pepper (Pepper Peeters) participates in childrens beauty pageants. Ambivalent over her daughters attractiveness to pageant officials, Pat accidentally kills Pepper and transforms her into a dead chanteuse. Finally, in "The Story of Jim," Jim (Joey Krebs) takes the advice of his best friend Eddie (Laurence Tolhurst) and gets a job in the television industry. When Jims career plummets due to poor ratings, he finds a sex change operation the perfect solution out of a desperate situation. 

==Cast==
* Stacie Abram - Woman Caller
* Christy Ball - Toyota Kawasaki
* Suzete Belouin - Laurie Harris
* Squeaky Blonde - Judge
* Barry Brisco - Johnnie Harris
* Tim Caszatt - Judge
* Michael Ciriaco - Robby Donan
* Joe Dallesandro - Jean Maries
* Billy Drago - Mr. Harris
* Erica Gavin - Mrs. Harris
* Heather Gray - Anne
* Joey Krebs - Jim
* George Kuchar - Telephone Man
* Daylyn Presley - Pervert
* Kimmy Robertson - Narrator
* Walter Reuben - Narrator
* Sebastian Sage - Hippy
* Mink Stole - Pat Peeters
* Peter Struve - Mercury Man
* Grant Sullivan - Kroger
* Laurence Tolhurst - Eddie
* Kat Turner - Dominatrix
* Dave White - Narrator

==Awards==
* Boston Underground Film Festival 2008 - Most Effectively Offensive  
* DragonCon Independent Film Festival (Atlanta, GA) 2008 - Best Dark Comedy  
* RADAR International Film Festival (Hamburg, Germany) 2008 - Best Art & Animation  
* Downtown Film Festival Los Angeles 2008 - Best Experimental Short Film   

== References ==
 

== External links ==
*  
*  

 
 