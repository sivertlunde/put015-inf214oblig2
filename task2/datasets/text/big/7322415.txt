Clive of India (film)
{{Infobox film
| name = Clive of India
| image = Clive_India35.jpg
| caption = 
| director = Richard Boleslawski
| producer = William Goetz   Raymond Griffith
| writer = W. P. Lipscomb  R. J. Minney
| starring = Ronald Colman  Loretta Young  Colin Clive  Francis Lister Alfred Newman
| cinematography = J. Peverell Marley
| editing = Barbara McLean
| studio = 20th Century Fox
| distributor = United Artists
| released =  
| runtime = 95 min.
| country = United States
| language = English
| budget =
}}
Clive of India is a 1935 American drama film based on Robert Clive, 1st Baron Clive|Robert, Lord Clives historical biography. It was written by R.J. Minney and W. P. Lipscomb and directed by Richard Boleslawski.  Colin Clive, who appears in the film, was a descendant of Clive.

==Cast==
* Ronald Colman as Baron Robert Clive
* Loretta Young as Margaret Maskelyne Clive
* Colin Clive as Captain Johnstone
* Francis Lister as Edmund Maskelyne
* C. Aubrey Smith as the British Prime Minister
* Cesar Romero as Mir Jaffar
* Montagu Love as Governor Pigot
* Lumsden Hare as Sergeant Clark
* Ferdinand Munier as Admiral Watson
* Gilbert Emery as Sullivan
* Leo G. Carroll as Manning
* Etienne Girardot as Warburton
* Robert Greig as Pemberton
* Mischa Auer as Suraj Ud Dowlah
* Ferdinand Gottschalk as Old Member
* Doris Lloyd as Mrs. Nixon
* Vernon Downing as Stringer David Dunbar as Clerk

==See also==
* Great Britain in the Seven Years War

==External links==
* 

 
 
 
 
 
 
 
 
 


 