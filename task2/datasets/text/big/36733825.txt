Don't Click
{{Infobox film
| name           = Dont Click
| image          = Don’t_Click_poster.jpg
| film name      = {{Film name
| hangul         =  :   
| hanja          = 미확인  :  클릭 
| rr             = Mihwagin Dongyeongsang: Jeoldaekeullik Geumji
| mr             = Mihwagin Tongyŏngsang: Chŏldaekŭllik Kŭmji}}
| director       = Kim Tae-kyung
| producer       = Ahn Dong-kyu   Cha Ji-hyeon   Jang Won-seok
| writer         = Kim Tae-hyoung   Hong Geon-guk   Kim Tae-kyung
| starring       = Park Bo-young   Joo Won 
| music          = Seong Ji-dam
| cinematography = Kim Gi-tae 
| editing        = Kim Sun-min
| distributor    = Showbox/Mediaplex
| released       =  
| runtime        = 93 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          = 
}}
Dont Click ( ; lit. "Unidentified Video Footage") is a 2012 South Korean horror film about a "forbidden video" that curses people to death.  

==Cast==
*Park Bo-young as Se-hee   
*Joo Won as Joon-hyuk 
*Kang Byul as Jung-mi
*Lee Malg-eum as young girl
*Kang Hae-in as the clever one
*Choi Ji-heon as Ji-heon
*Soo-min as Soo-min
*Lee Jeong-min as Sang-mi
*Kim Min-hyuk as Detective Kim
*Nam Kyeong-eup as Detective Jang

==Release==
Dont Click was originally slated to be released during the summer of 2011, but because of lack of screens available, it was pushed back to the autumn/winter season. Distributor Showbox/Mediaplex then decided to push the movies release to the summer of 2012, believing horror films generally do better business during that season. 

==References==
 

==External links==
*    
*  
*  
*  

 
 
 
 
 


 
 