Kokila (1937 film)
{{Infobox film
| name           = Kokila
| image          = 
| image_size     = 
| caption        = 
| director       = Sarvottam Badami
| producer       = Sagar Movietone
| writer         = Ramanlal Vasantlal Desai 
| narrator       =  Motilal Sabita Devi Shobhna Samarth Maya Bannerjee Anil Biswas  
| cinematography = Faredoon Irani
| editing        = 
| distributor    = Supreme Film Distributors, Bombay
| studio         = Sagar Movietone
| released       = 30 October 1937
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}} 1937 Hindi Anil Biswas Gujarati writer Ramanlal Vasantlal Desai.    The film starred Motilal (actor)|Motilal, Sabita Devi, Shobhna Samarth, Maya Bannerjee, Sitara Devi, Pesi Patel, Siddiqui and Kayam Ali. 

==Cast== Motilal
* Sabita Devi
* Shobhna Samarth
* Sitara Devi
* Sankata Prasad
* Maya Bannerjee
* Pesi Patel
* Kayam Ali
* Siddiqui

==Review and Box-Office==
The film was not a success with Baburao Patel of FilmIndia in his December 1937 editorial claiming it had "failed rather badly".   
However, it had a large audience attendance in the first week and that according to Patel was due to Sabita Devi. The screenplay was considered a poor adaptation of the story. Patel  was critical of Motilal and Siddiquis performances while praising Maya Bannerjee and Sankatha Prasad for their efforts.    Shobhna Samarth had made her entry in films through Nigah-e-Nafrat (1935), but came into prominence with her roles in films like Do Diwane (1936) and Kokila.    


==Music==
The music was composed by Anil Biswas with lyrics by Siddiqui and Zia Sarhadi  The singers were Sabita Devi, Maya Bannerjee, Anil Biswas and Dattaram Kadam.    

===Songlist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer 
|-  
| 1
| Auron Ka Jag Kya Hai
| Dattaram Kadam
|-
| 2
| Din Ki Ghadiyan Roye Bitai
| 
|-
| 3
| Sajan Ke Waste Main Bhon Le Jaoongi
|-
| 4
| Sagar Gaaye parvat Gaaye, Gaaye Koyal Ban Ki 
| Anil Biswas
|-
| 5
| Nainan  Ke Sain Se Bulayi Ho Shyam Mohe 
|
|-
| 6
| Mohe Ghar Ke Dwar Pe Laaga jaamuniya Ka Ped
| Anil Biswas
|-
| 7
| Jeevan Mein Sikh Paaya Humne
|
|}


==References==
 


==External links==
* 

 


 
 
 
 