All My Friends Part 2
{{Infobox film
| name           = All My Friends Part 2 (Amici miei - Atto II°)
| image          = AmicimieiAttoposter.jpg
| image size     =
| caption        = Poster
| director       = Mario Monicelli
| producer       = Aurelio De Laurentiis  Luigi De Laurentiis
| writer         = Leonardo Benvenuti  Piero De Bernardi  Tullio Pinelli  Mario Monicelli
| starring       = Ugo Tognazzi  Gastone Moschin  Philippe Noiret  Adolfo Celi  Renzo Montagnani
| music          = Carlo Rustichelli
| cinematography = Sergio DOffizi
| editing        = Ruggero Mastroianni
| distributor    = Filmauro
| released       = 1982
| runtime        = 125 min
| country        = Italy
| language       = Italian
| budget         =
| gross          =
}} 1982 Italy|Italian comedy film directed by Mario Monicelli. It is the sequel to Amici miei of 1975. The movie features Paolo Stoppa in one of his last roles. The last chapter of this saga is Amici miei - Atto III, directed by Nanni Loy (1985).

==The main characters and their characteristics==
  ("supercazzola"). Count Mascettis the best in making this joke of stopping a person and quickly communicate a flurry of words and verbs that have no logical connection and meaningful to the other person remains baffled and confused in the face of what he says Mascetti. In most cases, the party is so upset by the play of the count, who believes he can not understand anything, and even the Italian language, or include places or names mentioned by Mascetti as a "stretcher", "area code" or "inspector wisps" and indicates the desired object, while remaining very surprised. Meanwhile, friends are laughing heartily and make the other person look like a moron. Another characteristic element of the friends is teasing high alert as authorities or ministers of the church and especially invent scurrilous and vulgar songs from important symphonies such as The Barber of Seville. The primary Sassarli instead helps to save the day when the police or other people get angry, because he pretends to know all the magistrates of the city of Florence and most of the politicians in the area. Although the "carnival of illusions" of friends is bound to end one day, the cheerful and witty companions do not think a lot and always find a way to be happy, even in bad situations.

==Plot summary==
The four old friends meet on the grave of the fifth of them, Giorgio Perozzi, who died at the end of the first episode. Putting a conversation in front of the tomb of his friend, Lello Mascetti, Guido Necchi, Rambaldo Melandri and Alpheus Sassaroli reminiscent of the great and exciting filth and jokes that made George Perozzi, the friend died of a heart attack. So the movie comes back from the Eighties to 1968 in Florence when the wife of Perozzi was tired of his infidelities daily with eager bakers wife. While Perozzi liked to put the horns to his wife and grow in the miserable son of Count Mascetti studio, since he did not want his feet while he enjoyed in his escapades, Melandri falls in love with a beautiful lady with a rich dowry. The two marry and friends continue to combine their trouble, and then the poor Rambaldo finds himself also to stage a performance of the Passion of Jesus because of the extreme religiosity of the new wife. His friends mock and then find themselves facing the terrible flood that hit the city in that year, making the overflow Arno. Beds, furniture and people floating through the old streets of the old town and the wife of Perozzi he discovers his wife clinging to the basket. Returning to the present the four friends burst into laughter remember the details, but now they face the difficulties of the present. The minor child of Mascetti was impregnated by a crude and vulgar dishwasher and below is also haunted by debts to pay for the rental of the studio. He thinks to rely on a loan shark (Paolo Stoppa), but does nothing but accumulate its problems. Then the three friends involved in the rescue and Sassaroli, an expert surgeon, slimy man who pretends to have a problem with kidney stones. Thanks charade loan shark pay their money Sassaroli that lends them to the count Mascetti to pay the rent. Among the many adventures of Mascetti there is also that the sexual encounter with a Spanish dancer but was forced to leave quickly because he copulating with her in a luxurious room tree and does not know how to pay the bills. Also threatens to kill himself but in the end is too cowardly and reconsiders. One day Mascetti finds himself talking about his noble origins, known as Florence since the year 1200, but my friends mock him, and the Count is angry because he, as much rich in what was youth, now finds himself almost begging. To the fury Mascetti is also affected by a heart attack that forced him to stay for the rest of his life in a wheelchair, but friends always find a way to keep him happy and continue to have fun with him in their gypsy.

==Cast==
{|class="wikitable"
!Actor!!Portrayed
|- Ugo Tognazzi Conte Lello Mascetti
|- Gastone Moschin Rambaldo Melandri
|- Adolfo Celi Professor Sassaroli
|- Renzo Montagnani Guido Necchi
|- Milena Vukotic Alice Mascetti
|- Franca Tamantini Carmen
|- Angela Goodwin|| Nora Perozzi
|- Alessandro Haber Widower
|- Domiziana Giordano Noemi
|- Paolo Stoppa Savino Capogreco
|- Philippe Noiret||Giorgio Perozzi
|- Carmen Elisabete Twister
|}

==External links==
*  

 

 
 
 
 
 
 
 
 
 