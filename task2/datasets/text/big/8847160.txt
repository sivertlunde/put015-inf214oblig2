Million Dollar Mystery
 
{{Infobox film
| name           = Million Dollar Mystery
| image          = Million dollar mystery.jpg
| caption        = Theatrical release poster
| director       = Richard Fleischer
| producer       = Stephen F. Kesten
| writer         = Rudy De Luca Tim Metcalfe Miguel Tejada-Flores
| narrator       = 
| starring       = Tom Bosley Jamie Alcroft Royce D. Applegate Penny Baker Eddie Deezen Mack Dryden
| music          = Al Gorgoni
| cinematography = Jack Cardiff
| editing        = John W. Wheeler
| distributor    = De Laurentiis Entertainment Group
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $989,033 (USA)
| preceded_by    = 
| followed_by    = 
}} promotional tie-in for Glad (company)|Glad-Lock brand bags. This was the final feature-length film directed by Richard Fleischer.

==Plot summary== heart attack and before dying, reveals to onlookers the location of the first million dollars.

The occupants of the diner head out on a mad dash to find the loot. When they find the money, they lose it in a mishap.  They follow clues to the next million and lose it as well. After finding and losing the third million, the movie ends. During the closing credits, one of the characters informs the audience that there is a million dollars somewhere in the USA and if they follow the clues in specially marked Glad-Lock bags, they have the chance to win $1 million.

The plot was like the 1963 film Its a Mad, Mad, Mad, Mad World.

==Cast==
*Tom Bosley as Sidney Preston
*Eddie Deezen as Rollie
*Wendy Sherman as Lollie
*Rick Overton as Stuart Briggs
*Mona Lyden as Barbara Briggs
*Douglas Emerson as Howie Briggs
*Royce D. Applegate as Tugger
*Pam Matteson as Dotty Daniel McDonald as Crush
*Penny Baker as Charity
*Tawny Fere as Faith
*LaGena Hart as Hope
*Mack Dryden as Fred
*Jamie Alcroft as Bob
*Rich Hall as Slaughter Buzzárd
*Gail Neely as Officer Gretchen
*Kevin Pollak as Officer Quinn
*H.B. Haggerty as Awful Abdul
*Bob Schott as Bad Boris
*Peter Pitofsky as Toxic Werewolf
*Greg Travis as 2nd Toxic Man
*Tommy Sledge as Private Eye
*Christopher Cary as Chuck
*Rudy De Luca as Money Counter Mark Regan as Newscaster
*John Gilgreen as Motel Manager
*Katie La Bourdette as Sledges Secretary
*Pat McGroarty as Gas Station Attendant #1
*Clark Coleman as Gas Station Attendant #2
*Paul Stader as Old Man in Car
*Rosemary Johnston as Old Woman in Car David Trim as Scout Leader Susann Benn as Stewardess
*John Hammil as Pilot
*Gary Kelson as Co-Pilot
*Sal Lopez as Hijacker
*Augustine Lam as Tour Guide Jack Carpenter as Biker in Window
*Andy Epper as Biker #2

While performing a routine stunt for this film, legendary stuntman Dar Robinson died on November 21, 1986.

==Overview==
Producer Dino De Laurentiis conceived the idea for Million Dollar Mystery when he visited New York and saw a row of people lining up for what he presumably thought was a movie. A companion told De Laurentiis that they were actually lining up for lottery tickets. 

Glad Bags sponsored a sweepstakes timed for the films release. The company gave away entry forms, and the audience would fill out these forms with their answer to where the last million is hiding, based on clues given in the film. De Laurentiis said of the film: "This is a really broad comedy with car chases, designed for the young major moviegoing audience, about 12 to 24 years old. The sweepstakes gives us the potential to reach even more people &ndash; the infrequent moviegoer, the person more interested in winning a million dollars than in going to the movies, and these are the kind of people who use Glad Bags, housewives who maybe go to the movies once or twice a year." 

De Laurentiis had high expectations for the film, but it did not turn out to be a hit. The winner of the contest ended up being 14-year-old Alesia Lenae Jones of Bakersfield, California, who successfully guessed that the loot was hidden in the nose of the Statue of Liberty. 

==Award nominations==
Golden Raspberry Awards Worst Original Song, Barry Mann & John Lewis Parker (1988) Worst Supporting Actor, Tom Bosley (1988)
*Nominated: Worst Supporting Actor, Jamie Alcroft (1988)
*Nominated: Worst Supporting Actor, Mack Dryden (1988)

==External links==
*  
*  
*  
*  

==References==
 

==See also== Stunts that have gone wrong

 

 
 
 
 
 
 
 
 