Tiefland (film)
{{Infobox film
| name           = Tiefland
| image          = Tiefland-Farbplakat.jpg
| caption        = 
| director       = Leni Riefenstahl
| producer       = Leni Riefenstahl Josef Plesner
| writer         = Leni Riefenstahl Harald Reinl
| based on       =  
| narrator       =
| starring       = Bernhard Minetti Leni Riefenstahl
| music          = Herbert Windt Giuseppe Becce
| cinematography = Albert Benitz
| editing        = Leni Riefenstahl
| distributor    = Allianz Verleih Deutsche Cosmopol Taurus (video)
| released       =  
| runtime        = 98 minutes
| awards         = West Germany German
| RM 8.5 million
}} German film eponymous opera composed by Eugen dAlbert and written Rudolph Lothar and based itself in the catalan play Tiefland by Àngel Guimerà. The film co-stars Bernhard Minetti, and is Riefenstahls last feature film as both director and lead actress.

Riefenstahl started to develop the script in 1934, with the movie being shot between 1940 and 1944. However, it was not completed by the end of World War II and was eventually finalized and released on February 11, 1954. It was listed as the feature film with the longest production time in history by the Guinness World Records|Guinness Book of World Records.  Despite this record being surpassed by The Thief and the Cobbler (with 31 years of production from 1964 to 1995), Tiefland still stands as the live action film with the longest production time.

Riefenstahl’s movie is the second Tiefland film that is based on the opera, the first one being a silent film in 1922, directed by Alfred Licho, with Lil Dagover as the main actress. The earlier American silent movie Martha of the Lowlands (1914) was based on the English translation of a Spanish translation of Guimeràs play.

==Plot summary== Pyrenean mountain Catalan lowlands (northeastern Spain), the construction of a canal is completed and diverts water from the farms and fields of the peasants to support the prized bulls of the landowner, Don Sebastian, marquis of Roccabruno. The request of the peasants for water is arrogantly rejected. He has, however, significant debts and needs money. The rich Amelia plans to marry him, but he offends her. Martha, a “beggar dancer”, has come to the village, and entertains the people. Sebastian sees her and takes her to his castle, enchanted by her beauty and grace. He keeps her as his mistress in a "golden cage". Martha pleads with him to listen to the plight of the peasants, but he rejects their request again. Seeing his arrogance and inhumanity she runs away. She collapses in exhaustion in the mountains where Pedro finds her and takes her to his hut. Sebastians men locate her and return her to the castle. Sebastian in dire need to settle his finances conjures a plan. He will marry Amelia, but to keep Martha as a mistress, - he wants her married to somebody he can manipulate and control. Pedro is asked to marry her and installed in a mill under Sebastian’s control. For this Martha despises Pedro at first, but once she realizes that he married her out of love she responds. Sebastian arrives to be with his mistress. A fight ensues, and Pedro strangles him like he had done with the wolf. In the final scene Pedro and Martha walk up to the mountains.

==Production== Rosengarten of the Dolomites, as well as the Babelsberg Studios in Berlin. Near Mittenwald, the Spanish village of Roccabruno was reconstructed. Although the German press anticipated the release of the movie in 1941, the production proved to be much more difficult and costly and outdoor shooting lasted until 1944. In 1941, Goebbels had complained about the "waste of money", and one year later called it a "rat’s nest of entanglements".  Problems were compounded by Riefenstahl’s depression and other ailments, poor weather, accidents, and the difficulty of getting actors and staff organized during the war.  Eventually, at a cost of about  8.5 million Reichsmark, Tiefland  was the most expensive black-and-white movie produced in Nazi Germany.  After the bombing of the Babelsberg studios in Berlin, the Barrandov Studios in Prague were used to further the work, and by the time the war came to the end, Riefenstahl was in the editing and synchronization process at Kitzbühl.

Riefenstahl took the female lead role of Martha, a step that was not originally planned; however, she found no actress to her liking available at the time, and so she did it. Luc Deneulin. Essay. DVD version (2006) of Tiefland. Also accessible here    Her last major role had been a decade before. She may have been attracted to play a dancer, as dancing was her original artistic calling. She later regretted the decision, as she looked much too old by her own account. "When I saw myself on the screen, I was embarrassed. There was no doubt about it, I was miscast."  Critics seemed to agree: she was over forty, while her lover was played by the 23 year-old Franz Eichberger.

Although Tiefland was Riefenstahl’s movie, she had excellent people to work with. Bernard Minetti played Sebastian. Arnold Fanck, Veit Harlan and Georg Wilhelm Pabst all gave directorial assistance at one time or the other. Harald Reinl, who co-wrote the script, choreographed her dancing scene. Herbert Windt and Giuseppe Becce worked on the musical score that was inspired by Eugen dAlbert’s opera. The camera work delivered later well-received nature shots of the Karwendel and Dolomite mountains. The wrestling scene of Eichberger with the only half-tamed wolf was supervised by Bernhard Grzimek. 

After the war, the film was confiscated and kept by French authorities for several years, but eventually returned to her. Four reels of film were missing when Riefenstahl received the film, notably the scenes shot in Spain. Despite efforts she failed to retrieve the missing footage. After its final editing, the movie was released in 1954.

Riefenstahl deposited a quantity of unused Tiefland material with the Bundesarchiv, the German national archives. 

==Release==
The film secured distribution agreements for Germany, Austria and the United States in 1954. The world premiere was held on 11 February 1954 in Stuttgart. 

Riefenstahl embarked on a personal appearances tour of Austria in support of the film. She described the tour as "a roaring success". 

The film was also screened at several film festivals. This included the 1954 Cannes Film Festival where it was screened in the out of competition category. 

In 1981, the movie was released in the United States with a limited run.    When Riefenstahl was ninety, she negotiated the VHS release as part of the Leni Riefenstahl Collection. The 2006 DVD version has an essay by Luc Deneulin on the background to the movie. 

==Reception==
The film was released to a mixed reception. Riefenstahl regarded the response as "objective". Some critics decided that the style and topic of the movie appeared dated and out of touch, and the burden of her name made it unwelcome. Most ignored her acting performance, although this was recognised by those that commented as a weak performance. Almost all reviewers acknowledged photographic effects of unusual beauty and praised her direction.   

Jean Cocteau, then chairman of the 1954 Cannes Film Festival was struck by its "Breughel-like intensity" and  "the poetry of the camera." He offered to provide French subtitles himself and attempted to persuade the West German government to make the film its official entry. 

On the 1981 American re-release, The New York Times reviewed the film, deciding that the village scenes could have been more successful but heaped praise on the mountain footage:
 

The revisionist view of the 1990s has suggested that the film is a criticism of Nazism. Historians have claimed that the films true value is as a psychobiography, stressing the film as a political allegory rather than melodrama.    Sebastian represents a totalitarian government that tramples the rights and needs of the people, and Pedro is a hero who is "naive" and apolitical, only doing what he thinks is right. Even the wolf could be construed as an allegory for Hitler. Riefenstahl insisted that none of her movies had any political messages, and only conceded that this movie was her "inner emigration". Other interpretations saw the marquis as a representant of a Hitler figure, Martha as a stand-in for a repentant Leni, an unfortunate tempted by opportunism. 

==Comments==
 
  gypsy and dancing is "in (her) blood". She stands on her own, beyond the classes of the common people or the nobility. Martha rejects the temptation and trappings of power, once she realizes that this is at the cost of the welfare the people. She also refuses Pedro initially until she recognizes his motive. She insists on making her own choice.

==Controversy== Roma and Sinti background who were held in Nazi collection camps, so-called "Zigeunerlager". Fifty-one Roma and Sinti prisoners were chosen from the Maxglan-Leopoldskron camp (near Salzburg) for filming in the Alps in 1940, and, in 1942, at least 66 Roma and Sinti prisoners were taken from the Marzahn camp for scenes at Babelsberg.  These extras are seen, for instance, in the dancing sequence in the tavern, and  Sinti children run alongside Pedro when he comes down from the mountain to marry Martha. 

In three denazification trials after the war, Riefenstahl was accused of Nazi collaboration and eventually termed a "fellow traveler"; however, none of the Sinti was asked to testify. The issue surfaced after the German magazine Revue published the use of these extras in 1949 and indicated that they were forced labor and sent later to Auschwitz where many of them perished in the Holocaust. While some of the surviving Sinti claimed that they were mistreated, others dissented. Riefenstahl claimed that she treated these extras well, and that she was not aware that they were going to be sent to Auschwitz. At one point she even insisted that, after the war, she had seen "all the gypsies" who had worked on the film.

In 1982, Nina Gladitz produced a documentary Zeit des Schweigens und der Dunkelheit (Time of Darkness and Silence) and examined the use of these Sinti in the making of Tiefland. Riefenstahl subsequently sued Gladitz for defamation and while it was shown that she visited camps and selected Sinti for extras, Gladitz’ claim that Riefenstahl knew that they would be sent to Auschwitz had to be stricken from the documentary. Gladitz, however, refused to do so, and thus her film has not been shown since. 
  one hundred extermination of the gypsies. As a consequence of the case Riefenstahl made the following apology, "I regret that Sinti & Roma had to suffer during the period of National Socialism. It is known today that many of them were murdered in concentration camps." 

==Influence==
Robert von Dassanowsky indicates that James Camerons film Titanic (1997 film)|Titanic echoes and even copies much of what can be found in Tiefland. The setting, of course is different, but the woman is tempted on one side by power and riches, and on the other side by the man-child character who offers true love. Dassanowsky sees strong parallels in the key scenes of both films. 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 