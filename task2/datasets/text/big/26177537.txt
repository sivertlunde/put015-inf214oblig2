Out of the Tiger's Mouth
 
{{Infobox Film
| name           = Out of the Tigers Mouth
| image          = 
| caption        = 
| director       = Tim Whelan, Jr.
| producer       = Wesley Ruggles
| writer         = Tim Whelan, Jr. Wesley Ruggles
| narrator       = 
| starring       = Loretta Han-Yi Hwong
| music          = 
| cinematography = Emmanuel I. Rojas
| editing        = Jack Ruggiero
| distributor    = 
| released       =  
| runtime        = 78 minutes
| country        = United States
| language       = English
| budget         = 
}}

Out of the Tigers Mouth is a 1962 American drama film directed by Tim Whelan, Jr. It was entered into the 12th Berlin International Film Festival.   

==Cast==
* Loretta Han-Yi Hwong - Little Moon
* David Fang - Peaceful
* Lilian Wai - Grandma Yang
* Yang Juo Ching - Mme. Pang
* Mario Barri - Mario
* Lolita Shek - Su Mei
* Ngai Fung - Boatman Feng (as Fung Yi)
* Victoria Chan - Beggar girl

==See also==
*List of American films of 1962

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 