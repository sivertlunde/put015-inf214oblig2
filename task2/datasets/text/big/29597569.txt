Shark Attack 2
{{infobox film
| image        =
| caption      = DVD cover
| director     = David Worth
| producer     = 
| writer       = Scott Devine  William Hooke
| starring     = Thorsten Kaye Nikita Ager
| music        = Mark Morgan
| studio       = Nu Image Films Lionsgate Home Entertainment
| released     =
| runtime      = 88 minutes
}} horror film based in Wick, Scotland.

==Plot== Shark Attack are back, this time choosing Cape Town, South Africa as their hunting ground. Two sisters, Amy (Caroline Bruins) and Samantha Peterson (Nikita Ager), while diving near a wreck in the reef are attacked by a shark; Samantha survives. One week later, Dr. Nick Harris and his assistant get the shark and install it as a new attraction at Water World - a Sea World rip-off. When the shark escapes, Nick and Samantha go to hunt it and discover that there are 6 mutant Great Whites living in a cave near the beach, and the Water Worlds surfing competition might be the next victim! Now they team up with Roy - a Discovery Channel shark hunter - to kill the beasts and save Cape Town.

==Cast==
* Thorsten Kaye as Dr. Nick Harris
* Nikita Ager as Samantha Peterson
* Caroline Bruins as Amy Peterson
* Danny Keogh as Michael Francisco
* Warrick Grier as Morton
* Morne Visser as Mark Miller
* Daniel Alexander as Roy Bishop
* Sean Higgs as T.J.
* Ian Jepson as Jeff
* Alistair Cloete as Tom Miller
* Marc Derman as Kenny
* Stephen Fry as Chuck
* Sean Michaels as News Ancher Man

==Sequel==
It was followed by 2   in 2002, and Shark Zone in 2003.

==Reception==
Critical response was mostly negative. It holds 27% (or 2.7 stars out of 10) on IMDb.

==External links==

 
 
 
 
 
 

 