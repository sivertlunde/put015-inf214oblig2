Please, Not Now!
{{Infobox film name            = Please, Not Now! image           = La Bride sur le cou.jpg director        = Roger Vadim producer        = Jacques Roitfeld writer          = Claude Brule Roger Vadim starring        = Brigitte Bardot Michel Subor Claude Brasseur Jacques Riberrolles music  James Campbell cinematography  = Robert Lefebvre distributor     = Anchor Bay Entertainment, Inc. released        = 1961 runtime         = 89 min. language  French
|budget          =  gross = 2,815,047 admissions (France) 
}} French title French comedy film released in 1961, directed by Roger Vadim and starring his former wife, Brigitte Bardot.

==Synopsis==
Brigitte Bardot plays Sophie, a young model who discovers her boyfriend is thinking of leaving her for another woman. She plots deadly revenge in this French romantic comedy.

Sophie is not content with the status quo in her relationship with high-flying reporter boyfriend Philippe (Jacques Riberolles), but she is even less impressed when a wealthy American girl, Barbara (Josephine James), threatens to steal him from her. She resolves to win Philippe back or murder her rival. She enlists her doctor friend Alain (Michel Subor) to aid her in making him jealous. Sophie and Alain travel from Paris to the ski resorts of Italy to chase Philippe and Barbara, treating us to marvelous French comedy in the mischief that ensues.

Directed by Bardot’s former husband Roger Vadim, the film features a "nude" dance sequence, in which Bardot wore a body stocking. It was considered risqué at the time of the film’s release and provoked controversy. Michel Subor was cast as Bardot’s fraught suitor, in a role that made him famous.

==Cast==
*Sophie, Brigitte Bardot
*Alain, Michel Subor
*Philippe, Jacques Riberolles
*Barbara, Josephine James

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 

 