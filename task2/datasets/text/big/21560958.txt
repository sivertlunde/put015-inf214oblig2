Corrections (film)
 
 
 
{{Infobox film
|  name    = Corrections Bob Franklin
|  starring = Brooke Satchwell Roz Hammond Jo Stanley Bob Franklin
|  distributor = Boxing Clever Pictures
|  released =  
|  producer = Darren McFarlane Ben Grogan
}}

Corrections is a short film, taking viewers on a disturbing trip through the lens of a horror fan.

== Synopsis ==
The film explores societys fascination with body image. The film, penned by the actor/comedian, is dark and intense and will show how adept he is with working in alternative genres.

== Production ==
Corrections was shot entirely on location in Parkville, Melbourne over two days, using the Panavised Sony F900, with Panavision HD lenses.  Corrections.

==Cast==
*Brooke Satchwell .... Amy Perrin
*Jo Stanley .... Sophie Kroll
*Roz Hammond .... Receptionist

==External links==
* 
* 

 
 
 
 
 


 