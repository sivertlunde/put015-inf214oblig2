Three O'Clock High
{{Infobox film
| name = Three OClock High
| image = Three o clock high p.jpg
| image_size = 215px
| alt = 
| caption = Theatrical release poster by Drew Struzan
| director = Phil Joanou
| producer = David E. Vogel
| writer = Richard Christian Matheson Thomas Szollosi
| starring = Casey Siemaszko Annie Ryan Richard Tyson Stacey Glick
| music = Tangerine Dream
| cinematography = Barry Sonnenfeld
| editing = Joe Ann Fogle
| distributor = Universal Studios CIC Victor Video (Japan, VHS) Victor Video (West Germany & Brazil, VHS)
| released =  
| runtime = 101 minutes
| country = United States
| language = English
| budget = $5 million
| gross = $3,685,862 
}}
Three OClock High is a 1987   (Richard Tyson) and devotes most of his school day to avoiding a fistfight with the bully at 3:00 p.m.
 Ogden High cult classic.

==Plot summary== Anne Ryan) to Weaver High School, where the students this morning are gossiping about the new student Buddy Revell (Richard Tyson), a violent delinquent who has transferred in today from Continuation, a school for at-risk students.
	
Jerrys first hour is at the school newspaper where his best friend, Vincent Costello (Jonathan Wise), is the editor. The teacher has the idea of doing an article about Buddy to welcome the "new kid" and she assigns Jerry to do the interview. In a mens room, Jerry sees Buddy and clumsily attempts to introduce himself and brings up the idea of the article. Through a series of poorly chosen statements, Jerry realizes he is only making Buddy angry and ultimately decides to cut his losses and tells Buddy to "...just forget this whole thing happened", giving Buddy a friendly tap on the arm. Buddy, who does not like being touched by others, responds by tossing Jerry against a wall and stating that the two must fight in the parking lot after school at 3 oclock.

With little more than six hours until the encounter, Jerry tries different strategies to avoid the fight. Trying to reason with Buddy in the hallway doesnt work. Vincent suggests that he plant a switchblade in Buddys locker to get him kicked out of school (which backfires). Brei advises him to simply skip school, but when Jerry tries to drive away, he finds the switchblade stuck in the steering wheel, and the car ignition wires cut. Trying to run, Jerry is caught by an overzealous school security guard, Duke (Mitch Pileggi), who finds the switchblade and takes Jerry to the office of Mr. Dolinski (Charles Macaulay), the Dean of Discipline. Seeing an otherwise perfectly clean school record, the now suspicious Mr. Dolinski informs Jerry that he will be keeping his eye on him from now on and lets him go. 

Jerry makes several other attempts to avoid the fight: he steals money from the schools student store, which he manages, and pays an upperclassman to take care of Buddy; he tries to get thrown into detention by making a pass at his English teacher; he lets Buddy cheat by copying his answers during his final period math quiz. All of these efforts fail. 

Ultimately after trying to befriend Buddy, he offers him the cash he took earlier to call off the fight. Buddy accepts the money, but scornfully calls Jerry "the biggest pussy I ever met in my life." Jerry, seized with self-loathing and anger, decides to confront Buddy and demand his money back. When Buddy refuses, Jerry insists that he is no coward and declares that the fight is on. 

The fight begins in a parking lot, with hundreds of eager students observing. Principal ORourke, Mr. Dolinksi, Duke, Franny and even the guilt-plagued Vincent attempt to intervene, but Buddy easily disposes of them. Jerry, though obviously out-matched, still manages to stand his ground and ultimately uses Buddys brass knuckles to knock out the bully and win the fight.

The next day, many students show their appreciation to Jerry for giving them such a great fight (one student had set up a betting pool and did quite well), and begin buying individual sheets of paper from the school store for $1 each (at the suggestion of a remorseful Vincent) to help Jerry make up the lost student store cash. Buddy shows up silencing the bustling crowd and begrudgingly shows respect by returning the $350. Weaver High is now filled with gossip as Jerry replaces Buddy as the hot talk of the school, with the rumors having a wide and humorous range of alignment with the truth.

==Cast==
* Casey Siemaszko as Jerry Mitchell Anne Ryan as Franny Perrins
* Richard Tyson as Buddy Revell
* Stacey Glick as Brei Mitchell
* Jonathan Wise as Vincent Costello
* Jeffrey Tambor as Mr. Rice
* Philip Baker Hall as Detective Mulvahill John P. Ryan as Mr. ORourke
* Theron Read as Mark Bojeekus
* Liza Morrow as Karen Clark
* Guy Massey as Scott Cranston
* Mike Jolly as Craig Mattey
* Charles Macaulay as Voytek Dolinski
* Caitlin OHeaney as Miss Farmer
* Alice Nunn as Nurse Palmer
* Paul Feig as Hall monitor
* Yeardley Smith as Cheerleader
* Brian White as Bystander

==Reception==
Three OClock High opened in 849 theatres nationwide on October 9, 1987; gaining a $1,506,975 opening weekend gross. The total lifetime gross of the film is approximately $3,685,862, earning 40.9% of its total gross during opening weekend. 

Critical reviews were mixed. Rotten Tomatoes ranks the film at 67%, based on 9 reviews.   , a 1980 drama about a bullied high school student, explores the same themes as Three OClock High but is a far better film.

==See also==
* High Noon, 1952 film
*Three OClock High (soundtrack)|Three OClock High (soundtrack)
*List of teen films
*List of cult films

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 