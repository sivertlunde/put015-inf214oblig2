Jaago (2004 film)
 
 

{{Infobox film
| name           = Jaago
| image          = Jaago_poster.jpg
| caption        =
| director       = Mehul Kumar 
| producer       = Mehul Kumar
| writer         = K.K. Singh
| starring       = Manoj Bajpai Sanjay Kapoor Raveena Tandon Hansika Motwani
| music          = Sameer Sen
| cinematography = Mahendra Rayan
| editing        = Yusuf Sheikh
| distributor    = Mehul Movies (as M. K. Pictures)
| released       =  
| runtime        =
| country        = India
| awards         =
| language       = Hindi, English
| budget         =
| gross          =   http://ibosnetwork.com/asp/filmbodetails.asp?id=Jaago 
}}
 crime thriller film. It stars Sanjay Kapoor, Raveena Tandon, Manoj Bajpai and Hansika Motwani.<ref name="Bold theme, poor screen play
">   The film is based on a true incident from August 2002, in which a mentally challenged 10-year-old girl was raped on her local commuting train.       The plot deals with the rape and death of a ten-year-old schoolgirl and the dramatic quest for justice by a lone but determined police inspector.

== Plot ==
After a long day in school, ten-year-old Shruti Varma is accidentally locked in the school house, and by the time she is found and released, it is already night time. She boards a commuter train home, with an elderly couple and their daughter being her only company in the compartment. Then a trio of three young men, drug-hazed and violent, enter the wagon, notice the young girl and brutally gang-rape her. The other passengers are too timid to intervene; only after the culprits have left the train do they call the police. Shruti is taken to a hospital, but the trauma of the rape proves too much for her to bear, and she eventually dies from a massive shock in front of her anguished parents, Shrikant and Shradha Varma.

Kripa Shankar Thakur, an honest and upright inspector of the Indian police, is entrusted with the case. He begins to sympathise with the parents, especially after coming face to face with the indifference and selfishness of the authorities (even within the police force itself) and everyone who is – even indirectly – responsible for Shrutis rape and death. In addition, the culprits turn out to be the sons of wealthy and influential citizens, making it highly difficult for justice to be dealt out in the regular way.

After tracking down the identities of the witnesses, Shankar sets a trap to catch the offenders in the act: Dressed provocatively, Shradha acts as bait by boarding the same train her daughter has taken. Indeed, the three culprits appear and attempt to rape her as well. In her rage over her daughters suffering at the hands of these men, Shradha wrests a knife from one of them and viciously stabs her attacker to death; consequently, she is arrested and detained for manslaughter.

More determined than ever, Inspector Shankar continues his pursuit for justice, even with the weight of corruption rising to obstruct him; he meets threats with counter-threats and attempts at bribery with the arrest of the perpetrators, and even gains popular support from influential people in the law enforcement. But this is not enough for Shrikant: with his desperation having become unbearable, he bursts into the courthouse where the offenders are on preliminary trial, and shoots one of them.

Just when the cause seems lost for the surviving rapist, his lawyer Satwani resorts to vicious methods: he manages to buy off Shankars friend Sawant, who is to kill the witnesses before they can testify in court. But Shankar proves to be too resilient; he kidnaps Sawants wife and son, forcing the latter to release the witnesses. With the familys testimony and the moving pleas of Shrikant and Shradha for more justice, and Shankars own accusation of the corruption in Indias political system, the presiding judge acquits the Varmas and sentences the last of the culprits to be executed, by the hands of those directly involved in the case.

Following the execution, the film ends with the judge walking towards a sunset beach, saying by voiceover: "After delivering the verdict, I handed in my resignation. I dont know how those who know the law will react to my decision. What I do know, however, is that I have done justice to my soul. I have truly awakened," and then turning to the audience with the reminder: "Its time you awakened too."

== Cast ==
* Sanjay Kapoor –  Shrikant Varma
* Raveena Tandon –  Shradha Varma
* Manoj Bajpai –  Inspector Kripa Shankar Thakur 
* Puru Raajkumar –  Elias Ansari
* Hansika Motwani  –  Shruti
* Rupal Patel –  mother of one of the offenders (Cameo)

==Box office==
Jaago opened to a measly 30 per cent collection.  Finally it grossed  . 
 
==References==
 

== External links ==
*  
 
 
 
 
 
 