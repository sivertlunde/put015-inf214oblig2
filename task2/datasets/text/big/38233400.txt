Doghat Tisra Aata Sagala Visara
   
 
}}
{{Infobox film
| name           = Doghat Tisra Aata Sagala Visara
| image          = Doghat Tisra Aata Sajda Visara.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = Kaanchan Adhikkari
| producer       = Kaanchan Adhikkari
| writer         = 
| starring       = Makarand Anaspure Prasad Oak Mrunmayee Lagoo Mohan Joshi
| music          = Avdhoot Gupte
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Marathi
| budget         = 
| gross          = 
}}
Doghat Tisra Aata Sagala Visara is a 2008 Marathi film produced and directed by Kaanchan Adhikkari. The film is based on the delicate issues of trust between a husband and wife, and how a simple friendship can snowball into major misunderstanding. The film was a major hit at the Box office was house-full at all theatres. 

== Synopsis ==
Sameer and Harshada are a newly married couple living in Mumbai. Everything is hunky dory, until Damodar, Sameers childhood friend from Dengdi Pimpalgaon Village, shows up and invades the couples privacy. At first Harshada dislikes to the intruder, but later realizes that Damodar is a simple and noble-hearted person. She warms up to him and soon the two become good friends.

The new-found friendship irks Sameer and gradually drives a wedge between husband and wife. Sameer leaves Harshada and storms off. Professionally, Sameer must deal with another problem, his malevolent and overprotecting father-in-law. Damodar, takes it upon himself to help sort out the issues between the couple and reunite them.

== Cast ==

The cast includes Makarand Anaspure, Prasad Oak, Mrunmayee Lagoo, Tejasvini, Mohan Joshi & Others. 

==Soundtrack==
The music was directed by Avdhoot Gupte.

== References ==
 
 

== External links ==
*  
*  

 
 
 


 