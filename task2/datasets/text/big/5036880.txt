Soccer Dog: European Cup
 
 
 
 
 
{{Infobox Film |
|  name           = Soccer Dog: European Cup
|  image          = SOCCERDOGSEQUEL.jpg 
|  caption        = 
|  director       = Sandy Tung 
|  producer       = John H. Brister 
|  writer         = John E. Deaver 
|  starring       = Eric Don Nick Moran Jake Thomas Lori Heuring Scott Cleverdon Orson Bean Darren Reiher Frank Simmons  Joseph Williams 
|  cinematography = Duane Manwiller 
|  editing        = Chris Wong 
|  distributor    = Columbia TriStar Home Video 
|  released       =  
|  runtime        = 88 min.
|  country        = United States 
|  language       = English 
|  budget         = 
}}

Soccer Dog: European Cup is the 2004  , about a dog with an uncanny ability to play soccer.

==Synopsis==
 
The plot is about the dog from the first film who has been reclaimed by his real owners and is taken to Europe to play in the championship, which he eventually wins.

==Cast==
*Eric Don &ndash; Mickey
*Nick Moran &ndash; Bryan MacGreggor
*Jake Thomas &ndash; Zach Connolly
*Lori Heuring &ndash; Veronica Matthews
*Scott Cleverdon &ndash; Alex Foote
*Orson Bean &ndash; Mayor Milton Gallagher
*Darren Reiher &ndash; William Wallace
*Frank Simons &ndash; Dr. Oddlike Jack McGee &ndash; Knox
*Tony Collins &ndash; Referee
*Jeremy Mersereau &ndash; Dog-hating fan (uncredited)

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 


 
 