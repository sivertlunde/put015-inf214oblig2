Telling You
{{Infobox film
| name = Telling You
| image =Poster of the movie Telling You.jpg
| caption = Theatrical film poster
| director = Robert DeFranco
| producer = Christopher DeFranco David DuPuy
| writer = Denis Flood Marc Palmieri
| starring = Peter Facinelli Andy Berman Robert DeFranco Gary Wolf Dash Mihok Richard Libertini Jennifer Love Hewitt Gina Philips Jennifer Jostyn Matthew Lillard
| music = Sean Hall  Russ Landau Tom Romero
| cinematography = Mark Doering-Powell
| editing = Jonathan Cates Louis F. Cioffi
| distributor = Division I Entertainment Inc.
| released = 1998
| runtime = 94 min.
| country =   English
| budget =
| gross =
}}
 1998 romantic comedy film with some drama elements. The film was directed by Robert DeFranco and distributed by Miramax. Its filming location finds place in North Hollywood. It was released on August 7, 1998.

==Plot==
Two college graduates find themselves back home in New Jersey stuck behind the counter of a pizza parlor and frustrated about their lifes perspectives, while their friends move on, struggle to find a new direction for their lives. Dennis feels overtaken by those he outstripped at high school while Phil re-encounters an ex-girlfriend he left behind. In the meantime, dealing with problems created by some money missing from the restaurant, the pair begin meeting old acquaintances and trying to pull themselves up and reorganize their plans for the future.

==Cast==
*Peter Facinelli as Phil Fazzulo
*Andy Berman as Howard Gurtler
*Robert DeFranco as Steve Fagan
*Gary Wolf as John Foley
*Dash Mihok as Dennis Nolan
*Richard Libertini as Mr. P
*Jennifer Love Hewitt as Deb Freidman
*Gina Philips as Kristen Barrett
*Jennifer Jostyn as Beth Taylor
*Frank Medrano as Sal Lombardo
*Jensen Daggett as Susan
*Matthew Lillard as Adam Ginesberg
*Shanna Moakler as Cheryl Tangeray
*Charlotte Ayanna as Allison Fazio (as Charlotte Lopez)
*Marc Bossley as Tommy Spahn Stephanie Brown as Michelle Itzo
*Gerald Jennke as Michelle Itzos Neighbor
*Michelle Burke as Kristens Friend (as Michelle Thomas)
*Andrea Tiano as Maria (Sals Sister)
*Mike Muldoon as Jerry (Sals Brother-in-Law)
*Jack Rourke as Murph
*Lorin Eric Salm as Lettuce Man
*John Bachelder as John Batchelder
*Stephen Bundy DeFranco as Bundy
*Paul Anthony as Rough Stranger
*Kerri Kleiner as Allisons Friend
*Liz Reese as Telephone Montage
*Jennifer Foley as Amy
*Jon Cellini as the tough guy
*Joe Crusco as the Police Officer
*Sam T. Jensen as Little Kid 

==References==
 

==External links==
* http://www.imdb.com/title/tt0120859/

 
 
 
 