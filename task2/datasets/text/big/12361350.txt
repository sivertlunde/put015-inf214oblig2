Francis (1950 film)
{{Infobox film
| name           = Francis
| image          = Francis - 1950 - Poster.png Theatrical release lobby title card
| director       = Arthur Lubin Robert Arthur
| writer         = David Stern III Dorothy Reid (uncredited)  based on = the novel by David Stern
| starring       = Donald OConnor Patricia Medina
| music          =
| cinematography = Irving Glassberg
| editing        =
| distributor    = Universal-International
| released       = 8 February 1950 (New Orleans) First Play at Century Theater Set,
Los Angeles Times, 15 January 1950: D4.  
| runtime        = 91 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $2.9 million (US rentals) The Top Box Office Hits of 1950, Variety, 3 January 1951 
}} Robert Arthur, directed by Arthur Lubin, and stars Donald OConnor. The distinctive voice of Francis is a voice-over by actor Chill Wills.

Six Francis sequels from Universal-International followed this first effort.

During World War II, an American Army junior officer, Lt. Peter Stirling, gets sent to the psychiatric ward whenever he insists that an Army mule named Francis speaks to him.

==Plot== Second Lieutenant Peter Stirling (Donald OConnor) is caught behind Japanese lines in Burma during World War II. Francis, a talking Army mule (voiced by Chill Wills), carries him to safety. When Stirling insists that the animal rescued him, he is placed in a psychiatric ward. Each time Stirling is released, he accomplishes something noteworthy (at the instigation of Francis), and each time he is sent back to the psych ward when he insists on crediting the talking mule. Finally, Stirling is able to convince General Stevens (John McIntire) that he is not crazy, and he and the general become the only ones aware of Francis secret. In an effort to get himself released from the psych ward, Stirling asks Stevens to order Francis to speak, but the mule will not obey until it becomes clear that Stirling will be arrested for treason if he remains silent.

During one of his enforced hospital stays, he is befriended by Maureen Gelder (Patricia Medina), a beautiful French refugee. He grows to trust her and tells her about Francis. Later, a propaganda radio broadcast from Tokyo Rose mocks the Allies for being advised by a mule. This leads to the suspicion of Stirling or Maureen being a Japanese agent. The press is later informed that the absurd mule story was concocted in order to flush out the spy, and with Francis help, the real culprit is identified.

Francis is shipped back to the U. S. for further study, but his military transport crashes in the wilds of Kentucky. After the war, convinced that Francis survived the crash, Peter searches for and finally finds the mule still alive and well and talking!

==Cast==
*Donald OConnor as Peter Stirling
*Patricia Medina as Maureen Gelder
*Chill Wills as Francis the talking mule
*ZaSu Pitts as Nurse Valerie Humpert Ray Collins as Colonel Hooker
*John McIntire as General Stevens (as John McIntyre)
*Eduard Franz as Colonel Plepper
*Howland Chamberlain as Major Nadel
*James Todd as Colonel Saunders
*Robert Warwick as Colonel Carmichael
*Frank Faylen as Sergeant Chillingbacker
*Tony Curtis as Captain Jones (as Anthony Curtis)
*Mikel Conrad as Major Garber
*Loren Tindell as Major Richards Charles Meredith as Munroe, the banker

==Production==
Lubin became attached to the film in March 1948.  He was attracted to the light material because "as a movie fan myself, I am tired of watching neurotic material on the screen. I can easily skip the latest psychiatric spell binders, but Ive seen Miracle on 34th Street a half dozen times." 

In September 1948 it was announced Robert Stillman, Joseph H. Nadel, and Arthur Lubin had purchased the film rights from David Stern; Sillman would produce and Lubin would direct.  They set up the production at Universal where it was turned into a starring vehicle for Donald OConnor.  Dorothy Reid (widow of Wallace Reid) worked on the script.  

Lubin said it was the first time that he had a financial interest in any film he had made:
 Directing Francis gave me a new slant on picture making after some years of acting and producing in both New York and Hollywood. I love Francis, first because its good entertainment, and secondly because I own a bit of that ornery mule. So The Mmule Talks--: Reporter: By Arthur Lubin Director of "Francis". New York Times, 12 March 1950: X4.  
Filming started 7 May 1949 Mankiewicz Wins Film Guild Award: Directors Quarterly Prize Goes to Him for Letter to Three Wives, a Fox Picture,
 Thomas F. Brady, New York Times, 6 May 1949: 31.   and continued through to June.

Before its release in the U. S., Francis was first shown in January 1950 to Army troops stationed in West Germany.

Francis the mule was signed to a seven-year contract with Universal-International, according to an article in Newsweek magazine. Newsweek also reported that Francis entourage included "a make-up man, trainer, hairdresser, and sanitary engineer, complete with broom and Airwick."

==Reception==
The film was the eleventh biggest hit of the year in the U. S. 

In May 1950 it was announced Universal-International had purchased all rights to the character Francis from author David Stern, including the right to make an unlimited number of sequels. Francis Stories Are Bought By U.-I.: Studio Acquires All Rights to David Sterns Future Yarns About the Army Mule,
Thomas F. Brady, New York Times, 17 May 1950: 35.  

==Video releases==
The original film, Francis (1950), was released in 1978 as one of the first-ever titles in the new   (1951).

The first two Francis films were released again in 2004 by Universal Pictures on Region 1 and Region 4 DVD, along with the next two in the series, as The Adventures of Francis the Talking Mule Vol. 1. Several years later, Universal released all 7 Francis films as a set on three Region 1 and Region 4 DVDs, Francis The Talking Mule: The Complete Collection.

==References==
 

==External links==
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 