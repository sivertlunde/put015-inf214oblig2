Enfermés dehors
{{Infobox film name           = Enfermés dehors director       = Albert Dupontel writer         = Albert Dupontel Guillaume Laurant starring       = Albert Dupontel Claude Perron Hélène Vincent Yolande Moreau Bouli Lanners Terry Gilliam Terry Jones Jackie Berroyer released       =   country        = France language       = French music          = Denis Barthe, Vincent Bosle, Ramon Pipin, Jean-Paul Roy budget         = $5,3 million gross          = $4,464,847 
}} French movie directed by and starring Albert Dupontel, and written by him in collaboration with Guillaume Laurant. 

==Plot==
The story of a glue-sniffing homeless person who stumbles upon a policeman committing suicide and decides to put his abandoned uniform to good use. Initially this means using it to steal food from the police canteen but soon Roland discovers that wearing the uniform gives him certain powers and responsibilities, particularly tracking down the kidnapped child of a former porn star whose picture he had fallen in love with

==Cast==
* Albert Dupontel: Roland
* Claude Perron: Marie
* Nicolas Marié: Duval-Riché
* Hélène Vincent: Madame Duval
* Roland Bertin: Monsieur Duval
* Yolande Moreau: Gina
* Bouli Lanners: Youssouf
* Bruno Lochet: MBurundé
* Philippe Duquesne: Indian
* Lola Arnaud: Coquelicot
* Dominique Bettenfeld: Sergeant Kur
* Yves Pignot: grocer
* Jackie Berroyer: sex-shop customer
* Edouard Montoute: bus-driver
* Terry Gilliam: "fake baby" homeless person
* Terry Jones: homeless person
* Gustave de Kervern: policeman
* Serge Riaboukine: policeman Jean-Pierre
* Ludmila Kudjakova: woman out of the poster
* Pascal Ternisien: judge
* Eric Moreau: firefighter

==References==
 

==External links==
*  

 
 
 
 


 