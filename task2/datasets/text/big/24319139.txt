The Social Network
 
{{Infobox film
| name = The Social Network
| image = Social network film poster.jpg
| caption = Theatrical release poster
| director = David Fincher
| producer = {{Plainlist|
* Scott Rudin
* Dana Brunetti
* Michael De Luca
* Ceán Chaffin }}
| screenplay = Aaron Sorkin
| based on =  
| starring = {{Plainlist|
* Jesse Eisenberg
* Andrew Garfield
* Justin Timberlake
* Armie Hammer
* Max Minghella
}}
| music = {{Plainlist|
*Trent Reznor
*Atticus Ross
}}
| cinematography = Jeff Cronenweth
| editing = {{Plainlist|
*Kirk Baxter
*Angus Wall
}}
| studio = {{Plainlist|
*Relativity Media
*Trigger Street Productions
}}
| distributor = Columbia Pictures
| released =  
| runtime = 120 minutes    
| country = United States
| language = English
| budget = $40 million 
| gross = $224.9 million   
}}

The Social Network is a 2010 American  , the film portrays the founding of   on October 1, 2010.

The Social Network received widespread acclaim, with critics praising its direction, screenplay, acting, editing and score. It appeared on 78 critics Top 10 lists for 2010; of those critics, 22 had the film in their number-one spot, the most of any film in its year. Rolling Stone s Peter Travers said "The Social Network is the movie of the year. But Fincher and Sorkin triumph by taking it further. Lacing their scathing wit with an aching sadness, they define the dark irony of the past decade." It was also Roger Eberts selection for the best film of the year. Multiple people portrayed in the film, including Zuckerberg and Saverin, have criticized the films historical inaccuracies.
 Best Picture, Best Director Best Actor Best Adapted Best Original Best Film Best Motion Best Director, Best Screenplay, Best Original Score at the 68th Golden Globe Awards.

==Plot== academic probation. Cameron and Tyler Winklevoss and their business partner Divya Narendra. The trio invites Zuckerberg to work on Harvard Connection, a social network for Harvard students aimed at dating.

After agreeing to work on the Winklevoss twins concept, Zuckerberg approaches his friend Eduardo Saverin with an idea for what he calls Thefacebook , an online social networking website that would be exclusive to Ivy League students. Saverin provides $1,000 in seed funding, allowing Mark to build the website, which quickly becomes popular. When they learn of Thefacebook, the Winklevoss twins and Narendra are incensed, believing that Zuckerberg stole their idea while keeping them deliberately in the dark by stalling on developing the Harvard Connection website. They raise their complaint with Harvard President Larry Summers, who is dismissive and sees no value in either disciplinary action or Thefacebook website itself.

Saverin and Zuckerberg meet fellow student Christy Lee, who asks them to "Facebook me," a phrase which impresses both of them. As Thefacebook grows in popularity, Zuckerberg extends the network to Yale University, Columbia University and Stanford University. Lee arranges for Saverin and Zuckerberg to meet Napster co-founder Sean Parker, who presents a "billion dollar" vision for the company that impresses Zuckerberg. He also suggests dropping the "The" from Thefacebook. At Parkers suggestion, the company moves to Palo Alto, with Saverin remaining in New York to work on business development. After Parker promises to expand Facebook to two continents, Zuckerberg invites him to live at the house he is using as company headquarters.
 LSE and intellectual property. diluted from 34% to 0.03%, while maintaining the ownership percentage of all other parties. His name is removed from the masthead as co-founder. Saverin vows to sue Zuckerberg for all the companys shares before being ejected from the building. Later, a cocaine possession incident involving Parker and his attempt to place the blame on Saverin finally convinces Zuckerberg to cut ties with him.
 depositions taken in the Winklevoss twins and Saverins respective lawsuits against Zuckerberg and Facebook. The Winklevoss twins claim that Zuckerberg stole their idea, while Saverin claims his shares of Facebook were unfairly diluted when the company was incorporated. At the end, Marylin Delpy, a junior lawyer for the defense, informs Zuckerberg that they will settle with Saverin, since the sordid details of Facebooks founding and Zuckerbergs own callous attitude will make him highly unsympathetic to a jury. After everyone leaves, Zuckerberg is shown sending a friend request to Albright on Facebook and then refreshing the webpage every few seconds as he waits for her response.
 2008 Beijing Olympics, placing sixth; Eduardo Saverin received a settlement of an unknown amount and his name was restored to the Facebook masthead as a co-founder; the website has over 500 million members in 207 countries and is valued at 25 billion dollars; and Mark Zuckerberg is the worlds youngest billionaire.

==Cast==
 
* Jesse Eisenberg as Mark Zuckerberg
* Andrew Garfield as Eduardo Saverin
* Justin Timberlake as Sean Parker Cameron and Tyler Winklevoss
* Max Minghella as Divya Narendra
* Josh Pence as Tyler Winklevoss (double)
* Brenda Song as Christy Lee
* Rashida Jones as Marylin Delpy  
* Steve Sires as Bill Gates 
* John Getz as Sy
* David Selby as Gage
* Denise Grayson as Gretchen Larry Summers
* Rooney Mara as Erica Albright
* Joseph Mazzello as Dustin Moskovitz
* Dustin Fitzsimons as The Phoenix – S K Club President
* Wallace Langham as Peter Thiel
* Dakota Johnson as Amelia Ritter
* Malese Jow as Alice Cantwel
* Trevor Wright as Josh Thompson
* Shelby Young as K.C.
* Aaron Sorkin as Ad Executive
 

==Production==

===Screenplay===
Screenwriter Aaron Sorkin said, "What attracted me to   had nothing to do with Facebook. The invention itself is as modern as it gets, but the story is as old as storytelling; the themes of friendship, loyalty, jealousy, class and power.   I got a 14-page book proposal that Ben Mezrich had written for his publisher for a book he was going to call The Accidental Billionaires. The publisher was simultaneously shopping it around for a film sale. Thats how it wound up in my hands. I was reading it and somewhere on page three I said yes. It was the fastest I said yes to anything. But Ben hadnt written the book yet, and I assumed that Sony was going to want me to wait for Ben to write the book, and I would start a year from now. They wanted me to start right away. Ben and I were kind of doing our research at the same time, sort of along parallel lines."   

However, according to Sorkin, Mezrich did not send him material from his book as he wrote it: "Two or three times wed get together. Id go to Boston, or wed meet in New York and kind of compare notes and share information, but I didnt see the book until he was done with it. By the time I saw the book, I was probably 80 percent done with the screenplay."   Sorkin elaborated:
 

===Casting===
Casting began in early August 2009, with open auditions held in various states.   and Dakota Johnson were also confirmed to star in the film.  In a 2009 interview with The Baltimore Sun, Eisenberg said, "Even though Ive gotten to be in some wonderful movies, this character seems so much more overtly insensitive in so many ways that seem more real to me in the best way. I dont often get cast as insensitive people, so it feels very comfortable: fresh and exciting, as if you never have to worry about the audience. Not that I worry about the audience anyway – it should be just the furthest thing from your mind. The Social Network is the biggest relief Ive ever had in a movie". 

===Filming=== Love Story Red One digital cinema camera.  The rowing scenes with the Winklevoss brothers were filmed at Community Rowing Inc. in Newton, Massachusetts {{cite web |accessdate=September 24, 2010 |url=http://news.cnet.com/8301-13577_3-10385630-36.html |title=
Gossip: Social Network filming will row across the pond |work=CNET.com |date=June 2, 2010 |author=McCarthy, Caroline}}  and at the Henley Royal Regatta.  Although a significant portion of the latter half of the film is set in Silicon Valley, the filmmakers opted to shoot those scenes in Los Angeles and Pasadena, California|Pasadena. Miniature faking process was used in a sequence showing a rowing event at the Henley Royal Regatta.

Armie Hammer, who portrayed the Winklevoss twins, acted alongside body double Josh Pence while his scenes were filmed. His face was later digitally grafted onto Pences face during post-production, while other scenes used Split screen (filmmaking)|split-screen photography. Pence was concerned about having no face time during the role, but after consideration thought of the role as a "no-brainer". He also appears in a cameo role elsewhere in the film.    Hammer states that director David Fincher "likes to push himself and likes to push technology" and is "one of the most technologically minded guys Ive ever seen."    This included sending the actors to "twin boot camp" for 10 months to learn everything about the Winklevosses. 

====Rowing production====
  Trinity College, as well as local club rowers from Union Boat Club and Riverside Boat Club.  None of the cast rowing extras for the Henley Royal Regatta racing scene appeared in the film; filming for the race was originally planned to take place in Los Angeles, but Fincher decided to film in New England during production.   

David Fincher hired Loyola Marymount coach Dawn Reagan to help train Josh Pence and Armie Hammer.    While Hammer was new to the sport, Pence rowed previously at Dartmouth College. 
 pairs for the Charles River scene were cast as follows:
* Tom Owston and Joe Carrol
* Henry Roosevelt and Teddy Schreck
* Tyler Williamson and Matt Webb
* Perrin Hamilton and Steve Barchick
* Lucas Abegg and Jim Bayley (body doubles for Hammer and Pence) 

The indoor rowing scene was filmed at Boston Universitys indoor rowing tanks. All of BUs blue oars in the scene were repainted to Harvards crimson color for filming. Dan Boyne was the official rowing consultant for the film both in the US and the UK. 

===Soundtrack===
  Null Corporation EP was made available for download.  The White Stripes song "Ball and Biscuit" can be heard in the opening of the film and The Beatles song "Baby, Youre a Rich Man" concludes the film. Neither song appears on the soundtrack.
 Best Original Best Original Score.

==Release==

===Promotion===
The first theatrical poster was released on June 18, 2010.  The films first teaser trailer was released on June 25, 2010.  The second teaser was released on July 8.  The full length theatrical trailer debuted on July 16, 2010, which plays an edited version of the song "Creep (Radiohead song)|Creep", originally by Radiohead, covered by the Belgian choir group Scala & Kolacny Brothers.     The trailer was then shown in theaters, prior to the films Inception, Dinner for Schmucks, Salt (2010 film)|Salt, Easy A, The Virginity Hit, and The Other Guys. The theatrical trailer, put together by Mark Woollen & Associates, won the Grand Key Art award at the 2011 Key Art Awards,  sponsored by The Hollywood Reporter, and was also featured on The Film Informant s Perfect 10 Trailers in 2010. 

===Response by the principals===
  expressed his dissatisfaction with a film being made about him and noted that much of the films plot was not factual]] leaked online The Cardinal Courier stated that the film was about "greed, obsession, unpredictability and sex" and asked "although there are over 500 million Facebook users, does this mean Facebook can become a profitable blockbuster movie?"    
 coding Facebook.  Speaking to an audience at Stanford University, Zuckerberg stated that the film portrayed his motivations for creating Facebook inaccurately; instead of an effort to "get girls", he says he created the site because he enjoys "building things".    However, he added that the film accurately depicted his real-life wardrobe, saying, "Its interesting the stuff that they focused on getting right – like every single shirt and fleece they had in that movie is actually a shirt or fleece that I own." 

Facebook co-founder  . July 16, 2010. Retrieved February 15, 2011.  According to Moskovitz:
 

Co-founder Eduardo Saverin said of the film, "  the movie was clearly intended to be entertainment and not a fact-based documentary." 

Sorkin has stated that, "I dont want my fidelity to be to the truth; I want it to be to storytelling. What is the big deal about accuracy purely for accuracys sake, and can we not have the true be the enemy of the good?" 

Journalist Jeff Jarvis acknowledged the film was "well-crafted" but called it "the anti-social movie", objecting to Sorkins decision to change various events and characters for dramatic effect, and dismissing it as "the story that those who resist the change society is undergoing want to see."  Technology broadcaster Leo Laporte concurred, calling the film "anti-geek and Misogyny|misogynistic".  Sorkin responded to these allegations by saying, "I was writing about a very angry and deeply misogynistic group of people".   

Andrew Clark of The Guardian wrote that "theres something insidious about this genre of   scriptwriting," wondering if "a 26-year-old businessman really deserves to have his name dragged through the mud in a murky mixture of fact and imagination for the general entertainment of the movie-viewing public?" Clark added, "Im not sure whether Mark Zuckerberg is a punk, a genius or both. But I wont be seeing The Social Network to find out." 

Several noteworthy tech journalists and bloggers voiced their opinions of how the film portrays its real-life characters.   who says shes known Zuckerberg "for a long time", wrote of the film:
 

Harvard Law School professor Lawrence Lessig wrote in The New Republic that Sorkins screenplay does not acknowledge the "real villain" of the story:
 

In an onstage discussion with The Huffington Post co-founder Arianna Huffington, during Advertising Week 2010 in New York, Facebooks Chief Operating Officer Sheryl Sandberg said she had seen the film and it was "very Hollywood" and mainly "fiction". "In real life, he   was just sitting around with his friends in front of his computer, ordering pizza," she declared. "Who wants to go see that for two hours?" 

Divya Narendra said that he was "initially surprised" to see himself portrayed by the non-Indian actor Max Minghella, but also admitted that the actor did a "good job in pushing the dialogue forward and creating a sense of urgency in what was a very frustrating period." 

===Home media===
The Social Network was released on  , Jeff Cronenweth and David Fincher on the Visuals, and a Ruby Skye VIP Room: Multi-Angle Scene Breakdown feature. 

==Reception==

===Box office=== Tooth Fairy s 28.6% drop. As of August 19, 2011, the film has grossed $96,962,694 in the United States and $127,957,621 elsewhere, for a worldwide total of $224,920,315. 

===Critical reception===
Review aggregate  , indicating "universal acclaim" and making it one of the sites highest-rated movies of all time. 

  of   review called it "flawless" and gave it five stars.  Quentin Tarantino listed The Social Network as one of his favorite 20 movies of the year, second to Toy Story 3. 

Some reviewers pointed out that the film plays loosely with the facts behind Facebooks founding.  , so everything thats seen isnt necessarily to be believed." 

The film won Best Picture from the National Society of Film Critics, the New York Film Critics Circle, the Los Angeles Film Critics Association, and the National Board of Review, making it only the third film in history (after Schindlers List and L.A. Confidential (film)|L.A. Confidential) to sweep the "Big Four" critics.  The film also won the "Hollywood Ensemble Award" from the Hollywood Awards.   The Social Network appeared on 78 critics top 10 lists for 2010, of those critics 22 had the film in their number one spot. 

====Top ten lists====
The Social Network appeared on over 70 critics top ten lists of the best films of 2010. Over a dozen publications ranked the film first in their lists. 
  
 
* 1st&nbsp;– Peter Travers, Rolling Stone
* 1st&nbsp;– Roger Ebert, Chicago Sun-Times
* 1st&nbsp;– Christy Lemire, Associated Press
* 1st&nbsp;– Andrea Grunvall, Chicago Reader
* 1st&nbsp;– Owen Gleiberman and Lisa Schwarzbaum, Entertainment Weekly
* 1st&nbsp;– Betsy Sharkey and Kenneth Turan, Los Angeles Times
* 1st&nbsp;– Rene Rodriguez, Miami Herald
* 1st&nbsp;– Stephen Holden, The New York Times David Denby, The New Yorker Now Magazine
* 1st&nbsp;– Tim Robey, Telegraph
* 1st&nbsp;– Joshua Rothkopf, Time Out New York
* 1st&nbsp;– Ann Hornaday, Washington Post
* 1st&nbsp;– Joe Morgenstern, Wall Street Journal
* 2nd&nbsp;– Richard Brody, The New Yorker
* 2nd&nbsp;– Todd McCarthy, Kirk Honeycutt The Hollywood Reporter
* 3rd&nbsp;– Richard Roeper, RichardRoeper.com Anne Thompson, Indiewire
* 4th&nbsp;– Joe Neumaier and Elizabeth Weitzman, New York Daily News
* 5th&nbsp;– Richard Corliss, Time (magazine)|Time 
 

===Accolades===
  Best Motion Best Director, Best Screenplay, Best Original Score, making it the film with the most wins of the night.     
 Best Film, Best Actor Best Actor Rising Star Best Editing, Adapted Screenplay, Best Direction on February 13, 2011.   
 Best Picture, Best Actor, Best Cinematography, Best Director, Best Film Best Original Best Sound, Best Adapted Best Adapted Best Original Best Film Editing at the 83rd Academy Awards on February 27, 2011.

===Impact===
Since its release, The Social Network has been cited as inspiring involvement in start-ups and social media.    confirmed that they were inspired to create the fantasy trading community after watching The Social Network. 

Following his success with the film, Sorkin became attached to another project about a technology company, writing the script for the biopic  , into a movie. 

==See also==
 

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 