I Can't Sleep (film)
{{Infobox film
| name           = I Cant Sleep
| image          = I Cant Sleep (film).jpg
| image_size     = 
| caption        = 
| director       = Claire Denis
| producer       = Bruno Pésery
| writer         = Claire Denis
| narrator       = 
| starring       = Yekaterina Golubeva
| music          = 
| cinematography = Agnès Godard
| editing        = Nelly Quettier
| distributor    = 
| released       = 18 May 1994
| runtime        = 110 minutes
| country        = France
| language       = French
| budget         = 
| preceded_by    = 
| followed_by    = 
}} 1994 cinema French drama film written and directed by Claire Denis. It was screened in the Un Certain Regard section at the 1994 Cannes Film Festival.    The film was loosely inspired by the murders committed by Thierry Paulin.

==Plot==
Daiga (Yekaterina Golubeva), a woman from Lithuania immigrates to Paris with little money hoping to secure herself a job as a dancer. When her plans fall through she begins work as a maid in the hotel of a friend of her great-aunts. 

At the same time Theo (Alex Descas) is embroiled in a fight with his wife as he wants to leave for Martinique with their young son while she wants to remain in Paris. He is infrequently visited by his brother, Camille. 

Meanwhile the city is on edge because of a series of violent murders that have targeted elderly women living alone. The murders are being committed by Camille and his lover. The two live in the hotel run by Daigas employer.  

Eventually Daiga begins to follow Camille around and figures out that he is the murderer after spotting a police sketch of his face. After breaking in to his room she finds a bag of cash and steals it, leaving abruptly to go back to Lithuania. 

Camille is spotted by police after one of his victims recovers enough to give a description of him. Theo is brought down to the police station for questioning but insists that far from having anything to do with the murders he remained unaware of what his brother was doing the entire time.

==Cast==
* Yekaterina Golubeva as Daiga
* Richard Courcet as Camille
* Vincent Dupont as Raphael
* Laurent Grévill as le docteur
* Alex Descas as Theo
* Irina Grjebina as Mina
* Tolsty as Ossip
* Line Renaud as Ninon
* Béatrice Dalle as Mona
* Sophie Simon as Alice
* Patrick Grandperret as Abel

== Reception ==
The film received generally positive reviews from critics, garnering a 75% fresh score on Rotten Tomatoes. 

==References==
 

==External links==
* 

 

 
 
 
 
 


 