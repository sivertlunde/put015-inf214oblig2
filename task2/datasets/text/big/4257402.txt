To the Left of the Father
{{Infobox film
| name           = To the Left of the Father
| image          = To the Left of the Father.jpg
| caption        = Theatrical release poster
| director       = Luiz Fernando Carvalho Donald K. Ranvaud
| writer         = Luiz Fernando Carvalho
| based on       = Lavoura Arcaica by Raduan Nassar
| starring       = Selton Mello Raul Cortez Juliana Carneiro da Cunha Simone Spoladore Leonardo Medeiros Caio Blat Marco Antônio Guimarães
| cinematography = Walter Carvalho
| studio         = Video Filmes LFC Produções Raquel Couto Produções
| distributor    = RioFilme
| released       = 
| runtime        = 165 minutes
| country        = Brazil
| language       = Portuguese
| budget         = 
| gross          = R$874,018  ($372,52)
}}

To the Left of the Father ( ) is a 2001 Brazilian drama film directed by Luiz Fernando Carvalho, based on the novel of the same name by Raduan Nassar.

==Plot summary==

The story concerns a young man, André (Selton Mello), whose ideas are radically different from his fathers (Raul Cortez). The father predicates order and restraint, which enhance his own power under the mantle of family love. The son seeks freedom and ecstasy, challengingly signified, in the film, through his incestuous passion for his sister Ana (Simone Spoladore). When the son leaves home on the farm and moves to a seedy boarding house, his older brother Pedro (Leonardo Medeiros), is asked by their mother (Juliana Carneiro da Cunha) to bring him back. His return, however, will completely shatter the familys confining life.

==Cast==
*Selton Mello - André
**Pablo César Câncio - young André
**Luiz Fernando Carvalho - André voice (narrator)
*Raul Cortez - Father
*Juliana Carneiro da Cunha - Mother
*Simone Spoladore - Ana
*Leonardo Medeiros - Pedro
*Caio Blat - Lula
*Denise Del Vecchio - prostitute
*Samir Muci Alcici Júnior
*Leda Samara Antunes
*Felipe Abreu Salomão
*Raphaela Borges David

==Awards==
 
*Grande Prêmio BR de Cinema (2002): Best actress (Juliana Carneiro da Cunha) and Best Cinematography.
*Montréal World Film Festival (2001): Best Artistic Contribution.
*Festival de Brasília do Cinema Brasileiro(2001): Best Film, Best Actor (Selton Mello), Best Supporting Actress (Juliana Carneiro da Cunha) and Best Supporting Actor (Leonardo Medeiros).
*Mostra de Cinema de São Paulo (2001): Audience Award.
*Cartagena Film Festival: Best Film, Best Director, Best Cinematography and Best Soundtrack.
* ), Best Cinematography and Best Soundtrack.
*ABC Trophy (2002): Feature Film - Best Cinematography.
*Festival de Buenos Aires del cinema independiente (2002): ADF Cinematography Award, Audience Award, Kodak Award and Special Mention (Luiz Fernando Carvalho).
*Festival de Guadalajara - Mexico (2002): Best Film - International Jury
*Brothers Manaki International Film Festival (2002): Audience Award (Walter Carvalho), Golden Camera 300
*Entreveus Film Festival (2002): Feature Film - Audience Award
*Festival de Lima (2002): Best Actor (Selton Mello)
*Festival de Lleida (2002): ICCI Screenplay Award, Best Actor (Selton Mello)
*Associação Paulista de Críticos de Artes(2002): Best actress (Juliana Carneiro da Cunha)
*Festival di Trieste (2002): Best Film
*Festival de Valdivia (2002): Best Film

==References==
 

==External links==
* 

 
 
 
 
 
 