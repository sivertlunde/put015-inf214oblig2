Pit Pony (film)
{{Infobox film
| name           = Pit Pony
| image          = PitPony.jpg
| image_size     = 
| caption        = 
| director       = Eric Till
| writer         = Heather Conkie, based on the book by Joyce Barkhouse
| producer       = Andrew Cochran
| starring       = Ben-Rose Davis, Richard Donat, Ellen Page
| cinematography = Nikos Evdemon
| editing        = T. C. Martin
| studio         = Cochran Entertainment Inc.
| distributor    = RTL Entertainment (Netherlands, TV) Jonathan Goldsmith
| released       = December 14, 1997
| runtime        = 92 mins.
| country        = Canada
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
Pit Pony is a 1997 television film directed by Eric Till and is also Ellen Pages debut role.  It was nominated for numerous Gemini Awards in 1997. 

==Cast==
*Ben-Rose Davis as Ben MacLean
*Richard Donat as Rory MacLean
*Jennie Raymond as Nellie MacLean
*Andrew Keilty as John MacLean
*Ellen Page as Maggie MacLean

==References==
 

==External links==
* 

 

 
 
 
 
 
 


 