No God, No Master
{{Infobox film
|image=No_God,_No_Master_poster.jpg
| name           = No God, No Master
| director       = Terry Green
| producer       = Terry Green
| writer         = Terry Green 
| starring       = David Strathairn Sam Witwer Alessandro Mario
| cinematography = Paul Sanchez
| editing        = Suzy Elmiger A.C.E.
| music          = Nuno Malo
| studio         = Strata Productions
| distributor    = Monterey Media  
| festivals      =  
| country        = United States
| language       = English
}}
No God, No Master is a 2012 American independent crime suspense thriller directed, written, and produced by Terry Green and starring David Strathairn, Ray Wise, Sam Witwer, Edoardo Ballerini and Alessandro Mario. No God, No Master was filmed in Milwaukee, Wisconsin. The story includes references to the Ludlow Massacre and depictions of the Sacco and Vanzetti trial and the Wall Street bombing.

==Plot==
 Agent William true events war on terrorism and the role government plays to defeat it.   

==Cast==
* David Strathairn as William J. Flynn
* Sam Witwer as Eugenio Ravarini
* Edoardo Ballerini as Carlo Tresca
* Alessandro Mario as Bartolomeo Vanzetti

==Production==

===Development===
No God, No Master is directed, written, and produced by Terry Green.

===Filming===
No God, No Master was filmed in Milwaukee, Wisconsin, USA in 2009 over 24 days at 42 different locations, including Villa Terrace, South Shore Park Pavilion, City Hall and the old Pabst Brewery.   

==Release==
In October 2013, Monterey Media bought the United States distribution rights for release of the film in the United States and Canada in March 2014. 

===Festivals===
No God, No Master was selected to screen at the 2012 Stony Brook Film Festival   

==References==
 

==External links==
*  
* 
* 

 
 
 
 
 
 