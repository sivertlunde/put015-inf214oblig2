Philips and the Monkey Pen
{{Infobox film
| name           = Philips and the Monkey Pen
| image          = File:Philip_and_the_monkey_pen_poster.jpg
| caption        = 
| director       = Rojin Thomas Shanil Muhammed
| producer       = Sandra Thomas Vijay Babu
| writer         = Rojin Thomas
| screenplay     = 
| production     = Shibu G Suseelan
| story          = Shanil Muhammed
| based on       =  
| narrator       =  Mukesh Vijay Babu   
| music          = Rahul Subrahmanian  
| cinematography = Neil DCunha
| editing        = Prejish Prakash
| studio         = 
| distributor    = Friday Tickets & PJ Entertainments Europe
| released       =  
| runtime        = 140 minutes
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}}
 Mukesh and Joy Mathew.  Production Controller of Philips and the Monkey Pen is Shibu G Suseelan.

The film revolves around an ten-year-old called Ryan Philip (Sanoop Santhosh) and his adventures with a magical pen called the Monkey Pen.  The film was co-produced by actress Sandra Thomas under Friday Film House banner.    Neil D’Cunha is the cinematographer and the music has been scored by Remya Nambeesan’s brother, Rahul Subramanian. 

The film released on 7 November 2013 in Kerala.    The film completed 111 days in theaters and was a commercial success at the box-office.   The film won Kerala State Film Award for Best Childrens Film and Kerala State Film Award for Best Child Artist (Sanoop Santhosh).

==Cast==
*Sanoop Santhosh as Ryan Philip
*Jayasurya as Roy Philip
*Remya Nambeesan as Sameera Roy
*Vijay Babu as Padmachandran aka Pappan
*Joy Mathew as Captain Richard Philip Mukesh as Principal Innocent as God
*Dhiya Zelen as Joan
*Gourav Menon as Jahangir aka Jugru
*Aakhash Santhosh as Raman
*Antony Elrin DSilva as Innocent
*Nidheesh Boban as Decimal
*Augen as 7-Up
*Mathew Joy as Rahul
*Pradeep Kottayam as Pavithran
*Kiran Aravindakshan as Joans Father
*Dean Rowlins as Robert Bristo
*Sudheer Karamana as Decimals Father
*Sasi Kalinga as Murthy

==Critical reception==
Philips and the Monkey Pen got very much positive reviews from critics and other audiences.

Deccan Chronicle gave 3.5/5 rating and said, "This, to date, must surely rate as the year’s top contender of the prestigious Swarna Kamal for the Best Children’s Film. It lights up the righteous path without being preachy. When a son says to his father, “I thought the truth would pain you”, the wise one smiles, inspiringly, “The truth isn’t bitter; it is the lies that are excessively sweet”."

==Awards==
* Kerala State Film Award for Best Childrens Film
* Kerala State Film Award for Best Childrens Film Director- Rojin Thomas and Shanil Muhammed
* Kerala State Film Award for Best Child Artist - Sanoop Santhosh

==Soundtrack==
The films soundtrack contains 6 songs, all composed by Rahul Subrahmanian. Lyrics by Anu Elizabeth Jose, Sibi Padiyara, and Mamtha Seemanth.

{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Vinnile Thaarakam"
| Arun Alat
|-
| 2
| "En Kanimalare  "
| Neha Venugopal
|-
| 3
| "Baalyathil"
| Remya Nambeesan
|-
| 4
| "En Kanimalare  "
| Sachin Warrier
|-
| 5
| "Its Just Another Day"
| Shaan Rahman
|-
| 6
| "Kanavukalil"
| Najim Arshad
|}

==References==
 

==External links==
*  

 
 
 