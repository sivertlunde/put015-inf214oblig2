Delta Farce
 
{{infobox film
| name = Delta Farce
| image = Delta farce.jpg
| caption = Promotional movie poster, which parodies the 1987 film Full Metal Jacket
| director = C. B. Harding
| writer = Bear Aderhold  Tom Sullivan
| producer = Alan C. Blomquist J.P. Williams
| starring = Larry the Cable Guy  Bill Engvall  DJ Qualls  Danny Trejo  Keith David
| cinematography = Tom Priestley Jr
| editing       = Mark Conte
| music      = James S. Levine Lionsgate Shaler Entertainment Samwilla Productions
| distributor = Lionsgate
| released =  
| language = English
| country = United States
| runtime = 90 minutes 
| gross   = $8,729,473 
}}
 Army Rangers Green Berets.

== Plot ==
Larry McCoy (Larry the Cable Guy), Bill Little (Bill Engvall), and Everett Shackleford (DJ Qualls) are members of the Army National Guard. Larry loses his job and his girlfriend Karen reveals that she is pregnant with someone else’s baby. Bill is having a hard time with wife Connie and Everett lives in a storage locker as a security guard after getting fired from being a police officer. Bill shows Larry how to get over his grief of Karen by shooting the gifts that he got Karen. Meanwhile, an Army Colonel gets a request for more troops for Fallujah and sends Master Sergeant Kilgore (Keith David) to assess the situation at the local reserve base where he finds Bill, Larry, and Everett. He puts them through a training regimen and sends them to Fallujah. They fall asleep in a Humvee aboard the cargo plane and the pilots dump all of the cargo when the plane experiences turbulence. Sergeant Kilgore is also pulled out of the plane when he is snagged on a pallet of supplies.
After they wake up the next morning, they believe they are in Iraq. When climbing out of the Humvee, Everett stomps on the radio. They find Kilgore lying next to one of the supply crates, concluding he had died. After running out of water, Everett starts to urinate in a canteen and leaves it on a crate. They solemnly bury him and give a brief eulogy. After gearing up, Larry holds a machine gun and says, "All right ladies...Let’s Git R Done." Little do they know, they are actually about 500&nbsp;km from Mexico City. After some driving and looking at a map of Iraq, Larry confirms that they were lost. Everett complains about not getting to shoot anyone yet while Bill and Larry tell him the rules of engagement. Everett sees two Mexicans with their mule and views them as a threat. He fires as the two men and their donkey duck to the floor. The three soldiers confront the men and see that Everett killed their mule by mistake. Larry tells them that theyre here to liberate their people. The mule regains consciousness and the two Mexicans ask the soldiers to help their village. They accept. Back at the landing site, Kilgore jumps out of his burial and sees a sign which shows that he is in Mexico. He sees a canteen on a crate (which Everett peed in). He pours it on his head, drinks it, and realizes that it’s urine. Meanwhile, the soldiers and the two Mexicans look over a rock and Larry sees through the binoculars that "Iraqi Insurgents" abusing the residents of La Miranda. Larry, Bill, and Everett devise a plan and Bill isnt sure. Larry looks back and sees one of the Insurgents holding the Mayor’s daughter by the arm, presumably to be taken away as a slave girl or raped. The three soldiers drive into the village, poorly shooting and manage to drive and scare most of them away while capturing one for questioning. The villagers throw a party to celebrate. Meanwhile, Kilgore is found to be alive.
Larry asks the prisoner, Ricardo, if he is Republic Guard or Al Qaeda and where are the weapons of mass destruction, and Everett, in a homemade sniper suit, asks if hes a Turd or a Shiite. Ricardo realizes they are confused and laughs at them. When Larry asks what was funny, Ricardo says, "Youre not in Iraq, gringo." Larry realizes that gringo is a term used by Mexicans. Larry finds Bill who says jokingly that they may be in Mexico. They realize they are still fighting terrorism in a different form. They learn the name of the leader of the bandits is Carlos Santana (Danny Trejo). Meanwhile, the soldiers fix up the town, calling the mission "Operation: Sombrero." A bartender informs Bill of a nearby phone while he tries to fix the radio and Bill leaves without telling Larry or Everett. As Bill is being reprimanded for being in Mexico instead of Iraq, Santana takes him hostage.

Santana takes Bill back to the town and threatens to kill him, but Larry points out that he can kill Santana despite the number of bandits. Bill is released and Kilgore arrives to yell at Bill and Larry. He doesnt notice Santana and the others until he is held at gunpoint. Everett arrives and a fight begins.

The next day, Kilgore agrees to help defend La Miranda. The townspeople hide in the church when the bandits arrive. The bandits arrive with a  s for their actions as Bill also receiving the Purple Heart for being "shot in the ass" as a 101st medic put it. Sergeant Kilgore moves to Miami, Florida, where he opens a private exercise camp. Everett moves to Mexico and becomes a luchador named "Carne Asada." Bill sues the Mexican government and moved to Beverly Hills after an out of court settlement. Larry goes back to Mexico, marries Maria, and opens up a restaurant called "Larrys Mess Hall" in Cabo San Lucas. Santana was jailed and put into a rehab program. After being released, he became a ventriloquist to the repaired José.

== Cast == Private Larry McCoy
* Bill Engvall as Private Bill Little
* DJ Qualls as Private Everett Shackleford
* Keith David as Master Sergeant Kilgore
* Danny Trejo as Carlos Santana
* Marisol Nichols as Señorita Maria Garcia
* Lisa Lampanelli as Connie
* Jeff Dunham (cameo) as ""The Amazing Ken" (with José Jalapeño on a Stick)

== Reception ==
The film received an overwhelmingly negative response from critics. Based on 40 reviews it received a "rotten" rating of 5% from  , gay panic, and flatulence jokes." 
 

== References ==
 

== External links ==
*  
*  
*  
*    

 
 
 
 
 
 
 
 