A Sheep in the Deep
 
 
{{Infobox Hollywood cartoon
 | cartoon_name      = A Sheep in the Deep
 | series            = Merrie Melodies (Ralph Wolf and Sam Sheepdog)
 | image             = A Sheep in the Deep.png
 | caption           = Title card from A Sheep in the Deep 
 | director          = Chuck Jones Maurice Noble (co-director)
 | story_artist      = Chuck Jones Richard Thompson Bob Bransford
 | layout_artist     = Corny Cole William Butler
 | voice_actor       = Mel Blanc
 | musician          = Milt Franklyn
 | producer          = Warner Bros. Pictures
 | distributor       =
 | release_date      = February 10, 1962
 | color_process     = Technicolor
 | runtime           = 6 minutes and 10 seconds
 | preceded_by       = Ready, Woolen and Able
 | followed_by       = Woolen Under Where
 | movie_language    = English
}} Warner Bros. Sam Sheepdog and Ralph Wolf shorts, this short is mostly composed of visual gags.
 Sam Sheepdog Richard Thompson. That was also the first cartoon featuring the characters to be written by Chuck Jones (the previous cartoons with these characters were written by Michael Maltese).

==Plot==
 
 Sam Sheepdog and Ralph Wolf shorts, this one revolves around Ralph Wolf trying to steal the sheep which Sam Sheepdog is guarding. Like Ready, Woolen and Able, this short begins with a juxtaposition of how Ralph and Sam get to work. Sam wakes up at dawn and takes a leisurely stroll to work as Ralph sleeps in and the sun rises. Just as Sam reaches the punch clock and lifts his hair to read the time, Ralphs alarm clock goes off, triggering the conveyor system that he uses to get to work. A claw lifts his blanket, his bed tilts, dropping him into a trap door where he falls into the shower, after a second in the shower a spring below him engages, sending him into a towel and onto a roller skate. As he rides the roller skate down its rail and dries off with the towel, he is fed a slice of toast and coffee. Finally, he grabs his lunch from a hook and rides the roller skate out his front door, down the path, and punches into work before Sam. As always, the two merrily greet each other.

 
1. Ralph begins with the straightforward approach of sneaking into the field and snatching a sheep. As he walks back, Sam drops a banana peel which Ralph slips on. Instead of falling immediately, Ralph slides around on the banana peel, and on his way back Sam snags the stolen sheep with a lasso. As Ralph continues to slide forward, he begins to say "Ooh, Id like to..." only to hit a tree face-first.

2. This time Ralph digs a hole under Sam and out the side of the cliff face, and decides to try to snatch a sheep with a lasso in the same way Sam did in Ralphs last attempt. This is successful, but while Ralph is pulling the sheep up Sam leans over the edge and into Ralphs hole, where he glares at Ralph intimidatingly. Ralph innocently lowers the sheep down, but Sam grabs Ralph by the neck and hits him on the head, propelling him into a tree trunk. As Ralph climbs out of the tree trunk, he resembles a stack of pancakes.

 
3. Ralphs next plan is to tunnel under Sam, cut out a circle of earth around Sam with a saw, and jack that circle high into the air with a jack (device)|jack. Successful, Ralph ties a dinner napkin around his neck and prepares to steal a sheep. However, as hes doing this, Sam pulls out a corkscrew and removes the piece of earth that the jack is supporting, sending the entire chunk of earth crashing onto Ralph.
 BB pellets into his mouth and shoots at Ralphs balloons with a blow gun. Ralph is left grasping on to a single balloon, but the knot on the balloon slips and Ralph is projected into Sam. Sam grabs Ralph by the neck and prepares to drop him off the cliff.

However, before that can happen, the punch clock whistle blows and Sam and Ralph head off for lunch. They enjoy their sandwiches and Sam shares his coffee with Ralph, then they have a smoke break.

They both walk back to the cliff, and as soon as the whistle blows to signal the end of lunch, Sam grabs Ralph by the neck and finally drops him.
 slide behind Sam and dropping a cannonball down the slide. Sam casually points a large spring at the end of the slide, sending the cannonball back up the slide and into Ralphs dropped jaw. Ralph angrily walks away as the cannonball inside his tail weighs him down.

6. Ralph then attempts to use a record titled "Music to put sheep dogs to sleep by" to put Sam to sleep. This appears to be successful, and Ralph tests to make sure that Sam really is asleep, in much the same way as he tested to make sure Sam couldnt see him in Double or Mutton.  After walking off, stealing a sheep and preparing to eat it, the sheep turns out to be Sam is disguise, also similar to Double or Mutton. However, the two of them continue to remove disguises.

* Ralph turns out to be a sheep
* Sam turns out to be Ralph
* The sheep turns out to be Sam
* Ralph turns out to be a sheep
* Sam turns out to be Ralph

Finally, Ralph grabs the sheep by the neck, and the sheep turns out to be a stick of dynamite.

However, Ralph is saved by the bell when the punch clock whistles. Sam walks in off screen and extinguishes the stick of dynamite saying "Its too close to quittin time Ralph, lets pick it up there in the morning." The two amicably wish each other good night.

==Edited versions==
 

Two scenes were omitted from this short on American Broadcasting Company|ABC. The entirety of Ralphs second plan was omitted, and the entire lunchtime gag, where Sam holds Ralph over the edge of the cliff, the two leave to have lunch and then have a smoke break, and then return to drop Ralph off the cliff was omitted.

CBS and Cartoon Network left Ralphs second plan intact, but edited the lunch break scene to remove the brief shots of Ralph and Sam smoking. However, the part with Sam cleaning his pipe by tapping it on his foot was left in. {{Cite web
 | author      = Cooke, John 
 | date        = 2006 revision
 | title       = Censored Looney Tunes: S
 | work        = Censored Looney Tunes and Merrie Melodies
 | publisher   = Looney.GoldenAgeCartoons.com
 | url         = http://looney.goldenagecartoons.com/ltcuts/ltcutss.html
 | archiveurl  = http://web.archive.org/web/20061209031652/http://looney.goldenagecartoons.com/ltcuts/ltcutss.html
 | archivedate = December 9, 2006
}} 

==Availability==
This short is available on the "Looney Tunes: Assorted Nuts" laserdisc.

==See also==
* Looney Tunes and Merrie Melodies filmography (1960–1969)

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 