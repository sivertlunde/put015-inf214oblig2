Nihon Chinbotsu (1973 film)
{{Infobox film
| name           = Nihon Chinbotsu
| image          = Nihon Chinbotsu (1973 film).jpg
| image_size     = 
| caption        = Film poster using American title for the film
| director       = Shiro Moritani
| producer       = 
| writer         = Shinobu Hashimoto Sakyo Komatsu  (novel)
| narrator       =  Ayumi Ishida
| music          = Masaru Sato
| cinematography = Daisaku Kimura Hiroshi Murai
| editing        = Michiko Ikeda
| distributor    = Toho (Japan) New World Pictures (US)
| released       =  
| runtime        = 143 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = $7 million (Japan) 
| preceded_by    = 
| followed_by    = 
}} 1973 film Ayumi Ishida. Nihon Chinbotsu, loosely based on a second section of this series.

The movie caused a sensation in Japan and paved the way for later "panic" features.  Komatsu, the author of the novel, made a cameo in the beginning scenes of the movie.

==Synopsis==
Volcanic eruptions and earthquakes shake Japan.  Firestorms burn beautiful Japanese cities to the ground.  A weather survey group discovers that the Japanese Archipelago is moving towards the Japanese Trench, which if left to continue on its collision course, would bring the whole island of Japan under the sea.

==Cast==
* Keiju Kobayashi - Dr. Tadokoro 
* Hiroshi Fujioka - Onodera Toshio  Ayumi Ishida - Abe Reiko 
* Rhonda Leigh Hopkins - Fran 
* Lorne Greene - Ambassador Warren Richards 
* Tetsuro Tamba - Prime Minister Yamamoto  Shogo Shimada - Watari 
* John Fujioka - Narita  Andrew Hughes - Australian Prime Minister 
* Nobuo Nakamura - Japanese Ambassador 
* Haruo Nakajima - Prime Ministers Chauffeur 
* Hideaki Nitani - Dr.Nakata 
* Isao Natsuyagi - Yuuki 
* Yusuke Takita - Assistant Professor Yukinaga

==Tidal Wave==
{{Infobox film
| name           = Tidal Wave
| image          = 
| image_size     = 
| caption        = 
| director       = Andrew Meyer
| producer       = 
| writer         = 
| narrator       = 
| starring       = Lorne Greene
| music          = 
| cinematography = 
| editing        = 
| distributor    = New World Pictures (US)
| released       = May 1975
| runtime        = 82  minutes
| country        = 
| language       = 
| budget         = 
| gross          = $3.5 million (US) 
| preceded_by    = 
| followed_by    = 
}}
Roger Corman bought the US rights to the film for his New World Pictures. He cut out a great deal of footage and added new sequences directed by Andrew Meyer starring Lorne Greene as an ambassador at the United Nations. The film was a big success at the US box office. Christopher T Koetting, Mind Warp!: The Fantastic True Story of Roger Cormans New World Pictures, Hemlock Books. 2009 p 80-83 

==References==
 

==External links==
* 
* 

 
 
 
 
 
 