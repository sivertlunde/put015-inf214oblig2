Apne Huye Paraye
{{Infobox film
| name           = Apne Huye Paraye
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Ajit Chakraborty
| producer       = Ajit Chakraborty
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Manoj Kumar Mala Sinha
| music          = Shankar Jaikishan
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
}}
 Indian Bollywood film produced and directed by Ajit Chakraborty. It stars Manoj Kumar and Mala Sinha in pivotal roles.

==Cast==
* Manoj Kumar...Dr. Shankar
* Mala Sinha...Rekha
* Shashikala...Lata
* Agha (actor)|Agha...Fohkat
* Lalita Pawar...	Rekhas lawyer
* Iftekhar...Public Prosecutor
* Sunder (actor)|Sunder...Makhan

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Gagan Ke Chanda Na Poochh Humse"
| Subir Sen, Lata Mangeshkar
|}
==External links==
* 

 
 
 
 

 