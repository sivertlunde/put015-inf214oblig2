Our Family Wedding
 
{{Infobox film
| name           = Our Family Wedding
| image          = Our Family Wedding.jpg
| caption        = Theatrical release poster
| director       = Rick Famuyiwa
| producer       = Edward Saxon
| story          = Wayne Conley
| screenplay     =  
| starring       =  
| music          = Transcenders
| cinematography = Julio Macat
| editing        = Dirk Westervelt
| distributor    = Fox Searchlight
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $21,409,028 
}}

Our Family Wedding is a romantic comedy film starring Forest Whitaker, America Ferrera, Carlos Mencia, Lance Gross, Shannyn Sossamon, Charlie Murphy and Regina King. It received its wide release on March 12, 2010.

==Plot==
Marcus Boyd (Lance Gross), a young African-American, has recently graduated from Columbia Medical School and is headed to Laos for a year to work with Doctors Without Borders. Unbeknownst to their respective parents, Marcus and his fiance Lucia Ramirez (America Ferrera), who is Mexican-American, have been living together and would like to get married before they both head off to Laos together.

Meanwhile, in Los Angeles, Marcus father Brad (Forest Whittaker) is coming out of a "meeting" when he sees his car being towed by Miguel (Carlos Mencia), who happens to be Lucias father. Brad tries unsuccessfully to halt the tow by holding on to the door of his car. Both Miguel and Brad hurl various racially-based insults at one another. Brad and Miguel meet later that evening and discover that they will soon be in-laws. Both Marcus and Lucias family try to out do one another to make the wedding more African-American or Mexican-American, with comedic results.

Lucia has also not told her parents that she recently dropped out of Columbia Law School to volunteer teach at a charter school catering to recent immigrants. This leads Miguel to believe that she will be supporting Marcus as he volunteers as a doctor without pay. When Miguel tells Marcus that he disapproves of him living off his daughter, Lucia says nothing. Marcus feels abandoned, leading to an argument with Lucia about her commitment to their relationship. When Lucia asks Marcus if he no longer wants to get married, he replies he doesnt know, leading her to call off the wedding. She then angrily reveals to her parents the truth about dropping out of law school and living with Marcus for the past few months when Miguel offers to toss the ball and Lucias mother Sonia refers to it as our wedding.

Lucias sister Isabel (Anjelah Johnson), who disapproved of her sister getting married, makes Lucia and the rest of the family realize that Marcus makes Lucia happy and that race should not matter. Lucia goes to Marcus, they reconcile, and they end up having a wedding that embraces both African and Mexican customs.

During the end credits, Several pictures of the two families are shown depicting family events. Such as Isabels engagement to Harry (Harry Shum Jr.), who is Asian-American.

==Cast==
* Forest Whitaker as Bradford Boyd
* America Ferrera as Lucia Ramirez
* Lance Gross as Marcus Boyd
* Carlos Mencia as Miguel Ramirez
* Regina King as Angela
* Diana Maria Riva as Sonia Ramirez
* Anjelah Johnson as Isabel Ramirez
* Shannyn Sossamon as Ashley McPhee
* Charlie Murphy as T.J.
* Harry Shum Jr. as Harry (as Harry Shum)
* Hayley Marie Norman as Sienna

* Fred Armisen as Philip Gusto (DELETED SCENE Fred Armisen plays an eccentric wedding planner Phillip Gusto)

==Release==

===Box office===
Our Family Wedding In Theaters March 12, 2010. At the end of its theatrical run, the movie has collected $20,712,308 worldwide. 

===Critical reception===
The film has received generally negative reviews from critics. Fox Searchlight Pictures It currently holds a score of 39% "generally unfavorable" at the review aggregator site Metacritic,  and a "Rotten" score of 13% at Rotten Tomatoes. Rotten Tomatoes consensus is "Our Family Wedding is a mirthless, contrived affair that does little with its promising premise and talented cast." 

Roger Ebert of the Chicago Sun-Times gave the film two stars out of four, describing it as. He praised the performances of Ferrera and Gross.  Lisa Schwarzbaum of Entertainment Weekly gave the film a C-.

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 