Thai Veedu
{{Infobox film
| name           = Thai Veedu
| image          = 
| caption        = 
| director       = R. Thyagarajan
| producer       = C. Dhandayuthapani
| writer         = Thooyavan (dialogues)
| screenplay     = Devar Films Story Dept.
| story          = Devar Films Story Dept. Vijayakumar Silk Smitha Major Sundarrajan Thengai Srinivasan R. S. Manohar
| music          = Bappi Lahiri
| cinematography = V. Ramamoorthi
| editing        = M. G. Balu Rao
| studio         = Devar Films
| distributor    = Devar Films
| released       =  
| runtime        = 
| country        =   India Tamil
}}

Thai Veedu is a Tamil film is directed by R. Thyagarajan. It was remade in Hindi as Jeet Hamaari.

==Plot==

Major Sundarrajan plays a rich man in the movie who has the possession of an antique "sword" as a treasure. Thengai Srinivasan who is constantly eyeing to steal the sword from Major Sundararajan for a longtime with lot of unsuccessful attempts. On one such attempts Jaishankar saves Major Sundararajan and becomes his trusted guy to protect the sword. Rajinikanth (Character name is Raju)is an orphan, who is raised by an alcoholic ex-thief M.N Nambiar, is an expert in boosting cars. M.N Nambiar finds Rajinikanth as a child while stealing a car owned by Thengai Srinivasan several years back but he was not aware that the car and the child (Rajinikanth) actually belonged to Major Sundararajan. At one stage M.N Nambiar finds Thengai Srinivasan in a shopping place, believing that his adopted son Rajinikanth is the son of Thengai Srinivasan he hands Rajini to him as his own son. Thengai Srinivasan, in spite of knowing the truth, accepts Rajinikanth as his son and cleverly employs Rajinikanth to steal the "sword" from Major Sundarrajan, his real father. While Rajinikanth attempts to steal the sword, Jaishankar keeps resisting him. Despite Jaishankars efforts Rajinikanth manages to loot the sword but then realizes that hes the missing son of Major Sundarrajan and he himself had been instrumental in stealing his own family treasure for the wrong men. With the help of Jaishankar Rajinikanth fights with Thengai Srinivasans gang to successfully restore their family treasure in the right hands.

==Reception==
The movie was one of the template hits of Devar films and Rajinikanth. With the screenplay being knitted with all the action, humour and sentimental ingredients required for a Rajinikanth starrer, was received well in the box-office and was another hit for Rajinikanth.
The movie was remade to Hindi as "Jeet Hamaari" with Rajinikanth and Silk Smitha reprising their roles.

==Sound track==
The music composed by Bappi Lahiri. 

{| border="2" cellpadding="6" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center" Duration  (mm:ss) 
|----
|Aasai Nenje
|S. Janaki Vaali (poet)|Vaali
|6:42
|----
|Azhagiya Kodiye Aadadi
|S. P. Balasubrahmanyam
|4:40
|---
|Mama Mama En Paarthe
|S. P. Balasubrahmanyam, S. Janaki, K. J. Yesudas
|4:25
|----
|Unnai Azhaithathu
|S. P. Balasubrahmanyam, S. Janaki
|5:30
|----
|}

==References==
 

 
 
 
 


 