The Comedians (1941 film)
 
{{Infobox film
| name           = The Comedians
| image          = 
| caption        = 
| director       = Georg Wilhelm Pabst
| producer       = Hans Schweikart
| writer         = Olly Boeheim Axel Eggebrecht Georg Wilhelm Pabst Walter von Hollander
| narrator       = 
| starring       = Käthe Dorsch
| music          = 
| cinematography = Bruno Stephan
| editing        = Ludolf Grisebach
| studio         = Bavaria Filmkunst  
| distributor    = Bavaria Filmverleih
| released       =  
| runtime        = 111 minutes
| country        = Nazi Germany
| language       = German
| budget         = 
}}

The Comedians ( ) is a 1941 German drama film directed by Georg Wilhelm Pabst,    and based on the novel Philine by Olly Boeheim.  The film is set in the eighteenth century, and portrays the development of German theatre. 

==Plot==
Karoline Neuber attempts to improve the lot of actors, who are looked down upon as vagabonds.  When the Duchess refuses to let her son marry an actress, she defends them with such vehemence that she is driven from the country and finally dies in solitude.

==Cast==
* Käthe Dorsch as Karoline Neuber
* Hilde Krahl as Philine Schröder
* Henny Porten as Amalia, Herzogin von Weissenfels
* Gustav Diessl as Ernst Biron, Herzog von Kurland
* Richard Häussler as Armin von Perckhammer
* Friedrich Domin as Johann Neuber
* Ludwig Schmitz as Müller, der Hanswurst
* Sonja Gerda Scholz as Die Feigin
* Lucy Millowitsch as Die Lorenz
* Bettina Hambach as Victorine
* Walter Janssen as Koch, Charakterspieler
* Alexander Ponto as Kohlhardt, jugendlicher Liebhaber
* Viktor Afritsch as Graf Paul, Bruder der Herzogin von Weissenfels
* Kurt Müller-Graf as Studiosus Gotthold
* Harry Langewisch as Professor Gottsched
* Arnulf Schröder as Klupsch, Ratsherr in Leipzig

==References==
 

==Bibliography==
* Kreimeier, Klaus. The Ufa Story: A History of Germanys Greatest Film Company, 1918-1945. University of California Press, 1999.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 