Mohabbat Ki Kasauti
{{Infobox film
| name           = Mohabbat Ki Kasauti
| image          = 
| image_size     = 
| caption        = 
| director       = P. C. Barua
| producer       = New Theatres
| writer         = 
| narrator       = 
| starring       = K. L. Saigal Rattan Bai Pahadi Sanyal Noor Mohammed Charlie
| music          = R. C. Boral
| cinematography = Yusuf Mulji
| editing        = 
| distributor    =
| studio         = New Theatres
| released       = 1934
| runtime        = 121 minutes
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}} Bengali was 1934 Indian Indian "semi-historical" Jamuna started her career with a small role in the Hindi version.   
 Indian Talkies flashback technique in the narrative.   

==Cast==
*K. L. Saigal
*Rattan Bai
*Pahari Sanyal
*Noor Mohammed Charlie Jamuna
*Vishwanath

==Soundtrack==
The music direction was by R. C. Boral and the lyricist was Bani Kumar.    There were 13 songs in the film with "Sab Din Hott Na Ek Samaan" sung by Saigal with lyrics by Surdas.   

===Songlist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title 
|-
| 1
| Aa Ke Mujhe Raah Dikha De Raah Dikhanewale
|-
| 2
| Aalam Ko Pehchanenge Jaise Tumne Pehchana Hai
|-
| 3
| Chhayi Hai Kaisi Bahar
|-
| 4
| Dekho Dekho Ae Sainya Dekho Mohe Na Satao
|-
| 5
| Kaahe Pardesiya Mohe Aise Tadpao
|-
| 6
| Kaise Raseele Kaise Kateele Tore Nain Madmate
|-
| 7 
| More Maharaja Hain Bade Balwan
|-
| 8
| Naahi Padat Chain Tadpat Hun Din Rain
|-
| 9
| Naiya Mori Majhdhar Ishwar Kar De Paar
|-
| 10
| Prem Ki Mann Mein Katari Lagi
|-
| 11
| Sab Din Hott Na Ek Saman
|-
| 12
| Tora Sang Sainya Mose Na Chhoda Jaaye Re
|-
| 13
| Yeh Hai More Chanchal Chit Ki Pukar
|}

==References==
 

==External links==
* 

 
 
 
 
 

 