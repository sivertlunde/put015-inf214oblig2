Small Time
 
 
{{Infobox film
| name           = Small Time
| image          = SmallTime1996.jpg
| caption        = DVD cover
| director       = Shane Meadows
| producer       = Shane Meadows Dominic Dillon
| writer         = Shane Meadows
| starring       = Matt Hand Dena Smiles Shane Meadows
| music          = Gavin Clarke
| cinematography = Helene Whitehall David Wilson
| studio         = Big Arty Productions British Film Institute Intermedia Films
| distributor    = Mongrel Media (Canada)
| released       = 11 September 1996 (Toronto International Film Festival) 31 October 1997 (UK)
| runtime        = 60 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
Small Time is the first feature film by Shane Meadows.     It was written, produced and directed by Meadows. 

== Plot ==
The film follows a group of friends that are small time criminals in Sneinton, a suburb of Nottingham.  The main protagonists are best friends, Jumbo and Malc.  At the beginning of the film we hear Jumbo narrating, "Were not into anything heavy.  We just rob from the rich and sell it to the poor for half price.  Were just small time."

Malc and his partner Kate live next door to Jumbo and his partner Ruby.  Malc and Kate can continually hear the domestic abuse in the neighbouring house,  but Ruby is quick to defend Jumbo although she does concede that he is bad in bed and tells Kate that she uses a sex aid.

Malc is unhappy with the way his life is going but cannot seem to find a way out of the situation.  Kate is unhappy too and pushes Malc to sever ties with the group.  Kate attends a yoga class and makes friends with a classmate, Martin, who invites Kate and Malc to his house for the evening.   Kate, Malc, Martin and his girlfriend, Elaine are having a good evening until Jumbo turns up uninvited having found out where they were from their babysitter.  Malc makes it clear that he doesnt want Jumbo there.

Jumbo later relays this tale to the rest of the gang, who are not impressed with Malcs new attitude.  They decide to be nice to him until they have completed an armed robbery, which they need him for, and then cut him out of their circle of friends.  Malc is uncomfortable with the robbery but goes along anyway.

They raid a small new age store that sells incense, charms and the like.  Malc acts as a look out on the street with Terry, who is driving the getaway van.  Whilst the raid is going on, Malc convinces Terry to drive off.  Jumbo and the rest of the gang realise that there is no money on the premise they leave when they hear police sirens, but there is now no getaway van so they have to make a run for it.  Meanwhile Malc has taken the van and is doing a moonlight flit - in broad daylight - with his family.  The film ends with Jumbo being apprehended with by the police.

As the credits roll we see whats happened to the gang; Willy, Jumbo and Bets are in prison, Lenny gets his comeuppance for his dodgy deals, Mad Terry looking after Malc and Kates children in Skegness, where they now live.  Kate and Malc now run a refreshment hut by the sea.

== Cast ==
*Jumbo - Shane Meadows
*Malc - Mat Hand
*Ruby - Gena Kawecka 
*Kate - Dena Smiles
*Willy - Jimmy Hynd
*Bets - Leon Lammond Tim Cunningham
*Mad Terrance - Domonic Dillon Mark Armstrong
*Cuban Chef - Carlos Barreto
*Martin - Marcus Rowlands
*Elaine - Maria Woolley
*Man at door - Len Hand
*Yoga Tutor - Tanya Myers
*Hoover Buyer - Neil Johnson
*Trudy - Autumn Lily Smiles
*Denver - Ellya Kawecka
*Baby - Sun Orion Hand

==References==
 

==External links==
* 
* 

 
 
 
 
 

 