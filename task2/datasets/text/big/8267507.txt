Hawk of the Wilderness
{{Infobox Film|
| name           = Hawk of the Wilderness
| image          = Hawkofthewildernessposter.JPG
| image_size     = 
| caption        = Original poster for the 4th chapter of the serial John English
| producer       = Robert M. Beche
| writer         = Barry Shipman Rex Taylor Norman S. Hall Ridgeway Callow Sol Shor William L. Chester (novel) Herman Brix Ray Mala Monte Blue Jill Martin Noble Johnson William Royle Tom Chatterton
| music          = William Lava William Nobles Edgar Lyons
| editing        = Edward Todd Helene Turner
| distributor    = Republic Pictures
| released       =   3 December 1938 (serial) {{cite book
 | last = Mathis
 | first = Jack
 | title = Valley of the Cliffhangers Supplement 
 | origyear = 1995
 | publisher = Jack Mathis Advertising
 | isbn = 0-9632878-1-8
 | pages = 3, 10, 34–35 1966 (film) 
| runtime         = 12 chapters (213 minutes) (serial)  6 26½-minute episodes (TV series)  100 minutes (TV film) 
| country        =   English
| budget          = $117,987 (negative cost: $121,168) 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 Republic Movie pulp writer William L. Chester.
 Herman Brix produced serial The New Adventures of Tarzan.

==Plot== Native Americans, is shipwrecked.  The only survivors are Lincoln Rand Jr, Dr Rands son and his servant Mokuyi.  
 mutinies when they reach the island, abandoning Dr Munro and his expedition.  Fortunately, Dr Munro and company are rescued by Lincoln Rand Jr, alias Kioga, the adult son of Dr Rand, who has been raised on the island by Mokuyi.

==Cast== Herman Brix as Lincoln Rand Jr/Kioga ("Hawk of the Wilderness"), son of Dr Lincoln Rand Sr, a survivor of the initial shipwreck and raised by Mokuyi on the island
*Ray Mala as Kias, Kiogas servant
*Monte Blue as Yellow Weasel, villainous shaman opposed to Kioga, the Munros and the Smugglers
*Jill Martin as Beth Munro, Dr Munros daughter
*Noble Johnson as Mokuyi, the former servant of Kiogas late father. Rescued Kioga and, as the only other survivor of the shipwreck, raised him on the island
*William Royle as  Manuel Solerno, smuggler searching for wealth on the uncharted island
*Tom Chatterton as Dr Edward Munro, a scientist who leads an expedition to discover the fate of his old friend Dr Rand
*George Eldredge as Allen Kendall, a member of Dr Munros expedition
*Patrick J. Kelly as William Williams/Bill Bill, another member of Dr Munros expedition
*Dick Wessel as Dirk
*Fred Toones as George, Dr Munros servant
*Tuffie the dog as Tawnee

==Production==
Hawk of the Wilderness was filmed between 18 September and 13 October 1938, with location filming in Mammoth Lakes, California.  The serial was budgeted for $117,987 but the final negative cost rose slightly to $121,168. 

Tuffie was cast when his trainer, during the interview, said "Tuffie, its dark in here.  Turn on the light."  Tuffie did so by finding the switch, pulling a chair across to reach it and flipping the switch with his paw. Witney, William (2005). In a Door, Into a Fight, Out a Door, Into a Chase: Moviemaking Remembered by the Guy at the Door. McFarland & Company. ISBN 978-0-7864-2258-6 

Silent parts of the serial were filmed with a one-inch lens.  Cameraman Edgar Lyons had initially been filming more of the clouds in the sky than the actors, with the effect of partially cutting them out of the shot.  The studio complained.  Director William Witney compromised with the use of the wider lens, which would take in both cloudscape and actors.  Only silent scenes were shot in this manner because the camera would be both closer to the actors and take in more of the surroundings, preventing the microphone from getting close enough to work properly. 

===Special Effects===
The special effects in this serial were created by the Lydecker brothers.

===Stunts===
*Ted Mapes as Kioga (doubling Herman Brix)
*James Dime
*George Montgomery
*Henry Wills

==Release==
===Theatrical===
Hawk of the Wilderness official release date is 3 December 1938, although this is actually the date the sixth chapter was made available to film exchanges. 

===Television===
In the early 1950s, Hawk of the Wilderness was one of fourteen Republic serials edited into a television series.  It was broadcast in six 26½-minute episodes. 

It was also one of twenty-six Republic serials re-released as a film on television in 1966.  The title of the film was changed to Lost Island of Kioga.  This version was cut down to 100-minutes in length. 

==Critical reception==
The burial of Kias in the final chapter is regarded by Cline as one of the "very few successful attempts at drama in serials." {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | origyear = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | chapter = 3. The Six Faces of Adventure
 | page = 37
 }} 

==Chapter titles==
# Mysterious Island (28min 59s)
# Flaming Death (16min 40s)
# Tiger Trap (16min 46s)
# Queens Ransom (16min 50s)
# Pendulum of Doom (16min 35s)
# The Dead Fall (16min 40s)
# White Mans Magic (16min 41s)
# Ambushed (16min 41s)
# Marooned (16min 41s) - a clipshow|re-cap chapter
# Camp of Horror/Caves of Horror (16min 39s) 
# Valley of Skulls (16min 41s)
# Trails End (16min 40s)
 Source:   {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | origyear = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | chapter = Filmography
 | page = 223
 }} 

This was one of the two 12-chapter serials released by Republic in 1938, the other was The Fighting Devil Dogs.  This year began the studios standard release pattern of two 12-chapter and two 15-chapter serials in every year.  This pattern remained until 1944 with the exception of 1942, which was only one 15-chapter serial released.

==References==
 

==External links==
*  
*  

 
{{succession box  Republic Serial Serial 
| before=Dick Tracy Returns (1938 in film|1938)
| years=Hawk of the Wilderness (1938 in film|1939)
| after=The Lone Ranger Rides Again (1939 in film|1939)}}
{{succession box  English Serial Serial 
| before=Dick Tracy Returns (1938 in film|1938)
| years=Hawk of the Wilderness (1938 in film|1939)
| after=The Lone Ranger Rides Again (1939 in film|1939)}}
 

 
 

 
 
 
 
 
 
 
 
 
 