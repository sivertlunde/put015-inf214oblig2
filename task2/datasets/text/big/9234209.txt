My Name Is Shanghai Joe
{{Infobox film
| name           = My Name Is Shanghai Joe
| image          = 4monnomshangaijoez2hd.jpg
| caption        = DVD cover.
| director       = Mario Caiano
| producer       = 
| writer         = Mario Caiano Fabrizio Trifone Trecca|T.F. Karter 
| starring       = Chen Lee
| music          = Bruno Nicolai
| cinematography = Guglielmo Mancori
| editing        = Amedeo Giomini
| distributor    = Beacon Films Inc.
| released       = 
| runtime        = 98 min
| country        = Italy Italian
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 Chinese immigrant, Mexican slaves from their cruel master. The film was released in a number of alternate titles in the United States, including The Fighting Fists of Shanghai Joe, To Kill or to Die and The Dragon Strikes Back. The film was directed by Mario Caiano and starred Chen Lee as Shanghai Joe.   

==Cast==
* Chen Lee - Shanghai Joe
* Klaus Kinski - Scalper Jack
* Claudio Undari - Pedro, The Cannibal (as Robert Hundar)
* Katsutoshi Mikuriya - Mikuja
* Gordon Mitchell - Sam
* Carla Romanelli - Cristina
* Carla Mancini - Conchita
* Giacomo Rossi-Stuart - Triky
* George Wang - Yang
* Federico Boido - Slim (as Rick Boyd)
* Francisco Sanz - Cristinas Father
* Piero Lulli - Spencer
* Umberto DOrsi - The poker player

==DVD release==
On May 26, 2009, a Region 0 DVD of the movie was released by Alpha Video. 

==References==
 

==External links==
* 

 
 
 
 
 
 


 
 