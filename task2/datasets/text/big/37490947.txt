Routine Love Story
{{Infobox film
| name           = Routine Love Story
| image          = RLS Poster HD.jpg
| caption        = 
| director       =Praveen Sattaru
| producer       = Chanakya Booneti
| writer         = Praveen Sattaru
| starring       = Sundeep Kishan Regina Cassandra
| music          = Mickey J. Meyer
| cinematography = Suresh Bhargav
| editing        = Dharmendra
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Telugu
| budget         =   
| gross          =  
}}

Routine Love Story is a 2012 Telugu film starring Sundeep Kishan and Regina Cassandra. The movie is directed by Praveen Sattaru, who earlier made LBW (Life Before Wedding) and produced by Chanakya Booneti.  Soundtrack of the film was composed by Mickey J Meyer.  The film was Released on November 23, 2012 along with Nagarjunas Dhamarukam and both the films got a good start at the Box Office in Andhra Pradesh and other parts of the world. 

==Cast==
 
* Sundeep Kishan as Sanju
* Regina Cassandra as Tanvi
* MS Narayana
* Vennela Kishore Hema
* Chandra Mohan
* Kavitha
* Master Nikhil
* Jhansi Laxmi
* Thagubothu Ramesh
* RJ Hemanth
* RJ Bharat
* Snigdha
* Naveen
* Swapnika
 

==Soundtrack==
{{Infobox album
| Name = Routine Love Story
| Longtype = to Routine Love Story
| Type = Soundtrack
| Artist = Mickey J Meyer
| Cover =
| Released = 15–20 October 2012
| Recorded = 2012 Feature film soundtrack
| Length = 24:28 Telugu
| Label = Aditya Music
| Producer = Mickey J Meyer
| Reviews = Life Is Beautiful (2012)
| This album = Routine Love Story (2012)
| Next album = Seethamma Vakitlo Sirimalle Chettu (2012)
}}

The audio of the film was launched in a very different way. On 15 October 2012 the song 1 was released in Radio Mirchi with Mickey J Meyer between 6:00pm and 7:00pm. On 16 October 2012 the  song 2 was released in TV9 during evening news bulletin with Laxmi Manchu between 6:00pm and 7:00pm. On 17 October 2012 the song 3 was released in MAA Music "something special" Show with Manoj Manchu between 8:30am and 10:00am. On 18 October 2012 the song 4 was released in idlebrain.com. On 20 October 2012 the song 5 was released with conventional audio launch function whose live coverage was done by TV9 and MAA TV. 

{{tracklist
| headline = Tracklist
| extra_column = Singer(s)
| total_length = 24:28
| lyrics_credits = yes
| title1 = Naa Manasupai
| lyrics1 = Anantha Sreeram Sreeramachandra
| length1 = 4:16
| title2 = Neethone Unna
| lyrics2 = Vanamali
| extra2 =  Mickey J Meyer
| length2 = 4:41
| title3 = Vela Talukutaarale
| lyrics3 = Krishna Chinni Karthik
| length3 = 4:43
| title4 = Yeppatikaina
| lyrics4 = Anantha Sreeram
| extra4 = Naresh Iyer
| length4 = 4:42
| title5 = Nee Varasa Neede
| lyrics5 = Anantha Sreeram Karthik
| length5 = 4:38
| title6 = Routine Love Story Theme
| lyrics6 = 
| extra6 = Deepu
| length6 = 1:28
}}

==Reception==
The movie got Positive Reviews. 123telugu.com gave a review stating "Routine Love Story is a film which has its share of highs and lows. Fresh feel, good chemistry between the lead pair and some nice conversations are plus points. On the flip side, unnecessary and bad comedy tracks and irrelevant characters spoil the experience. Overall, RLS is a decent entertainer that will appeal to youngsters as a one time watch."  idlebrain.com gave a review stating "Routine Love Story departs from the routine love stories of Telugu cinema and tries to be different. The urbane youth humor makes sure that a subject of this nature is received with open arms. Praveen Sattaru who has shown sparks with his directorial debut LBW upped the ante now to reach for more audiences with a subject that appeals to both youth and urban families. You may watch Routine Love Story for humor and for an insight into relationships in changing times."  Oneindia Entertainment gave a review stating "Although, it is titled Routine Love Story, it has a fresh and brand new story, which has some realistic moments, which young audience can relate to their life. It is a must watch for youth."  apherald.com gave a review stating "A routine love story is presented in a youthful entertaining way , worth watching this weekend."  IndiaGlitz gave a review stating "All in all, RLS makes a beautiful movie watching experience for laic as well as discerning audiences."  Rediff.com gave a review stating "The film scores on account of the realism and logic despite being a commercial entertainer. The arguments and patch-ups are like everyday life but dont bore the audience. The bonding, separation, attitudes and differences between the couple are hugely believable and relatable in todays age. In that sense, the film moves in a lifelike way and not in a dream world. RLS may find favour with the urban audience, especially the youth, who may connect to it instantly." 

==References==
 

 
 
 