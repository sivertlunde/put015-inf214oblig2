Death to the Supermodels
{{multiple issues|
 
 
}}

{{Infobox film
| name           = Death to the Supermodels
| image          = Deathtosupermodelsdvd.jpg
| caption        = DVD cover
| director       = Joel Silverman
| producer       = Juan Feldman
| writer         = Joel Silverman
| starring       = Jaime Pressly Brooke Burns Taylor Negron Matt Winston Diane Delano Sung Hi Lee Jason Acuña
| music          = Jawara Fred Wilson 
| cinematography = William MacCollum
| editing        = Michael Alberts
| distributor    = MGM Home Entertainment
| released       =  
| runtime        = 84 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Death to the Supermodels is a 2005 black comedy starring Jaime Pressly, Brooke Burns, Kimberley Davies, Taylor Negron, Matt Winston, Diane Delano and Jason Acuña. It was written and directed by Joel Silverman.

==Plot==

Jaime Pressly stars as Tiffany Courtney, an overly perky editor working for Merle Magazine. To give her companys magazine a more legitimate readership (the main readers of the magazine are currently prisoners, transsexuals, priests and gays), Tiffany proposes to her boss that the magazine hold a swimwear shoot on a tropical island starring the worlds top five greatest supermodels.

1: Eva: (Burns) German beauty ranked as the worlds number-one supermodel. Eva considers herself as seductive and refuses to wash or cut her armpit hair.

2: Yo: An African American beauty from New York. Yo is famous for her large booty and is most renowned for inspiring rapper Inky Stinky to compose a rap about her behind.

3: Darbie: (Davies) Blonde and slender; American supermodel Darbie is known for her ability to transform herself in a human doll. She is reported to have spent 12 days in the window of Bloomingdales in New York, before store clerks realized she wasnt a mannequin.

4: Hou-Che: Asian beauty, famous for her eighth-degree black belt in martial arts and for her breast implants, to which she seeks the forgiveness of her deceased grandfather.

5: P: Spanish P is famous for her heartbreaking pouting and her bipolar disorder.

To take the pictures Tiffany recruits Gunter and Gerd, two asexual photographers from Germany.

The supermodels all agree to participate and are flown to the Islands of Isis, a tropical paradise where the shoot is to take place. Things dont start of well for Tiffany. As well as showing hatred towards each other the supermodels are instantly put of by Tiffanys excessive perkiness. Things go from bad to worse for Tiffany when she starts hearing the voice of Ryan from her self-help tapes in her head telling her to murder the supermodels.
Once Tiffany starts hearing the voices a Ninja begins to murder the supermodels. P is shot in the back by a spear gun during a shoot. Hou-Che has her neck broken whilst in hand to hand combat with the Ninja (following her death Hou-Che is revealed to be a transsexual, has male genitalia and appears as a man on a Chinese passport). Despite the murders Tiffany is determined to go ahead with the shoot. The next to be killed is Yo who dies of a massive bout of flatulence after the Ninja tampered with steroid suppositories she was taking to maintain her large behind. The voices in Tiffanys head continue to tell her that she had killed them and suspicion falls on Tiffany from the remaining models.
On the final day of the shoot, Evas failed attempts to seduce the asexual photographers results in her abandoning the shoot. She is later found shot along with Gunter. The Ninja confronts Tiffany and Gerd and is unmasked and is Tiffanys boss Merle. Darbie is revealed to be Merles lesbian lover. Merle and Darbie (whose real name is revealed to be Sarah) had conspired to kill the supermodels to promote the image of a new, intelligent supermodel. Gerd was also in on the scheme to escape from the dominance the mute Gunter to which he is basically a translator. Merle reveals that Tiffany is a major pawn in her scheme. The voices in her head were part of a scheme involving micro transmitters inserted in Tiffanys ears whilst she was having surgery to remove a freckle on her left nipple which Tiffany was concerned would turn cancerous. The voices are provided by Ryan himself (Wee Man) who is another associate of Merle. He is also Dieter, Evas poolboy. Merle turns the gun on Tiffany, she tries to talk them out of killing anyone else but is shot dead to set her up as the killer. The movie ends with Merle and Sarah/Darbie on the beach, talking about the future, but Merle is then shot (presumably with fatal consequences) by Sarah/Darbie assumably so only she would control the new wave of models.

==Notes==

The film features actresses who really are models.  Jaime Pressley and Brooke Burns both have modeling backgrounds.  Eva Derrek is a former Miss Germany with extensive experience as a fashion and fitness model (and incidentally a former flight attendant for a German airline).  She plays a French model in the film.

==External links==
* 
* 

 
 
 
 