Nosferatu
 
{{Infobox film
| name           =  
| image          = Nosferatuposter.jpg
| image_size     = 
| alt            = 
| caption        = 
| director       = F. W. Murnau
| producer       = {{plainlist|
* Enrico Dieckmann
* Albin Grau
}}
| screenplay     = Henrik Galeen
| based on       =  
| starring       = {{plainlist|
* Max Schreck
* Gustav von Wangenheim
* Greta Schröder
* Alexander Granach
* Ruth Landshoff Wolfgang Heinz
}}
| music          = Hans Erdmann
| cinematography = {{plainlist|
* Fritz Arno Wagner
* Günther Krampf
}}
| distributor    = Film Arts Guild
| released       =  
| runtime        = 94 minutes
| country        = Weimar Republic
| language       = {{plainlist|
* Silent film
* German and English intertitles
}}
}}
  German Expressionist horror film, directed by F. W. Murnau, starring Max Schreck as the vampire Count Orlok.

The film, shot in 1921 and released in 1922, was an unauthorized adaptation of Bram Stokers Dracula, with names and other details changed because the studio could not obtain the rights to the novel (for instance, "vampire" became "Nosferatu (word)|Nosferatu" and "Count Dracula" became "Count Orlok"). Stokers heirs sued over the adaptation, and a court ruling ordered that all copies of the film be destroyed. However, one print of Nosferatu survived, and the film came to be regarded as an influential masterpiece of cinema.   As of 2015, it is Rotten Tomatoes second best-reviewed horror film of all time. 

==Plot==
Thomas Hutter lives in the fictitious German city of Wisborg. His employer, Knock, sends Hutter to Transylvania to visit a new client named Count Orlok. Hutter entrusts his loving wife Ellen to his good friend Harding and Hardings sister Annie, before embarking on his long journey. Nearing his destination in the Carpathian mountains, Hutter stops at an inn for dinner. The locals become frightened by the mere mention of Orloks name and discourage him from traveling to his castle at night, warning of a werewolf on the prowl. The next morning, Hutter takes a coach to a high mountain pass, but the coachmen decline to take him any further than the bridge as nightfall is approaching. A  black-swathed coach appears after Hutter crosses the bridge and the coachman gestures for him to climb aboard. Hutter is welcomed at a castle by Count Orlok. When Hutter is eating dinner and accidentally cuts his thumb, Orlok tries to suck the blood out, but his repulsed guest pulls his hand away.

Hutter wakes up to a deserted castle the morning after and notices fresh punctures on his neck, which he attributes to mosquitoes or spiders. That night, Orlok signs the documents to purchase the house across from Hutters own home.  Hutter writes a letter to his wife and gets a coachman to send it. Reading a book about vampires that he took from the local inn, Hutter starts to suspect that Orlok is Nosferatu, the "Bird of Death." He cowers in his room as midnight approaches, but there is no way to bar the door. The door opens by itself and Orlok enters, his true nature finally revealed, and Hutter falls unconscious. The next day, Hutter explores the castle. In its crypt, he finds the coffin in which Orlok is resting dormant. Hutter becomes horrified and dashes back to his room. Hours later from the window, he sees Orlok piling up coffins on a coach and climbing into the last one before the coach departs. Hutter escapes the castle through the window, but is knocked unconscious by the fall and awakes in a hospital.

When he is sufficiently recovered, he hurries home. Meanwhile, the coffins are shipped down river on a raft. They are transferred to a schooner, but not before one is opened by the crew, revealing a multitude of rats. The sailors on the ship get sick one by one; soon all but the captain and first mate are dead. Suspecting the truth, the first mate goes below to destroy the coffins. However, Orlok awakens and the horrified sailor jumps into the sea. Unaware of his danger, the captain becomes Orloks latest victim when he ties himself to the wheel. When the ship arrives in Wisborg, Orlok leaves unobserved, carrying one of his coffins, and moves into the house he purchased. The next morning, when the ship is inspected, the captain is found dead. After examining the logbook, the doctors assume they are dealing with the Plague (disease)|plague. The town is stricken with panic, and people are warned to stay inside.

There are many deaths in the town, which are blamed on the plague. Knock, who had been committed to a psychiatric ward, escapes after murdering the warden. The townspeople give chase, but he eludes them by climbing a roof, then using a scarecrow. Meanwhile, Orlok stares from his window at the sleeping Ellen. Against her husbands wishes, Ellen had read the book he found. The book claims that the way to defeat a vampire is for a woman who is pure in heart to distract the vampire with her beauty all through the night. She opens her window to invite him in, but faints. When Hutter revives her, she sends him to fetch Professor Bulwer. After he leaves, Orlok comes in. He becomes so engrossed drinking her blood that he forgets about the coming day. When a rooster crows, Orlok vanishes in a puff of smoke as he tries to flee. Ellen lives just long enough to be embraced by her grief-stricken husband. The last scene shows Count Orloks ruined castle in the Carpathian Mountains, symbolizing the end of Count Orloks reign of terror.

==Cast==
* Max Schreck as Count Orlok Thomas Hutter Ellen Hutter Knock
* Shipowner Harding Annie
* Professor Bulwer Professor Sievers
* Max Nemetz as The Captain of The Empusa Wolfgang Heinz as First Mate of The Empusa
* Heinrich Witte as guard in asylum
* Guido Herzfeld as innkeeper
* Karl Etlinger as student with Bulwer
* Hardy von Francois as hospital doctor
* Fanny Schreck as hospital nurse

==Production==
 ; this photograph is from 1970.]]
  German film studio founded in 1921 by Enrico Dieckmann and Albin Grau, named for the Buddhist concept of prana. Its intent was to produce occult and supernatural themed films. Nosferatu was the only production of Prana Film  as it declared bankruptcy in order to dodge copyright infringement suits from Bram Stokers widow.

Grau had the idea to shoot a vampire film; the inspiration arose from Graus war experience: in the winter of 1916, a Serbian farmer told him that his father was a vampire and one of the Undead. 
 Expressionist style  screenplay was poetically rhythmic, without being so dismembered as other books influenced by literary Expressionism, such as those by Carl Mayer. Lotte Eisner described Galeens screenplay as " " ("full of poetry, full of rhythm"). Eisner 1967, page 27  

  in Lübeck served as the set for Orloks house in Wisborg.]] shots in Aegidienkirche served Orava Castle, Johannisthal locality and further exteriors in the Tegel Forest.

 
For cost reasons, cameraman Fritz Arno Wagner only had one camera available, and therefore there was only one original negative.  The director followed Galeens screenplay carefully, following handwritten instructions on camera positioning, lighting, and related matters.  Nevertheless Murnau completely rewrote 12 pages of the script, as Galeens text was missing from the directors working script. This concerned the last scene of the film, in which Ellen sacrifices herself and the vampire dies in the first rays of the Sun.   Murnau prepared carefully; there were sketches that were to correspond exactly to each filmed scene, and he used a metronome to control the pace of the acting. 

==Music== James Bernard, Hammer horror films in the late 1950s and 1960s, has written a score for a reissue. 

==Deviations from the novel== Arthur and Quincey Morris|Quincey, and changes all of the characters names (although in some recent releases of this film, which is now in the public domain in the United States but not in most European countries, the written dialogue screens have been changed to use the Dracula versions of the names). The setting has been transferred from Britain in the 1890s to Germany in 1838.

In contrast to Dracula, Orlok does not create other vampires, but kills his victims, causing the townfolk to blame the plague, which ravages the city. Also, Orlok must sleep by day, as sunlight would kill him, while the original Dracula is only weakened by sunlight. The ending is also substantially different from that of Dracula. The count is ultimately destroyed at sunrise when the "Mina" character sacrifices herself to him. The town called "Wisborg" in the film is in fact a mix of Wismar and Lübeck.   

==Release==
 , here shown in a 1900 postcard, was where Nosferatu premiered.]]
Shortly before the premiere, an advertisement campaign was placed in issue 21 of the magazine  , with a summary, scene and work photographs, production reports, and essays, including a treatment on vampirism by Albin Grau.  Nosferatus preview premiered on 4 March 1922 in the Marmorsaal of the Berlin Zoological Garden. This was planned as a large society evening entitled   (Festival of Nosferatu), and guests were asked to arrive dressed in Biedermeier costume.   The cinema premiere itself took place on 15 March 1922 at Berlins  .

==Reception and legacy==
Nosferatu brought the director Murnau reinforced into the public eye, especially since his film The Burning Acker was released a few days later. The press reported extensively on Nosferatu and its premiere. With the laudatory votes, there was also occasional criticism that the technical perfection and clarity of the images did not fit the horror theme. The Filmkurier of 6 March 1922 said that the vampire appeared too corporeal and brightly lit to appear genuinely scary. Hans Wollenberg described the film in photo-Stage No. 11 of 11 March 1922 as a "sensation" and praised Murnaus nature shots as "mood-creating elements."   In the Vossische newspaper of 7 March 1922, Nosferatu was praised for its visual style.
 Florence Stoker, sued for copyright infringement and won. The court ordered all existing prints of Nosferatu burned, but one purported copy of the film had already been distributed around the world. These prints were duplicated over the years, kept alive by a cult following, making it an example of an early cult film. 

The movie has received overwhelmingly positive reviews. On Rotten Tomatoes it has a "Certified Fresh" label and holds a 97% "fresh" rating based on 60 reviews.  It was ranked twenty-first in Empire (magazine)|Empire magazines "The 100 Best Films of World Cinema" in 2010.  In 1997, critic Roger Ebert added Nosferatu to his list of The Great Movies, writing:
 

===In popular culture===
 
In 1989, French progressive rock outfit Art Zoyd released Nosferatu (Art Zoyd album)|Nosferatu on Mantra Records. Thierry Zaboitzeff and Gérard Hourbette composed the pieces, to correspond with a truncated version of the film then heavily in circulation in the public domain. 
 Graveyard Shift", was written by Jay Lender, who pushed for a random gag that featured Orlok.  Though, here he is called Nosferatu.

In 2010, the Mallarme Chamber Players of Durham, North Carolina, commissioned composer Eric J. Schwartz to compose an experimental chamber music score for live performance alongside screenings of the film, which has since been performed a number of times. 

In 2012, scenes from the film were used in the exhibition Dark Romanticism at the Städel in Frankfurt as an example to illustrate the way in which ideas developed in 18th- and 19th-century art influenced story telling and aesthetics in 20th-century cinema. 

===Derivative works=== The song The satirical Joe Hill is a modern vampire story that utilizes the play on words of "Nosferatu." |
In 2009, Louis Pecsi wrote and illustrated the graphic novel Nosferatu: The Untold Origin, which gives an origin story to Count Orlock. 
}}

==See also==
 
* List of films in the public domain in the United States
* List of German films 1919–1933
* Vampire film

==Notes==
 

==References==
*   
*   
*   
*   
*    (1921-1922 reports and reviews)

==External links==
 
 
*  
*   ( )
*   complete film on YouTube

 
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 