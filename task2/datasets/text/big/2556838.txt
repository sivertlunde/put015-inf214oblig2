Chisum
 
{{Infobox film
| image          = chisum1.jpg
| caption        =
| name           = Chisum Andrew V. McLaglen
| producer       = Andrew J. Fenady
| writer         = Andrew J. Fenady
| narrator       = William Conrad Ben Johnson Geoffrey Deuel
| music          = Dominic Frontiere
| cinematography = William H. Clothier
| studio         = Batjac Productions Robert L. Simpson
| distributor    = Warner Bros.
| released       =  
| runtime        = 111 min.
| country        = United States English
| budget         =
| gross          = $6,000,000 (US/ Canada rentals)    
}}
 Western film starring John Wayne.
 Ben Johnson, Glenn Corbett, Geoffrey Deuel, Andrew Prine, Bruce Cabot, Patric Knowles, and Richard Jaeckel.
 Directed by Andrew V. McLaglen, it was adapted for the screen by Andrew J. Fenady from his short story, "Chisum and the Lincoln County Cattle War".

Although this movie is historically inaccurate in many details, it is loosely based on events and characters from the Lincoln County War of 1878 in New Mexico Territory, which involved Pat Garrett and Billy the Kid among others.

==Synopsis==
John Chisum (John Wayne), a virtuous, patriarchal land baron, locks horns with greedy Lawrence Murphy (Forrest Tucker), who will stop at nothing to get control of the trade and even the law in Lincoln County, New Mexico.

Chisum is an aging rancher with an eventful past and a paternalistic nature towards his companions and community. Murphy, a malevolent land developer, plans to take control of the county for his own personal gain.
 Ben Johnson) stop the bandits with help from a newcomer to the area, William Bonney (Geoffrey Deuel), also known as Billy the Kid. A notorious killer, Billy has been given a chance to reform by Chisums philanthropic neighbor, rancher Henry Tunstall (Patric Knowles). Billy also falls for Chisums newly arrived niece, Sallie (Pamela McMyler).

Murphy is buying up all the stores in town and using his monopoly to push up the prices. He appoints his own sheriff and deputies. He also brings in a lawyer, Alex McSween (Andrew Prine), whose principles lead him to switch sides and seek work with Chisum and Tunstall. The two ranchers set up their own bank and general store in town under McSweens control.

Chisums land and cattle remain targets. Murphys men attempt to steal Chisums cattle before he can sell them to the Army. Chisums ranch hands are warned by Pat Garrett (Glenn Corbett), a passing buffalo hunter. Garrett agrees to help Chisum and soon befriends Bonney. Together they foil an attack by Murphys men on the wagons bringing in provisions for the new store.
 Alan Baxter). On the way he is intercepted by Murphys deputies, who falsely accuse him of cattle rustling and shoot him dead. Chisum and Garrett hunt down the deputies and ride them back towards town for trial. Bonney, seeking revenge for the murder of his mentor, overpowers Garrett and shoots dead both deputies. Before Sheriff Brady (Bruce Cabot) can organise a posse, Billy rides into town and kills him too.

Murphy appoints bounty hunter Dan Nodeen (Christopher George) as the new sheriff, giving him orders to hunt down Bonney. Nodeen has a score to settle, as a previous encounter with Bonney has left him with a permanent limp.

Billys plans for revenge are only just beginning. He breaks into McSweens store looking for dynamite. He is spotted by Nodeen, who gets Murphys men to surround the store. McSween comes out unarmed but Nodeen shoots him in cold blood.

Chisum is alerted by McSweens wife (Lynda Day George) and rides into town. The main street is blocked, so Chisum stampedes his cattle through the barricades. He tracks down Murphy and takes him on in a fist fight which ends with both men falling from a balcony. Murphy ends up impaled on steer horns. With his paymaster dead, Nodeen flees with Billy in pursuit.

The film ends with Garrett taking over as sheriff and settling down with Sallie. Its been learned that General Lew Wallace takes over as governor of the area. With law and order restored, Chisum can resume his iconic vigil over the Pecos valley.

==Cast==
* John Wayne as John Chisum 
* Forrest Tucker as Lawrence Murphy Ben Johnson as James Pepper, Chisums sidekick
* Patric Knowles as Henry Tunstall (based on John Tunstall)
* Geoffrey Deuel as Billy The Kid 
* Pamela McMyler as Sallie Chisum 
* Glenn Corbett as Pat Garrett 
* Andrew Prine as Alexander McSween
* Christopher George as Dan Nodeen   Sheriff Brady 
* Richard Jaeckel as Jesse Evans, Murphys lead henchman  Lynda Day as Sue McSween 
* Robert Donner as Morton (Deputy Sheriff)
* John Mitchum as Baker (Deputy Sheriff)
* John Agar as Amos Patton, a shopkeeper ousted by Murphy
* Gregg Palmer as Karl Riker John M. Pickard as Sergeant Braddock
* Christopher Mitchum as Tom OFolliard

==Production==
Michael A. Wayne, executive producer, took on the project of making Chisum because he felt the story summed up well his fathers political views. The sizeable cast is packed with familiar faces from earlier John Wayne films, as well as friends such as Forrest Tucker. It was filmed in 1969 in Durango, Mexico. The picturesque vistas of the area were captured by cinematographer William H. Clothier.  The film was originally made for 20th Century Fox, but they sold the film to Warner Bros. 
 Academy Award True Grit.

The songs "The Ballad of John Chisum" was narrated by William Conrad, the song "Turn Me Around" was sung by Merle Haggard.
 
During filming, John Mitchum, brother of Robert, introduced John Wayne to his patriotic poetry. Seeing that Wayne was greatly moved by the word, Forrest Tucker suggested that the two collaborate to record some of the poetry, which resulted in a Grammy-nominated spoken-word album, America: Why I Love Her.

Chisum re-united several actors from Sands of Iwo Jima. John Wayne, John Agar, Forrest Tucker, and Richard Jaeckel.

==Box office and reception==
Released in June 1970, the film grossed $6 million at the box office. 
 President Richard Nixon commented on the film during a press conference in Denver, Colorado, on 3 August 1970. In doing so, he used the film as a context to explain his views on law and order: 
 Over the Western White Tricia selected it, was "Chisum" with John Wayne. It was a western. And as I looked at that movie, I said, "Well, it was a very good western, John Wayne is a very fine actor and it was: a fine supporting cast. But it was just basically another western, far better than average movies, better than average westerns."

I wondered why it is that the western survives year after year after year. A good western will outdraw some of the other subjects. Perhaps one of the reasons, in addition to the excitement, the gun play, and the rest, which perhaps is part of it but they can get that in other kinds of movies but one of the reasons is, perhaps, and this may be a square observation-is that the good guys come out ahead in the westerns; the bad guys lose.

In the end, as this movie particularly pointed out, even in the old West, the time before New Mexico was a State, there was a time when there was no law. But the law eventually came, and the law was important from the standpoint of not only prosecuting the guilty, but also seeing that those who were guilty had a proper trial.}}

"Chisum" is on dvd through Amazon.com, Warner Home Video will release "Chisum" for the first time on blu ray on December 14, 2014

==See also==
* John Wayne filmography

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 