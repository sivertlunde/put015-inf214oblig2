Tōki Rakujitsu
 
{{Infobox Film
| name           = Tōki Rakujitsu
| director       = Seijirō Kōyama
| producer       = Kazuyoshi Okuyama
| writer         = Kaneto Shindō (novel) Kaneto Shindō Junichi Watanabe
| starring       = Hiroshi Mikami Yoshiko Mita Chōichirō Kawarazaki Riho Makise Tatsuya Nakadai Tetsuko Kobayashi Julie Dreyfus
| music          = Tetsuji Hayashi
| cinematography = 
| editing        = Osamu Inoue
| distributor    = Shochiku
| released       = July 4, 1992
| runtime        = 119 min
| country        = Japan
}}
  is a Japanese film released in 1992, which was directed by  .  The screenplay was written by Kaneto Shindō. It stars Hiroshi Mikami.

==Cast==
* Yoshiko Mita - Shika Noguchi
* Hiroshi Mikami - Noguchi
* Tatsuya Nakadai - Kobayashi
* Riho Makise - Yoneko Yamauchi
* Takahiro Tamura - Ryutaro
* Choichiro Kawarasaki
* Shingo Yamashiro - Watanabe
* Toshinori Omi
* Hiroyuki Nagato
* Julie Dreyfus - Mary

==References==
 

== External links ==
*  

 
 
 
 
 


 