Studio Stoops
{{Infobox Film |
  | name           = Studio Stoops |
  | image          = StudioStoopsTITLE.jpg|
  | caption        = |
  | director       = Edward Bernds
  | writer         = Elwood Ullman| Kenneth MacDonald Chuck Hamilton Charles Jordan Stanley Price| Vincent Farrar | 
  | editing        = Henry DeMond |
  | producer       = Hugh McCollum |
  | distributor    = Columbia Pictures |
  | released       =  
  | runtime        = 16 00"
  | country        = United States
  | language       = English
}}

Studio Stoops is the 126th short subject starring American slapstick comedy team the Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959.

==Plot==
The Stooges are exterminators mistaken for B.O. Pictures publicity department. They are then instructed to drum up publicity for the studios lovely new actress  Dolly Devore (Christine McIntyre), and arrange a fake kidnapping. 

However, two gangsters hear the Stooges plan and kidnap Devore for real, forcing the Stooges to come to her rescue. Shemp winds up hanging out a tenth-story window from an old-fashioned extension telephone.
 

==Production notes==
Studio Stoops was filmed on February 22-25, 1949.   

The gag of Shemp hiding in a garment bag in the hotel room closet then managing to get out of the closet, wandering through an unbarricaded French window onto the narrow ledge surrounding the hotel on the 14th floor. was adapted from Buster Keatons film "So You Wont Squawk" (Columbia, 1941). 

==References==
 

== External links ==
* 
* 
*  at threestooges.net

 

 
 
 
 
 
 
 

 