Ratchet & Clank (film)
 
{{Infobox film
| name = Ratchet & Clank
| image = Ratchet & Clank poster.jpg
| director = Jericca Cleland Kevin Munroe
| producer = Kylie Ellis Brad Foxhoven David Wohl
| writer = T.J. Fixman
| based on =   Jim Ward
| music = Evan Wise
| cinematography = Anthony Di Ninno
| editing = Braden Oberson
| studio = Rainmaker Entertainment Blockade Entertainment
| distributor =  Cinema Management Group   
| released =  
| runtime = 
| country = United States
| language = English
| budget = 
| gross = 
}}
 3D Computer-animated science fiction same name original Insomniac Games writer T.J. Fixman as well as featuring the same voice cast to reprise their respective roles and utilizing assets from the video games in the film.    

==Plot== Ratchet & Ratchet and Clank met Chairman Drek later series Galactic Rangers.

==Voice cast==
The film will feature returning voice actors from the Ratchet & Clank|series, reprising their original roles from the video games.
*James Arnold Taylor as Ratchet (Ratchet & Clank)|Ratchet, Clanks Lombax friend.
*David Kaye as Clank (Ratchet & Clank)|Clank, Ratchets robot friend. Jim Ward as Captain Qwark, a galactic superhero and celebrity.
*Kevin Michael Richardson as Chairman Drek, the leader of the Blarg.
*Armin Shimerman as Doctor Nefarious, a robotic supervillian with a vendetta against Captain Qwark and organic life forms. 

==Production==
 
 official trailer released on YouTube,  for a 2015 theatrical release.  The film is being produced at Rainmaker Entertainments Vancouver studio in Canada, and will be executively produced by Michael Hefferon, President of Rainmaker Entertainment. The film will tap on Insomniac Games vast asset library from their Ratchet & Clank video games, while creating their own new unique locations and characters. 

==Release== 2014 Electronic Cannes Film Festival on May 15, 2015. 

==References==
 

==External links==
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 