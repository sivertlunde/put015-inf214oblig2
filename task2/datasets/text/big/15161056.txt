Killjoy (2000 film)
 
 
{{Infobox film

 | name = Killjoy
 | image = Killjoy VideoCover.jpeg
 | director = Craig Ross
 | producer = Mel Johnson, Jr.
 | writer = Carl Washington
 | starring = Ángel Vargas Vera Yell Lee Marks D Austin Jamal Grimes Corey Hampton Rani Goulant Napiera Groves Arthur Burghardt and William L. Johnson Penny Ford Dionne Rochelle Carl Washington
 | music = Richard Kosinski
 | cinematography = Carl Bartels
 | editing = Craig Ross
 | studio = Big City Pictures Full Moon Pictures Full Moon Entertainment
 | released =  
 | runtime = 72 minutes
 | country = United States
 | language = English
 | budget = $150,000
}}
Killjoy is a 2000   was released in 2002. A second sequel, Killjoy 3 was released in 2010, and Killjoy Goes to Hell in 2012.

==Plot==
 
The film begins with a boy named Michael (Grimes), a kid who has the hots for a beautiful girl named Jada (Yell), but is always rejected because Jada is stuck with a gangster, Lorenzo (William L. Johnson|Johnson), and when Michael comes close to her, he is threatened with bodily harm by getting beat up by both Lorenzo and his homeboys, T-Bone (Hampton) and Baby Boy (Goulant). But Michael is secretly involved in black magic, and tries to bring a doll that he keeps calling Killjoy to life. But the spell does not work quickly; after he is abducted by the gangsters and brought into an abandoned area of the woods, Michael is accidentally shot and killed by Lorenzo when they were supposedly joking around with him.

A year later, Jada is now going out with a guy named Jamal (Lee Marks), who is just as nice as Michael was. Meanwhile, Lorenzo is now going out with a girl named Kahara (Groves), who is just like Jada. When Lorenzo leaves to have sex with her, T-Bone and Baby Boy go to get some ice-cream to get munchies, when the ice-cream man, whos dressed up like a clown (Vargas) really sells drugs. Baby Boy and T-Bone hop in the truck, when suddenly, they end up in some abandoned building, far away from where they live. They split up to find a way out. Baby Boy finds a way out, but before he can make a run for it, he gets rammed into the wall by the ice-cream truck, killing him.
 The Mask) and shoots the bullets out of his mouth, killing Lorenzo.

Meanwhile, one of Jadas friends, Monique (Austin), calls Jada, telling her that some guy (Arthur Burghardt|Burghardt) came in to her room and needs them to come over. The homeless man tells them that Lorenzo killed Michael a year ago, and explains that Michael was brought back to life by the Ice Cream Man, whose real names Killjoy, the doll Michael has, and tells them that Killjoy just killed Lorenzo, Baby Boy, and T-Bone. Then he tells them that Killjoy/Michael can be killed by Jada, because the love of a young woman can destroy the evil in the heart. Before leaving, he tells them that they have to kill the doll the spirit of the ice cream man came from, and the ice cream truck is outside.

Still not believing what the homeless man said, they go inside the ice-cream truck to check it out, and they end up in the abandoned building, where theyre confronted by T-Bone, Baby Boy, and Lorenzo, who souls are under Killjoys control and are now his accomplices. The trio start fighting the accomplices until all three are killed. Then Killjoy comes out and knocks Jamal and Monique out. He asks Jada for a kiss, upon which Jada agrees to do so, under one condition: that he will leave her world and never come back. But instead of disappearing, Killjoy transforms into Michael. Michael tells Jada that he did it all for her. Jada, who desperately wants to stop Michael/Killjoy from killing, stabs Michael to death, and Michael fades away.

Jamal, Jada, and Monique are about to leave when Killjoy, Lorenzo, T-Bone, and Baby Boy appear behind them. Jada then remembers from the Homeless man that they have to kill the doll. The trio frantically run back into the ice-cream truck, where they end up in Michaels house, where the doll lies on the floor. The doll turns into Michael, who constantly begs her for forgiveness, but Jada continues to stab it. The earth starts to shake, and while Jada, Jamal, and Monique try to stay still within the circle of candles without breaking it, Baby Boy, T-Bone, and Lorenzos souls are sucked into a portal. The trio then watch as Killjoy finishes off Michael (via vaporization), and the trio are sent back to Jadas room, where the homeless man thanks them, and vanishes, meaning hes an angel from heaven.

The trio decides to go out and eat, when they are confronted by Ray Jackson (Carl Washington|Washington), and Tamara (Rochelle), who are both in Jadas English class. Ray says that he gained access into the club free because his brother owns the place, and his name is......Killjoy! The trio then see that Ray turns into Killjoy, and Tamara turns into Lorenzo. Killjoy starts laughing maniacally at the trio, with Jada screaming "NO!", until Jada wakes up in bed, along with Jamal, indicating that the entire experience was all a dream. Jamal tries to calm Jada down by going under the covers to perform oral sex on her, but then rises from the covers as Killjoy. The film ends with a shot of Jada screaming and Killjoy stating, "That was some good pussay!!!", followed by evil laughter.

==Cast==
*Ángel Vargas as Killjoy
*Vera Yell as Jada
*Lee Marks as Jamal
*D Austin as Monique
*Jamal Grimes as Michael
*Corey Hampton as T-Bone
*Rani Goulant as Baby Boy
*Napiera Groves as Kahara
*Arthur Burghardt as Homeless Man
*William L. Johnson as Lorenzo
*Penny Ford as Singer
*Carl Washington as Ray Jackson
*Dionne Rochelle as Tamara
==Release==
 

==Reception==
 
==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 