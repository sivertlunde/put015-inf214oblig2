Their Mad Moment
{{Infobox film
| name           = Their Mad Moment
| director       = Chandler Sprague
| producer       = Al Rockett
| writer         = Eleanor Mercein Kelly (novel) Leon Gordon
| starring       = Dorothy Mackaill
| cinematography = Dan Clark Arthur L. Todd
| editing        = Alexander Troffey Fox Film Corporation
| released       =  
| runtime        = 55 minutes
| country        = United States
| language       = English
| budget         = 
}}

Their Mad Moment is a 1931 American film directed by Chandler Sprague and starring  Dorothy Mackaill. It is based on the 1927 book by Eleanor Mercein Kelly, Basquerie.      
==Cast==
* Dorothy Mackaill - Emily Stanley
* Warner Baxter - Esteban Cristera
* ZaSu Pitts - Miss Dibbs
* Nance ONeil - Grand Mere
* Lawrence Grant - Sir Harry Congers
* Leon Janney - Narcio
* John St. Polis - Hotel Manager
* Nella Walker - Suzanne Stanley
* Mary Doran - Stancia

==References==
 

==External links==
* 

 
 
 
 
 