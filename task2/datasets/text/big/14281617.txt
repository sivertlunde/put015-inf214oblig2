Beyond the Years
{{Infobox film name           = Beyond the Years image          = Beyond_the_Years.jpg caption        = Poster film name      = {{Film name hangul         =   hanja          =   rr             = Cheon-nyeon-hak mr             = Ch‘ŏn-nyŏn-hak}} director       = Im Kwon-taek producer       = Lee Hee-won Kim Jong-won writer         = Kim Mi-yeong based on       =   starring       = Cho Jae-hyun Oh Jeong-hae Oh Seung-eun music          = Kunihiko Ryo cinematography = Jeong Il-seong editing        = Park Soon-deok studio         = Kino2 distributor    = Prime Entertainment released       =   runtime        = 106 minutes country        = South Korea language       = Korean budget         =  gross          =   
}}
Beyond the Years ( ) is a 2007 South Korean drama film. Celebrated director Im Kwon-taeks 100th film, it is based on the short fiction "The Wanderer of Seonhak-dong" by Lee Cheong-jun, and was presented at the 2007 Toronto International Film Festival.  Despite being an informal sequel to Ims phenomenally successful Sopyonje (1993 in film|1993), Beyond the Years was not popular with Korean audiences. 

==Synopsis==
Dong-ho and Song-hwa are separately adopted by Yu-bong (Im Jin-ta), a nomadic singer, and grow up as siblings. Dong-ho falls in love with Song-hwa, but he suffers from the fact that he has to call her sister and constantly fight with Yu-bong’s obsession to make her a great singer. Eventually, Dong-ho leaves home. However, with his unchanging affection for Song-hwa, he keeps following traces of his love while refining his drumming skills in order to match well with her singing. This is the heart-touching love story of Song-hwa, who devotes her life and love to her talent for Pansori (a traditional Korean form of narrative song), and Dong-ho, who has devoted his life to loving her.

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 

 