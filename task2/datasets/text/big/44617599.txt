Herogiri
 
{{Infobox film
| name           = Herogiri
| image          = Herogirifilm.png
| alt            =
| caption        = Theatrical release poster
| film name      = 
| director       = Rabi Kinagi
| producer       = Nispal Singh
| writer         = N.K. Salil
| screenplay     = N.K. Salil
| story          = N.K. Salil
| based on       = Balupu Dev Koel Mallick Sayantika Banerjee Mithun Chakraborty
| narrator       = 
| music          = Jeet Gannguli
| cinematography = Kumud Verma
| editing        = Md. Kalam
| studio         = Surinder Films
| distributor    = 
| released       =  
| runtime        = 151 minutes
| country        = India
| language       = Bengali
| budget         =
| gross         = 
}} Bengali romance romantic comedy comedy and action film directed by Rabi Kinagi and produced by Nispal Singh under the banner of Surinder Films. It features actors Dev (actor)|Dev, Koel Mallick,Sayantika Banerjee and Mithun Chakrabarty in lead roles. It is a remake of 2013 Telugu film Balupu.  

== Plot ==
Shuvo (Dev (actor)|Dev) works as a collection agent of ICICI in Kolkata and leads a happy life along with his friends and his father Dibakar Burman (Mithun Chakraborty) who wishes to see Shuvo married to a girl soon. Once Shuvo rushes to a Hospital where his friend is admitted as he tried to commit suicide by drinking nail polish solution and asks the reason for it. Then he describes about the traits of Maria (Koel Mallick) and her uncle (Kharaj Mukherjee). The duo have a habit of cheating gullible young men including his friend and Shuvo decides to teach them a lesson.

Shuvo enters their life as a gullible youngster and starts torturing them. Marias uncles plans and attempts always go in vain before him. As a part of their cunning plans, Marias uncle advises Maria to ask Shuvo to marry her. As fate would have it, Shuvo also comes with a marriage proposal to Maria. Then the pair approach Dibakar and tell him that Shuvo is in love with Maria and that he proposed to her. They assume that this would enrage him but the result is negative as Dibakar accepts the proposal heartily. However, Maria was already engaged to Rohit (Surajit Sen) by her father sometime in the past. After some one days saves Shruti from some goons and after doing so advises her not to play with peoples feelings in future. Maria finds herself falling in love with Shuvo and informs Shuvos father, Dibakar Burman.

Dibakar Burman leaves the final decision to Marias father who in turn guarantees that they would not face any problem in the future from Shuvo. On the other hand Rohits mother warns Marias father that she would torture her after her marriage. Listening to both, Marias father fixes Marias marriage with Shuvo. Rohits mother gets humiliated by this and calls her brother Bhavani Pathak (Bharat Kaul). Bhavani who arrives at the wedding venue with his hench men recognise that Shuvo-Dibakar are his biggest enemies Raj-Kaka. While Shuvo is fighting with the goons, Bhavani stabs Dibakar and flees away to Tollygunge with Maria. After admitting Dibakar into the hospital, Shuvo starts narrating his past.

In the past in Tollygunge when Bhavani tries to destroy the empire of Kaka, a dreaded don those days, he takes the help of Raj and makes him his partner in crime. Raj, with his aggressiveness and cleverness, starts destroying the empire of Kaka. Meanwhile, without knowing the fact that she was Kakas daughter, he falls in love with Dr. Nandini (Sayantika Banerjee) who reciprocates his love. Knowing this, Bhavanis elder son Shakti tries to injure Anjali by throwing acid on her, only to be killed by Kaka. Then she comes to know that Raj is a criminal and her fathers enemy. She pleads both Raj and her father to leave rowdyism, in which Raj turns successful. After his younger brother Rudra (Sumit Ganguly) advice, Bhavani tells to Kaka that Raj and Nandini are eloping. While Raj is involved in a feud with Kaka, Rudra shoots Nandini fatally after which Raj kills Rudra. Later Raj and Kaka fulfill her last wish by leaving rowdyism and living a normal life and later they came to leave in Kolkata.

Meanwhile Marias father rushes to the hospital and tells to Raj that Maria is being married to Rohit forcibly. Raj reaches the spot, acts as if though Rudras soul is in his body and it wants to kill Raj and Kaka. With the help of Maria, Nandinis father  and Dr. Konok Chapa and his friends, Ravi manages to kidnap Rohit. Bhavani along with Maria and Kaka along with Rohit reach the agreed spot. Raj reveals his real identity, defeats Bhavani and marries Maria in the end.

== Cast == Dev  as Raj/Shuvo
* Koel Mallick  as Maria
* Sayantika Banerjee  as Dr. Nandini
* Mithun Chakraborty  as Dibakar Burman/Nandinis father
* Kharaj Mukherjee as Marias uncle
* Bharat Kaul as Bhavani Pathak
* Surajit Sen as Rohit
* Supriyo Dutta as Officer in charge (police) Gurupodo Ghosh
* Kamalika Banerjee as Marias mother
* Sumit Ganguly as Rudra, Bhavanis brother
* Rajat Ganguly as Rohits father

== Production == Dev and Dev and I Love Dev and Sayantika Banerjee were spotted while shooting some action sequences at a shopping mall in Kolkata.    Filming of a song-and-dance sequence also took place at Krabi, Thailand.   

== Music ==
{{Infobox album  
| Name       = Herogiri
| Type       = soundtrack
| Artist     = Jeet Gannguli
| Cover      = 
| Alt        = 
| Released   =  
| Recorded   =  Feature film soundtrack
| Length     =  
| Label      = Shree Venkatesh Films
| Producer   = {{flatlist|
* Mahendra Soni
* Shrikant Mohta
}}
| Last album = Alone (2015 film)|Alone (2015)
| This album = Herogiri (2015)
| Next album = 
| Misc           =
{{Singles
| Name           = Herogiri
| Type           = soundtrack
| Single 1       = Maria
| Single 1 date  =  
| Single 2       = Janeman
| Single 2 date  =  
| Single 3       = Panga
| Single 3 date  =  
}}
}}
The soundtrack of the film has been given by Jeet Gannguli, while lyrics have been penned by Raja Chanda. The album includes four original tracks and marks the Tollywood debut of singer Deepali Sathe. Keralas Bald head singer Benny Dayal

{{Track listing
| collapsed       = 
| headline        = Herogiri (Original Motion Picture Soundtrack)
| extra_column    = Artist(s)
| total_length    =  
| all_lyrics      = Raja Chanda
| all_music       = Jeet Gannguli
| title1          = Maria
| extra1          = Shalmali Kholgade, Benny Dayal
| length1         = 3:44
| title2          = Panga
| extra2          = Shreya Ghoshal, Mika Singh
| length2         = 3:14
| title3          = Janemon
| extra3          = Deepali Sathe, Benny Dayal
| length3         = 3:36
| title4          = Ke Tui Bol
| extra4          = Arijit Singh
| length4         = 4:37  
}}

== Critical reception ==
The Times of India mentioned that the film wont stay with us once we leave the theater and it would have been a good movie if it had a touch of reality, but appreciated Mithun Chakraborty (remarking, "He overshadows everyone with his screen presence and his chemistry with Dev (actor)|Dev, half his age is something that makes the film watchable.") and gave 2 and a half stars. 

== References ==
 

==External links==
*  

 
 
 
 
 