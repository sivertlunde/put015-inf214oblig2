Fighter (2011 film)
{{multiple issues|
 
 
}}
 
 

{{Infobox film
 | name           = Fighter
 | image          = Fighter jeet.jpg
 | alt            = 
 | caption        = Marbo Noy Morbo
 | director       = Ravi Kinagi
 | producer       = IOU FLIMS(JEET)
 | writer         = Eskay Jeet Srabanti Srabanti Ferdous Ahmed Locket Chatterjee Biswajit Chakraborty Biplab Chatterjee Ashish Vidyarthi
 | music          = Indradeep Dasgupta
 | cinematography = P.Sylve Kumar
 | editing        = Rabi Ranjan Maitra
 | studio         = 
 | distributor    =  Eskay Movies
 | released       = 7 January 2011
 | runtime        = 160 minutes
 | country        = India Bengali
}} Bengali film Jeet and Telugu movie Lakshyam (2007 film)|Lakshyam.

==Plot==
The film starts with a flashback. ACP Bose (Ferdous) is a sincere police officer happily married and lives with his parents and younger brother Surya Jeet (actor)|(Jeet). Surjo is a college student and he falls in love with his classmate Indu Srabanti Chatterjee|(Srabanti). Enter Section Shankar, Bharat Kaul, the villain who is notorious for settlements and land deals. Indu comes across ACP Bose when she is on a field trip from her college with her friends. She gets friendly with his daughter Pinky who tells her that she would get her introduced to her uncle Surya and they would make a good pair. It turns out that Surya studies in Indus college and soon after they accidentally even come to know each other and get close.

ACP Bose investigates the case of a dreaded criminal Section Shankar whom no one has been able to touch. He is even involved with a land deal involving crores of money which is completely illegal and others including the D.I.G is involved. When the chairman of the bank who has lent money for the deal demand the money back as the deal hasn’t worked, but Section Shankar eliminates him. The people who are customers of the bank take to the streets and try to damage the bank.ACP Bose arrives there, arrests the manager and takes him away. Somehow Section Shankar comes to know where the manager has been taken .He arrives there with his men and the D.I.G and nearly kill ACP Bose. Section Shankar asks his men to dump the body. On the way, somehow Surya gets involved and ultimately rescues his brother from a burning bus and the elder brother dies in the younger brothers arms. The entire media and the people think that ACP Bose has swindled off all the money as they have been made to believe that by the D.I.G.Surya decides to take revenge on the people who killed his brother. Firstly he kills Nikhil (his friend and Section Shankars friend) .Then he kills the D.I.G (he had been taken to jail and he had escaped).Nikhil had also kidnapped Indu as he wants her at any cost, but Surya rescues her. Section Shankar also kidnaps the entire family of Surya.Surya goes to Section Shankar houses where he kills Shankar and rescues his family under the supervision of the new D.I.G who supports him wholeheartedly.

==Cast== Jeet as Surja Srabanti as Indu
*Locket Chatterjee as A.C.P Boses Wife
*Ferdous Ahmed as A.C.P Bose
*Biplab Chatterjee as D.I.G
*Ashish Vidyarthi as D.I.G(Cameo appearance)
*Biswajit Chakraborty as Surjas Father
*Joy Badlani as Indus Father
*Bharat Kaul as Section Shankar
*Sumit Ganguly
*Mousumi Saha as Indus Mother
*Biswanath Basu as Ice-Cream Boy
*Gargi Banerjee as Newsreader
* Arindam Saha as Train Passenger

 

 
 
 
 