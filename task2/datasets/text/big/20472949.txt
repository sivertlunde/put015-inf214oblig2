Fate (2008 film)
{{Infobox film
| name           = Fate
| image          = Fate film poster.jpg
| caption        = Theatrical poster
| film name      = {{Film name
 | hangul         =  
 | hanja          =  
 | rr             = Sukmyeong
 | mr             = Sukmyŏng}}
| director       = Kim Hae-gon
| producer       = Kang Min
| writer         = Kim Hae-gon
| starring       = Song Seung-heon Kwon Sang-woo
| music          = Choe Tae-wan Im Hyeong-sun
| cinematography = Choi Ji-yeol
| editing        = Choi Jae-geun Eom Jin-hwa
| distributor    = CJ Entertainment
| released       =  
| runtime        = 123 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          =  
}} 2008 South action film noir film.  

== Plot ==
Gang members Woo-min, Cheol-jung, Do-wan and Yeong-hwan are close friends, who, with the help of older gang member Gang-seop, decide to rob a casino so they can start their lives over. But Cheol-jung betrays the others, and Woo-min ends up in prison. After serving his time, Woo-min tries to stay out of trouble, but finds himself drawn back into the underworld.

== Cast ==
* Song Seung-heon ... Kim Woo-min   
* Kwon Sang-woo ... Jo Cheol-jung
* Kim In-kwon ... Jeong Do-wan 
* Park Han-byul ... Jeong Eun-yeong
* Ji Sung ... Park Yeong-hwan
* Hong Soo-hyun ... Jo Hyo-sook Lee Seung-joon ... Hyo-sooks husband
* Wi Seung-cheol
* Min Eung-sik ... Jeong Doo-man
* Ahn Nae-sang ... Cha Gang-seop
* Jung Woo ... Choi Jeong-hak

==Reception==
Before filming was complete, the Japanese distribution rights to Fate were presold to Formula Entertainment for  , a relatively high sum due to Kwon Sang-woos Korean Wave fanbase.  

The film was not a big success, selling only 858,215 tickets nationwide. 

== References ==
 

== External links ==
*    
*  
*  
*  

 
 
 
 


 
 