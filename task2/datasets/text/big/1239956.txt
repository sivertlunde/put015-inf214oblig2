I'm All Right Jack
{{Infobox film
| name           = Im All Right Jack
| image          = Im All Right Jack UK poster.jpg
| image size     = 
| border         = yes
| caption        = Original British film poster John Boulting
| producer       = Roy Boulting
| based on       =   Frank Harvey John Boulting
| starring       = Ian Carmichael Peter Sellers Richard Attenborough Margaret Rutherford Terry-Thomas
| music          = Ken Hare Ron Goodwin
| cinematography = Mutz Greenbaum
| editing        = Anthony Harvey
| studio         = Charter Film Productions
| distributor    = British Lion Films (UK)
| released       =  
| runtime        = 101 minutes
| country        = United Kingdom
| language       = English
| budget         =
}} Frank Harvey, Bafta Best Actor Award. The rest of the cast included many well-known British comedy actors of the time.

The film is a satire on British industrial life in the 1950s. The trade unions, workers and bosses are all seen to be incompetent or corrupt to varying degrees. The film is one of a number of satires made by the Boulting Brothers between 1956 and 1963.

==Plot==
After leaving the army and returning to university, newly graduated upper-class Stanley Windrush
(Ian Carmichael) is looking for a job but fails miserably at interviews for various entry level management positions. Stanleys uncle, Bertram Tracepurcel (Dennis Price) and his old army comrade, Sidney DeVere Cox (Richard Attenborough), persuade him to take an unskilled blue-collar job at Uncle Bertrams missile factory, despite Aunt Dollys (Margaret Rutherford) misgivings.

At first suspicious of the overeager newcomer, communist shop steward Fred Kite (Peter Sellers) takes Stanley under his wing and even offers to take him in as a lodger. When Kites curvaceous daughter Cynthia (Liz Fraser) drops by, Stanley readily accepts.

Meanwhile, personnel manager Major Hitchcock (Terry-Thomas) is assigned a time and motion study expert, Waters (John Le Mesurier), to measure how efficient the employees are. The workers refuse to cooperate but Waters tricks Windrush into showing him how much more quickly he can do his job with his forklift truck, than other more experienced employees. When Kite is informed of the results, he calls a strike to protect the rates his union workers are being paid.

This is what Cox and Tracepurcel want; Cox owns a company that can take over a large new contract with a Middle Eastern country at an inflated cost. He, Tracepurcel and Mr Mohammed (Marne Maitland), the countrys representative, would each pocket a third of the £100,000 difference.

Things dont quite work out for either side. Cox arrives at his factory to find that his workers are walking out in sympathy for Kite and his strikers. The press reports that Kite is punishing Windrush for working hard. When Windrush decides to cross the picket line and go back to work (and reveals his connection with the companys owner), Kite asks him to leave his house. This provokes the adoring Cynthia and her mother (Irene Handl) to go on strike. More strikes spring up, bringing the country to a standstill.
 nudist colony, only to have to flee from the female residents attentions.

==Cast==
 
*Ian Carmichael as Stanley Windrush
*Peter Sellers as Fred Kite
*Terry-Thomas as Major Hitchcock
*Richard Attenborough as Sydney DeVere Cox
*Dennis Price as Bertram Tracepurcel
*Margaret Rutherford as Aunt Dolly
*Irene Handl as Mrs Kite
*Liz Fraser as Cynthia Kite
*Miles Malleson as Windrush Snr
*Marne Maitland as Mr Mohammed
*John Le Mesurier as Waters
*Raymond Huntley as Magistrate
*Victor Maddern as Knowles
*Kenneth Griffith as Dai
*John Comer as Shop Steward
*Sam Kydd as Shop Steward
*Cardew Robinson as Shop Steward Ronnie Stevens as Hooper
*Martin Boddey as Num Yums Executive
*Brian Oulton as Appointments Board Examiner
*John Glyn-Jones as Detto Executive
*Terry Scott as Crawley
*Alun Owen as Film Producer
*Eynon Evans as Truscott David Lodge as Card Player Keith Smith as Card Player
*Clifford Keedy as Card Player
*Wally Patch as Worker
  
;Cast notes
*Malcolm Muggeridge and television announcer Muriel Young appear as themselves.

==Reception==
The film was a big hit, being the most popular film in Britain for the year ended 31 October 1959. FOUR BRITISH FILMS IN TOP 6: BOULTING COMEDY HEADS BOX OFFICE LIST
Our own Reporter. The Guardian (1959-2003)   11 Dec 1959: 4. 
==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 