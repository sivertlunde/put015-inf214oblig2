Love, Duty and Crime
{{Infobox film
| name           = El amor, el deber y el crimen
| image          = El amor, el deber y el crimen1926.jpg
| image size     = 200
| caption        =
| director       = {{Plainlist|
* Vincenzo di Doménico
* Pedro Moreno Garzón
}}
| producer       = {{Plainlist|
* Vincenzo di Doménico
* Pedro Moreno Garzón
}}
| writer         = {{Plainlist|
* Vincenzo di Doménico
* Pedro Moreno Garzón
}}
| narrator       = 
| starring       = {{Plainlist|
* Rafael Burgos
* Mara Meba
}}
| music          = María Claudia Galán (restored version)
| cinematography = Vincenzo di Doménico
| editing        = Vincenzo di Doménico
| production companies =
| distributor    = Sociedad Industrial Cinematográfica Latinoamericana
| released       =  
| runtime        = 27 minutes
| country        = Colombia
| language       = Silent film
| budget         = 
| gross          =
}} Colombian silent film directed by Vincenzo di Doménico and Pedro Moreno Garzón.

The film revolves around a young committed woman who falls for the painter who made his portrait, so she will be immersed in a conflict.

==Cast== Rafael Burgos
* Mara Meba
* Roberto Estrada Vergara
* Alejandro Barriga

==External links==
*  
*  
*   at Proimágenes Colombia

 
 
 
 
 

 
 