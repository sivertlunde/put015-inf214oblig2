Puccini for Beginners
{{Infobox film
| name = Puccini for Beginners
| image = Pucciniforbeginners.jpg
| director= Maria Maggenti
| producer = Gary Winick
| writer = Maria Maggenti
| starring = Elizabeth Reaser Gretchen Mol Justin Kirk
| music = Terry Dame
| cinematography = Mauricio Rubinstein
| editing = Susan Graef LOGO Films Red Envelope Entertainment
| distributor = Strand Releasing
| released =  
| runtime  = 82 minutes
| country = United States
| language = English
| budget = 
| gross =  $110,864 
}}
Puccini for Beginners is a 2006 American romantic comedy film written and directed by Maria Maggenti. The film debuted at the 2006 Sundance Film Festival. The film was released to DVD in the United States on July 3, 2007.

==Plot==
The story begins with Samantha (Julianne Nicholson) breaking up with Allegra (Elizabeth Reaser), a lesbian author who has had relationship problems in the past. Allegra meets a man named Philip (Justin Kirk) at a party, with whom she feels a connection. The next day, she meets Grace (Gretchen Mol), Philips ex-girlfriend, although Allegra does not know about it. Allegra and Philip begin seeing each other, and Philip leaves Grace for good. Allegra sees Grace outside of a movie theater and Grace cries about her boyfriend leaving her. Allegra goes on a date with Philip, but she leaves after thoughts in her mind tell her its wrong to be with a guy.

Allegra goes back and forth on dates with Philip and Grace. After several more dates, Grace shows Allegra a picture of her ex-boyfriend, and she learns that Philip and Grace were together. Philip and Grace go out for dinner, where they reveal to each other that theyre seeing someone else. Meanwhile, Allegra caters at a party, which turns out to be Samanthas engagement party. Philip and Grace show up at the party, and they both discover that they have been seeing the same woman. In the end, Allegra is back with Samantha and never sees Philip and Grace again.

==Cast==
* Elizabeth Reaser as Allegra
* Gretchen Mol as Grace
* Justin Kirk as Philip
* Julianne Nicholson as Samantha
* Tina Benko as Nell
* Jennifer Dundas as Molly
* Kate Simses as Vivian
* Brian Letscher as Jeff
* Will Bozarth as Jimmy
* Ken Barnett as Scott

== References ==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 

 