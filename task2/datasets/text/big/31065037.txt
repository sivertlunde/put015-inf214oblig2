Massage for Relaxation
 

{{Infobox film
| name           = Massage For Relaxation
| image          = Massage for Relaxation.jpg
| image size     = 180 px
| alt            = 
| caption        = 1987 VHS cover art
| director       = Patricia Mooney
| producer       = Mark Schulze Patricia Mooney
| writer         = Patricia Mooney
| narrator       = Patricia Mooney
| starring       = Patricia Mooney Maya Campos
| music          = Richard Plasko
| cinematography = Mark Schulze
| editing        = 
| studio         = 
| distributor    = New & Unique Videos
| released       = 1985 
| runtime        = 45 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 1985 educational instructional video and was among the first on how to massage another person.  Produced by Mark Schulze and Patricia Mooney of New & Unique Videos, of San Diego, California.    

==Background== massage technician studio with Beta and PAL videotape formats.  

==Synopsis==
The instructional film shows Cleo (aka Patty Mooney) performing a massage on Maria (aka Maya Campos) using   

==Release== BFS Limited in Canada, Instructional Video, Inc.,  The Learning Annex,  and Publishers Clearing House.

In 2011, New & Unique Videos reissued Massage For Relaxation in DVD format, and made it available for Video on demand|on-demand downloading at video kiosks at Microsoft and Sony Style stores in Europe and the United States. 

==Recognition==

===Awards and nominations===
In 1985, the film won an International Television Association (ITVA) Video Medallion Award. 

===Reception=== Nexus Magazine noted the 1985 ITVA award and commented that the video had a "soothing score" making it "easy to relax while learning how to massage oneself or another."  He noted also that through closeup shots the demonstrations were clear and the proffered techniques of massage and salt rub were easy to learn. 

In September 1988, Billboard Magazine wrote that while "Everyone enjoys a good massage", "the difficult part is getting someone to give you one," noting "Briskly paced and modestly priced, this program just may make a dent in the sell-through market." 

Vegetarian Times wrote that after a hard workout, a massage is always welcome, and noted the instruction available through the film. 

Total Health Magazine found the video to be unique in that it "is both instructional for someone learning to be a masseur and instructor Cleo demonstrates how to give yourself a massage.  Techniques are plainly shown how to relieve tension in neck, shoulders, back, hips, chest, abdomen, legs, arms, feet, face and head". 

Health Foods Business wrote in 1988 that while the benefits of massage might be obtainable "from the hands of a trained and certified pro," decent massage can be done by amateurs who use some of the available "books and video cassettes which, while not a substitute for a massage therapy course, can help the nonprofessional acquire some skills for home use."  They noted that Massage For Relaxation, put together in 1985 by trained masseuse and frequently published author Patricia Mooney, is one such video and that it contained "a 15-minute section on self-massage." 

The Video Rating Guide for Libraries wrote that while the videos cover jacket was "attractive and provocative", it was an otherwise "highly professional, nonpornographic treatment of whole body massage," noting that the camera angles and detail work presented an "excellent focus on various techniques of massage", and that the benefits of whole body massage were featured with "emphasis placed on relieving pain and enhancing digestion."  They made special note of the video including detailed explanation of self-massage techniques, and that the instructors demonstrations of a wide variety of techniques would be "especially useful for novices". 

==Patricia Mooney bibliography==
* "Body Healing: Two Artists On Their Philosophies," by Patricia Mooney, Resources for Changing Times, Spring 1985, p.5
 
* "John Ottombrino," by Patricia Mooney 
* "My Brief Career as a Healer," by Patty Mooney, 
* "Self Massage: How to Rub Pain and Stress Away," by Patricia Kathryn Mooney, 
* "Ten Ways to Beat Stress," by Patricia Mooney, 
* "The Soothing Art of Self-Massage," by Patricia Mooney  
* "The Unique Art of Massage," by Pat Mooney, 

==See also== Home video, history

== References ==
 

   

   

   

   

 Certificate of Copyright Registration, United States Copyright Office, Registration Number PA 257 263, June 17, 1985 

   

   

   

   

   

   

   

 "Self Massage: How to Rub Pain and Stress Away," by Patricia Kathryn Mooney, Beauty Digest, May 1984, p.10 

 "The Unique Art of Massage," by Pat Mooney, Bestways, November 1983, p.40 

 "Massage Giving America A Helping Hand," by Paul Bubny, Health Foods Business, September 1988, p.53 
 San Diego Daily Transcript, December 10, 1992 

 Wood Knapps Special Interest Video Catalog, Fall 1992 

 Wood Knapps Special Interest Video Catalog, Holiday 1992, p.27 

 Videos & Reviews Reference & Special Interest Video Catalog 1993–94, p.190 

 The Learning Annex, Summer 1989 

   

   

 

== External links ==
*   at Allmovie

 
 
 
 
 