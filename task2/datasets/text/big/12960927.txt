Good Copy Bad Copy
{{Infobox film
| name           = Good Copy Bad Copy
| image          = GDBC.png
| caption        = A documentary about the current state of copyright and culture.
| director       = Andreas Johnsen  Ralf Christensen  Henrik Moltke
| producer       = Rosforth
| writer         =
| starring       = Girl Talk Danger Mouse  Mikkel Meyer  Gnarls Barkley  De La Soul  NWA
| cinematography = Andreas Johnsen  Ralf Christensen  Henrik Moltke
| editing        = Adam Neilsen
| distributor    =
| released       = 2007
| runtime        = 59 minutes
| language       =
| budget         =
| preceded_by    =
| followed_by    =
| mpaa_rating    =
| tv_rating      =
| website        =
}}
Good Copy Bad Copy, A documentary about the current state of copyright and culture, is a 2007 documentary about copyright and culture in the context of Internet, peer-to-peer file sharing and other technological advances, directed by Andreas Johnsen, Ralf Christensen, and Henrik Moltke.

It features interviews with many people with various perspectives on copyright, including copyright lawyers, producers, artists and filesharing service providers.

A central point of the documentary is the thesis that "creativity itself is on the line" and that a balance needs to be struck, or that there is a conflict, between protecting the right of those who own intellectual property and the rights of future generations to create.

==Content== Girl Talk Danger Mouse, mashup scene who cut and remix sounds from other songs into their own. The interviews with these artists reveal an emerging understanding of digital works and the obstacle to their authoring copyright presents.
 mashup music and video culture. The documentary opens with explaining the current legal situation concerning Sampling (music)|sampling, licensing and copyright.
 sampling of music, as well as the distribution of copyrighted material via peer-to-peer file sharing search engines such as The Pirate Bay. MPAA (Motion Picture Association of America) CEO Dan Glickman is interviewed in connection with a raid by the Swedish police against The Pirate Bay in May 2006. Glickman concedes that piracy will never be stopped, but states that they will try to make it as difficult and tedious as possible. Gottfrid Svartholm and Fredrik Neij from The Pirate Bay are also interviewed, with Neij stating that The Pirate Bay is illegal according to US law, but not Swedish law.

The interviews document attitudes towards art, culture and copyright in a number of countries, including the United States, Sweden, Russia, Nigeria, and Brazil.

The situation in Nigeria and Brazil is documented in terms of innovative business models that have developed in response to new technological possibilities and changing markets.

In Nigeria the documentary interviews individuals working within the Nigerian film industry, or Nollywood. Charles Igwe, a film producer in Lagos, is interviewed at length about his views on the Nigerian film industry, the nature of Nigerian films, and copyright in the context of digital video technology. Mayo Ayilaran, from the Copyright Society of Nigeria, explains the Nigerian governments approach to copyright enforcement.
 sampling is FGV Brazil. Lemos explains that CDs or recorded music is treated merely as an advertisement for parties and concerts that generate revenue.

Good Copy Bad Copy also includes interview segments with copyright activist and academic Lawrence Lessig.  

==Credits== Girl Talk, Producer
*Dr Lawrence Ferrara, Director of Music Department NYU
*Paul V. Licalsi, Attorney Sonnenschein
*Jane Peterer, Bridgeport Music
*Dr. Siva Vaidhyanathan, NYU Danger Mouse, Producer
*Dan Glickman, CEO MPAA
*Gottfrid Svartholm, The Pirate Bay
*Fredrik Neij, The Pirate Bay Pirate Party
*Lawrence Lessig, Creative Commons
*Ronaldo Lemos, Professor of Law Fundação Getulio Vargas (FGV) Brazil
*Charles Igwe, Film Producer Lagos Nigeria
*Mayo Ayilaran, Copyright Society of Nigeria
*Olivier Chastan, VP Records John Kennedy, Chairman International Federation of the Phonographic Industry (IFPI)
*Shira Perlmutter, Head of Global Legal Policy International Federation of the Phonographic Industry (IFPI)
*Peter Jenner, Sincere Management
*John Buckman, Magnatune Records
*Beto Metralha, Producer Belém do Pará Brazil
*DJ Dinho, Tupinambá Belém do Pará Brazil

==Distribution== the Danish BitTorrent download. The filmmakers hope that releasing Good Copy Bad Copy for free will raise awareness and lead to other local broadcasting networks to show the documentary. 

The documentary first appeared on The Pirate Bay and then it was officially released under a Creative Commons Attribution-NonCommercial license on the Blip.tv video sharing site. 

On the 8th of May, 2008, Good Copy Bad Copy was shown on SVT2, Swedish Television. 

On the 8th of September, 2009, Good Copy Bad Copy was shown on YLE FST5, Finnish Television.

On the 5th of January 2010, Good Copy Bad Copy was shown on Special Broadcasting Service|SBS, Australian Television.

The documentary and an unofficial trailer are available on YouTube,   and the full film is on Google Video.  

==See also==
* Mashup (music)
* Remix culture
*  

==References==
 

==External links==
*   
* 
*  at blip.tv
*  at blip.tv
*  
*  at Vimeo, with English subtitles

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 