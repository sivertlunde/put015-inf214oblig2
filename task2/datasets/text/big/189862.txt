Intolerance (film)
{{Infobox film
| name = Intolerance
| image = Intolerance (film).jpg
| caption = Theatrical poster
| director = D. W. Griffith
| producer = D. W. Griffith
| writer = D. W. Griffith Hettie Grey Baker Tod Browning Anita Loos Mary H. OConnor Frank E. Woods Ralph Lewis Frank Bennett Elmer Clifton Miriam Cooper Alfred Paget
| music = Joseph Carl Breil Julián Carrillo Carl Davis (for 1989 restoration)
| cinematography = Billy Bitzer
| editing = D. W. Griffith James Smith Rose Smith Triangle Distributing Corporation
| released = September 5, 1916
| runtime = 210 minutes (original version) 197 minutes (most modern cuts)
| language = Silent film English intertitles
| country = U.S.A
| budget = $385,907 
}}
Intolerance is a 1916  s mission and death, (3) a French story: the events surrounding the   to Persia in 539 BC. Each story had its own distinctive color tint in the original print.  The scenes are linked by shots of a figure representing Eternal Motherhood, rocking a cradle. 

Intolerance was made partly in response to criticism of Griffiths previous film, The Birth of a Nation (1915),  which was criticized by the NAACP and other groups as perpetuating racial stereotypes and glorifying the Ku Klux Klan. 

==Storylines==
 
This complex film consists of four distinct, but parallel, stories—intercut with increasing frequency as the film builds to a climax—that demonstrate mankinds persistent intolerance throughout the ages. The film sets up moral and psychological connections among the different stories. The timeline covers approximately 2,500 years.

# The ancient "Babylonian" story (539 BC) depicts the conflict between Prince Belshazzar of Babylon and Cyrus the Great of Persia. The fall of Babylon is a result of intolerance arising from a conflict between devotees of two rival Babylonian gods—Bel-Marduk and Ishtar. Woman Taken in Adultery—intolerance led to the Crucifixion of Jesus. This sequence is the shortest of the four.
# The Renaissance "French" story (1572) tells of the religious intolerance that led to the St. Bartholomews Day Massacre of Protestant Huguenots by Catholic royals.
# The American "Modern" story (c. 1914) demonstrates how crime, moral Hardline|puritanism, and conflicts between ruthless capitalists and striking workers help ruin the lives of marginal Americans. To get more money for his spinster sisters charities, a mill owner orders a 10% pay cut to his workers wages. An ensuing workers strike is crushed and The Boy and The Dear One make their way to another city; she lives in poverty and he turns to crime. After they marry he tries to break free of crime but is framed for theft by his ex boss. While he is in prison, his wife must endure their child being taken away by the same "moral uplift society" that instigated the strike. Upon his release from prison, he discovers his ex-boss attempting to rape his wife. A struggle begins and in the confusion the girlfriend of the boss shoots and kills the boss. She escapes and The Boy is convicted and sentenced to the gallows. A kindly policeman helps The Dear One find the real killer and together they try to reach the Governor in time so her reformed husband will not be hanged.

Breaks between the differing time periods are marked by the symbolic image of a mother rocking a cradle, representing the passing of generations. One of the unusual characteristics of the film is that many of the characters do not have names. Griffith wished them to be emblematic of human types. Thus, the central female character in the modern story is called The Dear One. Her young husband is called The Boy, and the leader of the local Mafia is called The Musketeer of the Slums. Critics and film theorists maintain that these names reveal Griffiths sentimentalism, which was already hinted at in The Birth of a Nation, with names such as The Little Colonel.

 
File:Intolerance (1916).ogv|Intolerance Lillian Gish as "Eternal Motherhood"
 

==Cast==
*Lillian Gish as The Eternal Motherhood
The American "Modern" story
  fights against the Uplifters]]
*Mae Marsh as The Dear One
*Robert Harron as The Boy, a worker at Jenkins Mill Fred Turner as The Dear Ones father, a worker at the Jenkins Mill
*Miriam Cooper as The Friendless One, former neighbor of the Boy and Dear One Walter Long as Musketeer of the Slums Tom Wilson as The Kindly Officer/Heart
*Vera Lewis as Miss Mary T. Jenkins
*Sam De Grasse as Mr. Arthur Jenkins, mill boss
*Lloyd Ingraham as The Judge  Ralph Lewis as The Governor
*A. W. McClure as Prison Father Fathley
*Max Davidson as tenement neighbor of Dear One
Renaissance "French" story (1572)
 
*Margery Wilson as Brown Eyes
*Eugene Pallette as Prosper Latour
*Spottiswoode Aitken as Brown Eyes father
*Ruth Handforth as Brown Eyes mother
*Allan Sears as The Mercenary Soldier
*Josephine Crowell as Catherine de Medici, the Queen-mother
*Frank Bennett as Charles IX of France Prince Henry of France
*Joseph Henabery as Admiral Coligny Princess Marguerite of Valois (first role in film)
*W. E. Lawrence as Henry of Navarre
Ancient "Babylonian" story
 
*Constance Talmadge as The Mountain Girl (second role in film)
*Elmer Clifton as The Rhapsode, a warrior-singer
*Alfred Paget as Prince Belshazzar
*Seena Owen as The Princess Beloved, favorite of Belshazzar
*Tully Marshall as High Priest of Bel (mythology)|Bel-Marduk
*George Siegmann as Cyrus the Great
*Carl Stockdale as King Nabonidus, father of Belshazzar
*Elmo Lincoln as The Mighty Man of Valor, guard to Belshazzar
*Frank Brownlee as The Mountain Girls brother The Ruth St. Denis Dancers  as Dancing girls
The Biblical "Judean" story
  The Nazarene
*Lillian Langdon as Mary, the Mother
*Bessie Love as The Bride
*George Walsh as The Bridegroom
Cameo appearances/small roles
 
* Mary Alden
* Frank Borzage
* Tod Browning
* Frank Campeau
* Constance Collier
* Donald Crisp
* Carol Dempster
* Douglas Fairbanks (Drunken Soldier with monkey)
* Mildred Harris
* Dell Henderson
* Harold Lockwood
* Wilfred Lucas
* Francis McDonald
* Owen Moore
* Carmel Myers
* Wallace Reid
* Pauline Starke
* Erich von Stroheim
* Natalie Talmadge
* Ethel Grey Terry
* Herbert Beerbohm Tree
* King Vidor
 

==Production==
 
  in the central courtyard of Babylon from Intolerance.]] Karl Brown keeping script, and Miriam Cooper in profile, in a production still for Intolerance.]]

Intolerance was a colossal undertaking featuring monumental sets, lavish period costumes, and more than 3,000 extras. Griffith began shooting the film with the Modern Story (originally titled "The Mother and the Law"), whose planning predated the great commercial success of The Birth of a Nation, which had made $48 million, about $  million in  .    He then greatly expanded it to include the other three parallel stories under the theme of intolerance.
 flop at the box-office, the burden was so great that in 1918 the Triangle Film Corporation had to be put up for sale.

A detailed account of the films production is told in William M. Drews book D.W. Griffiths Intolerance: Its Genesis and Its Vision. 

==Reception==
Although Intolerance was a commercial failure upon its initial release, it has since received very positive reviews. Intolerance has been called "the only film  , the masterpieces of Michelangelo, etc., as a separate work of art. 

The film was shown out of competition at the 1982 Cannes Film Festival.   

In 1989, Intolerance was selected for preservation in the United States National Film Registry by the Library of Congress as being "culturally, historically, or aesthetically significant", going in during the first year of voting.

In 2007, AFIs 100 Years...100 Movies (10th Anniversary Edition) ranked Intolerance at number 49 of 100 films. The film currently holds a 96% approval rating on the aggregate site Rotten Tomatoes.
 David Thomson has written of the films "self-destructive frenzy": The cross-cutting, self-interrupting format is wearisome.... The sheer pretension is a roadblock, and one longs for the "Modern Story" to hold the screen....   is still very exciting in terms of its cross-cutting in the attempt to save the boy from the gallows. This episode is what Griffith did best: brilliant, modern suspense, geared up to rapidity – whenever Griffith let himself slow down he was yielding to bathos.... Anyone concerned with film history has to see Intolerance, and pass on.  

==Influence==
Intolerance and its unorthodox editing were enormously influential, particularly among European and Soviet filmmakers. Many of the numerous assistant directors Griffith employed in making the film—Erich von Stroheim, Tod Browning, Woody Van Dyke—went on to become important and noted Hollywood directors in subsequent years.  It was parodied by Buster Keaton in Three Ages (1923).   

A replica of an archway and elephant sculptures from the Babylon segment of the film serve as an important architectural element of the Hollywood and Highland shopping center in Hollywood, Los Angeles (built in 2001).

The set of Intolerance was a key location in L.A. Noire.

== Versions ==
Intolerance is now in the public domain. There are currently four major versions of the film in circulation.
 LaserDisc and DVD by Image Entertainment and is the most complete version currently available on home video, if not the longest. David Gill. Rohauer Collection, who worked in association with Thames on the restoration. It was given a further digital restoration by Cohen Media Group (which currently serves as keeper of the Rohauer library), and was reissued to select theatres, as well as on DVD and Blu-ray, in 2013. While not as complete as the Killiam Shows Version, this print contains footage not found on that particular print. Kino International, this version, taken from 35 millimeter material, is transferred at a slower frame rate than the Killiam Shows and Rohauer prints, resulting in a longer running time of 197 minutes. It contains a synthetic orchestral score by Joseph Turrin. An alternative "happy ending" to the "Fall of Babylon" sequence, showing the Mountain Girl surviving and re-united with the Rhapsode, is included on the DVD as a supplement. This version is less complete than the Killiam Shows and Rohauer prints. Arte France of the version shown on 7 April 1917 at the Theatre Royal, Drury Lane in London. This version runs approximately 177 minutes and premiered 29 August 2007 at the Venice Film Festival and on 4 October on arte. 

There are other budget/public domain video and digital video disc versions of this film released by different companies, each with varying degrees of picture quality depending on the source that was used. Most are of poor picture quality, but even the restored 35 millimeter versions exhibit considerable film damage.

The Internet Movie Database lists the standard running time as 163 minutes, which is the running length of the DVD released by "Public Domain Flicks". The Delta DVD released in Region 1 as Intolerance: A Sun Play of the Ages and in Region 2 as Intolerance: Loves Struggle Throughout the Ages clocks in at 167 minutes. The version available for free viewing on the Internet Movie Archive is the Killiam restoration.
 Karl Brown remembered a scene with the various members of the Babylonian harem that featured full frontal nudity in film|nudity. He was barred from the set that day, apparently because he was so young. While there are several shots of slaves and harem girls throughout the film (which were shot by another director without Griffiths involvement), the scene that Brown describes is not in any surviving versions.   

It is also known that a major segment of the Renaissance "French" story, involving the attempted assassination of the Admiral Coligny, was cut before the films release. 

Film historian Kevin Brownlow has written that, when Griffith re-released "The Modern Story" separately as The Mother and the Law in 1919, he softened the actions of the National Guard in the film, due to the First Red Scare that year. "He was obliged to put this title in the strike sequence: The militiamen having used blank cartridges, the workmen now fear only the company guards." In fact, "machine guns could not operate with blank cartridges at this period," Brownlow noted. 

==See also==
 
* Eyelash extensions, invented for this film

== References ==
 

== External links ==
 
*  
*  
*  
*  
*   at Filmsite.org
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 