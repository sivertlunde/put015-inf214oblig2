Bleak Night
{{Infobox film
| name           = Bleak Night
| image          = BleakNight2010Poster.jpg
| caption        = South Korean poster
| film name = {{Film name
 | hangul         = 파수꾼
 | hanja          =  꾼
 | rr             = Pasuggun
 | mr             = Pasukkun}}
| director       = Yoon Sung-hyun
| producer       = Yoon Sung-hyun   Kim Seung-june   Park Ki-yong   Jang Hyun-soo
| writer         = Yoon Sung-hyun
| starring       = Lee Je-hoon Seo Jun-young Park Jung-min Jo Sung-ha
| music          = Park Min-joon
| cinematography = Byeon Bong-seon
| editing        = Yoon Sung-hyun
| studio         = KAFA Films
| distributor    = Filament Pictures CJ Entertainment
| released       =  
| runtime        = 116 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          =   
}}
Bleak Night ( ; lit. "The Lookout") is a 2010 South Korean coming-of-age drama film written and directed by Yoon Sung-hyun. The film is about a fathers search for answers following his sons death, and the shifting dynamics at play among three high-school friends.  A Korean Academy of Film Arts graduation project by Yoon Sung-hyun, it received rave reviews and won several Best New Actor awards for Lee Je-hoon, as well as Best New Director for Yoon at the Grand Bell Awards and Busan Film Critics Awards.  

== Plot ==
Still mystified by his sons death, the father (Jo Sung-ha) of high school student Ki-tae (Lee Je-hoon) tries to track down his two best friends, classmates Hee-joon (Park Jung-min) and Dong-yoon (Seo Jun-young), to try to find an explanation. Through Ki-taes classmate Jae-ho, the father meets Hee-joon, who says he cannot help as he moved schools "weeks before what happened to Ki-tae." Afterwards, Hee-joon berates Jae-ho for giving his phone number to Ki-taes father but Jae-ho tells him that Ki-tae "went crazy" after he moved away. Hee-joon manages to trace Dong-yoon and urges him to contact Ki-taes father and provide some answers. In parallel, flashbacks to the time gradually reveal what really happened, starting with Ki-taes needling and bullying of Hee-joon and the latters response.   

== Cast ==
* Lee Je-hoon as Ki-tae
* Seo Jun-young as Dong-yoon
* Park Jung-min   as Baek Hee-joon ("Becky")
* Jo Sung-ha as Ki-taes father
* Bae Jae-ki as Jae-ho
* Lee Cho-hee as Se-jung, girlfriend of Dong-yoon

== Awards == 15th Busan International Film Festival 
*New Currents Award

2011 35th Hong Kong International Film Festival 
*FIPRESCI Prize

2011 48th Grand Bell Awards
*Best New Director: Yoon Sung-hyun
*Best New Actor: Lee Je-hoon
 32nd Blue Dragon Film Awards
*Best New Director: Yoon Sung-hyun
*Best New Actor: Lee Je-hoon

2011 19th Korean Culture and Entertainment Awards 
*Best New Actor (Film): Seo Jun-young

2012 3rd KOFRA   Film Awards 
*Best New Actor: Lee Je-hoon

== References ==
 

== External links ==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 


 