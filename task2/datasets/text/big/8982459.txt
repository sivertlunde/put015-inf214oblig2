Lawman (film)
{{Infobox film
| name           = Lawman
| image          = Lawman.jpg Frank McCarthy
| director       = Michael Winner
| producer       = Michael Winner
| writer         =  Gerry Wilson
| starring       = {{plainlist|
*Burt Lancaster
*Robert Ryan
*Lee J. Cobb
*Robert Duvall
}}
| music          = Jerry Fielding
| cinematography = Robert Paynter
| editing        = Frederick Wilson
| distributor    = United Artists
| released       =  
| runtime        = 99 mins
| country        = United States
| language       = English
}} Western film starring Burt Lancaster, Robert Ryan, Lee J. Cobb, and Robert Duvall.
 Gerry Wilson and directed by Michael Winner.

Its hero and the motives of the other characters are not as defined or clear-cut as in some Westerns.   Quote: While Winner’s screenplay is certainly not the model of originality, the old west clichés are presented with a fresh slant, and Lawman proves to be an engrossing film despite the familiar trappings. and Maddox comes across an old flame tangled up with the wrong guy, a nemesis from the past who may or may not be on the right side of the law and a young cowboy eager to make a name for himself. also Lee J. Cobb plays the "evil cattle baron" role with a surprising degree of sense and humanity. and But thanks to some thought-provoking ideas on legal murder and honour amongst thieves, this is better than a straight shoot em up.  Cobbs character, Vincent Bronson, is not a typically evil cattle baron but is portrayed with a sense of humanity.  The marshal and the guilty men nevertheless come to a series of deadly confrontations. Maddox can be seen as an anti-hero dedicated to upholding the law regardless of any extraneous code of honor, or any personal happiness. The plot generates questions regarding honor and under what circumstances murder becomes legal. 

== Synopsis ==
The film starts with a scene common to many Westerns, cowboys in a drunken state shooting up a town and wreaking havoc.    The rowdies are from the town of Sabbath and are visiting the town of Bannock for a little recreation that gets out of hand.

The towns marshal, Jared Maddox, rides into Sabbath and is not alone. He brings along the body of Marc Corman, one of the unruly cowhands from the recent drunken spree in Bannock, carrying it on the back of a horse.  Corman and five others were involved in the accidental killing of an old man and Maddox has warrants for them. The remaining five are Vernon Adams, Choctaw Lee, Jack Dekker, Harvey Stenbaugh and Hurd Price, all hired hands at wealthy Vincent Bronsons ranch.  

Maddox follows protocol and calls on Sabbaths sheriff, Cotton Ryan. He demands that the five surrender to him within 24 hours.    Quote: Robert Ryan Sabbath marshal Cotton Ryan  Ryan is a lawman whose career had seen better days. He urges Maddox to avoid a confrontation with Bronson. Maddox wont back down, although he believes the suspects are likely to get light sentences due to the accidental nature of their crime and the fact that the justice system of Bannock can easily be influenced by bribes. Ryan goes to Bronsons ranch to inform him of Maddoxs demands. 

Bronson, unaware of the killing in Bannock up to this point, tries to negotiate by offering compensation to the victims family and even to Maddox. Ryan explains that Maddox will not agree to anything other than an unconditional surrender of all five men.  One of the suspects, Stenbaugh, who is Bronsons foreman, tries to persuade Bronson to have Maddox killed. Despite his violent past, Bronson is tired of death and violence and refuses Stenbaughs suggestion, insisting on further negotiations.  

Laura Shelby, a romantic interest from Maddoxs past, tries to negotiate on behalf of Price, one of the suspects, who is now her common-law husband.  Maddox is unmoved by Lauras pleas for mercy. Bronson gives up hope of reasoning with Maddox and asks his men if they wish to surrender.  Adams refuses, claiming that he would go bankrupt if in jail. Retired gunfighter Choctaw volunteers to join forces with Stenbaugh in killing Maddox.

Bronson offers to compensate his men for any financial losses while at the same time trying to persuade Maddox that some compromise must exist short of total surrender. Stenbaugh and young Crowe Wheelwright come to town. Despite being told by Bronson to avoid confrontation, Stenbaugh draws out Maddox for a showdown and is killed. Crowe (who is not wanted by the law) backs down from Maddox after a brief discussion.  

Back at the ranch, Bronson grieves upon hearing of his close friend Stenbaughs death. He is comforted by son Jason. Maddoxs breakfast is interrupted by local businessman Harris, leading a delegation of armed citizens concerned that the lawman is creating a lot of problems for them.  Not a man to be intimidated, Maddox stands up to the townspeople and they flee the hotel.

Maddox goes to find sheriff Ryan but is confronted again by Crowe. A shot is fired by a hidden gunman, Dekker. Ryan does place Dekker under arrest but advises Maddox to leave town as the violence seems to be spiraling out of control. Maddox reiterates his position that a lawman never compromises. 

Price tries to leave town. Crowe meets with Maddox to swear that he did not set him up for Dekkers ambush. Maddox reveals his disillusionment with his job and admits that lawmen are little more than professional killers.  Price, while fleeing, joins Adams on the ride to Bronsons ranch. On the way they spot Maddox. In the ensuing gunfight, Adams horse is shot while Price escapes. The marshal captures Adams and takes him to Lauras home, where they tend to his gunshot wound.

During a romantic interlude, Maddox rekindles old feelings for Laura. He asks her to leave with him once his mission is done. She agrees under the condition that he resigns as marshal. 

Maddox turns over Adams to sheriff Ryan and announces his intent to leave town and start a new life.  Bronson and his remaining men come looking for Maddox without realizing he is a changed man. They dont act immediately.  When businessman Harris, who was waiting on the sidelines, opens fire at Maddox, the others follow suit. Choctaw draws on Maddox but is killed.  Maddox insists that he seeks no further trouble. Bronsons son, Jason, is not satisfied and seeks revenge. He too, is killed by the marshal.  Price panics. As he runs toward Laura, Maddox shoots him in the back, despite his code of never drawing first on a man. Seeing his son dead, a grief-stricken Bronson kills himself in the street. Maddox can do nothing more but ride by himself out of town. 

==Cast==
*Burt Lancaster as Jared Maddox
*Robert Ryan as Cotton Ryan
*Lee J. Cobb as Vincent Bronson
*Robert Duvall as Vernon Adams
*Sheree North as Laura Shelby
*Albert Salmi as Harvey Stenbaugh
*Richard Jordan as Crowe Wheelwright
*John McGiver as Mayor Sam Bolden
*Ralph Waite as Jack Dekker John Beck as Jason Bronson William C. Watson as Choctaw Lee
*Walter Brooke as Luther Harris
*Robert Emhardt as Hersham
*J. D. Cannon as Hurd Price Hugh McDermott as L.G. Moss
*Joseph Wiseman as Lucas

==Release dates==
{|class="wikitable"
! Country !! Date
|- 11 March 1971 (London premiere)
|- Austria ||April 1971
|- Finland ||2 April 1971
|- West Germany 2 April 1971
|-
|Sweden|| 5 April 1971
|-
|Norway|| 10 June 1971
|- 21 July 1971
|- USA ||4 August 1971
|- 16 September 1971
|}

==Alternative titles==
{|class="wikitable"
! Country !! Title
|- A törvény nevében
|- O Homem da Lei
|- Lawman
|- Spain ||En nombre de la ley
|- Poland ||Szeryf
|- France ||LHomme de la loi
|- Io sono la legge
|-
|Sweden|| Lagens män
|- Finland ||Lainvalvoja
|- Yo soy la ley
|- Mato em Nome da Lei
|}

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 

 