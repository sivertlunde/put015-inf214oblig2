The Ranchero's Revenge
 
{{Infobox film
| name           = The Rancheros Revenge
| image          =
| caption        =
| director       = D. W. Griffith
| producer       =
| writer         =
| starring       = Lionel Barrymore
| cinematography = G. W. Bitzer
| editing        =
| distributor    =
| released       =  
| runtime        = 17 minutes
| country        = United States Silent English intertitles
| budget         =
}}

The Rancheros Revenge is a 1913 American film directed by  D. W. Griffith.

==Cast==
* Lionel Barrymore as The Ranchero Harry Carey as The Schemer
* Claire McDowell as The Schemers Associate
* Clarence Barr as The Chinese Cook (as Clarence L. Barr)
* Viola Barry as At Party
* William Courtright as At Wedding
* Charles Hill Mailes as At Party

==See also==
* Harry Carey filmography
* D. W. Griffith filmography
* Lionel Barrymore filmography

==External links==
* 

 
 
 
 
 
 
 
 
 
 