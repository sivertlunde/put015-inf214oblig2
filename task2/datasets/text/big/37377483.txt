Glass (1989 film)
{{Infobox film
| name           = Glass
| image          = 
| image size     =
| caption        = 
| director       = Chris Kennedy
| producer       = Patrick Fitzgerald Chris Kennedy
| writer         = Chris Kennedy
| based on = 
| narrator       =
| starring       = Lisa Peers Natalie McCurry
| music          = 
| cinematography = Pieter de Vries
| editing        = James Bradley
| studio = Oilrag Productions
| distributor    = 
| released       = 1989
| runtime        = 92 mins
| country        = Australia English
| budget         =
| gross = 
| preceded by    =
| followed by    =
}}
Glass is a 1989 Australian erotic thriller which was the feature debut of Chris Kennedy. David Stratton, The Avocado Plantation: Boom and Bust in the Australian Film Industry, Pan MacMillan, 1990 p270  

==Cast==
*Alan Lovell as Richard Vickery
*Lisa Peers as Julie Vickery
*Adam Stone as Peter Breen
*Natalie McCurry as Alison Baume
*Julie Herbert as Brenda Fairfax
*Bernard Clisby as Inspector Ambrosoll
*Richard Gilbert as Reg
*Marilyn Thomas as Alice

==Production==
Chris Kennedy made the movie shortly after leaving film school:
 I had an Irish friend who considered himself a bit of a film producer and I considered myself a film writer and director and producer, so I went to the bank and talked them into giving us the money to make Glass, on the understanding that we would be able to recoup the money... They didnt know any better and I didnt know any better. So we went ahead and made the film. Essentially Glass was made with a view to selling it effectively. It was made to be something that you could turn the sound off in Iceland and still get a pretty good idea of whats going on. Apart from other things, apart from being a terrific learning curve for me, it was really making movies on the job without ever having made a movie. No-one at the top end of the cast or crew had ever made a feature film before, and I was the blind leading the blind.   accessed 19 October 2012  

==Reception==
According to Kennedy the film sold very well overseas and recouped a fair amount of its budget. The director calls it "a bit of a raw and amateurish effort, but there are bits and pieces of it I quite like." 

==References==
 

==External links==
*  at IMDB

 
 
 


 
 