Battle Royale II: Requiem
 
 
{{Infobox film
| name           = Battle Royale II: Requiem
| image          = BR2 DVDCover TartanAsiaExtreme.jpg
| image_size     = 215px
| alt            = 
| caption        = DVD cover
| director       = Kenta Fukasaku Kinji Fukasaku
| producer       = Kimio Kataoka
| screenplay     = Kenta Fukasaku Norio Kida
| based on       =   Ai Maeda Shugo Oshinari
| music          = Masamichi Amano
| cinematography = Toshihiro Isomi
| editing        = Hirohide Abe
| studio         = Fukasaku Group TV Asahi WOWOW Tokyo FM Sega
| distributor    = Toei Company
| released       =  
| runtime        = 133 minutes     152 minutes    
| country        = Japan
| language       = Japanese
| budget         = USD$9 million
| gross          = USD$14,902,587 
}} thriller film. Battle Royale, novel of the same title by Koushun Takami. An extended version of the film is titled Battle Royale II: Revenge.

Director Kinji Fukasaku, who directed the first film, started work on the sequel but died of prostate cancer on January 12, 2003, after shooting only one scene with Takeshi Kitano. His son Kenta Fukasaku, who wrote the screenplay for both films, completed the film and dedicated it to his father.

  wrote the novelization of the film. 

==Plot== delinquents and rugby players punks with dyed hair. More importantly, many are orphans whose parents or family died in bombings by the Wild Seven. After their school bus is diverted to an army base, the students are herded into a cage, surrounded by armed guards, and confronted by their schoolteacher, Riki Takeuchi, who lays down the ground rules of the new Battle Royale game. Wild Seven is hiding out on a deserted island (filmed on Hashima Island), and instead of being forced to kill each other, as in the old Battle Royale, the students are sent off to war and ordered to attack the terrorist group’s hideout in masse and kill Shuya within 72 hours. Most of the students are not interested in being forced to avenge their families, but are coerced to fight through exploding metal collars, which their captors can detonate by remote control. The teacher shows them a line in the caged class room: those who wish to participate are instructed to cross the line, while those who dont will be killed. The students are put into pairs; if one student dies, then his or her partner will be killed via collar detonation.

The students are sent via boats onto the dangerous island base of the Wild Seven, and a number of them are killed when they were bombed, shot or their collar detonated during the journey onto the island, leaving only a cluster alive. Most notably, two of the survivors are the main protagonist Takuma Aoi, and Shiori Kitano, the daughter of Kitano, the "teacher" from the first film who died after being shot by Shuya. Taken into the Wild Sevens base, the surviving students explosive collars are removed and they are encouraged to help the members of the Wild Seven stop the Battle Royale for good. While most of the survivors agree, Takuma and in particular Shiori remain unconvinced. Shuya sends a video message to the world of their goal to live free. In response to the video the U.S. fires a missile to the island, and under pressure from the U.S. government, the Japanese prime minister takes command of the military present at the Battle Royale headquarters and orders an attack on the island base, with no survivors allowed - if they fail, the U.S. will bomb the island. Takeuchi is enraged, and is discovered to have the same type of collar on his neck as the students. On Christmas Day, survivors of the base (including the surviving students, except Shiori) retreat to the mainland via a mine shaft while the war between the Wild Seven and the military occurs. Hearing the gunfire outside the tunnel, Takuma and two of his friends return to help in the fight. After a while, the combat takes numerous casualties on both sides, leaving Shuya, Takuma and Shiori as the only remaining fighters. While they try to evacuate, Takeuchi appears in a rugby uniform, and after a brief personal exchange, he allows the group to flee as he sacrifices himself. Outside Wild Sevens base the combat starts again and Shiori is shot, dying in Shuyas arms and seemingly forgiving him for his past crimes. Shuya and Takuma run out to kill the rest of the soldiers while the U.S. bombardment starts.

Three months later, Shuya and Takuma rejoin the other survivors, including Noriko Nakagawa, in what appears to be Pakistan or Afghanistan. They have regrouped as friends, and what lies next for them remains open.

==Cast==
 
* Tatsuya Fujiwara as Shuya Nanahara Ai Maeda as Shiori Kitano (Transfer Student)
* Shugo Oshinari as Takuma Aoi
* Ayana Sakai as Nao Asakura
* Haruka Suenaga as Haruka Kuze
* Yuma Ishigaki as Mitsugu Sakai
* Miyuki Kanbe as Kyoko Kakei
* Masaya Kikawada as Shintaro Makimura Yoko Maki as Maki Souda Yuki Ito as Ryo Kurosawa Natsuki Kato as Saki Sakurai
* Aki Maeda as Noriko Nakagawa
* Riki Takeuchi as Riki Takeuchi (himself)
* Sonny Chiba as Makio Mimura (Shinjis revolutionary uncle)
* Ai Iwamura as Mai (Smiling Winner from BR1)
* Mika Kikuchi as Ayane Yagi
* Takeshi Kitano as Kitano
* Yoshiko Mita as Takumas mother
* Nanami Ohta as Hitoe Takeuchi
* Takeru Shibaki as Shugo Urabe
* Toshiyuki Toyonaga as Shota Hikasa
* Masahiko Tsugawa as The Prime Minister
* Nana Yanagisawa as Mayu Hasuda

==Production==
 , needed to "provide something that Hollywood cant." Fukasaku intended to provide an alternative to what  . June 30, 2003. 

Kenta Fukasaku said that he viewed his task as finishing his deceased fathers movie instead of as directing his first creation; the son credits the film as his fathers. Kenta Fukasaku desired a lot of controversy and outrage for the sequel, adding that "the more strongly people react, the better."  The film was mainly shot at Hashima Island ("Battleship Island").

==Reception==
Requiem received generally negative reviews from film critics. The film received a rating of 38% at Rotten Tomatoes with a classification of "rotten", based on nine reviews, only three of which were positive. Many of the reviewers criticized the film for being inferior to the original, having a contrived, confusing plot line, its controversial, provocative sentiments, and generally bad acting. 
 fascistic adult America is staggeringly bold." 

==Music==
The sequels soundtrack has more original work by Masamichi Amano and fewer classical pieces. One of them, Farewell to the Piano, is played by Shiori Kitano herself during the film.

The song from the opening credits is Dies Irae, taken from the Verdi Requiem.
 first full length, self-titled album.

==Books==
The book The Road to BRII (ISBN 4834252124) is a behind-the-scenes photo collection about the production of the movie. About ten tie-in books related to the movie have been released in Japan.

==Related manga== Riki Takeuchi. There are numerous plot differences between the book and manga. 

==References==
 

==External links==
 
*  
*   at Rotten Tomatoes
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 