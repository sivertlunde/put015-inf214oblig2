And the Ship Sails On
{{Infobox film
| name           = And the Ship Sails On
| image          = And the ship sails on post2.jpg
| caption        = Italian release poster
| director       = Federico Fellini Renzo Rossellini (associate) Daniel Toscan du Plantier
| writer         = Federico Fellini Tonino Guerra Andrea Zanzotto (lyrics)
| starring       = Freddie Jones Barbara Jefford Peter Cellier Norma West Pina Bausch
| music          = Gianfranco Plenizio
| cinematography = Giuseppe Rotunno
| editing        = Ruggero Mastroianni Gaumont The Criterion Collection (DVD)
| released       =  
| runtime        = 132 minutes
| country        = Italy France
| language       = Italian
| budget         = 
}} Best Foreign Language Film at the 56th Academy Awards, but was not accepted as a nominee. 

==Plot==
The film opens depicting a scene in July 1914 immediately prior to the cruise ship Gloria N. setting sail from Naples Harbor. The opening sequence is in sepia tones, as if it were a film shot in that era, with no sound other than the whirring of the projector. Gradually the sepia fades into full colour and we can hear the characters’ dialogue.

Orlando, an Italian journalist, supplies commentary by directly addressing the camera, explaining to the viewer that the cruise is a funeral voyage to disperse the ashes of opera singer Edmea Tetua near the island of Erimo, her birthplace. Considered the greatest singer of all time, Tetua is celebrated for her goddess-like voice.

The bumbling but lovable journalist also provides highly subjective anecdotes and gossip on the wide array of cartoon characters that evoke the golden age of the "funny papers" (Little Nemo, Bringing Up Father, The Katzenjammer Kids) but with a perverse Felliniesque twist. These include more opera singers, voice teachers, orchestra directors, theatre producers, actors, prime ministers, counts, princesses, Grand Dukes, and panic-stricken fans of the deceased diva.
 hypnotizes a chicken. A curly-cued actor travels with his mother in order to seduce sailors. Sir Reginald Dongby, a voyeuristic English aristocrat, relishes spying on Lady Violet, his nymphomaniac wife. The Grand Duke of Harzock, a Prussian, is an obese bubble of a young man whose blind sister (choreographer Pina Bausch) schemes with her lover, the prime minister, to disinherit her brother. The brooding Count of Bassano closets himself in his cabin transformed into a temple dedicated to the diva’s memory.

An awful stench rises from the ship’s hold and soon it’s revealed that a love-sick rhinoceros has been neglected by the ship’s crew. The beast is pulled up, washed on deck, and returned to the hold with fresh water and hay.
  A Night at the Opera in a heady mix of cultures, both ethnic and artistic, where aristocrats and snobs joyfully share the stage (the ship’s deck) with peasants and vibrant Serbian folklore. 

But the revels end when the menacing flagship of the Austro-Hungarian fleet sails into view, demanding the return of the Serbian refugees. The captain agrees on condition that Edmea Tetua’s ashes be dispersed at Erimo beforehand. After the ceremony, the refugees are loaded into a lifeboat for delivery to the Austrians but a young Serbian hurls a bomb at the flagship, causing pandemonium. The Austrians respond by cannon fire. The Gloria N. sinks while Albertini wields his baton, aristocrats march to the lifeboats, a grand piano slides across the floor smashing mirrors, and butterflies twitter serenely above the melee of suitcases in flooded corridors.

In a reverse tracking shot, Fellini reveals the stupendous behind-the-scenes of his floating opera of a movie - giant hydraulic jacks (constructed by Oscar-winning set designer, Dante Ferretti) that created the ship’s rolling sea movements, along with acres of plastic ocean, an army of technicians burning naphthalene for the smoke of disaster effect, and, finally, an enigmatic figure that may be Orlando or Fellini intentionally hiding behind his own camera filming the main camera filming himself.

The main camera then tracks forward to a final shot of Orlando in a lifeboat with the rhinoceros happily munching on hay. "Did you know," confides Orlando, "that a rhinoceros gives very good milk?" Laughing, he once again mans the oars to disappear on a vast plastic ocean.

==Cast==
*Freddie Jones - Orlando
*Barbara Jefford - Ildebranda Cuffari
*Peter Cellier - Sir Reginald J. Dongby
*Norma West - Lady Violet Dongby
*Sarah-Jane Varley - Dorotea
*Pina Bausch - Princess Lherimia
*Janet Suzman - Edmea Tetua
*Philip Locke - Prime Minister
*Jonathan Cecil - Ricotin
*Elisa Mainardi - Teresa Valegnani
*Maurice Barrier - Ziloev
*Linda Polan - Ines Ruffo Saltini
*Pasquale Zito - Count Bassano
*Fiorenzo Serra - The Grand Duke
*Paolo Paoloni - Master Albertini
*Victor Poletti - Aureliano Fuciletto Fred Williams - Sabatino Lepori
*Elizabeth Kaza Colin Higgins - Police chief
*Vittorio Zarfati - Second Master Rubetti
*Umberto Zuanelli - First Master Rubetti
*Claudio Ciocca - Secondo di bordo
*Antonio Vezza - The Captain
*Alessandro Partexano - Official
*Domenico Pertica - Pastor Christian Fremont
*Marielle Duvelle
*Helen Stirling
*Ginestra Spinola - Edmeas cousin
*Umberto Barone - Scala
*Monica Bertolotti
*Danika La Loggia
*Roberto Caporali - Doroteas father
*Franca Maresa - Doroteas mother
*Savatore Calabrese - Banchiere
*Johna Mancini - Segretario del sovrintendente
*Filippo De Gara
*Francesco Scali
*Cecilia Cerocchi - Moglie del sovrintendente
*Pietro Fumelli
*Franco Angrisano - Il cuoco
*Ugo Fangareggi - Il barista

===Dubbed singing voices===
*Mara Zampieri - Ildebranda Cuffari
*Elizabeth Norberg-Schulz - Ines Ruffo Saltini
*Nucci Condò - Teresa Valegnani
*Giovanni Baviglio - Puciletto

==Critical reception==
Screened out of competition at the 40th Venice Film Festival, the film received a fifteen-minute standing ovation. 

Writing for the Italian weekly magazine LEspresso, novelist and film critic Alberto Moravia saw the film as an intuitive critique of European society that preceded the First World War. "What is brilliant," explained Moravia, "is the intuition that European society of the Belle Epoque had emptied itself of all humanism leaving only an artificial and exhaustive formalism. The result was a society founded on a continuous yet contemptible melodrama. The other genial intuition is that of the fundamental unity of the world back then which was completely bourgeois or utterly obsessed with the bourgeoisie. This idea comes through magnificently in the scene where immaculate opera singers perform leaning over the iron balcony of the engine room as sweat-grimed workers cease stoking the furnace with coal to listen to the splendid voices." 

==See also==
* List of submissions to the 56th Academy Awards for Best Foreign Language Film
* List of Italian submissions for the Academy Award for Best Foreign Language Film

==References==
===Notes===
 

===Bibliography===
* Burke, Frank, and Marguerite R. Waller (2003). Federico Fellini: Contemporary Perspectives. Toronto: University of Toronto Press.  ISBN 0-8020-7647-5 

==External links==
* 
*  

 

 
 
 
 
 
 
 
 
 
 
 
 