Vipra Narayana
{{Infobox film
| name           = Vipra Narayana
| image          = Vipranarayana.JPG
| image_size     =
| caption        =
| director       = P. S. Ramakrishna Rao
| producer       = P. S. Ramakrishna Rao Bhanumathi Ramakrishna|P. Bhanumathi
| writer         = Samudrala Raghavacharya   (dialogues and lyrics) 
| narrator       =
| starring       = A. Nageswara Rao P. Bhanumathi Relangi Venkata Ramaiah Rushyendramani
| music          = S. Rajeswara Rao
| cinematography = M. A. Rehman
| editing        = P. S. Ramakrishna Rao
| studio         = Bharani Studios
| distributor    = Bharani Studios
| released       = 1954
| runtime        =
| country        = India
| language       = Telugu
| budget         =
}}
Vipra Narayana or Vipranarayana  story is based on the life of Thondaradippodi Alvar, also called Vipranarayanar or Bhaktanghri Renu Swami was a Tamil Vaishnava Saint. He led his life in devotion to Lord Narayana and worked for the Perumal (Lord) by dedicating him with garlands. He is one of the 12 Alvars.

==Legend==
The story revolves around a Brahmin who makes flower garlands. He devotes his life to Perumal (Lord). He is seduced by a woman who is determined to make him break his vow.

==1937 film==
Vipra Narayana was made in Telugu language first time in 1937 by Aurora Pictures.  starring Kasturi Narasimha Rao, Kanchanamala and Tanguturi Suryakumari, directed by Ahindra Chaudhari.

==1938 film==
Vipra Narayana was made in Tamil language in 1938 by Sound City. Starring Kothamangalam Cheenu and T. V. Rajasundari in the lead roles, the film was produced and directed by A. Narayanan.

==1954 film==
Vipra Narayana was made in bilingual Telugu and Tamil directed and produced by P. S. Ramakrishna Rao and Bhanumathi Ramakrishna|P. Bhanumathi of Bharani Pictures. The title role is played by A. Nageswara Rao. 

===Cast===
{| class="wikitable" width="50%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
| A. Nageswara Rao || Vipranarayana
|-
| Bhanumathi Ramakrishna|P. Bhanumathi || Devadevi
|-
| Relangi Venkataramaiah || Rangaraju
|-
| Rushyendramani || Mother of Devadevi
|-
| V. Sivaram || Maharaju
|- Sandhya || Sister of Devadevi
|}

===Crew===
* Director: P. S. Ramakrishna Rao
* Producers: P. S. Ramakrishna Rao & P. Bhanumathi
* Editor: P. S. Ramakrishna Rao
* Production company: Bharani Pictures
* Lyrics: Samudrala Raghavacharya (Telugu) & K. D. Santhanam (Tamil)
* Music Director: S. Rajeswara Rao
* Playback singers : A. M. Rajah, P. Bhanumathi, T. V. Rathinam
* Cinematography: M. A. Rehman

===Soundtrack===
The music was composed by S. Rajeswara Rao.

;Telugu Songs:
Lyrics by Samudrala Raghavacharya.
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 ||  ||  || rowspan=9| Samudrala Raghavacharya || 
|- 
| 2 ||  ||  || 
|- 
| 3 ||  ||  || 
|- 
| 4 ||  ||  || 
|- 
| 5 ||  ||  || 
|- 
| 6 ||  ||  || 
|- 
| 7 ||  ||  || 
|- 
| 8 ||  ||  || 
|- 
| 9 ||  ||  || 
|}
* "Aadadhi Ante Layam Layam - Aa Needantene Bhayam Bhayam" by Relangi Venkata Ramaiah
* "Anuraagalu Dhooramu" by A. M. Rajaha & Bhanumathi Ramakrishna|P. Bhanumathi
* "Choodumadhe Cheliya" by A. M. Rajah
* "Dharicherukora Ranga" by A. M. Rajah
* "Endhukoyi Thotamali" by P. Bhanumathi
* "Evvade Athadevvade" by P. Bhanumathi
* "Madhura Madhuramee" by A. M. Rajah & P. Bhanumathi
* "Meluko Sriranga" by A. M. Rajah
* "Nanu Vidanaadakura" by P. Bhanumathi
* "Paalinchara Ranga" by A. M. Rajah
* "Raa Raa Naa Swamy" by P. Bhanumathi
* "Ranga Kaaveti Ranga" by A. M. Rajah
* "Ranga Rangayani" by P. Bhanumathi
* "Saavirahe Thava Dheena" by P. Bhanumathi
* "Thillana" by P. Bhanumathi
* "Yela Naapai Dhaya Choopavu" by P. Bhanumathi
* Slokams by P. Bhanumathi

;Tamil Songs:
Lyrics by K. D. Santhanam.
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Aadhavan Ezhundhaan Aaruyire En Maadhavaa || A. M. Rajah || rowspan=9| K. D. Santhanam || 
|- 
| 2 || Yogamadhe Ezhilaam Kannan Yogamadhe Ezhilaam || A. M. Rajah || 
|- 
| 3 || Malaril Madhuvellaam Innisai Thaane || A. M. Rajah & P. Bhanumathi || 
|- 
| 4 || Ini Aagatha Sogam Aanathaa || A. M. Rajah & P. Bhanumathi || 
|- 
| 5 || Naayagane Jaya Geeta Radha || P. Bhanumathi  || 
|- 
| 6 ||  ||  || 
|- 
| 7 ||  ||  || 
|- 
| 8 ||  ||  || 
|- 
| 9 ||  ||  || 
|}

==Awards==
* The film won National Film Award for Best Feature Film in Telugu - Certificate of merit in 1954 at 2nd National Film Awards   

==References==
 

==External links==
*  
 

 
 
 
 
 
 