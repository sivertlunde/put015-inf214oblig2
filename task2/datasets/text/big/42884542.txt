The Stone (2013 film)
{{Infobox film
| name           = The Stone
| image          = 
| director       = Cho Se-rae
| producer       = Jo Hyun-woo
| writer         = Cho Se-rae
| starring       = Cho Dong-in Kim Roi-ha Park Won-sang
| cinematography = Ha Kyung-ho
| editing        = Uhm Jin-hwa
| music          = Noh Hyeong-woo
| distributor    = Cinus Entertainment
| studio         = Shine Picture
| country        = South Korea
| language       = Korean
| runtime        = 119 minutes
| budget         = 
| gross          = 
| released       =  
}}
The Stone ( ) is a 2013 South Korean film written and directed by Cho Se-rae. It premiered at the 2013 Locarno International Film Festival.   

==Plot== Baduk Academy gangster boss, brought up on the streets and used to using his fists to gain authority. After accidentally meeting, playing and losing to Min-su in a gambling club, while his goons were collecting the monthly installment of protection money, his taste for the ancient game is rekindled and he hires the boy to become his private instructor.

As he goes deeper into the baduk experience, Nam-hae begins reviewing his own past in the perspective of the rules and requirements of the game and begins to  lose interest in the criminal world, becoming far more concerned with the future of his tutor, whom he encourages to face the challenge of a pro tournament and carve a legitimate future for himself. The fatherless young Min-su, on the other hand, discovers the coarse masculine underworld, the meaning of life and true victory through Nam-hae. However, when Nam-haes competition starts to expand their territory, he is forced into a path to destruction.  

==Cast==
* Cho Dong-in as Min-su
* Kim Roi-ha as Nam-hae
* Park Won-sang as In-geol
* Myung Gye-nam
* Park Min-gyu
* Son Jong-hak 
* Kim Dong-gon 
* Jo Ji-hwan 
* Heo Joon-seok 
* Choi Jeong-hyeon
* Il Soo-pa 
* So Hee-jeong

==References==
 

== External links ==
*    
*  
*  

 
 
 
 
 

 