Margarita With A Straw
 
{{Infobox film
| name             = Margarita With A Straw
| image            = Margarita,_with_a_Straw_-_poster.jpg
| caption          = 
| director         = Shonali Bose 
| writer           = Shonali Bose
| producer         = Shonali Bose Nilesh Maniyar
| starring         = Kalki Koechlin  Revathi Sayani Gupta
| cinematography   = Anne Misawa
| music            = Mickey McCleary
| editing          = Monisha R Baldawa
| studio           = Viacom 18 Motion Pictures 
| released         =    
| country          = India
| language         = Hindi English
}}
Margarita With A Straw, previously known as Choone Chali Aasman in the Indian market & Margarita With A Straw in the International market is a 2015 Indian film directed by Shonali Bose, starring Kalki Koechlin.   Kalki plays a girl with cerebral palsy.  The movie talks about her struggle with the normal activities in her life. Margarita, with a Straw is one of the five projects selected for the Work In Progress Lab of Film Bazaar 2013.   The film made its world premiere on 8 September 2014 as part of the Contemporary World Cinema Programme at the 2014 Toronto International Film Festival (TIFF). The movie later went on to be screened at Tallinn Black Nights Film Festival in Estonia, BFI London Film Festival, Busan International Film Festival (BIFF) and the Santa Barbara International Film Festival (SBIFF).          It was released in India on 17th April 2015. 

==Awards==
The film won the NETPAC award World or International Asian Film Premiere at 2014 Toronto International Film Festival (TIFF). It was the only Indian film to win an award at TIFF last year.
Kalki Koechlin won The Best Actress award at the Tallinn Black Nights Film Festival held in Estonia in 2014.   The Film won the Audience Award and the Youth Jury Award at Vesoul Film Festival in France in 2015. 
Won Asian Film Awards for the Best Composer Award 2015.  

==Plot==
Laila (Kalki Koechelin) is a wheelchair bound teenager with cerebral palsy. She is a student at Delhi University and an aspiring writer who writes lyrics and creates electronic sounds for an indie band at the university. Laila falls for the lead singer of the college band and is heartbroken when she is rejected. But she soon overcomes this phase, when she gets a scholarship for a semester at New York University and moves there with her mother (Revathy). Living in Manhattan, she meets an attractive young man named Jared (William Moseley) in her creative writing class, who is assigned to help her in typing. She also meets the fiery young activist Khanum (Sayani Gupta), a blind girl of a Pakistani and Bangladeshi descent, whom she later falls in love with. As she embarks on a journey of sexual discovery, she figures out she is bisexual, as she still feels attracted to men like Jared inspite of being with Khanum. Lailas mother believing the two are like best friends and oblivious to the fact that Khanum is her daughters lover, invites her to Delhi to spend the winter break with the family. It is during that time that Laila finally finds the courage to tell her mother about her sexuality and her actual relationship with Khanum. It is what happens next that builds the rest of the story.  

==Cast==
* Kalki Koechlin as Laila
* Revathi as Shubhangini 
* Sayani Gupta as Khanum
* Kuljeet Singh as Lailas father
* Hussain Dalal  William Moseley as Jared
* Malhar Khushu as Monu
* Jacob Berger
* Tenzin Dalha
* Shuchi Dwivedi
* Maninder Chauhan as Dhruvs mother

== Reception ==

Margarita With A Straw was praised by critics. Subhash K. Jha of SkjBollywoodNews.com gave the movie 5 out of 5 stars and wrote "Sip and savour the delicate tastes of life in Margarita With a Straw. This is a moving heartwarming lyrical and yummy tale of a girl so sassy,she defines the eternal quest to seize the day." 

==Music & Lyrics==
* Guest Composer - Joi Barua 
* Hindi Lyrics - Prasoon Joshi
* English Lyrics - Mickey McCleary  

==Soundtrack==

{{Track listing
| extra_column = Singer(s)
| lyrics_credits =yes
| title1 = Dusokute
| extra1 = Joi Barua
| length1 = 3:05
| lyrics1 = Prasoon Joshi
| title2 = Foreign Balamwa
| extra2 = Sonu Kakkar
| length2 = 2:28
| lyrics2 = Prasoon Joshi
| title3 = Choone Chali Aasman 
| extra3 = Rachel Varghese
| lyrics3 =  Prasoon Joshi length3 = 3:05
}}

==References==
 

==External links==
* 

 
 
 
 
 
 
 