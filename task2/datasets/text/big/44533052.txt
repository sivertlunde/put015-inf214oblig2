Vasantha Lakshmi
{{Infobox film 
| name           = Vasantha Lakshmi
| image          =  
| caption        = 
| director       = A. V. Seshagiri Rao
| producer       = K. Vittala Kumar K. V. Honnappa K. V. Gurunath
| story          = A. V. Seshagiri Rao
| writer         = H. V. Subba Rao (dialogues)
| screenplay     = A. V. Seshagiri Rao Vishnuvardhan Manjula Manjula
| music          = Vijaya Bhaskar
| cinematography = S. V. Srikanth
| editing        = P. Bhakthavathsalam
| studio         = Vittal Movies
| distributor    = Vittal Movies
| released       =  
| runtime        = 167 min
| country        = India Kannada
}}
 1978 Cinema Indian Kannada Kannada film, Vishnuvardhan and Manjula in lead roles. The film had musical score by Vijaya Bhaskar.  

==Cast==
 
*Srinath
*Aarathi Vishnuvardhan
*Manjula Manjula
*M. P. Shankar Leelavathi
*Pandari Bai
*B. V. Radha
*Jayamalini
*B. Jaya (actress)|B. Jaya
*Baby Indira
*Baby Jayashanthi
*Naveena
*K. S. Ashwath
*Vajramuni
*Dwarakish
*Thoogudeepa Srinivas
*Dinesh
*Tiger Prabhakar
*M. S. Sathya
*Ashwatha Narayana
*Jr Shetty
*Comedian Guggu
*M. S. Umesh Balakrishna
*Narasimharaju Narasimharaju
*Shivaram
 

==Soundtrack==
The music was composed by Vijaya Bhaskar. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Belli Modave Yelli Oduve || S. P. Balasubrahmanyam, Vani Jayaram || Chi. Udaya Shankar || 03.10
|- Susheela || Chi. Udaya Shankar || 03.32
|-
| 3 || Hennugalendoo Abale Yaralla || S. Janaki|Janaki, Vani Jayaram || Chi. Udaya Shankar || 03.13
|- Janaki || R. N. Jayagopal || 03.07
|-
| 5 || Nadeyale Naduvu || S. P. Balasubrahmanyam, K. J. Yesudas, S. Janaki|Janaki, Vani Jayaram || Chi. Udaya Shankar || 03.08
|}

==References==
 

==External links==
*  
*  

 
 
 
 


 