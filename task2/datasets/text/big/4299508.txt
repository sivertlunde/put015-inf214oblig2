Friends with Money
{{Infobox Film
| name           =  Friends with Money
| image          =  Friends with Money Poster.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = Nicole Holofcener
| producer      =  Anthony Bregman
| writer         = Nicole Holofcener
| starring       = {{Plain list |
*Jennifer Aniston
*Joan Cusack
*Catherine Keener
*Frances McDormand
*Jason Isaacs
*Scott Caan
*Simon McBurney
*Greg Germann
 
}}
| music          =  Rickie Lee Jones
| cinematography =  Terry Stacey
| editing        =  Robert Frazen This is that
| distributor    =  Sony Pictures Classics
| released       =   
| runtime        =  88 minutes
| country        =  United States English
| budget         =  $6.5 million   
| gross          =  $18,245,244 
}}

Friends with Money is a 2006 film written and directed by Nicole Holofcener. It opened the 2006 Sundance Film Festival on January 19, 2006 and went into limited release in North America on April 7, 2006.

==Plot==
Olivia (Jennifer Aniston) is a single, cash-strapped woman working as a maid in Los Angeles in order to make ends meet.  She is surrounded by a support network of well-off friends consisting of Franny (Joan Cusack) &ndash; a stay at home mom with a large trust fund; Christine (Catherine Keener) &ndash; a successful television writer; Jane (Frances McDormand) &ndash; a fashion designer; and their respective husbands.

While the disparity in financial situations between Olivia and her friends creates some friction, each woman is facing her own individual struggles. Olivia cant seem to find love or money and resorts to questionable tactics to satisfy both.  Frannys inheritance sometimes causes tension between her and her accountant husband, who likes to spend it.  Christines marriage is falling apart because she and her husband cant communicate effectively.  Finally, Jane is becoming increasingly unpleasant to be around, possibly because of perimenopause and her husbands sexual ambiguity. Together, these women attend charity benefits, have lunch, lean on each other, and wade their way through life.

==Cast==
*Jennifer Aniston as Olivia
*Catherine Keener as Christine
*Frances McDormand as Jane
*Joan Cusack as Franny
*Jason Isaacs as David
*Scott Caan as Mike
*Simon McBurney as Aaron
*Greg Germann as Matt
*Marin Hinkle as Maya
*Timm Sharp as Richard
*Jake Cherry as Wyatt
*Ty Burrell as Other Aaron Bob Stephenson as Marty

==Box office==
In its opening weekend in wide release, the film grossed a total of $4.96 million, ranking tenth at the North American box office, this soon became $13,367,101 domestically and $18,245,244 worldwide even though it received a limited release in most weeks (under 600 screens).

==Critical reception==
The film received generally positive reviews from critics. The review aggregator Rotten Tomatoes reported that 72% of the critics gave the film a positive review, based on 151 reviews.  Metacritic reported the film had an average score of 68 out of 100, based on 38 reviews.  McDormand won the Best Supporting Female award at the 2006 Independent Spirit Awards.

==Home Media==
The film was released on DVD on August 29, 2006. It has grossed $29.60 M in U.S. DVD/home video rentals.

==References==
 

==External links==
*  
*   at Metacritic.com

 

 
 
 
 
 
 
 