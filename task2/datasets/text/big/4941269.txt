The Acid House (film)
 
 {{Infobox film
| name           = The Acid House
| image          = TheAcidHouseFilmPoster.jpg
| image_size     = 
| alt            = 
| caption        = Theatrical release poster Paul McGuigan
| producer       = 
| writer         = Irvine Welsh
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 111 minutes
| country        = 
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
The Acid House is a film adaptation of Irvine Welshs short story collection The Acid House. Welsh himself wrote the screenplay, and appears as a minor character in the film.

==Plot==
 Paul McGuigan, dramatises three stories from the book: football team. It has elements of Franz Kafkas The Metamorphosis. One of the characters is a pitiless and profane God, who transforms him into a fly as punishment for wasting his life.
* "A Soft Touch": Kevin McKidd plays a cuckolded husband while Gary McCormack is Larry, the ruthless upstairs neighbour who steals his electricity and his wife, played by Michelle Gomez. acid trip and a bolt of lightning result in amiable schemie Coco Brice exchanging bodies with the baby of a middle class couple.

All three sections are independent, but are linked by setting and by the reappearance of incidental characters, in particular Maurice Roëves who appears variously as an inebriated wedding guest, a figure in a dream, and a pub patron. All three of his parts symbolise a human manifestation of God.

The film offended elements of the UK tabloid press with a depiction of a cynical and jaded, foul-mouthed God. In some English-speaking countries (such as Canada and the United States) it has been screened with subtitles because of the heavy Edinburgh accents.

==Cast==
;“The Granton Star Cause”
*Maurice Roëves - God
*Stephen McCole  - Boab
*Garry Sweeney - Kev
*Jenny McCrindle - Evelyn
*Simon Weir - Tambo
*Iain Andrew - Grant
*Irvine Welsh - Parkie
*Pat Stanton - Barman
*Alex Howden - Boab Snr
*Annie Louise Ross - Doreen (as Ann Louise Ross) Dennis OConnor - PC Cochrane John Gardner - Sgt. Morrison William Blair - Workmate
*Gary McCormack - Workmate
*Malcolm Shields - Workmate
*Stewart Preston - Rafferty
;“A Soft Touch”
*Maurice Roëves - Drunk
*Kevin McKidd - Johnny
*Michelle Gomez - Catriona
*Tam Dean Burn - Alec
*Scott Imrie - Pool Player
*Niall Greig Fulton - Alan
*Cas Harkins - Skanko
*Morgan Simpson - Chantal, Baby
*Marnie Kidd - Chantal, Toddler
*Alison Peebles - Mother
*Joanne Riley - New Girl
*Katie Echlin - Wendy
*William Giggs McGuigan - Pub Singer William Blair - Deck
*Gary McCormack - Larry
;“The Acid House”
*Maurice Roëves - Priest
*Ewen Bremner - Colin Coco Bryce
*Martin Clunes - Rory
*Jemma Redgrave - Jenny
*Arlene Cockburn - Kirsty
*Jane Stabler - Emma
*Doug Eadie - Cocos Father
*Andrea McKenna - Cocos Mother
*Cas Harkins - Skanko
*Billy McElhaney - Felix the Paramedic
*Ricky Callan - Tam the Driver
*Barbara Rafferty - Dr. Callaghan
*Stephen Docherty - Nurse Boyd
*Ronnie McCann - Andy

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
  
 
 
 
 
 
 
 