From Vegas to Macau II
{{Infobox film
| name           = From Vegas to Macau II
| image          = From Vegas to Macau II poster.jpg
| alt            =
| caption        = Film poster
| film name      = 
| director       = Wong Jing
| producer       = Andrew Lau
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| starring       = Chow Yun-fat Nick Cheung Carina Lau Shawn Yue Angela Wang
| narrator       = 
| music          = 
| cinematography = 
| editing        = 
| studio         =       
| distributor    = 
| released       =  
| runtime        = 110 minutes
| country        = Hong Kong   China
| language       = Cantonese   Mandarin   Thai 
| budget         = 
| gross          = US$157 million (China) 
}}
 3D  Hong Kong-Chinese film directed by Wong Jing and starring Chow Yun-fat, Nick Cheung, Carina Lau, Shawn Yue and Angela Wang. The film is the sequel to From Vegas to Macau. 
 Where Are film version of that TV show.

==Cast==
*Chow Yun-fat 
*Nick Cheung
*Carina Lau
*Shawn Yue
*Angela Wang
*Michelle Hu
*David Chiang
*Kimmy Tong  Wu Yue
*Jin Qiaoqiao
*Kenny Wong
*Philip Keung
*Derek Tsang 
*Eric Tsang 
*Natalis Chan
*Natalie Meng

===Guest stars===
*Connie Man
*Treechada Petcharat
*Rebecca Zhu
*Samantha Ko
*Candy Chang
*Jacky Cai
*Ken Lo
*Hazel Tong
*Dominic Ho
*Yuan Quan

==Critical response==
James Marsh of the magazine Screen International gave the film a negative review and says "The result is a noticeable step down in quality from last year’s offering" and "Much of the comedy throughout the film also falls flat."   

==Box office== ninth highest-grossing film in China of all time. 
 Dragon Blade Big Hero 6 took the top spot. 

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 