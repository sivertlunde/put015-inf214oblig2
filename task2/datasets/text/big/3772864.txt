The Great Water
{{Infobox film
| name =The Great Water
| caption =The Great Water poster
| image =The Great Water FilmPoster.jpeg
| director = Ivo Trajkov
| producer = Robert Jazadziski
| writer = Zivko Cingo, Ivo Trajkov
| starring =Saso Kekenovski Maja Stankovska
| music = Kiril Dzajkovski
| cinematography = Suki Medencevic
| editing = Atanas Georgiev
| distributor = 
| released =  
| runtime = 93 minutes
| country = Macedonia Macedonian / English  
| budget = 
}} Meto Jovanovski and Verica Nedeska. It was directed and written by Ivo Trajkov. The music was created by Kiril Dzajkovski. 

==Plot==
Based on a childrens book written by Zivko Cingo in the 1970s, the movie is about the difficult transition in Macedonia after World War II.
The film begins in the present as old Lem (Meto Jovanovski), Macedonian politician who is experiencing a heart attack and while he is being wheeled into a hospital and examined and wired, he has memory flashbacks to his childhood in 1945. He is brought to the orphanage where orphans are children of the enemies of the new regime. There he learns how to adjust to the role of obedient brainwashing. He becomes mesmerized by a new kid, Isak, a beautiful and charismatic boy. The struggles quietly underplaying all of the camp surface activity are many: the dichotomy of a Communist ideology removing the Church from existence with a people dependent upon the spiritual values of religion, the Stalin/Tito issue, the adjustments to the policies of Communist regime in a country where fierce national pride had ruled, and the depersonalization of children into political pawns despite the need for role models and the luxury of growing up with friends and confidants. 

==Awards== Best Foreign Language Film, but it failed to make the nominees shortlist. 

==Reception==
The film received favourable reviews from critics, earning a 71% "Fresh" rating based on 21 reviews on Rotten Tomatoes.  The film also received a generally favourable metascore of 62 based on 15 reviews on Metacritic. 

==References==
 

==External links==
*  

 
 
 
 
 