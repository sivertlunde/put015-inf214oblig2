The Suspended Vocation
{{Infobox film
| name           = The Suspended Vocation
| image          = 
| caption        = 
| director       = Raúl Ruiz (director)|Raúl Ruiz
| producer       = 
| writer         = Raúl Ruiz Pierre Klossowski
| starring       = Didier Flamand
| music          = 
| cinematography = Maurice Perrimond Sacha Vierny
| editing        = Valeria Sarmiento
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = France
| language       = French
| budget         = 
}}

The Suspended Vocation ( ) is a 1978 French drama film directed by Raúl Ruiz (director)|Raúl Ruiz.       It is an adaptation of the 1950 novel of the same name by Pierre Klossowski.

==Cast==
* Didier Flamand as Jérôme #1
* Gabriel Gascon as Le père-confesseur
* Pascal Bonitzer as Jérôme #2
* Maurice Bénichou as Le membre de la Dévotion #1
* Pascal Kané as Le membre de la Dévotion #2
* Alexandre Tamar as Le membre de la Dévotion #3
* Jean Badin as Lami
* Françoise Vercruyssen as La femme
* Marcel Imhoff as Le père-maitre #2
* Jean Lescot as Le père-maitre #1
* Daniel Isoppo as Le frère convers
* Gérard Berner as Le prieur #2
* Éric Burnelli as Le frère peintre
* Isidro Romero as Le prieur #1
* Jean-Robert Viard as Lévêque (as Jean Robert Viard)

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 