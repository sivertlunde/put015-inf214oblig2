Voices of the Children
 
Voices of the Children is a 1999 Emmy-Award winning documentary film {{cite web|url=http://www.imdb.com/Sections/Awards/News_And_Documentary_Emmy_Awards/1999|title=  	 Terezin concentration camp.  It was produced and shown on television in the United States. 

== Content ==
The American made-for-TV movie of 1999 tells the story of three people who were imprisoned during World War II as children in Terezin. The small Czech fortress and garrison town was adapted by the Nazis and renamed as Theresienstadt; it served as a concentration camp for Jews. More than 150,000 Jews were processed there; less than 20,000 survived the war. 

The filmmaker, who spent two years in Terezin, traces the survivors war experiences, with the help of their personal journals and drawings.  She follows their stories through the difficult postwar years into the present, filming the survivors with their families in the countries in which they settled: the United States, Austria and the Czech Republic.  In the film, parents and children question the need to talk about the past and they explore the effect of the Holocaust on their lives. The daughter of one survivor confronts her father for the first time, saying, "You cannot pretend it did not affect us."

In their efforts to use Terezin for propaganda purposes, the Nazis permitted inmates to stage a childrens opera called Brundibar. The survivors attend a performance of Brundibar in Prague and recall the special significance it had for them fifty years ago. Today the opera symbolizes the lost world of the Terezin children.  , Voices of the Children website 

== Terezin == Theresienstadt also accepted tens of thousands of Jews from Germany and Austria, with hundreds from the Netherlands and Denmark.

Later in the war, the Nazis used this so-called “model ghetto” for propaganda purposes. In June 1944, after a frenzied period of superficial improvements, they turned parts of the camp into a fake town and agreed to let the International Red Cross inspect it. The uniformed Jewish ghetto police and the Cultural Council elders were to convey the impression that the camp was governed by Jews.

The prisoners endured continual hunger, disease, and overcrowding. Of the 150,000 people who passed through Theresienstadt, 33,000 died there, mostly of starvation and illness. The inmates lived with the constant threat of deportation; thousands were regularly selected for transports to Auschwitz and other Nazi death camps in German-occupied Poland.

Between transports, some of the well-known musicians and performers among the prisoners were allowed to stage operas, plays, concerts and cabarets. Their theatrical productions were often freer of censorship than those in the rest of Nazi-occupied Europe, as the Nazis did not generally bother to censor the inmates, whom they considered doomed.

== Brundibar ==
"Brundibar, tells the story of Pepicek and his sister Aninka, whose ailing mother desperately needs milk. But they are poor, and the village milkman will not give it to them.
Pepicek and Aninka try to earn money by singing for the town’s people, but the greedy organ grinder, Brundibar ("bumblebee" in Czech), drowns out their song. When three animals help rally the towns children to help Pepicek and Aninka, the nasty Brundibar is vanquished.

Just before its premiere in 1942 at the Jewish boys orphanage in Prague, its composer, Hans Krasa, was sent to Terezin. Rudolph Freudenfeld, who was to conduct the premiere performances, took the score with him when he and the orphan boys were also sent to Terezin. The opera was performed 55 times at the camp The Nazis used its production as an example of the rich cultural life of the "model community." Ultimately, Krasa, most of his collaborators on the project and nearly all of the children who performed in it were killed at Auschwitz.

== Awards ==
* 1999 Emmy for best historical program
* Certificate of Merit, Chicago International Film Festival
* 1998 Gold Plaque, Chicago International Television Competition
* 1998 Best Documentary and Audience Choice for Best Documentary, Film Fest New Haven
* 1997 Silver Apple, National Educational Media Network

== Comments ==
"...an important contribution to the cinematic and historical record...a significant addition to our understanding of one of the darkest chapters of the 20th century." – Variety  

"... "Voices of the Children" achieves its powerful emotional force ..." Tulsa World  

"Intimate without being intrusive, sensitive without a jot of sentimentality." - New York Jewish Week 

== Credits ==
Written and directed by Zuzana Justman, 
Produced by Jiri Jezek and Robert Kanter.
Director of Photography: Ervin Sanders and Austin de Besche,
Editor: David Charap
Distributor: The Cinema Guild
Distributor: Ergo Media, Inc

== Further reading ==
*Adler, H.G. Theresienstadt, 1941-1945; das Antlitz einer Zwangsgemeinschaft. Geschichte, Soziologie, Psychologie. Tübingen, Mohr, 1960.
*Bondy, Ruth. "Elder of the Jews":Jakob Edelstein of Theresienstadt, translated from the Hebrew 1989
*Kantor, Alfred, The Book of Alfred Kantor, 1987
*Karas, Joza. Music in Terezin, 1941-1945, Pendragon Press, 1990
*Klíma, Ivan. "A Childhood in Terezin", Granta 44 (1993).
*Lederer, Zdenek, Ghetto Theresienstadt, 1953
*Makarova, Elena. University over the Abyss Lectures in Ghetto Theresienstadt, Sergei Makarov & Victor Kuperman,
*Redlich, Gonda. The Terezin Diary of Gonda Redlich
*Volavkova, Hana, ed. ...I never saw another Butterfly"...:Childrens Drawings and Poems,/Schocken Books" 1993.

==References==
 

== External links ==
* 

 
 
 
 
 
 