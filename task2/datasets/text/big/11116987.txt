Deedar (1951 film)
{{Infobox Film
| name           = Deedar
| image          = 
| image_size     = 
| caption        = 
| director       = Nitin Bose
| producer       = Rajendra Jain (Filmkar)
| writer         = Azm Bazidpuri
| narrator       = 
| starring       = Ashok Kumar Dilip Kumar Nargis Nimmi
| music          = Naushad Shakeel Badayuni (lyrics)
| cinematography = Dilip Gupta
| editing        = Bimal Roy
| distributor    = 
| released       = 1951
| runtime        = 130 min
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} 1951 Bollywood class inequalities. It is one of noted tragedies made in early Hindi cinema. It became a popular film of the Golden era and further established Dilip Kumar as "King of Tragedy".       

Many years later, when Deedar was having a rerun at Mumbai theatres, actor Manoj Kumar, who asked director Raj Khosla to accompany him for a show. Thereafter, the story of Do Badan (1966) was written after reworking its storyline, the film was also  a hit.  It is referred to repeatedly in the Vikram Seths 1993 novel A Suitable Boy, in which people watching it burst into tears and people who cant get tickets start a riot.   

==Cast==
*Ashok Kumar: Dr. Kishore
*Nargis: Mala
*Dilip Kumar: Shamu
*Nimmi: Champa Murad
*Tabassum (actress)|Tabassum: Baby Mala (as Baby Tabassum)
*Tun Tun: Rai s maidservant (as Uma Devi)
*Surendra:(as Surrender)
*Agha Miraz: (as Agha Mehraj)
*Yakub: Choudhury
*Motilal (actor)|Motilal: Bhalla
* Parikshit Sahni

== Soundtrack ==
The Soundtrack was composed by the legend Naushad with lyrics by Shakeel Badayuni. The all-round soundtrack consisted of all the elements of a great album. It exploited the talents of singing legends, such as Mohammad Rafi, Lata Mangeshkar, Shamshad Begum and G. M. Durrani to the utmost. This was also amongst those few soundtracks in which the career of veteran Mohammad Rafi coincided with that of his idol G. M. Durrani. 
{| class=wikitable
|-
!Song
!Singer
|- Bachpan Ke Din Bhoola Na Dena - Male Mohammad Rafi
|- Hue Hum Jinke Liye Barbad - I Mohammad Rafi
|- Hue Hum Jinke Liye Barbad - II Mohammad Rafi
|- Naseeb Dar Pe Tera Aazmane Aya Hon Mohammad Rafi
|- Meri Kahani Bhoolne Wale Mohammad Rafi
|- Dekh Liya Maine Kismat Ka Tamasha Mohammad Rafi & Lata Mangeshkar
|- Tu Kon Hai Mera Kehde Balam Lata Mangeshkar 
|- Le Ja Meri Duayeein Le Ja Lata Mangeshkar
|- Duniya Ne Teri Duniyawale Lata Mangeshkar
|- Bachpan Ke Din Bhoolana Dena - Female Shamshad Begum & Lata Mangeshkar
|- Chaman Mein Rakhe Verana Shamshad Begum
|- Nazar Na Phero Humse Shamshad Begum & G. M. Durrani
|}

== References ==
 
== External links ==
*  
*  

 
 
 
 
 
 
 

 
 