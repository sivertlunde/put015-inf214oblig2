Solas (film)
{{Infobox film
| name           = Solas
| image          = Solas.jpg
| image size     =
| caption        = Spanish film poster
| director       = Benito Zambrano
| producer       =
| writer         = Benito Zambrano
| narrator       = Ana Fernández Carlos Álvarez-Nóvoa
| music          =
| cinematography =
| editing        = Fireworks Pictures
| released       =  
| runtime        = 101 minutes
| country        = Spain
| language       = Spanish
| budget         =
}} 1999 cinema Spanish film written and directed by Benito Zambrano.

The film explores the lives of a mother and daughter and their struggle for survival and happiness.  Both of the women in the story are portrayed as alone (sola, plural solas), each in her own way.

It won five Goya awards in 2000 and several other prizes.

==Plot synopsis==
Solas (Alone) tells the story of Maria (Ana Fernández) and her mother Rosa (María Galiana). Maria is one of four adult children, all of whom moved as far as they could get from their parents and the farm where they grew up. Before the movie starts, the father (later revealed to be a violent, cruel, abusive man) has fallen ill and been brought to a hospital in Seville, where Maria lives. Rosa has been staying at the hospital with him, but the doctor tells her to leave before she falls ill herself. Maria takes Rosa to stay with her in the rundown suburban apartment where she lives, and Rosa rides the bus every day to visit her husband.

Maria is intelligent and wanted an education, but her father wouldnt allow it. Now, at 35, she works for a cleaning service; she is lonely, poor, angry and bitter. She discovers she is pregnant by a man who doesnt want a baby and tells Maria to get an abortion. When she tells him she wants to have the baby and raise it with him, the man rejects her. In her anger and despair, Maria starts drinking heavily.

As her mother Rosa returns from shopping one day, she meets Marias neighbor (vecino) Don Emilio (Carlos Álvarez-Novoa), a kind old widower living alone with his dog. A friendship blossoms between them: he lends Rosa some money when she runs short at the supermarket, and she cooks for him after he burns a stew he forgot was cooking. He falls in love with Rosa, but Rosa is faithful to her abusive husband. (At one point she says to Maria about her father, "He must not have an easy conscience. I do.")

Rosas husband recovers and she returns with him to the country, not knowing about Marias pregnancy. Maria tells Don Emilio about the baby and tells him she plans to abort it. In a long, emotional scene, he offers to be like a grandfather to the child if she decides to keep it, but Maria has been so badly treated by the men in her life that she has trouble believing him.

The movie ends with Maria visiting her parents grave with her baby girl and Don Emilio. He is going to sell his apartment in Seville and the three of them will move into Rosas house in the country to raise the baby.

==Cast==
*María Galiana as Madre Ana Fernández (actress) as María
*Carlos Álvarez-Nóvoa as Vecino
*Antonio Dechent as Médico
*Paco De Osca as Padre

==External links==
*  

 
 
 
 
 

 
 