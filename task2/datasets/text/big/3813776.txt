N.Y., N.Y. (film)
{{Infobox film
| name           = N.Y., N.Y.
| image          = N.Y.,N.Y. Tile.png
| image_size     = 
| caption        =  Francis Thompson
| producer       = 
| writer         = 
| narrator       = 
| starring       = 
| music          = Gene Forrell
| cinematography = 
| editing        = 
| distributor    = Anagram International
| released       = 
| runtime        = 15 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} 1957 film Francis Thompson. New York, New York.

N.Y., N.Y. is mentioned in Aldous Huxleys essay Heaven and Hell:

 
And then there is what may be called the Distorted Documentary a new form of visionary art, admirably exemplified by Mr. Francis Thompson’s film, NY, NY. In this very strange and beautiful picture we see the city of New York as it appears when photographed through multiplying prisms, or reflected in the backs of spoons, polished hub caps, spherical and parabolic mirrors. We still recognize houses, people, shop fronts, taxicabs, but recognize them as elements in one of those living geometries which are so characteristic of the visionary experience. The invention of this new cinematographic art seems to presage (thank heaven!) the supersession and early demise of non-representational painting. It used to be said by the non-representationalists that colored photography had reduced the old-fashioned portrait and the old-fashioned landscape to the rank of otiose absurdities. This, of course, is completely untrue. Colored photography merely records and preserves, in an easily reproducible form, the raw materials with which portraitists and landscape painters work. Used as Mr. Thompson has used it, colored cinematography does much more than merely record and preserve the raw materials of non-representational art; it actually turns out the finished product. Looking at NY, NY, I was amazed to see that virtually every pictorial device invented by the old masters of non-representational art and reproduced ad nauseam by the academicians and mannerists of the school, for the last forty years or more, makes its appearance, alive, glowing, intensely significant, in the sequences of Mr. Thompson’s film.
 

==External links==
*  

 

 
 
 
 
 