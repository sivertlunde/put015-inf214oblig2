Mone Boro Kosto
{{Infobox film
| name           = Mone Boro Kosto
| image          = Mone Boro Kosto.jpg
| alt            =  
| caption        = VCD Cover
| director       = Shahin Shomon
| producer       = Oiajit Ali Shomon
| writer         = Abualla Zohir Babu
| starring       = Shakib Khan Apu Biswas Nirob Keya
| music          = Shawkat Ali Imon
| cinematography = Poner
| editing        = Thofik Hossian Chowdhury
| studio         = 
Box Office       = 11 core
| distributor    = M. S. Films
| released       =   
| runtime        = 
| country        = Bangladesh
| language       = Bengali
| budget         = 
| gross          = 
}} Action Bangladeshi Bengali language film. Directed  by Shahin Shomon. The film released was 2009 in all over Bangladesh. Produced by Oiajit Ali Shomon also distributed by S. M. Films. The film stars Shakib Khan, Apu Biswas, Nirob, Keya and many more. This movie is actually inspired from 2008 Telugu film Souryam.

==Cast==
* Shakib Khan 
* Apu Biswas  
* Nirab 
* Keya
* Misa Shaodugar
* Don
* Kabila
* Nisran
* Probir Mitra 
* Jacki alamgir

==Crew==
* Director: Shahin Shomon
* Producer: Oiajit Ali Shomon
* Story: Abualla Zohir Babu
* Script: Abualla Zohir Babu
* Music: Shawkat Ali Imon 
* Lyrics: Kabir Bokul
* Cinematography: Poner
* Editing: Thofik Hossian Chowdhury
* Distributor: S. M. Films

==Technical details==
* Format: 35 MM (Color) Bengali
* Country of Origin: Bangladesh
* Date of Theatrical Release: 2009
* Year of the Product: 2008
* Technical Support: Bangladesh Film Development Corporation (BFDC)

==Music==
{{Infobox album
| Name = Mone Boro Kosto
| Type = soundtrack
| Cover = 
| Artist = Shawkat Ali Imon 
| Released = 2009 (Bangladesh)
| Recorded = 2009
| Genre = Films Sound Track
| Length =
| Label = Eagul Music
| Producer = Eagul Music
}}
Mone Boro Kosto  films music directed by Shawkat Ali Imon and the films lyrics by Kabir Bokul.

===Soundtrack===
{| border="3" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Tracks !! Titles !! Singers !! Performers 
|- 1
|Kosom kosom 
| S I Tutol Shakib Khan
|- 2
|Ekta moner 
| Kumar Bisojit and Baby Niznin Shakib Khan and Apu Biswas
|- 3
|Tomak pelam
| S I Tutol and Fahimda  Shakib Khan and Apu Biswas
|- 4
|Dhok Dhok Kore Vitor   Asif and Mila Nirab and Keya
|- 5
|Ei rupholo 
|
|
|}

==Box office==
Mone Boro Kosto was a Super Duper hit at the box office.

==References==
 

==External links==

 
 
 
 
 
 