A Star Is Bored
{{Infobox Hollywood cartoon
| image          = A Star is Bored title card.png
| series         = Looney Tunes (Bugs Bunny)
| director       = Friz Freleng
| producer       = Edward Selzer (uncredited)
| story_artist   = Warren Foster Art Davis   Virgil Ross   Gerry Chiniquy
| layout_artist = Hawley Pratt
| background_artist = Irv Wyner
| voice_actor    = Mel Blanc June Foray (uncredited) Arthur Q. Bryan (uncredited)
| musician       = Milt Franklyn
| studio         = Warner Bros. Cartoons
| distributor    = Warner Bros. Pictures The Vitaphone Corporation
| release_date   = September 15, 1956 (USA)
| color_process  = Technicolor
| runtime        = 7 minutes
| movie_language = English
| preceded_by = Half-Fare Hare
| followed_by = Wideo Wabbit
}}

A Star Is Bored is a Warner Bros. animated cartoon of the Looney Tunes series, directed by Friz Freleng. The cartoon is mainly made up of reused footage from earlier cartoons to expand upon the rivalry depicted between Bugs and Daffy in such films as Chuck Jones Rabbit Fire, this time placing the action in a show-biz setting. In this 7-minute short, Daffy must double for Bugs in any slapstick that Warners deems too dangerous for its top star.   

==Plot==
The opening frame depicts the exterior of Bugs dressing room, inside which he is talking to the journalist, Lolly (a reference to the nickname of famed Hollywood columnist Louella Parsons).

Daffy then marches into the casting directors (possibly Jack Warner) office just as he is on the phone with another executive discussing the difficulty in finding anyone "stupid enough" to be Bugs stunt double for his next picture. 
 Western co-starring Yosemite Sam. Initially, Daffy is extremely excited to be finally in any motion picture. He takes Bugs place in a rabbit costume and holding a carrot, and stands next to Sam.  

Next, Bugs is in a scene where Elmer Fudd is cast in his usual role as trying to hunt Bugs. Bugs is high in a tree, and Elmer is supposed to climb it to saw the branch Bugs is sitting on, off (though not all the way through, as Bugs reminds him). However, Daffy has other ideas. He tells Elmer to come closer to him, as he has something to tell him. Lacking a clue to Daffys actual motive, Elmer shuffles closer to Daffy, who whacks him in the head to knock him out. 
 bluefin tuna swallows him whole. 

The next scene has Bugs piloting a plane accelerating up to 20,000 feet, then going uncontrollably in the direction of the ground.

The final scene shows the filming of The Duck, with Daffy starring as a typical duck in a peaceful pond and directed by the same man who helmed the earlier movie wherein Daffy subbed for Bugs.

==Production==
This was the first of the three Friz Freleng Bugs Bunny/Daffy Duck cartoon to be mostly made up of reused animation from earlier cartoons. In terms of production order, the other two were Show Biz Bugs (1957) and Person to Bunny (1960).

==Cast==
*Mel Blanc as Bugs Bunny, Daffy Duck, Yosemite Sam, Film producer|Producer  (voice) 
*Arthur Q. Bryan as Elmer Fudd  (voice) (uncredited) 
*June Foray as Lolly  (voice) (uncredited) 

==Availability==
As of 2007, A Star Is Bored is available on the four-disc  ,  as well as the similar, two-disc DVD Looney Tunes Spotlight Collection: Volume 5. It is also available as a bonus on the Playstation 2 version of 
 , and the "Daffy Ducks Madcap Mania" VHS.
==See also==
* List of Bugs Bunny cartoons
* List of Daffy Duck cartoons
* List of Yosemite Sam cartoons

==References==
 

==External links==
*  

 
 
 
 
 
 