The Egg and I (film)
{{Infobox film
| name           = The Egg and I
| image          = Theegg&i.jpg
| caption        = Theatrical release poster
| director       = Chester Erskine
| producer       = Chester Erskine Fred F. Finklehoffe Leonard Goldstein (associate producer)
| writer         = Betty MacDonald (novel)  Chester Erskine Fred F. Finklehoffe
| starring       = Claudette Colbert  Fred MacMurray  Marjorie Main Frank Skinner
| cinematography = Milton R. Krasner
| editing        = Russell F. Schoengarth
| studio         = Universal-International
| distributor    = Universal-International
| released       =  
| runtime        = 108 minutes
| country        = United States
| awards         =
| language       = English
| budget         = $1,900,000 BUSIEST FILM PRODUCER IN HOLLYWOOD: Four on the Way Success Key What Corn Belt? Yard Boy
By GLADWIN HILL. New York Times (1923-Current file)   10 Dec 1950: X10.  
| gross          = $5,550,000 (est. US/ Canada rentals) 
}} book of the same name by Betty MacDonald and starring Claudette Colbert and Fred MacMurray, with Marjorie Main and Percy Kilbride as Ma and Pa Kettle.

The box office success of The Egg and I influenced the production of Universal Studios|Universal-Internationals Ma and Pa Kettle franchise, which consists of nine feature films most of which star Main and Kilbride together. On May 5, 1947, Colbert and MacMurray reprised their roles in a radio version of the film that was broadcast on the Lux Radio Theatre.
 Best Actress in a Supporting Role.

==Synopsis==
It tells the story of a young married couple who become chicken farmers. Betty follows her husband Bob to the countryside where his dream is to be a successful chicken farmer. The problem is, their home is old and needs to be repaired and the baby chicks need constant care. When a rich single woman with a new house and new farm equipment flirts with Bob, Betty questions their decision to move to the farm in the first place. In the end, she finds out that Bob was Trying to buy the new house for Betty as a surprise.

==Cast==
*Claudette Colbert as Betty MacDonald
*Fred MacMurray as Bob MacDonald
*Marjorie Main as Ma Kettle
*Louise Allbritton as Harriet Putnam
*Percy Kilbride as Pa Kettle Richard Long as Tom Kettle
*Billy House as Billy Reed
*Ida Moore as Emily (the old lady)
*Donald MacBride as Mr. Henty
*Samuel S. Hinds as Sheriff
*Esther Dale as Birdie Hicks
*Elisabeth Risdon as Bettys Mother
*John Berkes as Geoduck
*Victor Potel as Crowbar
*Fuzzy Knight as Cab Driver
*Isabel OMadigan as Mrs. Hicks Mother
*Dorothy Vaughan as Maid
*Banjo the Dog as Sport William Bailey, William Desmond, George Lloyd, Dorothy Vernon

==Production crew==
*Production Design ....  Bernard Herzbrun
*Set Decoration ....  Oliver Emert / Russell A. Gausman
*Hair Stylist ....  Carmen Dirigo
*Makeup Artist ....  Jack P. Pierce
*Assistant Director ....  Frank Shaw
*Second Unit Director ....  Jack Hively (uncredited)
*Sound Technician ....  Glenn E. Anderson
*Sound ....  Charles Felstead
*Orchestrator ....  David Tamkin
*Composer: Stock Music ....  Sam Perry (uncredited)
==Reception==
The film was a big hit earning $6 million at the box office. 
==References==
 

==External links==
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 


 