Once Upon a Time (1973 film)
{{Infobox Film
| name           = Once Upon a Time
| image          =
| caption        = Original one-sheet poster
| director       = Roberto Gavioli Rolf Kauka
| producer       = Rolf Kauka
| writer         = Roberto Gavioli Rolf Kauka
| starring       =  Peter Thomas
| cinematography = 
| editing        = 
| distributor    =      }}  West Germany: Constantin Film
| released       =  
| runtime        = 78 minutes
| country        = Italy, West Germany
| language       = Italian / German
| budget         = 
| gross          = 
}}
Once Upon a Time ( ) is a 1973 West German animated musical film written and directed by Roberto Gavioli and Rolf Kauka.  The story is based on the German fairytale "Frau Holle", or "Mother Hulda".

==Plot== Gypsy con-artist mother arrive in town. Mary-Lous mother poses as a fortune teller, telling fake fortunes for money, during which she meets Mr. Bottle and notices that he is wealthy. She cons him into marrying her, saying that great misfortune will befall him if he doesnt get married. Mary-Lou is the exact opposite of Maria, shes cruel, selfish, vain and rude. She and her mother torment Maria by treating her like a servant, and Marias father is helpless to stop them.

One day during a royal hunt, the Prince of the kingdom is separated from his hunting party when he chases after a white stag. He loses the stag, but stumbles upon Mary-Lou bathing in the woods and viciously chastising Maria, who is waiting on her. When Mary-Lou stalks off to change, the Prince approaches the crying Maria and comforts her. Mary-Lou reappears and, thinking the Prince is just a common hunter, tricks him and steals his horse. The Prince and Maria spend some time alone together and fall in love. As a token, the Prince gives Maria a garnet stone necklace, telling her that it will bring them together again. They part ways.

Back at their house, Mary-Lou sees Maria admiring her garnet stone necklace by a well. Mary-Lou taunts Maria by grabbing the necklace and dropping it into the well.

In the royal castle, the King (an avid beekeeper in his own right, gives the Prince his approval to marry a common girl. A proclamation is issued to the people that the "girl with the garnet stone" is to come to the castle, where she will wed the Prince. All girls of marrying age quickly find garnet stone necklaces and rush to the palace. Mary-Lou and her mother manage to get a garnet stone from a local hunchback cobbler whos in love with Mary-Lou, but when they go to the palace, Mary-Lou realises that the Prince is the hunter from the forest and that the necklace she needs to become queen is the one that was given to Maria.

Maria herself is unaware of the proclamation. After returning home, Mary-Lou offers to help Maria retrieve the necklace from the bottom of the well. When Mary-Lou reveals her intentions to take the necklace for herself, the two girls struggle and fall into the well. At the bottom of the well, they meet a Frog King, who tells them that the necklace has been taken by the magical Mother Hulda|Mrs. Holle.

The two girls end up in the magical realm of Mrs. Holle. They ask for directions to Mrs. Holles house, and along the way meet various magical creatures and things that need help. At each point Maria happily helps the creatures, while Mary-Lou refuses. When they arrive at Mrs. Holles house, Mrs. Holle explains that they have to stay a while, because the door leading to their world is closed. Maria agrees to stay and help Mrs. Holle with her chores, but Mary-Lou leaves to find her own way back. Maria is aided by Mrs. Holles toddler servants, while Mary-Lou runs into all amount of trouble. In the end Mary-Lou reluctantly returns to the house, and Mrs. Holle sends both girls back home.

The entire village watches as the two girls are magically returned from the well. Mrs. Holle appears in the sky and says, "For what you have done of your own accord, you both shall receive your just reward." Marias clothes transform to a beautiful white gown, and the garnet stone necklace appears around her neck. As for Mary-Lou, pitch and water falls on her, and she ends up getting married to the hunchback cobbler. The Prince arrives and is reunited with Maria, taking her to the castle to be married.

==Release==
 Fix und Foxi shortly before the films release.  The Fix und Foxi short "Symphonie in Müll" ("Symphony in Garbage") initially accompanied the feature in theaters.  Once Upon a Time is also known as Der Zauberstein ("The Garnet Stone") in German, and Cinderellas Wonderland in the UK.

==See also==
*List of animated feature-length films

==References==
 

==External links==
*  

 
 
 
 
 
 
 