Kai Po Che!
 
 

{{Infobox film
| name           = Kai Po Che!
| image          = Kai Poche film poster.jpg
| caption        = Theatrical release poster
| director       = Abhishek Kapoor
| producer       = Ronnie Screwvala Siddharth Roy Kapur
| based on       = The 3 Mistakes of My Life by Chetan Bhagat
| screenplay     = Pubali Chaudhari Supratik Sen Abhishek Kapoor  Chetan Bhagat
| starring       = Sushant Singh Rajput Asif Basra Amit Sadh Rajkummar Rao Amrita Puri
| music          = Amit Trivedi Background Score: Hitesh Sonik
| cinematography = Anay Goswamy
| editing        = Deepa Bhatia
| studio         = UTV Motion Pictures
| distributor    = UTV Motion Pictures
| released       =  
| runtime        = 127 minutes
| country        = India
| language       = Hindi
|  budget        =     
| gross =  
}} drama buddy buddy film Gujarati phrase that means "I have cut" which refers to Makar Sankranti (known as Uttarayan in Gujarat) where one of the competitors uses his kite to cut off another competitors kite and yells "Kai Po Che!"   The shooting started mid-April in Vadnagar and Ahmedabad.

Set in Ahmedabad, the story revolves around three friends, Ishaan (Sushant Singh Rajput), Omi (Amit Sadh) and Govind (Rajkummar Rao), who want to start their own sports shop and sports academy. The film tracks their deep friendship, and innocence tarnished by religious politics and communal hatred, which results in their fallout, and an eventual death. Kai Po Che! had a world premiere at the 63rd Berlin International Film Festival on 13 February 2013 where it was the first ever Indian film to feature in the World Panorama section.     Kai Po Che! released world wide on 22 February 2013 and met with critical acclaim among Indian reviewers, but negative reviews internationally.    

==Plot==
Ishaan "Ish" Bhatt (Sushant Singh Rajput) is an ex district level cricketer who is a victim of politics in the cricketing selection fraternity, Omkar "Omi" Shastri (Amit Sadh) is the nephew of a Hindu politician (Bittu) who funds their business and Govind "Govi" Patel (Rajkumar Rao) is a geek who is the mastermind behind starting a sports shop which also doubles as a sports academy. Together they open a sports shop and an academy to train and promote talented budding cricketers. After toiling hard, they succeed in establishing it as a center for cricket among the local youth and start to incur profits.

Ishaan requests Govind to teach his sister Vidya (Amrita Puri) mathematics as her exams are coming up. Govind is reluctant at first but agrees eventually. Vidya and Govind gradually fall in love with each other and secretly enter a relationship unknowingly. Omi discovers the same and warns Govind of the consequences, as Ishaan is very protective of his sister.
 destructive earthquake hits Gujarat and Govinds shop in the mall is destroyed. Govind is shattered as the amount of money he invested was very large and he is now loaded with a huge debt. Omi is reluctantly forced to work for his maternal Uncles right-wing party due to the money they owe him.
 test match against Australia. Omi gets busy with religious politics and joins Bittus party. Ishaan and Govind, meanwhile, are busy with Ali and Vidya respectively. Tension arises in the political sphere when Bittu loses the elections in his constituency to his opponent (Asif Basra) belonging to the secular party. As a part of their campaigning, Bittu sends pilgrims (kar sevaks) to Ayodhya to the Ram Temple. Omis parents were also among them. On the returning day, the shocking news of the Godhra train massacre reaches the people. Omi is shattered but Bittu convinces him to take revenge on the murderers of his parents.
 the communal riots which were about to start. As expected, the violence starts by sunset, and the mob led by Bittu storms into the Muslim locality, killing each and every Muslim in sight. A fight ensues between Alis father and Bittu. Alis father stabs Bittu in defense and rushes to save Ali and take shelter in their attic. Omi, enraged at the death of his uncle Bittu, follows him with a gun. Meanwhile, Ishaan comes to know about Vidya and Govinds relationship, when he reads Vidyas text message in Govinds mobile phone about her periods. Enraged, he starts thrashing Govind while Omi enters the premises with a gun in hand, desperate to find and kill Ali and his father. Ishaan and Govind break their own fighting and try to stop Omi as he desperately searches for Ali and his father. Omi finally aims and shoots at Ali. Ishaan in a bid to save Ali takes the bullet himself and consequently dies.
 Indian cricket team against Australia national cricket team|Australia. He plays his first shot by hitting the ball to the boundary with a Cover drive just like his "Ishaan bhaiya" had taught him.

==Cast==
* Sushant Singh Rajput as Ishaan Bhatt 
* Amit Sadh as Omkar Shastri aka Omi
* Rajkummar Rao as Govind Patel
* Amrita Puri as Vidya Bhatt
* Digvijay Deshmukh as Ali
* Asif Basra as Alis father
* Chitranjan Thakur as Vinay
* Manav Kaul as Bittu Mama
* Tahir Raj Bhasin as Ali (adult)
* Bakul Syal as Ballu Balram
* Rohit Bhatnagar as Tinku
* Manish Chawla as Mannu
* Ishaan (Chetan Bhagats son) as Ishaan Patel 
* Dinesh Kumar as John
* Ashish Kakkad as Vishwas
* Ajay Jadeja as cricket commentator (cameo role)
* Gaurav Kapur as cricket commentator
* Atheeq Samad & Jazeer as Guest Appearance

==Production==

===Filming===
Filming began in Vadnagar in Mehsana district of Gujarat along majority of scenes canned in old Ahmedabad and continued further in Porbandar, Diu, India|Diu.   A scene featuring the trio was shot at Hatkeshwar Mahadev temple.  One of the songs Meethi Boliyan was exclusively shot around Daman and Diu, India|Diu. Reportedly, Sushant Singh Rajput had to undergo four months cricket training under two coaches to fit into the book adapted character of Ishaan.  A cricket match sequence was shot at the Sabarmati Railway stadium sabarmati in Ahmedabad.  The Board of Control for Cricket in India (BCCI) has granted the director permission to use footage from a historic India–Australia Kolkata test match from 2001 in Kai Po Che!.   Director Abhishek Kapoor has admitted that the leading Bollywood stars didnt want to be a part of the film.   Former cricketer Ajay Jadeja has been roped in for a special appearance in this film. 

===Title===
Chetan Bhagat, whose book is adapted for the screen, first revealed the title of the film. Subsequently, a short contest was announced by him on Twitter to create the awareness about the film and its title. 

==Marketing== Huma Qureshi, Arshad Warsi praised the film.   

==Soundtrack==
The music was composed by Amit Trivedi. Lyrics were penned by Swanand Kirkire and Shruti Pathak(Gujarati lyrics in Shubhaarambh), while the background score has been scored by Hitesh Sonik.

{{Infobox album
| Name = Kai Po Che!
| Type = Soundtrack
| Artist = Amit Trivedi
| Cover = Kai Po Che 2013.jpg
| Border =
| Alt =
| Caption =
| Released =  
| Recorded = 2012–2013 Feature film soundtrack
| Length = 12:21 Hindi
| Label = UTV Software Communications
| Producer = Ronnie Screwvala, Siddharth Roy Kapur, Amit Trivedi
| Last album=Luv Shuv Tey Chicken Khurana  (2012)
| This album=Kai Po Che! (2013) Bombay Talkies (2013)
}}

{{Track listing
| headline = Tracklist
| extra_column = Singer(s)
| total_length = 12:21 
| lyrics_credits = Swanand Kirkire & Shruti Pathak(Shubhaarambh)
| title1 = Manjha
| extra1 = Amit Trivedi & Mohan Kanan
| lyrics1 = Swanand Kirkire
| length1 = 3:37
| title2 = Shubhaarambh
| extra2 = Shruti Pathak & Divya Kumar
| lyrics2 = Shruti Pathak
| length2 = 3:54
| title3 = Meethi Boliyaan
| extra3 = Amit Trivedi & Mili Nair
| lyrics3 = Swanand Kirkire
| length3 = 4:50
}}

==Release==
Kai Po Che! release date was confirmed as 22 February 2013. The film was released on 1000 screens in India.  Kai Po Che!s theatrical trailer was released along with Dabangg 2.  The first trailer of this movie was released on YouTube on 20 December 2012. 
The 3 Mistakes of My Life book cover will be relaunched with the poster of Kai Po che!.  Kai Po Che! received U certificate from the Censor Board prior to its release without any cuts.

==Critical reception==
The film was critically acclaimed by Indian critics but was met with a rather mixed reception from critics overseas. Sukanya Verma for Rediff.com gave 4/5 stars and says Abhishek Kapoors clarity of vision makes Kai Po Che!, the adaptation of a mediocre novel, so irresistible.     Taran Adarsh of Bollywood Hungama gave it 4 out of 5 and stated that the film is brimming with solid content.  Meena Iyer of The Times of India gave it 4 out of 5 stars saying, "Kai Po Che! is very likeable. Between tears, you find yourself smiling, because its the story of friendship and human triumph above all else."  Resham Sengar of Zee News gave 4/5 stars stating, "The magic of the film lies in its details!"  Saibal Chatterjee of NDTV gave 3.5 stars saying, "Kai Po Che! is a competently crafted, well acted and consistently engaging drama that makes its point without sinking into preachy paroxysms".  Shubhra Gupta of Indian Express gave 3.5/5 stars adding, "Abhishek Kapoor uses Kai Po Che! as an apt metaphor and crafts a lovely, emotive film on abiding friendship and the values that make life worth living."  India Today gave 3.5 stars. 

Khalid Mohamed of Deccan Chronicle gave 3 out of 5 stars and stated that it is a good film.  Deccan Herald stated that it is a warm film  while Live Mint said that Kai Po Che! is a well-crafted entertainer.  Anupama Chopra of The Hindustan Times gave the movie 4 out of 5 stars saying that "Kai Po Che! is Gujarati for "Ive cut". It is used as a cry of victory in kite-flying contests. Here victory is hard-earned and tinged with tears and regret. But its also deeply satisfying."  Rajeev Masand of IBN Live gave the movie 4/5 stars saying that "Its only February, but one of the years best films has arrived".  The Hindu stated that the film is breathless. 

The film is rated 57% rotten on the review site Rotten Tomatoes.  Critic Aaron Hillis of The Village Voice gave a negative review, stating that "The dramatic stakes are so puny that every obstacle can be overcome with a simple work-it-out montage, a cheap device prevalent enough in this movie to start a drinking game."    Rachel Saltz of the New York Times wrote that the film "Mixes, not quite successfully, traditional Bollywood storytelling with something less conventional."    Kate Taylor of the Globe and Mail wrote that the film "might be the next Bend It Like Beckham – if it did not have the sensibilities of the next Doctor Zhivago (film)|Dr. Zhivago."    On the other hand, Robert Abele of the Los Angeles Times gave it a slightly positive review, criticizing the film for its grimness and lack of subtlety, but complimenting the performances as "enjoyably boisterous."   

==Box office==
Kai Po Che! opened with a collection of   on day one with limited release where it did good business in metro cities especially.    After the opening day, the film showed very good growth, grossing   and   on its second and third days respectively to make a collection of around   in its first weekend.    It grossed   after its first week.    The film held well despite some new releases and collected around   in its second weekend.  It grossed   in second    and   in its third week.  The final domestic collections of the film are  . 

After its successful run in domestic market, the film has done well overseas with a business of around $2.25 million in 17 days.  Final overseas collection was US$2.325 million. 

==References==
 

==External links==
 
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 