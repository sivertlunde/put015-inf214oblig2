Leap into Bliss
Leap Austrian comedy Fritz Schulz and starring Schulz, Olly Gebauer and Rosi Barsony. It was one of a number of Austrian films made by newly founded independent film companies by German filmmakers who were forced to leave Nazi Germany after 1933 - a cycle which ended with the Anchluss of 1938. 

==Cast== Fritz Schulz - Fritz Wiesinger 
* Olly Gebauer -  Anny 
* Rosi Barsony - Ilona, ein Revuestar 
* Felix Bressart - Kriegel, Geheimdetektiv 
* Josef Rehberger - Rudi May, Inhaber Warenhaus May 
* Tibor Halmay - Karl, ein ehemaliger Artist 
* Fritz Imhoff - Ein Herr aus Linz 
* Hans Homma - Braun - Geschäftsführer 
* Iris Arlan - Mrs. Huber 
* Hans Unterkircher - Baron Rivoli 
* Illa Raudnitz - Verkäuferin im Warenhaus

==References==
 

==Bibliography==
* Kohl, Katrin & Robertson, Ritchie. A History of Austrian Literature 1918-2000. Camden House, 2006.

==External links==
* 

 
 
 
 
 


 