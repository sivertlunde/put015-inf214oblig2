Till I Met You
{{Infobox film
| name           = Till I Met You
| image          = TillIMetYou.jpg
| caption        = Poster
| director       = Mark A. Reyes
| producer       = Vic del Rosario(VIVA Films) Veronique del Rosario-Corpus(VIVA Films) Jose Mari Abacan(GMA Films) Annette Gozon-Abrogar(GMA Films)
| starring       = Regine Velasquez Robin Padilla Eddie Garcia Marky Cielo Jackie Rice 
| music          =
| cinematography =
| editing        =
| studio         = GMA Films
| distributor    = GMA Films VIVA Films
| country        = Philippines
| gross          = P100,000,000 language       = English, Tagalog, Filipino
}}
 romantic film from the Philippines, starring the romantic pairing of Regine Velasquez and Robin Padilla with a very special part by Eddie Garcia. Also appearing are Mike "Pekto" Nacua, Joonee Gamboa, Luz Valdez, Idda Yaneza and, in another special part, Kuh Ledesma, who originally sung the soundtrack. The cast also includes StarStruck (Philippine TV series)|StarStrucks ultimate survivors Jackie Rice and the Late Marky Cielo. The film was directed by Mark A. Reyes and produced by GMA Films and Viva Films.
 VIVA withdrew from GMA Network, Inc.|GMA. The joint production between VIVA & GMA is still a major factor after their splitup in 2000 and 2004.

==Synopsis==
Señor Manuel (Eddie Garcia)is an elderly gentleman who falls in love with the charming Luisa (Regine Velasquez), a confidence trickster posing as a rich socialite. In preparation for their upcoming wedding, Señor Manuel takes Luisa back to see his hacienda and introduces his new fiancé to the staff. Among which is Gabriel (Robin Padilla), Manuels right-hand man, who is not only well respected by the town’s people, but is also the one person that Señor Manuel trusts the most. Likewise, Gabriel cares deeply for the old man and is very protective of the haciendero; so much so that during the initial meeting, the fiercely devoted Gabriel immediately becomes suspicious that Luisa is a fake and is only after Señor Manuel’s money. In order to expose Luisa as nothing but a  ,  Caught in a tangled web of lies and deceit, all so unexpected . . .all so unwanted, what twisted fate awaits them?

==Trivia== Viva and GMA after their split up in 2000 and 2004.
*Its first week gross was P40 million with a total lifetime gross of 100,450,200.

==See also==
*VIVA Films
*GMA Films
*Shes Dating the Gangster (film)|Shes Dating the Gangster

==External links==
* 
* 

 
 
 
 
 
 


 