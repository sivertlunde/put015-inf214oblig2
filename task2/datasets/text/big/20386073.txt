303 Fear Faith Revenge
{{Infobox Film
| name           = 303 Fear Faith Revenge
| image          = 303_fear_faith_revenge_poster.jpg
| caption        = Thai Film Poster
| director       = Somching Srisupap
| producer       = Visute Poolvoralaks
| writer         = Cher Kori
| narrator       = 
| starring       = Taya Rogers  Ananda Everingham Artid Ryu
| music          = 
| cinematography = Taweesak Kumphati
| editing        = Mahasak Tassanapayak   
| distributor    = Tai Entertainment
| released       =  
| runtime        = 90 minutes
| country        = Thailand
| language       = Thai
| budget         = 
}}   Thai horror film starring Taya Rogers, Artid Ryu, and Ananda Everingham. In this film, an investigation into a star students suicide results in several students being stranded in their closed school where they are hunted and killed one-by-one.

==Cast==
*Ananda Everingham as Ghu 
*Adam Shekleton as Reptilicant
*Artid Ryu as Chaidan 
*Taya Rogers as Numkang 
*Prinya Intachai as Pongkhet 
*Paul Carey as Traisoon 
*Songwut Sricherdchutm as Sihnsamut 
*Rerng-Rit Wisamon as Joe 
*Chart-Yodom Hiranyadthiti as Tee 
*Suchao Pongwilai as Father 
*Micheal Pupart as Brother Wiwat 
*Areewan Chatutong as Miss Orasha 
*Charlie Sungkawess as Mr. Swaeng (as Chalie Sang-Kawes) 
*Apichat Choosakul as Mr. Choo 
*Thanalarp Somprakop as Dej 
*Chuchart Silpajarn as Cherd

==External links==
*  

 
 
 


 