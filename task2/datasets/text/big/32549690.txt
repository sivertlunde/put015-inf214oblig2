Family Life (1971 British film)
{{Infobox film
| name = Family Life
| image = 
| caption =
| director = Ken Loach
| producer = Tony Garnett
| writer = David Mercer
| story =
| based on =
| starring = Sandy Ratcliff Malcolm Tierney Grace Cave
| music = Marc Wilkinson
| cinematography = Charles Stewart
| editing = Roy Watts
| studio = EMI Films|Anglo-EMI  Kestrel Films
| distributor = MGM-EMI (UK theatrical) Cinema 5 Distributing (US theatrical)
| released =   
| runtime = 108 minutes
| country = United Kingdom
| language = English
| budget =£180,000 Alexander Walker, Hollywood, England, Stein and Day, 1974 p.381  
| gross = $1,827,374SEK ($291,648USD)
}} David Mercer. Wednesday Play series first transmitted by the BBC in March 1967, which was also written by Mercer and directed by Loach, 

==Plot==
A young woman, Janice, is living with her restrictive and conservative parents, who lead a dull working-class life, and consider their daughter to be "misbehaving" whenever shes trying to find her own way in life. When she becomes pregnant, they force her into abortion, and hypocritically blame her for "upsetting them" when she is unable to cope with the emotional and mental effect this has on her. Janice is subjected to shockingly self-righteous and ignorant doctors.

==Cast==
*Sandy Ratcliff ... Janice Baildon
*Bill Dean ... Mr. Baildon
*Grace Cave ... Mrs. Baildon
*Malcolm Tierney ... Tim
*Hilary Martin ... Barbara Baildon
*Michael Riddall ... Dr. Donaldson

==Production==
Half the budget was provided by the   on 2 October 1971.

==Awards==

===Won===
*1972 Berlin International Film Festival:
**FIPRESCI Prize – Forum of New Film: Ken Loach
**Interfilm Award – Forum of New Cinema: Ken Loach
**OCIC Award – Forum of New Film: Ken Loach
*French Syndicate of Cinema Critics 1974:
**Critics Award – Best Foreign Film: Ken Loach (UK)
*Sydney Film Festival 2003:
**Audience Award – Best Feature-Length Fiction Film: Ken Loach

===Nominated===
*BAFTA Awards 1973:
**UN Award – Best Film

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 


 