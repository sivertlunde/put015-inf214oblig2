Happy Husbands (2010 film)
 
{{multiple issues|
 
 
}}

{{Infobox film
| name           = Happy Husbands
| image          = Happy_Husbands.jpg
| alt            =  
| caption        = Movie poster
| director       = Saji Surendran
| producer       = Milan Jaleel
| screenplay     = Krishna Poojappura
| story          = Sakthi Chidambaram Indrajith Bhavana Bhavana Samvrutha Sunil Vandana Menon Rima Kallingal Suraj Venjaramoodu
| music          = M. Jayachandran
| cinematography = Anil Nair
| editing        = Manoj
| photographer   = Mahadevan Thampi
| distributor    = Anantha Vision
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 5.65 crores  
| gross          = 14 crores  
}}
 Malayalam comedy film directed by Saji Surendran and starring Jayaram, Jayasurya, Indrajith Sukumaran|Indrajith, Samvrutha Sunil, Bhavana Menon|Bhavana, Rima Kallingal, Vandana Menon, and Suraj Venjaramoodu. The film was declared as blockbuster hit of 2010.
 Charlie Chaplin. 

==Plot summary==
Mukundan Menon (Jayaram) runs a magazine called Kerala Today. He is a normal husband who loves his wife Krishnendu (Bhavana Menon|Bhavana) a lot, but she is too possessive about him. She thinks he is not very expressive about his feelings, and is always worried that he might one day fall for some other beautiful girl.

John Mathai (Jayasurya) is a photographer in Mukundans publication. He was neither interested in girls nor keen to get married until he met Sereena (Vandana).

Rahul (Indrajith Sukumaran|Indrajith) is just the opposite. He is extremely romantic and never comes home without some cute gifts for his wife, Shreya (Samvrutha Sunil). But she doesnt know that, behind her back, her darling husband is dying to flirt with every woman that he sees.

It is into the lives of these three couples that a bar singer, Diana (Rima Kallingal), arrives and things take some interesting turns. Diana is seeking to take revenge on Mukundan Menon since it was on his article in Kerala Today that the Maharashtra court took as a petition and then banned the bar girls in Maharasthra thus ruining Dianas profession.

Diana forcibly comes to Mukundan Menons house one day with her evil intentions and that catches the eyes of Krishnendu. To escape from trouble, Mukundan Menon says that she is the wife of John. Later, when John, Mukundan and Rahul were talking about Johns marriage proposal to Serenas dad, Diana comes there by accident and there Rahul says that she is Mukundans wife and she is mentally imbalanced. So now Krishnendu and Shreya thinks Diana is Johns wife and Johns real wife Serneena believes that Diana is Mukundans wife. At one time, all these people come together at Malaysia for a vacation and that leads to a lot of confusion among the wives and others. What happens next forms the rest of the story.

==Cast==
* Jayaram as Mukundan Menon Indrajith as Rahul Vallyathan
* Jayasurya as John Mathai
* Bhavana Menon as Krishnendu
* Samvrutha Sunil as Shreya
* Vandana Menon as Sereena
* Rima Kallingal as Diana Philip
* Suraj Venjaramood as Raj Boss (Rajappan)
* Salim Kumar as Satyapalan / Dharmapalan
* Maniyanpilla Raju as Pazhakutti Pavithran

==Music==
The soundtrack for this film was composed by M. Jayachandran.
# "Etho Poonilakalam" - Resmi Vijayan
# "Happy Husbands" - Indrajith Sukumaran|Indrajith, Anand Narayan, Achu Rajamani
# "Take It Easy" - Achu Rajamani

==Criminal case for copyright infringement==
For the first time in the Malayalam film industry, a criminal case was filed under the Copyright Act, 1957, against the alleged copying of the script of the Hindi film, No Entry, frame to frame. In the criminal case filed before the Judicial First class Magistrates Court in Trivandrum, the producer, director, writer and artists Jayaram, Jayasurya, Bhavana, Reema Kallungal, and Samwrutha Sunil are the accused. The artists, according to the complainant are offenders for abetting the crime. The Court has, as on March 23, 2012, taken the complaint to the file and is proceeding with it. The complainant, it is learnt, alleges that the script of the movie is a scene by scene blind copy of the Hindi movie. The alleged offence, if proved, would attract a minimum sentence of six months that may go up to two years and fine, as provided under the Copyright Act.

==Controversy==
Jayaram courted controversy during a promotional interview for the movie by describing a domestic help as a "dark, fat, buffalo-like Tamil woman", following which various organisations and activists demanded an unconditional apology from him. Jayaram said this when he was asked whether his wife doubts him in the way Jayarams character was doubted by his wife in the movie. Jayarams house in Chennai was attacked by Tamil activists and the actor deeply regretted his comments.

==Reception and box office==
Upon release, the film went to become a biggest commercial success of 2010 and successfully completed 150 days.

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  

 
 
 
 
 