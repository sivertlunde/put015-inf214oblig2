Torn (2009 film)
{{Infobox film
| name           = TORN
| image          =
| alt            = DVD cover of main character
| caption        = "TORN DVD cover Richard Johnson Corey Williams
| starring       = Chris Clanton Jace Nicole Marat Mostovoy Tyeisha Gibson Mia Barnes Trinetta Wright Tony Folden Paul Wiedecker Richard Johnson
| studio         = GoldenTiger Productions
| distributor    = R-Squared Films
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = English
}} Richard Johnson. This was his debut feature length narrative film.

== Production == Corey Williams Richard Johnson auditioned all actors for the film. The filmed is based off events that took place in St. Louis.

== Synopsis ==
A mans inability to see how his actions affect everyone around him and how the results of those actions could be the end of them all.

==Cast==
* Chris Clanton as Torn Pettigrew
* Jace Nicole as Barbie
* Tyeisha Gibson as Monica
* Marat Mostovoy as Richard
* Tony Folden as Max
* Trinetta Wright as Tiphany Pettigrew
* Mia Barnes as Tay Tay
* Paul Wiedecker as Private Investigator
* Torian Tarrant as Ken
* Monet Rovanell as Angie
* Tiphany Johnson as Zelda
* Mandy Tenly as Neighbor
* Billie Ruth-Bailey as Secretary

==Release==
TORN was released on DVD and Video on Demand on December 8, 2009.

== External links ==
*http://www.imdb.com/title/tt1569543/
*http://www.goldentigerproductions.com/about_us
*http://www.exploreharford.com/news/3062/shot-harford/
*http://www.exploreharford.com/news/3371/harford-movies/
*http://www.rsquaredfilms.com/films/torn.html
*http://www.allmovie.com/work/torn-500601
*http://www.amazon.com/Torn-Chris-Clanton/dp/B002NEODTA/ref=sr_1_1?s=dvd&ie=UTF8&qid=1287584463&sr=1-1
*http://www.amazon.com/Torn/dp/B003DZ2MTQ/ref=sr_1_22?s=digital-video&ie=UTF8&qid=1287584491&sr=1-22
*http://www.goldentigerproductions.com/torn_movie_stills_drama

 


 