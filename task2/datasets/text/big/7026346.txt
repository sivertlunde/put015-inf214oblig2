So Happy Together (film)
{{Infobox film
| name           = So... Happy Together
| image          = 
| alt            = 
| caption        = 
| film name      = 
| director       = Joel Lamangan
| producer       = Roselle Y. Monteverde
| writer         =  Ricardo Lee
| story          =  
| based on       =  
| starring       =  
| narrator       = 
| music          = Vincent de Jesus
| cinematography = Rolly Manuel
| editing        = Marya Ignacio
| studio         = Regal Films Regal Home Video
| released       =  
| runtime        = 76 minutes
| country        = Philippines
| language       = Tagalog 
| budget         = 
| gross          = 
}} Ricardo Lee, directed by Joel Lamangan, and produced by Regal Films.   |date=November 21, 2004}}  Led by Kris Aquino and Eric Quizon in their second film together,   |date=November 23, 2004}}  the films supporting stars include Tonton Gutierrez, Gloria Diaz, Cogie Domingo, Nova Villa, and Jay-R.   |date=December 20, 2004}}    |date=December 9, 2004}} 

==Plot==
After meeting during the 1980s at the first gay pride parade in Malate, the talkative Lianne (Kris Aquino) and the colorfully gay Osmond (Eric Quizon) become fast friends.  Over the next 30 years, the two do everything together... dining, shopping, discussing life, and even looking for the perfect man. The two became so inseparable, that even their mothers had become best friends as well. The Lianne and Osmond friendship lasts for decades, even beyond Liannes becoming a mother to two teenage daughters and until Osmonds death.

==Cast==
 
* Kris Aquino as Lianne 
* Eric Quizon as Osmond 
* Tonton Gutierrez as  Erwin 
* Gloria Diaz as Daisy 
* Cogie Domingo as  Oliver 
* Nova Villa as  Inday 
* Jay-R as Brent 
* Mark Herras as  Xander 
* Jennylyn Mercado as Wena 
* Yasmien Kurdi as  Ceraphieca 
* Yasmien Kurdi as Raphie
* Rainier Castillo as Miles 
* Linda Gordon as Diane
* Miguel Garcia as Mike
* Carlo Maceda as  Gordon 
* Paolo Paraiso as  Randy 
* Clint Pijuan as  Benjie 
* Richard Quan as  AJ 
* Douglas Robinson as  Hugh Dakma 
* Jon Romano as  Jay
* Jojo Vinzon as Violet
 

==Recognition==
* 2004, Eric Quizon won Golden Screen Award at Metro Manila Film Festival Philippines for "Best Actor" for his role of Osmond. 
 
===Reception===
Manila Bulletin praised the film, writing that "Happy Together is a funfilled comedy drama" and "the most unexpected and surprising movie of the season."   |date=December 22, 2004}} 

==References==
 

== External links ==
*   archived September 11, 2005

 
 
 
 
 
 
 
 