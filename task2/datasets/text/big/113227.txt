Mister Magoo's Christmas Carol
 
{{Infobox film| name =Mister Magoos Christmas Carol
  | image    = MagooChristmas.jpg
  | caption  = DVD Cover for Mister Magoos Christmas Carol
  | director = Abe Levitow
  | producer = Lee Orgel, executive producer Henry G. Saperstein
  | based on       =  
  | writer   = Freely Adapted from Charles Dickens, by Barbara Chain
  | starring = Jim Backus Morey Amsterdam Jack Cassidy Royal Dano Paul Frees
  | music    = Music by Jule Styne,
Lyrics by Bob Merrill,
Music Scored and Conducted by Walter Scharf   
  | cinematography =
  | editing  = UPA (1962-2000) Classic Media (2000-2012) DreamWorks Animation (2012-present)
  | released = December 18, 1962
  | runtime  = 53 min English
  | budget   =
  }} The Spirit animated holiday program ever produced specifically for television, originally airing in December 1962, {{cite web
  | last = Hill
  | first = Jim
  | authorlink =
  | coauthors =
  | title =  Scrooge U: Part VI -- Magoos a musical miser
  | work =
  | publisher = JimHillMedia.com
  | date = November 28, 2006
  | url = http://jimhillmedia.com/editor_in_chief1/b/jim_hill/archive/2006/11/28/christmas-carol-vi.aspx
  | doi = Rudolph the Red-Nosed Reindeer was first shown in December 1964. The special also inspired the 1964 TV series The Famous Adventures of Mr. Magoo.  It featured the voice of Jim Backus as Magoo, with voice-over appearances by Paul Frees, Morey Amsterdam, Joan Gardner, and Jack Cassidy.

==Overview== UPA animation studio in its declining days. Commissioned and sponsored by Timex Group USA|Timex, it first aired on NBC on December 18, 1962. {{cite web
  | last = Kurer
  | first = Ron
  | authorlink =
  | coauthors =
  | title = The Nearsighted Mister Magoo
  | work = Toon Tracker
  | publisher =
  | date = 2007-10-25
  | url = http://www.toontracker.com/magoo/magoo.htm
  | doi =
  | accessdate = 2007-12-21 }}  Although the special led to the Famous Adventures of Mr. Magoo television series, the studio ultimately found it could not adapt to the rigors of mass-producing cartoons for television.
 The CW acquired the broadcast rights to the special for the 2014 season.

==Reception== Funny Girl soon after their work on the special. 
As recently as December 25, 2006, many listeners told the National Public Radio program Talk of the Nation that Mister Magoo was their favorite Ebenezer Scrooge. {{cite web
  | last = Conan
  | first = Neil (host)
  | authorlink = Neil Conan
  | coauthors =
  | title = Choose Your Favorite Scrooge
  | work = Talk of the Nation
  | publisher = National Public Radio
  | date = 2006-12-25
  | url = http://www.npr.org/templates/story/story.php?storyId=6673835
  | format = audio
  | doi =
  | accessdate = 2007-01-03 }} 

==Comparisons with the Charles Dickens story== Tiny Tim. English characters American actors British accents.

==Comparison with the book==
The credits for the cartoon state that it is "freely adapted" from A Christmas Carol by Charles Dickens. This adaptation mostly serves to shorten the story to fit the television specials one-hour time slot. The Ghost of Christmas Present appears before the Ghost of Christmas Past, and no reference is made to Scrooges nephew Fred or the metaphorical children Ignorance and Want. {{cite web
  | last =  Howe
  | first = Tom
  | authorlink =
  | coauthors =
  | title =  Charles Dickens A Christmas Carol and Scrooge
  | work = Featured CED VideoDisc No. 26 - Fall 2002
  | publisher = CED Magic
  | year = 2002
  | url = http://www.cedmagic.com/featured/christmas-carol/christmas-carol.html
  | doi = Patrick Stewarts made-for-TV version.  A number of references to Scrooges (more accurately Magoos) poor vision are sprinkled through the story, a nod to the Magoo character, but except for the beginning and ending pieces which occur outside the framework of the Dickens story, there are none of the usual Magoo catastrophes.

==Cast of voices==
{| class="wikitable" style="width:50%;"
|- "
! Actor !! Role
|-
| Jim Backus || Ebenezer Scrooge Mr. Magoo
|-
| Morey Amsterdam || James  Brady
|-
| Jack Cassidy || Bob Cratchit
|-
| Royal Dano || Jacob Marley|Marleys Ghost
|-
| Paul Frees || Charity Man Fezziwig Old Joe The Undertaker Stage Director Top Hat Man
|- Joan Gardner Tiny Tim Ghost of Christmas Past {{cite web
  | last =
  | first =
  | authorlink =
  | coauthors =
  | title = Mister Magoos Christmas Carol Cast & Crew List
  | work = The Big Cartoon Database
  | publisher =
  | date =
  | url = http://www.bcdb.com/cartoon_characters/20046-Mister_Magoos_Christmas_Carol.html
  | doi =
  | accessdate =  2007-12-21}} 
|- John Hart || Billings Stage Manager Milkman
|-
| Jane Kean || Belle
|-
| Marie Matthews || Young Scrooge
|-
| Laura Olsher || Mrs. Cratchit
|-
| Les Tremayne || Ghost of Christmas Present
|-
|}
Note that sources differ on credits for the Eye Patch Man, Laundress, Charwoman, 3 Cratchit children at table, Dick Wilkins, Boy that gets turkey, and the Ghost of Christmas Past,  with June Foray sometimes credited for female voices. {{cite web
  | last =
  | first =
  | authorlink =
  | coauthors =
  | title = June Foray Biography (1919-)
  | work = Theatre, Film, and Television Biographies
  | publisher = Film Reference.com
  | date =
  | url = http://www.filmreference.com/film/6/June-Foray.html
  | doi =
  | accessdate =  2007-12-21}}  {{cite video
  | people =
  | title = Biographies of the Voice Talents, Featuring Jim Backus
  | medium = DVD special feature
  | publisher = SONY
  | location =
  |date= 2002 }}  However, Foray has stated on several occasions that she was not in the show at all.    Other sources credit Joan Gardner as the Ghost of Christmas Past.

==Songs==
The cartoons framing device consists almost entirely of Jim Backus as Quincy Magoo singing "Its Great To Be Back On Broadway", thus explaining in song that the character Magoo is portraying a character in a Broadway theatre production. 

"Ringle, Ringle", Scrooges theme song about "coins when they mingle", is half-sung by Jim Backus as Magoo, and serves to delineate the characters change of heart. Initially he appreciates the coins aesthetically and for the wealth they represent, while Jack Cassidy as Bob Cratchit sings  in counterpoint that "its cold, its frightfully cold", and musically begs Scrooge to spare the expense of "just one piece of coal" to warm him. Later, in a musical reprise, Scrooge sings that the coins are "meant for passing around" as he spends the coins to help the Cratchit family.

Joan Gardner as Tiny Tim ("played" by the animated character Gerald McBoing-Boing) sings of "razzleberry dressing" and "woofle jelly cake." in "The Lords Bright Blessing". The Cratchit childrens requests for better food, a tree and presents are countered by Jack Cassidy as Bob Cratchit singing of what the family has, in his view: "the Lords bright blessing, and knowing were together" - a togetherness that Scrooge lacks. 

In the Christmas Past sequence, Backus/Magoo as Scrooge sings in poignant duet with Scrooges younger self (sung by Marie Matthews),  left behind in boarding school after all the other children have gone home for Christmas.  "In perhaps the most touching moment... Magoo is transported back to his childhood, where he stands side-by-side with his youthful self. He watches his child self sing  Alone in the World, tracing his hand on the blackboard, hoping to find a hand of his own to hold... the quavering elderly voice blending with the clear, sweet youthful one, the invisible Magoo putting a transparent arm around his child  ."
 
::A hand for each hand was planned for the world
::Why dont my fingers reach?
::Millions of grains of sand in the world
::Why such a lonely beach?
:::: (Lyrics source  .) 

Jane Kean as Belle, once beloved of young Scrooge, sings of the cooling and hardening of his feelings toward her in "Winter Was Warm", a song of lost and yearning love. She is saddened that he has chosen gold over her, and he protests that that is the "way of the world", as he forlornly tries to cling to her.  Broadcast television airings starting from the late 1980s cut the scene in which Belle sings "Winter Was Warm", despite the fact that the theme permeates the score as bridging music.  Additionally, a chorus rendition of "Winter Was Warm" during the end credits was replaced by a reprise of "The Lords Bright Blessing".

Veteran voice actor Paul Frees sings two roles in "Were Despicable (Plunderers March)". The laundress, charwoman, and the undertaker go to Old Joes rag & bone shop to sell the items that they have taken from the newly deceased miser "with him lyin there", and gleefully cackle their way through such lyrics as, "Were rep-re-hensible. Well steal your pen-and-pencible".

Backus sings a short reprise of "Alone in the World" in the scene where the Ghost of Christmas-Yet-to-Come abandons Scrooge in the cemetery.

For a finale, Scrooge and the Cratchits sing a reprise (with happier lyrics) of "The Lords Bright Blessing".

A longstanding story suggests that "People (1964 song)|People" was originally written for Mr. Magoo,  but Theodore Taylors biography of Styne disputes this. {{cite book
  | last = Taylor
  | first = Theodore
  | authorlink = Theodore Taylor (author)
  | coauthors =
  | title = Jule: The Story of Composer Jule Styne
  | publisher = Random House
  | year = 1979
  | location =
  | pages =
  | url =
  | doi =
  | isbn = 0-394-41296-6 }} 

The Cleveland Plain Dealer also disputes this, saying

"Producer Lee Orgel heard Styne playing the piano. He thought the song was sensational and asked if it was a solo for Magoos Scrooge. Styne and Merrill told him it was for "Funny Girl." It was People. which would become a huge hit for Barbra Streisand." 

==Releases==
All home videos are Aspect Ratio: 1.33:1, Rating G (MPAA certificate #20330), Running time 52 or 53 minutes (exclusive of any extra features).
* US Television Release Date: December 18, 1962
* Production Company: United Productions of America (UPA)
* Available Formats: VHS • Laserdisc • DVD • VHS: Clam Shell • VHS: Slip Sleeve
----
*1970 Theatrical release; United Productions of America (UPA)/Maron Films Limited 
:Mr. Magoos Holiday Festival "Matinee Only" Motion Picture Double Feature:
::Mr. Magoos Christmas Carol
::Mr. Magoos Little Snow White
*1977; Book version adapted from the cartoon feature by Horace J. Elias. 64 pages.
* September 28, 1994; VHS, Paramount Home Video, NTSC, ASIN: 6301175239, UPC: 097361269337
* January 1, 1999; Laserdisc
* October 23, 2001; VHS, Good Times Video, NTSC, ASIN: B00005OLAZ
:UPC: 018713074584 (VHS Clam Shell); UPC: 018713766656 (VHS Slip Sleeve) ISBN 0-7662-0858-3
* October 23, 2001; DVD, Good Times Video, Region 1, ASIN: B00005OLB3, UPC: 018713812391
:DVD Features: Audio: English, mono Dolby; A history of Mister Magoo in film and TV; Retrospective of composer Jules Styne & lyricist Bob Merrill; Movie poster; Bonus Magoo cartoon short
* September 24, 2002; VHS, Sony Wonder (Video), NTSC, ASIN: B00006HB09, UPC: 074645433332
* September 24, 2002; DVD, Sony Wonder (Video), Region 1, ASIN: B00006HAWI, UPC: 074645433394
:DVD Features: Available Audio Tracks: English (Dolby Digital 2.0); A history of Mr. Magoo in film and TV; Career retrospective of the composer and lyricist; Poster; "Mr. Magoo Meets Gerald McBoing Boing" short
* September 14, 2004; DVD, Sony Wonder (Video), Region 1, ASIN: B0002I82ZA, UPC: 074645864198/073892864104
:Format: Color, Dubbed, Original recording remastered, NTSC; Run Time: 60 minutes

* Sep 4, 2007; DVD; Classic Media   
* Nov 10, 2008; DVD; UCA 
* Nov 16, 2010; Blu-ray/DVD; Classic Media; 2 Discs - Collectors Edition 
* Oct 4, 2011; DVD; Classic Media

==In popular culture==
Baltimore-based rock group Animal Collective titled their debut album Spirit Theyre Gone, Spirit Theyve Vanished after a line in the movie, spoken by Mr Magoo.

==See also==
* List of A Christmas Carol adaptations
* List of ghost films
* List of animated feature films

==References==
 

==External links==
*  on Internet Movie Database.
*  on JuleStyne.com.
* 
*  on Big Cartoon Database
* 
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 