Giants vs. Yanks
{{Infobox film
| name           = Giants vs. Yanks
| image          =
| image_size     =
| caption        =
| director       = Robert F. McGowan
| producer       = Hal Roach
| writer         = Hal Roach H. M. Walker
| narrator       = Jack Davis William Gillespie Fanny Kelly Joseph Morrison
| music          =
| cinematography =
| editing        =
| distributor    = Pathé
| released       =  
| runtime        = 20 minutes
| country        = United States
| language       = Silent film English intertitles
| budget         =
}}

Giants vs. Yanks is the 12th Our Gang short subject comedy released. The Our Gang series (later known as "The Little Rascals") was created by Hal Roach in 1922, and continued production until 1944.

==Plot==
While the boys are playing their baseball game, a couple living nearby offer to take care of Jacks baby sister, Imogene. Eventually, the gang goes into their house, when they realize theres a litter of puppies in there. By this time, however, the doctor has realized that one of the servants has a contagious disease and decides to quarantine the place, giving the gang a chance to turn into a disaster area.

==Notes== Mickey rigs the treadmill to turn the laundry. In a couple of scenes, the goat is walking on the treadmill, but the laundry is not turning. Ernie is batting.
*Mary Kornman does not appear in this film.
*When the television rights for the original silent Pathé Our Gang comedies were sold to National Telepix and other distributors, several episodes were retitled. This film was released into TV syndication as Mischief Makers in 1960 under the title "The Little League". About two-thirds of the original film was included. This film was also released as an episode of "Those Lovable Scallawags with Their Gangs" series under the title "Giants vs. Yanks". About two-thirds of the original film was included. Deleted scenes from this print include the entire sequence involving Ernie, Farina, and Squeaky attempting to retrieve a live turkey. Some of the original inter-titles were left intact.

==Cast==
===The Gang===
* Joe Cobb as Squeaky
* Jackie Condon as Squealer Jack Davis as Jack, alias Bugle Nose Davis
* Mickey Daniels as Mickey, alias Ironman Mickey
* Allen Hoskins as Farina
* Ernie Morrison as Ernie
* Doris Oelze as Imogene, Jacks sister
* Dinah the Mule as Herself

===Additional cast===
* Andy Samuel as Cooty Martin, one of the Yanks
* Roy Brooks as The plainclothes officer
* Frank Coghlan Jr. as One of the Yanks
* Beth Darlington as Mrs. Reddy a.k.a. Mr. Wife
* Dick Gilbert as Hungry Hogan William Gillespie as Mr. Reddy a.k.a. Mr. Husband
* Clara Guiol as The Maid who becomes ill
* Florence Hoskins as The other maid
* Wallace Howe as Doctor
* Joseph Morrison as Ernie and Farina’s father

==See also==
* Our Gang filmography

== External links ==
*  
*  

 
 
 
 
 
 
 
 