The Coast Guard (film)
{{Infobox film
| name         = The Coast Guard 
| image        = The Coast Guard movie poster.jpg 
| caption      = The Coast Guard movie poster 
| writer       = Kim Ki-duk 
| starring     = Jang Dong-gun Park Ji-a 
| director     = Kim Ki-duk  
| producer     = Kim Dong-joo Lee Seung-jae 
| cinematography = Baek Dong-hyeon 
| editing        = Kim Sun-min 
| distributor  = Korea Pictures Cineclick Asia
| released     =  
| country      = South Korea
| runtime      = 94 minutes 
| language     = Korean
| music        = 
| budget       =  
| gross        =    . Box Office Mojo. Retrieved 2011-12-24. 
| film name      = {{Film name
| rr             = Haeanseon
| mr             = Haeansŏn}}
}} 2002 South Korean film directed by Kim Ki-duk. The film deals with military atrocities and the absurdities of borders and conflicts.

==Plot== South Korean Marine Corps who is eager to shoot a North Korean spy. One evening he shoots and kills a South Korean civilian who has strayed into a forbidden zone to have sex with his girlfriend. Kang and the girlfriend of the dead civilian both have mental breakdowns. The woman believes the members of the coast guard are her dead lover, and engages in sexual affairs with them. Though commended after the shooting, Kang is dismissed from the coast guard. He then returns to kill other members of his troop. In the end he goes to Seoul and kills people at random with his bayonet. 

==Awards==
===Wins=== FIPRESCI Prize: Kim Ki-duk, "For the strong and innovative depiction of the illusion of power which destroys humanity on both sides of the fence."  NETPAC Award: Kim Ki-duk Town of Karlovy Vary Award: Kim Ki-duk

===Nominations===
* Crystal Globe: Kim Ki-duk

==Notes==
*  
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 


 