The Cry of the Owl (1987 film)
{{Infobox film
| name           = The Cry of the Owl
| image          = The Cry of the Owl Poster.jpg
| image_size     = 
| caption        = 
| director       = Claude Chabrol
| producer       = Antonio Passalia
| writer         = Claude Chabrol Odile Barski Patricia Highsmith (novel)
| narrator       = 
| starring       = Christophe Malavoy Mathilda May Virginie Thévenet
| music          = Matthieu Chabrol
| cinematography = Jean Rabier
| editing        = Monique Fardoulis
| studio         = Civite Casa Films Italfrance Films TF1
| distributor    = Delta
| released       = October 28, 1987
| runtime        = 102 minutes
| country        = France Italy
| language       = French 
| budget         = 
}} Italian thriller film, adapted from the 1962 novel The Cry of the Owl by Patricia Highsmith. The film was directed by Claude Chabrol and stars Christophe Malavoy, Mathilda May and Virginie Thévenet.

Divorced illustrator Robert spies on a young woman named Juliette who he envies for her seemingly happy life. When they finally meet and Juliette leaves her fiancé Patrick for Robert, the situation quickly escalates.

==Plot==
Parisian illustrator Robert becomes obsessed with a young woman, Juliette. Night after night, Robert sneaks around the house to catch a glimpse of Juliette, until one day he finally gathers the courage to introduce himself. Juliette realizes that she is not happy with her fiancé, Patrick, and leaves him to be with Robert. Robert in turn is not happy with Juliettes obtrusive advances.

One night, Patrick attacks Robert in a deserted area; Robert defends himself and knocks Patrick unconscious, leaving him on the shores of a nearby river. The next day, Robert is interrogated by the police. Patrick is missing, and suddenly Robert is the prime suspect. Nobody knows that Patrick has allied with Roberts bitter ex-wife, Véronique, to take revenge on Robert; he is hiding from the police in order to make them think Robert killed him. Roberts professional and private life falls apart after becoming a police suspect. The situation escalates when Juliette commits suicide, and Patrick launches a vendetta against Robert. In a final confrontation between the men, Véronique is accidentally killed, and although Patrick is defeated, Robert is again left as a suspect in an apparent crime scene.

==Cast==
* Christophe Malavoy as Robert
* Mathilda May as Juliette
* Jacques Penot as Patrick
* Jean-Pierre Kalfon as Police commissioner
* Virginie Thévenet as Véronique
* Patrice Kerbrat as Marcello
* Jean-Claude Lecas as Jacques
* Agnès Denèfle as Suzie
* Victor Garrivier as Doctor
* Jacques Brunet as Father
* Charles Millot as Director
* Yvette Petit as Neighbor
* Dominique Zardi as Neighbor
* Henri Attal as Cop
* Albert Dray as Cop
* Nadine Hoffmann as Josette
* Gérard Croce as Cop
* Isabelle Charraix as Mme Tessier
* Laurent Picaudon as Boy
* Christian Bouvier as Maître dhôtel
* Gilles Dreu as M. Tessier

==Release== US premiere on October 16, 1991 in New York City. Although released on VHS in many countries, the American DVD is the only available digital release for the home media market at present (2011).

==Other adaptations== German writer-director Der Schrei der Eule. 
 third film adaptation written and directed by Jamie Thraves and starring Julia Stiles and Paddy Considine was released in 2009. 

==Awards and nominations==
*César Awards (France) Most Promising Actress (Mathilda May) Best Actor &ndash; Supporting Role (Jean-Pierre Kalfon)

== References ==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 