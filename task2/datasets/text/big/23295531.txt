Allt flyter
{{Infobox Film
| name           = Allt flyter
| image          = Allt flyter.jpg
| image_size     = 
| caption        = Swedish cover
| director       = Måns Herngren
| producer       = 
| writer         = Jane Magnusson Måns Herngren Brian Cordray
| narrator       = 
| starring       = 
| music          = Ebbot Lundberg
| cinematography = Henrik Stenberg
| editing        = Fredrik Morheden
| distributor    = Nordisk Film AB (2008) (Sweden)
| released       =  
| runtime        = 
| country        = Sweden
| language       = Swedish
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} Swedish film directed by Måns Herngren.

== Plot summary ==
Fredrik discovers synchronised swimming and recruits some friends to compete in an international competition for men.

== Cast ==
*Jonas Inde as Fredrik
*Amanda Davin as Sara
*Andreas Rothlin-Svensson as Charles
*Jimmy Lindström as Larry
*Peter Gardiner as Victor
*Benny Haag as Peter
*Shebly Niavarani as Börje
*Kalle Westerdahl as Markus
*Maria Langhammer as Lillemor

== External links ==
* 
* 

 
 
 
 
 
 
 

 