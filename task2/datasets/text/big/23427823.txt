In Prison My Whole Life
{{Infobox film
|  name     = In Prison My Whole Life
|  image            =Inprisonmywholelife.jpg
|  director         = Marc Evans
|  writer           = Marc Evans William Francome
|    starring         = Mumia Abu-Jamal Boots Riley Noam Chomsky Mos Def Snoop Dogg Steve Earle William Francome Russell Simmons Alice Walker Howard Zinn
|  producer         = Livia Giuggioli Nick Goodwin Self
| studio      = Fandango Nana
|  editor           = Mags Arnold
|  released         =  
|  runtime          = 99 minutes
| country           = United Kingdom
|  language         = English
|  budget           = 
}}
In Prison My Whole Life is a 2007 documentary film about Mumia Abu-Jamal, directed by Marc Evans, and written by Evans and William Francome. The title comes from the fact that Abu-Jamal was arrested in December 1981, on the day Francome was born. Others involved with the project were Angela Davis, Anthony Arnove, Dead Prez, Howard Zinn, Mos Def, Noam Chomsky, Robert Meeropol, Russell Simmons, Snoop Dogg and Steve Earle.   The films executive producer is Colin Firth.
 Black Panther death sentence for killing a police officer was overturned in 2001 due to technical errors made during his original 1982 sentencing hearing. The film investigates Americas civil rights history and its justice system through Jamals court case. 

==Awards==
The film was nominated in 2008 for World Cinema - Documentary with the award of Grand Jury Prize at the Sundance Film Festival.  

The film premiered in France in October 2008  and was subsequently released by the festival in more than 20 towns.

At the 2009 International Festival of Cinema on Human Rights in Paris, In Prison... won the Student Award and the Planet Award.  The film was released nationwide in France on November 23, 2011. 

==Reception== Time Out critic Tom Huddleston said of the film, "Sadly, My Little Eye director Marc Evans’s doc is only tangentially about Jamal – instead, he chooses to focus on William Francome.   Francome is an uninteresting central figure, unconnected to the case or the history of civil rights, to which the pair present a sort of Idiots Guide|Idiot’s Guide midway through. But Evans keeps him front and centre throughout   making ‘In Prison My Whole Life’ just another self-absorbed agit-prop documentary." 

Jay Weissberg of Variety (magazine)|Variety called it an "unfocused, oddly naive" film, and that the editing "is overly dependent on computer tricks, and use of verbal loops to reinforce lines treats the audience like idiots." 

On the opposite, Le Monde calls the film "exciting", the editing being a "deliberated melting-pot", resulting in an overwhole "captivating portrait of Mumia Abu-Jamal". 

==See also==
*Interviews Before Execution

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 