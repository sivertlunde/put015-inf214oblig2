A Lonely Place for Dying
{{Infobox film
| name           = A Lonely Place for Dying
| image          = Lonely place for dying.jpg
| caption        = Theatrical release poster
| director      = Justin Eugene Evans
| producer       = Justin Eugene Evans Brent Daniels
| executive producer       = James Cromwell
| screenplay         = Justin Eugene Evans
| starring       = Ross Marquand  Michael Wincott  Michael Scovotti  James Cromwell Brad Culver Mike Peebler Luis Robledo Jason R. Moore
| music         = Brent Daniels
| cinematography = Justin Eugene Evans
| editing        = Brad Stoddard
| studio       =Humble Magi
| distributor    =Humble Magi LPD, LLC
| released       =   
| runtime        = 94 minutes
| country        = United States
| language       = English
| budget         = $200,000 USD 
}}
A Lonely Place for Dying is a 2012  American independent drama-thriller film directed by Justin Eugene Evans and produced by James Cromwell. Starring Ross Marquand, Michael Wincott, Michael Scovotti and James Cromwell, the film is set in 1972 during the Cold War. The films score was composed by Brent Daniels.

==Plot==
It is 1972. An abandoned Mexican prison sits alone in the dusty Chihuahua desert. KGB mole Nikolai Dzerzhinsky waits for his contact from the Washington Post. Dzerzhinsky holds explosive evidence against the CIA; information he will trade for asylum in the United States. Special Agent Robert Harper must obtain this evidence and kill Dzerzhinsky or risk the end of his CIA career. As the two men hunt each other, they discover that the sins from their past destined them for this deadly confrontation.

==Cast==
* Ross Marquand as KGB Officer Nikolai Dzerzhinsky
* Michael Scovotti as CIA Agent Robert Harper
* Michael Wincott as CIA Project Manager Anthony Greenglass
* James Cromwell as Washington Post Editor-In-Chief Howard Simons
* Brad Culver as Special Forces Captain Robert "Bob" Altman
* Mike Peebler as Special Forces First Lieutenant George Roy Hill
* Luis Robledo as Special Forces Staff Sergeant Solares
* Jason R. Moore as Special Forces Staff Sergeant William "Bill" Friedkin
*Stephen Jules Rubin as Special Forces Buck Sergeant Konigsberg

==Release==
"A Lonely Place For Dying" held an unannounced sneak preview at the 2008 Santa Fe Film Festival. Despite being an out-of-competition rough-cut screening with no visual effects, music, sound mix or color grade, it was nominated for three awards and won the Heineken Red Star Award.  

The first 22 minutes of the motion picture was released on VODO in July of 2011. It was downloaded over one million times and became the best-seeded movie Torrent in the world for several weeks.  "A Lonely Place For Dying" screened at its final three festivals and concluded its two-and-a-half-year-long festival run while "part one" of the film dominated Bittorrent. 

After nearly three years on the festival circuit, "A Lonely Place For Dying" received a limited theatrical release on approximately 19 screens throughout the United States. It debuted on September 7th, 2012 at the Al Ringling Theatre in Baraboo, Wisconsin  and concluded its run on November 4th of the same year in several theaters in Portland, Oregon. The film played in theaters across Wisconsin, Idaho, Montana, Ohio, Oregon and Washington.  

In February, 2013, it was released on iTunes and Amazon.  On May 31st, 2013, the filmmakers held a charity screening of "A Lonely Place for Dying" at Justin Eugene Evans high school in Clackamas, Oregon. All proceeds went to Clackamas High Schools speech and drama departments.  

==Accolades==
"A Lonely Place for Dying" was an official selection of 46 film festivals where it was nominated for 53 awards and won 29 including 18 as best picture.   The film began its festival run at the 2008 Santa Fe Film Festival and concluded its run as the Opening Night film for the 2011 Oaxaca Film. Fest  Below is a partial list of the films awards.

{| class="wikitable plainrowheaders sortable"
|- style="vertical-align:bottom;" Award
!scope="col"|Date of Ceremony Category
!scope="col"|Recipient(s) and nominee(s) Result
!scope="col"|Ref
|- Santa Fe Film Festival December 7, 2008 Heineken Red Star Award Justin Eugene Evans
| 
| 
|-
| Eugene International Film Festival
| October 10, 2010 Best Action Narrative Justin Eugene Evans
| 
| 
|- Somewhat North of Boston (S.N.O.B.) Film Festival
| November 10, 2010
| Best Action Film Justin Eugene Evans
| 
| 
|-
| Park City Film Music Festival
| June 10, 2010
| Silver Medal for Excellence In Original Music
| Brent Daniels
| 
| 
|- Wild Rose Independent Film Festival
| rowspan="10" | November 6, 2010
| Best Visual Effects
| Marc Leonard & Daniel Broadway
| 
| 
|-
| Best Cinematography (Certificate of Distinctive Achievement)
| Justin Eugene Evans
| 
| 
|-
| Best Makeup and Hair
| Catherine Doughty
| 
| 
|-
| Best Music
| Brent Daniels
| 
| 
|-
| Best Editing
| Brad Stoddard
| 
| 
|-
| Best Lighting Design
| Justin Eugene Evans
| 
| 
|-
| Best Ensemble
| The Cast of ALPFD
| 
| 
|-
| Best Actor
| Ross Marquand
| 
| 
|-
| Best Director - Feature Film
| Justin Eugene Evans
| 
| 
|-
| Best Feature Film
| Justin Eugene Evans
| 
| 
|- Maverick Movie Awards 2010
|Best Cinematography
| Justin Eugene Evans
| 
| 
|- Best Actor
| Ross Marquand
| 
| 
|- Best Supporting Actor
| Michael Wincott
| 
| 
|- Best Production Design
| Justin Eugene Evans, Jan Shane & Catherine Doughty
| 
| 
|- Best Special Effects
| Daniel Broadway, Marc Leonard & Mystic Arts
| 
| 
|- Best Special Effects Make-Up
| Catherine Doughty
| 
| 
|- Best Stunts
| Rick Kingi & Kurly Tlapayawa
| 
| 
|- The Precious
| James Cromwell
| 
| 
|- Beloit International Film Festival February 19, 2011 Best Screenplay Justin Eugene Evans
| 
| 
|- Best Feature
| Justin Eugene Evans
| 
| 
|- Indie Spirit Film Festival April 17, 2011
|Directors Choice Award Justin Eugene Evans
| 
| 
|- Van Wert International Film Festival July 10, 2011 Grand Prix Justin Eugene Evans
| 
| 
|- Outstanding Direction Justin Eugene Evans
| 
| 
|- Outstanding Actor Ross Marquand
| 
| 
|- Outstanding Film Editing Brad Stoddard
| 
| 
|- Burbank International Film Festival September 21, 2011 Best Feature Film Justin Eugene Evans
| 
| 
|-
| Best Picture Justin Eugene Evans
| 
| 
|- Oaxaca FilmFest November 12, 2011 Opening Night Film
| Justin Eugene Evans
| 
| 
|}

==Reception==
A Lonely Place for Dying was met with almost universal praise. Justine Browning of Huffington Post said "A Lonely Place for Dying (currently available on iTunes) is a powerful independent thriller from a promising new director Justin Eugene Evans."  Andrew L. Urban of Urban Cinefile said "Executed with skill, the film uses its primary element - the abandoned Mexican prison - to terrific effect...excellent design and music, with terrific camerawork, complete what is arguably a better movie than many which get a theatrical release."  Jesse Veverka of the Clyde Fitch Report refers to the films director as an "independent narrative film prodigy."  Charles Monroe Kane of WPTs "Directors Cut" said "A Lonely Place For Dying is an absolutely fantastic film!"  

==References==
 

==External links==
*  
* 
*  
*  
*  
*  
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 