The Big Fight (1930 film)
 
{{Infobox film
| name           = The Big Fight
| image          = 
| caption        = 
| director       = Walter Lang
| producer       =  Walter Woods
| starring       = Lola Lane
| music          = 
| cinematography = 
| editing        = 
| distributor    = Sono Art-World Wide Pictures
| released       =  
| runtime        = 65 minutes
| country        = United States
| language       = English
| budget         = 
}}

The Big Fight is a 1930 American drama film directed by Walter Lang, and released by Sono Art-World Wide Pictures.    

==Cast==
* Lola Lane as Shirley
* Ralph Ince as Chuck Guinn Williams as Tiger (as Guinn Williams)
* Stepin Fetchit as Spot
* Wheeler Oakman as Steve
* James Eagles as Lester (as James Eagle)
* Robert Emmett OConnor as Detective (as Robert E. OConnor)
* Edna Bennett as Winnie
* Tony Stabenau as Battler (as Tony Stabeneau)
* Larry McGrath as Pinkie
* Frank Jonasson as Berrili

==Preservation status==
This film is now considered a lost film.

==See also==
*List of lost films

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 