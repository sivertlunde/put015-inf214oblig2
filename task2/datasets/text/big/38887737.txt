Sadda Haq (film)
 
 

{{Infobox film
| name           = Sadda Haq

| image          = http://www.sikhnet.com/files/news/2013/04-April/SaddaHaqPoster3.jpg
| director       = Mandeep Benipal 
| producer       = Kuljinder Singh Sidhu Dinesh Sood Nidhi Sidhu
| writer         = Kuljinder Singh Sidhu
| starring       = Kuljinder Singh Sidhu Dhriti Saharan Gaurav Kakkar Parmod Moutho etc.
| music          = Jatinder Shah
| cinematography = Rishi Singh
| studio         = OXL Films Mumbai
| released       =  
| country        = India, United Kingdom, Canada, United States, Australia, Germany, and Italy.
| language       = Punjabi
| budget         =  
| gross= 
}}
 Punjabi movie based in the late 1980s and early 1990s during the Punjab insurgency. It portrays what prompted young men in the state to rise up and fight against a corrupt police and government system. The film evoked polarising reactions even before it was released, with sections of the Sikh community saying that it depicted the real face of police atrocities. Critics on the other hand felt that the movie was a mere "glorification" of terrorists. 

==Plot==
The movie tells the story of Sharon Gill played by Dhriti Saharan , a modern-day Canadian Sikh graduate student who travels to India as part of her thesis on minorities at war. Revelations about the past soon leave the student curious for more information, and Gills awareness changes after maneuvering into prison to interview Kartaar Singh. Singh is a hockey player whose life experiences lead him to fight against corruption in the police and government systems. The films central story is based around Kartaar Singhs journey which is filled with courage, perseverance, and faith.

==Cast==
*Kuljinder Singh Sidhu as Kartar Singh Baaz (base off of Jagtar Singh Hawara)
*Dhriti Saharan as Sharan Gill
*Gaurav Kakkar as Karan Randhawa 
*Parmod Moutho as JPS Randhawa (base off of KPS Gill)
*Yadd Grewal as Hansu Kadayanawala (base off of Sumedh Singh Saini and Ajit Poohla)
*Dev Kharoud as Rajwant Singh Raja (base off of Balwant Singh Rajoana)
*Dinesh Sood as Joravar Singh (base off of Dilawar Singh Babbar)
*Amritpal Singh Billa as Baba Tarsem Singh
*Sudanshu Ghor as Prathamjit Singh (base off of Paramjit Singh Bheora)
*Talwinder Singh Bhullar as Dr Lakhbir Singh Sabhi (base off of Gurmeet Singh Engineer)
*Neeraj Kaushik as Jailer
*Sandeep Kapoor CBI Officer Chetri
*Nidhi Sidhu
*Sunny Gill as Home Minister Baldev Singh (base off of Chief Minister Beant Singh)
*Praveen Kumar as Santokha Kala
*Prince Kj Singh as Gopi
*Himanshi Khurana

==Release and reception==
The movie was released in Australia, United Kingdom, America and Canada but the release was postponed in India. 

Originally, the film was to be released in December 2009  but was delayed due to the Censor Boards disapproval and was finally slated for worldwide release on 5 April 2013. The film was cleared by the Film Certification Appellate Tribunal (FCAT),  but on 5 April, the movie was banned in Punjab, Chandigarh, Haryana, Delhi and J&K. The ban was lifted 10 May 2013.

* The producer and lead actor Kuljinder Sidhu, termed the ban "shameful" and a "black spot on democracy".  Kuljinder Sidhu has decided to move court to get the ban revoked. Sidhu said the ban was arbitrary because the censor board had cleared the film with a U/A certificate.
* Punjabi actor Gippy Grewal came forward to ask for the ban to be removed. 
* All India Sikh Students Federation president Karnail Singh Peermohammad condemned Shiromani Gurdwara Parbandhak Committee for its U-turn on Punjabi movie Sadda Haq issue. He said SGPC has been misleading people and latest statement of committee president Avtar Singh Makkar it was wrong to support the movie proves that SGPC has lost its credibility. 
* Shiv Sena demanded ban on the movie, as according to them it may impacts Hindu Sikh relations in Punjab and threatened to go on strike. 
* A delegation of Punjab Mahila Congress (PMC) and Hindu organisations, led by the state vice-president of PMC, Nimisha Mehta, Saturday demanded a permanent ban on the Punjabi film Sadda Haq its trailers, songs, promos and promotional activities, which they alleged glorified the Khalistan movement.  The film is being heavily promoted by Khalistani militant organizations in Canada   

* The Canadian Sikh Coalition is batting for the film not only on its official website but also on its Facebook page where it projects itself as a "political organisation". The organization promoted and distributed the movie in Canada. 
* Chief Minister of Punjab, Parkash Singh Badal told reporters that "It is our priority to maintain peace and communal harmony in the state and we dont want that the movie should vitiate the communal atmosphere of the state" 
* In Delhi, A senior police official said Lieutenant Governor Tejinder Khanna issued the ban order under Section 13 of Cinematography Act to maintain communal harmony. 
* Senior Punjab BJP leader Manoranjan Kalia on Sunday lauded the Badal government for its timely action to ban the controversial film Sadda Haq in the interest of hard-earned peace in the state. Kalia said it was quite strange that the Central Board of Film Certification had failed to do its job (of banning the movie) despite the fact that the union ministry of information and broadcasting is headed by Manish Tewari, "who himself belongs to a martyrs family as his father Prof VN Tiwari was among those whose sacrifices helped in bringing peace to Punjab".
* On Thursday, 11 April, the Supreme Court refused to lift the ban on Bollywood movie Sadda Haq which has been barred from screening in Punjab, Haryana, Uttar Pradesh and in many other places.  
* Om Puri said the ban should be lifted if the movie is about the true facts. 
* The Supreme Court of India lifted the ban after proper screening of the movie in court no. 15 on Friday, 26 April 2013.  The movie will be released with an A certification on 10 May.  Manish Tiwari wanted to ban this movie. 

==Global public response==

Canada

The Canadian Sikh Coalition (CSC) is the official distributor and representative of the "Sadda Haq" team across Canada.  "Sadda Haq" is set to become the highest grossing Punjabi film to ever hit theatres in Canada 

==Multilingual dubbing==
The producers are in the process of dubbing the film in Hindi and English, in order to make it accessible to a wider audience. "We got a lot of demand from overseas as well as different parts of India to dub the movie. The dubbing has already started and the DVD rights and satellite rights for the film will be given in both these languages. The translated version may also be screened in a few cinema halls," Kuljinder Singh Sidhu producer of the movie declared during a press conference. 


Got PTC Awards.Best actor and actress in critical role
==References==
 

==External links==
* https://www.facebook.com/saadahaqmovie Sadda Haq Facebook.
*http://www.saddahaqmovie.com Sadda Haq Official Website

 
 
 
 