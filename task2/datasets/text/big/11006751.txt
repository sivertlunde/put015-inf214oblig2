The Man from Planet X
 
{{Infobox film
| name           = The Man from Planet X
| image          = The Man from Planet X.jpg
| image_size     = 250px
| caption        = theatrical poster
| director       = Edgar G. Ulmer
| producer       = Jack Pollexfen Aubrey Wisberg
| writer         = Aubrey Wisberg Jack Pollexfen
| starring       = Robert Clarke Margaret Field William Schallert
| music          = Charles Koff
| cinematography = John L. Russell
| editing        = Fred R. Feitshans Jr.
| distributor    = United Artists
| released       = March 9, 1951 (San Francisco) April 7 (NYC) April 27 (general)
| runtime        = 70 minutes
| country        = United States
| language       = English
| budget         = $51,000 (est.) Monroe Specifications Named for Karamazov,; Lean Offers Ford Film
Schallert, Edwin. Los Angeles Times (1923-Current File)   October 22, 1956: A11.  
| gross          = $1.2 million 
}}

The Man From Planet X is a 1951 American science fiction film.     starring Robert Clarke, Margaret Field and William Schallert. It was directed by Edgar G. Ulmer.

==Plot==
A spaceship from a previously unknown planet lands in the Scottish moorland|moors, bringing an alien creature to earth near the observatory of Professor Elliot (Raymond Bond), just days before the planet will pass closest to the earth.  When the professor and his friend, American reporter John Lawrence (Robert Clarke), discover the creature, they help it when it is in distress and try to communicate with it, but fail.  They leave, and the alien follows them home.  A colleague of the professor, the unscrupulous and ambitious scientist Dr. Mears (William Schallert), discovers how to communicate with the creature and tries to get from it by force the formula for the metal the spaceship is made of.  He shuts off the aliens breathing apparatus and leaves it for dead, telling the professor that communication was hopeless.

Soon, Lawrence discovers that the alien is gone, as is the professors daughter, Enid (Margaret Field). Tommy, the village constable (Roy Engle), reports that others from the village are missing as well.  Lawrence takes the constable to the site where the spaceship has been, but it is no longer there.  With more people now missing &ndash; including Mears &ndash; the phone lines dead and the village in a panic, they get word to Scotland Yard by using a heliograph to contact a passing freighter.

When an Inspector (David Ormont) and sergeant fly in and are briefed on the situation, it is decided that the military must destroy the spaceship.  Lawrence objects that doing so will also kill the people who are under the aliens control.  With the mysterious planet due to reach its closest distance to the earth at midnight, Lawrence is given until 11:00 to rescue them.  He sneaks up to the ship, and learns from Mears that the alien intends the ship to become a wireless relay station in advance of an invasion from its home planet, which is dying.  Lawrence orders the enthralled villagers to leave and attacks the alien, shutting off its breathing apparatus, then escapes with Enid and the professor.  Mears, however, returns to the ship and is killed when the military destroys the ship, just before the planet approaches and then recedes back into outer space.

==Cast==
*Robert Clarke as John Lawrence
*Margaret Field as Enid Elliot
*Raymond Bond as Professor Elliot
*William Schallert as Dr. Mears
*Roy Engel as Tommy the Constable Charles Davis as Georgie, man at dock
*Gilbert Fallman as Dr. Robert Blane
*David Ormont as Inspector Porter
*June Jeffery as Wife of missing man
*Franklyn Farnum as Sgt. Ferris, Porters assistant (uncredited)

Cast notes
*Actor Pat Goldin and dwarf actor Billy Curtis have both been rumored to be the unknown actor who played the role of the alien space visitor in the film.      However, Robert Clarke, who is frequently named as the source of the Pat Goldin rumour, never actually knew the name of the actor who played the role of the alien, nor did the other cast members, including Margaret Field and William Schallert. Johnston, John, Cheap tricks and Class Acts: Special Effects, Makeup, and Stunts from the Films of the Fantastic Fifties, Jefferson, N.C: McFarland & Co. Inc. Publishers, ISBN 0-7864-0093-5 (1996) pp. 224-225   , TCM.com, retrieved December 19, 2011   Furthermore, the unknown actor who played the role was noticeably taller than Billy Curtis.  Cast member Robert Clarke recalls only that the actor who played the part of the alien in the film was of Jewish origin, stood about five feet tall, and was once part of an acrobatic vaudeville act.   Margaret Field and producer Jack Pollexfen later recalled only that he had complained about his uncomfortable costume and his low pay,    while William Schallert remembered him only as a very small, interesting-looking middle-aged man who wasnt much of an actor. 
*Robert Clarke was paid $350/week for his work on this film. McGee,Scott and Stafford, Jeff   

==Production== Joan of Arc, using fog to change moods and locations. TCM    

==In popular culture== Invaders from The War The Day the Earth Stood Still is a close parallel, finishing production six months before this one, in the summer of 1951.
*The alien can only communicate using modulated musical sounds, a concept used three decades later in Close Encounters of the Third Kind.
*The alien appears alongside other film monsters in the 2003 film  , in the scene that occurs at  .

==References==
Notes
 

==External links==
*  
*  
*  
*  at Trailers from Hell

 

 

 
 
 
 
 
 
 
 
 