The Favourite Son
Le fils préféré (  film directed by Nicole Garcia and written by François Dupeyron, Jacques Fieschi and Garcia. It stars Gérard Lanvin, Bernard Giraudeau	and Jean-Marc Barr.

==Cast==
*Gérard Lanvin	 ...	Jean-Paul Mantegna
*Bernard Giraudeau	 ...	Francis
*Jean-Marc Barr	 ...	Philippe
*Roberto Herlitzka	 ...	Raphaël, le père
*Margherita Buy	 ...	Anna Maria
*Pierre Mondy	 ...	Le dentiste usurier
*Karin Viard	 ...	Martine
*Antoinette Moya	 ...	Odetta

==Awards and nominations==
*César Awards (France)
**Won: Best Actor &ndash; Leading Role (Gérard Lanvin)
**Nominated: Best Actor &ndash; Supporting Role (Bernard Giraudeau)
**Nominated: Best Director (Nicole Garcia)
**Nominated: Best Film
 Karlovy Vary Film Festival (Czech Republic)
**Nominated: Crystal Globe (Nicole Garcia)

==External links==
*  

 
 
 
 
 
 


 