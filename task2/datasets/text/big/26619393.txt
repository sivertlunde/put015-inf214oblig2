Re-Kill
{{Infobox film
| name           = Re-Kill
| image          = ReKill poster.jpg
| alt            =  
| caption        = Theatrical poster
| director       = Valeri Milev
| producer       = Stephanie Caleb Moshe Diamant Christopher Milburn Courtney Solomon
| writer         = Michael Hurst
| starring       = Scott Adkins Bruce Payne Daniella Alonso
| music          = 
| cinematography = Anton Bakarski
| editing        = Valeri Milev
| studio         = Midsummer Films After Dark Films
| distributor    = After Dark Films
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} directed by written by Michael Hurst. 

==Cast==
* Scott Adkins as Parker
* Daniella Alonso as Matthews
* Raicho Vasilev as Elvis
* Bruce Payne as Winston
* Roger Cross as Sarge
* Jesse Garcia as Hernandez
* Rocky Marshall as Langford
* Layke Anderson as Tom Falkirk
* Dimiter Doichinov as Adams
* Yo Santhaveesuk as Nyguyen
*   as Male Anchor
*  Cat Tomeny as News Anchor

==Synopsis==
It’s been 5 years since the outbreak that wiped out 85% of the world’s population, but the war between Re-Animates (Re-Ans) and Humans wages on. Most of the major cities are still uninhabitable. Within the few surviving cities, the Re-Ans have been segregated into “zones” and are policed by the R-Division of the QUASI S.W.A.T. Unit who hunt to re-kill the Re-Ans in the hope of quelling a second outbreak.

“R-Division, Frontline” is America’s #1 TV show in this Post-Apocalyptic world.  Weekly, the show follows different R-Division units on the battlefield as they work to keep America Safe and Re-An free. Jimmy, the videographer for the Outbreak Network, thought this week’s episode would be like every other. He was wrong.  Through the lens of his eyes and camera, we see raw, revealed and uncut, that which could be mankind’s last day. 

==Production==
The film is part of the eight film series After Dark Films Originals.  The After Dark Horrorfest 8 Films to Die For  brand, After Dark Films produced in cooperation with Lionsgate and NBC Universal’s Syfy the film.  The shot will begin in July 2010 in Sofia, Bulgaria  and is directed by Bulgarian filmmaker Valeri Milev. 

==Release== After Dark Originals Film Festival. 

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 