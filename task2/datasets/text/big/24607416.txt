Get Thrashed
{{Infobox Film
| name           = Get Thrashed
| caption        = Get Thrashed: The Story of Thrash Metal.
| image	=	Get Thrashed FilmPoster.jpeg
| director       = Rick Ernst
| producer       = Rick Ernst Rat Skates
| writer         = 
| starring       = Lars Ulrich Sully Erna Frank Bello Death Angel Dave Mustaine
| music          = 
| cinematography = 
| editing        = Rick Ernst Rat Skates
| distributor    = Vivendi Entertainment Lightyear Entertainment 
| released       =  
| runtime        = 100 minutes
| country        = United States
| language       = English
}}
Get Thrashed is a 2006 American documentary directed by Rick Ernst and starring Lars Ulrich, Sully Erna, Frank Bello, Death Angel, Dave Mustaine and many others ( ).

==Plot==
Get Thrashed traces the impact of thrash metal. It is the story of the hardest metal music of the 1980s and 1990s as told by the bands who lived it.

==Sources==
* 
*http://www.getthrashed.com/
*http://www.metroactive.com/metro/08.08.07/exodus-0732.html
*http://stylusmagazine.com/articles/movie_review/get-thrashed-the-story-of-thrash-metal.htm
*http://www.rottentomatoes.com/m/get_thrashed/

 
 
 
 
 


 