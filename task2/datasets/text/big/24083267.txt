Your Highness
 
 
{{Infobox film
| name           = Your Highness
| image          = Your Highness Poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = David Gordon Green
| producer       = Scott Stuber
| writer         = {{Plainlist|
* Danny McBride Ben Best}}
| starring       = {{Plainlist| 
* Danny McBride
* James Franco
* Natalie Portman
* Zooey Deschanel}}
| narrator       = Charles Shaughnessy
| music          = Steve Jablonsky
| cinematography = Tim Orr
| editing        = Craig Alpert Stuber Pictures Universal Pictures
| released       =  
| runtime        = 102 minutes  
| country        = United States
| language       = English
| budget         = $50 million   
| gross          = $28 million 
}} fantasy comedy Ben Best, the film was released on April 8, 2011. 

The film received generally negative reviews by critics and grossed slightly more than half its $50 million budget.

==Plot==
Thadeous and Fabious are sons of King Tallious in the Kingdom of Mourne. They are warriors: Fabious is dashing and skilled and Thadeous is lazy and ineffectual. While celebrating his latest victory over the evil sorcerer, Leezar, who has been ravaging Talliouss kingdom, Fabious introduces the virgin Belladonna whom he has freed from a tower and wishes to marry. Though his brother makes him the best man, Thadeous skips the wedding after overhearing Fabiouss Knights Elite, led by Boremont, talk about him negatively. The wedding is then crashed by Leezar, who reveals himself to be the one who placed Belladonna in the tower. Leezar re-kidnaps her and flees. Returning to the castle with his servant Courtney, Thadeous is given an ultimatum: join Fabious on his quest to rescue Belladonna or be banished from Mourne.

Visiting the Great Wize Wizard, Thadeous and Fabious learn that Leezar is attempting to fulfill a prophecy of a warlock having sex with a maiden when the two moons converge, impregnating her with a dragon that will allow him to take over King Talliouss kingdom. To destroy Leezar, they are given a magic compass that will lead them to the Blade of Unicorn, located within a labyrinth. On the way there, they learn Fabiouss slave, Julie, has been reporting to Leezar of their progress and that the Knights Elite are serving the warlock. Fabious sends his mechanical bird Simon to tell the king of the betrayal by the Knights Elite and request reinforcements. Thadeous, Fabious and Courtney are captured by nymphs under their leader, Marteetee, who imprisons them at an arena, where Fabious kills off Marteetees finest warrior. In retaliation, Marteetee summons a Lernaean Hydra|hydra-like monster to kill them. The brothers are rescued by Isabel, a warrior seeking revenge for her fathers murder at Marteetees hands.

Later that night, Thadeous learns that Isabel is also after Leezar for the slaughter of her brothers. The next day, the party learns too late that Isabel steals the compass from Thadeous. Fabious decides to find the Blade of Unicorn alone as Thadeous and Courtney go to a tavern, where they find Isabel and steal the compass back. But finding that his brother has been captured by Leezars men, Thadeous wins Isabel over as they join forces, entering the labyrinth where they encounter a Minotaur. After becoming separated from the others, Thadeous retrieves the Blade of Unicorn and slays the Minotaur. Thadeous and his group make their way to Leezars castle and free Fabious, giving him the Sword of Unicorn. As the others kill off Julie, Boremonts men and Leezars three witches, Fabious impales Leezar with the Blade of Unicorn, to prevent him from raping Belladonna.

After their victory, Isabel leaves for another quest and the heroes return home. Fabious and Belladonna marry, while Thadeous is approached by Isabel, who reveals that she has fallen in love with him. However, for them to have sex, he must first slay the witch who has cast a spell on her, locking her in a chastity belt. Though not in the mood to go out, Isabels suggestion convinces him to go on a new adventure.

==Cast==
 
* Danny McBride as Prince Thadeous
* James Franco as Prince Fabious
* Natalie Portman as Isabel
* Zooey Deschanel as Belladonna
* Justin Theroux as Leezar
* Toby Jones as Julie
* Damian Lewis as Boremont
* Rasmus Hardiker as Courtney
* Simon Farnaby as Manious the Bold
* Deobia Oparei as Thundarian
* Charles Dance as King Tallious
* John Fricker as Marteetee
* Matyelok Gibbs, Angela Pleasance and Anna Barry as Helinda, Sariah and Marlyne, Leezars three Mothers
* Charles Shaughnessy as Narrator / Soul of the Labyrinth
* Amii Grove as "Forest Woman"
 

==Production== Filming began in the summer of 2009 in Northern Ireland and concluded in October 2009. 

==Marketing==
A Trailer (promotion)#Rating cards|red-band trailer was released on IGN and Funny or Die.  On December 21, 2010, a green-band trailer was released online,  and shown before screenings of Little Fockers and The Dilemma. 

On March 23, 2011, a second red-band trailer was released. 

==Reception==
===Box office===
Your Highness opened on April 8, 2011 in 2,772 theaters nationwide and made $9,360,020 in its opening weekend, ranking number six in the domestic box office. By the end of its run, the film had grossed $21,596,445 in the United States and Canada and $6,417,288 overseas for a worldwide total of $28,013,733.   

===Critical reception===
The film received negative reviews from critics. On Rotten Tomatoes, the film holds a rating of 27%, based on 159 reviews. The sites consensus reads, "Big budgets and costumes in service of scatalogical jokes may seem funny on paper, but in execution this is a highly monotonous romp that registers only occasional laughs."  On Metacritic, the film has a score of 31 out of 100, based on 33 critics, indicating "generally unfavorable reviews". 
 lowbrow humor is "sometimes witless and sometimes winning comedy...begins with grade-school-level graffiti being scrawled across storybook pages and goes up and down from there. Still, the fun can be infectious...a farce within a farce...tawdry tale of the bothered and bewildered Kingdom of Mourne". 

  principles. Like members of some post-Dadaist Artist collective|collective, the filmmakers have dedicated themselves to memorializing every first, wrong impulse that popped into their heads, while ruthlessly excising any vestige of wit or narrative niceties as being too linear, dude." 
 Razzie nomination Worst Supporting Jack and Jill. 
 Pineapple Express, they started talking about making a sequel of their own films. One of them suggests Your Highness (making fun of it) by saying "How do we NOT do Your Highness 2?"

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 