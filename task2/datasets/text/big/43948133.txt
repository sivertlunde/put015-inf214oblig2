Ammayenna Sthree
{{Infobox film
| name           = Ammayenna Sthree
| image          =
| caption        =
| director       = KS Sethumadhavan
| producer       =
| writer         = KT Muhammad
| screenplay     = KT Muhammad Sathyan K. Ragini
| music          = A. M. Rajah
| cinematography = Melli Irani
| editing        = MS Mani
| studio         = Chithranjali
| distributor    = Chithranjali
| released       =  
| country        = India Malayalam
}}
 1970 Cinema Indian Malayalam Malayalam film, Ragini in lead roles. The film had musical score by A. M. Rajah.   

==Cast==
 
*Prem Nazir Sathyan
*K. R. Vijaya Ragini
*Jayabharathi
*Kaviyoor Ponnamma
*Adoor Bhasi
*Thikkurissi Sukumaran Nair
*Sankaradi Raghavan
*T. S. Muthaiah
*Bahadoor
*K. P. Ummer
*Paravoor Bharathan
*Thodupuzha Radhakrishnan
 

==Soundtrack==
The music was composed by A. M. Rajah and lyrics was written by Vayalar Ramavarma and Traditional.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aadithyadevante Kanmani || P Susheela || Vayalar Ramavarma || 
|-
| 2 || Aalimali Aattinkarayil || P Susheela || Vayalar Ramavarma || 
|-
| 3 || Amma Pettamma || Jikki || Vayalar Ramavarma || 
|-
| 4 || Madyapaathram Madhurakaavyam || K. J. Yesudas || Vayalar Ramavarma || 
|-
| 5 || Nale Ee Panthalil || A. M. Rajah || Vayalar Ramavarma || 
|-
| 6 || Pattum Valayum || A. M. Rajah || Vayalar Ramavarma || 
|-
| 7 || Thamasso ma Jyothirgamaya || PB Sreenivas || Traditional || 
|}

==References==
 

==External links==
*  

 
 
 


 