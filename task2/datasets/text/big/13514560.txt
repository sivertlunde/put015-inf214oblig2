Adventureland (film)
{{Infobox film
| name           = Adventureland
| image          = Adventurelandposter.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Greg Mottola
| producer       = Sidney Kimmel Anne Carey Ted Hope
| writer         = Greg Mottola
| starring       = Jesse Eisenberg Kristen Stewart Ryan Reynolds Martin Starr Bill Hader Kristen Wiig
| music          = Yo La Tengo
| cinematography = Terry Stacey
| editing        = Anne McCabe
| studio = Sidney Kimmel Entertainment
| distributor    = Miramax Films
| released       =  
| runtime        = 107 minutes  
| country        = United States
| language       = English
| budget         = 
| gross          = $17.2 million 
}}
Adventureland is a 2009 American comedy-drama film written and directed by Greg Mottola, starring Jesse Eisenberg and Kristen Stewart and co-starring Ryan Reynolds, Bill Hader, Martin Starr, and Margarita Levieva. The film is set in the summer of 1987 when recent college grad James Brennan (Jesse Eisenberg) is making big plans to tour Europe and attend graduate school in pursuit of a career in journalism. However, financial problems force him to look for a summer job instead of traveling abroad, which places him at Adventureland, a run-down amusement park in western Pennsylvania. There he meets Emily Lewin (Kristen Stewart), a co-worker with whom he develops a quick rapport and relationship.

Released on April 3, 2009, the film received mostly positive reviews and earned $17.1 million worldwide at the box office. Adventureland was less successful than Greg Mottolas previous film, the 2007 box-office hit Superbad, with a smaller release on 1,862 screens. It was nominated for "Best Ensemble Cast Performance" at the 19th Annual Gotham Independent Film Awards.

==Plot==
In 1987, James Brennan (Jesse Eisenberg) has two plans. The first plan is to have a summer vacation in Europe after graduating with a comparative literature degree from Oberlin College. His second plan is to attend a journalism graduate school at Columbia University when his holidays end. Unexpectedly, his parents (Wendie Malick and Jack Gilpin), announce they wont be able to help him financially and advise him to seek a part-time job, rather than going to Europe.

James gets a job at Adventureland, a local amusement park in his hometown of  ); Bobbys wife and park manager Paulette (Kristen Wiig); Sue OMalley (Paige Howard); Mark (Mark Miller); the alluring Lisa P. (Margarita Levieva); and the parks technician, Mike Connell (Ryan Reynolds), a part-time musician. Another games worker, Emily "Em" Lewin (Kristen Stewart), saves James from being stabbed by a lying-cheating customer.

With her father and stepmother away, Em has a party and gets to know James.  James and Em have fun in the houses swimming pool. After Em leaves the pool, Frigo humiliates James by announcing to the party that James has an erection while leaving the pool, causing an embarrassed James to jump back into the water. After the party, Connell, who has been having an affair with Em, comes over to further pursue it.

Later that week, James goes for a drink with Em and she is surprised to learn James has never had sex. They share a first kiss. The following day, James tells Connell about his strong feelings for Em, which Connell reports to her. Em tells James she wants to take things slow due to problems in her life, leaving James confused and upset. Sue drunkenly makes out with Joel, but rejects him the next day, saying her Catholic parents would not allow her to date a Jew; outraged, Em calls Sue an anti-Semitism|anti-Semite in front of other staff members. Lisa P. asks James on a date, but he has mixed feelings because of his relationship with Em. After Connell talks him into going, he accepts Lisa P.s offer.

After the date, during which Lisa and James kiss, James learns Em had called to say she regrets having rejected his feelings. Joel later sees James and Em walking together and, irritated by the chain of events, quits. James unsuccessfully tries to talk him out of it, and Joel reveals hes angry at James for dating Lisa P. when James is already in love with Em. James tells Em about going out with Lisa P. After hearing of this, Em goes to Connells mothers home to end their affair. The parks mentally challenged parking attendant, prompted by Tommy, tells James he saw Em and Connell doing "pushups without any pants on" in the back of Connells car.

James goes to Connells mothers house, which is where Lisa P. told him Connell takes girls to have sex, and sees Em leaving. Shocked to see James, Em becomes tongue-tied. James leaves angrily, and Em cries. James tells Lisa P. about the affair and asks her not to tell anyone, but she tells her friend Kelly. Upon noticing that all the park employees know, Em quits and moves back to New York. A heartbroken James drunkenly crashes his fathers car into a tree and passes out. The next morning, his mother angrily wakes him up and tells him he has to pay to repair it with his summer earnings.

Now, without enough money for graduate school, James nonetheless heads to New York City with his parents blessing, and waits outside Ems apartment. She is reluctant to talk to him, feeling she has ruined everything. James tells her he sees her in a different way than she sees herself. Touched, Em brings James up to her apartment. James reveals he is not going to Columbia this year, and considers next year. He gets out of his wet clothes and finds Em still has an Adventureland shirt. They kiss and begin to take their clothes off. James asks, "Are we doing this?", and Em says, "I think so."

==Cast==
 
* Jesse Eisenberg as James Brennan, a recent college graduate who majored in Comparative Literature and Renaissance Studies. He is an aspiring journalist who has to get a summer job so he can afford to go to Columbia University in New York. The only job he can get is in the games at the Adventureland theme park. He is a virgin and occasional marijuana smoker. Throughout his summer working in Adventureland, Brennan develops a relationship with his co-worker, Emily "Em" Lewin.
* Kristen Stewart as Emily "Em" Lewin, one of James Adventureland co-workers and love interest. She has a troubled home life; she hates her stepmother and resents her father for remarrying so soon after her mothers death from cancer. She is a student at NYU. She doesnt need to work, as her father is a rich lawyer, but she works in the games at the theme park to get out of the house. She is secretly having an affair with the parks maintenance man, Mike Connell, but eventually calls it off, and quits Adventureland after her affair becomes public knowledge.  jammed with Lou Reed. Through slips in his musical knowledge, it becomes apparent to James that this is not true. He has cheated on his wife with different girls and, by the beginning of the story, with Em.
* Martin Starr as Joel, one of James co-workers at the games in Adventureland and James friend in the summer. A college graduate in Russian literature and Slavic languages, a degree which he claims will get him a job as a "cabbie, hot dog vendor," or "marijuana delivery guy." He eventually quits the park after being attacked by a disgruntled customer and leaves Pittsburgh to head for an undisclosed location. He enjoys smoking a pipe, although he calls it a "revolting affectation." Matt Bush as Tommy Frigo, James childhood best friend; though they arent good friends any more, James still manages to tolerate Frigos immaturity and cruelty. Frigo constantly teases James and hits him in the testicles, repeatedly, often bringing him to his knees in pain. He even goes so far as to blackmail James about his date with Lisa P. At the end of the film, James hits Frigo in the testicles first for once before heading off to New York, finally getting his revenge.
* Margarita Levieva as Lisa P., one of the rides operators at the Adventureland park. A seductive girl to whom all the parks employees are attracted. Her father was injured and is now unable to work, and she bonds with James after he shows some sympathy for her. She invites James out on a date, but he leaves her to go back with Em. She eventually tells her friends at the park that Em is having an affair with Connell, a secret she told James she would not reveal.
* Bill Hader as Bobby, the eccentric assistant manager of Adventureland. He is extremely intolerant of people who litter but is very supportive of his employees and caring for his wife.
* Kristen Wiig as Paulette, Bobbys wife and manager of Adventureland. She is often as eccentric as her husband but is much more quiet.
* Mary Birdsong as Francy, Ems stepmother. She married Ems father shortly after the death of Ems mother. After her first divorce, she lost her hair from the stress of a nervous breakdown and wears a wig. Em says that if she were not a "status-obsessed witch",  she might feel bad for her.
* Josh Pais as Mr. Lewin, Ems father and Francys husband. He met Francy at temple while his first wife was slowly dying in the hospital. He married Francy, soon after Ems mother died.
* Jack Gilpin as Mr. Brennan, James father. He loses his job and transfers to a different office. He is an alcoholic, but he tries to hide his drinking from his family. James finds a bottle of scotch underneath his dads car seat, and he drinks the bottle while driving and crashes into his neighbors tree. He doesnt tell his mother that the bottle belonged to his father.
* Wendie Malick as Mrs. Brennan, James mother. She is the one that lets James know they cant help them pay for his trip or graduate school. She supplies the discipline of the two parents. However at the end of the film, she decides to let James go off to New York City.

==Production== Adventureland where Allegheny County and approximately 12 miles north of the city of Pittsburgh.  Other scenes were shot in Beaver County, Pennsylvania, and the airport area of Moon Township such as the Stardust Lounge.

The story takes place during the summer, but since it was shot in the winter months in Pittsburgh, Pennsylvania, crews had to often hide snowfall.  In some instances, during indoor takes, extras were paid to stand outside surrounding the windows and doors to block the snow falling behind the actors.
Internet star iJustine makes an uncredited cameo appearance as she was already in Kennywood when filming started.

==Release==
The film premiered at the 2009 Sundance Film Festival  and went on general release on April 3, 2009. The film was also screened at the 2009 Edinburgh International Film Festival. 

===Critical response===
 
Adventureland opened to very positive reviews with an 89% based on 208 reviews and is "Certified Fresh" on the review aggregator site Rotten Tomatoes. {{cite web
| title = Adventureland (2009)
| url = http://www.rottentomatoes.com/m/adventureland/
| work = Rotten Tomatoes
| publisher = Flixster
| accessdate = 2010-04-10
| archiveurl= http://web.archive.org/web/20100420013116/http://www.rottentomatoes.com/m/adventureland/#trailers| archivedate= 20 April 2010  | deadurl= no}}
 

Metacritic gave the film an aggregate rating of 76 based on 34 select critics.   

===Box office===
 
In its first week at the box office, Adventureland opened moderately wide in only 1,862 screens grossing $5.7 million ranking #6 at the box office. Despite opening up in fourteen more screens in its second week Adventureland only took in $3.4 million, falling to #9 at the box office. The film concluded its U.S. domestic run on May 28, 2009 with a gross of $16,044,025 and a total international gross of  $17,164,377  .       

===Awards and nominations===
 
The film was nominated for the Gotham Independent Film Awards "Best Ensemble Cast", scheduled in New York City on November 30, 2009. 

Adventureland was the winner of High Times|High Times magazines 2009 Stoner Movie of the Year Award. Kristen Stewart also received High Times  2009 Stonette of the Year Award, due in part to her performance in the film. 

Jesse Eisenberg was nominated for Favorite Male Breakthrough Performance at the Teen Choice Awards for his performance and also for his work in Zombieland.

===Home media===
 
The film was released August 25, 2009 on DVD and Blu-ray Disc|Blu-ray with unrated bonus features. 

==Soundtrack==
  The Replacements, and Crowded House.

The film maintains a notable reverence for Lou Reed, who is idolized by the main character, featured on T-shirts and posters of other cast members and has likewise elevated the status of the playground mechanic stemming from a rumored jam session with the artist. Earlier versions of the script replaced Lou Reed with singer-songwriter Neil Young as the musician Ryan Reynoldss character had played with, and used Youngs songs "Everybody Knows This Is Nowhere" and "Hey Hey, My My (Into The Black)" in key scenes.

{{tracklist
| collapsed       = yes
| headline        = Adventureland - Original Motion Picture Soundtrack
| extra_column    = Location where played in the film
| total_length    = 
| writing_credits = yes
| title1          = Satellite of Love
| extra1          = Played while Connell drives with James to help his mother
| writer1         = Lou Reed
| length1         = 3:39 Modern Love
| extra2          = Played in amusement park
| writer2         = David Bowie
| length2         = 3:56
| title3          = Im in Love with a Girl
| extra3          = Played during Ems party Big Star
| length3         = 1:47 Just Like Heaven
| writer4         = The Cure
| length4         = 3:31
| extra4          = Played during the bumper car sequence
| title5          = Rock Me Amadeus Falco
| length5         = 3:18
| extra5          = Played frequently over the amusement park speakers, to most of the characters annoyance
| title6          = Dont Change
| writer6         = INXS
| length6         = 4:26
| extra6          = Played over the credits Your Love
| writer7         = The Outfield
| length7         = 3:42
| extra7          = Played when Joel attempts to ask Sue out 
| title8          = Dont Dream Its Over
| writer8         = Crowded House
| length8         = 3:54
| extra8          = Played during the fireworks scene at 41:46
| title9          = Looking for a Kiss
| writer9         = New York Dolls
| length9         = 3:18
| extra9         = Played during Ems party
| title10         = Dont Want to Know If You Are Lonely|Dont Want to Know if You Are Lonely
| writer10        = Hüsker Dü
| length10        = 3:30
| extra10         = Plays as Em gives James a ride home Unsatisfied
| The Replacements
| length11        = 4:00
| extra11         = Played as James arrives in Manhattan
| title12         = Pale Blue Eyes
| writer12        = Lou Reed
| length12        = 5:28
| extra12         = Played twice in the movie
| title13         = Farewell Adventureland
| writer13        = Yo La Tengo
| length13        = 3:40
| title14         = Adventureland Theme Song
| writer14        = Brian Kenney & Ian Berkowitz
| length14        = 0:33
}}
;
{{tracklist
| collapsed       = yes
| headline        = Adventureland - Additional Songs
| extra_column    = Location where played in the film
| total_length    = 
| writing_credits = yes obsession
| extra1          = Characters dance to this in the Razzmatazz club
| writer1         = Animotion
| title2          = In the Ether
| extra2          = 
| writer2         = Black Swan Lane Point of No Return
| extra3          = Play in the Razzmatazz club at 49:15
| writer3         = Exposé (group)|Exposé Taste of Cindy acoustic version
| extra4          = 
| writer4         = The Jesus and Mary Chain
| title5          = Breaking the Law
| extra5          = Plays as James is chased through the park by an angry patron
| writer5         = Judas Priest
| title6          = In My House
| extra6          = Plays when James and Lisa P are in the Razzmatazz club at 1:20:44
| writer6         = Mary Jane Girls So It Goes
| extra7          = Record played by Connell when he and Em meet at his mothers house
| writer7         = Nick Lowe
| title8          = I Want Action
| extra8          = Video played on TV in Ems room Poison
| title9          = Bastards of Young
| extra9          = Played during the opening sequence The Replacements Tops
| extra10         = Played in the scene "Lisa P is back" at 27:11
| writer10        = The Rolling Stones Limelight
| extra11         = Munch, one of the games workers, does a poor rendition in an attempt to impress Em, also included in the trailer Rush
| Let the Music Play
| extra12         = Plays as Lisa P dances in front of a thrill ride at 45:25 Shannon
| title13         = Here She Comes Now
| extra13         = Plays during the opening credits
| writer13        = The Velvet Underground
| title14         = Blister in the Sun
| extra14         = Used in the trailer
| writer14        = Violent Femmes
| title15         = Dance Hall Days
| extra15         =  Wang Chung
| title16         = Here I Go Again
| extra16         = 
| writer16        = Whitesnake
}}

==References==
 

 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 