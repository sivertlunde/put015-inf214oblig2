Mr. Forbush and the Penguins
{{Infobox film
| name           = Mr. Forbush and the Penguins
| image          = 
| image_size     = 
| caption        = 
| director       = Roy Boulting Arne Sucksdorff Alfred Viola
| producer       = 
| based on       =  Anthony Shaffer
| narrator       = 
| starring       = John Hurt
| music          = 
| cinematography = 
| editing        = 
| studio         = EMI Films National Film Finance Corporation
| distributor    = British Lion Films
| released       = 1971
| runtime        = 
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}} British film, Roy Boulting. It stars John Hurt, Hayley Mills, Dudley Sutton and Tony Britton.

==Cast==
* John Hurt as Richard Forbush 
* Hayley Mills as Tara St. John Luke 
* Dudley Sutton as Starshot 
* Tony Britton as George Dewport 
* Thorley Walters as Mr. Forbush Sr. 
* Judy Campbell as Mrs. Forbush 
* Joss Ackland as The Leader 
* Nicholas Pennell as Julien 
* Avril Angers as Fanny 
* Cyril Luckham as Tringham 
* Sally Geeson as Jackie 
* Brian Oulton as Food-Store Clerk
* John Comer as Police Sergeant

==Production==
The film was a co-production between EMI Films, the National Film Finance Corporation and British Lion Films. 

Making the film was a turbulent experience. Penguin footage shot by Arne Sucksdorff on location in Antarctica did not cut smoothly into scenes involving humans. Roy Boulting of British Lion replaced director Al Viola, and replaced the female lead with his then-wife, Hayley Mills. John Hurt was angry at this and Bryan Forbes of EMI had to spend an entire evening persuading him not to quit. 

The film failed to recoup its considerable cost. 

==References==
 

==External links==
*  at the Internet Movie Database
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 