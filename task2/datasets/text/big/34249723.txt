Sons of Trinity
{{Infobox film
 | name = Trinità & Bambino ... e adesso tocca a noi! (Sons of Trinity)
 | image =  Sons of Trinity.jpg
 | caption =
 | director = Enzo Barboni (E.B. Clucher)
 | writer = Enzo Barboni, Marco Tullio Barboni
 | starring =  Heath Kizzier Keith Neubert
 | music = Stefano Mainetti
 | cinematography = Juan Amorós
 | editing = Antonio Siciliano
 | producer =  Italo Zingarelli
 | distributor = Trainidad Film
 | released =  29 June 1995
 | runtime = 103 min
 | awards =
 | country = Italy
 | language = Italian
 | budget =
 }} 1995 Cinema Italian spaghetti Trinity series starring Terence Hill and Bud Spencer, but although it was directed and produced by the same people as the original film, it failed to attract any interest in the audience.  It was the last film directed by Enzo Barboni.

== Plot summary ==
The children of Trinity and Bambino bear the same names of their fathers and, like them, they get a job in a dusty town in the West. Trinity Junior is a bounty hunter prankster and womanizer, while Bambino, more gruff, is also the sheriff and the jailer. The quiet peace of the two, who plan to marry two beautiful girls, is interrupted by the arrival of a gang of criminals in the city.

== Cast ==
*Heath Kizzier: Trinity
*Keith Neubert: Bambino
*Yvonne De Bark: Bonita
*Fanny Cadeo: Scintilla
*Renato Scarpa: Pablo
*Ronald Nitschke: Sheriff
*Siegfried Rauch: Parker
*Renato DAmore :Ramirez Primero
*Riccardo Pizzuti: Gunslinger Jack Taylor: Theopolis

==References==
 

==External links==
* 
  
 
 
 

 