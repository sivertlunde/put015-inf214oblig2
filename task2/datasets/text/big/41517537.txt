Anuradha (2015 film)
{{Infobox film
| name           = Anuradha
| image          = অনুৰাধা.jpg
| alt            =
| caption        =
| director       = Rakesh Sharma
| producer       = Luit Kumar Barman
| writer         = Rakesh Sharma
| starring       = Meghranjani, Diganta Hazarika, Joy Kashyap, Suruj Kalita, Taniya Nandy, Pranami Bora and Nayan Saikia.
| music          = Geet Priyam
| cinematography = Suruj Deka
| editing        = Ratul Deka
| studio         =
| distributor    =
| released       =  
| runtime        =
| country        = India
| language       = Assamese
| budget =
}} Assamese Assamese language drama film directed by Rakesh Sharma and starring Meghranjani and Diganta Hazarika in the lead roles. Gitika Talukdar. মেঘৰঞ্জনী আৰু দিগন্ত হাজৰিকাৰ ৰোমাণ্টিক যুটিৰে অনুৰাধা, Bahumukh(বহুমুখ), পৰিপূৰিকা, Amar Axom(আমাৰ অসম), 13 November 2013, Page: চিত্ৰবাণী (4) 
The film was produced by Luit Kumar Barman under the banner of M L Entertainment and Cine Dream Unlimited, and released on 27 March 2015. Axomiya Khabar(অসমীয়া খবৰ), 10 November 2013     

== Plot ==

The movie is based on the central character Anuradha (Meghranjani). It is about a woman abandoned by her husband to lead an unexpected solitary life in the laps of nature. The movie showcases different inner conflicts in various female characters in a male dominated Indian society. This movie explores different female mythological characters of Indian culture like Kunti, Shakuntala, Chitangada, Supornakha etc. and analyse their relevance to the common present day women.

==Cast==
* Meghranjani 
* Diganta Hazarika 
* Joy Kashyap 
* Pranami Bora 
* Suruj Kalita 
* Taniya Nandy 
* Nayan Saikia

==Production==
Story, dialog and screenplay of the film is the directors own. The editor of the film is Ratul Deka. Executive producer is Diganta Saikiya. Cinematography by Suruz Deka. Main assistant director is Anupam Baisya and Creative director of the film is Luit Kumar Barman 

==Soundtrack==
Music director of the film Anuradha is Geet Priyam. The singers are  , former Chief Minister of Assam.

==References==
 

 
 
 


 