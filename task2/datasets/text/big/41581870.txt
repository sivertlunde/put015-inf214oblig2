Washington Story
{{Infobox film
| name           = Washington Story
| image_size     =
| image	         = 
| caption        =
| director       = Robert Pirosh
| producer       = Dore Schary
| writer         = Robert Pirosh
| based on = 
| starring       = Van Johnson Patricia Neal Louis Calhern
| music          =
| cinematography =
| editing        =
| distributor    = Metro-Goldwyn-Mayer
| released       = 1952
| runtime        = 
| country        = United States English
| budget         = $1,419,000  . 
| gross          = $684,000 
| preceded_by    =
| followed_by    =
}}
Washington Story is a 1952 film. A reporter in search of government corruption falls for a congressman.

==Reception==
According to MGM records the movie was not a hit, earning $557,000 in the US and Canada and $127,000 elsewhere, making a loss to the studio of $1,060,000. 

==References==
 

==External links==
* 
*  at TCMDB

 
 
 
 
 
 
 


 