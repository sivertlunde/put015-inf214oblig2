Nativity! (film)
 
 
 Nativity}}
{{Infobox film
| name           = Nativity!
| image          = Nativity poster.jpg
| caption        = Theatrical release poster
| director       = Debbie Isitt  
| producer       = Nick Jones  
| writer         = Debbie Isitt Jason Watkins  Marc Wootton    Alan Carr
| music          = Nicky Ager  Debbie Isitt
| cinematography = Sean Van Hales
| editing        = Nicky Ager
| studio         = Mirrorball Films  BBC Films  Screen WM
| distributor    = Freestyle Releasing  E1 Entertainment
| released       =  
| runtime        = 122 minutes
| country        = United Kingdom
| language       = English
| budget         = £2,000,000
| gross          = £5,187,402
}}
Nativity! is a British comedy directed by Debbie Isitt and released on 27 November 2009. The film stars Martin Freeman and Ashley Jensen. The film is written by its director, Debbie Isitt, but is also partially improvised.    
The film premiered on 23 November 2009 in the   was released in 2012 and director Debbie Isitt has confirmed a third film, with filming starting in November 2013 and was released in November 2014 https://twitter.com/DeborahIsitt/status/379638745018138625 

==Plot==

Mr Maddens (Martin Freeman) is a "frustrated, under-achieving primary school teacher"  who once had ambitions of being successful as an actor, producer or director. Every year St. Bernadettes Catholic school in Coventry, where he teaches, competes with a local Protestant private school (Oakmoor) to see who can produce the best nativity play. Maddens hates Christmas because his ex-girlfriend Jennifer Lore (Ashley Jensen), who attended drama school with him, left him on Christmas before he could propose to her. Despite this, the St. Bernadettes headteacher Mrs Bevans (Pam Ferris) tasks him with running their nativity play this year, and gives him a new class teaching assistant called Mr Poppy (Marc Wootton), who turns out to be as much of a child as the rest of Mr Maddenss class.
 Jason Watkins), who runs the nativity plays at rival school Oakmoor. Determined not to be seen as a failure by his old rival, Maddens lies to Shakespeare about how a Hollywood producer—their old friend Jennifer—will be turning his production into a Hollywood film, though he hasnt even spoken to her in five years. Mr Poppy accidentally overhears this and is so excited that he has to spread the rumour. Soon Maddens finds his lie is out of control, and all he can do is go along with it as media attention mounts and the children get more and more excited.

Unfortunately, the St. Bernadettes children dont seem to be anywhere near as talented as Shakespeares Oakmoor class, and Maddens finds it difficult to have confidence in their abilities. However, enthusiastic and childlike Mr Poppy helps him and the class to create an energetic, interesting nativity which showcases all of the childrens unique—and often strange—talents. Meanwhile, Maddens tries to find a way to contact Jennifer to make the lie come true, even travelling to America to try to persuade Jennifer to visit. However, it turns out that she is only a secretary, not a film producer, and returns home disappointed.

Finally, amidst continuing media attention and the Mayors kind offer to allow the play to be performed in Coventry Cathedral, Mrs Bevans discovers that the Hollywood story was a lie and cancels the play, advising Mr Maddens to start looking for another job. Resigned, Maddens shouts at Mr Poppy and blames him for many things that have gone wrong, but comes to his senses when facing his disappointed class and decides that the show must go on.

The play is performed at the cathedral to an audience of the childrens parents and family friends, but there is no sign of any Hollywood producers, despite how good the play has turned out to be—and the fact that Mrs Bevans was initially furious to hear that her order had been ignored. Halfway through, jealous Gordon Shakespeare climbs on-stage to tell everybody that there is nobody from Hollywood there and the entire thing was a lie. Luckily, a helicopter flies over and Mr Poppy declares that its Hollywood come to see the play—the show continues, and Jennifer and her film producer boss finally come in at the back to watch the end, joined by Mr Maddens, who is still in love with Jennifer and kisses her.

The play is a success, and everyone, including Shakespeare, is reunited onstage to celebrate the childrens success. As the film closes, Mr Maddens and Jennifer are shown decorating his house together for Christmas, reunited at last.

==Cast==
*Martin Freeman as Paul Maddens, a primary school teacher trying to produce and direct a nativity play that will for once outdo a rival school.    Jason Watkins private primary school teacher and long-time rival of Maddens.  Hollywood secretary and ex-girlfriend, then once again girlfriend of Maddens. 
*Marc Wootton as Desmond Poppy, an immature-for-his-age classroom assistant
*Alan Carr as Patrick Burns a critic. 
*Ricky Tomlinson as The Mayor
*Pam Ferris as Mrs Patricia Bevan
*Clarke Peters as Mr Parker a Hollywood director, the boss of Maddens ex-girlfriend Jennifer 
*Geoffrey Hutchings as Father Tom
*Rosie Cavaliero as Miss Rye

{| class="wikitable"
|-
!  Mr Maddens Class !! Mr Shakespeares Class
|-
| Alexandra Allen as Cleo || Aidofe McLeod as Grace
|-
| Ben Wilby as Bob || Anna Price as Ellen
|-
| Bernard Mensah as TJ || Arun Nahar as Jake
|-
| Brandon McDonald as Oli || Bessie Cursons as Christy
|-
| Cadi Mullane as Crystal || Bethany Carter as Daisy
|- Charlie Dixon as Thomas
|-
| Dominic McKernan as Dan || Cherie Ng as Nicola
|-
| Ellie Coldicutt as Beth || Eleanor Bonas as Rachel
|-
| Faye Dolan as Jade  || Francesca York as Caroline
|-
| James Warner as Buddy || Freddie Watkins as Sebastian
|-
| Jake Pratt as Alfie || Georgina Owen as Emily
|-
| Joe Lane  as Edward || Hannah Ciotknowski as Elizabeth
|-
| Joshan Patel as Bill || Harriet Kilner as Charlotte
|- Krista Hyatt as Becky || Hayley Downing as Victoria
|-
| Maeve Dolan as Sam || Imogen Stern as Phoebe
|-
| Michael McAuley as William || Katie Maguire as Megan
|-
| Milly Webb as Neve || Katie Stafford as Molly
|-
| Morgan Brennan as Charlotte || Lauren Downing as Joanne
|-
| Rebecca Maguire as Saffron || Lily-Rose Sharry as Lynette
|-
| Reece Stowe as Fraser || Michael Brown as Charles
|-
| Rhyannon Jones as Alice || Molly Burton as Catherine
|-
| Sam Tott as Matt || Mi Tuulikki Kelly as Lorna
|-
| Sydney Isitt-Ager as Sadie || Safiya Asharia as Sophie
|-
| Thomas McGaritty as Zack || Salim Zayyan as Percival
|-
| Oscar Steer as Auditionee || Taylor Drew as Harry
|-
| Harvey Flanagan as Tyrese || Chris Reynolds as Freddy
|}

==Box office==
When released in the UK, the movie opened at number 5 taking £794,314 at the UK box office. In its 3rd week the movie rose to number 4 and in the end made £5,187,402. 

==Sequels==
* 
* 

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 