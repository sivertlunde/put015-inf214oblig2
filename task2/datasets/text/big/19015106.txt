Jai Kishen
 
 
{{Infobox film
| name = Jai Kishen
| caption =  Chandni Reema Lagoo Arun Bakshi
| director = Sunil Agnihotri
| producer = Narottam V. Purohit Vijay K. Ranglani
| distributor = Shalimar International
| released = 10 June 1994
| runtime = 
| language = Hindi
| music = Anand-Milind
| budget = 
| gross = 
|}}

Jai Kishen is a 1994 Bollywood action drama film directed by Sunil Agnihotri. It stars Akshay Kumar in a double role, Ayesha Julka and Chandni (actress)|Chandni.

==Cast==

*Akshay Kumar ... Jai Verma/Kishen
*Ayesha Jhulka ... Asha Chandni ... Anita
*Reema Lagoo ... Mrs. Verma
*Arun Bakshi ... Police Inspector
*Avtar Gill ... Inspector Pratap Singh
*Vikas Anand ... Commissioner of Police
*Tinu Anand ... Anand Rao Antya
*Mahavir Shah ... Chhote Bhai
*Viju Khote ... Havaldar Shevde
*Deepak Shirke ... Kaalia Shirke
*Harish Patel ... Putarmal
*Sparsh Puri ... child artist
*Mohit Vaya ... child artist

==Soundtrack==

The music was composed by Anand-Milind and the lyrics authored by Sameer. The song Jhoole Jhoole Lal, was based on Nusrat Fateh Ali Khans Dam Mast Kalendar and was a hit when released.
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Surat Hai Meri Bholi" Poornima
|-
| 2
| "Yeh Aalam Yeh Mausam"
| Poornima
|-
| 3
| "Pyar Hua Hai"
| Kumar Sanu, Alka Yagnik
|-
| 4
| "Yaaro Kya Ladki Hai"
| Kumar Sanu
|-
| 5
| "Tilda Banaspati"
| Kumar Sanu
|-
| 6
| "Jhoole Jhoole Lal"
| Arun Bakshi, P. T. Mishra
|-
| 7
| "Looshe Wai Wai"
| Poornima
|}

== External links ==
*  

 
 
 
 
 
 