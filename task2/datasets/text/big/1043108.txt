I Heart Huckabees
 
{{Infobox film
| name           = I  ♥  Huckabees
| image          = I Heart Huckabees_poster.JPG
| alt = 
| caption        = Theatrical release poster
| director       = David O. Russell
| producer       = David O. Russell Scott Rudin Gregory Goodman
| writer         = David O. Russell Jeff Baena
| starring       = Dustin Hoffman Isabelle Huppert Jude Law Jason Schwartzman Lily Tomlin Mark Wahlberg Naomi Watts
| music          = Jon Brion
| cinematography = Peter Deming
| editing        = Robert K. Lambert Scott Rudin Productions
| distributor    = Fox Searchlight Pictures
| released       =  
| runtime        = 106 minutes
| country        = United States
| language       = English French Spanish
| budget         = $20 million
| gross          = $20,072,172
}}
I  ♥  Huckabees (known usually as I Heart Huckabees    but also as I Love Huckabees   ) is a 2004 American philosophical comedy film from Fox Searchlight. It was produced and directed by David O. Russell, who co-wrote the screenplay with Jeff Baena.

The film stars an ensemble cast consisting of Dustin Hoffman, Isabelle Huppert, Jude Law, Jason Schwartzman, Lily Tomlin, Mark Wahlberg, and Naomi Watts.

==Plot==
Albert Markovski (Schwartzman) is a young man who heads the local chapter of an environmental group, the "Open Spaces Coalition." One of their current projects is an attempt to stop the building of a new Huckabees store, a chain of "big-box store|big-box" department stores. Albert is a rival of Brad Stand (Law), a shallow power executive at Huckabees. Brad infiltrates Open Spaces and charismatically displaces Albert as the leader. Dawn Campbell (Watts) is Brads live-in girlfriend and the face and voice of Huckabees; she appears in all of the stores commercials.
 existential detectives, romantic and transcendentalist philosophies)—and spy on him, ostensibly to help him solve the coincidence. Bernard and Vivian introduce Albert to Tommy Corn (Wahlberg), an obsessively anti-petroleum firefighter. Tommy is assigned to Albert as his Other.
 absurdist philosophy. She teaches them to disconnect their inner beings from their daily lives and their problems, to synthesize a non-thinking state of "pure being."  Being lifted from their troubles, they wish to keep that feeling forever, yet she tells them that it is inevitable to be drawn back to the human drama, and to understand that the core truth of that drama is misery and meaninglessness. In order to prove her point, Caterine takes Albert to go and have sex in the woods, leaving Tommy behind. Tommy finds out about the two of them being together and feels hurt. Caterine tells him that they found each other through all of the human suffering and drama. Tommy rejects this idea and leaves them furious and lost. Meanwhile, in Brads further attempts to undercut Albert, he and Dawn meet with and are influenced by Bernard and Vivian. In the following days, Brad and Dawn rethink their entire lives: Dawn rejects the modeling world and looks for deeper meaning, and Brad realizes that his whole ascent in the corporate ladder is meaningless, as he has lived his whole life just trying to please others and not himself.
 enlightenment when he synthesizes the two opposing outlooks of the Jaffes and Vauban to realize the cosmic truth of everything. Brad, meanwhile, is fired from Huckabees, leaving him rudderless.  Albert reveals to Brad that he burned Brads jet skis, and the fire spread to the house. Albert understands that he and Brad are no different, that everything really is inextricably connected, but that these connections necessarily arise from the often senselessly painful reality of human existence. Having realized this, he refers Brad to Caterine, hoping she will help him as she did Albert and Tommy. Albert and Tommy talk later about everything that has happened. As the two talk, Caterine and the Jaffes watch them, concluding that they can close both of their cases.

===Director commentary===
In an interview with the SuicideGirls website, director David Russell said in response to the question "How do you describe I Heart Huckabees?":  

==Cast==
*Dustin Hoffman as Bernard Jaffe
*Isabelle Huppert as Caterine Vauban
*Jude Law as Brad Stand
*Jason Schwartzman as Albert Markovski
*Lily Tomlin as Vivian Jaffe
*Mark Wahlberg as Tommy Corn
*Naomi Watts as Dawn Campbell

===Supporting characters===
*Ger Duany as Stephen Nimieri
*Isla Fisher as Heather
*Sydney Zarp as Cricket Hooten
*Darlene Hunt as Darlene
*Kevin Dunn as Marty
*Benny Hernandez as Davy
*Richard Appel as Josh
*Benjamin Nurick as Harrison
*Jake Muxworthy as Tim
*Tippi Hedren as Mary Jane Hutchinson
*Altagracia Guzman as Mrs. Echevarria
*Saïd Taghmaoui as Translator
*Bob Gunton as Mr. Silver
*Talia Shire as Mrs. Silver
*Jean Smart as Mrs. Hooten
*Jonah Hill as Bret Hooten (film debut)
*Shania Twain as herself
*Richard Jenkins as Mr. Hooten (uncredited)

==Reception==
The film opened October 1, 2004, with a $73,044 per screen average in New York and Los Angeles. Critical opinion for the film was mixed to positive. The film was rated 62% "fresh" on Rotten Tomatoes. Some critics were displeased with the existential subject matter and said the script was unfocused,  while others celebrated its performances (especially Wahlberg as Tommy Corn) and originality. 

Worldwide box office taken in for the film is $20,072,172 as of June 2009.

==Marketing==
In an advertising campaign for the film, four fictional websites were created. They were portrayed as if they were actual websites involving the characters and organizations featured in the movie.  Each website had a link called "Disclaimer" at the bottom that leads to FOX Searchlights   for the film.  Websites were rendered inactive a few months after the films release, each now redirecting to FOX Searchlights website.
*Huckabees Corporation:  The "official" website for the chain of Huckabees stores.  Featured the store history, announcements, three television ads featuring Dawn Campbell and a banner promoting the Huckabees collaboration with the Open Spaces Coalition.
*Open Spaces Coalition:  This website described the importance of the marshlands Albert Markovski is trying to protect, ways of aiding the cause.  The site also contained poetry written by Markovski and downloadable flyers.
*Existential Detectives:  The website promoting the Jaffes detective agency.  It explained the methodology of the detectives, had two case studies and an online questionnaire.
*Caterine Vauban:  The website of Caterine Vauban, author of "If Not Now".  The site contained reviews and excerpts from the book.

==Soundtrack==
 
 
Jon Brion provided the score and seven original songs for the film. His unique methods for writing previous film scores (Punch-Drunk Love, Eternal Sunshine of the Spotless Mind) involved very close collaboration with the director. Through this process, David O. Russell was able to sit in the same room with Brion and watch an early cut of the film. Russell described what he wanted to portray and Brion would compose music to Russells descriptions. The process can be seen on a featurette on the films special edition DVD.

David O. Russell, while working with Brion, had come across Brions first solo album, Meaningless (album)|Meaningless. Russell has mentioned that Brions album asks similar questions to the ones Russell was trying to ask with I Heart Huckabees. In particular, Russell notes that the questions on Meaningless are closer to the questions directed from Caterine Vaubans negative and dark point of view.

Many cues used in the film feature a Chamberlin, a keyboard instrument from the 1950s that replicates instrumental sounds using recorded tape.

==Tension during filming==
In March 2007, two videos were leaked onto YouTube portraying on-set arguments between David O. Russell and Lily Tomlin. When the Miami New Times asked Tomlin about the videos, she responded, "I love David. There was a lot of pressure in making the movie—even the way it came out you could see it was a very free-associative, crazy movie, and David was under a tremendous amount of pressure. And hes a very free-form kind of guy anyway." 

==See also==
*Albert Camus
*Deconstruction
*Dialectical monism
*Dirk Gently
*Friedrich Nietzsche
*Magritte
*Isabelle Huppert filmography
*Synchronicity

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 