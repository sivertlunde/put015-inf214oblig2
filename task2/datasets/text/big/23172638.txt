Tilly of Bloomsbury (1940 film)
 Tilly of Bloomsbury}} British comedy Tilly of Bloomsbury by Ian Hay. It starred Sydney Howard, Jean Gillie, Kathleen Harrison and Henry Oscar.  A young woman falls in love with an aristocracy (class)|aristocrat, and attempts to convince his family that she is of their social class. 

==Plot Summary==

The rich and wealthy aristocrat socialité bachelor Dick Mainwaring falls in love with a beautiful you of woman from a lower class, Tilly Welwyn, whose mother owns a boarding house. Problems arise because the two lovers come from completely different backgrounds. At first Dick is discouraged and behaves like a complete snob towards the hard working mother, but after a while he learns to see the good side of their life and changes his mind. Dick brings Tilly to his familys mansion in te country over the weekend. The visit starts out badly, since his mother, Lady Marion, strongly disapproves with the couples union. The mother tries to split the couple up, but they are aided by the cunning butler, Samuel Stillbottle. Ultimately their love grows stronger as they overcome their differences, and romance pull the longer straw in the end.

==Cast==
* Sydney Howard - Samuel Stillbottle 
* Jean Gillie - Tilly Welwyn 
* Henry Oscar - Lucius Welwyn 
* Athene Seyler - Mrs. Banks  Michael Wilding - Percy Welwyn 
* Kathleen Harrison - Mrs. Welwyn 
* Athole Stewart - Abel Mainwaring 
* Michael Denison - Dick Mainwaring 
* Martita Hunt - Lady Marion Mainwaring 
* Joy Frankau - Amelia Mainwaring 
* Eve Shelley - Diana

==References==
 

 
 
 
 
 
 


 