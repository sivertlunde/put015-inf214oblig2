Saw III
{{Infobox film
| name = Saw III
| image = Saw3 cape10.jpg
| caption = Promotional poster
| director = Darren Lynn Bousman
| producer = Mark Burg Gregg Hoffman Oren Koules
| screenplay = Leigh Whannell
| story = James Wan Leigh Whannell
| starring = Tobin Bell Shawnee Smith Angus Macfadyen Bahar Soomekh Dina Meyer 
| music = Charlie Clouser
| cinematography = David A. Armstrong
| editing = Kevin Greutert
| studio = Twisted Pictures
| distributor =  Lionsgate Films
| released =  
| runtime = 108 minutes 114 minutes (Unrated version)  120 minutes (Directors Cut) 
| country = Canada United States
| language = English
| budget = $10 million    
| gross = $164.8 million 
}}
Saw III is a 2006 Canadian-American  horror film directed by Darren Lynn Bousman from a screenplay  by Leigh Whannell and story by James Wan and Whannell. Wan and Whannell directed and wrote Saw (2004 film)|Saw and Bousman wrote and directed Saw II. It is the third installment in the seven-part Saw (franchise)|Saw franchise and stars Tobin Bell, Shawnee Smith, Angus Macfadyen, Bahar Soomekh, and Dina Meyer. Saw III marks the first appearances of Costas Mandylor and Betsy Russell, albeit minor roles; they would later become major characters in the series.
 Jeff Denlon, Jigsaw in order to try and let go of his vengeance for the man that killed his son. Meanwhile, a bed-ridden John Kramer has his apprentice Amanda Young kidnap Jeffs wife, List of Saw characters#Lynn Denlon|Lynn, who is tasked with keeping him alive for one final test before he dies.

Production began right after Saw II s successful opening weekend. Filming took place in Toronto from May to June 2006. Whannell aimed to make the story more emotional than previous installments, particularly with the Amanda and Jigsaw storyline. The film is dedicated to producer Gregg Hoffman who died on December 4, 2005.  
 Teen Choice Award. Saw III was released to DVD and Blu-ray Disc on   and topped the charts selling 2.5 million units in its first week. It was followed by Saw IV, released in October 2007.

==Plot== Eric Matthews Lieutenant Daniel Troy (J. Allison Kerry (Dina Meyer), who is guilt-ridden over Matthews disappearance, are called to the scene. Kerry notes that the door to the room was welded shut, breaking Jigsaws modus operandi of giving his victims a chance to escape. She is abducted from her home that night while reviewing Troys tape, and awakens in a harness that is hooked into her ribs. Although she is able to unlock it with a key retrieved from a suspended beaker of acid before the timer expires, the harness tears her rib cage apart anyway, killing her.
 Corbett (Niamh Wilson), as a result.
 Danica Scott Judge Halden Timothy Young (Mpho Koaho), his sons killer, who is strapped to a machine that twists his limbs one by one until they break. Initially content with watching Timothy suffer, Jeff is eventually convinced by Halden to retrieve the key from a shotgun trigger. Jeff accidentally kills Halden when the shotgun discharges, and Jeff is unable to free Timothy before his neck breaks.
 Jill Tuck cutting herself Adam Stanheight the first mercy killing out of guilt. In the present, Amanda finds a letter addressed to her, its contents driving her to hysterics. Lynn and John talk privately after the surgery, and Lynn admits that her ordeal has renewed her appreciation for her family. Amanda soon returns with news that Jeffs tests are complete, but refuses to remove Lynns collar. In the resulting argument, Amanda reveals she has become disillusioned with Johns philosophy, and the tests she designed, including Troys and Kerrys, reflected this. She also reveals that she and Eric Matthews fought when he escaped the bathroom, but she escaped and presumably left him for dead.

Amanda shoots Lynn in the back just as Jeff arrives, who retaliates by shooting Amanda in the neck with a gun provided by John. As Amanda slowly dies, a deeply saddened John reveals that the game was actually hers: aware of the fact that Amandas traps were inescapable, and unwilling to let a murderer continue his legacy, he created a game to test her will for the subjects to live; hiding the fact that Lynn and Jeff were married. After seeing to his wife, Jeff confronts John, who offers him one final test. He tells Jeff to choose between killing him or forgiving him, and offers to call an ambulance for Lynn, should Jeff choose to forgive. Jeff tells John he forgives him, before slashing his throat with a power saw. The door to the sickroom seals itself as John plays a final tape, which reveals that Jeff failed his final test by killing John, the only person who knew Corbetts whereabouts, and in order to save her he must play another game. The tape ends as John dies, and the collar simultaneously activates killing Lynn, leaving Jeff trapped in the sickroom with the three corpses.

==Cast==
  John Kramer
* Shawnee Smith as Amanda Young Jeff Denlon Lynn Denlon Detective Allison Kerry Detective Eric Matthews Lieutenant Daniel Rigg Detective Mark Hoffman Jill Tuck Adam Stanheight Timothy Young Judge Halden Danica Scott Corbett Denlon Dylan Denlon Troy
* Xavier Chavez Obi Tate Donnie Greco

==Production==

===Development and writing===
Producer Gregg Hoffman unexpectedly died a few weeks after the release of Saw II. Writer and director of Saw II, Darren Lynn Bousman and Saw writer Leigh Whannell originally turned the offer down to make a third film.  Whannell, Bousman, and James Wan got together to have lunch the day they heard of Hoffmans passing and came to conclusion that Saw III was going to be made with or without them so they decided to make the film in dedication to Hoffman.    Whannell aimed to make Saw III more emotional, describing the plot as essentially a "love story" between Jigsaw and Amanda.    
 
Bousman said they did not intend to have a twist ending, as distinctly as the previous films,  noting that "I think most people will figure it out in the first 15 minutes of the film". Whannell added, "What Darren and I struck for Saw III was to have an emotionally impactful ending. We wanted something that would almost make someone who was really invested in the story cry. We have Jigsaw, this character whos been so cold and clinical, hes been presented throughout the previous two films as someone whos very much in control. Hes more like a reptile than a human being. In Saw III he becomes a human being. You see him crack. His veneer cracks and that was what was most important to us far and above any sort of gimmick or twist".  Whannell also answered questions from previous films that were brought up by fans on the official Saw message board.  As with the previous two films, the ending was only given to the actors who appeared in the final scene at the time it was filmed. At one point the script was stolen from Bousmans chair, however it was returned before it was leaked online.   

===Casting===
Soomekh became close with Lionsgate after appearing in their film Crash (2004 film)|Crash (2004) and they wanted her in their next big film. Not a fan of horror films she found the role challenging. "I had nightmares the first month I was on set. We were shooting it for two months. People say because youre an actor its not a big deal because you go in there and its fake or whatever. But what they dont understand is that its actually the opposite because, as an actor, when you go in there you have to believe its real to make your performance real. You have to get lost in the mindframe of this character", she said. 

Larose was in Bousmans first short film titled Butterfly Dreams and helped finance Bousmans second short, Identity Lost. 

===Filming===
Saw III was given a larger budget of $10 million,  compared to Saw II s $4 million.    to create a dynamic feel".     Post-production services were provided by Deluxe Entertainment Services Group Inc.|Deluxe. 

===Trap designs===
Bousman described the hardest scene to film was the "Pig Scene", explaining that they had to rush and it involved filming "so many moving parts".  The pig carcasses were made out of foam, rubber and latex.  The pig props had live disinfected maggots attached with honey.  Bell said in an Empire (film magazine)|Empire interview that the "Pig Scene" was his favorite trap in the entire series. 
 prosthetics and practical effects.  

==Release==
Saw III was released on   in the United States, Canada and the United Kingdom. It was released in Australia on   and on   in New Zealand.   According to executive producer Daniel Heffner, the film was toned down seven times to obtain the Motion Picture Association of America film rating system#Ratings|"R" rating. According to Bousman, the Motion Picture Association of America (MPAA) ratings board was less concerned with the films graphic violence because television shows like CSI (franchise)|CSI have expanded the scope of what is acceptable viewing with their graphic depictions of crime scenes and autopsies. Bousman said the MPAA is more concerned with emotional torture that disturbs the audience.    In Japan, Saw III received a Eirin|R18+ rating while the previous two films received an R15+ rating.    At screenings in the United Kingdom, five people were reported to have fainted at separate cinemas with three at one cinema, resulting in ambulances called.  |deadurl=no}} 

===Marketing===
The opening scene of Troys trap was shown at San Diego Comic-Con International on July 21, 2006.  The same clip was planned to be shown before the opening of Crank (film)|Crank in theaters on  . However, the MPAA would not allow it.   On   Bell, Smith and Bousman appeared at Spike TVs Scream Awards to promote the film and the clip of Troys trap was shown. 
 blood drive for the Red Cross and collected 23,493 pints of blood. 

===Soundtrack===
  IGN Music gave it a 7.2 out of 10. 
 
{{Infobox album |  
| Name        = Saw III: Original Motion Picture Soundtrack
| Type        = Soundtrack
| Artist      = Various Artists
| Cover       = 
| Released    = October 24, 2006 Heavy metal, alternative rock, metalcore
| Length      = 72:01
| Label       = Artists Addiction
| Last album  =   (2005)
| This album  = Saw III: Original Motion Picture Soundtrack  (2006)
| Next album  =    (2007)
}}
;Track listing
{{Track listing
| collapsed       = yes
| headline         = Saw III: Original Motion Picture Soundtrack
| writing_credits = yes
| extra_column = Artist
| total_length = 72:01

| title1 =  This Calling
| writer1         = Oliver Herbert Phillip LaBonte Michael MArtin Jeanne Sagan All That Remains
| length1 = 3:38

| title2 = No Submission
| writer2         = Tony Campos Wayne Static
| extra2 = Static-X
| length2 = 2:40

| title3 = Eyes of the Insane
| writer3         =  Tom Araya Jeff Hanneman
| extra3 = Slayer
| length3 = 3:33

| title4 = Walk with Me in Hell Lamb of God
| extra4 = Lamb of God 
| length4 = 5:11

| title5 =  Monochrome
| writer5         = Page Hamilton Helmet 
| length5 = 3:47

| title6 = Guarded Disturbed
| extra6 = Disturbed
| length6 =  3:21

| title7 =  Drilled A Wire Through My Cheek
| writer7         = Justin Furstenfeld Patrick Sugg
| extra7 = Blue October
| length7 = 4:25

| title8 = No More
| writer8         = 
| extra8 =  Drowning Pool
| length8 = 4:26

| title9 =  Burn It Down
| writer9         = Avenged Sevenfold
| extra9 = Avenged Sevenfold
| length9 = 4:59

| title10 = Your Nightmare
| writer10         = Eighteen Visions
| extra10 = Eighteen Visions
| length10 = 3:23

| title11 = Dead Underground
| writer11         = Jim Kaufman Ron Underwood
| extra11 =  Opiate For The Masses
| length11 = 3:58

| title12 = Suffocating Under Words of Sorrow (What Can I Do)
| writer12         = Bullet for My Valentine
| extra12 = Bullet for My Valentine
| length12 = 3:36

| title13 = Fear Is Big Business
| writer13 = Al Jourgensen Thomas Victor Ministry
| length13 = 4:52

| title14 = The Wolf Is Loose Mastodon
| extra14 = Mastodon
| length14 = 3:34

| title15 = Killer Inside
| writer15 = Hydrovibe
| extra15 = Hydrovibe Featuring Shawnee Smith
| length15 = 3:17

| title16 = Sakkara
| writer16 = Hourcast  
| extra16 = Hourcast 
| length16 = 3:45

| title17 = Shed
| writer17 = Fredrik Thordendal Shagrath Galder
| extra17 = Meshuggah
| length17 = 3:35

| title18 = Effigy
| writer18 = The SmashUp
| extra18 = The SmashUp
| length18 = 4:36

| title19 = Siesta Loca
| writer19 =Ghost Machine 
| extra19 = Ghost Machine
| length19 = 3:50

| title20 = Sh*! hole Theme
| writer20 = Charlie Clouser 
| extra20 =  Clouser
| length20 = 3:15
}}

===Home media===
Saw III was released to DVD and Blu-ray Disc through Lionsgate Home Entertainment on  . It topped the home video charts in the United States and Canada with 1.6 million units sold its first day and finished the week with 2.5 million units sold.    The "Unrated DVD" was also released that day and features a 113 minute cut of the film that includes more gore.  A 120-minute long Directors Cut was released on   to coincide with the theatrical release of Saw IV on  . It also included an alternative ending.  The directors cut was released on Blu-ray in Region B on October 7, 2008, in France only. 

===Deleted scenes===
The original cut of the film ran for slightly over two hours, and several scenes were cut out, including a scene which depicted an extended scene of Kerry and Rigg examining Troys trap, where Kerry reveals to Rigg she has had nightmares about Eric, and she blames herself for what happened to him.  Adam had more scenes in the original cut.    A scene that showed Jigsaw regretting his actions was cut. Bell said, "Im glad they cut that scene. This guy knows exactly what hes doing. Does he start off with a model, then refine it? Yeah, he probably does. But there are certain things that are interesting and advance the story, and there are other things that are basically sort of backstory, and you dont really need to know".   

==Reception==

===Box office=== Puss in Boots ($34 million).   It was also Lionsgates highest-opening weekend. Lionsgates exit polling indicated that 69 percent of the audience was under 25 years old and 51 percent was male.   In its second weekend it placed number four dropping down 56% to $14.8 million, compared to Saw II  second weekend drop of 47% to $16.9 million.  The film was closed out of theaters on  , after 49 days of release.   
 United Kingdom Taiwan it Spain it Australia made Brazil made $3.8 million.  In its fourth weekend it placed fourth place with an estimated $5.6 million from 24 territories. Its best market was a second-place start in France. 
 United States Canada and $84.6 million in other territories, for a worldwide total of $164.8 million.   Saw III has the highest-grossing weekend in the series and also holds the records of highest-grossing in the international market and is the second highest-grossing film in the series worldwide.  It is also Lionsgates fifth highest-grossing film in the United States and Canada. 

{| class="wikitable" width=99% border="1"
!rowspan="2"  align="center" | Release date (United States)  
!rowspan="2"   align="center" | Budget (estimated) 
!colspan="3"  align="center" | Box office revenue 
|-
!align="center"   | United States/Canada
!align="center"   | Other markets
!align="center"   | Worldwide
|- October 27, 2006
|align="center" | $10,000,000
|$80,238,724 	 	 
|$84,635,551	
|$164,874,275
|-
|}

===Critical response===
The film was not screened in advance for critics.     The film received mixed reviews from critics. Review aggregator Rotten Tomatoes reports that 28% of 87 critics have given the film a positive review, with a rating average of 4.2 out of 10.  Metacritic, which assigns a weighted average score out of 100 to reviews from mainstream critics, gives the film a score of 48 based on 16 reviews.  CinemaScore polls reported that the average grade moviegoers gave the film was a "B" on an A+ to F scale. 

Variety (magazine)|Variety s Robert Koehler gave the film a mixed review.  He criticized the use of several flashbacks in the film, saying that it "  hinder  the movie, ratcheting down its tension and pace". He explained, "A bigger problem lies with Leigh Whannells script, which utilizes so many flashbacks and explanatory inserts that the tension, a defining feature of the first Saw, is lost". He praised Smiths performance and called MacFadyens performance "a strong, almost silent performance that conveys a pained fathers dark night of the soul", and Soomekh as "reasonably convincing as the surgeon".   Roger Moore of the Orlando Sentinel gave it two out of five stars, criticizing the plot and acting. 

The San Francisco Chronicle s Peter Hartlaub gave the film a negative review. He said, "It doesnt go much of anywhere until the infuriating last 10 minutes, when everything is sort of tied together while still producing more unanswered questions. The movie seems at times to be told in random order, often with flashbacks, and the closest thing to a plot is a weak story about the father who keeps confronting the people responsible for his sons drunken-driving death". He pointed out he lack of realism in the script, saying "One incredibly large and intricate torture device in this movie couldnt have been made without four or five subcontractors, but were supposed to believe a mentally unbalanced ex-junkie who weighs 100 pounds put it together in, at most, a few months".  Michael Ordoña of the Los Angeles Times said that "More gore is really all Saw III has to offer", saying that "the first few minutes cram in more graphic brutality than you can shake a bloody, pointed stick at". He listed other problems being "flat dialogue, uninvolving characters and a creeping sameness in the no-brain- required puzzles". He concluded his review saying, "Bottom line, those in the Saw factory know their audience and have brought along the appropriate buckets and bibs. Even devotees, however, may note pacing problems and tire of Jigsaws selective omnipotence (he can acquire copious amounts of deadly nerve agent but not a bottle of Ativan?). Those who see Saw III are in for ups and downs". {{cite web|url=http://www.calendarlive.com/printedition/calendar/cl-et-saw30oct30,0,3788651.story|title=
This new horror sequel little more than a hack|last=Ordoña|first=Michael|work=Los Angeles Times|publisher=Tribune Company|date=October 30, 2006|accessdate=September 13, 2011|archiveurl=http://www.webcitation.org/61gDBsQ9m|archivedate=September 13, 2011|deadurl=no}} 

Owen Gleiberman of Entertainment Weekly gave the film a "C".  Randy Cordova of The Arizona Republic gave it a negative review saying, "Saw III is devoid of any suspense or terror or common sense. Its simply an exercise in gore. And really, if thats all the filmmakers have up their sleeve, why bother with a plot? Just show one grisly makeup effect after another and youd create the same sensory experience".  Empire (film magazine)|Empire s Kim Newman gave the film two out of five stars. He said the acting was "surprisingly good" but criticized the script and torture devices, calling it "more contrived, and thus less effective". He ended his review saying, "It requires a stretch of the imagination too far, but theres still plenty of gore and tricksy murders here". 

====Award nominations==== MTV Movie Best Villain.  He lost to Jack Nicholson for his role in The Departed. 

==References==
 

==External links==
 
*   (archive)
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 