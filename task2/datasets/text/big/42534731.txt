Ginevra degli Almieri
{{Infobox film
| name =   Ginevra degli Almieri
| image =
| image_size =
| caption =
| director = Guido Brignone
| producer =  Liborio Capitani
| writer =  Luigi Bonelli   Ivo Perilli   Guido Brignone
| narrator =
| starring = Elsa Merlini   Amedeo Nazzari   Uberto Palmarini   Ugo Ceseri
| music = Gian Luca Tocchi 
| cinematography = Ubaldo Arata
| editing = Giuseppe Fatigati    
| studio = Capitani Film ICAR 
| released = 1935
| runtime = 87 minutes
| country = Italy Italian 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} historical drama film directed by Guido Brignone and starring Elsa Merlini, Amedeo Nazzari and Uberto Palmarini.  Merlini had spotted Nazzari during a stage play, and lobbied for his casting in his film debut.  Nazzari went on to be a leading star of Italian cinema. It is set in Florence in the fifteenth century.

==Partial cast==
* Elsa Merlini as Ginevra Degli Almieri  
* Amedeo Nazzari as Antonio Rondinelli 
* Uberto Palmarini as Padre di Ginevra 
* Ugo Ceseri as Francesco Agolanti  
* Guido Riccioli as Il Burchiello  
* Maurizio DAncora as Paolino  
* Ermanno Roveri as Menicuccio 
* Tina Lattanzi as Violante  
* Luigi Almirante as Il notaro

== References ==
 

== Bibliography ==
* Gundle, Stephen. Mussolinis Dream Factory: Film Stardom in Fascist Italy. Berghahn Books, 2013.

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 
 


 