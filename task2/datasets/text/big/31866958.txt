Arakawa Under the Bridge (film)
{{Infobox film
| name = Arakawa Under the Bridge
| image = Arakawa Under the Bridge film poster.jpg
| alt =
| caption = Film poster advertising this film in Japan
| director = Iizuka Ken
| producer =
| writer = Nakamura Hikaru (manga)
| starring = Kento Hayashi, Mirei Kiritani
| music =
| cinematography =
| editing =
| studio =
| distributor = Asmik Ace Entertainment
| released =  
| runtime =
| country = Japan
| language = Japanese
| budget =
| gross =
}}
 )]] same name and it is directed by the director Iizuka Ken. The film stars Kento Hayashi, Mirei Kiritani and released in Japanese cinemas on February 4, 2012.   

==Cast==
* Kento Hayashi as Riku/Kou Ichinomiya   
* Mirei Kiritani as Nino  
* Nana Katase as Maria 
* Yu Shirota as Sister 
* Natsumi Abe as P-ko 
* Takaya Kamikawa as Seki Ichinomiya, the father of Riku and head of Ichinomiya company.   
* Masahiro Takashima as Takayashiki, a government ministry member and a friend of Seki. 
* Kazuyuki Asano as Takai, an Ichinomiya Company employee. 
* Waka Inoue as Shimazaki, an Ichinomiya Company employee. 
* Shun Oguri as the Village Chief   
* Takayuki Yamada as Hoshi,  a self-proclaimed rock star who is in love with Nino.

==Filming location==
create sets and shot in dry riverbed of Kinugawa River on Jōsō, Ibaraki, Japan for Arakawa Under the Bridge. 

==References==
 

==External links==
*    

 
 
 
 


 