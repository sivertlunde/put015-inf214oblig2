The Forbidden City
 
{{Infobox film
| name           = The Forbidden City
| image          = The Forbidden City 1918.jpg
| image_size     =
| caption        = Advertisement in Moving Picture World, September 1918 Sidney Franklin
| producer       = Joseph M. Schenck
| writer         = Mary Murillo George Scarborough (story)
| starring       = Norma Talmadge Thomas Meighan
| cinematography = H. Lyman Broening Edward Wynard
| editing        =
| distributor    =
| released       =  
| runtime        = 62 minutes
| country        = United States
| language       = Silent English intertitles
| budget         =
}} Sidney Franklin.  The plot centers around an inter-racial romance between a Chinese princess (Talmage) and an American (Meighan).  When palace officials discover she has become pregnant she is sentenced to death.  In the latter part of the film Talmadge plays the now adult daughter of the affair, seeking her father in the Philippines.

== Cast (in credits order)==
*Norma Talmadge	... 	San San/Toy
*Thomas Meighan	... 	John Worden
*E. Alyn Warren	... 	Wong Li
*Michael Rayle	... 	Mandarin
*L. Rogers Lytton	... 	Chinese Emperor
*Reid Hamilton	... 	Lieutenant Philip Halbert

==External links==
*  
*  
*  
*  
*   available for free download at  

 

 
 
 
 
 
 
 


 
 