Ask Father
{{Infobox film
| name           = Ask Father
| image          =
| caption        =
| director       =
| producer       = Hal Roach
| writer         = H.M. Walker
| starring       = Harold Lloyd
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = 13 minutes
| country        = United States Silent English English intertitles
| budget         =
}}
 comedy made by Harold Lloyd in 1919 before he got into his classic full-length feature films. Aside from Lloyd, it features Bebe Daniels, a charming and spunky actress who appeared in dozens of films in the 1910s.

==Plot==
Lloyd is a serious young middle-class guy on the make, who wants to marry the boss’ daughter. The problem is getting in to see the boss so that he can ask for her hand in marriage; the office is guarded by a bunch of comic, clumsy flunkies who throw everyone out who tries to get in. When Lloyd gets into the boss’ office, the latter uses trap doors and conveyor belts to expel him; Lloyd then goes to the costume company next door, tries to get in wearing drag (no success), and then in medieval armor – that works, since he bangs everyone over the head with his club, but then he finds out that the daughter has eloped with another suitor. Lloyd decides to be sensible and he settles for the cute switchboard operator (Daniels) instead. The film includes a brief wall climbing sequence. Light-hearted, short, fast-paced.

==Cast==
* Harold Lloyd - The Boy
* Bebe Daniels - Switchboard operator
* Snub Pollard - The Corn-Fed Secretary
* Wallace Howe - The Boss
* Bud Jamison - Guardian at the door
* Noah Young - Large office worker
* Sammy Brooks - Short office worker
* Harry Burns
* Charley Chase - (unconfirmed) (as Charles Parrott)
* William Gillespie - Office worker
* Lew Harvey
* Margaret Joslin
* Dorothea Wolbert Charles Stevenson - The Cop (uncredited)

==See also==
* List of American films of 1919
* Harold Lloyd filmography

==External links==
*  
*  

 
 
 
 
 
 
 
 
 


 