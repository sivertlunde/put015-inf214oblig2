Little 8th Route Army
{{Infobox film
| name = Little 8th Route Army
| image = 8thRouteArmyStoryBook.jpg
| caption =
| imdb_rating =
| director = Lei You
| producer =
| writer =
| starring =
| music =
| cinematography =
| editing =
| distributor =
| released = 1973
| runtime =
| country = China Mandarin
| budget =
| preceded_by =
| followed_by =
| mpaa_rating =
| tv_rating =
}}
 Chinese animated puppet film.  It is also referred to as "Little 8th Route Heroes" and "Small 8th Route Army".

==Background== National Revolutionary 8th Route Army division, when it was under the control of Communist Party of China instead of the opposing Kuomintang.  Despite numerous movie adaptations, this is the only animation piece ever produced on the subject.  A story book was also released as a companion to the film.

==Story==
The story took place during the Second Sino-Japanese War.  The imperial Japanese army entered a northern village in China where young Huzi (虎子) was involved in the anti-Japanese movement.  His sister was taken as part of the village sweep.   Huzi went on the revenge and was rescued by an 8th Route Army chief named Yang.  After Huzi and the party restored the grain supplies and went on the offensive against the Japanese forces.  Eventually Huzi himself was admitted to the 8th Route division. 

==Adaptations==
The first documentary on the subject was the 1938 (延安和八路军).  The first movie to come before this puppetry animation was the 1961 "Little Heroes" (英雄小八路).  Two movies were also made in 1978 "Two Young 8th Route Army Soldiers" (两个小八路) and "We are the 8th Route Army Men" (我们是八路军). 

==References==
 

==External links==
*  

 
 
 
 
 
 