Almayer's Folly (film)
 
{{Infobox film
| name           = Almayers Folly
| image          = AlmayersFolly2011Poster.jpg
| caption        = French poster
| director       = Chantal Akerman
| producer       = Patrick Quinet
| screenplay     = Chantal Akerman
| based on       =  
| starring       = Stanislas Merhar Aurora Marion Marc Barbé
| music          = Steve Dzialowski
| cinematography = 
| editing        = Claire Atherton
| studio         = Liaison Cinématographique Paradise Films
| distributor    = Shellac
| released       =  
| runtime        = 
| country        = France Belgium
| language       = French English
| budget         = 
}}
Almayers Folly ( ) is a 2011 drama film directed by Chantal Akerman, starring Stanislas Merhar, Aurora Marion and Marc Barbé. It is an adaptation of Joseph Conrads 1895 debut novel Almayers Folly, and tells the story of a Dutchman searching for pirate treasure in Malaysia.  The setting has been relocated to 1950s.  The film was a coproduction between companies in France and Belgium. It received four Magritte Award nominations. 

==Cast==
* Stanislas Merhar as Almayer
* Marc Barbé as Captain Lingard
* Aurora Marion as Nina
* Zac Andrianasolo as Daïn
* Sakhna Oum as Zahira
* Solida Chan as Chen
* Yucheng Sun as Captain Tom Li
* Bunthang Khim as Ali

==Production== National Center of Cinematography and 465,000 euro from the Belgian French Community Film and Audiovisual Centre. In addition it was supported through pre-sales investment by Canal+ and CinéCinéma.   

Principal photography took place in Cambodia. Filming started 10 November and ended 24 December 2010. 

==Reception==
The film received critical acclaim from film critics. Review aggregator Rotten Tomatoes reports that 100% out of 8 professional critics gave the film a positive review, with a rating average of 8.5/10. 

==References==
 

==External links==
*    
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 