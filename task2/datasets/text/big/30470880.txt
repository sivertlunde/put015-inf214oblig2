Make a Wish (1937 film)
{{Infobox film
| name = Make a Wish
| image =
| caption = Kurt Neumann
| producer =
| writer =
| starring =
| music = Hugo Riesenfeld
| cinematography =
| editing =
| distributor = RKO Radio Pictures
| released = 27 August 1937
| runtime = 77 min.
| country = United States
| language = English
| budget =
}}
 1937 American Kurt Neumann and starring Bobby Breen, Basil Rathbone and Marion Claire.

==Plot==
Whilst at summer camp in the Maine woods, young Chip Winters (Breen) befriends British composer Johnathan Selden (Rathbone), who left the city high life to try and break his creative block, and is soon playing matchmaker for his widowed singer mother Irene Winters (Claire) and Selden.

==Reception==
The film was a mild success. As of 2013, Make a Wish currently holds a three star rating (6.3/10) on IMDb.

== Cast ==
* Bobby Breen as Chip Winters
* Basil Rathbone Johnny Selden
* Marion Claire as Irene Winters (her only film appearance)
* Ralph Forbes as Walter Mays
* Donald Meek as Butler Joseph
* Billy Lee as Pee Wee
* Henry Armetta as Composer Moreta
* Leon Errol as Composer Brennan
* Herbert Rawlinson as Camp Manager Stevens  Charles Richman as Mr. Wagner

==Awards== Academy Award for best musical score for this film.

==External links==
*  

 
 
 
 
 
 
 


 