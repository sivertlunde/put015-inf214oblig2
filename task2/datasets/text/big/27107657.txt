Bring 'Em Back Alive (film)
{{Infobox Film
| name           = Bring ‘Em Back Alive
| image          = BEBAposter2.jpg
| image_size     = 190px
| caption        = Theatrical poster
| director       = Clyde E. Elliott Amedee J. Van Beuren Frank Buck, Edward Anthony
| narrator       = Frank Buck
| starring       = Frank Buck
| music          = Gene Rodemich
| cinematography = Carl Berger, Nicholas Cavaliere    
| editing        = 
| distributor    = RKO Radio Pictures
| released       = August 19, 1932
| runtime        = 60 or 70 mins.
| country        = United States
| language       = English
| budget         = $100,000.00 
| gross          = $1,044,000 Richard Jewel, RKO Film Grosses: 1931-1951, Historical Journal of Film Radio and Television, Vol 14 No 1, 1994 p39    accessed 19 April 2014 gives the figure as $1.25 million. 
}} Malaya starring Frank Buck.

==Production== Amedee Van Beuren whose studio only made cartoons and live action short subjects released through RKO Studios.  Van Beuren agreed to Bucks conditions that he finance all expenses of Bucks expedition, pay Buck with a share of the profits and not view any of the footage sent back until Buck was present, as Buck was unsure of how the images would actually look on film.  Van Beuren kept his word and when viewing the footage they both realised they had enough film of high quality to make a feature film. 

==Photography==
In Bring ‘Em Back Alive, unlike in most other jungle pictures of the time, director Clyde E. Elliott kept the camera in the background. Neither the camera nor the cameramen are visible in any of the scenes. The result is an infinitely clearer conception of the clashes between tigers, pythons and crocodiles than had been achieved in previous films.  The movie was a huge hit, Elliotts (and Frank Bucks) most successful and popular film.

==Animals photographed==
 
Among the scenes in the film: 
*a fight between a tiger and a black leopard, which, according to Buck, begins as a thrilling battle and winds up as a streetcorner brawl.
*Buck adopts a honey bear and a baby elephant, which is fed cocoanut milk.
*an impressive struggle between a huge python and a crocodile in which the crocodiles back is broken
*the climactic fight, the longest and fiercest of all, is that between the python and a tiger. The tiger closes his great jaws on the python, but the snake succeeds in saving himself, and as the fight goes on the reptile coils itself around the tiger until the cat is panting for breath. Finally, with a supreme effort the tiger frees itself and slinks off. The python is afterward caught by Buck and so is the tiger. The packing of the python gives the moviegoer a good idea of Bucks nerve, for he grabs the pythons head and shoves the reptile into a wooden box.

==Behind the camera==
 
Scenes in the jungle were photographed from blinds erected whenever possible against  the wind to prevent the human scent from blowing toward the animals. Elephants, Buck reported, are especially dangerous in that respect. Their sight is undeveloped, but their sense of smell is hypersensitive. Nick Cavaliere, one of the cameramen, had an encounter with the python who is one of the films unwitting stars. The huge reptile was being photographed from a short distance when suddenly it shot forward, aiming at the film boxes, which lay under the camera tripod. The camera crew fled and the python began to encircle the boxes, probably suspecting them of producing the whirring noise which came from the electric motor of the camera. Cavaliere took a long stick and snapped off the motor, and the python lost interest, released its grip on the boxes and glided away. 
The wild animals did a good deal of their fighting where there was enough light to photograph them, but some of the fiercest bouts continued in the jungle growth where it was not possible to
take a picture. Often Buck, Director Elliott and the cameramen had to run for their lives, the animals being much too close for comfort. 

==Frank Buck describes New York opening==
"I shall never forget the premiere of the picture. The R.K.O. officials had decided it was so good that they would give it a tremendous ballyhoo, take a private theatre (the Mayfair on Times Square) and do the job in Hollywood style. On the day of the opening there was a line of people four deep and a block long, fighting to get into the theatre. On the big marquee were full-sized papier-mâché elephants and tigers that actually moved and waved their trunks and snapped their jaws. I made personal appearances, and for the first time people saw the face of the man who had brought to zoos and menageries of America the animals they had marveled at for so many years." 

==Reception==
The film earned an estimated profit of $155,000. 

==References==
 

==Bibliography==
* {{cite book
  | last = Lehrer 
  | first = Steven
  | title = Bring Em Back Alive: The Best of Frank Buck
  | publisher = Texas Tech University press
  | year = 2006
  | pages = 248
  | url = http://books.google.com/books?id=UNnhbq9gwTUC
  | isbn = 0-89672-582-0 
}}

==External links==
* 

 
 
 
 
 
 
 