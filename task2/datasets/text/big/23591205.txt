En Thangai Kalyani
 
{{Infobox film
| name = En Thangai Kalyani
| image =
| director = T. Rajendar
| writer = T. Rajendar Sudha Srividya S. S. Chandran
| producer = T. Rajendar
| music =  T. Rajendar
| cinematography = T. Rajendar
| editing = K. R. Shanmugam
| studio = Chimbu Cine Arts
| released = 5 February 1988
| runtime = Tamil
| country = India
| gross =  52&nbsp;million
| website =
}}
En Thangai Kalyani is a Tamil film released in 1988 directed and produced by T. Rajendar. Rajender himself appeared in the title role with Sudha (actress)|Sudha, whilst the film featured an extensive cast, which also featured Rajendars real life children, along with a bevy of supporting actors. The film released on 5 February 1988  and was a blockbuster.

==Plot==

Senthamarai deserts Srividhya & his two children Rajendar & Sudha, to live with his concubine. He marries her and lives with her without taking care of his wife & children. Rajendar grows up with hatred to his father. He loves his sister Kalyani (Sudha) very much & buys everything for her. He sends her to college, but she falls in love with her co-student against the wishes of her brother & mother.

The guy to whom she was in love with is a fraudulent & does anything for money. Rajendar tries to warn kalyani, but she elopes with him & marries him. Months later Rajendar finds out that kalyani is ill treated by her husband,who is now living with a dancer. He is ready to do anything for the dancer.

Kalyani becomes pregnant & Srividhya meets her secretly without the knowledge of her son. Soon a son is born to Kalyani. Rajendar is so affectionate to his son-in-law but he does not show it outside. But he did not speak to Kalyani & says he will never forget her.

Senthamarai was ill treated by his concubine who is now his wife. She kept him alive only for his property. She sleeps with another person & Senthamarai finds it. He confronts her & she with the help of her men, beat him to death & force him to sign the stamp paper thereby getting hold of all his property & kicks him out of the home.

He comes to visit Srividhya who invites him with pleasure & provides him food. When Rajendar finds about it he become furious & ask him to leave. Srividhya unwilling to let her husband go alone leaves along with him.

Kalyanis husbands friend comes to her home & expresses his desire to sleep with her. She beats him with broomstick. He in turn ask her husband to leave the dancers home. But he agrees for his desire saying that living with the dancer means everything to him. He tricks Kalyani & take her to his club were their men try to rape her.

Rajendar comes for her rescue. He manages to shoot one of them & spares her husband. But only to be shot by the dying guy whom he shot earlier. He dies & Kalyani also dies immediately.

==Cast==
* T. Rajendar Sudha
* S. S. Chandran
* Srividya
* Silambarasan
* Vadivelu
* Senthamarai

==Songs==
{{Track listing
| headline = Tracklist
| extra_column = Singer(s)
| total_length = 36:12
| all_music =
| lyrics_credits = yes
| title1 = Degam Suduguthu Vaadi 
| extra1 = S. P. Balasubrahmanyam
| lyrics1 = Jothi Kumar
| length1 = 05:20
| title2 = Ellaame En Thangachi
| extra2 = S. P. Balasubrahmanyam
| length2 = 04:27
| lyrics2 = Jothi Kumar
| title3 = Poo Ontru Valarthen
| extra3 = S. P. Balasubrahmanyam
| lyrics3 = Jothi Kumar
| length3 = 03:23
| title4 = Poo Vaangi Vantha Neram
| extra4 = S. P. Balasubrahmanyam
| length4 = 04:39+
| title4  =thanane  thananenea
| lyrics4 = Jothi Kumar
| title5 = Thangachikku Seemantham 
| extra5 = S. P. Balasubrahmanyam
| length5 = 03:05
| lyrics5 = Jothi Kumar
| title6 = Thol Meethu Thaalaatta
| extra6 = S. P. Balasubrahmanyam
| length6 = 04:38
| lyrics6 = Jothi Kumar
| title7 = Viradathil Naano
| extra7 = S. P. Balasubrahmanyam
| length7 = 04:43
| lyrics7 = Jothi Kumar
| title8 = Poottaane Moonu Mudichithan 
| extra8 = S. P. Balasubrahmanyam
| length8 = 05:37
| lyrics8 = Jothi Kumar
}}

==References==
 

==External links==
*  

 

 
 
 
 
 