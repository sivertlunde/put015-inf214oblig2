Painkiller Jane (film)
{{Infobox film
| name           = Painnkiller Jane
| image          = Painkiller jane movie poster.jpg
| image_size     = 220
| caption        = Movie poster
| director       = Sanford Bookstaver
| producer       = John Harrison John Harrison Don Keith Opper Greg Gold Joe Quesada Jimmy Palmiotti
| narrator       =
| starring       = Emmanuelle Vaugier, Eric Dane, Richard Roundtree Brian Tyler
| cinematography = Stephen McNutt
| editing        = Andrew Seklir
| studio         = GEP Productions
| distributor    = Sci Fi Channel (United States)|Sci-Fi Channel
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}
 comic book character of the same name.  It was first broadcast on the Sci Fi Channel (United States)|Sci-Fi Channel in December 2005.  The 2-hour film stars Emmanuelle Vaugier as the titular heroine.  The film differs significantly from the story of the comic book character.

The movie was a backdoor pilot for a possible television series, which was eventually approved.  The Painkiller Jane (TV series)|Painkiller Jane TV series appears to be a "Reboot (continuity)|reboot" of the TV-movie, discarding the films backstory and starting anew.

== Plot ==
A Special Forces unit, known as the Painkiller Unit, is exposed to a biochemical weapon while on a mission in Sovetskaia, Chechnya.  After attempting to retreat, the entire unit is ambushed and executed by a group of armed men in protective hazmat suits. One member of the unit, Captain Jane Browning (Emmanuelle Vaugier), not only survives the execution, but recovers from the viral infection, and develops an abnormally rapid healing factor, increased dexterity, speed, strength, enhanced senses, mental abilities and a photographic memory.

Hensley admits that he is in fact Peter Erfan and has a vial of liquid which he has extracted from Jane.  As he is talking with her the assassin starts to move in on her and she shoots him.  Erfan refers to her as the new Eve, as something in her body altered the virus and then the virus changed her.  This is why she survived but the rest of her unit did not.  Erfan has been able to recreate this modified virus and already gave it to the assassin.  As he is talking with her the assassin that was shot gets up and rips off a power box on the wall and hits Jane in the head with it.

The final scene sees a group of Asian gentlemen walking into a laboratory and meet with Lucas Hensley who apparently survived the fall unharmed. He holds a vial of green fluid, the genetic material for the enhancement virus in solution. The implication is that Erfan took the serum himself, in order to survive the impact with river and he will soon be creating an army of superhumans for the unnamed Asian business partner.

== Cast ==
*Emmanuelle Vaugier as Captain Jane Elizabeth Browning
*Eric Dane as Nick Pierce
*Richard Roundtree as Colonel Ian Watts - Janes supervising officer
*Nels Lennarson as Agent Thorpe
*Richard Harmon as Squeak
*Tate Donovan as Dr. Graham Knight/Lucas Insley
*Venus Terzo as Carla Browning - Janes sister

==Reboot==
In an interview with Moviepilot, Palmiotti is in progressing on a new film. 

==References==
 

==External links==
* 
 

 
 
 
 
 
 
 
 