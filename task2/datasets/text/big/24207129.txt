A Single Man
 
 
{{Infobox film
| name           = A Single Man
| image          = A Single Man.jpg
| caption        = Theatrical release poster
| director       = Tom Ford
| producer       = Tom Ford Andrew Miano Robert Salerno Chris Weitz
| screenplay     = Tom Ford David Scearce
| based on       =  
| starring       = Colin Firth Julianne Moore Matthew Goode Nicholas Hoult
| music          = Abel Korzeniowski  
| cinematography = Eduard Grau
| editing        = Joan Sobel
| studio         = Artina Films Depth of Field Fade to Black
| distributor    = The Weinstein Company
| released       =  
| runtime        = 100 minutes
| country        = United States
| language       = English
| budget         = $7 million 
| gross          = $24.9 million 
}}
 novel of directorial debut Best Actor gay British British university professor living in Southern California in 1962.
 34th Toronto International Film Festival, The Weinstein Company picked it up for distribution in the United States and Germany. An initial limited run in the United States commenced on December 11, 2009, to qualify it for the 82nd Academy Awards with a wider release in early 2010.   

== Plot ==
Taking place over the course of a single day, November 30, 1962, a month after the Cuban missile crisis, A Single Man is the story of George Falconer (Colin Firth), a middle-aged English college professor living in Los Angeles. George dreams that he encounters the body of his longtime partner, Jim (Matthew Goode), at the scene of the car accident that took Jims life eight months earlier. After awakening, George delivers a voiceover discussing the pain and depression he has endured since Jims death and his intention to commit suicide that evening.

George receives a phone call from his dearest friend, Charley (Julianne Moore), who projects lightheartedness despite her being equally miserable. George goes about his day putting his affairs in order and focusing on the beauty of isolated events, believing he is seeing things for the last time. Throughout, there are flashbacks to George and Jims sixteen-year-long relationship.

During the school day George comes into contact with a student, Kenny Potter (Nicholas Hoult), who shows interest in George and disregards conventional boundaries of student-professor discussion. George also forms an unexpected connection with a Spanish male prostitute, Carlos (Jon Kortajarena). That evening George meets Charley for dinner. Though they initially reminisce and amuse themselves by dancing, Charleys desire for a deeper relationship with George and her failure to understand his relationship with Jim angers George.

George goes to a bar and discovers that Kenny has followed him. They get a round of drinks, go skinny dipping, and then return to Georges house and continue drinking. George passes out and wakes up alone in bed with Kenny asleep in another room. George gets up and while watching Kenny discovers that he had fallen asleep holding Georges gun, taken from the desktop, to keep George from committing suicide. George locks the gun away, burns his suicide notes and in a closing voiceover explains how he has rediscovered the ability "to feel, rather than think". As he makes peace with his grief, George suffers a heart attack and dies.

== Cast ==
* Colin Firth as George Falconer
* Julianne Moore as Charlotte (Charley)
* Nicholas Hoult as Kenny Potter
* Matthew Goode as Jim
* Jon Kortajarena as Carlos
* Paulette Lamori as Alva
* Ryan Simpkins as Jennifer Strunk
* Ginnifer Goodwin as Mrs. Strunk
* Teddy Sears as Mr. Strunk
* Lee Pace as Grant Lefanu
* Erin Daniels as Bank Teller
* Aline Weber as Lois

Jon Hamm has an uncredited voice cameo as Harold Ackerly. He is a cousin of Jim who phones George to inform him of his partners death. 

== Production ==
Fashion designer Tom Ford, as a first-time director, financed the film himself.  The film places emphasis on the culture of the 1960s; the production design is by the same team that designed AMC televisions Mad Men, which is set in the same era. Mad Men star Jon Hamm has an uncredited voice cameo as Georges lovers cousin.    The actual house where the character George lives in the film was designed in 1948 by John Lautner, his first house after leaving Frank Lloyd Wright. 

The film was shot in 21 days, according to "The Making of A Single Man" on the DVD. 

== Marketing controversy ==
An early theatrical poster for A Single Man featured a close-up shot of Colin Firth and Julianne Moore lying side by side, their arms and shoulders touching. This led to speculation that the works gay content and themes were being deleted or diminished in its marketing materials to improve its chances of success with a wider audience. A new poster with Moore relocated to the background was issued. The films original trailer placed more emphasis on the relationship between George and Jim but a re-cut trailer omitted a shot of George and Jim kissing while retaining a kiss between George and Charley. Also deleted were a shot of George staring into a male students eyes, while keeping a shot of George staring into the eyes of a female student, shots of George meeting hustler Carlos outside a liquor store, and shots of George and Kenny running nude into the ocean.   

Speaking of the controversy, Moore said that director Tom Ford expressed concern that the original poster made the film appear to be a romantic comedy and that he ordered that the poster be changed.  However Ford, noting he does not see the film in terms of gay or straight, said, "I dont think the movies been de-gayed. I have to say that we live in a society thats pretty weird. For example, you can have full-frontal male nudity on HBO, yet in cinema, you cant have naked male buttocks. You cant have men kissing each other without it being considered adult content. So, in order to cut a trailer that can go into broad distribution in theaters, certain things had to be edited out. But it wasnt an intentional attempt to remove the gayness of the movie."  Conversely, Colin Firth said, "  is deceptive. I dont think they should do that because theres nothing to sanitize. Its a beautiful story of love between two men and I see no point in hiding that. People should see it for what it is."  Harvey Weinstein would only say, when asked about the revised poster, "Im good. You got enough. Thank you."  Peter Knegt of Indiewire suggested that The Weinstein Company "de-gayed" the trailer to better the films chances of receiving Academy Award nominations. 

== Reception ==
=== Critical response ===
 

The film has received an overall positive reception from critics, with most reviews singling out Colin Firths performance. It currently holds an 85% "Fresh" rating on Rotten Tomatoes, based on 175 reviews, with the sites consensus being that "Though the costumes are beautiful and the art direction impeccable, what stands out most from this debut by fashion designer Tom Ford is the leading performance by Colin Firth."  Metacritic has compiled an average score of 77 (generally favorable reviews) from 35 critic reviews. 

Michael Phillips from the Chicago Tribune wrote "Some films aren’t revelations, exactly, but they burrow so deeply into old truths about love and loss and the mess and thrill of life, they seem new anyway"    Bob Mondello from NPR commented “An exquisite, almost sensual grief suffuses every frame of A Single Man.”  Marc Savlov from The Austin Chronicle wrote “Everything fits perfectly, from titles to fin, but most of all Colin Firth, who dons the role of George like a fine bespoke suit.” 

Critics who liked the film include The A. V. Club film critic Nathan Rabin, who gave the film an A- score, arguing that "A Single Man is a film of tremendous style wedded to real substance, and rooted in "Firths affecting lead performance as a man trying to keep it together for one last day after his world has fallen apart."  Critic Roger Ebert from the Chicago Sun-Times also praised Firth, saying that he "plays George superbly, as a man who prepares a face to meet the faces that he meets. He betrays very little emotion, and certainly his thoughts cannot be read in his eyes." 

 s verdict: "Luminous and treasurable, despite its imperfections. An impressive helming debut for fashion designer Tom Ford." 

== Accolades == Queer Lion. BAFTA Award Best Actor Golden Globe, Screen Actors Academy Award Best Supporting Best Original Grand Prix from the Belgian Syndicate of Cinema Critics. 
 Outstanding Film – Wide Release at the 21st GLAAD Media Awards.   

== References ==
 

== Further reading ==
 
*  
 

== External links ==
*  
*  
*  
*  
*  
*  
*   at CharlieRose.com

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 