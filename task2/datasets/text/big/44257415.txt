A Pocketful of Chestnuts
{{Infobox film
 | name = A Pocketful of Chestnuts
 | image = A Pocketful of Chestnuts.jpg
 | caption =
 | director =  Pietro Germi
 | writer =  Leo Benvenuti   Piero De Bernardi  Tullio Pinelli Pietro Germi
 | starring =  
 | music =   Carlo Rustichelli
 | cinematography = Aiace Parolin 
 | editing =  	  
 | producer =   Italian
 | released =  1970
 }} 1970 Italian comedy film directed by Pietro Germi.         
==Plot==
Luigi Vivarelli is a television director, a cynical and unrepentant womanizer. One day he meets Carla Lotito, an architecture student, and is immediately attracted to her. But Carla isnt like the other women that Luigi deals with - shes a Catholic who believes in the old and simple values that modern society deems unfashionable. Both try to convince the other of the rightness of their philosophy, and their basic disagreements put their love in jeopardy.

== Cast ==
* Gianni Morandi: Luigi Vivarelli
* Stefania Casini: Carla Lotito
* Nicoletta Machiavelli: Teresa Lotito
* Patricia Allison: Lisa Lotito
* Franco Fabrizi: Bernardo Bembarbì
* Milla Sannoner: Maria Luisa
* Gigi Reder: Television Host
* Memè Perlini: Actor-Priest
* Stephan Zacharias: Don Raffaele
* Giuseppe Rinaldi: Poker Player Doctor

==Reception==
In his previous films, Pietro Germi sharply satirized the hypocritical morality and conformity of proper Italian society. In this film, however, he turned his scorn towards the "immoral" youth of the 1960s, defending the conservative values. For this reason the film was heavily attacked by Italian critics, criticizing it as didactic and reactionary. Germi angrily responded that "critics are only good for a sociological study - their reaction is proof of the cultural degeneration that they are a part of". 

==References==
 

==External links==
* 
 

 
 
  
 
 
  


 
 