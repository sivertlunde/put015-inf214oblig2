Battles of Chief Pontiac
{{Infobox film
| name           = Battles of Chief Pontiac
| image          = Battles of Chief Pontiac 1952 Poster.jpg
| image_size     =
| alt            =
| caption        = Theatrical release poster
| director       = Felix E. Feist
| producer       = Irving Starr Jack Broder
| screenplay     = Jack DeWitt
| starring       = Lex Barker Helen Westcott Lon Chaney Jr.
| music          = Elmer Bernstein
| cinematography = Charles Van Enger
| editing        = Philip Cahn
| studio         = Jack Broder Productions Inc.
| distributor    = Realart Pictures
| released       =  
| runtime        = 72 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Battles of Chief Pontiac is a 1952 American quasi historical film directed by Felix E. Feist.  The drama features Lex Barker, Helen Westcott  and Lon Chaney Jr.. 

==Plot== Ottawa Indigenous Native Americans, led by Chief Pontiac, and the British.

==Cast==
* Lex Barker as Lt. Kent McIntire
* Helen Westcott as Winifred Lancaster
* Lon Chaney Jr. as Chief Pontiac
* Berry Kroeger as Col. von Weber
* Roy Roberts as Maj. Gladwin
* Larry Chance as Hawkbill
* Katherine Warren as Chia
* Ramsay Hill as Gen. Sir Jeffrey Amherst
* Guy Teague as Von Webers aide
* James Fairfax as Guardhouse sentry
* Abner George as Doctor

==Reception==
===Critical response===
Film critic Hans J. Wollstein gave the film a mixed review, writing, "Burly actor Lon Chaney, Jr. said that the role of Chief Pontiac in this cheap action-thriller from low-budget entrepreneur Jack Broder, was one he truly coveted. Unfortunately, Chaney and the rest of the cast were let down by a preachy and frankly dreadful screenplay by Jack de Witt ... Battles of Chief Pontiac was filmed on location near Rapid City, South Dakota, and the scenery remains its only attractive feature." 

==References==
 

==External links==
*  
*  
*  
*   film at Hulu (free and complete)

 
 
 
 
 
 
 
 
 
 
 