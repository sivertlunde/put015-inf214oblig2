Jushin Thunder Liger: Fist of Thunder
{{Infobox Film
| name           = Jushin Thunder Liger: Fist of Thunder
| image          = Jushin Thunder Liger - Ikari no Raimei (VHS 1995).JPG
| image_size     =
| caption        = Cover of the original VHS release
| director       = Takuya Wada
| producer       = 
| writer         = Takuya Wada   Masanobu Utsugi   Go Nagai (original character)
| narrator       = 
| starring       = Jushin Thunder Liger   Masaru Matsuda   Reiko Kataoka   Muscle Kitamura   Satoshi Kojima   Yuji Nagata   Manabu Nakanishi   Shinjiro Otani   Tatsuhito Takaiwa
| music          = ZNX (songs)
| cinematography = Tatsuhiko Maeyama
| editing        = 
| distributor    = Bandai Visual
| released       = 1995-02-21
| runtime        = 91 minutes
| country        =   Japanese
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 Bandai Visual, Jushin Liger created by Go Nagai, who supervised this films production.  In this film, Liger and his arch-enemy Bounty Viper turn into demon-beast versions of themselves, both designed by veteran makeup artist Steve Wang.

In this film, Jushin Thunder Liger, when unmasked, is played by actor Masaru Matsuda (to protect the real Ligers identity).

==Plot==
Popular wrestler Jushin Thunder Liger is having fits of dizziness and headaches, as he recalls childhood memories of being chased by demons. Helping him is a woman photographer named Miki Aizawa (Reiko Kataoka).

Ligers fears will be realized when a match is announced that will have him in a three-way bout against an evil gaijin wrestler named Bounty Viper (played by wrestler Muscle Kitamura) and another wrestler named Riot Orff (played by wrestler Shinjiro Otani). Viper, in particular, has plans for Liger...

While Liger copes with his childhood fears, he gets ready for the three-way wrestling match. When the big fight begins on that night, the fight seems tough for Liger as he realizes that theres something very peculiar about Bounty Viper. He has demonic powers, similar to the demons that persecuted him as a child. In the course of the fight, Viper displays his wickedness by violently maiming Riot Orff. Liger eventually becomes violent, and when he and Viper tear into each other, both wrestlers begin to pulsate and glow with energy. In light speed, they dash out of the ring into a deserted street where they transform into demonic versions of themselves (Liger, of course, resembling his costumed self, and Viper superficially resembling a blue version of the Predator (alien)|Predator) and a savage fight to the finish ensues.

==External links==
* 
*    at allcinema
*    at the Japanese Movie Database
*    at the Japanese Horror Movies Database
*  at Superheroes Lives website

 
 
 
 
 
 
 
 