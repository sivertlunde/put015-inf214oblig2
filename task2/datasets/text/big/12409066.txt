Kasak (2005 film)
{{Infobox film
| name           = Kasak
| image          = Kasak.jpg
| caption        =
| director       = Rajiv Babbar
| producer       =
| writer         = Rajiv Babbar
| starring       = Lucky Ali  Meera (actress)
| music          = M. M. Kreem
| cinematography =
| editing        =
| released       =  
| runtime        =
| country        = India
| language       = Hindi
| budget         =
| gross =
}} Meera the Pakistani actress has made whilst in Bollywood.

==Plot==
Kasak is about the hero, Amar and the vamp/heroine Anjali. Amar and Anjali meet in a hospital where he takes care of an old lady who is in coma. They fall deeply in love. When the old lady (who is very rich) awakens from coma, her son tries to get her will changed so that he is the sole beneficiary. Disgusted, she names Amar as the beneficiary and dies.

Amar doesnt want the money since he is happy with Anjali. But Anjali convinces him to take the money claiming that he would be disrespecting the old ladys last wish by refusing. Amar and Anjali get married soon after. Once married, she convinces Amar to transfer the funds to her name. All Amar wants is her love and does so.

She shows her true colors soon after and throws him out. Even then Amar is still in love with her and comes to her to convince her to keep the money but still be with him. He doesnt want the money. She realizes that Amar wont let her go and creates a false case of domestic abuse against Amar.

Amar gets jailed and when he comes out, he becomes the bodyguard of a criminal Don. Soon Amar becomes a big criminal and becomes very rich.

But he is still in love with Anjali and constantly tries to contact her while she keeps on humiliating him. Finally she plots to kill him and seemingly succeeds. But she is exposed and gets jailed. In jail, she is shocked to see Amar visiting her who tells her that he realized that she would stop at nothing. And she has never understood his love and always mistreated him. He found out about her plot to kill him and pretended to be killed. Now she will be in jail forever and be the only person who knows that he is still alive. Hopefully she will then love him. Amar then gives up his criminal life and starts working with orphans.

                       Soundtrack Information Editing Dj Akash (AK)

Jaana Hai Jaana Hai             ||     Lucky Ali         ||     DJ Akash (AK)
 
Saansein Madham Hain            ||     Shreya Ghoshal    ||     DJ Akash (AK)
 
Todh Diya                       ||     Lucky Ali & Meera ||     DJ Akash (AK)

Yeh Zindagi Hai                 ||     Sunidhi Chauhan   ||     DJ Akash (AK)

Jaana Hai Jaana Hai - Part II   ||     Madhushree        ||     DJ Akash (AK)
 
Jaana Hai Jaana Hai - Par III   ||     Madhushree        ||     DJ Akash (AK)
   
Main Na Janoo Kaisi Kasak Hai   ||     Gayatri Iyer      ||     DJ Akash (AK)

Chandni Hai Khoi Khoi           ||     Lucky Ali         ||     DJ Akash (AK)

Bechainiyon Mein Lamha          ||     M M Kreem         ||     DJ Akash (AK)

==Soundtrack==
* Jaana Hai Jaana Hai
* Saansein Madham Hain
* Todh Diya HAI
                            
* Yeh Zindagi Hai
* Jaana Hai Jaana Hai - Part II
* Jaana Hai Jaana Hai - Par III
* Main Na Janoo Kaisi Kasak Hai
* Chandni Hai Khoi Khoi
* Bechainiyon Mein Lamha
Music director M.M. Kreem composed this soundtrack which was received well.He reused tune of his Telugu film song "Nuvve Naa Shwasa" from "Okariki Okaru" for "Saansein Maddham Hai & "Bechainiyon Mein Lamha".

==External links==
*  

 

 
 
 
 
 


 