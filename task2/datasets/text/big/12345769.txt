Rhubarb (1951 film)
{{Infobox film
| name           = Rhubarb
| image          = Rhubarb 1951.jpg
| alt            = 
| caption        = 
| film name      = 
| director       = Arthur Lubin
| producer       = William Perlberg George Seaton	
| writer         = David Stern
| screenplay     = Dorothy Davenport Francis M. Cockrell
| story          = 
| based on       =  
| narrator       = 
| starring       = Ray Milland Jan Sterling Gene Lockhart
| music          = Van Cleave 	
| cinematography = Lionel Lindon
| editing        = Alma Macrorie
| studio         = 
| distributor    = Paramount Pictures
| released       = 29 August 1951
| runtime        = 94 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 1951 film adapted from the 1946 novel Rhubarb by humorist H. Allen Smith. Directed by Arthur Lubin, the screwball noir comedy stars the cat Orangey along with Jan Sterling and Ray Milland. Cinematography was by Lionel Lindon.

Fourteen different cats portrayed Rhubarb at different points in the film. Each cat was trained to do a different trick. Three of the most identical cats appeared in the courtroom scene where Polly Sickles has to choose which one is the real Rhubarb. 

==Plot== 
Thaddeus J. Banner (Gene Lockhart), a lonely, eccentric millionaire who owns a baseball team, the Brooklyn Loons, takes a liking to a dog-chasing stray cat (played by Orangey), and takes him into his home. He names the cat "Rhubarb," which is baseball slang for an on-field argument or fight.

When the man dies, it is discovered that his last will and testament made Rhubarb his sole beneficiary; hence the cat inherits the baseball team. Team publicist Eric Yeager (Ray Milland) is named the cats guardian. His fiancee Polly Sickles (Jan Sterling), daughter of the teams manager (William Frawley), is terribly allergic to Rhubarb, causing many problems.

Banners unhappy daughter Myra (Elsie Holmes) files a lawsuit, contesting the will. And when the teams players discover they are owned by a cat, they stage a protest until Yeager persuades them that Rhubarb brings them luck.

Brooklyn begins winning and will play the powerful New York team for the championship. But a bookie who stands to lose big if Brooklyn wins decides to kidnap the cat. Brooklyns fortunes turn for the worse while the search for Rhubarb goes on, until the cat finally escapes from his captors and races to the ballpark to save the day.

==Cast==
* Orangey as Rhubarb
* Ray Milland as Eric Yeager
* Jan Sterling as Polly Sickles
* William Frawley as Len Sickles
* Gene Lockhart as T.J. Banner
* Elsie Holmes as Myra Banner
* Taylor Holmes as P. Duncan Munk
* Willard Waterman as Orlando Dill
* Henry Slate as Dud Logan
* James Griffith as Ogelthorpe Oggie Meadows
* Jim Hayward as Doom
* Donald MacBride as Pheeny
* Hal K. Dawson as Mr. Fisher

==Reception==
The film earned an estimated $1.45 million at the US box office in 1951. 

==Awards== Breakfast at Tiffanys, the only cat so far to win more than once. 

Strother Martin and Leonard Nimoy have uncredited roles in this film.

The film was released on DVD by Legend Films on July 1, 2008.

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 

 