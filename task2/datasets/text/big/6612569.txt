Little Murders
 
{{Infobox film
|  name           = Little Murders
|  image          =
|  director       = Alan Arkin
|  producer       = Jack Brodsky
|  writer         = Jules Feiffer John Randolph Doris Roberts Donald Sutherland Lou Jacobi Alan Arkin
|  music          = Fred Kaz  
|  editing        = Howard Kuperman
|  cinematography = Gordon Willis
|  distributor    = 20th Century Fox
|  released       = February 9, 1971
|  runtime        = 108 minutes
|  followed by    =
|  country        = United States
|  language       = English
|  budget         = $1,340,000 
|  gross          = $1,500,000 (US/ Canada) 
}}

Little Murders is a 1971 black comedy film starring Elliott Gould and Marcia Rodd, directed by Alan Arkin in his feature directorial debut. It is the story of a girl, Patsy (Rodd), who brings home her boyfriend, Alfred (Gould), to meet her severely dysfunctional family amidst a series of random shootings, garbage strikes and electrical outages ravaging the neighborhood. 

The film originated as a play written by cartoonist Jules Feiffer which was staged on Broadway in 1967 but which lasted only seven performances. This failure was followed by a successful London production by the Royal Shakespeare Company, directed by Christopher Morahan at the Aldwych Theatre.

It was then revived Off-Broadway in 1969 by Circle in the Square in New York City, directed by Arkin with a cast that included Linda Lavin, Vincent Gardenia, and Fred Willard. That production ran for 400 performances, and won Feiffer an Obie Award. Lavin won the 1969 Outer Critics Circle Award for Best Performance.
 John Randolph and Doris Roberts). 

The film opened to a lukewarm review by Roger Greenspan,  and a more positive one by Vincent Canby  in the New York Times. Roger Eberts review in the Chicago Sun Times was more enthusiastic, saying, "One of the reasons it works, and is indeed a definitive reflection of Americas darker moods, is that it breaks audiences down into isolated individuals, vulnerable and uncertain." 

==Plot==
Patsy Newquist is a 27-year-old interior designer who lives in a New York rife with street crime, noise, obscene phone calls, power blackouts and unsolved homicides. When she sees a defenseless man being attacked by street thugs, she intervenes, but is surprised when the passive victim doesnt even bother to thank her. She ends up attracted to the man, Alfred Chamberlain, a photographer, but finds that he is emotionally vacant, barely able to feel pain or pleasure. He permits muggers to beat him up until they get tired and go away.

Patsy is accustomed to molding men into doing her bidding. Alfred is different. When she brings him home to meet her parents and brother, he is almost non-verbal, except to tell her that he doesnt care for families. He learns that Patsy had another brother who was murdered for no known reason. Patsys eccentric family is surprised when she announces their intention to wed, then amazed when their marriage ceremony conducted by the atheistic Rev. Dupas turns into a free-for-all.

Determined to discover why her new husband is the way he is, Patsy coaxes Alfred into traveling to Chicago to visit his parents. He hasnt seen them since he was 17, but asks them to help with a questionnaire about his childhood at Patsys request.

Alfred ultimately agrees to try to become Patsys kind of man, the kind willing to "fight back". The instant that happens, a snipers bullet kills Patsy, again for no apparent reason. A blood-splattered Alfred goes to her parents apartment, New Yorkers barely noticing his state. He descends into a silent stupor, Patsys father even having to feed him.

A ranting, disturbed police detective, Lt. Practice, drops by, almost unable to function due to the number of unsolved murders in the city. After he leaves, Alfred goes for a walk in the park. He returns with a rifle, which he doesnt know how to load. Patsys father shows him how. Then the two of them, along with Patsys brother, take turns shooting people down on the street.

==Cast==
* Elliott Gould as Alfred Chamberlain
* Marcia Rodd as Patsy Newquist
* Vincent Gardenia as Patsys Father (reprising 1969 stage role)
* Elizabeth Wilson as Patsys Mother (reprising 1969 stage role)
* Jon Korkes as Patsys Brother (reprising 1969 stage role) John Randolph as Alfreds Father
* Doris Roberts as Alfreds Mother
* Donald Sutherland as the Rev. Henry Dupas
* Lou Jacobi as Judge Stern
* Alan Arkin as Lt. Practice

==In popular culture==
* On the album Cowbirds and Cuckoos by Ryland Bouchard there is a song titled "Little Murders".
* On the popular AMC television show Mad Men, set in the 1960s, the character of Megan Draper auditions for the original stage production.

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 