Footloose (1984 film)
 
 
{{Infobox film
| name           = Footloose
| image          = FootloosePoster.jpg
| alt            =
| caption        = Picture of Footloose (1984) movie poster
| director       = Herbert Ross
| producer       = Lewis J. Rachmil Craig Zadan
| writer         = Dean Pitchford
| starring = {{Plainlist|
* Kevin Bacon
* Lori Singer
* Dianne Wiest
* John Lithgow
}} Tom Snow Jim Steinman Kenny Loggins Dean Pitchford (lyrics) Miles Goodman (adaptor)
| cinematography = Ric Waite Paul Hirsch
| studio         = IndieProd Company Productions
| distributor    = Paramount Pictures
| released       =  
| runtime        = 107 minutes
| country        = United States
| language       = English
| budget         = $8.2 million
| gross          = $80 million (domestic)
}} dancing and rock music have been banned.

The film is loosely based on events that took place in the small, rural, and religious community of Elmore City, Oklahoma. 

==Plot==
Ren McCormack, a teenager raised in Chicago, moves to the small town of Bomont to live with his aunt and uncle. Soon after arriving, Ren makes a friend named Willard, and from him learns the city council has banned dancing and rock music. He soon begins to fall for a rebellious girl named Ariel, who has a boyfriend, Chuck Cranston, and a domineering father, Reverend Shaw Moore, an authority figure in the town.
 chicken involving tractors, and despite having never driven one before, he wins. Rev. Moore distrusts Ren, grounding Ariel and forbidding her to see him. Ren and his classmates want to do away with the no dancing law and have a senior prom.
 burning books that they think are dangerous to the youth. Realizing the situation has gotten out of hand, Moore stops the burning.

On Sunday, Rev. Moore asks his congregation to pray for the high school students putting on the prom, which is set up at a grain mill outside of the town limits. Moore and Vi are seen outside, dancing for the first time in years.

==Cast==
  portrayed Ren McCormack.]]
* Kevin Bacon as Ren McCormick
* Lori Singer as Ariel Moore
* Dianne Wiest as Vi Moore
* John Lithgow as Reverend Shaw Moore
* Chris Penn as Willard Hewitt
* Sarah Jessica Parker as Rusty
* Frances Lee McCain as Ethel McCormick
* Jim Youngs as Chuck Cranston John Laughlin as Woody
* Lynne Marta as Lulu Warnicker
* Douglas Dirkson as Berlington Cranston

==Production==
Dean Pitchford wrote the screenplay (and most of the lyrics) for Footloose, Herbert Ross directed the movie, and Paramount Pictures co-produced and distributed it.

Michael Cimino was hired by Paramount to direct the film when negotiations with Ross initially stalled. After four months working on the film, the studio fired Cimino, who was making extravagant demands for the production, including demanding an additional $250,000 for his work, and ended up rehiring Ross. 

===Casting=== All the Right Moves.     Lowe auditioned three times and had dancing ability and the "neutral teen" look that the director wanted, but injury prevented him from taking the part.     Bacon had been offered the main role for the Stephen King movie Christine (1983 film)|Christine at the same time that he was asked to do the screen test for Footloose. He chose to take the gamble on the screen test. After watching his earlier movie Diner (film)|Diner, the director had to convince the producers to go with Bacon. 
 Madonna also auditioned. Daryl Hannah turned down the offer to play Ariel in order to play Madison in Splash (film)|Splash. Elizabeth McGovern turned down the role to play Deborah Gelly in Once Upon a Time in America. Melanie Griffith, Michelle Pfeiffer, Jamie Lee Curtis, Rosanna Arquette, Meg Tilly, Julia Louis-Dreyfus, Heather Locklear, Meg Ryan, Jennifer Jason Leigh, Jodie Foster, Phoebe Cates, Tatum ONeal, Bridget Fonda, Lori Loughlin, Diane Lane and Brooke Shields were all considered for the role of Ariel. Dianne Wiest appeared as Vi, the Reverends devoted yet conflicted wife.

Footloose featured an early film appearance by Sarah Jessica Parker as Ariels friend Rusty, for which she received a Best Young Supporting Actress in a Motion Picture Musical, Comedy, Adventure or Drama nomination at the Sixth Annual Youth in Film Awards. It was also an early role for Chris Penn as Willard Hewitt, who is taught how to dance by his friend Ren.

===Filming=== Payson High Lehi Roller Mills featured in the final sequence.

For his dance scene in the warehouse, Bacon said he had four stunt doubles: "I had a stunt double, a dance double    and two gymnastics doubles."   

==Soundtrack==
 
 
The  , "Hurts So Good" by John Mellencamp, "Waiting for a Girl Like You" by Foreigner (band)|Foreigner, and the extended 12" remix of "Dancing in the Sheets".
 title song, Welsh singer Moving Pictures (the song played during Bacons solo dance scene); three singles—"Lets Hear It for the Boy (song)|Lets Hear It for the Boy" by Deniece Williams, "Somebodys Eyes" by Karla Bonoff, and "Dancing in the Sheets" by Shalamar; and the love theme "Almost Paradise" by Mike Reno from Loverboy and Ann Wilson of Heart (band)|Heart. Some of the songs were composed by Eric Carmen and Jim Steinman and the soundtrack went on to sell over 9 million copies in the USA.
 Best Music (Original Song). "Footloose" also received a 1985 Golden Globe Award nomination for Best Original Song – Motion Picture.

The late film composer Miles Goodman has been credited for adapting and orchestrating the films score.      

==Reception==

===Critical reception===
 
The film received mixed reviews, holding a 56% approval rating on Rotten Tomatoes based on 36 reviews, with the critics consensus: "Theres not much dancing, but whats there is great. The rest of the time, Footloose is a nice hunk of trashy teenage cheese.". 
 New York rechristened the film "Schlockdance", writing: "Footloose may be a hit, but its trash - high powered fodder for the teen market... The only person to come out of the film better off is the smooth-cheeked, pug-nosed Bacon, who gives a cocky but likeable Mr. Cool performance." 

Jane Lamacraft reassessed the film for Sight and Sound  "Forgotten pleasures of the multiplex" feature in 2010, writing "Nearly three decades on, Bacons vest-clad set-piece dance in a flour mill looks cheesily 1980s, but the rest of Rosss drama wears its age well, real song-and-dance joy for the pre-Glee generation." 

===Box office===
Despite critical reviews, the film grossed $80,035,403 domestically. 

===American Film Institute lists===
*AFIs 100 Years...100 Songs:
**"Footloose (song)|Footloose" – #96
*AFIs 100 Years...100 Cheers – Nominated 

==Musical version==
 
  West End, on Broadway theater|Broadway, and elsewhere. The musical is generally faithful to the film version, with some slight differences in the story and characters.

==Remake==
 
Paramount Pictures announced plans to fast-track a musical remake of Footloose.  The remake was written and directed by Craig Brewer.

Paramount revealed the full cast on June 22, 2010,  with  , October 12, 2011 

Filming started in September, 2010. It was budgeted at $25 million.  The release date was October 14, 2011.

==References==
 

==External links==
 
 
*  
*  
*  
*  
*   at The Numbers
*   Review, history and filming locations

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 