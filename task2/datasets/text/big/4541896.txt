Next Stop Wonderland
{{Infobox film
| name = Next Stop Wonderland
| image = Next stop wonderland.jpg
| image_size = 215px
| alt = 
| caption = Theatrical release poster Brad Anderson
| producer = Mitchell Robbins Laura Bernieri Rachael Horovitz
| writer = Brad Anderson Lyn Vaus
| starring = Hope Davis Alan Gelfant Philip Seymour Hoffman Cara Buono
| music = Claudio Ragazzi
| cinematography = Uta Briesewitz
| editing = Brad Anderson
| studio = Robbins Entertainment
| distributor = Miramax Films
| released =  
| runtime = 104 minutes
| country = United States
| language = English
| budget =  1 million
| gross =  3,395,581 
}} Brad Anderson and written by Anderson and Lyn Vaus.

==Plot== Native American marine biologist MBTA train Wonderland station, on the outskirts of Boston.

==Cast==
* Hope Davis as Erin Castleton
* Alan Gelfant as Alan Monteiro
* Philip Seymour Hoffman as Sean
* Cara Buono as Julie
* José Zúñiga as Andre de Silva
* Sam Seder as Kevin Monteiro
* Callie Thorne as Cricket
* Jimmy Tingle as Bartender
* Holland Taylor as Piper Castleton
* Robert Klein as Arty Lesser
* H. Jon Benjamin as Eric
*Frank L. Ridley as Whale Watch Captain (voice)

==Production==
The films soundtrack is scored by Claudio Ragazzi with various renditions by Vinicius Cantuaria, Arto Lindsay, and Bebel Gilberto.

==Release== studio distributors resulted in Miramax Films paying $6 million for the films North American distribution rights.   However, the film grossed only $3.3 million during its theatrical release. 

==References==
  Boston magazine, July 1998. (archived 1999)

==External links==
*  
*  
*  
*  
* Bernieri, Laura,  , Robbins Entertainment Co. (archived 1999)

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 