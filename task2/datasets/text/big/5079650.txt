Bean (film)
 	
 
{{Infobox_Film |
|  name           = Bean
|  image          = Bean movie poster.jpg
|  caption        = Theatrical release poster
|  writer         = Richard Curtis Robin Driscoll  
|  starring       = Rowan Atkinson Peter MacNicol Burt Reynolds Pamela Reed Richard Gant 
|  director       = Mel Smith 
|  cinematography = Francis Kenny
|  editing        = Christopher Blunden
|  producer       = Rowan Atkinson Peter Bennet-Jones Tim Bevan Richard Curtis Eric Fellner Rebecca OBrien
|  studio        = Working Title Films Tiger Aspect Films
|  distributor    = PolyGram Filmed Entertainment (UK) Gramercy Pictures (USA)
|  released       =  
|  music          = Howard Goodall 
|  budget         = $18 million
|  gross          = $251.2 million   
|  runtime        = 90 min.
|  language       = English
|  country        = United Kingdom
}}
Bean, also known as Bean: The Ultimate Disaster Movie or Mr. Bean: The Movie is a 1997 feature film based on the television series Mr. Bean. It stars  Rowan Atkinson in the title role and Peter MacNicol. It was directed by Mel Smith. The film used many of the ideas and skits from the original television series.

==Plot== Royal National Andrew Lawrence) and daughter Jennifer (Tricia Vessey), who subsequently leave for Allisons mothers house.

After some initial mishaps with the airport police and leaving the gallerys owner, Mr. Grierson (Harris Yulin) slightly doubtful about Beans intelligence after meeting him in person, David begins to question his decision, and his worst fears are realised when Bean accidentally ruins the painting shortly after it arrives. Fearing that he will lose his job and possibly face criminal charges for the damage, David becomes despondent and gets drunk, even though his family returns out of pity. Bean, however, comes up with a plan to save Davids career by sneaking into the gallery at night and replacing the damaged painting with a poster that he alters to make it resemble a genuine painting with egg whites and Allisons perfume. The plan works and the painting is a success, but Bean is unexpectedly called to give a speech about the painting with General Newton, countless journalists and reporters watching. Bean, however, manages to give a sentimental and deep monologue about the painting and wins the crowds praise and approval.

Just then, however, David is visited by Lieutenant Brutus (whom Bean had crossed more than once earlier on) and is initially perplexed with the police presence, assuming that the police might have discovered the issue with the priceless painting. Brutus however tells David that his daughter Jennifer has been in a motorcycle accident. They rush to the hospital, but Brutus and the police stop prematurely to deal with a mugging, in which Brutus is shot. David shares an uneasy reunion with Allison beside the unconscious Jennifers bed, while Bean is mistaken for a medical doctor and forced into a theatre to help remove the bullet from Brutuss chest, which he manages to do unorthodoxly. While still in the doctors garments, Bean comes across David, who asks him to help awaken Jennifer. Once alone with Jennifer, Bean begins fooling around the room and accidentally manages to wake Jennifer. Bean uneasily reveals himself to David and Allison and asks that he be allowed to stay with them another week, which they gladly accept. Bean goes on to spend quality time with David and his family until he leaves.

Eventually, David takes Bean to the airport for his flight home, where they part ways as friends. Once at home in London, Bean takes one last look at his room, which he has decorated with photographs with the Langleys and the original Whistlers Mother that was damaged and then stowed away, before going to bed.

==Cast==
*Rowan Atkinson as Mr. Bean 
*Peter MacNicol as David Langley
*John Mills as Chairman	
*Pamela Reed as Alison Langley
*Harris Yulin as George Grierson 
*Burt Reynolds as General Newton 	
*Larry Drake as Elmer 
*Sandra Oh as Bernice Schimmel   	
*Danny Goldring as Security Buck		
*Johnny Galecki as Stingo Wheelie  Chris Ellis as Detective Butler		 Andrew Lawrence as Kevin Langley 	
*Peter Egan	as Lord Walton 	
*Peter Capaldi as Gareth	
*June Brown as Delilah	
*Peter James as Doctor Rosenblum 	
*Tricia Vessey as Jennifer Langley 
*Richard Gant as Lieutenant Brutus

==Reception==
The film was criticised for breaking with the programs tradition of having Mr. Bean as the centre of attention and for the alleged Americanisation required to sell it overseas (Bean also speaks intelligibly, albeit with apparent difficulty, as opposed to his frequent mumbling in the TV show).  However, the movie grossed over United States dollar|USD$250 million globally on a budget estimated at $18 million.  It was followed by a sequel in 2007 called Mr. Beans Holiday.  

The film holds a 42% approval rating on Rotten Tomatoes with an average score of 5.3/10 based on 31 reviews.  The film holds a score of 52 on Metacritic based on 20 reviews. 

==Soundtrack==
The films original score was by Howard Goodall, who had also written the music for the television series, although the original Mr. Bean theme was not used. Other non-original songs were also featured, in particular The Beatles "Yesterday (Beatles song)|Yesterday" (sung by Wet Wet Wet). 
 Heavy metal icon Bruce Dickinson which features sound dubs of Mr. Bean making campaign promises. This had been used for Comic Relief in 1992.
 Picture of You".

===List of songs performed by various artists=== OMC  
# Picture Of You Performed by: Boyzone  
# I Get Around Performed by: The Beach Boys  
# Walking On Sunshine Performed by: Katrina And The Waves     
# Yesterday Performed by: Wet Wet Wet   Louise   
# That Kinda Guy Performed by: Thomas Jules-Stock   Gabrielle  
# Hes A Rebel Performed by: Alishas Attic  
# Stuck In The Middle With You Performed by: Susanna Hoffs   
# Art For Arts Sake Performed by: 10cc  
# Have Fun Go Mad Performed by: Blair   Code Red    
# Bean Theme (Mad Pianos) Performed by: Howard Goodall  
# Elected Performed by: Mr. Bean and The Smear Campaign featuring Bruce Dickinson

==References==
 

==External links==
 
* 
* 
* 
* 
* 
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 