4Chosen: The Documentary
{{Infobox film
| name           = 4Chosen: The Documentary
| image          = 
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Jon Doscher
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = Montel Williams
| starring       = {{Plainlist|
* Johnnie L. Cochran Jr.
* Wynton Marsalis
* Peter Nufeld
* Barry Scheck
* Al Sharpton
}}
| music          = {{Plainlist|
* Jonathan Hanser
* Cindy Valentine Leone
* Danny Reyes
}}
| cinematography = Craig Needelman
| editing        = 
| studio         = Starline Films
| distributor    = Starline Pictures
| released       =   
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
4CHOSEN: The Documentary is a 2008 documentary film narrated by Montel Williams. It was written and directed by Jon Doscher, and produced by Doscher and Fran Ganguzza.

The film is about the aftermath of an incident that occurred April 23, 1998 on the New Jersey Turnpike. Four young men from New York were traveling on the turnpike on their way to a basketball talent showcase in North Carolina when they were pulled over by two state troopers and shot at thirteen times.  The film documents the court proceedings  and the investigation of the New Jersey State Police (NJSP),  and interviews indicate government corruption during former Governor Christine Todd Whitmans administration.  The four basketball players were represented by attorney David Ironman, and supported by Reverend Al Sharpton and Johnnie Cochran in a case that became known as "one of the largest racial profiling cases in American history".  
 Asbury Park. 

 
|-
| 2008    
| 4 Chosen: The Documentary  The Legends Behind The Comic Books
| Garden State Film Festival, Documentary - Short (TIE)
|  
|}

== References ==
 

== External links ==
* 
* 

 
 
 
 
 
 
 
 
 
 