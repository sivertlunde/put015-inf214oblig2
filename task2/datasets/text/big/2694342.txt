The Third Wheel (film)
{{Infobox film
| name           = The Third Wheel
| image          = The Third Wheel film.jpg
| caption        = The Third Wheel DVD cover
| director       = Jordan Brady Chris Moore Adam Rosenfelt Chad Snopek
| writer         = Jay Lacopo
| starring       = Luke Wilson Denise Richards Jay Lacopo Ben Affleck Phil Lewis Deborah Theaker Lisa Coleman Wendy Melvoin Jonathan Brown
| editing        = Sam Citron Paul Seydor
| studio         =
| distributor    = Miramax Films
| released       =  
| runtime        = 83 minutes
| country        = United States English
| budget         =
}}
 2002 American film directed by Jordan Brady, starring Luke Wilson, Denise Richards, Jay Lacopo and Ben Affleck. The plot of the movie revolves around a first date between the clumsy, shy Stanley (Luke Wilson) and the gorgeous Diana (Denise Richards), which is interrupted by a homeless man Phil (Jay Lacopo).

== Plot summary ==

After more than a year fantasizing about asking out his co-worker, Stanley (Luke Wilson) finally asks Diana Evans (Denise Richards) out on a date. As the two venture out on their first date, Stanley hits a homeless man Phil (Jay Lacopo) with his car, and the events that follow are far from the night of romance that Stanley had planned. Meanwhile, Stans co-workers, including his friend, Michael (Ben Affleck), are monitoring the date between Stan and Diana, and they are betting on how far Stan will get with the beautiful Diana.

==Cast==
* Luke Wilson as Stanley
* Denise Richards as Diana Evans
* Jay Lacopo as Phil
* Ben Affleck as Michael
* Tim DeKay as Speaker
* David Koechner as Carl
* Phill Lewis as Mitch
* Nicole Sullivan as Sally (uncredited)
* Matt Damon as Kevin (uncredited)
* Lauren Graham as Woman at Party (uncredited)
* Jeff Garlin as Office Worker (uncredited)

== External links ==
*  

 

 
 
 

 