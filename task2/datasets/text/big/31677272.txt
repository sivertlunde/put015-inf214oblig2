Le Miracle des loups (1924 film)
 
{{Infobox film
| name           = Le Miracle des loups
| image          = 
| caption        = 
| director       = Raymond Bernard
| producer       = Société des Films Historiques
| writer         = André-Paul Antoine Raymond Bernard
| starring       = Jean-Emile Vanni-Marcoux Charles Dullin
| music          = Henri Rabaud
| cinematography = 
| editing        = 
| distributor    = 
| released       = 28 November 1924 (France)  23 February 1925 (USA)  27 July 1930 (USA)  28 November 1951 (France)
| runtime        = 132 minutes; 73 minutes (sound re-edition)
| country        = France
| awards         = 
| language       = French
| budget         = 
}}
 French Historical historical Drama drama film from 1924, directed by Raymond Bernard, written by André-Paul Antoine, starring Jean-Emile Vanni-Marcoux. The scenario was based on a novel of Henry Dupuis-Mazuel "Le miracle des loups", published in 1924.    

Numerous scenes were filmed at the Cité de Carcassonne with thousands of participants.  It was also filmed in Château de Pierrefonds, Col de Porte, Isère, Le Sappey-en-Chartreuse, and in studio.

Raymond Bernard was a director, screenwriter, and actor.    It was a debut for Marie Glory (uncredited). 

== Plot == Charles of Burgundy.    The kings forces are attacked by the Burgundian troops and Beauvais must be defended until reinforcements can arrive. The defence of the city is led by Jeanne Hachette. There are realistic scenes in which wolves attack some of the opponents of Louis XI. 

== Production and Distribution ==
The film was produced by the Société des Films Historiques (Henri Dupuy-Mazuel) and distributed by the Société des Etablissements L. Gaumont.  The première was at the Paris Opéra and was attended by the President of France, the Premier and many other distinguished persons. 

In the USA it was distributed under the title The Miracle of the Wolves  and in Germany under the title Das Wunder der Wölfe.   In Spain it was known as El milagro de los lobos,  and in Italy as Il Miracolo dei lupi. 

In 1930 a sound version was produced. 
 title (directed by André Hunebelle). 

== Reception ==
The film was recognised as a French equivalent of the American The Birth of a Nation (1915). 

The film turned its eye on a moment of French glory, the late 15th century, when a sense of national unity had yet to be forged.   In France it was the most popular film of 1924.   ISBN 0-19-811257-2  It was highly praised by the intelligentsia in France, for its realism, but it was derisively treated by American critics.  Later it was claimed as a spectacular production.  It has a reputation as one of the highpoints of French silent cinema. It belongs to the best French tradition. 

== Cast ==
*  
*  
*  
* Romuald Joubé: Le chevalier Robert Cottereau
* Armand Bernard: Bische
* Ernest Maupain: Fouquet
*  
* Gaston Modot: Le comte du Lau, sire de Châteauneuf
* Philippe Hériat: Tristan lErmite
* Raymond Bernard: Le joueur déchec
* Albert Préjean: Un soldat
* Robert Guilbert: Le duc de Bourbon
*  
* Émilien Richard: Commines
* Halma: Olivier le Daim Maud Richard: (uncredited) 

== References ==
 

== External links ==
*   at the Films de France  
*   at Filmkultur  
*   at centrimage.com  

 

 
 
 
 
 
 
 
 
 
 
 
 