Fortress of War
{{multiple issues|
 
 
}}
{{Infobox film
| name = Fortress of War
| image = Fortress of War 2010.jpg
| director = Alexander Kott
| writer = Igor Ugolnikov Konstantin Vorobyov
| starring = Andrey Merzlikin Pavel Derevyanko Alexander Korshunov Alexey Kopashov
| released =  
| music = Yuri Krasavin
| cinematography = Vladimir Bashta
| editing = Mariya Sergeenkova
| studio = Belarusfilm
| runtime = 138 minutes
| country = Belarus Russia
| language = Russian
| gross = $4,569,371
}}
Fortress of War ( ; Transliteration|translit.&nbsp;Brestskaia krepost; festival title: The Brest Fortress) is a 2010 Russian war film.

It recounts the events surrounding the June 1941 Defense of Brest Fortress against invading Wehrmacht forces in the opening stages of Operation Barbarossa, Nazi Germanys invasion of the Soviet Union during World War II. The movie is accompanied by in medias res narration from the perspective of (then) 15-year old Sasha Akimov, and mainly centers on three resistance zones holding out against the protracted German siege, headed by regiment commander Pyotr Gavrilov, the political commissar Yefim Fomin, and the head of the 9th Frontier Outpost, Andrey Mitrofanovich Kizhevatov.

The plot follows the events as closely to historical fact as possible, and the Brest Fortress Museum supervised the plot thoroughly.

==Plot==
 German commandoes.

The next morning, at 3:58, German forces  . Fomin takes command of the defenders around the Holmsky gate, while Gavirilov rallies the defenders around the Eastern Fort. Elsewhere, NKVD border guards under command of a Soviet Lieutenant, Kizhevatov, repel a German sortie into the fortress, and Vanshtein thwarts a German commandos attempt to undermine the defense of the 132nd NKVD battalions barracks. As the siege commences, Sasha finds himself stranded in one of the barracks. During the fighting for the East Fort, Alexander Akimov is killed while destroying two Panzer IIIs with an anti-tank gun, helping Gavrilov repel a German attack.

By the end of June 22, the Soviet defenders are divided into groups: One force under Fomin defending the Holmsky gates, a second force under Gavrilov defending the Eastern Redoubt, while Kizhevatov defends the 9th Frontier outpost, along with a group of civilians, and Vanshtein holds on to the barracks of the 132nd NKVD battalion. The next day, fighting continues for the fortress, and Sasha makes it to the Holmsky gates. An Polikarpov I-16|I-16 Soviet fighter aircraft is shot down over the fortress, and the pilot is rescued by Fomins men. After revealing that the Red Army is retreating toward Minsk, Fomin realizes that the men must leave the fortress or die.

On June 24, Sasha leaves the Holmsky gates to alert the other pockets about Fomins plan for a breakout. While Sasha finds the 132nds position overrun, and Vanshtein himself dead, he manages to deliver the message to Kizhevatov and Gavrilov. That night, a breakout is attempted by all three remaining groups, but is driven back after suffering heavy losses by the Germans. The next morning, realizing he cant properly defend them, Kizhevatov reluctantly orders the surviving civilians (including his own wife and daughter, and also Sasha) to vacate the fortress during a cease-fire.
 immediately executed by the Germans. Gavrilov orders his remaining men to attempt to break out individually. Kizhevatov and his surviving men manage to regroup in the barracks; Sasha returns to meet them there. After ordering Sasha to take the regimental colors and remember the truth about the defenders, Kizhevatov takes a machine gun to cover his men while they attempt a breakout. The breakout fails, and the remaining defenders, including Kizhevatov are killed. Sasha, however, manages to escape.

It is later revealed that Gavrilov was captured on July 23, 1941, but survived captivity. Kizhevatov and Gavrilov were later declared Heroes of the Soviet Union, while Fomin is also decorated with the Order of Lenin.

==Cast==
* Alexey Kopashov as Sashka Akimov
* Andrey Merzlikin as Kizhevatov
* Pavel Derevyanko as Yefim Fomin
* Alexander Korshunov as Pyotr Gavrilov

==Production==

The films score was composed by Yuri Krasavin.

== External links ==
*  
*  
*  
*  

 
 
 
 