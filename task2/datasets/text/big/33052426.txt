What Happened to Jean
 

 
{{Infobox film
| name           = What Happened to Jean
| image          = 
| image_size     =
| caption        = 
| director       = Herbert Walsh
| producer       = 
| writer         = Keith Yelland
| based on       = 
| narrator       =
| starring       = Edith Crowe
| music          =
| cinematography = Harry Krischock
| editing        = 
| studio = Trench Comforts Fund Committee
| distributor    = 
| released       = 7 November 1918 
| runtime        = 5,000 feet
| country        = Australia
| language       = Silent film  English intertitles
| budget         = 
| gross          = £2,000 
| preceded_by    = 
| followed_by    = 
}}

What Happened to Jean is a 1918 Australian silent film shot in South Australia. It is a lost film. Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, 84. 

==Plot==
Country girl Jean sets out to see the world. She arrives in Adelaide, runs into villainous Ashbourne, and wins a car in a competition conducted by the Trench Comfort Fund. She meets socialite Mrs de Tafford, who misses her long-lost daughter, and adopts Jean, and promotes her in society. Jean attends a garden party and government house and is send to a boarding school to complete her education. She discovers that she is in fact Mrs de Taffords long-lost daughter.  

==Cast==
 
*Edith Crowe as Jean
*Mrs Ernest Good as Mrs de Tafford
*Price Weir as Colonel de Tafford
*Herbert Walsh as Reg Stanton
*James Anderson as Dad Smith
*Ethelwyn Robin as Mum Smith
*Janet Ward as Stella
*Rita Crowe as maid
*Victor Fitzherbert as Ashbourne
*Roth Martin as George
*Hartley Williams as Jasper
*Harold Rivaz as Jabez
*Darcy Kelway as Bertie
 Premier Peake.  

==Production==
The film was made by the South Australian Trench Comforts Fund to raise money for charity. It was intended for South Australian audiences only and deliberately featured many local landmarks. Most of the cast and crew were amateurs. 

==Reception==
The film was hyped through a series of ads in Adelaide papers simply asking "what happened to Jean?"  It received a gala premiere, attended by the Premier, Governor General, and leading members of Adelaide society. 

The film was well received in Adelaide and raised a reported £2,000. 

==References==
 

==External links==
* 

 
 
 
 
 


 