Superman: Brainiac Attacks
 
{{Infobox film| name = Superman: Brainiac Attacks
| image          = Brainiac_attacks.jpg
| caption        = Promotional art
| director       = Curt Geda
| producer       = {{Plainlist|
* Duane Capizzi
* Margaret M. Dean
* Curt Geda
* Kyle Jolly
* Sander Schwartz }}
| writer         = {{Plainlist|
* Duane Capizzi
* Christopher Simmons }}
| based on       =   Joe Shuster}}
| starring       = {{Plainlist|
* Tim Daly
* Powers Boothe
* Dana Delany
* Lance Henriksen }}
| music          = Thomas Chase Jones
| cinematography = Dongwoo
| editing        = Margaret Hou
| studio         = {{Plainlist|
* DC Comics
* Warner Bros. Animation }}
| distributor    = Warner Home Video
| released       =  
| runtime        = 75 minutes
| country        =  
| language       = English
| budget         =
}}
Superman: Brainiac Attacks is a 2006   from Warner Bros. Animation, on aired a part of the Toonami block on Saturday June 17, 2006, and the released on DVD Tuesday June 20, 2006. It also aired on Toon Disney on June 16, 2008, being the first Superman film to air on Toon Disney. The film features Superman battling the forces of Lex Luthor and Brainiac (comics)|Brainiac. It also focuses on the relationship between Superman and Lois Lane and stands as the first Superman solo animated film.

Though the films visual style is the same as   (as are the majority of its returning voice cast), it is not in continuity with the   and  , as he was not part of the cast of the  s less serious portrayals of the character in live-action movies, making Luthor more light-hearted and darkly whimisical, going as far as to make jokes about the situations around him.

==Plot==
Lois Lane and Jimmy Olsen are sent to one of Lex Luthors laboratories after Brainiac arrives on Earth on a meteor, successfully dodging the attempts made by Luthors satellite to destroy any potential damage to the Earth (in an attempt to boost his popularity against Superman as the true hero of the people). Superman shortly arrives and finds Brainiac downloading data from the computers with information relating the various forms of weaponry from LexCorp, including the laser-equipped meteor shield that had attempted to destroy Brainiac earlier. Using his ice-breath, Superman is able to seemingly destroy Brainiac, after Superman and Brainiac had engaged in battle.

Witnessing the incident, and how his satellite could be used as an effective weapon against Superman, Luthor gathers a piece of Brainiacs remains and takes it to LexCorp where Brainiac is revived. There, Luthor proposes that Brainiac, with the technology of LexCorp as well as Kryptonite, defeat Superman, and then Luthor step in to chase Brainiac away from the Earth, in front of the world to make him appear as Earths true hero, where he will then be free to conquer other planets, leaving Lex in charge of Earth. Brainiac accepts the agreement, and proceeds to rebuilding and improving himself.

Meanwhile, Clark Kent contemplates the idea of revealing his secret identity to Lois. The opportunity presents itself when editor, Perry White, due to staff shortages, send both Kent and Lane to review a restaurant in Metropolis. However, it is during this time that Brainiac returns. Among his improvements is the ability to track down Superman based on his DNA. After another fight between Superman and Brainiac, Superman has been significantly injured, and infected, by Brainiacs power rays and Lois is injured in the process where it is revealed that her blood has been infected with a metallic-based poison, that if not treated would prove fatal.

Feeling guilty, Superman obtains a sample of Lois blood from the hospital and returns to the Fortress of Solitude where he analyzes Lois blood using his Kryptonian technology. It is then when Superman discovers that the only cure for Lois condition is to obtain a chemical substance from the Phantom Zone. However, Brainiac is able to locate Superman in his Antarctic retreat, and attempts to downloaded the information of Krypton from Supermans computer. Superman then initiates a self-destruct sequence. Brainiac, not being able to locate Superman, presumes that he has been killed in the explosion. Superman had in fact gone into the Phantom Zone in order to find the chemical that would not only cure Lois and heal himself, but provide him with increased strength against Brainiac.

Brainiac returns to Metropolis where Luthor awaits in order to fulfill their agreement. Brainiac however, intends to kill Luthor in order to conquer Earth, and had even removed the self-destruct component that Luthor had planted should Brainiac double cross him. Returning to Metropolis, Superman and Brainiac fight. Superman seemingly defeats Brainiac and then returns to the hospital in order to cure the ailing Lois. However, Brainiac is able to destroy the chemical that would cure Lois, before Superman finally destroys Brainiac.

With the cure now destroyed, Lois faces certain death due to her illness. Superman, regretting never telling Lois his true feelings then embraces her. It is then that his tears, containing the chemical that had healed him earlier, makes contact with Lois, curing her. Later back in the arctic, Superman recovers a piece of his destroyed Kryptonian technology where he aims to rebuild his fortress. He then vows to quit his job at the Daily Planet in an attempt to prevent future harm to his loved ones, should any of his enemies discover his secret identity.

The film ends with Luthor facing criminal prosecution after the discovery of LexCorps involvement with Brainiacs attack, and Lois Lane racing to cover the appearance of Mr. Mxyzptlk in Metropolis. Seeing Lois eagerness to put herself in harms way in order to cover a story, Superman goes back on his earlier decision to quit the Daily Planet so that he can be with Lois, as well as Metropolis protector against the most powerful threats from the universe.

==Cast==
* Tim Daly as Superman|Kal-El/Clark Kent/Superman
* Powers Boothe as Lex Luthor
* Dana Delany as Lois Lane Brainiac
* George Dzundza as Perry White Martha Kent
* Mike Farrell as Jonathan Kent David Kaufman as Jimmy Olsen
* Tara Strong as Mercy Graves

==See also==
*List of animated feature-length films

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 