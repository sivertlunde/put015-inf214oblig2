Sadko (film)
{{Infobox film name = Sadko image =Sadko_poster.jpg caption = Poster for Sadko writer = Konstantin Isayev Nikolai Rimsky-Korsakov (libretto from the opera Sadko) starring = Sergei Stolyarov Alla Larionova Yelena Myshkova director = Aleksandr Ptushko producer =  music = Nikolai Rimsky-Korsakov (music from the opera Sadko) Vissarion Shebalin (arrangements and additional music) cinematography =  Fyodor Provorov distributor = Filmgroup (US version) released = 1953 (original) 1962 (US version) runtime =  country = Soviet Union language = Russian
}}

Sadko ( ) is a 1952 Russian fantasy film directed by Aleksandr Ptushko. The film is based on an opera by Nikolai Rimsky-Korsakov, which was based on a Russian bylina (epic tale) with the same name, and scored with Rimsky-Korsakovs music from the opera.

==Plot summary==

This tale is based upon the legends born in ancient times in the old Russian city of Novgorod (the capital of Novgorod republic). Novgorods merchants are feasting in a gorgeous palace. A young gusli player named Sadko is bragging that he can bring to their land a sweet-voiced bird of happiness. But the merchants are ridiculing him. Nevertheless, Sadko sets off on a travel to bring the bird of happiness to Novgorod. He is offered help by the daughter of the Ocean King - she is mesmerized by Sadkos singing and is in love with him. The hero is destined to visit many lands in his search of the bird. He will come to India, Egypt and other countries. But only on his return to his native Novgorod, would Sadko realize that there is no better land than the one you were born in. And so there is no need to go far in search of ones happiness.

==Awards==

Sadko won the "Silver Lion" award at the Venice Film Festival in 1953, and festival judges included lead actor Sergei Stolyarov in a list of the world’s best actors in the 50-year history of film.

==The Magic Voyage of Sinbad==

The film was released in the United States in 1962 in an English-dubbed and modified form by  .
 Season 5, Kevin Murphy, voice of Tom Servo, has professed a love for the "breathtaking" visual style of this and other films by Aleksandr Ptushko in multiple interviews.   Paul Chaplin, another writer of the show, has also expressed admiration.

==DVD release==

The original Russian version of Sadko is available on DVD from RusCiCo.

The Mystery Science Theater 3000 treatment of the film was released on DVD as part of the 20th four-film box set of series episodes, through Shout! Factory on March 8, 2011. In some sets the DVD is mis-labeled as the film Project Moonbase.

==See also==

* Sadko, the Russian bylina (epic tale) upon which the film and opera are based
* Sadko (musical tableau), a symphonic poem by Nikolai Rimsky-Korsakov.
* Sadko (opera), an opera by Rimsky-Korsakov.

==References==
 

==External links==
*  
* 

 
 
 
 
 
 
 
 
 
 