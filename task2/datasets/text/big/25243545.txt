Maroc 7
{{Infobox film
 | name =Maroc 7
 | image =maroc7poster.jpg
 | director = Gerry OHara
 | writer =David D. Osborn 
 | starring = Gene Barry Elsa Martinelli
 | music =Kenneth V. Jones 
 | cinematography =Kenneth Talbot 	
 | editing = 
 | released = 1969
 | language = English British thriller jewel thief hatches a plan to go to Morocco and steal a valuable artifact. 

==Plot==
Louise Henderson is the editor of a respected fashion magazine, but she has a hidden career as mastermind of a ring of thieves. With their professional operation as a front, Louise uses one of her models, Claudia, and a photographer, Raymond Lowe, to steal precious artifacts and jewels.

Law enforcement agencies have their suspicious about her, so secret agent Simon Grant is assigned the case. He pretends to be a safecracker to infiltrate Louises gang, traveling to Morocco, where she intends to switch an imitation Arabian medallion for a priceless real one.

Grant is given cooperation in Morocco by a chief of police, Barrada, and a woman named Michelle Craig who is the chiefs top aide. Things go wrong when Grant needs to kill Lowe, who has discovered his true identity.

The theft goes on as planned, at least until Claudia is shot. To the surprise of cops and robbers alike, the precious medallion is stolen by the one person none of them suspected, Michelle, who escapes.

==Cast==
* Gene Barry - Simon Grant
* Elsa Martinelli - Claudia
* Leslie Phillips - Raymond Lowe
* Cyd Charisse - Louise Henderson
* Denholm Elliott - Inspector Barrada
* Alexandra Stewart - Michelle Craig
* Angela Douglas - Freddie
* Eric Barker - Professor Bannen Tracy Reed - Vivienne Maggie London - Suzie
* Ann Norman - Alexa
* Penny Riley - Penny Raymond Austin - Stunt Coordinator

==References==
 
 

 
 
 
 
 
 


 