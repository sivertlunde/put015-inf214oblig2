Beat It (film)
 
{{Infobox film
| name           = Beat It
| image          =
| image size     =
| caption        =
| director       = Gilbert Pratt
| producer       = Hal Roach
| writer         = H. M. Walker
| narrator       =
| starring       = Harold Lloyd
| music          =
| cinematography = Walter Lundin
| editing        =
| distributor    =
| released       =  
| runtime        =
| country        = United States Silent English intertitles
| budget         =
}}
 short comedy film featuring Harold Lloyd.   

==Cast==
* Harold Lloyd
* Snub Pollard
* Bebe Daniels
* William Blaisdell
* Sammy Brooks
* Lige Conley - (as Lige Cromley)
* William Gillespie
* Maynard Laswell - (as M.A. Laswell)
* Gus Leonard
* Belle Mitchell
* William Strohbach - (as William Strawback)
* Dorothea Wolbert
* King Zany - (as Charles Dill)

==See also==
* Harold Lloyd filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 