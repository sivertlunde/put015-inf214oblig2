Rewi's Last Stand
{{Infobox film
| name           = Rewis Last Stand
| image          =
| caption        =
| director       = Rudall Hayward
| producer       =
| writer         = Rudall Hayward
| based on       =
| starring       =
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       =  
| runtime        =
| country        = New Zealand English
| budget         =
| gross          =
}}
Rewis Last Stand is a 1925 feature film directed by pioneering New Zealand filmmaker Rudall Hayward. Both the 1925 silent movie and Haywards 1940 remake are historical dramas, based on the last stand of Rewi Maniapoto at the Battle of Orakau. 

Hayward believed that New Zealands history offered material as dramatic as any Hollywood western. He set out to make films involving conflicts between Maori and pakeha "while there were still people alive" who remembered the period accurately. 

==1940 Remake==
{{Infobox film
| name           = Rewis Last Stand
| image          =
| caption        =
| director       = Rudall Hayward
| producer       =
| writer         = Rudall Hayward
| based on       =
| starring       =
| cinematography =
| editing        =
| studio         =
| distributor    = 
| released       =  
| runtime        =
| country        = New Zealand English
| budget         =
| gross          =
}}

In 1940 Rudall Hayward remade his second feature on a more ambitious scale, this time with sound. He cast his future wife Ramai Hayward in the romantic lead.

The 1940 remake was released in a shortened version in the United Kingdom, as The Last Stand. The shortened version is the only one surviving.    accessed 22 November 2012 

==References==
 

==External links==
*  at IMDB
*  at IMDB
*  at NZ History
*  at New Zealand Feature Film Database
 

 
 
 
 
 
 
 
 
 
 
 
 