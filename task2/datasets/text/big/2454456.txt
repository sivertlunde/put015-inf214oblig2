Fata Morgana (1971 film)
{{Infobox film
 | name = Fata Morgana |
 | image = Fatamorgana.jpg
  | caption = |
  | director = Werner Herzog |
  | producer = Werner Herzog
  | writer = Werner Herzog |
  | music = Blind Faith   Third Ear Band   Leonard Cohen 
  | cinematography = Jörg Schmidt-Reitwein 
  | editing = Beate Mainka-Jellinghaus 
  | narrator = Lotte Eisner 
  | studio = Werner Herzog Filmproduktion  
  | distributor = 
  | released = April 19, 1971    |
  | runtime = 79 min |
  | country = West Germany 
  | language = German 
  | budget = 
  }}
 mirages in the Sahara desert. Some narration recites Mayan creation myth (the Popol Vuh) by Lotte Eisner, text written by Herzog himself.

==Production==
The film was initially intended to be presented with a science fiction narrative, casting the images as landscapes from another planet. This concept was abandoned as soon as filming began, but was realized in Herzogs later films Lessons of Darkness and The Wild Blue Yonder.
 {{cite book
  | last = Herzog
  | first = Werner
  | title = Herzog on Herzog
  | publisher = Faber and Faber
  |year=2001
  | isbn = 0-571-20708-1
  | page = 303 }} 
 VW bus with Herzog driving. The crew smoothed out the road themselves to prepare the shots.  
 in absentia. 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 


 