Saving Grace (2000 film)
 
 
{{Infobox film
| name           = Saving Grace
| image          = Saving Grace.jpg
| caption        = Theatrical release poster
| alt            = A man and a women sitting by a cliff view. 
| director       = Nigel Cole
| producer       = Mark Crowdy  Xavier Marchand  Cat Villiers
| writer         = Mark Crowdy  Craig Ferguson
| starring       = Brenda Blethyn  Craig Ferguson Mark Russell
| cinematography = John de Borman
| editing        = Alan Strachan
| distributor    = 20th Century Fox (UK)  Fine Line Features (USA)
| released       =  
| runtime        = 93 minutes
| country        = United Kingdom
| language       = English
| budget         = £4 million 
| gross          = £29,360,046   
}} British comedy film, directed by Nigel Cole and based on a screenplay by Mark Crowdy and Craig Ferguson. It was co-produced by Fine Line Features, Homerun Productions, Portman Entertainment, Sky Pictures, and Wave Pictures and filmed in London and the villages of Boscastle and Port Isaac in Cornwall, England,  starring Brenda Blethyn, Craig Ferguson, and Martin Clunes, among others. Distributed by 20th Century Fox in major territories, the film premiered at the 2000 Sundance Film Festival, where it won Cole the Audience Award for World Cinema. 
 independent British Munich Film Festival, also receiving a BAFTA Award nomination for Crowdy,  and ALFS Award, Golden Globe and Satellite Award nominations for Blethyn and her performance.   

==Plot==
After being unexpectedly widowed, respectable and reserved housewife Grace Trevethyn (Brenda Blethyn) discovers that her late husband ran up significant debts and left her facing foreclosure and numerous repossessions. Well liked around Port Isaac, the coastal fishing town she lives in, Grace is given emotional support by the towns inhabitants, particularly her loyal, cannabis (drug)|pot-smoking gardener Matthew Stewart (Craig Ferguson), his girlfriend and the towns fisherwoman Nicky (Valerie Edmond), and their friend Dr Martin Bamford (Martin Clunes). Despite wanting to help Grace, none of the residents are able to find a way to get her out of her dire situation.
 producing small buds, much to Matthews amazement.
 property appraiser arrives at her home, informing her that it is forcibly being put up for auction. She later learns that her husbands debts were worse than she thought, totaling more than £300,000, most owed to Ramptons, a London investment firm. The next day Grace asks Matthew the approximate value of the marijuana plant she rescued; when he informs her that, "ounce-for-ounce the really good stuffs worth more than gold," she proposes a partnership.  Matthew will help her grow and sell marijuana, and after accumulating enough cash to pay off her debts the rest will be split evenly. Matthew accepts, and that evening returns to Nicky, eager to share the good news.  Nicky is less than pleased, convinced the plan is reckless and fearing that Matthew will be caught and arrested. Now unsure of her relationship with him, Nicky holds off on telling Matthew that she has just discovered she is pregnant.
 hydroponic grow smoking pot for the first time, and thoroughly enjoys the experience. While the two are high, Matthew asks her what made her decide to grow marijuana; Grace cryptically replies with Honeys London address.

As the time to harvest the marijuana approaches Grace has a chance encounter with Nicky, whose evasion of her makes her curious. The two finally talk, and Nicky asks Grace to ensure that her boyfriend wont get arrested. Moved by Nickys plea, Grace tells Matthew that he wont be going to London to find a buyer, and subsequently sneaks off there herself. Graces efforts to locate a dealer prove unsuccessful, and she ultimately has to be rescued from a police station by Honey after venturing into a seedy pub. At Honeys home, Grace manages to convince the other woman to call her dealer, Vince (Bill Bailey), to try and sell the marijuana. Vince is impressed by the quality, but lacks the money necessary to buy the full 20 kilograms Grace is offering. After some pleading, Vince agrees to take the two women to someone who has enough money.  Back in Port Isaac, Matthew discovers that Grace has gone to London from Dr Bamford, and follows her to Honeys house accompanied by Bamford, remembering the London address Grace told him earlier. The two arrive in time to see Vince, Honey, and Grace leave, and follow.

Vince takes Grace and Honey to a yet-to-be-opened aquatic-themed rave club where they meet with French businessman Jacques Chevalier (Tchéky Karyo). Grace impresses Chevalier by instructing him on improving his casting with a fishing rod in French, and is progressing towards a deal when Matthew and Dr Bamford arrive, nearly causing an incident when Chevaliers bodyguard China MacFarlane (Jamie Foreman) mistakes their appearance for an ambush. The situation is quickly calmed, and in a private office Chevalier and Grace negotiate a deal for the marijuana, Grace almost crying with relief when the businessman accepts. Grace, Vince, and Dr Bamford head back to Port Isaac; Chevalier secretly instructs China to take Vince and follow them to Graces home.

Arriving back in Port Isaac to harvest the marijuana, Grace discovers that the Womens Institutes|Womens Institute is preparing to have a luncheon at her house, and that Ramptons adjuster Quentin Rhodes (Clive Merrison) is heading to the house as well to discuss the foreclosure. Grace and Matthews friend Harvey (Tristan Sturrock) stay to harvest the marijuana while Matthew returns to town to reconcile with Nicky. At the dock, Matthew apologizes and proclaims his love to Nicky, who responds by telling Matthew that she is pregnant. The two joyously celebrate momentarily, before being informed by local barkeep Charlie (Paul Brooke) that two men have been asking about Grace; Matthew quickly identifies them as Vince and China, and returns to warn Grace while Nicky tells local Police Sergeant Alfred Mabely (Ken Campbell) that the two are actually salmon poachers, prompting him to keep an eye on the two.

At Graces house, Grace, Matthew, Dr Bamford and Nicky all frantically attempt to dismantle the grow operation while Harvey keeps Quentin distracted in the house.  Vince and China arrive at the home; when Harvey leaves to warn Grace, Vince and China slip in and confront Quentin, believing he is another buyer of the marijuana. Sergeant Mabely arrives and discovers Graces grow operation, but rather than arresting her explains that he was well aware of her activities and decided to turn a blind eye. However, he warns her that hes called in other police officers to follow his supposed salmon poachers and leaves. Fearing their imminent discovery, Grace decides to burn the dried marijuana rather than facing possible jail time, but as she holds a lit match over the pile China arrives, holding Quentin at knifepoint and demanding Grace put the match out. Before she can it burns her fingers, and she drops it into the pile.  At the same moment, a woman from the local WI arrives, surprising China and causing her to flee at the sight of a man wielding a switchblade. China chases her, only to run into the rest of the arriving women, who beat him into submission with their handbags. The police arrive shortly thereafter, attempting to break up the riotous mob. Grace and her friends flee in the confusion while the huge marijuana pile burns inside the greenhouse.

Inside her house, Grace is surprised to find Chevalier. She confronts him over his betrayal, but he responds that he sent China to follow her not to steal from her, but to protect her.  He goes on to express a budding romantic interest in her, which after some hesitation Grace accepts. Back outside, Dr Bamford is unable to resist the opportunity and opens the door to the greenhouse, releasing a huge cloud of marijuana smoke that envelops him and the rioting townspeople. The riot quickly turns into a pleasant, carefree celebration.

Several months later Port Isaacs residents watch a television special in the pub, detailing Graces "mysterious" transition from an "unknown widow" to an overnight millionaire after the success of her marijuana-themed novel Joint Venture.  The special goes on to detail Graces marriage to Chevalier, as well as the mystery surrounding the large riot at her house in which "nobody could remember anything."  After some interviews with other town residents, the special cuts to a live broadcast from the New York Book Awards, where Grace wins an award for her novel. In her acceptance speech, Grace thanks Matthew for helping her.

==Cast==
* Brenda Blethyn as Grace Trevethyn, a middle-aged newly widowed woman who is faced with the prospect of financial ruin and turns to growing marijuana under the tutelage of her gardener in order to save her family home. Blethyn, who was Fergusons first choice, signed on the movie two years before shooting.     
* Craig Ferguson as Matthew Stewart, Graces gardener. Ferguson created the playful character with himself in mind. "I saw him as a decent chap who happens to like a bit of marijuana," Ferguson said. "He really cares about Grace and he wants to save her."    ITV television series Doc Martin, which states in its ending credits that the character was derived from the film Saving Grace.
* Valerie Edmond as Nicky, Matthews frowning girlfriend. Edmond won the role of the villages fishing captain in a large open casting call. 
* Tcheky Karyo as Jacques Chevalier
* Jamie Foreman as China MacFarlane
* Bill Bailey as Vince
* Diana Quick as Honey Chambers
* Tristan Sturrock as Harvey
* Phyllida Law as Margaret Sutton
* Linda Kerr Scott as Diana Skinner
* Leslie Phillips as Rev. Gerald Percy
* Paul Brooke as Charlie Ken Campbell as Sgt. Alfred Mabely
* Clive Merrison as Quentin Rhodes

==Reception==

===Commercial success   ===

The film was released on 19 May 2000 in the United Kingdom and Ireland, where it grossed £3,000,000 during its theatrical run. Although it took only a tenth of simultaneously released Gladiator (2000 film)|Gladiators box office haul, Saving Grace was, considered a "good showing" in consideration of the films low budget.   

In the United States the film opened on 4 August 2000, where it soon emerged as a small box-office surprise during the slow-seasoned summer.  Having originally opened at 30 screens,  it was eventually showing on more than 870 screens (Fine Line had only planned to open it across 200 screens) during its most successful weeks in early September 2000, when Saving Grace averaged takings of $3,351 per theatre - more than hits like X-Men (film)|X-Men and Hollow Man.   It eventually grossed £12,178,600 internationally. 

===Critical response   ===

The critical response of the film was positive. Rotten Tomatoes gave the film a rating of 62% based on reviews from 89 critics.    Review aggregation website Metacritic gave it a 62/100 rating, based on reviews from 32 critics.   

Jonathan Crow from   called the film "this summers bait for the Anglophiles," meaning "that theyre English and elderly apparently makes their antics screamingly funny to people who would turn up their noses at similar humor in a film like Scary Movie."   

===Awards   ===

; Won  Munich Film Festival 2000) - Mark Crowdy
* Audience Award (Norwegian International Film Festival 2000) - Nigel Cole
* Audience Award (Sundance Film Festival 2000) - Nigel Cole

; Nominated
* ALFS Award: "British Actress of the Year" - Brenda Blethyn
* BAFTA Award: Carl Foreman Award for the Most Promising Newcomer - Mark Crowdy
* British Independent Film Award
** "Best British Independent Film"
** "Best Director" - Nigel Cole
** "Best Screenplay" - Craig Ferguson, Mark Crowdy
** "Best Actress" - Brenda Blethyn
* Empire Award: "Best British Actress" - Brenda Blethyn
* Golden Globe: "Best Performance by an Actress in a Motion Picture - Comedy/Musical" - Brenda Blethyn
* Golden Satellite Award: "Best Performance by an Actress in a Motion Picture, Comedy or Musical" - Brenda Blethyn



==Soundtrack==

; Music from the Motion Picture album 
{{Infobox album  
| Name        = Saving Grace
| Type        = Soundtrack
| Longtype    =
| Artist      = Various Artists
| Cover       = Saving Grace OST.jpg
| Released    = 15 May 2000
| Recorded    = Pop  Rock  Film music
| Length      = WEA
| Mark Russell
| Last album  =
| This album  =
| Next album  =
}}

  Mark Russell)  – 1:02 Mark Russell)  – 2:42
# "Take a Picture"  (Filter (band)|Filter)  – 5:55
# "Make Me Smile (Come Up and See Me)"  (Steve Harley & Cockney Rebel)  – 4:07
# "Spirit in the Sky"  (Norman Greenbaum)  – 3:56
# "Will You Give Me One?  (film dialogue)  – 1:12
# "Sunshine at Last"  (Koot)  – 4:31 Mark Russell)  – 2:57
# "Human (Tin Tin Out Mix)"  (The Pretenders)  – 3:52 Mark Russell)  – 2:46
# "Might as Well Go Home"  (Red Box (band)|Plenty)  – 3:17
# "Would You Like Some Cornflakes?  (film dialogue)  – 0:22
# "Wise Up (Car Port Mix)"  (AFT)  – 3:12
# "New B323"  (film dialogue)  – 0:41 Mark Russell)  – 3:01
# "Accidental Angel"  (Sherena Dugani)  – 3:57 Robert Palmer)  – 3:17 Mark Russell)  – 2:51

==Spin-offs==
Martin Clunes starred in two television film prequels to this film, made by BSkyB: Doc Martin and Doc Martin and the Legend of the Cloutie, in which viewers learn that Bamford, a successful obstetrician, finds that his wife has been carrying on extramarital affairs behind his back with his three best friends. After confronting her with the news, he decides to leave London and heads for Cornwall, which he remembers fondly from his youth. Shortly after he arrives, he gets involved in the mystery of the "Jellymaker" and, following the departure of the villages resident GP, decides to stay in Port Isaac and fill the gap himself.
 ITV who generally liked it, but felt the character of Martin Bamford needed a little something more to him than just being a "townie" who is a little out of his depth in the country. ITV wanted something a little more edgy, so Clunes came up with the idea of the doctor being unusually grumpy. Out of that idea a new series, also called Doc Martin, was born. The series became a huge success in the UK and internationally; the end titles mention that it is "derived from Saving Grace".

==References==
 

==External links==
 
 
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 