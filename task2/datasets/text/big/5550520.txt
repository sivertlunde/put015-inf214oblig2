Class of 1984
 
{{Infobox film
| name           = Class of 1984
| image          = Class of nineteen eighty four.jpg
| border         = 
| alt            =  
| caption        = Theatrical release poster
| director       = Mark L. Lester
| producer       = Merrie Lynn Ross Mark Lester Arthur Kent Tom Holland John Saxton
| screenplay     = 
| story          = 
| based on       =  
| starring       = Perry King Roddy McDowall Timothy Van Patten Michael J. Fox Lisa Langlois
| music          = Lalo Schifrin
| cinematography = Albert J. Dunk
| editing        = Howard Kunin
| studio         = 
| distributor    = United Film Distribution Company
| released       =     
| runtime        = 98 minutes
| country        = Canada
| language       = English
| budget         = $4,300,000 (estimate)
| gross          =
}}
Class of 1984 is a 1982 Canadian action-thriller movie about a newly hired music teacher at a troubled inner city school, where students have to pass through a metal detector due to problems with gangs, drugs, and violence. It was directed by Mark L. Lester and starred Perry King as teacher Andrew Norris, Merrie Lynn Ross (who also co-executive produced) as his wife Diane Norris, Roddy McDowall as Terry Corrigan and Timothy Van Patten as Peter Stegman, the leader of the gang of thugs who terrorize the school. It was one of Michael J. Foxs early roles, before he was a well-established actor. It was a major box-office success for its time making more than 20 million dollars in the US alone on a budget of four and half million, and was the number one film in many countries worldwide on release.
 punk look Teenage Head.

The film begins with a warning that it is partially based on true events.

== Cast ==
*Tim Van Patten as Peter Stegman, the leader of the gang and a feared figure in the school. He and his gang sell drugs, fight rival gangs, and run a small prostitution racket. He carries around a switchblade and is willing to do anything to save himself from trouble.

*Lisa Langlois as Patsy, Stegmans girlfriend and the gangs only female member, showing some lesbian attractions.

*Stefan Arngrim as Drugstore, the drug dealer of the gang and himself is hooked on drugs. He carries a straight razor and uses a baseball bat in a gang fights.
 Keith Knight as Barnyard, a gang Criminal|enforcer. He wears clothes displaying the reversed swastika. He uses a large wooden plank in battle.

*Neil Clifford as Fallon, the other enforcer. He wears a lot of black leather and a Mohawk Hairstyle. His weapon is a chain, which he wears as a belt for easy access in fights.

==Reception==
Upon its original release, Class of 1984 was banned in several countries due to its lewd content. FilmCritic.com article: " ."  It currently maintains a 60% fresh rating on Rotten Tomatoes. Rotten Tomatoes page for " ." 

== Release ==
The film premiered on 20 August 1982 in the United States. 
 	
The Scream Factory released the film with a new artwork first time on Blu-ray on March 21, 2015 in the USA.  On April 14, 2015, Shout! Factory released the Collector’s Edition Combo Pack on Blu-ray Disc, which features some Interviews with the director, the composer & leading actors. 

==Legacy== call in radio program The Best Show on WFMU. Recidivism page: " ."  Recidivism page: " ." 

===Sequels===
The movie also had two   (1990) and the   (1994), the first of which was also directed by Lester and like its predecessor, was also released via Vestron Video, while the latter was released by Vidmark Entertainment. Lionsgate released Class of 1999 on DVD September 16, 2008. They have been released on DVD in many foreign territories, such as Korea, Italy and Australia.

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 