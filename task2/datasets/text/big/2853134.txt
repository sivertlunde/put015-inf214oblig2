Pagal Nilavu
{{Infobox film name = Pagal Nilavu
| image = Pagalnilavu.jpg
| director = Mani Ratnam
| writer = Mani Ratnam A. L. Narayanan (dialogues) Murali  Revathi Sarath Babu Sathyaraj Raadhika
| producer = G. Saravanan
| music = Ilaiyaraaja
| cinematography = Ramachandra Babu
| editing = B. Lenin V. T. Vijayan
| studio = Sathya Jyothi Films
| distributor = Sathya Jyothi Films
| released = 5 June 1985
| runtime = 141 mins
| country = India Tamil
| gross = $50,000
}}
Pagal Nilavu ( ;   film directed by Mani Ratnam. It is about a carefree youth caught between his loyalty to a mafia don and his love for a cops sister. The films score and soundtrack were composed by Ilaiyaraaja. The cinematography of the film was handled by Ramachandra Babu.This film marked Mani Ratnams entry into Tamil cinema. Pagal Nilavu failed at the box office.

==Plot==
Pagal Nilavu is in essence the story of Selvam(Murali) an aimless youth in Muttam(Kanyakumari Dist) who out of a sense of deep gratitude ends up joining the gang of Devaraj aka Periyavar(Sathyaraj) and the complications which arise when he falls in love with Jyothi(Revathi), the sister of Robert Manohar(Sarat Babu) an honest and committed police inspector who is newly posted to the town. Periyavar pretty much runs the town and though he helps people in need, he is a diabolical kingpin who will not compromise on his hold of the town for anything. He wins Selvam’s undying loyalty when he helps him with money and power at a critical juncture to save his mother. In this backdrop Robert takes charge and moves into the town with his sister and daughter. Being uncompromising in his morals, Robert is automatically drawn into a game of oneupmanship with Periyavar and his gang. He also consequentially cannot stand Periyavar’s chief henchman Selvam, and is consequentially drawn into continuous verbal and physical duels with him. Selvam meanwhile falls hook line and sinker to Jyothi(Revathi) and tries every trick in the book to woo her and eventually wins.

==Cast==
*Murali (Tamil actor)|Murali as Selvam
*Revathi Menon|Revathi as Jyothi
*Sathyaraj as Devarajan
*Sarath Babu as Robert Manohar
*Raadhika
*Goundamani
*Nizhagal Ravi
*Poovilangu Mohan
*Venu Arvind

==Soundtrack==
The music composed by Ilaiyaraaja. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Gangai Amaran || 04:23
|-
| 2 || Nee Appothu || Malaysia Vasudevan, S. P. Sailaja || 04:24
|-
| 3 || Poo Maalayae || Ilaiyaraaja, S. Janaki || 04:21
|-
| 4 || Poovilae Medai || P. Jayachandran, P. Susheela || 02:39
|-
| 5 || Vaarayo Vaanmathi || Ramesh, Usha Srinivasan || 04:33
|-
| 6 || Vaidhegi Raman || S. Janaki || 04:22
|}

== References ==
 

==External links==
* 

 

 
 
 
 
 


 