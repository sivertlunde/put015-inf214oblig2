Gretna Green (1915 film)
 
{{Infobox film
| name           = Gretna Green
| image          = Gretna Green 1915 filmposter.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = Film poster.
| director       = Thomas N. Heffron
| producer       = 
| writer         = Thomas Heffron (scenario)
| based on       =  
| starring       = Marguerite Clark Arthur Hoops Wilmuth Merkyl
| music          = 
| cinematography = 
| editing        = 
| studio         = Famous Players Film Company
| distributor    = Paramount Pictures
| released       =  
| runtime        = 40 mins.
| country        = United States
| language       = Silent English intertitles
}}
 silent romantic comedy film directed by Thomas N. Heffron and distributed through Paramount Pictures. It is based on the Broadway play by Grace Livingston Furniss and stars Marguerite Clark.  The film is now presumed Lost film|lost. 

The film and the play are based on the fame of the Scottish town Gretna Green. The town has become a popular destination for couples who want to legally marry as people under the age of 16 can do so without their parents consent. 

==Cast==
* Marguerite Clark - Dolly Erskine
* Arthur Hoops - Sir William Chetwynde
* Helen Lutrell - Lady Chetwynde
* Lyster Chambers - Lord Trevor
* Wilmuth Merkyl - Earl of Basset
* George Stilwell - Captain Cardiff
* J. Albert Hall - Colonel Hooker
* Martin Reagan - Innkeeper
* Julia Walcott - Innkeepers Wife

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 


 
 