The Tale of Genji (1951 film)
 
{{Infobox film
| name           = The Tale of Genji
| image          = The Tale of Genji - 1951 film.jpg
| caption        = Scene from the film.
| director       = Kōzaburō Yoshimura
| producer       = Masaichi Nagata
| writer         = Shikibu Murasaki Kaneto Shindo Junichirô Tanizaki
| starring       = Kazuo Hasegawa
| music          = Akira Ifukube
| cinematography = Kôhei Sugiyama
| editing        = 
| distributor    = 
| released       =  
| runtime        = 121 minutes
| country        = Japan
| language       = Japanese
| budget         = 
}}
 same name. It was entered into the 1952 Cannes Film Festival.   

==Cast==
* Kazuo Hasegawa as Hikaru Genji
* Michiyo Kogure as Fujitsubo
* Machiko Kyô as Awaji no ue
* Nobuko Otowa as Murasaki no ue
* Mitsuko Mito as Aoi no ue
* Yuji Hori as Yoshinari
* Denjirô Ôkôchi as Takuma nyudo
* Chieko Soma as Kiritsubo
* Yumiko Hasegawa as Oborozukiyo no kimi
* Chieko Higashiyama as Kobiden
* Osamu Takizawa as Kiritsubo gomon
* Kentaro Honma as Kashira
* Yuriko Hanabusa as Kiritsubos mother
* Hisako Takihana as Nun
* Taiji Tonoyama as Priest

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 