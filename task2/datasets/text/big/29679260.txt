New Year's Eve (film)
 
{{Infobox film
| name           = New Years Eve
| image          = New Years Eve Poster.jpg
| caption        = Theatrical release poster
| director       = Garry Marshall
| producer       = Mike Karz Wayne Allan Rice Garry Marshall
| writer         = Katherine Fugate
| starring       = {{plainlist |
*Halle Berry
*Jessica Biel
*Jon Bon Jovi
*Abigail Breslin Chris "Ludacris" Bridges
*Robert De Niro
*Josh Duhamel
*Zac Efron Hector Elizondo  
*Katherine Heigl
*Ashton Kutcher
*Seth Meyers
*Lea Michele
*Sarah Jessica Parker
*Michelle Pfeiffer
*Til Schweiger
*Hilary Swank
*Sofía Vergara}}
| music          = John Debney
| cinematography = Charles Minsky
| editing        = Michael Tronick
| studio         = New Line Cinema Wayne Rice Films Karz Entertainment
| distributor    = Warner Bros. Pictures
| released       =    
| runtime        = 118 minutes 
| country        = United States
| language       = English
| budget         = $56 million 
| gross          = $142,044,638 http://boxofficemojo.com/movies/?id=newyearseve.htm Box Office Mojo. Retrieved December 23, 2011. 
}} vignettes of Chris "Ludacris" Bridges, Robert De Niro, Josh Duhamel, Zac Efron, Héctor Elizondo, Katherine Heigl, Ashton Kutcher, Seth Meyers, Lea Michele, Sarah Jessica Parker, Michelle Pfeiffer, Til Schweiger, Hilary Swank and Sofía Vergara.

==Plot== Times Square ball drop with the help of her friend Sam (Josh Duhamel). Meanwhile, after being nearly run over by a car and denied a vacation, Ahern Records secretary Ingrid Withers (Michelle Pfeiffer) quits her job and offers the deliveryman Paul Doyle (Zac Efron) tickets for the Ahern Records Masquerade Ball if Paul helps her complete a series of New Years resolutions before midnight, which he accepts.

Pauls sister Kim Doyle (Sarah Jessica Parker) is having trouble with her teenage daughter Hailey (Abigail Breslin) who wants to spend New Years Eve with her friends and her boyfriend Seth Anderson (Jake T. Austin) in Times Square. Pauls friend, comic book illustrator Randy (Ashton Kutcher), who hates New Years Eve after his girlfriend left him on a date, gets stuck in an elevator with Elise (Lea Michele), an aspiring singer who will be providing back-up for musician Daniel Jensen (Jon Bon Jovi) in his show at Times Square. He also prepares to perform at the Ahern Records ball, where he rekindles his feelings for his ex-girlfriend, Laura (Katherine Heigl).

At a nearby hospital, Stan Harris (Robert De Niro), a man in the final stages of cancer who refuses chemotherapy and who only wishes to see the ball drop one last time, is kept company by Nurse Aimee (Halle Berry) and Nurse Mindy (Alyssa Milano) after his doctor (Cary Elwes) reveals he will not last much longer. In the same hospital, a young couple named Griffin (Seth Meyers) and Tess Byrne (Jessica Biel) are about to have their first child, and they compete with another couple, James (Til Schweiger) and Grace Schwab (Sarah Paulson), for a bonus offered to the family of the first child born in the new year. Elsewhere, Sam, a businessman from Ahern Records, attempts to go to the Ahern Records Ball, where he is to deliver an important speech, after his car malfunctions outside New York City, all the while wondering if he should attend a meeting with a mysterious woman he met and fell in love with on the previous New Years Eve.

In the early evening, one of the billboard lights of the Times Square panel malfunctions, jamming the ball and forcing Claire to call Kominsky (Héctor Elizondo), an electrician who the company had fired a few weeks prior. Kominsky repairs the ball before midnight, and, in gratitude, Claire leaves him in charge of the operation, and rushes to see the ball drop with her father, Stan. Meanwhile, Nurse Aimee has a video conference with her husband (Common (rapper)|Common), a soldier serving in Afghanistan. Stan passes away just after the ball drops and a chorus of Auld Lang Syne, with Claire holding his hand.

Paul helps Ingrid complete all the items on her list, and she gives him the tickets. Meanwhile, Randy and Elise bond, and, as they are about to kiss, the elevator is repaired by the building superintendent (James Belushi) and Elise rushes to Jensens show. Randy notices she forgot her rubber band and rushes to give it to her. At Times Square, Jensen leaves midway during his show to return to the Ahern Ball to apologize to Laura, who accepts him back and leaves with the approval of Sam. With Jensen gone, Elise is called to replace him and attracts the attention of the crowd. She kisses Randy, and they start a romantic relationship.

Griffin and Tess have their baby and, although it is born first, they allow James and Grace to have the bonus after discovering they already have two other children to provide for. Meanwhile, after being forbidden from attending the celebration, Hailey runs away to Times Square, where she sees Seth being kissed by another girl. Heartbroken, she meets and is comforted by her mother. Seth finds them and apologizes, claiming the girl stole a kiss from him. Hailey forgives and kisses him. Her mother allows her to go to an after-party. Kim then goes to a restaurant to meet Sam, who had succeeded in delivering his speech. She is the mysterious woman he met one year prior, and they finally discover each others names.

==Cast==
 
;Resolution Tour
* Michelle Pfeiffer as Ingrid
* Zac Efron as Paul
* Charlotte Marshall-Fricker as Caring Teenage Girl
* Fiona Choi as Balinese Woman
* Mary Marguerite Keane as Pet Adoption Clerk
* Michael Mandell as Murray
* Patrick Reale as Times Square Cop
* John Lithgow as Jonathan Cox

;Hospital Story
* Robert De Niro as Stan Harris
* Halle Berry as Nurse Aimee
* Cary Elwes as Stans doctor
* Alyssa Milano as Nurse Mindy Common as Soldier
* Barbara Marshall as Head Nurse Helen

;Maternity Ward
* Jessica Biel as Tess Byrne
* Seth Meyers as Griffin Byrne
* Sarah Paulson as Grace Schwab
* Til Schweiger as James Schawb
* Carla Gugino as Spiritual Dr. Morriset
* Amber Bela Muse as Nurse Risa
* Paul Vogt as Male Nurse
* Ross Ryman as Pedicab Driver
* Kal Parekh as Male Obstetrician

;Jensen and Lauras Story
* Katherine Heigl as Laura
* Jon Bon Jovi as Jensen
* Sofia Vergara as Ava
* Russell Peters as Chef Sunil
* Serena Poon as Chef Ming
* Matthew Estipona as Matthew Estipona

;Elevator
* Ashton Kutcher as Randy
* Lea Michele as Elise
* Jim Belushi as Building Superintendent
* Lillian Lifflander as Mrs. Lifflander

;Mother & Daughter
* Sarah Jessica Parker as Kim 
* Abigail Breslin as Hailey
* Jake T. Austin as Seth
* Mara Davi as Mika
* Jacklyn Miller as Choreographer
* Cassidy Rieff as Piper
* Nat Wolff as Walter
* Kendra Jain as Kelly
* Julia Randall as Julia
* Christian Fortune as Cody
* Tatyana Disla as Tatyana
* Cheally Phoung as Cheally
* Marvin Braverman as Vendor Marvin
* Alexandra Rose Guthy as Screaming Rocker Girl
* Denise Violante as Female Cop Denise
* Katherine McNamara as Lily Bowman
* Norman Bukofzer as Newark Commuter
* Beth Kennedy as Pipers Mom

;Ahern Party
* Josh Duhamel as Sam
* Joey McIntyre as Groom Rory
* Jackie Seiden as Bride Trish
* Sean OBryan as Pastor Edwin Larry Miller as Harley Jack McGee as Grandpa Jed
* Yeardley Smith as Maude
* Benjamin McGowan as Logan
* Jon-Christian Costable as Duncan
* Juliette Allen-Angelo as Saide
* Penny Marshall as herself
* Drena De Niro as Ahern Waitress
* Vanessa Mendoza as Leopard Print Girl
* Christine Lakin as Waitress Alyssa
* Sandra Taylor as Patty
* Shea Curry as Wendy
* Earl Rose as Pianist
* Johnny DeBrito as Male Party Guest Samuel E. Mitchell as Pennys dance partner
* Amare Stoudemire as Party Dancer
* Cherry Jones as Mrs. Rose Ahern

;Times Square
* Hilary Swank as Claire Morgan Chris "Ludacris" Bridges as Brendan
* Kathleen Marshall as Stage Manager Charlotte
* Joey Sorge as Radio Reporter Arthur
* Rob Nagle as Office Nolan
* Matt Walker as Engineer Douglas
* Wedil David as Reporter Christina
* David Valcin as Reporter Murphy
* Stephanie Fabian as Reporter Lupe
* Patrick Collins as Reporter Fadda
* Pat Battle as Reporter Pat Battle
* Tom Hines as Announcer Brady Finley Greg Wilson as Wade the Aide
* Héctor Elizondo as Kominsky
* Anna Aimee White as Ginger Adams
* Sam Marshall as Crowd Surfing Kid
* Susan Silver as Bunny Friedberg
* Emily Moss Wilson as Crazy Jensen Fan
* Bob Weston as Jensen Concert Police
* Matthew Broderick as Mr. Buellerton
* Lucy Woodward and Stephanie Alexander as Backup Singers
* Nicole Michele Sobchack as Kissing Reveler #1
* Anna Kulin as Kissing Reveler #2
* Ryle J. Neale as Kissing Female Cop
* Lily Marshall-Fricker as Reveler Lucy Camille
* Lori Marshall as Reveler Mrs. Camille
* Mayor Michael Bloomberg as himself
* Ryan Seacrest as himself
 

==Reception==
The film has a score of 22 on Metacritic, a "generally unfavorable" score based on 30 reviews.  The film has a 7% rating on Rotten Tomatoes based on 131 reviews. 
 The Telegraph named New Years Eve one of the ten worst films of 2011.  British film critic Mark Kermode named it as the worst film of 2011.  On the more positive side, Entertainment Weekly s Owen Gleiberman said, "New Years Eve is dunderheaded kitsch, but its the kind of marzipan movie that can sweetly soak up a holiday evening." 
 Jack and Jill.

===Box office===
The film opened at the No. 1 spot at the box office with $13.0 million.   It made $54,544,638 in the United States and Canada, as well as $87,500,000 in other countries, for a worldwide total of $142,044,638. 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 