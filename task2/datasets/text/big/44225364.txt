Dravidan (1998 film)
{{Infobox film
| name           = Dravidan
| image          =
| caption        =
| director       = Mohan Kupleri
| producer       = C Ramkumar
| writer         = Josy Vagamattam
| screenplay     =
| starring       = Jagathy Sreekumar Rajan P Dev Vijayakumar Chandni
| music          = S. P. Venkatesh
| cinematography = Utpal V Nayanar
| editing        = G Murali
| studio         = Ottappalam Films
| distributor    = Ottappalam Films
| released       =  
| country        = India Malayalam
}}
 1998 Cinema Indian Malayalam Malayalam film, directed by Mohan Kupleri and produced by C Ramkumar. The film stars Jagathy Sreekumar, Rajan P Dev, Vijayakumar and Chandni in lead roles. The film had musical score by S. P. Venkatesh.   

==Cast==
*Jagathy Sreekumar
*Rajan P Dev
*Vijayakumar
*Chandni
*Zeenath Usha
*Vani Viswanath Vijayaraghavan

==Soundtrack==
The music was composed by S. P. Venkatesh and lyrics was written by Gireesh Puthenchery. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ampada padavili || MG Sreekumar || Gireesh Puthenchery || 
|-
| 2 || Shoshannappoove Nin || K. J. Yesudas, Chorus || Gireesh Puthenchery || 
|}

==References==
 

==External links==
*  

 
 
 

 