Du Barry, Woman of Passion
{{infobox film
| name           = Du Barry, Woman of Passion
| image          =
| imagesize      =
| caption        = Sam Taylor
| producer       = Joseph M. Schenck
| writer         = David Belasco (play) Sam Taylor (adaptation)
| starring       = Norma Talmadge
| music          =
| cinematography = Oliver T. Marsh
| editing        = Allen McNeil
| distributor    = United Artists
| released       = October 11, 1930
| runtime        = 10 reels 
| country        = United States
| language       = English
}} talking film drama starring Norma Talmadge, produced by her husband Joseph Schenck, released through United Artists, and based on a 1901 stage play Du Barry written and produced by David Belasco and starring Mrs. Leslie Carter.

This film is the second talking picture of silent star Talmadge and also her last motion picture.  Prints of this film survive in the Library of Congress.  

==Cast==
*Norma Talmadge - Madame Du Barry
*William Farnum - Louis XV
*Conrad Nagel - Cosse de Brissac
*Hobart Bosworth - Duc de Brissac Ullrich Haupt - Jean Du Barry
*Alison Skipworth - La Gourdan
*E. Allyn Warren - Denys
*Edgar Norton - Renal
*Edwin Maxwell - Maupeou
*Henry Kolker - DAiguillon
*Oscar Apfel - ?
*Eugenie Besserer - Rosalie/Prison Matron
*Earle Browne - Stage Director
*Knute Erickson - Jailer
*Cissy Fitzgerald - Bit role
*Clark Gable - Extra
*Lucille La Verne - Bit role
*Tom Ricketts - Kings Aide
*Tom Santschi - Bit role
*Michael Visaroff - Bit

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 

 