Allos gia to ekatommyrio
{{Infobox film
| name           = Allos gia to ekatommyrio
| image          =
| image size     =
| caption        =
| director       = Orestis Laskos
| producer       = Roussopouloi-G. Lazaridis-D. Sarris-K. Psarras Productions
| writer         = Nikos Tsiforos Giorgos Lazaridis Polyvios Vasileiadis
| starring       = Mimis Fotopoulos Giannis Gkionakis Beata Assimakopoulou Vassilis Avlonitis Eleni Anousaki Stavros Paravas Giorgos Kappis Katerina Gioulaki Marika Krevata Periklis Christoforidis Dimitris Mavrommatis
| cinematography =
| photographer   =
| music          = Giorgos Mitsakis
| editing        =
| distributor    = Finos Film
| released       =  
| runtime        = 90 minutes
| country        = Greece
| language       = Greek
}}
Allos gia to ekatommyrio or Alos gia/yia to ekatomirio ( , Somebody for a Millionaire) is a 1964 Greek comedy film directed by Orestis Laskos and written by Nikos Tsiforos and starring Mimis Fotopoulos, Giannis Gkionakis, Beata Assimakopoulou, Vassilis Avlonitis and Eleni Anoussaki.

==Plot==
 
Until some jokingly honest employee holds his bill for his friend for a millionaire without any directions.  The problems started when his friend died in an air crach and the honest employee (Mimis Fotopoulos) prepared to return for much to the lawful inheritance.

==Cast==
*Mimis Fotopoulos as Savvas
*Vasilis Avlonitis as Babis
*Giannis Gkionakis as Kokos
*Eleni Anousaki as Eleni
*Stavros Paravas as Stratos Beata Assimakopoulou as Anna
*Giorgos Kappis as Aristidis
*Katerina Gioulaki as Loulou
*Marika Krevata as a relative
*Periklis Christoforidis as a director
*Dimitris Mavrommatis as Dimitris

==External links==
*  

 
 
 
 
 
 