Wednesday's Child (film)
{{Infobox film
| name           = Wednesdays Child
| image          = 
| alt            = 
| caption        = 
| director       = John S. Robertson Ray Lissner (assistant)
| producer       = Pandro S. Berman
| screenplay     = Willis Goldbeck
| based on       =   Edward Arnold Frank Conroy
| music          = Max Steiner
| cinematography = Harold Wenstrom 	
| editing        = George Hively
| studio         = RKO Pictures
| distributor    = RKO Pictures
| released       =  
| runtime        = 68 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Edward Arnold, Frank Conroy. The film was released on October 26, 1934, by RKO Pictures.  {{cite web|url=http://www.nytimes.com/movie/review?res=9C06E5D81E3CE23ABC4D52DFB467838F629EDE|title=Movie Review -
  Wednesday s Child - Master Frankie Thomas in a Fine Screen Version of Wednesdays Child -- Dealers in Death. - NYTimes.com|publisher=|accessdate=9 September 2014}}  

==Plot==
 

== Cast ==
*Karen Morley as Kathryn Phillips Edward Arnold as Ray Phillips
*Frankie Thomas as Bobby Phillips
*Robert Shayne as Howard Benson Frank Conroy as The Judge
*Shirley Grey as Louise Paul Stanton as Keyes
*David Durand as Charles
*Richard Barbee as Dr. Stirling
*Frank M. Thomas as Attorney for the Defense 
*Mona Bruns as The Nurse
*Elsa Janssen as Martha

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 