Ponnu Velayira Bhoomi
{{Infobox film
| name           = Ponnu Velayira Bhoomi
| image          =
| image_size     =
| caption        =
| director       = K. Krishnan
| producer       = A. G. Krishnan
| writer         = K. Krishnan
| starring       =   Deva
| cinematography = Ravi Shankar
| editing        = A. K. Shankar C. Srinivasan
| distributor    =
| studio         = AGS Films International
| released       =  
| runtime        = 135 minutes
| country        = India
| language       = Tamil
}}
 1998 Tamil Tamil comedy-drama Latha playing Deva and was released on 10 April 1998.    

==Plot==

Palanisamy (Rajkiran) is a kind-hearted rich farmer, he soon becomes the village chief. Palanisamy is known in his village for holding demonstrations. The poor village girl Valli (Vineetha) falls in love with Palanisamy until Valli knows that Palanisamy is in fact married to Pushpa (Kushboo) who is mentally ill.

In the past, Pushpa was an arrogant english-medium school owner. She first clashed with Palanisamy, then they fell in love with each other. On a misunderstanding, they get married and they lived happily together. Pushpas father (Vittal Rao) wanted to take revenge on Palanisamy. During her childbirth, Pushpas newborn baby died and Pushpa couldnt get pregnant again. Just after this incident, Pushpa became mentally ill.

Valli decides to take care of Pushpa. Later, at the village court, Valli claims that Palanisamy married her. In the meantime, Pushpa becomes again as she was and the upset Pushpa goes back to her parents. Palanisamy feels lost.

Meanwhile, the farmers has been scammed by a vicious bussinessman (Manivannan) and Pushpas father. Palanisamy recovers the farmers properties and ultimately saves Pushpas father from the angry farmers. Pushpa then reveals to Palanisamy that she wanted to see Palanisamy having a baby so she plans everything with Vallis help. Palanisamy and Valli forgive Pushpa. Palanisamy and Pushpa live happily ever after.

==Cast==

 
*Rajkiran as Palanisamy
*Kushboo as Pushpa
*Vineetha as Valli
*Manivannan
*Vadivelu as Amavasai
*R. Sundarrajan (director)|R. Sundarrajan as Arumugam
*Venniradai Moorthy
*Vittal Rao as Pushpas father Latha as Pushpas mother
*Vichithra
*Shakeela
*Vaani as Vallis mother
*Vijayamma
*Sekhar
*Idichapuli Selvaraj
*Vaiyapuri
*Pollachi Sukumaran
*Ravichandran
*Ramakrishna Iyer
*MLA Thangaraj
 

==Soundtrack==

{{Infobox Album |  
| Name        = Ponnu Velayira Bhoomi
| Type        = soundtrack Deva
| Cover       = 
| Released    = 1998
| Recorded    = 1998 Feature film soundtrack
| Length      = 23:22
| Label       =  Deva
| Reviews     = 
}}

The film score and the soundtrack were composed by film composer Deva (music director)|Deva. The soundtrack, released in 1998, features 5 tracks with lyrics written by Vaali (poet)|Vaali.  

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Duration
|- 1 || Paattu Kattum Kuyilu || Swarnalatha || 5:09
|- 2 || Oorae Mathichi Nikkum || Mano (singer)|Mano, Swarnalatha || 4:09
|- 3 || Poya Unn Moonjule || Deva (music director)|Deva, Anuradha Sriram, Swarnalatha, Vadivelu || 3:36
|- 4 || Vettu Vettu || S. P. Balasubrahmanyam || 4:33
|- 5 || Mania Thalikatti || K. S. Chithra || 5:55
|}

==References==
 

 
 
 
 
 