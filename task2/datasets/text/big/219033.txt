Hellcats of the Navy
{{Infobox film
| name           = Hellcats of the Navy
| image          = Hellcats of the navy poster.jpg
| image_size     = 200px
| caption        =
| director       = Nathan Juran
| producer       = Charles H. Schneer
| based on       =   Bernard Gordon
| narrator       = Nancy Davis Robert Arthur Max Showalter
| music          = Mischa Bakaleinikoff
| cinematography = Irving Lipman
| editing        = Jerome Thoms
| studio         = Morningside Productions Columbia
| released       = 1957
| runtime        = 82 minutes
| country        = United States English
| budget         =
| gross          = 
}}
 Nancy Davis, her then professional name. Married since 1952, this was the only film in which they appeared together. The story of the film is based on the non-fiction book Hellcats of the Sea by Vice Admiral Charles A. Lockwood and Hans Christian Adamson.

==Plot== Nancy Davis) back at home, gets into a dangerous situation, Abbott must struggle to keep his personal and professional lives separate in dealing with the crisis.

The results arouse ill feelings in the crew and especially Abbotts executive officer Lt. Commander Landon (Arthur Franz) who asks his captain to let him air his views in confidence. The results lead Abbott to write in Landons efficiency report that he should never be given command of a naval vessel, resulting in further ill feeling between the two.

==Cast==
* Ronald Reagan as Commander Casey Abbott Nancy Davis as Nurse Lt. Helen Blair
* Arthur Franz as Lt. Cmdr. Don Landon Robert Arthur as Freddy Warren
* William Leslie as Lt. Paul Prentice
* William Bill Phillips as Carroll (as William Phillips)
* Harry Lauter as Lt. Wes Barton
* Michael Garth as Lt. Charlie
* Joe Turkel as Chick (as Joseph Turkel)
* Don Keefer as Jug

==Production== Fleet Admiral Chester W. Nimitz endorses the film on screen at the start of the movie, and is also later played, as a character, by actor Selmer Jackson.  Reagan noted in his autobiography that he was disappointed in the film overall, having expected a result more like Destination Tokyo, a major Warner Bros. film of a decade previous.  The diminishing status of the films he was offered led to his leaving the big screen.
 USS Pueblo during its capture by North Korea in 1968. 

During the production of the film, as the USS Besugo was about to get underway, an argument ensued between one of the unions and the director. It must be noted that it was difficult for a submarine tied up in San Diego to get underway while a tide was running, so there was only a short window of opportunity to maneuver the boat away from the pier. Besugo was one of the first boats to get nylon lines, and when stretched, they can get about "as big around as a pencil" and become lethal if they break under strain. In the meantime, the order is given to the helmsman to answer all bells. Reagan took this opportunity to practice his lines on the deck of the boat hollering out, "Ahead one third, starboard back full... etc." About this time, the nylon lines were stretched to their breaking point where one of the officers gives the command, "All stop, ALL STOP, Goddammit, ALL STOP!" and Reagan, totally oblivious to what was going on, continued to practice his lines, rocking back and forth on his feet with his hands behind his back... as if nothing were wrong at all. 

==Reception==
Glenn Erickson of DVD Talk reviewed the DVD release of Hellcats, and thought that although the direction was "competent", the script was "completely derivative and cornball". He went on to criticize the lack of realism in the supporting characters and the use of stock footage, especially where footage of a US Navy patrol boat was used as a Japanese ship. Overall, he described the film itself as "fair".    David Krauss of Digitally Obsessed described the production values as "bargain basement" and found the stiff performances of the cast alienated viewers. He gave the film a C for style and a B- for substance, although he also described the direction as "dry as a military briefing on CNN".   

Erick Harper at DVD Verdict thought that the movie followed a series of submarine war movie clichés, in the love triangle and the action sequences. Parts of the film were compared to Star Trek, in that it followed a standard Hollywood formula. He described Ronald Reagan as "comfortable" and "believable",    and said that the film was "worth checking out for the historical value, if nothing else". 

==See also==
* Ronald Reagan filmography

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
  

 