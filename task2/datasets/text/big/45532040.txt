Hjärter Knekt
{{Infobox film
| name           = Hjärter Knekt
| image          =
| caption        =
| director       = Hasse Ekman
| producer       =
| writer         = Hasse Ekman
| narrator       =
| starring       = Hasse Ekman Hans Strååt Margareta Fahlén Eva Dahlbeck
| music          = Håkan von Eichwald
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = 84 minutes
| country        = Sweden Swedish
| budget         =
| gross          =
}}
 Swedish drama drama film directed by Hasse Ekman.

==Plot summary==
Lieutenant Anders Canitz only interests in life are women and horses. His lives on inherited money and has spent all of it. When he finds out that his brother leaves home for a while to meet with an old flame of his, Anders seduces his brothers wife. He breaks hearts, ruin other peoples relationships, lies and cheats with no regrets, or has he?

==Cast==
*Hasse Ekman as Anders Canitz, Lieutenant
*Margareta Fahlén as Elsa Canitz, his sister-in law
*Hans Strååt as Wilhelm Canitz, Anders brother
*Eva Dahlbeck as Gun Lovén, writer
*Holger Löwenadler as Krister Bergencreutz
*Gertrud Fridh as Charlotta Ulfhammar, Kristers fiancée
*Åke Fridell as Berra 
*Dagmar Ebbesen as Kristina Lundgren, Anders housekeeper
*Hjördis Petterson as Elisabeth Canitz, Anders and Wilhelms sister
*Ingrid Thulin as Gunvor Ranterud
*Tord Stål as Director Benzel
*Sif Ruud as Prostitute
*Yvonne Lombard as Margareta Lieven
*Margit Andelius as fröken Appellöf, Secretary 
*Barbro Larsson as Girl in the grass 

==External links==
* 

 

 
 
 
 
 
 
 

 