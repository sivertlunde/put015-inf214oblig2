Sex of Angels
 
{{Infobox film
| name = Sex of Angels
| image = File:Angels of Sex DVD Cover.jpg
| alt = 
| caption = 
| director = Xavier Villaverde
| producers = Pancho Casal, Jordi Mendieta, Bob Costa, and Leonel Vieira
| writer = Ana Maroto
| starring = Álvaro Cervantes  Àstrid Bergès-Frisbey  Llorenç González  Sonia Méndez  Marc Pociello  Marc García Coté  Julieta Marocco  Ricard Farré  Lluisa Castell
| music = Eduardo Molinero
| cinematography = Sergi Gallardo
| editing = Guillermo Represa
| studio =
| distributor = IFC Midnight
| released =  
| runtime = 106 minutes
| country = Spain Catalan
| budget = 
| gross = 
}}
 Spanish film by Xavier Villaverde starring Álvaro Cervantes, Àstrid Bergès-Frisbey, and Llorenç González. It was released in 2012.

==Synopsis==
Bruno, a struggling student, loves his girlfriend Carla but discovers a new side to himself when he meets a street dancer named Rai.  A new generation navigates sexual fluidity, torn affections, and open relationships in a steamy love triangle.

==Cast==
*  as Rai
*Àstrid Bergès-Frisbey as Carla
*Llorenç González as Bruno
*Lluïsa Castell as Carlas mother
*Marc García-Coté as Adrián
*Ricard Farré as Oscar
*Sonia Méndez as Marta
*Julieta Marocco as María
*Marc Pociello as Dani

==Location==
The film was shot at and is set in various locations around Barcelona.

== See also ==
* Discussing the sex of angels, the scholastic question from which the title is derived

==External links==
* 
* 

 
 
 
 
 