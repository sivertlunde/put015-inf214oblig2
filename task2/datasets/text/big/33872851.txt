The Lone Wolf Meets a Lady
{{Infobox film
| name           = The Lone Wolf Meets a Lady
| image          = 
| image_size     = 
| alt            = 
| caption        = 
| director       = Sidney Salkow
| producer       = Irving Briskin Ralph Cohn
| writer         = Louis Joseph Vance (story) Wolfe Kaufman (story) John Larkin John Larkin
| narrator       =  Jean Muir 
| music          = Sidney Cutner
| cinematography = Henry Freulich
| editing        = Al Clark
| studio         = 
| distributor    = Columbia Pictures Corporation
| released       =  
| runtime        = 71 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 drama directed Jean Muir. Lone Wolf series starring Warren William as Michael Lanyard. The film also introduces a sidekick for Lanyard, his bumbling valet Jamison, played perfectly by the incomparable Eric Blore. Blore would play Jamison in seven more films. 

==Plot summary==
 
A reformed jewel thief tries to clear a society beauty of murder charges. 

==Cast==
* Warren William as Michael Lanyard   Jean Muir as Joan Bradley  
* Eric Blore as Jamison  
* Victor Jory as Clay Beaudine   Roger Pryor as Peter Rennick  
* Warren Hull as Bob Penyon  
* Thurston Hall as Inspector M.J. Crane  
* Fred Kelsey as Detective Dickens (as Fred A. Kelsey)  
* Robert Emmett Keane as Peter Van Wyck  
* Georgia Caine as Mrs. Penyon   William Forrest as Arthur Trent  
* Marla Shelton as Rose Waverly  
* Bruce Bennett as McManus - Motorcycle Policeman

==Notes== Columbia that a number of changes in the script were necessary before the film could receive certification. Among the many demands by PCA were that the "radio announcer must not be characterized, in any way, as a pansy"; that the drinking in the film be "held to an absolute minimum"; that the hiccoughing be eliminated; that the "business of Pete slapping and cuffing Joan" be eliminated; that the film not reveal the details of the crime; and that there be "no showing of panties or other particularly intimate garments." 

==References==
 	

== External links ==
*  
*  
*  

 
 
 
 
 