Career of Nikos Dyzma
 
{{Infobox Film 
 | name= Career of Nikos Dyzma
 | image=
 | country= Poland
 | released=  
 | runtime= 103 minutes
 | director= Jacek Bromski
 | writer= Jacek Bromski Tomasz Kępski Ewa Kasprzyk Andrzej Grabowski Krzysztof Pieczyński Tomasz Schuchardt Filip Bobek
 | music = Henri Seroka Elektryczne Gitary Kuba Sienkiewicz
 | cinematography = Arkadiusz Tomiak
 | editing= Krzysztof Szpetmański
 | producer= Henryk Parnowski Michał Balcerzak Włodzimierz Otulak
 | distributor    =Vision Film Distribution
 | language= Polish
}}
Career of Nikos Dyzma (also known as Kariera Nikosia Dyzmy in Polish) is a 2002 Polish film directed by Jacek Bromski. 

==Cast list== 
* Cezary Pazura as Nikodem, "Nikos" Dyzma
* Anna Przybylska as Jadzia, the wife of the minister Jaszuński Ewa Kasprzyk as Senator Anna Walewska
* Andrzej Grabowski as Roman Kiliński
* Krzysztof Globisz as Minister Jaszuński
* Olgierd Łukaszewicz as Prime Minister
* Lew Rywin as Terkowski
* Katarzyna Figura as Nurse

==Awards==
* Eagles
year: 2003
Type: appointment
Award: Eagle
Category: Best Costume Design (Margaret Braszka)

* Golden Ducks
year: 2003
Type: appointment
Award: Golden Duck
Category: Best Polish actress (Anna Przybylska)

* Golden Lions
year: 2002
Type: appointment
Award: Golden Lions
Category: Participation in the main competition (Jacek Bromski)

==Other Information==

Period Picture: October 10 - November 21, 2001. 

Landscapes: Pabianice (Lutheran Cemetery), Lodz (Poznanski Palace Street. Ogrodowa, ul. Marathon, rector Film School - Film Walewska senators office, the rector of the Academy of Medicine - a film cabinet minister Jaszuńskiego, home of "Polonia", Al. Mickiewicza - film study Kilińskiego, a hypermarket M1, Poniatowski Park, st. Wolczanska requested to fill at the hospital), Wola, near Lodz Błędowska (golf course), Leśmierz (refinery), Warsaw, Gdynia (port).

Gala premiere took place March 7, 2002 in Lodz, in the cinema "Baltic" and the March 14, 2002 in Warsaw at the cinema Cinematheque. "

In 2002, the film received an award of the Association "Polish Cinemas - Golden Ticket for a fourth place ranking in the audience.

The film can be confused with the Polish comedy of manners in 1956 under the title "Nicodemus Dyzma"

The film can be confused with the Polish soap opera from 1980, which is a soap opera costume and satire titled "The Career of Nicodemus Dyzma"

Sound: Janusz Bouillon and Michael Kosterkiewicz

==References==
 

==External links==
* Excellent review  
*  

 
 
 