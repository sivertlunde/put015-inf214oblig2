Salami Aleikum
{{Infobox film
| name           = Salami Aleikum
| image          = Salami Aleikum.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Ali Samadi Ahadi
| producer       = Oliver Stoltz
| writer         = Ali Samadi Ahadi and Arne Nolting
| screenplay     = 
| story          = 
| narrator       = 
| starring       = Navíd Akhavan, Anna Böger, Michael Niavarani, Wolfgang Stumph, Proschat Madani, Eva-Maria Radoy
| music          = Ali N. Askin
| cinematography = Bernhard Jasper
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        =  99 min
| country        = Germany Persian
| budget         = 
| gross          = 
}} Persian Word plays and German and Iranian Characters are well Characterized. Salami Aleikum was a successful film in Germany.      

== Summary == reunification East Germany to buy sheep. In a village there, Mohsen falls in love with former shot-putter Ana (Anna Böger) who is vegetarian. Mohsen tells them that he is from a Persian textile dynasty, and the villagers hope he saves the villages textile industry. Mohsen becomes more and more en vogue until his family come to find him.    

== Prizes ==
Salami Aleikum won the International Filmfest Emden 2009, the second prize of the Bernhard Wicki Film Prize and the NDR Film Prize for the offspring. The film was nominated for the MFG-Star Baden-Baden 2009.

The film won the prize of the German Film Critics Award 2009, for Best Debut Film at the Berlinale. In 2010 the films composer Ali N. Askin was nominated for the Deutscher Filmpreis.
On 9 May 2012 Salami Aleikum received the Civis media prize. 

== References ==
 
* http://www.salami-aleikum.at/06-stab-besetzung.php

== External links ==
*  
*   Official website

 
 
 
 