Totalmente Inocentes
{{Infobox film
| name           = Totalmente Inocentes
| image          = Totalmente Inocentes.jpg
| caption        = Theatrical release poster
| director       = Rodrigo Bittencourt
| producer       = Iafa Britz Mariza Leão
| writer         = Rodrigo Bittencourt Rafael Dragaud Carolina Castro
| starring       = Fábio Porchat Fabio Assunção Leandro Firmino Mariana Rios Kiko Mascarenhas
| music          = Marcos Kuzka
| cinematography = Fabio Burtin
| editing        = Maria Rezende
| studio         = Atitude Produções Globo Filmes Migdal Filmes
| distributor    = Paris Filmes
| released       =  
| runtime        = 90 minutes
| country        = Brazil
| language       = Portuguese
| budget         = 
| gross          = $2,659,775     City of God and Elite Squad. {{Cite web|url=http://ultimosegundo.ig.com.br/cultura/cinema/2012-09-06/totalmente-inocentes-parodia-filmes-de-favela-mas-esquece-de-fazer-rir.html |title="Totalmente Inocentes" parodia filmes de favela, mas esquece de fazer rir
 |accessdate=2014-04-26 |work=IG}} 

The film stars Fábio Porchat, Fabio Assunção, Mariana Rios, Kiko Mascarenhas, Fábio Lago, Leandro Firmino and Álamo Facó, in addition to the special guests Ingrid Guimarães, Felipe Neto, Vivianne Pasmanter and Di Ferrero, lead singer of the band NX Zero.   

The film received generally negative reviews from critics, and grossed a total of $2,659,775 in Brazil.   

==Plot==
The community of the DDC is at war. The Branquelo do Morro and the Diaba Loira shemale are vying for power in the community. Oblivious to this, Da Fé believes he needs to become the boss of the hill to conquer the love of Gildinha. Everything gets worse when the reporter Vanderlei forges a cover.   

==Cast==
 
* Fábio Porchat as Do Morro
* Lucas De Jesus as Da Fé
* Cauê Campos as Torrado
* Gleison Silva as Bracinho
* Fábio Assunção as Wanderlei
* Mariana Rios as Gildinha
* Kiko Mascarenhas as Diaba Loura
* Ingrid Guimarães as Raquel
* Vivianne Pasmanter as Dotôra Delegada Leandro Firmino as Tranquilo
* Fábio Lago as Nervoso
* Álamo Facó as China
* Ícaro Silva as Átila
* Marcelo Escorel as Capitão Marrentão
* Felipe Neto as Presenter of videocast
 

==Reception==

===Critical response===
Totalmente Inocentes received more negative reviews than positive, in several websites; one of the main Brazilian websites about movies, the AdoroCinema.com ranked the film with 1.5. The critic website wrote that "Totalmente Inocentes is a tiring film, it doesnt explores convincingly the material at hand   a movie totally weak." Critics from other well-known websites, like CineClick said "Totalmente Inocentes has a poor script and direction shortsighted and does not comply with the basic commitment of a comedy: make people laugh".    The website Cinema com Rapadura, quoted that "The humor is ridiculous and so unfunny".    Criticism from viewers of the websites were not different, mostly criticizing the script of Rodrigo Bittencourt.   

==References==
 

== External links ==
*  

 
 
 
 
 