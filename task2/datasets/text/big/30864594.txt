Paandav
 
 
{{Infobox film
| name = Paandav
| image =Paandav-1995.jpg
| writer =
| starring = Akshay Kumar Nandini
| director = Raj N. Sippy
| producer = Keshu Ramsay
| music = Jatin-Lalit
| lyrics =
| released = 3 March 1995
| language = Hindi
| budget =  
| gross =  
}}

Paandav is a 1995 Indian Bollywood film directed by Raj N. Sippy. It stars Akshay Kumar and Nandini.   

==Cast==
* Akshay Kumar ... Inspector Vijay Kumar
* Nandini ... Ritu
* Kanchan ... Nisha Tiwari
* Pankaj Dheer ... Hariya
* Mukesh Khanna ... ACP Ashwini Kumar

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Tere Liye Rehta Hai"
| Kumar Sanu, Alka Yagnik
|-
| 2
| "Aaj Main Ye Izhaar Karoon"
| Kumar Sanu, Kavita Krishnamurthy
|-
| 3
| "Sapne Sajaa Kar Apna Banakar"
| Kavita Krishnamurthy
|-
| 4
| "Pyar Ka Andaz Tum"
| Udit Narayan, Devki Pandit
|-
| 5
| "Kasam Hai Pyar Ki Tumhe"
| Kumar Sanu, Devki Pandit
|-
| 6
| "Ye Haina Pyaar Hi To Haina"
| Amit Kumar, Sadhana Sargam
|-
| 7
| "Ye Chaman Jo Jal Gaya"
| Udit Narayan, Amit Kumar
|-
| 8
| "Trailer O Trailer"
| Alisha Chinai
|}

==References==
 

==External links==
*  

 
 
 
 

 