The Noah's Ark Principle
{{Infobox film
| name           = Das Arche Noah Prinzip
| image          = Das Arche Noah Prinzip.jpg
| director       = Roland Emmerich
| producer       = Wolfgang Längsfeld Hans Weth Peter Zenk Executive producer Klaus Dittrich Ulrich Limmer Gabriele Walther
| writer         = Roland Emmerich
| starring       = Richy Müller Franz Buchrieser
| music          = Hubert Bartholomae
| cinematography = Egon Werdin
| editing        = Tomy Wigand
| studio         = 
| distributor    = Filmverlag der Autoren
| released       =  
| runtime        = 100 minutes
| country        = West Germany
| language       = German DEM 1,2 million
}}
The Noahs Ark Principle ( ) is a 1984 West German science fiction film written and directed by Roland Emmerich as his thesis at the Hochschule für Fernsehen und Film München (HFF).

While his fellow students typically raised and spent 20,000 Deutsche Mark for their final work, Emmerich managed to collect a budget of 1,200,000 DM (around US$600,000). 

This film, shot in color with mono sound, received a rating of 12 in West Germany, and was sold to 20 countries. It was submitted to the 34th Berlin International Film Festival    and received some acclaim  for technical skill and special effects, but won no prizes.

==Plot summary==
The year is 1997, and World Peace seems to have come, with most classic weapons of mass destruction having been abandoned. However, orbiting the Earth there is the European/American space station FLORIDA ARKLAB, capable of controlling the weather at any location on the planet underneath. A civil project by nature, it might be abused as an offensive weapon, since it could deliver devastation to any potential adversary simply by creating natural disasters such as storms and floods. No wonder the space station soon becomes the central point in rising political tensions between East and West, next stop World War 3 (as indicated by the tagline "The end of our future has already begun"). We follow the main protagonist Billy Hayes, an astronaut aboard the station, as he wades through a plot of secrecy and sabotage trying to tell friend from foe in the process.

==Cast==
* Richy Müller as Billy Hayes
* Franz Buchrieser as Max Marek
* Aviva Joel as Eva Thompson
* Matthias Fuchs as Felix Kronenberg
* Nikolas Lansky as Gregor Vandenburg
* Matias Heller as Security Guard

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 


 
 