The Black Viper
{{Infobox film
| name           = The Black Viper
| image          = The Black Viper.webm
| image_size     =
| caption        =
| director       = D. W. Griffith Wallace McCutcheon
| producer       =
| writer         =
| narrator       =
| starring       = D. W. Griffith Mack Sennett Anthony OSullivan
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = approximately 6 minutes
| country        = United States Silent English intertitles
| budget         =
}}

The Black Viper (aka La vipère noire in France) is a 1908 film directed by D. W. Griffith. The film was made by the American Mutoscope and Biograph Company when it and many other early film studios in Americas first motion picture industry were based in Fort Lee, New Jersey at the beginning of the 20th century. 

==Plot==
A thug accosts a girl as she leaves her workplace but a man rescues her. The thug vows revenge and, with the help of two friends, attacks the girl and her rescuer again as theyre going for a walk. This time they succeed in kidnapping the rescuer. He is bound and gagged and taken away in a cart. The girl runs home and gets help from several Neighbourhood|neighbors. They track the ruffians down to a cabin in the mountains where the gang has trapped their victim and set the cabin on fire. A thug and Rescuer fight on the roof of the house.

==Cast== Edward Dillon as Mike
* George Gebhardt as Viper
* Mack Sennett as Rescuer
* D.W. Griffith as Rescuer
* Anthony OSullivan as Gang Member

==See also==
* D. W. Griffith filmography
* List of American films of 1908

==References==
 

==External links==
*   at http://www.buscabiografias.com
*  
*  available for free download at  

 

 
 
 
 
 
 
 
 
 
 
 