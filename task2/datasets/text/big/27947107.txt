Adelheid (film)
{{Infobox film
| name           = Adelheid
| image          = Rudig-2010-07-23-KostelSvatehoJanaKrtitele.JPG
| alt            = 
| caption        = Town of Vroutek (German: Rudig), location of the Lužec castle from the film Adelheid
| director       = František Vláčil
| producer       = 
| writer         = Vladimír Körner František Vláčil
| starring       = 
| music          = 
| cinematography = František Uldrich
| editing        = Miroslav Hájek
| studio         = 
| distributor    = 
| released       = 1969
| runtime        = 99 minutes
| country        = Czechoslovakia
| language       = Czech
| budget         = 
| gross          = 
}}
Adelheid  is a 1969 Czechoslovak drama film directed by   ( ) shortly after World War II. 

==Plot== Western front Eastern Front. Adelheid works as a cleaning lady at her own mansion. Viktor makes her his captive maid, and soon falls in love with her. His heart is torn between feelings of desire and his national identity and sympathies. However, Adelheid silently waits for her brother to return, which he does.   

The soundtrack, adapted by Zdeněk Liška is based on existing music by Bach and Strauss, which complements the atmosphere of the film. The film was produced by Film Studio Barrandov Czechoslovakia in 1969.

==Cast==
* Petr Čepek – Viktor Chotovický
* Emma Černá – Adelheid Heidenmannová
* Jan Vostrčil – Sergeant Hejna
* Jana Krupičková – Girl
* Pavel Landovský – Guardsman Jindra
* Lubomír Tlalka – Guardsman Karel
* Miloš Willig – Staff captain
* Karel Hábl – Guardian
* Zdeněk Mátl – Hansgeorg Heidenmann
* Alžběta Frejková – Old German women
* Josef Němeček – Slovakian
* Karel Bělohradský – Guardsman
* Vlasta Petříková – Women

==References==
 

==External links==
*  

 
 
 
 
 
 
 