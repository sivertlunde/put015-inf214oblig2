Bangkok Knockout
{{Infobox film
| name           = Bangkok Knockout
| image          = Bangkok-Knockout.jpg
| caption        = 
| director       = Panna Rittikrai 
| producer       = 
| writer         = 
| screenplay     = Jonathon Siminoe
| story          = 
| based on       =  
| starring       = Sorapong Chatree
| music          = 
| cinematography = 
| editing        = 
| studio         = Na Film
| distributor    = Sahamongkol Film International
| released       =  
| runtime        = 105 minutes
| country        = Thailand
| language       = Thai English
| gross          = 
}}

Bangkok Knockout (Thai language|Thai: โคตรสู้ โคตรโส) is a 2010 Thai martial arts film. 

==Plot==
After winning a contest to star in a Hollywood film, a group of martial arts students celebrate by hosting a party. However, they all get drugged and past out while celebrating. When they wake up, they are attacked and soon some of their friends have been kidnapped. They quickly learn that a group of assassins are coming after them and that the contest may not have been what it seemed. The only way to survive is to fight their way out and rescue their friends.

==Cast==
*Speedy Arnold as Mr. Snead
*Pimchanok Luevisadpaibul as Bai-Fern
*Gitabak Agohjit as Git
*Supaksorn Chaimongkol as Joy
*Sorapong Chatree as Jaram
*Virat Kemgrad as Jao
*Chatchapol Kulsiriwoottichai as Pod
*Sarawoot Kumsorn as U-Go
*Krittiya Lardphanna as Kuk Yai
*Sumret Muengput as Ao
*Deka Partum as	Jame
*Panna Rittikrai as Suthep Sisai	
*Puchong Sarthorn as Eddo	
*Poonyapat Soonkunchanon as Lerm
*Tanavit Wongsuwan as Pom
*Vinai Weangyanogoong as Black Men

==Production==
Magnolia Pictures has gained the U.S. distributional rights. 

==Reception== title = NYAFF 2011: BKO: BANGKOK KNOCKOUT Review
|work= Twitch Film|date=|url= http://twitchfilm.com/reviews/2011/06/nyaff-2011-bko-bangkok-knockout-review.php|accessdate=2011-07-06}}    

==References==
 

==External links==
*   - US website
*  
 

 
 
 
 
 
 
 


 
 