My Song Goes Round the World
{{Infobox film
| name           = My Song Goes Round the World
| image          = 
| image_size     = 
| caption        = 
| director       = Richard Oswald
| producer       = Richard Oswald  
| writer         = Ernst Neubach   Clifford Grey   Frank Miller
| narrator       =  John Loder   Charlotte Ander   Jack Barty
| music          = Hans May
| cinematography = Reimar Kuntze
| editing        = Walter Stokvis
| studio         = British International Pictures
| distributor    = Associated British Picture Corporation
| released       = 1934
| runtime        = 70 minutes 
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}} British musical John Loder and Charlotte Ander.  It was an English-language version of the 1933 German film A Song Goes Round the World, also directed by Oswald.

==Cast==
* Joseph Schmidt - Ricardo  John Loder - Rico 
* Charlotte Ander - Nina 
* Jack Barty - Simoni 
* Jimmy Godden - Manager 
* Hal Gordon - Stage Manager

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 


 
 