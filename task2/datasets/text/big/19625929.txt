Falling Down
 
{{Infobox film
| name           = Falling Down
| image          = Falling Down (1993 film) poster.jpg
| alt            = A poster depicting an older man standing on a concrete platform, wearing a business outfit, holding a briefcase and a shotgun. Above in black letters it reads: "Michael Douglas". Below in large white letters over a red background it reads: "Falling Down". Beneath that with the film credits, it reads in small white letters: "A Joel Schumacher Film". In the background are skyscrapers and a smog filled sky.
| caption        = Theatrical release poster
| director       = Joel Schumacher
| producer       =  
| writer         = Ebbe Roe Smith
| starring       = Michael Douglas
| music          = James Newton Howard
| cinematography = Andrzej Bartkowiak Paul Hirsch
| studio         =  
| distributor    = Warner Bros.
| released       = February 26, 1993 (USA)
| runtime        = 113 minutes
| country        = United States France United Kingdom
| language       = English
| budget         = $25 million
| gross          = $40,903,593 
}} defense engineer. sardonic observations LAPD Sergeant on the day of his retirement, who faces his own frustrations, even as he tracks down Foster.
 mental collapse, is taken from the nursery rhyme "London Bridge Is Falling Down", which is a recurring motif throughout the film.

==Plot== laid off from his job. His frustration grows when his car air conditioning fails while he is stuck in traffic. He abandons his car and begins walking across Los Angeles to attend Adele’s birthday party.
 Korean owner Mr. Lee (Chan) refuses to give change for a telephone call. Foster begins ranting about the high prices. The owner grabs a baseball bat and demands Foster leave. Foster takes the bat and destroys much of the merchandise before leaving. Shortly thereafter, while resting on a hill, he is accosted by two gang members who threaten him with a knife and demand his briefcase. Foster attacks them with the bat and takes their knife.

The two gang members, now in a car with three friends, cruise the streets and find Foster in a phone booth. They open fire in a drive-by shooting, hitting several bystanders but not Foster. The driver loses control and crashes. Foster picks up a gun, shoots the only surviving gang member in the leg, then leaves with their bag of weapons.

At a fast food restaurant, Foster attempts to order breakfast, but they have switched to the lunch menu. After an argument with the manager, Foster pulls a gun and accidentally fires into the ceiling. After trying to reassure the frightened employees and customers, he orders lunch, but is annoyed when the burger looks nothing like the one shown on the menu board. He leaves, tries to call Beth from a phone booth, then shoots the booth to pieces after being hassled by someone who was waiting to use the phone.

Sergeant Martin Prendergast (Duvall), on duty on his last day before retirement, insists on investigating the crimes. Interviews with the witnesses at each scene lead Prendergast to realize that the same person may be responsible. Fosters “D-FENS” vanity license plate proves to be an important lead, because Prendergast remembers being in the same traffic jam as Foster earlier that day. Prendergast and his partner, Detective Torres (Ticotin), visit Fosters mother. They realize Foster is heading toward his former familys home in Venice, California and rush to intercept him.
 rocket launcher, and congratulates him for intimidating "a bunch of niggers" at the Whammy Burger. When Foster expresses disgust for the store owners racism, the man pulls a gun and attempts to handcuff him, but Foster stabs him with the gang members knife, then shoots him dead. He changes into army fatigues and boots, takes the rocket launcher, and leaves.

He encounters a road repair crew, who arent working, and accuses them of doing unnecessary repairs to justify their budget. He pulls out the rocket launcher, but struggles to use it, until a young boy explains how it works. Foster accidentally fires the launcher, blowing up the construction site.

By the time Foster reaches Beth’s house, she has already fled with Adele. He realizes that they may have gone to nearby Venice Pier, but Prendergast and Torres arrive before he can go after them. Foster shoots Torres, injuring her, and flees with Prendergast in pursuit.
 shoot him dead before he can see that Fosters gun is only a water pistol. The movie ends with a video film of Fosters past happier days with his family, which he was watching before the showdown, continuing to play on Beths VCR. 

==Cast==
*Michael Douglas as William "D-Fens" Foster
*Robert Duvall as Sergeant Martin Prendergast
*Barbara Hershey as Elizabeth "Beth" Trevino
*Rachel Ticotin as Detective Sandra Torres
*Tuesday Weld as Amanda Prendergast
*Frederic Forrest as Nick, Army surplus store owner
*Michael Paul Chan as Mr. Lee, Korean store owner Steve Park as Detective Brian
*Vondie Curtis-Hall as Not Economically Viable Man
*Lois Smith as Fosters mother
*Joey Hope Singer as Adele Foster-Trevino
*Raymond J. Barry as Captain Yardley
*D. W. Moffett as Detective Lydecker
*Dedee Pfeiffer as Sheila Folsom, Whammy Burger employee

==Box office and reception==
The film grossed $40.9 million against a $25 million budget. It was the number one weekend movie during its first two weeks of release (2/26-28, 3/5-7/93).

Reviews for the film were generally positive. Falling Down holds a 73% "Certified Fresh" rating on Rotten Tomatoes  and a score of 56 out of 100 ("mixed or average reviews") on Metacritic.  Vincent Canby of The New York Times called it "the most interesting, all-out commercial American film of the year to date, and one that will function much like a Rorschach test to expose the secrets of those who watch it."    Philip Thomas of Empire (magazine)|Empire magazine wrote in his review of the film, "While the morality of D-Fens methods are questionable, theres a resonance about his reaction to everyday annoyances, and Michael Douglas hypnotic performance makes it memorable."  Roger Ebert, who gave the film a positive review at the time of its release, stated of William "D-Fens" Foster:

 

Tasha Robinson of The A.V. Club has been critical of the film: "It’s seemingly meant as a sort of dark comedy about the petty annoyances of life, and how they can accumulate and become so maddening that over-the-top cathartic violence seems like the only satisfying option. But Douglas’ violent reaction to his surroundings, and the way the film treats virtually everyone around him as worthless, and presents his violence as the comedic payoff, turns it into a tone-deaf, self-pitying lament about the terrible persecution facing the oppressed majority in an era of political correctness and increasing multiculturalism. In its ugly, skewed world, almost everyone but this madman is dumb, incompetent, and offensive, and his only possible solution is to wipe a few of these losers off the face of the earth, then die. It’s a profoundly hateful film disguised alternately (and erratically) as either tragedy or humor." 

James Berardinelli wrote "Falling Down is replete with gallows humor, almost to the point where it could be classified as a black comedy."  John Truby calls the film "an anti-Odyssey story" about "the lie of the American dream".    He adds "I cant remember laughing so hard in a movie." 
 Wall Street, Douglas again takes on the symbolic mantle of the Zeitgeist. But in Falling Down, he and Schumacher want to have their cake and eat it too; they want him to be a hero and a villain, and it just wont work." 

Peter Travers of Rolling Stone gave the film four stars out of five, writing "Theres no denying the power of the tale or of Douglass riveting performance - his best and riskiest since Wall Street. Douglas neither demonizes nor canonizes this flawed character. Marching across a violent urban landscape toward an illusory home, this shattered Everyman is never less than real..."Im the bad guy?" he asks in disbelief. Douglas speaks the line with a searing poignancy that illuminates uncomfortable truths without excusing the character. Schumacher could have exploited those tabloid headlines about solid citizens going berserk. Instead, the timely, gripping Falling Down puts a human face on a cold statistic and then dares us to look away." 

At the time of its release Douglas father, actor Kirk Douglas, declared "He played it brilliantly. I think it is his best piece of work to date."    He also defended the film against critics who claimed that it glorifies lawbreaking: "Michaels character is not the hero or newest urban icon. He is the villain and the victim. Of course, we see many elements of our society that contributed to his madness. We even pity him. But the movie never condones his actions." 

===Controversy=== Korean grocer.    Warner Brothers Korea canceled the release of Falling Down in South Korea following boycott threats.  Unemployed defense workers were also angered at their portrayal in the film.  Falling Down has been described as a definitive exploration of the notion of the "angry white male"; the character of D-FENS was featured on magazine covers, including Time (magazine)|Time magazine, and reported upon as an embodiment of the stereotype. 

===In popular culture===
The Simpsons would later use Douglass character as the model for a one-time character, Frank Grimes, in the episode "Homers Enemy."   

Iron Maidens 1995 single Man on the Edge is based on the film, including a chorus consisting of simply the words, "falling down".

The Foo Fighters based their music video for the song "Walk (Foo Fighters song)|Walk" on the plot of Falling Down. 

The French singer Disiz la peste based his song "Jpète les plomb" and the associated music video  on the plot of Falling Down. 

In 2015 the film was adapted for theatre by Danish director Anders Lundorph. The show opened in February 2015 at The Danish Royal Theatre in Copenhagen starring Nicolai Dahl Hamilton as William Foster.  

==Awards and nominations==
*1993 Cannes Film Festival, Nominated for the Palme dOr (Joel Schumacher)   
*1994 Edgar Award, Won for Best Motion Picture Screenplay (Ebbe Roe Smith)

==References==
 

==Further reading==

Jon Frauley. 2010. "Moral Transcendence and Symbolic Interaction in Falling Down." Criminology, Deviance, and the Silver Screen: The Fictional Reality and the Criminological Imagination. New York: Palgrave Macmillan.

==External links==
 
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 