Lyiza
 
 
{{Infobox film
| name           = Lyiza
| image          = 
| caption        = 
| director       = Marie-Clementine Dusabejambo
| producer       = 
| writer         = 
| starring       = Barbara Umutesi, Rodrigue Cyuzuzo
| distributor    = 
| released       =  
| runtime        = 21 minutes
| country        = Rwanda
| language       = 
| budget         = 
| gross          = 
| screenplay     = Marie-Clementine Dusabejambo
| cinematography = 
| editing        = Richard Mugwaneza
| music          = Babrah Dusange, Betty Murungi, Trevor Gureckis, Bryan Senti, Jay Wadley
}}

Lyiza is a 2011 Rwandan short film directed by Marie-Clementine Dusabejambo.  

== Synopsis == genocide in Rwanda. When she recognizes in the father of her classmate, Rwena, the person responsible for their murder, she says so publicly, creating great tension. But harmony returns through the intervention of a teacher who takes the youngsters to the museum of the genocide, the place of memory, and guides Lyiza towards forgiveness. Without being didactic and with an original narrative style, the film underlines the importance of sharing experiences and educating for truth and reconciliation. 

== References ==
 

 
 


 