Mayfly (film)
{{Infobox film
| name           = Mayfly
| image          = 
| caption        =
| director       = Michael Bodie
| producer       = Loretta Ramos
| writer         = Michael Bodie
| starring       = Charlotte Stewart Lucy Owen
| music          = Chanda Dancy
| cinematography = Lisa Wiegand
| editing        =  Michael Bodie
| studio         =
| distributor    =
| released       =  
| runtime        = 17 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Mayfly is a short film. It was written and directed by Michael Bodie, and deals with the difficult subject matter of assisted suicide and dying.  Bodie graduated as a writer / director, from UC Davis with a bachelors in dramatic arts. Mayfly was his fifth short film.

==Cast==
*Charlotte Stewart  as Barbara
*Andrew Masset  as Richard
*Lucy Owen  as Sams Daughter
*Judith Montgomery  as Samantha
*Tim Hodgin  as Carl

<!--
==References==
 
-->

==External links==
*  
*   at Vimeo.com
*  

 
 
 
 
 

 