Rime of the Ancient Mariner (film)
 
{{Infobox Film
| name           = Rime of the Ancient Mariner
| image          = Rime of the Ancient Mariner.jpg
| image_size     = 
| caption        = Illustration by Willy Pogany from his 1910 limited edition, modified for the film by illustrator-animator Ray daSilva, the directors brother.
| director       =  
| producer       = Raúl daSilva
| writer         = Samuel Taylor Coleridge (epic poem)   Donald Moffit (Coleridge biography section)
| narrator       = Sir Michael Redgrave Michael Johnson Miriam Margolyes George Murcell
| music          = Francis Lee Fred Murphy Ray daSilva (concept art, illustration, animation)
| editing        = Maxwell Seligman
| distributor    = Kultur International Films
| released       = 11 June 1978 (first TV broadcast)  24 April 2007 (DVD release)
| runtime        = 53 minutes
| country        = United Kingdom United States
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}
 poem of the same name, featuring a direct reading given by renowned British actor Sir Michael Redgrave.  DaSilvas film has won multiple minor awards and recognitions and, according to DaSilva, has been said to effectively render the otherwise-difficult poem comprehensible by even the youngest of readers.

==Planning==
Raúl daSilvas film introduces additional sensory perception elements to Coleridges Rime of the Ancient Mariner by taking the poem, which was written in a faux-archaic (purportedly Elizabethan) form of English and visualizing it.  This was accomplished with the help of Sir Michael Redgrave (whod taught the epic poem as a young schoolmaster before becoming an actor) and with the additional use of sound and music to enhance, support, and illustrate the poems dramatic and often difficult language.

The Wikipedia article on photoanimation explains the technique used for this film.  It is a time consuming, 
arduous procedure that includes the analysis and breakdown of the entire sound track and the planning of frame to frame exposure of all visual elements.  Using the Oxberry master series animation stand the process took six months.  The film took two years of production to complete.

==International Film Festival Recognition==
The film was entered into six international film festivals and won six prizes, five of which were in the first-place category.

*  
* Gold Medal(first place) International Film & TV Festival of New York
* Gold Venus (first place) Virgin Islands Film Festival
* CINDY Statuette (first place)   (Information Film Producers Association), Hollywood, California
* CHRIS Statuette (first place) Columbus International Film Festival
* Certificate of Merit (general recognition of creativity) Chicago International Film Festival

==Application==
Raúl daSilva used the film in his classes as a teaching aid. Not only did students enjoy it, but their overall comprehension rose from fifty to one-hundred percent. They found it easier to understand the poem’s timeless message, many going on to reread the poem with a much greater degree of understanding. In his efforts to study students’ reactions he also showed it to a number of student groups—some as young as third Grade—where it was equally enjoyed and understood. In the ensuing years Raúl received unanimous critical acclaim for his production, with letters coming from as far as Australia offering expressions of gratitude from English teachers. From that point it began to accumulate interest from the general public. 

==Television broadcast history== ABC affiliate program for a prime-time slot opposite CBS’s popular news program, 60 Minutes. The broadcast title for the film was The Strangest Voyage and included a short biography on the poems author, Samuel Taylor Coleridge. The fact that local newspapers ran front page stories on the broadcast might have helped as it allegedly completely eclipsed 60 Minutes in viewer response. According to daSilva, the then program manager reported that by the end of the broadcast the telephone switchboard was flooded with calls from viewers expressing praise and gratitude for the broadcast.  Similar reports came from the program’s sponsor, Columbia Banking, and according to their senior vice president marketing, hundreds of letters were received after the broadcast, many from people who said they planned on opening accounts at the bank. 

==Current history==
The film has been distributed on VHS since 1985 and starting in April 2007 on DVD. The film was released to a responsive commercial market despite the fact that it has never been promoted or advertised. 

A review in the April 1986 edition of Library Journal called it "a must for schools, and an excellent choice for public libraries." 

daSilva plans to have the film circulated worldwide where it has thus far not been released, believing Samuel Taylor Coleridge’s intrinsic message about the sanctity of all life on Earth is just as important today as it has ever been.

==DVD release==
* 

==References==
 

==External links==
*  

 
 
 
 
 
 