Agent Hamilton: But Not If It Concerns Your Daughter
{{Infobox film
| name           = Agent Hamilton: But Not If It Concerns Your Daughter
| image          =
| caption        =
| image_size     =
| director       = Tobias Falk {{Cite web|url= http://www.97filmproduction.com/en/fiche-film.php?id=236|title= Hamilton 2: But Not If It Concerns Your Daughter
|accessdate=2013-07-02}} 
| producer       = Jan Marnell
| writer         = Stefan Thunberg
| based on       =  
| starring       =Mikael Persbrandt Frida Hallgren Steven Waddington John Light
| music          = Philippe Boix-Vives Jon Ekstrand
| cinematography = Jan Jonaeus
| editing        = Thomas Lagerman SF International Sales 
| released       =   
| runtime        = 90 min
| country        = Sweden
| language       = Swedish English
| budget         =
| gross          =
}}

Agent Hamilton: But Not If It Concerns Your Daughter is a 2012 spy film  directed by Tobias Falk. {{cite web|url=http://www.thefilmcatalogue.com/catalog/FilmDetail.php?id=13703
|title=Agent Hamilton: But not if it Concerns Your Daughter|publisher=thefilmcatalogue.com|accessdate=2013-07-05}}  It is also known as Hamilton 2: Unless Its About Your Daughter. {{cite web|url=http://www.cine-adicto.com/en/movie/147655/Hamilton+2%3A+Unless+Its+About+Your+Daughter-2012
|title=Hamilton 2: Unless Its About Your Daughter|publisher=cine-adicto.com |accessdate=2013-07-05}}   {{cite web|url=http://www.airingnews.com/movies/512/Hamilton-2-Unless-Its-About-Your-Daughter
|title=Hamilton 2: Unless Its About Your Daughter|publisher=aringnews.com|accessdate=2013-07-05}}  The original title is Hamilton 2: Men Inte Om Det Gäller Din Dotter.  {{cite web|url=http://jamesbond007.se/events.asp?id=3105
|title=Hamilton 2: Men Inte Om Det Gäller Din Dotter|publisher=jamesbond007.se|accessdate=2013-07-05}}  {{cite web|url=http://www.tcm.com/tcmdb/title/973578/Hamilton-2-Men-Inte-Om-Det-G-ller-Din-Dotter/
|title=Hamilton 2: Men Inte om det Gäller din Dotter|publisher=Turner Classic Movies|accessdate=2013-07-05}}  It is the second part of a trilogy.  {{Cite web
|url= http://www.cinando.com/DefaultController.aspx?PageId=FicheFilm&IdC=913&IdF=116421|title= AGENT HAMILTON consists of three full-length features|publisher=cinando.com|accessdate=2013-07-06}}  {{cite web|url=http://www.sfinternational.se/film/on-her-majestys-service
|title=Agent Hamilton - On Her Majestys Service|publisher=sfinternational.se|accessdate=2013-07-05}} 
Mikael Persbrandt and Saba Mubarak reprise their roles as agent Carl Hamilton and agent Mouna al Fathar. The Australian Classification Board classified the film as "M", the highest unrestricted rating. 

==Plot== SAS background. His investigation leads him to the United Kingdom where he tries to retrieve information from one of their old SAS comrades who is evidently unwilling to fill in an outsider. He attempts to put Hamilton down and dies trying. Hamilton is then arrested and delivered to the SIS Building where he faces being interrogated by high-ranked MI-6 officials. After his release he learns he mustnt even rely on his own countrys secret service when he wants to rescue his goddaughter. As soon as he knows where Nathalie is kept, he asks Mouna al Fathar to help him. She demands that the leader of these Islamists will also be deducted because the PLO wants him brought before an international court. Together with Nathalies father, a former member of the French Foreign Legion, they raid the terrorists garrison. But Pierre Tanguy is out of shape and the terrorist leader is of all things secretly supported by an US-American who plays all sides. Hamilton gets caught and becomes subject to enhanced interrogation techniques.

==Cast==
*Mikael Persbrandt as Carl Hamilton
*Saba Mubarak as Mouna al Fathar
*Frida Hallgren as Eva Tanguy
*Reuben Sallmander as Pierre Tanguy
*Lennart Hjulström as DG
*Peter Eggers as Patrik Wärnstrand
*Steven Waddington as McCullen John Light as Jason Fox
*Sven Ahlström as Cedervall
*Said Legue as Suleiman Al-Obeid
*Maria Langhammer as Helene Boström

==Reception== franchise and had made him "keen to see the third film". {{Cite web|url= http://www.michaeldvd.com.au/Reviews/Reviews.asp?ID=10472
|title= Hamilton: But Not if it Concerns Your Daughter (2012) |author=Bruce, Daniel
|publisher=michaeldvd.com.au|date=2013-06-20|accessdate=2013-07-05}} 

==DVD Release==
The Australian DVD contains only one version. It is the original version which is partly in English and otherwise subtitled. 

==References==
 

==External links==
* 
* 
* 
*  

 
 
 
 
 
 
 