Le Mouton noir
{{Infobox film
| name           = Le Mouton noir
| image          =
| image_size     =
| caption        =
| director       = Jacques Godbout
| producer       =
| writer         =
| narrator       =
| starring       = Jacques Godbout
| music          =
| cinematography =
| editing        =
| distributor    =
| released       = 1992
| runtime        =
| country        = Canada French
| budget         =
| preceded_by    =
| followed_by    =
}} documentary film produced in 1992 by the National Film Board of Canada (NFB).  Jacques Godbout directed and starred in the film. Its style belongs to the Quebec cinéma vérité|cinéma direct school of filmmaking.

==Synopsis==

It chronicles the immediate   of the fall, in 1990, of the Meech Lake Accord, and its effects on the Quebec society and Quebec nationalism. It is set during the Commission on the Political and Constitutional Future of Quebec|Bélanger-Campeau commission (a public hearing to determine the way to choose for Quebec regarding its autonomy), before the Parti Libéral du Québec (in power, traditionally in favor of autonomy within Canada and co-instigator of the Accord with the Progressive Conservative Party of Canada) formally closed the door to independence in the light of the death of Meech Lake and the departure of some Liberal nationalists to create the Action démocratique du Québec|ADQ. The title means The Black Sheep, referring to Quebec and its difference coupled with a perceived ostracism by Canada, notably through its rejection of the Accord that would have recognized this difference.

==Protagonists==
 Liberal Party 1995 referendum on independence).
 Quebec public political scientist Premier Robert famous speech 1980 referendum. second referendum on independence. He is himself the director of Le confort et lindifférence, a similar analytic film about the 1980 plebiscite, and is brought into Le Mouton Noir especially for that reason.

==Production== narrative thread by setting up scenes where he researches his subject and interacts with others, to help push the documentary forward. Godbout provides his own voice for the voice-overs of the English version.

==Sequel==
In 2003, a sequel was brought to theaters called Les héritiers du mouton noir. It catches up with the five young politicians the first part centered on.

==See also==
*List of Quebec movies
*List of Quebec film directors
*Cinema of Quebec
*Culture of Quebec
*History of Quebec
*Politics of Quebec

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 