Mountain Strawberries 3
{{Infobox film
| name           = Mountain Strawberries 3
| image          = Mountain Strawberries 3.jpg
| caption        = Theatrical poster for Mountain Strawberries 3 (1987)
| film name      = {{Film name
 | hangul         =   3
 | hanja          =  딸기 3
 | rr              = Sanddalgi 3
 | mr             = Santtalgi 3}}
| director       = Kim Su-hyeong 
| producer       = Seo Hyeon
| writer         = Yoo Ji-hoeng
| starring       = Seonu Il-ran
| music          = Jeong Min-seob
| cinematography = Park Seung-duck
| editing        = Ree Kyoung-ja
| distributor    = Nam A Enterprises Co., Ltd.
| released       =  
| runtime        = 93 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          = 
}}
Mountain Strawberries 3 (hangul|산딸기 3 - Sanddalgi 3), also known as Wild Strawberries 3, is a 1987 South Korean film directed by Kim Su-hyeong. It was the third entry in the Mountain Strawberries series.

==Synopsis==
A young woman in a village is involved in romantic entanglements during the preparation of a festival celebration.   

==Cast==
* Seonu Il-ran 
* Ma Hung-sik
* Kim Kuk-hyeon
* Jang Mi-yeong
* Moon Tai-sun
* Park Yong-pal
* Chung Kyoo-young
* Han Song-i
* Park Jong-sel
* Im Jae-min

==Bibliography==

===English===
*  

===Korean===
*  
*  

==Notes==
 

 
 
 
 


 
 