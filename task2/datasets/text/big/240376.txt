Only When I Laugh (film)
 
 
{{Infobox film
| name           = Only When I Laugh
| image          = Only when i laugh.jpg
| caption        = Only When I Laugh theatrical poster
| director       = Glenn Jordan
| producer       = Neil Simon Roger M. Rothstein
| writer         = Neil Simon (based on his play The Gingerbread Lady)
| starring       = Marsha Mason Kristy McNichol James Coco Joan Hackett David Dukes
| music          = David Shire
| cinematography = David M. Walsh John Wright
| studio         = Rastar
| distributor    = Columbia Pictures
| released       =  
| runtime        = 120 minutes
| country        = United States
| awards         = Golden Globes (Supporting actress for Joan Hackett)
| language       = English
| budget         =
| gross          = $25,524,778
}}
Only When I Laugh is a 1981 film based on Neil Simons play The Gingerbread Lady.

The story is about an alcoholic Broadway actress who tries to stay sober while dealing with the problems of her teenaged daughter and her friends: an overly vain woman who fears the loss of her looks and a gay actor relegated to small roles in third-rate shows.  Simon changed the main characters name to Georgia Hines for the film adaptation; the character was named Evy Meara in the stage version.  The main character went from being a cabaret singer to a Broadway stage actress.
 I Ought to Be in Pictures, was released just six months later, and its plot was similar.
 Best Actor Best Actress Best Actress in a Supporting Role (Joan Hackett). It was the last film Hackett completed before her death. Only When I Laugh proved to be very successful at the box office.

Coco was also nominated for Worst Supporting Actor in Golden Raspberry Awards for the same role.

==Plot==
Actress Georgia Hines is being released from a rehab center where she has been undergoing treatment for alcoholism and weight gain. She returns to her Manhattan apartment to begin a new sober life with her supportive friends:  Jimmy, an unemployed actor and Toby, Georgias very polished married high society best friend.  She pledges to them both that she will maintain her sobriety, take care of herself and slowly ease back into theatre work.

Not back for more than a week, Georgias enthusiastic teenaged daughter Polly, who has been living with Georgias ex-husband and his new wife, asks if she could move in.  Georgia, although not completely convinced she is ready, agrees to Pollys request.  Later that week she and Jimmy repaint the apartment to "remove the martini stains from the wall" and make a symbolic gesture towards her new life with her new daughter.  Just as Polly arrives in a taxi, Georgia accepts a telephone call from her unpopular ex lover and unsuccessful writer David Lowe, who asks if they could meet.  With a renewed sense of confidence she strongly refuses David, hangs up and rushes to greet Polly.

That evening Georgia and Polly swap stories about their lives and the affects Georgias alcoholism had on Polly.  Polly is able to share her sadness at not being allowed to grow up with her mother and Georgia concedes that she feels she can finally provide for Polly.  However, as Polly sleeps in the next bedroom over, Georgia hesitantly rings David Lowe to apologise for her curt conversation.  David again asks to see Georgia and she finally agrees to meet him at their old theatre district hangout.

A trim and fit Georgia, wearing borrowed clothes from Polly, meets David Lowe for dinner.  As the meal comes to an end David presents Georgia with a script he has written about their turbulent alcohol-filled relationship.  Its his first script in two years and he says hes found a theatre to stage it and now wants Georgia to play the lead, which means Georgia will be playing herself.  Furious David brought her there for business, Georgia makes a scene and rebuffs Davids request.  As she is about to dramatically exit the restaurant David calmly asks her to reconsider.  Unable to resist his charm, Georgia laughs and takes the script home.

The reunion between Georgia and Polly is turning out to be a success. The two are seen shopping together, laughing, even flirting with college-aged boys who clumsily mistake Georgia and Polly for sisters.  After a day of mother-daughter bonding they share their adventures with a delighted Jimmy who while cooking his favourite Italian meal asks to see the dresses they bought but not before Georgia and Polly surprise him with a musical number they are working on for Tobys upcoming party. The apartment is filled with music and laughter as Georgia plays the piano and she and Polly sing for Jimmy.  In the middle of their performance the phone rings.  It is David Lowe and Georgia abruptly stops singing with Polly and rushes to take Davids call in the bedroom leaving Jimmy and Polly to sit in silence on the piano bench as Georgia is heard laughing in the background.

Georgia begins work on the play.  David joyfully watches from the side as Georgia shines in rehearsals. In one particular tense scene Georgia confuses art with life, loses her composure and must stop.  David joins an alone seated Georgia on the stage and consoles her.  She tearfully tells him that she doesnt think she can go forward with the play because it is too painful.  David, agreeing to whatever Georgia decides, takes her hand and begins to praise her performance and tells her that she is the only one who can do this part and that when he watches her he is mesmerized by her every word.  Georgia is again calmed and charmed by David and David tenderly kisses her on the cheek as he exits the stage with purpose leaving Georgia alone and watching him walk away.

Toby is impatiently sitting at a restaurant.  Georgia arrives happily late from rehearsals to find Toby, in a foul mood, asking her about her relationship with David.  Georgia assures her that there is nothing going on between her and David.  She continues to talk about the play when Toby reveals that she believes her marriage might be in trouble.  Before she can go further, Jimmy bounces in with great news that he finally has been given a part in a play and demands that they celebrate.  Jimmy is beaming and Georgia congratulates him while Toby looks on in silence.

After a time, Jimmy realises something is not right with Toby and the gang turn their attention to their friend.  Toby negatively responds to their latent concerns, invites them to her birthday party and with a few rude words demands that they get on with their lunch and order.  Tobys uncharacteristic use of foul language causes Georgia and Jimmy to hid their faces in their menus and laugh quietly then more loudly which causes Toby to realise her own behaviour and she joins in the laughter and the three friends enjoy a sincere moment.

Georgia is again seen at rehearsals.  She attentively greets people while actively seeking out David in the theatre.  David comes out from the darkness and Georgia presents him with a gift wrapped present.  She explains how she wanted to give it to him for all his hard work and friendship and David is as much taken aback as Georgia is delighted.  He asks to wait a moment and while Georgia waits smiling, David returns and introduces Georgia to his new girlfriend.  Georgia awkwardly holds her smile as she comes to understand that Davids affections towards her were only about the play.  The new couple leave and a lonely Georgia heads off to her dressing room but is stopped by a phone call from an upset Jimmy who informs her that Toby has called off her party because Tobys husband has just asked for a divorce.  Devastated for their friend, they agree to meet at Tobys that evening.

Georgia heads to her dressing room again and this time she ignores the people around her.  In the meantime, David, understanding that Georgia has figured out the shallowness of their relationship, finds an upset Georgia and begins to explain about his new girlfriend.  Hurt by David and upset for Toby, Georgia lashes out at David for being insensitive and self-absorbed to think that her emotions have anything to do with him.  She admonishes his thoughtlessness and kicks a deflated David out of her dressing room.

Unable to find a cab, Georgia is forced to walk in the cold and misting rain to Tobys high rise apartment.  She is greeted at the door by Toby, who is dressed perfectly for her cancelled party.  The two old friends sit together as Georgia fills Tobys glass with champagne and Toby tells Georgia the story of her past as an enviable college beauty, an eye-candy untalented actress in California and an impeccable wife to her high profile New York husband.  Her speech becomes faster and her voice more elevated as she begins to lose her composure over the fact that her husband has turned to her after twelve years of marriage to tell her he is no longer interested in her and wants a divorce.  About to crumble, the doorbell rings and Toby excuses herself to the bedroom to retouch her makeup.  At the door is a shaking Jimmy who immediately asks Georgia to fill him a glass of champagne.  He quickly gulps down two glasses and reveals that he was just fired from his play, three nights before the opening and after he invited all his family and friends.

Toby returns to the room and is told about Jimmy and the three friends share another warm bonding moment of sadness.  Making the excuse to get another bottle of champagne, Georgia retreats to the kitchen and proceeds to drink three full glasses of champagne in succession.  She returns to the room tipsy.  Now drinking openly with her unaware friends, Georgia tries to rally her friends out of their depression with over the top reactions to their achievements in life.  At some point Georgia agrees with Jimmy about how wonderful he was in a play she never saw when Jimmy and Toby see that Georgia is drinking and drunk and the attention is turned from Tobys failed marriage and Jimmys failed career to Georgias excuse for drinking because of her inability to handle their failures as the sober one.

Polly, unaware that they party has been cancelled, turns up at Tobys with her new boyfriend.  The three friends form a plan to conceal their problems from an unsuspecting Polly.  Toby and Jimmy greet Polly normally however Georgia, now very drunk, has another over the top reaction to seeing Polly.  Polly quickly realises that her mother has relapsed and Toby and Jimmy are back to covering up for her bad behavior.  She takes Georgia into Tobys bedroom and assertively scolds Georgia for her insensitive, selfish and uncaring attitude towards everyone around her.  She storms out with her boyfriend and Jimmy takes Georgia home.

Jimmy stays with Georgia watching an old movie and is prepared to wait up until Polly returns when Georgia insists he head out, that shell sleep on the sofa and Polly will be fine.  After Jimmy leaves, Georgia discovers she is out of cigarettes and goes to some random bar to buy a pack.  A stranger sitting at the bar draws her attention and they strike up a flirty conversation as Georgia orders another drink.  Across town, Polly has decided that she wants to understand why her mother drinks so she asks her new boyfriend to buy her some alcohol and watch her drink and tell her what he sees.  As Polly is getting more drunk, Georgia abruptly ends her conversation with the stranger at the bar and leaves only to find that the stranger has followed her outside.  She refuses his request to stay awhile and he violently grabs her and pulls her into a darkened alley.

A battered and bleeding Georgia goes to Tobys, who is horrified by Georgias state and immediately wants to ring a doctor.  Georgia, afraid of bad press, begs her friend not to call anyone.  Toby agrees.

Outside on the terrace, they talk as Toby tends to Georgias wounded face and Georgia drinks.  Toby asks Georgia why she isnt a stronger person considering all her talent that Toby would have loved to have had, and reminds Georgia that though she isnt as talented at least she isnt self-destructive.  This logic sets Georgia off as she mockingly demands Toby tell her when did Georgia ever have a relationship with anyone who wasnt as weak and as helpless as she is.

Furious with Georgias assertion, Toby reminds Georgia that they are friends not out of some kind of weakness but out of a mutual love and respect and awe for each other.  She continues that shes had it covering for Georgia from here on out and offers Georgia two solutions:  Either Georgia makes use of Tobys high rise terrace balcony to quickly finish her life or Georgia do everyone a favour and stop being such an "astronomical pain in the ass."

A sobering Georgia stares at her defiant friend and muses:  "I never said it was multiple choice question."  The two old friends share a tender laugh and hug and they walk back inside.

==Cast==
Starring
*Marsha Mason – Georgia Hines
*Kristy McNichol – Polly Hines
*James Coco – Jimmy Perrino
*Joan Hackett – Toby Landau
*David Dukes – David Lowe

and
*John Bennett Perry – Vincent Heller (the actor portraying Lou in the play) Guy Boyd – Man in Bar
*Ed Moore - Dr. Bob Komack
*Peter Coffield – Mr. Tarloff
*Mark Schubb - Adam Kasabian
*Venida Evans – Nurse Garcia
*John Vargas – Manuel
*Dan Monahan – Jason
*Jane Atkins – Doreen
*Kevin Bacon – Don Holcroft
*Phillip Lindsay – Super

==Awards and nominations==
;1982 Academy Awards
*Nominated : Best Actress in a Leading Role Marsha Mason
*Nominated : Best Actor in a Supporting Role James Coco
*Nominated : Best Actress in a Supporting Role Joan Hackett

;1982 Golden Globes
*Won : Best Motion Picture Actress in a Supporting Role Joan Hackett
*Nominated : Best Motion Picture Actor in a Supporting Role James Coco
*Nominated : Best Motion Picture Actress in a Supporting Role Kristy McNichol

;1982 Golden Raspberry Awards
*Nominated : Worst Supporting Actor James Coco
*Nominated : Worst Original Song – "Only When I Laugh"

;1982 Young Artist Awards
*Won : Best Young Motion Picture Actress Kristy McNichol

==Home media==
The film is available for download through Apple Inc.|Apples iTunes Store and Amazon Instant Video.   , it has only been physically released on VHS and not DVD or Blu-ray.

==References==
 

==External links==
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 