Shadow on the Wall (film)
{{Infobox film
| name           = Shadow on the Wall
| image          = Shadow on the wall poster.jpg
| alt            =
| caption        = Theatrical release poster
| director       = Pat Jackson
| producer       = Robert Sisk
| screenplay     = William Ludwig
| based on       =   Nancy Davis
| music          = André Previn
| cinematography = Ray June
| editing        = Cotton Warburton
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 84 minutes
| country        = United States
| language       = English
| budget         =$701,000  . 
| gross          = $769,000 
}} Nancy Davis. It is based on the novel Death in the Dolls House by Lawrence P. Bachmann and Hannah Lees. 

==Plot== Nancy Davis) is convinced she can cure the young girl and begins to suspect that another person is guilty.

==Cast==
* Ann Sothern as Dell Faring
* Zachary Scott as David I. Starrling
* Gigi Perreau as Susan Starrling Nancy Davis as Dr. Caroline Cranford
* Kristine Miller as Celia Starrling
* John McIntire as Pike Ludwell
* Tom Helmore as Crane Weymouth
* Helen Brown as Miss Burke
* Barbara Billingsley as Olga
* Marcia Van Dyke as Secretary
* Anthony Sydes as Bobby
* Jimmy Hunt as Boy

==Reception==
According to MGM records the film earned $433,000 in the US and Canada and $192,000 elsewhere, resulting in a loss of $330,000. 

===Critical response===
When first released The New York Times praised the acting, writing, "Nancy Davis is beautiful and convincing as the serious psychiatrist who uses affection and play therapy to delve into the youngsters mind for the evidence needed for both a cure and the eventual exposure of the criminal. Gigi Perreau is excellent as the mentally tortured moppet, and Zachary Scott does a realistic job as her architect father and wrongly convicted murderer. Kristine Miller is competent in the brief role of the victim, but Ann Sothern, who turns in a polished portrayal, seems out of character as the worried villainess of the piece. List Shadow on the Wall as obvious but interesting fare." 

Film critic Dennis Schwartz liked the film and wrote, "A taut suspense yarn in B&W, that plays like film noir...This villain role is out of character for the always sweet Ann Sothern, but she shows great agility in handling the difficult role. The melodramatic script was often not believable and the action part of the story looked like pretend acting, just like the therapy Nancy Davis was applying to Gigi. But the stars pulled this one together and made the tense story, revolving around the little girl, seem plausible. Gigi Perreau was marvelous, giving a convincing performance as a little girl who could be both adorable and then almost frightened out of her mind." 

==References==
 

==External links==
*  
*  
*  
*  
*   at Classic Film Guide
*  

 
 
 
 
 
 
 
 
 
 