Virumaandi
{{Infobox film
| name      = Virumaandi
| image     = Virumaandi.jpg
| director  = Kamal Haasan
| writer    = Kamal Haasan Abhirami Nassar(actor)|Nasser Pasupathy Napoleon
| producer  = Kamal Haasan Chandra Haasan
| released  = 14 January 2004
| runtime   = 165 minutes
| music = Ilayaraaja
| cinematography = Keshav Prakash
| editing = Ramsudharsan
| studio = Raaj Kamal Films International
| distributor = Raaj Kamal Films International
| country = India
| language = Tamil
| budget =  14 crores
| box office =  34 crores
}}

Virumaandi is a 2004 Indian Tamil film written, directed, and starring Kamal Haasan in the title role. The film revolves around the interview of two prison inmates, firstly Kothala Thevar (Pasupathy) life sentence inmate and secondly Virumaandi (Kamal Haasan) who is condemned to hang. The criminals express how they feel about the direction their lives have taken and how they have ended up where they are. The films narrative is based on the Rashomon effect. The film revolves around the controversy of the death penalty.
 Nassar in Telugu as Pothuraju. The film won critical acclaim and was a commercial success at the box office.

==Plot==
The film starts with Angela Kathamuthu (Rohini (actress)|Rohini) and her cameraman in Central Jail interviewing prisoners serving life imprisonment and awaiting the death sentence. She meets Kothala Thevar (Pasupathy) who is serving a life sentence. He tells his version of the story that led to his conviction. According to him the root cause of the problem is Virumaandi (Kamal Haasan) - the happy go lucky rogue. His support to Kothala Thevar in his clash against Nallama Naicker (Napoleon (actor)|Napoleon) brings about a bonding between the two. Annalakshmi (Abhirami), the niece of Thevar falls for Virumaandi. Thevar with an eye on the fertile land owned by Virumaandi does not object to the romance. According to Thevar, Annalakshmi was abducted by Virumaandi and raped. A clash between Virumaandi and Thevar takes places in which 24 innocent people are killed.

Now its Virumaandis turn to tell his version of the story. He tells of how he fell in love with Annalakshmi and of her influence on him. She taught him to apologize and that forgiveness is a virtue. A clash at the Panchayata infuriates Annalakshmi and she asks Virumaandi to apologize to the elders whom he insulted. He goes to apologize but Thevar and his men, thinking he has gone to take revenge, come in groups and hack down innocents to death. Thevar uses his clout to get out of the murder charge but Virumaandi who was involved in the bloodbath is disturbed because he had to lie to save Thevar and his kin. As part of the penance he wills his land for the villagers. Annalakshmi asks him to marry her and leave the village. He marries her at the village temple and goes away in the night with the girl and stays with a relative. Thevars men come and abduct Annalakshmi and forcibly get her married to Thevars nephew. This is to ensure that Annalakshmis property does not go to Virumaandi. Annalakshmi commits suicide. Virumaandi takes revenge by hacking to death Thevars family. Thevar escapes. Nallama Naicker gives refuge to Virumaandi and sends him off to Chennai. Thevar and his men ask Nallama Naicker to hand over Virumaandi. A battle ensues in which Nallama Naicker is killed. The case goes to court. Virumaandi turns up in court. He finds all evidence is against him. He is convicted of having raped Annalakshmi and killing her and later on killing 26 men of Thevar who inquired into the rape.

Angela Kathamuthus cameraman discreetly shoots the deputy jailor conspiring with wardens and convicts to bump off the chief jailor (Nassar). A revolt among the jail staff leads to a jail break, the jailor being stabbed and Virumaandi saving the vital evidence and Angela from the deputy jailor and his henchmen. The film ends with a note that the death sentence should be abolished.

==Cast==
*Kamal Haasan as Virumaandi Abhirami as Annalakshmi
*Pasupathy as Kothala Thevar Napoleon as Nallama Naicker Rohini as Angela Kaathamuthu
*Shanmugarajan as Paykkaman
*Nassar as Chief Jailor Jayanth
*Pyramid Natarajan as Vaidyanathan
*S. N. Lakshmi as Virumaandis grandma
*Bala Singh as Thevar Thennavan as Kondarasu Bagla as Oomayan
*Ganthimathi
*O.A.K. Sundar
*Periya Karuppu Thevar
*G. Gnanasambandam as Jallikattu commentator Rajesh
*Karate Raja
*Kadhal Sukumar
*Sujatha Sivakumar

==Production==
When Kamal Haasan commenced the shooting of his latest mega film Virumandi in Madurai in April, 2003, the crowd swelled to over a lakh of people, a number unheard of at a shooting spot in the annals of the Tamil film industry. The movie generated so much interest that it became the talk of the town and the industry.

The inaugural function itself indicated that the film will best the trouble-makers and also prove wrong the Doubting Thomases. Attempts by some vested interests to halt the shooting made Kamal Haasan more determined than ever to complete it. The controversy surrounding the shooting of the film itself was unprecedented. The film was launched in 18 April 2003 under the title "Sandiyar" 

Again, its the first movie where the ancient daredevilry bull-fight game of Jalli Kattu was shot live with real charging bulls and the never-say-die unarmed bull-fighters.

Although Jalli Kattu is very popular in places like Alanganallur and Tiruchi during the Pongal season, perhaps for the first time it was recorded live with all the accompanying action and bravery of the men for the benefit of the movie goers and for posterity as well. Over 50 choicest rugged bulls known for its toughness were specially selected along with trainers and bull fighters.

Not only the trainers, but the villagers who are familiar with the events were transported from the villages to the shooting spot to be part of the epoch-making event.

In all, almost 1,000 persons were brought from Madurai district to give an authentic flavor to the sequences. Several technicians risked their lives to capture live the realism involved in the bull fight which is going to be among the highlights of the film. It is bound to be action personified on the screen.

Two special Kangeyam bulls ever ready to charge were carefully trained with great difficulties for the movie, again a first in the tinsel world. The bulls were kept at Sivaji Gardens and trained. The lead actress Abirami and hero Kamal Haasan did their home work daily and spent time with the bulls as part of the familiarization process.

This is perhaps for the first time two southern villages were transported live to the Campa Cola complex in Chennai. It was a metamorphism of a rare kind. The complex was transformed into bustling Theni and Madurai villages with people speaking the local dialect.

The complex had festivities and fun, frolic and festoons. It was hard to distinguish the sets from the streets. The Jalli Kattu Street was so natural that even people who took part in scores of Jalli Kattu competitions could not believe their eyes and felt that they were in their native village.

Kottala Devar home and Nallamma Naicker palace were artistically built with dexterity that is bound to take people down the memory lane. It was hard to believe that the sets werecreated in Chennai, be it Virumandi temple, Central Prison or police station. They were the cynosure of all eyes.

The sets could become another Stratford-upon-Avon of England where people visit to see the monuments of WilIiam Shakespeare. There are even requests to Kamal Haasan not to abandon the sets for at least six months but to throw open to the public for viewing before the release of the film.

Art Director Prabhakaran had re-created the typical village that even a local villager who passes by the set would think that he is setting foot on his native land. The sets stand testimony to the creative hands and brains that worked for the success of the movie. Prabhakaran is all set to become Tamil Nadu s top art director with the release of the film, claims the unit.

In the arena of action, Kamal Haasan decided to use for the first time High velocity spear thrower, air pressure units in the stunts to give real-life effects. This was thought about for Marudanayagam. The speed thrower has an amazing grit to throw the sharp edged Vel to over half a kilometre distance.

It can help the object travel like the amazing speed. of a rocket or that of sound. This left the actor and the entire unit speechless. A constant learner and an experimenter himself, Kamal decided to learn how to drive a moped without using his hands.

Live sound recording system was used and for the first time, the Neuendo machine was kept at the shooting venue. There were special cameras for fight scenes as well. Vadipatti drummers were specially brought to give the local flavor for the movie. Not only Vadipatti drummers but Vadugapatti crackers were used too.

Cinematographer Kesav Prakash who was trained in India and the United States was an assistant to Ravi K Chandran in Marudanayagam project and Kamal Haasan spotted him. Both Kamal Haasan and Keshav Prakash tried to make the film in high definition mode but had to abandon the project due to some technical snags. 

==Awards==
The film has won the following awards since its release:

2004 Puchon International Fantastic Film Festival (South Korea)
* Won - International Award for Best Asian Film - Virumaandi - Kamal Haasan

==Controversy==
The leader of Pudhiya Tamizhagam, a caste-based political organisation in Tamil Nadu, Dr K Krishnasamy, had announced that he would not allow the shooting of the film in the southern districts of Tamil Nadu. The reason given out by him was that the movie would encourage casteist tendencies and foster violence and found the title of the film derogatory.

When actor Kamal Hassan sought protection for the shooting of the film from the Superintendent of Police, Theni District, the latter refused to provide protection stating that he could not arrange for protection to the film shooting for three months with the meagre police force available at their disposal.

Tamil Nadu Chief Minister J. Jayalalithaa, going by this version of the SP, was also initially reluctant to spare a large section of the police force for film shooting.

Kamal Hassan, however, tackled the sensitive situation admirably. He maintained composure, refused to be drawn into any controversy. He recently met the Chief Minister and conveyed to her that his film was not aimed at generating controversy, caste clashes or violence. His explanation apparently convinced the Chief Minister.

The Film was super hit.
 

==Release==
The film was certified "A" by Censor Board. 

==Box office==
The film released in 425 screens worldwide and first in Tamil cinema with crossing 400 screens. This film opened with highly positive reviews from critics,     it was an super hit performer at the Box Office. It won the Best Asian Film Award at the International film festival in South Korea.   

== Music ==
The soundtrack features 10 songs composed by Ilayaraaja. This movie features live recording; most of the sequences were recorded lively.

===Tamil tracklist===
* "Onnavida" - Kamal Hassan, Shreya Ghoshal penned by Kamal Hassan himself
* "Andha Kandamani" - Ilayaraja, Kamal Hassan, Karthik Raja, Surendar, Tippu
* "Anna Lakshmi" - Kamal Hassan
* "Chandiyera Chandiyera" - Shreya Ghoshal
* "Karbagraham Vitu Samy Veliyerathu" - Ilayaraja, Kamal Hassan, Karthik Raja, Surendar, Tippu
* "Karumathur Katukulae" - Mary, Periya Karuppu Thevar, Sukumar, Thiruvudiyan
* "Kombulae Poov Suthi" - Kamal Hassan
* "Maada Vilakkae" - Kamal Hassan
* "Magarasiyae Manna Vitu Poniyae" - Theni Kunjaramma
* "Nethiyelae Pottu Vai" - Karunanidhi, Sukumar, Thiruvudiyan

===Telugu tracklist===
* Andagada Andagada - Swarnalatha
* Chiti Chepamma -Swarnalatha
* Intiki Deepam - Swarnalatha
* Kommalaku Pulu - SPB
* Nuvvuthappa Nenu - SPB, Sujatha
* Pilisthe Palike Deivam - SPB
* Tharigida Tharigida

==Trivia==
* Extensive sets of rural Tamil Nadu and prison were erected in Chennai.
* Bulls from Madurai and nearby districts were transported to Chennai for the bull fight sequences.
* After much controversy, the film titled "Sandiyar" earlier was finally certified, and Kamal Haasan screened it at the National Film Festival in 2004.

== See also ==
* Rashomon effect

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 