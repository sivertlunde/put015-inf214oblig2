Touki Bouki
{{Infobox film
| name           = Touki Bouki
| image          = Touki_Bouki_cover.jpg
| image size     =
| caption        = 
| director       = Djibril Diop Mambéty
| producer       = 
| writer         = Djibril Diop Mambéty
| based on       = 
| starring       = Magaye Niang Mareme Niang
| music          = Josephine Baker Mado Robin Aminata Fall
| cinematography = Pap Samba Sow
| editing        = Siro Asteni Emma Mennenti   
| studio         = Cinegrit Studio Kankourama
| distributor    = World Cinema Foundation
| released       =  
| runtime        = 95 minutes
| country        = Senegal
| language       = Wolof
| budget         = $30,000
}}
 Wolof for Senegalese drama film, directed by Djibril Diop Mambéty.    It was shown at the 1973 Cannes Film Festival     and the 8th Moscow International Film Festival.   

The film was restored in 2008 at Cineteca di Bologna / L’Immagine Ritrovata Laboratory by the World Cinema Foundation.   

==Plot==
Mory, a charismatic cowherd who drives a motorcycle mounted with a bull-horned skull, and Anta, a female student, meet in Dakar. Alienated and tired of life in Senegal, they dream of going to Paris and come up with different schemes to raise money for the trip. Mory eventually contrives to steal the money, and much clothing, from the household of a wealthy homosexual while the latter is taking a shower. Anta and Mory can finally buy tickets for the ship to France. But when Anta boards the ship in the Port of Dakar, Mory, poised on the gangplank behind her, is suddenly seized by an inability to leave his roots, and he runs away madly to find his bull-horned motorcycle, only to see that it has been ruined in a crash that nearly killed the rider who had taken it. The ship sails away with Anta but not Mory while the hauntingly melodious song "Love Is Fleeting, But Rejection Lasts a Lifetime" is sung and Mory sits next to his hat on the ground, staring disconsolately at his wrecked motorcycle. The film is written in French and native Senegalese Wolof, with English subtitles.

==Cast==
*   as "Aunt Oumy"
* Ousseynou Diop as "Charlie"
* Magaye Niang as "Mory"
* Mareme Niang as "Anta"

==Production==
  juxtaposition of premodern, pastoral and modern sounds and visual elements, Touki Bouki conveys and grapples with the hybridization of Senegal.

==Awards==
* International Critics Award at 1973 Cannes Film Festival FIPRESCI at Moscow Film Festival 
* Touki Bouki ranked #52 in Empire (magazine)|Empire magazines "The 100 Best Films Of World Cinema" in 2010. {{cite web
| title = The 100 Best Films Of World Cinema – 52. Touki Bouki
| url = http://www.empireonline.com/features/100-greatest-world-cinema-films/default.asp?film=52 
| work = Empire 
}} 
 

==References==
*  
*  

 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 