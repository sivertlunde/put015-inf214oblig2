Polis (film)
{{Infobox film name = Polis image =  alt = Poster for Polis producer = Meagan Judkins Alex Sanchez Sonja Mereu director = Steven Ilous writer = Daniel Perea Steven Ilous starring = Parker Young Dileep Rao Mark Kelly music = Electric Youth Room8 cinematography = David Waldman editing = Simon Carmody Steven Ilous production company = SMI Entertainment released =   runtime = 5 min. 28 sec. budget = $20,000
}}
Polis is a 2014 short film directed by Steven Ilous and written by Steven Ilous and Daniel Perea. It won the New Regency and Defy Media PROTOTYPE competition in January 2015, the prize for which was a feature development deal at New Regency. 

==Synopsis==
Set in the distant future, Polis is a sci-fi thriller about David Porter, a young telepath whose search for his mother threatens to uncover a utopian society’s horrifying secrets. 

==Accolades==
* 2015: PROTOTYPE Grand Prize Winner
* 2015: Vimeo Staff Pick  
* 2015: One Room With a View—Short of the Week (February 9, 2015)

==References==
 

==External links==
*  
*  
*  
*  

 