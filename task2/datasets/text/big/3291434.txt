Danger Zone (film)
{{Infobox film
| name = Danger Zone
| image =Dangerzoneposter.jpg
| caption =
| director = Allan Eastman
| producer = Avi Lerner Danny Dimbort Trevor Short
| writer = Jeff Albert
| starring = Billy Zane Robert Downey Jr. Cary-Hiroyuki Tagawa Ron Silver
| music = Daniel Pelfrey
| cinematography = Yossi Wein
| editing = Alain Jakubowicz
| distributor = Nu Image Films
| released =August 2, 1996
| runtime = 92 min.
| country = United States/Canada/South Africa English
| budget = $8,500,000
| preceded_by =
| followed_by =
}}
Danger Zone is a 1996 film directed by Allan Eastman and starring Billy Zane and Robert Downey Jr.

==Cast==
* Billy Zane .... Rick Morgan
* Robert Downey Jr. .... Jim Scott
* Cary-Hiroyuki Tagawa .... Monsieur Chang
* Lisa Collins .... Dr. Kim Woods
* Ron Silver .... Maurice Dupont
* Patrick Shai .... Madumo
* Russel Savadier .... Nando
* Greg Latter .... Portuguese Sergeant
* Eric Miyeni .... Military Driver
* Elizabeth Mathebula .... African Woman
* Zane Meas .... Indian Sergeant
* Connie Magilo .... Tebi, Scotts Wife
* Waylin Jansen .... Scarlett, Scotts Daughter
* Patrick Ndlovu .... Minister
* George Lamola .... Scotts Jeep Driver
* Chris Buchanan .... Mercenary #1
* Tshepo Nzimande .... Mercenary #2
* Cordell McQueen .... Mercenary #3
* Hector Rabotabi .... Rebel #1
* Tracy Lee Cundill .... U.S. Reporter
* Harry Sekgobela .... Duponts Driver

==External links==
*  
*  

 
 
 
 
 
 
 
 
 


 
 