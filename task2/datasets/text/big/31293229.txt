Kullanari Koottam
{{Infobox film
| name = Kullanari Koottam
| image = 
| caption = 
| director = Sribalaji
| producer = V. Ashish Jain
| writer = Sribalaji 
| starring = Vishnu Vishal Remya Nambeesan
| music = V. Selvaganesh
| cinematography = J. Laxman
| editing = Mu. Kasivishwanathan
| studio = Darshan Creations
| distributor = 
| released =  
| runtime = 
| country = India
| language = Tamil
| budget =
| gross =
}}
Kullanari Koottam ( : Crowd of Foxes) is a 2011 Indian Tamil-language romantic comedy film written and directed by Sribalaji and produced by V. Ashish Jain.   It stars Vishnu Vishal and Remya Nambeesan in the lead roles, while the crew of Vennila Kabadi Kuzhu reunited for this film, with V. Selvaganesh composing the films score and J. Laxman and Kasi Viswanathan handling cinematography and editing, respectively.  The film released on 25 March 2011 and received positive reviews.

==Story==
Vetrivel (Vishnu Vishal), an MBA graduate, who doesnt find a job, lives with his family in the temple city of  Madurai  . One day, his father gives him Rs.1500 to E-recharge his mobile phone. But under confusion he says the number to the recharge girl fast and she recharges to another number which belongs to Priya (Remya Nambeesan). After sometime he realizes and asks her to return the money. While they meet, they fall in love and decide to get married. She asks him to see her father who is a Police Sub-Inspector in Ranuvapetai . He asks him to join the army or at least to become an police officer, only then he will allow him to marry his daughter. Then rest of the story revolves how he succeeds in love and job despite bad selection officers at selection. In the meantime, Vetri also makes friends with other participants at the selection.

==Cast==
* Vishnu Vishal as Vetrivel Shanmugasundaram
* Remya Nambeesan as Priya Sethuraman Soori as Murugesan
* Appukutty as Lenin
* A. Ramesh Pandian
* Vairavan as Naseer
* Pandi as Narayanan
* Mayi Sundar
* Aiyyappan

==Soundtrack==
The music of the film was composed by V. Selvaganesh. The soundtrack album was released by A. R. Rahman in Chennai. The soundtrack opened to generally positive reviews and the song Vizhigalile became popular.

{| class="wikitable" style="width:80%;"
|-
! No. !!Song Title !! Singers !! Lyricist
|-
|1|| "Vizhigalile" || Karthik (singer)|Karthik, Chinmayi || Na. Muthukumar
|-
|2|| "Aadugira Maattai" || Krishna Iyer, Mukesh || V. Elango
|- Hariharan || Surya Prabhakar
|-
|4|| "Kullanari Koottam" || Shankar Mahadevan || Na. Muthukumar
|-
|5|| "Achham" || Rashmi, Kalpana || Na. Muthukumar
|}

==Release==
The satellite rights of the film were sold to STAR Vijay. The film was given a "U" certificate by the Indian Censor Board.

==Reception==
Kullanari Koottam received mixed reviews. The Times of India gave 3 stars out of 5 and wrote, "Sribalaji makes a middling debut as director with this movie. Though it could have been made into a breezy entertainer, it ends up as an unsatisfactory watch".  Rediff gave 2.5 stars out of 5 and wrote, "Sri Balajis script does lack logic in a few places and sags at times, but taken with its humour quotient, its still a reasonably fresh attempt at a feel-good love story".  The Hindu wrote, "Its heartening that writer and director Sri Balaji has consciously steered clear of clichés in his maiden attempt. Without a hero who challenges the cantankerous, villains who are ruthless, a comedy track thats disjointed, stunts which are unbelievable and duets that come with unnatural jigs, Sri Balaji has made Kullanari Kootam fairly engaging".  Sify wrote, "Like the proverbial curate’s egg, KK is good in parts, but could have been better".  The New Indian Express wrote, "The film is mildly engaging, and a clean wholesome entertainer". 

Behindwoods gave 1 star out of 5 and wrote, "The plot had the potential to be woven into an interesting tale, had only the director made an attempt to impart some airtight packaging in the content. KNK very slowly unravels, takes various detours in its path, the scenes and dialogues get boring and the concept flounders. The connection of the audience with the characters does not happen and the amateurish performance of most of the artists further adds up to this factor".  Indiaglitz wrote, "Kudos to Sri Balaji and Vishnu for rendering a movie that is entertaining and light without any overdose of emotions. If you are not looking for a serious stuff, Kullanari Koottam may be the right choice to spend few hours of your weekend".  Deccan Herald wrote, "Rendered in a lackadiscal fashion, Sribalaji’s Kullanari Koottam is not the proverbial entertainer and falls flat in its intentions with nothing to write home about". 

==References==
 

==External links==
*  
* 

 
 
 
 
 