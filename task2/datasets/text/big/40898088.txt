Harvesters of the Bay
 
{{Infobox film
| name           = Harvesters of the Bay
| image          = 
| director       = Oliver Dickinson
| producer       = Anthony Dickinson
| cinematography = Oliver Dickinson
| editing        = Oliver Dickinson
| distributor    = LVP
| released       =  
| runtime        = 52 minutes
| country        = France United Kingdom
| language       = French
}}
Harvesters of the Bay is a documentary directed by Oliver Dickinson.

Over the course of a year, the film portrays the work and thoughts of three salt harvesters in the historic Bourgneuf Bay, located in the west of France.

Harvesters of the Bay has received prizes such as Best Medium Length Film and the Jury Award from the Mûrs-Erigné Nature Film Festival (France).

==See also==
* Salt evaporation pond
* Pays de Retz
* Ecology

==External links==
*  

 
 
 
 


 