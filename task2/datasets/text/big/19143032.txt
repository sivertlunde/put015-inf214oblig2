Chalo Let's Go
 
{{Infobox film
| name = Chalo Lets Go
| image = Chalo-lets-go1.jpg
| director = Anjan Dutt
| writer = Anjan Dutt
| starring = Rudraneel Ghosh Saswata Chatterjee Parambrata Chatterjee Ritwick Chakraborty
| producer = Arun Poddar
| distributor =
| cinematography = Indranil Mukherjee
| editing = Mainak Bhaumik
| released = 6 June 2008
| runtime = 142 minutes
| country = India Bengali
| music = Neel Dutt
}}

Chalo Lets Go  is a 2008 Indian movie in Bengali  starring Ritwick Chakraborty,  Rudraneel Ghosh, Saswata Chatterjee and Parambrata Chatterjee, written and directed by Anjan Dutt. 
{{Cite web
|url=http://www.telegraphindia.com/1080531/jsp/entertainment/story_9344312.jsp
|title=14 fellow travellers
|publisher=www.telegraphindia.com
|accessdate=2008-11-12
|date=31 May 2008
|last=
|first=
}}
  
{{Cite web
|url=http://www.telegraphindia.com/1080730/jsp/entertainment/story_9618595.jsp
|title=Tolly tally
|publisher=www.telegraphindia.com
|accessdate=2008-11-12
|date= 30 July 2008
|last=
|first=
}}
 

==Plot==
This is a story about four friends, Hari, Shekhar, Ashim, and Sanjay. They studied at the same school. Ashim studied to be a doctor, but decided to never pursue that profession. Sanjay worked for an ad agency but left the job. The rest have never held a job. They started a Bengali band, but they were beaten up by the public at a stage show in North Bengal. They vowed to never perform again. At this point, they start a travelling agency, called Gharoa Travels. On their first trip, they start the journey with only 9 passengers. On the way to the hills, a girl named Ria joins them. The film deals with their journey through North Bengal. The ten tourist and the four operators form an assorted medley of characters. As the journey progresses, their natures are slowly revealed. The film has a style of flash-forwarding to the future juxtaposed with the present, giving the audience a sense of the fates of the characters even as the story progresses.

==Cast==
* Saswata Chatterjee as Ashim

* Parambrata Chatterjee as Sanjoy (as Parambrata Chattopadhyay)

* Rudranil Ghosh as Hari

* Ritwick Chakraborty as Shekhar 

* Churni Ganguly as Miss Gombhir (or Miss Ganguly) (as Churni)

* Aparajita Ghosh Das as Ria (as Aparajita)

* Bidipta Chakraborty as Madhuja

* Koneenica Banerjee as June

* Sunita Sengupta as Tulu (as Sunita)

* Dhruv Mookerji as Dr. Basu
 Neel Mukherjee as Rudrashekhar

* Arindam Sil as Rabi Chatterjee

* Kaushik Ganguly as Anando

* Barun Chanda as Dhurjati

:Rest of the cast members

* Deep Arora

* Sudhir Bhitolkoty

* Ronit Ghosh

* Ronjit Ghosh

* Rohit Kumar Gupta

* Bijon Karmakar

* Sandip Mukherjee

* Austin Plant

===Cast Synopsis===

*Saswata Chattopadhyay—Ashim-I play Ashim, one of the four friends who start the travel agency. Ashim is a doctor by profession but he wants to do something else. His life changes completely after the trip to Darjeeling. I believe Ashim’s is the best role in Chalo Let’s Go. The character is very dynamic. And I loved the way he changes for the better at the end. Also, he is attracted to Miss Gombhir in a very subtle way.

*Parambrato Chattopadhyay—Sanjoy-I play Sanjoy. He is the narrator of the story. Sanjoy wanted to be a journalist but starts a travel agency instead with three friends (Rudranil, Saswata and Ritwick). He is shrewd and thinks a lot before he speaks. When Anjanda offered me the role, I felt Rudranil and Saswatada had the better roles. But then he told me that Sanjoy is the backbone of Chalo Let’s Go.

*Ritwick Chakraborty Shekhar-I am Shekhar, one of the four friends. Shekhar makes it very big as a singer later. He is rude and always angry but very innocent. I liked this contradiction in him. There’s no love angle to this character. He is basically an ambitious guy.

*Arindam Sil Rabi Chatterjee-I play Rabi Chatterjee, a Casanova who lives with June (Koneenica) & loves to introduce himself as "Madamcrackerologist". When Anjanda read out the script to us, everyone wanted to play Rabi. And I was under the impression that I would be asked to play the NRI doctor!

*Rudranil Ghosh Hari-I play Hari, the lead vocalist of a Bangla band. The other members are Parambrata, Ritwick and Saswata. But after a disastrous performance, the four friends decide to get into the travel business. Then they go travelling. A lot of the story unfolds in Darjeeling. Hari is a simple guy whose girlfriends run away because he can’t speak English. But Ria falls for Hari. Anjanda had initially offered me Ritwick’s role (Shekhar), but he changed his mind and cast me as Hari later.
 Churni Gangopadhyay Miss Gombhir-I am Miss Ganguly, a very sophisticated writer. Her fellow travellers call her Miss Gombhir. But she is a very understanding woman and before anyone else realises she becomes a part of their lives. Though she seems unapproachable, she is just the opposite.... I had a big yes for Anjan when he offered me the role. We gel very well. We both did our schooling in the hills.

*Koneenika Bandyopadhyay June-Mine is a very small role but I have never played anything like this before. I am June, a very fashionable girl in a live-in relationship with Rabi (Arindam Sil). June doesn’t interact with anyone else. And everyone gossips about her. Frankly when Anjanda approached me with the role, I had no clue it was going to be this small but it’s

*Aparajita Ghosh Das Ria-I am Ria, who is very confused with life. Ria is a bit tomboyish and she hides her identity because she thinks it will help her in some way. The one person she connects with is Churni (Ganguly), who gives her shelter. Parambrata and Rudranil like Ria, so there’s a love triangle and even more confusion. Actually, there’s a lot more to the character which I can’t reveal.

*Kaushik Ganguly Anando-I play Anando, a spineless character who is extremely scared of his wife Madhuja (Bidipta). It is a very interesting role.
 Neel Mukherjee Rudrashekhar-I am Rudrashekhar, a bachelor who loves to travel but complains about everything and everyone. He cribs about his seat on the bus, his room in the hotel and everything else under the sun. The others dislike him for this but there’s a slow change in him.

*Barun Chanda Dhurjati-I play Dhurjati, a kinky professor of chemistry. But nobody knows much about him. He has a research paper to develop, which is actually a book on pornography. He is one of those types who can’t see anything good in anyone. But there is a gradual change in him. He connects with Shekhar (Ritwick) like a father. And he is secretly attracted to Miss Gombhir.

*Sunita Sengupta Tulu-I play Tulu, an NRI married to Dhruv (Dr Basu). Both husband and wife are on a vacation in India. Tulu is a little psychic. She has a strong sixth sense and is into tarot card-reading. Her husband pampers her a lot. When Anjanda narrated the role, I found it to be very

*Bidipta Chakraborty Madhuja-I am Madhuja, the middle class wife of Kaushik Ganguly. She is full of Bengali idiosyncrasies. She is the type who would take care of her husband and worry about him. But she is trapped in a bad marriage and she confesses that to June (Koneenica).

*Dhruv Mookherji- DR BASU-I am Dr Basu, a cardiologist from London. He is married to Tulu (Sunita) and has come to India after many years. Dr Basu loves to jabber away and would strike up a conversation even when the other person is least interested. The only person he doesn’t get along with is Dhurjati (Chanda).

==Crew==
*Director: Anjan Dutt

*Producer Arun Poddar

*Music Director Neel Dutt

*Lyrics Anjan Dutt Vibha Singh

*Screenplay Anjan Dutt

*Story Anjan Dutt

*Dialogue Anjan Dutt

*Cinematographer Indranil Mukhopadhay

*Editor Mainak Bhowmik

*Costumes Designer Chhanda Dutta

*Playback Singer Anjan Dutt, Nachiketa Chakraborty|Nachiketa, Rupam Islam, Rupankar, Ujjaini, Taniya Sen & Srikanta Acharya,

*Presenter Anjan Dutt

==Soundtracks==
The music has been composed by Neel Dutt and has been released under Saregama.

{| class="wikitable"
|-  style="background:#ccc; text-align:center;"
! Track No. !! Title !! Length !! Music Composer !! Performed By !! Courtesy(TM/C)
|-
| 01.
| Chalo Lets Go
| 04:43
| Neel Dutt
| Anjan Dutt, Srikanta Acharya, Ujjaini, Rupam Islam
| Saregama
|-
| 02.
| Ei Path
| 04:31
| Neel Dutt
| Anjan Dutt, Srikanta Acharya, Ujjaini
| Saregama
|-
| 03.
| Aachhe Dukkho Aachhe Mrittyu
| 03:41
| Neel Dutt
| Srikanta Acharya
| Saregama
|-
| 04.
| Gaan Khuje Paai
| 05:08
| Neel Dutt
| Rupankar Bagchi
| Saregama
|-
| 05.
| Chupi Chupi Raat
| 05:03
| Neel Dutt
| Rupankar Bagchi
| Saregama
|-
| 06.
| Cross The Line
| 05:10
| Neel Dutt
| Rupam Islam
| Saregama
|-
| 07.
| Amazing Grace
| 05:03
| Neel Dutt Nachiketa & Taniya Sen
| Saregama
|-
|}

==Critical reception==
 

==References==
 

==External links==
* 

 
 
 
 
 