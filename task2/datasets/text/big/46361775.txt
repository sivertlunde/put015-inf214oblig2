Twilight (1940 film)
{{Infobox film
| name = Twilight
| image =
| image_size =
| caption =
| director = Rudolf van der Noss
| producer = Richard Riedel
| writer =  Arthur Pohl 
| narrator =
| starring = Viktor Staal   Ruth Hellberg   Carl Raddatz 
| music = Kurt Schröder 
| cinematography = Hugo von Kaweczynski   
| editing =  Else Baum         UFA
| distributor = UFA
| released =  
| runtime = 85 minutes
| country = Germany  German 
| budget =
| gross =
}}
Twilight (German:Zwielicht) is a 1940 German film directed by Rudolf van der Noss and starring Viktor Staal, Ruth Hellberg and Carl Raddatz.  The films art direction was by Hermann Asmus and Carl Ludwig Kirmse.

==Cast==
*   Viktor Staal as Walter Gruber 
* Ruth Hellberg as Grete Kuhnert 
* Carl Raddatz as Robert Thiele  
* Ursula Grabley as Renate Gutzeit  
* Fritz Genschow as Forstmeister Jürgens  
* Paul Wegener as Förster Kuhnert 
* Hans Stiebner as Schankwirt Gutzeit  
* Wilhelm König as Alfred Gruber 
* Willi Rose as Kriminalassistent Sievers 
* Wilhelm Althaus as Kriminalrat Malzahn 
* Gerhard Dammann as Dornkaat 
* Erich Dunskus as Gendarmeriewachtmeister Weber
* Albert Lippert as Kriminalkommissar Hagenbach  
* Ernst Rotmund as Zirkusdirektor Mansfeld 
* Käte Kühl as Emilie  
* Walter Lieck as Schulze 
* Lotte Rausch as Amanda 
* Willi Schur as Köppke
* Paul Westermeier as Paul Borchert  
* Otto Matthies 
* Bob Bolander 
* Otto Braml 
* Kurt Cramer 
* Kurt Dreml  
* Erich Haußmann 
* Paul Hildebrandt 
* Fritz Hube 
* Hellmuth Passarge 
* Arthur Reinhardt 
* Rudolf Renfer
* Walter Schenk 
* Herbert Schimkat 
* Willy Stettner 
* Hermann Stetza 

== References ==
 

== Bibliography ==
* Hans-Michael Bock and Tim Bergfelder. The Concise Cinegraph: An Encyclopedia of German Cinema. Berghahn Books.

== External links ==
*  

 
 
 
 
 

 