Mademoiselle Midnight
{{Infobox film
| name           = Mademoiselle Midnight
| image          = Mademoiselle_Midnight.jpg
| image size     =
| caption        = Italian Poster
| director       = Robert Z. Leonard
| producer       = Robert Z. Leonard John Russell
| starring       = Mae Murray  Monte Blue
| music          =
| cinematography =
| studio         = Tiffany Productions
| distributor    = Metro Pictures
| released       =  
| runtime        = 70 minutes
| country        = United States
| language       = Silent English intertitles
| budget         =
}}
 silent drama John Russell. Metro under the Tiffany Productions banner, owned by the couple.  A complete print of the film survives. 

==Plot==
Renée (Mae Murray) is the heiress of a Mexican ranch, granddaughter of a woman known for her recklessness and frivolity at night. This first "Mademoiselle Midnight" is banished in the opening scene by Napoleon III at Empress Eugenies insistence to Mexico.  Renee is kept locked at the hacienda at night by her father to prevent her following in her grandmothers wayward footsteps.  She falls in love with a visiting American (Monte Blue) but is also pursued by the craven outlaw Manuel Corrales. Miss Murray gets to do some of her trademark dancing, but this one isnt a comedy, despite comic relief provided by Johnny Arthur.

==Cast==
* Mae Murray - Renée de Gontran / Renée de Quiros
* John St. Polis - Colonel de Gontran (Prologue)
* Paul Weigel - Napoleon III (Prologue)
* Earl Schenck - Emperor Maximilian (Prologue)
* Clarissa Selwynne - Empress Eugénie (Prologue)
* J. Farrell MacDonald - Duc de Moing (Prologue)
* Monte Blue - Owen Burke / Jerry Brent Robert McKim - João / Manuel Corrales
* Robert Edeson - Don Pedro de Quiros
* Nick De Ruiz - Don José de Quiros
* Nigel De Brulier - Dr. Sanchez
* Johnny Arthur - Carlos de Quiros

== References ==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 


 