The Crimson Kimono
{{Infobox film
| name           = The Crimson Kimono
| image          = The-crimson-kimono-1959 poster.jpg
| image size     = 
| alt            = 
| caption        = Theatrical release poster
| director       = Samuel Fuller
| producer       = Samuel Fuller
| screenplay     = Samuel Fuller
| narrator       =  Victoria Shaw Glenn Corbett James Shigeta
| music          = Harry Sukman
| cinematography = Sam Leavitt
| editing        = Jerome Thoms
| studio         = Globe Enterprises
| distributor    = Columbia Pictures
| released       = October 1959
| runtime        = 82 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Victoria Shaw. 

It featured several ahead-of-its-time ideas about race and societys perception of race, a thematic and stylistic trademark of Samuel Fuller|Fuller.

==Plot== Victoria Shaw), and the two principal leads.

==Cast==
* James Shigeta as Detective Joe Kojaku
* Glenn Corbett as Detective Sgt. Charlie Bancroft Victoria Shaw as Christine Downes
* Anna Lee as Mac
* Paul Dubov as Casale
* Jaclynne Greene as Roma
* Neyle Morrow as Hansel
* Gloria Pall as Sugar Torch
* Pat Silver as Mother (as Barbara Hayden)
* George Yoshinaga as Willy Hidaka
* Kaye Elhardt as Nun

==Reception==

===Critical response===
The Crimson Kimono was met with critical acclaim. The film scored a perfect rating of 100% on Rotten Tomatoes based on 5 reviews. 

The staff at Variety (magazine)|Variety magazine said of the film, "The mystery melodrama part of the film gets lost during the complicated romance, and the racial tolerance plea is cheapened by its inclusion in a film of otherwise straight action...The three principals bring credibility to their roles, not too easy during moments when belief is stretched considerably. Anna Lee, Paul Dubov, Jaclynne Green and Neyle Morrow are prominent in the supporting cast." 
 Time Out magazine wrote that of the film saying, "Fuller developing his theme of urban alienation: landscape, culture and sexual confusion are all juxtaposed, forcing the Japanese-born detective (who, along with his buddy, is on the hunt for a burlesque queen murderer) into a nightmare of isolation and jealousy. Some fine set pieces - like the disciplined Kendo fight that degenerates into sadistic anarchy - and thoughtful camera-work serve to illustrate Fullers gift for weaving a poetic nihilism out of his journalistic vision of urban crime." 

More recently, Ed Gonzales of Slant Magazine liked the film and wrote, "The opening is a triumph of grungy lyricism achieved through snaky cutting and blunt compositions: Sugar Torch (Gloria Pall), a blond and bodacious piece of stripper meat, is shot to death in the middle of a Los Angeles street after witnessing a murder inside her dressing room. The tenor of the film oscillates between tight-fisted noir and chamber drama, but the theme is always the same: cultural and romantic unrest...Fullers feat is giving the films nonstop interrogations, meetings and confrontations profound racial and political meaning." 

==References==
 

==External links==
*  
*  
*  
*   essay by Jeremy Arnold at Turner Classic Movies
*   essay by Philip W. Chung at Asian Week
*  

 

 
 
 
 
 
 
 
 
 
 
 
 