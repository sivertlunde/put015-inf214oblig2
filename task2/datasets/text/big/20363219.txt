Cannibal (2006 film)
{{Infobox film
| name           = Cannibal
| image          = Cannibal - Aus dem Tagebuch des Kannibalen.jpg
| alt            = 
| caption        = DVD cover
| director       = Marian Dora
| producer       = Frank Oliver   Marian Dora
| screenplay     = Marian Dora
| starring       = Victor Brandl   Carsten Frank
| music          = Brandl Victor   J.G. Thirlwell   Victor Brandl   Ghazi Barakat   Alexander Hacke
| cinematography = Marian Dora
| editing        = Marian Dora
| studio         = Quiet Village Filmkunst
| distributor    = 
| released       =  
| runtime        = 89 minutes
| country        = Germany
| language       = English   German
| budget         = 
| gross          = 
}}
Cannibal is a 2006 German direct-to-video horror film written, directed and produced by Marian Dora.

== Plot ==

In the intro, a mother reads the story of Hansel and Gretel to her young boy. The setting then moves to the present day, where The Man goes about his day-to-day routines and occasionally chats with others on his computer, where he looks for someone who shares his cannibalistic fantasies. The Man meets with several people he had chatted with, but he either rejects them or is rejected (and in one case attacked) by them, except for The Flesh, a suicidal man who volunteers to be killed and eaten by The Man. The Flesh travels to The Man from Berlin, and the two bond, having sex and frolicking in the nude in and around The Mans home.

When The Flesh decides that it is time for him to die and be devoured, he tries to coerce The Man into biting off his penis, but The Man is unable to go through with it, even when The Flesh uses drugs to knock himself out in an attempt to make things easier. Disappointed, The Flesh chastises The Man. The Flesh decides to give him another chance when The Man begs him to stay just as he is about to board a train back to Berlin. Returning to The Mans home, The Flesh ingests a large amount of alcohol and pills, then instructs The Man to castrate him with a kitchen knife, which The Man succeeds at doing. The two then fry and attempt to eat the severed penis, before The Flesh seemingly dies of blood loss in a bath The Man places The Flesh in.

The Man drags The Fleshs inert body (which vomits and defecates repeatedly) to a room he has readied for slaughter. Before he can begin taking The Flesh apart, The Man is shocked to discover The Flesh is still alive, so he stabs him in the throat. The Man then beheads, guts and dismembers The Flesh, buries the inedible parts, and cooks and eats the rest; he places The Fleshs severed head at the head of the table. The Man then masturbates to video footage of what he has done, and leaves.

== Cast ==

* Carsten Frank as The Man
* Victor Brandl as The Flesh
* Manoush as The Mans Mother
* L. Dora
* Carina Palmer
* Tobias Sickert
* Joachim Sigl
* Bernd Widmann

== Production ==

The film is based on the true story of Armin Meiwes who killed and ate a man whom he met on the Internet.  

In 2004 director Marian Dora, accepted Ulli Lommels assignment to make a feature film in Germany that documented the Meiwes case. Doras finished film was rejected by Lommel as being too gory, and Dora subsequently released the movie on his own in Germany within months. Lommel set about producing his own version of the Miewes case, which became the 2007 film Diary of a Cannibal. 

== Release ==
 the list of Media Harmful to Young People on 29 June 2007.

In the US, it was released on DVD 19 December 2006. 

== Soundtrack ==

The US Brooklyn, New York based Glam rock band The Flesh (formed for the film) composed three songs for the official soundtrack.
;Tracklist
* The Flesh – Im your flesh
* The Flesh – Castrate me
* The Flesh – Bite it

== Reception ==
Scott Weinberg of DVD Talk rated the film 2/5 stars and wrote, "One of the sickest and freakiest movies ever to come from a nation well-known for its freaky and sick movies (Germany), Cannibal is shocking, outrageous, sickening ... and just a little bit interesting because its based on actual events."   Joshua Siebalt of Dread Central rated it 2/5 stars and wrote, "The only real selling point Cannibal has is its graphic depiction of cannibalism. There are some nasty, nasty moments throughout, but you have to get through a whole lot of nothing before you see them." 

== References ==

 

== External links ==

*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 