Life Begins for Andy Panda
{{Infobox Hollywood cartoon
| name = Life Begins for Andy Panda
| image = Life Begins for Andy Panda1.jpg
| caption =
| series = Andy Panda
| director = Alex Lovy
| story_artist = Ben Hardaway   L.E. Elliott
| animator = Alex Lovy   Frank Tipper
| voice_actor = Mel Blanc   Bernice Hansen Margaret Hill-Talbot
| musician = Frank Marsales
| producer = Walter Lantz
| studio = Walter Lantz Productions
| distributor = Universal Pictures
| release_date =   (USA)
| color_process = Technicolor
| runtime = 8 min (one reel)
| movie_language = English
| preceded_by = N/A
| followed_by = Andy Panda Goes Fishing (1940)
}}
 American short subject cartoon created by Walter Lantz, as the very first Andy Panda film.     

==Background== Su Lin, who had been donated to the Brookfield Zoo in Chicago three years earlier  and whose arrival created a consumer desire for panda-related products.   The film was the first Andy Panda film, introducing Andy as a baby.        It was not named after the feature film Life Begins for Andy Hardy, which was released two years later. The cartoon was titled "Life Begins for Andy Panda" upon its release. 
 Knock Knock Walter Lantzs more famous character, Woody Woodpecker, was first introduced   

==Plot synopsis==
The story begins with Finchell Broadcasting Station telling the news that the panda family had their first baby. All of the forest animals rush to see the new baby. All the animals are excited to see him and shout to name the cub. Mama Panda makes the decision, to name him Andy. The arrival of a skunk causes them all to run away.  6 months later, Papa Panda is talking with Andy about appreciating Mother Nature, until Andy looks under a tree with his slingshot and hits an opossum. The opossum stamp on Papa Pandas foot. After Papa opines that Mother Nature has no place to live, Andy starts to cry.  
 Pygmy hunters that live in the wasteland. Andy runs away from his dad into the wasteland and Papa runs after him and lands in a trap, presumably made by the hunters. The hunters spot Andy and begin to chase him.  Finchell announces their plight to the animals and they gather to form a rescue mission.  Mr. Whippletree, a turtle, is the first animal to attempt to rescue Andy but he failes because a hunter had his shell.  

Mama Panda joins the action and fights off some pygmies.  A kangaroo successfully put Andy in his pouch but he was distracted by a pygmy while another one slapped the kangaroos fanny with a plank. The skunk, who had six months earlier scared the animals chases the pygmies away and the animals cheer and Andy is rewarded by Papa. Andy wishes that the events would be put in a Newsreel and Papa, about to spank him, instead decides to snuggle Andy.  As the cartoon ends, Mr. Whippletree the turtle, is seen chasing the pygmy who took his shell. 

==Style==
The animation used in this film when the pygmies were climbing out the rock was used again in Andy Panda Goes Fishing and again in 100 Pygmies and Andy Panda.  The Pygmies design changed from human-like, having yellow grass skirts and light brown skin to ape-like, having orange grass skirts and have darker skin in later cartoons.  The cartoon wasnt seen in US television for a number of years due to concerns with the 1939 films use of inappropriate stereotyping of Blacks through the pygmys appearance, but it was seen in countries like Brazil. 

==References==
 

==External links==
*   at the Internet Movie Database

 
 
 
 
 
 
 
 
 
 
 
 
 