Vizontele
 
{{Infobox film
| name           = Vizontele
| image          = Vizontele.jpg
| alt            =
| caption        = Vizontele Theatrical Poster
| director       =  
| producer       = Necati Akpınar
| writer         = Yılmaz Erdoğan
| narrator       = 
| starring       =   
| music          = Kardeş Türküler
| cinematography = Ömer Faruk Sorak
| editing        = Mustafa Preşeva
| studio         = Beşiktaş Kültür Merkezi
| distributor    = Warner Bros.
| released       =  
| runtime        = 110 mins
| country        = Turkey
| language       = Turkish
| budget         =
| gross          =
}}

Vizontele is a 2001 Turkish comedy film|comedy-drama film, written and directed by Yılmaz Erdoğan and co-directed by Ömer Faruk Sorak, based on the writer-directors childhood memories of the arrival of the first television to his village in the late 70s. The film, which went on nationwide release on  , won three Golden Orange awards and was one of the most successful Turkish films to that date. A sequel, entitled Vizontele Tuuba, involving the director and most of the original cast followed in 2004.

==Production==
The film was shot on location in Gevaş, Van Province, Turkey.   

==Synopsis==
The mayor (Altan Erkekli) of a small village in Southeastern Turkey in the 70s bitterly opposes the activities of sleazy opportunist Latif (Cezmi Baskin) who runs open-air film screenings and seeks to break his monopoly over village entertainment with the introduction of the first television (called Visiontele by the locals). The mayor recruits a crazy electrician called Emin (Yılmaz Erdoğan) and some of his office staff to help him to set up a television transmitter  on the highest position of the mountain. While Latif seeks to undermine his efforts by decrying television as the work of the Devil and a slap in the face of Islamic tradition. The story is based on the childhood memories of the writer-director Yılmaz Erdoğan of the arrival of the first television to Hakkâri, Turkey in the 70s.

==Societal Conflicts==
The movie tries to represent one of the main conflicts in the Turkish society of the late 70s, continuing well into 80s; that of the religious versus secular groups. “ here have always been struggles and contests between secular groups and religious groups over the nature of the political system.”  This is partly shown through the image of the clergyman of the town, who ironically stutters a lot, and apparently feels vitriolic toward the new-coming television, but later on turns out to be a pawn at the hands of Latif. Children escape his Quran reading class in order to go into the wild and frolic. An obvious point the movie makes in favor of secular ideas is the character of   Emin (played by one of the directors: Yılmaz Erdoğan). He is pictured as someone who is secluded and one whose only interest is modern technology. But the movie makers are clever enough not to take sides with either side of the conflict. As put eloquently by Başkan, “According to secularization theory, modernization leads to a decline in religion’s role in the public realm, with it turning into a matter for the private sphere. Instead, however, the contemporary world has witnessed a resurgence of religion with the emergence of religious movements throughout the world.”  The first piece of news that the inhabitants get to hear on the newly set television is so devastating (where the Mayors son,currently serving in the Army was killed during the Turkish invasion of Cyprus in 1974) that a burial of the TV set is in order; an iconic replacement of the burial of the beloved son. And the man who has to bury the TV set is no other than the technocrat of the town, on allegations that it has brought evil to the small community of the religious people.

==Awards==
* Cologne Mediterranean Film Festival (2001)
** Audience Award (won)
* Antalya Golden Orange Film Festival (2001)
** Golden Orange for Best Actor: Altan Erkekli (won)
** Golden Orange for Best Actress: Demet Akbağ (tied with Yesim Salkim for Sarkici)
** Golden Orange for Best Music: Kardeş Türküler (won)

==References==
 

==External links==
*  
*  

 
 
 
 
 
 