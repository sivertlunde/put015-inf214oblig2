The Old Barn
{{Infobox film
| name           = The Old Barn
| image          =
| image_size     =
| caption        =
| director       = Mack Sennett
| producer       = Mack Sennett
| writer         = Hampton Del Ruth (writer) Alfred M. Loewenthal (writer) Andrew Rice (writer) Earle Rodney (writer) John A. Waldron (story supervisor)
| narrator       =
| starring       =
| music          =
| cinematography = John W. Boyle Ernie Crockett George Unholz
| editing        = William Hornbeck
| distributor    = Educational Film Exchanges, Inc.
| released       =  
| runtime        = 20 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}

The Old Barn is a 1929 American comedy film directed by Mack Sennett.

== Plot summary ==
A motley collection of guests and regulars at a country hotel are anxious one dark and stormy night when they hear by a radio news bulletin that a dangerous criminal has just broke jail and is headed their way. A surly mystery man answering the description shows up, and then hides out in a nearby barn.

== Cast ==
*Johnny Burke
*Thelma Hill
*Daphne Pollard
*Andy Clyde
*Irving Bacon
*Vernon Dent Dave Morris
*Ruth Kane

== Soundtrack ==
 

== External links ==
* 
* 

 
 
 
 
 
 
 
 


 