The Flying Liftboy
{{Infobox film
| name           = Abeltje
| image          = Abeltje.jpg
| image_size     = 
| caption        = 
| director       = Ben Sombogaart
| producer       = 
| writer         = Burny Bos (writer), Annie M.G. Schmidt (novel)
| narrator       = 
| starring       = 
| music          =   
| cinematography = 
| editing        = 
| distributor    = 
| released       = November 26, 1998
| runtime        = 110 minutes
| country        = Netherlands Dutch
| budget         = 
}} 1998 Netherlands|Dutch film directed by Ben Sombogaart.  The film was based on the 1953 Dutch childrens book Abeltje by Annie M. G. Schmidt.

==Cast==
*Victor Löw	... 	Schraap
*Elleke Vervat	... 	a friend of Laura
*Roxanne Stam	... 	a friend of Laura
*Afroditi-Piteni Bijker	... 	a friend of Laura (as Afroditi Piteni Bijker)
*Taina Moreno	... 	a friend of Laura
*Nicole Sanchez	... 	a friend of Laura
*Soraya Smith... 	Laura
*Rick van Gastel	... 	Abeltje Roef / Johnny Cockle Smith (as Ricky van Gastel)
*Kees Hulst	... 	Schoolmaster
*Annet Malherbe	... 	Mother Roef / Mrs. Cockle-Smith
*Arnita Swanson	... 	TV presenter
*Marisa Van Eyle	... 	Miss Klaterhoen
*Frits Lambrechts	... 	Jozias Tump
*Nora Kretz	... 	Mrs. Tump
*Joseph Gudenburg	... 	Policeman

==Awards== Golden Calf for best motion picture (1999)
* Public choice award at the Montreal International Childrens Film Festival

==Plot summary==
Young Abeltje gets a job as a liftboy in a department store. His boss tells him that he may not press the elevators top (green) button under any circumstances. One time, when Abeltje gets into trouble, he presses the button, and the elevator goes shooting out the building and flies off.

Trapped with him on the elevator are a travelling mothball salesman, Jozias Tump; a singing instructor, Miss Klaterhoen, and a young girl, Laura. They fly across the ocean and eventually arrive in New York City, where they land in Central Park. In New York, Abeltje is mistaken for another boy who has gone missing. They leave New York and fly to South America, where Tump is made president of a banana republic and the missing boy is freed. A coup against Tump breaks out and the foursome once again manage to escape with their lift.

== External links ==
*  

 
 
 
 
 

 