The Bitch (film)
 
{{Infobox film
| name           = The Bitch
| image          = Thebitch1979.jpg
| image_size     = 
| caption        = 
| director       = Gerry OHara
| producer       = Brent Walker, Ron Kass, Oscar Lerman, Edward Simons
| writer         = Jackie Collins  (novel) , Gerry OHara
| starring       = Joan Collins, Antonio Cantafora, Kenneth Haigh
| music          = Biddu
| cinematography = Dennis Lewiston
| editing        = Eddy Joseph
| distributor    = Thorn EMI, Hoyts Distribution  (Australia) 
| released       = 1979
| runtime        = 89 min
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}} The Stud (1978), and both films were based on novels by British author Jackie Collins. Like its predecessor, the film starred her sister, Joan Collins, as Fontaine Khaled. Both films are considered to be softcore porn.

==Plot==
Fontaine Khaled (Collins) is the owner of a trendy disco who is now facing financial problems. At the same time, she also has to deal with the various men in her life, as well as the Mafia.

The plot of the film differs slightly from that of the novel, particularly the ending.

==Reception==
Although both The Stud and The Bitch were generally panned by critics, they were nevertheless both commercial successes and helped to revive Joan Collins career. In 1981, her performance in The Bitch attracted the attention of Aaron Spelling and Esther and Richard Shapiro when they were looking for an actress to play the part of Alexis Carrington in their TV series Dynasty (TV series)|Dynasty.

==Music== UK Top 40 hit single in August 1979. {{cite book
| first= David
| last= Roberts
| year= 2006
| title= British Hit Singles & Albums
| edition= 19th
| publisher= Guinness World Records Limited 
| location= London
| isbn= 1-904994-10-5
| page= 406}} 

==References==
 
*Simon Sheridan Keeping the British End Up: Four Decades of Saucy Cinema, Reynolds & Hearn Books (third edition, 2007)

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 


 
 