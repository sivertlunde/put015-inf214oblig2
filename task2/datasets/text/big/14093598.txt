Good-bye, My Lady (film)
{{Infobox film
| name           = Good-bye, My Lady The Boy and the Laughing Dog
| image          = Good-bye My Lady 1955 poster.jpg
| image_size     = 220
| caption        = 1956 Theatrical Poster
| director       = William A. Wellman
| producer       =  James Street
| screenplay     = Sid Fleischman
| narrator       = 
| starring       = Walter Brennan Phil  Harris Brandon de Wilde Sidney Poitier William Hopper Louise Beavers
| music          = Music composed and played by Laurindo Almeida – guitar A.S.C.A.P. George Fields – harmonica A.S.C.A.P.
| cinematography = William H. Clothier
| editing        = Fred MacDowell
| studio         = A Batjac Production
| distributor    = Warner Bros.
| released       = May 11, 1956
| runtime        = 94 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 American film adaptation of the novel Good-bye, My Lady (1954) by James H. Street. The book had been inspired by Streets original 1941 story which appeared in The Saturday Evening Post. Street was going to be the principal advisor on the film when he suddenly died of a heart attack.  

The film was produced by John Waynes Batjac Productions. Directed by William A. Wellman, it starred Walter Brennan and Brandon deWilde, with Sidney Poitier and Phil Harris in supporting roles.

Music composed and played by Laurindo Almeida (guitar) and George Fields (Harmonica). Song: "When Your Boy Becomes a Man". Music by Don Powell and lyrics by Moris Erby.

A boy learns what it means to be a man by befriending and training a stray Basenji dog and then is forced to surrender her to its rightful owner. Both readers of the story and film-goers found the boys eventual loss of the dog unexpected. 

Chosen for the film was My Lady of the Congo, a six-month-old Basenji puppy of Miss Veronica Tudor-Williams of Molesey, England. My Lady was flown to Hollywood to be followed later by four young dogs as doubles, including her little brother My Lord of the Congo and Flageolet of the Congo, subsequently an International Championship (dog)|Champion. As it was, My Lady wound up doing most of the scenes. When not filming with then 13-year-old deWilde, the dog spent all her time with him and a real attachment developed between them. Unknown to theater-goers that saw boy and dog parted in the film was the fact that the written agreement that supplied the animal stated that My Lady would become the personal property of Brandon deWilde upon completion of filming. 

The rare breed of dog was heretofore unknown to most Americans. Affected by either the story, the novel or the movie, many people were inclined to become Basenji owners at this time. 
 The Wild Blue Yonder and Brennan and deWilde would unite again for the cameras in 1965 for Disney in Those Calloways. That same year deWilde would play producer John Waynes son in In Harms Way.

==Plot== come of age" and surrender the animal. With the $100 reward money given, he is able to purchase Jesse the false teeth that he needs, and is able to put a down payment on a hunting rifle.

==Cast==
{| class="wikitable" width="50%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|- 
| Walter Brennan || Uncle Jesse Jackson
|-
| Phil Harris || Cash Evans
|-
| Brandon de Wilde || Claude Jackson (Skeeter)
|-
| Sidney Poitier || Gates Watson
|- 
| William Hopper || Walden Grover
|-
| Louise Beavers || Bonnie Drew
|-
|}

==Home media==
Good-bye, My Lady was originally released on VHS in the United States by Warner Home Video, on December 13, 1993. On December 10, 2010, Warner Archive Collection released Good-bye, My Lady as a manufactured on-demand remastered wide-screen DVD-R release.

In an interview for Turner Classic Movies Gretchen Wayne, the daughter-in-law of John Wayne and the current president of Batjac Productions, was asked about a DVD. "Im not sure who owns Good-bye, My Lady -- it might be Warner Brothers. Its a charming story and it should be released," she said. 

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 