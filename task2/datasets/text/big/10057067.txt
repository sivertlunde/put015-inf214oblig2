A Prize of Gold
{{Infobox film
| name           = A Prize of Gold
| image          = APrizeofGold1S.jpg
| caption        = Theatrical poster
| producer       = Irving Allen Albert R. Broccoli Phil C. Samuel
| based on       =  
| director       = Mark Robson
| writer         = Robert Buckner John Paxton George Cole|
| music          = Malcolm Arnold
| cinematography = Ted Moore
| editing        = William Lewthwaite
| studio         = Warwick Films
| distributor    = Columbia Studios
| released       = October 14, 1955
| country        = United Kingdom
| runtime        = 100 minutes English
}}
 1955 Warwick Films Technicolor heist film directed by Mark Robson partly filmed in West Berlin. The film stars Richard Widmark as a United States Air Force Air Police Master Sergeant motivated by love and compassion to begin a life of crime. It was based on the 1953 novel by Max Catto.

==Plot==
Master Sergeant Joe Lawrence (Richard Widmark) is stationed in Berlin shortly after the end of World War II where he falls in love with Maria (Mai Zetterling), a refugee.
 Corps of George Cole) hijack the plane.

While the robbery goes off almost as planned, the three participants begin to have second thoughts about what to do with their ill-gotten gains. 

==Cast==
* Richard Widmark as Sergeant Joe Lawrence
* Mai Zetterling as Maria
* Nigel Patrick as Brian Hammell George Cole as Sergeant Roger Morris
* Donald Wolfit as Alfie Stratton
* Joseph Tomelty as Uncle Dan Watson
* Andrew Ray as Conrad
* Karel Stepanek as Dr. Zachmann Robert Ayres as Tex
* Eric Pohlmann as Fischer
* Olive Sloane as Mavis
* Alan Gifford as Major Bracken  
* Ivan Craig as British Major  
* Harry Towb as Benny

Joan Regan sings the title song.

==Production==
Filming took place at Shepperton Studios. "These Are the Facts", Kinematograph Weekly, 31 May 1956 p 14 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 