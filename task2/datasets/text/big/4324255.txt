Dr. Otto and the Riddle of the Gloom Beam
 
{{Infobox film
| name = Dr. Otto and the Riddle of the Gloom Beam
| image = DrOttoandRiddleOfGloom.jpg
| caption =
| director = John R. Cherry III
| writer = John R. Cherry III
| starring = Jim Varney Glenn Petach
| producer = Jerry Carden John R. Cherry III GoodTimes Home Video
| released =  
| runtime = 92 min.
| country = United States English
| budget =
}}
 GoodTimes Home Video. It was shot in Fall Creek Falls State Park, Boxwell Scout Reservation, and Nashville, Tennessee.

==Plot synopsis==
Dr. Otto is a mysterious villain with a hand growing on top of his head. He is plotting world domination using his gloom beam, which can delete matter or memory and has the power to destroy the world. The bumbling hero Lance Sterling is the only one who can stop him, but his only clue is a riddle which reads: "When the money is scrambled to the very last cent, riots and hatred soon will commence. When all the worlds commerce will be put in a bind, from the evil that lurks where the sun never shines. It is I, Dr. Otto von Schnick -ick-ick-ick, who has played on you this trick-ick-ick-ick." "But whos Dr. Otto?" you may well ponder, while all your magnetic cash is squandered. Its he who had an eye, and yet couldnt see. Its he who served bouillabaisse, when he was a she. Its he who gambled with brains, and a gun. Its he who had all, and yet had none. And to stop this horrible twisted trick, just exchange the poles of old Saint Nick. And if that doesnt do to save the day, put another quarter in and try another play." 

Soon Lance and his assistant Doris drive by Dr Ottos lair when they have car trouble, caused by Dr. Ottos gloom beam. They set off to find a phone to call for a mechanic. They eventually  wander into a day care, run by Dr. Otto, which trains the kids to be soldiers. After a few adventures they finally find Ottos lair and have a fight with the doctor. The lair explodes and it all appears to be over, though Dr. Otto apparently escaped. Lance and his friends push the car to a gas station, where the owner turns out to be Dr. Otto, disguised as Ernest P. Worrell. Lance asks for gas, but Dr. Otto replies they didnt have any. Lance and gang push the car off screen and Dr. Otto lifts his hat to reveal a hand on top of his head and yells "Have a good day, knowwhatimean!"

==Cast==
*Jim Varney - Ernest P. Worrell, Dr. Otto, Rudd Hardtact and others 
*Glenn Petach  - Ottos Head Hand  
*Myke R. Mueller -  Lance Sterling 
*Jackie Welch  -  Doris
*Daniel Butler  -  Slave Willie, Kegler
*Esther Huston  -  Tina  
*Henry Arnold  -  Bank Officer 
*Bill Byrge   -  Gas Station Attendant 
*Mac Bennett  -   V.P. # 1  
*David Landon -  Bank President Rutherford 
*Mary Jane Harvill - Lances Mom 
*Winslow Stillman - Lances Dad 
*Irv Kane - Herr Vonschnick 
*Leslie Potter - Madame Vonschnick

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 