Prey (2009 film)
 
 
{{Infobox film
| name           = Prey
| image          = Prey Official Poster.jpg
| caption        = Official Australian Poster
| director       = George T. Miller
| producer       = Robert Lewis Galinsky Elizabeth Howatt-Jackman
| writer         = John V. Soto George T. Miller Andrew Topp Jesse Johnson Ben Kermode Natalie Walker Christian Clark Kristin Sargent Nicholas Bell Stephen Beck Jacqueline Steward Zachary Schaefer Taylor Johnson
| music          = Dale Cornelius
| cinematography = Andrew Topp
| distributor    = Top Cat Films
| released       =  
| runtime        = 
| country        = Australia
| language       = English 
| gross = A$744 (Australia)   accessed 15 November 2012 
}} American Jesse Jesse Johnson. by Mike Catalano,  , 2 October 2008, "Prey for shower". Retrieved 10 December 2008  It grossed $744 at the box office. 

==Plot summary==
In April 1987, two North Americans disappeared in the West Australian desert on a 4WD holiday. They were never seen alive again. Their abandoned vehicles and unused supplies were found in sand dunes near an Aboriginal sacred site less than an hour away from the closest town. Two years later, in May 1989, the two men were both found dead of natural causes, on the same day, 1,000 miles apart back in North America.

Twenty years after the original incident, 3 couples who set out on a surfing trip are lured into the same desert area, by a strange local whose master needs fresh victims to consume. Preconceived assumptions about friendship, undiscovered sexual liaisons, and false leadership come apart as the three couples realize that the vacation is over.

==Cast==
* Natalie Bassingthwaighte as Kate  Jesse Johnson as Gus  Natalie Walker as Ling 
* Ben Kermode as Matt 
* Christian Clark as Jason 
* Kristin Sargent as Annika 
* Nicholas Bell 
* Pamela Shaw as Morgan Weismann
* Joe Hachem as Motel Operator 
* Geire Kami as Patient 
* Jennifer Hansen as Waitress 
* Suhayb Lahdo as The Man
* Dawn Klingberg as Kora
* Stephen Beck as Pianist 
* Jacqueline Steward as Confused Girl 
* Zachary Schaefer as Bemused Pedestrian 
* Taylor Johnson as Internet Enthusiast  

==Production==
The outback had to be re-created in a Melbourne warehouse to save filming costs.   In addition to a perforated eardrum which caused filming dates to be rescheduled, lead star Natalie Bassingthwaighte injured her ankle twice during filming, causing hassles regarding the physicality of her role.  There was friction between the producers and director George T. Miller due to last minute script changes by Miller.  Also, investor issues plagued the shoot and at one point the line-producers were hinting they were ready to walk. Bassingthwaighte was used in a lesbian shower that raised great controversy, but that will make it to the final release.    However, the medias spin on the scene was very misguided and the actual scene in the movie contains lesbian interaction but no nudity. 

==Release==
The film premiered on 5 May 2009 in Sydney and Melbourne,  but he was released in a theatrical release on 9 May 2009 in Australia. 

===DVD release===
The film is scheduled to have a U.S. DVD release on 13 July 2010 from Xenon Pictures under the new title "The Outback." 

==See also==
*Cinema of Australia

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 