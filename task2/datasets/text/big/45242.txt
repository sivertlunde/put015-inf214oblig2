Reds (film)
 
{{Infobox film
 | name           = Reds
 | image          = Redsposter.jpg
 | image_size     =
 | caption        = movie poster
 | director       = Warren Beatty
 | producer       = Warren Beatty
 | screenplay     = Warren Beatty Trevor Griffiths
 | based on       =  
| starring       = {{plainlist|
* Warren Beatty
* Diane Keaton
* Edward Herrmann
* Jerzy Kosinski
* Jack Nicholson
* Paul Sorvino
* Maureen Stapleton
}}
 | music          = Stephen Sondheim Dave Grusin
 | cinematography = Vittorio Storaro Craig McKay
 | studio         = Barclays Mercantile Industrial Finance JRS Productions
 | distributor    = Paramount Pictures
 | released       =  
 | runtime        = 194 minutes
 | country        = United States
 | language       = English Russian German
 | budget         = $32 million
 | gross          = $50,000,000    
}} John Reed, journalist and Russian Revolution in his book Ten Days That Shook the World. Beatty stars in the lead role alongside Diane Keaton as Louise Bryant and Jack Nicholson as Eugene ONeill.
 Roger Baldwin (1884–1981), and the American writer Henry Miller (1891–1980), among others.
 Academy Award Best Director Best Picture, Best Actor, Best Actress, Best Supporting Best Supporting On Golden Pond and Nicholson to John Gielgud for Arthur (1981 film)|Arthur. Beatty was also nominated, along with co-writer Trevor Griffiths, for Academy Award for Best Original Screenplay, but lost to Chariots of Fire. Warren Beatty became the third person to be nominated for Academy Awards in the categories Best Actor, Director and Original Screenplay for a film nominated for Best Picture. This was done previously by Orson Welles for Citizen Kane and Woody Allen for Annie Hall.
 Ten Top Ten" – the best ten films in ten "classic" American film genres – after polling over 1,500 people from the creative community. Reds was acknowledged as the ninth best film in the epic genre.  

==Plot== John Reed and Louise Bryant from their first meeting to Reeds final days in 1920 Russia. Interspersed throughout the narrative, several surviving witnesses from the time period give their recollections of Reed, Bryant, their colleagues and friends, and the era itself. A number of them have mixed views of Bryant and her relationship with Reed.
 Democratic Convention. events of the 1917 Revolution.

The second part of the film takes place shortly after the publication of Ten Days that Shook the World. Inspired by the idealism of the Revolution, Reed attempts to bring the spirit of Communism to the United States, because he is disillusioned with the policies imposed upon Communist Russia by Grigory Zinoviev and the Bolsheviks. While attempting to leave Europe, he is briefly imprisoned and interrogated in Finland. He returns to Russia and is reunited with Bryant at the railway station in Moscow. By this point, Reed is growing progressively weaker as a result of his kidney disorder. Bryant helps nurse the ailing Reed, who dies.

==Cast== John Silas "Jack" Reed
* Diane Keaton as Louise Bryant
* Edward Herrmann as Max Eastman
* Jerzy Kosinski as Grigory Zinoviev
* Jack Nicholson as Eugene ONeill
* Paul Sorvino as Louis C. Fraina
* Maureen Stapleton as Emma Goldman
* Nicolas Coster as Paul Trullinger
* William Daniels as Julius Gerber Liberal Club
* Ian Wolfe as Mr. Partlow
* Bessie Love as Mrs. Partlow
* MacIntyre Dixon as Carl Walters
* Pat Starr as Helen Walters
* Eleanor D. Wilson as Margaret Green Reed (mother)
* Max Wright as Floyd Dell
* George Plimpton as Horace Whigham
* Harry Ditson as Maurice Becker
* Leigh Curran as Ida Rauh
* Kathryn Grody as Crystal Eastman
* Dolph Sweet as Big Bill Haywood
* Gene Hackman as Pete Van Wherry
* Nancy Duiguid as Jane Heap
* Dave King as Allan L. Benson
* Roger Sloman as Vladimir Lenin
* Stuart Richman as Leon Trotsky
* Oleg Kerensky as Alexander Kerensky Senator Overman
* Jan Triska as Karl Radek

==Production==
Warren Beatty came across the story of John Reed in the mid-1960s and executive producer and film editor Dede Allen remembers Beatty mentioning making a film about Reeds life as early as 1966. Originally titled Comrades, the first script was written by Beatty in 1969,  but the process stalled. In 1976, Beatty found a suitable collaborator in Trevor Griffiths who began work but was delayed when his wife died in a plane crash.    The preliminary draft of the script was finished in 1978, but Beatty still had problems with it. Beatty and Griffiths spent four and a half months on fixing it, though Beattys friend Elaine May would also collaborate on polishing the script.
 Heaven Can Wait that producing a film alone is a difficult task. He briefly considered John Lithgow for the part of John Reed because the two looked similar in appearance. Eventually, however, Beatty decided to act in the film and direct it himself. Jack Nicholson was cast as Eugene ONeill over James Taylor and Sam Shepard. 

When principal photography began in August 1979 the original intention was for a 15-to-16-week filming shoot, but it ultimately took one whole year to just shoot the film. The process was slow because it was shot in five different countries and at various points the crew had to wait for snow to fall in Helsinki (and other parts of Finland), which stood in for the Soviet Union, and for rain to abstain in Spain. Beatty would also not stop the camera between takes and would have it continuously roll. He also insisted on a large number of takes. Paul Sorvino said he did as many as 70 takes for one scene and actress Maureen Stapleton had to do 80 takes of one particular scene which caused her to say to Beatty, "Are you out of your fucking mind?" 

Beatty and Keatons romantic relationship also began to deteriorate during the filming. Peter Biskind wrote about the making of Reds, "Beattys relationship with Keaton barely survived the shoot. It is always a dicey proposition when an actress works with a star or director – both, in this case – with whom she has an offscreen relationship. ... Keaton appeared in more scenes than any other actor, save Beatty, and many of them were difficult ones, where she had to assay a wide range of feelings, from romantic passion to anger, and deliver several lengthy, complex, emotional speeches." George Plimpton once observed, "Diane almost got broken. I thought   was trying to break her into what Louise Bryant had been like with John Reed." Executive producer Simon Relph adds, "It must have been a strain on their relationship, because he was completely obsessive, relentless." 

The editing process began in spring of 1980 with as many as 65 people working on editing down and going over approximately two and a half million feet of film.  Post-production ended in November 1981 more than two years after the start of filming. Paramount stated that the final cost of the film was $33.5 million, which would be the rough equivalent of around $80 million today. 

==Soundtrack==
The film introduced the song "Goodbye for Now," written by Stephen Sondheim. The song was later recorded by Barbra Streisand for The Movie Album (2003).

==Witnesses== New York Times film critic Vincent Canby. 

To gain perspective on the lives of Reed and Bryant, Beatty began filming the "witnesses" as early as 1971.  Some of them are very well known, others less so. As well as their being listed in the opening credits, American Film magazine identified the witnesses in its March 1982 issue. 

* Jacob Bailin, labor organizer
* Roger Nash Baldwin, founder, American Civil Liberties Union
* John Ballato, early socialist
* Harry Carlisle, writer, teacher Masses
* Andrew Dasburg, painter
* Tess Davis, cousin of Louise Bryants first husband
* Will Durant, historian
* Blanche Hays Fagen, with Provincetown Players Hamilton Fish, Congressman, Harvard classmate of John Reed
* Dorothy Frooks, "Recruiting girl," World War I Masses
* Emmanuel Herbert, student in Petrograd, 1917–18 George Jessel, entertainer
* Oleg Kerensky, son of Alexander Kerensky
* Isaac Don Levine, journalist, translator for Reed
* Arthur Mayer, film historian, Harvard classmate of Reed also film distributor
* Henry Miller, novelist
* Adele Nathan, with Provincetown Players
* Scott Nearing, sociologist, pacifist
* Dora Russell, delegate to Comintern
* Adela Rogers St. Johns, journalist
* George Seldes, U.S. journalist in Moscow
* Art Shields, political activist Jessica Smith, political activist
* Arne Swabeck, member, Communist Labor Party
* Bernadine Szold-Fritz, journalist
* Galina von Meck, witness to Russian Revolution
* Heaton Vorse, son of a Provincetown playwright Will Weinstone,organizer, U.S. Communist Party
* Rebecca West, feminist, author
* Lucita Williams, wife of a Lenin biographer

==Reception and response== thirteenth highest grossing picture of 1981, grossing $50,000,000 in the United States.  Beatty later remarked that the film "made a little money" in box office returns.
 review aggregate website Rotten Tomatoes.     In an interview with IMDb regarding his recently released film Interstellar (film)|Interstellar (2014), director Christopher Nolan stated that Reds strongly influenced some of its scenes.   

==Awards and honors== Academy Awards for the following:       Best Actress in a Supporting Role (Maureen Stapleton playing Emma Goldman) Best Cinematography (Vittorio Storaro) Best Director (Warren Beatty)
The film received the following nominations:  Best Actor in a Leading Role (Warren Beatty) Best Actor in a Supporting Role (Jack Nicholson) Best Actress in a Leading Role (Diane Keaton) Best Art Direction-Set Decoration (Richard Sylbert, Michael Seirton) Best Costume Design Best Film Editing Best Picture Best Sound (Dick Vorisek, Tom Fleischman and Simon Kaye) Best Writing, Screenplay Written Directly for the Screen. 

==Notes==
 

==External links==
* 
*  
*  at Rotten Tomatoes
* ; Vincent Canby, The New York Times (December 4, 1981)
*  ; A.O. Scott, The New York Times (October 4, 2006)

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 