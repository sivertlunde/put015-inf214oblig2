It's Pat
{{Infobox film
| name           = Its Pat
| image          = ItsPat.jpg
| image_size     = 215px
| alt            = A large person stands naked in the center of the poster, with a big pink question mark covering their body
| caption        = Theatrical release poster
| director       = Adam Bernstein
| producer       = Charles B. Wessler
| screenplay     =   
| based on       =  
| starring       = Julia Sweeney Dave Foley Charles Rocket Ween Kathy Griffin
| music          = Mark Mothersbaugh
| cinematography = Jeff Jur
| editing        = Norman Hollyn
| studio         = Touchstone Pictures
| distributor    = Buena Vista Pictures
| released       =  
| runtime        = 78 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $60,822   
}} androgynous misfit whose gender is never revealed.

Dave Foley plays Pats partner, Chris, and Charles Rocket, another SNL alumnus, plays Pats neighbor, Kyle Jacobsen.

==Plot== stalking Pat. Kyle sends in a tape of Pat performing karaoke to a TV show called Americas Creepiest People, bringing Pat to the attention of the band Ween, who feature Pat in one of their performances; Pat plays the tuba. When Pat learns that Ween intended to only use Pat for one gig, Pat breaks up with Chris. 

Pat discovers that their laptop (containing their diary) has been stolen. Later, it turns out that Kyle stole Pats diary. Kyle tries to coerce Pat into revealing the computers password, so he can access the files. Pats only answer is that the word is in the dictionary. Kyle then begins to type in in every single word in the dictionary. 
 therapist and host of a radio talk show. When Pat gives acerbic reactions to call-in listeners, the station fires Kathy and replaces her with Pat. 

Kyle ends up going through the entire dictionary until he reaches the last word, "zythum" (an Egyptian malt beer), which is the password. After reading through the diary, he discovers no new information in regards to Pats gender, and finally snaps.

Kyle calls Pat on Pats radio show, and tells Pat to meet him at the Ripleys Believe It Or Not!#Museums|Ripleys Believe It or Not! Museum, stating that this is the only chance for Pat to retrieve the laptop. Pat arrives to find Kyle dressed exactly like Pat. Kyle demands that Pat strip naked, and Pat runs off into a Ween concert. After Kyle corners Pat on a catwalk, Pat falls, and Pats clothes get caught on a hook. This tears off Pats pants and lowers Pat in front of the cheering audience, though Pats genitals are not revealed to Kyle nor the viewer. Kyle is subsequently taken away by security guards. Pat then runs to see Chris, just as Chris is leaving on an ocean liner. In an epilogue, Pat and Chris get married.

==Cast== Pat Riley
* Dave Foley as Chris
* Charles Rocket as Kyle Jacobsen
* Kathy Griffin as Herself
* Julie Hayden as Stacy Jacobsen
* Timothy Stack as Doctor
* Mary Scheer as Nurse
* Beverly Leech as Mrs. Riley
* Tim Meadows as KVIB-FM manager
* Phil LaMarr as Stage manager
* Larry Hankin as Postal supervisor
* Kathy Najimy as Tippy
* Jerry Tondo as Sushi chef Dean and Gene Ween as themselves

==Production==
The film was written by Sweeney, Jim Emerson (Sweeneys friend from their days with    While at the Groundlings, Emerson suggested that the character Pat, at the time a "character based on annoying co-workers who dont leave you alone", be made androgynous. 

Three months before the films release, Sweeney commented on her initial reluctance to do a film based on Pat:  , a May 1994 article from The New York Times 

:I resisted it completely. I just didnt know how we could make it last for two hours. But 20th Century Fox was really keen; our producer was really keen. So we thought, OK, well write the script. And after three months, we fell madly in love with the script. Unfortunately, Fox did not.

Touchstone Pictures decided to produce the film, after Fox bowed out. 

Quentin Tarantino revealed that he was an uncredited writer on the script.  

==Reception==
Its Pat received overwhelmingly negative reviews, and has a rare 0% on Rotten Tomatoes based on 11 reviews. 

Variety (magazine)|Variety magazine called the film "shockingly unfunny", noting that Sweeney had "perversely turned the relatively harmless TV character into a boorish, egotistical creep for the bigscreen"; the films "only really funny bit is Sexual Personae author Camille Paglia, deftly parodying herself, commenting on the significance of Pats androgyny." 

The film opened in only three cities  , a December 1996 article from Time (magazine)|Time magazine  (33 theaters ). Its total gross was just $60,822. As a result, the film was pulled from theaters after its opening weekend.

=== Coincidental tragedy and aftermath ===
In an unfortunate coincidence, the weekend that Its Pat opened to  ." 

The subsequent events, including the death of her brother, became the subject of Sweeneys One person show|one-woman show and film, titled God said "Ha!". 

===Awards and nominations===
  
Its Pat was a multiple nominee at the 16th Golden Raspberry Awards, though the films cast and crew lost in every category to Showgirls:  Worst Actress - Julia Sweeney Worst New Star - Julia Sweeney Worst Picture - Charles B. Wessler Worst Screen Couple - Dave Foley and Julia Sweeney Worst Screenplay - Jim Emerson, Stephen Hibbert, and Julia Sweeney

==Soundtrack==
No soundtrack album was released. The songs from Its Pat are listed below as shown within the films credits: 
# "Its Pat Theme" - Christina Zander, Julia Sweeney, Cheryl Hardwick
# "Walz Pompadour" (written by Tom Elliot)
# "Poem of Crickets" (written by 長沢 勝俊 (Katsutoshi Nagasawa) Gary Fletcher, Paul Jones, Dave Kelly, Tom McGuinness, Bob Townsend
# "The Cool Look" - Johnny Hawksworth
# "Brain Women" - Mark Mothersbaugh
# "Everybody Loves Somebody" - Julia Sweeney
# "Dude (Looks Like a Lady)" - Aerosmith
# "Le Freak" - Julia Sweeney
# "Paero" - Phillippe Lhommt, Jacques Mercier
# "Pork Roll, Egg and Cheese" - Ween
# "Hows It Gonna Be" - Dead Milkmen
# "Bring It to Me" - Collective Thoughts
# "Dont Get 2 Close (2 My Fantasy)" - Ween
# "Youre the Best Thing That Ever Happened to Me" - Gladys Knight & the Pips

==References==
 
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 