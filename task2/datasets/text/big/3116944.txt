Trapped (2002 film)
{{Infobox film
| name           = Trapped
| image          = Trapped film.jpg
| caption        = Theatrical poster
| director       = Luis Mandoki
| producer       = Mimi Polk Gitlin Luis Mandoki
| writer         = Greg Iles
| starring       = Charlize Theron Courtney Love Stuart Townsend Kevin Bacon Dakota Fanning Pruitt Taylor Vince
| music          = John Ottman
| cinematography = Frederick Elmes Piotr Sobocinski
| editing        = Gerald B. Greenberg
| distributor    = Columbia Pictures
| released       = September 20, 2002
| runtime        = 106 minutes
| country        = United States Germany English
| budget         = $30 million
| gross          = $13,414,416 
}} crime Thriller thriller film bestselling novel and directed by Luis Mandoki. It stars Charlize Theron, Courtney Love, Stuart Townsend, Kevin Bacon, Dakota Fanning and Pruitt Taylor Vince, and was released on September 20, 2002.

==Plot==
Joe Hickey (Kevin Bacon) is a man who is apparently in the business of kidnapping rich peoples children for ransom with his wife Cheryl (Courtney Love) and cousin Marvin as his accomplices. Will Jennings (Stuart Townsend), the father/husband in a typical happy family, is a research physician who has just had his big career break by patenting a new anesthetic drug. The mother/wife, Karen Jennings (Charlize Theron), is a stay-at-home mother who was previously a nurse.

The couples daughter Abby (Dakota Fanning) is kidnapped by Hickey and Marvin. Meanwhile Cheryl keeps Will at gunpoint and tells him about his daughters kidnapping after he just gets out of a ceremony in which he is honored for his career. The movie then follows the tense situation lived by the couple, separated by the bandits and kept in the dark as to what the outcome of their predicament is to be. It is later revealed that Cheryl and Joe kidnapped Abby because they believe that Will killed their daughter Katie who had tumour. Will says that it was the surgeon who killed her because he was unable to stop her bleeding during the operation. Cheryl says that Hickey wont believe what she says about this. The situation is worsened by Abbys asthma.

==Cast==
*Charlize Theron - Karen Jennings
*Courtney Love - Cheryl Hickey
*Stuart Townsend - Dr. Will Jennings
*Kevin Bacon - Joe Hickey
*Dakota Fanning - Abby Jennings
*Pruitt Taylor Vince - Marvin
*Colleen Camp - Joan Evans
*Steve Rankin - Hank Ferris Garry Chalk - Agent Chalmers
*Jodie Markell - Mary McDill
*Brigitte Svörwald - Angie McCoole
*Matt Koby - Peter McDill
*Gerry Becker - Dr. Stein
*Andrew Airlie - Holden
*Randi Lynne - Hotel Operator
*J.B. Bivens - Gray Davidson

==Reception==
Trapped received negative reviews from critics, as it currently holds an 18% rating on Rotten Tomatoes based on 56 reviews.

==Differences from the book==
*In the book, Joe wanted revenge because Will had "killed" his mother, whereas in the movie version Joe wanted revenge because he thought that Will had killed his child.
*In the book, Abby has diabetes, whereas in the movie, she suffers from asthma.
*In the book, Joes cousins name is Huey, whereas in the movie, his name is Marvin.

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 