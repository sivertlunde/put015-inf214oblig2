Who's Who in the Zoo
{{Infobox Hollywood cartoon
|cartoon_name=Whos Who in the Zoo
|series=Looney Tunes (Porky Pig)
|image=
|caption=
|director=Norman McCabe
|story_artist=Melvin Millar
|animator=John Carey
|background_artist=
|layout_artist=
|voice_actor=Mel Blanc
|musician=Carl Stalling, Milt Franklyn
|producer=Leon Schlesinger
|distributor=Warner Bros. Pictures The Vitaphone Corporation
|studio=Leon Schlesinger Productions
|release_date=February 14, 1942
|color_process=Black and White
|runtime=
|movie_language=English
}} 1942 Warner Bros. cartoon in the Looney Tunes series. It was directed by Norman McCabe, story by Melvin Millar, musical direction by Carl Stalling.

==Plot== Azusa Zoo". Some excerpts:

*In a comic "Rule of three (writing)#Comedy|triple", a timber wolf is shown, then a gray wolf, then a "Hollywood wolf" (a frequent reference in the 1940s WB cartoons).
*Other creatures include a "missing lynx", a "tortoise and the hair", "March hares" who march to a drumbeat, a down-on-his-luck "bum steer", an Indian elephant playing an American Indian, and a bald eagle wearing a toupee.
*There is also a running joke about a lion who is awaiting the arrival of the ice cream truck.
*An Alaskan Bear whos known for hugged his predator to death, picks up and starts hugging a little defenseless sheep. When the narrator told the bear to stop hugging the sheep, the sheep responded: "Oh, for goodness sake, mind your own business!"

==See also==
*Looney Tunes and Merrie Melodies filmography (1940–1949)
*List of Porky Pig cartoons

==Sources==
*Jerry Beck and Will Friedwald, Looney Tunes and Merrie Melodies, Owl Books, Henry Holt, 1989.

==References==
 
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 