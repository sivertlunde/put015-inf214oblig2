Manuelita (film)
{{Infobox film
| name=Manuelita
| caption= 
| image	=	Manuelita FilmPoster.jpeg
| director=Manuel García Ferré
| producer= Carlos Mentasti
| writer= Manuel García Ferré (writer) María Elena Walsh (story)
| starring= Rosario Sánchez Almada Pelusa Suero Enrique Conlazo Miguel Esteban Cecilia Gispert Susana Sisto 
| music= Néstor DAlessandro Roberto Lar María Elena Walsh 
| cinematography = 
| editing= Luis Busso
| studio=García Ferré Entertainment
| distributor= Columbia Pictures Buena Vista International  
| released= 
| runtime=86 minutes
| country=Argentina
| language=Spanish
| budget= 
| gross= 
}} Best Foreign Language Film submission at the 72nd Academy Awards, but did not manage to receive a nomination. 

Manuelita comes from a Manuelita la tortuga|childrens song of María Elena Walsh. Isa Cucinotta, co-curator of Castelo Rá-Tim-Bum (film)|Rá-Tim-Bum Castle, said that Manuelita was as famous in Argentina as Winnie the Pooh was in the United States. 
 Buena Vista Home Entertainment in the United States. 

==Plot==
The story begins with an old bird named the Patriarch of the Birds, who tells many other animals something that happened many years ago: The story of Manuela turtle. Manuelas story is told since she was in the egg had not hatched, her parents and grandfather were preparing the house and preparing for the arrival of Manuela, the new family member. When she hatched, Manuela was full of adventure and curiosity. A few years passed and Manuela grows and starts first grade with her two best friends, Dopi and Bartolito, the son of the headmistress. When they walk to school, a group of crooks, who are three dogs, bother them every time they go from school to their homes. At school, we meet Larguirucho the mouse, who makes another appearance in Manuel Garcia Ferres movie. The trio goes home as school finished, and the crooks again. They start to attack, but are stopped by Larguirucho by being chased away. That night, Manuelita has an imagination of Bartolito as a knight, and the crooks as a three headed dragons. The Patriarch tells that the years passed and Manuela turned into a beautiful teenager, and Bartolito is now a handsome find young man. Bartolito is in love with Manuela and confesses his feelings for her in a letter but when he tries to deliver, they discover that there is a fair circus Pehuajó, where Manuela, her parents, her grandfather, Bartolito and Dopi live. Since the family was going the fair, Bartolito decided to save the note for later and join them.

Seeing a giant balloon, Manuela asks permission to ride her mom but her mom and dad would not let her, but Grandpa convinced them other wise, since he rode in balloons in his young years. Larguirucho holds onto the ropes that held the balloon on the ground, but the wind carries it, along with Manuela, before they could climb. Bartolito tried the same and go with Manuela, but wasnt fast enough. They all decided get Manuela back no matter what happened. Manuela, at first, enjoying the trip but a big bird pops the balloon. Manuela falls in the middle of what appears to be the Atlantic Ocean. There, she is picked up by a boat of pigs pirates saw the box of the globe seems to be nothing inside, onto the ship believing that is a treasure but, finding there no treasure but it is Manuela. Not knowing what to do with her, the captain orders his men for her to be thrown in the brig of the ship until he decides what to do with. Manuela starts cries until some mice appear and cheer her up with a song. They started to sing and dance waking up the captain, who orders his men to stop it. Manuela and the mice managed to escape outside. They are surrounded, but the weight of the pigs causes the boat to sink. Manuela and the mice manage to grab a board and swim, while the pigs are stranded on their sunk ship. The crew starts to dance the song the mice sang, much to the captains dismay and annoyance. Manuela and mice see something, believing that it is an island but is actually a giant old turtle water. The turtle takes them all to the nearest city that is Paris before departing further toward the blue.

There, Manuela and mice are separated and each goes their own ways. Looking for a place to sleep, Manuela stops to watch a show where just had a parade and gets to play was parading as models parade when he discovers a man named François (pronunciation of the French name François, i.e. Francisco), the owner of the building. While all this was happening, Bartolito, Dopi, Larguirucho and grandfather Manuela try to create a balloon to go look but none goes well. Thanks to François and the fashion designer Coco Liche (which is the representation of Coco Chanel ), who had also been the organizer of the parade, Manuela becomes a famous model and her family finds out when in a beauty shop, her mother is combing and see on the cover of a fashion magazine, amazing everyone, especially Bartolito. Finally, after several failed attempts, Larguirucho, Bartolito, Dopie and Grandpa they construct a fit to blow balloon. Larguirucho and Dopie climb onto the balloon while Bartolito and grandpa stay behind and look for a compess. However, by accident, they cut the ropes up the globe, leaving without Grampa and Bartolito, who is now even more heartbroken. They go looking for Manuela, and manage to defeat the same that popped the balloon. Manuela every month gives money to fransua to have it sent to their parents in Pehuajó and letters to Bartolito, but Francios always lies to say yes Manuela sent it, as he hides the money in his case and the letters in a desk. After several days of travel, Larguirucho and Dopie arrive in Paris, without knowing that Manuela is there, as the balloon floats away and start looking for a room where they can spend the night. While on the streets of Paris looking for Manuela, Lanky and Dopie meets several celebrities asking them if they saw Manuela, found the statue of The Thinker, and Carlos Gardel.

Meanwhile, Manuela discovers that François has lied to her about sending the noted, but doesnt know about the money cheating yet. Afraid he might get exposed, Francois decides to destroy Manuelas career. During a fashion show, Francois rips Manuela dress, but is discovered. Manuela soon finds Francois and discovers the money. In panic, Francois accidentally drops his bag of money and it flies out. He tries catch it all, but falls out of the balcony and onto a pole, presumed to be arrest for his crimes. Manuela leaves the agency, with only a few dollars, and gets lost in the great city of Paris, to wander and begins to dream of her past and happy life, wondering what the others will think. The next day, however, she soon gets the message when she heard music coming from under the bridge where she was walking and listening to a bow (the representation of Edith Piaf) Manuela singing the song and so is Larguirucho and Dopie playing the bandoneon under a bridge. They are see each other again and the three travel home on plane with the money Manuela has. When she finally returned to Pehuajó, Manuela reunites with her family who are overjoyed to see her. When Manuela asked about Bartolito, they tell her that Bartolito is now the master of the school, following after his mothers retirement, much to Manuelas happiness. She goes to school, after it ended with same kids they were if when they were kids(possibly their kids since it has been years and they had to get older to be parents) and finds Bartolito drawing two hearts, one above the other. She, to surprise him, draws an arrow through two hearts symbolizing the hearts of two of them. Bartolito was surprise and looks towards Manuela and was overjoyed as well. Manuela and Bartolito embrace, happy to see each other, and Larguirucho calls for a wedding celebration. The next scene is the marriage of Manuela with Bartolito. In this wedding, all of Pehuajó, including the families of Manuela and Bartolito, attend. Several of the most important cartoons of the companys director of the film appear as well, such as Anteojito, Oaky, Hijitus and Trapito as well as the Patriarch of the birds, revealing to have narrated up until the present, and telling audience that the couple had a bright future together. Manuela and Bartolito exit and get a cheer of the crowd, as they, for the first, kiss each other on the lips. Manuela and Bartolito start traveling in a hot air balloon, waving goodbye to everyone, to start their honeymoon as the credits start, the film ends and fades to black.

== Characters ==
Manuela- The protagonist of the movie. She was full of exploring when she was born. As a child, she had a red rose hat. As a grown up, she wore a yellow hat. When she arrive in Parris, she became model. After it was destroyed, she head back home, reunited with her family, and most of all, married Bartolito.

Bartolito- He is Manuelas childhood friend, then boyfriend in their grown years, and then husband. Hes a greaser and mechanic As a child, he had on a brown vest with a white collar and a blue hat. As a grown up, he wore on a full white jack on and wore grey pants and a gray hat. He became the new headmaster of the school following his mothers retirement by the time Manuela returned. He later married Manuela.

Dopi- Bartolitos best friend. He originally wore a red and orange shirt then wore red overalls. He also wears glasses, since he is a mole.

Mr. and Mrs. Turtle- Manuelas parents. They with their daughter and Grandpa.

Grandpa Turtle- Manuelas grandpa and Mrs. Turtles father. He is possibly a widow, since his wife isnt around.

The headmistress- Bartolitos mother. She originally ran the school until her retirement, leaving her son to take care of the school.

The dog crooks- They were the antagonist at the beginning of the movie. The like make fun of others mostly Manuela and Bartolito for be together. However, at the end, they somehow changed.

The seabird- One of the ocean antagonist. He enjoys attacking other and destroying things. He was taken out by Larguirucho.

Captain Pig- The second antagonist of ocean seen. He and his crew take Manuela hostage. After their ship sank, they were left stranded in the middle of nowhere.

The singing mice- They were on the ship and befriended Manuela. They cheered her up and managed to escape the ship. They left to travel their own way.

The old turtle- She has curls and a pink bonnet. She helped the Manuela and the mice to Paris. She late left and traveled off in the blue.

François- The antagonist of Paris. Hes a rat who owns model place. Hes sly, greedy, always lying, and used Manuela to make her famous. After he was found out, he presumed arrested.

Coco Liche- Shes in charge of the model parade. Shes a turkey with nervous affects and likes to go coco.

Larguirucho- He made his fourth appearance in Manuel Garcia Ferres movie. He works as the school janitor.

A lot of other animals appear in the movie without lines. Also, almost all of Manuel Garcia Ferres character

==References==
 

==External links==
* 

 
 
 
 
 
 