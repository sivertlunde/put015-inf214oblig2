The Bride (1985 film)
{{Infobox film
| name = The Bride
| image = Thebrideposter.jpg
| caption = film poster for The Bride.
| director = Franc Roddam Chris Kenny
| writer = Mary Shelley (novel Frankenstein) Lloyd Fonvielle
| starring = {{Plainlist| Sting
* Jennifer Beals
* Geraldine Page
* Clancy Brown Anthony Higgins
* David Rappaport
}}
| music = Maurice Jarre
| cinematography = Stephen H. Burum Michael Ellis
| distributor = Columbia Pictures
| released = August 16, 1985
| runtime = 118 minutes
| country = United Kingdom United States
| language = English
| budget =
| gross = $3,558,669   
}}
 Sting as Baron Charles Frankenstein and Jennifer Beals as Bride of Frankenstein (character)|Eva, a woman he creates in the same fashion as his infamous Frankensteins monster|monster. 

==Plot==
  Eva (Jennifer Beals) for the monster. Eva is physically identical to a human and lacking the deformities of the monster. As such, she is revolted by the monster and rejects him. This causes the monster to fly into a rage and destroy Frankensteins laboratory. Frankenstein, believing himself and Eva to be the only survivors, flees with her back to Castle Frankenstein. He tells everyone, including Eva, that she was an amnesiac he found in the woods. There he decides to take Eva for himself and pursues the goal of making her a perfect human mate. 

The monster, having survived, wanders into the countryside where he befriends a dwarf, Rinaldo (David Rappaport). During their travels, Rinaldo tries to teach and look out for the monster, something that the Baron has never done, by giving the monster care and more importantly a name, Viktor. Over the course of their travels, unknown to both Viktor and Eva, the two share a psychic link in which each will often feel what the other feels. Viktor and Rinaldo become involved with a circus owners assistant who bullies Viktor and kills Rinaldo  out of spite. When Viktor overhears that owner and the assistant arguing, he fights the assistant and kills him out of revenge and defense. But the circus owner gets the group to turn on him and calls him the murderer, causing him to flee.

While over time Eva does develop a mutual respect for her unknown creator, during this pursuit to make her civilized she becomes more independent. During her lessons, she becomes very attracted to a Captain Josef Schoden (Cary Elwes), much to the Barons personal disdain. He becomes more obsessive of Eva, even watching her in her sleep, leaving her very unsettled. Josef then comes in secret to meet Eva, causing her to become very attracted. Unknown to Eva, Viktor returns and the two encounter one  another, sharing a tender moment of friendship. Though Eva is curious to know him, Viktor decides to hide the truth from her.

The Baron becomes extremely possessive of Eva as she grows more independent. After their encounter Viktor goes to a peddler to offer Eva jewelry to win her affection. Out of empathy for Viktor for giving him a lot of gold, he returns some of the money. Viktor nearly makes his way to Evas room to show her his gifts, but leaves after seeing Eva being lavished in riches the Baron gave her. That night Eva and the Baron attend a masquerade and she sneaks away to be with the Captain. The Baron finds them together and is shown silently enraged with jealousy. 

Viktor, meanwhile, is captured by the circus group who want retribution. The Barons obsession escalates as he discovers Eva had sneaked away to see the Captain at night. The Baron confronts them, but much to Evas shock the Captain only toys with her. Back at the castle, the Baron announces she belongs to him and demands that she obeys him. As they argue, the truth is revealed about Evas origins. Shes distraught as she realizes the truth through his notes. During her moment of horror at the truth, Viktor feels her pain.

When the Baron tells her he believes that the monster is dead, he reveals that he intends to have her and she continues to reject him. He tries many times to force himself upon Eva, much to her disgust. Viktor senses Evas distress and breaks free of his imprisonment and rushes back to the castle to save her. The Barons obsession rises and he breaks into Evas room to rape her. But Viktor comes in time and fights off his creator and during the struggle the Baron falls to his death.

The monster and Eva meet with each other, introducing himself to her with his name Viktor. Reunited, they head off to Venice; something that Rinaldo dreamed of seeing.

== Cast == Sting as Baron Charles Frankenstein
* Jennifer Beals as Eva
* Clancy Brown as Viktor, the Monster
* Geraldine Page as Mrs. Baumann
* David Rappaport as Rinaldo the Dwarf Anthony Higgins as Clerval
* Alexei Sayle as Magar
* Veruschka von Lehndorff as Countess
* Quentin Crisp as Dr. Zalhus
* Cary Elwes as Captain Josef Schoden
* Phil Daniels as Bela
* Timothy Spall as Paulus

==Production notes== dwarf in The Black The Invisible Woman (1940).

Some scenes were shot amidst the statuary at the Gardens of Bomarzo in Lazio, Italy.

==Reception==
The film earned negative reviews from critics and holds a 22% rating on Rotten Tomatoes. Jennifer Bealss performance in the film earned her a Razzie Award nomination for Worst Actress.

==Release==
Columbia Pictures released the film theatrically on August 16, 1985 and it grossed $3,558,669 at the domestic box office. 

The film was released on DVD by Sony Pictures Home Entertainment in 2001. 

==See also==

* List of films featuring Frankensteins monster

==References==
 

==External links==
 
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 