Giordano Bruno (film)
{{Infobox film
| name = Giordano Bruno
| image = Giordanobrun01.jpg
| caption =
| director = Giuliano Montaldo
| writer =
| starring =
| music = Ennio Morricone
| cinematography = Vittorio Storaro
| editing = Antonio Siciliano
| producer =
| distributor =
| released =  
| runtime =
| awards =
| country = Italy
| language =
| budget =
}} 1973 Cinema Italian biographical film|biographical-drama film directed by Giuliano Montaldo. 

The film chronicles the last years of life of the philosopher Giordano Bruno from 1592 to his execution in 1600. 

== Cast ==
*Gian Maria Volonté: Giordano Bruno
*Charlotte Rampling: Fosca
*Renato Scarpa: Fra Tragagliolo
*Mathieu Carrière: Orsini
*Hans Christian Blech: Sartori
*Giuseppe Maffioli: Arsenalotto Mark Burns: Bellarmino
*Massimo Foschi: Fra Celestino
*Paolo Bonacelli
*José Quaglio 
*Corrado Gaipa

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 
 


 