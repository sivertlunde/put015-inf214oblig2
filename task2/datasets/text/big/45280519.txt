Homeward Bound (1923 film)
{{Infobox film
| name           = Homeward Bound
| image          = 
| alt            = 
| caption        = 
| director       = Ralph Ince
| producer       = Adolph Zukor Jack Cunningham Paul Sloane
| starring       = Thomas Meighan Lila Lee Charles S. Abbe William P. Carleton Hugh Cameron Gus Weinberg
| music          = 
| cinematography = Ernest Haller
| editing        = 
| studio         = Famous Players-Lasky Corporation
| distributor    = Paramount Pictures
| released       =  
| runtime        = 70 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 lost  drama silent Jack Cunningham Paul Sloane. The film stars Thomas Meighan, Lila Lee, Charles S. Abbe, William P. Carleton, Hugh Cameron and Gus Weinberg. The film was released on July 29, 1923, by Paramount Pictures.  

==Plot==
 

== Cast ==
*Thomas Meighan as Jim Bedford
*Lila Lee as Mary Brent
*Charles S. Abbe as Rufus Brent 
*William P. Carleton as Rodney
*Hugh Cameron as Murphy
*Gus Weinberg as Captain Svenson
*Maude Turner Gordon as Mrs. Brannigan
*Cyril Ring as Rufus Brent Jr.
*Katherine Spencer as Clarissa Wynwood

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 

 