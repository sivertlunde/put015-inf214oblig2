Echoes of Innocence
{{Infobox Film |
  name     = Echoes of Innocence|
  image          = Echoes of Innocence.jpg |
  caption  = Echoes of Innocence|
  writer         = Nathan Todd Sims  |
  starring       = Sara Simmonds  Jake McDorman|
  director       = Nathan Todd Sims |
  music   = Brad Sayles |
  producer       = Clayton Coblentz |
  released   = September 9, 2005|
  runtime        =PG 13|
  runtime        = 114 Minutes |
  language = English |
  
}}
Echoes of Innocence is a Suspense/Adventure independent film from studio Lifesize Entertainment. It was released on video on March 28, 2006.

==Synopsis== Romans 8:18: I consider that our present sufferings are not worth comparing with the glory that will be revealed in us.

It has been five years since Sarahs (newcomer Sara Simmonds) soulmate Christopher (Cody Linley) mysteriously disappeared, riding off in a limousine without explanation. The two had become engaged at the age of twelve, planning to marry when both were eighteen. Sarah has kept to that commitment, believing that Chris will return. She is a devotee of Joan of Arc and has begun to experience her own heavenly visions and voices. Recently, these visions have grown in intensity. Their content mostly consists of premonitions of trouble or disaster.
 paranoid schizophrenic who berates Sarah constantly; she lives instead with her grandmother, an inveterate television-watcher who takes no interest in her. She prays alone in an abandoned chapel in a nearby forest, and confides in a Catholic priest, perhaps with an intent to convert to that faith, although this is never explicitly stated. She unwisely reveals her paranormal experiences to classmates, which in combination with her refusal to keep company with boys has made her something of a high school outcast. She does have at least one close girl friend, and is to play Titania in the school production of A Midsummer Nights Dream.

High school reporter Dave (Jake McDorman), a relative newcomer to the school, is intrigued by Sarah and determined to understand her fascination with the "virgin maid" - Joan of Arc. As Sarahs trust in Dave grows, she leads him to her chapel where she reveals the nature of her spiritual encounters. on the pretext of writing a feature about her for the school paper, Dave questions her thoroughly and investigates her background, discussing details of her life with classmates.

Meanwhile, rebellious Alec (Matt Vodvarka) desires Sarah for himself, viewing her virginity as a prize conquest. He is also a member of the trenchcoat mafia, a satanic figure who plants bombs around the school. His goals seem to be to have sex with Sarah and kill everyone else.
 possessed by a demon and attempts to attack Sarah and murder Dave, but the pair are saved by a gigantic bull which appears out of nowhere and is possibly of heavenly origin. At the same time, Dave reveals his own secret; he is Chris. He has been lying to Sarah, leading her down the path for months, because he wanted ironclad proof that she had kept her commitment to him. Also, he wants her to love him as he is now, not as the image of the twelve-year-old she remembers. No explanation is given for his sudden disappearance, but it is hinted that he comes from an extremely wealthy family. The film ends with Dave/Chris and Sarah completing their vows and living as husband and wife.

==External links==
*  

 
 
 
 

 