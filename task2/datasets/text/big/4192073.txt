RuPaul Is: Starbooty!
{{Infobox Film |
| name        = RuPaul Is: Starbooty!
| image       = 
| writer      = Jon Witherspoon
| starring    = RuPaul, Larry Tee, Lady Bunny
| director    = Jon Witherspoon
| producer    = Jon Witherspoon
| distributor = Funtone Records
| released    = 1987
| runtime     = Approximately 60 minutes
| language    = English
| music       = Songs by RuPaul
}}

RuPaul is: Starbooty! is a Low budget|low-budget underground film trilogy starring and co-produced by a then-unknown drag queen RuPaul. Because it was produced on a very low budget, various posters, advertisements and video covers alternate the spelling between "Starbooty" and "Starrbooty".

==Plot==
Filmed on a zero-dollar budget, the movies are a pastiche of 1960s blaxploitation films. RuPaul stars as Starbooty, a crime fighting federal agent who disposes of villains while getting entangled in romantic liaisons. At the time, RuPaul was still participating in a type of drag known as genderfuck; as such his appearance generally is of a man with feminine makeup and clothes, but no padding or taping to make the body look female.

Outside the canon of the actual films, a sketch on RuPauls VH1 talk show continued the storyline of the character of Starbooty with a preview for an alleged new film called "Starbooty in: Take That You Honky Bitch".

In 2006 RuPaul used his blog to discuss a film he was in the process of making which he called "The Untitled RuPaul Movie". The film was initially titled Starrbooty: Reloaded but ultimately is just called Starrbooty. The film follows similar plot lines from the previous ones, but contains more sexually explicit content.

When originally produced, cheaply manufactured video cassettes were passed out on the streets and at clubs. Currently they can be purchased together on one video cassette and DVD at the  , a trademark established by Jon Witherspoon, the man who paid to produce the films originally.

==Feature films== Atlanta neighborhood American citizen.  She is grateful that her employment with the United States government has turned her life into something positive, something she keeps repeating in the film.  Hiding in a house, the Evil Twins reveal that they plan to infect the world with the AIDS virus, which they have samples of bottled in a jar.  When the Twins discover Star Booty snooping around, they decide to dump the virus through the water pipes.  Star Booty breaks in and thwarts the plan.  The Twins start to fight her and as they fight, Star Booty finds heroin on the male twin.  As both twins are lying unconscious Star Booty gives a heartfelt monologue about her mission to purge drugs from society.  The Twins wake up and try to escape, but Star Bootys quick reflexes stop them. Star Booty throws Twins from the second story to the ground and they die.  Later, Max calls Star Booty to congratulate her on her success.  While he is speaking, Max and his secretary are fondling and doing heroin, which may suggest a conspiracy.

Star Booty II: The Mack is the sequel.  RuPaul reprises her role as Star Booty.  The film opens with Cornisha, leaving her pimp Macks hideout and renouncing her harlot ways.  Mack is furious and beats her down.  Cornisha, injured, manages to escape and reach a payphone to call Star Booty.  As she is talking, Cornisha dies, as a result of her injuries.
A helicopter arrives with Max.  Max is shown from the back and played by a different actor and his face is never revealed.  He meets Star Booty in downtown and gives her information about Mack.  Star Booty vows to avenge her sisters death and to kill Mack.
 hustle for their breakfast.  Star Booty, undercover, comes upon these girls with a swagger and a confidence that they dont have.  She inquires whether they have a pimp or not and she urges them to be self-reliant and join her.  Lizeth is revealed to be pessimistic and emotionally codependent on Mack and claims he treats her right and is reluctant to do so.  Eventually, she befriends them and before long, they are running the streets together.

Mack sees Star Booty on a street corner and takes her to his bar hideout.  Mack is smitten by Star Booty and gives her a puff of his cigarette. Their canoodling is interrupted by a phone call.  The caller who is revealed to be Lizeth, who warns that, Star Booty is going to kill him.  While Mack is speaking on the phone, Star Booty starts snooping around for evidence and discovers files that show evidence of her sisters murder, alcohol, and exploiting of women.  Mack catches Star Booty and she reveals who she really is and why is there.  Star Booty escapes and meets with the other girls and discovers that Lizeth is the traitor and Star Booty and the other girls go find her.  Unbeknownst to them, Lizeth is searching for Star Booty to kill her.  
Star Booty and the girls encounter Lizeth who pulls a knife on them.  A fight ensues and Lizeth is killed.  Star Booty searches for Mack and finds him in a parking lot.  A scuffle ensues and during the fight, Mack pulls a Tommy gun that Star Booty manages to knock out of his hand before he can shoot.  Quick thinking and fast reflexes, she picks up the gun and kills Mack.  Star Booty meets with Max, gives him searches for Mack and finds him in a parking lot. The films closes with a song called “The Mack”.

Star Booty III: Star Bootys Revenge is the third installment. The film opens with Max, telling Star Booty of her new assignment.  The Singing Peek Sisters have kidnapped Larry Tee.  This time the film takes us from Atlanta to New York city.  Theres an opening montage of shots that show Star Booty undercover throughout the city.  Star Booty arrives at the hideout of the Singing Peek Sisters where they have tied Larry Tee with dynamite and apparently are angry with not having a recording contract while their rivals do.  Star Booty encourages them to trust her and to follow her plan to success.  They let Tee go and with Star Bootys help, they hit the recording studio and start promoting themselves in New York city and eventually make it to number one on the Billboard charts.  In the end, Star Booty says, "I knew they could do it".

==Soundtrack==
{{Infobox album  
| Name       = RuPaul is Star Booty
| Type       = soundtrack
| Artist     = RuPaul
| Cover      = RuPaul Is Starbooty!.jpeg
| Alt        = 
| Released   = 1986
| Recorded   = 
| Genre      = {{flat list|
*Pop rock
*Electronic music|Electronic}}
| Length     = 33:10
| Label      = {{flat list|
*Funtone
*Every}}
| Producer   = {{flat list| Fenton Tart Randy Pop)}} Sex Freak (1985)
| This album = RuPaul is Star Booty (1986)
| Next album = Supermodel of the World (1993)
| Misc       =
}}
 CD in World of Wonder, the production company that now produces RuPauls Drag Race.

===Track listing===
Writing credits adapted from Allmusic. 
{{tracklist
| total_length    = 33:10
| writing_credits = yes
| title1          = The Mack
| writer1         = 
| length1         = 4:46
| title2          = Make That Move
| writer2         = 
| length2         = 5:42
| title3          = Ghetto Love
| writer3         = 
| length3         = 4:15
| title4          = Star Booty
| writer4         = {{Flat list|
*Bryan Chambers
*John Witherspoon}}
| length4         = 4:41
| title5          = You Want Love (Follow Me)
| writer5         = {{Flat list| Randy Pop Fenton Tart}}
| length5         = 2:56
| title6          = Everything
| writer6         = {{Flat list|
*Randy Pop RuPaul Charles
*Fenton Tart}}
| length6         = 3:54
| title7          = You Are The World (To Me)
| writer7         = {{Flat list|
*Randy Pop
*Fenton Tart}}
| length7         = 4:20
| title8          = Ernestines Rap
| writer8         = {{Flat list|
*Nancy Nolan Robert Warren}}
| length8         = 2:36
}}

===Release history===
{|class=wikitable
|-
! Format
! Date
! Label(s)
|- LP
| 1986
| Funtone
|- CD 
| October 7, 1997
| Every/Funtone
|-
|}

==See also==
*Starrbooty

==References==

 
1  

==External links==
*  

 

 
 