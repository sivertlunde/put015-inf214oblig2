Strike Commando
{{Multiple issues|
 
 
}}

{{Infobox film
| name           = Strike Commando
| caption        =
| image	=	Strike Commando FilmPoster.jpeg
| director       = Bruno Mattei
| producer       = Oscar Faradyne
| writer         = Claudio Fragasso Bruno Mattei
| starring       = Reb Brown Christopher Connelly
| music          = Luigi Ceccarelli
| cinematography = Richard Grassetti
| editing        = Bruno Mattei
| distributor    = Flora Film
| released       =  
| runtime        = 104 minutes
| country        = Italy Philippines
| language       = English
| budget         =
}}
 Christopher Connelly and Alex Vitale. The film, directed by Bruno Mattei, was shot on the Philippines. The sequel, Strike Commando 2, was released in 1988 in film|1988, but did not star Brown.

==Plot==
Sgt. Michael Ransom (Brown) and his team of "Strike Commandos" are sneaking into a Vietnamese base, who are planning to lay explosives. Colonel Radek and Ransoms majors watch from a nearby vantage point. All prove to be successful, until one of the commandos is caught and killed by a sentry, rising the alarm. The commandos have no choice but to retreat, but Colonel Radek who is in charged of the mission demands the explosives be set off as the commandos are still retreating. One of the explosives kill one of Ransoms soldiers as he retreats, and Ransom is blown into a river.
 French soldier America and Disneyland.

The next day, the group run into resistance while trying to cross a river, and are shot down by a nearby patrol boat. Ransom decides to go into the river and take down the boat. However, the group is flanked by a group of VCs who are taken down by Le Due with a grenade. Ransom dumps a huge amount of grenades into the boat and kills everyone in the patrol boat.
 Russian patch symbol torn from the soldiers uniform. The Russian soldiers are in search of the village people as Ransom kills them off one by one. Ransom soon discovers, however, that there are too many and that he must retreat. Radek commands the helicopter to retreat to base, but Ransoms major is able to talk the soldier into picking him up anyway. Ransoms major is able to calm Ransom so much so that Ransom volunteers to go back to take pictures of the base so that there will be proof of Russian presence in Vietnam.

Ransom goes back to Vietnam but finds the village people have been slaughtered. Ransom finds the village boy that helped him in the beginning as he is dying. Ransom tells him more things about the amazing Disneyland, just as he dies. Ransom finds the name of the Russian soldier, Jakoda, and seeks revenge. Ransom mugs a VC and he tells him where to find Jakoda. Ransom, enraged, fires at the village with his stolen M60 machine gun. Jakoda finds Ransom reloading the weapon, but he is able to talk Ransom into surrendering by holding a civilian hostage. Ransoms superiors find out that there are Russians in there and that now he is going to be tortured. They brutally torture Ransom by having him do yard work, beating him, electrifying him, and burning his back with a blow torch. After spending months in a cell with a corpse, Ransom breaks and agrees to make a demoralizing radio broadcast. However, he is able to get the best of the Russian soldiers and kills them. Ransom takes Jakodas girlfriend Olga (Kamteeg) hostage, he is able to kill some of the soldiers using Olga as bait. He then radios Radek to bring another chopper to pick him up, and he kills more of the soldiers. When Ransom and Olga reach the pick up point, Olga says that they will not pick up Ransom but attempt to kill him. The helicopter flies by but kills Olga and almost kills Ransom. Ransom is able to gun down one of the gunmen and the helicopter retreats. Ransom then comes across an army boat and dives in to go kill some more soldiers. A VC sneaks up on him and strangles him, but Ransom is able to stab him and dives out when the ship explodes.

Ransom comes back to the shore and kills a Russian soldier, but Jakoa boots him in the head and challenges Ransom in a fight. Ransom and Jakoda brutally beat each other until Ransom gets the best of Jakoda and propels him into a waterfall, possibly killing him. Ransom yells in triumph.

Ransom is able to find his way back to base and is enraged and looking for Radek. He fires at Radeks office. But Ransoms major says that Radek has gone AWOL. However, one of the generals finds out where Radek is hiding.

Ransom finds out that Radek has actually become an importer/exporter in Manila. Ransom arrives at the building where Radek is, and goes to the front desk lady. He then lays a 2-minute grenade on the ash tray on the desk. The lady then alerts all the people. Ransom roams the halls of the building using his M60 and mows down all the men. He then loads a grenade on his gun and fires at Radek, blowing him up.

Ransom then leaves the building, but he finds that Jakoda is back and that he has got a pair of metal teeth from their last fight. Jakoda tackles Ransom, but Ransom puts a grenade in Jakodas mouth; Ransom runs and Jakoda explodes, leaving only his metal teeth behind and the echo of Jadova screaming, Amerikanski!.

==Cast==
* Reb Brown as Michael Ransom Christopher Connelly as Colonel Radek, a major antagonist of the film.
* Alex Vitale as Jakoda, the main antagonist of the film.
* Luciano Pigozzi (as Alan Collins) as Le Due, Luciano also played "Pact" in the 1983 cult classic Yor, the Hunter from the Future, which also starred Reb Brown as Yor.
* Louise Kamsteeg (as Loes Kamma) as Olga
* James Gaines as Radeks soldier
* Edison Navarro as Lao
* Karen Lopez as Cho-Li

==Allegations with other films==
* The films plot is modeled after  ; the characters as well are based on many them, such as Michael Ransom (John Rambo was an inspiration to create this character), Colonel Radek and Ransoms Major.
*The scene where Ransom is woken up by the village people is a scene ripped from  .
* The character Jakoda has some similarities with some of actor Dolph Lundgrens characters. Missing in Action .

==Reception==
The film has received a mixed reception from critics. 

==External links==
*  

 

 
 
 
 
 
 
 