Getting Nowhere Faster
{{Infobox Film
| name = Getting Nowhere Faster
| image =
| caption = 
| director = 
| producer = Villa Villa Cola
| writer = 
| narrator =  Alex White
| music =
| cinematography = 
| editing =  Element
| released = 2004
| runtime = 45 minutes
| country = USA
| language = English
| budget = 
}}
 documentary about female skateboarders.  The documentary features skateboarding footage of the worlds best female skateboarders, as well as a fiction film called The Skatepark Hauntings of Debbie Escalante.  The skateboarding footage is interwoven with scenes from the film.
The DVD features an option to watch only the skateboarding footage, or the storyline sections of the feature.

==Plot==
Debbie Escalante and her partner, the Cowboy, haunt a skate park, until a group of female skaters leave their dance class to combat Debbie.

==Featured skaters==

*Amy Caron
*Vanessa Torres
*Lyn-Z Adams Hawkins Alex White
*Kenna Gallagher
*Faye Jaime
*Lauren Mollica Van Nguyen
*Elizabeth Nitu Nugget
*Stefanie Thomas
*Patiane Frietas
*Lauren Perkins

==Cameos==
Elissa Steamer, Cara-Beth Burnside, Jen OBrien, Monica Shaw, Lisa Whitaker, Jessie Van Roechoudt, Lacey Baker

==DVD bonus features==
*Bonus skate footage of several skaters
*Tour footage
*Skate footage from the public
*Slideshow

==Sponsors==
*Element Skateboards
*Etnies
*SG Magazine 
*PETA2 

==External links==
* 
* 
*  

 
 
 

 