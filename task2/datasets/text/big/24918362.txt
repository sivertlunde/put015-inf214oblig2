Sarah's Choice
{{Infobox film
| name           = Sarahs Choice
| image          = Sarahs Choice.jpg
| image_size     = 
| alt            = 
| caption        = 
| director       = Chad Kapper
| producer       = David A.R. White Byron Jones Michael Scott Russell Wolfe J.E. Smith
| writer         = Sean Paul Murphy Timothy Ratajczak
| narrator       = 
| starring       = Rebecca St. James Logan White Dick Van Patten Brad Stine
| music          = James Robert Ballard
| cinematography = Virgil L. Harper
| editing        = Sean Paul Murphy
| studio         = Pure Flix Entertainment EMI CMG Distribution
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = $120,000
| gross          = 
}}
 Christian direct-to-video pro-life film directed by Chad Kapper. The film was the first lead film role for contemporary Christian singer-songwriter Rebecca St. James as the title character, along with Logan White and Dick Van Patten.     Christian comedian Brad Stine also appears in the film,  along with Charlene Tilton and Staci Keanan. It was released to DVD on November 1, 2009,    and was aired on February 27, 2010 on the Trinity Broadcasting Network.    

== Plot ==
Sarah Collinss (Rebecca St. James) co-worker gets pregnant which means Sarah could get a promotion. Meanwhile her boyfriend Matt (Julian Bailey) is pulling pranks. Sarah then finds out she is pregnant. At the doctors office, a lady gives her a card and tells her the Lord will give her three visions. Megan (Logan White) reveals her story about her abortion. Sarah is considering an abortion. Before she makes her final decision, her visions cause her to think about her future.

== Cast ==
* Rebecca St. James as Sarah Collins
* Logan White as Megan
* Dick Van Patten as Pastor Smith
* Brad Stine as Clay
* Staci Keanan as Denise
* Robert Miano as Henry
* Charlene Tilton as Michelle Biden
* Ethan Cooper Roy as Jack
* Julian Bailey as Matt Evans
* Sidney Mason Gunn as Jill
* Sean McGowan as Chad
* Matthew Bacis as Michael
* Linda Bisesti as Agnes Collins
* Marc Davies as Bob
* Judy Lewis as Older Sarah
* Carey Scott as Justin
* Sean Sedgwick as Thad
* Libby Smallbone as Daisy
* Yvette Tucker as Jennifer

== Production ==
Principal filming was completed in Ohio at the end of February 2009.     When speaking of her role in the film Rebecca St. James said, "Obviously everyone wants a redemptive story, but the truth is that 43 percent of childbearing-age women today have abortions; its much higher than what most people think."  St. James wrote the song, "Little One," for the movie. 

== Release ==
Sarahs Choice was released to DVD on November 1, 2009, and DVD release was followed by a theatrical premiere at Warner Brothers Studios in Hollywood, California|Hollywood.        The DVD contains bonus features "The Making Of", "Trailers", and "Commentary", and is available from EMI CMG Distribution.  The film is also set to air uncut and commercial free on February 27, 2010 on the Trinity Broadcasting Network.  It was shown at the Projecting Hope Film Festival at Waterworks Cinemas near Fox Chapel, Pennsylvania. 

=== Reception === CBN noted the films storyline of lead character Sarah Collins by stating the film "portrays all the believable influences for abortion through her co-workers, pro-abortion medical clinic, and her personal motives to stay on the career track."  They noted the films "pro-life message" and that the filmmakers approached the topic without use of graphic representations, "making it suitable for family viewing, as well as, for church groups". The noted that Rebecca St James wrote and sang the song “Little One”, which was used as background music at the end of the film," and praised St. James for "infusing her character with the passion she portrays in music."   

==Book==
A novel based on the film will be released on May 27, 2014 and authored by Rebecca St. James and Nancy Rue. 

== References ==
 

== External links ==
*  
*  

 
 
 
 
 