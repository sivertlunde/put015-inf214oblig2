Samaritan Girl
{{Infobox film
| name = Samaritan Girl
| image = Samaritan Girl poster.jpg
| caption = 
| director = Kim Ki-duk
| writer = Kim Ki-duk Seo Min-jeong Lee Eol
| producer = Kim Ki-duk   Bae Jeong-min
| cinematography = Seon Sang-jae
| editing = Kim Ki-duk
| music = Park Ji-woong
| distributor = Cineclick Asia Tartan Video USA
| released =  
| runtime = 97 minutes
| country = South Korea
| language = Korean
| film name      = {{Film name
 | hangul =  
 | hanja = 
 | rr = Samaria
 | mr = Samaria}}
| budget =
| gross =    . Box Office Mojo. Retrieved 2012-03-04. 
}}
 2004 South Korean film written and directed by Kim Ki-duk. 

==Synopsis== prostituting herself while Yeo-jin acts as her pimp, setting her up with the clients and staying on guard for the police. Things take a turn for the worse when Yeo-jin gets distracted from her duty and the police raid the motel where Jae-yeong is meeting with a client. To avoid getting caught, Jae-yeong jumps out of a window, fatally injuring herself.

After Jae-yeongs death, Yeo-jin blames herself and to ease her own conscience, sets to return all of the money they earned to the clients while sleeping with them herself. Eventually Yeo-jins father, a policeman, is devastated when he discovers what she is doing. He starts following her discreetly and confronts her clients with increasingly violent results. Finally, he ends up brutally killing a client.

For the rest of its duration, the movie follows the father and daughter on a short trip to the countryside, where they both sense something is wrong with the other, but are unable to confront each other directly. In the end, the law catches up with the father, who hopes to have done enough to prepare Yeo-jin for her life without him.

==Cast==
*Kwak Ji-min - Yeo-jin
*Han Yeo-reum (credited as Seo Min-jeong) - Jae-yeong 
*Lee Eol - Yeong-ki, Yeo-jins father

==Reception==
As with other films by Kim Ki-duk, Samaritan Girl was not a box office success in its home country, but was better received overseas. After it won the Silver Bear, the second place award at the 2004 Berlin International Film Festival, it became a sought-after film for other international film festivals. 

== References ==
 

==External links and references==
* 
* 

 

 
 
 
 
 
 

 