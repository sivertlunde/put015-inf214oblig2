Repeat Performance
 
{{Infobox film
| name           = Repeat Performance
| image          = Repeat Performance poster.jpg
| alt            =
| caption        = Theatrical release poster
| director       = Alfred L. Werker
| producer       = Aubrey Schenck
| writer         = Walter Bullock (screenplay) William OFarrell (novel)
| starring       = Louis Hayward Joan Leslie Richard Basehart
| music          = George Antheil
| cinematography = L. William OConnell
| editing        = Louis Sackin
| studio         = Aubrey Schenck Productions
| distributor    = Eagle-Lion Films R&B Video (DVD & VHS)
| released       =  
| runtime        = 91 minutes
| country        = United States
| language       = English budget = $600,000 Tom Weaver, It Came from Horrorwood: Interviews with Moviemakers in the SF and Horror Tradition McFarland, 2000 p 272 
}}
Repeat Performance (1947 in film|1947) is a film of the film noir style starring Louis Hayward, Joan Leslie and Richard Basehart. The time travel picture combines elements of a 1940s drama with a science fiction twist. The film was released by Poverty Row studio Eagle-Lion Films, directed by Alfred L. Werker, and produced by Aubrey Schenck.

== Plot ==
On New Years Eve 1946, a woman is standing over her dead husband with a gun in her hand.  She panics and goes to her friends for help.  While seeking help from her friends at a pair of parties, she wishes that she could live 1946 all over again.

Magically, because she wished exactly at the strike of midnight on New Years, her wish is granted and she is transported back to the beginning of 1946 with her husband alive.  She attempts to relive the year without making the mistakes she and her friends made throughout the year, but certain events repeat themselves nonetheless, leaving Sheila to question whether there really is such a thing as fate or not.

The story climaxes again on New Years Eve, when through Sheilas interferences over the year, her husband becomes convinced that shes trying to destroy him. He violently confronts her. Her friend William, who believed in Sheilas foresight, shoots her husband with her gun.

== Cast ==
*Louis Hayward as Barney Page
*Joan Leslie as Sheila Page
*Virginia Field as Paula Costello
*Tom Conway as John Friday
*Richard Basehart as William Williams, Poet
*Natalie Schafer as Eloise Shaw
==Production==
The film changed the original story where the girl was the villain because it was felt Joan Leslie could not play a villain. 

==  Re-make  == 1933 film of the same name)

==See also==
* List of films featuring time loops

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 