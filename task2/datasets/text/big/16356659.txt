Moyl: The Story of a Traveling Jewish Ritual Circumciser
 
{{Infobox film
| name           = Moyl: The Story of a Traveling Jewish Ritual Circumciser
| image          = Moyl_DVD.gif
| image_size     =
| caption        =
| director       = Moti Krauthamer
| producer       =
| writer         =
| narrator       =
| starring       =
| music          =
| cinematography =
| editing        =
| distributor    =
| released       = 2004
| runtime        = 50 min.
| country        = United States English
| budget         =
| preceded_by    =
| followed_by    =
| website        =
}} documentary made in 2004 by Emmy Award-winning director Moti Krauthamer. {{Cite web
  | title = About Us features Moti Krauthamers Moyl
  | publisher =KCTS Television
  | date = September 2005
  | url =http://www.kcts.org/inside/news/archive/release_150.htm
  | accessdate = March 18  }}  It profiles David Bolnick, a man thousands of Jewish parents have trusted to make the cut that welcomes their child into the Jewish community.

==Summary==
The film follows Bolnick as he journeys across North America, conducting bris ceremonies, which are held eight days after a Jewish boy is born. Amid the ceremony, the child is officially introduced to relatives and associates, his name is announced, and, as a sign of the continued covenant between God and Abraham, he is circumcised. The picture explores the importance of Jewish circumcision on a theoretical and personal level, and demonstrates how the practice has been adapted for todays modern world.  Bolnick speaks openly about related subject matter from pain, to errors, to old and tired circumcision jokes, while  parents explain the meaning behind the tradition that dates back hundreds of years.

The documentary has interviews with fathers and they reflect on how it changed their relationships with their sons as they try to convey the feeling of bringing another person into a community.
 covenant that has been passed down for thousands of years. Bolnick suggests that the custom is special because it is the only occasion Jews are allowed to modify their bodies. It distinguishes the infant as a Jew and suggests that he will proceed to abide by the covenant for a lifetime.

==Notes==
 

==References==
*{{Cite web
  | title = About Us features Moti Krauthamers Moyl, Shannon Gees Seattle Transportation
  | publisher = KCTS Television/Seattle: The Public Network
  | date = September 2005
  | url =http://www.kcts.org/inside/news/archive/release_150.htm
  | accessdate = March 18  }}

*{{Cite web
  | title = Documentary "Moyl" to be Shown on Whitman Campus
  | publisher = Whitman College News Service
  | date = November 2005
  | url =http://www.whitman.edu/content/news/Moyl
  | accessdate = March 18  }}

==External links==
* 
* 

 
 
 
 
 
 


 