Before I Hang
{{Infobox film
| name           = Before I Hang
| image          = BeforeIhand.jpg
| caption        = Theatrical release poster
| director       = Nick Grinde
| producer       = Wallace MacDonald
| writer         = Robert Hardy Andrews Karl Brown
| starring       = Boris Karloff Evelyn Keyes
| music          = Morris Stoloff
| cinematography = Benjamin H. Kline
| editing        = Charles Nelson
| distributor    = Columbia Pictures
| released       =  
| runtime        = 62 minutes
| country        = United States
| language       = English
| budget         =
| preceded by    =
| followed by    =
}}
Before I Hang is a 1940 American science fiction, horror film released by Columbia Pictures, starring Boris Karloff. The film was directed by Nick Grinde (under the working title The Wizard of Death), and was one of several films Karloff starred in contract with Columbia. Pitts, 2010, p. 9  Stephen Jacobs, Boris Karloff: More Than a Monster, Tomohawk Press 2011 p 260-261 

==Plot==

Dr. John Garth (Boris Karloff) is on trial for murder after performing a mercy killing on an elderly friend.  In the trial, he reveals that he had been researching a cure for aging, but had not had time to perfect it before his friends pain became unbearable.  Despite his pleas for mercy, the judge sentences him to be hanged in three weeks time.
 commuted to life imprisonment.  At the same moment, the serums effect on his body causes Dr. Garth to collapse.

When Dr. Garth awakes in the prison medical ward, he discovers that the serum has reversed some of the effects of aging on his body, including the graying of his hair, the appearance of his face, and his physical fitness.  Encouraged, he decides to perform another test of the serum, this time on Dr. Howard.  As he is preparing Dr. Howard for injection, however, Dr. Garth is overcome by a sudden urge to kill, induced by the presence of an executed murderers blood in his system.  After he strangles Dr. Howard, a wandering prisoner enters the room who, after a struggle, is also killed by Dr. Garth.

When the prison authorities discover Dr. Garth, who doesnt remember committing the murders, and the two bodies, they believe that the wandering prisoner killed Dr. Howard and attempted to kill Dr. Garth.  As a result, Garth is labelled a hero and granted a full pardon.  He returns home to live with his daughter, Martha (Evelyn Keyes), and continues his research on the anti-aging serum.

Wishing to test it further, he confronts three of his aging friends and requests that they be his test subjects.  Initially, they refuse, but one of them, Victor Sondini (Pedro de Cordoba), later changes his mind after Dr. Garth pays him a personal visit.  Just as he is about to administer the serum, Garth is again overcome by the impulses of the executed prisoner and strangles his friend.  Finally beginning to realize what he has been doing, Garth visits one of his other friends, George Wharton (Wright Kramer), to confess his crimes and request that he be his final test subject before he turns himself in.  Wharton attempts to call for help, but Garth kills him before he can do so.

As the bodies begin to pile up and Dr. Garths behavior becomes more erratic, Martha begins to suspect that something is up and confronts her father.  Garth begs her to leave as he continues to fight the impulses of the murderous blood, but she refuses and he comes at her.  She faints and Dr. Garth flees.  In the final scene, Dr. Garth, now being pursued by the police, approaches the prison where he had been incarcerated.  The warden admits him, but Garth immediately makes aggressive movements toward the armed guard at the gate.  The guard shoots him and, as he is dying, the doctor admits that he committed suicide in order to prevent himself from killing anyone else.

==Cast==
*Boris Karloff (Dr. John Garth)
*Evelyn Keyes (Martha Garth)
*Bruce Bennett (Dr. Paul Ames)
*Edward Van Sloan (Dr. Ralph Howard)
*Ben Taggart (Warden Thompson)
*Pedro de Cordoba (Victor Sondini)
*Wright Kramer (George Wharton)
*Bertram Marburgh (Stephen Barclay)

==See also==
* List of American films of 1940
* Boris Karloff filmography

==Notes==
 

==References==
* 

==External links==
* 
* 

 

 
 
 
 
 
 
 