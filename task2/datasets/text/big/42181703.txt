Stage Fright (2014 film)
 
{{Infobox film
| name           = Stage Fright
| image          = File:Stage Fright 2014 horror musical poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Jerome Sable
| producer       = Jonas Bell Pasht Ari Lantos
| writer         = Jerome Sable Meat Loaf Aday
| music          = Jerome Sable Eli Batalion
| cinematography = Bruce Chun
| editing        = Christopher Donaldson Lisa Grootenboer Nicholas Musurca
| studio         = Serendipity Point Films, XYZ Films, Citizen Jones
| distributor    = Entertainment One   Magnolia Pictures  
| released       =  
| runtime        = 88 minutes
| country        = Canada
| language       = English
| budget         =
| gross          = $7,078
}} musical horror VOD release on April 3, 2014, and a theatrical release on May 9.  It stars Allie MacDonald as a hopeful young singer terrorized by a killer at a musical theater camp.  

==Plot== The Phantom of the Opera) to a packed audience. That same night, she was murdered backstage by an unknown assailant wearing the mask of the plays villain, Opera Ghost. Years later, Kylies children Camilla and Buddy have grown into teenagers raised by Roger McCall, a former lover of Kylies and the producer of a musical theater summer camp on the brink of bankruptcy. When Camilla hears that the camp will be producing a kabuki version of The Haunting of the Opera, she decides that she will sneak into auditions one way or another. She manages to convince a camper overseeing the auditions, Joel Hopton, to let her in and Camilla easily impresses the stage director Artie and wins the lead role of Sofia - much to Buddys dismay.

As the opening day grows closer and closer, Camilla discovers that Artie will only let her perform on opening day as long as she provides him with sexual favors. He emotionally blackmails her by playing her off of Liz Silver, a camper that will do anything to perform on opening night. Camilla tries to ward off Arties attentions by only making out with him, which disgusts Joel - whom Camilla has largely ignored since the audition. The night before the performance, Artie gives Camilla an ultimatum: either she sleeps with him or he gives the opening night performance to Liz. Camilla initially acquiesces to Arties overtures, but decides at the last minute that she cant go through with it. After she leaves, Artie is brutally murdered by someone wearing the Opera Ghost mask, but Roger tells everyone that Artie died by accident. Unwilling to potentially lose a visit from important Broadway agent Victor Brady, who is only willing to watch the performance if Camilla is performing, Roger manages to persuade everyone to perform opening night as planned.

That night, Joel tries to warn Camilla that Artie was killed and that the murderer is still out there, but his warnings go largely unheeded and the show begins. All seems well with the musical and Victor until later in the performance, when Opera Ghost kills several performers and stops Liz from taking revenge on Camilla for performing on opening night. This causes a gap in the performance, which the cast tries to fill with impromptu music and dancing while Camilla looks for her missing co-star. She instead finds an ever increasing amount of bodies as the murderer kills more and more people.

Camilla manages to intervene when she discovers Opera Ghost trying to kill Roger, only to discover that the killer is Buddy, who says that he did it because he did not want Camilla to get involved in the acting world, which he saw as corrupt- especially after he saw Roger kill their mother in a jealous rage. While Buddy is talking, Roger manages to free himself and stab Buddy to death. He chases her throughout the camp and corners her backstage, eventually forcing Camilla to kill him with a buzz saw and stumble onto stage. The audience initially assumes that this was all part of the show and applauds what they believe to be the finale.

The film then cuts to the Broadway revival of The Haunting of the Opera with Camilla as the lead. As she prepares backstage, Opera Ghost lunges out of the mirror at Camilla, only for it to be a hallucination.

==Cast==
 
* Allie MacDonald as Camilla Swanson
* Minnie Driver as Kylie Swanson Meat Loaf Aday as Roger McCall Douglas Smith as Buddy Swanson
* Kent Nolan as Joel Hopton
* Brandon Uranowitz as Arthur Phillippe "Artie" Getz
* Ephraim Ellis as Sam Brownstein
* Melanie Leishman as Liz Silver
* Thomas Alderson as David Martin James McGowan as Victor Brady
* Steffi DiDomenicantonio as Bethany
* Darren Summersby as Tobe
* Ryan Bobkin as Bobkin
* Leanne Miller as Sheila Kerry
* Adrianna Di Liello as Lithpy
* Chelsey Pozdyk as Whitney Rick Miller as the voice of The Metal Killer
 

==Reception==
Critical reception has been mixed to negative, with the film scoring a 33% on review aggregator aggregate site Rotten Tomatoes. The websites consensus reads, "There might be a good movie to be made from mashing up the slasher and musical genres; unfortunately, Stage Fright isnt quite it." 

 ." 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 