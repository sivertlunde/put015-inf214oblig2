The Chimp (2001 film)
 
{{Infobox film
| name           = The Chimp
| image          = 
| image_size     = 
| caption        = 
| director       = Aktan Abdykalykov
| producer       = Marc Baschet
| writer         = Aktan Abdykalykov Avtandil Adikulov Artandie Adykyeov Tonino Guerra
| narrator       = 
| starring       = Mirlan Abdykalykov
| music          = 
| cinematography = Khasan Kydyraliyev
| editing        = 
| distributor    = 
| released       = May, 2001
| runtime        = 92 minutes
| country        = Kyrgyzstan
| language       = Kyrgyz
| budget         = 
| preceded_by    = 
| followed_by    = 
}} Kyrgyz film directed by Aktan Abdykalykov. It was Kyrgyzstans submission to the 74th Academy Awards for the Academy Award for Best Foreign Language Film, but was not accepted as a nominee.         It was also screened in the Un Certain Regard section at the 2001 Cannes Film Festival.   

==Cast==
* Mirlan Abdykalykov - The Chimp
* Dzylykcy Dzakypov - The father
* Sergej Golovkin - Sacha
* Alexandra Mitrokhina - Zina
* Yuri Sokolov - Iouri
* Salynbek Sarymsakov - Akbar
* Alexei Manzinbine - Friend of the father
* Tchynarkoul Moukacheva - Woman with the skin stain
* Jyldyz Abakirova - Jyldyz
* Veronika Semibratova - Vicki
* Renata Tanabaeva - Renata
* Nelli Soultangazieva - Nelli
* Saida Mamyrbaeva - Saida
* Nikolai Bouriak - Head worker
* Ainagoul Essenkojeva - The Mother

==See also==
*List of submissions to the 74th Academy Awards for Best Foreign Language Film

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 