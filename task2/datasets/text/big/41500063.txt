I due carabinieri
{{Infobox film
 | name = I due carabinieri
 | image = I due carabinieri.jpg
 | caption =
 | director = Carlo Verdone
 | writer =   Leonardo Benvenuti  Piero De Bernardi Carlo Verdone
 | starring =  Carlo Verdone, Enrico Montesano, Massimo Boldi
 | music = Fabio Liberatori
 | cinematography =  Danilo Desideri
 | editing =  Antonio Siciliano
 | language = Italian 
 | country = Italy
 | runtime = 120 min
 | released = 1984
 | producer = Mario Cecchi Gori, Vittorio Cecchi Gori
 }} crime comedy film directed by Carlo Verdone.         

== Plot ==
Two friends of Rome enlist with the police station to make a change in their lives. In the barracks the two meet a third friend: Adalberto, with whom they share their adventures during the various missions assigned to them. But the three are very messy and fail to complete even one of these without disasters and clumsiness. When one day one two friends: Marino, discovers that another friend was cheating on him with his girlfriend, he breaks the relationship, but is again called to arms. Now the mission to fulfill is that of a scentare drug trafficking in Northern Italy. The two friends are able to complete the task, but Adalberto is killed. At the end of the story Marino is reconciled with his girlfriend and can marry her in all cheerfulness.

== Cast ==
* Carlo Verdone: Marino Spada
* Enrico Montesano: Glauco Sperandio
* Paola Onofri: Rita
* Massimo Boldi: Adalberto Occhipinti
* Marisa Solinas: the friend of Turin 
* Guido Celano: Uncle Renato 
* Andrea Aureli: Commander
* John Steiner: Criminal on train

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 


 
 