Pursuit of Equality
 
{{Infobox film
| name           = Pursuit of Equality
| image = Pursuit of Equality poster.jpg
| director       = Geoff Callan Mike Shaw 
| producer       = Geoff Callan]
| starring       = Gavin Newsom George W. Bush Rosie ODonnell 
| music          = John DeBorde
| cinematography = Geoff Callan Mike Shaw 	 
| editing        = Mike Shaw
| released       =  
| runtime        = 81 minutes
| country        = United States
| language       = English
}}
Pursuit of Equality is a 2005 documentary film directed by Geoff Callan and Mike Shaw, about the struggle of same-sex couples for marriage equality in the United States. Its focus is mostly on the San Francisco 2004 same-sex weddings|same-sex marriages performed in San Francisco from February 12 to March 11, 2004.
 California Proposition 8.

==Synopsis==
By issuing same-sex marriage licenses, San Francisco Mayor Gavin Newsom uproots the status quo, attempts to change the way the nation looks at life, love, and marriage.
 
From the first frame of the film, even before the press is aware, this film crew is with Mayor Gavin Newsom’s senior staff as the nation’s first same-sex couple exchange their vows and ignite a controversial civil rights topic.

The story continues on the streets, in the courtrooms, and on the steps of City Hall, where same-sex couples clash with church groups who declare that their sexual and romantic desires are sinful.
 equal rights.

This film made the independent film festival circuit, winning multiple awards, and is scheduled for a wider release in the United States in 2008.

==Awards==
* 2005: San Francisco International Film Festival, Audience Award for Best Documentary 
* 2006: San Luis Obispo Film Festival, Audience Award for Best Documentary 
* 2006: Melbourne Queer Film Festival, Audience Award for Best Documentary 
* 2006: Sonoma Valley Film Festival, Audience Award for Best Documentary 
* 2006: Honolulu Rainbow Film Festival, Audience Award for Best Documentary 
* 2006: Palm Springs International Film Festival, Best of the Fest 
* 2009: GLAAD Media Awards, Special Recognition for Directors Geoff Callan and Mike Shaw 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 


 