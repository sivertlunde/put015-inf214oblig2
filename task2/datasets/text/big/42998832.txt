The Gatling Gun
{{Infobox film
| name           = The Gatling Gun
| image          =
| caption        = Robert Gordon Mark Hanna   Mark Hanna  Joseph Van Winkle 
| narrator       =
| starring       = Guy Stockwell
| music          = Paul Sawtell Bert Shefter
| cinematography = Jacques Marquette 
| editing        = Edward Mann
| distributor    = 
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| website        =
}}
 Western shot Robert Gordon, was not released until 1971.

The Gatling Gun (1968 film) is the English title for Quel caldo maledetto giorno di fuoco.

==Plot==
Lured by Apache gold, two cavalry troopers desert their post, killing some of their fellow troopers in order to steal a Gatling Gun. The devious two use a pacifist pastor to bring the weapon across the country. A pursuing cavalry patrol kills one of the deserters and captures the other, however the recovered weapon has been made inoperable.

The Apaches, under their chief Two Knife, relentlessly attack the patrol in order to get the Gatling Gun, or "King Gun" as they call it, to use against the soldiers.

==Cast==
*Guy Stockwell  ...  Lieutenant Wayne Malcolm  
*Robert Fuller  ...  Trooper Sneed  
*Barbara Luna  ...  Leona  
*Woody Strode  ...  Runner   
*Patrick Wayne  ...  Jim Boland  
*Pat Buttram  ...  Tin Pot   Carlos Rivas  ...  Two Knife  
*John Carradine  ...  Rev. Harper  
*Judy Jordan  ...  Martha Boland  
*Phil Harris  ...  Luke Boland   Tommy Cook  ...  Trooper Elwood   Steve Conte  ...  Trooper Mitchell  
*David Cargo  ...  Cpl. Benton
*Kalai Strode  ...  Indian Who Shoots Sneed

==References==
 

==External links==
*  
*   soundtrack album released by Monstrous Movie Music

 
 
 
 
 
 
 

 