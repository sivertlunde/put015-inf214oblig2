Takigaks – Once Were Hunters
 
{{Infobox film
| name = Takigaks – Once Were Hunters
| image = 
| caption = 
| director = Kira Jääskeläinen
| producer = Jouni Hiltunen
| based on = 
| screenplay = 
| starring = 
| music = Uno Helmersson
| cinematography = Timo Peltonen (2nd camera Jouni Hiltunen, Kira Jääskeläinen)
| editing = Katja Pällijeff, Kira Jääskeläinen
| studio = 
| distributor = Katharsis Films Oy
| released =  
| runtime = 55 minutes
| country = Finland Russian
| budget = 
| gross = }}
 Polish director Kira Jääskeläinen. It was filmed during five years in Chukchi Peninsula|Chukotka, and it was released in 2012.

==Background==
While studying at the University of Copenhagen in the early years of the new millennium, Jääskeläinen spotted an announcement on the bulletin board of the university. What was wanted were people who could speak the Russian language, to participate in an expedition to Chukotka. Since this place was a remote border region with restricted access, she could not resist the temptation. After the expedition was over, she remained there for some time, just hanging around. Later she wrote her MA thesis on the material she had collected during the expedition. The visual material that had accumulated she put to good use, when she later sought financing for this document.        The Bering Strait area made a great impression on Jääskeläinen, “one had a feeling that one’s soul really can be at peace here”.   

==Filming trips==
Jääskeläinen went on the first filming trip alone, but during the subsequent three trips she had a cameraman as well as a sound man.

During the first trip Jääskeläinen found the persons whom she would film, made a deal with them and made sure that they were interested in participating in this project.

The filming trips were rather long, a one-way trip might take two or even three weeks, because connections might not function or weather could delay the travel. Thus it was natural that lots of time was spent on location.

Jääskeläinen was the only one in the group who could speak Russian, so she took care of all the practical matters. For a woman to have that role was something new for the most of the locals.

The making of the film took five years. Time and again the project was abandoned, and then resurrected again, “I had a feeling that this would never be completed”. 

==The contents of the documentary==
Takigaks — Once Were Hunters is a film about two brothers, Kolya and Sasha, who are Siberian Yupiks and live in Novoye Chaplino, at the eastern extremity of the Chukchi Peninsula.  

During the first two trips, Jääskeläinen had made observation on the aboriginal people of the region: “I was touched by the fact that the local whalers live in an incredibly beautiful area, and yet they don’t seem to be quite well psychologically. How would the young people build their identity, if they don’t want to become hunters.”
 Canadian company, until the company goes Bankruptcy|bancrupt. Life in the region is meagre, and one does not have many choices here. The young women have mostly moved into big cities. The lot of the men is to hunt for sea mammals and to fish. When the schools have been completed and the foreign company no longer provides employment, there is nothing left to do other than to watch the television on drink, if one is not engaged in hunting and fishing.
 Russian border guard follow the hunters, since the location is close to Alaska and the United States. One villager had sneaked out and gone to St. Lawrence Island, and this is no longer allowed to happen. Now the people of Novoye Chaplino can only dream about seeing their relatives on that island.

The document has a slow pace. Neither Kolya nor Sasha are very talkative, and the soundtrack has been recorded separately.        

==Reception==
According to Pia Ingström of Hufvudstadsbladet “this is a fine document that gives much food for thought”: 
 The camera follows them   in everyday chores in a landscape which during the summer is brilliantly luminous and beautiful, and during the winter the kingdom of snow, where and drunken man is filmed from a discreet distance as he stumbles home in the midst of snow banks. The young women have disappeared into the labour market of the cities and towns and into institutions of higher learning. In one scene we can see a small festival and young children dancing, which highlight the incredible ruggedness of the surrounding landscape.
 slouch along on Arctic Ocean, one is seriously mistaken. With Jääskeläinen we are giving a chance to participate in an imposing aspect of the human life; in the melancholy and lonely feelings that are part of the awareness of the fact that one belongs to the last of one’s kind, participants in a lifestyle and means of livelihood which is coming to an end. In the beginning both of the brothers are given their own space and time, it presents them as slow-moving, quiet and restrained persons, but at the same time aspects of humour and irony are present. In the end their father comments with stern words: “One has to keep oneself together. If one loses one’s grip, then all is lost.”}}

Critic Taneli Topelius of Iltasanomat writes: 

{{ cquote| … the most impressive part of the film has been saved to the end. In a small village in the Bering Strait the hunters have caught a whale and start to cutting the huge animal into smaller pieces.

Westeners seldom have a chance to witness such a moment. In our eyes the cutting of the huge whale with small knives seems cruel and sad. One has no words as one sees a life come to its end. For the locals, however, such as the main characters, Kolya and Sasha, this is something ordinary, something they have been doing already for decades.
 anthropologist she observes the villagers in their everyday chores. The slow pace of film gives it a somewhat sad feeling, but at times the film moves ahead even too slowly.}}

The film was shown in several film festivals in Finland and elsewhere in Europe,    and was also shown on Finnish TV on YLE TV1 in March 2014. 

==Prizes==
*1. prize, documentary films shorter than one hour
**Barents Ecology Film Festival,   | first = | date = | work = | publisher = Helsingin Sanomat
 | accessdate = 2014-03-23 }} 
*Special mention by the jury
**International Film Festival Pecheurs du Monde, Lorient, Brittany, March 2013. 

==External links==
* 

==References==
 

 
 