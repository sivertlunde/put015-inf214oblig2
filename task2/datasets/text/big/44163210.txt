Kaaryam Nissaaram
{{Infobox film
| name           = Kaaryam Nissaaram
| image          =
| caption        =
| director       = Balachandra Menon
| producer       = Raju Mathew
| writer         = Balachandra Menon
| screenplay     = Balachandra Menon Lakshmi
| music          = Kannur Rajan
| cinematography = Anandakkuttan
| editing        = G Venkittaraman
| studio         = Century Films 
| distributor    = Century Films 
| released       =  
| country        = India Malayalam
}}
 1983 Cinema Indian Malayalam Malayalam film, Lakshmi in lead roles. The film had musical score by Kannur Rajan.   

==Cast==
*Prem Nazir
*Sukumari as Annie
*Balachandra Menon Lakshmi as Ammini
*Jalaja as Sarala
*K. P. Ummer as Avarachan
*Lalu Alex
*Poornima Jayaram as Parvathi

==Soundtrack==
The music was composed by Kannur Rajan and lyrics was written by Konniyoor Bhas. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Kanmani Penmaniye (M) || K. J. Yesudas || Konniyoor Bhas || 
|-
| 2 || Kanmani Penmaniye   || Sujatha Mohan || Konniyoor Bhas || 
|-
| 3 || Konchi Ninna Panchamiyo || S Janaki || Konniyoor Bhas || 
|-
| 4 || Thaalam Shruthilaya Thaalam || K. J. Yesudas, S Janaki || Konniyoor Bhas || 
|}

==References==
 

==External links==
*  

 
 
 

 