Bramman
{{Infobox film
| name           = Bramman
| image          = Bramman poster.jpg
| alt            =
| caption        = 
| director       = Socrates
| producer       = K Manju
| writer         = Socrates Sasikumar Lavanya Santhanam
| music          = Devi Sri Prasad
| cinematography = Jomon T. John Faisal Ali
| editing        = Raja Mohammed
| studio         = K Manju Cinemas  
| distributor    = K Manju Cinemas   
| released       =  
| runtime        =
| country        = India
| language       = Tamil
| budget         =   14 crore
| gross          =   23 crore
}}
 Tamil film directed by Socrates,    starring M. Sasikumar and Lavanya Tripathi in the lead roles.     It has cinematography by Jomon T. John.

==Cast==
  Sasi Kumar as Siva
* Lavanya Tripathi as Gayathri
* Naveen Chandra as Madhan Kumar Santhanam as Nandhu Soori as NBK
* Jayaprakash as JP
* Chaams as Chaams
* Arjunan
* G. Gnanasambandam as Sivas Father
* Vanitha Krishnachandran as Sivas Mother
* Dushyanth Jayaprakash as Dushyanth
* Malavika Menon as Lakshmi
* Surekha Vani as College Professor
* Ajay Rathnam
* Vaiyapuri as himself
* Crane Manohar
* George Maryan Anu Mohan Jai as himself Vaibhav as himself Padmapriya in a special appearance
* Madhu Shalini as herself
 

==Production== Santhanam is Soori was also part of the cast, who was earlier wrongly reported to have replaced Santhanam in the film.    It was reported that Yuvan Shankar Raja would compose the music,    but Devi Sri Prasad was signed as the music director. Naveen Chandra stated that he played the second lead as the friend of Sasikumar in the film. 
 Padmapriya danced for an item song in the film. 

==Soundtrack==
*Vodu Vodu - Chinna Ponnu
*Unkannai Penne - Karthik, Manasi
*Vaanathile - Suraj Santhosh
*En Uyirin Uyiraga - Devi Sri Prasad, Anitha
*Vaada Vaada - M.L.R.Karthikeyan, Andrea Jeremiah

==Critical reception==
  called Bramman a "passable commercial entertainer with all essential ingredients that will keep the pot boiling". 


==References==
 

==External links==
*  


 
 
 
 
 
 
 
 