The Drowning Pool (film)
{{Infobox film
| name = The Drowning Pool
| image = Drowning_pool.jpg
| caption = original movie poster
| director = Stuart Rosenberg David Foster Lawrence Turman Walter Hill
| starring = Paul Newman Joanne Woodward Anthony Franciosa Murray Hamilton Gail Strickland Melanie Griffith
| music = Michael Small
| cinematography = Gordon Willis
| editing = John C. Howard
| studio = First Artists
| distributor = Warner Bros. Pictures
| released =  
| runtime = 109 min.
| country = United States
| language = English, French
| budget =
}} 1975 United American thriller film directed by Stuart Rosenberg, and based upon Ross Macdonalds novel The Drowning Pool. The film stars Paul Newman, Joanne Woodward, and Anthony Franciosa, and is a sequel to Harper (film)|Harper. The setting is shifted from California to Louisiana.

==Synopsis==
Private detective Lew Harper (Paul Newman) investigates a blackmail plot in Louisiana bayou country involving the nymphomaniac daughter (Melanie Griffith) of an old flame of his, Iris Devereaux (Joanne Woodward).

Harper is caught up in a power struggle between Iris and oil tycoon Kilbourne (Murray Hamilton), while local police authority Broussard (Anthony Franciosa) has a personal interest in the family and wants the private eye gone.

At one point, the complicated plot has Harper and Kilbournes wife Mavis (Gail Strickland) locked in a hydrotherapy room, with the water rising to the ceiling, hence the films title.

==Cast==
*Paul Newman as Lew Harper
*Joanne Woodward as Iris Devereaux
*Anthony Franciosa (credited as Tony Franciosa) as Chief Broussard
*Murray Hamilton as Kilbourne
*Gail Strickland as Mavis Kilbourne
*Melanie Griffith as Schuyler Devereaux
*Linda Haynes as Gretchen
*Andre Trottier as Hydrotherapist 
*Richard Jaeckel as Lieutenant Franks
*Paul Koslo as Candy
*Joe Canutt as Glo Andrew Robinson (credited as Andy Robinson) as Pat Reavis
*Coral Browne as Olivia Devereaux

==Production==
Producers David Foster and Lawrence Turman optioned the rights to MacDonalds 1950 novel "The Drowning Pool" for director Robert Mulligan. Hill was hired to adapt it. Barbra Nightingale: SELECTED SHORTS DETECTIVE WHO? TOUCHDOWN! Nurse Barbra
By A. H. WEILER. New York Times (1923-Current file)   29 Apr 1973: 135. 

Hill later estimated around "two little scenes" in the film were "pretty much the way I wrote them". "Hard Riding", Greco, Mike, Film Comment 16.3 (May/Jun 1980): 13-19,80. 

==Reception==
The movie was nominated as best picture of the year by the Edgar Allan Poe Awards.

A.H. Weiler of the New York Times said in the review: "Under Stuart Rosenbergs muscular but pedestrian direction, the script, adapted from (Ross Macdonalds) 1950 novel, transports our hero from his native California to present-day New Orleans and its bayou environs. ... Of Course, Mr. Newmans Harper survives beatings, traps and a variety of enticing offers with quips, charm and inherent decency projected in underplayed, workmanlike style. If his performance is not outstanding, it is a shade more convincing than the characterizations of the other principals, who emerge as odd types and not as fully fleshed, persuasive individuals. ... Unfortunately, the performances and such authentic facets as Cajun talk, bayous, New Orleans and an imposing, white-pillared, antebellum mansion set amid wide lawns and ancient live oaks, serve only to make The Drowning Pool a mildly interesting diversion."  

==DVD==
The Drowning Pool was released on November 14, 2006, as part of the Paul Newman Collection DVD box set.

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 
 
 