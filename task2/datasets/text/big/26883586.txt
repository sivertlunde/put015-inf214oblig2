The Fargo Kid
{{Infobox film
| name           = The Fargo Kid
| image          =Poster of the movie The Fargo Kid.jpg
| image_size     =
| caption        =
| director       = Edward Killy
| producer       = Bert Gilroy (producer) Lee S. Marcus (executive producer)
| writer         = Morton Grant (writer) Arthur V. Jones (writer)
| based on = story Sir Piegan Passes by W.C. Tuttle
| narrator       =
| starring       = Tim Holt
| music          = John Leipold
| cinematography = Harry J. Wild
| editing        = Frederic Knudtson
| distributor    =
| released       = December 6, 1940 
| runtime        = 63 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
The Fargo Kid is a 1940 American film directed by Edward Killy starring Tim Holt. It was the second in Holts series of Westerns for RKO.  
 

The script was based on a story which had been previously filmed as Man in the Rough (1928) and The Cheyenne Kid (1935). Richard Jewell & Vernon Harbin, The RKO Story. New Rochelle, New York: Arlington House, 1982. p154 

==Plot summary==
A cowpuncher is mistaken for a notorious gunman.

==Cast==
*Tim Holt as The Fargo Kid
*Ray Whitley as Johnny
*Emmett Lynn as Whopper
*Jane Drummond as Jenny Winters
*Cy Kendall as Nick Kane Ernie Adams as Bush Cleveland
*Paul Fix as Deuce Mallory
*Paul Scardon as Caleb Winters
*Glenn Strange as Sheriff Dave
*Mary MacLaren as Mrs. Sarah Winters

==Soundtrack== Fred Rose)
* Ray Whitleys combo - "Twilight on the Prairie" (Music and lyrics by Ray Whitley and Fred Rose)
* "Echo in the Night" (Music and lyrics by Ray Whitley and Fred Rose)

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 


 