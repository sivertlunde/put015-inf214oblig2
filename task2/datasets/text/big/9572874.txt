Trains of Winnipeg
{{Infobox Film
| name           =  Trains of Winnipeg
| image          =  
| image_size     = 
| caption        =  
| director       = Clive Holden
| producer       =   
| writer         = Clive Holden 
| narrator       =   
| starring       =   
| music          = {{Plainlist |
*Emily Goodden
*Christine Fellows
*Jason Tait
*Steve Bates}}
| cinematography =   
| editing        =   
| studio         = Cyclops Press
| distributor    =   
| released       =   
| runtime        =   
| country        = Canada
| language       = English
| budget         =   
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
Trains of Winnipeg is a film and multimedia art project by Clive Holden, released in stages between 2001 and 2004. The final project included a series of 14 short films, designed as visual representations of Holdens poetry, as well as a soundtrack CD and a book.

The short films were scored by Emily Goodden, Christine Fellows, Jason Tait and Steve Bates; additional contributors on the CD included John K. Samson and Leanne Zacharias, as well as an archival recording of Al Purdy.

The film series won the New Vision Award at the 2005 Copenhagen International Documentary Festival.

==Films==

# "Trains of Winnipeg"
# "18000 Dead In Gordon Head"
# "Love in the White City"
# "F-Movie"
# "Burning Down the Suburbs"
# "Nanaimo Station"
# "The Jew & the Irishman"
# "Saigon Apartments"
# "Bus North to Thompson with Les at the Wheel"
# "Condo"
# "Neighbours Walk Softly"
# "Unbreakable Bones"
# "Active Pass"
# "Hitler! (revisited)"

==Album==
{{Infobox album  
| Name       = Trains of Winnipeg
| Type       = soundtrack
| Artist     = 
| Cover      = 
| Alt        = 
| Released   =  
| Recorded   = 
| Genre      = Spoken word
| Length     =  
| Label      = Endearing Records
| Producer   = 
| Last album = 
| This album = 
| Next album = 
}}
# "Trains of Winnipeg"
# "Grain Train"
# "18,000 Dead in Gordon Head"
# "Death at Neepawa"
# "Neighbours Walk Softly"
# "Nanaimo Station"
# "Condo"
# "Bus North to Thompson with Les at the Wheel"
# "Wind"
# "Transcona"
# "Babette"
# "Nanoose"
# "Necropsy of Al Purdy"
# "Transience"
# "Unbreakable Bones"

==External links==
*  

 
 
 
 
 
 
 

 