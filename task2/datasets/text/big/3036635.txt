Lost & Found (1999 film)
{{Infobox film
| name           = Lost & Found
| image          = Lost and found poster.jpg
| caption        = Promotional film poster
| director       = Jeff Pollack
| producer       = Andrew Kosove Broderick Johnson Morrie Eisenman Wayne Rice
| writer         = J. B. Cook Marc Meeks David Spade
| starring       = David Spade Sophie Marceau
| music          = John Debney Paul Elliott	
| editing        = Christopher Greenbury
| studio         = Alcon Entertainment	
| distributor    = Warner Bros.
| released       = April 23, 1999
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         = $30 million
| gross          = $6,552,255 (USA)
}}
 American romantic comedy film starring David Spade and Sophie Marceau and directed by Jeff Pollack.

== Plot ==
Restaurant owner Dylan Ramsey (David Spade) is head-over-heels in love with his new neighbor, a French cellist named Lila (Sophie Marceau). In a desperate attempt to garner her affections, he kidnaps her beloved pet dog and offers to help her find him on a phantom dog hunt. A wrench is thrown in his plans, however, when the dog swallows his best friends diamond ring, and things get worse for Dylan as Lilas ex-fiancee, Rene, arrives to win her back.

== Cast ==
* David Spade as Dylan Ramsey
* Sophie Marceau as Lila Dubois
* Martin Sheen as Millstone
* Ever Carradine as Ginger
* Patrick Bruel as Rene
* Artie Lange as Wally Slack
* Carl Michael Lindner as Brat
* Jon Lovitz as Uncle Harry Briggs
* Carole Cook as Sylvia
* Estelle Harris as Mrs. Stubblefield
* Marla Gibbs as Enid
* Rose Marie as Clara
* Hal Sparks as DJ
* Jason Stuart as Jewelry Store Clerk
* Frankie Muniz as Boy in TV Movie
* Neil Diamond as Himself
* Agata Gotova as Party Guest (uncredited)

== Reception ==
The film received negative reviews, with Stephen Holden calling it "a rancid little nothing of a movie" in The New York Times,  and a "rotten" score of 18% on Rotten Tomatoes as of August 2012. 

The movie only grossed $6,552,255 in the US with a budget in excess of 30 million dollars. 

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 

 