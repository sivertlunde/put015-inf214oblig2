The Devil's Circus
 
{{Infobox film
| name           = The Devils Circus
| image          = The Devils Circus(1926 film).jpg
| image_size     =
| caption        = Belgian poster
| director       = Benjamin Christensen
| producer       =
| writer         = Benjamin Christensen
| narrator       =
| starring       = Norma Shearer Charles Emmett Mack
| music          =
| cinematography =
| editing        =
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 70 minutes
| country        = United States Silent English intertitles
| budget         =
}}
 silent drama film directed by Danish director Benjamin Christensen, based upon his screenplay. The film stars Norma Shearer and Charles Emmett Mack. It was the first of seven films directed by Christensen in the United States, and one of only four of those films that have not been lost.

==Synopsis==
Mary and Carlstop are a trapeze artist and a pickpocket who get themselves almost fatally involved with Lieberkind, a lion-tamer, and his jealous wife Yonna.

==Cast==
* Norma Shearer - Mary
* Charles Emmett Mack - Carlstop
* Carmel Myers - Yonna
* John Miljan - Lieberkind
* Claire McDowell - Mrs. Peterson
* Joyce Coad - Little Anita

==Preservation status==
The Devils Circus has been preserved by the George Eastman House. Funding for the films restoration was provided by The Film Foundation.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 


 