Every Little Step (film)
{{Infobox film
| name           = Every Little Step
| image          = EveryLittleStepPoster.jpg
| caption        = Original poster
| director       = James D. Stern Adam Del Deo
| producer       = James D. Stern Adam Del Deo
| writer         = 
| starring       = 
| music          = Jane Antonia Cornish Marvin Hamlisch
| cinematography = 
| editing        = Brad Fuller Fernando Villena
| studio         = 
| distributor    = Sony Pictures Classics
| released       =  
| runtime        = 96 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $2,443,479
}} Broadway revival of A Chorus Line   and explores the history of the award-winning musical theatre|musical, beginning with the informal interviews with Broadway dancers conducted by Michael Bennett that served as its basis. Their personal observations and feelings were captured on audiotape, many of which are heard in this film.

3,000 dancers arrived to audition for the revival on the first day.    Some of their stories are interwoven with recollections of members of the original cast, including Donna McKechnie and Baayork Lee; composer Marvin Hamlisch; and Bob Avian, who co-choreographed the original 1975 production and directed the 2006 revival

The film premiered at the Toronto International Film Festival in September 2008   and went into theatrical release with the title Broadway Broadway in Japan the following month.  It was shown at the Berlin International Film Festival, the Thessaloniki Documentary Festival, and the Sarasota Film Festival   before going into limited release in the US on April 17, 2009.

==Critical reception==
Kaori Shoji of The Japan Times said the film "plays off the echo effect of   premise with skill, grace and heartfelt sympathy; its clearly the work of people who love dancers, whether theyre grinning in the spotlight or fighting back tears after a failed tryout . . .  hat surfaces in the film is the fierce dedication   have to their craft and the sense that theyre here singing and performing their guts out in front of choreographers and producers because theres no other place on Earth theyd rather be." 
 television reality shows in which ordinary people use their talents to scramble for the spotlight. But those programs are spectacles of amateurism chasing after celebrity, an impulse that could not be further from what Mr. Stern and Mr. Del Deo, taking their cues from Mr. Bennett, set out to honor. The 17 members of that chorus line — and the thousands like them, including those who dream of playing them — are professionals, and one of the names they give to the glory they seek is work. The other is love." 

Kenneth Turan of the Los Angeles Times said the film "doesnt do anything unexpected, and it doesnt have to . . . its a cant-miss effort that knows how to please." 

Kyle Smith of the New York Post rated the film 1½ out of four stars and called it a "sloppily directed" film that "assumes you know everything going in, which makes it more a souvenir scrapbook than a narrative." 

Peter Travers of Rolling Stone rated the film three out of four stars and described it as "a thrilling combination of documentary and musical dazzler" and "a heartbreaker." 

Owen Gleiberman of Entertainment Weekly graded the film A, calling it "a movie as layered and as enthralling as its subject." 

==References==
 

==External links==
* 
* 


 
 
 
 
 
 