Up & Down: Mukalil Oralundu
{{Infobox film
| name           = Up & Down: Mukalil Oralundu
| image          = KY8A1069.jpg
| alt            =  
| caption        = 
| director       = T. K. Rajeev Kumar
| producer       = V. Balachandran R. Karunamoorthy Latha Kurien Rajeev
| story          = T. K. Rajeev Kumar
| screenplay     = Sunny Joseph Manuel George Indugopan Indrajith Meghana Raj Prathap Pothen Remya Nambeesan
| music          = Prashanth Murali (background score) M. Jayachandran
| cinematography = Jomon T. Thomas
| editing        = B. Ajith Kumar
| studio         = Blue Mermaid Picture Company
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}}
 Malayalam psychological thriller film roughly based on Hollywood film Elevator (2012 film), directed by T. K. Rajeev Kumar. The story unfolds in a lift where nine people get stuck.    Indrajith Sukumaran|Indrajith, Prathap Pothen, K. B. Ganesh Kumar, Rejith Menon, Baiju, Nandhu, Remya Nambeesan, Sruthi Menon, and master Devaraman play the nine characters who got trapped inside the lift.  Meghana Raj, Vijayakumar, Jaikrishnan, Kochu Preman are the other important cast. The film is produced by V. Balachandran, R. Karunamurthi and Latha Kurien Rajeev under the banner of Blue Mermaid Picture Company. The script is penned by Sunny Joseph and Manuel George and camera is cranked by Jomon Thomas. 

Almost the entire narrative takes place inside the limited space that is the lift and the entire storyline is set over a period of a few hours on a particular day.

==Plot==
The film unfolds after eight people and a child get stuck in the lift.They include the lift operator (Indrajith), a police commissioner (Ganesh Kumar), the apartments builder and his dancer-wife (Baiju and Remya Nambeesan, respectively), an alcoholic writer (Prathap Pothen), an IT professional and his girlfriend (Rejith Menon and Sruti Menon, respectively), an American-returnee (Nandhu) and a young boy (Master Devaraman), who intermittently keeps enquiring about his mother (Meghana Raj). They are on their way up to the top floor to take part in the building societys anniversary celebrations. Suddenly, the space becomes a sort of an altered reality. It becomes a place where the real becomes unreal; where emotions are raw and extra sensitive. It even prompts the very nature of the characters to change and secrets to be revealed. 

==Cast== Indrajith as Thampuran, an ex-serviceman-turned-lift operator
* Meghana Raj as the mysterious lady
* Prathap Pothen as Edathil Govindan Nair, a writer Baiju as Sam Christy, a wealthy builder
* Remya Nambeesan as Kalamandalam Prasanna, Christys wife 
* K. B. Ganesh Kumar as Siyaad, the Commissioner of Police
* Nandhu as Cheriyan
* Rejith Menon as a techie
* Shruthy Menon as Mitra
* Master Devaraman as Shanku
* Kochu Preman as the lift mechanic
* Vijayakumar
* Jaikrishnan as Surendran

==Production==
The script and dialogues are written by Sunny Joseph, Manual George and Indugopan. The three had discussed well about each and every scene in the movie.    Rajeev Kumar says there was a consensus among the script writers and him as a director. 

Major portion of the film was shot from Chithranjali Studios in Thiruvananthapuram where the lift was constructed using plywood, the insides covered with embossed metal sheets.  Mohandas was the art director for the film. 

When the script was developed, scenarists and the director felt that the scope of songs were less in it. Also they were apprehensive that the songs would affect the pace of the movie. So there are no songs in the film except a promo song.  Prasanth Murali has done the background score with Quarter tones. 

==Soundtrack==

{{Infobox album  
| Name        = Up And Down Mukalil Oraalundu
| Type        = Soundtrack
| Composer     = M. Jayachandran
| Cover       = 
| Alt         = 
| Released    =  
| Recorded    = Feature film soundtrack
| Length      = 
| Label       = Manorama Music
}}

The music of the film was composed by M. Jayachandran collaborating with T.K. Rajeev Kumar after Rathinirvedam (2011 film)|Rathinirvedam. Ramya Nambeesan sung the theme song. Background Score was composed by the debutante, Prashanth Murali.

{{Track listing
| total_length   =11:49 
| lyrics_credits = yes
| extra_column	 = Singer(s)
| title1	= Vaanam Chuttum Megham
| lyrics1 	= Rafeeq Ahmed
| extra1        = Vijay Yesudas & Mridula Warrier
| length1       = 03:42
| title2        = Up And Down Theme
| lyrics2 	= Rafeeq Ahmed
| extra2        = Ramya Nambeesan
| length2       = 03:38
| title3        = Here We Go Again
| lyrics3       = Jeremaniah John
| extra3 	= Jeremaniah John
| length3       = 04:28
}}

==Release==
Up & Down: Mukalil Oralundu was set to release on 17 May 2013. The date was postponed by a week even after giving the mandatory advertisements announcing the release of the film on 17 May in the popular dailies. The film released on 24 May. 
The film was a hit at the box office.

==References==
 

 
 
 
 
 
 