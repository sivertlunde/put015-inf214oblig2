Times and Winds
{{Infobox film name           = Times and Winds image          = Beş Vakit.jpg image_size     = caption        = director       = Reha Erdem producer       = Ömer Atay writer         = Reha Erdem narrator       = starring       = music          = Arvo Pärt cinematography = Florent Herry editing        = Reha Erdem distributor    = Atlantik Film released       = 2006 runtime        = 111 minutes country        = Turkey language       = Turkish budget         = preceded_by    = followed_by    =
}}
 2006 Turkey|Turkish drama film directed and written by Reha Erdem. The film premiered in the United States on January 11, 2008. It won the Best Turkish Film of the Year Award at the Istanbul International Film Festival.

==Plot==
In a small village in the mountains overlooking the sea the people struggle to survive on a daily basis. Their lives, like those of their ancestors, follow the rhythms of the earth, air and water, of day and night and the seasons, with days divided into five parts by the call to prayer. Childhood is difficult and a father typically has a preference of one son over the other. Ömer, the son of the Imam, is such a victim of his fathers dislike and he wishes for the death of his father. When his wish is not granted he begins to look for ways to kill him as a twelve-year-old boy might think of with his friend Yakup. Yakup seeing his father sexually interested in his teacher also develops a hatred of his father in the same way and as the children grow up they are riddled between guilt and love and hate for their fathers.

==Cast==
*Taner Birsel as Zekeriya
*Nihan Aslı Elmas as Yıldızs Mother
*Köksal Engür as Halil Dayi
*Sevinç Erbulak as Yakups Mother
*Selma Ergeç as The Teacher
*Elit İşcan as Yıldız
*Ali Bey Kayalı as Yakup
*Yiğit Özşener as Yusuf
*Tilbe Saran as Ömers Mother
*Tarık Sönmez as Shepherd Davut
*Cüneyt Türel as Grandfather
*Bülent Yarar as Imam

==External links==
 
* 
* 
* 

 

 

{{Succession box
| | before = Anlat İstanbul
  | after = Beynelmilel (film)|Beynelmilel Golden Boll Award for Best Picture
  | years = 2006
}}

 

 

 
 
 
 
 
 
 


 
 