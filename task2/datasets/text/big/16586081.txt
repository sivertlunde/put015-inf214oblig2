Little Lord Fauntleroy (1936 film)
{{Infobox film
| name           = Little Lord Fauntleroy
| image          = Little-Lord-Fauntleroy-Poster.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      =  John Cromwell
| producer       = David O. Selznick
| writer         = 
| screenplay     = Hugh Walpole
| story          = 
| based on       =  
| narrator       = 
| starring       = Freddie Bartholomew Dolores Costello C. Aubrey Smith
| music          = Max Steiner
| cinematography = Charles Rosher
| editing        = 
| studio         = Selznick International Pictures
| distributor    = United Artists
| released       =  
| runtime        = 98 minutes
| country        = United States English
| budget         = $590,000  , Inc., 1992 ISBN 0-394-56833-8 hardcover  
| gross          = 
}}
 novel of Gone With the Wind.
 Kino Lorber, following a restoration by the George Eastman House Motion Picture Department.

==Plot==
  and Freddie Bartholomew]]
Young Cedric "Ceddie" Errol (Freddie Bartholomew) and his widowed mother, whom he calls "Dearest" (Dolores Costello), live frugally in 1880s Brooklyn after the death of his father. Cedrics prejudiced English grandfather, the Earl of Dorincourt (C.&nbsp;Aubrey Smith), had long ago disowned his son for marrying an American. 

The earl sends his lawyer Havisham (Henry Stephenson) to bring Ceddie to England. As the earls sons are all dead, Ceddie is the heir to the title. Mrs.&nbsp;Errol accompanies her son to England, but is not allowed to live at Dorincourt castle. For Cedrics happiness, she does not tell him it is because of his grandfathers bigotry. The earls lawyer is impressed with the young widows wisdom. However, the earl expresses skepticism when Mr.&nbsp;Havisham informs him that Cedrics mother will not accept an allowance from him.
 
Cedric soon wins the hearts of his stern grandfather and everyone else. The earl hosts a grand party to proudly introduce his grandson to British society, notably his sister Lady Constantia Lorridaile (Constance Collier). 

After the party, Havisham informs the Earl that Cedric is not the heir apparent after all. American Minna Tipton (Helen Flint) insists her son Tom (Jackie Searl) is the offspring of her late husband, the earls eldest son. Heartbroken, the earl accepts her apparently valid claim, though Tom proves to be a rather obnoxious lad. 

Fortunately for Ceddie, his friend Dick Tipton (Mickey Rooney) recognises Minna from her newspaper picture. He takes his brother Ben, Toms real father, to England and disproves Minnas claim. The earl apologises to Ceddies mother and invites her to live with the delighted Ceddie on his estate.

==Cast==
 {{multiple image
 
| align     = right
| direction = vertical
| width     = 240

 
| image1    = Little Lord Fauntleroy (1936) 2.jpg
| width1    = 240
| alt1      =
| caption1  = Freddie Bartholomew, Guy Kibbee
 
| image2    = Little Lord Fauntleroy (1936) 3.jpg
| width2    = 240
| alt2      =
| caption2  = Freddie Bartholomew, Mickey Rooney
 
| image3    = Little Lord Fauntleroy (1936) 4.jpg
| width3    = 240
| alt3      = Una OConnor
 
| image4    = Little Lord Fauntleroy (1936) 1.jpg
| width4    = 240
| alt4      =
| caption4  = C. Aubrey Smith, Freddie Bartholomew, Dolores Costello
 
| image5    =
| width5    = 200
| alt3      =
| caption5  =
 
| image6    =
| width6    = 200
| alt6      =
| caption6  =
 
| image7    =
| width7    = 200
| alt7      =
| caption7  =
 
| image8    =
| width8    = 20
| alt8      =
| caption8  =
 
| image9    =
| width9    = 200
| alt9      =
| caption9  =
 

 
| header            =
| header_align      =  
| header_background =
| footer            =
| footer_align      =  
| footer_background =
| background color  =
}} the American Film Institute Catalog of Feature Films. 
*Freddie Bartholomew as Cedric "Ceddie" Errol, Lord Fauntleroy Dolores Costello Barrymore as "Dearest" Errol
*C. Aubrey Smith as the Earl of Dorincourt
*Guy Kibbee as Mister Silas Hobbs
*Henry Stephenson as Mister Havisham
*Mickey Rooney as Dick Tipton, a Brooklyn bootblack Una OConnor as Mary, the Errols servant
*Constance Collier as Lady Constantia Lorridaile, Dorincourts sister
*Jackie Searl as Tom Tipton
*Jessie Ralph as the Applewoman from Brooklyn
*Helen Flint as Minna Tipton
*Walter Kingsford as Mister Joshua Snade, Minnas lawyer
*E. E. Clive as Sir Harry Lorridaile, Constancias husband Ivan Simpson as Reverend Mordaunt
*Virginia Field as Miss Herbert, singer at party
*Eric Alden as Ben Tipton, Dicks brother

Uncredited
*Reginald Barlow as Mr. Newick, Dorincourts debt collector
*Lionel Belmore as Mr. Higgins, the farmer
*Tempe Pigott as Mrs. Dibble, village woman
*Gilbert Emery as Purvis, doorman of the castle
*Joseph Tozer as Thomas, servant of the castle
*May Beatty as Mrs. Mellon, chambermaid of the castle
*Lawrence Grant as Chief of the Lord Justice
*Robert Emmett OConnor as the Policeman in Brooklyn
*Elsa Buchanan as Susan, the parlor maid

==Production== David Copperfield discovery, Freddie Bartholomew.  
 John Cromwell, the film was shot during the last two months of 1935.   Made within its budget of $500,000, the films final cost was $590,000.  
 Foundation Hospital in Warm Springs, Georgia. 

==Box office== Gone With the Wind.  

==Critical response== John Cromwells sentient direction. Whatever the cause, and it probably was the combination of all four, the picture has a way with it and, unless we are very much in error, you will be pleased."  

==Home media releases== Kino Lorber in 2012. The film was remastered by the George Eastman House Motion Picture Department,  from Selznicks personal print. 

"This Kino Classics release, while far from perfect, sources an original 35mm nitrate print resulting in a better than acceptable presentation," wrote DVD Talk. "And unless original film elements turn up, this is probably the best Little Lord Fauntleroy is going to look for the foreseeable future. Highly Recommended." 

*2012: Kino Classics K914, UPC 738329091422

==See also==
* Little Lord Fauntleroy (1921 film)|Little Lord Fauntleroy (1921)
* Little Lord Fauntleroy, the book
* List of films in the public domain

==References==
 

==External links==
 
*  
*  
*  
* Film at Free Full Videos .co.nr  
*  
*   at Free Full Movies

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 