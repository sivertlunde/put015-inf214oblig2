Before the Hurricane
{{Infobox film
| name           = Qarishkhlis tsin
| image          = 
| caption        = 
| director       = Kote Marjanishvili
| producer       = 
| writer         = Shalva Dadiani
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| distributor    = Sakhkinmretsvi
| released       =  
| runtime        = 40 minutes
| country        = Georgian SSR Georgian Russian Russian
| budget         = 
| gross          = 
}} Georgian black-and-white silent film directed by Kote Marjanishvili based on a script by Shalva Dadiani.

==Plot==
 

==Cast==
* L. Kavsadze as Gijua
* L. Gogoberidze as Tasya
* I. Djordjadze as Maro
* L. Qartvelishvili as Tade
* L. Sokolovi as Revolutionist
* N. Gotsiridze as Old man
* M. Arnazi as Krilova
* Aleqsandre Imedashvili as Petua
* A. Alshibaya as Count Gigo
* Soso Jividze as Saqua
* Ushangi Chkheidze as Niko
* D. Mjavya as Police-officer
* Q. Andronikashvili as Katine
* G. Pronispireli as Kinto
* D. Chkheidze as Priest

==See also==
 
* List of Georgian films

== External links ==
*  

 
 
 
 
 
 


 
 