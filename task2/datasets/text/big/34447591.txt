Gun Smugglers
{{Infobox film
| name           = Gun Smugglers
| caption        =
| image	         =  Frank McDonald
| producer       = Herman Schlom
| writer         = Norman Houston
| starring       = see list below
| music          = Paul Sawtell
| cinematography = J. Roy Hunt
| editing        = Les Millbrook
| distributor    = RKO Radio Pictures
| released       =  
| runtime        = 61 minutes
| country        = United States
| language       = English
| budget         =
| gross          =

}}
 Western directed Frank McDonald.  The film is a Tim Holt B Western wherein Holt serves as a scout for the army in search of some smuggled gattling guns.

Tim Holt plays himself rather than a character. Richard Jewell & Vernon Harbin, The RKO Story. New Rochelle, New York: Arlington House, 1982. p228 
==Plot==
A ranger tracks down agents who steal weapons from the army and sell them to a foreign power.
==Cast==
* Tim Holt as Tim Holt Richard Martin as Chito Rafferty
* Martha Hyer as Judy Davis Gary Gray as Danny Reeves Paul Hurst as Sergeant L. McHugh Hasty Jones
* Douglas Fowley as Steve Reeves
* Robert Warwick as Colonel Davis
* Don Haggerty as Sheriff Schurslock 
* Frank Sully as Corporal Clancy 
* Robert Bray as Henchman Dodge

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 