The Soft Skin
{{Infobox film
| name           = The Soft Skin
| image          = The_Soft_Skin_Poster.jpg
| border         = yes
| caption        = Theatrical release poster
| director       = François Truffaut
| producer       = {{Plainlist|
* Marcel Berbert
* António da Cunha Telles
* François Truffaut
}}
| writer         = {{Plainlist|
* François Truffaut
* Jean-Louis Richard
}}
| starring       = {{Plainlist|
* Jean Desailly
* Françoise Dorléac
* Nelly Benedetti
}}
| music          = Georges Delerue
| cinematography = Raoul Coutard
| editing        = Claudine Bouché
| studio         = {{Plainlist|
* Les Films du Carrosse
* SEDIF
* SIMAR 
}}
| distributor    = Athos Films  (France)  Cinema V  (USA) 
| released       =  
| runtime        = 113 minutes
| country        = France, Portugal
| language       = French, Portuguese, English
| budget         = 
| gross          = 597,910 admissions (France) 
}} romantic drama film directed by François Truffaut and starring Jean Desailly, Françoise Dorléac, and Nelly Benedetti. Written by Truffaut and Jean-Louis Richard, the film is about a successful married publisher and lecturer who meets a beautiful air hostess with whom he has a love affair.    The film was shot on location in Paris, Reims, and Lisbon, and several scenes were filmed at Paris-Orly Airport.    At the 1964 Cannes Film Festival, the film was nominated for the Palme dOr.       Despite Truffauts recent success with Jules and Jim and The 400 Blows, The Soft Skin did not do well at the box office.   

==Plot==
Pierre Lachenay (Jean Desailly), a well-known writer and editor of a literary magazine, is running late for his flight to Lisbon. His friend gives him a ride to the airport, with his daughter Sabine going along for the ride, and they arrive just in time. On the airplane he makes eye contact with a beautiful air hostess named Nicole (Françoise Dorléac). Upon landing, he is greeted by photographers who ask that he pose with the air hostess.
 Balzac and Money" based on one of his books. Back at the hotel, he sees Nicole in the elevator and notices her room number. In his room he calls her room and asks if shed like a drink, but she declines because of the late hour. Shortly after hanging up, Nicole calls back, apologizes, and accepts his invitation for drinks the next day at the hotel bar.

The next evening, Pierre and Nicole spend hours talking through the night. She is captivated by his stories of Balzac and the world of literature. In the morning they return to the hotel and make love in her room. On the airplane the next day, Nicole slips him a matchbook with her phone number. Back in Paris, while he and his wife Franca (Nelly Benedetti) entertain friends, Pierre sneaks off and tries calling Nicole, but there is no one home. The next day he tries again from a phone booth, and this time he gets through, and they meet briefly. In the coming days, they spend time together between her flights. One day they meet at the airport and decide to spend the evening at a nightclub, ending up at her apartment where they make love.

Later that week, Pierre and Nicole travel to Reims where Pierre is scheduled to present a showing of the 1952 documentary film about André Gide at a conference. They check into the Hotel Michelet before Pierre heads over to the theater. After Pierre takes the stage, Nicole arrives without a ticket, and is forced to wait in the lobby during his speech. Afterwards, Pierres friend insists on having a drink before Pierre returns to Paris. As they leave, Pierre ignores Nicole who has been waiting in the street. When his friend invites himself to accompany him back to Paris, Pierre sneaks back to the hotel where an upset Nicole tells him he should leave without her. He apologizes and they decide to leave. After driving through the night they find a romantic cabin.

The next day, Pierre and Nicole enjoy their time together taking photographs. Their happiness is cut short, however, when Pierre calls his wife to tell her he had to stay in Reims overnight. Having called Reims the previous night, Franca suspects hes lying and hangs up in anger. After driving Nicole home, Pierre faces his wife and they argue. Franca accuses him of having an affair, and Pierre walks out of their apartment and spends the night sleeping in his office.

The following morning, Franca calls him at the office informing him that she will never forgive him for walking out on her and that hell be contacted by her divorce lawyer. Pierre goes to Nicoles apartment, but she is unable to talk because her father is visiting. Pierre passes her father on the stairwell as hes leaving. Back at his apartment, Pierre and Franca discuss their divorce and Franca falls apart, sometimes hitting him and at other times begging for his forgiveness. They make love, and afterwards she asks if he will be coming back. He tells her it would never work out with all thats happened. She slams the door on him.

Overcome with depression, Franca is comforted by her friend Odile, who throws away the sleeping pills she finds in the bathroom fearing her friend may try something foolish. Later that week, Pierre and Nicole have dinner and argue after Pierre gets annoyed and embarrassed by her talking loudly - the stress of divorce after fifteen years of marriage is having an effect on him. Later, as he shows her their future apartment under construction, Nicole expresses regret at having gotten involved with him and breaks off their relationship.

Meanwhile, Franca finds a photo shop receipt in one of Pierres jackets and goes to pick up the photos—the ones Pierre and Nicole took on their weekend away together. After seeing the photos, Franca goes home, gets a shotgun from her closet, drives to Le Val dIsère Bar which Pierre frequents, walks up to his corner table, tosses the photos at him, and then shoots him dead.

==Cast==
 
 
* Jean Desailly as Pierre Lachenay
* Françoise Dorléac as Nicole
* Nelly Benedetti as Franca Lachenay
* Daniel Ceccaldi as Clément
* Laurence Badie as Ingrid
* Sabine Haudepin as Sabine Lachenay
* Philippe Dumat as Movie theater manager in Reims
* Dominique Lacarrière as Lachenays assistant Dominique
* Paule Emanuele as Odile
* Jean Lanier as Michel
* Maurice Garrel as Bontemps, the bookseller
* Pierre Risch as Chanoine
 
* François Truffaut as Gas station attendant (voice)
* Carnero as Lisbon organizer (uncredited)
* Georges de Givray as Nicoles father (uncredited)
* Catherine-Isabelle Duport as Young girl (uncredited)
* Maximiliènne Harlaut as Mme. Leloix (uncredited)
* Charles Lavialle as Night watchman at Hôtel Michelet (uncredited)
* Gérard Poirot as Franck, the co-pilot (uncredited)
* Olivia Poli as Mme. Bontemps (uncredited)
* Thérèse Renouard as Cashier (uncredited)
* Jean-Louis Richard as Man in street (uncredited)
* Brigitte Zhendre-Laforest as Linen delivery woman (uncredited) 
 

==Production==

===Filming locations===
* Lisbon, Portugal 
* Paris-Orly Airport, Orly, Val-de-Marne, France 
* Paris, France 
* Reims, Marne, France 

==Reception==
In his review in The New York Times, Bosley Crowther called the film "a curiously crude and hackneyed drama."    Crowther goes on to write:
 

In his review in Slant Magazine, Glenn Heath Jr. called the film "a mesmerizing morality play detailing the machinations of adultery and their deadly consequences."   

On the review aggregator web site Rotten Tomatoes, the film holds a 91% positive rating among critics based on 22 reviews.   

==Analysis==
Despite receiving very mixed reviews upon release, La Peau douce is considered by some Truffaut specialists to be one of his strongest efforts . Truffauts own life followed the same path as Lachenays when the director left his wife for Françoise Dorléac. For the expectant audience, this film was a contrast to the kinetic joie de vivre of Jules et Jim, and may have been perceived as overly serious for a director who had tended towards lighter films up to that point .

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 