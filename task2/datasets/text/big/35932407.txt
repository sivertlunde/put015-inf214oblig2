The Mommy Returns
{{Infobox film
| name           = The Mommy Returns
| image          = The_Mommy_Returns.jpg
| director       = Joel Lamangan
| producer       = Lily Monteverde Roselle Monteverde-Teo Lea A. Calmerin
| starring       = Gabby Concepcion Ruffa Gutierrez Pokwang Jillian Ward Gloria Diaz John Lapus
| screenplay     = Senedy Que
| writer         = Senedy Que
| music          = Francis Concio
| cinematography = Mo Zee
| editing        = Tara Illenberger
| distributor    = Regal Entertainment
| released       =  
| runtime        = 115 minutes
| country        = Philippines
| language       = Filipino, English
| budget         =
| gross          = P30.6 million
}} Filipino family horror movie produced by Regal Films. It was released in Philippine cinemas on May 11, 2012.

== Plot ==
The Mommy Returns tells the story of a mother, Ruby (Pokwang), who dies right on the day of her 25th wedding anniversary with husband, William (Gabby Concepcion). With her untimely death, she leaves behind her three children Amethyst (Kiray Celis), Topaz (Gerald Pesigan), and Sapphire (Jillian Ward).

Ruby is also temporarily trapped in PURGA (short for Purgatory) with its guardian, Dyoga (John Lapus) and the pesky Manny, the chicken pet of San Pedro.

When Catherine (Ruffa Gutierrez), a younger and prettier woman comes into the life of William, Ruby escapes from PURGA and returns to earth as a ghost to drive Catherine out of the family she left behind.

==Cast==
* Pokwang as Ruby Pascual - Martirez
* Gabby Concepcion as William Martirez
* Ruffa Gutierrez as Catherine Laurel - Martirez
* Jillian Ward as Sapphire "Saph" P. Martirez
* Gloria Diaz as Mabel Diaz - Laurel
* John Lapus as Diyoga
* Gerald Pesigan as Topaz P. Martirez
* Kiray Celis as Amethyst " Amy" P. Martirez
* Moi Marcampo as Inday Moi
* Kerbie Zamora as Gary
* Ervic Vijandre as Rodel Hiro Magalona as Emil

==Release==

===Distribution===
The Mommy Returns had a premiere night in Cinema 9 of SM Megamall on May 8, 2012.  It was released in Philippine cinemas on May 11, 2012.

===Box office===
The Mothers Story grossed P30.6 million in the Philippines after two weeks of release, according to figures from Box Office Mojo. 

===Critical reception===
The Mommy Returns received generally negative reviews from local film critics. Mark Angelo Ching of GMA New Media#Philippine Entertainment Portal (PEP)|PEP.ph said the comedy film is not funny, and only works in its dramatic moments. He also commented on the dull performance of the cast. 

Johanna Poblete of Business World said The Mommy Returns did not offer anything new, and did not "elevate the local movie industry".  Philbert Dy of ClickTheCity.com said the movie is "a dreadful bore." 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 