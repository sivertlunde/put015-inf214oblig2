Robinson in Ruins
{{Infobox film
| name           = Robinson in Ruins
| alt            =  
| image	=	Robinson in Ruins FilmPoster.jpeg
| caption        = 
| director       = Patrick Keiller
| producer       = Patrick Keiller
| writer         = 
| screenplay     = 
| story          = 
| starring       = Vanessa Redgrave (narrator)
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = British Film Institute
| released       =  
| runtime        = 101 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
Robinson in Ruins is a 2010 British documentary film by Patrick Keiller and narrated by Vanessa Redgrave. It is a sequel to Keillers previous films, London (1994) and Robinson in Space (1997).  It documents the journey of the fictional titular character around the south of England. It premiered at the 67th Venice International Film Festival in September 2010. 

==Overview==
Vanessa Redgrave assumes the role of narrator, after the death of Paul Scofield, Companion of Honour|C.H., Commander of the Order of the British Empire|C.B.E., who had narrated the previous films. Redgrave takes the part of the previous narrators former lover. As her predecessor she guides us through the adventures of the unseen, titular character, Robinson. There is a special focus on the importance of nature as we are guided through the picturesque surroundings of Oxfordshire and Berkshire. Keiller weaves the surreal, philosophy, architecture, the arts, science, politics, history and agriculture in this exploration of the natural world. 

==Release==
The World Premiere was on September 4 at the 67th Venice International Film Festival.  It was also be screened at the New York Film Festival on 26 September 2010.  It was released and distributed by the British Film Institute on 19 November 2010. 

==References==
 

==External links==
*  

 
 
 
 
 
 