Dead Ringer (1964 film)
{{Infobox film
| name           = Dead Ringer
| image          = DR1964.jpg
| image_size     = 180px
| caption        = Theatrical release poster
| director       = Paul Henreid
| producer       = William H. Wright
| based on       = La Otra aka Dead Pigeon
| writer         = Story:   Oscar Millard
| starring       = Bette Davis Karl Malden  Peter Lawford Philip Carey  Jean Hagen
| music          = André Previn
| cinematography = Ernest Haller
| editing        = Folmar Blangsted
| studio         = Warner Bros
| distributor    = Warner Bros. Pictures
| released       =  
| runtime        = 115 minutes
| country        = United States English
| budget         = $1,200,000
}}
Dead Ringer, also known as Who is Buried in my Grave? is a 1964 thriller film made by Warner Bros. It was directed by Paul Henreid from a screenplay by Oscar Millard and Albert Beich from the story La Otra  by Rian James. The music score was by André Previn and the cinematography by Ernest Haller. The film stars Bette Davis, Karl Malden and Peter Lawford with Philip Carey, Jean Hagen, George Macready, Estelle Winwood, George Chandler and Cyril Delevanti. 
 A Stolen Life. For this reason, Dead Ringer is sometimes mistakenly listed as a remake of A Stolen Life. 

==Plot== cocktail lounge, is losing money and she is threatened with eviction for not paying her bills. 
 fire poker to cover her tracks.

Meanwhile, Ediths boyfriend, police sergeant Jim Hobbson (Karl Malden) visits "Margaret" several times, asking questions about the death of Edith, whom he loved. Ediths scheme runs into trouble when Margarets lover Tony (Peter Lawford) sees through her disguise. Tony blackmails her for killing her sister, and receives very expensive jewelry as payment. Edith then learns that Margaret and Tony conspired to murder Frank by poisoning him with arsenic. Tony and Edith quarrel; when he threatens her, Margarets Great Dane attacks and kills him. 

Jim Hobbson has become suspicious about DeLorcas death and leads an investigation in which the police eventually exhume Franks body and find traces of arsenic. When Jim arrives to arrest her, Edith confesses her true identity. Jim is repulsed and does not believe her, telling her "Edith would never hurt a fly." 

Edith (as Margaret) is tried, found guilty of murder, and sentenced to death. As she is taken away from the courthouse, a troubled Jim approaches her and asks if she really is Edith. She reminds him that "Edith would never hurt a fly" and departs.

==Cast==
  
* Bette Davis as Margaret DeLorca/Edith Phillips  
* Karl Malden as Sergeant Jim Hobbson  
* Peter Lawford as Tony Collins  
* Philip Carey as Sergeant Hoag  
* Jean Hagen as Dede Marshall  
* George Macready as Paul Harrison 
 
* Estelle Winwood as Dona Anna  
* George Chandler as George the Chauffeur  
* Cyril Delevanti as Henry, the Butler  
* Bert Remsen as Daniel Dan Lister the Bartender  
* Ken Lynch as Captain Johnson
 

==Notes== Rosedale Cemetery in Los Angeles.

The jazz combo in Edies Bar was composed of electronic organist Perry Lee Blackwell and drummer Kenny Dennis, both noted musicians, but uncredited in the film.
 A Stolen Life were created by him, and he would improve upon the process here. Makeup artist Gene Hibbs was also hired due to his unique talent for making older actresses look younger through a "painting" technique. 
 Mexican film La Otra, directed by Roberto Gavaldón and starring Dolores del Río.  Dead Ringer itself was remade in 1986 as Killer in the Mirror, a made-for-television movie starring Ann Jillian. 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 