The Sentimental Bloke (musical)
{{Infobox Musical
|name= The Sentimental Bloke
|image= 
|caption=
|music=
|lyrics=
|book=Albert Arlen Nancy Brown
|basis= The Songs of a Sentimental Bloke
|productions= 1961 Canberra
}}
The Sentimental Bloke is a 1961 Australian musical based on Songs of a Sentimental Bloke. It is one of the most successful Australian musicals of the 20h century and is frequently revived. 

The writers had worked on the play since 1950. 

==Film Version==
{{Infobox film
| name           = The Sentimental Bloke
| image          = 
| image size     =
| caption        = 
| director       = Alan Burke
| producer       = Michael Shrimpton
| writer         = Alan Burke
| based on = 
| narrator       =
| starring       = Graeme Blundell Geraldine Turner Jimmy Hannan Nancye Hayes
| music          = 
| cinematography = 
| editing        = 
| studio = ABC
| distributor    = 
| released       = 1976
| runtime        = 
| country        = Australia
| language       = English
| budget         = 
| gross = 
| preceded by    =
| followed by    =
}}
The musical was filmed for ABC TV in 1976.  Ed. Scott Murray, Australia on the Small Screen 1970-1995, Oxford Uni Press, 1996 p139 

==References==
 

==External links==
*  at AusStage
*  at IMDB

 
 
 
 