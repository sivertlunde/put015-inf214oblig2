Tiny Times
{{Infobox film
| name = Tiny Times
| image = Tiny Times poster.jpg
| image size = 
| border = 
| alt = 
| caption = 
| director = Guo Jingming
| producer =  Li Li  An Xiaofen  Adam Tsuei  Zhou Qiang  Angie Chai
| writer = Guo Jingming
| screenplay = Guo Jingming
| story = 
| based on = Tiny Times 1.0 by Guo Jingming
| narrator =  Hayden Kuo Xie Yilin   Chen Xuedong
| music = Hou Zhijian
| cinematography = Randy Che 
| editing = Gu Xiaoyun
| studio = He Li Chen Guang Media  EE-Media  Star Ritz Prods.  H&R Century Pictures  Beijing Forbidden City Film  Le Vision Pictures (Tianjin)  Le Vision Pictures  Shenzhen Desen Intl. Media  Amazing Film Studio  Comic Ritz Film & TV Culture  Mission Media Investment 
| distributor = China Film Group Cooperation (China) Dasheng International Media (China) Le Vision Pictures Co. (China)
| released =  
| runtime = 115 minutes 
| country = China 
| language = Mandarin
| budget = ¥45 million (estimated) 
| gross = ¥484 million   (US$79.7 million) 
}}

Tiny Times (Chinese: 小时代), also known as Tiny Times 1.0    is a 2013 Chinese romance drama film written and directed by Guo Jingming and based on the best-selling novel of the same name also by Guo.    The story follows the films narrator and protagonist Lin Xiao,  played by Yang Mi, along with her best friends Gu Li, Nan Xiang, Tang Wanru, as they navigate between relationships, work and friendship in Shanghai.  {{cite web|url=http://www.chinafile.com/rite-passage-nowhere|title=A Rite of Passage to Nowhere: 
Tiny Times, Chinese Cinema, and Chinese Women|author=Ying Zhu and Frances Hisgen|publisher=China File|date=2013-07-15}}   {{cite web|url=http://www.nytimes.com/2013/09/04/movies/in-tiny-times-movies-material-girls-have-a-nation-tsking.html|
title=A Film-Fueled Culture Clash Over Values in China: In ‘Tiny Times’ Movies, Material Girls Have a Nation Tsking|author=Sheila Melvin|publisher=New York Times|date=2013-09-03}} 

The film received mostly negative reviews from Chinese film critics, although it was a commercial success. A sequel titled Tiny Times 2, which was filmed together with the first film and based on the second-half of the novel, was released on August 8, 2013. Tiny Times 3, the third installment of the Tiny Times series was released on July 17, 2014.

==Plot==
The film is based on the first half of Tiny Times 1.0, the first novel in Guos Tiny Times series.  It depicts the friendship among four young girls, Lin Xiao, Gu Li, Nan Xiang, Tang Wanru from Lin Xiao’s perspective in Shanghai, illustrating the titanic transformation of their philosophies. 
The four young girls are classmates in high school and roommates in college. On campus, they start their internships and cope with intractable affairs happening continually. After graduation, they continue their correspondence, suffused with misunderstanding and jealousy. However, they have all changed significantly.

==Cast==
* Yang Mi as Lin Xiao
* Ko Chen-tung as Gu Yuan
* Amber Kuo as Gu Li
* Rhydian Vaughan as Gong Ming Bea Hayden Kuo as Nan Xiang
* Xie Yilin as Tang Wanru
* Chen Xuedong as Chong Guang
* Li Yi feng as Jian Xi
* Shang Kan as Kitty
* Jiang Chao as Xi Cheng
* Calvin Tu as Wei Hai
* Ding Qiaowei as Yuan Yi
* Wang Lin as Ye Chuanping
* Yolanda Yang as Lin Quan

==Release and reception==
===Box office===
The film grossed US$79.7 million at the Chinese box office.   

===Accolades===
{| class="wikitable" style="width:75%;"
|+List of awards and nominations
! | Award
! | Category
! | Nominee
! | Result
|- China Movie Channel Media Awards    || Best Feature ||||   (tied)
|-
| Best New Director || Guo Jingming ||  
|}

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 


 