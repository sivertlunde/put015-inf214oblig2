The Great Ecstasy of Woodcarver Steiner
{{Infobox film
| name           = The Great Ecstasy of Woodcarver Steiner
| image          = The Great Ecstasy of the Woodcarver Steiner (Herzog, 1974) 000020387.jpg
| image_size     =
| caption        = Opening shot of the film.
| director       = Werner Herzog
| narrator       = Werner Herzog
| producer       = Werner Herzog
| writer         = Werner Herzog
| starring       = Walter Steiner Werner Herzog Popol Vuh
| cinematography = Jörg Schmidt-Reitwein
| editing        = Beate Mainka-Jellinghaus
| studio         = Werner Herzog Filmproduktion Süddeutscher Rundfunk|Süddeutscher Rundfunk (SDR)
| distributor    = Werner Herzog Filmproduktion 1974
| runtime        = 45 min.
| country        = West Germany German English English
| budget         = DEM 72.000 (estimated)
| preceded_by    =
| followed_by    =
}}
 documentary film German filmmaker Werner Herzog.  It is about celebrated ski-jumper Walter Steiner who works as a carpenter for his full-time occupation. Herzog has called it "one of my most important films."  {{cite book
  | last = Herzog
  | first = Werner
  | title = Herzog on Herzog
  | publisher = Faber and Faber
  | year = 2001
  | isbn = 0-571-20708-1 }} 

==Production==
The film includes footage shot in the German towns of Oberstdorf and Garmisch-Partenkirchen, as well as Planica in Yugoslavia. The film was made as part of a series for a German television station, which restricted in some ways the content. Herzogs original cut was 60 minutes long, but it was edited down to 45 minutes to fit in a one hour television spot. Also, the station required Herzog himself to appear on camera, which he had not typically done in his documentaries until this point. 

==References==
 

==External links==
* 
*  at Fanzine

 

 
 
 
 
 
 
 
 


 