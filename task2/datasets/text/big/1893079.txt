Battle Circus (film)
{{Infobox film 
| name           = Battle Circus
| image          = Battle Circus - 1953 - poster.png
| caption        = 1953 film poster
| director       = Richard Brooks
| producer       = Pandro S. Berman
| writer         = Richard Brooks Allen Rivkin Laura Kerr Robert Keith
| cinematography = John Alton
| music          = Lennie Hayton
| editing        = George Boemler
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 90 minutes
| language       = English
| country        = United States
| budget         = $1,201,000   
| gross = $2,362,000  
}}
 Robert Keith.

The film is set in Korea during the Korean War. Bogart (in his sole film for Metro-Goldwyn-Mayer ) plays a surgeon and commander of Mobile Army Surgical Hospital (MASH) 8666 (shortened to "66" in the dialogue),  with Allyson playing a newly arrived nurse. Despite their initial handicaps, their love flourishes against a background of war, enemy attacks, death and injury.
 

==Plot==
A young Army nurse, Lieutenant Ruth McGara (June Allyson), is newly assigned to the 8666th MASH mobile field hospital, constantly on the move during the Korean War.  At first, Ruth is a bumbling addition to the nurse corps, but attracts the attention of the units hard-drinking, no-nonsense chief surgeon Dr. and Major Jed Webbe (Humphrey Bogart). Jed cautions that he wants a "no strings" relationship and Ruth is warned by the other nurses of his womanizing ways. She sees that he is beloved by the unit, especially the resourceful Sergeant Orvil Statt (Keenan Wynn).
 Robert Keith). William Campbell) fly desperately needed blood supplies at night, even in the teeth of a fierce storm. When the helicopter lands safely, Jed goes on a binge, forcing Whalters to make his chief surgeon either straighten up or ship out. When a now more assured Ruth treats some North Korean prisoners, a frightened prisoner with a concealed grenade is calmly disarmed by her soothing words.

After a North Korean advance forces the MASH unit to escape through enemy lines, the lovers are temporarily separated. When the unit commander is wounded in an attack, Jed has to assume leadership to lead the unit out of danger. Traveling cross-country, he sets out on a perilous journey, attempting to meet up with the nurses who have gone on ahead. Eventually the two caravans safely negotiate the battlefield, and Jed and Ruth are reunited.

==Cast==
 
As appearing in Battle Circus, (main roles and screen credits identified):   IMDb. Retrieved: October 18, 2012. 
* Humphrey Bogart as Major Jed Webbe
* June Allyson as Lieutenant Ruth McGara
* Keenan Wynn as Sergeant Orvil Statt Robert Keith as Lieutenant Colonel Hilary Whalters William Campbell as Captain John Rustford
* Perry Sheehan as Lieutenant Laurence
* Patricia Tiernan as Lieutenant Rose Ashland
* Adele Longmire as Lieutenant Jane Franklin
* Jonathan Cott as Adjutant
* Ann Morrison as Lieutenant Edith Edwards
* Helen Winston as Lieutenant Graciano
* Sarah Selby as Captain Dobbs
* Danny Chang as Korean child
* Philip Ahn as Korean prisoner Steve Forrest as Sergeant

==Production==
 .]]
According to Richard Brooks (in an interview filmed for the 1988 Bacall on Bogart documentary), Battle Circus was originally called MASH 66, a title rejected by MGM because the studio thought people would not understand the connection to a military hospital. The title of the film actually refers to the speed and ease with which a MASH unit, with its assemblage of tents, and portable equipment, can, like a circus (performing art)|circus, pick up stakes and move to where the action is. The films technical advisor, Col. K. E. Van Buskirk had commanded one of the first MASH units in Korea, and ensured that the MASH and aerial scenes were authentic.   Turner Classic Movies. Retrieved: October 18, 2012. 
 typecast her and she was encouraged by the studio to attempt more serious fare. 

In retrospect, Bogart did not enjoy working on the film, the least of which was when he burnt his left thumb, a scene that was left in the final production.  His $250,000 fee was the sole compensation.  Bogart told Brooks, a close friend, "lets not make any more movies together." 
 Camp Pickett, Virginia, where MASH units trained. The aerial sequences with the Bell 47 helicopters were filmed there. Miller, Frank.   Turner Classic Movies. Retrieved: October 18, 2012.  The camp commandant offered the film crews use of the base facilities, including his house for the lead actors, after initial scenes were finished. 

==Reception==
According to MGM records the film earned $1,627,000 in the US and Canada and $735,000 overseas resulting in a profit of $265,000. 

While commending Battle Circus for a revealing and engrossing wartime drama, reviewers noted that the tepid love story distracted. The review in The New York Times noted, "Unfortunately for the general pace and impact, considerable time is allotted to a dawdling and familiar personal drama, the romance of an Army surgeon and a rookie nurse." 

== See also ==
 
* List of American films of 1953
* M*A*S*H (movie)|M*A*S*H

==References==

===Notes===
 

===Bibliography===
 
* Allyson, June and Frances Spatz Leighton. June Allyson by June Allyson. New York: G.P. Putnams Sons, 1982. ISBN 0-399-12726-7.
* Porter, Darwin. Humphrey Bogart: The Making of a Legend. New York: Blood Moon Productions, 2010. ISBN 978-1-93600-314-3.
* Sperber, A.M. and Eric Lax. Bogart. New York: William Morrow & Co., 1997. ISBN 0-688-07539-8.
 

==External links==
 
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 