The Fourth Estate (film)
{{Infobox film
| name           = The Fourth Estate: A Film of a British Newspaper
| image          =
| image size     =
| caption        =
| director       = Paul Rotha
| producer       = Patrick Moyna
| writer         = Paul Rotha, Basil Wright, Carl Mayer Nicholas Hannen,  Dennis Arundell
| starring       =
| music          = Walter Leigh
| cinematography = James E. Rogers, Harry Rignold, A.E. Jeakins
| editing        =
| distributor    = 
| released       = 
| runtime        = 5,659 feet (63 minutes at 24fps)
| country        = United Kingdom
| language       = English
| budget         =
}} sponsored by the owners of The Times, and depicts the preparation and production of a days edition of the newspaper.
 Documentary Movement Ministry of Information were reluctant to sanction its release on the grounds that it depicted life in peacetime London, which would no longer be accepted by viewers as realistic.   However, Rotha himself claimed that the films sponsor was reluctant to release The Fourth Estate in the belief that it implicitly criticised The Times from a leftist perspective, portraying it as the mouthpiece of the establishment. 
 Weimar screenwriter, who by this time was living in Britain as an exile from the Nazis, acted as a scenario consultant to the film.

In 2012, the first public screening of the full film was at the University of Leeds using film print from the archive of the British Film Institute (BFI).   

==References==
 

==External links==
*   at the British Film Institutes SIFT database.
*  

 
 
 
 
 
 
 
 
 
 


 