Dragon Ball Z: Super Android 13!
{{Infobox film name = Dragon Ball Z: Super Android 13! image = DBZmovie7 Japan.gif caption = Japanese box art director = Kazuhito Kikuchi producer = Chiaki Imada Rikizô Kayano writer = Story:   starring = See   music = Shunsuke Kikuchi cinematography = editing = distributor = released = July 11, 1992 February 4, 2003 in North America and Europe runtime =  46:15 gross = ¥2.35 billion ($18.9 million)
}}

Dragon Ball Z: Super Android 13, known in Japan as  , is the seventh   movie and the   and in a double feature on February 10, 2009. Early concept art for the reissue used the title "Android Assault", but the final product went back to using FUNimations original title for the film. It was re-released to DVD again in a movie 4-pack with   and the next two films in the series on December 6, 2011.

==Plot== Androids 17 18 including the Red Ribbon Army, but also reveals that Gero previously copied his consciousness into an underground supercomputer, working on an alternate ultimate android.

Cutting to the present day, Goku is shopping with Gohan and Chi-Chi, while Krillin, Master Roshi, Oolong and Future Trunks wait for a beauty pageant. As Goku, Chi-Chi, Gohan and the gang eat in the restaurant on the shopping centres top floor, two humanoid beings enter the city and begin causing mayhem. Oblivious to their surroundings, they head straight for the restaurant and attack Goku. Goku briefly fights them and realises that they are androids, since he cannot sense their energy. The humanoids introduce themselves as Androids 14 and 15 as Gohan, Krillin and Trunks join the fray. Goku requests that they take the battle elsewhere in order to avoid harming innocent people, and the androids accept, flying to an Arctic area.

Goku and Trunks take on the androids and manage to hold their own until an android wearing the Red Ribbon Army insignia appears. This android introduces himself as Android 13, and explains that Dr. Gero may be gone, but previously programmed his supercomputer to think that it is Gero himself to continue Geros dream to killing Goku in revenge for defeating the Red Ribbon Army. Vegeta and Piccolo also show up to aid their friends. Goku, Vegeta and Trunks power up to their Super Saiyan forms. While Android 13 manages to hold the upper hand against both Goku and Piccolo, Trunks and Vegeta destroy Androids 14 and 15, and surround 13, ready to take him on at once. Irate, 13 proceeds to absorb 14 and 15s main cores into his own being and undergoes a hideous transformation into Super Android 13. In this form, nobody is able to match 13, and he pummels Goku and the others about.

Seeing no other choice, Goku begins summoning energy for the Spirit Bomb while Piccolo and the others stall 13. 13 eventually gets wind of this and tries to stop Goku, but Piccolo holds him off just long enough for Goku to go Super Saiyan again and merge with the Spirit Bomb. 13 attacks, but Goku effortlessly punches through 13s stomach and sends him into the core of the Spirit Bomb, where he is obliterated. With 13s demise, the underground supercomputer shuts down for good.

Following 13s death, Krillin and Gohan are sent to the hospital. There everyone jokes about Krillins attempt to help Gohans summer school (which Chi-Chi had previously been stressed about at the beginning of the film). Elsewhere, Piccolo and Vegeta sit back to back on an iceberg, isolated from the celebration and then Piccolo asked was it over and Vegeta said not until the fish jumps and then he says its over.

==New Characters==

===Android 13===
  was one of the original androids that took a long time to complete, along with Android #14 and Android #15, thus being activated after #16, #17, #18, #19, Gero and Cell. He was designed to serve Geros vendetta against Goku, who overthrew the Red Ribbon Army as a child. Android 13 is completed by Dr. Geros self-aware supercomputer later than androids 14 and 15, who by this time are already taking up arms against Goku and Future Trunks. Upon his arrival on the Glacier, Android 13 confronts Goku, leaving Trunks and Vegeta to struggle against 14 and 15, shortly after which Piccolo intervenes to assist Goku, as well. Ultimately, androids 14 and 15 are defeated following Trunks and Vegetas transformations to Super Saiyan, however even their deaths are fail-safe in Geros plan, as 13 pauses to absorb components from 14 and 15, resulting in his fusion into Super Android 13. Vegeta is the first to attack the newly transformed super android, only to be quickly brought to his knees by 13s amazing new strength. Goku, Future Trunks and Piccolos attempts are also foiled with little effort on 13s part. Becoming desperate to stop the android threat before he grows out of control, Goku is forced to prepare his ultimate weapon, the Super Spirit Bomb, whilst Vegeta, Piccolo, Gohan and Trunks distract 13. The Spirit Bombs effectiveness is amplified when Goku makes a transformation to Super Saiyan during the process of gathering energy. Krillin states that only a calm-hearted person can sustain a Spirit Bomb, and when Goku transforms into his Super Saiyan form, his body absorbs the Spirit Bombs immense energy. When 13 lunges at Goku, one punch from Goku sends the super android into the depths of the Spirit Bomb, where he is disintegrated by its ki.

In the English dub of the film, Android 13s demeanor is based on the stereotypical depictions of rednecks, with Future Trunks even addressing the android as a "Red Ribbon redneck" at one point. In addition, in the original Japanese version, Android 13, upon becoming Super Android 13, became completely silent aside from yelling "Son Goku!" when he was defeated.

==Censorship== punches him in the groin, replacing it with a white flash of light. This scene is frequently used in internet memes.

*In addition, one of Super Android 13s lines when attacking Goku was cut in the edited version due to his shouting "Damn you!" Similar dialogue edits were made in Android 13s introduction, where two of his lines were altered slightly to remove the use of the term "ass."

==Music==
*OP (Opening Theme)
*# "Cha-La Head-Cha-La (single)|CHA-LA HEAD-CHA-LA" (Chara Hetchara) (OP animation 1)
*#* Lyrics:  , Vocals: Hironobu Kageyama

*ED (Ending Theme)
*# "GIRI GIRI Sekai Kyokugen"
*#* Lyrics:, Music:, Arrangement:, Vocals: Hironobu Kageyama & Yuka (singer)|Yuka

The score for the English-language version was composed by Mark Menza, however the remastered release contains an alternate audio track containing the English dialogue and Japanese background music.

==Cast==
{| class="wikitable"
|-
! Character Name
! Voice Actor (Japanese)
! Voice Actor (English)
|-
| Goku|| Masako Nozawa|| Sean Schemmel
|- Gohan || Masako Nozawa || Stephanie Nadolny
|- Piccolo || Toshio Furukawa || Christopher Sabat
|-
| Krillin || Mayumi Tanaka || Sonny Strait
|- Future Trunks|| Eric Vale
|-
| Vegeta|| Ryo Horikawa || Christopher Sabat
|- Master Roshi || Kôhei Miyauchi || Mike McFarland
|- Naoko Watanabe|| Cynthia Cranz
|- Oolong || Naoki Tatsuta|| Bradford Jackson
|-
| Android 13/Super 13 || Kazuyuki Sogabe|| Phillip Wilburn
|-
| Android 14 || Hisao Egawa|| Chris Rager
|-
| Android 15 || Toshio Kobayashi || Josh Martin
|- Kent Williams
|-
| Narrator || Joji Yanami || Kyle Hebert
|}

==External links==
*   of Toei Animation
*  
*  

 

 
 
 
 
 