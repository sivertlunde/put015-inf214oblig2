Hammer the Toff
{{Infobox film
| name           = Hammer the Toff
| image          = Hammerthetoff.jpg
| caption        = 
| director       = Maclean Rogers
| producer       = Ernest G. Roy
| writer         = John Creasey John Bentley Patricia Dainton Valentine Dyall
| cinematography = Geoffrey Faithfull
| editing        = Jim Connock
| music          = 
| distributor    = Butchers Film Service
| released       =   
| runtime        = 71 minutes
| country        = United Kingdom English
}} John Bentley and Patricia Dainton.  The film was based on the 1947 novel of the same name by John Creasey, the 17th in the series featuring upper-class sleuth Richard Rollison, also known as "The Toff".  This film and another Toff adaptation Salute the Toff were shot back-to-back at Nettlefold Studios in the summer of 1951 with identical production credits and many of the same actors.  Hammer the Toff was issued to cinemas in  March 1952 as the sequel to Salute the Toff.  There would be no further entries in the series.
 75 Most Wanted" list of missing British feature films.   

==Plot==
On the train to the seaside resort of Brighthaven, Richard Rollison (Bentley) is sharing a carriage with an attractive young lady called Susan Lancaster (Dainton).  The journey is rudely interrupted when the window of the carriage is shattered by a barrage of bullets.  Richard learns from the shaken Susan that she is on her way to join an uncle on holiday, and offers to escort her safely to her hotel.  They learn that her uncle has disappeared, but has left Susan a package.  Later, Rollison happens to overhear a pair of shady characters discussing how to kidnap Susan.  She explains that her uncle has developed a secret formula which sinister characters are keen to get their hands on, and they have been receiving threats of menace, hence the flight to Brighthaven.

Rollison consults his old colleague Inspector Grice of Scotland Yard, who tells him that the evidence is pointing in the direction of a particular man as being responsible for the abduction.  Using his friends and contacts in the East End, Rollison investigates, while Susan is being kidnapped and tied up.  Rollison finally succeeds in identifying the criminals and their leader "The Hammer", releasing Susan and proving that the man suspected by the police is innocent.

==Cast== John Bentley as Richard Rollison
* Patricia Dainton as Susan Lancaster
* Valentine Dyall as Inspector Grice John Robinson as Linnett
* Wally Patch as Bert Ebbutt
* Roddy Hughes as Jolly
* Basil Dignam as Superintendent
* Lockwood West as Kennedy Katharine Blake as Janet Lord Charles Hawtrey as Cashier Ian Fleming as Doctor Lancaster

==Reception==
Like its predecessor, Hammer the Toff was well received by critics as good quality popular B-movie entertainment.  Kine Weekly described it as "well staged, with a bright line in dialogue, and neat crime angles", while the Daily Film Renter termed it "lively, easily-assimilated strong-arm stuff with a whiff of comedy and a dash of romance". 

==See also==
*List of lost films

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 