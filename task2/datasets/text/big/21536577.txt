Puce Moment
{{Infobox film
| name           = Puce Moment
| image          = 
| caption        = 
| director       = Kenneth Anger
| producer       = 
| writer         = 
| starring       = Yvonne Marquis
| music          = Jonathan Halper
| cinematography = 
| editing        = 
| distributor    =
| released       =  
| runtime        = 6 mins
| country        = United States
| awards         = 
| language       = 
| budget         = 
}}
 short 6-minute film by Kenneth Anger. Filmed in 1949, Puce Moment resulted from the unfinished short film Puce Women. The film opens with a camera watching 1920s-style flapper gowns being taken off a dress rack. The dresses are removed and danced off the rack to music. A long-lashed woman, Yvonne Marquis, dresses in the purple puce gown and walks to her vanity to apply perfume. She lies on a chaise longue which then begins to move around the room and eventually out to a patio. Borzois appear and she prepares to take them for a walk.

==Production==

The original soundtrack was Verdi opera music; in the 1960s, Anger re-released the film with a new psychedelic folk-rock soundtrack performed by Jonathan Halper.
 Sampson De Brier, a silent film actor, who later appeared in Angers Inauguration of the Pleasure Dome (1954).

Anger attempts to recreate silent era style by using alternating camera speeds. Curtis Harrington was a cinematographer on the film.

Yvonne Marquis, who was Angers cousin,  moved to Mexico shortly after the film was made. Anger reveals Marquis was a mistress to Lázaro Cárdenas, the Former President of Mexico.

==References==
 

==External links==
* 

 

 
 

 