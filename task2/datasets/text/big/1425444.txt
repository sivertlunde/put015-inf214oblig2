So Close
 
 
 
{{Infobox film
| name = So Close
| image = Soclose.jpg
| alt = 
| caption = Film poster
| film name = {{Film name| traditional = 夕陽天使
 | simplified = 夕阳天使
 | pinyin = Xīyáng Tiānshǐ
 | jyutping = Zik6-joeng4 Tin1-si2}}
| director = Corey Yuen
| producer = Chui Bo-chu
| writer = Jeffrey Lau
| starring = Shu Qi Karen Mok Song Seung-heon Zhao Wei
| music = Sam Kao Kenjo Tan
| cinematography = Venus Keung Chan Chi-ying Cheung Ka-fai
| studio = Eastern Film Production Columbia Pictures
| distributor = Columbia Pictures Sony Pictures Classics
| released =  
| runtime = 110 minutes
| country = Hong Kong
| language = Cantonese
| budget = 
| gross = HK$880,332   
}} Close to You", which has a prominent role in the film.

==Plot==
Lynn and her sister Sue are computer hackers, assassins and espionage specialists who use their late fathers secret satellite technology to gain an advantage over their rivals and law enforcement agents. At the beginning of the film, they infiltrate a high security building and assassinate Chow Lui, the chairman of a top company in Hong Kong.

After their successful mission, a police inspector named Kong Yat-hung is assigned to investigate the case and she manages to track down the assassins. In the meantime, Chow Luis younger brother Chow Nung, who hired Lynn and Sue to kill his brother so that he can become the chairman, wants to kill the assassins to silence them. The cat-and-mouse chase becomes more complicated as both the police and the thugs are out to get Lynn and Sue.

Sue has always been playing the role of the assistant by staying on the computer and helping to disable the security systems and giving instructions on navigating the area, while Lynn, who is older and more experienced, does all the field work. Sue is jealous and thinks that Lynn refuses to let her participate more actively because she is less adept, but actually Lynn is trying to protect her sister from danger. Their relationship becomes strained when Lynn falls in love with her friends cousin Yen and wants to give up her job and marry Yen. Sue intends to continue her career as a contract killer so that she can prove that she is as good as her sister.

Kong Yat-hung tracks down Sue in a bakery, where Sue is buying a birthday cake, and this leads to a frantic car chase. When Sue realises that she is being cornered by the police, she calls Lynn at home and asks her sister for help. At the same time, Chow Nungs assassins break into the house and kill Lynn and frame Kong Yat-hung for the murder. Sue escapes from the police and finds out the true identities of her sisters killers from the CCTVs in the house. She goes to see Kong Yat-hung and offers to help her clear her name, but Kong must assist her in avenging her sister. Left with no choice, Kong Yat-hung agrees to team up with Sue to hunt down and kill Chow Nung and his henchmen.

==Cast==
* Shu Qi as Lynn
* Karen Mok as Kong Yat-hung
* Zhao Wei as Sue
* Song Seung-heon as Yen
* Michael Wai as Ma Siu-ma Kurata Yasuaki as Master
* Deric Wan as Chow Nung
* Shek Sau as Chow Lui
* Josephine Lam as Alice Chow
* Ben Lam as Ben
* Ricardo Mamood as Peter
* May Kwong as May
* Henry Fong as Lynn and Sues father
* Paw Hee-ching as Lynn and Sues mother
* Tats Lau as Ghost King
* Kam Hing-yin as Captain
* Josie Ho as Ching
* Jude Poyer as murderer
* Dave Taylor as murderer
* David John Saunders as murderer
* Leon Head as murderer
* Lam Seung-mo as Lai Kai-joe
* Leo Ku as man in lift
* Wong So-bik as May
* Ben Yuen as Mr Yeung
* Huang Kaisen as Wong Fat-chi
* Victy Wong as bodyguard
* Wong Wai-fai as bodyguard
* So Wai-nam as bodyguard
* Kong Foo-keung as robber
* John Cheung as cop
* Keung Hak-shing as Lee Hong-fai
* Adam Chan
* Tanigaki Kenji
* Lam Kwok-git

==Reception==
So Close holds an average rating of 6.7/10 based on 37 reviews on Rotten Tomatoes  and an average score of 66/100 based on 18 reviews on Metacritic. 

==Awards & Nominations==
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 2px #aaa solid; border-collapse: collapse; font-size: 90%;"
|- bgcolor="#CCCCCC" align="center"
! colspan="4" style="background: LightSteelBlue;" | Awards
|- bgcolor="#CCCCCC" align="center"
!Award
!Category
!Name
!Outcome
|-
|-style="border-top:2px solid gray;"
|Central Ohio Film Critics Association Best Foreign-Language Film
|
| 
|-
|Hong Kong Film Awards Best Action Choreography Corey Yuen Jianyong Gao 
| 
|-
|}

==See also==
* Girls with guns

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 