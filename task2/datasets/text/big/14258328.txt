The Dance (film)
{{Infobox film
| name           = The Dance 
| image          = 
| caption        = 
| director       = Ágúst Guðmundsson
| producer       = Ágúst Guðmundsson
| writer         = Kristín Atladóttir  Ágúst Guðmundsson  William Heinesen (short story)
| starring       = Gunnar Helgason  Baldur Trausti Hreinsson
| music          = 
| cinematography = Ernest Vincze
| editing        = 
| distributor    = 
| released       =  
| runtime        = 83 minutes
| country        = Iceland
| language       = Icelandic
| budget         = 
| gross          = 
}}

The Dance (  ( )) is a 1998 Icelandic drama film produced and directed by Ágúst Guðmundsson. It is based on the short story Her skal danses by William Heinesen from Faroe Islands. It was both filmed in Faroe Islands and Iceland. It was entered into the 21st Moscow International Film Festival where Guðmundsson won the Silver St. George for Best Director.   

==Cast==
* Gunnar Helgason as Pétur
* Baldur T. Hreinsson as Ívar (as Baldur Trausti Hreinsson)
* Pálína Jónsdóttir as Sirsa
* Dofri Hermannsson as Haraldur
* Gísli Halldórsson as Nikulás
* Kristina Sundar Hansen as Anna Linda
* Saga Jónsdóttir as Salmóma Arnar Jónsson as Djákni
* Magnús Ólafsson as Sýslumaður
* Benedikt Erlingsson as Hólófernes

==References==
 

==External links==
*  
*  

 
 
 
 
 


 
 