The Storm (2009 film)
{{Infobox Film
| name           = The Storm
| image          = De Storm 2009 Poster.jpg
| caption        = Dutch-language poster for The Storm
| director       = Ben Sombogaart
| producer       = Johan Nijenhuis Alan de Levitta
| writer         = Marjolein Beumer Rik Launchpach
| narrator       = 
| starring       = Sylvia Hoeks Barry Atsma Dirk Roofthooft Monic Hendrickx Katja Herbers Lottie Hellingman
| music          = 
| cinematography = Piotr Kukla
| editing        = Herman P. Koerts
| distributor    = NL Film Productions Universal Pictures
| released       =  
| runtime        = 100 minutes
| country        = Netherlands
| language       = Dutch
| budget         = €6 million
| preceded_by    = 
| followed_by    = 
}}
The Storm ( .

==Plot==
A terrible storm causes hundreds of dikes to break in Zeeland, resulting in the North Sea flood of 1953. Julia, a single mother living with her parents, is caught in the middle of the catastrophic flood. She is rescued from drowning and taken to safety by her neighbour Aldo, who is a member of the armed forces. However, her baby is left in a wooden box in the attic of Julias parental home. Together Julia and Aldo return to the disaster area to look for the baby. When they finally find the box, it is empty, and they conclude that someone must have taken the child. The child ended up with a woman who recently lost her own baby in a car accident. Julia met her but because the woman did not want to lose the baby, she hid him for Julia. Eighteen years later, Julia meets her son and the woman again and she finds out what has happened.

==Cast==
*Sylvia Hoeks - Julia
*Barry Atsma - Aldo
*Dirk Roofthooft - Julias Father
*Monic Hendrickx - Julias Mother
*Katja Herbers - Krina
*Lottie Hellingman - Stientje

==American award==
In July 2010 The Storm won the Award for Outstanding Achievement in Filmmaking on the Stony Brook Film Festival in New York. Producer De Levita and actress Hoeks were attending.

==References==
 

==External links==
*  
*  

 
 
 
 