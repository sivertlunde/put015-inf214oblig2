Dívka v modrém
 
 
{{Infobox film
| name           = Dívka v modrém
| image          = 
| alt            = 
| caption        = 
| director       = Otakar Vávra
| producer       = Jan Sinnreich
| writer         = Otakar Vávra Felix de la Cámara Oldrich Nový
| starring       = Lída Baarová
| music          = 
| cinematography = Jan Roth
| editing        = Antonín Zelenka
| studio         = 
| distributor    = 
| released       =  
| runtime        = 87 minutes
| country        = Czechoslovakia
| language       = Czech
| budget         = 
| gross          = 
}}
Dívka v modrém  is a 1939 Czechoslovak comedy film directed by Otakar Vávra.   

==Cast==
* Lída Baarová - Komtesa Blanka z Blankenburgu
* Oldrich Nový - Jan Karas, notár v Lucine
* Ruzena Slemrová - Smrcinská
* Sylva Langova - Slávinka (as Sylva Langová)
* Antonie Nedošinská - Otýlie
* Natasa Gollová - Ruzenka Smutná
* Bedrich Veverka - Doktor Pacovský
* Frantisek Paul - Cádek
* Frantisek Kreuzmann - Musketýr
* Jindrich Láznicka - Houzvicka
* Eliska Pleyová - Majitelka módního salónu (as E. Pleyová)
* Vladimír Majer - Kastelan (as Vl. Mayer)
* Vladimír Repa - Velkostatkár Kabelka (as V. Repa)
* Josef Belský - Landa (as J. Belský)
* Jirí Vondrovic - Lékárníkuv syn (as V. Vondrovic)
* Bolek Prchal - Zapisovatel (as B. Prchal)
* Antonin Zacpal - (as Ant. Zacpal)

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 