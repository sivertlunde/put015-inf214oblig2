Blonde Fist
{{Infobox film
| name           = Blonde Fist
| image          = 
| caption        = 
| director       = Frank Clarke
| producer       = Joseph DMorais/Christopher Figg
| writer         = Frank Clarke
| starring       = Margi Clarke
| music          = 
| cinematography = 
| editing        = 
| studio         = Blue Dolphin Films
| distributor    = Blue Dolphin Films
| released       = 9 August 1991	
| runtime        = 102 minutes
| country        = United States
| language       = English
| budget         = £600,000 
| gross          = 
| followed by    = 
}}

Blonde Fist (or Blond Fist), is a 1991 film starring Margi Clarke as the protagonist Ronnie ODowd, who finds female boxing as an alternative to her domestic problems. 

==Plot==
The film revolves around the story of Ronnie O’Dowd, a woman who attempts to escape her domestic problems by fleeing to New York in search of her father. She finds him, and experiences new problems, some friendship, a romance, and an unexpected career as pro-boxer, to make ends meet.

At the near end of the film, Ronnie and her friends visit a nightclub, where a female boxing match was about to take place. The absence of one of the boxers led the ring announcer to issue a friendly challenge: 1000 dollars will go to the woman who lasts at least 3 minutes in the ring with his fighter. Ronnie eagerly accepts, intending to lasts the three minutes, but her opponent turned aggressive, forcing Ronnie to knock her out.

Ronnie later receives an invitation for another bout. This time, she faces off against a more skilled adversary, Crazy Sue. After getting knocked down twice, Ronnie finally managed to knock Sue out and win the prize

==Cast==
* Margi Clarke as Ronnie
* Ken Hutchison

==Reception==
The movie received a mixed reception from critics.  

==References==
 

==External links==
* 

 
 
 
 


 
 