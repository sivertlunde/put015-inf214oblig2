Rustum
 
{{Infobox film
| name = Rustum
| image =
| caption =
| director = A. Kodandarami Reddy
| producer = Venkanna Babu S.P.
| writer = Satyanand
| starring = Chiranjeevi Urvashi Rao Gopal Rao Allu Ramalingaiah
| music = K. Chakravarthy
| cinematography = Lok Singh
| editing = Vellaiswamy
| distributor =
| released =  
| runtime =
| country = India Telugu
| budget =
}} Tollywood movie directed by A. Kodandarami Reddy and has  Chiranjeevi in title role. The movie was released on 21 December 1984.

==Plot==
The movie starts with a panchayati scene under a tree in a village where RGR,the village head, judges a dispute between Mukkamala and a Rowdy.Mukkamala, a resident of the village, leaves it in some unknowncircumstances and returns to the village after 15 years. He learns that hisproperty (house) was owned illegally by a Rowdy and he is not willing togive the proprty back to Mukkamala. And hence the Panchayati.As usual, RGR (the Villain) supports the Rowdy and does injustice toMukkamala. Having no enough power to face the Rowdy or the village head, heagain leaves the village.
Hero fights with the Rowdy (staying in Mukkamala’s property) and owns hisproperty and does good to the village people. Village people start callinghim RUSTUM.Hero works for the village welfare and acts promplty for any help needed.RGR sets a trap to kick Hero out of the village. As a part of his trap, RGRgives the village head post to Hero. Mean while Hero and Oorvasi (RGR’sdaughter are in love). and also gets closer to Annapoorna and Gummadi, acouple that are affected by RGR’s injustice.Once Hero is suspected as the cause for a village girl’s suicide and wasasked to leave the village. Even the heroine suspects the hero.In the trial to prove that he is not the reason behind the suicide, helearns that GB is the main reason. RGR fixes up Satyanarayana, Gummadi’sbrother — who just gets released from prison, to kill Chiru.
Meanwhile, GB goes to city and tracks down Mukkamala and why Hero came andfought for Mukkamala’s property.GB learns the whole loop and passes it on to RGR.The loop is — Chiru, is none other than Gummadi’s son. Once when he was akid, sees a murder (done by RGR and fixes up Satyanarayana as murderer) andtries to escape from the murder scene. Mukkamala, who is Gummadi’s palEru,saves the kid and takes him to city.Searching for his son at the same time, Gummadi comes to murder spot and asper RGR’s fix, Gummadi believes that Satyanarayana is the murderer and hencegets him arrested.Mukkamala, Satyanarayana and Chiru plans to treat a good lesson to RGR anddo good to the village.
As a part of their plan, Satyanarayana and Chiru fights as if they are realopponents. But when RGR comes to know the truth from GB, he kidnaps thewhole Chiru’s family (Gummadi, Annapoorna, Mukkamala, Satyanarayana).Knowing the truth, Oorvasi helps Chiru in proving that RGR is a villain.The climax fight is very good shot in a Mango garden.After the climax fight there is one more twist. Chiru, the hero, comes in aninspector costume and arrests RGR. So, in the flash back, the kid-Chiru wasrescued by Mukkamala and he made the kid an inspector with the help ifSatyanarayana (with the money he earned while he was in prison).

==Cast==
*Chiranjeevi................Hari
*Urvashi................Padma (Rayudus daughter)
*Raogopal Rao................Ganga Rayudu
*Gummadi Venkateswara Rao................Brahmaiah Naidu
*Kaikala Satyanarayana................Rudraiah
*Allu Ramalingaiah................Lingam
*Suthi Velu................Panthulu
*Giribabu................Giribabu (Rayudus son)
*Nutan Prasad................Chalapathi
*Nirmalamma................Jaggamma
*Annapoorna (actress)|Annapoornamma................Parvathi
*Rajyalakshmi................Lakshmi
*Suthi Veerabhadra Rao................Chakali Rajendra Prasad................Punna Rao Saikumar
*Hari
*Suri
*Narasimha Rao
*Shubha (actress)|Shubha................Gowri
*K. Vijaya
*Mikkilineni Radhakrishna Murthy|Mikkilineni................Musalayya
*Chandana
*Chidatala Appa Rao
*Silk Smitha
*Bheemaraju................Ramudu
*Hemasunder
*Jagga Rao................Gangaiah
*Mithai Chitti
*Mallikharjuna Rao................Gopayya Chitti Babu
*Harish Kumar ......Child Actor (Child Chiranjeevi)

==Crew==
*Director: A. Kodandarami Reddy
*Writers: Girija Sribhagavan (story), Satyanand (dialogues)
*Producer: S.P. Venkanna Babu
*Music: K. Chakravarthy
*Cinematography: Lok Singh
*Codirector: Sarath
*Art: Sai Kumar, Vijaykumar (Assistant)
*Lyrics: Veturi Sundararama Murthy
*Playback Singers: S.P. Balasubramaniam & P. Susheela

== External links ==
*  

 
 
 
 
 