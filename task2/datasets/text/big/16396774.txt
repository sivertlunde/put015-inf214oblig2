The King of Fighters (film)
{{Infobox film
| name           = The King of Fighters
| image          = King-of-fighters-movie.jpg
| caption        = Official film poster
| director       = Gordon Chan
| producer       =  
| writer         =  
| based on       =  
| narrator       =
| starring       =  
| music          = Tetsuya Takahashi
| cinematography = Arthur Wong
| editing        = Kip-hop Chan
| studio         =  
| distributor    =   
| released       =  
| runtime        = 93 minutes
| country        = {{plainlist|
*Japan
*Taiwan
*United States
*Germany }}
| language       = English
| budget         = USD|$12 million
| gross          = $2,370,613 
}}
The King of Fighters is a 2010 science fiction-martial arts film based upon SNK Playmores The King of Fighters series of fighting games.     It stars Sean Faris as Kyo Kusanagi, Maggie Q as Mai Shiranui, Will Yun Lee as Iori Yagami, and Ray Park as Rugal Bernstein.   

==Plot==
 
At a museum in  , which would grant him limitless powers. However, the sword is revealed to be a fake - thus, Rugals quest is delayed. Mai Shiranui is told by an injured Chizuru Kagura that the whereabouts of the real sword are with Saisyu Kusanagi, who is housed at a mental institution. She is also warned that she alone must defeat Rugal and her boyfriend Iori Yagami should not be involved. At the institution, Mai meets a catatonic Saisyu and his son Kyo, but Ioris presence suddenly breaks Saisyus catatonic state, and the elder Kusanagi threatens to kill Iori before losing consciousness and dying.
 CIA agent Terry Bogard enters Chizurus room, demanding for information on Rugals whereabouts and the tournament. She tells him of the different dimensions that exist, but he does not believe a word. Chizuru then tells Terry to go to Seattle and ask Mai, who is actually an undercover operative sent by the CIA to infiltrate Chizurus organization a year ago.

At a cemetery in Seattle, where Saisyu is buried, Kyo and Iori confront each other once again. Iori explains that both Kusanagi and Yagami clans were destined to be enemies. Mai hitches a ride with Kyo to his home, where she explains to him that she is looking for the Kusanagi Sword. Kyo tells her that centuries ago, according to family folklore, a Yagami ancestor attempted to release the Orochi, but it consumed him with murderous rage. Kyos ancestor killed the Yagami and returned the Orochi into its world. Mai tells Kyo that Rugal is out to unleash the Orochi once again. Kyo, meanwhile, wants to confront Rugal, who destroyed his fathers mental state.

At a hotel, Mai and Kyo meet up with Iori and Terry. It is revealed that Rugal is using the tournament dimension to merge it with the real world. After Mai blows her cover in front of Kyo, Iori puts on his Bluetooth headset and enters the tournament dimension to confront Rugal. There, he defeats Rugals servants Mature and Vice in combat, only to have his mind consumed by the Orochi.

The next day, Kyo is lured into the tournament dimension, where he first fights Rugal and loses, but is allowed to live as a warning. This prompts him to bring out his ancestral sword and join Chizuru and Terry into the tournament after Mai is dragged in by Rugal. When the four meet up, they are separated into different dimensions, with Kyo fighting Rugal and Mai and Terry facing Mature and Vice. In the middle of a sword fight, Rugal is about to decapitate Kyo when Iori suddenly appears and intervenes. Here, Rugal reveals to Kyo that several years back, he battled Saisyu, Chizuru and Iori over control of the Orochi. During that fight, Iori allowed the Orochi to take over his body, defeating Rugal, but also destroying Saisyus mental state by bashing his head against a wooden barrier several times. This leads to a fight between Kyo and Iori. Kyo slashes Iori in the back, releasing the Orochi from his body.

Disappointed by the outcome of the fight, Rugal sends Kyo, Iori and Mai into another dimension to face them with his full potential. Chizuru and her multiple clones appear, revealing that she has found the mirror and the necklace. The heroes fail in their first attempt to combine the relics and trap Rugal, with Chizuru mortally wounded. Mai takes her place as the mirror holder, but as she, Kyo and Iori corner Rugal, they are once again overcome by his powers. Rugal destroys Kyos sword, but as he is about to finish him off with a fireball, Kyo magically generates a new sword to block it. He then throws the sword and destroys Rugal.

Back in the real world, Scott places a lantern on the ocean in memory of Chizuru. Kyo decides to keep the family tradition by continuing with the tournament. He reflects on his late fathers teachings while Iori stares at him from the other side of the pier.

==Cast==
*Sean Faris as Kyo Kusanagi, a Japanese-American motorcycle enthusiast and the son of martial artist Saisyu Kusanagi. He is also a descendant of the Kusanagi clan.  
:*Keanu Lam as young Kyo CIA operative sent to infiltrate the King of Fighters tournament.
*Will Yun Lee as Iori Yagami, a King of Fighters participant and descendant of the Yagami clan.
*Ray Park as Rugal Bernstein, a mysterious man who steals the three relics and uses them to take over the King of Fighters dimension. Chizuru Kagura, descendant of the Kagura clan and host of the King of Fighters tournament. Saisyu Kusanagi, descendant of the Kusanagi clan and keeper of the Kusanagi Sword. David Leitch  as Terry Bogard, a CIA agent investigating on the activities of the King of Fighters tournament. Leitch was also the films fight choreographer.
*Monique Ganderton as List of The King of Fighters characters#Vice & Mature|Mature, a female King of Fighters participant and Vices girlfriend, who is lured into the tournament dimension and brainwashed by Rugal. 
*Bernice Liu as List of The King of Fighters characters#Vice & Mature|Vice, a female King of Fighters participant and Matures girlfriend. As with Mature, she also becomes Rugals servant.
*Sam Hargrave as Sam, a fighter loosely based on List of Art of Fighting characters#Mr. Big|Mr. Big. He loses to Mai in the films opening battle.

==Production==
The film was shot at Aja Tan Studios, North Vancouver, British Columbia, Canada. Shooting finished on January 19, 2009. 

==Release and reception==
The King of Fighters was released Direct-to-video|direct-to-DVD in the United States on September 7, 2010 through Vivendi Home Entertainment.    It was re-released in the U.S. by Well Go USA Entertainment. 

Beyond Hollywood gave the film a negative review, saying that it "wastes a lot of unnecessary time trying to convince itself that the audience needs to know everything there is to know about its history (we don’t, by the way), when it should have just accepted that it’s a movie based on a 2D fighting game, spend 10 minutes tops on the explanations, and get to the fighting already."  Felix Vasquez, Jr. of Cinema Crazed gave the film one-and-a-half out of four stars, commenting that "yet another semi-classic video game from the nineties is butchered in to yet another half assed lazily made film." 

==References==
 

==External links==
*   
* 
*  

 
 

 
 
 
 
 
 
 
 
 
 
   
   
 
 
 
 
 