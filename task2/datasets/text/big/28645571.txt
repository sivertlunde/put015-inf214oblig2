Toxic Zombies
{{Infobox film
| name           = Toxic Zombies
| image          = Toxic Zombies poster.jpg
| alt            =
| caption        = Theatrical release poster
| director       = Charles McCrann
| producer       = Charles McCrann
| writer         = Charles McCrann
| starring       = {{plainlist|
* Charles McCrann
* Beverly Shapiro
* Dennis Helfend
* John Amplas
}} 
| music          = Ted Shapiro
| cinematography = David Sperling
| editing        = {{plainlist|
* Charles McCrann
* David Sperling
}}
| studio         = CM Productions
| distributor    = Parker National Distributing
| released       =  
| runtime        = 89 minutes
| country        = United States
| language       = English
| budget         =
}}
Toxic Zombies (also known as Bloodeaters and Forest of Fear) is a 1980 horror film directed by Charles McCrann, who also acted in the film. It was classified as a video nasty in the United Kingdom in the 1980s.

== Plot ==

Illegal drug plantations are sprayed with the chemical Dromax by passing aeroplanes in an anti-drug initiative organised by corrupt government officials. Instead of killing the plants, the hippie growers of the crop are turned into flesh eating zombie-like creatures.

== Cast ==
* Charles McCrann
* Beverly Shapiro
* Dennis Helfend
* John Amplas

== Release and controversy ==

The film was given a limited release theatrically in the United States by Parker National Distributing.

Toxic Zombies is one of the films labelled a video nasty and was banned in the United Kingdom in the 1980s. 

The film was released on VHS by smaller labels Monterey Video and Raedon Video in the 1980s. In 2007, the film was released on DVD by Televista, but this DVD is a bootleg.

== Reception ==
Leonard Maltin qualified the film as a "bomb".  The film was also panned in Cinefantastique.   Writing in The Zombie Film Encyclopedia, academic Peter Dendle said, "Despite inconsistent behavior patterns and embarrassing acting, Toxic Zombies has the dubious honor of inaugurating the entire redneck zombie subgenre that would thrive inexplicably in the 80s." 

== References ==
 

== External links ==

* 

 
 
 
 
 
 
 
 


 