The Invisible Kid
{{Infobox film
| name           = The Invisible Kid
| image          = The Invisible Kid.jpg
| image_size     = 
| alt            = 
| caption        = 
| director       = Avery Crounse
| producer       = Samuel Benedict Avery Crounse Philip J. Spinelli
| writer         = Avery Crounse
| starring       = Jay Underwood Wallace Langham Chynna Phillips Karen Black
| music          = Steve Hunter Jan King
| cinematography = Michael Barnard
| editing        = Gabrielle Gilbert
| studio         = 
| distributor    = Taurus Films
| released       = 1988  
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

The Invisible Kid is a 1988 American comedy film directed by Avery Crounse. The film stars Jay Underwood, Chynna Phillips and Karen Black. 

==Plot==
Following in his deceased fathers footsteps, Grover Dunn (Jay Underwood) finds a magic formula that makes him vanish. The formula causes all types of trouble for Grover and his mother (Karen Black) and his crush (Chynna Phillips).

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 