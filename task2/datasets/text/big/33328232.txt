Seethamma Vakitlo Sirimalle Chettu
 
 
{{Infobox film
| name           = Seethamma Vakitlo Sirimalle Chettu
| image          = Seethamma vakitlo sirimalle chettu film poster.jpg
| alt            =  
| caption        = Film poster
| director       = Srikanth Addala
| producer       = Dil Raju
| writer         = Ganesh Patro  (Dialogues) 
| story          = Srikanth Addala
| screenplay     = Srikanth Addala
| starring       =  
| music          = {{Plainlist|
* Mickey J Meyer  Background Music)  }}
| cinematography = K. V. Guhan 
| editing        = Marthand K. Venkatesh
| studio         = Sri Venkateswara Creations
| distributor    = {{Plainlist|
* Sri Venkateswara Creations
* 14 Reels Entertainment  (Overseas)  }}
| released       =  
| runtime        = 159 mins
| country        = India
| language       = Telugu
| budget         =  
| gross          =  (Distributors Share) 
}}
 Telugu drama Anjali and Samantha Ruth Prabhu|Samantha, with Prakash Raj, Jayasudha and Rohini Hattangadi in supporting roles. The film was produced by Dil Raju under Sri Venkateswara Creations.    The film is being dubbed into Tamil language with the title of Anandam Anandame.   
 South Filmfare Best Actor Best Female Playback Singer – Telugu.   

SVSC collected   net profit as distributors share in excess of its budget in its lifetime.

== Plot ==
In the town of Relangi lived a man called "Relangi Mavayya" (Uncle) (Prakash Raj). A middle-class family with two sons Peddodu (elder) (Venkatesh), Chinnodu (younger) (Mahesh Babu) and a daughter. The Relangi Mavayyas sister married into a wealthy family known as Vijayawada Manushulu and has a daughter Seetha (Anjali). Seethas lost her parents at child birth and is brought up by RM uncle and family. Since childhood Seetha has dreamed of becoming Peddodus wife, but he never reciprocates her feelings, he just calls her "oye"! Peddodu does not like to change or please others and is very introverted. Chinnodu, unlike his brother o fights for what is right. Although the two brothers are complete opposite yet very close buddies. The RMs families bond get stronger and stronger as their lives are fill with day to day incidents.

Everything changes when Peddodu quits his job because he was told by his boss to be more friendly and to interact with his coworkers. Meanwhile, Seethas paternal uncle the rich family "Vijayawada Manushulu" heard through rumor mill that RM uncle would like Seetha to wed Pedoddu. However, Seethas paternal uncle (Rao Ramesh) doesnt want that, Rao Ramesh, unlike the rest of his family is jealous and hates the RMs of their social and economic status. In the meantime Rao Rameshs older daughter is getting married and RMs family has been invited out of courtesy. Since RM uncle sends Chinnodu on his behalf. At this wedding Chinoddu sees Geetha (Samantha), Rao Rameshs younger daughter, and its love at first sight for both of them. Although they must go their separate ways after the wedding, they exchange phone numbers and speak on a regular basis. While RMs goes out of town to pick up his daughter (Abhinaya), he bumps into the newlyweds Rao Rameshs daughter, her husband, and his parents. The boys parents see RMs daughter and set a pelli choopulu (pre-engagement) for her and one of their close friends. Although Peddodu does not like the alliance because it came from Vijayawada, Chinnodu convinces him and the wedding is set.

At the wedding, Relangi Mavayya asks Peddodu to stand at the main entrance and greet everyone and Chinnodu to greet as people are seated. The entire Vijayawada family arrives and Rao Ramesh has nothing but ill to speak of Relangi Mavayyas family. When Peddodu is angry hearing this, Chinnodu tries to convince him to put on a brave face for the sake of their sister and parents happiness just for the night and sort out everything in the morning. However, this causes a friction between the brothers. Although, this friction does not last long. Their mother wants Peddodu to go to Hyderabad along with Chinnodu for a job as one of their relatives who had come for the wedding had promised a job for Peddodu. Peddodu reluctantly agrees and tags along. He goes to his relatives office for a job but is made to wait for a very long time, which makes Peddodu angry. He shouts at his relative and leaves the office. Meanwhile Vijaywada family comes to visit Hyderabad and Geetha (Samantha Ruth Prabhu) invites Chinnodu for lunch. At lunch Chinnodu is surprised to see the entire family in the restaurant. Even the family is surprised; they invite him to sit with them and have lunch. Geethas father ends up insulting his father while conversing with him which makes Chinnodu angry and he lashes back and leaves. Geetha tries to pacify Chinnodu, but they both run accidentally into Peddodu, who is upset seeing Chinnodu with Viajayawada familyand believes that Chinnodu is in love with Geeta although Chinnodu tries to convince him. Without asking for any details, he immediately leaves Hyderabad and his stubborn nature upsets Chinnodu. This causes a tension between the brothers till the end of the movie.

In the middle of all the tension their father gets into an accident while trying to save another person and is in the hospital. Turns out the person he saved is a son of a close relative. The relative realizes that he misunderstood Relangi Mavayya all these years and he apologizes. Tension between the brothers grows further when Peddodu tries to speak to Chinnodu but Chinnodu says they are good for nothing and cannot achieve anything. Seetha who witnesses this plans to visit   to solve all their family problems. For the SriRama Navami Kalyanam at the Bhadrachalam temple, havoc rises because of a short circuit in the tent. Meanwhile Peddodu and Chinnodu join the Police and the temple coordinators and start guiding the people to a safer place. While Peddodu saves Geetha and her family from a stampede, Chinnodu damages the transformer with a rod with the advice of a pilgrimage and thus putting an end to the havoc. After everything calms down, Relangi Mavayya praises the two brothers for their bravery. He makes them understand the value of being friends with everyone and loving people. This changes the mindsets of the brothers as well as Geethas father. The brothers unite and Peddodu declares his love for Seetha expresses it by calling her by her name. At the end Seetha marries Peddodu, Geethas marriage is planned with Chinnodu and Geethas father apologizies to Relangi Mavayya. As promised, the employed brothers gift their grandmother a set of golden bangles.

== Cast ==
  Venkatesh as Peddodu
* Mahesh Babu as Chinnodu Anjali as Seetha Samantha as Geetha
* Prakash Raj as Relangi Mamayya
* Jayasudha as Peddodu and Chinnodus mother
* Kota Srinivasa Rao as Brahmananda Rao
* Rao Ramesh as Geethas father
* Brahmanandam (deleted scenes)
* Tanikella Bharani as Kondala Rao MLA
* Ahuti Prasad as Seethas uncle
* Ravi Babu as Goodu Raju, Kondala Raos son Venu Madhav as Omkar
* Srinivasa Reddy as Peddodus friend Prithviraj as Company Owner
* Rajababu as Relangis friend
* Prabhas Sreenu as Goodu Rajus friend
* Praveen as Chinnodus friend
* Giridhar as Chinnodus friend
* Raja Ravindra as Police inspector
* RJ Hemanth as Bridegroom
* Rohini Hattangadi as Peddodu and Chinnodus Grandmother
* Rama Prabha as Seethas grandmother
* Rajitha as Geethas Aunty Abhinaya as Chinni
* Dhanya Balakrishna as a Girl who proposes Chinnodu
* Kalpika Ganesh as America akka, Geethas sister
* Tejaswi Madivada as Geethas sister
* Kalpalata as Geethas Aunty
 

==Soundtrack==
{{Infobox album
| Name        = Seethamma Vakitlo Sirimalle Chettu
| Tagline     = 
| Type        = film
| Artist      = Mickey J Meyer
| Cover       = 
| Released    = 2012
| Recorded    = 
| Genre       = Soundtrack
| Length      = 27:53
| Label       = Aditya Music
| Producer    = 
| Reviews     =
| Last album  = Routine Love Story
| This album  = Seethamma Vakitlo Sirimalle Chettu
| Next album  = Chandamama Kathalu
}}

Music composed by Mickey J Meyer. All songs are hit tracks. Music released on ADITYA Music Company.
{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 27:53
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = yes
| music_credits =

| title1  = Seethamma Vakitlo Sirimalle Chettu
| lyrics1 = Ananta Sriram Chitra
| length1 = 3:42

| title2  = Yem Cheddaam Sirivennela Sitarama Sastry Sreerama Chandra
| length2 = 4:28

| title3  = Aaraduguluntada
| lyrics3 = Ananta Sriram Kalyani
| length3 = 4:06

| title4  = Inka Cheppaale
| lyrics4 = Ananta Sriram
| extra4  = Rahul Nambiar, Shweta Pandit
| length4 = 3:42

| title5  = Meghallo  Sirivennela Sitarama Sastry  Sreerama Chandra
| length5 = 3:58

| title6  = Mari Anthaga  Sirivennela Sitarama Sastry   Sreerama Chandra
| length6 = 3:48

| title7  = Vaana Chinukulu 
| lyrics7 = Ananta Sriram  
| extra7  = Karthik (singer)|Karthik, Anjana Sowmya
| length7 = 3:45
}}

== Production ==

=== Development ===
Srikanth Addala, who debuted with Kotha Bangaru Lokam (2008), had worked for two years on the script of this film.  The film was initially planned to be made with Pawan Kalyan and Venkatesh, but the former was replaced with Mahesh.   Dil Raju confirmed in early January 2012 that Samantha was part of the project and would be paired opposite Mahesh Babu.  Mickey J Meyer was roped in to score the music.  K. V. Guhan would handle the cinematography, while Anantha Sreeram and Sirivennela Sitaramasastri will be penning the lyrics for the songs. 

=== Casting === Samantha was Sneha were Anjali for the role.  In January 2012, after months of speculation Dil Raju finally announced that Anjali would play the female lead opposite Venkatesh.   It was also announced that popular Telugu actress Jayasudha was cast in the role of mother in the film. 
 Abhinaya was cast as the sister for Venkatesh and Mahesh in the film.  It is also revealed that the characters of Venkatesh and Mahesh in the film would not have names, but instead they will be referred to as Peddodu (elder) and Chinnodu (younger) respectively. 

=== Filming ===
In October 2011, during the launch of the film, it was announced that filming would begin from last week of November 2011.  After months of delay, filming began near Andhra University campus in Visakhapatnam on 18 January 2012. After wrapping up the Visakhapatnam schedule, the unit moved to Courtallam|Kutralam, Tamil Nadu for the second schedule.  Filming was done from 6 February 2012 to 27 February at Kutralam with all of the lead cast taking part in it.   Filming resumed in Hyderabad on 12 March 2012 and continued till 2 April.  During the schedule, filming was done at City Center mall in Banjara Hills, Sanathnagar and Ramoji Film City.   On 13 March, while filming at Sanathnagar in Hyderabad, huge crowds turned up to watch the shoot and as the crowds became uncontrollable, the unit was forced to move the shoot to a different location where scenes on Mahesh and Prakash Raj were filmed.   Filming resumed at Hyderabad on 11 April 2012. On 15 April, the unit shifted from Hyderabad to Ahmedabad and filming took place on the banks of Sabarmati River and the schedule continued there till 25 April. 

A special house set worth   10&nbsp;million was built at Ramoji Film City under the supervision of art director A. S. Prakash,  where major part of filming took place.  A new schedule began on 4 July in the temple town of Ahobilam.   A minor accident took place when the film set that was built in Ahobilam collapsed. It was reported that except for a minor injury to one of the unit member, the film crew was safe and the filming continued in the sets after few hours of delay.  After filming at Ahobilam till 8 July, the unit moved to Relangi village in West Godavari district.  Filming was done in Relangi till 11 July, where scenes on Venkatesh and Anjali were canned.  Filming was done for couple of days in Bhadrachalam from 16 July 2012. 

The entire filming was stopped for over a month due to Samanthas illness and her unavailability. Filming resumed on 3 September 2012 in Sriperumbudur near Chennai.  On 15 September, the makers of the film announced that they have completed 78 days of filming and still would require 30 days of additional filming to wrap up the entire film.  The unit resumed filming on 2 October at Ramanaidu Studios in Hyderabad.  On 27 October, the final leg of filming began at Ramoji Film City, where a Bhadrachalam temple set  was built for the film by the art director.   On 2 December, the producers announced that except for two songs, the rest of the films shooting including the talkie part was completed.  On 6 December 2012 one of the remaining two songs was filmed on Mahesh Babu and Samantha at Magarpatta in Pune.   The last song in the film was filmed from 18 to 21 December on Venkatesh and Anjali in Kerala.  The entire filming of the movie was completed on 24 December 2012. 

=== Post-production === pooja event was held to formally commence the dubbing activities of the movie.  It was reported that the post-production activities would simultaneously be done in order to complete the film in time for its intended December release.  On 23 October 2012, Samantha started dubbing for her role in the film.  It was reported that it was her first time dubbing for a Telugu film, as her roles was previously dubbed by singer Chinmayi.  The other female lead in the film, Anjali began dubbing for her role on 30 October 2012.  Mahesh Babu started dubbing for his role on 10 December 2012.  Re-recording (filmmaking)|Re-recording for the film was done by popular Telugu composer Mani Sharma.  Mahesh Babu completed the voice dubbing for his role on 31 December 2012. 

== Release == Sankranthi week CBFC on 8 January 2013.  The film was released on 11 January 2013 in over 1500 screens worldwide. The film was released in 85 theaters and 100 screens in Hyderabad.  Premiere shows for the film were held at few cities in Andhra Pradesh on the night of 10 January 2013.  The film premiered in USA on 10 January 2013 at the AMC Empire 25 theater in Times Square, New York City. 

=== Marketing ===
On 31 May 2012 Dil Raju held a press conference at Hyderabad and released a thirty second first look teaser of the film through YouTube to coincide with Mahesh Babus father, actor Krishna (actor)|Krishnas birthday.  The teaser was actively shared through social networking sites upon its release and had also received positive response from the audience.  Mahesh through his Twitter account acknowledged the response and thanked the producers of the film.  Few days after the teaser release, a preview song from the film was released by Aditya Music. Three first look posters and working stills of the film were released on 8 August 2012.  All the three posters had a message signed by Venkatesh that read "Happy birthday to my brother Mahesh Babu".  A twelve second teaser was officially released on 12 December 2012 at 12:12 pm.  Later that evening, a thirty second teaser was also released by the producer.  Both the teasers were dedicated to Venkatesh on the eve of his birthday. 

=== Pre-release revenues === Nellore district were sold to Srinikethan Films for   18.5&nbsp;million. 

=== Critical reception ===
The film received positive reviews from critics in India.    Karthik Pasupulate of The Times of India gave the film 3.5 out of 5 stars and said "the movie lives up to the tag of being a festive family entertainer" and concluded that "Its a good old fashioned family drama sans the usual masala, so as long as you dont expect fire works youll not be disappointed".  Sangeetha Devi Dundoo of The Hindu gave the film a positive review calling it a "joy ride" and praising the performances of Prakash Raj, Venkatesh, Mahesh Babu and Anjali.  Radhika Rajamani of Rediff gave it 3 out of 5 stars and said "family relationships and values are the main focus in Seethamma Vakitlo Sirimalle Chettu" and "the film is a good old family entertainer, without any violence, of the kind that was popular in the 1950s, 60s, and 70s" and concluded by saying "it seems a little out of date".  Reviewer from Indiaglitz.com gave a positive review for the film and called it "a heart-tugging family entertainer".  Jeevi of Idlebrain.com gave the film 3.25 of 5 rating and said "SVSC is a different and sensible film to be watched along with the families".  Mahesh S Koneru of 123Telugu.com gave a positive review and called it "a beautifully woven tale of human emotions" and he said "The film is a must watch, just to see Venkatesh and Mahesh Babu bond so well on the big screen and finally stated "On the whole, SVSC is a Sankranthi entertainer that you need to watch with your family".  Shekhar from Oneindia.in gave the film 3 out of 5 stars and said "the much-hyped movie, which had created huge expectations among the film goers, is really a wonderful flick that suits the image of the superstars and it is a big treat for their fans this Sankranthi".  Reviewer from CNN-IBN said "Despite the presence of stars such as Mahesh Babu and Venkatesh, Seethamma Vakitlo Sirimalle Chettu (SVSC) boasts of being a successful multi-starrer throughout by giving equal weightage to both the actors" and added that "The film may or may not inspire one and all, but it will definitely send everybody back home with a smile". 

== Box office == Gabbar Singh, Eega and Dookudu.  The film has collected a gross of  547.5&nbsp;million worldwide till 1 March 2013.    The film completed a successful 50-day run in 25 direct centers on 3 March 2013.  The film has completed a successful 100 days on 20 April 2013. 

SVSC Collected   (Share) in its lifetime.Its overseas distribution rights were sold for   and Satellite Rights were sold for   which is a record sum.SVSC minted  (Share)

=== India ===
In India, the film took a good opening upon release. The film collected   in Andhra Pradesh on its opening day.  The film has collected a gross of  403.5&nbsp;million in Andhra Pradesh till 1 March 2013. 

=== Overseas ===
The film was premiered in USA on 10 January 2013 at 89 locations. The film opened to positive response and grossed $304,047 via the premiere shows and set a record among south Indian films.  In USA, the film set an opening record for Telugu films by collecting approximately $1.2 million in the first two days. 

=== Home media === telecasted on 9 June 2013.   

== Soundtrack ==
 
 Chitra was Hyderabad on 16 December 2012 and audio of the film was released on the same day.    The audio was a huge success and the triple platinum disc function of the film was held on 20 January 2013 at Shilpakala Vedika, Hyderabad. 

== Awards ==
;Filmfare Awards South
;;Won  Best Actor – Telugu - Mahesh Babu  Best Female Playback Singer – Telugu - K. S. Chitra
;;Nominations  Best Film – Telugu - Dil Raju Best Director – Telugu - Srikanth Addala  Best Supporting Actor – Telugu - Prakash Raj Best Supporting Anjali  Best Music Director – Telugu - Mickey J. Meyer

== References ==
 

== External links ==
*  

 

 
 
 