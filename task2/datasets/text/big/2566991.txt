Hey Ram
 
 
{{Infobox film
| name = Hey Ram
| image = Heyram.jpg
| caption = Tamil Poster of the film
| director = Kamal Haasan
| writer = Kamal Haasan Manohar Shyam Joshi (Hindi dialogue) Abbas Girish Vaali Showkar Janaki
| producer = Kamal Haasan
| music = Ilaiyaraaja
| cinematography = Tirru
| editing = Renu Saluja
| distributor = Raajkamal Films International
| country = India
| released =  
| runtime = 202 minutes (Tamil version) 199 minutes (Hindi version) Tamil Hindi
}} Indian crime period crime Tamil and Best Foreign Film category for the year 2000.
==Synopsis== partition of Bengal in the background. Its relevance in the present Indian environment clouded by religious extremism was widely unrecognised. This is the story of Saket Ram, as narrated by his grandson. The film takes place as Ram, a retired archaeologist, lies on the deathbed, on 6 December 1999, the 7th anniversary of the destruction of Babri Mosque in Ayodhya.

==Plot==
The movie begins at present day with Saket Ram (Kamal Haasan), an 89-year-old Hindu man at his death bed. The scene reverts to the past as Saket remembers the 1940s, when he and his good friend, Amjad Ali Khan (Shah Rukh Khan) are archaeologists working together under their boss, Mortimer Wheeler, in Mohenjo-daro (Indus Valley Civilization) in the Sindh province in what was then North-West India. Relations are pleasant between the Indians and the English, and Saket and Amjad do not approve of Partition and the creation of Pakistan. Bengali wife, Direct Action". Saket goes to Calcutta and is swept into the madness. In one instance, Saket saves an innocent Sikh girl from the hands of a barbaric Muslim gang. When he returns to his house, he finds a group of Muslims entering his house. They brutally rape and murder Aparna. Saket, unable to cope with his tragic loss, kills the Muslims who raped and killed his wife in a fit of rage.
 Indian state to pay Rs. 620&nbsp;million to Pakistan and some territorial concessions as well.

Urged by family to remarry, Saket weds Mythili (Vasundhara Das). However, on a trip to Maharashtra, he reunites with Abhyankar and becomes a part of his militant organisation that plots to do away with Gandhi. Due to an horse-riding accident, Abhyankar is left a quadriplegic and has Ram swear that he will carry on his work, that of killing the Mahatma.

Saket comes to the belief that  s film Gandhi (film)|Gandhi.

Then on, Saket Ram lives by Gandhian principles. As the 89-year-old Saket Ram is being taken to the hospital, he is told of bomb blasts in the city due to Hindu-Muslim communal riots. He asks "Innuma (even now)?". They are forced by the police to be taken into an underground shelter for their security, but Saket Ram dies there. In his funeral, Gandhijis grandson comes and sees Sakets private room which is full of historical photos. Sakets grandson hands over Gandhis footwear and spectacle which Saket had previously collected from the place of shootout and had treasured it all the while.

==Cast==
 
 
*Kamal Haasan as Saket Ram
*Shah Rukh Khan as Amjad Ali Khan
*Rani Mukerji as Aparna
*Hema Malini as Ambujam Iyengar
*Atul Kulkarni as Shriram Abhayankar
*Vasundhara Das as Mythili Iyengar
*Girish Karnad as Uppilli Iyengar
*Saurabh Shukla as Lalwani Kavignar Vaali as Bhashyam Iyengar
*Naseeruddin Shah as Mahatma Gandhi
*Farida Jalal as Kasturba Gandhi
*Vikram Gokhale as Maharaja
*Chandrahasan as Mohan Gandhiraman
*Shadaab Khan as AltafTailor
*Gautam Kanthadai as Saket Ram Jr. Abbas as Munavar
*Delhi Ganesh as Mr.Chari
*Shruti Haasan as Sardar Vallabhbhai Patels daughter Nikita Palekar as Pushpa, Ambujam Iyengars daughter
*Y. G. Mahendran
*Om Puri as Goel
*Manoj Pahwa as Jalal
*Showkar Janaki as Mythilis Grandmother
*Gollapudi Maruti Rao as Govardhan
*Vaiyapuri as Vedha
*Arun Bali as Shaheed Suhrawardy
*Sharath Founkshe as Nathuram Godse
*Baby Taj as Blindgirl
*Arun Patekar as Sardar Vallabhai Patel
*Raj Bhadra as Jawaharlal Nehru
*Aravind Akash as Sankar Kishthaya
*A. C. Murali Mohan|A. C. Murali as Parthasarathy
*Tushar Gandhi as himself
*Sanjana Khanna as Amjads Daughter#1
* Bobby Bedi as Gandhis one eyed corrupted servant
 
* Written and Directed by: Kamal Haasan
* Produced by: N.Chandrahasan, Kamal Haasan
* Music: Ilaiyaraaja
* Executive Producer: D.N.Subramaniyan
* Cinematography: Tirru
* Edited by: Renu Saluja
* Art Director: Sabu Cyril
 

==Soundtrack==

===Tamil version===
{| class="wikitable"
|-  style="background:#ccc; text-align:centr;"
! Song !! Singer(s) !! Lyrics
|-
|"Isaiyil Thodanguthamma" Ajoy Chakrabarty Ilayaraja
|-
|"Nee Partha" Asha Bhosle, Hariharan (singer)|Hariharan, Rani Mukerji Jibanananda Das, Kamal Haasan
|-
|"Pollatha Madhana Paanam"
|Annupamaa|Anupama, Mahalakshmi Iyer Vaali
|-
|"Ram Ram" Kamal Haasan, Shruti Haasan Kamal Haasan
|-
|"Ramaranalum" Jolly Mukherjee, Hariharan
|Vaali
|-
|"Sanyaas Mantra" Kamal Haasan
|
|-
|"Vaaranam Aayiram Vaishnava Janatho" Vibha Sharma, Asha Bhosle
|Aandal, Narsinh Mehta
|}

===Hindi version===
{| class="wikitable"
|-  style="background:#ccc; text-align:centr;"
! Song !! Singer(s) !! Lyrics
|-
|"Har Koi Samjhe" Ajoy Chakraborty Sameer (lyricist)|Sameer
|-
|"Janmon Ki Jwala" Asha Bhosle, Hariharan (singer)|Hariharan, Rani Mukherjee Sameer (Poem by Jibanananda Das)
|-
|"Asa Ga Madan Ban Ghusla Kasa"
|Annupamaa|Anupama, Preeti Uttam Sameer / Jagdish Khebudkar
|-
|"Hey! Ram" Kamal Haasan, Shruti Haasan Sameer
|-
|"Chahe Pandit Ho" Jolly Mukherjee, Kamal Haasan, Hariharan Sameer
|-
|"Sanyaas Mantra" Kamal Haasan
|
|-
|"Vaishnav Jana To" Vibha Sharma
|
|}

==Production==
The shooting was started in Parthasarathy Temple.  Originally violinist L. Subramaniam was selected as composer for the film but for whatever reasons he opted out of the film  instead Ilayaraaja was selected as a composer.  Apparently, Shah Rukh Khan did not ask for any remuneration from Kamal Haasan as he considered it an honour and privilege to work with one of his mentors.

==Release==
The film has won the following awards since its release: National Film Awards 
* Won – Silver Lotus Award – Best Supporting Actor – Atul Kulkarni
* Won – Silver Lotus Award – Best Costume Design – Sarika
* Won – Silver Lotus Award – Best Special Effects – Manthra

There were protests and press releases by political parties in select centres against perceived negative depiction of Gandhi.    In India the film was given an A certificate by the Central Board of Film certification. In Malaysia, it was rated as Universal. 

==See also==
*List of artistic depictions of Mohandas Karamchand Gandhi
*List of Indian submissions for the Academy Award for Best Foreign Language Film
*Karen Gabriel, Draupadis Moment in Sitas Syntax: Violations of the Past and the Construction of Community in Kamal Haasan’s Hey! Ram, in Women and the Politics of Violence, Taisha Abraham (ed.) New Delhi: Shakti 2002.

==References==
 

==External links==
* 
* 
* 

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 