Cockfighter
 
{{Infobox film
| name           = Cockfighter
| image          =  
| caption        = DVD cover
| writer         = Charles Willeford
| starring       = Warren Oates Harry Dean Stanton Richard B. Shull Ed Begley, Jr. Laurie Bird Patricia Pearcy Steve Railsback
| director       = Monte Hellman
| producer       = Roger Corman Michael Franks
| distributor    = New World Pictures (theatrical) Anchor Bay (DVD, 2004)
| released       =  
| runtime        = 83 min.
| country        = United States
| language       = English
| budget         =
| awards         =
}}
Cockfighter (also known as Born to Kill) is a 1974 film by director Monte Hellman, starring Warren Oates, Harry Dean Stanton and featuring Laurie Bird and Ed Begley, Jr. The screenplay is based on the novel of the same name by Charles Willeford.

==The plot==
The plot begins in medias res with a mute Frank Mansfield (played by Warren Oates) locked inside a trailer preparing his best cock for an upcoming fight.  He slices the chickens beak slightly so that it looks cracked in order to increase the betting against him in the upcoming fight. He bets his trailer, girlfriend, and the remainder of his money with fellow cocker Jack (played by Harry Dean Stanton). Mansfield loses the fight (ironically because of the cracked beak), almost all of his belongings, and is set on a rambling path to win the Cockfighter of the Year award.

Frank visits his home town, his family farm, and his long-time fiancée Mary Elizabeth (played by Patricia Pearcy). Mary Elizabeth has long grown tired of Mansfields cockfighter ways and asks him to settle down with her. Frank decides in favor of cockfighting, leaves Mary Elizabeth, sells the family farm for money to reinvest in chickens, and starts a partnership with Omar Baradinsky (played by Richard B. Shull). The partnership takes them all the way to the cockfighting championships.

==The screenplay==
  Calypso character. Removing this character also excluded the protagonists short-lived music career from the plot, although the movie does show Mansfield plucking a guitar at one point. Two other significant characters in the novel are also missing from the movie: Doc Riordan (a pharmacist / inventor who supplies Mansfield with conditioning medicines for his chickens) and the Judge who sells the Mansfield farm. The final scene of the movie also presents a dramatic shift from the end of the book: Mansfield claims that Mary Elizabeth loves him as she walks off, whereas in the book he realizes the relationship is over and he is free.
 Milledgeville tournament, Middletons wife had died in the book, but in the movie Middleton (played by Willeford himself) refers to his wife as living. And finally, in the movie, Mansfield does not "regain" his voice until after Mary Elizabeth leaves.

==Reception==
The film struggled to find an audience and Roger Corman said it was the only movie he backed in the 1970s that lost money. He had it recut and reissued under the title Born to Kill but it still did not succeed. Ed. J. Philip di Franco, The Movie World of Roger Corman, Chelsea House Publishers, 1979 p 153 

==References==
 

==External links==
*  
* Two articles by Jonathan Rosenbaum and Toshi Fujiwara about COCKFIGHTER on La furia umana in the dossier dedicated to Monte Hellman  

 

 
 
 
 
 
 