Serigala Terakhir
{{Infobox film
| name           = Serigala Terakhir
| image          = Poster-serigala-terakhir.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Promotional poster
| director       = Upi Avianto
| producer       = Investasi Film Indonesia
| writer         = Upi Avianto
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = {{Plainlist|
* Vino G. Bastian
* Fathir Mochtar
* Dion Wiyoko
* Ali Syakieb
* Dallas Pratama
* Reza Pahlevi
* Fanny Fabriana
}}
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 135 minutes
| country        = 
| language       = 
| budget         = 
| gross          = 
}}
Serigala Terakhir (Last Wolf) is a 2009 Indonesian action film|action-drama film directed by Upi Avianto.

==Plot==
Five friends, Ale (Fathir Mochtar), Jarot (Vino G. Bastian), Lukman (Dion Wiyoko), Sadat (Ali Syakieb), and Jago (Dallas Pratama) live in a slum. They look for trouble, fight, and extort merchants. With more leadership skills, Ale becomes the groups leader. He lives with his mother (Ully Artha), his little sister Aisyah (Fanny Fabriana), and little brother Bara (Agung Surya Putra). In the slum, there is a mute, Fathir (Reza Pahlevi), who lives with his grandmother. He is always mocked by the residents, including Ale, Lukman, Sadat, and Jago; however, Jarot often shows empathy to Fathir.

One day, there is a football match between Ales group and a rival group. After the match, a fight breaks out. One of the rival member plans to stab Ale. However, Jarot manages to foil it by hitting the attackers head with a stone, killing him.

Jarot is arrested. Ales group, Jarots parents, and his little sister Yani (Zaneta Georgina) are shocked. Ales group still do their routines. Fathir wants to be a group member, but is rejected. Returning home, he sees his grandmother is dead; he then leaves the slum. Meanwhile, in prison, Jarot is tortured by other inmates. During visiting hours, he is jealous of the other inmates, who are visited by their relatives. He misses his friends. After that, he fights back against the other inmates who disturb him.

When released, Jarot meets Fathir. Fathir brings him to headquarter of a drug gang, Naga Hitam (Black Dragon). Jarot is offered membership by their leader (George Rudy), which he accepts. Jarot and Fathir are assigned to protect drug distribution in his home slum. They have to deal with Ales group. Jarot then comes back home to see his family and Aisyah. He expresses his love to her. Later, Lukmans little brother overdoses. In revenge, Lukman plans to kill Jarot, but he is shot by Fathir. Sadat and Jago are furious, and plan to rape Yani in return; Jarot, enraged, tortures them to death.
 killed herself, while Ale suspects Jarot. Jarot calls Ale to meet him. They fight, and Ale takes out his pistol. As they continue to struggle, a gunshot is heard; Ale drops dead. As he walks away, Jarot is shot twice by Fathir. The film closes with a scene which shows Bara taking Ales pistol and leaving the slum with his friends to take revenge.

==Production==
Serigala Terakhir was directed by Upi Avianto,  who had previously worked horror film|horror, teen drama film|dramas, and chick flicks.    It was produced by Investasi Film Indonesia.  Avianto said that her inspiration to write the story was the social conditions faced by Indonesian people, namely the threat of crime,  and that she was most comfortable producing male-oriented films. 
 Rp 8 billion, but special effects caused the costs to swell to Rp 10 billion.    One of the locations used was Sukamiskin Penitentiary in Bandung, West Java, which helped show the hard life faced by inmates. 

==Themes==
Serigala Terakhir tells about a broken friendship and a vengeance based in a traditional slum.  It is based on the style of East Asian action films. 

==Release and reception==
Serigala Terakhir was released on 5 November 2009.   

Elang Riki Yanuar, reviewing for Okezone, wrote that audiovisual effects were not good, especially in the fire scene. However, he praised the acting.  Marcel Thee of Jakarta Globe noted that the film worked because of the focus "on the emotional evolution of its characters", and that while "production is well done for a local movie", there were nevertheless some "awkward visuals", such as the drug gangs "retro 1970s wardrobe" and "kitschy kung-fu imagery".   
 Best Leading Actor,  but the film did not win any.  Bastian and Pahlevi received two awards at the 2010 Indonesian Movie Awards for Best Pair and Favorite Pair.  

==References==
 

==External links==
* 

 
 
 