Black Widow (1987 film)
{{Infobox film
| name           = Black Widow
| image          = Black_widow_film.jpg
| caption        = Theatrical release poster
| director       = Bob Rafelson
| producer       = Laurence Mark Harold Schneider
| writer         = Ronald Bass
| starring       = Debra Winger Theresa Russell Sami Frey Dennis Hopper Rutanya Alda Terry OQuinn
| music          = Michael Small
| cinematography = Conrad L. Hall, ASC John Bloom
| distributor    = 20th Century Fox
| released       =  
| runtime        = 102 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $25,205,460 
}}
Black Widow is a 1987 thriller film starring Debra Winger, Theresa Russell, Sami Frey and Nicol Williamson. Dennis Hopper has a short role at the beginning of the film.
 Department of Justice who grows obsessed with bringing her to justice. It was directed by Bob Rafelson, from a screenplay by Ronald Bass.  The story takes on the form of a travelogue, as the murderess moves from New York to Dallas to Seattle and finally to Hawaii.

==Plot== Justice Department agent Alexandra "Alex" Barnes (Debra Winger) stumbles onto the first murder while investigating another case. As Alex delves further into the case, she uncovers a pattern which she believes ties the same woman to several similar murders.

Using exhaustive research and preparation as well as identity and appearance changes, Catherine weaves her web anew with each murder, killing a publishing magnate, a toy maker ( ), an international hotel tycoon. Later, she reveals she has been married six times, and possibly as many murders.

After the death of a rich, lonely museum curator in Seattle, Alexs boss Bruce (Terry OQuinn) gives her tacit permission to hunt down the killer, and Alex goes undercover as "Jessie Bates" to track down Catherine and identify her next potential victim. The trail leads to Hawaii, where Catherine is moving on Nuytten. With the aid of a private investigator, "Jessie" arranges to meet Catherine; the two women become friends and eventually engage in a sexually intense war of wits and wills orchestrated by Catherine.

Orchestrated by Catherine, they compete for the affection of wealthy Paul Nuytten; Catherine marries him and, during the reception, exposes to "Jessie" that the "black widow" knows about the federal agent.  "Jessie" attempts to warn Paul of Catherines murderous intentions but he is skeptical, refusing to believe she is after his money, because in the event either of their deaths, their wills both stipulate that their net worth be donated to the Cancer Foundation and not to each other. After discovering the private investigator dead of a suspicious overdose and on the accusation of Catherine, "Jessie" is arrested for Pauls murder when the police find poison in her room. Catherine meets with Pauls attorney, who explains that Paul was a resident of Florida, and according to state law, a spouse has the right to overturn charitable donations stipulated in their spouses will. Catherine then tells the attorney that shortly before his death, Paul expressed "profound reservations" about the Cancer Foundation. 

Catherine visits "Jessie" in prison, and while they talk, Catherine is confronted with the sister of one of her victims (the publisher) and Paul, who is clearly not dead, and now aware that Catherine was in fact, after his money. Catherine, shocked, realizes her attempt to double-cross Jessie/Alex has failed. Alexandra emerges as the hero and is cleared of wrongdoing.

==Cast==
* Debra Winger as Alexandra Barnes/Jessie Bates
* Theresa Russell as Catherine Petersen
* Sami Frey as Paul Nuytten
* Dennis Hopper as Ben Dumers
* Nicol Williamson as William McCrory
* Terry OQuinn as Alexandras boss Bruce
* James Hong as H Shin - Honolulu private investigator
* Diane Ladd as Etta
* D. W. Moffett as Michael
* Lois Smith as Sara
* Leo Rossi as Detective Ricci
* Danny Kamekona as FBI security
* Rutanya Alda as Irene
* Mary Woronov as Shelley
* Wayne Heffley as Ettas husband

==Reception==
Film4 notes that Black Widow succeeds through Rafelsons "menacing direction" and Debra Wingers "convincing struggle with temptation", while Theresa Russell "steals the show as the sexily assured devil sitting on her trackers shoulder". 

Vincent Canby, in the New York Times, writes that while the film promises more than it can deliver, its classy looks make it both soothing and "redeemingly funny, in part, at least, for not becoming mired in its own darker possibilities". He praises Winger for "the gift of seeming always to have hidden reserves of feeling that might erupt in chaos at any minute", while Russell "comes into her own" in the film, and has "a cleareyed sweetness that adds unexpected dimension to the homicidal Catharine." 

Roger Ebert gave Black Widow a mixed rating of 2.5 out of 4 stars, praising the solid performances by the main actors yet lamenting that "the movie makes no effort to keep us in suspense" by revealing too much too soon about Russells character. 

==References==
 

==External links==
*  
* http://www.channel4.com/film/reviews/film.jsp?id=101238 Black Widow from Channel 4 Film
* http://www.fast-rewind.com/ The 80s Movies Rewind website
* http://www.bbc.co.uk/dna/collective/A3393146 BBC - collective
* http://www.rottentomatoes.com/m/1002531-black_widow/ Rotten Tomatoes website

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 