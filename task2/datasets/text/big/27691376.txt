The Smell of Success
{{Infobox film
| name           = The Smell of Success
| image          = SOSPoster2.jpg
| caption        =  Michael Polish Mark Polish Michael Polish
| starring       = Billy Bob Thornton Téa Leoni Ed Helms Kyle MacLachlan
| music          = 
| cinematography = 
| editing        = 
| studio         = Initiate Productions 
| distributor    = Cinedigm
| released       =  
| runtime        = 94 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
The Smell of Success is an Initiate Productions film directed by Michael Polish, starring Billy Bob Thornton, Téa Leoni, Kyle MacLachlan, and Ed Helms. The film’s original title was Manure.  

==Plot==
It all begins when a tragic fan accident ends the life of Mr. Roses, the scientific genius behind Roses Manure Company, forcing his cosmetics salesgirl daughter Rosemary (Téa Leoni) to take control of the company. Rosemary isn’t sure if she has a nose for the family business, but when she discovers the company is about to go under, she is determined to find a way to keep the company successful. She’s going to need the help of her father’s best salesmen led by Patrick Fitzpatrick (Billy Bob Thornton).

==Cast==
*Billy Bob Thornton as Patrick Fitzpatrick
*Téa Leoni as Rosemary Rose
*Kyle MacLachlan as Jimmy St. James
*Ed Helms as Chet Pigford
*Mark Polish as Thaddeus Young
*Pruitt Taylor Vince as Cleveland Clod
*Frances Conroy as Agnes May

==Production==
The Smell of Success was filmed on location in Santa Clarita, California. 

==References==
 

==External links==
*  
* 
* 
* 

 

 
 
 
 
 


 