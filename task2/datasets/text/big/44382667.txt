The Taint (1915 film)
 

{{infobox film
| name = The Taint
| image          = Taint (The)020.jpg
| imagesize      =
| caption        = Lubin Bulletin.
| director = Sidney Olcott
| producer = Sid Films
| writer = Pearl Gaddis
| starring = Valentine Grant James Vincent Pat OMalley
| distributor = Lubin Manufacturing Company
| released =  
| runtime = 3000 ft
| country = United States language = Silent film (English intertitles) 
}}
The Taint is an American silent film produced by Sid Films and distributed by Lubin Manufacturing Company. It was directed by Sidney Olcott with Valentine Grant, James Vincent and Pat OMalley in the leading roles.

==Cast==
* Valentine Grant - Mabel Stuart
* James Vincent - Arthur Easton
* PH OMalley - Bert Stuart
* Roy Sheldon - Frank Board

==References==
* Motography, December 4, 1915. 
* The New York Dramatic Mirror, December 4, 1915, p 29.

==External links==
* 
*    website dedicated to Sidney Olcott

 
 
 
 
 
 
 
 


 