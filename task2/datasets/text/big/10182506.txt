Don Winslow of the Coast Guard
{{Infobox Film
| name = Don Winslow of the Coast Guard
| image =
| image_size =
| caption = Ray Taylor
| producer =
| writer =
| narrator =
| starring =
| music =
| cinematography =
| editing =
| distributor = Universal Pictures
| released = 1943
| runtime = 13 chapters (245 min)
| country = United States
| language = English
| budget =
}}
 film serial based on the comic strip by Frank V. Martinbek.

==Plot==
After service at Pearl Harbor, Naval Commander Don Winslow, and his friend and junior officer, Lieutenant "Red" Pennington, are assigned to the Coast Guard. There they are ordered to devote their activities to anti-fifth column work on the mainland. Winslow learns that The Scorpion, a fascist synthesizer, is in the pay of the Japanese and is expected to lay the ground work for a Japanese attack on the Pacific coast. Constantly in peril and aided by Mercedes Colby, the daughter of a Navy Admiral, they investigate secret island-bases and battles with submarines and enemy planes...

==Production==
Martinbeks comic strip was approved by the   in December - was one of the most timely contributions of the serial field." {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | year = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 18–19
 | chapter = 2. In Search of Ammunition
 }} 

==Chapter titles==
# Trapped in the Blazing Sea
# Battling a U-Boat
# The Crash in the Clouds
# The Scorpion Strikes
# A Flaming Target
# Ramming the Submarine
# Bombed in the Ocean Depths
# Blackout Treachery
# The Torpedo Strikes
# Blasted from the Skies
# A Fight to the Death
# The Death Trap
# Capturing the Scorpion
 Source:  {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | year = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 235
 | chapter = Filmography
 }} 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 


 