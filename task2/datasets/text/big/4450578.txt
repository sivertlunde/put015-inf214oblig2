A Problem with Fear
 
{{Infobox film

 | name = A Problem with Fear
 | caption = 
 | image	=	A Problem with Fear FilmPoster.jpeg Gary Burns
 | producer = George Baptist, Luc Déry
 | writer = Donna Brunsdale, Gary Burns
 | starring = Paulo Costanzo Emily Hampshire Willie Garson Keegan Connor Tracy Camille Sullivan
 | music = John Abram
 | cinematography = Stefan Ivanov
 | editing = Yvann Thibaudeau
 | distributor = Christal Films
 | released = 5 September 2003 (Canada) 17 April 2004 (US)
 | runtime = 
 | country = Canada English
 | budget = 
 | gross = 
}} absurdist comedy Canadian film-maker Gary Burns.

==Plot==
Laurie Harding (Paulo Costanzo) is a small shop clerk in the local Calgary mall. His world is full of tragic accidents - crosswalks, elevators, escalators and virtually anything associated with commercialism or technology is parodied as objects of fear and tragedy.

An elevator plunges 30 stories. A womans scarf is caught in an escalator, strangling her to death. A man is struck by a car, right before Laurie had the premonition to not cross.

His own world is full of fear of these very events, his predictions of them, and his dependence on his less-than-sympathetic sister, Michelle (Camille Sullivan).

Michelle heads product development at the company Global Safety Inc., whose mission is to provide products to combat the "Fear Storm" which assails the city. With their "Early Warning 2 Safe System tm", the PDA-like device warns you of danger ahead of time and the "Safe Bracelet tm" senses your fear and allows you to beep for help.

Along with Lauries prophetic powers, and the endless onslaught of accidents, the movie is full of absurd events: a mall announcer who continually requests a speaker of x foreign language - without any apparent reason; the same malls population which slowly dwindles, being virtually empty by the films end; an entire classroom population which is accosted simultaneously by hiccups;

Some of these are more apparently  , the San Francisco treat"; his girlfriend, Dot (Emily Hampshire), does a survey on "how the clothes you wear define you" - one she both detests and is obsessed by.

These events are framed by the pretextual plot of Laurie conquering his fears &mdash; and by extension, the fears of those around him. However, by the films end, it is doubtful that anything has been changed or achieved.

==Cast==

*Paulo Costanzo as Laurie
*Emily Hampshire as Dot
*Willie Garson as Erin
*Keegan Connor Tracy as Vicky
*Camille Sullivan as Michelle
*Benjamin Ratner as Alan
*Marnie Alton as Daphne
*Melba Montgomery as the escalator lady

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 
 
 