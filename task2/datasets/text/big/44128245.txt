Paraya Ghar
 
 
{{Infobox film
| name           = Paraya Ghar
| image          = 
| image_size     = 
| caption        = 
| director       = Kalpataru	 
| producer       = S Waris Ali 
| writer         = 
| narrator       = 
| starring       =Rishi Kapoor Jaya Prada
| music          = Laxmikant-Pyarelal
| cinematography = 
| editing        = 
| distributor    = 
| released       = June 9, 1989
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

 Paraya Ghar  is a 1989 Bollywood film directed by Kalpataru and starring Rishi Kapoor, Jaya Prada, Aruna Irani, Madhavi (actress)|Madhavi, Sachin Pilgaonkar, Tanuja and Kader Khan.

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Dulhan Tujhe Banaoonga"
| Anuradha Paudwal, Suresh Wadkar
|-
| 2
| "Humre Jiyara Ka Ghungroo"
| Alka Yagnik, Mohammad Aziz
|-
| 3
| "Pyar Mil Gaya"
| Alka Yagnik
|-
| 4
| "Saajan Se Milne Jana Hain"
| Anuradha Paudwal
|-
| 5
| "Humse Kiya Tha Tune Jhoota Waada"
| Anuradha Paudwal
|}

==References==
 

==External links==
 

 
 
 
 

 