Infernal Affairs
 
 
 
 
{{Infobox film
| name           = Infernal Affairs
| image          = IAmoviepost.jpg
| alt            = 
| caption        = Theatrical release poster
| film name = {{Film name
 | traditional    =  
 | simplified     =  }}
| director       = Andrew Lau Alan Mak
| producer       = Andrew Lau
| writer         = Alan Mak Felix Chong Tony Leung Anthony Wong Eric Tsang
| music          = Chan Kwong-wing
| cinematography = Andrew Lau Lai Yiu-fai Danny Pang Curran Pang Media Asia Films Basic Pictures Media Asia Distribution
| released       =  
| runtime        = 101 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          = Hong Kong dollar|HK$55,057,176
}}
 |s= |l="Unceasing Path"|j= Mou 4  Gaan 3  Dou 6  |p=Wú Jiān Dào}} thriller film hell in internal affairs" with the adjective "infernal". Due to its commercial and critical success, Infernal Affairs was followed by a prequel, Infernal Affairs II, and a sequel, Infernal Affairs III, both released in 2003.
 Tony Leung, Anthony Wong, Eric Tsang, Kelly Chen and Sammi Cheng), but it later received critical acclaim for its original plot and its concise and swift storytelling style. The film did exceptionally well in Hong Kong, where it was considered "a box office miracle" and heralded as a revival of Hong Kong cinema which at the time was considered to be direly lacking in creativity.

The film had been selected as the Hong Kong entry for the Best Foreign Language Film at the 76th Academy Awards but it was not nominated. Still, Miramax Films acquired the United States distribution rights of this film and gave it a limited U.S. theatrical release in 2004.

Infernal Affairs was remade  by Martin Scorsese in 2006 as The Departed, which went on to win the Academy Award for Best Picture.

==Plot== mole has intelligence over the other side. The more the moles become involved in their undercover lives, the more issues they have to cope with.
 Superintendent Wong Chi-shing. In reality, Chan has become an undercover agent reporting only to Wong. Over the course of ten years, Chan experiences great stress from his undercover work while Lau quickly rises through the ranks in the police department. The film begins with a meeting between Chan and Lau in a hi-fi store without either of them knowing the others identity.
 Thai cocaine dealer after receiving a tip-off from Chan using Morse code. However, Lau alerts Hon, giving him enough time to order his minions to dispose of the cocaine, eliminating solid evidence of the drug deal. After the incident, Wong and Hon are both aware that they each have a mole within their respective organisations, placing them in a race against time to root out the other mole. Later, Chan sees Hon conversing with Lau at a cinema but does not see Laus face clearly; he ultimately fails to capture Lau. By this time, both Chan and Lau are struggling with their double identities – Chan starts losing faith in himself as a cop after being a gangster for ten years; Lau becomes more accustomed to the life of a police officer and he wants to erase his criminal background.
 CIB Inspector OCTB squad to save Wong. Chan flees from the building using a crane while Wong sacrifices himself to save him by distracting Hons men. Wong is beaten and thrown off the roof by the gangsters. As the police close in, a shootout ensues in which several gangsters are killed. Keung drives Chan away from the scene, but later dies from a mortal gunshot wound. It is reported on the news that Keung himself was an undercover cop; Hon assumes that he was the mole and that Chan killed him to protect the triad.

Lau retrieves Wongs cell phone and contacts Chan, with both of them agreeing to foil a drug deal by Hon. The plan succeeds and many of Hons men are arrested, while Lau betrays Hon and kills him. Everything seems to have returned to normal &ndash; Chan can revert to his true identity as a cop, while Lau has erased his criminal connections by eliminating Hons triad. However, back at police headquarters, Chan discovers that Lau was the mole and leaves immediately. Lau, realising what has happened, erases Chans file from the police database. Chan spends an evening with his therapist, Dr. Lee Sum-yee, with whom he has fallen in love. He sends to Lau a compact disc with a recording that Hon kept between himself and Lau; the disc is inadvertently intercepted by Laus girlfriend, Mary.

Chan and Lau meet on the same rooftop where Wong was killed earlier. Chan disarms Lau without resistance and holds a gun to Laus head, as a rebuke to Laus plea for forgiveness and request to remain as a cop. Inspector B arrives on the scene shortly and orders Chan to release Lau. Chan holds Lau as a hostage at gunpoint and backs into an elevator, but upon moving his head from behind Lau he is suddenly shot in the head by B. B then reveals to Lau that he is also a mole planted by Hon. As they take the lift down to the lobby, Lau kills B out of his desire to eradicate traces of his past, become a "good guy" cop, and end the mole hunt.

The original ending climaxes with Lau identifying himself to the police as one of them. Lee discovers records revealing Chan as the undercover officer; B is blamed of being the mole within the force and the case is closed. Lau salutes Chan at his funeral, with Cheung and Lee present as well. A flashback reaffirms the point that Lau wished he had taken a different route in life. In mainland China, an alternate ending for the film was created, in which Lau exits the elevator and is informed by Cheung that the police have found evidence that he was a mole. Lau hands them his badge and is arrested without protest. The sequel, Infernal Affairs III, uses the original ending instead of the alternate one.

==Cast==
* Andy Lau as Senior Inspector Lau Kin-ming (劉健明; Lau Kin Ming), Hons mole in the police force.
** Edison Chen as young Lau Kin-ming Tony Leung as Chan Wing-yan (陳永仁; Chan Wing Yan), an undercover cop in Hons triad.
** Shawn Yue as young Chan Wing-yan Anthony Wong as Superintendent Wong Chi-shing (黃志誠; Wong Chi Shing), Chans superior.
* Eric Tsang as Hon Sam (韓琛; Hon Sum), the triad boss. He is the main antagonist.
* Chapman To as "Crazy" Keung (傻強; Silly  Keung), Hons henchman.
* Gordon Lam as Inspector B (大B; Big B), Laus subordinate who is actually also a mole in the police force.
* Kelly Chen as Dr. Lee Sum-yee (李心兒; Lee Sum Yi), Chans therapist.
* Sammi Cheng as Mary, Laus girlfriend.
* Berg Ng as Senior Inspector Cheung (張Sir; Cheung Sir). Wan Chi-keung as Officer Leung (梁Sir; Leung Sir), the police chief.
* Dion Lam as Del Piero (迪比亞路; Dibiyalu), Hons henchman.
* Elva Hsiao as May, Chans ex-girlfriend.

==Reception==
  Golden Horse the best films of all time and is the highest ranked Hong Kong film on Internet Movie Databases Top 250 movies list.

===Box office===
Infernal Affairs has grossed HK$55,057,176 in Hong Kong and USD$169,659 in North America.

==Awards and nominations==
{| class="wikitable" style="font-size:small;" ;
|- style="text-align:center;"
! colspan=4 style="background:#B0C4DE;" | List of Accolades
|- style="text-align:center;"
!  style="background:#ccc; width:28%;"| Award / Film Festival
!  style="background:#ccc; width:26%;"| Category
!  style="background:#ccc; width:36%;"| Recipient(s)
!  style="background:#ccc; width:10%;"| Result
|- style="border-top:2px solid gray;" Udine Far East Film Festival Audience Award Andrew Lau  Alan Mak
| 
|- Asia Pacific Film Festival Best Sound Kinson Tsang
| 
|- 46th Blue Ribbon Awards Best Foreign Language Film Andrew Lau  Alan Mak
| 
|- Belgian Syndicate of Cinema Critics Grand Prix Grand Prix
|
| 
|- 40th Golden Golden Horse Awards Best Picture
|
| 
|- Best Director Andrew Lau  Alan Mak
| 
|- Best Actor Tony Leung
| 
|- Best Supporting Actor Anthony Wong
| 
|- Best Sound Effects Kinson Tsang King-Cheung
| 
|-
|Viewers Choice Award
|
| 
|- Best Actor Andy Lau
| 
|- Best Original Screenplay Alan Mak  Felix Chong
| 
|- Best Film Editing Danny Pang  Pang Ching-Hei
| 
|- Best Cinematography Andrew Lau  Lai Yiu-Fai
| 
|- Best Art Direction Choo Sung Pong  Wong Ching-Ching
| 
|- Best Action Choreography Dion Lam Dik-On
| 
|- Best Visual Effects Christopher Doyle
| 
|- 8th Golden Bauhinia Awards Best Picture
|
| 
|- Best Director Andrew Lau  Alan Mak
| 
|- Best Actor Tony Leung
| 
|- Best Actor Andy Lau
| 
|- Best Supporting Actor Anthony Wong
| 
|- Best Original Screenplay Alan Mak  Felix Chong
| 
|- 9th Hong Kong Film Critics Society Awards Film of Merit
|
| 
|- Best Actor Anthony Wong
| 
|- 22nd Hong Kong Film Awards Hong Kong Best Film
|
| 
|- Hong Kong Best Director Andrew Lau  Alan Mak
| 
|- Hong Kong Best Screenplay Alan Mak  Felix Chong
| 
|- Hong Kong Best Actor Tony Leung Tony Leung
| 
|- Hong Kong Best Actor Andy Lau
| 
|- Hong Kong Best Supporting Actor Anthony Wong Anthony Wong
| 
|- Hong Kong Best Supporting Actor Eric Tsang
| 
|- Hong Kong Best Supporting Actor Chapman To
| 
|- Hong Kong Best Cinematography Andrew Lau  Lai Yiu-Fai
| 
|- Best Film Editing Danny Pang Danny Pang  Pang Ching Hei
| 
|- Best Costume Design Lee Pik-Kwan
| 
|- Hong Kong Best Action Choreography Dion Lam
| 
|- Best Original Film Score Chan Kwong Wing
| 
|- Best Original Film Song
|Song: "Infernal Affairs" 
Composer: Ronald Ng 
 Lyrics: Albert Leung
 Sung by: Tony Leung, Andy Lau
| 
|- Best Sound Design Kinson Tsang King-Cheung
| 
|- Best Visual Effects Christopher Doyle
| 
|}

==Music==
The original film score for Infernal Affairs was written and performed by Chan Kwong-wing.
{{Track listing
| headline     = Track listing
| extra_column = Artist(s)
| title1       = Entering The Inferno
| length1      = 2:06
| extra1       = Chan Kwong-wing
| title2       = If I Were Him
| length2      = 1:36
| extra2       = Chan Kwong-wing
| title3       = Goodbye Master
| length3      = 2:18
| extra3       = Chan Kwong-wing
| title4       = Who Are You?
| length4      = 2:44
| extra4       = Chan Kwong-wing
| title5       = Let Me Quit
| length5      = 1:32
| extra5       = Chan Kwong-wing
| title6       = I Dreamt About You
| length6      = 1:23
| extra6       = Chan Kwong-wing
| title7       = Salute
| length7      = 1:56
| extra7       = Chan Kwong-wing
| title8       = Mission Abort
| length8      = 4:31
| extra8       = Chan Kwong-wing
| title9       = I Am A Cop!
| length9      = 3:26
| extra9       = Chan Kwong-wing
| title10      = You Are The Only One
| length10     = 1:06
| extra10      = Chan Kwong-wing
| title11      = I Want To Be A Good Guy
| length11     = 3:30
| extra11      = Chan Kwong-wing
| title12      = Goodbye Master, Goodbye
| length12     = 1:56
| extra12      = Chan Kwong-wing
| title13      = The Inferno
| length13     = 1:51
| extra13      = Chan Kwong-wing
}}

The theme song, Infernal Affairs (無間道), was composed by Ronald Ng, lyrics provided by Albert Leung, and performed in Cantonese and Mandarin by Andy Lau and Tony Leung.

==Remake films and products==
 
In 2003, Brad Pitts production company Plan B Entertainment acquired the rights for a Hollywood remake, titled The Departed, which was directed by Martin Scorsese, and starred Leonardo DiCaprio, Matt Damon, Jack Nicholson, and Mark Wahlberg, set in Boston, Massachusetts, roughly based on the life of famed Boston mobster James "Whitey" Bulger. The Departed was released on 6 October 2006 and went to critical acclaim and won the Academy Award for Best Picture.

Lau, Tsang, and Cheung parodied the cinema scene to promote the Hong Kong Film Awards. Lau and Tsang, in their respective characters, go through the scene where they meet to gather info on the undercover cop amongst Hon Sams gang. Lau Kin-ming asks Hon "Why do we always meet in a cinema?", to which Hon answers "Its quiet. No one comes to movies". Cheung comes out from the shadows behind them and says "I dont know...quite a few people watch movies" and we see a slew of Hong Kong celebrities watching various clips of Hong Kong films on the screen. Originally Tony Leung was going to appear but scheduling conflicts led to the recasting.
 TVB spoof celebrating the Chinese New Year called Mo Ba To (吐氣羊眉賀新春之無霸道), the 2004 comedy film Love Is a Many Stupid Thing by Wong Jing, and the 2004 TVB television drama Shades of Truth were re-writings based on the plot of the film.

In Taiwan SHODA (劉裕銘) and a secondary school student Blanka (布蘭卡) cut and rearranged the original film and inserted new sound tracks to produce their videos Infernal Affairs CD pro2 and Infernal Affairs iPod on the web. The videos had many views and both producers removed their videos after receiving cease and desist letters from the Group Power Workshop Limited (群體工作室), the Taiwan distributor of the film. 

Media Asia released a limited edition of eight-DVD set of the Infernal Affairs trilogy in an Ultimate Collectible Boxset (無間道終極珍藏DVD系列(8DVD套裝)) on 20 December 2004. Features included an online game and two Chinese fictional novels of the film series by Lee Muk-Tung (李牧童), titled 無間道I+II小說 ISBN 962-672-259-2 and 無間道III終極無間小說 ISBN 962-672-271-1.
 Quality Tourism Services Scheme in Hong Kong. 
 Hidetoshi Nishijima TBS and WOWOW.  The production aired in two parts: "Police Impersonation" on WOWOW and "Undercover" on TBS.

==See also==
* Cinema of Hong Kong
* List of Hong Kong films
* Andy Lau filmography
* List of films featuring surveillance
* List of films set in Hong Kong

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 