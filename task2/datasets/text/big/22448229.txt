A Little Night Music (film)
{{Infobox film
| name           = A Little Night Music
| image          = A Little Night Music film poster.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Harold Prince
| producer       = Elliott Kastner
| screenplay     = Hugh Wheeler
| story          = Ingmar Bergman
| based on       =  
| starring       = Elizabeth Taylor Diana Rigg Len Cariou Lesley-Anne Down
| music          = Stephen Sondheim, Jonathan Tunick
| cinematography = Arthur Ibbetson
| editing        = John Jympson
| studio         = Sascha-Verleih S&T-Film Berlin
| distributor    = New World Pictures
| released       =  
| runtime        = 124 minutes
| country        = United States West Germany Austria
| language       = English
| budget         = $6 million 
}}
A Little Night Music is a 1977 film adaptation of the musical A Little Night Music. It stars Elizabeth Taylor, Diana Rigg, and Lesley-Anne Down. It also features Len Cariou, Hermione Gingold, and Laurence Guittard who reprised their Broadway roles. The film was directed by Harold Prince.

==Plot==
Fredrik Egerman (Len Cariou) is very happy in his marriage to an 18-year-old virgin, Anne (Lesley-Anne Down). However, Anne has nervously protected her virginity for the whole eleven months of marriage, and being a bit restless, Fredrik goes to see an old flame, the famous actress, Desirée Armfeldt (Elizabeth Taylor). Desirée, who is getting tired of her life, is thinking of settling down, and sets her sights on Fredrik, despite his marriage, and her own married lover Count Carl-Magnus Mittelheim (Laurence Guittard). She gets her mother to invite the Egermans to her country estate for the weekend. But when Carl-Magnus and his wife, Charlotte (Diana Rigg), appear, too, things begin to get farcical, and the night must smile for the third time before all the lovers are united.

==Cast==
* Elizabeth Taylor as Desiree Armfeldt
* Diana Rigg as Charlotte Mittelheim
* Len Cariou as Frederick Egerman
* Lesley-Anne Down as Anne Egerman
* Hermione Gingold as Madame Armfeldt
* Laurence Guittard as Count Carl-Magnus Mittelheim
* Christopher Guard as Erich Egerman
* Lesley Dunlop as Petra
* Chloe Franks as Fredericka Armfeldt
* Jonathan Tunick as Conductor

==Production==
===Music===
Stephen Sondheim wrote the music and lyrics for the film. He wrote the lyrics for the "Night Waltz" theme ("Love Takes Time") and wrote an entirely new version of "The Glamorous Life", which has been incorporated into several subsequent productions of the stage musical.

===Location===
The setting for the film was moved from Sweden to Austria, and was filmed on location.

==Release and reception==
The film received mostly negative reviews, with much being made of Taylors wildly fluctuating weight from scene to scene. Some critics talked more positively of the film, with Variety (magazine)|Variety calling it "an elegant looking, period romantic charade". The film has received critical praise for Diana Riggs performance. The film currently holds an 11% Rotten rating on Rotten Tomatoes. 

==Awards and honors== Best Music, Original Song Score, and Its Adaptation or Best Adaptation Score (Won) Best Costume Design (Nominated)

==Home media==
{{Infobox album Italic title=no
| Name = A Little Night Music
| Type = Soundtrack
| Artist = Stephen Sondheim
| Cover = A_Little_Night_Music_Soundtrack.jpeg
| Released = 1977
| Recorded = 1974–1977
| Genre = Soundtrack
| Length = 49:07
| Label = Masterworks Broadway
| Producer = Jonathan Tunick
}}
A soundtrack was released on LP.  In 2013, Masterworks Broadway released an expanded edition on compact disc featuring one previously unreleased stereo track prepared for the LP and three mono tracks taken directly from the films soundtrack. 

The film was, for a time, available on VHS and Laserdisc. A DVD release was issued in June 2007. The new version of "The Glamorous Life" has been included on the new remastered version of the Original Broadway Cast Recording.

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 