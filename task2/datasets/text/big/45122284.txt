The Devil's Needle
{{Infobox film
| name           = The Devils Needle
| image          =
| caption        =
| director       = Chester Withey
| producer       = Fine Arts Film
| writer         = Chester Withey
| starring       = Norma Talmadge
| music          =
| cinematography =
| editing        =
| distributor    = Triangle Films
| released       = August 13, 1916
| runtime        = 59 minutes
| country        = USA
| language       = Silent ...English titles

}}
The Devils Needle is a 1916 silent film drama directed by Chester Withey and starring Norma Talmadge and Tully Marshall. It was produced by D. W. Griffiths Fine Arts Film Company and distributed by Triangle Films.   

A 1923 rerelease print survives at the Library of Congress.  

==Cast==
*Tully Marshall - David White
*Norma Talmadge - Rene, His Model
*Marguerite Marsh - Wynne Mortimer
*F. A. Turner - William Mortimer
*Howard Gaye - Hugh Gordon
*John Brennan - Fritz
*Paul Le Blanc - Buck

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 


 