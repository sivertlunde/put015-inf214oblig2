Razzia sur la chnouf
{{Infobox film
| name           = Razzia sur la chnouf
| image          = Razzia-sur-la-chnouf.jpg
| image size     =
| alt            =
| caption        = 
| director       = Henri Decoin
| producer       = Paul Wagner
| writer         = Auguste Le Breton  Henri Decoin Maurice Griffe
| narrator       =
| starring       = Jean Gabin Marcel Dalio Lino Ventura
| music          = Marc Lanjean
| cinematography = Pierre Montazel
| editing        = Denise Reiss
| studio         = Gaumont Film Company
| distributor    = 
| released       = 7 April 1955
| runtime        = 105
| country        = France 
| language       = French
| budget         =
| gross          =
}}
 French for "Raid on the Drug Ring") is a 1955 French gangster film directed by Henri Decoin and starring Jean Gabin. The screenplay is based on a novel by Auguste Le Breton. The film was released as Razzia in the USA. 

== Synopsis ==
A criminal organisation assigns French gangster Henri Ferré to optimise the operations of an existing narcotics ring in Paris.  The bar „Le Troquet“ serves as a front for these activities. The bars cashier Lisette has a crush on him right from the start. Even so, Henri Ferrés main interest is to assemble all persons who have a significant part in the drug trade. Because of that he is increasingly suspected to have a hidden agenda. Finally it turns out he is an undercover agent who strives to dismantle the organisation once and for all.

== Cast ==
*  " ("Le Nantais")
* Magali Noël: Lisette, the cashier of the "Troquet"
* Paul Frankeur: inspector Fernand
* Marcel Dalio: Paul Liski, boss of the French anti-narcotics force
* Lino Ventura: Roger „the Catalan“
* Albert Rémy: Bibi, Rogers accessory
* Lila Kedrova: Léa, an addict

== References ==
 

==External links==
*  
*  
*   (French)
*   Last accessed: July 3, 2014.


 

 
 
 
 
 
 
 
 
 
 
 
 
 

 
 