Juvenile Offender
 
{{Infobox film name           = Juvenile Offender  image          = File:Juvenile_Offender_poster.jpg director       = Kang Yi-kwan producer       = Hyeon Byeong-cheol writer         = Park Joo-young   Kang Yi-kwan starring       = Lee Jung-hyun   Seo Young-joo    music          = Kang Min-guk cinematography = Byun Bong-sun    editing        = Park Yu-gyeong distributor    = Finecut released       =   runtime        = 107 minutes country        = South Korea language       = Korean budget         =  gross          =   film name      = {{Film name hangul         = 범죄소년  rr             = Beomjoe Sonyeon  mr             = Pŏmjoe Sonyŏn}}
}}
Juvenile Offender ( ; lit. "Crime Boys") is a 2012 South Korean film about a teenage criminal who reunites with his mother who gave him up at birth.  

It won the Special Jury Prize at the 25th Tokyo International Film Festival, and Best Actor for Seo Young-joo.     It was also awarded the Lino Brocka Grand Prize and Best Actor at the 14th Cinemanila International Film Festival    as well as Best Childrens Feature Film at the 2013 Asia Pacific Screen Awards. 
 Best Foreign Language Film at the 86th Academy Awards,   but it was not nominated.

==Plot==
Ji-gu is a 16-year-old juvenile offender under probation who lives with his only known relative - his grandfather who is sick in bed all the time with severe diabetic complications. To Ji-gu, home is only a place that stinks with sickness and school, a place to hang out. His only interest in life is his sweet new girlfriend Sae-rom. One day, he gets caught after committing burglary with the neighborhood big boys. And without a parent to plead for him, the judge sentences Ji-gu to the juvenile reformatory while others get lighter penalties.

11 months later, Ji-gu is informed that his grandfather has passed away. After the funeral, his teacher tracks down Ji-gu’s mom, whom Ji-gu thought was dead but in fact had run away from home after giving birth to Ji-gu at age 17. Ji-gu is simply shocked to realize that he has a mother. But after he gets discharged from the juvenile reformatory, he and his young mother try to make up for their time lost. Ji-gu starts off living together with his mother with high expectations, but he soon realizes that his mother is much too young just like himself and he comes to understand why she had to leave him right after giving birth. But when Ji-gu falls into a similar situation with his girlfriend, his mother whom he thought would understand is appalled at the news which creates a conflict between the two. 

==Cast==
*Lee Jung-hyun - Hyo-seung 
*Seo Young-joo - Jang Ji-gu
*Choi Won-tae - Jae-beom
*Jeon Ye-jin
*Kang Rae-yeon
*Jung Seok-yong

==Awards and nominations==
2012 25th Tokyo International Film Festival
*Best Actor - Seo Young-joo

2012 14th Cinemanila International Film Festival
*Best Actor - Seo Young-joo
 49th Baeksang Arts Awards
*Nomination - Best Actress - Lee Jung-hyun
*Nomination - Best New Actor - Seo Young-joo

2013 22nd Buil Film Awards
*Nomination - Best New Actor - Seo Young-joo
*Nomination - Best New Director - Kang Yi-kwan

2013 7th Asia Pacific Screen Awards Best Childrens Feature Film

2013 56th Asia-Pacific Film Festival
*Nomination - Best Actor - Seo Young-joo
*Nomination - Best Actress - Lee Jung-hyun

==See also==
* List of submissions to the 86th Academy Awards for Best Foreign Language Film
* List of South Korean submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*    
*  
*  
*  

 
 
 
 