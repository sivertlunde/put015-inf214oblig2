Suryavanshi (film)
{{Infobox film  name     = Suryavanshi image    = Suryavanshi.jpg caption    = CD Cover writer   = Rakesh    producer = Vijay Kumar Galani director = Rakesh Kumar starring = Sheeba Saeed Jaffrey | music    = Anand-Milind released = 14 February 1992 language = Hindi Urdu
}}
 1992 Bollywood|Hindi-language Indian feature directed by Rakesh Kumar and produced by Vijay Kumar Galani, starring Salman Khan, Sheeba Akashdeep|Sheeba, Saeed Jaffrey and Amrita Singh. {{cite web|title=Suryavanshi  CD-Compact Disc|url=http://www.ebay.com/itm/Suryavanshi-Do-Pal-Music-Anand-Milind-Rajesh-Roshan-Soundtrack-Melody-UK-/161413917798?pt=Music_CDs&hash=item259504ec66
|publisher=ebay|accessdate=2014-11-29}} 

==Plot==

Suryavanshi is a love story and incorporates various Indian cultural/social themes such as reincarnation, marital devotion and loyalty.

==Summary==

DD (Ajit Vachani), a senior officer in the Archaeological Survey of India, and JB (Saeed Jaffrey), a businessman now living in the USA, have been inseparable friends since childhood. When JB returns to India after a long stay in the USA, he asks for DDs daughter Sonias (Sheeba) hand in marriage for his son Vicky (Salman Khan). But when Vicky arrives in India, he informs Sonia that he is not interested in marriage. Sonia understands and accepts this, but suggests they marry anyway (for their parents, as respectful children do) and that she will not stand in the way of his adventures. Vicky agrees and they get engaged.

Around this time DD receives word that the lead archaeologist, Kishore, on his current project (an excavation in a small hamlet called Sangramgadh) has mysteriously disappeared. He asks his project officer Mahesh (Puneet Issar) to investigate. Mahesh reviews Kishores notes and learns of a local baba (wise old man in the village) who seems to know a bit about the situation. They arrange to bring baba to the city.

Baba narrates the history of Sangramgadh. Once a happy realm, it has now fallen upon hard times accompanied by sinister events. Although the village land is arable, the harvested grain inexplicably self-incinerates overnight. Those who leave Sangramgadh assuredly meet financial or personal ruin outside, and those who stay are plagued by the suffering of a bleak and hopeless existence. The baba reveals this is all a result of a curse placed upon the village by the wandering atma (soul) of an ancient princess Suryalekha (Amrita Singh). The baba once visited the ruins of the old palace and summoned the atma, whereupon it appeared and commanded him: if the village must be freed from her thrall, they must bring her her Suryavanshi (a prince born of the Sun dynasty). After revealing the story, baba asks all listeners and DD to stay at outhouse. Meanwhile baba sees a photograph of Vicky who looks like Suryavanshi Vikram singh. Out of shock, tumbles onto the ground where shards of glass pierce through. DD, JB, Vicky, Sonia, Mahesh and a field team arrive in Sangramgadh. They are stumped when they discover that the palace ruins (above ground) are on a bed of solid rock, impervious to digging. The next morning, however, a minor earthquake occurs and they find a way through the rock (apparently at night) into the palace. They find Kishores body, hanging grotesquely by the neck, in the palace. They also find an ancient manuscript in Sanskrit.

This manuscript, written by the ancient Rajguru (royal preceptor) of Sangramgadh, chronicles the events surrounding Suryalekhas death, which took place a thousand years earlier. Suryalekha was an only child. She hated men and refused to be married. When her mother arranged for various princes to visit her, she subjected them to her tests of bravery i.e. single combat with a bunch of her trained villains (gladiators, man-eating leopards and a terrible cannibal, bred to destroy human beings). One such prospective suitor, Prince Amar Singh, bravely defeated her gladiators, but was crushed and devoured by the cannibal. Eventually, warrior Suryavanshi Vikram Singh (this character is loosely modeled on the Norse hero Thor) took up her challenge and valiantly destroyed all her minions.

Suryalekha accepts Vikram Singh and they have sex ostensibly. After having sex with Suryalekha, Vikram Singh reveals his true intention: he only sought vengeance for his friend Amar Singh and was not interested in marrying her. Suryalekha begs him not to leave her and go now that she has lost her virginity to him but he refuses to stay. As he prepares to leave the palace, the Rajguru stops him (citing insult upon the royal family). As they duel, Suryalekha leaps from the palace tower to her death. Seizing the momentary distraction, the Rajguru (who otherwise was no match for Vikram Singh) attacks from behind and kills the prince. Unable to bear the disgrace of this dishonorable act (albeit in defending the honor of the royal princess), the Rajguru resigns himself to the andha kuan (dark palace dungeon) for the rest of his life. The Rajmata (queen mother), pronounced a curse upon Suryalekhas atma: it will forever remain confined to the palace and never attain salvation. The palace and kingdom of Sangramgadh sank into the ground under a bed of rock and Suryalekhas atma took to terrorizing the village.

The manuscript stops abruptly. The final page (bearing a revelation of how the atma may be freed of the curse) is missing. At this point, Vicky realizes that he is the Suryavanshi prince reborn.

That night, Sonia and Vicky enter the palace ruins. Suryalekha comes forth from the mirror and welcomes her Prince with flowers. She is enraged at the sight of Sonia. She sends flaming missiles at Vicky. Vicky eventually channels the spirit of Suryavanshi and prepares to battle Suryalekha once again. Sonia, in the meantime, finds the outer garden where there is a large painting of the Rajmata and the final page of the Rajgurus manuscript with the incantation to defeat Suryalekha. Raj guru had revealed that Suryalekha is much more fascinated with her mirror image and her looks. She stays captured after sunrise and comes out after sunset in her large portrait. If anyone breaks this portrait when she is out, Suryalekhas soul will be freed. Vicky briefly changes form (similar to the transformation of He-Man) and, due to the incantation, Suryalekha start dying slowly. She finally accepts her destiny, asks for forgiveness, and passes on to the other side. Vicky and Sonia wearily climb out of the palace and watch the sunrise. Vicky now professes his love and pledges his loyalty to his wife Sonia.

The film ends with the couple running into the arms of their waiting parents.

==Cast==
* Salman Khan ... Vicky/Suryavanshi Rajkumar Vikram Singh
* Amrita Singh ... Princess Suryalekha
* Sheeba Akashdeep|Sheeba... Sonia
* Saeed Jaffrey ... J.B.
* Ajit Vachani ... Senior Archaeological officer D.D.
* Sushma Seth ... Rajmata
* Shakti Kapoor ... Rajguru of Sangramgadh
* Puneet Issar ... Mahesh
* Abhinav Chaturvedi... Ajit
* Kadar Khan ... Baba

==Soundtrack==
{{Infobox album Name     = Suryavanshi Type     = film Cover    =  Released =  Artist   = Anand Milind Genre  Feature film soundtrack Length   = 40:59 Label    = Venus
}}

{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- style="background:#3366bb; text-align:center;"
! # !! Song !! Singer(s)!! Length
|-
| 1
| "Main Nahin Kehta"
| Kumar Sanu, Asha Bhosle
| 06:31
|- 
| 2
| "Mujhe Sadiyon Se Tera"
| Kavita Krishnamurthy
| 05:40
|- 
| 3
| "Goodbye Namaste" 	
| Amit Kumar
| 06:18
|- 
| 4
| "Jogi Tere Pyar Mein" 
| Udit Narayan, Asha Bhosle
| 08:21
|- 
| 5
| "Yeh Log Poochte Hain"
| Udit Narayan, Kavita Krishnamurthy
| 05:51
|- 
| 6
| "Tu Hi Mere Dil Ka Jaani" 
| Mangal Singh, Asha Bhosle
| 08:18
|}

== References ==
 

== External links ==
*  

 
 
 
 
 