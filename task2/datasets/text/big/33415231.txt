Thar She Blows!
 
{{Infobox film
| name           = Thar She Blows!
| image          = 
| image_size     = 
| caption        = 
| director       = 
| producer       = Ken G. Hall
| writer         = Ken G. Hall
| narrator       = Lionel Lunn
| starring       = 
| music          = 
| cinematography = Walter Sully
| editing        = 
| studio         = Cinesound Productions
| released       = February 1931 
| runtime        = two reels 
| country        = Australia
| language       = English
| budget         = 
| gross          = 
}}

Thar She Blows! is a 1931 short Australian film, the first production from Cinesound Productions. It is a documentary on the West Australian whaling industry.

==Synopsis==
The action takes place at Point Close station, 500 miles north of Perth. A steamer sights a whale, plunges a harpoon into it, then draws the carcass to the ships side, whereupon it is attacked by a school of sharks. The whale is towed ashore, where it is stripped of its blubber and prepared for market.   

==Production==
Ken G. Hall got hold of some spectacular footage shot by Walter Sully on board a Norwegian whaler off the coast of West Australia, including scenes of a shark attacking a whale carcass. Hall wrote a commentary, had Lionel Lunn record it, and added a soundtrack to the film. Sound recording was primitive in Australia at the time and Hall could not add music or dub in an effects track.  

==Release==
The film was released in support of a Hollywood feature and received good reviews, particularly from Kenneth Slessor, then a critic for Smiths Weekly. 

The Sydney Morning Herald called it "an interesting film.  The Sydney Mail said it was "a promising example of local synchronisation."  The Brisbane Courier said "it is most entertaining from an educational viewpoint. The audience receives an insight into an industry, the value of which is not generally realised, in spite of the fact that it is carried on in Australian waters." 

The Adelaide New said that the film was:
 Australias answer to the eternal question, "Why cant we make pictures as good as America? From a production point or view, the film is 100 per cent perfect... the shots easily eclipse anything :of the kind attempted by cinematographers in other parts of the world... The whole production dealing with whaling and especially the scene where a school of sharks attacks a dead whale, could hold its own against any in the world.   On Our Selection (1932). Ken G. Hall, Directed by Ken G. Hall, Lansdowne Press, 1977 p 55. 

==References==
 

==External links==
*  at National Film and Sound Archive

 

 
 
 


 