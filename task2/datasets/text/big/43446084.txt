Shree (2013 film)
 
 

 	
 
{{Infobox film
| name         = Shree
| image        = Shree.Jpg
| Caption      = Official Poster
| director     = Rajesh Bachchani
| producer     = Vikram M. Shah
| writer       = Rajesh Bachchani
| starring     = Hussain Kuwajerwala Paresh Ganatra Anjali Patil Shivani Tanskale
| music        = Talaash Band-The Band of Seekers   Claver Menezes
| Cinematography        = Subhransu Das
| editing      = Noor Memon 
| released     =  
| country      = India
| language     = Hindi
}}

Shree   is a 2013 Bollywood Science Fiction thriller film starring Hussain Kuwajerwala, directed by Rajesh Bachchani and produced by Vikram M. Shah. The film released in India on 26 April 2013.

Shree is a tale of an ordinary man lured in by promises of a bright and successful future, trading in 12 hours of his life.

==Cast==
*Hussain Kuwajerwala as Shree,  An Ordinary Man
*Paresh Ganatra as Inspector Ganpat 
*Anjali Patil as Sonu
*K C Shankar as Tilak
*Shivani Tanskale as Sheena
* Rio Kapadiya as Jairaj

==Plot==

SHREE ( Hussain Kuwajerwala ), an ordinary man, works at a telecom company. In love with girl but cannot marry yet as need to make some basic money. Randhawa, a wealthy businessman, in his ceaseless pursuit of power, has zeroed in on Shree as his last guinea pig. Shree is the last piece of puzzle of a scientific experiment, devised by a brilliant scientist, supported by commissioner of police and funded by Randhawa. If successful, it could change the fate of their lives and that of the whole world. Lured in by promise of a bright and successful future, Shree trade in 12 hours of his life. Randhawa offers him 20 lakhs for participating in an experiment which will last for 12 hours. He trust them and needs the money, excitedly Shree trade in 12 hours of his life. But as soon as these 12 hours start, he finds that his simple life has been turned upside down. Accused of murdering the scientist, the commissioner of police and many more people. Shree an ordinary man, now is the most wanted man in the city. Now must use his wit and intelligence to prove his innocence. Now he must rise above the ordinary, and become Extraordinary.

==Music==
Hussain Kuwajerwala at the official music launch  {{cite news|title=Celebs grace the first look & music launch of the film Shree
|url=http://www.bollywoodhungama.com/more/photos/view/stills/parties-and-events/id/1777848|newspaper=Bollywood Hungama|date=4 April 2013}}   of Shree on 2 April 2013 with producer Vikram M. Shah, director Rajesh Bachchani singer Sugandha and the cast. The music of the movie Shree is given by the Talaash band.

==Movie Reviews==

Shree got mixed response from the critics as well as the audience. Overall, it managed to garner around 3* from the critics.

==References==
 

==External links==
*  

 
 
 
 
 