Burning Sands
{{Infobox film
| name           = Burning Sands
| image          = 
| alt            = 
| caption        = 
| director       = George Melford
| producer       = Jesse L. Lasky
| screenplay     = Olga Printzlau Waldemar Young
| starring       = Wanda Hawley Milton Sills Louise Dresser Jacqueline Logan Robert Cain Fenwick Oliver Winter Hall
| music          = 
| cinematography = Bert Glennon 
| editing        = 
| studio         = Famous Players-Lasky Corporation
| distributor    = Paramount Pictures
| released       =  
| runtime        = 70 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 drama silent film directed by George Melford and written by Olga Printzlau and Waldemar Young. The film stars Wanda Hawley, Milton Sills, Louise Dresser, Jacqueline Logan, Robert Cain, Fenwick Oliver and Winter Hall. The film was released on September 3, 1922, by Paramount Pictures.  
 
==Plot==
 

== Cast ==
*Wanda Hawley as Muriel Blair
*Milton Sills as Daniel Lane
*Louise Dresser as Kate Bindane
*Jacqueline Logan as Lizette
*Robert Cain as Robert Barthampton
*Fenwick Oliver as Mr. Bindane
*Winter Hall as Governor
*Harris Gordon as Secretary
*Alan Roscoe as Ibrihim
*Cecil Holland as Old Sheik
*Joe Ray as Hussein 

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 