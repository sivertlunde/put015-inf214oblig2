The Resurrection of Broncho Billy
 
{{Infobox film
| name           = The Resurrection of Broncho Billy
| image          =
| image_size     =
| caption        =
| director       = James Rokos
| producer       = John Longenecker
| writer         = John Carpenter Nick Castle Jim Rokos John Longenecker Trace Johnston
| narrator       = Ruth Hussey Ricky Nelson
| starring       = Johnny Crawford Kristin Nelson
| music          = John Carpenter
| cinematography = Nick Castle
| editing        = John Carpenter
| distributor    =
| released       =  
| runtime        = 23 min.
| country        = United States English
| budget         =
}}
 short Western Western film, Best Short Subject.   

It was John Carpenters very first work: he acted as editor, composer of the music and co-writer of the film.

==Plot==
The Resurrection of Broncho Billy is the story of a young man (Johnny Crawford) who lives in a big city in present time, but his dreams are of the old west and western film heroes. Scenes of his everyday life take on the style of a western film as he visits with a western old timer Wild Bill Tucker; he crosses a busy boulevard packed with traffic and we hear the sound of a cattle drive; hes late for work at the hardware store; at an intersection crosswalk he has a western street showdown with a businessman as the light changes; he enters a saloon but has no I.D. for a beer; he is accosted in an alleyway; a pretty counter girl (Merry Scanlon) gives him soda but he realizes he has no money to pay for it. 

Then he meets a lovely artist (Kristin Nelson) in a park who draws a sketch of him in an old west setting and he talks to her for a time on a park bench about the old west and western films. The Artist gets up to leave and we hear the sound of hoofbeats as he rides up to her in the old west. The artist gives him back the watch he lost in the alley scuffle, she floats up onto his horse and they ride off across the prairie as the Broncho Billy theme song is heard over the scene. Hes taken her back to the magic old west that he loves.

==Production notes== USC Cinema 480 undergraduate production course at USC where he produced The Resurrection of Broncho Billy. The Super Crew was the name given to the group of filmmakers Longenecker brought together at USC and each of the four filmmakers made contributions to the story. Nick Castle was the cinematographer, John Carpenter was the film editor and wrote the original theme music for the picture, and James Rokos was the films director.

==Release==
Johnny Crawford and John Longenecker invited executives at Universal Studios to release the picture theatrically. It opened on December 25, 1970, in Westwood Village at the Mann Theatres National for what was intended as a one week Academy Award qualifying run. The theater continued to play the film for fourteen weeks. After winning an Oscar, Universal Studios distributed the short film with their feature movies for the next two years throughout the United States and Canada.

==Gallery==
 

==See also==
* List of American films of 1970

==References==
 

==External links==
*  
* 

 
 

 
 
 
 
 
 
 
 
 
 
 