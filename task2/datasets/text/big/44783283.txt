The Straight Road
{{Infobox film
| name           = The Straight Road
| image          = The Straight Road poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Allan Dwan
| producer       = Daniel Frohman
| screenplay     = Clyde Fitch
| starring       = Gladys Hanson William Russell Iva Shepard Arthur Hoops Lorraine Huling
| music          = 
| cinematography = 
| editing        = 
| studio         = Famous Players Film Company
| distributor    = Paramount Pictures
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 drama silent film directed by Allan Dwan and written by Clyde Fitch. The film stars Gladys Hanson, William Russell, Iva Shepard, Arthur Hoops and Lorraine Huling. The film was released on November 12, 1914, by Paramount Pictures.  

==Plot==
 

== Cast ==
*Gladys Hanson as Mary Moll OHara
*William Russell as Bill Hubbell
*Iva Shepard as Lazy Liz
*Arthur Hoops as Douglas Aines
*Lorraine Huling as Ruth Thompson

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 

 