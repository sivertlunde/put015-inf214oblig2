OSS 117: Lost in Rio
 
 
{{Infobox film
| name           = OSS 117: Lost in Rio
| image          = OSS 117Rio.jpg
| director       = Michel Hazanavicius
| producer       = Eric Altmeyer Nicolas Altmeyer
| screenplay     = Jean-François Halin Michel Hazanavicius
| story          = Jean Bruce (novels)
| starring       = Jean Dujardin Louise Monot
| music          = Ludovic Bource
| cinematography = Guillaume Schiffman
| editing        =  Gaumont
| released       =  
| runtime        = 101 minutes
| country        = France
| language       = French
| budget         = $23,200,000
| gross          = $39,569,831 
}}
OSS 117: Lost in Rio, in the original  ) is a 2009 French comedy film directed by  .

==Plot== Red Chinese Nazi sympathizers.  

Once in Rio, Hubert is attacked at various times by relatives of Mr. Lees gunmen, encounters a foul-mouthed American Felix Leiter-type CIA agent Bill Tremendous, the femme fatale Carlotta, Professor Von Zimmels luchador enforcers and eventually Mossad agents intent on bringing Professor Von Zimmel back to Israel for trial.  OSS 117 teams up with Dolorès Koulechov, a beautiful Israeli Army Colonel to bring Von Zimmel to Israel in the manner of Adolf Eichmann. Their lead to Von Zimmel is his son who is now a hippie.

Throughout the film the main character has two main romantic interests. The first is a mysterious beauty Carlotta (Reem Kherici). The second is Israeli Army officer, Dolorès Koulechov, who spends most of the film exasperated at OSS 117s Misogyny|misogynistic, Racism|racist, colonial tendencies and has no interest in the main character, but warms up to him in the end. When asked by de La Bath why Koulechov does not like the dictatural Brazilian military government of the time, she lists its examples of totalitarianism that the puzzled de La Bath finds the same as the France of Charles de Gaulle.

:- Do you know what a dictatorship is ? Its when people are communists, when they are cold with gray hats and boots with zippers. Thats a dictatorship !

:- Then, what do you call a country with a military leader controlling everything, a secret police, a single TV channel with every information controlled by the state ?

:- I call that "France", Miss. General De Gaulles France...

==Cast==
*Jean Dujardin as Hubert Bonisseur de La Bath, alias OSS 117
*Louise Monot as Dolorès Koulechov
*Alex Lutz as Heinrich Von Zimmel
*Rüdiger Vogler as Professor Von Zimmel

==Background==
The film is a continuation of the OSS 117 series of Eurospy films from the 1950s and 1960s, which were in turn based on a series of novels by Jean Bruce, a prolific French popular writer. The main character in the OSS 117 series is a secret agent of the SDECE, Hubert Bonisseur de La Bath, also known by his code name OSS 117. The character is played by Jean Dujardin.
 Christ the rear projection and camera movements are simple, and avoid the three-dimensional Steadycam and crane movements that are easily accomplished today.  The film also parodies Alfred Hitchcocks Vertigo (film)|Vertigo and North by Northwest with de La Baths clothing inspired by Harper (film)|Harper and the set furniture inspired by Dean Martins Matt Helm films. 

The film opens and closes with  ) (1965).

==Notes==
 

==External links==
*    
*    
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 