Fractured (film)
{{Infobox film
| name           = Fractured
| image          = Fractured Movie Poster 2013 Gierasch.jpg
| alt            = 
| caption        = 
| director       = Adam Gierasch
| producer       = 
| writer         = Jace Anderson Adam Gierasch
| starring       = Callum Blue Vinnie Jones Ashlynn Yennie
| music          = Adam Barber
| cinematography = Scott Winig
| editing        = Andrew Cohen Erin Deck
| studio         = 4PIX, Schism
| distributor    = Seven Arts Pictures, Uncorkd Entertainment 
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Fractured (originally known as Schism) is a 2013 horror thriller film that was directed by Adam Gierasch, based on a script by Gierasch and his writing partner Jace Anderson.  The film had its world premiere on October 12, 2013 at the Screamfest Horror Film Festival.  It stars Callum Blue as an amnesiac trying to discover what exactly happened to him.

==Synopsis==
Dylan White (Callum Blue) has no memory of his life prior to waking up from a coma. As such hes had to create an entirely new identity for himself, that of an average, ordinary cook in Baton Rouge. Things are normal, even boring for Dylan until he begins to experience several horrifying visions that force him to look further to discover who he actually was, even if it could kill him.

==Cast==
*Callum Blue as Dylan / Jaron
*Vinnie Jones as Quincy
*Ashlynn Yennie as Brandy
*Nicole LaLiberte as Marlena
*Jon Eyez as Reggie
*Eric F. Adams as Gary
*Steve DeMartino as Bartender
*Lily Virginia Filson as Hell Girl with Tattoo
*Brent Phillip Henry as Johnny (as Brent Henry)
*Jeanine Hill as Coma Patient
*Samantha Ann Huffman as Hell Girl Indigo as Holly
*Skitch King as Motel Clerk
*Charlotte Kirk as Hellion Ken Massey as Brothel John

==Production== The Human Centipede.    The film was given its world premiere in 2013 under the name Schism, but had to undergo a name change before it was given a further release due to the foreign sales company handling the movie. 

==Reception==
Fearnet gave a positive review for Fractured and praised one of the films death scenes, as they found it "so dark, disturbing, and visually powerful that it allows one to overlook some rather egregiously cheap CGI gun play that pops up later on."  In contrast, Shock Till You Drop and About.com both gave less positive reviews,  with About.com stating that the acting of the films leads was solid but that the supporting cast was "more uneven" and that the direction was "visually bland". 

==References==
 

==External links==
*  

 

 
 
 
 
 