The Cat's Pajamas
{{Infobox film
| name           = The Cats Pajamas
| image          = 
| alt            = 
| caption        = 
| director       = William A. Wellman	
| producer       = Jesse L. Lasky Adolph Zukor
| screenplay     = Louis D. Lighton Hope Loring Ernest Vajda
| starring       = Betty Bronson Ricardo Cortez Arlette Marchal Theodore Roberts Gordon Griffith Tom Ricketts
| music          = 
| cinematography = Victor Milner
| editing        =  
| studio         = Famous Players-Lasky Corporation
| distributor    = Paramount Pictures
| released       =  
| runtime        = 60 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 comedy silent film directed by William A. Wellman and written by Louis D. Lighton, Hope Loring and Ernest Vajda. The film stars Betty Bronson, Ricardo Cortez, Arlette Marchal, Theodore Roberts, Gordon Griffith and Tom Ricketts. The film was released on August 29, 1926, by Paramount Pictures.  

==Plot==
 

== Cast ==
*Betty Bronson as Sally Winton
*Ricardo Cortez as Don Cesare Gracco
*Arlette Marchal as Riza Dorina
*Theodore Roberts as Sallys Father
*Gordon Griffith as Jack
*Tom Ricketts as Mr. Briggs 

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 