Winter Journey (film)
{{Infobox film
| name           = Winter Journey
| image          = Winter Journey (2013 film) poster.jpg
| caption        = Theatrical release poster
| director       = {{plainlist|
* Lubov Lvova
* Sergey Taramaev
}}
| producer       =
| writer         = {{plainlist|
* Lubov Lvova
* Sergey Taramaev
}}
| starring       = {{plainlist|
* Aleksey Frandetti
* Evgeniy Tkachuk
}}
| music          =
| cinematography = Mikhail Krichman
| editing        =
| studio         = Mika Film
| distributor    =
| released       =  
| runtime        = 90 minutes
| country        = Russia
| language       = Russian
| budget         =
}}
Winter Journey ( , Transliteration|translit. Zimniy put) is a 2013 Russian drama film about a young classical singer who falls in love with a street thug.    The film takes its title from a Schubert song cycle, Winterreise, that the hero, Erik, a music student, is practising for a competition.  

==Plot==
Music student and gifted singer, Erik, is preparing to sing Schuberts Winterreise for a competition. As his teacher slams him for his poor performance, Eriks life is changed irrevocably by a chance meeting with Lyokha, a coarse and aggressive petty criminal.  

==Cast==
* Aleksey Frandetti as Erik
* Evgeniy Tkachuk as Lyokha

==Reception==
The Dutch film director Jos Stelling described the film as “...a genuine film...(which) went straight to my heart. The theme of the alleged homosexuality hardly played a role for me... To me this film sometimes approached the status of a masterpiece for its cinematographic values”.    

==Awards==
{| class="wikitable sortable"
! Award / Film Festival !! Year !! Category !! Recipient(s) and nominee(s) !! Result !! Ref.
|-
| Nika Award
| 2013
| Best Actor
| Evgeniy Tkachuk
|  
|  
|-
| London Russian Film Festival
| 2013
| Best Film
| Winter Journey
|  
|  
|-
| Lecce Festival of European Cinema
| 2014
| Best Cinematography
| Mikhail Krichman
|  
|  
|}

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 