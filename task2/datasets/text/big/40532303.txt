Transit (2013 film)
{{Infobox film name           =Transit image          =Transit (2013) movie poster.jpg caption        =Transit (2013) director       =Hannah Espia producer      =Paul Soriano writer         = screenplay     =Giancarlo Abrahan Hannah Espia cinematography   =  editing          =  music            =  starring       = Irma Adlawan Marc Alvarez Mercedes Cabral Jasmine Curtis Ping Medina studio = Cinemalaya TEN17P distributor =  released   =  runtime        = 93 minutes country        = Philippines language  Filipino  Tagalog English English Hebrew Hebrew
|budget           = gross            =
}} Cinemalaya 2013. The film won Best Film, directing, acting and other technical awards.
 OFW who was bringing home his child from Israel. In 2009, the Israeli government enacted a law that deports the children of migrant workers unless they fulfill a certain criteria. Both Israeli and migrant workers rallied against the law that separates parents from their children.  
 Best Foreign Language Film at the 86th Academy Awards,    but it was not nominated.

==Plot==
The film begins and ends in an airport during a father and son’s transit ﬂight from Tel Aviv to Manila.  It tells the story of Moises (Ping Medina), a Filipino single-dad working as a caregiver in Herzliya, Israel, who comes home to his apartment in Tel Aviv to celebrate his son Joshua (Marc Justine Alvarez)’s 4th birthday. It was on that day that Moises, together with their Filipino neighbors Janet (Irma Adlawan), and her daughter Yael (Jasmine Curtis), ﬁnd out that the Israeli government is going to deport children of foreign workers.  Afraid of the new law, Moises and Janet decide to hide their children from the immigration police by making them stay inside the house.  

==Cast==
*Ping Medina as Moises
*Irma Adlawan as Janet
*Jasmine Curtis-Smith as Yael
*Marc Justine Alvarez as Joshua
*Mercedes Cabral as Tina
*Perla (Pnina) Bronstein as Rotem
*Omer Juran as Omri
*Toni Gonzaga as Joshuas Mother
*Menahem Godick as Israeli policeman

==Awards== 2013 Cinemalaya Film Festival (New Breed)
** Best Film 
** Audience Award
** Best Director – Hannah Espia
** Best Actress – Irma Adlawan
** Best Supporting Actress – Jasmine Curtis-Smith
** Special Jury Citation for Best Acting Ensemble – (Irma Adlawan, Marc Alvarez, Mercedes Cabral, Jasmine Curtis, Ping Medina)
** Best Cinematography – Ber Cruz and Lyle Nemenzo Sacris
** Best Editing –  Benjamin Tolentino and Hannah Espia 
** Best Score – Mon Espia
*2014 Gawad Urian Awards
** Best Director – Hannah Espia

==See also==
* List of submissions to the 86th Academy Awards for Best Foreign Language Film
* List of Philippine submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 