My Darling, My Darling (film)
 
{{Infobox film
| name           = My Darling, My Darling
| image          = 
| caption        = 
| director       = Eduard Zahariev
| producer       = Angel Nikolov Alexander Tomov
| starring       = Mariana Dimitrova
| music          = 
| cinematography = Stefan Trifonov
| editing        = Magda Krasteva
| distributor    = 
| released       = 3 February 1986
| runtime        = 107 minutes
| country        = Bulgaria
| language       = Bulgarian
| budget         = 
}}

My Darling, My Darling ( , Transliteration|translit.&nbsp;Skapa moya, skapi moy) is a 1986 Bulgarian drama film directed by Eduard Zahariev. It was entered into the 36th Berlin International Film Festival.   

==Cast==
* Mariana Dimitrova as Ana
* Plamen Serakov as Ivan
* Ivan Donev as Vlado
* Raya Bachvarova as Raya
* Andrey Todorov as Andro
* Anton Radichev as Mitko
* Stoyan Stoev as Grigor
* Ana Guncheva as Minka Bozhidar Iskrenov as Bozho
* Katya Todorova as Sasedkata
* Blagovest Argirov as Blago
* Svetoslav Argirov as Svet
* Pepa Armankova as Pepa

==References==
 

==External links==
* 

 
 
 
 
 
 
 