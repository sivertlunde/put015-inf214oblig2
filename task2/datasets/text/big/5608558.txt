Another Fine Mess
 
 
 
{{Infobox film
  | name = Another Fine Mess
  | image =Another fine mess 1930 poster.jpg
  | caption = Theatrical release poster
  | director = James Parrott
  | producer = Hal Roach
  | writer = H.M. Walker 
  | starring = Stan Laurel   Oliver Hardy
  | music = Leroy Shield Jack Stevens
  | editing = Richard C. Currier
  | distributor = Metro-Goldwyn-Mayer
  | released =  
  | runtime = 28 09"
  | language = English
  | country = United States
  | budget =
}}
 Duck Soup.

==Plot== vagabonds being cellar of James Finlayson), South Africa. rented out chambermaid Agnes.

During a girl-talk scene with Thelma Todd and Stan (disguised as Agnes), Stans comments get sillier and sillier. The real Colonel returns to fetch his bow and arrows, to find the disorder that had ensued after his departure. Ollie continues his masquerade as Colonel Buckshot to the real colonel, until he sees the portrait on the wall of the real owner. Stan and Ollie escape the ensuing row dressed as a wildebeest on a stolen tandem bicycle. They ride into a railroad tunnel and encounter a train, but emerge riding unicycles.

==Cast==
*Stan Laurel
*Oliver Hardy
*Harry Bernard
*Bobby Burns
*Betty Mae Crane
*Beverly Crane Eddie Dunn
*Jimmy Finlayson
*Charles K. Gerrard
*Bill Knight
*Bob Mimford
*Gertrude Sutton
*Thelma Todd

==Production==
Unlike other Laurel and Hardy shorts, the technical credits are recited by two girls in usherette outfits. Beverly and Betty Mae Crane performed the "talking titles" for several Roach productions during the 1930–31 season as an experimental alternative to standard title cards.

This was also the first Laurel and Hardy film to feature the well-known Leroy Shield scorings for background music. A couple of previous episodes began experimenting with it, but, beginning with this film, these tunes would be heard regularly in Our Gang, Charley Chase, Boy Friends, and other Hal Roach productions.

No foreign-language versions are known to exist of this short. It was possibly shown with subtitles in non-English-speaking countries, as audiences were critical of the unnatural quality of the alternate versions.

== See also ==
* List of American films of 1930

== External links ==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 