Danger List
 
 
 
 
{{Infobox Film
| name           = Danger List
| image          = 
| image_size     = 
| caption        = 
| director       = Leslie Arliss
| producer       = Anthony Hinds Executive producer Michael Carreras
| writer         = J.D. Scott
| narrator       = 
| starring       = Philip Friend Honor Blackman
| music          = Edwin Astley Arthur Grant
| editing        = A.E. Cox
| distributor    = 
| released       = 1959
| runtime        = 22 min
| country        = United Kingdom
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}} Arthur Grant, and has a score by Edwin Astley. The running time is 22 minutes.

==Plot==
A nurse in the dispensary of an English hospital is suffering with a migraine, and accidentally dispenses the wrong medicines to three patients. The police and doctors have little time to locate the patients before the consequences are fatal.

All three patients are located. However, the husband (Johns) of the third uses the pills to kill his wife, who is already suffering from a terminal illness, and takes one himself to join her in death.

==Cast==
* Philip Friend as Dr. Jim Bennett 
* Honor Blackman as Gillian Freeman 
* Mervyn Johns as Mr. Ellis 
* Constance Fraser as Mrs. Ellis  Alexander Field as Mr. Carlton 
* Muriel Zillah as Mrs. Coombe 
* Amanda Coxell as Laura Coombe 
* Everley Gregg as Neighbour 
* Pauline Olsen as Young Nurse 
* Jeremy Longhurst as Mobile Policeman 
* Patricia Cree as Audrey

== External links ==
*  
* http://ftvdb.bfi.org.uk/sift/title/181520

 
 
 
 
 
 
 