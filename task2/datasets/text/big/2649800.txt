As White as in Snow
 
{{Infobox film
| name = As White as in Snow
| image = As white as in snow.jpg
| caption = Swedish DVD cover
| director = Jan Troell
| producer = Lars Hermann Kerstin Bonnier Erik Crone Johan Mardell
| writer = Jacques Werup (novel) Jan Troell Karl-Erik Olsson-Snogerod Jimmy Karlsson
| starring = Amanda Ooms Rikard Wolff Björn Granath Björn Kjellman
| music = Score Magnus Dahlberg
| editing = Jan Troell
| distributor = Svensk Filmindustri
| released = 1 February 2001  
| runtime = 154 minutes
| country = Sweden
| language = Swedish
| budget = SEK 30 million
| gross =  
}}
 best film, best direction best screenplay .

The screenplay is based on the novel Den ofullbordade himlen by Jacques Werup, which in turn is very loosely inspired by the life of Elsa Andersson, the first woman aviator in Sweden. She is portrayed by Amanda Ooms in the movie. Other key characters are played by Björn Granath, Stina Ekblad, Shanti Roney, Björn Kjellman, Reine Brynolfsson and Rikard Wolff.

The grammatically awkward title (literally translated "As white as a snow") is a quote from a poem/song ("Lejonbruden"), which is performed in the movie.

== Cast ==
* Amanda Ooms - Elsa Andersson
* Rikard Wolff - Robert Friedman
* Björn Granath - Sven Andersson
* Björn Kjellman - Erik Magnusson
* Stina Ekblad - Stine
* Shanti Roney - Lars Andersson
* Hans Pålsson - Felix Hansson
* Antti Reini - Koivunen
* Reine Brynolfsson - Enoch Thulin
* Maria Heiskanen - Merja

==External links==
* 
* 
* 

 
 

 
 
 
 
 
 
 
 


 