Boys Town (film)
{{Infobox film
| name           = Boys Town
| image          = Boys_town.jpg
| caption        = Theatrical release poster
| director       = Norman Taurog
| producer       = John W. Considine Jr. John Meehan
| starring       = Spencer Tracy Mickey Rooney Henry Hull
| cinematography = Sidney Wagner Edward Ward
| editing        = Elmo Veron
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 96 minutes
| country        = United States
| language       = English budget = $772,000  .  gross = $4,058,000 
}} biographical drama Father Edward J. Flanagans work with a group of underprivileged and delinquent boys in a home that he founded and named Boys Town (organization)|"Boys Town". It stars Spencer Tracy as Father Edward J. Flanagan, and Mickey Rooney, Henry Hull, Gene Reynolds, Edward Norris and Addison Richards.
 John Meehan, and was directed by Norman Taurog.
 MGM Studio head Louis B. Mayer, who was a Ukrainians|Ukrainian-American Jew known for his respect for the Catholic Church, later called this his favorite film of his tenure at MGM.   
 Boys Town is a community outside of Omaha, Nebraska.   
 MGM made a sequel, Men of Boys Town (see below), with Spencer Tracy and Mickey Rooney reprising their roles from the earlier film.

==Plot==
The story opens when a convicted murderer asks to make his confession on the day of his execution. The condemned man is visited by an old friend, Father Flanagan (Spencer Tracy) who runs a home for indigent men in Omaha, Nebraska. When the prison officials suggest that the condemned man owes the state a debt, Father Flanagan witnesses the condemned mans diatribe to prison officials and a reporter that describes his awful plight as a homeless and friendless boy who was a ward in state institutions. After the convicted man asks the officials to leave, Father Flanagan provides some comfort and wisdom. On the train back to Omaha, Father Flanagan is transformed in his humanitarian mission by revelations (echoed in the words) imparted by the condemned mans litany of hardships experienced as a child without friends or family as a ward of the state.

Father Flanagan believes there is no such thing as a bad boy and spends his life attempting to prove it. He battles indifference, the legal system, and often the boys themselves, to build a sanctuary which he calls Boys Town. The boys have their own government, make their own rules, and dish out their own punishment. One boy, Whitey Marsh (Mickey Rooney) is as much as anyone can handle. Whiteys older brother, in prison for murder, asks Father Flanagan to take Whitey (himself a poolroom shark and tough talking hoodlum) to Boys Town. Whiteys older brother escapes custody during transfer to federal prison. After thinking he has caused the death of a younger boy, Whitey leaves the un-fenced Boys Town and wanders the streets of town. Whitey is accused of bank robbery and murder on circumstantial evidence. Popular sentiment (stirred by sensationalized media reports headed by an unsympathetic newspaper owner) turns against Boys Town, and it seems likely the home will be permanently closed.  Whitey joins his brother, but Father Flanagan rescues Whitey and helps capture the gang in the act of robbery. Whitey and Father Flanagan return to Boys Town. 

==Cast==
* Spencer Tracy as Father Flanagan 
* Mickey Rooney as Whitey Marsh 
* Henry Hull as Dave Morris 
* Leslie Fenton as Dan Farrow 
* Gene Reynolds as Tony Ponessa 
* Edward Norris as Joe Marsh 
* Addison Richards as The Judge 
* Minor Watson as The Bishop 
* Jonathan Hale as John Hargraves 
* Bobs Watson as Pee Wee 
* Martin Spellman as Skinny 
* Mickey Rentschler as Tommy Anderson 
* Frankie Thomas as Freddie Fuller  Jimmy Butler as Paul Ferguson  Sidney Miller as Mo Kahn
* Janet Beecher as Mrs. Graves
* Everett Brown as Barky

==Reception==
The film was a massive hit and earned MGM over $2 million in profit. James Curtis, Spencer Tracy: A Biography, Alfred Knopf, 2011 p370 

==Awards==
{| class="wikitable" border="1"
|-
! Award !! Result !! Winner
|- Outstanding Production You Cant Take It With You 
|- Best Director You Cant Take It With You 
|- Best Actor ||   || Spencer Tracy
|- Best Writing, John Meehan and Dore Schary    Winner was Ian Dalrymple, Cecil Arthur Lewis, W. P. Lipscomb, George Bernard Shaw - Pygmalion (1938 film)|Pygmalion 
|- Best Writing, Original Story ||   || Eleanore Griffin and Dore Schary
|-
|}

In February 1939, when he accepted his   hastily struck another inscription, Tracy kept his statuette, and Boys Town got one, too. It read: "To Father Flanagan, whose great humanity, kindly simplicity, and inspiring courage were strong enough to shine through my humble effort. Spencer Tracy." 

==Box office==
According to MGM records the film earned $2,828,000 in the US and Canada and $1,230,000 elsewhere resulting in a profit of $2,112,000. 

==Home video releases== MGM on March 29, 1993 and re-released on VHS on March 7, 2000. On November 8, 2005, it was released on DVD as a part of the "Warner Brothers Classic Holiday Collection", a 3-DVD set which also contains Christmas in Connecticut and the 1938 version of A Christmas Carol, and as an individual disc. The DVD release also includes the 1941 sequel Men of Boys Town as an extra feature.

==Sequel: Men of Boys Town==
{{Infobox film
| name           =Men of Boys Town
| image_size     =
| image	         = 
| caption        =
| director       = 
| producer       = 
| writer         = 
| based on = 
| starring       = Spencer Tracy
| music          =
| cinematography =
| editing        =
| distributor    = Metro-Goldwyn-Mayer
| released       =1941
| runtime        = 
| country        = United States English
| budget         = $862,000  . 
| gross          = $3,166,000 
| preceded_by    =
| followed_by    =
}}
Released in April 1941, Men of Boys Town takes a darker tone to the plight of homeless and troubled youth. Tracy and Rooney reprise their characters as Father Flanagan and Whitey Marsh as they expose the conditions in a boys reform school. This movie was released on VHS on December 23, 1993, but is now available only as an extra feature on Boys Towns DVD.

===Plot===
Mr. and Mrs. Maitland, a childless couple, invite Whitey to their home on a trial basis. Whitey tries to visit a friend in reform school and inmate Flip is hiding in a car as Whitey leaves. Flip steals money and both boys go to reform school. (This is where the movie takes a darker tone as it depicts, using indirect camera angles, the physical abuse the boys suffer in detention at the facility). Father Flanagan exposes the conditions in the school and the boys are released to him. The Maitlands work to pay off the debts threatening Boys Town.

===Reception===
The film was a hit and became the 9th most popular movie at the US box office in 1941. FILM MONEY-MAKERS SELECTED BY VARIETY:  Sergeant York Top Picture, Gary Cooper Leading Star
New York Times (1923-Current file)   31 Dec 1941: 21.  According to MGM records the film earned $2,009,000 in the US and Canada and $1,157,000 elsewhere resulting in a profit of $1,269,000. 

==Popular culture==
In the Northern Exposure television series 1991 episode "The Big Kiss", orphan Ed Chigliak watches Boys Town and is inspired to find out who his real parents are. He mentions the film reference to several other characters.

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 