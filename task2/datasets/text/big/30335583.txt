Palangal
{{Infobox film
| name           = Palangal
| image          = Bharath Gopi and Nedumudi Venu in Palangal.jpg
| caption        = Bharath Gopi and Nedumudi Venu in Palangal
| director       = Bharathan
| producer       = 
| writer         = John Paul Puthusery Shankar
| Johnson
| cinematography = Ramachandra Babu
| editing        = Suresh
| distributor    = 
| released       =  
| runtime        = 140 minutes
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}}

Palangal ( ) is a 1981 Malayalam film written by John Paul Puthusery and directed by Bharathan starring Nedumudi Venu, Zarina Wahab, Bharath Gopi and Shankar (actor)|Shankar. Films plot is a love story of a man who works in railway lines and a woman who lost his lover in railway lines. Majority of the film was shot in Shornur due to the persistence of Bharathan as he always wants to hear the siren of the trains in the whole film. Coal engine was used in those periods in train and engines were brought from Madras for shooting this film. 

==Plot==

Usha(Zarina Wahab) comes to her sisters house for relief from a terrible tragedy of her life which is the accidental death of her lover Ravi played by Shankar (actor)|Shankar. In the new place she met Ramankutty played by Nedumudi Venu who is friend of Vasu Menon(Bharath Gopi) husband of her sister. Their friendship eventually turn into love, but Ramankuttys mother does not allow this proposal. Usha slowly realizes that Vasu Menons feelings for her are wayward, but she does not tell her sister as she fears that this will ruin their family. So Usha decides to go back to her home on the way she meets Ramankutty who has convinced his mother about he marrying Usha.

== Cast ==
* Nedumudi Venu as Ramankutty
* Zarina Wahab as Usha
* Bharath Gopi as Vasu Menon Shankar as Ravi
* Bahadoor as Varkey.
* K. P. A. C. Lalitha as Geetha, elder sister

==Soundtrack==  Johnson and lyrics was written by Poovachal Khader. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Etho Janma Kalpanayil || Vani Jairam, Unni Menon || Poovachal Khader || 4:05
|- 
| 2 || Pookondu Poo Moodi || K. J. Yesudas, Vani Jairam || Poovachal Khader || 4:26
|}

== External links ==
*  
*http://ddjunction.blogspot.com/2008/12/palangal-1981-malayalam-movie-by.html
*http://www.simplymalayalees.com/forum_posts.asp?TID=561

 
 
 
 
 

 