The Living Truth
{{Infobox film
| name           = The Living Truth
| image          =
| alt            =
| caption        =
| film name      = Živa istina
| director       = Tomislav Radić 
| producer       = Kruno Heidler 
| writer         =
| screenplay     = Tomislav Radić 
| story          =
| based on       =  
| starring       =  
* Božidarka Frajt
* Ružica Rošoci
* Vesna Veselić
* Zlatica Dubravčić
* Zdenka Livajsić
* Inge Grandić
* Vjeran Zuppa
* Mira Wolf
* Astrid Turina
* Joško Marušić
* Eva Ras 
 
| narrator       =
| music          = Arsen Dedić 
| cinematography = Dragutin Novak 
| editing        = Maja Filjak-Bilandžija 
| studio         = Radiotelevision Zagreb Film Authors Studio 
| distributor    =
| released       =  
| runtime        = 77 minutes 
| country        = Yugoslavia
| language       =
| budget         =
| gross          =  
}}
 Croatian film directed and written by Tomislav Radić, starring Božidarka Frajt.

Radićs directorial debut, a very low-budget cinéma vérité-style film about the everyday life of a struggling actress was met with critical acclaim and is considered one of the best Croatian films of the 1970s.  It started the career of Božidarka Frajt, whose performance won the Golden Arena for Best Actress at the 1972 Pula Film Festival.

==Plot==
Božidarka Frajt (playing herself ) is a stage actress who is unsuccessfully looking for a job in Zagreb. The film follows her everyday life: she meets her friends and colleagues, goes to parties, remembers her difficult childhood as a war orphan, and contemplates her professional and personal failures.   

==Background and production==
Before his feature film debut, Tomislav Radić, an accomplished theatre director, had gained some filmmaking experience while shooting documentaries about people with unusual occupations for Radiotelevision Zagreb. Radić had an idea to make a cinéma vérité-style film based on the life and personality of Božidarka Frajt, then a 30-year-old actress who, despite holding an acting diploma, was unable to find permanent employment, being constrained to minor film roles and occasional modeling work.  Instead of making a documentary about her, he would make a feature film about a woman very much like her, and the other participants would play themselves, likewise appearing under their own names.   
 16 mm 35 mm Framing and camera movement were not planned in advance; instead, the decisions were made on the spot, in agreement between the cinematographer Dragutin Novak and the director. 

==Analysis==
  pictured).  ]]
Croatian film critic Nenad Polimac argued that the film was not actually meant to be cinéma vérité, and could not be properly described as such. According to him, the very idea of actors playing themselves was radically different from anything that could be seen in the cinema of the era. Also, instead of preserving the continuity of the footage, Radić broke the scenes into short fragments that were interspersed throughout the film, creating a more dynamic slice of life|slice-of-life feel and structure.   The editing was sometimes deliberately crude, with abrupt cuts. The "dirty" aesthetics of the film was thus a mix of extreme realism and extreme artificiality. 

==Reception and legacy== naturalist character came as a shock to many contemporary viewers,  but most critics received it very positively.  Still, the political establishment was disapproving: The Living Truth was characterized as an "unrepresentative depiction of the social scene" and received the permit for public screening only after a lengthy delay. 

The film was shown in the out-of-competition section of the 1972 Pula Film Festival, which caused protests from the critics.  In the end, the festival jury – presided by Stipe Šuvar – made an unprecedented decision to award Golden Arena for Best Actress to Božidarka Frajt, contrary to the festivals regulations.    Frajts highly dynamic, dominant personality offset the otherwise depressing setting of the film, and her performance instantly made her one the leading actresses of Croatian and Yugoslav cinema.  

In a 1999 poll among 44 Croatian film critics and film historians, The Living Truth placed 20th in the list of all-time best Croatian films. 

The film was photochemically restored in 1998, digitally restored in 2009, and released on DVD in 2011. 

==References==
{{Reflist|refs=

 {{cite web
| url=http://filmovi.hr/index.php?p=article&id=1426 
| title=I danas svježe ostvarenje
| work=filmovi.hr
| first=Janko
| last=Heidl
| date=28 February 2011
| language=Croatian
| accessdate=2014-12-03
}} 

 {{cite web
| url=http://www.filmski-programi.hr/baza_film.php?id=67
| title=Živa istina
| work=filmski-programi.hr
| publisher=Croatian Film Association
| language=Croatian
| accessdate=2014-12-01
}} 

 {{cite journal
| url=http://www.hfs.hr/nakladnistvo_zapis_detail.aspx?sif_clanci=32698#.VHxPPclNcXQ
| title=Film koji je probudio hrvatsku kinematografiju
| last=Polimac
| first=Nenad
| authorlink=Nenad Polimac
| journal=Zapis
| issue=68
| year=2010
| publisher=Croatian Film Association
| language=Croatian
| accessdate=2014-12-01
}} 

 {{cite encyclopedia
| url=http://film.lzmk.hr/clanak.aspx?id=2121
| title=Živa istina
| work=Filmski leksikon
| publisher=Miroslav Krleža Institute of Lexicography
| year=2008
| location=Zagreb
| last=Peterlić
| first=Ante
| authorlink=Ante Peterlić
| language=Croatian
| accessdate=2014-12-01
}} 

 {{cite news 
| url = http://arhiv.slobodnadalmacija.hr/19991128/kultura.htm 
| language = Croatian | newspaper = Slobodna Dalmacija 
| title = "Tko pjeva, zlo ne misli" najbolji hrvatski film svih vremena!
| date = 28 November 1999
| accessdate = 2013-02-08
}} 

}}

==Further reading==
* {{cite journal
| url = https://www.repozytorium.amu.edu.pl/jspui/bitstream/10593/11546/1/Gilic.pdf
| title = Rane 1970-e i filmski slučaj Tomislava Radića
| trans_title = Early 1970s and the Cinematic Case of Tomislav Radić
| last = Gilić
| first = Nikica
| journal = Poznańskie Studia Slawistyczne
| issue = 6
| year = 2014
| pages = 91–103
| language = Croatian
| format = PDF
| accessdate = 14 December 2014
}}

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 