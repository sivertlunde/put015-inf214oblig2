Echoes of the Rainbow
 
 
{{Infobox film
| name = Echoes of the Rainbow
| image = EchoesOfTheRainbowPoster.jpg
| border = yes
| film name = {{Film name| traditional = 歲月神偷
| simplified  = 岁月神偷
| pinyin = Suìyuè Shéntōu
| jyutping = Seoi3 Jyut6 San4 Tau1}}
| director = Alex Law Kai-Yui
| producer = Mabel Cheung Yuen-Ting
| writer =
| starring = Simon Yam Sandra Ng Aarif Lee
| studio =
| distributor = Mei Ah Entertainment
| released =  
| runtime =
| country = Hong Kong
| language = Cantonese
| budget =
| gross = HK$23,111,759
}} 2010 Berlin Film Festival. 

It tells the story of a working family in Hong Kong whose eldest son, a popular boy and star athlete, becomes ill with leukemia.

The film is set in 1960s British Hong Kong and was shot on the historical Wing Lee Street in Sheung Wan. It was financed by the Hong Kong governments Film Development Fund. 

==Plot== late 1960s Hong Kong. Chun-yi looks up to his brother, who is a champion runner at the local high school, earns top grades, is a talented musician, and is very popular; Chun-yis parents and teachers often scold him for not being as good as his big brother. Desmond has a girlfriend, Flora, who comes from a wealthy family, and whose family moves away to the United States to escape the "lawless" situation in Hong Kong. Desmond gradually becomes aware of the inequalities in Hong Kong society of that time, both through interactions with a cocky British policeman who extorts money from his father in exchange for letting them keep their shop (and who insists that Desmond will never "make it big" because his English isnt good enough) and through his first time seeing the inside of Floras familys mansion. Most of this is told through Chun-yi, the films narrator.

Not long before Flora leaves, Desmonds grades start declining and he takes third place in a race he had hoped to win. One day he collapses suddenly after a typhoon nearly destroys the familys house, and his father takes him to the hospital, where he is diagnosed with leukemia. His parents search for a doctor who can cure him but find none, and his condition worsens until he has to remain in the hospital, where the nurses treat him poorly and extort money from his parents in exchange for basic care. Chun-yi, who spends much of his free time stealing trinkets around town, tries to offer them all to his brother in attempt to cheer him up, but Desmond doesnt accept any of them. Flora returns from the United States and visits him in the hospital, where they share their first kiss, but Desmond dies shortly thereafter. Years later Chun-yi, narrating retrospectively, remarks that "time is the greatest thief". In the films final scene, Chun-yi and his mother are shown visiting Desmonds grave, and Chun-yi, now a teenager, recounts a lesson Desmond had taught him about double rainbows.

==Cast==
* Simon Yam as Mr. Law
* Sandra Ng as Mrs. Law
* Aarif Lee as Desmond Law (aka Law Chun-yat)
* Buzz Chung as Law Chun-yi
* Evelyn Choi as Flora Lau (aka Lau Fong-fei) Paul Chun as Big Uncle/barber
* Lawrence Ah Mon as Goldfish Seller
* Jean-Michel Sourd as French teacher

==Reception==
 , where filming took place]]
China Daily placed the film on their list of the best ten Chinese films of 2010. 

Wing Lee Street, where Echoes of the Rainbow was shot, was originally inclued in a redevelopment plan. However, the Berlinale award won by the film stimulated the wish of Hong Kong people to preserve this heritage site and community. The Town Planning Board eventually decided to keep all the tong laus on the street and to designate the region as a preservation site. 

==Awards and nominations==
* 29th Hong Kong Film Awards
** Won: Best Screenplay (Alex Law)
** Won: Best Actor (Simon Yam)
** Nominated: Best Actress (Sandra Ng)
** Won: Best New Performer (Aarif Lee)
** Nominated: Best New Performer (Chung Shiu To)
** Won: Best Original Film Song (Lowell Lo, Alex Law and Aarif Lee)
* 60th Berlin International Film Festival
** Crystal Bear for the Best Film in the Children’s Jury "Generation Kplus"

==See also==
* List of submissions to the 83rd Academy Awards for Best Foreign Language Film
* List of Hong Kong submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 