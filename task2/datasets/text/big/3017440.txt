Radioland Murders
{{Infobox film
| name           = Radioland Murders
| image          = Radiolandposter.jpg
| caption        = Theatrical release poster
| director       = Mel Smith
| producer       = George Lucas Rick McCallum Fred Roos
| screenplay     = Willard Huyck Gloria Katz Jeff Reno Ron Osborn
| story          = George Lucas Michael Lerner Ned Beatty
| music          = Joel McNeely
| cinematography = David Tattersall
| editing        = Paul Trejo
| studio         = Lucasfilm Universal Pictures
| released       =  
| runtime        = 112 minutes
| country        = United States
| language       = English
| budget         = $15 million
| gross          = $1,316,865
}} 1994 comedy homage to Michael Lerner and Ned Beatty. Radioland Murders also features numerous small roles and cameo appearances, including Michael McKean, Bobcat Goldthwait, Jeffrey Tambor, Christopher Lloyd, George Burns (in his final film appearance), Billy Barty and Rosemary Clooney.
 Universal Pictures bombed at the box office, only grossing $1.37 million in US totals.

==Plot== General Walt page boy conductor Rick Rochester, announcer Dexter Morris, director Walt Whalen, Jr. and stage manager Herman Katzenback. After King commissions rewrites on the radio scripts, the WBN writers get angry, adding to the fact that they have not been paid in weeks.
 rat poisoning, a series of events ensue. Director Walt Jr. is hanged (the mysterious killer makes it look like a suicide), and his father, the General, has the Chicago Police Department (CPD) get involved to solve the murder mysteries as the nightly radio performance continues. Herman Katzenback is then killed after attempting to fix the main stage when the machinery malfunctions. Penny is appointed both stage manager and director due to Walt Jr. and Katzenbacks deaths. Writer Roger Henderson tries to solve the killings, much to the annoyance of the police, led by Lieutenant Cross.
 FCC scandal. King (laughing gas) and General Whalen (falls down an elevator shaft) are the next to die after Rogers warning, causing even more suspicion from the police.

After escaping from custody, Roger uses Billy to communicate and send scripts to Penny. When rewriting one of the programs, Gork: Son of Fire, Roger attempts to write the script with self-reference|self-referential events, proving to everyone that the mysterious killer is actually sound engineer Max Applewhite. Max explains that his killings were a revenge scheme that dealt with stock holders and patents, specifically detailing his invention of television, which other scientists have copied. Roger and Penny are taken by Max atop the radio tower at gunpoint. Max is eventually killed when a biplane shows up and guns him down. Impressed by the nightly performance, the sponsors decide to fund WBNs career in broadcasting. Roger and Penny reconcile their complex relationship and decide not to divorce.

==Cast==
*Brian Benben as Roger Henderson: Ecstatic writer of WBN and husband to Penny. Much to the consternation of the police force, Roger solves the murder mystery.
*Mary Stuart Masterson as Penny Henderson: Stressed WBN secretary who is promoted to both director and stage manager after the deaths of Walt Jr. and Herman Katzenback. She initially intends to divorce Roger after catching him in an affair with Claudette Katzenback, but they later reconcile their relationship.
*Scott Michael Campbell as Billy Budget: WBN page boy who is used by Roger to communicate with Penny and send scripts, despite the fact that he is trying to hide from the police. Michael Lerner as Lieutenant Cross: Short-tempered policeman who has a vendetta against Roger.
*Ned Beatty as General Walt Whalen: Owner of WBN who commands his staff with a military-like work environment. The General dies after falling down an elevator shaft.
*Brion James as Bernie King: WBNs main sponsor who has no sense of humor. King eventually dies from laughing gas.
*Stephen Tobolowsky as Max Applewhite: WBNs sound engineer who is found to be responsible for the murders. Max dies after getting shot atop the radio tower.
*Michael McKean as Rick Rochester: WBN band conductor who despises Dexter (McKean would later appear as Benbens boss in Dream On).
*Corbin Bernsen as Dexter Morris: The stations announcer who has a smoking habit. Dexter dies of electrocution, ignoring Roger and Billys warning.
*Bobcat Goldthwait as Wild Writer: Violent and melancholic WBN writer.
*Anita Morris as Claudette Katzenback: Famous singer and Hermans wife. Penny catches her with Roger, presumably having sex, but this appears to have been a prank Claudette created. Roger originally believes she was responsible for killings. This was Anita Morris final role, as she died seven months before the films release.
* -wearing son and show director. Larry Miller as Herman Katzenback: German stage manager of WBN. Herman is the third to die and is aware of Claudettes multiple affairs with other employees.
* er.
*Harvey Korman as Jules Cogley: Alcoholic writer who confirms that Ruffles death came from poisoning.
*Dylan Baker as Detective Jasper: Cross idiot assistant.
*Jack Sheldon as Ruffles Reedy: Drunk trumpet player of Rochesters band. He is the first to die.

Cameo appearances are provided by George Burns (in his final feature film, as Milt Lackey, a 100-year-old comedian), Joey Lawrence, Peter MacNicol, Robert Klein, Ellen Albertini Dow, Candy Clark and Bo Hopkins (as Billy Budgets parents), as well as singers Billy Barty, Rosemary Clooney and Tracy Byrd.

==Production==
The genesis of Radioland Murders came from  ).    

Lucas eventually negotiated a deal to produce Radioland Murders for Universal shortly after the successful release of American Graffiti in late 1973. Willard Huyck and Gloria Katz prepared a rough draft based on Lucass 1974 film treatment, and Universal was confident enough to announce pre-production soon after. Lucas was set to direct with Gary Kurtz producing. In the original Huyck/Katz script, Roger and Penny were not a married couple seeking divorce, but were boyfriend and girlfriend with a love-hate relationship.  Their script also included the controversy over the invention of radio.  
 digital mattes, would help bring Radioland Murders in for a relatively low budget of about $10 million,  which eventually rose to $15 million.  
 dark humor.  Universal was adamant that the ensemble cast be filled with then-popular TV stars of the early 1990s.  Christopher Lloyd agreed to make a small appearance as the eccentric sound designer Zoltan on the agreement that all of his scenes were shot in one day. 

 ,  ) at Industrial Light & Magic. Following a break, in which Lucas, director Mel Smith and editor Paul Trejo reviewed the footage using the new digital Avid Technology editing system (the successor to EditDroid), the cast and crew were reassembled for a further two weeks of filming. Hearn, pp. 180-182  Principal photography for Radioland Murders ended on December 23, 1993. 

==Release== The Flintstones bombed at the biggest second weekend drop at the box office, dropping 78.5% from $835,570 to $179,315. 

===Critical reception===
Radioland Murders received negative reviews with Rotten Tomatoes calculated a 20% approval rating based on 15 reviews collected.  Roger Ebert criticized the film for containing too much slapstick comedy instead of subtle humor. Although he praised the art direction and visual effects, Ebert believed "the movie just doesnt work. Its all action and no character, all situation and no comedy. The slapstick starts so soon and lasts so long that we dont have an opportunity to meet or care about the characters in a way that would make their actions funny."  Richard Schickel, writing in Time (magazine)|Time magazine gave a mixed review, mainly criticizing the film for its fast pacing.  Caryn James of The New York Times dismissed the film for trying too hard to pay homage to screwball comedy films of the 1930s.  
 On the Air. This film has more gags in it than anything this side of a Zucker, Abrahams and Zucker production, too few of which work." 

===Home media=== Region 1 DVD release came in March 1998 by Image Entertainment.  Universal Studios Home Entertainment re-released the film on DVD in August 2006. 

==References==
 

==External links==
*   at  
*  
*  
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 