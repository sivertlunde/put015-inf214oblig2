The Boy Who Owned a Melephant
"The American short film short directed by Saul Swimmer and featuring Tallulah Bankhead as narrator.

==Plot==
After seeing his first circus, young Johnnie (Brockman Seawell) asks for an elephant to keep as a pet. To placate him, his mother (Molly Turner) whimsically "gives" him the elephant in the local zoo. The boys classmates resent his pride in "owning" the pachyderm, and the boy learns to share, making his peers equal "owners".    
==Production== Tony Anthony, Palace Theatre in New York City. 

The film screened at the 1959  San Francisco International Film Festival,  and won a 1959 Gold Leaf award at the Venice International Childrens Film Festival.  On March 19, 1967, it was paired with the 1952 French short "White Mane" as an episode of the television anthology series The CBS Childrens Film Festival.   (fan site).   from the original on April 24, 2012. 

==References==
 
==External links==
* 

 
 
 
 