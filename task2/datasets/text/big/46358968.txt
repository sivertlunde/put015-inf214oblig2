The Gallows
 
{{Infobox film
| name           = The Gallows
| image          = 
| alt            = 
| caption        =
| director       = Travis Cluff Chris Lofing 
| producer       = Jason Blum Travis Cluff Benjamin Forkner Chris Lofing Dean Schnider 
| writer         = Travis Cluff Chris Lofing 
| starring       = Cassidy Gifford Pfeifer Brown Ryan Shoos Reese Mishler Alexis Schneider Price T. Morgan
| music          = Zach Lemmon
| cinematography = Edd Lukas 
| editing        = Chris Lofing 
| studio         = New Line Cinema Blumhouse Productions Management 360 Tremendum Pictures
| distributor    = Warner Bros. Pictures
| released       =   
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 found footage horror film written and directed by Travis Cluff and Chris Lofing. The film stars Cassidy Gifford, Pfeifer Brown, Ryan Shoos, Reese Mishler, Alexis Schneider and Price T. Morgan. The film is scheduled to be released by Warner Bros. on July 10, 2015.

== Plot ==
20 years after a horrific accident during a small town school play, students at the school resurrect the failed show in a misguided attempt to honor the anniversary of the tragedy - but soon discover that some things are better left alone. 

== Cast ==
*Cassidy Gifford as Cassidy
*Pfeifer Brown as Pfeifer
*Ryan Shoos as Ryan
*Reese Mishler as Reese
*Alexis Schneider
*Price T. Morgan
*Mackie Burt 	

==Production== Warner Bros. on April 16, 2015.

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 