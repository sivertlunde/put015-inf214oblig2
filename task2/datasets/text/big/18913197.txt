Tonari no 801-chan
{{Infobox animanga/Header
| name            = 
| image           =  
| caption         = Tonari no 801-chan manga volume 1 cover.
| ja_kanji        = となりの801ちゃん
| ja_romaji       = Tonari no Yaoi-chan Slice of life
}}
{{Infobox animanga/Print
| type            = manga
| author          = Ajiko Kojima
| publisher       = Ohzora Publishing
| demographic     = Josei manga|Josei
| magazine        = Romance Tiara
| first           = April 18, 2006
| last            = 
| volumes         = 3
| volume_list     = 
}}
{{Infobox animanga/Video
| type            = live film
| director        = Kōtarō Terauchi
| producer        = 
| writer          = 
| music           = 
| studio          = 
| released        = September 5, 2007
| runtime         = 60 minutes
}}
{{Infobox animanga/Print
| type            = manga
| title           = Tonari no 801-chan: Fujoshiteki Kōkō Seikatsu
| author          = Jin
| publisher       = Kodansha
| demographic     = Shōjo manga|Shōjo
| magazine        = Bessatsu Friend
| first           = November 28, 2007
| last            = April 13, 2009
| volumes         = 3
| volume_list     = 
}}
{{Infobox animanga/Print
| type            = manga
| title           = 801-shiki Chūgakusei Nikki: Tonari no Hina-chan
| author          = Jun Minamikata
| publisher       = Ohzora Publishing
| demographic     = Josei manga|Josei
| magazine        = Romance Tiara
| first           = April 11, 2009
| last            = 
| volumes         = 
| volume_list     = 
}}
 
 Internet manga bound volume in December 2006; as of July 2008, three volumes have been published. By June 2007, the first volume had sold 150,000 copies.  The manga began serialization from the first chapter in Ohzoras Romance Tiara magazine in April 2009.
 drama CD was released in April 2008, and another followed in October 2008. An anime adaptation by Kyoto Animation was announced to air in Japan in 2009,  but was unexpectedly canceled. 

==Origin== Kamo eggplant, Japanese as ya-o-i (yaoi), the mascot soon became an Internet phenomenon. This brought the mascot to the attention of Ajiko Kojima, a Japanese blogger who took the mascot and slightly altered its appearance, using it as a basis for use his new webcomic Tonari no 801-chan in April 2006.

==Plot and characters== narrated by  , otherwise known as  , a twenty-eight-year-old company employee and boyfriend to Yaoi, otherwise known as  . Yaoi, who also works in a company, is a twenty-two-year-old fujoshi, a female otaku who is a fan of anime and manga series featuring yaoi, or romantic relationships between men. Tibet, who is an otaku himself, initially met Yaoi over the Internet. When she obsesses over yaoi, a small green furry monster comes out of a zipper from her back as the manifestation of her obsession.
 3D aspect diet so as to become a cuter girl. All goes according to plan, until a scheming girl going to the same school named   makes it harder for Rei to get closer to Kei.

==Media==

===Manga=== Internet manga bound volume of the manga on December 14, 2006 under their Next comics imprint (trade name)|imprint, and as of July 17, 2008, three volumes have been published. Included in the volumes are additional manga strips not featured on his blog. The first two volumes have sold over 320,000 copies in Japan.  The manga began serialization from the first chapter in Ohzoras josei manga magazine Romance Tiara on April 11, 2009.   
 manga magazine Bessatsu Friend on November 28, 2007.  The first volume was released on March 13, 2008, and the second followed on July 11, 2008; the volumes are published under Kodanshas KC Deluxe imprint.

A Spin-off (media)|spin-off manga series entitled   written and illustrated by Jun Minamikata started serialization in Romance Tiara on April 11, 2009. 

===Film===
A live action straight-to-DVD sixty-minute film directed by Kōtarō Terauchi was produced on September 5, 2007. The film stars Sō Hirosawa as Yaoi, and Koji Seto as Tibet, and also features Kotaro Yanagi playing a role. Ajiko Kojima, the original manga author, has a small cameo appearance in the film.

===Drama CDs=== drama CD based on the original manga was released by Marine Entertainment in Japan on April 23, 2008, and the first-print version of the CD came bundled with a mini hand towel.  s drama CD official website|publisher=Marine Entertainment| accessdate=2008-08-18|language=Japanese}}  Another drama CD was released by Marine Entertainment on October 22, 2008, and the first-print version came bundled with a mousepad sticker, and an extra track featuring a group talk of the drama cast. 

===Anime=== TBS to begin airing in Japan in 2009,  s Kyoto Animation to Produce Tonari no 801-chan|publisher=Anime News Network|date=2008-08-17|accessdate=2008-08-18}}  but was unexpectedly canceled.    However, a 90-second "opening anime" called   was produced by A-1 Pictures and directed by Yutaka Yamamoto; it was bundled with the fourth volume of the original web manga released on September 10, 2009. 

==References==
 

==External links==
*   
*  at Ohzora Publishing  
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 