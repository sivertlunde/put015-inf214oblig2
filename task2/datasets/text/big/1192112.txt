The Cockleshell Heroes
 
{{Infobox film 
| name = The Cockleshell Heroes
| image =The Cockleshell Heroes.jpg
| caption = US cinema poster
| director = José Ferrer    
| producer = Phil C. Samuel   
| story  = George Kent
| screenplay = Bryan Forbes Richard Maibaum
| starring =José Ferrer Trevor Howard    
| music = John Addison
| cinematography = John Wilcox
| editing = Alan Osbiston
| studio = Warwick Films
| distributor = Columbia Pictures
| released =  
| runtime = 97 minutes 
| country = United Kingdom
| language = English
| budget =
}} David Lodge Second World War, it is a fictionalised account of Operation Frankton, the December 1942 raid by canoe-borne British commandos on shipping in Bordeaux Harbour. It was the first Warwick Film to be filmed in CinemaScope.

==Plot==

 
 Major Stringer collapsible canoes, commandos to reach an enemy-held harbour undetected and blow up ships with limpet mines. He is given command of a small group of volunteers. 
 Captain Hugh Thompson (Trevor Howard). The two officers represent the clash of cultures in the Royal Marines in World War II and postwar. Stringer is the enthusiastic promoter of commando operations requiring daring and initiative, but has no experience leading men or operations. Thompson represents the old guard of traditional ships detachments. Sergeant Craig (Victor Maddern) trains the men following Stringers directions, but Thompson strongly disapproves of his commanders lax methods. When a test mission ends disastrously, Stringer admits his mistake and turns to Thompson, who soon whips the marines into shape.
 David Lodge), AWL due to marital problems. Thompson gets to Ruddocks wife first and finds her with her civilian lover, but leaves when they both insult him. He goes to the local pub for a drink and finds the missing Marine. Thompson gives Ruddock enough time to beat up his wifes paramour, then drives him back to camp. 
 German patrol boat knocks out Ruddocks partner.  Thompson, who was not supposed to go on the raid, volunteers to take his place.  The raiders then disembark and begin their attack.  They face seventy miles of paddling upriver in their Cockle Mk II Folding kayak|canoes.  After moving by night and hiding by day, only four men reach the target, where they plant limpet mines on a number of ships.

The raid is successful, but only Stringer and one marine manage to escape. The rest are captured. When Thompson and the other prisoners refuse to divulge what their mission was, they are shot by firing squad, but not before hearing the mines explode.

==Main cast==
 
*José Ferrer as Major Stringer RM
*Trevor Howard as Captain Hugh Thompson RM
*Dora Bryan as Myrtie
*Victor Maddern as Sergeant Craig RM
*Anthony Newley as Marine Clarke David Lodge as Marine Ruddock
*Peter Arne as Corporal Stevens RM Percy Herbert as Marine Lomas
*Graham Stewart as Marine Booth
*John Fabian as Marine Cooney
*John Van Eyssen as Marine Bradley
*Robert Desmond as Marine Todd
*Walter Fitzgerald as the Gestapo commandant
*Karel Stepanek as Assistant Gestapo Officer
*Beatrice Campbell as Mrs. Ruddock
*Sydney Tafler as Policeman
*Gladys Henson as Barmaid
*Jacques B. Brunius as French Fisherman
*Andreas Malandrinos as French Fisherman WVS driver
*Christopher Lee as Submarine Commander Alan Grieves
*Patric Doonan as Sailor Claridge
* Sam Kydd as fish lorry driver
 

==Production notes==
José Ferrer had Bryan Forbess script rewritten by Richard Maibaum,  but Irving Allen decided Maibaums script didnt have enough comedy, so he had Forbes rewrite Maibaums revision and direct some sequences without telling Ferrer. When Ferrer found out, he left the film.  
 Eastney Barracks in Southsea, Hampshire. Eastney Barracks is now the home of the Royal Marines Museum. The Royal Navy ships,   and HMS Leeds Castle (K384)|Leeds Castle (K384), were used to portray a German anti-submarine vessel dropping depth charges. Studio scenes were shot at Shepperton. "These Are the Facts", Kinematograph Weekly, 31 May 1956 p 14 
 Lieutenant Colonel Herbert "Blondie" HaslerRM, the leader of the real-life raid, was seconded to Warwick Films as technical advisor.  Ex-Corporal Bill Sparks, the other survivor of the raid, was also an advisor.

The film briefly uses several railway locations including the level crossing at Fort Brockhurst on the (by then goods only) Gosport branch in Hampshire. As he cycles south, José Ferrer has to wait for a passing train (a van hauled by T9 class locomotive 30729) so takes the opportunity to abandon his bicycle in favour of a ride in the rear of a fish lorry. Later Ferrer steals the fish lorry only to abandon it at Shepperton Station (Surrey) in order to catch a just-departing train.

In another sequence David Lodge ducks out of sight into a WC alongside the North Woolwich Branch. This was possibly at the footbridge opposite Fernhill Street on Albert Road, west of North Woolwich station. David Lodge is also filmed running over the road bridge adjacent to Chertsey station where a Southern electric train can be seen.

Trevor Howard and David Lodge nearly drowned while filming a sequence in a canoe and the canoe overturned. 

==Reception==
The film was one of the ten most popular movies at the British box office in 1956. BRITISH. FILMS MADE MOST MONEY: BOX-OFFICE SURVEY
The Manchester Guardian (1901-1959)   28 Dec 1956: 3 
==References==
 

==External links==
*  
*  
*  
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 