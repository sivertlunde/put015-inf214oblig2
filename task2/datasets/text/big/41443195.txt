Three Bites of the Apple
{{Infobox film
 | name = Three Bites of the Apple
 | image = Three Bites of the Apple.jpg
 | caption = 
 | director = Alvin Ganzer George Wells 
 | starring =  
 | music =  Eddy Lawrence Manson 
 | cinematography = Gábor Pogány
 | editing = 
 | language =  English 
}}

Three Bites of the Apple is a 1967 American Romance film|romance-comedy film directed by Alvin Ganzer.  

In this lightweight comedy, David McCallum stars as Stanley Thrumm, a retiring British tour guide who strikes it rich one night in a casino on the Riviera. Hes not sure that he wants to take the cash back to England, because hell have to pay taxes on it, so he decides to put it in a Swiss bank account. But Carla Moretti (Sylva Koscina), an apparently helpful woman whom he has met, has designs on the loot, and she enlists her ex-husband in an effort to get it. Thrumm takes his winnings on a roundabout trek to Switzerland while Carla and her husband pursue, and the result is a long car chase with many comic diversions and a lot of Alpine scenery. ~ Michael Betzold, Rovi (from MTV.com)

There also is a soundtrack album where David McCallum sings the theme song over the opening credits.

== Cast ==

* David McCallum: Stanley Thrumm
* Sylva Koscina: Carla Moretti 
* Tammy Grimes: Angela Sparrow 
* Harvey Korman: Harvey Tomlinson 
* Domenico Modugno: Remo Romano
* Aldo Fabrizi: Dr. Manzoni
* Mirella Maravidi: Francesca Bianchini Riccardo Garrone: Croupier
* Avril Angers: Gladys Tomlinson
* Claude Aliotti: Teddy Farnum  
* Freda Bamford: Gussie Hagstrom
* Arthur Hewlett: Alfred Guffy
* Alison Frazer: Peg Farnum
* Cardew Robinson: Bernhard Hagstrom
* Ann Lancaster: Winnifred Batterf

==References==
 

==External links==
* 

 
 
 
 


 