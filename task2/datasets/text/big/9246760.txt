Everything Will Be OK
 

{{Infobox film
| name           = Everything Will Be OK
| image          = EverythingWillBeOKFilm.png
| image size     = 190px
| alt            = DVD cover of Everything Will Be OK, copyright 2006 Bitter Films
| caption        = DVD cover of the film
| director       = Don Hertzfeldt
| producer       = Don Hertzfeldt
| writer         = Don Hertzfeldt
| screenplay     = 
| story          = 
| based on       = "Temporary Anesthetics"
(webcomic by Don Hertzfeldt)  
| narrator       = Don Hertzfeldt
| starring       =
| music          = 
| cinematography = 
| editing        = Brian Hamblin
| studio         = Bitter Films
| distributor    = 
| released       =  
| runtime        = 17 minutes
| country        = United States
| language       = English
}}

Everything Will Be OK is a 2006 animated short film by Don Hertzfeldt. It is the first chapter of a three-part story about a man named Bill. Hertzfeldt released the second film in the series, titled I Am So Proud of You, in 2008.  The final chapter, "Its Such a Beautiful Day," was released in 2011.  The entire three-part story was then edited together and released as a seamless feature film in 2012, also titled  "Its Such a Beautiful Day (film)|Its Such a Beautiful Day."

Everything Will Be OK won the 2007 Sundance Film Festival Jury Award in Short Filmmaking, a prize rarely bestowed on an animated film. To date it has won 40 awards, including the Grand Jury Award for Best Film at the London International Animation Festival and the Lawrence Kasdan Award for Narrative Film from the Ann Arbor Film Festival. 

Despite the films short running length, Variety film critic Robert Koehler named Everything Will Be OK one of the "Best Films of 2007".  The film was extremely well received by critics, describing it as "essential viewing" and, "simply one of the finest shorts produced over the past few years, be it animated or not."   The Boston Globe called the film a "masterpiece" with the Boston Phoenix declaring Hertzfeldt a "genius."  The short film was a cover story on the Chicago Reader, receiving four stars from critic J.R. Jones.

Outside of theaters, the movie was released as a limited edition DVD "single" in 2007, exclusively from Hertzfeldts website, http://www.bitterfilms.com. The DVD featured an extensive archival area of deleted scenes, Dons production notes, sketches, and layouts, as well as a hidden Easter Egg that plays an alternate, narration-free version of the film to highlight the sound design. The movie was subsequently released on the "Don Hertzfeldt Volume Two" DVD anthology in 2012.

==Synopsis==

The main character and a few of the films scenarios were developed from sketches and webcomics Hertzfeldt did in 1999-2000 for his website while working on other film projects. The film tells the story of Bill, whose daily routines, perceptions, and dreams are illustrated onscreen via multiple split-screen windows. Though his life seems mundane at first, he has trouble remembering things and the sights and sounds of the world are sometimes overwhelming. His life is narrated in several humorous and dramatic anecdotes, very gradually growing darker and terrifying as he appears to be suffering from a possibly fatal mental disorder (and has apparently been receiving treatment for it for quite some time). The split-screen windows and layered audio tracks slowly battle for attention on screen and begin to smother Bill. The film illustrates the struggle many people have with mental illness, in the inherent difficulty in deciding which muddled thought or distracted perception is most important and should be paid attention to. Meanwhile, Bill is suddenly faced with the possible end of his life.

==Style==

The story is told with Don Hertzfeldts signature style of animated stick-figure line drawings. However, in this film, there are often multiple frames on the screen at once, each in an irregular white enclosure, all against a background of pure black. Some animated still photographs are also incorporated inside certain windows, as well as a handful of the colorful special effects and experimental film techniques that Hertzfeldt first utilized in his 2005 film, The Meaning of Life (animated film)|The Meaning of Life. As with many of his films, no computers were used in creating the picture and all of the split-screen effects were captured in-camera on his 35mm animation camera through careful multiple-exposure photography.

There is no dialogue spoken directly by the characters (with the exception of some of Bills hallucinations). The film and Bills thoughts are entirely narrated in great detail. The deadpan narrator refers to Bill only in the third person and describes what all the characters say.

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 