Roman Polanski: Wanted and Desired
 
 

{{Infobox film
| name           = Roman Polanski: Wanted and Desired
| image          = Roman polanski wanted and desired.jpg
| caption        = Promotional poster
| director       = Marina Zenovich
| producer       = Jeff Levy-Hinte Lila Yacoub Marina Zenovich P. G. Morgan Michelle Sullivan
| writer         = Marina Zenovich Joe Bini P. G. Morgan
| starring       = Samantha Geimer Roman Polanski David Wells Sharon Tate
| cinematography = Tanja Koop
| editing        = Joe Bini
| music          = Mark Degli Antoni HBO
| released       =  
| runtime        = 99 minutes
| country        = United States United Kingdom
| language       = English French
| budget         =
| gross          =
}}
 his sexual abuse case. It examines the events that led to Polanski fleeing the United States after being embroiled in a controversial trial, and his unstable reunion with his adopted country. A follow-up to the film, also directed by Zenovich, titled Roman Polanski: Odd Man Out was released on 26 March 2013, detailing Polanskis successful legal battle to avoid extradition to the US, a battle that took place after Roman Polanski: Wanted and Desired came out.     

==Reviews==
  scores the film at 89% positive reviews based on 46 reviews. 
 Polanski himself, and the footage is from a 16-minute short called The Fat and the Lean that he made in 1961, on the brink of his fame as a brilliant new European director. The wordless scene may last less than a minute in Marina Zenovichs documentary, but it sticks with you, and it echoes another clip in her film. This one is from a television interview Polanski did decades later where he says he felt like a mouse with which an abominable cat was making sport. The cat in question was Los Angeles Judge Laurence J. Rittenband, whod presided over the directors 1977 criminal case for having sex with a 13-year-old girl." {{cite web
|first=Cathleen
|last=McGuigan
|title=Hollywood’s Most Beloved Fugitive
|url=http://www.newsweek.com/id/138382
|publisher=Newsweek
|date=2 June 2008
|accessdate=2009-09-30
}} 

One negative review by Bill Wyman, writing for   using the phone in a scene from Rosemarys Baby (film)|Rosemarys Baby — that, too, about a horrifically abused woman. But the scene isnt used to illustrate the victims story -- its about poor Roman. Hes the person making the desperate phone call. Its an odd juxtaposition when you think about it." {{cite web
|first=Bill
|last=Wyman
|title=Whitewashing Roman Polanski
|url=http://www.salon.com/2009/02/19/roman_polanski_documentary/
|publisher=Salon.com
|date=2 February 2009
|accessdate=2009-09-29
}} 

Nathan Southern of Allmovie writes that, "filmmaker Marina Zenovich revisits this difficult case via extensive interviews with Geimer  , defense attorney Douglas Dalton, Assistant DA Roger Gunson, and others. In the process, she raises pivotal questions about the U.S. legal system and the fairness of the judge, Laurence J. Rittenband (who was reportedly extremely vocal about his desire to topple Polanski) and encounters many recollections of judicial malfeasance from those who were involved." {{cite web
|first=Nathan
|last=Southern
|title=Roman Polanski: Wanted and Desired
|url=http://www.allmovie.com/work/426678
|publisher=Allmovie
|accessdate=2009-09-30
}} 

==Awards==
Roman Polanski: Wanted and Desired was nominated for five primetime   awarded it the title of one of the top 5 documentaries of 2008.  It won the "Editing Award Documentary" at the Sundance Film Festival  It was also listed as one of ten films in the "Hall of Shame" of the 2008 Women Film Critics Circle Awards. 

==Retraction by David Wells==
After the arrest of Roman Polanski in Switzerland in 2009, David Wells, now a retired deputy district attorney, recanted the interview he gave the director about advising the judge in the case in ex parte communication. According to Marcia Clarke of The Daily Beast: 
:"I lied, Wells told me yesterday, referring to his comments in the movie that he told the judge how he could renege on a plea-bargain agreement and send Polanski back to jail after he had been released from a 42-day psychiatric evaluation—the heart of Polanski’s claims of prosecutorial and judicial misconduct. I know I shouldn’t have done it, but I did. The director of the documentary told me it would never air in the States. I thought it made a better story if I said I’d told the judge what to do." {{cite news
|first=Marcia
|last=Clarke
|title=Polanskis Lost Alibi
|url=http://www.thedailybeast.com/blogs-and-stories/2009-09-30/polanskis-lost-alibi/
|publisher=The Daily Beast
|date=30 September 2009
|accessdate=2009-10-01
}} 

Wells then proceeded to blast Polanski  publicly, calling him a pedophile rapist, despite the 28-page probation report stating that he was not.   Wells said:

:"Its outrageous. This pedophile raped a 13-year-old girl. Its still an outrageous offense. Its a good thing he was arrested. I wish it would have happened years before."

Zenovich responded, as reported by Peter Knegt at indieWire:
:"I am perplexed by the timing of David Wells’ statement to the press that he lied in his interview with me for the documentary ‘Roman Polanski: Wanted and Desired,’ she said. Since June of 2008, the film has been quite visible on U.S. television via HBO, in theaters and on DVD, so it is odd that David Wells has not brought this issue to my attention before.

:Zenovich said that the day she filmed Mr. Wells at the Malibu Courthouse, he gave her a one-hour interview.  He signed a release like all my other interviewees, giving me permission to use his interview in the documentary worldwide," she said.  "At no time did I tell him that the film would not air in the United States.

:She went on to say she is astonished that Wells has changed his story. Mr. Wells was always friendly and open with me, she said. At no point in the four years since our interview has he ever raised any issues about its content.  In fact, in a July 2008 story in The New York Times, Mr. Wells corroborated the account of events that he gave in my film…  It is a sad day for documentary filmmakers when something like this happens." {{cite news
|first=Peter
|last=Knegt
|title="Desired" Director Zenovich Responds To Polanski Prosecutor’s "Lies"
|url=http://www.indiewire.com/article/desired_director_zenovich_responds_to_polanski_prosecutors_lies/
|publisher=indieWire
|date=1 October 2009
|accessdate=2009-10-01
}} 
Michael Ceply commented on the affair in the New York Times:
:"The incident points to the power and the limitations of the documentary art.  Pat Aufderheide, director of the Center for Social Media at American University   pointed out that the documentarians’ tendency to focus on untold stories has occasionally reopened a legal case or righted a perceived wrong. "

:"But even an on-camera account like that of Mr. Wells, which provoked a legal challenge that is still before the appellate courts in California, has its vulnerabilities. Ms. Zenovich acknowledged on Thursday, for instance, that she had no second person describing the contacts between Mr. Wells, who was assigned to the courthouse but had no official role in the Polanski case, and Judge Rittenband, who has since died. There wasn’t a witness to the conversations with Rittenband, she said."

:"Since Mr. Polanski’s arrest, Ms. Zenovich said, a strong public reaction against his misdeeds in the original case seem to have overridden concerns about any misdeeds in the courts. Originally charged with seven counts, including rape and sodomy, he pleaded guilty to having sex with a minor and then fled the country before his sentencing."

:"People don’t seem to connect to what this is, really, because of the crime, and the schism it has caused with finding normalcy and sanity she said of her continuing examination of the Los Angeles legal system, with its problems with celebrity justice."

:"Even if Dave Wells were to be lying in the original film, she said, we still have a judge who was instructing the prosecutor and defense lawyer on how to behave, and doing it based on how he would look in the media, she added, referring to other allegations in the documentary." {{cite news
|first=Michael
|last=Ceply
|title=2008 Film Plays Role in Polanski Case
|url=http://www.nytimes.com/2009/10/02/movies/02polanski.html?_r=1&ref=todayspaper
|publisher=New York Times
|date=1 October 2009
|accessdate=2009-10-01
}} 

==References==
 

==External links==
* 
* 
* Photcopy of statement dated June 11, 2008, signed by Douglas Dalton and Roger Gunson  
* The public information office at the Los Angeles County Superior Court sends out this media advisory on Roman Polanski documentary on HBO.  
*   Official Website and Blog for film, by Director Marina Zenovich.

 

 
 
 
 
 
 
 
 