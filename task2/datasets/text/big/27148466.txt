Superman (1997 film)
{{Infobox film
| name           = Superman
| image          = Superman (1997 film).jpg
| image_size     = 
| alt            = 
| film name      = സൂപ്പർമാൻ
| caption        = Movie screenshot Rafi and Mecartin
| producer       = Siddique, Lal, Azeez
| writer         = Rafi Mecartin
| narrator       =  Siddique
| music          = S. P. Venkatesh
| cinematography = Anandakuttan
| editing        = Harihara Puthran      
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}

Superman (  by Rafi Mecartin starring Jayaram and Shobana in lead roles. The film was produced and distributed by Siddique, Lal and Azeez under the banner of Kavyachandrika Release.

==Synopsis==
Nithya (Shobana) is the deputy commissioner of the city  who lives with her Valliyachan (Janardhanan) since both her parents were killed in an accident. She was put on to investigate a corruption case done by the Home Minister and his friend a business man called Rajan Phillip (Cochin Haneefa). Unknowingly she was helped in the investigation by a criminal nicknamed Superman (Jayaram). Superman aka Harikrishnan nurtures a vendetta with the home minister and rajan phillip and uses Nithya to trap his enemies. In the whole process Superman uses Judiciary as his weapon, by providing vital evidences to Nithya at times in order to collect evidences Superman himself breaks the law, but as it is for the greater good it is forgiven by Nithya in the end.

==Cast==
*Jayaram ...  Hareendran/Superman
*Shobana ...  Nithya (Dy. Commissioner of Police)
*Jagadish ...  Balachandran (Police Officer & friend of Hareendran) Siddique ...  City Police Commissioner
*Spadikam George ...  Jaganathan (Police Officer)
*Cochin Haneefa ...  Rajan Philip Janardhanan ...  Nithyas Valyachan Innocent ...  Kochunni (Friend of Hareendran)
*Sreejaya ...  Nalini
*Nedumudi Venu ...  Raman Nair (Hareendrans father)
*Zeenath ...  Bhavani (as Zeenat)
*Vinduja Menon ...  Hareendrans Sister
*Bindu Panicker ...  Swarnalatha (Jaganathans wife)
*T. P. Madhavan ...  Home Minister Krishnakumar ...  Himself

== Crew ==
* Cinematography: Anandakkuttan
* Editing: Hariharaputhran
* Art: Valsan
* Makeup: P. S. Mani
* Costumes: Velayudhan Keezhillam
* Cinematography: Nadeem Khan, Kumar, Shanti
* Stunts: Kanal Kannan
* Lab: Prasad Colour Lab
* Stills: Surya John
* Effects: Murukesh
* Production Controller: Girish Vaikom
* Re-recording: Sambath
* Second unit cameraman: Raju Warrior
* Associate Director: K. C. Ravi
* Laison: Mathew J. Neriyamparambil

== Songs ==
S. P. Venkatesh scored the music for this film which was written by S. Rameshan Nair and I. S. Kundoor. The music was distributed by Kavyachandrika Audios.

; Songs list
* Onathumbi padoo: K. J. Yesudas
* Marumozhi thedum kilimakale: M. G. Sreekumar
* Aavaram poovinmel: Biju Narayanan, Sujatha Mohan
* Hole hole manthrikan njaan: M. G. Sreekumar (written by I. S. Kundoor)
* Thappu thalangngaL: M. G. Sreekumar (written by I. S. Kundoor)

==External links==
*  
* http://popcorn.oneindia.in/title/7564/superman.html

 
 
 
 
 
 
 