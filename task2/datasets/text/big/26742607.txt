I Was a Spy
{{Infobox film
| name           = I Was a Spy
| image          = I Was a Spy 1933 Poster.jpg
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| director       = Victor Saville Herbert Mason (Assistant Director) Frank Sherwin Green (Assistant Director)
| producer       = Michael Balcon
| screenplay     = {{Plainlist|
* Ian Hay
* W.P. Lipscomb
* Edmund Gwenn
}}
| based on       =  
| narrator       = 
| starring       = {{Plainlist|
* Madeleine Carroll
* Herbert Marshall
* Conrad Veidt
}}
| music          = Louis Levy  
| cinematography = Charles Van Enger
| editing        = Frederick Y.Smith Gaumont British Picture Corporation
| distributor    = {{Plainlist|
* Woolf & Freedman Film Service    Fox Film Corporation  
}}
| released       =  
| runtime        = 89 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
I Was a Spy is a 1933 British thriller film directed by Victor Saville and starring Madeleine Carroll, Herbert Marshall, and Conrad Veidt. Based on the 1932 memoir I Was a Spy by Marthe Cnockaert, the film is about a Belgian woman who nurses injured German soldiers during World War I while passing intelligence to the British. 

I Was a Spy was also the first film dubbed in Poland (while there were earlier examples of films dubbed in Polish, they were recorded in Paramount studio in Joinville, France), released in 1935 as Siostra Marta jest szpiegiem, starring Lidia Wysocka as Martha Cnockhaerts voice. The screenplay was written by Edmund Gwenn.

==Plot==

In Belgium 1914, a nurse is trained as a spy.

==Cast==
* Madeleine Carroll as Martha Cnockhaert
* Herbert Marshall as Stephan
* Conrad Veidt as Commandant Oberaertz
* Edmund Gwenn as Burgomaster
* Gerald du Maurier as Doctor
* Donald Calthrop as Cnockhaert
* May Agate as Madame Cnockhaert
* Eva Moore as Canteen Ma
* Martita Hunt as Aunt Lucille
* Nigel Bruce as Scottie George Merritt as Captain Reichmann
* Anthony Bushell as Otto

==Reception==
The film was voted the best British movie of 1932, and Madeleine Carrolls performance was voted the best in a British movie. 

Although it was very successful in the Box Office this was not Savilles reaction. He watched the completed I Was a Spy with one of the Assistant Directors Herbert Mason and was devastated however Mason reassured him that it was his "best to date." 

==Home Media==

I Was a Spy was released on DVD on May 19, 2014.

==References==
 

==Bibliography==

* Moseley, Roy. (2000). Evergreen: Victor Saville in His Own Words. Southern Illinois University Press

==External links==
*  
*   BFI
*   Britmovie | Home of British Films

 

 
 
 
 
 
 
 
 
 