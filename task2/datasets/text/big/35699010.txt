Jungle Child (film)
{{Infobox film
| name           = Jungle Child
| image          = 
| caption        = 
| director       = Roland Suso Richter
| producer       = 
| writer         = Roland Suso Richter Sabine Kuegler
| starring       = Stella Kunkat
| music          = 
| cinematography = Holly Fink
| editing        = 
| distributor    = 
| released       =  
| runtime        = 131 minutes
| country        = Germany
| language       = German
| budget         = 
}}

Jungle Child ( ) is a 2011 German drama film directed by Roland Suso Richter.    

==Cast==
* Stella Kunkat as Sabine Kuegler - child
* Thomas Kretschmann as Vater Klaus Kuegler
* Nadja Uhl as Mutter Doris Kuegler
* Sina Tkotsch as Sabine Kuegler - youth
* Tom Hoßbach as Christian Kuegler - child
* Milena Tscharntke as Judith Kuegler - child
* Sven Gielnik as Christian Kuegler - youth
* Emmanuel Simeon as Auri - child
* Felix Tokwepota as Auri - youth
* Rangee Pati as Faisa
* Francesca Passingan as Ältere Faisa

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 