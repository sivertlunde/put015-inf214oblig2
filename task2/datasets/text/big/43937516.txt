Bidugade
{{Infobox film
| name           = Bidugade
| image          = 
| caption        = 
| director       = Y. R. Swamy
| producer       = T. P. Venugopal
| writer         = Ramesh Movies
| screenplay     = Ramesh Movies
| story          = 
| based on       =  
| narrator       =  Rajkumar  Bharathi  Rajesh  K. S. Ashwath
| music          = M. Ranga Rao
| cinematography = Annayya
| editing        = Babu   N. C. Rajan
| studio         = Ramesh Movies
| distributor    = 
| released       = 1973 
| runtime        = 175 minutes
| country        = India
| language       = Kannada
| budget         = 
| gross          = 
}}
 Rajesh and Bharathi in Kalpana made a brief appearance in the film.  M. Ranga Rao scored the music and the story was written and produced by Ramesh Movies. Chi. Udaya Shankar wrote the lyrics and dialogues.

The cinematography by Annayya won him the Karnataka State Film Award for Best Cinematographer award.

== Cast == Rajkumar  Bharathi  Rajesh
* Balakrishna
* Narasimharaju
* K. S. Ashwath
* Vajramuni
* Thoogudeepa Srinivas
* Shakti Prasad
* Sampath
* Dinesh Kalpana in a guest appearance

== Soundtrack ==
The music of the film was composed by M. Ranga Rao and the lyrics were written by Chi. Udaya Shankar. 

===Tracklist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s) !! Length
|- 
| 1
| "Bedagina Henna"
| P. B. Sreenivas, P. Susheela
| 06:06
|-
| 2
| "Baanige Neeliya"
| P. B. Sreenivas, P. Susheela
| 05:38
|-
| 3
| "Ninneyo Mugida Kathe"
| S. Janaki
| 03:27
|-
| 4
| "Nanna Putta Samsara"
| P. Susheela
| 03:26
|-
|}

==See also==
* Kannada films of 1973

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 

 