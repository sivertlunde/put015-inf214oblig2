Cattle Empire
{{infobox_film
| name           = Cattle Empire 
| image          =
| imagesize      =
| caption        =
| director       = Charles Marquis Warren 
| producer       = Robert Stabler II
| writer         = Daniel B. Ullman Endre Bohem Eric Norden Charles Marquis Warren   
| starring       = Joel McCrea Gloria Talbott
| music          = Paul Sawtell Bert Shefter
| cinematography = Brydon Baker
| editing        = Fred W. Berger 
| distributor    = 20th Century Fox
| released       =  
| runtime        = 
| country        = United States
| language       = English  
}}
 Western movie directed by Charles Marquis Warren and starring Joel McCrea. The film also features Gloria Talbott, Don Haggerty, Phyllis Coates and Bing Russell and serves as something of a forerunner for director Warrens subsequent television series Rawhide (TV series)|Rawhide starring Eric Fleming and Clint Eastwood, which used the pictures writer Endre Bohem as well as some of its supporting cast (Paul Brinegar, Steve Raines, Rocky Shahan and Charles H. Gray).

==Cast==
* Joel McCrea as John Cord
* Gloria Talbott as Sandy Jeffrey
* Don Haggerty as Ralph Hamilton
* Phyllis Coates as Janice Hamilton
* Bing Russell as Douglas Hamilton
* Richard Shannon as Garth
* Paul Brinegar as Tom Jefferson Jeffrey
* Charles H. Gray as Tom Powis (billed as Charles Gray)
* Hal K. Dawson as George Washington Jeffrey
* Patrick OMoore as Rex Cogswell
* Duane Grey as Juan Aruzza
* William McGraw as Jim Whittaker (billed as Bill McGraw)
* Jack Lomas as Sheriff Brewster

==External links==
 

 
 
 
 
 
 