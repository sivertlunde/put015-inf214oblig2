Warsaw Premiere
{{Infobox film
| name           = Warsaw Premiere
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Jan Rybkowski
| producer       = Tadeusz Karwański   Zygmunt Szyndler
| writer         = Stanisław Różewicz   Jerzy Waldorff   Mira Zimińska   Jan Rybkowski
| narrator       = 
| starring       = Jan Koecher   Barbara Kostrzewska   Jerzy Duszyński   Nina Andrycz
| music          = Kazimierz Sikorski
| cinematography = Andrzej Ancuta
| editing        = Brunon Jankowski
| studio         = Film Polski 
| distributor    = Film Polski 
| released       = 1951
| runtime        = 101 minutes
| country        = Poland
| language       = Polish
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}
Warsaw Premiere (Polish:Warszawska premiera) is a 1951 Polish historical film directed by Jan Rybkowski and starring Jan Koecher, Barbara Kostrzewska and Jerzy Duszyński.  The films art direction was by Roman Mann. The film portrays the life of the Polish composer Stanisław Moniuszko, particularly focusing on the composition of his 1848 opera Halka. The film was the first Polish costume film made since the Second World War, and was stylistically similar to historical biopics in other Eastern Bloc countries such as Rimsky-Korsakov (film)|Rimsky-Korsakov (1952). 

==Cast==
* Jan Koecher as Stanisław Moniuszko  
* Barbara Kostrzewska as Paulina Rivoli 
* Jerzy Duszyński as Włodzimierz Wolski 
* Nina Andrycz as Maria Kalergis 
* Ryszard Barycz as Poet  
* Ludwik Benoit as Sculptor 
* Halina Billing-Wohl as Moniuszko (wife)  
* Zenon Burzyński as Tunio  
* Gustaw Buszyński as Theater Director Ignacy Abramowicz  
* Jan Ciecierski as Bishop  
* Tadeusz Cygler as Julian Dobrski  
* Bronisław Darski 
* Wacław Domieniecki as Italian Tenor  
* Aleksander Gąssowski as Jefimow   Andrzej Gawroński  Jerzy Kaczmarek
* Maria Kaniewska 
* Józef Karbowski as Secretary  
* Emil Karewicz as Georg  
* Krystyna Karkowska as Magdalenka  
* Jan Kurnakowicz as Prof. Jan Quatrini  
* Wiesława Kwaśniewska 
* Henryk Modrzewski as Sennewald 
* Zdzisław Mrożewski as Count Alfred 
* Józef Nowak as Student 
* Kazimierz Opaliński as Theater Records-keeper 
* Lech Ordon as Publisher 
* Bronisław Pawlik as Shopkeeper 
* Mieczysław Pawlikowski as Musician 
* Tadeusz Pluciński as Painter 
* Wacław Ścibor-Rylski as Sacchetti 
* Ludwik Sempoliński as Klimowicz  
* Danuta Szaflarska as Countess Krystyna 
* Zdzisław Szymański as Reporter 
* Tadeusz Teodorczyk as Reporter  
* Janusz Warnecki as Professor  
* Stanisław Żeleński as Józef Sikorski, music critic 
* Janusz Ściwiarski 
* Olga Sawicka
* Teresa Szmigielówna
* Kazimierz Szubert 
* Jan Zieliński
* Czesław Piaskowski
* Jerzy Pietraszkiewicz
* Janina Ordężanka

==References==
 

==Bibliography==
* Liehm, Mira & Liehm, Antonín J. The Most Important Art: Eastern European Film After 1945. University of California Press, 1977.

==External links==
*  
*   at the www.filmpolski.pl  

 
 
 
 
 
 
 
 
 
 
 
 
 
 


 