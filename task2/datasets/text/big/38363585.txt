Josh: Independence Through Unity
 
 
 
{{Infobox film
| name           = Josh: Independence Through Unity 
| image          = Theatrical Release Poster of film Josh.jpg
| caption        = Theatrical release poster
| alt            = Poster showing the lead cast of film.  
| director       = Iram Parveen Bilal
| producer       = Iram Parveen Bilal Saad Bin Mujeeb Kelly Thomas
| writer         = Iram Parveen Bilal
| screenplay     = Iram Parveen Bilal Farah Usman  Ali Rizvi
| music          = Shahi Hasan Andrew T. Mackay
| cinematography = Nausheen Dadabhoy
| editing        = Jochen Kunstler
| casting        = Gulerana Azhar Asma Rizwan
| studio         = Parveen Shah Productions Twenty Nine Dash One Productions
| distributor    = ARY Films
| released       =  
| runtime        =
| country        = Pakistan
| language       = Urdu English
| budget         =  
| gross          = 
}} mystery Thriller thriller drama Ali Rizvi in the ensemble cast.

== Plot ==
Josh is about Fatima (Aamina Sheikh), a dedicated school teacher, who is living a high cosmopolitan life in Karachi until one day her life shatters when her nanny Nusrat-bhi inexplicably disappears. Fatima then takes on the challenge to seek the dangerous truth in Nusrat’s feudal village. The themes being tackled are class separation, feudalism, poverty, individual empowerment, and women’s rights.

== Cast ==
* Mohib Mirza
* Aamina Sheikh
* Naveen Waqar
* Khalid Malik Ali Rizvi
* Adnan Shah Tipu
* Salim Mairaj
* Kiaser Khan Nizamani
* Nyla Jaffri
* Parveen Akbar
* Faizan Haqquee

==Release and reception==
Josh world premiered at the Mumbai International Film Festival in October 2012.  The film released in Pakistan on 12 August 2013. 

==Soundtrack==
{{Infobox album  
| Name = Josh
| Type = Soundtrack
| Artist = Zoe Viccaji, Devika, Shahi Hasan, Noor Lodhi, Manesh Judge and Ali Azmat
| Cover = 
| Released = To Be Released.
| Genre = Film soundtrack
| Length = 
| Producer =Indus World Music
}}
Josh is the soundtrack album of the 2012 Urdu-language Pakistani film Josh by Iram Parveen Bilal.

==Overview and reception==
All songs are mixed and mastered by Shahi Hasan. The singers include Zoe Viccaji, Devika Chawla, Shahi Hasan, Noor Lodhi, Manesh Judge and Ali Azmat.

=== Awards ===
Josh received the 2012 Women in Film Finishing Grant, the 2013 Silent River Film Festival Best First Feature and Best Actress, the 2013 Filmfest Hamburg, Best Political Film Nomination. It also won the Best Screenplay Jury Prize and the Best Feature Film Audience Prize at the Washington, D.C. South Asian Film Festival 2014.

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 