Wings Up
{{Infobox film 
|  image = Wings Up.ogv
|  name     = Wings Up Robert Preston
|  producer       = First Motion Picture Unit
|  distributor    = Office of War Information 
|  runtime        = 20 min 
|  language = English 
|  color = 
}}
Wings Up is a short propaganda film film produced by the US Air Force during World War II.

Narrated by Clark Gable the short informed the youth of America about the Officers Candidate School of the Army Air Forces. It emphasized that while usually these courses would take years, the country was at war now and needed all the pilots it could get and fast. The curriculum is briefly outlined as well as the kind of life a cadets would lead at one of these facilities.

== External links ==

* 
* 

 
 
 
 
 
 
 
 
 


 
 