The Sinister Man
{{Infobox film
| name           = The Sinister Man
| image          = "The_Sinister_Man"_(1961).jpg
| caption        = 
| director       = Clive Donner
| producer       = Jack Greenwood
| writer         = Robert Stewart Edgar Wallace (novel) John Bentley Charles Blackwell
| cinematography = Bert Mason
| editing        = Derek Holding
| distributor    = 
| released       = 1961
| runtime        = 61 mins
| country        = United Kingdom
| language       = English
}}
The Sinister Man is a 1961 British drama film directed by Clive Donner and starring Patrick Allen, Gerald Anderson and Edward Atienza.  It was one of the series of Edgar Wallace Mysteries, British second-features, produced at Merton Park Studios in the 1960s. 

==Plot== archeological artefact known as the Kytang Wafers, and this is now missing. Scotland Yard investigates.

==Cast==
* Patrick Allen as Dr. Nelson Pollard 
* Gerald Anderson as Major Paul Amery 
* Edward Atienza as Clerk  John Bentley as Superintendent Wills 
* Wilfrid Brambell as Lock keeper 
* Yvonne Buckingham as Miss Russell 
* Michael Deacon as Angus 
* Jacqueline Ellis as Elsa Marlowe 
* Keith Faulkner as Lock keepers assistant 
* William Gaunt as Mitch Hallam 
* John Glyn-Jones as Dr. Maurice Tarn 
* Peter Hager as Warden  John Horsley as Pathologist 
* Burt Kwouk as Captain Feng 
* Arnold Lee as Soyok  Robert Lee as Nam Lee 
* Brian McDermott as Detective Sergeant Stillman
* Don McKillop as Reporter

==Critical reception== Steptoe Wilfred Brambell, Patrick Allen, William Gaunt and Burt Kwouk. John Bentley who took the lead as Superintendent Willis was a popular actor in the late fifties and early sixties but is all but forgotten these days."  

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 

 