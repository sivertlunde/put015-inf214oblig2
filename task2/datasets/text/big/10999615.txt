The Train (2007 film)
 
 

{{Infobox Film | name = The Train: Some Lines Should Never Be Crossed
  | image    = thetraine.jpg
  | caption  = Movie poster for The Train
  | director = Hasnain Hyderabadwala and Raksha Mistry		
  | producer = Siddhivinayak Creations
  | starring = Emraan Hashmi Geeta Basra Sayali Bhagat
  | music    = Mithoon
  | country  = India
  | runtime  = 134 min
  | released = 8 June 2007
  | language = Hindi
  | budget = 
  | gross = 
}}

The Train is a 2007 Bollywood thriller film directed by Hasnain Hyderabadwala and Raksha Mistry. about a married couple caught up in a complex emotional tangle. The film is a thriller that revolves around an extramarital affair. This is the debut for actress Sayali Bhagat. This movie is a remake of 2005 American film Derailed (2005 film)|Derailed.

==Plot==

Vishal Dixit (Emraan Hashmi), a regular middle class man, is settled in Bangkok with his wife, Anjali (Sayali Bhagat) and their 5-year-old daughter, Nikki. Anjali and Vishal are trying to hold on to their deteriorating marriage which is already under tremendous pressure. One ordinary day, on his way to work, Vishal meets Roma (Geeta Basra), a beautiful, captivating woman. The attraction between them is instantaneous. As Vishal gets to know her better, he realizes that she too is a loner, trapped in a loveless marriage. The attraction between them reaches a peak and at a point they decide to give in to an adulterous affair. As Anjali starts getting suspicious of Vishals erratic behavior, his romance with Roma deepens. He is torn between the two loves of his life. Just when Vishal thought life could not get more complicated, he realizes that his secret liaison is known to someone, an unknown elusive enemy who is bent on using the knowledge to destroy all three lives. Vishal now realizes that the game will never end till he finds a permanent solution and that would be to eliminate the enemy. But how does one find an enemy who has no identity? Can he protect his family? Will he be able to save both the women in his life? All Vishal is sure of is that time is running out..... Is it too late? For Vishal? For Anjali? For Roma?

==Cast==
*Emraan Hashmi as Vishal Dixit	
*Sayali Bhagat as Anjali Dixit
*Geeta Basra as Roma Kapoor/Richa Malhotra	
*Aseem Merchant as Tony
*Rajat Bedi as Officer Asif Ahmed Khan
*Rajesh Khattar as friend of the girl

==Soundtrack==
{{Infobox album |  
  Name        = The Train |
  Type        = Album |
  Cover       = |
  Released    = 6 April 2007| Feature film soundtrack |
  Length      = 60:59|
  Artist      = Mithoon|  
  Label       = T-Series|
}}
The music of the film was composed by Mithoon with lyrics penned by Sayeed Quadri. The songs Woh Ajnabee, Beete Lamhe & Mausam were chartbusters

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers || Length (m:ss) ||Notes
|- KK || 05:01 || Picturised on Sayali Bhagat and Emraan Hashmi
|- KK || 06.17 || 
|-
| 3|| "Mausam" || Mithoon|| 05:45 ||  Picturised on Emraan Hashmi
|-
| 4|| "Mausam" - Club Mix || Mithoon || 05:18 || 
|-
| 5|| "Mausam" - Progressive Mix || Mithoon || 04:50 ||
|-
| 6|| "Teri Tamanna" || Shahrukh Khan, Zubeen Garg || 06:22 || Picturised on Emraan Hashmi and Geeta Basra
|-
| 7|| "Teri Tamanna" - Club Mix || Krishnakumar Kunnath|KK, Zubeen Garg || 06:47 || 
|-
| 8|| "Teri Tamanna" - Euro Mix || Krishnakumar Kunnath|KK, Zubeen Garg || 05:37 ||
|- Shaan || 05:19 ||
|-
| 10|| "Woh Ajnabee" || Mithoon, Shilpa Rao || 05:07 || Picturised on Sayali Bhagat and Emraan Hashmi
|-
| 11|| "Woh Ajnabee" - Club Mix || Mithoon, Shilpa Rao || 04:36 ||
|}

==External links==
* 

 
 
 
 
 
 
 