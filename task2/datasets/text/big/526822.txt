Christmas on Mars
{{Infobox film
| name           = Christmas on Mars
| image          = Christmas on Mars poster.jpg
| image_size     =
| caption        = theatrical poster
| director       = Wayne Coyne
| writer         = Wayne Coyne
| starring       = Wayne Coyne Steven Drozd Michael Ivins
| music          = The Flaming Lips
| cinematography = Bradley Beesley
| editing        = George Salisbury
| distributor    = Warner Independent Pictures
| released       =  
| country        = United States
| language       = English
| budget         =
| gross          =
}}

Christmas on Mars is a science fiction film from the alternative rock band The Flaming Lips, written and directed by the bands frontman, Wayne Coyne and featuring the entire band in the cast, as well as many of their associates, including Steve Burns, Adam Goldberg, and Fred Armisen.

The film began development in 2001, filming was completed in October 2005, and the film premiered on May 25, 2008 at the Sasquatch! Music Festival.     For its general release in the United States, Christmas on Mars was booked into several dozen cities for unconventional screenings, in venues which included a former Ukrainian Socialist Social Club in New York City.    The film was released in three different packages on November 11, 2008 through conventional retailers as well as through the bands website. A vinyl edition was released November 25, 2008.

== Plot ==
The film tells the story of the experiences of Major Syrtis during the first Christmas on a newly colonized Mars.

The main character, Major Syrtis (played by Steven Drozd), is trying to organise a Christmas pageant to celebrate the birth of the first colonist baby. Coyne explains that this birth is also special for other reasons,

Fortunately, Major Syrtis finds an unlikely ally in Coynes strange and mysterious character, a "Martian that lands, but the Martian isnt really perceived as a Martian. People just sort of think hes another crazy guy whos flipped and turned himself green. They cant find a quick replacement for Santa so they just use this Martian guy. So the Martian guy becomes the Martian and Santa Claus at the same time."

== Cast ==
 
*Steven Drozd as Major Syrtis
*Wayne Coyne as The Alien Super-Being
*Michael Ivins as Deuteronilus
*Kliph Scurlock as Orcus
*Steve Burns as Astronaut

*Scott Booker as Sirenum
*Adam Goldberg as Dr. Scott Zero
*Fred Armisen as Noachis
*Mark Degraffenreid as Captain Icaria
*Jimmy Pike as Jim Eleven
*Kenny Coyne as Ed Fifteen

;Cast notes: Isaac Brock of Modest Mouse and Elijah Wood do not appear in the films final cut. However, a scene with Isaac Brock and Adam Goldberg exists on the DVD as an easter egg.

== Production ==
At the beginning of 2002, over 20 minutes of edited film were ready, with music and preliminary sound effects also completed for these scenes. Most of the movie was shot on 16 mm film, with most of the sets based in Waynes Oklahoma City house. Most scenes were filmed in and around Oklahoma City, using locations such as old industrial facilities.  Further filming was done in Austin, Texas.

== Release ==
Originally, Christmas on Mars was not to be released at conventional movie theaters. Instead, a DVD release would be preceded by a number of screenings at rock venues.  Speaking to mtv.com, Coyne has explained, "We want to show the movie with a mega-sound system and snow machines and just make it like a bigger event than what has become the typical movie-going experience. Im hoping that people can watch this movie while they drink beer, smoke cigarettes, and have a good time."
 HD and adding "in-depth special effects." On September 12, 2008, the film made its New York City debut at 7am within the KGB Complex, a former Ukrainian Socialist Social Club, on the Lower East Side.   

==DVD== easter egg.

== Trailer ==
A trailer for Christmas on Mars was first made available on the bands Fight Test Extended play|EP, and can now be found along with movie information on The Flaming Lips own website.
 special edition of their tenth album Yoshimi Battles the Pink Robots. Further trailers were made available through the bands website and the Warner Records YouTube channel.

== Closing credits ==
Under the English closing credits are what appear to be Russian translations. But the Russian translations seem unrelated to the English credit listings. For example, "Make Up & Hair", has Russian under it that translates to "The sparkle on the mountain peak". "Feature" has Russian under it that translates  to "Loud sounds make me feel fine." "Marching Band & Bubble Room Extras" has under it, "Science can be magical".

==Soundtrack==
{{Infobox album 
| Name        = Once Beyond Hopelessness
| Type        = soundtrack
| Artist      = The Flaming Lips
| Cover       = Christmas on mars cover.jpg
| Released    =  
| Recorded    = January 2001&nbsp;– September 2008 score
| Length      =   Warner Bros.
| Producer    = The Flaming Lips
| Last album  = At War with the Mystics  (2006)
| This album  = Once Beyond Hopelessness (2008)
| Next album  = Embryonic (2009)
}}
{{Album ratings rev1 = Allmusic rev1score =    rev2 = Okayplayer rev2score = (75/100)  rev3 = The Quietus rev3score = (favourable)  rev4 = Pitchfork Media rev4score = (7.1/10) 
}}
Two tracks from the movie soundtrack, "Protonilus Death March" and "Syrtis Major" were released as a 5000-only picture-disc EP late in 2004, available only through the bands online store in conjunction with the purchase of their limited-edition pictorial biography Waking Up With a Placebo Headwound. "Syrtis Major" and another soundtrack cut, "Xanthe Terra", were also released as B-sides to the 2-part European single release of "Do You Realize??" These songs are both entirely instrumental, in a similar style to acclaimed Lips instrumentals such as the Grammy-winning Approaching Pavonis Mons by Balloon (Utopia Planitia). For the final soundtrack release, "Protonilus Death March" has been renamed to "The Gleaming Armament of Marching Genitalia," "Syrtis Major" is now "Space Bible With Volume Lumps" and "Xanthe Terra" is now "Suicide and Extraordinary Mistakes."

===Track listing===
{{Track listing
| title1 = Once Beyond Hopelessness
| length1 = 3:07
| title2 = The Distance Between Mars and the Earth, Pt. 1
| length2 = 0:52
| title3 = The Horrors of Isolation: The Celestial Dissolve, Triumphant Hallucination, Light Being Absorbed
| length3 = 4:39
| title4 = In Excelsior Vaginalistic
| length4 = 3:02
| title5 = Your Spaceship Comes from Within
| length5 = 1:28
| title6 = Suicide and Extraordinary Mistakes
| length6 = 3:28
| title7 = The Distance Between Mars and the Earth, Pt. 2
| length7 = 0:57
| title8 = The Secret of Immortality: This Strange Feeling, This Impossible World
| length8 = 3:43
| title9 = The Gleaming Armament of Marching Genitalia
| length9 = 3:58
| title10 = The Distress Signals of Celestial Objects
| length10 = 2:11
| title11 = Space Bible with Volume Lumps
| length11 = 3:15
| title12 = Once Beyond Hopelessness
| length12 = 2:03   
}}

== References ==
 

== External links ==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 