Keeping the Faith
 
{{Infobox film
| name           = Keeping the Faith
| image          = Keeping the faith.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Edward Norton
| producer       = Hawk Koch Edward Norton Stuart Blumberg
| writer         = Stuart Blumberg
| starring       = Ben Stiller Jenna Elfman Edward Norton Eli Wallach Anne Bancroft
| music          = Elmer Bernstein
| cinematography = Anastas N. Michos Malcolm Campbell
| studio         = Touchstone Pictures Spyglass Entertainment Koch Co. Norton/Blumberg Productions Triple Threat Talent Buena Vista Pictures
| released       =  
| runtime        = 128 minutes
| country        = United States
| language       = English
| budget         = $29 million
| gross          = $59.9 million
}}
Keeping the Faith is a 2000 American romantic comedy film, written by Stuart Blumberg and directed by Edward Norton. It stars Ben Stiller, Norton, Jenna Elfman, Eli Wallach, and Anne Bancroft. This film was released by Touchstone Pictures and Spyglass Entertainment, in association with Triple Threat Talent on April 14, 2000.

The film is Nortons directorial debut and was dedicated to his late mother, Robin Norton. The film had a budget of $29 million. 

==Plot==
  Conservative synagogue; his lack of effort to find a Jewish wife often results in his mother, Ruth (Anne Bancroft) and other women of his congregation setting him up on blind dates, much to his dismay. The two men show a close bond, even in their professions, where the two are planning the opening of a jointly sponsored community center. In its earlier days, the friendship included a third party. Via flashbacks and reminiscent musings, Anna Reilly (Jenna Elfman) is introduced: she met Jake and Brian in middle school, after beating up a bully who was picking on them. The three became great friends, and enjoyed their childhood together. Unfortunately, Annas father got a new job that resulted in the Reillys moving to California, and ultimately she lost touch with Brian and Jake.

Sixteen years later, Anna calls her old friends out of the blue and the friendship is rekindled when her company temporarily reassigns her to a New York position. Feelings quickly begin to run deeper than before, as Anna, despite her workaholic tendencies, is as vibrant as Brian and Jake remembered her; however, it is, ironically, the mens careers that prove to be the most problematic. She and Jake begin sleeping together, but he is reluctant to be involved in a serious relationship with her because she is not Jewish, a fact which could compromise his relationship with his congregation and also with his mother (who disowned her eldest son - Jakes older brother Ethan - for marrying outside the faith). Between the religious conflict and their desire to spare the feelings of their mutual friend, the relationship is kept mostly secret, resulting in both humorous and harmful complications. As the months pass, both Jake and Annas feelings for each other become stronger but due to the aforementioned issues, Jake still refuses the relationship as a serious one, despite Anna dropping hints to him about her having been recently taking a class (but refusing to tell him what kind of class it is), and her becoming visibly upset when they run into members of Jakes congregation while on a date and Jake introducing her only as "my old friend Anna".

Meanwhile, Brian is involved in his own test of faith as he struggles with his feelings for Anna despite his vows. Apart from praying about the situation and discussing it with Fr. Havel, he keeps these thoughts mostly to himself. Brian begins misinterpreting Annas words and actions (some of which are subtle signals to Jake as their affair is kept under wraps) and even has an erotic dream about her; he begins to seriously consider quitting the priesthood to pursue a romantic relationship with her. Anna tells Jake that she wants things to be more serious between them and he does not respond well. While the three have dinner one night with Jakes mother Ruth, Ruth has a private conversation with Anna, where she tearfully reveals that she knows about Anna and Jakes secret relationship. Jake and Brian walk in on the ladies having a tearful moment, and later Jake and Anna have an argument over the religious issues complicating their romance, which ends in the two parting ways in frustration. Anna calls Brian for comfort and he rushes over to her apartment. Still unaware of whats been going on, he takes her tearful ramblings to be a confession of feelings for him, then kisses her and admits his love. When she interrupts him, he first assumes it to be guilt based on his vows, but she tells him she is in love with Jake and finally admits that she and Jake have been seeing each other secretly for months. Feeling embarrassed and rejected, Brian raids Annas liquor cabinet, angrily cutting off her attempts to re-assure him and apologize. He leaves and spends the whole night out drinking on the streets. The next day, still drunk, Brian stumbles into Jakes temple and interrupts a post-bar mitzvah gathering, resulting in a confrontation with Jake that ends with the priest punching the rabbi. He leaves and stumbles around the city, which brings the movie back to the very first scene with the bartender.
 convert to Judaism. She tells him she hopes to pick it up again as she is also now staying in New York, with Jake clearly thrilled. The film ends happily with the three childhood friends posing for a photo together.

==Cast==
* Ben Stiller as Rabbi Jacob "Jake" Schram
* Samuel R. Goldberg as Teenage Jake Schram
* Edward Norton as Father Brian Kilkenney Finn
* Michael Charles Roman as Teenage Brian Finn
* Jenna Elfman as Anna Reilly
* Blythe Auffarth as Teenage Anna Reilly 
* Anne Bancroft as Ruth Schram
* Miloš Forman as Father Havel
* Eli Wallach as Rabbi Ben Lewis
* Holland Taylor as Bonnie Rose
* Lisa Edelstein as Ali Decker
* Rena Sofer as Rachel Rose
* Bodhi Elfman as Howard the Casanova, the businessman in the office across the road
* Brian George as Paulie Chopra, the Sikh Catholic Muslim with Jewish in-laws who owns an Irish Pub
* Ron Rifkin as Larry Friedman
* Eugene Katz as Mohel (performing the circumcision in opening sequence where Jake faints)
* Ken Leung	as Dang, the electronics store owner
* Susie Essman as Ellen Friedman
* Catherine Lloyd Burns as Debbie

==Box office== 28 Days Rules of Engagement.  The film eventually grossed $37,047,880 in North America and $22,897,303 in other territories, totaling $59,945,183 worldwide.   

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 