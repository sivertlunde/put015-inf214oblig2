The Canterbury Tales (film)
{{multiple issues|
 
 
}}

{{Infobox film
| name           = The Canterbury Tales
| image          = Canterbury-Tales-1972.jpg
| image size     =
| director       = Pier Paolo Pasolini
| producer       = Alberto Grimaldi
| based on       =  
| writer         = Pier Paolo Pasolini Produzioni Europee Associate
| distributor    = United Artists
| starring       = Franco Citti, Ninetto Davoli, Laura Betti, Pier Paolo Pasolini
| released       = West Germany 2 July 1972 (premiere at Berlin Film Festival|BIFF) Italy 2 September 1972 USA 30 March 1980
| runtime        = 122 minutes 110 min (reduced cut)
| country        = Italy
| language       = Italian/English
| music          = Ennio Morricone
| cinematography = Tonino Delli Colli
}} The Decameron Arabian Nights. It won the Golden Bear at the 22nd Berlin International Film Festival.   

The adaptation covers eight of the 24 tales and contains abundant nudity, sex and slapstick humour. Many of these scenes are present or at least alluded to in the original as well, but some are Pasolinis own additions.

The film sometimes diverges from Chaucer. For example, "  — it does not occur to the judge that such an act cannot be committed by one person alone — and is sentenced to death. As a foretaste of Hell, he is burned alive inside an iron cage ("roasted on a griddle" in the words of one spectator) while vendors sell beer and various baked and roasted foods to the spectators.

In Pasolinis version of the fragmentary "The Cooks Tale|Cooks Tale", Ninetto Davoli plays the role of Perkyn in manner clearly inspired by Charlie Chaplin.

This film featured Tom Baker, the famed Fourth Doctor from Doctor Who, in a small role, as one of the husbands of the Wife of Bath.

==Plot summary== Gloomy Grim Reaper. In the eighth and final story, a pleasure-loving monk is trying to earn as much food in exchange for extreme unction to a dying man. That same night in the mans convent comes an angel, which leads him to Hell, to show him the terrible punishments that belong to the friars that subvert the message of Christ.

All the stories are linked to the arrival of a group of pilgrims to the shrine of Canterbury, among which there is the poet Geoffrey Chaucer (played by Pasolini himself), who during breaks in the pub with friends, writes and tells the stories, which have the purpose of moralizing and delight the audience.

==Cast==
* Hugh Griffith - Sir January
* Laura Betti - The Wife of Bath
* Ninetto Davoli - Perkin
* Franco Citti - The Devil
* Josephine Chaplin - May Alan Webb - Old Man
* Pier Paolo Pasolini - Geoffrey Chaucer
* Jenny Runacre - Alison
* Tom Baker - Jenkin
* Robin Askwith - Rufus Michael Balfour - John the Carpenter
* Vernon Dobtcheff - The Franklin
* Derek Deadman - The Pardoner (as Derek Deadmin)
* Adrian Street - Fighter

==References==
 

== External links ==
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 