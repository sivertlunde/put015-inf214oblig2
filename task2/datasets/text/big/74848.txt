Killer of Sheep
 
{{Infobox film
| name           = Killer of Sheep
| image          = Killer of sheep.jpg
| image_size     = 220px
| border         = yes
| alt            = 
| caption        = 2007 re-release theatrical poster Charles Burnett
| producer       = Charles Burnett
| writer         = Charles Burnett
| starring       = Henry G. Sanders Kaycee Moore Charles Bracy Angela Burnett
| cinematography = Charles Burnett
| editing        = Charles Burnett
| distributor    = Milestone Films
| runtime        = 80 minutes  
| country        = United States
| language       = English
| budget         = United States dollar|US$10,000
| gross          = $416,509 
}} Charles Burnett. urban African-Americans Watts district. The films style is often likened to Italian Neorealism (art)|neorealism.
 US United States dollar|$150,000 and the film was restored and transferred from a 16mm to a 35mm print. Killer of Sheep received a limited release 30 years after it was completed, with a DVD release in late 2007.

Film critic  , March 30, 2007  There are no acts, plot arcs or character development, as conventionally defined.

==Plot==
Stan works long hours at a slaughterhouse in Watts, Los Angeles, California|Watts, Los Angeles. The monotonous slaughter affects his home life with his unnamed wife and two children, Stan Jr. and Angela.

Through a series of episodic events - some friends try to involve Stan in a criminal plot, a white woman propositions Stan in a store, Stan and his friend Bracy attempt to buy a car engine - a mosaic of an austere working-class life emerges in which Stan feels unable to affect the course of his life.

==Cast==
* Henry G. Sanders as Stan
* Kaycee Moore as Stans wife
* Charles Bracy as Bracy
* Angela Burnett as Stans daughter
* Eugene Cherry as Eugene
* Jack Drummond as Stans son

==Production==
Directed by  , Los Angeles Times, April 6, 2007 

==Critical reception==
Though the film won the Critics Award at the Berlin Film Festival and was acclaimed at the Toronto International Film Festival, it never saw wide release due to complications in securing the music rights for the 22 songs on the soundtrack, which included such big names as Dinah Washington, Paul Robeson, Louis Armstrong, and Earth, Wind and Fire. It remained in obscurity for nearly thirty years, garnering much critical and academic praise and earning a reputation as a lost classic.

Killer of Sheep holds a 97% "fresh" rating on   and  , March 30, 2007 

The film was chosen by the National Society of Film Critics as one of the 100 Essential Films. In 1990, Killer of Sheep was selected for preservation in the United States National Film Registry by the Library of Congress for being "culturally, historically, or aesthetically significant." 

===Lists===
The film appeared on several critics top ten lists of the best films of 2007. 

* 1st – Ed Gonzalez, Slant Magazine
* 2nd – Philip Martin, Arkansas Democrat-Gazette
* 3rd – Glenn Kenny, Premiere (magazine)|Premiere
* 3rd – Michael Sragow, The Baltimore Sun
* 3rd – Nick Schager, Slant Magazine
* 3rd – Richard Corliss, Time (magazine)|TIME magazine
* 5th – Dana Stevens, Slate (magazine)|Slate
* 10th – Ella Taylor, LA Weekly

==Distribution==
Having previously only existed on worn 16mm prints, the film was restored and enlarged to 35mm by the UCLA Film and Television Archive and Milestone Films, thanks in part to a donation from filmmaker Steven Soderbergh. The soundtrack, which had not been licensed, was also paid for at a cost of over US$150&nbsp;000.

On March 30, 2007, it opened in  ).

On Martin Luther King, Jr. Day, January 21, 2008, Turner Classic Movies presented the world broadcast premiere of the film as part of a night-long marathon of Burnetts work. Burnett was interviewed before and after the film by TCMs primetime host Robert Osborne.

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 