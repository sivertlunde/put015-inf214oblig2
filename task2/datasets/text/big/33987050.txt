Yamashita: The Tiger's Treasure
{{Infobox film
| name           = Yamashita: The Tigers Treasure
| image          = Yamashita the tigers treasure.jpg
| caption        = The original DVD cover.
| alt            = 
| director       = Chito S. Roño
| producer       = Douglas Quijano Sherida Monteverde Allan Escaño
| writer         = Roy Iglesias Rustom Padilla Camille Prats
| music          = Nathan Brendholdt Kormann Roque
| cinematography = Neil Daza
| editing        = Manet Dayrit
| studio         = Roadrunner Network, Inc. MAQ Productions
| distributor    = Regal Entertainment
| released       =    (Metro Manila Film Festival|MMFF) 
| runtime        = 113 minutes
| country        = Philippines
| language       = Tagalog English Japanese
| budget         = Philippine Peso|PhP. 80 million (estimated) gross           = 
}}
 Filipino epic epic adventure-drama Best Picture. 

==Plot== Yamashita Treasure. Japanese World War II veteran.
 Yamashita Treasure to his grandson, Jobert. Jobert decided to return to the Philippines to attend a reunion with his friends and former classmates. Flashbacks of Lolo Melos life, including his happy life with his younger brother, Peping, which he gave an expensive harmonica as a gift before he enlists in the Army. World War II breaks out and Melo, along with his brother were captured by Imperial Japanese Army|IJA.
 Yamashita orders some POWs to follow some orders. They were chosen, along with some POWs and Filipino civilians. They were herded in a Japanese transport ship bound to Mindoro. They suffered severe maltreatment, from rotational torture, feeding them with meager rations,from spoiled rice to boiled camote roots and among others.
 Rustom Padilla. They were forced to ride a speedboat to the location of the treasure. The diary, revealed as the map pieces, was torn by Emong and the henchman. They go to Mindoro to confirm the area. Lolo Melos past are revealed further by flashbacks. When they arrive in Mindoro,they are forced to dig several very large tunnels to bury something. Then the ship, carrying Yamashitas loot, arrives in Mindoro. They were ordered to place the treasures in the tunnels and place some traps, including land mines. Melo and several others are forced to seal the area after the duty and an American air raid attacks them, burying also his younger brother and some of their fellow prisoners alive. Peping plays the harmonica one last time,as his co-prisoners are dying due to suffocation and dies of his wounds.

Meanwhile, Jobert and the gang arrives in Mindoro. Jobert was reunited with Lolo Melo, captured by Naguchis men as asset, bargaining chip and guide, along with multiple trucks to transport the loot. But, a distraction in the form of backups called by the secret service agents following Naguchis moves. Jobert and Lolo Melo, along with Jarco leads them into the tunnel,seemingly full of gold,loot and the legendary Golden Buddha. Lolo Melo forces Jobert to leave the tunnel as he confronts the henchman. He saw his younger brothers skeletal remains, along with the rusting harmonica he gave many decades ago. Jarco, seemingly mesmerized by countless loot in his front, opens the Golden Buddha. But he accidentally steps on a land mine, causing a chain of explosions that destroys the tunnel. Lolo Melo plays the harmonica one last time, while Pepings spirit smiles in front of him and embraces him. He dies in the explosion.
 EDSA was renamed Carmelo Rosales Avenue due to his honor, heroism and resilience.

==Cast==

===Main roles===
*Armando Goyena as Carmelo Rosales, also known as Lolo Melo
*Danilo Barrios as Jobert Rosales, Lolo Melos grandson 
*Albert Martinez as Emong, a Philippine Government Official
*Carlo Muñoz as the young Carmelo
*Vic Diaz as the old World War II Japanese veteran Naguchi Rustom Padilla as Jarco, the right-hand of Naguchi
*Tetsuya Matsui as the young Naguchi
*Camille Prats as Xyra, the love interest of Jobert
*Tadakazu Sakuma as General Yamashita

===Supporting roles===
*Janus Del Prado as Omar
*Bearwin Meily as Elmore
*Mico Palanca as Vince
*Ethan Javier as Willie
*Leni Rivera as Glecy Castro
*Johnny Revilla as Orly
*Bambi Arambulo as Laila
*Marvin Lanuza as Phil
*Heidi Reyes as Pam
*Gail Valencia as Connie
*Pocholo Montes as Gen. Rivas
*Fonz Deza as Velasco
*Reggie Curley as Bello
*Niño Benedicto as Peping, Carmelos younger brother
*Troy Martino as Xyras dad
*Mia Gutierrez as Xyras mom
*Hernando San Pedro as old Delfin
*Jez Umali as young Delfin

==Production==
The film was shot in United States and Philippines. Roadrunner Network, Inc. is responsible for the majority of visual effects. The titles were made by Cinemagic. The films were printed by LVN Pictures.

==Soundtrack== sound mixing was done at The Elemantal Music.

"The Treasure In You" is the theme song of the film, composed by Elvin Reyes and Normann Roque and arranged by Ruth Bagalay. It was recorded by singer Pops Fernandez.

==Release==

===Reception===
In review aggregator Rotten Tomatoes it has an approval rating of 57% and an average score of 3.5 out of 5 from users based on 80 reviews.

===Home Media===
The official home video of the film was released on November 15, 2005 in Region-3 DVD format.

==Accolades==

===2001 Metro Manila Film Festival===
*Won best picture
*Won best director for Chito S. Roño

===2001 FAMAS Awards===
*Won best actor for Armando Goyena
*Won best art Direction for Max Paglingawan and Fernan Santiago
*Won best special Effects for Roadrunner Network, Inc.
*Won best supporting Actor for Carlo Muñoz.
*Won best visual Effects for Roadrunner Network, Inc.

===Young Critics Circle, Philippines===
*Won best cinematography for Neil Daza
*Won best visual Design for Max Paglingawan and Fernan Santiago
*Won best achievement in Sound for Ross Diaz, Ronald de Asis and Albert Michael Idioma
*Won best achievement in Aural Orchestration for Kormann Roque and Nathan Brendholdt

==Controversies==

===MMFF===
When the film won the Best Picture award over the much favored film Bagong Buwan (a film about the military conflict in Muslim Mindanao directed by Marilou Diaz-Abaya), many moviegoers and critics were shocked over its selection for the top award as the movie was presented as an adventure film tackling the legend about the lost gold of Yamashita in the Philippines. For the most part, Yamashita was a fantasy and its subject matter was considered irrelevant to Filipinos.

===Pearl Harbor=== Pearl Harbor due to a scene on the film featuring Japanese fighter planes attacking a Philippine base which coincidentally happen to be very similar to one of the scene on Pearl Harbor. The scene was made using the most advanced visual effects technology available at that time.

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 