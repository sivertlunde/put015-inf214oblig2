Escort Girl (film)
{{Infobox film
| name           = Escort Girl
| image_size     =
| image          = Escort Girl FilmPoster.jpeg
| caption        =
| director       = Edward E. Kaye
| producer       = J.D. Kendis (producer)
| writer         = David Halperin (story and screenplay) Ann Halperin (story and screenplay)
| cinematography = Jack Greenhalgh 
| editing        = Holbrook N. Todd
| country        = United States}}

Escort Girl is a 1941 American film directed by Edward E. Kaye; it is also known as Scarlet Virgin.

==Plot summary==
Ruth Ashley (Betty Compson) is a former escort girl who now owns the Hollywood Escort Bureau in Los Angeles together with Gregory Stone (Wheeler Oakman). Ruth is co-owner of the Café Martinique, where the escorts go with their dates to watch striptease and drink champagne. The escort bureau is managed by Breeze Nolan (Guy Kingsford) who provides customers with both male and female escorts.

Ruth has a daughter, June (Margaret Marquis), who is ignorant of her mothers occupation. June believes that her mother works in real estate. Ruth had sent her daughter to a private school in the eastern U.S. Eventually June is engaged to Drake Hamilton (Robert Kellard), and when Drake travels to Los Angeles for work, June decides to accompany him to meet her mother. At the same time, Ruth and her partner learn  that the district attorney is planning to put an end to illegal escort services. Since her daughter is coming to town, Ruth is  concerned that the truth will come out about her business. She grows more worried when she discovers that Drake is in Los Angeles to assist the district attorney in stopping all escort operations.

In the line of duty Drake pretends to be a prospective client and calls the bureau to book a girl for the evening. His plan is to lay a trap for the bureau owners. But Stone learns from a snitch that Drake is investigating, and instead of sending a regular girl, he sends June to Drakes hotel room at the specified time. Drake ends up believing that June works as an escort girl and breaks their engagement. June is devastated and blames Stone for what happened. She  threatens to go to the DA. To stop her and exculpate himself, Stone reveals that Ruth is his partner. He tries to hire June as an escort girl after plying her with drinks.

Since Nolan is the face of the escort business, Drake finds him and beats him up, heartbroken because of what he thought about June. Drake learns that Stone is the owner. When Ruth arrives at Stones apartment, June is still there. June renounces her mother and leaves. Ruth learns what happened and tells Stone to reveal the truth to Drake, threatening to shoot him. Drake arrives, walking in on Ruth holding Stone at gunpoint. He tries to overpower her, and as they struggle for the gun, Ruth is accidentally shot and fatally wounded. As the struggle continues between Stone and Drake, Stone falls out a window and breaks his neck. With her last words, Ruth tells Drake the truth, and he promises to take care of June. Ruth dies, and Drake and June reconcile happily. 

==Cast==
*Betty Compson as Ruth Ashley
*Margaret Marquis as June Ashley
*Robert Kellard as Drake Hamilton
*Wheeler Oakman as Gregory Stone
*Guy Kingsford as Breeze Nolan
*Gay Seabrook as Maizie
*Isabelle LaMal as Snuggles
*Arthur Housman as Al, the Drunk
*Rick Vallin as Jack

==External links==
* 
* 

==References==
 

 
 
 
 
 