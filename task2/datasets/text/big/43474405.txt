Farm Relief
{{Infobox Hollywood cartoon|
| cartoon_name = Farm Relief
| series = Krazy Kat
| image = 
| caption = 
| director = Ben Harrison Manny Gould
| story_artist = 
| animator = 
| voice_actor = 
| musician = Joe de Nat
| producer = Charles Mintz
| studio = 
| distributor = Columbia Pictures
| release_date = December 30, 1929
| color_process = Black and white
| runtime = 7:28 English
| preceded_by = Sole Mates
| followed_by = The Cats Meow
}}

Farm Relief is an animated short subject produced by Columbia Pictures, featuring Krazy Kat. The film is also the characters fifth to employ sound after the studio made the transition less than a year ago.

==Plot==
Krazy lives in a barn with his farm animals. Whenever he has no farm work to do, he plays on his piano.

One day a pig in black coat and sunglasses comes to the farm. He then puts up a stand to sell liquor. His first customer is a cow. The cow, after drinking a few ounces, then heads to tell the other animals. They too are instrested as they flock to the stand.

Eventually, Krazy learns of this when he sees an intoxicated chicken. Though he reprimands that fowl for drinking, he also becomes interested when other animals come and offer him a bottle. And when he too drinks and gets intoxicated, Krazy celebrates by playing a song in his piano. The animals join his singing.

After playing his instrument, Krazy goes to collect milk from a cow. It turns out what he tries to milk is actually a large mutated rabbit. The angry mutated rabbit pushes Krazy back and smashes the pail on his head. A real cow shows up feeling sympathetic for Krazy.

==External links==
*  at the Big Cartoon Database

 

 
 
 
 
 
 
 
 
 


 