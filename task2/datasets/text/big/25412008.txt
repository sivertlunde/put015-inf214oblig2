David Garrick (1913 film)
 
  British actor David Garrick by T. W. Robertson, adapted by Max Pemberton. The film was directed by Leedham Bantock. 

== Plot summary==
The film is set in London in the 1740s where Ada Ingot (Ellaline Terriss), a young woman, is infatuated with the actor David Garrick (Seymour Hicks). Her love for Garrick is so strong that she refuses to accept a marriage arranged by her father, Mr. Ingot (William Lugg). Ingot meets with Garrick and initially tries to persuade him to leave the country or give up acting, but when Garrick learns the reason, he assures Ingot that he will be able to cure Ada of her attraction and asks Ingot to arrange a meeting. Garrick is sympathetic to Adas plight because he himself has fallen in love with a girl he doesnt know, but he promises her father that he will not make any romantic moves towards Ada.

== Cast ==
*David Garrick - Seymour Hicks
*Mr. Simon Ingot - William Lugg
*Mr. Alexander Smith - J. C. Buckstone
*Mr. Brown - Henry Kitts
*Mr. Jones - Lawrence Caird
*Ada Ingot - Ellaline Terriss
*Lord Fareleigh - Vincent Sternroyd
*Miss Araminta Brown - Nellie Dade

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 
 