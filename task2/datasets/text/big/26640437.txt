Wild Horse Mesa (1932 film)
{{Infobox film
| name           = Wild Horse Mesa
| image          = Wild_Horse_Mesa_1932_Poster.jpg
| border         = yes
| caption        = Theatrical release poster
| director       = Henry Hathaway
| producer       = Harold Hurley
| screenplay     = {{Plainlist|
* Harold Shumate
* Frank Howard Clark
}}
| based on       =  
| starring       = {{Plainlist|
* Randolph Scott
* Sally Blane
* Fred Kohler
}}
| music          = John Leipold (uncredited)
| cinematography = Arthur L. Todd
| editing        =
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 65 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Western film directed by Henry Hathaway and starring Randolph Scott, Sally Blane, and Fred Kohler.    Based on the novel Wild Horse Mesa by Zane Grey, the film is about an Arizona rancher who goes after a gang that is trapping and catching wild horses using barbed-wire enclosures. Wild Horse Mesa is a remake of the 1925 Paramount silent film of the same name.

==Cast==
* Randolph Scott as Chane Weymer
* Sally Blane as Sandy Melberne
* Fred Kohler as Rand
* Lucille La Verne as Ma Melberne
* Charley Grapewin as Sam Bass
* James Bush as Bent Weymer 
* Jim Thorpe as Indian Chief 
* George "Gabby" Hayes as Slack 
* Buddy Roosevelt as Horn 
* E.H. Calvert as Sheriff   

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 


 