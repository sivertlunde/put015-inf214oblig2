Tomorrow's Children
{{Infobox film
| name           = Tomorrows Children
| image          = 
| image_size     = 
| caption        = 
| director       = Crane Wilbur
| producer       = Bryan Foy (producer)
| writer         = Wallace Thurman (story) Wallace Thurman and Crane Wilbur
| narrator       = 
| starring       = See below
| music          = 
| cinematography = William C. Thompson
| editing        = 
| studio         = 
| distributor    = 
| released       = 1934
| runtime        = 70 minutes
| country        = USA
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

Tomorrows Children is a 1934 American film directed by Crane Wilbur which protests against the eugenic policies then in force in many states. The film was deemed "immoral", "tending to corrupt morals", and "tending to incite crime".   

The film is also known as The Unborn in the United Kingdom.

== Summary ==
 
Tomorrows Children follows the nature vs. nurture story of Alice Mason, starring Diane Sinclair, who wants nothing more than to settle down with her fiance, Jim, and raise their prospective family. All those goals crumble when her parents, Mr. and Mrs. Mason, are forced into sterilization, or they must give up the welfare checks. Due to her parents alcoholism and less than stellar discipline, her siblings are physically disabled, mentally unstable, or have criminal ties. Although Alice represents the only real beneficial member of the family, she has been ordered to sterilize herself along with her relatives, thus ending their further corrupt offspring.

Rather than accepting with the court order with the rest of the Mason family, Alice flees the house before giving an answer. She is later caught and taken by the police. Her fiance, Jim, makes a bold case to Dr. Brooks, to which he aids them by testifying on Alices behalf. The courts decision was unchanged. Another ally of Alice and Jim is Father OBrien, played by director Crane Wilbur. OBrien is talking to Mrs. Mason, begging her to reconsider the decision to sterilize. She refuses and decides to keep the welfare income. However, in a drunken haze, it is revealed that she was not Alices biological mother. Father OBrien races to stop the procedure with the new information. Dr. Brooks is ultimately able to stop the procedure in time.


== Cast ==
*Diane Sinclair as Alice Mason Donald Douglas as Dr. Brooks John Preston as Dr. Crosby
*Carlyle Moore Jr. as Jim Baker
*Sterling Holloway as Dr. Dorsey
*W. Messenger Bellis as Dr. McIntyre
*Hyram A. Hoover as Spike
*Constance Kent as Nurse
*Lewis Gambart as Jeff
*Crane Wilbur as Father OBrien
*Arthur Wanzer as Mr. Mason
*Sarah Padden as Mrs. Mason

== Struggle for License ==
The film industry revolves around certain formulas and general rules for what was okay to talk about. Some producers tested and pushed the limits of society to promote new ideas, however, those who create these hard pressing issues are not members of the Association of Motion Picture Producers. Instead of being backed by this organization, the Foy Productions was forced to present the film to state censorship boards located in New York, Ohio, Kansas, Pennsylvania, Virginia, and Maryland.

This film received a lot of negative attention for its prominent themes of genetic alcoholism, deformed offspring, and sterilization. The film was only approved in Pennsylvania and Ohio.   Since Tomorrow’s Children deliberately ignored the standard rules of the industry, of leaving these controversial topics alone, the film was denied its license and there was a lot of effort devoted to the delaying of its production. The Producers’ Association was responsible for most of the obstacles in the film’s course. 

The original decision to ban the film came from Censor Irwin Esmond and Dr. Frank Graves in the State Education department. The Applelate Division of the Supreme Court backed Esmond and Graves with 3 votes in favor of the ban and 2 against it. Years later in 1938, Foy Productions urged the US appeals court to revisit “Tomorrow’s Children”, especially in New York. Frederick Crane, of the appeals court, screened the film along with 6 others to decide its future in the empire state.  

== Eugenics Around the World ==
The film has a very prominent theme of sterilization, taking away the ability to reproduce and thus ending parentage and future offspring. In the early 20th century, the US was flooded with ideals revolving eugenics. Eugenics is the idea that through selective breeding and sterilization, we would grow as a population and remove the seemingly negative traits. By this selective process, we would essentially be able to create offspring who all resemble each other. 

In 1927 US declared that it is in favor of these eugenic processes. Supreme Court Justice, Oliver Wendell Holmes, wrote, “...society can prevent those who are manifestly unfit from continuing their kind.” This quote would be later used against the US during the Nazi trials. Because the film is a reflection on the evils of society, it went against the status quo and took a stand. Criticizing sterilization and eugenic activities meant also criticizing the standard thought in American culture. 

Barely a month after Tomorrow’s Children was released, one of history’s most infamous eugenic icons, Adolf Hitler became the recognized sole-leader of Germany on August 19, 1934. As Hitler began his conquest through Germany, sterilizing the bloodlines of different races and religions of people. Hitler’s ideals revolved around humanity becoming its purest by removing the parts he deemed unworthy. Although Hitler began his eugenic practices in 1936, Foy and Wilbur were able to relate the same message through the sterilization of Diane Sinclairs family in Tomorrow’s Children. Foy Productions appealed its denied license in 1938 while the Nazi Aryan movement was still gaining power, thus resulting the films extremely unfit theme.    

== External links ==
* 
* 

== References ==
 

 
 
 
 
 
 
 
 
 
 


 