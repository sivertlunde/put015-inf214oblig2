Account Rendered (1932 film)
{{Infobox film
| name =  Account Rendered 
| image =
| image_size =
| caption =
| director = Leslie Howard Gordon
| producer = 
| writer =  Michael Joseph (play)   Leslie Howard Gordon
| narrator =
| starring = Cecil Ramage   Reginald Bach
| music = 
| cinematography =  Desmond Dickinson 
| editing = 
| studio = Producers Distributing Corporation 
| distributor = Producers Distributing Corporation
| released = 11 July 1932 
| runtime = 35 minutes
| country = United Kingdom English
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} silent crime film directed by Leslie Howard Gordon and starring Cecil Ramage, Reginald Bach and Marilyn Mawn. It was made as a quota quickie at Cricklewood Studios. 

==Cast==
*  Cecil Ramage as Barry Barriter 
* Reginald Bach as Hugh Preston 
* Marilyn Mawn as Barbara Wayne 
* Jessie Bateman as Mrs. Wayne 
* Frederick Moyes as General Firmstone 
* Hubert Leslie as Parsons  Ronald Ritchie as Jim  Arthur Prince as Lawyer

==References==
 

==Bibliography==
*Chibnall, Steve. Quota Quickies: The Birth of the British B Film. British Film Institute, 2007.
*Low, Rachael. Filmmaking in 1930s Britain. George Allen & Unwin, 1985.
*Wood, Linda. British Films, 1927–1939. British Film Institute, 1986.

==External links==
*  

 
 
 
 
 
 
 

 