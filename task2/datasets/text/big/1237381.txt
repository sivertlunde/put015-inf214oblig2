Blackmail (1929 film)
  
{{Infobox film
| name           = Blackmail
| image          = Blackmail 1929 Poster.jpg
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| director       = Alfred Hitchcock
| producer       = John Maxwell
| screenplay     = {{Plainlist|
* Alfred Hitchcock  
* Benn W. Levy  
}}
| story          = 
| based on       =  
| narrator       = 
| starring       = {{Plainlist|
* Anny Ondra
* John Longden
* Cyril Ritchard
}}
| music          = Jimmy Campbell and Reg Connelly Hubert Bath (arrangements) Billy Mayerl (song: "Miss Up-to-Date")
| cinematography = Jack E. Cox
| editing        = Emile de Ruelle
| studio         = 
| distributor    = {{Plainlist| BIP  
* Sono Art-World Wide Pictures  
}}
| released       =  
| runtime        = 84 minutes  
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
 thriller drama Charles Bennett, the film is about a London woman who is blackmailed after killing a man who tries to rape her.

After starting production as a silent film, British International Pictures decided to convert Blackmail into a sound film during filming. A silent version was released for theaters not equipped for sound (at 6740 feet), with the sound version (7136 feet) released at the same time.  The silent version still exists in the British Film Institute collection.   

==Plot==
Scotland Yard Detective Frank Webber (John Longden) escorts his girlfriend Alice White (Anny Ondra) to a tea house. They have an argument and Frank storms out. While reconsidering his action, he sees Alice leave with Mr. Crewe (Cyril Ritchard), an artist she had earlier agreed to meet.

Crewe persuades a reluctant Alice into coming up to see his studio. She admires a painting of a laughing clown, and uses his palette and brushes to paint a cartoonish drawing of a face; he adds a few strokes of a feminine figure, and they both sign the "work".  He gives her a dancers outfit and Crewe sings and plays "Miss Up-to-Date" on the piano.

Crewe steals a kiss, to Alices disgust, but as she is changing and preparing to leave, he takes her dress from the changing area.  He attempts to rape her; her cries for help are not heard on the street below. In desperation, Alice grabs a nearby bread knife and kills him.   She angrily punches a hole in the painting of the clown, then leaves after attempting to remove any evidence of her presence in the flat, but forgets to take her gloves. She walks the streets of London all night in a daze.

When the body is found, the killing is assumed to be murder. Frank is assigned to the case and finds one of Alices gloves. He recognizes both the glove and the dead man, but conceals this from his superior. Taking the glove, he goes to speak with Alice at her fathers tobacco shop, but she is still too distraught to speak.

But as they hide from her father in a telephone booth, Tracy (Donald Calthrop), the model for the clown, arrives. He had seen Alice go up to Crewes flat, and gone in and taken one of the gloves. When he sees Frank with the other one, he attempts to blackmail the couple. His first demands are petty ones and they accede. Then Frank learns by phone that Tracy is wanted for questioning: he was seen near the scene and has a criminal record. Frank sends for policemen and tells Tracy he will pay for the murder.
 Reading Room, but slips, crashes through a skylight, and falls to his death inside. The police assume he was guilty of murder.

Unaware of this, Alice finally feels compelled to give herself up and goes to New Scotland Yard. She writes a confession letter and goes to see the Chief Inspector. Before she can bring herself to confess, the inspector receives a telephone call and asks Frank to deal with Alice. She finally tells him the truth—that it was self-defense against an attack she cannot bear to speak of—and they leave together. As they do, a policeman walks past, carrying the damaged painting of the laughing clown and the canvas where Alice has painted over her name and Crewes.

==Cast== dubbing capacity, her dialogue was simultaneously recorded offscreen by actress Joan Barry. Ondras British film career was over. ]]
  
* Anny Ondra as Alice White
* Sara Allgood as Mrs. White
* Charles Paton as Mr. White
* John Longden as Detective Frank Webber
* Donald Calthrop as Tracy
* Cyril Ritchard as Mr. Crewe, an artist
* Hannah Jones as the landlady
* Harvey Braban as the Chief Inspector    
 
* Johnny Ashby as boy   Joan Barry as Alice White  
* Johnny Butt as Sergeant  
* Phyllis Konstam as gossiping neighbour  
* Sam Livesey as the Chief Inspector  
* Phyllis Monkman as gossip woman  
* Percy Parsons as crook  
 

==Production==
The film began production as a silent film. To cash in on the new popularity of talkies, the films producer, John Maxwell of British International Pictures, gave Hitchcock the go-ahead to film a portion of the movie in sound. Hitchcock thought the idea absurd and surreptitiously filmed almost the entire feature in sound (the opening 6½ minutes of the sound version are silent, with musical accompaniment, as are some shorter scenes later), along with a silent version for theatres not yet equipped for talking pictures.
 Lights of New York, was released in July 1928 by Warner Brothers in their Vitaphone sound-on-disc process.) The film was shot at British and Dominions Imperial Studios soundstage in Borehamwood, the first purpose-built sound studio in Europe.   

  Czech accent Joan Barry was hired to actually speak the dialogue off-camera while Anny lip-synched them for the film. This makes Ondras performance seem slightly awkward.

Hitchcock used several elements that would become Hitchcock "trademarks" including  a beautiful blonde in peril and a famous landmark in the finale. Without informing the producers, Hitchcock used the Schüfftan process to film the scenes in the Reading Room of the British Museum since the light levels were too low for normal filming.

On this production, future directors Ronald Neame worked as a "clapper boy" operating the clapperboard and Michael Powell took still photographs. 

The film was a critical and commercial hit.  The sound was praised as inventive. A completed silent version of Blackmail was released in 1929 shortly after the talkie version hit theaters. The silent version of Blackmail actually ran longer in theaters and proved more popular, largely because most theaters in Britain were not yet equipped for sound.  Despite the popularity of the silent version, history best remembers the landmark talkie version of Blackmail. It is the version now generally available although some critics consider the silent version superior.  Alfred Hitchcock filmed the silent version with Sam Livesey as the Chief Inspector and the sound version with Harvey Braban in the same role.

==Hitchcocks cameo== Alfred Hitchcocks cameo, a signature occurrence in many of Hitchcocks films, shows him being bothered by a small boy as he reads a book on the London Underground. The small boy was Jacque Carter.  This is probably the lengthiest of Hitchcocks cameo appearances in his film career, appearing from 10 min. 24 sec to 10 min. 44 sec.  As the director became better-known to audiences, especially when he appeared as the host of his own television series, he dramatically shortened his on-screen appearances.

==Role in UK film history==
As an early "talkie", the film is frequently cited by film historians as a landmark film,  and is often cited as the first truly British "all-talkie" feature film.  

Earlier British sound films include:
*The Gentleman, a short film in the Phonofilm sound-on-film process, was an excerpt of The 9 to 11 Revue, directed by William J. Elliott, and released in the UK in June 1925; The Clue of the New Pin, based on the novel by Edgar Wallace, and filmed in British Phototone, a sound-on-disc system using 12-inch discs; The Crimson Circle, a UK-German silent film, also based on a Wallace novel, dubbed after the fact with the Phonofilm sound-on-film process;
*Black Waters, a British all-talkie production shot in the US and released on 6 April 1929. 

In March 1929, Pin and Circle were trade-shown at the same screening for film exhibitors in London.
 The Constant Rookery Nook The Middle Watch (1931), and Sunshine Susie (1932). 

==References==
Notes
 

Bibliography
*Ryall, Tom, Blackmail (London: British Film Institute, 1993)

==External links==
*  
*  
*  
*  
*  
*  
*  
*  
*  
*   at Library of Congress;  select "Document number" and type "V8003P432" 

 

 
 
 
 
 
 
 
 
 