The Nomi Song
{{Infobox film
| name           = The Nomi Song
| image          = Nomi Song.jpg
| image size     =
| alt            = 
| caption        = Theatrical release poster Andrew Horn
| producer       = 
| writer         = Andrew Horn
| screenplay     = 
| story          = 
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 96 minutes (98 outside of Germany)
| country        = Germany
| language       = German and English
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}
 documentary about Andrew Horn.  The film debuted at the Berlin International Film Festival in February 2004,  where it won a Teddy Award for "Best Documentary Film." 

==Structure== East Village the Mumps, and David Bowie.

==Critical reception==
In his review, Entertainment Weeklys Owen Gleiberman described the documentary as "loving and meticulous", giving the film an A-.  MTVs Kurt Loder dubbed it "strange and fascinating".  In its review of the film, The Advocate called it "engaging",  indicating at the DVD release that "New Wave countertenor Klaus Nomi gets his posthumous due in this acclaimed doc." 

==DVD features==
The DVD includes numerous bonus features, including remixes of Nomi songs by Richard Barone, Ana Matronic of the Scissor Sisters, The Moog Cookbook, and Man Parrish.

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 

 
 