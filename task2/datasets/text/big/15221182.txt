Yangsan Province
{{Infobox film name           = Yangsan Province image          = Yangsan do poster.jpg caption        = Korean poster for Yangsan Province (1955) director       = Kim Ki-young  producer       = Byeon Sun-je writer         = Lee Tae-hwan Kim Ki-young starring       = Kim Sam-hwa Cho Yong-soo music          = Seong Gyeong-rin cinematography =  editing        = Kim Ki-young distributor    = Seorabyol Public Films released       =   runtime        = 90 minutes country        = South Korea language       = Korean budget         =  gross          = 
| film name = {{Film name hangul         =   hanja          =   rr             = Yangsando mr             = Yangsando  context        = }}
}} 1955 South Korean film directed by Kim Ki-young.

==Synopsis==
The film is a historical melodrama about a high government official who wants to marry a woman who is engaged to marry another man. 

==Cast==
* Kim Sam-hwa... Ok-rang 
* Cho Yong-soo... Su-dong
* Kim Seung-ho... Ok-rangs father
* Park Am... Mu-ryeong
* Go Seon-ae... Su-dongs mother
* Ko Seol-bong
* Lee Gi-hong
* Go Il-yeon
* Choe Ryong
* Lee Yeong-ok

==Notes==
 

==Bibliography==
*  
*  
*  
*  

 

 
 
 
 

 