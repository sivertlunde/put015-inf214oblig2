Good Burger
 
{{Infobox film
| name           = Good Burger
| image          = Good Burger film poster.jpg
| caption        = Theatrical release poster
| director       = Brian Robbins Mike Tollin Brian Robbins Dan Schneider Kevin Kopelow Heath Seifert
| starring       = Kel Mitchell  Kenan Thompson Abe Vigoda 
| music          = Stewart Copeland
| cinematography = Mac Ahlberg
| editing        = Anita Brandt-Burgoyne
| studio         = Nickelodeon Movies Tollin/Robbins Productions
| distributor    = Paramount Pictures
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         = $9 million
| gross          = $23.7 million 
}} Good Burger" featured on the Nickelodeon series All That. The film was produced by Tollin/Robbins Productions and Nickelodeon Movies and released on July 25, 1997 by Paramount Pictures.

==Plot==
 
On the first day of summer, dim-witted but kind-hearted Ed (Kel Mitchell) experiences a nightmare featuring talking burgers. Troubled by it, he shows up late to work at Good Burger. On the same morning, Dexter (Kenan Thompson), a high school student, steals his mothers car after she leaves for a business trip. He accidentally crashes into that of his teacher, Mr. Wheat (Sinbad (entertainer)|Sinbad). With no drivers license or vehicle insurance|insurance, he is in danger of going to jail. Mr. Wheat agrees to allow him to pay for a total of $1,900 in car damage, which later becomes $2,500, instead of calling the police. He must find a summer job to pay for the expenses. He first finds employment at Mondo Burger, but after a clash with the strict manager, Kurt Bozwell (Jan Schweiterman), he is fired and has to find employment at another place, and manages to do so at Good Burger. There, he meets and reluctantly befriends Ed (who helps him find a job position) and a slew of colorful employees. Little does he know Ed inadvertently caused the car accident; he was rushing to make a delivery on rollerblades, and skated in front of Dexter, causing him to swerve and hit Mr. Wheats car.

Dexter finds much success at Good Burger, but his success takes a turn for the worse. Across the street, the Mondo Burger where he previously worked opens to the public. It produces oversized burgers, much bigger than the Good Burgers, and threatens to put them out of business. Fortunately, with the invention of Eds "secret sauce", they are saved. After Dexter realizes that Ed caused his car accident, he takes part of Eds bonus paychecks earned from the sauce to pay off his debt to Mr. Wheat.

Good Burger continues to be popular because of Eds sauce, causing Kurt to attempt to steal it. First, he entices Ed with a higher hourly wage at Mondo Burger. Dedicated to Good Burger, he denies it. He buys Dexter a similar yo-yo that his dad bought him as a kid. It was at this moment that he not only begins to truly care about Ed, but he is also overcome with guilt for taking most of Eds bonus money. Kurt then sends in an attractive employee named Roxanne (Carmen Electra) who fails to seduce Ed into divulging his "secret sauce" recipe, ending up badly injured and resigning from her job.

Meanwhile, Dexter takes a shine to coworker Monique (Shar Jackson), and she eventually agrees to a double date with him along with Ed and Roxanne. The next day, however, she tells him off and breaks up with him after she discovers his attempts to cheat Ed out of most of his money.

Later on, Ed and Dexter encounter a dog on the street and attempt to feed him a Good Burger and a Mondo Burger. It eats the former, but refuses to even look at the latter, apparently sensing something wrong with the meat. Suspicious, Ed and Dexter disguise themselves as old women and enter Mondo Burger to find out what is wrong with their meat. They discover that they chemically induced their burgers with an illegal food additive called "Triampathol," which makes them oversized. Kurt is not fooled by Ed and Dexters disguises and kidnaps them by sending them to Demented Hills Mental Hospital (run by his friend) to prevent the public from learning Mondo Burgers secret.

After sending Ed and Dexter to Demented Hills, Kurt and his employees break into Good Burger after closing hours and spike Eds "secret sauce" with shark poison. When Ed and Dexters coworker, Otis (Abe Vigoda), attempts to call the police on them, Kurt takes him captive and sends him to Demented Hills as well. There, he tells Ed and Dexter about Kurts scheme to poison the "secret sauce." They devise a plan to escape. Ed begins to dance and sing, encouraging the other patients to join in. After the entire mental hospital breaks out in dance, the trio escapes. They steal an ice cream truck to leave, but are pursued by a Demented Hills van; cleverly, Ed and Dexter throw ice cream treats at it, causing it to crash. Upon arriving at Good Burger, Ed manages to prevent a lady from eating a Good Burger with the poison-laced sauce just in time. Dexter then informs the staff that the sauce has been poisoned by Kurt and convinces them not to let anyone consume it. Ed and Dexter then break into Mondo Burger so they could expose the chemically induced burgers to the police. While Dexter provides a distraction, Ed tries to take a can of Triampathol, but accidentally knocks one into the meat grinder. Ed then decides to pours the rest of it into there. On the roof, Kurt catches Ed and Dexter and confiscates the empty can that Ed is holding. Just then, the building begins to blow up, caused by the Triampathol-overdosed burgers exploding. After it destructs and the patrons flee, the police arrest Kurt for illegal use of a substance. Ed then explains to Dexter that he did that on purpose to prevent Kurt from manipulating the legal system and escaping conviction, ironically responding to Dexters questions by saying, "Im not stupid." With the destruction of Mondo Burger, Mr. Wheats car is destroyed once again (much to Dexters amusement as he hands him the first half of the money owed for the damage).

Dexter apologizes to Ed for taking advantage of the latters salary from the sauce and both end up on friendly terms. They then walk back to Good Burger, where the employees praise them, especially Ed, as Good Burger heroes. The film ends as Ed proudly says "Welcome to Good Burger, home of the Good Burger. Can I take your order?"

==Cast==
* Kel Mitchell as Ed, the cashier of Good Burger.
* Kenan Thompson as Dexter Reed, a high school student who desires to slack during his summer vacation.
* Abe Vigoda as Otis, an elderly Good Burger employee who cooks the French fries.
* Jan Schweiterman as Kurt Bozwell, the C.E.O. and restaurant owner of Mondo Burger who will stop at nothing to make his food chain number one. Sinbad as Mr. Wheat, a teacher who demands money from Dexter for car damage.
* Shar Jackson as Monique, a female Good Burger employee who scolds Dexter for using Eds gullibility to steal most of his money, but eventually becomes his girlfriend. Dan Schneider as Mr. Baily, the owner and manager of Good Burger.
* Ron Lester as Spatch, the head fry cook of Good Burger.
* Lori Beth Denberg as Connie Muldoon, a customer whose extremely complex orders are too difficult for Ed to memorize.
* Josh Server as Fizz, the drive-thru employee of Good Burger.
* Ginny Schreiber as Deedee, one of the 2 female employees at Good Burger and a vegetarian.
* Linda Cardellini as Heather, an insane girl in Demented Hills that has feelings for Ed.
* Shaquille ONeal as Himself George Clinton as Dancing Crazy, a Demented Hills patient.
* Robert Wuhl as an Angry Customer
* Carmen Electra (uncredited) as Roxanne, a henchwoman of Kurt who tries, but fails, to seduce Ed into telling his secret sauce recipe.
* Marques Houston as Jake, Dexters high school friend.
* J. August Richards as Griffen, one of Kurts right hand men.
* Hamilton Von Watts as Troy, Kurts other right-hand man
* Wendy Worthington as the Demented Hills nurse.

==Filming==
Most of the films scenes were filmed along S Glendora Avenue in West Covina, California in 1996.  The building known as "Good Burger" in the movie was filmed at a restaurant currently known as "Peters El Loco" 437 Glendora Ave., West Covina, CA. Meanwhile, Mondo Burger was located across the street at the Samantha Courtyard shopping center, with extra details added to the facade for the film.

==Soundtrack==
 
A soundtrack containing hip hop, R&B, funk and rock music was released on July 15, 1997 by Capitol Records. It peaked at 101 on the Billboard 200|Billboard 200 and 65 on the Top R&B/Hip-Hop Albums.

==Release==
===Short film===
The Action League Now! episode Rock-a-Big Baby was released prior to the films screening. It was rated PG "for some risqué humor."

===Box office===
In its opening weekend, Good Burger grossed $7.1 million, finishing in 5th at the box office. It went on to gross $23.7 million. http://www.boxofficemojo.com/movies/?id=goodburger.htm 

===Critical reception===
Rotten Tomatoes gives the film a score of 33% based on reviews from 27 critics.  Most praise came to Kel Mitchell and Kenan Thompsons performances.

Lisa Alspector of Chicago Reader gave the film a negative review, and wrote "The received notion that kids want their movies fast and furious is barely in evidence in this 1997 comedy, a laboriously slow suburban adventure in which a teenagers summer of leisure slips through his fingers when he has to get a job—an experience that proves almost life threatening because of the cutthroat competition between two burger joints." 
 well done, but it does have energy." 

Leonard Klady of Variety (magazine)|Variety enjoyed the film and wrote "The meat of the piece is definitely FDA cinematically approved, and perfect if you like this brand of entertainment with the works." 

Roger Ebert of the Chicago Sun-Times wrote "It didnt do much for me, but I am prepared to predict that its target audience will have a good time." He gave the film two out of four stars.   

===Home media===
Paramount released the film on VHS on February 17, 1998 and on DVD on May 27, 2003. Warner Home Video (who releases Paramount titles on DVD and Blu-ray under license, as Paramount themselves have moved to digital-only distribution) re-issued the film on DVD on September 24, 2013.

==References==
 

==External links==
 
*  
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 