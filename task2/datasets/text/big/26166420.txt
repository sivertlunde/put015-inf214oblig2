The Love War
{{Infobox Film
| name           = The Love War 
| image          = 
| image_size     = 
| caption        = 
| director       = George McCowan
| producer       = Aaron Spelling
| writer         = David Kidd Guerdon Trueblood
| narrator       = 
| starring       = Lloyd Bridges Angie Dickinson
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = March 10, 1970
| runtime        = 74 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
The Love War (1970) is a science fiction ABC Movie of the Week starring Lloyd Bridges as an alien warrior and Angie Dickinson as the woman he befriends.

==Plot==
Two warring planets choose to settle their conflict over which of them will take over the planet Earth, each sending a trio of soldiers to Earth to fight to the death. The combatants, disguised as human beings, can only identify each other by using special visors.

Kyle (Lloyd Bridges), one of the combatants, falls in love with Sandy (Angie Dickinson), a woman he meets during his stay in a small town. In the end, despite cheating by the other side, Kyle is the sole survivor. But before he can signal his people he has won, Sandy shoots him with one of the alien weapons.  A dying Kyle then learns that Sandy is also an alien; the other side has cheated twice. She chose duty to her people over her love for him. Weeping as she watches him die, she asks him what their half-breed children would have been. The film’s closing shot shows Sandy through the visor as she really is: a hideously scarred humanoid. The Earth faces an orgy of destruction and the extermination of humanity.

The setting is Kansas, as revealed by a dilapidated sign during the shootout bearing the words "Spivey, Kansas".

==Cast==
*Lloyd Bridges as Kyle
*Angie Dickinson as Sandy
*Harry Basch as Bal
*Daniel J. Travanti as Ted (as Dan Travanty)
*Allen Jaffe as Hort
*Bill McLean as Reed
*Byron Foulger as Will
*Pepper Martin		
*Bob Nash as Limo Driver
*Art Lewis

==Home media==
It was released on VHS by Guild Home Video. 

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 


 