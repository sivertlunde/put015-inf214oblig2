Cathy's Child
 
 
{{Infobox film
| name           = Cathys Child
| image          =
| caption        =
| director       = Donald Crombie
| producer       = Pom Oliver Errol Sullivan
| writer         = Ken Quinnell
| based on       = book A Piece of Paper by Dick Wordley
| narrator       =
| starring       = Michele Fawdon Alan Cassell Bryan Brown Arthur Dignam Willie Fennell
| music          = William Motzing
| cinematography = Gary Hansen
| editing        = Tim Wellburn
| distributor    = Village Roadshow
| released       =  
| runtime        = 89 minutes
| country        = Australia
| language       = English
| budget         = A$325,000 David Stratton, The Last New Wave: The Australian Film Revival, Angus & Robertson, 1980 p152-153 
| gross          = A$135,000 (Australia)  $100,000 (overseas sales) Best Actress in a Leading Role Australian Film Institute Awards
}}

Cathys Child is a 1979 Australian film, directed by Donald Crombie and starring Michele Fawdon, Alan Cassell and Bryan Brown.

==Plot==
Cathy Baikas is a woman of Greek heritage who lives in Sydney, Australia with her three-year-old daughter. When her daughters father kidnaps the child and takes her back to Greece, Cathy discovers the authorities can do little to help her. She turns to the media. The editor of a major daily newspaper proves sympathetic to Cathys problem and begins giving her case press coverage, because the same situation had happened to him. The film is based on a true story.

==Historical Basis==
On 14 January 1973 Greek born John Baikas left Australia for Athens, taking his daughter Maris with him on a forged passport. Her mother Cathy found out and tried to get her back. The government seemed to do little so she contacted journalist Dick Wordley to run a campaign.

The film used the real names for the characters of Cathy Baikas, Dick Wordley and Wordleys editor Paul Nicholson. However other names were fictionalised. Peter Beilby & Scott Murray, "Donald Crombie", Cinema Papers, Oct-Nov 1978 p131-132 

==Production==
Dick Wordleys book on the case was read by Ken Quinnell. He gave it to producer Errol Sullivan who thought it might make "a small but highly emotional film, one that could reach the middle audience in Australia - the audience that people like Hoyts say doesnt exist: namely, the North Shore, blue rinse set." Peter Beilby & Scott Murray, "Errol Sullivan", Cinema Papers, Oct-Nov 1978 p128 

It was thought the budget had to be kept below $400,000 so the action was set in the present day rather than 1973.  Finance from the Australian Film Commission, the New South Wales Film Corporation, Roadshow Distributors and $55,000 of private investment. Gillian Armstrong was originally meant to be director and money was raised from the AFC on the basis of her name, but there was a potential clash with My Brilliant Career so Donald Crombie was offered the job instead; Crombie had a history of making films about women. 

Filming started on June 1978, with the majority of the film shot in Sydney over four weeks, with a weeks filming in Greece. Money had been allocated in the budget for an overseas actor to play the Australian consul in Greece but the filmmakers were unable to find any one for an appropriate price and Willie Fennell took the role. 

The script included a scene where Cathy and Dick Wordley go to bed together. Wordley denied this ever happened but allowed it in the film after much discussion.  The scene was shot but ended up being cut after a preview. 

==Reception==

===Awards=== Best Actress in a Leading Role at the 1979 Australian Film Institute Awards for her role as Cathy Baikis.
 Best Actor Best Direction Best Film at the same awards.

===Box Office===
Cathys Child grossed $135,000 at the box office in Australia,  which is equivalent to $527,850
in 2009 dollars.

In 1996 Donald Crombie said the film was his favourite of all the features he had made:
 Mainly because I think we were extraordinarily successful in creating that character, Cathy Bikos. Michelle Fawdon is obviously not Maltese, but she pulled that off brilliantly, I thought. The accent, Im told, is perfect. She lived with a family and thats how she achieved it. That was a very good project to work on.  

==See also==
*Cinema of Australia

==References==
 

==External links==
* 
*  at Oz Movies
* 
* 

 

 
 
 
 
 