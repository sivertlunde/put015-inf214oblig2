Kawa (film)
{{Infobox film
| name = Kawa
| image = Kawa-film-poster.jpg
| caption = Original film poster
| director = Katie Wolfe
| writer = Kate McDermott
| based on =  
| starring = Calvin Tuteao  Dean OGorman  Nathalie Boltt   George Henare   Vicky Haughton
| producer = 
| music = Joel Haines
| cinematography = Fred Renata
| editing = Lisa Hough
| distributor = 
| released =  
| runtime = 
| country = New Zealand
| language = English  Māori language
| budget = 
| gross = 
}}
Kawa is a 2010 New Zealand film directed by Katie Wolfe originally titled Nights in the Gardens of Spain. The film stars Calvin Tuteao as Kawariki. A coming out film drama, it is based on the novel Nights in the Gardens of Spain by Witi Ihimaera.

==Plot== gay ever since childhood. He has been desperately fighting his feelings for years, but now frequents gay baths and he has a love affair with Chris (Dean OGorman).

Things get more complicated and acquire more urgency when his mother Grace (Vicky Haughton) discovers him kissing Chris on the beach after a fight between the two and decides to expel him from her house. When he finally comes out to his father and his wife, it is too late for Chris who has moved in with another partner after waiting for so long for him. 

Kawa has to quit home, but he is torn with his love for his son Sebastian (Pana Hema Taylor) and his daughter Miranda (Miriama-Jane Devantier). Meanwhile his daughters absolute attachment to him is the catalyst for the family coming back together. 

The film ends with a scene in which Kawas father is reconciled with himself and visits his son at his new residence to deliver him the traditional Maori emblem.

==Accolades==
During the 2011 New Zealand Film and TV Awards, Dean OGorman was nominated for the award for "Best Performance by a Supporting Actor" for his role as Chris in the film.

==Cast==
*Calvin Tuteao as Kawariki
*Nathalie Boltt as Annabelle
*George Henare as Hamiora
*Vicky Haughton as Grace
*Dean OGorman as Chris
*Pana Hema Taylor as Sebastian
*Miriama-Jane Devantier as Miranda
*Geoffrey Snell as Walter
*Jarod Rawiri as Wayne
*Jack Walley as Coach
*Bridget Pyc as Emily

==References==
 

==External links==
*  

 
 
 
 
 
 