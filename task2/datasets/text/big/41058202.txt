Leave All Fair
 
 
{{Infobox film
 | name = Leave All Fair 
 | image = 
 | caption = 
 | director = John Reid
 | producer = John OShea 
 | writer = Stanley Harper   Maurice Pons   Jean Betts   John Reid  
 | starring = John Gielgud Jane Birkin 
 | music = 
 | cinematography = 
 | editing = 
 | distributor =
 | released = 1985 
 | runtime = 90 min. English
 | budget =
 | gross = 
 }}
Leave All Fair is a 1985 New Zealand made film starring John Gielgud as John Middleton Murry the husband of Katherine Mansfield. He is presented as a sanctimonious exploiter of her memory, who ill-treated her during their association. Jane Birkin plays both Mansfield in flashbacks and the fictitious Marie Taylor who finds a letter from the dying Mansfield to Murry in his papers.

The theme was developed by New Zealand director Stanley Harper, but he was fired two weeks before shooting, and John Reid took over the project, introducing the "ghost" element and the two time frames. Shot in France at Moulin dAnde and St Pierre du Vauvray with finance raised by Pacific Films, the film had to be finished before the 1984 cut-off date for New Zealand tax breaks.

==Plot==
The film is set in France in 1956, 33 years after the death of Mansfield and a year before Murry’s own death. Murry visits André de Sarry a (fictional) French publisher who is about to publish an edition of her collected letters and journals. Murry is presented as struggling with his conscience as he recalls the ill and alone Mansfield (seen in flashbacks), and decides to publish almost all of her work.

de Sarry’s New Zealand partner Marie Taylor reads Mansfield’s work and among Middleton Murry’s papers finds a letter to him from the dying Mansfield. She confronts him as "another exploitative male" who makes a sanctimonious speech at the book launch. The letter from Mansfield says (rather ambiguously):

::I should like him to publish as little as possible …He will understand that I desire to leave as few traces of my camping ground as possible .... All my manuscripts I leave entirely to you to do what you like with .... Please destroy all letters you do not wish to keep and all papers .... Have a clean sweep.... and leave all fair, will you.

==Film reception==
Helen Martin says the film is "beautifully shot in the European tradition" and was described at the London Film Festival as "arguably the best film to come out of New Zealand so far", but others criticised the portrayal of Middleton Murray as "simplifying the KM/Murry relationship into a cliché". Variety said it was "an affecting experience".

==Cast==
* John Gielgud as John Middleton Murry 
* Jane Birkin as both Katherine Mansfield and Marie Taylor 
* Feodor Atkine as André de Sarry 
* Simon Ward as young John (John Jeune) 
* Louba Guertchikoff as Lisa
* Maurice Chevit as Alain 
* Mireille Alcantara as Violetta 
* Leonard Pezzino as Alfredo

==References==
*New Zealand Film 1912-1996 by Helen Martin & Sam Edwards p116 (1997, Oxford University Press, Auckland) ISBN 019 558336 1

==External links==
*  
*   
*  

 
 
 
 
 
 
 
 