The Thomas Crown Affair (1968 film)
{{Infobox film
| name           = The Thomas Crown Affair
| image          = Crown_A.jpg
| caption        = Theatrical release poster
| director       = Norman Jewison
| producer       = Norman Jewison
| writer         = Alan Trustman Paul Burke Jack Weston
| music          = Michel Legrand
| cinematography = Haskell Wexler
| editing        = Hal Ashby Ralph E. Winters Byron Brandt
| studio         = The Mirisch Corporation Simkoe Solar Productions
| distributor    = United Artists
| released       =  
| runtime        = 102 minutes
| country        = United States
| language       = English
| budget         = $4.3 million Tino Balio, United Artists: The Company The Changed the Film Industry, Uni of Wisconsin Press, 1987 p 187 
| gross          = $14,000,000   
}}
 1968 film Best Original Windmills of remake was released in 1999.

==Plot==
Millionaire businessman-sportsman Thomas Crown (Steve McQueen) pulls off a perfect crime by orchestrating four men to rob $2,660,527.62 from a Boston bank, along with a fifth man who drives the getaway Ford station wagon with the money and dumps it in a cemetery trash can.  None of the men ever meets Crown face-to-face, nor do they know or meet each other before the robbery. Crown retrieves the money from the trash can personally after secretly following the driver of the station wagon, then personally deposits the money into an anonymous Swiss bank account in Geneva, making several trips, never depositing the money all at once so as to not draw undue attention to his actions.

Independent insurance investigator Vicki Anderson (Faye Dunaway) is contracted to investigate the heist and will receive 10% of the stolen money if she recovers it. When Thomas first comes to her attention as a possible suspect, she intuitively recognizes him as the mastermind behind the robbery.
 Paul Burke) bring the guilty party to justice.
 getaway driver, Erwin Weaver (Jack Weston), to "fink" on him. Vicki finds out that he was hired by a man he never saw, but whose voice he heard. She tries putting Erwin in the same room as Thomas, but there is no hint of recognition on either ones part. However, Vicki is clearly closing in on Thomas.

Thomas organizes another robbery exactly like the first with different accomplices and tells Vicki where the "drop" will be, because he has to know for sure that she is on his side. The robbery is successful, but there are gunshots and the viewer is left with the impression that people might have been killed, raising the stakes for Vickis decision.

Vicki and the police stake out the cemetery, where they watch one of the robbers make the drop, and wait for Thomas to show up so they can arrest him. When his Rolls Royce arrives, however, she sees that Thomas has sent a messenger in his place, with a telegram asking her to bring the money and join him — or else keep the Rolls Royce as a gift. She tears the telegram to bits and throws the pieces to the wind, looking up at the sky with tears in her eyes. Crown flies away in a jet.

==Cast==
* Steve McQueen as Thomas Crown
* Faye Dunaway as Vicki Anderson Paul Burke as Detective Eddie Malone
* Jack Weston as Erwin Weaver
* Gordon Pinsent as Jamie McDonald
* Biff McGuire as Sandy
* Yaphet Kotto as Carl
* Addison Powell as Abe
* Astrid Heeren as Gwen

==Production== split screens In the A Place to Stand, that latter of which pioneered the use of Christopher Chapmans Multi-dynamic image technique|"multi-dynamic image technique", images shifting on moving panes.     Steve McQueen was on hand for an advance screening of A Place to Stand in Hollywood and personally told Chapman he was highly impressed; the following year, Norman Jewison had incorporated the technique into the film, inserting the scenes into the already finished product. 

The film also features a chess scene, with McQueen and Dunaway playing a game of chess, silently flirting with each other.  The photography is unusual for a mainstream Hollywood film, using a split-screen mode. McQueen undertook his own stunts, which include playing polo and driving a dune buggy at high speed along the Massachusetts coastline.     This was similar to his starring role in the movie Bullitt, released a few months afterwards, in which he drove a Ford Mustang through San Francisco at more than 100&nbsp;mph. In an interview, McQueen would later say this was his favorite film.
 Ferrari 275 GTB/4 NART Spyders built.  Today, this model is one of the most valuable Ferrari road cars of all time. McQueen liked the car very much, and eventually managed to acquire one for himself.  The dune buggy was a Meyers Manx, built in California on a VW beetle floor pan with a hopped-up Corvair engine. McQueen owned one, and the Manx, the original "dune buggy", was often copied.  Crowns Rolls Royce carried Mass. vanity license tag "TC 100" for the film.  
 1999 remake.

==Filming locations==
The film was filmed primarily on location in Boston and surrounding areas in Massachusetts and New Hampshire: Beacon Hill, Harrison Gray Otis, was Thomas Crowns residence.  Congress St., Boston.  The current location is noted as 44 Water Street, the offices of private investment firm Brown Brothers.  The interiors were renovated and partially restored in 1999 by the firm GHK, Malcolm Higbee-Glace, Project Manager. Beverly across from City Hall.
* The money-dumpings were shot in Cambridge Cemetery, Coolidge Ave., Cambridge, Massachusetts|Cambridge.
* The polo sequences were filmed at the Myopia Hunt Club, 435 Bay Road, South Hamilton.
* The golf sequences were filmed at the Belmont Country Club, 181 Winter St., Belmont, Massachusetts|Belmont. Beacon Hill.
* Thomas drove his dune buggy on Crane Beach in Ipswich, Massachusetts. Schweizer SGS 1-23H glider was flown at Salem, New Hampshire.  North End. North End. Beacon Hill, a narrow, cobblestoned lane often called "the most photographed street in America."

Other locations included:
* the Allston-Brighton tollbooths on the Massachusetts Turnpike;
* Anthonys Pier 4 restaurant at 140 Northern Ave. in South Bostons Seaport District;
* the Boston Common;
* the old Boston Police Headquarters on Berkeley Street (since renovated as the Back Bay Hotel);
* Cambridge Street and Linden Avenue, Allston; North End;
* the North End Greenmarket;
* South Station, 700 Atlantic Ave., Boston;
* the Tobin Bridge.
* the Prudential Tunnel portion of the Massachusetts Turnpike going under Huntington Avenue (then-future Massachusetts Route 9) - years before the Westin Hotel in Copley Square and the parking garage on Clarendon Street were built over the toll highway in a scene where McQueen was driving the getaway car
* the then-Dewey Square Tunnel (future Interstate 93) where McQueen emerged onto the Massachusetts Turnpike - a feat technically impossible since McQueen drove into the Prudential Tunnel one scene earlier

==Reception==
The film was moderately successful at the box office, grossing $14 million on a $4.3 million budget.  Even though it is now regarded as a cult movie, reviews at the time were mixed. Critics praised the chemistry between McQueen and Dunaway and Norman Jewisons stylish direction, but considered the plotting and writing rather thin. Roger Ebert gave it 2  stars out of four and called it "possibly the most under-plotted, underwritten, over-photographed film of the year. Which is not to say it isnt great to look at. It is." 
 Windmills of Original Music Score.

==Remake==
 
The 1999 remake stars Pierce Brosnan as Crown, Rene Russo as the insurance investigator, and Denis Leary as the detective. The original films co-star Faye Dunaway also appears as Crowns therapist.

This version is different from the original in that it is set in New York rather than Boston and the robbery is of a priceless painting instead of cash, amongst other story line differences.

==References  ==
 

==External links==
*  
*  
*  
*  

 

 

 
 
 
 
 
 
 
 
 
 
 
 
 