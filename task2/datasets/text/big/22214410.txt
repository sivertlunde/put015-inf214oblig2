Preacher's Kid (film)
{{Infobox film
| name           = Preachers Kid
| image          = Preachers_Kid_one_sheet.jpg
| director       = Stan Foster
| producer       =  
| writer         = Stan Foster
| starring       =  
| cinematography = Dave Perkal
| editing        = Richard Nord Gener8Xion Entertainment
| distributor    = Warner Premiere
| released       =  
| runtime        = 110 minutes
| country        = United States
| language       = English
| budget         = $3 million
| gross          = $515,065
}} Durrell "Tank" Kiki Sheard, Sharif Atkins, Tammy Townsend, and Essence Atkins. The film was released to theaters on January 29, 2010 by Warner Premiere.

==Plot==
Small-town preachers kid Angie King leaves the church and her Augusta, GA home to pursue a dream of singing stardom. Luckett plays Angie, the daughter of a stern but loving bishop, whose attraction to the hunky star (Tank) of a traveling gospel show takes her on the road…and into romance, heartbreak and the realization that happiness may lie in the home she left behind.

==Release==
Preachers Kid was released to 109 theaters on January 29, 2010.  Gen8X committed to donate the films opening day net proceeds to humanitarian aid for 2010 Haiti earthquake relief  through charities such as Smile of a Child, Friend Ships, and Samaritans Purse.  In its opening weekend, it grossed $190,638, which equals $1,749 per theater.  The film has accumulated $515,065 to date.

===Reception===
The film received mixed to positive reviews from film critics. Kam Williams gave the film a "Very Good" review, saying, " ts well-enough executed, especially for a flick on a modest budget, to forgive the low production values and a tendency towards melodrama."  Michael Dequina of The Movie Report said, "Fosters film confirms the unique, undeniable power this genre can achieve on both stage and film..."  John Anderson of Variety (magazine)|Variety gave the film a mixed review, saying, "Stan Foster, making his directorial debut, has reared a Preachers Kid thats largely wooden, unlikely in many spots and, despite the few hard-edges of his script, could have used a tougher sensibility. The results are a movie that doesnt quite know what it wants to be. But, overall it was a good movie." 

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 