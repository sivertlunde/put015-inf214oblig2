My Little Pony: A Very Pony Place
{{Infobox film
  | name           = My Little Pony: A Very Pony Place
  | image          = 
  | caption        = 
  | director       = John Grusd
  | producer       = Cheryl McCarthy Jeanne Romano Robert Winthrop
  | writer         = Jeanne Romano Bonnie Solomon
  | starring       = Kathleen Barr Adrienne Carter Anna Cummer Maryke Hendrikse Janyse Jaud Andrea Libman Erin Mathews Britt McKillip Kelly Metzger Tracey Moore Tabitha St. Germain Chantal Strand Venus Terzo Cathy Weseluck
  | music          = Mark Watters Terry Sampson  
  | cinematography = 
  | editing        = Bruce W. Cathcart
  | studio         = SD Entertainment Paramount Home Shout Factory (Current)
  | released       = February 6, 2007
  | runtime        = 45 minutes
  | country        = United States Canada
  | language       = English 
  | budget         = 
  }} Animated film Paramount Home Entertainment in association with Hasbro. The film is the fourth feature in the third incarnation of the My Little Pony series and the first to have three separate stories in one feature. It was released on February 6, 2007 and received favorable reviews from critics.

==Plot==
Unlike the previous three specials, A Very Pony Place features three different stories, each of them focusing on the new characters exclusively released in the special. These were: Lily Lightly, Storybelle, Star Flight, Heart Bright and lastly, Puzzlemint.

===Come Back, Lily Lightly===
The first story, Come Back, Lily Lightly focuses on the main heroine Lily Lightly. The story begins when Lily Lightly announces the event called the Night of the Thousand Lights. Part of the event is the Rainbow Lights party, in which each unicorn pony in Unicornia decorates the place with lights. After nighttime falls and the whole place is decorated with lights, Cheerilee gave Lily Lightly the title "Princess of All that Twinkles and Glows", but her horn starts to glow and then runs off, leaving some of the ponies confused. Meanwhile, Pinkie Pie and Minty were going to Unicornia by balloon. Minty said she left the map at home and she doesnt need it. Back at Unicornia, Lily Lightly got embarrassed as everyone saw her horn glowing and runs away from Unicornia. Back at Pinkie Pie and Minty, both ponies were now getting lost through a sea of clouds while Minty said they need to see the bright lights of Unicornia to get there. Back on the ground, Brights Brightly and Rarity goes to search for Lily Lightly at the outskirts of Unicornia.

===Two for the Sky===
The second story, Two for the Sky focuses on Storybelle and her story about Star Flight and Heart Bright. Pinkie Pie, Minty and Sunny Daze all went to Storybelles house to listen to one of her stories. Storybelle herself picked her favorite story, Two for the Sky, a story about two ponies who were so close that they were almost twins. As she explained the story, both of them play together, eat sundaes together and wished that they can gain wings so they can fly to the sky. Both of them did several attempts to fly, but ended not lifting up in the ground, until they asked the Breezies in Breezy Blossom on how they can fly. However, the Breezies answered that they can actually fly, but not knowing why or how. As both ponies returned to Ponyville to get some sleep, they both wished and wanted to dream they wanted to fly. As they woke up, they both gain wings and both flew to the sky, not expecting that flying is not easy as it looks.

===Positively Pink===
The third and final story, Positively Pink focuses on Pinkie Pie, Minty and Puzzlemint. Minty looked at the Birthday Book and found out its Pinkie Pies birthday. Everyone hold up a meeting at Sweetberrys Sweet Shoppe and decided they plan a surprise party for Pinkie Pie by making everything pink. As Pinkie Pie enters the shop, everyone kept her mouths shut about the party and left the place. Feeling confused, Pinkie Pie asked Sweetberry whats going on, only answering that she needs to consult Puzzlemint to "Solve some kind of puzzle". As everyone in Ponyville prepares for her surprise party, Puzzlemint halts everyone on the preparations and goes on to distract Pinkie Pie so the preparations can continue.

==Characters==
 
;Lily Lightly
:Voiced by: Erin Mathews
:The main protagonist of Come Back, Lily Lightly, a unicorn pony with a light purple body, a light pink, pink and purple mane and pink and blue tail. Her cutie mark is a pink lily flower on a blue stem with little stars below it. Dubbed as the "Princess of All that Twinkles and Glows", Lily Lightly has a special horn which lights up when she is sad or happy. Though Lily finds her ability strange, her friends view it as part of her uniqueness.

;Storybelle
:Voiced by: Kelly Metzger
:The storyteller in Two for the Sky, an earth pony with a pink body, a yellow, pink, purple and blue mane and blue tail. Her cutie mark is a windmill with a rainbow and a book. Storybelle is the president of the Story Club, described to be a good storyteller and seen alongside Gossamer, her assistant. She tells Minty, Sunny Daze and Pinkie Pie about the story of Star Flight and Heart Bright.

;Star Flight
:Voiced by: Anna Cummer
:One of the two protagonist of Storybelles story Two for the Sky, an earth pony with a pink body and a dark pink and blue mane and tail. Her cutie mark is a big yellow star with a tail of white lines, pink stars, and yellow stars. She alongside Heart Bright both think alike like twins: playing together, eating sundaes together and wished that they can gain wings so they can fly to the sky.

;Heart Bright
:Voiced by: Anna Cummer
:One of the two protagonist of Storybelles story Two for the Sky, an earth pony with a white body, a blue and purple mane and a pink tail. Her cutie mark is a big pink heart with aqua vines trailing down her leg and smaller purple and pink hearts. She alongside Star Flight both think alike like twins: playing together, eating sundaes together and wished that they can gain wings so they can fly to the sky.

;Puzzlemint
:Voiced by: Kathleen Barr
:Puzzlemint is an earth pony with a white body and a yellow, purple, and pink mane and tail. Her cutie mark is a Magnifying glass looking at a puzzle piece and her name is a play on the word "puzzlement". One of the party planners for Pinkie Pies surprise party, she tries to distract Pinkie Pie to do a scavenger hunt so Minty and her friends can prepare a surprise party for her. Puzzlemint also enjoys solving puzzles.

==Media==

===Books===
A storybook adaptation of the film was created by Nora Pelizzari and was published by HarperFestival on January 30, 2007. The book adapts the storyline of the DVD special, except for Two for the Sky.

==Reception==
The special received favorable reviews from critics, having a score of 7.2 out of 10 in the Internet Movie Database.

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 