I Am Ichihashi: Journal of a Murderer
 
{{Infobox film
| name           = I am Ichihashi: Journal of a Murderer
| image          = I am ichihashi fim poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Dean Fujioka
| producer       = Toshiaki Nakazawa
| writer         = Hiroaki Yuasa
| based on       =  
| starring       = Dean Fujioka
| music          = Tomohide Harada
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Sedic International Inc.
| released       =  
| runtime        = 83 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
}}
  is a 2013 Japanese film directed by and starring Dean Fujioka in the title role. The film was released in Japan on 9 November 2013.    Based on the book Until I was Arrested by Tetsuya Ichihashi, the film portrays the two and a half years in which Ichihashi remained on the run following the murder of Lindsay Hawker in March 2007 until his arrest in November 2009. 

==Cast==
* Dean Fujioka as Tetsuya Ichihashi
* Takashi Nishina
* Shinichi Tsuha
* Cozy Sueyoshi

The theme song, "My Dimension", is also sung by Dean Fujioka.   

==Production==
The family of the deceased Lindsay Ann Hawker were not contacted by the producers during the filming of the movie, and Ichihashi refused to meet with Fujioka before filming started.  The production company has no plans to donate profits from the film to the Hawker family, as they have already refused to accept royalties from Ichihashis book. 

==References==
 

==External links==
*   
* 
*  

 
 


 