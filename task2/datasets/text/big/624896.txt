The Titfield Thunderbolt
 
 
{{Infobox film
| name = The Titfield Thunderbolt
| image = Titfield Thunderbolt poster.jpg
| caption = Theatrical release poster
| director = Charles Crichton
| writer = T.E.B. Clarke
| producer = Michael Truman
| executive producer = Michael Balcon
| starring = Stanley Holloway George Relph Naunton Wayne John Gregson Hugh Griffith Gabrielle Brune Sid James
| music = Georges Auric
| cinematography = Douglas Slocombe
| editing = Seth Holt
| distributor = General Film Distributors (UK) Universal Studios|Universal-International (USA)
| released = March 1953
| screened =
| runtime = 84 minutes
| country = United Kingdom
| language = English
| budget =
| preceded_by =
| followed_by =
}}
 narrow gauge Talyllyn Railway in Wales, the worlds first heritage railway run by volunteers.

It starred Stanley Holloway, George Relph and John Gregson, and was directed by Charles Crichton. Michael Truman was the producer. The film was produced by Ealing Studios. It was the first Ealing comedy shot in Technicolor and one of the first colour comedies made in the UK.

There was considerable inspiration from the book Railway Adventure by established railway book author L. T. C. Rolt, published in 1952. Rolt had acted as honorary manager for the volunteer enthusiasts running the Talyllyn Railway for the two years 1951-52. A number of scenes in the film, such as the emergency re-supply of water to the locomotive by buckets from an adjacent stream, or passengers being asked to assist in pushing the carriages, were taken from this book.

==Plot==
The residents of the (fictional) rural village of Titfield rely on the railway branch line to commute to work and transport their produce to market. So they are shocked when the government announces that the line is to be closed. Particularly hard hit is the local vicar, railway enthusiast Rev. Sam Weech (George Relph); he comes up with the idea to run it locally. He and the local squire, Gordon Chesterford (John Gregson), persuade wealthy Walter Valentine (Stanley Holloway) to provide the financial backing by telling him they can legally operate a bar while the train is running, so he will not have to wait all morning for the local pub to open.

The branch line supporters are bitterly opposed by bus operators Alec Pearce (Ewan Roberts) and Vernon Crump (Jack MacGowran), but, despite the fears of town clerk George Blakeworth (Naunton Wayne), the supporters persuade the Ministry of Transport to grant them a months trial period with an inspection at the end. Dan Taylor (Hugh Griffith), a retired railway worker, knows how to run an engine and joins the venture.

On the maiden run, Crump and Pearce try to block a crossing, first with their lorry and then with a passing steam roller operated by Harry Hawkins (Sid James), but the steam locomotive is too powerful and pushes them off the track. The next day, Crump and Pearce persuade an irate Hawkins to shoot holes in the water tower, but the passengers form a bucket brigade and refill the engine from a nearby river using buckets from the nearby farm. Defeated, Crump proposes a merger, but is turned down.

The night before the inspection, Hawkins, Crump and Pearce sabotage the line by using the steamroller to tow the unguarded engine and coach down the gradient. It runs off the track where the three men have removed a rail and is severely damaged. Blakeworth is mistakenly blamed and arrested.

Taylor and Valentine get drunk together and decide to "borrow" an engine from the Mallingford yards, but end up driving the engine along the main street of Mallingford and finally running the locomotive into a large oak tree. They are arrested, with the arresting constable giving the desk sergeant a long and humorous list of the many offences they have committed.
 goods brake van for Chesterford, the Conductor (transportation)#Train guard|guard, to use.
 black maria carrying Taylor and Valentine to gaol. Pierce panics and confess to the train wrecking and the two are also arrested.

Weech and Chesterford also have to improvise a means of connecting the engine to the rest of the train since the coupling method had greatly changed since Thunderbolt s heyday. The village craftsman uses a length of rope, but warns Chesterford to be careful.

As they are about to start their run, the police demand to be carried to Mallingford with their four prisoners. The Ministry inspector (John Rudling) refuses to adjust the starting time for the delay.

During a braking test, the rope snaps, and Thunderbolt leaves the rest of the train behind; however, several villagers turn out and manage to quietly push the carriages to meet up again with Thunderbolt at the water tower, with the inspector none the wiser. Joan Hampton (Gabrielle Brune) promises to marry Hawkins to get him to lend them the chain from his rollers steering mechanism to replace the broken rope.
 Light Railway Order to be granted.

==Cast==
*Stanley Holloway as Walter Valentine
*George Relph as Vicar Sam Weech
*Naunton Wayne as George Blakeworth
*John Gregson as Squire Gordon Chesterford
*Godfrey Tearle as Ollie Matthews, the Bishop of Welchester
*Hugh Griffith as Dan Taylor
*Gabrielle Brune as Joan Hampton
*Sid James as Harry Hawkins
*Reginald Beckwith as Coggett
*Edie Martin as Emily
*Michael Trubshawe as Ruddock
*Jack MacGowran as Vernon Crump
*Ewan Roberts as Alec Pearce
*Herbert C. Walton as Seth
*John Rudling as Clegg
*Nancy ONeil as Mrs. Blakeworth
*Campbell Singer as Police Sergeant Frank Atkinson as Station Sergeant
*Wensley Pithey as Policeman
 Westbury depot, provided to operate the train on location. Charles Crichton spoke with them on location and realised they "looked and sounded the part", so they were given speaking roles and duly credited.

GWR 1400 Class 0-4-2T locomotive number 1401  was hired by the producers. Locomotive number 1401 was a real locomotive.

==Production==
  (seen here in May 1980) masqueraded as Thunderbolt for the film.]] Camerton branch Cam Brook Camerton and Limpley Stoke. The branch had closed to all traffic on 15 February 1951, but was reopened for filming. Dunkerton Colliery. Midford Viaduct on the Somerset and Dorset Joint Railway, where the branch passed under the viaduct. The scene featuring Sid James characters traction engine, and the Squires attempts to overtake it, was filmed in Carlingcott.

The scene, where a replacement locomotive is stolen was filmed in the Oxfordshire village of Woodstock, Oxfordshire|Woodstock. The locomotive was a wooden mock-up mounted on a lorry chassis; the rubber tyres can (just) be spotted sitting inside the driving wheels.
 tender sustained some actual damage, which remains visible beneath the buffer beam to this day. The scene where Thunderbolt is removed at night from its museum was filmed in the (now demolished) Imperial Institute building near the Royal Albert Hall in South Kensington, London.

==The steam roller==
The steam roller used  was a 1904 Aveling & Porter 5 Horsepower#Nominal_horsepower|n.h.p. 10 ton Type R10 Steam Road Roller&nbsp;named Maid Marion, registration number CH3282, &nbsp;(works number 5590).  It  was still in commercial service at the time of filming. It was eventually sold for preservation and after  a full restoration, the roller returned to steam in 2006. It was in action as part of the road-making demonstration at the Great Dorset Steam Fair that year.

==Reception==
The film was well received by critics upon its original release, and currently holds a respectable three and a half star rating (7.1/10) on IMDb.

==Home media==
It was released first on VHS in 1998, and then on DVD in 2004.

A release on Blu ray|Blu-ray in 2013 followed the restoration of the film.

==Trivia== The Re-shaping of British Railways resulted in the closure of many branch lines like the one portrayed in the film. Beeching also wrote some technical dialogue for The Man in the White Suit, another of Clarkes films.
* The name "Titfield" is an amalgamation of the villages of Limpsfield and Titsey in Surrey, near Clarkes home at Oxted. {{Cite web
  |title=The Titfield Thunderbolt and the Camerton Branch
  |date=February 2011
  |author=Simon Castens
  |work=Address to Wells Railway Fraternity
  |url=http://www.railwells.com/documents/WRF_Meeting_Reports/WRF_Meeting_02_2011.pdf
}} 

== References ==
 

==Further reading==
*{{cite book
 | last = Fosker
 | first = Oliver
 | title = The Titfield Thunderbolt ~ Now & Then
 | date = 1 November 2008
 | publisher = Up Main Publishing
 | isbn = 978-0-9561041-0-6
}}
*{{cite book
 | last = Castens
 | first = Simon
 | title = On the Trail of The Titfield Thunderbolt
 | date = 22 July 2002
 | publisher = Thunderbolt Books
 | isbn = 0-9538771-0-8
}}
*{{cite book
 | last = Huntley
 | first = John
 | title = Railways in the Cinema
 | year = 1969
 | publisher = Ian Allan
 | isbn= 0-7110-0115-4
 | pages = 76–79
}}
*{{cite book
 | last = Mitchell
 | first = Vic
 |author2=Keith Smith
  | title = Frome to Bristol including the Camerton Branch and the "Titfield Thunderbolt"
 |date=June 1996
 | publisher = Middleton Press
 | isbn = 1-873793-77-4
}}

==External links==
*  
*  
* http://www.lionlocomotive.org.uk/ LION, an interesting Old Locomotive, probably best known as taking a starring part in the film Titfield Thunderbolt

 

 
 
 
 
 
 
 
 
 
 
 