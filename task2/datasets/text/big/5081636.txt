The Last War (1961 film)
{{Infobox film
| name           = The Last War
| image          = Last War.jpg
| image_size     = 
| caption        = 
| director       = Shūe Matsubayashi
| producer       = Tomoyuki Tanaka Sanezumi Fujimoto
| writer         = Takeshi Kimura   Toshio Yasumi
| narrator       = 
| starring       = Frankie Sakai   Akira Takarada   Yuriko Hoshi   Nobuko Otowa   Yumi Shirakawa
| music          = Ikuma Dan
| cinematography = Rokuro Nishigaki
| editing        = Koichi Iwashita
| distributor    = Toho (Japan) Medallion Pictures (USA)
| released       = August 10, 1961 (Japan) January 8, 1967 (U.S.)
| runtime        = 110 min. 79 min. (USA)
| country        = Japan Japanese English
| budget         = 
| gross          = 
}}
 Toho Studios in 1961. Its special effects were created by Eiji Tsuburaya who is also known for working on Godzilla and Ultraman.

Toshio Yasumis script for the film was reused for Prophecies of Nostradamus, which also included re-edits of the films special effects, in addition to its family melodrama center.
 Toei film The Final War, which told the story of a nuclear accident over North Korea that starts war with Japan caught as an innocent victim. The said film is also known as World War III Breaks Out. 

== Plot ==
Japanese merchantman Takano arrives back home with his crew after a long cruise and has a flashback.

Several months before, he returns to Japan to propose to his girlfriend, Saeko Tamura, and ask for her fathers blessing. Her own family continues to live life in relative peace and quiet. Despite reservations, the couple get married and spend one night in Yokohama before Takano ships out.

Meanwhile, tensions between the Federation and the Alliance (fictional stand-ins for the US and NATO against the USSR/Warsaw Pact, respectively) build up, especially after an intelligence-gathering vessel is captured. A new Korean War breaks out across the 38th parallel, but is quickly stopped. Although Japan calls on both sides to seek peace, government officials think that the country could be ripe for Alliance retaliation in light of its open support for the Federation. Dogfights between Federation and Alliance fighters over the Arctic (with both sides using nuclear-tipped air-to-air missiles) are just the beginning of a renewed conflict. The Alliance tips the scales by launching ICBMs on Japan and the West, with several cities totally destroyed. The Federation responds with their own nuclear strikes

As Takanos ship arrives at the remains of a decimated Tokyo, a childrens song plays in the background.

== Cast ==
*Frankie Sakai
*Akira Takarada - Takano
*Yuriko Hoshi
*Nobuko Otowa
*Yumi Shirakawa
*Chishū Ryū
*Jerry Ito 
*Eijirō Tōno
*So Yamamura - Prime Minister
*Ken Uehara - 
*Seizaburo Kawazu
*Nobuo Nakamura

==Similar films==
* The War Game
* Threads
* The Day After

== References ==
 

==External links==
* 
* Bogue, Michael. (2002).  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 


 