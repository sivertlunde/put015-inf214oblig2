Manushya Mrugam (1980 film)
{{Infobox film
| name           = Manushya Mrugam
| image          =
| caption        = Baby
| producer       = Thiruppathi Chettiyar
| writer         = Pappanamkodu Lakshmanan
| screenplay     = Pappanamkodu Lakshmanan
| starring       = Jayan Jagathy Sreekumar Kaviyoor Ponnamma Jose Prakash
| music          = KJ Joy
| cinematography = KB Dayalan
| editing        = K Sankunni
| studio         = Evershine
| distributor    = Evershine
| released       =  
| country        = India Malayalam
}}
 1980 Cinema Indian Malayalam Malayalam film, Baby and produced by Thiruppathi Chettiyar. The film stars Jayan, Jagathy Sreekumar, Kaviyoor Ponnamma and Jose Prakash in lead roles. The film had musical score by KJ Joy.   

==Cast==
 
*Jayan
*Jagathy Sreekumar
*Kaviyoor Ponnamma
*Jose Prakash
*Manavalan Joseph
*Prathapachandran
*Janardanan
*Jayaprabha
*Jayaragini
*Jyothi Lakshmi
*Master Sandeep Seema
*VP Nair
*Vijayalakshmi
 

==Soundtrack==
The music was composed by KJ Joy and lyrics was written by Pappanamkodu Lakshmanan. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ajantha Shilpangalil || S Janaki, P Jayachandran, Chorus || Pappanamkodu Lakshmanan || 
|-
| 2 || Kasthoori Maanmizhi || K. J. Yesudas, Chorus || Pappanamkodu Lakshmanan || 
|-
| 3 || Sneham Thaamara || K. J. Yesudas || Pappanamkodu Lakshmanan || 
|}

==References==
 

==External links==
*  

 
 
 

 