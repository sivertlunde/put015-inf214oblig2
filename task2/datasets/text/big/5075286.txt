Aashiq Banaya Aapne
{{Infobox film
| name = Aashiq Banaya Aapne
| image = Aashiq Banaya Aapne.jpg
| image size =
| caption = Theatrical poster
| director = Aditya Datt
| producer = Balabhai Patel
| writer = Aditya Datt
| starring = Emraan Hashmi Sonu Sood Tanushree Dutta
| music = Himesh Reshammiya
| cinematography = A K N Sebastian
| editing = Irfan Shaikh
| distributor = Shagun Film Creations
| released =  
| runtime =
| country = India
| language = Hindi
| budget =
}}
Aashiq Banaya Aapne is a Bollywood film starring Emraan Hashmi, Sonu Sood and Tanushree Dutta. It was released on 2 September 2005. The film marked the debut of actress Tanushree Dutta. The film was shot under the banner Shagun Film Creations. Although the film was a critical and commercial failure, the music of film was fantastic and widely appreciated.It marks debut of Himesh Reshammiya as singer.

== Plot ==
Karan (Sonu Sood) loves Sneha (Tanushree Dutta), but is too much of an introvert to ever express his feelings to her. Although Karans feelings are evident to many, he is content in just seeing Sneha smile and never gathers the courage to tell her how he feels. On an occasion Karan invites Sneha to a party where she meets the charming and mischievous Vicky.
  
Vicky (Emraan Hashmi) is Karans childhood friend. The exact opposite of Karan, Vicky is an extrovert and a notorious flirt. All threes lives change, when Vicky joins Karan and Sneha in college, and Sneha begins feeling drawn towards Vicky.

Fearing Vickys intentions, Karan tries to talk him out of his relationship with Sneha, but Vicky assures him that this time he really is in love with Sneha and not flirting. Karan feels he has lost the only woman he has ever loved but knows he cannot do anything about it. However life goes on and the three of them share a special bond of friendship. 
The closeness between Vicky and Sneha cross all boundaries, and just as everything seemed to be going well between the two, a certain incident one night shatters them both. Trust broken, obsession and confusion ensue and Sneha turns to Karan for support. Over time, Karan finally musters the courage to propose to her.

Just as Sneha and Karan are about to get engaged, Vicky re-enters their lives. The three friends reunite for that one night, which brings them all to a dangerous point of life and death.

== Cast ==
* Emraan Hashmi   ... Vikram "Vicky" Mathur
* Sonu Sood       ... Karan Oberoi
* Tanushree Dutta ... Sneha
* Naveen Nischol ... Snehas Dad
* Viveck Vaswani ... Karans Uncle
* Naresh Suri    ... Principal
* Zabyn Khan     ... Chandni

== Crew ==
* Director: Aditya Datt
* Producer: Bala Patel
* Music Director: Himesh Reshammiya
* Lyricist: Sameer
* Editing: Irfan Shaikh
* Choreography: Shabina Khan

== Music ==
The soundtrack was composed by Himesh Reshammiya. All songs were very popular with the title track "Aashiq Banana Aapne" becoming a rage among the youth. The soundtrack was marked no 1 in the Top music sales. This album also marked the debut of Himesh Reshammiya as a singer in Bollywood. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Song !! Singer(s)!! Duration
|-
| Aashiq Banaya Aapne
| Himesh Reshammiya, Shreya Ghoshal
| 6:04
|-
| Mar Jaawan Mit Jaawan
| Abhijeet Sawant, Sunidhi Chauhan
| 4:28
|-
| Aap Ki Kashish
| Himesh Reshammiya, Krishna Beura, Ahir
| 5:33
|-
| Dil Nashin Dil Nashin KK
| 6:32
|-
| Dillagi Mein Jo Beet Jaaye
| Sonu Nigam, Shaan (singer)|Shaan, Himesh Reshammiya, Jayesh Gandhi, Vasundhara Das
| 4:14
|-
| Aashiq Banaya Aapne (Remix)
| Himesh Reshammiya
| 4:32
|-
| Dil Nashin Dil Nashin (Remix)
| Himesh Reshammiya
| 4:32
|-
| Aap Ki Kashish (Remix)
| Himesh Reshammiya, Ahir
| 4:41
|}

== External links ==
*  

 
 
 
 