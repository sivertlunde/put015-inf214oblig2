On Your Mark
 
{{Infobox film
| name            = On Your Mark
| image           = 
| caption         = 
| director        = Hayao Miyazaki
| writer          = Hayao Miyazaki
| starring        =
| music           = Chage and Askas On Your Mark Toshio Suzuki
| editing         = Takeshi Seyama
| cinematography  = Atsushi Okui
| studio          = Studio Ghibli
| distributor     = Toei Company
| released        =    
| runtime         =  7 minutes
| country         = Japan
| language        = Japanese English
| budget          =
| gross           =
}} On Your rock duo Chage & Aska. The song was released in 1994 as part of the single "Heart". In 1995, Hayao Miyazaki wrote and directed the short film for the song as a side-project after having writers block with Princess Mononoke. The anime music video is non-linear, providing multiple reiterations and alternate scenes to depict the events. The music video added sound effects to the audio track, but contains no dialogue. Miyazaki purposely misinterpreted the lyrics to present his vision of a world where the surface becomes inhospitable and humans live in an underground city. Miyazaki purposely made the video cryptic to evoke creative interpretations among viewers.

The music video follows two policemen who raid a religious cult and find an angelic being only to have her taken away and confined to a laboratory. Haunted by the fate of the "angel", the two men formulate a plan and break into the laboratory. Fleeing in an armored truck the three plummet into an abyss after trying to force past a police aircraft along a narrow suspended roadway. After a montage of the previous scenes, the armored truck suddenly rockets into an apartment complex, allowing their escape. The three escape to the surface, ignoring the radiation and danger signs, emerging near an encased nuclear reactor. The two men set the "angel" free and she flies off into the sky.

The music video was well-received and praised for its animation and attention to detail. It premiered as a short before Studio Ghiblis Whisper of the Heart and has since been released on Laserdisc and DVD as part of All Things Ghibli Special Short Short. It has not been released outside of Japan.

==Synopsis==
The video begins with shots of a vacant village, overgrown with weeds, and the concrete sarcophagus of a covered-over nuclear reactor in the background. As the music begins, the scene changes to a Science fiction|sci-fi-style nighttime military-style police raid on a cult. Futuristic flying troop transports crash through the windows of a tower topped by  gigantic neon-lit eyes and occupied by armed defenders. The policemen exchange gunfire and grenades with cultists whose hoods depict an enormous eye. The victorious police begin to sort through the bodies of the cultists, two policemen find what appears to be a girl, lying unconscious, with large feathered wings on her back.
 Alfa Romeo Giulietta Spider passo corto down an empty road. As one of the men helps the girl up, she spreads her wings and he holds her hands while she gains confidence. With a nudge she is airborne, but she seems hesitant and afraid as he lets go.
 radiation suits arrive and quickly take the girl away after placing her into a container.
 armored truck and drive along a narrow suspended roadway over what appears to be a domed city built in a crater. Police hovercrafts are in pursuit, and one of them comes very low to the roadway to block the fugitives truck. The roadway collapses when the protagonists try to force their way through, sending the truck plummeting. The winged girl refuses to let go of the hands of her rescuers, and the three of them fall into the abyss.

A brief montage of previous shots follows: the discovery of the girl, the girl flying through a blue sky, the two men rescuing the girl from the laboratory and stealing the truck, the truck plummeting amidst the wreckage of the roadway. But this time, inexplicably, the truck fires stabilizing thrusters and makes a short flight into the side of an apartment building. After their escape, the three are seen in an old Alfa Romeo Giulietta Spider passo corto racing through a dark tunnel underneath signs which bear radiation symbols and read (in kanji) "Beware Of Sunlight" and "Survival Not Guaranteed", then finally they emerge into daylight. They drive past nuclear cooling towers and a sign which reads "Extreme Danger" and continue down the road.

One of the men helps the girl up, she spreads her wings and gives them a grateful smile; he kisses her hand, and the other winks in farewell. Soon, she is gone drifting upward into the sky. Briefly, a major urban cityscape is seen beyond the trees. From a birds-eye view, we see the shape of the car veering off the road and slowing to a stop in the grass.

==Production== Toshio Suzuki told Helen McCarthy, the British author of numerous anime reference books, that Studio Ghibli had not given "100 percent" focus to the music video. 
 traditional hand Masashi Andō. Long time Miyazaki collaborator Michiyo Yasuda was in charge of colour selection. Backgrounds were created by Kazuo Oga. Yōji Takeshige made his debut as Art Director.    There is no dialogue in the music video and the two policemen are loosely modeled after Chage and Aska. 

==Analysis==
The deliberately non-linear, enigmatic and cryptic style of the music video was intended to stimulate the imaginations of its viewers and their interpretations of the music video."    Miyazaki offered an interpretation of the angel as "Hope" and to protect hope could paradoxically mean "to let it go where no-one can touch it".  Dani Cavallaro, an author of books related to anime, reflected on this by proposing that hope retains its purity and authenticity when it is ephemeral, evanescent and elusive.  Hope may cause exertion and possibly pain, but denying hope is to deny the feasibility or and vision it provides.  Miyazaki said that in the music videos setting, humans live in an underground city after the surface of the Earth has been contaminated with radiation, creating a sanctuary for nature.  Miyazaki did not find this believable, though, as humanity would suffer on the surface instead.  Miyazaki intentionally misinterpreted the lyrics to reflect upon the vision of a world filled with disease and radiation and peoples reactions to that world.  Cryptically, he implied the two policemen might not be able to return to their old life, but offered no reason as to why.   
 Only Yesterday with the oppressive urban environment and the freedom of open spaces. 
 Ariel in his play The Tempest. 
The final volume of the Nausicaä manga was released in January 1995. Miyazaki started creating On Your Mark that same month. 

McCarthy highlighted similarities to different works and real life found throughout the film, remarking that the opening city sequence could be an homage to Akira (film)|Akira or Blade Runner and the attack on the religious cult could be a reflection of the Aum Shinrikyo movement.  Nausicaa.net stated the production occurred prior to the police raid following the Sarin gas attack on the Tokyo subway in March 1995.    McCarthy also noted that the films scientists decontamination gear look like the hero of Porco Rosso and the rescue scene is reminiscent of Princess Leia in Star Wars.  The encased or "box" structure in the film is an homage to the Chernobyl Nuclear Power Plant which was entombed in concrete following the Chernobyl disaster.  

Miyazaki scholar   observed that the urban settings have a China Town style and resemble the cityscapes of Mamoru Oshiis Ghost in the Shell animated film adaptation, in production at the time. While Miyazaki has been non-committal about the nature of his winged entity in On your Mark, Kanō noted that the manga  , an unfinished collaborative effort by Oshii and Satoshi Kon, with its Angel Disease theme, was still serialized in Animage at the time Miyazaki was creating On Your Mark in 1995. 

==Reception==
The music video was well received. Dr. Patrick Collins, a science writer called it, "the most perfect short science fantasy film Ive ever seen."  McCarter of EX Magazine praised the films attention to detail that brought the world to life.  THEM anime praised the music video and went so far as to justify the purchase of Ghibli ga Ippai Special Short Short for this music video. 

==Releases== Aska (Shigeaki Walt Disney Studios Japan removed On Your Mark from the upcoming DVD/Blu-ray box set containing Hayao Miyazakis works and ceased shipments of All Things Ghibli Special Short Short.  On October 27, 2014, Studio Ghiblis Toshio Suzuki announced on the companys web page that they had re-considered the situation and would be sending out Blu-ray discs to purchasers of the Hayao Miyazaki box set, as long as they could provide proof of purchase. 

==References==
 

==External links==
*  
*   at Big Cartoon Database
*  
*  

 
 

 
 
 
 
 