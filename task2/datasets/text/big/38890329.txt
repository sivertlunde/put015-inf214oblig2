The Best Job in the World
 
 
{{Infobox film
| name           = The Best Job in the World
| image          = 
| caption        = 
| director       = Gérard Lauzier
| producer       = Jean-Louis Livi
| writer         = Gérard Lauzier
| starring       = Gérard Depardieu Michèle Laroque
| music          = 
| cinematography = Jean-Yves Le Mener
| editing        = 
| distributor    = 
| released       =  
| runtime        = 105 minutes
| country        = France
| language       = French
| budget         = $9,900,000
| gross          = $13,643,412 
}}

The Best Job in the World ( ) is a 1996 French drama film directed by Gérard Lauzier. It was entered into the 20th Moscow International Film Festival.   

==Cast==
* Gérard Depardieu as Laurent Monier
* Michèle Laroque as Hélène Monier
* Souad Amidou as Radia Ben Saïd
* Ticky Holgado as Baudouin
* Guy Marchand as Gauthier
* Philippe Khorsand as Le gardien de limmeuble
* Daniel Prévost as Albert Constantini, le voisin
* Roschdy Zem as Ahmed Raouch
* Mouss Diouf as Momo
* Faisal Attia as Nacir

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 