The Long Haul (1957 film)
{{Infobox film
| name           = The Long Haul
| image          = The Long Haul 1957 film poster.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Ken Hughes 
| producer       = Maxwell Setton
| writer         = Ken Hughes Mervyn Mills (novel)
| starring       = Victor Mature Diana Dors Patrick Allen Gene Anderson
| music          = Trevor Duncan
| cinematography = Basil Emmott
| editing        = Raymond Poulton
| studio         = Marksman Films
| distributor    = Columbia Pictures
| released       = 1957
| runtime        = 100 mins.
| country        = United Kingdom 
| language       = English 
}} British drama film directed by Ken Hughes and starring Victor Mature, Patrick Allen and Diana Dors.  An American ex-servicemen settles in Britain with his English wife and becomes a lorry driver in Liverpool where he begins a relationship with the girlfriend of a major crime figure.

==Cast==
 
* Victor Mature -  Harry Miller 
* Diana Dors -  Lynn
* Patrick Allen -  Joe Easy
* Gene Anderson - Connie Miller Peter Reynolds -  Frank
* Liam Redmond -  Casey John Welsh -  Doctor
* Meier Tzelniker - Nat Fine
* Michael Wade -  Butch Miller 
* Dervis Ward -  Mutt 
* Murray Kash -  Jeff 
* Jameson Clark -  MacNaughton  John Harvey -  Superintendent Macrea 
* Roland Brand -  Army sergeant 
* Stanley Rose -  Foreman 
* Barry Raymond -  Depot manager
* Norman Rossington - Liverpool driver
* Arthur Mullard - Minor role
 

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 

 