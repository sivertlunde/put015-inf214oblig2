A Perfect Couple
 
 
{{Infobox film
| name           = A Perfect Couple
| image          = APerfectCouple1979Poster.jpg
| caption        = Theatrical poster
| director       = Robert Altman
| producer       = Robert Altman
| writer         = Allan F. Nicholls Robert Altman
| screenplay     = 
| story          = 
| based on       =  
| starring       = Paul Dooley Marta Heflin Titos Vandis Belita Moreno
| music          = Allan F. Nicholls
| cinematography = Edmond L. Koons
| editing        = Tony Lombardo
| studio         = 
| distributor    = 20th Century Fox
| released       =  
| runtime        = 110 minutes
| country        = United States
| language       = English
| budget         = $1.9 million  
| gross          = 
}}

A Perfect Couple is a 1979 film directed by Robert Altman.

== Plot ==
An older man, played by Paul Dooley, tries romancing a younger woman, played by Marta Heflin.  She is part of a travelling band of bohemian musicians who perform gigs in outdoor arenas around the country.  He joins them on the road and tries to fit into their communal lifestyle.  The film features multiple musical numbers.

==Cast==
*Paul Dooley as Alex Theodopoulos
*Marta Heflin as Sheila Shea
*Titos Vandis as Panos Theodopoulos
*Belita Moreno as Eleousa
*Henry Gibson as Fred Bott
*Dimitra Arliss as Athena
*Allan F. Nicholls as Dana 115
*Ann Ryerson as Skye 147 Veterinarian
*Poppy Lagos as Melpomeni Bott
*Dennis Franz as Costa
*Margery Bond as Wilma
*Mona Golabek as Mona
*Terry Wills as Ben
*Susan Blakeman as Penelope Bott
*Melanie Bishop as Star

== Production ==
The role of Sheila Shea was originally written for Sandy Dennis. Paul Dooley was seriously allergic to cats though, and when cat-lover Dennis would come to the script readings with up to five cats at a time, he was briefly hospitalized. As a result, Allan Nicholls re-wrote the role of Sheila Shea from an Earth Mother type to the young singer/groupie played by Marta Heflin.

==Soundtracks==
Songs listed here, and in the movie credits, do not always appear on CD soundtracks.

*"Romance Concerto (Adieu Mes Amis)" &mdash; by Thomas Pierson & Allan Nicholls
*"Sompins Got A Hold On Me" &mdash; by Tony Berg & Ted Neeley
*"Hurricane" &mdash; by Tony Berg, Ted Neeley & Allan Nicholls
*"Week-End Holiday" &mdash; by Allan Nicholls, B.G. Gibson & Tony Berg
*"Wont Somebody Care" &mdash; by Tony Berg & Allen Nicholls
*"Love Is All There Is"&mdash; by Allan Nicholls, Tony Berg & Ted Neeley
*"Searchin For The Light" &mdash; by Tomi-Lee Bradley, Tony Berg, Allan Nicholls & Ted Neeley
*"Lonely Millionaire" &mdash; by Cliff De Young & Tony Berg
*"Fantasy" &mdash; by Allan Nicholls
*"Dont Take Forever" &mdash; by Allan Nicholls, B.G. Gibb & Tony Berg
*"Let The Music Play" &mdash; by Allan Nicholls & Otis Stephens
*"Goodbye Friends" &mdash; by Allan Nicholls

==References==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 


 
 