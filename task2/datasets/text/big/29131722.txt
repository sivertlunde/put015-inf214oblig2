Six Hours to Lose
{{Infobox Film
| name           = Six Hours to Lose
| image          = 
| caption        = 
| director       = Alex Joffé Jean Lévitte
| producer       = 
| writer         = Alex Joffé Jean Lévitte
| starring       = André Luguet
| music          = Henri Dutilleux
| cinematography = Pierre Montazel
| editing        = Madeleine Gug Denise Baby
| distributor    = Pathé Consortium Cinéma
| released       = 22 January 1947
| runtime        = 90 minutes
| country        = France
| awards         = 
| language       = French
| budget         = 
| gross
}}
 French Romance romance film from 1946, directed by Alex Joffé, written by Alex Joffé and Jean Lévitte, starring André Luguet. The film was starred by Louis de Funès. The film based on the novel of Robert Brasillach, with the same title Six heures à perdre, edited posthumously in 1953. 

==Plot==
A traveller is stuck in an unknown town because his connecting train will only arrive in six hours. He decides to kill time by taking a stroll. He is not prepared to get confused with somebody else. In fact the citizens are eagerly awaiting the visit of a famous man and the clusless traveller is his doppelgänger. Soon he experiences what that means.

== Cast ==
* André Luguet: traveller / Léopold de Witt
* Denise Grey: Misses de Witt
* Pierre Larquey: Joseph
* Paulette Dubost: Annette
* Jacqueline Pierreux: Simone
* Dany Robin: Rosy
* Louis de Funès: the driver
* Luce Fabiole: the passenger
* Marguerite de Morlaye: the rich widow
* Jean-Jacques Delbo: Claude
* Jean Gaven: Antoine

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 