The Principle
 
{{Infobox film
| name           = The Principle
| image          = 
| alt            = 
| caption        = 
| director       = Katheryne Thomas
| producer       = Rick Delano Robert Sungenis
| writer         = 
| narrator       = Kate Mulgrew
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Rocky Mountain Pictures
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $86,172 though April 26, 2015 
}}
The Principle is a 2014 American documentary film produced by Rick Delano and Robert Sungenis questioning the Copernican principle and discussing geocentricism. The film opened in Chicago on October 24, 2014. The film is narrated by Kate Mulgrew and features scientists such as Lawrence M. Krauss and Michio Kaku. Mulgrew and many of the scientists who were interviewed have since repudiated the ideas advanced in the film and have alleged that their involvement was the result of being misled by the filmmaker. 

==Summary==
The films producers state that the documentary concerns recent observations challenging the Copernican principle.      Some authors speculate that the film aims to support the theory that the Earth is motionless at the center of the universe.    

The release date of the film was October 24, 2014, when it was screened at the Marcus Addison Cinema in Addison, Illinois, according to the distributor Rocky Mountain Pictures. 

==Controversy==

===Claims=== George Ellis has said that "I was interviewed for it but they did not disclose this agenda, which of course is nonsense. I dont think its worth responding to -- it just gives them publicity. To ignore is the best policy. But for the record, I totally disavow that silly agenda." 
Michio Kaku said that the film was likely "clever editing" of his statements and bordered on "intellectual dishonesty"    and Lawrence Krauss said he had no recollection of being interviewed for the film and would have refused to be in it if he had known more about it.   Julian Barbour claims he never gave permission to be in the film.   

===Counterclaims===
The release forms for Lawrence Krauss and Julian Barbour were displayed on a live web cast session by the producers of the film on May 28, 2014.    The release form for Lawrence Krauss, and similarly Julian Barbour include the verbage, "Interviewee...agrees that the footage... will be used in a feature documentary ... interviewee also understands Producer will seek out ... unconventional interpretations and theories as well as mainstream views."  On the live recorded weblog Rationally Speaking uploaded to YouTube May 22, 2014, Lawrence Krauss states that after thinking about it, he recalls being interviewed for The Principle. After making the admission, he is critical of Kate Mulgrews participation as narrator of the documentary but ultimately gives her the benefit of the doubt.   

==References==
 

==External links==
* 
*  official movie website

 
 
 
 
 
 
 