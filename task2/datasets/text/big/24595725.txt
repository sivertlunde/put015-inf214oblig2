Flicker (film)
{{Infobox film
| name           = FLicKeR
| image          =
| alt            =  
| caption        =
| director       = Nik Sheehan
| producer       = {{Plainlist|
* Silva Basmajian
* Christina Clarke}}
| writer         = Nik Sheehan
| based on       =  
| starring       = {{Plainlist|
* Brion Gysin
* Kenneth Anger
* Marcus Boon
* Udo Breger}}
| music          = Edmund Eagan
| cinematography = Harald Bachmann
| editing        = {{Plainlist|
* Caroline Christie
* Miume Jan Eramo}}
| studio         =
| distributor    =
| released       =  
| runtime        =
| country        = Canada
| language       = English French
| website.       = www.flickerflicker.com
| gross          =
}}
FLicKeR is a Canadian documentary film written and directed by Nik Sheehan, produced by Maureen Judge and Silva Basmajian (National Film Board of Canada|NFB). The film is based on the book Chapel of Extreme Experience  by John G. Geiger about the work of artist Brion Gysin and his Dreamachine.

Gysins Dreamachine used a 100-watt light bulb, a motor, and a rotating cylinder with cutouts. Its users would sit in front of it, close their eyes, and experience visions as a result of the flashes of light. Gysin believed that by offering the world a drugless high the invention could revolutionize human consciousness.   

The documentary features interviews with many prominent figures from the beat movement who had experimented with Gysins invention and discuss his life and ideas in the film. Notable figures include Marianne Faithfull, DJ Spooky, The Stooges, Iggy Pop, Lee Ranaldo, Genesis P-Orridge, John Giorno, Floria Sigismondi, and Kenneth Anger. 
 Hot Docs and received the festivals Special Jury Prize for the best Canadian Feature Length Documentary.  It then went on to win in the Best Film on International Art category at the 2009 Era New Horizons Film Festival in Poland,  and was also nominated for a 2009 Gemini Award in the category of Best Performing Arts Program or Series or Arts Documentary Program or Series, and best original score by composer Edmund Eagan.  Nik Sheehan was also nominated for a Canadian screenwriting award by the Writers Guild of Canada. 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 