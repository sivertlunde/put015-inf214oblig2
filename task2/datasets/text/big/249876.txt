Bend It like Beckham
 
{{Infobox film
| name = Bend It like Beckham
| image = Bend It Like Beckham movie.jpg
| image_size = 215px
| alt = Two sporty girls hugging.
| caption = British release poster
| director = Gurinder Chadha
| producer = {{Plainlist|
* Gurinder Chadha
* Deepak Nayare
}}
| writer = {{Plainlist|
* Gurinder Chadha
* Guljit Bindra
* Paul Mayeda Berges
}}
| narrator = Charlotte Hill
| starring = {{Plainlist|
* Parminder Nagra
* Jonathan Rhys Meyers
* Keira Knightley
* Archie Panjabi
* Shaznay Lewis
* Anupam Kher
* Preeya Kalidas
}}
| music = Craig Pruess
| cinematography = Jong Lin
| editing        = Justin Krish Helkon Media AG iDream Productions  
| released       =  
| runtime        = 112 minutes
| country        = {{Plainlist|
* United Kingdom
* India
}}
| language       = {{Plainlist| 
* English
* Punjabi
* German
* Hindi
}}
| budget         = $6 million (£3.7 million) estimated
| gross          = $76,583,333 
}}
 British Comedy-drama|comedy-drama football player free kicks by "bending" the ball past a wall of defenders.
 Punjabi Sikhism|Sikhs football but her parents have forbidden her to play because she is a girl. She joins a local womens team, which makes its way to the top of the league.
 musical version Phoenix Theatre in 2015. 

==Plot== Punjabi Sikhism|Sikh football but closeted homosexual and his buddies, where her skills draw the attention of Juliette "Jules" Paxton, who plays for the womens team of the local club, the fictional Hounslow Harriers. Their coach, Joe, is impressed with her play and puts her on the team. Jess pretends to have a job to play with the team and Jules and Jess become best friends, despite the fact that both are attracted to Joe. Jess enlists her sister Pinky to cover for her when the team travels to Germany for a big match, but everything unravels when Jules spies Joe kissing Jess, causing a breach between them, and Jesss parents find out and forbid her to continue. Meanwhile, Juless mother wrongly thinks Jules is a lesbian and the girls spat is a lovers quarrel. Jesss parents are confronted by a similar accusation before Jess reveals the truth about her attraction to Joe.

While the elder Bhamras are distracted by the elaborate preparations for Pinkys upcoming wedding, Jess continues to play and the Hounslow Harriers make their way towards the top of the league. Joe encourages Jess to come clean with her parents and eventually approaches Mr. Bhamra to explain she has a chance to win a prestigious Athletic scholarship|scholarship, but  Mr. Bhamra refuses to believe it. Jess and Jules must sort their differences, make peace with their parents, and overcome cultural prejudice to win the league championship, and earn scholarships to Santa Clara University in California.

==Cast==
* Parminder Nagra as Jesminder Kaur "Jess" Bhamra
* Keira Knightley as Juliette "Jules" Paxton
* Archie Panjabi as "Pinky" Bhamra
* Jonathan Rhys Meyers as Joe
* Shaznay Lewis as Marlena "Mel" Goines
* Anupam Kher as Mohaan Singh Bhamra
* Shaheen Khan as Mrs. Bhamra
* Juliet Stevenson as Paula Paxton
* Frank Harper as Alan Paxton
* Ameet Chana as Tony
* Kulvinder Ghir as Teetu
* Pooja Shah as Meena
* Preeya Kalidas as Monica
* Trey Farley as Taz
* Saraj Chaudhry as Sonny
* Paven Virk as Bubbly
* Nithin Sathya (uncredited) as Waiter
* Emma Daly  team player
* Nina Wadia as Wedding Caterer
* Ace Bhatti as Nairobi Grandson

==Production==
  
There is a rumour that the films plot originally resolved with the two female leads ending up together romantically but director Gurinder Chadha rewrote the script for fear of upsetting conservative Indians, however this is untrue. The film has an important message about challenging homophobia and the role of women in society. 

==Reception==

===Critical response===
The film surprised the critics and it was met with mostly positive reviews. Kenneth Turan of the Los Angeles Times noted that the film "was really full of easy humor, an impeccable sense of milieu that is the result of knowing the culture intimately enough to poke fun at it while understanding its underlying integrity."  The Times of India noted that Bend It "is really about the bending of rules, social paradigms and lives – all to finally curl that ball, bending it like Beckham, through the goalpost of ambition   The creeping divide shows that Britain is changing, but hasnt quite changed yet. The stiff upper lip has travelled miles from the time Chadhas father was denied a pint at some pubs at Southall, but like dollops of coagulated spice in badly stirred curry, discrimination crops up to spoil the taste, every now and then, in multi-racial Britain." 

Planet Bollywood gave the film a 9 out of 10 and stated that the "screenplay not only explores the development of Jesse as a person, but also the changing values and culture of   argues, "if ever there is a film that is positive, realistic and yet delightful, then it has to be Dream Productions latest venture directed by Gurinder Chadha   Light hearted, without taking away the considerable substance in terms of values, attitudes and the love for sport, the film just goes to prove that there are ways to be convincing and honest."  The BBC gave it 4 out of 5 stars and argued that "Mr. Beckham ought to be proud to have his name on such a great film." 

Review aggregation website Rotten Tomatoes gives Bend It like Beckham a rating of 85%, based upon 147 reviews (125 fresh and 22 rotten). {{cite web
| url= http://www.rottentomatoes.com/m/bend_it_like_beckham/
| title= Bend It Like Beckham
| accessdate= 5 October 2010
| work= Rotten Tomatoes
| publisher= Flixster
}} 

 
The British film went on to set the record in India for most number of tickets sold during a single weekend for a foreign movie. It also went on to become the highest-grossing Indian-themed film ever in the USA with $32 million in box office revenue.  iDream Productions in India. 

===Awards and nominations===
 
 
;Wins Pyongyang Film Festival: Music Prize British Comedy Awards: Best comedy film
* 2003   
* 2004  
* 2002  
*  
* 2002 International Film Festival of Marrakech: Special Jury Award – Gurinder Chadha
* 2003 National Board of Review of Motion Pictures: Special Recognition
* 2002 Sydney Film Festival: PRIX UIP – Gurinder Chadha
* 2003 The Comedy Festival: Film Discovery Jury Award – Gurinder Chadha

;Nominations Billie Award – Entertainment (Best film)
* 2004 Writers Guild of America Award – Best Screenplay
* 2003 British Academy of Film and Television Arts – Best film Golden Globe for Best Film – Musical or Comedy

==Soundtrack==
{{Infobox album
| Name        = Music From the Motion Picture Bend It Like Beckham
| Type        = soundtrack
| Artist      = Various artists
| Cover       = 
| Released    = 2002
| Recorded    = Bhangra Pop Pop
| Length      =
| Label       = Milan Entertainment (US) Cube Records (UK)
| Producer    =
}}
   
}} bhangra music, Melanie C (whose song I Turn to You (Melanie C song)|"I Turn To You" was used in the film but is not included in the soundtrack) and the band Texas (band)|Texas. It also features "Baddest Ruffest" by Backyard Dog, the aria Nessun Dorma, from Giacomo Puccini|Puccinis Turandot and excerpts from the dance band Basement Jaxx. The USA release rearranges the tracks and excludes some material. The single "Dream the Dream", which is recorded by Shaznay Lewis (who portrays Mel in the film) appears in the movie but didnt make it the final cut on the soundtrack.

;UK release
# Craig Pruess & Bally Sagoo Feat. Gunjan – "Titles" Blondie – "Atomic (song)|Atomic"
# Backyard Dog – "Baddest Ruffest" B21 – "Darshan"
# (Movie Dialogue) – "Its Beckhams Corner"
# Victoria Beckham – "I Wish"
# (Movie Dialogue) – "Learn To Cook Dahl"
# Malkit Singh – "Jind Mahi"
# Nusrat Fateh Ali Khan – "Tere Bin Nahin Lagda" craig michael
# Bally Sagoo Feat Gunjan – "Noorie"
# (Movie Dialogue) – "Juicy Juicy Mangoes"
# Basement Jaxx – "Do Your Thing"
# (Movie Dialogue) – "Eyes Down" Texas – "Inner Smile"
# Melanie C – "Independence Day" (New Version)
# (Movie Dialogue) – "Cant Make Round Chapattis"
# Hans Raj Hans – "Punjabiyan Di Shaan"
# Gunjan – "Kinna Sohna"
# Tito Beltrán – "Nessun Dorma"
# (Movie Dialogue) – "The Offside Rule Is"
# Bina Mistry – "Hot Hot Hot"
# Craig Pruess & Bally Sagoo Feat. Gunjan – "Hai Raba!"
# Curtis Mayfield –  "Move on Up"

;US release
# Craig Pruess & Bally Sagoo Feat. Gunjan – "Titles"
#* (Movie Dialogue) – "Its Beckhams Corner" Texas – "Inner Smile"
# Malkit Singh – "Jind Mahi"
# Bally Sagoo Feat Gunjan – "Noorie"
#* (Movie Dialogue) – "Learn To Cook Dahl"
# Victoria Beckham – "I Wish"
#* (Movie Dialogue) – "Juicy Juicy Mangoes"
# Gunjan – "Kinna Sohna"
# Partners in Rhyme (featuring Nusrat Fateh Ali Khan) – "Tere Bin Nahin Lagda"
#* (Movie Dialogue) – "Cant Make Round Chapattis"
# Melanie C – "Independence Day" B21 – "Darshan"
#* (Movie Dialogue) – "Eyes Down"
# Bina Mistry – "Hot Hot Hot" Blondie – "Atomic (song)|Atomic"
# Craig Pruess & Bally Sagoo Feat. Gunjan – "Hai Raba!"
# Tito Beltrán – "Nessun Dorma"

==North Korean broadcast== North Koreas British Ambassador to South Korea, Martin Uden, said it was the "1st ever Western-made film to air on TV" in North Korea. 

==See also==
 

==References==
 

==External links==
 
*  
* 
*  
*  
*  
*  
*  
*   on Comedy Movies

 
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 