The Package (2013 film)
{{Infobox film
| name = The Package
| image = The Package 2013.jpg
| caption = DVD/Blu-Ray Cover
| director = Jesse V. Johnson
| writer = Derek Kolstad Steve Austin Dolph Lundgren Eric Keenleyside
| released =  
| runtime = 
| country = United States
| language = English
| budget = 
| gross = $1,469 (domestic) 
}}
The Package is a 2013 direct-to-video action film directed by Jesse V. Johnson and starring "Stone Cold" Steve Austin and Dolph Lundgren.  

Filming began in February 2012, part of which took place in Abbotsford, British Columbia,  and the film released on February 9, 2013. 

==Plot== Steve Austin), a man tasked with delivering a mysterious package to "The German" (Dolph Lundgren), a dangerous crime lord. Along the way Wick is attacked repeatedly by a rival gang that wants what he is carrying, eventually he discovers that his cargo is not what he was initially thought it was.

Tommy arrives at a bowling alley and beats up a rival for not paying back Big Doug. Afterwards he visits Big Doug who tells him he wants a package delivered to the German. En route he is pursued by a rival gang who wants what he is carrying, forcing him to fight them off and run.

Eventually Tommy is captured by the rival gang. He manages to escape, and is captured by the German and brought to see if he has compatible DNA with the German. He then breaks free and has a final showdown with the German, killing him. He then wishes Big Doug that the incident be filed away and they wish each other a hearty godspeed. Tommy then rings his wife about the cash and tells her I love you.

==Cast== Steve Austin as Tommy Wick
*Dolph Lundgren as The German
*Eric Keenleyside as Big Doug
*Mike Dopud as Julio
*John Novak as Nicholas
*Kristen Kerr as Darla
*Darren Shahlavi as Devon
*Paul Wu as Dosan
*Lochlyn Munro as Eddie
*Mark Gibbon as Jake
*Peter Bryant as Ralph
*Monique Ganderton as Monique
*Michael Daingerfield as Anthony
*Jerry Trimble as Carl
*Patrick Sabongui as Luis

==Box office==
For a direct-to-dvd movie, The Package was not a financial success. In its first week, the movie debuted at number 81 at the box-office - only making $1,469. 

==Reception==
The Los Angeles Times gave a favorable review of The Package, calling it an "uncomplicated guys guy movie time, the screen version of the starchy passing pleasures of bar food."  In contrast, DVD Verdict panned the film, calling it mediocre and saying that Austins fight scenes were "unimaginatively produced and not much fun". 

==References==
 

==External links==
*  

 
 
 
 
 
 
 


 