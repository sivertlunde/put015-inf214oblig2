Cabaret Neiges Noires
 
{{Infobox Film
| name = Cabaret Neiges Noires
| director = Raymond Saint-Jean
| producer = Michel Ouellette
| cinematography = Ronald Plante
| writer = Dominic Champagne
| starring = André Barnard Julie Castonguay Dominic Champagne Estelle Esse Norman Helms Roger Larue Suzanne Lemoine Didier Lucien Jean Petitclerc Dominique Quesnel
| music = Pierre Benoît
| editing = Philippe Ralet
| studio = Cine Qua Non Films
| distributor = Antenna Distribution
| released = 1997
| runtime = 97 minutes
| country = Canada
| language = French
}}

"Cabaret Neiges Noires" is a 1997 French Canadian musical film, by Cine Qua Non Films. It is directed by Raymond Saint-Jean and is written by Dominic Champagne. Its tagline is Le cri dune génération.

==Cast==
*André Barnard - Le Démon
*Julie Castonguay - Maria
*Dominic Champagne - Joyeux Troubadour
*Estelle Esse - Prêtresse
*Norman Helms - JeanJean
*Roger Larue - Claude Jutra
*Suzanne Lemoine - Peste
*Didier Lucien - Martin
*Jean Petitclerc - Mario
*Dominique Quesnel - Le Vieille Dame

==References==
* 
* 
* 
*  (in French)
*  (in French)

 
 
 
 


 