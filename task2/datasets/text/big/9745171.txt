Kimi yo Fundo no Kawa o Watare
{{Infobox film
| name         = Kimi yo Fundo no Kawa o Watare
| image        =
| caption      =
| writer       = Jukō Nishimura
| starring     = Ken Takakura Yoshio Harada
| director     = Junya Satō
| producer     =
| editing      =
| cinematography =
| distributor  = Shochiku
| released     = February 11, 1976
| runtime      = 151 minutes Japanese
| rating       =
| music        =
| budget       =
| preceded_by  =
| followed_by  =
}}
  is a Japanese film released in 1976, directed by Junya Satō.

== Cast ==
*Ken Takakura
*Yoshio Harada
*Kunie Tanaka
*Ryōko Nakano
*Kō Nishimura
*Hiroko Isayama
*Hideji Ōtaki

== Influence in China ==
This movie was released in China in 1978. It was the first foreign movie released in China after the Cultural Revolution which ended in 1976. It became hugely popular in China at that time. The main cast of the film Ken Takakura became an idol for an entire generation of Chinese youth. When he died of lymphoma in November 2014, a huge number of Chinese internet users expressed their sympathies and condolences, including many celebrities in the Chinese movie industry.  The spokesman of Chinas Ministry of Foreign Affairs Hong Lei said that Takakura made significant contributions to the cultural exchange between China and Japan. 

==References==
 

==External links==
* 

 

 
 
 
 
 


 