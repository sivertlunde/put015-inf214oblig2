Three Little Wolves (film)
 
{{Infobox Hollywood cartoon
| cartoon name      = Three Little Wolves Silly Symphony
| image             =
| image size        =
| alt               =
| caption           = David Hand
| producer          = Walt Disney
| story artist      =
| narrator          =
| voice actor       = Alice Ardell Billy Bletcher Pinto Colvig Leone Ledoux
| musician          = Frank Churchill
| animator          = Norm Ferguson Fred Moore Eric Larson Bill Roberts   
| layout artist     = Ferdinand Horvath
| background artist = Mique Nelson Walt Disney Productions
| distributor       = United Artists
| release date      =  
| color process     = Technicolor
| runtime           = 9 minutes
| country           = United States
| language          = English Elmer Elephant
| followed by       = Toby Tortoise Returns
}} Three Little Pigs. It introduces the Big Bad Wolfs sons, the Three Little Wolves, all of whom just as eager for a taste of the pigs as their father.

==Plot==
This short opens with the Wolf describing to his sons the edible parts of a pig. The cubs, after pelting their father with stones shot from slingshots just for a prank (first at his hat which falls off, then, as he picks up his hat, his rear), and after he threateningly exclaims that hell blow their ears off if they dont behave ("Hey, cut it out or Popll blow your ears off!"), sing and dance to "Whos Afraid of the Big Bad Wolf?" Then it fades to Fifer and Fiddler Pig doing exactly the same thing of singing and dancing. They then discover a wolf alarm (used for emergencies only) and then they discover their brother Practical Pig building a contraption called a Wolf Pacifier. Fifer and Fiddler then play around with the alarm (which is in the form of a horn) to get Practicals attention and when he discovers that it was just a trick, he warns his brothers, "Someday the Wolfll get ya. Then youll be in a fix. Youll blow that horn and I wont come. Ill think its one of your tricks." He then storms off in a huff, but not before Fifer and Fiddler scare him again by blowing the horn right behind him, causing him to fire a big hole in the top of his hat with his blunderbuss.   

Unbeknownst to Fifer and Fiddler, however, the Big Bad Wolf and his three sons are stalking them. The Wolf dresses in drag, this time as Little Bo Peep and he/she sadly tells the pigs that he/she lost his/her sheep and doesnt know where to find them. Then the pigs discover the "sheep" (the Wolfs three sons in disguise) and the Wolf and his sons, still in disguise, run home to their cave, and the pigs follow. The Wolf then locks the door and swallows the key. At first, the pigs embarrassedly think that "Bo Peep" has romantic intentions (a rather unusual scene; "Why, Bo Peep!"), but the wolves spring their trap and overwhelm the pigs. They try to blow the wolf alarm horn, but Practical doesnt come. Soon Fifer and Fiddler are soon put in a roasting pan by the wolves and they tauntingly blow the horn repeatedly. Still hoping for Practical to come to their rescue, the pigs challenge the wolf cub blowing the horn to blow it real loud ("Uh, why dont you blow it loud?"). He tries to, but cant, and the pigs taunt him that it was "a sissy blow." So the Big Bad Wolf blows the horn to prove what the Wolf family is made of ("Sissy, huh? Gimme that horn. Ill show em!"). This time, it is so loud that Practical hears ("The Wolf!") and goes to the rescue, pulling the Wolf Pacifier mechanism along behind him. 

The Wolf is about to place the pigs in the oven when he hears a knock on the door. Its Practical, disguised as an Italian vegetable peddler. He is giving a free sample on tomatoes, and the Wolf accepts the offer and comes out. He tells him to "let him have it", which Practical does - throwing a tomato in the Wolfs face. anger, the Wolf chases Practical into the Wolf Pacifier contraption. The result is the Wolf getting assaulted by the contraptions many mechanisms: buzzsawed, bashed on the head by rolling pins, kicked by boots, punched by boxing gloves (at which point, the Wolfs sons rush out of their den to see what was going on), tarred and feathered and, finally, being shot out of a cannon, with his sons following him. The short ends with the Three Little Pigs emerging from the Wolfs den, playing "Whos Afraid of the Big Bad Wolf?" patriotically (with Fifer playing a flute, Fiddler beating a homemade drum and Practical holding a white flag, which is the Wolfs pair of Bo Peep bloomers). 

==Voice cast==
* Billy Bletcher as Big Bad Wolf
* Pinto Colvig as Practical Pig
* Dorothy Compton as Fifer Pig
* Mary Moder as Fiddler Pig

==Symbolism==
While Disney produced the sequels in order to capitalize on the success of the Three Little Pigs as characters, this film in particular was also a symbolic message about the threatening danger of European fascism, and can be seen as an indication of the levels of fear and patriotism it aroused in the American populace.  In the opening scene, the Big Bad Wolf is instructing his three rowdy wolf pups in "German," pointing to a chart of pork cuts and saying "Ist das nicht ein Sausage Meat," etc., reinforcing the interpretation that he is a stand-in for Hitler.   

While the hapless Fifer and Fiddler have their naval garb, musical instruments, and professed bravado—a possible critique of European military allies who were unable to stop Hitlers advances—their confidence cannot save them from being trussed and on the verge of being deposited in the oven by the time that Practical Pig comes to their rescue. Practical Pig, the industrious "American" brother, in workmans overalls, relies on the "Italian" character for distraction, and while the Wolf is focused on his free sample of tomatoes, he is pulled into an elaborate mechanical contraption, which points to the idea that technological superiority is the secret to winning the impending war.   At one point, while receiving the mechanized pummeling from the machine, the Wolfs hair is parted and slicked down the center, producing a brief resemblance to Hitler.

==References==
 

==External links==
*  
*   at The Encyclopaedia of Disney Animated Shorts

 

 
 
 
 
 
 
 
 