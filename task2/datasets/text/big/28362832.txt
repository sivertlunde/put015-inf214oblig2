Body Troopers
{{Infobox film
| name           = Body Troopers
| image          = 
| image size     =
| caption        = 
| director       = Vibeke Idsøe
| producer       = 
| writer         = Vibeke Idsøe
| narrator       =
| starring       = Torbjörn T. Jensen   Jenny Skavlan   Kjersti Holmen   Benjamin Helstad   Lage Fosheim   Morten Faldaas   Harald Eia
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 4 October 1996
| runtime        = 91 minutes 
| country        = Norway
| language       = Norwegian
| budget         = 
| gross          =
| preceded by    =
| followed by    =
}}
 Norwegian Childrens white and red blood cells, and the vocal cords.   

The film is based on the book Jakten på nyresteinen, also written by Idsøe. 

==Awards==
{| class="wikitable"
|-
!Award !! Category !! Recipient !! Result
|-
| 1997 Chicago International Childrens Film Festival
| Adults Jury Award
| 
|    
|-
| 1997 Amanda Awards
| Best Debut
| 
|  
|-
| 1997 Fantafestival
| Best Special Effects,  Grand Prize of European Fantasy Film in Silver
| 
|  
|-
| 1997 Fantasporto
| Grand Prize of European Fantasy Film in Gold
| 
|  
|-
| 1997 Málaga International Week of Fantastic Cinema
| Best Director,  Best Film,  Best Special Effects
| 
|  
|-
|}

==See also==
* Fantastic Voyage
* Resizing (fiction)

==References==
 

==External links==
*  
*   at Filmweb.no (Norwegian)
*   at the Norwegian Film Institute

 
 
 
 
 

 
 