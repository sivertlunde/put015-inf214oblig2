The Bells of Nagasaki
{{infobox book
| name             = The Bells of Nagasaki
| title_orig       = Nagasaki no Kane
| translator       = William Johnston
| image            = The Bells of Nagasaki book cover.jpg
| caption    = The Bells of Nagasaki
| author           = Takashi Nagai
| illustrator      = 
| cover_artist     = 
| country          = Japan, United States
| language         = Japanese language|Japanese, English
| series           = 
| subject          = 
| genre            = Non-fiction Essay
| publisher        = Kodansha International
| pub_date         = January 1949
| english_pub_date = August 1994
| media_type       = Book, Music, Film
| pages            = 118
| isbn             = 978-4-7700-1845-8 
| oclc             = 
| preceded_by      = 
| followed_by      = 
}}

{{Infobox film
| name           = Nagasaki no Kane
| image          = The Bells of Nagasaki film.jpg
| image_size     =
| alt            = 
| caption        = The Bells of Nagasaki
| director       = Hideo Ōba
| producer       = 
| writer         = Takashi Nagai
| narrator       = 
| starring       = Masao Wakahara, Yumeji Tsukioka, Keiko Tsushima, Osamu Takizawa, Kōji Mitsui
| music          = Yuji Koseki
| cinematography = Toshio Ubukata
| editing        = Kaneto Shindō, Sekiro Mitsuhata, Sugako Hashida
| studio         = 
| distributor    = Shochiku Ōfuna
| released       =  
| runtime        = 94 min.
| country        = Japan Japanese
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}

  is a 1949 book by Takashi Nagai. It vividly describes his experiences as a survivor of the Atomic bombing of Nagasaki. It was translated into English by William Johnston. The title refers to the bells of Urakami Cathedral, of which Nagai writes:

These are the bells that did not ring for weeks or months after the disaster. May there never be a time when they do not ring! May they ring out this message of peace until the morning of the day on which the world ends.
 American forces occupying Japan, until an appendix was added describing Japanese atrocities in the Philippines. This appendix was later removed.

==Records and CDs==
* July 1, 1949, Song by Ichiro Fujiyama, Mariko Ike, Written by Hachiro Sato, Composed by Yuji Koseki
* September 1949, Song by Yoshie Fujiwara, Written and Composed by Kazuo Uemoto
* 1996, Song by Yumi Aikawa, Composed by Yuji Koseki

==Film==
Released September 23, 1950, Produced by Shochiku Directed by Hideo Ōba   Scripted by Kaneto Shindo, Sekiro Mitsuhata, Sugako Hashida   Composed by Yuji Koseki

===Cast===
*Masao Wakahara as Takashi Nagai
*Yumeji Tsukioka as Midori Nagai
*Keiko Tsushima as Sachiko Yamada
*Osamu Takizawa as Professor Asakura
*Kōji Mitsui as Yamashita

==See also==
* Urakami Cathedral
* Atomic bombing of Nagasaki

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 


 
 
 