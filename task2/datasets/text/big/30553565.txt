Princess and the Pony
{{Infobox film
| name           = Princess and the Pony
| image          = Princess and pony dvd.jpg
| caption        = DVD cover
| director       = Rachel Lee Goldenberg
| producer       =  
| writer         =  
| starring       =  
| music          = Chris Ridenhour
| cinematography = Adam Silver
| editing        =  
| distributor    = The Asylum 
| released       =  
| runtime        = 88 minutes
| country        = United States
| language       = English
| budget         = $17.000
| gross          = 
}}
Princess and the Pony (also known as 1st Furry Valentine) is a 2011 drama film by The Asylum.  It is The Asylums first family film not marketed under their Christian-oriented label Faith Films, deviating from the studios use of mature content on their other films.

== Plot ==
In order to protect her identity, young Princess Evelyn (Fiona Perry) is sent to live with distant relatives in America. She initially has difficulty adjusting to life in the town until she befriends a pony held captive by a shady carnival owner.

==Cast==
*Fiona Perry as Princess Evelyn Cottington
*Bill Oberst Jr. as Theodore Snyder
*Bobbi Jo Lathan as Aunt Fay
*Ron Hajak as Lawrence
*Aubrey Wakeling as Fernando
*Alison Lees-Taylor as Velora
*Jonathan Nation as Sheriff Bartelbaum
*Olivia Stuck as Becky Kim Little as Queen Matilda
*Brian Ibsen as Roberts
*Michael William Arnold as Timmy
*Twinkie as Echo the Pony

==Reception==
Commonsensemedia gave the film one star and criticized it for having "a shocking amount of violence for a movie billed as "family entertainment.""  In contrast, the Dove Foundation rated the film favorably and stated it was an "entertaining movie for the entire family" and gave it the Dove "Family-Approved" seal. 

==References==
 

==External links==
*   at The Asylum
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 

 