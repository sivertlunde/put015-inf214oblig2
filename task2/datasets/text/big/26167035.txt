The Strange Woman
{{Infobox film
| name           = The Strange Woman
| image          = The Strange Woman 1946 poster.jpg
| alt            = 
| border         = yes
| caption        = Theatrical release poster
| director       = Edgar G. Ulmer
| producer       = {{Plainlist|
* Jack Chertok
* Eugen Schüfftan
}}
| writer         = {{Plainlist|
* Hunt Stromberg
* Edgar G. Ulmer
}}
| screenplay     = Herb Meadow
| based on       =  
| narrator       = 
| starring       = {{Plainlist|
* Hedy Lamarr
* George Sanders
* Louis Hayward
}}
| music          = Carmen Dragon
| cinematography = Lucien N. Andriot
| editing        = {{Plainlist|
* John M. Foley
* Richard G. Wray
}}
| studio         = United Artists
| distributor    = United Artists
| released       =  
| runtime        = 100 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
The Strange Woman is a 1946 American dramatic thriller film directed by Edgar G. Ulmer and starring Hedy Lamarr, George Sanders, and Louis Hayward. Originally released by United Artists, the film is now in the public domain.

==Plot==
In Bangor, Maine|Bangor, Maine in 1824, a cruel young girl named Jenny Hager pushes a terrified Ephraim Poster into a river knowing he cannot swim. She is prepared to let him drown until Judge Saladine happens by, at which point Jenny jumps into the water and takes credit for saving the boys life.
 manipulative young woman. Her father, a drunken widower, whips Jenny after learning of her flirtation with a sailor. She secretly schemes to wed the richest man in town, the much older timber baron Isaiah Poster, while his son Ephraim is away to college at Cambridge.

Poster is unkind to his mild-mannered son upon Ephraims return. He is unaware that the boy and Jenny were once sweethearts and that Jenny is again flirting with Ephraim behind her husbands back. Poster is more concerned about the lawlessness in town, lumberjacks drunkenly pillaging the town, manhandling the women and killing the judge, confirming Posters long-held belief that Bangor must organize a police force.

Jenny secretly hopes that her husband will die after he falls ill. When he recovers, Poster must make a trip to his lumber camps. Jenny appeals to Ephraim to arrange his fathers death, saying, "I want you to return alone." In the rapids, both men fall from an overturned canoe and Poster drowns. His son, still deathly afraid of water, is unable or unwilling to save him.

To his shock, Ephraim returns to Jenny telling him, "You cant come into this house, you wretched coward...Youve killed your father." He becomes a hopeless drunk, hating her and speaking freely about her deceitful ways. Posters superintendent in the timber business, John Evered, goes to confront Ephraim but isnt sure whether to believe the harsh words he hears about Jenny.

Jenny proceeds to seduce Evered, who is engaged to marry her best friend, Meg Saladine, the judges daughter. Lust overtakes them during a thunderstorm. After their wedding, Evered is eager to have children, but Jenny learns she cannot bear any.

A traveling evangelist, Lincoln Pettridge, preaches fire and brimstone that results in Jennys confession to her husband that everything Ephraim said about her was true. Evered goes off to be by himself at a lumber camp, where Meg comes to persuade Evered to go back to his wife. Jenny, however, discovers them together, violently whips her horse and tries to run them down with her carriage. It careens off a cliff and she is killed.

==Cast==
 
* Hedy Lamarr as Jenny Hager
* George Sanders as John Evered
* Louis Hayward as Ephraim Poster
* Gene Lockhart as Isaiah Poster
* Hillary Brooke as Meg Saladine Rhys Williams as Deacon Adams
* June Storey as Lena Tempest
* Moroni Olsen as Rev. Thatcher
* Olive Blakeney as Mrs. Hollis
* Kathleen Lockhart as Mrs. Partridge
* Alan Napier as Judge Henry Saladine
* Dennis Hoey as Tim Hager
* Christopher Severn as Ephraim Poster (as a child)

==Production==
The production dates were from December 10, 1945 to mid-March 1946 at the Samuel Goldwyn Studios.  Hedy Lamarr and Jack Chertok formed a partnership to produce this film. Production on the film was shut down between December 13, 1945 and January 3, 1946 due to Hedy Lamarrs bout with the flu.

The film went over budget by $1 million but was a moderate success at the box office. {{cite book
 | last = Balio
 | first = Tino
 | authorlink =
 | coauthors =
 | title = United Artists: The Company Built by the Stars
 | date = 2009
 | publisher = University of Wisconsin Press
 | isbn = 978-0-299-23004-3
 }} p203 
==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 