The Big House (1930 film)
  
{{Infobox film
| name = The Big House
| image = The Big House film poster.jpg
| caption = Original lobby card depicting Chester Morris and Wallace Beery George Hill
| producer = no credit
| screenplay = Story and dialogue by Frances Marion Additional dialogue by Joe Farnham and Martin Flavin
| based on = Robert Montgomery Leila Hyams George F. Marion J. C. Nugent
| music = no credit
| cinematography = Harold Wenstrom
| editing = Blanche Sewell
| studio = A Cosmopolitan Production
| distributor = Metro-Goldwyn-Mayer
| released = June 24, 1930 
| runtime = 87 minutes
| language = English
| country = United States
| budget = 
}} George Hill, Robert Montgomery, and released by Metro-Goldwyn-Mayer.. The story and dialogue were written by Frances Marion, with additional dialogue by Joe Farnham and Martin Flavin. {{cite AV media
 | people = Metro-Goldwyn-Mayer 
 | title = The Big House
 | medium = DVD
 | publisher = Warner Bros. Archive Collection
 | location = 
 | date = 2009
 | url = }} 
 Best Actor in a Leading Role, he became the worlds highest paid actor within two years.
 Academy Award Best Picture. prison films ever made and was tremendously influential on the genre. 

==Plot== Robert Montgomery), a young law-abiding man kills someone while driving drunk, is sentenced to ten years for manslaughter. In an overcrowded prison designed for 1800 and actually holding 3000, he is placed in a cell with Butch (Wallace Beery) and Morgan (Chester Morris), the two leaders of the inmates. Butch is alternately menacing and friendly, while Morgan tries to help out the frightened, inexperienced youngster, but Kent rebuffs his overtures.

When Butch is ordered into solitary confinement for sparking a protest over the unappetizing food, he passes along his knife before being searched. It ends up in Kents hands. Meanwhile, Morgan is notified that he is to be paroled. Out of spite, Kent hides the knife in Morgans bed. When it is found, Morgans parole is canceled, and he is put in solitary as well. He vows to make Kent pay for what he has done.

When Morgan is let out of solitary, he escapes by switching places with a corpse. He makes his way to the bookstore run by Kents beautiful sister, Anne (Leila Hyams). She, however, recognizes him. She manages to get his gun and starts to call the police, but then changes her mind and gives him back his pistol. Morgan (who has been attracted to Anne since he saw Kents photograph of her) gets a job and becomes better acquainted with Anne and her family. They all like him, especially Anne. However, he is caught and sent back to prison.

When Butch tells Morgan of his plan for a jailbreak on Thanksgiving, Morgan tells him that he is going straight. In return for a promise of freedom, Kent informs the warden (Lewis Stone) of the attempt, though he is not privy to the details.

Despite the warning, the inmates succeed in taking over the prison, capturing many of the guards, though they are unable to force their way out. Thwarted, Butch threatens to shoot the guards one by one unless they are allowed to escape. When the warden stands firm, Butch shoots the wardens right-hand man in cold blood, then tosses the dying man out for all to see. Army tanks are summoned to break down the entrance. Morgan grabs a pistol from the prisoner assigned to watch the guards. He finds Kent cowering with the guards, but spares him. Kent panics and flees before Morgan locks the guards in to save their lives. When Kent tries to open the front doors, he is killed in the crossfire. Butch is told that Morgan was the "stoolie" who tipped off the warden and learns that he has also put the guards beyond his murderous reach. He sets out to kill his former friend. In the ensuing gunfight, both are wounded, Butch fatally. Before he dies, he learns that Kent was the traitor, and he and Morgan reconcile.

For his efforts, Morgan is given a full pardon. When he exits the prison, Anne rushes to embrace him.

==Foreign Language Versions==
In the early days of sound films, it was common for Hollywood studios to produce "Foreign Language Versions" of their films using the same sets, costumes and so on. While many of these versions no longer exist, the French and German-language versions of The Big House survive, which are entitled Révolte dans la prison and Menschen Hinter Gittern.

==Cast==
  
*Chester Morris as Morgan
*Wallace Beery as Butch
*Lewis Stone as Warden Robert Montgomery as Kent
*Leila Hyams as Anne
*George F. Marion as Pop
*J. C. Nugent as Mr. Marlowe
*Karl Dane as Olsen
*DeWitt Jennings as Wallace
 
*Mathew Betz as Gopher
*Claire McDowell as Mrs. Marlowe
*Robert Emmet OConnor as Donlin Tom Kennedy as Uncle Jed Tom Wilson as Sandy
*Eddie Foyer as Dopey
*Rosco Ates as Putnam
*Fletcher Norton as Oliver
 

==Reception==
Mordaunt Hall of The New York Times described it as "a film in which the direction, the photography, the microphone work and the magnificent acting take precedence over the negligible story."  Variety (magazine)|Variety called it a "virile, realistic melodrama".  John Mosher of The New Yorker wrote, "So expert are many of the scenes, so effective the photography, so direct and spare the dialogue, that certain obvious, silly, and dull moments may almost be overlooked."  

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 