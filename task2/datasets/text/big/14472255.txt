5ive Girls
{{Infobox Film
| name           = 5ive Girls
| image          = 5ve-girls-movie.jpg
| caption        = 
| director       = Warren P. Sonoda
| producer       = Curtis Petersen Gary Howsam
| writer         = Warren P. Sonoda Jennifer Miller  Jordan Madley  
| music          = Craig McConnell
| cinematography = David Mitchell Curtis Petersen
| editing        = James P. Villeneuve
| distributor    = Peace Arch Entertainment Group
| released       = 2006
| runtime        = 95 min.
| country        = Canada English
| budget         = $3,000,000 (est.)
| gross          = 
}} Jennifer Miller, and Jordan Madley. It was written and directed by Warren P. Sonoda and has been available on DVD since March 6, 2007.

==Plot== Catholic boarding school for girls.  One day, a young student, Elizabeth, is studying in a classroom on the third floor, when without warning, she is attacked by unseen evil forces. One of St. Marks priests, Father Drake, attempts to save Elizabeth from the apparently demonic aggressor, but he quickly proves to be powerless against it, and the girl vanishes without a trace.

The school is immediately shut down, and all of the students are removed from campus by worried families.  Five years later, the school reopens.  A harsh Head teacher|headmistress, Miss Pearce, rules the girls with an iron fist; Father Drake remains at the school as a teacher, but because of the disappearance of Elizabeth he has become a drunkard, found by Miss Pearce at a bar. Five troubled and unwanted girls are left by their families at the school: Alex, Mara, Cecilia — who is blind, Leah, and Connie.  The girls are strictly forbidden to go to the third floor (the site of Elizabeths disappearance). Cecilia and Mara enter the third floor, prompting Miss Pearce to punish the responsible party.  Alex takes the blame and is severely beaten with a ruler.
 possessed by a demon. Other strange things begin to happen, revealing that all five of the girls possess supernatural gifts.  Connie is a "conduit," or a magnet for spirit activity, Leah can pass through objects (though not doors or walls), Cecilia has "second sight" or what the viewer could interpret as ESP, Mara can heal recent wounds (she heals Alex after her beating), and Alex has Psychokinesis|telekinesis.

It is revealed that Miss Pearce has brought the girls to the school for a very specific reason and is seen conjuring them to a pentagram on the third floor, after which Connie appears to be possessed.  A possessed Connie then attempts to drown Leah and the demon passes into her while Connie falls dead.  Leah/ .  They argue, Father Drake attempts to exorcise her, and Leah uses her newfound demon powers to stab him with gold crucifixes.  Alex, Mara, and Cecilia have been reading Elizabeths journal (that appeared after Connies possession) and have learned enough about Legion to know that they need to escape.  They split up to search for the others, Mara finding Father Drake, Alex finding Connie, and Cecilia running into the demon.

After a prolonged fight where Cecilia is severely bloodied and Leahs head is smashed in with a book, Legion moves on to Cecilia and grants her "first sight."  Terrified, Mara and Alex try to flee, but Ms. Pearce locks them in and breaks Maras healing hand.  It is revealed that she is Elizabeths sister, trying to save her from Legion.  Mara and Alex then hole up in the bedroom and use Connies spellbook to create a protective circle.  Cecilia/Legion finds them and while cannot initially penetrate the circle, uses her own blood to cover over the lines and then possesses Mara.

Miss Pearce is in another part of the building, chanting and Elizabeths body slowly begins to appear before vanishing again.  Mara is chasing Alex, who gets stabbed in the stomach.  Miss Pearce begs Legion to let Elizabeth go, but they state that they only have 4 girls and the deal was for 5.  Alex then uses her own powers of telekinesis to force Legion out of Mara and into Miss Pearce before ramming the demons head through a crucifix.  Mara and Alex collapse and presumably several hours later, Mara awakes and heals herself, but is unable to heal Alex in time. She begins to leave and encounters Virgil (a man in monkish robes and appears to be a type of groundskeeper and is seen briefly throughout the movie).  Elizabeth comes running down the stairs, whole and alive, and greets him as father (cue Maras exit).  He is happy and excited, until a bloody Miss Pearce grabs him by the throat and transfers Legion to him.  Elizabeth screams and cries as the film ends.

==Cast==
*Ron Perlman as Father Drake Jennifer Miller as Alex/telekinetic
*Jordan Madley as Mara/healer
*Terra Vnesa as Cecilia/psychic Legion
*Tasha May as Connie/Conduit
*Amy LaLonde as Miss Pearce
*Krysta Carter as Elizabeth
*James Kidnie as Virgil
*Richard Alan Campbell as Mr. Garrison

==External links==
*  

 
 
 
 
 
 
 