Patch Adams (film)
{{Infobox film
|name=Patch Adams
|image=Patch Adams.jpg
|caption=Theatrical release poster
|director=Tom Shadyac Barry Kemp Marvin Minoff Charles Newirth Marsha Garces Williams
|screenplay=Steve Oedekerk based on=  
|starring=Robin Williams Monica Potter Philip Seymour Hoffman Bob Gunton Daniel London Peter Coyote
|music=Marc Shaiman
|cinematography=Phedon Papamichael Jr. Don Zimmerman
|studio=Blue Wolf Bungalow 78 Productions Faller/Minoff Universal Pictures
|released= 
|runtime=115 minutes
|country=United States
|language=English
|budget=$90 million
|gross=$202.3 million
}}

Patch Adams is a 1998 semi-  alone.

==Plot== Dean Walcott (Bob Gunton), who believes that doctors must treat patients as patients and not bond with them as people. Because of this and incidents such as setting up a giant pair of legs during an obstetric conference, he is expelled from the medical school, although he is later reinstated due to his methods actually helping patients improve. He even goes to a meat packers convention and uses a butchers jacket that he is given to impersonate a second-year med-student and infiltrate rounds. Adams encourages medical students to work closely with nurses, learn interviewing skills early, and argues that death should be treated with dignity and sometimes even humor. 
 renovates an medical insurance comedy sketches for them.
 molested her, Larry murders reincarnated and it revives his spirits, and he decides to continue his work in her honor.

However he is expelled from medical school a second time for running a clinic and practicing medicine without a license. He files a grievance with the state medical board at the advice of his former medical school roommate, Mitch Roman (Philip Seymour Hoffman). Patch is able to convince the board that he did his best to help the people that came to him, and as a doctor it is his responsibility to treat the body and the spirit. The board accepts Patchs methods and decides to allow him to graduate. He receives a standing ovation from the packed hearing room. 
 nude bottom under his gown.

==Cast==
* Robin Williams as Patch Adams|Dr. Hunter "Patch" Adams
* Daniel London as Truman Schiff
* Philip Seymour Hoffman as Mitch Roman
* Monica Potter as Carin Fisher
* Bob Gunton as Dean Walcott
* Frances Lee McCain as Judy
* Irma P. Hall as Joletta
* Josef Sommer as Dr. Eaton
* Harold Gould as Arthur Mendelson
* Harve Presnell as Dean Anderson
* Michael Jeter as Rudy
* Barry Shabaka Henley as Emmet
* Richard Kiley as Dr. Titan
* Ryan Hurst as Neil
* Peter Coyote as Bill Davis
* Alan Tudyk as Everton
* Dot Marie Jones as Miss Meat
* Douglas Roberts as Lawrence "Larry" Silver
* Norman Alden as Truck Driver
* James Greene as Bile
* Greg Sestero as Jaime
* Ralph Peduto as Organizer

==Production notes== Treasure Island, UC Berkeley.
 producers worked very closely with the Make-A-Wish Foundation. For this reason, all but two of the ill children acting in this film actually have cancer. 

The film has several major departures from Adams real history.  One is that the character of Carin is fictional, but is analogous to a real life friend of Adams (a man) who was murdered under similar circumstances.  Another difference is the then-47 year old Robin Williams portrays Adams as enrolling in medical school very late in his life, his older age even being brought up in dialogue.  In reality, Adams started medical school immediately and his educational progress was quite normal for a physician:  He graduated high school at 18, college at 22, and medical school at 26.

==Reception==

===Box office performance===
The film was released on December 25, 1998 in the United States and Canada and grossed $25.2 million in 2,712 theaters its opening weekend, ranking #1 at the box office. After its first weekend, it was the #2 film for four weeks.  The film grossed USD|US$202,292,902 worldwide &mdash; $135,026,902 in the United States and Canada and $67,266,000 in other territories. 

===Critical reception===
The review aggregator Metacritic reported that the film averaged a rating of 25%,  and currently has a score of 23% on Rotten Tomatoes based on 56 reviews with the consensus reading, Syrupy performances and directing make this dramedy all too obvious.  Noted Chicago Sun-Times film critic Roger Ebert gave the film one and a half stars out of four and wrote,  "Patch Adams" made me want to spray the screen with Lysol. This movie is shameless. Its not merely a tearjerker. It extracts tears individually by liposuction, without anesthesia.  Robert K. Elder of the Chicago Tribune called Monica Potter "the best thing about the otherwise dopey Patch Adams." 

It received "Two Thumbs Down" on the television series Siskel & Ebert, with particular criticism towards the character of Patch who they viewed as "overbearing", "obnoxious" and "sanctimonious" as well as noting they would never trust a doctor who acted like Adams did. Gene Siskel said I would rather turn my head and cough than see another moment of Patch Adams again;  he would later single it out as the worst film of 1998, the last film he would ever give a "Worst of" to before his death.

=== Awards === Best Motion Best Actor – Motion Picture Musical or Comedy (Robin Williams).

== Reaction of the real Patch Adams == real Patch Adams has been openly critical of the film, saying that it sacrificed much of his message in order to make a selling film. He also said that out of all aspects of his life and activism, the film portrayed him merely as a funny doctor.    At a Conference of World Affairs, he told film critic Roger Ebert, "I hate that movie." 

During a speech in 2010 at the Mayo Clinic, Patch Adams said, "The film promised to build our hospital. None of the profits from the film ever came to us, and so, basically 40 years into this work, we are still trying to build our hospital."  

===Adams thoughts on Williams===
Furthermore, Adams stated,  "  made $21 million for four months of pretending to be me, in a very simplistic version, and did not give $10 to my free hospital. Patch Adams, the person, would have, if I had Robins money, given all $21 million to a free hospital in a country where 80 million cannot get care."   However, in another interview, Adams did clarify that he did not hate Williams,  and Williams actively supported St. Jude Childrens Research Hospital for several years. 

After Williamss death in 2014, Adams said,

"The terrible news of the passing of Robin Williams reached me here in the Peruvian Amazon late Monday night with tremendous sadness. Surrounded by over 100 friends and clowns on our annual clown trip, we mourn this tragic loss and continue to treasure his comic genius. Robin Williams was a wonderful, kind and generous man. One important thing I remember about his personality is that he was unassuming—he never acted as if he was powerful or famous. Instead, he was always tender and welcoming, willing to help others with a smile or a joke. Robin was a brilliant comedian—there is no doubt. He was a compassionate, caring human being. While watching him work on the set of the film based on my life—Patch Adams–I saw that whenever there was a stressful moment, Robin would tap into his improvisation style to lighten the mood of cast and crew. Also, I would like to point out, Robin would be especially kind toward my children when they would visit the set. Contrary to how many people may view him, he actually seemed to me to be an introvert. When he invited me and my family into his home, he valued peace and quiet, a chance to breathe—a chance to get away from the fame that his talent has brought him. While early in life, he turned to drug use and alcohol to escape, he replaced the addiction with moments of solitude to help cope with the stress that fame brought. This world is not kind to people who become famous, and the fame he had garnered was a nightmare. While saddened, we are left with the consequences of his death. I’m enormously grateful for his wonderful performance of my early life, which has allowed the Gesundheit Institute to continue and expand our work. We extend our blessings to his family and friends in this moment of sadness. Thank you for all you’ve given this world Robin, thank you my friend." 

==See also==
* List of films featuring diabetes
==References==
 
==External links==
 
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 