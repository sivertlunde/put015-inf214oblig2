Dr. Black, Mr. Hyde
 
 
{{Infobox Film
| name           = Dr. Black, Mr. Hyde
| image          = Drblackmrhyde.jpg
| caption        = Dr. Black, Mr. Hyde film poster William Crain
| producer       = Charles Walker
| eproducer      =
| aproducer      =
| writer         = Lawrence Woolner
| starring       = Bernie Casey Rosalind Cash Marie OHenry Ji-Tu Cumbuka Milt Kogan Stu Gilliam
| music          = Johnny Pate
| cinematography = Tak Fujimoto
| editing        = John C. Horger Dimension Pictures Inc.
| released       = January, 1976
| runtime        =
| country        =
| language       =
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}
 1976 blaxploitation William Crain, who had also directed the successful Blacula for American International Pictures in 1972 in film|1972.

==Plot summary== Watts projects.

Motivated by reasons that seem to be connected to the traumatic death of his mother when he was younger, Pryde begins a series of unorthodox experiments to test his serum.  First he injects a dying elderly patient with the treatment, causing a violent reaction that causes the black womans skin and hair to briefly turn white.  When Dr. Worth, who is clearly in love with Henry, objects to his unethical experiment, Henry uses the serum on himself.  The results are disastrous: he transforms into a hulking white creature.

From here, Prydes mental condition seems to rapidly deteriorate.  He has a genuine affection for a young woman named Linda (Marie OHenry), a patient he treats for hepatitis at the clinic.  Linda is a prostitute, and when Dr. Pryde unexpectedly asks her out on a date, he reveals to her that he harbors resentment over the death of his mother when he was a child; Prydes mother worked as a cleaning woman in a bordello, and the prostitutes refused to help her when she fell ill, resulting in her death.  Linda is touched by Prydes revelation, until he takes her to his house and tries to get her to take the serum.  She refuses but Pryde turns forceful, and Linda tries to deflect him by asking him to take the injection first just to prove that it is safe.  Pryde transforms and tries to attack her, but she escapes.

Seemingly beyond control now, Pryde begins murdering the prostitutes and pimps that Linda works with.  When the police begin to investigate, Linda is tormented by the fact that she knows who the murderer is, yet feels helpless to stop it.  Pryde realizes that Linda will give him away, and he attacks her as Mr. Hyde, dragging her to the Watts Towers.  Confronted by police, Hyde releases Linda and climbs the towers, where he is shot down and falls to his death.

==External links==
*  
*  

 

 
 
 
 


 