Parakkum Paavai
{{Infobox film
| name           = Parakkum Paavai
| image          = Parakkum Paavai.jpg
| image_size     =
| caption        =
| director       = T. R. Ramanna
| producer       = T. R. Ramanna
| writer         = Sakthi T. K. Krishnasamy
| narrator       = Kanchana J. P. Chandrababu M. N. Nambiar S. A. Ashokan
| music          = M. S. Viswanathan
| cinematography = M. A. Rehman
| editing        = M. S. Mani
| studio         = R. R. Pictures
| distributor    = R. R. Pictures
| released       = 11 November 1966 
| runtime        = 171 mins
| country        = India Tamil
| budget         =
| gross          =
| website        =
}}
 Indian Tamil Tamil  Kanchana in the lead role. The film ran for 80 days. 

== Plot ==
Jeeva (M. G. Ramachandran) promised Védhatchalam (Chittor V. Nagaiah), the father of Kala (B. Saroja Devi), to keep an eye on Kala. In order to do so, he becomes a trapeze artist in the National Cirucs in 1960s India.

==Cast==

{| class="wikitable" width="72%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
| M. G. Ramachandran ||  as Jeeva (The trapeze artist)
|-
| B. Saroja Devi || as Kala Devi Védhatchalam (The female trapeze artist)
|-
| J. P. Chandrababu || as Rubber (The clown)
|-
| R. S. Manohar || as Kabali (The trapeze artist)
|-
| K. A. Thangavelu || as The director of The National Circus
|-
| S. V. Ramadoss || as Veeramuthu (The trapeze artist)
|-
| Chittor V. Nagaiah || as Védhatchalam
|-
| O. A. K. Devar || as Balasubramanium, Védhatchalams family advocat
|-
| M. N. Nambiar || as Rangaswamy, Védhatchalams family doctor
|-
| S. A. Ashokan || as Vidjayan (The tamer of big cats)
|-
| N. S. Natarajan || as Dandhabany (The mysterious killer)
|-
| Sadhan (Pattam) || as A thin man of the circus, the leader of the group of the men on "Nilavenna Aadi"
|-
| Raja Sulochana || as Sarusa Védhatchalam, Kabalis wife
|- Kanchana || as Shantha (The nurse)
|-
| G. Sakunthala || as Kannamma, the 2nd wife of Védhatchalam
|-
| S. D. Subbulakshmi || as Amuniyamma, Jeevas mother
|- Manorama || as Dilruba, Rubbers wife
|-
| Madhavi || as Ritha, the director of The National Circuss wife
|-
| Kala || as 
|-
| Mala || as
|}

The casting is established according to the original order of the credits of opening of the movie, except those not mentioned

==Around the movie==

It reigns over PARAKKUM PAAVAI a suspense deserving of Alfred Hitchcocks movies !
 	
It was the eighth and last adventure between MGR and director-producer, T.R.Ramanna.
 
  
 	
As it was the case on NALA NERAM for the producer Chinnappa Devar, PARAKKUM PAAVAI was for T.R.Ramanna, his first movie in color with the same MGR.
 	
They enclosed their collaboration with MGR by a movie in color.
 
The movie starts by a credits in the form of cartoon create by Raki about 2 minutes and 40 seconds.
 	
The director T.R.Ramanna magnificently exploited well the environment of the circus. 
 	
The numbers (trapeze artists, jugglers, tamers, etc...) presents in the movie, are very well filmed by its camera.
 	
The soundtrack of M.S.V. is always sublime.
 
K.J.Yésudass sings for the first time in a movie of MGR. Here, he lends his voice for the comedian J.P.Chandrababu in the song "Sugam Yethilae...".

==Songs==
The music composed by M. S. Viswanathan. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Kannadasan || 04:25
|-
| 2 || Mutthamo Mogamo || T. M. Soundararajan, L. R. Eswari || 06:15
|-
| 3 || Nilavenna Aadi || T. M. Soundararajan, P. Susheela || 03:18
|-
| 4 || Sugam Yethilae || T. M. Soundararajan, L. R. Eswari, K. J. Yesudas || 04:09
|-
| 5 || Unnaiththaaney || T. M. Soundararajan, P. Susheela || 03:19
|-
| 6 || Yaaraithan Nambuvadho || P. Susheela || 03:08
|}

==References==
 

==External links==
 

 
 
 
 


 