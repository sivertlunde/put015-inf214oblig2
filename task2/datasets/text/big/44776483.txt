Slave Girl (film)
{{Infobox film
| name           = Slave Girl
| image          = 
| image size     =
| caption        = 
| director       = Charles Lamont
| producer       =
| writer         =
| narrator       =
| starring       =Yvonne de Carlo
| music          =
| cinematography =
| editing        =
| distributor    = 
| released       = 1947
| runtime        =
| country        = United States English
| budget         =
| gross          = 
}}
Slave Girl is a 1947 American film starring George Brent and Yvonne de Carlo directed by Charles Lamont.
==Production==
The film was originally called The Flame of Tripoli and budgeted at $1.6 million. MEREDITH TO PLAY PRESIDENT MADISON: Signed by Skirball-Manning for The Magnificent Doll, With Ginger Rogers and Niven Of Local Origin
Special to THE NEW YORK TIMES.. New York Times (1923-Current file)   26 Apr 1946: 29.   It was envisioned as a melodrama but during the shoot the filmmakers decided to add comedy. Previews were not encouraging so further reshoots were done, including adding a comic camel. This appeared to work. CONTRACT TIME IN HOLLYWOOD: Actors Guild Presents Demands -- Soviet Spy Film -- Other Items
By THOMAS F. BEADY. New York Times (1923-Current file)   13 Apr 1947: 69. 
==References==
 
==External links==
*  at TCMDB
 
 