George's Island (film)
{{Infobox film
| name           = Georges Island Paul Donovan Paul Donovan Bob Hicks Lorenzo P. Lampthwait Maura OConnell J. William Ritchie Stefan Wodoslawski Paul Donovan
| starring       = Ian Bannen Sheila McCarthy Maury Chaykin Nathaniel Moreau Vickie Ridler
| music          = Marty Simon
| cinematography = Les Krizsan
| editing        = Stephan Fanfara
| released       =  
| runtime        = 89 minutes
| country        = Canada
| language       = English
}}
Georges Island is a Canadian family adventure film. It was shot in and around Halifax, Nova Scotia|Halifax, Nova Scotia. 

==Plot== Brian Downey and Irene Hogan) who keep him locked up in a basement cell. On Halloween night, George and the Beanes other adoptee, Bonnie (Vickie Ridler), escape with Captain Waters and head for safety on Georges Island. Miss Birdwood and Mr. Droonfield give chase, but they accidentally awaken the ghosts of Captain Kidd (Gary Reineke) and his men, who think that the intruders are after their chest of gold. 
==Cast==
*Ian Bannen as Captain Waters
*Sheila McCarthy as Miss Birdwood
*Maury Chaykin as Mr. Droonfield
*Nathaniel Moreau as George Waters
*Vickie Ridler as Bonnie Brian Downey as Mr. Beane
*Irene Hogan as Mrs. Beane
*Gary Reineke as Captain Kidd

==Inspiration==
Georges Island is an actual island in the Halifax Harbour. Local maritime folklore holds tales of pirates seeking revenge over buried treasure stolen from the island. These tales inspired the script for the film. 

==Reception==
Upon its release, the film went relatively unnoticed. It was overshadowed by films with larger production and advertising budgets. It was only shown in Canadian theatres for three weeks. 

Georges Island was judged the best live-action film at the 1990 Chicago International Festival of Childrens Films. 

Currently, there are no critical reviews of the film on Rotten Tomatoes.    

==References==
 