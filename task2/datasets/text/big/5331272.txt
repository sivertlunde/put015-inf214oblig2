Spin (1995 film)
 
{{Infobox Film  name         = Spin image        =  director     = Brian Springer starring     = Al Gore Bill Clinton Laura Bush Jon Voight George H. W. Bush Pat Robertson Larry Agran writer       = Brian Springer released     = 1995 runtime      = 56 min language  English
}}

 
 presidential election, Operation Rescue abortion protests. 

Using the 1992 presidential election as his springboard, Springer captures the behind-the-scenes maneuverings of politicians and newscasters in the early 1990s. Pat Robertson banters about "homos," Al Gore learns how to avoid abortion questions, George H. W. Bush talks to Larry King about Halcion—all presuming theyre off camera. Composed of 100% unauthorized satellite footage, Spin is a surreal expose of media-constructed reality.   

The film documents behind the scenes footage of Larry Agran who unsuccessfully sought the Democratic Party nomination for President. Agran was generally ignored by the media during his candidacy, a topic covered in the documentary. The media did not report his polling numbers even as he met or exceeded the support of other candidates such as Jerry Brown. Party officials excluded him from most debates on various grounds, even having him arrested when he interrupted to ask to participate. When he managed to join the other candidates in any forum, his ideas went unreported. 

Spin is a followup of the 1992 film Feed; for which Springer provided much of the raw satellite footage.

== References ==
 

== External links ==
*  , watch online or download from Archive.org
*  
*  
*   at 0xDB
*   (Video Data Bank)

 
 
 
 


 