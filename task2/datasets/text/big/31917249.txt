The Extraordinary Waiter
{{Infobox film
| name           = The Extraordinary Waiter
| image          = TheExtraordinaryWaiter.jpg
| image_size     = 
| caption        = Screenshot from the film
| director       = Walter R. Booth
| producer       = Robert W. Paul
| writer         = 
| narrator       =
| starring       =
| music          =
| cinematography =
| editing        =
| studio         = Pauls Animatograph Works
| distributor    =
| released       =  
| runtime        = 1 minute 16 secs
| country        = United Kingdom Silent
| budget         =
}}
 1902 UK|British short  silent comedy film, directed by Walter R. Booth, featuring a brutish colonialist failing to destroy a blackfaced waiter. The film, "makes for somewhat uncomfortable viewing," but according to Michael Brooke of BFI Screenonline, "its just about possible to read this as a metaphor for the rather more widespread frustrations arising from British colonial rule (the Boer War was still a current issue), though it seems unlikely that this was intentional on Booths part."   

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 


 
 