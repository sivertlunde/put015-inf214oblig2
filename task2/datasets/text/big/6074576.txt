The Living End (film)
{{Infobox film
| name           = The Living End
| image          = The Living End (poster).jpg
| image_size     = 
| alt            = 
| caption        = Theatrical release poster
| director       = Gregg Araki
| producer       = Jon Gerrans Marcus Hu Jim Stark
| writer         = Gregg Araki
| starring       = Mike Dytri Craig Gilmore
| music          = Cole Coonce Sascha Konietzko
| cinematography = Gregg Araki
| editing        = Gregg Araki
| studio         = October Films
| distributor    = Cineplex Odeon Films
| released       =  
| runtime        = 84 minutes  
| country        = United States
| language       = English
| budget         = $22,769
| gross          = $692,585 
}} Thelma and Louise," the film is an early entry in the New Queer Cinema genre. The Living End was nominated for a Grand Jury Prize at the Sundance Film Festival in 1992.

==Plot== homophobic police officer, they go on a road trip with the motto "Fuck everything."

==Cast==
* Mike Dytri as Luke
* Craig Gilmore as Jon
* Mark Finch as Doctor
* Mary Woronov as Daisy
* Johanna Went as Fern
* Darcy Marta as Darcy
* Scot Goetz as Peter
* Bretton Vail as Ken
* Nicole Dillenberg as Barbie Magie Song as the 7-11 couple
* Peter Lanigan, Jon Gerrans, and Jack Kofman as Three Stooges
* Chris Mabli as a Neo-Nazism|Neo-Nazi
* Michael Now as Tarzan
* Michael Haynes as Jane
* Peter Grame as Gus
* Craig Lee and Torie Chickering as the arguing couple at Ralphs Buddhist
* Paul Bartel as Twister master

==Music==
The films soundtrack is mostly Industrial music|industrial, post punk and shoegazing music. Many references to bands and their members are made throughout the film. Joy Divisions Ian Curtis is mentioned, along with Dead Can Dance, Echo & the Bunnymen and others. A Nine Inch Nails sticker is on the dashboard of Jons car. The films title comes from a song by The Jesus and Mary Chain, and a cover version of the JAMC song is performed by Wax Trax! Records artists Braindead Soundmachine during the films credits. Early in the movie, Luke is seen wearing a JAMC shirt. Braindead Soundmachine guitarist Cole Coonce is credited with scoring the films original music.

==Reception==
Janet Maslin of The New York Times found The Living End to be "a candid, freewheeling road movie" with "the power of honesty and originality, as well as the weight of legitimate frustration. Miraculously, it also has a buoyant, mischievous spirit that transcends any hint of gloom." She praised Araki for his solid grasp on his lead characters plight and for not trivializing it or inventing an easy ending.  Conversely, Rita Kempley for The Washington Post called the film pretentious and Araki a "cinematic poseur" along the lines of Jean-Luc Godard and Andy Warhol. The Living End, she concluded, "is mostly annoying".  Rolling Stone|Rolling Stones Peter Travers found The Living End a "savagely funny, sexy and grieving cry" made more heart-rending by "Hollywoods gutless fear of AIDS movies". 
 Robert Patrick, Quentin Crisp called the film "dreadful." 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 