Aghaat
{{Infobox film
| name           = Aaghat
| image          = Aghaat(1985).jpg
| image_size     = 200px
| caption        = Film poster
| director       = Govind Nihalani
| producer       = Manmohan Shetty
| writer         = Vijay Tendulkar
| narrator       =
| starring       = Naseeruddin Shah Om Puri Bharath Gopi
| music          = Vanraj Bhatia Ajit Varman
| lyrics         = Vasant Deo
| cinematography = Govind Nihalani
| editing        = Sutanu Gupta
| studio         = Neo Films Association
| distributor    =
| released       =  
| runtime        =
| country        = India
| language       = Hindi
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}

Aaghat is a 1985  Bollywood movie directed by Govind Nihalani and produced by Manmohan Shetty under the banner Neo Films Association. It stars Om Puri, Naseeruddin Shah and Bharath Gopi in lead roles while Amrish Puri, Pankaj Kapoor and Deepa Sahi and Sarika in supporting roles.  

==Cast==
*Bharath Gopi-Krishnan Raju
*Om Puri-Madhav Verma
*Sadashiv Amrapurkar-VP Sarnaik
* K K Raina- Paranjpe
* M K Raina- Dubey
*Rohini Hattangadi-Mrs. Ali - Social Worker
*Pankaj Kapoor-Chotelal
*Naseeruddin Shah-Rustom Patel
*Deepa Sahi-Chotelals wife
*Amrish Puri-Chakradev
*Harish Patel-Chotelal brother
*Achyut Potdar-Chotelals elder brother

==References==
 

== External links ==
*  

 

 
 
 

 