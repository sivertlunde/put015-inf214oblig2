The Red Lily
{{Infobox film
| name           = The Red Lily
| image          = 
| caption        = 
| director       = Fred Niblo
| producer       = Fred Niblo
| writer         = Fred Niblo Bess Meredyth
| starring       = Ramon Novarro Enid Bennett
| cinematography = Victor Milner
| editing        = Lloyd Nosler
| distributor    = Metro-Goldwyn
| released       =  
| runtime        = 81 minutes
| country        = United States 
| language       = Silent with English intertitles
| budget         = 
}}
 silent drama film directed by Fred Niblo, starring Ramon Novarro, Enid Bennett and Wallace Beery.    A print of the film exists.   

==Plot==
Jean Leonnec (Ramon Novarro) and Marise La Noue (Enid Bennett) are pennyless lovers who eloped to Paris.  However, they are separated shortly after their arrival, leading to a downward spiral in each of their lives.  She becomes a prostitute known as the red lily; he learns the ways of the underworld from Bo-Bo (Wallace Beery).

==Cast==
* Ramon Novarro - Jean Leonnec
* Enid Bennett - Marise La Noue
* Frank Currier - Hugo Leonnec
* Mitchell Lewis - DAgut
* Rosita Marstini - Madame Charpied (as Risita Marstini) Sidney Franklin - M. Charpied - Her Husband
* Wallace Beery - Bo-Bo George Nichols - Concierge
* Emily Fitzroy - Mama Bouchard
* George Periolat - Papa Bouchard
* Rosemary Theby - Nana
* Milla Davenport - Madame Poussot
* Gibson Gowland - Le Turc
* Dick Sutherland - The Toad
*Marcelle Corday - woman in bar (uncredited)

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 


 