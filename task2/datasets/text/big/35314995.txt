Trumped (film)
{{Infobox film name          =Trumped image         =Official_Trumped_One_Sheet_Poster.jpg caption       =Theatrical release poster director      =Michael Whitton producer      =Kellie Maltagliati writer        =Michael Whitton starring      =James James Matt Kawczynski David Whitton music         =Michael Whitton
|cinematography=Sean Daly editing       =Michael Whitton studio        =ON3 Films Ask Around Productions distributor   =3 Walls Entertainment released      =  runtime       =7 minutes country       =United States language      =English budget        = gross         =
}}
 thriller short film written and directed by Michael Whitton and starring James James, Matt Kawczynski, and David Whitton. The film follows the spree of a three-man costumed gang bent on scaring people for fun.

==Plot Summary==
The film is focused around a demented scare game that the desensitized players: Greg (James), Anthony (Kawczynski), and Tommy (Whitton) think is harmless and the random victims believe is real; but when an accident blurs the two, the players are forced to face a consequence that trumps all the rest.

==Cast==
(in order of appearance)
* James James as Greg (Blue), a thrill-seeker
* David Whitton as Tommy (Red), Gregs roommate
* Matt Kawczynski as Anthony (Green), Gregs friend by default
* Tamara Gschaider as Mia, young attack victim
* Kathy Corrigan as Ima, old attack victim
* Larry Whitton as Gentleman, Imas husband
* Jaime Whitton as Girl, a potential attack victim
* Rodney Amieva as Dude, Girls friend
* Ray Chavez, Jr. as Mello, a hoodie gangster
* Rene Arreola as Once, a hoodie gangster
* David Fernandez as Keso, a hoodie gangster

==Production== Super 16mm on location in Los Angeles, California.

==Accolades== Ivy Film Festival 
* Winner  BEST SHORT  at the 2010 FirstGlance Short Online Contest 
* Winner  PREMIERE  at the FirstGlance Film Fest Philadelphia TwentyTen (Shorts Too)  
*  SEMI-FINALIST  at the 2011 Action/Cut Short Film Competition 
* Shortlisted at 2010 Visionfest  and the 2011 Independents Film Festival. 

==See also==
*Aestheticisation of violence Adrenaline junkie
*Hooliganism

==References==
 

==External links==
*  
*  

 
 
 
 