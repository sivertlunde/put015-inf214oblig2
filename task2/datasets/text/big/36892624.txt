Things from Another World (film)
 
{{Infobox film
| name           = Things from Another World
| image          = Things from Another World (film).jpg
| caption        = Film poster
| director       = Francesco Patierno
| producer       = 
| writer         = Diego De Silva Giovanna Koch Francesco Patierno
| starring       = Diego Abatantuono
| music          = Simone Cristicchi 
| cinematography = Mauro Marchetti
| editing        = 
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}

Things from Another World ( ) is a 2011 Italian comedy film directed by Francesco Patierno,     loosely based on A Day Without a Mexican.

==Cast==
* Diego Abatantuono as Mariso Golfetto
* Laura Efrikian as Arieles mother
* Valentina Lodovini as Laura
* Valerio Mastandrea as Ariele Verderame
* Paola Rivetta as Herself
* Roberta Sparta as Truccatrice
* Vitaliano Trevisan as Tassista

==See also==
* Films about immigration to Italy

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 