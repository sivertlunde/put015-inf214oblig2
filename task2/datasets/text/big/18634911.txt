Adio Kerida (film)
 
{{Infobox Film
| name           = Adio Kerida: Goodbye my Dear Love
| image          =AdioKerida-1-.jpg
| image_size     =
| caption        = 
| director       = Ruth Behar
| producer       = 
| writer         = 
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| distributor    =
| released       = 2002 (USA) 
| runtime        = 82 min.
| country        = 
| language       = Spanish with English subtitles
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 documentary by American anthropologist Ruth Behar that follows her trip to Cuba, which her family left when she was four. She searches for memories from her past and investigates the dwindling Sephardic Jewish community that remains, estimated at less than 800 in 2011.

==Summary==
Ruth Behar was born in Havana, Cuba before the Cuban Revolution and was four years old when her family immigrated to the United States. She is a professor at the University of Michigan and travels to Havana to explore what remains of Jewish Cuba. As immigrants there in the early 20th century, her father and grandfather, Sephardi Jews from Turkey, once worked as peddlers in the city. {{cite news
  | last =
  | first =
  | title =Adio Kerida
  | publisher = The Jewish Channel
  | date = 2007–2014
  | url =http://www.tjctv.com/movies/adio-kerida/
  | accessdate = July 28, 2014}} 

 
Behar presents Danayda Levy as an example of the complex present of Cuban Jewry. Danaydas mother is a Jehovahs Witnesses|Jehovahs Witness, and her father Jose is president of the Sephardi Jewish center.  She is interviewed with her sister while they are in their parents apartment. Her sister now practices Santería, an Afro-Cuban creole religion developed by African slaves in the New World, and incorporating practices from historic Nigeria. Danayda is committed to Judaism and can read in Hebrew from the Torah with her fathers help. 

Ruth Behars mother was Ashkenazi, with relatives from Poland and Germany. Her fathers family was Sephardic in ancestry and from Turkey. They told of being expelled from Spain in 1492 and making their way around the Mediterranean to the Ottoman Empire. The family left Turkey after the First World War and social disruption following break-up of the empire. 

"It is said that when our ancestors left Spain, they took the keys with them," says Behar, "always believing in the possibility of return."  Given the Jewish abandonment of Havana, many elements of their history remain. Her parents’ former apartment has not been changed; she finds the furniture where she remembered it. The Sephardic cemetery has numerous gravestones with the Behar name. On a street named Inquisador (Inquisitor), Behar visits the remains of her fathers former temple. (At the time of the Revolution, there were five Jewish temples in Havana.) She asks, "Who am I in Cuba? A returning native, a reluctant anthropologist, or a tourist?" 

==Reception==
The film received awards at some regional film festivals and was shown widely around the United States in college venues. It received the following awards:  , Women Make Movies website, 2002, accessed 24 August 2014 
*East Lansing Film Festival - Documentary Award
*San Fran. Bay Area Latino F F - Jury Award
*Cine Festival - PREMIO MESQUITE Honorable Mention

Visual Anthropology Review:
"Personal, poetic, and reflective...offers a glimpse into a relatively unknown realm of the Cuban reality. Recommended." 

Library Journal:
"Offers an easy-to-view introduction to a fascinating culture. Libraries with strong Jewish studies collections should definitely have this one." 

==See also==
*Culture of Cuba
*History of the Jews in Latin America
*History of the Jews in Cuba

Other films about Cuban Jewry:
* 
* 
*Trip to Jewish Cuba

==References==
 

==External links==
* 
*  

 
 
 
 
 
 
 