Primrose Path (film)
 
{{Infobox film
| name           = Primrose Path
| image          = Primrose Path Poster.jpg
| image_size     =
| caption        =
| director       = Gregory La Cava
| producer       = Gregory La Cava Allan Scott
| starring       = Ginger Rogers Joel McCrea
| music          = Werner R. Heymann
| cinematography = Joseph H. August
| editing        = William Hamilton RKO Radio Pictures
| released       =  
| runtime        = 93 minutes
| country        = United States English
| budget         = $702,000 Richard Jewel, RKO Film Grosses: 1931-1951, Historical Journal of Film Radio and Television, Vol 14 No 1, 1994 p56 
| gross          = $1,200,000 
}}
 1940 film about a young woman determined not to follow the profession of her mother and grandmother, prostitution. It stars Ginger Rogers and Joel McCrea. The film was based on the play of the same name by Robert L. Buckner and Walter Hart and the novel February Hill by Victoria Lincoln (uncredited for legal reasons ).

Marjorie Rambeau was nominated for the Academy Award for Best Supporting Actress.

==Plot==
Tomboy Ellie May Adams (Ginger Rogers) keeps her virtue despite her difficult circumstances. Her alcoholic, Greek scholar father Homer (Miles Mander) is unemployable, leaving her loving mother Mamie (Marjorie Rambeau) to support the family by going out with men. Her ex-prostitute grandmother (Queenie Vassar) sees nothing wrong with their shared profession.

One day, Ellie May warily accepts a ride to the beach from Gramp (Henry Travers). Gramp runs a beachside restaurant and gas station along with wisecracking Ed Wallace (Joel McCrea). Ellie May falls in love with Ed and eventually, after lying to him about being thrown out by her family over him, gets him to marry her. She becomes an industrious, well-liked waitress in the restaurant.

However, she makes a grave mistake when she finally agrees to take Ed to meet the rest of her family. When her lies about her relations are revealed, Ed leaves her. To add to her woes, her father accidentally shoots her mother during one of his drunken, half-hearted attempts at suicide. Before she dies, Mamie gets Ellie May to promise to take care of the family.
 Charles Lane) on a car trip to San Francisco. On the way, Ellie May gets them to stop at Eds favorite nightclub, where she bitterly pretends to be what her husband thinks she is. However, after a private talk with a sympathetic Mr. Smith, Ed figures out the truth and takes Ellie May back. He also accepts the burden of her family.

==Cast==
* Ginger Rogers as Ellie May Adams
* Joel McCrea as Ed Wallace
* Marjorie Rambeau as Mamie Adams
* Henry Travers as Gramp
* Miles Mander as Homer Adams
* Queenie Vassar as Grandma
* Joan Carroll as Honeybell Adams
* Vivienne Osborne as Thelma
* Carmen Morales as Carmelita, Eds former girlfriend

==Reception==
The film made a profit of $110,000. 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 


 