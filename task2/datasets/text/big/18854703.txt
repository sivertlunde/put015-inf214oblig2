Guyana: Crime of the Century
{{Infobox film
| name           = Guyana: Crime of the Century
| image          = Guyana-Crime of the Century-poster.jpg
| image_size     =
| caption        = The current DVD cover features the two alternate English titles of the film
| director       = René Cardona Jr.
| producer       = 
| writer         = René Cardona Jr.
| narrator       =  John Ireland, Joseph Cotten, Bradford Dillman, Mel Ferrer
| music          = George S. Price, Nelson Riddle and Bob Summers	 	
| cinematography = Leopoldo Villaseñor Earl Watson
| distributor    = Universal Pictures
| released       =  : January 25, 1980
| runtime        = 115 min. (Mexico), 90 min. (USA), 108 min. (UK)
| country        = Mexico (co-produced with Spain and Panama)
| language       = English
| budget         = 
| gross          = $3,798,102
| preceded_by    = 
| followed_by    = 
}}

Guyana: Crime of the Century (also known as Guyana: Cult of the Damned) is a 1979  , the cult is led by "Reverend James Johnson" (Whitman) rather than Jim Jones|Rev. Jim Warren Jones, and the murdered Congressman is "Lee OBrien" (Barry) rather than Leo Ryan.

==Plot==
In early 1977, Reverend James Johnson, the fanatic and paranoid leader of an independent church in San Francisco, moves his 1,000-strong congregation to the jungles of South American in the country of Guyana to create their own utopia free of the so-called corruption of the civilized world. Life at the jungle commune, called "Johnsontown", becomes unbearable as there are featured acts of brutally and cruelty that Johnson inflicts on his followers. In November 1978, when California Congressman Lee OBrien investigates several reports of commune members being held against their will, he ventures to Johnsontown with a team of reporters to investigate the allegations of abuse. Despite the positive facade that Reverend Johnson puts out to Congressman OBrien, the reality of the camp becomes apparent. When OBrien leaves Johnsontown with a group of defectors, Johnson orders his loyal hit squads to kill OBrien and the reporters, and then orders his followers to commit ritual mass suicide.

==Cast==
* Stuart Whitman as Reverend James Johnson
* Gene Barry as Congressman Lee OBrien John Ireland as Dave Cole
* Joseph Cotten as Richard Gable
* Bradford Dillman as Dr. Gary Shaw
* Jennifer Ashley as Anna Kazan
* Yvonne De Carlo as Susan Ames
* Nadiuska as Leslie Stevens Tony Young as Ron Harvey
* Erika Carlsson as Marilyn Johnson
* Robert DoQui as Oliver Ross
* Hugo Stiglitz as Cliff Robson
* Carlos East as Mike Sheldon

==Production==

 

==Reception==

 

==Historical inaccuracies==

Most notably, the "Johnsontown" membership is largely cast with white actors, while in reality, and at its height, the majority of Peoples Temple members were African-American.

Moreover, the film depicts "Susan Ames" (the fictionalized version of Peoples Temple loyalist Sharon Amos, as played by Yvonne DeCarlo) being murdered, along with her children by a shadowy, knife-wielding man. In reality, Sharon Amos—a hardcore supporter of Jim Jones stationed in nearby Georgetown—followed the orders of Jones for his followers to die on November 18, 1978. Amos reportedly took a kitchen butcher knife and slit the throats of her two youngest children (Christa, age 11 and Martin, age 10), then asked her eldest daughter Liane (age 21) to kill her with the knife, thus leaving Liane to kill herself.

A minor inaccuracy concerns the "Johnson" death scene; as Johnson (Stuart Whitman) appears to expire from a gunshot, as he collapses, he pulls his shirt open by the lower four buttons. This is meant to explain why in actual Jonestown death scene photos, Jim Jones is seen laying on the pavilion floor with his red shirt open. It appears the real Jones shirt was in that condition due to the effects of the body swelling after death, which is quite evident in all photos of the deceased Jones.

==Alternate versions==

 

==References==
 
 

==External links==
*  
*  

 

 
 
 
 
 
 
 