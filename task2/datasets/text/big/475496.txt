The Life of Oharu
{{Infobox film
| name           = The Life of Oharu
| image          =
| caption        = original Japanese movie poster made by Shintoho
| director       = Kenji Mizoguchi
| producer       = Hideo Koi Kenji Mizoguchi Isamu Yoshiji (executive producer) Shintoho Koi Productions
| writer         = Saikaku Ihara (novel) Kenji Mizoguchi Yoshikata Yoda
| starring       = Kinuyo Tanaka Tsukie Matsuura Ichirō Sugai Toshiro Mifune Takashi Shimura
| music          = Ichirō Saitō
| cinematography = Yoshimi Hirano Yoshimi Kono
| editing        = Toshio Gotō
| distributor    = Shintoho
| released       = April 17, 1952 (Japan) 
| runtime        = 148 min.
| country        = Japan
| awards         =
| language       = Japanese
| budget         =
}}

The Life of Oharu 西鶴一代女, Saikaku Ichidai Onna, "Saikakus Amorous Woman" is a 1952 historical fiction black-and-white film directed by Kenji Mizoguchi starring Kinuyo Tanaka as Oharu, a one-time concubine of a daimyo (and mother of a later daimyo) who struggles to escape the stigma of having been forced into prostitution by her father.  It is based on a novel by Ihara Saikaku.
 class and hierarchy in Japanese society in the Edo period. 

  in Edo period.]]

==Background==
The Life of Oharu is based on various stories from Ihara Saikakus The Life of an Amorous Woman. It was produced by the Shintoho Company, with cinematography by Yoshimi Hirano and screenplay by Yoshikata Yoda.

It has been claimed that this movie was one of Kenji Mizoguchis favorite projects, even though it was under-financed. Other crew members included production designer Hiroshi Mizutani and historical consultant Isamu Yoshi.

==Awards==
The Life of Oharu won the International Prize at the 1952 Venice International Film Festival and was nominated to Golden Lion. The film (include 1952 films Himitsu, Lightning (film)|Inazuma and Okaasan) won 1953 Mainichi Film Concours for best film score (Ichirō Saitō). 

==Plot==
  as page Katsunosuke, who courted Oharu]]
The story opens on Oharu as an old woman in a temple flashing back through the events of her life. It begins with her love affair with a page, Katsunosuke (Toshirō Mifune), the result of which (due to their class difference) is his execution and her familys banishment. Oharu attempts suicide but fails and is sold to be the mistress of Lord Matsudaira with the hope she will bear him a son. She does, but then is sent home with minimal compensation to the dismay of her father, who has worked up quite a debt in the meantime. He sends her to be a courtesan, but there, too, she fails and is again sent home. She goes to serve the family of a woman who must hide the fact that she is bald from her husband. The woman becomes jealous of Oharu and makes her chop off her hair, but Oharu retaliates, revealing the womans secret. She again must leave—this time she marries a fan maker who is killed shortly after during a robbery. She attempts to become a nun, but Oharu is thrown out after being caught naked with a man seeking reimbursement for an unauthorized gift (it is unclear if this is seduction or rape). She is thrown out of the temple, becomes a prostitute, but fails even at that. In the end, she is recalled to the Lords house in order to keep secret her activities and to be exiled within the compounds to keep her secrets locked away. While being scolded for the life she chose, she attempts to find her son, and in the process, ends up running away as she chooses the life of a beggar over the life in exile.

== Gender issues ==
 
  as Oharu, the mistress of Lord Matsudaira]]
  role as 17th Century Japanese society is greatly strained as she was originally cast aside from her role as a member of court for choosing her own lover. It is Oharus original and later refusals to obey such patriarchal systems of gendered labour through making her own decisions that precipitate her descent through the class system and into depression. 

== Director ==
 
The director of The Life of Oharu,  , The Story of the Last Chrysanthemums, The Life of Oharu, Sansho the Bailiff, Ugetsu, and Street of Shame. Films such as these helped establish Mizoguchis reputation as a feminist director.

== Cast and main characters ==
 ).]]
  as Lady Matsudaira.]]
* Kinuyo Tanaka as Oharu – The protagonist whose bad luck and misfortune lead to various struggles in life.
* Tsukie Matsuura as Tomo, Oharus Mother – A kind character in the film; her mother tended to side with Oharu and did not wish to see her become a courtesan.
* Ichirō Sugai as Shinzaemon, Oharus Father – Oharus father was consumed by both money and social status. His misjudgments about Oharu caused much of her downfall.
* Toshiro Mifune as Katsunosuke – A page who courted Oharu and they fell into a forbidden love. He is beheaded once their relationship is discovered.
* Toshiaki Konoe as Lord Harutaka Matsudaira – He takes Oharu as a mistress in order to bear a child heir. Unfortunately for Oharu, he falls in love with Oharu and his wifes jealousy causes her dismissal.
* Hisako Yamane as Lady Matsudaira – The wife of Harutaka Matsudaira who, because of her jealousy of her husbands love for Oharu, banishes her.
* Jūkichi Uno as Yakichi Ogiya – He was a respected fan maker who married Oharu, however, he is tragically murdered shortly into their marriage.
* Kiyoko Tsuji as Landlady.
* Eitarō Shindō as Kahe Sasaya.
* Akira Oizumi as Fumikichi, Sasayas friend.
* Kyoko Kusajima as Sodegaki.
* Masao Shimizu as Kikuoji
* Daisuke Katō as Tasaburo Hishiya
* Toranosuke Ogawa as Yoshioka
* Hiroshi Oizumi as manager Bunkichi
* Haruyo Ichikawa as Lady-in-waiting Iwabashi
* Yuriko Hamada as Otsubone Yoshioka
* Noriko Sengoku as Lady-in-waiting Sakurai
* Sadako Sawamura as Owasa
* Masao Mishima as Taisaburo Hishiya
* Eijirō Yanagi as forger
* Chieko Higashiyama as Myokai, the old nun
* Takashi Shimura as old man
* Benkei Shiganoya as Jihei
* Komako Hara as Otsubone Kuzui

==See also==
* List of films in the public domain

== References ==
 

==External links==
 
* 
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 