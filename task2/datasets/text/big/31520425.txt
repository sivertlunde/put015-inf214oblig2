Darling Companion
{{Infobox film
| name           = Darling Companion
| image          = Darling companion poster.jpg
| caption        =
| director       = Lawrence Kasdan
| producer       = Elizabeth Redleaf
| writer         = Lawrence Kasdan Meg Kasdan
| starring       = Mark Duplass Richard Jenkins Diane Keaton Kevin Kline Elisabeth Moss Sam Shepard Dianne Wiest Ayelet Zurer
| music          = James Newton Howard
| cinematography = Michael McDonough
| editing        = Carol Littleton
| studio         = Werc Werk Works Kasdan Pictures Likely Story
| distributor    = Sony Pictures Classics (USA) Sierra/Affinity (non-USA) 
| released       =  
| runtime        = 103 minutes
| country        = United States
| language       = English
| budget         = $12 million
| gross          = $793,815
}}
Darling Companion is a 2012 drama film directed by Lawrence Kasdan, written by Kasdan and his wife Meg, and starring Diane Keaton and Kevin Kline. Filming took place in Utah in 2010 and was released on April 20, 2012.

==Plot==
Beth Winter (Diane Keaton|Keaton) rescues a lost dog from the roadside and names him Freeway. Her children have grown up and moved away, and her husband, Joseph (Kevin Kline|Kline), is distracted and self-involved. Beth forms a strong friendship with the dog and is deeply upset when, after her daughters wedding, her husband loses the dog. They engage the service of a psychic gypsy to find the dog again.

In the end after finally giving up, the family boards an airplane. While flying over the mountains, Beth sees the dog and her husband fakes a ruptured appendix to have the pilot turn the airplane around. In one last attempt at a search, they scour the trees in the area Beth saw the dog, when at last Freeway appears in a field and runs to Beth, reunited at last, bringing the family closer together.

==Cast==
* Mark Duplass as Bryan Alexander 
* Richard Jenkins  as Russell  
* Diane Keaton as Beth Winter  
* Kevin Kline  as Dr. Joseph Winter  
* Elisabeth Moss  as Grace Winter 
* Sam Shepard as Sheriff Morris  
* Dianne Wiest  as Penny Alexander   
* Ayelet Zurer as Carmen

==Reception==
Darling Companion received mostly negative reviews from critics and was a box-office flop. Rotten Tomatoes gives it a score of 22% based on 81 reviews. 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 


 