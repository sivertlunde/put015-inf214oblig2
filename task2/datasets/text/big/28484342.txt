Jacktown (film)
{{Infobox film
| name           = Jacktown
| image          =
| image_size     =
| caption        = William Martin William Martin Robert Stanley (associate producer) William Martin (writer)
| narrator       =
| starring       = See below
| music          = Aldo Provenzano
| cinematography = Arthur J. Ornitz
| editing        = Ralph Rosenblum
| distributor    =
| released       =
| runtime        = 62 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
 William Martin.

== Plot summary ==

In Jacktown, Michigan, a young delinquent named Frankie gets arrested for statutory rape efter being caught in flagrante with a fifteen-year-old girl in the back seat of his car. 

He claims his innocence since he didnt know she was under age and had consented to the relation. Despite this, Frankie is convicted and sentenced to prison for 2 1/2 to 5 years. The sentence is to be served in Southern Michigan Prison. 

Because of the crime he is convicted for, he is unpopular among the other prisoners. To release the tension his presence builds up in the correctional facility, Frankie is put on gardening duty in the warden living quarters. 

While gardening, Frankie meets the wardens daughter, Margaret, and they  fall in love with each other. When the warden learns about Frankies relation woth his daughter he moves him to another position, as chauffeur outside the prison walls. 

There is a prison riot inside while Frankie is outside driving a prisoner. The guard who is along on the ride is overpowered by the other prisoner and Frankie gets a chance to escape. 

As a fugitive, Frankie goes to Margaret, but she convinces him to turn himself in, promising to wait for him until he is properly released and a free man. 

== Cast ==
*Patty McCormack as Margaret
*Richard Meade as Frances "Frankie" Stossel George Taylor as Himself
*Douglas Rutherford as Warden
*Russ Paquette as Vince "Vinnie"
*Mike Tancredi as Mike Joanna Douglas as Mrs. Stossel Gordon Grant as Lefty John Anthony as Dutch
*Joseph Julian as Narrator

== Soundtrack ==
 

== External links ==
* 
* 

==References==
 

 
 
 
 
 
 
 
 


 