Undertow (2009 film)
 
{{Infobox film
| name           = Undertow
| image          = Contracorriente.jpg
| caption        = Film poster
| director       = Javier Fuentes-León
| producer       = Javier Fuentes-León
| writer         = Javier Fuentes-León Julio Rojas
| starring       = Cristian Mercado Manolo Cardona Tatiana Astengo
| music          = Selma Mutal Vermeulen
| cinematography = Mauricio Vidal
| editing        = Roberto Benavides
| distributor    = 
| released       =  
| runtime        = 100 minutes
| country        = Peru Colombia
| language       = Spanish
| budget         = 
}}
 Cabo Blanco, Peru, won the World Cinema Audience Award in the Dramatic category at the 2010 Sundance Film Festival in the U.S.A.    Other Audience Awards included Cartagena, Montreal, Miami, Chicago, Utrecht, Lima, and Galway, as well as Jury Awards in Madrid, San Francisco, Seattle, Toulouse, and Philadelphia. It received a nomination as Best Latin American Film at the 2011 Goya Awards in Spain. 
 Best Foreign Language Film at the 83rd Academy Awards    but it did not make the final shortlist.   

==Cast==
* Cristian Mercado as Miguel
* Manolo Cardona as Santiago
* Tatiana Astengo as Mariela

==Plot==
Miguel (Cristian Mercado) is a young fisherman of Cabo Blanco, a small village in northern Peru with specific traditions regarding death. He is married to Mariela (Tatiana Astengo), who is pregnant with their first son, but he also has a secret affair with a male painter called Santiago (Manolo Cardona).

Santiago accidentally drowns at sea, and his ghost returns to ask Miguel to find his body, in order to bury it with their villages rituals. Miguel eventually finds Santiagos body in the water, but does not tell his ghost of the discovery. Meanwhile, the villagers discover nude paintings of Miguel at Santiagos house, fueling a rumour that he was having an affair. Mariela hears the rumours, confronts Miguel about them, and upon hearing him confess, she goes to her mothers house with their newborn child.

Miguel returns to look for the body of Santiago, but he finds that the current has taken it away. Mariela eventually returns home, but then the body of Santiago appears in the nets of a fishing boat. Miguel decides to claim the body of Santiago for a burial at sea. He takes the coffin of Santiago to sea, but a second after the coffin glides into the sea and Miguel is in bitter tears, the ghost of Santiago reappears for a last time, caressing Miguel, who returns home alone in the sundown.

==Production==
Undertow had been gestating since 1996, when the director Javier Fuentes-León wrote the very first scene. Originally conceived of as a supernatural revenge thriller about a fisherman having an affair with a prostitute, Javier decided to change it after coming out of the closet himself, in an attempt to explore more interesting themes and material.   

==Critical reception==
The film received positive reviews from critics. Review aggregator Rotten Tomatoes reports that 88% out of 32 critics gave the film a positive review, with a rating average of 7.3/10.  Manhola Dargis from the New York Times praised the directors ability to show powerful relationships between the characters, particularly focusing on the complexity of Miguel’s feelings for Mariela, which she called "gratifying".  David Wiegang from the San Francisco Chronicle also gave the film a positive review, saying that the "films accomplishments are many, but not the least is its ability to take a human story and frame it as a parable, without losing a bit of credibility or irresistible heart." 

Pam Grady from Boxoffice Magazine wrote “Sensual and romantic with a heavy dose of the supernatural and populated by indelible characters.”    Bob Mondello from NPR wrote “Undertow, for all its narrative tricks, has been given the rhythm and texture of real life, as well as emotional undercurrents that are haunting.”  

==Awards and nominations==
;For Undertow   
*2009: Won Sebastian Award at the San Sebastián International Film Festival
*2010: Won Audience Awards for "World Cinema - Dramatic" category at the Sundance Film Festival
*2010: Nominated for Grand Jury Prize for "World Cinema - Dramatic" category at the Sundance Film Festival
*2010: Won Audience Award at the Lima Latin American Film Festival Miami Film Festival
*2010: Nominated for Golden India Catalina for "Best Film (Mejor Película)" at Cartagena Film Festival
*2011: Nominated GLAAD Media Award for Outstanding Film – Limited Release
*2011: Nominated for Goya for "Best Spanish Language Foreign Film" at Goya Awards

==See also==
* List of submissions to the 83rd Academy Awards for Best Foreign Language Film
* List of Peruvian submissions for the Academy Award for Best Foreign Language Film
* List of lesbian, gay, bisexual, or transgender-related films by storyline

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 