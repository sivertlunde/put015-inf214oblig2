36 Hours
 

{{Infobox film|
| name           = 36 Hours
| image          = 36_hours_movieposter.jpg
| caption        = Movie poster
| producer       = William Perlberg
| director       = George Seaton
| screenplay     = George Seaton
| story          =  
| based on       =  
| starring       = James Garner Rod Taylor Eva Marie Saint
| music          = Dimitri Tiomkin
| cinematography = Philip H. Lathrop
| editing        = Adrienne Fazan
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| country        = United States
| gross          = $2,200,000 (US/ Canada rentals) 
| runtime        = 115 min.
| language       = English
}} Beware of Rod Taylor and was directed by George Seaton.  On 2 June 1944, a German army doctor tries to obtain vital information from an American military intelligence officer by convincing him that it is 1950 and World War II is long over.

== Plot == General Eisenhowers final briefing on the Normandy landings (D-Day), U.S. Army Major Jeff Pike (James Garner) is sent to Lisbon to confirm with an informant that the Nazis still expect the invasion in the wrong place (Pas de Calais). However, Pike falls into a trap; he is drugged into unconsciousness and transported to Germany.
 needs glasses Occupied Germany, Rod Taylor) explains that he has been having episodes of memory loss for the past few years, ever since he sustained physical trauma in Portugal in June 1944. He advises Pike not to worry, as his blocked memories have always resurfaced within a few weeks, helped along by a treatment that mostly consists of remembering events prior to Lisbon and then pushing on into the blank period. Gerber is assisted by a nurse, the dispassionate Anna Hedler (Eva Marie Saint).  To support the illusion that he has been a hospital patient for some time, Pike is provided with letters supposedly written by his father, and photos of German doubles who resemble his parents&mdash;Gerber has been researching Pike for many months to prepare for this event&mdash;and Hedler tells Pike she is his wife. He is also shown fake newspapers dated 1950, and listens to a faked American forces radio broadcast. The grounds are full of (captured) U.S. Army jeeps and seemingly American staff and patients, some happily playing baseball.
 German High Command), the units involved, and the date, June 5, to his eager listeners.
 Nazi cause. Russian Front; but they had been perverted to this purpose. Pikes hair had been dyed, of course; and an injection of atropine had impaired his close vision. However, when Pike claims he knew the truth all along and his statements about Normandy were a cover story, Gerber is skeptical.
 concentration camp SS Officer Schack (Werner Peters) that he knew all along it was a ruse. Schack now believes the invasion will be at Calais. Gerber, though, does not, so he plays one last trick, setting the clock in Pikes room ahead a whole day. When Pike thinks the invasion has already begun, he lets his guard down and confirms Gerbers suspicions about the Normandy invasion. Gerber then sends an emergency dispatch to Wehrmacht authorities, which Schack intercepts and disregards, even suggesting Gerber may be a double agent.  As it happens, the weather is too rough; and Eisenhower postpones the invasion a day, discrediting Gerber, and Schack orders Gerbers arrest.

Gerber knows that Schack will return to kill them when the Normandy information proves correct, so that his blunder is not revealed.  The doctor secretly lets Anna and Pike go, asking Pike to take his psychological research papers on true amnesiacs with him to the West.  When he hears the news of the Normandy landing, he takes poison. When Schack shows up, Gerber tries to shoot him but dies too soon. Schack pursues the escaped couple alone, ordering his men to follow when they are assembled.

During their escape, Anna tells Pike of the abuse in the camp, which has left her emotionless.  She and Pike go to the local minister, where they are referred to a frankly corrupt, middle-aged German border guard, Sgt. Ernst (John Banner), who is willing to help them cross into Switzerland in return for Pikes watch and Hedlers gold ring. Ernst gives the ministers housekeeper, Elsa (Celia Lovsky), the ring. After the couple and Ernst head for the border, Schack shows up at the manse. When he sees Hedlers gold ring on Elsa’s finger, he forces her to tell him where to find the escapees. Schack catches up with Pike and Hedler at the border, but Ernst shoots him because he doesnt want Schack to mess up his human-smuggling business. Ernst and Pike arrange Schack’s body to make it look as if he had been killed while trying to escape.

Safely in Switzerland, Pike and Hedler are put in separate cars. Pike is told he will be taken to the Diplomatic mission#Extraterritoriality|U.S. Embassy, while Hedlers fate is uncertain. Hedler cries, her first display of emotion in years.  In the final scene, the cars come to a fork in the road, with one turning left, to the Embassy, and the other turning right, to a refugee camp.

==Cast==
*James Garner as Major Jefferson F. Pike
*Eva Marie Saint as Anna Hedler
*Rod Taylor as Major Walter Gerber
*Werner Peters as Otto Schack
*John Banner as Sgt. Ernst
*Russell Thorson as General Allison
*Alan Napier as Colonel Peter MacLean
*Oscar Beregi, Jr. as Lt. Colonel Karl Ostermann (as Oscar Beregi)
*Ed Gilbert as Captain Abbott
*Celia Lovsky as Elsa
*Carl Held as Corporal Kenter (as Karl Held)
*Martin Kosleck as Kraatz

==Production==
Most of the film was shot in Yosemite National Park.  Exterior shots were filmed at the Wawona Hotel near the entrance of Yosemite National Park.

==Background==
*D-Day was actually delayed a day because of the inclement weather, which was also a major plot point of the film Garner had made just before this one, The Americanization of Emily (1964).
*Banners part, which provided the comedy relief in this movie, was the model for his role as another easy-going German soldier, POW camp guard Sgt. Schultz, in the TV series Hogans Heroes (1965–71). Sig Ruman played opposite Banner in a late scene (Sig Ruman was Schultz in the movie Stalag 17).
*The film was remade as a 1989 TV movie Breaking Point (1989 film)|Breaking Point starring Corbin Bernsen.  newswell%7ctext%7cIndyStar.com%7cp |accessdate=8 November 2010|newspaper=Indianapolis Star|date=8 November 2010}} 

==References==
 

==External links==
*  
*  
*  
* 
* 
*  at Archive of American Television

 
 

 
 
 
 
 
 
 
 
 
 
 
 