The Stronger Love
{{Infobox film
| name           = The Stronger Love
| image          = The Stronger Love 1916 - publicity photo.jpg
| alt            = A young woman standing next to a tall strong country-side man.
| caption        = Publicity photo.
| director       = Frank Lloyd
| producer       = 
| screenplay     = Julia Crawford Ivers
| story          = Alice von Saxmar 
| starring       = Vivian Martin Edward Peil, Sr. Frank Lloyd Jack Livingston Alice Knowland Herbert Standing
| music          = 
| cinematography = James Van Trees
| editing        = 
| studio         = Oliver Morosco Photoplay Company
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 drama silent film directed by Frank Lloyd and written by Julia Crawford Ivers. The film stars Vivian Martin, Edward Peil, Sr., Frank Lloyd, Jack Livingston, Alice Knowland and Herbert Standing. The film was released on August 13, 1916, by Paramount Pictures.  

==Plot==
Vivian Martin played Nell Serviss, a beautiful young girl from the mountains that was engaged to Jim Serviss, who is the leader of their clan. She meets a stranger at the Rutherford farm and falls in love. The stranger is in their mountains searching for radium in rocks. Someone lights the Serviss farm on fire and the stranger is falsely accused. Everyone is mad at the stranger (who at this point is revealed to be a Rutherford) but Nell says she wants to marry him so that no one will kill him. In the end, Nell chases after Jim to tell him that she lied and really wants to be with him. 

== Cast ==
*Vivian Martin as Nell Serviss
*Edward Peil, Sr. as Jim Serviss
*Frank Lloyd as Tom Serviss
*Jack Livingston as Rolf Rutherford
*Alice Knowland as Mrs. Jane Rutherford
*Herbert Standing as Oriel Kincaid
*John McKinnon as Peter Kincaid
*Louise Emmons as Widow Serviss 
*William Jefferson

== References ==
 

== External links ==
 
*  

 
 
 
 
 
 
 
 
 

 