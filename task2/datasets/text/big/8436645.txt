The Fuller Brush Girl
{{Infobox film
| name           = The Fuller Brush Girl
| image          = Poster of the movie The Fuller Brush Girl.jpg
| image_size     = 190px
| caption        =
| director       = Lloyd Bacon
| producer       = S. Sylvan Simon
| writer         = Frank Tashlin
| narrator       = 
| starring       = Lucille Ball Heinz Roemheld
| cinematography = Charles Lawton Jr. William Lyon
| distributor    = Columbia Pictures
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         =
}}
The Fuller Brush Girl is a 1950 slapstick comedy starring Lucille Ball and directed by Lloyd Bacon. Animator Frank Tashlin wrote the script. Ball plays a quirky door-to-door cosmetics salesperson for the Fuller Brush Company. The film also stars Eddie Albert and has an uncredited cameo by Red Skelton (who had starred in the Tashlin-scripted The Fuller Brush Man two years earlier).

==Plot synopsis==
Sally and Humphrey have just put a down payment on a house, when Sally loses her receptionist job after accidentally destroying the switchboard.  She applies for a Fuller Brush franchise, but needs a reference from her former employer, Harvey Simpson.  Meanwhile, Harvey is in trouble with his wife because hes come home with a suit coat smelling of Fuller Brush powder.  Mrs. Simpson thinks her husband is having an affair, so Harvey calls Humphrey to have Sally go to Harveys house and explain everything to his wife.  With her reference letter depending on it, Sally goes to the house to find a bogus Mrs. Simpson, a dead body, and missing diamonds. Afraid the police will suspect her of foul play, Sally and Humphrey identify the real culprit and pursue her to her job dancing at a burlesque theater, and then onto a departing ocean liner.  Hilarity ensues as the pair are chased around the ship by a criminal gang trying to silence them, while they hide variously in rooms filled with leaky wine barrels, bunches of bananas, and a talking parrot who nearly gives them away.

==Cast==
*Lucille Ball ...  Sally Elliot
*Eddie Albert ...  Humphrey Briggs
*Carl Benton Reid ...  Mr. Christy
*Gale Robbins ...  Ruby Rawlings
*Jeff Donnell ...  Jane Bixby
*Jerome Cowan ...  Harvey Simpson
*John Litel ...  Mr. Watkins Fred Graham ...  Rocky Mitchell Lee Patrick ...  Claire Simpson
*Arthur Space ...  Insp. Rodgers

==External links==
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 

 