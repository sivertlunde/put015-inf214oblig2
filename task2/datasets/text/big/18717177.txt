Shrek
 
 
 
 
{{Infobox film
| name        = Shrek
| image       = shrek.jpg
| imagesize   = 250px
| caption     = Theatrical release poster
| border      = yes
| director    =  
| producer    =  
| screenplay  =  
| based on    =  
| narrator    =  
| starring    =     John Powell
| editing     = Sim Evan-Jones
| studio      = Pacific Data Images
| distributor = DreamWorks Pictures  
| released    =  
| runtime     = 90 minutes
| country     = United States
| language    = English
| budget      = $60 million
| gross       = $484.4 million   
}}

Shrek is a 2001 American   includes music by Smash Mouth, Eels (band)|Eels, Joan Jett, The Proclaimers, Jason Wade, Baha Men, and John Cale (covering Leonard Cohen).

The rights to the books were originally bought by Steven Spielberg in 1991, before the founding of DreamWorks, when he thought about making a traditionally animated film based on the book. However, John H. Williams convinced him to bring the film to DreamWorks in 1994, the time the studio was founded, and the film was put quickly into active development by Jeffrey Katzenberg after the rights were bought by the studio in 1995. Shrek originally cast Chris Farley to do the voice for the title character, recording about 80%–90% of his dialog. After Farley died in 1997 before he could finish, Mike Myers was brought in to work for the character, who after his first recording decided to record his voice in a Scottish accent. The film was also originally planned to be motion-captured, but after poor results, the studio decided to get PDI to help Shrek get its final computer-animated look.

Earning $484.4 million at the worldwide box office, the film was a critical and commercial success. Shrek also received promotion from food chains such as Baskin-Robbins (promoting the films DVD release) and Burger King. It was acclaimed as an animated film worthy of adult interest, with many adult-oriented jokes and themes but a simple enough plot and humour to appeal to children. Shrek won the first ever Academy Award for Best Animated Feature and was nominated for the Academy Award for Best Adapted Screenplay. The film was also nominated for six British Academy of Film and Television Arts awards, including the BAFTA Award for Best Actor in a Supporting Role for Eddie Murphy for his voice-over performance as Donkey (Shrek)|Donkey, and won the BAFTA Award for Best Adapted Screenplay. The films main (and title) character was awarded his own star on the Hollywood Walk of Fame in May 2010. 
 Puss in stage musical and even a comic book by Dark Horse Comics.

==Plot==
  Donkey who is the only fairytale creature who knows the way to Duloc.

Farquaad tortures the  , who is locked in a castle tower guarded by lava and a dragon. The Mirror tries to mention "the little thing that happens at night" but is unsuccessful.

Shrek and Donkey arrive at Farquaads palace in Duloc, where they end up in a tournament. The winner gets the "privilege" of rescuing Fiona so that Farquaad may marry her. Shrek and Donkey easily defeat the other knights in wrestling-match fashion, and Farquaad accepts his offer to move the fairytale creatures from his swamp if Shrek rescues Fiona.
 dragon and sweet-talks the beast before learning that it is female. Dragon takes a liking to him and carries him to her chambers. Shrek finds Fiona, who is appalled at his lack of romanticism. As they leave, Shrek saves Donkey, caught in Dragons tender clutches, and forces her to chase them out of the castle. At first, Fiona is thrilled to be rescued but is quickly disappointed when Shrek reveals he is an ogre.

As the three journey to Duloc, Fiona urges the two to camp out for the night while she sleeps in a cave. Shrek and Donkey stargaze while Shrek tells stories about great ogres and says that he will build a wall around his swamp when he returns. When Donkey persistently asks why, he says that everyone judges him before knowing him; therefore, he feels he is better off alone, despite Donkeys admission that he did not immediately judge him when they met.

Along the way, Shrek and Fiona find they have more in common and fall in love. The trio is almost at Duloc, and that night Fiona shelters in a windmill. When Donkey hears strange noises coming from it, he finds Fiona turned into an ogre. She explains her childhood curse and transforms each night, which is why she was locked away, and that only her true loves kiss will return her to her "loves true form". Shrek, about to confess his feelings for Fiona with a sunflower, partly overhears them, and is heartbroken as he mistakes her disgust with her transformation to an "ugly beast" as disgust with him. Fiona makes Donkey promise not to tell Shrek, vowing to do it herself, but when morning comes, Shrek has brought Lord Farquaad to Fiona. The couple return to Duloc, while a hurt Shrek angrily leaves his friendship with Donkey and returns to his now-vacated swamp, remembering what Fiona "said" about him.

Despite his privacy, Shrek is devastated and misses Fiona. Furious at Shrek, Donkey comes to the swamp where Shrek says he overheard Donkey and Fionas conversation. Donkey keeps his promise to Fiona and tells Shrek that she was talking about someone else. He accepts Shreks apology and tells him that Fiona will be getting married soon, urging Shrek into action to gain Fionas love. They travel to Duloc quickly, thanks to Dragon, who had escaped her confines and followed Donkey.

Shrek interrupts the wedding before Farquaad can kiss Fiona. He tells her that Farquaad is not her true love and only marrying her to become king. The sun sets, which turns Fiona into an ogre in front of everyone in the church, causing Shrek to fully understand what he overheard. Outraged by Fiona, Farquaad orders Shrek killed and Fiona detained. Shrek whistles for Dragon who bursts in along with Donkey and devours Farquaad. Shrek and Fiona profess their love and share a kiss; Fiona is bathed in light as her curse is broken but is surprised that she is still an ogre, as she thought she would become beautiful, to which Shrek replies that she is beautiful. They marry in the swamp and leave on their honeymoon while the rest celebrate by singing "Im a Believer".

==Cast==
  Shrek  Donkey 
* Cameron Diaz as Princess Fiona 
* John Lithgow as Lord Farquaad 
* Vincent Cassel as List of characters in the Shrek series#Monsieur Robin Hood|"Monsieur" Robin Hood  Gingerbread Man  Chris Miller Magic Mirror    Pinocchio / The Three Little Pigs  Three Blind Mice  Three Blind Mice and Thelonius Big Bad Wolf 
* Jim Cummings as Captain of the Guards 
* Kathleen Freeman as Old Woman (Donkey (Shrek)|Donkeys ex-owner) 
* Andrew Adamson as Duloc Mascot (a man dressed in a suit that looks like Lord Farquaad)  Baby Bear from the Three Bears  Peter Pan 
* Elisa Gabrielli as additional voices 

==Production==

===Development===
At the time DreamWorks was founded, producer John H. Williams got hold of the book from his children, and when he brought it to DreamWorks, it caught Jeffrey Katzenbergs attention and the studio decided to make it into a movie.  Recounting the inspiration of making the film, Williams said:   }}
After buying the rights to the film, Katzenberg quickly put the film in active development in November 1995.   Steven Spielberg had thought about making a traditionally animated film of the book before, when he bought the rights to the book in 1991, before the founding of DreamWorks, where  , and was replaced with story artist Vicky Jenson. Both Adamson and Jenson decided to work on the film in half, so the crew could at least know who to go to with specific detail questions about the films sequences; "We both ended up doing a lot of everything", Adamson said. "Were both kinda control freaks, and we both wanted to do everything." 

Some early sketches of Shreks house were done in 1996 through 1997 using Adobe Photoshop|Photoshop, with the sketches showing Shrek first living in a garbage dump near a human village called Wart Creek. It was also thought one time that he lived with his parents and kept rotting fish in his bedroom.  Donkey was modeled after Pericles (born 1994; also known as Perry), a real miniature donkey from Barron Park, Palo Alto, California.  Raman Hui, supervising animator of Shrek, stated that Fiona "wasnt based on any real person." and he did many different sketches for Princess Fiona and had done over 100 sculptures of Fiona before the directors picked the final design.  In early development, the Art Directors visited Hearst Castle, Stratford upon Avon and Dordogne for inspiration. Art Director Douglas Rogers visited a magnolia plantation in Charleston, South Carolina for inspiration for Shreks swamp.       Planned characters not used in the film include Goldilocks and Sleeping Beauty. 

===Casting===
  was re-cast as Shrek after Chris Farleys death.]]
Chris Farley was planned to do the voice for Shrek which he recorded 80 to 90% (or 95% according to Farleys brother Tom) of the dialogue for the character, but died before completing the project.    DreamWorks then re-cast the voice role to Mike Myers, who insisted on a complete script rewrite, to leave no traces of Farleys version of Shrek.  According to Myers, he wanted to voice the character "for two reasons: I wanted the opportunity to work with Jeffrey Katzenberg; and   a great story about accepting yourself for who you are." 

After Myers had completed providing the voice for the character, when the film was well into production, he asked to re-record all of his lines in a  .  According to the DVD commentary, he had also tried using country and Canadian accents. Shrek. DVD Commentary, 2001.  After hearing the alternative, Katzenberg agreed to redo scenes in the film, saying, "It was so good we took $4m worth of animation out and did it again."  A point Myers disputes, saying "it didn’t cost the studio “millions of dollars,” as rumored. “What it meant is instead of me going in for ten sessions, I went in for twenty sessions. I got paid the same.".   Because of Myers voicing the character, more ideas began to come. There were clearer story points, fresher gags and comedy bits.  Myers said: "I got a letter from Spielberg thanking me so much for caring about the character&nbsp;... And he said the Scottish accent had improved the movie." 

Another person planned to voice a character in the film was Janeane Garofalo, who was set to star alongside Farley as Princess Fiona. However, she was fired from the project with little explanation. Years later, Garofalo stated "I was never told why  . I assume because I sound like a man sometimes? I dont know why. Nobody told me&nbsp;... But, you know, the movie didnt do anything, so who cares?" 

===Animation===
Shrek was originally set up to be a live-action/CG animation hybrid with background plate miniature sets and the main characters composited into the scene as motion-captured computer graphics, using an ExpertVision Hires Falcon 10 camera system to capture and apply realistic human movement to the characters.  A sizable crew was hired to run a test, and after a year and a half of R & D, the test was finally screened in May 1997.  The results were not satisfactory, with Katzenberg stating, "It looked terrible, it didnt work, it wasnt funny, and we didnt like it."  The studio then turned to its production partners at Pacific Data Images|PDI, who began production with the studio in 1998    and helped Shrek to get its final, computer-animated look.  At this time, Antz was still in production by the studio  and Effects Supervisor Ken Bielenberg was asked by Aron Warner "to start development for Shrek."  Similar to previous PDI films, PDI used its own proprietary software (like its own Fluid Animation System) for its animated movies. However, for some elements it also took advantage of some of the powerhouse animation software that was in the market. This is particularly true with Autodesk Maya|Maya, which PDI used for most of its dynamic cloth animation and for the hair of Fiona and Farquaad. 

"We did a lot of work on character and set-up, and then kept changing the set up while we were doing the animation," Hui noted. "In Antz, we had a facial system that gave us all the facial muscles under the skin. In Shrek, we applied that to whole body. So if you pay attention to Shrek when he talks, you see that when he opens his jaw, he forms a double chin, because we have the fat and the muscles underneath. That kind of detail took us a long time to get right."  One of the most difficult parts of creating the film was making Donkeys fur flow smoothly so that it didnt look like a Chia Pets fur. This fell into the hands of the surfacing animators who used flow controls within a complex shader to provide the fur with many attributes (ability to change directions, lie flat, swirl, etc.).  It was then the job of the visual effects group, lead by Ken Bielenberg, to make the fur react to environment conditions. Once the technology was mastered, it was able to be applied to many aspects of the Shrek movie including grass, moss, beards, eyebrows, and even threads on Shreks tunic. Making human hair realistic was different from Donkeys fur, requiring a separate rendering system and a lot of attention from the lighting and visual effects teams. 

Shrek has 31 sequences, with 1,288 shots in every sequence total.  Aron Warner said that the creators "envisioned a magical environment that you could immerse yourself into." Shrek includes 36 separate in-film locations to make the world of the film, which DreamWorks claimed was more than any previous computer animated feature before. In-film locations were finalized and as demonstrated by past DreamWorks animated movies, color and mood was of the utmost importance. 

===Music===
  Shrek series) John Powell John Powell author = title  = George Lucass Blockbusting:
A Decade-by-Decade Survey of Timeless Movies Including Untold Secrets of Their Financial and Cultural Success
| trans_title           = 
| url                   = 
| archiveurl            = 
| archivedate           = 
| format                = 
| accessdate            = October 10, 2012
| type                  = 
| edition               = 
| series                = 
| volume                = 
| date                  = 
| origyear              = 
| year                  = 2010
| month                 = 
| publisher             = HarperCollins
| location              = 
| language              = 
| isbn                  = 0061778893
| oclc                  = 
| lccn                  = 
| doi                   = 
| bibcode               = 
| id                    = 
| pages                 = 
| nopp                  = 
| at                    = 
| chapter               = 
| trans_chapter         = 
| chapterurl            = 
| quote                 = 
| ref                   = 
| laysummary            = 
| laydate               = 
| author-mask           = 
| author-name-separator = 
| author-separator      = 
| display-authors       = 
| separator             = 
| postscript            = 
| lastauthoramp         = 
}}  The score was recorded at Abbey Road Studios by Nick Wollage and Slamm Andrews, with the latter mixing it at Media Ventures and Patricia Sullivan-Fourstar handling mastering. 
 On the Road Again" and "Try a Little Tenderness" were integrated in the films score.  As the film was about to be completed, Katzenberg suggested to the filmmakers to redo the films ending to "go out with a big laugh"; Instead of ending film with just a storybook closing over Shrek and Fiona as they ride off into the sunset, they decided to add a song "Im a Believer" covered by Smash Mouth and show all the fairytale creatures in the film. 
 DreamWorks and John Cale was not, thus licensing issues prohibited Cales version from appearing in the soundtrack album, despite having the filmmakers wanting to have Cales version appear in the film. 

==Cultural references== three little Peter Pan. Snow White Beauty and the Beast. 

When Shrek crosses the bridge to the Castle and says, "Thatll do, Donkey, thatll do," this is a reference to the movie Babe (film)|Babe.  The scene where Princess Fiona is fighting the Merry Men is a lengthy reference to the film The Matrix, which was released before the film in 1999.  At the end of the film, the Gingerbread Man at the end with a crutch (and one leg) says "God bless us, everyone" which is a reference to Tiny Tim in A Christmas Carol. 

In the scene where the   and Snow White.    In addition, Lord Farquaads theme park style kingdom Duloc heavily mimics Disneyland, even in so far as parodying the famous Its A Small World Afterall musical ride in the scene with the singing puppets. 

==Release==

===Marketing===
In 2000, IMAX released CyberWorld onto its branded large-screen theaters. It was a compilation film that featured stereoscopic conversions of various animated shorts and sequences, including the bar sequence in Antz. DreamWorks was so impressed by the technology used for the sequences "stereoscopic translation", that the studio and IMAX decided to plan a big-screen 3D version of Shrek. The film would have been re-released during the Christmas season of 2001, or the following summer, after its conventional 2D release. The re-release would have also included new sequences and an alternate ending. Plans for this was dropped due to "creative changes" instituted by DreamWorks and resulted in a loss of $1.18 million, down from IMAXs profit of $3.24 million.   

Radio Disney was told not to allow any ads for the film to air on the station, stating, "Due to recent initiatives with The Walt Disney Company, we are being asked not to align ourselves promotionally with this new release Shrek. Stations may accept spot dollars only in individual markets."   The restriction was later relaxed to allow ads for the films soundtrack album onto the network. 

On May 7, 2001, Burger King began promotions for the film, giving out a selection of nine exclusive Candy Caddies based on the Shrek characters, in Big Kids Meal and Kids Meal orders.  Ice cream chain Baskin-Robbins also ran an 8-week promotion of the film, selling products such as Shreks Hot Sludge Sundae, a combination of Oreo Cookies n Cream ice cream, hot fudge, crushed chocolate cookies, whipped cream and squiggly gummy worms, and Shrek Freeze Frame Cake, featuring an image of Shrek and Donkey framed by sunflowers. This was to support the films DVD/VHS release. 

===Home media===
The film was released on VHS and DVD on November 2, 2001. Both releases included Shrek in the Swamp Karaoke Dance Party!, a 3-minute musical short film, that takes up right after Shreks ending, with films characters performing a medley of modern pop songs. 

Shrek was released to video on Friday, the same day that Pixars Monsters, Inc. hit theaters. Since videos were traditionally released on Tuesdays, Disneys executives didnt receive this well, saying "that the move seemed like an underhanded attempt to siphon off some of their films steam". DreamWorks responded that "it simply shifted the release to a Friday to make it more of an event and predicted that it and other studios would do so more frequently with important films." Monsters, Inc. earned that weekend more than $62 million, breaking the record for an animated film, while the Shreks video release made more than $100 million,  and eventually became the biggest selling DVD of all time with over 5.5 million sales. 

A 3D version of the film was released on Blu-ray 3D on December 1, 2010, along with its sequels.
The films were sold separately in 2012. 

==Reception==

===Critical response===
Shrek was well-received, with critics praising Shrek as an animated film worthy of adult interest, with many adult-oriented jokes and themes but a simple enough plot and humor to appeal to children.  Review aggregate Rotten Tomatoes reports that 87% of critics have given the film a positive review based on 190 reviews, with an average score of 7.7/10. The critical consensus is: "While simultaneously embracing and subverting fairy tales, the irreverent Shrek also manages to tweak Disneys nose, provide a moral message to children, and offer viewers a funny, fast-paced ride." 
 New York magazine liked the script, also stating that "The animation, directed by Andrew Adamson and Vicky Jenson, is often on the same wriggly, giggly level as the script, although the more "human" characters, such as Princess Fiona and Lord Farquaad, are less interesting than the animals and creatures -- a common pitfall in animated films of all types."  Peter Travers of Rolling Stone gave the film a positive review, saying "Shrek is a world-class charmer that could even seduce the Academy when it hands out the first official animation Oscar next year."  James Berardinelli of ReelViews gave the film three and a half stars out of four, saying "Shrek is not a guilty pleasure for sophisticated movie-goers; it is, purely and simply, a pleasure."  Kenneth Turan of the Los Angeles Times gave the film a positive review, saying "The witty, fractured fairy tale Shrek has a solid base of clever writing."  Lisa Schwarzbaum of Entertainment Weekly gave the film an A-, saying "A kind of palace coup, a shout of defiance, and a coming of age for DreamWorks."  Jay Boyar of the Orlando Sentinel gave the film a positive review, saying "Its a pleasure to be able to report that the movie both captures and expands upon the books playful spirit of deconstruction." 

Steven Rosen of The Denver Post gave the film a positive review, saying "DreamWorks Pictures again proves a name to trust for imaginative, funny animated movies that delight kids and adults equally."  Susan Stark of The Detroit News gave the film four out of four stars, saying "Swift, sweet, irreverent, rangy and as spirited in the writing and voice work as it is splendid in design."  Lou Lumenick of the New York Post gave the film four out of four stars, saying "A fat green ogre with a grouchy disposition and worse manners, Shrek is the sort of unlikely hero that nobody could love -- except just about everyone who sees this hip and hilarious animated delight."  Jami Bernard of the New York Daily News gave the film four out of four stars, saying "The brilliance of the voice work, script, direction and animation all serve to make Shrek an adorable, infectious work of true sophistication."  Rene Rodriguez gave the film three out of four stars, calling it "A gleefully fractured fairy tale that never becomes cynical or crass."  Elvis Mitchell of The New York Times gave the film four out of five stars, saying "Beating up on the irritatingly dainty Disney trademarks is nothing new; its just that it has rarely been done with the demolition-derby zest of Shrek."  William Steig, the author of the original book, and his wife Jeanne Steig also enjoyed the film, stating "We all went sort of expecting to hate it, thinking, What has Hollywood done to it? But we loved it. We were afraid it would be too sickeningly cute and, instead, Bill just thought they did a wonderful, witty job of it." 

John Anderson of Newsday gave the film a positive review, saying "The kind of movie that will entertain everyone of every age and probably for ages to come."  John Zebrowski of The Seattle Times gave the film three out of four stars, saying "The movie is helped immensely by its cast, who carry it through some of the early, sluggish scenes. But this is Murphys movie. Donkey gets most of the good lines, and Murphy hits every one."  Jay Carr of The Boston Globe gave the film a positive review, saying "In an era when much on film seems old, Shrek seems new and fresh and clever."    Stephen Hunter of The Washington Post gave the film five out of five stars, saying "Despite all its high-tech weirdness, is really that most perdurable of human constructions, a tale told well and true."  Joe Baltake of The Sacramento Bee gave the film a positive review, saying it "Isnt so much a fractured spoof of everything Disney, but actually a Monty Python flick for kids -- kids of all ages."  Andrew Sarris of The New York Observer gave the film a positive review, saying "What gives Shrek its special artistic distinction is its witty and knowingly sassy dialogue, delivered by vocally charismatic performers whose voices remind us of their stellar screen personae in live-action movies."  Lisa Alspector of the Chicago Reader gave the film a positive review, saying "This romantic fantasy complicates the roles of beauty and beast, making it hard to guess what form a sensitive resolution will take."  Joe Morgenstern of The Wall Street Journal gave the film a positive review, saying "The charms of Shrek, which is based on the childrens book by William Steig, go far beyond in-jokes for adults." 

A mixed review came from Mark Caro of the Chicago Tribune, who gave the film two and a half stars out of four and compared the film to Toy Story 2, claiming it "had a higher in-jokes/laughs ratio without straining to demonstrate its hipness or to evoke heartfelt emotions."  On the more negative side, Michael Atkinson of The Village Voice said he was "desperately avoiding the risk of even a half-second of boredom," and said "the movie is wall-to-window-to-door noise, babbling, and jokes (the first minute sees the first fart gag), and demographically its a hard-sell shotgun spray."  Christy Lemire of the Associated Press described Shrek as a "90-minute onslaught of in-jokes," and said, while it "strives to have a heart" with "a message about beauty coming from within," "somehow   rings hollow."  Anthony Lane of The New Yorker said, despite the film "cunning the rendering of surfaces, theres still something flat and charmless in the digital look, and most of the pleasure rises not from the main romance but from the quick, incidental gags." 

===Box office===
Shrek opened in more than 3,587 movie theaters on its 2001 release,  with eleven of them showing the film digitally, made possible by the  , and Monsters, Inc.. 

Shrek became the highest-grossing animated film ever to be released in  , earning a $20.3 million since its opening in the UK. 

===Accolades===
At the  .  Entertainment Weekly put it on its end-of-the-decade, "best-of" list, saying, "Prince Charming? So last millennium. This decade, fairy-tale fans — and Princess Fiona — fell for a fat and flatulent Ogre. Now, thats progress."  It was nominated for The Golden Globe Award for Best Motion Picture – Musical or Comedy. 
 Best Animated Outstanding Individual Achievement for Directing in an Animated Feature Production. 
 Ten top Ten"; the best ten films in ten "classic" American film genres—after polling over 1,500 people from the creative community Shrek was acknowledged as the eighth best film in the animated genre, and the only non-Disney·Pixar film in the Top 10. 
    Shrek was also ranked second in a Channel 4 poll of the "100 Greatest Family Films", losing out on the top spot to E.T. the Extra-Terrestrial.  In 2005, Shrek came sixth in Channel 4s 100 Greatest Cartoons poll behind The Simpsons, Tom and Jerry, South Park, Toy Story and Family Guy.  In November 2009, the character, Lord Farquaad, was listed #14 in IGN UKs "Top 15 Fantasy Villains".  In 2006, it was ranked third on Bravo (U.S. TV channel)|Bravos 100 funniest films list. 

American Film Institute recognition:
* AFIs 100 Years...100 Heroes & Villains:
** Shrek - Nominated Hero 
* AFIs 100 Years...100 Songs:
** Im a Believer - Nominated 
* AFIs 100 Years...100 Movies (10th Anniversary Edition) - Nominated 
* AFIs 10 Top 10 - #8 Animated film 

===Festivals=== Peter Pan (1953) to receive that honour. 

===Influence=== The Princess Chicken Little.  It also inspired a number of computer animated films which also spoofed fairy tales, or other related story genres, often including adult-oriented humor, most of which were not nearly as successful as Shrek, such as Happily NEver After, Igor (film)|Igor, and Hoodwinked!. 

==Other media==
 
Several   (2002),   (2002),   (2002) and Shrek SuperSlam (2005).  Shrek was also included as a bonus unlockable character in the video game Tony Hawks Underground 2 (2004). 
 trade paperback. 
 West End Best New Musical. 

==Sequels and spin-offs==
  Puss in Boots (a prequel to the Shrek series, exploring Pusss origin story and his life before meeting Shrek and Donkey) and several shorts.    A fifth feature film was also planned for release, but was later cancelled in 2009, after it was decided that Shrek Forever After (originally titled Shrek Goes Fourth) was to be the last film in the series. 

==References==

===Citations===
 

==Bibliography==
 
*  
*  
 

==External links==
 
 
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 