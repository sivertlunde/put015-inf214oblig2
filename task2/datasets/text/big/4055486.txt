Time Walker
{{Infobox film
| name = Time Walker
| image = time_walker.jpg
| caption = Time Walker movie poster Tom Kennedy
| writer = Tom Friedman and Jason Williams (story) Tom Friedman and Karen Levitt (screenplay)
| producer = Dimitri Villard Jason Williams
| starring = Ben Murphy Nina Axelrod Kevin Brophy James Karen Austin Stoker Darwin Joston Antoinette Bower Sam Chew Jr. Shari Belafonte Jack Olson; Alien
| cinematography = Robbie Greenberg
| music = Richard Band
| editing =
| distributor = New World Pictures
| runtime = 83 min.
| released = November 1982 (USA)
| country =  
| budget = $750,000 (estimated)
| gross= Unknown English
}} Tom Kennedy. This movie (under the title Being from Another Planet) was featured in Mystery Science Theater 3000 episode 405,  which first aired on July 4, 1992.

==Plot==

While California University professor Douglas McCadden (Ben Murphy) explores the tomb of the ancient Egyptian king Tutankhamun, an earthquake causes a wall in the tomb to collapse, revealing a hidden chamber. Inside, McCadden finds a mummy in a sarcophagus. Unbeknownst to McCadden, the "mummy" is not the body of a dead Egyptian, but an extraterrestrial alien in suspended animation, being wrapped up and buried alive thousands of years before and covered with a dormant, green fungus. 

The body is brought back to California and McCadden has it examined by Dr. Ken Melrose (Austin Stoker) and X-rayed by student Peter Sharpe (Kevin Brophy) before a big press conference about the discovery. While reviewing the X-rays, Sharpe notices there are five crystals around the "mummys" head. Sharpe steals the crystals and makes new X-rays to cover up his theft. He sells four of the crystals to students who are unaware of their origin. The second set of X-rays overdose the body with radiation. This causes the fungus to re-activate and the alien to awaken from suspended animation.

At the press conference the next day, one of the students touches the fungus on the sarcophagus, which eats away one of his fingers. The sarcophagus is then opened in front of the press to reveal that the mummy is gone. Melrose and his colleague Dr. Hayworth (Antoinette Bower) attempt to identify the fungus and destroy it.

At first, everyone assumes that the mummys disappearance is because of a fraternity prank. However, University President Wendell Rossmore (James Karen) wants to pin the "theft" on McCadden, so that he can give the Egyptian departments directorship to his flunkie, Bruce Serrano (Sam Chew Jr).

Meanwhile, the "mummy" tracks down the students who have the stolen crystals. The crystals are crucial components of an intergalactic transportation device that will allow the alien to return to its home planet. The alien violently reclaims its crystals, and, when he brutally attacks a female student, Lt. Plummer (Darwin Joston) is called in to investigate the crime. As more students turn up dead or injured, Plummer believes that he is on the trail of a serial killer.
 hieroglyphic text from the sarcophagus. He hopes it will reveal the identity of the mummy. The text reveals that Tutankhamun found the alien in a coma-like state. Thinking that the unconscious alien was a god, Tutankhamun and his attendants touched it and were killed by its infectious fungus. The king and the alien were then buried together in the kings tomb. McCadden, having figured out that the "mummy" is an alien, makes the connection between the alien and the crystals. He then traces the stolen crystals back to Sharpe, who admits to the theft and gives McCadden the one crystal he kept for himself.

In the end McCadden, Rossmore, Serrano, two students, a security guard and the alien all end up in a boiler room where the alien has set up its transportation device. The alien activates the device by placing the last recovered crystal on it; his mummy wrappings disintegrate, revealing his true form. The security guard urged by Serrano shoots at the alien, but McCadden leaps in front of the alien to protect it. As McCadden lies injured the alien takes McCaddens hand, and the two disappear. A single crystal is left where the alien stood. Serrano grabs the crystal, and the fungus begins to destroy his hand.

==John Carpenter Connection==
This film is also notable for featuring the two lead actors from John Carpenters cult classic Assault on Precinct 13 in a film together again: Austin Stoker and Darwin Joston.

==See also==
* List of American films of 1982

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 