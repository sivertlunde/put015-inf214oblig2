Yesteryou, Yesterme, Yesterday
 
 
{{Infobox Film
| name           = Yesteryou, Yesterme, Yesterday 記得...香蕉成熟時
| director       = Samson Chiu
| producer       = Peter Chan Ho Sun Wallace Cheung Kwok Chung
| writer         = 
| screenplay     = 
| story          =
| based on       = 
| narrator       = 
| starring       = 
| music          = Richard Lo
| cinematography = Jingle Ma
| editing        = Chan Ki-hop
| studio         = Movie Impact Limited
| distributor    = 
| released       =   
| runtime        = 93 minutes
| country        = Hong Kong Cantonese
| budget         = 
| gross          = HK$ 6.08 M. (Hong Kong) 
}}

Yesteryou, Yesterme, Yesterday (記得...香蕉成熟時) is a 1993 Hong Kong Cantonese comedy-drama film directed by Samson Chiu and starring Fung Bo Bo, Ellen Lo, Eric Tsang, John Tang and Roy Wong.

The film forms a trilogy with:
* Over the Rainbow, Under the Skirt (1994)
* Yesterday You, Yesterday Me (1997)

==Cast==
* Fung Bo Bo 
* Ellen Lo
* Eric Tsang
* John Tang
* Ann Bridgewater
* Roy Wong
* Michael Miu
* Barry Wong

== External links ==
*  
*  
*  

 
 


 