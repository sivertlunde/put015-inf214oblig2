D-Tox
 
{{Infobox film
| name = D-Tox
| image = D-Tox.jpg
| director = Jim Gillespie
| producer = Karen Kehela Ric Kidney Kevin King Howard Swindle
| based on =  
| story = Ron L. Brinkerhoff Jeffrey Wright Kris Kristofferson John Powell
| cinematography = Dean Semler
| editing = Tim Alverson Steve Mirkovich
| studio = KC Medien Capella International Universal Pictures (UK) DEJ Productions (US)
| released  =  
| runtime = 96 minutes
| country = United States
| language = English
| budget = $55 million 
| gross = $6,337,141
}} psychological thriller film directed by Jim Gillespie, and starring Sylvester Stallone and Charles S. Dutton. The film was released in the United States on September 20, 2002.
 1999 novel Howard Swindle, three years later, where it was given a limited release under the title Eye See You by DEJ Productions.

==Plot== FBI agent Jake Malloy is pursuing a serial killer who specializes in killing police officers. Malloys former partner becomes one of the victims. While Malloy is at his partners home, the murderer calls Malloy from Malloys home while Mary, his girlfriend, is there. The killer tells Malloy that he pursued him four years before for a series of prostitute murders; he is holding a grudge and hes going to kill Mary. Malloy rushes home to find Mary dead. When the killers hideout is found, Seattle police and the FBI blockade the area before Malloy arrives. Malloy pursues the killer only to find that he appears to have committed suicide.

Three months later, Malloy descends into alcoholism over Marys murder. After Malloy slits his wrists in an unsuccessful suicide attempt, his best friend and supervising officer, Agent Chuck Hendricks, enrolls Malloy in a rehabilitation program designed for law enforcement officers. The clinic was formerly an abandoned military base. Dr. John "Doc" Mitchell, a former cop and recovering alcoholic, established the rehab center as a way of dealing with his problems. Hendricks stays in Wyoming to ensure Malloy will be okay.

Malloy meets several other officers who are patients in the clinic, including Peter Noah, an arrogant and paranoid ex-SWAT officer, Frank Slater, a cynical, opinionated British police officer, Willie Jones, a religious homicide detective, Jaworski, an alcoholic narcotics cop who attempted suicide, Lopez, a foul-mouthed LAPD officer, and McKenzie, an elderly member of the Royal Canadian Mounted Police who saw his partner get killed. He meets several staff members, including Docs assistant and mechanic Hank and compassionate resident psychiatrist and nurse Jenny Munroe, with whom Malloy develops a bond.

A blizzard seals everyone in the rehab center with no outside communication. The next day, Jenny finds the body of Connor, one of the troubled patients who apparently killed himself. Jenny suspects otherwise, believing Connor would have come to her. The next morning Hank finds the body of Carl Brandon, another patient. Brandon also seems to have killed himself, but Malloy deduces otherwise.

Doc locks up the surviving patients while he reviews their files. Jenny informs Doc that Jack Bennett, a deranged employee who was a former patient in the clinic, is missing along with a snowmobile. Malloy convinces Jenny to lock her room. After Doc is killed by a man wielding an axe, everyone but Malloy and Jenny think Jack is the killer. Malloy opens the safe and hands the surviving cops their sidearms. Hendricks finds a body in a frozen lake, and learns from the owner of a nearby fishing shop that the victim was a cop. Hendricks and the owner of the shop return to the clinic.

Hank, the clinics cook Manny, and helper Gilbert, volunteer to drive through the blizzard in a pickup truck to get outside help. Malloy wants Jenny to leave as well, but she refuses. While driving away, Hank sees something in front of him and veers away from it. The truck slides off the icy road, crashing into a tree at the bottom of a ravine. Malloy and Jenny hear the crash, and Malloy hands a gun to Jenny before investigating. Malloy finds Manny murdered after he survived the crash. Malloy also finds Jacks body, the object that caused Hank to crash the truck. Malloy is startled by Gilbert approaching behind him. Gilbert, alive but scared, flees while Malloy rushes back to the clinic.

McKenzie, who is patrolling the old execution chamber, finds the machine activated. The killer electrocutes McKenzie, deactivating the buildings power and heating system. Malloy forces everyone except Jenny to their cells as he realizes one of the cops is a killer impersonating a patient. Suspecting the man responsible is Marys killer, Malloy exams Connors body in the kitchen. Remembering the killer saying, "I see you, but you dont see me", Malloy looks inside Connors eyelids and finds "I" on one eyelid and "CU" on the other. As Malloy and Jenny return to the cells, Hank, thinking Malloy might be the killer, knocks him out. He locks Malloy in Slaters cell and releases everyone else.

Malloy finds a vent, but as he uses a matchbook from Slater to light the cell, Malloy learns it came from a Seattle restaurant frequently visited by cops and Malloy himself, which reveals Slater as the killer as he watches policemen to learn about their habits and Malloys. Malloy escapes the cell and finds the missing badges above Slaters room, which he collects as trophies. After establishing his innocence, Malloy has Jones and Lopez conduct patrol while Jaworski stays with Jenny in the patient room. Malloy heads into the tunnels beneath the facility.

Hank, Noah, and Slater go to the tunnels to retrieve logs for heating, with the other two unaware that Slater is the killer.  Slater convinces them to split up before killing each of them. As Malloy patrols the tunnels, Slater taunts him over a CB radio and lures Malloy to Noahs hanged body where he finds the other radio. Slater is about to leave the clinic, but hears Jenny calling out Malloys name. Malloy learns that Slater is at the tunnels trapdoor and rushes to save Jenny.

Outside the installation, Hendricks and the fishing shop owner find Gilbert alive and bring him inside the snowcat before arriving at the rehab center. Hendricks sees Jennys footprints and follows them. Jenny runs to a nearby shed, hiding from Slater. Malloy arrives, telling Jenny to stay inside the shed. Slater catches Hendricks before Malloy catches him from behind. Slater jumps into the shed and knocks Jenny out and wounds Hendricks. Malloy and Slater confront each other, with Malloy ultimately killing Slater.

Jenny regains consciousness and helps the wounded Hendricks walk to the clinic with Malloy, whose left arm was stabbed during the fight, following them. Malloy stops for the moment and puts the ring he was planning to propose to Mary with on a tree branch before walking away from it.

==Cast==
* Sylvester Stallone as FBI agent Jake Malloy
* Charles S. Dutton as FBI agent Chuck Hendricks
* Polly Walker as Jenny Munroe
* Kris Kristofferson as Dr. John "Doc" Mitchell Mif as Carl Brandon
* Christopher Fulford as Frank Slater Jeffrey Wright as Jaworski
* Tom Berenger as Hank
* Stephen Lang as Jack Bennett
* Alan C. Peterson as Gilbert
* Hrothgar Mathews as Manny Angela Alvarado Rosa as Lopez
* Robert Prosky as McKenzie
* Robert Patrick as Peter Noah
* Courtney B. Vance as Willie Jones
* Sean Patrick Flanery as Conner
* Dina Meyer as Mary
* Rance Howard as Geezer
* Tim Henry as Weeks

==Production==
 
The film was set in Los Angeles California, Vancouver British Columbia and Toronto Ontario Canada on January 25 and May 23, 1999.

After movie was finished, Universal studio decided to screen it to a test audience but all screenings of the first cut got very bad reactions by them. Movie was then shelved for quite some time while re-shoots and story changes were being done.   Composer John Powell wrote two complete scores for the film, one of which was rejected. With the film delayed and literally banished to a European release by Universal due to the studios dissatisfaction with the film in general, most of Powells score was replaced with additional music by William Ross, Geoff Zanelli, and Nick Glennie-Smith as an attempt to make the film salvageable. New ending was also filmed in which main villain is killed in different way.   But even after re-shoots and title changes (Movie was originally called Detox and then title was changed into The Outpost and then into Eye See You) Universal studio didnt care for it and released it over three years after it was originally finished but in limited release.

In Q&A for Aint It Cool News in December 2006, Sylvester Stallone was asked why movie didnt get wider release, and this is his answer;

It’s very simple why D-TOX landed in limbo. A film is a very delicate creature. Any adverse publicity or internal shake-up can upset the perception of - and studio confidence in - a feature. For some unknown reason the original producer pulled out and right away the film was considered damaged goods; by the time we ended filming there was trouble brewing on the set because of overages and creative concerns between the director and the studio. The studio let it sit on the shelf for many months and after over a year it was decided to do a re-shoot. We screened it, it tested okay, Ron Howard was involved with overseeing some of the post-production… but the movie had the smell of death about it. Actually, if you looked up, you could see celluloid buzzards circling as we lay there dying on the distributor’s floor. One amusing note: It was funny, when we were met at the airport by the teamsters they’d have a sign in front of them saying DETOX, and all these actors like Kris Kristofferson, Tom Berenger and myself looked like we were going into rehab rather than a film shoot.  

==Reception==
The film received negative reviews. Danny Graydon of BBC Films said: "Clearly, Hollywoods confidence in this film is lower than Pee-Wee Hermans Oscar chances, and their instincts are right: a boring, formulaic mix of serial killers and stalknslash, this will not reinvigorate Sylvester Stallones action hero status or loosen his maniacal destruction of the quality control button". 

==Home media==
  Region 2 in the United Kingdom on 17 June 2002, it was distributed by Universal Studios.

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 