Gabbia
 

{{Infobox Film |
  name           = Gabbia |
  image          = Gabbia_Poster.jpg|
  writer         = Francesco Roder|
  starring       = Claudia Soberón | 
  director       = Francesco Roder |
  producer       = Placido Roder, Dulce Maria Bullé Goyri, Gian Paolo Roder |
  released       = April 14, 2003 (Gradisca International Video Short Film Festival, Italy) |
  runtime        = 13 minutes |
  music          = Federica Diana|
}}
Gabbia is a 2003 independent short-film directed by Italian filmmaker Francesco Roder, who also wrote the script. It stars Mexican actress Claudia Soberón.

== Plot ==
An empty room. Small. Dark. Dirty. A young woman is pushed in and the door is locked. Its the beginning of the worst nightmare as her entire freedom is slowly taken away from her. Forever.

==Cast==
*Claudia Soberón Enclosed

==Production==
Gabbia began production in early 2002, with a story that evolved from a 1-minute video commercial screenplay to a 13-minutes short-film script. The video was shot in late 2002 in Sacile, a small town in north  . The short film was then released in several other short film festivals in Italy. A positive review was published in the popular HorrorCult.com, considered one of the biggest web sites about horror films. Gabbia "is a really well crafter short-film" with "a solid direction by young Francesco Roder" and starring "an extraordinary Claudia Soberon", wrote reviewer Federico Caddeo.

==Festivals==
*Gradisca International Video Short Film Festival 2003 (Quality Student Film certificate) 
*Tirrenia Trema Short Film Festival 2003
*LInvasione degli Ultracorti Short Film Festival 2004
*Bassano Short Film Festival 2004
*Campobasso Short Film Festival 2004
*Fuori Visione Showcase 2005
*Notte di Corti 2005 (Best Actress Award)
*Palermo International Videoart, Film and Media Festival 2005

==External links==
* 

 
 
 
 


 