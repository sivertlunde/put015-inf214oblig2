Sommar och syndare
{{Infobox film
| name           = Sommar och syndare
| image          = 
| image size     = 
| caption        = 
| director       = Arne Mattsson
| producer       = 
| writer         = Willy Breinholst Erik Pouplier
| narrator       = 
| starring       = Yngve Gamlin
| music          = 
| cinematography = Åke Dahlqvist
| editing        = Lennart Wallén	
| distributor    = 
| released       = 26 September 1960
| runtime        = 
| country        = Sweden 
| language       = Swedish
| budget         = 
| preceded by    = 
| followed by    = 
}}

Sommar och syndare is a 1960 Swedish film directed by Arne Mattsson and starring Yngve Gamlin. 

==Cast==
* Yngve Gamlin - Prof. Cornelius
* Lena Granhagen - Mrs. Zitter
* Nils Hallberg - Åke Johansson
* Karl-Arne Holmsten - Emil Horneberg
* Sture Lagerwall - Sven Molmagen
* Yvonne Lombard - Heidi Horneberg
* Dirch Passer - Kansas Joe
* Gio Petré - Liselotte Högsbo
* Elsa Prawitz - Helga Krus
* Sif Ruud - Ms. Prytz
* Olof Thunberg - Ove Högsbo

==External links==
* 

 
 
 
 
 