En Selvame
{{Infobox film
| name = En Selvame
| image = EnSelvame1.jpg
| caption = LP Vinyl Records Cover
| director = S. P. Muthuraman
| writer = Suman Ambika Ambika
| producer = Mani
| music = Illayaraja
| editor =
| released = 1985
| runtime = Tamil
| budget =
}}
 1985 Tamil directed by Suman and Ambika in lead roles.   

==Cast==
 Suman
*Ambika Ambika
*Anju Baby Anju

==Soundtrack ==

{{Infobox album Name     = En Selvame Type     = film Cover    = Released =  Artist    = Illayaraja Genre  Feature film soundtrack Length   =  Label    = Echo
}}

The music composed by Illayaraja.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics 
|-
| 1 || Sonnalum Nalale... || Malaysia Vasudevan, S. P. Sailaja || Panchu Arunachalam
|-
| 2 || Anbe Deivam Endre... || Radhika || Panchu Arunachalam
|-
| 3 || Kaalam Neram Kaninthu Varum... || Malaysia Vasudevan, S. Janaki || Panchu Arunachalam
|-
| 4 || Naan Vaazha Kuruthu Azhagu... || S. Janaki || Gangai Amaran
|-
|}

==References==
 

==External links==
*  
 

 
 
 
 
 


 