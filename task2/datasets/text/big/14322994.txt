Black Samurai
{{Infobox film
| name           = Black Samurai
| image          = Black Samurai.jpg
| caption        = Theatrical poster to Black Samurai (1977)
| director       = Al Adamson
| producer       =
| writer         = B. Readick
| narrator       = Jim Kelly
| music          =
| cinematography = Louis Horvath
| editing        = Jim Landis
| distributor    =
| released       =  
| runtime        = 88 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} American blaxploitation Jim Kelly.  The script is credited to B. Readick, with additional story ideas from Marco Joachim.  The film is based on a novel of the same name, by Marc Olden.    

==Plot==
Robert Sand, agent of D.R.A.G.O.N. (Defense Reserve Agency Guardian Of Nations), is playing tennis on his vacation with a beautiful black girl, when his commanding officers ask him to save a Chinese girl named Toki who happens to be Sands girlfriend, and the daughter of a top Eastern Ambassador. The ransom for the abduction was the secret for a terrific new weapon - the freeze bomb - but the Warlock behind the deed is also into the business of drug dealing and Voodoo ritual murders. The search takes him from Hong Kong to California through Miami, and plenty of action, against bad men, bad girls, and bad animals.

==Cast== Jim Kelly as Robert Sand
*Bill Roy as Janicot
*Roberto Contreras as Chavez
*Marilyn Joi as Synne
*Essie Lin Chia as Toki Konuma
*Biff Yeager as Pines
*Charles Grant as Bone
*Jace Khan as Jace
*Erwin Fuller as Bodyguard
*Peter Dane as Farnsworth
*Felix Silla as Rheinhardt
*Cowboy Lang as Himself
*Little Tokyo as Himself
*Jerry Marin as Shotgun Spiro
*Alfonso Walters as Leopard Man
*Charles Walter Johnson	as Leopard Man
*Regina Carrol as Voodoo Dancer / Party Stripper (as Gina Adamson)
*Jesus Thillet as Martial Arts Fighter
*Cliff Bowen as Martial Arts Fighter
*DUrville Martin (uncredited)
*Aldo Ray as DRAGON chief (uncredited)

==See also==
* Yasuke, a real life black samurai who existed during the Sengoku period of ancient Japanese history.

== References ==
 

==External links==
* 
 
 
 
 
 
 

 