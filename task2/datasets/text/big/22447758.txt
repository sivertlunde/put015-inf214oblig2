The Crazies (2010 film)
{{Infobox film
| name           = The Crazies
| image          = Crazies ver2.jpg
| caption        =  Theatrical release poster
| screenplay     = Scott Kosar Ray Wright The Crazies by George A. Romero
| director       = Breck Eisner
| producer       = Michael Aguilar Dean Georgaris Rob Cowan
| music          = Mark Isham
| editing        = Billy Fox
| cinematography = Maxime Alexandre Imagination Abu Dhabi
| distributor    = Overture Films Joe Anderson Danielle Panabaker 
| released       =  
| runtime        = 101 Minutes 
| country        = United States
| language       = English
| budget         = $20 million   
| gross          = $54,677,170 
}} 1973 film of the same name by George A. Romero, who is also an executive producer of the remake.
 Pierce County, Iowa, "friendliest place on Earth," whose town water supply is accidentally infected with the "Trixie" virus. After an incubation period of 48 hours, this virus gradually transforms the mental state of the infected into that of cold, calculating, depraved, bloodthirsty killers, who then prey on family and neighbors alike.

The film was released on February 26, 2010 to positive reviews from critics, and was a box office success both domestically and internationally.

==Plot== Joe Anderson), spots Rory Hamill, a local resident, entering the outfield holding a shotgun, presumably drunk. David is forced to shoot and kill the unresponsive Rory when he begins to raise his weapon. David and his wife Judy (Radha Mitchell), the community doctor, begin to notice other town residents exhibiting bizarre behavior, including listlessness and repetitive speech.  The next night, a local farmer burns his house down with his wife and son trapped inside, killing them. By the time firefighters and other townspeople arrive, he is found mowing his lawn while his house burns.

While investigating the discovery of a pilots body in a swamp, David and Russell discover a military aircraft submerged under the water, which David realizes has contaminated the drinking water and caused the town residents odd behavior. Against the mayors direct orders, he shuts off the towns water supply. Soon after, communication services are lost in town, and soldiers arrive to take all residents to quarantine at the high school. Everyone is examined for symptoms of infection. Judy does not pass the examination and is separated from David. She explains her fever is due to pregnancy, but is sedated anyway. She wakes strapped to a gurney as the perimeter of the facility is breached by the infected townspeople in an effort to escape. The military personnel evacuate, abandoning Judy and the other civilians. David is nearing evacuation, but escapes and rejoins Russell. They rescue Judy and her hospital assistant, Becca (Danielle Panabaker).
 biological weapon. It was en route to Texas to be destroyed by incineration when the plane crashed. Enraged, Russell shoots the driver and threatens Judy and David. When confronted about his behavior, Russell realizes he is infected and, after being disarmed, begs to go on with Judy and David. He later dies while distracting soldiers at a roadblock, so that Judy and David can sneak past.
 Cedar Rapids, a view from a military satellite highlights first the couple, then the city, and the words "Initiate containment protocol" appear, signifying a new containment attempt.

In the credits, Bruce Aune, a real newscaster from KCRG-TV 9 in Cedar Rapids, Iowa reports that an explosion originating from the Dakon Pendrill chemical plant started a massive fire in Ogden Marsh. He says a perimeter has been set and civilians are not being allowed into the area. A Trixie-infected individual appears on camera just before the signal is lost.

==Cast==
 
 
*Timothy Olyphant as David 
*Radha Mitchell as Judy  Joe Anderson as Russell 
*Danielle Panabaker as Becca 
*Christie Lynn Smith as Deardra Farnum
*Brett Rickaby as Bill Farnum
*Preston Bailey as Nicholas 
*John Aylward as Mayor Hobbs
*Joe Reegan as Pvt. Billy Babcock
*Glenn Morshower as Intelligence Officer
*Larry Cedar as Ben Sandborn
*Gregory Sporleder as Travis Quinn
*Mike Hickman as Rory Hamill
*Lisa K. Wyatt as Peggy Hamill
*Justin Welborn as Curt Hammil
*Chet Grissom as Kevin Miller
*Tahmus Rounds as Nathan
*Brett Wagner as Jesse
*Alex Van as Red
*Anthony Winters as Town Pastor
*Frank Hoyt Taylor as Mortician Charles Finley
*Justin Miles as Scotty McGregor Marian Green as Mrs. McGregor
*E. Roger Mitchell as Tom
*Bruce Aune as News Anchor
 

==Production==
Much of the film was shot in  .  The special effects were created by Robert Green Hall.  Actress Lynn Lowry, a star from the original film, makes a cameo in the remake billed as "Woman on Bike".

===Makeup===
  Prom Night. Director Breck Eisners first visions of what the infected would look like were zombies. He and the makeup crew made many molds and sketches of what the infected should look like, with deformities and skin hanging off and so forth. Eventually, he grew tired of the "zombie" look which he believed to be too cliché and decided to go for a more realistic "go under the skin," in which the blood vessels would appear to be bursting forth and face and neck muscles and tendons tight and wrought. Eisner has described this look as "hyper alive."

The directors one and only rule for the makeup design was that they would have to research in medical books and consult medical professionals for the design of the infected. Lead make-up artist Rob Hall said "If we were to pitch something to Breck, about, if you know, one side of his face should look like this, Breck would immediately want to know what disease it came from, and what version of reality it could be implemented into Trixie. But the most important thing was to make sure it felt real. Make it feel like you could get it, too." The basis of the makeup the crew used was mainly rabies, tetanus and Stevens–Johnson syndrome.

Each "Crazy" design has about 21 separate pieces that took over three hours to apply for the final effect seen in the film. Robert stated the final effect in the film seen was not just the makeup, but the lighting, camera angles, and post-production effects were the main factor. The main theme for the design was "stress." He has stated he wanted the "Crazies" to look stressed out. The veins and eyes were the main focus of the design. The contact lenses covered the actors entire eyes and required eyedrops every five minutes to prevent permanent eye damage. 

==Release==
The film premiered on February 24, 2010 in Los Angeles  and received a wide release in the North America on February 26, 2010.  The Canadian DVD and Blu-ray Disc were released June 29, 2010.  The DVD and Blu-ray Disc + Digital Copy combo pack was released in the North America on June 29, 2010 and in the UK on July 19. 

===Critical reception===
Reviews for the film have been generally positive. Based on 146 reviews collected by Rotten Tomatoes, the film has an overall approval rating of 72%, with an average score of 6.4/10. The sites consensus states the film is "Tense, nicely shot, and uncommonly intelligent, The Crazies is the rare horror remake that works."  By contrast, Metacritic calculated a "mixed or average" score of 55% based on 30 reviews. 
 Michael Phillips of The Chicago Tribune awarded the film 3½ stars of 4 commenting that he "greatly prefer this cleverly sustained and efficiently relentless remake to the 73 edition. It is lean and simple."  Eric M. Armstrong of The Moving Arts Film Journal wrote that "The Crazies is a solid B-movie and one of the few remakes that actually surpasses the original."   Ty Burr of The Boston Globe gave the film 3/4 stars touting the film as "extremely solid stuff – about as good as you could hope from a B-movie retread."  Variety (magazine)|Variety film critic Dennis Harvey also praised the film, writing "While not a slam dunk, this revamp by helmer Breck Eisner (of the enjoyable but underperforming Sahara (2005 film)|Sahara) emerges an above-average genre piece thats equal parts horror-meller and doomsday action thriller. 

However, Owen Gleiberman of Entertainment Weekly graded the film a C, writing, "I dont care how this premise has been dressed up, weve seen it a jillion times before."  Mike Hale of The New York Times wrote a mixed review stating "The filmmakers seem so determined to make a serious, respectable horror movie that they have only the bare minimum of fun."  Amy Biancolli, writing for San Francisco Chronicle, wrote that the remake "boasts less of the plot and fewer characters than the original, but the hairdos are spiffier and the special effects have graduated from cheapo stage blood to the extravagant gross-outs that horror audiences now routinely expect." 

===Box office=== Cop Out Shutter Island with $16,067,552.  By May 2010, the film has grossed an estimated $50 million worldwide. 

==Nominations==
{| class="wikitable"
|-
! Year !! Award !! Category !! Result
|-
| 2011
| Peoples Choice Awards
| Favorite Horror Movie
|  
|}

==Merchandise==
On February 23, 2010, an iPhone app, Beware the Infected, was released. 

===Comic book===
On February 17, 2010, iTunes released a graphic novel adaptation of the film.  A comic book was also released chronicling how the virus was spread. It went on for four issues.

===Browser game=== Starz Digital Media released a Facebook game based on the film. 

==References==
 

==External links==
* 
* 
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 