Invitation to Happiness
{{Infobox film
| name           = Invitation to Happiness
| image          = Invitation to Happiness poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Wesley Ruggles
| producer       = Wesley Ruggles 
| screenplay     = Claude Binyon
| story          = Mark Jerome
| starring       = Irene Dunne Fred MacMurray Charlie Ruggles Billy Cook William Collier, Sr. Marion Martin
| music          = Friedrich Hollaender
| cinematography = Leo Tover 
| editing        = Alma Macrorie
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 97 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}

Invitation to Happiness is a 1939 American drama film directed by Wesley Ruggles and written by Claude Binyon. The film stars Irene Dunne, Fred MacMurray, Charlie Ruggles, Billy Cook, William Collier, Sr. and Marion Martin. The film was released on June 16, 1939, by Paramount Pictures.  

==Plot==
 

==Cast==
*Irene Dunne as Eleanor Wayne 
*Fred MacMurray as Albert King Cole
*Charlie Ruggles as Henry Pop Hardy
*Billy Cook as Albert Cole Jr.
*William Collier, Sr. as Mr. Wayne
*Marion Martin as Lola Snow
*Oscar OShea as Divorce Judge
*Burr Caruth as Butler
*Eddie Hogan as The Champ

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 

 