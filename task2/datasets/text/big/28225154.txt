Northern Comfort
Northern Comfort is a 2010 American improvisational film starring   starring  | url=http://www.rottentomatoes.com/m/greenberg/news/1919487/2/greenbergs_greta_gerwig_americas_next_it_girl
|accessdate=October 18, 2010}} 

==Synopsis==
The film tells the story of Horace (Webber) and Cassandra (Gerwig)  who meet while traveling to Canada. The pair skirt around their feelings as Gerwig deals with an illness which shes keeping to herself. "Because they’re damaged in similar ways — we learn they’re both the black sheep of their respective families — they make for sweet-and-sour traveling companions, or would if Cassandra didn’t keep disappearing on Horace. The people they meet on the road offer varying object lessons in coping with the disaster of living. A comically mismatched couple at a roadside motel (Joseph James Bellamy and Irina Peligrad) seem ready to seduce our heroes or at least drag them into a discussion of superheroes as modern gods. A hermit (Markus Nechay) offers Horace and Cassandra beds for the night, a musical interlude, and a glimpse of one man’s sad isolation. The closest the movie gets to a statement comes from an older man in a diner (Robert Koch) who locates nirvana in the act of tying flies."    

==Critique==
The Boston Globe described the film as "a tiny movie that casts a tiny but genuine spell."    
 CGI as the pair bumbles their way through the plot, meets real people along the way, awkwardly waxing philosophy and never mentioning their feelings to each other, while being filmed. All of which makes Webber, decidedly anti-James Cameron."  {{cite news|title=Northern Comfort|author=Paul Ryan|date=May 28, 2010|work=The Weekly Dig| url=http://www.weeklydig.com/arts-entertainment/201005/northern-comfort
|accessdate=October 18, 2010}} 

==Reel Fest==
Northern Comfort premiered at  . Reel Fest 2 was May 28 to June 2, 2010. {{cite news|title=Keeping It Reel: A unique Opportunity for Local Filmmakers|author=Kerry O’Donnell|date=May 5, 2010|work=Newenglandfilm.com| url=http://www.weeklydig.com/arts-entertainment/201005/northern-comfort
/accessdate=October 18, 2010}} 

== References ==
 
 
 

==External links==
 
* 

 
 
 
 