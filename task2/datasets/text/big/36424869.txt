Piazza Fontana: The Italian Conspiracy
{{Infobox film
 | name =Piazza Fontana: The Italian Conspiracy
 | image = Piazza Fontana- The Italian Conspiracy.jpg
 | caption =
 | director = Marco Tullio Giordana
 | writer = 
 | starring =  Valerio Mastandrea
 | music = Franco Piersanti
 | cinematography =  Roberto Forza
 | editing =  
 | producer = Fabio Massimo Cacciatori (executive producer)
 | distributor =
 | released =  
 | runtime =
 | country = Italy
 | language =  Italian
 | budget =
 }} Italian drama film directed by Marco Tullio Giordana. It is loosely based on the book Il segreto di Piazza Fontana by Paolo Cucchiarelli.   
The film deals with the reconstruction of the Piazza Fontana bombing that took place in Milan December 12, 1969, and of the tragic events that ensued, from the death of Giuseppe Pinelli, which occurred in mysterious circumstances during an interrogation, to the death of the Commissioner Luigi Calabresi, who had led the investigation.  

Romanzo di una strage entered the 2012 Karlovy Vary International Film Festival, in which it won the Special Jury Prize.  It was nominated to 16 David di Donatello awards, and won three (for best supporting actor, best supporting actress and best visual effects).   It also won three Nastro dArgento awards (for best script, best actor and best supporting actress)   and two Ciak doro (for best supporting actor and best score). 

== Cast ==
*  
*  
* Laura Chiatti: Gemma Calabresi
*  
* Michela Cescon: Licia Pinelli
*  
*  
* Luigi Lo Cascio: Judge Paolillo
* Giorgio Tirabassi: the professor 
*  
* Denis Fasolo: Giovanni Ventura
*  
* Gianni Musy: confessor of Moro
* Luca Zingaretti:the doctor
* Francesco Salvi: Cornelio Rolandi
* Corrado Invernizzi: Judge Pietro Calogero

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 


 
 