Aai Thor Tujhe Upkar
 

{{Infobox film
| name           = Aai Thor Tujhe Upkaar - Marathi Movie
| image          = Aai Thor Tuze Upkar.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = Prakash Bhende
| producer       = Uma Prakash Bhende
| screenplay     = 
| story          = 
| starring       = Uma Bhende Ashok Saraf Laxmikant Berde Sukanya Kulkarni Mohan Joshi Avinash Kharshikar
| music          = Nandu Honap
| cinematography = 
| editing        = 
| studio         = Everest Entertainment Pvt. Ltd.
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Marathi
| budget         = 
| gross          = 
}}
Aai Thor Tujhe Upkaar is a Marathi movie, which was released in November 1999. The movie is produced by Uma Prakash Bhende and directed by her husband Prakash Bhende,   the movie shows how a greed ridden son drags his own mother to court, for money. 

== Synopsis ==
Motherhood is the biggest blessing in a womans life, but in todays materialistic world, it’s become a curse for aged women. This is the heart-rending tale of Sharada Inamdar who is coerced to pay a grave price for motherhood.

Sharada the aged mother is seen as an unwanted burden by her son. He overlooks the commitment his mother has put entire life, in bringing him up and she has made so many sacrifices for him that he could never repay even if he wanted to. His lust for money is so grave that he drags his own mother to court. A heartbroken Sharada demands justice, but can a court of law provide it?  

== Cast ==

The movie has a star cast of Uma Bhende, the inseparable duo of Ashok Saraf & Laxmikant Berde, Sukanya Kulkarni, Mohan Joshi as well as Avinash Kharshikar.

==Soundtrack==
The music has been provided by Nandu Honap and the sound tracks are listed below.

===Track listing===
{{Track listing
| title1 = Picnic song | length1 = 3:20
| title2 = Baicha Putlaa | length2 = 4:36
| title3 = Vada Paav Wala | length3 = 4:20
| title4 = Priye Laajwanti | length4 = 5:20
| title5 = Kashi Asel Vahinibai | length5 = 4:16
}}

== References ==
 
 

== External links ==
*  
*  
*  

 
 
 


 