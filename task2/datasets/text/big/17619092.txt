Last Stop on the Night Train
{{Infobox film
| name           = Lultimo treno della notte
| image          = Night Train Murders Sleeve.jpeg
| image_size     =
| caption        = UK DVD Sleeve under the Night Train Murders title
| director       = Aldo Lado
| producer       = Pino Buricchi Paulo Infascelli
| writer         = Roberto Infascelli Renato Izzo Aldo Lado Ettore Sanzò
| narrator       =
| starring       = Flavio Bucci Macha Meril Marina Berti Irene Miracle Gianfranco De Grassi Laura DAngelo
| music          = Ennio Morricone
| cinematography = Gabor Pogany
| editing        = Alberto Galliti
| distributor    = 
| released       =   
| runtime        = 90 minutes
| country        = Italy Italian
| budget         =
| gross          =
}} Italian revenge thriller.  It was directed by Aldo Lado, scored by Ennio Morricone and stars Flavio Bucci, Macha Meril and Irene Miracle.  It gained notoriety when it was banned in the UK as a video nasty in the 1980s.

==Plot==
 
Two young girls, Margaret (Irene Miracle) and Lisa (Laura D’Angelo), are set to take the overnight train from Munich in Germany to stay with Lisas parents in Italy for Christmas. On boarding they find the train is full and are forced to sit in the corridor. Meanwhile, two petty criminals, Blackie (Flavio Bucci) and Curly (Gianfranco De Grassi), also board the train as it is leaving Munich to escape from a pursuing policeman.

Wandering down the train looking for kicks the two thugs come across Margaret and Lisa who help them hide from the ticket collector. Blackie then encounters an upper-class blonde older woman (Macha Méril who is never named) who he makes to molest in the toilets only for her to aggressively seduce him instead. As Curly gets into a fight, the girls become increasingly wary of the two thugs behaviour and make their way further down the train to escape them.

Arriving in Innsbruck in Austria the train is stopped and searched following a tip-off that there was a bomb on board. The girls decide to try and call Lisas parents to tell them of the delay but cant get through. Meanwhile in Italy her parents are preparing for Christmas and hosting a dinner party for their friends where Lisas father (Enrico Maria Salerno), a doctor, decries the growing violence in society.

Back in Innsbruck the girls decide to board a different train which will take them directly through to their destination.  On boarding the new train however they find it is old, run down and virtually empty. Finding a compartment in the last carriage they settle down for their trip, happy to have seats at last, and begin to eat a packed lunch by candlelight.

As the train travels into the night the girls are alarmed to discover that the two thugs and the blonde woman are on board as well and the three soon force their way into the girls compartment. Stealing the food, Blackie and the blonde then engage in various lewd acts while taunting the girls. Curly is acting strangely and disappears to shoot up heroin in the toilets. Returning, he catches Margaret just as she attempts to escape the compartment only for the girl to knee him in the genitals. Strung out and enraged, Curly beats Margaret into submission then, egged on by Blackie and the blonde, forces Lisa to masturbate him. Now completely manipulating the situation, the blonde spots another passenger, a peeping tom who is watching them through the compartment window. Grabbing the man, the two thugs force him to rape Margaret, but when they are distracted by Lisa vomiting he escapes down the train.

Continuing to taunt and abuse the girls the blonde encourages Curly to rape Lisa but he is unable to break her hymen. Although Blackie is now becoming concerned at the way events are heading the blonde holds down Lisa while encouraging Curly to cut her with his flick knife to help him. The blonde enthusiastically grabs the knife and forces it deeper into Lisa causing her to hemorrhage and killing her.

With Lisa now dead and daylight breaking, the previously catatonic Margaret makes a break for it and manages to lock herself in the toilet while the blonde orders the two thugs to bring her back. Now frantic, Margaret climbs out of the window and flings herself from the train in an attempt to escape only to be killed in the fall.

Angry at the turn of events the blonde gets the others to throw Lisas body out of the window followed by their luggage, stealing their tickets and some other items in the process. Angry at what has happened, Blackie blames the blonde and punches and kicks her.

Arriving at the station to meet the girls, Lisas parents are alarmed when they dont arrive at the allotted time. The stationmaster tells them that their train had been delayed, so the doctor and his wife elect to return home and await news of the arrival time of the girls. As they are leaving they come across the blonde and two thugs and Gulio agrees to treat the blondes injuries (received in her fight with Blackie) back at the house.

Waiting for news of the girls the couple feed the two thugs while Gulio treats the blonde. Lisas mother becomes suspicious of her houseguests when she spies Curly is wearing a tie exactly like the one she was told Lisa had bought for her father as a Christmas present. The trio continue to act suspiciously and Gulio elects to take them back to town and goes to fetch the car. While in the car he hears a radio report naming his daughter as a body that had been found by the train track before spying the blonde who is trying to escape having realised they are at the house of one of the girls they had murdered.

Grabbing the blonde, Gulio tries to strangle her but she convinces him that the two thugs had killed the girls and had threatened to do the same to her. Believing the woman, Gulio leaves her with his wife before heading off to find the others. He discovers Curly in his surgery injecting some heroin he had found and grabs the younger man forcing the needle further into his arm, overdosing him. The doctor then repeatedly beats Curly with various furniture and surgical instruments leaving him in a bloody mess on the floor.

Spying Blackie trying to flee, he grabs a shotgun and pursues him through the gardens. Curly manages to crawl into the driveway and tries to grab the blonde only for her to kick him to death. Gulio wounds Blackie in the leg before shooting him at point blank range.  As the police arrive, the blonde womans fate remains uncertain.

==Cast==
*Flavio Bucci as Blackie
*Macha Méril as Lady On The Train
*Gianfranco De Grassi as Curly
*Enrico Maria Salerno as Professor Giulio Stradi
*Irene Miracle as Margaret Hoffenbach
*Laura DAngelo as Lisa Stradi
*Marina Berti as Laura Stradi
*Franco Fabrizi as Perverted Train Passenger
*Dalila Di Lazzaro as Party Guest

==Critical reception== The Last House on the Left seem deserved. Still, it suffers from so many flaws—poor pacing, an anemic screenplay, weak overdubbed dialogue, and a tendency to flinch during the sparse brutality—that I cant recommend the movie on its own merits with any great enthusiasm." 
 Wes Cravens Alan Lados train leaves a station in Verona traveling west at 35 mph, then can a cheap imitation of a film actually be better than the film its ripping off? Well, in this case yes. While The Last House on the Lefts success came largely from its grittiness, Lado proves that a professional sheen can be just as effective, and puts his film on the express track to success." {{cite web | url=http://www.dvdverdict.com/reviews/nighttrainmurders.php | title=Night Train Murders | author=
Paul Corupe | publisher=DVD Verdict | accessdate=2011-04-22}} 

J.C. Maçek III of WorldsGreatestCritic.com commented, "Most viewers would find the disconcerting elements to be too much to take, ignoring the more artistic aspects (which admittedly are minor). In this respect, Lado was completely successful. He strove to make a movie calculated to make audiences walk out and audiences walked out... and then it was banned with the rest of the Video Nasties. Unlike many of the rest of its peers with this dubious distinction, LUltimo Treno Della Notte has still never been released in the UK. In that respect, too, Lado was successful, as his movie is in good company. Guy Fawkes wasnt released in the UK either. And, like Lado, he knew bombs pretty damned well!" 

==Controversy==
The film was rejected by the BBFC when submitted for cinema classification in the UK in 1976.     It was banned as a video nasty in 1983, though it was acquitted and removed from the list in 1984.  It did not get a full release until 2008 when it was finally passed uncut and distributed on DVD by Shameless Screen Entertainment.   

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 