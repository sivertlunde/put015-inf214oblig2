Coming of Age: Adult Bat Mitzvah
Coming of Age: Adult Bat Mitzvah, is a documentary produced in 1989
and directed by Bonnie Burt <ref name= "WOMEN MAKE
MOVIES | Bonnie Burt">{{cite web
  | title = Bonnie Burt
  | publisher = WOMEN MAKE MOVIES 
  | date = March 2005
  | url = http://www.wmm.lfchosting.com/filmcatalog/makers/fm613.shtml
  | accessdate = June 27}} 

==Summary==
Bat mitzvahs are not only for   and what it says about the push for equality of the sexes in synagogue life.

Coming of Age sits down with a group of women, aged 32 to 83, from different walks of life, to find out why, after all these years, they’ve decided to learn Hebrew and study to receive their bat mitzvahs. Sharing their excitement at being included in the less-than-century-old tradition, the women explain what their faith means to them.

Rabbis began to experiment with the idea of a bat mitzvah in the 1920s, but for decades it was still generally believed that men ran a synagogue — and a woman had no place on the bimah  . It’s only with the recent feminist movement that girls have been invited to study Jewish texts and, in some sects, are encouraged to be active members of the synagogue.

Interestingly, as they prepare to be welcomed into their faith, many of the women interviewed reflect on their relationships with their fathers. Their bonds with the important male figures in their lives seem to represent many of these women’s connection to what has traditionally been a male-dominated faith.

In fact, many of the women have faced great perils that make their bat mitzvahs all the more meaningful and achievement. One woman says she’s grateful to be alive after her dramatic recovery from open-heart surgery, and with the bat mitzvah ceremony is celebrating her second chance at life. Another woman came from a family of seven sisters and was one of the few to survive the Holocaust. After making it through these trying experiences that tested their faith, these women value Judaism in a way they never could have at the age of twelve or thirteen.

Ultimately, the adult bat mitzvah is still a coming-of-age ceremony, but, at an older age, these
women are coming at the milestone from a more self-aware and appreciative perspective.

==Notes==
 

==References==

*{{cite web
  | title = Coming of Age - Bonnie Burt Productions 
  | publisher = Bonnie Burt Productions 
  | date = June 2008
  | url =http://www.bonnieburt.com/movies/coming_of_age.html
  | accessdate = July 1  }}

 
 
 
 
 
 

 