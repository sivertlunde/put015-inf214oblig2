Black Tight Killers
{{Infobox film
| name = Black Tight Killers
| image = Black Tight Killers.jpg
| image_size =
| caption = Theatrical poster for the U.S. release of Black Tight Killers (1966)
| director = Yasuharu Hasebe   
| producer =
| writer = Ryūzō Nakanishi Michio Tsuzuki
| narrator =
| starring = Akira Kobayashi Chieko Matsubara
| music = Naozumi Yamamoto
| cinematography = Kazue Nagatsuka
| editing = Akira Suzuki
| distributor = Nikkatsu
| released = February 12, 1966
| runtime = 86 minutes
| country = Japan
| language = Japanese
| budget =
| gross =
| preceded_by =
| followed_by =
}}
 1966 Japanese film directed by Yasuharu Hasebe and based on the novel   by Michio Tsuzuki.   

==Synopsis==
Daisuke Honda, a war photographer in Vietnam, meets Yuriko Sawanouchi, a stewardess on his plane back to Japan. After drinking with her at a Tokyo bar, he becomes involved in saving Yuriko from assassination by stylish, female ninjas. When trying to rescue Yuriko from kidnappers, Daisuke discovers a group of foreigners are hunting for a WWII-era treasure hidden on an island by Yurikos father.   

==Cast==
* Akira Kobayashi as Daisuke Honda 
* Chieko Matsubara as Yuriko Sawanouchi
* Mieko Nishio as Fuyuko
* Kozue Kamo as Yoshie
* Satoko Hamagawa as Natsuko
* Akemi Kita as Akiko
* Keisuke Noro as Man A
* Shuntarō Tamamura as Man 1

==Critical appraisal==
Jonathan Crow of Allmovie notes that the influence of Hasebes mentor Seijun Suzuki can be seen in Black Tight Killers. Like Suzuki, he uses the tropes of the gangster genre to create "a pop-art dreamscape" with "tail fins, flawless fashion, sudden and unexpected go-go dancing, cool jazz, and freakish violence". Hasebes quirky use of gaudy color is singled out for comment in the review, which judges the film to be "wild, decadent fun". 

In his survey of the pink film genre, Steve Fentone sums up Black Tight Killers with, "Chix with guns. What more do ya need?"    Jasper Sharp writes that the plot is not especially impressive, but of Hasebes visuals, he comments, "there is not a single individual sequence here that fails to deliver enough great dollops of saccharine-coated eye candy to satisfy even the most jaded visual gourmand". He concludes, "this simply magical film is a hoot from start to finish". 

==Availability==
Black Tight Killers was released theatrically in Japan on February 12, 1966.  It was released in the U.S. during this era.   It was released in DVD format in Japan in 2005.  Image Entertainment released the film on DVD in the United States. This DVD has the burned-in subtitles which were part of the films original U.S. release.   

==Bibliography==

===English===
*  
*  
*  
*  

===Japanese===
*  
*  
*  
*  

==Notes==
 

 

 
 
 
 
 
 
 