Black and White (2008 Italian film)
{{Infobox film
| name           = Black and White
| image          = Bianco e nero.jpg
| caption        = Theatrical release poster
| director       = Cristina Comencini
| producer       = 
| writer         = Cristina Comencini Giulia Calenda
| narrator       = 
| starring       = 
| music          =
| cinematography =Fabio Cianchetti 	
| editing        =Cecilia Zanuso 	
| distributor    = 
| released       =  
| runtime        = 
| country        = Italy Italian
| budget         = 
}}
Black and White ( ) is a 2008 Italian romantic comedy-drama film directed by Cristina Comencini. The film deals with race and inter-racial relationships when the married Carlo (Fabio Volo) falls in love with Nadine, the wife of his own wifes work colleague.

==Cast==
* Fabio Volo as Carlo 
* Ambra Angiolini as Elena 
* Aïssa Maïga as Nadine 
* Eriq Ebouaney as Bertrand 
* Katia Ricciarelli as Olga 
* Anna Bonaiuto as Adua 
* Franco Branciaroli as Alfonso 
* Teresa Saponangelo as Esmeralda 
* Billo Thiernothian as Ahamdou 
* Awa Ly as Veronique 
* Bob Messini as Dante

== See also ==
* Movies about immigration to Italy

== External links ==
*  

 
 
 
 
 
 
 
 


 
 