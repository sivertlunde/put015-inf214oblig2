Scrooge, or, Marley's Ghost
{{Infobox film
| name           = Scrooge, or, Marleys Ghost
| image          = Scrooge or Marleys Ghost (1901) - yt.webm
| image_size     = 
| caption        = 
| director       = Walter R. Booth
| producer       = Robert W. Paul
| based on       =  
| writer         = Charles Dickens J.C. Buckstone
| narrator       =
| starring       =
| music          =
| cinematography =
| editing        =
| studio         = Pauls Animatograph Works
| distributor    =
| released       =  
| runtime        = 6 mins 20 secs
| country        = United Kingdom Silent
| budget         =
}}
 1901 UK|British short  silent drama film, directed by Walter R. Booth, featuring the miserly Ebenezer Scrooge confronted by Marleys ghost and given visions of Christmas Past, Present and Future, which is the oldest known film adaptation of Charles Dickens 1843 novel A Christmas Carol. The film,  "although somewhat flat and stage-bound to modern eyes,"  according to Michael Brooke of BFI Screenonline, "was an ambitious undertaking at the time," as, "not only did it attempt to tell an 80 page story in five minutes, but it featured impressive trick effects, superimposing Marleys face over the door knocker and the scenes from his youth over a black curtain in Scrooges bedroom."   

==The film==
Filmed in 35mm and in black and white, only 620 feet of this   website  It was presented in Twelve Tableaux or scenes.  The film contains the first use of intertitles in a film. 

Evidence suggests that Pauls version of A Christmas Carol was based as much on J. C. Buckstones popular stage adaptation Scrooge as on Dickens original story. Like the play, the film dispenses with the different ghosts that visit Scrooge, instead relying upon the figure of Jacob Marley, draped in a white sheet, to point out the error of Ebenezer Scrooge|Scrooges ways. The film featured impressive trick effects by 1901 standards, superimposing Marleys face over the door knocker, and displaying the scenes from his youth on a black curtain in Scrooges bedroom. R. W. Paul was a trick film specialist; Walter Booth, credited as the films director, was a well-known magician as well as a trick and comic film specialist.   The film makes early use of dissolving between scenes. Some scenes are tinted.
 Edward VII Queen Alexandra at Sandringham House in December 1901 in a Royal Command Performance. 

==Content==

The only known surviving footage, about 4 minutes and 55 seconds in length, is preserved by the   
The actor who played Scrooge in the film has not yet been identified.

==See also==
* The Death of Poor Joe the oldest surviving film featuring a Charles Dickens character. (Joe in Bleak House)
*List of ghost films
*Adaptations of A Christmas Carol

==References==
 

==External links==
* 
*  
* 

 

 
 
 
 
 
 
 
 
 