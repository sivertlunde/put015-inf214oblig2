Forbidden Adventure
{{Infobox film
| name           = Forbidden Adventure
| image          = 
| image_size     =
| caption        = 
| director       = Norman Taurog
| producer       = 
| writer         = Edward E. Paramore Joseph L. Mankiewicz Agnes Brand Leahy (scenario)
| based on       =  
| narrator       =
| starring       = Mitzi Green Edna May Oliver Louise Fazenda Jackie Searl Bruce Line
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 77 minutes
| country        = United States English
| budget         = $270,000 
| gross          = 
}}
Forbidden Adventure, also known as Newly Rich, is a 1931 film directed by Norman Taurog and starring Mitzi Green, Edna May Oliver, Louise Fazenda, Jackie Searl and Bruce Line. Three children - two actors and a king - run away from their constricted lives and have a forbidden adventure.

Filming took place from 2 April to 7 May 1931.

==Cast==
* Mitzi Green as Daisy Tate
* Edna May Oliver as Bessie Tate
* Louise Fazenda as Maggie Tiffany
* Jackie Searl as Tiny Tim
* Bruce Line as King Maximilian
* Virginia Hammond as Queen Sedonia
* Lawrence Grant as Equerry
* Dell Henderson as Director

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 


 