Perez.
{{Infobox film
| name     = Perez.
| image    = Perez poster 14.jpg
| director = Edoardo De Angelis
| producer = 
| writer =  Edoardo De Angelis  Filippo Gravino
| starring =Luca Zingaretti
| cinematography =  Ferran Paredes
| music = 
| country =  Italian 
| released =  
}}
Perez. is a 2014 Italian neo-noir film written and directed by Edoardo De Angelis. It was screened in the Horizons section at the 71st Venice International Film Festival.   

== Plot ==
Demetrio Perez is a famous criminal lawyer from Naples who years ago was considered one of the best in his field, but after having had made too many enemies  is now reduced to make the public defender. His life begins to fall apart when his daughter Tea falls madly in love with Francesco Corvino, the son of a boss of the Camorra. 

Luca Buglione, the boss of a rival clan, decides to become a pentito. He makes a deal with Perez: if the lawyer will retrieve for him a batch of smuggled diamonds, then he will testify against Francesco.

== Cast ==

* Luca Zingaretti as Demetrio Perez
*Marco DAmore as Francesco Corvino
* Simona Tabasco as  Tea Perez
* Massimiliano Gallo as  Luca Buglione
* Giampaolo Fabrizio as Ignazio Merolla
* Ivan Castiglione as  Walter

== References ==  
 

== External links ==
* 

 
 
  
 
 
 

 
 