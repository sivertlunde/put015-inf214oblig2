Hud (film)
 
 
{{Infobox film
| name = Hud 
| image = Hud_moviep.jpg
| image_size=
| caption = Theatrical release poster
| screenplay = Irving Ravetch  Harriet Frank, Jr.
| based on = Horseman, Pass By   by Larry McMurtry
| starring = Paul Newman Melvyn Douglas Patricia Neal Brandon deWilde
| music = Elmer Bernstein 
| cinematography = James Wong Howe
| editing = Frank Bracht
| director = Martin Ritt 
| producer = Irving Ravetch Martin Ritt
| distributor = Paramount Pictures
| released =  
| runtime = 112 minutes
| country = United States
| language = English
| budget = $2.35 million The Numbers. Retrieved September 5, 2013. 
}} Western film Larry McMurtrys 1961 novel, Horseman, Pass By. The films title character, Hud Bannon, was a minor character in the original screenplay but was reworked as the lead role. With its main character an antihero, Hud was later described as a revisionist Western.

The film centers on the ongoing conflict between principled patriarch Homer Bannon and his unscrupulous and arrogant son, Hud, during an outbreak of foot-and-mouth disease putting the familys cattle ranch at risk. Lonnie, Homers grandson and Huds nephew, is caught in the conflict and forced to choose which character to follow.
 Best Actress, Best Supporting Academy Award for Best Black and White Cinematography. Howes use of contrast to create space and his selection of black-and-white was favored by critics. In later reviews, the film received additional praise.

==Plot==
Hud Bannon (Paul Newman) is ambitious and self-centered, the opposite of his deeply principled rancher father Homer (Melvyn Douglas). Also living on the Bannon ranch is Huds teenage nephew, Lonnie (Brandon deWilde), who looks up to both men but is most impressed by Hud.  Lonnie and Hud are attracted to the Bannons middle-aged housekeeper, Alma (Patricia Neal). Although she is attracted to Hud, Alma keeps her distance because she has been mistreated in the past by men like him.
 
 
After the sudden, inexplicable death of a cow on the ranch, Homer sends Lonnie to town to bring Hud to the ranch for his opinion. Hud is annoyed by his fathers decision to summon the state veterinarian, and suggests selling the animals to other ranchers before the news spreads; otherwise, government agents will kill all the cattle and destroy everything they have worked for. He blames his father for not realizing that the cheap Mexican cattle were sick before he bought them. Adhering to his principles, Homer ignores Huds idea and waits for the veterinarian. Upon his arrival, the state veterinarian immediately issues a legally binding State Livestock Transfer Order directing the quarantine of the ranch for a possible foot-and-mouth disease outbreak. This freezes the movement of all livestock to or from the Bannon ranch, while they await the test results.
Aware of the possibility of bankruptcy to the ranch, Homer nevertheless complies. 

One night, Hud takes Lonnie out and they prevail in a drunken barroom brawl. Back at the ranch he reflects on the past (when he and Lonnies father did the same thing), revealing his feelings about his brother Normans death and his fathers coldness to him. When they enter the house Homer confronts Hud, accusing his son of trying to corrupt Lonnie. They argue, with Hud accusing Homer of hypocrisy and resentment of him for Normans death. Homer replies that his disappointment in Hud began before the accident; he cares about no one but himself, and is "not fit to live with". Hurt and angry, Hud retorts "My mama loved me, but she died" as he walks away. When Lonnie tells Homer that he was too harsh and other people act like him, Homer replies that one day he will have to decide for himself what is right and wrong.

 
After learning from Lonnie that Hud is trying to seize the ranch, Homer confronts Hud.  Infuriated by his eroded inheritance, Hud threatens to have Homer declared legally incompetent so he can take over the ranch.  Homer tells his son he will lose. He admits that he made mistakes raising Hud, and was too hard on him. When Hud accuses him of having a "shape up or ship out" policy, Homer wonders aloud how a man like Hud can be his son and storms off to his room. Hud, drunk, goes outside and tries to rape Alma before Lonnie comes to her aid. 

When the herd tests positive for foot-and-mouth disease, the veterinarian orders them to be killed and buried on the ranch under state supervision to keep the disease from spreading.  Hud points out that they could sell some oil leases to keep the ranch profitable, but Homer refuses as he only has pride in cattle, despite his ruinous decision to purchase the Mexican cattle.

Alma decides to leave the ranch. After Lonnie drops her off at the bus station, Hud sees her as she is waiting. He apologizes for his drunken assault but not his attraction to her, and he would remember her as "the one who got away". Driving back to the ranch, Lonnie sees his grandfather lying on the side of the road after a fall from his horse during a survey of the ranch. Hud pulls up behind Lonnie and, despite their efforts, he dies.

Lonnie is repelled by his uncles treatment of Homer and Alma and leaves the ranch after his grandfathers funeral, uncertain if he will ever return. When he tells Hud to put his half of their inheritance in the bank, his uncle replies that Lonnie now sees him as Homer saw his son. Hud goes back into the Bannon house alone; as he closes the door, the final fade-out is the window shades pull-ring swaying.

==Cast== Texas accent he was coached by Bob Hinkle, who coached James Dean for his role as Jett Rink in Giant (1956 film)|Giant. 
*Melvyn Douglas as Homer Bannon: Huds father, Lonnies grandfather and owner of the Bannon ranch.  Although Paramount was doubtful about casting him due to his heart condition, Martin Ritt insisted that he was the right actor for the role. 
*Brandon De Wilde as Lonnie Bannon, Huds teenage nephew who idolizes him. De Wilde, a former child actor, was best known at the time for his award-winning role in Shane (film)|Shane.  The Untouchables episode, "The Maggie Storm Story". The actress signed for $30,000;  although she had third billing and 25 minutes of screen time, the film had a major impact on her career. 

==Production==
After working together on other projects, director Martin Ritt and Paul Newman co-founded Salem Productions and the company made a three-film deal with Paramount Studios.  For its first film Salem hired husband-and-wife scriptwriters Irving Ravetch and Harriet Frank, Jr., who worked with Ritt and Newman on The Long, Hot Summer. Ravetch found Larry McMurtrys novel, Horseman, Pass By, in an airport shop during a Dallas stopover and presented the project to Ritt and Newman after reading a description of Hud Bannon. The partners met Ravetch and Frank at their home, approved the project  and the writers adapted the script. 

Although McMurtrys novel focuses on Lonnie Bannon, Ravetch and Frank expanded Huds character to the lead role.  Ritt wanted Hud to be an antihero who did not regret his actions at the end of the film.  He was changed from Homers son-in-law to his son, and the character of Homers wife was eliminated.  Newman and Ritt initially named the project Wild Desire, followed by The Winners, Hud Bannon Against the World, Hud Bannon and finally Hud.  Ravetch and Frank accompanied Ritt and Newman through pre-production, casting and publicity design. 

 
Ritt asked that the housekeeper character (originally Halmea, a black woman) be renamed Alma and played by a white actress, because he thought a relationship between Hud and a black woman would not work.  According to Ravetch and Frank, "Neither American film nor American society was quite ready for that back then". Although Halmea is raped by Hud in the novel, Ravetch and Frank added Lonnies intervention to "highlight" his significance and keep Hud "human" and not "totally and simplistically evil".  To accentuate the scenes violence, Huds roughness was complemented by the use of shadows,  while a camera was attached on Newmans back to create a "mans view angle" while he chased Neal.  Film critic Pauline Kael described Neals performance as "perhaps the first female equivalent of the white-negro". 

Cinematographer James Wong Howe shot Hud in black-and-white to "elevate its dramatic propensities".  Filmed in Panavision,  Howe used high contrast with unbalanced light and dark tones. He highlighted the white ground and clear skies, making the shadows black. Dark tones were "overpowered" by light ones, creating a sense of "infinite space". For faces and structures, Howe used light reflected from the ground. The contrast between the environment and objects silhouetted against the background provides a sense of depth.  Ritts biographer, Carlton Jackson, wrote that in Hud "the scenery becomes a part of the thematic development itself".  According to Texas Monthly, "Howes austere rendition of Texas landscapes&nbsp;  remains one of the films most distinctive pleasures". 
 Claude as Humane Society was present to monitor the animals treatment. The herd was sprayed with a substance to make it appear ill, and bungee cords were tied to the cows legs.   Camera angles were arranged by Ritt and Howe to avoid showing the death of the cattle.  When a man was shown shooting, the camera would switch to the cattle; the crew shook the cords, creating an effect of the herd being shot.  During location shooting, Newman and De Wilde often changed hotel rooms due to female fans following them. 

Elmer Bernstein used sparse arrangements for Hud s score;  in its theme, Bernstein "insinuated" natural sounds with "poignant strings on the guitar".   Variety (magazine)|Variety called the theme "vital and noteworthy", "sombre, plaintive and foreboding". 

Hud s budget was $2.35&nbsp;million, and Paramount executives were unhappy with the film. They felt it was too dark and were displeased by the black-and-white cinematography and Huds lack of remorse and unchanged behavior.  Although Martin Rackin asked Ritt to change the films ending, he and Newman decided to keep the original.  After Hud was previewed Paramount considered dropping the project, feeling that it was not "commercial enough", but Ritt flew to New York and convinced the executives to release the film unmodified. 
Advertising posters, with Newman in blue jeans in a "suggestive, full-length pose", read: "Paul Newman is "Hud"!&nbsp;... the man with the barbed-wire soul". 

==Release and reception== theatrical rentals.  It was the 1963 in film|19th-highest-grossing film of the year. Life (magazine)|Life called Hud an "arresting almost great movie", describing Paul Newmans acting as "faultless".  An Outlook reviewer wrote that the four main cast members acted "splendidly"; Newman "speaks at times with an unpleasant nasal twang, but is clearly suited to the part." They described Melvyn Douglas performance as "impeccable", Brandon de Wildes as "  in looking earnest unsure of himself" and praised Patricia Neals expressiveness.  Time (magazine)|Time called the performances "splendid", and Howes photography "brings the Texas panhandle to dusty, sweaty life."  The New York Times, in a favorable review, said Ritts direction had "  powerfully realistic style" and called Ravetch and Franks work "  excellent screenplay." The newspaper called Newmans acting "tremendous", Douglas "magnificent", de Wildes "eloquent of clean, modern youth" and Patricia Neals "brilliant." The review also praised James Wong Howes "excellent" camera work and Elmer Bernsteins "poignant" score.  Variety (magazine)|Variety called Hud "a near miss"; its screenplay fails to "filter its meaning and theme lucidly through its characters and story", although it called the four leads performances "excellent." 

  Saturday Review called him a  "charming, raffish monster".  According to Outlook, "Hud Bannon is a mean, unscrupulous man who never has even a momentary twinge of conscience or change of heart"; in the end scene, Hud " ulls down the shade on the world of goodness and decency".  Hud was originally described by critic Pauline Kael as an "anti-Western"; she called it an "Anti-american film", which was "so astutely made and yet such a mess that it (was) redeemed by its fundamental dishonesty." 

Paul Newman said, "We thought   last thing people would do was accept Hud as a heroic character&nbsp;... His amorality just went over   head; all they saw was this western, heroic individual". Martin Ritt later attributed audience interpretation of the character to the counterculture of the 1960s which "changed the values" of the young audiences who saw Hud as a hero. 

===Later evaluation in film guides===
Leonard Maltins Movie Guide gave Hud four stars out of four. Maltin called the story "excellent" and its performances "impeccable".   Steven H. Scheuers Movies on TV also gave the film four stars out of four; Scheuer called it "a must for movie-drama fans", and said the cast was "superb".  In Film and Video Guide Leslie Halliwell gave Hud four stars out of four, calling it "unique".  Allmovie gave Hud five stars out of five, calling the film "a warning shot for the Sixties" and saying that its "generational conflict would prove prescient". It praised Howes cinematography, which gave the film "an authentic Western feel". 

===Awards and nominations=== 36th Annual Best Actress Best Supporting Best Cinematography BAFTA Award for Best Foreign Actress.  The film was nominated for five Golden Globe Awards, won four Laurel Awards (Top Drama, Top Male Dramatic Performance, Top Female Dramatic Performance and Top Male Supporting Performance) and received the Best Written American Drama Writers Guild of America Award. 

{| class="wikitable collapsible sortable state = collapsed" border="1" style="font-size: 90%;"
|-
! Group !! Award !! Recipient !! Result
|- 36th Academy Academy Awards|| Academy Award for Best Actress || Patricia Neal ||  
|-
| Academy Award for Best Supporting Actor || Melvyn Douglas ||  
|-
| Academy Award for Best Cinematography| Academy Award for Best Cinematography, Black-and-White || James Wong Howe ||  
|-
| Academy Award for Best Director || Martin Ritt ||  
|-
| Academy Award for Best Actor || Paul Newman ||  
|-
| Academy Award for Best Adapted Screenplay || Irving Ravetch Harriet Frank, Jr. ||  
|-
| Academy Award for Best Art Direction || Hal Pereira Tambi Larsen  Samuel M. Comer  Robert R. Benton ||  
|- 21st Golden Golden Globe Awards || Golden Globe Award for Best Motion Picture - Drama || Irving Ravetch Martin Ritt ||  
|- Golden Globe Award for Best Director || Martin Ritt ||  
|- Golden Globe Award for Best Actor - Motion Picture Drama || Paul Newman ||  
|- Golden Globe Award for Best Supporting Actress - Motion Picture || Patricia Neal ||  
|- Golden Globe Award for Best Supporting Actor - Motion Picture || Melvyn Douglas ||  
|- 17th British BAFTA Awards BAFTA Award for Best Foreign Actress || Patricia Neal ||  
|- BAFTA Award for Best Film || Martin Ritt ||  
|- BAFTA Award BAFTA Award for Best Foreign Actor || Paul Newman ||  
|- American Cinema Editors|| Best Edited Feature Film || Frank Bracht ||  
|- Directors Guild Outstanding Directorial Achievement in Motion Pictures || Martin Ritt ||  
|-
|rowspan="4"| Laurel Awards || Top Drama ||  ||  
|- Top Male Dramatic Performance || Paul Newman ||  
|- Top Female Dramatic Performance || Patricia Neal ||  
|- Top Male Supporting Performance || Melvyn Douglas ||  
|- National Board National Board of Review || National Board of Review Award for Best Actress || Patricia Neal ||  
|- National Board of Review Award for Best Supporting Actor || Melvyn Douglas ||  
|- National Board National Board of Review Top Ten Film || ||  
|- 1963 New New York Film Critics Circle Awards || New York Film Critics Circle Award for Best Actress || Patricia Neal ||  
|-
| New York Film Critics Circle Award for Best Screenplay || Irving Ravetch Harriet Frank Jr. ||  
|- New York Film Critics Circle Award for Best Film || ||  
|- New York Film Critics Circle Award for Best Director || Martin Ritt ||  
|- New York Film Critics Circle Award for Best Actor || Paul Newman ||  
|- 24th Venice Venice Film OCIC Award || Martin Ritt ||  
|- 24th Venice Golden Lion || Martin Ritt ||  
|- Writers Guild Best Written American Drama || Irving Ravetch Harriet Frank Jr. ||  
|-
|}

==Footnotes==
 

==References==
 
*  
* 
*  
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
*  
*  
* 
* 
*  
* 
* 
* 
* 
* 
* 
 

==External links==
 
 
* 
* 

 
 
 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 