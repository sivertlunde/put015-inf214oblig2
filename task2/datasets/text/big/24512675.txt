A Summer Evening with Floating di Morel
{{Infobox film
| name           = A Summer Evening with Floating di Morel
| image          =
| image_size     = 
| caption        = 
| director       = Dietmar Post and Lucia Palacios
| producer       = Play Loud! Productions
| writer         = 
| narrator       = 
| starring       = Sabine Blödorn, Kai Drewitz, Thorsten Neu
| music          = 
| cinematography = 
| editing        = Karl-W. Huelsenbeck
| distributor    = Play Loud! Productions 2009
| runtime        = 32
| country        = United States, Germany, Spain English
| budget         = 
| gross          = 
}}
A Summer Evening with Floating di Morel is the title of the first film within the "play loud! (live) music series". It features the Berlin-based rock n roll band Floating di Morel. The band performs 7 songs at their home studio. Filmmakers Dietmar Post and Lucia Palacios follow the group through one "summer evening" in the style of Direct Cinema. 

== Song List ==
# looking back
# 52 52
# ois
# filter the green
# g.c. 55 or the idea of the north
# marlen
# slumtary gibly

All songs appear in completely different studio versions on the 2009 Floating di Morel album "Said My Say" (pl-06)

== References ==
 

== External links ==
* 
* 
* 

 
 
 
 
 


 