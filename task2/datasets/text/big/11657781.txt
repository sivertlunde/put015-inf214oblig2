How to Behave
{{Infobox Film
| name           = How to Behave
| image          = 
| image_size     = 
| caption        = 
| director       = Arthur Ripley
| producer       = 
| writer         = Robert Lees Frederic I. Rinaldo Robert Benchley
| narrator       = Robert Benchley
| starring       = Robert Benchley
| music          = 
| cinematography = 
| editing        = 
| distributor    = Metro-Goldwyn-Mayer
| released       = 1936
| runtime        = 10 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
| preceded_by    = 
| followed_by    = 
}}
How to Behave is a short film released through MGM Studios starring Robert Benchley.  The short, released in 1936, spoofs the social etiquette films and norms of the time.

==References==
* Billy Altman, Laughters Gentle Soul: The Life of Robert Benchley. (New York City: W. W. Norton, 1997. ISBN 0-393-03833-5).

==External links==
* 

 

 
 
 
 
 
 
 
 