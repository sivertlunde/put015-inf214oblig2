Dorothy Meets Ozma of Oz
 
{{Infobox film
| name           = Dorothy Meets Ozma of Oz
| image          = 
| image_size     =
| alt            =
| caption        =  Lisa Wilson Thomas A. Bliss (live action)
| producer       = Thomas A. Bliss Diana Dru Botsford
| writer         = Jim Carlson Terrence McDonnell L. Frank Baum (book)
| narrator       =
| starring       = 
| music          = 
| cinematography = 
| editing        =
| studio         = The Kushner-Locke Company Akom Production Company Prairie Rose Productions
| distributor    = Lorimar Home Video
| released       =  
| runtime        = 28 minutes
| country        = USA
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}
 Michael Gross of Family Ties. It is based on Ozma of Oz by L. Frank Baum.

==Plot== Toto are swept overboard in a storm. Dorothy manages to climb in with a hen who had also fallen into the sea.

Next morning, Dorothy finds the hen, whose name is Billina, had laid an egg and can talk. Upon this discovery, Dorothy finds that they are in the Land of Oz. Finding Totos tracks in the sand, they follow them to a place in the forest where they find a warning signed in the sand: "Beware the Wheelers". Upon stopping again, they decide to have breakfast. Dorothy finds that the tree she is sitting under grows lunchboxes.

Upon finishing breakfast, they are chased by two Wheelers. They reach a rocky hill, and climb up it knowing the Wheelers cant follow them. One of the Wheelers threatens to tear them into little pieces for stealing "their" lunchboxes. Dorothy and Billina agree that they could be lying. Suddenly, the Tin Woodman, the Scarecrow (Oz)|Scarecrow, and the Cowardly Lion arrive, followed by Tik-Tok (Oz)|Tik-Tok and the Hungry Tiger.
 Ev and her ten children from the evil Nome King and his Magic Belt.

Suddenly the travelers come upon a metallic giant with a hammer. They manage to pass under the giant and are eventually greeted by the Nome Kings Rock Fairies. Upon hearing Dorothys plea, the Nome King allows them to enter. Ozma introduces herself and demands the Nome King release the Queen of Ev and the ten children. The Nome King explains that he changed them into ornaments and set them in his treasure room. He then offers them a chance to rescue the Queen and her children. They had to touch an ornament to which they believed were the Queen or her children; if they were right, the Queen and her children would be restored – but if they chose wrong, they would be transformed into ornaments themselves. As the Nome King takes the travellers into his treasure room, Billina stays behind under the throne.

The travellers decide to split up and search. Meanwhile, Billina overhears the Nome King and his minions say that the Queen of Ev and her children were the royal purple ornaments. She manages to sneak away and find Dorothy, while the Hungry Tiger, Tik-Tok, the Cowardly Lion, the Scarecrow, the Tin Woodman and Princess Ozma are changed into ornaments themselves. Fortunately, Dorothy and Billina find the royal purple ornaments and restore the Queen of Ev and her children and return to the Nome King.

The Nome King is surprised that Dorothy managed to restore the Queen and her children, and even more so when he discovers that Billina told him. At once, the Nome King and his minions become frightened, and even more so when Billina lays an egg, saying that its poisonous to them. Dorothy threatens to splatter the egg on the Nome King, who tries to get away. She then throws the egg at him, which splatters against his head. While wiping the egg off, Dorothy manages to grab the Kings Magic Belt which restores her friends and even Toto back to normal.

United with Toto, Dorothy asks to be sent back to her Aunt and Uncle. Billina gives Dorothy one of her eggs to remember her by as Ozma puts on the Magic Belt and sends Dorothy and Toto back to Aunt Em and Uncle Henry. She finds herself on a bed, at first wondering if it was a dream but finds Billinas egg. Upon this discovery, Dorothy hopes that someday she and Toto can find their way back to the Land of Oz again.

==Cast== Michael Gross — Himself/Host
*Janice Kawaye - Dorothy Gale (credited as Hiromi Kawaye) Matthew Stone Scarecrow
*Sandra J. Butcher
*Nancy Chance Jay David
*Fredie Smootie

==External links==
* 

 

 
 
 
 
 