From Saturday to Sunday
{{Infobox film
| name           = From Saturday to Sunday
| image          = 
| caption        = 
| director       = Gustav Machatý
| producer       = František Horký
| writer         = Vítězslav Nezval Gustav Machatý
| starring       = Ladislav H. Struna
| music          = 
| cinematography = Václav Vích
| editing        = Gustav Machatý
| distributor    = 
| released       =  
| runtime        = 69 minutes
| country        = Czechoslovakia
| language       = Czech
| budget         = 
}}
From Saturday to Sunday ( ) is a 1931 film directed by Gustav Machatý and starring Ladislav H. Struna.    

==Cast==
* Ladislav H. Struna - Karel Benda, typesetter
* Magda Maderova - Mána, audio typist
* Jiřina Šejbalová - Nany, audio typist
* Karel Jičínský - Mr. Ervín
* R. A. Dvorský - Pavel
* F. X. Mlejnek - Announcer
* Mimi Erbenová - Attendant at nightclub
* Jan Richter - Drunk at café
* Frantisek Sauer - Drunk at café
* Míla Svoboda - Drunk at café
* Václav Menger - Commissar
* Leo Marten - Dancer at nightclub
* Asa Vasátková - Dancer at nightclub
* Milada Matysova - Bar Dancer

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 