Rachel and the Stranger
{{Infobox film
| name           = Rachel and the Stranger
| image          = Rats1948.jpg
| producer       = Richard H. Berger Jack J. Gross Norman Foster
| writer         = Howard Fast (story) Waldo Salt
| starring       = Loretta Young William Holden Robert Mitchum
| music          = Roy Webb
| cinematography = Maury Gertsman
| editing        = Les Millbrook RKO
| released       =   |ref2= }}
| runtime        = 80 minutes
| country        = United States English
}}
 1948 Western western film Norman Foster-directed film was one of the few to address the role of women in the pioneer west, as well as portray early Americas indentured servant trade. It was based on the Howard Fast short story "Rachel".

While the film had a low budget, it was RKO Radio Pictures|RKOs most successful film that year, making $395,000.

==Plot== Gary Gray) needs a woman around to help raise him. He goes to the nearest settlement and consults Parson Jackson (Tom Tully). David gets talked into buying the contract of an indentured servant named Rachel (Loretta Young) and marrying her.

Their marriage, however, is in name alone. Rachel serves more as a servant than a wife and Davey resents what he sees as an attempt to replace his dead mother Susan. Jim Fairways (Robert Mitchum), a family friend (and former suitor of Susans), visits and falls in love with Rachel. When he offers to buy her, David must fight to keep her and discovers his love in the process.

==Cast==
* Loretta Young as Rachel Harvey
* William Holden as David Harvey
* Robert Mitchum as Jim Fairways Gary Gray as Davey
* Tom Tully as Parson Jackson
* Sara Haden as Mrs. Jackson
* Frank Ferguson as Mr. Green
* Walter Baldwin as Gallus
* Regina Wallace as Mrs. Green

==Reception==
The film recorded a profit of $395,000. Richard Jewell & Vernon Harbin, The RKO Story. New Rochelle, New York: Arlington House, 1982. p. 231 

After Mitchum was arrested for possessing marijuana, RKO rushed to release the film to take advantage of the news of Mitchums arrest. 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 


 