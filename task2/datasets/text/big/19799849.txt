1-2-3 Go
{{Infobox film
| name           = 1-2-3 Go
| image          =
| caption        = Edward Cahn
| producer       = Metro-Goldwyn-Mayer
| writer         = Hal Law Robert A. McGowan
| narrator       =
| starring       =
| music          =
| cinematography = Jackson Rose
| editing        = Leon Borgeau
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 10 21"
| country        = United States
| language       = English
| budget         =
}} Edward Cahn.  It was the 199th Our Gang short (200th episode, 111th talking short, 112th talking episode, and 31st MGM produced episode) that was released.

==Plot==
While playing baseball on a busy street in Greenpoint, Mickey is struck by a car. Though he fully recovers from his injuries, Mickey meets several other kids in the hospital who werent so lucky. Instantly developing a sense of civic responsibility, the Gang members establish the "1-2-3 Go Safety Society," dedicated to lowering the number of auto injuries in their community.   

==Cast== Mickey Gubitosi (later known as Robert Blake) as Mickey
* Billy Laughlin as Froggy
* George McFarland as Spanky
* Billie Thomas as Buckwheat

===Additional cast===
* Vincent Graeff as Kid umpire
* Giovanna Gubitosi as Kid eating ice cream
* James Gubitosi as Kid giving up home run
* Freddie Walburn as Kid retrieving home run from street Barbara Bedford as Ann, a nurse
* Margaret Bert as Nurse at reception desk
* John Dilson as Mayor Of Greenpoint
* Arthur Hoyt as Horace
* May McAvoy as Miss Jones, a nurse
* Anne ONeil as Horaces wife
* William Tannen as Cab driver
* Joe Young as Man calling for ambulance

==See also==
* Our Gang filmography

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 


 