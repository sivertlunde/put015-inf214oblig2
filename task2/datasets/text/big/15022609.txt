Headin' South
 
{{Infobox film
| name           = Headin South
| image          = Headin-south1918newspaperad.jpg
| caption        = Newspaper advertisement.
| director       = Allan Dwan Arthur Rosson
| producer       = Douglas Fairbanks
| writer         = Allan Dwan
| starring       = Douglas Fairbanks
| cinematography = Connie De Roo Glen MacWilliams Hugh McClung Len Powers Harris Thorpe Charles Warrington
| editing        = 
| studio         = Famous Players-Lasky/Artcraft
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 minutes 
| country        = United States  Silent English intertitles
| budget         = 
}}
 
 silent romantic comedy film directed by Arthur Rosson with supervision from Allan Dwan and starring Douglas Fairbanks.  The film is now considered lost film|lost. 

==Cast==
* Douglas Fairbanks as Headin South
* Frank Campeau as Spanish Joe
* Katherine MacDonald as The Girl James Mason as His Aide 
* Johnny Judd
* Tom Grimes
* Art Acord
* Hoot Gibson
* Edward Burns credited as Ed Burns Jack Holt Marjorie Daw
* Bob Emmons
* Alice H. Smith

==See also==
* Hoot Gibson filmography
*The House That Shadows Built (1931) promotional film by Paramount with excerpt of Headin South
* List of lost films

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 


 
 