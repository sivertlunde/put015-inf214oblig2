In the Name of the Girl
{{Infobox film
| name           = In the Name of the Girl
| image          = 
| caption        = 
| director       = Tania Hermida
| producer       = Tania Hermida
| writer         = Tania Hermida
| starring       = Eva Mayu Mecham Benavides
| music          = Nelson García
| cinematography = Armando Salazar
| editing        = 
| distributor    = 
| released       =  
| runtime        = 100 minutes
| country        = Ecuador
| language       = Spanish
| budget         = 
}}

In the Name of the Girl ( ) is a 2011 Ecuadorian drama film written and directed by Tania Hermida.    

==Cast==
* Eva Mayu Mecham Benavides as Manuela
* Markus Mecham Benavides as Camilo
* Martina León as María Paz
* Sebastián Hormachea as Andrés
* Francisco Jaramillo as Emilio
* Paul Curillo as Pepe
* Dianneris Díaz as Juanita
* Pancho Aguirre as Uncle Felipe
* Juana Estrella as Grandma Lola
* Felipe Vega de la Cuadra as Grandpa Emilio

==References==
 

==External links==
*  

 
 
 
 
 
 