Thaamarathoni
{{Infobox film
| name           = Thaamarathoni
| image          =
| image_size     =
| caption        =
| director       = Crossbelt Mani
| producer       =
| writer         = Dr. Balakrishnan
| screenplay     = Dr. Balakrishnan
| starring       = Prem Nazir Jayabharathi Adoor Bhasi Bahadoor
| music          = R. K. Shekhar
| cinematography = Mani
| editing        = Chakrapani
| studio         = United Movies
| distributor    = United Movies
| released       =  
| country        = India Malayalam
}}
 1975 Cinema Indian Malayalam Malayalam film,  directed by  Crossbelt Mani. The film stars Prem Nazir, Jayabharathi, Adoor Bhasi and Bahadoor in lead roles. The film had musical score by R. K. Shekhar.   

==Cast==
 
*Prem Nazir
*Jayabharathi
*Adoor Bhasi
*Bahadoor
*K. P. Ummer
*Nellikode Bhaskaran
*Philomina
*Rajakokila
 

==Soundtrack==
The music was composed by R. K. Shekhar and lyrics was written by Vayalar.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aishwaryadevathe || KP Brahmanandan, Kasthoori Sankar || Vayalar ||
|-
| 2 || Bhasmakkuri Thottu || P. Madhuri || Vayalar ||
|-
| 3 || Butterfly Oh Butterfly || K. J. Yesudas || Vayalar ||
|-
| 4 || Ithu Sisiram || Vani Jairam || Vayalar ||
|-
| 5 || Onnu Pettu Kunju || Gopalakrishnan, Kasthoori Sankar || Vayalar ||
|-
| 6 || Thudiykkunnathidathu kanno || K. J. Yesudas || Vayalar ||
|}

==References==
 

==External links==
*  

 
 
 


 