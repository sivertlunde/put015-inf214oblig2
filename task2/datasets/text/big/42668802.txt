Sevanthi Sevanthi
{{Infobox film
| name = Sevanthi Sevanthi
| image =
| caption = 
| director = S. Narayan
| writer = S. Narayan
| based on = 
| producer = Jayamma Chinne Gowda
| starring = Vijay Raghavendra   Ramya   Doddanna   Komal
| music = S. A. Rajkumar
| cinematography = PKH Das
| editing = P. R. Soundar Rajan
| studio  = Sowbhagya Pictures
| released =  
| runtime = 166 minutes
| language = Kannada  
| country = India
}}
 folk songs since the story is about Devu (Vijay Raghavendra), a son of butter seller, who is very good at singing folk songs. 

The film upon release met with average to positive response at the box-office. Critics noted that the film is enjoyable with quality performances and high-class visuals 

== Cast ==
* Vijay Raghavendra as Devu
* Ramya as Sevanthi
* Mukhyamantri Chandru
* Doddanna Thulasi Shivamani
* Komal Kumar
* Padmini Prakash
* Asha Rani
* Vijaya Krishna
* Suraj
* Jayaram

== Soundtrack ==
All the songs are composed and scored by S. A. Rajkumar. The soundtrack comprises all the songs which are reused from the famous Kannada folk songs written by acclaimed poets. The same old songs are remixed with new singers and are presented here. 

{|class="wikitable"
! Sl No !! Song Title !! Singer(s) || Lyrics
|-
| 1 || "Nodavalandaava" || Shankar Mahadevan || 
|-
| 2 || "Bhagyada Balegara" || Kunal Ganjawala, Shreya Ghoshal || 
|-
| 3 || "Chellidaru Malligeya" || S. A. Rajkumar || 
|-
| 4 || "Nimbiya Banada Myagala" || Kunal Ganjawala, Priya || 
|-
| 5 || "Jaaji Mallige Hoove" || Vijay Raghavendra, Shreya Ghoshal || 
|-
| 6 || "Maayadanta Male" || K. S. Chithra || 
|}

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 


 

 