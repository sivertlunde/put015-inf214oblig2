The Barrier of Flames
{{Infobox film
| name           = The Barrier of Flames
| image          = The Barrier of Flames 1914.png
| alt            = 
| caption        =  Jack Harvey
| producer       = 
| writer         = Philip Lonergan Shep the Morgan Jones 
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        =
| country        = United States
| language       = Silent
| budget         = 
| gross          = 
}} Jack Harvey. Shep the Morgan Jones.    The film is about a "little child is saved from a fire by her devoted collie. Having rescued her he jumps from the roof of the burning building into a fire net." 

==Plot==
The mayor refuses to sign certain franchise bills which he believes to be dishonest, and when, unexpectedly, the political boss finds in his hands the means of coercing his superior, he determines to make the most of his opportunity. Little Helen, Mayor Southwicks child, straying away from an automobile party, gets lost in the woods. She comes to the house where the boss holds his secret conferences, and he orders his housekeeper to keep guard over the child while he motors to the city. His plan is to hold the child until her father has signed the bills. Meanwhile, the housekeeper wanders away to a neighbors, leaving little Helen locked in an upper room. But the childs devoted collie, who misses her sorely, already is tracing her. Shep reaches the house just in time to rescue Helen. A fire has broken out while she is locked in alone. He alarms the firemen, climbs a ladder to Helens room, and, jumping through a skylight, leads the rubber-coats to where the child is. With little Helen safe, Shep leaps from the top story of the burning house into the fire net

==Reception==
The film was well-received critically. The Moving Picture World wrote on December 26, 1914: "The big feature of this two-reel picture is not the story, but the incidents and especially the work of Shep, a collie, that acts in an astonishingly intelligent way and is the center of both excitement and pathos. He is the means of bringing the rescue to the Kidlet, shut up in a burning house - very well handled scenes. It is the picture in which Shep jumps out of the second story window into a fire net. The best scenes in the picture are worthy of a better story; but the offering is one that will make an especially strong appeal to all kinds of audiences. The direction is by John Harvey." 

==References==
 
 

==External links==
*  at the Internet Movie Database

 

 
 
 
 
 
 
 
 
 


 