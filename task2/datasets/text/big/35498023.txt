The Last of the Blue Devils
 
The Last of the Blue Devils, subtitled The Kansas City Jazz Story, is a 1979 film documentary with notable figures from the history of Kansas City jazz starring Count Basie and Big Joe Turner.  The film was produced and directed by Bruce Ricker.

The film was made at a two musical gatherings of old Kansas City hands in 1974 at the Mutual Musicians Foundation, the Kansas City African-American Musicians Union, and consists largely of impromptu musical performances by Turner, Basie, Jay McShann, Jimmy Forrest, and Ernie Williams in various combinations, as well as film from a concert by the full Count Basie Orchestra at the University of Kansas in Lawrence.  
 Bennie Moten Orchestra which ultimately became the Count Basie band.
 Chains of Night Train" featuring a lengthy improvisation on tenor saxophone by the songs writer, Jimmy Forrest.

 
 
 
 