Thunder and Lightning (1938 film)
 
{{Infobox film
| name =  Thunder and Lightning 
| image =
| image_size =
| caption =
| director = Anders Henrikson
| producer =
| writer =  P.G. Wodehouse  (novel)   Hasse Ekman 
| narrator =
| starring = Olof Winnerstrand   Nils Wahlbom   Frida Winnerstrand   Åke Söderblom
| music = Eric Bengtson 
| cinematography = Elner Åkesson  
| editing = Oscar Rosander  
| studio = Svensk Filmindustri  
| distributor = Svensk Filmindustri 
| released =  
| runtime = 91 minutes
| country = Sweden Swedish
| budget =
| gross =
}}
Thunder and Lightning (Swedish:Blixt och dunder) is a 1938 Swedish comedy film directed by Anders Henrikson and starring Olof Winnerstrand, Nils Wahlbom and Frida Winnerstrand. It is an adaptation of the novel Summer Lightning by P.G. Wodehouse.  The films art direction was by Arne Åkermark.

==Cast==
*   Olof Winnerstrand as Magnus Gabriel Hägerskiöld  
* Nils Wahlbom as Pontus Hägerskiöld 
* Frida Winnerstrand as Charlotta Hägerskiöld  
* Åke Söderblom as Claes-Ferdinande Hägerskiöld  
* Marianne Aminoff as Inga 
* Hasse Ekman as Bertil Bendix  
* Sickan Carlsson as Pyret Hanson  
* Eric Abrahamsson as Head Waiter Härman  
* Torsten Winge as Axel Hjalmar Stencloo 
* Weyler Hildebrand as Charlie Blomberg 
* Alice Babs as Flower Girl  
* Julia Cæsar as Telegraph operator  
* Emil Fjellström as Andersson 
* Nils Dahlgren as Head waiter at Savoy  
* David Erikson as Driver 
* Georg Fernqvist as Waiter at Savoy  
* Åke Grönberg as Chucker-out  
* Eivor Landström as Guest at Savoy  
* Otto Malmberg as Adolf  

==References==
 

==Bibliography==
* Qvist, Per Olov & von Bagh, Peter. Guide to the Cinema of Sweden and Finland. Greenwood Publishing Group, 2000. 

==External links==
* 

 
 
 
 
 
 
 
 
 

 