Of God and Dogs
{{Infobox film
| name           = Of God and Dogs
| image          = 
| alt            = 
| caption        = 
| director       = Abounaddara Collective
| producer       = Abounaddara Collective Charif Kiwan
| writer         = 
| screenplay     = Abounaddara Collective
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 12 minutes
| country        = Syria
| language       = Arabic
}} documentary short film, directed by Abounaddara Collective.   Abounaddara Collective is a Damascus based independent film company that specializes in documentaries.  

The film later screened at 2014 Sundance Film Festival on January 18, 2014.  It won the Grand Jury Prize at the festival.       The film later screened at 2014 Sundance London Film Festival on April 26, 2014. 

==Synopsis==
The film narrates the story of a soldier, who had killed an innocent man and now seeks vengeance from God.

==Accolades==
  
 
{| class="wikitable sortable"
|-
! Year
! Award
! Category
! Recipient
! Result
|-
| 2014 Sundance Film Festival
| Short Film Grand Jury Prize
| Abounaddara Collective 
|    
|}
 

==References==
 

==External links==
*  

 
 
 
 
 
 
 

 