Book Revue
{{Infobox Hollywood cartoon
| series         = Looney Tunes (Daffy Duck)
| image          = Bookrevue.jpg
| caption        = Title card of the original print
| director       = Robert Clampett
| story_artist   = Warren Foster
| animator       = Robert McKimson Rod Scribner Manny Gould Bill Melendez
| layout_artist = Cornett Wood
| background_artist = Philip DeGuard
| voice_actor    = Mel Blanc (uncredited)  Sara Berner (uncredited)
| musician       = Carl Stalling
| producer       = Eddie Selzer (uncredited)
| studio         = Warner Bros. Cartoons, Inc.
| distributor    = Warner Bros. Pictures
| release_date   = January 5, 1946 (USA)
| color_process  = Technicolor
| runtime        = 7 minutes
| preceded_by    = 
| followed_by    = 
| movie_language = English
}}

Book Revue (later re-issued on May 19, 1951, as Book Review   ) is a 1945 Looney Tunes cartoon short featuring Daffy Duck, released in 1946, with a plotline essentially being a mixture of the plots of 1937s Speaking of the Weather, 1938s Have You Got Any Castles? and 1941s A Coy Decoy. It is directed by Bob Clampett, written by Warren Foster and scored by Carl Stalling.  An uncredited Mel Blanc and Sara Berner provided the voices. As originally released, the title is a pun, as a Revue is a variety show, while a Review is an evaluation of a work (this pun is not retained in the reissue).
 

==Plot==
  Moonlight Sonata. Complete works of Shakespeare". Shakespeare is shown in silhouette while his literally-rendered "works" are clockwork mechanisms, along with old-fashioned "stop" and "go" traffic signals, set to the "ninety years without slumbering, tick-tock, tick-tock" portion of "My Grandfathers Clock".
 Young Man You Made It Had to Be You", as a striptease is about to begin on the cover of Cherokee Strip. Book covers for The Whistler and The Sea Wolf show their characters shouting and whistling at the off-screen action. 
 Ochi chyornye as background music . He dons a zoot suit and a curly, blonde wig, as well as what appears to be a set of fake teeth (which explains why his trademark lisp is nowhere to be heard in this film).

The background changes to a strange one with legible newsprint superimposed on silhouettes of urban buildings; Daffy continues in his fake Russian accent as he sings, Carolina In The Morning  inadvertently teasing the Big Bad Wolf, who at this point is still in the window of "GranMas House"; Daffy beats a hasty retreat to stage left. Meanwhile, Little Red Riding Hood, based on Margaret OBrien, skips past Daffy and toward GranMas House.
 So Big, turns toward the wolf, and his huge nose trips the wolf, who goes sliding down Skid Row, nearly falling into Inferno (Dante)|Dantes Inferno. The wolf scrambles to the top, but the Sinatra caricature reappears, held in the orderlys hands as if he were a doll. The Wolf, being in the grandma archetype, just as the female characters did and skids head first into the inferno.

==Influence==
 
*Later releases of the short had the title card replaced with Warner Brothers "Blue Ribbon" title card on which the title was misspelled (see above).  The original title card has since been located and the fully restored short can be seen on the   four-DVD box set and the Looney Tunes Spotlight Collection:  Vol 2 two-DVD set. 
*In 1994 it was voted #45 of the 50 Greatest Cartoons of all time by members of the animation field. 
* In one episode of Animaniacs, Yakko Wakko and Dot held a Video Review after being released in a videostore. Just like the books, they run in and out of films and mingle with movie characters. Daffy Duck made a cameo in the episode.
*In one segment of the Tiny Toon Adventures episode "Inside Plucky Duck", Plucky performs Daffys giant eye double-take (dubbed "a Clampett Corneal Catastrophe"), only to be stuck in eye form, unable to "de-take" until the segments end.
*Most of the ostensible "book" titles in this cartoon are actually the titles of contemporary magazines or movies while some of the more surreal backgrounds, particularly those in the scat-singing scene, apparently used actual newsprint. Even Inferno (Dante)|Dantes Inferno was the title of a film released a few years earlier by 20th Century-Fox.
*One of the magazines featured, Life, would eventually be co-owned with Warner Bros. under the Time Warner umbrella until spun off in June 9, 2014.

==References==
 
 

==External links==
* 
* 

 
 
 
 