Aap Aye Bahaar Ayee
{{Infobox film
| name           = Aap Aye Bahar Ayee
| image          = AapAayeBaharaaye.jpg
| image_size     =
| caption        = 
| director       = Mohan Kumar
| producer       = Mohan Kumar
| writer         = Mohan Kumar Ved Rahi
| narrator       = 
| starring       = Rajendra Kumar  Sadhana Prem Chopra
| music          = Laxmikant Pyarelal
| cinematography = 
| editing        = Suraj Prakash
| distributor    = 
| released       = 1971
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 1971 Bollywood film produced, directed and written by Mohan Kumar. It stars Rajendra Kumar, Sadhana Shivdasani, Rajendranath and Prem Chopra. The films music is by Laxmikant Pyarelal.

==Plot==
Rohit, Whiskey, and Kumar (Rajendra Kumar, Rajendra Nath, and Prem Chopra respectively) are childhood friends. While travelling to his estate in the company of Whiskey, Rohit meets with beautiful Neena (Sadhana) and falls in love with her. Subsequently, he meets with her father (Raj Mehra) where they are properly introduced, and she too falls in love with him. Unknowingly, Kumar also sends his proposal for marriage to Neena, but she not only rejects it, but also makes it a laugh. Kumar is offended. He plans to take revenge and, shortly before the marriage of Neena and Rohit, just when the marriage is officially fixed, in the same night, Neena un-knowingly surrenders to Kumar, presuming that he is Rohit. After Neena gets molested, Rohit and Kumar become enemies, have a fight and Kumar loses sight in his left eye. Feeling that she is not worthy of Rohit anymore, Neena refuses to marry him. 

Neena discovers that she is pregnant with Kumars child and tries to commit suicide. Rohit stops her from doing so and marries her in a nearby Shiv Mandir. Rohit brings up the child as his own son who is adored by all. Kumar, who is now a known criminal, comes to Rohit asking for money. While he and Rohit are having an argument, Rohit slips out the information that the child is not his. Kumar secretly records this information and blackmails him. One day he kidnaps Rohits son; Rohit goes to his hideout to get his son back. During their fight, Rohit is badly injured, but then Neena comes and kills Kumar with one of with his own henchmans gun. Rohit, Neena and their son live happily ever after.

==Songs==

Aap Aye Bahar Ayee was a box office musical hit.

# Mujhe Teri Mohabbat Ka Sahara - Lata Mangeshkar, Mohd Rafi 
# Poochhe Jo Koi Mujhse - Mohd Rafi 
# Aap Aye Bahaar Ayee - Mohd Rafi  
# Tare Kitne Neel Gagan Pe Tare - Hemlata, Mohd Rafi  
# Tumko Bhi To Aisa Kuchh Hota - Lata Mangeshkar, Kishore Kumar
# Koyel Kyon Gaaye - Lata Mangeshkar, Mohd Rafi

==Cast==
* Rajendra Kumar....Rohit Kumar Verma
* Sadhana Shivdasani....Neena Bakshi
* Prem Chopra....Kumar
* Rajendra Nath....Whiskey
* Bobby
* Raj Mehra....Bakshi (Neenas dad)
* Mumtaz Begum (actress)....Rohits mom
* Meena T.....Rasilee
* Kamaldeep....Manager
* Madhu Apte
* June
* Subhash (as Subhash)
* Sarita Devi
* Kodi S. Irani (as Khodu Baba)
* Surendra (as Surinder)
* Prem Kumar (actor)
* Lamba
* Raj Kumar
* Shaikh
* Uma Khosla
* Master Rohit

== External links ==
*  

 
 
 
 

 