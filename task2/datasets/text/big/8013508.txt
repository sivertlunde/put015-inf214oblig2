Malice in the Palace
 
{{Infobox Film |
  | name           = Malice in the Palace 
  | image          = File:Malice In The Palace.jpg
  | image size     = 190px
  | director       = Jules White Felix Adler
  | starring       = Moe Howard Larry Fine Shemp Howard Vernon Dent George J. Lewis Frank Lackteen Everett Brown Johnny Kascier Joe Palma
  | cinematography = Vincent J. Farrar 
  | editing        = Edwin H. Bryant
  | producer       = Jules White 
  | distributor    = Columbia Pictures 
  | released       =   
  | runtime        = 15 42" 
  | country        = United States
  | language       = English
}}
Malice in the Palace is the 117th short subject starring American slapstick comedy team the Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959.

==Plot==
 
The Stooges, while running the Cafe Casbah Bah (a Middle Eastern restaurant) and attempting to prepare a meal for customers Hassan Ben Sober (Vernon Dent) and Ginna Rumma (George J. Lewis), discover a plan that their hungry customers are hatching. These two thieves are attempting to rob the tomb of King Rootintootin, which contains a priceless diamond, but they discover that the emir of Schmow (Johnny Kascier) has already gotten his hands on the diamond. The two plotters start wailing and are thrown out of the restaurant. The Stooges then attempt to retrieve the diamond themselves, as there is a $50,000 reward at stake.

The Stooges arrive at the emirs palace, all three dressed as Santa Claus. They then manage to acquire the diamond and make a quick exit, but not before dealing with a burly guard.

==Production notes==
 
According to The Three Stooges Journal, a part was written for Curly Howard after his brief cameo in 1947s Hold That Lion!. The lobby card photo noticeably features a slim, mustachioed Curly as an angry chef. However, his illness caused his scenes to be cut (another story is that Moe Howard decided that "The Four Stooges" could not be sustained) and ultimately Larry assumed the role as the chef. Solomon, Jon. (2002) The Complete Three Stooges: The Official Filmography and Three Stooges Companion; Comedy III Productions, Inc., ISBN 0-9711868-0-4 
 reworked in 1956 as Rumpus in the Harem, using ample stock footage from the original. 

==In popular culture== TBS 1995 Halloween special The Three Stooges Fright Night.
 NBA brawl between the Indiana Pacers and the Detroit Pistons has come to be known as the Malice at the Palace, a play on the title of this short and a reference to the fact that the event happened at The Palace of Auburn Hills.

This short was seen in a movie theater in The Garbage Pail Kids Movie.

==References==
 

== External links ==
 
*  
*  
*  

 

 
 
 
 
 
 
 
 