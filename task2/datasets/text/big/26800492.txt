Melody for Three
{{Infobox film
| name           = Melody for Three
| image          =
| image_size     =
| caption        =
| director       = Erle C. Kenton
| producer       = William Stephens (producer)
| writer         = Walter Ferris (writer) Lee Loeb (writer)
| narrator       =
| starring       = Jean Hersholt Fay Wray Walter Woolf King
| music          = C. Bakaleinikoff
| cinematography = John Alton Edward Mann
| distributor    =
| released       =   
| runtime        = 67 minutes
| country        = USA
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Melody for Three is a 1941 American film directed by Erle C. Kenton, one of the six films of the Dr. Christian series.

== Plot summary ==
 

== Cast ==
*Jean Hersholt as Dr. Paul Christian
*Fay Wray as Mary Stanley
*Walter Woolf King as Antoine Pirelle
*Astrid Allwyn as Gladys McClelland
*Schuyler Standish as Billy Stanley
*Maude Eburne as Mrs. Hastings
*Andrew Tombes as Mickey Delany
*Toscha Seidel as Violinist

== References ==
 

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 
 

 