Cube Zero
{{Infobox film
| name           = Cube Zero
| image          = CUBEZEROPOSTER.jpg
| caption        = Theatrical release poster
| director       = Ernie Barbarash
| producer       = {{plainlist|
* Suzanne Colvin Goulding
* Jon Goulding }}
| writer         = Ernie Barbarash
| starring       = {{plainlist|
* Zachary Bennett
* Stephanie Moore
* Michael Riley
* Martin Roach }}
| music          = Norman Orenstein
| cinematography = François Dagenais
| editing        =  
| distributor    = Lions Gate Entertainment
| released       = October 15, 2004
| runtime        = 97 minutes
| country        = Canada French
| budget         =
}} written and directed by the first film. Even though the first two films take place almost entirely within the maze, Cube Zero takes place in both the interior and exterior of the cube and makes significant use of outdoor scenes.

==Plot==

The film starts with a man, Ryjkin, trying to escape from the Cube. He enters a cube and is sprayed with liquid, but thinks it is only water. However, when he rubs the back of his hand, a piece of flesh falls off. This continues all over his body, and he then melts on the floor and dies.

A man named Eric is watching a video feed of this in an observation room with his co-worker named Dodd. Afterwards, Eric is shown to be sketching a portrait of Dodd (in the form of a superhero called Chessman) while playing chess with him at the same time. Dodd has the chess board in front of him, but Eric is at another table, simply drawing and naming his moves. Eric seems to be vastly intelligent, calculating all of Dodds moves.  After Eric wins, he asks Dodd questions about their missing colleagues. Dodd tells him not to ask too many questions or get involved with the occupants of the Cube because it wouldnt make "those upstairs" happy.

Both Eric and Dodd get an order from "upstairs" that asks them to record the dream of a subject, Cassandra Rains. In her dream, Eric sees that she was captured while walking in a pleasant forest with her daughter named Anna. After waking, Cassandra meets the other occupants of the Cube. One of the males placed in the cube, Robert Haskell, has a tattoo on his forehead like the soldier who captured Cassandra. However, Haskell, like everyone else, has no recollection of his former life or how he got there - he only knows his name. According to what Eric and Dodd know, everyone in the Cube faced a death sentence and was presented with a choice: Go into the Cube with their memory completely erased or take the death sentence. A consent form must be signed for someone to be placed in the Cube. Eric discovers that there is no consent form in Cassandras file and argues with Dodd that they should inform the people "upstairs" about this. Meanwhile, Eric and Dodd get "lunch" (in the form of a flavored pill) from "upstairs" via the elevator. 
 incinerates Owen. Eric argues with Dodd over the fate of Owen, on which he says that he said "no", and that no one said yes. Eric realizes that the Cube is inhumane and that those "upstairs" are planning to place people in the Cube without their consent - they will begin randomly placing anyone inside the Cube. He decides to enter the Cube to help Cassandra escape it. He distracts Dodd and enters the elevator, which contain only three buttons - UP (to those "upstairs"), MIDDLE (pointing to their office), and DOWN (to one of the entrances into the Cube). He selects DOWN. 

Jax, one of the Cubes supervisors who monitors the employees, and two of his analysts arrive from "upstairs" to stop Eric from helping Cassandra. Despite their efforts to stop them, Eric and Cassandra make it to the exit room with the help of Dodd, who sabotages the control panels servicing the Cube. He then swallows the main power coupler for the cameras inside the Cube, saying "you will have to take it out of me". Jax kills him with a knife, cuts open his stomach, and gets the coupler. The sabotage reset the Cube,  causing every trap to shut down like Eric planned. He also informs Cassandra that they only have 10 minutes to escape before the Cube enters the "Reset Mode," which sterilizes the rooms by vaporizing whats inside.

Once Eric and Cassandra reach an "exit room," Haskell catches them, now a hacked soldier who tries to capture them. They escape using a secret auxiliary exit just as the Cube enters "Reset Mode," which vaporizes Haskell. They escape into a lake and end up in a forest; however, soldiers searching for them have already arrived. Cassandra manages to escape, but Eric is shot by a dart and passes out.

Eric wakes up in a surgery room and confronts Jax, who reveals that Cassandra may have gotten away. He claims that Eric has been found guilty of "high treason" and "sabotage" against "country and God." Jax tells him "your sentence has been extended for two more lifetimes". Jax claims Eric has already been convicted in a trial and shows him his consent form, saying he agreed to become a test subject many years ago but does not remember.  His brain gets surgically altered, and Eric dreams about Cassandra reuniting with her daughter and praising Eric as a superhero. Eric is then placed in the Cube. His behavior is now similar to that of Kazan from the first film.

==Cast==
 
* Zachary Bennett as Eric Wynn, a Cube technician.
* David Huband as Dodd, a Cube technician.
* Stephanie Moore as Cassandra Rains, a political demonstrator trapped in the Cube.
* Martin Roach as Robert P. Haskell, a soldier employed to take people to the Cube.
* Michael Riley as Jax, a higher ranking supervisor.
* Mike "Nug" Nahrgang as Meyerhold, a man trapped in the Cube.
* Terri Hawkes as Jellico, a woman trapped in the Cube.
* Richard McMillan as Bartok, a man trapped in the Cube.
* Tony Munch as Owen, a cube technician placed in the Cube.
* Jasmin Geljo as Ryjkin, a man trapped in the Cube.
* Joshua Peace as Finn.
* Diego Klattenhoff as Quigley.
* Alexia Filippeos as Anna.
* Sandy Ross as Chandler.
* Dino Bellisario as Smith.
* Ashley James as McCaw.
* Fernando Cursione as Doctor.
* Araxi Arslanian as Female Doctor.
 

==The Cube==
 the second Cube. The only differences are several traps, entrances and exits, and the internal design.
 the second Cube, the first Cube contained not one, but three Exits.

The first exit was an official exit which a subject inside the Cube could find. It is the same exit as in the second Cube and it represents a bright white void. However, as the subject would walk further, he/she would suddenly be chained. The Cubes operator would then ask the person on the loudspeaker his/her name. Given the answer, the operator would ask the person does he/she believe in God. If the answer is negative, the person would then be incinerated. Its unknown what would have happen if the person responded positive, since it was stated that "No one ever said yes." The second exit is actually also an entrance to the Cube, which could be accessed by the Cubes operators via service elevator which would lead into it, by simply walking into it and pressing the "DOWN" button. It is possible that this entrance is used to place the subjects inside the Cube and also to conduct maintenance on it if its possible. The third exit was known only to the Cubes operators, and it lead to an underground lake which would then reach to the surface.

The Cube also had the ability to "reboot" itself: if the Cube was momentarily disabled and ran out of power, the traps and the sensors would be disabled, allowing the subjects inside to freely roam the rooms without any harm. However, the Cube would then start a 10-minute countdown until rebooting, after which the Cube would "reset" and obliterate every human being inside. The Cube also had cameras to monitor the subjects, and also had a type of scanner that would allow the operator to view a persons dream.

The film also shows the exterior as well as the handlers and operators of the Cube: it appears that the structure is controlled by some future totalitarian government that gives people who are sentenced to a life or death sentence the choice to enter the Cube instead. Once they gave their signed consent, they would have their memory erased and be placed inside. Also, the governmental organization controlling the Cube has a chain of command; one of the supervisors is a man named Jax, who oversees the Cube assisted by his own crew of two technicians. A team of regular "observers", also part of the alternate sentence program, and also monitored, are placed in an observation room just outside the Cube, where they are instructed to observe the Cube and handle people who reach an exit. In exchange, they are given a place to live inside the building and meals consisting of food-flavored pills. However, it appears that the government is also forcefully sentencing people to the Cube without their consent, such as political protesters like Rains.

==Reception==
Professional reviews have been mostly positive, garnering reviews from JoBlo.com, AMCs Movie Guide, DVD Talk, and Bloody Disgusting,    with Bloody Disgusting saying that "Cube: Zero isnt the best of the series, but it comes close." 

==References==
 

==External links==
 
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 