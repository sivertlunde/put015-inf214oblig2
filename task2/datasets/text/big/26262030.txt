Tana (film)
{{Infobox film
| name           = Tana
| image          = 
| alt            = 
| caption        = 
| director       = Kristaq Dhamo
| producer       = Llazar Lipivani	 Teodor Siliqi
| writer         = Kristaq Dhamo Fatmir Gjata Nasho Jorgaqi
| screenplay     = Fatmir Gjata
| story          = 
| based on       =  Naim Frashëri Pjetër Gjoka  Kadri Roshi Andon Pano Thimi Filipi Marie Logoreci
| music          = Çesk Zadeja
| cinematography = Mandi Koçi Sokrat Musha
| editing        = Vitori Çeli
| studio         = Shqipëria e Re
| distributor    = 
| released       =  
| runtime        = 82 minutes
| country        = Albania
| language       = Albanian
| budget         = 
| gross          = 
}}
Tana, is known as the first Albanian feature film, produced by the "New Albania" Film Studio ( ). The movie premiered on 17 August 1958. The film was directed by Kristaq Dhamo, and written by Kristaq Dhamo, Fatmir Gjata, and Nasho Jorgaqi. The music was also composed by Çesk Zadeja. The film was entered into the 1st Moscow International Film Festival.   

==Plot== Naim Frashëri) and they both live in an unnamed mountain village in Albania. Tana has to face the old mentality of her old grandfather and she also has to fight the jealousy of Lefter (Kadri Roshi). It is a love game, while socialist progress is highlighted as is often in the socialist realism.

==Cast==
*Tinka Kurti - Tana
*Naim Frashëri (aktor) - Stefani
*Pjetër Gjoka - The Grandfather
*Kadri Roshi - Lefter Dhosi
*Andon Pano - The Cooperative Chief
*Thimi Filipi - Partys secretary
*Marie Logoreci - Stefans Mother
*Violeta Manushi		
*Nikolla Panajoti		
*Melpomeni Çobani	
*Mihal Stefa		
*Vani Trako		
*Lazër Filipi		
*Pandi Raidhi	
*Lazër Vlashi
*Esma Agolli

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 