Line Describing a Cone
  
Line Describing a Cone is a film produced in 1973 by artist Anthony McCall as part of his "solid light" film series. McCalls reduction of film to the essential properties of the medium resembles the work of other radical artists of his time,  and has inspired various projects by artists such as Stan Douglas,  Richard Serra, and Gordon Matta-Clark. 

==Description== 16mm films, Line allowed him to actualize his ideas on the relationship between viewer and film and the medium of film itself.  The thirty minute film begins with a single white dot projected onto a black surface. As time progresses, the dot begins to form a curved line, tracing the circumference of a circle until the end of the line reaches its starting point. Meanwhile, particles in the air reveal the path of light in the space between the projector and the wall, making visible a cone of light.  If the artists display specifications are met, this beam of light projects between thirty and fifty feet. The circle that is projected onto the surface sits approximately twelve inches above the ground, and its diameter spans seven to nine feet.  The exhibition space lacks seating, inviting the viewer to interact with the ray of light beaming from the projector to the screen. When multiple spectators view the piece together, these encounters with the light, at once an interruption and component of the piece, become an interaction with other audience members. 

==Exhibition==
Exhibition of Line has changed since its first displays. Following the rules of cinema, early screenings occurred at a specific time and place. Audiences had to arrive at the beginning of the film, typically hosted in New York lofts, and follow it through its conclusion.  The dust and cigarette smoke that were common in these displays created the cone of light mentioned previously. When the film moved into art institutions such as the Whitney Museum of American Art, dust and smoke were replaced with fog machines.  Moreover, these institutions played the film on a loop, allowing viewers to enter and leave the screening as they pleased. 

In addition to the changes that have already occurred in the display of Line over time, changes in technology and media rouse concern for the long-term fate of the piece. Apart from the change in materials that make the cone visible, from cigarette smoke to artificial fog, the impending obsolescence of 16mm film may require the piece to be transferred to a digital projector. 

==Relation to Film and Sculpture==
Line Describing a Cone reflects McCalls interests in film and sculpture.  Line addresses the medium of film by removing the narrative demands and addressing the specific properties of the medium itself such as projection, frames, and light.  Moreover, by emphaszing the physical space between the projector and screen McCall calls attention to the sculptural dimensions of projection.  In his artistss statement written to judges of the Fifth International Experimental Film Competition, McCall writes:

 

McCall’s manipulation of film has placed the piece within the art world as well as the world of avant-garde film,  and is featured on the Internet Movie Database (IMDb). 

==References==
 

 