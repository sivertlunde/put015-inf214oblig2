Beloved Augustin (1940 film)
{{Infobox film
| name = Beloved Augustin 
| image =
| image_size =
| caption =
| director = E.W. Emo
| producer = Karl Künzel 
| writer =  Horst Wolfram Geissler  (novel)    Hanns Saßmann  
| narrator =
| starring = Paul Hörbiger   Michael Bohnen   Auguste Pünkösdy    Hilde Weissner  
| music = Willy Schmidt-Gentner  
| cinematography = Reimar Kuntze  
| editing =  Munni Obal 
| studio = Wien-Film 
| distributor = Terra Film
| released = 17 December 1940  
| runtime = 95 minutes
| country = Austria (Part of Greater Germany)  German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} novel of later remade as a 1960 film. 
 
==Cast==
*  Paul Hörbiger as Der liebe Augustin - ein Bänkelsänger  
* Michael Bohnen as Der Kaiser  
* Auguste Pünkösdy as Die Kaiserin 
* Hilde Weissner as Marquise de Valais - kaiserl. Freundin  
* Rudolf Prack as Podl Schauerhuber, Musikant  
* Franz Böheim as Wastl, Musikant 
* Erich Nikowitz as Martl, Musikant 
* Richard Romanowsky as Thomas Wolfsgruber 
* Maria Andergas as Mariandl, seine Nichte 
* Anton Pointner as Graf Sinzendorf, Oberhofmeister  
* Richard Eybner as Graf Trautenberg 
*  Hans Unterkircher as Marquis de Valais, ihr Gatte  
* Karl Ehmann 
* Oskar Wegrostek
*   Lina Frank 
*Heinrich Heilinger 
*Helene Lauterböck 
*Eduard Spieß
* Paul Steidtner  
* Otto Storm

== References ==
 

== Bibliography ==
* Hake, Sabine. Popular Cinema of the Third Reich. University of Texas Press, 2001.

== External links ==
*  

 
 
 
 
 
 
 
 
 

 