Eddie and the Cruisers
{{Infobox film
| name           = Eddie and the Cruisers
| image          = Eddie and the cruisers.jpg
| caption        = Theatrical release poster.
| director       = Martin Davidson Joseph Brooks Robert K. Lifton
| based on       = The novel by P.F. Kluge
| screenplay     = Martin Davidson Arlene Davidson
| starring = {{Plainlist|
* Tom Berenger
* Michael Paré
}} John Cafferty
| cinematography = Fred Murphy
| editing        = Priscilla Nedd-Friendly
| distributor    = Embassy Pictures
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English budget = $5 million   accessed 23 March 2014 
| gross          = $4,700,000
}}
Eddie and the Cruisers is a 1983 American film directed by Martin Davidson with the screenplay written by the director and Arlene Davidson, based on the novel by P.F. Kluge. It was marketed with the tagline: "Rebel. Rocker. Lover. Idol. Vanished."

==Plot==
  rock n roll band called Eddie and the Cruisers. 
 Somers Point, David Wilson).

The bands first album, Tender Years, is a hit, featuring songs like "On the Dark Side," "Wild Summer Nights," and the title track. But recording their next album, A Season in Hell, turns out to be a nightmare. Inspired by the bleak, fatalistic poetry of Arthur Rimbaud, Eddie pushes his band mates beyond their limits, musically and personally. Eddie wants to be great, but bassist Sal replies, "We aint great. Were just some guys from Jersey." Eddie makes it clear that if the band cannot be great, then there is no reason to ever play music again.
 Stainton Memorial Causeway. Though presumed dead, Eddie vanishes without a trace. His body is never found.
 documentary is Asbury Park. Atlantic City casino.

During the documentary interviews, the band expresses a desire to relive the past, even though many of their memories are humiliating. For example, during a concert at Benton College, where Frank was once a student, Eddie ridicules Frank repeatedly by referring to him as "Toby Tyler." The other Cruisers share similar stories.

The storys climax involves Joann, completing the one piece of the puzzle that Frank could not: revealing what happened to the bands second album. After storming from the studio, Eddie brought her to the Palace of Depression, a makeshift castle made of garbage and junk that he visited often as a child. She reveals it was in fact she who took the master tapes for the album from Satin Records, hiding them in the Palace of Depression, where she felt they belonged.

Frank and Joann go back to the Palace of Depression to retrieve the master tapes. A mystery man driving a blue 57 Chevy|57 Chevy Bel Air convertible identical to Eddies arrives at the house and calls to Joann. But before she can reach the car, Frank unmasks the impostor, revealing him to be Doc, who was after the master tapes all these years. Moved by his story, Frank and Joann give him the tapes.

The film closes with Maggies story about the band being viewed by a crowd outside a television store. As the credits roll and a song from A Season in Hell plays for the first time, a lone bystander is revealed to be a much older Eddie Wilson.

==Cast==
*Tom Berenger as Frank Ridgeway
*Michael Paré as Eddie Wilson
*Joe Pantoliano as Doc Robbins
*Matthew Laurance as Sal Amato
*Helen Schneider as Joann Carlino David Wilson as Kenny Hopkins
*Michael "Tunes" Antunes as Wendell Newton
*Ellen Barkin as Maggie Foley
*Joanne Collins as Regina Lewis

==Cast notes==
Only two cast members, Michael "Tunes" Antunes, the tenor saxophone player for John Cafferty & The Beaver Brown Band, and backup singer Helen Schneider were professional musicians in the fictional band.

==Production==
===Development===
Martin Davidson has said that the inspiration for the film came from a desire to "get all my feelings about the music of the last 30 years of rock music into it."    He optioned P. F. Kluges novel with his own money and at great financial risk. Muir 2007, p. 84.  He wrote the screenplay with Arlene Davidson and decided to use a Citizen Kane-style story structure. He remembered, "That was in my head: the search." Muir 2007, p. 86.  
 Heart Like a Wheel, and Davidsons film. 

===Shooting===
In order to get a credible looking and sounding band for the film, Davidson hired Kenny Vance, one of the original members of Jay and the Americans.  He showed Davidson his scrapbook, the places the band performed, the car they drove in, and how they transported their instruments. Vance also told Davidson stories about the band, some of which he incorporated into the script.  Tom Berenger has said that he did not try to learn piano for the film but did practice keyboards for hours in his trailer.    Matthew Laurance actually learned how to play the bass through rehearsals. Only Michael "Tunes" Antunes, the tenor saxophone player for John Cafferty and the Beaver Brown Band, and Helen Schneider were professional musicians in the cast.  

Michael Pare was discovered in a New York City restaurant working as a chef. He said of his role in the film that it was "a thrill Ive never experienced. Its a really weird high. For a few moments, you feel like a king, a god. Its scary, a dangerous feeling. If you take it too seriously ..."  

Davidson had the actors who played in Eddies band rehearse as if they were getting ready for a real concert. Pare remembers, "The first time we played together - as a band - was a college concert. An odd thing happened. At first, the extras simply did what they were told. Then, as the music heated up, so did the audience. They werent play-acting anymore. The screaming, stomping and applause became spontaneous."    Davidson recalls, "One by one, kids began standing up in their seats, screaming and raising their hands in rhythmic applause. A few girls made a dash for the stage, tearing at Michaels shirt. We certainly hadnt told them to do that. But we kept the cameras rolling."  Additionally, New Jersey musician Southside Johnny was hired as a technical advisor for the film. 

Ellen Barkin, who has the small role of the television reporter, Maggie Foley, later said that she "hated" making the film:
 That was what we liked to call a “pay the rent” job. It wasn’t a script I liked, but I remember my agent at the time saying, “Look, you only have to work two weeks, and they’re going to pay you a lot of money. We’ll just say it was your first movie and they just didn’t release it.”... I think people were all fucked-up on drugs. I don’t know. I was a little removed, because I wasn’t on the movie the whole time, but it seemed like it was just a mess. Like, when I’d go, I’d think--I like to make a movie where I know who the boss is. I like a big boss. I like a real director. And it seemed like it was just, “Who’s driving the ship here? What’s going on?”  
According to Davidson, when he completed making the film, three different studios wanted to distribute it, and he went with Embassy Pictures because they offered him the most money. However, they had no prior experience in distribution and were unable to properly release it in theaters. Davidson remembered, "And six months later, somebody said, Your picture is appearing on HBO this weekend, and I didnt even know."   

==Soundtrack==
Vance asked Davidson to describe his fictitious band and their music. Initially, Davidson said that the Cruisers sounded like Dion and the Belmonts, but when they meet Frank, they have elements of Jim Morrison and The Doors.  However, Davidson did not want to lose sight of the fact that the Cruisers were essentially a Jersey bar band, and he thought of Bruce Springsteen and the E Street Band. The filmmaker told Vance to find him someone that could produce music that contained elements of these three bands.  Davidson was getting close to rehearsals when Vance called him and said that he had found the band--John Cafferty & The Beaver Brown Band from Providence, Rhode Island. Davidson met the band and realized that they closely resembled the band as described in the script, right down to a Cape Verdean saxophone player, whom he cast in the film. Muir 2007, p. 87.  Initially, Cafferty was only hired to write a few songs for the film, but he did such a good job of capturing the feeling of the 1960s and 1980s that Davidson asked him to score the entire film. 

After successful screenings on HBO in 1984, the album suddenly climbed the charts, going quadruple platinum. The studio re-released the soundtrack in the fall of 1984.    Nine months after the film was released in theaters, the main song in the film, "On the Dark Side," was the number one song in the country on Billboard (magazine)|Billboards Mainstream, Rock, and Heatseeker charts; Muir 2007, p. 88.  and #7 on the Billboard Hot 100|Billboard Hot 100 chart.   Another single from the film, "Tender Years," peaked at #31 on the Billboard Hot 100. 

==Reaction==
Eddie and the Cruisers was originally intended to open during the summer, but a scheduling error resulted in a September release, when its target audience - teenagers - were back in school.  The film had its world premiere at Deauville.    Embassy Pictures threw a promotional party for the film at a West Hollywood dance club in September, 1983 where Cafferty and his band played. 
 USD $1.4 million on its opening weekend. It would go on to make a paltry $4.7 million in North America.    The film was pulled from theaters after three weeks and all of the promotional ads pulled after one week. 

In the fall of 1984, the single "On the Dark Side" from the soundtrack album suddenly climbed the charts, as the film was rediscovered on cable television and home video, prompting the studio to briefly re-release the album.

==Critical reception==
Eddie and the Cruisers was not well received by critics.  , wrote, "At any rate, it seemed to me that what Eddie and the Cruisers aspired to do was certainly worth doing. The problem is that it finally lacks the storytelling resources to tell enough of an intriguing story about a musical mystery man."   

==Re-release==
In 1984, Eddie and the Cruisers was discovered by additional audiences during its first pay cable run on HBO.  Embassy Pictures re-released the film for one week based on successful summer cable screenings and a popular radio single, but it once again failed to perform at the box office.    Looking back, Davidson said, "that picture should have been a theatrical success. There was an audience for it. People still watch it and still tell me about it." 

Davidson was offered to direct a sequel to the film, but he was not keen on the idea and wanted no participation.  The eventual project, which, unlike the first, had had no source novel from Kluge as its basis, was released as   in 1989.

==References==
 

==Bibliography==
*Muir, John Kenneth. The Rock and Roll Film Encyclopedia. Applause Books, 2007.

==External links==
 
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 