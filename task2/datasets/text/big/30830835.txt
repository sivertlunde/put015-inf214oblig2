Oru Ponnu Oru Paiyan
 
{{Infobox film
| name = Oru Ponnu Oru Paiyan
| image = 
| caption = 
| director = Naren Deivanayagam
| producer = Dr.P.K.Kesavaram K.Bharatkrishna K.Lalitha J.Sai Krithikka
| story = Naren Deivanayagam
| screenplay = Naren Deivanayagam Madhu Shubha Poonja
| music = Karthik Raja
| cinematography = AJ Darshan
| editing = B. Lenin
| studio = 
| distributor = 
| released = 2 September 2007
| runtime = 
| country = India
| language = Tamil
| budget = 
| gross = 
}}
Oru Ponnu Oru Paiyan is a film directed by Naren Deivanayagam. It dwells on the untold love of a pair due to family circumstances. The film stars Sandeep, Roopasree and Shubha Poonja.

==Plot==
Sakthi (played by Sandeep) is the only son of Sharath Babu and Bhanupriya. Having tired of urban living, the elderly couple takes up residence in a village along with their son. Viswanathan (Madhu) is the head of the village. He is highly respected for his fairness and for his love and compassion for the poor. His granddaughter (played by Roopa) is a village belle. She catches Sakthis attention as she flits around the village like a butterfly. The two are drawn towards each other and eventually fall in love. Their affair has the tacit blessings of Viswanathan. But it is not going to be a bed of roses for the two as trouble brews up in the person of Sakthis ex-flame, city-bred glamour girl played by Shubha Punja.

==Cast==
* Sandeep as Sakthi
* Roopasree Madhu as Viswanathan
* Shubha Poonja
* Sarath Babu
* Bhanupriya
* Charan Raj
* Subbalakshmi

 
 
 


 