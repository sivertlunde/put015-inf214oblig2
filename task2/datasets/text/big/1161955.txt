Reach for the Sky
 
 
 
{{Infobox film
| name = Reach for the Sky
| image = Reach for the Sky.jpg 
| caption = original theatrical poster
| director = Lewis Gilbert
| producer = Daniel M. Angel
| writer = Paul Brickhill (book) Lewis Gilbert (screenplay) Vernon Harris (addl scenes)
| starring = Kenneth More Muriel Pavlow Lyndon Brook Lee Patterson Alexander Knox
| music = John Addison
| cinematography = Jack Asher
| editing = John Shirley
| distributor = The Rank Organisation
| released =     (US) 
| runtime = 136 minutes
| country = United Kingdom
| language = English
| budget =
}}

Reach for the Sky is a 1956 British biographical film about aviator Douglas Bader, based on the 1954 biography of the same name by Paul Brickhill. The film stars Kenneth More and was directed by Lewis Gilbert. It won the BAFTA Award for Best British Film of 1956.
 

==Plot== Air Vice-Marshal Halahan for his disregard for service discipline and flight rules, he successfully completes his training and is posted to No. 23 Squadron RAF|No. 23 Squadron at RAF Kenley. In 1930, he is chosen to be among the pilots for an aerial exhibition.

Later, although his flight commander has explicitly banned low level aerobatics (as two pilots have been killed trying just that), he is goaded into it by a disparaging remark by a civilian pilot. He crashes.

Mr Joyce, surgeon at the Royal Berkshire Hospital, has to amputate both legs to save Baders life. During his convalescence, he receives encouragement from Nurse Brace. Upon his discharge from the hospital, he sets out to master prosthetic legs. At a stop for some tea, he meets waitress Thelma Edwards. Once he can walk on his own, he starts courting her.

Despite his undiminished skills, he is refused flying duties simply because there are no regulations covering his situation. Offered a desk job instead, he leaves the RAF and works unhappily in an office. He and Thelma marry.
 fought in wing commander.

In 1941, Bader has to bail out over France. He is caught, escapes, and is recaptured. He then makes such a nuisance of himself to his jailers, he is repeatedly moved from one POW camp to another, finally ending up in Colditz Castle. He is liberated after four years of captivity. The war ends (much to Thelmas relief) before Bader can have "one last fling" in the Far East.

On 15 September 1945, the fifth anniversary of the greatest day of the Battle of Britain, Bader, now a group captain, is given the honour of leading eleven other battle survivors and a total of 300 aircraft in a flypast over London.

==Cast==

===Credited===
   Flight Cadet (later Group Captain) Douglas Bader
* Muriel Pavlow as Thelma Edwards (later Bader)
* Lyndon Brook as Flight Cadet (later Wing Commander) Johnny Sanderson Flying Officer (later Group Captain) Stan Turner
* Alexander Knox as Mr J. Leonard Joyce, surgeon at Royal Berkshire Hospital
* Dorothy Alison as Nurse Brace, Royal Berkshire Hospital Flight Lieutenant (later Wing Commander) Harry Day
* Sydney Tafler as Robert Desoutter, prosthetics expert
* Howard Marion-Crawford as Wing Commander (later Group Captain) Alfred "Woody" Woodhall
* Jack Watling as Peel
* Nigel Green as Streatfield
* Anne Leon as Sister Thornhill Charles Carson Air Chief Marshal Sir Hugh Dowding Ronald Adam Air Vice-Marshal (later Air Chief Marshal Sir) Trafford Leigh-Mallory
  Air Vice-Marshal Frederick Halahan Flying Officer Denis Crowley-Milling
* Philip Stainton as Police Constable
* Eddie Byrne as Flight Sergeant Mills, RAF Cranwell instructor
* Beverley Brooks as Sally, Baders girlfriend
* Michael Ripper as Warrant Officer West, 242 Squadron crew chief
* Derek Blomfield as Civilian Pilot at Reading Aero Club
* Avice Landone as Douglas Baders Mother
* Eric Pohlmann as Adjutant at Prison Camp
* Michael Gough as Flying Officer W. J. "Pissy" Pearson, RAF Cranwell flying instructor
* Harry Locke as Bates, Baders batman
* Sam Kydd as Warrant Officer Blake, Air Ministry medical clerk
* Ernest Clark as Wing Commander W. K. Beiseigel
 

===Uncredited===
   Frank Atkinson as Tullin, Desoutters assistant
* Balbina as Lucille Debacker, nurse at St Omer hospital
* Trevor Bannister as Man Listening to Radio
* Victor Beaumont as German Doctor at St Omer hospital
* Peter Burton as Peter, officer at RAF Coltishall Peter Byrne as Civilian Pilot at Reading Aero Club Paul Carpenter as Hall, 242 Squadron
* Hugh David as Flight Cadet Taylor, RAF Cranwell
* Stringer Davis as Cyril Borge
* Guy De Monceau as Gilbert Petit, French Resistance
* Anton Diffring as German Stabsfeldwebel in French Village
* Basil Dignam as Air Ministry Doctor
* Raymond Francis as Wing Commander Hargreaves
* Alice Gachet as Madame Hiecque, French Resistance
 
* Philip Gilbert as Canadian Pilot with 242 Squadron
* Alexander Harris as Don Richardson Charles Lamb as Walker, Desoutters assistant Jack Lambert as Adrian Stoop
* Barry Letts as Tommy
* Richard Marner as German Officer in Staff Car Roger Maxwell as Man at The Pantiles
* Rene Poirier as Monsieur Hiecque, French Resistance
* Clive Revill as RAF Medical Orderly at RAF Uxbridge George Rose as Squadron Leader Edwards, staff officer, Fighter Command John Stone as Limping Officer
* Jack Taylor as British Pilot with 242 Squadron
* Russell Waters as Pearson
* Gareth Wigan as Woodhalls Assistant
 

==Aircraft==
The following aircraft were used in the filming of Reach for the Sky. "Lewis Gilbert, Douglas Baders film biographer." Aeroplane, Vol. 38, No. 9, September 2010, p. 28. 

{| class="wikitable"
|-
! Aircraft
! Registration or serial
! Role
! Fate
| Photo
|-
| Avro 504K
| E3404
| Flying
| Preserved at Shuttleworth Collection, Old Warden, United Kingdom. 
|  
|-
| Avro Tutor
| K3215
| Static
| Preserved at Shuttleworth Collection.
|  
|- Bristol F.2b
| D8096
| Camera ship
| Preserved at Shuttleworth Collection.
|  
|-
| Bristol Bulldog
| K2227
| Static RAF Museum, Hendon, United Kingdom.
|  
|- Hawker Hurricane I
| P2617
| Static
| static exhibit at RAF hendon Museum.
|
|- Hawker Hurricane IIC
| LF363
| Flying
| Airworthy with the Battle of Britain Memorial Flight, RAF Coningsby, United Kingdom.
|  
|-
| Hawker Hurricane IIC
| unknown
| Static
| Believed scrapped.
|
|-
| Spartan Arrow
| G-ABWP
| Static
| Airworthy, privately owned.
|  
|- Supermarine Spitfire XVI
| RW345
| Static
| Scrapped May 1956
|
|-
| Supermarine Spitfire XVI
| RW352
| Flying
| Scrapped 1957
|
|-
| Supermarine Spitfire XVI
| SL574
| Flying Air & Space Museum, San Diego, United States.
|  
|-
| Supermarine Spitfire XVI
| SL745
| Static
| Scrapped May 1956
|
|-
| Supermarine Spitfire XVI
| TB293
| Static
| Scrapped May 1956
|
|-
| Supermarine Spitfire XVI
| TB885
| Static
| Under restoration in United Kingdom to flying condition.
|
|-
| Supermarine Spitfire XVI
| TE288
| Static
| Preserved at Royal New Zealand Air Force Museum, Wigram Aerodrome|Wigram, New Zealand
|  
|-
| Supermarine Spitfire XVI
| TE341
| Static
| Scrapped at Pinewood Studios
|
|-
| Supermarine Spitfire XVI
| TE358
| Flying
| Scrapped by Coleys Ltd, Feltham, United Kingdom in April 1957
|
|-
| Supermarine Spitfire XVI
| TE456
| Flying
| Preserved at Auckland War Memorial Museum, Auckland, New Zealand
|  
|}

==Production==
To depict the various Royal Air Force bases realistically, principal filming took place in Surrey at RAF Kenley, and around the village of Bagshot. Studio work was completed at the Pinewood Studios, Iver Heath, Buckinghamshire, England, UK. Available wartime combat aircraft including Hawker Hurricane and Supermarine Spitfire fighters were arranged to take on the aerial scenes.

According to Ben Mankiewicz on Turner Classic Movies|TCM, Richard Burton was producer Danny Angels first choice for the lead but he dropped out. Mores fee was £25,000.  Mankiewicz also noted that More arranged to meet Bader to prepare for the role. They played a round of golf; much to Mores surprise (as he was a good golfer), Bader beat him decisively. 

==Reception==
The film fared well with the public, being the most popular film in the UK for 1956. When the film was released in North America in 1957, the American release version was slightly altered with 12 minutes edited out. Rank made a concerted effort to ensure the film was successful in America, sending Kenneth More over to do a press tour, but the public was not enthusiastic. 

==References==
Notes
 
Citations
 

Bibliography
 
* Bader, Douglas. Fight for the Sky: The Story of the Spitfire and Hurricane. Ipswich, Suffolk, UK: W.S. Cowell Ltd., 2004. ISBN 0-304-35674-3. 
* Brickhill, Paul. Reach for the Sky: The Story of Douglas Bader DSO, DFC. London: Odhams Press Ltd., 1954. ISBN 1-55750-222-6. 
* Dolan, Edward F. Jr. Hollywood Goes to War. London: Bison Books, 1985. ISBN 0-86124-229-7.
* Hardwick, Jack and Ed Schnepf. "A Buffs Guide to Aviation Movies". Air Progress Aviation Vol. 7, No. 1, Spring 1983.
* More, Kenneth. More or Less. London: Hodder & Staughton, 1978. ISBN 978-0-340-22603-2.
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 