Franz Kafka's It's a Wonderful Life
{{Infobox film
| name          = Franz Kafkas Its a Wonderful Life
| image         = 
| caption       = 
| director      = Peter Capaldi
| writer        = Peter Capaldi
| starring      = Richard E. Grant
| producer      = Ruth Kenley-Letts
| music         = Philip Appleby
| cinematography= 
| release       = 2003
| distributor   = 
| runtime       = 23 minutes
| country       = United Kingdom
| language      = English
}}

Franz Kafkas Its a Wonderful Life (1993) is a short comic film for BBC Scotland. Written and directed  by Peter Capaldi, it stars Richard E. Grant as Franz Kafka and co-stars Ken Stott.

The title refers to the name of the writer Franz Kafka and the film Its a Wonderful Life, directed by Frank Capra, and the plot takes the concept of the two to absurd depths. The great writer is about to write his famous work, The Metamorphosis, but inspiration is lacking, and he suffers continual interruptions.
 Naughty Marietta.
 following year it tied for an Academy Award for Live Action Short Film with Trevor (film)|Trevor.   }} 

==Cast==
* Richard E. Grant - Franz Kafka
* Crispin Letts - Gregor Samsa
* Ken Stott - Woland the Knifeman
* Elaine Collins - Miss Cicely
* Phyllis Logan - Frau Bunofsky
* Lucy Woodhouse - Party Girl

==References==
 

==External links==
* 
*  

 
 

 
 
 
 
 
 
 
 
 
 

 