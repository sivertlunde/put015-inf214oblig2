Lo (film)
{{Infobox film
| name           = Lo
| image          = Lo (film).jpg
| image size     = 
| alt            = 
| caption        = 
| director       = Travis Betz
| producer       = Vicky Jenson Tom Devlin Aaron Gaffey Jessica Petelle Lola Wallace
| writer         = Travis Betz
| starring       = Sarah Lassez Jeremiah Birkett Ward Roberts
| music          = Scott Glasgow
| cinematography = Joshua Reis
| editing        = Travis Betz
| studio         = SKD
| distributor    = Entertainment One (DVD)
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 2009 experimental experimental comedy film|comedy/horror film|horror/romance film written and directed by Travis Betz.    The film premiered at the Austin Film Festival October 2009  and had DVD release in February 2010.   

==Plot==
The film opens with Justin (Ward Roberts) sitting in his apartment, inside a pentagram. After following the steps in a dark magic book, he summons the crippled demon Lo (Jeremiah Birkett), who is less than impressed with Justin and insists on calling him dinner and heavily implying that he will be eaten if he leaves the pentagram. Justin quickly tells Lo that he used the ritual to be able to order a demon to search Hell for his girlfriend April (Sarah Lassez), whom he insists was taken by a demon (his proof being large claw marks on his chest). Lo becomes angered not only by the demand but the fact that there are countless billions of people in Hell and finding April would be an impossible task. Undeterred, Justin orders Lo to search anyway. Conceding, Lo asks about April so that finding her would be easier. As Justin begins to recount their first meeting, Lo projects the memory onto a wall so he can watch. Lo notes that during the meeting April was both odd and that she gave Justin a fake name when she had to think what her name was.

Justin looks up to see April in front of him. She notes that he didnt burn the book, something Justin apologizes for as he wasnt able to, but she forgives him. She asks him if he still wants to save her, despite everything he has been told, to which Justin responds that he knows who she really is. Upon seeing that Justin loves her, she tells him to turn to a page in the book and read the spell. After doing so, April is able to enter the pentagram. 

She kisses Justin, extracting the poison from his body. April tells Justin that she cant go with him as the demons would always find them, putting Justin in danger. Justin offers to stay but April refuses to let him stay in Hell for eternity. Justin simply says that she is the only thing he has in  his life, to which April reminds him that she isnt a woman or a human. She tells him once again to burn the book when he leaves, leaving the circle and undoing the spell that allowed her to enter. As April drags herself away, Justin calls after her. Looking back, she becomes Lo, revealing that they were the same person from the start, and that Los intention was to deter Justin because it didnt believe it was worth saving. Lo looks back silently and begins to drag itself away again. Justin watches as Lo leaves, not saying a word.

==Cast==
* Ward Roberts as Justin
* Sarah Lassez as April
* Jeremiah Birkett as Lo
* Devin Barry as Jeez
* Aaron Gaffey as Waiter
* Sarah Larissa Deckert as The Demon Rat
* John Karyus as Demon Lord
* Liz Loza as June

==Production==
Writer/director Travis Betz created the films experimental concept after watching Jan Švankmajers Faust (1994 film)|Faust and determining that restraining a lone character into one place and having demons interact with him "was a very tempting challenge".   Shot in 3 days,  the film has a unique visual element in that there is no scenery and that characters are revealed through lighting as the rest of the set remains in darkness.  The film plays out like a real stage play.  There are a few characters and the storyline revolves around dialogue between the various demons and Justin. 

==Reception==
Independent Critic gave the film a B (3.0 stars), calling it "a love story of the lowest common denominator", and noted that while the film did not reach the level of Betzs 2008 film Sunday (2008 American film)|Sunday, it was a "more ambitious film" in which Betz stretched the limits of what could be done technically on a modest budget, making it "a stellar example of how to assemble a quality, technically proficient film with largely convincing special effects on a limited budget."  They also praised that despite the viewers being aware that the entire film takes place within one restricted area, the special effects makeup, the "larger-than-life" sound design, and "deceptively simple" production design, the film maintains a simplicity that "remains completely captivating." They also noted that the film worked as well as it did because of the directors having assembled a fine cast, with special note that the films lead Ward Roberts was able to convincingly play both ends of the emotional spectrum and take what could have been a one-dimensional character and bring "him vividly to life."  They concluded that the film was "Warped, original, imaginative and quite funny."
   

Blog Critics relates how the film centers around the main character of Justin sitting in the center of an elaborate pentagram drawn on the floor in his darkened apartment.  In describing the characterization of the summoned demons, they write that Lo "comes off like a stand-up comic putting down a heckler" and that Jeez "is at his horrifying best as the lead singer in a lounge act". They note that the films use of flashbacks seems to "emphasize artifice as opposed to realism," and that the film has "cult potential".   

JoBlo noted from viewing a trailer in 2008, that restraining a lone character into one place and having the demons interact with him "seems twisted enough to possibly be an interesting idea,"    but of the DVD release of the completed film, they wrote that while the concept had originality, "it sometimes feels a tad pretentious", and that due to the minimalist staging and heavy dialog, the project might have been better if explored on a theatrical stage. They also noted that while the scenes were somewhat predictable, it was "sometimes fun to watch the verbal tennis match between the characters," as the leads of Ward Roberts as Justin and Jeremiah Birkett as Lo "are good as they try to battle with words about what each player wants."  But they also felt the dialog tended to drag and lose momentum. They reviewer was intrigued by the plots "bizarre events", and granted that although it "sometimes felt as if it was trying too hard," the film for the most part held his attention through clever writing and capable actors.  He also spoke well of the character make-up, writing "I was impressed with most of the make up effects here. I did find that each of the demon characters looked like something from the past, I still give credit for making it look much better than it could have."   

Quiet Earth called the 2008 trailer "deliciously, and sickly, whimsical",    but their opinion was modified after viewing the complete film at its debut at the Austin Film Festival.  They wrote the film "is a modern variation on the "Faust" story and has its clever moments, but ultimately its reach way exceeds its grasp."  They praised the make-up designs of the title demon of Lo and his associate Jeez, writing that they were "top-notch and highly effective".  They also praised the acting of Ward Roberts as Justin and Jeremiah Birkett as Lo, writing they did "fine work, good enough to distract us from the fact that both of them spend most of the running time sitting or lying in the same spot," but did not think as highly of the rest of the cast, offering that Sarah Lassez as April failed in making her character "both weird and lovable" and that Devin Barry as a "nasally fey" Jeez became "seriously annoying within seconds."  They wrote that the biggest disappointment with the film was "it is simply too obviously padded," and every scene suffered from the directors "need to make the runtime acceptably feature length."  They granted that the film was ambitious and it stood apart from most micro-budget films of its kind "by shooting for much artier heights", but as technically adept as the project was, it would have been far better had it been released in a shorter version.   

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 