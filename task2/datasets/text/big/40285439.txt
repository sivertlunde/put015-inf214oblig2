The Outcasts (1982 film)
{{Infobox film
| name           = The Outcasts
| image          = The Outcasts 1982 film poster.jpg
| image_size     = 
| border         = 
| alt            = Poster for The Outcasts (1982 film)
| caption        = Poster for The Outcasts 
| film name      = 
| director       = Robert Wynne-Simmons
| producer       = Tony Dollard
| writer         = Robert Wynne-Simmons
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Mary Ryan, Mick Lally, Cyril Cusack
| music          = Stephen Cooney
| cinematography = Seamus Corcoran
| editing        = Arthur Keating
| studio         = Tolmayax
| distributor    = 
| released       =  
| runtime        = 95 minutes
| country        = Ireland
| language       = English
| budget         = 
| gross          = 
}}
 Channel 4 Film on Irish Film industry.

==Description==
This 95-minute feature film was written and directed in 1982 by Robert Wynne-Simmons, starring Mary Ryan as Maura and  , however it was released on VHS in 1983.  This dreamlike film does not precisely fit any genre, except perhaps fantasy. 

==Plot== Great Famine shaman known as Scarf Michael. He becomes her lover and teacher. 

==Awards== San Remo Oporto Film Festival 1984, and Prix du Publique at Geneva Film Festival.  It has the Arts Council of Ireland Award.  

==References==
 

==External links==
*  
*  
*  
 
 
 