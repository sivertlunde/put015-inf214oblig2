Fuzz (film)
 
{{Infobox film
| name           = Fuzz
| image          = FuzzPoster.jpg
| image_size     = 
| alt            = 
| caption        = Film poster for Fuzz
| director       = Richard A. Colla George Edwards Jack Farren Ed Feldman
| writer         = Evan Hunter
| narrator       = 
| starring       = Burt Reynolds Jack Weston Tom Skerritt Yul Brynner Raquel Welch
| music          = Dave Grusin
| cinematography = Jacques Marquette
| editing        = Robert L. Kimble
| studio         = 
| distributor    = United Artists
| released       = July 14,  
| runtime        = 92 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
Fuzz is a 1972 American action comedy film directed by Richard A. Colla and starring Burt Reynolds, Yul Brynner, Raquel Welch, Tom Skeritt, and Jack Weston.

The screenplay was written by Evan Hunter, based on the 1968 novel of the same name that was part of the "87th Precinct" series he wrote under the name Ed McBain. Dave Grusin composed the films soundtrack score.

Unlike the series 87th Precinct, which is set in a fictional metropolis based in New York City, Fuzz is set and was shot on location in Boston, Massachusetts.

==Plot==
Detectives Steve Carella (Reynolds), Meyer Meyer (Weston), Eileen McHenry (Welch), and Bert Kling (Skeritt) are part of the 87th Precincts team investigating a murder-extortion racket run by a mysterious deaf man (Brynner). While attempting to investigate and prevent the murders of several high-ranking city officials, they also must keep track of the perpetrators of a string of robberies. Further complicating matters is a rash of arson attacks on homeless men.

==Controversy==
In Boston, on October 2, 1973, 24-year-old Evelyn Wagler, after running out of fuel, was walking back to her car with a two-gallon can of gasoline. Six teenagers dragged her into an alley and forced her to pour gasoline on herself. She complied, and was then set on fire by the teenagers. The teenagers walked away laughing.  Wagler was white, and the youths were black, and this murder occurred during a racially tense period in Boston history. After the incident, the press reported that Fuzz had aired the previous weekend, and the perpetrators may have re-enacted the fictional arson attack portrayed in the movie. The case was never solved.   at CelebrateBoston.com 

In Miami, on October 20, 1973, 38-year old Charles Scales, a homeless person sleeping outdoors behind an abandoned building, was approached by a group of teenagers, doused with gasoline, and set on fire. Two other homeless people were also attacked in the same incident, but fortunately escaped. A homeless person that survived stated the teenagers “Were laughing and throwing gas and striking matches” at them. ”Torch Victim Tells Killer’s Name, Dies,” Modesto Bee, October 22, 1973, page b-2, AP Story  The film Fuzz was mentioned in the news reports about the killing, as the attack completely mimicked the movie’s plot. Both the perpetrators and victims were black, ruling out the motivation for the murder may have been racial in nature.

The incidents led to a careful review by network Standards and Practices and a general public feeling that violence on television was inspiring violence in real life. Networks had to curb their violence throughout the decade as a result, and Fuzz got pulled temporarily from TV movie blocks until it returned in its uncut version to cable years later.

==Filming== Orange Line Red Line Cambridge tunnel North End, Public Garden, where Burt Reynolds runs around disguised as a nun.

==Cast==
* Burt Reynolds as Det. Carella
* Raquel Welch as Det. McHenry
* Yul Brynner as The Deaf Man
* Tom Skeritt as Det. Kling
* Jack Weston as Det. Meyer
* James McEachin as Det. Brown
* Dan Frazer as Lt. Byrnes
* Tamara Dobson as Rochelle
* Charles Martin Smith as Baby Don Gordon as La Bresca
* Bert Remsen as Sgt. Murchison
* Steve Ihnat as Det. Parker
* Norman Burton as Police Commissioner Nelson
* Charles Tyner as Pete
* Neile Adams as Teddy

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 