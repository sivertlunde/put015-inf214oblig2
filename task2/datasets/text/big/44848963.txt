The Love Mask
{{Infobox film
| name           = The Love Mask
| image          =
| alt            = 
| caption        = 
| director       = Frank Reicher
| producer       = Jesse L. Lasky
| screenplay     = Cecil B. DeMille Jeanie MacPherson 	
| starring       = Cleo Ridgely Wallace Reid Earle Foxe Bob Fleming Dorothy Abril Lucien Littlefield
| music          = 
| cinematography = Walter Stradling
| editing        = 
| studio         = Jesse L. Lasky Feature Play Company
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 drama silent film directed by Frank Reicher and written by Cecil B. DeMille and Jeanie MacPherson. The film stars Cleo Ridgely, Wallace Reid, Earle Foxe, Bob Fleming, Dorothy Abril and Lucien Littlefield. The film was released on April 13, 1916, by Paramount Pictures.  

==Plot==
 

== Cast ==
*Cleo Ridgely as Kate Kenner
*Wallace Reid as Dan Derring
*Earle Foxe as Silver Spurs
*Bob Fleming as Jim
*Dorothy Abril as Estrella
*Lucien Littlefield

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 