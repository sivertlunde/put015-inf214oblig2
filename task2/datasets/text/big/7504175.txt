Little Nobody (1935 film)
{{Infobox Hollywood cartoon|
| cartoon_name = Little Nobody
| series = Betty Boop
| image =
| caption =
| director = Dave Fleischer
| story_artist =
| animator =
| voice_actor = Mae Questel
| musician =
| producer = Max Fleischer
| distributor = Paramount Pictures
| release_date = January 27, 1936
| color_process = Black-and-white
| runtime = 7 mins
| movie_language = English
}}

Little Nobody is a 1936 Fleischer Studios animated short film starring Betty Boop, and featuring Pudgy the Puppy.

==Synopsis==
Bettys puppy Pudgy is infatuated with the cute dog next door, but is crushed when the dogs owner refers to him as a "little nobody." Betty cheers up her pet by singing "Every Little Nobody is Somebody." Pudgy later proves this when he rescues his doggie love from a waterfall.

==External links==
*  
 

 
 
 
 
 


 