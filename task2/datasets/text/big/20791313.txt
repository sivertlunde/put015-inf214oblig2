Too Young to Kiss
 
{{Infobox film
| name           = Too Young to Kiss
| image          = Too-Young-To-Kiss.jpg
| caption        = Theatrical release poster
| director       = Robert Z. Leonard
| producer       = Sam Zimbalist
| writer         = Everett Freeman Frances Goodrich Albert Hackett
| starring       = June Allyson
| music          = 
| cinematography = Joseph Ruttenberg
| editing        = Conrad A. Nervig
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 91 minutes
| country        = United States
| language       = English
| budget         = $1,439,000  . 
| gross          = $2,308,000 
}} Best Art Best Actress – Motion Picture Musical or Comedy.

==Plot==
Eric Wainwright (Van Johnson), a busy impresario, is besieged by hordes of wannabe concert stars, eager for their big break. One of them is Cynthia Potter (June Allyson), a talented pianist, but she cant get in to see him. When she learns that Wainwright is auditioning young musicians for a childrens concert tour, Cynthia dons braces and bobby sox and passes herself off as a child prodigy. 

==Cast==
* June Allyson as Cynthia Potter
* Van Johnson as Eric Wainwright
* Gig Young as John Tirsen
* Rita Corday as Denise Dorcet (as Paula Corday)
* Kathryn Givney as Miss Benson
* Larry Keating as Danny Cutler
* Hans Conried as Mr. Sparrow
* Esther Dale as Mrs. Boykin
* Antonio Filauri as Veloti Jo Gilbert as Gloria
* Alexander Steinert as Conductor

==Reception==
According to MGM records the film earned $1,602,000 in the US and Canada and $706,000 elsewhere resulting in a profit of $30,000. 

==References==
 

==External links==
 
* 

 

 
 
 
 
 
 
 
 
 
 