Suzakumon (film)
{{Infobox film
| name           = Suzakumon
| image          =
| caption        = Japanese movie poster
| director       = Kazuo Mori
| producer       = executive producer Masaichi Nagata 
| writer         = Matsutarō Kawaguchi (novel) Fuji Yahiro (screenplay)
| starring       = 
| music          = Ichirō Saitō
| cinematography = Kazuo Miyagawa
| editing        = 
| studio         = Daiei Film
| distributor    = 
| released       = March 20, 1957 
| runtime        = 100 min.
| country        = Japan
| awards         = 
| language       = Japanese
| budget         = 
| preceded_by    = 
| followed_by    = 
| 
}}
 Japanese film directed by Kazuo Mori and based on a novel by Matsutarō Kawaguchi. At the 1957 Asia-Pacific Film Festival the film won awards for best film and best cinematography (Kazuo Miyagawa). The film also won a special award at the 1958 Mainichi Film Concours.

== Cast ==
* Ayako Wakao as Princess Chikako, The Princess Kazu (和宮 親子内親王, Kazu-no-miya Chikako naishinnō)
* Raizo Ichikawa as Prince Arisugawa Taruhito
* Fujiko Yamamoto as Yuhide, Princess Kazus waiting woman
* Shunji Natsume as Emperor Kōmei
* Kuniko Miyake as Kyoko, Kazunomiyas mother (Tsuneko Hashimoto)
* Eijiro Tono as Tomofusa Kunokura, Yuhides father
* Eitaro Ozawa as Iwakura (as Sakae Ozawa) Tokunaga
* Tokugawa
* Masao Mishima as Tadayoshi Sakai (Sakai Tadaaki) 13th Shoguns mother
* Kimiko Tachibana as Oriko
* Hisao Toake as Kujo
* Eijirō Yanagi as Ryuan, Yuhides real father

== See also ==
* Suzakumon

== References ==
 

== External links ==
*  
*   http://www.raizofan.net/link4/movie2/kojyo.htm

 

 
 
 
 
 
 


 