When Worlds Collide (1951 film)
{{Infobox film
| name           = When Worlds Collide
| image          = Worldcollide.jpg Theatrical release poster
| director       = Rudolph Maté
| producer       = George Pal Philip Wylie Peter Hansen John Hoyt
| music          = Leith Stevens
| cinematography = W. Howard Greene John F. Seitz
| editing        = Arthur P. Schmidt
| studio         = Paramount Pictures Corp. Paramount Pictures Corp.
| released       =  
| runtime        = 83 minutes
| country        = United States
| language       = English
| Budget         = $936,000 (estimated)  
| gross          = $1.6 million (US rentals) 
}}

When Worlds Collide is a 1951 American Technicolor Peter Hansen of the same name, co-written by Philip Wylie and Edwin Balmer.
 rogue star Bellus and the desperate efforts to build a space ark that will save and transport a small portion of humanity to the stars single orbiting planet, Zyra.

==Plot==
Pilot David Randall is paid to fly top-secret photographs from South African astronomer Dr. Emery Bronson to Dr. Cole Hendron in the United States. Hendron, with the assistance of his daughter Joyce, confirms their worst fears: Bronson has discovered a rogue star named Bellus that is on a collision course with Earth.
 industrialist Sidney Stanton. Stanton demands the right to select the passengers, but Hendron insists that he is not qualified to make those choices; all he can buy with his wealth is a single seat aboard the ark.

Joyce becomes attracted to Randall and prods her father into finding reasons to keep him around, much to the annoyance of her boyfriend, medical doctor Tony Drake. The ships construction is a race against time. As Bellus nears, former skeptics admit that Hendron was right and governments prepare for the inevitable. Groups in other nations also begin building spaceships. Martial law is declared and residents in coastal regions are moved to inland cities.

Zyra makes a close approach, its gravitational attraction causing massive earthquakes, volcanic eruptions, and tidal waves that wreak havoc around the world. Several people are killed at the arks construction camp, including Dr. Bronson. In the aftermath, Drake and Randall travel by helicopter to provide assistance to survivors. When Randall leaves the helicopter to rescue a little boy stranded on a rooftop surrounded by water, Drake must resist a strong temptation to strand Randall.
 microfiche copies of books, equipment, and animals. Finally, the lucky passengers are selected by a lottery, though Hendron reserves seats for a handful of people: himself, Stanton, Joyce, Drake, pilot Dr. George Frey, the young boy who was rescued, and Randall, for his daughters sake. When a young man turns in his winning ticket because his sweetheart was not selected, Hendron arranges for both to go. Randall refuses his seat and only pretends to participate in the lottery, believing he has no needed skills once the ark leaves for Zyra. For Joyces sake, Drake fabricates a "heart condition" for Frey, making a co-pilot necessary; Randall is the only choice.

The cynical Stanton becomes increasingly anxious as time passes. Knowing human nature, he fears what the desperate lottery losers might do, so as a precaution, he has stockpiled weapons; Stantons suspicions prove to be well-founded. His much-abused assistant, Ferris, tries to get himself added at gunpoint to the passenger manifest, only to be shot dead by Stanton. During their final night on Earth, the selected passengers and animals are quietly moved to the launch pad to protect them from more violence.

Shortly before takeoff, many of the lottery losers riot, taking up Stantons weapons to try to force their way aboard the space ark. Hendron bravely stays behind at the last moment, forcibly keeping Stanton in his wheelchair, conserving additional fuel for the flight. With an effort born of desperation, Stanton stands up and starts to walk and then collapses in a futile attempt to board the now departing spaceship.

The crew are rendered unconscious by the g-force of acceleration and do not witness the Earths collision with Bellus, displayed on the forward television monitor. When Randall comes to and sees Dr. Frey already awake and piloting the ship, he realizes he has been deceived.

As the space ark enters the atmosphere, the fuel finally runs out; Randall then takes control, gliding to a rough, but safe landing. Earths survivors begin to disembark, finding Zyra to be habitable. David Randall and Joyce Hendron follow, walking hand-in-hand down the ramp to explore an unknown but hopeful future.

==Cast==
 
* Richard Derr as David Randall
* Larry Keating as Dr. Cole Hendron
* Barbara Rush as Joyce Hendron, his daughter
* John Hoyt as Sydney Stanton Peter Hansen as Dr. Tony Drake
* Alden Chase as Dr. George Frey, Dr. Hendrons second in command
* Hayden Rorke as Dr. Emery Bronson
* Frank Cady as Harold Ferris, Stantons assistant
 

==Production==
A feature film, based on both original novels first serialized in Blue Book magazine, had been considered in the 1930s by Cecil B. Demille. When George Pal began producing his version years later, he initially wanted a more lavish production with a larger budget, but he wound up being forced to scale back his plans. Warren,1982, pp. 151–163. 

Douglas Fairbanks Jr. was first considered for the role of Dave Randall, but Richard Derr was finally hired for the part. Mitchell   

Chesley Bonestell is credited with the artwork used for the film; he created the design for the space ark that was constructed to journey to Zyra. The final scene in the film, showing the sunrise landscape of the alien world, was taken from a Bonestell sketch. Because of budget constraints, the director was forced to use this color sketch rather than a finished matte painting, drawing criticism. The additional poor quality still image showing a drowned New York City is often attributed to Bonestell, but it was not actually drawn by him. Miller et al.   

The Differential analyzer at UCLA is shown briefly near the beginning of the film; it verifies the initial hand-made calculations confirming the coming destruction of the Earth. "There is no error." 

Producer George Pal considered making a sequel based on the second novel After Worlds Collide, but the box office failure of his 1955 Conquest of Space made that impossible. 

==Reception== Destination Moon: "... this time the science soothsayer, whose forecasts have the virtue, at least, of being represented in provocative visual terms, offers rather cold comfort for those scholars who would string along with him. One of the worlds which he arranged to have collide is ours." 

Freelance writer Melvin E. Matthews calls the film a "doomsday parable for the nuclear age of the 50s."    Emory University physics professor Sidney Perkowitz notes that this film is the first in a long list of movies where "science wielded by a heroic scientist confronts a catastrophe." He calls the special effects exceptional. Perkowitz   

Librarian and filmographer Charles P. Mitchell was critical of the "... scientific gaffes that dilute the storyline," as well as a "failure to provide consistent first class effects." He pointed out that there were inconsistencies in the script, such as the disappearance of Dr. Bronson in the second half of the film.   In his flawed analysis, Mitchell also does not recognize that sister spacecraft are being built by other nations and their ultimate fate. He summarizes by saying, "the large number of plot defects are annoying and prevent this admirable effort from achieving top-drawer status." 

===Awards=== Academy Award for special effects. It was also nominated for Best Cinematography-Color. Sullivan et al.   

==Cultural references== tabloid arranges for a publicity-loving LAPD officer to arrest a young actor on the night of this films premiere, resulting in photos of the arrest with the theatre holding the premiere in the background accompanied by the headline "Movie Premiere Pot Bust" (the scene is shown taking place in 1953, long after the 1951 premiere of When Worlds Collide). 

When Worlds Collide is the title of a 1975 album (the related single is "Did Worlds Collide?") by Richard Hudson and John Ford, their third release after leaving Strawbs. When Worlds Collide (Powerman 5000 song)|"When Worlds Collide" is the title of a single by the heavy metal band Powerman 5000 from the 1999 album Tonight the Stars Revolt!.   allmusic.com. Retrieved: January 9, 2015. 

==Remake==
Paramount Pictures began pre-production on a remake of When Worlds Collide c.2013.  As of April 2015, however, its status remained unknown, according to IMDB, and it appears unlikely that the remake will ever be made. 

==References==
Notes
 
Citations
 
Bibliography
 
* Hickman, Gail Morgan. The Films of George Pal. South Brunswick, New Jersey:  A. S. Barnes and Company, Inc., 1977. ISBN 978-0-49801-960-9.
* Matthews, Melvin E. Hostile Aliens, Hollywood, and Todays News: 1950s Science Fiction Films and 9/11. New York: Algora Publishing, 2007. ISBN  978-0-87586-498-3.
* Miller, Ron, Chesley Bonestell, Frederick C. Durant and Melvin H. Schuetz. The Art of Chesley Bonestell. New York: HarperCollins, 2001. ISBN 978-1-85585-884-8.
* Miller, Scott. Sex, Drugs, Rock & Roll, and Musicals. Lebanon, New Hampshire: University Press of New England, 2011. ISBN 978-1-55553-761-6. Greenwood Press, 2001. ISBN 978-0-31331-527-5.
* Perkowitz, S. Hollywood Science: Movies, Science, and the End of the World. New York: Columbia University Press, 2007. ISBN 978-0-23114-281-6.
* Reginald, R. and Douglas Menville. Things to Come: An Illustrated History of Science Fiction Film. New York: Times Books, 1977. ISBN 978-0-81290-710-0.
* Sullivan, III, C. W., Tobias Hochscherf, James Leggott, Donald E. Palumbo, et al., eds. British Science Fiction Film and Television: Critical Essays, Critical Explorations in Science Fiction and Fantasy 29. Jefferson, North Carolina: McFarland & Company,  2011. ISBN 978-0-78644-621-6.
* Bill Warren (film historian and critic)|Warren, Bill. Keep Watching The Skies Vol. I: 1950 - 1957. Jefferson, North Carolina: McFarland & Company, 1982. ISBN 0-89950-032-3.
 

==External links==
 

*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 