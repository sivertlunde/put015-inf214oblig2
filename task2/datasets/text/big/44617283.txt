The Catastrophe of the Balloon "Le Pax"
{{Infobox film
| name           = The Catastrophe of the Balloon "Le Pax"
| image          = 
| alt            =
| caption        = 
| film name      = 
| director       = Georges Méliès
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| based on       = 
| starring       = 
| narrator       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = Star Film Company
| distributor    = 
| released       = 1902
| runtime        = 
| country        = 
| language       = 
| budget         = 
| gross          =  
}}
 

The Catastrophe of the Balloon "Le Pax" ( ) was a 1902 short silent film directed by Georges Méliès. It was released by Mélièss Star Film Company and is numbered 398 in its catalogues.   

The film is a recreation of a real-life catastrophe that occurred in Paris on 12 May 1902.  At 5 a.m. on that day, the Brazilian inventor   and his mechanic, M. Saché, set off in Severos dirigible, the Pax. They intended to fly from Paris to Issy-les-Moulineaux. However, while the aeronauts were still over Paris at about 400 meters altitude, the motor stopped and the dirigible exploded. Both Severo and Saché were killed. 

The Catastrophe of the Balloon "Le Pax" is the second-to-last of Reconstructed newsreels by Georges Méliès|Mélièss "reconstructed newsreels" (staged re-enactments of current events), made between The Eruption of Mount Pelee and The Coronation of Edward VII.    It is currently presumed lost film|lost. 

==See also==
*List of lost films

==References==
 

==External links==
* 

 

 
 
 
 
 
 


 
 