Nazar Ke Samne
 
 
{{Infobox film
| name = Nazar Ke Samne
| image =
| writer =
| starring = Akshay Kumar Farheen
| director = Jagdish A. Sharma
| producer =
| music = Mahesh Kishore
| lyrics =
| released = 2 June 1995
| language = Hindi
| budget  =  
| gross  =  
}}

Nazar Ke Samne is an Indian film directed by Jagdish A. Sharma and released in 1994. It stars Akshay Kumar, Farheen.

==Plot==
Umesh, a photographer is arrested for the murder of his newspaper editor. The fact that they had an altercation earlier and that he was found at the murder scene with the murder weapon (a knife) in his hands lead to his arrest. However, in court the lawyer- Sahni(Kiran Kumar), almost wins the case for Umesh. Enters an eyewitness- Jai (Akshay Kumar) whose statement results in a sentence to be hanged until death for Umesh. Jai Kumar is a fraud who can be paid to become an eyewitness. Sarita- Umeshs sister comes to know about her brothers predicament and that Jai is a liar. She makes Jai realise how his lies can destroy families. He has a change of heart, falls in love with Sarita and promises to save Umesh. However, the court rejects Sahnis request to re-open the case. They come across a photograph which was taken accidentally at the murder scene. However, only the murderers shoes are seen. Jai tries to find the man who had paid him to lie and to find the owner of the shoes. Turns out that the lawyer is not such a nice man after all. He murdered the editor who was blackmailing him. Now as Jai is trying to find him, he has to make sure there is no proof or witnesses (Badshah Khan Sharbatwala) to prove he is the murderer. Who succeeds and how is the rest of the story.

==Characters==
* Akshay Kumar ... Jai Kumar
* Farheen... Sarita
* Ekta Sohini ... Chamia (as Ektaa)
* Mukesh Khanna ... Badshah Khan Sharbatwala
* Kiran Kumar ... Advocate Sangram Singh Sahni
* Dalip Tahil ... Mac (as Dilip Tahil)

==Soundtracks==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| Dil Dhadke Kuchh Kah Nahin
| Kumar Sanu, Sadhana Sargam
|-
| 2
| Main Badshah Khan Yaaro
| Mohammad Aziz
|- 
| 3
| Duniya Ne Mari Thokar  Abhijeet
|- 
| 4
| Dheere Dheere Baat Badi 
| Kumar Sanu, Sadhana Sargam
|- 
| 5
| Itne Karib Aaye
| Kavita Krishnamurthy
|- 
| 6
| Umar Dekh Jani Kamar Dekh Jani 
| Kavita Krishnamurthy
|}

==External links==
*  

 
 
 

 