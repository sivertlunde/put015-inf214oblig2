Three Men and a Baby
{{Infobox film
  | name           = Three Men and a Baby
  | image          = Three men and a baby p.jpg
  | caption        = Theatrical release poster
  | director       = Leonard Nimoy
  | producer       = Ted Field Robert W. Cort Jim Cruickshank James Orr
  | based on       =  
  | starring       = Tom Selleck Steve Guttenberg Ted Danson
  | music          = Marvin Hamlisch Adam Greenberg
  | editing        = Michael A. Stevenson
  | studio         = Touchstone Pictures Silver Screen Partners III Interscope Communications Buena Vista Pictures
  | released       =  
  | runtime        = 102 minutes
  | country        = United States
  | budget         = $11 million
  | gross          = $167,780,960
  | language       = English
}}

Three Men and a Baby is a 1987 comedy film directed by Leonard Nimoy, and starring Tom Selleck, Steve Guttenberg, Ted Danson and Nancy Travis. It follows the mishaps and adventures of three bachelors as they attempt to adapt their lives to pseudo-fatherhood with the arrival of the love child of one of them. The script was based on the 1985 French film Trois hommes et un couffin (Three Men and a Cradle).

The film was the biggest American box office hit of that year, surpassing Fatal Attraction and eventually grossing US$167 million in the US alone.  The movie won the 1988 Peoples Choice Award for Favorite Comedy Motion Picture. 

==Plot==
Architect Peter Mitchell (Tom Selleck), cartoonist Michael Kellam (Steve Guttenberg), and actor Jack Holden (Ted Danson) are happy living their lives as bachelors in their lofty New York City apartment where they have frequent parties and flings with different women. Their lives are disrupted when a baby named Mary arrives on their doorstep one day. A note with her, written by a lady named Sylvia, indicates that she is Jacks, the result of an tryst between actor/actress. Mary arrives in his absence – he is in Turkey shooting a B movie, leaving Peter and Michael to fend for themselves in taking care of her. Prior to leaving, Jack had made arrangements with a director friend to have a "package" delivered to the apartment as a favor. Before Marys arrival, he calls and leaves a message with Peter and Michael informing them of it and to keep it a secret per the director friends wishes. When she arrives, they mistakenly believe she is "the package", even though there is a note from her mother.

Peter and Michael are totally befuddled on how to care for Mary, and Peter leaves to go buy whatever supplies are needed. While he is gone, Mrs. Hathaway (Cynthia Harris), the landlady, delivers a small box (which is the actual "package" containing heroin) to the apartment and Michael tosses it aside while trying to keep Mary under control. After Peter returns, they eventually figure out her proper care, right down to diaper changes, baths, and feedings. 

The next day, two men (who are drug dealers) arrive at the apartment to pick up the package. Peter and Michael mistakenly give Mary to them instead, and shortly after they leave, Peter discovers the actual package. He runs downstairs to intercept them, but trips and stumbles, and the packages contents spill. He gathers it and retrieves Mary from them, but retains the heroin while allowing them to take a can of powdered milk. After the exchange, a police officer attempts to ticket them for illegal parking, but they escape. He accosts Peter and detains him in the apartment until Sgt. Melkowitz (Philip Bosco), a narcotics officer, arrives to question him and Michael about the drugs. They successfully hide them from him during the interrogation, in which they learn that Jacks friend is a drug dealer as well. He leaves with suspicions and puts them and the apartment under surveillance.

Peter and Michael are able to persuade Mrs. Hathaway to babysit Mary while they work. Once they get home, however, they find her bound and gagged and the apartment ransacked, apparently by the dealers demanding the heroin. Mary is safe, however. They continue with their care of her, adjusting to surrogate fatherhood and growing attached to her, until Jack returns.

Once Jack returns, Peter and Michael question him about the entire drug deal and Mary. He replies that he knew nothing about the heroin and initially denies everything about Mary until he reads the note from Sylvia. He then recalls the tryst that eventually led to her being born. Peter and Michael do not hesitate in taking their revenge and passing all responsibility of looking after her to him, but he quickly grows to love her. 

Later, Peter discovers in the mail a news clipping of Jacks director friend being hospitalized after a mugging (presumably by the drug dealers), with a handwritten note, "Dont let this happen to you." They formulate a plan to meet and trap them when they negotiate a deal to deliver the illicit goods. With a recording of the conversation, they prove their innocence to Melkowitz and the dealers are arrested.

By now, they have fully embraced their role as Marys guardians. However, one morning, Sylvia (Nancy Travis) arrives, asking for her back intending to take her to London to live with her family. Handing her over, they quickly find themselves miserable and desperately missing her. Deciding to stop her and Sylvia from leaving, they rush to the airport to try and persuade the latter to stay, but they arrive just as her plane is backing up from the gate. Defeated, they return home, where they find both Mary and Sylvia, who did not go to London after all. Sylvia tearfully explains she doesnt want to give up her acting career but cant do so if she has to raise Mary alone, so Peter quickly invites her and Mary to move in with them with Jack and Michaels agreement, and she agrees.

==Cast==
* Tom Selleck as Peter Mitchell
* Steve Guttenberg as Michael Kellam
* Ted Danson as Jack Holden
* Michelle and Lisa Blair as Mary
* Margaret Colin as Rebecca
* Celeste Holm as Mrs. Holden 
* Nancy Travis as Sylvia Bennington
* Alexandra Amini as Patty
* Peter Brown as Store Clerk
* Francine Beers as Woman at Gift Shop
* Philip Bosco as Sgt. Melkowitz
* Paul Guilfoyle as Vince
* Barbara Budd as Actress
* Michael Burgess as Man at Party
* Claire Cellucci as Angelyne Eugene Clark as Man #2 at Party
* Derek de Lint as Jan Clopatz
* Jacob Strackeljahn as Juan Pablo Jr.
* Jeff Kingsley as Dr. Octavius Agustus Steelex
* Dave Foley as Grocery Store Clerk
* Cynthia Harris as Mrs. Hathaway

==Production==
Mary was played by twins Lisa and Michelle Blair. 

The soundtrack included the Peter Cetera song "Daddys Girl", which was used for the movies big music montage sequence, and the Miami Sound Machine song "Bad Boy", which opened it.

==Urban legend==
  Hollywood Babylon", a second season episode of the TV series Supernatural (U.S. TV series)|Supernatural.
   
 
The figure is actually a cardboard cutout "standee" of Jack, wearing a tuxedo and top hat, that was left on the set. It was created as part of the storyline, in which he, an actor, appears in a dog food commercial, but this portion was cut from the final version of the film. The standee does show up later in the film, however, when Jack stands next to it as Sylvia comes to reclaim Mary. Snopes.com contends that the one in the first scene looks smaller from its appearance in the later scene because of the distance and angle of the shot, and because the curtains obscure its outstretched arms. As for the contention that a boy died in the house, all the indoor scenes were shot on a Toronto sound stage, and no kind of residential dwellings were used for interior filming.  

==Reception==
Critical reception of the film was generally positive.  , based on 34 reviews. 

===Box office=== Walt Disney Studios since it was the first production from the studio to gross over $100 million domestically.

==Sequel==
The film was followed by a 1990 sequel, Three Men and a Little Lady. A new sequel, Three Men and a Bride, supposedly in development, would reunite Selleck, Guttenberg and Danson. 

==Remakes== Mukesh and Saikumar in Tamil as Asathal in 2001.

In August 2011, it was reported Adam Sandler was planning to remake the film, starring Chris Rock, David Spade, and Rob Schneider. 

==In popular culture==
In the 2009 film The Hangover, where three of the main characters acquire a missing baby while searching for their lost friend, character Alan Garner references the film, saying, "Its got Ted Danson, Magnum, P.I., and that Jewish actor".

In the TV show Home Improvement, season 4 ep 21, Tim manages to change a tire in 38 seconds. The head racer says, "In that amount of time, we could change 23 tires and a baby.", to which Al Borland replies, "I love that movie.", referencing the film.

==References==
 

==External links==
*  
*  
 

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 