The Earth Cries Out
{{Infobox film
 | name =The Earth Cries Out
 | image = Il grido della terra.jpg
 | caption =
 | director = Duilio Coletti
 | writer = Giorgio Prosperi  Carlo Levi   Alessandro Fersen
 | story = Tullio Pinelli
 | music = Alessandro Cicognini
 | cinematography =    	Domenico Scala
 | editing =  Mario Serandrei
 | producer =
 | distributor =
 | released =  1948
 | runtime =
 | awards =
 | country =  Italian
 | budget = 
 }} 1948 Cinema Italian Action film|action-drama film directed by Duilio Coletti.   

In 2008 it was restored and shown as part of a retrospective "Questi fantasmi: Cinema italiano ritrovato" at the 65th Venice International Film Festival. 

== Cast ==
*Marina Berti as Dina 
*Andrea Checchi as Ariè
*Vivi Gioi as Judith
*Carlo Ninchi as Commandante della nave 
*Elena Zareschi as Ada 
*Filippo Scelzo as Professor Taumen 
*Vittorio Duse 
*Cesare Polacco   
*Arnoldo Foà  
*Nerio Bernardi  
*Wanda Capodaglio

==References==
 

==External links==
* 

 
 
 
 
 


 