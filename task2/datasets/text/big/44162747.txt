Varanmaare Aavashyamundu
{{Infobox film
| name           = Varanmaare Aavashyamundu
| image          =
| caption        = Hariharan
| producer       = Joy
| writer         =
| screenplay     = Rajkumar
| music          = KJ Joy
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       =  
| country        = India Malayalam
}}
 1983 Cinema Indian Malayalam Malayalam film, Hariharan and Rajkumar in lead roles. The film had musical score by KJ Joy.   

==Cast==
 
*Sukumari
*Pattom Sadan
*V. D. Rajappan Rajkumar
*Bahadoor
*Balan K Nair
*Kuthiravattam Pappu
*Oduvil Unnikrishnan
*Ravi Menon Swapna
*Vincent Vincent
 

==Soundtrack==
The music was composed by KJ Joy and lyrics was written by P. Bhaskaran. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Anuraaga Daaham Nayanangalil || K. J. Yesudas || P. Bhaskaran || 
|-
| 2 || Maina Hu Menake || K. J. Yesudas || P. Bhaskaran || 
|-
| 3 || Menaka Njaan Menaka || Vani Jairam || P. Bhaskaran || 
|-
| 4 || Pandu Ninne || K. J. Yesudas || P. Bhaskaran || 
|}

==References==
 

==External links==
*  

 
 
 

 