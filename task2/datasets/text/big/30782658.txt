Blonde Fever
{{Infobox film
| name           = Blonde Fever
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Richard Whorf
| producer       = William H. Wright
| writer         = 
| screenplay     = Patricia Coleman
| story          = 
| based on       =  
| narrator       = 
| starring       = Philip Dorn Mary Astor Felix Bressart Gloria Grahame
| music          = Nathaniel Shilkret
| cinematography = Lester White 
| editing        = George Hively
| studio         =  MGM
| released       =   
| runtime        = 69 min.
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}
Blonde Fever is a 1944 comedy film directed by Richard Whorf. It is also known as Autumn Fever.

==Plot==
Peter Donay (Philip Dorn) is the not so happy owner of the Café Donay, which is a fancy road side establishment somewhere in between Reno and Lake Tahoe in Nevada. His marriage isn’t what it should be and he has a gambling addiction. One day he meets the curvy night club waitress Sally Murfin (Gloria Grahame), who really is a lot more interested in Peter’s money and business than anything else. Peter’s wife, Delilah (Mary Astor), knows about her husbands love affair, and is determined to get rid of Sally by tricking her that there is no money to be had from Peter, by telling Sally about the gambling and lying about the business being poor. Her plan doesn’t work, and instead Delilah tries to split them up by hiring Sally’s beau Freddie Bilson (Marshall Thompson) as a waiter and let him stay above their garage. Her plan goes to waste, when Sally overhears that Peter is the winner of $40,000 in a lottery. Now Sally is more determined to lay her hands on Peter. Sallys’s advances on Peter makes Freddie very jealous and outraged, and eventually Freddie pulls a gun on Peter and threatens to shoot him. Peter confesses that he and Sally are in love and to be married, and Delilah asks Peter for a divorce, asking him for the $40,000 lottery ticket as her lot in the settlement. Peter refuses at first, but eventually he gives in and gives her the money. Full of regret, he then tells Sally’s friend Johnny about his mistake, and that he wants his wife back. Sally is outraged when she hears about the settlement, and is more interested in Freddie, now that Delilah has bought him a new motorcycle. Sally disappears with Freddie, and Peter begs his wife Delilah for forgiveness, and gets it. It turns out she was bluffing about divorcing and leaving him all along, when her suitcase opens up as they kiss and make up, showing that it was empty. At the end of the movie Astor winks to the camera and smiles.

==Cast==
*Philip Dorn as Peter Donay
*Mary Astor as Delilah Donay
*Felix Bressart as Johnny
*Gloria Grahame as Sally Murfin
*Marshall Thompson as Freddie Bilson
*Curt Bois as Brillon
*Elisabeth Risdon as Mrs. Talford Arthur Walsh as Willie
*Jessica Tandy as diner at inn (uncredited)

==External links==
* 

<!--==References==
 
-->
 
 
 
 
 
 
 