The Horseman (film)
{{Infobox film
| name           = The Horseman
| image          = The_Horseman.jpg
| alt            =
| caption        = Official Teaser Poster
| director       = Steven Kastrissios
| producer       = Rebecca Dakin Steven Kastrissios
| writer         = Steven Kastrissios
| starring       = Peter Marshall Caroline Marohasy Evert McQueen
| music          = Ryan Potter
| cinematography = Mark Broadbent
| editing        = Steven Kastrissios
| studio         = Kastle Films
| distributor    = Umbrella Entertainment Screen Media Ventures
| released       =  
| runtime        = 96 minutes
| country        = Australia
| language       = English Irish language
| budget         =
| gross          =
}}  thriller directed and written by Steven Kastrissios  and starring Peter Marshall, Evert McQueen and introducing Caroline Marohasy. 

==Plot== video tape, it depicts several men having sexual intercourse with his drugged-up daughter. Christian then decides to avenge his daughter by killing all those linked to the sex tape. Along the way he meets a young woman named Alice, and a fragile friendship begins to unfold.

==Cast==
* Peter Marshall as Christian
* Caroline Marohasy as Alice
* Brad McMurray as Derek
* Jack Henry as Finn
* Evert McQueen	as Jim
* Christopher Sommers as Pauly
* Bryan Probets	as Walters
* Steve Tandy as Devlin
* Chris Betts as Hilton
* Damon Gibson as Chuck
* Hannah Levien	as Jesse
* Ron Kelly as Det. Adams Robyn Moore as Irene
* Warren Meacham as Richards
* Greg Quinn as Kenneth
* Rhye Copeman as Eddie

==Production==
The Horseman was conceived as a short film. 

==Critical reception== R rating,  writer and director Steven Kastrissios has mentioned he believes that the brutality is essential to the story.  It currently has 58% on the movie review site Rotten Tomatoes. 

==Release==
The film came out on DVD and Blu-ray on 1 March 2010 in the UK.  The film was part of the Sitges Film Festival,  Fantasia Festival,  the Film4 FrightFest  and South by Southwest 2009.  Screen Media Ventures set the limited theatrical run for the United States on 15 June 2010. 

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 