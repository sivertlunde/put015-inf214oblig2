The Longest Week
{{Infobox film
| name           = The Longest Week
| alt            = 
| image          = The Longest Week FilmPoster.jpeg
| caption        = 
| director       = Peter Glanz
| producer       =  
| writer         = Peter Glanz 
| story          =  
| starring       =  
| music          = 
| cinematography = 
| editing        = 
| studio         = YRF Entertainment
| distributor    = Gravitas Ventures
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = $2.5 million
| gross          = 
}}
The Longest Week is a 2014 comedy-drama film, written and directed by Peter Glanz. The film stars Olivia Wilde, Jason Bateman and Billy Crudup in the lead roles. It has been produced by Uday Chopra, along with Neda Armian.  It is the first project of Yash Raj Films subsidiary Hollywood production house YRF Entertainment. 

== Plot ==
Affluent and aimless, Conrad Valmont lives a life of leisure in his parents prestigious Manhattan Hotel. In the span of one week, he finds himself evicted, disinherited, and... in love.

== Trivia ==
The Director and the financier had two very different versions of the film, including both the cut and presentation (poster, trailer, marketing, et al). A Directors Cut still might come out at some point.

== Cast ==
* Olivia Wilde
* Jason Bateman
* Billy Crudup
* Jenny Slate Tony Roberts

== References ==
 

== External links ==
*  
*    

 
 
 

 