Red Salute (film)
{{Infobox film
| name           = Red Salute
| image          =
| image_size     =
| border         =
| alt            =
| caption        =
| director       = Vinod Vijayan
| producer       = Milan Jaleel
| writer         =
| screenplay     = A. Sajeevan
| story          = Rassaque Thykavu
| based on       =  
| narrator       =
| starring       = Kalabhavan Mani
| music          = Alex Paul
| cinematography = Vinod Illampally
| editing        = Vivek Harshan
| studio         = Galaxy Films
| distributor    =
| released       =   
| runtime        =
| country        =
| language       = Malayalam
| budget         =
| gross          =
}} Malayalam film that was directed by Vinod Vijayan and produced by Milan Jaleel.  The film starred Kalabhavan Mani as Chaala Vaasu, an activist and head load worker. 

==Synopsis==
Red Salute follows the character of Chaala Vaasu (Kalabhavan Mani), a coolie that eventually works his way to the top of the abkari liquor trade business. Despite this bringing him great fortune, Vaasu begins to realize that this fortune comes with its own problems and starts longing for his former life and the simplicity he thinks it brought. 

==Cast==
* Kalabhavan Mani
* Sreedevi Vijayaraghavan
* Kottayam Nazeer
* Cochin Haneefa Devan
* Subair
* Suraj Venjarammoodu
* KPAC Lalitha
* Captain Raju Ramu
* Bheeman Raghu
* Urmila Unni
* Sajitha Betti
* Gayathri

==Reception==
IndiaGlitz gave a lukewarm review of Red Salute, calling it "old wine in a new bottle" and that towards the end parts of the film were "sloppy and unimpressive". 

==References==
 

==External links==
*  
*  

 
 
 


 