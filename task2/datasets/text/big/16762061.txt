Congkak (film)
 
 
{{Infobox film
| name           = Congkak
| image          =
| caption        =
| director       = Ahmad Idham
| producer       = David Teo
| starring       = Erynne Erynna Ruminah Sidek Riezman Khuzaimi
| music          =
| distributor    = Metrowealth Productions Sdn Bhd
| released       = 2008
| runtime        =
| country        = Malaysia
| awards         =
| language       = Malay
| budget         =
}}

Congkak is a 2008 Malaysian horror film directed by Ahmad Idham.

== Plot ==
Kazman is a dedicated husband who, despite his wife Sufiahs protests, decided to buy a bungalow for their family near a lake as a family getaway. Whenever she is in the new house, Sufiah feels as if someone is watching her. At night, Sufiah and Kazmans daughter Lisa goes downstairs to play congkak with someone whom only she can see. Lisas invisible friend compounds Sufiahs uneasiness about the new house. Sufiah throws the congkak in the lake, but is awakened the next night by the sound of the congkak being played again. Sufiah gets up to investigate the congkak sound, and sees an old lady playing it. Soon afterward, Lisa disappears, and the oldest resident in that area, Pak Tua, comes to the familys rescue and helps to locate the missing girl.

==Cast== Nanu as Sufiah
* Riezman Khuzaimi as Kazman
* M. Rajoli as Pak Tua
* Erin Malek as Ely
* Erynne Erynna as Lisa
* Ruminah Sidek as in a supporting role

==External links==
*  
*  
*  

 

 
 

 
 
 
 
 