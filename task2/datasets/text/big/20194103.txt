The City Without Jews
 
{{Infobox film
| name           = Die Stadt ohne Juden
| image          = 
| image_size     = 
| caption        = 
| director       = Hans Karl Breslauer
| producer       = Walterskirchen und Bittner
| writer         = {{plainlist|
* H. K. Breslauer
* Ida Jenbach
}}
| based on       =  
| starring       = {{plainlist|
* Johannes Riemann
* Eugen Neufeld Hans Moser
* Anny Milety
}} Gerhard Gruber (new arrangement 2000)
| cinematography = Hugo Eywo
| editing        = 
| distributor    = 
| released       =  
| runtime        = 80 minutes
| country        = Austria
| language       = German
| budget         = 
| gross          = 
}}
Die Stadt ohne Juden (The City Without Jews) is an 1924 Austrian  .

In his novel, published in 1922, Hugo Bettauer succeeded in creating a relatively accurate allegorical vision of the near future, although the book was intended as entertainment and as a satirical response to the primitive  , a former member of the Nazi Party, who was lionized by the antisemitic Austrian masses and was released from jail shortly after his conviction for murder.   

== Plot ==
In Austria the   and Ludwig Anzengruber are still performed. Many cafes are empty, or are converted into beer halls selling sausages. After an initial upturn, the economy declines, as business has greatly diminished, and has moved to other cities, such as Prague and Budapest. Inflation and unemployment run wild.

The political characters of the book (although not of the film, in order to avoid difficulties with censorship) are delineated in such a way as to be identifiable with real politicians of the period: Bundeskanzler Schwerdtfeger, for example, is based on Ignaz Seipel. Besides the political action, the film also dwells on the love relationship between Lotte (Anny Milety), a typical Viennese girl (Wiener Mädel) and the daughter of a member of the National Assembly who voted for the banishment of the Jews, and the Jewish artist Leo Strakosch (Joseph Riemann).
 Hans Moser), Stars of David. 

This sequence also represents the divergence of the film from the book, as the action of the film is revealed as a dream of the antisemitic Councillor Bernard. This surprising turn of events was described in the original film programme as follows:
"In this moment of the highest distress Councillor Bernard awakes from his dream, finds himself in the tavern at a very late hour and says to the baffled Volbert: Thank God that stupid dream is over - we are all just people and we dont want hate - we want life - we want to live together in peace." 

== Background ==
The film was made in 1924, about two years after the publication of Bettauers novel, which in several essential particulars differs from the film version. For one thing, the city in the film is called "Utopia", instead of "Vienna", as in the book. For another, the film produces a conciliatory happy ending, which is not only the exact opposite of the message of the book, but also represents a surprising and abrupt change of direction within the film itself, the whole of which is thus revealed as the dream of an antisemite who at last comes to the realisation that the Jews are a "necessary evil".  The reason for these fundamental deviations from the original was to reduce the controversial political impact of the content.

Hans Moser had in this film his second ever cinematic role. The female lead was played, as so often in Breslauers films, by Anny (or Anna) Milety, whom he later married (in 1925). The well-known Jewish actors Gisela Werbisek (billed as "Werbezirk") and Armin Berg appeared only in minor roles, as Kathi the cook and Isidor the commissionaire. The Expressionist backdrops and decor which characterise some scenes were the work of Julius von Borsody.

Other cast members were Eugen Neufeld (Bundeskanzler Dr. Schwerdtfeger), Karl Thema (Cllr. Linder), Ferdinand Mayerhofer (Cllr. Volbert), Mizi Griebl (Volberts wife) and Hans Effenberger (Alois Carroni).

== Performance history ==
The premiere took place on 25 July 1924 in Vienna, although there were still technical problems with the film. Bettauer and Breslauer, the director, fell out entirely, and Bettauer later refused to acknowledge any connection between the film and his book. The technically inferior prints of the film were often manually cut and shortened by the cinema owners themselves. Nevertheless the cinema auditoriums were often full, although not only in Austria but also in   the showing of the film was banned. Heimo Halbrainer on  , July 2001 (page viewed on 19 January 2008) 

A campaign of vilification against Bettauer was instigated, partly because of this film and partly because of other activities of his. In the spring of 1925 he was murdered by a NSDAP member, Otto Rothstock, who was hailed as a hero and despite being found guilty of murder, was soon set free. 

In 1933 the film was shown commercially for the last time, again causing a stir, when it was screened in the Amsterdam theatre Carré as a protest against Hitlers Germany. This copy of the film is presumably the same as the one discovered in 1991 in the Nederlands Filmmuseum. It was on a cellulose nitrate base and on the point of disintegration. The German Bundesarchiv in Coblenz therefore made an "emergency copy", which was then reconstructed on behalf of the Filmarchiv Austria (Austrian Film Archive) by the company HS-ART Digital Service of Graz using the "DIAMANT" software developed by Joanneum Research; faded parts were then re-coloured. 

In October 2008 the film was made available on a portable medium for the first time as part of the DVD series  Der österreichische Film.

== Critical reaction ==
"In many places the film follows the original book almost word for word, which makes the Utopian ending even more obviously an expression of appeasement. At the end the surprised audience learns that the entire dramatic action only took place in a dream, and thus never really happened. The on-screen happy ending, dictated by compromise, not only negates the meaning of Bettauers book but also the very real antisemitism that it reflects. Instead it documents a by no means unthinkable and in no way dreamlike reality   This surprising turn of the plot, deviating totally from the literary original, which simplifies the action as the content of a dream, cannot merely be regarded as a simple dramatic exigency, but as a prime example of the Austrian souls ability to repress. This naïve and perhaps crude experiment from 1924 can be taken as a forerunner of what was generally practised after World War II in the country ohne Eigenschaften" (Thomas Ballhausen, Günter Krenn, 2006). 

==See also==
*A Day Without a Mexican: A 2004 film on the effects of the sudden disappearance of all Mexican immigrants in US life.
*Berlin Without Jews, a similar novel likely inspired by The City Without Jews

== References ==
 

== Bibliography ==
* Walter Fritz und Josef Schuchnig (eds.), 1991: Die Stadt ohne Juden. Materialien zum Film. Österreichisches Filmarchiv, Vienna, (Schriftenreihe des Österreichischen Filmarchivs; Folge 26).
* Guntram Geser und Armin Loacker (eds.), 2000: Die Stadt ohne Juden. Filmarchiv Austria, Vienna (Reihe Edition Film und Text. 3.) ISBN 3-901932-08-9.

== External links ==
*    
*  
*    
*    

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 