¿Para qué sirve un oso?
{{Infobox film
| name           = ¿Para qué sirve un oso?
| image          = Paraquesirveunoso.jpg
| caption        = Promotional poster
| director       = Tom Fernández
| producer       = Daniel Bajo Javier Méndez Lola Salvador
| writer         = Tom Fernández Jesse Johnson Oona Chaplin
| music          =
| cinematography = Arnau Valls Colomer
| editing        = Ángel Hernández Zoido
| distributor    = 
| released       =  
| runtime        = 113 minutes
| country        = Spain
| language       = Spanish
| budget         =
| gross          =
}}
¿Para qué sirve un oso? ( ) is a 2011 Spanish eco-comedy film written and directed by Tom Fernández. The film stars Javier Cámara, Gonzalo de Castro and Emma Suárez. It also stars Geraldine Chaplin and her real-life daughter, Oona Castilla Chaplin|Oona. The film premiered on 26 March 2011 at the Málaga Film Festival, this was soon followed by a theatrical release in Spain on 1 April.

==Plot==
A biologist, Guillermo (Camara) returns to his native Spain after discovering a plant growing in the Antarctic ice. The signs are that he is ready to give up his planet-saving cause. He contacts his wildlife-photographer brother, Alejandro (Castro), who lives in a woodland hut with an enthusiastic young Californian, Vincent (Jesse Johnson), both hoping that the bears that once inhabited this area of Spain will eventually return.

Nearby to the woodland hut is Natalia (Emma Suarez), a widow and mother of Daniela (Garcia). Vincent also meets the schoolteacher, Rosa (Oona Chaplin), who he quickly falls for. Guillermo is forced to move into the woodland hut after he is kicked out of the home he shared with his foster mother, Josephine (Geraldine Chaplin). Tensions between the two brothers reach boiling point, as Alejandros idealistic perspective is in sharp contrast with his jaded brother, Guillermo. 

==Cast==
*Javier Cámara as Guillermo
*Gonzalo de Castro as Alejandro
*Emma Suárez as Natalia
*Geraldine Chaplin as Josephine Jesse Johnson as Vincent Oona Chaplin as Rosa
*Sira García as Daniela

==References==
 

==External links==
*  
* 

 
 
 
 
 

 
 