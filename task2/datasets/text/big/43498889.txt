Don Camillo's Last Round
{{Infobox film
 | name = Don Camillos Last Round -Don Camillo e lOnorevole Peppone
 | image = Don Camillos Last Round.jpg
 | caption =
 | director = Carmine Gallone 
 | writer =  Giovannino Guareschi (novel) Age & Scarpelli, Giovannino Guareschi, Leo Benvenuti, Piero De Bernardi, Rné Barjavel
 | starring =  Gino Cervi, Fernandel, Claude Sylvain
 | music =   Alessandro Cicognini
 | cinematography =  Anchise Brizzi
 | editing =  Niccolò Lazzari 
 | producer =   Angelo Rizzoli
 | released =  
 | language  = Italian  
 | country = Italy
 | runtime = 97 min

 }}Don Camillos Last Round is a 1955 French-Italian comedy film directed by Carmine Gallone, starring Fernandel and Gino Cervi. The French title is La grande bagarre de Don Camillo and the Italian title is Don Camillo e lonorevole Peppone. It was the third of five films featuring Fernandel as the Italian priest Don Camillo and his struggles with Giuseppe Peppone Bottazzi, the communist mayor of their rural town. The film had 5,087,231 admissions in France. 

==Plot==
In the small village of Brescello are continuing skirmishes between the parish priest Don Camillo and the communist mayor Peppone Bottazzi. The latter, after a theft of chickens against Don Camillo, finally decides to get around in big politics, going to stand as honorable in Rome. In fact Peppone is convinced by a female "friend" of his party, but the mayors wife goes to complain to Don Camillo, who decides to replace the embarrassing situation.

==Cast==
* Fernandel as Don Camillo
* Gino Cervi as Giuseppe Peppone Bottazzi
* Claude Sylvain as Clotilde
* Leda Gloria as La signora Bottazzi, moglie di Peppone
* Umberto Spadaro as Bezzi
* Memmo Carotenuto as Lo Spiccio
* Saro Urzì as Brusco, il parucchiere
* Guido Celano as Il maresciallo
* Luigi Tosi as Il prefetto
* Marco Tulli as Lo Smilzo 

==References==
 

 
 
 
 
 
 
 
 
 
 