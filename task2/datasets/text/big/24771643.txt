We Think the World of You (film)
{{Infobox film
| name           = We Think the World of You
| image          = Wethinktheworldofyou.jpg
| image_size     = 150
| alt            = 
| caption        = 
| director       = Colin Gregg 
| producer       = Tomasso Jandelli Paul Cowan
| writer         = J.R. Ackerley (novel) Hugh Stoddart (screenplay)
| narrator       =  Liz Smith Frances Barber
| music          = Julian Jacobson Jeremy Sands
| cinematography = Michael Garfath
| editing        = Peter Delfgou
| studio         = Cinecom
| distributor    = 
| released       = 22 September 1988 (UK)
| runtime        =  92 min.
| country        =     English
| budget         = 
| gross          = $20,998 (USA)
| preceded_by    = 
| followed_by    = 
}}
 1988 film directed by Colin Gregg, starring Gary Oldman and Alan Bates, adapted from the J.R. Ackerley novel of the same name. It was produced by Tomasso Jandelli and Cinecom.

==Main cast==
{| class="wikitable" border="1"
|-
! Actor
! Role
|-
| Alan Bates || Frank Meadows
|-
| Max Wall || Tom
|- Liz Smith || Millie
|-
| Frances Barber || Megan
|-
| Gary Oldman || Johnny
|}

==Plot== class war erupts over Evies welfare, exacerbated by Johnnys manipulative and antagonistic wife Megan, whose sole aim is to claim Johnny back from Frank on his forthcoming release. A set of tragi-comic relationships evolve with the dog coming to represent the hold they have over each other.

==Reception==
We Think the World of You has not garnered enough reviews at Rotten Tomatoes to produce an overall rating. Roger Ebert gave the film 3/4 stars, writing: "This is a film that rewards attention. It is wise and perceptive about human nature, and it sees how all of us long for love and freedom, and how the undeserved, unrequited love of an animal is sometimes so much more meaningful than the crabbed, grudging, selfish terms that are often laid down by human beings." 

==References==
 

== External links ==
*  
*  

 
 
 
 
 
 