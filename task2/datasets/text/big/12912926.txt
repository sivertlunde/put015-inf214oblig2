The Shrink Is In
{{Infobox film
| name     = The Shrink Is In
| image    = Shrink dvd.jpg
| caption  = The Shrink Is In DVD
| director = Richard Benjamin
| producers = Andrew Form Tag Mendillo
| writers   = Alison Balian Joanna Johnson Courteney Cox Arquette David Arquette David James Elliott Carol Kane
| cinematography = Kenneth MacMillan David Lawrence Marjorie Maye
| editing  = Jacqueline Cambas
| distributor = Universal Pictures
| runtime  = 87 minutes
| country  = United States
| language = English
}} Courteney Cox Arquette and David Arquette.

== Plot == Courteney Cox Arquette) is a travel journalist who is still recovering from the break-up with her last boyfriend, when her psychiatrist (Carol Kane) ironically suffers a nervous breakdown. While Samantha is canceling her appointments, her new neighbor (David James Elliott) comes for a session. Having never met the real Dr. Rosenberg, she poses as her shrink in an attempt to steer him away from his girlfriend and towards her. Because of her new situation, Samantha ends up seeing a few of Dr. Rosenbergs other patients, including eccentric magazine salesman Henry (David Arquette). Thus leading her to question her life, including whether her perfect man is actually what she truly desires after all.

== Cast == Courteney Cox Arquette as Samantha Crumb
*David Arquette as Henry Popopolis
*David James Elliott as Michael
*Carol Kane as Dr. Louise Rosenberg
*Kimberley Davies as Isabelle
*Viola Davis as Robin
*Jon Polito as Judge Bob

== External links ==
*  

 

 
 
 
 
 
 
 
 