The Elevator (1996 film)
  Richard Lewis and Martin Sheen in the film within a film sequences.

==Synopsis==
Movie producer Roy Tilden is on his way to an award ceremony held in his honour when he gets stuck on an elevator with a young screenwriter, David Brochman Junior. Tilden is not interested in his scripts which are all shorts - but as there is no-one to help the two out of the elevator he is finally forced to listen to David as he reads three of the scripts to him.

The first script titled Dahny, Yow tells of a man who wakes up in the middle of the night by a giant transvestite nun and dwarf Rabbi, both named Dahny, who tell him to change his life. The second script titled Painting the Eiffel Tower is set in a police sergeants office as he has a heart-to-heart talk with an artistically talented street punk. The final script, Typing, Bad Dingo and Crimes Against Humanity tells of an acting student who has to type out a University students thesis by the following day but ends up cracking under the pressure of her own social life.

==External links==
*  

 
 
 
 
 
 
 


 