The Fall of the Romanoffs
{{Infobox film
| name           = The Fall of the Romanoffs
| caption        = Ad for film
| image          = The Fall of the Romanoffs.jpg
| director       = Herbert Brenon
| producer       = Herbert Brenon
| writer         = Van Wyck Brooks George Edwardes-Hall Austin Strong Iliodor (book)
| starring       = Alfred Hickman Nance ONeil Edward Connelly Iliodor
| music          = James C. Bradford
| cinematography =
| editing        =
| distributor    = First National Pictures
| released       =  
| runtime        = 8 reels
| country        = United States Silent (English intertitles)
| budget         = $250,000   
}}
 silent American historical drama film directed by Herbert Brenon. It was released only seven months after the abdication of Tsar Nicholas II in February 1917. This film is also notable for starring, Rasputins rival, the monk Sergei Trufanov|Iliodor, as himself. Costars Nance ONeil and Alfred Hickman were married from 1916 to Hickmans death in 1931. The film was shot in North Bergen, New Jersey, nearby Fort Lee, New Jersey, where many early film studios in Americas first motion picture industry were based at the beginning of the 20th century.    Its survival status is classified as unknown,  suggesting that it is a lost film. 

==Plot==
The film takes place during the final days of Rasputins influence on the Imperial Family shortly before the Russian Revolution.

==Cast==
* Alfred Hickman — Tsar Nicholas II Alexandra
* Edward Connelly — Rasputin
* Iliodor — as Himself
* Charles Edward Russell — as Himself
* Conway Tearle - Prince Felix Yussepov Charles Craig - Grand Duke Nicholas
* Georges Deneubourg - Kaiser Wilhelm II
* Robert Paton Gibbs - Baron Frederick
* William E. Shay - Theofan
* Lawrence Johnson - The Infant Czarevitch
* W. Francis Chapin - Alexander Kerensky
* Peter Barbierre - General Korniloff
* Ketty Galanta - Anna Vyrubova
* Pauline Curley - Princess Irena
* Sonia Marcelle - Sonia
 

==Production==
Iliodor, who left Russia in 1914, played himself in the film, while Nance ONeil was cast as the czarina due to her resemblance of Empress Alexandra.  Director Brenon edited the film during production, allowing it to premiere at the Ritz-Carlton Hotel in New York City on September 6, 1917, which was just days after filming ended.  To keep the film current with events in Russia, Brenon continued to edit and add footage through October 1918 to include scenes of the czars execution and the death of the czarina. 

==References==
 

==External links==
 
*  

 
 

 
 
 
 
 
 


 
 