Remembering Arthur
{{Infobox film
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Martin Lavut
| producer       = Dennis Mohr
| writer         = Martin Lavut
| starring       = 
| music          = Tutu Combo
| cinematography = Andrew Watt
| editing        = Rob Ruzic
| studio         = 
| distributor    = 
| released       =2006
| runtime        = 90 min.
| country        = Canada
| language       = English
}} documentary about collage filmmaker Arthur Lipsett that debuted at the 2006 Toronto International Film Festival. It is directed by Lipsetts close friend Martin Lavut and takes a personal approach to the story of his life through interviews with family, friends and colleagues.  The film was produced by Public Pictures in association with the National Film Board of Canada, Bravo! and TVOntario.

In 2007 it won the "Best Cinematography in a Documentary" Award from The Canadian Society of Cinematographers.

== External links ==
* 
*  
*   at the National Film Board of Canada

 

 
 
 
 
 
 
 
 
 
 