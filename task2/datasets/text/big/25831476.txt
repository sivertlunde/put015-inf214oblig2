Son of India (1931 film)
 
{{Infobox Film
| name           = Son of India
| image          = Son of India poster.jpg
| caption        = Theatrical poster
| director       = Jacques Feyder
| producer       =  John Meehan Claudine West
| based on       =  
| starring       = Ramón Novarro Madge Evans
| music          = 
| cinematography = Harold Rosson
| editing        = Conrad A. Nervig
| distributor    = Metro-Goldwyn-Mayer
| released       = August 1, 1931
| runtime        = 73 minutes
| country        = United States  English
| budget         = 
}}
Son of India is a 1931 American romance film directed by Jacques Feyder. The film is based on the 1882 novel Mr. Issacs written by Francis Marion Crawford. 

==Plot== holy man massacre is caused, but Karim is unnoticed and, unlike his father, survives the tragedy. He is left with his highly valuable diamond.

Karim next journeys to Bombay, where he attempts to sell the diamond in a jewelry store. Feeling that they arent offering him enough money, he leaves. The corrupt store owners, hoping to someway make profit, run out of the store and claim that Karim is a thief. He is taken in arrest and, unable to prove he is the true owner of his fathers diamond, faces a long prison sentence. William Darsay, an American witness, saves him by revealing the truth and Karim is released.

Some time later, Karim becomes one of the wealthiest men of Bombay, attending many high society social functions. At a polo match, he meets Janice Darsey, an attractive young American woman accompanied by her aunt and Dr. Wallace. Feeling attracted to each other, they are soon romanced. This is much to Mrs. Darseys dislike, who doesnt approve her niece dating an Indian man. She attempts to sabotage their relationship by announcing that they will leave for Kolkota.

Janice, however, does not want to leave Karim and runs away from her aunt to secretly accompany Karim on a tiger hunting|hunt. When her aunt finds out, she is infuriated and immediately calls for William, who happens to be Janices brother. During the hunt, Karim notices his fathers killer. Upon confronting him, the murderer starts a shooting. Janice starts to hide and stumbles upon a poisonous plant. Karim brings her to safety and removes the poison, after which they become engaged. Back at home, William and Mrs. Darsey try to stop the marriage by telling them lies, but Karim and Janice come to the conclusion that their love for each other is stronger.

==Cast==
*Ramón Novarro as Karim
*Madge Evans as Janice Darsey
*Conrad Nagel as William Darsey
*Marjorie Rambeau as Mrs. Darsey
*C. Aubrey Smith as Dr. Wallace
*Mitchell Lewis as Hamid
*John Miljan as Juggat
*Nigel De Brulier as Rao Rama
*Katherine DeMille as Amah (uncredited)
*Ann Dvorak as Dancer (uncredited)
Ray Milland as Subaltern(uncredited)

==Production==
Son of India reunited Ramón Novarro with Jacques Feyder, whom he previously had worked with before on Daybreak (1931 film)|Daybreak (1931).  It was Feyders last film before returning to Europe.  Novarros leading lady was Madge Evans. Evans was at the beginning of her MGM career and she was later assigned as Novarros love interest several times again.

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 