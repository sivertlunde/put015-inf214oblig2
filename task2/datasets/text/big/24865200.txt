Eastern Plays
{{Infobox film
| name = Eastern Plays
| image = Eastern-plays-screenshot.jpg
| caption = Itso (left) and Georgi (right) conversing
| director = Kamen Kalev
| writer = Kamen Kalev
| starring = Hristo Hristov   Ovanes Torosyan   Saadet Aksoy   Nikolina Yancheva
| producer = Kamen Kalev & Stefan Piryov (Waterfront Film)    Film i Väst   Fredrik Zander (The Chimney Pot)
| distributor =  Republiken   Memento Films International   Epicentre Films (France)   Bir Film (Turkey)   Kinokuniya (Japan)   Mermaid Film (Japan) 
| released =  
| runtime = 83 minutes
| country = Bulgaria
| language = Bulgarian
}}
 Hristo Hristov, Ovanes Torosyan, Saadet Aksoy and Nikolina Yancheva. The film debuted at the 2009 Cannes Film Festivals Directors Fortnight,    though regular showings in Bulgaria began on 16 October 2009.   

==Synopsis== woodcarver who Turkish family from Istanbul, who are spending the night in Sofia en route to Germany. While Georgi initially participates in the beating, he is frightened and runs away. Itso, who had seen the family at a restaurant while he dined with his now-ex-girlfriend Niki (Nikolina Yancheva), ends up intervening and saving the family, including the beautiful daughter Işıl (Saadet Işıl Aksoy). In his developing feelings for Işıl, Itso sees hope of a positive change in his destiny, while Georgis actions and his brothers help make him question his philosophy and reconsider his outlook on life.       

==Production==
As well a screenwriter and director, Kamen Kalev co-produced the movie along with Stefan Pirjov through their partnership production company Waterfront Film. Other film production houses that co-produced the movies were Chimney Pot, the Swedish Film i Väst and Art Eternal (Bulgaria). 

A large part of the cast of Eastern Plays were non-professionals, including debutant male lead Hristo Hristov who plays himself in the film and who died from a drug overdose towards the end of filming.   Hristovs actual apartment is shown in the film, as are his drawings and the woodcarving workshop he works at.  Nikolina Yancheva, an actress in Stefan Danailovs class at the Krastyo Sarafov National Academy for Theatre and Film Arts, also plays herself in the film. Turkish actress Saadet Işıl Aksoy lends her name to her character in Eastern Plays.

Eastern Plays was mostly filmed in the Bulgarian capital Sofia, with a few scenes in Istanbul, Turkeys largest city. Post-production for the film was done on a laptop computer  due to the very restricted budget.

The soundtrack of Eastern Plays contains two song by the avant-garde musical band Nasekomix,  as well as original music composed by Jean-Paul Wall. 

The world première of Eastern plays happened during the 2009  Cannes Film Festival on 17 May 2009 at Théâtre Croisette. The film had its theatrical première on 6 October 2009 in Burgas, followed by première events in Sofia on 16 October 2009 and Paris in 2010. It was the first Bulgarian feature film to be selected for the Cannes Film Festival official selection since 1990. Although it was not distinguished in Cannes, the movies received many awards from international film festivals, becoming  one of the most awarded Bulgarian films.

==Awards== Best Foreign Language Film at the 83rd Academy Awards,    but it didnt make the final shortlist.   

The film has been screened at a number of film festivals:
* Tokyo International Film Festival 2009        
** Tokyo Sakura Grand Prix (Award for Best Film)
** Award for Best Director (Kamen Kalev)
** Award for Best Actor (Hristo Hristov, posthumously)
* Sarajevo Film Festival 2009 
** CICAE Award
* Antalya Golden Orange Film Festival 2009 
** Youth Jurys Award
* Warsaw International Film Festival 2009     
** 1-2 Competition Award (competition for directors first and second feature films)
* International Film Festival Bratislava 2009   Hristo Hristov, tied with Harold Torres for Norteado )
** Best Director (Kamen Kalev)
** Prize of the Ecumenical Jury (Kamen Kalev)
* Estoril Film Festival 2009  
** Special Prize of the Jury - João Bénard da Costa (tied with The Girl)
* Trieste Film Festival - Alpe Adria Cinema 2010  
** Premio CEI
* Angers European First Film Festival 2010  
** Grand prix du jury long metrage Europeen (Ex-aequo) (tied with La Pivellina)
* Mons International Love Film Festival 2010  
** Le Prix du Public – Prix Be TV
* Sofia International Film Festival 2010   
** The Kodak Award for Best Bulgarian Feature Film
* Las Palmas de Gran Canaria International Film Festival 2010 
** Best First Time Director (Kamen Kalev)

==See also==
* List of submissions to the 83rd Academy Awards for Best Foreign Language Film
* List of Bulgarian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  
*  

 

 
 
 
 