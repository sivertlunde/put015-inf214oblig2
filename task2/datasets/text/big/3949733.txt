Dave Chappelle's Block Party
{{Infobox film
| name           = Dave Chappelles Block Party
| image          = Dave Chappelles Block Party (movie poster).jpg
| alt            = 
| caption        = Promotional poster
| director       = Michel Gondry
| producer       = Dave Chappelle Michel Gondry
| writer         = Dave Chappelle
| starring       = Dave Chappelle
| music          = Corey Smyth
| cinematography = Ellen Kuras Bob Yari Productions
| distributor    = Rogue Pictures
| released       =  
| runtime        = 103 minutes
| country        = United States
| language       = English
| budget         = $3 million 
| gross          = $12,051,924 
}}
Dave Chappelles Block Party, also known as Block Party, is a 2005 documentary film hosted and written by comedian Dave Chappelle, and directed by Michel Gondry.  Its format is inspired by the documentary Wattstax. 
 music producer lupus one month before the films release. The film was officially released at the 2005 Toronto International Film Festival. The film grossed $12.1 million in the box office and debuted at #6 in its opening weekend, grossing $6 million in 1,200 theaters.

==Plot==
  Clinton Hill neighborhood of Brooklyn, New York City. The film features nearby sites including the Broken Angel House in Clinton Hill, Brooklyn as well as areas in Fort Greene, Brooklyn and Bedford-Stuyvesant, Brooklyn. The film was produced before Chappelles highly publicized decision to walk away from a $50 million deal to continue his hit Chappelles Show, and gained prominence after the announcement.
 sketches in between the musical acts.

==Performers==
 
* Dave Chappelle
* Kanye West
* Mos Def
* Talib Kweli
* Jill Scott
* Erykah Badu
* The Roots Common
* Big Daddy Kane
* Kool G Rap
* The Fugees Bilal
* Dead Prez
* Cody Chesnutt
* John Legend
* Central State University Marching Band
* A-Trak
 

==Soundtrack==
{{Infobox album
| Name        = Dave Chappelles Block Party
| Type        = Live Album
| Artist      = Various Artists
| Cover       = Blockpartysndtrk.jpg
| Background  = darkturquoise
| Released    =  
| Recorded    = September 18, 2004
| Genre       = Neo-soul, alternative hip hop, East Coast hip hop
| Length      = Geffen
| Producer    =
| Reviews     = 
| Last album  =
| This album  =
| Next album  =
}}
{{Album ratings
| rev1      = Allmusic
| rev1Score =    
| rev2      = HipHopDX.com
| rev2Score =    
}}
A compilation of "music from and inspired by" the film was released on March 14, 2006. 

The album was released by Geffen Records, and produced by Corey Smyth for Blacksmith Music Corp and Questlove.

# Dead Prez&nbsp;– "Hip Hop" Black Star&nbsp;– "Definition"
# Jill Scott&nbsp;– "Golden"
# Mos Def&nbsp;– "Universal Magnetic"
# Talib Kweli feat. Erykah Badu&nbsp;– "The Blast" Common feat. Erykah Badu & Bilal (musician)|Bilal&nbsp;– "The Light"
# The Roots feat. Big Daddy Kane & Kool G. Rap&nbsp;– "Boom!"
# Erykah Badu&nbsp;– "Back in the Day"
# Jill Scott&nbsp;– "The Way"
# Mos Def&nbsp;– "UMI Says"
# The Roots feat. Erykah Badu & Jill Scott&nbsp;– "You Got Me" Black Star&nbsp;– "Born & Raised"

All the songs were recorded live in concert, except "Born & Raised", an exclusive new studio track from Mos Def and Talib Kwelis Black Star. Many performances, including The Fugees reunion performance, could not be included due to legal restraints with the groups record labels.

Chappelles version of Thelonious Monks Round Midnight (song)|"Round Midnight" was featured in the film, but was not released on the compilation.

Cody chesnuTT  was featured in the film with his song ″Parting Ways″ among others, was not released on the compilation but is shown on the end credits.

==Box office and critical reception== normalized rating out of 100 to reviews from mainstream critics, the film has received an average score of 84 based on 30 reviews.  The DVD has sold a total of 1,240,405 copies since 2006, grossing a total of $18,776,445. 

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 