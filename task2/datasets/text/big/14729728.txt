Lastikman: Unang Banat
{{Infobox Film
| name = Lastikman: Unang Banat
| image = Lastikman_Unang_Banat1.JPG 
| caption = Lastikman: Unang Banat DVD Cover
| director = Mac Alejandre
| producer = 
| writer = Wali Ching  (story)
R.J. Nuevas (script)
| starring = Mark Bautista   Sarah Geronimo  Cherrie Gil  John Estrada   Danillo Barrios  John Manalo
| music = 
| cinematography = 
| editing = 
| distributor = 
| released = December 25, 2004
| runtime =
| country = Philippines English / Tagalog  
| budget = 
}}

Lastikman: Unang Banat is a movie released on December 25, 2004 under VIVA Films, starring Mark Bautista and Sarah Geronimo.

==Plot==
Adrian (Mark Bautista) is just an ordinary boy who lacks skills and talents. He likes his friend Lara (Sarah Geronimo), but is often bullied by his classmates and kids in the neighborhood. Despite being a weakling, Adrian possesses a pure and brave heart when he tries to fight illegal loggers who cut trees on their barrio. Unfortunately, he is beaten by the loggers and left almost half dead. But Because of his pure personality, the enchanted rubber tree that he saves heals him and grants him powers that transform him into a super hero named Lastikman.

==Casts==

===Main Casts===
*Mark Bautista...Lastikman/Adrian Rosales
*Sarah Geronimo...Lara Manuel
*Cherie Gil...Lastika
*John Estrada...Taong Aso
*Danilo Barrios...Reden/Taong Aso

===Also Starring===
*Elizabeth Oropesa...Laras mother
*Joel Torre...Adrians father
*Bearwin Meily...Adrians older brother
*John Manalo...Adrians younger brother
*Mark Gil...Laras Mothers 2nd Spouse
*Athena Tibi...Christy, Lastikas daughter
*Tuesday Vargas...Maritess
*Miles Ocampo...as Young Lara
*Mikel Campos
*Crystal Moreno...Laras Step Sister
*Bobby Andrews
*Marky Lopez
*Vanna Garcia
*Ella V
*Isabella De Leon...(Cameo Only)

==Trivia==
*Supported by PLDT Touch Card, Milo, Jolibee, Nescafe, Skechers, Philam Plans, and Cafe Lupe.

*Hero Angeles was the original choice but ABS-CBN/Star Cinema did not lend him to Viva.

*In preparing for the role, Mark watched Spider-Man (2002) and Superman (1978), but not the Lastikman (2003) starring Vic Sotto saying he wanted to have his own style in playing the character.

*Cherie Gil, John Estrada and Danilo Barrios were appeared in TV Series "Mars Ravelos Lastikman" (on ABS-CBN 2) in 2007-08.

==Awards==
{|| width="90%" class="wikitable sortable"
|-
! width="10%"| Year
! width="30%"| Award-Giving Body
! width="25%"| Category
! width="25%"| Recipient
! width="10%"| Result
|- 2004
| rowspan="6" align="left"| Metro Manila Film Festival   Best Visual Effects
| align="center"| 
|  
|}

==References==
 

== External links ==
*  

 
 
 
 
 
 
 