All the Colors of the Dark
{{Infobox film
| name           = All the Colors of the Dark
| image          = All-the-Colors-of-the-Dark-1972.jpg
| alt            = 
| caption        = 
| director       = Sergio Martino
| producer       = Mino Loy Luciano Martino
| screenplay     = Ernesto Gastaldi Sauro Scavolini
| story          = Santiago Moncada George Hilton Edwige Fenech 
| music          = Bruno Nicolai
| cinematography = Miguel Fernández Mila  (as Miguel F. Mila)  Giancarlo Ferrando
| editing        = Eugenio Alabiso
| studio         = Lea Cinematografica National Cinematografica C.C. Astro
| released       = 28 February 1972
| runtime        = 94 min.
| country        = Italy Spain
| language       = Italian
}}

All the Colors of the Dark (Italian: Tutti i colori del buio) is a 1972 Italian giallo film directed by Sergio Martino.    The film was also released under the alternate titles Day of the Maniac and Theyre Coming to Get You!.

== Synopsis == 
Jane lives in London with Richard, her boyfriend. When she was five, her mother was murdered, and she recently lost a baby in a car crash. Shes plagued by nightmares of a knife-wielding, blue-eyed man. Richard, a pharmaceutical salesman, thinks the cure is vitamins; Janes sister Barbara, who works for a psychiatrist, recommends analysis; a neighbor Janes just met promises that if Jane participates in a Black Mass, all her fears will disappear. Jane tries the Mass, but it seems to bring her nightmares to life. Is there any way out for her short of death or a living hell?

== Critical reception ==
 AllMovie called the film "tiresome". 

== Cast == George Hilton as Richard
* Edwige Fenech as Jane Harrison
* Ivan Rassimov as Mark Cogan
* George Rigaud as Dr. Burton  Susan Scott as Barbara Harrison
* Marina Malfatti as Mary Weil Alan Collins as Lawyer Franciscus Clay

== References ==
 

== External links ==
*  
 

 
 
 
 


 