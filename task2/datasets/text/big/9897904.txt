Article 99
 
{{Infobox film
| name           = Article 99
| image          = Article_ninety_nine_poster.jpg
| caption        = Theatrical Release Poster
| writer         = Ron Cutler
| starring       = Ray Liotta Kiefer Sutherland Forest Whitaker Lea Thompson John Mahoney John C. McGinley Keith David Kathy Baker
| director       = Howard Deutch
| producer       =
| cinematography =
| music          =
| editing        =
| distributor    = Orion Pictures
| released       = March 13, 1992  (U.S.A.) 
| runtime        =
| country        = United States
| language       = English
| budget         =
| gross          = $6,375,979 (US)   
}}
 American comedy dramedy film VA hospital benefits. 

==Plot==


When Dr. Peter Morgan (Sutherland) begins his medical internship at a Veterans Administration hospital, he expects to breeze through on his way to a cushy practice. Instead, hes thrust into a bizarre bureaucratic maze where the health of patients is secondary to politics. And the temperature really rises when he teams up with some freewheeling physicians, led by Dr. Richard Sturgess (Liotta), who think theyve learned how to break the rules-and save lives-without getting caught.

==Production==
 
The film was filmed in Kansas City, Missouri. Many downtown landmarks can be seen in the introduction to the movie and throughout, including the Liberty Memorial. The hospital that was used in the film was known as St. Marys Hospital that sat across the street from Liberty Memorial in Kansas City. The former hospital is now  gone and now a Federal Reserve Bank occupies the land where the hospital once sat.

==Reception==
Article 99 earned $  ($ }} in todays terms) in its opening weekend (March 13, 1992), screening in 1,262 theaters, and ranking it as the number 6 film of that weekend. It earned a total domestic gross of $  ($ }} in todays terms). 

Article 99 has received mixed reviews, currently holding a 46% "rotten" rating on rotten tomatoes based on 13 reviews.

==See also==
* Walter Reed Army Medical Center neglect scandal
* Coming Home (1978 film)|Coming Home (1978 film)
* Born on the Fourth of July (film)|Born on the Fourth of July (1989)

==References==
 

==External links==
*  
*  
*   at Metro-Goldwyn-Mayer (US distributor)
*  , Deseret Morning News, Mar. 13, 1992

 

 
 
 
 
 
 
 
 
 
 