Adventuress Wanted
 

{{Infobox film
| name           = Adventuress Wanted
| image          = 
| caption        = 
| director       = Thomas McAlevey
| producer       = Thomas McAlevey 
| writer         = Thomas McAlevey
| starring       =  
| music          = Thomas Almqvist
| cinematography =  
| editing        =  
| studio         =
| distributor    = 
| released       = 
| runtime        = 85 minutes
| country        = Sweden
| language       = English
| budget         = 
}}
 Adventuress Wanted  is a documentary film starring Thomas McAlevey and Yoshiko Kino. It is written and directed by Thomas McAlevey, who founded Bandit Rock in Stockholm in 1995.

==Plot==
On a quest for the perfect partner, an American man chooses a Japanese woman to attempt a 30,000-mile adventure across Africa, in a beach buggy.

The film opens in Stockholm, following the life of Tom, a successful Western businessman, who decides to recruit a female companion for the ultimate adventure. The journey begins during a blizzard – forcing the pair to drive, roofless, through a freezing Europe and over snow-covered Alps. In the brutal Sahara desert, the buggy literally falls to pieces, paralleling the growing tribulations of Yoshiko and Tom’s relationship. Tom falls sick in Cairo and has emergency surgery. While recovering, he and Yoshiko argue and decide to give up the trip.

Yoshiko and Tom weather the elements and meet the people. With only one another to rely on, they brave everything. From close encounters with lions and elephants to a death defying illegal entry into Sudan, through the joy and the tears and the mind-boggling physical challenges, Yoshiko and Tom keep the camera rolling.

The Swedish press closely followed their trip to its conclusion, including the Swedish tabloid  Aftonbladet, one of the larger daily newspapers in the Nordic countries,  
  and others. 

It is a film about love and life, about heroic efforts in the face of adversity, and about noble intentions gone awry. Set against the majestic and dramatic landscape of Africa, the film counterpoints uncommon natural beauty with the universal challenges of a developing human relationship.

==Release==
The film was originally screened for the public in April 2010 at the New York International Independent Film and Video Festival on July 29, 2010 in New York City. It was also screened at the Downtown Film Festival in Los Angeles in September, 2010. DVD release is not yet set.

==Reviews==
The film was screened for the members of the Royal Geographic Society (RGS) Overland Workshop in York, UK, on May 28, 2010. In his review, Martin Solms of The Africa Overland Network wrote "Adventuress Wanted will motivate the traveler within, inspiring you to dream large and to see your dreams fulfilled." 

==Awards==
New York International Independent Film and Video Festival 2010 - Best Directorial Debut (Feature Documentary) to Thomas McAlevey  

== References ==
 

==External links==
*  
*  

 
 
 
 
 
 