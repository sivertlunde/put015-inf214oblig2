Vishnulokam
{{Infobox film 
| name           = Vishnu Lokam
| image          =
| caption        = Kamal
| producer       = Sanal Kumar G Suresh Kumar
| writer         = TA Razak
| screenplay     = TA Razak Urvashi Shanthi Krishna Kaithapram
| music          = Raveendran Shankar Jaikishan
| cinematography = Saloo George
| editing        = K Rajagopal
| studio         = Sooryodaya Creations
| distributor    = Sooryodaya Creations
| released       =  
| country        = India Malayalam
}}
 1991 Cinema Indian Malayalam Malayalam film, Kamal and produced by Sanal Kumar and G Suresh Kumar. The film stars Mohanlal, Urvashi (actress)|Urvashi, Shanthi Krishna and Kaithapram in lead roles. The film had musical score by Raveendran and Shankar Jaikishan.   

==Cast==
 
*Mohanlal as Vishnu Urvashi
*Shanthi Krishna
*Kaithapram
*Sukumari
*Nedumudi Venu Murali
*Santhosh Santhosh
*Maniyanpilla Raju
*Krishnankutty Nair
*Aranmula Ponnamma
*Balan K Nair
*Bobby Kottarakkara
*Jagadish
*Kaveri
*Nayana
*Oduvil Unnikrishnan
*Shyama
*Soorya
 

==Soundtrack==
The music was composed by Raveendran and Shankar Jaikishan and lyrics was written by Kaithapram and Hasrat Jaipuri. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aadyavasanthame || MG Sreekumar || Kaithapram || 
|-
| 2 || Aadyavasanthame || KS Chithra || Kaithapram || 
|-
| 3 || Awaaraa hoon || Mohanlal || Hasrat Jaipuri || 
|-
| 4 || Kasthoori ente kasthoori || MG Sreekumar, Sujatha Mohan || Kaithapram || 
|-
| 5 || Mindaathathenthe || MG Sreekumar || Kaithapram || 
|-
| 6 || Paanappuzha || Malaysia Vasudevan || Kaithapram || 
|}

==References==
 

==External links==
*  

 
 
 

 