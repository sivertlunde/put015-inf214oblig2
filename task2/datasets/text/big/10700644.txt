Sagina (film)
{{Infobox Film
| name           = Sagina
| image          = 
| image_size     = 
| caption        = 
| director       =Tapan Sinha
| producer       =J.K. Kapur
| writer         = Tapan Sinha (screenplay) Rahi Masoom Reza (dialogue)
|story= Rupadarshi
  narrator       = Dilip Kumar
| starring       = Dilip Kumar Saira Banu Aparna Sen music          = S. D. Burman
| cinematography = Bimal Mukherjee
| editing        = Subodh Roy
| distributor    = 
| released       = 1974
| runtime        = 
| country        = India Hindi
| budget         = 
}}

Sagina is a 1974 Hindi movie, produced by J.K. Kapur and directed by Tapan Sinha, the film stars Dilip Kumar, Saira Banu, Aparna Sen and Om Prakash. It was a remake of a Bengali version called Sagina Mahato which  was released in 1970, directed by Tapan Sinha, with the same lead pair in the cast.   

==Cast==
* Dilip Kumar as Sagina Mahato
* Saira Banu as Lalita
* Aparna Sen as Secretary Vishaka Devi
* Anil Chatterjee as Aniruddha
* Chinmoy Roy
* Rajni Gupta 
* Kalyan Chatterjee
* Bhanu Bannerjee
* Kader Khan as Anupam Dutt
* K.N. Singh as Factory owner
* Om Prakash as Guru

==Music==

The film has music by  S.D. Burman and lyrics by Majrooh Sultanpuri, and is also famous for one of its song Saala mein to sahab ban gaya, sahab ban ke kaisa tan gaya sung by Kishore Kumar and picturised on Dilip Kumar. Other numbers include, Uparwala dukhiyon ki nahi sunta tha  by  Kishore Kumar, Gajab chamki bindiya tori aadhi raat  by Lata Mangeshkar and Kishore Kumar and Tumre sang to rain bitayee sung by Lata Mangeshkar and Kishore Kumar. 

==Awards and legacy==
The film won the 1974 Filmfare Best Art Direction Award for Sudhendu Roy. In the 1975 film Chupke Chupke a billboard featuring the poster can briefly be seen.

== References ==

 

== External links ==
*  

 
 
 
 
 
 


 