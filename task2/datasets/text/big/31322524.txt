Story of A Young Couple
{{Infobox film
| name           =story of a young couple
| image          = 
| image size     =
| caption        =
| director       = Kurt Maetzig
| producer       =Alexander Lösche
| writer         = Bodo Uhse
| narrator       =
| starring       =Yvonne Merin, Hans-Peter Thielen
| music          = Wilhelm Neef
| cinematography = Karl Plintzner
| editing        =Lena Neumann
| distributor    = PROGRESS-Film Verleih
| released       = 18 January 1952
| runtime        = 104 minutes
| country        = East Germany German
| budget         =
| gross          =
| preceded by    =
| followed by    =
}} East German film, directed by Kurt Maetzig. It was released in List of East German films|1952. 

==Plot==
Agnes and Jochen are two young actors who meet and fall in love while appearing in a Berlin production of Nathan the Wise. After the two marry, Agnes is drawn to the communist cause, and begins acting in East German films, which her husband views as sheer propaganda, especially when she recites a poem praising Stalin. When Jochen decides to accept a role in Les Mains Sales, his wife cannot bring herself to follow him, viewing the play as seditious. They decide to divorce. Jochen becomes a celebrated star in the West, but slowly realizes that not all is well: he sees that former influential Nazis are rehabilitated. After witnessing an anti-war demonstration brutally dispersed by the police, he arrives in the divorce court and asks Agnes to reconcile with him. The two reunite and move to East Berlin.

==Cast==
* Yvonne Merin: Agnes Sailer
* Hans-Peter Thielen: Jochen Karsten
* Willy A. Kleinau: Dr. Ulrich Plisch
* Hilde Sessak: Carla
* Martin Hellberg: Möbius
* Hanns Groth: Lutz Frank
* Alfons Mühlhofer: Ernst Winkler
* Horst Preusker: Jonas
* Waltraud Kogel: Asttrid Kern
* Albert Grabe: Otto Dulz
* Brigitte Krause: Brigitte Dulz
* Gisela Rimpler: Felicitas Bach
* Alwin Lippisch: Hartmann (Veit Harlan)

==Production==
The script was written by Maetzig and author  , 13 February 1952.  

==Reception==
On 22 January 1952, a Berliner Zeitung columnist wrote that "the film makes us sit up and take notice - and we are pleased to see a film with such sound, humane groundwork".  The West German Der Spiegel commented that "Maetzigs work was too progressive even for the party hard-liners" and quoted  DEFA official Albert Wilkening who disapproved of the picture, saying that "unfortunately, there is still much rigidity in our film industry".  

Seán Allan and John Sandford wrote that the film presented a confrontation between the morally-superior Socialist system in East Germany and the barely de-nazified, corrupt one of West Germany, in a manner common in pictures from the Democratic Republic.  Peter C. Rollins and John E. OConnor described Story of A Young Couple as "painfully true to the party line";  according to Alexander Stephan, it also contained anti-American rhetoric typical to the time.  Anke Pinkert characterized the picture as one of DEFAs most important "woman films", in which the figure of an emancipated female was the main driver of the plot and played an important role in bringing about social change.  John Griffith Urang noted that rather than have love transcend politics, as was the case in Maetzigs Marriage in the Shadows, Story of A Young Couple had the protagonists romantic relations depend on their world view.   

==References==
 
 
==External links==
*  
*  on PROGRESS website.
*  on filmpotal.de.
*  original poster on ostfilm.de.
*  on film-zeit.de. 
 

 
 
 
 
 