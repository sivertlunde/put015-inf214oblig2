God Speed You! Black Emperor
 
{{multiple issues|
 
 
}}

{{Infobox film
| name           = God Speed You! Black Emperor
| image          = God Speed You!.jpg
| image_size     =
| caption        = DVD cover
| director       = Mitsuo Yanagimachi
| producer       = Mitsuo Yanagimachi
| writer         =
| narrator       =
| starring       =
| music          =
| cinematography = Taro Akashi Katsutoshi Iwanaga Makoto Sugiura Kimio Tsukamoto Yoshibumi Yokoyama
| editing        =
| distributor    =
| released       =  
| runtime        = 90 minutes
| country        = Japan
| language       = Japanese
| budget         =
| website        =
}}
  is a 1976 Japanese black-and-white 16&nbsp;mm documentary film, 90 minutes long, by director Mitsuo Yanagimachi, which follows the exploits of young Japanese motorcyclists, the "Black Emperors". The 1970s in Japan saw the rise of a motorcycling movement called the bōsōzoku, which drew the interest of the media. The movie follows a member of the "Black Emperors" motorcycle club and his interaction with his parents after he gets in trouble with the police.

The Canadian post-rock band Godspeed You! Black Emperor took their name from the film.

==External links==
* 
*    

 

 
 
 
 
 
 


 
 