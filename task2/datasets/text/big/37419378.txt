Salaakhen (1998 film)
{{Infobox film
| name           = Salaakhein
| image          = Salaakhen (1998 film).jpg 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Guddu Dhanoa
| producer       = Guddu Dhanoa	 Rajiv Tolani
| writer         = Dilip Shukla	
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Sunny Deol Raveena Tandon
| music          = Dilip Sen-Sameer Sen
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
}}
 Indian Bollywood film directed by Guddu Dhanoa. It stars  Sunny Deol and Raveena Tandon in pivotal roles. 
 

==Plot==
Kandivli-based Sachidanand Agnihotri is a honest school-teacher, who has always faced problems due to his honesty, first with Poornima School; then in a Government school, and now he has come forward to testify against the sexual assault and murder of a young woman from Worli. Sachidanand points an accusing finger at Nagesh, the son of prominent and influential Jaspal Rana. Sachidanands family, which consists of his wife, Gayetri, and son, Vishal, attempt in vain to talk him out of testifying. The Police are forced to arrest Nagesh and hold him in a cell. Then the Police, headed by a corrupt Assistant Commissioner of Police, Kamble, attempt to intimidate Sachidanand and follow him everywhere on the pretext of protecting him. Sachidanand is literally driven out of his mind, and on the day of the testimony, Nageshs lawyer, Ashok Pradhan, confuses him to such an extent that Sachidanand kills himself. Vishal, who is being held in prison for assaulting several policemen, breaks out, abducts Nagesh, and then kills Ashok Pradhan. When Kamble comes to Nageshs rescue, Vishal kills both of them. He then hunts down Jaspal and eventually kills him. On the run from the Police, he recruits prominent advocate, Khurana, who defends him and gets him acquitted. After the acquittal, instead of thanking his lawyer, Vishal surprisingly kills him in open court. The question remains - what prevailed upon Vishal to kill his very own attorney - who had fought so hard to get his acquittal? 

==Cast==
* Sunny Deol...Vishal S. Agnihotri
* Raveena Tandon...Neha G. Rao
* Anupam Kher...Sachidanand Agnihotri
* Farida Jalal...Gayetri S. Agnihotri
* Amrish Puri...Jaspal Rana
* Mohan Joshi....Advocate Ashok Pradhan
* Deven Verma....Giri Rao
* Dinesh Hingoo...Badri
* Manisha Koirala...Dancer / Singer
* Harish Patel...Bhutaneshwar

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #a
|-
| 1
| "Dhak Dhak"
| Abhijeet Bhattacharya|Abhijeet, Shweta Shetty
|-
| 2
| "Pichhu Pade Hai"
| Shweta Shetty
|-
| 3
| "Pagal Diwana Awara" Chithra 
|-
| 4
| "Zubaan Pe Jo Nahin Aaye"
| Udit Narayan, Chithra
|-
| 5
| "Punajbi Kudi Maare Jhatke"
| Lalit Sen, Hema Sardesai
|-
| 6
| "Pagal Karna Pagal Hona"
| Udit Narayan, Anuradha Paudwal
|-
| 7
| "Dil Mera Le Gayee"
| Kumar Sanu, Alka Yagnik
|}

==References==
 

==External links==
* 

 
 
 

 