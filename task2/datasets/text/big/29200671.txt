Mannequin in Red
{{Infobox film
| name           = Mannequin in Red
| image          = MannequinInRed1958Cover.jpg
| caption        = Swedish DVD cover
| director       = Arne Mattsson Sandrews
| writer         = Folke Mellvig (novel and screenplay), Lars Widding (writer)
| screenplay     = 
| story          = 
| based on       =  
| starring       = Karl-Arne Holmsten Annalisa Ericson Anita Björk Lillebil Ibsen Nils Hallberg
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 108 minutes
| country        = Sweden
| language       = Swedish
| budget         = 
| gross          = 
}} Swedish crime/thriller thriller film directed by Arne Mattson and written by Folke Mellvig. 

Starring film couple Karl-Arne Holmsten and Annalisa Ericson as the investigating detective couple John and Kajsa Hillman; this time investigating a murder of a model connected to a famous Stockholm fashion house, La Femme.
 Lady in White (1962), and The Yellow Car (1963).

==Cast==  
*Karl-Arne Holmsten as John Hillman
*Annalisa Ericson as Kajsa Hillman
*Anita Björk as Birgitta Lindell
*Lillebil Ibsen as Thyra Lennberg
*Gio Petré as Gabrielle von Hook
*Nils Hallberg as Freddy Sjöström
*Lena Granhagen as Sonja Svensson
*Bengt Brunskog as Johan Robert Bobbie Nordahl
*Lennart Lindberg as Rickard von Hook
*Kotti Chave as Sune Öhrgren, police inspector
*Lissi Alandh as Peter Morell
*Anita Lindblom as Monika, model in the fashion house

==External links==
 

 
 
 
 
 
 