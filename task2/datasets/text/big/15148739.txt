One Terrible Day
{{Infobox film
| name           = One Terrible Day
| image          = Scene_From_One_Terrible_Day.jpg
| caption        = Scene from the film Tom McNamara
| producer       = Hal Roach
| writer         = Hal Roach H. M. Walker Tom McNamara Jack Davis William Gillespie Charles Stevenson
| music          =
| cinematography =
| editing        =
| distributor    = Pathé
| released       =  
| runtime        = 20 minutes
| country        = United States
| language       = Silent film English intertitles
| budget         =
}} silent short Tom McNamara, the two-reel short was released to theaters on September 10, 1922 by Pathé.  It is a comedic film about a group of children from the poor section of town being taken by a wealthy matron to her country estate.  The party is crashed as more kids than were invited show up, along with their dog, and stow away in the limousine.  When they arrive at the country estate, the boys proceed to make mayhem and wreak general havoc.

This was the first  , who makes only a brief appearance in the company of an unnamed character at the country estate.
 Buckwheat character years later.

When the television rights for the original silent Pathé Our Gang comedies were sold to National Telepix and other distributors, several of the Our Gang films were released into TV syndication and retitled. In 1960 the collective title "Our Gang" was changed to "Mischief Makers" and the title of "One Terrible Day" was changed to "The Outing."  About two-thirds of the original footage from the film was included. Most of the original inter-titles were also cut and later replaced. The film, having been produced prior to 1923, and never having the copyright renewed, is in the public domain. However, the "Mischief Makers" additions to the film were copyrighted in 1960.

==Plot== Jack Davis, Booker T. Farina in Jackie that William Gillespie), who gets doused with water when the monkey finds a garden hose and turns it on him.  When things finally settle down, Mrs. Van Renssalaer, Alvira, the chauffeur, and the five boys get into the limousine. Unbeknownst to them, however, Booker and Jackie stow away on the tailgate with Jackie’s dog sitting beside them, and Farina being pulled along in her little toy wagon.

After they get going, Mickey starts flying his kite behind the car, but Mrs. Van Renssalaer’s monkey grabs the kite’s tail and is hoisted up into the sky.  When the string breaks, the monkey falls.  The chauffeur has to get out of the car and rescue the monkey and when he returns, he discovers the stowaways in the rear of the limousine.  Mrs. Van Renssalaer tells him to let all the children and the dog ride in the car.  After they get going again, the limousine has a blowout.  While the chauffeur is changing the tire, the kids start playing with the unused tire.  When the chauffeur gets his tire back, he tries to replace it on the car, but the kids continue to pester him.  He picks one child up and sets him aside, only to find another child standing there.  He continues to set the children aside, one at a time, not noticing that the children are tricking him by running around to the end of the line to be lifted aside again.  Finally, the chauffeur gets the tire replaced and tells the children to get into the car.  But the kids trick him again.  As they get in the car, they run across and exit the car on the other side and get back in line to get back into the car.  The chauffeur catches on and makes all the children get in and sit down.  

They finally arrive at the country estate and the kids play for a while in the house, making a mess of things and breaking a lamp, until Mrs. Van Renssalaer drives them outside where they entertain themselves by torturing the animals and swimming in the fountain in their long-handles, only to be interrupted when Peggy Cartwright comes riding up on a mule. They can’t get dressed because the monkey has stolen their clothes, so they hide in the barn. Peggy goes into the house and the boys get dressed and come out of the barn.  They then decide to become bullfighters, but get scared and wind up cowering in a tree, only to be ridiculed by Peggy, who explains that the ‘’bull’’ is actually a cow. 
 Charles Stevenson, tells the kids to come into the house for dinner, they sit down at the table.  Meanwhile, the monkey steals Mrs. Van Renssalaers pearls and climbs up onto the chandelier. The boys climb up on the table to get it down, ruining the meal.  The gang chases the monkey all over the house. The monkey gets away and stashes the pearls on Farina.  It’s finally time to go home, and they all pile back into the car. But before they get back to town, they have another blowout, bringing the show to an end.

==The Cast==
 

===The Kids===
  riding Dinah the Mule in One Terrible Day]]
  in One Terrible Day]]
  in One Terrible Day]]
  in One Terrible Day]]
  in One Terrible Day]]
  as Farina in One Terrible Day, riding her toy wagon]]
  in One Terrible Day]]
 

* Peggy Cartwright – Her first appearance in the film is when she is seen riding up on Dinah the Mule while the boys are playing in the fountain in their undies.  Later she has to explain the difference between a cow and a bull to the boys.

* Jackie Condon – Jackie is the little boy with the wild hair and is accompanied by the large dog, probably a Golden Retriever.  He does not have an invitation, so he stows away in the back of the limousine.  At the country estate, he teases a horse by throwing water in the horse’s face.  Later the horse gets revenge by pushing him in the water.

* Mickey Daniels – The freckle faced little boy who is the first to arrive at Mrs. Van Renssalaer’s mansion with an invitation.  He seems to be the leader of the gang and most of the story revolves around him.  He and Ernie are the ones who later aspire to be bull fighters.
 Jack Davis – Jack is the second boy to arrive at the mansion, although his invitation is folded into a paper airplane.  He is the one who comes up with the idea of using the fountain for a swimming pool.

* Weston Doty – The Doty twins get their best exposure when they present their invitations to Alvira.  Their speech and gestures are all done in unison.
* Winston Doty – Winston and Weston are ensemble actors throughout the film.

* Allen Hoskins – Farina – Allen plays the part of Booker T.’s little sister, a little African American child, first seen riding in a toy wagon.  During the scene where the chauffeur is changing the tire, she hides under the car.  Later, she and Jackie try fishing in the fountain and Jackie uses one of her pigtails for bait.

* Ernie Morrison – Booker T. Bacon - Ernie is the African American who does not have an invitation and convinces Jackie that they can still go if they stow away in the back of the limousine.  During the scene where the chauffeur is changing the tire, he convinces the other boys to let him get inside the tire and then roll him down the hill.  Later he and Mickey try their hands at bull fighting, not knowing the difference between a cow and a bull.

* Unknown Child Actor - plays the tall boy wearing glasses, although it is an ensemble part throughout the film.  When he arrives at the mansion, he has a snooty attitude, which causes Mickey to take great delight in making fun of him.  As the story progresses, however, he does loosen up a bit and plays well with the other children.

===The Adults===
* Ed Brandenburg – the cook – He has a small part as the operator of a sidewalk café where the monkey lands after falling from the kite. William Gillespie – the chauffeur who chases the monkey and gets squirted with the garden hose.  He is tormented by the boys when they continue to pester him while he is changing the tire.  When Farina is found hiding under the car, he crawls under the car to retrieve her, but the monkey gets into the driver’s seat, and pulls a lever, which spills oil on the chauffeur’s face.
* Helen Gilmore – Caroline Culpepper – Society Reporter for "The Tattler".  She shows Mrs. Van Renssalaer an article about a rival socialites party.  Her long haired cat also tangles with the monkey.  She is listed by the Internet Movie Data Base and The Lucky Corner as Carlene Culpepper.  However, her business card has the name printed as Caroline Culpepper.
* Clara Guiol – Alvira, Mrs. Van Rensselaer’s secretary.  She is seen receiving the boys at the mansion and accepting their invitations.  She is affronted when Mrs. Van Rensselaer’s monkey mimics her with reading glasses.  Throughout the film, she and Mrs. Van Rensselaer react to the children’s antics with snooty snobbishness.
* Wallace Howe – The Policeman who stops the limousine at the intersection just before the car has a blowout. Charles Stevenson – The Butler at the country estate.
* Fanny Kelly – Mrs. Pennington Van Renssalaer, who invites the local children on the outing to her country estate.  She has a pet monkey and is the butt of many of the sight jokes because of her wealthy snootiness.
* Unknown actress - the maid (caucasian) - She is seen applying beauty cream to Mrs. Van Renssalaers neck during the opening scene of the film.  She later carries the monkey into the kitchen.
* Unknown actress - the maid (African-American) - She appears briefly and gets a face full of beauty cream courtesy of Caroline Culpeppers cat.
* Unknown actress - Lady seen with Peggy Cartwright outside of the country estate.
* Unknown actor - Man riding in the front seat of the limousine next to the chauffeur.

===The Animals===
* Dinah the Mule, ridden by Peggy Cartwright|Peggy, when she almost discovers the boys swimming in their underwear. Jackie – Because of its size the dog causes considerable grief to the wealthy matron and her secretary while riding in the limousine.
* Monkey, owned by the wealthy matron – The monkey offends Mrs. Van Renssalaer’s secretary and douses the butler with water.  Later, the monkey is hoisted into the air when it grabs hold of the tail of Mickey’s kite.
* Cat, belonging to Caroline Culpepper – this long haired feline climbs the drapes in Mrs. Van Renssalaers house and ultimately ends up slinging a big glob of beauty cream into a housemaids face.
* Various farm animals – The kids are seen tormenting the goats by pulling their tails, throwing water in the horse’s face, chasing the turkeys and geese, and playing bull fighter with the cow.

===Questionable listings===
* Mary Kornman, listed by Internet Movie Database.  However, according to The Lucky Corner, she does not actually appear in the film.
* Lincoln Stedman, also listed by Maltin and Bann as well as Internet Movie Database as the secretary.  However, according to The Lucky Corner, he does not actually appear in the film.

==The Crew==
* Produced by Hal Roach
* Supervised by Charles Parrott, (Charley Chase) Tom McNamara Tom McNamara Tom McNamara
* Distributed by Pathé

==Sequence==
Succeeding Short: Fire Fighters (film) (1922)

==Production== Roach Studios Our Gang, Fire Fighters Roach showed Roach decided to release the film to the public first.  Consequently, One Terrible Day was the fourth Our Gang film to enter production, but the first to be released.

==Reception==
According to Maltin and Bann, The New York Post’s critic column said, “The best thing on the Rivoli Theater program this week is the funniest comedy shown for some time, One Terrible Day, bringing forth screams of laughter.”  The New York Herald said, “The outstanding hit of the bill at the Rivoli is a rollicking Pathé comedy, One Terrible Day.”  The New York World also commented by calling the film “extremely funny.” 

==Technical details==
*One Terrible Day was designated at Roach Studios as Production A-4 and was filmed from May 22 to June 10, 1922.  It was released on September 10, 1922 and was the first Our Gang film in the series to be released.
*The original copyright was recorded on October 9, 1922, by Pathé Exchange, Inc.  Registration no. LU18286.
*The film was a silent two-reeler, approximately 20 minutes in length.  Subsequent editing has reduced the film to about 15 minutes.
*Extant lobby posters indicate that the original opening title card probably read:  "Our Gang" Comedies - Hal Roach presents His Rascals in One Terrible Day.  The title of some of the early Our Gang films read "Our Gang" Comedy or "Our Gang" Series.
*The film was released into TV syndication as Mischief Makers episode no. 1043, "The Outing," copyrighted Sep. 1, 1960, with registration number LP17348.

==References==
;Notes
 

;Bibliography
* Maltin, Leonard and Bann, Richard W. (1977, rev. 1978). Our Gang: The Life and Times of The Little Rascals. New York: Crown Publishing/Three Rivers Press. ISBN 0-517-52675-1
* http://theluckycorner.com/rps/004.html

== External links ==
 
*  
*  

 
 
 
 
 
 
 
 
 