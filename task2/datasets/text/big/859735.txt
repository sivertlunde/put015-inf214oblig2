Some Folks Call It a Sling Blade
{{Infobox film
|name=Some Folks Call It a Sling Blade
|director=George Hickenlooper
|producer=George Hickenlooper
|writer=Billy Bob Thornton
|starring=Billy Bob Thornton Molly Ringwald J. T. Walsh
|music=Bill Boll
|cinematography=Kent L. Wakeford
|editing=Henny Bouwmeester George Hickenlooper
|released=1994
|runtime=29 minutes
|country=United States English
}}

Some Folks Call It a Sling Blade is a short film written by Billy Bob Thornton, directed by George Hickenlooper and starring Thornton, Molly Ringwald, and J. T. Walsh. 
 Academy Award Best Adapted Screenplay, as well as a nomination for Best Actor in a Leading Role. 

==Overview== mentally retarded mental hospital for the past 25 years for murdering his mother and her lover. On the day of his release, he is interviewed by a reporter, Theresa Tatum (Molly Ringwald), who is writing her article with the intent of examining whether criminals judged to be insane should be released. Before the interview, Tatum is of the opinion that criminals like Childers should never be released. During the interview, however, Tatum must question her previous beliefs in this matter.

The title of the film comes from Childerss description of the murders. He admits to committing murder with a "sling blade, some folks call it a sling blade, I call it a Kaiser blade."

==References==
 

==External links==
* 

 

 
 
 
 
 

 