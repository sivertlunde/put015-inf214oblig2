Endgame (2009 film)
{{Infobox film
| name           = Endgame
| image          = Endgame film.jpg
| caption        = Theatrical release poster
| director       = Pete Travis
| producer       = Hal Vogel
| writer         = Paula Milne
| starring       = William Hurt Chiwetel Ejiofor Jonny Lee Miller Mark Strong
| music          = Martin Phipps
| cinematography = David Odd
| editing        = Clive Barrett Dominic Strevens
| distributor    = Target Entertainment
| released       =  
| runtime        = 101 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}} Robert Harvey. Vantage Point actor William Hurt. It also stars Chiwetel Ejiofor, Jonny Lee Miller and Mark Strong. The film dramatises the final days of apartheid in South Africa. It was filmed at locations in Reading, Berkshire, England and Cape Town, South Africa in the first half of 2008 and was completed in December that year. 

The film had its world premiere on 18 January 2009 at the Sundance Film Festival and was broadcast on Channel 4 on 4 May 2009. It will also have an international theatrical release, the distribution of which is handled by Target Entertainment Group.

==Premise==
The film depicts the final days of   and Thabo Mbeki.   
 Michael Young, a British businessman who worked for Consolidated Gold Fields, a firm with considerable interests in South Africa. The talks took place in Mells Park House, a country house near Frome in Somerset. The house was then owned by Consolidated Gold Fields.

Consolidated Goldfields was a company with interests in South Africa which is the subject of sanctions by other nations. In one scene, Young and Rudolf Agnew, chairman of Consolidated Goldfields, leave their offices in London and are mobbed by anti-apartheid protesters who batter and chase their car, unaware that the two men are sponsoring the very talks that are leading to the end of the system they oppose.

In an interview on BBC Radio 4s Today programme on 24 April 2009, Michael Young mentioned how he had been asked by Thabo Mbeki to write the final chapter of the 2003 book by Robert Harvey on the Fall of Apartheid - the chapter titled "Endgame" - on which this film is based.  

==Cast==
* William Hurt as Willie Esterhuyse, professor of philosophy at Stellenbosch University
* Chiwetel Ejiofor as Thabo Mbeki, director of information for the African National Congress Michael Young, director of communications for Consolidated Gold Fields Dr Niel National Intelligence Service
* Derek Jacobi as Rudolf Agnew, chairman of Consolidated Gold Fields
* Timothy West as P.W. Botha, State President of South Africa
* Ramon Tikaram as Aziz Pahad, international arm of ANC
* Clarke Peters as Nelson Mandela
* John Kani as Oliver Tambo, the former President of the African National Congress

==Production== The Government Inspector. 
 Vantage Point (2008), was sent the script by Milne. Travis was not interested in directing a historical drama about recent events and decided to turn the film into a political thriller. Kemp, Stuart (4 November 2008). " ". The Hollywood Reporter (Nielsen Business Media).  William Hurt and Chiwetel Ejiofor were first to be cast.  Hurt, who played President Henry Ashton in Vantage Point, was cast as Will Esterhuyse because Travis wanted to cast actors he had worked with before.  Other actors were interested in the part even after Hurt had signed on.  Travis wanted to work with Ejiofor, who was his first choice for the part of Thabo Mbeki. 

  composed the film soundtrack.  The final cut of the film was completed on 24 December 2008. 

==Release==
Target Entertainment sold the international theatrical distribution rights in 2008 at the   on Public Broadcasting Service|PBS.   This was followed by a theatrical release on October 30 through Monterey Media in select U.S. cities. 

==Reception==
Overnight ratings indicated that Endgame s first Channel 4 broadcast was seen by 837,000 viewers (a 3.9% audience share). 64,000 more watched on Channel 4s one-hour timeshift service, Channel 4+1.  A repeat on the evening of 9 May got 336,000 viewers (1.7% share) on Channel 4 and 35,000 on Channel 4+1. 

The films reception was positive. Review aggregator Rotten Tomatoes reports that 71% of professional critics gave the film a positive review, with a rating average of 6.5 out of 10.  The Daily Telegraph praised Lee Millers performance but argued that "the elements never quite cohered". The newspaper concluded that the script "seemed too fuzzy in its focus, and also too eager to write history with an unambiguously broad brush."  Other publications praised the film. In contrast with the Telegraph, The Independent praised the script "Paula Milnes script skilfully interspersed talk with action".  The Times rated the film four out of five stars.  It also won a Peabody Award in 2009. 

==References==
 

==Further reading==
* Travis, Pete (19 March 2009). " ". Moving Pictures Magazine.

==External links==
* Official website was http://www.endgame-themovie.com/ but now seems unregistered.
* , official PBS website
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 