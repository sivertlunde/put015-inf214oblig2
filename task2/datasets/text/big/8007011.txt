Money (1991 film)
 
{{Infobox Film |
 name = Money |
 image =Money film 91.jpg|
 caption = Money original DVD Cover|
 writer = Larry Pederson, Gordon Roback |
 starring = Eric Stoltz, Christopher Plummer, Maryam dAbo |
 director = Steven Hilliard Stern |
 producer = André Djaoui, René Malo |
 composer = Ennio Morricone |
 distributor = United International Pictures |
 released = 1991 |
 runtime = 105 minutes |
 language = English |
 budget = |
}}

Money is a 1991 drama film directed by Steven Hilliard Stern.

==Plot==
Frank Cimballi (Eric Stoltz) is a rich 21-year-old who goes to claim his inheritance only to find it has been embezzled by his fathers former business partners. Traveling the globe in search of the white-collar thieves who have robbed him of millions, Frank locates his fathers seriously ill associate Will Scarlet (F. Murray Abraham), who admits to his role in the crime and agrees to help Frank track down the rest of the men on his revenge list.

==Cast==
* Eric Stoltz - Frank Cimballi
* Maryam dAbo - Sarah Wilkins
* Bruno Cremer - Marc Lavater
* Mario Adorf - The Turk
* Anna Kanakis - Anna Lupino
* F. Murray Abraham - Will Scarlett
* Christopher Plummer - Martin Yahl
* Bernard Fresson - Henry Landau
* Angelo Infanti - Romano
* Tomas Milian - Robert Zara
* Martin Lamotte

==External links==
* 

 
 
 
 
 

 