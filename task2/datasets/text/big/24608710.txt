Manu Uncle
{{Infobox film
| name = Manu Uncle
| image = ManuUnclefilm.png
| caption = Poster designed by Gayathri Ashokan
| director = Dennis Joseph
| producer = Joy Thomas
| screenplay =   Shibu Chakraborthy
| story = Dennis Joseph
| starring = Mammootty Master Sreenivasan G Master Anoop Master Sandeep Master Kareem Baby Sonia M. G. Soman Prathapachandran Suresh Gopi  (cameo)   Mohanlal  (cameo)  Shyam
| cinematography = Jayanan Vincent
| editing = K. Shankunny
| distributor = Jubilee Productions
| released =   
| runtime =
| country = India
| language = Malayalam
| budget =
}}
 National Award and a Kerala State Film Award. 

==Plot==
Mammootty plays the lead role Manu, M.A., L.L.B., BTech Electronics who is researching in astronomy. Manus father (M. G. Soman) is a police officer. Lisi plays the role of Manus sister Uma. Prathapachandran plays Ravunni, a family friend. Antony (Azeez) and Mary (K. P. A. C. Lalitha) are neighbours of Manu. Their children are Dany aka Lother and Ikru.  Manus elder sister (Jalaja)s children comes from Delhi for their vacation. They with Dany and Ikru make up a four member team headed by Dany.

The children happened to visit the Art Museum on the very same time when the Marthandavarmas crown was stolen. Ikru noticed the robbers, Gomas, Appu and Kittu but was not able to describe them to his grandpa. Manu who has a well equipped communication system once happened to hear a conversation between Kittu and Gomas planning to loot Marthandavarmass sword. Manu and his father rush to the palace. Gomas meanwhile already gets hold of the sword when he encounters Manu and his dad. A fight follows in which Manu is injured by Gomas; so now Manus father follows the robbers alone. But the robbers kill the police officer in a planned lorry accident. The viewers are just now able to know who the mastermind behind the robbery is; Ravunni, the family friend.

Unfortunately for him though, the incident was witnessed by Khader, a young orphan who was happened to be in that lorry. The robbers see the boy and go after him. The boy manages to escape from them and in the process ends up meeting the four children, and tell them that their grandpa was not dead in an accident but was killed.

The curious children decide to investigate the murder. Ikru who had already met Gomas drew his picture which was noticed by Manu and Ravunni. Ravunni gets shocked and Manu started developing  doubts about Ravunni after watching his tensed face. Manu goes to Ravunnis house when he was not their and made sure his doubts where true.

The children meanwhile decide to go to the gangs hideout as per the directions from Khader. They get shocked on knowing that it was Ravunni leading the gang. The gang sees the children who crossed their fence and Ravunni directs his cronies to kill them. But, soon Manu reaches there and saves the children. The climax scene has a very funny fight scene which was enhanced by the funny inspector role of Suresh Gopi as Minnal Prathapan. The movie ends when the robber gang gets arrested by the police.

Mohanlal and Suresh Gopi had special appearance in the film. Mohanlal played the role of himself and Suresh Gopi as SI Minnal Prathapan, a comic inspector.

==Cast==
*Mammootty as Manu
*Master Sreenivasan G as Dany aka Lother
*Master Kuriyachan as Ikru
*Master Sandeep as Shanku Linza as Thankamma
*Master Akhil Nambiath as Khader Lizzy as Uma
*M. G. Soman as DySP Sonia
*Azeez Azeez as Antony
*K. P. A. C. Lalitha as Mary
*Prathapachandran as Ravunni Menon
*Thiagarajan|B. Thyagarajan as Gomas
*Murali Menon as Kittu
*Mohan Jose as Security guard of Ravunni Menon
*Jalaja as Manus elder sister
Special appearances
*Mohanlal as himself
*Suresh Gopi as SI Minnal Prathapan (Jagathy Sreekumar was originally supposed to play this role, but he had other commitments at the time.)

==Crew==
*Story,Direction ..... Dennis Joseph
*Screenplay, Dialogues and Lyrics .... Shibu Chakraborthy
*Producer .... Joy Thomas
*Distribution .... Jubilee Productions
*Cinematography .... Jayanan Vincent
*Editor .... K. Shankunny
*Stunt .... A. R. Basha
*Art Direction .... Sabu Pravadas
*Associate direction .... Jimmy
*P. R. O. .... Ranji Kottayam
*Stills .... R. Sukumar
*Make up .... Thomas
*Costume Design .... Kumar

==Awards==
* National Film Award for Best Childrens Film 
* Kerala State Film Award for Best Childrens Film

==Soundtrack==
Composer: Shyam (composer)|Shyam; Lyricist: Shibu Chakraborhy
*"Mele Veettile" .... K. S. Chithra
*"Oru Kili" .... K. S. Chithra & M. G. Sreekumar

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 