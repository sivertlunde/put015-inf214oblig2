The Staffan Stolle Story
 
{{Infobox film
| name           = The Staffan Stolle Story
| image          = 
| caption        = 
| director       = Hasse Ekman
| producer       = Felix Alvo
| writer         = Hasse Ekman
| starring       = Povel Ramel
| music          = 
| cinematography = Åke Dahlqvist
| editing        = 
| distributor    = 
| released       =  
| runtime        = 107 minutes
| country        = Sweden
| language       = Swedish
| budget         = 
}}
 musical comedy Best Foreign Language Film at the 29th Academy Awards, but was not accepted as a nominee.  The film stars Povel Ramel in a leading role, and features many scenes where Ramel performs musical numbers.

==Cast==
* Povel Ramel as Staffan Stolle
* Martin Ljung as Vicke Wickberg
* Gunwer Bergkvist as Tipsie Blink
* Yvonne Lombard as Charlotte Nibbing
* Hasse Ekman as Klad Traenger
* Sigge Fürst as Överste Nibbing
* Georg Funkquist as Fabiansson (as Georg Funkqvist)
* Siv Ericks as Fröken Lefverhielm
* Stig Järrel as Ulf Christer Lefverhielm
* Oscar Rundqvist as Sjungande badare

==See also==
* List of submissions to the 29th Academy Awards for Best Foreign Language Film
* List of Swedish submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 

 
 