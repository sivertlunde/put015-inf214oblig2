The Lonesome Chap
{{Infobox film
| name           = The Lonesome Chap
| image          =
| alt            = 
| caption        = 
| director       = Edward LeSaint
| producer       = 
| screenplay     = Harvey Gates Emma Rochelle Williams 
| starring       = Louise Huff House Peters, Sr. John Burton Eugene Pallette J. Parks Jones Pietro Buzzi
| music          = 
| cinematography = Allen M. Davey 	
| editing        = 
| studio         = Pallas Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 drama silent film directed by Edward LeSaint and written by Harvey Gates and Emma Rochelle Williams. The film stars Louise Huff, House Peters, Sr., John Burton, Eugene Pallette, J. Parks Jones and Pietro Buzzi. The film was released on April 19, 1917, by Paramount Pictures.  

==Plot==
 

== Cast ==
*Louise Huff as Renee DArmand
*House Peters, Sr. as Stuart Kirkwood
*John Burton as Doc Nelson
*Eugene Pallette as George Rothwell
*J. Parks Jones as George Rothwell Jr.
*Pietro Buzzi as Victor DArmand 
*Betty Johnson as Peggy Carter

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 

 