Bloodsport II: The Next Kumite
 
{{Infobox film name           = Bloodsport II: The Next Kumite image          = caption        = director       = Alan Mehrez producer       = Alan Mehrez writer         = Jeff Schechter narrator       = starring       = Daniel Bernhardt Pat Morita Donald Gibb James Hong music  Stephen Edwards cinematography = Jacques Haitkin editing        = J. Douglas Seelig studio         =F.M. Entertainment International N.V. distributor    =Transcontinental Film Corporation (VHS) Lions Gate Entertainment (DVD) released       = 1 March 1996 runtime        = 86 min. country        =  language       = English budget         =
}}
 
Bloodsport II: The Next Kumite is the sequel to the 1987 film Bloodsport (film)|Bloodsport. This sequel had a limited release before going direct-to-video in 1996 and starred Daniel Bernhardt, who reprised the role in a sequel.

==Plot== Iron Hand". When a "best of the best kumite" is to take place, Demon gets an invitation. Now Master Sun and Alex need to find a way to let Alex take part in the kumite, too.

The final fight of the movie pits Alex and Demon together.  At first, and for a long time, Demon has the upper hand in terms of strength and fighting ability.  When Alex is down, he takes one last look at Master Sun and uses the "Iron Hand" against his opponent, severely damaging and defeating Demon.  Alex is the winner, and as part of deals previously made in the movie, Master Sun is freed from prison, and so is Alex.

==Cast==
*Daniel Bernhardt – Alex Cardo
*Pat Morita – David Leung
*Donald Gibb – Ray Tiny Jackson
*James Hong – Master Sun
*Lori Lynn Dickerson – Janine Elson
*Ong Soo Han – Demon
*Philip Tan – John Nicholas Hill – Sergio DeSilva Ron Hall – Cliff
*Hee Il Cho – Head Judge
*Shaun Gordon – Suns Student
*Lisa McCullough – Kim Campbell
*Chuay – Chien
*Steve Martinex – Head Referee
*Jeff Wolfe – Flash
*Cliff Bernhardt – Len
*Nils Allen Stewart – Gorilla Eric Lee – Seng
*Kevin Chong – Suns Student
*Jerry Piddington – Kumite fighter
*Richard Kee Smith – Kumite fighter
*Gokor Chivichyan – Kumite fighter

==Series continuity== the first character from the first film to the second film. James Hong and Pat Morita appear in both Bloodsport II and Bloodsport III.

===Cameo=== novelist Kevin role in this film as Suns student.

==International titles==
*Brazil: O Grande Dragāo Branco: A Revanche (The Great White Dragon: The Revenge)
*Denmark: Bloodsport II: Ironhand
*Germany: Bloodsport II: Die Nächste Herausforderung (Bloodsport II: The Next Challenge)
*Italy: Colpi Proibiti 2
*Spain: Contacto Sangriento 2 (Bloody Contact 2)

==External links==
*  

 

 
 
 
 
 
 
 
 


 