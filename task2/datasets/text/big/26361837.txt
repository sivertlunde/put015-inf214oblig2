Housekeeping (film)
{{Infobox film
| name           = Housekeeping
| image          = Housekeeping 1988 Poster.jpg
| border         = 
| alt            = 
| caption        = Theatrical release poster
| director       = Bill Forsyth
| producer       = Robert F. Colesberry
| screenplay     = Bill Forsyth
| based on       =  
| starring       = {{Plainlist|
* Christine Lahti
* Sara Walker
* Andrea Burchill
}} Michael Gibbs
| cinematography = Michael Coulter
| editing        = Michael Ellis
| studio         = Columbia Pictures
| distributor    = Columbia Pictures
| released       =  
| runtime        = 117 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Housekeeping is a 1987 American comedy-drama film written and directed by Bill Forsyth and starring Christine Lahti, Sara Walker, and Andrea Burchill. Based on the 1980 novel Housekeeping (novel)|Housekeeping by Marilynne Robinson, the film is about two young sisters growing up in Idaho during the 1950s. After being abandoned by their mother and raised by elderly relatives, the sisters are looked after by their eccentric aunt whose unconventional and unpredictable ways affect their lives. Filmed on location in Alberta and British Columbia, Canada, Housekeeping won two awards at the 1987 Tokyo International Film Festival.   

==Plot==
Teen sisters Ruth and Lucille, raised by a grandmother after their mothers suicide, end up living with an aunt in a western U.S. town called Fingerbone after the grandmother dies.

Aunt Sylvie is an unusual woman. She likes to sit in the dark and sleep in the park. Others in town are never quite sure what to make of her. And the same holds true for the girls, even when Sylvie writes elaborate excuses to get them out of school.

==Cast==
* Christine Lahti as Sylvie
* Sara Walker as Ruth
* Andrea Burchill as Lucille
* Anne Pitoniak as Aunt Lily
* Barbara Reese as Aunt Nona
* Margot Pinvidic as Helen  
* Bill Smillie as Sheriff  
* Wayne Robson as Principal 
* Betty Phillips as Mrs. Jardine
* Karen Elizabeth Austin as Mrs. Paterson
* Dolores Drake as Mrs. Walker
* Georgie Collins as Grandmother
* Tonya Tanner as Young Ruth
* Leah Penny as Young Lucille

==Production==
Housekeeping was the first North American film by writer and director Bill Forsyth, whose previous films—That Sinking Feeling (1980), Gregorys Girl (1981), Local Hero (1983), and Comfort and Joy (1984)—were produced in Scotland.    Forsyths screenplay for the film is based on the 1980 novel Housekeeping (novel)|Housekeeping by Marilynne Robinson. It was in fact the prospect of bringing the novel to film that brought him to America. Forsyth would later describe his film version as an attempt to "make a commercial to get people to read the novel".   
 Castlegar and Nelson, British Columbia|Nelson, British Columbia, Canada.    Principal photography began on September 22, 1986 and ended in December 1986.   

==Release== Regus London Film Festival.    Housekeeping was released on VHS video in the United States on July 8, 1988. 

==Critical response==
In his review for The New York Times, Vincent Canby described the film as a "haunting comedy about impossible attachments and doomed affection in a world divided between two kinds of people"—those who pass aimlessly through life and embrace random existence, and those who seek to impose reason and order on random existence.    These two types are represented by the eccentric aunt Sylvie, who wanders aimlessly from town to town, and Lucille, who longs to establish familial roots and "live the way other people do".  The relationship between Lucille and her sister Ruth is transformed as the latter grows increasingly drawn to her aunts way of life. Canby notes that Forsyth is able to "make us care equally" for both sisters. 

Canby described Lahtis performance as "spellbinding" in this "role of her film career":
 
Canby is equally impressed with Sara Walkers portrayal of Ruth and Andrea Burchills Lucille, noting that "every member of the cast is special".  Canby concluded:
 

In his review for the Chicago Sun-Times, Roger Ebert gave the film four out of four stars, calling it "one of the strangest and best films of the year".    According to Ebert, Lahti was the perfect choice to play the central role of Sylvie.
 
Ebert concluded, "At the end of the film, I was quietly astonished. I had seen a film that could perhaps be described as being about a madwoman, but I had seen a character who seemed closer to a mystic, or a saint." 

On the AllMovie website, Housekeeping has an editorial rating of four and a half out of five stars.   

==Awards and nominations==
{| class="wikitable"
|- Award
!style="width:250px;"|Category Nominee
!style="width:50px;"|Result
|-
| New York Film Critics Circle Awards, 1987
| Best Actress
| Christine Lahti
|  
|- Tokyo International Film Festival, 1987
| Best Screenplay Award
| Bill Forsyth
|  
|-
| Special Jury Prize 
| Bill Forsyth
|  
|-
| Tokyo Grand Prix 
| Bill Forsyth
|  
|-
| USC Scripter Award, 1989
| USC Scripter Award
| Bill Forsyth, Marilynne Robinson 
|  
|}

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 