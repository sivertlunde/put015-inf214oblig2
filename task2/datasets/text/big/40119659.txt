The Clodhopper
The Charles Ray and Margery Wilson and directed by Victor Schertzinger.

==Plot==
Isaac Nelson (French) is the tight-fisted president of a country bank and owns a farm, where his son Everett (Ray) works long hours every day, even on Sundays. Everett wears his fathers cast-off clothes, and after his mother (Knott) buys him a mail order suit, Everett goes to a Fourth of July picnic. The father sees his wife doing on the farm doing the sons work and then trashes the son. Everett Nelson run off to the big city and gets a job as a janitor at a theater. There he meets a showman who puts him in a cabaret as a country dancer doing the "clodhopper slide," making $200 a week. Back in his hometown, rumors start to spread about Everett, creating a run on Mr. Nelsons bank. Everetts girlfriend, Mary Martin (Wilson), goes to New York to ask him for help and sways him to return home. Everett saves the bank and he and Mary get married.   

==Cast== Charles Ray - Everett Nelson
*Charles K. French - Isaac Nelson, Everetts Father
*Margery Wilson - Mary Martin
*Lydia Knott - Mrs. Nelson
*Tom Guise - Karl Seligman

==Reception== city and state film censorship boards. The Chicago Board of Censors required one cut of a stamped postcard (the board cut closeups of all envelopes and postcards from films). 

==References==
 

==External links==
*  
 
 
 
 
 
 
 

 