Killer Tattoo
{{Infobox film
| name           = Killer Tattoo
| image          = Muepeun.jpeg
| caption        = The Thai film poster.
| director       = Yuthlert Sippapak
| producer       = 
| eproducer      = 
| aproducer      = 
| writer         = Yuthlert Sippapak
| starring       = Suthep Po-ngam Somchai Kemglad Sornsutha Klunmalee Petchtai Wongkamlao Pongsak Pongsuwan
| music          = 
| cinematography = 
| editing        =  RS Film
| released       = April 5, 2001
| runtime        = 114 min. Thailand
| awards         =  Thai
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} Thai action Thai comedians Petchtai Wongkamlao and Pongsak Pongsuwan (playing an assassin who thinks hes Elvis Presley).

==Plot==
The scene is set sometime in the near future, after some type of apocalypse or war, and Thailand has been taken over by the United States.
 assassin Bae Buffgun is offered a job – killing Bangkoks police chief. Buffgun forms a team of killers, comprising his old partner, Ghost Rifle and two newcomers, Dog Badbomb, a short-tempered explosives expert and Elvis M-16, whos suffered some of trauma that makes him think hes Elvis Presley.

Meanwhile, Thailands most deadly assassin, Kit Silencer has also been hired to kill the top cop. Confusion ensues, and even though the chief is killed, the crime lord who hired the killers wants them all dead anyway.

All the hitmen have issues:
* Buffgun is guilt-ridden over abandoning his daughter years earlier.
* Ghost Rifle is haunted by the accidental shooting of his beautiful wife.
* Dog harbours a secret reason for his allegiance to Elvis. English commands. But none of the other hitmen speak English.
* Kit Silencer is driven by avenging the deaths of his parents, who were killed by a man with a trident tattoo on the inside of his right wrist. He checks everyone he kills or comes into contact with for that tattoo. Buffgun happens to be wearing a bandage over the exact spot on his wrist where a tattoo might be, so Kit suspects the older gunman.

==Cast==
* Suthep Po-ngam as Pae Buffgun
* Somchai Kemglad as Kit Silencer
* Sornsutha Klunmalee as Ghost Rifle
* Petchtai Wongkamlao as Dog Badbomb
* Pongsak Pongsuwan as Elvis M 16

==Film festivals==
* 2002 Seattle International Film Festival
* 2002 Moscow Film Festival
* 2002 Tokyo International Fantastic Film Festival
* 2002 Stockholm International Film Festival
* 2003 New York Asian Film Festival

==External links==
*  
*  
*  
* 

 

 
 
 
 
 
 
 