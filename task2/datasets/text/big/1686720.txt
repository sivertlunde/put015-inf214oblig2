The Twilight Samurai
{{Infobox film
| name        = The Twilight Samurai
| image       = Twilight_Samurai_Poster.jpg
| caption     = Film poster
| writer      = Yôji Yamada Shuhei Fujisawa Yoshitaka Asama 
| starring    = Hiroyuki Sanada Rie Miyazawa
| director    = Yôji Yamada
| producer    =
| music       = Isao Tomita
| distributor = Shochiku Co., Ltd. Empire Pictures
| released    =  
| country     = Japan
| runtime     = 129 min. Japanese
| gross       = $7,338,987 
}}
The Twilight Samurai or   is a 2002 Japanese film directed by Yoji Yamada.  Set in Edo period|mid-19th century Japan, a few years before the Meiji Restoration, it follows the life of Seibei Iguchi, a low-ranking samurai employed as a bureaucrat. Poor, but not destitute, he still manages to lead a content and happy life with his daughters and senile mother. Through an unfortunate turn of events, the turbulent times conspire against him.

The Twilight Samurai was nominated for the Academy Award for Best Foreign Language Film at the 76th Academy Awards, List of Japanese submissions for the Academy Award for Best Foreign Language Film|Japans first in twenty two years, losing to the French Canadian (Québec) film Les Invasions Barbares. The Twilight Samurai also won an unprecedented 12 Japanese Academy Awards, including Best Picture, Best Director, Best Actor, Best Actress, and Best Screenplay.

==Plot==
At the start of the film, the main character, Iguchi Seibei, becomes a widower when his wife succumbs to tuberculosis. His wife receives a grand funeral, more than what a low-ranking samurai such as Seibei could afford. Seibei works in the grain warehouse, accounting for stores inventory for the samurai clan. His samurai colleagues mock him behind his back with the nickname Tasogare (Twilight) because, when evening approaches, Seibei rushes home to look after his senile elderly mother and two young daughters, Kayano and Ito, instead of bonding with his supervisor and other samurai colleagues over customary nights of dinner, geisha entertainment, and sake drinking. Even though he is a samurai, Seibei continues to neglect his own appearance, failing to bathe and being shabbily dressed. The well-being of his young daughters and medicine for his mother take priority over new clothes or the monthly bath fee.

Things change when Seibeis childhood friend Tomoe (sister of Iinuma Michinojo, one of his better, kinder samurai friends) returns to town. Recently divorced from an abusive alcoholic husband (Koda, a samurai captain), Tomoe finds comfort and solace with Seibeis daughters. When her ex-husband Koda barges into the household of Michinojo in the middle of night in a drunken demand for Tomoe, Seibei accepts a duel with the captain, hoping to put a stop to the abuse. There seems little chance for him to beat the captain, but Seibei feels he must try. Dueling amongst clan members is strictly forbidden. The penalty is usually death for the winner as the loser is already dead. Seibei decides to use only a wooden stick whilst Koda brandishes a steel katana. Seibei overcomes Koda, sparing both their lives.

When Iinuma Michinojo asks Seibei to marry his sister, he feels that Iinuma is teasing him for his strong feelings for Tomoe, like when he, Iinuma, and Tomoe were children. Iinuma knows Tomoes feeling for Seibei, and Seibei is a kind man who would treat Tomoe better than Koda. With much deep regret, Seibei declines Iinumas offer of his sisters hand in marriage, citing his inferior social status and how he did not want to see Tomoe share the burden of poverty as Seibei struggles every month to feed Kayano and Ito whilst caring for his ailing mother. Seibei stoically regrets how his departed wife suffered in his care, who like Tomoe came from a wealthier family. Iinuma talks no more of it. Tomoe stops visiting Kayano and Ito.

In the final act, the head of Seibeis clan, having heard of his prowess with a sword, orders Seibei to kill a samurai retainer, Yogo Zenemon, who has been "disowned" and who stubbornly refuses to resign his post by committing seppuku. The young lord of the clan has died from measles, and there is a succession struggle going on behind the scenes over who will be the new lord of the clan. Yogo ended up on the losing side of this conflict, hence his ordered suicide. Yogo and Seibei had met before, as Yogo is friends with Koda and had visited Seibei in the grain warehouse, telling Seibei that Koda had asked him to beat Seibei in retaliation for being humiliated by Seibei. Yogo however had not, stating that Koda was a coward and Seibei a good man, and it would be wrong to do so. Yogo has already killed a formidable samurai that was sent to kill him. Seibei is promised a rise in rank and pay if he accepts the dangerous mission. Seibei is very reluctant at first, requesting one month to prepare for it. He says that, because of great hardship in his life, he has lost all resolve to fight with ferocity and disregard for his own life, because of the experience of watching his two girls grow. As they continue to insist, he requests two days to get himself up to the task. The new clan leader is furious over this answer and orders him expelled from the clan. Seibei is finally forced to agree to attempt the mission. Upon parting that evening, Seibeis supervisor (who was present during the meeting) promises him that he will make sure the girls will be taken care of if the worst comes to pass.

The following morning, Seibei attempts to get ready, but there is no one to help him prepare in the rituals that are customary of samurai before battle. With no one to turn to, he asks Tomoe for her assistance. Before he leaves, he tells Tomoe that he was wrong to decline the offer of marriage. He says that if he lives, he would like to ask for her hand in marriage now that there is promise of a promotion. She regretfully tells Seibei she has already accepted another proposal. Seibei, feeling like a fool, tells Tomoe to forget about the silly conversation. Tomoe says that she will not be waiting at his household for him to return, but that she hopes from her heart that he will return safely. Seibei says he understands completely. He thanks Tomoe for her generosity for assisting him in this final ritual. They part.

At Yogos house, Seibei finds his target drinking in a dark, fly-infested room. Yogo recognizes Seibei and invites him to sit and drink. He then asks Seibei to allow him to run away. He explains he was only faithfully serving his master and describes how both his wife and daughter also died of tuberculosis, and only thanks to his masters generosity could he afford a proper funeral. Seibei commiserates and reveals that he sold his katana to pay for his wifes funeral. He reveals that his scabbard contains a fake bamboo sword. This angers Yogo who believes Seibei is mocking him. Seibei explains he has been trained with the short sword, which he still carries, but Yogo is not placated.

Seibeis kodachi fighting style is matched up against Yogos ittō-ryū (single long sword) swordsmanship in an intense close quarters duel. Despite allowing Yogo to slash him several times, Seibei kills Yogo when his longer sword gets caught in the rafters. Despite his wounds, Seibei limps home. Kayano and Ito rush to him in the courtyard, happy to see him. Tomoe is still there, waiting in the house. They have an emotional reunion and are married.

In a brief epilogue set many years later, Seibeis younger daughter, now middle-aged, visits the grave of Seibei and Tomoe. Narrating, she explains that their happiness was not to last: he died three years later in the Boshin War, Japans last civil war, with Tomoe ensuring that his daughters were provided for. Ito often heard from fellow co-workers that Tasogare Seibei was a very unfortunate character, a most pathetic samurai with no luck at all. Ito disagrees: her father never had any ambition to become anything special; he loved his two daughters, and was loved by the beautiful Tomoe.

==Cast==
*Hiroyuki Sanada as Seibei Iguchi
*Rie Miyazawa as Tomoe Iinuma
*Nenji Kobayashi as Choubei Kusaka
*Ren Osugi as Toyotarou Kouda
*Mitsuru Fukikoshi as Michinojo Iinuma
*Hiroshi Kanbe as Naota
*Miki Itô as Kayano Iguchi
*Erina Hashiguchi as Ito Iguchi
*Reiko Kusamura as Iguchis Mother
*Min Tanaka as Zenemon Yogo
*Keiko Kishi as Ito, as an old woman
*Tetsuro Tamba as Tozaemon Iguchi

==Reception==

===Awards===
Beside several nominations, the film won the following awards:
*Award of the Japanese Academy (2003)
*Blue Ribbon Awards (2003)
*Hawaii International Film Festival (2003)
*Hochi Film Award (2002)
*Hong Kong Film Award (2004)
*Kinema Junpo Award (2003)
*Mainichi Film Concours (2003)
*Nikkan Sports Film Award (2002)
*Udine Far East Film Festival (2004)

===Critical reaction===
The Twilight Samurai has a rating of 99% at Rotten Tomatoes and is certified as "Fresh".  Metacritic
gave it an overall score of 82 out of 100. 

Stephen Hunter of The Washington Post stated "This is an absolutely brilliant film but in a quiet way." 

Roger Ebert of The Chicago Sun-Times gave it a four-star (out of four) rating saying "Seibeis story is told by director Yoji Yamada in muted tones and colors, beautifully re-creating a feudal village that still retains its architecture, its customs, its ancient values, even as the economy is making its way of life obsolete." 

==Soundtrack==
*Composer: Isao Tomita
*Theme song: "Kimerareta Rizumu" ("The Rhythm which is Decided"), sung by Yōsui Inoue.

== References ==
 

==External links==
* 
*  
* 
* 
*  at Metacritic
 
 
{{Navboxes  title = Awards  list =
 
 
 
 
 
}}

 
 
 
 
 
 
 
 
 
 