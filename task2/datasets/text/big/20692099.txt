Pudhu Pudhu Arthangal
{{Infobox film
| name = Puthu Puthu Arthangal
| image = Puthu_Puthu_Arthangal.jpg
| size = 300px
| caption = Official DVD Box Cover
| director = K. Balachander   
| producer = Rajam Balachander Pushpa Kandaswamy
| writer = K. Balachander
| narrator = Rahman  Sithara  Geetha  Janagaraj  Vivek   Chi Guru Dutt Ilaiyaraaja
| music = Ilaiyaraaja 
| cinematography = Raghunatha Reddy
| editing = Ganesh Kumar
| studio = Kavithalayaa Productions
| distributor = Kavithalayaa Productions
| released = 28 October 1989
| runtime =
| country = India
| language = Tamil
| budget =
| gross =
| preceded_by =
| followed_by =
}}
 Vivek and Ilaiyaraaja. 

__TOC__

== Plot ==

This refreshing tale from K. Balachander focuses on the fairly realistic tale of a famous singer Bharathi (Rahman with singing by S. P. Balasubrahmanyam) and his extremely possessive wife named Gowri (Geetha). Tricked into marriage by his convincing mother-in-law, what really irks Bharathi is his wife’s inability to deal with his rabid female fans. Tensions escalate until Bharathi flees from home and ends up encountering someone else named Jyothi (Sithara (actress)|Sithara) who’s also escaping from her life as a dancer because her husband made her dance forcefully and tried forcing her to be a prostitute.

Geetha’s character is surprisingly well-defined. As a rabid fan who gets to marry the object of her desires, she is obviously afraid her husband will cheat on her with one of the thousands of women throwing themselves at him – after all, he slept with them before their marriage. Geetha turns in a convincing performance as a woman whose all-consuming love ends up consuming her. Rahman, who sounds suspiciously like ‘Nizhagal’ Ravi, also does well as the famous singer – but a tiny quibble with the story: he goes to work in a restaurant in North India serving South Indian cuisine, and none of the customers recognize the famous singer. Then, he makes a living in Goa with Jyothi, meet one Malayali couple, who died later in the movie and are both fluent in Hindi, and by watching the couple,they learn the true definition for love while falling in love themselves.

Then Bharathi get a prank call from his PA that Gowri is ill. So he comes back with Jyothi from Goa, which makes Gowri angry and she doesnt let Jyothi in without knowing who she is. Bharathi gets angry and they stay in another one of his houses. Bharathi gets so angry that he claims that he loves Jyothi and that she cannot go because of her problems. So Gowri files for a divroce, informs news reporters about Jyothi. Jyothis husband appears later in the story. Gowri plans to get married to a cricket player Guru. Guru had proposed to her in the past. He accepts the proposal and abandons one of his fans, Yamuna, who is obsessed with him. Later, Yamuna commits suicide in the marriage hall readied for Gowris marriage. Gowris mother and her PA feel that she is mentally unstable. Her mother begs Bharathi to go and see her in the hospital and he goes with Jyothi to see her. Jyothi then sees that Bharathi and Gowri belong to each other. Jyothi later finds her husband, who was indeed in love with her. After that, Bharathi goes to recording with him being happy and visits his wife in the hospital.

==Cast== Rahman as ManiBharathi Sithara as Jyothi Geetha as Gowri Janagaraj as Jolly
* Jayachitra as Kanchanamala
* Poornam Vishwanathan
* Sowcar Janaki Vivek as Vittal Manibharathis personal assistant
* Chi. Guru Dutt as Guru, Cricketer
* Ilaiyaraaja as himself in "Kalayanamalai" song
* Ganesh as Jollys assistant Kuyili

==Remake==

Puthu Puthu Arthangal was remade in Hindi, but never got released. The film had title as Dilon Ka Rista starred Rahul Roy, Anu Agarwal and Ashwini Bhave had music by Bappi Lahiri and was directed by K.Balachander.

== Soundtrack ==

{{Infobox album  
| Name        = Puthu Puthu Arthangal
| Type        = soundtrack
| Artist      = Ilaiyaraaja
| Cover       =
| Caption     =
| Released    =    
| Recorded    =
| Genre       =
| Length      = Tamil
| Label       =
| Producer    =Ilaiyaraaja
| Reviews     =
| Compiler    =
| Misc        =
}}

The music composed by Ilaiyaraaja and lyrics were penned by Vaali (poet)|Vaali. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers || Length (m:ss)
|-
| 1 || "Eduthu Naan Vidava" || Ilaiyaraaja, S. P. Balasubrahmanyam || 04:45
|-
| 2 || "Ellorum Mavatta" || S. P. Balasubrahmanyam, S. P. Sailaja || 04:34
|-
| 3 || "Guruvayurappa" || S. P. Balasubrahmanyam, K. S. Chithra || 02:12
|-
| 4 || "Kalyaana Maalai 1" || Ilaiyaraaja, S. P. Balasubrahmanyam || 05:57
|-
| 5 || "Kalyaana Maalai 2" || S. P. Balasubrahmanyam || 04:40
|-
| 6 || Keladi Kanmani || S. P. Balasubrahmanyam || 04:35
|}

== References ==
 

== External links ==
*  
*  

 
 
 

 
 
 
 
 
 
 
 