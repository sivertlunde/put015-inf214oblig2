A Sound of Thunder (film)
{{Infobox film
| name           = A Sound of Thunder
| image          = A Sound of Thunder poster.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Peter Hyams
| producer       = {{Plainlist|
* Moshe Diamant
* Howard Baldwin
* Karen Baldwin }} Thomas Dean Donnelly Joshua Oppenheimer Gregory Poirier
| based on       =  
| starring       = {{Plainlist|
* Edward Burns
* Catherine McCormack
* Ben Kingsley }} 
| music          = Nick Glennie-Smith
| cinematography = Peter Hyams
| editing        = Sylvie Landra
| studio         = {{Plainlist|
* Franchise Pictures
* Crusader Entertainment Baldwin Entertainment Group }}
| distributor    = {{Plainlist|
* Warner Bros.  
* Bioscop   }}
| released       =  
| runtime        = 110 minutes
| country        = United Kingdom United States Germany Czech Republic
| language       = English Mandarin
| budget         = $80 million 
| gross          = $11,665,465 
}} science fiction thriller film United Kingdom, United States, Germany and Czech Republic.
 of the time tourists" who accidentally interfere too much with the past, completely altering the present.

==Plot==
The film opens in the   is observed to erupt five minutes after they arrive, their target is an Allosaurus (which actually went extinct in the Jurassic) that would imminently get stuck in tar, frozen nitrogen bullets are used as ammunition to leave no trace, and the party wears self-contained suits and are told they must stay on a floating path and not touch anything. Furthermore, all the guns are tied to Traviss weapon, and will not fire until he does so.

They successfully complete the expedition and return to 2055 Chicago, where Time Safari chairman Charles Hatton (Ben Kingsley) organizes a party to celebrate their success. The party is interrupted by the disgruntled Sonia Rand (Catherine McCormack), who sprays blood on Hatton and the Wallenbecks. Sonia is the inventor of the time machine software, TAMI, but received no credit due to loopholes in the contract she signed to license TAMI. She is later confronted by Travis and the two have a brief conversation in Sonias lab, in which she explains to him the danger of altering the past using the time machine.
 Corey Johnson), Ryers gun fails as the Allosaurus rushes the party. The tourists go out of sight while he corrects the weapon in time to kill it. The party returns to their time, unaware that someone has accidentally stepped off the path. The next morning, the TV news describe a drastic rise in temperature and humidity, and Travis observes a sudden growth in plant life.

On a subsequent trip with different guests, the party finds that they have arrived too late: the Allosaurus is already dead and the volcano is only seconds away from erupting. When Travis returns to report these changes, Time Safari is immediately shut down by the government. Travis seeks out Sonia at her apartment, and she points out the arrival of the first "time wave", which drastically alters the city because of the changes in the past and fluctuation in the evolutionary tree. A swarm of beetles appears, and Travis causes an explosion while fighting them off; the two jump off the building, and are caught by the branches of a tree that has burst through the building. Sonia warns that more waves can be expected, each affecting a later stage of evolution, of which humans are the current final link.

Returning to Time Safari, the government works with the company to try and correct Middletons mistake. Travis mistakenly appears in the American Southwest during the 18th century; he returns to 2055 as another time wave hits, leaving the city out of power and completely wrecked by dense vegetation. Sonia explains that the presence of time waves is preventing the machine from operating properly. Payne discovers that the expedition came back from the past 1.3 grams heavier, and that Hatton had shut down the costly "bio-filter" which was meant to prevent items from the past from arriving in the present. They scan their expedition clothes, and nothing turns up, meaning it was one of the tourists who stepped off. Seeing as Eckels was terrified on the hunt, they decide it must have been him. Sonia explains that the time machine can theoretically break through the waves using a wormhole to go back to the approximate time of the hunt, then slingshot to the arrival of the party. However, this would leave the time traveler only seconds to fix the timeline. The group goes to seek Eckels while Derris and Hatton stay behind. On the way Payne is attacked by a poisonous plant which leaves him delusional and partly Paralysis|paralysed. They then encounter the Baboonlizards, alternatively evolved dinosaur-primate hybrids, and Payne sacrifices himself to let the group escape. Sonia explains that even if the timeline is fixed, subsequent expeditions could still go awry unless their past selves can shut down Time Safari.
They find Eckels, but he claims he never stepped off the path, so they go after Middleton. Middleton, also poisoned, is severely paranoid and commits suicide without telling them anything. In searching through Middletons kit from the safari they discover a butterfly on the sole of his boot, the one he had stepped on in the past.
 subway tracks to the university. As they get inside a train car, the ceiling of the tunnel gives way, flooding the car and leaving the three trapped inside. A long reptilian eel-like creature bursts through a window and devours Jenny. Sonia escapes and Travis fights it off long enough for the trains ceiling to cave in and pin down the creature. Sonia retrieves Travis and they arrive at the university, followed by a new, more simian kind of Baboonlizard, indicating that only one wave remains. Sonia is able to send Travis back in time just as the wave hits, leaving her as a catfish-like humanoid.

Travis successfully intercepts the hunting party and corrects Middletons mistake. Before disappearing, he tells Jenny that the bio-filter has been turned off, and asks her to give the old Travis the recording of the expedition. She does, and Travis brings it to Sonia; she agrees to watch it, at his insistence that the video contains enough evidence to put an end to Time Safari.

==Cast==
* Edward Burns as Travis Ryer
* Catherine McCormack as Sonia Rand
* Ben Kingsley as Charles Hatton
* Jemima Rooper as Jenny Krase
* David Oyelowo as Marcus Payne
* Wilfried Hochholdinger as Dr. Lucas
* August Zirner as Clay Derris Corey Johnson as Christian Middleton
* Heike Makatsch as Alicia Wallenbeck
* Armin Rohde as John Wallenbeck
* William Armstrong as Ted Eckels

==Production==
The film, announced in 2001, was originally going to be directed by Renny Harlin, star Pierce Brosnan in the main role,  and be shot in Montreal.  However, Harlin was fired over a disagreement with Ray Bradbury. Brosnan left the project as well and was replaced by Edward Burns. 

After Franchise Pictures went bankrupt during post-production, the remaining backers provided only $30 million to work with,  out of the $80 million originally allocated. This put the visual effects department under time and resource constraints, forcing them to use off-the-shelf software that did not require long rendering times. The films computer graphics ended up being harshly criticized as cheap and unconvincing (see "Reception" below).

==Video games== video game link cable.
 The Thing engine was being developed by Computer Artworks for BAM! Entertainment for the PlayStation 2 and Xbox, but ended up being cancelled. Its plot differed from that of the film: the changes in the course of evolution were not an accident, but acts of terrorism caused by a Luddite cult. The "present" time was also changed to 2038. The game was to have nine missions taking place in both the past and present. Real-life bands would have been hired to provide the music. 

==Reception==
The film received overwhelmingly negative critical reviews, currently holding a 6% rating on Rotten Tomatoes based on 96 reviews. The film scored an average rating of 2.8 out of 10; out of 88 reviews by critics, 81 gave it a poor rating.  Common complaints against the film included its poor special effects, uninvolved performances, scientific errors and Ben Kingsleys hair.   

Due to negative reviews and lack of promotion, the production grossed only $1,900,451 in the United States and $9,765,014 elsewhere for a worldwide total of $11,665,465.     

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 