Ride the Wild Surf
{{Infobox Film
| name           = Ride the Wild Surf
| image          = Ride the Wild Surf.jpg
| caption        =  Don Taylor
| producer       = Art Napoleon Jo Napoleon
| writer         = Art Napoleon Jo Napoleon Fabian Shelley Peter Brown Barbara Eden Tab Hunter Susan Hart James Mitchum Stu Phillips
| cinematography = Joseph F. Biroc
| editing        = Howard A. Smith Eda Warren	
| distributor    = Columbia Pictures
| released       = August 5, 1964
| runtime        = 101 min.
| country        = USA
| awards         = 
| language       = English
| budget         = 
| gross          = est. $1,400,000 (US/ Canada) 
| preceded_by    = 
| followed_by    = 
}}
 beach party style. It was filmed in 1963 and distributed in 1964.  Unlike most films in the genre, it is known for its exceptional big wave surf footage – a common sight in surf movies of the time, but a rarity in beach party films.   Likewise, the film has only one pop song – the titular Jan and Dean track, which is heard once, at the end of the film.

==Plot== Peter Brown), who come to Hawaiis Oahu Island to ride the worlds biggest waves and compete against surfers from all over the world.  

Steamer falls in love with Lily Kilua (Susan Hart), whose mother objects to the romance because she considers surfers to be "beach bums," since her husband--a surfer--left home and family to follow the surf circuit. Self-described college dropout and surf bum Jody falls for the demure Brie Matthews (Shelley Fabares), who challenges him to return to college. In the case of the relatively strait-laced Chase, he finds himself pursued by the adventurous Augie Poole (Barbara Eden).

The main story, though, is the challenge to surf the monster waves at Waimea Bay, and fit in among the champion surfers there such as Eskimo (James Mitchum).  Despite conflicts, injuries and rocky romances, Wallis, Chase and Steamer prove themselves brave—or crazy—enough to try to be the last one to ride in the highest wave.

==Music== Stu Phillips - it was the third film score he had ever composed.  Phillips also founded Colpix Records and produced hits for Nina Simone, The Skyliners and one of Ride the Wild Surfs stars, Shelley Fabares.
 Roger Christian, and recorded by Jan & Dean becoming a Top 20 national hit, reaching Billboard Hot 100|Billboards #16 spot. 

==Production==
===Location=== Waimea broke often.  The jet stream had altered its course temporarily and huge west swell surfs became common all the way through the following February, which was when Columbia arrived to shoot the movie.

The Naploeons travelled to Hawaii in late 1963 and early 1964 to shoot surf footage. They then returned to Hollywood to write the script. Tom Lisanti, Hollywood Surf and Beach Movies: The First Wave, 1959-1969, McFarland 2005, p141-149 
===Casting===
Jan & Dean both were scheduled to appear in the film, supporting Fabian, who had been borrowed by 20th Century Fox. Jan and Dean  were pulled by Columbia after Dean’s friend, Barry Keenan, became involved in the kidnapping of Frank Sinatra, Jr.  They were replaced by Tab Hunter and Peter Brown. 

Susan Hart was cast after impressing Mike Frankovich of Hollywood in some TV appearances she had made; she dyed her hair black for her role. Hart was seen by James H. Nicholson of AIP in the film, which lead to him signing her to that studio and later marrying her. 

Fabian had never surfed before and spent three weeks learning. Fabian: Yesteryears Idol: UNDER HEDDAS HAT
Hopper, Hedda. Chicago Tribune (1963-Current file)   02 Aug 1964: i14.   Australian Olympic swimming champion Murray Rose was given a small role. New Krasna Farce a Moon Is Bluer: Jane Fonda Faces Quandary in Sunday in New York
Scheuer, Philip K. Los Angeles Times (1923-Current File)   26 Mar 1964: C9. 
 Phil of Downey, California - aka Phil Sauers, the maker of "Surfboards of the Stars."  Sauers is portrayed in Ride the Wild Surf as a character, played by Mark LaBuse. Sauers was also the stunt coordinator for the film.

===Shooting===
Art and Jo Napoleon shot the movie for three weeks. Then Columbia replaced them with Don Taylor. Looking at Hollywood: Columbia Shoots Film for 3 Weeks, Starts Over
Hopper, Hedda. Chicago Tribune (1963-Current file)   02 Apr 1964: d10.   Taylors mother died during the shoot so Phil Karlson returned for three days. Looking at Hollywood: Hedda Rings Big Bell for Mary Tyler Moore
Hopper, Hedda. Chicago Tribune (1963-Current file)   21 Apr 1964: a2.  

===Stunts===
Surfers Mickey Dora, Greg Noll and Butch Van Artsdalen performed a large part of the surfing seen in the film. 

===Costumes & Make-up===
 Peter Brown was made into a blonde by makeup artist Ben Lane (to match the hair of Brown’s surfing double – and to keep all three men from being brunettes), which required his girlfriend, the blonde Barbara Eden, to have auburn hair; likewise the dark-haired Shelley Fabares – who is paired with the dark-haired Fabian (entertainer)|Fabian, became a Scandinavian blonde.  Susan Hart’s black hair was sufficiently different from her male counterpart Tab Hunter’s that no change was required. 

The stunt surfers were given swim trunks that matched their movie star counterparts, except for star James Mitchum, who was instead given trunks that replicated his stunt double Greg Noll’s famous black & white "jailhouse stripe" boardshorts.

===Movie tie-in===

Although the film featured lots of music, it had only one song - the 1:07-long version of "Ride The Wild Surf". A 12-inch  .  Of the 12 tracks on the LP, only one was from the film: a 2:13-long version of the title song.

==See also==
*Blue Crush, a 2002 film about three surfer girls living in Hawaii North Shore, a 1987 film about a surfer from Arizona that learns to surf in Hawaii

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 