Ajana Path
 
{{Infobox film
| name           = Ajana Path
| image          =
| caption        =
| director       = Srinibas Chakraborty
| producer       = J. P. Productions
| story          =
| screenplay     = Pran Utpal Dutta Satya Bandyopadhyay Nita Puri 
| distributor    =
| studio          = J. P. Produtions
| editing        =
| cinematography =
| writer         = Chittaranjan Maity
| released       =  
| runtime        = 2:35:46
| country        = India Bengali
| music          = R. D. Burman
| lyrics         =
| awards         =
| budget         =
}}
 Bengali film directed by Srinibas Chakraborty & produced by J. P. Productions.This film starring Prosenjit Chatterjee,Satabdi Roy,Pran (actor)|Pran,Utpal Dutta,Satya Bandyopadhyay,Nita Puri. This film has been music composed by R. D. Burman. 

==Cast==
* Prosenjit Chatterjee
* Satabdi Roy Pran
* Utpal Dutta
* Satya Bandyopadhyay
* Nita Puri

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s) !! Duration
|-
| 1
| Ae Katha Janto
| 
| 5:07 
|-
| 2
| Ghare Te Aasena Se
| 
| 5:22
|-
| 3
| Haria Jaoa Sab Kichu
| 
| 5:49
|-
| 4
| Jibaner Naam Holo Chala
| 
| 5:27
|-
| 5
| Karo Bhalo Bhabte Giye
| 
| 5:08
|-
| 6
| Pardeshi Janina
| 
| 6:37 
|-
|}

==References==
 

==External links==
*   at the Gomolo
* http://www.in.com/ajana-path/profile-391446.html

 
 
 


 