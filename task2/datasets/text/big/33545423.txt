A Sister to Assist 'Er (1938 film)
{{Infobox film
| name           = A Sister to Assist Er
| image          = 
| alt            =  
| caption        = 
| director       = George Dewhurst Widgey R. Newman
| producer       = Widgey R. Newman
| writer         = George Dewhurst John le Breton
| based on       =  
| starring       = Muriel George Polly Emery Charles Paton Billy Percy
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 72 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}

A Sister to Assist Er is a 1938 British comedy film directed by George Dewhurst and Widgey R. Newman and starring Muriel George, Polly Emery and Charles Paton.  It was based on the play A Sister to Assist Er by John le Breton.

==Cast==
* Muriel George ...  Mrs. May / Mrs. le Browning 
* Polly Emery ...  Mrs. Getch 
* Charles Paton ...  Mr. Harris 
* Billy Percy ...  Alf 
* Harry Herbert ...  Mr. Getch  Dorothy Vernon ...  Mrs. Thistlethwaite 
* Dora Levis ...  Mrs. Hawkes 
* Elsie Shelton ...  Miss Pilbeam

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 


 