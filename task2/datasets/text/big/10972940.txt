Search for the Truth (film)
Search compares it to the Bible. The video implies that you have to follow Jesus or Joseph Smith but not both.

==Distribution== The God Makers in 1982. The producers claim that the video was made out of love for the Mormon people. The purpose of The God Makers, according to Concerned Christians, was also “to reach out in love to those lost in Mormonism.” See  . General Conference of the church that was held on March 31 and April 1, 2007:
  evangelistic outreach until after March 25, 2007. Why such extreme caution? If the leadership of the Mormon cult learns of our plans, they will publicly instruct their people not to watch the video and many Mormons will blindly obey. 
 
The letter goes on to say that the "Mormon Church is vulnerable. We firmly believe that with enough exposure, Mormonism will crumble and become a shadow of what it is today."

==Content==

===Nature of God and Jesus Christ=== previous life.” 
 John Whitcomb (Theology Professor, Old Testament Scholar), quotes and interprets passages from the Bible to support this claim. Referring to Paul’s statement in 1 Corinthians 8:4 about “many gods and many lords,” Whitcomb points to his head and states that that “the Bible says through the Apostle Paul they’re only in here.”
 Dave Hunt (Author and Founder, The Berean Call Ministries) states that the serpent in the Garden of Eden promised Eve that “she could become a god.”  Hunt continues by claiming that Brigham Young said that “the devil told the truth.” 
 his wife was his sister, “is similar to the Bible version only in the book of Abraham god is telling Abraham to lie.” 
 President of The Church of Jesus Christ of Latter-day Saints, is misquoted  as saying, “No I don’t believe in the traditional Christ. The traditional Christ of whom they speak is not the Christ of whom I speak, for the Christ of whom I speak has been revealed in this, the dispensation of the fullness of times.” 

===Character of Joseph Smith===
A quote from the Bible (Ephesians 2:8-9) regarding boasting is juxtaposed with a quote from Joseph Smith Jr. in which he claims to boast. 

The narrator repeats a quote from Brigham Young, in which he claims that “no man or woman in this dispensation will ever enter the celestial kingdom of god without the consent of Joseph Smith.” 

It is claimed that Mormons believe that their "godhood rests on the act of polygamy”, based upon a quote made by Brigham Young in 1866: “The only men who become Gods, even the sons of God, are those who enter into polygamy.” 

===First Vision=== first vision Sacred Grove and several accounts of the visit of an angel in Smith’s bedroom. The onscreen list shows the following accounts:

#1827 – A spirit appears to Joseph telling him of a record on gold plates at age 17. 
#1827 – An angel appears to Joseph telling him he has been chosen to be a prophet and bring forth a record on gold plates at age 18. 
#1830 – An angel tells Joseph where to find a secret treasure. Joseph returns once a year for several years before obtaining the plates. 
#1832 – Jesus Christ appears to Joseph at age 15. 
#1834 – Angel appears to Joseph in his bedroom at age 17. 
#1835 – Two personages appear to Joseph in grove at age 14. 
#1835 – Many angels appear to Joseph in grove at age 14. 
#1838 – God the Father and Jesus appear to Joseph in grove at age 14.
#1844 – Two unidentified personages appear to Joseph at age 14. 

The narrator claims that Brigham Young “denied that the Lord came to Joseph Smith in the first vision.” Several sentences from a speech given by Brigham Young are shown which state, “The Lord did not come…but He did send His angel to this same obscure person, Joseph Smith…and informed him that he should not join any religious sects of the day…” 

===Archaeology=== archaeological evidences.” A 1969 quote by BYU professor Dee Green is referenced in which he “confesses” that “No book of Mormon location is known.” It is stated that “not even one coin” has been found, and that coins were mentioned as “being common in Joseph’s writings.”

Joel Kramer (Director, Living Hope Ministries) relates that his group went to the Middle East and Central America and talked to experts in archaeology and anthropology. Kramer states that “in all cases” that they found the “historical reliability” of the Bible and that for the Book of Mormon it was “non-existent.”

Floyd McElveen (Author and Lifetime Evangelist) adds that “Joseph Smith claimed there were huge cites – thirty eight cities in the Americas,” but that “not one single city has ever been dug up.”   McElveen adds, “would you want to base your eternity on something that is totally unknown?”
 Sandra Tanner (President, Utah Lighthouse Ministry) notes that the LDS Church will not “commit itself” to a specific map of Book of Mormon lands.

===Prophecies=== sun was also inhabited. 

===Occult===
The video notes that on March 15, 1842, Joseph Smith joined the Freemasonry|Masons, which is claimed by the narrator to be “an organization that believes that Jesus is not divine.” It is further claimed that certain Mormon practices and architecture have “Masonic overtones.”

The video cites comments made by LDS historian Dr. Reed Durham regarding an item referred to as the “Jupiter Talisman," which is said to have been owned by Joseph Smith Jr. The narrator refers to this as an "occultic" item whose “talismatic magic” is said to bring riches, power and women to its possessor.  The narrator claims that the talisman was “found in Joseph’s pocket the day he died in Carthage.” 
 The God temple rituals.

===Translator===
The narrator states that Joseph Smith “boasted the bold claim” that the Book of Mormon was the “most accurate book in existence.” 

==Response==
The Church of Jesus Christ of Latter-day Saints responded with a news release, part of which stated, 
 
"The Church of Jesus Christ of Latter-day Saints has weathered such attacks throughout its history. At a time when the Church is growing strongly throughout the world, it’s not surprising that some groups try to curb that growth in such ways." 
 
 Mormon bashing." Bill Straus, Regional Director of the Anti-Defamation League, stated,
 
"This is the same kind of plain, old-fashioned Mormon-bashing that Jim Robertson and his group have been spewing for over a quarter-of-a-century. The only difference is that back then, it was the film, The God Makers, and today its the DVD, Jesus Christ/Joseph Smith. It was wrong then, and its wrong now." 
 
 hate directed First Presidency of The Church of Jesus Christ of Latter-day Saints protesting what he called the churchs "tacit approval" of the Anti-Defamation Leagues condemnation of the video. McKeever states, "I wish to express my indignation at your church’s tacit approval of the Anti-Defamation Leagues accusation of hatred towards Christians who were involved in a recent DVD distribution. It is one thing to disagree with the content of the DVD, but it is quite another to accuse them of being motivated by hate." He concludes by stating, "With all the talk of repentance at your last conference, I think you need to lead by example and offer an apology to the thousands of Christans you have offended with this false accusation."  McKeever followed up on this letter with an article posted in the Christian Examiner Online, in which he claimed that the "Mormon Church went into one of its most hypocritical frenzies in modern times." Claiming that the media in Utah is "either owned by the LDS Church or is sympathetic to it," McKeever claimed that " alk show hosts did their best to stir up their listeners against the religious bigots who dared come onto our property with their message of hate." 

The Foundation for Apologetic Information & Research (FAIR), a non-profit organization specializing in Mormon apologetics, produced a point-by-point rebuttal of inaccuracies and exaggerations in the video. This was posted on the FAIR Wiki web site one week before the initial distribution of the DVD.  In addition, the LDS affiliated Foundation for Ancient Research and Mormon Studies posted a page with links to articles written by Mormon scholars which deal directly with subjects addressed in the video. 

==Notes==
 

==References==
*{{Citation
 | last=McKeever
 | first=Bill
 | title=When disagreement is labeled hate
 | publisher=Keener Communications Group
 
 | date=June 2007
 | url=http://www.christianexaminer.com/Articles/Articles%20Jun07/Art_Jun07_oped1.html
 |accessdate=2007-05-30
 }}.
*{{Citation
 | last=Scharffs
 | first=Gilbert W
 | authorlink=Gilbert W. Scharffs
 | title=The Truth About "The God Makers"
 | publisher=Bookcraft
 | year=1994
 | url= http://www.fairlds.org/The_God_Makers
 | isbn=0-88494-963-X
 }}.
*{{Citation
 |last=Smith
 |first=Joseph Fielding
 |authorlink=Joseph Fielding Smith
 |title=Teachings of the Prophet Joseph Smith
 |publisher=Deseret Book
 
 |date=June 1977
 |isbn=0-87579-243-X
}}.
*{{Citation
 | last=Smith
 | first=Joseph, Jr.
 | authorlink=Joseph Smith, Jr.
 | editor-last=Jessee
 | editor-first=Dean C
 | title=The Personal Writings of Joseph Smith
 | publisher=Deseret Book Company
 | year=2002
 | editor-link=Dean C. Jessee
 | isbn=1-57345-787-6
 }}.
*{{Citation
 | last=Tanner
 | first=Jerald and Sandra
 | authorlink=Jerald and Sandra Tanner
 | title=Mormonism: Shadow or Reality
 | publisher=Utah Lighthouse Ministry
 | place=Salt Lake City, Utah
 | year=1992
 | isbn=99930-74-43-8
}}.
*{{Citation
 | last=Young
 | first=Brigham
 | authorlink=Brigham Young
 | title= The Constitution and Government of the United States-Rights and Policy of the Latter-day Saints
 | journal=Journal of Discourses
 |volume=2
 
 | date=Feb 18, 1855
 | url=http://en.wikisource.org/wiki/Journal_of_Discourses/Volume_2/The_Constitution_and_Government_of_the_United_States%2C_etc.
 | accessdate=2007-05-23
 }}.
*{{Citation
 | last=Young
 | first=Brigham
 | authorlink=Brigham Young
 | title= Intelligence, Etc
 | journal=Journal of Discourses
 |volume=7
 
 | date=Oct 9, 1859
 | url= http://en.wikisource.org/wiki/Journal_of_Discourses/Volume_7/Intelligence%2C_Etc.
 | accessdate=2007-05-23
 }}.
*{{Citation
 | last=Young
 | first=Brigham
 | authorlink=Brigham Young
 | title=Delegate Hooper-Beneficial Effects of Polygamy-Final Redemption of Cain
 | journal=Journal of Discourses
 |volume=11
 
 | date=Aug 19, 1866
 | url=http://en.wikisource.org/wiki/Journal_of_Discourses/Volume_11/Delegate_Hooper%E2%80%94Beneficial_Effects_of_Polygamy%2C_etc.
 | accessdate=2007-05-30
 }}.
*{{Citation
 | last=Young
 | first=Brigham
 | authorlink=Brigham Young
 | title=The Gospel-The One-Man Power
 | journal=Journal of Discourses
 |volume=13
 | date=July 24, 1870
 | url=http://en.wikisource.org/wiki/Journal_of_Discourses/Volume_13/The_Gospel%E2%80%94The_One-man_Power
 | accessdate=2007-05-25
 }}.
*{{Citation
 | last=Young
 | first=Brigham
 | authorlink=Brigham Young
 | title=DISCOURSE by President Young delivered in the New Tabernacle, Salt Lake City, Sunday Afternoon, June 8, 1873
 | journal=Deseret News
 | date=June 18, 1873
 | page=4
 | url=http://udn.lib.utah.edu/cdm4/document.php?CISOROOT=%2Fdeseretnews3&CISOPTR=143021&REC=26&CISOBOX=brigham 
 }}.

==External links==
*   
* 
* 

 
 
 
 
 