Badavara Bandhu
{{Infobox film
| name           = Badavara Bandhu
| image          = 
| caption        = 
| director       = Vijay
| producer       = N. Ramaswamy
| writer         = 
| screenplay     = Shankar-Sundar
| story          = 
| based on       = 
| narrator       =  Rajkumar  Jayamala
| music          = M. Ranga Rao
| cinematography = Annayya   Mallik
| editing        = P. Bhakhavatsalam
| studio         = 
| distributor    = CNR Shakthi Productions
| released       =  
| runtime        = 143 minutes
| country        = India
| language       = Kannada
| budget         = 
| gross          = 
}}
 Rajkumar and Jayamala in the lead roles.

== Cast == Rajkumar
* Jayamala
* K. S. Ashwath Balakrishna
* Vajramuni
* Thoogudeepa Srinivas
* Tiger Prabhakar
* Sampath
* Vadiraj
* Joker Shyam
* Hanumanthachar
* Bhatti Mahadevappa

== Soundtrack ==
The music of the film was composed by M. Ranga Rao, with lyrics for the soundtrack penned by Chi. Udaya Shankar.

===Tracklist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|- 
| 1
| "Naga Beda Naga Beda"
| P. B. Sreenivas
|-
| 2
| "Ninna Kangala Bisiya Hanigalu" Rajkumar
|-
| 3
| "Ninna Nudiyu Honna Nudiyu"
| Rajkumar
|-
| 4
| "Naidhileyu Hunnimeya"
| P. Susheela
|}

== References ==
 

== External links ==
*  

 
 
 
 


 