Avtaar
{{Infobox film
| name = Avtaar
| image = Avtaarfilm.jpg
| caption = 
| director = Mohan Kumar
| producer = Mohan Kumar
| writer =  Sachin Yunus Parvez
| music = Laxmikant Pyarelal
| lyrics = 
| cinematography = K. K. Mahajan
| editing =
| released =  
| studio = 
| runtime = 
| country = India
| language = Hindi
| budget = 
| preceded_by = 
| followed_by = 
}}
 Amrit with Rajesh Khanna with similar story. Telugu film Madhu and Baghban in 2003 starring Amitabh Bachchan and Hema Malini was partially inspired from this film. Avtaar is included in the Best Films of Rajesh Khanna by Filmy Keeday.
 
==Plot==

The film starts with a mourning being held at Avtaar Industries. Radha Kishen (Shabana Azmi) garlands her husband Avtaar Kishens (Rajesh Khanna) bust. The story starts from a flashback. Radha is the only daughter of Seth Jugal Kishore (Madan Puri). Radha is in love with Avtaar, a poor boy. Jugal Kishore resents Radhas love. Hence, the duo elope and get married.

Avtaar and Radha face various hardships in life, but ultimately succeed. After 3&nbsp;decades, Avtaar is owner of a small garage and a small fortune. He has two sons, Chander (Gulshan Grover) and Ramesh (Shashi Puri). Chander is married to Renu (Rajni Sharma), while Ramesh is married to Shobha (Priti Sapru). Avtaar also has a servant named Sewak (Sachin (actor)|Sachin).

Like Avtaar, his sons have also married rich girls, daughters of Seth Laxmi Narayan (Pinchoo Kapoor). But, they are totally henpecked. When Avtaar discovers this, he leaves his home. Radha and Sewak also follow him. With help of a moneylender Bawaji (Sujit Kumar), Avtaar starts his own garage.

Avtaar faces an uphill task, since he has no money to buy equipment, is aged and his right hand is paralysed in a freak accident. Sewak helps his master by illegally donating blood to arrange for money, but when Avtaar thinks that Sewak resorted to robbery, Bawaji tells the truth. Moved, Radha and Avtaar regard him as their own son.

Meanwhile, both Ramesh and Chander are enjoying their own life. Avtaars luck changes again when the engine he is working on gives a successful result. Soon, Avtaar starts manufacturing the engine parts. This gives way to an industrial empire headed by Avtaar, Radha and Sewak. To help other old people like him who were spurned by their own families, Avtaar starts some social institutions.

Avtaars success takes a toll on Laxmi Narayans business and he holds Chander responsible. Meanwhile Ramesh commits fraud with the bank and is arrested.  Shobha come to Avtaar for help, but he rebukes her and sends her away. Radha gets angry over this, but keeps silent. Avtaar secretly gives Bawaji the bail money on the condition that he tells no one the truth. Bawaji bails both Ramesh.

Meanwhile holding Chandar responsible for the loss in business, Laxmi Narayan throws him out of the house. Ramesh, Chandar and Shobha go to Radha for help. Although Avtaar is not willing to help them. Next day, Avtaar goes to office and does not return. Radha calls him many times and finally reaches office late night. She tries to convince him that she is a mother too. But he doesnt listen to all that. Deep in emotions, Radha accuses him of having turned heartless. Same day, Bawaji meets Radha, whereupon Radha tells him the whole story. Bawaji has to tell the truth to her as he cannot bear Avtaar being insulted despite his big heart.

On learning the truth, Radha realizes her mistake and tries to call Avtaar. Sewak informs her that Avtaar had a heart attack. Everybody goes to Hospital, where Avtaar had already written his will and hands it to Radha and dies. The story comes to the present, where Radha garlands Avtaars bust and the film ends.

==Cast==
*Rajesh Khanna as Avtaar Kishen
*Shabana Azmi as Radha
*A. K. Hangal as Rashid Ahmed
*Madan Puri as Seth Jugal Kishore Radhas Father
*Gulshan Grover as Chander Kishen
*Shashi Puri as Ramesh Kishen Sachin as Sewak
*Yunus Parvez as Paanwala
*Pinchoo Kapoor as Seth Laxmi Narayan
*Sujit Kumar as Bawaji
*Priti Sapru as Shobha R. Kishen
*Rajni Sharma as Renu C. Kishen
*Ranjan Grewal as Anwar R. Ahmed
*Madhu Malini as Zubeida Anwar
*Shivraj as Gopal

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Din Maheene Saal"
| Kishore Kumar, Lata Mangeshkar
|-
| 2
| "Oopar Wale Tera Jawab Nahin"
| Kishore Kumar
|-
| 3
| "Chalo Bulawa Aaya Hai"
| Mahendra Kapoor, Asha Bhosle, Narendra Chanchal
|-
| 4
| "Yaaro Utho Chalo"
| Kishore Kumar, Mahendra Kapoor
|-
| 5
| "Zindagi Mauj Udane"
| Mahendra Kapoor, Suresh Wadkar, Alka Yagnik
|}

==Filmfare Nominations==
*Best Picture
*Best Director-Mohan Kumar
*Best Actor-Rajesh Khanna
*Best Actress-Shabana Azmi
*Best Story-Mohan Kumar
*Best Dialogue Writer - Mushtaq Jalili

==References==
 

==External links==
* 

 
 
 
 