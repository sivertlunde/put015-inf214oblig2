The Flame (1947 film)
 The Flame}}
{{Infobox film
| name           = The Flame
| image          = The flame poster 1947.jpg
| image_size     = 160px
| alt            =
| caption        = Theatrical release poster
| director       = John H. Auer
| producer       = John H. Auer
| screenplay     = Lawrence Kimble
| story          = Robert T. Shannon
| narrator       =  John Carroll Vera Ralston Robert Paige Broderick Crawford
| music          = Heinz Roemheld
| cinematography = Reggie Lanning
| editing        = Richard L. Van Enger
| studio         = Republic Pictures
| distributor    = Republic Pictures
| released       =  
| runtime        = 97 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} John Carroll, Vera Ralston, Robert Paige and Broderick Crawford. 

==Plot==
A man (Carroll) induces an ambitious nurse (Ralston) to marry his rich brother (Paige) for the money who has a terminal disease.

==Cast== John Carroll as George MacAllister
* Vera Ralston as Carlotta Duval
* Robert Paige as Barry MacAllister
* Broderick Crawford as Ernie Hicks
* Henry Travers as Dr. Mitchell
* Blanche Yurka as Aunt Margaret
* Constance Dowling as Helen Anderson
* Hattie McDaniel as Celia
* Victor Sen Yung as Chang
* Harry Cheshire as The Minister
* John Miljan as Detective
* Garry Owen as Detective
* Eddie Dunn as Police Officer

==Reception==
===Critical response===
The critic at The New York Times panned the film, "The sole distinction of The Flame, a rambling, inept bit of claptrap which sidled into the Gotham yesterday, is the bleakly amusing fact that most of the performers seem either bored or amused with the whole thing. And no wonder. There is a grim, unimaginative which-brother-do-I-love plot, centering on Vera Ralston." 
 Hal Erickson discussed the production values in his brief review, "In terms of both budget and histrionic level, The Flame is one of the most lavish of Republic Pictures late-1940s productions." 

==References==
 

==External links==
*  
*  
*  
*  

 

 

 
 
 
 
 
 
 
 