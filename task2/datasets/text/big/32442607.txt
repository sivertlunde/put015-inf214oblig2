Hothat Brishti
 
{{Infobox film
| name           = Hothat Brishti
| image          =
| image_size     =
| caption        = The poster for the film
| director       = Basu Chatterjee
| producer       = Gramco Films (India) & Ashirbar Chalachchitra (Bangladesh)
| writer         =
| narrator       =
| starring       = Ferdous Ahmed Priyanka Trivedi
| music          = Nachiketa Chakraborty
| cinematography =
| editing        =
| distributor    =
| released       = 1998
| runtime        = 2:30 Hours
| country        = Bangladesh & India Bengali
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}
 Bengali  romance movie released in 1998 under the direction of Basu Chatterjee.  It was produced by both Bangladesh and India. The movie featured Ferdous Ahmed (Bangladesh) and Priyanka Trivedi (India).It was a huge success both in India and Bangladesh. It is a remake of the National award winning Tamil film Kadhal Kottai, and also Hindi movie Sirf Tum was remade based on the same story in June 1999.

==Plot==
Deepa Nandy (Priyanka Trivedi) lives in Taramati with her sister and brother in law Premendra (Raisul Islam Asad). Tired of being a burden on her relatives, Deepa goes to Kolkata to look for a job. She stays with her friend Aruna. Despite appearing at a number of interviews, she does not manage to get a job and decides to go back to Taramati. On her way back, a pickpocket snatches her bag from the train window. This bag contained all her certificates. The co-passengers chip in to pay for her ticket and she manages to reach home. Her brother-in-law makes a sarcastic remark on seeing her back.

Meanwhile Ajit Choudhury (Ferdous Ahmed) boards the train to go to Jaisalmer. Incidentally the pickpocket had thrown Deepas bag into the train after taking the money. Ajit finds the bag. During Ajits discussions with a co-passenger we come to know that Ajit is an orphan. He has a curious hobby of collecting peoples names and birthdays and sending them birthday cards.

Ajit arrives at Jaisalmer and meets Haradhan Kundu or Kundu Singh (Manoj Mitra). Kundu Singhs grandfather had come from Dhaka and set up a hotel. However, the business floundered and Kundus job became to help visitors get hotels etc. Kundu Singh arranges a room for Ajit. Here Ajit meets Rupmati, who lives next door.

Ajit sends Deepas bag with the certificates to her. Deepa is overjoyed and sends back a thank you note. This exchange of letters continues and Ajit and Deepa fall in love with each other. Meanwhile Rupmati (June Malia) is infatuated with Ajit and tries to learn Bengali phrases from Kundu Singh to impress Ajit. But when Kundu Singh tells Rupmati about Ajits postal affair with Deepa, Rupmati is infuriated and confronts Ajit.

Ajit calls up Deepa every Sunday and Deepa send a T-Shirt to Ajit so that she can recognize Ajit. Meanwhile, Deepa gets another interview call. Her brother in law Premendra arranges for the money to go to Kolkata and tells Deepas sister that he will be glad if Deepa gets a job and has already identified a match for her. Over in Jaisalmer, Ajit is asked by the company MD to take new responsibility in factory to manage union troubles. Union leaders threaten Ajit. Ajit tries to quit job and go back to Kolkata. The company gives him a transfer instead to Kolkata. Rupmati asks for his forgiveness before Ajit leaves for Kolkata.

Ajit bumps into Deepa (literally) at the train station but they dont recognize each other. Deepa harbours a dislike for Ajit and tries to avoid him even though they meet accidentally. Preeti (Sreelekha Mitra) is the daughter of the MD of Ajits company. Preeti flirts with Ajit. Ajit discourages Preeti and spurns her advances. Preeti gets a job in Singapore but does not want to leave Kolkata. Deepa goes back to Taramati. A letter from Deepa for Ajit gets into the hands of Preeti.

Ajit is not able to get a job and decides to drive a taxi in the meantime. Deepas brother-in-law brings a marriage proposal for Deepa. Deepa meets the proposed candidate and tells him about Ajit. Deepa goes to Kolkata to look for Ajit. She goes to look for Aruna but Aruna wasnt home. Deepa gets into Ajits taxi to go to Ajits former office but still does not recognize him. Preeti gives Deepa the address of Ajits former colleague and roommate. Deepa goes to meet Ajits former colleague, who happens to be Prabodh (Shahin Alam), who had once proposed to Deepa. Prabodh (Kunal) reassures Deepa that he will try to find Ajit.

Deepa gets drenched in rain and goes in Ajits taxi to his friends house. Ajit gives Deepa the sari he had bought for her without knowing her identity. As AJit was drneched too, he wears the T-shirt Deepa had sent him but covers it with a shawl. Deepa goes back to Arunas house but she had not returned. So Deepa decides to go back to Taramati.

Deepas friend gets into the taxi of Ajits friend. They come to know that their friends Ajit and Deepa love each other. Aruna gets the letter from Deepa and decides to go to the station to meet Deepa. Just as the train is leaving, Ajit removes his shawl and Deepa recognizes the T-Shirt. She runs into Ajits arms as the credits roll.

==Cast==
* Ajit Choudhury: Ferdous Ahmed
* Deepa Nandy: Priyanka Trivedi
* Premendra Nandy: Raisul Islam Asad
* Kundu Singh: Manoj Mitra
* Sourabh: Shahin Alam
* Preety: Sreelekha Mitra
* Rupamati: June Malia
* Probodh: Kunal
*   (Cameo appearance)

==Crew==
* Producer(s): Gramco Films & Ashirbad Chalachchitra
* Director: Basu Chatterjee
* Story: S. Ahathian
* Production Design: Gautam Bose
* Lyrics and Music: Nachiketa Chakraborty
* Editing : Satish Patnakar
* Cinematography: Shirsha Roy
* Singer: Nachiketa Chakraborty, Saoli Mitra, Shikha Bose, Swastika Mitra
<!--==Music==
 -->

==Critical reception==
Hothat Brishti was a huge success both in India and Bangladesh. This movie raised the leading Bangladeshi film star Ferdous Ahmed to stardom.
 

==References==
 

==External links==
*  

 
 
 
 


 