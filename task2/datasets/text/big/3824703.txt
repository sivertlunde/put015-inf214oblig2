The Big Broadcast of 1937
{{Infobox film
| name           = The Big Broadcast of 1937
| image          = BigBroadcast1937.jpg
| image_size     = 190px
| caption        = 
| director       = Mitchell Leisen
| producer       = Lewis E. Gensler
| writer         = 
| narrator       = 
| starring       = Jack Benny George Burns Gracie Allen
| music          = 
| cinematography = 
| editing        = 
| distributor    = Paramount Pictures
| released       = 
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Bob Burns, Martha Raye, Shirley Ross, Ray Milland, Benny Fields, Frank Forest and the orchestra of Benny Goodman (featuring Gene Krupa).  Uncredited roles include Jack Mulhall. The version shown in British cinemas also included clips featuring Richard Tauber and the Vienna Boys Choir, not in the original American version.

German animator Oskar Fischinger was hired to create an animation sequence in Technicolor. However, when Paramount changed the production to black-and-white only, Fischingers original abstract animation design was changed to a hybrid animation and live-action sequence showing consumer products emanating from a broadcasting tower, to the song "Radio Dynamics" by Ralph Rainger. 

It has yet to receive a DVD or VHS issue.

==Cast==
Jack Benny Jack Carson 
George Burns Mr. Platt 
Gracie Allen Mrs. Platt  Bob Burns Bob Black 
Martha Raye	Patsy 
Shirley Ross Gwen Holmes 
Ray Milland	Bob Miller

==Films in series==
*The Big Broadcast (1932)
*The Big Broadcast of 1936 (1935)
*The Big Broadcast of 1938 (1937)

==References==
 

==External links==
* 

 

 
 
 
 
 
 


 