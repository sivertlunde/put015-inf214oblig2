Madame Aema 6
{{Infobox film
| name           = Madame Aema 6
| image          = Madame Aema 6.jpg
| caption        = Theatrical poster for Madame Aema 6 (1992)
| film name      = {{Film name
 | hangul         =   6
 | hanja          =   6
 | rr             = Aemabuin 6
 | mr             = Aemapuin 6}}
| director       = Suk Do-won 
| producer       = Choe Chun-ji
| writer         = Suk Do-won
| starring       = Da Hui-a
| music          = Kim Eun-gyu
| cinematography = Ham In-ha
| editing        = Cho Ki-hyung
| distributor    = Yun Bang Films Co., Ltd.
| released       =  
| runtime        = 
| country        = South Korea
| language       = Korean
| budget         = 
}} 
Madame Aema 6 (hangul|애마부인 6 - Aema Buin 6) is a 1992 South Korean film directed by Suk Do-won. It was the sixth entry in the Madame Aema series, the longest-running film series in Korean cinema. 

==Plot==
In this entry in the long-running   with her daughter and memories of unrequited love. The fifth generation tries to console the fourth generation Aema, while dealing with her own issues of isolation after declaring herself an independent woman. Sixth generation Aema is undergoing marital difficulties with an unfaithful husband whom she eventually divorces.   

==Cast==
* Da Hui-a: 6th Generation Aema 
* Ju Ri-hye: 4th Generation Aema
* So Bi-a: 5th Generation Aema
* Dokgo Young-jae: Hyeon-woo
* Won Seok
* Maeng Chan-jae: Suk-woo
* Sue Young-suck
* Yoo Seong
* O He-chan
* Sin Jin-hui

==Bibliography==

===English===
*  
*  
*  

===Korean===
*  
*  
*  

==Notes==
 
 
 
 
 
 
 


 
 