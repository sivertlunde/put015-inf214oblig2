Bandolero!
 
 
:For the bullet belt, see Bandolier.

{{Infobox film
| name           = Bandolero!
| image          = Bandolero! (movie poster).jpg
| caption        = Film poster by Tom Chantrell
| director       = Andrew V. McLaglen Stanley Hough (Story)  James Lee Barrett (Screenplay)
| starring       = James Stewart Dean Martin Raquel Welch George Kennedy Will Geer Denver Pyle
| producer       = Robert L. Jacks
| music          = Jerry Goldsmith
| cinematography = William H. Clothier
| editing        = Folmar Blangsted
| distributor    = 20th Century Fox
| released       =  
| runtime        = 106 minutes
| country        = United States
| language       = English
| budget         = $4.45 million 
| gross          = $12,000,000 
}} western directed by Andrew V. McLaglen starring James Stewart, Dean Martin, Raquel Welch and George Kennedy. The story centers on two brothers on a run from the posse, led by a local sheriff (July Johnson) who wants to arrest the runaways and free a hostage (Raquel Welch) that they took on the way. They head on the wrong territory, which is controlled by "Bandoleros".

==Plot==
 
Posing as a hangman, Mace Bishop arrives in town with the intention of freeing his brother Dee from the gallows. Dee and his gang have been arrested for a bank robbery in which Maria Stoners husband was killed by gang member Babe Jenkins. After freeing his brother, Mace successfully robs the bank on his own after the gang has fled with the posse in pursuit.

Dee has taken Maria as a hostage after they come across her wagon, during which Gang member Pop Chaney shoots and kills the man escorting Maria. The posse, led by local sheriff July Johnson and deputy Roscoe Bookbinder, chases the fugitives across the Mexican border into territory policed by bandoleros, whom Maria describes as men out to kill any gringos (foreigners) that they can find. Maria further warns Dee that the sheriff will follow, because they have taken the one thing that July Johnson has always wanted: her.

Despite initial protestations, Maria falls for Dee and finds herself in a quandary. She had never felt anything for the sheriff, nor for her husband, who had purchased her from her family. The posse tracks them to an abandoned town and captures the gang. The bandoleros also arrive, shooting Roscoe, so the sheriff releases the outlaws so that the men can fight back in defense.

In this final showdown, almost everyone is killed. Dee is fatally stabbed by the leader of the bandits, El Jefe, and then Mace is shot by another. Babe and gang member Robbie Ohare die after killing numerous bandoleros. Pop Chaney is killed while going after the money Mace stole, and his son, Joe, dies after trying to rescue him.  Maria grabs a pistol and shoots El Jefe dead. Maria and the sheriff, with little left of the posse, bury the Bishop brothers and dead posse members without markers, after which Maria notes that no one will know who was there nor what had happened.

==Production== The Alamo.  The Alamo Village is located north of Brackettville, Texas. The location closed in 2009 after remaining open to movie companies and the public since 1960.

Larry McMurtry, the author of the novel Lonesome Dove, reportedly paid homage to Bandolero! by using similar names for the characters in his book. Both tales begin near the Mexico border and involve bandoleros. Both have a sheriff named July Johnson and a deputy Roscoe who travel a great distance in search of a wanted criminal and the woman who has rejected the sheriffs love. Both stories have a charismatic outlaw named Dee, who is about to be hanged and who wins the love of the woman before he dies. In the Lonesome Dove miniseries, the main characters twice pass directly in front of the Alamo—or at least a set built to replicate the Alamo.

==Cast==
*James Stewart... 	Mace Bishop
*Dean Martin	... 	Dee Bishop
*Raquel Welch... 	Maria Stoner
*George Kennedy... Sheriff July Johnson
*Andrew Prine... Deputy Sheriff Roscoe Bookbinder
*Will Geer... Pop Chaney
*Clint Ritchie... Babe Jenkins
*Denver Pyle	... Muncie Carter
*Tom Heaton... Joe Chaney
*Rudy Diaz... Angel
*Sean McClory... Robbie OHare
*Harry Carey, Jr. ... Cort Hayjack (billed as Harry Carey)
*Don "Red" Barry... Jack Hawkins (billed as Donald Barry)
*Guy Raymond... Ossie Grimes
*Perry Lopez... Frisco
*Wilford Brimley ... Stunts
*John Mitchum ... bath house customer

==Reception==
The film earned North American rentals of $5.5 million in 1968. 

==See also==
* List of American films of 1968

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 