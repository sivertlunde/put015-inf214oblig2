Slave Women of Corinth
{{Infobox film
 | name =Slave Women of Corinth
 | image =Slave Women of Corinth.jpg 
 | image_size = 256px
 | director = Mario Bonnard
 | writer =  Mario Bonnard   Alberto Manca  Mario di Nardo  Sergio Leone Ugo Moretti
 | starring =  Isabelle Corey  Anthony Steffen
 | music = Giovanni Fusco
 | cinematography =  Tino Santoni
 | editing =   
 | producer = 
 | released = 1958
 | country = Italy
 | language       = Italian
}} 1958 Cinema Italian Epic epic historical drama film written and directed by Mario Bonnard.      

== Plot ==
Antigonus, archon of Corinth, wants to build a magnificent temple dedicated to the goddess Aphrodite, for which the people are  oppressed by new and very high taxes. The sculptor Demetrius, asked to make the face of the goddess Diana (mythology)|Diana, falls in love with a slave, the Christian Lerna.  With the arrival of the plague the people are increasingly discontented and Antigonus has easy game accusing the Christians who are immediately arrested and sentenced to death. The Roman troops arrive to restore order, saving the two lovers Demetrius and Lerna.

== Cast ==

* Isabelle Corey: Lerna 
* Anthony Steffen: Demetrius (credited as Antonio De Teffe) 
* Irène Tunc: Diala 
* Clara Calamai: Stenele 
* John Kitzmiller: Tomoro 
* Ivo Garrani: Antigonus
* Massimo Serato: Quinto Rufo 
* Giulio Donnini: Erasto 
* Carlo Tamberlani: Matteo 
* Gianpaolo Rosmino: Dineo (credited as Giampaolo Rosmino) 
* Andrea Aureli: Kibur 
* Matteo Spinola: Glauco 
* Adriano Micantoni: Ftire 
* Mino Doro: Crepilo 
* Germano Longo: Osco  Paul Muller: LAsiatico (credited as Paolo Muller) 
* Livio Lorenzon: La Spia (credited as Silvio Lorenzon) 
* Liliana Gerace: La Veggente  
* Emma Baron: Onoria

==References==
 

==External links==
* 

 
 
 
 
 
   
 
   
 

  
 