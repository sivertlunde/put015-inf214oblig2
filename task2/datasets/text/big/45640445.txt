Bham Bolenath
 
 
{{Infobox film
| name           = Bham Bolenath
| image          = File:Bham_Bolenath_poster.jpg
| caption        = 
| director       = Karthik Varma Dandu
| producer       = Siruvuri Rajesh Varma
| writer         = 
| narrater       =
| story          = Karthik Varma Dandu
| starring       = Navdeep Pooja Zaveri
| music          = Sai Karthik
| cinematography = Bharani K. Dharan
| editing        = 
| studio         = R.C.C Entertainments
| distributor    = 
| released       = February 27, 2015
| runtime        = 
| country        = India
| language       = Telugu
| budget         = 
| gross          = 
}}
Bham Bolenath is a Telugu comedy film directed by Karthik Varma Dandu and produced by Siruvuri Rajesh Varma under R.C.C Entertainments.      It stars Navdeep and Pooja zaveri in the lead roles. The film was released on February 27, 2015.   lt received mixed reviews from critics.

==Cast==
* Navdeep as Vishnu
* Naveen Chandra as Krishna
* Pooja Zaveri as Sri Lakshmi
* Posani Krishna Murali as Sethji
* Pradeep Machiraju as Rocky
* Kireeti Damaraju as Roshan
* Thagubothu Ramesh
* Fish Venkat
* Pankaj Kesari
* Naveen Neni

== References ==
 

== External links ==
* 
 
 
 