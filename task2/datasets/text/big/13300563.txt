Another Gay Sequel: Gays Gone Wild!
{{Infobox Film
| name = Another Gay Sequel: Gays Gone Wild!
| image = Another gay sequel gays gone wild.jpg
| image_size = 215px
| alt = 
| caption = Theatrical release poster
| director = Todd Stephens
| producer = Eric Eisenbrey Todd Stephens Jonah Blechman Derek Curl
| screenplay = Todd Stephens
| story = Todd Stephens Eric Eisenbrey Scott Thompson Brent Corrigan Colton Ford
| music = Marty Beller
| cinematography = Carl Bartels
| editing = Spencer Schilly
| studio = Luna Pictures Caveat Pictures
| distributor = TLA Releasing
| released =  
| runtime = 99 minutes 
| country = United States Germany
| language = English Spanish
| budget = $500,000
| gross = $104,828
}} Scott Thompson (Mr. Wilson), Stephanie McVay (Mrs. Hunter), and Andersen Gabrych. It was released in only seven theaters and ran for only 10 weeks before going to DVD. 

Nancy Sinatra, who sang the song "Another Gay Sunshine Day" for the first film, receives Special Thanks. Singer RuPaul announced on his website that he and Lady Bunny had recorded a song for the soundtrack that would be released as a single, but the producers of the film ultimately scrapped that song and instead utilized a solo by Lady Bunny. Further, that song was not released as a single, instead replaced by the song "The Clap" by Perez Hilton. The duet of RuPaul and Lady Bunny was later released as a bonus track on RuPauls album Champion (RuPaul album)|Champion.

==Plot==
Dorky Andy (Jake Mosser), flamboyant Nico (Jonah Blechman), jock Jarod (Jimmy Clabots), and nerdy Griff (Aaron Michael Davies) reunite in Fort Lauderdale for spring break. The plot revolves around a contest -- "Gays Gone Wild!"—to see who can have sex with the most guys during the duration of spring break. The winner will be crowned "Miss Gay Gone Wild".
 Brent Corrigan). Jasper (Will Wikle, Brand Lim, and Isaac Webster) seem to be very anxious to win the contest by any means.

In a subplot, the guys meet Perez Hilton on an airplane and Hilton pursues a young priest to the bathroom, he hits his head, turning him into a religious zealot trying to dispel the gay activities going on; he is later hit in the head again and changes back.

==Cast==
* Jonah Blechman as Nico Hunter
* Jake Mosser as Andy Wilson
* Aaron Michael Davies as Griffin "Griff"
* Jimmy Clabots as Jarod
* Euriamis Losada as Luis
* Ashlie Atkinson as Dawn Muffler
* Perez Hilton as Himself
* RuPaul as Tyrell Tyrelle Scott Thompson as Mr. Wilson
* Lady Bunny as Sandi Cove
* Will Wikle as Jasper
* Brandon Lin as Jasper Chan
* Isaac Webster as Jasper Pledge / Fake Griff Brent Corrigan as Stan the Merman
* Lypsinka as Mrs. Wilson
* Amanda Lepore as Debbie Gottakunt
* Willam Belli as Nancy Needatwat
* Colton Ford as Butch Hunk
* Jim Verraros as Singing Priest Michael Lucas as Pizza Boy
* Stephanie McVay as Bonnie Hunter Edge of Seventeen)
* Jeffrey Coon as Surfer Boy
* Ellen Jacoby as Crusty Nurse
* Manny Santilice as Altar Boy
* Eric Eisenbrey as Gay Nerd
* Barbara Marr as Madge
* Henny Levine as Jewish Lady
* Rhoda Gelman as Jewish Lady
* Bobbi Mar as Jewish Lady
* Cheri as Jewish Lady
* Rochel "Tiby" Pass as Jewish Lady
* Carlos Duques as Gorgeous Porn Star
* Tony Dee as Persnickety Waiter
* Franco Sperduti as Jasper Bear
* Frank Hagerty as The Skipper
* Bruce Simpson	as Gilligan
* Yos Menendez as Random Trick
* Laurent Hennaut as Police Officer
* Jordan Jaric & Aden Jaric as	Vana Whites
* Jody Travis Thompson (credited as Jody Thompson-Adams) as Piss Pig Top
* Jesse Adams (credited as Jesse Thompson-Adams) as Piss Pig Bottom
* Mike Jones as Zombie Puckov
* Jackson Wilde as Hockey Dude
* Jorge Ortiz as Tiki Rim Dude
* Dallas Reeves as Jaspers Paramour
* Neo as Jaspers Paramour
* Tommy Blade as Jaspers Paramour
* Kevin Wipplinger as Andy Water Ski Double
* Phillip Calderone as Luis Water Ski Double
* Anthony Anselmi as Fake Jarod
* Nathan Buffaloe as Fake Andy
* Miss Kitty as Senior Drag Queen

==Production== Frameline Film Festival in San Francisco on June 28, 2008.   
 The Wizard Jonathan Chase), and Griff (Mitch Morris) in the first film killed, semi-explaining the actors absence from the sequel. Mrs. Hunter (Stephanie McVay) later said that "doing two gay movies in a row will make people think youre actually gay", an allusion as to what the actors agents may have said.

==Critical reception==
The film received 20% on the Rotten Tomatoes meter. 

==References==
 

==Further reading==
* Padva, Gilad. Boys Want to Have Fun! Carnivalesque Adolescence and Nostalgic Resorts in Another Gay Movie and Another Gay Sequel. In Padva, Gilad, Queer Nostalgia in Cinema and Pop Culture, pp.&nbsp;98–122 (Palgrave Macmillan, 2014, ISBN 978-1-137-26633-0).

==External links==
* 
* 
* 
* 
*  at  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 