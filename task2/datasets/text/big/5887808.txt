Johnny (2003 film)
 
 

{{Infobox Film | 
  name     = Johnny |
  image          = johnnyposter.jpg |
  writer         = Pawan Kalyan |
  starring       = Pawan Kalyan Renu Desai Raghuvaran |
  director       = Pawan Kalyan |
  producer       = Allu Aravind |
  distributor    = Geetha Arts  KAD Movies |
  released       = 25 April 2003 |
  runtime        = 178 min|
  editing        = Yousuf Khan |
  cinematography = Chota K. Naidu  Shyam Palav | Telugu |
  music          = Ramana Gogula |
|  budget         =   
| gross          =  (Share)
}} Tollywood sports film released on 25 April 2003, written and directed by Pawan Kalyan. Kalyan also starred in the film, along with Renu Desai. The film didnt perform well commercially, although it recovered its budget. Box Office India declared it a Box office bomb|flop. Also, it proved to be the biggest flop in Pawan Kalyans film career. Johnny s distribution rights were sold for a record breaking amount of  . 

== Plot ==
Johnny (Pawan Kalyan) loses his mother as a kid. He then runs away from his father (Raghuvaran) as he is a chain smoker, a drinker, and doesnt care for his son. Years later, Johnny becomes a martial arts coach. One day, a girl named Geeta (Renu Desai) lodges a complaint on Johnny for beating up a man. Later, Geeta comes to know that it was a misunderstanding. After Geeta apologises, both become friends and eventually get married. After marriage, Johnny finds out that his wife has cancer. Borrowing as much money as he can from his friends, he moves to Mumbai but he is still short.

Johnny learns from a stranger that there are boxing fights every day. At first, Johnny refuses the offer but when he sees the condition of his wife, he accepts the offer and goes through kickboxing competitions to raise money. Geetas condition deteriorates to the point that Johnny needs Rs.   overnight. The owner of the boxing fights makes a deal with Johnny that he must fight two professional martial artists to get the amount overnight. The rest is how he defeats them.

== Cast ==
* Pawan Kalyan ... Johnny
* Renu Desai ... Geetanjali
* Geeta ...  (Johnnys mother)
* Raghuvaran ...Ravishankar Damle Johnnys father
* Lillete Dubey ... Johnnys step-mother
* MS Narayana ... Lingam
* Mallikarjuna Rao ... Anthony
* Ali ... Pathan Babu Lal
* Satya Prakash... Karate Satti
* Brahmaji... S.I.
* Raza Murad (credited as Raja Murad) ...Tatya
* Harish Pai... Pandu
* Yaadi...Dayanand
* Aryan Khan(Parvez)...Nazir
* Shoaib...Laddu yadav
* Usman...Mahesh

== Crew ==
* Director: Pawan Kalyan
* Story: Pawan Kalyan
* Screenplay: Pawan Kalyan
* Dialogue: Satyanand
* Producer: Allu Aravind
* Music: Ramana Gogula
* Art Direction: Anand Sai
* Editor: Yousuf Khan
* Costume Designer: Renu Desai
* Cinematography: Chota K. Naidu & Shyam Palav

==Soundtrack==
The music for the film was composed by Ramana Gogula.
 1. Go Johnny
 2. Dojo music
 3. Nuvvu Saara
 4. Ravoyi Maa Country ki
 5. Dharmardha Kamamulalona
 6. Cool and Lovely
 7. Naraaz Kaakuraa
 8. Ee Reyi Teeyanidi
 9. Ye Chota Nuvvunna
 10. Naalo Nuvokasagamai

==Distribution rights==
Johnny worldwide distribution rights were sold for a record breaking amount of  . http://www.rediff.com/movies/2003/may/17pawan.htm  Johnny was released in 325 Theaters worldwide with digital print quality.

==Box office ==
The film didnt perform well commercially, although it recovered its budget. Box Office India declared it as a flop. Its distribution rights were sold for  . But the film only collected   share. 

==References==

 

== External links ==
 

 
 
 
 
 