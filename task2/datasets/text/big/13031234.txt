I Want You (1998 film)
{{Infobox film
| name           = I Want You
| image = I Want You 1998.jpg
| caption = 
| director       = Michael Winterbottom
| producer       = Andrew Eaton
| writer         = Eoin McNamee
| starring       = Rachel Weisz Alessandro Nivola Luke Petrusic [[Labina M
itevska]] Ben Daniels Carmen Ejogo Kenny Doughty Paul Popplewell Adrian Johnston
| cinematography = Sławomir Idziak
| editing        = Trevor Waite
| studio         = PolyGram Filmed Entertainment
| distributor    = Gramercy Pictures
| released       =  
| runtime        = 87 minutes
| country        = United Kingdom
| language       = English
| budget         =
| gross          = 
}}

I Want You is an 1998 English crime film directed by Michael Winterbottom.

==Plot==
Martin (Nivola) is an ex-convict who returns home and finds that Helen (Weisz), his former girlfriend, is involved with someone else. Despite this, he pursues her.

==Cast==
*Rachel Weisz.....Helen
*Alessandro Nivola.....Martin
*Luke Petrusic.....Honda
*Labina Mitevska.....Smokey
*Ben Daniels.....Bob
*Carmen Ejogo.....Amber

==Awards and nominations== Berlin Film Festival   
*1998: Won, "Special Mention" - Sławomir Idziak
*1998: Nominated, "Golden Berlin Bear Award" - Michael Winterbottom

Camerimage
*1998: Nominated, "Golden Frog Award" - Slawomir Idziak
 Valladolid International Film Festival
*1998: Won, "Youth Jury Award" - Michael Winterbottom

==References==
 

==External links==
* 
*  
* 

 

 
 
 
 
 
 
 
 


 