The Distortion of Sound
{{multiple issues|
 
 
}}
{{infobox film
| name           = The Distortion of Sound
| image          = The Distortion of Sound.png
| alt            =
| caption        =
| director       = Jacob Rosenberg
| production company       = Bandito Brothers
| producer       = Hana Leshner  Dennis McKinley Suzanne Hargrove
| writer         = Micheal Abell Kevin Gentile Carol Martori
| screenplay     =
| story          = 
| based on       = Slash Quincy Jones Mike Shinoda Lianne La Havas Snoop Dogg Hans Zimmer
| narrator       = 
| music          = Lorne Balfe
| cinematography =
| editing        = Carol Martori
| distrubutor    = 
| released       =  
| runtime        = 22 minutes
| language       = English
| budget         = 
| gross          =
}}
 documentary film about the decline of sound quality for recorded music, narrated by various artists who explain the decline in the sound quality from the last two decades. It was directed by Jacob Rosenberg, written by Michael Abell and Kevin Gentile, and produced by Hana Lasber, Dennis McKinley, and Suzanne Hargrove. The film features interviews with vocalists, guitarists, producers, writers, rappers, film composers, mixing engineers, mixers, music journalists, acoustic researchers, loudspeaker engineers and chief engineers—including such artists as Mike Shinoda of Linkin Park, Slash (musician)|Slash, Quincy Jones, Snoop Dogg and Steve Aoki.  

The movie was presented by Harman International Industries and is available on YouTube on the channel with the same name of the film. The trailer of the film was also available on YouTube. The film, as well as the trailer, was available on the official website of the film.    The film is also available for purchase on Google Play. 

The film was premiered in Los Angeles on July 10, 2014.  

== Plot ==
Saying that compressed music, MP3s and Streaming Media|streaming, have diminished the quality and flattened the emotion. It also plots about the decline of sound quality, and how technology has changed the way people listen to music.

==Cast==
The film includes the following various musical artists: 
* Quincy Jones Slash
* Snoop Dogg 
* Steve Aoki 
* Lianne La Havas
* Mike Shinoda
* Hans Zimmer
* Kate Nash
* A. R. Rahman
* Dan the Automator 
* Manny Marroquin
* Andrew Scheps 
* Neil Strauss 
* Kate Nash 
* Dr. Sean Olive
* Greg Timbers
* Chris Ludwig

==Clips==
The original film included clips of interviews. All the clips were uploaded separately on the official YouTube channel in the given sequences.
{{tracklist
|extra_column= Presenter
| title1   = Introduction of the CD  Slash
| length1  = 1:15
| title2   = The Artists Intention 
| extra2   = Mike Shinoda
| length2  = 1:07
| title3   = Musicians 
| extra3   = Hans Zimmer
| length3  = 0:28
| title4   = Physical Music 
| extra4   = Kate Nash
| length4  = 1:02
| title5   = Head Bob Test 
| extra5   = Andrew Scheps
| length5  = 1:07
| title6   = Shared Social Experience 
| extra6   = Mike Shinoda
| length6  = 2:14
| title7   = Importance of Sound 
| extra7   = Hans Zimmer
| length7  = 1:00
| title8   = Compressed Music 
| extra8   = Andrew Scheps
| length8  = 1:52
| title9   = The Digital Age 
| extra9   = Kate Nash
| length9  = 1:32
| title10  = Connecting with Fans 
| extra10  = Lianne La Havas
| length10 = 1:24
| title11  = MP3 
| extra11  = Neil Strauss
| length11 = 1:06
}}

==Music==
The original score for the film was composed by Lorne Balfe.  Whereas the film used only three songs out of which one is the collaboration between Linkin Park and Steve Aoki, and the other two songs are performed by Lianne La Havas. Whereas the song "Holding Company (Lost in the Echo 2011 Demo)" from the LP Underground XIII extended play was briefly used in the trailer for the film.

{{tracklist writing_credits = yes extra_column = Performer(s)
| title1  = A Light That Never Comes
| writer1 = Linkin Park, Steve Aoki
| extra1  = Linkin Park x Steve Aoki
| length1 = 3:49
| title2  = Ghost of Me
| writer2 = Lianne La Havas, Matthew Hales
| extra2  = Lianne La Havas
| length2 = 4:14
| title3  = Is Your Love Big Enough?
| writer3 = Lianne La Havas, Matt Hales, Willy Mason
| extra3  = Lianne La Havas
| length3 = 3:22
}}

==Reference==
 

 
 
 
 
 
 
 