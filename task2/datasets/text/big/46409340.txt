The Gasman (film)
{{Infobox film
| name = The Gasman
| image =
| image_size =
| caption =
| director = Carl Froelich
| producer =  Carl Froelich
| writer =  Heinrich Spoerl
| narrator =
| starring = Heinz Rühmann   Anny Ondra   Walter Steinbeck
| music = Hanson Milde-Meissner   
| cinematography = Reimar Kuntze  
| editing =    Gustav Lohse   Johanna Rosinski    
| studio = Tonfilmstudio Carl Froelich UFA
| released = 1 August 1941
| runtime = 85 minutes
| country = Germany  German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
The Gasman (German:Der Gasmann) is a 1941 German comedy film directed by Carl Froelich and starring Heinz Rühmann, Anny Ondra and Walter Steinbeck.  The films sets were designed by Walter Haag. It was made by Forelichs separate production unit, and distributed by the major studio Universum Film AG|UFA.

==Cast==
*  Heinz Rühmann as Hermann Knittel  
* Anny Ondra as Erika Knittel  
* Walter Steinbeck as Herr - der nicht erkannt sein möchte  
* Erika Helmke as Blondes Fräulein Lilott  
* Will Dohm as Schwager Alfred  Franz Weber as Gerichtsvorzitsender  
* Kurt Vespermann as Staatsanwalt  
* Hans Leibelt as Prominenter Verteidiger  
* Charlotte Susa as Schöne Zeugin  
* Gisela Schlüter as Entzückende kleine Frau  
* Herbert Bach as Kellner 
* Luise Bethke-Zitzman as Knittels Nachbarin 
* Paul Bildt as Nervenarzt Dr. Brauer 
* Fritz Draeger as Mann im Zugabteil 
* Paul Esser   
* Hugo Froelich as Taxifahrer 
* Wilhelm Große as Auskunftsbeamter  
* Wolfgang Heise 
* Bruno Hellwinkel as Polizeibeamter  
* Otto Krieg-Helbig as Polizeibeamter 
* Wilhelm P. Krüger as Flickschneider  
* Walter Lieck as Bankdiener   Manfred Meurer as Verdächtiger Jüngling  
* Herta Neupert as Mopsgesicht  
* Erik Radolf as Kriminalbeamter  
* Marga Riffa as Beamtin der Sittenpolizei  
* Rolf Rolphs as Polizeibeamter  
* Max Rosenhauer 
* Oscar Sabo as Pensionierter Schutzmann  
* Ernst Stimmel as Bankdirektor  
* Eleonore Tappert as Zimmervermieterin  
* Eva Tinschmann as Frau Maschke  
* Hans Ulrich as Bankangestellter  
* Egon Vogel as Beamter für Verkehrsunfälle  
* Ewald Wenck as Kriminalbeamter  
* Hermine Ziegler as Frauenberaterin Janette Knoll  
* Bruno Ziener as Vornehmer alter Herr  
* Reinhard Kolldehoff as Polizeibeamter 
* Kurt Seifert as Finanzbeamter  
* Helmut Weiss as Romantischer Jüngling

== References ==
 

== Bibliography ==
* Bock, Hans-Michael & Bergfelder, Tim. The Concise CineGraph. Encyclopedia of German Cinema. Berghahn Books, 2009.

== External links ==
*  

 

 
 
 
 
 
 
 

 