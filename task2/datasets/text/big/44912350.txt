Delivery (2005 film)
{{Infobox film
| name           = Delivery
| image          = Delivery film poster.jpg
| alt            = 
| caption        = film poster
| film name      =  
| director       = Till Nowak
| producer       = Till Nowak
| writer         = Till Nowak
| screenplay     = 
| story          = 
| based on       =  
| starring       = 
| narrator       =  
| music          =  	
| cinematography = Till Nowak
| editing        = Till Nowak FrameboX
| distributor    = KurzFilmAgentur Hamburg
| released       =  
| runtime        = 9 minutes
| country        = Germany
| language       = English
| budget         = 
| gross          =  
}}
 German digital digital and visual artist, graphic designer and filmmaker Till Nowak, created as his thesis film project.   

==Background==
Nowak wrote and produced the film in 2005 as his thesis project at the University of Applied Sciences in Mainz. {{cite news
|last1=John
|first1=Camilla
|title=Till Nowak: der Meister des Alltäglichen
|url=http://www.abendblatt.de/hamburg/article2075333/Till-Nowak-der-Meister-des-Alltaeglichen.html
|accessdate=3 January 2014
|language=German
|publisher=Hamburger Abendblatt
|date=29 October 2011}}  {{cite web
|last1=Dixit
|first1=Amit
|title=Interview with Till Nowak
|url=http://cg-india.com/cgartists/till_nowak.html
|date=15 April 2006
|work=CGIndia
|accessdate=1 January 2014}}   He had no expectation that anything would develop from releasing the film, and stated "I didn’t expect this little film to change my life, but it all went really crazy. At first, when I entered it only into some small festivals, I didn’t have big plans for it. I just thought that one or two festivals would be nice to show the film and meet some people. Then the film won awards in the first three festivals and I recognized that it had much more potential. Over three years I travelled to Australia, Korea, USA, Spain and many other countries and until now the film has been screened in more than 200 festivals. I have sold it to some TV stations in some countries and won more than 35 awards."    These events caused Nowak to change his professional focus from commercial advertising work to independent art and film. {{cite web
|last1=staff
|title=Interview with Till Nowak
|url=http://www.itsartmag.com/features/tillnowak/tillnowak-p1.html
|work=Its Art Magazine AFI Fest Friedrich Wilhelm Murnau Kurzfilmpreis, and a nomination from the European Film Awards. {{cite web
|last1=staff
|title=Interview with Digital Artist Till Nowak
|url=https://www.youtube.com/watch?v=tWdM2D9pXcQ
|date=2 October 2012
|work=Vernissage TV
|accessdate=22 December 2014
|format=video}}  {{cite news
|last1=staff
|title=Till Nowak on the making of The Centrifuge Brain Project
|url=http://filmnosis.com/interviews/till-nowak-on-the-centrifuge-brain-project/
|accessdate=22 December 2014
|publisher=Filmnosis
|date=November 2013}}   He had first shared the film on an internet forum discussing simulated 3-D in film, the resulting attention included hundreds of emails with job offers and festival invitations. {{cite news
|last1=staff
|title=Preisgekrönt Der Mainzer Grafikdesigner Till Nowak
|url=http://www.framebox.de/images_content/press/max.jpg
|accessdate=2 January 2014
|language=German
|pages=130
|format=archived PDF
|publisher=Max (German magazine)|Max}} 

==Plot==
A lonely senior lives in a gloomy and dirty factory town and dedicates himself to caring for a small potted plant on his balcony. The world around is dying, as is the flower on his plant. One day he receives a mysterious box in the mail, one which enables him to have impacting effects on his surroundings.

==Recognition== Audi Festival Die Woche in Australien Called Nowak a rising young star" and praised his film Delivery as a successful exploration.     Katja Sprenz of Schnitt praised the film in her review,    and Frankfurter Allgemeine Zeitung also praised the film, its story, and its creator.   

===Partial awards and nominations===
* 2005, Won both Short Award and Audience Award at AFI Fest     Prix UIP Ghent (European Short Film) at Ghent International Film Festival     Wiesbaden goEast   
* 2005, Won Hamburg Animation Award for Best Short Film at OFFF Barcelona    
* 2005, Won the Bergischer Filmpreis for Best Animation  
* 2006, Won Friedrich Wilhelm Murnau|Friedrich-Wilhelm-Murnau Short Film Award 
* 2006, Won Jean-Luc Xiberras Award for a First Film at Annecy International Animated Film Festival   
* 2006, Won 2nd place Childrens Jury Award at Chicago International Childrens Film Festival   
* 2006, Nominated for Best Short Film Award at European Film Awards     Hamburg International Short Film Festival 
* 2006, Won Prix du Conseil Général at List of film festivals in Europe#France|Pontault-Combault Short Film Festival San Sebastián Horror and Fantasy Film Festival   
* 2006, Won Best Animation at Tehran Short Film Festival    Fantasy Worldwide Film Festival    Crested Butte Film Festival Anifest Hungary    Durango Film Festival     Montecatini Filmvideo - International Short Film Festival   
* 2007, Won both Directors Choice Award and Excellence in Filmmaking at Sedona International Film Festival   
* 2007, Won Golden Glibb for Best Short Film at Weekend of Fear, Nuremberg, Germany

==References==
 

==External links==
*  
*   at the Internet Movie Database

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 