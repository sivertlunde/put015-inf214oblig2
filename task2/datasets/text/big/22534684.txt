The Golden Horde (film)
{{Infobox film
| name           = The Golden Horde
| image_size     =
| image	         = The Golden Horde FilmPoster.jpeg
| caption        =
| director       = George Sherman Robert Arthur and Howard Christie
| writer         = George Drayson Adams (writer) Harold Lamb (story)
| starring       = Ann Blyth David Farrar
| cinematography = Russell Metty Frank Gross	 	
| distributor    = Universal-International
| released       =  
| runtime        = 77 mins.
| country        = United States English
| gross          = $1.5 million (US rentals) 
}} 1951 Historical Historical Adventure David Farrar. Many of the exterior scenes were shot in Death Valley National Park, California, United States|USA.

==Plot synopsis== Marvin Miller) and his hordes. Shalimar hopes to defeat the conqueror by guile, whilst Sir Guy prefers to put up  a brave (if ultimately futile) fight.  Despite the mutual attraction between Shalimar and Sir Guy, their differing methods threaten any hope either may have of victory.

==Cast==
*Ann Blyth - Princess Shalimar David Farrar - Sir Guy of Devon
*George Macready - Raven the Shamen Henry Brandon - Juchi, son of Genghis Khan
*Howard Petrie - Tuglik Richard Egan - Gill Marvin Miller - Genghis Khan
*Donald Randolph - Torga

==See also==
*Golden Horde
*List of historical drama films of Asia

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 