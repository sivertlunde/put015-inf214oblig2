Chembarathi
 
{{Infobox film
| name           = Chembarathi
| image          = Chembarathi film.jpg
| caption        =
| director       = P. N. Menon (director)|P. N. Menon
| producer       = S. Krishnan Nair
| writer         = Malayattoor Ramakrishnan
| based on       = Madhu Roja Ramani Raghavan Sudheer
| music          = Devarajan Ashok Kumar
| editing        = Ravi Kiran
| studio         = New India Films
| distributor    =
| released       =  
| runtime        = 140 minutes
| country        = India
| language       = Malayalam
| budget         =
| gross          =
}} Madhu along with newcomers Roja Ramani, Raghavan and Sudheer in major roles. The film was an adaptation of Malayattoors short story "Lodge". Renowned film director Bharathan worked as art director in this film.

It was a notable critical and commercial success upon release. The film was remade in Tamil as Paruva Kaalam in 1974 with Roja Ramani repeating her role and Kamal Haasan playing the male lead.

==Cast== Madhu
* Roja Ramani (as Shobana) Raghavan
* Sudheer
* Kottarakkara Sreedharan Nair
* Rani Chandra
* Adoor Bhavani
* Sankaradi
* Adoor Bhasi
* Bahadoor
* Kuthiravattom Pappu
* Paravoor Bharathan
* Balan K. Nair Janardhanan
* Radhamani
* Sudharma
* Pathma
* P. O. Thomas
* Pathiyil Madhavan (guest)
* Vaidyar K. R. Velappan Pillai

==Sound track==
The film sound track album has 8 songs written by Vayalar Ramavarma and composed by G. Devarajan.
#	Chakravarthini Ninakku ...	KJ Yesudas	 P Madhuri	
#	Sharanamayyappaa Swamee ...	KJ Yesudas	
#	Kunukkitta Kozhi ...	P Madhuri	
#	Ambaadi Thannilorunni ...	P Madhuri	
#	Poove Polipoove ...	P Madhuri, Chorus	
#	Chakravarthini Ninakku   ...	P Madhuri	
#	Ambaadi Thannilorunni (Bit - Sad) ...	P Madhuri

==External links==
*  
*   at the Malayalam Movie Database

 
 
 


 