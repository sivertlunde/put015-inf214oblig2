Loss of Sensation
 
 
 

Loss of Sensation, alternatively titled Robot of Jim Ripple, Russian: «Гибель сенсации» («Робот Джима Рипль») is a 1935 Soviet science fiction sound film directed by Alexandr Andriyevsky.

The films plot is centered on an engineer Jim Ripple who invents universal robots to help workers, being himself from a workers family. He theorizes that cheap production will make all goods so cheap that Capitalism will fall. The workers do not share his view and his family considers him a traitor. A key element of his invention is a high-capacity capacitor that powers the robots. The government becomes interested in the invention because the robots can be used as a weapon as well. Ripple is given a top secret factory and funding so that he can produce robots. The robots are not autonomous or intelligent, and controlled either by radio or by sound of different frequency, for which purpose Ripple uses a saxophone. When being drunk he even makes the robots to dance.

At a day of a universal workers strike, the administration of a factory where the Ripples brother works, located in the same town where the robot producing plant is located, replaces workers with robots. A workers delegation visits the factory to see that there are no strike breakers, and finds that actually it is the robots who works. The meeting ends with an accident when Ripple tries to show the abilities of a robot to the workers, so that one of the workers dies. This sparks a conflict between workers and the plant administration assisted with the military. The military decides to use robots against the workers as a weapon. The robots are commanded by an officer sitting in a tank using a radio remote control device. Trying to prevent the hostilities Ripple tries to stop the robots with a saxophone, but unsuccessfully and gets killed.

Finally the workers gain control over the robots with their own remote control device, which they assembled covertly before, making necessary measurements on the robot assembling factory and researching the Ripples prototype robot "Micron", whom he left damaged at his home.

Although the film uses the abbreviation "R.U.R" for the robots, it is not based on the novel by Karel Čapek.

The film is as of 2013 in public domain.

==External links==
*   (Russian)

 
 
 
 
 


 
 