Nightbreaker (film)
{{Infobox film
| name           = Nightbreaker
| director       = Peter Markle
| image	=	Nightbreaker FilmPoster.jpeg
| producer       = Tony Garnett
| writer         = Howard L. Rosenberg and T. S. Cook
| starring       = Martin Sheen Emilio Estevez Joe Pantoliano Lea Thompson Peter Bernstein
| cinematography = Ronald Víctor García
| editing        = Stephen E. Rivkin
| distributor    = Turner Home Entertainment
| released       =  
| runtime        = 99 minutes
| country        = United States
| language       = English
| budget         =
}}

Nightbreaker (a.k.a. Advance to Ground Zero  in the United Kingdom) is a 1989 television movie starring Martin Sheen, Emilio Estevez, Joe Pantoliano and Lea Thompson.

==Plot==
Nightbreaker shows Dr. Alexander Brown (played by Sheen in framing scenes and Estevez in flashbacks) reflecting on his involvement in the exposure of American soldiers to radiation in the proving grounds in Nevada in the 1950s after he is approached by a man who is dying of cancer due to the tests.

==Cast==
* Martin Sheen as Alexander Brown (1980s)
* Emilio Estevez as Alexander Brown (1950s)
* Joe Pantoliano as Sergeant Jack Russell 
* Lea Thompson as Sally Matthews
* Melinda Dillon as Paula Brown
* Paul Eiding as Roscoe Cummings 
* Geoffrey Blake as Python  James Marshall as Barney Immerman

==External links==
* 

 

 
 
 
 
 
 


 