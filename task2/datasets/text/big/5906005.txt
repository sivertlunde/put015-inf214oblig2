The Cat Creeps
 
{{Multiple issues|
 
 
}}
{{Infobox film
| name           = The Cat Creeps
| image          = Thecatcreeps-1930-titlelobbycard.jpg
| image_size     =
| caption        = Title lobby card. John Willard
| producer       = Carl Laemmle, Jr.
| writer         = William J. Hurlbut Gladys Lehman Neil Hamilton
| music          = Heinz Roemheld
| cinematography = Hal Mohr
| editing        = Maurice Pivar
| distributor    = Universal Pictures
| released       =  
| runtime        = 71 min.
| country        = United States English
}}
 The Cat Neil Hamilton, Lilyan Tashman, Jean Hersholt, and Montagu Love, it is considered to be a lost film.

==Production==
A Spanish language|Spanish-language version, La Voluntad del Muerto, starring Lupita Tovar and directed by George Melford, was filmed by Universal Pictures at night on the same sets used for The Cat Creeps during the day.

In the documentary The Young and the Dead, the husband of the lead actress  creates a video tribute due to her illness and shows part of her memorial service. He states that he became her agent during the filming of the movie and married her shortly after filming ended.
 Elizabeth Patterson The Cat and the Canary.

==Preservation status==
No prints or negatives of The Cat Creeps are known to exist, only some stills and the soundtrack. A few clips do survive in a short film produced by Universal entitled Boo! (1932 film)|Boo! (1932). 

==See also==
*List of lost films
*List of incomplete or partially lost films

==References==
 

==External links==
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 


 