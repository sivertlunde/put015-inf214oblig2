Marry the Girl (1935 film)
Marry same title, written by George Arthurs and Arthur Miller. 

Nine of the twelve Aldwych plays had been adapted for film by 1935, with some of the leading roles played by members of the original stage company. Eight of these films were directed by Tom Walls and one by Jack Raymond.  The production companies for the earlier films in the series were British & Dominions Film Corporation and Gaumont British.  Marry the Girl, however, was filmed by British Lion Films with none of the original stars, except for Winifred Shotter reprising her stage role.

==Synopsis==
Wally Gibbs is sued for breach of promise by his former girlfriend Doris Chattaway. His current fiancée, Jane Elliott, breaks off their engagement. Wally ends up with Doris, and Jane pairs off with Wallys friend Hugh Delafield, who has been the Counsel for the Plaintiff in the lawsuit.

==Cast==
*Wally Gibbs – Sonnie Hale
*Doris Chattaway – Winifred Shotter 
*Hugh Delafield – Hugh Wakefield 
*Jane Elliott – Judy Kelly 
*Banks – C. Denier Warren 
*Cyril Chattaway – Kenneth Kove 
*Mrs Elliott – Maidie Hope 
*Bookmaker – Wally Patch 
*Judge – John Deverell 
*Counsel for the defence – Lawrence Anderson
::Source :  British Film Institute 

==Notes==
 

==External links==
*  

 
 
 
 
 
 
 
 
 


 