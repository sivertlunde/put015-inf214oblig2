Grandi cacciatori
 
{{Infobox film
| name           = Grandi cacciatori
| image          = 
| caption        = 
| director       = Augusto Caminito
| producer       = Augusto Caminito
| writer         = Augusto Caminito Antonino Marino
| starring       = Klaus Kinski
| music          = 
| cinematography = Romano Albani
| editing        = Nino Baragli
| distributor    = 
| released       = 1988
| runtime        = 98 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}

Grandi cacciatori is a 1988 Italian adventure film directed by Augusto Caminito and starring Klaus Kinski.   

==Cast==
* Thomas Attguargarvak - Cacciatore eschimese
* Roberto Bisacco - Hermann
* Deborah Caprioglio - Deborah Brian Cooper - Ufficiale
* Bob Crocket - Cacciatore Norvegese
* Moser Dangwa - Gubai
* Absalom Dhikinya - Shamano
* Nick Duggan - Bracconiere africano
* Graham Early - Bracconiere africano
* James Haas - Cacciatore norvegese
* Roger Heacham
* Michael Hoffmann - Guardia Albert Jones - Cacciatore africano
* Harvey Keitel - Thomas
* Klaus Kinski - Klaus Naginsky
* Steven Ningeok - Cacciatore eschimese
* Rossahn Peetok - Capo eschimesi
* Iris Peynado - Nera
* Yorgo Voyagis - Leader

==References==
 

==External links==
* 

 
 
 
 


 
 