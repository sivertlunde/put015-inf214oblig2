Up from the Depths
{{Infobox film
| name           = Up From the Depths
| image          =
| alt            =
| caption        =
| director       = Charles B. Griffith
| producer       = Jack Atienza Cirio H. Santiago
| writer         = Anne Dyer Alfred Sweeney
| narrator       =
| starring       = Sam Bottoms Suzanne Reed Virgil Frye Kedric Wolfe Oscar Forbes R. Lee Ermey
| music          = Russell OMalley
| cinematography =
| editing        = G. V. Bass
| studio         =
| distributor    = New World Pictures Shout Factory (DVD)
| released       =  
| runtime        = 75 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

Up From the Depths is a 1979 horror film directed by Charles B. Griffith. The film, along with many monster movies of the 1970s and 1980s is very similar to Jaws (film)|Jaws (1975).

==Plot==
The staff and vacationers at a first-class resort on the island of Maui are beginning to mysteriously disappear. A biologist believes that an underwater earthquake has caused a giant and very hungry dormant prehistoric fish to be released from his slumber. Voraciously the fish helps himself to a tourist buffet. Now its open season for the local fishermen to find the creature and kill it.

==Production==
Charles B Griffith later called the film a "terrible experience".
 We had it written by one of the typists or secretaries in the office who didn’t have any thoughts of becoming a writer. I think Roger did it to punish me, to send me out to The Philippines where I didn’t know what I was getting into. I was making an action picture, but The Philippines people were all so depressed, and they had made this goofy-looking fish with bug eyes. I told them that we’ll make it a comedy, and their eyes lit up! So I sent back a comedy on one plane, and I arrived on the next one. By the time I arrived, Roger had already cut 75 minutes out. As an editor would say, “That’s a set-up, that’s a payoff!”  

I was hired by Cormans production company after the film was returned from the Phillipines to add a few additional scenes to help salvage the film. This included shooting some B-roll fake shark monster footage off Seal Beach, CA. I was in the camera boat just out of the shot with my stuntman scuba diver who had a dorsal fin strapped to his back- that  looked like a giant fin from the camera position on shore. My diver had just entered the water when suddenly there was a great thrashing in the water near the "fake" dorsal fin, and I heard on the walkie-talkie from shore, "Keep it up that thrashing and splashing it looks great!!" Whooaa!, a REAL seven-foot Thresher shark had suddenly appeared near the boat and was raising havoc in a school of baitfish near us on the surface! I decided not to tell my diver after he surfaced. (Ted Boehler, 2nd unit cameraman).

==Release==
The film was released theatrically in the United States by New World Pictures in June 1979.

The film was released on VHS by Vestron Video in the 1980s.

The film was released on DVD and Blu-ray Disc|Blu-ray by Shout Factory in 2011 as a double feature alongside the similar Demon of Paradise.

==References==
 

==External links==
* 

 

 
 
 
 
 
 


 