This Summer at Five
{{Infobox Film
| name           = This Summer at Five
| image          = 
| image size     = 
| caption        = 
| director       = Erkko Kivikoski
| producer       = 
| writer         = Erkko Kivikoski Marja-Leena Mikkola Juha Tanttu
| narrator       = 
| starring       = Martti Koski
| music          = 
| cinematography = Virke Lehtinen
| editing        = Anja Kivikoski
| distributor    = 
| released       = 15 November 1963
| runtime        = 75 minutes
| country        = Finland
| language       = Finnish
| budget         = 
| preceded by    = 
| followed by    = 
}}

This Summer at Five ( ) is a 1963 Finnish drama film directed by Erkko Kivikoski. It was entered into the 14th Berlin International Film Festival.   

==Cast==
* Martti Koski - Juhani
* Tuula Elomaa - Ritva Järvinen
* Carita Gren - Dark-haired girl
* Pekka Juutilainen
* Päivi Kaasinen
* Milja Luukko - Nurse
* Pekka Sahenkari - Advertising man
* Kaarlo Wilska - Night guard at hospital

==References==
 

==External links==
* 

 
 
 
 
 
 
 