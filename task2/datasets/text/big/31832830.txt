The Secret Kingdom (film)
  British silent silent fantasy film directed by Sinclair Hill and starring Matheson Lang, Stella Arbenina and Eric Bransby Williams.  It is an adaptation of the novel The Hidden Fire by Bertram Atkey. A wealthy man acquires a mind-reading machine but is soon horrified to discover what people are really thinking.

It was reissued in 1929 under the alternative title of Beyond the Veil. 

==Cast==
* Matheson Lang - John Quarrain 
* Stella Arbenina - Mary Quarrain 
* Eric Bransby Williams - Philip Darent 
* Genevieve Townsend - The Secretary 
* Rudolph de Cordova - The Protege 
* Robin Irvine - The Son 
* Lilian Oldland - The Daughter 
* Frank Goldsmith - Henry

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 