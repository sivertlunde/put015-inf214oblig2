Poo (film)
 
 
{{Infobox film
| name = Poo
| image = Poo Poster.jpg
| caption = Theatrical release poster Sasi 
| writer = Thamizhselvan  Srikanth Parvathi Menon
| producer = Moser Baer  
| music = S. S. Kumaran
| cinematography = P. G. Muthiah
| Editing = Mathan Gunadeva
| studio = Moser Baer Entertainment Nesagee Cinemas
| released =  
| runtime = 
| country = India
| language = Tamil
| budget = 
}}
 Srikanth and Parvathi Menon in the lead roles. The film released in November 2008 to widespread critical acclaim, winning awards in international and regional film ceremonies.
 
==Plot==
The film is set in a fictional village, located somewhere between Rajapalayam and Sivakasi in southern Tamil Nadu. It entails two different dreams of two different people for one person. The main aspects of this film are of the selfless, deep love and admiration that Maari (Parvathi) develops for her cousin Thangarasu (Srikanth) and the love and expectations of a poor, hardworking father (Ram) to his son (Thangarasu). 

The film begins showing Maari happily married to her husband (Prabhakaran), a small shop owner. She leads an uncomplaining life with a smile on her face at all shades of events which fascinates even her husband. One day, she leaves alone to her village festival with the permission of her husband, who promises that he will follow her there after his business for the day has finished. When her mother asks why Maari came alone, she answers that she came early in order to see her cousin Thangarasu, much to her mothers dismay. On her way to see Thangarasu, she begins to reminisce of her past that includes her bold proclamation of her aim to be Thangarasus wife from her school days - a time when they used to play together - that develops into her shy, unrequited yearning for him in her teenage years - a time when Thangarasu is an engineering student while Maari works as a labourer in her villages fireworks company along with her best friend Cheeni (Inbanila), a practical girl who at first advises Maari to be realistic, but later seeing her passion for Thangarasu helps her out in many ways as she could. By that time, everyone in her village knows about her ardent love for Thangarasu except for him. After many attempts, she fails at trying to confess her love to him or to even talk to him openly without feeling shy while he is away in city or in her village. However, Thanagarasu comes to realise her love after many incidents where people he knows tells him of this. 

At the same time, the film shows Thangarasus father working as a cart driver and carrying labourer. He expects respect for his age from his young bosses and colleagues, but due to an unpleasant incident he presumably thinks that being rich and owning modern items such as a car only will bring him respect. He takes pride in the fact that his son is studying to be an engineer and dreams of big things about him. But, Thangarasu breaks his castle in the sky by saying that once he starts working, unless he joins a renowned company, his salary will be merely Rs.6000, not as large as Rs 35000 qw software engineers earn as he is just a mechanical engineer. This shatters the dreams of Thangarasus father who then resorts to alcohol and deems that he will never achieve the respect that he yearns. 

Thangarasus father is against the idea of Thangarasu marrying Maari as he believes that one cannot live happily and with respect just alone with higher education, but with money. Thangarasu, though initially after coming to know of Maaris faithful passion begins to reciprocate her feelings, but after finding out that the son of his best friend who had married his cousin had birth defections, he finds it not a good idea to marry someone in related by blood. Therefore, heartbreakingly agreeing to marry the proposed daughter of someone from a wealthy family that his dad approves of, thus choosing the dream of his father. Maari too agrees to marry someone else on a condition that her mother and brother must attend Thangarasus wedding as they initially refused to go after discovering that Thangarasu will not wed Maari. Maari happily marries while wishing that Thangarasu and his wife to become successful and Thangarasu to be happy just as she will try to be with her husband. 

Current day sees Maari entering Thangarasus house and sees a car parked outside and also Thangarasu talking to someone on mobile about business related matters. Seeing her come, he directs her to go inside. She comes into the house and sees it decorated with latest modern devices. But nothing made her happier than seeing Thangarasus wedding photo hung on the wall. She sees Thangarasus wife in the backyard sitting on a chair reading a novel. She goes upto her and talks. Initially nice, his wife sneers later at the question of possible pregnancy making Maari shocked. She later witnesses in a hidden place, that Thangarasus wife treats him badly and shames him for marrying her shamelessly, a rich woman, when he himself is poor. Though, begging his wife to stop shouting, in fear that Maari may hear and knowing that she still lives for him and hearing this will terribly traumatize her, he sees that she is hiding listening to all this with tears in her eyes. He then goes away with shattered heart to his room while Maari runs away and meets Thangarasus father outside, who heeds to the situation seeing his arrogant daughter-in-law outside and his crying son inside, is also heartbroken. He explains to Maari saying that we both had a dream that we wanted, but I did not stop to hear the dream of that girl, by this indirectly quoting that had he, Thangarasu would not be in this unhappy marriage right now. 

Maari flees from the scene and departs with her belonging back on the way she came just as her husband touches his feet into the village. He discovers his Maari too shocked to understand what had happened sitting on a tree stump emotionless. Upon hearing no answers from Maari for why she is here and not at her home, he shakes her and she starts wailing out loud, showing that her seemingly happy life led with her husband was all in hope that Thangarasu was also leading a happy life. But since he is not, it seems that she will lament for the rest of her life knowing the sadness and pain of her lover whom she cannot console.  

==Cast== Srikanth as Thangarasu
*Parvathi Menon as Maari
*Inigo Prabhakaran as Maaris husband
*Inbanila as Cheeni
*Periya Karuppu Thevar
*Paravai Muniyamma
*Ram as Thangaraasus father

==Production== Srikanth in Srikanth was retained and the director worked on toning down the actors complexion, while Malayalam actress Parvathi Menon was selected to play the lead role in the film, and also went through a similar character acclimatisatio procedure. 

==Release== in the women in good light.  The film was also screened across international film festivals, notably at the Los Angeles Indian Film Festival, and won Sasi the Best Director award in Ahmedabad Film Festival. 

==Soundtrack==
The soundtrack features 6 tracks, composed by debutant S. S. Kumaran. The soundtrack was released on 25 September 2008 in a launch event with the special guest being film director Ameer Sultan|Ameer.
{{Infobox album|
  Name        = Poo|
  Type        =  Soundtrack |
  Artist      = S. S. Kumaran |
  Released    = 25 September 2009 |
  Recorded    = 2008 |
  Genre       = Soundtrack |
  Label       = Sony BMG|
  Producer    =  S. S. Kumaran |
  This album  = Poo (2008) |
  Next album  = Kalavani (2010) |
}}
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| Track. || Song || Singers || Length (min:sec) ||Lyrics 
|-
| 1 || Choo Choo Maari || Mridula, Srimathi Parthasarathy || 05:25 || Na. Muthukumar 
|-
| 2 || Maman Engirukka || Harini, Tippu (singer)|Tippu, Karthik (singer)|Karthik, Master Rohith || 05:15 || Na. Muthukumar  
|-
| 3 || Aavaram Poo ||   
|-
| 4 || Dheena || Shankar Mahadevan, Hemambiga || 05:24 || Na. Muthukumar 
|-
| 5 || Sivakasi Rathiyae || Periya Karuppa Thevar  || 05:29 || S. Gnanakaravel
|-
| 6 || Paasa Mazhai || S. S. Kumaran || 05:34 || S. Gnanakaravel
|}

==Awards and nominations==
2008 Ahmedabad Film Festival
* Won - Best Film
* Won - Best Director - Sasi (director) | Sasi

2008 Tamil Nadu State Film Awards
* Won - Tamil Nadu State Film Award for Best Film (Special Prize) Best Film Portraying Woman in Good Light
* Won - Tamil Nadu State Film Award for Best Storywriter | Best Storywriter - Tamizhchelvan

2008 Vijay Awards Best Debut - Parvathi
* Nominated - Vijay Award for Best Director | Best Director - Sasi
* Nominated - Vijay Award for Best Editor | Best Editor - Madan Gunadeva
* Nominated - Vijay Award for Best Actress | Best Actress - Parvathi
* Nominated - Vijay Award for Best Make Up Artistes | Best Make Up - Shanmugham & Manohar

2008 56th Filmfare Awards South
* Won - Filmfare Best Actress Award (Tamil) | Best Actress- Parvathi

2008 Vikatan| Ananda Vikatan Awards

* Won - Best Film
* Won - Best Actress - Parvathi
* Won - Best Supporting Actor - Ramu
* Won - Best Storywriter - S. Tamizhchelvan

 Makkal TV Awards | Makkal TV
* Won - Best Film

Jaya TV Awards | Makkal TV
* Won - Best Film
* Won - Best Music Director

 Popular Film Awards
* Won - Best Film
* Won - Best Director - Sasi

===Screened at Festivals===
** Chennai International Film Festival
** Ahmedabad Film Festival
** Norway Film Festival
** Los Angeles Indian Film Festival
** Official Selection at the Indian Panorama

==References==
 

 

 
 
 
 
 