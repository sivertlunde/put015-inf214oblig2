Rajkumar (1964 film)
{{Infobox film
| name           = Rajkumar
| image          = Rajkumar (1964).jpg
| image_size     = 
| caption        = DVD cover
| director       = K. Shankar
| producer       = 
| writer         = 
| narrator       =  Pran Om Prakash Rajindernath
| music          = Shankar Jaikishan
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1964
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
}}
Rajkumar is a 1964 Hindi film directed by K. Shankar. The film stars Shammi Kapoor, Sadhana Shivdasani, Pran (actor)|Pran, Prithviraj Kapoor, Om Prakash and Rajindernath. The music is by Shankar Jaikishan. The film became a box office hit. 

==Plot==
The Maharaja is eager to see his foreign-returned son, Bhanu Pratap who will eventually take over the reins of the region. When he finally gets to see his son, he is shocked to see that the crown prince is in fact a "clown" prince. He openly shows his disgust and disappointment, and decides to continue to rule. Bhanu Pratap and his friend, Kapil, decide to dress incognito and mingle with the general public and find out if there is anyone conspiring to dethrone the king. What they find out will change their lives, and endanger the lives of their loved ones as well.

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Aaja Aai Bahar"
| Lata Mangeshkar
|-
| 2
| "Tumne Pukara Aur Hum Chale Aaye"
| Mohammed Rafi, Suman Kalyanpur
|-
| 3
| "Nach Re Man Badkamma"
| Lata Mangeshkar, Asha Bhosle
|-
| 4
| "Dilruba Dil Pe Tu"
| Mohammed Rafi, Asha Bhosle
|-
| 5
| "Tumne Kisi Ki Jaan Ko"
| Mohammed Rafi
|-
| 6
| "Is Rang Badalti Duniya Mein"
| Mohammed Rafi
|-
| 7
| "Hum Hain Rajkumar"
| Mohammed Rafi
|}

==References==
 

==External links==
*  

 
 
 
 
 
 