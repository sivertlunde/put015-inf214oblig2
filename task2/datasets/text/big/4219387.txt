North Shore (film)
{{Infobox Film |
 name = North Shore|
 image = North shore poster.jpg|
 caption = Theatrical release poster|
 director = William Phelps|
 writer =  Randal Kleiser Tim McCanlies William Phelps|
 starring = Matt Adler Nia Peeples Gerry Lopez John Philbin Gregory Harrison|
 producer = Bill Finnegan Tim McCanlies|
 studio   = Finnegan/Pinchuk Productions|
 distributor = Universal Pictures| 
 released = 1987|
 runtime = 96 min|
 country =  |
 language = English (language)|English|
 budget =  |
 gross = $3,832,228|
  }}
 North Shore of O ahu and see if he has the skills to cut it as a pro surfer. As he progresses on his journey, he learns the qualities he possesses are not going to pull him through alone.

==Synopsis==

Rick Kane, approximately 18 years of age at the start of the film having just graduated high school, uses his winnings from a wave tank surfing contest in his native state of Arizona to fly to Hawaii the summer before the start of college to try and become a professional surfer. He takes a plane to Honolulu with plans to stay with a surfer that he met in Arizona six months previous. He finds the friend tending bar at a seedy gentlemens club.  At the bar he meets up with two pro surfers, Alex and Mark (Mark Occilupo), and stays with them at the house of Lance Burkhart.  In the morning, he goes out surfing with Alex and Mark and realizes that surfing in the ocean is totally different to surfing in a wave tank and he is not as good as he had initially thought.  During this scene he gets in the way of Vince, leader of the local group of the "The Hui" (wave slider club) and (real surfing legend Gerry Lopez), which causes Vince to wipe out and leads to a confrontation where he is chased off the beach after he realizes his stuff was stolen from the beach by a member of "The Hui".  With nowhere to go, he fortuitously runs into Turtle (John Philbin). Kane meets and falls in love with Kiani (Nia Peeples), a beautiful local girl, coincidentally the cousin of Vince, who helps him acclimate to the local culture and customs. Turtle introduces him to Chandler (Gregory Harrison), a shaper and soul surfer, who teaches Rick about soul surfing and Rick masters the art of appreciating and riding the waves.  Rick is a talented graphic artist who helps Chandler design a more contemporary logo for his surfboards.  The films antagonist is Lance Burkhart (Laird Hamilton), a famous, top-ranked surfer whose competitive and materialistic values conflict with the spiritual teachings of Chandler ("You still have a single-fin-mentality").  The film climaxes with a surf contest on Banzai Pipeline as Rick ends up competing against Lance in a duel of skills and beliefs.

==Professional surfers==

North Shore features professional surfers Shaun Tomson, Gerry Lopez ("Vince"), Laird Hamilton ("Lance Burkhart"), Mark Occhilupo, Robbie Page ("Alex Rogers"), Mark Foo, Derek Ho, Hans Hedemann, Ken Bradshaw, Christian "the KID" Fletcher and several others. The character of Rick Kane was loosely based on Connecticut born surfer Benjamin "Barney" Partyka. The first-ever professional surfer, Corky Carroll, plays a competition announcer in the film.

==Connections to real life==

Laird Hamiltons adoptive father Bill Hamilton was a surfboard shaper and glasser on Oahu in the 1960s and 1970s and owned a small business handmaking custom, high-performance surfboards for the Oahu North Shore big wave riders of the era, similar to the character Chandler in the film.  In reality, Laird is a big wave rider and loathes competition like the Chandler character does. 

==Popular culture== Christian swing music|swing/ska band The Ws on their CD titled Fourth From the Last.

Part of the plot is parodied in Surfs Up (film)|Surfs Up.

During a concert in 2010, Tegan Quin of Tegan and Sara admitted that their song "Northshore" was entitled after this movie after Tegan had an experience similar to that of Rick Kane.

The movie is mentioned heavily in the Sarah Vowell book about Hawaiian history, Unfamiliar Fishes.

==External links==
*  

 
 
 
 
 
 
 