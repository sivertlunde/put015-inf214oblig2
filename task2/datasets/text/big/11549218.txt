Satan's Waitin'
 
{{Infobox Hollywood cartoon|
| cartoon_name = Satans Waitin
| series = Looney Tunes (Tweety Bird|Tweety)
| director = Friz Freleng|I. Freleng
| writer = Warren Foster Arthur Davis Manuel Perez   Ken Champin
| layout_artist = Hawley Pratt
| background_artist = Irv Wyner
| voice_actor = Mel Blanc
| musician = Carl Stalling
| producer = Eddie Selzer Warner Bros. Pictures  The Vitaphone Corporation
| release_date = August 7, 1954
| color_process = Technicolor
| runtime = 7 mins 
| movie_language = English
}}
 1953 animated Blue Ribbon Sylvester and Tweety.

==Synopsis==
Sylvester is in pursuit of Tweety, chasing him to the top of a building.  Sylvester falls from the building (first he grabs some of Tweetys feather to help him fly, but Tweety takes them back), crashes to the sidewalk (which cracks) and dies.  The spirit of his first life approaches two escalators and takes the "down" (to Hell|Hades) one (since the one going "up" (to Heaven) is roped off) and ends up in Hell.  He is greeted by a Satanic bulldog (Hector the Bulldog), who realizes he must goad Sylvester into giving up his remaining eight lives, so he asks life #1 to sit on a bench to wait for the others.

Sylvester wakes up and Tweety tells him hes in trouble for breaking the sidewalk (which cracked upon impact of his earlier fall).  Sylvester has had enough of Tweets and tells him to get lost.  The bulldogs spirit reminds him that he has eight lives left, so Sylvester starts the chase up again.  He chases Tweety around a moving steamroller but gets flattened, sending life #2 through the street and into Hades.  The flat #2 gets up and sits beside #1.

The chase then continues through an amusement park.  They both run into a lions mouth entranceway to the fun house, but Sylvester steps back out, takes one look at the lion and is literally "frightened to death".  As scared-white-as-a-sheet life #3 takes his place on the waiting bench, the cat recovers and finds Tweety amongst the moving targets in a shooting gallery.  He climbs into the targets to get at his prey but is shot several times in rapid succession.  With each shot, lives 4 through 7 pop up on the bench.  Sylvester bursts out of the gallery (narrowly missing another shot) and sees Tweety heading towards the roller coaster.  As Tweety sits in the front seat proclaiming "that puddy tat will never find me here", the cat takes the seat directly behind him.  The train ascends the lift hill and proceeds to go through the drops and turns.   Near the end of the ride on straight track, Sylvester stands up.  Just as he is about to pummel an unsuspecting Tweety with a club, he slams into the entraceway of a tunnel.  Upon impact, the train carrying life #8 in the front seat runs through the tunnel and down Hades twisted conveyor route that took Sylvesters first life down earlier.

Recovering, Sylvester realized that he only has one life left.  The bulldog again goads him to go after Tweety, but Sylvester screams "No, no, no! I dont want him! I dont want him!" and runs off.  He decides to move into a bank vault with several cans of food, commenting that hell be safe in there and nothing can happen to him. Later that night, two bank robbers (named Rocky and Mugsy) try to break into the safe, but fail when they use too much nitroglycerine and end up killing themselves and Sylvester.  As the three of them go down the escalator conveyor to Hades, one of the robbers tells the other: "Yer used too much, Mugsy."  The disgruntled #9 adds: "Now he tells him!"

==Edited versions==
*The CBS version of this cartoon ends after the devil dog urges Sylvester to go after Tweety despite that Sylvester has one life left to remove the last scene at the bank where Sylvester locks himself in a vault with food to keep himself from getting harmed any further, only to die in an explosion late at night caused by two bumbling bank robbers who overuse nitroglycerin to crack the vault door open. Also cut on CBS was how Sylvesters eighth life was lost: Sylvester stands up on the roller coaster car as he chases Tweety, only to smack face-first into the tunnel entrance. Sylvester was shown speeding towards the tunnel, but there was an abrupt cut at this point to Sylvesters eighth life riding a rollercoaster car into Hell.

==Availability==
This short is available on the VHS "Sylvester and Tweetys Tale Feathers". It is also available uncut and fully restored on Disc 1 of   DVD set and Disc 2 of   Blu-Ray set, with the latter restored to HD quality.

==External links==
* 

 
 
 
 
 
 