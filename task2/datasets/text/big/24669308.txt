El miedo no anda en burro
{{Infobox film
| name           = El miedo no anda en burro
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Fernando Cortés
| producer       = Fernando de Fuentes
| writer         = Fernando Galiana
| screenplay     =
| story          = 
| based on       =  
| narrator       = 
| starring       = María Elena Velasco Eleazar García Fernando Luján Emma Roldán Óscar Ortiz de Pinedo
| music          = Sergio Guerrero Gustavo Pimentel
| cinematography = Fernando Colín
| editing        = Sergio Soto
| studio         = 
| distributor    = Diana Films
| released       =  
| runtime        = 88 minutes
| country        = Mexico
| language       = Spanish
| budget         = 
| gross          = 
}}
El miedo no anda en burro ( ) is a 1976 Mexican comedy horror film directed by Fernando Cortés and starring María Elena Velasco, Eleazar García, Fernando Luján, Emma Roldán and Óscar Ortiz de Pinedo. 
 India María franchise, having remained in the Cine Metropolitan for an astounding nineteen weeks.   

==Plot==
María Nicolasa Cruz is doña Claritas loyal indigenous maid. However, meanwhile doña Clarita is dying, she is leaving behind a large monetary estate, a mansion, properties, and Mimí: her affectionate Shih Tzu dog. María is with doña Clarita during her last moments, albeit her sister Paz, brother Marciano, nephew Braulio, and grandsons Raul and Laura are waiting anxiously downstairs for her death, believing they will inherit all her riches. Doña Clarita finally dies at the hand of a corrupt doctor employed by doña Claritas relatives, and María inconsolably goes downstairs to the living room to deliver the news. As expected, María finds none of doña Claritas relatives mourning her death. Believing she is of no use anymore, doña Claritas relatives (or "The Vultures" as María calls them) fire María, therefore she decides to return to her native hometown. Marciano, Paz, Braulio, Raul, and Laura meet with the notary to hear Doña Claritas will. To everyones surprise, the will only mentions Mimi (the dog), and Maria as her guardian. The family members go back to stop Maria from leaving the mansion. 

The "zopilotes" (vultures \ Marias nickname for Doña Claritas family) convince Maria to stay in the mansion to take care of Mimi as her new guardian. The "zopilotes" try several times to kill Maria along with Mimi but all attempts failed. Until Marciano feeds Mimi a piece of meat with an explosive inside. Before Mimi eats it, Maria takes it from her and cooks it for the afternoon meal. Braulio, unfortunately, receives the piece of meat and after cutting it, the explosive goes off. Mimi ears were gravely affected. The doctor advises Maria to go on a vacation with Mimi - to Doña Claritas old vacation house in Guanajuato. Upon arrival, Maria and Mimi are scared by the butler Franki who guides them through the haunted house. Both Maria and Mimi survive two scary hands, a cyclop, a giant talking frog, a fern monster, a werewolf, and many more horrific things.
To Marias surprise, she discovers that those scary characters are the "zopilotes" They catch them and put them in a squishing torture machine. Maria and Mimi, on the verge of death, are saved by Franki who was a detective investigating Doña Claritas murder - committed by her family members. Maria with all these surprises asks Franki, whose real name is Maldonado, permission to faint on the floor. As she does, Marciano, Paz, Braulio, Raul, and Laura end up arrested for homicide.

==Cast==
*María Elena Velasco as La India María|María Nicolasa Cruz
*Eleazar García as Braulio
*Fernando Luján as Raúl
*Emma Roldán as doña Paz
*Óscar Ortiz de Pinedo as don Marciano
*Gloria Mayo as Laura
*Alfredo Wally Barrón as Frankie / Maldonado (credited as Wally Barron)
*José Cibrián Jr. as American tourist
*Antonio Bravo as Doctor
*Carlos Bravo y Fernández as Notary
*Alfonso Zayas as Policeman Mimi as Herself (Eleazar García "Chelelo" was her owner)

==References==
 

==External links==
* 

 

 
 
 
 