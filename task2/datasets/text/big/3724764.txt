Mexicanos, al grito de guerra (film)
: This article is about the film.  For the National Anthem of Mexico, see Mexicanos, al grito de guerra.
 

{{Infobox film| name = Mexicanos, al grito de guerra
  | image = 
  | caption = 
  | director = Álvaro Gálvez y Fuentes Ismael Rodríguez
  | producer = 
  | writer = Elvira de la Mora Álvaro Gálvez y Fuentes Joselito Rodríguez
  | starring = Pedro Infante Lina Montes Miguel Inclán
  | music = Raúl Lavista
  | cinematography = Ezequiel Carrasco
  | editing = 
  | studio = Producciones Rodríguez Hermanos
  | distributor = 
  | released =  
  | runtime = 101 minutes
  | language = 
  | budget =
  }}

Mexicanos, al grito de guerra (Mexicans, to the Cry of War) is a 1943 historical drama movie produced in Mexico starring Pedro Infante. The main story revolves around a soldier, a woman, love and an impending war.

== Plot ==

At the beginning of the film, there is a young man who decides to become a soldier to defend Mexico from an incoming invasion by the French army. After enlisting, he fell in love with a young lady, but it turns out that she is the niece of the French ambassador to Mexico. In one battle where the Mexican forces were near the brink of defeat, the soldier, Luis, decides to grab a trumpet and play the song "Mexicanos, al grito de guerra", the National Anthem of Mexico. Upon hearing the anthem played, the soldiers rally and overcome the French forces. However, Luis is shot and dies in the end at the same time his love escapes imprisonment.

==External links==
 


 
 
 
 
 
 

 
 