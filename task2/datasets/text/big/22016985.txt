Night Life (film)
{{Infobox Film 
  | name = Night Life|
  | image = 
  | caption = 
  | director = David Acomba
  | producer = Charles Lippincott
  | writer = Keith Critchlow
  | starring = {{plainlist|
* Scott Grimes
* John Astin
* Cheryl Pollak
* Anthony Geary
* Alan Blumenfield
* Kenneth Ian Davis
* Darcy DeMoss
* Lisa Fuller
* Mark Pellegrino
* Phil Proctor
}}
  | cinematography = Roger Tonry
  | music = Roger Bourland
  | editing = Michael John Bateman
  | studio = Creative Movie Marketing
  | released =  
  | runtime = 89 minutes
  | language = English
  | budget =
  }} zombie corpses of his former high school bullies (recently deceased) after they are brought back to life by a freak lightning storm.  It is also known under the name Grave Misdemeanors.

== Plot ==
Archie Melville takes a job with his Uncle Verlin at the mortuary.  When the local bullies die in a car crash, they are taken to the mortuary, where they become reanimated.  The bullies, now zombies, become even nastier and begin to torment Archie and his friend Charly.

== Cast ==
* Scott Grimes as Archie Melville
* John Astin as Uncle Verlin
* Cheryl Pollak as Charly
* Anthony Geary as John Devlin
* Alan Blumenfield as Frank
* Kenneth Ian Davis as Rog Davis
* Darcy DeMoss as Roberta Woods
* Lisa Fuller as Joanie Snowland
* Mark Pellegrino as Allen Patumbo
* Phil Proctor as Randolph Whitlock

== Reception ==
Writing in The Zombie Movie Encyclopedia, academic  , called it a "rather minor teen effort". 

== References ==
 

==External links==
*  

 
 
 
 
 
 


 