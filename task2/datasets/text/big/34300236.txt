The Best and the Brightest (film)
{{Infobox film
| name           = The Best and the Brightest
| image          = The Best and the Brightest.gif
| alt            =  
| caption        = Film poster
| director       = Josh Shelov
| producer       = Nicholas Simon Patricia Weiser Robert Weiser
| screenplay     = Josh Shelov Michael Jaeger
| starring       = Neil Patrick Harris Bonnie Somerville Christopher McDonald Amy Sedaris
| music          = Ted Masur
| cinematography = John Inwood
| editing        = Peter Iannuccilli
| studio         = High Treason Pictures
| distributor    =
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
The Best and the Brightest is a 2010 American independent film. Directed by Josh Shelov, the film stars Neil Patrick Harris and Bonnie Somerville as Jeff and Sam respectively, a young couple that moves into New York City. The film centers around the effort they have to put in to get their five-year-old daughter Beatrice (Amelia Talbot) into an elite private kindergarten.

This is director Josh Shelovs debut feature-film, co-written with Michael Jaeger. The film received mostly mixed to negative reviews.

==Plot==

The film opens with Jeff (Neil Patrick Harris) and Sam (Bonnie Somerville) moving from Delaware into New York City with their five-year-old daughter Beatrice. Sam takes Beatrice on visit to several schools in the city to get Beatrice admitted for the current session, only to realize that there is very little prospect in such short period. After losing hope, she enlists help of Sue Lemon (Amy Sedaris), a consultant who helps couples in getting their kids admitted into schools. Though first Sue declines to help them for citing short notice, later she agrees after seeing Sams stubborn determination.
 sex chat he had with a prostitute with Jeff. Later at school, the couple is interviewed by Katharine Heilmann (Jenna Stern) who mistakes Clarks sex chat print copy for a poem written by Jeff and is visibly impressed. However, a freak incident involving Clark makes her decline Jeff and Sams application.

Upon failing to impress Katharine, Sam and Sue decide and plan to befriend The Player (Christopher McDonald), a rich guy who is also chairman of school committee. Thus begins a series of comic plays and lies that the couple has to go through to get approval of The Player, The Players wife (Kate Mulgrew), and Katharine. By the end of the movie, Sam is forced to choose between living the life she has dreamed of – but lying in order to do so – or going back to Delaware as herself. 

==Cast==
* Neil Patrick Harris as Jeff
* Bonnie Somerville as Sam
*  Amelia Talbot as Beatrice
* Amy Sedaris as Sue Lemon
* Jenna Stern as Katharine Heilmann
* Peter Serafinowicz as Clark
* Christopher McDonald as The Player
* Kate Mulgrew as The Players Wife
* Bridget Regan as Robin, Jeffs college crush

==Production==
Films casting was done by Jessica Kelly and Suzzane Smith (credited as Suzanne Smith Crowley).

Several scenes were filmed in the historic Bryn Mawr College in Bryn Mawr, Pennsylvania

==Reception==
The film received mostly mixed to negative reviews.

==References==
 

==External links==
*  

 

 
 
 
 
 
 