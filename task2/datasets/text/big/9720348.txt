Women Who Work
{{Infobox film
| name           =  Mujeres que trabajan
| image          =Mujeres que trabajan.jpg
| image size     =
| caption        =
| director       =Manuel Romero
| producer       =
| writer         =
| narrator       =
| starring       =Mecha Ortiz, Tito Lusiardo, Niní Marshall, Pepita Serrador
| music          =
| cinematography =
| editing        =
| distributor    =
| studio = Lumiton 1938
| runtime        =77 minutes
| country        = Argentina
| language       = Spanish
| budget         =
| preceded by    =
| followed by    =
}} Argentine comedy film drama directed by Manuel Romero. The film premiered in Buenos Aires and starred Tito Lusiardo.

==Cast==
*Mecha Ortiz... 	Ana María del Solar
*Tito Lusiardo	... 	Lorenzo Ramos
*Niní Marshall	... 	Catita
*Pepita Serrador	... 	Luisa
*Alicia Barrié	... 	Clara
*Fernando Borel	... 	Carlos Suárez Roldán
*Sabina Olmos	... 	Elvira
*Alita Román... 	Anita
*Mary Parets	... 	Marta Suárez Roldán
*Enrique Roldán	... 	Andrés Stanley
*Hilda Sour	... 	Dolores Campos
*Alímedes Nelson	... 	Emilia
*María Vitaliani	... 	Doña Petrona
*Berta Aliana	... 	Dora
*Alicia Aymont	... 	Mrs. Suárez Roldán

==External links==
* 

 
 
 
 
 
 


 
 