One of the Best (film)
{{Infobox film
| name           = One of the Best 
| image          =
| caption        =
| director       = T. Hayes Hunter
| producer       = Michael Balcon   Carlyle Blackwell
| writer         = George Edwardes-Hall (play)   Seymour Hicks (play)   Patrick L. Mannock Walter Byron   Randle Ayrton
| music          = James Wilson
| editing        = 
| studio         = Gainsborough Pictures
| distributor    = Woolf & Freedman Film Service
| released       = November 1927 
| runtime        = 8,000  
| country        = United Kingdom 
| awards         =
| language       = English
| budget         = 
| preceded_by    =
| followed_by    =
}} British silent silent historical historical drama Walter Byron and Eve Gray.  It was based on a play by Seymour Hicks. Film historian Rachael Low described it as an "unsophisticated costume drama". 

==Cast==
* Carlyle Blackwell - Philip Ellsworth  Walter Byron - Lieutenant Dudley Keppel
* Eve Gray - Mary Penrose 
* Randle Ayrton - General Gregg 
* James Carew - Colonel Gentry 
* Julie Suedo - Claire Melville  James Lindsay - Maurice de Gruchy  Pauline Johnson - Esther 
* Elsa Lanchester - Kitty 
* Charles Emerald - Private Jupp 
* Cecil Barry - Lieutenant Wynne 
* Simeon Stuart - Squire Penrose 
* Harold Huth - Adjutant

==References==
 

==Bibliography==
* Low, Rachael. The History of British Film, Volume 4 1918-1929. Routledge, 1997.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 


 
 