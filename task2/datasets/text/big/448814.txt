The Little Mermaid II: Return to the Sea
 
{{Infobox film
|  name = The Little Mermaid II: Return to the Sea
|  image = The_Little_Mermaid_2_Poster.jpg
|  caption = DVD release cover
|  writer = Elizabeth Anderson Temple Mathews Elise DHaene Eddie Guzelian Tara Strong Pat Carroll Buddy Hackett Max Casella Stephen Furst Clancy Brown
|  music = Danny Troob Original songs: Michael Silversher  Patty Silversher
|  director = Jim Kammerud Brian Smith David Lovegren Disney Television Animation
|  distributor = Buena Vista Home Entertainment
|  released =  
|  runtime = 75 minutes English
}}
The Little Mermaid II: Return to the Sea is a 2000 Disney  , a 2008 direct-to-video animated feature.

==Plot==
  Ariel and Eric celebrate Melody on King Triton presents Melody with a magic locket. The party is interrupted by List of Disneys The Little Mermaid characters#Morgana|Morgana, sister of the deceased Ursula (The Little Mermaid)|Ursula, who threatens to cause Melody harm, using her as leverage to get Tritons trident. Ariel and Eric work together to foil Morganas plan.

Fearing Morgana and remembering Ursula, Ariel decides to withhold all knowledge of the sea world from Melody. The locket is tossed into the ocean, and a massive wall is built to separate the royal castle from the sea.

Twelve years later, Melody remains unaware of her mother’s mermaid heritage and is forbidden from ever going into the sea. However, she has been sneaking out the castle regularly to swim and one day finds her locket. Seeing her name on the locket, Melody confronts her mother and, frustrated with her mothers refusal to answer her questions, Melody takes a boat and sails away from home. Her parents soon learn that she is gone, and Triton uses his magic to transform Ariel back into a mermaid to search for Melody.
 Cloak and Tip the Dash the walrus, Melody successfully returns to Morgana with the trident.  Ariel tries to stop Melody, but before she can explain that Morgana is evil, Melody gives the trident to Morgana. With the trident in her grip, Morgana reveals her true intentions and grabs Ariel with her tentacle and holds her hostage.

Morgana uses the tridents magic to lord over the ocean, rising to the surface to gloat. List of Disneys The Little Mermaid characters#Scuttle|Scuttle, Triton, List of Disneys The Little Mermaid characters#Sebastian|Sebastian, and Eric arrive, and a battle ensues against Morgana and her minions.  Melody manages to grab the trident and throw it back to Triton by climbing up the cliff on which Morgana is standing and sneaking up on her.  Triton uses his trident to encase Morgana in a block of ice, which sinks underwater.

Melody reunites with her family, and Triton offers his granddaughter the option of becoming a mermaid permanently. Instead, she uses the trident to disintegrate the wall separating her home from the sea, reuniting the humans and the merpeople.

==Voice cast==
  Ariel
*Tara Melody
*Samuel Sebastian
*Pat Pat Carroll Morgana
*Rob Prince Eric King Triton Flounder
*Buddy Scuttle
*Clancy Undertow
*Max Tip
*Stephen Dash
*Rene Chef Louis
*Edie McClurg as Carlotta Grimsby
*Tress MacNeille as Mother Penguin and Baby Penguin
*Frank Welker as Max the Sheepdog

==Release==
The film was released direct-to-DVD on September 19, 2000.  On November 6, 2006, the film was released in a bundle together with the original film in the Region 2 release.  The original DVD release was later discontinued and a  , were released in 2-Movie collection on DVD and Blu-ray on November 19, 2013.

==Reception==
 
The film received mixed to negative reviews from film critics. The film holds a 33% "rotten" rating on Rotten Tomatoes.

==Soundtrack==
{{Infobox album
| Name        = Songs from The Little Mermaid II: Return to the Sea & More!
| Type        = soundtrack
| Artist      = Various artists
| Cover       = The Little Mermaid II Return to the Sea (soundtrack).jpg
| Released    =  
| Recorded    = 2000
| Genre       =
| Length      = 25:21
| Label       = Walt Disney Records
| Producer    = Bambi Moe, Don Mizell, Eric Silver, Leonard Jones, Shepard Stern
| Misc        =
}}
 original films soundtrack.    Two limited edition two tracked CD samples were released as a promotion for the soundtrack in 2000.   

{{tracklist
| writing_credits   = yes
| extra_column      = Recording artist(s)
| total_length      = 25:21
| title1            = Down to the Sea
| writer1           = Michael Silversher, Patty Silversher
| extra1            = Jodi Benson, Rob Paulsen, Clancy Brown, Kay E. Kuter, Samuel E. Wright and Chorus
| length1           = 3:29
| title2            = Tip and Dash
| writer2           = Silversher, Silversher Tara Charendoff
| length2           = 1:59
| title3            = Iko Iko
| note3             =  * 
| writer3           = Barbara Ann Hawkins, Jessie Thomas, Joan Johnson, Maralyn Jones, Joe Jones, Rose Lee Hawkins, Sharon Jones
| extra3            = Wright
| length3           = 3:49
| title4            = Octopuss Garden
| note4             =  *  Richard Starkey
| extra4            = Wright
| length4           = 2:47
| title5            = For a Moment
| writer5           = Silversher, Silversher
| extra5            = Benson, Charendoff
| length5           = 2:28
| title6            = Give a Little Love
| note6             =  * 
| writer6           = Albert Hammond, Diane Warren
| extra6            = Wright
| length6           = 3:57
| title7            = Hot Hot Hot (Arrow song)|Hot, Hot, Hot Alphonsus Cassell
| extra7            = Wright
| length7           = 5:08
| title8            = Here on the Land and Sea
| note8             = Finale
| writer8           = Silversher, Silversher
| extra8            = Benson, Charendoff, Wright
| length8           = 1:44
}}
{{tracklist
| collapsed         = yes
| headline          = Limited edition two track CD 1 
| writing_credits   = yes
| extra_column      = Recording artist(s)
| title1            = Part of Your World
| writer1           = Alan Menken, Howard Ashman
| extra1            = Cheyl Wright
| length1           = 3:25
| title2            = Limbo Rock
| note2             =  * 
| writer2           =
| extra2            = Samuel E. Wright
| length2           = 2:29
}}
{{tracklist
| collapsed         = yes
| headline          = Limited edition two track CD 2 
| writing_credits   = yes
| extra_column      = Recording artist(s)
| title1            = Here on the Land and Sea
| note1             = Finale
| writer1           = Michael Silversher, Patty Silversher
| extra1            = Jodi Benson, Tara Charendoff, Samuel E. Wright
| length1           = 1:44 Coconut
| note2             =  * 
| writer2           = Harry Nilsson
| extra2            = Wright
| length2           =
}}
*Note:  * These tracks were originally recorded for the album  .

The films ending credits play a new version of Part of Your World from the original movie.  It is performed by country singer Chely Wright. Another version of that song was performed by Ann Marie Boskovich. That version of the song was played in international dubbings of the film. 

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 