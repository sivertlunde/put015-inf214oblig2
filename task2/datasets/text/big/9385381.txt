Sorority House Massacre II
{{multiple issues|
 
 
}}

{{Infobox film
| name = Sorority House Massacre 2
| image = Shm2dvd.jpg
| caption =
| director = Jim Wynorski
| producer = Julie Corman Roger Corman
| writer = Mark Thomas McGee James B. Rogers Bob Sheridan
| starring = Gail Harris Melissa Moore Dana Bentley Mike Elliot Stacia Zhivago Barbii Bridget Carney
| music = Chuck Cirino
| cinematography =
| editing = Nina Gilberti
| distributor = Concorde Pictures New Horizon Home Video
| released =  
| runtime = 80 minutes
| country = United States
| language = English
| budget =
}}
 Melissa Ann Moore and Gail Harris (credited as Robyn Harris).

==Plot==
Five women, Linda (Gail Harris), Jessica (Melissa Moore), Kimberly (Stacia Zhivago), Suzanne (Barbii) and Janey (Dana Bentley) buy the old Hokstedter place for their new sorority house. They get it cheap because of the bloody incidents from five years before committed by Hokstedter. They decide to stay in it for the night so they can meet the movers in the morning, despite the electricity and the phones not working. Janey tells the group of the murders years before, putting the group on edge. As it turns to night, a storm rolls in and the girls are crept out by they neighbor Orville Ketchum (Peter Spellos) who recalls the night of the murders, and how Hokstedter was defeated. He gives them the keys to the basement before returning home. The girls decide to explore the basement, and find Hokstedters tools and also a ouija board. Meanwhile, Lt. Mike Block (Jürgen Baum) and Sgt. Phyliss Shawlee (Toni Naples) set out in the storm to get to the Hokstedter house after they receive a disturbance call from the house, and also suspect Orville had something to do with the murders, although Mike was unable to pin anything on him at the time.
 bear trap before the killer stabs her to death. Meanwhile, Mike and Phyliss travel to a strip club to talk to Candy (Bridget Carney) a survivor of the Hokstedter massacre. However, Candy can not recall if Orville was part of the murders.

Linda, Jessica and Kimberly begin to think Janey and Suzanne are playing a trick on them, and so go down to the basement to find them. Just as they are about to give up, they find their bodies strung up on the ceiling. The girls run upstairs and arm themselves with knives before attempting to leave. However they run into Orville and so retreat back into the house and lock the doors and windows. As the survivors become more panicked, they realize they left the attic window open. They run upstairs and lock the window, however Kimberly realizes that he has already gotten into the house. She panics and runs downstairs. While Linda remains in the attic, Jessica goes after Kimberly. Kimberly bumps into Orville and hides in a bathroom, but the killer gets in and murders her.

While Linda hides in the attic, Orville enters. Linda manages to stab him numerous times before finally choking him. She goes downstairs in search of Kimberly and Jessica, but instead finds Kimberly dying in a bathtub. Linda is attacked by a still alive Orville, but Linda overpowers him and drowns him in the toilet. She goes downstairs to find Jessica, but answers the phone when it rings. A woman asks for her husband, Hokstedter, before warning her he is in the house, before hanging up. Linda is lured into the basement by Jessica, who reveals herself to have been possessed by Hokstedter. Jessica chases Linda upstairs where the two fight, before Orville reveals himself to still be alive. Orville stabs Jessica, however Jessica knocks him out, before Linda manages to defeat Jessica, stabbing her in the neck.

The next morning, Mike arrives with police officers after the movers found the bodies. They find Linda still alive, but now possessed by Hokstedter. Orville wakes up and shoots Linda dead before the police officers shoot Orville. He however, survives and is rushed to hospital and later released after police could not pin the murders on him.

==Cast==
*Gail Harris as Linda
*Melissa Moore as Jessica
*Stacia Zhivago as Kimberly
*Barbii as Suzanne
*Dana Bentley as Janey
*Jürgen Baum as Lt. Mike Block
*Toni Naples as Sgt. Phyliss Shawlee Mike Elliott as Eddie
*Bridget Carney as Candy
*Peter Spellos as Orville Ketchum

==Production==
The film was written by Jim Wynorski and J B Rogers over a weekend. Chris Nashawaty, Crab Monsters, Teenage Cavemen and Candy Stripe Nurses - Roger Corman: King of the B Movie, Abrams, 2013 p 202-203 

==Sequel==
Sorority House Massacre III Began Production in 2001 and wrapped principal photography in February 2002. in August 2002 it was announced that the film had changed the name to FINAL EXAM. in April 2007 it was Announced that the film would be Released later in the year on DVD. under a other new title The Legacy. But Roger Cormans Concorde Pictures, closed up shop and the Film was ever Released. in March 2015 Director Jim Wynorski Released a Poster for the film on his official Facebook Page with the title Sorority House Massacre III: The Final Exam. its unknown at this stage if the film will ever be released.

==References==
 

==External links==
* 
*  
* 
*  

 

 
 
 
 
 
 
 
 
 
 
 