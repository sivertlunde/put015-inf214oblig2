Shrunken Heads (film)
{{Infobox film
| name           = Shrunken Heads
| image          = Shrunken heads.jpg
| caption        = VHS Artwork
| director       = Richard Elfman
| producer       = Charles Band
| writer         = Matthew Bright
| starring       = {{plainlist|
* Julius Harris
* Meg Foster
}}
| music          = {{plainlist|
* Richard Band
* Danny Elfman
}}
| cinematography = Stephen McNutt
| editing        = Charles Simmons
| distributor    = {{plainlist|
* Multicom Entertainment Group
* Full Moon Entertainment
}}
| released       =  
| runtime        = 86 minutes
| country        = United States
| language       = English
| budget         = $1 million 
}}
Shrunken Heads is a 1994 American film directed by Richard Elfman and written by Matthew Bright. It is Full Moon Entertainments first theatrical feature, and the last film in which Julius Harris appeared.

Director Richard Elfmans brother, Danny Elfman, provided the main theme music for the film, while the rest of the score was created by Richard Band. Richards son, Bodhi Elfman, appeared in the film as Booger Martin.

==Synopsis==
After a street gang kills several youths, a friendly voodoo priest reanimates them so that they can seek vengeance.

==Cast==
*Julius Harris as Aristide Sumatra, a voodoo priest
*Meg Foster as Big Moe, a tough lesbian mob boss
*Aeryk Egan as Tommy Larson
*Rebecca Herbst as Sally
*Bo Sharon as Bill Turner
*Bodhi Elfman as Booger Martin
*A.J. Damato as Vinnie Benedetti
*Darris Love as Freddie Thompson
*Troy Fromin as Podowski
*Leigh-Allyn Baker as Mitzi
*Paul Linke as Mr. Larson
*Billye Ree Wallace as Mrs. Wilson
*R.J. Frost as The Vipers
*Randy Vahan as the plumber
*Don Dolan as the arresting cop

==Reception==
Emanuel Levy of Variety (magazine)|Variety called it "only mildly entertaining".   J. R. Taylor of Entertainment Weekly rated it B and wrote, "Elfmans fun-loving touches manage to get this new series up and running with twisted enthusiasm".   Writing in The Zombie Movie Encyclopedia, academic Peter Dendle said that the characters "lovingly exagerate every preposterous line with relish, yielding some outrageous dialogue in an otherwise treadmill production." 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 


 
 