Boys Will Be Joys
 
{{Infobox film
| name           = Boys Will Be Joys
| image          =
| caption        =
| director       = Robert F. McGowan
| producer       = Hal Roach F. Richard Jones
| writer         = Hal Roach H. M. Walker
| starring       =
| cinematography = Art Lloyd
| editing        = Richard C. Currier
| distributor    = Pathé|Pathé Exchange
| released       =  
| runtime        = 20 minutes
| country        = United States
| language       = Silent with English intertitles
| budget         =
}}
 short silent silent comedy film directed by Robert F. McGowan.       It was the 41st Our Gang short subject released.

==Cast==

===The Gang===
* Joe Cobb as Joe
* Jackie Condon as Jackie
* Mickey Daniels as Mickey
* Johnny Downs as Johnny
* Allen Hoskins as Farina
* Mary Kornman as Mary
* Jannie Hoskins as Mango
* Pal the Dog as Himself

===Additional cast===
* Gabe Saienz as Kid
* Jay R. Smith as Bit part
* Charles A. Bachman as Surveyor
* Allan Cavan as Member, Board of Directors
* George B. French as Member of Board of Directors William Gillespie as Member, Board of Directors
* William Orlamond as Member, Board of Directors
* Paul Weigel as Henry Mills, board chairman
* Charley Young as Member, Board of Directors
* Noah Young as Officer
* Andy Samuel as Unconfirmed

==See also==
* Our Gang filmography

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 


 