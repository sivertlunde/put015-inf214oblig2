Evil Aliens
{{Infobox film |
 name = Evil Aliens |
 image = Evil-aliens-poster.jpg|
 caption = Poster for Evil Aliens|
 producer = Falcon Film Productions |
 director = Jake West |
 writer = Jake West | Emily Booth Christopher Adamson  Norman Lovett | Richard Wells|
 cinematography = |
 editing = Jake West |
 distributor = ContentFilm |
 released =   |
 runtime = 89 minutes  |
 budget =  |
  country = United Kingdom |
  language = English |
}}
Evil Aliens is a Welsh slapstick horror-comedy film directed by Jake West, in the tradition of films such as Braindead (1992 film)|Braindead, House (1986 film)|House, and Evil Dead.

It was the first full-length Sheep-shagging horror film to be filmed using Sony HD (High Definition) cameras, and contains almost 140 digital effects shots and a huge amount of gory conventional special effects.

==Plot==
The film begins with the alien abduction on Scalleum, a remote island off the coast of Wales, of Cat Williams and her boyfriend. Cats boyfriend is gorily killed through brutal anal probing, and Cat is (also gorily) implanted with an alien fetus. Cats story attracts the attention of Michelle "Foxy" Fox (Emily Booth), the bosomy host of the cable TV show Weird Worlde,  who brings a film crew to the island &mdash; her cameraman boyfriend Ricky (Sam Butler); Jack the sound man; nerdy UFO expert Gavin Gorman; and actors Bruce Barton and Candy Vixen (the latter, Foxys producer assures her, "because shes good, not because shes my girlfriend").

The island is accessible via a narrow causeway only at low tide. The Weird Worlde crew sets out in their van, but it is dark by the time they reach the Williams familys creepy farmhouse, where they meet Cat and her three hulking and sadistic brothers (who speak only Welsh with English subtitles). The crew (with the exception of Gavin Gorman) initially assume that Cats story is a hoax, and even go so far as to make a crop circle in a nearby field so they can film it for the show, to Gormans great disgust. However, it soon turns out that the aliens are all too real and rather malevolent.
 Combine Harvester (Brand New Key)" by The Wurzels.
 Independence Day) to overload the ley lines of the nearby stone circle. As Cats alien child rips his arms off, Gavin manages to press the space bar with his nose, sending the stones shooting into the underside of the alien craft, which crashes into a convenient mountain. Jack the sound man, meanwhile, having been blinded by alien ichor early in the film, swims across the channel to the mainland, only to discover that hes lost the videotape that was the only proof of their extraterrestrial encounter.
 Jerry Springer (and subtitled in English), on which Gavins female alien is trying to explain how her entire crew was killed by humans and she herself carries the love child of one of those humans. The audience roars with laughter, and the host cuts her mike.

==Cast== Emily Booth, Christopher Adamson, model Jodie Shaw, and Jennifer Evans.

 
*Emily Booth &ndash; Michelle Fox
*Jamie Honeybourne &ndash; Gavin Gorman
*Sam Butler &ndash; Ricky Anderson
*Jodie Shaw &ndash; Candy Vixen
*Peter McNeil OConnor &ndash; Jack Campbell
*Nick Smithers &ndash; Bruce Barton
*Norman Lovett &ndash; Howard Marsden Christopher Adamson &ndash; Llyr Williams/Surgeon Alien
*Jennifer Evans &ndash; Cat Williams
*Mark Richard Hayes &ndash; Dai Williams
*Chris Thomas &ndash; Thomas Williams
*Scott Joseph &ndash; Lead Alien
*Mildred von Heildegard &ndash; Female Alien
*Tim Clark, Mark Holloway, Glenn Collier &ndash; Aliens
*Tree Carr &ndash; Dream Alien
*Dan Palmer &ndash; Onkey (UFO Witness 1)
*James Heathcote &ndash; Merv (UFO Witness 2)

==Production==
Evil Aliens is the first in a slate of films planned  by the British production company Falcon Film Productions PLC, and the second feature film from director Jake West (Razor Blade Smile). The film was produced by Tim Dennison (Lighthouse, 
Revenge of Billy the Kid, Silent Cry); the executive producer was Quentin Reynolds.

== Filming ==
Although the film is set in Wales, most of it was actually filmed on a farm in Cambridgeshire, including the combine harvester sequence and the UFO crash. The harvester was provided by a local agricultural contracting firm, and driven by one of their staff (whose hand is visible controlling the harvester during much of the sequence).

==Soundtrack==
*"Combine Harvester (Brand New Key)" by The Wurzels

==Release== UK on March 10, 2006, by ContentFilm International, who also act as international sales agents. Image Entertainment released the film in U.S. cinemas on September 6, 2006. It was released on DVD (with extras) in the UK on 26 September 2006. American DVD release was scheduled for October 2007. 

==Further reading==
* 

==External links==
*  
*  
*  

 
 
 
 
 
 