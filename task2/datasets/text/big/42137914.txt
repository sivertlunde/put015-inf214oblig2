Tsubasa no gaika
{{Infobox film
| name           = Tsubasa no gaika A Triumph of Wings
| image          =
| image_size     =
| caption        =
| director       = Satsuo Yamamoto
| producer       = Sanezumi Fujimoto
| writer         = Akira Kurosawa and Bonhei Sotoyama
| starring       =
| music          =
| cinematography = Taiichi Kankura
| editing        =
| distributor    = Toho Film Distributing Co. Ltd.
| released       = October 14, 1942 {{cite web |url=http://www.mcomet.com/movie/Tsubasa_no_gaika-677382/ |title=Tsubasa no gaika(1942)
 |work=mcomet.com |publisher= |accessdate=April 20, 2014}} 
| runtime        = 109 minutes
| country        = Japan
| awards         =
| language       = Japanese
| budget         =
}} Japanese film directed by Satsuo Yamamoto made for Toho Film (Eiga) Co. Ltd. It was co-written by Akira Kurosawa. The films full title is The Triumphant Song of the Wings (lit: Winged Victory).

==Cast==

* Joji Oka 
* Takako Irie 
* Ranko Hanai 
* Heihachiro Okawa 
* Seizaburô Kawazu 
* Takuzô Kumagai 
* Akio Kusama 

== References ==
 

== External links ==
*  

 

 
 
 
 
 


 