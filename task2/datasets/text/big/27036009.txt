Preminchi Choodu
{{Infobox film
| name           = Preminchi Choodu
| image          = Preminchi Choodu.jpg
| caption        = Film poster
| director       = P. Pullaiah
| producer       = P. Pullaiah P. Santha Kumari V.Venkateswarlu
| writer         = C. V. Sridhar (story) Mullapudi Venkata Ramana (dialogues)
| narrator       = Kanchana Kongara Girija Gummadi Venkateswara Rao Rajasree Chittor V. Nagaiah P. Santha Kumari
| music          = Master Venu
| cinematography = P. L. Roy
| editing        = N. M. Shankar
| studio         = Padmasri Pictures
| distributor    =
| released       = 1965
| runtime        =
| country        = India
| language       = Telugu
| budget         =
}}
 Kanchana and Tamil comedy Kaadhalikka Neramillai, directed by C. V. Sridhar.

==Plot==
Ranga (Akkineni) is son of Teacher (Raavi Kondala Rao). His friend is Vasu (Jaggaiah). Buchabbai is proprietor of Buchabbai Estates. Buchabbai has a son Raju (Chalam) and two daughters (Kanchana and Rajasree). Vasu and Elder daughter are in love each other. Ranga works in the office of Buchabbai. Ranga hits the car driven by the daughters of Buchabbai. They influence their father to remove Ranga from the service. He protests peacefully in front of their house with friends. During this period he gets close to their daughters and son. He finally convinces Buchabbai with the help of Vasu (in disguise of his rich father) and becomes his son-in-law.

==Credits==

===Cast===
* Akkineni Nageshwara Rao	as   Ranga
* Kongara Jaggaiah	 as   Vasu
* Relangi Venkataramaiah	... 	Buchabbai Kanchana   as  Elder daughter of Buchabbai
* Rajasree	as Younger daughter of Buchabbai
* P. Santha Kumari
* Chalam   as  Raju, son of Buchabbai
* Gummadi Venkateswara Rao
* Chittor V. Nagaiah Girija
* Allu Ramalingaiah	  as   Manager
* Raavi Kondala Rao
* Boddapaati
* K. S. Reddy

===Crew===
* Director: P. Pullaiah
* Associate Director: K. Raghavendra Rao
* Assistant Director: C. S. Bose
* Story: C. V. Sridhar
* Dialogues: Mullapudi Venkata Ramana
* Producers: V.Venkateswarlu P. Santha Kumari and P. Pullaiah
* Cinematography: P. L. Roy
* Operative Cameraman: K. S. Ramakrishna Rao
* Art Director: V. Suranna
* Film Editor: N. M. Shankar
* Stills: Satyam
* Lyrics: Aathreya, Dasaradhi, Arudra, Srirangam Srinivasa Rao, C. Narayana Reddy, Mullapudi Venkata Ramana
* Original Music: Master Venu
* Playback singers: L. R. Eswari, P. B. Srinivas, Ghantasala Venkateswara Rao, P. Susheela, M. S. Raju and Basaveshwari

==Soundtrack==
The music and background score for the movie was composed by Master Venu.
* Adi Oka Idile Ataniki Thagule (Lyrics:   and P. Susheela; Cast: ANR and Rajasree)
* Andale Thongi Choose Hayihayi (Singers: P. Susheela, L. R. Eswari and Basaveshwari)
* Dorikeru Doragaru Ika Nannu Vidaleru (Singers: P. Susheela and Ghantasala Venkateswara Rao; Cast: Jaggaiah and Kanchana)
* Kalakalalade Kannulu Tahatahalade Oohalu (Lyrics: Arudra; Singers: L. R. Eswari and M. S. Raju)
* Meda Meeda Meda Katti Kotlu Koodabettinatti Kamandu (Singer: P. B. Srinivas and chorus; Cast: ANR and friends)
* Mee Andala Chetulu Kandenu Papam (Lyrics: Arudra; Singer: P. B. Srinivas)
* Preminchi Choodu Pilla Pelladudamu Malla (Singer: Ghantasala Venkateswara Rao)
* Vennela Reyi Ento Chali Chali (Lyrics: Dasarathi, Singers: P. B. Srinivas and P. Susheela)

==External links==
*  
*  

 
 
 
 
 