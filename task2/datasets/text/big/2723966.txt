Walking the Streets of Moscow
 

{{Infobox film name = Walking the Streets of Moscow image = Shagayu.jpg image_size =  caption = Nikita Mikhalkov on the 1964 film poster. director = Georgiy Daneliya producer =  writer = Gennady Shpalikov  narrator =  starring = Nikita Mikhalkov Aleksei  Loktev Evgeniy Steblov  music = Andrei Petrov cinematography = Vadim Yusov editing =  studio = Mosfilm  released = 1963 runtime = 78 minutes country = Soviet Union language = Russian budget = 
}}
Walking the Streets of Moscow ( ,  , Vladimir Basov, Lev Durov, and Inna Churikova. The famous movie theme, performed by Mikhalkov, was written by the composer Andrei Petrov. The film, regarded as one of the most characteristic of the Khrushchev Thaw, premiered at the 1964 Cannes Film Festival and won a prize for the work of cameraman,    Vadim Yusov, best known for his subsequent collaboration with Andrei Tarkovsky.

==Plot summary==
The film opens at a Moscow airport in summer 1963. A young man, Volodya (Aleksei Loktev), calls out to a young woman he sees singing to herself and dancing. 
:– Arriving or departing?
:– Waiting for arrivals.
:– Who is it?
:– My husband.
:– Hes lucky to have someone to meet him.
:– Get married, youll have someone as well.
:– And you are both happy?
:– Yes, we are.
:– It never happens.
:– Believe me, it happens.

Volodya is an aspiring writer from Siberia. His first short story has just been published in the magazine Yunost ("Youth"), and a famous author, Voronin, has invited him to Moscow to discuss his work. In the Moscow Metro Volodya unexpectedly makes a friend, Kolya (Nikita Mikhalkov), who is returning home after a hard night shift. Volodya wants to stay at his old friends home, but he doesnt know where the necessary street is so Kolya decided to help him to find it. 

Unfortunately, a dog bites Volodya near Clean Ponds. Then Kolya decided to help his new friend again – they both came to Kolyas home where Kolya sews Volodyas trousers and introduces him to Kolyas large family. Volodya recognised that his old Moscow friends arent in Moscow (they left for south) and Volodya stays at Kolyas. Then Volodya goes for a walk.
 Main Department Store to buy a suit for a bridegroom and they meet Volodya there (Volody has recently bought a new suit for himself). Then friends decided to buy a present for a bride and they go to the music shop, because the saleswoman, Alyona (Galina Polskikh) is a love interest for Kolya...

==Cast==
*Aleksei Loktev -  Volodya
*Nikita Mikhalkov - Kolka
*Galina Polskikh -  Alyona
*Yevgeni Steblov - Sasha
*I. Titova - Sveta
*Irina Miroshnichenko - Katya, Kolkas sister
*Lyubov Sokolova -  Kolkas mother
*Yekaterina Melnikova - Kolkas grandmother
*Arkadiy Smirnov  -  Aleksey Petrovich Voronov, writer
*Vladimir Basov  - floor polisher
*Rolan Bykov - Man in Park
*Valentina Ananina - ice-cream vendor 
*Veronika Vasilyeva - mistress of ceremonies
*Irina Skobtseva - Nadya 
*Inna Churikova - girl participating in the playful contest
*Marya Vinogradova - woman with a dog
*Gennadiy Yalovich - tour-guide
*Alevtina Rumiantseva - underground guard
*Vladimir Shurupov - Viktor, Kolkas brother
*Danuta Stolyarskaya - Anya, Viktors wife
*Boris Balakin - taxi driver
*Boris Bityukov -Alyonas father
*Pyotr Dolzhanov -  passerby
*Lev Durov -  militsioner
*Arina Aleynikova - girl at the airport
*Igor Bogolybov - military commissar
*Vadim Shilov - underground squabbler Anna Pavlova -  secretary at the military commissariat
*Viktor Volkov - construction foreman
*Viktor Shkurin - thief
*Uno Masaaki - Japanese man
*Svetlana Besedina - girl in the rain
*Oleg Vidov - boy on the bike
*Georgi  Danelia - shoeshine man

==Legacy==
This film was highly beloved by Soviet youth in the early 1960s. Though its plot is a bit naive and unpretentious, it showed how wonderful life was, gives hope and tries to look at the unpleasant things in an optimistic way. The song by the same name from the film is still popular and became the unofficial hymn of Moscow youth. 

The popularity of this film was low in the 1970s but rose again in the 1980s in contrast to contemporaneous "chernukha" ("black") films, gloomy satirical and social dramas with philosophical motifs. Nowadays it is still very famous.

There are new versions of the song by some 1990s Russian rock groups (for example, Nogu Svelo!) and also a film remake, Heat (2006 film)|The Heat, which was commercially successfully but critically panned.

== References ==
 

==External links==
* 
* 
*  

 

 
 
 
 
 
 
 