Kaahalam
{{Infobox film
| name           = Kaahalam
| image          =
| caption        =
| director       = Joshiy
| producer       = Areefa Hassan
| writer         =
| screenplay     = Seema Raveendran Raveendran
| music          = A. T. Ummer
| cinematography =
| editing        = K Sankunni
| studio         = Arifa Enterprises
| distributor    = Arifa Enterprises
| released       =  
| country        = India Malayalam
}}
 1981 Cinema Indian Malayalam Malayalam film, Seema and Raveendran in lead roles. The film had musical score by A. T. Ummer.   

==Cast==
*Prem Nazir
*Nanditha Bose Seema
*Raveendran Raveendran

==Soundtrack==
The music was composed by A. T. Ummer and lyrics was written by B Manikyam, Ramachandran and KG Menon. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ente Karavalayam || P Susheela || B Manikyam || 
|-
| 2 || Kanni vettakkorungi nilkkum || K. J. Yesudas || Ramachandran || 
|-
| 3 || Raajasadassinilakkam || LR Anjali, SP Shailaja || B Manikyam || 
|-
| 4 || Vaanam poovanam || K. J. Yesudas, LR Anjali || KG Menon || 
|}

==References==
 

==External links==
*  

 
 
 

 