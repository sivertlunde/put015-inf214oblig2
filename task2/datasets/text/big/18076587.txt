Ek Jind Ek Jaan
{{Infobox Film
|  name           = Ik Jind Ik Jaan(Movie)
|  image          =
|  caption        = Hero Aryan Vaid and Naghma in a  scene of film Ik Jind Ik Jaan.
|  writer         = 
|  starring       = Raj Babbar   Nagma   Aryan Vaid    Mighty Gill    Deep Dhillion   Donny Kappoor    Kuldip Sharma    Ritu Shivpuri    Aroon Bakshi
|  director       = Chitraarth & Vikram Maini
|  producer       = Hansraj
|  distributor    = Eros International
|  released       = 2006
|  runtime        =  Punjabi
|  music          = Uttam Singh
|  awards         = 
|  budget         = 
}}  2006 in India.

==Story==

Sukhi(Raj Babbar) and Gurjeet (Ritu Shivpuri) become engaged, but trouble brews when the to-be bride discovers that her intended has no desire to procreate. You see, Sukhi has already taken on the  responsibility of  raising his young step-siblings which he feels will be more than enough child rearing for one lifetime. Gurjeet does not seem to be overjoyed by the prospect of raising someone else’s children. Rather than shatter Gurjeet’s dreams of starting a brood of her own, Sukhi decides that it is best to call off the marriage, despite Gurjeet’s brother Kundan (Deep Dhillon)’s offer to pay to have the step-kiddies shipped to a far off hostel (how thoughtful) so that the couple would not have to deal with them. Kundan is not pleased that Sukhi is refusing to retract his un-engagement to his sister and vow’s that the man is destined to be abandoned by his siblings once they have grown up. 

15 years later…..
….and the little ones have grown up. Guddi (Prabhleen)…Channi (Mighty Gill)…and above all, Karma (Aryan Vaid)…Guddi and Channi have become well educated, with Guddi now a college co-ed and Channi a medical student at Hero DMC Clinic.

On the other hand, having spent his childhood bunking school, the  handsome, but educationally unmotivated Karma finds himself working a dead-end, low wage delivery job…Since childhood, slacker Karma has harbored a love for Nimmi (Nagma). Sadly, Nimmi and her family went away to the city many years ago and Karma has neither seen nor heard from her since. In flashback we see the young Karma (Ranvijay) and Nimmi (Rittu) as best friends.While Sukhi is seeing Nimmi and her family off at the train station for their move to the big city in hopes of a better life, young Karma is off trying to scrape up enough money to buy Nimmi a going away present, a set of red bangles which she specifically had wished for.  Since Sukhi, a farmer, and Nimmi’s dad Dhanwant Singh (Arun Bakshi), a tongawala, are good friends they decide to fix the marriage of the two children right then and there, thus little Nimmi leaves on a happy note. Karma, with the red bangles, arrives at the station long after the train has departed and thus is unable to gift his friend the jewelry that he worked all day long to buy. Back to “15 Years Later” we find that Karma is still pining away for Nimmi. Seeing this, Sukhi decides it is time to reconnect with Dhanwant Singh and family and he sets off for the big, bad metropolis. What Sukhi does not realize yet is that Dhanwant has become a rich, successful, and uncaring capitalist who now has no intention of allowing his daughter to marry Karma, who he considers a penniless grunt. Humiliated, Sukhi returns to the village and tries to keep the disappointing news from Karma. But Karma finds out and decides to go to the city to find out what’s what with Nimmi.

Posing as an unemployed stranger he manages to worm his way into a washerman job at Singh’s palatial home, thanks to Dhanwant’s kind wife, who has no idea that the new employee is the now grown-up little boy who had been her daughter Nimmi’s best friend. Into the scene walks Nimmi, all grown up and acting pricey. Her attitude does not deter Karma, who still loves her. Nimmi, however, now has a fiancée, a beefy tennis player named Pal (Sameep Kang). Karma refuses to accept defeat and, while still posing as a household servant, vows to learn why Nimmi has forgotten him and taken another. But when he is in danger of losing Nimmi for good, he reveals the truth about who he is during Nimmi and Pal’s engagement party.

Will Nimmi remember Karma? And if so, would her father ever allow her to marry him?  Will Pal break Karma’s instrument? And if so, how will Karma and Nimmi ever make sweet music together?  Will Sukhi ever get over his insult at the hands of his former friend, Dhanwant? And will his step siblings really abandon him as Kundan had predicted?  And what of Channi and Guddi? Will these two siblings develop plotlines of their own?

==Cast==

{| class="wikitable"
! Star:
! Character:
|-
| Raj Babbar
| Sukhdev Singh 
|-
| Arun Bakshi
| Dhanwant Singh Kang
|-
| Kulbir Bederson
|   mother of nagma
|-
| Deep Dhillon
| Kundan Singh
|-
| Mighty Gill
| Channi Singh
|-
| Gurpreet Guhggi
|   comedy king
|-
| Harbhajan Jabbal
| unknown
|-
| Sumeep Kang
| Pal Jindal
|-
| Donny Kapoor
| Matto D. Singh
|-
| Jatinder Kaur
| unknown
|-
| Nagma
| Nimmi Kang Singh
|-
| B.N. Sharma
| Subedar Kapoor Singh
|-
| Aryan Vaid
| Karamvir Singh
|-
| Prabhleen Sandhu
| Guddi
|}

==Abridged Production Staff==

{| class="wikitable"
|-
! Name:
! Position:
|-
| Daler Mehndi (Mehandi)
| Playback Singer
|-
| Nanadlal Prasad
| Film Editor
|-
| Preeti Uttam Singh
| Playback Singer
|-
| Deepak Maini
| Producer
|- Hans Raj
| Playback Singer
|-
| Sonu Nigam
| Playback Singer
|-
| Pawni Pande
| Playback Singer
|- first song her life
| Jaspinder Narula
| Playback Singer
|-
| Chitraarth
| Director
|-
| Vikram Maini  
| Co-Producer
|-
| Bali Brahmabhatt
| Playback Singer
|-
| Dinesh Telkar
| Cinematography
|-
| Uttam Singh
| Original Music
|-
| E.P. Subash Singh
| Co-Producer
|-
| Udbhav Ojha
| Original Music
|}

==References==
* 
* 
* 
* 
* 
* 

 
 
 