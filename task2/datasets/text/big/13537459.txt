How to Hook Up Your Home Theater
  
{{Infobox Hollywood cartoon| cartoon_name   = How to Hook Up Your Home Theater series         = Goofy image          = How to Hook Up Your Home Theater poster.jpg caption        = Poster for How to Hook Up Your Home Theater director       = Kevin Deters  Stevie Wermers producer       = Tamara Boutcher  Chuck Williams  John Lasseter story_artist   = Kevin Deters  Wilbert Plijnaar  Stevie Wermers  Dan Abraham voice_actor    = Bill Farmer  Corey Burton musician       = Michael Giacchino animator  Eric Goldberg  Dale Baer studio         = Walt Disney Pictures Walt Disney Animation Studios distributor  Walt Disney Studios Motion Pictures release_date   =   color_process  Digital
|runtime        = 6 minutes country        = United States movie_language = English
}} cartoon from home theater system, to watch American football|football.

==Production== Toon Boom Harmony for the animation, while other animators such as Mark Henn and Andreas Deja continued to work in the traditional method with pencil on paper.   It was reported on Animation World Network  that about 50% of the short was done using the new paperless technique.

==Release== The Game Animation Show of Shows in 2007.

==References to other media==
*The two football teams are called the Dawgs and the Geefs, both of which refer to two of Goofys pseudonyms over the years, Dippy Dawg (his earliest incarnation) and George Geef (the name he went by in his "everyman" shorts of the 1950s).
*When the box arrives, a label on the box says "Dopey Digital data|Digital", a reference to Dolby Digital and Snow White. The construction number on the side of the box is M1C-K3Y MO-U5E, a reference to Mickey Mouse.
*On Goofys shelf there is a photo of Walt Disney, Clarabelle Cow and Goofys first appearance from Mickeys Revue.

==External links==
*  at Walt Disney Animation Studios
* 
*  at The Encyclopedia of Disney Animated Shorts
* 
* 
* 

==References==
 

 

 

 
 
 
 