Taxi 3
 
{{Infobox film 
 | name = Taxi 3
 | image = Taxi 3 film.jpg
 | caption = Theatrical release poster
 | director = Gérard Krawczyk 
 | writer = Luc Besson
 | starring = Samy Naceri Frédéric Diefenthal Marion Cotillard 
 | producer = Luc Besson 
 | distributor = EuropaCorp
 | budget = €14,500,000
 | gross = €50,443,870 
 | released =   
 | runtime = 84 minutes
 | country = France
 | language = French
}}
Taxi 3 is a 2003 French comedy film directed by Gérard Krawczyk. It is the sequel to Taxi 2 and was followed by Taxi 4.

==Plot==
A group of thieves calling themselves the Santa Claus gang are wreaking havoc, and the Marseille police are, as usual, unable to keep up. Superintendent Gibert (played by Bernard Farcy) is distracted by a Chinese journalist (Bai Ling) writing a story on his squad. Detective Emiliens wife, Petra, has just announced that shes pregnant and taxi driver Daniel (Samy Naceri) is in the midst of a relationship crisis. His long-suffering girlfriend Lilly has walked out on him after finding him customising his taxi at four oclock in the morning, and complaining that their house has become a mere garage. After a string of mistakes in which the thieves outsmart the police time and time again, a journalist is kidnapped. It is revealed that the journalist is the leader of the Santa Claus gang. The police go in search, but Emilien is captured after another botched attempt to arrest them. The journalist sets a trap; she leaves Emilien tied to a chair in an old warehouse, directly in the path of a giant ball which will crush him five minutes later. At the very last moment, Daniel rushes in with his taxi and rescues him. They track the gang to their hideout in the Swiss mountains, where the journalist and her accomplices are arrested by a crack team of Alpine troops. Gibert lands in an ice-bound lake after leaping from an aircraft with them. Petra gives birth, Daniel proposes to Lilly and a Gibert is seen being pushed around in a wheelchair covered in ice.

==Cast==
* Samy Naceri as Daniel Morales
* Frédéric Diefenthal as Émilien Coutant-Kerbalec
* Bernard Farcy as the Superintendent Gibert
* Bai Ling as Qiu
* Emma Wiklund as Petra
* Marion Cotillard as Lilly Bertineau
* Edouard Montoute as Alain
* Jean-Christophe Bouvet as the General Edmond Bertineau
* Patrice Abbou as Rachid
* Claude Sese as Planton
* David Gabison as the Minister
; Cameos cameo at the beginning of the film as a taxi passenger with his voice dubbed as he cannot speak French.

== Production ==
The opening titles are an amusing spoof of those featured in the James Bond movies, and there are several nods to the 007 canon throughout the film e.g. Daniels rotating number plate and the ski chase at the films climax.

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 

 