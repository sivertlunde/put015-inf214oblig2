The Place of Honour
{{Infobox film
| name           = The Place of Honour
| image          =
| caption        =
| director       = Sinclair Hill
| producer       =
| writer         = Ethel M. Dell (novel) William J. Elliott
| starring       = Hugh Buckler Madge White Miles Mander
| cinematography =
| editing        =
| studio         = Stoll Pictures
| distributor    = Stoll Pictures
| released       =  
| runtime        =
| country        = United Kingdom
| awards         =
| language       = Silent English intertitles
| budget         =
}} silent adventure film directed by Sinclair Hill and starring Hugh Buckler, Madge White and Miles Mander.  It was based on a novel by Ethel M. Dell set in British India.

==Cast==
* Hugh Buckler as Maj. Eustace Tudor  
* Madge White as Mrs. Tudor  
* Pardoe Woodman as Lt. Philip Trevor  
* Miles Mander as Lt. Devereaux  
* M. Gray Murray as Capt. Raleigh  
* Ruth Mackay as Mrs. Raleigh  
* Bob Vallis as Pvt. Archie Smith

==References==
 

==Bibliography==
* Low, Rachael. History of the British Film, 1918-1929. George Allen & Unwin, 1971.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
  
 
 
 

 