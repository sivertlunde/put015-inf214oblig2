Hollywood Hotel (film)
{{Infobox film
| name           = Hollywood Hotel
| image          = Hollywood Hotel - Poster.jpg
| image_size     = 225px
| caption        = theatrical release poster
| director       = Busby Berkeley
| producer       = Uncredited:  Samuel Bischoff Bryan Foy
| screenplay     = Jerry Wald Maurice Leo Richard Macaulay
| story          = Jerry Wald Maurice Leo Rosemary Lane Lola Lane  Hugh Herbert Ted Healy Glenda Farrell Johnnie Davis 
| music          = Songs:    Heinz Roemheld	 George Barnes (musical numbers)
| editing        = George Amy
| studio         = First National Pictures
| distributor    = Warner Bros.
| released       =  
| runtime        = 109 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Rosemary Lane, Grant Mitchell and Edgar Kennedy. 

The film was based on the popular     The films recreation of the  program features Louella Parsons, Frances Langford, Raymond Paige and His Orchestra, Jerry Cooper, the announcer Ken Niles, Duane Thompson and Benny Goodman and His Orchestra.

Hollywood Hotel, the film, is now best remembered for the featured song and opening number "Hooray for Hollywood" by Johnny Mercer and Richard A. Whiting, sung in the film by Davis and Langford, accompanied by Goodman and his orchestra. The song has become a standard part of the soundtrack to movie award ceremonies, including the Academy Awards. Mercers lyrics contain numerous references, often satirical, to the movie industry and the path to film stardom.

==Plot==
Saxophone player and singer Ronnie Bowers (Dick Powell), is on his way to Hollywood, having been signed to a ten-week contract by All Star Pictures. At the airport, his former employer, Benny Goodman, and his band give him a big sendoff, performing "Hooray for Hollywood".
 Grant Mitchell) Rosemary Lane), who has already worked as a stand-in for Mona. For her escort, Bernie chooses an unsuspecting (and starstruck) Ronnie.

The charade works. Everyone, from Ronnie to Louella Parsons to the radio host at the premiere (Ronald Reagan) is fooled. Things take an unexpected turn when Ronnie and Virginia begin to fall in love, wading in a fountain pond and singing "Im Like a Fish Out of Water".

The next day, Bernie takes Ronnie to lunch at the restaurant where Virginia is working as a waitress, to break the news of his dates real identity. Ronnie and Virginia begin dating.
 William Davidson) is impressed and offers him a job. Ronnie is disappointed to learn, however, that he will not be acting, only. Kelton dubbing the singing for Monas longtime screen partner, Alex Dupre (Alan Mowbray).

Dupres "singing" impresses the audience at the preview. When Louella Parsons invites him to perform on her radio program, he accepts without thinking. Desperate, All Star Pictures pays Ronnie an exorbitant fee to sing for the actor. However, Ronnie has his own ideas. Virginia (posing as Mona) picks up Dupre in a limousine driven by Fuzzy. The pair drive him out into the countryside so he misses the program. Ronnie substitutes for Dupre and is a hit, so Faulkin decides to re-sign him, at a larger salary.

==Cast==
  
*Dick Powell as Ronnie Bowers Rosemary Lane as Virginia Stanton
*Lola Lane as Mona Marshall
*Hugh Herbert as Chester Marshall, Monas father
*Ted Healy as Fuzzy Boyle 
*Glenda Farrell as Jonesy, Monas assistant
*Johnnie Davis as Georgia
*Louella Parsons as herself
*Alan Mowbray as Alexander Dupre
*Mabel Todd as Dot Marshall, Monas sister
*Frances Langford as Alice Crayne
*Jerry Cooper as himself
*Ken Niles as himself
*Duane Thompson as himself
*Allyn Joslyn as Bernie Walton
  Grant Mitchell as B. L. Faulkin
*Edgar Kennedy as Callaghan, the drive-in owner
*Fritz Feld as the Russian, a restaurant patron
*Curt Bois as Butch, the dress designer
*Perc Westmore as himself
*Eddie Acuff as Joe, the camerman
*Clinton Rosemond as Tom, African-American singer William Davidson as Director Walter Kelton 
*Wally Maher as Assistant Director Drew
*Georgie Cooper as seamstress
*Libby Taylor as Cleo, Monas maid
*Joe Romantini as Waiter
*Paul Irving as Bramwell
*Raymond Paige and his Orchestra as themselves
*Benny Goodman and His Orchestra as themselves
 

Cast notes:
*Louella Parsons, a noted gossip columnist of the time, created the concept of Hollywood Hotel for the radio, and appears in the film as herself.   It was her screen debut. 
*The Benny Goodman Orchestra at this time included drummer     The strong reaction of the bands fans to its appearance in the film helped to convince Goodman to do the Carnegie Hall concert that had been suggested by his publicist, Wynn Nathanson.  Goodman had been concerned that it would be perceived as a publicity stunt. 
*Ted Healy is perhaps best known for creating the vaudeville act which later evolved into The Three Stooges. Hollywood Hotel was released in January 1938, less than a month after Healys untimely death.  Healy suffered a heart attack a few hours after attending a preview of the film. 
*Lola Lane, who plays Mona Marshall, and Rosemary Lane, who plays Marshalls stand-in, were sisters.  Another sister, Priscilla Lane, was an even more successful film actress. 
*Ronald Reagan makes his second film appearance in Hollywood Hotel, uncredited, as the radio host at a film premiere.
*Both Carole Landis, as a hatcheck girl, and Susan Hayward, as a starlet, appear in the film uncredited.  It was Haywards film debut. 

==Production==
Warner Bros. originally wanted Bette Davis to play both Mona Marshall and her stand-in, but Davis managed to convince them that it was not a good idea. 

The studio was sued by both the Campbell Soup Company, who sponsored the Hollywood Hotel radio program, and by the hotel itself, for using the name without authorization.   The Hollywood Hotel in its heyday had attracted the royalty of Hollywood, such as Mary Pickford and Douglas Fairbanks, but it had fallen in prominence by the time this film was made.  Some exteriors of the hotel appear in the films.  The hotel no longer exists, in its place is the Dolby Theatre, from where the Academy Awards presentations have originated since 2001. 

==See also==
*Hollywood Hotel (radio program)
*Ronald Reagan filmography

==References==
Notes
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 