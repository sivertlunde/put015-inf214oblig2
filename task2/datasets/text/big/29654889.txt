Supermen Against the Orient
{{Infobox film
| name           = Supermen Against the Orient
| image          = Supermen Against the Orient.jpg
| image_size     =
| caption        = Cantonese Release Poster
| director       = Bitto Albertini Kuei Chih Hung Jackie Chan(action) Corey Yuen(action)
| producer       = Tino Biasia Attilio Fattori Run Run Shaw	
| writer         = Bitto Albertini Gino Capone
| narrator       =  Mars Jackie Chan Yuen Biao Jacques Dufilho Alberto Farnese
| music          = Nico Fidenco
| cinematography = Pier Luigi Santi
| editing        = Fausto Ulisse
| distributor    = Shaw Brothers
| released       =  
| runtime        = 100 minutes
| country        = Hong Kong Italy Mandarin  Italian
| budget         =  gross      = 
}} Hong Kong-Cinema Italian co-production 1967 film I Fantastici tre supermen.   The film is notable for featuring a cameo appearance by a young Jackie Chan who here also already worked as choreographer     It was a commercial success in Italy and also in Hong Kong.   
==Plot==
FBI agent Robert Wallace has orders find six disappeared Americans in Bangkok. The trace leads to Hong Kong. During his investigation he makes new friends who stand by his side when he gets attacked by henchmen.

==Cast==
* Robert Malcolm: Robert Wallace 
* Antonio Cantafora: Max  Salvatore Borghese: Jerry 
* Lo Lieh: Master Tang 
* Shih Szu: mysterious young lady
* Tung Lin: Chen-Loh 
* Alberto Farnese: Colonel Roger 
* Jacques Dufilho: American ambassador
* Isabella Biagini: the ambassadors wife

==Reception==
Kevin Lyons from "EOOFTV Reviews" stated the film presented "lots of very silly" moments and an "embarrassing collection of worn-out gags". But he considered it amusing for slapstick  fans.    Andrew Pragasam from "The Spinning Image" described the film as a "lively, if meandering romp" with a "memorably silly theme song".  

==References==
 

==External links==
*  
*  
*  

 
 
 
 