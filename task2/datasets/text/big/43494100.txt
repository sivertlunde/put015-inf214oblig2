Pushin' Up Daisies
{{Infobox film
| name           = Pushin Up Daisies
| image          =
| alt            =
| caption        =
| director       = Patrick Franklin
| producer       = Patrick Franklin
| screenplay     = Patrick Franklin
| story          = Patrick Franklin Robert J. Wagstaff
| starring       =
| music          =
| cinematography = Tom Pritchard
| editing        =
| studio         = Pushin Up Daisies
| distributor    =
| released       =   }}
| runtime        = 87 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Pushin Up Daisies is a 2010 American comedy film written and directed by Patrick Franklin.  It stars Sheehan OHeron and Orlando Vicente as documentary filmmakers Darren and Anthony, who film Darrens brother, Rusty (Simon Sorrells).  In the midst of their documentary, a zombie attack breaks out.  It premiered at the Atlanta Film Festival on April 16, 2010.

== Plot ==
Documentary filmmaker Darren convinces his friend Anthony to travel with him back to his hometown in Georgia and make a film about Darrens brother, Rusty, a flower deliveryman.  With the help of the blind voice actor Mr. Emerson, they follow the reluctant subject throughout his delivery schedule despite his objections.  While filming, a zombie outbreak occurs, and Rusty insists that they continue the documentary.  Annoyed with the disruption from zombies, Rusty attempts to shoot around them and maintain his creative vision, which does not allow for zombies in his artistic documentary.

== Cast ==
* Sheehan OHeron as Darren
* Orlando Vicente as Anthony
* Simon Sorrells as Rusty
* Ken Osbourn as Mr. Emerson

== Production == found footage.   Franklin conceived of the idea for the film while driving through a quiet town on the way to shoot a documentary.  He began to brainstorm ideas with a friend, eventually deciding on a story that involves a documentary filmmaker who refuses to acknowledge a supernatural disaster.  Shooting began in 2007 and ended in 2009. 

== Release ==
Pushin Up Daisies premiered at the Atlanta Film Festival on April 16, 2010.   It was later shown at Athfest on July 25, 2010.   Franklin turned to Kickstarter to raise funds for distribution.  

== Reception == mockumentaries and zombie films. 

== References ==
 

== External links ==
*  

 
 
 
 
 
 