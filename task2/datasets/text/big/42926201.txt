Clarissa (film)
{{Infobox film
| name = Clarissa 
| image =
| image_size =
| caption =
| director = Gerhard Lamprecht
| producer = Gustav Althoff
| writer =  Ela Elborg   Georg C. Klaren   Georg Rothkegel   Thea von Harbou
| narrator =
| starring = Sybille Schmitz   Gustav Fröhlich   Gustav Diessl   Charlotte Radspieler
| music = Giuseppe Becce 
| cinematography = Karl Hasselmann 
| editing =  Johanna Meisel    
| studio = Aco-Film 
| distributor = Various
| released = 8 September 1941
| runtime = 79 minutes
| country = Germany  German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Clarissa is a 1941 German romance film directed by Gerhard Lamprecht and starring Sybille Schmitz, Gustav Fröhlich and Gustav Diessl.  Schmitz plays the domineering manager of a bank who eventually falls in love with one of the other employees.

==Cast==
* Sybille Schmitz as Clarissa von Reckwitz  
* Gustav Fröhlich as Lutz Bornhoff  
* Gustav Diessl as Bankdirektor Feerenbach  
* Charlotte Radspieler as Lotte Becker  
* Werner Scharf as Paul Becker 
* Elga Brink as Ingeborg von Stahl  
* Gerhard Dammann as Lohnbuchhalter Köbner  
* Josefine Dora as Friederike  
* Olga Engl as Clarissas Tante Ernestine 
* Albert Florath as Ferdinand von Reckwitz  
* Liselotte Fülster as Lohnbuchhalterin Erna Wunderlich  
* Friedl Haerlin as Mondäne Kundin  
* Erika Helmke as Ilse Moll 
* Melanie Horeschowsky as Clarissas tante Fränzchen  
* Käthe Jöken-König as Reinigungskraft der bank  
* Viggo Larsen as Bankangestellter Stammler  
* H.A. Löhr as Banklehrling Waldemar 
* Edith Oß as Ursel Brennecke   Klaus Pohl as Bankbuchhalter Huhn  
* Maria Seidler as Edith Feerenbach  
* Julia Serda as Frau von Reckwitz  
* Werner Stock as Krümel  
* Ada Tschechowa as Lohnbuchhalterin Lore schneider

== References ==
 

== Bibliography ==
* Hake, Sabine. Popular Cinema of the Third Reich. University of Texas Press, 2001.

== External links ==
*  

 

 
 
 
 
 
 
 


 