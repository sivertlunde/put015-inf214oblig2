The Hooligan Factory
{{Infobox film
| name           = The Hooligan Factory
| image          = 
| caption        = 
| alt            =
| director       = Nick Nevern
| producer       = Will Clarke
| writer         = Michael Lindley, Nick Nevern Tom Burke Steven ODonnell Morgan Watkins Josef Altin Leo Gregory Keith-Lee Castle
| music          = Tom Linden
| cinematography = Ali Asad
| editing        = Lewis Albrow
| studio         = Altitude Films Entertainment
| distributor    = Universal Pictures
| released       =  
| runtime        = 90 minutes
| country        = United Kingdom
| language       = English
}} spoof film parodies titles British hooligan The Firm, The Football Factory, Rise of the Footsoldier, I.D. (1995 film)|I.D., Green Street and Cass (2008 film)|Cass.

==Plot==
Danny (Jason Maza) wants something more from life having been expelled from school and living in his grandfathers flat, he longs to live up to the image of his estranged father Danny Senior (Ronnie Fox). Sent to prison for force feeding a judge his own wig Danny Senior was a legend and Danny is looking for a way to emulate his fathers achievements and rise to be "top boy". Meanwhile legendary football hooligan Dex (Nick Nevern) is about to be released. Dex is on a quest of his own, one of vengeance against his nemesis and rival firm leader The Baron (Keith-Lee Castle). But when Danny and Dexs paths cross they embark on a journey as old as hooliganism itself. Dex, Danny and The Hooligan Factory travel the length of the country on a mission to re-establish their firms glory days. However, the police are closing in and we get a sense that the Hooligan Factorys best days may be behind them, but with Danny on their side, and Dex finding his old form who knows where this may lead. After all... Its a funny old game. 

==Production== West Ham Chloe Sims Charlie Clapham also make minor appearances. Dexter Fletcher also filmed a scene but it was cut from the final take. 

==Cast==
 
* Jason Maza as Danny
* Nick Nevern as Dex Tom Burke as Bullet
* Ray Fearon as Midnight Steven ODonnell as Old Bill
* Morgan Watkins as Trumpet
* Josef Altin as Weasel
* Leo Gregory as Slasher
* Keith-Lee Castle as The Baron
* Ronnie Fox as Danny snr
* Lorraine Stanley as Sharon
* Juliet Oldfield as Karen
* Alex Austin as Fanta
* Ian Lavender as Grandad Albert
* Danny Dyer as Jeff
* Craig Fairbrass as Mickey
* Tamer Hassan as Jack
* Cass Pennant as Cass (himself)
* Julian Dicks as Slashers Top Boy Chloe Sims as Chloe (herself)
* Tony Denham as Millwalls Top Boy Charlie Clapham as Freddy the Nonce
 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 