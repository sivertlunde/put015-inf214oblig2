Varning för Jönssonligan
{{Infobox film
| name           = Varn!ng för Jönssonligan
| original name = Beware of the Johnson Gang
| image          = Jönsson Varning.jpg
| image_size     = 190px
| caption        =
| director       = Jonas Cornell
| producer       = Ingemar Ejve
| writer         = Henning Bahs, Rolf Börjlind, Erik Balling
| starring       = Gösta Ekman Ulf Brunnberg Nils Brandt Siw Malmkvist
| music          = Ragnar Grippe
| cinematography = Roland Sterner
| editing        = Solveig Nordlund
| distributor    = Svensk Filmindustri
| released       =  
| runtime        = 89 min
| country        = Sweden Swedish
| budget         =
| gross          =
}} Swedish movie about the gang Jönssonligan made in 1981. Filming  dates:  July 1981 &ndash; September 1981

It was the first Swedish adaptation of one of the original Danish Olsen Gang movies. Somewhat ionically, it was based on the sixth Olsen Gang movie, Olsen-bandens sidste bedrifter, which was intended as the last one when it was made.

==Cast==
*Gösta Ekman as Charles-Ingvar "Sickan" Jönsson
*Ulf Brunnberg as Ragnar Vanheden
*Nils Brandt as Rocky
*Siw Malmkvist as Eivor
*Per Grundén as Wall-Enberg Jr.
*Jan-Olof Strandberg as Svensson
*Tomas Norström as Holm
*Weiron Holmberg as Biffen
*Johannes Brost as robber
*Peter Hüttner as robber

== External links ==
* 
* 

 

 
 
 
 
 


 
 