Shock Troops (film)
 
{{Infobox film
| name           = Shock Troops
| image          = Shock Troops (film).jpg
| caption        = Film poster
| director       = Costa-Gavras
| producer       = Costa-Gavras
| writer         = Jean-Pierre Chabrol Costa-Gavras
| starring       = Charles Vanel
| music          = Michel Magne
| cinematography = Jean Tournier
| editing        = 
| distributor    = 
| released       =  
| runtime        = 110 minutes
| country        = France Italy
| language       = French
| budget         = 
}}

Shock Troops ( ) is a 1967 French-Italian drama film directed by Costa-Gavras. It was entered into the 5th Moscow International Film Festival.    Film producer Harry Saltzman has a "presented by" credit. 

==Plot==
Set in central France, the film follows French resistance fighters who press the battle on the Germans. Along the way, they break into a prison and release some German prisoners, but discover there may be a spy deliberately planted to flush them all out.

==Cast==
* Charles Vanel as Passevin
* Bruno Cremer as Cazal
* Jean-Claude Brialy as Jean
* Michel Piccoli as The Extra Man
* Gérard Blain as Thomas
* Claude Brasseur as Groubec
* Jacques Perrin as Kerk
* François Périer as Moujon
* Claude Brosset as Ouf
* Pierre Clémenti as Lucian
* Michel Creton as Solin
* Paolo Fratini as Philippe
* Julie Dassin as Girl
* Nino Segurini as Lecocq
* Marc Porel as Octave 

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 