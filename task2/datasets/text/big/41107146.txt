My Sister in Law
{{Infobox film
 | name = La Pretora (The Magistrate)
 | image = My Sister In Law.png
 | caption =
 | director = Lucio Fulci
 | writer =  Franco Marotta Laura Toscano Franco Mercuri (dialogue)
 | starring = Edwige Fenech Giancarlo Dettori Gianni Agus
 | music = Nico Fidenco
 | cinematography = Luciano Trasatti    
 | editing = Ornella Micheli 	 
 | producer = Roberto Sbarigia (executive producer) 
 }} 1976 Cinema Italian commedia sexy allitaliana directed by Lucio Fulci.   

== Plot ==
Viola Orlando, a magistrate of a little town in Veneto, is inflexible and severe. Some speculators, sworn enemies of the magistrate, discover that the twin sister of Viola, Rosa, is a famous pornstar, so they try to use her to discredit Viola.

== Cast ==
*Edwige Fenech: Viola Orlando/ Rosa Orlando
*Giancarlo Dettori: Count Renato Altieri
*Gianni Agus: Angelo Scotti
*Oreste Lionello: Francesco Lo Presti
*Raf Luca: Raffaele Esposito
*Mario Maranzana: Lawyer Bortolon
*Carletto Sposito: Prosecutor 
*Luca Sportelli: Toni 
*Pietro Tordi: Pavanin 
*Marina Hedman: Regina
*Lucio Fulci: Gas Station Attendant

==References==
 

==External links==
* 
 

 
 
 
 
 
 


 
 