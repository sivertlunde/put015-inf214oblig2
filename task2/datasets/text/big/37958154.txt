Ombak Rindu
 
 
{{Infobox film
| name           = Ombak Rindu
| image          =
| caption        = Poster filem Ombak Rindu
| director       = Osman Ali
| producer       = Sharad Sharan
| writer         = Fauziah Ashari
| starring       = Aaron Aziz   Maya Karin   Lisa Surihani   Bront Palarae ,  Qamaruddin Fauzi
| music          =
| cinematography =
| editing        = Affandi Jamaludin
| distributor    =
| released       =  
| runtime        =
| country        = Malaysia Malaysian
| budget         = RM 1.56 million
| gross          = RM 10.9 million
}}

Ombak Rindu (Love Waves) is a Malaysian 2011 romance drama film directed by Ali Osman.

==Plot==
Izzah (Maya Karin) and Hariz (Aaron Aziz) are from two different worlds and  they have to face many hurdles before finding happiness. Izzah is a village girl who is sold by her uncle to an entertainment club in Kuala Lumpur to become a prostitute. Hariz is  a young man who is the son of Madam Sufiah Hariz (Azizah Mahzan), the owner of the business empire Sufiah Catering. He buys  Izzah to be his mistress, but Izzah pleads with him to marry her instead to legalise their relationship.

Hariz agrees to the terms, provided that  Izzah not claim any rights as a wife. However, the  rough and hot-tempered Hariz finds himself falling in love with Izzah who is full of tenderness.Their relationship is frowned upon by Madam Sufiah. Hariz is then forced to marry Mila Amylia(Lisa Surihani), a childhood friend and now a famous model and actress, after being pressured by his mother. When Hariz falls into a coma in the hospital due to a road accident, Madam Sufiah drives Izzah out of the bungalow and confiscates the car keys and mobile phones.

Once out of the hospital, Madam Sufiah lies to Hariz about Izzah having forgotten about him and having a relationship with another man. She procduces a photograph, apparently taken by a private detective, that shows Mail, Izzahs childhood friend, appearing intimate with Izzah (in actual fact, Izzah is helping Mail get a job in KL and has  put him up at Pak Dolahs house). Hariz drives over to Pak Dolahs house and sees Mail and Izzah there, thereby confirming his mothers allegations. He gets angry at Izzah and tells her that he is leaving her. As he drives off in his car, Izzah chases him and falls. She starts to bleed, implying that she may be suffering a miscarriage. At the hospital, she and baby are found to be safe.

Pak Dollah, arrives at Harizs home and confronts Madam Sufiah. Hariz learns of the secret pact that Pak Dolah and Madam Sufiahs husband made. Hariz has been submitted to the Madam Sufiah for adoption. Hariz feels remorseful and misses Izzah who has gone to her village. He goes to her village to ask for forgiveness. Izzah readily accepts him.  Mila appears at the village and after realising that Hariz and Izzah are very much in love, she asks Hariz to divorce her. The film was ends with Izzah and Hariz leading a simple happy life together.

==Cast==
* Aaron Aziz as Hariz - son of Madam Sufiah, playboy
* Maya Karin as Izzah - village girl, Harizs wife
* Lisa Surihani as Mila Amylia
* Bront Palarae as Mail - Izzahs friend from her village
* Julia Ziegler as Wafiah
* Azizah Mahzan as Madam Sufiah - Harizs mother
* Zaidi Omar as Pak Dollah
* Normah Damanhuri as Mak Jah

=== Reviews ===
(to edit) The film received poor reviews from critics. This can be seen in the story of the tragedy experienced by Ustazah Izzah. Connect tragedy stories - stories such as love prevails West because the humanity within the highlighted character Hariz, starring Aaron Aziz. This film is not to reinforce the love for religion even character Izzah is a village ustazah. Highlighting the thick soul sisters are not displayed properly in the story.

On the technical side, no doubt that this film highlights the best cinematography techniques. Apparently, Ali Osmans touch in describing the story adapted from the novel has succeeded. Illustrations in this film gives pleasure to the audience to watch the film (to edit)

Acting displayed by the actors  Malaysia should be commended. Characterisation by these actors did not disappoint. Izzahs character played by Maya Karin, successfully silenced critics who had doubts about Maya Karins acting abilities. Aaron Aziz proved to be a versatile actor who was able to emote as the character Hariz, a man who is has a tough exterior but a gentle soul within. Lisa Surihani managed to get out of her comfort zone when other characters act out of her comfort zone. Bront Palarae, even though his character is not a lot, but can be felt when convected by Izzah but faithfully waited until he had found a new love. Actor - senior actors like the lead character Mahzan Azizah mother Hariz, told this to remind us of the Virgin Mother character in Mother-in-law command Tan Sri P. Ramlee.

==Controversy==
Controversy occurred when the author of this novel, Fauziah Ashari, claimed she did not get paid accordingly based on the collection of broken film stage. In addition, she also stated that she was not called on to be an advisor in the production of the script but was not called to be the screenplay writer of the film

But her statement was repulsed by Karangkraf Media Group Managing Director, Datuk Hussamuddin Yaacub. He stated that the process of adapting the novel to film scripts had been conducted since 2009. He also refuted the  allegations thatnwriters did not get adequate pay for the amount of money he had given nearly RM 200,000 as royalty. Additionally, the author of the book will benefit from the increased sales revenue due to the commercial success of the film.

==Achievements==
* Best Director = Osman Ali
* Best Actress = Maya Karin
* Best Cinematography = Khalid Zakaria|
* Award for Greatest Director = Osman Ali
* Greatest Hero Award = Aaron Aziz
* Greatest Heroine Award = Maya Karin
* Awards Greatest Movie Songs = Hafiz & Adira - "Love Waves"

==External links==
*  
*   at Cinema Malaysia
*   in FINAS

 
 
 