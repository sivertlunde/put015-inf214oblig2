A Romance of the Western Hills
{{Infobox film
| name           = A Romance of the Western Hills
| image          = 
| image_size     = 
| caption        = 
| director       = D. W. Griffith
| producer       = 
| writer         = Stanner E. V. Taylor
| narrator       = 
| starring       = Mary Pickford Blanche Sweet
| music          = 
| cinematography = G. W. Bitzer
| editing        = 
| distributor    = Biograph Company
| released       =  
| runtime        = 16 minutes
| country        = United States  Silent English intertitles
| budget         = 
}}
 short silent silent drama film directed by D. W. Griffith, starring Mary Pickford and Blanche Sweet. A print of the film survives in the film archive of the Library of Congress.   

==Cast==
* Mary Pickford as Indian
* Alfred Paget as Indian
* Arthur V. Johnson as Indian
* Kate Bruce as Tourist
* Dell Henderson as Tourist
* Blanche Sweet Charles West as The Nephew Dorothy West as Tourist
* Kathlyn Williams as Second Woman

==See also==
* D. W. Griffith filmography
* Mary Pickford filmography
* Blanche Sweet filmography

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 

 