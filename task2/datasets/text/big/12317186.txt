Hawayein
{{Infobox Film |
 name = Hawayein|
 image = HawayeinPoster.jpg |
 caption = Movie poster for Hawayein |
 writer = |
 starring = Babbu Mann, Mahi Gill, Sunny Mann, Ammtoje Mann |
 director = Ammtoje Mann |
 producer = Nippy Dhanoa |
 distributor = Baldev Bhatti |
 released = 8 June 2003 |
 runtime = |
 language = Hindi, Punjabi |
 music = Babbu Mann |
 awards = |
 budget = |
}}

Hawayein ( . The movie stars Babbu Mann, Ammtoje Mann, and Sunny Mann. The films tagline is Some winds don’t blow… Yet take away everything. The film, set against the backdrop to the 1984 anti-Sikh riots|anti-Sikh riots of 1984 is banned in the Indian states of Delhi and Punjab. 

== Plot ==
This film depicts real life events seen through the eyes of its central protagonist ‘Sarabjit’, a music loving student who, disillusioned after the riots, becomes one of Indias "most-wanted" terrorists. This film attempts to explore the causes underlying the alienation of the youth of Punjab and the turmoils suffered by their families.

It has been shot on 35mm cinemascope using the latest camera Arri 435 & was released with Dolby Digital sound. It was slated for an April release & has become a much hyped media event and a memorable celluloid explosion on the Hindi screen.

==Cast==
* Ammtoje Mann
* Babbu Mann
* Mukul Dev
* Tom Alter
* Mukesh Tiwari
* Sardar Sohi
* Kamini Kaushal
* Kulbhushan Kharbanda
* Mahi Gill
* Gursahib singh cheema
* Rama Vij
* Anandee Tripathi

==Tracks==
* Pabb Chak De – Babbu Maan
* Nachoongi Saari Raat – Jaspinder Narula, Babbu Maan
* Aaja O Yaara – Sukhwinder Singh, Preeti Uttam, Babbu Maan
* Teri Yaad – Sukhwinder Singh, Preeti Uttam, Babbu Maan
* Meri Muskaan – Babbu Maan
* Meri Muskaan – 2 – Babbu Maan
* Hawayein – Sadhana Sargam, Babbu Maan
* Bhangra Paa Laiye – Sadhana Sargam, Bhavdeep, Babbu Maan

==Crew==
* Producer : Baldev Bhatti, Babbu Maan, Nippy Dhanoa, Amrinder Singh
* Executive Producer : Poornima Battacharya
* Director : Ammtoje Mann
* Music Director : Babbu Mann
* Lyricist : Babbu Mann
* Cinematographer : Nazir Khan
* Story Writer : Ammtoje Mann
* Dialogue Writer : Harjeet Singh
* Screenplay Writer : Ammtoje Mann
* Editor : Sanjay Jaiswal
* Art Director : Anant Shinde
* Background Score : Sukhwinder Singh
* Choreographers : Remo & Pony Verma

== External links ==
*  

==References==
 

 
 
 
 
 


 