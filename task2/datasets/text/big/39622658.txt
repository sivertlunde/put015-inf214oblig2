Love Between the Raindrops
{{Infobox film
| name           = Love Between the Raindrops
| image          = 
| caption        = 
| director       = Karel Kachyňa
| producer       = 
| writer         = Jan Otcenásek Vladimír Kalina Karel Kachyňa
| starring       = Eduard Cupák
| music          = 
| cinematography = Jan Curík
| editing        = 
| distributor    = 
| released       =  
| runtime        = 126 minutes
| country        = Czechoslovakia
| language       = Czech
| budget         = 
}}
 Best Foreign Language Film at the 53rd Academy Awards, but was not accepted as a nominee. 

==Cast==
* Eduard Cupák as Narrator (voice)
* Vladimír Mensík as Vincenc Bursík - cobbler
* Lukás Vaculík as Kajda Bursík - son
* Michal Dlouhý as Kajda Bursík - child
* Jan Hrusínský as Pepan Bursík - son
* David Vlcek as Pepan Bursík - child
* Zlata Adamovská as Vera Bursíková - Daughter
* Lucie Zednícková as Vera Bursíková - Child (as Lucie Bártová)
* Eva Jakoubková as Fanka Bursíková - Mother
* Therese Herz as Pája (as Tereza Pokorná)
* Rudolf Hrusínský as Druggist
* Miroslav Machácek as Ráb - Druggist friend

==See also==
* List of submissions to the 53rd Academy Awards for Best Foreign Language Film
* List of Czechoslovak submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 