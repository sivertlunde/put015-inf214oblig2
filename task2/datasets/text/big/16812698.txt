Colleen (film)
{{Infobox film
| name           = Colleen
| image          = Colleen1936movie.jpg
| caption        = Movie still Peter Milne F. Hugh Herbert Sig Herzig
| story          = Robert Lord
| starring       = Dick Powell Ruby Keeler Jack Oakie Joan Blondell
| director       = Alfred E. Green
| producer       =
| music          =
| cinematography = Byron Haskin Sol Polito
| editing        = Terry O. Morse
| distributor    = Warner Bros.
| released       =  
| runtime        = 89 minutes
| country        = United States English
| budget         =
|}}
Colleen is a 1936 Warner Bros. musical film directed by Alfred E. Green. It stars Dick Powell, Ruby Keeler, and Joan Blondell.

==Plot==
Colleen is the manager of a dress shop named "The Ames Company", owned by Donald Ames. They try to keep Uncle Cedric from working, because hell ruin the company. Troubles start when he hires schemer Joe as his personal assistant. He later also hires Minnie, a woman who has a great passion for fashion. When he buys the dress shop for Minnie where Colleen works as a bookkeeper, a scandal is soon followed. Donald decides to shut the shop, but is stopped because of his infatuation towards Colleen. It is Colleen who eventually makes a profit out of the things that happened. Meanwhile, a man named Cedric tries to adopt Minnie. Minnie refuses and thereby causes a scandal. This angers Alicia, but the press cant get enough of it. Donald loses Colleens affection and thus is sued by Joe.

==Cast==
* Dick Powell &ndash; Donald Ames 3rd
* Ruby Keeler &ndash; Colleen Reilly
* Jack Oakie &ndash; Joe Cork
* Joan Blondell &ndash; Minnie Hawkins
* Hugh Herbert &ndash; Uncle Cedric Ames
* Louise Fazenda &ndash; Aunt Alicia Ames Marie Wilson &ndash; Mabel 
* Hobart Cavanaugh &ndash; Noggin

==Soundtrack==
*Boulevardier from the Bronx
**Music by Harry Warren
**Lyrics by Al Dubin
**Sung and danced by Joan Blondell and Jack Oakie
*An Evening with You
**Music by Harry Warren
**Lyrics by Al Dubin
**Sung by Ruby Keeler and Dick Powell
*I Dont Have to Dream Again
**Music by Harry Warren
**Lyrics by Al Dubin
**Sung by Dick Powell and chorus and danced by chorus
**Sung as The Magic and the Mystery of Clothes by Ruby Keeler 
*Summer Night
**Music by Harry Warren
**Lyrics by Al Dubin
**Played on piano and hummed by Dick Powell
*You Gotta Know How to Dance
**Music by Harry Warren
**Lyrics by Al Dubin
**Played during the opening photo credits and sung with special lyrics by Dick Powell,  Marie Wilson Paul Draper and chorus Paul Draper and Ruby Keeler in the finale
*Bridal Chorus (Here Comes the Bride)
**Written by Richard Wagner
*I Love You Truly
**Written by Carrie Jacobs Bond

==External links==
* 

 

 
 
 
 
 
 
 