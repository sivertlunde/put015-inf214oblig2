Udayon
{{Infobox film
| name           = Udayon
| image          = Udayon.jpg
| caption        = 
| director       = Bhadran
| producer       = Subair
| writer         = Bhadran Laya  Innocent   Nassar   Salim Ghouse
| music          = Ouseppachan
| cinematography = Ramachandra Babu
| editing        = 
| distributor    = Varnnachithra
| released       = 2005
| runtime        = 180 minutes
| country        = India
| language       = Malayalam
| budget         = 
}} Malayalam film starring Mohanlal and directed by Bhadran.The film was a box office failure.
Jyothika was also offered to act as lead actress with Mohanlal in Udayon directed by Bhadran.but she couldnt do it due to her busy schedules in Tamil.  she was later replaced by Laya.

==Plot==
 Telugu actress Laya is the heroine in this film.  His beliefs on the land and agrarianism is good but his value system that farmers and farming is more important and better is not. He does not let his sons pursue there interest and wants them to do farming also. Which leads in they becoming negative towards father. Also when he tries to marry off his daughter to an ugly but farming loving another narrow minded character instead of an educated handsome doctor makes them feel that the father is destroying and spoiling their lives with his stubbornness and high handedness.

The presence of a villainous brother in law to his sisters son JCB (he drives a JCB played by Kalabhavan Mani) adds a twist as he interferes in the family property dispute. All said and done the sister doesnt want eveil to her brother (Mohanlal Sr) and her younger son the mute also doesnt want. Papoyi tries to save things but never has luck ultimately he tries and prevents many a bad scene but cannot prevent his youngest brother being killed and Kunju kills the villainous brother in law Perumal (Salim Ghouse) who does the evil deed and gives the family property keys to sister and the patriarch stick to Papoyi after the death of his youngest kid Ponnan.

== Cast ==
* Mohanlal	 ...	Shooranad Pappoyi / Shooranad Kunju
* Jagathy Sreekumar	 ...	Shavappetty Thoma
* Manoj K. Jayan	 ...	Pottan Laya ...	Maya Innocent	 ...	Rarichan
* Kalabhavan Mani	 ...	Mathan Siddique	 ...	Mammali
* Shammi Thilakan ... S.I Itti		
* Bheeman Raghu
* Mohan Jose ... Manthodi Joppan
* Sadiq	
* Kulappulli Leela .... Kuttiyamma
* Rajan P. Dev ... Chetti
* M. R. Gopakumar .... Chackochi	
* Idavela Babu	 ...	Itti
* Salim Ghouse	 ...	Perumal
* Nassar	 ...	Unni Vaidyan
* Bindu Panicker	 ...	Ichamma Rekha	 Sukanya	 ...	Susiemol
* Ambika Mohan ...   Rarichans wife
* V. K. Sreeraman .... Advocate Sreedharan 
* Kochu Preman  .... Kangaroo
* Vinayakan
* Valsala Menon Kalpana ... Kunjujamma
* Unnikrishnan Namboothiri ... Priest
* Kukku Parameshwaran

==Critical Reception==
The film had high expectations since it last film from the team Spadikam was attain a classic status. However Udayon was a total disappointment. The script gave too much importance in characterization and failed to focus on the story line. Unlike Spadikam, here the characters and story never had a parallel run. The result was nothing but a box office bomb.

==References==
 

==External links==
*  
* http://popcorn.oneindia.in/movie-synopsis/2294/udayon.html

 
 
 
 