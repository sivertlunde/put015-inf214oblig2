The Girl with the Dragon Tattoo (2009 film)
 
 
 
{{Infobox film
| name = The Girl with the Dragon Tattoo
| image = Men Who Hate Women.jpg
| alt = Lisbeth Salander with Mikael Blomkvist
| caption = Swedish release poster
| director = Niels Arden Oplev
| producer = Søren Stærmose
| screenplay = {{Plainlist|
* Nikolaj Arcel
* Rasmus Heisterberg}}
| based on =  
| starring = {{Plainlist|
* Michael Nyqvist
* Noomi Rapace}}
| music = Jacob Groth
| cinematography = {{Plainlist|
* Eric Kress
* Jens Fischer}}
| editing = Anne Østerud
| studio = {{Plainlist| Yellow Bird
* Music Box Films
* Alliance Films
* Lumiere
* GAGA}}
| distributor = Nordisk Film
| released =  
| runtime = 153 minutes  
| country = Sweden 
| language = Swedish
| budget = $13 million 
| gross = $104,395,170 
}} drama Thriller thriller film the novel of the same name by Swedish author/journalist Stieg Larsson. It is the first book in the trilogy known as the Millennium series|Millennium series, published in Sweden in 2005. By August 2009, it had been sold to 25 countries outside Scandinavia, most of them planning a release in 2010, and had been seen by more than 6 million people in the countries where it was already released. Directed by Niels Arden Oplev, the film stars Michael Nyqvist and Noomi Rapace.

==Plot== libel case involving allegations he published about billionaire financier Hans-Erik Wennerström. He is sentenced to three months in prison and a hefty fine. Lisbeth Salander, a brilliant but damaged surveillance agent and Hacker (computer security)|hacker, is hired by Henrik Vanger, the patriarch of the wealthy Vanger family, to investigate Blomkvist. Vanger then hires Blomkvist to investigate the disappearance of his niece, Harriet, who vanished on Childrens Day in 1966. Vanger believes that Harriet was murdered by a family member. 
 sexual sadist, forces Salander to perform fellatio on him in return for the money she needs to buy a new computer; he withholds the full amount she has requested. At her next meeting with Bjurman, he beats and rapes her. Having used a hidden camera to record Bjurman raping her, Salander returns to take her revenge, torturing him and threatening to ruin him unless he gives her full control of her life and finances. She then uses a tattoo gun to brand Bjurmans abdomen with the message "I am a sadist pig and a rapist".

Blomkvist moves into a cottage on the Vanger estate, and meets the Vanger family, including Harriets brother Martin and cousin Cecilia. Inside Harriets diary, he finds a list of five names alongside what might appear to be phone numbers. He visits police inspector Morell, who informs him that his investigation team had been unable to decipher them. After viewing photographs taken during the Childrens Day parade, Blomkvist sees Harriets facial expression change suddenly just before she leaves and, after getting photographs taken from the same side of the street as her, comes to believe that Harriet may have seen her murderer that day.
 Jewish names, which intrigues Blomkvist, as the Vanger family has a long history of antisemitism. During the investigation, Blomkvist and Salander become lovers.

They suspect Henriks reclusive brother Harald to be the murderer, as the two other Vanger brothers had already died by the time Harriet disappeared. Salander searches through Vangers business records to trace Harald to the crime scenes, while Blomkvist breaks into Haralds house, believing it to be unoccupied. When Harald attacks Blomkvist, Martin appears and saves him. He escorts Blomkvist to his home, where Blomkvist reveals what he and Salander have uncovered. Martin drugs him. In the meantime, Salanders search of the company accounts points to Martin and his late father, Gottfried, having been jointly responsible for the murders. She returns to the cottage to find Blomkvist missing.

Blomkvist wakes to find himself bound in Martins cellar. Martin brags about raping and murdering women for decades, but denies killing Harriet, insisting that she disappeared. As Martin is in the process of hanging Blomkvist, Salander appears and attacks Martin with a golf club. While she frees Blomkvist, Martin flees in his car. Salander gives chase on her motorcycle. Martin drives his car off the road and Salander finds him trapped in the wreckage but still alive. The car goes up in flames, and she does nothing to save him.

Blomkvist realizes that Cecilias late sister Anita was the near-double of Harriet, and that some of the film taken on the day of Harriets disappearance show Anita, not Harriet as previously thought. Blomkvist and Salander discover that Harriet has been using Anitas name, and is still alive in Australia. Blomkvist flies there to look for her, and persuades her to return to Sweden, where she is reunited with her uncle. Harriet explains the truth about her disappearance: that her father and her brother had repeatedly raped her; that she killed her father by drowning him only to find herself being blackmailed by Martin; and that her cousin Anita had smuggled her away from the island. 

Salanders mother, living in a nursing home, apologizes for not choosing a "better papa" for her. Salander then visits Blomkvist in prison and gives him new information on the Wennerström case. After his release Blomkvist publishes a new story on Wennerström in Millennium, which ruins Wennerström and makes the magazine a national sensation. Wennerström is soon found dead. His offshore bank account in the Cayman Islands is raided; the police suspect a young woman caught on Closed-circuit television|CCTV, whom Blomkvist recognizes as Salander in disguise. The film ends with Salander, dressed very smartly as she exits her car, walking along a sunny beach promenade.

==Cast==
 
* Michael Nyqvist as Mikael Blomkvist
* Noomi Rapace as Lisbeth Salander
* Lena Endre as Erika Berger
* Sven-Bertil Taube as Henrik Vanger
* Peter Haber as Martin Vanger Peter Andersson as Nils Bjurman
* Marika Lagercrantz as Cecilia Vanger
* Ingvar Hirdwall as Dirch Frode
* Björn Granath as Gustav Morell
* Ewa Fröling as Harriet Vanger
* Michalis Koutsogiannakis as Dragan Armansky
* Annika Hallin as Annika Giannini
* Tomas Köhler as "Plague"
* Gunnel Lindblom as Isabella Vanger
* Gösta Bredefeldt as Harald Vanger
* Stefan Sauk as Hans-Erik Wennerström
* Jacob Ericksson as Christer Malm
* Julia Sporre as young Harriet Vanger
* Tehilla Blad as young Lisbeth Salander
* Sofia Ledarp as Malin Eriksson
* David Dencik as Janne Dahlman
* Reuben Sallmander as Enrico Giannini
 

==Release==
===Critical response===
The Girl with the Dragon Tattoo was well received by critics. Review aggregation website   gives the film a weighted average score of 76% based on reviews from 36 critics. 
   Roger Ebert of the Chicago Sun-Times gave the film four out of four stars, noting that "  is a compelling thriller to begin with, but it adds the rare quality of having a heroine more fascinating than the story".   

===Box office===
The film grossed more than $10 million in North America in a limited release of 202 theatres.    The total gross worldwide is $104,617,430.  

===Awards and nominations===
{| class="wikitable"
|-
! Association
! Category
! Nominee
! Result
|-
| Amanda Award
| Best Foreign Feature Film
| Niels Arden Oplev
|  
|-
| rowspan="3"| BAFTA Award Best Actress in a Leading Role
| Noomi Rapace
|  
|- Best Adapted Screenplay
| Nikolaj Arcel and Rasmus Heisterberg
|  
|- Best Film Not in the English Language
|
|  
|-
| rowspan="2"| Broadcast Film Critics Association Award Best Actress
| Noomi Rapace
|  
|- Best Foreign Language Film
| Niels Arden Oplev
|  
|-
| rowspan="2"| Empire Awards 
| Best Thriller
|
|  
|-
| Best Actress
| Noomi Rapace
|  
|-
| rowspan="3"| European Film Awards
| Audience Award
| Niels Arden Oplev
|  
|- Best Actress
| Noomi Rapace
|  
|-
| Best Composer
| Jacob Groth
|  
|-
| rowspan="5"| Guldbagge Award
| Audience Award
| Niels Arden Oplev
|  
|- Best Actress
| Noomi Rapace
|  
|- Best Film
| Søren Stærmose
|  
|-
| Best Cinematography
| Eric Kress
|  
|- Best Supporting Actor
| Sven-Bertil Taube
|  
|- Houston Film Critics Society Award
| Best Foreign Language Film
|
|  
|-
| Best Actress in a Leading Role
| Noomi Rapace
|  
|- London Film Critics Circle Award
| Actress of the Year
| Noomi Rapace
|  
|- New York Film Critics Online Award
| Breakthrough Performer
| Noomi Rapace
|  
|-
| Palm Springs International Film Festival
| Audience Award for Best Narrative Feature
| Niels Arden Oplev
|  
|-
| rowspan="3"| Satellite Award Best Actress – Motion Picture Drama
| Noomi Rapace
|  
|- Best Foreign Language Film
|
|  
|- Best Adapted Screenplay
| Nikolaj Arcel and Rasmus Heisterberg
|  
|-
| rowspan="2"| St. Louis Gateway Film Critics Association Awards 2010|St. Louis Gateway Film Critics Association Award
| Best Actress
| Noomi Rapace
|  
|-
| Best Foreign Language Film
|
|  
|-
| Washington D.C. Area Film Critics Association Award
| Best Foreign Language Film
|
|  
|}

==TV mini-series== French premium extended versions 2011 film adaptation of the novel.

In France, the audience of the Canal+ broadcast of the first part on 22 March 2010 was 1.2 million (18% of the channels subscribers in the country) and the largest audience of a foreign series at Canal+ that year. 

A home video set of all six parts of the mini-series was released on DVD and Blu-ray Disc by Music Box Home Entertainment on 6 December 2011.

==Sequels== The Girl Who Played with Fire  The Girl Who Kicked the Hornets Nest

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 