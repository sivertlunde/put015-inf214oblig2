Muppet Treasure Island
 
{{Infobox film
| name = Muppet Treasure Island
| image = Muppettreasure.png
| alt = 
| caption = Theatrical release poster by Drew Struzan
| director = Brian Henson
| producer = {{Plainlist| 
* Martin G. Baker
* Brian Henson}}
| screenplay = {{Plainlist|
* James V. Hart
* Jerry Juhl
* Kirk R. Thatcher}}
| based on =  
| starring = {{Plainlist| 
* Kermit the Frog
* Miss Piggy
* The Great Gonzo
* Rizzo the Rat
* Tim Curry
* Dave Goelz
* Steve Whitmire
* Jerry Nelson
* Kevin Clash
* Frank Oz}}
| narrated by = Billy Connolly
| music = Hans Zimmer
| cinematography = John Fenner
| editing = Michael Jablow
| studio = {{Plainlist| Jim Henson Productions
* Walt Disney Studios Motion Pictures}} Buena Vista Pictures
| released =  
| runtime = 99 minutes 
| country = United States
| language = English
| budget = 
| gross = $34.3 million
}} musical Adventure adventure comedy film based on Robert Louis Stevensons Treasure Island. It is the fifth feature film to star The Muppets and was directed by Brian Henson.
 Muppet Christmas Carol, the key roles were played by live-action actors, with the Muppets in supporting roles. The live-action actors consisted of Tim Curry, Billy Connolly, Jennifer Saunders, and Kevin Bishop in his feature film debut. Kermit the Frog appeared as Captain Abraham Smollett, Fozzie Bear as Squire Trelawney, Sam Eagle as Mr. Samuel Arrow, and Miss Piggy as the gender-bent castaway "Benjamina" Gunn. Following their success as the narrators of The Muppet Christmas Carol, The Great Gonzo and Rizzo the Rat appeared in specially created roles as Jims best friends. The film was released on February 16, 1996, one month before Muppets Tonight premiered on American Broadcasting Company|ABC.

==Plot== Jim Hawkins Gonzo and Rizzo at the Admiral Benbow inn in England. Dreaming of sea voyages, Jim only has the tales of Billy Bones (to help, Bones telling Jim and the inn patrons of Captain Flint, his old captain, burying his treasure on a remote island and killing all of his crew, and that no one knows the whereabouts of the map. One night, as Jim, Gonzo, and Rizzo continue to dream of sea voyages, Bones fellow pirate Blind Pew arrives and gives Bones the black spot. Bones dies of a heart attack but reveals to Jim, Gonzo and Rizzo he had the map all along beforehand. Blind Pew returns with an army of pirates, but the boys escape with the map.

Going to a harbor town, the boys meet the half-wit son of Squire Trelawney who decides to fund a voyage to find Treasure Island and Flints fortune. Accompanied by Dr. Livesey and his assistant Beaker (Muppet)|Beaker, the boys and Trelawney hire the Hispaniola, commanded by Captain Abraham Smollett (Kermit the Frog) and his overly strict first mate Mr. Arrow. The boys meet the cook Long John Silver, a one-legged man who Bones warned the boys about before dying. The ship sets sail, but Smollett is concerned by the pirate-like crew, learning they were hired on Silvers suggestion. Jim and Silver bond, but Gonzo and Rizzo are captured by the pirates Polly Lobster, Mad Monty, and Clueless Morgan who demand they surrender the map; before Mr. Arrow catches them in the act and imprisons them in the brig. Smollett locks the map in his safe upon ordering Jim to give him the map for safekeeping.
 native pigs ruled over by Benjamina Gunn, Smolletts ex-fiancée who he left at the altar. Jim, Silver, and the pirates find the hiding place of Flints treasure only to find the treasure missing. Silver then sends Jim away as a fight breaks out among the pirates. The pirates come across Smollett and Benjamina. Smollett is then suspended from a cliff until Benjamina tells Silver the treasure is hidden in her home, but when Benjamina spits out a kiss from Silver, he leaves the two to dangle, allowing the pair to fall in love again.
 figureheads of the Hispaniola save Smollett and Benjamina. A battle breaks out between the heroes and the pirates where Sweetums defects to Smolletts side and takes out some of the pirates. Smollett then fights Silver in a sword battle but loses. Jim and the others rally to their captains aid, resulting in Silver surrendering honorably. All the pirates are stuffed into the brig, but Silver escapes using Mr. Arrows keys. Jim catches him in the act but ultimately allows Silver to leave for the sake of their previous friendship, though he renounces it henceforth, calling him "Mr. Silver" and concealing his emotion at their final farewell. However, Mr. Arrow informs Jim and Smollett that the longboat Silver took was unsafe, forcing Silver to abandon ship and swim to Treasure Island the next day. The crew of the Hispaniola sail off into the sunset while some scuba-diving rats discover the treasure.

During the credits, it is revealed that Silver is eating a banana, marooned on the island with only a wisecracking Moai head for company as the Moai tells him a bad joke.

==Cast== Jim Hawkins, a good-natured orphan boy who for most of his life has worked at Mrs. Bluberidges inn, but has always dreamed of nautical adventures. He is an incredibly trusting boy, which proves to be somewhat of a downfall for him, as he forms a bond with the ships chef Long John Silver, who is ultimately revealed to be a pirate.
* Tim Curry as Long John Silver, a deceptively charming pirate, posing as a chef, who befriends Jim Hawkins at first, until he is overheard by Gonzo, Rizzo, and Jim as he reveals his dastardly plans to his fellow pirates aboard the Hispaniola. During Silvers siege on Treasure Island, it is suggested that Long John Silver and Benjamina Gunn share a romantic history. Despite his villainous nature, he genuinely cares about Jim.
* Jennifer Saunders as Mrs. Bluberidge, a loud, plump woman who owns the Admiral Benbow Inn where Jim and his friends work. She has an uncanny ability to know when people are not doing what they should be doing, which leads to various characters exclaiming, "How does she do that?!" Though rough with the "boys," she does show a genuine concern for Jim, helping him escape the pirates before seeing them off herself.
* Billy Connolly as Billy Bones,  an ex-pirate, previously a member of Captain Flints crew who witnessed the burial of gold on Treasure Island and tells Jim he still has the map to the treasure before he suffers a fatal heart attack. During a 2002 live performance in Dublin, Connolly jokingly claimed to be the only man to ever die in a Muppet movie.  In fact, in the film, Rizzo points this out: "He died? And this is supposed to be a kids movie!"
* David Nicholls as Captain Flint, a pirate who buried the treasure at his own island years ago, and killed his crew to prevent them from telling where the treasure was buried. He also marooned Benjamina Gunn on Treasure Island. His first name is revealed to be Burney.
* Frederick Warder as Calico Jerry. One of the human pirates of Long John Silvers crew, he was the first aboard the Hispaniola to come down with the Cabin Fever. He is later defeated by Jim Hawkins.
* Harry Jones as Easy Pete, another member of Silvers Crew. He is defeated easily by Benjamina Gunn.
* Peter Geeves as Black Eyed Pea, a pirate with two black eyes. Another of the human pirates and members of Long John Silvers Crew. He is introduced during the roll call and sings a solo in "Professional Pirate".  He too is defeated by Benjamina via a swift kick between the legs. dwarf pirate and the smallest human pirate on board the Hispaniola. Like Easy Pete and Black Eyed Pea, he is defeated by Benjamina in the fight sequence with a kiss and a head butt.
* Jessica Hamilton as Big-Fat-Ugly-Bug-Faced-Baby-Eating OBrien, a beautiful woman pirate with a deep manly voice. Her name does not at all match that of her appearance: the horrible name of the beautiful lady was a comic parallel to the lovely name of the hideous monster Angel Marie, who was next on the roll call. The character is shown only once and does not appear again.

Five other unnamed human pirates with no speaking lines appear aboard the Hispaniola while the other pirates go with Long John Silver. The beginning of the film also features several unnamed human pirates who were members of Flints crew during the credit sequence of "Shiver My Timbers". Other unnamed human pirates are members of Blind Pews at the Admiral Benbow Inn. There were also other unnamed non-pirate human characters who appeared both at the Admiral Benbow inn and in the town of Bristol.

===Muppet performers===
 
 
* Steve Whitmire as:
** Kermit the Frog as Captain Abraham Smollett
** Rizzo the Rat as Himself
** Beaker as Himself
** Jacques Roach
** Walleye Pike
** Murray the Minstrel from Fraggle Rock
* Frank Oz as:
** Fozzie Bear as Squire Trelawney
** Miss Piggy as Benjamina Gunn
** Sam the Eagle as Mr. Samuel Arrow, first mate
* Dave Goelz as:
** The Great Gonzo as Himself
** Bunsen Honeydew|Dr. Bunsen Honeydew as Dr. David Livesey Waldorf as Figurehead
* Jerry Nelson as: Statler as Figurehead
** Lew Zealand Blind Pew
** Mad Monty
** Old Joe
** Calico
** Old Tom
** Spotted Dick
* Kevin Clash as:
** Polly Lobster
** Spaam
** Real Old Tom
** Black Dog
* Bill Barretta as:
** Clueless Morgan
** Mudwell the Mudbunny from Fraggle Rock
** Angel Marie
** Mr. Bitte John Henson as Sweetums
* Louise Gold as Tourist Rat, Brool the Minstrel from Fraggle Rock
* Don Austen as Background Pirates, Native Pigs
* David Rudman as Wander McMooch from Fraggle Rock
* Geoff Felix as Begoony from Fraggle Rock
 
 dubbed the voices in post-production. Oz had already participated in a recorded read-through of the script; Clash used these recordings to help prompt his performances. According to Kevin Clash, Frank Oz gave him a brief description of each of his characters prior to shooting. Oz described Miss Piggy as "a truck driver wanting to be a woman", and Fozzie Bear as somebody similar to Jerry Lewis.

==Release==
===Box office===
The movie debuted at No.2.

===Critical reception===
Muppet Treasure Island received generally positive reviews; Rotten Tomatoes reported that 70% of critics gave the film positive reviews.

===Home media=== Walt Disney Region 1. Before then, Jim Henson Video released a Muppet Sing Alongs VHS tape entitled Muppet Treasure Island to coincide with the films theatrical release. The first DVD release in the U.S. on June 4, 2002 was in a fullscreen-only format. Other releases of these were in widescreen only format. The DVD release has 3 bonus features added like "Hidden Treasure Commentary", "The Tale of the Story Behind the Tail" and "Treasure Island Sing-Along" (but the menus were in widescreen format).

===Video game===
A video game based on the film was released for Windows and Mac OS in 1996 by Activision.  PC Zone gave the game 80 out 100 in a review. 

==Music==
{{Infobox album   Name = The Muppet Treasure Island: Original Motion Picture Soundtrack Type = soundtrack Artist = The Muppets Cover =  Released = October 29, 1996 Genre = Soundtrack
|Label = Angel Records Reviews = 
| Chronology  = The Muppets
| Last album =   (1992)
| This album = Muppet Treasure Island: Original Soundtrack (1996)
| Next album =   (1999)
}}
{{Album ratings rev1 = Allmusic rev1score =   
}} 
The Muppet Treasure Island: Original Motion Picture Soundtrack features an instrumental score by Hans Zimmer, with additional music by Harry Gregson-Williams, as well as songs written by pop songwriters Barry Mann and Cynthia Weil. The films ending includes the reggae number "Love Power" performed by Ziggy Marley, which was released as a single and promoted with a music video featuring Marley and some Muppets with dreadlocks.
;Track listing
# "Treasure Island" John Henson)
# "Something Better" (Kevin Bishop, Dave Goelz, and Steve Whitmire)
# "Sailing for Adventure" (Tim Curry, Kevin Bishop, Nelson, Goelz, Whitmire, Brian Henson, Frank Oz, Kevin Clash, Bill Barretta, and John Henson)
# "Cabin Fever" (Rob Paulsen, Jerry Nelson, Jess Harnell, Dave Goelz, Steve Whitmire, Brian Henson, Frank Oz, Kevin Clash, Bill Barretta, and John Henson with Louise Gold)
# "Professional Pirate" (Tim Curry, Jerry Nelson, Goelz, Whitmire, Brian Henson, Rob Paulsen, Kevin Clash, Bill Barretta, and John Henson)
# "Boom Shakalaka" (Jerry Nelson, Dave Goelz, Steve Whitmire, Brian Henson, Jess Harnell, Kevin Clash, Bill Barretta, and John Henson)
# "Love Led Us Here" (Frank Oz, Steve Whitmire)
# "Map"
# "Captain Smollett"
# "Land Ho"
# "Compass"
# "Long John"
# "Rescue"
# "Honest Brave and True" Melody Makers) John Berry, Helen Darling)

Note: During the song "Cabin Fever", the upbeat music plays a tiny portion of the Toccata and Fugue in D Minor when Real Old Tom comes out of the black coffin with purple silk inside.

;Score cues left off the soundtrack
 
# Isnt That a Story Worth the Hearin?
# Lighting the Lamp (segues into "Something Better")
# Blind Pew
# The Hispaniola
# Many a Dark-Hearted Scoundrel/One-Legged Man
# This Voyage Has Begun (segues into "Sailing for Adventure") Electric Mayhem Band)
# Waiting to Pounce
# Give Me the Map/No Wind
# Mutiny
# Never Get Involved in Politics (Electric Mayhem Band)
# "A Professional Pirate" (Full version, with instrumental interlude)
# Benjamina Gunn
# Treasure Hunting
# You Dare to Give ME the Black Spot?
# Return of Mr. Arrow
# The Pirates Attack/Sneaking Aboard/Where’s the Bloody Treasure?!
# Long John Escapes
# Sailing for Adventure (Finale)

==Lawsuit==
The Hormel Foods Corporation (who are the creators of Spam (food)|Spam) sued Jim Henson Productions for using the name "Spaam" for one of the films tribal pig characters.  Their suit was defeated on September 22, 1995. The judge dismissed it after a trial for failure to prove damages, noting, "one might think Hormel would welcome the association with a genuine source of pork."  When Spaam later appeared as a racing boss in Muppet RaceMania, he was credited as "Pig Chief."

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 