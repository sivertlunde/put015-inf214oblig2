Away We Go
 
{{Infobox film
| name           = Away We Go
| image          = Away we go poster.jpg
| caption        = Theatrical film poster
| director       = Sam Mendes
| producer       = Edward Saxon Marc Turtletaub Vincent Landay
| writer         = Dave Eggers Vendela Vida Josh Hamilton Chris Messina Paul Schneider
| music          = Alexi Murdoch
| cinematography = Ellen Kuras
| editing        = Sarah Flack Big Beach Neal Street Productions
| distributor    = Focus Features
| released       =  
| runtime        = 98 minutes 
| country        = United States
| language       = English
| budget         = $17 million   
| gross          = $14,899,417 
}} Paul Schneider, Chris Messina, Josh Hamilton, Jim Gaffigan, and Maggie Gyllenhaal.

It had a limited theater release in the United States starting June 5, 2009. It opened the 2009 Edinburgh International Film Festival in Edinburgh, Scotland.   The film was released on DVD and Blu-ray Disc on September 29, 2009.

==Plot==

Verona De Tessant (Maya Rudolph) and Burt Farlander (John Krasinski) are in their early thirties and struggling to meet daily needs and build fulfilling lives. When they learn they will soon become parents, they are confronted with the challenge of how – and where – to raise a child and build a happy family.

Six months into Veronas pregnancy, she and Burt visit their only family in the area, Burts parents, Gloria and Jerry (Catherine OHara and Jeff Daniels), only to find that they have decided to move to Antwerp, Belgium, for two years a month before the baby is due to be born. Frustrated at Burts parents selfishness and ill-thinking, he and Verona see this as an opportunity to find somewhere else to raise their family.

They first visit Phoenix, Arizona, meeting up with Veronas old boss, Lily (Allison Janney), her husband, Lowell (Jim Gaffigan), and their two children. Burt in particular is disturbed by Lily and Lowells crass and mean-spirited behavior toward one another and their children.

Burt and Verona next visit Veronas sister, Grace (Carmen Ejogo), in Tucson, Arizona. At Veronas request, Burt tries to persuade Grace to stay with her boring boyfriend. When Burt takes a call and displays his trademark humor, Grace tells Verona that she is lucky to have him and Verona agrees.
 Josh Hamilton) continuum home." When Rodericks condescension and LNs backhanded compliments to Verona get to be too much for Burt, he tells them they are horrible people and he and Verona leave but not before letting their kid take a ride in the stroller, which he enjoys.
 Chris Messina) and his wife, Munch Garnett (Melanie Lynskey), and their diverse family of adopted children. Verona and Burt are happy to have found a loving family and a nice town and decide to move to Montreal. After dinner, Tom confesses to Burt that Munch has recently suffered her fifth miscarriage and that they seem unable to have biological children.
 Paul Schneider), in Miami, whose wife has left him. Burt and Verona fly to Miami, where Courtney worries about his young daughter and the potential effects of a divorce on her. Burt tries to comfort Courtney while Verona spends time with his daughter. Burt and Verona spend the night outside on a trampoline, promising to love each other and their daughter and have a happy home.

The next day, Verona tells Burt a story about her childhood and her now deceased parents. Moved by her memory, they decide to settle in Veronas old family home. Realizing it is the place for them, they sit together happily, overlooking the water.

==Cast==
*John Krasinski as Burt Farlander
*Maya Rudolph as Verona De Tessant
*Carmen Ejogo as Grace De Tessant
*Jeff Daniels as Jerry Farlander
*Catherine OHara as Gloria Farlander
*Allison Janney as Lily
*Jim Gaffigan as Lowell
*Maggie Gyllenhaal as LN Josh Hamilton as Roderick
*Melanie Lynskey as Munch Garnett Chris Messina as Tom Garnett Paul Schneider as Courtney Farlander

==Critical reviews==
The film received a positive rating of 67% based on 173 film critics at the aggregate site Rotten Tomatoes.  A.O. Scott of The New York Times described the two main characters as self-righteous people "aware of their special status as uniquely sensitive, caring, smart and cool beings on a planet full of cretins and failures".    Metacritic reported the film had an average score of 58 out of 100, based on 33 reviews. 

==Soundtrack==
The soundtrack of Away We Go was released on June 2, 2009, and primarily features songs from Scottish singer/songwriter Alexi Murdoch.

(All songs by Alexi Murdoch except where noted)
# "All My Days" (4:57) Orange Sky" (6:18)
# "Blue Mind" (5:45)
# "Song for You" (4:38)
# "Breathe" (4:18)
# "Towards the Sun" (4:40)
# "Meet Me in the Morning" by Bob Dylan (4:21)
# "What Is Life" by George Harrison (4:24)
# "Golden Brown" by The Stranglers (3:30)
# "Wait" (5:59)
# "Oh! Sweet Nuthin" by The Velvet Underground (7:28)
# "The Ragged Sea" (3:19)
# "Crinan Wood" (5:45)
"All My Days" was featured in the films trailer.

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 