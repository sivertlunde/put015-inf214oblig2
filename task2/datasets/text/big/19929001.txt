La Loi du Nord
{{Infobox film
| name           = La Loi du nord
| image          = La Loi du nord film poster.jpg
| caption        = Theatrical release poster
| director       = Jacques Feyder
| producer       = Roland Tual
| writer         = Jacques Feyder Alexandre Arnoux  Charles Spaak  Maurice Constantin-Weyer (novel)
| starring       = Michèle Morgan  Pierre Richard-Willm  Charles Vanel
| music          = Louis Beydts
| cinematography = Jean Charpentier  
Paul Fabian  Roger Hubert
| editing        = Roger Spiri-Mercaton
| studio         = Filmos
| distributor    = DisCina
| released       = 1939
| runtime        = 95 minutes
| country        = France
| language       = French
| budget         =
| gross          =
}}
La Loi du nord is a 1939 French adventure drama film directed by Jacques Feyder who co-wrote screenplay with Alexandre Arnoux and Charles Spaak, based on novel "Telle quelle était de son vivant" by Maurice Constantin-Weyer. The films stars Michèle Morgan, Pierre Richard-Willm and Charles Vanel. It tells the story of an escaped prisoner, his woman secretary and two guardsmen in the Far North. It competed for the Golden Palm at the Cannes Film Festival. 

==Plot== trapper who takes them for film-makers, they hide in Northern Canada. But the corporal Dalrymple discovers their identity and hunts them, until Jacqueline dies exhausted by such a hard expedition. 

==Cast==
*Michèle Morgan as  Jacqueline
*Pierre Richard-Willm as  Robert Shaw
*Charles Vanel as  corporal Dalrymple
*Max Michel as  the advocat
*Youcca Troubetzkov as  Ellis
*Fabien Loris as  Daugh

==References==
 

==External links==
* 
* 
* 
*  at DvdToile

 

 
 
 
 
 
 

 