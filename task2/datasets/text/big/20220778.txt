The Thieving Hand
{{Infobox film
| name           = The Thieving Hand
| image          = The Thieving Hand.webm
| writer         =
| starring       = Paul Panzer
| director       = J. Stuart Blackton
| producer       = J. Stuart Blackton
| distributor    = Vitagraph Studios
| released       =   frames per second)
| language       = Silent film
| country        = United States
| awards         =
| budget         =
}}
 American silent silent short directed and produced by J. Stuart Blackton of Vitagraph Studios.

The film was shot on location in Flatbush, Brooklyn, New York City and released on February 1, 1908 in the United States.

It is one of the 50 films in the 4-disc boxed DVD set called Treasures from American Film Archives (2000), compiled by the National Film Preservation Foundation from 18 American film archives. The films print is preserved by the George Eastman House.   

In November 2008, it was shown in Leeds Film Festival, as part of Back to the Electric Palace, with live music by Sasha Siem, performed in partnership with Opera North.

==Plot==
A one-armed beggar receives a new limb from a strange store that specializes in human appendages. The new arm has a mind of its own, though, and brings the man nothing but trouble.

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 


 