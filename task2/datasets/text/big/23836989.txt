Kismat (2004 film)
{{Infobox film
  | name     = Kismat 
  | image    = Kismatposter_Ver1.jpg
  | caption  = Movie poster for  Kismat 
  | director = Guddu Dhanoa		
  | producer = Dhirajlal Shah Hasmukh Shah Pravin Shah
  | writer   = Robin Bhatt
  | starring = Bobby Deol Priyanka Chopra Kabir Bedi
  | music    = Anand Raj Anand  
  | released =  
  | runtime  = 143 min
  | language = Hindi
  | country  = India
  | budget   = 
  | Box Office =
  | distributor = Time Magnetics
}}  action drama Payback (1999). It was released throughout India and international territories in late 2004.

==Plot==
Tony (Bobby Deol), works as a hit-man for gangster, Vikas Patil (Ashish Vidyarthi), who owes his allegiance to wealthy Raj Mallya (Kabir Bedi). Raj Mallya is involved in marketing spurious and out-dated drugs, and as a result is the subject of an investigation by the Food & Drugs Administrations inspector Dr. Hargobind Gosai (Mohan Joshi). Raj asks Vikas to take care of Hargobind through Tony, which Tony does, and in this manner, Raj is absolved of all wrongdoing. Then Tony meets with attractive starlet named, Sapna (Priyanka Chopra), and falls head over heels in love with her. When Sapna tells him that she is engaged to be married to Dr. Ajay Saxena, he is heart-broken. Then a scandal breaks out, and Hargobind is implicated in the deaths of three children that were killed by Rajs spurious drugs. All the evidence points against Hargobind, and not a single lawyer is willing to take his case; his wife kills herself, and the marriage of his daughter has been canceled. Then Tony finds out that Hargobind is none other than Sapnas dad – and he has ruined the only chance he had for marrying the girl of his dreams.

==Cast==
*Bobby Deol as Tony
*Priyanka Chopra as Sapna Gosai
*Kabir Bedi as Raj Mallya
*Sanjay Narvekar as Goli (Tonis Friend)
*Mohan Joshi as Dr. Hargobind Gosai
*Shahbaaz Khan as Raj Mallyas son
*Smita Jaykar as Mrs. Gosai
*Ashish Vidyarthi as Vikas Patil
*Mushtaq Khan as Pankaj Bhai (Sapnas secretary)
*Veerendra Saxena as Sinha (Mallyas secretary)

==Soundtrack==
The music is composed by Anand Raj Anand and the lyrics are penned by Dev Kohli. There are nine tracks in the album, including three instrumentals.  

{{Track listing
| extra_column = Performer(s)
| title1 = Mahi Mahi Mahi | extra1 = Sunidhi Chauhan
| title2 = Sajna Se Milne Jaana | extra2 = Sunidhi Chauhan Richa Sharma
| title4 = Chitti Dudh Kudi  | extra4 = Shaan (singer)|Shaan, Gayatri Iyer
| title5 = Hum Hain Mast Maula | extra5 = Abhijeet Bhattacharya, Alka Yagnik
| title6 = Bicchi Padi Hai | extra6 = Anand Raj Anand
| title7 = Mahi Mahi Mahi | extra7 = Instrumental
| title8 = Sajna Se Milna Jaana | extra8 = Instrumental
| title9 = Dil Teri Deewangi Mein | extra9 = Instrumental
}}

==References==
 

==External links==
*  

 
 
 
 


 