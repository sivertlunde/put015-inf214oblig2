Each to His Kind
{{Infobox film
| name           = Each to His Kind
| image          = 
| alt            = 
| caption        =
| director       = Edward LeSaint
| producer       = Jesse L. Lasky
| screenplay     = George DuBois Proctor Paul West 
| starring       = Sessue Hayakawa Tsuru Aoki Vola Vale Ernest Joy Eugene Pallette Guy Oliver
| music          = 
| cinematography = Allen M. Davey 	
| editing        = 
| studio         = Jesse L. Lasky Feature Play Company
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 drama silent film directed by Edward LeSaint and written by George DuBois Proctor and Paul West. The film stars Sessue Hayakawa, Tsuru Aoki, Vola Vale, Ernest Joy, Eugene Pallette and Guy Oliver. The film was released on February 5, 1917, by Paramount Pictures.  

==Plot==
 

== Cast ==
*Sessue Hayakawa as	Rhandah
*Tsuru Aoki as Princess Nada
*Vola Vale as Amy Dawe
*Ernest Joy as Col. Marcy
*Eugene Pallette as Dick Larimer
*Guy Oliver	as Col. Dawe Walter Long as Mulai Singh
*Paul Weigel as Asa Judd
*Cecil Holland as The Maharajah

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 

 