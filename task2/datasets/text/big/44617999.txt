The Hard Easy (film)
{{Infobox film
| name = The Hard Easy
| image = The Hard Easy (film) poster.jpg
| caption = Official film poster
| director = Ari Ryan
| producer = Scott M. Gold Daniel Levin
| screenplay = Jon Lindstrom Tom Schanley
| starring = Henry Thomas Vera Farmiga David Boreanaz Bruce Dern Peter Weller Gary Busey
| music = Keith Power
| cinematography = Robert Morris
| editing = Monica Anderson Gary Trentham
| studio = Blue Star Pictures Platform Entertainment Twin Pines Entertainment Home Box Office (HBO) Lightning Entertainment
| released =  
| runtime = 105 minutes
| country = United States English
| budget = 
| gross = 
}} crime action film|action-drama film directed by Ari Ryan, written by Jon Lindstrom and Tom Schanley, and starring Henry Thomas, David Boreanaz, Vera Farmiga, Bruce Dern, Peter Weller, Nick Lachey and Gary Busey. The film was released straight-to-DVD in the United States on May 22, 2007. 

==Plot==
In Los Angeles, the gambler Paul Weston (Henry Thomas) is a loser, and owes a lot of money to the dangerous mobster Freddie (Rae Allen). When he is brutally beaten and the life of his former wife is threatened by Freddie, his acquaintance bartender Vinnie (Gary Busey) sends him to Dr. Charlie Brooks (Vera Farmiga), and then convinces him to participate in a heist in the jewelry district, with the professional thief Gene (Bruce Dern), to raise the necessary money to pay off his debt. 

Meanwhile, the stock market broker Roger Hargitay (David Boreanaz) finds that the company where he works embezzled $5 million from their clients and he will be arrested in a couple of days. His boss Ed Koster (Peter Weller) convinces Roger and two co-workers to participate in an easy robbery of jewelry planned by an inside man. On Saturday morning, both gangs arrive in the same jewelry district for the same heist at the same time. 

==Cast==
* Henry Thomas as Paul Weston
* David Boreanaz as Roger Hargitay
* Vera Farmiga as Dr. Charlie Brooks
* Bruce Dern as Gene
* Peter Weller as Ed Koster
* Gary Busey as Vinnie
* Nick Lachey as Jason Burns
* Elimu Nelson as Stephen McKinley
* Rae Allen as Freddie

==Production==

===Casting===
On September 17, 2004, it was reported that David Boreanaz, Henry Thomas, Vera Farmiga, and Bruce Dern had been cast in the film as Roger Hargitay, Paul Weston, Dr. Charlie Brooks, and Gene, respectively.   On September 23, 2004, it was announced that Nick Lachey had joined the cast and would be making his film debut in the feature, portraying a character named Jason Burns. 

===Filming===
Principal photography took place in Los Angeles and Saugus, Santa Clarita, California in September 2004.   The Hard Easy was released direct-to-video|straight-to-DVD in the United States on May 22, 2007.  It was released to Hungarian theaters on November 4, 2006.

==Critical response==
The film received mixed reviews from film critics. Jeff Swindoll from   but it was a nice little crime-thriller that benefits from the performances with kudos going toward the older members of the cast, but the young bucks arent too bad either. The tale is full of some interesting twists and comes off well in the end."  Rachel Buccicone of 7M Pictures wrote: "The Hard Easy is actually a very good story and reminds me in some ways of films like Snatch (film)|Snatch and Lock, Stock and Two Smoking Barrels. There is not one dull moment from start to the very twist-filled end. Additionally, the robbery scene is shown repeatedly from a few different perspectives, thus breaking traditional linear narrative form." 

Ben Odgren of CHUD.com gave a negative review, writing: "The Hard Easy tries to incorporate every possible cliché genre staple that comes standard with the type of films its trying to emulate. When a new character is introduced, the image freezes for a few seconds, changes color, a name pops up, and then resumes. At one point in the film, a character actually says, Are you in or out? Come on…theres a point where loyalty to genre standards needs to be thrown out the window and at least tries to bring something new to the table." 

==External links==
*  
*  
*  

==References==
 

 
 
 
 
 
 
 
 
 
 
 