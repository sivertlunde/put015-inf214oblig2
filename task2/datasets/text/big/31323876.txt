Reportage 57
{{Infobox film
| name           =Reportage 57
| image          = Reportage 57.jpg
| image size     =
| caption        =
| director       =János Veiczi
| producer       =
| writer         = Lothar Dutombé, János Veiczi
| narrator       =
| starring       =
| music          = 
| cinematography = 
| editing        =
| distributor    = 
| released       = 1959
| runtime        =
| country        = East Germany German
| budget         =
| gross          =
| preceded by    =
| followed by    =
}} East German film, released in List of East German films|1959. It was directed by János Veiczi and stars Annekathrin Bürger, Willi Schrade and Gerhard Bienert.
The film is said to have "drew on negative depictions of Halbstarke and rock n roll in West Berlin in its critiques of the West."   

==Cast==
*Annekathrin Bürger as Inge
*Willi Schrade as Heinz
*Gerhard Bienert as Vater Kramer
*Wilhelm Koch-Hooge as Lowinsky
*Edwin Marian as Leimtüte
*Paul Berndt as Godelmann
*Manfred Krug as Biene
*Habbo Lolling as Ede
*Christina Monden as Hedi
*Gert Andreae as Boy
*Werner Lierck as Paul

==References==
 

==External links==
*  

 
 
 
 


 