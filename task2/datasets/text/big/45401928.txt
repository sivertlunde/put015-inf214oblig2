Marmorera (film)
{{Infobox film name = Marmorera  image =  caption = director = Markus Fischer	 producer = Jörg Bundschuh Markus Fischer Josefa Haas writer = Markus Fischer Dominik Bernet starring = Eva Dewaele Anatole Taubman Corin Curschellas Mavie Hörbiger Ursina Lardi Mathias Gnädinger  music = Peter Scherer cinematography = Jörg Schmidt-Reitwein editing = Bernhard Lehner production companies = Snakefilm GmbH, Kick Film GmbH, Schweizer Radio und Fernsehen SRF, ARTE     distributor = Rialto Film AG  released = 25 January 2007    runtime = 95 min language = German Swiss Rumantsch
|budget =
}} 2007 Switzerland|Swiss film. It tells the fictitious story of the small Alpine village Marmorera, the identity of its inhabitants and a mysterious women. The mystery film was filmed and produced in Switzerland.

== Synopsis ==
In the early 1950s wide parts of the Alpine village of Marmorera and the surrounding valley were flooded in favour of a dam project. In the present days, an unknown, confused young woman is rescued from the Lai da Marmorera, and soon becomes a puzzling figure of much concern for psychiatrists of the Burghölzli sanatorium in Zürich-Weinegg. Simon Cavegn was born in Marmorera, treats the young woman who he calls Julia and starts to discover the dark secret of Marmorera.    Marmorera is a ghost story about the mystery of identity of individuals and even a village that disappeared beneath the waters of a reservoir. The identity of a young woman without name, origin and language skills, and the identity of a young psychiatrist who takes care of this young woman and gradually realizes that his own identity bases on the soil of this mysterious lake, and the identity of the growing number of dead people, all taken from their home village Marmorera.

== Plot (excerpt) ==
In the Marmorera reservoir, a mysterious young woman is found without identity and language; at the same time, a fishermen disappears on the Lai da Marmorera. The young red-haired woman is taken to the Burghölzli psychiatric clinic in Zürich where Simon Cavegn becomes her psychiatrist in charge. Cavegn was born in Marmorera, and soon suspects that this patient, which he calls Julia, will be much more than another medical problem to solve. Is Julia maybe really dangerous, or merely an interesting example of an extraordinary mind, or actually a curse from a bygone era, which the residents of Marmorera carefully tried to hide? For Simons wife Paula, Julia is soon a dangerous rival, although the unusual patient is always quiet and modest – as long she is allowed to use a bathtub whenever she wants ...

In the meantime, at the Marmorera village that was re-erected at the road to the Julier Pass in 1954, bizarre deaths accumulate, and Simon finds increasingly clear connections between these "accidents", his patient and the flooded mountain village at the base of the dam. In Marmorera, myth and reality meet, and the remaining few inhabitants are convinced that the rumors are not just fairy tales. The inscription "Resistance" at the facade of a house in the new Marmorera village remembers that not all residents agreed to sell their village and their country to the Zürich electricity company before the dam was built ... Cavegn tries to prevent further casualties, but fails.

== Cast ==
* Eva Dewaele : Julia  
* Anatole Taubman : Simon Cavegn 
* Corin Curschellas : Rosa Pellegrina
* Mavie Hörbiger : Paula Cavegn 
* Ursina Lardi : Dr. Alexandra Kovach 
* Mathias Gnädinger : Gregor Sonderegger

== Awards ==
* 2007: Festival del film Locarno, Prix SUISA for the best film music.    Festival de Cine Fantástico de Málaga, awarded as "Best Film". 
* 2007: Festival de Cine Fantástico de Málaga, awarded as "Best Cinematographie" 2007. 

== Home media ==
The film was released under the title Marmorera - Der Fluch der Nixe in the DVD format (Regional lockout|RC2) on 15 May 2008. The home release included language versions in German and Swiss German in Dolby Digital 5.1, and subtitles in English, French and German.    The soundtrack by Peter Scherrer is available as download.   

== Critical response ==
The German movie website film.de claims a moody and visually stunning horror thriller from Switzerland.   

== Background ==
Zürich needed electrical power, and so unceremoniously the village Marmomera sunk in a dam reservoir in 1954 – despite of the resistance of the population. Theo village was rebuilt and its inhabitants resettled. Based on the historical facts, the fictitious mystery film was produced at the original locations.  The script was published by Dominik Bernet as a paperback on 1 March 2008.   

== Literature ==
* Dominik Bernet: Marmorera. Piper Verlag GmbH, München 2008, ISBN 978-3-4922-5143-3.

== References ==
 

== External links ==
*    
* 

 
 
 
 
 
 
 
 
 
 