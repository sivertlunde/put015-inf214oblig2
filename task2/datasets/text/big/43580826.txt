The Woman on the Index
{{infobox_film
| title          = The Woman on the Index
| image          = The Woman on the Index.jpg
| imagesize      =
| caption        = lobby poster
| director       = Hobart Henley
| producer       = Samuel Goldwyn
| writer         = Lillian Trimble Bradley (play) George Broadhurst (play)
| cinematography = Edward Gheller
| editing        =
| distributor    = Goldwyn Pictures
| released       = February 23, 1919
| runtime        = 50 minutes
| country        = United States
| language       = Silent (English intertitles)
}} lost  Julia Dean.     

==Plot==
As described in a film magazine,  Sylvia Martins (Frederick) past is that of despair. Turned out of an unhappy home, she becomes the wife of a handsome and manly type of crook. However, before the marriage can be Consummation|consummated, he kills himself to avoid arrest. Sylvia is put on trial for murder and acquitted, but her name is recorded in a police index that falls into the hands of Hugo Declasse (Mack), an astute agent of the Bolsheviks|Bolsheviki. He pursues the wife, but she is also compelled to lend herself to the schemes of a secret service officer. She through her cleverness obtains documents held in the rooms of Declasse. In the end, she is returned to the arms of a forgiving and adoring husband.

==Cast==
*Pauline Frederick - Sylvia Martin
*Wyndham Standing - David Maber
*Willard Mack - Hugo Declasse
*Ben Hendricks Sr. - John Alden (*as Ben Hendricks)
*Jere Austin - Louis Gordon
*Louis Stern - John Martin
*Francis Joyner - Butler (as Frank Joyner) 
*Florence Ashbrooke - Mother Fralonz
*Florida Kingsley - Mrs. Martin

==References==
 

==External links==
 
* 
* 
*  (University of Washington, Sayre collection)

 
 
 
 
 
 
 