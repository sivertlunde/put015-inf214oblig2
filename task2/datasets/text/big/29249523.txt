Les Compagnes de la nuit
{{Infobox film
| name           = Les Compagnes de la nuit
| image          = 
| caption        = 
| director       = Ralph Habib
| producer       = Films Metzger et Woog
| writer         = Paul Andréota Jacques Companéez
| starring       = Françoise Arnoul Louis de Funès Marthe Mercadier
| music          = Raymond Legrand
| cinematography = 
| editing        = 
| distributor    = Les Films Corona
| released       = 12 July 1953 (France)
| runtime        = 90 minutes
| country        = France
| awards         = 
| language       = French
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 French Drama drama film from 1953, directed by Ralph Habib, written by Paul Andréota, starring Françoise Arnoul and Louis de Funès. 

== Cast ==
* Françoise Arnoul : Olga Viterbo
* Raymond Pellegrin : Jo Verdier
* Nicole Maurey :  Yvonne Leriche
* Noël Roquevert : the Smiling
* Marthe Mercadier : Ginette Bachelet
* Louis de Funès : a client
* Pierre Cressoy : Paul Gamelan
* Suzy Prim : Pierrette
* Jane Marken : Mrs Anita
* Christian Fourcade : Jackie Viterbo
* André Valmy : the inspector Maréchal
* Pierre Mondy : Sylvestre, campaign of Paul
* Huguette Montréal : Bella

== References ==
 

== External links ==
*  
*   at the Films de France

 
 
 
 
 
 
 


 