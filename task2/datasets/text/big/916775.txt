Let's Go with Pancho Villa
{{Infobox film
| name = Lets Go with Pancho Villa
| image = ¡Vámonos con Pancho Villa!.jpg
| caption = DVD Cover
| director = Fernando de Fuentes
| producer = Alberto J. Pani
| writer = Fernando de Fuentes Rafael F. Muñoz Xavier Villaurrutia
| starring = Domingo Soler Antonio R. Frausto Ramón Vallarino Manuel Tamés
| distributor = Spanish
| released = 31 December 1936
| country = Mexico
| runtime = 92 minutes
| budget =
|}} Mexican film|motion picture directed by Fernando de Fuentes in 1936 in film|1936, the last of the directors Revolution Trilogy, besides El prisionero trece and El compadre Mendoza.

An anti-epic based on a novel, it focuses on the cruelty of the Mexican Revolution and Pancho Villa himself, contrary to most of the Mexican movies about this national hero.

The movie is thought to have been the first Mexican super-production and led to the bankruptcy of the film company that made it.

==Plot==
Villa was portrayed by Domingo Soler. Directed by Fernando de Fuentes, the film tells the story of a group of friends who hear about the revolution and Villa and decide to join him, only to suffer the cruel reality of war under the command of a Villa who simply does not care about his men.

The movie has two endings: the original ending shows the last surviving friend returning to his home, disenchanted with both Villa and the Revolution.

The second ending, discovered many years later, returns to the same scene ten years later, when an old and weakened Villa tries to recruit the last survivor again; when the father hesitates as he does not want to leave his wife and daughter behind, Villa kills the wife and daughter. The angry father then tries to kill Villa, before another man shoots the father dead. Villa takes the sole survivor, the son, with him. 

==Background== Mexican cinema both for its approach to the theme and its technical merits.

It stands apart among the many movies made about Villa in that it portrays the man and the Revolution in its cruelty; most other films, like those by Ismael Rodríguez in the 1960s, take an almost idyllic view of both, following the official (government) Mythology|mythos.

The movie music was composed by Silvestre Revueltas, who makes a cameo appearance.

==See also==
 
* Cinema of Mexico

==External links==
*    .
*  

 

 

 
 
 
 
 
 
 
 
 


 
 