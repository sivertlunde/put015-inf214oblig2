Life Begins in College
 
{{Infobox film
| name           = Life Begins in College
| image          =
| caption        =
| director       = William A. Seiter
| producer       = Harry Wilson
| writer         = Don Ettlinger Karl Tunberg Tony Martin Gloria Stuart
| music          = Lew Pollack Sidney D. Mitchell Louis Silvers
| cinematography = Robert H. Planck
| editing        = Louis R. Loeffler
| distributor    = Twentieth Century-Fox Film Corporation
| released       = October 1, 1937
| runtime        = 94 min 
| country        = United States
| awards         =  English
| budget         =
}}

Life Begins in College is a 1937 American comedy film. It marked the Ritz Brothers first starring role in a feature film. 

==Plot==
The action takes place at Lombardy College, founded "to give the Indian nations of North America access to higher education". Little Black Cloud (Nat Pendleton) enrolls as "George Black". After being the subject of a hazing prank by football star Bob Hayner (Dick Baldwin), George decides he is too humiliated to stay at school. He first stops to get his ripped pants repaired and meets the Ritz Brothers who convince him to stay.

Tim OHara (Fred Stone), the football coach, is being pressured to resign because of his age. Janet (Gloria Stuart), the coachs daughter, is Bobs love interest. Bob is on the committee trying to force out Coach OHara, but he likes the coachs daughter Janet. He tries to gain Janets approval by helping her father, but fails at both. Janet, meanwhile, helps George escape from Inez (Joan Davis, a love-struck student.  Then George wishes to return the favor by helping the coach. George is actually rich, but does not want anyone to know in case they only like him for his money. The Ritz Brothers offer to pretend it is their money.  After spending a good deal on themselves, the brothers donate $50,000 to the college with two conditions: OHara must remain as coach, and they must be allowed to play for the football team.

After a scuffle between them on the practice field, Bob is replaced by George as quarterback. A winning streak for Lombardy ensues, no thanks to the Ritz Brothers, who score for the other side whenever they play. Inez continues to pursue George, and Janets feelings towards Bob soften. Bob, however, is being pursued by his former girlfriend, Cuddles (Joan Marsh).

It turns out that Georges participation on a company football team makes him a professional and therefore ineligible for the college team. This information is used by Cuddles to blackmail Bob into dating her.  When she sees Bob with Janet, she makes good on her threat of exposing George right before the "big game". Since George cannot play, Bob must take over as quarterback. Bob scores a touchdown but breaks his collarbone while the other team is still ahead. The Ritz Brothers sneak onto the field and nearly destroy Lombardys chances with their antics. At the last second Harry Ritz scores an unlikely touchdown to win the game.

== Cast ==
  Al Ritz as Al Harry Ritz as Harry Jimmy Ritz as Jimmy
*Joan Davis as Inez Tony Martin as Band Leader
*Gloria Stuart as Janet OHara
*Fred Stone as Coach Tim OHara
*Nat Pendleton as George "Little Black Cloud" Black
*Dick Baldwin as Bob Hayner
*Joan Marsh as Cuddles
*Jed Prouty as Oliver Stearns, Jr.
*Maurice Cass as Dean Moss
*Ed Thorgersen as Radio announcer
*Marjorie Weaver as Miss Murphy Robert Lowery as Sling
*Lon Chaney Jr. as Gilks
*J.C. Nugent as T. Edwin Cabot
*Fred Kohler Jr. as Bret
*Elisha Cook Jr. as Ollie Stearns Charles C. Wilson as Coach Burke
*Frank Sully as Acting captain
*Norman Willis as Referee
 

==Production==
Life Begins in College was filmed between early July to early September 1937. The several working titles were variations on Pigskin Parade.  The screenplay by Don Ettlinger and Karl Tunberg was based on a series of stories by Darrell Ware.  The Ritz Brothers had been on contract at 20th Century Fox for over a year but this was the first picture to give them star billing,  as well as the first to make them more than incidental to the plot. 
 Al Lewis, and Murray Mencher. Special material for the Ritz Brothers was contributed by Sam Pokrass, Sid Kuller, and Ray Golden. 

==Release==
The film premiered in New York September 24, 1937, and was released nationally on October 1.   Generally favorable reviews appeared in Variety,  Motion Picture Daily,  and The Film Daily. 

==References==
{{reflist|refs=
   
   
   
   
   
   
   
   

}}

== External links ==
*  

 
 
 
 