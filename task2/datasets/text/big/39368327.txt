Prema Ishq Kaadhal
 
 
{{Infobox film
| name           = Prema Ishq Kaadhal
| image          = "Prema_Ishq_Kaadhal".jpg
| caption        = PIK ur story..
| director       = Pavan Sadineni
| producer       = Bekkam Venugopal  Daggubati Suresh Babu  (Presents)   
| screenplay     = Pavan Sadineni
| starring       = {{Plainlist|
* Harshvardhan Rane
* Ritu Varma Vishnu Vardhan
* Haris
* Vithika Sheru
* Sree Mukhi}}
| music          = Shravan
| Photography    = Manisha Panchawati
| cinematography = Karthik Gattamneni
| studio         = Lucky Media
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Telugu
| budget         = 
| gross          = 
}} Pondfreaks Entertainment. Actors Harshavardhan, Vishnu and Harish, and three girls, Vithika Sheru, Srimukhi and Ritu Varma, play three romantic pairs. 

==Cast==
* Harshvardhan Rane as Randhir (Randy)
* Sree Vishnu as Royal Raju
* Harish Varma as Arjun
* Vithika Sheru as Sarayu
* Ritu Varma as Sameera
* Sree Mukhi as Shanti
* Edward Stevenson Pereji

==Crew==
* Director  : Pavan sadineni
* Screenplay & Dialogue  : Pavan Sadineni and Krishna Chaitanya
* Photography  : Karthik Gattamneni
* Editor  : Goutham Raj Nerusu
* Background score  : Shravan
* Art director  : Mohan
* Lyrics (songs)  : Krishna Chaitanya
* Executive Producer  : Ramesh
* Associate Director  : Satish varma
* 1st Assistant Director : Vamsha vardhan

===Development===
The film was in pre-production stage from January 2012 to March 2013, where director and producer were keen in developing the narrative of the motion picture. During the pre-production time, the director Pavan Sadineni has also has directed a short film Bewars, in which Karthik Gattamneni was the cinematographer, and Vishnu Vardhan played the protagonist. Vardhan was cast to play in Prema Ishq Kaadhal, and Harshvardhan was roped in to play a rock star in the film.  Vithika Sheru, who is paired opposite him also donned the hat of a stylist for the film,  while Rane worked as the choreographer for the climax song.   

===Filming===
The principal photography for the movie began on 9 April 2013,    and the major scenes were shot in a special coffee shop set that was erected specially for this movie at Nanakramguda.  The filming of the movie was expected to end in May 2013.

The producer planned to select 100 city students and record their responses to the film and carry out the changes they suggest.  The film was shot in 50 days. 

==Soundtrack==
{{Infobox album
| Name        = Prema Ishq Kaadhal
| Type        = Soundtrack
| Artist      = Various
| Cover       = 
| Released    = 24 October 2013
| Recorded    = Score
| Length      = 
| Label       = Madhura Audio
| Producer    =
| Last album = Alias Jaanaki (2013)
| This album = Prema Ishq Kaadhal (2013)
| Next album = 
}}

The soundtrack with six songs were released on 24 October 2013.

{{Track listing
| extra_column = Singer(s)
| lyrics_credits = yes
| title1 = Prema Ishq Kaadhal 
| extra1 = Shravan
| lyrics1 = Krishna Chaitanya
| length1 = 4:30
| title2 = Tulle Tulle
| extra2 = Sai Charan
| lyrics2 = Krishnakanth
| length2 = 4:04
| title3 = Sammatame
| extra3 = Sai Krishna
| lyrics3 = Krishna Chaitanya
| length3 = 4:21
| title4 = Gundu Soodhi
| extra4 = Varun Madhav
| lyrics4 = Krishna Chaitanya
| length4 = 3:48
| title5 = Chetakaani
| extra5 = Shravan
| lyrics5 = Krishna Chaitanya
| length5 = 5:08
| title6 = Tulle Tulle (Remixed)
| extra6 = Shravan
| lyrics6 = Krishna Chaitanya
| length6 = 3:55
}}

==References==
 

==External links==
* http://filmcircle.com/prema-ishq-kaadhal-review/

 
 
 
 
 