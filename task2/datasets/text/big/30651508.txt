State Property (film)
{{Infobox film
| name           = State Property
| image          = State Property poster.jpg
| caption        = Theatrical release poster
| writer         = Abdul Malik Abbott Ernest "Tron" Anderson
| starring       = Beanie Sigel Jay-Z Damon Dash Memphis Bleek Omillio Sparks
| director       = Abdul Malik Abbott
| producer       = Damon Dash Phyllis Cedar
| cinematography = David Daniel
| editing        = Paul Frank Tim French Justine Harari   
| music          = Abdul Malik Abbott Evan Eder Gregory Darryl Smith Lions Gate Films
| released       =  
| runtime        = 88 min.
| language       = English
| budget         =$600,000 USD
| gross          = $2,106,838 (United States) 
}}
State Property is a 2002 American crime film starring Beanie Sigel, Omillio Sparks, Memphis Bleek and Damon Dash. Rapper Jay-Z appears in a cameo role. It was produced by Roc-A-Fella Films and distributed by Lions Gate Entertainment. Abdul Malik Abbott directed the film and co-wrote its screenplay with Ernest "Tron" Anderson. The film was loosely based on Aaron Jones and the JBM in Philadelphias drug trade from the late 1980s to early 1990s. The movie was followed by a sequel, State Property 2 which was released three years later in 2005.

==Synopsis==
Frustrated with being broke, Beans (Sigel) decides that the only way to grasp the American Dream is to take it. The film follows Beans and his crew, the ABM, as they take over the city, creating mayhem as their empire builds. Beans now struggles to maintain his family life while bumping heads with opposing gangsters and police. It all comes to a head when he cannot surpass the citys most notorious crew, run by Untouchable J (Jay-Z) and Dame (Dash). The moves Beans and the ABM decide to make come with severe consequences.

== Cast ==
* Beanie Sigel - Beans
* Omillio Sparks - Baby Boy
* Memphis Bleek - Blizz
* Damon Dash - Boss Dame
* Sundy Carter - Aisha
* Tyran "Ty-Ty" Smith - Shareef
* Oschino -  D-Nice
* Jay-Z - Untouchable J
* Brother Newz - P-Nut Rell - Butter
* Young Chris - Ricky Young Neef - Troy Amil - Boss Assassin
* DJ Clue - Boss Informant Jacob the Jeweler - Eli
* Casha - Heather McGee
* Randolph Curtis Rand - Saul Weisberg
* Nicole Madeo - Leslie Tucci
* Leslie Pilgrim - Aja
* Kyndra Monet - Selena
* Pain in da Ass - Mario
* Dee Lee - Poochie
* Leah Leatherbury - Tonya
* Reb - Snoop
* Rashie Constantine - Mel
* Stephen "X" Baker - Bruce
* Willie Esco - Papi Wil
* Cousin Ervan - E
* Andrea Videla - Nina

==Reception==
The film gained negative reviews from critics gaining 9% from Metacritic 

TV Guide (Rating 2/4)- Formulaic but performed with some verve. 

==Soundtrack==
 
  State Property rap group. It was released on January 29, 2002 and peaked at 14 on Billboard 200|Billboard 200 and 1 on the Top R&B/Hip-Hop Albums.

==References==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 