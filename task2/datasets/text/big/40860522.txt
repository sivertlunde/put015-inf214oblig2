Stories of Joy
{{multiple issues|
 
 
 
}}

{{Infobox film
| name= Stories of Joy (2013 film)
| image = Stories of joy web.jpg
| alt=
| caption=
| director=Vikrant Pawar,  Nupoor Bora,  Vaibhav Khisti,  Swapnil Kumawat, Prasad Bhardwaja,  Shreyas Kulkarni, Yogesh Jagam 
| producer=Flying Horse Studios   7th Std. Motion Pictures
| starring=Gajanan Paranjape,  Shrikant Yadav, Anita Date, Saurabh Yadav, Shashi Bhushan, Mahesh Ghag,  Arjun Jog and  Yogesh Jagam
| music=Gandhaar
| cinematography= Abhimanyu Dange,  Digvijay Thorat,  Kiran Ghadge,  Tejashree Joshi
| editing=Shreyas Kulkarni
| studio=Flying Horse Studios
| distributor=
| runtime= 41 minutes
| released =  
| country=India Marathi
}} National Award for his earlier short film ‘Kaatal’ 

The Five Short Films are titled ‘Chaiwala’, ‘A Joy Unbound’, ‘Chor’, ‘Dodka’ and ‘The Last One. The first screening of the film was on 19th October 2013 at National Film Archives of India, Pune

This project was well received and appreciated by the media. 
*  
*  
*  
*  
 
Produced by 7th Std. Motion Pictures and Flying Horse Studios

The music is composed by Gandhaar and films are edited by Shreyas Kulkarni.

==Chaiwala==
Directed by: Shreyas Kulkarni & Yogesh Jagam

A short and simple story based on true life event. We all are so busy in our day to day activities and are always looking at the bigger picture but, in the process we sometimes forget that the small simple act can bring smile on someone’s face.

==A Joy Unbound==
Directed by: Vaibhav Khisti

Joy of giving does not always means that you need to give something, sometimes just letting someone enjoy what has brought smiles on their face is more than enough.  A Simple short story about the passion of a father and his son, towards the religion named ‘cricket.’ How the journey through the museum of cricket unfurls is what the story is all about.

==Chor==
Directed by: Swapnil Kumawat & Prasad Bhardwaja

If we adults follow the same as what we teach kids while they are growing the world will be so different.  Forgiveness, love, acts of giving is what innocently portrayed through kids point of view.

Selected at 2nd Smita Patil Documentary and Short Film Festival 2013  

==Dodka==
Directed by: Vikrant Pawar

A young mans bike breaks down in the middle of the night. An old man is stranded clueless, as he has forgotten his way back home after shopping for vegetables. The film is more than just the walking conversation between the two, as the younger man safely guides the older counterpart back to his home, where a concerned daughter awaits.

==The Last One==
Directed by: Noopur Bora

In the ‘Joy of Giving Week’, our dude is on a mission of targeting as many under privileged people as he can, and bring smiles to their faces. He’s found his mission to be extremely easy and has also grabbed a lot of attention on social media. He’s been successful in bringing smiles to all his targets, except the last one. Given a reality check by this last homeless man, will our Dude understand the real Joy of Giving?

Selected at 2nd Smita Patil Documentary and Short Film Festival 2013  

==References==
 
http://dff.nic.in/NonFeaturedFilm_60th_NATIONAL_FILM_AWARDS_2012_Announced.pdf



 
 
 
 