Syriana
 
{{Infobox film
| name          = Syriana
| image         = Syriana.jpg
| caption       = Theatrical release poster
| director      = Stephen Gaghan
| producer      = {{Plainlist|
* Jennifer Fox
* Michael Nozik
* Georgia Kacandes
}}
| writer    = Stephen Gaghan
| based on    =  
| starring      = {{Plainlist|
* George Clooney
* Matt Damon Jeffrey Wright
* Chris Cooper
* William Hurt
* Mazhar Munir
* Tim Blake Nelson
* Amanda Peet
* Christopher Plummer
* Alexander Siddig
}}
| music         = Alexandre Desplat
| cinematography = Robert Elswit
| editing       = Tim Squyres
| studio        = Participant Media Warner Bros. Pictures
| released      =  
| runtime       = 128 minutes  
| language      = English
| country       = United States
| budget        = $50 million   
| gross         = $93.9 million 
}} geopolitical thriller thriller film adapted from See No Jeffrey Wright), and a young unemployed Pakistani migrant worker (Mazhar Munir) in an Arab state in the Persian Gulf. The film also features an extensive supporting cast including Amanda Peet, Tim Blake Nelson, Mark Strong, Alexander Siddig, Amr Waked, and Academy Award winners Christopher Plummer, Chris Cooper and William Hurt.

As with Gaghans screenplay for Traffic (2000 film)|Traffic, Syriana uses multiple, parallel storylines, jumping between locations in Iran, Texas, Washington, D.C., Switzerland, Spain and Lebanon.
 Best Original Screenplay. As of April 20, 2006, the film had grossed a total of $50.82 million at the U.S. box office and $42.9 million overseas, for a total of $93.73 million.

==Plot== Jeffrey Wright) is assigned to promote the impression of due diligence to the DoJ, deflecting any allegations of corruption.

===Emir storyline=== energy analyst based in Geneva, Switzerland. Woodmans supervisor directs him to attend a private party hosted by the emir at his estate in Marbella, Spain, to offer his companys services. The emirs illness during the party prevents Woodman from speaking directly with him while, at the same time, the emirs younger son, Prince Meshal Al-Subaai (Akbar Kurtha), shows the estates many rooms and areas to Chinese oil executives via remote-controlled cameras. No one notices that a crack in one of the swimming pool areas underwater lights has electrified the water. Just as Woodman and all the other guests are brought to the pool area, Woodmans son jumps into the pool and is electrocuted.
 reparation and democratic reforms, in sharp contrast to his fathers repressive government, which has been supported by American interests.  His father, at the urging of the American government, names the younger Meshal, who has gained the favor of Whiting, as his successor, causing Nasir to attempt a coup.

===Assassination storyline=== Stinger MANPAD) memos about the missile theft and is subsequently reassigned to a desk job. However, unaccustomed to the political discretion required, he quickly embarrasses the wrong people by speaking his mind and is sent back to the field with the assignment of assassinating Prince Nasir, whom the CIA identifies as being the financier behind the Egyptians acquisition of the missile. Prior to his reassignment, Barnes confides in his ex-CIA agent friend, Stan Goff (William Hurt), that he will return to Lebanon. Goff advises him to clear his presence with Hezbollah so they know he is not acting against them. Barnes travels to Lebanon, obtains safe passage from a Hezbollah leader, and hires a mercenary named Mussawi (Mark Strong) to help kidnap and murder Nasir. But Mussawi has now become an Iranian agent and has Barnes abducted instead; Mussawi himself then tortures Barnes. The Hezbollah leader ultimately arrives at the scene of Barness torture in time to stop Mussawi from beheading Barnes.

When the CIA learns that Mussawi plans to broadcast the agencys intention to kill Nasir, they try to distance themselves by scapegoating Barnes, portraying him as a rogue agent. Barness boss, Terry George, worries—first about Barnes talking about the Nasir assassination plan, second about the possibility that Nasirs coup might have a greater likelihood of success, and third that killing Nasir with an MQ-1 Predator drone would make it obvious as an American-backed assassination. So he has Barness passports revoked, locks him out of his computer at work, and has him investigated. Barnes, however, learns from Goff that Whiting, working on behalf of a group of businessmen calling themselves the Committee to Liberate Iran, is responsible for Barness blackballing and the assassination job, and threatens him and his family unless he halts the investigation and releases Barness passports.

After Barnes eventually learns why he was portrayed as a rogue agent, he returns to the Middle East and approaches Prince Nasirs convoy to warn him of the assassination plan. As he arrives, a guided bomb from a circling Predator drone strikes the automobile of Nasir and his family, killing them instantly. Woodman, having earlier offered his seat to Nasirs family, survives the blast and makes his way home to his wife and son.

===Wasim storyline=== migrant workers Islamic school LNG tanker using a Shaped charge|shaped-charge explosive from the missing Tehran missile.

===Merger storyline===
Bennett Holiday meets with U.S. Attorney Donald Farish III (David Clennon), who is convinced that Killen bribed someone to get the drilling rights in Kazakhstan. While investigating Connex-Killens records, Holiday discovers a wire transfer of funds that leads back to a transaction between Texas oilman and Killen Co. colleague Danny Dalton (Tim Blake Nelson) and Kazakh officials. Holiday tells Connex-Killen of his discovery, and they pretend not to have known about it. Holiday advises Dalton that he will likely be charged with corruption in order to serve as a "body" to get the DoJ off the back of the rest of Connex-Killen; Dalton responds with a fervent defense of how corruption is simply the way of competition and how America "wins" against the rest of the corrupt world. Farish then strong-arms Holiday into giving the DoJ information about illegal activities he has discovered. Holiday gives up Dalton, but Farish says this is not enough. Holiday meets with the CEO of Killen Oil, Jimmy Pope (Chris Cooper), and informs him that the DoJ needs a second body in order to drop the investigation. Pope asks Holiday whether a person at Holidays firm above him would be sufficient as the additional body. Holiday acknowledges that if the name were big enough, the DoJ would stop the investigation and allow the merger.

Holiday is brought by his colleague and mentor Sydney Hewitt (Nicky Henson) to meet with the CEO of Connex Oil, Leland "Lee" Janus (Peter Gerety). In a surprise move, Holiday reveals an under-the-table deal that Hewitt made while the Connex-Killen merger was being processed. Holiday has given Hewitt to the DoJ as the second body, thereby protecting the rest of Connex-Killen. Janus is able to accept the "Oil Industry Man of the Year" award with a load taken off his shoulders. Throughout the film, Holiday has angrily crossed paths with his alcoholic father Bennett Sr.; at the movies end when the merger has been completed, Bennett Jr. brings his father into his house.  

===Father–son storylines===
The final Holiday-family scene serves to underscore the fact that every storyline here is built around major father–son relationships: Woodman returns home to embrace his surviving younger son following the murder of Prince Nasir, whose own father, the Emir, had rejected him and handed the throne to his younger brother. Wasim hugs and waves farewell to his totally unsuspecting Pakistani father before embarking on his suicidal terrorist mission. And Bob Barnes, collateral damage in the Nasir assassination, leaves behind his only child, the high school senior who had earlier called both him and his CIA-employed mother "professional liars." Throughout the film, in fact, the solitary, visible mother figure—indeed, one of the only compassionate female characters of any type—is Julie Woodman (Amanda Peet), the grieving mother of a son, one of three young sons tragically killed in the film.

==Cast==
* George Clooney as Bob Barnes, a veteran CIA field officer stationed in the Middle East. His son is going to college and dislikes him for his job.
* Matt Damon as Bryan Woodman, an energy analyst living in Geneva, Switzerland with his wife and two sons. Jeffrey Wright as Bennett Holiday, an attorney investigating the proposed merger of oil companies, Connex and Killen. Employed by Whiting Sloan law firm.
* Christopher Plummer as Dean Whiting, managing partner of Whiting Sloan, employer of Sydney Hewitt and Bennett Holiday; member of the Committee to Liberate Iran (CLI). 
* Amanda Peet as Julie Woodman, Bryans wife.
* Chris Cooper as Jimmy Pope, owner of the Killen oil company.
* Tim Blake Nelson as Danny Dalton, oilman from Texas and member of CLI.
* William Hurt as Stan Goff, retired CIA agent and associate of Bob Barnes.
* Mark Strong as Mussawi, a mercenary hired by a Hezbollah leader.
* Robert Baer in a guest role, as a CIA officer.
* Mazhar Munir as Wasim Ahmed Khan, Pakistani migrant worker laid off from Connex.
* Alexander Siddig as Prince Nasir Al-Subaai, gulf prince who is the first born successor to the Emir.
* Amr Waked as Muhammad Sheikh Agiza, the Egyptian Muslim cleric.
* Akbar Kurtha as Prince Meshal, younger brother of Prince Nasir who plots against his brother to inherit the kingdom.
* Viola Davis as CIA Chairwoman (uncredited).
* Jayne Atkinson as CIA Division Chief.
* Jamey Sheridan as Terry George, CIA Chair.

==Production==
While working on Traffic, Stephen Gaghan began to see parallels between drug addiction and Americas dependency on foreign oil.    Another source of inspiration came from 9/11 and Gaghans lack of knowledge about the Middle East. He said, "When 9/11 happened, it suddenly was a war on terror, which I think of as a war on emotions. It all started to click for me."    A few weeks after 9/11, Steven Soderbergh sent Gaghan a copy of ex-CIA officer Robert Baers memoir, See No Evil.    The screenwriter read the book and wanted to turn it into a film because it added another layer to the story that Gaghan wanted to tell.  Soderbergh bought the rights to See No Evil and negotiated the deal with Warner Bros.   
 London and lawyers in Washington, D.C. Moments after arriving in Beirut in 2002, Gaghan was taken from the airport in a blindfold and hood where he met with Sheik Mohammed Hussein Fadlallah, who was interested in films. He decided to grant the writer an audience even though he had not requested one. In addition, Gaghan dined with men suspected of killing former Lebanese Prime Minister Rafiq Hariri and met with Former Defense Policy board chairman Richard Perle. 

Gaghan has cited as influences on Syriana, European films like Roberto Rossellinis Rome, Open City, Costa Gavras Z (1969 film)|Z, and Gillo Pontecorvos The Battle of Algiers.   

Another influence, or resource--one that might also explain the movies use of a documentary clip featuring John D. Rockefeller--is the fact that Gaghans fashion-designer wife Minnie Mortimer is the great-granddaughter of onetime Standard Oil executive Henry Morgan Tilford.

Harrison Ford turned down the role of Bob Barnes (the role played by George Clooney), regretting it later, stating, "I didnt feel strongly enough about the truth of the material and I think I made a mistake."  This is the second Stephen Gaghan-written role Ford has declined, having turned down the role of Robert Wakefield in Traffic (2000 film)|Traffic, a role that eventually went to Michael Douglas. 

===Principal photography===
Gaghan shot in over 200 locations on four continents with 100 speaking parts.  Syriana originally had five storylines, all of which were filmed. The fifth storyline, centering on Michelle Monaghan playing a Miss USA who becomes involved with a rich Arab oilman, was cut when the film became too complicated.   Also, a role played by Greta Scacchi, as Bob Barnes wife, was also cut before the final release.

===Score===
 

==Title==
The movies title is suggested to derive from the hypothesized Pax Syriana, as an allusion to the necessary state of peace between Syria and the U.S. as it relates to the oil business. In a December 2005 interview, Baer told NPR that the title is a metaphor for foreign intervention in the Middle East, referring to post-World War II think tank strategic studies for the creation of an artificial state (such as Iraq, created from elements of the former Ottoman Empire) that ensured continued western access to crude oil. The movies website states that "‘Syriana’ is a real term used by Washington think-tanks to describe a hypothetical reshaping of the Middle East."  Gaghan said he saw Syriana as "a great word that could stand for mans perpetual hope of remaking any geographic region to suit his own needs."  more extensive Hebrew original of the Scriptures, but appears in the Septuagint as the translation of Aram (biblical region)|Aram. Herodotus speaks of "Syrians" as identical with Assyrian people|Assyrians, but the terms geographical significance was not well defined in pre-Greek and Greek times. As an ethnic term, "Syrian" came to refer in Antiquity to Semitic peoples living outside Mesopotamian and Arabian areas.  Greco-Roman administrations were the first to apply the term to a definite district. 

==Reaction==
Syriana was released on November 23, 2005 in limited release in only five theaters grossing $374,502 on its opening weekend. It went into wide release on December 9, 2005 in 1,752 theaters grossing $11.7 million on that weekend. It went on to make $50.8 million in North America and $43.1 million in the rest of the world for a worldwide total of $93.9 million.   

===Critical reception===
Syriana received generally positive reviews and has a certified fresh rating of 72% On Rotten Tomatoes based on 193 reviews with an average score of 6.9 out of 10. The consensus states "Ambitious, complicated, intellectual, and demanding of its audience, Syriana is both a gripping geopolitical thriller and wake-up call to the complacent."  The film also has a score of 76 out of 100 on Metacritic based on 40 critics indicating "Generally favorable reviews". 

As a motion picture, the main criticism, even among reviewers who praised the film, was the confusion created by following numerous stories. Most critics stated that it was almost impossible to follow the plot, though some, notably Roger Ebert, praised precisely that quality of the film and offered an interesting hidden story possibility (a covert deal between the U.S. and China involving oil being shipped through Kazakhstan and passed off as coming from a different source).    The audience confusion mimics the confusion of the characters, who are enmeshed in the events around them without a clear understanding of what precisely is going on. As with Gaghans screenplay for Traffic (2000 film)|Traffic, Syriana uses multiple, parallel storylines, jumping from locations in Texas, Washington D.C., Switzerland, Spain, and the Middle East, leading film critic Ebert to describe the film as hyperlink cinema. 

 , Kenneth Turan wrote, "This is conspiracy-theory filmmaking of the most bravura kind, but if only a fraction of its suppositions are true, we—and the world—are in a world of trouble."      USA Today gave the film three-and-a-half stars out of four and wrote, "Gaghan assumes his audience is smart enough to follow his explosive tour of global petro-politics. The result is thought-provoking and unnerving, emotionally engaging and intellectually stimulating."    Entertainment Weekly gave the film a "B−" rating and Lisa Schwarzbaum wrote, "its also the kind of movie that requires a viewer to work actively for comprehension, and to chalk up any lack of same to his or her own deficiency in the face of something so evidently smart."   

In his review for   magazines Peter Travers gave the film his highest rating and praised George Clooneys performance: "This is the best acting Clooney has ever done—hes hypnotic, haunting and quietly devastating."    Philip French, in his review for The Observer, praised the film as "thoughtful, exciting and urgent".    In his review for The Guardian, Peter Bradshaw wrote, "But what complicates the plot is writer-director Stephen Gaghans reluctance to criticise America too much. Instead of complexity, there is a blank, uncompelling tangle, which conceals a kind of complacent political correctness."   

Ebert named it the second-best film of 2005, behind Crash (2004 film)|Crash. Peter Travers of Rolling Stone named it as the third best film of 2005.    Entertainment Weekly ranked Syriana as one of the 25 "Powerful Political Thrillers" in film history.   

===Awards=== Golden Globe Award for Best Supporting Actor in a Motion Picture.    He was also nominated for a BAFTA Award for Best Actor in a Supporting Role.   
 Grand Prix of the Belgian Syndicate of Cinema Critics. 

The National Board of Review named Syriana one of the best films of the year and Stephen Gaghans screenplay as the Best Adapted Screenplay.   

==See also==

* List of films featuring drones

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  
*  
*   - Diagram explaining, in detail, the plot of Syriana.
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 