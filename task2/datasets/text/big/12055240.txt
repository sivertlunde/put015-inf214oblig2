Manufactured Landscapes
{{Infobox film
| name           = Manufactured Landscapes
| image          = ManufacturedLandscapes.jpg
| caption        = 
| director       = Jennifer Baichwal
| producer       = Nick de Pencier Daniel Iron Jennifer Baichwal
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| starring       = Edward Burtynsky
| music          = Dan Driscoll
| cinematography = Peter Mettler
| editing        = Roland Schlimme
| studio         = 
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = Canada
| language       = English French
| budget         = 
| gross          = 
}}
Manufactured Landscapes is a 2006 feature length documentary film about the work of photographer Edward Burtynsky. It was directed by Jennifer Baichwal and is distributed by Zeitgeist Films.

== Subject matter ==
The film involves the photographs and videos of photographer and visual artist Ed Burtynskys trip through landscapes that have been altered by large-scale human activity, captured with Super-16mm film.  Most of the photographs featured in the film are pieces that are exhibited all over the world and are taken with a "large format field camera on large 4x5-inch sheet film and developed into high-resolution, large-dimension prints (of approximately 50x60 inches)"   While some would call the work beautiful, his main goal was to challenge notions while raising questions about the interplay of environmental ethics and aesthetics. The footage was compiled from a trip to China where Burtynsky visited factories which Western society has come to rely on for most of its appliances, including a factory that produces most of the worlds supply of clothes irons, which is one kilometer in length and employs 23,000 workers. The film also features the Three Gorges Dam, which, along with being the largest dam in the world, has uprooted more than one million people and flooded 13 cities, 140 towns and 1350 villages since the beginning of its construction in 1994.  Unlike most documentaries, there is very little commentary, which allows viewers to take in the images and try to make sense of what theyre seeing, while at the same time the film "tries to shift our consciousness about the world and the way we live in it". 

Jennifer Baichwal elaborated on the films minimal commentary and its connection to the films overall message in a Q&A with Film Forum, noting that "...if the film was didactic it would imply an easy answer . . ."  With this statement, the film does not propose a concrete solution to human environmental impact, but rather asks the viewer to consider the many contributing factors.

==Critical reception==

Since its debut at the Toronto Film Festival in 2006, the film has received generally positive reviews. Lisa Schwarzbaum of Entertainment Weekly gave the film an "A" and said "The opening tracking shot through a Chinese factory where 23,000 employees make most of the worlds irons is a stunner."    The review that appeared in the Boston Globe said the film "begs to be hung on the wall, studied, absorbed, and learned from" and also "taken as a whole, Manufactured Landscapes is a mesmerizing work of visual oncology, a witness to a cancer thats visible only at a distance but entwined with the DNA of everything we buy and everywhere we shop."    Ken Fox of TV Guide gave the film four stars and said, "Jennifer Baichwals important, disquieting documentary offers the strongest reminder since Born into Brothels that art can serve a crucial, consciousness raising purpose."    Kenneth Baker of the San Francisco Chronicle said "the viewer soon realizes that   shares Burtynskys astonishment and concern over the scale, tempo and irreversibility of postmodern humanitys global frenzy of production and consumption", and also that the film "leaves its audience with many troubling questions."     The Host), and Joe Morgenstern of The Wall Street Journal named it the 8th best film of 2007. 

Although most have praised the film, there has been some negative reception. Michael Phillips of the Chicago Tribune praised the opening shot, but said "the rest of director Baichwals picture feels constrained and rather dutiful, no matter how passionate these people are about what theyre observing."   

As of April 2012, the film had an average score of 79 on Metacritic based on 16 reviews. On Rotten Tomatoes 84% of critics had given the film a "fresh" rating based on 61 reviews.

== Awards ==
* Best Documentary – 2007 Genie Awards   
* Best Canadian Film – Toronto International Film Festival 
* Best Canadian Film & Best Documentary -  Toronto Film Critics Association Awards 
* Nominated for Grand Jury Prize - 2007 Sundance Film Festival 
* Reel Current Award (presented by Al Gore) - 2007 Nashville Film Festival   
* Best Documentary - 2007 RiverRun International Film Festival   

==See also==
*Watermark (film)|Watermark, a 2013 documentary film co-directed by Baichwal and Burtynsky

==References==
 

== External links ==
* 
*  
* 
* 

 
 
 
 
 
 
 
 
 
 