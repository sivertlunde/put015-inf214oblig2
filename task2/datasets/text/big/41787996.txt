Rasam (film)
{{Infobox film
 | name               = Rasam
 | image              = Rasam Movie poster.jpg
 | caption            =  
 | director           = Rajeev Nath
 | producer           = Group 10
 | writer             = 
 | screenplay         = Nedumudi Venu Rajeev Nath Sudip Kumar
 | story              = Sudip Kumar
 | starring           = Indrajith Sukumaran Nedumudi Venu Varuna Shetty
 | music              = Songs: Job Kurian Background Score: Vishwajith 
 | cinematography     = Krish Kamal
 | editing            = Babu Ratnam
 | associate director = Indrajith Ramesh
 | studio             = Group Ten Entertainments Pvt. Ltd Chaya Films Maxlab Release
 | released           =  
 | runtime            = 135 minutes
 | country            = India
 | language           = Malayalam
 | budget             =  
 }}
 Devan and Varuna Shetty in supporting roles. Mohanlal had a cameo appearance as himself.   The film was shot in Qatar and Dubai. Indrajith plays Balu who is reluctant to take is fathers tradition of cooking.

The film takes place in Doha where Shekar Menon, a business tycoon arranges his daughters wedding to be done in a traditional Kerala style feast. Superstar Mohanlal invites Valliyottu thirumeni to take up the catering for the wedding reception of the stars close friend Shekar Menons daughter Janaki. Vasudevan, Balu and team fly to Dubai. But circumstances force Balu to take over from his father. As the name suggests, food forms the central character in the film. 

The principal photography commenced on January 8, 2014 at Doha. Filming underway at the locations in Doha, Dubai, Qatar, Kochi, Ottapalam and Thiruvananthapuram. The film released in January 23, 2015. 

The film opened on 23 January 2015 and is considered a box office flop. The film also received mixed reviews from critics whereas was panned by Audience.

==Cast==
* Mohanlal as Himself 
* Indrajith Sukumaran as Bala Shankar
* Varuna Shetty as Janaki, Menons daughter
* Nedumudi Venu as Valliyottu Thirumeni
 Devan as Shekar Menon
* Jagadish as Abdhu
* Nandhu as Govindan Nair
* Mythili as Shahida
* Ambika Mohan as Mrs. Thirumeni
* Rajesh Rajan as Job
* Dileep Shankar
* Albert Alex as Josemon Satheesh Menon as Family Doctor
* Nihal Pillai as Manu
* Bindu K Menon as Mrs Menon

==Production== Gulf regions. The shooting in Dubai is expected to be wrapped up within 2 weeks and the team will shoot in Ottapalam. {{cite web|title=Rasam started rolling
|url=http://www.nowrunning.com/rasam-started-rolling/88087/story.htm|publisher=nowrunning.com|accessdate=10 October 2014|date=9 January 2014}}  On May, the film has completed its shoot in Doha, Dubai, Kochi, and Thiruvananthapuram. 

==Casting==

Reportedly   is sharing screen space with Mohanlal.  Indrajith plays the role of a Namboothiri cook (Balu) who runs a restaurant in Kerala. And Balu is a B.Tech & MBA holder. His father, a popular cook famous for preparing delicious food is invited to Doha to do catering service for the marriage of the daughter of a rich man, he follows him as a chef in charge. 

Varuna Shetty had done the heroine in the film, which is her first Malayalam debut. She has been launched by director Ranjith Bajpe for his TULU film NIREL MOVIE.  She is one main female lead in the movie as janaki, the Dubai-based business tycoon R.J Menons daughter. It is her wedding which centres the main plot of the film. Menon calls renowned chefs from kerala to make a traditional Sadya for her wedding. Mythili is the other female lead in the film.  Nihal Pillai is playing a villain role in the film. 

==Music==

{{Infobox album  
| Name       = Rasam
| Type       = soundtrack
| Artist     = Job Kurian
| Cover      = 
| Alt        = 
| Released   =  
| Recorded   = 
| Genre      = Film soundtrack
| Length     =  
| Language   = Malayalam
| Label      = Manorama Music
| Producer   = Job Kurian
| Last album = 
| This album = Rasam (2015)
| Next album = 
}}

The soundtrack consists of 3 songs composed by Job Kurian and sung by K. S. Chithra, Kavalam Sreekumar and Job Kurian himself. Lyrics are penned by Kavalam Narayana Panicker.  Rasam makes the debut of musician turned composer Job Kurian. Previously he had co-composed the background score of a Kannada film. K.S Chithra had sung a traditional thiruvathira song and Kavalam Sreekumar sung a fun song on the occasion of a feast. Job Kurians is a travel track in the drum and base genre with Indian elements. 

{{tracklist
| headline = Tracklist
| extra_column = Singer(s)
| total_length = 11:25
| lyrics_credits = yes
| title1  = Dhanumasa Palazhi
| lyrics1 = Kavalam Narayana Panicker
| extra1  = K. S. Chithra
| length1 = 3:55
| title2  = Sarasa Sarasaro
| lyrics2 = Kavalam Narayana Panicker
| extra2  = Kavalam Sreekumar
| length2 = 3:16
| title3  = Mayamo Marimayamo
| lyrics3 = Kavalam Narayana Panicker
| extra3  = Job Kurian
| length3 = 4:14
}}

==References==

 

==External links==
*  

 
 
 
 