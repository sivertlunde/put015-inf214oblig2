Snow Time
{{Infobox Hollywood cartoon|
| cartoon_name = Snow Time
| series = Krazy Kat
| image =
| caption =
| director = Ben Harrison Manny Gould
| story_artist = Ben Harrison George Herriman
| animator = Allen Rose Harry Love
| voice_actor =
| musician = Joe de Nat
| producer = Charles Mintz
| distributor = Columbia Pictures
| release_date = November 30, 1932
| color_process = Black and white
| runtime = 5:58 English
| preceded_by = The Minstrel Show
| followed_by = Wedding Bells
}}

Snow Time, also known as Alaska Daze in some reissues, is an animated short film distributed by Columbia Pictures, and as part of the Krazy Kat series.

==Plot==
It is winter time and Krazy is in his horse-drawn sleigh, running through the snowy outdoors. Next, he stops over at the house of his spaniel girlfriend. Krazy then calls and invites her to go out with him. Showing herself through a window, the spaniel discloses she cannot come outside because her door is blocked by thick snow. In this, Krazy and his horse goes on to clear the doorway. In just a short while, the impeding snow has been removed, and the spaniel finally steps out. They then head off in the sleigh.

Krazy and the spaniel arrive at their destination, the frozen lake. They then put on their ice skates and set foot on the ice. For several moments, things are going very smooth for them. Their movements are fluent and they never stumble into things. This is until Krazy crashes into a wooden barrel and starts to lose control. He then overshoots the edge of the lake and gets himself into a snowy slope. As he tumbles down the slope, Krazy becomes covered in snow and immediately becomes a large rampaging snowball. The snowball bashes a cabin and rolls into a barn where it runs over the farm animals. Finally, it spatters onto a larger house, and Krazy is free at last. Just nearby him is the spaniel who is relieved to know he is unharmed. Without further ado, they continue looking for ways to spend time in the frosty landscape.

==Availability==
*Columbia Cartoon Collection: Volumes 3 and 12. {{cite web
|url=http://theshortsdepartment.webs.com/columbiacartoons.htm
|title=The Columbia Cartoons
|accessdate=2012-06-17
|publisher=the shorts development
}} 

==References==
 

==External links==
*  at the Big Cartoon Database
* 

 

 
 
 
 
 
 
 


 