Shining Time Station: 'Tis a Gift
{{Infobox television film
| name                 = Shining Time Station: Tis a Gift
| image                =
| director             = Gregory Lehane
| producer             = Britt Allcroft Rick Siggelkow
| writer               = Brian McConnachie
| starring             = Ringo Starr Didi Conn Nicole Leach Rachel Miner Brian OConnor Jason Woliner Lloyd Bridges
| music                = Stephen Horelick Joe Raposo
| editing              = Michael Belanger
| studio               = Quality Family Entertainment
| network              = PBS
| released             =  
| runtime              = 56 minutes
| country              = United States
| language             = English
}} Christmas special PBS television series Shining Time Station first broadcast on December 9, 1990.

==Plot==
It takes place during the Christmas season at Shining Time Station. The scene opens with Mr. Conductor (Ringo Starr) carrying a freshly cut Christmas tree into his home. 

The station is filled with people trying to catch trains to take them to see their families. Even Schemer (Brian OConnor) is in a good mood, because the Indian Valley committee is putting on a Christmas pageant and he is almost certain to get the part of Santas helper. The kids, Matt (Jason Woliner) and Tanya (Nicole Leach), write letters to Santa Claus and Mr. Conductor sends them on their way. 

An old man, Mr. Nicholas (Lloyd Bridges), enters the station carrying a Christmas tree with Harrys cousin Tucker Cupper (Ardon Bess), and immediately knows everything about Stacy Jones (Didi Conn) from when she was a little girl. Mr. Nicholas says he is there to catch the Northern Star, a train that doesnt run on that railway, but Mr. Nicholas is confident it will come.

Meanwhile, Stacy agrees to watch Claires (Judy Marshak) daughter, Vickie (Rachel Miner), a bratty little girl, at the station. Matt and Tanya dislike her because of her attitude. She meets Mr. Nicholas who teaches her to be nice to people and it soon begins to work; Schemers mom has made him a Santas helper costume and he begins his campaign against Stacy by telling Midge Smoot (Bobo Lewis), J.B. King (Mart Hulswit) and the Mayor (Jerome Dempsey) untrue things about her. 

Mr. Nicholas (actually Santa Claus) is still waiting for the Northern Star. To everyones surprise, the train pulls into the station as he had predicted. Meanwhile, the fighting continues over who will play the part of Santas helper. Mr. Nicholas encourages Vickie to use her gift of song to quell the conflict. Her singing of the 1848 Shaker song "Simple Gifts" reminds them all of the true spirit of Christmas. Harmony is restored as all gather around the tree to celebrate the power of believing.

The second season casting change from Ringo Starr to George Carlin as Mr. Conductor would be explained by Starrs character having left Shining Time Station to go with Mr. Nicholas to the North Pole.  While there is a brief encounter at the end of this program between Mr. Nicholas and Mr. Conductor, nothing in the story actually mentions Mr. Conductors departure.  

The snowy winter footage used for the opening and closing sequences was filmed at the Grand Canyon Railway between Williams, Arizona and the South Rim of the Grand Canyon. The final filmed scene of the title sequence shows the train pulling into the former Santa Fe Railway station at Grand Canyon Village on the South Rim.  The historic Bright Angel Lodge and El Tovar Hotel are in the background nearby.  The steam locomotive shown is #18, a 2-8-0 type.  Number 18 appears again during the third musical number, "Its Christmas Time at the Railway Station" and also contributes the sound of its whistle, a Southern Pacific six-chime, for Mr. Nicholas heard-but-not-seen "Northern Star."  This locomotive now resides at the Mount Hood Railroad in Oregon.

==Cast==
* Ringo Starr as Mr. Conductor
* Didi Conn as Stacy Jones Brian OConnor as Schemer 
* Jason Woliner as Matt Jones
* Nicole Leach as Tanya Cupper
* Lloyd Bridges as Mr. Nicholas
* Ardon Bess as Tucker Cupper
* Jerome Dempsey as Mayor Flopdinger
* Mart Hulswit as Mr. J.B. King Esquire
* Judy Marshak as Claire
* Rachel Miner as Vickie
* Bobo Lewis as Midge Smoot

The Jukebox Band Jonathan Freeman as Tito
* Olga Marin as Didi 
* Vaneese Thomas as Grace
* Alan Semok as Tex
* Craig Marin as Rex

==See also==
* List of American films of 1990
* List of Christmas television specials

==External links==
*  

 

 
 
 
 
 
 