Rocky Mountain Mystery
{{Infobox film
| name           = Rocky Mountain Mystery
| image          = Rocky_Mountain_Mystery_1935_Poster.jpg
| border         = yes
| caption        = Theatrical release poster Charles Barton
| producer       = Harold Hurley
| screenplay     = {{Plainlist|
* Edward E. Paramore Jr.
* Ethel Doherty
}}
| based on       =  
| starring       = {{Plainlist|
* Randolph Scott
* Mrs. Leslie Carter
* Ann Sheridan
}}
| music          = Rudolph G. Kopp
| cinematography = Archie Stout
| editing        = Jack Dennis
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 63 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Western mystery film directed by Charles Barton and starring Randolph Scott, Mrs. Leslie Carter, and Ann Sheridan.    Based on an unpublished novel Golden Dreams by Zane Grey, the film is about a mining engineer who teams up with a crusty deputy sheriff to solve a series of mystery killings at an old radium mine where the owners family waits for his death for their inheritance. The film was re-released under the title The Fighting Westerner.   

==Plot==
Mining engineer Larry Sutton (Randolph Scott) arrives at the Ballard radium mine to take over as chief engineer from his missing brother-in-law Jack Parson, who is a suspect in the murder of ranch caretaker Adolph Borg. Sutton teams up with deputy sheriff Tex Murdock (Chic Sale) who is investigating the murder. Staying at the ranch with the ailing owner, Jim Ballard (George F. Marion), are his niece Flora (Kathleen Burke) and nephew Fritz (Howard Wilson) whove been notified of their uncles failing health now wait to inherit his legacy. Also staying at the ranch is a mysterious Chinaman Ling Yat (Willie Fung), the housekeeper Mrs. Borg (Leslie Carter), her son John (James Eagles), and the beautiful and spirited Rita Ballard (Ann Sheridan), another niece, who quickly earns Suttons trust and romantic interest.

Shortly after Sutton arrives, Ballards nephew Fritz is murdered by a mysterious cloaked figure in the same manner that Adolph was killed—crushed beneath the massive weight of a stamp mill, a huge apparatus used to pulverize rock to unearth valuable ore. Sutton and Tex find that the ranch guests all have alibis. Soon the mysterious cloaked figure strikes again, shooting young John, attacking Sutton, and slashing Floras throat. While the investigation continues, Jim, who is apparently an invalid, takes a turn for the worse, prompting Sutton to contact his ex-wife who hasnt been to the ranch in thirty years.

Mrs. Ballard arrives at the ranch, Mrs. Borg tries to prevent her from seeing her ailing ex-husband. Sutton arrives and helps her upstairs where she discover that "Jim" is actually Adolph Borg, and that he and his wife had killed the real Jim Ballard sometime earlier during a takeover attempt by the Borg family. Adolph then tricks Sutton and escapes, taking Rita as a hostage. Sutton follows them to the mine where he fights with Adolph and John, nearly being crushed by the stamp mill. After Adolph falls to his death, Sutton rescues Rita. Afterwards, Mrs. Borg, her son John, and their Chinese servant Ling Yat are sentenced to twenty years in prison, and Tex is made sheriff. Larry and Rita get married and buy a ranch in Hawaii.

==Cast==
* Randolph Scott as Larry Sutton
* Mrs. Leslie Carter as Mrs. Borg
* Ann Sheridan as Rita Ballard
* Chic Sale as Tex Murdock
* Kathleen Burke as Flora Ballard
* George F. Marion as Jim Ballard
* Florence Roberts as Mrs. Ballard
* Howard Wilson as Fritz Ballard
* Willie Fung as Ling Yat
* James Eagles as John Borg   

==Production==
Rocky Mountain Mystery was filmed on location at the Doble mine and at the stamping mill on Gold Mountain at Big Bear Lake, Big Bear Valley, California. Based on an unpublished novel Golden Dreams by Zane Grey, the films original working title was The Vanishing Pioneer. The film was later re-released under the title The Fighting Westerner.

==Reception==
In his review for DVD Talk, Carl Davis noted the films uniqueness in its "modern bent for being a western".    Set shortly after World War I, the story incorporates automobiles, telephones, and even a modern radium mine that "set the film apart and add depth to this typical, by-the-numbers mystery."  Davis concludes, "The Fighting Westerner really is a mystery, and a fair play one at that. Enough clues are given so that if the audience pays attention, they can figure out the answer well before the final reel." 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 