The Man Upstairs (film)
{{Infobox film
| name           = The Man Upstairs
| image          = "The_Man_Upstairs"_(1958).jpg
| image_size     = 
| caption        = Original UK quad poster
| director       = Don Chaffey
| writer         = Alun Falconer Robert Dunbar Don Chaffey
| producer       = Robert Dunbar
| starring       = Richard Attenborough
| music          =  Gerald Gibbs
| editing        = John Trumper
| studio         = 
| distributor    = 
| released       = February 13, 1961
| runtime        = 88 min.
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
 Act Films Ltd. 

==Plot==
Peter Watson, a tenant of an apartment block is troubled with pain and an inability to sleep. He repeatedly tries unsuccessfully to light the gas-fire that requires coins and seeks help from another lodger, artist Nicholas, who is spending the night with his model, and is reluctant to be disturbed. Another neighbour, Pollen calls for police help. The others in the boarding house are awaken by this time, and Mrs. Harris tries to help the mentally confused Watson but he also refuses her help. The police clash with Dr. Sanderson, a welfare worker, who thinks he can take the gun-toting Watson without complications, but when a police sergeant is injured, Police Inspector Thompson is determined to take Watson by force if necessary.

==Cast==
* Richard Attenborough as Peter Watson
* Bernard Lee as Inspector Thompson
* Donald Houston as Dr. Sanderson
* Dorothy Alison as Mrs. Barnes
* Patricia Jessel as Mrs. Lawrence
* Virginia Maskell as Helen Grey 
* Kenneth Griffith as Pollen 
* Alfred Burke as Mr. Barnes 
* Charles Houston as Nicholas 
* Maureen Connell as Eunice Blair 
* Amy Dalby as Miss Acres
* Walter Hudd as Superintendent
* Patrick Jordan as	Injured Sergeant
* Graham Stewart as Sergeant Morris
* Victor Brooks as Sergeant Collins
* Edward Judd as P.C. Stevens

==Critical reception==
TV Guide wrote, "a superb performance from Attenborough is at the core of this character study";  and The New York Times singled out Alun Falconers "script, the tight direction by Don Chaffey, and the performances of the principals", and noted, "although they (the performances) do not make "The Man Upstairs" extraordinary, they give this modest effort the sheen of honesty and quality."  

==References==
 

==External links==
* 

 

 
 
 
 