Enga Veettu Pillai
 
 

{{Infobox film
| name            = Enga Veettu Pillai
| image           = MGR-EVP.jpg
| caption         = Film Poster Chanakya
| Chakrapani
| writer          = Sakthi T. K. Krishnasamy
| story           = D. V. Narasa Raju
| starring        = M. G. Ramachandran   B. Saroja Devi   M. N. Nambiar   Nagesh
| music           = Viswanathan–Ramamoorthy
| cinematography  = A. Vincent P. N. Sundaram
| editing         = C. P. Jambulingam Vijaya Combines Productions Vijaya Combines Productions
| released        = 14 January 1965
| runtime         = 147 mins
| country         = India
| gross           =   1.7 Crores Tamil
}}
 Indian Tamil Tamil film, Vaali and Alangudi Somu.
 Adimai Pen four years later. The film celebrated Silver Jubilee in 7 theatres and one of the another film achieved an all time record, which broke all previous records of Tamil Cinema. This movie was taken in Eastman Color

== Plot ==
Ramu (MGR) is the cowardly one – the heir to all riches of Poonjolai Jamin. He has been raised that way by his sisters husband Narendran (Nambiar). Ramu shivers at the very mention of his brother in laws name and a whiplash is Narendrans favorite form of punishment. Narendran wants to get Ramu married to Leela (Saroja Devi) but she is turned off by his cowardice. Ilango (MGR) is a jobless young man, prone to pick a fight and for this reason, the cause of trouble for his mother. Circumstances lead to Ramu and Ilango taking each others place. Ilango teaches a lesson to Narendran while Ramu learns the ways of the world. Then Ilango teaches Narendran a lesson which forces him to leave the house. Due to this, his sister scolds and he plans to leave by reveal that he was not Ramu. Then the relation of the story reveals that Ramu and Ilango are brothers. With Ramu being kidnapped and Ilango going to help his brother and revealing everything to narendran brings the film to climax.

== Cast ==
* M. G. Ramachandran as Ramu/Ilango
* B. Saroja Devi as Leela
* S. V. Ranga Rao
* M. N. Nambiar as Narendran
* K. A. Thangavelu
* Nagesh
* Rathna
* Pandari Bai

==Crew== Chanakya
*Producer: Chakrapani
*Production Vijaya Combines Productions
*Music: Viswanathan–Ramamoorthy Vaali & Alangudi Somu
*Story: D. V. Narasa Raju
*Dialogues: Sakthi T. K. Krishnasamy
*Cinematography: A. Vincent & P. N. Sundaram
*Editing: C. P. Jambulingam
*Art Direction: S. Krishna Rao
*Choreography: Chinni-Sampath
*Stunt: Shyam Sundar
*Audiography: V. Sivaram

==Production==
The film was a remake of the 1964 Telugu movie Ramudu Bheemudu which had N. T. Rama Rao in the lead role.  Tapi Chanakya who directed the Telugu version has also directed Tamil version. The makers were set deadline by producers to complete the film within 45 days to release it in Pongal. Sets were erected in all the floors at Vauhini Studios. For the scene involving both MGR who cross each other, cinematographer A. Vincent used lighting mask technique to shoot the scene.  Once during the shooting, the director found the actors struggling with the words, the director asked Sakthi’s assistant to change the words, despite MGR warning him not to. Sakthi heard this, came to him took the dialogue papers, tore them and walked away saying no one can change his dialogues. MGR who watched all this with a smile called the director and told him to go to the writer’s house to apologise. Sakthi came back and wrote dialogues that were agreeable to all.  Raja Sandow, wrestler appeared in a small role in the film, his scene in the film is based on the real life incident happened in his life during his experiences of his first stint at acting. MGR who came to know about the incident incorporated it in the film.  

== Soundtrack ==
{{Infobox album
| Name      = Enga Veettu Pillai
| Longtype  = to Enga Veettu Pillai
| Type      = Soundtrack
| Artist    = Viswanathan–Ramamoorthy
| Producer  = Viswanathan–Ramamoorthy
| Genre     = Film soundtrack Tamil
| Label     = HMV
| Length    = 31:11
}} Vaali and Alangudi Somu. The song "Naan Aanaiyittal" remains one of the famous songs of MGR.  The song "Kumari Pennin" was remixed by Srikanth Deva in Perumal (film)|Perumal (2009). 

; Tracklist 
{{track listing
| extra_column    = Singer(s)
| lyrics_credits  = yes
| total_length    = 31:11

| title1      = Naan Aanaiyitaal
| extra1      = T. M. Soundararajan
| lyrics1     = Vaali
| length1     = 05:05

| title2      = Kankalum Kaavadi
| extra2      = L. R. Eswari
| lyrics2     = Alangudi Somu
| length2     = 05:08

| title3      = Kumari Pennin
| extra3      = T. M. Soundararajan, P. Susheela
| lyrics3     = Vaali
| length3     = 05:50

| title4      = Malarukku Thendral
| extra4      = L. R. Eswari, P. Susheela
| lyrics4     = Alangudi Somu
| length4     = 04:59

| title5      = Naan Maanthoppil
| extra5      = T. M. Soundararajan, L. R. Eswari
| lyrics5     = Vaali
| length5     = 05:56

| title6      = Penn Ponaal
| extra6      = T. M. Soundararajan, P. Susheela
| lyrics6     = Alangudi Somu
| length6     = 04:13
}}

== Reception == National Award if it is remake of movie from other language. But this movie was selected for award because of its hit in commercial value.  Enga Veetu Pillai was an out of the ordinary film which increased the fan base of MGR to numerous folds. Enga Veetu Pillai became an aspiration for many would be Tamil Cinema heroes and a cult classic with MGR fans. 

==Legacy==
The film inspired several later films in Tamil which focused on the theme of look alike twins separated at birth and then crossing their paths when they grow up. The Hindu included "Naan Aanaiyittal" among lyricist Vaalis best songs in their collection, "Best of Vaali: From 1964 - 2013". 

The songs from the film has inspired film titles - Naan Aanaiyittal (1966), Kumari Pennin Ullathile (2010). The footage of song "Naan Aanaiyittal" has been intersposed in Villu (film)|Villu (2009). 

In 2009, director Badri (director)|Badris new film Thambikku Indha Ooru, was initially titled Enga Veettu Pillai but this was changed as the producer could not get the rights to use the same.   One of the song sequences from the film had 80 feet huge cut-out of MGR in the get up which appeared in Enga Veetu Pillai. 
 Vijay were speculated to play the lead role.  Vijay expressed his interest to remake the film. In April 2014, it was reported that K. Selva Bharathy would direct the remake.  However Selvabharathy refuted the rumours stating that he is not doing the remake. 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 