Chicken Hawk: Men Who Love Boys
 
{{Infobox film
| name           = Chicken Hawk: Men Who Love Boys
| image          =
| image size     =
| alt            =
| caption        =
| director       = Adi Sideman
| producer       = Adi Sideman
| writer         = Adi Sideman, Nadav Harel
| screenplay     =
| story          =
| narrator       = Barbara Adler, Mimi Turner
| starring       =
| music          =
| cinematography = Nadav Harel
| editing        =
| studio         =
| distributor    =
| released       =  
| runtime        = 55 minutes
| country        = USA
| language       = English
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}
 North American Man/Boy Love Association (NAMBLA) who discuss sexual relationships between men and boys below the age of consent. The film is an exposé on the groups controversial beliefs and their clandestine lifestyle. Sidemans evenhanded approach provides the audience with an insight into the group members psyche. The film has drawn attention for its unique approach: letting its subjects, the NAMBLA Members, incriminate themselves in a public forum. Since its release, the film has been screened for the FBI, university criminology departments and other law enforcement agencies. 

The term "Chickenhawk (gay slang)|chickenhawk" is used in gay slang to refer to an older man who chases after younger men.

==Synopsis==
The film describes the organization and its history. It presents a series of interviews with NAMBLA members who describe their feelings towards boys and justifications for such feelings. 
Scenes in the movie include a group of NAMBLA members participating in the 1993 March on Washington for Lesbian, Gay and Bi Equal Rights and Liberation. NAMBLA members argue for NAMBLAs inclusion in the gay rights movement, to the disapproval of other attendees. 
Poet and free speech advocate Allen Ginsberg, NAMBLAs most famous member and defender, appeared in the documentary and read a "graphic ode to youth". 

Scenes in the movie include following 4 outspoken NAMBLA members: Leyland Stevenson, Renatto Corazzo, Peter Melzer and Chuck Dodson.

==Release and reception==
The film was released to critical acclaim.  The premiere at the New York Underground Film Festival was met with fanfare and was covered by national news organizations as well as shock jocks like Howard Stern.

The film has been well received by anti NAMBLA groups such as "Straight Kids USA" and "National Traditionalist Caucus," both of which were represented in the film. 
Tom McDonough, Straight Kids USAs 47-year-old founder, said, "We feel everybody should see this movie because it exposes NAMBLA for all the evil they are."      

Don Rosenberg, 46, of the New York-based National Traditionalist Caucus said, "We thought the movie was very fair. I think Adi did a very good job of letting Leyland Stevenson (the films central character) and his cohorts hang themselves."    

In this New York Newsday review (article not archived but copied),    the reviewer addressed the controversy: "To say Sideman has taken a hands-off position is a horrific pun, but its also true, and wise. It would have been too easy to become strident, had he set out to make an agitprop piece about the evils of pedophilia. So he lets NAMBLA bury itself. And the organization obliges." Sideman is later quoted: ″Im not a supporter of NAMBLA″, says the filmmaker in a tone of voice that suggests the statement should be obvious.

Since its release, the film has gone on to screen for psychology, sociology, and criminology departments throughout the USA and has also been screened for the FBI.   

Several reviews of the film are compiled here. 

Chicken Hawks distributor, Stranger than Fiction, was run by Todd Phillips, who founded the New York Underground Film Festival, and later on went to produce The Hangover, and Due Date.    

==See also==
 
*Age of consent reform

==References==
 

==External links==
* 
* 
* 
* 
* , Review published in The New Republic
* 
* 
* 

 
 
 
 
 
 
 
 
 