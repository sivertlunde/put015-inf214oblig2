Első kétszáz évem
{{Infobox film
| name           = Első kétszáz évem
| image          = 
| caption        = 
| director       = Gyula Maár
| producer       = József Bajusz
| writer         = Gyula Maár Pál Királyhegyi
| starring       = Zoltán Bezerédy
| music          = 
| cinematography = Iván Márk
| editing        = Júlia Sívó
| distributor    = 
| released       =  
| runtime        = 104 minutes
| country        = Hungary
| language       = Hungarian
| budget         = 
}}

Első kétszáz évem is a 1985 Hungarian drama film directed by Gyula Maár. It was entered into the 36th Berlin International Film Festival.   

==Cast==
* Zoltán Bezerédy as Királyhegyi Pál (as Bezerédi Zoltán)
* Anna Kubik as Maud
* László Márkus as Krausz
* Béla Both as Krausz
* Jirí Adamíra as Filmproducer
* Mari Törőcsik as A filmcézázár felesége
* Ferenc Paláncz as Surányi testvér
* Gábor Nagy (actor)|Gábor Nagy as Nyilas tiszt
* György Kézdi as Henrik (as György Kézdy)
* László Csákányi as Kondor
* Kati Sír as Nyilas pártszolgálatosnő
* János Gálvölgyi as Német tiszt
* Gyula Benkő as Kegyelmes úr
* Endre Harkányi as Ernő
* Péter Andorai as Betörő

==References==
 

==External links==
* 

 
 
 
 
 
 
 