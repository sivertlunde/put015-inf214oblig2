Land Ho!
{{Infobox film
| name           = Land Ho!
| image          = Land Ho! poster.jpg
| alt            = 
| caption        = 
| director       = {{plainlist|
* Martha Stephens Aaron Katz
}}
| producer       = {{plainlist|
* Mynette Louie
* Sara Murphy
* Christina Jennings
}}
| writer         = {{plainlist|
* Martha Stephens
* Aaron Katz
}}
| starring       = {{plainlist|
* Paul Eenhoorn
* Earl Lynn Nelson
* Karrie Crouse
* Elizabeth McKee
* Alice Olivia Clarke
* Emmsjé Gauti
}}
| music          = Keegan DeWitt
| cinematography = Andrew Reed
| editing        = Aaron Katz
| studio         = {{plainlist|
* Gamechanger Films
* Syncopated Films
* Max Cap Productions
}}
| distributor    = Sony Pictures Classics
| released       =  
| runtime        = 95 minutes
| country        = United States Iceland
| language       = English
| budget         = 
| gross          = $727,594 
}} adventure comedy Aaron Katz.   The film made its world premiere at 2014 Sundance Film Festival on January 19, 2014.  It also screened at the 2014 Tribeca Film Festival, Los Angeles Film Festival, Nantucket Film Festival, Locarno International Film Festival, and BFI London Film Festival . 

Sony Pictures Classics acquired worldwide distribution rights to the film after its world premiere at the Sundance Film Festival.  
 2015 Independent Spirit Awards.

==Plot==
A pair of ex-brothers-in-law set off to Iceland in an attempt to reclaim their youth through Reykjavík nightclubs, trendy spas, and rugged camp-sites.

==Cast==
* Paul Eenhoorn as Colin
* Earl Lynn Nelson as Mitch
* Karrie Crouse as Ellen
* Elizabeth McKee as Janet
* Alice Olivia Clarke as Nadine
* Emmsjé Gauti as Glow Stick Guy

==Production== Blue Lagoon. Red One cameras to better capture the actors natural mannerisms and reactions, and was edited in just six weeks. A mere one year and three days elapsed between the films conception and its Sundance premiere. 

The film was executive produced by David Gordon Green, as well as Julie Parker Benello, Dan Cogan, Geralyn Dreyfous, and Wendy Ettinger of Gamechanger Films. It is the first feature to be financed by Gamechanger Films, an equity fund dedicated to financing features directed and co-directed by women. 

==Promotion==
The first trailer of the film was released on May 16, 2014. 

==Reception== 2015 Independent Spirit Awards and has received mostly positive reviews from critics. 

Review aggregator Rotten Tomatoes reports that 78% of 77 film critics have given the film a positive review, with a rating average of 6.6 out of 10. The sites consensus reads "Gently amusing and agreeably modest in scale, Land Ho! uses its stars warm chemistry to impart its poignant, quietly effective message."  On Metacritic, which assigns a weighted mean rating out of 100 reviews from film critics, it has an average score of 68 from 30 reviews. 

In her Sundance round-up, Manohla Dargis of The New York Times called the film "Delightfully funny...By turns playful and wistful, the movie is anchored by its irresistible lead performances....The filmmakers adroitly skirt both the cute and the condescending — the default registers of too many movies about geezers — to offer a dual portrait of gloriously alive men who just happen to be old." 

Amy Taubin of Film Comment said, "The scenery is spectacular, the women they encounter are all marvelously independent, and Eenhoorn and Nelson make a hilarious, endearingly ribald odd couple. A serious film about loneliness, loss, and aging, it is also filled with joie de vivre from beginning to end (and a very cheeky ending it is)." 

Justin Chang of Variety (magazine)|Variety said that "Paul Eenhoorn and Earl Lynn Nelson give pitch-perfect performances in this gently elegiac road comedy from helmers Martha Stephens and Aaron Katz." 

Todd McCarthy in his review for The Hollywood Reporter praised the film by saying that "A gently amusing odyssey with two old gents on the road in Iceland." 

Chris Cabin of Slant Magazine, praised the direction by saying that "That the filmmakers consistently catch the nuances of character that bind the two men to each other, rather than simply tracing the pros and cons of their dispositions, is what gives the film its melancholic yet vibrant resonance." 

Dan Fienberg of HitFix said, "Its a funny and moving film about aging, but its also a wacky journey across Iceland with two characters who are instantly likable and ultimately quite lovable." 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 