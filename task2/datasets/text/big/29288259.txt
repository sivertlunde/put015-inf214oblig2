White Gold (1927 film)
{{Infobox film
| name           = White Gold
| image          =
| caption        =
| director       = William K. Howard
| producer       = Cecil B. DeMille
| based on       =  
| writer         = John Farrow (titles)
| starring       = Jetta Goudal
| cinematography = Lucien Andriot
| editing        = Jack Dennis
| distributor    = Producers Distributing Corporation
| released       =  
| country        = United States
| language       = Silent film (English intertitles)
}}

White Gold is a 1927 silent film dramatic western produced and distributed by Cecil B. DeMille and directed by William K. Howard. 

==Cast==
*Jetta Goudal as Dolores Carson Kenneth Thomson as Alec Carson
*George Bancroft as Sam Randall George Nichols as Carson, Alecs Father
*Bob Perry as Bucky ONeill( billed as Robert Perry) Clyde Cook as Homer

==Critical reception==
Mordaunt Hall of the New York Times described the film as an "interesting production" that also had "marked simplicity" in terms of its story.  Hall also said that "but for some repetitions, a few accentuated actions and instances of forced comedy,   would be one of the really great productions."  The Ottawa Citizen said that, because of a new scripting technique employed by William Howard, "the film more closely approaches realism than anything ever before attempted in motion pictures."  Philip K. Scheuer of the Los Angeles Times called it a "distinguished film" that "employed sound by suggestion--creaking rockers, ticking clocks, the click of poker chips". 

==References==
;Notes
 

;Bibliography
*  
*  

==External links==
*  

 

 
 
 
 
 
 


 
 