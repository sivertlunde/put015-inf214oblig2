Buddy's Beer Garden
{{Infobox Hollywood cartoon
| series = Looney Tunes (Buddy (Looney Tunes)|Buddy)
| image =  
| caption = Earl Duval
| story_artist = Jack King Tish Tash Jack Carr Bernice Hansen (both uncredited) Norman Spencer
| producer = Leon Schlesinger Leon Schlesinger Productions
| distributor = Warner Bros. The Vitaphone Corporation
| release_date = November 1933 (USA)
| color_process = Black-and-white
| runtime = 7 minutes
| movie_language = English
| preceded_by = Buddys Day Out (1933)
| followed_by = Buddys Show Boat (1933)
}}
 animated short Warner Bros. cartoons directed by him, and one of only three Buddy shorts. Musical direction was by Norman Spencer.

==Summary== Cookie neatly prepares several pretzels, which then are salted by the same little dachshund, and carried thence away. The tongue sandwiches offered as part of the bars free lunch sing & lap up Mustard (condiment)|mustard; an impatient patron (presumably the same brute who serves as the villain in later shorts, such as Buddys Show Boat and Buddys Garage) demands his beer, which he instantly gulps down upon its arrival.

All present take part in "Its Time to Sing Sweet Adeline Again": some sing, one patron plays his spaghetti as though the noodles were strings on an harp, Buddy makes an instrument out of his steins, &c. Cookie comes around, offering cigars and cigarettes to the patrons, one of whom, the same impatient brute as before, accepts, but not before freshly stroking the girls chin. Cookie performs an exotic dance for the entire beer garden, and is joined by the selfsame patron, & a formerly stationery piano. The film goes on: Buddy whistles "Hi Lee Hi Lo", tossing beer from one mug to another, preparing sandwiches, clearing tables.

As a final treat for his customers, Our Hero introduces a lady singer (who bears a striking resemblance to  ; the dress tears, & the throaty performer, now grounded, is revealed to be a cross-dressed Buddy. Pleasantly embarrassed, Buddy stalks away, waving blithely to all present; in the final shot, we see that the bird cage strapped to Buddys posterior (there to replicate the voluptuousness of his singing persona), in fact houses an exotic bird, which shows itself to have a voice & nose like those of Jimmy Durante, as well as a saying: "Am I mortified!"

==Modern releases==
The cartoon is available on the  . Along with Buddys Day Out and Buddys Circus, it is one of only three Buddy shorts thus honored.

==References==
 

==External links==
*  
*  
 

 
 
 
 