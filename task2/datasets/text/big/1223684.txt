Veer-Zaara
 
 
{{Infobox film
| name           = Veer-Zaara
| image          = veerzaara.jpg
| caption        = Theatrical release poster
| director       = Yash Chopra
| producer       = Yash Chopra Aditya Chopra
| story          = Aditya Chopra
| screenplay     = Aditya Chopra
| narrator       = Shah Rukh Khan
| starring       = Shah Rukh Khan Preity Zinta Rani Mukerji Manoj Bajpayee Madan Mohan Sanjeev Kohli
| cinematography = Anil Mehta
| editing        = Ritesh Soni
| studio         =
| distributor    = Yash Raj Films
| released       = 12 November 2004
| runtime        = 192 minutes
| country        = India
| language       = Hinustani (Hindi, Urdu)
| awards         =
| budget         =   
| gross          =     
|}}
 2004 Indian romantic drama film directed by Yash Chopra under the Yash Raj Films banner. The film stars Shah Rukh Khan and Preity Zinta in the leading roles, with Rani Mukerji, Manoj Bajpayee, Kirron Kher, Divya Dutta and Anupam Kher in supporting roles. Veteran actors Amitabh Bachchan and Hema Malini make a special appearances in the film. The films story and dialogues were written by Aditya Chopra.
 conflict between India and Pakistan, this star-crossed romance follows the unfortunate love story of an Indian Air Force pilot, Squadron Leader Veer Pratap Singh and a Pakistani woman hailing from a rich political family of Lahore, Zaara Hayat Khan, who are separated for 22 years. Saamiya Siddiqui, a Pakistani lawyer, finds Veer in prison and upon listening to his story, tries to get him freed.
 Madan Mohan Most Popular National Film Awards and the Filmfare Award for Best Film, among others. The film marked Chopras return as a director after seven years post Dil To Pagal Hai, which also starred Shah Rukh Khan.

==Plot==
Most of the story is revealed as a flashback from the prison cell of Veer Pratap Singh.

The narrative begins by showing Zaara Haayat Khan (Preity Zinta), an independent, carefree, and sprightly young Pakistani girl travelling to India.Her family is of a political background and a well known family of Lahore. She is on her way to India with the ashes of her Sikh governess Bebe (a Punjabi word to denote mother or grandmother, but here used for Zaaras old governess). Before dying, Bebe (Zohra Sehgal) begs Zaara to fulfill her final wish - to take her ashes to India, to the holy Sikh city of Kiratpur Sahib|Kiratpur, and scatter them in the Sutlej river, among her ancestors. Zaara decides to carry out Bebes dying wish.
 Lodi festival and meet Veers uncle Choudhary Sumer Singh (Amitabh Bachchan) and aunt Saraswati Kaur (Hema Malini). With his uncle telling Veer that, in a dream he has seen Zaara becoming Veers wife, Veer realises he is falling in love with Zaara. Taking her to catch her train to Lahore, Veer is just waiting for the right time to tell Zaara about his feelings, but before he can do that they are met by Zaaras fiance, who has come looking for her, Raza Sharazi (Manoj Bajpayee). Just before she boards the train, Veer confesses his love to Zaara. He gets no sense of Zaaras feelings, but as she is leaving he discovers he still has one of her silver anklets. She nods for him to keep it; both believe that this is the end of the road for their relationship and that they will probably never meet again.

On reaching Pakistan, Zaara realises that she is having deep feelings of love for Veer, but that it is her duty to keep her familys honour and marry her fiancé, a wedding that will further her fathers political career. She initially tells her mother of an Indian man who is ready to give his life for her and for whom she has fallen for. Soon Zaara starts to see Veer everywhere and finally tells Shabbo (Divya Dutta), her maid and friend, that she has fallen in love with him. Shabbo calls Veer and tells him how miserable Zaara is without him. She asks him to come and take Zaara away. Veer who had told Zaara that he would give up his life for her, quits the Indian Air Force and goes to Pakistan to bring her back with him to India. Zaaras mother, Mariam Hayaat Khan (Kirron Kher), however, begs him to leave Zaara as Zaaras father, Jahangir Hayaat Khan (Boman Irani) is a high-profile politician whose reputation, and health, will be ruined if news gets out that his daughter is in love with an Indian. Veer respects this request and decides to leave for India but Raza, who is outraged by the shame Zaara has brought upon him, frames Veer and has him wrongly imprisoned on charges of being an Indian spy.

The story moves forward by 22 years and Veer now meets Saamiya Siddiqui (Rani Mukerji), who is an idealistic Pakistani lawyer, whose mission in life is to pave the path for womens empowerment in Pakistan. The Pakistani government has decided to review the cases of some Indians, but stacking the deck against Siddiqui winning her first case, she has been given the case of prisoner 786 (Veer). Many view it as an impossible task as the man has been languishing in prison and has not spoken to anyone for the last 22 years. Also, the prosecution is led by Zakir Ahmed (Anupam Kher), her ex-boss who has never lost a case.	
 786 is considered by some Muslims to be a holy number in Islam; this convinces Saamiya that God has chosen Veer for some special purpose, and she becomes even more determined to exonerate him, restore his name and identity, and return him to his country.

After the prosecution presents it case, Saamiya realises she must cross the border and find someone in Veers village who can prove Veers true identity. There, Saamiya meets Zaara, who had fled to India and has taken over running the girls school after the deaths of Veers uncle and aunt. She had thought that Veer died on his bus that ran off a cliff, killing everyone on its way to India. Saamiya takes Zaara back to Pakistan to tell the court the truth about Veers identity. The judge releases Veer from prison and apologises on behalf of Pakistan. After Veer is finally released, he and Zaara say goodbye to Saamiya and Pakistan at the Wagah border crossing, returning to their village together. The story states two lovers always find their way no matter how difficult it is and destiny will always put you together.

==Cast==
* Shah Rukh Khan as Squadron Leader Veer Pratap Singh
* Preity Zinta as Zaara Hayat Khan
* Rani Mukerji as Saamiya Siddiqui
* Manoj Bajpayee as Raza Sharazi
* Amitabh Bachchan as Choudhary Sumer Singh (Special Appearance)
* Hema Malini as Saraswati Kaur (Special Appearance)
* Divya Dutta as Shabbo
* Akhilendra Mishra as a Pakistani jailor
* Kirron Kher as Maryam Hayat Khan
* Boman Irani as Jehangir Hayat Khan
* Anupam Kher as Zakir Ahmed
* Zohra Sehgal as Bebe
* Tom Alter as a Doctor

==Music==
{{Infobox album   
| Name = Veer-Zaara
| Type = Album Madan Mohan
| Cover = Veer-Zaara_Audio_CD.jpg 
| Released =  18 September 2004 (India)
| Recorded = Feature film soundtrack
| Length =
| Label =  Yash Raj Music  Madan Mohan
| Reviews =
| Last album = Chaal Baaz (1980)
| This album = Veer-Zaara (2004)
| Next album =  -- 
}} Madan Mohan, as revised by his son Sanjeev Kohli.

Yash Raj Music also released complete background music of Veer-Zaara, this being a rarity. The CD was titled The Love Legend Themes - Instrumental.
 Madan Mohan, so there was a special poignancy to her contributions. According to Yash Chopra, upon coming for the recording, with tears in her eyes, Lata Mangeshkar told him, "Madan Mohan was like my brother. You   are like my brother. I feel I have gone back in past".  Other singers like Jagjit Singh, Udit Narayan, Sonu Nigam, Gurdas Mann, Roop Kumar Rathod, Ahmed Hussain, Mohammad Hussain and Pritha Mazumder also appear in the soundtrack. The album was the best selling Bollywood soundtrack of the year.  The album was nominated for Filmfare Best Music Director but lost to Main Hoon Na, although it managed to win IIFA Award for Best Music Director.

===List of tracks===
{{Track listing
| extra_column = Singer(s)
| lyrics_credits  = no
| title1 = Tere Liye | extra1 = Lata Mangeshkar, Roop Kumar Rathod| lyrics1 = | length1 = 05:34
| title2 = Main Yahan Hoon| extra2 = Udit Narayan| lyrics2 = | length2 = 04:57
| title3 = Aisa Des Hai Mera| extra3 = Lata Mangeshkar, Udit Narayan, Gurdas Mann, Pritha Mazumder| lyrics3 = | length3 = 07:10
| title4 = Yeh Hum Aa Gaye Hain Kahan| extra4 = Lata Mangeshkar, Udit Narayan| lyrics4 = | length4 = 05:45
| title5 = Do Pal| extra5 = Lata Mangeshkar, Sonu Nigam| lyrics5 = | length5 = 04:27
| title6 = Kyon Hawa| extra6 = Lata Mangeshkar, Sonu Nigam| lyrics6 = | length6 = 06:14
| title7 = Hum To Bhai Jaise| extra7 = Lata Mangeshkar| lyrics7 = | length7 = 04:19
| title8 = Aaya Tere Dar Par| extra8 = Ahmed Hussain, Mohammad Hussain, Mohd. Vakil| lyrics8 = | length8 = 07:53
| title9 = Lodi| extra9 = Lata Mangeshkar, Udit Narayan, Gurdas Mann| lyrics9 = | length9 = 06:55
| title10 = Tum Paas Aa Raahein Ho| extra10 = Lata Mangeshkar, Jagjit Singh| lyrics10 = | length10 = 05:12
| title11 = Jaane Kyun| extra11 = Lata Mangeshkar| lyrics11 = | length11 = 05:16
}}

==Production==
Yash Chopra returned to directing after seven years, his last directed film being Dil To Pagal Hai. The film was originally going to be called Yeh Kahan Aa Gaye Hum, a name taken from the title of a song in the movie Silsila (film)|Silsila (1981). Chopra, eventually decided on Veer-Zaara as a title. However, one of the songs in the film is called "Yeh Hum Aa Gaye Hain Kahan", a twist on the proposed title.

Veer Zaara was Chopras third consecutive film to feature Shahrukh Khan in the lead role. Originally, Chopra had intended Aishwarya Rai to play the supporting role of Sammiya Siddiqui, however she was replaced by Rani Mukerji. Chopra later stated that he had cast Zinta in the lead role, as he wanted to transform her acting persona from being westernised to being versatile, in being able to play a Punjabi girl. The casting of the movie helped the film garner a larger hype prior to its release.

==Reception==
Veer-Zaara opened in theatres on 12 November 2004. It grossed over   worldwide. 

The film collected   in India, becoming An All Time Blockbuster of the year.  In its first week it grossed  , an opening week record which was held until 2005. 

The film was a success not only in India and Pakistan, but also overseas, notably in United Kingdom, Germany, France, South Africa, Canada and the United States. It earned   in the overseas markets, making it 2004s top-grossing Bollywood production overseas and was declared an all time blockbuster.   

When the three leading actors of the film, Khan, Zinta and Mukerji, visited the Virgin Megastore in the UK, over 5,000 fans thronged the store. Apart from that, it was screened at the Berlin Film Festival, where it was received well.  In February 2005, it was featured in the issue of the National Geographic Magazine in an article about Bollywood.  On 26 April 2006, Veer-Zaara had its French premiere at The Grand Rex, the biggest theatre in Paris. It is the first Hindi film to premiere in such a large and luxe venue.  In September 2007, a book based on the making of the film, titled They said it, was released. The book contains testimonials from members of the film’s cast and crew and follows the production stages of the film. 

===Critical reaction===
Veer-Zaara received positive reviews from critics. Aakash Gandhi of Planet Bollywood gave the film a 9.5/10 rating and said, "Love stories come and go, films become hits and flops; but eternal love stories and immortal films are what stay alive in the hearts of movie lovers. Veer-Zaara is the majestic tale of two people, whose love for one another crosses into the realm of inspirational devotion. Veer and Zaara…two names detached by the wraths of religion…two entities divided by military and national borders…two forms estranged by miles of expansive distance…one soul joined by sacrificial love…one existence…Veer-Zaara.  S.Mahapatra of smashits.com observed, "This emotional love story is not only a serious entertainer but also one which contains a message for the public...a message to strengthen the bond of love between India and Pakistan."  Subhash K. Jha of glamsham.com stated, ""Veer-Zaara" is a very simple story of immense nobility and idealism. Its contours are fleshed out with the most precious colours of life to complete a picture that is at once symmetrical and sublime, sweet, tender and yet secreting a core of strength and conviction that takes it beyond the conventional romantic musicals."  Giving the film 4 out of 5 stars, Nikita Desai of apunkachoice.com wrote, "Veer Zaara is a movie that will appeal to many. Although it dabbles in drama at the cost of realism, the movie does pluck a few strings at heart with a story that comes as a refreshing change in these times of sexually explicit flicks." 

The film was also acclaimed overseas. Manish Gajjar of BBC commented, "Veer Zaara has a great storyline with some unpredictable twists and emotions, keeping you engrossed throughout."  BBC guest reviewer Jay Mamtora said, "Veer Zaara is an intense, emotionally charged, mature love story that never loses focus." He remarked, "...its a very sensible and mature film that has the makings of a classic regardless of its fate at the box office."  Derek Elley of Variety (magazine)|Variety wrote, "Though it doesnt quite match recent classics like "Kabhi Khushi Kabhie Gham..." in sheer technique and production sheen, in-depth star casting and thorough entertainment values make this a must-see for Bollywatchers.  LilAni of bollywhat.com stated, "Veer-Zaara evades description; the viewer must simply experience it for herself  . To watch it is to fall in love with the characters. You will emerge from the theatre feeling like you have just immersed yourself in a different, more magical world." 

==Awards==

===Filmfare Awards===
Winner: Best Film Best Dialogue - Aditya Chopra Best Story- Aditya Chopra Best Lyricist- Javed Akhtar for "Tere Liye"

Nominated: Best Director - Yash Chopra Best Actor - Shahrukh Khan Best Actress - Preity Zinta Best Supporting Actor - Amitabh Bachchan Best Supporting Actress - Rani Mukerji Best Supporting Actress - Divya Dutta Best Lyricist - Javed Akhtar for "Main Yahaan" Best Lyricist - Javed Akhtar for "Aisa Des Hai Mera" Best Music Director - Madan Mohan Best Male Playback - Sonu Nigam for "Do Pal" Best Male Playback - Udit Narayan for "Main Yahaan Hoon"

===IIFA Awards=== Best Picture - Aditya Chopra  Best Director - Yash Chopra Best Actor - Shahrukh Khan Best Supporting Actress - Rani Mukerji Best Music Director - The Late Madan Mohan Best Story- Aditya Chopra

===National Film Awards=== Best Popular Film Providing Wholesome Entertainment

===Stardust Awards=== Star of the Year - Female - Preity Zinta
* Stardust Best Supporting Actress Award - Kirron Kher
Best Actor - Shahrukh Khan

===Star Screen Awards=== Best Film Best Actor - Shahrukh Khan Best Dialogue - Aditya Chopra Best Story - Aditya Chopra Jodi No.1 - Shah Rukh Khan & Preity Zinta Best Supporting Actress - Rani Mukerji (Nominated)

===Zee Cine Awards=== Best Film - Yash Chopra & Aditya Chopra (Producers) Best Director - Yash Chopra Best Actor - Shahrukh Khan Best Supporting Actress - Divya Dutta

===Bollywood Movie Awards=== Best Film - Yash Chopra & Aditya Chopra Best Director - Yash Chopra Best Actor - Shahrukh Khan Best Music - Late Madan Mohan Best Lyrics - Javed Akhtar - (Tere Liye...)

===Global Indian Film Awards=== Best Actor - Shahrukh Khan Best Supporting Actress - Divya Dutta Best Story - Satish Babariya

==DVD release== 
On 6 June 2005 Yash Raj Films released the DVD of Veer-Zaara on a Collectors Double DVD Pack, with disc one containing the original film and disc two having special features, e.g. making of the songs (with little original melodies), interview with Yash Chopra, premier, deleted scenes, trailers and promo and deleted song "Yeh Hum Aa Gaye Hain Kahan".

Disc 1 (the film) has one audio track in Hindi, and eleven subtitle tracks in English / Arabic.

The film was released on Blu-ray in December 2009.

==References==
 

==External links==
*  
*  

 
 
  

 
 
 
 
 
 
 
 
 
 
 
 