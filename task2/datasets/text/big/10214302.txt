August (2008 film)
{{Infobox film
| name = August
| image = Augustposter.jpg
| image_size = 
| caption = US Theatrical release poster
| director = Austin Chick
| producer = Josh Hartnett David Guy Levy Charlie Corwin Clara Markowicz Elisa Pugliese
| writer = Howard A. Rodman
| narrator = Adam Scott Emmanuelle Chriqui
| music = Nathan Larson
| cinematography = Andrij Parekh
| editing = Pete Beaudreau
| studio = Original Media Periscope Entertainment 
| distributor = First Look Studios
| released =  
| runtime = 88 minutes
| country = United States
| language = English
| budget = $3.4 million
| gross = $12,636 
}}
August is a 2008 American drama film directed by Austin Chick and presented by 57th & Irving. The screenplay by Howard A. Rodman focuses on two brothers, ambitious Dot-com company|dot-com entrepreneurs attempting to keep their company afloat as the stock market begins to collapse in August 2001, one month prior to the 9/11 attacks.
 2008 Sundance Film Festival.

==Plot==
Tom and Joshua Sterling are two brothers, whose Internet start-up, Landshark, is as hot as a New York City summer – only this is the summer of 2001, their company is in lock up, its stock price is plunging and, in a few weeks, the world will change forever.
 67 Camaro convertible and hangs out at a new club called Bungalow 8. Tom Sterling is a true showman, a demigod in a cult – and culture – of personality.
   
== Cast ==
* Josh Hartnett as Tom Sterling
* Naomie Harris as Sarah Adam Scott as Joshua Sterling
* Robin Tunney as Melanie Hanson
* Andre Royo as Dylan Gottschalk
* Emmanuelle Chriqui as Morella Sterling
* Laila Robins as Ottmar Peevo
* Caroline Lagerfelt as Nancy Sterling Alan Cox as Burton
* David Bowie as Cyrus Ogilvie
* Rip Torn as David Sterling

== Festival screenings ==
{| class="wikitable"
|- style="text-align:center;"
! colspan=3 style="background:#B0C4DE;" | 
|- style="text-align:center;"
! style="background:#ccc;"| Festival
! style="background:#ccc;"| Section
! style="background:#ccc;"| Screening Dates
|- Sundance Film Festival 
| Spectrum  
| January 22, 2008 January 23, 2008 January 24, 2008 January 26, 2008
|- Seattle International Film Festival   Contemporary World Cinema   
| May 29, 2008 June 2, 2008
|- Brooklyn International Film Festival   Narrative Feature Films     May 31, 2008 June 6, 2008  
|- Karlovy Vary International Film Festival 
| Horizons   
| July 7, 2008
|- Filmfest Oldenburg|Oldenburg International Film Festival  International Section     September 11, 2008
|- Flanders International Film Festival Ghent   World Cinema    October 10, 2008
|-
| Bahamas International Film Festival 
| Spirit Of Freedom: Dramatic   December 8, 2008
|}

==Critical reception==
As of October 13, 2008, the review aggregator Rotten Tomatoes reported that 35% of critics rated the film positively based on 23 reviews, with a consensus that "Josh Hartnett puts in a well-intentioned performance but overall, August only superficially explores its dotcom-burst setting."  Metacritic reported the film had an average score of 39 out of 100 based on 10 reviews, indicating a generally negative response. 

== References ==
 

==External links==
* 
* 
* 
*  
*  at the Movie Review Query Engine
*  (ContentFilm)

 
 
 
 
 