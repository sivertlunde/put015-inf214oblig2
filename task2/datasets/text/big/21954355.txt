Killer Party
{{Infobox film
| name = Killer Party
| image =
| caption =
| director = William Fruet
| producer = Michael Lepiner
| writer = Barney Cohen Martin Hewitt Paul Bartel John Beal John Lindley
| editing = Eric Albertson
| distributor = Metro-Goldwyn-Mayer
| released =  
| runtime = 91 mins.
| country = United States English
| budget =
| box office =
}}
 horror comedy film released in 1986 in film|1986.  The movie was directed by William Fruet. This film is the latest pre-May 1986 MGM movie to be owned of Turner Entertainment since 1986 (and then owned by Warner Bros. since the 1996 Time Warner-Turner merger)

==Plot==
Three friends, Vivia, Jennifer, and Phoebe, decide to pledge a sorority.  The night of the initiation, Vivia (Sherry Willis-Burch) plays an elaborate prank on the sorority sisters as the three girls are being initiated through a hazing ritual.  Subsequently, all three girls are chosen to join the sorority, and Vivia learns that her prank was the only reason for her acceptance. Now she must gimmick up a traditional April Fools party that the sorority is hosting for a fraternity.  The party is to be held at an old abandoned frat house where a young man named Allan was killed 22 years prior. Jennifer (Johnson), is bothered by this house, but Phoebe (Wilkes) and the guy she is interested in (Hewitt), convince her to go along with the party plans.  Unfortunately for the friends and everyone else at the party, someone or something else does not want anyone in the abandoned house.

Near the end, its revealed that Jennifer who happens to be possessed by the demon tries to kill Phoebe and Vivia with a trident, but Vivia and Phoebe escape from Jennifer and head upstairs to escape by breaking off the blocked window and escaping to the roof, but then Jennifer appears on the roof and attacks Vivia outside of the house, causing her to fall from the roof and landing on the ground in damage. Phoebe, with a sharpened wooden column, saves Vivia by impaling the possessed Jennifer with the wooden column through her chest, apparently killing both the girl and the demon. However, the film ends with the demon taking hold of Phoebe just before the police arrive and take her into custody. As bodies are removed from the house, Vivia is placed on a stretcher and taken to an ambulance where Phoebe is waiting, still under the demons control. Vivia begs not to be put in the ambulance with Phoebe, but her screams are ignored. The movie ends when the ambulance doors are closed and the vehicle drives away as Vivias cries continue. It is left to the audience to wonder if Phoebe will kill Vivia at the Hospital or at a certain point.

==Cast==
* Sherry Willis-Burch as Vivia
* Joanna Johnson as Jennifer
* Elaine Wilkes as Phoebe Martin Hewitt as Blake
* Paul Bartel as Professor Zito
* Ralph Seymour as Martin
* Alicia Fleer as Veronica Woody Brown as Albert Harrison
* Joanna Johnson as Jennifer
* Terri Hawkes as Melanie
* Deborah Hancock as Pam
* Laura Sherman as Sandy
* Jeff Pustil as Virgil
* Pam Hyatt as Mrs. Henshaw

==Production==
The movie was originally to be called "April Fool", but the title was changed to Killer Party when Paramount announced their own horror film called April Fools Day.

Actress Sherry Willis-Burch revealed in an interview that her character "Vivia" was initially meant to be killed early in the film when she first was given the role, but was rewritten to survive until the very end of the film

== Critical reception ==
Allmovie wrote "This late straggler in the slasher-movie cycle of the early 1980s is a "neither fish nor fowl" exercise in frustration." 

== References ==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 