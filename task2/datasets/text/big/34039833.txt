The Shepherd King
{{infobox film
| name           = The Shepherd King
| image          =
| imagesize      =
| caption        =
| director       = J. Gordon Edwards William Fox
| writer         = Wright Lorimer (play) Arnold Reeves (play) Virginia Tracy (scenario)
| starring       = Violet Mersereau
| cinematography = Benny Miggins
| editing        =
| distributor    = Fox Film Corporation
| released       = November 25, 1923 (US) June 8, 1926 (Portugal)
| runtime        = 9 reels
| country        = United States
| language       = Silent film(English intertitles)
}}
The Shepherd King is a 1923 American silent film produced and distributed by the Fox Film Corporation. It is based on the 1904 Broadway play The Shepherd King by Wright Lorimer (1874-1911) and Arnold Reeves (1870-1935). Top Fox director J. Gordon Edwards shot the movie, a sumptuous production for sure, with a primarily Italian cast and he filmed in Middle Eastern countries such as Palestine, Jerusalem and Egypt. 
 The Ten Commandments (1923) which told much the same story. 

==Cast==
*Violet Mersereau - Princess Michal
*Edy Darclea - Princess Herab
*Virginia Lucchetti - Adora, Davids Adopted Sister
*Nerio Bernardi - David
*Guido Trento - Saul
*Ferrucio Biancini - Jonathan
*Sandro Salvini - Doeg (* billed Alessandro Salvini)
*Mariano Bottino - Adriel
*Samuel Balestra - Goliath
*Adriano Bocanera - Samuel
*Enzo De Felice - Ozem
*Eduardo Balsamo - Abimelech
*Americo De Giorgio - Omah
*Gordon McEdward - Egyptian Prisoner
*Ernesto Tranquili - Jesse, Father of David

==Preservation status==
All prints of The Shephard King appear to be lost as with many a Fox silent feature and if any survived past the introduction of sound in the late twenties, they most likely destroyed in 1937 Fox Films vault fire in New Jersey. The Shepherd King is now considered a lost film.   

==References==
 

==See also==
*List of lost films

==External links==
* 
* 
* 

 

 
 
 
 
 
 