Zulu Love Letter
{{Infobox film
| name           = Zulu Love Letter 
| image          = 
| caption        = 
| director       = Ramadan Suleman
| producer       = JBA Production
| writer         = 
| starring       = Pamela Nomvete Marimbe, Mangi Mpumi Malatsi, Me’Tau, Sophie Mgcina, Moola Kurt Egelhof
| distributor    = 
| released       = 2004
| runtime        = 105
| country        = France Germany South Africa
| language       = 
| budget         = 
| gross          = 
| screenplay     = Ramadan Suleman, Bhekizizwe Peterson
| cinematography = Manuel Teran
| editing        = Jacques Comets
| music          = Zim Ngqawana
}}

Zulu Love Letter is a 2004 film.

== Synopsis ==
Thandeka, a young black journalist, lives in fear of Johannesburg’s past. She’s so troubled that she can’t work, and her relationship with Mangi, her 13-year-old deaf daughter, goes from bad to worse. One day Me’Tau, an elderly woman, arrives at the newspaper’s office. Ten years earlier, Thandeka witnessed the murder of Me’Tau’s daughter, Dineo, by the secret police. Me’Tau wants Thandeka to find the murderers and Dineo’s body so that the girl can be buried in accordance with tradition. What Me’Tau couldn’t know is that Thandeka has already paid for her knowledge, for having dared stand up to the apartheid system run by the whites. Meanwhile, Mangi secretly prepares a Zulu love letter, four embroidered images representing solitude, loss, hope, and love, as a final gesture towards her mother so that she won’t give up the fight.

== Awards ==
* Cartago 2004
* Mons 2005
* FESPACO 2005
* Cape Town World Cinema 2005
* Angers 2005

== References ==
 

== External links ==
*  

 
 
 
 


 