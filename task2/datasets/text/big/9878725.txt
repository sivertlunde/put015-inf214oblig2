School of Champions
{{Infobox film
| name           =School of Champions
| image          =School of Champions.jpg
| image_size     =
| caption        =
| director       =Ralph Pappier
| producer       =
| writer         =E. Escobar Bavio, Homero Manzi
| narrator       =
| starring       =
| music          =Juan Ehlert
| cinematography =Pablo Tabernero 	
| editor       =José Cañizares, O. Viloni 	
| distributor    =
| released       =19 December 1950
| runtime        =95 minutes
| country        = Argentina Spanish
| budget         =
}}
 1950 Argentina|Argentine adventure drama film
directed by Ralph Pappier, and starring George Rigaud, Silvana Roth, and Pedro Quartucci.   It won the Silver Condor Award for Best Film, given by the Argentine Film Critics Association in 1951 for the best picture of the previous year. 

==Cast==
*George Rigaud as Alexander Watson Hutton
*Silvana Roth as Margaret Budge
*Pedro Quartucci
*Enrique Muiño as Faustino Sarmiento
*Enrique Chaico		
*Carlos Enríquez
*Héctor Coire		
*Gustavo Cavero	
*Pablo Cumo		
*Hugo Mújica	
*Eduardo Ferraro 		
*Francisco Ferraro 		
*Marcos Zucker 			
*Warly Ceriani 		
*Oscar Villa

==References==
 

==External links==
*  
 

 
 
 
 
 
 
 
 
 