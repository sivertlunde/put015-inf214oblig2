A Double Life
 
{{Infobox film
| name           = A Double Life
| image          = A Double Life poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = George Cukor
| producer       = Michael Kanin
| writer         = Ruth Gordon Garson Kanin
| starring       = Ronald Colman Signe Hasso Edmond OBrien
| music          = Miklós Rózsa	
| cinematography = Milton R. Krasner	
| editing        = Robert Parrish
| distributor    = Universal International Pictures
| released       =  
| runtime        = 104 minutes
| country        = United States
| language       = English
| budget         =
|}}
A Double Life is a 1947 film noir which tells the story of an actor whose mind becomes affected by the character he portrays. The movie starred Ronald Colman and Signe Hasso. It was directed by George Cukor and written for the screen by Ruth Gordon and Garson Kanin. 

==Plot==
Celebrated stage actor Anthony John (Ronald Colman) has driven away his actress wife Brita (Signe Hasso) with his erratic temper. However, they star together in the play Othello. Gradually, his portrayal of a jealous murderer undermines his sanity, and he kills his mistress, Pat Kroll (Shelley Winters).

==Cast==
* Ronald Colman as Anthony John
* Signe Hasso as Brita
* Edmond OBrien as Bill Friend
* Shelley Winters as Pat Kroll Ray Collins as Victor Donlan
* Philip Loeb as Max Lasker
* Millard Mitchell as Al Cooley
* Joe Sawyer as Pete Bonner
* Charles La Torre as Stellini
* Whit Bissell as Dr. Stauffer

==Background==

===Noir analysis===
Julie Kirgo wrote that A Double Life is truly a picture of opposing forces, mirror images and deadly doubles: "Anthony John is at war with Othello, the elegant world of the theater is opposed to the squalid existence of Shelley Winters Pat Kroll, and illusion versus reality are all conveyed in opposing lights and darks of Milton R. Krasner|Krasners luminous photography." 

==Reception==

===Critical response===
When the film was released film critic Bosley Crowther lauded the film, writing, "We have it on the very good authority of Ruth Gordon and Garson Kanin, who should know—they being not only actors and playwrights but wife and spouse—that what seems a fairly safe profession, acting, is as dangerous as they come and love between people of the theatre is an adventure fraught with infinite perils. Especially is it risky when an actor takes his work seriously and goes in for playing "Othello." Then handkerchiefs and daggers rule his mind. At least, that is what is demonstrated in a rich, exciting, melodramatic way in the Kanins own plushy production...George Cukor, in his direction, amply proves that he knows the theatre, its sights and sounds and brittle people." 

Critic Jerry Renshaw wrote, "A Double Life is an unusually intelligent, literate noir that is a classy departure from the pulpy "B" atmospherics often associated with the genre. Keep an eye out for Paddy Chayefsky and John Derek in minuscule bit parts." 

===Accolades===
Academy Awards Best Actor in a Leading Role – Ronald Colman (winner) Best Music, Scoring of a Dramatic or Comedy Picture – Miklós Rózsa (winner) Best Director – George Cukor (nominee) Best Writing, Original Screenplay – Garson Kanin and Ruth Gordon (nominees)
Golden Globes Best Actor – Motion Picture Drama – Ronald Colman (winner)

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 