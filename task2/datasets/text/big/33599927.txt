With Love, Delhi!
{{Infobox film
| name           = With Love, Delhi!
| image          =
| alt            =  
| caption        = Wild Film poster
| director       = Nikhil Singh
| guest director = Ashutosh Matela
| producer       = Manav Vigg Ashutosh Matela Ashish Lal
| writer         = Nikhil Singh and Ashish Lal
| starring       = Ashish Lal Pariva Pranati Tom Alter Seema Biswas Kiran Kumar
| music          = Sanjoy Chowdhury
| cinematography = Sethu Sriram
| editing        =Namrata Rao
| studio         = RedMat Reves Films Pvt. Ltd.
| distributor    = Mirchi Movies
| released       =  
| runtime        = 100 minutes
| country        = India
| language       = Hindi English
| budget         =
| gross          =
}}
With Love, Delhi! is an Indian film. It is originally made in English and dubbed in Hindi by the actors themselves. Introducing  , Baby etc.) and popular singers Shaan and Suraj Jagan.

==Plot==
Khanna (Kiran Kumar), the biggest real estate developer in Delhi, is kidnapped by Ajay (Tom Alter), who claims to be a historian. To rescue Khanna, his daughter Priyanka (Pariva Pranati) has to solve cryptic historical clues pointing to monuments of Delhi. It gets impossible for her to solve these clues. There comes Ashish (Ashish Lal), her friend and a history student, and the entire game takes a wild turn!

Are Ashish and Priyanka able to find Khanna? Is Khanna alright? Who is Ajay and why has he kidnapped Khanna? Is Ashish able to get the love of Priyanka who loves somebody else? What are the clues and can they solve them in the given tough constraints? All these questions are answered through an engaging thrilling movie – WITH LOVE, DELHI!

==Cast==
* Ashish Lal ... Ashish
* Pariva Pranati ... Priyanka
* Tom Alter ... Ajay
* Kiran Kumar ... Khanna
* Seema Biswas ... Mom of Ashish

==Sound track==
The music is composed by Sanjoy Chowdhury. The film has two Hindi songs and both are directed by Ashutosh Matela. The choreographers are Amit Verlani and Gaurav Ahlawat. The singers are Shaan, Sarika and Suraj Jagan. The lyricist is Amitabh Verma.

==References==
 

==External links==
*  

 
 
 
 
 