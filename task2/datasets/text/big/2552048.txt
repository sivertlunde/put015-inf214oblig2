Nightfall (1957 film)
{{Infobox film 
| name           = Nightfall
| image          = NightfallPoster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Jacques Tourneur
| producer       = Ted Richmond
| screenplay     = Stirling Silliphant
| based on       =  
| starring       = Aldo Ray Brian Keith Anne Bancroft
| music          = George Duning
| cinematography = Burnett Guffey
| editing        = William A. Lyon
| studio         = Copa Productions
| distributor    = Columbia Pictures
| released       =  
| runtime        = 78 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
|}} flashbacks as a device to tell the story, which was based on a 1947 novel by David Goodis. 

==Plot==
Commercial artist James Vanning (Aldo Ray) and his friend, Dr. Edward Gurston (Frank Albertson), are on a hunting and fishing trip in Wyoming.  They stop to help two men whose car has crashed. John (Brian Keith) and Red (Rudy Bond) are bank robbers, fleeing with $350,000 in loot, who dont plan on leaving any witnesses.

They murder Gurston using Vannings hunting rifle, but through luck Vanning survives.  Hes knocked out cold but is still alive. He awakens to discover the stolen money, left behind by mistake, and runs with it from the returning hoods. He gets away but loses the bag in the blizzard.
 James Gregory) who has been following the case all along.

John and Red have found the money and get the drop on the other three. The crooks double-cross one another, however, and one is shot dead. Vanning fights with the other, who is killed by a snow plow. The insurance man will clear Vanning, who is now free to marry Marie.

==Cast==
* Aldo Ray as James Vanning 
* Brian Keith as John 
* Anne Bancroft as Marie Gardner 
* Jocelyn Brando as Laura Fraser James Gregory as Ben Fraser
* Frank Albertson as Dr. Edward Gurston
* Rudy Bond as Red

==Reception==
===Critical response===
Critic Dennis Schwartz liked the film and wrote, "Splendid adaptation by Stirling Silliphant of David Goodiss 1947 novel. Jacques Tourneur (Out of the Past and I Walked with a Zombie) gets the most out of this minor film noir about a paranoid man haunted by his past, who cant fully comprehend how he got into such a tight predicament where hes being pursued by both the law and two dangerous criminals. Burnett Guffeys brilliant composite photography adds chills to the already tense narrative. His exterior daytime shots of a wintry Wyoming landscape signify danger contrasted with the neon-lit dark city night streets that signify safety." 

Critic Jay Seaver gave the film a mixed review, writing, "Nightfall isnt worried about purity of genre; it occasionally threatens to become an almost light-hearted caper movie...The storytelling is more than a bit cumbersome. Stirling Silliphants script starts shaky, with Vanning making annoyingly vague comments about not being able to remember the source of his woes, and Maries appearance in the somewhat low-class bar where she meets him almost seems out of character by the end. The direction is similarly uneven; Jacques Tourneur has some impressive items on his résumé but also a fair amount of mediocrity, and this ones somewhere in between. He gets us into and out of flashbacks smoothly, and knows when to sit back and let the actors do their thing. If the end fizzles, it might be less Tourneurs fault and more the environment he was working in - the finale really calls for a bit of blood spatter, but you just didnt get that in 1957, so the tension that has been built nicely doesnt quite have the release one might like." 

===Noir analysis===
Film critic Alain Silver makes the case that even though the films locations include bright snow cover landscapes the protagonist in the film is "typically noir."  He writes, "Despite being made near the end of the cycle, the dilemma of Nightfalls protagonist is typically noir.  Although he is a victim of several mischances, Vannings paranoia compounds these problems significantly.  Tourneur relegates those causal incidents to a flashback halfway through the film; but he does not allow them to be distorted by Vannings point-of view.  Rather, they reflect Vannings struggle to comprehend how such violent but basically simple past occurrences have put him in such dangerous and complicated present predicament." 

Writer Spencer Selby called the film a "paranoid thriller which seems to be Tourneurs return to some of the territory he explored in Out of the Past." 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 