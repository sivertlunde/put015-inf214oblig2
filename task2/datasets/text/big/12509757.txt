Dragonfly (2002 film)
 
{{Infobox film
 | name = Dragonfly
 | image = Dragonfly_movie.jpg
 | caption = Dragonfly film poster
 | director = Tom Shadyac
 | screenplay = Brandon Camp Mike Thompson David Seltzer
 | story = Brandon Camp Mike Thompson
 | starring = Kevin Costner Joe Morton Ron Rifkin Linda Hunt Kathy Bates Mark Johnson
 | music = John Debney
 | cinematography = Dean Semler Don Zimmerman Buena Vista Pictures Shady Acres Entertainment Gran Via Kalima Productions GmbH & Co. KG NDE Productions Universal Pictures   Buena Vista International  
 | released = February 22, 2002 ( USA )
 | runtime = 104 min.
 | language = English
 | budget = $60 million
 | gross = $52,322,400
 | }} doctor being contacted by his late wife through his patients near-death experience.

==Plot== Amazon area. She dies when a bus is hit by a landslide and plunges into the river below. Her body is never found by the local authorities.

Without taking time to grieve Joe returns to work. One night he is awakened by his wifes dragonfly paper weight that falls and rolls across the room. His wife always had a passion for dragonflies and even had a birthmark on her shoulder which resembled a dragonfly. Joe starts visiting Emily´s patients at the pediatric oncology in the hospital. One of his wifes patients is brought in unconscious. Joe hears the child calling his name and follows the staff who are trying to revive him without success - the child´s heart flatlines. As Joe approaches the child, suddenly the heart begins beating again.

The following afternoon Joe returns to the child who asks him if he is "Emilys Joe" and tells him she sent him back to tell Joe something. All over the room are drawings of a curvy cross, but the boy doesnt know what the symbol means. The boy tells about his near death experience, that he saw a light, and a woman showing him an image of Joe, and that the cross symbol was what he saw at the end of the rainbow.  Later, while passing by another childs room, Joe sees the same drawing. That boy immediately knows who Joe is and tells him that he must "go to the rainbow".

When Joe arrives at home, his parrot mysteriously goes into a rage breaking a pot making the same wavy cross symbol drawn in the spilled soil on the floor. Joe spots a dragonfly flying outside the window, and briefly sees Emily reaching for him outside that same window. Joes neighbour, Miriam Belmont, tries to talk him back into reality. Instead, he goes to Sister Madeline, a controversial nun who investigated near-death experiences. Sister Madeline advises Joe that Emily is indeed trying to contact him from the other side. 

The breaking point occurs at the hospital when Joe is alone with a clinically dead patient. Joe hears his wife speaking through the patient, calling his name, but no one believes him. He decides to sell his home and go on vacation. While packing away his wifes belongings, the lightbulb in the room burns out. When he returns with a new bulb, all the belongings he had packed away are suddenly back in their original places. He enters his kitchen where a map has blown open, showing the mysterious curvy cross symbol at several places. He learns from a friend that the wiggly cross is the map symbol for a waterfall. Joe remembers and finds a photo of his wife posing in front of a waterfall with a rainbow behind her.

He takes a trip to the South American area where his wife died. Joes pilot, Victor, takes him to the victims graves near a tribe village. Joe shows the photo of his wife and asks his native guides if they know where his wife is buried. They start arguing with each other that he should be brought to the village. Joes attention then shifts to the village and he runs off to it. He comes to a cliff and sees the bus far down below in the water. Joe jumps into the river and enters the semi-flooded bus, causing the bus to shift and becoming completely submerged. Joe is trapped inside but calms down when a bright glow fills the bus and his wife appears to him, reaching his hand. The events of her final hours flash before him, showing she survived the initial accident and was pulled to safety by nearby Yanomami villagers. He is then suddenly rescued out of the bus by Victor.

Joe runs to the village just to become surrounded by angry native men holding weapons. He holds up a photo of his wife. A native woman tells him they couldnt save her body but they saved her soul. Perplexed, he follows her into one of the huts, and inside is a female infant in a basket. The child his wife was carrying had survived the accident. The woman shows him a birthmark on the child in the shape of a dragonfly. As he embraces his daughter he realizes what his wife was trying to tell him.

The film ends with Joe playing with his daughter, who is now a toddler, having the same wavy blonde hair and who is the very image of his wife.

==Cast==
*Kevin Costner as Dr. Joe Darrow
*Susanna Thompson as Dr. Emily Darrow
*Joe Morton as Hugh Campbell
*Ron Rifkin as Charlie Dickinson
*Kathy Bates as Miriam Belmont
*Linda Hunt as Sister Madeline
*Jacob Vargas as Victor
*Robert Bailey, Jr. as Jeffrey Reardon

==Reception==
The film received poor reviews from critics and was a flop at the box office. The review aggregator Rotten Tomatoes reported that 7% of critics gave the film positive reviews, based on 120 reviews.   

Rotten Tomatoes ranked Dragonfly #90 on its "The 100 Worst Reviewed Films of All Time: 2000-2009" list
   

==Remake==
An Indian re-make was made of this film by the name Saaya (2003 film)|Saaya in 2003.

==References==
 

==External links==
* 
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 