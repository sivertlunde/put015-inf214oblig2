Totò sceicco
 
{{Infobox film
| name           = Totò sceicco
| image          = Totò sceicco.jpg
| caption        = Film poster
| director       = Mario Mattoli
| producer       = Romolo Laurenti
| writer         = Agenore Incrocci Marcello Marchesi Vittorio Metz Furio Scarpelli
| starring       = Totò
| music          = 
| cinematography = Mario Albertelli
| editing        = Giuliana Attenni
| distributor    = 
| released       =  
| runtime        = 95 minutes
| country        = Italy 
| language       = Italian
| budget         = 
}}

Totò sceicco is a 1950 Italian comedy film directed by Mario Mattoli and starring Totò.   

==Cast==
* Totò - Antonio Sapore, il maggiordomo
* Tamara Lees - Antinea, la regina di Atlantide
* Laura Gore - Lulù
* Lauretta De Lauri - Fatma
* Ada Dondini - La marchesa
* Kiki Urbani - La danzatrice araba
* Aroldo Tieri - Il marchese Gastone
* Cesare Polacco - Mohamed
* Arnoldo Foà - Il matto
* Mario Castellani - Il colonnello dei ribelli
* Riccardo Billi - Larabo della cella bianca
* Ubaldo Lay - Il maggiore della legione
* Carlo Duse - Un beduino
* Carlo Croccolo - Il cameriere
* Ughetto Bertucci - Ludovico, lautista

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 


 
 