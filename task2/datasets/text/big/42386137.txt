Forbidden Hours
{{Infobox film
| name           = Forbidden Hours
| image          =
| caption        =
| director       = Harry Beaumont
| producer       =
| writer         =
| starring       = Ramon Novarro Renée Adorée Dorothy Cumming Roy DArcy Edward Connelly Mitzi Cummings Alberta Vaughn
| cinematography = Merritt B. Gerstad
| editing        =
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 6 reel#Motion picture terminology|reels/4987 or 5011 ft 
| country        = United States
| language       = English
| budget         = $293,000
| gross          =
}}
Forbidden Hours is a 1928 American silent film directed by Harry Beaumont as a vehicle for Mexican-born star Ramon Novarro. It was the second of four films to pair Novarro with leading lady Renée Adorée.

==Plot==
Set in the fictitious European kingdom of Balanca, Prince Michael IV is being coerced, by his advisers, to marry a young woman of royal blood. However, he has fallen for a peasant.

==Production==
The film was shot in Los Angeles with a budget of $293,000. Working titles included The Sun King, His Night and The Loves of Louis.  The script originally contained reworked plot elements from Man in the Iron Mask but these elements were eventually discarded and the film took on a more Prussian design scheme reminiscent of the earlier Novarro success, The Student Prince in Old Heidelberg. Plot elements were allegedly adapted from the reign of Louis XIV of France.  

News sources reported that Jacqueline Gadsden, Marcelle Corday and a Shirley OHara were also in the cast.    Sven Hugo Borg may have also appeared in the film. 

As originally scripted, Prince Michael eventually marries his betrothed in order to keep peace between his nation and hers. The concluding scene showed him passing a convent where Marie now resides as a nun. This ending, which deliberately recalled Student Prince, was changed to a happier one, but press materials were still issued by the studio detailing the original ending, causing some confusion in the press. 

==Release== Capitol Theater Merry Widow themes."  The Palm Beach Post, however, was one source who praised the films scenario, design and performances.  Reviewer Anne Austin suggested in her report on the films altered ending that Renée Adorée seemed too old for the role of Marie. 

Forbidden Hours eventually made a profit of $109,000.  It was considered a commercial disappointment by the studio, however. Long thought to be lost, Forbidden Hours was discovered to have survived in 2000,  and had its first theatrical screening in seventy-three years at the Bijou Theater in Lincoln City, Oregon in 2002. 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 

 