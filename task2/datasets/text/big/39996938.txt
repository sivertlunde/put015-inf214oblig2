Paulette (film)
{{Infobox film
| name           = Paulette
| director       = Jérôme Enrico
| image          = Paulette_Schlosstheater.jpg
| caption        = Film poster in Germany
| producer       = Alain Goldman
| writer         = Bianca Olsen   Laurie Aubanel  Jérôme Enrico Cyril Rambour
| starring       = Bernadette Lafont
| music          = Michel Ochowiak
| cinematography = Bruno Privat
| editing        = Antoine Vareille
| studio         = Gaumont
| distributor    = 
| released       =  
| runtime        = 87 minutes 
| country        = France
| language       = French
}}
Paulette is a 2012 French comedy crime film directed by Jérôme Enrico.  {{Cite web|url= http://www.cinenews.be/Movies.Detail.cfm?MoviesID=12490&lang=en
|title= Paulette|publisher=cinenews.be|accessdate=2013-07-17}}  {{Cite web|url= http://www.cinemaquebec.com/movies/Quebec/31097/Paulette.html
|title= Paulette|publisher=cinemaquebec.com|accessdate=2013-07-17}}   He wrote the script in cooperation with Bianca Olsen, Laurie Aubanel and Cyril Rambour.  It has been said the plot was based on true events. 

==Plot==
Paulette and her late husband had a  .

==Cast==
* Bernadette Lafont as Paulette
* Carmen Maura as Maria
* Dominique Lavanant as Lucienne
* Françoise Bertin as Renée 
* André Penvern as Walter
* Ismaël Dramé as Léo
* Jean-Baptiste Anoumon as Ousmane
* Axelle Laffont as Agnès
* Paco Boublard as Vito
* Aymen Saïdi as Rachid
*  

==Reception==
Brendan Kelly wrote for The Gazette (Montreal)|Montreals Gazette and the The Vancouver Sun the film had "its highs and lows". {{cite web|url= http://www.montrealgazette.com/entertainment/Review+Paulette/8228160/story.html
|title=Review: Review: Paulette|author=Kelly, Brendan|publisher=The Gazette (Montreal)|date=2013-04-11|accessdate=2013-07-17}}   Carola Almsinck (kinocritics.com) wrote on the occasion of the German release "Paulette" was a "very French" film and eligible as an "amusing fairytale". 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 