A Hidden Life (film)
 
{{Infobox film
| name           = A Hidden Life
| image          = A Hidden Life Suzana Amaral.jpg
| caption        = Theatrical release poster
| director       = Suzana Amaral
| producer       = Assunção Hernandes
| writer         = Suzana Amaral
| based on       =  
| starring       = Sabrina Greve
| music          = Luiz Henrique Xavier
| cinematography = Lauro Escorel
| editing        = Verónica Sáenz
| studio         = Raiz Filmes
| distributor    = Riofilme
| released       =  
| runtime        = 95 minutes
| country        = Brazil
| language       = Portuguese
| budget         = 
| gross          = R$86,160 
}}

A Hidden Life ( ) is a 2001 Brazilian drama film directed by Suzana Amaral. 

==Cast==
* Sabrina Greve as Biela
* Eliane Giardini as Constança
* Cacá Carvalho as Conrado
* João Antônio as Dr. Godinho
* Benício Aleixo Bernardo as trucos player 3
* Neuza Borges as Joviana
* Tânia Botelho as Dona Alice
* Erasmo Xavier da Costa as trucos player #2
* João Luiz Pompeu de Pina as Cavaleiro 2
* Itamar Gonçalves as Gumercindo
* Nayara Guércio as Mazília

==Reception==
It was entered into the 24th Moscow International Film Festival.    At the 34th Festival de Brasília, it won the Best Actress Award (Sabrina Greve) and Best Sound.  It received the Best Director, Best Actress (Greve), and Jurys Special awards at the 28th Festival de Cine Iberoamericano de Huelva. 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 