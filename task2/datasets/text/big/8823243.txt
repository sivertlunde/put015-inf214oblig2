The Vulture (1981 film)
{{Infobox Film |
  name         = The Vulture|
  image              = VOULTURE_POSTER1.jpg|
  writer             = Yaky Yosha|
  starring           = Shraga Herpaz Niza Shaul Shimeon Finkel Hana Maron Andy Richman Anat Azmon|
  director           = Yaky Yosha|
  producer           = Dorit Yosha|
  movie_music        = Yoni Rechter|
  distributor        = Yaky Yosha Ltd. |
  released           = 1981 (Israel) |
  runtime            = 92 minutes |
  country      = Israel |
  language     = Hebrew |
  budget         = |
  music          = |
  awards         = |
}}
 Yaky Yoshas first war in Lebanon, dealt with the problematic immortalization industry resultant from young war casualties. The film provoked great controversy among the Israeli public, which felt it has crossed a blood-red line. The Israeli censors cut The Vulture, but when selected to represent the country at the Cannes Film Festival, it was screened uncut.

==Plot==
Boaz, a young officer, returns home from the Yom Kippur War (1973). He left for the war with two friends and returned with one dead and one badly injured. Down and out and lonely, Boaz aimlessly wanders the streets of Tel-Aviv. 
To comfort himself, Boaz goes to console his dead friends parents, only to find himself sucked into a most complex relationship with the bereaved parents. First out of courtesy, then out of cynicism, Boaz gives them all they’re missing: a poem their son allegedly wrote, false tales of heroism and some occasional snapshots.
Out of thin air Boaz erects a false monument of a dead hero out of a fairly mediocre child, who didnt get to leave much behind him.  

Before long, Boas is running a full scale immortalization industry, “manufacturing” for each bereaved family a creative, sensitive son. A soldier and a poet.

Boaz becomes romantically involved with his dead friends girlfriend, and simultaneously with the beautiful coordinator in the armys memorial department. And so, by day they serve a holy trinity of comfort and immortality and by night they are fallen angels, ménage à trois. 

Before too long, the next war breaks out. Boaz is called again for duty. Now an older, experienced officer, Boaz makes sure every single soldier in his company carries in his pocket a personal poem. Just in case.   

The Vulture represented Israel at the Cannes Festival 1981.

== External links ==
*  
 

 
 
 
 
 
 
 
 

 
 