Eyes of an Angel (film)
 


{{Infobox film
| name           = Eyes of an Angel
| image          = Eyesofanangel.jpg
| image_size     =
| caption        = Theatrical release poster Robert Harmon Michael Phillips Michael Douglas
| writer         = Robert Stitzel
| starring       = John Travolta Ellie Raab Tito Larriva Jeffrey DeMunn
| music          = Randy Edelman
| cinematography = Theo van de Sande
| editing        = Donn Cambern Zach Staenberg
| distributor    = Artisan Entertainment
| released       =  
| runtime        = 91 minutes
| country        = USA
| language       = English
| budget         =
}}
 Robert Harmon. Pulp Fiction.  According to the opening credits, the movie is based on a true story.  The film was shot entirely in Chicago.

Travolta plays Bobby Allen, a down-on-his-luck single father and recovering alcoholic whose wife died of a drug overdose.  With no job and no money, his former brother-in-law Cissy gets him a job as a money courier while Bobbys 10-year-old daughter finds a wounded fighting dog.  She takes care of the dog over her fathers objections, until Bobby is betrayed by Cissy, who blames him for his sisters death.  Desperate, Bobby steals Cissys money and flees with the girl to California, leaving the dog behind.  The dog follows them across the country to be reunited with the girl, while Cissy pursues Bobby to retrieve his money and to take the girl.

==Plot==
Partygoers gather at a mansion. A man named Bobby also enters the party. A man tries to make him leave, but Bobby claims to have an appointment with Cissy. When Cissy shows up, Bobby says he has a business opportunity for him, a dry cleaning store. Bobby notices a station wagon entering the garage. In the garage, the man in the station wagon opens the back to reveal a Doberman Pinscher, muzzled, in a small metal cage.

Baby takes money from an envelope hidden in the headboard of a bed, and buys a cigarette lighter, which she has giftwrapped.  Back at the house she decorates a misshapen cake with M&Ms. Bobby discovers the missing money, and yells at her for stealing the money.

The dog arrives at the same bridge the girl stood on a few nights ago, and picks up her scent. He follows it through the city, but finds a crying, drunken Bobby instead.  The dog leads him to the girl back at the bridge where she ecstatically greets the dog. Bobby, still crying, hugs her and apologizes.  They go back to Georges where the brothers hug.  While the girl plays outside, Bobby explains everything to George.

As the crowd quietly stares at Cissy, Bobby walks across the ring and knocks Cissy into the ring, where the doberman attacks him. It rips his coat and pants before getting a hold of Cissys neck.  Bobby stands over Cissy and tells him "its over."  When Cissy agrees, the dog lets go.  Bobby and Baby leave with the dog as Cissys goons help him up.

==Cast==
* John Travolta as Bobby
* Ellie Raab as The Girl
* Tito Larriva as Cissy
* Richard Edson as Goon
* Vincent Guastaferro as Goon
* Jeffrey DeMunn as George
* Lisa Ziegler as Georges Wife
* Rudd Weatherwax as The Dogs Handler
* Tripoli as The Dog

==Distribution== Pulp Fiction.  At that point, it was released straight to VHS format.  It was released on DVD in 2002.

==Reception==
Despite the star appeal of Travolta, actor Michael Douglas being an executive producer, and having a soundtrack from Randy Edelman, Eyes of an Angel seemed to gain little attention upon its release. Robert Harmon was, however, nominated for a Critics Award for the film at the 1991 Deauville Film Festival in France (under its original name of The Tender).

==References==
 

==External links==
* 
*  
* 

 

 
 
 
 
 
 
 
 
 
 