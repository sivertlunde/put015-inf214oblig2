The Interview (1998 film)
 
{{Infobox film
| name           = The Interview
| image          = The Interview.JPG
| image_size     =
| caption        = Film poster.
| director       = Craig Monahan Bill Hughes
| writer         = Craig Monahan Gordon Davie Tony Martin Aaron Jeffery
| music          = David Hirschfelder
| cinematography = Simon Duggan
| editing        = Suresh Ayyar
| distributor    = Sullivan Entertainment The Cinema Guild Umbrella Entertainment
| released       =  
| runtime        = 103 minutes
| country        = Australia
| language       = English
| gross          = $556,263
}} Tony Martin, and Aaron Jeffery. 

==Plot synopsis==
Eddie Rodney Fleming (Hugo Weaving|Weaving) is a quiet, nervous man who recently lost his job and family. One morning, Eddie is seized from his apartment for unknown reasons by two men claiming to be cops. They take him to headquarters and question him about a stolen car. But as tempers rise and the truth is slowly unraveled, Eddie realizes theres more to this interview than meets the eye.

==Cast==
* Hugo Weaving as Eddie Rodney Fleming Tony Martin as Detective Sergeant John Steele
* Aaron Jeffery as Detective Senior Constable Wayne Prior
* Paul Sonkkila as Detective Inspector Jackson
* Michael Caton as Barry Walls
* Peter McCauley as Detective Hudson
* Glynis Angel as Detective Robran
* Leverne McDonnell as Solicitor
* Libby Stone as Mrs Beecroft
* Andrew Bayly as Constable Prowse
* Doug Dew as Beecroft

==Awards== AFI winner Best Film, Best Original Best Performance by an Actor in a Leading Role (Hugo Weaving).

==Alternate ending==
An alternate ending of the film was featured on the DVD release. In this version, Eddie Rodney Fleming is seen hitchhiking along a desolate road. A car stops and Barry Walls (Michael Caton|Caton) offers him a ride. Fleming accepts and they drive away, followed at a distance by Steele (Martin) on his motorcycle, no doubt intending to enforce some vigilante justice.

==Box office==
The Interview grossed $556,263 at the box office in Australia. 

==Home media==
The Interview was released on DVD by Umbrella Entertainment in July 2011. The DVD is compatible with all region codes and includes special features such as audio commentary by Craig Monahan, deleted scenes, the alternate ending and cast and crew interviews.   

==See also==
*Cinema of Australia

==References==
 

==External links==
*  at the National Film and Sound Archive
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 