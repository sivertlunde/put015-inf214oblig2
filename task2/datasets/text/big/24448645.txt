Forest of the Damned 2
 
 
{{Infobox film
| name           = Forest of the Damned II: Death by Desire
| image          = Forest-of-the-Damned-II.jpg
| image_size     = 180px
| caption        = Official poster
| director       = Ernest Riera
| producer       = Mark Evans Ernest Riera Johannes Roberts
| writer         = Mark Evans Ernest Riera
| starring       = Francesca Fowler Alex Humes Rachel Freeman Sebastian Knapp Lewis Clements Sally Janman Marysia Kay Eleanor James Victoria Broom
| music          = Robert Pawliczek Bernhard Riener Arlen Figgis
| cinematography = John Raggett Rainer Antesberger
| runtime        = 86 minutes
| country        = United Kingdom
| language       = English
| budget         =
| studio          = Lonely Crow Productions
}} horror film directed by Ernest Riera.

==Plot==
 
Despite Lucys reservations, she agrees to her husband’s request to attend a wild party of lust and sexual exploration on a remote island. Their passion soon turns to terror when they discover that one of the group has disappeared. Some start to believe there is something evil lurking in the forest while others begin to suspect each other. Lucys biggest fear was that the weekend could ruin her marriage, she didnt realize it could cost her her life too.

==Cast==
*Francesca Fowler as Lucy
*Alex Humes as James
*Rachel Freeman as Sarah
*Sebastian Knapp as Richard
*Lewis Clements as Pat
*Sally Janman as Hailey
*Marysia Kay as Angel One
*Eleanor James as Angel Two
*Victoria Broom as Monica
*Jennifer-Ashley Kearn as Katie
*Jatin Mehta as Matt
*  as Rachel
*Ryan Spencer Wilson as Mark
*Cleo Mason as Young woman
*Tim Taylor as Young man
*Carl Waters as Tom
*Ryan Haysom as John
*Jenny Martin as Angel Three
*Elanor Hughs as Angel Four
*Pat Kidd as Dead body One
*David Williamson as Dead body Two
*William Crook as Dead body Three
*Jonathan Moffat as Dead body Four
*Mark Evans as Man with Ripped Throat

==Production==
  HD in Dorset, and London.

==Soundtrack==
 
The score was composed by Robert Pawliczek, Bernhard Riener and Arlen Figgis. The soundtrack features songs by Belligerence.

==Release==
 
The full length film was released in 2011 and will run as Demonic II in the United States. 
==Reception==
 
==Prequel==
 
The movie is the sequel to Forest of the Damned, which was sold in over 30 countries worldwide with Warner Brothers releasing in the U.K. the director Ernest Riera worked as producer on the film.

==References==

 

==External links==
* 
* 

 
 
 
 
 
 
 