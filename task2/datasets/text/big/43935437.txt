I Am Nancy
{{Infobox film
| name           = I Am Nancy
| image          = File:I Am Nancy.jpg
| alt            =
| caption        = DVD cover
| director       = Arlene Marechal 
| producer       = Heather Langenkamp Arlene Marechal
| writer         =
| starring       = Heather Langenkamp Robert Englund Wes Craven
| narrator       = 
| music          = Matthew Ian Cohen
| cinematography = 
| editing        = Troy Bogert
| studio         = Some Pig Productions
| distributor    = 
| released       =   }}
| runtime        = 
| country        = United States
| language       = English
}}
I Am Nancy is a 2011 American Direct-to-video|direct-to-DVD autobiography of actress Heather Langenkamp.

== Plot == Nancy in A Nightmare on Elm Street, the fandom that surrounds the franchise, and why most of it focuses on Freddy Krueger, rather than Nancy.

== Cast ==
* Heather Langenkamp
* Robert Englund
* Wes Craven

== Production ==
Langenkamp was inspired to make a documentary about herself and Nancy after a receptionist rebuffed her efforts to contact director Wes Craven.    The film was originally planned to coincide with the 25th anniversary of A Nightmare on Elm Street but was delayed for her participation in Never Sleep Again.  Langenkamp felt that documentary did not answer all the questions that she had.  Production took two years. 

== Release ==
I Am Nancy premiered at the Days of the Dead film festival. 

== Reception ==
Emilie Noetzel of Dread Central rated it 4/5 stars and wrote, "I Am Nancy is a must-see for fans of the A Nightmare on Elm Street series and of Heather Langenkamp." 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 