Miss London Ltd.
{{Infobox film
| name           = Miss London Ltd.
| image	     = Miss London Ltd. FilmPoster.jpeg
| image_size     = 
| caption        = 
| director       = Val Guest Edward Black
| writer         = Marriott Edgar Val Guest
| starring       = see cast list
| music          = Bob Busby
| cinematography = Basil Emmott
| editing        = R.E. Dearing
| studio         = Lime Grove Studios (credited as Gaumont British)
| distributor    = Gainsborough Pictures
| released       =  
| runtime        = 99 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
 directed by Val Guest and starring Ronald Shiner as Sailor Meredith and Arthur Askey as Arthur Bowden.   produced by Edward Black, Maurice Ostrer, Fred Gunn and Gainsborough Pictures.

== Plot ==
This musical comedy pöaying in wartime London, stars Arthur Askey as Arthur Bowman alias Miss London, the name of the escort agency, he inherited from his mother. Soon he is joined by his new american partner Terry Arden  (Evelyn Dall), as she inherited the other half of the Agency from her parents, who just arrived from abroad. T first thing she accomplishes is to clean up the office together with her partner. Anne Shelton) to hire. The opening sequence of the film features the latter singing "The 8.50 Choo Choo For Waterloo Choo" at Waterloo Station before she is recruited by Bowden for his agency. As usual, Ronald Shiners character of Sailor Meredith plays a decisive role.

The film features a surreal self-parodying sequence in which Bowden to gain entrance to a hotel, pretends to be the famous Arthur Askey, using some of his choice catchphrases. Other spoofs include Askey and Dall doing a routine as Fred Astaire and Ginger Rogers and, with Shiner in addition, as the three Marx Brothers.

== Cast ==
* Arthur Askey as Arthur Bowman
* Evelyn Dall as Terry Arden Anne Shelton as Gail Martin
* Richard Hearne as Commodore Joshua Wellington Max Bacon as Romero
* Jack Train as Joe Nelson Peter Graves as Captain Michael "Rory" OMore
* Jean Kent as The Encyclopedia Girl
* Ronald Shiner as Sailor Meredith
* Iris Lang
* Virginia Keiley
* Una Shepherd
* Sheila Bligh
* Noni Brooke Patricia Owens as Miss London
* Hilda Campbell_Russell as Cabaret Singer

== Soundtrack ==
*Evelyn Dall and Anne Shelton – "A Fine How Do You Do" (Words and music by Val Guest and Manning Sherwin)
*Evelyn Dall – "Keep Cool Calm and Collected" (Words and music by Val Guest and Manning Sherwin)
*Arthur Askey – "The Moth" (Words and music by Val Guest and Manning Sherwin)
*Arthur Askey – "Im Only Me" (Words and music by Val Guest and Manning Sherwin)
*Anne Shelton – "You Too Can Have a Lovely Romance" (Words and music by Val Guest and Manning Sherwin)
*Anne Shelton – "The 8.50 Choo Choo" (Words and music by Val Guest and Manning Sherwin)
*Anne Shelton – "If You Could Only Cook" (Words and music by Val Guest and Manning Sherwin)
* "My Father Was a Yes Man" (Words and music by Val Guest and Manning Sherwin)

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 