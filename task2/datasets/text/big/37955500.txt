Inkaar (1978 film)
{{Infobox film
| name           =Inkaar  
| image          = 
| image_size     = 
| caption        = 
| director       = Raj N. Sippy
| producer       = Romu Sippy
| writer         = Jyoti Swaroop (screenplay) Sagar Sarhadi (dialogue)
| narrator       =  Helen
| music          = Rajesh Roshan
| cinematography = Anwar Siraj
| editing        = Waman Bhosle, Gurudutt Shirali
| distributor    = 
| released       = 1978
| runtime        = 
| country        = India
| language       = Hindi 
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
Inkaar is a 1978 Hindi movie produced by Romu N Sippy and directed by Raj N. Sippy. the film stars Vinod Khanna, Vidya Sinha, Shreeram Lagoo, Amjad Khan and Helen Jairag Richardson|Helen. The music is by Rajesh Roshan.
 High and Low (1963)  directed by Akira Kurosawa  which itself was based on an American novel Kings Ransom (novel)|Kings Ransom (1959) by Ed McBain.
 Telugu film Dongala Veta (1979). 

==Plot==
Haridas Choudhry lives a wealthy lifestyle in Mumbai, India, along with his wife, Sonu, son, Guddu, and sister, Geeta. He had started his career as a lowly cobbler on a the corner of a busy street, but is now the owner of a shoe company. His associates want him to make shoes that wear out soon, but he refuses to do so, and would like to buy out National Shoes for 20 Lakh Rupees. He withdraws the money, but before he could undertake the transaction, Guddu gets kidnapped, and the demand from his abductors is for 20 Lakhs. Much to his relief he finds out that his servants son, Bansi, has been mistakenly abducted in place of his son. Nevertheless he decides to pay the ransom, this time with the help of Inspector Amarnath Gill, his sisters estranged boyfriend, who he had turned down as he was not wealthy enough. The money is turned over to the kidnappers, two associates, Manmohan and Preeti, are arrested, Bansi is found and returned to his dad. But the money and the real abductor, Raj Singh, is still at large - and as long as he remains at large none of them can really be safe for he has a grudge to settle against Haridas, and the missing 20 Lakhs may result in the bankruptcy of Haridas company, they may have to forfeit their family home, and Haridas may well face a jail sentence for embezzling this amount for personal gain. But Inspector catches the thief and returns the money in the end. Amazing performances by Vinod Khanna,Amjad Khan and Shreeram Lagoo and great direction. Music adds flavour to the chilling movie.

== Cast ==
*Vinod Khanna - CID Officer Amarnath "Amar" Gill
*Vidya Sinha - Geeta Chaudhry
*Shreeram Lagoo - Haridas Chaudhry
*Amjad Khan - Raj Singh Kidnapper
*Sadhu Meher - Sitaram Servant
*M.Rajan - Planeclothes Officer
*Lily Chakravarty - Sonu H. Chaudhry
*Master Rajesh - Guddu H. chaudhry
*Raju Shrestha - Bansi
*Bharat Kapoor - Manmohan Sheetal - Preeti
*Ranjita Thakur - Hema Gill
*Harish Magon -  Anil Sharma Helen - Dancer at Bar
*Keshto Mukherjee - Drunk fishing at the china creek
*Rakesh Roshan - Himself
*Gurbachchan Singh -  Bar Room Brawler

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Chhodo Yeh Nigahon Ka Ishara"
| Kishore Kumar, Asha Bhosle
|-
| 2
| "O Mungada Mungada"
| Usha Mangeshkar
|-
| 3
| "Dil Ki Kali Yun Hi Sada Khilti (Happy)"
| Mohammed Rafi
|-
| 4
| "Dil Ki Kali Yun Hi Sada (Sad)"
| Mohammed Rafi
|}
==References==
 

== External links ==
*  

 
 
 
 
 
 