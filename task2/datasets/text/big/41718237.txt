The Monitors (film)
{{Infobox film
| name           = The Monitors 
| image          = File:The Monitors film poster.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Theatrical release poster
| film name      =  Jack Shea
| producer       = Bernard Sahlins
| writer         = 
| screenplay     = Myron J. Gold 
| story          = 
| based on       =  
| narrator       = 
| starring       = {{Plainlist |
* Guy Stockwell
* Susan Oliver
* Avery Schreiber
* Sherry Jackson
* Shepperd Strudwick
* Keenan Wynn
* Ed Begley
* Larry Storch
}}
| music          = Fred Kaz William Zsigmond
| editing        = 
| studio         = {{Plainlist | Bell & Howell Productions
* Wilding Inc.  Second City Productions
}}
| distributor    = Commonwealth United Entertainment
| released       =  
| runtime        = 92 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} satirical science Second City comedy troupe  and was coproduced and financed by the Bell and Howell film-equipment manufacturing company (then based in nearby Skokie) in an effort to establish Chicago as a film production center.  It is based on the novel of the same name by Keith Laumer.

==Synopsis==
Earth has been taken over by a benign group of aliens known as the Monitors, gentlemanly figures clad in black overcoats and bowler hats. They are dedicated to suppressing humanitys propensities for violence, sex, war, and trouble, enforcing their ethos with spray cans of a pacifying gas and with television ads praising the Monitors rule—the latter featuring cameos by a variety of comedic actors, as well as bandleader Xavier Cugat and Illinois senator Everett Dirksen (who died before the films release).

A conflict with the Monitors, inspired by the outrageous antics of a street preacher (Larry Storch), leads to the flight of movie actress Barbara (Susan Oliver), who is a somewhat reluctant collaborator with the Monitors, along with free-lance pilot Harry (Guy Stockwell) and Harrys brother Max (Avery Schreiber), and their spiriting away by the "preacher", who turns out to be a leader of S.C.R.A.G., or "Secret Counter Retalitorial Group", an anti-Monitor resistance group.  After a series of vicissitudes, with Harry among the Monitors and Barbara and Max among the S.C.R.A.G. forces, the principals are reunited and, minus Barbara, fly off to Washington, D.C., in an attempt to foil a S.C.R.A.G. plot to bomb Monitor headquarters.

The Monitors, who have been aware of all these events, have decided that human beings are not worthy of their leadership, and they depart. Humanity is free to return to its violent and corrupt ways.

==Cast==
* Guy Stockwell as Harry
* Susan Oliver as Barbara
* Avery Schreiber as Max
* Sherry Jackson as Mona
* Shepperd Strudwick as Tersh Jeterax (Monitor leader)
* Keenan Wynn as The General
* Ed Begley as The President
* Larry Storch as Colonel Stutz
* Adam Arkin – cameo
* Alan Arkin – cameo
* Xavier Cugat – cameo
* Barbara Dana – cameo
* Everett Dirksen – cameo
* Stubby Kaye – cameo Jackie Vernon – cameo

==Reception==
The Monitors received mixed notices, with the New York Times reviewer Howard Thompson remarking that it "clips along with considerable verve" but that the "endless wisecracks seem none too wise or witty, or, for that matter, new".  The entry on the film in The Encyclopedia of Science Fiction, on the other hand, calls it "an oddity, which flopped badly". 

The film scholar Vivian Sobchack has noted that the "short and simple jingle dealing with the Monitors and their ability to bring happiness" used recurrently in the film serves to spoof "the incantations and sacred songs attendent to the selling of material goods and politicians". 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 