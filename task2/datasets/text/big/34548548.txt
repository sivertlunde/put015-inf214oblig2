The Secret Game (1917 film)
{{infobox film
| name           = The Secret Game
| image          = Thesecretgame-1918-newspaperad.jpg
| image_size     = 185px
| caption        = Newspaper advertisement.
| director       = William C. deMille Harry Haskin (asst director)
| producer       = Jesse Lasky
| writer         = Marion Fairfax (story, scenario)
| starring       = Sessue Hayakawa
| music          =
| cinematography = Charles Rosher
| editing        =
| distributor    = Paramount Pictures
| released       = December 3, 1917
| runtime        = 5 reels
| country        = United States Silent (English intertitles)
}}
The Secret Game is a surviving 1917 American silent film produced by Jesse Lasky and released through Paramount Pictures. It was directed by William C. deMille and starred Sessue Hayakawa. It survives complete at the Library of Congress and was released on DVD.  

==Cast==
*Sessue Hayakawa - Nara-Naru Jack Holt - Major John Northfield
*Florence Vidor - Kitty Little
*Mayme Kelso - Miss Loring
*Raymond Hatton - Mrs. Harris Charles Ogle - Dr. Ebell Smith

==References==
 

==External links==
*  
* 

 

 
 
 
 
 
 
 
 


 