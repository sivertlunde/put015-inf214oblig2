Ghair Khanooni
 
{{Infobox Film |
  name           = Gair Kanooni |
  image          = gair-kanooni.jpg |
  starring       = Sridevi Rajanikanth Govinda Kimi Katkar |
  director       = Prayaag Raaj |
  producer       = Chetana M Mody |
  music          = Bappi Lahiri |
  cinematography =  |
  editing        =  |
  released       = March 24, 1989|
  runtime        = 135 mins |
  country = India|
  language = Hindi language|Hindi| |
}} 1989 film starring Sridevi, Rajnikant, Govinda, and Kimi Katkar. The movie was an instant hit due to sridevis crowd pulling power at that time.

==Synopsis==
This movie begins in a hospital in India where Sridevi and Govinda are switched at birth.  Kader Khan, Sridevis dad, steals Govinda from the nursery after finding out that his wife gave birth to a girl. He leaves the girl in the arms of the bewildered mother who passes away from a heart attack after discovering that someone stole her baby.  As the tragedy unfolds throughout the movie, Govinda and Sridevi eventually find who out their biological parents really are. 

Both actors meet in a financial scandal and fall in love, but Sridevi resists.  Sridevi commits insurance fraud by intentionally walking in front of Govindas vehicle. She is compensated accordingly, but not before stealing his father Kader Khans watch. Though the events in the movie are tragic, they are portrayed in a comedic way.  This is supported by Sridevi pretending to be a courtesan minutes after the incident. Sridevi performs to a dance number with a beautiful coral colored Indian dress. The name of the song is, "Tum Jo Parda Rako Ge," which means, "If you keep the curtain in its place," referring to the first day Sridevi met Govinda in the financial scandal where Sridevi stole money from Govinda along with his fathers watch.

 
 
 


 