Big Shots (film)
{{Infobox film
|name=Big Shots
|image=Big Shots.jpg
|image_size=
|caption=
|director=Robert Mandel
|writer=Joe Eszterhas
|producer=Michael C. Gross Joe Medjuck
|starring=Ricky Busker Darius McCrary Robert Joy Robert Prosky Jerzy Skolimowski Paul Winfield Brynn Thayer Joe Seneca Beah Richards Ellen Geer
|music=Bruce Broughton
|cinematography=Miroslav Ondříček
|editing=William M. Anderson Sheldon Kahn Dennis Virkler Lorimar Film Entertainment
|released= 
|runtime=94 minutes
|country=United States
|language=English
|gross=$3,390,043
}}
Big Shots is a 1987 American comedy adventure film directed by Robert Mandel, starring Ricky Busker and Darius McCrary.

==Plot==
 
An 11-year-old boy from Hinsdale, Illinois named Obie Dawkins (Ricky Busker) is out fishing with his father, who tells him about the birds and the bees. All the while Obie shows interest in his dads watch, who eventually gives it to him. Later that morning Obie is in school when his mom shows up to tell him that his dad is in the hospital. The family learns that he suffered a massive heart attack, which he dies from days later. After the funeral, Obie lingers to the memory of him and his father, the day his dad gave him his watch, as he and his family tries to move forward. 
 South Side of Chicago. There, Obie is lured into a lot by some boys, attacked, and robbed of his bike and watch. One of the boys named Scam (Darius McCrary), an 11-year-old street hustler, befriends him and aids Obie in retrieving his watch. Scam takes Obie to a bar where they greet a man named Johnnie Red (Paul Winfield). Scam believes he bought the watch from Obies muggers, Johnnie Red denies it at first, but later he tells them he did have it but gave it to a man named Keegan (Robert Prosky), a slick pawn shop owner.

Scam and Obie go to visit Keegan, and it is there at his pawn shop where Obie finds his watch. But Keegan has put it on display to be sold. Obie and Scam leave promising Keegan they will get him the money. Instead of going home Obie stays the night with Scam, who lives at a hotel managed by a Miss Hanks (Beah Richards). As they are heading to Scams room a hit man named Doc (Jerzy Skolimowski), and his assistant Dickie (Robert Joy), are beating up a man trying to find another man, their target hit, who runs out the door seconds later with Doc and Dickie chasing him.

The next morning, Doc and Dickie return to the hotel having caught their mark, killed him, and stuffed his body in the trunk of their car, a dark gray colored Mercedes-Benz. Using Obie as an accomplice Scam steals the car, and uses it to drive Obie to the bank to get the money for his watch, with both unaware of the body in the trunk. After leaving the bank with the money Obie has Scam to take him by his house, just to make sure his mother and sister were okay. While there, they learn that Obies mother has reported him missing to the police. Having discovered their car stolen, Doc and Dickie go around offering a reward to anyone for its recovery. Obie and Scam return to the pawn shop to buy Obies watch from Keegan. Keegan stiffs them by taking the money and refusing to give Obie his watch. Scam attempts to get the watch himself, but Keegan stops him. Plus he make a horrible comment about Obies dad, sending him running out the pawn shop upset. Obie goes from being saddened to being angry, and Scam determines that with his fire of emotions they both are ready to really get back Obies watch.

Back at Scams room, they get cap guns to use as real guns to scare Keegan. On the way out they have an encounter with Dickie, then overhear Doc asking Miss Hanks about their missing car, causing them hide it. Later they burst in Keegans pawn shop, pointing their cap guns, demanding Keegan give up Obies watch. But Keegan hits his silent alarm and decides to call their bluff. Convinced that the guns are toys he dares them to shoot him. Squeezing the trigger, Obie fires off a live round. Realizing and revealing the gun Obie has is in fact real. They force Keegan into his own cage and make off with Obies watch. The police arrive and Scam is caught. Obie exits seconds after the police grab Scam and place him in the patrol car. Obie gets rid of the gun and hides. He manages to subdue one of the officers and steals the car that Scam is in. After leading officers on a chase through the city streets and down an alley, Obie and Scam crash the car but manages to get away.

They head to a diner where Scam informs Obie that his own father. Scam reveals his father went south to look for work after his parents got divorced. Scams mother died 3 months previously while in her custody, and has been on his own ever since. Having only his fathers old drivers license, Scam vows to find him. At this point Obie now considers Scam his friend as he heads home. Arriving home the next morning, the Dawkins are visited by the police to find out what happened to Obie and where he was. Obie lies, claiming he caught amnesia and does not remember. Afterwards he has his mom take him to school, not wanting to mention his bike was stolen.

While at school Obie starts thinking about Scam. Considering Scam his friend Obie decides that for all he has done for him he would help find his father. The next morning Obie steals the family jeep and returns to the hotel to look for Scam. But finds out from Miss Hanks that a social worker had came and put Scam in a home. Obie visits the social worker and finds out where Scam is. On his way back to the jeep, Obie finds it swarmed with police looking for him. Determined to help Scam he gets the Mercedes out of hiding and enlists the help of Johnnie Red. Though hesitant at first, Johnnie Red agrees to help him. As they set out to get Scam, someone spots the car and calls his friend to inform Doc and Dickie to claim the reward. Obie and Johnnie Red arrive at the youth center where Scam was placed. And with Johnnie Red passing himself off as Scams father the youth center releases him. Obie informs Scam that his uncle works at the Internal Revenue Service and with his help they will find his father.

Meanwhile Doc and Dickie head back to the bar to locate the car. Attacking the bartender the men are referred to Johnnie Red. Arriving back at Johnnie Reds place Doc and Dickie sneak up, grabbing the boys from behind. In an attempt to kidnap them Johnnie Red saves Obie and Scam, making Doc and Dickie flee. Obie and Scam head downtown to the IRS office of Obies uncle. Using his fathers drivers license, they find that Scams dad lives in a town in Louisiana, and the two set out on a road trip. Obies uncle tells his mom where they are headed, causing her extreme worry. Recalling their encounter with the boys previously at the hotel, Doc and Dickie force Miss Hanks into giving them Obies full name, thus leading them to the Dawkins 
residence.

Posing as Chicago Police officers they find out from Obies mom where the boys are headed. Because the car is registered in Docs name both he and Dickie set out after the boys, before they discover the body in the trunk.

When Obie and Scam get to Missouri they pick up a hitchhiking bible salesman. He talks them into heading to a diner to get a bite to eat. At the diner he asks them to watch his suitcase while he goes to wash his hands. Scam is convinced that he is a conman. Meanwhile, Doc and Dickie pass the diner, all the while unaware that they just passed the car. Revealed to be a conman, just as Scam suspected, the phony bible salesman sneaks outside and steals the car, leaving Obie and Scam stranded. Stopping so that Doc could urinate, he and Dickie spot their approaching Mercedes. Thinking its the kids they jump back in their car and give chase. But before they are able to catch up to the car, a State Trooper starts pursuing it for speeding, forcing Doc and Dickie break chase. When they notice that its not the kids they follow the car to the police station. Waiting to retrieve it Dickie and Doc sit inside a diner across the street. 

Meanwhile, Obie and Scam are aboard a bus heading towards their destination. But when Scam notices the conman being taken into police custody, he and Obie leave the bus to retrieve the car. Taking their eyes off the Mercedes for a few seconds allowed Obie and Scam to sneak up and regain the car. They drive off with Doc and Dickie chasing out right after them. They catch up to the boys and starts ramming them to force them off the road, with Doc firing gunshots at them. But good driving skills allowed Obie and Scam to evade their pursuers. Obie and Scam then decide to take the car to a dealership and trade it for another car, not ever knowing of the body inside its trunk.

Having traded the Mercedes for an old beat up Cadillac convertible, Obie and Scam make it to the town and address in Louisiana. Heading inside the house, they learn that Scams dad has moved. As they are heading back to the car, they noticed that Obies mom has shown up looking for him with the police. Both he and Scam run out the backdoor managing to avoid them. They head into a bar to ask about Scams dad. The bartender makes a racist remark about Scams dad, leaving Obie to tell him off with a few choice words before they both leave. Overhearing their inquiry, a waitress follows them outside and she tells them where they can find him. 

As they are heading back to their car the boys are grabbed once again by Doc and Dickie, forcing them back to the dealership where they traded the car. Returning to the dealership, Dickie holds the kids captive, as Doc attempts to take the car back from the car salesman at gun point. When Dickie is briefly distracted, Obie and Scam attack him and flee. Running across the road, a semi-trailer truck swerves to avoid hitting them, causing the driver to crash into the cars on the lot. Doc tries to run, but the chain reaction of car crashes sets off an explosion killing him. As Obie and Scam escape motorists spot an armed Dickie. They subdue him and hold until police arrive. Opening the trunk of the Mercedes, the police discover the body of the man they killed. With Doc dead, Dickie tries pinning the murder solely on Doc, and informs the police about the kids that stole the car.

Obie and Scam reach a car ferry to take them across the river. But the ferryman (Joe Seneca) charges them money they do not have. Rather than let Scam give up, when they are so close, Obie willingly gives his watch for payment. The ferryman accepts Obies watch, then has a change of heart and decides to take them for free. The two pull up to a factory where employees are leaving after the ending workday. Through the crowd Scam spots his dad, who recognizes him as they both run towards one another other and start hugging. As witnessing the reunion of Scam and his dad, Obie has found closure, and is accepting his fathers death. The movie ends with Scam and his dad riding back on the ferry with Obie, as he is reunited with his mother.

==Cast==
*Obidiah "Obie" Dawkins - Ricky Busker
*Jeremy "Scam" Henderson - Darius McCrary
*Dickie - Robert Joy
*Keegan - Robert Prosky
*Doc - Jerzy Skolimowski
*Johnnie Red - Paul Winfield
*Mrs. Dawkins/Mom - Brynn Thayer Bill Hudson
*Uncle Harry - Jim Antonio
*Alley Dawkins - Andrea Bebel
*Bible Salesman - Hutton Cobb
*Ferryman - Joe Seneca
*Miss Hanks - Beah Richards
*Bar Waitress - Ellen Geer

==External links==
* 
* 
* 

 
 
 
 
 