Tales of an Ancient Empire
{{Infobox film
| name           = Tales of an Ancient Empire
| image          = TalesOfAnAncientEmpire2010Poster.jpg
| alt            =  
| caption        = 
| director       = Albert Pyun
| producer       = {{plainlist|
* Nicholas Celozzi
* Cynthia Curnan
* Sazzy Lee Calhoun
}}
| writer         = Cynthia Curnan
| starring       = {{plainlist|
* Kevin Sorbo
* Michael Paré
* Sasha Mitchell
* Olivier Gruner
* Melissa Ordway
* Whitney Able
* Victoria Maurette
}} 
| music          = Anthony Riparetti
| cinematography = Philip Alan Waters
| editing        = David Lamb
| studio         = New Tales
| distributor    = 
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} sword and sorcery film directed by Albert Pyun.  It is the sequel to Pyuns directorial debut, The Sword and the Sorcerer.

==Synopsis==
Queen Ma’at finds her kingdom of Abelar under attack when treasure seekers accidentally open the tomb of vampire queen Xia. The Queen sends her half-sister, Princess Tanis, to the outlaw city of Douras to find her real father so he can save the kingdom. Meanwhile, servant girl Kara who shares the same father as Tanis, but her mother is Xia and discovers she (Xia) is really a vampire and begins hunting Tanis. Once in Douras, the Princess finds her half-brother Aedan  and convinces him to help her. Together they locate half-sister Malia, another half-sister Rajan and her daughter Alana. With this small group they plan to thwart vampire queen Xia.

==Cast==
* Kevin Sorbo as Aedan
* Michael Paré as Oda
* Whitney Able as Xia
* Melissa Ordway as Princess Tanis
* Sarah Ann Schultz as Malia
* Janelle Giumarra as Rajan 
* Inbar Lavi as Alana
* Jennifer Siebel Newsom as Queen Maat
* Ralf Moeller as General Hafez
* Matthew Willig as Giant Iberian
* Victoria Maurette as Kara
* Lee Horsley as Talon
* Sasha Mitchell as Rodrigo

==Production==
The sequel was originally announced in the end credits of The Sword and the Sorcerer. The production officially beginning with the writing of the script in November 2007.     Principal photography was completed in California.  According to Pyun, actors considered for the film included Yancy Butler, Kari Wuhrer, Mark Dacascos, Val Kilmer, Steven Seagal, Olivier Gruner, and Christopher Lambert. 

==Release==
The film had its official North American premiere at the Fright Night Film Fest during July 2010 in Louisville, Kentucky.  The film was released in the United States by Lionsgate in January 2012. 

==Reception==
Bob Calhoun of Salon.com wrote that Sorbo "sinks to sad new lows" after starring in the film.   Scott Weinberg of Twitch Film called it "one of the worst films Ive ever seen".  Weinberg said that it is "the sort of movie that makes you reconsider Uwe Bolls status as the reigning king of movie crap".   Nick Hartel of DVD Talk rated it 0/5 stars and also called Pyun worse than Boll, whom he called merely lazy rather than incompetent like Pyun.   David Johnson of DVD Verdict wrote that the films only redeeming quality is that it is "laughably short". 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 