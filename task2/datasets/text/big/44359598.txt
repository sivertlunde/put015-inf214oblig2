Ruby Red (film)
{{Infobox film
| name           = Ruby Red
| image          = 
| image size     =
| caption        =
| director       = Felix Fuchssteiner
| producer       =
| writer         = 
| narrator       =
| starring       = Maria Ehrich   Jannis Niewöhner   Laura Berlin
| music          = 
| cinematography = 
| editing        =
| distributor    = 
| released       =  
| runtime        = 122 minutes
| country        = Germany
| language       = German
| budget         =
| gross          =
| preceded by    =
| followed by    =
}} book with the same name by Kerstin Gier.  

==Cast==
* Maria Ehrich - Gwendolyn "Gwen" Shepherd
* Jannis Niewöhner - Gideon de Villiers
* Laura Berlin - Charlotte Montrose
* Veronica Ferres - Grace Shepherd
* Uwe Kockisch - Falk de Villiers
* Katharina Thalbach - Tante Maddy
* Gottfried John - Dr. White
* Josefine Preuß - Lucy Montrose
* Florian Bartholomäi - Paul de Villiers
* Johannes Silberschneider - Mr. Bernhard
* Gerlinde Locker - Lady Arista
* Axel Milberg - Lucas Montrose
* Jennifer Lotsi - Leslie Hay

==References==
 

==External links==
*  
*  

 
 


 