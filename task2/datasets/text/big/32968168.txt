Here is My Heart
{{Infobox film
| name           = Here is My Heart
| image          = Here_is_My_Heart_Poster.jpg
| border         = yes
| caption        = Theatrical release poster
| director       = Frank Tuttle
| screenplay     = {{Plainlist|
* Edwin Justus Mayer
* Harlan Thompson
}}
| based on       =  
| starring       = {{Plainlist|
* Bing Crosby
* Kitty Carlisle
* Roland Young
}}
| producer       = Louis D. Lighton
| cinematography = Karl Struss
| editing        = 
| distributor    = Paramount Pictures
| released       =  
| country        = United States
| language       = English
| runtime        = 77 minutes
}}
Here is My Heart is a 1934 American musical comedy film directed by Frank Tuttle and starring Bing Crosby, Kitty Carlisle, and Roland Young. Based on the play La Grande-duchesse et le garçon détage by Alfred Savoir, the film is about a famous singer who pretends to be a penniless waiter to get close to the woman of his dreams, a European princess.   

==Cast==
* Bing Crosby as J. (Jasper) Paul Jones
* Kitty Carlisle as Princess Alexandra
* Roland Young as Prince Nicholas / Nicki
* Alison Skipworth as Countess Rostova
* Reginald Owen as Prince Vladimir / Vova
* William Frawley as James Smith
* Marian Mansfield as Claire
* Cecilia Parker as Suzette, the Maid
* Akim Tamiroff as Manager of Hotel
* Arthur Housman as Drunken Waiter
* Charles Arnt as Higgins, Pauls Valet   

==Production==

===Soundtrack===
* "June in January" (Ralph Rainger and Leo Robin) by Bing Crosby
* "With Every Breath I Take" (Ralph Rainger and Leo Robin) by Bing Crosby
* "Love is Just Around the Corner" (Lewis E. Gensler and Leo Robin) by Bing Crosby   

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 


 