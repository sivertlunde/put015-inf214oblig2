City Streets (film)
{{Infobox film
| name           = City Streets
| image          = CITY STREETS.jpg
| border         = yes
| caption        = Theatrical release poster
| director       = Rouben Mamoulian
| producer       = E. Lloyd Sheldon
| writer         = {{Plainlist|
* Oliver H.P. Garrett
* Max Marcin (adaptation)
}}
| story          = Dashiell Hammett
| starring       = {{Plainlist|
* Gary Cooper
* Sylvia Sidney
* Paul Lukas
}}
| music          = 
| cinematography = Lee Garmes William Shea
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 83 minutes
| country        = United States
| language       = English}}

City Streets is a 1931 American crime film noir directed by Rouben Mamoulian and starring Gary Cooper, Sylvia Sidney and Paul Lukas. Based on a story by Dashiell Hammett, this Pre-Code crime films|Pre-Code crime film is about a racketeers daughter who is in love with a shooting gallery showman. Despite her prodding, the showman known as The Kid has no ambitions about joining the rackets and making enough money to support her in the lifestyle shes accustomed to. Her father implicates her in a murder and shes sent to prison, after which her father convinces The Kid to join the gang to free his daughter.

==Plot==
  Stanley Fields), at the urging of Big Fella Maskal (Paul Lukas), because Blackie was against Maskals involvement with Blackies gun moll Aggie (Wynne Gibson).

After Pop shoots Blackie, he passes the gun to Nan, thus implicating her in the murder. She naïvely takes the rap, believing the mob will arrange for her acquittal, but is sent to prison. Pop Cooley tries to convince The Kid to join the gang to free Nan, and he does so out of love for her. However, her attitude had changed since she was railroaded to prison. When The Kid visits Nan in prison in a fur coat, she becomes terrified of his involvement with Pops gang after witnessing a fellow inmates mobster boyfriend being gunned down outside the prison gate. When Nan is released, having served her term, she wants nothing more to do with the mob. She tries to persuade The Kid to quit the gang, but he refuses. Things go downhill from there. She finds that her father is unrepentant and involved with a loose, gold-digging woman named Pansy (Betty Sinclair). Maskal soon takes a strong liking to Nan and throws her a homecoming party, forcing her to dance with him all evening. When The Kid finally asserts his claim over Nan, Maskal threatens him, then later sends his thugs to kill him, but The Kid successfully disarms them, then goes after Maskal.

Terrified her lover will be killed, Nan goes to Maskal to warn him and offers herself to him in exchange for The Kids life. Aggie, now Maskals mistress, shoots him with Nans gun after he leaves her for Nan, and Nan is accused of murder. The Kid then names himself mob chief and escapes with Nan in a car with three of Maskals men, who aim to kill him. Thus events culminate in The Kid and Nan being taken "for a ride" by rival thugs. By racing a train and maintaining high speeds, The Kid keeps himself alive until Nan pulls a gun on the men and disarms them. Dropping the thugs off with "no hard feelings", The Kid tells them he has quit the beer business, and he and Nan drive off.

==Cast (in credits order)==
* Gary Cooper as The Kid
* Sylvia Sidney as Nan Cooley
* Paul Lukas as Big Fellow Maskal William "Stage" Boyd as McCoy
* Wynne Gibson as Agnes
* Guy Kibbee as Pop Cooley Stanley Fields as Blackie
* Betty Sinclair as Pansy
* Robert Homans as Police Inspector

==Awards and Nominations==
City Streets was nominated for AFIs 10 Top 10#Gangster|AFIs Top 10 Gangster Films list. 

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 