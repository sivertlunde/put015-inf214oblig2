Killer (1998 film)
{{Infobox film
| name           = Killer
| image          = 
| image_size     = 
| caption        = 
| director       = Darezhan Omirbaev
| producer       = Joël Farges Elise Jalladeau Gaziz Shaldybayev
| writer         = Darezhan Omirbaev Limara Zjeksembajeva
| narrator       = 
| starring       = Talgat Assetov Roksana Abouova
| music          = 
| cinematography = Boris Trochev
| editing        = R. Belyakova
| distributor    = Celluloid Dreams
| released       = September 12, 1998 (Toronto)
| runtime        = 80 minutes
| country        = Kazakhstan France
| language       = Russian
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} Kazakhstani Crime crime drama film directed by Darezhan Omirbaev.

==Plot==
Marat (Talgat Assetov) is a chauffeur who, following a traffic accident, finds himself in debt. When his baby becomes ill, he agrees to murder a journalist in order to earn some money. {{cite web
  | last = Stewart
  | first = Bhob
  | title = Plot Synopsis
  | publisher = Allmovie
  | url = http://www.allmovie.com/cg/avg.dll?p=avg&sql=1:162524~T0
  | accessdate =2008-01-25 }} 

==Cast==
*Talgat Assetov as Marat
*Roksana Abouova as Aijan

==Awards==
Killer won the Un Certain Regard Award at the 1998 Cannes Film Festival.    At the Karlovy Vary International Film Festival, it won the Don Quijote Award - Special Mention and was nominated for the Crystal Globe. {{cite web
  | title = Awards for Tueur à gages
  | publisher = Internet Movie Database
  | url = http://www.imdb.com/title/tt0157124/awards
  | accessdate =2008-01-25 }} 

==References==
 

==External links==
* 
*  

 

 
 
 
 
 
 
 

 
 