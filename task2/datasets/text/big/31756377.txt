Small Town Murder Songs
{{Infobox film
| name           = Small Town Murder Songs
| image          = Small_town_murder_songs.jpg
| caption        = Theatrical poster
| director       = Ed Gass-Donnelly
| producer       = Lee Kim Ed Gass-Donnelly
| writer         = Ed Gass-Donnelly
| starring       = Peter Stormare Martha Plimpton Aaron Poole Jill Hennessy Bruce Peninsula
| cinematography = Brendan Steacy 
| editing        = Ed Gass-Donnelly
| studio         = 3 Legged Dog Films Resolute Films and Entertainment
| distributor    = Kinosmith   Monterey Media  
| released       =  
| runtime        = 76 minutes
| country        = Canada
| language       = English
| budget         = 
| gross          = $30,858
}}
Small Town Murder Songs is a 2010 Canadian crime-thriller directed by Ed Gass-Donnelly. The film premiered at the Toronto International Film Festival on September 14, 2010. The film is written by Gass-Donnelly, produced by Gass-Donnelly and Lee Kim, and stars Peter Stormare, Jill Hennessy, and Martha Plimpton.
 Conestoga Lake, Palmerston in Ontario, Canada. 

The film has been given a limited theatrical release in the United States beginning on May 26, 2011.

==Cast==
* Peter Stormare as Walter
* Martha Plimpton as Sam
* Aaron Poole as  Jim
* Jill Hennessy as Rita
* Jackie Burroughs as Olive
* Ari Cohen as Washington
* Stephen Eric McIntyre as Steve
* Trent McMullen as Officer Kevin
* Alexandria Benoit as Sarah

==Plot==
The official website for the film provides this synopsis: "A modern, gothic tale of crime and redemption about an aging police officer from a small Ontario Mennonite town who hides a violent past until a local murder upsets the calm of his newly reformed life."

Stephen Holden, writing for The New York Times, explained the story:

  }}

==Release==
In March 2011 it was announced that Monterey Media had acquired the United States distribution rights.  The limited United States theatrical release began May 26, 2011 at the O-Cinema in Wynwood, Miami, Florida.  Monterey Media is set to release the film on DVD on July 19, 2011. 

===Festivals===
Small Town Murder Songs has been selected to screen at the following film festivals:

*Santa Catalina Film Festival - Winner Best Feature   
*Phoenix Film Festival - World Cinema Director   
*Whistler Film Festival - WINNER Best Actress   
*Torino Film Festival - WINNER Fipresci Critic’s Prize Best Film
*International Film Festival Rotterdam
*Toronto International Film Festival
*Hamptons International Film Festival
*Cucalorus Film Festival
*Palm Springs International Film Festival
*Santa Barbara International Film Festival
*Cinequest Film Festival
*Miami International Film Festival
*Cleveland International Film Festival
*Vail Film Festival
*Dallas International Film Festival
*Minneapolis-St. Paul International Film Festival
*Reel Dakota Film Society/Film Festival
*Seattle International Film Festival
*Indianapolis International Film Festival

==Critical reception==

Review aggregate Rotten Tomatoes reports that 79% of critics have given the film a positive review based on 19 reviews, with an average score of 6.1/10.  Stephen Cole from The Globe and Mail wrote a positive review, saying, "STMS succeeds as an Ontario Gothic mood piece". 

Michael Rechtshafen of The Hollywood Reporter wrote that Small Town Murder Songs is "An effective ensemble backed by a bracingly haunting soundtrack" after its screening at the Santa Barbara International Film Festival; he also saw similarities with Winters Bone and found

 

Dustin Hucks with Aint It Cool News also saw the film at The Santa Barbara International Film Festival and said, "Small Town Murder Songs is definitely a winner in the stable of films showing at the Santa Barbara International Film Festival this year". 

Holden of the New York Times wrote that the film "is not really a whodunit but a character study of a man squeezed in a psychological, spiritual and professional vise... Small Town Murder Songs is compellingly acted from top to bottom. As the raw passions of its hard-bitten characters seep into you, the songs hammer them even more deeply into your consciousness. The films only flaw — a big one — is its brevity. When it ends after 76 minutes, you are left wishing it had included Walters back story and had offered a more detailed picture of the town." 

Shannon from Movie Moxie had wonderful things to say about the film: "The powerful score sets an impressive and all-encompassing atmosphere to the film".  Robert Bell from Exclaim! had pleasant things to say about the film, stating that there were "some impressive cinematography and an understanding of tone through stillness and minor stylization make for a pleasant experience aesthetically".  Howard Feinstein from Screen Daily praised writer/director Gass-Donnelly: "Ed Gass-Donnelly makes appropriate, unpretentiously artful, stylistic choices in this tale of redemption". 

Alison Willmore of The AV Club, also praising the music, wrote, "Whos responsible for the killing is never much of a mystery in Small Town Murder Songs; there are no dark conspiracies, only dark natures. The tension instead focuses on whether Stormare will be able to rein himself in when the investigation inexorably pulls him toward his old life. Everyone is so restrained, their turmoil buried so deep, that the depth of what theyre feeling has to be excavated from whats left unsaid." 

==Awards==
;Santa Catalina Film Festival
:2011: won Best Feature

;Whistler International Film Festival
:2010: won Best Actress — Martha Plimpton

;Torino International Film Festival
:2010: won Fipresci Critic’s Prize Best Film 

;Phoenix Film Festival
:2011: won World Cinema Director — Ed Gass-Donnelly

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 