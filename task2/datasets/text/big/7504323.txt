More Pep
{{Infobox Hollywood cartoon|
| cartoon_name = More Pep
| series = Betty Boop
| image = 
| caption =
| director = Dave Fleischer
| story_artist =  Thomas Johnson 
| voice_actor = Mae Questel 
| musician = 
| producer = Max Fleischer
| distributor = Paramount Pictures
| release_date = June 19, 1936
| color_process = Black-and-white
| runtime = 6 mins
| movie_language = English
}}

More Pep is a 1936 Fleischer Studios animated short film starring Betty Boop, and featuring Pudgy the Puppy.

==Synopsis==

"Uncle Max" (Max Fleischer) draws Betty and Pudgy out of the inkwell.  Pudgy is tired and unwilling to perform on Bettys command. Betty uses pen and ink to draw a machine that give Pudgy more pep. The machine soon runs amok when she puts too many ingredients into the machine, speeding up not only Pudgy and Betty, but the entire city as well.

==External links==
* 
* 
 

 
 
 
 
 


 