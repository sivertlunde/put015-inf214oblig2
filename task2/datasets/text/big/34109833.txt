This Is Heaven
{{Infobox film
| name           = This Is Heaven
| image          =
| caption        =
| director       = Alfred Santell
| producer       = Samuel Goldwyn George Marion (dialogue)
| narrator       =
| starring       = Vilma Bánky
| music          = George Barnes Gregg Toland
| editing        =
| studio         = Samuel Goldwyn Productions
| distributor    = United Artists
| released       =  
| runtime        = 90 min. (sound) 7950 ft. (silent)
| country        = United States
| language       = English
| budget         =
}}

This Is Heaven (1929) is an American film, produced by Samuel Goldwyn, released through United Artists, and directed by Alfred Santell.

==Production background==
This film was released in both silent and sound versions. 

Uncertain about the future of sound films, believing that his product should either be all-talking or all-silent. With Vilma Bánky less than diligent about her vocal lessons, Goldwyn inserted three talking sequences into this silent picture then sat on the film for several months. His instincts proved correct: the box office didnt like it much either.   Bánky would make only three more films.

== Cast ==
* Vilma Bánky as Eva Petrie James Hall as James Stackpoole
* Fritzi Ridgeway as Mamie Chase
* Lucien Littlefield as Frank Chase Richard Tucker as E.D. Wallace

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 


 