After the Verdict
{{Infobox film
| name           = After the Verdict 
| image          =
| caption        =
| director       = Henrik Galeen
| producer       = I.W. Schlesinger Robert Hichens (novel)   Alma Reville Betty Carter   Malcolm Tod
| music          =
| cinematography = Theodor Sparkuhl  
| editing        = 
| studio         = Tschechowa Film
| distributor    = British Independent Film Distributors
| released       = January 1929
| runtime        = 9,370 feet 
| country        = United Kingdom 
| awards         =
| language       = English
| budget         = 
| preceded_by    =
| followed_by    =
}} silent drama Betty Carter. novel of Robert Hichens. It was made as an independent film at British International Pictures Elstree Studios. It is now considered a lost film. It was Galeens penultimate film as a director, after returning to Germany he directed the thriller The House of Dora Green (1933). 

==Cast==
* Olga Tschechowa as Vivian Denys 
* Warwick Ward as Oliver Baratrie  Betty Carter as Mrs. Sabine 
* Malcolm Tod as Jim Gordon 
* Ivo Dawson as Vivians father 
* Daisy Campbell as Vivians mother 
* Winter Hall as Lord Dartry 
* Annie Esmond as Lady Cardridge 
* A.B. Imeson as Coroner
* Henry Victor as Mr. Sabine

==References==
 

==Bibliography==
* Low, Rachael. History of the British Film, 1918-1929. George Allen & Unwin, 1971.

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 

 

 