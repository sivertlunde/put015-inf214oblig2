List of Malayalam films of 1994
 

The following is a list of Malayalam films released in the year 1994.

{| class="wikitable sortable"
!  width="25%" | Title
!  width="18%" | Director
!  width="12%" | Story
!  width="18%" | Screenplay
!  width="40%" | Cast
|- valign="top"
| Parinayam (1994 film)|Parinayam Hariharan
| &nbsp;
| &nbsp;
| Mohini, Vineeth
|- valign="top"
| Varaphalam
| Thaha
| &nbsp;
| &nbsp;
| Mukesh (Malayalam actor)|Mukesh, Maathu
|- valign="top"
| Pavithram
| T. K. Rajeev Kumar
| &nbsp;
| &nbsp;
| Mohanlal, Shobana
|- valign="top"
| Bharya (1994 film)|Bharya
| V. R. Gopalakrishnan
| &nbsp;
| &nbsp; Urvashi
|- valign="top"
| Bharanakoodam Sunil
| &nbsp;
| &nbsp; Geetha
|- valign="top"
| Sudhinam
| Nissar
| &nbsp;
| &nbsp; Madhavi
|- valign="top"
| Chukkan
| Thampy Kannanthanam
| &nbsp;
| &nbsp;
| Suresh Gopi, Gauthami
|- valign="top"
| Napoleon (1994 film)|Napoleon
| Saji
| &nbsp;
| &nbsp;
| Babu Antony, Maathu
|- valign="top"
| Sukham Sukhakaram
| Balachandra Menon
| &nbsp;
| &nbsp; Geetha
|- valign="top"
| Gentle Man Security
| J. Williams
| &nbsp;
| &nbsp;
| Babu Antony, Moon Moon Sen
|- valign="top"
| Kashmeeram
| Rajeev Anchal
| &nbsp;
| &nbsp;
| Suresh Gopi, Priya Raman
|- valign="top"
| Bheesmacharya
| Cochin Haneefa
| &nbsp;
| &nbsp; Siddique
|- valign="top"
| CID Unnikrishnan B.A., B.Ed.
| Rajasenan
| &nbsp;
| Sasdharan Arattuvazhi Chippy
|- valign="top"
| Commissioner (film)|Commissioner
| Shaji kailas
| &nbsp;
| Renji Panicker
| Suresh Gopi, Shobana
|- valign="top"
| Chief Minister K. R. Gowthami
| Babu Raj
| &nbsp;
| &nbsp; Geetha
|- valign="top"
| Thenmavin Kombathu
| Priyadarshan
| &nbsp;
| &nbsp;
| Mohanlal, Nedumudi Venu, Shobana
|- valign="top"
| Rajadhani (1994 film)|Rajadhani
| Joshy Mathew
| &nbsp;
| &nbsp; Geetha
|- valign="top"
| Kadal (1994 film)|Kadal
| Siddique Shameer
| &nbsp;
| &nbsp;
| Babu Antony, Charmila
|- valign="top"
| Pingami
| Sathyan Anthikkad
| &nbsp;
| &nbsp; Kanaka
|- valign="top"
| Gandheevam
| Sab John
| &nbsp;
| &nbsp; Siddique
|- valign="top"
| Malappuram Haji Mahanaya Joji
| Thulasidas
| &nbsp;
| &nbsp;
| Mukesh (Malayalam actor)|Mukesh, Siddique (actor)|Siddique, Maathu
|- valign="top"
| Chanakya Soothrangal
| Somanathan
| &nbsp;
| &nbsp;
| Nedumudi Venu, Menaka

|- valign="top"
| Ilayum Mullum
| K. P. Sasi
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top" The City
| I. V. Sasi
| &nbsp;
| &nbsp; Urvashi
|- valign="top"
| Pradakshinam
| Pradeep Chokli
| &nbsp;
| &nbsp;
| Manoj K Jayan, Siddique (actor)|Siddique, Sunitha (actress)|Sunitha, Maathu
|- valign="top"
| Vishnu (1994 film)|Vishnu
| P. Sreekumar
| &nbsp;
| &nbsp;
| Mammootty, Shobana
|- valign="top"
| Kudumba Vishesham
| Anil Babu
| &nbsp;
| &nbsp; Urvashi
|- valign="top"
| Puthran
| Jude Attipety
| &nbsp;
| &nbsp; Rohini
|- valign="top"
| Pakshe Mohan
| &nbsp;
| &nbsp;
| Mohanlal, Shobana
|- valign="top"
| Vendor Daniel State Licency
| Balu Kiriyath
| &nbsp;
| &nbsp;
| Thilakan, Jagadish
|- valign="top"
| Nandini Oppol
| Mohan Kapleri
| &nbsp;
| &nbsp;
| Geetha (actress)|Geetha, Sunitha (actress)|Sunitha, Siddique (actor)|Siddique, Nedumudi Venu
|- valign="top"
| Cabinet (film)|Cabinet
| Saji
| &nbsp;
| &nbsp; Anju
|- valign="top"
| Avan Ananthapadmanabhan
| Prakash Kolery
| &nbsp;
| &nbsp;
| Sudha Chandran
|- valign="top"
| Varanamalyam
| Viji P. Nair
| &nbsp;
| &nbsp;
| Siddique (actor)|Siddique, Santhikrishna
|- valign="top"
| Tharavadu (film)|Tharavadu
| Krishnan Munnadu
| &nbsp;
| &nbsp;
| Siddique (actor)|Siddique, Suchithra
|- valign="top"
| Paalayam
| T. S. Suresh Babu
| &nbsp;
| &nbsp;
| Babu Antony, Manoj K Jayan
|- valign="top"
| Kinnaripuzhayoram
| K. K Haridas (director)|K. K Haridas
| Priyadarsan
| Priyadarshan, Gireesh Puthenchery Mukesh
|- valign="top"
| Dhadha
| P. G. Viswambaran
| &nbsp;
| &nbsp;
| Babu Antony, Santhikrishna


|- valign="top"
| Minnaram
| Priyadarshan
| &nbsp;
| &nbsp;
| Mohanlal, Shobana
|- valign="top"
| Sainyam
| Joshi
| &nbsp;
| &nbsp;
| Mammootty, Mohini
|- valign="top"
| Vardhakya Puranam
| Rajasenan
| &nbsp;
| &nbsp; Kanaka
|- valign="top"
| Njan Kodiswaran
| Jose Thomas
| &nbsp;
| &nbsp;
| Jagadish, Vinodini
|- valign="top"
| Pavam I. A. Ivachan
| Roy P. Thomas
| &nbsp;
| &nbsp;
| Innocent (actor)|Innocent, Srividya, Jagadish, Siddique (actor)|Siddique,  Jagathy
|- valign="top"
| Sagaram Sakshi
| Sibi Malayail
| &nbsp;
| &nbsp;
| Mammootty, Sukanya
|- valign="top"
| Rudraksham
| Shaji Kailas
| &nbsp;
| &nbsp; Geetha
|- valign="top"
| Pidakkozhi Koovunna Noottandu
| Viji Thampi
| &nbsp;
| &nbsp;
| Urvashi (actress)|Urvashi, Manoj K. Jayan, Vinaya Prasad, Kalpana (Malayalam actress)|Kalpana, Rudra, K. P. A. C. Lalitha
|- valign="top"
| Manathe Vellitheru Fazil
| &nbsp;
| &nbsp;
| Shobana, Vineeth, Baby Saranya
|- valign="top"
| Sudha Mathalam
| Thulasidas
| &nbsp;
| &nbsp;
| Mukesh (Malayalam actor)|Mukesh, Madhurima
|- valign="top"
| Vadhu Doctoranu
| K. K. Haridas
| &nbsp;
| &nbsp; Nadia Moidu
|- valign="top"
| Chakoram
| M. A. Venu
| &nbsp;
| &nbsp; Murali
|- valign="top"
| Gamanam
| Sriprakash
| &nbsp;
| &nbsp; Lakshmi
|- valign="top"
| Swaham
| Shaji N. Karun
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Kambolam
| Biju Kottarakkara
| &nbsp;
| &nbsp;
| Babu Antony, Charmila
|- valign="top"
| Manathe Kottaram Sunil
| &nbsp;
| &nbsp; Dileep
|- valign="top"
| Sukrutham Harikumar
| &nbsp;
| M. T. Vasudevan Nair
| Mammootty, Shanthi Krishna, Gauthami
|- valign="top"
| Santhanagopalam
| Sathyan Anthikkad
| &nbsp;
| &nbsp;
| Jagadish, Thilakan, Chippy (actress)|Chippy, Ilavarasy, Balachandra Menon
|- valign="top"
| Santhanagopalam
| &nbsp;
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Kochaniyan
| &nbsp;
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Dollar (film)|Dollar
| &nbsp;
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top" The Porter
| &nbsp;
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Ponthan Mada
| &nbsp;
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Daivathinte Vikrithikal
| &nbsp;
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Bhaagyavaan
| &nbsp;
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Sraadham
| &nbsp;
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Galileo (1994 film)|Galileo
| &nbsp;
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Avalude Janmam
| &nbsp;
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Bharanakoodam
| &nbsp;
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Vidheyan
| &nbsp;
| &nbsp;
| &nbsp;
| &nbsp;
|}

==Dubbed films==
{| class="wikitable sortable"
!  width="25%" | Title
!  width="18%" | Director
!  width="12%" | Story
!  width="18%" | Screenplay
!  width="40%" | Cast
|- valign="top"
|- valign="top"
| Hey Hero
| Raghavendra Rao
| &nbsp;
| &nbsp;
| Chiranjeevi, Nagma
|- valign="top"
| Chiranjeevi
| Kodi Ramakrishnan
| &nbsp;
| &nbsp;
| Chiranjeevi, Bhanupriya
|- valign="top"
| Ladies Only
| Singhitham Sreenivasa Rao
| &nbsp;
| &nbsp; Urvashi
|}

==References==
 
* 
* 
* 

 
 
 

 
 
 
 
 