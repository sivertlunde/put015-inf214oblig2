The Immortal Story
{{Infobox film 
| name           = The Immortal Story 
| image          = Immortal Story poster.jpg
| image_size     = 
| caption        = Spanish theatrical poster
| producer       = Micheline Rozan
| director       = Orson Welles 
| writer         = Karen Blixen (story) Louise de Vilmorin Orson Welles
| starring       = Jeanne Moreau Orson Welles Roger Coggio Norman Eshley
| music          =
| cinematography = Willy Kurant
| editing        = Claude Farny Françoise Garnault Yolande Maurette Marcelle Pluet
| distributor    = Altura Films S.L. (US) Omnia-Film (world)
| released       = May 24, 1968 (France) September 18 (New York Film Festival|NYFF) February 1969 (US)
| runtime        = 60 mins (English version) 48 mins (French version)
| country        = France
| language       = English French
| budget         = 
| gross          =
}}
 1968 Cinema French film Danish writer Karen Blixen (more widely known by her pen name Isak Dinesen).  With a running time of 60 minutes, it is the shortest feature film directed by Welles.

==Plot==
In 19th century Macao, Mr. Clay (Orson Welles) is a wealthy merchant at the end of his life. His only constant companion is his bookkeeper, a Polish-Jewish emigrant named Levinsky (Roger Coggio).  One evening, Levinsky mentions an apocryphal story of a rich old man who offers a sailor five guineas to impregnate his wife. Clay becomes obsessed in making that legendary tale come true, and Levinsky is dispatched to find a sailor and a young woman who will play the part of Clay’s wife.  Levinsky approaches Virginie (Jeanne Moreau), the daughter of Clay’s one-time business partner.  Clay’s ruthless dealings drove Virginie’s father to bankruptcy and suicide, and she is eager to participate in this action to get her revenge.  The destitute sailor, a young Englishman named Paul (Norman Eshley), is discovered on the street and recruited.  Virginie and Paul find an emotional bond in their brief union, but go their separate ways – Virginie is exorcised of her bitterness against Clay while Paul disappears into Macao’s teeming streets.  Levinsky goes to inform Clay about what took place, but discovers the old merchant has died. 

==Cast==
*Jeanne Moreau as Virginie Ducrot
*Orson Welles as Mr. Charles Clay
*Roger Coggio as Elishama Levinsky
*Norman Eshley as Paul, the sailor
*Fernando Rey as Merchant

==Production==
Orson Welles was a self-professed admirer of the writing of Karen Blixen and, at one point, announced plans to create a series of films based on her writing.    The Immortal Story is a short story first published in Anecdotes of Destiny and Ehrengard a collection of short stories by Blixen. Originally The Immortal Story was meant to be half of a two-part anthology film, with the second half based on the Blixen story The Deluge at Nordenay. However, the second film was cancelled when Welles raised concerns about the professionalism of his crew in Budapest, Hungary, where production was to have taken place.   
  
Welles received financing from Organisation Radio-Télévision Française to create The Immortal Story for premiere presentation on French television, to be followed by theatrical release in France and other countries.  As part of the financing, Welles was contractually obligated to shoot the film in color.  Welles was not a fan of color cinematography, and in one interview he stated: "Color enhances the set, the scenery, the costumes, but mysteriously enough it only detracts from the actors. Today it is impossible to name one outstanding performance by an actor in a color film."
 

Much of the film was shot in Welles’ home, located outside of Madrid, Spain.  Exterior scenes depicting Macao were shot in Chinchón, a town near Madrid.  Welles used Chinese restaurant waiters from Madrid as extras to recreate the setting for Macao. 

==Release==
The Immortal Story was entered into the 18th Berlin International Film Festival in June 1968.    The film had its U.S. premiere at the 1968 New York Film Festival. In February 1969, it had its U.S. theatrical release on a double feature bill with Luis Buñuels Simon of the Desert. 

To date, The Immortal Story has never had an official home video or DVD release in the U.S., although it has been released in European home entertainment markets. 

In September 2010 Madman Entertainment will release both the English language and shorter French language versions of the film on a single Region 4 DVD.

The film has been recently aired on the Turner Classic Movies cable television network. The film is now available to view on Hulu under the hulu plus category. 

==References==
 

==External links==
*  
*  
*  

 

 

 
 
 
 
 
 
 
 
 