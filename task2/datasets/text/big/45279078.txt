Fascination Amour
{{Infobox film
| name           = Fascination Amour
| image          = FascinationAmour.jpg
| alt            = 
| caption        = Film poster
| film name      = {{Film name
| traditional    = 愛情夢幻號
| simplified     = 爱情梦幻号
| pinyin         = Ài Qíng Mèng Huàn Hào
| jyutping       = Ngoi3 Cing4 Mung6 Waan6 Hou6 }}
| director       = Herman Yau Raymond Wong
| writer         = 
| screenplay     = Raymond Wong Chau Ting Wong Ho-wa
| story          = 
| based on       = 
| starring       = Andy Lau Hikari Ishida
| narrator       = 
| music          = Brother Hung
| cinematography = Joe Chan
| editing        = Robert Choi Mandarin Films Fitto Movie
| distributor    = Mandarin Films Distribution
| released       =  
| runtime        = 95 minutes
| country        = Hong Kong Mandarin Japanese Japanese English English
| budget         = 
| gross          = HK$8,889,770
}}

Fascination Amour is a 1999 Hong Kong romantic comedy film directed by Herman Yau and starring Andy Lau and Hikari Ishida.

==Plot==
Albert Lai (Andy Lau) is the only child of Hong Kongs richest tycoon. Albert brings Kathy (Qu Ying), a waitress introduced to him by his mother (Susan Tse), on to a cruise ship, where on the first date night, he receives the approval of Kathy and got engaged. This is already Alberts eighth engagement since love always comes and goes very quick for him, and he would always break an engagement whenever he loses the feeling of novelty in each relationship.

Albert becomes increasingly fond of Kathys ability to cope decently. It turns out Kathy has been secretly struggling to study the topic of the second day of the cruise, making her feel extremely exhausted. At this time, Kathy meets Timothy (Huang Lei), a waiter on the ship, who came from a same background as her. Kathy feels unrestrained while being with Timothy, and thus, her heart starts to shift.

However, Albert is unwilling to give up and he is confident that he will be able to win Kathy back with his charm. At this time, Sandy (Hikari Ishida) appears around Timothy and she views Albert as an enemy. Compared to Albert, Sandy is far more acerbic and a much bigger spender. However, Albert gradually discovers that while Sandy is not bickering with him, he would feel empty and lonely, a feeling that he never had. Before, nobody had dared to point out Alberts shortcomings, only Sandy dared to point them out publicly. As he comes in contact with Sandy more, he becomes attracted to her personality.

While the ship was to dock and Albert wanted to express his feelings, he discovered police officers on shore waiting for Sandy. It turns out that Sandy was bankrupt and this will be her last journey. Whether or not Kathy and Timothy could be together, or whether Albert is able to reverse the adverse and win Sandys heart, there would always be an end on a wandering life on a ship when it goes to shore.


==Cast==
*Andy Lau as Albert Lai
*Hikari Ishida as Sandy Fong
*Lillian Ho as Maggie Lai
*Qu Ying as Kathy Luk
*Huang Lei as Timothy Wong Raymond Wong as Jack
*Christine Ng as Rose Anthony Wong as Eric
*Astrid Chan as Mandy
*Susan Tse as Mrs. Lai
*James Ha as Alberts assistant
*Nelson Cheung as Alberts friend on cruise
*Shrila Chun as Alberts friend on cruise

==Production==
In 2010, when Andy Lau appeared on the Taiwanese talk show Kangxi Lai Le, he revealed that this film left him some regrets. He states while he was paid over HK$10 million, when he went aboard the cruise ship to film at the Caribbean Sea, after the film company payed him his salary, they did not have any money to hire crew members and extra actors, and thus, Lau was very busy on set having to take care of other crew work.

Speaking of how he felt cheated when he got on board, Lau states,

 

On set, Lau had to pave the rails for the cameras to move and worked as set coordinator and inviting cruise passengers to work as extras. Lau also states,

 
 

==Reception==
===Critical=== Lunar New Year, this fluffy romantic comedy rests solely on Andy Lau|Lau’s able shoulders. Sadly, the filmmakers didn’t give him a proper character." 

===Box office===
The film grossed HK$8,889,770 at the Hong Kong box office during its theatrical run from 13 February to 19 March 199 in Hong Kong.

==See also==
*Andy Lau filmography
*List of Hong Kong films of 1999
*List of films set in Puerto Rico

==References==
 

==External links==
* 
*  at Hong Kong Cinemagic
* 

 

 
 
 
 
 
 
 
 
 