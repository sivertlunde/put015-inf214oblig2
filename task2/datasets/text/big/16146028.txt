The Late Show (film)
{{Infobox film
| name           = The Late Show
| image          = Lateshowdvd.jpg
| caption        = DVD cover
| director       = Robert Benton
| producer       = Robert Altman Scott Bushnell
| writer         = Robert Benton
| starring       = Art Carney Lily Tomlin Bill Macy Eugene Roche Joanna Cassidy
| music          = Kenneth Wannberg
| cinematography = Charles Rosher Jr. Lou Lombardo
| distributor    = Warner Brothers Pictures
| released       = April 22, 1977
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 1977 neo-noir, mystery film written and directed by Robert Benton and produced by Robert Altman.  It stars Art Carney, Lily Tomlin, Bill Macy, Eugene Roche, and Joanna Cassidy. 

A drama with a few comic moments, the story follows an aging detective trying to solve the case of his partner’s murder while dealing with a flamboyant new client.  

Benton was nominated for the Academy Award for Best Original Screenplay in 1977 in film|1977. 

==Plot==
A financially strained, aging Los Angeles private detective named Ira Wells isnt a well man and is barely active in the business. He is a loner who doesnt much care for company or small talk. When his ex-partner Harry Regan, however, shows up at Iras boarding house one night mortally wounded while on a case, Ira feels its up to him to get to the bottom of it.

The trail leads Ira to a small-time fence named Birdwell, whose young bodyguard Lamar is only too happy to rough up the old man when Ira pays a call. But they make a mistake in intimidating and underestimating Ira, who ends up paying Lamar back in kind as well as tracking down Birdwells missing wife. 

Meanwhile, a would-be client named Margo Sperling is introduced to Ira by a mutual acquaintance, Charlie Hatter, a tipster. Margo is a quirky individual who acts as an agent for a singer, sells marijuana on the side and wants to hire Ira to find not a murderer but just her missing cat.

As they get to know each other after a rocky start, Ira and Margo hit it off to the point that she offers to become his new partner. But first they need to deal with a dangerous confrontation in Margos apartment.  

==Cast==
* Art Carney as Ira Wells
* Lily Tomlin as Margo Sperling
* Bill Macy as Charlie Hatter
* Eugene Roche as Ronnie Birdwell
* Joanna Cassidy as Laura Birdwell John Considine as Lamar Ruth Nelson as Mrs. Schmidt
* John Davey as Sergeant Dayton
* Howard Duff as Harry Regan

==Production== 1976 and wrapped in November.  Lou Lombardo, who had a long relationship with Altman and edited several of Altmans films in the 1970s, edited along with Peter Appleton.
 Group Theatre. Arch of Triumph in 1948.

==Reception==
===Critical reception===
The Late Show got extremely positive reviews when it was initially released in 1977. Pauline Kael wrote, "The Late Show never lets up; the editing is by Lou Lombardo (who has often worked with Robert Altman) and Peter Appleton, and I cant think of a thriller from the forties that is as tight as this, or has such sustained tension. ... The Late Show is fast and exciting, but it isnt a thriller, exactly. Its a one-of-a-kind movie—a love-hate poem to sleaziness."   Subscription required for online access.  Also in 1977, Roger Ebert wrote in the Chicago Sun-Times, "And most of all, its a movie that dares a lot, pulls off most of it, and entertains us without insulting our intelligence" giving the film a four-star rating.  A more recent appreciation of the film was penned by Doug Krentzlin in 2014, who called the film "a unique, one-of-a-kind film that lived up to its advertising tagline, The nicest, warmest, funniest, and most touching movie you’ll ever see about blackmail, mystery, and murder." 

The Late Show has a 100% rating at Rotten Tomatoes based on 13 reviews. 

===Awards and nominations=== BAFTA Award Golden Globe Best Motion Picture Screenplay at the Edgar Awards. 

==Television series==
The film was the inspiration for a short-lived US television series, Eye to Eye (1985).  
==Home video==
The Late Show was released as a zone 1 DVD in 2004.     It had previously been released as a VHS tape. 

==References==
 

==External links==
*  
*  
*   

 

 
 
 
 
 
 
 
 
 
 
 
 
 