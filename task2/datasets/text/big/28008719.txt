The Black Ninja
 
{{Infobox film
| name           = The Black Ninja
| image          = The Back Ninja cover.jpg
| alt            = 
| caption        = 
| director       = Clayton Prince
| producer       = Parris Z. Moore
| writer         = Clayton Prince
| starring       = Clayton Prince Carla Brothers Nicky DeMatteo Yuki Matsuzaki Heather Hunter John Canada Terrell Michael Chance
| music          = 
| cinematography = Brendan Flynt
| editing        = Michael Rector
| studio         = MTI Home Video
| distributor    = MTI Home Video Singa Home Entertainment
| released       =  
| runtime        = 91 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
The Black Ninja is a 2003 American   was released as a direct download February 2015.

==Plot==
Maliq Ali (Clayton Prince) is a defense attorney whose guilt over freeing guilty criminals, and the death of his family, leads him to become a costumed ninja vigilante who stalks these same criminals at night. After refusing to defend Tony Fanelli (Nicky DeMatteo), a small-time mobster accused of murder, he finds himself involved in protecting the only witness, Tracey Allen (Carla Brothers), from Fanellis hired thugs. While protecting this young woman, he is confronted by The Red Ninja (Yuki Matsuzaki), a Japanese assassin, who years before killed his family rather than pay Ali for defending him.

==Cast==
*Clayton Prince as Maliq Ali and The Black Ninja
*Carla Brothers as Tracey Allen
*Nicky DeMatteo as Tony Fanelli
*Yuki Matsuzaki as Shinji Hagiwara and The Red Ninja
*Heather Hunter as Patty Ali 
*John Canada Terrell as Mr. Fanellis Lawyer 
*Michael Chance as Detective Howell

==Production==
Primarily financed by Clayton Prince, filming began in Philadelphia, Pennsylvania and ended after two weeks. Critics noted that the rush to finish the film drastically affected its overall quality, much of which could have been cleaned up post-production editing, leaving it looking "unfinished and more like an amateur, student film". An hour-long "Making of" documentary was produced and included on the DVD following its release. It gave a "behind the scenes" look on the set of a low-budget film and featured many of the cast and crew voicing their opinions on the lack of opportunity in the film industry in regards to race.

==Reception==
The Black Ninja was given a limited theatrical release on November 2, 2002, and distributed worldwide on DVD on January 14, 2003. The film was heavily criticized for its apparent lack of martial arts choreography and generally poor quality of the story, acting and cinematography.

== Sources ==
* 
* 
* 
*http://redlettermedia.com/half-in-the-bag/whats-your-number-and-the-black-ninja/ Whats Your Number and the Black Ninja

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 