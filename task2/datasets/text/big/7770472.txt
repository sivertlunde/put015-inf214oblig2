Una Película de Huevos
{{Infobox film
| name           = Una película de huevos
| image          = Unapeliculadehuevos.jpg
| image_size     =
| caption        = Movie poster
| director       = Gabriel and Rodolfo Riva Palacio Alatriste
| producer       = Gabriel and Rodolfo Riva Palacio Alatriste Francisco Arriagada Raúl Calao Carlos Zepeda Jose Lara
| writer         = Gabriel and Rodolfo Riva Palacio Alatriste
| narrator       =
| starring       = Bruno Bichir  Angélica Vale  Carlos Espejel
| music          = Carlos Zepeda
| cinematography =
| editing        = Valeria Foster Leandro Spatz	 	
| studio         = Huevocartoon Producciones
| distributor    = Videocine
| released       = April 21, 2006
| runtime        = 90
| country        = Mexico 
| language       = Spanish
| budget         = $17,000,000
}}
 Spanish for A Movie of Eggs) is a 2006 animated Mexican adventure comedy film produced by Huevocartoon Producciones (Eggcartoon Productions). The film featured voices by Bruno Bichir, Angélica Vale and Carlos Espejel. This film, along with its sequel, were released direct-to-video in the United States,  which is available in some Walmart locations.


==Synopsis==
The movie starts off with the egg Toto who wants to become a chick but he was taken away from his mother before he could hatch. Then he was put for sale in a supermarket where Toto was bought. Toto meets Willy and a bunch of other eggs who want to escape. After Willy and the Leader devised a plan to escape, they chose Toto to distract the cat while they escape. Willy would serve as his aid when Toto escaped the cat, but sadly they get left behind by the others and were left trapped in the house, forced to look for a way to get to a farm so Toto can hatch.

==Characters==

===Chicken Eggs and allies===

*Toto: Toto is a three-day-old chicken egg who was hoping to hatch into a rooster, but was mistakenly taken away from his mother before that could happen. He can be very stubborn at times, specially about getting as quick as possible to a farm, but hes also very gentle and caring, even deciding to give up what could have been his only chance of hatching in spite of saving his friends. Even so, he manages to make his way to the farm and hatches successfully. As an egg hes clearly distinguishable because of the feathers growing on his head and brown spots on his belly. As a chick he retains his head feathers as well as the markings.

*Willy: Willy is a four-day-old chicken egg and Sargent of Leaders army, who served as look-out in the egg container. At first he only tags along Toto and Tocino because hes afraid of being left alone in the kitchen, but soon grows attached to them, risking his life on several occasions in order to save their lives. At first he decides to try and become a rooster like Toto, but this objective changes when he meets Bibi and decides to take a chance and choose his own road. Willy always wears a green army helmet and a belt.

*Tocino: Tocino is a mute strip of bacon that Toto and Willy found in the floor of the kitchen. Hes often used by his friends as a rubber band or weapon due to his physical properties such as elasticity and greasiness. Despite his lack of speech, Tocino excels at mimic and can be easily understood by most characters with just a simple movement. he doesnt wear any clothing, but his feet resemble shoes.

*Bibi: Bibi is a performer egg who works at a carnival and is no longer fresh. Despite being a spoiled egg, she doesnt let that get her down and has decided to live her life at the fullest. Though she initially sees Willy as a friend, her feelings develop further when he confesses his love for her. Bibi has long, curly brown hair tied up in a ponytail and sports a blue cape with star brooches hanging from it. She has two brothers, Bebe and Bubi.

*Bebe and Bubi: Bebe and Bubi are Bibis brothers who perform in the same juggling act as her. Despite their looks, Bebe and Bubi are very mature and love their sister deeply, to the point of endangering their lives to save her from Serp. Bebe is a large egg that wears a red body suit with star markings and has a Mohawk hairstyle|mohawk, Bubi is a blond egg who wears a blue bodysuit with a star on the chest.

*Leader: Leader is the General of the kitchens egg army. Hes quick to recruit any new eggs into his gang, even when theyre not interested. He has bad hearing, which derived on him calling Toto "Pompis". He speaks in a German accent and is the only egg with facial hair. He wears a General army hat.

*Clara: Clara is the Leaders loud subordinate and Willys superior. She often bosses other eggs around, but does so out of genuine concern for their safety. Despite her simple appearance she can be told apart from other eggs because of her pink lips and pink army hat.

*Effeminate Egg: This unnamed egg is a prominent member of the Leaders army. Although portrayed as a stereotypical homosexual, there are several instances in which he shows his sheer bravery, such as happily volunteering to be the decoy egg or fearlessly opposing a relatively larger reptile egg. In Totos birthday party, he is hinted to be in a relationship with Coco.

===Reptile Eggs===

*Coco: Coco is a crocodile egg and leader of the reptile gang sent by the adult reptiles to crush the chicken eggs. Despite being the leader he doesnt really care about the missions given to him by his father and spends most of his time dreaming of becoming a famous actor. He can be quite clueless, even to the point of believing the entire carnival battle to have been staged by the Leader. Hes got big, bulky arms tipped with yellowish cuffs and an exaggerated Elvis Presley-style hairdo. In his final appearance, after joining the Leaders army mistaking it for a theater company, he sports a cleaner hair cut, a pink cape and fashionable glasses. In Totos birthday party its hinted that he has since started a relationship with the effeminate chicken egg.

*Serp: Serp is a rattlesnake egg, member of Cocos gang. Hes very quick tempered, which contrasts starkly with the easygoing nature of the other reptile eggs. Hes constantly scolding Coco into concentrating on the mission at hand. Due to being a snake egg, he lacks hands and so depends on his surfacing tail to grab objects. In the end hes thrown away by an annoyed Coco who, as he states, is "sick of his rattle".

*Iguano: Iguano is an iguana egg and the largest of the reptile egg gang. His low intelligence makes him the target of the others most abusive orders, which he follows blindly even when this clearly puts him in danger. He often carries around a club and his hair is messy and overgrown

*Lagartijo: Lagartijo is a common lizard egg and part of the reptile egg gang. Hes a neurotic egg with a persecution complex.

*Torti: Torti is the slow tortoise egg whose powerful jaws Cocos gang commonly use to snap things apart. Due to his slow movements he often annoys the other gang members, who prefer to carry him around or shut him up instead of waiting for him to walk or speak. Hes the only quadrupedal egg featured, despite the fact that most of the other members of the reptile gang also are eggs of quadrupedal animals.

===Other Eggs===

*Huevay "The Second": Huevay is a chocolate egg that got lost into the familys sofa a year ago. Being a chocolate egg, he could be considered a portrayal of black people, but his behavior suggests hes actually Cuban. Being still half wrapped, Huevay wears what resembles to be golden pants.

*Confi: Confi is a confetti egg and the most prominent of the eggs of the same name. Among his people hes considered some sort of spiritual guide and is often followed by the others in his senseless prayers and speeches. He also seems to operate the night club-like establishment behind the amusement park. Confi, like traditional confetti eggs, is painted in bright colors and tipped with a piece of navy blue india paper.

===Animals===

*Totos Mother: Totos mother is a sweet and caring, but also brave hen that leaves her original farm in spite of finding her son. Shes followed around by her unnamed friends who are very talkative and gossipy, but kindhearted. Totos Mom is mostly white with traces of brown and wears a blue handkerchief around her neck while her friends wear pearls. In the end all of them choose to stay at "Granjas El Pollon" until Toto hatches.

*Cat: This unnamed cat belongs to the family Toto is sold in the beginning of the movie and is aware that the eggs are trying to escape the kitchen. The cat is very vicious and insistent; And as such Toto and Huevay could barely escape from it. The cat has mostly yellow fur.
 crossing the to get a better life" (In regards crossing the sewers "river") or about whether they should just follow destiny or actually work for food. They like to eat eggs and bacon, and try to eat Toto, Willy and Tocino, but are fooled easily due to their incompetence. While Tlacua is quick tempered, Cuache is very easygoing. They call each other "compadre".

*Cocodrilo: Cocodrilo is the crocodile leader of the adult reptiles group and father of Coco. He wishes his son was a bully just like him, but is depressed by the fact that Coco cares more about acting. After Serps plan to crush Willy and Bibi fails, a roller coaster cart filled with children crashes on the Herpetarium he resides in, showing that hes more scared of children than they are of him. Cocodrilo sports a military-like hairdo.

*Dove: This unnamed dove was tamed by Willie so he could fly Toto to the farm as a thanks for saving his life. After landing safely the dove escapes.

===Humans===
*Mister: Dad is a middle aged man that enjoys drinking raw eggs. Although the eggs of the kitchen conceive him as an evil monster hes just an average man frustrated with his life and family.

*Misses: Misses is Misters overweight wife and mother to three misbehaving children. She enjoys making breakfast for her family unaware that the eggs perceive her as a demon who likes to torture them.

*The twins: The twins are the sons of Mister and Misses, and tend to quarrel over small things. Like their mother theyre overweight. While one of them likes scrambled eggs the other favors egg malts.

*Claudia: Claudia is Mister and Misses youngest child and the only one whose name was said. Shes a little girl with strong moral values who opposed to letting her classmate copy her homework, even when he threatened with throwing her lunchbox out of the bus. Like her father she is not overweight.

*Juggler: The juggler is a carnival performer and Bibis boss. He treats his eggs with respect and claims that theyre very old, something that Bibi and her siblings can account to.

*Farmer: The farmer was supposed to, unknowingly, transport Toto to the farm in his truck, but Toto decided to save Willie and so the farmer left alone.

==Sequel==
A sequel to this film, Otra Película de Huevos y un Pollo was released in 2009.

==Videogame==
The first videogame based on Una película de huevos was released in April, 2010. The game, called Un Juego de Huevos in Mexico and Um Jogo de Ovos in Brazil, was designed specifically for the Zeebo system, a 3G wireless-enabled entertainment and education platform from Zeebo Inc. currently available in Mexico and Brazil. Un Juego de Huevos features animations by Huevocartoon and voice-overs by the same actors who voiced Una película de huevos.     

==References==
 

==External links==
*  
*  

 
 
 
 
 