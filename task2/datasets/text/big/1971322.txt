Elmer's Pet Rabbit
 

{{Infobox Hollywood cartoon cartoon_name = Elmers Pet Rabbit series = Merrie Melodies (Bugs Bunny/Elmer Fudd) image = Elmers Pet Rabbit.png caption = Lobby card director = Charles M. Jones story_artist = Rich Hogan animator = Rudy Larriva (uncredited)  Ken Harris   Bob Cannon   Ben Washam   Phil Monroe layout_artist = John McGrew background_artist = Paul Julian voice_actor = Mel Blanc (uncredited) Arthur Q. Bryan (uncredited) musician = Carl W. Stalling producer = Leon Schlesinger distributor = Warner Bros. release_date = January 4, 1941 color_process = Technicolor runtime = 7:41 movie_language = English
}} directed by Chuck Jones and written by Rich Hogan. Voices are provided by Mel Blanc and Arthur Q. Bryan. It was produced by Leon Schlesinger.

==Plot==
Elmer buys Bugs Bunny in a pet shop (for 98¢). When they get home, Elmer builds an enclosure for Bugs, and then serves him dinner (a bowl of vegetables) which Bugs acts awfully towards. Then Bugs is seen grumbling in the night and he eventually takes Elmers bed as his own. Throughout the short, Bugs irritates Elmer in various ways - from dancing to attempts getting in the shower, etc. - which culminates when Elmer severely attacks Bugs (in a dark room with humorous fireworks exploding) and sending him out of the house. Only does Bugs manage to get inside and reclaim Elmers bed.

==Evolution of Bugs Bunny==
Bugs Bunnys voice is pitched noticeably lower than in later incarnations of the character. His character is also very different from the more familiar version of himself (and even the earlier prototype versions), having a much more aggressive, arrogant, almost thuggish personality rather than his usual fun loving and comic relief personality. This short is the only one where Bugs has yellow gloves instead of white and no front teeth and claims to not eat carrots (yet he eats them and other vegetables while complaining).

 

==The song== While Strolling Through the Park One Day," arranged by Carl Stalling, performed by Elmer and the rabbit. Elmer, of course, has trouble with many of the words, due to his "rounded L and R" speech impediment.

==External links==
*  
*  

 
{{succession box before = A Wild Hare title = Bugs Bunny Cartoons years = 1941 after = Tortoise Beats Hare}}
 

 
 
 
 
 
 
 