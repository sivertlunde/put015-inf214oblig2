Woensdag
{{Infobox film
| name           = Woensdag
| image          = WoensdagDVD.jpg
| alt            = 
| caption        = DVD released by Arts Home Entertainment
| film name      = 
| director       = Bob Embregts   Jean-Paul Arends
| producer       = Bob Embregts   Jean-Paul Arends
| writer         = Bob Embregts   Jean-Paul Arends
| screenplay     = 
| story          = 
| based on       = 
| starring       = Reine Rek   Cefas Hoofs   Corne Heyboer   Sjanett de Geus   Nadia Huffmeijer   Marisa Goedhart   Ronald van den Burg   William van der Voort
| narrator       = 
| music          = Ruud Jolie   Jorg van Beers
| cinematography = Bob Embregts   Peter Schrijvers
| editing        = Bob Embregts
| studio         = Asura Pictures
| distributor    = Arts Home Entertainment
| released       =  
| runtime        = 75 minutes
| country        = Netherlands
| language       = Dutch
| budget         = 
| gross          = 
}}

Woensdag is a 2005 horror film written and directed by Bob Embregts and Jean-Paul Arends.

== Plot ==

Eight people are chosen to compete on Camp Slasher, a new horror-themed game show in which the contestants must complete challenges while trying to avoid a masked killer, one who is based on a local urban legend. Unbeknownst to the cast and crew of the show, an actual homicidal maniac resides in the woods where they have decided to shoot, and after massacring the programs behind-the-scenes personnel, he sets his sights on the competitors.

== Cast ==

* Reine Rek
* Ronald van den Burg
* Marisa Goedhart
* Corne Heyboer
* Sjanett de Geus
* Cefas Hoofs
* William van der Voort
* Nadia Huffmeijer

== Reception ==
 slasher flick from the Netherlands which wont likely impress anyone who lives outside Holland. Its a very dull slasher and the concept has been done to death in several, much better flicks. It doesnt help that its almost impossible to see what was going on for most of the movie either".   The acting, dialogue, story, and production values were panned by Neerlands Filmdoek, which gave Woensdag a 1½, but conceded, "Nevertheless, there is still something to enjoy for lovers who have no objection to these shortcomings; the film is never boring and has enough amusing horror moments, fitted with the necessary gore".  

== References ==

 

== External links ==

*  
*   with Jean-Paul Arends at Top of the Flops

 
 
 
 
 
 
 
 
 
 
 
 
 
 