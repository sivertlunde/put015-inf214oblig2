Superhero Movie
 
{{Infobox film
| name           = Superhero Movie
| image          = superhero movie.jpg
| caption        = Theatrical release poster
| director       = Craig Mazin
| producer       = {{Plainlist| David Zucker
* Robert K. Weiss }}
| writer         = Craig Mazin
| starring       = {{Plainlist|
* Drake Bell
* Sara Paxton
* Christopher McDonald
* Kevin Hart
* Brent Spiner
* Jeffrey Tambor
* Robert Joy
* Regina Hall
* Pamela Anderson
* Leslie Nielsen }}
| music          = {{Plainlist|
* James L. Venable
* Drake Bell }}
| cinematography = Thomas E. Ackerman
| studio         = Dimension Films    
| distributor    = {{Plainlist|
* Dimension Films
* Metro-Goldwyn-Mayer  }}
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         = $35 million
| gross          = $71,237,351
}} comedy spoof David Zucker and Robert K. Weiss, and starring Drake Bell, Sara Paxton, Christopher McDonald, and Leslie Nielsen. It was originally titled Superhero! as a nod to one of David and Jerry Zuckers previous films Airplane!.
 spoof of the superhero film genre, mainly the first Spider-Man (2002 film)|Spider-Man, as well as other modern-day Marvel Comics film adaptations. The film follows in the footsteps of the Scary Movie (series)|Scary Movie series of comedies, with which the films poster shares a resemblance. It was also inspired by, and contains homages to, some of Zucker, Abrahams and Zuckers earlier spoof films such as Airplane! and The Naked Gun.

Production began on September 17, 2007, in New York. It was released on March 28, 2008 in the United States, and the UK release was June 6, 2008, and received $9,000,000 on its opening weekend and was #3 at the box office.

==Plot==
Rick Riker (Drake Bell) is an unpopular student at Empire High School who lives with his Uncle Albert (Leslie Nielsen) and Aunt Lucille (Marion Ross) and has one friend and confidant, Trey (Kevin Hart). His crush is the breathtaking Jill Johnson (Sara Paxton), but she is dating bully Lance Landers (Ryan Hansen). One day, Rick and his class go on a school field trip at an animal research lab that is run by terminally ill businessman Lou Landers (Christopher McDonald), who is Lances uncle. During the trip, Rick accidentally saturates himself in animal-attraction liquid, which causes a group of animals to hump him. This also leads a chemically enhanced radioactive dragonfly to fly onto Ricks neck and bite him.
 Xavier (Tracy media sensation, despite being unable to fly. Later, Dragonfly attempts to stop Hourglass from robbing a warehouse full of "ceryllium" as part of his evil plan but unsuccessfully fails, leaving himself injured and allowing Hourglass to escape.

Later that night, Jill is attacked by thieves, but Dragonfly saves her and they share a kiss. Meanwhile, Landers plans to construct a machine that will kill people and give him enough life energy to make him immortal. Later that night, Landers and Lance have dinner with Ricks family and Jill, but Landers secretly learns of Ricks true identity when he notices the same injuries on Rick as on Dragonfly. Making up an awkward excuse, he and Lance leave. Landers returns minutes later as Hourglass and murders Aunt Lucille. After a comic funeral, Jill meets Rick and offers to begin a relationship with him. However, Rick fears that his enemies will come for Jill if there were together, and therefore rejects Jill, leaving her hurt and furious. 

Rick decides to end his superhero career once and for all, but knowing that Hourglass would head to an awards ceremony to kill hundreds of people, he gets Albert to take him there. At the ceremony, Jill discovers that Landers is Hourglass. When Hourglass clashes with Dragonfly on a rooftop, he tries to activate his machine, but Dragonfly manages to kill him with a bomb that had been comically stuck onto his genitals after being thrown by Hourglass. Jill is thrown off the side of the building by the explosion, but Dragonfly finally manages to grow wings and save her. Jill learns Rick is Dragonfly and the two finally begin a relationship. After being thanked for saving the city, Rick flies away with Jill, but the two are unexpectedly rammed by a passing helicopter.

==Cast== Rick Riker/Dragonfly Jill Johnson Lou Landers/Hourglass Uncle Albert Trey
* Aunt Lucille Lance Landers
* Robert Joy as Stephen Hawking
* Brent Spiner as Mendel Stromm|Dr. Strom
* Jeffrey Tambor as Dr. Whitby
* Tracy Morgan as Professor Xavier
* Regina Hall as Mrs. Xavier Invisible Girl
* Simon Rex as Human Torch Wolverine
* Robert Hays as Blaine Riker
* Nicole Sullivan as Julia Riker
* Sam Cohen as Young Rick Riker
* Dan Castellaneta as Carlson
* Keith David as Chief Karlin Storm
* Miles Fisher as Tom Cruise
* Charlene Tilton as Mrs. Johnson
* Sean Simms as Barry Bonds
* Freddie Pierce as Tony Bennett
* Howard Mungo as Nelson Mandela
* Lil Kim as Xaviers daughter
* Cameron Ali Sims as Xaviers son
* Marque Richardson as Xaviers oldest son (uncredited)
* Kurt Fuller as Mr. Thompson

==Production== David Zucker.  However, it was delayed, and the film later began production on September 17, 2007 in New York, and the directors chair was shifted to Craig Mazin, with Zucker being pushed back to being a producer.       Though the film was produced in New York, the flyover scenes used as transitions in the film use footage of the business district in downtown Kansas City, Missouri.
 Fantastic Four, and Superman film series|Superman. The producer elaborated, "Its a spoof of the whole superhero genre, but this one probably has more of a unified plot, like the Naked Gun had."   

==Release==
===Critical response===
Though the film received mostly negative reviews from critics, it was more positively reviewed than previous spoofs like Meet the Spartans and Epic Movie. As of 2013, the review aggregator Rotten Tomatoes reported that 16% of critics gave the film positive reviews. The Rotten Tomatoes Consensus stated "Superhero Movie is not the worst of the spoof genre, but relies on tired gags and lame pop culture references all the same".  Metacritic reported the film had an average score of 33 out of 100, based on 14 reviews, indicating "generally negative reviews". 

===Box office performance===
On its opening weekend, the film grossed $9,510,297 in 2,960 theaters averaging to about $3,212 per venue and ranked #3 at the box office. As of June 25 it has grossed $25,881,068 in North America, and $45,285,554 overseas for a total of $71,166,622 worldwide.

===DVD release===
Superhero Movie was released on DVD July 8, 2008. It was released in the rated PG-13 theatrical version (75 min.) and the extended edition (81 min.). The extended DVD features commentary by Zucker, Weiss, and Mazin, deleted scenes, and an alternate ending. There is also a Blockbuster Exclusive version of the Film which is the PG-13 version with the bonus features on the Unrated version and even more deleted scenes.

* Audio commentary by writer/director Craig Mazin and producers David Zucker and Robert K. Weiss&nbsp;— Extended Version Only
* Deleted scenes
* Alternate ending
* Meet the Cast featurette
* The Art of Spoofing featurette
* Theatrical trailer

The European (Region 2) DVD has 15 certificate and (according to play.com) has all the features of the Extended Region 1 version.


==Music==
Sara Paxton performing the song heard during the credits, titled "I Need A Hero", which she also wrote with Michael Jay and Johnny Pedersen.

===Superhero! Song===
{{Infobox song |
| Name           = Superhero! Song
| Cover          = Drake Bell - SuperheroSong.png
| Artist         = Drake Bell
| Album          =
| Released       = April 8, 2008  (Digital download) 
| Type           = Promotional single Digital download
| Recorded       = 2007-2008
| Genre          = Pop rock
| Length         = 3:14
| Label          = Universal Republic
| Writer         = Drake Bell, Michael Corcoran
| Producer       = Backhouse Mike
| Last single    = "Leave It All to Me" (2007)
| This single    = "Superhero! Song" (2008)
| Next single    = "Terrific (song)|Terrific" (2011)
}}
 Michael Corcoran) and recorded a song for the movie entitled "Superhero! Song" during the movies post-production. Co-star Sara Paxton provided backup vocals for the song. This song can be heard in the credits of the movie, however it is credited as being titled "Superbounce". It originally appeared on Bells MySpace Music page. It was released in iTunes Store as a digital downloadable single on April 8, 2008.

==Parody targets== superhero genre but is mainly a direct parody of the first Spider-Man (2002 film)|Spider-Man.  However, the film also features some spoofs of Batman Begins, X-Men, and the Fantastic Four. The scene of the death of Bruce Waynes parents is parodied. Some of the members of the Fantastic Four are also featured in the movie.

The film also makes references and homages to other films such as when Rick Riker and Trey are in a bus and Trey is pointing out the different groups of cliques, this parodies the Mean Girls scene where Janis explains to Cady the cliques. One of the cliques is "Frodos" - kids dressed up as Hobbits looking similar to Frodo Baggins|Frodo, The Lord of the Rings character.
 alleged use of steroids. It also makes fun of British scientist, Stephen Hawking.   

==See also== Spoof film

==References==
 

==External links==
 
 
*  
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 