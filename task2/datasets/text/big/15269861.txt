K-9: P.I.
 
 
{{Infobox Film
| name           = K-9: P.I.
| image           =K-9-pi-cover-3.jpg
| director       = Richard J. Lewis
| producer       = Ron French
| writer         = Gary Scott Thompson
| starring       = James Belushi Gary Basaraba
| music          = Nick Pierone
| cinematography = Roy H. Wagner
| editing        = Ron Wisman Universal Family and Home Entertainment Production
| released       =  
| runtime        = 95 min.
| country        = United States
| language       = English
}}

K-9: P.I. is a motion picture comedy which was released direct-to-video in 2002. It was directed by Richard J. Lewis and stars James Belushi as Detective Thomas Dooley. The film serves as the sequel to the 1989 film K-9 (film)|K-9 and the 1999 film K-911.

After retiring  from the LAPD,  Detective Thomas Dooley and his Lovable K-9 Partner Jerry Lee go on one last adventure before they retire and start to enjoy the good life.

==Summary==

After retiring, Detective Dooley and Jerry Lee have a retirement party with all of their friends. After the party, Dooley and Jerry Lee are both drunk. They enter LA Micro Labs and find a dead security guard apparently shot by criminals who have stolen a chip. Jerry Lee and Dooley must now track down the criminals and retrieve the chip.Several twists,a pair of Secret Agents,a missing person and a constipated dog makes the story go.

==Cast==

*James Belushi	... 	Thomas Dooley
*Gary Basaraba	... 	Pete Timmons
*Kim Huffman	... 	Laura Fields
*Jody Racicot	... 	Maurice
*Christopher Shyer... 	Charles Thyer
*Barbara Tyson	... 	Catherine
*Blu Mankuma	... 	Captain Thomas
*Duncan Fraser	... 	Frankie the Fence
*Jason Schombing	... 	Carlos Cuesta
*Kevin Durand	... 	Agent Verner
*Matthew Bennett	... 	Agent Henry
*Jay Brazeau	... 	Dr. Tilley
*Sarah Carter	... 	Babe
*Terry Chen	... 	Sato
*Dean Choe	... 	Thief
*Michael Eklund	... 	Billy Cochran
*G. Michael Gray	... 	Junkie
*Ellie Harvie	... 	Jackie Von Jarvis
*Dee Jay Jackson	... 	Auto Pool Guy David Lewis	... 	Jack Von Jarvis
*Angela Moore	... 	Angie
*Natassia Malthe	... 	Dirty Dancer (as Lina Teal)
*King		... 	Jerry Lee

==External links==
* 
* 

 
 
 
 
 
 
 
 


 