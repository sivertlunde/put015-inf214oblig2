The Dark Eyes of London (film)
 
 
{{Infobox film
| name           = The Dark Eyes of London
| image          = The Human Monster.jpg
| image_size     =
| border         =
| alt            =
| caption        = Theatrical poster under its American title The Human Monster
| director       = Walter Summers
| producer       = John Argyle
| writer         = John Argyle Patrick Kirwan Walter Summers Jan Van Lusil
| screenplay     =
| story          =
| based on       =   Rhodes, 2006. p. 115 
| narrator       = Alexander Field
| music          = Guy Jones
| cinematography = Bryan Langley
| editing        =
| studio         = Argyle Film
| distributor    =
| released       =  
| runtime        = 75 minutes
| country        = United Kingdom
| language       = English
| budget         =
| gross          =
}}
 British horror 1924 novel of the same name by Edgar Wallace. The film is about a scientist named Dr. Orloff who commits a series of murders for insurance money, while periodically disguising himself as the blind manager of a charity to further his scheme.

It was released in November 1939 in Britain, where it became the first British film to receive the "H" rating for "Horrific". It was released in the USA in 1940 by Monogram Pictures under the title of The Human Monster.

==Plot==
In London, Dr. Orloff (Bela Lugosi) runs an insurance agency where he loans money in exchange for the assigning of life insurance benefits to himself of many of his customers policies. A resident of Orloffs (under the name "Dearborn") home for the "Destitute Blind" is Jake, a tall, hideous brute, who murders on Orloffs behalf. Scotland Yard begins finding bodies in the Thames River. One of the dead men has a daughter named Diane (Greta Gynt) for whom Orloff obtains employment at the home for the blind. Suspicions begin to arise surrounding Dearborn and Orloff in relation to the dead bodies. Orloff sends Jake (Wilfred Walter) to kill Diane who has found out too much about them. When Orloff disappears, Diane finds one of her fathers cuff links in the home from the blind. Confronted by Diane, Dearborn removes his disguise to show himself as Orloff. Orloff traps Diane with a straight-jacket and calls for Jake to finish the job. Jake refuses as he has found out that Orloff has murdered a blind friend of his. Jake turns on Orloff and throws him out of a window allowing him to sink in the mud below.

==Production==
  dubbed the voice of Professor Dearborn]]
Production began on The Dark Eyes of London in 1938.    Actor Bela Lugosi sailed to England to star in the films dual role of Dr. Orloff and Professor Dearborn. (The original novel has two Orloff characters but the films script combined them into one.)  When portraying the role of Dearborn, Lugosis voice is dubbed by O. B. Clarence.  The day after Lugosi arrived on set, shooting began and lasted 11 days.  Production ended on the film in 1939. 

The final scene involving Orloffs demise was difficult to film. A seven-foot-deep tank was filled with a concoction to resemble river mud. A member of the crew was lowered into the mess with a chain that allowed him escape easily if he were sucked under. It is intercut with a shot of Lugosis head sinking, but Lugosi did not submerge. Weights had to be tied to Lugosis ankles to keep his body sunk.  

==Release== British Board of Film Censors (BBFC) for films labelled "Horrific" for "any films likely to frighten or horrify children under the age of 16 years" 

The H certificate was replaced on 1 January 1951 by the X certificate.  The film was released in the United States under the title of The Human Monster by Monogram Pictures in March 1940. 

== See also ==
 
*Bela Lugosi filmography
*British films of 1939
*List of horror films of the 1930s

==References==
 

==Notes==
*  
*  
*  
*  

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 