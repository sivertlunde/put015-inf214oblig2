Char Dil Char Rahen
{{Infobox film
| name           = Char Dil Char Rahen
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = 
| director       =Khwaja Ahmad Abbas
| producer       =Khwaja Ahmad Abbas (Naya Sansar)
| writer         = Inder Raj Anand V. P. Sathe
| screenplay     = Khwaja Ahmad Abbas
| story          = 
| based on       =Char Dil Char Rahen  
| narrator       =  Ajit  Shammi Kapoor Meena Kumari Anil Biswas Sahir Ludhianvi
| cinematography = S. Ramachandra
| editing        = 
| studio         = 
| distributor    = 
| released       = 1959 
| runtime        = 160 min.
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}}
 1959 Hindi film directed by Khwaja Ahmad Abbas, and starring two big stars of the era, Shammi Kapoor and Raj Kapoor. The movie is based on a novel of the same name. It was released simultaneously with other big films,  Devendra Goels Chirag Kahan Roshni Kahan and V Shantarams Navrang, while Navrang was a hit, Chirag Kahan Roshni Kahan broke even, Char Dil Char Rahen failed at the box office.    

==Plot==
The story revolves around three love stories that run simultaneously among. The background of the movie is a construction of a crossroad with 4 roads and all people are related to the road that is being built and are involved. How the love stories unfold forms the crux of the movie.

==Cast==
* Raj Kapoor as Govinda Ajit as Dilawar
* Shammi Kapoor as Johnny Braganza
* Meena Kumari as Chavli Kumkum as Stella DSouza
* Nimmi as Pyari Anwar Hussain as Nawab Saab David Abraham as Ferreira
* Nana Palsikar as Pujariji
* Achala Sachdev
* P. Jairaj as Nirmal Kumar
* Rashid Khan
* Kumari Naaz (as Baby Naaz)

==Controversy==
Shammi Kapoor received a legal notice from director Abbas when he refused to act for one of the songs in the film and many other controversies with the stars of that era that director Abbas vowed to stop making movies for mainstream movie stars. 

==References==
 

==External links==
*  

 

 
 
 
 
 
 


 