The Swap
{{Infobox film
| name           = The Swap
| image_size     =
| image	=	The Swap FilmPoster.jpeg
| caption        =
| director       = Jordan Leondopoulos (credited as John Shade)
| producer       =
| writer         = John C. Broderick (screenplay)
| narrator       =
| starring       = See below
| music          = Bill Conti
| cinematography = Álex Phillips Jr.
| editing        =
| distributor    =
| released       = 1979
| runtime        = 83 minutes
| country        = USA
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

The Swap is a 1979 American film directed by Jordan Leondopoulos. The film is edited from the 1969 film Sams Song also directed by Leondopoulos.

The film is also known as Line of Fire (American video title).

== Plot summary ==
The film opens as Sam Nicoletti (Robert de Niro) works in his office where he is a film editor (in the original lovie Sams Song he is working on a documentary about Nixon but here he is working on a porno film) , little knowing there is an intruder. After talking on the phone, Sam is knocked out from behind by the intruder.

After the credits the film cuts to ten years later. Sams brother Vito is being released from prison for an unspecified crime. he sets out to find out who killed Sam and why. He goes to visit Erica Moore, a publisher, who he knows spent time with Sam in his last days. She tells him how her husband had an affair with Carole, a girl Sam was dating, and ended up marrying Carole. In flashbacks we learn more about a weekend that Sam spent with rich friends Warren and Mickey.

From a caretaker in the cemetery where Sam is buried, Vito learns that a girl has been visiting Sams grave for the last ten years. He confronts here when she visits, but she drives off. Tracing her licence plate, he traces her to her home, where he reveals he is Sams brother. Vivian reveals that she was in Sams porno films. Vito is surprised as he was unaware of Sams involvement in that business.

Further complexities lead Vito to Warren and his wife, who both have reason to prevent the porno film seeing the light of day. Vito gets his hands on the reel of film and has it processed, while Vivian transports him around. Warrens wife seduces Vito at his hotel but slips him a mickey, later kidnapping him at gunpoint. But Vito grabs her, turns the gun on her and she is killed.

Vito himself is wounded but goes to the film processing place where with Vivian he watches the porno film and sees that Warren was in it. Vivian drives him to Warrens where he shoots Warrens houseboy, and finds Warren in the bathtub. Warren pleads for his life but Vito shoots him.

The film ends as Vivian drives Vito away. She thinks they can make it to the mountains where she can get him a doctor, but he is clearly dying. She tells him that she loves him.

== Cast ==
*Robert De Niro as Sam Nicoletti (1969 scenes) (archive footage)
*Jennifer Warren as Erica Moore (1969 scenes) (archive footage)
*Jarred Mickey as Andrew Moore (1969 scenes) (archive footage) (as Jerry Micky)
*Terrayne Crawford as Carole Moore (1969 scenes) (archive footage)
*Martin J. Kelley as Mitch Negroni (1969 scenes) (archive footage) (as Martin Kelley)
*Anthony Charnota as Vito Nicoletti (new scenes)
*Lisa Blount as Vivian Buck (new scenes)
*Sybil Danning as Erica Moore (new scenes)
*John Medici as Joey (new scenes) James Brown as Lt. Benson (new scenes)
*Sam Anderson as Paul (new scenes)
*Tony Brande as Father Testa (new scenes)
*Matt Greene as Marges Assistant (new scenes) (as Matthew Greene)
*Alvin Hammer as Cemetery Caretaker (new scenes)
*Jack Slater as Party Guest (new scenes)
*Phyllis Black as Marge Negroni (1969 scenes) (archive footage) (uncredited) Viva as Girl with the hourglass (1969 scenes) (archive footage) (uncredited)

==Production==

In 1969 a young Robert De Niro starred in a drama called Sams Song. It made little profit but it got him noticed. Ten years later, a filmmaker wanted to get De Niro onto his project but didnt have the resources to do it. So he took to stealing much of Sams Song & incorporating it into his own film. The result was The Swap.

Robert De Niro was angered that Cannon Films took his previously shot footage and inserted it into an entirely new movie - legal action was almost taken.

== External links ==
* 
* 

 
 
 
 
 
 
 


 