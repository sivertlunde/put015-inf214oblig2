Fast Times at Ridgemont High
 
 

{{Infobox film
| name           = Fast Times at Ridgemont High
| image          = Fast Times at Ridgemont High film poster.jpg
| caption        = Theatrical release poster
| director       = Amy Heckerling
| producer       = Art Linson Irving Azoff
| screenplay     = Cameron Crowe
| based on       =  
| starring       = {{Plain list | 
* Sean Penn
* Jennifer Jason Leigh
* Judge Reinhold
* Phoebe Cates
* Brian Backer
* Robert Romanus
* Ray Walston 
}}
| cinematography = Matthew F. Leonetti
| editing        = Eric Jenkins
| studio         = Refugee Films
| distributor    = Universal Studios
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = $4.5 million
| gross          = $27,092,880  (domestic) 
}} teen comedy film written by Cameron Crowe, adapted from his 1981 book of the same name. As a freelance writer for Rolling Stone magazine, Crowe went undercover at Claremont High School in San Diego, California,   and wrote about his experiences.
 directorial debut) stoned surfer, facing off against uptight history teacher Mr. Hand (Ray Walston), and Stacys brother, Brad (Judge Reinhold), a senior who works at a series of entry-level jobs in order to pay off his car, and who is pondering easing out of his relationship with his girlfriend, until she herself dumps him.

In addition to Penn, Reinhold, Cates and Leigh, the film marks early appearances by several actors who later became stars, including Nicolas Cage, then billing himself as Nicolas Coppola, Forest Whitaker, Eric Stoltz, and Anthony Edwards. Among the actors listed, Penn, Cage, and Whitaker would later on in their careers win the Academy Award for Best Actor, with Penn winning twice.

In 2005, Fast Times at Ridgemont High was selected for preservation in the United States National Film Registry by the Library of Congress as being "culturally, historically, or aesthetically significant".

==Plot==
Brad Hamilton is a popular senior who is looking forward to his last year of school and almost has his 1960 Buick LeSabre paid off. He has a part-time job at a burger joint where his girlfriend, Lisa, also works. He is pondering how to end his relationship with Lisa so he can play the field during his senior year. However, Brad is fired for losing his temper at an obnoxious customer; and when he tries to tell Lisa how much he needs her, she says she is dumping him to see other guys. Brad quits his next job because of the humiliation of having to wear a pirate costume when delivering food. He gets a job at a convenience store, where he successfully thwarts an attempted robbery and is promoted to store manager.

Brads sister, Stacy Hamilton, is a 15-year-old sophomore. She is worried that she is not attractive or experienced enough to even meet, let alone have a relationship with, a guy. While working at the mall she meets a 26-year-old stereo salesman who asks her out, after she tells him she is 19. She sneaks out to meet him and they have sex in a dugout at a softball field. Though he sends her a bouquet of roses the next day, she never hears from him again. She reveals the loss of her virginity to her friend, popular (and sexually experienced) senior Linda, who assures her that it will hurt less as she does it more.
 taking bets scalping tickets, fancies himself a ladies man. He lets his younger friend Mark Ratner in on his secrets for picking up girls. Mike coaxes Mark into asking Stacy out to dinner at an upscale restaurant. Stacy invites Mark into her bedroom after the date, but he quickly shies away and leaves when Stacy tries to initiate sex with him.

Later, Damone goes to Stacys house for a swim in the backyard pool, which leads to sex in the pool house with Stacy. When Stacy later informs Damone that she is pregnant, he agrees to pay half the cost of an abortion and to drive her to the clinic. But on the day of her appointment he ignores her, refusing to answer her calls. Desperate, Stacy asks Brad to drive her to a bowling alley, but after he drops her off, Brad sees her going to the abortion clinic across the street. He goes back and waits for her, and she makes him promise not to tell their parents. Word gets around school about Damone and Stacy, which leads to a confrontation between Damone and Mark in the boys locker room.

Jeff Spicoli is a surfer and habitual marijuana user who runs afoul of strict history teacher, Mr. Hand, who is intolerant of Spicolis hedonistic attitude and immaturity. Finally, on the evening of the graduation dance, Mr. Hand shows up at Spicolis house and informs him that since he has wasted eight hours of class time over the past year, Mr. Hand intends to make up for it that night. They proceed to have a one-on-one session that lasts until Mr. Hand is satisfied that Spicoli has understood the lesson. Spicoli wrecks Ridgemont football player Charles Jeffersons 1979 Chevrolet Camaro Z28 during a joyride with Jeffersons younger brother. Spicoli decides to park the car in front of the school with slurs painted on it supposedly written by Ridgemonts rival, Lincoln High. When Ridgemont plays Lincoln, Jefferson is so angry about his car that he thrashes several of Lincolns players and wins the game for Ridgemont in a blowout.

In addition to Mark and Stacys continuing relationship and Brads promotion to manager at the convenience store, fates of some of the other characters are revealed in subtitles: Spicoli saves Brooke Shields from drowning and then spends the reward money hiring Van Halen to play at his birthday party; Linda gets accepted to UC Riverside and moved in with her Abnormal Psychology professor; Damone gets arrested for scalping Ozzy Osbourne tickets; and Mr. Hand still believes everyone is on drugs.

==Cast==
* Sean Penn as Jeff Spicoli
* Jennifer Jason Leigh as Stacy Hamilton
* Judge Reinhold as Brad Hamilton
* Robert Romanus as Mike Damone
* Brian Backer as Mark "Rat" Ratner
* Phoebe Cates as Linda Barrett
* Amanda Wyss as Lisa, Brads girlfriend
* Ray Walston as Mr. Hand
* Forest Whitaker as Charles Jefferson Scott Thomson as Arnold
* Vincent Schiavelli as Mr. Vargas
* Lana Clarkson as Mrs. Vargas
* Eric Stoltz as Stoner Bud
* Anthony Edwards as Stoner Bud Nicolas Coppola as Brads Bud 
* Taylor Negron as Mr. Pizza Guy
* James Russo as Convenience store robber

==Soundtrack==
{{Infobox album  
| Name        = Fast Times at Ridgemont High: Music from the Motion Picture
| Type        = Soundtrack
| Longtype    =
| Artist      = various artists
| Cover       = Fasttimesatridgemonthighsoundtrack.jpg
| Released    = July 30, 1982
| Recorded    = Pop
| Length      = 65:50 Elektra
| Producer    =
| Last album  =
| This album  =
| Next album  =
}}
{{Album ratings
| rev1      = Allmusic
| rev1Score =   
}}
The soundtrack album, Fast Times at Ridgemont High: Music from the Motion Picture, peaked at #54 on the Billboard (magazine)|Billboard album chart.  The soundtrack features the work of many quintessential 1980s rock artists.

Several of the movies songs were released as singles, including Jackson Brownes "Somebodys Baby", which reached #7 on the Billboard Hot 100|Billboard Hot 100 singles chart.  Other singles were the title track by Sammy Hagar, "So Much in Love" by Timothy B. Schmit, "Raised on the Radio" by The Ravyns and "Waffle Stomp" by Joe Walsh. In addition to Schmit and Walsh, the album features solo tracks by two other members of the Eagles (band)|Eagles, Don Henley and Don Felder. The soundtrack also included "I Dont Know (Spicolis Theme)" by Jimmy Buffett.

Five tracks in the film, but not included on the soundtrack, are: " " and "Wooly Bully".

The Donna Summer track, "Highway Runner", was initially recorded in 1981 for her double album entitled Im a Rainbow; however, the album was shelved by Summers then-label, Geffen Records, but ultimately released in 1996 by Mercury Records.  The album is once again out of print.

Todd Rundgren also recorded the song, "Attitude", for the film at Crowes request. It was not included in the film, but was later released on Rundgrens Demos and Lost Albums in 2001.

Amy Heckerling, in the DVD audio commentary, states that the 1970s "classic rock" artists, like the Eagles (band)|Eagles, were introduced by one of the films producers. Coincidentally, Irving Azoff, one of the movies producers, was the personal manager for the Eagles.

{{track listing |extra_column=Performer
|title1=Somebodys Baby |extra1=Jackson Browne |length1=4:05
|title2=Waffle Stomp |extra2=Joe Walsh |length2=3:40
|title3=Love Rules |extra3=Don Henley |length3=4:05
|title4=Uptown Boys |extra4=Louise Goffin |length4=2:45
|title5=So Much In Love |extra5=Timothy B. Schmit |length5=2:25
|title6=Raised on the Radio |extra6=The Ravyns |length6=3:43
|title7=The Look In Your Eyes |extra7=Gerard McMahon |length7=4:00
|title8=Speeding |extra8=The Go-Gos |length8=2:11
|title9=Dont Be Lonely |extra9=Quarterflash |length9=3:18
|title10=Never Surrender |extra10=Don Felder |length10=4:15
|title11=Fast Times (The Best Years of Our Lives) |extra11=Billy Squier |length11=3:41
|title12=Fast Times at Ridgemont High |extra12=Sammy Hagar |length12=3:36
|title13=I Dont Know (Spicolis Theme) |extra13=Jimmy Buffett |length13=3:00
|title14=Love Is the Reason |extra14=Graham Nash |length14=3:31
|title15=Ill Leave It Up To You |extra15=Poco |length15=2:55
|title16=Highway Runner |extra16=Donna Summer |length16=3:18
|title17=Sleeping Angel |extra17=Stevie Nicks |length17=3:55
|title18=Shes My Baby (And Shes Outta Control) |extra18=Palmer/Jost |length18=2:53
|title19=Goodbye, Goodbye |extra19=Oingo Boingo |length19=4:34
}} 

==Origins and production==
The film is adapted from a book Crowe wrote after a year spent at  , on whom the character "Rat" was modeled.  
 Nancy Wilson of Heart (band)|Heart, has a cameo as "Beautiful Girl in Car".

===Filming locations===
  Los Angeles (although it is never explicitly mentioned as such in the film) in late 1981, and many people identify the movie with that area and the teen culture that existed there, or was perceived to, in the early 1980s. "Ridgemont" is a fictional name. Crowe applied it to Clairemont High School in San Diego where he attended the school undercover. (Spicoli mentions surfing at Sunset Cliffs, a genuine surf spot in San Diego.) Most of the exteriors of Ridgemont High School were shot at Van Nuys High School, and other scenes were shot at Canoga Park High School. The "carrot" scene and football game were shot at James Monroe High School in North Hills, Los Angeles|Sepulveda, CA. The  "Ridgemont Mall" shown in the film was actually the Sherman Oaks Galleria, with its exterior shot at Santa Monica Place. Both have since been converted to open-air malls.  "The Point" was filmed at the Encino Little League Field in Encino, Los Angeles|Encino.
 West Hills because she thought the neighbors might be spying on the set from the surrounding rooftops.

==Reception==

===Box office===
Universal Pictures gave it a limited theatrical release on August 13, 1982, opening in 498 theaters. It earned $2.5 million in its opening weekend. The release was later widened to 713 theaters, earning $3.25 million. The film ranked 29th among US releases in 1982, ultimately earning more than $27 million,  six times its $4.5 million budget, and later gaining popularity through television and home video releases.

Over the years the film has obtained an iconic status. In an interview, Sean Penn stated "None of us had any idea it would take on a life of its own."

===Critical response===
The film has an 79% "Certified Fresh" rating on Rotten Tomatoes based on 48 reviews, with the sites consensus stating "While Fast Times at Ridgemont High features Sean Penns legendary performance, the film endures because it accurately captured the small details of school, work, and teenage life." 

It was panned by critics at the time. Roger Ebert called it a "scuz-pit of a movie", though he praised the performances by Leigh, Penn, Cates and Reinhold.  Janet Maslin wrote that it was "a jumbled but appealing teen-age comedy with something of a fresh perspective on the subject." 

===Accolades===
   WGA Award Entertainment Weeklys list of the "50 Best High School Movies". 

American Film Institute recognition
* 2000: AFIs 100 Years... 100 Laughs #87

==Television spinoff== Wally Ward as Mark, Claudia Wells (Back to the Future) as Linda, Patrick Dempsey (Greys Anatomy) as Mike Damone, Dean Cameron as Jeff Spicoli and James Nardini as Brad. Kit McDonough played teacher Leslie Melon, a character that was exclusive to the series.

Moon Zappa was a "technical consultant" for the television series.  She was hired in order to research slang terms and mannerisms of teenagers, as she had just graduated from high school at the time and had a much better grasp of then-current high school behavior than the writers, and Danny Elfman (Oingo Boingo) wrote and performed the television series theme.

==See also==
 
* Fast Times at Barrington High, an album by the band The Academy Is... is a play on the title of the film.
* "Fast Times at Buddy Cianci Jr. High", a Family Guy episode from Season 4
* Fast Times at Fairmont High, a novella by Vernor Vinge, is named in reference to the film.
*"Stacys Mom", a song by Fountains of Wayne which pays homage to the movie.

==References==
 

==External links==
 
*  
*  
*  
*  
*  
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 