Veerabhadran
{{Infobox film
| name = Veerabhadran
| image =
| caption =
| director = N Sankaran Nair
| producer = LN Potti
| writer = VT Nandakumar Thoppil Bhasi (dialogues)
| screenplay = LN Potti Hari Ambika Ambika Lalu Alex
| music = G. Devarajan
| cinematography = Jayanan Vincent
| editing = Sasikumar
| studio = Lakshminarayana Pictures
| distributor = Lakshminarayana Pictures
| released =  
| country = India Malayalam
}}
 1979 Cinema Indian Malayalam Malayalam film, Ambika and Lalu Alex in lead roles. The film had musical score by G. Devarajan.   

==Cast==
*Sukumari  Hari  Ambika 
*Lalu Alex 
*Nellikode Bhaskaran 
*Rama

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by LN Potti. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Kanna kanna || Rajalakshmi || LN Potti || 
|- 
| 2 || Karakaanaakkadal || Suryakumar || LN Potti || 
|- 
| 3 || Premanjanakkuri || Rajalakshmi || LN Potti || 
|- 
| 4 || Vadaamallippoovukale || Suryakumar || LN Potti || 
|}

==References==
 

==External links==
*  

 
 
 


 