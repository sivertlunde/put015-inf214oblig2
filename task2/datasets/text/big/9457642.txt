Man Trouble
 
{{Infobox film
| name           = Man Trouble
| image          = Mantrouble.jpg
| caption        = Theatrical release poster
| director       = Bob Rafelson
| producer       = Vittorio Cecchi Gori Carole Eastman Bruce Gilbert
| writer         = Carole Eastman
| starring       = Jack Nicholson Ellen Barkin Harry Dean Stanton Beverly DAngelo Michael McKean Saul Rubinek Veronica Cartwright
| music          = Georges Delerue
| cinematography = Stephen H. Burum
| editing        = William Steinkamp
| distributor    = 20th Century Fox
| released       = July 17, 1992
| runtime        = 100 minutes
| country        = United States English
| budget         = $30 million
| gross          = $4,096,030
|}} 1992 romantic comedy starring Jack Nicholson and Ellen Barkin. It was directed by Bob Rafelson, and written by Carole Eastman, who together had been responsible for 1970s Five Easy Pieces. This film is held in somewhat lower regard and was a high-profile flop upon release.

This comical thriller is the fifth collaboration between Nicholson and Rafelson. Beverly DAngelo and Harry Dean Stanton co-star.

==Plot==
Harry Bliss (Nicholson) runs a guard dog service and is going through counseling with his wife, Adele (Lauren Tom). A serial killer is on the loose in Los Angeles, so when the apartment of classical singer Joan Spruance (Barkin) is ransacked and she starts receiving threatening phone messages, Joan moves into the Hollywood Hills home of her sister, Andy (DAngelo).

Joan doesnt feel safe there, either, because shes harassed by Andys ex-lovers. She hires a guard dog from Harrys company, and soon Harry is providing more than protection for the beautiful singer.

Harry is a natural-born liar who, because of his profession, feels that he lives by a code of honor — even if he cant quite explain it — as one thing after another spins out of his control. Joan is soft and vulnerable as she is badgered by her conductor husband, harassed by unknown callers, menaced by men from her sisters past, and "helped" by Harry.

==Cast==
*Jack Nicholson as Harry Bliss
*Ellen Barkin as Joan Spruance
*Harry Dean Stanton as  Redmond Layls
*Beverly DAngelo as Andy Ellerman
*Michael McKean as Eddy Revere
*Saul Rubinek as Laurence Moncreif
*Paul Mazursky as Lee MacGreevy
*Lauren Tom as Adele Bliss
*Viveka Davis as June Huff
*Veronica Cartwright as Helen Dextra
*David Clennon as Lewie Duart
*John Kapelos as Detective Melvenos
*Gary Graham as Butch Gable

==Reception==
Man Trouble was not well received by the majority of critics. It currently holds a 7% "Rotten" rating on Rotten Tomatoes based on 30 reviews. 

Variety (magazine)|Varietys review said that "Jack Nicholson fans should feel cheated by Man Trouble, an insultingly trivial star vehicle. After some initial business attracted by his name on the marquee, film is fated for pay-cable use."

Film Fours review stated: "Sold on the proven teamwork of director Rafelson and actor Nicholson -- who had previously worked together on Five Easy Pieces, The King of Marvin Gardens and The Postman Always Rings Twice -- this romantic comedy proved to be one of their least inspired collaborations. Nicholson plays a grouchy dog-trainer who slowly loosens up in the presence of romantically challenged opera singer Barkin, who needs to get some canine security after a series of death threats. Obviously intended to be a bright and breezy romantic-comedy thriller, it ends up a mangy old mutt of a movie thanks to a charmless script and disastrous casting decisions."

According to Time out Magazine: "The trouble is, the film never seems to know where its headed. Not quite a romance, a thriller or a comedy, its a movie with an on-going identity crisis. Barkin, playing against type, produces a shrill caricature of femininity, while Rafelson indulges Nicholsons familiar soft-spoken laxity, another of his personable rogues."

Jack Nicholson earned a Golden Raspberry Award nomination for Worst Actor for his performances in both this film and Hoffa, but lost the trophy to Sylvester Stallone for Stop! Or My Mom Will Shoot.

==References==
 

==External links==
*   At  
*  

 
 
 
 
 
 
 
 
 