Sedona (film)
{{Infobox film
| name           = Sedona
| image          = Sedona_Poster.jpg
| caption        = Theatrical film poster
| director       = Tommy Stovall
| producer       = Marc S. Sterling Tommy Stovall Ebony Tay
| writer         = Tommy Stovall
| starring       = Frances Fisher Seth Peterson Beth Grant Matt Williamson
| music          = Ebony Tay
| cinematography = Rudy Harbon
| editing        = Tommy Stovall
| studio         = Pasidg Productions Inc. 
| released       =  
| runtime        = 87 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = US$21,638   
}}
Sedona is a 2011 comedy-drama film about the experiences of an advertising executive and a lawyer in Sedona, Arizona. Released theatrically in 2012, the films cast includes Frances Fisher, Seth Peterson, Barry Corbin, Christopher Atkins, Lin Shaye, and Beth Grant.   

==Plot== Phoenix to land the biggest client of her career until a wrong turn gets her lost and takes her into Sedona by mistake, where she is run off the highway by a tour plane making an emergency landing and right in the middle of town, and so she is forced to wait in Sedona while her damaged car is being repaired. Across town, a lawyer named Scott (Seth Peterson) is hiking in the woods with his partner Eddie (Matt Williamson) and their two young sons. Eddie brought the workaholic Scott to Sedona for some rest and relaxation, but wind up in an unexpected adventure when 7-year-old Denny (Trevor Sterling Stovall) gets lost. The ensuing frantic search for Denny sends Scott into a profound life-changing journey that forces him to examine his own priorities and determine whats truly important in his life. Meanwhile, Tammy unwittingly begins a spiritual transformation of her own as she encounters an array of the townspeople, each of them touching her in different meaningful ways. A chain of coincidences spirals her out of control, leaving her haunted by her past and forcing her to face her demons head on. Letting her guard down, she confronts the implausible possibility that she has been brought to Sedona for a reason.

==Cast==
* Frances Fisher as Tammy
* Seth Peterson as Scott
* Beth Grant as Deb Lovejoy
* Matt Williamson as Eddie
* Trevor Sterling Stovall as Denny
* Rand Schwenke as Jeremy
* Christopher Atkins as Pierce
* Barry Corbin as Les
* Tatanka Means as Chuck
* Lin Shaye as Claire
* Kylee Cochran as Alana
* Robert Shields as Sky
* Rachel Reenstra as Linda
* Andy Ridings as Pat

==Reviews==
TV Guides Jason Buchanan said viewers "will likely walk away smiling thanks to stunning cinematography that gives a genuine sense of place, two skillfully interwoven storylines, convincing performances by a talented cast, and a fantastic portrayal of two loving, supportive gay parents that makes their sexuality a complete non-issue."     Rotten Tomatoes Critic Mark Leeper gave Sedona a score of 7 out of 10.   

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 