Kuroneko
 
{{Infobox film
  | image = Kuronekoposter.jpg
  | caption = Mexican poster for Kuroneko
  | director = Kaneto Shindo
  | producer = Nichiei Shinsha
  | writer = Kaneto Shindo Kichiemon Nakamura Nobuko Otowa Kiwako Taichi
  | music = Hikaru Hayashi
  | cinematography = Kiyomi Kuroda
  | editing = Hisao Enoki
  | distributor =
  | released =  
  | runtime = 95 min.
  | language = Japanese
  | budget =
  | country = Japan studio = Kindai Eiga Kyokai
  }}
  is a 1968 black-and-white Japanese horror film, directed by Kaneto Shindo, and an adaptation of a supernatural folktale. Set during a civil war in Japans Heian period, the spirits of a woman and her daughter-in-law seek revenge after losing their lives to a brutal incident.

== Plot ==
Yone (Nobuko Otowa) and her daughter-in-law Shige (Kiwako Taichi), who live in a house in a bamboo grove, are raped and murdered by soldiers, and their house is burned down. A black cat appears, licking at the bodies.

The women return as ghosts with the appearance of fine ladies, who wait at Rajōmon. They find samurai and bring them to an illusory mansion in the bamboo grove where the burnt-out house was. They seduce and then kill the samurai like cats, tearing their throats with teeth.
 northern Japan a battle is taking place with the Emishi. A young man, Hachi (Nakamura Kichiemon II), fortuitously kills the enemy general, Kumasunehiko. He brings the severed head to show the governor, Minamoto no Raikō (Kei Sato). He lies that he fought the general under the name Gintoki. He is made a samurai in acknowledgement of his achievement. When he goes looking for his mother and bride, he finds their house burned down and the women missing.

Raikō tells Gintoki to find and destroy the ghosts who are killing the samurai. Gintoki encounters the two women and realizes that they are his mother and wife. They have made a pact with the underworld to return and kill samurai in revenge for their deaths. Because Gintoki has become a samurai, by their pact they must kill him, but the bride breaks her pledge to spend seven nights of love with Gintoki. Then, because she has broken the pact, she is condemned to the underworld. Gintoki lies to Raikō that he has destroyed one of the ghosts.

Gintoki encounters the other ghost again at Rajōmon trying to seduce samurai. After seeing her reflection as a ghost in a pool of water, he attacks her with his sword, cutting off her arm, which takes on the appearance of a cats leg. She flees but then returns to retrieve the arm, then disappears by flying through a roof. Finally Gintoki is left flailing his sword around in the illusory mansion. The mansion disappears, and the film ends with Gintoki lying face up in the snow with his sword in hand.

== Themes ==
 film version of the story.    Although the Japanese title literally means "a black cat in a bamboo grove", the phrase yabu no naka in Japanese is also used idiomatically to refer to a mystery that is difficult to unravel. Suzumura also identified the legends of Minamoto no Raikō as an influence on the film: since Raikō himself appears in the film, it is likely that the films protagonists name   is a reference to the name of Raikōs legendary follower  . 

== Reception ==

It was placed in competition at the 1968 Cannes Film Festival,    but the festival was cancelled due to the events of May 1968 in France.

===Critical reception===

Manohla Dargis describes it as "a ghost story that’s more eerie than unnerving, and often hauntingly lovely".  Maitland McDonagh writes that it is "darkly seductive" and "sleek, hair-raisingly graceful, and ready to take its place alongside the other landmarks of Japanese horror history". 

== Cast == Kichiemon Nakamura: Gintoki
* Nobuko Otowa: Yone (Mother)
* Kiwako Taichi: Shige (Daughter-in-Law)
* Kei Sato: Raiko
* Taiji Tonoyama: A Farmer
* Rokko Toura: A Samurai
* Hideo Kanze: Mikado
* Hideaki Esumi
* Masashi Oki

== References ==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 