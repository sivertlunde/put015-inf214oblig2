Across the Bridge (film)
 
 
{{Infobox film
| name           = Across the Bridge
| image_size     = 
| image	=	Across the Bridge FilmPoster.jpeg
| caption        = 
| director       = Ken Annakin
| producer       = John Stafford
| writer         = Guy Elmes Dennis Freeman
| based on       =  
| starring       = Rod Steiger James Bernard
| cinematography = 
| editing        = 
| distributor    =  
| released       =  
| runtime        = 103 min.
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}

Across the Bridge is a 1957 British film directed by Ken Annakin. It is based on the sort story "Across the Bridge" by Graham Greene. It stars Rod Steiger and Bernard Lee.

==Plot==

Carl Schaffner (Steiger) is a crooked English (previously German) businessman who flees to Mexico after stealing company funds. While travelling by train, Schaffner decides to evade authorities. He drugs and switches identities with fellow train passenger Paul Scarff, who looks like him and has a Mexican passport. He throws Paul Scarff off the train, injuring Scarff. Carl later discovers that Scarff is wanted in Mexico as a political assassin. Carl then tracks down Scarff, who is resting from his injuries, to get back his original passport. Carl arrives in Mexico and is captured by the local police, who mistake him for Scarff. Carl then fights to show his true identity to the local police.  The plan seems foolproof until he is forced to care for the dog of Scarff. The local police chief and Scotland Yard inspector Hadden conspire to keep him trapped in the Mexican border town of Katrina in an effort to get him to cross the bridge back into the U.S. and face justice. The misanthropic Schaffner has grown attached to Scarffs pet spaniel and is tricked into going across the dividing line of the bridge to get the dog. He is accidentally killed trying to escape the authorities. The final irony is that the discovery his own humanity has cost the cynical, friendless Schaffner his life.

==Cast==
*Rod Steiger as Carl Schaffner
*David Knight as Johnny
*Marla Landi as Mary
*Noel Willman as Chief of Police
*Bernard Lee as Chief Inspector Hadden
*Eric Pohlmann as Police Sergeant
*Alan Gifford as Cooper
*Ingeborg von Kusserow as Mrs. Scarff (as Ingeborg Wells)
*Bill Nagy as Paul Scarff
*Faith Brook as Kay
*Marianne Deeming as Anna

==Reception==
Reviews for the film are mostly positive. Britmovies review of the film signals out Steigers performance as one of the films highlights: "Rod Steiger produces a gripping and highly charismatic performance as the conceited financier trapped in limbo with luck running out." 
 Double Take, was released in 2001.

==References==
 

;Bibliography
* Rank Film Library,"16mm Entertainment Film Catalogue 1978,9"

==External links==
* 

 

 
 
 
 
 
 
 
 
 