Sound of the Mountain
{{Infobox film
| name           = Sound of the Mountain
| image          = Yama no oto poster.jpg
| image_size     =
| caption        = Original Japanese movie poster
| director       = Mikio Naruse
| producer       = Sanezumi Fujimoto
| writer         = Yasunari Kawabata Yoko Mizuki (screenplay)
| narrator       =
| starring       =
| music          = Ichiro Saito
| cinematography = Masao Tamai
| editing        = Eiji Oi (credited as Hideji Oi)
| distributor    = Toho
| released       =  
| runtime        = 96 min.
| country        = Japan
| language       = Japanese
| budget         =
}}
  is a 1954 black-and-white Japanese film directed by Mikio Naruse starring Setsuko Hara, So Yamamura, and Ken Uehara. In a film about social change, an elderly man whose daughters marriage has failed is forced to watch his sons marriage falling apart before his eyes. It is based on the novel The Sound of the Mountain by Nobel Prize|Nobel-Prize winner Yasunari Kawabata.

==Cast==
*Setsuko Hara as Ogata Kikuko
*So Yamamura as Ogata Shingo
*Ken Uehara as Ogata Shuuichi 
*Yoko Sugi as Tanizaki Hideko
*Teruko Nagaoka as Tanizaki Yasuko
*Yasuko Tan’ami as Ikeda
*Chieko Nakakita as Aihara Fusako
*Reiko Sumi as Kinuko
		
==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 

 