Games of Love and Chance
{{Infobox film
| name           = Games of Love and Chance
| image          = GamesOfLoveAndChance.jpg
| image_size     = 
| caption        = 
| director       = Abdellatif Kechiche
| producer       = Jacques Ouaniche
| writer         = Abdellatif Kechiche Ghalia Lacroix
| narrator       = 
| starring       = Osman Elkharraz Sara Forestier Sabrina Ouazani
| music          = 
| cinematography = Lubomir Bakchev
| editing        = Antonella Bevenja Ghalia Lacroix
| distributor    = New Yorker Films (United States|USA)
| released       = November 25, 2003
| runtime        = 117 minutess (France) 123 minutess (United States|USA)
| country        = France French
| budget         = 
| preceded_by    = 
| followed_by    = 
}} French drama Best Director, Best Writing Most Promising Actress.

The film was shot in Seine-Saint-Denis in 6 weeks in October and November 2002. 

== Synopsis == Paris suburbs Games of Marivaux for their French class.  Abdelkrim, or Krimo, who initially does not act in the play, falls in love with Lydia.  In order to try to seduce her, he accepts the role of Arlequin and joins the rehearsal.  But his timidness and awkwardness keeps him from participating in the play as well as succeeding with Lydia.

== Cast ==
Other than Sara Forestier, many of the actors in this film were inexperienced in cinema and recruited specifically for the film. 

* Osman Elkharraz as Krimo
* Sara Forestier as Lydia
* Sabrina Ouazani as Frida
* Nanou Benhamou as Nanou, the sister of Krimo
* Hafet Ben-Ahmed as Fathi, Krimos best friend
* Aurélie Ganito as Magalie, the girlfriend of Krimo (broke up near the beginning of the film)
* Carole Franck as French professor
* Hajar Hamlili as Zina
* Rachid Hami as Rachid / Arlequin
* Meryem Serbah as the mother of Krimo
* Hanane Mazouz as Hanane
* Sylvain Phan as Slam
* Olivier Loustau, Rosalie Symon, Patrick Kodjo Topou, Lucien Tipaldi as the police
* Reinaldo Wong as The couturier
* Nu Du, Ki Hong, Brigitte Bellony-Riskwait, Ariyapitipum Naruemol, Fatima Lahbi

==References==
 

== External links ==
*  
*  
*  
*  

 
 

 
 
 
 
 
 

 
 