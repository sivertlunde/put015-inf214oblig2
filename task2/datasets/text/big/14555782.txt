Crossed Lines (film)
{{Infobox film
| name           = Crossed Lines
| image          = Crossed line.jpg
| image_size     = poster
| caption        =
| director       = Liu Yiwei Lin Jinhe
| producer       =
| writer         =
| starring       = Ge You Fan Bingbing Annie Shizuka Inoh Liu Yiwei
| music          =
| cinematography =
| editing        =
| distributor    =
| released       = November 29, 2007
| runtime        = 120 min. China
| Mandarin
| budget         =
| website        =
}}
Crossed Lines ( ) is a Chinese comedy anthology film.

==Cast==

===Segment 1: "The Misunderstanding"===
*Director: Liu Yiwei
*Yang Lixin Xu Zheng
*Xu Fan
*Liu Yiwei

===Segment 2: "The Eulogy"===
*Director: Lin Jinhe
*Fan Bingbing
*Wang Xuebing
*Che Yongli
*Jin Sha

===Segment 3: "The Sticks"===
*Director: Sun Zhou
*Ge You
*Yan Ni
*Mao Junjie
*Sun Zhou

===Segment 4: "The Boy Who Cried Wolf"===
*Director: Shen Lei & Alfred Cheung
*Chiu Hsin-chih
*Yao Chen
*Annie Yi Kong Wei
*Michelle Bai
*Aya Liu

==External links==
*   at the Chinese Movie Database

 
 
 
 
 
 


 
 