Pooja (1940 film)
 
 
{{Infobox film
| name           = Pooja
| image          = Pooja_(1940).jpg
| image_size     = Meena
| director       = Abdul Rashid Kardar
| producer       = National Studios
| writer         = M. Sadiq
| narrator       =
| starring       = Sardar Akhtar Sitara Devi Zahur Raja Jyoti Sunalini Devi Anil Biswas
| cinematography = P. G. Kukde
| editing        = 
| studio         = National Studios
| distributor    =
| released       = 1940
| runtime        = 191 minutes
| country        = British India
| language       = Hindi
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}} 1940 Hindi/Urdu Anil Biswas, with lyrics by Khan Shatir Ghaznavi.
Produced by National Studios, the story, screenplay and dialogue writer was M. Sadiq and the cinematographer was P. G. Kukde.    The film starred Sardar Akhtar, Zahur Raja, Sitara Devi, Jyoti, Sankatha Prasad, Sunalini Devi and Bhudo Advani.    

The story involves two sisters, Rama and Lachhi. The younger sister Lachhi, is raped by the rejected suitor of the other sister and gets pregnant. The film then follows the child being brought up by Rama while the real mother, Lachhi, works as a maid in the same house.   

==Plot==
Two sisters Rama (Sardar Akhtar) and Lachhi (Sitara Devi) spend their time teasing each other. Ramas marriage is fixed with Darpan (Zahur Raja). However, the marriage is called off and Rama is married off to someone else. Unable to bear the humiliation, Darpan supposedly rapes Lachhi, as its not made clear whether she was a willing partner. In the meantime, Ramas husband dies and she is now living a widowed life. When its learnt that Lachhi is pregnant, Rama takes her in and brings up Bina (Jyoti) as her own daughter. Bina assumes Lachhi is just a maid working in her house. Much later, Bina finds out the truth about Lachhi being her mother.

==Cast==
* Sardar Akhtar as Rama
* Zahur Raja as Darpan
* Sitara Devi as Lachhi
* Jyoti as Bina Kanhaiyalal
* Sankatha Prasad
* Sunalini Devi
* Bhudo Advani
* Meena Kumari
* Sayani Atish
* Satish
* Amir Bano

==Production==
 

Pooja deals with the "shattering consequences of feudal sexual codes".   
Kardar was interested in themes involving "sexual abnormality". He had earlier used the idea in his film Pagal (film)|Pagal, which dealt with a "psychotic doctor" given to sexual obsession.   

Sitaras role of the unwed mother in the film was appreciated.   

==Soundtrack== Anil Biswas, while the lyricist was Shatir Ghaznavi. The singers were Sitara Devi, Sardar Akhtar, Zahur Raja, Jyoti, Tara Harish and Anil Biswas.    

===Songlist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer
|-
| 1
| Aaj Piya Ghar Aayenge
| Sardar Akhtar, Sitara Devi
|-
| 2
| Aaja Sajaniya Aaja
| Zahur Raja
|-
| 3
| Chal Prem Ke Pankh Lagaayein
| Tara Harish, Jyoti
|-
| 4
| Ek Baat Kahun Main Sajan
| Sardar Akhtar, Zahur Raja
|-
| 5
| Gori Gori Gwaalan Ki Chhori
| Sitara Devi
|-
| 6
| Jeevan Hai Ek Prem Kahani
| Anil Biswas
|-
| 7
| Jhoolo Jhoolo Meri Asha Nit Nainan Mein Jhoolo
| Sitara Devi
|-
| 8
| Kaun Gumaan Bhari Re Baasuriya
| Chorus
|-
| 9
| Lat Uljhi Suljha Ja Balam
| Jyoti
|-
| 10
| Mori Bagiya Mein Aayenge Mali
| Jyoti
|-
| 11
| Sukh Kya Dukh Kya Jeevan Kya
| Zahur Raja
|}

==References==
 

==External links==
*  

 

 
 
 
 