Baader (film)
{{Infobox film
| name = Baader
| image = 
| image_size =
| alt =
| caption = 
| director = Christopher Roth
| producer = Stephan Fruth Christopher Roth Mark Glaser
| screenplay = Christopher Roth Moritz von Uslar
| starring = Frank Giering Laura Tonke Vadim Glowna Birge Schade
| music = Bob Last
| cinematography = Bella Halben Jutta Pohlmann
| editing = Barbara Ries Cristopher Roth
| studio = 
| distributor = 
| released = 2002
| runtime = 110 minutes
| country = Germany
| language = German
| budget = 
| gross = 
}}
Baader is a 2002 German film directed by Christopher Roth. It is a biopic about revolutionary Andreas Baader of the notorious Red Army Faction ("the Baader-Meinhof Gang") which operated mainly in West Germany during the 1970s.

The leading roles are played by Frank Giering (Andreas Baader) and Laura Tonke (Gudrun Ennslin). Birge Schade portrays Ulrike Meinhof. 

Though the script is inspired by real-life persons and events, the storyline of the film continuously mixes fact and fiction.

==See also==
* Der Baader Meinhof Komplex
* If Not Us, Who?
* Stammheim (film)|Stammheim - Die Baader-Meinhof-Gruppe vor Gericht

==References==
 
*Time Out Film Guide 2009

==External links==
* 

 
 
 
 
 
 
 
 

 