Color's Notes
{{multiple issues|
 
 
}}

{{Infobox film
| name = Colors Notes
| runtime = 115 minutes
| director = Xavi Tello
| writer = Xavi Tello
| starring = Eliades Ochoa Raimundo Amador Sazed Ul Alam
| studio = Producciones Ociosas
| released =  
| country = Spain

}}
Colors Notes  is an independent music documentary film about a music festival called "Mestival" which took place in Elx (Spain) from 17 May to 6 June 2005. Filmed at the 10th Mestival Anniversary for six days, some of musicians from the different places of the World came to Spain to take place in a Cross Culture Music Festival, like Eliades Ochoa (Cuba), Raimundo Amador (Spain), Ken Hensley (Great Britain), Sazed ul Alam (Bangladesh), among others.

The documentary film only uses footage shot during the events, and music recorded there, and includes interviews with the musicians, talking about different concepts like: Peace, Music and Cross Culture. It also includes Fatma el Medhis participation (Sahara womens) and a Benefit Concert to the Nelson Mandela Childrens Fund.
 
The film was produced by Filmmaker Xavi Tello (Musician, and independent Producer) and Juanpe Gimeno (also Musician, and sound ingineer), and was shot with small minidv (3ccds) cameras, by fourteen kinocs from Alicante, Elx, and Murcia who were joined in less than two days.

== The Festival ==
Mestival, or the International Festival of Music and Ethnic Culture and Miscegenation, is a festival that is realized from 1996 between the months of between May and June in the city of Elche (Spain). This festival represents a forum for the approximation and the cultural exchange between the peoples of the world. It is characterized by spectacles of music, dance, film|cinema, theatre and workshops on gastronomy. The festival has as aim principal promote the peace and the brotherhood, and takes place in the natural environment of the Palm grove of Elche, declared from 2000 Heritage of the Humanity for the UNESCO.

== Performers ==
19th May. Great Theatre
Eliades Ochoa and the Patria Quartet (Cuba)

28th May. Elx Streets
Muluk the Hwa (Marrakech)

29 May. Great Theatre
Francisco Contreras "The child of Elx" (Spain)

1st June. Great Theatre
Sarabanda (Spain, Uruguay, Brazil, Cuba)

3rd June. Municipal Park
Luétiga (Cantabria)
La Negra (Spain)
Aztex (Mexico / USA)

4 June. Municipal Park
Benefit concert  Nelson Send Childs Fund 
Fetium Kora (Senegal)
Ken Hensley (Great Britain)
Raimundo Amador (Spain)
Shiv Shankar (India)
Sazed Ul Alam (Bangladesh)

== Film production ==
In May 2005, the first events of the 10th Music Anniversary of the Mestival, began to be known in walls and press, and Xavi Tello made a propousal to Sazed Ul Alam (Master sitar, and Nelson Mandela concert organizador)  to film and record the concerts  of the benefit Concert with new digital cameras (minidv 3cdds) that began to be accessible to the young filmmakers.

Two days befote, the beginning of the Mestival, in a small meeting in a bar, the Mestival crew offers the possibility to film all the acts, conferences, concerts, and interviews that it World take place during these days. There wasnt any Money to do it, but there was cameras and sound equipment

Inspired by the father of the Documentary Dziga Vertov, and his texts about the Kino-Eye, Xavi Tello called some young Filmmakers and friends of the zone, and joined team up to fourteen people with their own digital cameras, and like Vertov’s Kinocs, came out to film the Festival. With only 400 € paid in videotapes, fuel, and some food&te, 72 hours of video footage were filmed and 12 hours of live music were recorded from the mix table. It began a long process of catalogation of all tapes, listen the concerts, and transcript the interviews made by Producciones Ociosas. In August 2005, the first videoclip of a Song were edited to the band ‘La Negra ’.
In 2006 there was released by a small edition of a music album with the concert of Sazed Ul Alam, Shiv Shankar  and Raimundo Amador. (It was produced by Master&Padawan).

With any money and only time and a PC edit station, the edition process continued, selecting and editing one full song of each band. Involved in successive productions, shorts movies, and shootings during the next four years, while was working as a Filmmaker and Editor of War Documentaries, Xavi Tello got time to continue the show edition of the film. Three extra editors were hended to end all songs.
In the Autumn of 2009, all concerts were edited, and began to join interviews and music to do the first Script. The film began to take sense. Finally in the beginning of the 2010 the first version was ended, and it began the post-production work and next the audio edition, mixes and masterization.

The film was filmed by Producciones Ociosas and Zenital Films and produced by Producciones Ociosas and Ninguna Parte Estudios, placed in Alicante, Spain. Its the first Spanish film produced, distributed and exhibited in digital.

== Exhibition ==

December 2012. Online Festival. Humanity Explored. India

September 2012. Lucerne International Film Festival . Award of Merit

16 to 23 March 2012. VII Encuentro Hispanoamericano de Cine y Video Documental Independiente
Voces contra el Silencio. México.

28 July 2011. FESTIVAL 1000 Metros Bajo Tiera PIA. Alicante. Spain

June 2011. 39th FESTIVAL OF NATIONS 2011. Ebensee, Austria. Metion of the Jury

23 April 2011. 1º Temuco International Documentary Film Fest. 
South America Premiere. Chile.

16 February 2011. Local Premiere. Club Información. Alicante.Spain.

Since 7 February 2011. Atlantida Film Fest (Official Section)

1–6 February 2011. Docs Barcelona International Documentary Film Festival.

== World diffusion ==
In 2011, February 25, a digital copy sent to a festival, appeared in the web, and quickly it was extended world wide.

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 
 