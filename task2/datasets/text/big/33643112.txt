My Memories of Old Beijing (film)
{{Infobox film
| name           = My Memories of Old Beijing
| director       = Wu Yigong
| starring       = Shen Jie Zhang Min Zheng Zhenyao Zhang Fengyi Yan Xiang
| runtime        = 96 minutes
| country        = China
| language       = Mandarin
}} novel of Best Foreign Language Film at the 56th Academy Awards, but was not accepted as a nominee. 

==Plot==
In the late 1920s, a six-year-old girl named Lin Yingzi (Shen Jie) lived on a hutong in the south of Beijing with her family.

Her first friend was a mad woman Xiu Zhen who was always standing at the hutong entrance waiting for her missing daughter Xiao Guizi. Xiu Zhen fell in love with a young man in college. However, the young man was arrested before she gave birth to their daughter who was later abandoned by her family. Yingzi showed great sympathy for Xiu Zhen. It turned out that Yingzi’s friend Niuniu happened to be the daughter of Xiu Zhen. Xiu was so excited that she could not wait to take her daughter to her father in prison. But unfortunately, they both died after being hit by a train.

Yingzi and her family then moved to another hutong. She befriended a young man with thick lips who became a thief to support his little brother. Yingzi thought the young man was kind but was unsure if he was a good guy. He was arrested by the police, which made Yingzi quite sad.

When Yingzi was nine, the husband of her nurse came to her family. According to him, his son had died and his daughter was sold two years ago. Yingzi became very sad and could not understand why her nurse went out to earn money instead of taking care of her own kids at home.

Finally, Yingzi’s dad died of tuberculosis. After his death, Yingzi left Beijing with her family and said goodbye to all the memories of her childhood.

==See also==
* List of submissions to the 56th Academy Awards for Best Foreign Language Film
* List of Chinese submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  
* http://v.youku.com/v_show/id_XMTcwNTk0ODY0.html
*http://movie.mtime.com/15543/
*http://am774.rbc.cn/netfm/english_service/zt/11_film/11_movie/201105/t20110513_652152.htm

 
 
 
 
 


 