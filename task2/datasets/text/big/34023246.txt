Gemeni (film)
{{Infobox film
| name           = Gemeni
| image          = 
| image_size     = 
| caption        = Saran
| producer       = M. Saravanan (film producer)|M. Saravanan  M. Balasubramanian   M.S. Guhan   B. Gurunath
| writer         = Posani Krishna Murali  
| story          = Saran
| screenplay     = Saran Venkatesh Namitha
| music          = RP Patnaik 
| cinematography = A. Venkatesh
| editing        = Suresh Urs
| studio         = AVM Productions   Suresh Productions  
| distributor    = Suresh Productions
| released       =  
| runtime        = 2:12:27
| country        = India
| language       = Telugu
| budget         =
| gross          =
}}
 Saran and Venkatesh and Namitha in the lead roles and music composed by RP Patnaik. The film is remake of the Tamil film Gemini (2002 Tamil film)|Gemini, released the same year.  The films title was spelt Gemeni to differentiate from the original Tamil version.  The film recorded as average at box-office. 

==Plot==
The story of Gemini revolves around Gemini (Daggubati Venkatesh|Venkatesh) and Ladda (Kalabhavan Mani), two contemporary rowdies in Vijayawada. Geminis friend is murdered by Laddas friend. Then Gemini and his gang search for the person who murdered his friend and kills him. This incident marks the start of their rivalry.

Meanwhile Gemini meets Manisha Natwarlal (Namitha Kapoor|Namitha), and falls in love with her. To gain her affections he goes to study in an evening college as her classmate, and eventually she too falls in love with him.

Gemini and Ladda clash again for a market holdout. Using his brilliant tactics, Gemini fools and wins against Ladda which drives him crazy. In the mean time, Vijayawada gets a new Police Commissioner in Viswanath (Murali (Malayalam actor)|Murali). He arrests both Gemini and Ladda. Understanding their rivalry, Viswanath puts them in a private cell so they can beat each other to death and at the same time can put a finish line to rowdyism. But instead Gemini convinces Ladda to plead Viswanath to give them a chance to lead a normal life. Geminis trick works and they both are given a chance. While Gemini changes his life, Ladda did not change his life. He keeps on disturbing Gemini and wants him to help him out in his business. But instead Gemini informs about Ladda and his activities to Viswanath. So Ladda is now in jail. Meanwhile Gemini tries to reunite with Manisha as he had changed now and eventually she does forgive him.

After some months Viswanath gets transferred as the transport DG of Andhra Pradesh and the new Police Commissioner (Kota Srinivasa Rao) arrives, but to Geminis bad luck he is corrupt and releases Ladda from jail. Now Ladda and the new Commissioner urge Gemini to help him in his business. But he still tries to stay away from them.

Forcing Gemini to return to his old business, he would later kill Geminis right hand and whos also a best friend to him. Then in the climax, Gemini tricks the new Commissioner to kill Ladda. The new Commissioner is transferred as transport DGP and Viswanath becomes the new Commissioner of Police. The film ends with Gemini starting to lead a fresh life with Manisha.

==Cast==
{{columns-list|3| Venkatesh as Gemini
*Namitha as Manisha		
*Kalabhavan Mani as Ladda
*Kota Srinivasa Rao  Murali as Commissioner Viswanath 
*Brahmanandam  Sudhakar		 Venu Madhav		  	
*Ahuti Prasad
*Raghunath Reddy 
*Vizag Prasad
*Posani Krishna Murali
*Raghu Babu
*Surya			
*Gundu Hanumantha Rao 
*Lakshmipathi
*Krishna Bhagavan 
*Kadambari Kiran 
*TK Hari 
*Kandal Rao 
*Babar 
*Balaji 
*Bhargav 
*Ramana 
*Chandu 
*Ramesh 
*Murali 
*Madhu 
*Radha Krishna 
*Sravan Kumar 
*Nagaraju 
*Srikanth Rao 
*Anil 
*Pavan 
*Devi Charan						 Sujatha	
*Mumtaj as Kamini 
*Ashalata 
*Preethi 
*Manu Shetty
}}

==Soundtrack==
{{Infobox album
| Name        = Gemini 
| Tagline     = 
| Type        = film
| Artist      = RP Patnaik
| Cover       = 
| Released    = 2002
| Recorded    = 
| Genre       = Soundtrack
| Length      = 30:46 
| Label       = Aditya Music
| Producer    = RP Patnaik
| Reviews     =
| Last album  = Holi   (2002)
| This album  = Gemini   (2002)
| Next album  = Nee Sneham   (2002)
}}

Music composed by RP Patnaik. All songs are blockbusters. Music released on ADITYA Music Company.
{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 30:46
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = yes
| music_credits =

| title1  = Cheli Chedugudu Gemini  Veturi
| SP Balu, Anuradha Sriram
| length1 = 4:04

| title2  = Pulalo Tene Prema
| lyrics2 = Veturi
| extra2  = Rajesh
| length2 = 4:16

| title3  = Dil Diwana Main Hasina
| lyrics3 = Veturi Usha
| length3 = 4:15

| title4  = Brahma Oh Brahma
| lyrics4 = Kulashekher
| extra4  = SP Balu
| length4 = 4:13

| title5  = Chukkallo Kekkinadu
| lyrics5 = Veturi
| extra5  = Vandemataram Srinivas
| length5 = 2:58

| title6  = Cheli Chedugudu Gemini 
| lyrics6 = Veturi  
| extra6  = SP Balu, Anuradha Sriram
| length6 = 4:04

| title7  = Bandhame Mullu
| lyrics7 = Veturi
| extra7  = RP Patnaik
| length7 = 2:35

| title8  = Nadaka Chuste Vayyaram
| lyrics8 = Veturi
| extra8  = Shankar Mahadevan, Usha
| length8 = 4:12
}}

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 