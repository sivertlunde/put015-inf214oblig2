SM Teacher: Tied Up by Students
{{Infobox film
| name = SM Teacher: Tied Up by Students
| image = SM Teacher - Tied Up by Students.jpg
| image_size = 
| caption = Theatrical poster for SM Teacher: Tied Up by Students (1996)
| director = Yutaka Ikejima   
| producer = 
| writer = Kyōko Godai
| narrator = 
| starring = Runa Hayama Kazuhiro Sano
| music = 
| cinematography = Akira Shimomoto
| editing = Shōji Sakai
| studio = Cement Match Office Burroughs
| distributor = Xces
| released = August 23, 1996
| runtime = 60 minutes
| country = Japan
| language = Japanese
| budget = 
| gross = 
| preceded_by = 
| followed_by = 
}}
 1996 Japanese pink film directed by Yutaka Ikejima and scripted by his wife, screenwriter Kyōko Godai. It won the Best Film fourth place award at the Pink Grand Prix ceremony. Actor-director Kazuhiro Sano was given the Best Actor, 2nd place award for his performance in this film.   

==Synopsis==
Ms. Kajikawa, a new female high school teacher, volunteers to serve as a subject for Mr. Ezakis art class. She becomes the object of SM activities at the hands of both the teacher and his class. 

==Cast==
* Runa Hayama ( ) 
* Kazuhiro Sano
* Moto Ōno ( )
* Kaori Nishiyama ( )

==Awards==
At the third annual   release of the year. The lead male actor in the film, prominent pink film actor and director Kazuhiro Sano, was given the award for Best Actor, 2nd place for his performance in SM Teacher: Tied Up by Students . 

==Availability==
 Yutaka Ikejima filmed SM Teacher: Tied Up by Students for his own Cement Match production company  and Office Burroughs.  Xces released it theatrically in Japan on August 23, 1996.  Xces re-released the film on May 17, 2002 under the title,   

==Bibliography==
*  
*  
*    

==Notes==
 

 
 
 
 
 

 
 

 
 
 
 
 


 
 