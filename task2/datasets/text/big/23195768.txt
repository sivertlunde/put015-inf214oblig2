Avalukendru Oru Manam
{{Infobox film 
| name = Avalukendru Oru Manam
| image = AvalukkendruOruManam.jpg
| caption = Avalukendru Oru manam
| director = C. V. Sridhar
| producer = C. V. Sridhar
| writer = C. V. Sridhar (dialogues)
| story = C. V. Sridhar
| narrator = Kanchana
| music = M. S. Viswanathan
| cinematography =
| editing = N. M. Shankar
| distributor =
| released = 1971
| runtime = 2 hr 38 min
| country = India Tamil
| budget =
| gross =
| preceded_by =
| followed_by =
}}
 1971 Indian Tamil language film starring Gemini Ganesan, Bharathi Vishnuvardhan|Bharathi, R. Muthuraman|Muthuraman, Kanchana (actress)|Kanchana. 

The film was simultaneously made in Hindi and Tamil.

==Plot==
The film deals with a triangular love with a good measure of values of the high society and those of liberal culture of the 60s.

==Cast==
*Gemini Ganesan Bharathi
*R. Muthuraman
*Kanchana Kanchana
*Major Sundarrajan
*V. S. Raghavan
*Veeraraghavan

== Soundtrack ==
Lyrics for the songs were written by Kannadasan. 

{|class="wikitable"
|-
! Song !! Singer !! Duration
|-
|"Malar Edhu En Kangal"
|P. Susheela
|6:00
|-
|"Mangayaril Maharani"
|S. P. Balasubrahmanyam, P. Susheela
|3:28
|-
|"Ariya Paruvamada"
|S. P. Balasubrahmanyam
|3:14
|-
|"Unnidathil Ennai Koduthaen"
|S. Janaki
|3:26
|-
|"Ayiram Ninaivu Ayiram Kanavu"
|S. Janaki
|3:30
|-
|"Deviyin Kovil Paravai Idu" Vani Jayaram
|3:30
|}

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 


 