Pandhayam
{{Infobox film
| name           = Pandhayam
| image          = Pandhayam Movie.jpg
| image_size     =
| caption        =
| director       = S. A. Chandrasekhar
| screenplay     = S. A. Chandrasekhar
| producer       = Shoba Chandrashekhar
| writer         = S. A. Chandrasekhar
| narrator       = Vijay Prakash Raj Raadhika Sarathkumar Meghna Naidu
| music          = Vijay Antony
| cinematography = Srinivas Devamsam
| editing        = J. N. Harsha
| distributor    = Ayngaran International
| studio         = V. V. Creations
| released       = September 19, 2008
| runtime        =
| country        = India
| language       = Tamil
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}} Vijay did a guest role as himself.

==Plot==
The story- A local thug and criminal who kills people like mosquitoes Masanam (Prakash Raj) slowly rises up the ranks to be a Minister. We have Shakthivel (Nithin Sathya) a die-hard fan of actor Vijay who studies in a city college. He falls in love with the wicked ministers sister Thulasi (Sindhu Tolani) and challenges him (he has a reason for that) and a cat and mouse game ensues. What follows is a series of incidents between the two that leads to a melodramatic climax. 

==Cast==
* Nithin Sathya as Sakthivel
* Sindhu Tolani as Thulasi
* Prakash Raj as Masanam
* Raadhika Sarathkumar as Masanams Wife
* Ganeshkar as Ganesh
* Vadivel Ganesh as Vadivel Ganesh
* Rajini Murugan as Mani
* Vimal as Murugesan
* Lavanya as Lavanya
* Rajini Nivetha as Murugesans Mother

Special Appearances :
 Vijay as himself
* Meghna Naidu as herself
* Perarasu as himself
* Rajkapoor as himself
* O Podu Rani as Chinna Maami

==Soundtrack==
The soundtrack consists of 5 songs composed by Vijay Antony.

*"Ammane" - Belly Raj, Nitesh Gopalan, Vinitha
*"Chinna Maamiye" - MK Balaji,Vinaya, Christopher, Shobha
*"Kadhal Theeviravathi" - Vinaya, Jeydev
*"Surangani" - MK Balaji, Maya, Megha
*"Lusimbara" - Christopher, Ramya

==References==
 

==External links==
* 
* 

 

 
 
 
 


 