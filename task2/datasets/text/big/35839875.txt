Harrison and Barrison
{{Infobox film
| name           = Harrison and Barrison
| image          = 
| image_size     = 
| caption        = 
| director       = Alexander Korda
| producer       = Alexander Korda
| writer         = Gyula Kőváry   Richárd Falk
| narrator       = 
| starring       = Márton Rátkai Dezsõ Gyárfás  Nusi Somogyi
| music          = 
| editing        = 
| cinematography = 
| studio         = Corvin Film
| distributor    = 
| released       = 12 November 1917
| runtime        = 
| country        = Hungary Silent  Hungarian intertitles
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} Hungarian silent silent comedy film directed by Alexander Korda and starring Márton Rátkai, Dezsõ Gyárfás and Nusi Somogyi. Korda broke from his previous practice of adapting literary works, to direct an original screenplay. The films style is a madcap one, which relied on the talents of its two stars Rátkai and Gyárfás who were popular comedians. It was Kordas most famous Hungarian film, better known than his literary adaptions.  Korda himself considered the film his best work of the period. 

==Cast==
* Márton Rátkai   
* Dezsõ Gyárfás   
* Nusi Somogyi   
* Manci Dobos   
* Károly Lajthay  
* Ilona Bánhidy   
* Árpád id. Latabár  
 
==References==
 

==Bibliography==
* Kulik, Karol. Alexander Korda: The Man Who Could Work Miracles. Virgin Books, 1990.
* Liehm, Mira & Liehm, Antonín J. The Most Important Art: Eastern European Film After 1945. University of California Press, 1977.

==External links==
* 

 

 

 
 
 
 
 
 