Dealers (film)
{{Infobox film
| name           = Dealers
| image          = Dealers 2.jpg
| image_size     =
| alt            = 
| caption        = Theatrical poster
| director       = Colin Bucksey
| producer       = William P Cartlidge
| writer         = Andrew MacLear
| starring       = Paul McGann Rebecca De Mornay Derrick OConnor John Castle Paul Guilfoyle Adrian Dunbar Sara Sugarman
| music          = Richard Hartley
| cinematography = Peter Sinclair
| editing        = Jon Costelloe
| studio         = Euston Films Rank Organisation
| distributor    = J. Arthur Rank Film Distributors
| released       = Toronto Film Festival  
| runtime        = 87 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
Dealers is a 1989 British film directed by Colin Bucksey. It stars Paul McGann and Rebecca De Mornay.

==Plot==
 Dean Witter.

==Cast==
*Paul McGann as Daniel Pascoe
*Rebecca De Mornay as Anna Schuman
*Derrick OConnor as Robbie Barrell
*John Castle as Frank Mallory
*Paul Guilfoyle as Lee Peters
*Adrian Dunbar as Lennon Mayhew
*Sara Sugarman as Elana

==Reception==
Dealers received a fairly mixed response from American critics at the Austin Texas film festival, some describing it as poorly acted and poorly scripted, with others describing it as having an edge-of-your-seat climax. The film was popular among university students.
However the film was extremely popular by the very people it protagonised and herofied for ever. Risk taking "propriety traders" or "prop traders"
In "the City" where it was based, Londons Financial "Square Mile" it became an instant "must see" by city gents and city girls mostly because of its incredible attention to detail but also by the fact the realistic roller coaster tension took your breath away and that by comparison left "the other film" in its wake. It was regarded as a slice of (a bygone) life, of over-the-phone, over-the-counter bond dealing, high salary, high bonus, trading floor culture. A brokerage house and investment bank little known but cult classic. An epic that captures the essence of the "wheeler dealer". The dealing room it self was so meticulously well done, costing a rumoured one million Pounds, that it was maintained kept intact and a whole television series in Britain was made and based on it, called Capital City (TV Series) which successfully capitalized on the post Wall Street, trendy City Job, City Lifestyle.

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 