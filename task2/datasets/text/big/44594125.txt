Rasika (film)
{{Infobox film
| name = Rasika
| image = 
| caption =
| director = Dwarakish
| writer = Dwarakish Shruti
| producer = Rockline Venkatesh
| music = Hamsalekha
| cinematography = G. S. V. Seetharam
| editing = Suresh Urs
| studio = Rajadurga Productions
| released = 1994
| runtime = 116 minutes
| language = Kannada
| country = India
| budget =
}} romantic drama drama film Shruti among others.  The film was a musical blockbuster hit upon release with the songs composed and written by Hamsalekha being received well.

== Cast ==
* V. Ravichandran 
* Bhanupriya  Shruti
* Jayanthi
* Dwarakish
* Puneet Issar
* Rockline Venkatesh

== Soundtrack ==
The music was composed and lyrics were written by Hamsalekha.  All the seven tracks composed for the film became popular with "Ambaraveri" and "Haadondu Haadabeku" being received well.

{{track listing
| headline = Track listing
| extra_column = Singer(s)
| lyrics_credits = yes
| collapsed = no
| title1 =  Thananam Thananam
| extra1 = S. P. Balasubrahmanyam, K. S. Chithra
| lyrics1 = Hamsalekha
| length1 = 
| title2 = Chitapata Chitapata
| extra2 = S. P. Balasubrahmanyam, S. Janaki
| lyrics2 = Hamsalekha
| length2 = 
| title3 = Haadondu Haadabeku
| extra3 = S. P. Balasubrahmanyam
| lyrics3 = Hamsalekha
| length3 = 
| title4 = Yavvo Yaako Maige
| extra4 = S. P. Balasubrahmanyam, S. Janaki
| lyrics4 = Hamsalekha
| length4 = 
| title5 = Baare Hogona Preethi Madona
| extra5 = S. P. Balasubrahmanyam, K. S. Chithra
| lyrics5 = Hamsalekha
| length5 = 
| title6 = Haadondu Haadabeku
| extra6 = K. S. Chithra
| lyrics6 = Hamsalekha
| length6 = 
| title7 = Ambaraveri Ambaraveri
| extra7 = S. P. Balasubrahmanyam
| lyrics7 = Hamsalekha
| length7 = 
|}}

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 


 

 