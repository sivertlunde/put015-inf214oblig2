Little Noises
{{Infobox film
| name           = Little Noises
| image          = Little Noises dvd cover.jpg Jane Spencer, Jon Zeiderman Matthew Hutton Gianin Loffler Steven Schub Cathy Haase Rik Mayall John C. McGinley Carole Shelley Carolyn Farina Barry Papick Jane Spencer
| producer       = Brad M. Gilbert, Michael Spielberg
| runtime        = 110 min. Makoto Watanabe Mike Murphy
| studio         = 
| distributor    = Monument Pictures
| released       =   Premiered in Competition at the Sundance Film Festival, and was also in competition at Goteborg Festival in Sweden, and at the Wine Valley Festival.
| awards         =  English
}} Jane Spencer. The movie was released on June 1, 1992 in the United States and stars Crispin Glover as an awkward and unsuccessful writer who achieves fame after stealing the poetry of a deaf-mute.  Little Noises was initially intended to be released straight to video but was given a theatrical release by Monument Pictures. 

==Synopsis== Matthew Hutton) a deaf-mute poet. Not only does Joey succeed, but he also manages to sign with literary agent Mathias (Rik Mayall). While Joey is successful, it comes as the cost of Martys own happiness and the man quickly falls into a deep depression and becomes homeless. Fame quickly goes to Joeys head and as he feels little guilt over the theft or loyalty to his friends and girlfriend, he breaks off communication with all of them.

==Cast==
*Tatum ONeal as Stella
*Crispin Glover as Joey
*Nina Siemaszko as Dolores
*Tate Donovan as Elliott Matthew Hutton as Marty
*Gianin Loffler as Wayne Wacker
*Steven Schub as Timmy Smith
*Cathy Haase as Eve
*Rik Mayall as Mathias
*John C. McGinley as Stu
*Carole Shelley as Aunt Shirley
*Carolyn Farina as Linny
*Barry Papick as Bud

==Reception==
Little Noises premiered at the Sundance Film Festival in the main competition, and also was screened in competition at Goteborg, in Sweden, and at the Wine Valley Festival, California. The Los Angeles Times gave the film a mostly positive review, noting that while it had "a few flaws" the film was ultimately "a promising debut film filled with talent and feeling.", calling Glovers performance virtuosic   The Chicago Tribune was less positive and they commented that while they enjoyed Glovers performance, the film "drifts through a number of ill-defined, unnecessary sequences-including scenes involving Nina Siemaszko as a pretty girl with a crush on Joey, and John C. McGinley as the true poet`s drug-dealing brother-before it arrives at its surprisingly bleak conclusion."  The Chicago Sun-Times wrote a mostly negative review, stating that while the film had some highlights they also felt that the films subplot was "pretentious" and "overreaching".  The Austin Chronicle gave it a very positive review, calling it brilliant...

==References==
 

==External links==
*  

 
 


 