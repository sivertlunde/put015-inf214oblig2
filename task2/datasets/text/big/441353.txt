Education for Death
{{Infobox film
| name           = Education for Death
| image          = Education for Death.jpg
| alt            = 
| caption        = 
| director       = Clyde Geronimi
| producer       = 
| writer         =  Art Smith
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = Walt Disney Productions
| distributor    = RKO Radio Pictures
| released       =  
| runtime        = 10 minutes
| country        = United States German  (characters) 
| budget         = 
| gross          = 
}}
 animated propaganda Walt Disney Productions and released on January 15, 1943, by RKO Radio Pictures, directed by Clyde Geronimi and principally animated by Ward Kimball. The short is based on the non-fiction book of the same name  by American author Gregor Ziemer.

==Plot==
 
The film features the story of Hans, a boy born and raised in Nazi Germany, who is bred to become a merciless soldier.
 German couple they are Aryan blood and agree to give their son, whom they name Hans at the judges approval,  into the service of Adolf Hitler and the Nazi Party. They are given a copy of Mein Kampf by the judge as a reward for their service to Hitler; their passport contains spaces for 12 more children (a hint that the couple is expected to produce a large family for the Fatherland#English usage and Nazi connotations|Fatherland).
 representing Germany, from a wicked witch, representing democracy. (The narrator sarcastically comments that "the moral of this story seems to be that Hitler got Germany on her feet, climbed onto the saddle, and took her for a ride.") Thanks to this kind of distorted childrens story, Hans becomes fascinated with Hitler as he and the rest of the younger members of the Hitler Youth give a portrait of him dressed as a knight the Hitler salute.

In the following segment, the audience sees Hans sick and bedridden. His mother prays for him, knowing it will only be a matter of time before the authorities come and take him away to serve Hitler. A Nazi officer bangs on the door to take Hans away, but his mother says he is sick and needs care. The officer orders her to heal her son quickly and have him ready to leave, implying if Hans does not get well, he will be Action T4|euthanized. He orders her not to do anything more to him that will cause him to lose heart and be weak, explaining that a soldier must show no emotion, mercy, or feelings whatsoever.

Hans eventually recovers and resumes his "education" in a school classroom, where Hans and the rest of his classmates all in Hitlerjugend uniforms, after giving portraits of Hitler, Hermann Göring, and Joseph Goebbels the Hitler salute, watch as the teacher draws a cartoon on the blackboard of a rabbit being eaten by a fox, prompting Hans to feel sorry for the rabbit. The teacher, furious over the remark, orders Hans to sit in the corner wearing a dunce cap. As Hans sits in the corner, he hears the rest of the classmates "correctly" interpret the cartoon as "weakness has no place in a soldier" and "the strong shall rule the weak". This sparks Hans to recant his remark and agrees that the weak must be destroyed.
 Marching and Heil Hitler|heiling, heiling and marching!" until he reaches his teens (wearing a uniform similar to that of the Sturmabteilung) still "marching and heiling" until he becomes an adult or "Good Nazi" (now in Wehrmacht uniform) embroiled in hatred towards anyone else who opposes Hitler, having "no seed of laughter, hope, tolerance, or mercy" planted in him, and he "sees no more than the party wants him to  , says nothing but what the party wants him to say, and he does nothing but what the party wants him to do."

In the end, Hans and the rest of the German soldiers march off to war only to fade into rows of identical graves, with nothing on them except a swastika and a helmet perched on top. Thus Hanss education is complete &ndash; "his education... for death."

==Production==
Education for Death: The Making of the Nazi was released when Disney was under government contract to produce 32 animated shorts from 1941 to 1945. In 1940, Walt Disney spent four times his budget on the feature film Fantasia (1940 film)|Fantasia (1940) which suffered from low box office turnout. Nearing bankruptcy and with half of his employees on Disney animators strike|strike, Walt Disney was forced to look for a solution to bring money into the studio. The studios close proximity to the military aircraft manufacturer, Lockheed Corporation|Lockheed, helped foster a U.S. government contract for 32 short propaganda films at $4,500 each. This saved the company from bankruptcy and allowed them to keep their employees on payroll. {{Cite journal
| last1 = Raiti | first1 = G. C.
| title = The Disappearance of Disney Animated Propaganda: A Globalization Perspective
| doi = 10.1177/1746847707074703
| journal = Animation
| volume = 2
| issue = 2
| pages = 153–169
| year = 2007
| issn = 17468477
}} 

The dialogue of the characters is in German language|German, neither subtitled nor directly translated by Art Smiths lone English language narration. A voice track of Adolf Hitler in full demagogic rant is used in a torchlight rally scene. A sequence follows in which Hans becomes a German soldier along with other Hitler Youth.

Intended as anti-Nazi propaganda during World War II, the film is rarely shown today, but it is featured on the DVD Walt Disney Treasures: On the Front Lines, a compilation of Disneys wartime shorts released on May 18, 2004.

==Relationship to the Ziemer book==

Gregor Ziemer, an American author and educator who lived in Germany from 1928 to 1939, wrote the book Education for Death after fleeing Germany on the eve of World War II. The book highlights what was going on in the Nazi schooling of the German youth.
 Horst Wessel anthem around the fire.
 wedding march, Nazi race laws, and the burning of a pile of art. {{Cite journal
| last1 = Fishburn | first1 = Matthew
| title =  Books Are Weapons: Wartime Responses to the Nazi Bookfires of 1933
| doi = 10.1353/bh.2007.0004
| journal = Book History
| volume = 10
| pages = 223–251
| year = 2007
| issn = 1098-7371
| publisher = Pennsylvania State University Press
| url = http://muse.jhu.edu/login?uri=/journals/book_history/v010/10.1fishburn.pdf
}} 
 

==See also==
*Walt Disneys World War II propaganda production
*American propaganda during World War II
*Der Fuehrers Face
*List of World War II short films

==Notes==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 