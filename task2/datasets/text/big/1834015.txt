Quick Change
 
{{Infobox film
| name = Quick Change
| image = Quick_change.jpg
| caption = Quick Change movie poster
| director = Howard Franklin Bill Murray
| producer = Bill Murray Robert Greenhut
| writer = Howard Franklin
| based on =  
| starring =  Bill Murray Geena Davis Randy Quaid Jason Robards
| music = Randy Edelman Michael Chapman
| editing = Alan Heim
| distributor = Warner Bros.
| released =  
| runtime = 89 min.
| country = United States English
| budget =
| gross = $15,260,154 
}} Bob Elliott, and Philip Bosco. It is based on a book of the same name by Jay Cronley.

The film is set in New York City, particularly in Manhattan and Queens, with scenes taking place on the New York City Subway and within John F. Kennedy International Airport. Times Square, the Empire State Building, and the Statue of Liberty are also briefly seen.

Quick Change is the only directorial credit of Bill Murrays career.

==Plot==
Grimm, dressed as a clown, robs a bank in midtown Manhattan. He ingeniously sets up a hostage situation and then slips away with an enormous sum of money and his accomplices: girlfriend Phyllis and best friend Loomis.

The heist itself is comparatively straightforward and easy, but the getaway turns into a nightmare. The relatively simple act of getting to the airport to catch a flight out of the country is complicated by the fact that fate, luck and all of New York City appears to be conspiring against their escape.

For starters, the trio is seeking the Brooklyn-Queens Expressway to get the airport, but the signs were taken during construction work, resulting in the three robbers becoming lost in an unfamiliar part of the city. Then, a con-artist/thief robs the trio of everything they have (except the bank money, which they have taped under their clothes).

When changing into new clothes, they are almost gunned down by the stressed incoming tenant of Phyllis apartment, as members of the fire department respond to a call by pushing their hydrant-blocking car out of the way only to make it roll into a ditch.

When the three crooks eventually manage to flag down a cab, the driver is hopelessly non-fluent in English. This leads Loomis to jumping out of the moving cab to grab another, but he runs into a newsstand and the driver leaves, thinking hes killed Loomis. An anal-retentive bus driver, a run-in with mobsters and Phyllis increasing desperation to tell Grimm the news that she is pregnant with his child add further complications.

All the while, Rotzinger, a world-weary but relentless chief of the New York City Police Department, is doggedly attempting to nab the fleeing trio. A meeting on board an airliner at the airport occurs between the robbers and the chief, who gets the added prize of having a major crime-boss dropped in his lap with their assistance. Unfortunately the chief only realizes who they were after their plane has taken off.

==Cast==
* Bill Murray as Grimm
* Geena Davis as Phyllis Potter
* Randy Quaid as Loomis
* Jason Robards as Chief Walt Rotzinger
* Tony Shalhoub as Cab Driver
* Philip Bosco as Bus Driver
* Phil Hartman as Edison
* Jamey Sheridan as Mugger
* Stanley Tucci as Johnny
* Kurtwood Smith as Lombino

==Reception==
Despite not being a major commercial success, the film was well received critically.  In fact, several critics    claim it is one of Murrays finest performances: a jaded man who has just had too much of The Big Apple. Also praised were the strong performances by the supporting cast, particularly Robards as the police chief Rotzinger, who, while almost as burned out as Murray, is still determined to capture the robbers as a swan song to his long career.

Roger Ebert in his July 13, 1990 Chicago Sun-Times review wrote: "Quick Change is a funny but not an inspired comedy. It has two directors...and I wonder if that has anything to do with its inability to be more than just efficiently entertaining." 

The film currently holds a 79% fresh rating on review aggregate Rotten Tomatoes. 

==Home media==
Quick Change has been released on VHS in both NTSC and PAL regions in 1991, on Region 1 (NTSC) DVD in 2003 and on Region 2 (PAL) DVD in 2006.

==References==
 

==External links==
*  
*   at Rotten Tomatoes
*  

 
 
 
 
 
 
 
 
 
 
 
 
 