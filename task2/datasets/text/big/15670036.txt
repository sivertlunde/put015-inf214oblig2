Haunted Island
{{Infobox film
| name           = Haunted Island
| image          = 
| caption        = 
| director       = Robert F. Hill
| producer       = 
| writer         = Frank R. Adams
| starring       = Jack Dougherty Helen Foster
| cinematography = 
| editing        =  Universal Pictures
| released       =  
| runtime        = 
| country        = United States  Silent English intertitles
| budget         = 
}}
 silent action action film serial directed by Robert F. Hill. The serial was released in 10 chapters of two reels each,  with the first episode ("A Night of Fear") released on March 26, 1928.  Each episode featured a lurid title, such as "The Phantom Rider," "The Haunted Room," "The Fires of Fury," or "Buried Alive."  The serial was a remake of the 1918 Universal serial The Brass Bullet, which was based on the story "Pleasure Island." Lahue, Kalton C. Continued Next Week: A History of the Moving Picture Serial. Stillwater, Okla.: University of Oklahoma Press, 1964; Langman, Larry. Return to Paradise: A Guide to South Sea Island Films. Lanham, Md.: Scarecrow Press, 1998. ISBN 0-8108-3268-2 

As of October 2009, Haunted Island is considered a lost film. 

==Synopsis== South Seas island known as Pleasure Island. A hidden cache of gold is allegedly buried on the island, which has several haunted structures.  Rosalinds uncle, Spring Gilbert (Al Ferguson), wants the gold for himself and declares he will stop at nothing, not even the death of his niece, to get it.  Rosalind, meanwhile, is befriended by Jerry Fitzjames (Jack Dougherty), a playwright. Unfortunately, Jack has only recently escaped from a psychiatric hospital. Although he swears to protect Rosalind, she doubts Jerrys sanity. The two lovers race against Uncle Gilbert (who has set several traps for them) to find the treasure. In the end, Rosalind and Jerry are aided by the "Phantom Rider," a spectral horseman. 

==Casting and marketing==
Actor Jack Dougherty had been signed by Universal in 1924, and quickly became of the leading men in Universals serials.  However, this film was Doughertys last for Universal. 

To market the film, Universal Studios printed a unique "one-sheet"—a 27 inch by 41 inch (68.6 cm by 119.4 cm), five-color poster in the form of an old pirate map illustrated with scenes from each chapter of the serial. 

==Cast==
* Jack Dougherty - Jerry Fitzjames
* Helen Foster - Rosalind Joy
* Carl Miller - Yetor King
* Al Ferguson - Spring Gilbert
* John T. Prince - Mark Joy
* Grace Cunard - Mary Strong
* Wilbur Mack
* Myrtle Grinley
* John Wallace
* Scotty Mattraw

==See also==
* List of film serials
* List of film serials by studio

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 