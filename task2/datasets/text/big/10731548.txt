Pati Patni Aur Woh
:This article is about the movie, For the television show see Pati Patni Aur Woh (TV series)
{{Infobox film
| name           = Pati Patni Aur Woh
| image          = 
| image_size     = 
| caption        = 
| director       = B. R. Chopra
| producer       = B. R. Chopra
| writer         = Kamleshwar
| narrator       = 
| starring       = Sanjeev Kumar Vidya Sinha Ranjeeta
| music          = Ravindra Jain
| cinematography =
| editing        = 
| distributor    = 
| released       = 1978
| runtime        = 141 minutes
| country        = India
| language       = Hindi
| budget         = 
}}
 Sanjeev Kumar, Vidya Sinha, Ranjeeta Kaur, and in guest appearances Rishi Kapoor, Neetu Singh and Parveen Babi.

==Summary==
The film is a comical take on extra-marital affairs. Ranjeet Chaddha (Sanjeev Kumar) & Sharda (Vidya Sinha) meet, fall in love & get married. Eventually, Ranjeet gets a promotion, a higher salary & a temptation - Nirmala Deshpande (Ranjeeta Kaur), his new secretary. Ranjeet lies to Nirmala—to get her sympathy—that his wife is terminally ill. Nirmala feels sorry for Ranjeet and gets close to him. Ranjeet uses this to further his plans. But when everything is going as planned, Sharda gets suspicious. She spies on them & learns the truth. When Ranjeet gets busted in front of both the women, the proverbial hell breaks loose.

==Plot==
The film starts off indicating the parallels of the story with that of Adam & Eve. Here, Adam is Ranjeet, Eve is Sharda while the apple is Nirmala. Ranjeet is newly employed in a company, whose pay scales can be gauged from the fact that he goes to work on a bicycle. However, this bicycle itself brings him face-to-face with Sharda, when he bumps into her by accident. Shardas bicycle gets badly damaged & Ranjeet drops her off. The same evening, Ranjeet goes to the wedding of his friend Abdul Karim Durrani (Asrani), a co-worker & a poet. Sharda is also present at the ceremony. Sharda & Ranjeets love blossoms from there & soon they get married.

In the course of a few years, Ranjeet is Sales Manager of the company and father of a son. Sharda & Ranjeet are still living in marital bliss. That is, until Nirmala, Ranjeets new secretary, shows up. Ranjeet is inexplicably attracted to Nirmala. She is an honest girl who is trying to make two ends meet. She is much more beautiful compared to Sharda. But most of all, she knows nothing about Ranjeets true intentions & his married life. Ranjeet is initially upset with his thoughts about her, but finally gives in.

He carefully plans his further steps. He pretends to be the helpless grieving husband of a cancer stricken wife, who wont live much longer. Nirmala feels sorry for him, thus making it easier for him to get close to her. Nobody, not Sharda, not even his closest friend, suspects a thing. One day, Ranjeet bluffs to Sharda that he will be late coming home as he has a meeting. He takes Nirmala out to dinner. Next day, Sharda finds Nirmalas handkerchief, with lipstick marks on it, in Ranjeets pocket.

She immediately confronts Ranjeet, who makes up a story about a co-worker whose handkerchief he may have accidentally taken. Sharda reluctantly believes him. Ranjeet decides to take his next steps more carefully. Sharda too starts thinking that her fears were unfounded. Ranjeet makes even more interesting back up plans: He prepares two books of poetry, professing his love. The poems are the same in both, only one book contains Nirmalas name, and the other contains Shardas.

Ranjeet courts Nirmala without Shardas knowledge. The turning point comes when Sharda sees him in a hotel with Nirmala. She later asks him about his meeting, about which the clueless Ranjeet lies. Shardas fears are confirmed. She starts spying on him & Nirmala, taking incriminating pictures. After sufficient evidence is obtained, she secretly meets Nirmala, posing as a journalist. Nirmala, who hasnt seen Ranjeets "ailing wife" yet, thinks Sharda intends to blackmail her. But Sharda reassures her that she wont.

Nirmala spills all the beans, upon which Sharda reveals her true identity. Meanwhile, Ranjeet gets another promotion & rushes home happily to tell his wife about it. Sharda catches him unawares and lets him know that he is busted. Ranjeet does not know what has hit him. He turns round, only to see Nirmala behind him. Sharda tells him that she is leaving him & the divorce papers will be soon sent to him. Sharda & Nirmala console each other. Ranjeet calls upon his friend & lies that Nirmala has said some malicious lies to Sharda about him.

Ranjeets friend sides with him & lies about Nirmalas character. Sharda exposes Ranjeet in front of him as well, with help of the evidence she has collected. Sharda tells Ranjeet to choose either her or Nirmala. Ranjeet quietly gives Nirmala some money & lies to her, in a last-ditch attempt at damage control. But honest Nirmala returns the money to Sharda, making things even worse for Ranjeet. Sharda prepares to walk out on Ranjeet, while Nirmala resigns and leaves Ranjeet as well. Sharda comes to visit Ranjeet one last time, when their innocent son asks what is happening.

Sharda decides to give Ranjeet another chance, if only for their son and soon life comes back on track. But soon another secretary(Parveen Babi) joins the office & Ranjeet tries to resort to his antics once more. Just by coincidence, Ranjeets friend suddenly walks in & Ranjeet backs off, taking this as a warning.

==Cast==
*Sanjeev Kumar  as  Ranjeet Chhadha
*Vidya Sinha  as  Sharda Chhadha
*Ranjeeta Kaur  as  Nirmala Deshpande
*Asrani  as  Abdul Karim Durrani
*Parveen Babi  as  Neeta (Guest appearance)
*Nana Palsikar  as  Nirmalas Nanaji
*Om Shivpuri  as  Shardas Father
*Rishi Kapoor  as  Singer (Cameo of song "Tere Naam Tere Naam")
*Neetu Singh  as  Singer (Cameo of song "Tere Naam Tere Naam")

==Soundtrack==

{{Track listing
| headline        = Songs
| extra_column    = Playback
| all_lyrics      = Anand Bakshi
| all_music       = Ravindra Jain

| title1          = Ladki Cycle Wali
| extra1          = Mahendra Kapoor, Asha Bhosle

| title2          = Na Aaj Tha Na Kal Tha
| extra2          = Kishore Kumar

| title3          = Tere Naam Tere Naam
| extra3          = Mahendra Kapoor

| title4          = Thande Thande Paani Se Nahana Poornima
}}

== Awards and Nominations ==
 
|- 1979
|Kamleshwar  Filmfare Award for Best Screenplay
| 
|-
| Sanjeev Kumar
| Filmfare Award for Best Actor
|  
|-
| Ranjeeta Kaur
| Filmfare Award for Best Supporting Actress
|  
|}

== References ==

 
==External links==
*  

 
 
 