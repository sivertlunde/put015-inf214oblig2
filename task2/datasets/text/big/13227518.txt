The Engagement Ring
{{Infobox film
| name           = The Engagement Ring
| image          = 
| image_size     = 
| caption        = 
| director       = Ramiz Azizbeyli
| producer       = Gara Gaya Film Studio
| writer         = Vagif Samadoghlu, Ramiz Azizbeyli, Orkhan Sadigov
| narrator       = 
| starring       = Afag Bashirgyzy, Rafael Dadashov, Firangiz Rahimbeyova, Valeh Karimov
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1991
| runtime        = 96 minutes
| country        = Azerbaijan Azeri
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}

The Engagement Ring ( ) is a full-length Azerbaijani comedy film released in 1991. The film plot is based on a drama of the same name by Azerbaijani writer Vagif Samadoghlu.

==Plot==
The plot is built around the story of Sara (played by Gulshad Bakhshiyeva), who is married to an orphaned alcoholic Huseyn (played by Ayshad Mammadov), losing her engagement ring during the family vacation at a country house near Baku. Sara suspects her yard neighbors of stealing the ring, and it does not take long before neighbors start fighting with each other. The story further reveals the ills and corruption of society at the end of Soviet period, disintegration of old social values, relationships between neighbors living in hidden envy of each other.

==Cast==
* Afag Bashirgyzy (Soylu),
* Valeh Karimov (Moshu),
* Firangiz Rahimbeyova (Seda)
* Rafael Dadashov (Rasim)
* Ayshad Mammadov (Huseyn)
* Gulshad Bakhshiyeva (Sara)
* Nasir Sadigzade (Tanribey)
* Mukhtar Maniyev (Zyrpy)
* Jahangir Novruzov (Police Chief)
* Arif Guliyev (Policeman)

==See also==
*Azerbaijani films of the 1990s

==External links==
*  

 
 
 
 
 
 


 
 