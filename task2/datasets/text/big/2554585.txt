Arthur and the Invisibles
 
{{Infobox film
| name           = Arthur and the Invisibles
| image          = Arthur and the Invisibles poster.jpg
| caption        = Theatrical release poster
| director       = Luc Besson
| producer       = Luc Besson Emmanuel Prévost
| writer         = Luc Besson Céline Garcia Madonna Snoop Dogg David Bowie Ron Crawford Robert De Niro Jason Bateman Jimmy Fallon Mia Farrow Anthony Anderson
| music          = Éric Serra
| cinematography = Thierry Arbogast
| editing        = Karim Benhammouda Yann Hervé Vincent Tabaillon Narfia Entertainment Group
| distributor    = The Weinstein Company Metro-Goldwyn-Mayer (theatrical distributor only)
| released       =  
| runtime        = 94 minutes   104 minutes  
| country        = France
| language       = English
| budget         = $86 million 
| gross          = $107,944,236 
}} CGI animated/live-action feature film adaptation of the 2002 childrens book Arthur and the Minimoys, and the 2003 sequel Arthur and the Forbidden City, written by filmmaker Luc Besson, who also directed the film. It premiered in limited release in France on November 29, 2006, and received wide releases in a number of countries in the following weeks. In the United States, it opened on December 29, 2006, for one week in Los Angeles, California, with a wider release on January 12, 2007 and it was released in the United Kingdom on February 2, 2007. 

With a budget of euro|€65,000,000, Arthur and the Invisibles was briefly the most expensive French film production  until surpassed by Astérix at the Olympic Games (film)|Astérix at the Olympic Games.

==Plot==
In the year 1960, protagonist Arthur lives with his grandmother Daisy in a quiet farm house. His grandfather Archibald has recently gone missing and he sees little of his parents. Daisy entertains Arthur with stories of his grandfathers adventures in Africa, featuring the tall Bogo Matassalai and the minuscule Minimoys, of whom the latter now live in Archibalds garden, protecting a collection of rubies. Arthur becomes enamoured of a picture of Selenia, the princess of the Minimoys. When Daisy receives a two-day deadline to pay a large sum of money to a building developer named Ernest Davido, who plans to evict the two, Arthur looks for the rubies to pay off the debt, and discovers various clues left by his grandfather. He is met in the garden by the Bogo Matassala, who reduce Arthur to Minimoy size. From the Minimoys, Arthur learns that they are in danger from Maltazard, a Minimoy war hero who now rules the nearby Necropolis, after corruption by a weevil, by whom he has a son named Darkos.
 British namesake, draws a sacred sword from its recess and uses it to protect the Minimoys from Maltazards soldiers; whereupon Sifrat, the ruler of the Minimoys, sends Arthur to Necropolis, with the princess Selenia and her brother Betameche. En route, they are attacked on two occasions by Maltazards soldiers. In Necropolis, Selenia kisses Arthur, marking him as her husband and potential successor, and confronts Maltazard alone. When Maltazard learns that she has already kissed Arthur and thus can no longer give him her powers and cure his corruption, he imprisons all three, who discover a Minimoy form of Archibald. Thereafter Arthur and his grandfather escape and return to human form, with little time to spare before Maltazards flood reaches the Minimoys.  With the help of Mino, a royal advisors long-lost son, Arthur redirects the flood to Necropolis; whereupon Maltazard abandons Necropolis and his son, and the water ejects the rubies above ground. Archibald pays Davido with one ruby; and when he tries to take them all, the Bogo Matassalai capture him and give him to the authorities (scene deleted in the U.S. edition). The film ends with Arthur asking Selenia to wait for his return, and her agreement to do so.

==Cast==

===Live-Action===
* Freddie Highmore as Arthur.
* Mia Farrow as Daisy, Arthurs long-suffering grandmother.
* Ron Crawford as Archibald: Arthurs grandfather, known to the Minimoys as Archibald the Benevolent.
* Adam LeFevre as Ernest Davido, a greedy landowner who presides over and founded the multi-national Davido Corporation, which specializes in property development.
* Penny Balfour as Rosie Suchot, Arthurs mother.
* Doug Rand as Francis, Arthurs father.
* Chazz Palminteri as Travel Agent.

===Animated Voices===
* Freddie Highmore as Arthur. Madonna as Princess Selenia, the daughter of Emperor Sifrat. The character was dubbed by French singer Mylène Farmer in the French version Arthur et les Minimoys, German singer Nena in the German version Arthur und die Minimoys and Finnish Pop singer Paula Vesala from PMMP in the Finnish version.
* Jimmy Fallon as Prince Betameche, Selenias younger brother. 
* David Bowie as Emperor Maltazard (also known as the Evil M, Maltazard the Evil, or Malthazar the Cursed), antagonist. Although the film refers to him as Malthazar, his name in the book was Malthazard and some film versions refer to him as Maltazard. For the Japanese release, he is voiced by Gackt who is also a singer and actor.
* Jason Bateman as Prince Darkos, Maltazards vicious but dim-witted son.
* Robert De Niro as Emperor Sifrat XVI, Betameche and Selenias father.
* Snoop Dogg as Max, the leader of the Koolamassai, a race of beings similar to the Minimoys, who supply Maltazards people with a candy fruit and are therefore left relatively free.
* Harvey Keitel as Miro, the royal advisor.
* Erik Per Sullivan as Mino, Miros son.
* Anthony Anderson as Koolamassai.
* Emilio Estevez as Ferryman.

==Production== Madonna and David Bowie, a camera was used to record their lips to help the animators. The animation was done with proprietary software. 

==Reception==
The film was budgeted at $86,000,000.    In its first two weeks in cinemas in France Arthur earned over US$20 million. 

Arthur and the Invisibles received negative reviews from film critics. thesps Snoop The Ant Bully, which itself was based on a childrens book written three years before Bessons. "It all simply looks as if   Garcia and Besson couldnt decide on any one thing to copy," said Frank Lovece of Film Journal International, "so they copied them all."  Lovece also noted that, "the whole thing gets seriously creepy when   the grown-up, pinup-beauty princess and the 10-year-old boy fall for each other. Mary Kay Letourneau comes uncomfortably to mind." Common Sense Media disliked the film, giving it 2 stars out of 5 and saying, "Uneven animation-live action combo may bore kids."    Josh Tyler of Cinema Blend greatly disliked the film, giving it 1.5 stars out of 5 and saying, "Sure it has sometimes loved French director Luc Besson’s name on it, but the character designs look like they were stolen from those wispy haired troll dolls that were popular for about five minutes fifteen years ago, and the plot sounded like it was written by a ten-year-old kid underneath a heavy bedspread with a big chief tablet and a pencil the size of a horse’s leg." 

Besson, in a May 2007 interview, blamed American distributor The Weinstein Company for the films poor critical reception in the U.S., saying "Why the critics didnt like Arthur was because   changed so much of the film and tried to pretend the film was American.   America and the UK were the only countries where the films were changed. The rest of the world has the same film as France." 

===Awards===
On February 1, 2007, the film received the Imagina Award in the category Prix du Long-Métrage. 

On October 1, 2007, Mylène Farmer was awarded the NRJ Ciné Award for her dubbing of Sélénias voice in Arthur and the Minimoys. 

===Home media release===
The US edition DVD was released on May 15, 2007 with just the English-language version and cut down about 10 minutes from the original version. The international DVD versions include the uncut English-language version and the local-language version.

==Differences between release versions==
After a screening test in the United States, the Weinstein company edited the film.  Approximately nine minutes were cut. Most of the edits pertained to the love story between Arthur and Selenia. The Scenes were:
*Arthurs arrival at the Minimoy world in the middle of a Ceremony centering on Selenias coming of age;
*Arthur falling in love with Selenia at first sight;
*Arthur removing a string from Selenias corset to use as a climbing-rope, and Selenia taking the string back;
*Max referring to their drinks as Jack Fire instead of Genie Soda. Also, when Arthur escapes the bar during the blackout, Max smokes something that resembles a blunt (marijuana)|blunt.
*Selenia kissing Arthur confronting Maltazard, and Betameche congratulating them;
*Selenia kissing Arthur before he returns to his own world;
*Arthur learning of the custom that a princess must wait ten lunar months before kissing her chosen husband for the second time;
*Davido attempting to steal the treasure from Archibald, before being captured by the Bogo Matassalai;
*Arthurs Grandmother in the antique dealership prior to them arriving at her home;
*Max telling Selenia about his 7 wives while they are dancing;
*Malthazar confronting Selenia about her engagement to Arthur.
*Selenia teasing Arthur while crawling into the toy car, causing Arthur to gasp at the distracting display.

The entire storyline involving the parents and their greed for money was also deleted, cut short by a small cutscene and a narrator explaining that worrying over their son was all they needed to reform completely.

The British version of the film, also distributed by the Weinstein Company, similarly lacked these scenes.

===Technology===
The Minimoys featured in the first Augmented Reality Nestle Chocopic cereal box with the help of Dassault Systemes technology 3DVIA Virtools. 

==Sequels==
Arthur and the Invisibles was followed by a 2009 sequel,   based on the final book in the series. The two films were edited together and released in the UK and Ireland as a single film titled Arthur and the Great Adventure.

All three films received mixed to poor reviews.

==See also==
* Arthur and the Revenge of Maltazard
*  
* Arthur series (Besson)
* Arthur and the Invisibles (soundtrack)|Arthur and the Invisibles (soundtrack)

==References==
 

==External links==
 
*  
*  
*  
*   at Keyframe
*   at CanMag
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 