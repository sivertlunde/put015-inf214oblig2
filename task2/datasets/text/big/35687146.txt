Woman by Woman: New Hope for the Villages of India
{{Infobox film
| name           = Woman by Woman: New Hope for the Villages of India
| image          = 
| caption        = 
| director       = Dorothy Fadiman
| producer =      Kristin Atwell 
| narrator      = Dorothy Fadiman
| cinematographer = Daniel Myers
| released       =  
| runtime        = 28 minutes
| country        = United States
| language       =  English and Hindi
}}
Woman by Woman:  New Hope for the Villages of India is a 2001 documentary film by filmmaker, Dorothy Fadiman.

==Synopsis==
The film follows several women from small villages in India who come out of seclusion to serve their communities by teaching, particularly to other women, about family planning resources. The women are trained through Janani which is a non-profit health organization founded in the state of Bihar, India by way of DKT International.  The women become rural medical practitioners and counselors.      

==Release and reception==
The film premiered in Palo Alto and afterwards, Chitra Banerjee Divakaruni presided as a moderator for a panel which included Kavita Nandini Ramdas(the president at the time of the Global Fund for Women), LS Aravinda (coordinator with Association for Indias Development), and Richard T. Schlosberg III.   The film also participated twice in 2001 and 2009 at the United Nations Association Film Festival at Stanford University.        The documentary won a FREDDIE Award at the 2002 International Health and Medical Media Festival. 

Rediff.com described the message of the film as "change individuals can make". This message is spread by examples, such as a woman being encouraged by her mother-in-law to advance in the community, and a woman that works as an equal with her husband. 

==References==
 

== External links ==
*  
*   at vimeo.com
*   at vimeo.com
*   
*    
*   March 2001
*   
*    March 23, 2001 from the Palo Alto Weekly

 
 
 
 
 
 
 