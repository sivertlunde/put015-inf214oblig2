The Watch (2012 film)
 
 
{{Infobox film
| name           = The Watch
| image          = The watch movie poster.jpg
| caption        = Theatrical release poster
| director       = Akiva Schaffer
| producer       = Shawn Levy
| writer         = Jared Stern Seth Rogen Evan Goldberg
| starring       = Ben Stiller Vince Vaughn Jonah Hill Richard Ayoade Rosemarie DeWitt
| music          = Christophe Beck
| cinematography = Barry Peterson
| editing        = Dean Zimmerman 21 Laps Productions
| distributor    = 20th Century Fox
| released       =  
| runtime        = 102 minutes 
| country        = United States
| language       = English
| budget         = $68 million 
| gross          = $68.3 million 
}} science fiction comedy film directed by Akiva Schaffer and written by Jared Stern, Seth Rogen and Evan Goldberg. It stars Ben Stiller, Vince Vaughn, Jonah Hill and Richard Ayoade. The film follows Evan (Stiller), Bob (Vaughn), Franklin (Hill), and Jamarcus (Ayoade), a group of neighbors who form a suburban neighborhood watch group. When they uncover an alien plot threatening the world, they are forced into action.

The film began its development in 2008 under producer Shawn Levy as a teen-targeted project written by Jared Stern. Between 2009 and late 2010 it saw different directors and stars join the project until November 2010, when it moved in a new direction under Rogen and Goldberg (who rewrote the script for an adult audience). Filming began in October 2011 in the state of Georgia (U.S. state)|Georgia, concluding in January 2012.
 Trayvon Martin by a neighborhood-watch member. As a result, the campaign was refocused on the alien premise instead of the film leads and the films name was changed from Neighborhood Watch to The Watch. Released on July 27, 2012, the film received generally negative reviews, with critics focusing on the plot, frequent "vulgar and offensive" jokes and numerous product placements. However, the lead cast was more positively received.

==Plot==
 
In the town of Glenview, Ohio, Evan Trautwig (Ben Stiller) is an active participant in the community and senior manager of the local Costco store. His life is disrupted when the stores night security guard is murdered. The local police have no leads, and show no interest in investigating further. Determined to find the killer and bring him to justice, Evan decides to form a neighborhood watch. However, he only manages to recruit Bob (Vince Vaughn), a construction worker and loving father; Franklin (Jonah Hill), a high school dropout who dreams of being a police officer but failed the written, physical, and mental-health tests; and Jamarcus (Richard Ayoade), a recent divorcé.

The watch members use the group as an excuse to drink and have fun (much to Evans annoyance). While driving on patrol, they accidentally hit something. They discover a strange metallic orb that acts as a highly-destructive weapon, and deduce that it is of alien origin. Meanwhile, several more townspeople are mysteriously killed. The watch responds to the murders and encounters an alien, which attacks them. Evan (apparently) kills it with a lawn gnome before the group returns with the creature to Bobs house. The creature regains consciousness and escapes, stealing the metallic orb and warning them that the aliens have already infiltrated the town. The watch theorizes that the aliens are stealing their victims skin and disguising themselves as humans, so anyone in Glenview could be an alien. Bob confides to Evan that he is worried about his daughter Chelsea (Erin Moriarty), and does not trust her boyfriend Jason (Nicholas Braun). Evan admits that he has been avoiding his wife Abby (Rosemarie DeWitt) because he is infertile, and revealing that to her may cause her to leave.

Evan suspects that one of his neighbors is an alien (due to his strange, stiff way of speaking and because he always seems to be watching him). As the watch scouts the neighbors house, Bob learns that Chelsea is at an unsupervised party with Jason. Bob disobeys Evans orders, and rushes to the party with Franklin. Bob prevents Jason from raping Chelsea, but Jason beats him until Franklin intervenes. Evan and Jamarcus investigate the odd neighbor alone, discovering that he (only) hosts orgies in his basement. When Bob returns, he and Evan argue over his putting his daughter above the watch. Bob is fired from the watch after saying Evan has no friends because he tries to control everything. Evan goes home and admits his infertility to Abby, who accepts the news and tells him they will work things out.

Evan then receives an urgent visit from Jamarcus, who confesses that he is one of the aliens but has decided to side with humanity after experiencing human culture (and oral sex). He warns the group that the aliens are building a transmitter (beneath the Costco store) which will summon their armada to destroy the earth, and is expelled from the watch for his deception. Bob, Franklin, Evan and Abby arm themselves and infiltrate the Costco to destroy the transmitter. Bob encounters Jason who reveals that he is also an alien, and they fight. Evan and Franklin attempt to disable the transmitter, but are surrounded by aliens. Jamarcus arrives and saves the pair, revealing that the aliens brains are located in their crotch; Bob kills Jason by ripping off his penis. Evan discovers that the transmitter is powered by the metallic orb and removes it, disabling the machine. More aliens arrive, forcing the group to flee. The watch uses the metallic orb to destroy the Costco building, killing all of the aliens inside.

In the epilogue, Evan and Abby rekindle their passion and adopt a daughter. Bob is closer to Chelsea and accepts her new boyfriend (who is intimidated by stories of how Bob killed Jason by "ripping his dick off"). Franklin is finally accepted by the Glenview Police Department, and Jamarcus continues participating in the secret neighborhood orgies. The group maintains the watch, continuing to protect Glenview from criminals (and aliens). 
 

==Cast==
*Ben Stiller as Evan Trautwig, a Costco manager and suburban resident who forms a neighborhood watch after his friend is murdered; he keeps forming new groups because he has no friends.  
*Vince Vaughn as Bob McAllister, a resident who uses the watch to spy on his teenage daughter; Vaughn began negotiations to star in June 2011.   The characters relationship with his daughter is what convinced Vaughn to take the role.  balisong knife for his character. 
*Richard Ayoade as Jamarcus, a recently divorced resident; Chris Tucker was considered for the role before Ayoade was signed. 
*Rosemarie DeWitt as Abby Trautwig (Evans wife) 
 Erin Moriarty Doug Jones (as the chief alien villain),   R. Lee Ermey as Manfred Salisbury (a local resident) and Joseph A. Nunez as Antonio Guzman (Evans colleague, whose murder inspires him to start the neighborhood watch). Billy Crudup appears (uncredited) as Evans "creepy neighbor", Paul.  Director Akiva Schaffer and his collaborators in the comedy troupe The Lonely Island, Andy Samberg and Jorma Taccone, make cameo appearances in the film as masturbating participants in Pauls orgy. 

==Production==

===Development=== David Dobkin and Will Ferrell were negotiating to direct and star, respectively   (with Dobkins involvement progressing to revising Sterns script). However, by August Dobkin and Ferrell had left the project. 
 Hot Rod).  On May 4, 2012 (two months before the films release), it was renamed The Watch.  

===Filming=== Trayvon Martin case|alt=Red-and-white neighborhood-watch sign on a post]] Cobb County Powder Springs). Brookhaven community. Campbell High School in Smyrna, Georgia.  On November 23, 2011 a minor, walk-on role in the film was auctioned to benefit the Stiller Foundation; the role was sold for $23,000 to an undisclosed bidder.  

In late November 2011, the site of a former BJs Wholesale Club in Norcross, Georgia was converted in appearance to a Costco for shooting.   On January 23, 2012, Hill announced that filming had concluded.  Multiple scene takes were made, allowing the cast to follow the script as written and inject their own improvisations.  Special effects company Legacy Effects provided creature designs and effects for the aliens.   The alien costume featured an animatronic head, remotely controlled by three people. The suit was hot even in cold areas, and had to be unzipped to let Doug Jones cool off between takes. The costume was enhanced with CGI in a number of ways, including replacing arms and dilating pupils (which Schaffer thought made the creature seem more alive). 

===Marketing=== Trayvon Martin in Sanford, Florida by neighborhood-watch captain George Zimmerman. The trailer featured Hill imitating a gun with his hand pretending to shoot at teenagers, while the poster featured a bullet-ridden, alien-themed neighborhood-watch sign. According to insiders Fox intended to move into the next phase of the marketing campaign as soon as possible (focusing on the films science-fiction aspects), while replacing previous posters with images of the cast.   Fox maintained that the July 27, 2012, release date would remain unchanged by the Martin case or the marketing changes. 

In a statement about the changes Fox said, "We are very sensitive to the Trayvon Martin case, but our film is a broad alien-invasion comedy and bears absolutely no relation to the tragic events in Florida...these initial marketing materials were released before this incident ever came to light. The teaser materials were part of an early phase of our marketing and were never planned for long-term use."  The alien-focused campaign began on May 4, 2012 with the release of a new trailer, coinciding with Foxs changing the films title to The Watch to further distance the film from the Martin case.  

==Release==
The premiere of The Watch took place on July 23, 2012 at Graumans Chinese Theatre in Hollywood.   It was released in North America on July 27, 2012. On July 25, Harkins Theatres (the sixth-largest North American cinema chain) announced it would not be showing the film after failing to reach a financial agreement with Fox. 

===Box office=== mass murder in a Colorado cinema the previous week. This, plus competition from the simultaneous launch of the 2012 Summer Olympics, would negatively impact ticket sales for The Watch. Tracking showed that the film could earn $13–15&nbsp;million during its opening weekend.   

The film earned an estimated $4.5&nbsp;million on its opening day. During its opening weekend it earned $12.7&nbsp;million from 3,168 theaters – an average of $4,025 per theater – ranking third behind   ($13.3&nbsp;million) and The Dark Knight Rises ($62.1&nbsp;million).   The largest segments of the opening-weekend audience were over age 25 (59%) and male (60%).  The film left theaters on October 18, 2012 (after 12 weeks) with a total gross of $35.3&nbsp;million. 

Outside North America, the film had its most successful opening weekends in the United Kingdom ($3.5&nbsp;million), Australia ($1.8&nbsp;million) and Russia ($1.3&nbsp;million).  These countries also represented its largest total grosses, with $6&nbsp;million from the UK, $5.9&nbsp;million from Australia and $3.2&nbsp;million from Russia. 

===Critical reception=== average rating of 4 out of 10. The consensus is that "The Watch uneasily mixes sci-fi elements with gross-out gags and strands its talented cast with a script that favors vulgarity over wit at nearly every turn."  Metacritic gave it a score of 36 out of 100 from 35 critics, indicating "generally unfavorable" reviews.  CinemaScore polls reported that the average grade moviegoers gave the film was a C+ on an A+-to-F scale. 

The Hollywood Reporter s Sheri Linden described the film as feeling like "part three of a past-its-prime franchise", criticizing the plot as moving "lazily from setup to punchline to setup, with no particular point and almost no punch".  Roger Ebert awarded the film 2 out of 4 stars, stating that the film "has lots of energy but not much inspiration". Ebert commented that the comic timing of the lead actors benefits the dialog, but frequent instances of "crude, vulgar and offensive" comedy became unwelcome and unamusing.  The Rolling Stone s Peter Travers awarded the film 1.5 out of 4 stars, stating that it should have been a "Ghostbusters lite" and "should be crazy, stupid fun but settles for just stupid." He was also critical of the use of Costco in the plot, which he considered product placement. However, he praised Hills "scene-stealing" performance. 

  s Justin Chang complimented the interaction between the four leads, but criticized the film for relegating ethnic characters to murder victims and female characters "to be protected or consoled". He described the film as a "lowbrow, lame-brained mash-up of buddies-on-patrol comedy and sci-fi actioner, held together (barely) by an endless string of penis jokes". However, he praised Ayoades "mellow, nerdy-funky appeal   one of the   few novel aspects". 

The Los Angeles Times  Betsy Sharkey was more positive, stating that "the funniest stuff comes from the kind of situational misfires that can happen when dudes try to do things, like catch aliens, that they are clearly not cut out to do". She considered that Schaffers previous experience directing short comic videos for Saturday Night Live was partially responsible for individual comic moments broken up by transitions where "things tend to fall apart".  Screencrush s Matt Singer was positive, calling the film funny and praising the improvisational feel of the scenes and "ingenious" set-pieces, but was critical of the promotion of Costco and Costco products throughout.  Boxoffice (magazine)|Boxoffice s James Rocchi offered a positive stance, stating that the "plot moves, the supporting cast is lively and the action stays small-scale and intimate, never overwhelming the laughs"; he felt that the profanity and vulgarity never became "stale". Rocchi, however, commented that the film was forgettable and "disposable". 

===Home media===
The Watch was released on DVD, Blu-ray disc and digital download on November 13, 2012. The DVD version contains deleted scenes, a gag reel, the theatrical trailer and feature videos "Alien Invasions & You" and "Casting the Alien". The Blu-ray disc contains the DVD content, a digital copy of the film and two additional features: "Jonah Alternate Takes" and "Watchmakers."  

==References==
{{Reflist|30em|refs=

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   
}}

==External links==
*  
*  
*  
*  
*  
*  

  

 
 
   
   
 
 
 
 
 
 
 
 