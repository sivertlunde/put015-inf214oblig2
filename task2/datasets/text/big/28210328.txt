Forbidden (1949 film)
{{Infobox film
| name           = Forbidden
| image          = Forbidden 1949 film poster.jpg
| image_size     =
| caption        = Theatrical release poster George King
| producer       = George King
| writer         = Katherine Strueby
| starring       = Douglass Montgomery Patricia Burke Hazel Court
| music          = George Melachrino
| cinematography = Hone Glendinning
| editing        = Douglas Myers
| distributor    = British Lion Films
| released       = 28 February 1949
| runtime        = 87 min.
| country        = United Kingdom English
| gross          = £110,903 (UK) 
}}
 British Thriller thriller film, George King, George King for Pennant Picturens, and was the last directorial assignment for King, and the final screen appearance by Montgomery.

==Plot summary==
This thriller is set in Blackpool, where trained chemist Jim Harding (Douglass Montgomery) has been reduced to making a living peddling potions and medicines from a fairground stall with a former army colleague Dan Collins (Ronald Shiner).   Trapped in a loveless marriage with the vulgar, shrewish and domineering harpy Diana (Patricia Burke), a woman who harbours ambitions of breaking into showbusiness, Jim finds himself attracted to the kinder working-class Jane Thompson (Hazel Court), who sells candyfloss and ice cream at an adjacent stall on the fairground. Jim does not reveal to Jane that he is married as the two fall in love and begin an affair.  Diana meanwhile is engaged in a liaison of her own with the older Jerry Burns (Garry Marsh), who she believes will be able to help with her theatrical aspirations.

Diana finds out about Jims affair and visits Jane at home, revealing Jims married status, trying to convince her that Jim is a serial philanderer and Jane is only the latest in a succession of young women he has targeted, and offering her cash to end the relationship.  Jane refuses to be bought off and confronts Jim, who protests that he is caught in an intolerably unhappy marital situation with a selfish, unscrupulous woman.  Jim then faces up to Diana and demands a divorce, which she refuses out of hand.

In desperation, Jim determines that the only way out of the mess is to kill Diana.  Aware of her addiction to multiple medications, he uses his knowledge to concoct some pills containing a lethal dose which he slips amongst her habitual supply.  Then having second thoughts, he hurries home but finds Diana already dead.  In a panic he buries her body beneath the floorboards in his workshop, only to discover later when clearing up in the bedroom that the deadly pills he made up are untouched &ndash; Diana in fact has died of natural causes and Jims disposal of her body has been unnecessary and incriminating.

Dianas disappearance in unexplained circumstances arouses the suspicions of the police, who come to the conclusion that all the indications are that she has been murdered by her husband.  Jim attempts to flee but is tracked down and chased through the streets of the town, where the final confrontation takes place at Blackpool Tower.

==Cast==
* Douglass Montgomery as Jim Harding
* Hazel Court as Jane Thompson
* Patricia Burke as Diana Harding
* Garry Marsh as Jerry Burns
* Ronald Shiner as Dan Collins
* Kenneth Griffith as Johnny
* Eliot Makeham as Pop Thompson
* Frederick Leister as Dr. Franklin Richard Bird as Jennings
* Michael Medwin as Cabby
* Andrew Cruickshank as Inspector Baxter
* Peggy Ann Clifford as Millie Peter Jones as Pete
* Erik Chitty as Schofield
* Sam Kydd as Joe

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 