You Know What Sailors Are (1928 film)
 
{{Infobox film
| name = You Know What Sailors Are
| image =
| image size =
| alt =
| caption =
| director = Maurice Elvey
| producer = A.C. Bromhead and R.C. Bromhead
| writer = E.W. Townsend (novel)
| screenplay =
| story =
| narrator =
| starring =
| music =
| cinematography = Percy Strong
| editing =
| studio = Gaumont British Picture Corporation
| distributor = Gaumont British Distributors (UK)   Universum Film (Germany)
| released =  
| runtime = 80 minutes
| country = United Kingdom
| language = English
| budget =
| gross =
}}
 silent comedy comedy drama film directed by Maurice Elvey and starring Alf Goddard, Cyril McLaglen and Chili Bouchier.  The crews of British and Spanish ships enter into a bitter rivalry. It was made at Lime Grove Studios in Shepherds Bush. The film is based on the novel A Light for his Pipe by E.W. Townsend.

==Cast==
* Alf Goddard as The British Mate
* Cyril McLaglen as The Spanish Mate
* Chili Bouchier as The Spanish Captains Daughter
* Jerrold Robertshaw as The Spanish Captain
* Mathilde Comont as The British Captain
* Leonard Barry as The British Captains Husband
* Mike Johnson as Seaman
* Wally Patch as Seaman

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 


 
 