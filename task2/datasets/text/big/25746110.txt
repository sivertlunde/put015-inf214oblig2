The Great Texas Dynamite Chase
{{Infobox Film
| name           = The Great Texas Dynamite Chase
| image          = 
| image_size     = 
| caption        = 
| director       = Michael Pressman
| writer         = 
| narrator       = 
| starring       = Claudia Jennings
| music          = 
| cinematography = 
| editing        = 
| distributor    =  
| released       = July 1976
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
The Great Texas Dynamite Chase, also known as Dynamite Women, is a 1976 criminal comedy film directed by Michael Pressman. 

==Plot summary==
Busting out of prison, sexy blonde Claudia Jennings (as Candy Morgan) gets out of her jumpsuit and robs a small Texas bank, with lighted sticks of dynamite. Ms. Jennings is assisted by sexy blonde bank teller Jocelyn Jones (as Ellie-Jo Turner), who has just been fired for "total lack of character." Lingering in bed with men causes Ms. Jones to be late for work. Later, Jennings picks up Jones hitchhiking. The two tightly outfitted women decide to team-up and become a modern day "Bonnie and Clyde" (or "Bonnie and Bonnie"). They meet ex-"Riflemen" cowboy Johnny Crawford (as Slim) robbing a convenience store, and take him hostage. Knowing good gigs when he sees them, Mr. Crawford makes the dynamite duo a threesome.

==Cast==
*Claudia Jennings	... 	Candy Morgan
*Tara Strohmeier	... 	Pam Morgan
*Jocelyn Jones	... 	Ellie-Jo Turner
*Miles Watkins	... 	Boyfriend
*Nancy Bleier	... 	Carol
*Buddy Kling	... 	Mr. Sherman
*Oliver Clark	... 	Officer Andy
*Tom Rosqui	... 	Jason Morgan
*Ed Steef	... 	Todd
*Danny Sullivan	... 	Young Texan
*Bart Braverman	... 	Freddie
*Peggy Brenner	... 	Bank Teller
*Jim Boles	... 	Mr. Ralston
*Christopher Pennock	... 	Jake
*Priscilla Pointer	... 	Miss Harris

==External links==
* 

 
 
 
 
 
 
 