Strawberry Shortcakes (manga)
{{Infobox animanga/Header
| name            = Strawberry Shortcakes
| image           =  
| caption         = Cover of Strawberry Shortcakes as published by Shodensha
| genre           = 
}}
{{Infobox animanga/Print
| type            = manga
| author          = Kiriko Nananan
| publisher       = Shodensha
| publisher_en    = 
| demographic     = Josei
| magazine        = Feel Young
| published       =  2002
| volumes         = 1
}}
{{Infobox animanga/Video
| type            = live film
| title           = 
| director        = Hitoshi Yazaki
| producer        = 
| writer          = 
| music           = 
| studio          = 
| released        = 2006
| runtime         = 127 minutes
}}
 
 Sakka  a film of the same title.  It was once licensed for English release by Central Park Media as Sweet Cream & Red Strawberries. 

==Reception==
Xavier Guilbert, writing for du9, feels it is quite different from other josei manga.  M. Natali, writing for BD Gest, notes that the nonlinear presentation of the womens stories reinforces the connection between the characters, a resonance between the four characters lives, their feelings and disappointments.  The reviewer for Manga-News said that the author "managed to get it right".  Art wise both du9 and BD Gest described it as uncluttered & aerial,   and the use of oblong panels taking all the width of the page as reinforcing the impression of intimacy.    

==References==
 

==External links==
* 

 
 
 
 