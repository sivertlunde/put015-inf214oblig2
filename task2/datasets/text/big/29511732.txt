A Millionaire for Christy
{{Infobox film
| name           = A Millionaire for Christy
| image          = AMillionaireForChristy1951Poster.jpg
| alt            = 
| caption        = Film poster George Marshall
| producer       = Bert E. Friedlob
| writer         = Ken Englund Robert Harari (story)
| narrator       =
| starring       = Fred MacMurray Eleanor Parker
| music          = Victor Young
| cinematography = Harry Stradling Sr.
| editing        = Daniel Mandell
| studio         = Thor Productions Inc.
| distributor    = Twentieth Century Fox Film Corporation
| released       =  
| runtime        = 91 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $1 million (US rentals) 
}}

A Millionaire for Christy is a 1951 comedy film directed by George Marshall, and starring Fred MacMurray and Eleanor Parker. A screwball comedy, where Christy Sloane (Parker) is a legal secretary from San Francisco who is sent to California to inform radio host Peter Lockwood (MacMurray) that he has just inherited $2 million.

==Plot summary== pesos ($2,000,000) that a deceased uncle has bequeathed him and obtain the necessary legal documents. The pragmatic Patsy sees the assignment as the opportunity Christy has been waiting for and counsels her to romance the heir before telling him of his good fortune. Christy is not inclined to a gold digger, but Patsys words ring in her ears as she makes the journey. Unbeknownst to her, Peter is "the Sunshine Man," the self-promoting and simpering host of a radio program on which he offers syrupy homilies and moral tales about "spreading the sunshine around" (often linked to the products of his sponsors). Christy arrives on Peters wedding day as he is scurrying to get dressed for the ceremony and finish packing for a honeymoon cruise to Honolulu with his bride, heiress June Chandler.
 Richard Carlson), who despite his irritation over losing June to Peter is to be best man at the wedding, enters the apartment and assumes that Peter is having one last fling before his marriage. While Christy is trying to explain her mission from his bathroom, Peter is in the living room trying to convince Doc that appearances are not what they seem and does not hear her.

Christy follows Peter and Doc to the Chandler mansion where the wedding is being held. Doc refuses to be part of the wedding, and a bewildered Christy inadvertently gives the impression that she is Peters girl friend, whom he has abandoned for June. Christys erratic behavior and insistence that Peter has inherited a fortune in pesos convince everyone that she is unbalanced. The wedding is called off until Peter can clear himself by driving Christy to Docs clinic in La Jolla to confirm her instability. During the trip, fog rolls in and Peter drives off the highway onto a beach, where he loses the car keys. The wet and weary couple are found by a group of Mexican railroad workers, who, mistaking them for newlyweds, welcome them into their crew car on a nearby siding (rail)|siding. After enjoying a night of dancing and drinking tequila with the Mexicans, Peter and Christy go out into the moonlight and kiss. In the morning, Peter apologizes to Christy for making the advances as the car is being towed to La Jolla.

Doc talks with Christy, who explains everything, and confirms the truth with her boss. Doc bemoans the fact that while his clinic is facing financial ruin, Peter has just come into a fortune of his own, but also deduces that Christy is in love with Peter. Hoping to regain June for himself and Peter for Christy, Doc conspires with her to perpetuate her "illness" for 24 hours while he tricks Peter into "letting her down gently" in order to effect a cure. After putting up Peter in his own room and installing Christy in the same hotel, Doc summons June, who finds Peter again succumbing to Christys charms. Doc, however, persuades June that Peter was just following Docs therapy. The glum Doc and Christy get drunk together in the hotel bar, where one of Peters Sunshine Man broadcasts makes Doc want to make him "eat his words." They approach Peter and June to "share" their bottle of tequila. After several drinks, Peter, still believing that he is humoring Christy, is cajoled by the jealous June and intoxicated Doc to pledge his entire inheritance to various La Jolla charities, including Docs clinic.

The next morning, a hungover Peter is awakened by the press and a stream of well-wishers, all congratulating him on his generosity. Upon learning that he really did inherit two million dollars and has given it away, Peter is dumbfounded and believes he owes Christy an apology for thinking that she was crazy. June angrily warns him not to see her again but he knocks on Christys door anyway, only to see her leaving. Peter chases her into a train station, where she hides in the ladies room. Peter declares his love for her, and literally smokes her out with burning newspapers. The happy couple escape pursuing police and reporters by jumping aboard a train carrying their Mexican friends.

==Cast==
* Fred MacMurray as Peter Ulysses Lockwood
* Eleanor Parker as Christabel "Christy" Sloane Richard Carlson as Dr. Roland "Doc" Cook
* Una Merkel as Patsy Clifford
* Chris-Pin Martin as Manolo
* Douglass Dumbrille as A.K. Thompson
* Kay Buckley as June Chandler
* Raymond Greenleaf as Benjamin Chandler
* Nestor Paiva as Mr. Rapello

==References==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 