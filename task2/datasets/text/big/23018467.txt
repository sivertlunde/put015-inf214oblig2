Felicitas (film)
{{Infobox film
| name           = Felicitas
| image          =
| caption        =
| director       = María Teresa Costantini
| producer       = Margarita Gómez
| screenplay     = Graciela Maglie Sabrina Farji Felix Augusto Quadros Teresa Correa Avila
| starring       = Sabrina Garciarena
| music          = Nico Muhly
| cinematography = Lula Carvalho
| editing        = Laura Bua
| released       =  
| runtime        = 128 minutes
| country        = Argentina
| language       = Spanish
| studio         = Buenos Aires Producciones S.A.
}}
Felicitas is a 2009 Argentine romantic drama film directed by María Teresa Costantini.

==Cast==
* Sabrina Garciarena as Felicitas
* Gonzalo Heredia as Enrique Ocampo
* Alejandro Awada as Carlos Guerrero
* Ana Celentano as Felisa Guerrero
* Nicolas Mateo as Cristian De Maria
* Antonella Costa as Manuela
* Luis Brandoni as Martín de Alzaga

==External links==
*  

 
 
 
 
 

 