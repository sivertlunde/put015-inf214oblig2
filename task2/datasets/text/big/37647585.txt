Edegarike
{{Infobox film
| name           = Edegarike
| image          = 2012 Kannada film Edegarike poster.jpg
| caption        = 
| director       = D. Sumana Kittur
| producer       = Syed Aman Bachchan   M. S. Ravindra
| based on       =  
| writer         = Agni Shridhar
| screenplay     = Agni Shridhar D. Sumana Kittur Aditya Atul Kulkarni Akanksha
| music          = Sadhu Kokila
| cinematography = B. Rakesh
| editing        = Anthony L. Ruben
| studio         = Megha Movies
| distributor    = 
| released       =  
| runtime        = 105 minutes
| country        = India Kannada
| budget         = 
| gross          = 
}}
 Kannada Crime crime Drama drama film Aditya in the lead role as Sona, a Contract killing|hitman, who decides to relinquish his life of crime which unnerves his boss, who decides to have him contract killed. Atul Kulkarni, Achyuth Kumar, Srujan Lokesh, Dharma and Akanksha Mansurkar feature in supporting roles.

The film is based on the real-life incidents that occurred in the 1990s and is about cult figures in the Karnataka underworld such as Sridhar, Muthappa Rai and Bachchan.   The film opened to widespread critical acclaim upon theatrical release.  It won the Special Jury Award at the 6th Bangalore International Film Festival.   

==Cast== Aditya as Sona
* Atul Kulkarni as Sridhar Murthy
* Akanksha Mansurkar as Rashmi
* Achyuth Kumar as Tukaram Shetty
* Sharath Lohitashwa as Anthony "Kalia"
* Dharma as Muddappa
* Srujan Lokesh as Bachchan
* Chi. Guru Dutt as Dholakia Ramakrishna as Rashmis father Police Inspector J. Nayak, Crime Branch

==Production==

===Development=== crime novel written by journalist Agni Sridhar in the early 2000s about the India Mafia circling around Mumbai and Bangalore headed a mafia leader from Dubai. The novel had been a chart topper for several weeks and proved a hit among readers.  Since then, around 15 attempts were made to adapt it into a film. Following this, Sumana Kittur who had earlier directed Slum Bala and Kallara Santhe took up the project.    

===Casting and filming=== Kishore would Bhavana had Aditya was then signed in to play the lead role in the film, with Srujan Lokesh to play a supporting character.  Filming that began in late-2011 continued for a period of 35 days in places such as Bangalore, Sakleshpur, Mangalore.  It completed in June 2012. 

==Soundtrack==
{{Infobox album
| Name        = Edegarike
| Type        = Soundtrack
| Artist      = Sadhu Kokila
| Cover       = 2012 Kannada film Edegarike album cover.jpeg
| Border      = yes
| Caption     = Soundtrack cover
| Released    = November 2012
| Recorded    =  Feature film soundtrack
| Length      = 3:48
| Label       = Anand Audio
| Producer    = 
| Last album  = Sanju Weds Geetha 2011
| This album  = Edegarike 2012
| Next album  = Myna (film)|Myna 2013
}}

Sadhu Kokila composed music for the films soundtrack, and the lyrics were written by Sumana Kittur. The album consists of one soundtrack. Kokila also sung the track. 

{{tracklist
| headline = Tracklist
| extra_column = Singer(s)
| total_length = 3:48
| lyrics_credits = yes
| title1 = Neenondu Mugiyada Mouna
| lyrics1 = Sumana Kittur
| extra1 = Sadhu Kokila
| length1 = 3:48
}}

==Release and reception==

===Screenings===
As a part of screening marking 100 years of Indian cinema, Edegarike was screened at the Puerto Rico Film Institute in 2013. It was then screened at the first edition of the Mumbai Womens International Film Festival in October 2013, as the only Indian film.  Following this the film was screened at the 6th edition of the Bangalore International Film Festival in January 2014, where it was awarded the Special Jury Award.

===Critical reception===
Upon release, the film opened to positive reviews from critics. The films direction, screenplay and the performances of all the lead actors received praise. G. S. Kumar of The Times of India reviewed the film giving it a rating of four out of five and wrote, "Gripping narration and excellent screenplay with a touch of romance make the story quite interesting." He concluding writing praises of the performances of actors playing pivotal roles in the film.  Writing for Daily News and Analysis, Shruti I. L. gave the film a 3.5/5 rating and wrote, "The film remains loyal to the book and comes only with a few changes that are needed to cater to cinematic sensibilities. The script and screenplay is aptly supported by hard hitting and thought provoking dialogues" and added, "Director Suman Kittur has managed to extract stellar performances from each of her actors. Lead actors Adithya and Atul Kulkarni steal the show with their understated acts."  Newstrackindia gave the film a 3/5 rating and said the film "rides high on good script   powerful performances". It added that "a good script, background score and perfect locations back the film." The reviewer concluded praising the roles of acting, cinematography and music in the film, calling Adityas performance "top class". 

==Awards==
{| class="wikitable" style="width:99%;"   
|-
! width="25%" scope="col" | Award / Film Festival
! width="25%" scope="col" | Category
! width="25%" scope="col" | Recipient
! width="10%" scope="col" | Result
! width="3%" scope="col" | Reference
|-
| rowspan="1" |  Bangalore International Film Festival 
|  Special Jury Award 
|  Sumana Kittur 
| rowspan="1"  
| align="center" rowspan="1" |  
|-
| rowspan="2" |  Karnataka State Film Awards  Third Best Film 
|  Syed Aman Bachchan, M. S. Ravindra 
| rowspan="2"  
| align="center" rowspan="2" |  
|- Best Cinematographer 
|  B. Rakesh 
|-
| rowspan="4" |  Filmfare Awards South  Best Supporting Actor – Kannada 
|  Atul Kulkarni 
| rowspan="1"  
| align="center" rowspan="4" |   
|- Best Film – Kannada 
|  Syed Aman Bachchan, M. S. Ravindra 
| rowspan="3"  
|- Best Director – Kannada 
|  Sumana Kittur 
|- Best Actor – Kannada  Aditya 
|-
| rowspan="2" |  Udaya Film Awards 
|  Best Supporting Actor 
|  Dharma 
| rowspan="2"  
| align="center" rowspan="2" |  
|-
|  Best Male Playback Singer 
|  Sadhu Kokila 
|-
| rowspan="3" |  Bangalore Times Film Awards 
|  Best Film 
|  Syed Aman Bachchan, M. S. Ravindra 
| rowspan="3"  
| align="center" rowspan="3" |  
|-
|  Best Director 
|  Sumana Kittur 
|-
|  Best Actor – Male 
|  Aditya 
|}

==See also==
* List of crime films
* India Mafia

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 