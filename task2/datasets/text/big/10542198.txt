A Man Called Horse (film)
{{Infobox film
| name           = A Man Called Horse
| image          = Man called horse poster.jpg
| caption        = Theatrical release poster design by Tom Jung
| director       = Elliot Silverstein
| writer         = Jack DeWitt Dorothy M. Johnson
| starring       = Richard Harris Judith Anderson Frank Brill Sandy Howard
| music          = Leonard Rosenman Lloyd One Star
| cinematography = Robert Hauser Gabriel Torres Philip W. Anderson
| studio         = Cinema Center Films
| distributor    = National General Pictures (1970, original) Paramount Pictures (2003, DVD, and 2011, Blu-Ray DVD) 
| released       =  
| runtime        = 114 minutes
| country        = United States
| language       = English
| gross = $6 million (US/ Canada rentals) 
}} A Man Native American tribe.

==Plot== hamstrung behind both knees.

Determining that his only chance of freedom is to gain the respect of the tribe, he overcomes his repugnance and kills two warriors from the neighboring (enemy) Shoshone tribe, which allows him to claim warrior status. After his victory, he proposes marriage to one of the women with the horses taken in battle as bride-price and undergoes painful initiation rites, taking the native name "Shunkawakan" (or "Horse") as his Sioux name.

When one of the warriors takes a vow never to retreat in battle, Morgans changing perspective is shown, as he turns angrily on the uncomprehending Baptiste, telling him "Five years youve lived here, and youve learned nothing about these people – all his death is to you is a means of escape."

After successfully helping to fend off an attack by the enemy tribe, he becomes a respected member of the tribe and ultimately their leader.

==Cast==
* Richard Harris as John Morgan
* Judith Anderson as Buffalo Cow Head
* Jean Gascon as Baptiste
* Manu Tupou as Yellow Hand
* Corinna Tsopei as Running Deer
* Dub Taylor as Joe
* James Gammon as Ed William Jordan as Bent
* Eddie Little Sky as Black Eagle
* Michael Baseleon as Longfoot
* Lina Marín as Thorn Rose
* Tamara Garina as Elk Woman
* Terry Leonard as Striking Bear
* Iron Eyes Cody as Medicine man
* Tom Tyon as Medicine man

The tribal people were acted by members of the Rosebud Sioux tribe of South Dakota.

==Production== John Chambers created a prosthetic chest.   

==Sequels==
Two sequels to the original movie were made, both with Harris reprising his role:

* The Return of a Man Called Horse (1976)
* Triumphs of a Man Called Horse (1983)

==Representation of cultures== white man American Indian nations, but encompassing both cultures.  However, some Indian activists criticized the film harshly. Buffy Sainte Marie said, "Even the so-called authentic movies like A Man Called Horse—thats the whitest of movies Ive ever seen."  Vine Deloria, Jr. said, "As we learned from movies like A Man Called Horse, the more accurate and authentic a film is said to be, the more extravagant it is likely to be in at least some aspects of its misrepresentation of Indians." 

==DVD==
A Man Called Horse was released to DVD by Paramount Home Entertainment on April 29th, 2003 as a Region 1 widescreen DVD and on May 31st, 2011 as a Region 1 widescreen Blu-Ray DVD.

==References==
 

== External links ==
*  
*  
*  

 

 

 
 
 
 
 
 
 
 
 
 
 
 
 