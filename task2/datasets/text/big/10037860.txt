Gold Diggers: The Secret of Bear Mountain
 
{{Infobox film
| name           = Gold Diggers: The Secret of Bear Mountain
| image          = Golddiggersposter.jpg
| caption        = Theatrical release poster
| director       = Kevin James Dobson
| writer         = Barry Glasser
| starring       = Christina Ricci Anna Chlumsky Polly Draper Brian Kerwin Diana Scarwid David Keith
| music          = Joel McNeely
| studio         = Bregman/Baer Productions, Inc.
| distributor    = Universal Pictures
| released       = November 3, 1995 (United States) December, 1995 (Australia) 1996 (United Kingdom)
| runtime        = 94 minutes
| country        = United States Canada
| language       = English
| budget         = $9 million
| gross          = $6,029,091
}} Bear Mountain.

==Plot==
In the summer of 1980, Beth Easton and her recently widowed mother move from Los Angeles to a small town in Washington in an attempt to reassemble their broken lives. At first, Beth misses L.A. and resents their new life in the country, but a chance encounter with the outspoken and free-spirited Jody Salerno piques Beths curiosity about the town. Jody, a social outcast with a troubled homelife and an alcoholic mother and her boyfriend Ray who is implied to be abusing Jody, shares Beths love for Winnie the Pooh stories, and the two girls become fast friends. Looking for adventure, Beth and Jody make plans to explore the mysterious caves below Bear Mountain, where legend tells of a long-lost gold mine buried deep within the mountains treacherous depths. However, Beth and Jody are not the only people hunting for this treasure, and the two girls must rely upon their wits, their courage, and the power of their friendship if they are to survive this ordeal and strike it rich.

==Cast==
{| class="wikitable"
|- bgcolor="CCCCCC"
! Actor !! Role
|-
| Christina Ricci || Beth Easton
|-
| Anna Chlumsky || Jody Salerno
|-
| Polly Draper || Kate Easton
|-
| Brian Kerwin || Matt Hollinger
|-
| Diana Scarwid || Lynette Salerno
|-
| David Keith || Ray Karnisak
|-
| Jewel Staite || Samantha
|-
| Roger Cross || Paramedic 
|-
| Jesse Moss || Adam 
|}

==DVD Release==
A special edition DVD of the film was originally set to be released in February 2007, but was cancelled. Currently, Universal is selling the DVD exclusively on Amazon.com as part of the Universal Vault Series These DVDs are MOD (Manufactured On Demand) using DVD-R Recordable Media.

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 