December 7th (film)
{{Infobox film
| name = December 7th
| director = John Ford Gregg Toland Harry Davenport
| cinematography = Gregg Toland
| writer = Budd Schulberg (uncredited)
| editing = Robert Parrish Alfred Newman
| producer = United States Army Air Forces
| distributor = Office of War Information
| runtime = 34 min
| language = English
| color = Technicolor
}}
December 7th (1943) is a propaganda film produced by the US Navy and directed by John Ford and Gregg Toland, about the December 7, 1941 attack on Pearl Harbor, the event which sparked the Pacific War and American involvement in World War II.

==Production background==
The film begins with a chronological breakdown of the events of December 7, starting with the town of Honolulu gradually waking up and coming to life in the morning. A young private is credited with intercepting some vital information which his superiors dismiss; other sailors play baseball or attend religious services.

Then, "like locusts", the Japanese planes start humming over the air above Oahu, and begin the now infamous attack on American military installations on the island, including the sinking of the USS Arizona (BB-39)|Arizona, and the bombing of Hickam Field. All the while, back in Washington, Japanese diplomats are still talking with Secretary of State Cordell Hull.

An animated sequence is then shown, with a large radio tower over Japan, broadcasting a fictional speech by Prime Minister Hideki Tojo. The narrator contradicts most of the "facts" that the Japanese leader tells his listeners in Tokyo, Kobe, and Okure.

After the attack Honolulu isnt quite the same; the island is put under martial law, barbed wire and other protective barriers are set up in case of invasion and even children have to be evacuated and given gas masks. The film is notable for its sympathetic depiction of the Japanese in Hawaii, and the difficulties they now had to go through.

==Preservation status==
Started within days of the attack, the original film was 82 minutes long and asked some embarrassing questions, such as why there was no long-range reconnaissance and no short range air patrols. Further, the film had a lot of time devoted to the culture of the 160,000 Japanese in Hawaii and their response to the attack. For these reasons the long version of the film was censored for decades and the shorter 32-minute version released.

==Awards== Documentary Short Subject.   

==See also==
*List of Allied propaganda films of World War 2

==References==
 

==External links==
 

==External links and reference==
*  
*  

 
 

 
 
 
 
 
 
 
 
 


 