The Radio Pirates
{{Infobox film
| name           = The Radio Pirates
| caption        = 
| director       = Stig Svendsen
| producer       = Håkon Øverås Aagot Skjeldal
| writer         = Gunnar Germundson (also radio play)
| starring       = Gard Eidsvold Henrik Mestad Per Christian Ellefsen Ane Dahl Torp
| music          = Bjørnar Johnsen
| cinematography = John Andreas Andersen
| editing        = Pål Gengenbach Siv Ebelstoft
| distributor    = Nordisk Filmdistribusjon (Norway only)
| released       = September 14, 2007
| runtime        = 85 minutes
| country        = Norway
| language       = Norwegian
| budget         =   (estimated)
}}
 Norwegian family director Stig Svendsen and is based on a radio play by Gunnar Germundson. It stars Gard B. Eidsvold, Per Christian Ellefsen, Henrik Mestad and Ane Dahl Torp.

==Plot==
The Radio Pirates is the story of Karl Jonathan and his father. They leave the city for the fathers dilapidated childhood home but soon realise that the entire village of "Skjelleruten" has been transformed into the ultimate safe society, where children are strictly protected from behaving like children. Karl Jonathan and his new friend, Sisseline, start an uprising against this model village with the aid of a closed-down pirate radio. But the model citizens refuse to give in without a fight!

==External links==
*  

 
 
 
 
 

 