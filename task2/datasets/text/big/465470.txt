The Prince of Egypt
 
 
{{Infobox film
| name        = The Prince of Egypt
| image       = Prince of egypt ver2.jpg
| caption     = Theatrical release poster
| director    = Simon Wells Brenda Chapman Steve Hickner
| producer    = Penney Finkelman Cox Sandra Rabins Jeffrey Katzenberg  (executive producer) 
| screenplay  = Philip LaZebnik Nicholas Meyer
| starring    = Val Kilmer Ralph Fiennes Michelle Pfeiffer Sandra Bullock Jeff Goldblum Patrick Stewart Danny Glover Steve Martin Martin Short
| based on    = The Book of Exodus Stephen Schwartz  (Songs)  
| editing     = Nick Fletcher DreamWorks Pictures   
| distributor = DreamWorks Pictures 
| released    =  
| runtime     = 98 minutes
| country     = United States English Hebrew
| budget      = $70 million   
| gross       = $218,613,188 
}} animated epic epic musical musical List biblical film children of Stephen Schwartz Deliver Us", in over seventeen languages for the films dubbing),  who sang their own parts.
 Original Musical Best Original 1999 Academy 1999 Golden Globes,    and was also nominated for Outstanding Performance of a Song for a Feature Film at the ALMA Awards.
 Disney animated traditionally animated non-Disney film until 2007, when it was out-grossed by 20th Century Foxs  The Simpsons Movie.    The Prince of Egypt is DreamWorks Animations only traditionally animated film to win an Oscar and one of the four DreamWorks Animation films to be nominated for more than one Oscar.

==Plot==
In Ancient Egypt, Jochebed|Yocheved, a Hebrew slave, and her two children, Miriam and Aaron, watch as Hebrew babies are taken and slaughtered by Egyptian soldiers, as ordered by Seti I, who fears that an increase in Hebrew slaves could lead to rebellion. To save her own newborn son Moses, Yocheved places him in a basket afloat on the Nile. Miriam follows the basket to the Pharaohs palace and witnesses her baby brother adopted by Tuya (queen)|Pharaohs queen.

Twenty years later, Moses and his foster brother Rameses are scolded by their father for accidentally destroying a temple during one of their youthful misadventures, though Moses tries to take the blame and says that Rameses wants their fathers approval.  That evening at a palace banquet, Seti, deciding to give Rameses this opportunity, names him Prince regent and gives him authority over Egypts temples. As a tribute, the high priests Hotep and Huy offer him the captive Tzipporah, and Rameses gives her to Moses.  Moses debunks Tzipporah, and Rameses appoints him Royal Chief Architect.

Later that night, Moses helps Tzipporah escape from the palace and is reunited with his siblings Miriam and Aaron.  Despite Aarons attempts to protect her, Miriam tries to tell Moses about his past, but he refuses to listen to her and returns to the palace.  The truth about his past is later confirmed by a nightmare, and finally by Seti himself. The next day, Moses accidentally pushes an Egyptian guard off the scaffolding of the temple, while trying to stop him from whipping a Hebrew slave, and the guard falls to his death.

Ashamed and confused, Moses flees into the desert in exile, despite Rameses pleas to stay.  After Moses defends Tzipporahs younger sisters from bandits, he is welcomed into the tribe by their father Jethro (Bible)|Jethro. After assimilating this new culture, Moses becomes a shepherd and marries Tzipporah. While chasing a stray lamb, Moses discovers a burning bush through which God instructs him to guide the Hebrew slaves to their promised land, and bestows Moses shepherding staff with his power. Moses and Tzipporah return to Egypt, where Moses is happily greeted by Rameses, who is now Pharaoh.

When Moses requests the Hebrews release and changes his staff into an Egyptian cobra, to demonstrate his alliance with God, Hotep and Huy boastfully re-create this transformation, only to have their snakes eaten by Moses snake. Rather than persuaded, Rameses is hardened and increases the Hebrews workload. Moses and Tzipporah thereafter live with Miriam, who convinces Aaron and the other Hebrews to trust them. Later, Moses inflicts nine of the Plagues of Egypt; but Rameses refuses to relent, and Moses prepares the Hebrews for the tenth and final plague. That night, the final plague kills all the firstborn children of Egypt, including Rameses son, while sparing those of the Hebrews. The next day, Rameses finally gives Moses permission to free the Hebrews.
 part the sea, while a fire blocks the armys way. The Hebrews cross the open sea bottom; and when the fire vanishes and the army gives chase, the water closes over the Egyptian soldiers, sparing Ramses alone. Thereafter Moses leads the Hebrews to Mount Sinai, where he receives the Ten Commandments.

==Cast==
* Val Kilmer as Moses, a Hebrew who was adopted by Pharaoh Seti.
** Val Kilmer also provides (uncredited) the voice of God
** Amick Byram provides Moses singing voice. Rameses II, Moses adoptive brother and eventual successor to his father, Seti.
* Michelle Pfeiffer as Tzipporah, Jethros oldest daughter and Moses wife.
* Sandra Bullock as Miriam, Moses and Aarons biological sister.
** Sally Dworsky provides Miriams singing voice.
** Eden Riegel provides both the speaking and singing voice of a younger Miriam.
* Jeff Goldblum as Aaron, Moses and Miriams biological brother.
* Patrick Stewart as Pharaoh Seti I, Rameses father, Moses adoptive father and the first Pharaoh in the film. Despite his callousness towards the Hebrew slaves, he is shown to treat Moses and Rameses with care and love.
* Danny Glover as Jethro (Bible)|Jethro, Tzipporahs father and Midians high priest.
** Brian Stokes Mitchell provides Jethros singing voice.
* Helen Mirren as Queen Tuya, Setis consort, Rameses mother, and Moses adoptive mother.
** Linda Dee Shayne provides Queen Tuyas singing voice.
* Steve Martin as Hotep Huy
* Ofra Haza as Jochebed|Yocheved, the biological mother of Miriam, Aaron, and Moses.

Director Brenda Chapman briefly voiced Miriam when she sings the lullaby to Moses. The vocal had been recorded for a scratch audio track, which was intended to be replaced later by Eden Riegel. The track turned out so well that it remained in the film.

==Production==

===Development=== The Ten Commandments. While working for The Walt Disney Company, Katzenberg suggested this idea to Michael Eisner, but he refused. The idea for the film was brought back at the formation of DreamWorks SKG in 1994, when Katzenbergs partners, Amblin Entertainment founder Steven Spielberg, and music producer David Geffen, were meeting in Spielbergs living room.    Katzenberg recalls that Spielberg looked at him during the meeting and said, "You ought to do The Ten Commandments." 
 Avid Media Biblical scholars, Christian, Jewish and Muslim Theology|theologians, and Arab American leaders to help his film be more accurate and faithful to the original The Exodus|story. After previewing the developing film, all these leaders noted that the studio executives listened and responded to their ideas, and praised the studio for reaching out for comment from outside sources. 

===Design and animation===
Art directors Kathy Altieri and Richard Chavez and Production Designer Darek Gogol led a team of nine visual development artists in setting a visual style for the film that was representative of the time, the scale and the architectural style of Ancient Egypt.    Part of the process also included the research and collection of artwork from various artists, as well as taking part in trips such as a two-week travel across Egypt by the filmmakers before the films production began. 

Character Designers Carter Goodrich, Carlos Grangel and Nicolas Marlet worked on setting the design and overall look of the characters. Drawing on various inspirations for the widely known characters, the team of character designers worked on designs that had a more realistic feel than the usual animated characters up to that time.  Both character design and art direction worked to set a definite distinction between the symmetrical, more angular look of the Egyptians versus the more organic, natural look of the Hebrews and their related environments.  The Backgrounds department, headed by supervisors Paul Lasaine and Ron Lukas, oversaw a team of artists who were responsible for painting the sets/backdrops from the layouts. Within the film, approximately 934 hand-painted backgrounds were created. 

The animation team for The Prince of Egypt, including 350 artists from 34 different nations, was primarily recruited both from Walt Disney Feature Animation,  which had fallen under Katzenbergs auspices while at The Walt Disney Company, and from Amblimation, a defunct division of Steven Spielbergs Amblin Entertainment.  As at Disneys, character animators were grouped into teams by character: for example, Kristof Serrand, as the supervising animator of Older Moses, set the acting style of the character and assigned scenes to his team.  Consideration was given to properly depicting the ethnicities of the ancient Egyptians, Hebrews, and Nubians seen in the film. 
 Animo software system,  and the compositing of the 2D and 3D elements was done using the "Exposure Tool", a digital solution developed for DreamWorks by Silicon Graphics.      

===Creating the voice of God===
The task of creating Gods voice was given to Lon Bender and the team working with the films music composer, Hans Zimmer.    "The challenge with that voice was to try to evolve it into something that had not been heard before," says Bender. "We did a lot of research into the voices that had been used for past Hollywood movies as well as for radio shows, and we were trying to create something that had never been previously heard not only from a casting standpoint but from a voice manipulation standpoint as well. The solution was to use the voice of actor Val Kilmer to suggest the kind of voice we hear inside our own heads in our everyday lives, as opposed to the larger than life tones with which God has been endowed in prior cinematic incarnations."  As a result, in the final film, Kilmer gave voice to Moses and God, as well, yet the suggestion is that someone else would have heard God speak to him again in his own voice.

===Music===
 

Composer and lyricist Stephen Schwartz began working on writing songs for the film from the beginning of the films production. As the story evolved, he continued to write songs that would serve to both entertain and help move the story along. Composer Hans Zimmer arranged and produced the songs and then eventually wrote the films score. The films score was recorded entirely in London, England. 
 official The Babyface rewrite of the original Schwartz composition, sung by Michelle Pfeiffer and Sally Dworsky in the film. Amy Grant also sings a version of "River Lullaby".

====Musical numbers====
# "Deliver Us"  – Yocheved and Chorus 
# "All I Ever Wanted"  – Moses 
# "River Lullaby"  - Miriam 
# "All I Ever Wanted (Queens Reprise)"  – Queen Tuya 
# "Through Heavens Eyes"  – Jethro 
# "Playing with the Big Boys"  – Hotep and Huy 
# "The Plagues"  – Moses, Rameses, and Chorus 
# "When You Believe"  – Miriam, Tzipporah, and Chorus 

==Reception==

===Box office performance=== gained 4% in its second weekend, earning $15,119,107 and finishing in fourth place. It had a $4,698 average from 3,218 theaters. It would hold well in its third weekend, with only a 25% drop to $11,244,612 for a $3,511 average from 3,202 theaters and once again finishing in fourth place. The film closed on May 27, 1999 after earning $101,413,188 in the United States and Canada with an additional $117,200,000 overseas for a worldwide total of $218.6 million.

 
{| class="wikitable"
|-
|+The Prince of Egypt box office revenue
|-
! Source !! Gross (United States dollar|USD) !! % Total !! All Time Rank (Unadjusted)
|-
| United States & Canada
| $101,413,188 
| 46.4%
| 398 
|-
| Foreign
| $117,200,000 
| 53.6%
|–
|-
| Worldwide
| $218,613,188 
| 100.0%
| 319 
|}

===Critical reception=== normalized 0–100 rating to reviews from mainstream critics, calculated an average score of 64 from the 26 reviews it collected. 

Roger Ebert of the Chicago Sun-Times praised the film in his review saying, "The Prince of Egypt is one of the best-looking animated films ever made. It employs computer-generated animation as an aid to traditional techniques, rather than as a substitute for them, and we sense the touch of human artists in the vision behind the Egyptian monuments, the lonely desert vistas, the thrill of the chariot race, the personalities of the characters. This is a film that shows animation growing up and embracing more complex themes, instead of chaining itself in the category of childrens entertainment."  Richard Corliss of Time (magazine)|Time magazine gave a negative review of the film saying, "The film lacks creative exuberance, any side pockets of joy."  Stephen Hunter from The Washington Post praised the film saying, "The movies proudest accomplishment is that it revises our version of Moses toward something more immediate and believable, more humanly knowable." 

Lisa Alspector from the Chicago Reader praised the film and wrote, "The blend of animation techniques somehow demonstrates mastery modestly, while the special effects are nothing short of magnificent."  Houston Chronicles Jeff Millar reviewed by saying, "The handsomely animated Prince of Egypt is an amalgam of Hollywood biblical epic, Broadway supermusical and nice Sunday school lesson."  James Berardinelli from Reelviews highly praised the film saying, "The animation in The Prince of Egypt is truly top-notch, and is easily a match for anything Disney has turned out in the last decade", and also wrote "this impressive achievement uncovers yet another chink in Disneys once-impregnable animation armor."  Liam Lacey of The Globe and Mail gave a somewhat negative review and wrote, "Prince of Egypt is spectacular but takes itself too seriously."  MovieGuide also reviewed the film favorably, giving it a rare 4 out of 4 stars, saying that, "The Prince of Egypt takes animated movies to a new level of entertainment. Magnificent art, music, story, and realization combine to make The Prince of Egypt one of the most entertaining masterpieces of all time." 

==Home media==
The Prince of Egypt was released on DVD and VHS on September 14, 1999.  The ownership of the film was assumed by DreamWorks Animation when that company split from DreamWorks Pictures in 2004; as with the rest of the DreamWorks Animation catalog, it is available for streaming on Netflix in HD. 

==Awards==
{| class="wikitable"
|-
! Award
! Category
! Recipient
! Result
|-
| rowspan="2" | Academy Awards  Best Original Musical or Comedy Score
|
|  
|- Best Original Song
| "When You Believe"
|  
|-
| rowspan="5" | Annie Awards  Best Animated Feature
|
|  
|-
| Individual Achievement in Directing
| Brenda Chapman, Steve Hickner, and Simon Wells
|  
|-
| Individual Achievement in Storyboarding
| Lorna Cook (Story supervisor)
|  
|-
| Individual Achievement in Effects Animation
| Jamie Lloyd (Effects Lead&nbsp;— Burning Bush/Angel of Death)
|  
|-
| Individual Achievement in Voice Acting
| Ralph Fiennes ("Rameses")
|  
|-
| rowspan="2" | Golden Globe Awards  Best Original Score
|
|  
|- Best Original Song
| "When You Believe"
|  
|- 3rd Golden Satellite Award  Best Animated or Mixed Media Feature
|
| 
|-
|}

==Banning== messengers of God are revered in Islam, and therefore cannot be portrayed".   Following this ruling, the censor board banned the film in January 1999. In the same month, the Film Censorship Board in Malaysia banned the film, but did not provide a specific explanation. The boards secretary said that the censor body ruled the film was "insensitive for religious and moral reasons".   

==Prequel==
 
  Joseph from the Book of Genesis.

==References==
 
* The Jewish study Bible. New York: Oxford University Press, http://www.booktopia.com.au/the-jewish-study-bible-adele-berlin/prod9780195297515.html. Web. Apr 5, 2013. http://books.google.com.au/books?id=aDuy3p5QvEYC&printsec=frontcover&dq=The+Jewish+study+Bible&hl=en&ei=unbbTonKMZGQiQejvpjdDQ&sa=X&oi=book_result&ct=result&resnum=1&ved=0CDsQ6AEwAA#v=onepage&q=The%20Jewish%20study%20Bible&f=false
* "Man." Genetically Change in Ancient Egypt 2.4 (1967):  551-568. JSTOR. Web. Apr 5, 2013. <http://www.jstor.org.ezproxy.hclib.org/stable/2799339?&Search=yes&searchText=Egypt&searchText
* Cerny, Jaroslav. "The Will of Naunakhte and the Related Documents." The Journal of Egyptian Archaeology 31 (1945): 29-53. JSTOR. Web. Apr 6, 2013.  .
* Toivari, Jaana i. "Man versus Woman: Interpersonal Disputes in the Workmens Community of Deir el-Medina." Journal of the Economic and Social History of the Orient 40.2 (1997):  153-173. JSTOR. Web. Apr 6, 2013.  .

==External links==
 
 
*   archived from   on April 12, 2001
*  
*  
*  
*  
*  

 
 
 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 