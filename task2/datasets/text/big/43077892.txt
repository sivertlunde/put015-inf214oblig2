Brothers (2015 film)
 
 
{{Infobox film
| name = Brothers
| image =Brothers-first-look-akshay-kumar-sidharth-malhotra-jackie-shroff-.jpg
| caption =First look poster
| director = Karan Malhotra 
| producer = Hiroo Yash Johar Karan Johar
| writer = Ekta Pathak Malhotra  (Story/Screenplay)  Siddharth-Garima  (Dialogue) 
| based on =  
| starring = Akshay Kumar Sidharth Malhotra Jacqueline Fernandez 
| music = Ajay-Atul
| cinematography = Hemant Chaturvedi
| editing =Akiv Ali
| studio = Dharma Productions Lionsgate Films Endemol India 
| released =   
| runtime =
| country = India
| language = Hindi 
| budget =
| gross =
}}
 Indian action-drama film, directed by Karan Malhotra and produced by Dharma Productions, Lionsgate Films and Endemol India.  The film is an official remake of the 2011 Hollywood film Warrior (2011 film)|Warrior.  It stars Akshay Kumar and Sidharth Malhotra in lead roles, while Jackie Shroff and Jacqueline Fernandez play supporting roles. Principal photography began on October 2014 and the film is scheduled for release on 14 August 2015.        The first look poster of the film was released on 9th of March 2015.The movie trailer will release on 7th of June.   

== Cast ==
* Akshay Kumar
* Sidharth Malhotra
* Jackie Shroff
* Jacqueline Fernandez

== References ==
 

 
 
 
 
 

 