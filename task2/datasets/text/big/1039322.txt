Black Sheep (1996 film)
 
{{Infobox film
| name           = Black Sheep
| image          = BlackSheep_Poster.jpg
| caption        = Theatrical release poster
| director       = Penelope Spheeris
| producer       = Lorne Michaels Fred Wolf
| starring       = Chris Farley David Spade Gary Busey Tim Matheson William Ross
| cinematography = Daryn Okada
| editing        = Ross Albert
| distributor    = Paramount Pictures
| released       =  
| runtime        = 87 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $32,417,995 
}} Fred Wolf Governor of Chris Owen Kevin and John appear as two security guards at an MTV Rock the Vote concert. It was the second collaboration between Farley and Spade, as well as the duos second film with Paramount Pictures, following the 1995 film Tommy Boy. The film grossed $32.3 million during its U.S. theatrical run. 

==Plot== Vietnam veteran Sgt. Drake Sabitch (Gary Busey), who ends up stealing his rental car.
 Garfield County.

The next day, as Steve and Mike hang fliers, Steve tries to make a call via cell phone; while looking for a place with reception, Steve stumbles upon the home of the aforementioned Drake Sabitch - an old school bus with a TV, a hammock, a grill, and several weapons. While trying to find a high-ground to get reception on his phone, Steve accidentally loosens a rock in a pile of stones. Later on, as the guys play Checkers (game)|checkers, a huge boulder rolls down the mountain, almost completely knocking their cabin off its foundation; that night, a stormy wind blows the cabins roof away and hail falls inside. The next day, Mike tries to talk to Al, but Kovary refuses, so Mike decides to head into Seattle that night to talk to Al, who is going on MTVs Rock the Vote campaign. However, after hanging out with some Rastafarians, Mike makes a fool of himself onstage (culminating to his yelling "KILL WHITEY!" to a suddenly silent audience) as a shocked Al and Kovary helplessly watch. Because of his stunt, Al decides to have Mike to not bother helping him with the campaign, leaving Mike down. The next day, Steve and Mike sneak into Drakes home (after dodging some hidden land mines) to watch Als debate on his TV. When Steve goes outside to use the restroom, he is attacked by Drake, but is saved by Mike, who beats the ex-soldier in unarmed combat. Drake is impressed by Mikes fighting skill and befriends both men.

Governor Tracy, in hopes of sabotaging Al following their debate, purchases the pictures of Mike at the rec center fire and posts them on the TV news, therefore allowing Tracy to win the election. Mike notices that the voting results are wrong, since the total vote count is 1,882 for Garfield County, when in fact there are only 1,502 registered voters there; furthermore, Mike recognizes the two men who set the recreation center on fire standing next to Tracy. Mike and Steve go to the Garfield County Courthouse, where they obtain the names of the voters in the election. Steve discovers that over half the people who voted for Tracy have been dead for over ten years (including Drakes father and grandfather), proving Tracy had rigged the elections. To get this to the people and Al, Steve and Mike borrow Robbies squad car to Governor Tracys victory party the following day.

At the party, the duo appears during Tracys victory speech and the police try to arrest Mike for arson. At the podium, Mike takes a gun from one of the cops and pretends to hold Steve hostage, while Drake shows up in time to prevent a sniper from shooting Mike and controls the crowd by threatening them with an Rocket-propelled grenade|RPG. Mike reveals Tracys election fraud, overturning the election results and making Al the election winner, while Tracy is ousted for fraud (especially after her long-suffering assistant, Neuschwander (Bruce McGill), drunkenly admits that she made him and her other associates go along with the fraud).

Three months later, Steve is Als new assistant, Mike has his job running the recreation center back, and Al has decreased crime rates in Washington. As Al and Steve get into a jet to go to a meeting, Mikes jacket gets caught in the jets door and he is trapped outside the plane while it takes off.

==Cast==
 
*Chris Farley as Mike Donnelly
*David Spade as Steve Dodds
*Tim Matheson as Al Donnelly
*Christine Ebersole as Governor Evelyn Tracy
*Gary Busey as Drake Sabitch
*Grant Heslov as Robbie Mieghem
*Timothy Carhart as Roger Kovary
*Bruce McGill as Neuschwander
*Boyd Banks as Clyde Spinoza
*David St. James as Motorcycle Cop
*Skip OBrien as State Trooper Chris Owen as Hal
*Mudhoney as Themselves Fred Wolf as Ronald Forte
*Julie Benz as Woman
 

==Production== Tommy Boy and was now looking to cash in on the same comedy formula.    At the time, Michaels had just finished contentious battles with the studio over the script of Waynes World 2, and the animosity between the two camps spilled over into Farleys contract with Paramount.  Although his agent lined up possible roles for the actor in The Cable Guy (for which he was offered $3 million)  and Kingpin (1996 film)|Kingpin, the movie studio remained firm on wanting another buddy comedy with Farley and Spade. 

The film was written by Fred Wolf, who claimed the studio told him to "deliver a finished script by midnight on Sunday, the last day Chris was contractually allowed to get out of the movie. If I didnt have a finished script -- any finished script -- they were going to sue me."    Wolf wrote 45 pages within only a few days, and dropped the script off at Paramount 15 minutes before his deadline.  After reading the script, Farley said that he "wasnt crazy" about it, and only agreed to do the film after coaxing from David Spade. 

Spheeris had notable disagreements with writer Fred Wolf and David Spade throughout the entire production of the film. Spheeris fired Wolf from the film three times (he was hired back twice by Farley and once by Lorne Michaels), then refused to speak to him and finally banned him from the set.     Her relationship with Spade was equally as tumultuous. Speaking to Farleys official biographer, she said, "I dont think Ive ever even smiled at anything David Spades ever done... I still have a recording of a message David left on my answering machine. He said, Youve spent this whole movie trying to cut my comedy balls off."  Surprisingly, the two worked together again in the 1998 comedy Senseless.

The combination of bright lights on-set and working under sunlight while filming Black Sheep caused permanent damage to David Spades eyes. Spade says of his condition: "I have to wear a hat even indoors and flashes in particular freak me out. I even have to make them turn down the lights in the make-up trailers. Ive become such a pain in the butt with this light-sensitive thing, its a wonder they dont just shoot me!" 

==Reception== Tommy Boy. It has a 28% rating on Rotten Tomatoes.  Film critic Gene Siskel stated that Black Sheep was one of only three movies he ever walked out on in 26 years;    Siskel stated several times that he did not respect Farley and thought of him as a terrible actor, stating at one point "I hate Chris Farley, just rubs me the wrong way. I knew John Belushi, and hes no John Belushi."  Siskels colleague Roger Ebert also hated the film, calling it "not only one of the worst comedies Ive ever seen, but one of the least ambitious; it doesnt even feel like theyre trying to make a good movie".  However, a few weeks later, during their televised review of Happy Gilmore, Ebert tried to defend Farley, saying that he believed Siskel was too hard on him, and that he believes that with a good script, Farley could be good in a film. 

==References==
 

==External links==
 
* 
*  
* 

 
 

 
 
 
 
 
 
 
 
 
 
 