La porta del cielo
 
{{Infobox film
| name           = La porta del cielo
| image          = 
| caption        = 
| director       = Vittorio De Sica
| producer       = Corrado Conti di Senigallia Salvo DAngelo
| writer         = Vittorio De Sica
| starring       = Marina Berti
| music          = Enzo Masetti
| cinematography = Aldo Tonti
| editing        = Mario Bonotti
| distributor    = 
| released       =  
| runtime        = 88 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}

La porta del cielo is a 1945 Italian film directed by Vittorio De Sica.

Vittorio de Sica hired approximately 300 extras, whom were Jewish or simply being persecuted by the Nazi regime, because of their physical oddity. To avoid their deportation and latter execution, he prolonged the shooting of the film as long as he could, awaiting the arrival of the allied armies. 

The film won the OCIC Special Award at the 53rd Venice International Film Festival in 1996 for efforts to restore the film.      

==Plot==
This is the story of a pilgrimage train full of sick and maimed people that was headed to see "Nostra Signora de Lourdes" in Rome, Italy. 

==Cast==
* Marina Berti as La crocerossina
* Elettra Druscovich as Filomena, la governante
* Massimo Girotti as Il giovane cieco
* Roldano Lupi as Giovanni Brandacci, il pianista
* Carlo Ninchi as Laccompagnatore del cieco
* Elli Parvo as La signora provocante
* María Mercader as Maria (as Maria Mercader)
* Cristiano Cristiani as Claudio Gorini, il bambino paralizzato
* Giovanni Grasso as Il commerciante paralitico
* Giuseppe Forcina as Lingegniere
* Enrico Ribulsi as Uno dei nepoti del commerciante
* Amelia Bissi as La signora Enrichetta
* Annibale Betrone as Il medico del treno bianco
* Tilde Teldi as La contessa crocerossina
* Pina Piovani as La zia del piccolo Claudio Giulio Alfieri as Un signore anziano
* Giulio Calì as Il napoletano curioso

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 