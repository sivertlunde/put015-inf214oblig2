The Master Mind
{{Infobox film
| name           = The Master Mind
| image          = Themastermind-1914-newspaperad.jpg
| image_size     =
| caption        = Newspaper advertisement. 
| director       = Oscar Apfel Cecil B. DeMille (uncredited)
| writer         = Clara Beranger
| based on       =  
| starring       = Edmund Breese
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        =
| country        = United States Silent English intertitles
}}
 1914 American silent crime film|crime/drama film directed by Oscar Apfel and Cecil B. DeMille and stars Edmund Breese. The film is based on the play of the same name by Daniel D. Carter. 

==Overview==
The plot revolves around a defense attorney who, unable to obtain the acquittal of an innocent young man, concocts a complicated and diabolical scheme to revenge himself upon the prosecutor.

==Cast==
*Edmund Breese as Richard Allen
*Fred Montague as Henry Allen
*Jane Darwell as Milwaukee Sadie
*Dick La Reno as Blount Harry Fisher as Diamond Willie
*Mabel Van Buren as Lucine, Three-Arm Fanny Richard La Strang as Safe Blower 
*Monroe Salisbury as District attorney Billy Elmer as Creegan

==See also==
* List of American films of 1914

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 


 