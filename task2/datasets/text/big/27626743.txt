Jalopy (film)
{{Infobox film
| name           = Jalopy
| image_size     =
| image	         = Jalopy FilmPoster.jpeg
| caption        =
| director       = William Beaudine
| producer       = Ben Schwalb Tim Ryan Edmond Seward Jack Crutcher
| narrator       =
| starring       = Leo Gorcey Huntz Hall David Gorcey Bernard Gorcey
| music          = Marlin Skiles
| cinematography = Harry Neumann William Austin Allied Artists
| distributor    = Allied Artists
| released       =  
| runtime        = 62 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 Allied Artists and is the twenty-ninth film in the series.

==Plot==
Sach convinces Louie to rent the back room of the sweet shop to a professor.  Meanwhile Slip is preoccupied with entering their car in an auto race to raise enough money to help Louie pay his bills.  They dont have any luck with the car until Sach invents a formula that makes the car go faster.  A crooked gambler tries to steal the formula from them, with no luck.  Slip enters into another race, but has to start the race without the formula in the car as Sach was making another batch.  When Sach reaches him in the middle of the race and puts the formula in the gas tank the cars begins to accelerate...only in reverse!  Sach and Slip then continue the race in reverse and wind up winning.  Sach then realizes why the formula didnt work as it did before...the seltzer that he added to the formula was flat!

==Cast==

===The Bowery Boys===
*Leo Gorcey as Terrance Aloysius Slip Mahoney
*Huntz Hall as Horace Debussy Sach Jones
*David Gorcey as Chuck (Credited as David Condon)
*Bennie Bartlett as Butch

===Remaining cast===
*Bernard Gorcey as Louie Dumbrowski  Robert Lowery as Skid Wilson
*Jane Easton as Bobbie Lane
*Leon Belasco as Prof. Bosgood Elrod

==Production== Ascot Park Allied Artists in the same year.   It was also the first Bowery Boys film released by Allied Artists, which was the new name of Monogram Pictures, the same company that all their previous films were released under.

==Home media== Warner Archives released the film on made to order DVD in the United States as part of "The Bowery Boys, Volume Three" on October 1, 2013.

==References==
 

==External links==
* 

 
{{succession box
| title=The Bowery Boys movies
| years=1946-1958 No Holds Barred 1952
| after=Loose in London 1953}}
 

 
 

 
 
 
 
 