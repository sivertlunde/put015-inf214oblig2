The Romance of Old Bill
{{Infobox film
| name           = The Romance of Old Bill
| image          =
| caption        = George Pearson
| producer       = 
| writer         = Bruce Bairnsfather (play) Arthur Eliot (play) T.A. Welsh George Pearson
| starring       = Charles Rock Arthur Cleave Hugh E. Wright
| cinematography =
| editing        =
| studio         = Welsh-Pearson
| distributor    = Jury Films
| released       =   
| runtime        =
| country        = United Kingdom
| awards         =
| language       = Silent English intertitles
| budget         =
}} silent comedy comedy war war directed George Pearson and starring  Charles Rock, Arthur Cleave and Hugh E. Wright. It was made at Twickenham Studios. It is based on the play The Better Ole, with the setting updated to the First World War. 

==Cast==
* Charles Rock as Old Bill  
* Arthur Cleave as Bert  
* Hugh E. Wright as Alf  
* Mary Dibley as Maggie Busby 
* Hayford Hobbs as Jim 
* Lillian Hall-Davis as Lil  
* Alfred Phillips as Spy  
* Michelin Potous as Victoire  
* Marguerite Blanche as Suzette  
* J.M. Wright as Singer  
* Sid Jay as Juggler 
* Mansell Fane as Grouser  
* Frank Adair as Colonel  
* Meggie Albanesi as Waitress 
* Mercy Hatton 
* Micrommelynck as Colonel  
* Fernand Leone as Peasant  

==References==
 

==Bibliography==
* Goble, Alan. The Complete Index to Literary Sources in Film. Walter de Gruyter, 1999.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 

 