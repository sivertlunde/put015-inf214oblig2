Sahara (1983 film)
:For other films with the same name, see Sahara_(disambiguation)#Film_and_television|Sahara.
{{Infobox film
| name = Sahara
| image = sahara(1983).jpg
| image_size = 215px
| alt = 
| caption = Theatrical poster by Drew Struzan
| director = Andrew McLaglen
| producer = Menahem Golan Yoram Globus
| writer = James R. Silke
| story = Menahem Golan
| starring = {{Plainlist|
* Brooke Shields
* Lambert Wilson
* Horst Buchholz
* John Rhys-Davies
* Ronald Lacey
* John Mills
}}
| music = Ennio Morricone
| cinematography = David Gurfinkel Armando Nannuzzi
| editing = Michael John Bateman Michael J. Duthie Cannon Group
| distributor = Metro-Goldwyn-Mayer
| released =  
| runtime = 111 minutes  
| country = United States United Kingdom
| language = English
| budget = $25 million 
| gross = $1,402,962 
}} adventure drama film directed by Andrew McLaglen and starring Brooke Shields, Lambert Wilson, Horst Buchholz, John Rhys-Davies, and John Mills. The original music score was composed by Ennio Morricone.

The films tagline is "She challenged the desert, its men, their passions and ignited a bold adventure."

==Plot==
The setting is in the year 1927. After her father dies, a young American heiress, Dale disguises herself as a man and takes the place of her father in an international car race through the Sahara. Dale is taken prisoner by Rasoul, but is rescued by Jaffar. However, more trouble awaits her before she can finish the race. Dale falls in love with Jaffar and marries him. Rasoul ends up dying in the end. She wins the race, becoming the first woman to win this international car race.

==Cast==
* Brooke Shields as Dale
* Lambert Wilson as Jaffar
* Horst Buchholz as Von Glessing
* John Rhys-Davies as Rasoul
* John Mills as Cambridge
* Ronald Lacey as Beg
* Cliff Potts as String
* Perry Lang as Andy
* Terrence Hardiman as Brownie Steve Forrest as Gordon
* Tuvia Tavi as Bertocelli

==Production==
Sahara was partially filmed in Israel.  

==Reception==
At an alleged $25 million budget, Sahara was a massive box office bomb, grossing only $1,402,962 in the domestic box office.   

==Accolades== 1984 Razzies, Worst Actress Worst Supporting Actor as "Brooke Shields (with a moustache)", making her the first and only actress to win this award. 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 