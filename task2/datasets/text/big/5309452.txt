Stuck on You!
 
{{Infobox Film
| name           = Stuck on You!
| image          = TromaStuckOnYou.jpg
| image_size     = 
| caption        = The DVD cover for Stuck on You! Michael Herz Lloyd Kaufman
| producer       = Michael Herz Lloyd Kaufman Stuart Strutin 
| writer         = Jeffrey Delman Tony Gittleson Michael Herz Lloyd Kaufman Darren Kloomok Warren Leight Duffy Caesar Magesis Melanie Mintz Don Perman Stuart Strutin 
| starring       = Irwin Corey Virginia Penta Mark Mikulski Albert Pia Norma Pratt
| music          = 
| cinematography = Lloyd Kaufman
| editing        = Richard W. Haines Darren Kloomok Ralph Rosenblum 
| distributor    = Troma Entertainment
| released       = 1982
| runtime        = 90 minutes English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 Michael Herz of Troma Entertainment. Stuck on You! was the third in a series of four "sexy comedies" that helped establish Troma, beginning with 1979s smash hit Squeeze Play!, 1981s Waitress!, and followed by 1983s The First Turn-On!.

The film, supposedly inspired by the writings of Tom Lehrer and Stan Freberg, follows 
estranged couple Bill and Carol, who are in a palimony suit against each other.  The zany Judge Gabriel (played by Professor Irwin Corey) is handling their suit. As Bill and Carol relate their problems to Gabriel, he demonstrates how all lovers from the beginning of time, including Adam and Eve, Queen Isabella and Christopher Columbus, and King Arthur and Lady Guinevere have all faced their exact same troubles.  The judge is finally revealed to be the angel Gabriel, sent down in hopes of bringing the couple back together. 

Lloyd Kaufman has stated that Stuck on You! is his favorite of Tromas "sexy comedies".

Tagline: Its Boys, Its Girls, Its Crazy...Its A Stuckin Good Time!!!

==External links==
* 
* 

 
 
 
 
 
 

 

 