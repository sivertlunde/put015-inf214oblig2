The Black Power Mixtape 1967-1975
{{Infobox film
| name        = The Black Power Mixtape 1967-1975
| image       = 
| caption     = 
| alt         = 
| director    = Göran Olsson
| executive producer = Tobias Janson 
| producer    = Annika Rogell
| studio      = 
| writer      = 
| editing     = Göran Olsson & Hanna Lejonqvist
| starring    = Stokely Carmichael, Dr. Martin Luther King, Jr., Eldridge Cleaver, Bobby Seale, Huey P. Newton, Emile de Antonio, Angela Davis
| distributor = 
| released    =  
| runtime     = 100 minutes
| country     = Sweden
| language    = Swedish
| music       = Corey Smyth - Music Producer 
| budget      = 
}}
The Black Power Mixtape 1967-1975 is a 2011 documentary film, directed by Göran Olsson, that examines the evolution of the Black Power Movement in American society from 1967 to 1975. It features the found footage shot by a group of Swedish journalists (discovered some 30 years later in the cellar of Swedish Television) overlaid with commentaries and interviews from leading contemporary African-American artists, activists, musicians and scholars. 
The footage includes appearances by Stokely Carmichael, Dr. Martin Luther King, Jr., Eldridge Cleaver, Bobby Seale, Huey P. Newton, Emile de Antonio, Angela Davis and commentaries by Erykah Badu, Talib Kweli, Harry Belafonte, Kathleen Cleaver, Angela Davis, Robin Kelley and Abiodun Oyewole, amongst others.

==References==
 

==External links==

 
 
 

 
 