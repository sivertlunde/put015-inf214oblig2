Bokya Satbande
{{Infobox film
| name           = Bokya Satbande
| image          = Bokya satbande film.jpg
| caption        = DVD poster
| director       = Raj Pendurkar
| producer       = Kanchan Satpute
| writer         = 
| based on       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         =  
| distributor    =  
| released       =     
| runtime        = 
| country        = India
| language       = Marathi
| budget         =  
| gross          = 
}}
 Marathi film directed by Raj Pendurkar and produced by Kanchan Satpute of Checkmate (2008 film)|Checkmate fame. The film is based on the book series of the same name written by Dilip Prabhavalkar. The film chronicles the adventures of a young ten-year-old boy called Bokya.  

Bokya is played by Aryan Narvekar, son of actor Sanjay Narvekar.  The music is composed by Shailendra Barve who has also composed music for Taare Zameen Par and lyrics are written by Jitendra Joshi. The songs are sung by Avadhoot Gupte and Suresh Wadkar.

==Cast==
* Aryan Narvekar as Bokya/Chinmayanand Satbande
* Dilip Prabhawalkar as Mr. Bhilwandi
* Jyoti Subhash 
* Vijay Kenkre 
* Shubhangi Gokhale
* Chitra Navathe 
* Madhavi Juvekar 
* Alok Rajwade 
* Nisha Satpute 
* Anjali Bhagwat in guest appearance

==Music==
The songs featured in the film are as enlisted below.
* "Bokya Satbande" - Singer: Avdhoot Gupte 
* "Man Aakashache" - Singer: Suresh Wadkar 
* "Thodi Style Pahije" -Singer: Avdhoot Gupte 

==References==
 

 
 
 

 