Daata
{{Infobox film
 | name = Daata
 | image = Daatafilm.jpg
 | caption = VCD Cover Sultan Ahmed Sultan Ahmed
 | banner = Sultaan Productions
 | writer = 
 | dialogue =  Pran Amrish Puri Deepak Parashar Ranjeet Saeed Jaffrey Bharat Bhushan
 | music = Kalyanji Anandji
 | lyrics = 
 | cinematographer = 
 | associate director = 
 | art director = 
 | choreographer = 
 | released =   1989 (India)
 | runtime = 135 min.
 | language = Hindi Rs 4 Crores
 | preceded_by = 
 | followed_by = 
 }}
 1989 Hindi Indian feature directed by Sultan Ahmed, starring Mithun Chakraborty, Shammi Kapoor, Padmini Kolhapure, Suresh Oberoi, Supriya Pathak, Pran (actor)|Pran, Amrish Puri, Ranjeet, Deepak Parashar, Shafi Inamdar, Saeed Jaffrey and Bharat Bhushan. This movie was AVERAGE in big cities but in small cities and in north india It was a big HIT. "DAATA" remains for Mithun Chakrabortys one of the best performmece till date. The movie has one of the best bidaai song sung by Kishore Kumar "Baabul Ka Yeh Ghar"

==Plot==

Daata is an action film with  Mithun Chakraborty and Padmini Kolhapure playing the lead roles, supported by Shammi Kapoor, Supriya Pathak, Pran (actor)|Pran, Prem Chopra, Deepak Parashar and Amrish Puri.
Dinanath is a school-teacher in a small town in India, and lives with his wife, Kamla; daughter, Shanti; and son Kundan. He has published a book called "Daata" in which he has made reference to all major religions of the world, and is honored for this contribution by none other than the President of India through Education Minister, Raja Suraj Singh. He arranges the marriage of Shanti to the son of Gopaldas, when Gopaldas demands dowry he is unable to afford this, and states that he has given sufficient gold and jewelery to Shanti. The gold turns out to be fake, as a result the marriage is canceled, Shanti kills herself, Dinanath dies of a heart attack. His name tarnished, no one comes forward to help Kundan or his mother. Kundan decides to avenge his fathers death by killing Gopaldas son, as a result he becomes a bandit, joins a band of other bandits and is on the look-out to kill Gopaldas. The question remains is this what Dinanath had foreseen for his son - a life of a bandit, and death at the hands of the police?

==Cast==

*Mithun Chakraborty...... Kundan
*Shammi Kapoor...... Sheroo
*Padmini Kolhapure...... 
*Suresh Oberoi...... Ramzan Khana
*Supriya Pathak...... Suraiya
*Pran
*Amrish Puri...... Gopal Das
*Prem Chopra...... Lala Nagi
*Ranjeet...... Natwar
*Saeed Jaffrey...... Master Dinanath
*Deepak Parashar...... Rao
*Shoma Anand...... Alka
*Arjun
*Bharat Bhushan
*Birbal
*Anil Dhawan
*Satyen Kappu
*Raj Mehra
*Leela Mishra...... Jumna 
*Mac Mohan
*Jamuna
*Mukri...... Pandit Ram Prasad
*Joyoshree Arora

==References==
* http://www.bollywoodhungama.com/movies/cast/5262/index.html
* http://ibosnetwork.com/asp/filmbodetails.asp?id=Daata -

== Songs==

{Babul Ka Yeh Ghar - Kishore Kumar}

 
 
 
 
 
 


 
 