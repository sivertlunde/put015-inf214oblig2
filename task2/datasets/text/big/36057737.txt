The Village Rogue
{{Infobox film
| name           = The Village Rogue
| image          = 
| image_size     = 
| caption        = 
| director       = Miklós Pásztory 
| producer       = 
| writer         = Ede Tóth (play)   Miklós Pásztory   Ladislaus Vajda  
| narrator       = 
| starring       = Helene von Bolvary   Dezsö Kertész   Péter Andorffy
| music          = 
| editing        = 
| cinematography = Béla Zsitkovszky
| studio         = Astra Filmgyár
| distributor    = 69 minutes
| released       = 16 February 1916
| runtime        = 
| country        = Hungary Silent  Hungarian intertitles
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} Hungarian silent silent drama film directed by Miklós Pásztory and starring Helene von Bolvary, Dezsö Kertész and Péter Andorffy. It was based on an 1875 play by Ede Tóth. An alternative translation of its title is Village Rascal.

==Cast==
* Helene von Bolvary - Finum Rózsi 
* Dezsö Kertész - Lajos - Feledi fia 
* Péter Andorffy - Gonosz Pista 
* Ida Andorffy - Boriska 
* Amália Jákó - Bátky Tercsa 
* Mari K. Demjén - Gonosz Pista felesége 
* József Kürthy - Feledi báró 
* Jenõ Medgyaszay - Göndör Sándor 
* Lajos Szõke - Jóska béres 
* Sándor Szõke - Cigányprímás 

==Bibliography==
* Cunningham, John. Hungarian Cinema: From Coffee House to Multiplex. Wallflower Press, 2004.

==External links==
* 

 

 
 
 
 
 
 
 