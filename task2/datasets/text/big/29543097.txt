We're Rich Again
{{Infobox film
| name           = Were Rich Again
| image          =
| caption        =
| director       = William A. Seiter
| producer       =
| writer         = Alden Nash (play) Ray Harris
| narrator       =
| starring       = Edna May Oliver Billie Burke Marian Nixon
| music          =
| cinematography =
| editing        =
| studio         = RKO Radio Pictures
| distributor    = RKO Radio Pictures
| released       =  
| runtime        = 71 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Were Rich Again is a 1934 comedy film starring Edna May Oliver, Billie Burke and Marian Nixon. A formerly wealthy family tries to stave off bankruptcy until one of them can marry a rich man. It is based on the play And Who Will Be Clever by Alden Nash.

==Cast==
*Edna May Oliver as Maude Stanley
*Billie Burke as Linda Page
*Marian Nixon as Arabella Sykes (as Marion Nixon) Reginald Denny as Bookington "Bookie" Wells
*Joan Marsh as Carolyn "Carrie" Page
*Buster Crabbe as Erasmus Rockwell "Erp" Pennington (as Larry "Buster" Crabbe) Grant Mitchell as Wilbur Page
*Gloria Shea as Victoria "Vic" Page
*Edgar Kennedy as Healy
*Otto Yamaoka as Fugi
*Lenita Lane as Charmion
*Dick Elliott as Fred Green
*Andrés de Segurola as Jose

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 


 