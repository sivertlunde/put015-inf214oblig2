Close to My Heart
{{Infobox film
| name           = Close to My Heart
| image_size     = 
| image	=	Close to My Heart FilmPoster.jpeg
| caption        = 
| director       = William Keighley
| producer       = William Jacobs
| writer         = James R. Webb 
| narrator       = 
| starring       = Gene Tierney Ray Milland
| music          = Max Steiner
| cinematography = Robert Burks
| editing        = Clarence Kolster
| distributor    = Warner Bros.
| released       = 1951
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Close to My Heart is a 1951 Warner Bros. drama directed by William Keighley, written by James R. Webb (based on his novel A Baby for Midge), and starring Ray Milland and Gene Tierney.  

==Plot== adopt and inquire at an adoption agency.  Learning of an abandoned child left at the police station, Midge is determined to have the baby as her own, but Brad refuses to go along with Midges plans until he can find out something about the childs parents, which then makes the boy an adoption risk.

==Production==
Ray Milland and Gene Tierney were loaned out to star in this Warner Bros. film.  

Gene Tierney was still trying to cope with the personal tragedy of giving birth to a severely retarded daughter. 

==Cast==
*Ray Milland - Brad Sheridan
*Gene Tierney - Midge Sheridan
*Fay Bainter - Mrs. Morrow
*Howard St. John - E.O. Frost

==Notes==
 

==External links==
* 
* 

 

 
 
 
 
 
 


 