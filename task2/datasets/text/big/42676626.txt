Innocent Thing
{{Infobox film name           = Innocent Thing image          = Innocent Thing poster.jpg caption        = Theatrical poster director  Kim Tae-kyun producer       = Kim Tae-kyun  writer         = Lee Seong-min starring       = Jang Hyuk   Jo Bo-ah music          = Kim Tae-seong   Jung Ji-hoon cinematography = Jeong Han-cheol editing        = Choi Jae-geun   Kim So-yeon studio         = Novus Mediacorp   Invent Stone  distributor    = Invent D released       =   runtime        = 117 minutes country        = South Korea language       = Korean
}} Kim Tae-kyun, starring Jang Hyuk and Jo Bo-ah.   

==Plot== rugby athlete and is currently a popular physical education teacher at an all-girls high school. His pregnant wife Seo-yeon (Sunwoo Sun) is due to give birth to their first child imminently. Although hes used to playful advances from pubescent students, things take a dangerous turn when Young-eun (Jo Bo-ah) falls hard for her teacher and daringly confesses her feelings for him. Amid growing rumors, he is soon consumed with guilt and attempts to end the relationship. However Young-euns pure love slowly turns to obsession. She starts to regard everyone related with Joon-ki as obstacles, and the farther he keeps her away, the bolder her obsession and madness grows.  

==Cast==
 
*Jang Hyuk as Kim Joon-ki
*Jo Bo-ah as Ha Young-eun
*Sunwoo Sun as Seo-yeon
*Lee Do-ah as Min-joo
*Do Gwang-won as Seok-jin
*Jang Seo-kyeong as Joo-hee
*Shin Cheol-jin as school custodian
*Park Jin-young as Jang-in, Joon-kis father-in-law
*Do Yong-gu as gynecologist
*Wang Tae-eon as principal
*Kim Kyeong-sik as Teacher Park
*Han Hee-jung as housekeeper
*Son Hee-seon as Joon-kis mother-in-law
*Yoo Ji-yeon as Cheo-hyeong, Joon-kis sister-in-law 
*Kim Won-sik as Dong-seo 
*Yoon Bok-in as OB/Gyn nurse
*Baek Eun-joo as student council president
*Kim Ye-eun as Ye-eun 
*Han Seul-gi as Seul-gi 
*Lee Sang-mi as Sang-mi 
*Park Jeong-hee as Jeong-hee 
*Lee Sae-bom as Sae-bom 
*Woo Do-im as Do-im 
*Park Gyu-ri as Gyu-ri 
*Kang Do-eun as Joo-hees mother
*Jeong Joon-yeong as Teacher Jeong 
*Choi Won-seok as Won-seok 
*Heo Ji-hye as new ward nurse 
*Kim Mi-kyeong as neonatal nurse 
*Oh Ui-taek as male student 
*Lee Yeon-joon as "kiss" man
*Park Joon-seo as man in TV drama
*Kim Bo-ryeong as woman in TV drama
 

==Production== Kim Tae-kyun and actor Jang Hyuk for the first time in thirteen years since Volcano High (2001).   Kim first encountered the screenplay in 2007 at a competition, and he was won over right away, captivated by the blurred lines between love and obsession. He called the film "an experiment" due to its eclectic mix of suspense film|suspense, horror film|horror, melodrama and romance film|romance, and that his use of cinematography and camera techniques were essential in delivering the "feeling of being trapped." Wanting the schoolgirl character to embody both innocence and pure sex appeal, Kim auditioned 250 actresses, before casting Jo Bo-ah in her big screen debut.  

The early working title was Strawberry Milk ( ), a drink which Jos character has a fixation on. It was changed to Thorn because Kim said, "There is nothing sweet and soft about the movie like strawberry flavored milk. Only sharpness like a thorn exists. It represents something difficult." 

Filming began on September 14, 2013, at Jijok Middle School in Daejeon.  Shooting wrapped on November 10, 2013.

==Box office==
Innocent Thing was released on April 10, 2014. It drew 121,292 viewers on its opening day.  

==References==
 

==External links==
*   
* 
* 
* 

 

 
 
 
 
 
 
 
 