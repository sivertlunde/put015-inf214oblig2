About a Boy
 
 
 
{{Infobox film
| name = About a Boy
| image = about_a_boy_movie_poster.jpg
| image_size =
| alt =
| caption = Theatrical release poster Paul Weitz
| producer = Jane Rosenthal Robert De Niro Brad Epstein Tim Bevan Eric Fellner
| screenplay = Peter Hedges Chris Weitz Paul Weitz About a Boy by Nick Hornby
| narrated by = Nicholas Hoult
| starring = Hugh Grant Nicholas Hoult Toni Collette Rachel Weisz
| music = Badly Drawn Boy
| cinematography = Remi Adefarasin Nick Moore
| studio = StudioCanal TriBeCa Productions Working Title Films Universal Pictures
| released = 26 April 2002
| runtime = 105 minutes
| country = United Kingdom United States France
| language = English
| budget = $30 million
| gross = $130,549,455   
}} Paul Weitz. adaptation of novel of the same name by Nick Hornby. The film stars Hugh Grant, Nicholas Hoult, Toni Collette, and Rachel Weisz. The film at times uses double voice-over narration, when the audience hears both Wills and Marcuss thoughts.
 Best Adapted BAFTA Award respectively for their performances.

==Plot==
Will Freeman  (Hugh Grant) lives a serene and luxurious lifestyle devoid of responsibility in London thanks to substantial royalties left to him from a successful Christmas song composed by his father. Will begins attending a support group for single parents as a way to meet women and as part of his ploy, invents a two-year-old son named Ned. His plan succeeds and he meets Suzie (Victoria Smurfit).  Will brings Suzie on a picnic where he meets Marcus (Nicholas Hoult), the 12-year-old son of Suzies friend, Fiona (Toni Collette). Will gains Marcus interest and trust after he lies to a park ranger to cover up for Marcus killing a duck. Afterward, when Will and Suzie take Marcus home, they find Fiona in the living room, overdosed on pills in a suicide attempt. 

Marcus attempts to fix Will up with his mother in order to cheer her up, but the plan fails after a single date.  Instead, Marcus becomes close to Will after blackmailing him with the knowledge that "Ned" doesnt exist, and begins to treat him as a surrogate big brother. Marcus influence leads Will to mature and he seeks out a relationship with Rachel (Rachel Weisz), a self-assured career woman, bonding over their experiences raising teenaged sons, though Will neglects to explain his relationship to Marcus. Marcus, in turn, becomes infatuated with Ellie (Natalia Tena) but gives up his romantic interest in favor of a close platonic friendship. Will, realizing that he desires true intimacy with Rachel, decides to be honest with her about his relationship with Marcus, but this backfires and their relationship ends.

One day, Marcus comes home from school to find his mother crying in the living room. Marcus attempts to unburden himself to Will, but Will is withdrawn following his break-up.  Marcus decides to sing at a school talent show in order to make his mother happy.  Will attempts to return to his previous lifestyle, but finds it unfulfilling and decides to help Marcus.  He crashes a meeting of the single parents support group to find Fiona and beg her not to commit suicide. She assures him that she has no plans to do so in the immediate future and reveals that Marcus has decided to sing at the school show that day.

Will realizes this will be a huge embarrassment for Marcus and rushes to the school to stop him, but Marcus is steadfast in his decision to perform, believing it will be the only thing that will make his mother happy. Marcus steps on stage and sings his mothers favorite song - "Killing Me Softly with His Song" as the student body taunts him. Suddenly, Will appears onstage with a guitar to accompany Marcus for the rest of the song, turning himself into the butt of the joke and rescuing Marcus from humiliation.

The following Christmas, Will hosts a celebration at his place with his new extended family. The idea of Will marrying Rachel is brought up and Marcus seems unenthusiastic.  But Marcus reveals in voiceover that hes not against Will and Rachel marrying, merely that he believes that couples dont work on their own and that everyone needs an extended support system like he now has, concluding "No man is an island." 

==Cast==
* Hugh Grant as Will Freeman
* Nicholas Hoult as Marcus Brewer
* Toni Collette as Fiona Brewer
* Rachel Weisz as Rachel
* Natalia Tena as Ellie
* Sharon Small as Christine
* Nicholas Hutchinson as John
* Victoria Smurfit as Suzie
* Isabel Brook as Angie
* Ben Ridgeway as Lee, the bully
* Jenny Galloway as Frances / SPAT
* Augustus Prew as Ali
* Tim Rice (uncredited archive footage) as Himself

==Soundtrack==
 
The soundtrack was released on 23 April 2002, composed by singer/songwriter Badly Drawn Boy.

;Track listing
# "Exit Stage Right"
# "A Peak You Reach"
# "Something to Talk About"
# "Dead Duck"
# "Above You, Below Me"
# "I love NYE"
# "Silent Sigh"
# "Wet, Wet, Wet"
# "River, Sea, Ocean"
# "S.P.A.T."
# "Rachels Flat"
# "Walking Out of Stride"
# "File Me Away"
# "A Minor Incident"
# "Delta (Little Boy Blues)"
# "Donna and Blitzen"

==Reception== ten best movies of the year.

The film received a B+ CinemaScore from American audiences. 

Almost universally praised, with an Academy Award-nominated screenplay, About a Boy was determined by the Washington Post to be "that rare romantic comedy that dares to choose messiness over closure, prickly independence over fetishised coupledom, and honesty over typical Hollywood endings."  Rolling Stone wrote, "The acid comedy of Grants performance carries the film   gives this pleasing heartbreaker the touch of gravity it needs," {{Cite news | last = Peter | first = Travers | author-link = | title = Reviews: About A Boy
 | newspaper = Rolling Stone | date = 6 June 2002 |accessdate=29 September 2007 |url=http://www.rollingstone.com/reviews/movie/5947699/review/5947700/about_a_boy | publisher =  , About a Boy was a more modest box office grosser than other successful Grant films, making all of $129&nbsp;million globally. The film earned Grant his third Golden-Globe nomination, while the  s Owen Gleiberman took note of Grants maturation in his review, saying he looked noticeably older and that it "looked good on him."  He added that Grants "pillowy cheeks are flatter and a bit drawn, and the eyes that used to peer with love me cuteness now betray a sharks casual cunning. Everything about him is leaner and spikier (including his hair, which has been shorn and moussed into a Eurochic bed-head mess), but its not just his surface thats more virile; the nervousness is gone, too. Hugh Grant has grown up, holding on to his lightness and witty cynicism but losing the stuttering sherry-club mannerisms that were once his signature. In doing so, he has blossomed into the rare actor who can play a silver-tongued sleaze with a hidden inner decency."   

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

 