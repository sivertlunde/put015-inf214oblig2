Gentlemen Broncos
{{Infobox film
| name           = Gentlemen Broncos
| image          = Gentlemen broncos.jpg
| image_size     = 215px
| alt            =
| caption        = Promotional poster Jared Hess Mike White John J. Kelly
| writer         = Jared and Jerusha Hess
| starring       = Michael Angarano Jemaine Clement Jennifer Coolidge Sam Rockwell
| music          = David Wingo
| cinematography = Munn Powell
| editing        = Yuka Ruell HH Films
| distributor    = Fox Searchlight Pictures
| released       =  
| runtime        = 89 minutes  
| country        = United States
| language       = English
| budget         = $10 million 
| gross          = $118,192   
}}
Gentlemen Broncos is a 2009 comedy film written by Jared and Jerusha Hess and directed by Jared Hess. The film stars Michael Angarano, Jemaine Clement, Jennifer Coolidge, and Sam Rockwell.

==Plot==
Benjamin Purvis lives with his mother Judith, who designs tacky clothes and makes rock hard popcorn balls. Judith and Benjamin make ends meet by working at a womens retail clothing store. Benjamin spends his spare time writing science fiction stories, and he has recently completed a story called Yeast Lords, which centers on a hero named Bronco, modeled after his long-dead father.

At various times, portions of Yeast Lords are seen as Benjamin imagines them. Bronco is obliquely masculine, and he valiantly struggles with a villain over yeast production.

At a two-day writing camp for aspiring fantasy and science fiction authors, Benjamin attends lectures by his idol, the prolific and pretentious writer Ronald Chevalier. Chevalier announces a contest for the writers, in which the winners story will be published nationally. After encouragement from fellow camper Tabatha, Benjamin submits Yeast Lords. Tabatha shows the story to her friend Lonnie Donaho who runs an ultra low-budget video production company. Lonnie gives Benjamin a post-dated check for $500 and begins adapting Yeast Lords into a film.

As Chevalier reviews the stories from the campers, he gets a call from his publisher, rejecting his latest manuscript. Panicked, he picks up Benjamins story, and it sparks his imagination. Chevalier changes Purvis Bronco into Brutus, an extremely effeminate and comically flamboyant hero, changes the other character names and title, but otherwise leaves the story intact. His publisher loves it, and the novel is rushed into production under the title, Brutus and Balzaak.

Portions of Chevaliers version are now seen playing out alongside Benjamins original vision of the story.

At the local premiere of Donahos version of Yeast Lords, Benjamin is nauseated to see how badly Donaho has adapted his work, and he abruptly leaves the film with Tabatha. They go to a bookstore where he discovers Chevaliers plagiarism after reading a paragraph from Brutus and Balzaak. He confronts Chevalier at a local book signing, and assaults him with some merchandise Chevalier had offered him in exchange for keeping his theft quiet. Two policeman hustle him out of the store and he is placed in jail.

Judith comes to visit her son in jail to give him his birthday present. She hands him a box of manuscripts, all officially bound by the Writers Guild of America. She explains that she has been registering all his stories with them since he was seven years old, thinking they would make a nice keepsake for his children. Yeast Lords is one of the registered stories.

Copies of Chevaliers novel are unceremoniously dumped from store shelves, being replaced with Benjamins original novel.

==Cast==
* Michael Angarano as Benjamin Purvis
* Jemaine Clement as Dr. Ronald Chevalier
* Jennifer Coolidge as Judith Purvis
* Halley Feiffer as Tabatha Jenkins
* Héctor Jiménez as Lonnie Donaho
* Josh Pais as Todd Keefe Mike White as Dusty

==="Yeast Lords" by Purvis/"Brutus and Balzaak" by Chevalier===
* Sam Rockwell as Bronco/Brutus
* Suzanne May as Vanaya/Venonka
* Edgar Oliver as Duncan/Lord Daysius

==Production== Mike White, who co-wrote Nacho Libre with them, to produce. Filming began in March 2008 in Utah.    Much of the film was shot in Tooele, UT. In early August 2008, work on an Internet viral marketing began, which had a video introducing the character of Ronald Chevalier.  A second video was released in October 2008.  A trailer was released on August 19, 2009. Some of the artwork in the opening credits is by fantasy and science fiction artist David Lee Anderson.

==Release==
Gentlemen Broncos was intended to be released theatrically on October 30, 2009, but due to poor reviews the national release was pulled from theaters.

==Reception==
 
The film received mostly negative reviews and holds an 18% rating on Rotten Tomatoes based on 78 reviews by critics.  Metacritic gave it a generally unfavorable 28 out of 100 based on 21 reviews.  Roger Ebert gave the film 2 out of 4 stars, writing that while "Hess invents good characters" they quickly become lost in a disjointed and meandering story. 

==Home media==
 
Gentlemen Broncos was released on DVD and Blu-ray Disc|Blu-ray on March 2, 2010. 

==Soundtrack==
* "In the Year 2525"
** Written by Rick Evans
** Performed by Zager & Evans

* "Act Naturally" Johnny Russell
** Performed by Buck Owens

* "Cedar Dreams", "Twilight", "First Flight", "Winds of Stillness"
** Written and performed by John Two-Hawks

* "What a Town"
** Written by Bobby Charles and Richard Claire Danko
** Performed by David Bromberg

* "John Sebastionss Girl"
** Written and performed by Shara Joyce and Rory ODonoghue

* "The Oh of Pleasure"
** Written and performed by Ray Lynch and Tom Canning

* "Beautiful Girl" Mark Stevens

* "Wind of Change"
** Written by Klaus Meine Scorpions

* "The New World Anthem"
** Written and performed by Jeremy Wall

* "Celtic Voyage"
** Written by Joel Bevan

* "Moment of Truth"
** Written by Dan Graham

* "Celestial Soda Pop", "Tiny Geometries"
** Written and performed by Ray Lynch

* "Just Like Jesse James"
** Written by Diane Warren and Desmond Child
** Performed by Cher

* "Don Carlos" Robert Miller

* "Carry on Wayward Son"
** Written by Kerry Livgren Kansas

* "Paranoid (Black Sabbath song)|Paranoid" John Osbourne, Terence Butler William Ward
** Performed by Black Sabbath

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 