One Rainy Afternoon
{{Infobox film
| name           = One Rainy Afternoon (Matinee Scandal)
| image          = One Rainy Afternoon dvd.jpg
| image_size     =
| caption        = DVD cover
| director       = Rowland V. Lee
| producer       = Jesse L. Lasky Mary Pickford
| writer         = Screen story:   Addl Dialogue: Maurice Hanline
| starring       = Francis Lederer Ida Lupino
| music          = Ralph Erwin (music) Preston Sturges  (lyrics)  Jack Stern (lyrics)
| cinematography = Merritt B. Gerstad J. Peverell Marley
| editing        = Margaret Clancey
| distributor    = United Artists
| released       = May 13, 1936 January 1948 (re-release)
| runtime        = 94 min. 80 min. (dvd)
| country        = United States
| language       = English
| budget         = $511,383 (est)
| gross          = $603,903 (world)}}
 Erik Rhodes. French film Monsieur Sans-Gêne by Emeric Pressburger and René Pujol, which was based on the story "The Satyr" by Pressburger. TCM     The film was reissued in 1948 as Matinee Scandal.

==Plot== Erik Rhodes), makes a public accusation against Phillip, and the priggish head of the Purity League (Eily Malyon) exploits the incident until it becomes a national scandal, with Phillipe dubbed "The Kissing Monster". When Phillipe is tried, his defense is that he was overcome by Moniques beauty, and that it is a Frenchmans nature to be romantic, even to perfect strangers. His punishment is to spend just three days in jail, but when he is released, he discovers that Monique has paid his fine, supposedly to avoid more publicity, but actually because she is secretly attracted to him.

Meanwhile, the tabloids have made Phillipe into a national hero, and instead of his producer, Maillot (Roland Young), firing the actor, he gets a raise. His new show will have him re-enact the kissing incident, but on the day of the opening Moniques father has him arrested, only be released when Yvonne, who turns out to be the wife of the Minister of Justice, convinces him to allow Phillipe to do his performance, where Phillipe learns that Monique has taken the place of the actress with whom he was to re-enact the kiss.  Erickson, Hal    

==Cast==
* Francis Lederer as Philippe Martin
* Ida Lupino as Monique Pelerin
* Hugh Herbert as Toto
* Roland Young as Maillot Erik Rhodes as Count Alfredo Donstelli
* Joseph Cawthorn as Monsieur Pelerin
* Donald Meek as Judge
* Georgia Caine as Cecile
* Murray Kinnell as Theatre Manager
* Mischa Auer as Leading Man
* Richard Carle as Minister of Justice
* Phyllis Barry as Felice (Maillots secretary)
* Lois January as Monsieur Pelerins Secretary
* Eily Malyon as President of Purity League

==Songs==
*"One Rainy Afternoon" - by Ralph Erwin (music) and Jack Stern (lyrics)
*"Secret Rendezvous" - by Ralph Erwin (music) and Preston Sturges (lyrics) 

==Production==
One Rainy Afternoon was the first of a small number of United Artists sound films which were produced by its vice-president, Mary Pickford,  and the first film for Pickford-Lasky Productions.  This film also marked Francis Lederers first starring role for the studio. 

United Artists ran an advertisement in the Hollywood Reporter with "thank-yous" to executives from other studios who allowed their stars to appear in One Rainy Afternoon. The ad states: "We are returning these artists to you with increased box office value." 

The film began production in early January 1936.  It was released on 13 May of that year, and re-released in January 1948.   The film was made for an estimated $511,383, and grossed $603,903 worldwide. 

== References ==
 

== External links ==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 