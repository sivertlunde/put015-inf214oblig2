Getting Started
 
{{Infobox Film
| name = Getting Started
| image =
| image_size =
| caption =
| director = Richard Condie
| producer = Jerry Krepakevich
| writer = Richard Condie
| narrator =
| starring = Jay Brazeau (voice)
| music = Patrick Godfrey
| cinematography =
| editing =
| studio = National Film Board of Canada (NFB)
| distributor =
| released =
| runtime = 12 minutes
| country = Canada English
| budget =
| gross =
| preceded_by =
| followed_by =
}}
Getting Started is a 1979 animated short by Richard Condie and produced in Winnipeg by the National Film Board of Canada.

The film is a comical look at procrastination, based partly on the filmmakers own experiences, portraying the inability of a pianist to rehearse a Debussy composition.    

Awards for Getting Started included the Genie Award for best animation film.    The film also won awards at the Zagreb World Festival of Animated Films and the Tampere Film Festival, as well as a "Bijou" at the Canadian Short Film and Television Awards.    

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 