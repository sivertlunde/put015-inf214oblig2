The Brand New Testament
{{Infobox film
| name           = The Brand New Testament
| image          = 
| caption        = 
| director       = Jaco Van Dormael
| producer       = Jaco Van Dormael Frank Van Passel David Grumbach Daniel Marquet Olivier Rausin
| writer         = Jaco Van Dormael Thomas Gunzig
| based on       = 
| starring       = Benoît Poelvoorde Yolande Moreau Catherine Deneuve François Damiens
| music          = An Pierlé
| cinematography = Christophe Beaucarne		
| editing        = Hervé de Luze
| distributor    = Belga films Le Pacte
| studio         = Terra Incognita Films Climax Films Caviar Après le Déluge Juliette Films
| released       =  
| runtime        = 106 minutes
| country        = France Belgium Luxembourg
| language       = French
| budget         = 
| gross          = 
}}
The Brand New Testament ( ) is a 2015 comedy film directed by Jaco Van Dormael. The film is a co-production between France, Belgium and Luxembourg. The film will be screened at the Directors Fortnight section at the 2015 Cannes Film Festival..      

==Cast==
 
* Benoît Poelvoorde as God
* Yolande Moreau as Gods wife
* Catherine Deneuve as Martine
* Pili Groyne as Gods daughter
* François Damiens
* Johan Leysen
* Johan Heldenbergh
* Romain Gelin
* Marco Lorenzini
* Laura Verlinden
* Emylie Buxin
* Cyril Perrin
 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 

 
 