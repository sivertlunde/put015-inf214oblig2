Red Dust (1999 film)
 

{{Infobox film
| name           = Red Dust
| image          = 
| caption        = 
| director       = Zrinko Ogresta
| producer       = Ivan Maloča
| writer         = Zrinko Ogresta Goran Tribuson
| starring       = Ivo Gregurević Kristijan Ugrina Josip Kučan Marko Matanović Slaven Knezović
| music          = Neven Frangeš
| cinematography = Davorin Gecl
| editing        = Josip Podvorac
| distributor    = 
| released       =  
| runtime        = 105 minutes
| country        = Croatia
| awards         =
| language       = Croatian
| budget         =
| preceded_by    =
| followed_by    =
}}
 1999 Cinema Croatian film Best Foreign Language Film submission at the 72nd Academy Awards, but did not manage to receive a nomination. 

==Cast==
*Ivo Gregurević.....Kirby
*Kristijan Ugrina.....Škrga
*Josip Kučan.....Crni
*Marko Matanović.....Zrik
*Slaven Knezović.....Boss

==Awards and nominations==
Haifa International Film Festival
*1999: Won, "Golden Anchor Award" - Zrinko Ogresta

Pula Film Festival
*1999: Won, "Audience Award" - Zrinko Ogresta
*1999: Won, "Best Direction" - Zrinko Ogresta
*1999: Won, "Best Editing" - Josip Podvorac
*1999: Won, "Best Screenplay" - Zrinko Ogresta Goran Tribuson
*1999: Won, "Best Supporting Actor" - Ante Vican

==References==
 

==External links==
* 
*  

 

 
 
 
 


 
 