Benji (1974 film)
{{Infobox film
| name           = Benji
| image          = Benji1974.jpg
| caption        = Theatrical release poster
| director       = Joe Camp
| producer       = Joe Camp
| writer         = Joe Camp Higgins Patsy Garrett Cynthia Smith Allen Fiuzat Peter Breck
| music          = Euel Box
| cinematography = Don Reddy
| editing        = Leon Seith
| distributor    = Mulberry Square Releasing
| released       =  
| runtime        = 85 minutes
| country        = United States English
| budget         = $500,000
| gross          = $45 million   
}} Best Original Song for the theme song "I Feel Love," by Euel Box. The film was turned down for distribution by every studio in Hollywood and Camp had to form a distribution company to distribute the film worldwide, completely without the help of Hollywood. Yet Variety reported that is was the #3 Box Office film of the year.

==Plot==

Benji is a lovable stray dog who lives in a small Texas town where he has befriended many local people, each of whom calls him by a different name, but the people he likes best are two school-age children (Paul and Cindy) and their housekeeper, Mary. Paul and Cindys father, however, doesnt want a dog around the place. One day Benji befriends a stray dog (presumably a Maltese) that Mary calls Tiffany, and the kids beg their father to allow them to keep her and Benji, but he refuses. Benji escorts Tiffany to his hide-out, an apparently abandoned house. Two people later break in and bring a kidnapped Paul and Cindy into the house. Benji rushes to "tell" George and Mary of the kids whereabouts, but is shooed out. He then grabs the ransom note, but it gets taken from him and poor Benji is a loss at what to do next. He follows two policemen into the station and gets locked in and seems doomed until he accidentally turns on the drive through intercom and the policeman lets him out. He goes back to the old house and spots a failed try at writing the ransom note and an idea strikes him. He grabs the crumpled note, but is grabbed by one of the kidnappers and Tiffany rushes out and bites him and gets a vicious kick in return (she is not killed but apparently injured). Benji races back to Mary the kids Dad but Linda beats him there. The rest is for you to see. Camp never revealed the plot to the press but rather spoke of the essence of the movie: "Its a love story about a dog struggling against the odds to accomplish something that most people would consider to be impossible; and its the first film ever in which a dog actually acted and expressed emotion on the screen. He was more or less the three-dimensional character in the film and the people were more or less the props. The dialog is in the eyes of the dog." Many film critics stated that Benji deserved a Best Actor Oscar for his acting. 

==Theme song== Best Original Song in 1975. 

==Production==
Benji was filmed near Dallas, Texas. The park scenes were in Dallas. The municipal building was filmed in Denton, Texas. 

The outdoor scenes were filmed primarily in McKinney, Texas and the house located at 1104 South Tennessee (now a bed and breakfast inn), Dowell House, served as the "haunted house" where the children were being held hostage, as well as serving as production headquarters during the filming.

The film, and the ensuing franchise, was created after Joe Camp expressed concern over the overabundance of family films released through the   has to some degree become if its G, it cant be for me."  Camp observed that four-wall companies had saturated the market for G-rated product; in response to the lowered-down quality of their films, he created Benji.   

==Reception== ninth highest review aggregate website Rotten Tomatoes. 

==Cast== Higgins - Benji
*Patsy Garrett - Mary
*Cynthia Smith - Cindy Chapman
*Allen Fiuzat - Paul Chapman
*Peter Breck - Dr. Chapman Christopher Connelly - Henry
*Tom Lester - Riley
*Mark Slade - Mitch
*Deborah Walley - Linda
*Herb Vigran - Lt. Samuels
*Frances Bavier - Lady with Cat
*Edgar Buchanan - Bill
*Terry Carter - Officer Tuttle
*Larry Swartz - Floyd

==References==
 

==External links==
*  
*  
*  
* http://benji.com

 
 

 
 
 
 
 
 
 
 
 
 