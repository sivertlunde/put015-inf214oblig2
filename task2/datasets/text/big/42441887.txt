The Wolf of the Sila
{{Infobox film
| name = The Wolf of the Sila
| image =
| image_size =
| caption =
| director = Duilio Coletti
| producer =  Dino De Laurentiis   Carlo Musso Steno   Vincenzo Talarico
| narrator =
| starring = Silvana Mangano   Amedeo Nazzari   Vittorio Gassman   Jacques Sernas
| music = Enzo Masetti   Osvaldo Minervini 
| cinematography = Aldo Tonti  
| editing = Adriana Novelli      
| studio = Lux Film 
| distributor = Lux Film
| released = December 1949
| runtime = 95 minutes
| country = Italy Italian 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
The Wolf of the Sila (Italian:Il lupo della Sila) is a 1949 Italian drama film directed by  Duilio Coletti and starring Silvana Mangano, Amedeo Nazzari and Vittorio Gassman.  Much of the film was shot on location around La Sila in Calabria.

==Synopsis==
After the death of her brother at the hands of the police, a young woman takes her revenge on the two men she believes to have been responsible.

==Cast==
* Silvana Mangano as Rosaria Campolo  
* Amedeo Nazzari as Rocco Barra  
* Vittorio Gassman as Pietro Campolo  
* Jacques Sernas as Salvatore Barra  
* Luisa Rossi as Orsola Barra  
* Olga Solbelli as La madre di Rosaria  
* Dante Maggio as Gennaro  
* Michele Capezzuoli as Salvatore da bambino   Laura Cortese as Rosaria da bambina 
* Attilio Dottesio as Un contadino  
* Rudy Randi as Contadino

== References ==
 

== Bibliography ==
* Forgacs, David & Gundle, Stephen. Mass Culture and Italian Society from Fascism to the Cold War. Indiana University Press, 2007.

== External links ==
*  

 
 
 
 
 
 
 
 
 
 
 