Hum Se Badkar Kaun
Click Humse Badhkar Kaun for the Sunil Shetty - Saif Ali Khan starrer released in 1998 
{{Infobox film
 | name = Hum Se Badkar Kaun
 | image = HumSeBadkarKaunfilm.jpg
 | caption = Vinyl Records Cover
 | director = Deepak Bahry
 | producer = Pranlal Mehta
 | writer = 
 | dialogue = 
 | starring = Mithun Chakraborty Amjad Khan Danny Denzongpa Birbal Mohan Choti
 | music = Raamlaxman
 | lyrics = 
 | associate director = 
 | art director = 
 | choreographer = 
 | released = 1981
 | runtime = 139 min.
 | language = Hindi Rs 1.5 Crores
 | preceded_by = 
 | followed_by = 
 }}
 1981 Hindi Indian feature directed by Deepak Bahry, starring Mithun Chakraborty, Vijayendra Ghatge, Ranjeeta Kaur, Amjad Khan, Danny Denzongpa, Kajal Kiran, Birbal and Mohan Choti. 

==Plot==

Hum Se Badkar Kaun is an Action film starring Mithun Chakraborty, Amjad Khan well supported by Danny Denzongpa.

==Cast==

*Mithun Chakraborty
*Amjad Khan
*Danny Denzongpa
*Ranjeeta Kaur
*Vijayendra Ghatge
*Birbal
*Mohan Choti
*Neeta Mehta
*Padmini Kapila

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Ganpati Bappa Moriya(Deva Ho Deva)" Bhupinder Singh, Shailender Singh, Sapan Chakraborty, Asha Bhosle
|-
| 2
| "Dekho Logo"
| Mohammed Rafi, Asha Bhosle
|-
| 3
| "Humse Badhkar Kaun"
| Kishore Kumar
|-
| 4
| "Huyi Umar Yeh"
| Asha Bhosle
|-
| 5
| "Kammoji"
| Mahendra Kapoor, Usha Mangeshkar
|-
| 6
| "Sapnon Ka Gharonda"
| Bhupinder Singh
|}

==External links==
*  
* http://ibosnetwork.com/asp/filmbodetails.asp?id=Humse+Badhkar+Kaun+%281981%29

 
 
 

 