Commando (2013 film)
 
 

 

{{Infobox film
| name=Commando – A One Man Army
| image=Commando (2013 film).jpg
| image_size =
| alt=
| caption=Theatrical release poster
| director=Dilip Ghosh
| producer=Vipul Shah
| story=
| based on=
| screenplay=
| starring=Vidyut Jamwal Pooja Chopra Jaideep Ahlawat
| music= Mannan Shaah Background Score: Prasad Sasthe   
| cinematography=Sejal Shah
| editing=Amitabh Shukla
| studio=
| distributor=Reliance Entertainment
| released=  
| runtime=
| country=India
| language=Hindi
| budget         = 
| gross          =    
}}
 2013 Hindi action thriller film directed by Dilip Ghosh and produced by Vipul Shah. The film features Vidyut Jamwal as the protagonist, Pooja Chopra and Jaideep Ahlawat as main characters.The film opened to mixed reviews with major praise directed towards Jamwals performance and was a moderate box office success.

==Synopsis== Para Commandos of the Indian Army. During a routine helicopter training their helicopter crashes into the Chinese side. The lone survivor of the wreckage, he is captured by Chinese officials. As the crash happened on a river and wreckage of the chopper got washed away, Karan as well as his seniors are not able to convince the Chinese of his innocence. The Chinese feel they can use these circumstances to brand him as an Indian Spy and use this political leverage to embarrass the Indian Government and create an International fracus. The Indian Government feels that under such politically unfavourable circumstances, the Chinese will not accept any proof of Karan having crashed during a routine training so they order the Army to erase Karans army record, identities and simply deny his existence. Due to this Karan has to suffer inhuman torture from Chinese army for one year in a bid to force a confession out of him.

However as Karan still refuses to relent, he is transferred to be tried at the Chinese Military Court with the most probable and possible sentence being the death penalty. During his transfer however, he escapes from his captors and crosses into Himachal Pradesh at the Lepcha border and travels through Kinnaur to reach his base at Pathankot. As Karan crosses the Himachal – Punjab border, he accidentally bumps into a girl, Simrit (Pooja Chopra) who is escaping from Amrit Kanwal Singh (Jaideep Ahlawat) goons and AKs brother whos a MP. Amrit Kanwal Singh or as he is better known AK-74, a notorious cold-blooded criminal and murderer with such strong political ties that he gets away with anything and everything illegal he does even after he murdered a Forest Official, wants to marry Simrit for political gains as it will lend him some respectability and political leverage in the upcoming electoral campaign. Karan warns the goons to let go off her but they dont listen and bear the brunt of his pent up anger and after losing some men they retreat. Simrit plays him to escort her out of the town by telling Karan that he has created more trouble for her and now he must escort her till she feels safe. AK waylays them on the Andheria Bridge. Finding himself outnumbered and outgunned, Karanvir jumps off the bridge into the fast flowing river with Simrit after kicking AK on his face. After flowing along the river for some distance, they climb to the banks and end up inside the forest. AKs power, position and respect from ordinary people of Dilerkot is largely attributed to fear-tactics efficiently used by him. After being publicly humiliated by Karan, AK senses that this act of Karan would affect his fear among people negatively, he declares that within one day he would track him down alive and hang him in the middle of marketplace in Dilkerkot. In the meantime AK kills a visibly frightened onlooker who laughs at AKs failure to capture Karanvir on the bridge. In the forest, Karan hears Simrits story and promises to help her. He decides to stick to the jungle route and then get out on the highway once he feels that the heat from Amrit Kanwal Singh and his henchmen has died down. AK on the other hand decides to pursue them inside the jungle.

Now begins a cat and mouse game between AK, his men, and Karan in the forest. Karan at first successfully overpowers AKs henchmen by taking into effect his physical strength, Guerilla warfare tactics, cunning warfare skills, training as a commando and extreme knowledge of the areas flora and fauna and had an upper hand over the untrained and novice AKs men. The next morning AK74 hires special trackers and sets out along hunting dogs. AK spots a torn piece of 
Simrits shirt. The hunting dogs thus get a scent of the duo and tracks them down. Karan misleads the dogs and crosses a river and the dogs loose them. Karan and simrit get outnumbered and Karan is shot in his stomach. AK throws Karan into a shallow river with rock beds jutting out here and there. One year later Karan survives and after recuperating fully heads back to the town. Meanwhile AK kills Simrits parents and stages it as an accident and threatens Simrit to marry him. Karan fights all of AKs goons and an hired assassin sent by the Chinese. He beats up AK badly, breaks his ankle and hangs AK on the town square and surrenders to his Commander Col. Akhilesh Sinha (Darshan Jariwala) who had timely arrived to save him from the local police who are mere puppets of AK.

The movie ends with Karan finally telling Simrit his full name, Captain Karanvir Singh Dogra and promises to return after the court-martial.

==Cast==
* Vidyut Jamwal as Captain Karanvir Singh Dogra
* Pooja Chopra as Simrit Kaur
* Jaideep Ahlawat as Amrit Kanwal (AK 74)
* Jagat Rawat as M.P. Mahendra Pratap
* Darshan Jariwala
* Nathalia Kaur Special Appearance (Item Dance)

==Soundtrack==
The soundtrack album of Commando was released on 28 March 2013 as digital download on popular digital music platforms, iTunes and amazon. It contains 4 original tracks. The music is composed by Mannan Shaah with lyrics penned by Mayur Puri, while the background score has been scored by Prasad Sasthe.  

{{Infobox album
| Name = Commando - A One Man Army
| Type = Soundtrack
| Artist = Mannan Shaah
| Lyrics = Mayur Puri
| Cover = 
| Border =
| Alt =
| Caption =
| Released =  
| Recorded = 2012-2013 Feature film soundtrack, Soundtrack
| Length = 20:27 Hindi
| Super Cassettes Industries Ltd.
| Producer =
| Last album=Bas Ek Pal (2006) 
| This album=Commando - A One Man Army (2013)
| Next album=
}}

{{Track listing
| headline = Tracklist
| extra_column = Singer(s)
| total_length = 20:27
| lyrics_credits = yes
| title1 = Lutt Jawaan
| extra1 = Dhruv Sangari
| lyrics1 = Mayur Puri
| length1 = 4:48
| title2 = Saawan Bairi
| extra2 = Rahat Fateh Ali Khan
| lyrics2 = Mayur Puri
| length2 = 5:20
| title3 = Mungda
| extra3 = Sunidhi Chauhan
| lyrics3 = Mayur Puri
| length3 = 5:09
| title4 = Lena Dena
| extra4 = Daler Mehndi
| lyrics4 = Mayur Puri
| length4 = 5:10
}}

==Release==

===Critical reception===
Upon release,Commando garnered mixed to positive reviews from critics. At Reviewbol.com, it got an average review score of 3/5 from major sites. Taran Adarsh of Bollywood Hungama gave Commando 3.5 out of 5 stars and said "A high-voltage action fare thats racy, pulsating and packed with some adrenaline-pumping stunts".  Meena Iyer of The Times of India rated it with 3.5 out of 5 star while remarking "For the first time in eons, the action scenes are totally convincing".  Shubhra Gupta of Indian Express awarded it 3.5 out of 5,noting," That an action film should have no place for song and dance is a no-brainer. It also has a story that creaks. Minus these irritants, it would have been a pure adrenaline rush".  Saibal Chatterjee of NDTV gave it 2 out of 5 while commenting "Despite all the bravura technical effort that has clearly been put into Commando, the end result simply isn’t compelling enough". 
Alisha Coelho of in.com gave the movie 2.5 stars, saying "Commando- A One Man Army is a one man show where Vidyut carries the entire weight on his shoulders."    

===Boxoffice===
  The film earned around   on its opening day.    The film raked   over its first weekend.  At its first week,It netted around   having decent business in single screens.    It finished at around   in India.
.

==References==
 


==External links==
*  

 
 
 
 
 
 
 