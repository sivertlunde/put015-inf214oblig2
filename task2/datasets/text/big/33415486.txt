Bees Saal Baad (1988 film)
{{Infobox film
 | name = Bees Saal Baad
 | image = BeesSaal89.jpg
 | caption = Promotional Poster
 | director = Rajkumar Kohli
 | producer = Nishi  
 | writer = 
 | dialogue = 
 | starring = Mithun Chakraborty Dimple Kapadia Meenakshi Sheshadri Shakti Kapoor Vinod Mehra Amjad Khan Anupam Kher Om Prakash Asrani
 | music = Laxmikant-Pyarelal
 | lyrics = 
 | cinematographer = 
 | associate director = 
 | art director = 
 | choreographer = 
 | released = 1988 (India)
 | runtime = 135 min.
 | language = Hindi Rs 5 Crores
 | preceded_by = 
 | followed_by = 
 }}
 Indian Horror horror feature directed by Rajkumar Kohli, starring Mithun Chakraborty, Dimple Kapadia, Meenakshi Sheshadri, Vinod Mehra, Amjad Khan, Shakti Kapoor, Anupam Kher and Om Prakash.

==Plot==

Bees Saal Baad is a horror family drama, featuring Mithun Chakraborty, Dimple Kapadia, Meenakshi Sheshadri in lead roles, well supported by Vinod Mehra, Amjad Khan, Shakti Kapoor, Anupam Kher and Om Prakash.

Kiran is the only child of wealthy widower, Thakur. While she resides in Britain, her dad lives in a palatial house in rural India. When the time comes for her to return home, her dad arrives at the airport to receive and is surprised to see her in the company of a young man, who she later introduces as her husband, Suraj. Thakur overcomes his shock at seeing Suraj, but welcomes him. On the way while driving, Suraj sees a woman in white blocking the road, but the driver is unable to stop the car in time. They alight from the car and note that there is no one on the road. That night, Suraj has a dream about a woman clad in white who is summoning him to her, claiming that she is Nisha, his wife from a previous birth. Using her magical powers she inserts a nail in one of Surajs legs on a new moon night. All she has to do is wait for the next new moon, then insert another nail in Surajs other foot, and then wait for Karva Chouth - that is the day that will put him completely under her control. Nisha also has an old score to settle with Thakur. A frantic Kiran cannot hear of this and she and her dad seek help from Tantrik Baba and Bhavani Baba. The question remains will the Tantrik and Bhavani be able to assist them or will they too fall prey to the vengeful spirit.

==Cast==

*Mithun Chakraborty ...  Suraj 
*Dimple Kapadia ...  Nisha 
*Meenakshi Sheshadri ...  Kiran Thakur 
*Vinod Mehra ...  Inspector Verma 
*Amjad Khan ...  Bhavani Baba
*Om Prakash ...  Sarju 
*Shiva Rindani ...  Badal 
*Anupam Kher ...  Thakur 
*Shakti Kapoor ...  Tantrik Baba 
*Jagdeep ...  Chedhu 
*Aruna Irani ...  Mithi 
*Jankidas ...  Jankidas (Broker) 
*Pinchoo Kapoor ...  Senior Police Officer 
*Tej Sapru ...  Badal 
*Jayshree T. ...  Tara

==Soundtrack ==
{{Infobox album Name     = Bees Saal Baad Type     = film Cover    = Beessaalbaadaudio.jpg Released =  Artist   = Laxmikant–Pyarelal Genre  Feature film soundtrack Length   = 25:16 Label    = Venus
}}

The music composed by Laxmikant–Pyarelal.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics 
|-
| 1 || Hum Tumhein Itna Pyar Karenge  || Anuradha Paudwal, Mohammad Aziz|| Anand Bakshi
|-
| 2 || Kitne Sawan Baras Gaye 1&2  || Anuradha Paudwal || Anand Bakshi
|-
| 3 || Jago Jago Devi Mata  || Anuradha Paudwal || Anand Bakshi
|-
| 4 || Mere Saamne Tu Din Raat Rahe || Mohammad Aziz, Sarika Kapoor || Anand Bakshi
|-
| 5 || O Baliye Ni Chal Chaliye || Anuradha Paudwal, Mohammad Aziz || Anand Bakshi
|-
|}

==References==
 
* http://ibosnetwork.com/asp/filmbodetails.asp?id=Bees+Saal+Baad+(1989) -
* http://www.bollywoodhungama.com/movies/cast/5254/index.html

==External links==
*  

 
 
 
 
 