Classmates (1924 film)
{{infobox film
| name           = Classmates
| image          =File:Classmates 1924.jpg
| imagesize      =
| caption        =Lobby card
| director       = John S. Robertson
| producer       = Inspiration Pictures
| writer         = Josephine Lovett (scenario) H. E. R. Studios (intertitles)
| based on       =  
| starring       = Richard Barthelmess Madge Evans
| music          =
| cinematography = Roy Overbaugh John F. Seitz William Hamilton Associated First National
| released       =  
| runtime        = 7 reel#Motion picture terminology|reels; 6,992 feet
| country        = United States Silent (English intertitles)
}} silent drama Associated First National Pictures. 
 Margaret Turnbull.

The play was previously filmed in 1908 and 1914.  

==Cast==
*Richard Barthelmess - Duncan Irving Jr Charlotte Walker - Mrs. Stafford
*Madge Evans - Sylvia, her niece
*Reginald Sheffield - Bert Stafford, her son
*Beach Cooke - Bubby Dumble
*Antrim Short - Jones
*Herbert Corthell - Drummer
*James Bradbury Jr - Silent Clay
*Henry Balding Lewis - Major Lane
*Richard Harlan - A Halfbreed
*Claude Brooke - Duncan Irving Sr
*Chief Tony Tommy - Indian Guide
*Jack Oakie - Bit Part (uncredited)

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 


 