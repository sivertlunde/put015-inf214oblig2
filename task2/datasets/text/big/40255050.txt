Little Accidents
{{Infobox film
| name           = Little Accidents
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = Little Accidents poster
| film name      = 
| director       = Sara Colangelo
| producer       = Anne Carey   Jason Berman   Tom Fore
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| starring       = Elizabeth Banks   Boyd Holbrook   Chloë Sevigny   Josh Lucas   Jacob Lofland
| music          = Marcelo Zarvos
| cinematography = Rachel Morrison
| editing        = Suzy Elmiger 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 105 min
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Little Accidents is an 2014 American drama film directed and written by Sara Colangelo, based on her own 2010 award-winning short film of same name. The film stars Elizabeth Banks, Boyd Holbrook, Chloë Sevigny and Josh Lucas.  The film had its world premiere at 2014 Sundance Film Festival on January 22, 2014. 

==Plot==
In a small American town still living in the shadow of a terrible coal mine accident, the disappearance of a teenage boy draws together a surviving miner, the lonely wife of a mine executive, and a local boy in a web of secrets.

==Cast==
* Elizabeth Banks   
* Boyd Holbrook   
* Chloë Sevigny   
* Josh Lucas 
* Jacob Lofland 

==Production== Chris Columbus, Eleanor Columbus, Kwesi Collisson and Ruth Mutch. 

==Release== Amplify picked up the US rights to the film. A January 2015 release is planned. 

==References==
 

==External links==

* 

 
 
 
 
 
 
 


 