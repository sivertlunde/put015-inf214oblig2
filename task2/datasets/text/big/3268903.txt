Night of the Demons (1988 film)
{{Infobox film
| name           = Night of the Demons
| image          = Night of the Demons poster.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Kevin S. Tenney
| producer       = Joe Augustyn
| writer         = Joe Augustyn
| starring       = William Gallo Hal Havins Amelia Kinkade Cathy Podewell Linnea Quigley
| music          = Dennis Michael Tenney
| cinematography = David Lewis
| editing        = Daniel Duncan
| distributor    = Republic Pictures International Film Marketing Skouras Pictures Metro-Goldwyn Mayer
| released       =  
| runtime        = 87 minutes   90 minutes  
| country        = United States
| language       = English
| budget         = $1.2 million
| gross          = $3,109,904
}} supernatural horror horror film written and produced by Joe Augustyn and directed by Kevin S. Tenney. The film tells the story of ten high school seniors having a Halloween party in an isolated mortuary. Their party turns into a nightmare when after conducting a séance as a party game, they unlock the demon that remains locked in the crematorium. Filming of Night of the Demons took place in South Central Los Angeles, California, USA, and lasted for two months. Anchor Bay Entertainment released it to DVD in 2004; Scream Factory (under license from current rights holder MGM) released a Blu-ray/DVD combo pack collectors edition on February 4, 2014.
 remake in 2009. 

==Plot== apples and razor blades.

Judy and her boyfriend Jay are picked up by Max and Frannie for the party. Sal Romero, another teen also comes and crashes the party. The house is rumored to be cursed with evil spirits. When Angela holds a séance as a party game, Helen sees a demons face in the mirror, which causes the mirror to shatter. This prompts a demon to come out of the crematorium furnace and possess Suzanne. Helen and Roger want to leave and so are given Angelas car keys.

The possessed Suzanne kisses Angela, passing the demon to her. Judy discovers that Jay only invited her to the party to have sex with her. She refuses him and is abandoned by him. Roger and Helen are unable to find an exit. Helen leaves Roger in the car, after which she is killed by a demon. Stooge wanders off to find a bathroom with Suzanne and is irritated when she locks him out. Inside the bathroom Suzanne ages and disappears into a mirror. Confused, Stooge goes to find Angela and is promptly killed by her. The party goers are then picked off one by one by the increasing number of demon possessed people, Jay dying during a sexual encounter with Suzanne and Max and Frannie by a now-possessed Stooge. 

Roger, Sal and Judy attempt to escape the demons. Sal is killed after he falls off a roof and lands on a spike. Judy and Roger end up in the mortuarys crematorium and use fire to keep the demons at bay. The two attempt to escape and are instead chased by various demons throughout the house. They manage to make it out of the house just in time to see the sun rise and banish the demons back to Hell. 

Roger and Judy slowly walk home in a state of shock, passing by the elderly man from the first scene. The elderly man goes into his house and begins to eat one of his wifes homemade apple pies that she made with the apples into which he had stuck razor blades. The razor blades cut through his throat as they kill him. The woman kisses his head and says "Happy Halloween dear."

==Cast==
* Cathy Podewell as Judy Cassidy
* Amelia Kinkade (credited as Mimi Kinkade) as Angela Franklin
* Linnea Quigley as Suzanne
* Alvin Alexis as Roger
* Allison Barron as Helen
* Lance Fenton as Jay Jansen
* William Gallo as Sal Romero
* Hal Havins as Stooge
* Philip Tanzini as Max
* Jill Terashita as Frannie

==Soundtrack== Bauhaus – "Stigmata Martyr"
* Dennis Michael Tenney – "Main Title Theme"
* Dennis Michael Tenney, Steve Ring, and Tim Wojan – "Computer Date"
* Dennis Michael Tenney, Steve Ring, Rich Lowe, Paul Ojeda, and Bobby Thompson – "The Beast Inside"
* Dennis Michael Tenney, Steve Ring, Rich Lowe, Paul Ojeda, and Bobby Thompson – "Victims of the Press"

==Reception==
Initial critical reception to the film was predominantly negative, with the Washington Post criticizing the film as "a convergence of stereotypes ... and cliches".  The New York Times reported that "the cleverest thing about Night of the Demons is its advertising campaign" and that it "is stupid; it is sexist; at 89 minutes it feels unforgivably long". 

Later reviews have been more positive, with Cinematical writing that "while not particularly original, Tenneys film is definitely entertaining if youre into the whole teens wander into an isolated locale and die horrible deaths subgenre of horror".  HorrorNews.net called Night of the Demons one of the 80’s great legacies in horror".  DVD Talk praised the films 2004 DVD release, but stated that the director and producer commentary was "seemingly stodgy".  Dread Central stated, "Its fun. Lively. A masterpiece, its not."  Bloody Disgusting praised the movies DVD release, calling it "the perfect DVD for all fans of this lost era: “The Eighties Horror Film”." 

==Sequels== remake of the movie was also released in 2009.  This movie has attempted a Kickstarter campaign to produce its own sequel, but this has been unsuccessful.

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 