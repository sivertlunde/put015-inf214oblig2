Over My Dead Body (2012 film)
{{Infobox film
| name           = Over My Dead Body 
| image          = Over My Dead Body-poster.jpg
| caption        = Promotional poster for Over My Dead Body
| film name      = {{Film name
| hangul         =  
| hanja          =  가 돌아왔다
| rr             = Sichega Dolawattda
| mr             = Sicheka Tolawatta}}
| director       = Woo Seon-ho
| producer       = Lee Choon-yeon
| writer         = Woo Seon-ho
| starring       = Lee Beom-soo Ryoo Seung-bum Kim Ok-bin
| music          = Yoon Joon-ho
| cinematography = Jo Sang-yoon
| editing        = Park Kyung-sook
| studio         = Cine 2000
| distributor    = CJ E&M
| released       =  
| runtime        = 110 minutes
| country        = South Korea
| language       = Korean
| budget         =  
| gross          =   
}}
Over My Dead Body ( ; lit. "Return of the Corpse") is a 2012 South Korean comic heist film, starring Lee Beom-soo, Ryoo Seung-bum and Kim Ok-bin. The plot centers around a biotech researcher, a woman whose father was murdered, and a man attempting to commit insurance fraud whose lives get tied up in the case of a stolen semiconductor chip and a missing corpse.   Released on March 29, 2012, the film sold 985,178 tickets in total. 

==Plot==
Baek Hyun-chul (Lee Beom-soo) is a biotech scientist researching the cure for skin cancer. Deciding to shut the lab and sell Hyun-chuls work overseas, the head of the pharmaceutical conglomerate, Kim Taek-soo, sends Steve Jung and his gang to force the lab to shut down and take the research. Hyun-chuls colleague Han Jin-soo protests the decision, and is later involved in a hit-and-run accident and falls into a vegetative state.
 
Taek-soo is also betrayed and murdered by Steve, little knowing the research is on a microchip planted inside his body. Driven by vengeance, Hyun-chul and Jin-soos pink-haired daughter Dong-hwa (Kim Ok-bin) plot to steal Taek-soos body to pay for Jin-soos hospital bills. However the body they escape from the morgue with isn’t Taek-soos but that of Ahn Jin-oh (Ryoo Seung-bum), a man who faked his death in order to hide from loan sharks. Believing Taek-soos body has been stolen, Steve and his gang begin a hunt for Hyun-chul and Dong-hwa to recover the precious microchip.  

==Cast==
*Lee Beom-soo - Baek Hyun-chul  
*Ryoo Seung-bum - Ahn Jin-oh    
*Kim Ok-bin - Han Dong-hwa   NIS agent Jang Ha-yeon
*Jung Man-sik - Steve Jung 
*Shin Jung-geun - Team leader Jo Go Chang-seok - Sung-koo 
*Oh Jung-se - Myung-kwan 
*Bae Jeong-nam  - Jong-moo 
*Jung In-gi - Han Jin-soo

==Production==
When director Woo Seon-ho was at his maternal grandfathers funeral, he wondered what would happen if his grandfathers dead body suddenly disappeared. This unusual and dark thought inspired his debut feature film, Over My Dead Body. He had previously won the comedy award at the 4th Mise-en-Scene Short Film Festival for My Really Big Mike.  

Over My Dead Body has elements of social satire and black comedy as well as character comedy. At the core of the comedy is various unexpected situations as different characters come together. There are also chase scenes shot around Banpo Bridge and the surrounding high-rise apartment blocks. 

==References==
 

==External links==
*    
*  
*  
*  

  
 
 
 