The Command (film)
{{Infobox film
| name           = The Command
| image	=	The Command FilmPoster.jpeg
| caption        = Original film poster David Butler
| producer       = David Weisbart
| writer         = James Warner Bellah (novel "Rear Guard") Samuel Fuller Russell S. Hughes
| narrator       = 
| starring       = Guy Madison
| music          = Dmitri Tiomkin
| cinematography = Wilfred M. Cline
| editing        = 
| studio         = 
| distributor    = Warner Bros.
| released       =  
| runtime        = 94 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $2.5 million (US) 
}} Western film David Butler. It stars Guy Madison and James Whitmore.  It was based on the novel Rear Guard by James Warner Bellah and features a screenplay by Sam Fuller.

==Plot==
Once the commanding officer of a cavalry patrol is killed, the ranking officer who must take command is an army doctor.

==Cast==
*Guy Madison as Capt. Robert MacClaw
*Joan Weldon as Martha Cutting
*James Whitmore as Sgt. Elliot
*Carl Benton Reid as Col. Janeway
*Harvey Lembeck as Pvt. Gottschalk

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 

 