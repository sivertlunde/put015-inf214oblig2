Fired Up!
 
 
{{Infobox film
| name           = Fired Up!
| image          = Fired-up.jpg
| caption        = Theatrical release poster
| director       = Will Gluck
| producer       = Will Gluck Matthew Gross Paddy Cullen
| writer         = Freedom Jones
| music          = Richard Gibbs Mark Hoppus Avril Lavigne
| cinematography = Thomas E. Ackerman
| editing        = Tracey Wadmore-Smith
| distributor    = Screen Gems
| released       =  
| runtime        = 89 minutes
| country        = United States
| language       = English
| budget         = $20 million
| gross          = $18,599,102   
}}
Fired Up! is a 2009 American teen comedy film written and directed by Will Gluck. The main plot revolves around two popular college students football players (portrayed by Eric Christian Olsen and Nicholas DAgosto) who attend a cheerleading camp for the summer to get close to its 300 female cheerleaders.

==Plot==
Nick Brady and Shawn Colfax (Eric Christian Olsen and Nicholas DAgosto) are two popular football players at the fictional Gerald R. Ford High School who manage to get out of football camp and later con their way into the cheerleading squad after overhearing a conversation about the camps abundant female population of 300 cheerleaders. Their objective is to infiltrate the cheerleading camp in order to meet girls. While attending a cheer camp, Nick and Shawn realize that they actually enjoy cheering and they start to care about their squad as well as the cheer competition. Shawn develops feelings for the head cheerleader, Carly Davidson (Sarah Roemer) and Nick chases after Diora (Molly Sims), their camp coachs wife.
 David Walton), also reveals that Nick and Shawn initially planned to leave cheer camp before the cheer competition. Nick and Shawn leave camp after being ejected from the squad. While attending a party at their friends house, Nick and Shawn find out that they are genuinely fond of cheer camp and want their squad to succeed. Nick and Shawn decide to return to cheer camp and help the squad in the cheer competition. While Nick and Shawn are doing their routine, Carly notices Rick is cheating on her with their rivals head cheerleader, Gwyneth (AnnaLynne McCord). Shawn and Carly later focus all of their attention to the routine. The squads routine results in their best finish yet, with a perfect attempt at executing the "Fountain of Troy" maneuver. However, when the squad go for executing the forbidden maneuver, Shawn accidentally goes for a triple backflip instead of a double like Carly and backflips into the water in front of them. The crowd gasps at the impact and the squad rushes to help, but Shawn manages to emerge and yells "Tigers!" before losing consciousness. Although the squad did not win the contest, they place ten spots better than they did last year. The film ends when Nick and Shawn end up with the girls with Shawn and Carly kiss with each other.

==Cast==
 
* Nicholas DAgosto as Shawn Colfax
* Eric Christian Olsen as Nick Brady
* Sarah Roemer as Carly Davidson David Walton as Dr. Rick
* AnnaLynne McCord as Gwyneth
* Juliette Goglia as Poppy Colfax
* Molly Sims as Diora
* John Michael Higgins as Coach Keith
* Danneel Harris as Bianca
* Margo Harshman as Sylvia
* Hayley Marie Norman as Angela
* Adhir Kalyan as Brewster
* Philip Baker Hall as Coach Byrnes
* Edie McClurg as Mrs. Klingerhoff
* Jake Sandvig as Downey
* Collins Pennie as Adam 
* Nicole Tubiola as Marcy
* Smith Cho as Beth
* Masi Oka as Eagle Mascot
* Michael Blaiklock as Mookie
* Alan Ritchson as Bruce
* Shoshana Bush as Girl
* Kate Lang Johnson as Jennifer
* Jill Latiano as Haley
* Amber Stevens as Sara
* Francia Raisa as Marly
* Janel Parrish as Lana
* Madison Riley as Lily
* Jessica Szohr as Kara
* Tanya Chisholm as Denise
* Julianna Guill as Amy Heather Morris as Fiona
* Kelen Coleman as Maddy
* Kayla Ewell as Margot Jane Lindsworth-Calligan
* Rachele Brooke Smith as Tiger Cheerleader
* Kate French as Cute Captain
 

==Production==
===Filming locations=== Gerald R. Ford High Schools mascot was made the "Tigers" since South Pasadena Highs mascot is a tiger.  Some of the South Pasadena Tigers Football teams gear such as pads, were borrowed for use in the film. However, filming for the football game scene took place at Calabasas High School. The filming of the pool scene took place at Long Beach Polytechnic High School.  The cheerleader camp was filmed at Occidental College, which incidentally also has the tiger as a mascot. In one of the early scenes, the train passing by is the  Metro Gold Line (LACMTA) Pasadena line. The location where the cheerleaders were practicing was filmed in the Los Angeles County Arboretum and Botanic Garden.
==Reception==
===Critical response ===
When the film was released, it was screened to negative reviews. Rotten Tomatoes gave the film a rating of 23% based on 101 reviews, with the general consensus being that "though not as raunchy or juvenile as the average teen comedy, Fired Up is also not as funny." {{cite web
| url = http://www.rottentomatoes.com/m/fired_up/
| title = Fired Up Movie Reviews
| work = Rotten Tomatoes
| publisher= Flixster
| accessdate =May 1, 2010
}}
 
Metacritic gave the film a "generally unfavorable" score of 31% based on a normalized average of 18 reviews.  A common criticism,   addressed by director Will Gluck in the films commentary track, is that the filmmakers "casted a little bit older." Star Eric Christian Olsen adds, "If by older, you mean thirteen years!" Lead actors Olsen and Nicholas DAgosto were playing high school students at the ages 30 and 27 at the time of filming, respectively. Gluck also points out Roger Eberts and the New York Timess negative reviews specifically, as well as a mention of the Washington Post. He, however, omits the Washington Posts backhanded compliment that "Gluck directs with frantic, go-for-broke pacing, which is what you do when your reserves of wit are bankrupt." One of the more positive reviews, from Hollywood.com, admits its satisfying for the audience its aimed towards: "An outrageous, sex-obsessed teen comedy that’s something to cheer about -- especially if you’re 16."

=== Box office === Confessions of done R, but I kind of like the idea of doing a movie that everyone can go see, and not just over 18 or have to sneak into it." It left American cinemas after seven weeks.

==Home media==
 
The movie was released on DVD, Universal Media Disc|UMD, and Blu-ray Disc|Blu-ray formats June 9, 2009. An unrated version was also released containing non-censored profanity and brief nudity not seen in the theatrical cut.
Besides Canadian singer Avril Lavigne collaborated with her song Girlfriend for the soundtrack of this movie .

==References==
 

==External links==
 
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 