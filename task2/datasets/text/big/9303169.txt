A Ninja Pays Half My Rent
 
{{Infobox film
| name           = A Ninja Pays Half My Rent
| image          = 
| image_size     = 
| alt            = 
| caption        = 
| director       = Steven Tsuchida
| producer       = Mark Castro Steven Tsuchida
| writer         = Aaron Ginsburg Wade McIntyre Steven Tsuchida
| starring       = Timm Sharp Anthony Liebetrau Shin Koyamada
| music          = 
| cinematography = Rhet W. Bear
| editing        = Ken Mowe
| studio         = Black Gold Films
| distributor    = Oil Factory
| released       = Sundance Film Festival  
| runtime        = 5 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
A Ninja Pays Half My Rent is a short comedy film by Steven Tsuchida, it was first released in 2003.

==Plot==
When your roommate dies from an allergic reaction to fruit and the rent is due and you dont have the cash to cover it, theres no time to mess around picking a new roommate. So it goes that our hero in this short comes to share his home with a ninja. Then another ninja wants to be his roommate. Conditions for disaster, fulfilled.

==Cast==
*Timm Sharp - Barry
*Shin Koyamada - Black Ninja
*Steven K. Tsuchida - Red Ninja
*  - Running Friend
*Anthony Liebetrau - Grapefruit Roommate

==Film Festivals and Awards==
 
*2003 Sundance Film Festival
*2003 HBO US Comedy Arts Festival
*2003 South by Southwest Film Festival
*2003 New Directors/New Films at the Lincoln Center
*2003 Aspen ShortsFest (Special Jury Recognition)
*2003 Philadelphia International Film Festival
*2003 Boston Independent Film Festival
*2003 MVPA Film Festival
*2003 Arizona International Film Festival (Best Comedy Short)
*2003 Visual Communication Film Festival
*2003 Maryland Film Festival
*2003 Waterfront Film Festival
*2003 Asian Film Festival of Dallas
*2003 Lake Placid Film Forum
*2003 Fantasy Film Fest – Germany (Bronze Audience Award)
*2003 Fantasia Film Fest-Montreal
*2003 IFP Los Angeles Film Festival
*2003 HypeFest
*2003 1 Reel Film Festival – Bumbershoot
*2003 Tulsa Overground Film Festival
*2003 Mid Coast film & Arts Festival
*2003 Sidewalk Moving Picture Festival (Audience Award)
*2003 Rehoboth International Film Festival
*2003 Calgary International Film Festival
*2003 Washington D.C. Asian Film Festival
*2003 Cardiff International Film Festival
*2003 San Francisco World Film festival
*2003 Hamptons International Film Festival
*2003 RES Fest
*2003 Denver International Film Festival
*2003 Edinburgh Film Festival
*2003 Stockholm International Film Festival
*2003 Hawaii International Film Festival
*2003 Deauville Film Festival

==External links==
* 
* 
* 
* 

 
 
 
 