Prince of Tempters
{{Infobox film
| name = Prince of Tempters
| image =
| image_size =
| caption =
| director = Lothar Mendes Robert Kane
 | writer = E. Phillips Oppenheim (novel)   Paul Bern 
| narrator =
| starring =  Lois Moran   Ben Lyon   Lya De Putti   Ian Keith
| music = 
| cinematography = Ernest Haller  
| editing =    
| studio = Robert Kane Productions 
| distributor = First National Pictures 
| released = October 17, 1926
| runtime = 82 minutes
| country = United States English intertitles
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} silent romance Cosmopolitan Studios New York. It was the first film made in America by the German director Lothar Mendes, who married Dorothy Mackaill while working on the production.   

A copy of this film is held at the Museum of Modern Art, New York. 

==Cast==
* Lois Moran as Monica 
* Ben Lyon as Francis 
* Lya De Putti as Dolores 
* Ian Keith as Mario Ambrosio, later Baron Humberto Giordano 
* Mary Brian as Mary 
* Olive Tell as Duchess of Chatsfield  Sam Hardy as Apollo Beneventa 
* Henry Vibart as Duke of Chatsfield 
* Judith Vosselli as Signora Wembley 
* Fraser Coalter as Lawyer 
* J. Barney Sherry as Papal Secretary 
* Jeanne Carpenter as Flower girl

==References==
 

==Bibliography==
* Koszarski, Richard. Hollywood on the Hudson: Film and Television in New York from Griffith to Sarnoff. Rutgers University Press, 2008. 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 

 