Doctor in Love
 
 
{{Infobox film
| name           = Doctor in Love
| image          =  Doctor in Love quad poster.jpg
| image_size     =   quad film poster
| director       = Ralph Thomas
| producer       = Betty E. Box
| screenplay         =Nicholas Phipps
| based on       =    Michael Craig Leslie Phillips Carole Lesley Joan Sims Bruce Montgomery
| cinematography = Ernest Steward
| editing        = Alfred Roome
| studio         = Rank Organisation
| distributor    = Rank Film Distributors (UK) Governor Films (US)
| released       =  
| runtime        = 
| country        = United Kingdom
| language       = English
| budget         = 
}}
 Michael Craig Doctor in Distress.

==Plot==
Dr Richard Hare is a recently graduated medical intern at St Swithins Hospital. When his new romantic interest, nurse Sally Nightingale, suddenly leaves the hospital, he is devastated. He also leaves after being offered a job in private practice. But when his senior partner, Dr Cardew, has to visit California for a few months, Hare is left in charge. Dr Nicola Barrington joins the practice and Hare is suddenly in love again.

The romance doesnt go well, especially when Sally re-appears and takes the job of practice secretary. Nicola is hurt and stalks off. She is replaced by Dr Tony Burke who proceeds to airily order expensive equipment that the practice cannot afford.

Hare struggles through various comedic and other complications, mainly steming from Burkes amorous attentions to female patients.

After enlisting Sir Lancelot Spratts assistance to save a young dying boy, he diagnoses Spratt with appendicitis and decides to operate, despite Spratts loud objections. He objects even more when Dr Burke fills in at the last moment as the anaesthetist. Despite Spratts vociforous protestations, the operation is a success.

Hare in reunited with Nicola and returns to St Swithins.

==Main cast==
 
* James Robertson Justice as Sir Lancelot Spratt Michael Craig as Dr. Richard Hare
* Leslie Phillips as Dr. Tony Burke
* Joan Sims as Dawn
* Liz Fraser as Leonora
* Virginia Maskell as Dr. Barrington
* Carole Lesley as Kitten Strudwick
* Reginald Beckwith as Wildewinde
* Nicholas Phipps as Dr. Clive Cardew 
* Ambrosine Phillpotts as Lady Spratt
* Irene Handl as Professor MacRitchie
* Fenella Fielding as Mrs. Tadwich
* Nicholas Parsons as Dr. Hinxman
* Moira Redmond as Sally Nightingale Ronnie Stevens as Harold Green Michael Ward as Dr. Flower
* John Le Mesurier as Dr. Mincing Meredith Edwards as Father
* Esma Cannon as Raffia Lady
* Patrick Cargill as Car Salesman
* Bill Fraser as Police Sergeant
* Joan Hickson as Nurse
* John Junkin as Policeman
* Rosalind Knight as Doctor
* Roland Curram as Student Doctor
* Sheila Hancock as Librarian
* Robin Ray as Doctor
* Norman Rossington as Doorman
* Peter Sallis as Love-struck Patient
* Marianne Stone as Nurse Jimmy Thompson as Doctor
* Sally Douglas as Stripper
* Angela Browne as Susan
 

==Reception==
The film was the most popular movie at the British box office in 1960.
==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 