The King Is Alive
{{Infobox film
| name = The King Is Alive
| image = The King Is Alive VideoCover.jpeg
| image_size =
| caption =
| director = Kristian Levring
| producer = Vibeke Windeløv
| writer = Kristian Levring Anders Thomas Jensen
| narrator = David Bradley, David Calder, Jennifer Jason Leigh, Brion James
| music = Jan Juhler
| cinematography = Jens Schlosser
| editing = Nicholas Wayman Harris Nordisk Film Distribution A/S
| released =  
| runtime = 109 minutes
| country = Denmark
| language = English
| budget =
}}

The King Is Alive (2000) is the fourth film to be done according to the Dogme 95 rules. It is directed by Kristian Levring. It was screened in the Un Certain Regard section at the 2000 Cannes Film Festival.   

==Synopsis==
A group of tourists are stranded in the Namibian desert when their bus loses its way and runs out of fuel. Canned carrots and dew keep the tourists alive, but they are helplessly entrapped, completely cut off from the rest of the world. As courage and moral fibre weaken and relationships grow shaky, Henry, a theatrical manager, persuades the group to put on William Shakespeare|Shakespeares tragedy King Lear. As the tourists work their way through Henrys hand-written scripts for an audience of only the sand dunes and one distant, indigenous  watcher, real life increasingly begins to resemble the play.

==Cast==
* Miles Anderson&nbsp;– Jack
* Romane Bohringer&nbsp;– Catherine David Bradley&nbsp;– Henry David Calder&nbsp;– Charles
* Bruce Davison&nbsp;– Ray
* Brion James&nbsp;– Ashley
* Peter Khubeke&nbsp;– Kanana (as Peter Kubheka)
* Vusi Kunene&nbsp;– Moses
* Jennifer Jason Leigh&nbsp;– Gina
* Janet McTeer&nbsp;– Liz Chris Walker&nbsp;– Paul
* Lia Williams&nbsp;– Amanda

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 

 
 
 
 