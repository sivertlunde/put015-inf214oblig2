Labbra serrate
{{Infobox film
| name           = Labbra serrate
| image          = 
| caption        = 
| director       = Mario Mattoli
| producer       = Giulio Manenti
| writer         = Leo Cattozzo Mario Mattoli Aldo De Benedetti Marcello Marchesi
| starring       = Annette Bach
| music          = 
| cinematography = Anchise Brizzi
| editing        = Fernando Tropea
| distributor    = 
| released       = 13 November 1942
| runtime        = 80 minutes
| country        = Italy 
| language       = Italian
| budget         = 
}}

Labbra serrate is a 1942 Italian film directed by Mario Mattoli and starring Annette Bach.    

==Cast==
* Annette Bach - La contessa Lydia Lamsky
* Andrea Checchi - Carlo Massani
* Fosco Giachetti - Ruggero DAnzi
* Vera Carmi - Anna Massani
* Giulio Donadio - Il giudice Massani
* Carlo Campanini - Camillo Pardini
* Tino Scotti - Francesco Ugoletti
* Armida Bonocore - Laura Croci / Antonietta Marradi (as Oliva Egli)
* Nino Pavese - Il direttore del carcere
* Armando Migliari - Il commissario

==References==
 

==External links==
* 

 

 
 
 
 
 
 