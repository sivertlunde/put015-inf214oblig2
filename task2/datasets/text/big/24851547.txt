Space Battleship Yamato (2010 film)
 
{{Infobox film
| name           = Space Battleship Yamato
| image          = Space-Battleship-Yamato-poster.jpg
| caption        = Japanese release poster
| director       = Takashi Yamazaki
| producer       =
| screenplay     = Shimako Sato
| based on       =  
| narrator       = Isao Sasaki
| starring       = Takuya Kimura Meisa Kuroki Toshirō Yanagiba Naoto Ogata Hiroyuki Ikeuchi Maiko Shinichi Tsutsumi Reiko Takashima Isao Hashizume Toshiyuki Nishida Tsutomu Yamazaki
| music          = Naoki Satō
| cinematography = Kozo Shibasaki
| editing        = Ryuji Miyajima
| studio         = Robot Communications
| distributo     = Toho
| released       =  
| runtime        = 138 minutes
| country        = Japan
| language       = Japanese
| budget         = $23.9 million 
| gross          = $49,670,273 
}}
  is a 2010 Japanese science fiction film based on the Space Battleship Yamato anime series by Yoshinobu Nishizaki and Leiji Matsumoto. The film was released in Japan on December 1, 2010. It was released on DVD and Blu-ray in Japan on June 24, 2011, and in the United States on April 29, 2014.

==Plot==
In 2199, after five years of attacks by an alien race known as Gamilas, the Earth Defense Force launches a counter-offensive near Mars. The fleets weapons are no match for the Gamilas, who easily wipe out much of the force. During the battle, EDF captain Mamoru Kodai volunteers to use his damaged ship, the destroyer Yukikaze, as a shield to cover Captain Jyuzo Okitas ship, allowing his escape. Mamorus ship is destroyed.
 battleship Yamato, when an object impacts near him and knocks him unconscious. He awakens to find an alien message capsule. Susumu also notices that the radiation has been reduced to safe levels around him. He is rescued by Okitas returning ship and it is discovered that the capsule contains engineering schematics for a new warp drive and coordinates for the planet from which it came (Iskandar). After learning what happened at Mars, Susumu accuses Okita of using his brother as a sacrificial lamb and tries to hit him, but crewmember Yuki Mori violently stops him.

Okita believes the hope for humanity lies within Iskandar. A request for volunteers for the mission is sent out, and Kodai - a former EDF pilot - decides to reenlist. Their last battleship, the long-dead Yamato, is rebuilt and enhanced with alien technology. Before the Yamato can launch, the Gamilas attack with a gigantic missile. Captain Okita gives the order to fire the yet-untested Wave Motion Cannon, which successfully destroys the incoming missile. Kodai is reunited with his old fighter squad. Yuki, who joined the EDF years ago because of her admiration for Kodai, is bitter towards him.
 brig for disobeying orders. Shima, Kodais former squad-mate, tells Yuki that Kodai left the service because he accidentally caused the death of his own parents and also Shimas wife during a mission.
 hive mind. Kodai stuns the possessed Saito, but the alien apparently is destroyed.

Later, an ailing Captain Okita makes Kodai the acting-captain. The crew discovers the captured Gamilas fighter contains a homing beacon, giving away their position. The Yamato fires its Wave Motion Cannon to destroy a Gamilas ship, but a stealth Gamilas spacecraft latches onto the ships occupied third bridge on the bottom of the hull. Kodai reluctantly orders Yuki, in her fighter, to blast the third bridge support away moments before it detonates, saving the Yamato but killing several crew. Kodai apologizes to Yuki for ordering her to doom their crewmates, and they have an intimate moment as the ship warps again.

The Yamato arrives at Iskandar, but is met by a large Gamilas fleet that sends a spacecraft to obstruct the muzzle of the Wave Motion Cannon. With their main weapon disabled, Kodai makes the dangerous choice to conduct a random warp and ends up at the opposite side of Iskandar. They are surprised to see that it is lifeless, and in fact strongly resembles Earth in its presently irradiated state. It is then discovered that Gamilas and Iskandar are the same planet. The crew thinks it is a trap, but Kodai urges them to go ahead. He leads an attack party to the planets surface against heavy Gamilas opposition. Much of the assault force is killed, and the remaining pilots stay behind to cover for Kodai, Sanada, Saito and Mori as they head for the coordinates.

Once they reach the coordinates, an alien possesses Yukis body and explains that the Gamilas and Iskandar are two aspects of the same race. The alien says their planet is dying and they saw Earth as the most suitable replacement, after first killing off humanity. Iskandar did not agree with this and was imprisoned. Iskandar implants in Yuki the ability to clean the radiation from Earth. As she and Kodai return to the Yamato, Saito and Sanada sacrifice themselves by destroying the Gamilas power source. The Yamato returns to Earth, where Okita dies. The crew rejoices at their return home, but a surviving Gamilas ship ambushes them and severely damages the warship. Dessla now appears and says they no longer wish to invade the Earth; however, since the majority of the Gamilas were killed, he intends to destroy the planet with his ship to avenge his race. Kodai orders the surviving crew to abandon ship before he pilots the Yamato on a kamikaze attack against Desslers ship. He fires the blocked Wave Motion Cannon, which destroys both spaceships.

The ending shows Yuki standing with a child, implied to be Kodais son, on the Earths surface now restored to its original state.

==Cast==
The main cast of characters differ from that of the original series. Yuki has a more active fighting role, and two of the series main male characters – Aihara and Dr. Sado – were recast as women.   

===Yamato Crew===
* Takuya Kimura as Susumu Kodai
* Meisa Kuroki as Yuki Mori, Black Tiger Squadron ace pilot
* Toshirō Yanagiba as Shirō Sanada, chief science/technology Officer
* Naoto Ogata as Daisuke Shima, chief navigator
* Reiko Takashima as Dr. Sado, ship doctor
* Toshiyuki Nishida as Hikozaemon Tokugawa, chief engineer
* Hiroyuki Ikeuchi as Hajime Saitō, Space Commandos leader
*  ) as Aihara, communications officer
* Toshihiro Yashiba as Yasuo Nanbu, tactical unit
* Kazuki Namioka as Saburō Katō, Black Tiger Squadron commander
* Takumi Saito as Akira Yamamoto, Black Tiger Squadron member
* Takahiro Miura as Furuya, Black Tiger Squadron member
* Tsutomu Yamazaki as Capt Jūzō Okita
* Kensuke Ōwada as Kenjirō Ōta, navigator
* Sawai Miyū as Higashida

===Others===
* Shinichi Tsutsumi as Mamoru Kodai, Susumus older brother and captain of the space destroyer Yukikaze
* Isao Hashizume as Heikurō Tōdō, Earth Defense Force commander-in-chief

===Voice cast===
The following are actors who were involved with the original series in any capacity. Kenichi Ogata as Analyzer, robot assistant of Susumu Kodai.
* Masatō Ibu as Dessla, leader of the "evil" side of the Gamilas.
* Miyuki Ueda as Iskandar, originally named Starsha and leader of the "good" side of the Gamilas.
* Isao Sasaki as Narrator.

==Production==
 
TBS Films, the film production arm of the Tokyo Broadcasting System (TBS) television network has been planning the live action film since 2005.  Noboru Ishiguro, director and staff member of the original Space Battleship Yamato television series, confirmed at his Otakon panel on July 17, 2009 that a live action version of Space Battleship Yamato was in development. 
 CGI technology to recreate the space battles from the TV series.  Having been moved by the 3D scenes of Avatar (2009 film)|Avatar, Takuya Kimura, who plays Susumu Kodai, lobbied with Yamazaki to improve the quality of the movies CGI scenes and reshoot them if possible. Because of the danger of getting overbudget, he also agreed to sacrifice part of his talent fee to keep the cost down. 

Erika Sawajiri was originally scheduled to also star in the film as the female lead character Yuki Mori, but was replaced by Meisa Kuroki.  

Principal photography began on October 12, 2009 and was completed by years end. Computer graphics, editing, and other elements of post-production took over nine months before Toho opened the film on December 1, 2010 on 440 screens in Japan. 

==Promotion and distribution== the first the second Space Commandos" (Kuukan kiheitai), were included as well.

 
On September 16, Tokyograph reported that Aerosmiths Steven Tyler wrote a song, titled "Love Lives", for the film based on an English translated script and several clips of the film.  The song is also performed by Tyler and was released on November 24, 2010 a week before the film is released.  A preview of the song is heard in the official trailer.  It was Tylers first solo project.
 Golden Harvest (Peoples Republic of China|China, Hong Kong and Macau), Catchplay (Republic of China|Taiwan), Encore Films (Singapore, Malaysia, Brunei and Indonesia), Funimation (USA & Canada), Nexo Digital (Italy), Manga Entertainment (United Kingdom) and Sahamongkol Film International Co. Ltd. (Thailand).  Mediatres Studio made a deal with TBS Company for home distribution of the film in Spain. The film will be released on DVD in that country under the "Winds of Asia" quality seal and will be sold through Warner Home Video of Spain. 

Different DVD and Blu-ray editions were released in Japan on June 24, 2011. 

Manabu Wakui also wrote the novelization of the film. The book further explains the story behind the 2194 planet bomb attacks and also includes a scene where Shimas mute son, Jiro, finally speaks for the first time upon seeing the Yamato survivors arrival. 

==Box office and reception== Harry Potter and the Deathly Hallows (Part 1). 

Zac Bertschy of the Anime News Network rated the film a B, calling it "a thoroughly modernized adaptation, one that hits the gas pedal right out of the gate and doesnt stop for a moment, unashamed to just try and entertain the hell out of you." 

The Japan Times gave the film a positive review. The reviewer said the pressure of releasing a film with much corporate backing and expectations by die-hard fans put much stress on the production team, but they were able to have sufficient entertaining fare for kids and make serious drama work. 

The Hollywood Reporter stated that the movie had a "sleek futurist look without losing sight of the ‘70s illustration style" of the original series in spite of a small budget (compared with other Western sci-fi movies). 

On the other hand, Christoph Mark of The Daily Yomiuri said the film "lacked gravity" and criticized the production design as too reminiscent of the Battlestar Galactica (2004 TV series)|Battlestar Galactica remake. 

==References==
 

==External links==
*    
*    
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 