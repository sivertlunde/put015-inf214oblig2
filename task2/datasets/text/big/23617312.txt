Killjoy 2: Deliverance from Evil
{{Infobox film
| name        = Killjoy 2
| image       =  
| director    = Tammi Sutton
| producer    = Charles Band
| writer      = Douglas Snaffer
| starring    = Charles Austin Olimpia Fernandez Debbie Rochon Logan Alexander Jermaine Cheeseborough Nicole Pulliam Rhonda Claerbaut Choice Skinner and Trent Haaga
| music       = Jon Greathouse
| distributor = Full Moon Features
| released    =  
| budget      = $30,000
| runtime     = 77 min.
| country     = United States
| language    = English
}} Full Moons hit urban horror movie, Killjoy (2000 film)|Killjoy.

==Plot==
The film begins with a young man named Nic (Charles Austin) being chased by two police officers ( ), Ce-Ce (Nicole Pulliam) and the shy Charlotte Davis (Olimpia Fernandez) are taken by two detention officers, Denise Martinez and Harris Redding (Debbie Rochon and Logan Alexander) to Loxahatchee Canon, where there is a detention center for the delinquents to stay in order for them to pay for their crimes. On the way down, their engine blows in the middle of nowhere, with no cell phone reception, no gas stations nearby and no hope.

The men then head out to get a signal on Reddings cellphone, where they find a house nearby. Ray-Ray breaks into the house to find a working phone when suddenly, a gunshot is heard and Ray-Ray falls down on the ground with a shotgun bullet in his chest. The men are then greeted by a redneck local named Lilly (Tammi Sutton), who threatens to shoot them next if they dont leave. When the group refuses, she points the gun at Eddie and just before she pulls the trigger, Redding shoots her in the head with his gun, who proclaims: "Dont fuck with my kids". Redding tells them to get Ray-Ray back to the van while he searches for a phone. On the way back, they hear another gunshot, warning them that there are more rednecks out there. Believing that Redding is dead, the men quickly head back.

They get back to the van and find out that they dont have a medical kit with them. Warning them about the rednecks, the group head out to find a phone booth. About an hour later, they find another house belonging to a voodoo priestess named Kadja Boszo (Rhonda Claerbaut), who tells them that she can try to help Ray-Ray with her magic, but proclaims that if Ray-Ray chooses to stay alive, hell be alive. If he doesnt, he would be dead. Meanwhile, Ce-Ce tells the group a story that her grandmother told her about, an evil spirit named Killjoy, therefore proving that the events of the previous film occurred. Nic, intrigued and believing that Killjoy can help Ray-Ray, bribes Ce-Ce with cocaine if she can bring Killjoy to life.

After seemingly to have failed, Nic yells at Ce-Ce and she goes to cry about it in an outhouse. Suddenly a pair of teeth chatters its way through the outhouse and off-screen, starts killing her. The teeth chatters its way out and a gloved hand picks it up and puts the teeth back in his mouth, revealing him to be...Killjoy! (Trent Haaga). Killjoy heads back to the house where he bumps into Eddie, who is pumping water out of a water pump. Killjoy, by using his telekinetic powers, lifts Eddie up off the ground and impales him onto the pump. Back inside the house, Nic arrives and gets told that Ray-Ray choose not to stay and now hes dead, and Ce-Ce and Eddie are missing. Nic heads out to go find them and takes Ms. Martinezs gun for protection.

Outside, he runs into Killjoy. Realizing he killed Eddie and Ce-Ce, he shoots him a few times but nothing happens. Killjoy, with his powers, magically makes Nic kill himself with his pocket knife. Ms. Martinez, Charlotte and Kadja, now the only people left alive, now discuss how to kill Killjoy. Kadja tells them that there is a spell to put him away for good, but before that can be accomplished, Killjoy uses his powers to slash Kadjas throat as she makes her way outside to confront him. Killjoy enters the house, knocks out Ms. Martinez and just as hes about to kill Charlotte, Redding comes back, and just as he and Killjoy are about to fight, Charlotte splashed Holy Water on Killjoys face, melting his head and killing him. The next day, Charlotte, Ms. Martinez and Redding are taken out of the wilderness by a ranger (Devin Hamilton).

==Cast==
* Charles Austin (actor) | Charles Austin as Nic
* Wayland Geremy as Officer Donnelly
* Bobby Marsden as Officer White Aaron Brown as File Clerk Logan Alexander as Harris Redding
* Debbie Rochon as Denise Martinez
* Nicole Pulliam as Ce-Ce
* Choice Skinner as Ray-Ray
* Olimpia Fernandez as Charlotte
* Jermaine Cheeseborough as Eddie
* Babalda Francis Cledjo as Security Guard
* Tammi Sutton as Lilly
* Rhonda Claerbaut as Kadja
* Trent Haaga as Killjoy
* Devin Hamilton as Ranger

==External links==
* 
* 

 

 
 
 
 
 