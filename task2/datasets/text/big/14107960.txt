El Paso Wrecking Corp.
{{Infobox film
| name           = El Paso Wrecking Corp.
| image          = El Paso Wrecking Corp. poster.jpg
| alt            =
| caption        = Promotional theatrical poster
| director       = Tim Kincaid  (as Joe Gage) 
| producer       = Sam Gage
| writer         = Tim Kincaid  (as Joe Gage) 
| starring       = Fred Halsted Richard Locke
| music          = Al Steinman
| cinematography = Nick Elliot
| distributor    = Joe Gage Films
| released       = 1978
| runtime        = 64 minutes
| country        = United States
| language       = English
}}
 American gay gay pornographic film written and directed by Tim Kincaid, better known as Joe Gage. It is the second in what has come to be known as his "Working Man Trilogy", which begins with 1976s Kansas City Trucking Co. and concludes with 1979s L.A. Tool & Die. The lead roles are by Richard Locke and Fred Halsted.

== Plot ==

Hank (Richard Locke) and Gene (Fred Halsted) are fired from their trucker jobs after an alcohol-fueled brawl. The two search for opportunities in the blue-collar worker|blue-collar workforce, but are often distracted by other men along the way.

== Cast ==
 
 
* Richard Locke as Hank
* Fred Halsted as Gene
* Beth McDyer as Lil
* Georgina Spelvin as Millie, Roadhouse Owner
* Steve King as Will, as Voyeuristic Man
* Jeanne Marie Marchaud as Voyeuristic Woman
* Stan Braddock as Foreman
* Aaron Taylor as Jim as Man in Car
* Robert Snowden as Wayne as Man in Car
* Keith Anthoni as Boyd, Bike Rider
* Kenneth Brown as Chuck, Stranded Motorist
* Rob Carter as Homer, Mechanic
* Clay Russell as Roy, Park Ranger
* Veronica Compton as Cindi
 
* Guillermo Ricardo as Diego, Gardener
* Lou Davis as Roger, Repairman
* Jared Benson as Seth
* Mike Morris as Mr. Harris
* Hal Dorn
* Tim Kincaid
* Ty Harper
* Elmer Jackson
* Yank Jankowski
* Christian Laage
* Buck Lingren
* Bill Oberfeldt
* Al Yeager
 

== Legacy ==

Adult Video News (2006) included the "Working Man Trilogy" in its list of the top ten most innovative, influential and "hottest" gay pornographic films.  

== Critical reception ==
 TLA Video, in their review of the DVD release, was very favorable towards the film, giving it four stars out of a possible four. 

== DVD release ==

The films comprising the "Working Man Trilogy" were restored and released on DVD by HIS Video. 

== References ==

 

== External links ==

*  

 
 
 