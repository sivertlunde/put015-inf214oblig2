Tooth Fairy, Where Are You?
{{Infobox television film |
  name     =Tooth Fairy, Where Are You? |
  image          =Tooth Fairy, Where Are You.JPG|
  creator        = Paul Schibli |
  director       = Paul Schibli |
  producer       = Paul Schibli Gerald Tripp Sheldon S. Wiseman |
  writer         = Paul Schibli |
  starring       =Lenore Zann Amy Fulco |
  network        =CTV Television Network |
  released=1991 (Canada) |
  runtime        =25 minutes |
  language       =English |
  country        =   Canada |
  budget         = |
  music          = Victor Davies|
  awards         = |
      followed_by = 
}}

Tooth Fairy, Where Are You? is a 25-minute made for TV animated short produced by Lacewood Productions and directed by Paul Schibli.  It was originally broadcast on Canadas CTV Television Network in the year 1991.

== Story ==

The animated short tells the story of Dottie, an apprentice tooth fairy who has trouble learning the requirements in order to graduate from Matildas class.  One night, when all the fairies are out collecting teeth, Dottie receives notice through a "sparkling call" that a little girl named Lori is disappointed because the tooth fairy had not yet visited her house after many nights.  Heartbroken, Dottie wonders what to do and decides that she must go ahead and collect her tooth on her own, or Lori may forever give up believing in fairies.  When she arrives at Loris house, Dottie has a lot of difficulty getting around, partly because she refuses to wear the glasses she needs in order to see well.  Dottie tries the tooth-retrieving "sparkling" spell, but is unsuccessful.  She suffers from a cold, and when she tries to get the tooth on her own, without the aid of her wand, she lets out a big sneeze and wakes up the sleeping girl.  Lori wonders who she is for a minute, but seeing her wings and wand she realizes that the tooth fairy has finally arrived.  Dottie is dismayed and disappointed, knowing that she has broken one of the major rules of the tooth fairies; getting seen by humans is prohibited.  Still, Lori, who is also afraid of wearing her new glasses, allows Dottie to spend the night inside her dollhouse and the next morning, she invites her new friend to come to school with her.  Though Dottie tries to explain that she must return home and practice for her exams, she is touched by Loris pleas and agrees to stay with her the remainder of the week.  During the days the two of them spend together, they develop a strong bond and both are heartbroken when they must say goodbye.  However, though Dottie is scolded afterwards for allowing herself to be seen by a human child, her good heart is rewarded in the end when a classmate, Twinkles, speaks out for her and her friendship with Lori.

== Cast ==

* Lenore Zann - Dottie
* Amy Fulco - Lori
* Leo Leyden - Narrator
* Aline Van Dine - Matilda
* Elizabeth Kevrvorst - Twinkles Rick Jones - Judge, Father
* Amyas Godfrey - Gordon
* Linda Feige - Mrs. Stewart

== External links ==

* 
*  at the Big Cartoon Database

 
 
 