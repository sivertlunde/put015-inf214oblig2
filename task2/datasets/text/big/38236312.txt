Dead Pigeon on Beethoven Street
{{Infobox film
| name           = Dead Pigeon on Beethoven Street
| image          = 
| image_size     = 
| caption        = 
| director       = Samuel Fuller
| writer         = Samuel Fuller
| narrator       = 
| starring       = Glenn Corbett Anton Diffring Christa Lang Stéphane Audran Sieghardt Rupp Alex DArcy Can (as The Can)
| cinematography = Jerzy Lipman
| editing        = Liesgret Schmitt-Klink
| distributor    = Emerson Film Enterprises (U.S.) 
| released       = 1974
| runtime        = 102 min. (theatrical cut) 127 min. (directors cut)
| country        = Germany
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}

Dead Pigeon on Beethoven Street (Tote Taube in der Beethovenstraße) is episode 25, season 1, of Tatort, a 1974 German cop show, directed by Samuel Fuller.  It was given a small theatrical release in America by Emerson Film Enterprises in 1975.

==Overview==
An American private detective is killed in Germany while possessing incriminating photos, and his partner (Glenn Corbett) seeks revenge. Joining with the seductive blonde (Christa Lang) who appeared in the photos, the investigator hopes to discover who was behind them, and why his partner was killed for having them.

==Production History==
Fuller was offered the opportunity to direct an episode of the popular German crime drama by film critic (and later writer/director) Hans-Christoph Blumenberg, in appreciation for the directors help in securing interviews with filmmakers Howard Hawks and John Ford for a documentary project. Upon meeting with the programs producers and feeling initial doubts about being able to conform to the shows standard template, he suggested a storyline inspired by the then-recent Profumo affair in England, which the producers approved to Fullers surprise. 

Fullers screenplay took liberties with the established style of the show by eliminating a primary series protagonist early in the episode in order to introduce a one-time American character to helm the investigation, by conducting the majority of the program in English rather than German (though subtitles were provided in the German broadcast), and by ultimately treating the story with a satirical and often broadly comic tone. 

==Restoration==
In March 2015, the UCLA Film and Television Archive premiered a restored directors cut which added 25 extra minutes previously unseen in its TV broadcast or theatrical release.

==References==
 

==External links==
* 

 

 
 
 
 
 
 


 
 