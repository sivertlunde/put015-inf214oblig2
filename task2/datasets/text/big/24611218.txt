I Start Counting
 
{{Infobox film
| name = I Start Counting
| image =
| caption = David Greene
| producer = David Greene Stanley R. Jaffe Richard Harris
| based on =  
| starring = Jenny Agutter Bryan Marshall
| music = Basil Kirchin Alex Thomson
| editing = Keith Palmer
| distributor = United Artists
| released = November 1970
| runtime = 105 minutes
| country = United Kingdom
| language = English
| budget = 
| gross =
}} David Greene and starring Jenny Agutter in one of her first major roles. It was based on the 1966 novel of the same name by Audrey Erskine Lindop.

The film was moderately controversial because of Agutters squeaky-clean image, her youth (16, playing a 14-year-old), and the films sexual content.   It also included Simon Wards first major film role.

== Plot summary ==
Wynne (Agutter), an adopted 14-year-old girl, has a crush on her 32-year-old stepbrother, George (Bryan Marshall). While spying on George in the bathroom, Wynne notices he has several scratches on his back, and finds a bloody jumper she made for him that he threw in the rubbish, which leads her to suspect him of being the serial killer of several local teenage girls, and who is still at large. Despite this belief, Wynne continues to have romantic sexual fantasies about George, and dreams of marrying him when she comes of legal age.

Wynne hides in Georges minibus, thinking she will catch him in the act of murdering a young girl, but instead she discovers he is having an affair with a suicidal older woman (Lana Morris), whose blood was on the discarded jumper after slashing her wrist in a suicide attempt. 

Wynnes best friend Corinne (Clare Sutcliffe) teases her about her crush on George, and when the family goes out on a picnic Corrinne even flirts with him; he gets angry at her for acting like a "pathetic little mini-tart," and pins her down roughly on the ground. Corrinne runs away from the picnic, and later telephones Wynne to tell her shes staying out all night with a boy she met, who turns out to be the actual serial killer, a bus conductor (Simon Ward). Wynne gets worried about her friend, goes searching for her, and finds Corrinnes dead body. When the bus conductor sees Wynne, he intends to make her his next victim, but the police arrive on the scene in time.

The theme song, also entitled "I Start Counting", was sung by Lindsey Moore.

==Cast==
* Jenny Agutter as Wynne
* Bryan Marshall as George
* Clare Sutcliffe as Corinne
* Simon Ward as Conductor
* Gregory Phillips as Len
* Lana Morris as Leonie
* Billy Russell as Granddad
* Madge Ryan as Mother
* Michael Feast as Jim
* Fay Compton as Mrs. Bennett
* Lally Bowers as Aunt Rene
* Charles Lloyd-Pack as Priest at School
* Lewis Fiander as Priest at Church
* Gordon Richardson as Tramp

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 

 