The Mexican Dream
{{Infobox film
| name = The Mexican Dream
| image = themexicandream.jpg
| caption = Poster
| director = Gustavo Hernández Pérez
| producer = Jan L. Latussek
| writer = Gustavo Hernández Pérez
| starring = Jesús Chuy Pérez Jeff LeBeau Martin Morales Karla Zamudio
| cinematography = Joerg Schodl
| production Designer = Kristrun Eyjolfsdottir
| music = Juan J. Colomer
| editing = Halima K. Gilliam AFI La Frontera Films
| distributor = HBO
| released =  
| runtime = 28 minutes
| country = United States
| language = English / Spanish
}} illegal alien who in many ways represents a modern Don Quixote in pursuit of becoming a movie star.

A fast paced and fresh film that brings together two worlds and slices into one of this century’s biggest social realities; immigration.
The film explores the dreams and delusions, the hopes and desperation of an individual who goes to extremes to give his family a better tomorrow by testing the strength, courage, and passion it takes to follow your dreams.

==Plot==

The Mexican dream is a tragic comedy about the longings and delusions of Ajileo Barajas, a naïve, impulsive, but above all else passionate man, who will stop at nothing to live the dream of becoming a movie star, and to give his family a better life.

The problem is, Ajileo lives in Mexico. To make his dream come true he will have to play the biggest role of his life.  Loaded with madness and desperation, Ajileo comes up with his own master plan.  He dresses up as a woman and decides to cross the border illegally, convinced that if he ever faces the law in the worst case-scenario he will be treated as a lady.

Ajileo journeys through the dangerous desert. An immigrant smuggler: El Coyote; the burning sun; the Border Patrol; and the harsh reality of the big city are just some of the obstacles he will have to tackle to pursue his goal.

Settled down in Hollywood, where everything and nothing is as he dreamed it would be. Ajileo loses his innocence as he discovers that his own odyssey has just begun.

==Cast==
* Jesus Perez as Ajileo Barajas
* Martin Morales as Coyote
* Jeff LeBeau as Caucassian Officer
* Leonard Rodriguez as Latino Officer
* Karla Zamudio	as Ortencia Barajas
* Robby D. Bruce as Cristancio Barajas (as Robby Bruce)
* Shelly Desai as Illegales Driver
* Kevin Planeta as Director 1st session
* Peter Santana	as Producer 1st session
* George McDonald as Producer 2nd Session
* Jenna Chevigny as Casting Director 2nd Session
* Gregory Hughes as Director 2nd session
* Gina DeVettori as Receptionist (as Arianna Nicolette)
* Norman Mora as Mercedes driver (as Norman Mora)
* Joel McDonell as Border Jumper
* Sam Bowen as Man with donut (uncredited)

==Production==
The idea for The Mexican Dream was inspired by thousands of stories of valiant people who have come to the north in search of a better place, and found that their dreams are all they are left with, all that remains. Gustavo Hernández Pérez, writer and director of the film, was moved to tackle this delicate subject after reading some articles that appeared in different independent magazines.  He elaborated a story that could embrace and could relate to anyone who has gone or has to go through a similar situation, but could also reach out to the general audience. He managed to do so by infusing the main character with the basic and common threads that unite immigrants around the world, simplifying the complexity of the matter, humanizing the subject and then exploding into an adventure of the magnitude of The Mexican Dream. 

Jan L. Latussek, the producer, was captured by the project from the beginning. The possibility of making a meaningful film is what moved him to push this film to completion and maintain as much of the initial intention and spirit in it. When "The Mexican Dream" was born, it was already clear who would play Ajileo Barajas, Jesús “Chuy” Perez, a Latino actor who had taken part in a couple of previous short films shot at AFI. Once he was on board, the director and he worked long hours in creating and redefining the character. It was a long, but very useful process. 

From the first draft of the script it was clear that all the locations would be needed. Nor the producer or the director wanted to sacrifice any of them. The script was really tight and everything was there for a reason. It took some time to find some of the locations. It was an odyssey to itself. The desert where most of the border crossing action takes place was not that difficult. The bar and carwash were probably the hardest, especially the car wash. The producers car had never been that clean. Then came the bar, the exterior motel and the interior hallway to “American Dream Productions”. With days taken already by the desert locations and the time constraints the decision was taken to try to find these locations as close to one another as possible.

Two months prior to principal photography, the whole cast was put together and rehearsing had started. The group became like a family working close, and enjoying the growth of the project as something common. In the meantime, the team was also on the search of a production designer that could help with the complicated task of portraying the two opposed worlds in the film. Kristrun Eyjolfsdottir immediately blended into the vision of the team and worked hard to make the most out of the budgetary restrictions that the production was faced with.

The budgetary restraints and the fast-paced production schedule made the team take a very important decision, one that proved to be a very wise one. The shooting format of the film had been the theme of extended conversations, and it was finally decided to shoot on high definition video, or HD 24p as it has come to be known. The latest technology in film-making was made available to cinematographer Joerg Schodl through the AFIs Sony Video center and the Sony Corporation of America. 

On May 28, 2003, after months of preparatory work, the shooting began and the days started to go by. With a very limited crew and working under extreme conditions, the shoot was hard, but made bearable by the spirits of the team; from the PAs to the actors, everyone collaborated to its completion, even under a scalding 105° desert sun.

==Post production==
The help and support given by the people at The Post Group, where the HD online, the color correction, and the few effects were made, was incredible.
The sound post was done at the facilities of AZ productions in Los Angeles. The job done was satisfactory, and ended up giving the film its final dimension.
Juan J. Colomer, composer of the main soundtrack of the short had contributed to bring to live the melodic and more cinematic part of the soundtrack, his great understanding of the pictures dramatic needs and again, the collaboration between Gustavo Hernández Pérez, gave the musical essence to the film.

The need was there to find a musician who would be able to embody in his compositions the whole world that Ajileo represents. Aureliano Méndez Panasuyo in collaboration with actor/singer Jesús Chuy Perez, a song was written for the main title sequence.  This sequence was another fantastic collaboration to the project by Russian animator Marina Ratina. For the director it was very important to create a dynamic title sequence that would not interrupt the flow of the film, but giving instead new information about Ajileo’s world.  After several conversations, several tests, and hard work, Marina brought to life a very powerful animated piece. The results are simply stunning.

==Critical reception==
* Best Latino Director West Coast. DGA Student Film Awards.
* Best Director. Franklin J. Schaffner Fellowship Award. American Film Institute.
* National Finalist. Narrative Short Film. 30th Student Academy Awards.
* Platinum Award. Best Comedy Short Film. 36th Houston World Fest.
* Platinum Award. Best Original Short Film. 36th Houston World Fest.
* Best Narrative Short Film. Fort Lauderdale International Film Festival
* Audience Award Grand Prize. Grinnell College Film Festival. Iowa.
* Best Short Film. 10th Festival Internacional de Cine de Valdivia, Chile.
* Best live Action Short Film. San Francisco World Film Festival.
* Best Screenplay. Miami International Film Festival.
* Best Integral Realization. Philadelphia Short Film Festival.
* 2nd Place. XXVI Festival Internacional del Cine Independiente de Elche. Spain.
* 3rd Place. Narrative Short Film. 30th Athens International Film and Video Festival. Ohio.

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 