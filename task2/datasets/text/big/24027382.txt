Weapons (film)
 
{{Infobox film
| name           = Weapons
| image          = Weapons movie poster.jpg
| image_size     =
| alt            =
| caption        = Weapons official movie poster.
| director       =  Adam Bhala Lough 
| producer       = Robert N. Fried Dan Keston Bill Straus Sol Tryon (executive) Elizabeth Destro (associate)
| writer         = Adam Bhala Lough
| narrator       =
| starring       = Nick Cannon  Paul Dano  Mark Webber  Riley Smith   Brandon Mychal Smith
| music          =
| cinematography = Manuel Claro Jay Rabinowitz
| studio         = Lionsgate
| distributor    =
| released       = 2007
| runtime        = 82 minutes
| country        = United States
| language       = English
| budget         = $500,000
| gross          =
| preceded_by    =
| followed_by    =
}} American teenage crime drama film directed and written by Adam Bhala Lough.

The film premiered in competition at the 2007 Sundance Film Festival and was released straight-to-DVD by Lionsgate in 2009.

== Cast ==
* Nick Cannon as Reggie
* Jade Yorker as Mikey
* Paul Dano as Chris Mark Webber as Sean
* Riley Smith as Jason
* Brandon Mychal Smith as James
* Serena Reeder as Darnelle

== Plot ==
The film starts off in a violent crime committed against Reggie (Nick Cannon|Cannon), who ends up having his head blown off while eating a burger in a fast-food restaurant. After his death, the film opens up in a Pulp Fiction-esque story arc, unveiling why Reggie died in the beginning of the film—and ultimately, who killed him.

=== Acts ===
;Welcome Home
The return of Sean (Mark Webber (actor)|Webber) and his sequential, radical lifestyle—joined by Jason (Riley Smith) and Chris (Paul Dano|Dano).

;Bulletproof
The previous day, Reggie and his sister Sabrina argue over the bruises on her face. She reveals to him that Jason gave her the scars during a rape, forcing Reggie to retaliate. He brings along his friend Mikey, (Jade Yorker|Yorker) and Mikeys younger brother James (Brandon Mychal Smith|Smith), to retrieve a gun from Mikeys distant, irrational uncle (Arliss Howard), solely to kill Jason.

;Im Making A Movie
The night of Sabrinas rape through the eyes of Chris.

;You Were My First
Sabrinas revealed pregnancy the same night of her rape.

;The Funeral
The morning of Jasons funeral, Reggies death, and his killers breakdown.

== Style ==
The style in the film foreshadows the characters. Gritty images and shots within scenes are common, but there are also times where the film becomes bright and clear when it is irrelevant, adding to the plot.

== Budget, equipment and reception ==
{|class="toccolours" style="float: right; margin-left: 1em; margin-right: 2em; font-size: 85%; background:#c6dbf7; color:black; width:28em; max-width: 40%;" cellspacing="5"
|style="text-align: left;"|"The first and last scenes were shot on 35mm, but most of the film was shot on 16mm. The scenes Paul Dano shot are on a PD150. So initially that was how we wanted to cover it for a number of reasons. We were on a super low budget. It was around $500,000. We were shooting in 18 days and practically speaking there wasn’t going to be time to do traditional coverage. I was more interested in doing something aesthetically that lent itself to this type of run-and-gun style."
|-
|style="text-align: left;"|— Adam Bhala Lough 
|}
The films budget estimated $500,000.

Danos character uses a Sony PSR-PD150 in the film for his directorial debut.

The film received extremely negative reviews from the critics. At the review aggressor site Rotten Tomatoes, the film received a "rotten" score of 0% based on nine reviews. However, users seemed to appreciate the film a little bit more, giving the film an overall 54% (which is still a "rotten" rating). 

== Lawsuit and release ==
There was a lawsuit against the revamped works towards the film, including the horrific rape scene and also a scene where Danos character urinates on a fellow party member. In general, the film was delayed because no one contacted Lough on releasing the film to theaters as planned; consequently, Lough teamed up with Lionsgate for releasing the unrated version of the film straight to DVD.

== Music ==
The film is notable for having an all DJ Screw soundtrack (the first of its kind) and an original score by the band International Friends,  originally composed for the film, but inspired by DJ Screw music.

== Awards and nominations ==
{| class="wikitable"
|-
! Year
! Award 
! Category
! Recipient
! Result
|- 2007
|Sundance Film Festival Grand Jury Prize Adam Bhala Lough Nominated
|}

== External links ==
*  

== References ==
 

 
 
 