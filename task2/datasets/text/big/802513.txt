Symbiosis (film)
 
{{ Infobox attraction
| name                = Symbiosis
| logo                =  
| logo_width          =  
| image               =  
| imagedimensions     =  
| caption             = 
| location            = Epcot The Land pavilion
| status              = Closed
| cost                = 
| soft_opened         = 
| opened              = October 1, 1982
| closed              = January 1, 1995
| previousattraction  =  
| replacement         =  
| coordinates         =  
| type                = Theater
| manufacturer        =  
| designer            = Walt Disney Imagineering
| model               = 
| theme               =  
| music               = 
| height_ft           =  
| height_m            =  
| drop_ft             =  
| drop_m              =  
| length_ft           =  
| length_m            =  
| speed_mph           =  
| speed_km/h          =  
| sitearea_sqft       =  
| sitearea_sqm        =  
| gforce              = 
| capacity            = 
 
| vehicle_type        = 
| vehicles            = 
| riders_per_vehicle  =  
| rows                = 
| riders_per_row      =  
| participants_per_group=  
| audience_capacity   =  
| duration            = 
| restriction_ft      =  
| restriction_in      =  
| restriction_cm      =  
| virtual_queue_name  = 
| virtual_queue_image =  
| virtual_queue_status=  
| single_rider        =  
| pay_per_use         =  
| custom_label_1      = Directed by
| custom_value_1      = Paul Gerber
| custom_label_2      = Producer
| custom_value_2      = Sascha Schneider
| custom_label_3      = Assistant Director
| custom_value_3      = Paul Lawrence
| custom_label_4      = Camera man
| custom_value_4      = Marc Wolff
| custom_label_5      = Narrated by
| custom_value_5      = Philip L. Clarke
| custom_label_6      = 
| custom_value_6      = 
| custom_label_7      = 
| custom_value_7      = 
| custom_label_8      = 
| custom_value_8      = 
| accessible          = 
| transfer_accessible = 
| assistive_listening = 
| cc                  = 
}}
 The Land pavilion at Epcot at the Walt Disney World Resort in Lake Buena Vista, Florida. It was directed by Paul Gerber and narrated by veteran voice-actor Philip L. Clarke.
 protection of the environment.  The film showed environmental damage caused by humans and what is being done to fix the damage created.

It closed on January 1, 1995 and was replaced by  .  The new film featured some re-edited footage from Symbiosis.

==Details==
* Grand Opening: October 1, 1982
* Director: Paul Gerber
* Producer: Sascha Schneider
* Assistant Director: Paul Lawrence
*  Director of Photography: Eric Saarinen
* Camera: Marc Wolff
* Narrator: Philip L. Clarke

==See also==
*Epcot attraction and entertainment history

==References==
 

==External links==
* 
*  

 
 
 
 
 
 

 