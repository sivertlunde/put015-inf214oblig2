Armless
 
{{Infobox film
| name           = Armless
| image          = Armless.jpg
| image size     =
| alt            =
| caption        =
| director       = Habib Azar
| producer       = Habib Azar Hsiano Bian
| writer         = Kyle Jarrow
| starring       = Daniel London Janel Moloney Matt Walton Zoe Lister-Jones Laurie Kennedy
| music          = Habib Azar Kyle Jarrow Nathan Leigh Performed by the band Super Mirage
| cinematography = Orson Robbins-Pianka
| editing        = Sarah Smith
| studio         =
| distributor    =
| released       =  
| runtime        = 82 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Armless is a 2010 comedy film directed by Habib Azar and written by Kyle Jarrow, starring Daniel London, Janel Moloney, Matt Walton, Zoe Lister-Jones and Laurie Kennedy.

It was an official selection of the 2010 Sundance Film Festival, as part of the new category NEXT which selects films for their innovative and original work in low- and no-budget filmmaking. 

==Plot==
The film tells the story of John, a man who suffers from a psychological condition known as body integrity identity disorder in which an individual does not feel "whole" unless he loses one or more major limbs. John leaves his wife and goes to New York City to find a doctor to amputate his arms.

==Production==
Armless is Habib Azars first feature film.  It was made in New York City with additional filming in Gillette, New Jersey.  The film was shot in 12 days from March 20 to April 2, 2008.

==References==
 
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 


 