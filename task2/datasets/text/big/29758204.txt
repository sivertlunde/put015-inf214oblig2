Western Cyclone
{{Infobox film
| name           = Western Cyclone
| image          =
| image_size     =
| caption        =
| director       = Sam Newfield
| producer       = Sigmund Neufeld (producer)
| writer         = Patricia Harper (original screenplay)
| narrator       =
| starring       = See below
| music          = Leo Erdody
| cinematography = Robert E. Cline
| editing        = Holbrook N. Todd
| distributor    =
| released       =  
| runtime        = 62 minutes(original version) 39 minutes(edited re-release version)
| country        = United States
| language       = English
| budget         =
| gross          =
}}

Western Cyclone is a 1943 American film directed by Sam Newfield. The film is also known as Frontier Fighters (cut American reissue title).

== Plot ==
On a western land, a governors daughter (Marjorie Manners) is kidnapped and held hostage. Billy the Kid (Buster Crabbe) and Fuzzy Q, Jones (Al St. John) try to rescue her and capture a lot of men known to work for the kidnapper and hold them for questioning. While out on the ranch, Billy and Fuzzy spot a cowboy (Kermit Maynard) who happens to be a henchman with a letter about the kidnapped girl. He refuses to tell who gave him the letter so Billy decides just to find the girl on his own and leaves Fuzzy in charge of tying up the henchman and put him with the rest. Fuzzy tells the henchman to pick up a gun on the floor and when he bends down Fuzzy kicks him and knocks him out. While Billy is looking for the girl, Fuzzy ties up the henchman and takes his boots and socks off to perform an Indian fire torture on the soles of his feet in effort to make him talk, but the man still refuses to tell him who gave him the letter and Fuzzy cant bring himself to burn the henchman. While trying to light a cigar, Fuzzy strokes a match down the sole of the mans foot causing him to burst with laughter. Having found out the henchman was ticklish, Fuzzy grabs his feet and ask him to tell him who gave him the letter. The man once again refuses so Fuzzy begins tickle torturing his bare feet. Fuzzy asked him again and the man still refuses to talk, but the more he refused the more Fuzzy would mercilessly keep tickling the soles of his feet. After enduring brutal tickling on his feet, the henchman cant take anymore and confesses. Fuzzy then tells his friends how the henchman was so ticklish that he got him to talk. Now knowing who send the letter the guys all go to try to rescue the kidnapped girl.

== Cast ==
*Buster Crabbe as Billy the Kid
*Al St. John as Fuzzy Q, Jones
*Marjorie Manners as Mary Arnold
*Karl Hackett as Governor Jim Arnold
*Milton Kibbee as Senator Peabody
*Glenn Strange as Dirk Randall Charles King as Ace Harmon
*Hal Price as Sheriff Hastings
*Kermit Maynard as Ticklish henchman Hank Jack Ingram as Rufe Meeker

== External links ==
* 
* 
 

 

 
 
 
 
 
 
 
 