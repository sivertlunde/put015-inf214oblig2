Kadhal Vaaganam
 
{{Infobox film
| name           = Kadhal Vaaganam
| image          = 
| caption        = 
| director       = M.A.Thirumugham 
| producer       = « Sandow » M.M.A.Chinnappa Devar
| writer         = T.N.Balu
| starring       = M. G. Ramachandran Jayalalitha O.A.K.Devar S.A.Asogan R.S.Manogar Nagesh
| music          = K.V.Mahadevan
| cinematography = N.S.Varma
| editing        = M.A.Thirumugham
| studio         = Devar Films
| distributor    = Devar Films
| released       =  
| runtime        = 
| country        = India
| language       = Tamil
}} 1968 Tamil language drama film directed by M.A.Thirumugham. 
The film features M. G. Ramachandran, Jayalalitha and S. A. Ashokan in lead roles.

==Plot==

In 1968, between Kuala Lumpur and Madras...

A caravan becomes the stake in all the greeds...

Why ? It is necessary to see the movie for the knowledge !

==Cast==
{| class="wikitable" width="72%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
| M. G. Ramachandran || as Sundharam
|-
| Nagesh || as Chellappin
|-
| R. S. Manohar || as Kala s husband
|-
| S. A. Ashokan || as Marimuthu
|-
| Major Sundarrajan || as Pandjachalam
|-
| O.A.K.Devar || as The chief
|-
| Tapist Gopu || as
|-
| Sandow M. M. A. Chinnappa Thevar || as a goon
|-
| J. Jayalalitha || as Vanidha
|-
| Vijayalalitha || as Fancy
|-
| Manorama (Tamil actress)|Manorama|| as Kokila
|-
| C. K. Saraswathi || as Mangalam
|-
| Seethalakshmi || as Kala s mother
|-
| Chandrakanth (Guest-star) || as
|-
|}

The casting is established according to the original order of the credits of opening of the movie, except those not mentioned

==Around the movie==

The film, produced by Sandow M. M. A. Chinnappa Thevar under Devar Films, had musical score by K. V. Mahadevan.

MGR disguises as english woman to free (deliver) the beautiful Jayalalitha from Asogan.
In this scene, the female singer, L.R.Eswari lends her voice to MGR.

MGR is splendidly dressed by the fashion designer(dressmaker) M.A.Muthu.

Regrettably, it was the worst movie of Devar movies and of MGR. KADHAL VAAGANAM held the poster only 56 days, in the time.

But the movie is really worth seeing, if only for MGR and out of curiosity.

KADHAL VAAGANAM was before the last one (black & white movie) of MGR-Chinnappa Devar Combination, 15 / 16 movies Devar Films !

==Soundtrack==
The music composed by K. V. Mahadevan

==External links==

 
 
 
 
 


 