Wings of Defeat
Wings of Defeat is a 2007 documentary feature film in which former Kamikaze pilots reveal they were not fanatics but were ordered to die by a desperate military. Wings of Defeat, broadcast on the PBS Independent Lens series in May 2009, was awarded the 2009 Erik Barnouw Award by the Organization of American Historians.

In Japan, World War II Kamikaze are still revered as self-sacrificing heroes. Internationally, they remain a potent symbol of fanaticism. In astonishingly candid interviews, four former Kamikaze reveal that they were neither suicidal nor fanatical. The film reveals they were young men sentenced to death by a military that could not admit defeat. In heartbreaking testimony corroborated with rare archival footage, they tell us about their dramatic survival and their survivors’ guilt. This riveting, seamlessly edited film is an emotionally charged and timely exposé, probing the responsibilities that a government at war has to its people and its soldiers.
 USS Drexler, a destroyer sunk by two kamikaze pilots. The American veterans illustrate the enduring trauma of the suicide attacks. The film features commentary by John W. Dower, the Pulitzer Prize-winning, American historian of modern Japan, and Emiko Ohnuki-Tierney, author of the books Kamikaze Diaries and Cherry Blossoms, Kamikaze and Nationalism.

== Awards ==
*2009 Winner - Organization of American Historians Erik Barnouw Award
*2008 Winner - Special Jury Award, SF Intl Asian American Film Festival
*2008 Winner - Audience Award, New York Asian American Intl Film Festival
*2008 Winner - Best Documentary, Southern Appalachian International Film Festival
*2008 Winner - Best Director, Southern Appalachian International Film Festival
*2008 Winner - Best Editing, Southern Appalachian International Film Festival
*2008 Winner - Above and Beyond Award, Women Film Critics Circle

== References==
 
*Japan Focus:  
*NPR Story:  
*Japan Times:  
*CNN.com:  
*Reuters: 

== External links==
* 
* 

 
 
 
 
 

 