Highway to Battle
{{Infobox film
| name           = Highway to Battle
| image          = "Highway_to_Battle"_(1960).jpg
| image_size     =
| caption        = British theatrical poster
| director       = Ernest Morris
| producer       = Edward J. Danziger Harry Lee Danziger
| writer         = Brian Clemens Eldon Howard Joseph Pole  (story)
| narrator       =
| starring       = Gerard Heinz Margaret Tyzack Ferdy Mayne Vincent Ball
| music          = Bill LeSage
| cinematography = Stephen Dade
| editing        =  Spencer Reeve
| studio         = Danziger Productions
| distributor    = Paramount British Pictures  (UK)
| released       =  
| runtime        = 71 minutes
| country        = United Kingdom
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
Highway to Battle is a 1961 British thriller film set just prior to the Second World War. 

==Plot==
Before the Second World War, a Nazi party member starts to have misgivings about the Nazis plans. He attempts to defect to England, but is chased by the Gestapo. 

==Cast==
* Gerard Heinz as Constantin
* Margaret Tyzack as Hilda
* Ferdy Mayne as Zeigler
* Dawn Beret as Gerda Peter Reynolds as Jarvost
* Vincent Ball as Ransome
* George Mikell as Brauwitz
* John Gabriel as Carl

:Rest of cast listed alphabetically
* Robert Bruce as Editor
* Robert Crewdson as Newmens
* Hugh Cross as Official
* Jill Hyem as Stewardess
* Cavan Malone as Hoffman
* Bernadette Milnes as Bar Girl
* Richard Shaw as Franz

==References==
 

==External links==
* 

 
 
 
 

 