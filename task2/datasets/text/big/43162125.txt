The Toy Shoppe
{{Infobox Hollywood cartoon|
| cartoon_name = The Toy Shoppe
| series = Oswald the Lucky Rabbit
| image = ToyShoppeColored1934.jpg
| caption = Oswald making the Dutch dolls in the colorized print
| director = Walter Lantz Bill Nolan
| story_artist = 
| animator = Manuel Moreno Lester Kline Fred Kopietz George Grandpre Ernest Smythe
| voice_actor = 
| musician = James Dietrich
| producer = Walter Lantz
| studio = Walter Lantz Productions
| distributor = Universal Pictures
| release_date = February 19, 1934
| color_process = Black and white Colored (1984)
| runtime = 7:05 English
| The County Fair
| followed_by = Kings Up
}}

The Toy Shoppe is a short animated film produced by Walter Lantz Productions, and is one of the many with the character Oswald, the Lucky Rabbit. While the film was originally released in black and white, a colorized version was released in 1984.

==Plot==
Oswald works as a toy maker in his shop where children buy his wares or just look at him through the shops window creating them. A mischievous rat pops out of a hole in a wall, and is startled when he comes upon a toy leopard he thinks is real. The leopard is attacked until sand pours from its wounds onto Oswald who is working under the injured toy. Oswald shoos away the rat by throwing a doll head at the rat.

Later that night, Oswald turns out the lights, closes his shop, and heads to bed above the shop. Alone, two dolls resembling a Dutch boy and a Dutch girl come to life, turn on the lights and start to play music from a turnstile. Other toys start to parade and play. When the Dutch boy and a Dutch girl doll dance near a large box, a jester promptly captures and takes her onto higher shelves. A co-co bird from a nearby clock starts bothering the jester. The jester puts down the girl doll in an attempt to assault the co-co bird but a large flock springs out of the clock and pushes the jester off his perch. The boy doll manages to lure the jester into a vice. The jester strikes at what looks like a dolls head but is actually one knob end of the vice; he is crushed.

After tossing the jester to the floor, the toys celebrate loudly. Oswald is wakened and goes to the workshop and finds all is as he had left it except for the broken jester on the floor. He believes the damage was the work of the rat and ponders how to get rid of the rodent.

==Colorization==
Universal was given a chance to have some of their black and white cartoons colorized. The film The Toy Shoppe was used for the testing. For undisclosed reasons, Universal turned down the colorization offer. {{cite web
|url=http://lantz.goldenagecartoons.com/1934.html
|title=The Walter Lantz Cartune Encyclopedia: 1934
|accessdate=2012-01-11
|publisher=The Walter Lantz Cartune Encyclopedia
}} 

==References==
 

==External links==
*  at the Big Cartoon Database

 

 
 
 
 
 
 
 
 
 
 


 