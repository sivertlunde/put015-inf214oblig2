Undressing Extraordinary
{{Infobox film
| name           = Undressing Extraordinary
| image          =Undressing Extraordinary (1901) - yt.webm
| image_size     = 
| caption        = 
| director       = Walter R. Booth
| producer       = Robert W. Paul
| writer         = 
| narrator       =
| starring       =
| music          =
| cinematography =
| editing        =
| studio         = Pauls Animatograph Works
| distributor    =
| released       =  
| runtime        = 3 minutes 10 secs
| country        = United Kingdom Silent
| budget         =
}}
 1901 UK|British short  silent comedy film, directed by Walter R. Booth, featuring a tired traveller struggling to undress for bed. The film, "provides one of the earliest filmed examples of something that would become a staple of both visual comedy and Surrealist art: that of inanimate objects refusing to obey natural physical laws, usually to the detriment of the person encountering them," and according to Michael Brooke of BFI Screenonline, "has also been cited  as a pioneering horror film," as, "the inability to complete an apparently simple task for reasons beyond ones control is one of the basic ingredients of a nightmare."   

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 


 
 