The Glass Cage (1955 film)
 
 
{{Infobox film
| name           = The Glass Cage
| image          = The Glass Tomb poster.jpg
| caption        = Theatrical release poster
| director       = Montgomery Tully
| producer       = Anthony Hinds Steve Fisher John Ireland
| music          = Leonard Salzedo
| cinematography = Walter J. Harvey
| editing        = James Needs
| studio         = Hammer Film Productions
| distributor    = Lippert Pictures (USA) Exclusive Films (UK)
| released       =  
| runtime        = 59 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
 John Ireland, Honor Blackman and Sid James. It was made by Hammer Film Productions. 

==Plot==
A carnival is interrupted by a couple of murders, causing the police to investigate.

==Cast==
  John Ireland as Pel Pelham 
* Honor Blackman as Jenny Pelham 
* Geoffrey Keen as Harry Stanton 
* Eric Pohlmann as Sapolio 
* Sid James as Tony Lewis 
* Liam Redmond as Lindley 
* Sydney Tafler as Rorke
 

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 
 

 