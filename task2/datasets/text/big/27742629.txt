Model for Murder
{{Infobox film
| name           = Model for Murder
| image          = "Model_for_Murder"_(1959).jpg
| caption        = U.S. poster
| director       = Terry Bishop
| producer       =  Robert Dunbar Jack Parsons  (as C. Jack Parsons)
| writer         = Terry Bishop Robert Dunbar Peter Fraser (story)
| starring       =  Keith Andes  Hazel Court
| music          =  William Davies
| cinematography = Peter Hennessy
| editing        =  Helga Cranston
| studio         = Jack Parsons Productions (as Parroch)
| distributor    = British Lion Film Corporation  (UK)
| released       = February 1959  (UK)
| runtime        = 73 minutes
| country        = United Kingdom
| language       = English
}} British crime film directed by Terry Bishop and starring Keith Andes, Hazel Court and Jean Aubrey. 

==Plot==
American sailor David Martens, on shore leave in England, visits his brother Jacks grave. He meets fashion designer Sally Meadows, who by coincidence works with Jacks ex-fiancee, Diana, a model.

Successful stylist Kingsley Beauchamp and financial backer Madame Dupont own the company where Sally and Diana are employed. Expensive jewels are to be worn by Diana when she models a new dress, but Beauchamp hires two men, Costard and Podd, to break into a safe after hours and steal the gems.

Diana stumbles on the robbery and Costard kills her. He knocks David unconscious and decides to frame him for the theft, crashing a car with David inside, reporting it stolen and placing a ruby bracelet in Davids pocket.

While the police make David a prime suspect, Beauchamp and Costard dispose of Dianas body and the diamonds. They decide to murder David after he suspects them, using gas, but Sally saves him.

Dianas body is found in a river. David goes to Beauchamps home and discovers the missing dress worn by Diana. He knows that Beauchamp is about to fly to Amsterdam, so he hurries to the airport. Costard is there, being double-crossed while Podd smuggles the jewels on Beauchamps behalf. The police arrive in time to arrest all three.

==Cast==
* Keith Andes -  David Martens
* Hazel Court - Sally Meadows
* Jean Aubrey -  Annabelle Meadows
* Michael Gough -  Kingsley Beauchamp
* Julia Arnall -  Diana Leigh
* Patricia Jessel - Madame Dupont Peter Hammond - George
* Edwin Richfield - Costard, Chauffeur
* Alfred Burke -  Podd Richard Pearson -  Bullock George Benson - Freddie
* Diane Bester -  Tessa
* Howard Marion-Crawford -  Inspector Duncan
* Neil Hallett - Sgt. Anderson
* Barbara Archer - Betty Costard
* Annabel Maule - Hospital Sister  Charles Lamb - Lock Keeper

==References==
 

==External links==
* 

 
 
 
 
 

 
 