A Single Life (2014 film)
 
{{Infobox film
| name           = A Single Life
| image          = A-Single-Life-(2014-film)-poster.jpg
| caption        = Film poster
| director       = Marieke Blaauw Joris Oprins Job Roggeveen   
| producer       = 
| writer         = Marieke Blaauw
| narrator       = 
| starring       = Pien Feith
| music          = 
| cinematography =
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 3 minutes
| country        = Netherlands
| language       = English
| budget         = 
}}

A Single Life is a 2014 Dutch animated short film directed by Marieke Blaauw, Joris Oprins and Job Roggeveen. It was nominated for the Academy Award for Best Animated Short Film at the 87th Academy Awards.   

==Plot== Happy Camper featuring Pien Feith. 

According to Oprins, the film took three months to make and was originally inspired by an incident where a record skipped while being played.  The duration of the film was limited because the film was originally intended to be screened at movie theaters prior to the showing of feature length films. 

==Cast==
* Pien Feith as (voice)

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 