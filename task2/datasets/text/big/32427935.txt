Ultraman Saga
{{Infobox film
| name           = Ultraman Saga
| image          = USagapos.jpg
| caption        = Promotional poster
| director       = Hideki Oka
| producer       = Tsuburaya Productions
| writer         = Keiichi Hasegawa DAIGO Takeshi Tsuruno Taiyo Sugiura
| music          = Fumio Hara
| cinematography = Sou Takahashi
| editing        = Akira Matsuki
| distributor    = Shochiku
| released       =  
| runtime        = 90 minutes
| country        = Japan
| language       = Japanese
| budget         =
}}
  is a Japanese  . The catchphrase for the movie is  . The film features Ultraman Zero, Ultraman Dyna, and Ultraman Cosmos as well as the five Ultra Brothers facing a new ultimate threat,   and the monster army created by the evil  . The film also features a new Ultraman known as  . The movie is set in the world of Ultraman Dyna, taking place 18 years after the series and features much of the supporting cast returning. A selection of members from the idol group AKB48 has been chosen to portray the characters of Team-U, a special monster attack team part of the fictional Earth Defense Force. 

In its first week in theaters, Ultraman Saga opened at the #4 spot in the Japanese box offices, about 166,006 people in 369 screens with a budget of ¥143,747,300. 

==Plot==
The film opens with a powerful apocalyptic vision of Tokyo, buildings destroyed and the city seemingly devoid of life. We are soon introduced to the Earth Defense Force (EDF), Team U (played by members of the female idol group AKB48), who go on a shopping raid in a desolate store in the city. During their raid, Team U is attacked by the monster, Earthtron. Despite their setup traps and weapons assisting them, they are no match for the monster. As the monster rampages, Ultraman Dyna appears and quickly destroys Earthtron.

Meanwhile in the alternate universe in which  , Ultraman Zero receives a distress call from an unknown voice as he is battling a hoard of Legionoids and proceeds into the multiverse after making quick work of the robots. Upon arriving to the universe where he received the distress signal, he goes towards the Earth in aid, which is revealed to be the same universe in while 
Ultraman Dyna took place. Shortly before Zero departs, the hosts and Human forms of Ultraman, Seven, Jack, Ace and Leo comment on something unusual taking place in the multiverse, referring to the universe in which Zero is headed for.

Taking place 15 years after Shin Asuka (Ultraman Dyna) disappeared through a black hole at the seriess ending, Earth has since then set up a space station on Mars in Asukas honor and memory. The space station is then attacked by a few Granspheres, only for the invaders to quickly be fended off by a young defense team pilot named Taiga. Shortly after recklessly pursuing the Granspheres, Taiga is trapped (along with a few other Granspheres) inside of a Fortress-like Ship of an invading alien force, that quickly disappears.

Taiga soon finds himself in the apocalyptic version of Tokyo, as Ultraman Zero battles the fleet of spaceships that were released by the Fortress. As Taiga notices a crashing ship heading straight towards a young boy, he pilots his ship directly into its path in an act of self-sacrifice. Witnessing his brave actions, Ultraman Zero rescues Taiga and joins with him. Once Taiga reawakens, he discovers that Ultraman Zero saved him and is not happy to be joined with the Ultra. Trying to dismiss and expel the Ultra from his body, Taiga and the boy are attacked by another monster named List of Ultraman monsters#Gubira|Gubira. As Taiga tries to keep the boy and himself from getting harmed, Gubira is confronted by another Ultraman, Ultraman Cosmos. After a relatively short fight, Cosmos manages to calm down Gubira and the monster leaves the deserted city peacefully.

Shortly after encountering Ultraman Cosmoss human host, Musashi Haruno, both Ultras and the boy named Takeru are picked by Team U and taken to their base, which is revealed to be a warehouse full of children. Taiga and Musashi learn that Team U and the children are the last survivors on planet Earth after the world was invaded by an evil alien known as  .

As Taiga and Musashi bond with Team U and the children, it is revealed that the reason for Taigas general dislike towards Ultraman is due to his parents being killed by accident during one of Ultraman Dynas fights against a monster, thus forcing him to live a very lonely lifestyle growing up. It is also revealed by Team U that Ultraman Dyna and his human host Asuka had once stayed with the team and protected them, but was lost in battle against Alien Bats Zetton. The next day Alien Bat reveals his customized Zetton and what remains of Ultraman Dyna, whom has been turned to stone. With his creation complete, Alien Bat unleashes Zetton onto Team U and the survivors, destroying their shelter and leaving them all in despair. In their depression, Team U is exposed as being frauds, not associated with Earths Defense Force but instead are a group of amateur mechanics who took it upon themselves to protect the surviving children by lying to them. Touched by Team Us bravery, and now realizing the seriousness of the situation this Earth is in, Taiga puts his differences with Ultraman aside and transforms into Ultraman Zero, where he and Cosmos head off to destroy Zetton.

Arriving at the Zettons birthplace, Zero and Cosmos manage to hold off against the monster, but Alien Bats Zetton is too big in size and strength for the combined Ultras to stop him. During the crossfire Cosmos is knocked out of commission, leaving Zero to face the Zetton by himself. With the Ultra fighting Zetton, Team U and Takeru arrive to help him when Takeru digs up the object that can revive Ultraman Dyna, Dynas Reflasher. Despite some interference by Zetton, the Reflasher reunites with the statue of Dyna and Ultraman Dyna is revived to assist Zero and Cosmos, (whom had been revived by Dyna and Zero) in destroying Alien Bats Zetton. However just as it seems the Zetton has been defeated, Alien Bat merges both his fortress and himself with the remains of the Zetton to reveal his creations true form: Hyper Zetton. 

Now with enhanced speed and power, Hyper Zetton easily defeats the three Ultras, leaving the group of heroes hopeless in stopping the crazed alien and his weapon. Unwilling to give up their home and the lives of their friends, Taiga, Musashi, and Asuka merge to form a new Ultraman known as "Ultraman Saga". Ultraman Saga confronts Hyper Zetton, but both Ultra and Monsters abilities are evenly matched with both combatants unable to best each other. Motivated by the Ultra, Team U assists Saga when the battle is taken to the deserted city. Leading Hyper Zetton to a trap that was set for Earthtron, Ultraman Saga takes advantage of the distraction by slicing off Hyper Zettons wings, the source of Hyper Zettons abilities. With Hyper Zetton crippled, Ultraman Saga takes the monster into the Earths Atmosphere and finally destroys it and Alien Bat with an energy-powered punch.

With Alien Bat and his Zetton destroyed, all those abducted by the alien return to Earth to live their lives in peace. The children reunite with their parents, Musashi returns to his home planet on Juran, Asuka returns to his home dimension briefly to inform his old teammates on Taigas status and says that he will wait for them catch up to him before resuming his journey through space, and Taiga remain on Earth to live with Team U. Feeling confident that Taiga is ready to protect the Earth with Team U, Ultraman Zero separates from him and returns to his own universe.

== Cast == DAIGO
*  :  
*  :  
* Team U: AKB48
**  :  
**  :  
**  :  
**  :  
**  :  
**  :  
**  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  

==Ultraman Zero Gaiden==
To promote the film, two special DVDs have been released to lead up to the storyline of the film titled  .  ,  , and   in these DVDs. The film also features  , who turns to good and becomes renamed  , created based on Jean-bot by the evil  , the primary antagonist.
;Stages
#  - November 25, 2011
#  - December 22, 2011

==Theme song==
;Insert theme
*" " 
**Artist: Takeshi Tsuruno

;Ending theme
*  DiVA

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 