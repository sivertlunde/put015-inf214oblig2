Anthaka Mundu Aa Tarvatha
{{Infobox film
| name           = Anthaku Mundhu Aa Taruvatha
| image          = Anthaku Mundu Aa Tharuvatha poster.jpg
| caption        = Movie poster
| director       = Mohan Krishna Indraganti
| producer       = K. L. Damodar Prasad
| writer         = Mohan Krishna Indraganti Madhubala Ravi Rohini Rao Ramesh
| music          = Kalyani Malik
| cinematography = P. G. Vinda
| editing        = Marthand K. Venkatesh
| studio         = Sri Ranjith Movies
| distributor    =
| released       =  {{cite web|url=http://filmcircle.com/anthaku-mundu-aa-taruvatha-movie-review/ |title=
Anthaku Mundu Aa Taruvatha Movie Review }} 
| runtime        =
| country        = India
| language       = Telugu
| budget         =
| gross          =
}} Telugu romance Madhubala made a comeback with this film as Eeshas mother.  The film was Nominated for Best film at the International Indian Film festival in South Africa. 
 Kalyani Koduri and cinematography is by P G Vinda.  It released on 23 August 2013.  The film, which deals about the dynamics of personal relationships before and after marriage, received highly positive critical acclaim and was a hit at the box office completing a successful 50 day run on October 16, 2013. 

==Plot==
Anil (Sumanth Ashwin) is a guy who does not want to settle for an arranged marriage. His mother Sujatha (Rohini (actress)|Rohini) is keen to get him married and he decides to push off to Hyderabad, to escape from the pressure. He has the support of his father (Rao Ramesh). In Hyderabad, he comes across Ananya (Eesha) and falls for her. Ananya is a painter and greeting cards designer and she works for Latha (Jhansi). Anil starts wooing Ananya and she falls for his romantic charms. But Ananya is a little apprehensive about taking the relationship further. Her parents Sridhar (Ravi Babu), a Daily serial creative head and Vidhya (Madhubala), a yoga expert and acting fetish keep having arguments and this worries her. Anil sets a company based on flowers supply.

After Ananya expresses her problems, Anil also has similar worries. “What if our relationship becomes boring and mundane after marriage?” is the question on their minds. They decide to have a secret live-in relationship, to see if they have good compatibility. For that, they rent in a flat for 2 months with a condition that they should not try to impress each other and should be themselves. So their life starts on a slight disturbing note and a night, Ananya experiences Food poisoning during her menstrual periods and Anil, who hates diseases, after losing in a heated argument with her, takes her to Hospital. After returning from the Hospital differences crop up as both argue again. But however both reconcile next morning. After everything seems fine, Anil and Ananya is shocked to find out that they had sex the previous night and are tensed. Fortunately Ananya is found to be out of risk of pregnancy.

Next Day, the couple had to babysit Lathas son because of Lathas departure to America due to some Emergency. Though they find it initially hard, they gel with it after some days. This happens to continue for weeks and Ananya is annoyed as Anil ignores her as he is indulged in the babys care. As Ananya is about to leave Mumbai for a painting exhibition for a week, she comes to know that her mother is going to divorce her father after winning an award for Best Vamp in a serial aired in Sridhars rival channel. Thus after Latha comes back, Anil vacates the flat after the request of Ananya to stop their live-in relationship. Meanwhile Anils father comes to know about the betrayal done to him by Sujatha before their marriage which he shares with Anil.

Anils father loved Shyamala (Pragathi (actress)|Pragathi) but their marriage was opposed by Anils Grandparents. By that time Sujatha, who had a one side love for Anils father coerced Shyamala and her family to vacate the town, paving way for their marriage. This fact was told to him after 25 years by one of Sujathas cousins before he died. Now Shyamala is in a hospital in Hyderabad and the duo go to the hospital and are shocked to find out that the donor of the required liver tissue is Sujatha. She did it as an act of confession to waive the sin of separating them, raising her level of respect and love in the hearts of the Duo. On the other hand Sridhar is filled with sorrow as Vidhya left him but to the surprise of Sridhar and Ananya, Vidya arrives the next morning as she came due to change in Sridhars and her attitudes after Anil spoke to her in private about the life with Ananya spent in a span of 2 months. After gaining acceptance from both families, an emotional Ananya reconciles with Anil.

==Cast==
* Sumanth Ashwin as Anil
* Eesha as Ananya Madhubala as Vidhya
* Ravi Babu as Sridhar
* Rao Ramesh as Anils father Rohini as Sujatha
* Avasarala Srinivas as Anils friend
* Thagubothu Ramesh as Retail Seller Pragathi as Shyamala
* Jhansi as Latha

==Soundtrack==
{{Infobox album
| Name = Anthaka Mundu Aa Tarvatha
| Longtype = To Anthaka Mundu Aa Tarvatha
| Type = Soundtrack Kalyani Koduri
| Cover =
| Released = May 30, 2013
| Recorded = Feature film soundtrack
| Length = Telugu
| Label = Madhura Audio Kalyani Koduri
| Reviews =
| Last album = Adhinayakudu (2012)
| This album = Anthaka Mundu Aa Tarvatha (2013)
| Next album =
}}
 Kalyani Koduri composed the Music for the film. The audio was launched on May 30, 2013 in Hyderabad and received positive response from both critics and audience alike. 

{{Tracklist
| collapsed       =
| headline        = Tracklist
| extra_column    = Artist(s)
| total_length    =
| writing_credits =
| lyrics_credits  = yes
| music_credits   =
| title1          = Gammattuga Unnadi
| lyrics1         = Sirivennela Sitaramasastri
| extra1          = Vedala Hemachandra|Hemachandra, Koganti Deepthi
| length1         = 4:09
| title2          = Hey Kanipettey
| lyrics2         = Anantha Sreeram
| extra2          = Kaala Bhairava, Sravanthi
| length2         = 3:13
| title3          = Thene Mullula
| lyrics3         = Sirivennela Sitaramasastri Kalyani Koduri, Sravanthi
| length3         = 3:52
| title4          = Nenena Aa Nenena
| lyrics4         = Sirivennela Sitaramasastri Sunitha
| length4         = 3:46
| title5          = Thamari Thone
| lyrics5         = Anantha Sreeram
| extra5          = Kalyani Koduri, Sunitha
| length5         = 4:04
| title6          = Ye Inti Ammayive
| lyrics6         = Anantha Sreeram
| extra6          = Hemachandra
| length6         = 4:29
| title7          = Naa Anuragam
| lyrics7         = Indraganti Srikantha Sharma
| extra7          = Kalyani Koduri, Prashanthi Tipirneni
| length7         = 3:08
}}

==Reception==
Anthaka Mundu Aa Tarvatha received positive reviews from critics.
 Oneindia Entertainment gave a review stating "Anthaka Mundu Aa Tarvatha is a wonderful romantic movie, which not only entertains you, but also makes you think. It is a must watch film. Dont miss it" and rated the film 3.5/5.  A reviewer of APHerald.com gave a review stating "Anthaku Mundu Aa Tharuvatha is one film which offers different type of entertainment to the audience in the days of commercial entertainers. Camera work is good too but background music is exceptionally brilliant. The title of the film is apt-it’s a delicious mix of love, humour and romance— all folded in a quit love story. Go grab it" and rated the film 3/5.  Mahesh Koneru of 123telugu.com gave a review stating "Anthaka Mundu Aa Tarvatha is a clean, thoughtful and warm love story. There is a lot of tenderness in the first half and youngsters will connect to it well. The movie is not without its flaws. But classy performances, witty dialogues and some thought provoking sequences end up making AMAT a good watch" and rated the film 3.25/5.  Jeevi of idlebrain.com gave a review stating "Director Mohana Krishna Indraganti who is known for Teluguness and sensibilities has chosen another subject which most of the commercial filmmakers shy away from making. I am giving 3.25 for this film as it’s a good attempt to make a clean and mature film" and rated the film 3.25/5.  Srivathsan Nadadhur of Cinegoer.net gave a review stating "AMAT as a film balances all departments adequately, is engaging and consciously respects the intelligence of a viewer. A worthy watch, in fact, the best in the recent past" and rated the film 3.75/5. 

==References==
 

==External links==
*  

 
 
 
 
 