Thin Ice (1937 film)
{{Infobox film
| name           = Thin Ice
| image          = Thin-ice-1937.jpg
| caption        = Film poster by Joseph A. Maturo
| producer       = Raymond Griffith
| director       = Sidney Lanfield
| writer         = Attila Orbók (novel) Boris Ingster Milton Sperling
| starring       = Sonja Henie Tyrone Power Arthur Treacher Raymond Walburn Joan Davis Sig Ruman
| music          = Mack Gordon Sidney D. Mitchell Lew Pollack Harry Revel
| cinematography = Edward Cronjager Robert H. Planck Robert L. Simpson
| distributor    = Twentieth Century-Fox
| released       =  
| country        = United States
| runtime        = 79 minutes English
}}
Thin Ice (1937 in film|1937) is a United States comedy/romance film directed by Sidney Lanfield starring Tyrone Power and figure skater Sonja Henie.

==Plot==
The plot follows ski instructor Lili Heiser (Henie), who works at a local luxury hotel in the Swiss Alps.  She falls in love with a man who goes skiing every morning (Power).  She thinks hes an everyday tourist, not knowing that hes actually a prince  trying to escape the pressures of royal life.
 1932 and 1936 Winter Olympics, Henie became a professional film actress in 1936.

The film also features Tyrone Power in the beginnings of his career.

The movie was nominated for an Academy Award for Best Dance Direction for the Prince Igor Suite.

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 


 