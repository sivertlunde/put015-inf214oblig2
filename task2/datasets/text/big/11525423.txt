Red Line 7000
{{Infobox film
| name           = Red Line 7000
| image          = Poster of the movie.jpg
| caption        = 
| director       = Howard Hawks
| producer       = 
| writer         = George Kirgo (screenplay) Howard Hawks (story) James Caan Laura Devon Gail Hire Charlene Holt John Robert Crawford Marianna Hill
| music          = 
| cinematography = Milton R. Krasner
| editing        = Stuart Gilmore
| distributor    = Paramount Pictures
| released       =  
| runtime        = 110 minutes
| country        = United States
| language       = English
| gross          = $2,500,000 
}} James Caan, Laura Devon and Marianna Hill in a story about young stock-car racers trying to establish themselves and about the complicated romantic relationships in their lives.

 , appears in a supporting role. Teri Garr appears uncredited as a go-go dancing|go-go dancer in a nightclub.

NASCAR driver Larry Frank helped to film the movie by allowing the film crew to mount cameras on his car. Frank later drove the camera-car in a NASCAR race.

The film features tracks like Daytona International Speedway, Darlington Raceway, and Atlanta Motor Speedway. In this film, it features many crashes from the season, including A.J. Foyts violent crash at Riverside International Raceway earlier in the year.
 red line beyond the safety margin.

==Plot==
A racing team run by Pat Kazarian starts out with two drivers, Mike Marsh and Jim Loomis, but a crash at Daytona results in Jims death. His girlfriend Holly McGregor arrives too late for the race and feels guilty for not being there.

A young driver, Ned Arp, joins the team and also makes a play for Kazarians sister, Julie. A third driver, Dan McCall, arrives from France and brings along girlfriend Gabrielle Queneau, but soon he develops a romantic interest in Holly.

Arp is seriously hurt in a crash, losing a hand. Mike, meanwhile, doesnt care for Dans ways with women and tries to run him off the track in a race, but Dan survives. He and Holly end up together, but Mike is consoled by Gabrielle.

The movie is distinguished by the appearance of a 1965 Shelby GT-350 racing on the track, and one of the characters drives a 1965 Cobra Daytona Coupe as his street car. For Shelby enthusiasts, this is one of the few movies they appeared in.

==Reception==
Quentin Tarantino is a fan of the film: Grand Prix and stuff, but the story isn’t dissimilar. It’s got soap opera with everyone trying to sleep with everyone else, but it’s done in a fun way. It actually plays like a really great Elvis Presley movie. Elvis’ racing movies were good but not this good. I like the way that Red Line 7000 has a community of characters all staying in this Holiday Inn together and hanging out. That’s a cool platform.  

==References==
 

==External links==
*  

 

 
 
 
 
 
 