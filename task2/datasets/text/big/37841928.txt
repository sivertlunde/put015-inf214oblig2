The Fitzgerald Family Christmas
 
{{Infobox film
| name           = The Fitzgerald Family Christmas
| image          = The Fitzgerald Family Christmas.jpg
| caption        = Theatrical release poster
| director       = Edward Burns	 
| producer       = Edward Burns Aaron Lubin William Rexer
| writer         = Edward Burns
| starring       = Kerry Bishé Edward Burns Heather Burns Marsha Dietlein Caitlin Fitzgerald Anita Gillette Tom Guiry  Ed Lauter Mike McGlone Noah Emmerich Connie Britton
| music          = PT Walkley
| cinematography = William Rexer
| editing        = Janet Gaynor Tribeca Film 
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         =  
| gross          =   $50,292 
}}
The Fitzgerald Family Christmas is a 2012 comedy-drama film starring Edward Burns and Connie Britton.  The film, written, directed, and produced by Burns was premiered at the 2012 Toronto International Film Festival. It has been met with positive reviews from critics with 64% on Rotten Tomatoes. 

==Cast==
* Edward Burns as Gerry
* Connie Britton as Nora
* Heather Burns as Erin
* Kerry Bishé as Sharon
* Marsha Dietlein as Dottie
* Caitlin Fitzgerald as Connie
* Anita Gillette as Rosie
* Tom Guiry as Cyril
* Ed Lauter as Big Jim
* Michael McGlone as Quinn
* Noah Emmerich as Francis "FX" Xavier

== References ==
 

==External links==
*  

 

 
 
 
 
 
 
 