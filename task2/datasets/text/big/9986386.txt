The Blue Butterfly
 Blue Butterfly}}
{{Infobox film
| name = The Blue Butterfly
| image =
| caption =
| writer = Pete McCormack
| starring = William Hurt   Pascale Bussières   Marc Donato
| director = Léa Pool
| music = Stephen Endelman Monterey Media
| released =
| runtime = 97 mins.
| language = English
| movie_series =
| awards =
| producer =
| budget =
}}
 Canadian drama Blue Morpho entomologist Alan Osborne, who takes him to the jungles of Costa Rica to find the insect.

The story is based on the life of David Marenger  and his trip with famous entomologist Georges Brossard in 1987. It was filmed on location in Canadas Montreal, Quebec and Central Americas Costa Rica.

==Plot== entomologist Alan Blue Morpho butterfly. However, he dismisses her, but later comes to their home upon receiving a phone call from Pete saying he would go to Central America himself. The two arrive at a small village when they learn the blue morphos have already Insect migration|migrated. Heartbroken, they prepare to leave, until they find one is still in the jungle. They chase after it several times, but are unable to catch it. Pete is determined to find the "magical" butterfly so his cancer can be cured, which leads to them falling into an underground cavern, injuring Osborne badly. Pete escapes to get help, but it only leads to him being lost, suffering from hallucinations. Osborne is rescued, and as they are leaving to a hospital for Osborne, a friendly villager reveals she has caught the butterfly, giving it to Pete. As he is about to kill it for his collection, he lets it go so it can make more of the magical butterflies. An epilogue shows that at the next visit to the doctor, Petes cancer had miraculously disappeared.

==Cast==
*William Hurt as Alan Osborne
*Pascale Bussières as Teresa Carlton
*Marc Donato as Pete Carlton
*Raoul Trujillo as Alejo
*Marianella Jimenez as Alejos daughter

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 


 
 