New in Town
 
{{Infobox film
| name           = New in Town
| image          = New_in_town.jpg
| caption        =  Jonas Elmer Tracey E. Edmonds Paul Brooks Phyllis Laing Andrew Paquin  
| writer         = C. Jay Cox Ken Rance
| starring       = Renée Zellweger Harry Connick, Jr. Siobhan Fallon Hogan J.K. Simmons Frances Conroy
| music          = John Swihart
| cinematography = Chris Seager
| editing        = Troy Takaki Safran Company Edmonds Entertainment Lionsgate
| released       =  
| runtime        = 97 minutes  
| country        = United States
| language       = English
| budget         = $8 million
| gross          = $29,010,817 	 
}} Jonas Elmer, starring Renée Zellweger and Harry Connick Jr. It was filmed in Winnipeg and Selkirk, Manitoba, Canada, and in Los Angeles and South Beach, Miami, Florida, United States|US.     The "making of..." feature on the DVD documents that the cast and crew survived bitterly cold temperatures of below   in Manitoba, which sometimes resulted in malfunctions of cameras and other equipment.

==Plot==
A high-powered consultant (Renée Zellweger) in love with her upscale Miami lifestyle is sent to New Ulm, Minnesota, to oversee the restructuring of a blue-collar manufacturing plant. After enduring a frosty reception from the locals, icy roads and freezing weather, she warms up to the small towns charm, and eventually finds herself being accepted by the community. When she is ordered to close down the plant and put the entire community out of work, shes forced to reconsider her goals and priorities, and finds a way to save the town. After tasting her secretarys secret recipe of tapioca pudding, she decides to adapt a former yogurt production line to produce this special recipe of tapioca.

==Cast==
* Renée Zellweger as Lucy Hill
* Harry Connick Jr. as Ted  Mitchell
* Siobhan Fallon Hogan as Blanche Gunderson
* J.K. Simmons    as Stu Kopenhafer
* Frances Conroy as Trudy Van Uuden
* Hilary Carroll as Kimberley
* Barbara James Smith as Joan
* Nancy Drake as Flo Kopenhafer
* Mike OBrien as Lars Ulstead
* Ferron Guerreiro as Roberta Bobby Mitchell
* James Durham as Rob Deitmar
* Robert Small as Donald Arling
* Kristina Dawson as Clubber

==Release==
The film was released at 1,941 theaters on January 30, 2009 and grossed in its opening day approx. $2.4 million to $2.5 million.  By the end of the first 3-day weekend, it had grossed an estimated $6.75 million, placing it 8th for the weekend in gross box office ticket sales.

==Box office==
The film grossed $16,734,283 at the domestic box office, $12,276,534 at the foreign box office for a total gross of $29,010,817 worldwide. 

==Critical reception==
New in Town received mostly negative reviews from critics. The film has a 29% approval rating on the review aggregator website   of The Guardian was also critical, stating "Renée Zellwegers rabbity, dimply pout – surely the strangest facial expression in Hollywood – simpers and twitches out of the screen in this moderate girly flick that adheres with almost religious fanaticism to the feelgood romcom handbook." 

==Soundtrack==
Songs featured in trailer/TV spots:
* Kelly Clarkson – "Breakaway (Kelly Clarkson song)|Breakaway"
* David Archuleta – "Crush (David Archuleta song)|Crush" Be OK"
* The Kills – "Cheap and Chearful"
* Lenka – "The Show"

Songs featured in the film: 
* Perk Badger – "Do Your Stuff"
* Donavon Frankenreiter – "Move by Yourself"
* APM Music – "Im Movin Out"
* T.Rex (band)|T-Rex – "20th Century Boy" Walking on Sunshine"
* Renée Zellweger – "I Will Survive"
* Crit Harmon – "Boss Of Everything"
* Missy Higgins – "Steer (song)|Steer"
* Elizabeth & The Catapult – "Race You"
* Brittini Black – "Life Is Good"
* Craig N. Cisco – "On The Other Side" Another Country"
* Marty Jensen – "Just Because Were Over"
* Carrie Underwood – "Thats Where It Is"
* Moot Davis – "In The Thick Of It"
* Natalia Safran – "Hey You" (featuring Mikolaj Jaroszyk)

==References==
 
See http://en.wikipedia.org/wiki/Wikipedia:Footnotes for an explanation of how to generate footnotes using the  tags, and the template below. 
 -->
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 