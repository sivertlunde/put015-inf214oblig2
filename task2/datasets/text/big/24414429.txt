Everything's on Ice
{{Infobox film
| name           = Everythings on Ice
| image          = Edgar Kennedy in Everythings on Ice.jpg
| image_size     = 180px
| caption        = Edgar Kennedy in the film
| director       = Erle C. Kenton
| producer       = Sol Lesser
| writer         = Adrian Landis Sherman L. Lowe
| narrator       =
| starring       = 
| music          = Amedeo De Filippi Max Terr
| cinematography = Russell Metty
| editing        = Arthur Hilton
| distributor    = RKO Pictures
| released       =  
| runtime        = 65 minutes
| country        = UNited States
| language       = English
| budget         =
| gross          =
}}
Everythings on Ice is a 1939 American film directed by Erle C. Kenton. The film is also known as Frolics on Ice (American video title).

==Plot summary==
 

==Cast==
*Irene Dare as Irene Barton
*Edgar Kennedy as Joe Barton
*Roscoe Karns as Felix Miller
*Lynne Roberts as Jane Barton
*Eric Linden as Leopold Eddington
*George Meeker as Harrison Gregg Bobby Watson as French
*Mary Currier as Miss Tillifer
*Maxine Stewart as Hat Check Girl
*Wade Boteler as White
*Pierre Watkin as Hotel Manager
*Larry Jackson as Papa Penguin
*Kenny Williams as Dr. Penguin

==Songs==
*"Everythings on Ice", music by Milton Drake and Fred Stryker

==External links==
* 
* 

 
 

 
 
 
 
 
 
 



 