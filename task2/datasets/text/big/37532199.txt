Indonesia Malaise
{{Infobox film
| name      = Indonesia Malaise
| image     = Indonesia Malaise ad.jpg
| image size   = 
| border     = 
| alt      = 
| caption    = Advertisement
| director    =Wong brothers
| producer    =
| writer     = 
| starring    = {{plain list|
*Oemar
*MS Ferry
}}
| music     = 
| cinematography = Wong brothers
| editing    = 
| studio     = Halimoen Film
| distributor  = 
| released    =  
| runtime    = 
| country    = Dutch East Indies Malay
| budget     = 
| gross     = 
}}
Indonesia Malaise is a 1931 film directed by the Wong brothers. It was the brothers first sound film and one of the first such films in the Dutch East Indies. Billed as a comedy, the story follows a woman who is overcome by tragedy in her personal life. A commercial failure, it may be lost film|lost.

==Plot==
A woman is forced to marry a man she does not love instead of her current boyfriend. The marriage goes poorly, as her husband leaves her, their child dies, and her lover is imprisoned. She falls ill but is eventually healed by a mans singing. Meanwhile, a comedian romances a housemaid. 

==Production== ethnic Chinese native audiences.  Only two of its cast members names are recorded, MS Ferry and Oemar. The leading lady may have been a stage actress; the reporter Mohammad Enoh, who viewed Indonesia Malaise as a child, wrote in 1976 that she had been fairly Physical attractiveness|attractive. 
 Boenga Roos dari Tjikembang (Rose from Cikembang) in 1931, although whether it preceded Indonesia Malaise is not certain. These early films had poor sound and lots of static, but through repeated experimentation the quality was eventually brought to acceptable levels. 

==Release and reception==
Indonesia Malaise was released in 1931 and was advertised as a film which would "surely make viewers laugh" ("Penonton tentoe misti ketawa").  It was preceded in screenings by M. H. Schillings Dutch-oriented comedy Sinjo Tjo Main di Film; this is likely to have been an effort to draw Dutch audiences as well. 

The film performed poorly, perhaps because audiences did not want a reminder of the ongoing   (1932), was made on contract. 

The film is likely lost film|lost. The American visual anthropologist Karl G. Heider writes that all Indonesian films from before 1950 are lost.  However, JB Kristantos Katalog Film Indonesia (Indonesian Film Catalogue) records several as having survived at Sinematek Indonesias archives, and Biran writes that several Japanese propaganda films have survived at the Netherlands Government Information Service. 

==Footnotes==
 

==Works cited==
 
*{{cite book
 |title= 
 |trans_title=History of Film 1900–1950: Making Films in Java
 |language=Indonesian
 |last=Biran
 |first=Misbach Yusa
 |author-link=Misbach Yusa Biran
 |location=Jakarta
 |publisher=Komunitas Bamboo working with the Jakarta Art Council
 |year=2009
 |isbn=978-979-3731-58-2
 |ref=harv
}}
*{{cite book
 |url=http://books.google.ca/books?id=m4DVrBo91lEC
 |title=Indonesian Cinema: National Culture on Screen
 |isbn=978-0-8248-1367-3
 |author1=Heider
 |first1=Karl G
 |year=1991
 |publisher=University of Hawaii Press
 |location=Honolulu
 |ref=harv
}}
* {{cite web
  | title = Indonesia Malaise
  | language = Indonesian
  | url = http://filmindonesia.or.id/movie/title/lf-i016-31-404203_indonesia-malaise
  | work = filmindonesia.or.id
  | publisher = Konfidan Foundation
  | location = Jakarta
  | accessdate = 22 July 2012
  | archiveurl = http://www.webcitation.org/69LaqbqS9
  | archivedate = 22 July 2012
  | ref =  
  }}
 

 
 

 
 
 
 