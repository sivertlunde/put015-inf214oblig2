Raanjhanaa
 
 

{{Infobox film
| name           = Raanjhanaa
| image          = Raanjhanaafilmposter.jpg
| alt            =
| caption        = Theatrical release poster
| director       = Anand L. Rai
| producer       = Krishika Lulla 
| writer         = Himanshu Sharma
| starring       = Dhanush Sonam Kapoor Abhay Deol
| music          = A. R. Rahman
| lyrics         = Irshad Kamil
| cinematography = Natarajan Subramaniam Vishal Sinha
| art direction  =  Tariq Umar Khan
| editing        = Hemal Kothari
| studio         = Colour Yellow Pictures
| distributor    = Eros International
| released       =   (Hindi)   (Tamil)   
| runtime        = 140 minutes Hindi
| budget         =  
| gross          =   
}}
 2013 Cinema Indian Romance romantic drama film, directed by Anand L. Rai and written by Himanshu Sharma. The film is produced by Krishika Lulla under the banner Eros International. It stars Dhanush, in his Bollywood debut, Sonam Kapoor and Abhay Deol in lead roles.  The film was released on 21 June 2013 worldwide,  while the dubbed Tamil version titled Ambikapathy  was released a week later.   The dialogues for the Tamil version was written by John Mahendran.
 Hindi lyrics Tamil lyrics were written by Vairamuthu.

Within a week of release, the films collections reached   domestic nett.    The film was declared a hit by Box Office India.    Post four weeks of its run, the film grossed over   worldwide.   

==Plot==
Kundan Shankar (Dhanush) is the son of a Hindu. Since his childhood, Kundan has had a strong and obsessive one-sided love for Zoya Haider (Sonam Kapoor), a Muslim girl residing in Varanasi. He always attempts to be around her but gets slapped several times in the process. Zoya finally changes her mind because of Kundans consistency of affection. Her family is orthodox, preventing the lovers from being together. After Zoya moves to Aligarh for further studies, Kundan harmonizes with Zoyas family, assisting them with chores. Zoya gets into Jawaharlal Nehru University and discovers a long-lost strength as she confronts and then falls for student leader Akram Zaidi (Abhay Deol).

After eight years, Zoya returns to Varanasi, barely recognizing Kundan when she meets him again. Kundan again attempts to ask Zoya for her love but she is reluctant, revealing that she is in love with Akram. She urges Kundan to convince her family about her marriage with Akram. Kundan eventually agrees and gets their wedding arranged, promising Zoya that he will also marry someone else and forget her. He proposes to his childhood friend Bindiya (Swara Bhaskar), who has had a crush on him since childhood.

On the day of Zoyas marriage, Kundan finds out that Akram is a Sikh, whose actual name is Jasjeet Singh Shergill. This enrages him, as Zoya had used their religious differences to convince him that their match was not possible. He crashes the wedding ceremony and reveals the truth to Zoyas parents. Kundan is later informed that Zoya tried to commit suicide, and Jasjeet was mercilessly beaten by Zoyas relatives. At the hospital, Jasjeet tells Kundan that Zoya convinced him to portray himself as Muslim to marry her, a plan he should not have agreed to. While with Jasjeet, Kundan forgets about his own marriage ceremony with Bindiya, taking place that day, and is too late when he returns. Hurt and angry, Kundans family disowns him. Jasjeets parents take him back to his village. Kundan tries to redeem himself by taking Zoya to Jasjeets village. There, he is devastated to find that Jasjeet has succumbed to his injuries, finally understanding that his one-sided love has resulted in an innocent mans death.

Kundan becomes a homeless vagrant and begins visiting various religious places and volunteering in their activities so that he can atone for his sins. During one such visit, a man advises him to do the right thing rather than run away from his sins. Motivated, Kundan locates Zoya to her university, where she is spearheading the political party "All India Citizen Party" (AICP), that had been created by Jasjeet. Kundan joins the nearby canteen and serves the party, while trying to get Zoyas attention. Over time, he becomes popular with the party due to his simple nature. Theyre also impressed when Kundan helps them in negotiating some tough situations by using his simplistic witty tricks. However, Zoya holds a grudge that the person who is responsible for Jasjeets death is now taking his place. She tries to instigate party members against Kundan but Jasjeets sister Rashmi (Shilpi Marwaha) tells Kundan to continue, as he is the best choice for fulfilling Jasjeets vision.

Kundan, however, only wants Zoya to forgive him. The Chief Minister tells Zoya that she must get revenge on Kundan. To get back Jasjeets position, she tells Zoya to let Kundan get injured as he delivers his campaign speech. Zoya apparently seems to get brainwashed by this plan and as the Chief Minister desired, Kundan is hurt badly and sent to ICU, thus fulfilling Zoyas revenge.

At a press meet, the Chief Minister denies having any hand in the blast that hurt Kundan. However, Zoya steps forward and reveals that the plan was plotted by her and the Chief Minister and that she is prepared to go to prison for this. Zoya then finds out that Kundan was aware of the plot and still let himself be injured. Shocked, she rushes to the hospital to be with him.

In a final voiceover, a dying Kundan wonders about letting go. He says he might have the desire to live again if Zoya called out to him, but then again, everything has become so tiring, he would rather let go and rest (euphemism for dying). He says that he can be born again and again in the same Benaras, fall in love with a girl like Zoya again, and become a Raanjhanaa in her love.

==Cast==
*Dhanush as Kundan Shankar
*Sonam Kapoor as Zoya Haider
*Abhay Deol as Jasjeet Singh Shergill/Akram Zaidi
*Swara Bhaskar as Bindiya
*Mohammed Zeeshan Ayyub as Murari
*Shilpi Marwaha as Rashmi
* Suraj Singh as Anand
* Kumud Mishra as Zoyas Father/Guruji
* Saniya Anklesaria as Zoyas childhood
*Arvind Gaur as Guptaji (cameo)
* Sujata Kumar as Chief Minister
*Vipin Sharma as Kundans father (cameo)
* Ishwak Singh (Doctor from Lucknow)
* Tejpal Singh (Father of Abhay Deol)
* Nisha Jindal (Student of JNU)
* Late Ginny Singh (JNU activist) 

==Production==

===Development=== Punjab and Chennai, and that actor Abhay Deol would be seen in a special appearance in the film.  Urmila Sharma, well known for her Hindi TV serial roles was signed to play the character of Kundans mother in the film.  Initially, Aditi Rao Hydari was supposed to play the role of Kundans childhood friend but she opted out due to lack of availability, being replaced by Swara Bhaskar. 

===Characters===
Actor Dhanush plays the character Kundan who has a passion for his city Banaras and Zoya . It depicts him a young boy and a teen who turns into a sensitive adult. Sonam Kapoor quoted her character as, "Zoya is childlike and unpredictable. She can be cold and at the same time, objective. She has every quality that makes her desirable to a man." In an interview, Kapoor revealed that for playing the role of a school girl in the film, she drew inspiration from the character Jaya Bachchan played in the 1971 film Guddi (1971 film)|Guddi.  Actor Abhay Deol as Akram plays a secure yet confident university student, socialist and a budding politician.   

===Filming===
After main casting announcements, filming was substantially delayed; the reason was speculated to be composer A. R. Rahmans music being denied outright by the film director.    Filming officially began in Varanasi, India in early September 2012 and continued for 40 days in and around the city. As per reports, the leading duo were also seen playing the role of 17-year-old teenagers.  In mid-September 2012, the schedule of filming was put on hold as actor Dhanush fell ill on the sets in Varanasi.  While filming in October 2012, the actor injured his shoulder during the filming of an action sequence for his Tamil outing Maryan (film)|Maryan. The consequences led to the scheduled shooting of dance sequences to be postponed and were shot in Varanasi on 19 December 2012.   On 4 November 2012, Sonam Kapoor and Abhay Deol shot the song "Tu Mun Shudi" at India Gate, Delhi,  and their dialogue scenes were shot at the Indian Institute of Mass Communication campus in Delhi by early December 2012. Permission to shoot inside was denied to the director by the college authorities. So, the unit shot some scenes in Amity International School, Noida. To avoid footage leaks, over a hundred potential crew members were made present at the filming venue. Certain filming was also done in Gurgaon and Faridabad.    The title track of the film was shot on 27 December 2012.    The final schedule of filming began in Delhi on 7 January 2013.  In March 2013, the films crew shot several scenes at the Pataudi Palace in Haryana over two days. 

==Soundtrack==
  Eros Music  on 31 May 2013 and the Tamil version on 19 June 2013. The soundtrack received positive critical reception on release.

==Marketing== Radio Mirchi Studios in Mumbai on 27 May 2013.   Dressed in typical South Indian attire, Dhanush and Sonam Kapoor promoted the Tamil version of Raanjhanaa in Chennai. The leading duo promoted their film in Mumbai, Delhi, Ahmedabad, Lucknow and Jaipur from mid-May to June 2013. 

==Release==
The first look of the characters in the film was revealed as a poster with no credits and film name on the day of Holi 2013.   The first theatrical trailer was released on 24 April 2013.     The Hindi version of the film released worldwide on 21 June 2013 with the estimated number of release screens being 1,000. The film opened to an occupancy of 50–55%, the highest compared to other Bollywood films that released on the same date. 

===Critical reception===
;India
Critic Komal Nahta responded positively and said, "On the whole, Raanjhanaa is an interesting, entertaining and a fairly different love story. It is like heady wine and its effect will only grow."  Rajeev Masand of CNN-IBN wrote, "For its immensely entertaining first half, a winning score by AR Rahman, but most of all for Dhanush, this is a film thats worth your time. Im going with three out of five for Raanjhanaa. Its not perfect, but itll do."  Resham Sengar of Zee News gave the film 4 out of 5 stars and summarised "Raanjhanaa is a love story that does not fall within the confines of a clichéd Bollywood romance."  Taran Adarsh of Bollywood Hungama gave the film 3.5 out of 5 and stated, "On the whole, Raanjhanaa encompasses romance and myriad emotions most wonderfully, besides bravura performances and a popular musical score from the maestro." Adarsh also called it "a film that touches the core of your heart" and said it was "definitely worthy of a watch".  At NDTV, Saibal Chatterjee gave it 3.5/5 and opined in the review, "The film defies the expectations of the audience at several crucial junctures and holds out absolutely no apologies for springing abrupt surprises. A love story with a huge difference that benefits no end from a clutch of exceptional performances."  Sukanya Verma of Rediff|Rediff Movies gave 3 out of 5 stars and claimed, "Raanjhanaa isnt easy viewing. Kundan and Zoya arent easily likeable. They have flaws. They make mistakes. Blunders, really but Rai shows them for what they are; he never paints a pretty picture. And this brutal honesty coupled with a commanding Dhanush is what works."  Meena Iyer of The Times of India claimed, "Raanjhanaa is a love story that has a Shakespearean touch and is mounted on a lavish scale". She noted, "You may not like this film if you cannot digest brooding love stories", and gave it 3.5 out of 5.  Kaushik Rmesh of Planet Bollywood gave the film an 8 on 10 and summarised, "A realistic romance that brims with impressive elements (including and especially the enchanting music), Raanjhanaa is surely a winner at the end and must be watched for its unconventional handling and freshness". 

Nabanita of   rated the film 3/5 and judged, "Raanjhanaa harks back to the way Bollywood used to make love stories once upon a time. With some imagination, the effect would have been nostalgic, too."  Shubha Shetty Saha of Mid-Day assigned 3.5/5 to the film and praised actor Dhanush and stated, "And then the second half is when the pace dips, the sincerity of the storyline gets somewhat hazy and the film gets wee bit disappointing. An absolutely believable one-sided romance takes a slightly deceptive political drama twist and I am not sure if that is what you wanted it to be. It is unpredictable, yes, but not in a great, believable way."  At Mumbai Mirror, Karan Anshuman pointed, "Raanjhanaa flows like good poetry. It is arguably the best love story of the year so far, the kind of film others in the genre should aspire.". 

On the contrary to above, critic Mayank Shekhar wrote, "His (Dhanush) character is supposed to be gifted with great inter-personal skills. It doesnt quite show."  Shubhra Gupta of The Indian Express gave it 2.5 out of 5 and mentioned, "Raanjhanaa is a film which is all of a piece in its engaging first half, and a good Bollywood launchpad for Dhanush. Makes me want to see what he will do in his second pass."  At Emirates 24/7, Sneha May Francis said, "While music maestro AR Rahman tunes the track, and leaves us occasionally cheerful, the moments are far too few to erase the horrid after effects of this movie."  Critic Manohar Basu at Koimoi stated, "However a sluggish screenplay and lurching script makes Raanjhanaa a half baked effort and hence gets a 3/5 from me."  Sudhish Kamath of The Hindu concluded, "A dream debut for Dhanush even if the film gets stuck in its messy political subtext that kills the romance." 

;Overseas
At The Hollywood Reporter, Lisa Tsering left the film unrated and asserted, "The fact that the film marks the Hindi-language debut of South Indian star and YouTube superstar Dhanush is bound to draw interest at the box office, though Rais firm refusal to play by the rules of the typical Bollywood love story may make it hard to sustain momentum." 

===Controversy=== Central Board of Film Censors (CFBC) banned the film before its scheduled release in Pakistan. chief executive officer of IMGC Global Entertainment Amjad Rasheed, the importer of Raanjhanaa, revealed that he received a letter from the CBFC with directives to shelve the films release which stated that the film portrays an inapt image of a Muslim girl (played by Sonam Kapoor) falling in love with a Hindu man and having an affair with him. 

==Box office==

===Domestic=== CP Berar, Central India and states like Uttar Pradesh, Bihar and Rajasthan.  On its opening day, it grossed more than 500,000 in the city of Lucknow itself.  On Saturday, a day just after release, collections rose to  . The film had a growth rate of 40–45%,  making its total first weekend collection  .    Within a week of its release, its gross surpassed   and Box Office India declared it a "Hit".  After two weeks of release, the film had collected   at the box office.    Raanjhanaa also had the fourth-highest second week collections of the year 2013. {{cite web|url=http://www.boxofficeindia.com/boxnewsdetail.php?page=shownews&articleid=5835&nCat=| title =
Top Second Week Collections 2013: RAANJHANAA FOURTH| work =Box Office India|accessdate=6 July 2013}}  It grossed Rs 75&nbsp;million in its third week, taking its total domestic overall collections to   and worldwide collections nearing to a remarkable mark and becoming the second highest-grosser of 2013 at the time of its release.     The film ended its run with an estimated  . 

===Overseas=== UAE and $46,000 in Australia.  Raanjhanaa also grossed $1.55 million in ten days.  Further, the film receded its total collections and dropped around $1.8 million by 17 July 2013. 

==Awards and nominations==
::Note &ndash; The lists are ordered by the date of announcement, not necessarily by the date of ceremony/telecast.
{| class="wikitable" style="width:99%;"   
|+ Positive awards and nominations 
|-
! width="23%" scope="col" | Distributor
! width="15%" scope="col" | Date announced
! width="25%" scope="col" | Category
! width="23%" scope="col" | Recipient
! width="8%" scope="col" | Result
! width="2%" scope="col" | Reference

|-
| rowspan="3" |  BIG Star Entertainment Awards 
| rowspan="3" |  31 December 2013 
|  Most Entertaining Romantic Film 
|  Anand L. Rai 
| rowspan="3"  
| align="center" rowspan="3" |   
|-
| Most Entertaining Debut Actor - Male 
|  Dhanush 
|-
| Most Entertaining Music 
|  A. R. Rahman 
|-
| rowspan="11" |  Screen Awards 
| rowspan="11" |  14 January 2014 
| Best Actor in a Supporting Role (Female) 
|  Swara Bhaskar 
| rowspan="1"  
| align="center" rowspan="11" |    
|-
|  Best Actor (Popular Choice) 
| rowspan="2" |  Dhanush 
| rowspan="16"  
|-
| Best Actor (Male) 
|-
| Best Actor in a Supporting Role (Male) 
|  Mohammed Zeeshan Ayyub 
|-
| Best Actor (Popular Choice) 
|  Sonam Kapoor 
|-
| Best Cinematography 
|  Natarajan Subramaniam  Vishal Sinha 
|-
| Best Music 
| rowspan="2" |  A. R. Rahman 
|-
| Best Background Score 
|-
| Best Costume 
|  Payal Saluja 
|-
| Best Sound Design 
|  Arun Nambiar 
|-
| Production Design 
|  Wasiq Khan 
|-
| rowspan="6" |  Apsara Film & Television Producers Guild Award 
| rowspan="6" |  16 January 2014 
|  Best Actor in a Supporting Role 
|  Abhay Deol 
| align="center" rowspan="6"|   
|-
| Best Screenplay 
|rowspan="3" |  Himanshu Sharma 
|-
| Best Story 
|-
| Best Dialogue 
|-
|  Best Music 
|  A. R. Rahman 
|-
| Best Performance in a Comic Role 
| Mohammed Zeeshan Ayubb  Swara Bhaskar 
|- ETC Bollywood Business Awards 
| rowspan="1" |  18 January 2014 
|  Highest Grossing Debut (Male) 
|  Dhanush 
| rowspan="2"  
| align="center" rowspan="1"|   
|- Idea Filmfare Awards 
| rowspan="6" |  20 January 2014 
|  Best Debut (Male) 
|  Dhanush 
| align="center" rowspan="6" |      
|-
| Best Director 
|  Anand L. Rai 
| rowspan="9"  
|-
| Best Film 
|  Anand L. Rai  
|-
| Best Actor in a Leading Role (Female) 
|  Sonam Kapoor 
|-
|-
| Best Actor in a Supporting Role (Female) 
|  Swara Bhaskar 
|-
| Best Music 
|  A. R. Rahman 
|-
| rowspan="4" |  Global Indian Music Awards (GiMA) 
| rowspan="4" |  20 January 2014 
|  Best Engineer (Film Album) 
|  R. Nitish Kumar 
| align="center" rowspan="4" |   
|-
| Best Background Score 
|rowspan="2"|  A. R. Rahman 
|-
| Best Film Album 
|-
| Best Playback Singer (Female) 
|  Shreya Ghoshal    

|-
| rowspan="7" |  Filmfare Awards 
| rowspan="7" |  26 January 2014 
|  Best Film 
|  Eros International - Krishika Lulla 
| rowspan="6"  
|-
| Best Director 
|  Anand L. Rai 
|-
| Best Actor 
|  Dhanush 
|-
| Best Actress 
|  Sonam Kapoor 
|-
| Best Supporting Actress 
|  Swara Bhaskar 
|-
| Best Music Director 
|  A. R. Rahman 
|-
| Best Male Debut 
|  Dhanush 
| rowspan="1"  
|-

|-
| rowspan="2" |  International Indian Film Academy Awards 
| rowspan="2" |  26 April 2014 
| Best Actress In A Supporting Role 
|  Swara Bhaskar 
| rowspan="1"  
|-
| Male Debutant Star 
|  Dhanush 
| rowspan="1"  
|-
|}

==References==
 

==External links==
* 
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 