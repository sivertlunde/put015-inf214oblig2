Rebellion (2011 film)
 
{{Infobox film
| name           = Rebellion
| image          = Rebellion (2011 film) film poster.jpg
| caption        = Film poster
| director       = Mathieu Kassovitz
| producer       = Mathieu Kassovitz Christophe Rossignon
| writer         = Benoît Jaubert Pierre Geller Mathieu Kassovitz
| starring       = Mathieu Kassovitz
| music          = Klaus Badelt
| cinematography = Marc Koninckx
| editing        = Thomas Beard Lionel Devuyst Mathieu Kassovitz
| distributor    = 
| released       =  
| runtime        = 135 minutes
| country        = France
| language       = French
| budget         = 
}}
 historical drama film directed, produced, co-written, co-edited by and starring Mathieu Kassovitz.     Set in New Caledonia but filmed in Tahiti, the film recreates a version of the Ouvéa cave hostage taking in 1988.  Kassovitz, Benoît Jaubert and Pierre Geller were collectively nominated for the 2012 Best Writing (Adaptation) César Awards 2012|César Award.

==Plot==
In a French colony 30 policemen are taken hostage by local separatists. Captain Philippe Legorjus is sent out to negotiate with rebel leader Alphonse Dianou. With him come 300 French soldiers who stand ready in case his efforts for a peaceful solution should fail.

==Cast==
* Mathieu Kassovitz as GIGN Captain Philippe Legorjus
* Iabe Lapacas as Alphonse Dianou
* Malik Zidi as JP Perrot
* Alexandre Steiger as Jean Bianconi Daniel Martin as Bernard Pons
* Philippe Torreton as Christian Prouteau
* Sylvie Testud as Chantal Legorjus
* Steeve Une as Samy
* Philippe de Jacquelin Dulphé as Brigadier Vidal
* Patrick Fierry as Colonel in the Dubut Army
* Jean-Philippe Puymartin as the commandant of the Gendarmerie Jérôme
* Stefan Godin as Lieutenant Colonel of the Gendarmerie Benson (as Stéfan Godin)

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 


 
 