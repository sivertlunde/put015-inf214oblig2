Gallivant
 

Gallivant is the first feature-length movie by Andrew Kötting. Released in 1996, it was a "highly idiosyncratic" documentary. It recorded a journey the director took clockwise around the coast of Britain accompanied by his 85-year-old grandmother, Gladys, and his seven-year-old daughter Eden. Eden was born at Guys Hospital, London, in 1988 with a rare genetic disorder – Joubert syndrome – causing cerebral vermis hypoplasia and several other neurological complications. The growing closeness between these two and the sense of impending mortality give the film its emotional underpinning.

==Reception== Time Outs 100 best British films.

==External links==
 
*  

 
 
 
 
 
 
 


 