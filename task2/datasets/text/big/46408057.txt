Up All Night (2015 film)
{{Infobox film
| name           = Up All Night
| image          = Up All Night Theatrical Release Poster.jpg
| alt            = "Up All Night" movie poster
| caption        = Theatrical release poster
| film name      =   John Henderson
| producer       = Jeremy Salsby Jules Elvins Dan Shepherd
| writer         = Toby Davies
| based on       = Friday Download
| starring       = Dionne Bromfield Shannon Flynn Bobby Lockwood George Sear Richard Wisker Ethan Lawrence Louisa Connolly-Burnham Tyger Drew-Honey
| music          = Banks & Wag
| cinematography = 
| editing        = 
| studio         = Saltbeef TV Pilot Media Bay Studios
| distributor    = Great Point Media
| released       =  
| runtime        = 
| country        = United Kingdom
| language       = English
| budget         = 
| gross          =  
}} CBBC childrens entertainment show Friday Download. The film was announced in 2014 and will be released on 22 May 2015. It stars Dionne Bromfield, Shannon Flynn, Bobby Lockwood, George Sear, Richard Wisker, Ethan Lawrence, Louisa Connolly-Burnham and Tyger Drew-Honey. 

== Cast ==
* Dionne Bromfield as Dionne
* Shannon Flynn as Shannon
* Bobby Lockwood as Bobby
* George Sear as George
* Richard Wisker as Richard
* Ethan Lawrence as Fraser
* Louisa Connolly-Burnham as Clara
* Tyger Drew-Honey as Caleb
* Luke Langsdale as Uncle Pete
* Angus Barnett as Gene Peck David Mitchell as Policeman  The Vamps as themselves 
* Bars and Melody as themselves

== Production == Vue cinemas.

== References ==
 

 
 
 