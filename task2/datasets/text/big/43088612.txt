Jonny Saves Nebrador
{{Infobox film
| name = Jonny Saves Nebrador
| image =
| image_size =
| caption =
| director = Rudolf Jugert
| producer = Heinrich Jonen  
| writer =  Karl Lerbs  (novel)   Werner Jörg Lüddecke     Heinz Pauck   Per Schwenzen   Hans Tannert
| narrator =
| starring = Hans Albers   Margot Hielscher   Peter Pasetti   Ferdinand Anton
| music = Willy Dehmel   Werner Eisbrenner 
| cinematography = Hans Schneeberger 
| editing =  Friedel Buckow     
| studio = Bavaria Film   Meteor-Film 
| distributor = Allianz Filmverleih
| released = 24 November 1953 
| runtime = 96 minutes
| country = West Germany  German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Jonny Saves Nebrador (German:Jonny rettet Nebrador) is a 1953 West German adventure film directed by Rudolf Jugert and starring Hans Albers, Margot Hielscher and Peter Pasetti.  The film is set in South America, but was shot on location in Italy.

==Cast==
*    Hans Albers as Jonny / General Oronta 
* Margot Hielscher as Marina  
* Peter Pasetti as Lt. Col. Dacano  
* Ferdinand Anton as Lt. Articos  
* Trude Hesterberg as Madame Dubouche  
* Linda Hardt as Rosita  
* Al Hoosmann as Totti  
* Franz Muxeneder as Paco  
* Kurt E. Ludwig as Carlo  
* Fritz Benscher as Rubino  
* Rudolf Vogel as Major Souza  
* Horst Loska as Maracas  
* Hans Bergmann as Rastano 
* Wolfgang Molander as Captain Tolly  
* Karl-Heinz Peters as Major Vinaigle  
* Ernst Rotmund as President Dacapo 
* Meloani
* Walter Wehner 
* Viktor Afritsch 
* Johannes Buzalski 
* Otto Friebel 
* Oliver Hassencamp  Franz Koch 
* Hans Schulz 
* Jürgen Krumwiede 
* Bum Krüger
* Fritz Lafontaine
* Kurt Lang 
* Ernst Legal 
* F. Neubert 
* Panos Papadopulos 
*Abdullah Schächly 
* Alfons Teuber 
* Bobby Todd 
* Karl von Malachowsky

== References ==
 

== Bibliography ==
* Bock, Hans-Michael & Bergfelder, Tim. The Concise CineGraph. Encyclopedia of German Cinema. Berghahn Books, 2009.
* Fenner, Angelica. Race under Reconstruction in German Cinema: Robert Stemmles Toxi. University of Toronto Press, 2011. 

== External links ==
*  

 
 
 
 
 
 
 
 
 
 

 