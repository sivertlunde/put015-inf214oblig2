Goodnight, Vienna
 
{{Infobox film
| name           = Goodnight, Vienna
| image          = Good_Night_Vienna.jpg
| image_size     = 
| caption        = Jack Buchanan and Anna Neagle
| director       = Herbert Wilcox
| producer       = Herbert Wilcox
| writer         = Eric Maschwitz
| narrator       = 
| music          = Tony Lowry   Harry Perritt 
| cinematography = Freddie Young
| editing        = Michael Hankinson 
| starring       = Jack Buchanan   Anna Neagle   Gina Malo 
| distributor    = United Artists
| released       =  
| runtime        = 75 minutes
| country        = United Kingdom
| language       = English
| budget         = 
}}

Goodnight, Vienna (also known as Magic Night) is a 1932 British musical film directed by Herbert Wilcox and starring Jack Buchanan, Anna Neagle and Gina Malo.  Two lovers in Vienna are separated by the First World War, but are later reunited.

It was based on a radio operetta written by Eric Maschwitz.  It features the song "Good-night, Vienna". Wilcox reportedly cast Neagle, whom he would later marry and direct in many films, after discovering her by chance in a stage show. 

==Plot==

Max is an Austrian officer in the army and son of a highly placed general. His father wants him to marry a Countess but he has fallen in love with Vicki. Attending a party given in his honour, they are informed that war has broken out. Max writes a note to Vicki and goes off to war. Unfortunately the note is lost. Some time after the war, Max is just a shoe shop assistant while Vicki is now a famous singer. They meet and at first she snubs him but then falls in love with him again.

==Cast==
* Jack Buchanan - Captain Maximilian Schletoff
* Anna Neagle - Vicki
* Gina Malo - Frieda
* Clive Currie - General Schletoff William Kendall - Ernst
* Joyce Bland - Countess Helga
* Gibb McLaughlin - Maxs Orderly
* Herbert Carrick - Johann
* Clifford Heatherley - Donelli
* O. B. Clarence - Theatre Manager
* Peggy Cartwright - Greta
* Muriel Aked - Marya
* Aubrey Fitzgerald - Waiter

==References==
;Notes
 

;Bibliography
* Street, Sarah. British National Cinema. Routledge, 2009.

==External links==
* 

 

 
 
 
 
 
 
 
 


 
 