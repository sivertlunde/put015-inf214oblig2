Acrobatty Bunny
{{Infobox Hollywood cartoon
| series = Looney Tunes (Bugs Bunny)
| image = AcrobattyBunny1.JPG
| caption = Bugs Bunny meets Nero
| director = Robert McKimson
| story_artist = Warren Foster Art Davis Richard Bickenbach Cal Dalton I.Ellis (uncredited)
| layout_artist = Cornett Wood
| background_artist = Richard H. Thomas
| voice_actor = Mel Blanc
| musician = Carl W. Stalling
| producer = Edward Selzer
| studio = Warner Bros. Cartoons
| distributor = Warner Bros. Pictures
| release_date = June 29, 1946 (USA)
| color_process = Technicolor
| runtime = 8 minutes
| preceded_by = Kitty Kornered
| followed_by = The Great Piggy Bank Robbery
| movie_language = English
}}

Acrobatty Bunny is a Warner Bros. cartoon released in 1946 as part of the Looney Tunes series, directed by Robert McKimson (his second), and starring Bugs Bunny and Nero the Lion.
 acrobat is a person skilled at high-wire or other high-in-the-air exploits, while "batty" is a slang term for "crazy" (as is "Bugs").

==Plot synopsis==
A circus is being set up just above Bugs rabbit hole, causing much noise and vibration. The Lion cage is set up directly above the hole, and the Lion takes deep sniffs (alternatively yanking Bugs towards the hole or throwing him back) to determine that the animal below is Bugs. When the Lion (whom Bugs eventually refers to as "Nero") roars again, Bugs comes to the surface to see whats going on, riding an elevator that makes twists and turns. Bugs tries to reason with the lion ("Im the tenant downstairs, and theres entirely too much noise!"), but soon makes a hasty escape when Nero takes a swipe at him.

Nero manages to get out of his cage, and chases Bugs around the circus grounds. Bugs at one point ducks into a dressing room, coming out as a clown trying to convince Nero to laugh ("COME ON, LAUGH!"), which he eventually does - until Bugs takes some whacks at the lion with a wooden board. The lion then chases Bugs into the big top, where they swing around acrobat swings. Eventually, Bugs tricks Nero into a cannon and sets the cannon off, causing Nero to do a hula in his skirt, plays the ukulele, and ends the cartoon by adding, "Were also available for picnics, lodge meetings, childrens parties, and smokers."

==Availability==
This cartoon can be found on the   DVD. It is also available on the Marx Brothers A Night in Casablanca DVD (2004).

==External links==
* 

 
{{succession box |
before= Hair-Raising Hare | Bugs Bunny Cartoons |
years= 1946 |
after= Racketeer Rabbit|}}
 

 
 
 
 
 
 


 