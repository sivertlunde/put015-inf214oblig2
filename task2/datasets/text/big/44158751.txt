Set Fire to the Stars
 
 
{{Infobox film
| name = Set Fire to the Stars
| image = 
| alt = 
| caption = 
| director = Andy Goddard
| producer = Andy Evans Andrew Riach Elijah Wood Executive: Steve Clark-Hall Simon Laub Samuel Potter
| writer = Andy Goddard Celyn Jones
| starring = Elijah Wood Celyn Jones Kelly Reilly Steven Mackintosh Shirley Henderson Kevin Eldon
| music = Gruff Rhys
| cinematography = Chris Seager
| editing = Mike Jones
| studio = Mad As Birds Films Masnomis
| distributor = Munro Film Services
| released =  
| runtime = 97 minutes  
| country = United Kingdom
| language = English
| budget = 
| gross = 
}} biographical drama directorial debut. John M. Brinnin and Jones as Dylan Thomas with supporting roles by Kelly Reilly, Steven Mackintosh, Shirley Henderson, and Kevin Eldon. The film is set to be released in the United Kingdom on 7 November 2014.

==Plot== Harvard graduate John M. Brinnin embarks on a week-long retreat to save his hero, acclaimed Welsh poet Dylan Thomas.

==Cast==
  John M. Brinnin
* Celyn Jones as Dylan Thomas Caitlin Thomas (née Macnamara)
* Steven Mackintosh as Jack
* Shirley Henderson as Shirley
* Kevin Eldon as Stanley
* Steve Spiers
* Maimie McCoy as Rosie
* Richard Brake as Mr. Unlucky
* Andrew Bicknell
* Kate Drew
* Ken Drury
* Nicola Duffett
* Adam Gillen
 

==Release== premiered at the 2014 Edinburgh Film Festival. 

==Reception==
Early reviews have been mixed; the film currently holds a 50% rating on review aggregator website Rotten Tomatoes. 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 