Imitation of Life (1934 film)
{{Infobox film
| name           = Imitation of Life
| image          = Imitation of Life poster2.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = John M. Stahl
| producer       = Carl Laemmle Jr.
| screenplay     = William J. Hurlbut
| based on       =  
| starring       = {{Plainlist|
* Claudette Colbert
* Warren William
* Rochelle Hudson
}}
| music          = Heinz Roemheld
| cinematography = Merritt B. Gerstad
| editing        = {{Plainlist|
* Philip Cahn
* Maurice Wright
}}
| studio         = Universal Pictures
| distributor    = Universal Pictures
| released       =  
| runtime        = 111 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 novel of the same name, was augmented by eight additional uncredited writers, including Preston Sturges and Finley Peter Dunne.  The film stars Claudette Colbert, Warren William and Rochelle Hudson and features Louise Beavers and Fredi Washington.
 1959 remake with the same title stars Lana Turner.

In 2005, Imitation of Life was selected for preservation in the United States National Film Registry. It was named by Time (magazine)|Time in 2007 as one of "The 25 Most Important Films on Race".   

==Plot==
White widow Bea Pullman (Claudette Colbert) and her daughter Jessie (Juanita Quigley as a toddler, Marilyn Knowlden as an eight-year-old) take in black housekeeper Delilah Johnson (Louise Beavers) and her daughter Peola (Sebie Hendricks), whose fair complexion reveals her mixed-race ancestry. Bea exchanges room and board for work, although struggling to make ends meet. Delilah and Peola quickly become like family to Jessie and Bea. They particularly enjoy Delilahs pancakes, made from a special family recipe. Five years later, Jessie and Peola prove to be challenging children to raise: Jessie is demanding, not particularly studious, relying instead on her charm. She is the first person to call Peola "black" in a hurtful way, making it clear that their childhood idyll is doomed. Peola does not tell her classmates at school that she is "colored" and is humiliated when her mother shows up one day, revealing her secret.
 ]] 
Bea finds it difficult to make a living selling pancake syrup (as her husband had done). Using her wiles to get a storefront on the busy Atlantic City boardwalk refurbished for practically nothing, she opens a pancake restaurant, where Delilah cooks in the front window. Later, at the suggestion of a passerby, Elmer Smith (Ned Sparks), she sets up an even more successful pancake flour corporation, marketing Delilah as an Aunt Jemima-like figure. She gives Delilah a 20% interest in the business, but Delilah continues to act as Beas housekeeper and factotum. Bea becomes wealthy from her business, but ten years later, the two older women are confronted with problems.
 passes as white, identifying with her European ancestry and  breaking Delilahs heart. Leaving her Negro college, Peola takes a job as a cashier in a white store. When her mother and Bea track her down, she is humiliated to be identified as black. She finally tells her mother that she is going away, never to return, so she can pass as a white woman without the fear that Delilah will show up. Her mother is heartbroken and, in a melodramatic scene, takes to her bed, murmuring Peolas name and forgiving her. 

The black servants sing a spiritual as she dies, with Bea holding her hand at the end. Delilahs last wish had been for a large, grand funeral, complete with a marching band and a horse-drawn hearse. Just before the processional begins, a remorseful, crying Peola appears, begging her dead mother to forgive her.  

Peola returns to her Negro college and presumably embraces her African descent. The film ends with Bea breaking her engagement with Stephen because of the situation with Jessie. She does not want to hurt her daughters feelings by being with him, but promises to find him after Jessie is over her infatuation. The movie ends with Bea embracing Jessie, while remembering the girls insistent demands for her toy duck (her "quack quack") when she was a toddler.

==Cast==
*Claudette Colbert as Beatrice "Bea" Pullman
*Warren William as Stephen "Steve" Archer
*Rochelle Hudson as Jessie Pullman, Age 18
*Ned Sparks as Elmer Smith
*Louise Beavers as Delilah Johnson
*Fredi Washington as Peola Johnson, Age 19
*Juanita Quigley as Baby Jessie Pullman, Age 3
*Marilyn Knowlden as Jessie Pullman, Age 8
*Dorothy Black as Peola Johnson, Age 9 (uncredited) Alan Hale as Martin, the Furniture Man
*Henry Armetta as The Painter
*Wyndham Standing as Jarvis, Beatrices Butler

Cast notes:
*Child actress Jane Withers has a small part as a classmate of Peola, her fifth movie appearance.
*Franklin Pangborn appears uncredited as "Mr. Carven"

==Production== Imitation of Life was a road trip to Canada she took with her friend, the black short-story writer and folklorist Zora Neale Hurston.  The novel was originally to be called Sugar House but was changed just before publication. 

Universal borrowed Warren William from Warner Bros. for the male lead, but the studio had first wanted Paul Lukas for the part.   The parents of the child playing "Jessie" as a baby changed her name from "Baby Jane" to "Juanita Quigley" during production of the film. 

Universal had difficulty receiving approval from the censors at the Hays Office for the original script they submitted for Imitation of Life.  Joseph Breen objected to the elements of miscegenation in the story, which "not only violates the Production Code but is very dangerous from the standpoint both of industry and public policy."  He rejected the project, writing, "Hursts novel dealing with a partly colored girl who wants to pass as white violates the clause covering miscegenation in spirit, if not in fact!"  The Production Code Administrations (PCA) censors had difficulty in "negotiating how boundaries of racial difference should be cinematically constructed to be seen, and believed, on the screen."  , Genders, Vol. 27, 1998, accessed 21 May 2013  

Their concern was the character of Peola, in whose person miscegenation was represented by this young woman considered black, but with sufficient white ancestry to pass as white and the desire to do so. Susan Courtney says that the PCA participated in "Hollywoods ongoing desire to remake interracial desire, an historical fact, as always already having been a taboo."  In addition, she explains the quandary imagined by the censors: "the PCA reads Peolas light skin, and her passing, as signifiers of miscegenation. By conflating miscegenation and passing in this way, the censors effectively attempt to extend the Codes ban on desire across black and white racial boundaries to include a ban on identification across those boundaries as well." 
 lynched for approaching a white woman whom he believed had invited his attention. Breen continued to refuse to approve the script up to July 17, when the director had already been shooting the film for two weeks.  , TCM  

Imitation of Life was in production from June 27 to September 11, 1934, and was released on November 26 of that year. 

All versions of Imitation of Life issued by Universal after 1938, including TV, VHS and DVD versions, feature re-done title cards in place of the originals. Missing from all of these prints is a title card with a short prologue, which was included in the original release. It reads:
 Atlantic City, in 1919, was not just a boardwalk, rolling-chairs and expensive hotels where bridal couples spent their honeymoons. A few blocks from the gaiety of the famous boardwalk, permanent citizens of the town lived and worked and reared families just like people in less glamorous cities.  

The scene in which Elmer approaches Bea with the idea to sell Delilahs pancake mix to retail customers refers to a legend about the origins of Coca-Colas success. It has been credited with strengthening the urban myth about the secret of Cokes success — that is, to "bottle it". 

==Themes==
"According to Jean-Pierre Coursodon in his essay on John M. Stahl in American Directors, 
 "Fredi Washington...reportedly received a great deal of mail from young blacks thanking her for having expressed their intimate concerns and contradictions so well. One may add that Stahls film was somewhat unique in its casting of a black actress in this kind of part - which was to become a Hollywood stereotype of sorts." Jeff Stafford, "Imitation of Life (1934)"], Turner Classics Film Article, Turner Classics  (Later films dealing with mulatto women, including the 1959 version of this work, cast white women in the roles.) 

The themes of the movie, to the modern eye, deal with very important issues—Passing (racial identity)|passing, the role of skin color in the black community and tensions between its light-skinned and dark-skinned members, the role of black servants in white families, and maternal affection. It is still compelling. 

Some scenes seem to have been filmed to highlight the fundamental unfairness of Delilahs social position—for example, while living in Beas fabulous NYC mansion, Delilah descends down the shadowy stairs to the basement where her rooms are. Bea, dressed in the height of fashion, floats up the stairs to her rooms, whose luxury was built from the success of Delilahs recipe. Others highlight the similarities between the two mothers, both of whom adore their daughters and are brought to grief by the younger womens actions. Some scenes seem to mock Delilah, because of her supposed ignorance about her financial interests and her willingness to be in a support role, but the two women have built an independent business together. By the end of the movie—especially with the long processional portraying a very dignified African-American community, Delilah is treated with great respect.

==Awards and honors== Best Picture, Best Assistant Sound Mixing for Theodore Soderberg.    

In 2005, Imitation of Life was selected for preservation in the United States National Film Registry of the Library of Congress. In February 2007 Time (magazine)|Time magazine included it among The 25 Most Important Films on Race, as part of the magazines celebration of Black History Month. 

==DVD== Universal Home Entertainment.
The movie as well as the 1959 remake were re-released on DVD and Blu-ray as part of Universals 100th Anniversary on January 10, 2012.

==References==
 
 

==Further reading==
*Alicia I. Rodriquez-Estrada, “From Peola to Carmen: Fredi Washington, Dorothy Dandridge, and Hollywood’s Portrayal of the Tragic Mulatto” in Quintard Taylor and Shirley Ann Wilson Moore, eds., African American Women Confront the West, 1600-2000 (Norman: University of Oklahoma Press, 1997). 

==External links==
 
 
*  
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 