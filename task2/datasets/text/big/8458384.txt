Visible Secret 2
 
 
{{Infobox Film
| name           = Visible Secret 2
| image          = 
| image_size     = 
| caption        = 
| director       = Abe Kwong
| producer       = 
| writer         = Ho Leung Lau 
| narrator       = 
| starring       = Eason Chan Cherrie Ying Roger Kwok
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 93 min 
| country        = Hong Kong 
| language       = Cantonese
| budget         = 
}}

Visible Secret 2 ( ) is a 2002 Hong Kong film directed by Abe Kwong and stars Eason Chan and Cherrie Ying. In this movie, a man with the ability to see ghosts begins to suspect that his wife may be possessed.

==Cast==
* Eason Chan
* Cherrie Ying
* Roger Kwok
* Jo Kuk
* Joe Cheung
* Law Lan
* Sheila Chan
* Sylvia Lai

==External links==
* 

 
 
 

 