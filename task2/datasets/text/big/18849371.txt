Mahadhevi
{{Infobox film
| name = Mahadhevi மகாதேவி
| image = Mahadhevi.jpg
| director = Sundar Rao Nadkarni
| writer = Kannadasan
| screenplay = Kannadasan
| based on =   Savithri P. S. Veerappa  M. N. Rajam J. P. Chandrababu T. P. Muthulakshmi
| producer = Sundar Rao Nadkarni B. Radhakrishna
| cinematography = G. K. Ramu
| editing = P. Venkatachalam
| music = Viswanathan-Ramamoorthy
| studio = Sri Ganesh Movietone
| distributor = Sri Ganesh Movietone
| released = 22 November 1957
| runtime = 160 mins
| country = India Tamil
| budget = 
}}
 Savithri in the lead roles. The film was based on novel Punya Prabhav by R. G. Katkari, Kannadasan wrote screenplay and dialogues. The film directed by Sundar Rao Nadkarni.  The film was super hit at box-office and ran for more than 125 days. 

==Cast==
{| class="wikitable" width="50%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
| M. G. Ramachandran || General Vallavan
|- Savithri || Mahadhevi
|-
| P. S. Veerappa || General Karunakaran
|-
| M. N. Rajam || Princess Mangamma
|-
| J. P. Chandrababu || Mariappan
|-
| T. P. Muthulakshmi || Vasantha
|-
| O. A. K. Thevar || 
|-
| A. Karunanidhi || 
|-
| Sattampillai Venkatraman || 
|-
| K. R. Ramsingh || Chellaiah
|}

==Crew==
*Producer: Sundar Rao Nadkarni & B. Radhakrishna
*Production Company: Sri Ganesh Movietone
*Director: Sundar Rao Nadkarni
*Story: Based on Novel Punya Prabhav written by R. G. Katkari
*Screenplay:Kannadasan
*Dialogue: Kannadasan
*Music: Viswanathan-Ramamoorthy
*Lyrics: Thanjai N. Ramaiah Dass, Kannadasan & A. Maruthakasi 
*Art Direction: K. Nageswara Rao
*Editing: P. Venkatachalam
*Choreography: T. Thangaraj, Sampath & Chinnilal
*Cinematography: G. K. Ramu
*Stunt: R. N. Nambiar
*Audiography: A. Krishnan
*Dance:

==Soundtrack== Playback singers are T. M. Soundararajan, A. M. Rajah, M. S. Rajeswari, P. Suseela, Raavu Balasaraswathi |R. Balasaraswathi Devi, T. S. Baghavathy, K. Jamuna Rani & A. G. Rathnamala.
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Kaka Kaka Maikondaa || M. S. Rajeswari || A. Maruthakasi || 03:20
|-
| 2 || Kanmoodum Velaiyilum || A. M. Rajah & P. Susheela || Kannadasan || 03:14
|-
| 3 || Un Thirumugatthai || J. P. Chandrababu & A. G. Rathinamala || Thanjai N. Ramaiah Dass || 03:20
|- Kannadasan || 03:01
|-
| 5 || Singara Punnagai || M. S. Rajeswari & Raavu Balasaraswathi |R. Balasaraswathi Devi || 03:33
|-
| 6 || Kurukku Vazhiyil || T. M. Soundararajan || 03:03
|-
| 7 || Kaamugar Nenjil || K. Jamuna Rani || A. Maruthakasi || 03:29
|- Thanjai N. Ramaiah Dass || 03:21
|-
| 9 || Eru Poottuvom || T. M. Soundararajan || 03:17
|- Kannadasan || 03:29
|-
| 11 || Sevai Seivadhe Aanandham || M. S. Rajeswari & T. M. Soundararajan || 04:08
|}

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 


 