The Victors (film)
{{Infobox film
| name           = The Victors
| image          = The Victors poster.jpg
| caption        = 
| producer       = Carl Foreman
| director       = Carl Foreman
| writer         = Written for the screen by Carl Foreman From the novel The Human Kind by Alexander Baron George Hamilton Melina Mercouri Jeanne Moreau George Peppard Maurice Ronet Rosanna Schiaffino Romy Schneider Elke Sommer Eli Wallach and Michael Callan
| music          = Composed and conducted by Sol Kaplan
| cinematography = Christopher Challis B.S.C.
| editing        = Alan Osbiston
| distributor    = Columbia Pictures
| released       =  
| runtime        = 175 minutes
| country        = United Kingdom
| language       = English
| gross = $2,350,000 (US/ Canada) 
}} American war film written, produced and directed by Carl Foreman, whose name on the films posters was accompanied by nearby text, "from the man who fired The Guns of Navarone". Shot on location in Western Europe and UK|Britain, The Victors features an all-star cast, with fifteen American and European leading players, including six actresses (Melina Mercouri from Greece, Jeanne Moreau from France, Rosanna Schiaffino from Italy, Romy Schneider and Senta Berger from Austria as well as Elke Sommer from West Germany) whose photographs appear on the posters.  One of the posters carries the tagline, "The six most exciting women in the world… in the most explosive entertainment ever made!".

==Overview==
The film follows a group of American soldiers through Europe during the Second World War, from Britain in 1942, through the fierce fighting in Italy and France, to the uneasy peace of Berlin. Production of the storys action meant filming scenes that took place in Sweden, France, Italy and England. 

It is adapted from a collection of short stories called The Human Kind by English author Alexander Baron, based upon his own wartime experiences. In the film the British characters of the original book were changed into Americans in order to attract American audiences.

Carl Foreman wrote, produced and directed the epic. He called it a "personal statement" about the futility of war. Both victor and vanquished are losers. 

The film slips between Pathé-style newsreel footage showing the conquering heroes abroad for the audience at home, and the grim reality of battlefield brutality and post-conflict ennui. No battle scenes are depicted in the film.

The story is told in a series of short vignettes, each having a beginning and an ending in itself, though all are connected to the others, as a series of short stories adding up to a longer one.
 GIs shows Russians alike exploiting German women sexually.

The hostility of German civilians towards their American and Soviet occupiers is also depicted.

One of the cinematic high points is the detour of one truckload of GIs out of a convoy, for the express purpose of supplying witnesses to the execution by firing squad of a GI deserter (a scene inspired by the real-life 1945 execution of Pvt. Eddie Slovik). Depicted in a huge, otherwise empty, snow-covered field near a chateau at Sainte-Marie-aux-Mines on Christmas Eve, while the film audience first hears Frank Sinatra singing "Have Yourself a Merry Little Christmas" and then a chorus of "Hark! The Herald Angels Sing", after the fatal shots are fired. This scene is remarkable for its stark, visually extreme imagery, and the non-combat stress and anguish foisted on GIs during a lull in combat. The New York Times film review stated "it stands out in stark and sobering contrast to the other gaudier incidents in the film". 

The whole film is shot in black and white, and so the black regimented figures of the firing squad and witnesses face the lone man bound to a stake in the midst of a snow-covered plain. The addition of surreal accompanying Christmas music and absence of dialogue make this scene an often cited one. The juxtaposition of saccharine music with a frightful scene was emulated the following year by Stanley Kubrick in Dr. Strangelove, which was also shot in black and white.

An anti-war message also unusual for the time period - and particularly regarding Americas involvement in the Second World War - is found in the final vignette. An American soldier (co-star George Hamilton) stationed in post-war Berlin picks a fight with a drunken Soviet soldier (Albert Finney), possibly to avenge the rape of his German girlfriend by Soviet soldiers during the Battle of Berlin. The fight ends with each man killing the other and the camera slowly pulls back to show the bodies of the two one-time allies lying in the shape of a "V" for victory in a seemingly limitless desert of rubble and ruins.

Saul Bass created the opening montage and title sequence that covers European history from the First World War to the Battle of Britain in the Second World War.

The film was nominated for a Golden Globe (Most Promising Newcomer, actor Peter Fonda).

==Cast==
  
Starring in alphabetical order
*Vincent Edwards  
*Albert Finney   George Hamilton  
*Melina Mercouri  
*Jeanne Moreau  
*George Peppard  
*Maurice Ronet   
*Rosanna Schiaffino  
*Romy Schneider  
*Elke Sommer  
*Eli Wallach  
*and Michael Callan as Eldridge
 
Co-Starring 
*Peter Fonda  
*Jim Mitchum  
*Senta Berger  
With 
*Albert Lieven  
*Mervyn Johns  
*Tutte Lemkow   John Crawford  
*Peter Vaughan  
*George Mikell  
*Alf Kjellin  
*Russ Titus
 
*Alan Barnes  
*John Rogers  
*Marianne Deeming  
*Sean Kelly
*Patrick Jordan  
*James Chase   Mickey Knox
*Peter Arne
*Malya Nappi  
*Veite Bethke
*Milo Sperber  
*George Roubicek  
 
The Squad  
*Riggs OHara
*Charles De Temple
*Al Waxman
*Tom Busby Robert Nichols
*Graydon Gould
*Larry Caringi
*Ian Hughes
*Anthony McBride
 

==Songs listed in opening credits==
"March of the Victors" "Sweet Talk" "No Other Man" by Sol Kaplan Freddy Douglass 
"My Special Dream" by Sol Kaplan Freddy Douglass Howard Greenfield  Jack Keller Gerry Goffin

==End quotation==
"My subject is War, and the pity of War.
The Poetry is in the pity.
All a poet can do today is warn…" Wilfred Owen Born, 18 March 1893. Killed in France, 4 November 1918.

==End credit==
Photographed on locations in Italy, France, England and Sweden, with the kind co-operation of the Swedish Army Ordnance Corps and at Shepperton Studios, England Released through Columbia Pictures Corporation

==Release== Leonard Maltins Film & Video Guide). Seen on Antenna TV in 4:3 standard definition on Memorial Day 2014.
 Hays Code, insisted that several scenes be deleted that showed two of the American soldiers were patronising a male French prostitute and paying him with food. While the Code had been gradually liberalised in the 1950s-early 1960s, homosexuality was still something that could only be, vaguely, implied in order to get approval from the Hollywood Production Code and the Catholic Legion of Decency.  

American film executives encouraged Foreman to include a nude scene with Elke Sommer, already in the version released in Europe and Britain, when he submitted it for a Production Code seal. This was to be used as a bargaining chip in case of any other objections. Foreman submitted the more modest version of the scene that had been shot for the American market and the film was passed without incident.  
 George Hamilton argued it "was way too dark, foreshadowing the great paranoid movies of the later sixties, ahead of the bad times that seemed to begin with the Kennedy assassination." George Hamilton & William Stadiem, Dont Mind If I Do, Simon & Schuster 2008 p 177 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 