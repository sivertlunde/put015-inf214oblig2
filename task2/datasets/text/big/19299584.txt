Commencement Day
{{Infobox film
| name           = Commencement Day
| image          =
| image_size     =
| caption        =
| director       = Robert F. McGowan Mark Goldaine
| producer       = Hal Roach
| writer         = Hal Roach H. M. Walker
| narrator       =
| starring       =
| music          =
| cinematography =
| editing        =
| distributor    = Pathé|Pathé Exchange
| released       =  
| runtime        = 20 minutes
| country        = United States Silent English intertitles
| budget         =
}}
 short silent silent comedy film directed by Robert F. McGowan.       It was the 25th Our Gang short subject released.

==Plot==
Mickey gets in a fight with another boy over Mary. The parents show up for Commencement Day at school and listen to the kids recite and play their musical instruments. Jackie puts pepper in Joe’s saxophone. Mickey loses his frog. Farina falls in a well. While the parents are out rescuing Farina, the kids get in a food fight. School is dismissed.

==Notes==
When the television rights for the original silent Pathé Our Gang comedies were sold to National Telepix and other distributors, several episodes were retitled. This film was released into the 16mm home movie market under the title "Graduation Day" and into a television syndication as "Mischief Makers" in 1960 under the title Little Red Schoolhouse. About two-thirds of the original film was included.

==Cast==

===The Gang===
* Joe Cobb as Joe
* Jackie Condon as Jackie
* Mickey Daniels as Mickey
* Allen Hoskins as Farina
* Mary Kornman as Mary
* Ernest Morrison as Ernie
* Jannie Hoskins as Mango
* Dinah the Mule as Herself

===Additional cast===
* Gabe Saienz as Snoozer, the bully
* Andy Samuel as Andy
* George B. French as School teacher
* Helen Gilmore as Joes mother
* Gus Leonard as Parent with ear-horn
* Lyle Tayo as Marys mother Dorothy Vernon as Mickeys mother
* Charley Young as Parent

==See also==
* Our Gang filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 


 