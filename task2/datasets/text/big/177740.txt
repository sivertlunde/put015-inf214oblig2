A Patch of Blue
{{Infobox film
| name           = A Patch of Blue
| image          = Patch of blue mp.jpg
| caption        = Promotional movie poster for the film Guy Green
| producer       = Guy Green Pandro S. Berman
| screenplay     = Guy Green
| based on       =   Be Ready with Bells and Drums by Elizabeth Kata
| narrator       =
| starring       = Sidney Poitier Shelley Winters Elizabeth Hartman
| music          = Jerry Goldsmith
| cinematography = Robert Burks
| editing        = Rita Roland
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 105 minutes
| country        = United States
| language       = English
| budget         = $800,000
| gross          = $6,750,000  (rentals)  
| preceded by    =
| followed by    =
}}
 Guy Green about the relationship between a black man, Gordon (played by Sidney Poitier), and a blind white female teenager, Selina (Elizabeth Hartman), and the problems that plague their relationship when they fall in love in a racially divided America. Made in 1965 against the backdrop of the growing civil rights movement, the film explores racism from the perspective of "love is blind."
 The Diary of Anne Frank. It was also the final screen appearance for veteran actor Wallace Ford.

Scenes of Poitier and Hartman kissing were excised from the film when it was shown in film theaters in the Southern United States.   Turner Entertainment colorized the movie for broadcast on the Turner-owned cable station TNT (TV channel)|TNT.  The colorized version was not released on VHS or DVD, and has not been shown since shortly after its initial broadcasts.
 Best Actress Best Art George Davis, Best Cinematography Best Music (Original Music Score). Hartman, 22 at the time, was the youngest Best Actress nominee ever, a record she held for ten years before 20 year-old Isabelle Adjani broke her record in 1975.   

== Plot ==

Selina DArcey (Elizabeth Hartman) is a blind girl living with her prostitute mother Rose-Ann (Shelley Winters) and grandfather Ole Pa (Wallace Ford), in a city apartment. She strings beads to supplement her familys small income, and spends most of her time doing chores. Her mother is abusive, and Ole Pa is an alcoholic. Selina has no friends, rarely leaves the apartment, and has never received an education. 

Selina convinces her employer to bring her to the park, where she happens to meet Gordon Ralfe (Sidney Poitier), an educated and soft-spoken black man working night shifts in an office, and the two quickly become friends. Gordon learns that she was blinded at the age of 5 when Rose-Ann threw chemicals on her while attempting to hit her husband, and also that she was raped by one of Rose-Anns "boyfriends".

Rose-Anns friend, Sadie, is also a prostitute, and while lamenting the loss of her youth, she realizes that Selina can be useful in their business. Subsequently, Rose-Ann and Sadie decide to move into a better apartment, leave Ole Pa and force Selina into prostitution.

In the meantime, Gordon has contacted a school for the blind which is ready to take Selina. While Rose-Ann is away, Selina runs away to the park and with some difficulty, meets Gordon. She tells Gordon about Rose-Anns plan, and he assures her that she will be leaving for the school in a few days. Finding Selina missing from the apartment, Rose-Ann takes Ole Pa to the park and confronts Gordon. Despite Rose-Anns resistance, Gordon manages to take Selina away, and Ole Pa stops Rose-Ann, telling her that Selina is not a child anymore. 

At Gordons house, Selina asks Gordon to marry her, to which Gordon replies that there are many types of love, and she will later realize that their relationship will not work. Selina tells him that she loves him, and knows that he is black, and that it does not matter to her. He then tells her they will wait one year to find out if their love will lead to marriage. Meanwhile, a bus arrives to pick up Selina.

==Cast==
* Sidney Poitier as Gordon Ralfe
* Shelley Winters as Rose-ann DArcey
* Elizabeth Hartman as Selina DArcey
* Wallace Ford as Ol Pa
* Ivan Dixon as Mark Ralfe
* Elisabeth Fraser as Sadie
* John Qualen as Mr. Faber
* Kelly Flynn as Yanek Faber
* Debi Storm as Selina, age 5
* Renata Vanni as Mrs. Favaloro
* Saverio LoMedico as Mr. Favaloro
* Casey Merriman as Casey M

==Soundtrack== top 25 American film scores.   The score has been released three times on CD; in 1991 through Mainstream Records (with the score to David and Lisa by Mark Lawrence, in 1992 through Tsunami Records (with his score to Patton (film)|Patton), and an extended version in 1997 through Intrada Records. 

=="A Cinderella Named Elizabeth"== short about middle America town of Youngstown, Ohio|Youngstown, Ohio, and includes segments from her screen test and associated "personality test", in which the actress is filmed while being herself and answering questions about everyday topics such as her taste in fashion|clothing. The short also shows her visiting the Braille Institute of America to watch blind people being trained to do handwork &mdash; similar to the beadwork her character does in the film &mdash; and to perform tasks of daily living and self-care, of the sort that Poitiers character teaches Selina to do.   via AOL Video. Retrieved November 25, 2007. 

==Reception==

===Critical reception===



A Patch of Blue currently has a 100% approval rating on Rotten Tomatoes.

===Awards and nominations===

{| class="wikitable"
|-
! Award
! Category
! Subject
! Result
|- Academy Award Academy Award Best Actress Elizabeth Hartman
| 
|- Academy Award Best Supporting Actress Shelley Winters
| 
|- Academy Award Best Cinematography Robert Burks
| 
|- Academy Award Best Original Score Jerry Goldsmith
| 
|- Academy Award Best Production Design
| 
|- British Academy BAFTA Award BAFTA Award Best Actor Sidney Poitier
| 
|- Golden Globe Award Golden Globe Best Actor – Motion Picture Drama
| 
|- Golden Globe Best Actress in a Motion Picture – Drama Elizabeth Hartman
| 
|- Golden Globe New Star of the Year – Actress
| 
|- Golden Globe Best Best Motion Picture – Drama Pandro S. Berman
| 
|- Guy Green Guy Green
| 
|- Golden Globe Best Screenplay
| 
|- Golden Globe Best Director
| 
|-
|}

===Box office===
The film proved to be the most successful in Poitiers career, which proved a lucrative development considering he agreed to a salary cut in exchange for 10% of the films gross earnings. In addition, the film made Poitier a major national film star with excellent business in even southern cities like Houston, Atlanta and Charlotte. 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 