A Finnish Summer with Turisas
{{Infobox film
| name        = A Finnish Summer with Turisas
| image       = A Finnish Summer with Turisas.jpg
| caption     = Film poster
| director    = 
| producer    = 
| starring    = Turisas
| released    =  
| runtime     = 160 minutes
| distributor = Century Media
}} Finnish metal band Turisas, featuring live performances.

It was released on 3 November 2008 in Europe and 25 November 2008 for the United States. 

==Track listing==
Summer Festival Live Performances:
#As Torches Rise
#To Holmgard and Beyond
#A Portage to the Unknown 
#The Messenger
#One More
#In The Court of Jarisleif Fields of Gold
#The Dnieper Rapids
#The Land of Hope and Glory
#Miklagard Overture
#Sahti-Waari Rasputin
#Battle Metal 
Extras:
*70 minute tour documentary
*Rasputin Music Video
*Blooper Reel
Limited Edition Includes:
*Warpaint colour card
*"Battle Metal 2008" 3"CD

===Recordings===
* "
*Nummirock 2008: "To Holmgard and Beyond", "One More", "The Dneiper Rapids", "Miklagard Overture", "Battle Metal"
*Ilosaarirock 2008: "A Portage to the Unknown", "Fields of Gold", "Sahti-Waari"
*Wanaja 2008: "The Messenger"
*Voimasointu 2008: "The Land of Hope and Glory"

== References ==
 

 

 
 

 