Athidhi (film)
 
 
{{Infobox film
| name           = Athidhi  ( )
| image          = Athithi.jpg
| caption        =
| director       = Surender Reddy
| producer       = Ronnie Screwvala Siddharth Roy Kapoor Ramesh Babu
| writer         = Vakkantham Vamsi
| narrator       = Abburi Ravi
| starring       = Mahesh Babu Amrita Rao Ashish Vidyarthi Kota Srinivasa Rao Nassar
| music          = Mani Sharma
| cinematography = Sameer Reddy
| editing        = Gowtham Raju
| studio         = UTV Motion Pictures Krishna Pictures Private Limited
| distributor    = UTV Motion Pictures
| released       = 18 October 2007
| dvd released   = 10 October 2008
| runtime        = 172 minutes
| country        = India Telugu
| gross          =   (Share First week)  
| budget         =   (citation needed)  
}} 2007 Telugu Tamil as Thani Kaattu Raja, and into Hindi as International Khiladi:The Iron Man.Athidi was released with 500 Prints in 820 Theaters all over the world.  Athidi collected a Share of   in its lifetime.  Royal with Hiran and Sayantika Banerjee.

==Trivia==
Atithi, an orphan is implicated in the murder and sent to jail for 13 years. Once out, it becomes his mission to trace and kill Kaiser, the boy who had murdered Amritas parents. The romantic element comes in the form of Amrita who is an art student in Delhi. Though the twosome meet in Delhi, the story moves to Hyderabad and it is here the rest of the story unravels.

There is a love story interwoven into the story, yet, Atithi is essentially a revenge drama and the action scenes are the main highlight of the film. The entire look of the film is rich and the makers have spent ostensibly on sets for the song sequences. The only drawback is the climax, when Mahesh goes on a killing spree. While the makers have used special effects to make it look less gory, it nevertheless gets a bit too violent. Royal is Hiran & heroine Sayantika Banerjee. Raja Chanda is the director of this upcoming film.

==Plot==
An orphan boy, named Sunil, wanders through all of Delhi looking for a job. When selling balloons, he sees a girl and gives her a free balloon. A few days later, during a storm, the girl finds the boy getting soaked and gives him an umbrella. The umbrella blows into a place nearby, where the girls parents are headed. The boy stops their car and indicates them about the dangerous path ahead destroyed by the storm. As the boy rescues the girls parents, the boy gets adopted by the parents. They name the boy "Atithi" which means "guest". One day, while driving Atithi to a hostel, they are stopped by a teenager, a Psychopath, who mugs them, and shoots the parents. In his rage, Athithi lunges after the teen managing to take the teens gun. Nearby witnesses see Athithi with a gun and the girls parents and think that Atithi killed them. The police come and arrest Atithi. The girl also thinks that it was Atithi who killed her parents and starts to hate him and thinks that she will never see him again.

Fourteen years later, Atithi (now Mahesh Babu) gets out of jail, and is now in search of the Psychopath {teen who has killed the girls parents} and the girl who is alive. In Delhi, local goons are feared by Atithi, as he becomes a hurdle for their illegal operation. At one instance, he runs across a girl named Amrita (Amrita Rao) chased by stalkers from her college. Amrita and Atithi meet many times. Amrita starts to like Atithi, but has to go back to Hyderabad before she can confess her love. When she goes to try tell him that she is leaving Atithi gets annoyed. He continuously tells her to leave him alone, when she keeps asking to get some coffee and slaps her. She runs away crying. Later Atithi goes to Amrita to say sorry but only finds her friend. She tells him that Amrita just wanted to tell him that shes leaving. They then go to the cemetery seeing if she is there. Amritas friend then explains that a boy adopted by Amritas parents killed them when she was young and Amrita goes to the cemetery to see their graves all the time. Atithi realizes that she was the same girl that he met in his childhood and leaves for Hyderabad to find her.

When Atithi also arrives to Hyderabad, and decides to stay with Amritas family, he learns that a Mafia leader and kidnapper named Kaiser is feared there. Later, after few events, Atithi realizes that Kaiser was the psychopath teen who killed the girls parents. Now Kaiser is trying to kill Amrita. Atithi stops two attempts by Kaiser to murder Amrita. Meanwhile, Officer Ajay (Murali Sharma) is trying to track down Kaiser. Ajay dies in an explosion in his house, which was planned by Kaiser. Atithi figures out later on that Ajay is none other than Kaiser. Ajays real name is Kaiser. Before Kaisers house blew up, Kaiser managed to escape. When the MLA realizes that Ajays real name is Kaiser, the MLA, after succumbing to shock, dies with a heart attack. Athithi manages to kill many of Kaisers men. Kaiser then kidnaps Amrita and her cousin. Kaiser kills Amritas cousin. Then Kaiser leads Amrita to a dungeon and calls Atithi and tells him that he will kill Amrita if Atithi doesnt rescue her in 12 hours. Then Atithi kidnaps Kaisers brother, and trades for Amrita. Amrita is severely cut and stuck in a certain air tank (Kaiser planned), but is rescued. Kaiser is killed. Atithi and Amrita unite, realising he wasnt the one who killed her parents.

==Cast==
* Mahesh Babu ... Atithi
* Amrita Rao  ... Amrita
* Ravi Prakash... Hero friend
* Ashish Vidyarthi ... Danny Bhai
* Murali Sharma ... Inspector Ajay Sastry/Kaiser
* Nassar      ... Amritas uncle, the Home Minister
* Kota Srinivasa Rao... Maccha Srinu, an MLA who works under Kaiser
* Brahmanandam Sunil
* Subbaraju... Ganni Bhai Pragathi ... Nassers wife
* Rajiv Kanakala ... Cameo Venu Madhav... Cameo
* Malaika Arora Khan ... Item number in the song "Rathraina"

==Crew==
* Director: Surender Reddy
* Producer: Ramesh Babu
*  
*  
* Film editing|Editor: Gowtham Raju
* Stunt director: Stun Siva

==Box office==
* The film was released worldwide with 500 prints in 820 Theaters across the globe. 
* The film opened with a gross collection of   6 Crores in the first day itself. It collected a total of around   Share in the first four days of its release. 
Athidi collected a Share of   in its first week breaking all records at that time.
the film collected a final Share amount of  (nett) in 2 weeks.
Gross collections of the film went up to  .

==Soundtrack==
{{Infobox album
| Name       = Athidhi
| Type       = Soundtrack
| Artist     = Mani Sharma
| Cover      = Athithi.jpg
| Genres     =
| Label      = Supreme
| Music      = Mani Sharma
}}

{{Track listing
| extra_column = Singer(s)
| lyrics_credits = yes
| title1 = Khabaddarani
| extra1 = Naveen, Rahul Nambiyaar
| length1 = 4:57
| title2 = Gona Gona
| extra2 = Naveen, Rita
| length2 = 4:26
| title3 = Satyam Emito
| extra3 = Deepu, Usha
| length3 = 4:27
| title4 = Khiladi Koona
| extra4 = Karthik, Rita
| lyrics4 =
| length4 = 3:55
| title5 = Ratraina
| extra5 = Ranjit, Anushka
| length5 = 5:12
| title6 = Valla Valla
| extra6 = Rahul Nambiyaar, Dharmna
| length6 = 4:21
}}

==DVD==
* The DVD was released on 10 October 2008. 

==References==
 

==External links==
 

 

 
 
 
 
 
 
 
 