Running Scared (1972 film)
 Running Scared}} 1972 film written and directed by David Hemmings. This was the only film written by Hemmings, and his first time directing. It was based on a novel of the same name written by Gregory Mcdonald.

Robert Powell played Tom Betancourt, a student at Cambridge University, who leaves after watching his best friend (Andy Bradford) committing suicide by slashing his wrists. On principle, Tom refuses to interfere, having discussed the matter with his friend, who insists that it he is doing this by choice and does not wish to be prevented.

Tom then visits his friends parents under an assumed name, and falls in love with the dead boys sister (Gayle Hunnicutt). A complicated affair ensues. Tom Milne in The Times commented that it "somehow fritters itself away into long, broody pauses and soulful searchings". Several sequences, on the other hand, were "done with a razor-sharp incisiveness that would not have shamed Losey and Pinter ... on balance, it seems worth risking the tedium to watch a born director at work". 

Filmed on location in Braunston, mainly around the canal and marina. Other filming took place in high street, Daventry, at Bilton Grange and at Oundle School in Northamptonshire. The film crew spent most of the summer of 1971 there and local people were asked to be extras. A film camera was positioned on top of the then disused windmill to film Robert Powell in the Austin-Healey sports car being driven through the village. The windmill was also used as the "base" for the crew. Although never released on video, there was apparently a 16mm version made.

Local interest in the film is surprisingly strong. At a recent exhibition in Braunston (Braunston festival 2010) Photographs taken during filming, as well as press cuttings, original scripts and posters were displayed to the public. The original film was intended to be shown if a copy could be located but unfortunately this was not the case. Due to the interest shown, a larger exhibition and possibly a documentary (with input from local residents) is planned for the 40th anniversary of the making of the film in summer 2011. A copy of the original film if located will be screened.

In December 2011 the film was shown twice in Braunston Village Hall, lead actress Gayle Hunnicutt was present at the viewing. The copyright has yet to be determined. At least one more showing is scheduled at the end of January 2012

==References==
 

==External links==
* 

 
 


 