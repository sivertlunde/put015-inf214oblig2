Tinker Bell and the Legend of the NeverBeast
 
{{Infobox film name           = Tinker Bell and the Legend of the NeverBeast image          = Tinker Bell and the Legend of the NeverBeast poster.jpg caption        =  director       = Steve Loter producer       = Makul Wigert  writer         =  based on       =   starring       = Mae Whitman Ginnifer Goodwin Rosario Dawson Lucy Liu Megan Hilty Raven-Symoné Angelica Huston music          = Joel McNeely  cinematography =  editing        =  studio         = DisneyToon Studios distributor  Walt Disney Studios Home Entertainment released       =   runtime        = 76 minutes country        = United States language       = English budget         =  gross          = $9,074,501 
}}
Tinker Bell and the Legend of the NeverBeast  is an American computer-animated film directed by Steve Loter.  It is the sixth full-length and seventh film in the DisneyToon Studios Tinker Bell (film series)|Tinker Bell film series, based on the character Tinker Bell from J. M. Barries Peter and Wendy. 

It was released theatrically in selected markets including the United Kingdom on December 12, 2014. In the United States, it had a limited theatrical release, opening on January 30, 2015 at the El Capitan Theatre for a 13-day engagement,  and was released direct-to-video on March 3, 2015.  

Mae Whitman, Lucy Liu, Raven-Symoné, Megan Hilty, Pamela Adlon and Anjelica Huston reprise their roles of Tinker Bell, Silvermist, Iridessa, Rosetta, Vidia and Queen Clarion.    Ginnifer Goodwin joins the cast, replacing Angela Bartys as the voice of Fawn in this film,    Rosario Dawson joins the cast as new character, Nyx  and singer Mel B also joins the cast as new character Fury in the U.K. version while Danai Gurira voices Fury in the U.S. version.

==Plot==
Fun and talented animal fairy Fawn believes you cant judge a book by its cover, or an animal by its fangs, so she befriends a huge and mysterious creature known as the NeverBeast. While Tink and her friends arent so sure about this scary addition to Pixie Hollow, the elite Scout Fairies set out to capture the monster before he destroys their home. Fawn must trust her heart and take a leap of faith if she hopes to rally the girls to save the NeverBeast.

Fawn has rescued a baby hawk with a broken wing. Since hawks eat fairies, scout-fairies notify Queen Clarion and Fawn is reprimanded for only following her heart and not her head. After helping the baby hawk return to its rightful place, Fawn hears an unfamiliar roar and decides to fly to the depths of the forest to investigate. She finds a strange creature that has never been seen before. After observing its behavior she surmises that it has traits similar to many different animals shes encountered, save for one peculiar activity. The beast is building mysterious towers of rock in each area of Pixie Hollow. She names him Gruff.

Meanwhile, in Pixie Hollow, an ambitious scout fairy named Nyx also heard the roar, and decides to investigate. She too happened upon the beast and does research in the fairy lore library to find out what shes up against.
Misunderstanding some information gathered from several torn pages of an undisclosed animal-book, she discovers that Gruff will transform into an even more ferocious beast and (seemingly) destroy Pixie Hollow with a mysterious series of events that leads to a deadly storm.

Queen Clarion urges both Fawn and Nyx to work together and "do the right thing" regarding the protection of Pixie Hollow. Each fairy takes this edict to mean different things; Fawn sets out to help Gruff complete whatever mysterious task he intends to carry out, while Nyx is determined to capture him and prevent the impending storm. Gruff indeed transforms into the monster depicted in the fairy lore, but his intentions are a surprise to all as Gruff is in fact trying to save Pixie Hollow by collecting all of the lightning through the towers he built thus preventing them from hitting the trees and causing a fire.   

==Cast==
* Ginnifer Goodwin as Fawn 
* Mae Whitman as Tinker Bell 
* Rosario Dawson as Nyx 
* Lucy Liu as Silvermist 
* Raven-Symoné as Iridessa 
* Megan Hilty as Rosetta 
* Pamela Adlon as Vidia
* Angelica Huston as Queen Clarion
* Danai Gurira as Fury (U.S. version)
** Mel B as Fury (U.K. version)
* Chloe Bennet as Chase Thomas Lennon as Scribble
* Jeff Corwin as Buck
* Olivia Holt as Morgan  
* Grey Griffin as the Narrator
* Kari Wahlgren as Robin, Ivy

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 
 