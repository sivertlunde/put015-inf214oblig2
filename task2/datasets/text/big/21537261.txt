The Chasers (1959 film)
{{Infobox film
| name           = The Chasers
| image          = 
| caption        = 
| director       = Erik Løchen
| producer       = Sølve Kern
| writer         = Erik Løchen
| starring       = Rolf Søder
| music          = 
| cinematography = Tore Breda Thoresen
| editing        = Erik Løchen
| distributor    = 
| released       = 24 August 1959
| runtime        = 94 minutes
| country        = Norway
| language       = Norwegian
| budget         = 
}}

The Chasers ( ) is a 1959 Norwegian film directed by Erik Løchen. It was entered into the 1959 Cannes Film Festival.     

==Cast==
* Rolf Søder - Bjørn
* Bente Børsum - Guri (as Benedikte Liseth)
* Tor Stokke - Knut
* Harald Aimarsen
* Bjarne Bø
* Anders Boger
* Carsten Byhring - Cameo appearance
* Kristen Dahl
* Eilert Flyen
* Bonne Gauguin
* Lillemor Grimsgaard
* Odd Grythe
* Olafr Havrevold - Narrator (voice)
* Egil Hjorth-Jenssen
* Matias Lindalen
* Egil Lorck
* Rolf Just Nilsen - Cameo appearance
* Henri Poirier
* Thorleif Reiss

==References==
 

==External links==
* 

 
 
 
 
 
 