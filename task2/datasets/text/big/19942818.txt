Piranha 3D
{{Infobox film
| name           = Piranha 3D
| image          = Piranha 3d poster.jpg
| alt            =  
| caption        = Theatrical poster
| director       = Alexandre Aja
| producer       = Alexandre Aja Mark Canton Marc Toberoff Grégory Levasseur
| writer         = Pete Goldfinger Josh Stolberg Adam Scott Jerry OConnell Ving Rhames Jessica Szohr Steven R. McQueen Christopher Lloyd Richard Dreyfuss
| music          = Michael Wandmacher
| cinematography = John R. Leonetti
| editing        = Baxter
| studio         = The Weinstein Company Atmosphere Entertainment Chako Film Company Intellectual Properties Worldwide
| distributor    = Dimension Films
| released       =  
| runtime        = 88 minutes
| country        = United States
| language       = English
| budget         = $24 million
| gross          =  $83,188,165   
}} 3D horror Adam Scott, Kelly Brook, Riley Steele, Ving Rhames and Eli Roth. 

==Plot==
Fisherman Matt Boyd (Richard Dreyfuss) is fishing in Lake Victoria when a small earthquake hits, splitting the lake floor and causing a whirlpool. Boyd falls in and is ripped apart by a school of piranhas that emerge from the chasm.

Jake Forester (Steven R. McQueen) is admiring attractive tourists as spring break begins. He reunites with his old crush, Kelly Driscoll (Jessica Szohr) and meets Derrick Jones (Jerry OConnell), a sleazy pornographer, as well as Danni Arslow (Kelly Brook), one of his actresses. Derrick convinces Jake to show him good spots on the lake for filming a pornographic movie. That night, Jakes mother, Sheriff Julie Forester (Elisabeth Shue), searches for the missing Matt Boyd with Deputy Fallon (Ving Rhames). They find his mutilated body and contemplate closing the lake. However, this decision is made difficult by the two thousand partying college students on spring break, who are important for bringing revenue to the small town. The next morning, a lone cliff diver is attacked and consumed by the marauding fish.

Jake bribes his sister and brother, Laura (Brooklynn Proulx) and Zane (Sage Ryan), to stay home alone so that he can show Derrick around the lake. After Jake leaves, Zane convinces Laura to go fishing on a small sandbar island. They forget to tie the boat down and are stranded in the middle of the lake. Meanwhile, Jake goes to meet with Derrick and runs into Kelly, who invites herself onto Derricks boat, The Barracuda. Jake meets Crystal Shepard (Riley Steele), another one of Derricks actresses, and cameraman Andrew Cunningham (Paul Scheer).
 Adam Scott), Sam Montez (Ricardo Chavira), and Paula Montellano (Dina Meyer)—to the fissure. Novak speculates that the rift leads to a buried prehistoric lake. Paula and Sam scuba dive to the bottom and discover a large cavern filled with large piranha egg stocks. Both are killed by the piranhas before they can alert the others to the discovery. Novak and Julie find Paulas corpse and pull it onto the boat, capturing a lone piranha, which they take to Carl Goodman (Christopher Lloyd), a marine biologist who works as a pet store owner. He explains that it is a super aggressive prehistoric species, long believed to be extinct, and that the piranhas have survived through cannibalism.

Julie, Novak, Fallon, and Deputy Taylor Roberts (Jason Spisak) try to evacuate the lake, but their warnings are ignored until the piranhas begin to attack the tourists. Novak boards a jet-ski with a shotgun to help while Fallon drags people to shore and Julie and Taylor try to get swimmers into the police boat. Almost everyone in the lake is either wounded or killed by the piranhas or panicking guests attempting to escape.

Meanwhile, Jake spots Laura and Zane on the island, and forces Derrick to rescue them. Derrick crashes the boat into some rocks, flooding the boats lower deck, and causing the boat to start sinking. Kelly is trapped in the kitchen while Derrick, Crystal and Andrew fall overboard from the impact of the collision. Crystal is devoured and Andrew escapes to shore unharmed. Meanwhile, Danni manages to get a partially eaten Derrick back on board.

Deputy Fallon makes a last stand, taking a boat motor and using its propeller to shred many piranhas, though he is presumably killed. After the chaos settles, Julie receives a call from Jake pleading for help. Julie and Novak steal a speed boat and head off towards the kids. They reach Jake and attach a rope to his boat. Julie, Danni, Laura, and Zane start crossing the rope, but the piranhas latch onto Dannis hair and devour her. The others make it across safely, but the rope comes loose. Using Derricks corpse as a distraction, Jake ties the line to himself and goes to save Kelly. He ties Kelly to him and lights a flare after releasing the gas from a pair of stored propane tanks. Novak starts the boat and speeds away just as the piranhas surround Kelly and Jake. They are dragged to safety as the propane tanks explode, destroying the boat and killing most of the piranhas.

Mr. Goodman calls Julie on the radio, and Julie tells him that they seem to have killed the majority of the piranhas. The horrified Goodman tells her that the reproductive glands on the piranha they obtained were not mature, which means that the fish they have killed were only the babies. As Novak wonders aloud where the parents are, a human sized piranha leaps out of the water and eats him.

==Cast==
 
{{columns-list|2|
* Elisabeth Shue as Julie Forester Adam Scott as Novak
* Jerry OConnell as Derrick Jones
* Ving Rhames as Deputy Fallon
* Jessica Szohr as Kelly
* Steven R. McQueen as Jake Forester
* Dina Meyer as Paula
* Christopher Lloyd as Carl Goodman
* Richard Dreyfuss as Matt Boyd
* Ricardo Chavira as Sam
* Kelly Brook as Danni
* Paul Scheer as Andrew  
* Cody Longo as Todd Dupree
* Sage Ryan as Zane Forester
* Brooklynn Proulx as Laura Forester
* Riley Steele as Crystal
* Devra Korwin as Mrs. Goodman
}}
 Robert Shaw sing together aboard Quints Robert Shaw (actor)|(Robert Shaw)s boat the Orca, in the movie Jaws.

Eli Roth, Ashlynn Brooke, Bonnie Morgan, Genevieve Alexandra, and Gianna Michaels appear as spring breakers who meet gruesome demises, while Franck Khalfoun and Jason Spisak portray deputies.

==Production==
Chuck Russell was originally scheduled to direct the film, and made uncredited rewrites to the script by Josh Stolberg and Peter Goldfinger, as well as incorporating the original John Sayles script that Joe Dante directed the first time around.  Alexandre Aja was selected to direct the film instead. 
 CGI fish, but also because it all happens in a lake.  We were supposed to start shooting now, but the longer to leave it the colder the water gets.  The movie takes place during Spring Break and, of course, the studio wanted it ready for the summer, but if youve got 1,000 people who need to get murdered in the water, you have to wait for the right temperature for the water, for the weather, for everything." 

Shooting took place in June 2009 at Bridgewater Channel in Lake Havasu, located in Lake Havasu City, Arizona. The water was also dyed red for the shooting. 

Citing constraints with 3D camera rigs, Aja shot Piranha in 2D and converted to 3D in post production using a 3D conversion process developed by Michael Roderick and used by the company, Inner-D.  Unlike some other 3D converted films released in 2010, Piranhas conversion was not done as an afterthought, and it represents one of the first post-conversion processes to be well received by critics.   

==Release== A Nightmare on Elm Street and Inception. It was set to have a panel on 24 July 2010 as part of the San Diego Comic-Con International but was cancelled after convention organizers decided the footage that was planned to be shown was not appropriate.  Nine minutes of footage, with some unfinished effects, were leaked onto websites. The clip used in promotional TV ads and the trailer that shows Jessica Szohrs character, Kelly, face to face with a pack of piranhas was not used in the movie, and was used for promotion only.

The official poster was released June 22, 2010. 

===Box office===
Piranha 3D grossed $10,106,872 in its first 3 days, opening at #6 in the United States box office.  In the United Kingdom, Piranha 3D opened at #4 at the box office, earning £1,487,119. As of May 16, 2011, Piranha 3D has made $83,188,165 worldwide. 

===Reception=== normalized rating out of 100 to reviews from mainstream critics, the film has received a score of 53, based on 20 reviews, which indicates "mixed or average reviews".  A tongue-in-cheek scholarly review of the movie was written for the journal Copeia (Chakrabarty & Fink 2011), which reviewed the movie as if it were a documentary film. 

 , film critic for the   referred to the film as "a pitch-perfect, guilty-pleasure serving of late-summer schlock that handily nails the tongue-in-cheek spirit of the Roger Corman original" while stating "Jaws (film)|Jaws it aint Aja exhibits little patience for such stuff as dramatic tension and tautly coiled suspense, and there are some undeniable choppy bits...but he never loses sight of the potential fun factor laid out in Pete Goldfinger and Josh Stolbergs script."  The Orlando Sentinel gave the film one and a half stars out of four, stating that "Piranha 3D goes for the jugular. And generally misses, but generally in an amusing way." 

===Home media=== 3D formats 2D releases Channel 5 on February 10, 2013 for the first time, and in 2D format.

==Soundtrack==
{{Infobox album  
| Name        = Piranha 3D: Original Motion Picture Soundtrack
| Type        = Soundtrack
| Artist      = Various artists
| Cover       = Piranha 3D Original Motion Picture Score (Official Album Cover).jpg
| Alt         =
| Released    =  
| Recorded    =
| Genre       =
| Length      = 42:47
| Label       = Lakeshore Records  LKS 341872  
| Producer    = Skip Williamson  (Executive producer|exec.) , Brian McNelis  (exec.) 
}}
Lakeshore Records released the soundtrack album of Piranha 3D which included mostly rap, dance (music)|dance, hip-hop and contemporary R&B|R&B music. Artists include Shwayze, Envy, Flatheads, Amanda Blank, Public Enemy, Dub Pistols, and Hadouken!. 

===Track listing===
{{Track listing
| extra_column     = Artist
| writing_credits  = yes
| title1           = Get U Home Aaron Smith
| extra1           = Shwayze
| length1          = 3:14
| title2           = Shake Shake Leviticus
| Envy feat. Leviticus
| length2          = 3:25
| title3           = Here She Comes
| writer3          = Geoff Segel & Nik Frost
| extra3           = Flatheads
| length3          = 3:34
| title4           = Make It Take It
| writer4          = Amanda McGrath, Alex Epton, Mario Andreoni, Santi White & Tyler Pope
| extra4           = Amanda Blank
| length4          = 2:27
| title5           = Bring the Noise (Remix Pump-kin Edit)
| writer5          = Carlten Ridenhour, Eric Sadler & Hank Shocklee
| length5          = 3:39 Public Enemy vs. Benny Benassi
| title6           = She Moves
| writer6          = Jason OBryan, Barry Ashworth & Ter K. Lawrence
| extra6           = Dub Pistols
| length6          = 3:12
| title7           = Flower Duet from Lakmé
| writer7          = Léo Delibes
| extra7           = Adriana Kohutkova & Denisa Slepkovska
| length7          = 6:37
| title8           = Nadas Por Free
| writer8          = Willy "Wil-Dog" Abers, Ulises Bella, Raul Pacheco, Justin Porée, Asdru Sierra & Jiro Yamaguchi
| extra8           = Ozomatli
| length8          = 2:57
| title9           = Come And Get It
| writer9          = Eli Paperboy Reed, Ryan Spraker & Michael Montgomery
| extra9           = Eli Paperboy Reed
| length9          = 3:33
| title10          = Now You See It (Benny Benassi & DJ Shimik Extended Mix)
| writer10         = Armando C. Perez, Justin Roman, Vince Garcia, Tony Arazadon & Richard Bailey
| extra10          = Honorebel feat. Pitbull & Jump Smokers
| length10         = 3:25
| title11          = M.A.D. James Smith, Alice Spooner, Daniel Rice, Nick Rice & Chris Purcell
| extra11          = Hadouken!
| length11         = 3:25
| title12          = Im in the House William Adams & Justin Bates
| extra12          = Steve Aoki feat. Zuper Blahq
| length12         = 3:24
}}

===Songs not included on the soundtrack=== Mitch Miller & The Gang LMFAO
* "Fetish" by Far East Movement
* "Girls on the Dance Floor" by Far East Movement

===Credits=== 
*A&r   – Eric Craig 
*Cello  , Guitar, Percussion, Programmed By – Michael Wandmacher 
*Composed By – Susie Benchasil Seiter 
*Conductor – Michael Wandmacher 
*Edited By   – Joshua Winget 
*Executive Producer   – Brian McNelis, Skip Williamson 
*Mixed By – Mark Curry (3) 
*Orchestrated By – Chad Seiter, Michael Wandmacher, Susie Benchasil Seiter 
*Producer – Michael Wandmacher

==Sequel==
 
Dimension Films announced a sequel shortly after the first film was released.  The film is Piranha 3DD and is directed by John Gulager with Patrick Melton and Marcus Dunstan writing. It was released on June 1, 2012. It stars Ving Rhames, Paul Scheer and Christopher Lloyd, reprising their roles from Piranha 3D .  Piranha 3DD is set at a waterpark where the piranhas find a way through the pipes. 

Following its release, it failed to generate the positive critical reaction of its predecessor and grossed only $8,493,728. 

==References==
 

==External links==
* 
* 
* 
* 
* 
 
 
 
 
 
  
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 