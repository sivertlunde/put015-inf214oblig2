The Shadow on the Window
{{Infobox film
| name           = The Shadow on the Window
| image          = Shadow on the window poster small.jpg
| image_size     = 
| alt            = 
| caption        = Theatrical release poster
| director       = William Asher
| producer       = Jonie Taps
| screenplay     = David P. Harmon Leo Townsend
| story          = John Hawkins Ward Hawkins
| narrator       = 
| starring       = Philip Carey Betty Garrett John Drew Barrymore
| music          = George Duning
| cinematography = Frank G. Carson
| editing        = William A. Lyon
| distributor    = Columbia Pictures 
| released       =  
| runtime        = 76 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
The Shadow on the Window is a 1957 film directed by William Asher. It stars Philip Carey and Betty Garrett. 

==Plot==
Upon seeing his mother Linda terrorized by three thugs, young Petey is traumatized and wanders off. Truck drivers find him and the boy is brought to police headquarters, where he is recognized as the son of detective Tony Atlas.

Petey is in shock and momentarily cant even recognize his dad, much less explain what happened. The youths who have Tonys wife are named Joey, Gil and Jess. It turns out they have robbed and killed Lindas employer, Canfield, and are arguing whether to also kill her, the only eyewitness.

Canfields niece comes looking for him, but upon finding Lindas car mistakenly believes he might be having an affair with her. Gil slips away to retrieve a gun from his mothers house. Tony gets the drop on him and kills Gil in self-defense, finding Lindas wallet on him.

The truckers provide the general vicinity of where they found the boy. Petey seems to recognize a tractor, so Tonys men find Canfields body and surround the house. Joey is accidentally killed by Jess, who is disarmed by Tony. At the sight of his mother, Petey emerges from his state of shock.

==Cast==
* Philip Carey as Tony Atlas
* Betty Garrett as Linda Atlas
* John Drew Barrymore as Jess Reber
* Corey Allen as Gil Ramsey
* Gerald Sarracini as Joey Gomez
* Eve McVeagh as Bessie Warren
* Jerry Mathers as Petey

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 


 