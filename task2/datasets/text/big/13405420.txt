The Battle at Elderbush Gulch
 
{{Infobox film
| name           = The Battle at Elderbush Gulch
| image          = Battle at Elderbush Gulch Poster.jpg
| caption        = Film poster
| director       = D. W. Griffith
| producer       = 
| writer         = D. W. Griffith Henry Albert Phillips
| starring       = Mae Marsh Lillian Gish
| cinematography = G. W. Bitzer
| editing        = 
| distributor    = 
| released       =  
| runtime        = 29 minutes 
| country        = United States
| language       = Silent with English intertitles
| budget         = 
}}
  silent Western Western film directed by D. W. Griffith    and featuring Mae Marsh, Lillian Gish, and Lionel Barrymore.

==Plot==
Sally (Mae Marsh) and her little sister are sent to visit their three uncles in the west. Among other baggage they bring their two puppies. Melissa (Lillian Gish) is in the same stagecoach with husband and newborn baby. The uncles find the little girls amusing but tell them that the dogs must stay outside. Meanwhile, a nearby tribe of very very evil looking Indians is having a tribal dance. The puppies, left outside in a basket, run off. Sally, worried about the dogs goes outside and discovers they are gone.  She follows their trail and runs into two hungry Indians who have captured them for food. There is a scuffle but her uncles arrive and intervene. Gunfire ensues and one of the Indians is left dead. The other Indian returns to the tribe to inform them and aroused by "savage hatred" they go into a war dance.

Meanwhile, a tearful Sally has persuaded a friendly hand to build a secret door in the cabin so she can bring the puppies inside at night. The Indians attack the village and the frightened settlers run off toward the lonely cabin.
In the melee the baby is captured by the Indians. The Indians attack the cabin just after a scout rides off to alert the fort.

The Indians ride in circles around the cabin, While the settlers try to fight them off. Melissa, in the cabin,  is distraught worrying about the fate of her baby. Sally, more worried about her puppies, sneaks out her secret door and finds not only them, but the baby in the arms of a dead Indian. In a hectic battle scene, she brings the babies back through the secret door. Just as the  settlers are running out of ammunition, the cabin is burning, and the Indians, crawling on their stomachs, are almost in the cabin, the cavalry arrives. The Indians are quickly dispatched, all is well but for Melissas grief over her missing baby. Sally pops out of a chest holding baby and puppies. All is well. The uncle agrees to let Sally keep the puppies inside.

==Cast==
 
* Mae Marsh - Sally
* Leslie Loveridge - A waif
* Alfred Paget - Waifs uncle
* Robert Harron - The father
* Lillian Gish - The mother
* Charles Hill Mailes - Ranch owner
* William A. Carroll - The Mexican
* Frank Opperman - Indian Chief
* Henry B. Walthall - Indian Chiefs son Joseph McDermott - Waifs guardian Jennie Lee - Waifs guardian
* Lionel Barrymore
* Elmer Booth
* Kate Bruce - Settler Harry Carey Charles Gorman - Among the Indians
* Dell Henderson
* Elmo Lincoln - Cavalryman
* W. Chrystie Miller - Settler
* W. C. Robinson - Among the Indians
* Blanche Sweet

==See also==
* List of American films of 1913
* Harry Carey filmography
* D. W. Griffith filmography
* Lillian Gish filmography
* Blanche Sweet filmography
* Lionel Barrymore filmography

==References==
 

==External links==
* 
*  

 

 
 
 
 
 
 
 
 
 
 
 