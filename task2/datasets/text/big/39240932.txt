Tout ce qui brille
{{Infobox film
| name          = Tout ce qui brille
| image         = Tout-ce-qui-brille.jpg
| caption       = Theatrical release poster
| director      = Géraldine Nakache  Hervé Mimran
| producer      = Aïssa Djabri  Farid Lahouassa
| writer        = Géraldine Nakache   Hervé Mimran
| starring      = Géraldine Nakache Leïla Bekhti Virginie Ledoyen Audrey Lamy
| music          = 
| cinematography = Guillaume Deffontaines
| editing        = Scott Stevenson
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = France
| language       = French
| budget         = $6,600,000
| gross          = $21,835,178  
}}
Tout ce qui brille is a 2010 French film and the debut feature film for Géraldine Nakache and Hervé Mimran, who co-wrote and co-directed the film. It was filmed in Puteaux, La Défense, and Paris, notably the 16th arrondissement.

Originally, Tout ce qui brille was a 2007 short film shot by the same directors.

==Plot==
Ely (Géraldine Nakache) and Lila (Leïla Bekhti) are two working class girls and best friends who dream of a glitzier more glamorous lifestyle. They sneak into a club where they meet Agathe (Virginie Ledoyen) who, impressed when Ely headbutts a man who was yelling at her, offers them a ride home. Lila lies about where they live giving a much chicer address that belongs to a family she used to babysit for. 

Lila continues to ingratiate herself into the life of Agathe while Ely feels more and more left out as she realizes that Agathe and her friends think of her more as the help than an actual friend. Eventually she grows sick of Lilas lies and the two grow apart. When Lilas lies begin to crumble around her she realizes that Agathe and her social circle are not really her friends and that by working hard Ely is slowly achieving everything they dreamed of. She gets a real job working at an upscale shoe store and eventually reconnects with Ely. 

==Cast==
* Géraldine Nakache as Ely Wapler
* Leïla Bekhti as Lila Belaifa
* Virginie Ledoyen as Agathe
* Linh-Dan Pham as Joan, Agathes friend
* Audrey Lamy as Carole, Ely and Lilas friend
* Manu Payet as Éric, Lilas fiancé 
* Simon Buret as Maxx,  Daniel Cohen as Maurice, Elys father
* Nanou Garcia as Danielle, Elys mother
* Ary Abittan as Lilas father
* Fejria Deliba as Nadia, Lilas mother
* Lucie Bourdeu as Annah, Elys younger sister
* Nader Boussandel as Slim, Elys neighbor
* Jeanne Ferron as Mme Houbloup, gossip lady in the building
* Alexandre Gars as Elvis, Agathes son
* Sabrina Ouazani as Sandra, Lilas colleague
* Maria Ducceshi as Jil
* Pascal Demolon as taxi driver
* Sébastien Castro as movie theatre director

==Awards and nominations==
 
;Awards won
*2010: Special jury award and European Public Award at Festival international du film de comédie de lAlpe dHuez 
*2011: Best Feminin Hope of the Year (newcomer) for Leïla Bekhti at 36th César Awards 
*2011: Étoiles dor du cinéma français (Gold Star of French Cinema) for:
**Best Debut Film for Géraldine Nakache and Hervé Mimran 
**Feminin Revelation of the Year for Leïla Bekhti

;Nominations
*2011: César for Best Debut Film for Géraldine Nakache et Hervé Mimran at 36th César Awards
*2011: César for Best Femini Hope (newcomer) for Audrey Lamy

==Soundtrack==
The soundtrack of the film has proven popular particularly the song "Chanson sur une drôle de vie" sung by appearing in SNEP official French Singles Chart. It is a remake of a previous Véronique Sanson hit. The soundtrack also featured the song Fit But You Know It by The Streets.

{| class="wikitable" Year
!align="center" Single
!align="center" Peak positions Notes
|- FR     
|- 2010
|"Chanson sur une drôle de vie"   (Géraldine Nakache & Leïla Bekhti)  6
|align="center" From soundtrack
|-
|}

==References==
 

==External links==
* 
* 

 
 
 
 
 