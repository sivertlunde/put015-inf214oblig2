Batman: Year One (film)
{{Infobox film
| name           = Batman: Year One
| image          = Batman- Year One Blu-Ray.jpg
| border         = 
| alt            = 
| caption        = Blu-ray cover
| director       = Sam Liu Lauren Montgomery
| producer       = Lauren Montgomery Alan Burnett
| screenplay     = Tab Murphy
| based on       =  
| starring       = Bryan Cranston Benjamin McKenzie Eliza Dushku Jon Polito Alex Rocco Katee Sackhoff
| music          = Christopher Drake
| cinematography = 
| editing        = Margaret Hou
| studio         = Warner Premiere Warner Bros. Animation DC Entertainment
| distributor    = Warner Home Video
| released       =  
| runtime        = 64 minutes
| country        = United States
| language       = English
| budget         = $3.5 million  
| gross          = $5,251,108 DVD  
}}
Batman: Year One is a 2011   printed in 1987. It premiered at San Diego Comic-Con International|Comic-Con on July 22 and was officially released October 18, 2011. The film was directed by Lauren Montgomery and Sam Liu.     It is the 12th film released under the DC Universe Animated Original Movies banner, and was released on DVD, Blu-ray, and Digital copy.   

==Plot== Bruce Wayne James Gordon Detective Arnold Flass assaulting a teen for fun.
 Holly Robinson Selina Kyle. his father’s bust, requesting guidance in his war on crime. A bat crashes through a window and settles on the bust, giving him inspiration - "I will become a bat."
 Commissioner Gillian Loeb, several officers attack him, including Flass, who threatens Gordon’s pregnant wife. Gordon tracks Flass down, beats him up, and leaves him naked and handcuffed in the snow.
 Carmine "The Roman" Falcone, Loeb orders Gordon to bring him in by any means necessary.
 Harvey Dent becomes Batman’s first ally.
 Detective Sarah Essen suggests Wayne as a Batman suspect and she and Gordon witness Batman save an old woman from a runaway truck. Essen holds Batman at gunpoint, but Batman disarms her and flees to an abandoned building.  Loeb fraudulently orders a bomb dropped on it, forcing Batman into the fortified basement. A SWAT team is sent in, led by trigger-happy Lieutenant Branden, whom Batman attempts to trap in the basement. Branden manages to climb out of the trap through a collapsed chimney, and joins in the gun battle. Enraged as the team’s careless gunfire injures several people outside, Batman beats the team into submission. Using a device to attract the bats of his cave, Batman escapes amid the chaos. Selina Kyle, after witnessing him in action, dons a costume of her own to begin the life as Catwoman.

Gordon has a brief affair with Essen, while Batman intimidates a drug dealer for information. The dealer goes to Gordon to testify against Flass, who is brought up on charges. Loeb blackmails Gordon with proof of his affair against pressing charges. After taking Barbara with him to investigate Waynes connection to Batman, Gordon confesses the affair to her.

Batman sneaks into Falcone’s manor and overhears a plan against Gordon but is interrupted when Catwoman, hoping to build a reputation after her robberies were pinned on Batman, attacks Falcone and his bodyguards, aided by Batman. Identifying Falcone’s plan as the morning comes, the un-costumed Bruce leaves to help Gordon.

Gordon tries to rebuild his relationship with his family after Essen leaves Gotham. While leaving home, Gordon spots a motorcyclist enter his garage. Suspicious, Gordon enters to see Johnny Vitti (Falcone’s nephew) and his thugs holding his family hostage. Gordon realizes if he lets them go, they will most likely kill his wife and son. Therefore, Gordon shoots the thugs and chases Vitti, who has fled with the baby. Bruce Wayne, on a motorcycle, also rushes to chase Vitti. Gordon blows out Vittis car tire on a bridge and the two fight, with Gordon losing his glasses, before Vitti and James Gordon Jr. fall over the side. Bruce leaps over the railing and saves the baby. Gordon realizes that he is standing before an unmasked Batman, but says that he is "practically blind without   glasses," and lets Bruce go.
 the Joker" threatens to poison the citys reservoir, Gordon summons Batman with the Bat-Signal and waits on a rooftop for the Dark knight to arrive.

==Cast== Bruce Wayne / Batman Lieutenant James Gordon Selina Kyle / Catwoman
* Jon Polito as Commissioner Gillian B. Loeb 
* Alex Rocco as Carmine Falcone  Detective Sarah Essen
* Jeff Bennett as Alfred Pennyworth
* Grey DeLisle as Barbara Eileen-Gordon (credited), Vicki Vale (uncredited) Harvey Dent Keith Ferguson as Jefferson Skeevers Danny Jacobs as Flasss Attorney
* Nick Jameson as Officer Stanley "Stan" Merkel Holly Robinson  Louisa Falcone
* Stephen Root as Lt. Branden
* Fred Tatasciore as Detective Arnold John Flass/Johnny Vitti

==Production==
Producer Bruce Timm noted that the adaption of the film was relatively straightforward due to the cinematic nature of the original story arc. Bryan Cranston originally turned down the role as James Gordon because he was unfamiliar with both animation and classic comics. Cranston said "I wasnt aware of this level of storytelling in animation."   

==Reception==
Batman: Year One received positive reviews upon its release. Rotten Tomatoes gives the film a score of 86% based on reviews from 7 critics, with an average rating of 6.8 out of 10. 

An   R.L. Shaffer
October 18, 2011 
Tommy Cook of Collider called the film a "faithful adaptation".  
The A.V. Club gave the film an A-, saying "Batman: Year One is a stellar adaptation, copying Miller’s words and Mazzucchelli’s images almost verbatim at times." Concluding that, "It all recalls what it felt like to read Batman: Year One for the first time, and sense that this was a story that had always existed." 

Cinemacrazed criticized the short run time of the film as its main downfall.  
James OEhley of SciFiMoviePage notes that the faithfulness to the source material works for and against the film, with voiceover and dialog slowing down the action, and he goes on to say how the animation could be bolder, the voices gruffer and the sound more stirring but that overall the film is better than other DC animated films. 
 Caped Crusader, Jim Gordon. Walter White on Breaking Bad, he argued that the choice of casting "truly encompassed the characters determination and downright badass attitude in the comic Year One". 

==Home video==
The DVD and Blu-ray release includes a short animated film titled  . In the film, Catwoman deals with the crime boss Rough Cut (voiced by John DiMaggio) while trying to stop a cargo shipment.  
There is also a sneak peek for the film  , two featurettes, a commentary, a digital comic book, two   episodes ("Catwalk" and "Cult of the Cat"), a standard edition of the film, and a high definition edition of the film. 

==Connections to Other Works== James Gordon in television series Gotham (TV series)|Gotham.

==References==
 

==External links==
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 