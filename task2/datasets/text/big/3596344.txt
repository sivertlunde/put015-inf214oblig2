The Contact (1997 South Korean film)
{{Infobox film
| name           = The Contact
| image          = The Contact.jpg
| caption        = Theatrical poster
| film name      = {{Film name
 | hangul         =  
 | hanja          =  
 | rr             = Jeobsok
 | mr             = Chŏpsok}}
| director       = Jang Yoon-hyeon
| producer       = Sim Jae-myung
| writer         = Jang Yoon-hyeon Jo Myeong-joo
| starring       = Han Suk-kyu Jeon Do-yeon
| music          = Choi Man-sik Jo Yeong-wook
| cinematography = Kim Seong-bok
| editing        = Park Gok-ji
| distributor    = Myung Films
| released       =  
| runtime        = 105 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          = 
}} 1997 South Korean romance film directed by Jang Yoon-hyeon. Starring Han Suk-kyu and Jeon Do-yeon, it was the second biggest-grossing Korean film of 1997 with 674,933 admissions,  and was awarded the Grand Bell Awards for best picture.

==Plot== Velvet Underground record. Dong-hyeon hopes that the record was sent by his former lover. He decides to play the song Pale Blue Eyes off of that record. At the same time, a home shopping telemarketer, Soo-hyeon (Jeon Do-yeon) listens to the radio program while driving her car.

The next day, Soo-hyeon makes a request through the internet for Dong-hyeon to play the song again. Dong-hyeon then contacts Soo-hyeon, hoping she is his former girlfriend or someone he knows.

==Cast==
*Jeon Do-yeon as Soo-hyeon 
*Han Suk-kyu as Dong-hyeon
*Chu Sang-mi as Eun-hee
*Park Yong-soo
*Choi Cheol-ho   Kim Tae-woo
*Lee Beom-soo
*Kim Min-kyung as female employee at convenience store

==Awards==
1997 Grand Bell Awards 
* Best Film
* Best Editing - Park Gok-ji
* Best Adapted Screenplay - Jang Yoon-hyeon
* Best New Actress - Jeon Do-yeon

1997 Blue Dragon Film Awards
* Best New Actress - Jeon Do-yeon

1998 Baeksang Arts Awards
* Most Popular Actress (Film) - Jeon Do-yeon

1998 Korean Association of Film Critics Awards
* Best New Actress - Jeon Do-yeon

== References ==
 

== External links ==
*  
*   at Koreanfilm.org

 
 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 


 
 