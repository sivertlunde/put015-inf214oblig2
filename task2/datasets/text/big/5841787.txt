Partner (2007 film)
{{Infobox film
| name           = Partner
| image          = Partner-01.jpg
| caption        = Movie poster for Partner
| director       = David Dhawan
| producer       = Sohail Khan Parag Sanghvi
| writer         = Sanjay Chhel David Dhawan Yunus Sajawal
| narrator       = Govinda  Salman Khan  Lara Dutta Katrina Kaif
| music          = Sajid-Wajid
| cinematography = Johny Lal
| editing        = Nitin Rokade   Pranay Patel
| distributor    = Sohail Khan Productions K. Sera Sera
| released       =  
| runtime        = 139 mins
| country        = India
| language       = Hindi
| budget         = 
| gross          =   
}} Govinda and Salman Khan, with Katrina Kaif and Lara Dutta. The film is a remake of the 2005 Hollywood film, Hitch (film)|Hitch. 

==Synopsis==
 
Prem  (Salman Khan) is a Love Guru who solves the love issues of his clients. He meets Bhaskar Diwakar Chaudhary (Govinda (actor)|Govinda) who comes to Prem for help in his love life. Bhaskar loves his boss Priya Jaisingh (Katrina Kaif) but is unable to express his love to her as she is the daughter of a wealthy businessman. Prem initially refuses to help Bhaskar and goes to Phuket Province|Phuket, Thailand. Bhaskar follows him there and convinces him to help.

After returning from Thailand, Prem meets Naina ( . Prem saves her and falls in love with her. Meanwhile, he starts teaching Bhaskar how to impress Priya. But Bhaskar uses his own simplicity and nonsense acts to impress Priya. Priya finally falls in love with Bhaskar but does not disclose it to him.

Prem comes to know from Bhaskar that Priya is getting married to someone according to her fathers will. They both come to Priyas wedding ceremony with Naina, and Priyas father is convinced by Bhaskars acts. Priya now gets ready to marry Bhaskar.

Meanwhile, a spoiled brat named Neil comes to Prem for love help and he asks Prem to convince a girl to have a one-night stand. Prem gets angry with Neil and tells him that he does not help people with such bad intentions. Neil somehow manages to get his one-night stand and then ditches her, telling her the love guru gave him advice to do so. Unfortunately, the girl turns out to be Nainas friend Nikki. Naina then sets out to expose the Love Guru and finds out that it is Prem. Naina hates Prem for what she thought he did to her friend and publishes a front page article claiming that Prem can set anyone up with the girl they want using the Priya/Bhaskar relationship as an example.

Prem thinks that Bhaskar may commit suicide without Priya and goes to her to tell her what really happened. Priya realizes that all the things she liked about Bhaskar are what Prem wanted Bhaskar to hide from her and Priya is ready to take Bhaskar back. Prem makes up with Naina by making her hear the truth about him not helping Neil, and they get back together. On both couples honeymoon night, Bhaskar again asks Prem for help but this time they both get mingled with their respective wives.

==Cast== Govinda as Bhaskar Devakar Chaudhary
* Salman Khan as Prem (Love Guru)
* Lara Dutta as Naina
* Katrina Kaif as Priya Jaisingh
* Master Ali Haji as Rohan (Nainas son)
* Dalip Tahil as Raj Jaisingh
* Aarti Chabria as Nikki (Nainas colleague)
* Rajat Bedi as Neil Bakshi
* Suresh Menon as Kiran
* Deepshikha as Pammi, Prems sister
* Tiku Talsania as Nainas boss Ali Asgar as Nainas colleague
* Atul Parchure
* Vindu Dara Singh as Chadha
* Razak Khan as John uncle
* Puneet Issar as Rana
* Rajpal Yadav as Chhota Don
* Shashi Kiran as Newspaper stall owner

==Soundtrack==
{{Infobox album |  
 Name = Partner |
 Type = Soundtrack |
 Artist = Sajid-Wajid |
 Cover = | 2007 |
 Recorded = | Feature film soundtrack |
 Length = |
 Label =  T-Series|
 Producer = Sajid-Wajid |
 Last album = Jaane Hoga Kya (2006) |
 This album = Partner (2007) |
 Next album = Kismat Konnection (2008) |
}}
{{Album ratings
| rev1 = Planet Bollywood
| rev1Score =    
| rev2 = Bollywood Hungama
| rev2Score =    
}}

The music was composed by Sajid-Wajid. The soundtrack entered the top five on 23 July 2007. 

===Tracklist===
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- style="background:#3366bb; text-align:center;"
! # !! Song !! Singer(s) !! Picturised on !! Length
|-
| 1
| "Do You Wanna Partner"
| Udit Narayan, Shaan (singer)|Shaan, Sajid-Wajid|Wajid, Suzanne DMello, Clinton Cerejo
| Salman Khan & Govinda
| 04:30
|-
| 2
| "You’re My Love"
| Shaan, Shweta Pandit, Suzanne DMello, Earl D’Souza
| Govinda, Katrina Kaif, Salman Khan & Lara Dutta
| 04:35
|-
| 3
| "Dupatta Tera Nau Rang Da"
| Sonu Nigam, Shreya Ghoshal, Kunal Ganjawala, Suzanne DMello
| Govinda, Katrina Kaif, Lara Dutta & Salman Khan
| 05:00
|-
| 4
| "Soni De Nakhre"
| Labh Janjua, Sneha Pant, Wajid
| Govinda & Katrina Kaif
| 04:17
|-
| 5
| "Maria Maria"
| Wajid, Sonu Nigam, Shakib, Sunidhi Chauhan, Naresh Iyer
| Salman Khan & Lara Dutta
| 04:36
|-
| 6
| "You’re My Love (Remix)"
| Shaan, Shweta Pandit, Suzanne DMello, Earl D’Souza
| Excluded in film
| 04:27
|-
| 7
| "Do You Wanna Partner (Remix)"
| Udit Narayan, Shaan, Wajid, Suzanne DMello, Clinton Cerejo
| Excluded in film
| 03:58
|}

==Reception==
===Box office===
According to Box Office India, it was a super hit of Bollywood in the year 2007. It grossed Rs. 1.38 Billion in India  and $4.19 million overseas. 
* 
* 

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 