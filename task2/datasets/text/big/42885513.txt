Heinz in the Moon
{{Infobox film
| name = Heinz in the Moon
| image =
| image_size =
| caption =
| director = Robert A. Stemmle
| producer = 
| writer =  Marcel Arnac  (novel)   Robert A. Stemmle 
| narrator =
| starring = Heinz Rühmann   Rudolf Platte   Annemarie Sorensen   Oskar Sima
| music = Franz Grothe   
| cinematography = Carl Drews   
| editing =  Rudolf Schaad      
| studio = Cicero Film  NDLS 
| released =5 September 1934 
| runtime = 82 minutes
| country = Germany  German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Heinz in the Moon (German:Heinz im Mond) is a 1934 German comedy film directed by Robert A. Stemmle and starring Heinz Rühmann, Rudolf Platte and Annemarie Sorensen. Stemmle renamed the title from Hans to Heinz to take advantage of the stars popularity. 

==Cast==
*    Heinz Rühmann as Aristides Nessel  
* Rudolf Platte as Arthur Kosemund, Nessels Diener  
* Annemarie Sörensen as Anna Busch, Privatsekretärin  
* Oskar Sima as Martin Fasan, Börsenmakler 
* Erika Glässner as Helene Fasan   Ellen Frank as Siddie Fasan  
* Anita Mey as Dina, Dienstmädchen bei Fasan  
* Hans Leibelt as Professor Ass  
* Susi Lanner as Cleo Ass  
* Inge Conradi as Corinna Linck  
* Alexa von Porembsky as Emma, Dienstmädchen von Ass  
* Julia Serda as Frau Bach  
* Fita Benkhoff a sMadame Pythia  
* Dodo van Doeren 
* Friedrich Ettel 
* Ernst Nessler 
* Max Wilmsen 
* Josef Dahmen 
* Walter Steinweg 
* Carl Walther Meyer
* Hans Albin 

== References ==
 

== Bibliography ==
* Hake, Sabine. Popular Cinema of the Third Reich. University of Texas Press, 2001.

== External links ==
*  

 

 

 
 
 
 
 
 

 