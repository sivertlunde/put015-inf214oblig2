Sex with a Smile
{{Infobox film
| name = Sex with a Smile
| image = Sex with a smile.jpg
| director = Sergio Martino
| writer = Sergio Martino Tonino Guerra
| starring = Marty Feldman Barbara Bouchet Edwige Fenech Dayle Haddon
| producer = Luciano Martino
| music = Guido & Maurizio De Angelis
| cinematography = Giancarlo Ferrando
| editing=  Eugenio Alabiso
| distributor = Medusa (Italy) Centaur/Surrogate (U.S.)
| released =
| runtime =
| budget =
| gross =
| language = Italian
}}
Sex with a Smile is a 1976 Italian comedy film starring Marty Feldman, Barbara Bouchet, Edwige Fenech, Dayle Haddon and directed by Sergio Martino.  While the cast was relatively popular internationally, advertising for the film in the U.S. concentrated almost exclusively on Marty Feldman, even though he only appeared in one segment of the film.

The original Italian title was 40 gradi allombra del lenzuolo,  colloquially translatable as 104 Degrees Under the Sheets (40 degrees Celsius equals 104 degrees Fahrenheit). It was followed by a sequel entitled Spogliamoci così senza pudor, colloquially translatable as So Naked, Without Modesty.

==Synopsis==
An Italian anthology film sex comedy that features a series of five short comedic sketches that parody Italian sexual mores.

==Segments==

One for the Money ("I soldi in banca")

In Switzerland, an affluent Italian wife (Barbara Bouchet) of a traveling executive is visited by an eccentric stranger (Enrico Montesano), also Italian, who offers her 20 million lira to have sex with him. Initially put off by the idea, she ultimately agrees to his offer, unaware of the strangers actual business.

The Bodyguard ("La guardia del corpo")

A pampered and bored socialite (Dayle Haddon) grows weary of the extreme measures her bodyguard (Marty Feldman) employs to keep tabs on her. However, when she steals away for an impulsive encounter with a visiting artist, she realizes his level of devotion may have been justified.

Catch It While Its Hot ("Lattimo fuggente")

What initially appears to be a tryst between a countess (Giovanna Ralli) and her put-upon chauffeur (Alberto Lionello) reveals surprising secrets about the woman, man, and circumstances of their rendezvous.

Dream Girl ("La cavallona")

An extremely attractive lawyers wife (Edwige Fenech) is ogled by almost all the men in her neighborhood, but one particularly nebbishly man (Tomas Milian) regularly phones her, complaining of the many erotic dreams she has inspired in him. Over time, she finds her own dreams impacted by their conversations, and she asks to meet him so that they can both get back to normal.

A Dogs Day ("Un posto tranquillo")

A vertigo sufferer (Aldo Maccione) considering renting an apartment discovers a woman (Sydne Rome) from the apartment next to his space, out on her ledge, appearing to contemplate suicide. He coaxes her into his room for safety, and they are quickly attracted to each other. She insists on entertaining him in her own apartment, but when he joins her, he runs afoul of Othello, her extremely jealous dog.

==Cast==
*Marty Feldman: Alex (The Bodyguard)
*Giovanna Ralli: Esmeralda (Catch it While its Hot)
*Dayle Haddon: Marina (The Bodyguard)
*Edwige Fenech: Emilia Chiapponi (Dream Girl)
*Sydne Rome: Marcella Fosne (A Dogs Day)
*Barbara Bouchet: The rich woman (One for the Money)
*Tomas Milian: Cavaliere Marelli (Dream Girl)
*Aldo Maccione: Adriano Serpetti (A Dogs Day)
*Enrico Montesano: Salvatore (One for the Money)
*Alberto Lionello: Filippo (Catch it While its Hot)
*Christian Aligny: Dracula (Dream Girl)
*Franco Diogene: Ignazio (One for the Money)
*Nello Pazzafini: Rental car clerk (Catch it While its Hot)

==American Release==

While an English-language export version was created by the producers, American distributor Centaur/Surrogate significantly altered it for their domestic release. The credit sequence was truncated to a single title card that featured Marty Feldmans credit, omitting all other credits for the cast and above the line crew (though primary cast members were ultimately credited at the beginning of the story they appeared in), and the closing credit crawl was removed. Also, the order of the segments was changed to present "Dream Girl" first, followed by "The Bodyguard", "One For the Money", "Catch it While its Hot," and closing with "A Dogs Day."

==References==
 

== External links ==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 


 
 