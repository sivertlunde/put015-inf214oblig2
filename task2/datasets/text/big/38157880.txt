The Infamous Lady
{{Infobox film
| name           = The Infamous Lady
| image          =
| caption        =
| director       = Geoffrey Barkas   Michael Barringer
| producer       = 
| writer         = Geoffrey Barkas   Michael Barringer Ruby Miller   Walter Tennyson   Muriel Angelus
| music          =
| cinematography =  Sydney Blythe
| editing        = 
| studio         = New Era Films
| distributor    = New Era Films
| released       = November 1928
| runtime        = 7,500 feet 
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         = 
| preceded_by    =
| followed_by    =
}} silent drama Ruby Miller and Walter Tennyson. It was made at Twickenham Studios. It is also known by the alternative title Mayfair.

==Cast==
* Arthur Wontner as The Kings Counsel Ruby Miller as The Adventuress 
* Walter Tennyson as The Man 
* Muriel Angelus as The Girl 
* John Rowal as The Tramp 
* Dora Barton as The Wife 
* Ian Swinley as The Explorer 

==References==
 

==Bibliography==
* Low, Rachel. The History of British Film: Volume IV, 1918–1929. Routledge, 1997.

==External links==
* 

 
 
 
 
 
 
 
 

 