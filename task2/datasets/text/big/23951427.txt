The Three Million Trial
{{Infobox film
| name           = The Three Million Trial
| image          =
| image_size     = 230px
| caption        = 
| director       = Yakov Protazanov
| producer       = 
| writer         = Yakov Protazanov Umberto Notari (play)
| narrator       = 
| starring       = Igor Ilyinsky
| music          = 
| cinematography = Pyotr Yermolov
| editing        = 
| distributor    = 
| studio         = Gorky Film Studio|Mezhrabpom-Russ
| released       = 23 August 1926 (USSR) 29 October 1933 (USA)	
| runtime        = 66 minutes (1,931 metres)
| country        = Soviet Union Russian intertitles)
| budget         = 
}}
 Soviet silent comedy film starring Igor Ilyinsky and directed by Yakov Protazanov based on the play The Three Thieves ( ) by Umberto Notari. It was also released as Three Thieves in the United States.

==Plot==
To prepare grounds for yet another speculation, the banker Ornano (Mikhail Klimov) sells his house for three million rubles, but since it the weekend he cannot deposit the money in a bank and must carry it with him. He leaves the city for his country home for a short period of time, only return right away owing to his worries about the money. His wife sends a note to her lover telling him that there are three million in the house but the note, owing to the machinations of one of the three thieves on whom the film focuses, is intercepted. The note, falling into hands of the thief/adventurer Cascarilla (Anatoli Ktorov) causes him to plot his attempt to steal the money. Meantime, the small-time burglar Tapioca (Igor Ilyinsky) also chooses the same night to break into the house.

==Cast==
* Igor Ilyinsky - Tapioka
* Anatoli Ktorov - Cascarilla
* Mikhail Klimov - Ornano, the Banker
* Olga Zhiznyeva - Noris, the Bankers wife
* Nikolai Prpzprovsky  - Guido
* Vladimir Fogel - Man with binoculars
* Daniil Vvedensky

==References==
* .
* .

==External links==
* 

 

 
 
 
 
 
 
 
 


 
 