Thurakkatha Vathil
{{Infobox film 
| name           = Thurakkatha Vathil
| image          =
| caption        =
| director       = P. Bhaskaran
| producer       = A Raghunath
| writer         = KT Muhammad
| screenplay     = Madhu Jayabharathi Sreelatha Namboothiri
| music          = K. Raghavan
| cinematography = U Rajagopal Benjamin
| editing        = K Narayanan
| studio         = Sanjay Productions
| distributor    = Sanjay Productions
| released       =  
| country        = India Malayalam
}}
 1970 Cinema Indian Malayalam Malayalam film,  directed by P. Bhaskaran and produced by A Raghunath. The film stars Prem Nazir, Madhu (actor)|Madhu, Jayabharathi and Sreelatha Namboothiri in lead roles. The film had musical score by K. Raghavan.    The movie received the National Award for the best film on National Integration in 1970, second Malayalam film to win this prestigious award under the category, the first being Janmabhoomi in 1967. Philomina won Kerala State Film Award for Second Best Actress for this movie. 

==Cast==
 
*Prem Nazir Madhu
*Jayabharathi
*Sreelatha Namboothiri
*BK Pottekkad
*Bahadoor
*CA Balan Khadeeja
*Nellikode Bhaskaran
*Philomina
*Radhamani
*Raghava Menon Ragini
*Ramankutty
 

==Soundtrack==
The music was composed by K. Raghavan and lyrics was written by P. Bhaskaran.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Kadakkannin Muna Kondu || S Janaki, Renuka || P. Bhaskaran || 
|-
| 2 || Manassinullil || S Janaki || P. Bhaskaran || 
|-
| 3 || Nalikerathinte || K. J. Yesudas || P. Bhaskaran || 
|-
| 4 || Navayugaprakaashame || K. J. Yesudas || P. Bhaskaran || 
|-
| 5 || Paarvanenduvin || K. J. Yesudas || P. Bhaskaran || 
|}

==References==
 

==External links==
*  

 
 
 


 