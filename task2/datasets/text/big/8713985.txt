Manticore (film)
{{Infobox television film |
  name = Manticore |
  image =   |
  caption = DVD cover|
  writer = John Werner |
  starring = Robert Beltran Heather Donahue Chase Masterson Jeff Fahey |
  director = Tripp Reed |
  producer = Jeffery Beach, Phillip Roth |
  editing = David Flores, Tripp Reed | David C. Williams |
  network = Syfy|Sci-Fi Channel |
  released = November 26, 2005 |
  runtime = 88 minutes |
  country = United States |
  language = English, Arabic |
}}

Manticore is a Sci-Fi original movie that aired on the Syfy|Sci-Fi Channel on November 26, 2005. It was directed by Tripp Reed and featured Heather Donahue, Chase Masterson and Robert Beltran. It is about a squad of United States Army soldiers in Iraq that must fight against a resurrected, nearly unstoppable manticore awoken from its slumber by an Iraqi insurgent leader. 

==Overview== foreign occupation forces at any price. The Manticore goes on a killing rampage and soon, the only ones left are Baxter, Keats, and Ashley Pierce. The Manticore kills Ashley by spitting acid on her face before eating her alive, but Baxter and Keats manage to kill it with a camcorder and a sledgehammer.

== Cast ==
* Robert Beltran as Sgt. Tony Baxter
* Heather Donahue as Cpl. Keats
* Chase Masterson as Ashley Pierce
* Jeff Fahey as Maj. Spence Kramer
* A. J. Buckley as Pvt. Sulley
* Michail Elenov as Fathi
* Edmund Druilhet as Sgt. Cohen
* Jonas Talkington as Mouth
* Richard Gnolfo as John Busey
* Jeff M. Lewis as Ortiz
* Faran Tahir as Umari

==References==
 

==External links==
*  
*  
*   (Dead link as of 31 January 2012)

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 