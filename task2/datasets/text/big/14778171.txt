Patria (serial)
{{Infobox film
| name           = Patria
| image          = Patria adv.jpg
| caption        = Advertisement (1917)
| director       = Leopold Wharton (chapters 1-10) Theodore Wharton (chapters 1-10) Jacques Jaccard (chapters 11-15)
| producer       = Leopold Wharton Theodore Wharton William Randolph Hearst
| writer         = J.B. Clymer Charles W. Goddard Louis Joseph Vance
| narrator       =
| starring       = Irene Castle Milton Sills Warner Oland
| music          =
| cinematography = Levi Bacon John K. Holbrook Ray June Lew Tree
| editing        =
| distributor    = International Film Service
| released       =  
| runtime        = 310 minutes
| country        = United States English
| budget         = $90,000 approx.
}}
  in Patria]] serial film Argentine title for the film was La Heroina de Nueva York.

==Plot==
Spies from Japan conspire to steal the Channing "preparedness" fortune and invade the United States, beginning in New York, then allying themselves with Mexicans across the border. They are stopped by the efforts of munitions factory heiress Patria Channing and U.S. Secret Service agent Donald Parr.

==Cast==
* Irene Castle as Patria Channing, the heroine of the serial (billed as "Mrs Vernon Castle"), and a lookalike dancer in Episode 4 named Elaine
* Milton Sills as Captain Donald Parr 
* Warner Oland as Baron Huroki, the villain Dorothy Green as Fanny Adair 
* George Majeroni as Senor Juan de Lima 
* M.W. Rale as Kato
* Allan Murnane as Rodney Wrenn 
* F.W. Stewart as Edouard
* Leroy Baker
* Floyd Buckley
* Nigel Barrie
* Charles Brinley
* George Lessey
* Rudolph Valentino

==Production== President Woodrow Wilson asked Hearst to modify the serial and remove anti-Japanese material. As a result, Warner Olands character name in title cards was changed to "Manuel Morales," and the character was shown more frequently dressed in a suit, though the Japanese characters retained their kimonos in early episodes. The action was also moved across the border to Mexico beginning in Episode 11, though as is sometimes erroneously stated, Pancho Villa did not appear in the film, Baron Huroki and a new character, General Nogi, continued on as adversaries to Patria and Captain Parr until Hurokis defeat and suicide in Episode 15.

The serial was based on the novel The Last of the Fighting Channings by Louis Joseph Vance.   

Jacques Jaccard directed scenes in California while Leopold Wharton and Theodore Wharton directed from Ithaca, New York. 

==Senate hearing==
The production was investigated by a Senate committee as German propaganda after World War I.  A German propagandist, whose articles had appeared in Hearst newspapers, had written a letter to Franz von Papen explaining the scheme to use a motion picture to deprecate Japan.  Captain G. C. Lester of US Military Intelligence, testified that "Patria exploited the very idea which was set forth generally in (the propagandist) Foxs statement." {{cite book
 | last = Stedman
 | first = Raymond William
 | title = Serials: Suspense and Drama By Installment
 | year = 1971
 | publisher = University of Oklahoma Press
 | isbn = 978-0-8061-0927-5 
 | page =40
 | chapter = 2. The Perils of Success
 }} 

==See also==
* Zimmermann Telegram, a German plot for the Mexican invasion of the USA.
* List of film serials
* List of film serials by studio

==References==
 

==External links==
 
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 