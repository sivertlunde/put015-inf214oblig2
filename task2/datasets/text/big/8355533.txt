Viktor Vogel – Commercial Man
{{multiple issues|
 
 
}}

{{Infobox film
| name           = Viktor Vogel - Commercial Man
| image          = Viktor-Vogel-Commercial-Man.jpg
| image_size     = 215px
| alt            = 
| caption        = DVD cover
| director       = Lars Kraume
| producer       = Joachim von Vietinghoff
| writer         = Lara Kraume Thomas Schlesinger
| starring       = Alexander Scheer Götz George Chulpan Khamatova
| music          = Minus 8
| cinematography = Andreas Doub
| editing        = Benjamin Hembus Deutsche Columbia TriStar Filmproduktion
| released       =  
| runtime        = 108 minutes
| country        = Germany
| language       = German
| budget         = 
| gross          = 
}}
Viktor Vogel – Commercial Man is a 2001 German art comedy film released in 2001 in the US and other countries as  Advertising Rules!   Directed by Lars Kraume, the cast includes Alexander Scheer, Götz George, and Chulpan Khamatova.

==Plot==
During a meeting between Opel and the advertising agency Brainstorm, Viktor Vogel sneaks in and offers his views on how the agencys new campaign is boring and lacking in irony. This helps the already dissatisfied Opel people tell Brainstorm they need to come up with a new idea as they leave. After some discussion at Brainstorm the agency decides that they need to find Viktor, whom they threw out after the meeting, and hire him to work on the campaign. Eventually Viktor becomes the partner of Edward Kaminsky, a veteran ad man who is hoping to retire soon and they begin work on an idea that will keep Brainstorm alive through Opels money.

Meanwhile Viktor meets the artist Rosa and begins a relationship, albeit rocky at first. While at a party that Rosa has thrown, Viktor and Rosa work out an idea for a gallery exhibit where Rosa is in a shopping mart hunting (literally) for her groceries. Sometime later at a meeting with Opel, Viktor accidentally happens to blurt out the idea, which Opel loves, and they are told to begin work on the idea.

Now Viktor, torn between his work and his girlfriend, must decide what is most important to him. Edward tells him that all he has to do is pay her for the idea, but Viktor knows that for this artist the idea is more important than money. However, Rosas parents feel that they can butt into her life all the time because they give her an allowance and Viktor, seeing this as his opportunity to save himself from his mistake, tries to convince her that this is her opportunity to liberate herself from this cycle. With no exhibit Rosa is now forced to create something for the gallery that already has her booked and she ends up being turned into a dove as a magic act.

In an attempt to help out his new friend, Viktor, Kaminsky tries to get the agency to change from the hunting idea to something else, which inadvertently gets him let go from the firm. After this, Eddie hatches a plan which culminates with Viktor defecting from Brainstorm and stealing a client in the process. Viktor tries to patch things up with his girlfriend, but as she is still a dove we are left to our own imagination about how it will work out.

==Cast==
* Alexander Scheer as Victor Vogel
* Götz George as Edward Kaminsky
* Chulpan Khamatova as Rosa Braun
* Maria Schrader as Johanna von Schulenberg
* Vadim Glowna as Werner Stahl
* Nele Mueller-Stöfen as Michelle

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 