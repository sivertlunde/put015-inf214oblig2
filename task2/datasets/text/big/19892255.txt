Legend of the Bat
 
 
{{Infobox film name = Legend of the Bat image = Legend Of The Bat.jpg caption = Film poster traditional = 楚留香之二蝙蝠傳奇 simplified = 楚留香之二蝙蝠传奇 pinyin = Chǔ Líuxiāng Zhī Biǎnfú Chuánqí jyutping = Co2 Lau4 Heong1 Zi1 Ji6 Pin1 Feok1 Zyun6 Kei4}} director = Chor Yuen  producer = Mona Fong screenplay = Chor Yuen story = Gu Long starring = Yueh Hua Derek Yee Ching Li Wong Chung Candice Yu music = Frankie Chan cinematography = Wong Chit editing = Yu Siu-fung Chiang Hsing-lung  studio = Shaw Brothers Studio distributor =  released =   runtime = 101 minutes country = Hong Kong language = Mandarin budget =  gross = 
}}
Legend of the Bat, also known as Bat Island Adventure or Clans of Intrigue 2, is a 1978 Hong Kong wuxia film adapted from Bianfu Chuanqi of Gu Longs Chu Liuxiang novel series. The film was directed and written by Chor Yuen, produced by the Shaw Brothers Studio, and starred Ti Lung as the lead character. It was preceded by Clans of Intrigue (1977) and followed by Perils of the Sentimental Swordsman (1982).

==Plot==
Chu Liuxiang and Zhongyuan Yidianhong pass by a shipwreck and meet Master Kumei. They also discover that a group of highly skilled pugilists have been murdered at a gathering in Siming Mountain Manor. The only survivor is a man called Yuan Suifeng, who has become insane after being severely injured. They hear of Bat Island, a mysterious place where anyone can buy anything he desires, as long as he has money. Chu and his friends travel to the island to seek answers. However, they are not the only ones going there: Li Yuhan and his wife, Liu Wuming, are there in search of an extraordinary drug; Jin Lingzhi is there to find her father; a group of imperial guards are there to arrest the Bat Prince.

==Cast==
*Ti Lung as Chu Liuxiang
*Ling Yun as Zhongyuan Yidianhong
*Derek Yee as Bat Prince Yuan Suiyun Yueh Hua as Li Yuhan
*Ching Li as Liu Wuming
*Wong Chung as Gou Zichang
*Candice Yu as Jin Lingzhi
*Tony Liu as Yuan Suifeng
*Chan Si-gai as Li Hongxiu
*Chong Lee as Song Tianer
*Norman Chu as Xiang Feitian
*Wang Lai as Master Kumei
*Lau Wai-ling as Gao Yanan
*Ku Kuan-chung as Ding Feng
*Cheng Miu as Priest Tieshan
*Ngai Fei as Long Wu
*Yeung Chi-hing as Gongsun Jieming
*Tang Tak-cheung as Monk Wugen
*Yuen Wah
*Lam Ching-ying
*Yuen Bun
*Shum Lo
*Alan Chui
*Wong Pau-gei
*Chan Shen
*Keung Hon
*Wang Han-chen
*Lo Wai
*Chung Fat
*Tam Bo
*Lee Hang
*Yuen Yat-choh
*Chui Fat
*Yuen Shun-yee
*Wong Chi-keung
*Wan Fat
*Ng Yuen-fan
*Fong Yue

==External links==
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 

 