Joe Palooka in the Big Fight
{{Infobox film
| name           = Joe Palooka in the Big Fight
| image          = "Joe_Palooka_in_the_Big_Fight"_(1949).jpg
| image_size     =
| caption        =
| director       = Cy Endfield
| producer       = Hal E. Chester
| screenplay     = Stanley Prager
| narrator       =
| starring       = Leon Errol Joe Kirkwood Jr
| music          = Edward J. Kay
| cinematography = Mack Stengler
| editing        = Otho Lovering Fred Maguire
| studio         = Monogram Pictures
| distributor    = Monogram Pictures
| released       = 1949
| runtime        = 66 mins
| country        =
| language       = English
| budget         =
}}
Joe Palooka in the Big Fight is a 1949 comedy film directed by Cy Endfield,  based on the comic strip by Ham Fisher.  It is an entry in Monogram Pictures |Monograms low-budget, high-grossing "Joe Palooka" series.   

==Plot==
Joe is framed by gamblers who hope to fix the outcome of an upcoming boxing match. When Joe manages to clear his name, the gamblers frame the scrupulously honest boxer with murder. On the run from the law, Joe is forced to turn gumshoe and solve the murder himself--and hed better hurry if hes going to get to the Big Fight on time. 

==Cast==
*Knobby -	Leon Errol
*Joe Palooka -	Joe Kirkwood Jr David Bruce
*Maxine Harlan -	Lina Romay
*Louie -	George OHanlon
*Anne Howe -	Virginia Welles
*Grady -	Greg McClure
*Dr. Benson -	Taylor Holmes Ian Macdonald
*Talmadge -	Lou Lubin
*Pee Wee -	Bert Conway
*Lt. Muldoon -	Lyle Talbot
*Fight Secretary -	Benny Baker
*Canvas -	Eddie Gribbon
*Scranton -	Jack Roper
*Wardrobe Woman -	Frances Osborne
*Commissioner L.R. Harris -	Harry Hayden Frank Fenton George Fisher
*Frank T. Macy, Fight Manager -	Ned Glass
*Fight Promoter -	Dick Elliott
*First Referee -	John Indrisano Harry Tyler
*Mr. Howe -	Paul Maxey
*Detective Burns -	Dewey Robinson

==Critical reception==
TV Guide noted a "better-than-average "Joe Palooka" film...Making the most of realistic dialog, director Cyril Endfield moves the film along at a nice pace while holding together a number of plot lines. Surprisingly, there isnt much boxing footage in this one. 

==References==
 

==External links==
*  at IMDB

 

 