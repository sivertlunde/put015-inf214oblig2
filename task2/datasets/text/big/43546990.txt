Ghatotkachudu
{{Infobox film
| name           = Ghatothkachudu
| image          =
| caption        =
| writer         = Diwakar Babu  
| story          = S. V. Krishna Reddy K. Atchi Reddy
| screenplay     = S. V. Krishna Reddy
| director       = S. V. Krishna Reddy
| producer       = K. Atchi Reddy Kishore Rathi   Satyanarayana Ali Ali Roja Roja
| music          = S. V. Krishna Reddy
| cinematography = Sarath
| editing        = K. Ramgopal Reddy
| studio         = Manisha Films
| released       =   
| runtime        = 170 minutes
| country        = India
| language       = Telugu
| budget         =
| gross          =
}}
 Telugu comedy Roja in lead roles and Akkineni Nagarjuna, Rajasekhar (actor)|Dr.Rajasekhar in cameo appearance and music also composed by S. V. Krishna Reddy. The film recorded as Super Hit at box-office.   

==Plot==
The film deals with mythological character Ghatotkacha.

==Cast==
{{columns-list|3|
*Akkineni Nagarjuna as Chitha
*Rajasekhar (actor)|Dr.Rajasekhar as Karna Srikanth as Arjuna Satyanarayana as Ghatotkacha Ali as Ranga Roja as Roja
*Sarath Babu
*Tinnu Anand
*Brahmanandam wizard
*Tanikella Bharani as Tota Ramudu AVS
*Chalapathi Rao Rallapalli
*Giri Babu as Dharma Raja
*Prasad Babu as Duryodhana
*Chakrapani as Lord Krishna
*Jaggu as Bhima
*Vijaya Rangaraju Mallikarjuna Rao
*Gundu Hanumantha Rao Chitti Babu
*Ananth
*Sivaji Raja
*Subbaraya Sharma
*Sudha
*Rituparna Sengupta as princess Sri Lakshmi
*Baby Nikita as Chitti
}}

==Soundtrack==
{{Infobox album
| Name        = Ghatotkachudu
| Tagline     = 
| Type        = film
| Artist      = S. V. Krishna Reddy
| Cover       = 
| Released    = 1995
| Recorded    = 
| Genre       = Soundtrack
| Length      = 28:50
| Label       = Magna Sound
| Producer    = S. V. Krishna Reddy
| Reviews     = Number One   (1994) 
| This album  = Ghatotkachudu   (1995)
| Next album  = Top Hero   (1995)
}}

The music was composed by S. V. Krishna Reddy. All songs are hit tracks. Music released on MAGNA SOUND Audio Company. 
{|class="wiki"
{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 28:50
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = yes
| music_credits =

| title1  = Ja Ja Ja Roja
| lyrics1 = Bhuvanachandra SP Balu,Swarnalatha
| length1 = 5:06

| title2  = Andala Aparanji Bomma Sirivennela Sitarama Sastry SP Balu
| length2 = 4:43

| title3  = Bhamaro Nanne Pyar Karo Sirivennela Sitarama Sastry SP Balu,Swarnalatha
| length3 = 5:00

| title4  = Bham Bham Bham
| lyrics4 = Jonnavithhula Ramalingeswara Rao SP Balu
| length4 = 4:05

| title5  = Dingu Dingu Robot 
| lyrics5 = Jonnavithhula Ramalingeswara Rao  SP Balu,K. Chitra
| length5 = 4:54

| title6  = Priya Madhuram  
| lyrics6 = Jonnavithhula Ramalingeswara Rao  
| extra6  = Suresh Peters,Swarnalatha
| length6 = 5:02
}}
|}

==References==
 

 

 
 
 
 
 

 