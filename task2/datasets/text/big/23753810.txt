Paris Frills
{{infobox film
| name = Paris Frills
| director = Jacques Becker
| writer = Jacques Becker Maurice Aubergé Maurice Griffe
| starring = Raymond Rouleau Micheline Presle Jean Chevrier Gabrielle Dorziat Jeanne Fusier-Gir
| music = Jean-Jacques Grünenwald
| runtime = 110 minutes
| country = France
| language = French gross = 2,108,663 admissions (France) 
| released = 1945
}}
Paris Frills ( ), is a 1945 French drama film directed by Jacques Becker. Exteriors were shot in  the 16th arrondissement of Paris.

==Plot==
Micheline (Micheline Presle),  a young woman from the provinces, arrives in Paris to prepare for her marriage to a silk manufacturer from Lyon, Daniel Rousseau (Jean Chevrier). But she falls in love with the best friend of her husband-to-be, the fashion designer Philippe Clarence (Raymond Rouleau). He is an impenitent Don Juan who seduces her when he feels the need for some creative inspiration and then drops her just as quickly  when he comes to devote himself to a new collection. Micheline no longer feels she can go ahead and get married. A few weeks later Clarence tries to reconquer her but it is too late. She refuses. Clarence goes mad and throws himself from a window.
==References==
 
==External links==
*  

 

 
 
 
 
 

 
 