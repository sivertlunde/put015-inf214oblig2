Sad Movie
{{Infobox film
 | name         = Sad Movie
 | image        = Sad Movie.jpg
 | caption      = "Sad Movie" movie poster
 | film name    = {{Film name
 | hangul       =  
 | rr           = Saedeu Mubi
 | mr           = Saedŭ Mubi}}
 | writer       = Hwang Seong-gu   Kwon Jong-kwan
 | starring     = Jung Woo-sung Im Soo-jung Cha Tae-hyun Yum Jung-ah Shin Min-a Son Tae-young Lee Ki-woo Yeo Jin-goo
 | director     = Kwon Jong-kwan
 | producer     = Jeong Hoon-tak
 | editing      = Kim Sang-bum   Kim Jae-bum
 | cinematography = Kim Byeong-seo
 | music        = Jo Dong-ik
 | distributor  = 
 | released     =  
 | runtime      = 108 minutes
 | country      = South Korea
 | language     = Korean
 | budget       =   
 | gross        =   
}}
Sad Movie is a 2005 South Korean romantic melodrama film with a star-studded cast.    The film was released in South Korean theaters on October 20, 2005 and had a total of 1,066,765 admissions nationwide. 

==Plot==
Jin-woo is a firefighter who buys a ring for his girlfriend. Her name is Soo-jung, and she works at a TV station as a news translator for the deaf. She is waiting for him to propose, with the rationale that given his dangerous job, she likes the idea of him having to think of her, to hesitate for a while before jumping into danger. He, on the other hand, is waiting for that perfect opportunity and setting, before popping the question. 

Suk-hyun tells her unemployed boyfriend Ha-seok that she needs a more stable guy who has a good job. So Ha-seok goes off and finds himself a job helping other couples break up.

Ju-young is a mother is too busy to spend time with her young son Hee-chan, until an illness confines her to a hospital bed. There the mother and son begin to communicate more and more.

Soo-eun is a deaf girl who works as a costumed character mascot in a theme park. There she meets a young artist who she quickly begins to develop feelings for, yet she refuses to take off her mask in front of him because of her scar.

==Cast==
*Jung Woo-sung as Lee Jin-woo
*Im Soo-jung as Ahn Soo-jung
*Cha Tae-hyun as Jung Ha-seok
*Son Tae-young as Choi Suk-hyun
*Yum Jung-ah as Yeom Ju-young
*Yeo Jin-goo as Park Hee-chan
*Shin Min-a as Ahn Soo-eun
*Lee Ki-woo as Sang-gyu

==References==
 

== External links ==
*    
*  
*  
*  

 
 
 
 
 