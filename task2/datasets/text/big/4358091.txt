Symphony Hour
 
{{Infobox Hollywood cartoon
| cartoon name      = Symphony Hour
| series            = Mickey Mouse
| image             = Symphony Hour.jpg
| image size        = 
| alt               = 
| caption           = Theatrical release poster
| director          = Riley Thomson
| producer          = Walt Disney
| story artist      = 
| narrator          = 
| voice actor       = Billy Bletcher, Pinto Colvig, Walt Disney, Florence Gill, John McLeish, Clarence Nash
| musician          = 
| animator          = Jack Campbell, Les Clark, Ed Love, Jim Moore, Kenneth Muse, Riley Thomson, Bernard Wolf
| layout artist     = 
| background artist =  Walt Disney Productions RKO Radio Pictures
| release date      =   (USA)
| color process     = Technicolor
| runtime           = 6 minutes
| country           = United States
| language          = English
| preceded by       = Mickeys Birthday Party
| followed by       = Mickeys Delayed Date
}} Walt Disney RKO Radio Pictures. The cartoon depicts Mickey Mouse conducting a symphony orchestra sponsored by Pete (Disney)|Pete. The film was directed by Riley Thomson and features music adapted from the "Light Cavalry Overture" by Franz von Suppé. The voice cast includes Walt Disney as Mickey, Billy Bletcher as Pete, and John McLeish as a radio announcer.

The film marked the last theatrical appearance of Horace Horsecollar, Clarabelle Cow, and Clara Cluck for over 40 years, finally reappearing in Mickeys Christmas Carol (1983). Symphony Hour is also the last time that Mickey appeared with either Donald Duck or Goofy in a theatrical film for the same length of time.   

Symphony Hour bears similarities with the 1935 film The Band Concert. Leonard Maltin called this short a "Spike Jones version of The Band Concert". 

The soundtrack for the "ruined" version of the Light Cavalry Overture was used in the October 22, 1956 episode of The Mickey Mouse Club.

==Plot== Pegleg Pete as Mr. Sylvester Macaroni) loves the rehearsal and agrees to have it shown in concert. On the night of the performance, Goofy drops all the instruments under an elevator, causing the instruments to damage their musical sounds. Macaroni and Mickey are unaware of the mishap until the orchestra starts to "play" the damaged instruments. Throughout the outrageous concert, Mickey struggles with anxiety while Macaroni throws tantrums from inside of his private viewing room. Macaroni is reduced to tears when the concert ends, believing his reputation to be ruined. But he suddenly lightens up when he hears the thunderous applause from the audience. Clara Cluck is in the orchestra in the beginning "rehearsal" sequences but not in the actual performance at the end. Other characters appearing in this short are Donald Duck, Clarabelle Cow and Horace Horsecollar.

At one point, Donald is so fed up with the   caused by the damaged instruments that he packs his things and leaves. Mickey, determined to carry on come what may, points a gun at Donalds head to get him back into playing. This scene is missing in some versions of the short.

==Cast==
*Walt Disney &ndash; Mickey Mouse
*Billy Bletcher &ndash; Sylvester Macaroni (Pete (Disney)|Pete)
*John McLeish &ndash; Radio announcer
*Pinto Colvig &ndash; Goofy

==Production team==
;Principal animation
*Jack Campbell
*Les Clark
*George De Beeson
*John Elliotte
*Ed Love
*Jim Moore
*Kenneth Muse
*Riley Thomson
*Bernard Wolf
;Effects animation
*Joseph Gayek
*Jack Manning
*Ed Parks

==Notes==
 

==External links==
* 
 
 
 
 
 