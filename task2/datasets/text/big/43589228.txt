East Side of Heaven
{{Infobox film
| name           = East Side of Heaven
| image          = East Side of Heaven poster.jpg
| alt            = 
| caption        = Theatrical release poster David Butler
| producer       = David Butler Herbert Polesie 
| writer         = William M. Conselman James V. Kern
| story          = David Butler Herbert Polesie  Robert Kent Jerome Cowan Frank Skinner
| cinematography = George Robinson 
| editing        = Irene Morra 
| studio         = Universal Pictures
| distributor    = Universal Pictures
| released       =  
| runtime        = 88 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 David Butler Robert Kent and Jerome Cowan.   The film was released on April 7, 1939, by Universal Pictures.

==Plot==
A man finds himself the father, by proxy, of a ten-month-old baby and becomes involved in the turbulent lives of the childs family.

== Cast ==
*Bing Crosby as Denny Martin
*Joan Blondell as Mary Wilson
*Mischa Auer as Nicky
*Irene Hervey as Mona Barrett
*C. Aubrey Smith as	Cyrus Barrett Snr. Robert Kent as Cyrus Barrett Jr.
*Jerome Cowan as Claudius De Wolfe
*Baby Sandy as Sandy, the Barrett Baby
*Jane Jones as Mrs. Kelly, One of the Singing Cooks
*Rose Valyda as Singing Cook
*Helen Warner as Singing Cook
*Jack Powell as Happy Jack, the Chef
*Matty Malneck as Orchestra Leader

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 