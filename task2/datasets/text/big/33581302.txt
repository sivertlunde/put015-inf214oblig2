At Five O'Clock in the Afternoon
{{Infobox film
| name           = At Five OClock in the Afternoon
| image          = 
| caption        = 
| director       = Juan Antonio Bardem
| producer       = 
| writer         = Alfonso Sastre Juan Antonio Bardem
| starring       = Rafael Alcántara
| music          = 
| cinematography = Alfredo Fraile
| editing        = 
| distributor    = 
| released       =  
| runtime        = 103 minutes
| country        = Spain
| language       = Spanish
| budget         = 
}}
 Best Foreign Language Film at the 33rd Academy Awards, but was not accepted as a nominee. 

==Cast==
* Rafael Alcántara as Acompañante
* Ramsay Ames as Americana
* Manuel Arbó as Camarero viejo
* Matilde Artero as Encargada de servicios
* Joaquín Bergía as Policía
* José Calvo as Amigo
* Germán Cobos as José Álvarez
* Faustino Cornejo as Amigo
* Rafael Cortés as Aficionado
* Enrique Diosdado as Manuel Marcos (as Enrique A. Diosdado)
* Núria Espert as Gabriela

==Reception==
According to MGM records the film made a profit of $46,000.  . 

==See also==
* List of submissions to the 33rd Academy Awards for Best Foreign Language Film
* List of Spanish submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 


 
 