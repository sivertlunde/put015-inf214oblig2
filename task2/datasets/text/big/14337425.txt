The Invitation (film)
 
{{Infobox film
| name           = The Invitation
| image          = The Invitation Film cover.jpg
| caption        = 
| director       = Claude Goretta
| producer       = 
| writer         = Michel Viala Claude Goretta
| starring       = Jean-Luc Bideau
| music          = 
| cinematography = 
| editing        = 
| distributor    = Janus Films (USA)
| released       =  
| runtime        = 100 minutes
| country        = Switzerland
| language       = French
| budget         = 
| gross          = 
}}
The Invitation ( ) is a 1973 Swiss film directed by Claude Goretta. 

The Invitation was nominated for the Academy Award for Best Foreign Language Film    and shared the Jury Prize at the 1973 Cannes Film Festival.   

==Plot==
The Invitation tells the story of a group of office workers, one of whom inherits a large country house and invites his co-workers to a party. At the party, they gradually let go of their inhibitions and get to know one another.

==Cast==
* Jean-Luc Bideau - Maurice
* François Simon (actor)|François Simon - Emile
* Jean Champion - Alfred
* Corinne Coderey - Simone
* Michel Robin - Remy
* Cécile Vassort - Aline
* Rosine Rochette - Helene
* Jacques Rispal - René Mermet
* Neige Dolsky - Emma
* Pierre Collet - Pierre
* Lucie Avenay - Mme. Placet
* Roger Jendly - Thief
* Gilbert Costa - Linspecteur William Jacques - Le jardinier
* Daniel Stuffel - Le surnuméraire

==See also==
* List of submissions to the 46th Academy Awards for Best Foreign Language Film
* List of Swiss submissions for the Academy Award for Best Foreign Language Film

==Additional information==
This film was also released under the following titles:
*La invitación - Argentina / Spain
*Bjudningen - Sweden
*Die Einladung - West Germany
*Invitationen - Denmark (imdb display title)
*Kutsut - Finland
*Linvito - Italy
*Meghívó szombat délutánra - Hungary
*O Convite - Portugal (imdb display title)
*The Invitation - (undefined)
*Zaproszenie - Poland

==References==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 