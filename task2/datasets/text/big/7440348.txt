Neo Ned
{{Infobox film |
  name           = Neo Ned|
  image          = NeoNedDVDArtwork1.jpg |
  caption        = Original DVD Artwork for Neo Ned|
  writer         = Tim Boughn |
  starring       = Jeremy Renner Gabrielle Union Sally Kirkland Cary Elwes Ethan Suplee Eddie Kaye Thomas Giuseppe Andrews |
  director       = Van Fischer |
  distributor    = CodeBlack Entertainment Vivendi Entertainment |
  released       =   |
 runtime = 97 minutes |
 language = English |
}}
Neo Ned is a 2005 romance drama film starring Jeremy Renner, Gabrielle Union, Sally Kirkland, Cary Elwes, Eddie Kaye Thomas and Ethan Suplee. It was written by Tim Boughn and directed by Van Fischer.   

==Synopsis== hookup leads them down a shocking road to recovery.

==Cast==
* Jeremy Renner as Ned
* Gabrielle Union as Rachael
* Sally Kirkland as Shelly Nelson
* Cary Elwes as Dr. Magnuson
* Eddie Kaye Thomas as Joey 
* Ethan Suplee as Orderly Johnny
* Giuseppe Andrews as Josh
* Steve Railsback as Mr Day
* Richard Riehle as Officer Roy Pendleton
* Michael Shamus Wiles as Neds Dad

==Reception==
The film currently holds an 80% on Rotten Tomatoes. 

==Awards==
To date, the film has won 10 film awards, out of 10 total nominations.

===2005===
;Slamdance Film Festival 
*Best Narrative Feature

===2006===
;Ashland Independent Film Festival
*Best Feature Film
  
;Newport Beach Film Festival
*Outstanding Achievement in Directing, Van Fischer 
  
;Palm Beach International Film Festival 
*Best Actor, Jeremy Renner 
*Best Actress, Gabrielle Union 
*Best Director, Van Fischer 
*Best Feature

;San Diego Film Festival
*Best Feature Film
*Best Screenplay, Tim Boughn 
  
;Sarasota Film Festival 
*Best Narrative Feature

==Film Release== Starz on August 19, 2008. It was picked up for distribution by CodeBlack Entertainment, which released the film on DVD on December 2, 2008.

==References==
 

== External links ==
* 
* 

 
 
 
 
 
 
 
 