Trail of the Pink Panther
{{Infobox film
| name           = Trail of the Pink Panther
| image          = Trail of the Pink Panther poster.jpg
| caption        = Theatrical release poster
| director       = Blake Edwards Tony Adams
| screenplay     = Frank Waldman Tom Waldman Blake Edwards Geoffrey Edwards
| story          = Blake Edwards
| starring = {{plainlist|
* Peter Sellers
* David Niven
* Herbert Lom
* Richard Mulligan
* Joanna Lumley
* Capucine
* Robert Loggia
* Harvey Korman
}}
| music          = Henry Mancini
| cinematography = Dick Bush
| editing        = Alan Jones Blake Edwards Entertainment
| distributor    = United Artists|MGM/UA Entertainment Company
| released       =  
| runtime        = 96 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = $9 million   Box Office Mojo. Retrieved August 22, 2012. 
}}
Trail of the Pink Panther is a 1982 comedy film starring Peter Sellers. It was the seventh film in The Pink Panther series, and the last in which Sellers appeared as Inspector Clouseau. Sellers died before production began and the film contains no original material apart from the animated opening titles, created by DePatie-Freleng Enterprises. His performance only consists of flashbacks and outtakes from previous films.

==Plot==
When the famous Pink Panther diamond is stolen again from Lugash, Chief Inspector Clouseau (Peter Sellers) is called on the case despite protests by Chief Inspector Dreyfus (Herbert Lom). While on the case, Clouseau is pursued by the Mafia. Clouseau first goes to London to interrogate Sir Charles Lytton (having forgotten that he lives in the South of France). Traveling to the airport, he accidentally blows up his car, but mistakenly believes it an assassination attempt, and disguises himself in a heavy cast on the flight, which causes complications in the air and on land and leads to an awkward introduction to the Scotland Yard detectives at Heathrow. Meanwhile, Dreyfus learns from Scotland Yard that Libyan terrorists have marked Clouseau for assassination; but permits him to continue.
 the next film, and tries to file a complaint against Langlois with Chief Inspector Dreyfus; but Dreyfus refuses to press charges.

The film ends with Marie hoping that Clouseau might be alive, and Clouseau (played by John Taylor, only seen from behind ) is seen looking over a seaside cliff, when a seagull flies over and messes the sleeve of his coat. The words "Swine seagull!" are heard in the distinctive over French accent of Clouseau. The animated Pink Panther in trench coat and trilby hat is then revealed in place of Clouseau watching the sunset; he turns around to face the camera and flashes his coat open, but his trenchcoat reveals a montage of funny clips of Peter Sellers from his five Pink Panther films, while the end credits roll.

==Cast==
* Joanna Lumley as Marie Jouveat
* David Niven as Sir Charles Litton
* Herbert Lom as Chief Insp. Charles Dreyfus
* Burt Kwouk as Cato Fong
* Capucine as Lady Simone Litton
* Robert Loggia as Bruno Langois
* Andre Maranne as Sgt. François Chevalier
* Graham Stark as Hercule Lajoy
* Richard Mulligan as Clouseaus Father Ronald Fraser as Dr Longet
* Daniel Peacock as Clouseau age 18
* Lucca Mezzofanti as Clouseau age 8
* Colin Blakely as Alec Drummond (uncredited)
* Denise Crosby as Denise, Brunos moll

===Archive footage only=== Inspector Jacques Clouseau
* Robert Wagner as George Litton
* Claudia Cardinale as Princess Dala
* Colin Gordon as Tucker

===Previously unseen footage only===
* Harvey Korman as Prof. Auguste Balls
* Leonard Rossiter as Superintendent Quinlan
* Marne Maitland as Deputy Commissioner Lasorde Liz Smith as Martha
* Harold Berens as Hotel Clerk

==Background==
 
Sellers died over 18 months before production began, and his performance was constructed from deleted scenes from The Pink Panther Strikes Again.
 The Pink loop his own dialogue during post-production. He was dubbed by impressionist Rich Little during post-production.
 A Shot in the Dark) and Burt Kwouk as Clouseaus faithful manservant Cato. The film also featured Joanna Lumley as an investigative reporter on the trail of the missing Clouseau. Trail featured animated opening and closing credits, which were animated by DePatie-Freleng Enterprises, now Marvel Productions.
 Inspector Clouseau) does not have a cameo appearance as Clouseau in the World War II flashback.

==Critical and commercial reception== Ted Wass as Clouseau replacement Clifton Sleigh. That film would also be a critical and commercial failure.

==References==
 

==External links==
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 