A Very Serious Person
{{Infobox Film

 | name = A Very Serious Person
 | image_size = 
 | caption = 
 | director = Charles Busch Richard Guay
 | writer = Charles Busch Carl Andress
 | narrator = 
 | starring = Polly Bergen Charles Busch Dana Ivey Julie Halston Carl Andress P.J. Verhoest
 | music = Andrew Sherman
 | cinematography = Joseph Parlagreco Frank Reynolds
 | distributor = Wolfe Video
 | released = April 28, 2006
 | runtime = 95 minutes
 | country = USA English
 | budget = 
 | gross = 
 | preceded_by = 
 | followed_by = 
 | image= A Very Serious Person VideoCover.jpg
}}
 2006 drama film directed by Charles Busch.

== Plot ==
Jan (Charles Busch), an itinerant male nurse from Denmark, takes a new job with Mrs. A (Polly Bergen), a terminally ill Manhattan woman raising her parentless thirteen-year-old grandson, Gil (PJ Verhoest). Spending the summer by the shore, the emotionally reserved Jan finds himself oddly cast as a mentor to Gil in having to prepare the sensitive boy for life with his cousins in Florida after his grandmother’s death. A deep friendship grows between these two solitary people. By the end of the summer, Gil has developed a new maturity and independence, while the enigmatic Jan has revealed his own vulnerability. 

== Cast ==
{| class="wikitable" cellpadding="2"
|-
! style="background-color:silver;" | Actor
! style="background-color:silver;" | Role
|-
| Polly Bergen || Mrs. A
|-
| Charles Busch || Jan
|-
| Dana Ivey || Betty
|-
| Julie Halston || Glenda
|-
| Carl Andress || Lee
|-
| P.J. Verhoest || Gil
|-
| J. Smith-Cameron || Carol
|-
| Mick Hazen || Dave
|-
| Eric Nelsen || Dan
|-
| Frank Senger || Handyman
|-
| Carmen Pelaez || Nurse Terry
|-
| Becky London || Mrs. Kupchunas
|-
| Marvin Einhorn || Mr. Horowitz
|-
| Jonathan Ospa || Travis
|-
| Ben Roberts || Waiter
|-
| Heather Schacht || Little Girl
|-
| Simon Fortin || Gilles
|-
| Judith Hawking || Maude
|-
| Alexa Eisenstein || Crystal
|-
| Kevin Scullin || Paramedic
|-
| John McNamara || Paramedic
|-
| Bunny Levine || Mrs. Nadel
|-
| Michael McCormick || Photo Vendor
|-
| Mark Richard Keith || Larry 
|-
| Arnie Kolodner || Randy
|-
| William Charles Mitchell || Harvey
|}

== References ==

===Notes===
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 


 