Serenity (film)
{{Infobox film
| name = Serenity
| image = Serenity One Sheet.jpg
| image_size = 215px
| alt = 
| caption = Theatrical release poster
| director = Joss Whedon
| producer = Barry Mendel
| writer = Joss Whedon
| starring = Nathan Fillion Gina Torres Alan Tudyk Adam Baldwin Summer Glau Chiwetel Ejiofor  David Newman Jack Green
| editing = Lisa Lassek Universal Pictures
| released =  
| runtime = 119 minutes  
| country = United States
| language = English
| budget = $39 million    
| gross = $38,869,464 
}} space western Fox Science science fiction final episode. Set in 2517, Serenity is the story of the captain and crew of Serenity (Firefly vessel)|Serenity, a "Firefly-class" spaceship. The captain and first mate are veterans of the Unification War, having fought on the losing side. Their lives of petty crime are interrupted by a psychic passenger who harbors a dangerous secret.
 Universal Pictures. It received generally positive reviews and was #2 during its opening weekend but it did not make back its budget until its home media release. Serenity won numerous awards, including the 2006 Hugo Award for Best Dramatic Presentation.

==Plot== overpopulated Earth Alliance and Independents refusing the Operative is tasked with recapturing her.
 Malcolm "Mal" subliminal message designed to trigger Rivers mental conditioning. He notes River whispered "Miranda" before attacking and warns someone has seen the footage.
 Inara Serra, Shepherd Book. The Operative is killing all who have ever offered the crew safe haven in an attempt to flush them out, and he promises he will pursue them until he captures River.

Mal decides they must sail through the fleet of Reaver vessels to Miranda and learn what the Alliance is hiding. On Miranda, the crew discover a fully terraformed planet filled with corpses. A distress beacon leads them to a recording by an Alliance survey team, which explains that an experimental chemical designed to suppress aggression in humans was added into Mirandas air. The experiment worked too well, and most residents became so docile they allowed themselves to die. However, a small portion of the population had the opposite reaction and became exceedingly aggressive and violent, turning into the Reavers.
 Hoban Washburne is killed when Reavers attack immediately after. The crew flees, and they plan to make a stand against the Reavers to buy Mal time to broadcast the recording. Through a message recorded by Mr. Universe before his death, Mal learns of a backup transmitter. After sustaining heavy injuries, the crew retreats behind a set of blast doors, which do not properly close. A Reaver shoots through the opening and severely wounds Simon, prompting River to charge through the doors and close them as the Reavers drag her away.

In a confrontation at the backup transmitter, Mal disables the Operative and forces him to watch the recording as it is broadcast. Mal returns to the crew, and the blast doors open to reveal River has killed all the Reavers. Alliance troops reach the group, but the disillusioned Operative orders them to stand down.
 Kaylee Frye. The Operative tells Mal the broadcast weakened the Alliance government. While he will try to convince them that River and Simon are no longer threats, he cannot guarantee the Alliance will end their pursuit. Inara decides that she will remain with the crew, and Serenity takes off with River as Mals co-pilot.

==Cast==
 ,  .)]]
  Malcolm "Mal" Reynolds, a former sergeant (now captain of his privately owned ship) on the losing side of the Unification War, he struggles to survive free and independent of the Alliance. Zoe Washburne (née Alleyne): A former corporal who fought under Mal in the war, and Washs wife. She is the second-in command of Serenity, and is fiercely loyal to Mal, whom she addresses as "sir". Hoban "Wash" Washburne: The pilot of the ship, and Zoes husband. He often acts as a voice of reason on the ship. Companion who formerly rented one of Serenity  s shuttles. In one of the Operatives traps, Mal is reunited with Inara at her training house, and the two escape back to Serenity.
* Adam Baldwin as Jayne Cobb, a mercenary skilled with weapons, Jayne is often the "main gun" for jobs and is someone who can be depended on in a fight.  Jayne acts and seems dumb most of the time, but may be smarter than he lets on. Whedon, Firefly: the complete series: "Serenity" commentary  As Whedon states several times, he is the person that will ask the questions that no one else wants to.  Kaywinnet Lee "Kaylee" Frye:     the ships mechanic, has an intuitive, almost symbiotic, relationship with machines and is, consequently, something of a mechanical wizard. She is also notable for a persistently bright and sunny disposition, and her crush on Simon Tam. Simon Tam, Rivers loving older brother who helped rescue her from the Alliance. He and River are taken in by the crew of Serenity. A trauma surgeon before the rescue, he serves as a doctor to the crew. His life is defined by his sisters needs. 
* Summer Glau as River Tam, a 17-year old psychic genius. She and her brother are taken in by the crew of Serenity after he rescues her from an Alliance Academy where she was subjected to medical experimentation and brainwashing. The Alliances pursuit of River acts as the films motive. More abstractly the film is the "story of Mal as told by River".  Shepherd Derrial Book, a shepherd, or preacher, with a mysterious past, Book was once a passenger on Serenity, but now resides on the planet Haven. Mal and the crew look to him for help. The Operative, a ruthless intelligence agent of the Alliance assigned to track down River and Simon. Although Ejiofor was on the top of the casting directors list for the role, the studio wanted someone better known. Whedon was eventually able to cast Ejiofor. 
* David Krumholtz as List of minor characters in the Firefly universe#Mr. Universe|Mr. Universe, a "techno-geek" with good relations with the crew of Serenity, especially Wash, Mr. Universe lives with his "love-bot" wife and monitors incoming signals from around the universe.

==Production==

=== Development ===
The film is based on Firefly (TV series)|Firefly, a television series canceled by the Fox Broadcasting Company in December 2002, after 11 of its 14 produced episodes had aired.  Attempts to have other networks acquire the show failed,    and creator Joss Whedon started to sell it as a film.  He had been working on a film script since the shows cancellation.  Shortly after the cancellation, he contacted Barry Mendel, who was working with Universal Studios, and "flat-out asked him" for a way to continue the series as a film, including as a low-budget television film. Mendel introduced Whedon to then Universal executive Mary Parent. She had seen Firefly and immediately signed on to the project, even though Whedon had yet to create a story.  Whedon remarked,  In July 2003, Whedon said that though there was interest in the project, "I wont know really until I finish a draft whether or not its genuine."    He felt that any film deal was contingent on keeping the shows original cast,  though he later stated that retaining the cast was "never an issue" as Universal executives believed the cast suitable after watching every episode of the series.   

In early September 2004, a film deal with Universal was publicly confirmed. Universal acquired the rights to Firefly shortly before the confirmation.  Whedon felt that the strong sales of the Firefly DVD, which sold out in less than twenty-four hours after the pre-order announcement,    "definitely helped light a fire and make them   go, Okay, weve really got something here. It definitely helped them just be comfortable with the decisions they were making, but they really had been supporting us for quite some time already."   

===Writing===
After Universal acquired the film rights from Fox, Whedon began writing the screenplay. His task was to explain the premise of a television series that few had seen without boring new viewers or longtime fans. He based his story on original story ideas for Firefly s un-filmed second season. Serenity Collectors Edition DVD cast commentary  Whedons original script was 190 pages, and attempted to address all major plot points introduced in the series. After presenting the script to Barry Mendel under the title "The Kitchen Sink", Whedon and Mendel collaborated on cutting down the script to a size film-able under his budget constraints.  The tightened script and a budget Mendel and Whedon prepared were submitted to Universal on a Friday and on the following Monday morning, Stacey Snider, then head of Universal, called Mendel to officially greenlight the movie. 

Universal planned to begin shooting in October 2003, but delays in finishing the script postponed the start of shooting to June 2004. 

===Filming===
Universal, while on board with the film, was not willing to spend the typical $100 million for a story set in space. Whedon convinced them he could do it for less money, and do it in 50 days, instead of the usual 80.    On March 3, 2004, the film was given the greenlight to enter production with a budget of under $40 million.  Typically, production would save money by shooting outside of Los Angeles, but Whedon insisted on filming locally.

Principal photography began on June 3, 2004. Whedon announced the film would be titled Serenity to differentiate it from the TV series.     (Whedon also mentions in the Serenity DVD commentary that Fox still owned the rights to the name "Firefly".)  All nine principal cast members from the television series returned for the movie, although Glass and Tudyk could not commit to sequels, leading to the death of their characters in the script.  Stunt coordinator Chad Stahelski, a student of Jeet Kune Do under Dan Inosanto, created a customized fighting style for Summer Glau to use in the films fight scenes. It was a hybrid of Kung Fu, kickboxing and elements of ballet, all combined to create a "balletic" martial art.  

One cost-cutting item that could not be reused from the television show was the original set of the interior of the spaceship Serenity, which had to be entirely rebuilt based on images of the Firefly DVD set.  The set for the failed colony, Miranda, was filmed on location at Diamond Ranch High School in Pomona, California. 

On September 17, 2004, Whedon announced on the films official website that shooting had been completed. 
 long steadicam shot of several minutes to establish "safety", Whedon, Serenity: Directors Commentary, track 1 "Living Weapon"  as well as (re-)introduce every character aboard the ship and touch on their personalities and motivations.

Serenity was also the first film to be screened digitally, fully Digital Cinema Initiatives|DCI-compliant. 

===Design===
  influenced clothing and weaponry in Firefly and Serenity.]]
Comic book artist Bernie Wrightson, co-creator of Swamp Thing, contributed concept drawings for the Reaver (Firefly)|Reavers.  Other comic book artists who contributed to the production design include Joshua Middleton and Leinil Francis Yu (Visual Companion).

Serenity costumes are influenced by   organization within the series (in reality, reused uniforms from  , Serenity goes for an occasional underdone look, or "used future", as Star Wars creator George Lucas refers to it.   

 

This future envisioned in Serenity has two political and cultural centers: Anglo-American and Chinese. Characters all speak English and Standard Mandarin|Mandarin, with the latter language reserved for the strongest curse words. 

===Visual effects===
As the budget for the film was considerably smaller than for other films, practical special effects were used as much as possible: if a   and CGI, much like those used in the pod race in  , were quickly ruled out, creating a challenge for the production team to find an alternative.  Instead, the crew fashioned a trailer with a cantilevered arm attached to the "hovercraft" and shot the scene while riding up Templin Highway north of Santa Clarita.  Serenity visual effects supervisor Loni Peristere told the Los Angeles Times, "Traditionally this would have been, like, a 30-day shoot. I think we did it in five."  Zoic Studios, the company that produced the graphics for the series, had to perform a complete overhaul of their computer model of Serenity, as the television model would not stand up to the high-definition scrutiny of cinema screens (and high-definition video resolution).    

===Musical score===
  Chinese and other Asian musical elements. He wanted the musical elements "mixed up, hidden, or its as much a cliché as the western feel. We dont want to be too specific about culture or time. We want to be comfortable enough with the sounds not to let them take us out of the story, but not so comfortable that we begin to be told where the story is." Music was to draw heavily on what could be carried, and he highlighted four instruments: voice, percussion, woodwind, strings particularly guitar. He cautioned against vocal orchestration, believing there to be only two voices in Hollywood and wishing to avoid both, and advised moderation in woodwind, feeling wind instruments to be "either too airy or too sophisticated."   

Universal Studios wanted a composer with experience scoring films, ruling out Firefly  composer Greg Edmonson. Whedon first thought of Carter Burwell, of whom Whedon was a fan. However, Whedon later felt that Burwell was not the right choice because as the film changed, the needs of the score changed as well.    Burwell found working on the project difficult as it required he work "opposite" to his usual approach.    The production would have continued with Burwell, but his other obligations left him little time to compose an entirely new score for Serenity.  Burwell was dropped from the project a few weeks before the scheduled February 2005 recording. 
 David Newman was recommended by Universals music executives when Whedon requested a composer capable of "everything" and "quickly." Whedons instructions to Newman for the ship Serenity  theme was something homemade and mournful, evoking the idea of pioneers who only had what they could carry. Whedon wished the theme to let viewers know they were now home. River Tams theme was played on a uniquely shaped, square, antique piano that was slightly out of tune. The piano reminded Newman of River and composed a "haunting, haunted, vaguely eastern and achingly unresolved" theme that Whedon felt proved Newmans understanding of the films musical needs.   

The score was performed by the Hollywood Studio Symphony under Newmans direction.  The official soundtrack was released September 27, 2005. 

==Release==
Serenity had its world premiere at the Edinburgh International Film Festival on August 22, 2005.     The premiere sold out,  and the festival arranged for two more screenings on August 24, which sold out in twenty-four hours,     and in the "Best of the Fest" line-up on August 28.  The film was theatrically released September 30, 2005. 

===Marketing===
In April 2005, Universal launched a three stage grassroots marketing campaign. A rough cut of the film was previewed in a total of thirty-five North American cities where the Firefly television series received the highest Nielsen ratings. The screenings did not bear the name of the film and relied on word-of-mouth within the fanbase for promotion. All screenings sold out in less than twenty-four hours, sometimes in as quickly as five minutes.    The first screening was held May 5, 2005 in ten cities.  The second screening on May 26 increased the number of cities to twenty. In the twenty-four hours following the announcement of the second screening, the Firefly fanbase launched trial and error efforts to uncover the theaters holding the screenings, leading the event to be sold out before the official listing was released.  The third screening on June 23 was held in thirty-five cities.  A final screening was held at Comic-Con International, followed by a panel with Whedon and the cast. 

====Session 416  ====
Session 416, also known as the R. Tam Sessions,    are a series of five short videos anonymously released by Whedon through various websites and message boards as a form of viral marketing.    The first video, bearing the title card "R. Tam, Session 416, Second Excerpt", was released on the iFilm website on August 16, 2005.    By September 7, 2005, all five videos had been released.    Though a representative from Universal Studios stated to have no knowledge of the videos origin,  the idea to launch an online viral marketing campaign came first from Universal executives. After they approached Whedon with the idea, he decided to use the format to explore events before either the film or the television series.    The clips were filmed with a "tiny" crew in a single day shoot and are shot in grainy, low quality, black-and-white.     They were later included on the Collectors Edition DVD. 

The videos, sequenced out of chronological order, depict excerpts of counseling sessions between River Tam, played by Summer Glau, and her unnamed therapist, played by Whedon, while she is held at the Alliance Academy.    They follow her change from shy and sweet child prodigy to the mentally unstable girl of the television series. 

===Home media=== UMD on December 20, 2005.   The DVD ranked #3 in sales for the week ending December 25, 2005.  Bonus features available on the DVD version audio commentary from Whedon, deleted scenes and outtakes, a short introduction by Whedon originally preluding the film at advance screenings, an easter egg on the creation of the Fruity Oaty Bar commercial, and three featurettes on the Firefly and Serenity universe, special effects, and the revival of the television series to film.  Region 2 releases included an additional making-of featurette,   and Region 4 releases included additional extended scenes, a tour of the set, a feature on cinematographer Jack Green, and a question and answer session with Whedon filmed after an advance screening in Australia.   
 its title key was ripped from a software player and released online. 

A 2-disc Collectors Edition DVD was released for Region 1 on August 21, 2007. In addition to the special features featured on the Region 4, with exception of the question and answer session, the release included Session 416, a documentary on the film, and a second commentary with Whedon and actors Nathan Fillion, Adam Baldwin, Summer Glau, and Ron Glass.  The film was released on Blu-ray on December 30, 2008, adding to the special features a video version of the cast commentary, picture-in-picture visual commentary, a two databases of in-universe material, and a digital tour of Serenity. 

==Reception==

===Box office===
Despite critical acclaim and high anticipation, Serenity performed poorly at the box office. Although several pundits predicted a #1 opening,    the film opened at #2 in the United States, taking in $10.1 million on its first weekend, spending two weeks in the top ten, and closed on November 17, 2005 with a domestic box office gross of $25.5 million.  Movie industry analyst Brandon Gray described Serenitys box office performance as "like a below average genre picture". 

Serenity s international box office results were mixed, with strong openings in the UK, Portugal and Russia, but poor results in Spain, Australia, France and Italy. United International Pictures canceled the films theatrical release in at least seven countries, planning to release it directly to DVD instead. The box office income outside the United States was $13.3 million,  with a worldwide total of $38.9 million,  slightly less than the films $39 million budget, which does not include the promotion and advertising costs.

===Critical reception===
 
Serenity received mostly positive reviews from film critics. Rotten Tomatoes gives the film a "Certified Fresh" score of 82% based on 179 reviews. The sites consensus is "Snappy dialogue and goofy characters make this Wild Wild West soap opera in space fun and adventurous."  Metacritic gives the film a weighted average score of 74% based on reviews from 34 critics indicating generally favorable reviews. 
 Ebert and Ebert in his review on the Chicago Sun-Times gave the film three out of four stars, commenting that "  is made of dubious but energetic special effects, breathless velocity, much imagination, some sly verbal wit and a little political satire". "The movie plays like a critique of contemporary society", he observed, also stating that in this way it was like Brave New World and Nineteen Eighty-Four.   
The San Francisco Chronicle called it "a triumph",    while The New York Times described it as a modest but superior science fiction film.  
Science fiction author Orson Scott Card called Serenity "the best science fiction film ever", further stating "If Enders Game cant be this kind of movie, and this good a movie, then I want it never to be made. Id rather just watch Serenity again."  In December 2005, it was named the best film of the year by viewers of Film 2005 and ranked #383 on Empire magazines list of "The 500 Greatest Movies of All-Time" in 2008. 

Some reviewers felt the film was unable to overcome its television origins, and did not successfully accomplish the transition to the big screen. USA Today wrote that "the characters are generally uninteresting and one-dimensional,   and the futuristic Western-style plot grows tedious" while Variety (magazine)|Variety declared that the film "bounces around to sometimes memorable effect but rarely soars". 

===Awards===
 
* Film of the year awards from Film 2005    and FilmFocus.    
* IGN Films Best Sci-Fi, Best Story and Best Trailer awards and runner up to Batman Begins for the Overall Best Movie   
* Won the 7th annual "User Tomato Awards" for Best Sci-Fi Movie of 2005 at Rotten Tomatoes.
* Won Nebula Award for Best Script for 2005.
* 2006 Viewers Choice Spacey Award for favorite movie.
* Voted as the Best Film of 2006 by the writers for the website Box Office Prophets and it came 12th Best film of 2006 in the websites readers poll.
* Won Best Dramatic Presentation, Long Form at the 2006 Hugo Awards.    Prometheus Special Award. 
* SyFy Genre Awards 2006: {{Cite web|url= archiveurl = archivedate = 2006-11-07}} 
** Best Movie Runner-Up
** Best Actor/Movie Runner-Up: Nathan Fillion
** Best Actress/Movie Runner-Up: Summer Glau
* SFX magazine s best sci-fi movie of all time.   

=== Cultural impact ===
NASA astronaut Steven Swanson, a fan of the show,  took the Region 1 Firefly and Serenity DVDs with him on Space Shuttle Atlantis STS-117 mission, which lifted off on Friday June 8, 2007. The DVDs will permanently reside on the International Space Station as a form of entertainment for the stations crews. 
 Node 3 of the International Space Station; NASA-suggested options included Earthrise, Legacy, Serenity, and Venture.  At the March 20, 2009 poll close, Serenity, led those four choices with 70% of the vote, though the winner of the poll was Colbert, a reference to late night comedy show host Stephen Colbert. In the end, the poll was discarded and the node was eventually named Tranquility.   

==== Charity screenings ====
Beginning in January 2006, fans (with Universals blessing) began organizing charity screenings of Serenity to benefit Equality Now, a human rights organization supported by Joss Whedon. By mid-June, 41 such screenings had been confirmed for cities in Australia, Canada, England, New Zealand, and the United States, and as of June 19, 2006, there were 47 scheduled screenings. The project was referred to as "Serenity Now/Equality Now" on the official website, but was often referred to in shortened form as "Serenity Now", and was coordinated through  .  The name officially changed in 2007 to Cant Stop The Serenity (CSTS)

This has become a multi-venue event held each calendar year in various countries and cities and on various dates throughout the year. Funds raised by the events go to Equality Now (and other charities ).

==Related works and merchandise==

Universal Studios wished to do an animated prequel to the Serenity film. Whedon and   was released from July through September 2005 by Dark Horse Comics.     

A novelization of the film was written by   released a one-shot souvenir magazine.     Margaret Weis Productions released the Serenity Role Playing Game|Serenity tabletop role-playing game based on the film on September 19, 2005.  
 trading card set, including autographed cards and cards with swatches of costumes used in the film, on September 21, 2005.   The set won Diamond Comics 2005 Non-Sports Card Product of the Year Gem Award.  Diamond Select Toys released five six-inch action figures initially featuring Malcolm Reynolds, Jayne Cobb, and a Reaver,    later adding River Tam,  Inara Serra,  and Zoe Washburne.  The line was deemed to be "disappointing" with the figure of Malcolm Reynolds particularly singled out; both won MCWToys silver award for Worst Line and Worst Male Figure under twelve inches for 2005. 

==Themes and cultural allusions==
 While the film depicts the Alliance as an all-powerful, authoritarian-style regime, Whedon is careful to point out that it is not so simple as that. "The Alliance isnt some evil empire," he explains, but rather a largely benevolent bureaucratic force. The Alliances main problem is that it seeks to govern everyone, regardless of whether they desire to belong to the central government or not.  What the crew of Serenity represent—specifically Mal and his lifestyle—is the idea that people should have the right to make their own decisions, even if those decisions are bad. Whedon, Serenity: Directors Commentary, track 17 "Fighting for Belief" 

The Operative embodies the Alliance and is, as Whedon describes, the "perfect product of whats wrong with the Alliance". He is someone whose motives are to achieve a good end, a "world without sin". The Operative believes so strongly in this idea that he willingly compromises his humanity in furtherance of it—as he himself admits, he would have no place in this world. In contrast, Mal is, at the movies beginning, a man who has lost all faith.  By the end of the movie Mal has finally come to believe so strongly in something—individual liberty—that he becomes willing to lay down his life to preserve it.  Whedon, Serenity: Directors Commentary, track 10 "Posing a Threat". 
 The Tempest, who in Act V, scene I says: "O brave new world, / That has such people int!"  The Alliance had hoped that Miranda would be a new kind of world, filled with peaceful, happy people, and represents the "inane optimism of the Alliance". 

The Fruity Oaty Bar commercial is partially inspired by Mr. Sparkle, the mascot of a fictional brand of dish-washing detergent, who was featured in The Simpsons episode "In Marge We Trust".  Whedon mentions in a DVD feature that when the Fruity Oaty Bar commercial was being designed, he constantly asked the animators to redesign it and make it even more bizarre than the previous design, until it arrived at the version presented on screen.

==Sequel possibilities==
Fans had hoped that if Serenity had been successful, it might lead either to a revival of the television series or a film franchise.   The former was always unlikely, since Fox still owns the Firefly television rights and Joss Whedon reportedly refused to work for Fox again. 
 IGN Filmforce reported that Universal had expressed an interest in making a Serenity TV movie for broadcast on the Syfy|Sci-Fi Channel (which is owned by Universal), and eventual DVD sale. It was expected that commissioning of a television sequel would be contingent on strong DVD sales of Serenity.  In a January 2006 interview, Whedon doubted the chances of a sequel. 

On October 1, 2006, Whedon posted a comment to the Whedonesque.com website, responding to a rumor that he was working on a sequel to Serenity. He wrote,  

Whedons response to the rumor consequently sparked many websites to publish articles stating that he would never work on a sequel to Serenity. Whedon again returned to Whedonesque.com to respond to the new stories and wrote,
 after all those years as a movie writer, youd think Id be prepared for another lesson on my unimportance in the scheme of things, but I wasnt.... All these rumor of projects or the death of projects... When the two worlds align and something actually happens, whatever it is, you guys know Ill be on this site as soon as Im allowed to be. And Ill be very very clear. There is no news. Not never, just now. }}

In an interview at the 2007 Comic-Con International|Comic-Con, Whedon stated that he believes hope for a sequel rests in the sales of the Collectors Edition DVD.   In an August 2007 interview with Amazon.com prior to the Collectors Edition DVD release, Whedon stated, "Its still on my mind, I mean, but I dont know if mine is the only mind that its on." He later said, "You know, whether or not anybody whos involved would be available at that point everybodys working, Im happy to say is a question, but whether I would want to do another one is not a question."  On October 4, 2007, Alan Tudyk suggested in an interview that Universal was considering another film due to DVD sales,  although Joss Whedon later discounted Tudyks statement as "wishful thinking". 

==References==
 

==Additional reading==
* 
* 
* 

==External links==
 
*  
*  
*  
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 