A Gentle Breeze in the Village
{{Infobox animanga/Header
| name =
| image =  
| caption = Cover of Tennen Kokekkō volume 14 as published by Shueisha
| ja_kanji = 天然コケッコー
| ja_romaji = Tennen Kokekkō
| genre = Slice of life
}}
{{Infobox animanga/Print
| type = manga
| author = Fusako Kuramochi
| publisher = Shueisha
| demographic = shōjo manga|Shōjo
| magazine =
| first = 1994
| last = 2000
| volumes = 14
| volume_list =
}}
{{Infobox animanga/Video
| type = live film
| director = Nobuhiro Yamashita
| producer =
| writer = Aya Watanabe
| music =
| studio =
| released = July 24, 2007
| runtime = 121 minutes
}}
 

A Gentle Breeze in the Village, also known as   is a Japanese slice of life shōjo manga|shōjo manga series written and illustrated by Fusako Kuramochi,   serialized in the magazine Chorus from 1994 to 2000. The manga won the 20th Kodansha Manga Award in 1996.

It was made into a movie in 2007, directed by Nobuhiro Yamashita starring Kaho (actress)|Kaho, and it was released on July 24, 2007.

==Plot summary==
One of six students in a combined primary and junior high school, Soyo Migita (Kaho (actress)|Kaho) is the most senior pupil. For her, school happens to be a joyful experience with an extended family of loving little brothers and sisters but her days as the tallest and oldest student are soon to be over. Hiromi Osawa (Masaki Okada), a cool city boy from Tokyo, arrives in the village. Attracted to him, Soyo tries to ignore the foreign feelings that begin to occupy her heart and mind. However, she soon surrenders to her passion and learns to act upon her newfound emotions.

==Main characters==
*  - Birth: May 18
*  - Birth: December 12
:A boy who came from Tokyo, and became Soyos first classmate. He later becomes Soyos boyfriend. He has a very good sense of fashion.
* 
:One grade younger than Soyo, but her best friend. She wants to be a manga illustrator. She uses Hiromi as a model for the male character in her story.
* 
:One grade younger than Soyo, but her best friend. His house is a store.
* 
:Mother of Soyo.

==Film==
The film was shown at the 2007 Toronto International Film Festival during the Contemporary World Cinema programme. It was later shown at the 28th Yokohama Film Festival.

This film was ranked as the 2007 Asahi Best Ten Film Festival Number 1, 2007 Japan Movie Best Ten Number 2, and as the 2007 Yokohama Film Festival Japan Movie Best Ten Number 2. Kaho has won two new actress awards from this film, including the Best New Actress award.

===Cast=== Kaho as Soyo Migita
*Masaki Okada as Hiromi Osawa
*Erisa Yanagi as Ibuki Taura
*Shoko Fujimura as Atsuko Yamabe
*Yui Natsukawa as Itoko Migita
*Kōichi Satō as Atsukos Father

===Staff===
*Film director - Nobuhiro Yamashita
*Filming - Tatsuto Kondō
*Music - Rei Harakami
*Theme songs - "Kotoba wa Sankaku Kokoro wa Shikaku" by Kururi

===Filming location===
*Shimane prefecture

==References==
 

==External links==
*   
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 


 