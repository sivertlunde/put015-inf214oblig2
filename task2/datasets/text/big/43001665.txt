RajadhiRaja
{{Infobox film
| name            = RajadhiRaja (The King of Kings)
| image           = File:RajadhiRaja.jpg
| caption         = Film poster
| director        = Ajai Vasudev
| cinematography  = Shaji
| editing         = Mahesh Narayanan Udayakrishna Siby K. Thomas Siddique Mukesh Khanna Taniya Stanley
| producer        = M. K. Nasser Stanly Silvester Consera
| music           = Karthik Raja Berny-Ignatius Gopi Sunder  (Background score) 
| distributor     = Play House Release PJ Entertainments  (Europe)  Nanma cinemas & Tricolor Entertainments  (Australia) 
| released        =   
| runtime         = 
| country         = India
| language        = Malayalam
| budget          = 
| gross           = 
}}
 2014 Malayalam Lena and Taniya Stanley in major supporting roles.  The film released on September 5, 2014. 

The film is the story is about Shekarankutty (Mammootty) was leading a happy family life with his wife (Raai Laxmi) and his daughter.   In his past, Shekharankutty had led a very brutal life and was the notorious don Rajashekhar.  

==Cast==
 
* Mammootty as Shekharan Kutty.
* Raai Laxmi as Radha.
* Joju George as Ayyappan. Siddique
* Mukesh Khanna Lena
* Taniya Stanley Nawab Shah
* Raza Murad
* Rahul Dev
* Unni Mukundan (Guest appearance) 
* Shamna Kasim (Guest appearance) 
 


==Critical reception==
Sify  said, "Rajadhi Raja is a mimicry of some tasteless masala flicks, which are seriously outdated. This one has perhaps been aimed at the hardcore fans of the superstar."  IndiaGlitz gave six out of ten and said "Rajadhiraja is an good Thriller" and stated "In the final analysis, Rajadhiraja doesnt offer any freshness in narratives. But it can be a fine for an one-time watch, if you are not in theatres with any big expectations."  Paresh C Palicha of Rediff gave two stars out of five with a caption "Rajadhi Raja fails to impress" and wrote "Rajadhi Raja proves to be a lacklustre Onam release"  Unni R Nair of Kerala9 gave two out of five and stated "Just about good entertainment watchable if you know what to expect..."  Deepa Soman of The Times of India gave 3.5 out of 5 and said "Rajadhiraja is a winsome film with enough moments to entertain you this Onam season." 

==References==
 

==External links==
*  

 
 
 