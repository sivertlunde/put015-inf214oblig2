Hey! Hey! USA
{{Infobox Film
| name           = Hey! Hey! USA
| image          = "Hey!_Hey!_USA"_(1938).jpg
| image_size     =
| caption        = 
| director       = Marcel Varnel
| producer       = Michael Balcon
| writer         = J.O.C. Orton
| narrator       =  David Burns Charles Williams
| cinematography = Arthur Crabtree
| editing        = R.E. Dearing
| distributor    = Gainsborough Pictures
| released       = October 1938
| runtime        = 80 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          =
| preceded_by    = 
| followed_by    = 
}}
Hey! Hey! USA is a 1938 British comedy film starring comedian Will Hay, Edgar Kennedy and Eddie Ryan.  Hay appears as Benjamin Twist, a porter who accidentally finds himself on a ship bound for the United States. The film features an early appearance by child actor Roddy McDowell, before he went to live in America.   

==Plot outline==
 Benjamin Twist, a teacher working during school holidays as a ships porter ends up on a ship bound for America and impersonating a professor, Phineas Tavistock. Along with American gangster and stowaway Bugs Leary (Edgar Kennedy), Twist finds himself entangled in a plot to kidnap the son of a millionaire whom Professor Tavistock is teaching.  Things are further complicated by the fact that two sets of gangsters are attempting to get their hands on the ransom money, which Twist is given to hand over.

==Cast== Benjamin Twist/Professor Phineas Tavistock
* Edgar Kennedy - Bugs Leary David Burns - Tony Ricardo
* Eddie Ryan - Ace Marco
* Fred Duprez - Cyrus Schultz
* Paddy Reynolds - Mrs Schultz
* Tommy Bupp - Bertie Schultz
* Arthur Goullet - Glove Johnson
* Gibb McLaughlin - Steward
* Eddie Pola - Broadcast Announcer
* Roddy McDowall - Boy
* Peter Gawthorne - Ships Captain Charlie Hall - Learys Pal Charles Oliver - Curly Danny Green - McGuire, the Chicago cop

==Critical reception==
Sky Movies wrote, "incomparable Will Hay reprises his splendidly shifty Dr Benjamin Twist character (the incompetent headmaster of St Michaels) in this breezy British comedy set in a quaintly observed America full of gun-toting gangsters. Comic stalwart Edgar Kennedy provides slow-burning support under the direction of Marcel Varnel, the dapper Frenchman who made most of Hays biggest successes. "  

==Bibliography==
* Slide, Anthony. Banned in the USA: British Films in the United States and their Censorship, 1933-1960. I.B. Tauris & Co, 1998.

==External links==
* 
* 

==References==
 

 

 
 
 
 
 
 
 
 


 