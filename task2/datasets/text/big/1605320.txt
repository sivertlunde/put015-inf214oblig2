The Woodsman
{{Infobox film name           = The Woodsman image          = The Woodsman movie poster.jpg alt            =  caption        = Theatrical release poster director       = Nicole Kassell producer       = Lee Daniels screenplay     = Steven Fechter Nicole Kassell based on       =   starring  Eve David Alan Grier music  Nathan Larson cinematography = Xavier Pérez-Grobet editing        = Lisa Fruchtman Brian A. Kates studio         = Dash Films distributor  Universal Pictures released       =   runtime        = 87 minutes http://www.boxofficemojo.com/movies/?id=woodsman.htm  country        = United States language       = English budget         = $2.5 million gross          = $4.7 million 
}} child molester who must adjust to life after prison.

==Plot== child molester, returns home to Philadelphia after serving 12 years in prison. His friends and family have abandoned him, with the exception of his brother-in-law, Carlos (Benjamin Bratt). Walters apartment is just across the street from an elementary school—an obvious source of temptation. He gets a job at a local lumber mill and meets Vicki (Kyra Sedgwick), one of the few women working there. After sleeping with Vicki, Walter reveals his history of molesting little girls. Vicki is clearly shocked and disturbed by this new information, but before she can consider how to respond to it, Walter orders her out of the apartment.

Walter receives frequent visits from a suspicious, verbally abusive police officer named Lucas (Mos Def). Lucas makes it clear that he is waiting to catch Walter reoffending. Watching the school, Walter sees a man offering candy to the children in an apparent effort to gain their confidence. He realizes that this man, whom he nicknames "Candy" (Kevin Rice), is another child molester. Walter also meets an apparently lonely young girl named Robin (Hannah Pilkes) who is a bird-watcher. Walter sees Candy abduct one of the children; however, he does not report this to the police. A visit from Lucas, who is unaware of the crimes committed near the school, takes its toll upon Walters morale. Walters life takes a further downturn when a suspicious co-worker, Mary-Kay (Eve (entertainer)|Eve), learns of his conviction and alerts the entire mill. Some of the employees attack Walter, but Vicki and the boss of the mill come to his defense.

Ostracized and frustrated, Walter leaves his workplace and goes to the park. Vicki, fearing the worst, begins to search for him. Walter ends up meeting with Robin at the park. As they talk, he begins to succumb to his desires and invites Robin to sit on his lap. She politely refuses, but then begins to confide in him. As she begins to cry, Walter realizes that she is being abused by her father. In her anguish, and sensing a similarity between her father and Walter, she offers to sit on Walters lap, wanting his approval. Walter undergoes an epiphany, realizing the harm child molestation causes, which hed heretofore rationalized as being something the victims wanted. Walter tells Robin to go home and, as she leaves, she gives him a hug. On his way home, he sees Candy dropping off a young boy near the school at night. In a fit of rage and self-hatred, Walter gives him a thorough beating. Afterwards, he goes to Vickis home, and she accepts him.
 raping a young boy. Lucas decides not to charge Walter with the assault. With Carlos help, Walter is reunited with his sister, whom he has not seen in years. However, she refuses to forgive him and leaves. In a voice-over discussion in which his therapist (Michael Shannon) tells him that eventual forgiveness may take several years, Walter replies that he understands and accepts her anger, and expresses optimism for his own future.

==Cast==
*Kevin Bacon as Walter
*Kyra Sedgwick as Vicki
*Mos Def as Lucas
*Benjamin Bratt as Carlos Eve as Mary-Kay
*David Alan Grier as Bob
*Hannah Pilkes as Robin
*Kevin Rice as "Candy"
*Michael Shannon as Rosen
*Carlos Leon as Pedro
*Jessica Nagle as Annette

==Production==
The movie was shot in Philadelphia, which is the hometown of cast members Kevin Bacon, and Eve (entertainer)|Eve, as well as the birthplace of director Nicole Kassell, and producer Lee Daniels. Due in whole or in part to this, Bacon chose to speak with a thicker Philadelphia accent than he has, because he thought it was essential to the character." 

==Reception==
The film was well-received critically, with Bacons performance in particular drawing praise. Rotten Tomatoes reported that 88% of critics gave the film a positive review based on 130 reviews with an average rating of 7.3 out of 10 with the consensus "Kevin Bacons performance as a pedophile who is trying to start fresh has drawn raves from critics, who have praised the Woodsman as compelling, creepy, complex and well-crafted."  The film also has a score of 72 on Metacritic based on 34 reviews.  It was nominated for the "Grand Jury Prize" award at the 2004 Sundance Film Festival, won the "Jury Special Prize" at the Deauville Film Festival, and was a featured film at the 2005 Traverse City Film Festival.

The films release in the U.S. was limited, reaching a peak of 84 theaters. Despite being advertised in cinemas in the U.K. for several months, the film had a very limited release in the U.K. due to its controversial subject matter. Its gross in the U.S. was $1,576,231, while its worldwide gross totalled $4,678,405 

== See also ==
 

==References==
 

==External links==
*  
*  
*  
*  
*    about The Woodsman

 

 
 
 
 
 
 
 
 
 
 