Meatballs III: Summer Job
 
{{Infobox Film
| name           = Meatballs III: Summer Job
| image          = Meatballs iii.jpg
| caption        = Theatrical release poster
| director       = George Mendeluk John Dunning André Link
| writer         = George Mendeluk Bradley Kesden Michael Paseornek
| starring       = Sally Kellerman Patrick Dempsey Al Waxman Isabelle Mejias
| music          = Paul Zaza
| cinematography = Peter Benison
| editing        = Debra Karen The Movie Store
| released       = October 27, 1986
| runtime        = 96 min
| country        = Canada
| language       = English
| gross          = $2,147,228
}}
 rated R.  Unlike Meatballs Part II, this film follows Rudy (the main character from the first Meatballs), grown up (played by Patrick Dempsey) and working on a summer job for an angry boss named Mean Gene.  Unlike the first two movies, this one is not set at summer camp, but rather at a nearby marina.

==Synopsis==
When porn star Roxy Doujor is denied entrance into the afterlife, she is given one last chance to help some poor soul on Earth.  She finds Rudy Gerner, (whose character was the center of the original film as an adolescent) working at a summer river resort.  Roxy is given the task of helping Rudy lose his virginity in order to be allowed into the afterlife.

==Cast==
*Patrick Dempsey - Rudy
*Sally Kellerman - Roxy Doujor
*Shannon Tweed - The Love Goddess
*Isabelle Mejias - Wendy
*Maury Chaykin - Huey the River Rat Leader
*Caroline Rhea - (uncredited) Beach Girl #4 (her debut role)

==External links==
*   Meatballs Movie Website
* 

 

 
 
 
 
 
 
 
 
 
 


 