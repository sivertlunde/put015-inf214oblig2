Breaking Wind
 
 
{{Infobox film
| name           = Breaking Wind
| image          = Breaking Wind film poster.jpg
| image_size     = 215px
| alt            = 
| caption        = 
| director       = Craig Moss
| producer       = Bernie Gewissler Craig Moss
| screenplay     = Craig Moss
| starring       = Heather Ann Davis Eric Callero Frank Pacheco Emma Bell Danny Trejo
| music          = Todd Haberman
| cinematography = Rudy Harbon
| editing        = Austin Michael Scott	
| studio         = 
| distributor    = Lionsgate Home Entertainment
| released       =  
| runtime        = 82 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Breaking Wind is a 2012 vampire  .  It stars Heather Ann Davis, Eric Callero, Frank Pacheco and Danny Trejo.  Distributed by Lionsgate Home Entertainment, which became a sister company to Twilight studio Summit Entertainment in 2012, it wound up released only on DVD in the United States. 

==Plot==
One night, a young man named Ronald (Michael Hamilton) is attacked by an unseen assailant, and during the course of the attack he loses his shirt, then one of his teeth, and gains a tattoo. Finally, hes bitten on his buttocks, which causes him to begin transforming into a vampire and incessantly break wind.

A year later, Bella (Heather Ann Davis) asks her boyfriend Edward Colon (Eric Callero) to transform her into a vampire, but he insists that he marry her first. Bella also has to deal with the affections of Jacob (Frank Pacheco), a member of a pack of overweight, flatulent werewolves. One night, Edward gets a vision of Ronald breaking into Bellas room, and brings her to the Colon family home for help, but they are unsure of what to do. Jacob then takes her to meet his grandfather (Danny Trejo), who recounts the time that he faced an ancient evil that manifested itself as "different characters all played the exact same way" (a stab at Johnny Depp and his performances as Jack Sparrow, Edward Scissorhands, Willy Wonka and The Mad Hatter), and believes that a similar incident is about to occur. With this information and a recent spate of people going missing, the Colons are able to deduce that a rogue vampire is creating an army of newborns (or "noobs").

The noobs get Bellas scent from a pornographic magazine, and the Colons realize that they cannot protect her, especially because of Rosalies homicidal rages toward her and Carlisles (John Stevenson) inability to stop himself from drinking her blood. She seeks refuge with her flamboyantly homosexual father Charlie (Flip Schultz), but soon realizes that she cannot stay with him either, due to his insistence in graphically describing his sex acts and holding orgies at his house. Eventually, Edward works out a compromise with Jacob, who takes Bella to safety and uses his farts to mask her scent.

That night, Edward meets with Bella and Jacob and the three spend the night in a tent, where Jacob has sex with Bella as an oblivious Edward talks about how he thinks he and Jacob could be friends if they put their differences aside. The following morning the noobs catch up with the trio, but the Colons and Jacobs pack also arrive, and a fight ensues. Bella distracts the noobs by eating a special cake given to her by Jacob, which causes her to experience severe flatulence and creates a smell bad enough to distract the noobs long enough for the Colons to kill them all. Bella agrees to marry Edward, while Jacob accidentally ends up stabbing himself to death during a fight with a TMZ reporter.

The movie then ends by parodying the trailer to  , followed by a segment ridiculing the reactions to said trailer (in the form of YouTube videos) by certain "Twi-Hard" fans of the series.

==Cast== Bella
*Eric Edward
*Frank Jacob

==Reception== Time Out declared that "nothing prepares you for the ineptness and crassness of this ‘Twilight’ parody".  The A.V. Club reviewed the film and its audio commentary track on the "Commentary Tracks of the Damned" section.  

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 