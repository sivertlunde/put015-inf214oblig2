Between Strangers
 {{Infobox Film
| name           = Between Strangers
| image          = Between strangers poster.jpg
| caption        = Theatrical release poster
| director       = Edoardo Ponti
| producer       = Gabriella Martinelli Elda Ferri
| writer         = Edoardo Ponti
| starring       = Sophia Loren Mira Sorvino Deborah Kara Unger Pete Postlethwaite Julian Richings Klaus Maria Brandauer Malcolm McDowell Gérard Depardieu Len Doncheff Corey Sevier
| music          = Zbigniew Preisner   
| cinematography = Gregory Middleton   
| editing        = Roberto Silvi
| distributor    = Overseas FilmGroup 
| released       =  
| runtime        = 95 minutes
| country        = Canada Italy
| language       = English
| budget         =
| gross          =   
}}
Between Strangers is a 2002 film, written and directed by Edoardo Ponti, son of Sophia Loren, the first time they worked together.   

==Cast==

*Sophia Loren as Olivia
*Mira Sorvino as Natalia Bauer
*Deborah Kara Unger as Catherine
*Pete Postlethwaite as John
*Julian Richings as Nigel
*Klaus Maria Brandauer as Alexander Bauer
*Malcolm McDowell as Alan Baxter
*Gérard Depardieu as Max
*Len Doncheff as Grocery Store Owner
*Corey Sevier as Jeb

==Plot==

Three women in Toronto confront emotional crises regarding the men in their lives. Olivia (Sophia Loren) looks after her husband John (Pete Postlethwaite) who is confined to a wheelchair. Olivia has aspired to a career as an artist but John has refused to hear of her wasting her time. However, Olivia finds encouragement from an unlikely source, Max (Gérard Depardieu), an eccentric French gardener.

Natalia (Mira Sorvino) is a news photographer who, on assignment in Angola, took a portrait of a crying child orphaned by war. Her father Alexander (Klaus Maria Brandauer), also a well-known photojournalist, is proud of Natalia when her photo appears on the cover of a major news magazine but she is haunted by the fact that while she made the child famous, she couldnt save her life. 

Catherine (Deborah Kara Unger) has never been able to resolve her hatred of her father, Alan (Malcolm McDowell), who beat her mother to death when she was young. When Alan is released from prison, shes willing to abandon her husband, children and career as a musician to track him down and kill him, unable to accept that hes a changed man. 

==References==

 

==External links==
*  

 
 
 
 
 
 