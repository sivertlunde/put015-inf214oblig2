Abhijan
 
 
{{Infobox film
| name           = অভিযান Abhijan (The Expedition)
| image          = Dvd abhijan.jpg
| director       = Satyajit Ray
| producer       = Abhijatrik
| writer         = Satyajit Ray, adapted from the novel Abhijan by Tarashankar Bandopadhyay
| starring       = Soumitra Chatterjee Waheeda Rehman Ruma Guha Thakurta Gnyanesh Mukherjee  Charuprakash Ghosh Rabi Ghosh Arun Roy
| distributor    =
| cinematography = Soumendu Roy
| released       = 28 September 1962
| runtime        = 150 min.
| country        = India Bengali
| budget         =
}} Bengali film Indian filmmaker Satyajit Ray.

==Introduction==
The film gives the famous Ray flavour in its composition, flow and dialogues, and use of symbols. The protagonist Narasingh (played by Soumitra Chatterjee) is seen as a prototype for the character of the cynical cab driver Travis Bickle (played by Robert De Niro) in Martin Scorseses Taxi Driver (1976).  Scorsese himself has credited Satyajit Ray as a major influence on his work.  

==Plot==
 Marwari businessman with a record of smuggling and human trafficking. 
 
Sukharam (Charuprakash Ghosh) offers him a handsome fee to transport some goods which actually is opium. The realisation of the immoral trade puts Narshing in a compromising position, but he decides to join hands with Sukharam anyways. After all no one he saw was truly following the path of law and morality, not even his ideal and beloved Neeli (Ruma Guha Thakurta).

Neeli runs away with the crippled lover of hers and Narasinghs deep distrust of women deepens. As a result, even after knowing that Gulabi is a victim of Sukharams trafficking he forces her to sleep with him without any emotional involvement. At this point he is almost on the verge of becoming the one he used to hate, the lawless ones who also fail to face the world.

Gulabi (Waheeda Rehman), on the other hand is a melancholy, demonstrative and beautiful village widow. Gulabi is instinctively drawn to Narsingh. In spite of losing her dignity, she still looks at the bright side of life and has trust that Narshing is not immoral. She is attracted to him from the beginning and ready for a physical relationship, though not as a prostitute which Sukharam intended her to be at that time, but as a village girl.

   

After he decides to join the gang of smugglers consisting of a legal deal and selling his car, he finds that all his friends that had been with him for so long, including Rama, and Neelis brother, have abandoned him. He gets the money and social status he wanted but reduces himself to Mama Bhagne, a symbol of someone carrying the baggage of his own sin on his head, until he topples and is reduced to mere pebbles with no dignity. He changes, and rescues Gulabi just in time, before she was to be sold to the same lawyer who was a member of the smuggling racket he had thought of joining.

The tension of the good and evil collapses and the old car makes another journey into nothingness but with a halo of light ahead of it, the light of love.

==Cast==
* Soumitra Chatterjee as Narsingh
* Waheeda Rehman as Gulabi
* Ruma Guha Thakurta as Neeli
* Rabi Ghosh as Assistance of Narsingh
* Gnyanesh Mukherjee
* Charuprakash Ghosh as Sukharam
* Arun Roy

==Awards== National Film Awards
*     

==References==
 
*Satyajit Ray, The Inner Eye – W. Andrew Robinson, ISBN 1-86064-965-3

==External links==
*  at satyajitray.org
* 
* 
*  
*  

 
 

 
 
 
 
 
 