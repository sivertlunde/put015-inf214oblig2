Angel on the Amazon
 
{{Infobox film
| name = Angel on the Amazon
| image = Angel on the Amazon.jpg
| image_size =
| caption =
| director = John H. Auer
| producer = John H. Auer
| writer = Earl Felton Lawrence Kimble
| narrator =
| starring = George Brent Vera Ralston Brian Aherne Constance Bennett Nathan Scott
| cinematography = Reggie Lanning
| editing = Richard L. Van Enger
| studio = Republic Pictures
| distributor =  Republic Pictures
| released =  
| runtime = 86 minutes
| country = United States English
| budget =
| gross =
| website =
}}
Angel on the Amazon is a 1948 American adventure film directed by John H. Auer and starring George Brent, Vera Ralston and Brian Aherne.  It is also known by the alternative title Drums Along the Amazon.

==Plot summary== Amazonian jungle, its passengers are rescued by a mysterious young woman.

==Cast==
* George Brent as Jim Warburton  
* Vera Ralston as Christine Ridgeway  
* Brian Aherne as Anthony Ridgeway 
* Constance Bennett as Dr. Karen Lawrence 
* Fortunio Bonanova as Sebastian Ortega  
* Alfonso Bedoya as Paulo  
* Gus Schilling as Dean Hartley  Richard Crane as Johnny MacMahon   Walter Reed as Jerry Adams  
* Ross Elliott as Frank Lane 
* Konstantin Shayne as Dr. Jungmeyer  
* Alfredo DeSa as Brazilian Reporter  Elizabeth Dunne as Housekeeper  
* Franklyn Farnum as Diner   James Ford as Rio Nightclub Patron  
* Gerardo Sei Groves as Native  
* Dickie Jones as George  
* Charles La Torre as Waiter   Tony Martinez as Bellhop
* Alberto Morin as Radio Operator  
* Barry Norton as Brazilian Reporter  
* Manuel París as Night Desk Clerk  
* John Trebach as Waiter

==References==
 

==Bibliography==
* Kellow, Brian. The Bennetts: An Acting Family. University Press of Kentucky, 2004.

==External links==
* 

 

 
 
 
 
 
 
 
 

 