Into the White Night
{{Infobox film
| name           = Into the White Night
| image          = Into the White Night.jpg
| border         = yes
| alt            = 
| caption        = 
| director       = Yoshihiro Fukagawa
| producer       = Hiroyuki Ishigaki
| writer         = Yoshihiro Fukagawa Shingo Irie Akari Yamamoto
| based on       =  
| starring       = Maki Horikita Kengo Kora
| music          = Mamiko Hirai
| cinematography = Hirokazu Ishii
| editing        = Naoya Bando 
| studio         = 
| distributor    = GAGA CORPORATION
| released       =  
| runtime        = 149 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = $4,593,914 
}}
  is a 2010 Japanese film directed by Yoshihiro Fukagawa.  It was screened in the Main Programme of the Panorama section at the 61st Berlin International Film Festival.    It is based on a novel by Keigo Higashino.

==Plot==
A pawn shop owner in Osaka is murdered, but due to a lack of conclusive evidence the police lists the mans death as a suicide. Detective Sasagaki, who investigated the case, cant forget the main suspects daughter Yukiho (Maki Horikita) and the pawn shop owners son Ryoji.

As time goes by, more mysterious deaths surround Yukiho and Ryoji. Detective Sasagaki, still unable to let go of the pawn shop owner case, discovers startling details about Yukiho and Ryoji.

==Cast==
*Maki Horikita as Yukiho Karasawa
*Kengo Kora as Ryouji Kirihara
*Keiko Toda as Mikiko Kirihara
*Tetsushi Tanaka as Isamu Matsuura
*Ayame Koike as Mika Shinoduka
*Eichiro Funakoshi as Junjo Sasagak
*Nobuo Kyo as Kazunari Shinozuka
*Yurie Midori as Eriko Kawashima
*Urara Awata as Noriko Kurihara
*Yuki Imai as Ryouji Kirihara (10 years old)
*Shiori Fukumoto as Yukiho Karasawa (10 years old)

==See also==
*Byakuyako White Night- Korean version of film

==External links==
*    
*   Variety

==References==
 

 

 
 
 
 
 
 
 


 

 