The Air Mail
 
{{Infobox film
| name           = The Air Mail
| image          = 
| caption        = 
| director       = Irvin Willat 
| producer       = Adolph Zukor Jesse Lasky
| writer         = Byron Morgan (story) James Shelley Hamilton (scenario)
| starring       = Warner Baxter Billie Dove Douglas Fairbanks, Jr.
| music          = 
| cinematography = Alfred Gilks
| design         = 
| editing        =  Famous Players-Lasky Corporation
| distributor    = Paramount Pictures
| released       =  
| runtime        = 80 minutes
| country        = United States
| language       = Silent film English intertitles
| budget         = 
| gross          = 

}}
The Air Mail is a 1925 silent film directed by Irvin Willat and starring Warner Baxter, Billie Dove, and Douglas Fairbanks, Jr.. It was produced by Famous Players-Lasky and distributed through Paramount Pictures. Filmed in Death Valley National Park and the ghost town of Rhyolite, Nevada, it was released in the United States on March 16, 1925. Only four of eight reels survive in the Library of Congress.  

==Plot== George Irving) needs medicine, he flies to get it, but on the way back is chased by ruffians in other airplanes. As a result, Kanes friend, Sandy (Douglas Fairbanks Jr.), parachutes from Kanes plane with the medicine. Meanwhile, escaped prisoners have invaded Alices home. All is resolved when a sheriffs posse confronts the invaders, Kane destroys the bandit planes, and Sandy becomes a pilot.  

==Production==
 Reno via Tonopah, Nevada|Tonopah. The filming was completed by the end of January.    During the filming, Famous Players-Lasky restored the Bottle House, one of the deteriorating buildings in the ghost town.   

==Reception==

Reviewer Mordaunt Hall, writing for The New York Times in 1925, said that although Dove and Baxter "deliver creditable performances", the story is "only mildly interesting and often quite tedious". While he thought the scenes of planes taking off from the ground were "quite inspiring", he found improbable the stock villains, Deadwood Dick adventures, and romantic conversations between a man at   in the air and a woman on the ground. "This picture", he concluded, "is interesting because of the modern touch to an ordinary Western story, but the idea deserves to be more thoughtful and sincere."   

==References==
 

==External links==
* 
* 
*  


 
 
 
 
 
 
 
 
 
 