The Naked Witch
{{Infobox film
| name           = The Naked Witch
| image          =The Naked Witch (1964) - Title.jpg
| border         =
| alt            =
| caption        =Libby Hall as the witch of the film
| director       = Larry Buchanan Claude Alexander
| producer       = Larry Buchanan
| writer         = Larry Buchanan Claude Alexander
| screenplay     = 
| story          =
| based on       =
| starring       =Libby Hall Robert Short Jo Maryman
| music          =Ray Plagens
| cinematography =
| editing        =
| studio         =Alexander Enterprises  
| distributor    =
| released       = 1964
| runtime        =
| country        = United States
| language       = English
| budget         = $8,000   accessed 26 March 2013 
| gross          = $80,000 
}}
The Naked Witch is a 1961 American horror film written and directed by Larry Buchanan. It was financed by a Texan drive-in theater owner who wanted a movie with lots of Nudity in film|nudity, even though the actual resulting nudity in the film is negligible.  The film was shot in 1960 in Luckenbach, Texas, but was not released until 1964.  The copyright date in the films credits is 1961.  The film was successful and helped launch Buchanans career. 

==Plot== Robert Short), researching early German settlements in Central Texas (Luckenbach), uncovers the grave of a formerly executed witch (played by Libby Hall).  The vengeful witch rises from her grave and embarks on a campaign of seduction, revenge and murder against the descendants of those responsible for her death.

==Cast==
* Libby Hall, The Naked Witch Robert Short, The Student
* Jo Maryman, as Kirska
* Gary Owens, narrator (uncredited)

==References==
 

==External links==
 
*  

 

 
 
 


 