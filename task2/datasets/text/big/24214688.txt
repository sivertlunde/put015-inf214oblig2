Martha & Ethel
{{Infobox Film |
  name     = Martha & Ethel |
  starring       = Martha Kneifel, Ethel Edwards |
  director       = Jyll Johnstone |
  producer       = Jyll Johnstone, Barbara Ettinger |
  distributor    = Sony Pictures Classics |
  released   = March 8, 1995 |
  runtime        = 78 mins |
  language = English |
  budget         = N/A |
}} 1994 documentary film directed by Jyll Johnstone.  It premiered at the 1994 Sundance Film Festival 
{{cite web
|url=http://www.imdb.com/Sections/Awards/Sundance_Film_Festival/1994
|title=Sundance Film Festival 1994
|publisher=www.imdb.com
|accessdate=2009-08-25
|last=
|first=
}} Steve James for Hoop Dreams. 
{{cite web
|url=http://www.imdb.com/title/tt0110473/awards
|title=Martha & Ethel(1994)-Awards
|publisher=www.imdb.com
|accessdate=2009-08-25
|last=
|first=
}}
  The film was distributed in theaters by Sony Pictures Classics and on home video by Columbia TriStar Home Video.

==Synopsis==
Martha & Ethel tells the stories of two women in their 80s: a German-Catholic woman named Martha and an African-American woman named Ethel, the former nannies of director/producer Jyll Johnstone and co-producer Barbara Ettinger. It examines each woman’s background and hiring into affluent New York families. The Johnstone and Ettinger children, now grown, reflect on how Martha and Ethel played formative—and often confusing—roles in their lives.

==Reception==
Upon release, the film received mostly positive reviews. It currently (as of August 2009) maintains a 100% "freshness" rating on review aggregation website rottentomatoes.com, based on five reviews. 
{{cite web
|url=http://www.rottentomatoes.com/m/martha_and_ethel/
|title=Martha & Ethel Movie Reviews,Pictures - Rotten Tomatoes
|publisher=www.rottentomatoes.com
|accessdate=2009-08-25
|last=
|first=
}}
  Roger Ebert gave the film three stars and called it "as fascinating for what it doesnt say as for what it does." 
{{cite news
| last = Ebert 
| first = Roger
| title = Martha And Ethel
| work = Chicago Sun-Times
| date = 1995-02-24
| url = http://rogerebert.suntimes.com/apps/pbcs.dll/article?AID=/19950224/REVIEWS/502240302/1023
| accessdate = 2009-08-25 }} 

==External links==
*  
*  
*  
*  
*  
*  

==References==
 

 
 
 