Run for Your Wife (2012 film)
 
 
  Run for produced and directed the film. The executive producer is Vicki Michelle.  Upon release the film was savaged by critics and has been referred to as one of the worst films of 2013 after it grossed only £602 in its opening weekend at the British box office to its £900,000 budget.  

==Plot==
The story of London cab driver John Smith, with two wives, two lives and a very precise schedule for juggling them both. With one wife at home in Stockwell and another at home in Finsbury.

Trouble brews when Smith intervenes in a mugging. After being hit on the head, he ends up in hospital.  This upsets his schedule and causes both wives to report him missing. Smith becomes hopelessly entangled in his attempts with his lazy layabout neighbour downstairs in Stockwell, to explain himself to his wives and two suspicious police officers. 

==Production==
Over 80 celebrities agreed to make cameo appearances having all agreed to donate their fees to a theatrical charity. 

==Reception== The Independents Metro commenting Daily Record The Berkhamsted Daily News stated that "Run for Your Wife could be the worst film in history",  the Studio Briefing website reported that "Some writers are making the case that the British comedy Run for Your Wife, written by and starring comedian Ray Cooney, may be “the worst film ever"”,  and The Daily Mirror reported (a few months after its release) that Run For Your Wife "was branded the worst British film ever." 

Run for Your Wife currently has an "0%" rating on Rotten Tomatoes. 

==Cast==
*Danny Dyer	- John Smith
*Denise van Outen - Michelle Smith
*Sarah Harding - Stephanie Smith
*Neil Morrissey - Gary Gardner Ben Cartwright - D. S. Troughton
*Nicholas Le Prevost - D. S. Porterhouse
*Kellie Shirley - Susie Browning
*Christopher Biggins - Bobby Franklin
*Lionel Blair - Cyril
*Jeffrey Holland - Dick Holland
*Derek Griffiths - Stockwell Police Officer
*Louise Michelle - Frances
*Nick Wilton - Taxi Driver
*Christopher Sweeney - Police Officer

===Cameo roles (alphabetical)===
*Russ Abbot - Hospital patient
*Robin Askwith - Bus driver
*Lynda Baron - Nurse
*Richard Briers - Newspaper seller
*Tony Britton - Man on bus
*Jess Conrad - Piano player
*Tom Conti - Actor
*Wendy Craig - Nanny
*Bernard Cribbins - Hospital patient
*Barry Cryer - Busker
*Ian Cullen - Wrinkled Man
*Pamela Cundell - War Widow
*Geoffrey Davies - Man in theatre queue
*Judi Dench - Bag lady
*Derek Fowlds - Man in hat
*William Gaunt - Man on bus
*Liza Goddard - Exercising woman
*Rolf Harris - Busker
*Nicky Henson - Hospital patient
*Louise Jameson - Doctors Receptionist
*Maureen Lipman - Exercising woman
*Vicki Michelle - Tourist Brian Murphy - Allotment man
*Derren Nesbitt - Man on bus Geoffrey Palmer - Man on toilet
*Bill Pertwee - Man on bus
*Su Pollard - Shopkeeper
*Cliff Richard - Busker
*Andrew Sachs - Clumsy waiter
*Prunella Scales - Woman at pub
*Jenny Seagrove - Taxi passenger
*Donald Sinden - Man on bus
*Sylvia Syms - Hip operation woman
*Frank Thornton - Man getting off bus
*Wanda Ventham - Lady on Bus
*Marcia Warren - Woman on seat
*Dennis Waterman - Minding Person
*Giles Watling - Man in pub
*Moray Watson - Man on bus
*Timothy West - Man in pub
*June Whitfield - Exercising woman Simon Williams - Café customer
*Mark Wingett - Man outside café

==See also==
*List of films considered the worst

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 