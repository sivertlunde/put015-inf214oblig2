Aida (1953 film)
{{Infobox film
| name = Aida
| image = Aida Loren1.jpg
| caption = Original film poster
| director = Clemente Fracassi
| producer = Gregor Rabinovitch Federico Teti
| writer = Clemente Fracassi  Antonio Ghislanzoni (libretto)
| starring = actor = (a)   singer = (s)  Sophia Loren (a) Renata Tebaldi (s)   Lois Maxwell (a)  Ebe Stignani (s)   Luciano Della Marra (a)   Giuseppe Campora (s)   Afro Poli (a)   Gino Bechi (s)
| music = Giuseppe Verdi
| cinematography = Piero Portalupi
| editing = Mario Bonotti
| distributor = CEI Incom  I.F.E. Releasing Corporation
| released = 23 October 1953 (Italy) 8 October 1954 (United States)
| runtime = 95 minutes
| country = Italy
| awards =
| language = Italian
| budget =
| preceded_by =
| followed_by =
}} 1953 cinema Italian film version of the opera Aida by Giuseppe Verdi. It was directed by Clemente Fracassi and produced by Gregor Rabinovitch and Federico Teti. The screenplay was adapted by Fracassi, Carlo Castelli, Anna Gobbi and Giorgio Salviucci from the libretto by Antonio Ghislanzoni. The cinematography was by Piero Portalupi, the production design by Flavio Mogherini and the costume design by Maria De Matteis. The Italian State Radio Orchestra was conducted by Giuseppe Morelli, the ballet was choreographed by Margherita Wallmann.

The film was screened out of competition at the 1987 Cannes Film Festival.   

==Plot==

This is the desperate love story of an Ethiopian slave Aida (Sophia Loren) staying in the then capital of Egypt Memphis with the young Egyptian supreme commander Radames (Luciano). Aida’s mistress, Amneris (Lois Maxwell), the daughter of the pharaoh is too in love with Radames. Aida doesn’t know how to hide her love, and Amneris is jealous of her because of her beauty and simplicity.
	
On the war with the Ethiopians, Egypt will concur and the pharaoh will declare the marriage of his daughter with Radames. Radames will come to meet Aida, perhaps for the last time! Aida doesn’t know what to do, or whether she will get her love back?

==Cast==


*Sophia Loren 		: Aida
*Lois Maxwell		:  Amneris
*Luciano Della Marra	: Radamès
*Afro Poli		: Amonasro
*Antonio Cassinelli	: Ramfis

 
The singing voices were provided by Renata Tebaldi (Aida), Ebe Stignani (Amneris), Giuseppe Campora (Radamès), Gino Bechi (Amonasro) and Giulio Neri (Ramfis). Yvette Chauviré and Léonide Massine are the dancers in the ballet with the Rome Opera Ballet.

==Production notes==
Renata Tebaldi was originally cast to play Aida, but she decided not to appear in the film. Gina Lollobrigida was also considered for the role, before Sophia Loren was cast. This was Lorens first leading role. Sophia Lorens performance was met with critical acclaim for her role as Aida.

==References==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 

 