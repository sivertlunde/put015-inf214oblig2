Rain in the Mountains
{{multiple issues|
 
 
}}

{{Infobox film
| name           = Rain in the Mountains
| director       = Joel Metlen Christine Sullivan
| producer       = Joel Metlen Christine Sullivan Lillian Parker
| screenplay     = Joel Metlen
| starring       = Steve Pierre Nick Erb Audrey Seymour Robert Satiacum Joseph Heldman
| music          = Yazan Fahmawi
| cinematography = Joel Metlen
| editing        = Joel Metlen Christine Sullivan
| studio         = Foxhall Films GO Film
| released       = 2007
| runtime        = 90 minutes
| country        = United States
| language       = English
}}
 2007 Indigenous Native American film directed by Joel Metlen and Christine Sullivan.

== Plot ==

The film revolves around a Native American man, Eric Smallhouse (Steve Pierre), who, after a chance encounter with an elderly man known as The Deadman (Joseph Heldman), attempts to recapture the old ways of his people.

Throughout the film, his efforts, halfheartedly aided by his young son, Todd (Nick Erb), yield negative and often comedic results, much to the consternation of his wife, Lindsay (Audrey Seymour).  Furthermore, his new-found antagonism toward modern conveniences, especially electricity, culminate in a chase involving the town Deputy, John (Robert Satiacum), and the FBI. 

== Cast ==
Main cast
* Eric Smallhouse - Steve Pierre
* Todd Smallhouse - Nick Erb
* Lindsay Smallhouse - Audrey Seymour
* Deputy John - Robert Satiacum
* The Deadman - Joseph Heldman

== External links ==
*   at the Internet Movie Database

 
 
 
 
 


 