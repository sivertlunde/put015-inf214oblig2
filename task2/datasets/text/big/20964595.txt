Mine Own Executioner
 
{{Infobox film
| name           = Mine Own Executioner
| image	         = Mine Own Executioner FilmPoster.jpeg
| caption        = Film poster
| director       = Anthony Kimmins
| producer       = Anthony Kimmins Jack Kitchin Alexander Korda (exec producer)
| writer         = Nigel Balchin
| based on = novel by Nigel Balchin
| starring       = Burgess Meredith
| music          = 
| cinematography = Wilkie Cooper Richard Best
| studio = London Films
| distributor    = British Lion Films
| released       =  
| runtime        = 108 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross = £158,734 (UK) 
}} thriller drama film starring Burgess Meredith and directed by Anthony Kimmins, and based on the novel of the same name by Nigel Balchin. It was entered into the 1947 Cannes Film Festival.   

The title is derived from a quotation of John Donnes "Devotions upon Emergent Occasions|Devotions", which serves as the motto for the original book.

==Plot==
Felix Milne (Meredith) is an overworked psychologist with psychological problems of his own.  Molly Lucian has a husband traumatized from having been in a Japanese POW camp, and she needs Milnes help in treating her husband, Adam.  Adam is about to become severely schizophrenic. To make matters worse, Felix finds his own home life deteriorating.

==Cast==
* Burgess Meredith as Felix Milne
* Dulcie Gray as Patricia Milne
* Michael Shepley as Peter Edge
* Christine Norden as Barbara Edge
* Kieron Moore as Adam Lucian
* Barbara White as Molly Lucian
* Walter Fitzgerald as Dr. Norris Pile
* Edgar Norfolk as Sir George Freethorne
* John Laurie as Dr. James Garsten Martin Miller as Dr. Hans Tautz
* Clive Morton as Robert Paston
* Joss Ambler as Julian Briant
* Jack Raine as Inspector Pierce
* Laurence Hanray as Dr. Lefage
* Helen Haye as Lady Maresfield

==Production== An Ideal Husband (1947). 

Australian Frederic Hilton worked as technical adviser. 

==Reception== Sons of Liberty, an anti-British group active in the time.  The picketing was part of the groups call to boycott British films and products, and had little to do with "Mine Own Executioner" in itself.

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 