Gary Cooper, Who Art in Heaven
{{Infobox film
| name           = Gary Cooper, Who Art in Heaven
| image          = 
| caption        = 
| director       = Pilar Miró
| producer       = 
| writer         = Antonio Larreta Pilar Miró
| starring       = Mercedes Sampietro
| music          = 
| cinematography = Carlos Suárez
| editing        = 
| distributor    = 
| released       =  
| runtime        = 98 minutes
| country        = Spain
| language       = Spanish
| budget         = 
}}

Gary Cooper, Who Art in Heaven ( ) is a 1980 Spanish drama film directed by Pilar Miró. It was entered into the 12th Moscow International Film Festival where Mercedes Sampietro won the award for Best Actress.   

==Cast==
* Mercedes Sampietro as Andrea Soriano
* Jon Finch
* Carmen Maura as Begoña
* Víctor Valverde (as Victor Valverde)
* Alicia Hermida as María
* Isabel Mestres
* José Manuel Cervino (as Jose Manuel Cervino)
* Mary Carrillo as Madre
* Agustín González as Álvaro (as Agustin Gonzalez)
* Fernando Delgado as Bernardo Ortega
* Amparo Soler Leal as Carmen

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 