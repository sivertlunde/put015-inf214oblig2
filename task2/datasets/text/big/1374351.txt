Levity (film)
{{Infobox film
| name           = Levity
| image          = Levity poster.JPG
| alt            = 
| caption        = Promotional poster for Levity
| writer         = Ed Solomon
| editing        = Pietro Scalia
| starring       = Billy Bob Thornton Morgan Freeman Holly Hunter Kirsten Dunst
| director       = Ed Solomon
| cinematography = 
| producer       = Richard N. Gladstein Adam J. Merims Ed Solomon
| studio         = StudioCanal Revelations Entertainment
| distributor    = Sony Pictures Classics
| released       = January 16, 2003 (Sundance Film Festival) (premiere)
| runtime        = 100 minutes
| country        = United States France
| language       = English
| music          =
| awards         =
| budget         = $7,500,000 (estimated)
| gross          = $209,695
}}
 The score for this film was composed by Mark Oliver Everett of the band Eels (band)|Eels.  Levity was filmed in Montreal, Canada.

== Plot ==

Manual Jordan, a man who served nearly 23 years  for killing a teenager during an attempted robbery, is released after serving a life sentence. After spending his time staring at a clipping of Abner Easley, the boy he killed, he returns to the city he used to live in to find redemption. He ends up living at a community house which is run by Miles Evans, a preacher. He offers Manual work so he can pay for the room, and Manual places Abners photo in his room to remind himself.

While staying at the community house, he befriends Sofia Mellinger, a wild young woman with no adult figure in her life. Manual also encounters Adele Easley, the elder sister of Abner. She does not recognize Manual and in his pursuit for forgiveness, he forms a friendship with her, and their relationship begins to develop.

Manual gets his chance for redemption when Adeles rebellious teenage son becomes involved in violence. Manual tries to befriend him and steer him in the right direction, ever mindful of the past.

== Cast ==

* Billy Bob Thornton as Manual Jordan
* Morgan Freeman as Miles Evans
* Holly Hunter as Adele Easley
* Kirsten Dunst as Sofia Mellinger
* Manuel Aranguiz as Senor Aguilar
* Geoffrey Wigdor as Abner Easley
* Luke Robertson as Young Abner Easley
* Dorian Harewood as Mackie Whittaker
* Catherine Colvey as Claire Mellinger
* Billoah Greene as Don
* Sadiki Burke as Sadiki
* Abede Burke as Abede
* Diego Abella as Raul
* Brent Rogers as Ty
* Cordell Clyde as Cleve
* David Acer as Comedian

== Reception ==
Levity received mixed to negative reviews from critics, as it holds a 35% rating on Rotten Tomatoes based on 89 reviews.

== See also ==
* 2003 in film

== External links ==
*  
*  
*  
*  
*   

 
 
 
 
 
 
 
 
 