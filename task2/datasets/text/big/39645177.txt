The Land of Smiles (1952 film)
{{Infobox film
| name           = The Land of Smiles 
| image          = 
| image_size     = 
| caption        = 
| director       = Hans Deppe   Erik Ode 
| producer       = Kurt Ulrich
| writer         = Ludwig Herzer (libretto)   Fritz Löhner-Beda (libretto)   Viktor Léon   Axel Eggebrecht   Hubert Marischka
| narrator       =  Walter Müller   Paul Hörbiger
| music          = Franz Lehár (operetta)   Paul Dessau
| editing        = Margarete Steinborn Kurt Schulz 
| studio         = Berolina Film
| distributor    = Herzog-Filmverleih 
| released       = 2 October 1952
| runtime        = 107 minutes
| country        = West Germany
| language       = German
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} West German Walter Müller. a 1930 film starring Richard Tauber. 

==Cast==
* Mártha Eggerth as Lissy Licht, Sängerin 
* Jan Kiepura as Sou Bawana Pantschur, Prinz von Javora  Walter Müller as Gustl Potter, ein junger Wiener 
* Paul Hörbiger as Professor Ferdinand Licht 
* Karin Von Dassel as Mi, Schwester von Prinz Sou 
* Karl Meixner as Exzellenz Tschang 
* Ludwig Schmitz as Kato 
* Brigitte Lwowsky as Chrysanthème

==References==
 

==Bibliography==
* Traubner, Richard. Operetta: A Theatrical History. Routledge, 2003.

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 


 
 