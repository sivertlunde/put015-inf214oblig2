Naughty Boy (film)
 
 Comedy and Romance Bollywood film, starring Kishore Kumar and Kalpana Mohan|Kalpana. It was produced and directed by Shakti Samanta. Music was composed by Sachin Dev Burman|S.D.Burman and he was assisted by his own son Rahul Dev Burman|R.D. Burman. Dialogues were written by Vrajendra Gaur. http://www.imdb.com/title/tt0268489/fullcredits?ref_=tt_cl_sm#cast 

==Plot==
Pritam works as a Book-keeper in an Export Import Firm and does not have a roof to live under. When he goes to search for accommodation, he runs into a friend, Jagdish, who takes him to a rooming house and lets him share one room with himself, Kavi Viyogi, and Bhimsen. The landlady has very strict rules for all her tenants to wit: No one is allowed to romance on her property. On a rainy day when Pritam goes to buy milk, he runs into a beautiful girl, Meena Sharma, and their umbrellas get entangled, and when freed get interchanged. He goes to look for her house and finds she lives with her maternal uncle, a music maestro of sorts, and Pritam enrolls himself in his class. Pritam and Meena continue meeting and fall in love with each other. When Meenas sister is about to get married, she travels to Poona and thats when Pritam finds out that her train had an accident and she has been listed as one of the dead. Heart-broken and devastated he is severely depressed, until his co-workers decide that he should go for a picnic and this does cheer him considerably. In this cheerful state he returns home and decides to carry on with life without Meena. It is then Meena returns back, quite very much alive, and finds that Pritam is not in a state of mourning but is enjoying life to the fullest. She decides to teach him a lesson that he will never forget. Watch what happens when the lesson commences and what impact this has on our care-free friend - who is currently wooing a dancer by the name of Edna Wong. 

==Cast==
*Bhattacharya		
*Laxmi Chhaya...Bela
*Kalpana Mohan|Kalpana...Meena Sharma / Edna Wong
*Kamaldeep 		
*Tina Katkar 		
*Nand Kishore		
*Krishnakant...Kavi Viyogi
*Kishore Kumar...Pritam
*Kundan...Bhimsen
*Masood  		
*Pachhi 		
*Praveen Paul...Landlady
*Om Prakash...Vaidraj Churandas Jari-Bhuti Chaturvedi
*Madan Puri...R.L. Mathur Matur
*Rafia		
*Laxman Rao 		
*Kanu Roy 	 		
*Shivraj...Mr. Sharma
*Sunder (actor)|Sunder...Jagdish
*Edwina...Dancer (uncredited)
*Marie...Dancer (uncredited)
*Shinde...Dancer (uncredited)
 

==Soundtrack== Shailendra

*Ab Toh Batla Are Zalim

Singer(s): Asha Bhosle and Kishore Kumar

*Dil Dhadka Usse Dekhte Hi

Singer(s): Kishore Kumar

*Ho Gayi Shyam Dil Badnam

Singer(s): Manna Dey and Asha Bhosle

*Jahan Bhi Gaye Hum O Mere Humdam

Singer(s): Asha Bhosle and Kishore Kumar

*Nazren Milake Jo Duniya Ki

Singer(s): Kishore Kumar

*Rang Yeh Duniya Badalti Hai

Singer(s): Kishore Kumar

*Sa Sa Sa Sa Re

Singer(s): Kishore Kumar and Asha Bhosle

*Tum Mere Pehchane Phir Bhi

Singer(s): Asha Bhosle
 

==References==
 

==External Links==
 

 
 
 