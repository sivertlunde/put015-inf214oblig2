The Housemaid (2010 film)
 
{{Infobox film
| name           = The Housemaid
| image          = The housemaid 2010 poster.jpg
| caption        = Theatrical release poster
| film name      = {{Film name
| hangul         =  
| hanja          =  
| rr             = Hanyeo
| mr             = Hanyŏ }}
| director       = Im Sang-soo
| producer       = Jason Chae
| writer         = Im Sang-soo
| based on       =  
| starring       = Jeon Do-yeon Lee Jung-jae Seo Woo Youn Yuh-jung
| music          = Kim Hong-jib
| cinematography = Lee Hyung-deok
| editing        = Lee Eun-soo
| studio         = Mirovision Sidus FNH
| released       =  
| runtime        = 107 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          =   
}} thriller film The Housemaid. It competed for the Palme dOr at the 2010 Cannes Film Festival.  

==Plot==
Eun-yi is hired as an au pair for Hae-ra (pregnant with twins) and her rich husband Hoon. Eun-yis primary task is watching the couples young daughter, Nami.  Eun-yi is eager to connect to Nami, who gradually warms to her. Hoon begins to secretly flirt with Eun-yi, enticing her with glasses of wine and his piano playing, and they eventually begin a sexual relationship. Despite the affair, Eun-yi is still warm and friendly to Hoons oblivious wife, Hae-ra; she even expresses enthusiasm and delight at the progress of Hae-ras pregnancy.  

Byeong-sik, aka "Miss Cho" (the other live-in maid, originally Hae-ras childhood maid) witnesses Eun-yi and Hoon having sex. She tries to subtly pry details from Eun-yi, but Eun-yi brushes her off casually. Later, Miss Cho reveals her suspicion to Hae-ras mother that Eun-yi is pregnant. Hae-ras mother then visits the family and stages an "accident," resulting in Eun-yi falling from a ladder positioned at the top of a set of stairs. Dangling from a chandelier, Eun-yi begs Hae-ras mother to pull her over the railing. Hae-ras mother does not oblige, and Eun-yi falls. Suffering only a concussion, Eun-yi spends the night in the hospital.  During her stay, she learns that she is pregnant and contemplates abortion. Meanwhile, the affair is revealed to Hae-ra.  

Hae-ras mother, Mi-hee, instructs Hae-ra to ignore the affair; she insists that all wealthy husbands will eventually cheat and that if Hae-ra ignores it she can "live like a queen." Later that night, Hae-ra stands over Eun-yis bed with a golf club but is unable to strike the sleeping woman. The next day, Hae-ra and her mother confront Eun-yi, offering her $100,000 to have an abortion and leave. Hae-ra knows that Eun-yi would not abort her child "for all the money in the world," so she takes matters into her own hands by poisoning the herbal medicine packets Eun-yi drinks every day.  Hae-ra goes to the hospital and delivers her twin sons. Hoon visits the hospital, where Hae-ra makes her ill will toward him known.  Furious, he returns home alone and finds Eun-yi in his bathtub. She reveals that she is pregnant and plans on keeping the baby.

Eun-yi succumbs to the effects of the poison, and Mi-hee arranges an abortion without Eun-yis consent. After the abortion, Miss Cho reveals that she told Mi-hee about Eun-yis pregnancy. Eun-yi is angry, but forgives Miss Cho and vows to get revenge on the family. After recovering from her abortion, Eun-yi sneaks into the house with Miss Chos assistance. Hoon finds her breastfeeding one of the newborn babies. Hae-ra insists that Miss Cho chase Eun-yi out of the house, but Miss Cho refuses and quits her job on the spot. Eun-yi then confronts the entire family (Hae-ra, Mi-hee, Hoon, and Nami), hanging herself from the same chandelier she once clung to, then lighting her body on fire as the family watches in horror.

The final scene depicts the family outdoors in the snow celebrating Namis birthday, all speaking English. While Hae-ra sings "Happy Birthday", Hoon hands a glass of champagne to Nami. Both appear insane as Nami looks on.

==Cast==
* Jeon Do-yeon as Eun-yi
* Lee Jung-jae as Hoon
* Seo Woo as Hae-ra
* Youn Yuh-jung as Byeong-sik
* Ahn Seo-hyeon as Nami
* Park Ji-young as Mi-hee

==Production== Kim Soo-hyun, The Housemaid from 1960, Im has said that he tried to never think of it during the production in order to come up with a modern and original work. One major difference between the versions is that the original film took place in the middle class, while the remake is set in an extreme upper-class environment. Im explains this with South Koreas social structure around 1960, which was a time when the countrys middle class started to form and many poor people moved from the countryside to work in the cities: "women became housemaids who served not only for the rich but also the middle class and that issue had served as the basis to Kim Ki-youngs work. What I realized upon reworking The Housemaid in 50 years was that there are much more wealthy people now, people who are millionaires. ... I wanted to depict the reality in which housewives from normal families have to undertake hard work too".  The film was produced by the Seoul-based company Mirovision. 

In the birds-eye shots used in the film from time to time, a large ornate chandelier is an observer that looks down on the bourgeois family for which Eun-yi works. It also plays a role in the dramatic and tragic end of the movie’s heroine. That chandelier in all its detail is actually a copy of the 2008 work "Song of Dionysus" created by artist Bae Young-whan. The decision to include the chandelier in The Housemaid was quite a deliberate one. At first glance, the light fixture looks like an elegant Art Nouveau craft, but a closer look reveals that its green glass pieces are actually sharp shards from broken wine and soju bottles. In the same sense, the high-class family members in the movie look elegant at a glance but are actually selfish and cruel enough to break their housemaids heart. 

==Release== How to Train Your Dragon.  Box Office Mojo reported a total revenue of $14,075,390 in the films domestic market.  The film had 2,289,709 admissions nationwide being the 10th most attended domestic release of the year.  The international premiere took place on 14 May in competition at the 2010 Cannes Film Festival.  American distribution rights were acquired by IFC Films, who released it in 2011. 

==Reception==
Following the screening in Cannes, Maggie Lee of  tic" and "even with Jeons calibrated performance, Eun&#8209;yis characterization is problematic... he absence in motivation of her behavior does not really convince".  Lee Hyo-won of The Korea Times was all praises, saying that Im "brings a sexy, seamlessly quilted film that throbs with intrigue, lively characters and finely crafted melodrama". 

==Awards and nominations==
;2010 Cannes Film Festival
* Nomination - Palme dOr 

;2010 Chunsa Film Art Awards 
* Best Supporting Actress - Youn Yuh-jung

;2010 Buil Film Awards
* Best Supporting Actress - Youn Yuh-jung

;2010 Grand Bell Awards
* Best Supporting Actress - Youn Yuh-jung
* Nomination - Best Film
 2010 Blue Dragon Film Awards
* Best Supporting Actress - Youn Yuh-jung
* Best Art Direction - Lee Ha-jun
* Nomination - Best Film
* Nomination - Best Director - Im Sang-soo
* Nomination - Best Actress - Jeon Do-yeon

;2010 Cinemanila International Film Festival 
* Best Supporting Actress - Youn Yuh-jung
* Best Director - Im Sang-soo

;2010 Korean Film Awards
* Best Supporting Actress - Youn Yuh-jung
* Nomination - Best Actress - Jeon Do-yeon
* Nomination - Best Supporting Actress - Seo Woo
* Nomination - Best Art Direction - Lee Ha-jun
* Nomination - Best Sound Effects - Lee Sang-joon, Kim Suk-won, Park Joo-kang
* Nomination - Best Music - Kim Hong-jib

;2010 Busan Film Critics Awards
* Best Supporting Actress - Youn Yuh-jung

;2011 Max Movie Awards 
* Best Supporting Actress - Youn Yuh-jung
 2011 Asian Film Awards Best Supporting Actress - Youn Yuh-jung
* Peoples Choice for Favorite Actress - Jeon Do-yeon Best Actress - Jeon Do-yeon
* Nomination - Best Costume Design - Choi Se-yeon

;2011 Fantasporto Directors Week
* Manoel de Oliveira Award for Best Film 
* Best Actor - Lee Jung-jae
* Best Actress - Jeon Do-yeon

==See also==
* Cinema of South Korea
* List of South Korean films of 2010

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 