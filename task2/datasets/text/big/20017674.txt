The Three Stooges Collection
{{Infobox film
| name           = The Three Stooges Collection
| image          = Stooges3436.jpg
| caption        = The Three Stooges Collection, Volume One: 1934–1936 DVD cover. Interestingly, the cover photo is from their Monogram Studios feature film (and not included in these Columbia collections), 1946s Swing Parade of 1946
| director       = 
| producer       = Jules White Hugh McCollum Del Lord Charley Chase
| starring       = Moe Howard Larry Fine Curly Howard Shemp Howard Joe Besser
| music          = R. H. Bassett
| distributor    = Sony Pictures Home Entertainment
| released       = United States|U.S. Volume One October 30, 2007 Volume Two May 27, 2008 Volume Three August 26, 2008 Volume Four October 7, 2008 Volume Five March 17, 2009 Volume Six June 16, 2009 Volume Seven November 10, 2009 Volume Eight June 1, 2010 The Ultimate Collection June 5, 2012
| runtime        = Volume One 340 mins. Volume Two 415 mins. Volume Three 396 mins. Volume Four 360 mins. Volume Five 432 mins. Volume Six 390 mins. Volume Seven 356 mins. Volume Eight 515 mins. Total running time 3204 mins.
| country        = United States English |
}}

The Three Stooges Collection is a series of  , another first for the comedy teams body of   slowed down the release schedule. Volume Five: 1946-1948 was belatedly released on March 17, 2009 and was the first volume to feature original Stooge member, Shemp Howard and the final volume to feature Curly Howard. Volume Six: 1949-1451 was released three months later on June 16, 2009. Volume Seven: 1952–1954 was released on November 10, 2009, and included a pair of 3-D film|3-D glasses to view the two shorts, Spooks! and Pardon My Backfire. As of 2013, the 3-D versions of these shorts have been removed for the current versions of this volume. The final volume, Volume Eight: 1955-1959, was released on June 1, 2010, which is a three-disc set and covers the last five years with Columbia Pictures. This is the final volume to feature Shemp Howard and the first and only volume to feature Joe Besser.

In 2012, all eight volumes were reissued in a box set entitled The Three Stooges: The Ultimate Collection, with the addition of a ninth, 3-disc volume entitled Rare Treasures from the Columbia Picture Vault. The additional volume featuring the feature films Rockin in the Rockies and Have Rocket, Will Travel, several cartoons featuring the Stooges from the 1930s and 1940s, and a number of Columbia shorts featuring Shemp Howard, Joe Besser and Joe DeRita as solo comedians prior to joining the Stooges; several of these solo films are remakes of Stooge films.

==Volume One: 1934–1936== 
The films original release date is listed next to the title.

===Disc One===

====1934 in film|1934====
* 001 Woman Haters (May 5)
* 002 Punch Drunks (July 13) Men in Black (September 28)
* 004 Three Little Pigskins (December 8)

====1935 in film|1935====
* 005 Horses Collars (January 10)
* 006 Restless Knights (February 20)
* 007 Pop Goes the Easel (March 29)
* 008 Uncivil Warriors (April 26)
* 009 Pardon My Scotch (August 1) Hoi Polloi (August 29)
* 011 Three Little Beers (November 28)

===Disc Two===

====1936 in film|1936====
* 012 Ants in the Pantry (February 6)
* 013 Movie Maniacs (February 20)
* 014 Half Shot Shooters (April 30)
* 015 Disorder in the Court (May 30)
* 016 A Pain in the Pullman (June 27) False Alarms (August 16)
* 018 Whoops, Im an Indian! (September 11)
* 019 Slippery Silks (December 27)

==Volume Two: 1937–1939==
===Disc One===

====1937 in film|1937====
* 020 Grips, Grunts and Groans (January 13)
* 021 Dizzy Doctors (March 19)
* 022 3 Dumb Clucks (April 17) Back to the Woods (May 14)
* 024 Goofs and Saddles (July 2) Cash and Carry (September 3)
* 026 Playing the Ponies (October 15)
* 027 The Sitter Downers (November 26)

====1938 in film|1938====
* 028 Termites of 1938 (January 7)
* 029 Wee Wee Monsieur (February 18)
* 030 Tassels in the Air (April 1)
* 031 Healthy, Wealthy and Dumb (May 20)
* 032 Violent Is the Word for Curly (July 2)

===Disc Two===
* 033 Three Missing Links (July 29)
* 034 Mutts to You (October 11)
* 035 Flat Foot Stooges (December 5)

====1939 in film|1939====
* 036 Three Little Sew and Sews (January 6)
* 037 We Want Our Mummy (February 24)
* 038 A Ducking They Did Go (April 7)
* 039 Yes, We Have No Bonanza (May 19)
* 040 Saved by the Belle (June 30)
* 041 Calling All Curs (August 25)
* 042 Oily to Bed, Oily to Rise (October 6)
* 043 Three Sappy People (December 1)

==Volume Three: 1940–1942==
===Disc One===

====1940 in film|1940====
* 044 You Nazty Spy! (January 19)
* 045 Rockin thru the Rockies (March 8)
* 046 A Plumbing We Will Go (April 19)
* 047 Nutty But Nice (June 1)
* 048 How High Is Up? (July 26) From Nurse to Worse (August 23)
* 050 No Census, No Feeling (October 4)
* 051 Cookoo Cavaliers (November 15)
* 052 Boobs in Arms (December 27)

====1941 in film|1941====
* 053 So Long Mr. Chumps (February 7)
* 054 Dutiful But Dumb (March 21)
* 055 All the Worlds a Stooge (May 16)
* 056 Ill Never Heil Again (July 4)

===Disc Two===
* 057 An Ache in Every Stake (August 22)
* 058 In the Sweet Pie and Pie (October 16)
* 059 Some More of Samoa (December 4)

====1942 in film|1942====
* 060 Loco Boy Makes Good (January 8) Cactus Makes Perfect (February 26)
* 062 Whats the Matador? (April 23)
* 063 Matri-Phony (July 2)
* 064 Three Smart Saps (July 30)
* 065 Even As IOU (September 18)
* 066 Sock-a-Bye Baby (November 13)

==Volume Four: 1943–1945==
===Disc One===

====1943 in film|1943====
* 067 They Stooge to Conga (January 1)
* 068 Dizzy Detectives (February 5)
* 069 Spook Louder (April 2)
* 070 Back from the Front (May 28)
* 071 Three Little Twirps (July 9)
* 072 Higher Than a Kite (July 30)
* 073 I Can Hardly Wait (August 13)
* 074 Dizzy Pilots (September 24)
* 075 Phony Express (November 18)
* 076 A Gem of a Jam (December 30)

===Disc Two===

====1944 in film|1944====
* 077 Crash Goes the Hash (February 4) Busy Buddies (March 18)
* 079 The Yokes on Me (May 26) Idle Roomers (July 15)
* 081 Gents Without Cents (September 22)
* 082 No Dough Boys (November 24)

====1945 in film|1945====
* 083 Three Pests in a Mess (January 19)
* 084 Booby Dupes (March 17)
* 085 Idiots Deluxe (July 20)
* 086 If a Body Meets a Body (August 30)
* 087 Micro-Phonies (November 15)

==Volume Five: 1946–1948==
===Disc One===

====1946 in film|1946====
* 088 Beer Barrel Polecats (January 10)
* 089 A Bird in the Head (February 28)
* 090 Uncivil War Birds (March 29)
* 091 The Three Troubledoers (April 25)
* 092 Monkey Businessmen (June 20)
* 093 Three Loan Wolves (July 4)
* 094 G.I. Wanna Home (September 5)
* 095 Rhythm and Weep (October 3)
* 096 Three Little Pirates (December 5)

====1947 in film|1947====
* 097 Half-Wits Holiday (January 9) (Curly Howards final starring role) Fright Night (March 6) (Shemp Howards first starring role) Out West (April 24)
* 100 Hold That Lion! (July 17) (Curly Howards final cameo role and first and only episode the four Stooges were shown.)

===Disc Two===
* 101 Brideless Groom (September 11) 
* 102 Sing a Song of Six Pants (October 30)
* 103 All Gummed Up (December 18)

====1948 in film|1948====
* 104 Shivering Sherlocks (January 8)
* 105 Pardon My Clutch (February 26)
* 106 Squareheads of the Round Table (March 4) Fiddlers Three (May 6)
* 108 The Hot Scots (July 8)
* 109 Heavenly Daze (September 2)
* 110 Im a Monkeys Uncle (October 7)
* 111 Mummys Dummies (November 4)
* 112 Crime on Their Hands (December 9)

==Volume Six: 1949–1951==
===Disc One===

====1949 in film|1949==== The Ghost Talks (February 3) Who Done It? (March 3) Hokus Pokus (May 5)
* 116 Fuelin Around (July 7)
* 117 Malice in the Palace (September 1)
* 118 Vagabond Loafers (October 6)
* 119 Dunked in the Deep (November 3)

====1950 in film|1950====
* 120 Punchy Cowpunchers (January 5)
* 121 Hugs and Mugs (February 2)
* 122 Dopey Dicks (March 2) Love at First Bite (May 4)
* 124 Self-Made Maids (July 6)

===Disc Two===
* 125 Three Hams on Rye (September 7)
* 126 Studio Stoops (October 5)
* 127 Slaphappy Sleuths (November 9)
* 128 A Snitch in Time (December 7)

====1951 in film|1951====
* 129 Three Arabian Nuts (January 4)
* 130 Baby Sitters Jitters (February 1)
* 131 Dont Throw That Knife (May 3)
* 132 Scrambled Brains (June 7)
* 133 Merry Mavericks (September 6)
* 134 The Tooth Will Out (October 4)
* 135 Hula-La-La (November 1)
* 136 Pest Man Wins (December 6)

==Volume Seven: 1952–1954==

===Disc One===

====1952 in film|1952====
* 137 A Missed Fortune (January 3)
* 138 Listen, Judge (March 6)
* 139 Corny Casanovas (May 1)
* 140 He Cooked His Goose (July 3)
* 141 Gents in a Jam (July 4)
* 142 Three Dark Horses (October 16)
* 143 Cuckoo on a Choo Choo (December 4)

====1953 in film|1953====
* 144 Up in Daisys Penthouse (February 5)
* 145 Booty and the Beast (March 5)
* 146 Loose Loot (April 2)
* 147 Tricky Dicks (May 7)

===Disc Two===
* 148 Spooks (film)|Spooks! (June 15) (First 3-D film|3-D short, first flat widescreen short)
* 149 Pardon My Backfire (August 15) (Second and last 3-D short)
* 150 Rip, Sew and Stitch (September 3) Bubble Trouble (October 8)
* 152 Goof on the Roof (December 3)

====1954 in film|1954====
* 153 Income Tax Sappy (February 4)
* 154 Musty Musketeers (May 13)
* 155 Pals and Gals (June 3)
* 156 Knutzy Knights (September 2)
* 157 Shot in the Frontier (October 7)
* 158 Scotched in Scotland (November 4)

==Volume Eight: 1955–1959==

===Disc One===

====1955 in film|1955====
* 159 Fling in the Ring (January 6)
* 160 Of Cash and Hash (February 3)
* 161 Gypped in the Penthouse (March 10)
* 162 Bedlam in Paradise (April 14)
* 163 Stone Age Romeos (June 2)
* 164 Wham Bam Slam|Wham-Bam-Slam! (September 1) Hot Ice (October 6)
* 166 Blunder Boys (November 3)

====1956 in film|1956====
* 167 Husbands Beware (January 5)
* 168 Creeps (film)|Creeps (February 2)
* 169 Flagpole Jitters (April 5)

===Disc Two===
* 170 For Crimin Out Loud (May 3)
* 171 Rumpus in the Harem (June 21) Hot Stuff (September 6)
* 173 Scheming Schemers (October 4)
* 174 Commotion on the Ocean (November 8) (Shemp Howards final starring role)

====1957 in film|1957====
* 175 Hoofs and Goofs (January 31) (Joe Bessers first starring role)
* 176 Muscle Up a Little Closer (February 28)
* 177 A Merry Mix Up (March 28)
* 178 Space Ship Sappy (April 18)
* 179 Guns a Poppin (June 13)
* 180 Horsing Around (September 12)

===Disc Three===
* 181 Rusty Romeos (October 17)
* 182 Outer Space Jitters (December 5)

====1958 in film|1958====
* 183 Quiz Whizz (February 13)
* 184 Fifi Blows Her Top (April 10)
* 185 Pies and Guys (June 12)
* 186 Sweet and Hot (September 4)
* 187 Flying Saucer Daffy (October 9)
* 188 Oils Well That Ends Well (December 4)

====1959 in film|1959====
* 189 Triple Crossed (February 2)
* 190 Sappy Bull Fighters (June 4)

==Rare Treasures from the Columbia Pictures Vault==
3-DVD set of bonus material released with Ultimate Collection box set, focusing on solo films featuring Shemp Howard, Joe Besser and Joe DeRita. Also included were animated cartoons featuring the Stooges.

===Disc One===
====Feature films starring the Three Stooges====
* Rockin in the Rockies (1945)
* Have Rocket, Will Travel (1959) Columbia Cartoons featuring the Three Stooges====
* Bon Bon Parade (1935)
* Merry Mutineers (1936)
* A Hollywood Detour (1942)

===Disc Two===
====Columbia Films shorts featuring/starring Shemp Howard====
* Home on the Rage (1938)
* Glove Slingers (1939)
* Money Squawks (1939)
* Boobs in the Woods (1940)
* Pleased to Mitt You (1940)
* Pick a Peck of Plumbers (1944) (partial remake of A Plumbing We Will Go)
* Open Season for Saps (1944) (remake of Charley Chases initial Columbia short The Grand Hooter)
* Off Again, On Again (1945) (remake of Charley Chases Time Out for Trouble)
* Where the Pest Begins (1945)
* A Hit with a Miss (1945) (remake of Punch Drunks)
* Mr. Noisy (1946) (remake of Charley Chases The Heckler)
* Jiggers, My Wife (1946)
* Society Mugs (1946) (remake of Termites of 1938)
* Bride and Gloom (1947) (remake of Charley Chases The Awful Goof)

===Disc Three===
====Columbia Films shorts starring Joe Besser====
* Waiting in the Lurch (1949)
* Dizzy Yardbird (1950)
* Fraidy Cat (1951) (remake of Dizzy Detectives)
* Aim, Fire, Scoot (1952) (remake of Boobs in Arms)
* Caught on the Bounce (1952)
* Spies and Guys (1953)
* The Fire Chaser (1954) (remake of Waiting in the Lurch)
* G.I. Dood It (1955) (remake of Dizzy Yardbird)
* Hook A Crook (1955) (remake of Fraidy Cat and Dizzy Detectives)
* Army Daze (1956) (remake of Aim, Fire, Scoot and Boobs in Arms)

====Columbia Films shorts starring Joe DeRita====
* Slappily Married (1946) (remake of Andy Clydes A Maid Made Mad)
* The Good Bad Egg (1947) (remake of Andy Clydes Knee Action)
* Wedlock Deadlock (1947) (remake of Monte Collinss Unrelated Relations)
* Jitter Bughouse (1948) (remake of The Radio Rogues Do Your Stuff)

==References==
 

==External links==
 
 
*   Detailed synopses, with historical lore and anecdotes, of every short and film in this set, in chronological order with year dates.


 

 
 
 
 
 
 