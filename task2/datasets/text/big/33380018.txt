Yorck
 
{{Infobox film
| name           =  Yorck
| image          = 
| caption        =
| director       = Konrad Petzold
| producer       =  Konrad Petzold
| writer         = 
| starring       = Charlotte Küter, Bernd Kuss, Peter Schmidt
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  4 April 1958 (East Germany)
| runtime        = 60 min
| country        = Germany
| awards         = 
| language       = German
| budget         = 
| preceded_by    =
| followed_by    =
}} German war film directed by Gustav Ucicky and starring Werner Krauss, Grete Mosheim and Rudolf Forster. It portrays the life of the Prussian General Ludwig Yorck von Wartenburg, particularly his refusal to serve in Napoleons army during the French Invasion of Russia in 1812. It was a Prussian film, one of a cycle of films made during the era that focused on Prussian history.

==Cast==
* Werner Krauss as General Yorck von Wartenberg 
* Grete Mosheim as Barbara  King Friedrich Wilhelm III of Prussia Karl August Fürst von Hardenberg General von Clausewitz General Kleist von Nollendorf General McDonald 
* Hans Rehmann as Lt. Rüdiger Heyking 
* Walter Janssen as Vicomte Noailles 
* Günther Hadank as Seydlitz 
* Theodor Loos as Roeder 
* Paul Otto as Natzmer  Graf Diebitsch-Sabalkanskij
* Jakob Tiedtke as Krause

==External links==
* 
*  at Virtual History

 

 
 
 
 
 
 
 
 
 
 
 
 

 
 