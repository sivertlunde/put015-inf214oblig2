Madame Blueberry
 
{{Infobox film
| name = Madame Blueberry
| image = veggietales_dvd_blueberry.jpg
| caption = DVD cover art
| writer = Mike Nawrocki Original story Gustave Flaubert
| director = Mike Nawrocki Big Idea
| distributor = Word Entertainment (1998 version) Lyrick Studios (1999 version) Warner Home Video (2002 version) Sony Wonder (2003 DVD version)
| producer = Chris Olsen Dan Anderson Mike Sage Shelby Vischer
| editing = John Wahba Mike Nawrocki
| music = Kurt Heinecke Phil Vischer Mike Nawrocki
| released =   (1998 Word Entertainment)   (1999 Lyrick Studios)
| runtime = 37 minutes 40 seconds (all versions) 40 minutes (1999 Lyrick Studios VHS) 38 1/2 minutes (1998 Word Entertainment VHS)
| language = English Spanish French Greek
}}
Madame Blueberry is the tenth episode in the VeggieTales animated series. On January 1, 1998, Word Entertainment originally released on VHS, and on October 1999, it was reissued by Lyrick Studios with a trailer for Larry-Boy and the Rumor Weed|Larry-Boy! And the Rumor Weed saying Coming February 2000!. It was also re-released on VHS on both 2002 and 2003 by Warner Home Video and issued on DVD for the very first time. Subtitled "A Lesson in Thankfulness", it conveys the message that material possessions will never truly make us happy, and that instead we must be thankful for what we have.

==Plot== The French Peas, Jean-Claude and Philippe, they appear and Jean-Claude begins narrating the story.
 the Scallions) arrive at Madame Blueberrys treehouse to promote "Stuff-Mart," a new local mega-store, and claim that the store has anything and everything that Blueberry needs to be happy. Though Bob has his doubts, Blueberry eagerly buys into the sales pitch, following the three salesmen to Stuff-Mart.

On the way there, Madame Blueberry sees a girl named Annie happily celebrating her birthday with her parents, even though all she has to celebrate with is a single piece of apple pie with a candle on top. She wonders why Annie is so happy even with so little, but instantly forgets about it upon entering Stuff-Mart and goes into a shopping frenzy. As she buys everything she sees, Madame Blueberry has it all delivered to her tree house via pea-driven shopping carts.

During a lunch break, Madame Blueberry sees Junior Asparaguss excitement over a new ball his dad bought for him at Stuff-Mart, despite his disappointment over learning that his dad couldnt afford the train set he really wanted. Madame Blueberry again wonders why someone can be happy even though he isnt getting what he really wants and then wonders why she isnt happy despite receiving everything she wants. Realizing that she already has a wonderful home and two loyal friends in her butlers, Madame Blueberry finally sees her folly in not being thankful for what she has and tells the salesmen that she is done giving in to their materialistic temptations.

When she leaves Stuff-Mart, Madame Blueberry beholds to her horror her treehouse tipping over in the distance, full of the stuff she bought still being delivered to her home.  The arrival of a giant air compressor proves to be too much for the tree house to handle as the back door opens and dumps every one of her possessions into the lake below. In turn, the sudden loss of weight causes the tree to fling the house out of its branches, over the forest, and onto the ground where it immediately collapses. As the story draws to a close, Madame Blueberry is comforted by the presence of her butlers, Annie, Junior, and their respective parents, and trusts that everything will turn out right in the end.  Although she is now homeless, Madame Blueberry is nonetheless happy and finally understands that "a thankful heart is a happy heart".

At the end of the story, Bob and Larry are returning being seen crying because the story was beautiful, but proceeds to the end of the show with the Bible verse, Proverbs 15:27a.

==Cast of Voice Actors==
* Phil Vischer Voices:
**Bob the Tomato Scallion #1 Phillipe Pea
**Archibald Asparagus
**List of VeggieTales characters#Mr. Lunt|Mr. Lunt
**List of VeggieTales characters#The Green Onions|Annies Dad
**The Silly Song Narrator
* Mike Nawrocki Voices:
**Larry the Cucumber Scallion #2 Jean Claude Pea Jerry Gourd
* Megan Moore Burns Voices: Madame Blueberry
* Mike Sage Voices: Scallion #3
* Lisa Vischer Voices:
**Junior Asparagus
**List of VeggieTales characters#The Green Onions|Annies Mom
* Dan Anderson Voices: Dad Asparagus
* Shelby Vischer Voices: Annie

==Cast of Characters==
* Madame Blueberry as herself
* Bob the Tomato as himself
* Larry the Cucumber as himself
* Scallion #1 as Stuff Mart Salesman #1
* Scallion #2 as Stuff Mart Salesman #2
* Scallion #3 as Stuff Mart Salesman #3
* Mother Green Onion as Herself
* Father Green Onion as Himself
* Annie the Green Onion as Herself
* Archibald Asparagus as himself, only writing the notes due to a bad caution of the latest silly song
* Mr. Lunt as himself, only singing His Cheeseburger
* The French Peas as Themselves
* Dad Asparagus as Himself
* Junior Asparagus as the himself

==Segments==
*Countertop Intro
*Madame Blueberry (Part I)
*Silly Songs with Larry (as "Love Songs with Mr. Lunt"): His Cheeseburger
*Madame Blueberry (Part II)
*QWERTY Closer (crying scene)

==Songs==
In addition to the ubiquitous "VeggieTales Theme" and "What We Have Learned," this episode contains the following songs:

*Im So Blue, sung by Madame Blueberry, Bob & Larry
*Stuff Mart Rap (also called the Salezmunz Rap), sung by the Scallions
*His Cheeseburger, sung by Mr. Lunt during the Silly Songs segment
*The Thankfulness Song, Part 1, sung by Annie the Green Onion
*The Thankfulness Song, Part 2, sung by Dad Asparagus and Junior Asparagus
*The Thankfulness Song, Part 3, sung by Madame Blueberry, Junior, Annie, Dad Asparagus, and Annies Dad
*Stuff Mart Blue Danube, End Credits

==References==
 

==External links==
* 
* 

 
 

 
 
 