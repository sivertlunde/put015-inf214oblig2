Interview (1971 film)
{{Infobox film name           = Interview image          =  image_size     =  caption        =  director       = Mrinal Sen producer       = Mrinal Sen Productions writer         = Ashish Barman narrator       = Ranjit Mullick starring       = Ranjit Mullick, Karuna Banerjee music          = Vijay Raghav Rao cinematography = K. K. Mahajan editing        =  distributor    =  released       =   runtime        = 101 min. country        = India language  Bengali
|budget         = 
}}
 Bengali film directed by noted Indian art film director Mrinal Sen. A path-breaking film in terms of the narrative innovation and cinematic technique, it was a commercial success and went to run for six weeks amidst gushing admiration and accolades, when it was screened first. It also happened to be the debut film of Ranjit Mullick. Though according to the director, it was a film on the colonial hangover,it touched upon the diverse issue of anti-establishment, middle class cowardice, unemployment.

==Plot==
Ranjit Mullick is a smart personable young man. A friend of the family, who works in a foreign firm, has assured him of a lucrative job in his firm. All Ranjit has to do is to appear in an interview, dressed in a western style suit.

It seems a simple task, but fate wills otherwise. A strike by a labour Union means that he cant get his suit back from the laundry. His fathers old suit wont fit him. He borrows a suit but loses it in a fracas. Ultimately he has to go to the interview dressed in the traditional Bengali Dhoti and Kurta (Dhuti-Panjabi).

This film is considered to be the first film of Mrinal Sens Calcutta trilogy, the others being Calcutta 71, and Padatik (film)|Padatik.

==Cast==
* Ranjit Mullick
* Karuna Banerjee

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 


 