Anytown (film)
{{Infobox film
| name           = Anytown
| image          = Anytown_poster.jpg
| caption        = Film Poster
| director       = Dave Rodriguez
| producer       = Matt Keith George M. Kostuch Dave Rodriguez
| writer         = Zak Meyers Dave Rodriguez
| starring       = Matt OLeary Marshall Allman Jonathan Halyalkar Sam Murphy Ross Britz Brooke Johnson Meghan Stansfield Paul Ben-Victor Natasha Henstridge
| music          = Phil Symonds
| cinematography = John Barr
| editing        = Lauren A. Schaffer
| distributor    = 
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Anytown is a 2009 drama film, written and directed by Dave Rodriguez. The film has won three film awards. "Best Picture" at the Charleston International Film Festival, "Excellence in Filmmaking" at The Method Fest and "Best Screenplay" at the Long Island International Film Expo. Anytown also garnered a strong review from Variety.

==Cast==
*Matt OLeary as Brandon OLeary 
*Marshall Allman as Mike Grossman 
*Jonathan Halyalkar as Eric Singh 
*Sam Murphy as Bo Aznabev 
*Ross Britz as Kyle Castranovo 
*Brooke Johnson as Michelle 
*Meghan Stansfield as Charlotte 
*Paul Ben-Victor as Principal Wheeler 
*Natasha Henstridge as Carol Mills

==External links==
*  
*  

 
 
 
 
 
 

 