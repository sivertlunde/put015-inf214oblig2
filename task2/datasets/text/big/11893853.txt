Paperback Hero (1999 film)
 
 
{{Infobox Film
| name           = Paperback Hero
| image          = Paperback Hero.jpg
| image size     =
| alt            = 
| caption        = DVD cover
| director       = Antony Bowman John Winter
| writer         = Antony Bowman
| starring       = Hugh Jackman Claudia Karvan Angie Milliken
| music          = Burkhard von Dallwitz
| cinematography = David Burr
| editing        = Veronika Jenet
| distributor    = Polygram Filmed Entertainment
| released       = 25 March 1999
| runtime        = 96 minutes
| country        = Australia
| language       = English
| gross            = $1,369,280 
}} Australian comedy film starring Claudia Karvan and Hugh Jackman. It was directed by Antony Bowman who also wrote the screenplay.  The film was predominantly shot in Queensland including Nindigully.   

==Synopsis==
The film follows the adventures of a truck driver, Jack Willis, from rural Australia after he writes a bestselling novel. Being embarrassed by its romantic content, he uses the name of his female friend as a pen name. When a publisher decides to take the author "Ruby Vale" on, he is suddenly faced with a problem. He tells his friend that he has used her name, and initially, she wants to tell the publisher the truth. However, Jack and the publisher convince her by telling her that they will organise her wedding, (she is marrying Jacks best friend, Hamish) and so, Ruby is convinced.

Ruby and Jack then go to Sydney together, so that she can gain the book some more publicity. On the way, Ruby reads his book and finds that the lead female character is herself, and the main male character, Brian, is Jack. She is significantly touched by his book, even crying when she reads of Brians death. The next few days go smoothly, although Ruby often voices her opinion that Jack should get the credit for writing such a story.

Things turn sour however, when the publisher finds out that Jack is indeed the author, but tells Jack not to let Ruby know. He doesnt, and that night the couple kiss after another publicity event. Hamish comes to Sydney, as he knows that Jack is the author, and tells Ruby, just before she is about to go on a satellite feed through to London, that he will stay for as long as it takes him to have a drink. That, coupled with the eventual knowledge that he knows who the real author is, sends Ruby back to Hamish and back to her rural hometown.

Jack does not follow her, instead staying and eventually making up his mind on what to do. While watching the T.V. in the local cafe, she hears Jacks voice on it. She finds that he has let the world know that he is the real author of the novel.

Soon after this, Hamish breaks up with her, knowing that she really loves Jack. As Ruby is wiping away the tears and driving home, she sees a plane flying overhead, spelling the words, "I love you". She stops her car, and the plane lands in front of her. Jack hops out, and after some gentle sparring of words, they kiss and the story ends.

==Cast==
*Hugh Jackman as Jack Willis
*Claudia Karvan as Ruby Vale
*Angie Milliken as Ziggy Keane
*Andrew S. Gilbert as Hamish
*Jeanie Drynan as Suzie

==Reception==

Reviewing the film on The Movie Show, David Stratton suggested that it worked very well for the first half an hour but was let down by laboured plot developments and a slowed pace from thereon. He did however commend Jackman and Karvan for making a great romantic team, and also praised Jeanie Drynan for her performance. He gave the film 2½ stars out of 5. Margaret Pomeranz gave the film a much more favourable review and 3½ stars. 

Paperback Hero grossed $1,369,280 at the box office in Australia. 

==See also==
* Cinema of Australia

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 