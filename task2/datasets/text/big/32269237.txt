Dacait
{{Infobox film
| name = Dacait
| image = 
| writer = 
| starring = Sunny Deol Raakhee Meenakshi Sheshadri Suresh Oberoi
| director = Rahul Rawail
| producer = 
| music = R.D. Burman
| lyrics =
| released = 10 April 1987
| language = Hindi
}}
 Chambal area which is the confluence of 3 states UP MP & Rajasthan and has been ignored for development since independence.

The Chambal ravine or beehad (Hindi-बीहड़) in Dholpur has harboured dacoits (bandits) for centuries. In modern times, due to the old legends and rugged terrain, the labyrinthian ravines along the river were hiding places to gangs of bandits led by colourful figures like Man Singh, Daku Madho Singh,Dhanraj singh Rathur(Chhibramau) Paan Singh Tomar and Phoolan Devi. The last notable dacoit, Nirbhay Gujjar was killed in 2005. Today, a tourist lodge and other facilities promote eco-tourism here. This area is also known as the "Veer Bhoomi Chambal".

==Characters==
* Raakhee ... Arjuns Mother
* Sunny Deol ... Arjun Yadav
* Meenakshi Sheshadri ... Javli Satyajeet ... Ahmed
* Suresh Oberoi... Amritlal Yadav
* Raza Murad... Thakur Bhanwar Singh
* Paresh Rawal ... Vishnu Pandey
* Daljeet Kaur ... Amritlals wife

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Maine Kaha Tumne Suna"
| Suresh Wadkar, Asha Bhosle
|-
| 2
| "Gaon Mein Mach Gaya Shor"
| Kishore Kumar, Suresh Wadkar, Asha Bhosle
|-
| 3
| "Mere Yaar Ko Mere Allah"
| Shabbir Kumar
|-
| 4
| "Kis Kaaran Naiya Doli"
| Suresh Wadkar, Asha Bhosle
|-
| 5
| "Woh Teri Duniya Nahin"
| Lata Mangeshkar
|}

==External links==
*  

 
 
 
 
 

 