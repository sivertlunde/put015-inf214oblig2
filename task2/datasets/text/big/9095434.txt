Dixie (film)
{{Infobox film
| name           = Dixie
| image          = DixieBingCrosbyFilm.jpg
| border         = yes
| caption        = Theatrical release poster
| director       = A. Edward Sutherland
| producer       = Paul Jones
| writer         = {{Plainlist|
* Karl Tunberg
* Darrell Ware
* Claude Binyon (adaptation)
}}
| screenplay     = 
| story          = William Rankin
| starring       = {{Plainlist|
* Bing Crosby
* Dorothy Lamour
}}
| music          = {{Plainlist|
* Jimmy Van Heusen
* Johnny Burke
}}
| cinematography = William C. Mellor
| editing        = William Shea
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Universal and not actively marketed. The movie was broadcast several times in the late 1980s on American Movie Classics channel. The movie produced one of Crosbys most popular songs, "Sunday, Monday, or Always".  

==Cast==
* Bing Crosby as Daniel Decatur Emmett
* Dorothy Lamour as Millie Cook
* Billy De Wolfe as Mr. Bones
* Marjorie Reynolds as Jean Mason
* Lynne Overman as Mr. Whitlock
* Eddie Foy, Jr. as Mr. Felham
* Raymond Walburn as Mr. Cook Grant Mitchell as Mr. Mason
* Clara Blandick as Mrs. Mason
* Tom Herbert as Homer
* Olin Howland as Mr. Deveraux (as Olin Howlin)
* Robert Warwick as Mr. LaPlant

==External links==
*  

 
 

 
 
 
 
 
 
 