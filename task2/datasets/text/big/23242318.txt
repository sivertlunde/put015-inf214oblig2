The Opponent
{{Infobox Film
| name           = The Opponent
| image          = Opponent-erika.jpg
| caption        = 
| director       = Eugene Jarecki
| producer       = Eugene Jarecki Paige Simpson
| writer         = Eugene Jarecki
| narrator       = 
| starring       = Erika Eleniak James Colby Aunjanue Ellis John Doman Harry OReilly
| music          = Vytas Nagisetty
| cinematography = Joe Di Gennaro
| editing        = Simon Barker
| distributor    = Trimark Video
| released       = 2000
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 sports drama film released in 2000. The film stars Erika Eleniak and James Colby.

==Film synopsis==

When Patty Sullivan (Eleniak) gets tired of the abuse of her boyfriend, she decides to take boxing lessons to protect herself. During her practices, she gains the attention of fellow boxer, June (Aunjanue Ellis) and promoter, Fred (John Doman). They introduce her to Tommy (Colby), a trainer that runs a local gym. Tommy takes Patty through a rigorous training program to prepare her for her first professional match.

==Filming==

The film was shot in several New York locations like Port Chester, Troy, New York|Troy, and Waterford (town), New York|Waterford.

==Cast==

* Erika Eleniak as Patty Sullivan
* James Colby as Tommy
* Aunjanue Ellis as June
* John Doman as Fred
* Harry OReilly as Jack

==Release==
 AFI Film Festival on October 20, 2000. This was followed the same year by a premier on New York City.

==External links==
* 
*  at Rotten Tomatoes

 
 
 
 

 