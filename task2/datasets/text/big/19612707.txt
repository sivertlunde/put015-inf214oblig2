The Spirit of Culver
{{Infobox_Film |
  name           = The Spirit of Culver |
  image          =  |
    producer       = Burt Kelly |
  director       = Joseph Santley |
  writer         = Tom Buckingham, George Green, Clarence Marks (story)  Whitney Bolton, Nathanael West (screenplay)|
  starring       = Jackie Cooper Freddie Bartholomew | Frank Skinner Charles Henderson |
  cinematography = Elwood Bredell | Frank Gross |
  distributor    = Universal Pictures  |
  released       = 10 March 1939 |
  runtime        = 89 minutes |
  country        =   | English |
  budget         =  |
}} 
	 1939 drama starring Jackie Cooper and Freddie Bartholomew. Directed by Joseph Santley and written by Whitney Bolton and Nathanael West, the film is a remake of 1932 in film|1932s Tom Brown of Culver.

==Plot== Culver military academy. His arrogance and unwillingness to comply with the academys strict rules soon gets him in trouble, but through the help of his roommate he gets by.

==Cast==
* Jackie Cooper - Tom Allen 
* Freddie Bartholomew - Bob Randolph 
* Andy Devine - Tubby 
* Henry Hull - Doc Allen 
* Jackie Moran - Perkins 
* Tim Holt - Capt. Wilson 
* Gene Reynolds - Carruthers 
* Kathryn Kane - June Macy 
* Walter Tetley - Hank 
* Pierre Watkin - Capt. Wharton  John Hamilton - Maj. White 

==External links==
* 
* 
*  

 
 
 
 
 
 
 
 