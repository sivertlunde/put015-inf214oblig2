Ratchagan
{{Infobox film
| name           = Ratchagan
| image          = Ratchagan DVD Cover.jpg
| caption        = DVD Cover
| writer         = Crazy Mohan  
| story          = K. T. Kunjumon
| screenplay     = Praveen Gandhi 
| producer       = K. T. Kunjumon Francis Joseph
| director       = Praveen Gandhi
| starring       = Akkineni Nagarjuna Sushmita Sen 
| music          = A. R. Rahman
| cinematography = Ajayan Vincent
| editing        = B. Lenin V. T. Vijayan
| studio         = J. R. S. Combines
| released       =  
| runtime        = 2:33:47
| country        =   India
| language       = Tamil
| budget         =  18 Crores
| preceded_by    = 
| followed_by    = 
}}
 Tamil action film, produced by K. T. Kunjumon& Francis Joseph on J. R. S. Combines banner, directed by Praveen Gandhi.  Starring Akkineni Nagarjuna and Sushmita Sen in the lead roles, alongside Raghuvaran, S. P. Balasubrahmanyam, Vadivelu and Girish Karnad in supporting roles. The soundtrack composed by A. R. Rahman remained a chartbuster.

==Plot==
Ajay (Akkineni Nagarjuna|Nagarjuna) is an unemployed youth who always gets into trouble because of his temper. This loud and angry young man gets heated up when he encounters anyone committing a crime; he takes the law into his own hands, beats them up and then gives them some free advice. Ajay’s father (S. P. Balasubrahmanyam), who works for an insurance firm, is very unhappy with his son’s attitude, and tries his best to change Ajays attitude through a meditation expert but he fails in this matter as the expert suggests that Ajays anguish is reasonable. Ajay meets Sonia (Sushmita Sen) through his father, and Sonia falls in love with him. What Sonia likes about Ajay is his temper and anger when he encounters anything which is against law. Sonia gets Ajay’s fathers help to get Ajay to love her. Still, Ajay is not interested in her and seriously rejects her at first; but he later changes his mind after going through some unusual situations, and love blossoms between them.

Sonia’s father Sriram ( ), and they hamper the progress of his factory with frequent, pre-planned mishaps. This leads to the factorys property loss.

Gnaneswar is the son of Srirams stepmother who gets deceived by Sriram and loses his share of his fathers common business empire. Further, in unusual circumstances, Gnaneswar spends some years in jail and returns with a planned conspiracy and vengeance on his stepbrother Sriram. Gnaneswar is now trying to put Sriram out of business, so his ten men create havoc in the factory by killing old workers and ragging new workers. So when Ajay comes to work in Sriram’s factory, these ten men do the same to Ajay. Ajay keeps cool, trying to keep his promise to Sriram. But Sriram hopes otherwise; he wants Ajay to break the promise and beat up the ten men and throw them out of the factory. At the same time, Sriram hopes that the imminent marriage between Ajay and Sonia will be cancelled, since he does not want his daughter to get married to an angry street fighter like Ajay. So Sriram keeps on hoping that Ajay will somehow burst out.

When Ajays father is murdered, he banishes the ten men from the factory. Meanwhile, Gnaneswar kidnaps Sonia and blackmails Ajay and Sriram that Sonia is going to kill herself (to save Ajay from Gnaneswar) and destroy Srirams factory by bombarding it with a truck armed with a powerful Russian explosive. The rest of the film concerns the manner in which Gnaneswar spends his time in jail and develops his own business empire to crush Sriram, how Srirams criminal deeds and false motives are exposed to his daughter, what happens to Easwar and Srirams business empire, and how Sonia is rescued by Ajay.

==Cast==
{{columns-list|3| Nagarjuna as Ajay
* Sushmita Sen as Sonia
* S. P. Balasubrahmanyam as Padmanaban
* Raghuvaran as Ghaneswar
* Girish Karnad as Sriram
* Vadivelu as Ajays friend
* Subhalekha Sudhakar
* Ajay Rathnam
* Chinni Jayanth Sudarshan
* Shankar
* Sanjay
* Thalapathy Dinesh
* Mitran
* Abusalim
* Josafh
* Lukaavargis
* Dalidth Singh
* Kevin Erik Gray
* Karikalan
* Madan Babu
* Gundu Kalyanam
* Juttu Narasimham
* Sukumari
* Kavitha Alphonso - Item girl
* Brendan Schindler
* Vinodini
}}

==Production==
The film was launched in January 1996 but filming only commenced the following year in January 1997. Despite signing on established cast and crew such as Akkineni Nagarjuna|Nagarjuna, Miss Universe pageant winner Sushmita Sen, Girish Karnad and A. R. Rahman, Kunjumon signed debutant Praveen Gandhi to be director. The producer revealed that in the director he saw "a young man who is very talented and full of ideas who was willing to work hard."  The period in between was spent with pre-production works with art director Thotta Tharani constructing a huge set in Mahabalipuram with an elaborate bungalow and swimming pool as a part of the heroines home, as well as a huge five star hotel where the villains lived and a landing area for helicopters. http://www.indolink.com/tamil/cinema/Specials/97/Nov/deepavali/deepavali1.htm 
 Suresh of Panneer Pushpangal.

==Release==
The film was initially slated to be released on July 31, 1997 but the strikes in the Tamil film industry delayed such plans.
 

==Soundtrack==
{{Infobox album |  
 Name = Ratchagan
| Type = Soundtrack
| Artist = A.R.Rahman
| Cover = Ratchagancover.jpg
| Caption = Front Cover
| Released = 1997
| Recorded = Panchathan Record Inn Feature film soundtrack
| Length =
| Label = T-Series
| Producer = A. R. Rahman
| Reviews =
| Last album = Daud (film)|Daud  (1997)
| This album = Ratchagan (1997)
| Next album = Vishwavidhaata   (1997)
}}
The soundtrack featured eight songs and a theme song composed by A. R. Rahman. The lyrics were written by Vairamuthu and Vaali (poet)|Vaali. Hindi company, Sony music T-Series, bought the music rights of the film in 1997. 

; Tamil tracklist
{{tracklist
| headline = Track listing
| extra_column = Singer(s)
| all_music =
| all_lyrics = Vairamuthu
| title1 = Soniya Soniya
| extra1 = Udit Narayan, Unnikrishnan, Harini
| length1 = 5:35
| title2 = Love Attack Gopal , Kavita Paudwal
| length2 = 5:01 Vaali
| title3 = Chandiranai Thottathu Yar Sujatha
| length3 = 6:46
| title4 = Pogum Vazhiyellam
| extra4 = K. S. Chitra
| length4 = 4:13
| title5 = Lucky Lucky
| extra5 = S P Balasubrahmanyam, Sukhwinder Singh, Swarnalatha
| length5 = 6:06
| title6 = Nenje Nenje
| extra6 = K J Yesudas, Sadhana Sargam
| length6 = 6:56
| title7 = Kanava Illai Kaatra Srinivas
| length7 = 4:34
| title8 = Mercury Pookkal
| extra8 = Anupama (singer)|Anupama, Swarnalatha
| length8 = 5:23 Vaali
| title9 = Theme Music
| extra9 = Instrumental
| length9 = 3:42
}}

;Telugu tracklist
{{tracklist
| headline = Track listing
| extra_column = Singer(s)
| all_music =
| all_lyrics =
| title1 = Soniya Soniya
| extra1 = Udit Narayan, Unnikrishnan, Harini
| length1 = 5:35
| title2 = Love Attack
| extra2 = S P Balasubrahmanyam, Kavita Paudwal
| length2 = 5:01
| note2 =
| title3 = Chandruni Takinadi Sujatha
| length3 = 6:46
| title4 = Prema
| extra4 = Sangeetha
| length4 = 4:13
| title5 = Lucky Lucky
| extra5 = S P Balasubrahmanyam, Sukhwinder Singh, Swarnalatha
| length5 = 6:06
| title6 = Ninne Ninne
| extra6 = K J Yesudas, Sadhana Sargam
| length6 = 6:56
| title7 = Mercury Poollu
| extra7 = Anupama (singer)|Anupama, Swarnalatha
| length7 = 5:23
| note7 =
| title8 = Kalava Leni Kavala 
| extra8 =  K. S. Chitra 
| length8 = 4: 13 
| title9 = Theme Music
| extra9 = Instrumental
| length9 = 3:42
}}

==References==
 

==External links==
*  

 
 
 
 
 
 