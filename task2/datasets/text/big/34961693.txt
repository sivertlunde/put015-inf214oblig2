Imelda (film)
 
{{Infobox film
| name           = Imelda
| image          = 
| caption        = 
| alt            = 
| director       = Ramona S. Diaz
| producer       = Ramona S. Diaz
| screenplay     = 
| based on       = 
| starring       = {{Plain list |
* Imelda Marcos
* Imee Marcos
* Ferdinand Marcos, Jr.
}}
| music          = Bob Aves Grace Nono
| cinematography = Ferne Pearlstein
| editing        = Leah Marino
| studio         = CineDiaz
| distributor    = Unitel Pictures
| released       =  
| runtime        = 103 minutes
| country        = Philippines
| language       = Filipino, English
| website        = 
| budget         = 
| gross          = US$500,992 (Worldwide)
}} First Lady of the Philippines. Beginning with her childhood, the film documents her marriage to future President of the Philippines Ferdinand Marcos, her rule under the dictatorship, her exile in Hawaii and her eventual return to the Philippines.
 Excellence in Cinematography Award Documentary award at Sundance Film Festival in 2004. Imelda outsold Spider-Man 2 in the Philippines, but only took   at the US box office with an additional   worldwide. Reviews from critics are favorable with a 94% fresh rating from Rotten Tomatoes and a 69/100 from Metacritic.

== Synopsis == First Lady Imee and human rights Congress and martial law in 1972 to protect democracy.  She says that she took 3,000 pairs of shoes with her when she went into exile, and justifies her extravagant clothing by saying that it "inspired the poor to dress better".  She also says that she had enormous museums and theaters constructed to enrich the lives of Filipinos. 
 George Hamilton sang "I cant give you anything but love, Imelda", are also used in the film. 

== Release and reception ==
  Excellence in Cinematography Award Documentary.    The film was also screened at the Maryland Film Festival in Baltimore.

Critical reviews were mostly favorable.  .  . Published on December 2, 2004. Retrieved on January 8, 2014.   .  . Published on June 18, 2004.  Retrieved on January 8, 2014.  The film has a 94% fresh rating from  .  Retrieved on January 8, 2014.   The website  . Retrieved on January 8, 2014.   . Retrieved on January 8, 2014.   . Published on June 9, 2004.  Retrieved on January 8, 2014.  The San Francisco Chronicle said it was "spellbinding". 

Both the Chronicle and  . Published on March 17, 2004. Retrieved on January 8, 2014.  Variety said that Imelda who has been accustomed to public attention since her teenage years, was convinced that her charm and charisma would create a more favorable impression in the film than might otherwise be expected. It said that "her defenses of her husband and his regime are obviously filled with rationalizations and obfuscations".  Other reviewers were more scathing,  or note her distorted reality and the many contradictions with which she lives.  

The film took   at the box office in the United States.  .  . Published on August 6, 2004. Retrieved on January 8, 2014.  Outside the US, the film received box office revenue of  . 

== See also == List of banned films

== References ==
 

== External links ==
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 