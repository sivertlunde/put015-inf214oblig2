House of Wedlock
{{Infobox film
| name           = House of Wedlock
| image          = 
| caption        = 
| film name = {{Film name| kana           = ウホッホ探険隊
| romaji         = Uhohho tankentai}}
| director       = Kichitaro Negishi
| producer       = 
| writer         = Yoshimitsu Morita
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = Directors Company New Century Producers (NCP)
| distributor    = Toho
| released       =  
| runtime        = 105 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
}} Japanese film directed by Kichitaro Negishi.

==Plot==
Tokiko Enomoto finds out that her husband Kazuya, who mostly lives at an apartment near his work while she and their children live at home, has been having an affair with a live-in mistress, Ryōko, at his apartment. Tokiko tells her two sons, Tarō and Jirō, that she has decided to divorce their father. The children are shocked but understand their mothers position. Kazuya realizes the damage he has done and tries to remedy the situation.

==Cast==
* Yukiyo Toake as Tokiko Enomoto
* Kunie Tanaka as Kazuya Enomoto
*   as Tarō Enomoto
*   as Jirō Enomoto
*   as Ryōko
*   as the photographer
*  

==Background==
The screenplay for the film, based on a novel by Agata Hikari, was written by director Yoshimitsu Morita.  The film has been described as "one of the forerunners of Japanese new age movies in the 80s." 

==Awards and nominations== Japan Academy Awards 
* Nominated: Best Screenplay - Yoshimitsu Morita

8th Yokohama Film Festival  
* Won: Best Film
* Won: Best Screenplay - Yoshimitsu Morita

29th Blue Ribbon Awards 
* Won: Best Film
* Won: Best Actor - Kunie Tanaka

==References==
 

==External links==
*  
*  
*  

 
 
 

 
 
 
 
 


 