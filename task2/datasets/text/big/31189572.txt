Mumsie
{{Infobox film
| name           = Mumsie
| image          = 
| image_size     = 
| caption        = 
| director       = Herbert Wilcox
| producer       = Herbert Wilcox
| writer         = Edward Knoblock (play) Frank Stanmore
| music          = 
| cinematography = Bernard Knowles
| editing        = 
| studio         = Herbert Wilcox Productions 
| distributor    = Woolf & Freedman Film Service
| released       = September 1927
| runtime        = 6,858 feet 
| country        = United Kingdom English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} British silent silent drama lost film.  It was made at Twickenham Studios.

The film was a major success and helped Herbert Marshall establish himself in Hollywood soon afterwards. 

==Cast==
* Pauline Frederick – Mumsie
* Nelson Keys – Spud Murphy
* Herbert Marshall – Col. Armytage Frank Stanmore – Nobby Clarke
* Donald Macardle – Noel Symonds
* Irene Russell – Louise Symonds
* Rolf Leslie – Edgar Symonds

==References==
 

==Bibliography==
* Low, Rachel. The History of British Film: Volume IV, 1918–1929. Routledge, 1997.

==External links==
* 

 

 
 
 
 
 
 
 
 


 