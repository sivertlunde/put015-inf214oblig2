The Family Secret (1951 film)
The Family Secret is a 1951 crime drama film directed by Henry Levin. The film has a film noir concept. Park (20011), p. 147-148 

==Plot==
Law student David Clark kills best friend Art Bradley in self-defense after an argument. He flees, but later confesses to his father, Howard, a lawyer. The next morning, Howard expects his son to explain what happened to district attorney Redman, but instead David merely asks the DA if he can be of any help after another man, Joe Elsner, is arrested. Elsner is a bookie to whom the dead man owed a debt.

Marie Elsner comes to Howard Clark, asking that he represent her husband in court. David sits by his fathers and the defendants side at the trial. He has always been irresponsible, which is why secretary Lee Pearson keeps resisting Davids romantic attentions, even though she is attracted to him. Howard Clark proves a key eyewitness to be a convicted perjurer. It looks like Elsner might be found innocent, but the stress causes him to have a fatal heart attack. David finally realizes he must turn himself in and promises to Lee he will lead a better life.

==Cast==

* John Derek as David Clark
* Lee J. Cobb as Howard Clark
* Jody Lawrance as Lee Pearson
* Erin OBrien-Moore as Ellen Clark
* Santos Ortega as District Attorney Redman
* Henry ONeill as Donald Muir
* Carl Benton Reid as Dr. Steve Reynolds
* Peggy Converse as Sybil Bradley
* Jean Alexander as Vera Stone
* Dorothy Tree as Marie Elsner
* Whit Bissell as Joe Elsner
* Raymond Greenleaf as Henry Archer Sims
* Onslow Stevens as Judge Geoffrey N. Williams

==References==
*  
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 


 