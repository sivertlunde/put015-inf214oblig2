Trespass (2011 film)
{{Infobox film
| name           = Trespass
| image          = Trespass2011poster.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Joel Schumacher
| producer       = Rene Besson  Irwin Winkler  David Winkler
| writer         = Karl Gajdusek
| starring       = Nicolas Cage  Nicole Kidman  Liana Liberato  Cam Gigandet  Ben Mendelsohn  Jordana Spiro  Dash Mihok
| music          = David Buckley
| cinematography = Andrzej Bartkowiak
| editing        = Bill Pankow
| studio         = Saturn Films Nu Image
| distributor    = Millennium Entertainment
| released       =  
| runtime        = 90 minutes  
| country        = United States
| language       = English
| budget         = $35 million 
| gross          = $10 million  
}} United States on October 14, 2011.  It was released on DVD and Blu-ray Disc just a few weeks later on November 1, 2011.

==Plot== charming facade, it is immediately clear that the family is dysfunctional and emotionally distant: Avery disrespects her parents and, despite being forbidden to do so, sneaks out of the house to go to a party with her friend Kendra. Sarah appears bored with life as a housewife and yearns for more in her marriage while Kyle seems to harbor a hidden aversion towards his wife. Just as Kyle is about to leave for a business transaction, the house is suddenly invaded by a gang of robbers masquerading as police.

The thieves, consisting of leader Elias (Ben Mendelsohn), his stripper girlfriend Petal (Jordana Spiro), his younger brother Jonah (Cam Gigandet), and a large intimidating man named Ty (Dash Mihok), tell Kyle and Sarah that they have been spying on them for some time and are aware of the large amounts of cash and diamonds hidden in their home. They demand Kyle to open a safe hidden in the wall but, despite a threat to utilize a syringe containing lethal injection chemicals, he defiantly refuses, believing that he and Sarah would be killed if he simply gives in to their demands. Sarah, meanwhile, recognizes Jonah through his mask. Through a series of Jonahs flashbacks and a conversation with Elias, it is strongly implied that Sarah and Jonah had a previous affair when the latter was employed as a technician to install the houses security system. Unbeknownst to Elias, Sarah secretly steals the syringe from him during this conversation.

Having left her party in disgust (after the host Jake clumsily attempted to seduce her), Avery returns home and, after a brief chase, is also captured and taken to her parents. Elias appeals to Kyle, claiming that he needs money to pay for a kidney transplantation for his dying mother and that, if Kyle refuses to comply, he will instead take one of Averys kidneys. Sarah catches Elias off-guard and holds him hostage with the syringe. Forced into a Mexican standoff, the thieves compromise by letting Avery escape in exchange for Kyle opening the safe. He does so, revealing it to be completely empty. Kyle explains that he is actually bankrupt and has no money; his house and all of his possessions were bought on loaned credit. Enraged, Elias breaks Kyles hand and instead demands material compensation by taking Sarahs prized diamond necklace. However, Kyle reveals that this too is a fake cubic zirconia replica and is completely worthless. Ty recaptures Avery from outside and brings her back into the house. Kyle volunteers for his own kidney to be taken instead of his daughters, but Elias reveals that it was a ploy and that he hated his mother, who is already dead. Ty grows impatient after receiving a phone call and commands Elias to hurry up; Kyle realizes that the burglars are themselves being coerced into committing the heist against their will.

The thieves then separate the Miller family, with Kyle and Avery being tied up and guarded in the living room and Sarah being pursued by Jonah in the kitchen. Jonah states to Sarah that he still loves her and promises that she and Avery will be left unhurt. Meanwhile, Kyle uses a lighter to burn both his own and Averys binds; they attempt an escape and set off the houses security system. Kyle ends up in a struggle with Ty, and, despite Tys superior strength and fighting skills, Kyle manages to inject a portion of the syringe chemicals into his arm, causing him to fall unconscious.
 drug dealer working for an organized crime syndicate. Shortly after being given a job to sell $180,000 worth of cocaine, Elias and Petal were carjacked at gunpoint and all of their shipments were promptly stolen. Faced with threats of retribution, Elias was then forced to commit a heist (under the supervision of henchman Ty) to pay off his debt. Jonah, who had previously seen the Miller residence, suggested it as a place to rob.

Avery attempts to run out of the house again but is caught by Jonah. Under the threat of her parents being killed, she is forced to answer a call from the security company and successfully convinces them to call off the police. However, a security guard shows up nevertheless, with Sarah ordered to make him leave, but the security guard catches sight of Jonah and recognises him as a colleague, prompting Jonah to shoot him in the head.

Kyle reveals to Elias that the only thing of any worth on him is his life insurance. Desperate and out of options, Elias kicks Kyle down and prepares to kill him. At the last second, Avery remembers the money she saw at the earlier party and pleads with the thieves that she can help them steal it from the party if they spare her fathers life. Elias reluctantly agrees to the proposition and sends Petal to supervise Avery as she drives to the party house and ensure that she keeps her word (during a conversation between Elias and Petal its revealed their daughter was taken into care). Although Averys initial idea is to seduce Jake and then steal his money, she is horrified when Petal begins to deliriously proclaim that she plans to massacre all of the party guests and then take the cash. Seeing a swerving road ahead, Avery accelerates the car, unbuckles Petals seat belt, and intentionally crashes into a telephone pole, incapacitating Petal and allowing Avery to handcuff Petal to the steering wheel.

Back at the Miller home, Sarah learns that Kyle had discovered a picture of her kissing Jonah from security footage and was suspicious for some time that she had been unfaithful. Sarah, however, insists that she is innocent and only loves Kyle: the same flashback is now shown from her perspective, revealing that Jonah is in fact   him into stealing more money for them. More damningly, Ty also claims that Jonah had masterminded the entire plan so he could have an excuse to return to the Miller residence and profess his love to Sarah. Elias is shocked by the allegations but refuses to believe them.

Through the chaos, Sarah and a wounded Kyle escape to the tool shed behind the house and are pursued by Elias and Jonah. After a brief fight, the thieves discover a large amount of money hidden inside the shed; Kyle reveals that he had sold Sarahs real diamond necklace and was saving the money as a nest egg for his family. As Elias and Jonah begin to collect the cash, Avery appears (having survived the car crash with minor injuries) and points a gun at them. Elias calls her bluff and aims his gun at Sarah but he is shot and killed by Jonah for this. Jonah tries again to convince Sarah that she belongs with him but she rebuffs his offers, calling him insane. In an attempt to sacrifice himself, Kyle tells his wife and daughter to run while setting the money in the tool shed on fire. He also shoots Jonahs foot with a nail gun, trapping him in the shed. While Avery goes to call the police, Sarah tries to help Kyle but she is grabbed in a last-ditch effort by Jonah, who is convinced that it is destiny for her to die together with him in the fire. However, Kyle then shoots him in the neck, causing him to fall and become engulfed in the flames. Sarah then carries Kyle away to safety just as the shed collapses.

In the backyard, Kyle tries to tell Sarah to let him die so that she and Avery can survive on his life insurance fund but she refuses, stating that she loves him regardless of whether he has money or not. Avery runs back to her parents announcing that help is finally on the way. The three family members embrace as the real police arrive and surround the house.

==Cast==
* Nicolas Cage as Kyle Miller
* Nicole Kidman as Sarah Miller
* Cam Gigandet as Jonah 
* Jordana Spiro as Petal 
* Ben Mendelsohn as Elias
* Liana Liberato as Avery Miller
* Dash Mihok as Ty
* Nico Tortorella as Jake
* Emily Meade as Kendra
* Terry Milam as Travis

==Production== Thirteen Days and began his career as a film director.

==Reception==
===Critical response===
Trespass was panned by critics and has a rating of 10% on  .   However, the film also received some positive reviews from mainstream critics, praising the performances of Kidman, Gigandet,  and Mendelsohn. 
 Razzie Award Season of Jack and Jill and Just Go with It.

===Box office===
Trespass was only given a limited release for 1 week in 10 theaters in North America and earned $24,094. While the film made $9,988,226 internationally this was still below the production budget of $35 million.  This film became the fastest movie to be released on home video after its initial theater release. This record was previously held by 2003s From Justin to Kelly, which was released on DVD after 29 days; Trespass made it in only 18.

==See also==
*List of films featuring home invasions

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 