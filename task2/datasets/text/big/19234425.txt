Life for Ruth
{{Infobox film
| name           = Life for Ruth
| image          = "Life_for_Ruth"_(1962).jpg
| image_size     =
| caption        = Theatrical poster
| director       = Basil Dearden
| producer       = Basil Dearden Michael Relph
| writer         = Janet Green
| narrator       = Michael Craig Patrick McGoohan Janet Munro
| music          = William Alwyn
| cinematography = Otto Heller
| editing        = John D. Guthridge 
| studio         = Allied Film Makers Rank (UK)
| released       = 30 August 1962 (World Premiere, London)
| runtime        = 93 minutes
| country        = United Kingdom
| language       = English
| budget         = £126,800 Alexander Walker, Hollywood, England, Stein and Day, 1974 p248 
| gross          = £53,788 (by 1971) 
}} 1962 British Michael Craig, Patrick McGoohan and Janet Munro.
 Leicester Square Theatre in Londons West End. 

==Plot==
John Harris finds himself ostracized and placed on trial for allowing his daughter Ruth to die. His religious beliefs forbade him to give consent for a blood transfusion that would have saved her life. Doctor Brown is determined to seek justice for what he sees as the needless death of a young girl.

==Cast== Michael Craig as John Harris 
* Patrick McGoohan as Doctor Brown 
* Janet Munro as Pat Harris  Paul Rogers as Hart Jacobs 
* Malcolm Keen as Mr. Harris Sr 
* Megs Jenkins as Mrs. Gordon  Michael Bryant as Johns counsel 
* Leslie Sands as Clyde 
* Norman Wooland as Counsel for the Crown 
* John Barrie as Mr. Gordon
* Walter Hudd as Judge
* Michael Aldridge as Harvard 
* Basil Dignam as Mapleton 
* Maureen Pryor as Teddys mother
* Kenneth J. Warren as Sergeant Finley
* Ellen McIntosh as Duty sister 
* Frank Finlay as Teddys father  John Welsh as Marshall
* Maurice Colbourne as Vicar 
* Freddy Ramsey as Teddy 
* Lynn Taylor as Ruth 
* Brian Wilde as Newspaper photographer (uncredited)

==Critical reception==
The New York Times wrote of the film, "in avoiding blatant bias, mawkish sentimentality and theatrical flamboyance, it makes a statement that is dramatic, powerful and provocative." 

==See also==
* Jehovahs Witnesses and blood transfusions

==References==
 

== External links ==
* 

 

 
 
 
 
 
 
 
 


 