Sarphira
{{Infobox film
| name           =  Sarphira
| image          =
| caption        =
| director       = Ashok Gaekwad
| producer       =
| writer         = Madhavi Sumeet Saigal Kimi Katkar Kiran Juneja
| music          = Rahul Dev Burman
| cinematography =
| editing        =
| production_company =
| distributor    =
| released       = 10 April 1992
| runtime        =
| country        = India
| awards         =
| language       = Hindi
| budget         =
| gross          =
| website        =
}}
Sarphira is a 1992 Bollywood film directed by Ashok Gaekwad. The film stars Vinod Mehra, Sanjay Dutt, Madhavi (actress)|Madhavi, Sumeet Saigal, Kimi Katkar, Kiran Juneja and Shreeram Lagoo.

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Diwane O Diwane"
| Asha Bhosle
|-
| 2
| "Khwab Dekh Dekhke"
| Mohammed Aziz, Asha Bhosle
|-
| 3
| "Ho Sardi Jukam"
| Suresh Wadkar, Asha Bhosle
|-
| 4
| "Meri Hone Wali Bhabhi"
| Amit Kumar, Asha Bhosle
|}

== External links ==
*  

 
 
 
 


 