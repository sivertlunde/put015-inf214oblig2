At Night, They Dance
{{Infobox film
| name           = At Night, They Dance
| image          = At Night, They Dance.jpg
| alt            =  
| caption        = 
| director       = Isabelle Lavigne
| producer       = Lucie Lambert
| based on       = 
| starring       =  
* Reda Ibrahim Mohamed Ali
* Hind Said Samir Sayed
* Amira Said Samir Sayed
* Bossy Said Samir Sayed
 
| music          = 
| cinematography = 
| editing        = René Roberge
| studio         = 
| distributor    = 
| released       = 
| runtime        = 81 minutes
| country        = Canada Egypt
| language       = Arabic
| budget         = 
| gross          = 
}} Canadian documentary film, released in 2011. Directed by Isabelle Lavigne and Stéphane Thibault, the film profiles a group of belly dancers in Cairo, Egypt.   

The film won the Special Jury Prize (Canadian Feature) at the 2011 Hot Docs Canadian International Documentary Festival, and the Genie Award for Best Feature Length Documentary at the 32nd Genie Awards.

==Director==
Isabelle Lavigne is a Canadian film director known or her previous film J.U.I.C.E, which tackled similar themes dealing with the male social sphere and how women interact with male authority.  The film won several awards.  Lavigne, 30, is now a mother and as such has become increasingly interested in womens issues.   

==Synopsis==
The movie follows the lives of a family of belly dancers living in a small Egyptian neighborhood in Cairo.  Reda is the matriarch of the family who passes down her knowledge of the art of belly dancing to each of her children.  Her husband died more than a year prior to the making of the documentary, so the viewer sees her in a state of mourning and uncertainty.  She represents the authoritative figure, but her seven children often test the boundaries of her power.   Each of her children contributes to the family profession through performing at local events, learning the trade during the day, or in the case of her younger son, watching the process of knowledge transition from generation to generation.  Only three of her daughters currently perform, typically for all-male audiences.  The documentary follows their journeys, from their household to the stage.

The documentary is expository and as such does not include commentary from the director.  However,  the cameras catch moments of questioning and contention between accepted social morality and the belly dancing profession.  Reda’s oldest daughter, Amina, longs to marry one of the male assistants who helps them travel to and from shows, but he is unwilling to marry someone who dances for a living.  Moments such as these emphasize the complex social norms that belly dancing serves to uphold and destroy.  Unlike famous belly dancers in the region, Reda’s daughters dress in more revealing clothing and perform with multiple men surrounding them on stage, indicating the different registers of the dancing style.  This distinction is also made evident by the arrest Reda’s younger daughter, Hind, while returning from a performance.  When stopped by the police and made to reveal the costume beneath her gallibiya, she is arrested for being a belly dancer at the age of sixteen.  In recalling the episode, the daughter recounts their treatment of her as if she were a prostitute.

Tensions surrounding the legitimacy of the belly dancing profession in Egyptian culture, in relationship to individual aspirations for success and love, dominate the film.  Reda’s daughters forge romantic relationships with clients they meet as viewers watch them navigate these interactions, reconciling the sexualized nature of their work with their feelings for their romantic interests.  Furthermore, the film expands the scope of its representation of female-male relationships by containing several scenes in which viewers see the isolation of female characters or their assertion of dominance in a space.  For example, there are several shots of the dancers preparing for their performances near the stage in which we see them completely surrounded by onlooking men.  Alternatively, in Reda’s household, we see men coming to hire her daughters, deferring to her authority over the family business.  Director Isabelle Lavigne uses this film to ask, “How does our environment make us what we are? Who shapes the dreams that we think are ours? How much space is left for being yourself?” 

==Reception==
The film has not been widely screened in Egypt, but has been lauded by  international audiences.    magazine finds that the cinematography was able to capture beautiful shots and sounds in cramped spaces while maintaining an atmosphere of ease and authenticity amongst those filmed.  Hot Docs echoes these sentiments on cinematography, stating that the film captures “breathtaking images.” 

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 