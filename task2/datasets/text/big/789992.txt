Princess Nicotine; or, The Smoke Fairy
{{Infobox film
| name = Princess Nicotine; or, The Smoke Fairy
| image = Princess Nicotine.ogv
| image_size =
| caption =
| director = J. Stuart Blackton
| producer = J. Stuart Blackton
| writer =
| narrator =
| starring =
| music =
| cinematography = Tony Gaudio
| editing =
| distributor = Vitagraph Company of America
| released =  
| runtime = 5 mins.
| country = United States
| language =
| budget =
| gross =
}} 1909 five minute silent film directed by J. Stuart Blackton based on an earlier musical starring Lillian Russell.
 fairies (one of which is played by Gladys Hulette). Audiences marveled at the primitive special effects featuring the fairies interacting with objects much larger than themselves.

Princess Nicotine; or, The Smoke Fairy was the first instance of tobacco product placement (for Sweet Corporal cigarettes and cigars) in the movies.

In 2003, the Library of Congress deemed it "significant" and preserved it in the National Film Registry.

==External links==
*  

 
 
 
 
 
 
 
 
 
 


 