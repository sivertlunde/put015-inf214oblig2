Spangles (1926 film)
{{infobox film
| name           = Spangles
| image          =
| imagesize      =
| caption        =
| director       = Frank OConnor
| producer       = Carl Laemmle
| writer         = Nellie Revell (novel) Hugh Hoffman (adaptation) Leah Baird (scenario) Walter Anthony
| starring       = Marian Nixon
| music          =
| cinematography = Andre Barlatier - ( )
| editing        =
| distributor    = Universal Pictures
| released       = November 7, 1926
| runtime        = 58 minutes; 6 reels
| country        = United States Silent (English intertitles)

}} silent drama Pat OMalley and Hobart Bosworth.  

A surviving print of the film was preserved at UCLA Film and Television and it is out on DVD.

==Cast==
*Marian Nixon - Spangles Delancy Pat OMalley - Dick Hale, Dick Radley
*Hobart Bosworth - Robert Big Bill Bowman
*Gladys Brockwell - Mlle. Dazie
*Jay Emmett - Jack Milford
*James Conly - Biff - a Halfwit
*Grace Gordon - Davidina - Bearded Lady
*Paul Howard - Armless Man
*Tiny Ward - Goliath - A Giant
*Charles Becker - Little Tommy Tumtack - A Dwarf
*Nellie Lane - Fat Woman
*Clarence Wertz - Rawlins
*Harry Schultz - Strong Man
*Herbert Shelly - Skeleton

unbilled
*Walter Brennan - Lunch Counterman
*Joe Martin - Joe Martin - The Chimp

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 


 