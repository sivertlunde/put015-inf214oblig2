The Slave (1917 drama film)
{{infobox_film
| name           = The Slave
| image          =
| imagesize      =
| caption        =
| director       = William Nigh
| writer         = William Nigh (scenario)
| starring       = Valeska Suratt
| music          =
| cinematography = Joseph Ruttenberg
| editing        =
| distributor    = Fox Film Corporation
| released       =  
| runtime        = 5 reels
| country        = United States
| language       = Silent (English intertitles)
}}
 silent drama film written and directed by William Nigh. The film starred Valeska Surratt.  It is now considered Lost film|lost. 

==Plot==
As described in a film magazine review,  Caroline (Suratt) works at a hair dressing parlor. A wealthy man falls in love with her, takes her home in his automobile, and proposes on the curb. Caroline has a dream where she marries the man, who turns out to be penurious and keeps her locked up in his mansion. He finally dies, and Caroline starts out having a good time with his money, but she sees the folly of her ways. She wakes up from the dream and turns down the offer of marriage, deciding instead to wait several years for her honest young man to return from the west.

==Cast==
* Valeska Suratt - Caroline
* Violet Palmer - Dulce
* Eric Mayne - Dr. Atwell
* Herbert Heyes - David Atwell
* Edward Burns - Egbert Atwell 
* Edwin Roseman - Dr. Ghoul (*actor also known as Edward Roseman) 
* Dan Mason - The Fossil
* Tom Brooke - Professor Winther Martin Faust - Author
* Martin Hunt

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 

 