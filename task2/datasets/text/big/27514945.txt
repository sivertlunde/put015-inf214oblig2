Temptation: Eating Me
{{Infobox film
| name = Temptation: Eating Me
| image = Tempation - Eating Me.jpg
| image_size = 
| caption = Theatrical poster for Temptation: Eating Me (2007)
| director = Osamu Satō 
| producer = Akira Fukamachi
| writer = Osamu Satō
| narrator = 
| starring = Akiho Yoshizawa
| music = Ichimi Oba
| cinematography = Takuya Hasegawa
| editing = Shōji Sakai
| studio = Shintōhō
| distributor = Shintōhō
| released = December 7, 2007
| runtime = 63 minutes
| country = Japan
| language = Japanese
| budget = 
| gross = 
| preceded_by = 
| followed_by = 
}}
 2007 Japanese pink film written and directed by Osamu Satō. It won the Bronze Prize at the Pink Grand Prix. Yūka Ōsawa was also given a Best New Actress award at the ceremony. 

==Synopsis==
Aki works part-time at an Italian restaurant owned by Kawano. She is tired of her relationship with her unfaithful boyfriend, Masaru, and has a crush on Kawano. Kawanos lover, Kyoko, had created a dessert which made the restaurant popular, however the recipe was lost with Kyokos death in a surfing accident. Aki tries to impress Kawano by recreating the dessert, but she is not as talented a chef as was Kyoko.    

==Cast==
* Akiho Yoshizawa as Aki 
* Hideki Nishioka ( ) as Kawano
* Elly Akira|Yūka Ōsawa ( ) as Rika
* Naoyuki Chiba ( ) as Masaru
* Yuria Hidaka ( ) as Saori

==Availability==
Osamu Satō filmed Temptation: Eating Me for Shintōhō who released it theatrically in Japan on December 7, 2007.  In early March 2008 it was shown as part of the fourth annual R18 Love Cinema Showcase pink film festival,  and in November of that year it was shown as part of the 2008 Pink Film Festival in South Korea.  It was released on DVD in Japan on July 4, 2008.  

==References==

===Bibliography===
*  
*  
*  

===Notes===
 

 
 
 
 
 

 

 
 
 
 


 
 