Whatever You Wish
 
{{Infobox film name           = Sen Ne Dilersen image          = Sen Ne Dilersen.jpg image_size     =  caption        = DVD cover director       = Cem Başeskioğlu producer       = Kara Film writer         = Cem Başeskioğlu narrator       =  starring       = Işık Yenersu Işın Karaca Fikret Kuşkan Yıldız Kenter music          = Cafer İşleyen Selim Bölükbaşı cinematography = Gökhan Atılmış editing        =  distributor    =  released       = 2005 runtime        = 93 minutes country        = Turkey language  Turkish some Greek
|budget         =  gross          = 
}}
Sen Ne Dilersen (International   pop singer. Famous actors like Fikret Kuşkan and Yıldız Kenter starred in the film. Leading lady "Eleni" who is portrayed by Işık Yenersu, a well-known actress in Turkey, has cancer in the film. Işık Yenersu has also suffered from cancer in real life during the production of the film.   
 Greek family who lives in Turkey. 

==Cast==
*Işık Yenersu (Eleni)
*Fikret Kuşkan (Musa)
*Işın Karaca (Marika)
*Zeynep Eronat (Eftimiya)
*Yıldız Kenter (Mimi)
*Güler Ökten (Anastasia)
*Ahmet Mümtaz Taylan (Zülfikar)
*Haldun Boysan (Rıfkı)
*Okan Yalabık (Stavro)
*Ayçin İnci (Rosas daughter)
*Begüm Birgören (young Eleni)
*Evrim Solmaz (young Eftimiya)
*Ali Taygun (Apostos)
*Hasan Yalnızoğlu (angel)
*Asuman Krause (angel)

Other (uncredited):
*Aytaç Arman
*Anta Toros (Rosa)
*Cem Özer
*Leman Çıdamlı
*Ebru Karanfilci (Sümbül)
*Ayşin Zeren (angel)
*Yakup Yavru (doctor)
*Özden Özgürdal (verger)
*Ayşe Merve

==Awards==
{| class="wikitable"
|-
!  Year !! Award ceremony !! Category !! Winner
|-
|rowspan="3"| 2006
| Adana Altın Koza Film Festival
| Best Supporting Actress
| Zeynep Eronat
|-
| Ankara International Film Festival
| Best Supporting Actress
| Zeynep Eronat
|-
| Ankara International Film Festival
| New hoping writer
| Cem Başeskioğlu
|}

==External links==
* 

 
 
 
 
 