The Magnificent Cuckold
{{Infobox film
| name           =Il magnifico cornuto
| image          =The Magnificent Cuckold.jpg
| caption        =
| director       =Antonio Pietrangeli
| producer       =
| writer         = Diego Fabbri, Ruggero Maccari
| narrator       =
| starring       =
| music          =Armando Trovajoli
| cinematography =Armando Nannuzzi
| editing        = Eraldo Da Roma
| distributor    =
| released       = 1964
| runtime        = 124 minutes
| country        = Italy Italian
| budget         =
}}
 Belgian play Le Cocu magnifique.

==Plot==
The Magnificent Cuckold is about a hat tycoon who is ecstatically, if not hungrily, in love with his youthful wife. It is all blissful, that is, until our man, middle-aged and somewhat of a square among his blasé, upper-class friends to whom cuckoldry is a common practice, is seduced by one of them. At this point doubts and suspicions, like conscience, begin to plague him. If he could succumb to extramarital confections, why not his gorgeous mate? Quickly his love for his spouse degenerates beyond obsessive, into the realm of maniacal. He becomes madly concerned that his wife is cheating on him --even though she is not being unfaithful. When he looks at her it becomes obvious to him that she is a very attractive woman. And, all the men around her must be dying to be with her. Gnawed by jealously, he will imagine variations on nabbing her and her lover red-handed.

==Cast==
*Claudia Cardinale	... 	Maria Grazia
*Ugo Tognazzi	... 	Andrea Artusi
*Bernard Blier	... 	Mariotti
*Michèle Girardon	... 	Cristiana
*Paul Guers	... 	Gabriele
*Philippe Nicaud
*Gian Maria Volonté	... 	Assessore
*Susy Andersen	... 	Wanda Mariotti
*José Luis de Vilallonga	... 	Presidente
*Ester Carloni
*Edda Ferronao
*Olindo De Turres
*Elvira Tonelli
*Antonio Gerini
*Liliana Salvioni

==Reception==
The New York Times published a positive, detailed review and stated Il magnifico cornuto was "mildy amusing". 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 

 