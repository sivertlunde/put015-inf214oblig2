Diary of a Mad Housewife
{{Infobox film
| name           = Diary of a Mad Housewife
| image          = Diary of a Mad Housewife 1970.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = Frank Perry
| producer       = Frank Perry
| screenplay     = Eleanor Perry
| based on       = Diary of a Mad Housewife by Sue Kaufman
| narrator       =  The Alice Cooper Band
| music          = 
| cinematography = Gerald Hirschfeld
| editing        = Sidney Katz
| distributor    = Universal Pictures
| released       =  
| runtime        = 104 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $2,003,745  (rentals)  
}}
Diary of a Mad Housewife is a 1970 comedy-drama film about a frustrated wife portrayed by Carrie Snodgress. Snodgress was nominated for the Academy Award for Best Actress and won a Golden Globe award in the same category. The film was adapted by Eleanor Perry from the 1967 novel by Sue Kaufman and directed by Perrys then-husband, Frank Perry. The film co-stars Richard Benjamin and Frank Langella. 

==Plot==
Tina Balser, an educated, frustrated housewife and mother, is in a loveless marriage with Jonathan, an insufferable, controlling, emotionally abusive, social-climbing lawyer in New York City. He treats her like a servant, undermines her with insults, and belittles her appearance, abilities and the raising of their two girls, who treat their mother with the same rudeness as their father. Searching for relief, she begins a sexually fulfilling affair with a cruel and coarse writer, George Prager, who treats her with similar brusqueness and contempt, which only drives her deeper into despair. She then tries group therapy, but this also proves fruitless when she finds her male psychiatrist, Dr. Linstrom, as well as the other participants, equally shallow and abusive.

==Cast==
* Richard Benjamin as Jonathan Balser
* Frank Langella as George Prager
* Carrie Snodgress as Bettina "Tina" Balser
* Lorraine Cullen as Sylvie Balser
* Frannie Michel as Liz Balser
* Lee Addoms as Mrs. Prinz
* Peter Dohanos as Samuel Keefer
* Katherine Meskill as Charlotte Rady
* Leonard Eliott as M. Henri
* Margo (actress)|Margo as Valma
* Hilda Haynes as Lottie
* Don Symington as Pediatrician
* Allison Mills as Womens Lib Girl The Alice Cooper Band as Themselves
* Lester Rawlins as Dr. Linstrom (uncredited)
* Peter Boyle as Man in group therapy (uncredited)

==Response==
The film was critically acclaimed: it maintains a 77% rating at Rotten Tomatoes.    Roger Ebert gave the movie three out of four stars, saying, "What makes the movie work... is that its played entirely from the housewifes point of view, and that the housewife is played brilliantly by Carrie Snodgress." 

 . Soon after, Young and Snodgress became romantically involved for several years. 

Groucho Marx spoke out against the movie in an interview on The Dick Cavett Show on May 25, 1971. He stated that it was an example of dirty entertainment and that he did not like it because the characters were in bed for 80 minutes. He made a joke of this, and stated, "Well Im not interested in that. I dont care what they are doing in the sack, if I am not doing it, why should I sit in the theater and watch it?" 

==Availability==
The film has never been released on DVD; VHS copies of Housewife have become rare, with sealed copies routinely fetching over $100 on websites like Amazon.com and eBay. 

==Awards and nominations==
*Best Picture – Musical or Comedy (nominee) – 1970 Golden Globe
*Best Actor – Musical or Comedy (nominee) – Richard Benjamin – 1970 Golden Globe
*Best Actress – Musical or Comedy (winner) – Carrie Snodgress – 1970 Golden Globe
*New Star of the Year – Male (nominee) – Frank Langella – 1970 Golden Globe
*New Star of the Year – Female (winner) – Carrie Snodgress – 1970 Golden Globe
*Best Actress (nominee) – Carrie Snodgress – 1970 Academy Award
*Best Picture (nominee) – 1970 National Board of Review
*Best Supporting Actor (winner) – Frank Langella – 1970 National Board of Review

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 