The Unknown Lover
{{infobox film
| name           = The Unknown Lover
| image          =
| imagesize      =
| caption        =
| director       = Victor Halperin
| producer       = Victory Pictures(aka Victor Halperin Productions)
| writer         = Victor Halperin(scenario) Frank Mayo Mildred Harris
| music          =
| cinematography =
| editing        =
| distributor    = The Vitagraph Company of America
| released       = October 30, 1925
| runtime        = 7 reels;(6,895 feet)
| country        = United States
| language       = Silent film(English intertitles)
}}
The Unknown Lover is a 1925 silent film drama produced and directed by Victor Halperin (an early effort for him) under his Victory Pictures banner and released by the Vitagraph Company of America, soon to become part of Warner Brothers. This is the last silent film of star Elsie Ferguson. The film is considered lost film|lost.  

==Cast==
*Elsie Ferguson - Elaine Kent Frank Mayo - Kenneth Billings
*Mildred Harris - Gale Norman
*Peggy Kelly - Gladys
*Leslie Austin - Fred Wagner

==References==
 

==External links==
* 
* 
* (Univ. of Wash. Sayre collection)

 

 
 
 
 
 
 
 
 
 


 