Shaitan (film)
 
 
 
{{Infobox film
| name           = Shaitan
| image          = Shaitanfilm.jpg
| caption        = Theatrical release poster
| director       = Bejoy Nambiar
| producer       = Anurag Kashyap Sunil Bohra Guneet Monga Meraj Shaikh
| writer         =
| screenplay     = Megha Ramaswamy
                   Bejoy Nambiar
| story          =
| based on       =  
| starring       = Rajit Kapoor Rajeev Khandelwal Kalki Koechlin Pawan Malhotra Shiv Pandit Gulshan Devaiya Neil Bhoopalam Kirti Kulhari Rukhsaar Rehman Sheetal Menon Sonali Sachdev
| music          = Prashant Pillai Amar Mohile Ranjit Barot Anupam Roy
| cinematography = R. Madhi
| editing        = Sreekar Prasad
| studio         = AKFPL Getaway Films
| distributor    = Viacom 18 Motion Pictures
| released       =  
| runtime        =
| country        = India
| language       = Hindi
| budget         =  
| gross          =  
}}

Shaitan (translation: Devil) is a 2011 Hindi thriller film, directed by Bejoy Nambiar starring Rajeev Khandelwal, Kalki Koechlin, Gulshan Devaiya, Shiv Pandit, Neil Bhoopalam, Kirti Kulhari, Rajit Kapoor, Pawan Malhotra and Rajkummar Rao. The film was released on 10 June 2011. 

==Plot==
Shaitan starts off with Amy (Kalki Koechlin), who is mentally disturbed and deeply affected by the attempted suicide and eventual institutionalisation of her mother. She moves from Los Angeles to Mumbai, where she meets KC (Gulshan Devaiya) at a party her parents forcefully take her to. KC introduces her to his gang – Dash (Shiv Pandit), Zubin (Neil Bhoopalam) and Tanya (Kirti Kulhari). They lead a directionless life, having fun, drinking, using drugs and driving around in a Hummer. On one such occasion, they start racing a random car and win but, in the celebration rush, they run over two people on a scooter killing them instantly.
 Pawan Malhotra) for help. The Police Commissioner assigns an uptight cop Arvind Mathur (Rajeev Khandelwal) to solve the case unofficially as he is suspended after he throws a corporator from the first floor of his own house for allegedly beating up another woman. Inspector Arvind Mathur is shown having a disturbed married life and nearly divorces his wife.

The group of youngsters then hide in a shady hotel, acting upon the plan hatched by Dash. When Tanya is nearly raped by a man, they react violently and kill him. They escape from the lodge and hide out in a cinema, where Amy finds a packet of cocaine in Dashs satchel. Tanya is deeply disturbed by their recent actions, and convinces Zubin to take her home. While Zubin leaves to find a taxi, Tanya gets into an altercation with Amy. Amy, while under the influence of cocaine, is convinced that the phone call that Tanya was making to her sister was actually to the police. In the physical confrontation that follows, KC severely injures Tanya. Zubin, upon returning, finds the gruesome scene and flees. He is later arrested by the police while attempting to leave the city. Dash takes Amy and KC to a church in the outskirts of Mumbai and instructs them to hide there while he tries to figure things out with Malwankar.

The arrested Zubin, in the meantime, confesses the entire scenario to the police. They track down Malwankar, who was attempting to escape with his wife. In the ensuing chase, Malwankar is nearly killed in an auto collision. While buying food, the exasperated KC sees his handicapped sister being traumatised by the media. He then calls his father and asks him for help. Using this call, the police trace the trio to the church. Upon returning to the church, KC tells the other two everything and tries to convince Amy to leave the church with him. Dash, however, confronts him and the two get into a bloody confrontation. It ends with Dash beating KC to death. Dash then tries to get Amy to leave the church with him. However, a cocaine-fuelled Amy has a traumatic flashback involving her mother trying to drown her as a child. In this frenzy, she stabs Dash, who is trying to restrain her. She is then found by Inspector Mathur.

The police, despite knowing the truth, get the three surviving friends to sign a statement saying that Dash had kidnapped all four. This is done to save the reputation of the much-maligned police force. Mathur then meets his wife, indicating a possible reconciliation. The film ends with Amy being resigned to the convent fate that she always feared, albeit with a different name (Saira).

==Cast==
* Rajeev Khandelwal as Inspector Arvind Mathur
* Kalki Koechlin as Amrita Jayshankar a.k.a. Amy
* Shiv Pandit as Dushyant Sahu a.k.a. Dash
* Neil Bhoopalam as Zubin Shroff
* Gulshan Devaiya as Karan Chaudhary a.k.a. KC
* Kirti Kulhari as Tanya Sharma
* Rajit Kapoor as Amys Father
* Pawan Malhotra as Commissioner of Police
* Nikhil Chinapa as Inspector Sandeep
* Rukhsaar Rehman as Tanyas elder sister
* Rajkummar Rao as Inspector Malvankar(Pintya)
* Sheetal Menon as Nandini (Arvinds wife)
* Rajat Barmecha as Shomu (Cameo)
* Imran Rasheed as Rehmat

==Production==
Director Bejoy Nambiar, who assisted Mani Ratnam in Guru (2007 film)|Guru, approached Kalki Koechlin to play a role in the movie. But since the film was not moving for two years, Anurag Kashyap agreed to produce it on a smaller budget. 

==Reception==
The film received positive response from critics in India. Taran Adarsh from Bollywood Hungama gave it 4/5 stars and said "Shaitan is bound to raise eyebrows thanks to its contemporary, thrilling, hard-hitting and forceful content. One of the most ingenious and entertaining thrillers Ive seen in a long time."  Pankaj Sabnani from Glamsham also gave the film 4/5 and said "Shaitan is a spellbinding thriller with awesomeness written all over it."  Nikhat Kazmi gave it 3.5/5 stars and wrote "it turns the camera three-sixty degrees and brings you the grime of a subterranean sub-culture that throbs amidst a certain section of metropolitan maverick twenty-somethings."  Anupama Chopra from NDTV gave 3/5 stars.  Mayank Shekhar from Hindustan Times gave 3/5 stars.  Ankit Ojha of Planet Bollywood praised Shaitan and its thematic element to a large extent, giving the movie 9 stars out of 10, writing, "Nambiar has managed to impress each and everyone of the discerning film viewer and movie buff. This is Hindi cinema finally coming of age! Recommended for more than one watch, as to absorb the totality of the film’s context and subtext! Outstanding stuff!" 

One negative review came from Raja Sen of Rediff.com. He gave it 2/5 stars and wrote "Populated exclusively by the very coolest of character actors, backgrounded by a blaring retro-loving soundtrack, and shot dizzyingly in hypertechnicolor." 

==Awards and Nominations==
{| class="wikitable"
|-
! Award !! Category !! Nominee !! Result
|- 18th Annual Colors Screen Awards Screen Award for Most Promising Debut Director
| Bejoy Nambiar
|  
|- Screen Award Best Background Music
| Ranjit Barot
|  
|-
| Screen Award for Best Cinematography
| R. Madhi
|  
|-
| Screen Award for Best Sound Design
| Kunal Sharma
|  
|- Best Film
| Shaitan
|  
|- Best Director
| Bejoy Nambiar
|  
|- Screen Award Best Actress
| Kalki Koechlin
|  
|-
| Screen Award for Best Actor in a Negative Role (Female)
| Kalki Koechlin
|  
|- Screen Award for Best Ensemble Cast
| Rajeev Khandelwal, Shiv Pandit, Kalki Koechlin, Gulshan Devaiya, Neil Bhoopalam, Kirti Kulhari
|  
|- Screen Award Best Female Playback
| Suman Sridhar
|  
|- Screen Award Best Screenplay
| Bejoy Nambiar and Megha Ramaswamy
|  
|- Screen Award Best Editing
| Sreekar Prasad
|  
|- Screen Award for Best Action
| Javed-Ejaaz
|  
|- Zee Cine Awards 2012 Zee Cine Best Background Music
| Ranjit Barot
|  
|- 7th Apsara Film & Television Producers Guild Awards Apsara Award for Best Screenplay
| Bejoy Nambiar and Megha Ramaswamy
|  
|- Apsara Award for Best Actor in a Supporting Role
| Gulshan Devaiya
|  
|- 57th Idea Filmfare Awards 2011 Filmfare Award for Best Background Score
| Ranjit Barot
| 
|- Filmfare Award Best Male Debut
| Shiv Pandit and Gulshan Devaiya
|  
|- Filmfare Award for Best Debut Director
| Bejoy Nambiar
|  
|- Filmfare Award Best Cinematography
| R. Madhi
|  
|- Stardust Awards 2012 – Searchlight Awards Best Director
| Bejoy Nambiar
|  
|- Stardust Award Best Actress
| Kalki Koechlin
|  
|- Stardust Award Best Actor
| Gulshan Devaiya
|  
|- The Global Indian Film and Television Honours 2012 Best Actor in Supporting Role (Female)
| Kalki Koechlin
|  
|- Most Promising New Director
| Bejoy Nambiar
|  
|- Most Promising New Debutant (Male)
| Gulshan Devaiya
|  
|- Big Star Young Entertainment Awards 2012 Big Star Young Entertainer – Star Director & Movie
| Bejoy Nambiar & Shaitan
|  
|- Big Star Young Entertainer of the Year (Female)
| Kalki Koechlin
|  
|- Big Star Young Entertainer Debut of the Year (Male)
| Shiv Pandit
|  
|}

==Soundtrack==
The music is composed by Prashant Pillai, Amar Mohile, Ranjit Barot, and Anupam Roy. Lyrics are penned by K. S. Krishnan, Sanjeev Sharma, Colin Terence, Abhishek, and Shradha. Ironically, Khoya Khoya Chand (remix song by Mikey McCleary, vocals by Suman Sridhar) is not included in the track lists by the producers. The popular number "Hawa Hawai" from the 1987 film, Mr. India, composed by the duo Laxmikant-Pyarelal has been reused (remix song by Mikey McCleary, vocals by Suman Sridhar) in the film.

===Track listing===
{{track listing
| extra_column = Singer(s)
| music_credits = yes
| title1 = Amys Theme
| extra1 = Suzanne DMello
| music1 = Ranjit Barot
| lyrics1 = Sanjeev Sharma
| length1 = 4:07
| title2 = Fareeda
| extra2 = Suraj Jagan
| music2 = Prashant Pillai
| lyrics2 = Sanjeev Sharma
| length2 = 3:18
| title3 = Josh
| extra3 = Colin Terence, Abhishek, Shradha
| music3 = Amar Mohile
| lyrics3 = Colin Terence, Abhishek, Shradha
| length3 = 4:05
| title4 = Hawa Hawai
| extra4 = Prashant Pillai, Suman Sridhar
| music4 = Laxmikant-Pyarelal
| lyrics4 = Javed Akhtar
| length4 = 4:54
| title5 = Nasha
| extra5 = Prashant Pillai, Bindu Nambiar
| music5 = Prashant Pillai
| lyrics5 = Sanjeev Sharma
| length5 = 3:15
| title6 = O Yaara
| extra6 = Kirti Sagatia, Preeti Pillai
| music6 = Prashant Pillai
| lyrics6 = Sanjeev Sharma
| length6 = 5:07
| title7 = Pintya
| extra7 = Chandan Shive
| music7 = Ranjit Barot
| lyrics7 = Sanjeev Sharma
| length7 = 2:58
| title8 = Zindagi
| extra8 = Ranjit Barot
| music8 = Ranjit Barot
| lyrics8 = Sanjeev Sharma
| length8 = 3:11
| title9 = Bali
| note9 = The Sound of Shaitan
| extra9 = Farhad Bhiwandiwala, K.S. Krishnan, Preeti Pillai, Hitesh Modak, Kalloist
| music9 = Prashant Pillai
| lyrics9 = K. S. Krishnan
| length9 = 3:36
| title10 = Enter
| note10 = Music
| extra10 =
| music10 = Prashant Pillai
| lyrics10 = Sanjeev Sharma
| length10 = 1:20
| title11 = Nasha
| note11 = Rock & Soul Version
| extra11 = Ranjit Barot, Farhad Bhiwandiwala, Bindu Nambiar
| music11 = Prashant Pillai
| lyrics11 = Sanjeev Sharma
| length11 = 4:17
| title12 = Outro
| note12 = Music
| extra12 =
| music12 = Prashant Pillai
| lyrics12 = Sanjeev Sharma
| length12 = 1:36
| title13 = Retro Pop Shit
| note13 = Music
| extra13 =
| music13 =  Anupam Roy
| lyrics13 = Sanjeev Sharma
| length13 = 3:17
| title14 = Unleashed
| note14 = Music
| extra14 =
| music14 = Bhayanak Maut
| lyrics14 =
| length14 = 3:39
}}

==References==
 

==External links==
*  
 
 
 

 
 
 
 
 
 
 
 