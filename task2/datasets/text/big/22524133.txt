Lock out (film)
 

{{Infobox film name            = Lock Out image           = caption         = director        = Antoni Padrós producer        = writer          = starring        = music           = cinematography  = Carles Gusi editing         = distributor     = released        = 1973 runtime         = 127 minutes language  Spanish
|budget          =
|}} Spanish artist and director Antoni Padrós.

The recently restored film is an allegory of political oppression under Francisco Franco|Franco, with a plot alternating between narrative sequences and dream-like moments styled after the Spanish avant garde.  Padros employs a range of musical forms including opera, scores, and choreography    to underscore a plot featuring a group of dropouts living in a junkyard,    who try to re-integrate with "The System" as represented by a party.  Anarchy and absurdism are the stylistic veils covering this harsh criticism of the Franco regime.

==Sources==
*  at Film Society of Lincoln Center
*  at Pragda

==References==
 

==External links==
*  

 
 
 
 

 