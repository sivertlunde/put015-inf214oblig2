Charleston (film)
{{Infobox film
 | name =  Charleston
 | image =  Charleston (film).jpg
 | caption =
 | director = Marcello Fondato
 | writer = Marcello Fondato, Francesco Scardamaglia
 | starring =  Bud Spencer James Coco Herbert Lom
 | music =   Maurizio De Angelis Guido De Angelis
 | cinematography = Mario Montuori
 | editing = Nadia Bonifazi
 | producer = Elio Scardamaglia
 | distributor = Delfo Cinematografica
 | released =  5 March 1977
 | runtime = 105 min
 | awards =
 | country = Italy
 | language = Italian
 | budget =
 }} 1977 Cinema Italian comedy film written and directed by Marcello Fondato. It reprises the style of the film The Sting.  

== Plot summary ==
Joe Lo Monaco is an owner of a vessel which has inside a large casino. A band of evildoers exploits man to have earnings about gambling on roulette, so Lo Monaco really wants to destroy the ship, but he finds out that the boat is assured and that destroying it would lose a lot of money. In London meanwhile the trickster Charleston is arrested for being found with a false identity. However, he manages to escape from prison as soon as he was offered the job of helping Lo Monaco from evildoers ...

== Cast ==
*Bud Spencer: Charleston
*James Coco: Joe lo Monaco
*Herbert Lom: Inspector Watkins
*Jack La Cayenne: Jack Watson / Columbus
*Dino Emanuelli: Bull
*Ronald Lacey: Frankie
*Geoffrey Bayldon: Fred
*Renzo Marignano: Morris
*Lucretia Love: Secretary
* Calvin Levels: Jeff

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 


 
 