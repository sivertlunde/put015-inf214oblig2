Tornado Glory
Tornado 2006 by storm chasers Reed Timmer and Joel Taylor through Tornado Alley during the 2003 storm season. The film was produced and directed by Ken Cole.

==Overview==
As opposed to many documentaries focusing strictly on the science and destructive nature of tornadoes, Tornado Glory examines the determination, excitement, and struggles of storm chasing from the personal experiences of two University of Oklahoma meteorology students, Reed Timmer and Joel Taylor. Timmer in particular has emerged as a controversial figure in the storm chasing world,  favoring high-risk chase tactics such as speeding and driving in reverse to get out of a tornado’s path. Taylor and Timmer’s critics, including notable meteorology professors,  take issue with such tactics, as well as Timmer’s high-profile television appearances.

The documentary blends the personal stories and backgrounds of the two main chasers with chase sequences from their 2003 chase season. Events covered in the movie include October 9, 2001 in Elk City, Oklahoma, April 5, 2003 in Stonewall County, Texas, the May 15, 2003 outbreak, and June 24, 2003 in Manchester, South Dakota, in which Timmer was almost hit by a mile-wide tornado.

According to the DVD commentaries, Taylor, Timmer, and Cole studied meteorology together at the University of Oklahoma, graduating in 2002. Taylor graduated with a bachelor’s degree, while Timmer is currently pursuing a doctorate in meteorology. Cole, who was also cinematographer and editor, graduated magna cum laude with a B.S. in meteorology in 2002. He then studied filmmaking as part of his master’s degree.

==External links==
* 
* 

 
 
 
 
 


 