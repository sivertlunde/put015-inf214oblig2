Mohavum Mukthiyum
{{Infobox film 
| name           = Mohavum Mukthiyum
| image          =
| caption        =
| director       = J. Sasikumar
| producer       = Sasikumar
| writer         = S. L. Puram Sadanandan
| screenplay     = S. L. Puram Sadanandan Lakshmi
| music          = M. K. Arjunan
| cinematography = CJ Mohan
| editing        =
| studio         =
| distributor    =
| released       =  
| country        = India Malayalam
}}
 1977 Cinema Indian Malayalam Malayalam film, Lakshmi in lead roles. The film had musical score by M. K. Arjunan.   

==Cast==
*Prem Nazir
*Sheela
*Adoor Bhasi Lakshmi
*Sreelatha Namboothiri
*Nilambur Balan
*Prathapachandran Meena
*Philomina

==Soundtrack==
The music was composed by M. K. Arjunan and lyrics was written by Sreekumaran Thampi. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Bhagavaan Anuraagavasantham || Vani Jairam, B Vasantha || Sreekumaran Thampi || 
|-
| 2 || Chumbana Varna || K. J. Yesudas || Sreekumaran Thampi || 
|-
| 3 || Kale Ninne Kandappol || Sreelatha Namboothiri, Zero Babu || Sreekumaran Thampi || 
|-
| 4 || Maravithan Thirakalil || K. J. Yesudas || Sreekumaran Thampi || 
|}

==References==
 

==External links==
*  

 
 
 

 