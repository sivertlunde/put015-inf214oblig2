American Threnody
{{multiple issues|
 
 
}}

{{Infobox film| name           = American Threnody
| director       = Robert Rex Jackson
| runtime        = 105 minutes
| country        = United States
}}
 

American Threnody is a 2007 American documentary film, directed by Robert Rex Jackson. It concerns the Maxey Flat Low Level Radioactive Waste facility in eastern Kentucky. The facility was built on the former site of the farm where the filmmakers grandfather was born. The film examines the impact of the facility on the community and examines the persistent containment problems that have been the subject of media coverage. Current methods of storing and disposing of transuranic isotopes and how they differ from the techniques used at Maxey Flat are also explored.

==External links==
 
* 
* 

 
 
 
 
 
 
 
 

 