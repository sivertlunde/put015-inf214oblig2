Murphy of Anzac
 
{{Infobox film
  | name     = Murphy of Anzac
  | image    = 
  | caption  = 
  | director = J. E. Mathews
  | producer = 
  | writer   = 
  | based on = 
  | starring = Martyn Keith
  | music    = 
  | cinematography = 
  | editing  = 
| studio = Fraser Film Release and Photographic Company
  | distributor = 
  | released = 24 April 1916 
  | runtime  = 4,000 feet 
  | language = Silent film English intertitles
  | country = Australia
  | budget   = 
  }}

Murphy of Anzac is a 1916 Australian silent film directed by J. E. Mathews. It tells the story of John Simpson Kirkpatrick during the Gallipoli Campaign in World War I.  It is considered a lost film. Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, p 62 

==Plot==
John Simpson Kirkpatrick, aka "Murphy", an English emigrant to Queensland, enlists in the army in World War I and is attached to the ambulance corps. On the voyage over he discovers a traitor (Martyn Keith) giving information to the enemy by wireless and overcomes him. The Australian troops land at Gallipoli and Murphy brings the wounded back from the trenches on his donkey. He is killed by a Turkish shell while rescuing his 104th man.  

A highlight of the film was the German spy being thrown off a cliff 50 foot into the water. 

==Production==
The cast was largely returned servicemen.  The technical adviser was Gallipoli veteran, Corporal Robson, who had known Simpson, and also appeared in the film.   

==Reception==
The film ran for six weeks in Sydney and nine weeks in Melbourne. 

==Historical Accuracy==
The film was criticised at the time by Gallipoli veterans for a number of historical errors including showing:
*Murphy being allotted to the AMC when he was with the Ninth Division;
*Murphys mother in Australia, when she never left England;
*Murphy killed by a shell when he was shot through the heart by a sniper near Shrapnel Gully;
*Murphy receiving a white feather, when no such thing happened. 

==References==
 

==External links==
*  
*  at National Film and Sound Archive

 
 
 
 
 
 


 