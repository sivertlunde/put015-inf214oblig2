Muddina Maava
 
{{Infobox film
| name           = Muddina Maava
| image          =
| caption        =
| director       = Om Sai Prakash
| producer       = K. Prabhakar
| writer         = Shruti S.P.Balasubrahmanyam Tara Dwarakish Doddanna
| music          = S.P.Balasubrahmanyam
| cinematography =
| editing        =
| distributor    = Shreedevi Combines
| released       = 1993
| runtime        = 145 min
| country        = India
| language       = Kannada
| Budget         =
| preceded_by    =
| followed_by    =
| awards         =
| Gross          =
}}
Muddina Maava (English Translation: Beloved father-in-law) is a Kannada language feature film released in the year 1993.starring Shashi Kumar, Shruti (actress)|Shruti, S.P.Balasubrahmanyam, Tara (Kannada actress)|Tara. The film is a remake of Telugu film Mamagaru (1991) starring Dasari Narayana Rao.

==Cast==
* Shashi Kumar Shruti
* S.P. Balasubrahmanyam Tara
* Doddanna
* Dwarakish
* Girija Lokesh

==Soundtrack==
The films soundtrack was composed by S. P. Balasubrahmanyam|S.P. Balasubrahmanyam.
{| class="wikitable"
|- style="background:#ff9; text-align:center;"
! Track# !! Song !! Singer(s) !! Duration
|-
| 1 || "Aaradhane" || S. P. Balasubrahmanyam|S.P. Balasubrahmanyam, K. S. Chithra|K.S. Chithra ||
|-
| 2 || "Deepavali Deepavali" || Rajkumar (actor)|Dr. Rajkumar, S.P. Balasubrahmanyam ||
|-
| 3 || "Premakke" || Manjula Gururaj ||
|-
| 4 || "Shivane" || Dr. Rajkumar ||
|-
| 5 || "Varane Maduve Maduve" || S.P. Balasubrahmanyam, K.S. Chithra ||
|}

== Trivia ==
"Deepavali Deepavali" song in the movie was picturized on S.P.Balasubrahmanyam and was sung by Rajkumar (actor)|Rajkumar. This was a rare occasion where an actor sang for a singer, probably unmatched in the world of cinema.
 

 

== References ==
 

 
 
 


 