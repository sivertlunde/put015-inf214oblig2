Little Longnose
{{Infobox film
| name = Little Longnose
| image = Littlelongnose.jpg
| caption = Original poster
| imdb_rating =
| director = Ilya Maksimov
| producer = Sergey Selyanov
| writer = Wilhelm Hauff (story)
| starring = Albert Asadullin Yelena Shulman Yevgeniya Igumnova Natalya Danilova Igor Shibanov
| music = Valentin Vasenkov
| cinematography =
| editing =
| distributor = 2003 (Russia)
| runtime = 79 min
| country = Russia Russian
| budget =
| preceded by =
| followed by =
| mpaa_rating =
| tv_rating =
}} traditionally animated feature film directed by Ilya Maksimov, made by Melnitsa Animation Studio. It opened in Russia on March 20, 2003, and had 375,000 admissions during its theatrical run. A computer game based on the film was produced by Melnitsa, K-D Labs, and 1C (Russian software company)|1C.

==Plot==
The film is a loose adaptation of the fairy tale "Zwergnase" (German for "The Dwarf   Nose") written by Wilhelm Hauff. 

Jacob, the son of a shoemaker, refuses to do a favour for an evil witch. In retaliation, she takes away seven years of his life and turns him into an ugly dwarf. At the same time the Kings daughter Princess Greta is turned into a goose by the same witch after she discovers her in her fathers library stealing a spell which will help her rule the kingdom. Jacob and Greta meet and plan to overthrow the witch and get back to their families and true forms.

==See also==
*History of Russian animation
*List of animated feature films
*Konstantin Bronzit

==External links==
* 
* 
* 

 
 
 
 
 

 