Little Shop of Horrors (film)
 Little Shop of Horrors}}
 
{{Infobox film
| name = Little Shop of Horrors
| image = Little shop of horrors.jpg
| image_size = 220px
| border = yes
| alt =
| caption = Theatrical release poster
| director = Frank Oz
| producer = David Geffen
| screenplay = Howard Ashman
| based on =     Stanley Jones
| starring = Rick Moranis Ellen Greene Vincent Gardenia Steve Martin Levi Stubbs
| music = Miles Goodman
| cinematography = Robert Paynter
| editing = John Jympson The Geffen Company
| distributor = Warner Bros.
| released =  
| runtime = 94 minutes       103 minutes    
| country = United States
| language = English
| budget = $25 million
| gross = $38,748,395
}} rock musical horror  musical comedy of the The Geffen Company and released by Warner Bros. on December 19, 1986.
 Albert R. Broccoli 007 Stage at the Pinewood Studios in England, where a "downtown" set, complete with overhead train track, was constructed. The film was produced on a budget of $25 million, in contrast to the original 1960 film, which, according to Corman, only cost $30,000.    The films original 23-minute finale, based on the musicals ending, was rewritten and reshot after receiving a strong negative reception from test audiences. Before it was fully restored in 2012 by Warner Home Video, the original ending was never available publicly other than in the form of black-and-white workprint footage.

==Plot==
In September 1962, Seymour Krelborn (Rick Moranis) and his colleague, Audrey (Ellen Greene), work at Mushniks Flower Shop, lamenting they cannot escape the slums of New York City. Struggling from a lack of customers, Mr. Mushnik (Vincent Gardenia) prompts to close the store, only for Audrey to suggest displaying an unusual plant Seymour owns. Immediately attracting a customer, Seymour explains he bought the plant, which he dubbed "Audrey II", from a Chinese flower shop during a solar eclipse. Attracting business to Mushniks shop, the plant soon starts dying, worrying Seymour. Accidentally pricking his finger, he then discovers Audrey II needs human blood to thrive.
 sadistic dentist boyfriend Orin Scrivello (Steve Martin). Despite this, Audrey is interested in Seymour, as well as dreams of marrying him and escaping Skid Row. After Seymour closes up shop, Audrey II (Levi Stubbs) begins to talk to Seymour, demanding more blood than Seymour can give. The plant proposes Seymour murder someone in exchange for fame and fortune: Seymour initially refuses, but agrees upon witnessing Orin slapping Audrey.
 abuses nitrous oxide, puts on a type of venturi mask to receive a constant flow of the gas. Accidentally breaking an intake valve and unable to remove the mask, Orin begs Seymour for help removing it as Seymour just stands there. When Orin asks Seymour what he ever did to him, Seymour replies, "Nothing, its what you did to her." Orin dies from asphyxiation and Seymour drags his body back to Audrey II. While dismembering the body for the plant, Seymour is unknowingly spotted by Mushnik, who flees in fear. 

After feeding Orins parts to Audrey II, Seymour discovers the police investigating Orins disappearance. Audrey, feeling guilty about wishing Orin would disappear, is comforted by Seymour and the two admit their feelings for each other. That night, Mushnik confronts Seymour, believing he murdered Orin. Threatening to turn Seymour in, Mushnik offers to let him escape in exchange for the secret to the plants care routine. Out of options, Seymour causes Mushnik to back into Audrey IIs open mouth, who then devours Mushnik.
 James Belushi), Seymour is offered a contract to breed Audrey II and sell the saplings worldwide. Seymour then realizes he must destroy Audrey II and the plants plans for planetary domination.
 alien from outer space. Trapping Seymour, Audrey II collapses the store, attempting to kill him. Seymour, trapped under debris, grabs an exposed electrical cable and electrocutes Audrey II, causing it to explode. Leaving the destroyed shop, Seymour safely reunites with Audrey. The two wed and move to the suburbs: arriving at their new home, a smiling Audrey II bud can be seen among the flowers in their front yard.

===Original ending===
During production, director Oz shot a 23-minute ending based on the off-Broadway musicals ending. However, after receiving negative reviews from test audiences, the ending had to be rewritten and re-shot for the theatrical release with a "happier ending".

In the original ending, after Audrey is attacked by Audrey II, Seymour rescues Audrey, who is seriously injured. Confessing to Audrey he fed Mushnik and Orin to Audrey II, Audrey requests Seymour feed her to the plant and earn the success he deserves before she dies in his arms. Seymour does so, but soon attempts to commit suicide only to be stopped by Patrick Martin (Paul Dooley). Patrick offers to reproduce and sell Audrey II as he had grown a smaller Audrey II from one of the clippings that he harvested as the smaller Audrey II smiles at him. He also warns Seymour that his consent isnt necessary as plants are considered public domain. Realizing Audrey IIs plans for global domination, Seymour climbs down the roof with the resolution to destroy the plant. Returning to the shop, Seymour confronts and tries to kill Audrey II, who tears down the shop, plucks Seymour out of the rubble and eats him alive. Audrey II then spits out Seymours glasses and laughs.

The three chorus girls appear in front of a large American flag and tell how although Audrey II buds became a worldwide consumer craze, the buds grew into an army of monstrous plants who begin to take over the Earth.    Giant Audrey II plants are shown destroying cities, toppling buildings, as well as eating people. The final shot shows the United States Army|U.S. Army as it attempts to fight the buds as they ascend the Statue of Liberty. An Audrey II then breaks the fourth wall to eat the audience as the camera zooms into its mouth.

==Cast== florist who loves "strange and interesting" plants. He means well but becomes easy-influenced when the plant, Audrey II, tricks him into feeding it humans.
* Ellen Greene as Audrey , a sweet, quiet, ditsy and insecure coworker who is the object of Seymours affections, but dating the sadistic Orin Scrivello.
* Vincent Gardenia as Mr.  Mushnik, the grouchy, penny-pinching owner of Mushniks Flower Shop. sadistic and nitrous oxide-huffing dentist, as well as Audreys boyfriend. extraterrestrial plant take over the planet. Tisha Campbell as The Crystals|Crystal, The Ronettes|Ronette, and The Chiffons|Chiffon, the three dropout schoolgirls who act as a Greek chorus throughout the film. James Belushi as Patrick Martin, a Licensing and Marketing executive from World Botanical Enterprises who offers Seymour a proposal to sell Audrey IIs worldwide. Belushi appears in the theatrical release after re-shoots, as actor Paul Dooley (who played Martin in the original ending) was unavailable to reprise his scenes. DJ (the only one) who enjoys putting on a radio show about "weird stuff" called, "Wink Wilkinsons Weird World"
* Christopher Guest as The First Customer, the first customer to enter the flower shop and notice Audrey II. masochist man who goes to Orin for "a long, slow root canal."
* Miriam Margolyes as a Dental Nurse, Orins sarcastic Nursing|nurse/secretary who Orin appears to enjoy harming frequently. Stanley Jones as the Narrator, whose voice is heard reading the opening words.
* Mak Wilson, Danny John-Jules, Danny Cunningham, Gary Palmer, and Paul Swaby as the doo-wop backup singers.

Jim Hensons daughter Heather Henson cameos as one of Orins patients.
 John Alexander, David Barclay, Mike Quinn, Paul Springer, William Todd-Jones, Ian Tregonnian, Robert Tygner, and Mak Wilson.

==Musical numbers==
# "Prologue: Little Shop of Horrors"– Chiffon, Ronette, Crystal
# "Skid Row (Downtown)"– Seymour, Audrey, Mushnik, Chiffon, Ronette, Crystal, Company
# "Da-Doo"– Seymour, Chiffon, Ronette, Crystal
# "Grow for Me"– Seymour, Chiffon, Ronette, Crystal (off-screen)
# "Somewhere Thats Green"– Audrey
# "Some Fun Now"– Chiffon, Ronette, Crystal
# "Dentist!"– Orin, Chiffon, Ronette, Crystal
# "Feed Me (Git It)"– Audrey II, Seymour
# "Suddenly, Seymour"– Seymour, Audrey, Chiffon, Ronette, Crystal
# "Suppertime"– Audrey II, Chiffon, Ronette, Crystal
# "The Meek Shall Inherit"– Chiffon, Ronette, Crystal, Company
# "Suppertime (Reprise)"– Audrey II, Audrey, Chiffon, Ronette and Crystal (off screen)
# "Suddenly, Seymour (Reprise)"– Audrey, Seymour
# "Mean Green Mother from Outer Space"– Audrey II, the Pods
# "Little Shop of Horrors medley" (end credits)– Company

;Original ending
# "Somewhere Thats Green (Reprise)"– Audrey, Seymour
# "Mean Green Mother from Outer Space"– Audrey II, the Pods
# "Finale (Dont Feed the Plants)"– Chiffon, Ronette, Crystal, Company

"Bad" 2003 Broadway Revival Cast Album. From the dialogue included on the recording, the track seems to have been an early version of the climax, performed solely by Audrey II, and shares many similarities to the song "Mean Green Mother from Outer Space".  

==Production==
===Development=== executive produce the film and Martin Scorsese to direct. Scorsese wanted to shoot the film in 3-D film|3-D, but plans fell through and Scorseses first 3-D film would be Hugo (film)|Hugo 25 years later.   John Landis was also approached to direct.

Geffen then offered the film to Frank Oz, who was finishing work on The Muppets Take Manhattan around the same time. Oz initially rejected it, but he later had an idea that got him into the cinematic aspect of the project, which he did not figure out before. Oz spent a month and a half to restructure the script which he felt was stage-bound. Geffen and Ashman liked what he had written and decided to go with what he did. Oz was also studying the Off-Broadway show and how it was thematically constructed, all in order to reconstruct it for a feature film.   

The film differs only slightly from the stage play. The title song is expanded to include an additional verse to allow for more opening credits.  The song "Ya Never Know" was re-written into a Calypso music|calypso-style song called "Some Fun Now", although some of the lyrics were retained.   Four other songs ("Closed for Renovation", "Mushnik and Son", "Now (Its Just the Gas)", as well as "Call Back in the Morning") were cut from the original production score. An original song written by Ashman and Menken, "Mean Green Mother from Outer Space", was created for the film.

===Casting===
Greene was not the first choice for the role of Audrey. Geffen wanted a star role for the film. The studio wanted Cyndi Lauper, who turned it down. Barbra Streisand was also rumored to have been offered the part. Since Greene was the original off-Broadway Audrey, the role was given to her. "Shes amazing", Oz said. "I couldnt imagine any other Audrey, really. She nailed that part for years off-Broadway."  The character of the masochistic dental patient, Arthur Denton, played in the original film by Jack Nicholson and cut from the stage version, was added back to the story and played by Bill Murray, who improvised all of his dialogue. It supposedly took Steve Martin six weeks to film all his scenes as Orin. He contributed ideas such as socking the nurse in the face (originally he was to knock her out using his gas mask) and ripping off the doll head.

===Filming=== My Blue Heaven and L.A. Story. 

As mentioned, additional sequences and songs from the original off-Broadway show were dropped or re-written in order for the feature version to be paced well. The notable change was for the "Meek Shall Inherit" sequence. As originally filmed, it detailed through a dream sequence Seymours rising success and the need to keep the plant fed and impress Audrey. In the final cut, the dream sequence and much of the song is cut out. Oz said, "I cut that because I felt it just didnt work and that was before the first preview in San Jose. It was the right choice, it didn’t really add value to the entire cut."  The full version of the song was included on the films soundtrack album, as were the songs from the original ending. The sequence was deemed to be lost until in 2012, it was rediscovered on a VHS workprint that contained alternate and extended takes and sequences. 

===Operating the plant===
The films version of Audrey II was an extremely elaborate creation, using puppets designed by Lyle Conway.

While developing the mouth of the plant for the dialogue scenes and musical numbers, Oz, Conway and his crew were struggling to figure out how to make the plant move convincingly. "We kept trying and trying and it didnt work."  The solution presented itself while reviewing test footage of the puppet. When the film was run backwards or forward at a faster than normal speed, the footage looked much more convincing and lifelike.  They realized they could film the puppet at a slower speed, making it appear to move faster when played back at normal speed. "By slowing it down it looked it was talking real fast. We then went holy cow, look at that. We can do it."  The frame rate for filming the plant was slowed to 12 or 16 frames per second, depending on the scene, and frequent screen cuts were used to minimize the amount of screen time the puppet spent with human actors, and when interaction was totally necessary, the actors (usually Moranis) would pantomime and lip sync in slow motion.  The film was then sped up to the normal 24 frames per second and voices were reinserted in post-production. Levi Stubbs recordings were run through a harmonizer when slowed down so that they were coherent for Moranis or Ellen Greene.

There are no  .  , Reelviews.net, 1999. 

===The finale===
  Flash Gordon and Brazil (1985 film)|Brazil. "It was all model stuff, that was the brilliant thing. He created the bridge, the buildings, several Audrey IIs and created all of it, all on tabletop. Its all old-fashioned, tabletop animation"   .

Reportedly the entire, planned climax cost about $5 million to produce.   Oz said in an interview, "this was, I think, the most expensive film Warner Bros. had done at the time."  As the film was nearing completion, the excited studio set up a test screening in San Jose. Oz said, "For every musical number, there was applause, they loved it, it was just fantastic...  until Rick and Ellen died, and then the theatre became a refrigerator, an ice box. It was awful and the cards were just awful. You have to have a 55 percent "recommend" to really be released and we got a 13. It was a complete disaster." Oz insisted on setting another test screening in L.A. to see if they would get a different reaction. Geffen agreed to this, but they received the same negative reaction as before.  Oz later recounted, "I learned a lesson: in a stage play, you kill the leads and they come out for a bow — in a movie, they dont come out for a bow, theyre dead. They’re gone and so the audience lost the people they loved, as opposed to the theater audience where they knew the two people who played Audrey and Seymour were still alive. They loved those people, and they hated us for it."   
 James Belushi Tisha Campbell was unavailable for the final appearance of the chorus girls in the yard and was replaced with a lookalike seen only from the waist down. 

"We had to do it," Oz recounted. "-We had to- do it in such a manner that the audience would enjoy the movie. It was very dissatisfying for both of us that we couldnt do what we wanted. So creatively, no, it didnt satisfy us and being true to the story. But we also understood the realities that they couldnt release the movie if we had that ending."  "We had to cut -the work-print- apart, and we never made a dupe of -the original ending-." At the time, the only copies of it that were made to be viewed were VHS work-print tapes given to few crew members.    The scene in which Seymour proposes to Audrey originally contained the reprise of "Suddenly, Seymour". This scene was re-shot and the reprise was placed later in the new ending. Frank Oz DVD commentary, Little Shop of Horrors (2000).  In the final theatrical cut, the only miniatures that are retained are the New York City streets passing behind Steve Martins motorcycle ride at the beginning of "Dentist!"  "When we did re-shoot the ending, the crowd reaction went over 50 percent in our favor. Before it was a point where they hated it so much, Warner probably wouldnt even release the movie," Oz said. 

==Release==
===Box office===
Little Shop of Horrors, after a delay needed to complete the revised ending, was released on December 19, 1986 and was anticipated to do strong business over the 1986 holiday season.  The film grossed $38 million at the box office,  which, from the view point of the studio, was considered an underperformer. However, it became a smash hit upon its home video release in 1987 on VHS and Beta.

===Critical reception===
The film earned a very positive critical reception.  , which uses an average of critics reviews, the film has an 81% rating based on 15 reviews, indicating "universal acclaim" (14 positive reviews, 1 mixed, and no negative).  Richard Corliss of Time Magazine said, "You can try not liking this adaptation of the Off-Broadway musical hit -- it has no polish and a pushy way with a gag -- but the movie sneaks up on you, about as subtly as Audrey II." 

In The New York Times, Janet Maslin called it "a full-blown movie musical, and quite a winning one."  "All of the wonders of "Little Shop of Horrors" are accomplished with an offhand, casual charm. This is the kind of movie that cults are made of, and after "Little Shop" finishes its first run, I wouldnt be at all surprised to see it develop as one of those movies that fans want to include in their lives," said Roger Ebert in his review.  Ozs friend and Muppet colleague, Jim Henson, praised the film and said "the lip sync on the plant in that film is just absolutely amazing." 

;American Film Institute lists
*AFIs 100 Years...100 Songs:
**"Somewhere Thats Green" – Nominated
*AFIs 100 Years of Musicals – Nominated

===Accolades===
  Academy Awards, Best Visual Best Original Best Motion Best Original The Mission.

===Home media=== recalled for content.  In 1998, Warner Bros. released a special edition DVD that contained approximately 23 minutes of unfinished footage from Ozs original ending, although it was in black and white and was missing some sound, visual, and special effects.  Producer and rights owner David Geffen was not aware of this release until it made it to the stores. Geffen said, "They put out a black-and-white, un-scored, un-dubbed video copy of the original ending that looked like shit." As a result the studio removed it from shelves in a matter of days and replaced it with a second edition that did not contain the extra material. Geffen wanted to re-release the film to theaters with the original ending intact.  Geffen also claimed to have a color copy of the original ending, while the studio had lower quality, black and white duplicates as their own color print was destroyed in a studio fire years earlier. But Geffen had not known, until after the DVD was pulled, that the studio did not know there was no colored copy of the original ending in existence. 
 Museum of DVD on Richard III and Heavens Gate (film)|Heavens Gate. 

==References==
 

==External links==
 
 
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 