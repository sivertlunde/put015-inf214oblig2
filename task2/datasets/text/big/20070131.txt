No Exit (1962 film)
{{Infobox film
| name           = No Exit
| image          = No exit 1962 film poster.jpg
| image size     =
| caption        = Theatrical poster
| director       = Tad Danielewski
| producer       = Fernando Ayala Héctor Olivera
| writer         = Jean-Paul Sartre (play) George Tabori
| narrator       =
| starring       = Morgan Sterne Viveca Lindfors Rita Gam
| music          = Vladimir Ussachevsky
| cinematography = Ricardo Younis
| editing        = Jacques Bart Carl Lerner Atilio Rinaldi
| distributor    = Zenith International Films
| released       =  
| runtime        = 85 minutes
| country        = United States Argentina
| language       = English
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}
No Exit, also known as Sinners Go to Hell,  is a 1962 film adaptation of Jean-Paul Sartres play No Exit directed by Tad Danielewski. The film stars Morgan Sterne (1926-2011), Viveca Lindfors, and Rita Gam.

==Plot==
The Valet (Manuel Rosón) enters a hotel room with Joseph Garcin (Morgan Sterne) in tow. The windowless room has a single entrance and no mirrors. Two women, Inès Serrano (Viveca Lindfors) and Estelle Rigault (Rita Gam), are then led in; afterwards, the Valet leaves and locks the door. Realizing that they are in hell, the trio expects to be tortured; however, no torturer is forthcoming. While waiting, they strike up a conversation and discuss each others sins, desires, and unpleasant memories; they slowly realize that such probing is the form of torture they are meant to receive.

It later becomes apparent that Joseph, once a journalist, was executed for cowardice and the betrayal of the French Resistance. Estelle, who has a voracious sexual appetite, was a gold digger and seductress who killed a man. Meanwhile, the lesbian Inès abused her partners love for her and eventually killed them both in a murder-suicide. As the story progresses, Garcin becomes increasingly annoyed by Inès considering him a coward, while Estelle makes unreciprocated advances on him; Inès is tempted by Estelle, but crazed by Estelles heterosexuality.

The three at first continue to see events happening on Earth, but eventually, as the living move on, they are left with only their own thoughts and the company of the other two. Towards the end of the film, Garcin demands he be let out; in response, the door opens. However, none leave. They resign themselves to their fate.

==Production==
Sartre did not know about the film while it was in production.  Viveca Lindfors, at the time married to screenwriter George Tabori, was chosen to play Inès. 

==Style==
The film uses dialogue-free flashbacks when the main characters talk about their lives.  Alison Darren in the Lesbian Film Guide notes that Inès is a "typical screen lesbian" of the early 1960s, both physically attractive and inherently evil. 

==Release and reception==
After being screened at the 12th Berlin International Film Festival in June 1962, where Rita Gam and Viveca Lindfors shared the Silver Bear for Best Actress award,  No Exit received its American release on December 5. 

Bosley Crowther, writing for the New York Times, found the film "antiseptic", with emotionless acting and stagy directing; he summarized that the film "prove  that "No Exit" is inappropriate material for a full-length  ".  Darren calls the film an "excellent psychological drama" with a "surprisingly overt" depiction of lesbianism. 

==References==
;Footnotes
 

;Bibliography
 
*  |work=berlinale.de}}
* 
*  |work=The New York Times}}
* 
* 
*  |work=Internet Movie Database}}
 

==External links==
* 

 
 
 
 
 
 
 
 