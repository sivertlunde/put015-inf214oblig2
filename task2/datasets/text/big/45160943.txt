The Fighting Chance
{{Infobox film
| name           = The Fighting Chance
| image          = 
| alt            = 
| caption        =
| director       = Charles Maigne
| producer       = Jesse L. Lasky
| screenplay     = Will M. Ritchey 
| based on       = The Fighting Chance by Robert W. Chambers
| starring       = Anna Q. Nilsson Conrad Nagel Clarence Burton Dorothy Davenport Herbert Prior Ruth Helms
| music          = 
| cinematography = Faxon M. Dean 	
| editor         = 
| studio         = Artcraft Pictures Corporation Famous Players-Lasky Corporation
| distributor    = Paramount Pictures
| released       =  
| runtime        = 60 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 drama silent film directed by Charles Maigne and written by Will M. Ritchey. The film stars Anna Q. Nilsson, Conrad Nagel, Clarence Burton, Dorothy Davenport, Herbert Prior and Ruth Helms. It is based on the novel The Fighting Chance by Robert W. Chambers. The film was released on August 1, 1920, by Paramount Pictures.  

==Cast==
*Anna Q. Nilsson as Sylvia Landis
*Conrad Nagel as Stephen Siward
*Clarence Burton as	Leroy Mortimer
*Dorothy Davenport as Leila Mortimer
*Herbert Prior as Kemp Farrell 
*Ruth Helms as Grace Farrell
*Bertram Grassby as	Howard Quarrier
*Maude Wayne as Lydia Vyse
*Fred R. Stanton as Beverly Plank 
*William H. Brown as Maj. Bellweather

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 

 