Her Right to Live
{{Infobox film
| name           = Her Right to Live
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Paul Scardon
| producer       = 
| writer         = Paul West
| starring       = Peggy Hyland Antonio Moreno Mae Costello John S. Robertson Julia Swayne Gordon
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Vitagraph Studios
| released       =  
| runtime        = 5 reels (approx. 50 minutes)
| country        = United States English intertitles
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}} 1917 dramatic silent film released by the Vitagraph Studios.

==Plot==
Polly Biggs (Peggy Hyland) is the eldest of a family of orphaned children who are taken in by their uncle, Mayor Hoadley (John S. Robertson). Hoadley despises the children and has only taken them in as good publicity for the upcoming election. His wife, Mrs. Hoadley (Julia Swayne Gordon) is equally cruel to the children, especially Polly.   

One day, Polly Biggs takes the children fishing and meets a young man named John Oxmore (Antonio Moreno), who is the son of the opposing mayoral candidate. When she returns home, Polly discovers that her uncle intends to send all the children to the poorhouse as soon as the election is over. Polly plans to take revenge on her uncle and immediately takes the children to the poorhouse herself, rather than let her uncle do so. Mayor Hoadley, frightened that voters may be incensed to learn that his nieces and nephews are living as orphans in a squalid poorhouse, goes to retrieve them. When Polly sees his car arriving at the poorhouse, she and the children flee. They find an unoccupied cabin in the woods where the brood of youngsters settle in. Unbeknownst to Polly, the cabin is owned by John Oxmore, the young man she met earlier. After Oxmore finds them at his cabin, he grants Polly permission to keep the children there. However, the next day he is accused of a murder committed by Mayor Hoadley. Although John was at the cabin at the time the murder was committed but he says nothing, in hopes of sparing Polly and the children. After John is arrested and Polly discovers his fate, she rushes to the courthouse and announces that John couldnt have committed the crime because he was with her and the children when it occurred. John is released and the cruel Mayor Hoadley is arrested and convicted of the crime.

==Cast==
* Peggy Hyland - Polly Biggs 
* Antonio Moreno - John Oxmore 
* Mae Costello - Mrs. Biggs 
* Bobby Connelly - Jimmy Biggs 
* Helen Connelly - Janet Biggs 
* Mildred Platz - Alice Biggs 
* John S. Robertson - Daniel Hoadley  Jack Ellis - Hawkins 
* Julia Swayne Gordon - Mrs. Hoadley 

==References==
 

==External links==
*  
*   

 
 
 
 
 
 
 
 
 