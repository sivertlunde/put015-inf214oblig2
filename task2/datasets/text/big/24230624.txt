Waati
 
{{Infobox film
| name           = Waati
| image          = 
| image_size     = 
| caption        = 
| director       = Souleymane Cissé
| producer       = Xavier Castano
| writer         = Souleymane Cissé
| narrator       = 
| starring       = Sidi Yaya Cissé
| music          = 
| cinematography = Jean-Jacques Bouhon Vincenzo Marano Georgi Rerberg
| editing        = 
| distributor    = 
| released       = May 1995
| runtime        = 140 minutes
| country        = Mali
| language       = Bambara
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

Waati is a 1995 Malian drama film directed by Souleymane Cissé. It was entered into the 1995 Cannes Film Festival.   

==Cast==
* Sidi Yaya Cissé - Solofa
* Mariame Amerou Mohamed Dicko - Nandi at 6 years
* Balla Moussa Keita - Teacher
* Vusi Kunene
* Martin Le Maitre
* Eric Miyeni - The father
* Nakedi Ribane - The mother
* Adam Rose - Killer Policeman
* Niamanto Sanogo - Rastas prophet
* Linèo Tsolo - Nandi
* Mary Twala - Grandmother

==References==
 

==External links==
* 

 
 
 
 
 


 
 