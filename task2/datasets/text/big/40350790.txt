The White Panther
{{Infobox film
| name           = The White Panther
| image          = 
| image_size     = 
| caption        = 
| director       = Alan James 
| producer       = Phil Goldstone
| writer         = 
| based on      = 
| narrator       = 
| starring       = Snowy Baker
| music          = 
| cinematography = 
| editing        = 
| studio         = Phil Goldstone Productions
| released       = January 1924 This Weeks Photoplays
New York Times (1923-Current file)   20 Jan 1924: X5. 
| runtime        = 
| country        = USA
| language       = silent
| budget         = 
}}
The White Panther is a 1924 American film set in India starring Australian actor Snowy Baker about the love affair between Major Wainwright, an English officer, and the governors daughter.   It features an early appearance by Boris Karloff. Stephen Jacobs, Boris Karloff: More Than A Monster Tomohawk Press 2011 p 68 

==Plot==
Irene Falliday is the daughter of the British governor who rules over an Indian province. She loses a shawl and finds Yasmini wearing it and asks Tommy Farrell to get it back. Farrell is in love with Irene, but Yasmini loves him, and when he finally gets the shawl from her, the circumstances shame her in the eyes of her father Shere Ali, Sirdar of the Afghans, and his subjects. In revenge, Shere Ali kidnaps Irene.

Irenes long time secret admirer, Major Bruce Wainright, goes to rescue her with the aid of his horse, "The White Panther."

==Cast==
*Snowy Baker as Major Bruce Wainwright
*Gertrude McConnell as Irene Falliday
*Lois Scott as Yasmini
*Frank Whitson as Shere Alie Phil Burke as Tommy Farrell William Bainbridge as British governor
*Boomerang as The White Panther
*Boris Karloff as "a native" 

==References==
 

==External links==
* 
*  at TCMDB

 
 
 
 
 


 