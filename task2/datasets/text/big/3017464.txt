Heart Like a Wheel (film)
{{Infobox film
| name           = Heart Like a Wheel
| image          = Heart Like a Wheel (film) poster.jpg
| caption        =
| director       = Jonathan Kaplan
| producer       = Lamar Card Marty Katz Charles Roven Arne Schmidt
| writer         = Ken Friedman David E. Peckinpah
| starring       = Bonnie Bedelia Beau Bridges
| music          = Laurence Rosenthal
| cinematography = Tak Fujimoto
| editing        = O. Nicholas Brown
| distributor    = 20th Century Fox
| released       =  
| runtime        = 113 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $272,278
}}

Heart Like a Wheel is a 1983 biographical film directed by Jonathan Kaplan and based on the life of drag racing driver Shirley Muldowney. It stars Bonnie Bedelia as Muldowney, and Beau Bridges as drag racing legend Connie Kalitta.
 William Theiss for an Academy Award for Costume Design.

==Plot summary==
In 1956 Schenectady, New York, waitress Shirley Roque (Bonnie Bedelia) marries auto mechanic Jack Muldowney (Leo Rossi) over the mild objections of her singer father Tex (Hoyt Axton), who wants her to be able to take care of herself, rather than having to rely on a husband. Jack buys a gas station, Shirley becomes a housewife, and they have a son.

For fun, Jack races his sportscar against others on deserted stretches of road late at night. One time, Shirley talks him into letting her drive. She wins and continues winning. A chance encounter with professional driver "Big Daddy" Don Garlits (Bill McKinney) gives her the idea to look for sponsorship from one of the major car manufacturers, despite her husbands skepticism. This being the 1950s, a pretty housewife is not taken seriously, especially since there are no women pro drivers at all. But when she returns home, Jack tells her that he can build her a drag racing|dragster. 

In 1966, she is ready. She still needs to get three signatures before she can get her National Hot Rod Association (NHRA) license, nearly impossible in the macho racing world. Finally, Garlits (seeing an opportunity to broaden the popularity of the sport) signs, followed by funny car driver Connie Kalitta (Beau Bridges), who has his own reasons; Connie talks a reluctant third driver into going along. In her first attempt to qualify for a race, she sets a track record. Later, during a dinner with their respective spouses, Connie gets her alone, makes a pass at her, and gets slapped in the face.

Shirley becomes successful, racing on weekends, but when Connie decides to move up to Top Fuel dragsters, she wants to buy his funny car and compete year round. This is the last straw for her husband, who just wants his wife back, and they separate.

Connie and Shirley become involved, despite his continual philandering. In a 1973 race, her funny car is destroyed and she herself is seriously burned. When Connie is suspended indefinitely by the NHRA for fighting on her behalf, she tells him that she is going to Top Fuel. He becomes her crew chief. She wins her first NHRA national event in 1976, then the World Championship in 1977. Finally, tired of Connies womanizing, she drops him from her team. Angry, he gets himself reinstated by the NHRA. 

Shirley, with little sponsorship and an inexperienced crew, has two lean years, but rebounds in 1980. She races against Kalitta in that years NHRA championship final. The film ends with her victory and their reconciliation, and her ex-husband (who had watched the victory on TV) giving her a private cheer.

==Cast==
*Bonnie Bedelia as Shirley Muldowney
*Beau Bridges as Connie Kalitta
*Leo Rossi as Jack Muldowney
*Hoyt Axton as Tex Roque
*Bill McKinney as Don Garlits|"Big Daddy" Don Garlits
*Anthony Edwards as John Muldowney, Shirley and Jacks son
*Tiffany Brissette as Little Shirley

Heart Like a Wheel marked the film debut of character actor Leonard Termo, who had a small role as Good Joe.   

==Reception by Muldowney==
Muldowney would rather have had Jamie Lee Curtis play her; she called Bedelia "a snot", and stated, "When she was promoting the movie on TV shows, she would tell interviewers she didnt even like racing. She got out of   race car like she was getting up from the dinner table."  Although Muldowney acted as creative consultant on the production, she has mixed feelings about the film itself, stating, "No, the movie did not capture my life very well at all, but more importantly, I thought the movie was very, very good for the sport." 

==See also==
* List of American films of 1983

==References==
 

==External links==
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 