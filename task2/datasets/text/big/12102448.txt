Nada (1947 film)
 
{{Infobox Film
| name           = Nada
| image          = Nada film.jpg
| image_size     = 
| caption        = Spanish film poster
| director       = Edgar Neville
| producer       = 
| writer         = Conchita Montes
| narrator       = 
| starring       = Conchita Montes Fosco Giachetti
| music          = José Muñoz Molleda
| cinematography = Manuel Berenguer        
| editing        =   
| distributor    = CIFESA
| released       = 1947 
| runtime        = 120 minutes
| country        =   Spanish
| budget         = 
}} 1947 Spain|Spanish Nada which won the Premio Nadal. It was written by Carmen Laforet. 

The novel was filmed also in Argentina in (1956) by Leopoldo Torre Nilsson with the title Graciela.

Although the film is an entirely Spanish production, the cast includes some Italian actors: Fosco Giachetti, María Denis, Adriano Rimoldi.

The film was censored and cut by 30 minutes, so credited actors such as Félix Navarro, María Bru and Rafael Bardem disappeared from the film. The role of José María Mompín was hardly reduced. Most of the Barcelona exteriors were removed.

==Plot summary==
 

==Cast==
*Conchita Montes	 ... 	Andrea
*Fosco Giachetti	... 	Román
*Tomás Blanco (actor)|Tomás Blanco	 ... 	Juan
*Mary Delgado	... 	Gloria
*María Cañete	... 	Angustias
*Julia Caba Alba	... 	Antonia
*María Denis ... 	Ena
*Adriano Rimoldi ... 	Jaime

==External links==
*  

 

 
 
 
 
 
 
 
 
 


 