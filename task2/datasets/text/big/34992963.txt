Les Damnés de la mer
{{multiple issues|
 
 
}}

{{Infobox film
| name           =  
| image          = 
| caption        = 
| director       = Jawad Rhalib
| producer       = Latcho Drom Arte RTBF Irène Productions Clap d’Ort Films
| writer         = 
| starring       = 
| distributor    = 
| released       = 2008
| runtime        = 71 minutes
| country        = Belgium France Morocco
| language       = 
| budget         = 
| gross          = 
| screenplay     = 
| cinematography = Olivier Pullincks
| sound          = Khalid Oudadess
| editing        = Karima Saïdi
| music          = Hassan Laarousi
}}

  is a 2008 documentary film.

== Synopsis == Dakhla (in the southernmost part of Morocco), one of the richest fishing regions in the world, hundreds of Moroccan fishermen, pushed by the increasing shortage of resources in the north, huddle together in tents sprayed by the ocean. However, their quest for a miraculous catch has revealed itself to be a tragic trap. As they have no licenses, they are sentenced to remain a few yards from the shoreline and catch what they can, while foreign trawlers equipped with the latest in sonar technology, captures the seas riches to export them to other continents.

== References ==
 

 
 
 
 
 
 
 
 
 
 


 
 