Relentless 3
{{Infobox Film |
 | name = Relentless 3
 | image =Relentless 3.jpg
 | director  = James Lemmo
 | producer  = Lisa M. Hansen (producer) Paul Hertzberg (producer)  Catalaine Knell (co-producer)  Leo Rossi (co-producer)
 | writer          = James Lemmo
 | cinematography = Jacques Haitkin William Forsythe
 | music = Scott Grusin
 | distributor = New Line
 | released  =  
 | runtime  = 93 minutes
 | country  = United States
 | language = English 
}}

Relentless 3 is a 1993 crime thriller film directed by James Lemmo. The tagline for the movie was: When the fear stops...youre dead! Relentless 3 was filmed in Los Angeles, California, USA. It is the third installment in the Relentless series.

The film was referenced twice on Saturday Night Live. The first time in 1993 Jeff Goldblum / Aerosmith #19.3 as a poster on the counter in  ‘Karl’s Video Store’ and again on The Best of David Spade in 1995, again a poster on the counter in ‘Karl’s Video Store’.

==Synopsis==

Sam Dietz returns to Los Angeles from "up North" and agrees to consult on a serial killer case. Not wanting to be more involved changes however, when the killer targets Dietz’s latest love interest, thereby, forcing him to become actively involved in the investigation. The killer is someone hes arrested before. 

==Cast==
*Leo Rossi as Sam Dietz William Forsythe as Walter Hilderman
*Robert Costanzo as Roy Kalewsky
*Edward Wiley as Lt Muldowney Tom Bower as Captain Phelan
*Savannah Smith Boucher as Maianne
*Stacy Edwards as Toni Keely
*Mindy Seeger as Francine
*George Tovar as Detective Santos Jack Knight as Detective Schulte
*Felton Perry as Detective Ziskie
*Charles Dennis as Detective Cirrillo
*Signy Coleman as Paula
*Diane Rodrigues as Angry Woman
*Jay Arlen Jones as Angry Man
*Brendan Ryan as Corey Dietz

==Home media==
* The movie was released directly to videocassette on August 18, 1993. In 2006, Image Entertainment released a double feature DVD containing this and the fourth film. Both films are presented in widescreen.

== Other films in the series ==
*Relentless (1989 film)|Relentless (1989)
*  (1992)
*  (1994)

==References==
 

==External links==
* 
* 

 
 
 
 
 

 