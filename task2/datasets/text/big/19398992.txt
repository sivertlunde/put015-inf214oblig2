Shrimps for a Day
{{Infobox Film
| name           = Shrimps for a Day
| image          = Shrimps 4 a day TITLE.JPEG
| image_size     = 
| caption        = 
| director       = Gus Meins
| producer       = Hal Roach
| writer         = 
| narrator       =  Matthew Beard Jackie Taylor Billie Thomas
| music          = Leroy Shield
| cinematography = Francis Corby
| editing        = Louis McManus MGM
| released       =  
| runtime        = 20 18"
| country        = United States 
| language       = English
| budget         = 
}} short comedy film directed by Gus Meins.  It was the 133rd Our Gang short (45th talking episode) that was released.

==Plot== Clarence Wilson) and Mrs. Crutch (Rosa Gore). Invited to a garden party at the home of wealthy Mr. Wade, the children enjoy a good time and are showered with gifts, though they know full well that their new clothes and toys will be appropriated and sold by the Crutches once they return to the orphanage.

Meanwhile, Mr. Wades daughter Mary (Doris McMahan) and her boyfriend Dick (Joe Young) stumble upon a magic lamp, which grants them their wish: to be children again. Dick and Mary are summarily rounded up by the Crutches and bundled off to the orphanage, where they manage to get the goods on the underhanded operation. Spanky has some funny scenes where he refuses to take a dose of castor oil and instead pushes it into Mr. Crutchs mouth. During a sleepless night, Spanky helps Dick escape out the window. Dick runs to Marys house where he finds the lamp and wishes he was an adult again. He then returns to being an adult and leads Mr. Wade back to the orphanage, exposing Crutch and restoring Mary to adulthood. Spanky has his revenge on Crutch by using the lamp to wish him down to his size, then beating him up.   

==Notes==
*Shrimps for a Day, like the previous short Mamas Little Pirate, was an Our Gang foray into pure fantasy.
*This is the last appearance of Jackie Lynn Taylor. She had been in the series since early 1934.

==Cast==
===The Gang=== Matthew Beard as Stymie
* Scotty Beckett as Scotty
* George McFarland as Spanky
* Billie Thomas as Buckwheat
* Jerry Tucker as Jerry
* Alvin Buckelew as Alvin
* Marianne Edwards as Marianne
* Leonard Kibrick as Leonard
* Jackie Lynn Taylor as Jane
* Gordon Evans as Boy who finds lamp
* Eileen Bernstein as Our Gang member
* Barbara Goodrich as Our Gang member
* Harry Harvey, Jr. as Our Gang member
* Paul Hilton as Our Gang member
* Philbrook Lyons as Our Gang member
* Tommy McFarland as Our Gang member
* Donald Profitt as Our Gang member
* Jackie White as Our Gang member
* Phyllis Yuse as Our Gang member

===Additional cast===
* Dick Brasno as Mr. Crutch as a child
* George Brasno as Dick as a child
* Olive Brasno as Mary as a child Herbert Evans as The butler
* Rosa Gore as Mrs. Crutch
* Wilfred Lucas as Mr. Wade, the sponsor
* Doris McMahan as Mary
* Ray Turner as Frightened man Clarence Wilson as Mr. Crutch
* Joe Young as Dick 
* Laughing Gravy as Dog
* Marialise Gumm as Undetermined role
* Dorian Johnson as Undetermined role
* Joyce Kay as Undetermined role
* Fred Purner, Jr. as Undetermined role
* Delmar Watson as Undetermined role

==See also==
* Our Gang filmography

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 