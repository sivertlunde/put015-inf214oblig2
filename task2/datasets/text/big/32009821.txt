A Beautiful Soul
{{Infobox film
| name           = A Beautiful Soul
| image          =
| caption        =
| director       = Jeffrey W. Byrd
| producer       = Holly Carter Dominique Telson
| screenplay     = Allison Elizabeth Brown Deitrick Haddon
| starring       = Deitrick Haddon Robert Richard Harry Lennix Lesley-Ann Brandt Barry Floyd
| music          = Deitrick Haddon
| studio         = Relevé Entertainment Manhaddon Entertainment  Tyscot Film + Entertainment
| distributor    = Tyscot Music & Entertainment
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         =
| gross          = $54,000  
}}
A Beautiful Soul is a 2012 drama film directed by American director Jeffrey W. Byrd. The film was first released upon May 4, 2012 and stars Deitrick Haddon, Lesley-Ann Brandt, and Harry Lennix.   

==Plot==
R&B superstar Andre Stephens (Deitrick Haddon) is on top of the world.  He has success, fame, and fortune but spiritually he has lost his way.  However, his "perfect" life is shattered when his entourage is brutally attacked, leaving Andre and his best friend Chris Johnson (Robert Richard) clinging to life.  On a spiritual journey that exists in a place that is neither on Earth nor in Heaven, Andre is given the opportunity to reevaluate his life and his faith. Andre realises he has a long way to go at church before being great.

==Cast==
* Deitrick Haddon as Andre Stephens
* Robert Richard as Chris Scott
* Lesley-Ann Brandt as Angela Berry
* Harry Lennix as Jeff Freeze
* Barry Floyd as Terrance Wilson Trevor Jackson as Quincy Smith

==Reception==
Variety (magazine)|Variety gave a mostly negative review for A Beautiful Soul, remarking that the "slackness of the storytelling has the effect of subjecting the low-budget pic’s supernatural elements to charm-killing scrutiny." 

===Awards=== Outstanding Television or Mini-Series Film Award at the Black Reel Awards (2013) 

==References==
 

== External links ==
*  

 
 
 
 
 
 


 