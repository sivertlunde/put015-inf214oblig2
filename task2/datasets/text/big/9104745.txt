Queen of the Night Clubs
{{Infobox film
| name           = Queen of the Night Clubs
| image          = queennight1929.jpg
| image_size     = 
| caption        = 
| director       = Bryan Foy
| producer       = Bryan Foy
| writer         = Addison Burkhard   Murray Roth
| narrator       =  John Davidson Lila Lee John Miljan Arthur Housman Eddie Foy Jr. Jack Norworth George Raft
| music          = 
| cinematography = Edwin B. DuPar
| editing        = 
| distributor    = Warner Bros. Pictures
| released       = March 16, 1929 
| runtime        = 60 minutes
| country        = United States
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

Queen of the Night Clubs (1929 in film|1929) is a sound (all-talking) musical-drama film produced and directed by Bryan Foy, distributed by Warner Brothers, and starring legendary nightclub hostess Texas Guinan. The picture, which featured appearancess by Eddie Foy, Jr., Lila Lee, and George Raft,  is now considered a lost film. 

==Plot==
After working as a hostess for Nick and Andy, Tex Malone leaves their employ and opens a club of her own. Looking for talent to book for the floor show, Tex hires Bee Walters and thereby breaks up Bees act with Eddie Parr. Andy spitefully kills Texs friend, Holland, and young Eddie is arrested for the crime on circumstantial evidence. Tex then learns from Eddies father, Phil, that Eddie is her long-lost son. At the trial, Tex comes to Eddies defense and persuades one member of the jury that there is reasonable doubt of Eddies guilt. The jury repairs to Texs club, where Tex discovers a piece of evidence that conclusively links Andy with the murder. Eddie is freed, and Tex and Phil get together for a second honeymoon.

==Cast==
*Texas Guinan - Texas Malone John Davidson - Don Holland
*Lila Lee - Bea Walters
*Arthur Housman - Andy Quinland
*Eddie Foy, Jr. - Eddie Parr
*Jack Norworth - Phil Parr
*George Raft - Gigola
*Jimmy Phillips - Nick
*William B. Davidson - Assistant District Attorney
*John Miljan - Lawyer Grant
*Lee Shumway - Crandall
*Joseph Depew - Roy
*Agnes Franey - Flapper
*Charlotte Merriam - Girl
*James T. Mack - Judge

==Production==
The film starred the legendary bar hostess and silent film actress Texas Guinan as "Texas Malone", a character obviously based upon herself. Guinans pal George Raft also appears in his first movie role. Queen of the Night Clubs was directed by Bryan Foy.  
 

==Reception== John Mosher of The New Yorker expressed disappointment, writing, "Rather to our surprise and much to our regret, Miss Guinan doesnt carry the picture with as much verve as it might seem that she would." 
  
==Preservation status==
*No film elements are known to exist. The complete soundtrack, however, survives on Vitaphone disks. Winner Take All (1932), an early James Cagney vehicle. HBO series Remember When...1927 hosted by Dick Cavett. This documentary series had Cavett cover a given year out of each decade from 1917 to 1969. Since this episode of Remember When was about 1927, the footage of Guinan could be newsreel footage from 1927 or extant 1929 footage from Queen of the Night Clubs (the same footage in Winner Take All).

==See also==
*List of lost films

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 


 