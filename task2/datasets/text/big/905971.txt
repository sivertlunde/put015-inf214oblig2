¡Three Amigos!
 
{{Infobox film
| name           = Three Amigos
| image          = Three amigos ver2.jpg
| caption        = Theatrical release poster
| director       = John Landis
| producer       = Lorne Michaels George Folsey, Jr.
| writer         = Lorne Michaels Steve Martin Randy Newman
| starring       = Steve Martin Chevy Chase Martin Short
| music          = Elmer Bernstein   Randy Newman  
| cinematography = Ronald W. Browne Malcolm Campbell
| studio         = HBO Films
| distributor    = Orion Pictures
| released       =  
| runtime        = 104 minutes
| country        = United States
| language       = English
| budget         = $25 million 
| gross          = $39,246,734   from Box Office Mojo 
}} western adaptation The Magnificent Seven.  Steve Martin, Chevy Chase, and Martin Short star as the title characters,    three silent film stars who are mistaken for real heroes by the suffering people of a small Mexican village and must find a way to live up to their reputation.

==Plot== protection money from the small Mexican village of Santo Poco. Carmen (Patrice Martinez), daughter of the village leader, searches for someone who can come to the rescue of her townspeople. While visiting a small village church, she watches a silent film featuring "The Three Amigos" and, believing them to be real heroes, sends a telegram to Hollywood asking them to come and stop El Guapo. However, the telegraph operator edits her message since she has very little money to pay for it.

Meanwhile, Lucky Day (Steve Martin), Dusty Bottoms (Chevy Chase), and little Ned Nederlander (Martin Short) are Hollywood silent film actors who portray the heroic Amigos on screen. When they demand a salary increase, studio boss Harry Flugleman (Joe Mantegna) fires them and evicts them from their studio-owned housing. Shortly afterward, they receive Carmens telegram, but misinterpret it as an invitation to make an appearance with El Guapo.

After breaking into the studio to retrieve their costumes, the Amigos head for Mexico. Stopping at a cantina near Santo Poco, they are mistaken for associates of a fast-shooting German pilot (Kai Wulff), who is also looking for El Guapo and who arrived just before they did. The Amigos perform a show at the Cantina, singing "My Little Buttercup", and leave the locals confused. The Germans real associates then arrive at the cantina, proving themselves adept with their pistols. A relieved Carmen picks up the Amigos and takes them to the village, where they are put up in the best house in town and treated very well.

The next morning, when three of El Guapos men come to raid the village, the Amigos do a Hollywood-style stunt show that leaves the men very confused. The bandits ride off, making everyone think that the Amigos have defeated the enemy. In reality, the men inform El Guapo of what has happened, and he decides to return in full force the next day and kill the Amigos.

The village throws a boisterous celebration for the Amigos and their (supposed) victory. The next morning, El Guapo and his gang come to Santo Poco and call out the Amigos, who confess that they have only been acting and are too scared to confront him after Lucky gets shot in the arm. El Guapo allows his men to loot the village and kidnaps Carmen, and the Amigos leave Santo Poco in disgrace.

With nothing waiting for them back home, Ned persuades Lucky and Dusty to become real-life heroes and go after El Guapo. Their first attempt to find his hideout fails when Dusty accidentally kills the Invisible Swordsman who can show them the way, but they spot an airplane and follow it to El Guapo; the plane is flown by the German, who has brought a shipment of rifles for the gang with his associates help. Preparations are underway for El Guapos 40th birthday party, and he plans to make Carmen his bride. The Amigos try to sneak into the hideout, with mixed results: Lucky is captured and chained up in a dungeon, Dusty crashes through a window and into Carmens room, and Ned gets his spurs stuck in a piñata hanging overhead.

As Lucky frees himself and Dusty sneaks out only to be caught, Ned falls loose and is also captured. The German, having idolized Neds Fast Draw|quick-draw and gunspinning pistol skills since childhood, challenges him to a shootout. Ned wins, killing the German, and Lucky holds El Guapo at gunpoint long enough for Carmen and the Amigos to escape—first on horseback, then in the Germans plane.

Returning to Santo Poco with El Guapos entire army in pursuit, the Amigos rally the villagers to stand up for themselves and plan a defense. The bandits arrive in the seemingly empty village, only to find themselves suddenly being shot at by Amigos from all sides and falling into hidden water-filled trenches dug by the villagers. Eventually all of El Guapos men either desert him or die in the gunfire, and he takes a fatal wound as well. As he lies dying, the villagers, all armed and wearing replicas of the Amigos costumes, step out to confront him. El Guapo congratulates them on this plan, then shoots Lucky in the foot and dies.

The villagers offer to give the Amigos all the money they have, but the Amigos refuse it, saying (as in their movies) that seeing justice done is enough of a reward for them. They then ride off into the sunset.

==Cast==
* Steve Martin as Lucky Day
* Chevy Chase as Dusty Bottoms
* Martin Short as Ned Nederlander
* Joe Mantegna as Harry Flugleman
* Phil Hartman and Jon Lovitz make cameo appearances as Sam and Morty, Harry Fluglemans assistants
* Tino Insana as The Studio Guard
* Alfonso Arau as El Guapo
* Tony Plana as Jefe
* Loyda Ramos as Conchita
* Patrice Martinez as Carmen
* Phillip Gordon as Rodrigo
* Kai Wulff as the German pilot
* Norbert Weisser and Brian Thompson as the Germans friends
* Fred Asparagus as the bartender
* Randy Newman as the Singing Bush
* Carl LaFong as the Tortoise Rebecca Underwood as Hot Senorita (kisses Ned at the close of the film)

==Production==
The film was written by Martin, Michaels and   was composed by Elmer Bernstein. It was shot in Simi Valley, California, Coronado National Forest, Old Tucson Studios, and Hollywood, Los Angeles, California|Hollywood. 

John Landis was on trial over the Twilight Zone tragedy during the editing of ¡Three Amigos!, and the studio heavily edited the film down after he submitted his final cut. 

===Deleted scenes===
Several deleted scenes were included in the Blu-ray release.    An alternate opening featured the peaceful village of Santa Poco being rampaged upon by El Guapo and his men, prompting Carmens search for help. Extended sequences of the Three Amigos at the studio mansion and backlot lead-in to another deleted subplot involving an up-and-coming rival actress at the studio, Miss Rene (Fran Drescher).   

A deleted scene featuring Sam Kinison as a mountain man was lost,  as was most of Dreschers material. 

==Music==
Elmer Bernstein wrote the score for ¡Three Amigos! and Randy Newman wrote the songs.
# "Ballad of the Three Amigos"
# "Main Title"
# "Big Sneak"
# "My Little Buttercup"
# "Santo Poco"
# "Fiesta and Flamenco"
# "Guapo"
# "Return of the Amigos"
# "Blue Shadows on the Trail"
# "Singing Bush"
# "Amigos at the Mission"
# "Capture"
# "Guapos Birthday"
# "Chase "
# "Amigos, Amigos, Amigos"
# "Farewell"
# "End Credits"

==Reception==

===Box office===
With an estimated budget of $25 million, ¡Three Amigos! had a domestic gross of $39,246,734. 

===Critical response===
The movie received mixed reviews. Review aggregator Rotten Tomatoes reports that 44% of 34 film critics gave the film a positive review, with a rating average of 5 out of 10.  Film critic Roger Ebert awarded the film one out of four stars and said, "The ideas to make Three Amigos into a good comedy are here, but the madness is missing."  It was ranked #79 on Bravo (US TV channel)|Bravos list of the "100 Funniest Movies". 

== See also ==
 

==References==
 

==External links==
 
*  
*  
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 