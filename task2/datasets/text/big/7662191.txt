The Chance of a Lifetime
 
{{Infobox film
| name           = The Chance of a Lifetime
| image          = Chanceofalifetime.jpg
| caption        = Film poster
| writer         = Jack Boyle Paul Yawitz
| starring       = Chester Morris Erik Rolf Jeanne Bates
| director       = William Castle
| producer       = Wallace MacDonald
| music          = M. W. Stoloff Ernest Miller
| editing        = Jerome Thoms
| distributor    = Columbia Pictures
| released       =  
| runtime        = 65 min.
| country        = United States English
}}
 crime drama drama starring Chester Morris, Erik Rolf and Jeanne Bates.  It is one of 14 films made by Columbia Pictures involving detective Boston Blackie, a criminal-turned-crime solver. This was the sixth in the series and one of three that did not have his name in the title.  The film is also William Castles directorial debut. As with many of the films of the period, this was a flag waver to support Americas efforts during World War II.

==Plot==
Boston Blackie (Chester Morris) helps get prisoners with needed skills released on parole to help in the machine and tool plant of his friend, Arthur Manleder (Lloyd Corrigan). Those chosen want to support Americas war effort. All of the parolees have to stay in Blackies apartment, all except robber Dooley Watson. Blackie allows him to see his wife and son.
 John Harmon) Richard Lane).

==Cast==
* Chester Morris as Boston Blackie
* Erik Rolf as Dooley Watson
* Jeanne Bates as Mary Watson Richard Lane as Inspector John Farraday
* George E. Stone as The Runt
* Lloyd Corrigan as Arthur Manleder

Uncredited:
* Arthur Hunnicutt as Elwood "Tex" Stewart
* Pierre Watkin as Gov. Rutledge
* Douglas Fowley as "Nails" Blanton
* Sid Melton as "Sunny" Hines
* Walter Sande as Detective Sgt. Mathews
* Haarry Semels as Jerome "Egypt" Hines
* Ray Teal as Joe, a cop

==Reception== Hal Erickson of Allmovie said "The Chance of a Lifetime represents the first directorial effort of William Castle, who later claimed that, saddled with a hopeless project, he made the film "work" by re-arranging the reels in the editing room." 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 