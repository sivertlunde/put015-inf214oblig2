Tholi Prema
{{Infobox film
| name           = Tholi Prema
| image          = Tholi Prema.jpg
| caption        =
| writer         = Chintapalli Ramana  
| story          = A. Karunakaran
| screenplay     = A. Karunakaran
| producer       = G.V.G Raju
| director       = A. Karunakaran
| starring       = Pawan Kalyan Keerthi Reddy Deva
| cinematography = Mahindra
| editing        = Marthand K. Venkatesh
| studio         = NSC Arts
| released       =  
| runtime        = 146 minutes
| country        = India
| language       = Telugu
| budget         =
| gross          = 
}}  
 Hindi as Mujhe Kucch Kehna Hai starring Tusshar Kapoor and Kareena Kapoor and in Kannada as Preethsu Thappenilla with V. Ravichandran. It was dubbed in Tamil as Aanandha Mazhai.

==Plot== Venu Madhav and others) and hangs around with them. On one occasion, Balus father is angry with him; Balu leaves the house to be left alone for a while (on the eve of Diwali). He chances to see Anu (Keerthi Reddy) on the road with some kids and he falls for her. She has returned from the USA to spend time with her grandfather (P.J. Sharma).

Anu is impressed with Balu when he saves a kids life on the motorway; she tries to get his autograph but fails. Balu leaves for Ooty for his studies and accidentally meets Anu on the way when his taxi breaks down. Anu gives Balu a lift and leaves her driver to help the taxi driver. While on the way, Anus car slips into a valley due to a rash lorry driver. Balu makes an effort to save Anu and rescues her to safer ground. Trying to save Anu, Balu slips off into a valley. Anu is depressed that Balu sacrificed himself to save her, searches for him and loses hope. Balu is saved by a truck driver and admitted to a hospital. On his return, Anu meets Balu at his house to express her gratitude. Balu thinks of proposing his love but is warned by his sister Priya(Bujji). Balu develops a friendship with Anu but never dares to express his love. Priya gets married and leaves. At the same time, Anu gets admission into Harvard University (USA) and is about to leave. They remain silent until they reach the airport. Just before Anu leaves, she realizes that she is indeed in love with Balu. She proposes to him and they agree to meet after realizing their goals.

==Cast==
* Pawan Kalyan as Balu
* Keerthi Reddy as Anu Ali as Balus friend Venu Madhav as Arnold Sekhar
* Narra Venkateswara Rao as Viswanatham Nagesh as Elder uncle
* Achyuth as Anil
* Prasanna Kumar
* Ravi Babu as Villain 
* P. J. Sarma as Anus Grandfather
* Vasuki as Bujji / Priya Sangeetha 
* Bangalore Padma 
* Manisha

==Soundtrack==
{{Infobox album
| Name        = Tholi Prema
| Tagline     = 
| Type        = film Deva
| Cover       = 
| Released    = 1998
| Recorded    = 
| Genre       = Soundtrack
| Length      = 24:20
| Label       = Lahari Music Deva
| Reviews     =
| Last album  = 
| This album  = 
| Next album  = 
}}

Music composed by Deva (music director)|Deva. All songs are blockbusters. Music released on Lahari Music Company.
{|class="wik{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length =
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = yes
| music_credits =

| title1  = Ee Manasese
| lyrics1 = Bhuvanachandra SP Balu
| length1 = 4:26

| title2  = Emaindo Emo Sirivennela Sitarama Sastry
| extra2  = SP Balu
| length2 = 4:42

| title3  = Yemi Sodara
| lyrics3 = Sirivennela Sitarama Sastry
| extra3  = Krishna Raj
| length3 = 4:18

| title4  = Gagananiki Udayam
| lyrics4 = Sirivennela Sitarama Sastry
| extra4  = SP Balu
| length4 = 5:55

| title5  = Romancelo Rhythms
| lyrics5 = Bhuvanachandra 
| extra5  = Suresh Peters, Unni Krishnan
| length5 = 3:09
}}
|}
==Awards==
* The film won the National Film Award for Best Feature Film in Telugu to producer G.V.G Raju.
* The film won the Nandi Award for Best Feature Film.

==Trivia==
* This is A. Karunakaran|Karunakarans first film as director.
* Karunakaran saw Pawan Kalyan’s photo on a news magazine at a pan shop in Chennai and decided that he would be the lead actor.
* Karunakaran is a painter in real life. Keerthi Reddy’s picture on the post cards was drawn by him.
* Pawan Kalyan cut down his Remuneration for the Taj Mahal set in “Gagananiki Udayam Okate”.
* The film had run for more than 100 days in 21 centres; it ran for more than 200 days in 2 centres.
* Pawan Kalyan’s bike is shown as a modified version of 2 stroke RX-100; the bike shown after modification is a 4 stroke dirtbike.
* There is no female voice in any of the songs in the movie.
* The film was later remade in Hindi as Mujhe Kuch Kehna Hai starring Tusshar Kapoor and Kareena Kapoor. It was dubbed in Tamil as Aanandha Mazhai.
* It was winner of six Nandi Awards and National Award Best Feature Film (Telugu) and ran for more than 365 days.

==Others== Hyderabad

==External links==
*  

 
 

 
 
 
 
 
 