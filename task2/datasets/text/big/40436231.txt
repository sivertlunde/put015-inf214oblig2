Loved by a Maori Chieftess
 
{{Infobox film
| name           = Loved by a Maori Chieftess
| image          =
| image_size     =
| caption        =
| director       = Gaston Méliès
| producer       = Gaston Méliès
| writer         = Edmund Mitchell
| narrator       =
| starring       =
| music          =
| cinematography = George Scott
| editing        =
| distributor    =
| released       = 14 March 1913 (USA)
| runtime        = 2000 ft or 2 reels ca. 34 min.
| country        = France/New Zealand
| language       = silent
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}

Loved by a Maori Chieftess is a 1913 New Zealand feature film directed by Gaston Méliès. Principal photography took place in Rotorua, New Zealand.   

A paragraph by "Musico-Dramaticus" in the New Zealand Herald of 10 May 1913   says about "Loved by a Maori Chieftess" that: The first of the New Zealand films taken by the Melies company in the King Country was released on March 14 although Sam Edwards says that the three feature films by Melies were shot at Rotorua.

He shot three other films in New Zealand in 1912-13: Hinemoa (1913 film)|Hinemoa, How Chief Te Ponga Won His Bride and The River Wanganui. Méliès sent his film to the United States for post-production treatment, so it is doubtful if any were shown in New Zealand.

== Plot ==
Wena a Māori princess is told by a sourceress that "she will marry a white man, tall and handsome, with eyes as fair as the sky and a fair beard". Chadwick a trapper who was hunting in the bush is captured and bought to the pa to be burnt at the stake to provide "white mans meat". She recognises Chadwick as that man, but the young chief Te Heuheu arrives to court her. She helps Chadwick escape and they swear eternal love. Chadwick goes through the geysers to his hut on the lake, but they are discovered there by Te Heuheu. Chadwick is threatened with death, so she returns to the tribe. But when she is near death from pining for him her father the chief relents. Chadwick is made a chief, she recovers and the tribe celebrates with a feast. Wena was probably played by Maata Horomona of Ohinemutu.

== External links ==
*  
* 

==References==
*New Zealand Film 1912-1996 by Helen Martin & Sam Edwards p20 (1997, Oxford University Press, Auckland) ISBN 019 558336 1  
 

 
 
 
 
 
 
 
 
 


 
 