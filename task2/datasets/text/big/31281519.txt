The Women on the 6th Floor
 
 
{{Infobox film
| name           = The Women on the 6th Floor
| image          = Affiche de "Les femmes du 6e etage".jpg
| caption        = Cinematic poster
| director       = Philippe le Guay
| producer       = 
| writer         = Philippe le Guay Jérôme Tonnerre
| screenplay     = 
| story          = 
| based on       =  
| starring       = Fabrice Luchini Sandrine Kiberlain Natalia Verbeke
| music          = Jorge Arriagada
| cinematography = Jean-Claude Larrieu
| editing        = Monica Coleman
| studio         = 
| distributor    = 
| released       =  
| runtime        = 106 minutes
| country        = France
| language       = French Spanish
| budget         = €  7 million
| gross          = $23,267,311   
}}
The Women on the 6th Floor ( ; also known as Service Entrance) is a 2010 French film directed by Philippe le Guay.   Retrieved 24 March 2011      Le Monde, 15 February 2011. Retrieved 25 March 2011.     It was written by Le Guay and Jérôme Tonnerre. 

The film premièred at the Montpellier International Festival of Mediterranean Film on 23 October 2010.  Its cinematic run in France began on 16 February 2011.  First shown in the USA in March 2011 at Rendezvous with French Cinema, it began its release on 7 October 2011.	

==Plot==
Set in Paris in the 1960s, the film is a social comedy that pits the propriety of a well-to-do French family with the earthiness and humour of Spanish  , 15 February 2011. Retrieved 26 March 2011.  It follows Monsieur Joubert (Fabrice Luchini), an unadventurous stockbroker, as he befriends the Spanish maids who live on the top floor of his building. Maria (Natalia Verbeke), his new maid, introduces him to her compatriots and their simple but happy lives animated by friendship and folklore, in contrast to the relative emotional austerity of his own life. Slowly he recovers his joie de vivre by tasting lifes simple pleasures; when his wife (Sandrine Kiberlain) falsely accuses him of having an affair he moves into an empty room in the servants quarters upstairs, the first time he has had a bedroom of his own.

==Production==
Two of the Spanish actresses, Berta Ojea and Concha Galán, did not speak French before the film and learned their roles phonetically.   Retrieved 26 March 2011   
 
== Release and reception ==
The film was screened out of competition at the Berlinale in 2011.  It was well received by critics and audiences. Le Monde wrote "The entertainment is as good as the actors are pitch-perfect. Fabrice Luchini and Sandrine Kiberlain are among our best stars."  Dissenting, La Croix described the "lazy screenplay, poor dialogue, catalogue of clichés, indigent mise en scène".  The New York Press reviewed the film at the Rendez-vous with French Film festival in New York, calling it "charming". 
== Awards ==
The movie was nominated in three categories at the César Award 2012 : Best costume design, Best production design and Best supporting actress. It won the Best supporting actress César - Carmen Maura.  37th César Awards, 37th César Awards  

==Discography ==
The original soundtrack to Les Femmes du 6e étage is included on the CD compilation Les Musiques de Jorge Arriagada pour les films de Philippe Le Guay, released by Canadian label Disques Cinémusique in 2013. French and English liner notes.  .

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 