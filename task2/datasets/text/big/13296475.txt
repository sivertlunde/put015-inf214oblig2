Albert Fish (film)
{{Infobox Film
| name = Albert Fish
| image = FISH_POSTER5x7.jpg
| caption = Theatrical release poster
| director = John Borowski
| producer = John Borowski
| writer = John Borowski
| starring = Tony Jay  Oto Brezina  Bob Dunsworth  Harvey Fisher Derek Gaspar  Nathan Hall Garrett Shriver
| music = Corey A. Jackson
| distributor = Facets Video
| released =  
| runtime = 86 minutes
| country = United States
| language = English
}}
Albert Fish is a 2007 biographical documentary film directed by John Borowski. The film relates the life story of American serial killer and cannibal Albert Fish. In addition to interviews, period footage and photographs, the film also recreates many of Fishs crimes in numerous reenactment scenes. The film is also Tony Jays final work, having died seven months prior to its release.

==Plot==
(DVD description)
 sadomasochistic cannibal, biblical tales as he took the stories of pain, punishment, atonement, and suffering literally as he preyed on victims to torture and sacrifice.
 Joe Coleman and renowned true crime|true-crime author Katherine Ramsland, Ph.D.

== Reception ==

“Borowski offers plenty of material in his second docudrama to generate many conversations, raising his work above B-movie gore into the realm of
philosophical discourse.” 

"A very well made and well directed examination of one of American historys most unusual and depraved subjects." 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 