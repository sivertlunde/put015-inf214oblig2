Rann (film)
 
 
{{Infobox film
| name           = Rann
| image          = Rannr.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = Ram Gopal Varma
| producer       =Madhu Mantena Sheetal Vinod Talwar
| story          = Rohit Banawlikar
| starring       =  
| music          = Dharmaraj Bhatt Jayesh Gandhi Bapi-Tutul Sanjeev Kohli Imran-Vikram Amar Mohile
| cinematography = Amit Roy  Amol Rathod (second unit)
| editing        = Nipun Gupta
| studio         =
| distributor    = PVR Pictures Vistaar Religare Film Fund Big Bang Films Production WSG Pictures
| released       =  
| runtime        =
| country        = India Canada
| language       = Hindi
| gross          =  
|
}} political thriller film that stars Amitabh Bachchan, Sudeep, Ritesh Deshmukh, and Gul Panag in the lead roles  and Paresh Rawal in a negative role.  The film, directed  by Ram Gopal Varma, focuses on Indian media. Rann received positive reviews, and was premiered at Toronto International Film Festival.  Taran Adarsh praised the film and its cast, especially Amitabh Bachchan.  Nikhat Kazmi of the Times of India gave the film four stars, calling it a "riveting experience."  Noyon Jyoti Parasara of AOL gave the film 3.5 out of a possible 5 and said, "Overall, Rann is quite an inspirational fare. It might just teach you to own up and admit that you were wrong at point." 
 
 Despite garnering positive reviews the film could not do well at the box office.

==Plot==
Vijay Harshwardhan Malik (Amitabh Bachchan), the ethical CEO of struggling television channel India 24/7, is losing the ratings battle with a rival channel headed by Amrish Kakkar (Mohnish Behl). Maliks son Jai (Sudeep) makes a deal with a wealthy and corrupt politician, Mohan Pandey (Paresh Rawal), to frame the Prime Minister (K K Raina) for being complicit in a terror attack. Pandey can then take over the position and Jai will have enough money to start his own channel. Jais brother-in-law (Rajat Kapoor) supports Mohan Pandey because he wants to become the biggest industrialist in the country and Pandey could help him by framing such policies. Jai shoots a short video featuring his friend Khanna and a close friend of the PM. This meeting is staged and the PMs friend, under duress, says that the PM was involved in plotting a bomb blast so as to create fear and panic among the people so that he could get a bill passed. Jai convinces his father that the story is true and believing it to be so, Malik airs it on his network. The scandal rocks the nation and elections are held in which Mohan Pandey wins. He becomes the PM. However, one of Maliks reporters, Purab (Ritesh Deshmukh), discovers the plot. He initially approaches Amrish Kakkar with a request to air his findings on the news. Amrish, though, cuts a deal with Mohan Pandey and does not air the CD. Purab then tells his boss of his findings on the day his son is getting engaged. Malik goes on air one final time and confesses the wrongdoings of his son, son-in-law and exposes Mohan Pandey. Unable to bear the guilt, Jai commits suicide. Mohan Pandey denies his role in the scandal. Malik steps down as the CEO of the news channel and hands over the baton to the reporter who exposed the truth.

==Cast==
*Amitabh Bachchan as Vijay Harshwardhan Malik
*Sudeep as Jai Malik
*Ritesh Deshmukh as Purab Shastri
*Paresh Rawal as Mohan Pandey
*Mohnish Behl as Amrish Kakkar
*Rajat Kapoor as Naveen Shankalya
*Rajpal Yadav as Anand Prakash Trivedi
*Gul Panag as Nandita Sharma
*Suchitra Krishnamurthy as Nalini Kashyap
*Neetu Chandra as Yasmin Hussain
*Simone Singh as Mrs Naveen Shankalya
*K K Raina as Prime Minister Hooda
*Anuj Tikku as Khanna

==Trivia==
Indian actor and musician, Himesh Reshammiya was supposed to play a role in the film, but later opted out due to date clash. RGV, being a fan of Himesh, however included a couple of lines from Himeshs song Lut Jaaoon (Karzzzz) in the film. In one scene, Rajpal Yadavs character recites the same.

==Critical Reception==
Rajeev Masand criticised the film as being poorly researched and as "sexy and predictable".  Mayank Shekhar of the Hindustan Times called Rann an "exercise in corniness"  while other critics complained that the film is cliché  with a poor script and shallow characters. 

==Music==
The films music is by Amar Mohile, Dharam-Sandeep, Bapi-Tutul, Sanjeev Kohli and Jayesh Gandhi; the lyrics are by Prashant Pandey and Sarim Momin. The title song, "Rann Hai", was written by debut lyricst Vayu.
The background score of the film was scored by Dharam-Sandeep, and the track named "Sikkon Ki Bhook" was also composed by the duo.

==References==
  

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 