Encounter: The Killing
{{Infobox film
| name           = Encounter: The Killing
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Ajay Phansekar
| producer       = Ajay Phansekar
| writer         = Ajay Phansekar
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Naseeruddin Shah Dilip Prabhavalkar Tara Deshpande
| music          = Amar Mohile Arun Paudwal
| cinematography = 
| editing        = Arun Shekhar
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}}
 Indian Bollywood film produced and directed by Ajay Phansekar. It stars Naseeruddin Shah, Dilip Prabhavalkar and Tara Deshpande in pivotal roles. 

==Cast==
* Naseeruddin Shah...Inspector Sam Bharucha
* Dilip Prabhavalkar...Ponappa Awadhe
* Tara Deshpande...Kiran Jaywant
* Ratna Pathak...Mrs. Sudhakar Rao
* Akash Khurana...Sudhakar Rao
* Sanjeev Dabholkar...Avinash Marathe
* Rahul Mahendale...Lallya
* Sachin Shivsagar...Sonal
* Sushant Shellar...Martin
* Mahesh Jadhav...Nana

==References==
 

==External links==
* 

 
 
 

 