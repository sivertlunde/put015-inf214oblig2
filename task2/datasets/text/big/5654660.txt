Andhrawala
{{Infobox film
| name           = Andhrawala
| image          = andhrawala_poster.jpg
| writer         = Kona Venkat
| starring       = Jr. NTR Rakshitha Sayaji Shinde Rahul Dev
| director       = Puri Jagannadh
| producer       = R. R. Venkat Kona Venkat
| editing        = Marthand K. Venkatesh
| cinematography = Shyam K. Naidu
| released       =  
| runtime        = 154 minutes
| country        = India
| language       = Telugu Chakri
| budget         =  10 crores
}} Telugu action Chakri while cinematography was handled by Shyam K. Naidu.  The film created a lot of hype as Jr. NTR was acting in this film just after the blockbuster Simhadri (film)|Simhadri. However, it became a disaster. This film was simultaneously made in Kannada as Veera Kannadiga with Puneeth Rajkumar, directed by Meher Ramesh; the Kannada version was a huge hit. . The film has been dubbed in Tamil as Velaa and in Hindi as Barood:Man On Mission.

== Plot ==
Shankar Pehalvan (Jr. NTR), is the leader of labor from Andhra in Mumbai. Shankar fights with the mafia don Bade Mia (Sayaji Shinde) and in the process Andhrawala also becomes a don. In the ensuing mafia feud, Bade Mia kills Shankar and his wife (Sanghavi). Basha (Benarjee) - a trusted lieutenant of Sankar - takes away the kid to rescue him from Bade Mia. When mafia goons follow him, Basha leaves the kid on a footpath besides a beggar and leaves. The boy, Munna (Jr. NTR), is raised in a slum area. While Basha is in search of Munna to safeguard him, Bade Mia is in search of him to avenge the death of his son in the hands of Shankar. Thanks to the similarities in appearance of father and son, Basha and Bade Mia recognize that Munna is son of Sankar. Munna gets to know his identity and the story of his father and his death from Basha. In the meanwhile, Bade Mia men descend to Hyderabad to kill Munna. Munna decides to take the battle to Bade Mia and goes to Mumbai to confront with Bade Mia face to face. The rest of the story is all about how Munna succeeds in killing his familys murderers.

== Cast ==
* Jr. NTR as Munna /  Shankar Pehalvan
* Rakshitha as Chitra
* Sanghavi as Shankar Pehalvans wife
* Sayaji Shinde as Bade Mia
* Atul Kulkarni as Bade Mias sidekick
* Rahul Dev as Dhanraj
* Nassar
* Banerjee as Basha
* Brahmanandam
* M. S. Narayana
* Uttej
* Telengana Shakuntala Venu Madhav Jeeva
* Raghu Babu
* Rajiv Kanakala
* Raghu Kunche
* Ramaprabha
* Bandla Ganesh
* Pavala Shyamala

==Soundtrack==
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s)!! Notes
|- 1 || Kokkoka Missaa || Chakri (music director)|Chakri, Kousalya || 
|- 2 || Nairey Nairey || Chakri, Kousalya || 
|- 3 || Unga Unga || Chakri, Kousalya ||
|- 4 || Mallegateegaroi || Chakri, Kousalya ||
|- 5 || Gitchi Gitchi || Chakri, Kousalya ||
|- 6 || Nipputonnakai || Chakri, Kousalya ||
|}

== Crew ==
* Director:: Puri Jagannadh
* Producer: ASHOK MIRIYALA
* Story: Puri Jagannadh
* Dialogues: kona venkat
* Cinematography: Shyam K. Naidu Chakri
* Choreography: Raghava Lawrence, Krishna Reddy, & Pradeep Anthony
* Editing: Marthand K. Venkatesh
* Art: Chinna

==External links==
* 

 

 
 
 


 