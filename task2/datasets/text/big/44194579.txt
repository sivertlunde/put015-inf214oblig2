(Sex) Appeal
{{Infobox film
| name           = (Sex) Appeal
| image          = (Sex) Appeal poster.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Wang Wei-ming
| producer       = Hsu Hsiao-ming
| writer         = 
| screenplay     = Hsu Kun-hua
| story          = Wang Wei-ming
| based on       =  
| narrator       = 
| starring       = Vivian Hsu Alyssa Chia Amber Kuo Leon Dai
| music          = Annie Lo Mark Lee
| editing        = Man Chi-ming
| studio         = Kuli Film Co.,Ltd Stellar Mega Films Limited Guangdong 21St Century Media Co.,Ltd Le Vision Pictures (Beijing) Co.,Ltd Taiwan Pictures Co.,Ltd Arong Co.,Ltd
| distributor    = 
| released       =   
| runtime        = 109 minutes
| country        = Taiwan China
| language       = Mandarin
| budget         = 
| gross          = 
}}
 romance drama film directed by Wang Wei-ming. It was released in Taiwan and China on October 24, 2014.  

==Cast==
*Vivian Hsu as Fang An-yu
*Alyssa Chia as Lin An-ni
*Amber Kuo as Pai Hui-hua
*Leon Dai as Li Jen-fang
*Jade Chou as Wang Wen-hui
*Yuan Huang
*Lene Lai as Li Ya
*Fion Fu as Fu Hsiao-ling

==Reception==

===Critical response===
On Film Business Asia, Derek Elley gave it a 7 out of 10, calling it a "notable, if over-dense, drama centred on a teacher-student "rape"   raises the Taiwan bar." 

===Accolades===
{| class="wikitable sortable" width="90%"
|- style="background:#ccc; text-align:center;"
! Award
! Date
! Category
! Recipients and  nominees
! Result
|- Golden Horse Awards
| rowspan="3"| November 22, 2014 
| Best Supporting Actor
| Leon Dai
|  
|-
| Best Cinematography Mark Lee
|  
|-
| Best Original Film Song
| (Sex) Appeal
|  
|-
|}

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 
 