Fack ju Göhte
{{Infobox film
| name           = Fack ju Göhte
| image          = Fack ju göhte movie poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Bora Dagtekin
| producer       = Christian Becker Lena Schömann
| writer         = Bora Dagtekin
| starring       = Elyas M’Barek Karoline Herfurth
| music          = Beckmann   Djorkaeff   Beatzarre
| cinematography = Christof Wahl
| editing        = Charles Ladmiral  Zaz Montana
| studio         = Constantin Film  Rat Pack Filmproduktion
| distributor    = Constantin Film
| released       =  
| runtime        = 118 minutes
| country        = Germany
| language       = German
| budget         = 
| gross          = 
}} Fuck you, German comedy by Bora Dagtekin. Elyas MBarek and Karoline Herfurth play the main roles while upcoming actors Max von der Groeben and Jella Haase play the supporting roles. The film was produced by Rat Pack Filmproduktion in coproduction with Constantin Film.

Fack ju Göthe premiered on the 29th of October 2013 in Munich, and was subsequently released in cinemas on the 7th of November 2013.

== Plot ==
Bank robber Zeki Müller has just been released from prison. Upon his release, he goes to retrieve the money he stole in order to repay a debt, but his friend buried it in a construction site. During Zekis time in prison, the construction project was completed and his money is now buried beneath the new gymnasium at the Goethe Gesamtschule. In order to obtain the money, Zeki applies for the vacant position of a deceased janitor. However, due to a misunderstanding, he is given a job as a substitute teacher.

Lisi Schnabelstedt is a student teacher at the Goethe Gesamtschule, her former high school, because she has guardianship over her younger sister, Laura. Lisi does not impose the necessary authority to manage the students successfully.

When Ingrid Leimbach-Knorr, homeroom teacher of class 10B, attempts suicide, the management of the difficult class goes to Lisi Schnabelstedt, although the classs brutal pranks upon the new teacher quickly reduces her to tears. Zeki takes over as the substitute homeroom teacher for Lisis former seventh grade class. In order to regain his money, Zeki works during the night in the cellar of the school on a tunnel leading to the buried money. He also copies Lisis diploma because he does not have a high school or college education, which eventually becomes known to Lisi. She then blackmails Zeki, allowing her to get her old seventh grade class back while Zeki assumes responsibility over class 10B.

Zeki is thrown out of his living situation at a strip club, and tries to sleep in Lisis garage. When discovered, Lisi allows Zeki to live in her basement as long as he properly teaches class 10B instead of watching films every class period.
 Jugend forscht group. Zeki, concerned about Lisis image amongst the class, leads the class on an excursion to paint graffiti on a train, during which a student paints the title ‘Fack ju Göhte’. As a result of this increased respect, Lisi passes her practical teaching exam with class 10B. In addition, Zeki arranges an affair for Lisis little sister Laura with her crush Danger, as well as ensuring Lisis continued legal guardianship of Laura by pretending to be Lisis serious boyfriend when Lauras social worker comes to visit. Eventually Zeki finds the money in the tunnel, but the tunnel beneath the gymnasium causes the floor to break and allows Lisi to discover Zeki filling in the tunnel. When Lisi learns about Zekis past as a criminal, she threatens to call the police if he does not leave his job and her home at once. Zeki, out of options, agrees to drive the get away car for a bank heist. As Zekis class is about to take their final exam in German, they all take out their motivation photos. A student informs Lisi that Zekis motivation photo is in his desk. When Lisi opens the desk, she discovers that his motivation for becoming a better person is her. A friend of Zekis is able to convince Lisi that Zeki wants to change his ways for her. 

Zeki doesnt follow through with the heist because three students stop him to ask if he will be their homeroom teacher next year and he sees the train with the ‘Fack ju Göthe’ graffiti. He sends Lisi a dress and an invitation to the prom and reports himself to the school principal. The principal wants to keep him and even hands him a falsified high school diploma with a 2.9 GPA. She informs Zeki that the class 10B has drastically improved and if they continue to work towards their high school diplomas, the school will become the best in the city. Their marks in German, previously 5s and 6s (equivalent to Ds and Fs), have now become better than 3s (Cs).

== Cast ==

* Elyas M’Barek: Zeki Müller
* Karoline Herfurth: Elisabeth ‘Lisi’ Schnabelstedt
* Katja Riemann: Gudrun Gerster
* Jana Pallaske: Charlie
* Alwara Höfels: Caro Meyer
* Jella Haase: Chantal Ackermann
* Max von der Groeben: Daniel ‘Danger’ Becker
* Anna Lena Klenke: Laura Schnabelstedt
* Gizem Emre: Zeynep
* Aram Arami: Burak
* Uschi Glas: Ingrid Leimbach-Knorr
* Margarita Broich: Frau Sieberts
* Farid Bang: Paco
* Christian Näthe: Biology teacher
* Bernd Stegemann: Herr Gundlach
* Erdal Yıldız: Attila
* Laura Osswald: Kindergarten teacher

== Production ==

=== Casting ===

The role of Zeki Müller was written by Bora Dagtekin for Elyas MBarek. MBarek and Dagtekin had previously worked together on the television series Schulmädchen, Doctor’s Diary, Türkisch für Anfänger and its film adaptation. Producer Lena Schömann praised MBarek for his unbelievable discipline, as the actor had worked out five times a week with a personal trainer, losing eight kilos, months before filming began. MBarek had just trained his upper body for Türkisch für Anfänger. Press material of Fack ju Göhte 
 Grimme Award winner Karoline Herfurth, who after Mädchen, Mädchen and its 2004 sequel was only seen rarely in comedies. Schömann gave his reasons for Herfurths choice, stating that she has a fantastic instinct for comedy and timing.  The role of school principal went to Katja Riemann, who had just made a guest-appearance in the film of the series Türkisch für Anfänger.
 Goldenen Kamera. Jella Haase, who appears as Chantal, had previously been awarded the Studio Hamburg Nachwuchspreis (Studio Hamburg Young Performers Award) and the Günter-Strack-Fernsehpreis (Günter Strack Television Prize) for best actress.

=== Filming ===
 

Filming took place mainly in Munich and Berlin. The Lise-Meitner-Gymnasium in Unterhaching served as the backdrop of the Goethe-Gesamtschule in the film. The school had been used before in films, such as Schule, Die Wolke, and the ZDF television series, Klimawechsel. Filming in Berlin was done, among other places, on the Kurfürstendamm and in an apartment block area in Berlin-Neukölln. Also, the swimming pool scenes were shot in Berlin. The prison scenes were shot in a former GDR prison in Keibelstraße in Berlin. 

Filming commenced on the 28th of April 2013 and lasted for forty-one days. 

=== Opening ===
On the 28th of July 2013, four teaser trailers were released, while the official trailer was released on the 10th of October 2013. After that, on the 29th of October 2013, the premiere was held at the Mathäser Cinema in Munich. On the 2nd of November 2013, a one-week tour through Germany and Austria commenced, with Bora Dagtekin, Elyas M’Barek and Karoline Herfurth, before the film was released in German cinemas on the 7th of November 2013. 25 cities were visited in Germany, amongst them Hamburg, Berlin, Karlsruhe, Cologne, Cottbus and Dortmund. Linz, Vienna und Salzburg were visited in Austria. 

Within 17 days, the film was seen by three million people, proving to be the most successful German film of the year in the shortest time.  , retrieved 25 November 2013 

== Soundtrack ==
{{Infobox album  
| Name       = Fack ju Göhte (Official Soundtrack)
| Type       = Soundtrack Various Artists
| Cover      = 
| Alt        = 
| Released   =  
| Recorded   = 
| Genre      = Soundtrack
| Length     =  
| Label      = Polydor (Universal)
| Producer   = 
| Last album = 
| This album = 
| Next album = 
}}

A soundtrack CD was released at the time of the films release. Aside from various songs which were used in the film, the soundtrack also includes parts of the films score by Beckmann and Djorkaeff.

;Track list (Soundtrack) John Newman
# ‘Fack Ju Göhte Beat’ − Djorkaeff
# ‘Error (song)|Error’ − Madeline Juno 
# ‘Pumpin Blood’ − Nonono
# ‘Let Her Go’ − Passenger
# ‘Hands Around The World’ − Djorkaeff
# ‘Benzin Beat’ − Djorkaeff
# ‘Move’ − D/R Period
# ‘Brother’ − Smashproof
# ‘Aint No Fun’ − Nitro
# ‘Hey Now’ − Martin Solveig
# ‘Everybody Talks’ − Neon Trees
# ‘Das Ist Pimkie!’ − Beckmann
# ‘Dry My Soul’ − Amanda Jenssen	
# ‘Change Is Gonna Come’ − Olly Murs 	 Busted
# ‘High Hopes’ − Kodaline
# ‘Zeitkapsel’ − Beckmann
# ‘Cant Help’ − Parachute
# ‘Deep Diving’ − Thilo Brandt
# ‘Klassenzimmer Beat’ − Djorkaeff
# ‘Laura, Sie Redet Mit Dir!’ − Beckmann
# ‘Theater Beat’ − Djorkaeff
# ‘Balkan Bachata’ − Clea & Kim
# ‘Touch Me (I Want Your Body)’ − Samantha Fox
# ‘Cheating’ (Remix) − John Newman

== Reviews == Bora Dagtekins zweite Kinoregiearbeit „Fack ju Göhte“ ist eine frech-witzige Komödie mit Lachern im Minutentakt, einer Prise Gefühl und glänzenden Darstellern – ganz in der Tradition seines Debüts „Türkisch für Anfänger“.
 Boro Dagtekins second theatrical feature ‘Fack ju Göthe’ is a cheeky and funny comedy with laughter every minute, a pinch of feeling and brilliant actors – entirely in the tradition of his debut ‘Türkisch für Anfänger’.|Filmstarts.de }}
 Klar gibt es hier und da ein paar Längen, und es scheint auch obligatorisch zu sein, dass ein deutscher Film ohne abschließende Versöhnung und Zusammenführung aller Charaktere nicht funktionieren kann. Doch wenn man bedenkt, wie genüsslich man in den 120 Minuten unterhalten wurde, fällt das nicht so sehr ins Gewicht.
 Sure, there are bits which drag on here and there. And it also seems obligatory that a German film without final reconciliation and coming together of all the characters wont work. However, when you consider how well you are entertained during the 120 minutes, that really doesnt matter much.|MovieMaze.de }}

== Awards == Bayerischen Filmpreis  
* 2014: Bambi Award for the best German film
* 2014: Goldene Leinwand  Bogey Award in Platin for 5 million views at the cinema in 50 days 
* 2014: Deutscher Comedypreis for the best comedy cinema movie

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 