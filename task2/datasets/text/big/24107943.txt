The Man in the Raincoat
{{infobox film
| name = The Man in the Raincoat
| image = The_man_in_the_raincoat_poster.jpg
| director = Julien Duvivier
| writer   = Julien Duvivier, René Barjavel, from the novel Tiger by the Tail by James Hadley Chase
| starring = Fernandel Bernard Blier Jacques Duby Jean Rigaux Claude Sylvain
| released = 22 February 1957
| country = France
| language = French
| runtime = 108 minutes
}}
The Man in the Raincoat  ( ) is a French-Italian comedy-thriller film directed by Julien Duvivier, scripted by the director and René Barjavel, from the 194 novel Tiger by the Tail by James Hadley Chase. It was released in 1957 and shown at the 7th Berlin International Film Festival in competition for the Golden Bear.    It stars Fernandel, (with whom Duvivier had made two successful Don Camillo films earlier in the 1950s), and Bernard Blier.

==Plot==
The wife of Albert Constantin  goes to visit her uncle, who is sick. Albert, (Fernandel), a clarinet player with the orchestra of the Théâtre du Châtelet finds himself alone for a week. Albert finds it hard to cope, being domestically inept, and his colleague in the orchestra, Émile, (Jean Rigaux), recommends he go to see Éva (Judith Magre). He, himself, sees her from time to time. At first hesitant, Albert goes to see the woman.

So much the worse for Albert. Éva is murdered, while he waits to see her in her living-room. Realising Éva is a prostitute he hurries away, only to read the next day of a murder and reports of a man running away, in a raincoat, from the scene of the crime.  He soon finds himself dealing with a blackmailer, a neighbour of the murdered woman, Monsieur Raphaël (Bernard Blier), and professional killers.  And so Albert is overtaken by a series of events that plunge him ever  deeper into troubles.

==Cast==
*Fernandel as Albert Constantin, clarinetist
*Bernard Blier as Monsieur Raphaël
*Jacques Duby as Maurice Langlois
*Jean Rigaux as Émile Blondeau
*Claude Sylvain as Florence
*Judith Magre as Éva
*John McGiver as OBrien
*Julien Bertheau as the director

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 


 
 