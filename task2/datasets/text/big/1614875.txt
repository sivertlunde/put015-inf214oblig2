The Rookie (2002 film)
 
 
{{Infobox film
| name           = The Rookie
| image          = Rookie02poster.jpg
| caption        = Theatrical release poster
| writer         = Mike Rich
| novel by       = Elmore Leonard Brian Cox
| director       = John Lee Hancock Mark Johnson
| music          = Carter Burwell
| cinematography = John Schwartzman
| editing        = Eric L. Beason
| studio         = Walt Disney Pictures Buena Vista Pictures
| released       =  
| runtime        = 127 minutes
| country        = United States
| language       = English
| budget         = $22 million
| gross          = $80.7 million
}}
 sports drama Brian Cox.

==Plot==
The film tells the story of Jim Morris, the son of a career Navy man, who moves the family to a small Texas town. Jim is shown to be a very skilled pitcher as a youth, though his father disapproves of Jims dream of making it to Major League Baseball. It is later mentioned that the town to which Jims family moved, Big Lake, has never cared for baseball. Thus, he was unable to play baseball in high school. He later gets a chance when he is drafted by the Milwaukee Brewers, but he tears up his shoulder, ending his hopes of achieving his lifelong dream.

Years later in 1999, Morris, married with three children, is a high school science teacher, as well as head baseball coach. His team, the Reagan County Owls, is very unsuccessful with many of his players skilled, but unmotivated, especially with very little community support. One day after practice, the team catcher offers to play catch with Morris. There it is revealed that Jimmy may still have his fastball, and it is soon displayed to the rest of the team. The Owls believe that Jimmy could possibly pitch in the major leagues and offer him a deal: If the Owls can win district and make the state playoffs, Jim will try out again, which Jim accepts. Furthermore, the team urges him to throw his fastball in batting practice, which immensely improves their hitting.
 Tampa Bay Devil Rays, and Jim goes, without telling his wife, afraid that her fear of him re-injuring his shoulder would keep him from going. After his tryout, Jim is told that his fastball is a staggering 98&nbsp;mph, which is abnormally higher than it was in his younger years (which Jim had estimated was 85–86&nbsp;mph). The lead scout tells Jim that, despite his advanced age (35), he could be signed to a minor-league deal. Jims wife finds out after getting two phone messages from the Tampa Bay scouts and she is at first reluctant to let Jim go, citing his home responsibilities, but after seeing how Jim is inspiring their son, Hunter (Angus T. Jones), she allows him to go. Jim tells his father, with whom he still has a cold relationship, of his situation, and his father once again tries to dissuade Jim from trying to achieve his dream again, telling Jim, "Its OK to think about what you want to do until its time to start doing what you were meant to do."
 Orlando Rays (now the Montgomery Biscuits) but quickly moves up to the AAA Durham Bulls. Concerned for his family due to mounting bills (the pay in the minor leagues being low) and unhappy that some of the organizations younger prospects view him as a publicity stunt and mock his age, Jimmy decides to give it up and come home. But his wife Lorri talks him out of it, not wanting Jim to give up again. Jim gets inspired again when he watches a Little League game one night, remembering the same love for baseball he had as a kid.

In September Jim is told that the Major League club has called him up, and that they will be playing in Texas against the Texas Rangers (baseball)|Rangers. Jim calls his family, who in turn informs the town. Advising his wife of the dress code in the majors, Jim finds his sports coat, a necktie and his St. Rita necklace hanging in his locker.  St. Rita is the saint of impossible dreams.  His family, high school players and many townspeople go to the game. Jim impresses many of the coaches in warm-ups with his fastball, and late in the game, with Tampa Bay losing badly, Jim is called into the game to pitch to Royce Clayton and end the inning. Jim ends up striking out Clayton on 3 straight fastballs. After the game, Jim gets interviewed by the press, being the oldest MLB rookie in over 40 years. During the interview, Jim notices his father had also come to the game. Jims father finally admits that he is proud of what Jim has done and also apologizes for never supporting him. Jim thanks him and gives him the ball with which he had gotten the strikeout, and the two finally repair their relationship. Jim then meets with his family and all the townspeople who had come to the game, applauding Jim on his amazing success story.

The final scene shows the Big Lake high school trophy case, which has Jims Major League jersey prominently displayed. It is then mentioned that Jim would go on to pitch in the majors for 2 seasons before retiring and returning to teaching in Texas.

==Cast==
* Dennis Quaid as Jim Morris
* Rachel Griffiths as Lorri Brian Cox as Jim Sr.
* Angus T. Jones as Hunter
* Angelo Spizzirri as Joel De La Garza (Owls catcher)
* Jay Hernandez as Joaquin Wack Campos
* Rick Gonzalez as Rudy Bonilla (Owls pitcher)

==Reception==
The movie received generally favorable reviews with Metacritic giving it a 72 out of 100 based on 31 reviews.

==Real life differences== Texas Rangers, Ballpark in ball (a pitch outside the strike zone, at which the batter does not swing), but it was later decided that the scene played better with only three pitches. Big Lake, San Angelo and commuted to work daily.
*The film shows Morris teaching chemistry, and a broadcaster calls him a chemistry teacher.  In reality, he taught physical science.
*The scene with the radar sign, which was copied by ESPN in a commercial with Bobby Valentine taking the part of Morris, never actually happened. Big Lake High School; in real life, the school is in the town of Big Lake but its actual name is Reagan County High School. Tampa Bay Texas Rangers uniforms worn in the movie are incorrect for the era in which the film takes place. This was a deliberate choice by the filmmakers.  By using the 2001 uniforms, rather than the period-accurate 1999 uniforms, the filmmakers were able to film second unit footage of an actual game between the Texas Rangers and the Tampa Bay Devil Rays and intercut that footage with footage of the actors wearing the same uniforms.
*The bullpen in which Jim warms up prior to his first major league appearance against the Texas Rangers is actually the Rangers bullpen; the visitors bullpen at Rangers Ballpark in Arlington was much less visible at the time. Steve Cox was the player called up with Jim. Brooks was a fictional character created for the film. Texas Rangers, but in 1999, he was really a member of the Seattle Mariners.

==Filming locations==
The Rookie was filmed almost entirely in North and Central Texas. Apart from scenes filmed at Rangers Ballpark in Arlington, locations included the following:
 Big Lake. Thorndale High Schools interior, exterior parts of the building and baseball field were used for Big Lake High Schools campus. Thorndales Main Street and downtown area was also used extensively in the film.

*Neighboring Thrall High School in Thrall, Texas, was dressed for several differing scenes, including scenes of several different "away" baseball games filmed on the schools field. Thralls then-recently completed football stadium stood in as Big Lakes. Thralls old football field, dressing rooms and recreation pavilion were dressed as an oil refinerys outlay in a deleted scene viewable on the DVDs special features.

*A scene shot in front of a motel supposedly in Florida was actually filmed in front of what is now a Best Western in Taylor, Texas.

Most of the population portrayed in this movie of Big Lake, Texas were fictional. Only the baseball team and those directly connected were based on real people.

==See also==
 
*2002 in film

==External links==
*  
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 