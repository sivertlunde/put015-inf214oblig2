Sachaai
{{Infobox film
| name           = Sachaai
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = K. Shankar
| producer       = M.C. Ramamurthy
| writer         = 
| screenplay     = K. Shankar
| story          = 
| based on       =  
| narrator       = 
| starring       = Shammi Kapoor Sanjeev Kumar Sadhana Shivdasani
| music          = Shankar Jaikishan
| cinematography = Sudhin Majumdar	
| editing        = K. Sankunny
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
}}
 Indian Bollywood film directed by K. Shankar and produced by M.C. Ramamurthy. It stars Shammi Kapoor, Sanjeev Kumar and Sadhana Shivdasani in pivotal roles.

==Plot==

Ashok and Kishore (Shammi Kapoor and Sanjeev Kumar respectively) are two room-mates, living in a hostel. While Ashok takes to crime and dishonesty, Kishore is honest to a fault. When Kishore finds that his dad (Rajan Mehra) has been taking bribes, he leaves home angrily. Ashok and he disagree on a number of issues, mainly involving honesty, and both agree to meet each other after a period of three years, and see what life has had an effect for them. Kishore gets into bad company inadvertently, and is unable to free himself, and gets deeper and deeper into crime. While Ashok realizes it is not worthwhile to pursue a criminal career, and becomes a police inspector. Kishore is now known as the notorious Baghi Sitara, and Ashok is assigned the task of apprehending him. The two do not know who the other is. Kishore comes to know of this assignment and his men kidnap Ashok, but Ashok manages to escape. Kishore must kill Ashok in order to carry out his nefarious activities. After three years the two have an emotional meeting, delighted to see other. What will be the outcome when they come to know that they each others mortal enemies?

==Cast==
* Shammi Kapoor...Ashok
* Sanjeev Kumar...Kishore Dayal
* Sadhana Shivdasani...Shobha Dayal
* Pran (actor)|Pran...Prakash Johnny Walker...Chaman
* Helen (actress)|Helen...Meena
* Sulochana Latkar...Mrs. Din Dayal
* Raj Mehra...Din Dayal

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Sau Baras Ke Zindagi Se"
| Mohammed Rafi, Asha Bhosle
|-
| 2
| "Ae Dost Mere Maine Duniya Dekhi Hai"
| Mohammed Rafi, Manna Dey
|-
| 3
| "More Saiyan Pakde Baiyan"
| Asha Bhosle
|-
| 4
| "Kabse Dhari Hai Samne Botal Sharab Ki"
| Mohammed Rafi, Asha Bhosle
|-
| 5
| "Beet Chali Haye Ram"
| Asha Bhosle
|-
| 6
| "Mere Gunaah Maaf Kar"
| Mohammed Rafi
|}

==External links==
* 

 
 
 
 
 

 