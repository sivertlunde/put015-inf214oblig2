Weary River
{{Infobox film
| name           = Weary River
| image          = Weary_River_Poster.jpg
| border         = yes
| caption        = Theatrical release poster
| director       = Frank Lloyd
| producer       = Richard A. Rowland
| screenplay     = Screen version Bradley King
| story          = Courtney Riley Cooper
| starring       = {{Plainlist|
* Richard Barthelmess
* Betty Compson
}}
| music          = Musical score & Vitaphone Symphony Orchestra conducted by Louis Silvers
| cinematography = Ernie Haller Special photography Alvin Knechtel
| editing        = Edward Schroeder & James Gibbon A First National Vitaphone Picture
| distributor    = Warner Bros.
| released       =  
| runtime        = 86 minutes (8 reels)
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Weary River is a 1929 American romantic drama film directed by Frank Lloyd and starring Richard Barthelmess, Betty Compson, and William Holden.    Produced by First National Pictures and distributed through Warner Brothers, the film is a part-talkie, part-silent hybrid made at the changeover from silent movies to sound movies. Based on a story by Courtney Riley Cooper, the film is about a gangster who goes to prison and finds salvation through music while serving his time. After he is released and falls back into a life of temptation, he is saved by the love of a woman and the warden who befriended him. The film received an Academy Award nomination for Best Director in 1930.   

Weary River was preserved at the Library of Congress and restored by the LoC, UCLA Film & TV and Warner Brothers. It is now available on DVD directly from the Warner Archive Collection.    

==Plot==
Jerry Larrabee (Richard Barthelmess) is framed by rival gangster Spadoni (Louis Natheaux) and sent to prison, where he is befriended by a kind and understanding warden (William Holden). Through the wardens patient influence, Jerry becomes interested in music and forms a prison band, broadcasting over the radio. Jerrys singing deeply moves his radio listeners and soon Jerry is given a pardon by the governor. 

Jerry pursues a singing career in vaudeville, billing himself as the Master of Melody, but constant whispers of "Convict!" from the audience disturb his concentration. Moving from job to job, Jerry is haunted by his past. With no hope of succeeding in music, Jerry returns to his old gang and takes up with his former sweetheart, Alice Gray (Betty Compson). As he prepares for a final confrontation with Spadoni, Alice gets in touch with the warden, who arrives on the scene in time to keep Jerry on the straight and narrow path. Jerry eventually becomes a radio star and marries Alice.

==Cast==
  
* Richard Barthelmess as Jerry Larrabee
* With Betty Compson as Alice Gray
 
* William Holden as Warden
* Louis Natheaux as Spadoni George Stone as Blackie
* Raymond Turner as Elevator Boy
* Gladden James as Manager
 

==References==
 

==External links==
*  
*  
*  
*   at Virtual History

 

 
 
 
 
 
 
 
 