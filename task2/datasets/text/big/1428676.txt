Kes (film)
 
 
{{Infobox film
| name           = Kes
| image          = Kes 1969 film poster.jpg
| image_size     = 215px
| alt            =
| caption        = UK theatrical release poster
| director       = Ken Loach
| producer       = Tony Garnett
| screenplay     = Barry Hines Ken Loach Tony Garnett
| based on       =   David Bradley Freddie Fletcher Lynne Perrie Colin Welland Brian Glover John Cameron
| cinematography = Chris Menges
| editing        = Roy Watts
| studio = Woodfall Film Productions
| distributor    = United Artists
| released       =  
| runtime        = 112 minutes   
| country        = United Kingdom
| language       = English
| budget = £157,000 Alexander Walker, Hollywood, England, Stein and Day, 1974 p378 
}} list of the 50 films you should see by the age of 14.

==Plot==

The film focuses on 15-year-old Billy Casper, who has little hope in life and is Bullying|bullied, both at home by his physically and verbally abusive older half-brother, Jud, and at school. He is constantly held to account for some prior run-ins with the police, although he insists that his mischief is behind him. Yet we see evidence of his mischievous side as he carries out his morning newspaper delivery, stealing eggs and milk from milk floats. He has difficulty paying attention in school and is often provoked into tussles with classmates. Billy comes across as an emotionally neglected boy with little self-respect. Billys father has left the family some time ago, and his mother refers to him in the film as a "hopeless case." 
 PE teacher taking part in a football game, fantasising about himself as Bobby Charlton and commentating on the match.

Outside cadging money and daydreaming at school, Billy has no positive interests. His greatest fear is ending up working down the pit as a coal miner, but he has no apparent escape route until he finds an outlet through training a kestrel that he takes from a nest on a farm. His interest in learning falconry prompts Billy to steal a book on the subject from a secondhand book shop, as he is underage and cannot be given a borrowers card from the public library.

As the relationship between Billy and "Kes", the kestrel, improves during the training, so does Billys outlook and horizons. For the first time in the film, Billy receives praise, from his English teacher after delivering an impromptu talk on his relationship with the bird.

Jud leaves money and instructions for Billy to place a bet on two horses, but, after consulting a bettor who tells him the horses are unlikely to win, Billy spends the money on fish and chips and on meat for his bird. However, the horses do win (meaning Jud would have won over Pound sterling|£10 if Billy had placed the bet). Furious at Billy and unable to find him, Jud takes revenge by killing his kestrel, whose body Billy retrieves from the trash bin. After showing the kestrel to Jud and his mother, in grief and rage, Billy buries his kestrel on the hillside overlooking the field where hed flown.

==Cast==
  David Bradley as Billy Casper
* Freddie Fletcher as Jud
* Lynne Perrie as Mrs Casper
* Colin Welland as Mr Farthing
* Brian Glover as Mr Sugden
* Bob Bowes as Mr Gryce
* Bernard Atha as Youth employment officer
* Joey Kaye as Pub comedian
* Bill Dean as Fish and Chip Shop Man
* Geoffrey Banks as Mathematics teacher
* Duggie Brown as Milkman
* Harry Markham as Newsagent

==Production==
Both the film and the book provide a portrait of life in the mining areas of Yorkshire of the time, reportedly the miners in the area were then the lowest paid workers in a developed country.  The film was shot on location, including in St. Helens School, Athersley South, later renamed Edward Sheerien School (demolished in 2011); and in and around the streets of Lundwood.

Set in Barnsley, the film contains broad local dialects. The cast have authentic Yorkshire accents and used or knew the dialects. The extras were all hired from in and around Barnsley. The DVD version of the film has certain scenes dubbed over with fewer dialect terms than in the original.    In a 2013 interview, director Ken Loach said that, upon its release, United Artists organised a screening of the film for some American executive and they said that they could understand Hungarian better than the dialect in the film. 
 Family Life and The Save the Children Fund Film.

==Certification== at a time when the only other certificates were Adult and X.  Three years later, Stephen Murphy, the BBFC Secretary, wrote in a letter that it would have been given the new Advisory certificate under the system then in place. {{cite web
 | last =
 | first =
 | authorlink =
 | title =Correspondence from Stephen Murphy on the certification of Kes
 | work =
 | publisher =
 | date =
 | url =http://www.bbfc.co.uk/sites/default/files/attachments/KESfile_38-39v2%20FINAL.pdf 
 | format =
 | doi =
 | accessdate = 23 August 2014
 | archiveurl = 
 | archivedate = }}   Murphy also argued that the word "bugger" is a term of affection and not considered offensive in the area that the film was set.  In 1987, the VHS release was given a PG certificate on the grounds of "the frequent use of mild language", and the film has remained PG since that time. {{cite web
 | last =
 | first =
 | authorlink =
 | title =BBFC Case Studies - Kes
 | work =
 | publisher =BBFC
 | date =
 | url =http://www.bbfc.co.uk/case-studies/kes 
 | format =
 | doi =
 | accessdate = 23 August 2014
 | archiveurl = 
 | archivedate = }} 

In the original undubbed version of the film, there is one case of the word "fucking" towards the end of the film although, as this takes place during an argument, it is not clear.  In the dubbed version, which is more widely available, this is replaced by the word "bloody".

The novel contains more and stronger profanity than does the film. 

==Reception==
The film was a word of mouth hit in Britain, eventually making a profit. However it was a complete commercial flop in the US.  In his four star review, Roger Ebert said that the film failed to open in Chicago, and attributed the problems to the Yorkshire accents.   film review by Roger Ebert, Chicago Sun Times, 16 January 1973   Ebert saw the film at a 1972 showing organised by the Biological Honor Society in Loyola, which led him to ask, "were they interested in the movie, or the kestrel?" 

The film has universal acclaim and currently holds a score of 100% on Rotten Tomatoes.

==Home media==
A digitally restored version of the film was released on DVD and Blu-ray by The Criterion Collection in April 2011. The extras feature a new documentary featuring Loach, Menges, producer Tony Garnett, and actor David Bradley, a 1993 episode of The South Bank Show with Ken Loach, Cathy Come Home (1966), an early television feature by Loach, with an afterword by film writer Graham Fuller, and an alternate, internationally released soundtrack, with postsync dialogue. 

==Awards==
* 1970: Karlovy Vary International Film Festival – Crystal Globe 
* 1971: Writers Guild of Great Britain|Writers Guild of Great Britain Award – Best British Screenplay   Retrieved June 2008. 
* 1971: British Academy Film Awards  Best Actor in a Supporting Role – Colin Welland
:* Most Promising Newcomer to Leading Film Roles – David Bradley

==See also==
 
* BFI Top 100 British films
* BFI list of the 50 films you should see by the age of 14
*  by Graham Fuller.

==References==
 

==Literature==

*  

*Till, L. & Hines, B. (2000). Kes: Play, London: Nick Hern Books. ISBN 978-1-85459-486-0

==External links==
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 