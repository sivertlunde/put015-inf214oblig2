Murder in the First (film)
{{Infobox film
| name           = Murder in the First
| image          = Murder in the first ver1.jpg
| caption        = Theatrical poster
| director       = Marc Rocco
| producer       = Marc Frydman Mark Wolper Dan Gordon
| starring       = Christian Slater Kevin Bacon Gary Oldman Embeth Davidtz Brad Dourif William H. Macy R. Lee Ermey
| music          = Christopher Young Fred Murphy
| editing        = Russell Livingstone Le Studio Canal+
| distributor    = Warner Bros.
| released       = January 20, 1995
| runtime        = 122 minutes
| country        = United States
| language       = English
| budget         = $23 million
| gross          = $17,381,942
}}
 1995 film, murder in the first degree. The film also stars Christian Slater and Gary Oldman.

==Plot== federal offense. Leavenworth Penitentiary, Kansas. After later being transferred to Alcatraz Federal Penitentiary|Alcatraz, he participates in an escape attempt with two other prisoners.
 the hole" which is in Alcatrazs dungeons. Except for 30 minutes on Christmas Day in 1940, he is left in there for three years. The solitary confinement causes Young to lose his sanity. On release back to the general population, he experiences a psychotic episode in the prison cafeteria and attacks McCain, stabbing him to death with a spoon in full view of the prison staff and the other convicts.

Young is put on trial in San Francisco for first degree murder in what prosecutors and the public-defenders office believe is an open-and-shut case. Public defender James Stamphill (Christian Slater), a recent graduate of Harvard Law School, is given the case. After discovering the facts of Youngs case, Stamphill attempts to put Alcatraz on trial by alleging that its harsh conditions caused his client to commit murder. The trial becomes highly politicized and contentious. Eventually Young is convicted of involuntary manslaughter, not first degree murder. He is returned to Alcatraz where he subsequently dies. The film concludes that The Rocks associate warden Milton Glenn (Gary Oldman), is convicted of mistreatment and is banned from working in the US penal system.

==Production notes==
Because the producers wanted authenticity, co-stars Bacon, Slater and Oldman spent some of their free time locked in jail cells while the movie was being filmed. Bacon lost twenty pounds for his role as Henri Young. 

Filming during one of the courtroom scenes was interrupted by the 1994 Northridge earthquake. 

==Historical reality==
The film makes numerous changes to historical events. The real Henri Young was not convicted of stealing $5 to save his sister from destitution. He had been a hardened bank robbery| bank robber who had taken a hostage on at least one occasion and had committed a murder in 1933. Young was also no stranger to the penal system. Before being incarcerated at Alcatraz in 1936, he had already served time in two state prisons in Montana and Washington (U.S. state)|Washington. In 1935 he spent his first year in federal correctional facilities at McNeil Island, Washington before being transferred to Alcatraz.
 Walla Walla to begin a life sentence for the murder conviction in 1933.

In 1972 after Young was released from Washington State Penitentiary at age 61, he jumped parole. According to Washington State authorities his whereabouts remain unknown. Young was born in 1911; if still alive as of  , he would be about   years old.

According to   for over 3 years. This is taken directly from the paper, "Emphasis which they repeatedly laid on the fact that Young was in isolation or solitary confinement for more than three years—and that he drove his knife into McCain’s abdomen just eleven days after release form such confinement, made it clear that the defense hopes to show not only that Young was “punch-drunk” but that the punches were administered by the Alcatraz “system.”

Many of the ideas in the movie were taken directly from newspaper articles of the trials, including the ending scene where the jury only convicts Young of manslaughter, and requests that Alcatraz be investigated. 

==Cast==
*Kevin Bacon – Henri Young
*Christian Slater – James Stamphill
*Gary Oldman – Warden Milton Glenn
*Embeth Davidtz – Mary McCasslin
*William H. Macy – D.A. William McNeil
*Stephen Tobolowsky – Mr. Henkin
*Brad Dourif – Byron Stamphill
*R. Lee Ermey – Judge Clawson
*Mia Kirshner – Rosetta Young Dial
*Ben Slack – Jerry Hoolihan
*Stefan Gierasch – Warden James Humson
*Kyra Sedgwick – Blanche, Hooker
*Alex Bookston – Alcatraz Doctor
*David Michael Sterling – Rufus McCain Arthur "Doc" Barker

==Reception==
Murder in the First received mixed reviews from critics, as the movie currently holds a rating of 50% on Rotten Tomatoes based on 36 reviews.

== References ==
 

==External links==
 
* 
*  
*  
*  
* 
* http://www.sfgenealogy.com/sf/history/hgoe08.htm

 

 
 
 
 
 
 
 
 
 
 
 
 