La Madre María
{{Infobox film
| name           =La Madre María
| image          =
| image_size     =180px
| caption        =Tita Merello in the film
| director       =
| producer       =
| writer         =
| narrator       =
| starring       =
| music          =
| cinematography =
| editor       =
| distributor    =
| released       = 1974
| runtime        =
| country        = Argentina Spanish
| budget         =
}}
 1974 Argentina|Argentine film.

==Cast==
 

==Plot==
 healer (1854-1928). The film begins with an old Madre Maria on trial for quackery and deceit. On a series of flashbacks, her life is told, starting with a visit María paid to another famous Argentina faith healer, Pancho Sierra.

Pancho Sierra instructs Madre María to continue his work by helping the poor and praying with the sick. After her husband’s death, Madre María establishes a mission or co-op in La Rioja Street in Buenos Aires. She teaches women how to sew, delivers food to the hungry, pays the debts of the poor. She prays with the sick and teaches a simple gospel of faith in God.

After the medical establishment begins to use the police and the judicial system to stop her work, María is exiled in Turdera, near Buenos Aires, where she starts her work among the poor and the sick one more time. She starts an orphanage and defends the workers from police brutality.

Maria’s defense prevails in the trial, and a cheering crowd receives her in her home, but she faints and shortly after dies. The final scene shows a solemn funeral procession taking María’s casket to the cemetery.

== See also ==
* Faith healing

==External links==
*  

 
 
 
 

 