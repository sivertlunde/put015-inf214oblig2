Adventurous Youth
 
 
{{Infobox film
| name           = Adventurous Youth
| image          =
| caption        =
| director       = Edward Godal 
| producer       = Edward Godal
| writer         = Edward Godal
| starring       = Derrick De Marney  Renee Clama   Dino Galvani   Sybil Wise
| music          = Warner Bros
| cinematography = 
| editing        = 
| studio         = Pall Mall Productions
| distributor    = Warner Brothers
| released       = 1928
| runtime        = 54 minutes 
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         = 
| preceded_by    =
| followed_by    =
}} silent adventure film directed by Edward Godal and starring Derrick De Marney, Renee Clama and Dino Galvani. It depicts an Englishman (Derrick de Marney) who was  voluntarily caught up during the Mexican Revolution in order to save the village where he is working for from being sacked and destroyed. It was made as a quota quickie and distributed in United States by Warner Brothers.

==Cast==
* Derrick De Marney as The Englishman 
* Renee Clama as  Mary Ferguson
* Dino Galvani as Don Esteban 
* Sybil Wise as The Vamp 
* Loftus Tottenham as Mr. Ferguson 
* Julius Kantorez as Father OFlannigan  Harry Bagge   
* Lionel dAragon   
* Harry Peterson

==References==
 

==Bibliography==
* Chibnall, Steve. Quota Quickies: The Birth of the British B film. British Film Institute, 2007.
* Wood, Linda. British Films, 1927-1939. British Film Institute, 1986.

==External links==
* 
*  

 
 
 
 
 
 
 
 
 
 


 
 