Vamsha Vriksha
{{Infobox film
| name           = Vamsha Vriksha
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = 
| director       = B. V. Karanth Girish Karnad
| producer       = M. N. Basavarajaiah
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       =  Vishnuvardhan
| music          = Bhaskar Chandavarkar
| cinematography = U. M. N. Sharief
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 166 minutes
| country        = India
| language       = Kannada
| budget         = 
| gross          = 
}}
 genalogy tree), National Film Best Direction. Filmfare Award south in 1972. Vishnuvardhan  and actress, Uma Shivakumar. The film was later remade into Telugu as Vamsa Vruksham, starring Anil Kapoor   

==Synopsis==

The story starts in 1924 with Kathyayani, widow of Nanjunda Shrothri, who lives with her in-laws Shrinivasa Shrothri and Bhagirathamma and their servant, Lakshmi, in Nanjangud. After the loss of her husband, Kathyayani did not take to a widow’s garb as her mother-in-law wished because her father-in-law thought it would be too much for the young woman to bear. Shrothri, a scholar in Indian philosophy and the shastras, is occasionally visited by priests and professors for his opinion on an intricate problem in the scriptures. In one such event, Sadashiva Rao, a college lecturer in Mysore, visits Shrothri to discuss some matters of Indian philosophy. Listening to the two men talk, Kathyayani’s thoughts turn to college. Despite a little protest from Bhagirathamma, Shrothri encourages his daughter-in-law to pursue her BA degree, which Nanjunda had been studying for before he had drowned in the nearby Kapila river. At the university, she meets the brother of Sadashiva Rao, Raja Rao, professor of Shakespearean drama and organiser of the Dramatics Club, with whom she falls in love.

Simultaneously, Sadashiva Rao embarks on a massive five-volume project that would detail the cultural history of India from the days of the Indus Valley Civilisation to present times. With an initial grant from Shrothri of Rs. 1,000 and funding from the Maharaja of Mysore, Sadashiva Rao embarks on a tour of India to visit various libraries, monuments, and archives to conduct his research. During his visit to Ajanta, he meets with a Sinhalese couple and their daughter, Karunaratne, a historian of Buddhism from Cambridge. Rao is enchanted by the woman’s sharp mind and dedication to her research, and invites her to continue her studies at his university. Ratne takes Rao up on his offer as she has some admiration for his work, and over the course of her doctoral degree and work with him as his research assistant, falls in love with him.

Sadashiva Rao, already married to Nagalakshmi, is concerned that society will see his interaction with Ratne, which by now had become quite informal, as improper. He is also frustrated with his wife for not being able to take part in his intellectual life, which was all that mattered to him. Rao offers to marry Ratne despite his earlier marriage and live in a bigamous relationship. Ratne is hesitant at first, but only for a short while. She agrees, and the two get married. Needless to say, Nagalakshmi, a dutiful and loving wife, is devastated. Around the same time,  Rao’s younger brother, Raja Rao marries Kathyayani. The widow wrestles with the notion of remarriage, especially as she is aware of the dishonour it will bring to the Shrothri family, but ultimately gives in to the material pleasures of a married life. Unable to face her dharmic father-in-law, she writes him a letter and departs from the house with just the clothes on her back. The Shrothri family is shocked, even more so as Kathyayani has abandoned even her four-year-old son for Raja Rao, but Shrothri goads them back to some level of equanimity after the catastrophe.

As the years pass, Nagalakshmi, despondent, finds peace in religion, and the old Shrothris continue the burden of domestic life, denied the pleasures of retirement, first by the death of their son and then by the abandonment by their daughter-in-law. Kathyayani, after the initial enjoyment of marital bliss, suffers three miscarriages and wonders if her fate might not be in some way a divine judgement on her actions. Similarly, Rao falls sick as he churns out volume after volume of his magnum opus. Ratne, who had framed her marriage to Rao in intellectual terms, loses both her parents to old age and begins to feel the pangs of motherhood. Unfortunately for her, Rao is quite weak, and any time spent on her pregnancy and maternity leave might mean that their five-volume project may remain incomplete.

One day, Kathyayani suddenly comes across her son in her English class – after her bachelor’s, she had studied for an MA and become a teacher along with her new husband. Unable to restrain herself, she invites him home and inquires about his family. When he tells her that both his parents are dead, Kathyayani feels his rejection more keenly than had the boy hurled insults at her or demanded an explanation from her. Her health deteriorates due to her mental anguish, much to the worry of her doting husband. Rao also hovers near death as the strain of his work wears him down, as does the guilt of abandoning his first wife, Nagalakshmi.

In the meanwhile, sadness once again descends upon the Shrothri house with the passing of Bhagirathamma. Battered by fate, Shrothri barely manages to hold on to his mental calm. He decides that it is time he took sanyasa and begins to put his affairs in order. He arranges for his grandson to be married to the daughter of a  respected Sanskrit teacher in town, and bequeaths two acres of land to Lakshmi, who had served the Shrothri family from her father’s days. In the midst of sorting out his papers, Shrothri finds an old letter addressed to his father, cursing the man for cheating his brother out of his share of the ancestral property. Shocked, the old man investigates the accusation. As an old tale of greed, jealousy, and other human weaknesses unfolds, Shrothri is aghast to find that his father was not Nanjunda Shrothri, but some priest who had been chased off after impregnating his mother. This had been arranged so that Nanjunda Shrothri Sr. would have an heir to whom he could pass the ancestral property to, without having it pass to his brother. Shrinivasa Shrothri decides to track down his uncle’s descendants, but when any fail to turn up, he gives away all his property and wealth to needy people in the village. His grandson, Shrinivasa Shrothri Jr., is supportive of the decision, as he too would not like to taint himself with ill-gotten wealth.

In the closing act, Sadashiva Rao, racked with guilt, returns to live with both his wives under one roof. Within a day of his having made his peace, he passes away. He had just finished his five-volume work. Ratne, when she raises the topic of Rao’s research with his son (from his first wife), Prithvi, informs her that he doesn’t care for history or philosophy and that he is a science student. Shrothri leaves for his sanyas, but decides to visit his daughter-in-law to see her one last time – he hadn’t seen her since she left around 20 years ago. As he enters the Rao house, Kathyayani, no longer her radiant, beautiful, self but a pale emaciated shell, is in her final moments. Shrothri sends for her son, his grandson, and is shocked to find out that the young man knew of his mother but had rejected her – he had hoped that the junior Shrothri would have been raised better. While they await the grandson’s arrival, the old man is surprised to find out that Nagalakshmi’s aunt was his wronged uncle’s daughter. Shrothri Jr., or Cheeni as his mother called him so many years ago, enters, and as he tries to pour some water down his mother’s throat, she too passes away. Ratne has made preparations to return home to Ceylon, and Shrinivasa Shrothri finally leaves for his sanyas, telling his grandson that his first duty now was to perform the death rituals for his mother.

==Awards==
* National Film Awards 1971
** Best Kannada Film
** Best Direction – Girish Karnad, B V Karanth
* Karnataka State Film Awards 1971-72
**Best Film
**Best Actor – Venkatarao Thalgeri
**Best Actress – L V Sharada
**Best Story Writer – S L Bairappa
**Best Dialogue Writer – Girish Karnad, B V Karanth
**Best Editing – Aruna Desai

* Filmfare Award South 1972
**Best Film
**Best Actor – Venkatarao Thalgeri
**Best Direction – Girish Karnad, B V Karanth

==Notes==
 

==References==
 
* 
 

==External links==
*  

 
 
 
 
 
 
 