The Wonderful Lies of Nina Petrovna
{{Infobox film
| name           = The Wonderful Lies of Nina Petrovna
| image          = 
| image_size     = 
| caption        = 
| director       = Hanns Schwarz
| producer       = Erich Pommer
| writer         = Hans Székely   Fritz Rotter
| narrator       = 
| starring       = Brigitte Helm   Francis Lederer   Warwick Ward   Lya Jan
| music          = Maurice Jaubert   Willy Schmidt-Gentner 
| editing        = 
| cinematography = Carl Hoffmann   Hans Schneeberger 
| studio         = Universum Film AG
| distributor    = Universum Film AG
| released       = 15 April 1929
| runtime        = 102 minutes
| country        = Germany
| language       = German
| budget         = 
| gross          = 
| preceded_by    = 
| followed
}} silent drama sound began with Melody of the Heart.  The film premiered on 15 April 1929 at the Ufa-Palast am Zoo in Berlin.  It was amongst the most popular films released in Germany that year. 

It was remade in France in 1937 as The Lie of Nina Petrovna.

==Synopsis== mistress of the influential Colonel Beranoff. She chooses to give up the luxurious existence that Beranoff offers her, in order to live with the much poorer Rostof. Although they can not even pay their electricity bills on time they are both incredibly happy.
 honourable way out. When Petrovna is informed, in order to save her lovers career and life, she agrees to abandon Rostof and go back to Beranoff. The Colonel in turn agrees to destroy the incriminating evidence. To hide the real reason from him, Petrovna pretends to abandon Rostof because he cannot afford to supply her with expensive clothes and jewels. Rostof is left heartbroken, while Petrovna is secretly anguished.

At the end of the film, as his regiment rides out of St. Petersburg, Rostof symbolically ignores a rose thrown to him from her balcony by Petrovna. Shortly afterwards when Beranoff arrives to call on Petrovna in expectation of resuming their relationship, he discovers she has killed herself.

==Cast==
* Brigitte Helm as Nina Petrovna 
* Francis Lederer as Lt. Michael Rostof 
* Warwick Ward as Col. Beranoff 
* Lya Jan as Bauernmädchen 
* Harry Hardt   
* Ekkehard Arendt   
* Michael von Newlinsky   
* Franz Schafheitlin

==References==
 

==Bibliography==
* Bergfelder, Tim & Bock, Hans-Michael. The Concise Cinegraph: Encyclopedia of German. Berghahn Books, 2009.
* Hardt, Ursula. From Caligari to California: Erich Pommers Life in the International Film Wars. Berghahn Books, 1996.
* Prawer, S.S. Between Two Worlds: The Jewish Presence In German And Austrian Film, 1910-1933. Berghahn Books, 2005.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 


 
 