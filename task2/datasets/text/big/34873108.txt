Mahallada duv-duv gap
{{Infobox film
| name = Mahallada duv-duv gap/Маҳаллада дув-дув гап
| image = Mahallada_duv-duv_gap.jpg
| alt=
| caption = A screenshot from the movie (in Russian)
| director = Y. Stepchuk 
| producer = Shuhrat Abbosov
| writer = O. Razamzonov   B. Rest
| starring =  
| music = M. Leviyev
| cinematography = 
| editing = I. Gordon
| studio = Uzbekfilm
| distributor = 
| released =  
| runtime = 80 minutes
| country = Uzbek SSR Uzbek and Russian
| budget = 
| gross = 
}}
 Uzbek comedy film|comedy. The film was directed by Y. Stepchuk and produced by Shuhrat Abbosov, a prominent Uzbek filmmaker.    Mahallada duv-duv gap is considered to be one of the best Uzbek films of all time   and Shuhrat Abbosov, who received a National Artist of the USSR award for his works, is celebrated as one of the founders of the Uzbek film making industry.     

==Plot==
The events in Mahallada duv-duv gap occur in a mahalla — a traditional Uzbek neighborhood — in an old part of Tashkent at a time when big-scale construction works are taking place. The movie humorously depicts the relationships between traditional parents and their modern children. 

==Script==
The script for the film was originally written in Russian language|Russian. However, Shuhrat Abbosov wanted to make the film in the Uzbek language. Just one day before the start of production, he asked the renowned Uzbek author Abdulla Qahhor to translate the script. Qahhor was in hospital at the time. He translated the dialogues in the script in two hours sitting on a bench in front of the hospital.    Abdulla Qahhor is credited as a translator and an editor in the movie.

==References==
 

 

 
 
 
 
 
 
 

 
 