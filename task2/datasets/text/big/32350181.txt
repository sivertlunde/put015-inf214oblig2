Twinkletoes
{{Infobox film
| name        = Twinkletoes
| image       = Twinkletoes.jpg John McCormick
| writer      = Winifred Dunn (scenario) Mervyn LeRoy (Comedy const) (screenplay)
| starring    = Colleen Moore Kenneth Harlan Tully Marshall Gladys Brockwell
| director    = Charles Brabin
| studio      = First National
| distributor = First National
| released    =  
| runtime     = English intertitles
| country     = United States
| music       =
| budget      =
}}
 1926 silent silent film John McCormick with the couple distributing through Moores resident studio First National. This film is one of Moores surviving films from the late silent era and is available on DVD. 

==Cast==
* Colleen Moore – Twinkletoes
* Kenneth Harlan – Chuck Lightfoot
* Tully Marshall – Dad Minasi
* Gladys Brockwell – Cissie Lightfoot
* Lucien Littlefield – Hank
* Warner Oland – Roseleaf
* John Kolb – Bill Carsides (aka John Philip Kolb)
* Julanne Johnston – Lilac
* William McDonald – Police Inspector Territon Dorothy Vernon – ?
* Ned Sparks – ?
* Dick Sutherland – ?
* Carl Stockdale – ?
* Aggie Herring – ?
* Harold Lockwood, Jr. – ? (*son of the silent star who died in 1918)

==Background==
This was Colleens first "serious" film since her portrayal of Salena Peake in So Big. It came between comedies, as it was the studios strategy to stagger her comedies and dramatic roles, so that the public would not become tired of any single genre.

==Story==
Monica "Twinkletoes" Minasi, a motherless child of the London Limehouse district, is a brilliant young dancer who lives in poverty. She saves a crowd from abuse by the police through an impromptu performance, during which she meets Chuck Lightfoot, a champion fighter and older married man whose wife, Cissie, was the cause of the ruckus. Twinks finds herself slowly falling in love with Chuck but resists, because he is married and much older (he is in his late twenties, she might be as young as 15), but when he saves her from an attack one night she realizes that it is useless to fight her feelings.

She dances at the head of the "Quayside Kids," a local dance group in a music hall run by Roseleaf, who has designs on the young girls that dance for him. Chucks wife Cissie realizes that her husband had feelings for Twinks and, learning that Twinks dad is a burglar, exposes him to the police. Twinks is distraught when she learns the news that her father—whom she admired above all other people—is a criminal. Roseleaf takes her to his apartment and attempts to have his way with her, but she manages to escape. Cissie is killed in an accident, and, in despair, Twink throws herself into the river. She is rescued by Chuck and in his arms finds something to live for.

==Details== Thomas Burke. So Big. Two endings were filmed and exhibitors were allowed to pick which version they wanted to show.  The audiences generally preferred the sad ending.

Winifred Dunn did research in San Franciscos Chinatown for the atmosphere of the film  Coleen danced several of the numbers in the movie herself. 

==References==
 

==Footnotes==
*Jeff Codori (2012), Colleen Moore; A Biography of the Silent Film Star,  ,(Print ISBN 978-0-7864-4969-9, EBook ISBN 978-0-7864-8899-5).

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 