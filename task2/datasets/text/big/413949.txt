Peeping Tom (film)
 
 
{{Infobox film
| name           = Peeping Tom
| image          = Peepingtomposter.jpg
| image_size     = 220px
| border         = yes
| alt            =
| caption        = Original film poster
| director       = Michael Powell
| producer       = Michael Powell
| writer         = Leo Marks Carl Boehm Moira Shearer Anna Massey Maxine Audley Pamela Green
| music          = Brian Easdale
| cinematography = Otto Heller
| editing        = Noreen Ackland
| distributor    = Anglo-Amalgamated|Anglo-Amalgamated Film Distributors
| released       =  
| runtime        = 101 minutes  
| country        = United Kingdom
| language       = English
| budget         = £125,000 John Hamilton, The British Independent Horror Film 1951-70 Hemlock Books 2013 p 77-84 
}} cryptographer and polymath Leo Marks. The title derives from the slang expression peeping Tom describing a voyeur. The film revolves around a serial killer who murders women while using a portable movie camera to record their dying expressions of terror.

The films controversial subject and the extremely harsh reception by critics effectively destroyed Powells career as a director in the United Kingdom. However, it attracted a cult following, and in later years, it has been re-evaluated and is now considered a masterpiece.      
 music score, Gordon Watson.

==Plot==
 
Mark Lewis meets a prostitute, covertly filming her with a camera hidden under his coat. Shown from the point of view of the camera viewfinder, tension builds as he follows the woman into her home, murders her and later watches the film in his den as the credits roll on the screen.

Lewis is a member of a film crew who aspires to become a filmmaker himself. He also works part-time photographing soft-porn pin-up pictures of women, sold under the counter. He is a shy, reclusive young man who hardly ever socialises outside of his workplace. He lives in the house of his late father, renting most of it via an agent, while posing as a tenant himself. Helen, a sweet-natured young woman who lives with her blind mother in the flat below his, befriends him out of curiosity after he has been discovered spying on her 21st birthday party.
 guinea pig for his fathers psychological experiments on fear and the nervous system. Marks father would study his sons reaction to various stimuli, such as lizards he put on his bed and would film the boy in all sorts of situations, even going as far as recording his sons reactions as he sat with his mother on her deathbed. He kept his son under constant watch and even wired all the rooms so that he could spy on him. Marks fathers studies enhanced his reputation as a renowned psychologist.

Mark arranges with Vivian, a stand-in at the studio, to make a film after the set is closed; he then kills her and stuffs her into a prop trunk. The body is discovered later during shooting by a female cast member who has already antagonised the director by fainting for real at points which are not in the script. The police link the two murders and notice that each victim died with a look of utter terror on her face. They interview everyone on the set, including Mark, who always keeps his camera running, claiming that he is making a documentary.

Helen goes out to dinner with Mark, even persuading him to leave his camera behind for once. Her mother finds his behaviour peculiar, aware how often Mark looks through Helens window. Mrs. Stephens is waiting inside Marks flat after his evening out with her daughter. She senses how emotionally disturbed he is and threatens to move, but Mark reassures her that he will never photograph or film Helen. 

A psychiatrist is called to the set to console the upset star of the movie. He chats with Mark and is familiar with Marks fathers work. The psychiatrist relates the details of the conversation to the police, noting that Mark has "his fathers eyes." Mark is tailed by the police who follow him to the newsagents where he takes photographs of the pin-up model Milly (two versions of this scene were shot; the more risqué version is credited as being the first female nude scene in a major British feature, although even on the racier version, Milly only exposes one breast for a few seconds). Slightly later, it emerges that Mark must have killed Milly before returning home.

Helen, who is curious about Marks films, finally runs one of them. She becomes visibly upset and then frightened when he catches her. Mark reveals that he makes the movies so that he can capture the fear of his victims. He has mounted a round mirror atop his camera, so that he can capture the reactions of his victims as they see their impending deaths. He points the tripods knife towards Helens throat, but refuses to kill her.

The police arrive and Mark realises he is cornered. As he had planned from the very beginning, he impales himself on the knife with the camera running, providing the finale for his documentary. The last shot shows Helen crying over Marks dead body as the police enter the room.

==Cast== Carl Boehm as Mark Lewis
* Moira Shearer as Vivian
* Anna Massey as Helen Stephens
* Maxine Audley as Mrs. Stephens
* Pamela Green as Milly, the model
* Brenda Bruce as Dora
* Miles Malleson as Elderly gentleman customer
* Esmond Knight as Arthur Baden Martin Miller as Dr. Rosan
* Michael Goodliffe as Don Jarvis Jack Watson as Chief Insp. Gregg
* Shirley Anne Field as Diane Ashley
* Michael Powell as A.N. Lewis
* Columba Powell as Young Mark Lewis
* John Barrard as Small Man (uncredited)

==Themes==
Peeping Tom has been praised for its psychological complexity.    On the surface, the film is about the Freudian relationships between the protagonist and, respectively, his father, and his victims. However, several critics argue that the film is as much about the voyeurism of the audience as they watch the protagonists actions. Roger Ebert, in his review of the film, states that "The movies make us into voyeurs. We sit in the dark, watching other peoples lives. It is the bargain the cinema strikes with us, although most films are too well-behaved to mention it."    In this reading, Lewis is an allegory of the director of a horror film. In horror movies, the directors kill victims, often innocents, to provoke responses from the audiences and to manipulate their responses. Lewis records the deaths of his victims with his camera and by using the mirror and showing each of his victims their last moments, provokes their own fear even as he kills them.

Martin Scorsese, who has long been an admirer of Powells works, has stated that this film, along with Federico Fellinis 8½, contains all that can be said about directing:

 

According to Paul Wells, the film deals with the anxieties of British culture in regarding sexual repression, patriarchal obsession, voyeuristic pleasure and perverse violence. Wells (2000), p. 68  The impossible task in the film is the quest to photograph fear itself. Wells (2000), p. 68 

In the opinion of Peter Keough, the death scenes of the film would provide a field day to Freudian psychoanalysis and deconstructionists. Cinema here is equated to sexual aggression and death wish, the camera to the phallus, photography to violation, and film to ritualized voyeurism. Keough (2005), p. 219-221  The emphasis of the film lies on morbidity, not on eroticism. In a memorable sequence, an attractive, semi-nude female character turns to the camera and reveals a disfiguring facial scar.  This peeping tom is turned on not by naked bodies, but naked fear. And as Mark laments, whatever he photographs is lost to him.  Mark is a loner whose only, constant companion is his film camera. He is also the victim of his fathers studies in the phenomenon of fear in children, a human guinea pig subjected to sadistic experiments.  His love interest Helen has her own fascination with a morbid gaze. She is a childrens writer whose book concerns a magic camera and what it photographs. 

==Production==
The film was financed by Nat Cohen at Anglo-Amalgamated with the rest of the money coming from the National Film Finance Corporation. Cohen originally wanted a star to play the lead role and suggested Dirk Bogarde but the Rank Film Organisation, who had him under contract, refused to loan him out. Laurence Harvey was attached for a while but pulled out during pre-production and Powell ended up casting Karl Boehm. 

Filming took six weeks beginning in October 1959. 

==Release==
In the United Kingdom, it was released by Anglo-Amalgamated. It is often considered part of a Sadean trilogy with Horrors of the Black Museum (1959) and Circus of Horrors (1960). The three films had different production companies but the same distributor. They are connected through their themes of voyeurism, female disfigurement, and sadistic patriarchal figures. Heffernan (2004), p. 128-131  Anglo-Amalgamated films were typically released in the United States by American International Pictures through a deal between the two companies. But AIP was not interested in releasing Peeping Tom, apparently skeptical of its ability to satisfy audiences. 

In the United States, the film was released by importer and distributor Astor Pictures in 1962. It was released simultaneously to the markets for genre horror films, art films, and exploitation films. It failed to find an audience and was one of the least successful releases by Astor. Heffernan (2004), p. 114-115  The film received a B rating from the National Legion of Decency, signifying "morally objectionable in part" content. The organization identified voyeurism and sadism as key elements of the film in its rating. 

==Reception== Len Mosley writing for the Daily Express claimed that the film is more nauseating and depressing than the leper colonies of East Pakistan, the back streets of Mumbai|Bombay, and the gutters of Kolkata|Calcutta.  It was banned in Finland until 1981.  However, the film earned a cult following, and since the 1970s has received a critical reappraisal. Powell noted ruefully in his autobiography, "I make a film that nobody wants to see and then, thirty years later, everybody has either seen it or wants to see it."
 Ian Christie David Thompson. Alphabet City, which, Scorsese notes, was a seedy district of New York. The film was released in a cut black-and-white print but immediately became a cult fascination among Scorseses generation. Scorsese states that the film, in this mutilated form, influenced Jim McBrides David Holzmans Diary. Scorsese himself first saw the film in 1970 through a friend who owned an uncut 35mm colour print. In 1978, Scorsese was approached by a New York distributor, Corinth Films, which asked for $5,000 for a wider re-release. Scorsese gladly complied with their request, which allowed the film to reach a wider audience than its initial cult following. 
 BFI poll Bravo Channels 100 Scariest Movie Moments.  Roger Ebert has included it in his Great Movies column. 

Film aggregate Rotten Tomatoes has awarded the film a 96% rating, based on 46 reviews and an average score of 8.6/10. The sites consensus is: "Peeping Tom is a chilling, methodical look at the psychology of a killer, and a classic work of voyeuristic cinema." 

==Remake==
Director George Romero and Thelma Schoonmaker, Michael Powells widow, gained the rights to remake the film, but no plans have yet been announced.

==Relationship with Hitchcocks films==
The themes of voyeurism in Peeping Tom are also explored in several films by Alfred Hitchcock. In his book on Hitchcocks 1958 film Vertigo (film)|Vertigo, Charles Barr points out that the films title sequence and several shots seem to have inspired moments in Peeping Tom. 

Chris Rodleys documentary A Very British Psycho (1997) draws comparisons between Peeping Tom and Hitchcocks Psycho (1960 film)|Psycho; the latter film was given its New York premiere in June 1960, two months after Peeping Tom s premiere in London. Both films feature as protagonists atypically mild-mannered serial killers who are obsessed with their parents. However, despite containing material similar to Peeping Tom, Psycho became a box-office success and only increased the popularity and fame of its director (although the film was widely criticized in the English press). One reason suggested in the documentary is that Hitchcock, seeing the negative press reaction to Peeping Tom, decided to release Psycho without a press screening. 

In his early career, Powell worked as a stills photographer and in other positions on Hitchcocks films, and the two were friends throughout their careers. A variant of Peeping Tom s main conceit, The Blind Man, was one of List of unproduced Hitchcock projects#The Blind Man (1960)|Hitchcocks unproduced films around this time. Here, a blind pianist receives the eyes of a murder victim, but their retinas retain the image of the murder.

According to Isabelle McNeill, the film fits well within the slasher film subgenre, which was influenced by Psycho. She lists a number of elements which it shares with both Psycho and the genre in general. A recognizably human killer, who stands as the psychotic product of a sick family. The victim being a beautiful and sexually active woman. The location of the murder being not within a home, but within some other "terrible place". The weapon being something other than a gun. The attack registered from the victims point of view and coming with shocking suddenness. McNeill (2008), unnumbered pages  She finds that the film actually goes further than Psycho into slasher territory through introducing a series of female victims, and with Helen Stephens functioning as the bright and sympathetic final girl. 

==Home media==
Peeping Tom has currently received releases on DVD by the following different distributors:
* Studio Canal/Warner Bros (Region 2) Released with just the film and a photo gallery.
* The Criterion Collection (Region 1) Includes the Channel Four documentary A Very British Psycho and a commentary by Laura Mulvey
* Warner Home Vidéo / LInstitut Lumière (Region 2) 6 DVD boxed set of I Know Where Im Going!, A Canterbury Tale and Peeping Tom The Peeping Tom double disk set includes:
** Memories of Michael (Part 7) by Thelma Schoonmaker-Powell (In English with French subtitles); 12 min
** A Daring Adventurer (Part 7) by Bertrand Tavernier (In French with English subtitles); 20 min
** A Very British Psycho (In English with French subtitles); 51 min
** My Fetish Film An interview by the French director Gaspar Noé Gaspar Noé is named as Gaspard Noé on the DVD box but as Gaspar Noé on the DVD itself  (In French with English subtitles); 14 min
** The Film Poster An interview by the French Director Gaspar Noé  (In French with English subtitles); 2 min
* Optimum Releasing (Region 2)  A special edition as another of their Studio Canal re-releases, which include the 2006 special edition of Dont Look Now. It was released on 26 March 2007. Details follow:
** Brand new and exclusive commentary by Ian Christie, Powell expert
** Brand new and exclusive introduction by Martin Scorsese Oscar winning film editor
** The Eye of the Beholder (30 mins) A documentary in which Scorsese, Schoonmaker and Christie talk about the film
** The Strange Gaze of Mark Lewis (25 mins) A documentary about the psychology of the protagonist
** Trailer
** Booklet containing essay, interview with screenwriter Leo Marks and extract from Powells autobiography, Million Dollar Movie
** Behind the scenes stills gallery

Optimum Releasing 50th Anniversary Blu-ray disc was released on 22 November 2010

==Comparisons==
Comparisons have been made between Peeping Tom and other significant films in this genre such as:
*  
*  
*  

==Cultural references==
* Mike Pattons band Peeping Tom, and its Peeping Tom (Peeping Tom album)|self-titled album, are named in tribute to this movie. 
* Scarlett Thomas makes reference to the film in her 1999 novel In Your Face. Ghostface in Wes Cravens Scream 4 (2011) as being the first film to "put the audience in the killers POV."
* During a reminiscence in David Foster Wallaces novel Infinite Jest, Dr. James O. Incandenza, the man who went on to make a film that literally kills its audience, refers to having "still-posters from Powells Peeping Tom" in his childhood bedroom. 

==See also==
* List of films featuring surveillance

==Sources==
*  
*  
*  
*  

==References==
 

===Bibliography===
 
* Christie, Ian. Arrows of Desire: the films of Michael Powell and Emeric Pressburger. London: Faber & Faber, 1994. ISBN 0-571-16271-1. 163pp (illus. filmog. bibliog. index)
* Leo Marks|Marks, Leo. Peeping Tom. London: Faber & Faber, 1998. ISBN 0-571-19403-6
*  , 1986. ISBN 0-434-59945-X.
* Powell, Michael. Million Dollar Movie. London: Heinemann, 1992. ISBN 0-434-59947-6.
 

==External links==
*  
*  
*  . Full synopsis and film stills (and clips viewable from UK libraries).
*   at Powell & Pressburger Pages.
* 
*  
*   Essay

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 