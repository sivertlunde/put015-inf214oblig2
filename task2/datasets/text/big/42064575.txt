ID A
 
{{Infobox film
| name           = ID:A
| image          =
| image_size     =
| border         = 
| alt            = 
| caption        = 
| film name      = 
| director       = Christian E. Christiansen
| producer       = Louise Vesth
| writer         = Tine Krull Petersen
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Tuva Novotny
| music          = Kristian Eidnes Andersen
| cinematography = Ian Hansen
| editing        = Bodil Kjærhauge Anders Refn
| studio         = 
| distributor    =
| released       =  
| runtime        = 104 minutes
| country        = Denmark Netherlands Sweden
| language       = Danish
| budget         =
| gross          =
}}

ID:A is a 2011 thriller film, starring Tuva Novotny and directed by Christian E. Christiansen.

==Cast==
* Tuva Novotny - Aliena / Ida
* Flemming Enevold - Just
* Carsten Bjørnlund - Martin 
* Arnaud Binard - Pierre 
* John Buijsman - Rob 
* Rogier Philipoom - Guus 
* Jens Jørn Spottag - HP 
* Marie Louise Wille - Marietta 
* Françoise Lebrun - Isabelle
* Koen Wouterse - Tim

==Awards and nominations== Best Actress, Best Editor, and Best Special Effects, but lost out to Melancholia (2011 film)|Melancholia in all three categories. 

==References==
 

==External links==
*  

 
 
 
 
 


 