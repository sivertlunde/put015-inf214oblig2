Nemesis (1992 film)
 
{{Infobox film|
| name = Nemesis
| image = Nemesisposter93.jpg
| caption = Promotional film poster
| director = Albert Pyun
| writer = Rebecca Charles
| producer = Tim Karnowski Eric Karson Ash R. Shah
| starring = Olivier Gruner Tim Thomerson Cary-Hiroyuki Tagawa Yuji Okumoto Marjorie Monaghan Nicholas Guest
| cinematography = George Mooradian
| editing = Mark Conte David Kern
| music = Michel Rubini
| distributor = Imperial Entertainment Astra Distribution 
| released =  
| budget =
| runtime = 95 minutes
| language = English
| country = United States
}}

Nemesis is an American 1992 science fiction film directed by Albert Pyun and starring Olivier Gruner and Tim Thomerson.  It is the first installment in the Nemesis film series.

==Development history==

Nemesis was first conceived as Albert Pyuns last film under a three-picture contract with Cannon drawn up in 1987. Following the highly stylized high school thriller   with John Travolta and a police/serial killer procedural thriller, then entitled Alex Rain after the main character.

When Cannon decided Travoltas commercial viability was too weak, they essentially killed the Johnny Guitar remake. Pyun shifted to Alex Rain and began developing the movie around actress Kelly Lynch as a deeply troubled FBI agent hunting a serial killer amongst the Neo-Nazi community. Pyun incorporated futuristic touches and set the film 25 years in the future; at one point he even considered setting it on Mars 400 years into the future. Thus began Pyuns interest in a cyberpunk setting.
 Journey to the Center of the Earth. After this he focused on other projects, most notably the 1989 film Cyborg (film)|Cyborg.

Pyun returned to Alex Rain in 1991, after making the virtual reality film Arcade (film)|Arcade for Full Moon Entertainment. Pyuns research on that film gave him a number of ideas he wanted to incorporate into Alex Rain, including lowering the age of the protagonist to 13 and making Alex a tough, violent street urchin working undercover for a futuristic L.A.P.D. Having just worked with Megan Ward on Arcade, Pyun met with Ward and her agents to outline the film and discuss casting her as the lead. Ward and her agents responded favorably, despite reservations about the high level of violence and a scene which called for her character to be completely nude. Pyun shot a few test scenes with Ward and then began looking for a studio partner.

Pyun read an interview with Imperial Entertainments production executive, Ash Shah, and realized he and Shah shared similar views about where low-budget action movies could go. Pyun met with the three Shah brothers and pitched them his ideas for the film and its possibilities. He left the screenplay with the Shahs and they called the next day to say they wanted to make the film. However, there was one condition: they wanted the lead changed from a 13-year-old girl to a 30-year-old male, so as to utilize their recent discovery, French kickboxer Olivier Gruner. At a long meeting at Imperial, both sides went back and forth until Imperial agreed that, other than the sex change, they would let Pyun make the movie he wanted to make no matter how unorthodox his methods. Pyun agreed to cast Gruner and pre-production began within days.

==Plot== assassin for the Los Angeles Police Department|LAPD. During a routine mission, he is attacked by a group freedom fighters known as The Red Army Hammerheads. Nearly killed by the surviving leader, Rosaria (Jennifer Gatti), Alex resists her assertion that he is a mindless robot: "Eighty-six point five percent   is still human."

After months of cybernetic reconstruction and recovery, Alex tracks Rosaria to Old Baja and kills her. His handlers show up, led by his former lover Jared (Marjorie Monaghan), who is an android. Alex decides he has had enough and leaves the LAPD, becoming a freelance hustler and triggerman.

However, his LAPD bosses are just letting him run free for a while. After he is shot and badly wounded on a job, his old boss, Commissioner Farnsworth (Tim Thomerson), has him kidnapped and brought in for one final assignment. According to Germaine (Nicholas Guest), Jared has stolen vital security information regarding an upcoming international summit and must be stopped before she leaks the plans to the Red Army Hammerheads. Alex is told by Farnsworth that a bomb was implanted in his heart during his latest repairs. He is given three days to find Jared before she meets with the leader of the Hammerheads, Angie-Liv (Cary-Hiroyuki Tagawa); otherwise, the bomb will detonate and kill him. After flying to the island of Shang-Lu in the Pacific Rim, he is turned loose as bait for Jared.

In reality, the freedom fighters are not battling against government control of peoples lives, but for humanity’s future. A newly designed android is infiltrating the higher echelons of human society, copying the minds of powerful leaders into synthetic bodies, Farnsworth among them. Jared threatens their plans, and so Alexs real mission is to smoke her out for the synthetics to destroy.

Burned out, Alex halfheartedly begins his search, checking into a local hotel. He is soon intercepted by Julian (Deborah Shelton), a cyborg representing Jared. She tells him he is being followed by an LAPD strike team led by Farnsworth, waiting for the opportunity to hit the Hammerheads and Jared.

It turns out that Jared was fatally wounded in her escape from LA, requiring her memory core to be salvaged from her body. After removing a surveillance device implanted in Alexs eye, Julian injects him with a digital scrambler that prevents the bomb from being remotely detonated. She gives him Jareds memory core, enabling him to talk to her. The strike team storms the hotel and Julian sacrifices herself to let Alex escape.

Alex eventually joins up with a local woman, Max Impact (Merle Kennedy) who acts a scout for the Hammerheads while fronting as a tour guide. She is also Rosarias sister, the woman he killed in Old Baja, but while she wants Alex dead, her loyalty to the freedom fighters comes first. He is brought to the Hammerheads and is convinced by Angie-Liv to join their cause. Unfortunately, the strike team tracks them down, leading to a shootout and chase through the rundown city. Most of the Hammerheads, including Angie-Liv, are killed by Farnsworths men. Alex saves Maxs life, eventually earning her forgiveness. In a confrontation with Farnsworth, Alex shoots him with a grenade launcher, apparently killing him.

Alex and Max arrive at a secret hangar where Yoshiro (Yuji Okumoto), a surviving Hammerhead, is waiting. While launching their escape vehicle, an aerodyne, they are attacked by the cyborg Farnsworth, reduced to his mechanical endoskeleton. Alex defeats him, but suffers grave injuries in the process and discovers just how much of him really is cybernetically enhanced.

Alex brings Jareds core to another Hammerhead compound where they will be able to destroy the labs being used to duplicate people. Unfortunately this means wiping her memory from the core, effectively killing her. Heavily bandaged and temporarily blind, Alex is forced to say goodbye.

Sneaking into LA and hunting down the synthetic agents, Alex corners Germaine on the helipad of LAPD headquarters. Despite Germaines protests that he cannot hope to kill all the synthetics, Alex shoots him.

Before she died, Jared told Alex that the real Commissioner Farnsworth left him a letter at an old dead drop. In it, his former mentor apologizes for his sometimes rough treatment, reminding him that they all have to do what is right. Alex walks off with his new partner Max, and they joke about how they are going to smuggle his synthetic body through airport customs: "Piece by piece, Max..."

==Alternate ending==
The so-called Extended Version, released only in Japan, offers a darker ending. After Alex and Maxs conversation about going to New York, Farnsworth appears as they walk up the stairs. A female voice asks: "Should we take them out now?" Farnsworth turns to the off-screen woman and answers: "Why not?", suggesting that they succeed in the termination of Alex and Max.

This version does not contain the fight between Alex and Farnsworths endoskeleton on the aerodyne, explaining his appearance here. There is also an extended scene with Germaine in his office at LAPD headquarters, asking the security staff if there has been a breach within security.

==Cast==
* Olivier Gruner as Alex
* Tim Thomerson as Farnsworth
* Cary-Hiroyuki Tagawa as Angie-Liv
* Yuji Okumoto as Yoshiro Han
* Marjorie Monaghan as Jared
* Nicholas Guest as Germaine
* Vincent Klyn as Michelle
* Thom Mathews as Marion
* Marjean Holden as Pam
* Brion James as Maritz
* Deborah Shelton as Julian
* Jennifer Gatti as Rosaria
* Thomas Jane as Billy
* Jackie Earle Haley as Einstein
* Borovnisa Blervaque as Morico
* Merle Kennedy as Max Impact
* Adrianna Miles as German National
* Rob Carlton as Waiter
* Branscombe Richmond as Mexican Man

==Release==
The film was given a limited release theatrically in the United States by Imperial Entertainment in January 1993, grossing $2,001,124 at the box office.   The company also released it on VHS and laserdisc the same year. 

The film was released on DVD by Sterling Home Entertainment in 1998.   This version is currently out of print.

In 2010, director Pyun announced his intention to re-release the film in a new, alternate cut that featured enhanced computer effects. 

In 2014, German company Digidreams has released a complete set of the Nemesis film series. Fully remastered in HD. The set includes Nemesis 1-4 transferred in 1080p Widescreen, Nemesis 1 CD Soundtrack, Directors Cut of Nemesis 1, Bonus features for all four films, and Audio Commentary by the Director. The set includes both Dutch and English Audio 5.1/2.0 and is not region locked. Most of the Bonus material is in English.

==Reception==
The movie received a mixed-to-negative reception from critics, citing its convoluted storyline, amateurish acting, and emphasis on action at the expense of a decent script.  however, its reputation has improved over time and it is now considered a cult classic. 

==Sequels==
The film spawned three sequels titled  , set 73 years after the events of the first film,    which features characters sent back in time to 1998 and  .  Nemesis 3 was made using footage left over from the production of Nemesis 2. 

== References ==
 

==External links==
* 
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 