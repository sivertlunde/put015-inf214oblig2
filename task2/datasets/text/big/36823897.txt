In Person (film)
{{Infobox film name            = In Person image           = director  James Anderson (assistant) producer        = writer          = starring        = Ginger Rogers music           = cinematography  = editing         = studio          = RKO distributor     = released        =   country         = United States runtime         = language        = English budget          = $493,000 Richard Jewel, RKO Film Grosses: 1931-1951, Historical Journal of Film Radio and Television, Vol 14 No 1, 1994 p55  gross           = $715,000 
}}
In Person is a 1935 film starring Ginger Rogers. It made a profit of $147,000.  It is about Miss Carol Corliss, a beautiful movie star so insecure about her fame, that she goes around in disguise. She later meets a rugged outdoorsman who is unaffected by her star status.

== References ==
 

== External links ==
*  at IMDB

 
 
 
 


 