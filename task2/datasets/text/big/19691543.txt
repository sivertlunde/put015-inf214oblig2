Middle of Nowhere (2008 film)
{{Infobox Film
| name           = Middle of Nowhere
| image          = Middle of Nowhere.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster John Stockwell
| producer       = John Stockwell Michel Litvak David Lancaster Nicole Rocklin Michelle Morgan
| starring       = Eva Amurri Susan Sarandon
| music          = Ferraby Lionheart
| cinematography = Byron Shah
| editing        = Tom McArdle
| studio         = Bold Films Louisiana Media
| distributor    = Image Entertainment
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
}} John Stockwell, Michelle Morgan, premiered at the 2008 Toronto International Film Festival.  The film received a Golden Trailer Awards nomination in the category of Best Music. 

==Plot==
The film follows Grace (Eva Amurri), a young woman whose irresponsible mother, Rhonda (Susan Sarandon), ruins her daughters credit rating.    Rhonda uses the money to finance Graces younger sister, Taylors (Willa Holland), modeling campaign. While working a summer job, Grace meets the lonely Dorian Spitz (Anton Yelchin) and they start selling drugs together for extra cash. A love triangle forms when Dorian is attracted to Grace, who is interested in Ben Pretzler (Justin Chatwin). 

==Cast==
* Eva Amurri as Grace Berry 
* Susan Sarandon as Rhonda Berry 
* Anton Yelchin as Dorian Spitz 
* Justin Chatwin as Ben Pretzler 
* Willa Holland as Taylor Elizabeth Berry
* Scott A Martin as Morris Kraven
* Kenny Bordes as Ryan
* Kyle Clements as Ted

==Release==
The film was released on DVD and Blu-ray Disc|Blu-ray on July 13, 2010 by Image Entertainment. 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 