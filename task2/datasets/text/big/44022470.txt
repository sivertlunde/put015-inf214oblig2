The Paddy Lincoln Gang
 
 
{{Infobox film
| name = The Paddy Lincoln Gang
| image = The_Paddy_Lincoln_Gang_(film)_poster.jpg
| alt = 
| caption = 
| director = Ben Jagger
| producer =  Alistair Audsley  David Bainbridge  Ben Jagger  Graham Kentsley  James Rayner
| writer =  Alistair Audsley
| starring =  Dean S. Jagger  Joseph DiMasso  Richard Wagner  Demetri Watkins  Stephen Bridgewater  Amy Lawhorn Glen Matlock
| music = Julien Diaz  Tim Palmer
| cinematography =  Ryan Ovadia
| editing = Alex Fenn
| studio = Belief Films  Solus Entertainment Stealth Media Group
| distributor = 
| released =  
| runtime = 90 minutes
| country = United Kingdom
| language = English
| budget = 
| gross =
}}
The Paddy Lincoln Gang is a 2012 British drama film written and produced by Alistair Audsley, and directed by Ben Jagger. The film stars  Dean S. Jagger,  Joseph DiMasso, Stephen Bridgewater,  Amy Lawhorn and Glen Matlock. Principal photography began on 2 November 2011.    The film premiered in competition at the 2012 SoCal Film Festival.   

==Plot== Irish lead singer is haunted by his own paranoia and suspicions that something is not right with the band, his manager or his girlfriend.

==Cast==
* Dean S. Jagger as Rob McAlister
* Joseph DiMasso as Steady Eddie
* Richard Wagner as Rick
* Demetri Watkins as Tom Dufresne
* Stephen Bridgewater as Dan Craine
* Amy Lawhorn as Leyla Dufresne
* Glen Matlock as Himself

==Production==

===Development=== short went on to win several awards at the Hoboken International Film Festival,    as well as featuring at the 2011 Cannes Film Festival.      
 Screen Daily Variety were the first major outlet to reveal Sex Pistols founding-member Glen Matlocks involvement in the project.   

===Filming=== editing inspiration from Steven Soderberghs crime film The Limey.   

A docudrama scene was filmed backstage at a live festival performance from The Faces. Improvised between Sex Pistols bassist Glen Matlock and lead actor Dean S. Jagger.  Rolling Stones guitarist Ronnie Wood was a notable observer.   

==Music==
Musicians Matthew Steer and Colin Lizzard McGuinness  were brought into the project to shape the sound of the fictional Paddy Lincoln Gang band.    Hot Press featured Give Anger a Name and covered U2 and Pearl Jam producer Tim Palmers involvement in the soundtrack.   

==Release== US premiere ambassadorial role European debut Herts Advertiser respectively, promoting the film.  

==Reception==
The Paddy Lincoln Gang received positive reviews from its debut at the 2012 SoCal Film Festival, winning multiple awards including the Best in Fest prize.    It garnered further acclaim that year with the Oregon International Film Festivals Platinum Award.      

==References==
 

==External links==
*  
*   at the British Board of Film Classification

 
 
 
 
 
 
 
 
 
 
 
 
 
 