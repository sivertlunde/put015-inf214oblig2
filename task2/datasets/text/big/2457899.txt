Kaala Patthar
{{Infobox film
| name = Kaala Patthar 
| image = Kaalapatthar.jpg
| image_size =
| caption =
| director = Yash Chopra
| producer = Yash Chopra 
| writer = Salim-Javed
| narrator =
| starring = Amitabh Bachchan Shashi Kapoor Raakhee Gulzar Shatrughan Sinha Neetu Singh Parveen Babi Parikshat Sahni Prem Chopra Poonam Dhillon Mac Mohan 
| music = Songs:   Kay Gee
| editing =
| studio =
| distributor = Yash Raj Films Trimurti Films
| released =   
| runtime = 171 mins
| country = India
| language = Hindi
| budget =
| preceded_by =
| followed_by =
}}
 Kabhie Kabhie (1976) and Trishul (film)|Trishul (1978). However, this film did average business at the box office.  It was nominated for Filmfare awards.  While Rajesh Roshan provided music for the lyrics penned by Sahir, the background score was composed by Salil Chowdhary.

The movie is now termed as a Yash Chopra classic.

==Synopsis==

Vijay Pal Singh (Amitabh Bachchan) is a disgraced Merchant Navy captain who is branded a coward, humiliated by society and disowned by his parents for abandoning his ship risking the lives of over 300 passengers (perhaps inspired by Joseph Conrads Lord Jim). Feeling guilty over his cowardice and with low self-esteem, he starts working as a coal miner to forget his past. He meets and becomes friends with Ravi (Shashi Kapoor), an engineer in charge of the mine. He also makes an enemy in another co-worker named Mangal (Shatrughan Sinha) who is an escaped criminal working in the mine to avoid the police. Vijays past comes to haunt him as he sees Mangal causing trouble for the coal miners and angry over his cowardice over what happened when he was in the navy, Vijay tries to defend the miners against Mangal. Ravi eventually gets Vijay and Mangal to be friends after circumstances surrounding the mine force Mangal to change himself. The one person who supports Vijay him in his life is Dr Sudha Sen (Raakhee Gulzar) who tries to make Vijay face up to his past and move on. Ravi and Mangal also get involved in their own romances with Anita (Parveen Babi) and Channo (Neetu Singh), respectively.

When Seth Dhanraj (Prem Chopra) makes life difficult for the coal miners by giving them poor equipment, less than sufficient medical supplies and lack of facilities, Vijay, Ravi and Mangal come together to fight for justice against Dhanraj when water floods the mines endangering the lives of hundreds of workers including Mangals. The film have three songs of Mohd Rafi which are popular. Mujhe pyar ka tofa of Mohd Rafi Usha Mangeshkar was not picturised in the movie. It was scheduled to be pictured on Shatrughina Sinha and Nettu Singh. Dhoom Mache Dhoom of Mohd Rafi voice was  playbacked on Shahsi Kapoor and Shatrughina Sinha concurrently in the same songs. Bahoon Mein Tere Rafi Lata combo was a romantic duet picturised on Shahsi Kapoor and Parveen Babi.

==Cast==

* Amitabh Bachchan  as  Vijay Pal Singh
* Shashi Kapoor  as  Ravi Malhotra 
* Shatrughan Sinha  as  Mangal Singh
* Raakhee Gulzar  as  Dr. Sudha Sen
* Neetu Singh  as  Channo 
* Parveen Babi  as  Anita 
* Prem Chopra  as  Dhanraj Puri 
* Parikshat Sahni  as  Jagga 
* Satyen Kappoo  as  Raghunath 
* Gita Sidharth  as  Mrs. Raghunath
* Poonam Dhillon  as  Raghunaths Daughter
* Babu Kumar  as  Raghunaths Son
* Suresh Oberoi  as  Navy Officer
* Gautam Sarin  as  Chief Navy Officer
* Om Sahni  as  Navy Officer
* Suresh Bedi  as  Navy Officer Lamba  as  Navy Officer
* Iftekhar  as  Mr. Singh, (Vijays Father) 
* Sudha Chopra  as  Mrs. Singh, (Vijays Mother) 
* Sanjeev Kumar  as  Dr. Mathur
* Annu Kapoor  as  Compounder
* Pardesi  as  Compounder
* Yunus Parvez  as  Maneklal Saxena 
* Romesh Sharma  as  Vikram
* Madan Puri  as  Vikrams Father
* Manmohan Krishna  as  Crippled Hotel Owner
* Sharat Saxena  as  Dhanna
* Mohan Sherry  as  Shanker
* Mac Mohan  as  Rana
* Jagdish Raj  as  Police Inspector
* Mahaan Swadesh  as  Murli
* Baldev Trehan  as  Ozi
* Vikas Anand  as  Ram Singh
* Raj Verma  as  Munim
* Harish Chander  as  Harish Nagar  as  Daag Chandu  as  Chandu
* Uttam Sodi  as  Qaidi Darshan  as  Jailor
* Bhola  as  Gyan
* Ramanand  as  Roshan Singh
* Nazir Kashmiri  as  Miner

==Crew==
* Direction – Yash Chopra
* Writer – Salim-Javed
* Production – Yash Chopra
* Production Company – Yash Raj Films
* Cinematography – Kay Gee
* Music Direction – Rajesh Roshan
* Lyrics – Sahir Ludhianvi
* Playback – Kishore Kumar, Lata Mangeshkar, Mahendra Kapoor, Mohammed Rafi, Pamela Chopra, S.K. Mohan, Usha Mangeshkar

==Soundtrack==
{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length =
| all_writing =
| all_lyrics = Sahir Ludhianvi
| all_music = Rajesh Roshan
| writing_credits =
| lyrics_credits =
| music_credits =
| title0 =  
| note0 =
| writer0 =
| lyrics0 =
| music0 =
| extra0 =
| length0 =
| title1 = Ek Rasta Hai Zindagi
| note1 =
| writer1 =
| lyrics1 =
| music1 =
| extra1 = Kishore Kumar, Lata Mangeshkar
| length1 = 5:20
| title2 = Bahon Mein Teri
| note2 =
| writer2 =
| lyrics2 =
| music2 =
| extra2 = Mohammad Rafi, Lata Mangeshkar
| length2 = 4:15
| title3 = Meri Dooron Se Aye Baarat
| note3 =
| writer3 =
| lyrics3 =
| music3 =
| extra3 = Lata Mangeshkar, Chorus
| length3 = 3:20
| title4 = Jagaya Jagaya
| note4 =
| writer4 =
| lyrics4 =
| music4 =
| extra4 = Mahendra Kapoor, S.K. Mohan, Pamela Chopra
| length4 = 6:55
| title5 = Dhoom Mache Dhoom
| note5 =
| writer5 =
| lyrics5 =
| music5 =
| extra5 = Mohammad Rafi, Lata Mangeshkar, Mahendra Kapoor, S.K. Mohan
| length5 = 7:40
| title6 = Mujhe Pyar Ka Tohfa Dekar
| note6 =
| writer6 =
| lyrics6 =
| music6 =
| extra6 = Mohammad Rafi, Usha Mangeshkar
| length6 =5:15
}}

==Awards and Nominations==
* 1980 Filmfare Award Nominations Best Film Best Director - Yash Chopra Best Actor - Amitabh Bachchan Best Supporting Actor - Shatrughan Sinha Best Supporting Actress - Neetu Singh Best Story - Salim-Javed Best Music Director - Rajesh Roshan Best Male Playback Singer - Kishore Kumar for the song "Ek Raasta Hai Zindagi"

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 