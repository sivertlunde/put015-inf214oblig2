Wanted (2010 film)
 
 
{{multiple issues|
 
 
}}
{{Infobox film
 | name           = Wanted
 | image          = wanted_the_film.jpg
 | alt            =
 | caption        = Theatrical release poster
 | director       = Ravi Kinagi
 | producer       = Eskay Movies
 | writer         = Anonno Mamun
 | starring       = Jeet (actor)|Jeet, Srabanti Chatterjee, Sharad Kapoor
 | music          = Rajesh Roy
 | cinematography = Selva Kumar Prasad
 | editing        = Rabiranjan Moitra
 | studio         =
 | distributor    = Ashok Dhanuka, Himanshu Dhanuka
 | released       = 30 April 2010
 | runtime        = 153 minutes
 | country        = India Bengali
 | budget         = 
 | gross          = 
}}
 Jeet and Srabanti Chatterjee in the lead roles. The film is a remake of 2005 Telugu superhit film Athadu starring Mahesh Babu and Trisha Krishnan in the lead roles. Wanted received mostly positive reviews and one of the biggest hit of 2010.

==Plot==
Rajkumar Banerjee /Raja (Jeet (actor)|Jeet) is a professional assassin, and Joy (Indrajit Chakraborty) is his partner. They charge Rs. 15&nbsp;million per assassination.

A party leader, Amarnath Ganguly wants someone to attempt an assassination on him so that he can get more sympathy votes in the upcoming election.Amarnath Gangulys colleague Shubhankar Banerjee(Biplab Chatterjee) hooks him up with Raja for a deal. However, before Raja can shoot Amarnath Ganguly, someone else fatally shoots him. Within minutes, police surround the building. Just before the exit of the building, a big car accident occurs on the street with Rajas car. Raja assumes that Joy got trapped and died in the accident. So Raja goes to the top of the skyscraper. With the police behind him and in front of him, he is helpless. Suddenly a train comes and Raja uses a rope to jump from the skyscraper to the train.

On the train, Raja meets Shibu (Atanu Mukherjee). Shibu reveals that he ran away from his village, Ganganagar, when he was young, he explains that he is finally returning to his village. Later that same evening, while aiming for Raja, the police accidentally shoot Shibu. Raja escapes and decided to go to Ganganagar. Thinking that Raja is Shibhu, his family accepts Raja into their family.

Meanwhile, a CBI officer, Salim Ali Khan (Sharad Kapoor), investigating the murder of Amarnath Ganguly comes across Raja alias Shibu. He suspects Raja of Amarnath Gangulys murder.He tries taking Rajas fingerprints unconventionally but Raja outsmarts him. Soon clues lead to Joy but he is dead.

Meanwhile, in Ganganagar, after the marriage of the granddaughter was over, the CBI come because they find out that Raja is not Shibu and suspect that Raja is the killer. Raja learns of the situation and flees. After the CBI leave, Raja returns to explain everything: who is he and how the real Shibu was killed. At first the family shows no interest in helping Raja to find the real assassin as they are all shocked but the grandfather (Biswajit Chakraborty) helps Raja by giving him a gun.
Raja calls Shubhankar Banerjee and demands to know who killed Amarnath Ganguly. At first, Shubankar Banerjee refuses but after Raja tells him that he recorded their first conservation on the phone,Shubankar Banerjee reveals everything. It was Joy who killed Amarnath Ganguly for Rs. 20&nbsp;million. He faked his death by sending a drunkard in the car at the time of the accident. Raja records this conversation and immediately leaves for the old church, where Joy is. Raja and Joy meet and Raja has the gun loaded before Joy. But the same policemen who killed Shibu barge in. Raja manages to kill all of them and also kill Joy. Raja hands over the recorded cassette to the CBI officer. The CBI officer goes directly to Shubhankar Banerjees office and tells him that he will hand him over to the police. Shubhankar Banerjee tells him that he has no concrete evidence. The CBI officer tells him that the tape is evidence enough for Amarnath Gangulys hot headed son,who has vowed vengeance for his fathers death.Trapped without any recourse,Shubhankar Banerjee commits suicide. In the last scene, Raja immerses Shibus ashes and the CBI officer lets him go to Shibus family, where he stays forever.

==Cast== Jeet – Rajkumar Banerjee (Raja)/Shibu
* Srabanti Chatterjee – Pooja
* Sharad Kapoor – CBI Inspector Salim Ali Khan
* Biplab Chatterjee – Subhankar Banerjee
* Atanu Mukherjee- Shibranjan Chowdhury (Shibu)
* Biswajit Chakraborty – Siddhartha Narayan Choudhury
* Kamalika Banerjee- Shibus mother
* Indrajit Chakraborty-Joy
* Joy Badlani-Sadhu
* Kharaj Mukherjee-Shibus uncle
* Aritra Dutta Banik

==Critical response==
Amrita RoyChoudhury of The Times of India gave the film a rating of   and told "Wanted has all the right ingredients — stylised action sequences (certainly new to the Bengali audience), a gripping narrative backed by solid performances, a powerful presentation technique and an extremely well packaging, to top it all. Jeet proves once again that he is a seasoned actor who has only got better with time. Srabanti looks cute as the chulbuli Pooja, who falls hook, line and sinker for Raja whom she assumes to be gramer paliye jaoya chhele, Shibu. Kharaj does a fabulous job as the funnyman and even before he opened his mouth, the audience was in splits. Another surprise in the film comes in the form of Sharad Kapoor aka Salim Khan, the CBI officer whose only weakness is goodlooking women. His character too is well-etched out and adds that extra zing to the narrative." 

==References==
 

 

 
 
 