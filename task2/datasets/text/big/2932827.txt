Gridlock'd
{{Infobox film
| name           = Gridlockd
| image          = Gridlockdposter.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Vondie Curtis-Hall 
| producer       = {{plainlist|
* Damian Jones Paul Webster
* Erica Huggins
}}
| writer         = Vondie Curtis-Hall
| starring       = {{plainlist|
* Tim Roth
* Tupac Shakur
* Thandie Newton
}}
| music          = Stewart Copeland
| cinematography = Bill Pope
| editing        = Christopher Koefoed
| studio         = {{plainlist|
* Polygram Filmed Entertainment
* Interscope Communications
* Def Pictures
* Dragon Pictures
}}
| distributor    = Gramercy Pictures
| released       =  
| runtime        = 91 minutes
| country        = United States
| language       = English
| budget         = $5 million
| gross          = $5.6 million
}}
Gridlockd is a 1997 American comedy-drama film written and directed by Vondie Curtis-Hall and starring Tupac Shakur, Tim Roth and Thandie Newton. It was the directorial debut of Curtis-Hall, who also has a small part in the film. The films opening was relatively low, despite critical acclaim; its opening weekend netted only $2,678,372 and it finished with a little over $5.5 million.  The film paid tribute to star Tupac Shakur, who had been murdered four months prior to the films release.

==Plot== addicts Spoon overdoses on rehabilitation program.

==Cast==
* Tupac Shakur as Ezekiel "Spoon" Whitmore
* Tim Roth as Alexander "Stretch" Rawland
* Thandie Newton as Barbara "Cookie" Cook
* Charles Fleischer as Mr. Woodson
* Bokeem Woodbine as Mud
* Howard Hesseman as Blind man
* John Sayles as Cop #1
* Eric Payne as Cop #2
* Tom Towles as D-Repers Henchman Tom Wright as Koolaid
* Lucy Liu as Cee-Cee
* Billie Neal as Medicaid woman #1
* Debra Wilson as Medicaid woman #2
* Rusty Schwimmer as Medicaid nurse
* Richmond Arquette as Resident doctor
* Elizabeth Peña as Admissions person
* Kasi Lemmons as Madonna
* Vondie Curtis-Hall as D-Reper

== Soundtrack ==
{|class="wikitable"  Year
!rowspan="2"|Album Peak chart positions Certifications
|-
!width=40| Billboard 200|U.S. 
|- 1997
|Gridlockd (soundtrack)|Gridlockd 
* Released: January 28, 1997 Interscope
|align="center"|1
|
* Recording Industry Association of America|US: Gold
|-
|}

==Reception==
The New York Times editor Janet Maslin praised Shakurs performance: "He played this part with an appealing mix of presence, confidence and humor". {{cite news
 | last = Maslin
 | first = Janet
 | coauthors =
 | title = And You Thought Recovery Was Serious Business
 | work = The New York Times
 | pages = 
 | language =
 | publisher = 
 | date = January 29, 1997
 | url = 
 | accessdate = }}  Desson Howe, for the Washington Post, wrote, "Shakur and Roth, who seem born for these roles, are allowed to take charge – and have fun doing it". {{cite news
 | last = Howe
 | first = Desson
 | coauthors =
 | title = Surprisingly Footloose
 | work = Washington Post
 | pages = 
 | language =
 | publisher = 
 | date = January 31, 1997
 | url = 
 | accessdate = }}  USA Today gave the film three out of four stars and felt that Hall had not "latched onto a particularly original notion of city blight. But he knows how to mine the humor in such desperation". {{cite news
 | last = Wloszczyna
 | first = Susan
 | coauthors =
 | title = Late Rapper and Roth Animate Gridlockd
 | work = USA Today
 | pages = 
 | language =
 | publisher = 
 | date = January 29, 1997
 | url = 
 | accessdate = }}  

Entertainment Weekly gave the film "B" rating and Owen Gleiberman wrote, "Gridlockd doesnt have the imaginative vision of a movie like Trainspotting (film)|Trainspotting, yet its more literally true to the haphazard torpor of the junkie life than anything weve seen on screen since Drugstore Cowboy... Curtis Hall has caught the bottom-feeder enervation of heroin addiction". {{cite news
 | last = Gleiberman
 | first = Owen
 | coauthors =
 | title = Gridlockd
 | work = Entertainment Weekly
 | pages = 
 | language =
 | publisher = 
 | date = January 31, 1997
 | url = http://www.ew.com/ew/article/0,,286619,00.html
 | accessdate = 2009-06-14 }} 

===Box office===
Gridlockd debuted at #9 at the US box office with $2,678,372. 

==See also==
* Gridlockd (soundtrack)

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 