The New Daughter
{{Infobox film
| name           = The New Daughter
| image          = The New Daughter DVD Cover.jpg
| alt            =  
| caption        = 
| director       = Luis Berdejo
| producer       = Paul Brooks
| screenplay     = John Travis John Connolly
| starring       = Kevin Costner Ivana Baquero Samantha Mathis
| music          = Javier Navarrete
| cinematography = Checco Varese
| editing        = Tom Elkins Robb Sullivan
| studio         = Gold Circle Films New Daughter Productions
| distributor    = Anchor Bay Entertainment
| released       =  
| runtime        = 108 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} John Connolly, burial mound.

==Plot==
John James (Kevin Costner), a recently divorced novelist, moves into an old house in the country with his teenage daughter, Louisa (Ivana Baquero), and young son, Sam (Gattlin Griffith). John and Louisas relationship is strained; She accuses him of never loving her mother and ruining everything.
On the familys first night in the house, Louisa hears strange noises outside her bedroom window. Unseen by her is a humanoid creature lurking outside on the roof.
The following day, while exploring the surrounding fields and forest, the children come across a large mound. Louisa is instantly attracted to it, but Sam is reluctant to go near it.
In town, John learns that his house is locally infamous for the disappearance of a woman who lived there. Upon returning home, he finds Louisas pet cat mutilated near the house.

Louisa returns to the mound after school and, while relaxing in the sunshine, hears strange noises. As Louisa hears what sounds like approaching animal growls, the film jump-cuts to John cutting his hand while washing dishes. That night John finds a trail of muddy footprints leading from the open front door to the bathroom. Louisa is inside with the door locked, sitting in the tub as the shower washes her extremely muddied body. The muddy water is shown turning red with blood as it reaches the drain.
Later that night, John finds Louisa sleepwalking. When he takes her back to bed, he finds a strange doll made from straw, inside of which is a dark ball containing a live spider. The next morning Louisa comes down for breakfast wearing a short dress and make-up. When John asks her about the doll, she claims not to have seen it.

Louisa is bullied by a preppy girl in the staircase of their school. Meanwhile, at home, John comes across a pile of muddy clothes in her bedroom. While investigating outside the house, he finds the head of Louisas doll at the mound, and the bloody remains of a blackbird.
John is called to come to pick up Louisa, who says she does not feel well. In the infirmary he passes the preppy girl, who had apparently "fallen" down the stairs and injured her arm. Sams teacher, Cassandra (Samantha Mathis), gives John her phone number, offering her friendship.
He is later startled to find a nest of spiders in the kitchen drawer where hed placed the straw doll. That night, John sees Louisa emerging from the woods after having previously ordered her to be home by dark. He calls attention to scarring visible on the skin around her neck, but Louisa storms off without explanation. At dinner she eats in an animal-like manner, as if starved.

John contacts Cassandra one night, and they meet sociably. While John is away, Louisa hears noises outside the house and exits.
As John is returning home in his car, he sees a shadowy figure running through the woods in front of the house, which vaguely looks like Louisa.
He exits his car and follows sounds of animal growls, but rushes back growing fearful. A stone hits his window and he speeds back to the house.
Asking Louisa if shed been outside, she says no. He orders her not to go near the mound any more.

The next day, Johns agent drives up to the house and they talk about the house.
Inside, Sam is directed by Louisa to climb the ladder to the attic. Sam falls when frightened by animal noises there. He requires stitches, and John berates Louisa for not caring for her younger brother.
That night, John searches the internet, finding an article about burial mounds.
He telephones an expert on the subject, Professor White (Noah Taylor), but is ignored. He then researches the previous owner of the house, Sarah Wayne; she disappeared one day without a trace, after having locked her teenage daughter Emily in her bedroom from the outside. Emily was eventually found alive and went to live with her grandfather, Roger Wayne (James Gammon).

John leaves the children with a babysitter and goes to find Roger Wayne. As Louisa watches him drive away from her window, scarring can be seen on the nape of her neck. She then returns to the mound. John arrives at Roger Waynes empty house, but lets himself in and looks around. In the girls room he finds a strange nest made from twigs and straw, and a depiction of the burial mound drawn onto a wall with the word "home" below it. Roger arrives and confronts John with explanations of what had occurred with Emily, that he burned down his house with her inside, insisting that it wasnt his granddaughter anymore.

While John is away, the babysitter is locked out of the house, as animal growls are heard approaching. Sam hides in his room as he hears the babysitters cries for help.
John arrives home, comforts a frightened Sam, and rushes to Louisa to ask what happened to the babysitter. She says she does not know. After reporting the babysitters disappearance to police, John takes an ill Louisa to bed. She tearfully asks if hes going to leave her as her mother did. He tells her he never will.
That night, John dreams of a doorway on top of the burial mound and Louisa transforming into a creature who announces "Im your new daughter".
The next day he finds a nest in Louisas closet similar to the one at Rogers house. John calls a contractor to destroy the mound that day. Professor White, having called back to ask about the mound, suddenly arrives and pleads with John to wait. He tells John of an ancient civilization who worshiped the burial mounds, believing them to be the homes of ancient Gods, or "mound-walkers".
He mentions a ritual exchange of gifts — including small straw dolls, as John had found — and the mound-walkers search for a young girl with whom to mate and give birth to a new generation of deities to reclaim the earth.
Horrified by this, John instructs the bulldozer to start. When it digs into the mound, the body of the babysitter is uncovered. Louisa, meanwhile, in the schoolyard, is scraping the ground with her fingers. John is taken to the police station for questioning while Cassandra looks after the kids.

That night, as officer Ed Lowry (Erik Palladino) drives John home, they are attacked, and Lowry is dragged from the patrol car by a creature.
John continues home, discovering it in disarray and finding Cassandra with her throat slit. She dies while motioning toward Louisa standing in the doorway. 
John gathers the children to leave, but Louisa refuses. Mound-walkers begin attacking the house, and John kills three.
After the attack, Louisa is missing. Her screams can be heard coming from the mound. John leaves Sam in his room, tells him to wait for the police, and tells him "be a big boy".
John goes to the mound to rescue Louisa and finds a tunnel leading into it. He pierces a can of gasoline, sets explosives left from the postponed demolition by the tunnel entrance, sparks a flare for light and crawls inside. Louisa is found unconscious and covered in mud. As he carries her out, angered creatures howl and give chase. Meanwhile, Sam has exited the house, clutching a framed family portrait and looking into the dark toward sounds coming from the mound.

John escapes the mound with Louisa and blocks the tunnel with the leaking fuel canister, but another mound-walker is already there outside. Louisa collapses and begs her father not to leave her. John looks down at her and sees her beginning to transform into one of the creatures. He drops a flare into the leaking diesel fuel and the mound goes up in a huge explosion. As Sam watches, the flames are reflected on the glass of the frame. A figure is seen emerging, although its identity is unclear. Sam asks, "Daddy?", as shadows can also be seen moving in the trees and house behind him. A growling creature emerges directly behind him as the screen goes black.

==Cast==
* Kevin Costner as John James
* Ivana Baquero as Louisa James 
* Gattlin Griffith as Sam James 
* Samantha Mathis as Cassandra Parker 
* Noah Taylor as Professor Evan White
* Margaret Anne Florence as Alexis Danella
* James Gammon as Roger Wayne
* Erik Palladino as Officer Lowry
* Guy Perry  as Alpha Creature

==Production== John Connolly.   

===Filming===
Filming took place primarily at the Wedge Plantation along the Santee River just north of McClellanville, South Carolina.  Secondary filming was done in McClellanville and on the College of Charleston campus.  The exterior of the house in which John James finds Roger Wayne was shot at 67 Moultrie St., Charleston, South Carolina. The school used in filming is West Ashley Middle School, about 3 miles from the original 1670 Charleston Town settlement (The school was formerly Middleton High School, the alma mater of Darius Rucker, and before that it was St. Andrews Jr. High School).  School remained in session during filming, with teachers peeking out the windows at Kevin Costner, and film crews and vehicles crowded in between cars in the teachers parking lot. The side entrance of the building was used as the "front" of the school in the movie.  Air conditioning units were hidden by temporary evergreen trees which were removed after filming.

==Release== limited theatrical Anchor Bay.  It was released on DVD and Blu-ray Disc on May 18, 2010.  

==Reception==
The New Daughter holds a critic rating of 33% on Rotten Tomatoes, indicating generally unfavourable reviews. 

The movie has a user rating of 6.0/10 on Metacritic, indicating average reviews. 

==References==
 

==External links==
*  
*  
*   at Metacritic
*  

 
 
 
 
 
 
 