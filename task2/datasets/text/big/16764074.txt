Cabfair
 
{{Infobox Film
| caption        = theatrical poster
| director       = Justin Chenier
| producer       = Justin Chenier Mark Thompson K. Shane Doulton
| writer         = Justin Chenier K. Shane Doulton
| narrator       = 
| starring       = K. Shane Doulton Matt McLaren Giulio Veri Derrick Leslie Bruno Sanita April Johnson Jessie Gower Mark Nesseth
| music          = Ryan Lemmon
| cinematography = 
| editing        = Justin Chenier
| distributor    = 
| released       = 
| runtime        = 111 min.
| country        = Canada English
| budget         = $500
| gross          = 
| preceded_by    = 
| followed_by    = 
}}

CABFAIR is a 2008 mockumentary film produced and directed by Justin Chenier, who came up with the original concept in 2004. Justin Chenier and K. Shane Doulton are listed as the writers, although the film did not have a conventional script, with each scene improvised by the actors while following the storyline written by Justin Chenier. CABFAIR was originally intended to be an 8-part television series, much like Trailer Park Boys, with an open casting call on Saturday, February 18, 2005. 

CABFARE was first covered in the media by the Kingston Whig Standard in a February, 2005 article that reported on the casting call. CABFAIR was shot from June 17 to October 28, 2007 in Kingston, Ontario. While the cast and crew worked from detailed storylines, the film was unscripted, giving the actors the freedom to improvise their scenes while keeping true to the story. It was submitted for consideration to the 2008 Kingston Canadian Film Festival, and if selected will have a world premiere in Kingston at the festival between February 27 and March 2, 2008. The film follows a Kingston taxi driver for a day when his favourite radio station threatens to change format.
 Snake Eyes CKWS News FM96 morning show co-host Rob McDonald. The soundtrack features the original song “Surrounded” by Kingston musician and radio personality Ryan Lemmon, who also has a small cameo in the film and worked as an Audio Assistant during production of the film.

==Tagline==
What goes on when the meter goes off

==Plot summary==
Reggie O’Brien (K. Shane Doulton) is a cab driver with an opinion on everything. When he is not taxiing his usual fares, Reggie can be found enjoying a game of bowling in the morning league, breaking for a cup of coffee with his cab-cleaning pal Mugford (Matt McLaren), arguing with his ex-wife Nola O’Brien (April Johnson), filling in his co-worker and former Pentecostal minister Pastor Bob (Mark Nesseth) on the ways of secular world, hanging with his buddy Bounty “The Dog” Hunter (Derek Leslie), arguing with cab company owner Raoul Cassivi (Giulio Veri), or clashing with the authority of Kingston’s taxi commissioner, Bruno DeLuca (Bruno Sanita).

When a film crew spends a day with Reggie, documenting a day in the life of a cab driver, his world seems to fall apart. When he gets word that Pat James (Tony Orr), the owner of his favourite radio station and Kingston’s only country music station, is going to flip to an “all women, all talk” format, Reggie pulls no stops to keep country music on Kingston’s airwaves. And just when he thinks he has a stake in the grand prize of the “Drink to the Bottom and Win” contest at the local coffee shop, he learns the true meaning of friendship. To keep his cab on the road, and his dream alive, Reggie has to overcome adversity, friendship, and himself.

==Cast==
* K. Shane Doulton as Reggie OBrien.
* Matt McLaren as Mugford.
* Giulio Veri as Raoul Cassivi.
* Derrick Leslie as Bounty "The Dog" Hunter.
* Bruno Sanita as Bruno "The Commie" DeLuca.
* April Johnson as Viola "Nola" OBrien.
* Jessie Gower as Miwako Ozawa.
* Mark Nesseth as Pastor Bob.
* Fern DeSousa as Cue Ball.
* Dan Publicover as Tommy Hunter.
* Jeff McKinnon as Dick Hunter.
* Tony Orr as Pat James.
* Glenn Willams as Valery Stominkov.
* Patrick Johnson as Running Guy.
* Cathy Johnson as Mugfords Mom.
* Rob McDonald as DMV Officer.
* Kevin Thompson as Kevin.
* Olivia Aeckerle as Candi Apple.
* Dylan Chenier as 8-Track Kid.
* Shauna Cunningham as Yolanda Brown.

==External links==
*  
* 

==References==
 

 
 