The Incredibly True Adventure of Two Girls in Love
{{Infobox film
| name = The Incredibly True Adventure of Two Girls in Love
| image = True Adventures of 2 girls 1995.jpg
| caption = Laurel Holloman (L) and Nicole Ari Parker in the movie poster
| director = Maria Maggenti
| producer = Dolly Hall
| writer = Maria Maggenti
| starring = Laurel Holloman   Nicole Ari Parker   Maggie Moore
| music = Terry Dame   Tom Judson
| cinematography = Tami Reiker
| editing = Susan Graef
| studio = New Line Cinema   Smash Pictures
| distributor = New Line Cinema
| released =    
| runtime = 94 minutes
| country = United States
| language = English
| budget = $250,000 (estimated)
| gross = $1,977,544 (USA)
| italic title = force
}}

The Incredibly True Adventure of Two Girls in Love is a 1995 film, written and directed by Maria Maggenti, of the story of two very different high school girls who fall in love.

==Plot== final year tomboyish personality romantic partner Wendy, a married woman who drops by the gas station when it pleases her, even though Randy knows they are in a dead-end relationship. Randy lives with her lesbian aunt Rebecca and her girlfriend Vicky, as well as Rebeccas ex-girlfriend Lena, who has no place to stay and is living with them until she finds somewhere else she can go to.
 cliquish friends. During this time, Randy is approached by Wendys jealous husband Ali at the gas station, who grabs Randy and warns her to stay away from his wife. Randy spends much of her time with Evie hanging out in meadows, trading music (opera and Mozart from Evie, punk rock from Randy) and talking. When Wendy next visits her, Randy rejects her, telling her she has a new girlfriend, Evie.
 Operation Rescue-like group. On the front steps of Rebeccas house, they kiss for the first time. Evie records it in her diary later, apparently wondering what it all means.

Randy and Evie experiment with how “  and marijuana. That night, they make love, then fall asleep in Evie’s mother’s bed. 

The next morning, it is Evies 18th birthday; Evelyn returns prematurely with presents for her, but is shocked by the mess in the kitchen and the rest of the house. Furiously searching the upstairs, she discovers Evie and Randy, but only realizes Randy is a girl when she runs past her on her way out. Rebecca, who has learned that Randy will not be graduating high school, goes over to go to Frank’s house with Vicky and Lena, where Randy had told them she would be staying the night. Rebecca threatens Frank until, panicked, he turns over Evie’s phone number. She calls, but Evie and Randy have already absconded and she is left talking to a furious Evelyn.

Evie and Randy, crying, scared and accusatory, take refuge in a motel. Randy finally calls Wendy, who comes out, pays for the room and tries to comfort the girls. Ali sees her car in the parking lot, however, and comes bursting in, eventually attracting Evelyn, Rebecca, Vicky, Lena, Frank, and Evie’s three friends, who were driving past reading aloud from Rita Mae Brown’s Rubyfruit Jungle (apparently still processing Evie’s news). The movie ends with Randy and Evie kissing and hugging in the open motel room doorway while everyone else argues in the background at top volume. 


  }}

==Cast==

===Principal cast===
*Laurel Holloman — Randall "Randy" Dean
*Nicole Ari Parker — Evelyn "Evie" Roy Jr
*Maggie Moore — Wendy
*Kate Stafford — Rebecca Dean
*Sabrina Artel — Vicky
*Nelson Rodríguez — Frank
*Dale Dickey — Regina
*Katlin Tyler — Girl #1
*Anna Padgett — Girl #2
*Chelsea Catthouse — Girl #3
*John Elsen - Ali, Wendys husband
*Stephanie Berry — Evelyn Roy

===Supporting/others===
*Toby Poser — Lena
*Andrew Wright — Hayjay
*Babs Davy - Waitress
*Lillian Kiesler - Old Lady #1
*Maryette Charlton - Old Lady #2

==Production==
Maggenti had started a script with an image in her head of a tomboy with love notes in her back pocket, and gradually added more characters such as the tomboy’s family and her love interest. Later, she realized this character was based on her first girlfriend.    Between 1992 and 1994 her script, "a dark look at the middle and end of the girls relationship", grew until Maggenti abandoned it.

However, the film’s future associate producer, Melissa Painter, convinced Maggenti to shop the story around as an independent film - two producers wanted a pivotal element which would make two very different girls cross paths, and Maggenti then improvised the story that formed the beginning of their relationship. The producers were delighted with this, so Maggenti wrote a new script which she finished in eight days. She found she had surprised herself in that her script was now a comedic farce; the story wasnt about gay love or coming out, but about first love between teenagers.   


Shooting was completed in 21 days. Scenes taking place at Randy and Evies high school were filmed in divisions of The Horace Mann School, in Riverdale, Bronx.

==Release and reception==

The Incredibly True Adventure of Two Girls in Love was originally released on 16 June 1995 in the United States, with the film being released into UK theatres on 20 September 1996. It generated good notices and publicity at the 1995 Sundance Film Festival, where the film was released on 23 January.    It also won a GLAAD Media Award for Outstanding Film – Limited Release in 1996, with its success launching the film careers of the movies fronting actresses Laurel Holloman, Nicole Ari Parker and Dale Dickey. 

==References==
 

==External links==
*  
*  
*  
 

 
 
 
 
 
 
 
 
 
 
 