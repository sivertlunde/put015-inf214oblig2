Defiance of a Teenager
{{Infobox film
| name           = Defiance of a Teenager
| image          = Defiance of a Teenager poster.jpg
| caption        = Poster for Defiance of a Teenager (1959)
| director       = Kim Ki-young 
| producer       = 
| writer         = O Yeong-jin
| starring       = Hwang Hae-nam Um Aing-ran
| music          = Han Sang-ki
| cinematography = Kim Deok-jin
| editing        = O Yeong-geun
| distributor    = Hyeob i Films Co., Ltd Beoma Productions
| released       =  
| runtime        = 
| country        = South Korea
| language       = Korean
| film name      = {{Film name
 | hangul         = 10   
 | hanja          = 10 의  
 | rr             = 10daeui banhang
 | mr             = 10taeŭi panhang }}
| budget         = 
| gross          = 
}}
Defiance of a Teenager (hangul|10대의 반항 - Shipdae-eui banhang) is a 1959 South Korean film directed by Kim Ki-young.

==Synopsis==
A melodrama about a group of delinquent teenagers under the leadership of a corrupt boss. 

==Cast==
* Hwang Hae-nam 
* Um Aing-ran
* Jo Mi-ryeong
* Ahn Sung-ki
* Hwang Jung-seun

==Notes==
 

==Bibliography==
*  
*  
*  

 

 
 
 
 


 