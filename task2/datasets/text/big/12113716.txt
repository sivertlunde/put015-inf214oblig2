Boulevard (1994 film)
{{Infobox film
| name           = Boulevard
| image          =Boulevard_(1994_film).jpg
| image size     =
| caption        =
| director       = Penelope Buitenhuis
| producer       = Ilana Frank Peer J. Oppenheimer Ray Sager Peter R. Simpson
| writer         = Andrea Wilde
| narrator       =
| starring       = Rae Dawn Chong Kari Wührer Lou Diamond Phillips Lance Henriksen Joel Bissonnette Judith Scott Ian Thomas
| cinematography = David Frazee
| editing        = Bernadette Kelly
| distributor    =
| released       =
| runtime        = 95 minutes
| country        = Canada
| language       = English
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}

Boulevard is a 1994 crime thriller film starring Rae Dawn Chong, Kari Wührer and Lou Diamond Phillips.

==Plot==
The film is about a woman named Jennefer (played by Kari Wührer) who runs from her abusive husband, gives her baby up for adoption and ends up on the streets during a grim and cold winter in Toronto. Shes taken in by a prostitute named Ola (played by Rae Dawn Chong). Ola sees her pimp Hassan (Lou Diamond Phillips) murder another prostitute but refuses to testify against him, knowing that Hassan has associates that will kill her. A police officer named McClaren (Lance Henriksen) attempts to interrogate her and she is deported. Jennefer then becomes a prostitute under Hassan and later confronts her husband who tracks her down with the intent on killing her.

==Cast==
*Rae Dawn Chong &mdash; Ola
*Kari Wührer &mdash; Jennefer
*Lou Diamond Phillips &mdash; Hassan
*Lance Henriksen &mdash; McClaren
*Joel Bissonnette &mdash; J&ndash;Rod
*Judith Scott &mdash; Sheila
*Amber Lea Weston &mdash; Debi
*Greg Campbell &mdash; Coco
*Keram Malicki&ndash;Sánchez &mdash; Sister
*Katie Griffin (as Katie Griffen) &mdash; Lorraine
*Marcia Bennett &mdash; Mrs. Braverly
*Michael Eric Kramer (as Michael Kramer) &mdash; Doctor
*Linlyn Lue &mdash; ICU Nurse
*James Loxley &mdash; Dr. Edwards
*Timm Zemanek &mdash; Ticket Clerk
*Renato Rizzuti &mdash; Large Man
*Jim Feather &mdash; Chester
*Jimmy Loftus &mdash; Paul
*Christopher Bickford &mdash; Punk
*Alex Bandiera &mdash; Thug
*Kathleen Kitts &mdash; Blonde Woman

==Reception==
Allmovie critic Sandra Brennan gave the film 1½ stars out of 5. 

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 