The Pirate
 

{{Infobox film
| name           = The Pirate
| image          = Pirate.jpeg
| caption        = Original film poster
| director       = Vincente Minnelli
| producer       = Arthur Freed
| based on       =   
| screenplay     = Frances Goodrich Albert Hackett
| starring       = Gene Kelly Judy Garland
| music          = Lennie Hayton (score) Cole Porter (songs)
| cinematography = Harry Stradling Sr.
| editing        = Blanche Sewell
| studio         = Metro-Goldwyn-Mayer
| distributor    = Loews Cineplex Entertainment|Loews
| released       =  
| runtime        = 102 min.
| country        = United States
| awards         =
| language       = English
| budget         = $3,768,000  . 
| gross          = $2,656,000 
}}
 1948 United American Technicolor musical feature film from Metro-Goldwyn-Mayer. With songs by Cole Porter, it stars Judy Garland and Gene Kelly with co-stars Walter Slezak, Gladys Cooper, Reginald Owen, and George Zucco.

==Plot==
Manuela Alva (Judy Garland), who lives in the small Caribbean village of Calvados, dreams of being swept away by the legendary Pirate, Mack "the Black" Macoco. However, her aunt and uncle (who have raised her) insist that she marry the town mayor, the rotund and bullying Don Pedro (Walter Slezak).

Shortly before her wedding, Manuela visits a nearby town, Port Sebastian. A traveling circus has arrived, and Serafin (Gene Kelly), its handsome leader, flirts with all the girls in the song "Nina." When he encounters Manuela, however, he falls in love at first sight with her. Serafin compliments Manuelas beauty and begs her not to marry Don Pedro, but, angered, she hurries away. That night, however, Manuela cannot sleep, and she sneaks out to go see Serafins show.

At the show, Serafin hypnotizes Manuela, thinking that she will admit she loves him. Instead, the hypnotized girl wildly sings and dances about her love for "Mack the Black." Serafin awakens Manuela with a kiss, and she flees in horror.

On Manuelas wedding day, the traveling players arrive in Calvados. Serafin begs Manuela to join his troupe, and asks her to admit that she loves him. Don Pedro, hearing noise in Manuelas room, arrives at her door, and asks Manuela to go away so that he can teach Serafin a lesson.

Serafin recognizes Don Pedro as Macoco, retired and obese. He blackmails Pedro with this information, swearing to tell it to Manuela if Don Pedro forbids the performers from putting on a show. Serafin then decides to pretend to be Macoco in order to win over Manuela. He reveals himself before the whole town as Macoco, then asks Manuela if she will come with him; she again refuses. Still, watching from her window as "Macoco" dances, she begins to daydream about the pirate.  The next day, "Macoco" threatens to burn down the town if he cannot have Manuela. Finally, she happily agrees to go with him.

One of Serafins troupe accidentally reveals Serafins plan to Manuela. To get her revenge, she first pretends to seduce Serafin, then attacks him with words and hurtling objects. She accidentally knocks him out, then realizes that she loves him, and sings "You Can Do No Wrong."

Meanwhile, Don Pedro convinces the viceroy that Serafin is the real Macoco and should hang for it. He plants treasure in Serafins prop trunk to make him look like a pirate. The army arrests Serafin, and Manuelas protests cannot free him.  On the night of Serafins hanging, Manuela finally gets to look at the false evidence, and recognizes a bracelet with the same design as the wedding ring that Pedro gave her, and realizes that Pedro is the pirate.

Serafin asks to do one last show before he is hanged, and sings and dances "Be a Clown" with two fellow troupe members (the Nicholas Brothers). As a finale, Serafin plans to hypnotize Don Pedro into admitting he is Macoco, but Manuelas aunt breaks the mirror that Serafin uses to hypnotize people. Panicked, Manuela pretends to be hypnotized and sings "Love of My Life," vowing everlasting devotion to Macoco. Don Pedro, jealous, reveals himself as the true Macoco and seizes Manuela. Serafins troupe attacks Don Pedro with custard pies and juggling balls, and the lovers embrace.  Manuela joins Serafins act and the film ends with the two of them singing a reprise of "Be a Clown."

==Cast==
 
* Judy Garland as Manuela
* Gene Kelly as Serafin
* Walter Slezak as Don Pedro Vargas
* Gladys Cooper as Aunt Inez
* Reginald Owen as The Advocate
* George Zucco as The Viceroy
* The Nicholas Brothers as Specialty Dance
* Lester Allen as Uncle Capucho
* Lola Deem as Isabella
* Ellen Ross as Mercedes
* Mary Jo Ellis as Lizarda
* Jean Dean as Casilda
* Marion Murray as Eloise
* Ben Lessy as Gumbo
* Jerry Bergen as Bolo
 

==Production== The Nicholas Brothers, Fayard and Harold, dancing with Kelly. (It was the first time they had danced onscreen with a Caucasian, and while it was Kellys insistence that they perform with him, they were the ones who were punished. Essentially blackballed, they moved to Europe and did not return until the mid-60s.)
 Easter Parade, also starring Judy Garland and produced by Arthur Freed.

The production of the movie was frequently tense: Garlands struggles with ongoing prescription drug addiction led to several angry confrontations with husband/director Minnelli, presaging their divorce a few years later. The production lasted for over two months, with Garland missing 99 of the 135 shooting days.

==Reception== David Shipman, writing in his book The Great Movie Stars: The Golden Years, did describe The Pirate as being overall "a neat moneymaker, but otherwise probably the least successful of Garlands M-G-M films." 

According to MGM accounts, the film earned $1,874,000 in the US and Canada and $782,000 elsewhere, resulting in a loss to the studio of $2,290,000. 

==Soundtrack for extended CD version==
# "Main Title (Mack the Black)"
# "Niña"
# "Mack the Black" 
# "Love of My Life" (Outtake)
# "Pirate Ballet"
# "You Can Do No Wrong"
# "Be a Clown"
# "Love of My Life" (Reprise)
# "Be a Clown" (Finale)
# "Mack the Black" (Unused Version)
# "Papayas / Seraphins March" (Partial Demo) 
# "Voodoo (Outtake)"
# "Manuela (Demo)"
# "Voodoo (Demo)"
# "Niña (Demo)"
# "You Can Do No Wrong" (Demo)
# "Be a Clown" (Demo)
# Judy Garland Interview with Dick Simmons  
# Gene Kelly Interview with Dick Simmons

== References ==
 

== External links ==
*  
*  
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 