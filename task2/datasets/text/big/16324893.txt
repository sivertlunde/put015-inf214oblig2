Straight for the Heart (film)
{{Infobox Film
| name           = À corps perdu
| image          = 
| image_size     = 
| caption        = 
| director       = Léa Pool
| producer       = Denise Robert Robin Spry
| writer         = Marcel Beaulieu Léa Pool Yves Navarre (novel "Kurwenal")
| narrator       = 
| starring       = Matthias Habich Johanne-Marie Tremblay Michel Voita Jean-François Pichette
| music          = Osvaldo Montes
| cinematography = Pierre Mignot
| editing        = Michel Arcand
| distributor    = 
| released       = September 30, 1988   January 12, 1990  
| runtime        = 92 minutes
| country        =  /  French
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
À corps perdu ( /Swiss Canadian French|French-language drama film.

== Plot ==
Pierre (Habich) is a photojournalist from Montreal whos working on a reportage in Nicaragua. There he sees many people being executed and he takes photographs of them. Even of the death of a young child and of his mother crying.
Back home in Montreal, his ten-year bisexual ménage à trois (threesome) is over. Sarah (Tremblay) and David (Voita) have moved out, leaving Pierre wondering why. Pierre is haunted by his experiences and memories in war, and those of his relationship with Sarah and David. The memories in his mind are most of the times shown in black and white mouvies with emotional background-music.
After some days, when hes been stalking David and Sarah with his photocamera, he meets the young deaf-mute Quentin (Pichette). After a while hes able to begin a new life with Quentin.

== Recognition ==
* 1989
** Genie Award for Best Achievement in Art Direction/Production Design - Vianney Gauthier - Nominated
** Genie Award for Best Achievement in Cinematography - Pierre Mignot - Nominated
** Genie Award for Best Achievement in Editing - Michel Arcand - Nominated
** Genie Award for Best Motion Picture - Robin Spry, Denise Robert - Nominated
** Genie Award for Best Music Score - Osvaldo Montes - Nominated

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 
 




 
 