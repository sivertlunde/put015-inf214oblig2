Zindagi 50-50
 
 

{{Infobox film
| name           = Zindagi 50-50
| image          = Zindagi50-50 new.jpg
| caption        = Movie Poster
| director       = Rajiv S. Ruia
| producer       = Veeraj Kumar
| screenplay     = S Sachindra
| story          = Rajiv S. Ruia
| starring       = Veena Malik Riya Sen Rajan Verma Arya Babbar Supriya Kumari Rajpal Yadav Murli Sharma Atul Parchure ABC
| music          = Vivek Kar & Amjad Nadeem
| cinematography = Mukesh Maru
| editing        = Satish Patil
| studio         = Ram Gopal Productions, Spotlight International Film
| distributor    = 
| released       =  
| runtime        = 130 minutes
| country        = India
| language       = Hindi
| budget         =   
| gross          =  
}}
Zindagi 50-50 is a 2013 Bollywood film which is directed by Rajiv S. Ruia. It stars Veena Malik, Rajan Verma, Supriya Kumari, Arya Babbar, Riya Sen and Rajpal Yadav. This film dubbed is in Tamil titled Mutham Thara Vaa and in Telugu as Rangeela.   

==Cast==
*Veena Malik - Madhuri
*Riya Sen - Naina
*Rajan Verma - Birju
*Arya Babbar - Addy
*Supriya Kumari - Rupa
*Rajpal Yadav - PK Lele
*Murli Sharma - INS Pawar
*Atul Parchure - Mota
*Adi Irani
*Swati Aggarwal

==Music==
The music for Zindagi 50-50 is given by Amjad Nadeem and Vivek Kar consist of following Audio List.

==Soundtrack==
{{Infobox album
| Name = Zindagi 50-50
| Type = Soundtrack
| Artist = Amzad-Nadeem
| Cover = 
| Released = 2013
| Recorded = Feature film soundtrack
| Length = 
| Label = T-Series
| Producer = Veeraj Kumar
| Last album = Zilla Ghaziabad (2013)
| This album = Zindagi 50-50 (2013)
}}
T-Series acquired the music rights for Zindagi 50-50 
{{Track listing
| extra_column = Artist(s)
| music_credits = yes
| lyrics_credits = yes
| title1 = Zindagi 50-50 (Title track)
| extra1 = Bappi Lahiri, Antara Mitra & Gufy
| music1 = Vivek Kar
| lyrics1 = Depak Agarwal
| length1 = 
| title2 = Tu Saamne jo aaye
| extra2 = Mika Singh
| music2 = Amjad-Nadeem
| lyrics2 = Shabbir Ahmed
| length2 = 
| title3 = Sadde Naal aaja
| extra3 = Manak-E, Neha Batra & Gufy
| music3 = Vivek Kar
| lyrics3 = Ashish Pandit
| length3 = 
| title4 = Toh se naina
| extra4 = Rekha Bhardwaj
| music4 = Amjad-Nadeem
| lyrics4 = Shabbir Ahmed
| length4 =
| title5 = Rabba
| extra5 = Rahat Fateh Ali Khan
| music5 = Vivek Kar
| lyrics5 = Shabbir Ahmed
| length5 = 
| title6 = Delhi Delhi
| note6 = 
| extra6 = Dev Negi
| music6 = Vivek Kar
| lyrics6 = Dev Negi-Vivek Kar
| length6 =
}}

==Dialogue==
{| class="wikitable"
|-
! Sl. !! Star !! Dialogue
|-
| 1. |||| Zindagi 50 50 hoti hai ... kabhi haar toh kabhi jeet hoti hai
|-
| 2. || Veena Malik || Saali zindagi hai hi fifty fifty ... kisi ki kaali zindagi mein umeed ki safedi ... toh kisi ki safed zindagi mein samjaute ka kaala daag
|-
| 3. || Veena Malik || Zindagi zaroorto ke hisaab se guzaarni chahiye ... na ki khwaisho ke hisaab se ... kyun ki zaroortein saala fakir ki bhi poori ho jaati ... magar khwaishein badshaho ki bhi adhoori reh jaati hai
|}

==Critical reception==
{| class="wikitable infobox plain" style="float:right; width:23em; font-size:80%; text-align:center; margin:0.5em 0 0.5em 1em; padding:0;" cellpadding="0"
! colspan="2" style="font-size:120%;" | Professional reviews
|-
! colspan="2" style="background:#d1dbdf; font-size:120%;" | Review scores
|-
! Source
! Rating
|-
| Rediff.com
|  
|-
| colspan="2" style="text-align:center;"|   indicates that the given rating is an average rating of all reviews provided by the source
|}

Prasanna D Zore for Rediff.com has given 2/5 stars and says Zindagi 50-50 makes a brave but half-hearted effort to depict the dark cruelty that surrounds ordinary lives of three women. It fails miserably as sleaze—of language as well as skin—overtakes the proceedings.     In Short, The movie is ok for a one time watch.

This movie earned 20-40&nbsp;million in opening.

==References==
 

==External links==
*  

 
 