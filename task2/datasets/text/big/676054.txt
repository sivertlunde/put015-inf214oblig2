In the Line of Fire
 
{{Infobox film
| name           = In the Line of Fire
| image          = In the line of fireposter.jpg
| caption        = Theatrical release poster
| writer         = Jeff Maguire
| starring       = Clint Eastwood John Malkovich Rene Russo Dylan McDermott
| director       = Wolfgang Petersen
| producer       = Jeff Apple Gail Katz John Bailey
| editing        = Anne V. Coates
| music          = Ennio Morricone
| studio         = Castle Rock Entertainment
| distributor    = Columbia Pictures
| released       =  
| runtime        = 128 minutes
| country        = United States
| language       = English
| budget         = $40 million Hughes, p.80 
| gross          = $187,343,874
}} action thriller thriller film CIA agent assassinate the Secret Service assassination in 1963. The film also stars Rene Russo, Dylan McDermott, Gary Cole, John Mahoney, and Fred Thompson. The film was co-produced by Columbia Pictures and Castle Rock Entertainment, with Columbia handling distribution. Eastwood and Petersen also originally offered the role of Leary to Robert De Niro, who turned it down due to scheduling conflicts with A Bronx Tale. In the Line of Fire was the final film Eastwood starred in that he did not direct by himself until 2012s Trouble with the Curve.

==Plot==
Frank Horrigan and Al DAndrea meet with members of a counterfeiting group at a marina. The groups leader, Mendoza, tells Horrigan that he has identified DAndrea as a United States Secret Service agent, and forces him to prove his loyalty by putting a gun to DAndreas head and pulling the trigger. Horrigan shoots Mendozas men, identifies himself as an agent, and arrests the counterfeiter.
 John F. Kennedy in Dallas in 1963 in a modified version of Ike Altgens picture.  He is the only remaining active agent who was guarding the President that day, but guilt over his failure to react quickly enough to the first shot, which could have enabled him to shield Kennedy from the subsequent fatal bullet, caused Horrigan to begin drinking excessively and his family to eventually leave him.

Horrigan receives a phone call from the tenant, who calls himself "Booth". He tells Horrigan that like John Wilkes Booth and Lee Harvey Oswald, he plans to kill the President, who is running for reelection and is making many public appearances around the country. Horrigan asks to return to the Presidential Protective Detail despite his age, where he begins a relationship with fellow agent Lilly Raines.
 tapped and Lafayette Park, matches the print, but the persons identity is classified, so the bureau cannot disclose it to the Secret Service; but the FBI does notify the CIA.
 composite Improvised zip gun to evade metal detectors and hides the bullets and springs in a keyring.

DAndrea confides to Horrigan that he is going to retire immediately because of nightmares about the Mendoza incident, but Horrigan is able to dissuade him. After Leary taunts Horrigan about the President facing danger in California, the assassin kills DAndrea after the two agents chase him across Washington rooftops. Horrigan asks Raines to reassign him to the protective detail as the President visits Los Angeles, but a television crew films him mistaking a bellboy at the Westin Bonaventure Hotel for a security threat, and he must again leave the detail.

Horrigan connects Leary to a bank employees murder and learns that Leary, who has made a large campaign contribution, is among the guests of a campaign dinner at the hotel. He sees the President approach the assassin and jumps in front of his bullet. As the Secret Service quickly removes the President, Leary uses Horrigan&mdash;who is wearing a bulletproof vest&mdash;as a hostage to escape to the hotels external elevator. The agent uses his earpiece to tell Raines and sharpshooters where to aim; although they miss Leary, Horrigan defeats him. The would-be-assassin chooses to fall to his death from the elevator.

Horrigan, now a hero, retires as his fame no longer lets him do his job. He and Raines find a farewell message from Leary on Horrigans answering machine. Horrigan and Raines leave the house and visit the Lincoln Memorial.

==Cast==
* Clint Eastwood as Agent Frank Horrigan, a Secret Service agent still haunted by his failure to save John F. Kennedy and now seeks redemption.
* John Malkovich as Mitch Leary, a psychotic assassin determined to kill the President of the United States. 
* Rene Russo as Lilly Raines, a Secret Service agent and Franks love interest.
* Dylan McDermott as Al DAndrea, Franks partner.
* Gary Cole as Bill Watts, Franks superior, wary of Franks reputation.
* Fred Thompson as Harry Sargent, the White House Chief of Staff, also distrusting of Frank.
* John Mahoney as Secret Service Director Sam Campagna, Franks colleague and friend in the Secret Service.
* Gregory Alan Williams as Matt Wilder codenamed "Traveler"), Learys target.
* Sally Hughes as the First Lady of the United States
* Tobin Bell as Mendoza, a counterfeiter, apprehended by Frank in the films opening scene.
* Clyde Kusatsu as FBI agent Jack Okura
* Steve Hytner as FBI agent Tony Carducci
* Patrika Darbo as Pam Magnus John Heard as Prof. Riger
* Joshua Malina as Secret Service agent Chavez
* Steve Railsback as CIA Agent David Coppinger (Uncredited)

==Production==
Producer   began developing In the Line of Fire in the mid-1980s. His previous credits included Zapped!, a teen comedy starring Scott Baio. For Fire, Apple made several research trips to Secret Service headquarters in Washington, D.C. and recruited Secret Service agents, including Robert Snow, to consult. Snow helped lead Apple to the real Secret Service Agent the Clint Eastwood character is modeled on. The Secret Service also gave an official nod to Apples project.  

Apple planned on making a movie about a U.S. Secret Service Agent on detail during the Kennedy assassination since his boyhood. Apple was inspired and intrigued by a vivid early childhood memory of meeting Vice President Lyndon B. Johnson in person surrounded by Secret Service Agents with earpieces in dark suits and sunglasses. The concept later struck Apple as an adolescent watching televised replays of the assassination of President John F. Kennedy. In 1991, writer Jeff Maguire came aboard and completed the script that would become the movie.   

Filming began in late 1992 in Washington, D.C.  Scenes in the White House were filmed on a pre-built set, while an Air Force One interior set had to be built at a cost of $250,000. 
 reelection campaign. George Herbert Bill Clintons 1992 campaign events. 

The movie also uses digitized imagery from 1960s Clint Eastwood movies inserted into the Kennedy assassination scenes. As Jeff Apple described it to the Los Angeles Times, Clint "gets the worlds first digital haircut".   

==Reception==

===Critical response===
In the Line of Fire was released in United States theaters in July 1993. The film received mostly positive reviews, receiving a 95% "Certified Fresh" positive rating by top film critics based on 64 reviews with an average rating of 7.8 out of 10 and a 73% positive audience rating based on 53,265 reviews.  
Roger Ebert gave the film three and a half stars out of four, writing: "Most thrillers these days are about stunts and action. In the Line of Fire has a mind."   

===Box office===
The film was a considerable financial success as well, earning $187,343,874 worldwide   The Numbers. Retrieved September 6, 2013.  (over $102 million in North America and $85.1 million in other territories), while its budget was about $40 million.

===Accolades===
   Academy Award Nomination for Best Actor in a Supporting Role (John Malkovich)
* 1994 Academy Award Nomination for Best Film Editing (Anne V. Coates)
* 1994 Academy Award Nomination for Best Writing, Screenplay (Jeff Maguire)
* 1994 ASCAP Award for Top Box Office Films (Ennio Morricone) Won BAFTA Film Award Nomination for Best Actor in a Supporting Role (John Malkovich)
* 1994 BAFTA Film Award Nomination for Best Editing (Anne V. Coates)
* 1994 BAFTA Film Award Nomination for Best Screenplay (Jeff Maguire)
* 1994 Chicago Film Critics Association Award Nomination for Best Supporting Actor (John Malkovich)
* 1994 Golden Globe Award Nomination for Best Performance by an Actor in a Supporting Role (John Malkovich)
* 1994 MTV Movie Award Nomination for Best Villain (John Malkovich) {{cite web|title=Awards for In the Line of Fire |publisher=Internet Movie Database 
|url=http://www.imdb.com/title/tt0107206/awards |accessdate=March 13, 2012}}    
* AFIs 100 Years...100 Heroes and Villains (2003):
** Mitch Leary – Nominated Villain

==References==
;Citations
 

;Bibliography
 
*  
 

==External links==
 
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 