Bunny Lake Is Missing
 
{{Infobox film
| name           = Bunny Lake Is Missing
| image          = Bunny Lake Is Missing.jpg
| caption        = film poster by Saul Bass
| director       = Otto Preminger
| producer       = Otto Preminger
| writer         = 
| based on       =  
| screenplay     = John Mortimer Penelope Mortimer
| starring       = Laurence Olivier Carol Lynley Keir Dullea Martita Hunt The Zombies Noël Coward
| music          = Paul Glass
| cinematography = Denys N. Coop
| editing        = Peter Thornton
| distributor    = Columbia Pictures
| released       = 1965
| country        = United Kingdom
| runtime        = 107 minutes
| language       = English
| budget         =
}}
 psychological thriller novel of the same name by Merriam Modell. The score is by Paul Glass and the opening theme is often heard as a refrain. The Zombies also appear in a television broadcast.

Dismissed by both critics and Preminger as insignificant upon its release in 1965, the film received a strong review by critic Andrew Sarris.  The movie was released on DVD in 2005 (Region 1) and 2007 (Region 2).

==Plot==
American single mother Ann Lake (Carol Lynley), recently moved to London from New York, arrives at the Little Peoples Garden preschool to collect her daughter, Bunny, but the child has mysteriously disappeared. An administrator recalls meeting with Ann, but claims never to have seen the missing child. Ann and her brother Stephen (Keir Dullea) search the school and find a sinister woman living upstairs who claims she collects childrens nightmares. In desperation, the Lakes call the police, and Superintendent Newhouse (Laurence Olivier) arrives on the scene.

They find that all of Bunnys possessions have been removed from the Lakes home. Ann cannot understand why anyone would do this and reacts emotionally. Superintendent Newhouse begins to suspect that Bunny Lake does not exist after he learns that "Bunny" was the name of Anns imaginary childhood friend.

Anns landlord (Noël Coward), an aging actor, attempts to seduce her. Newhouse decides to become better acquainted with Ann in order to learn more about Bunny. He takes her to a local bar where he plies her with brandy.
 doll hospital for repairs. Regarding the doll as proof of Bunnys existence, she frantically rushes to the doll hospital late at night and retrieves the doll. Stephen knocks out Ann and burns the doll. He takes Ann to a hospital and tells the desk nurse that Ann has been hallucinating about a missing girl who does not exist. Ann is sedated and put under observation. 

Later, Ann wakes and escapes from the hospital. She discovers Stephen burying Bunnys possessions; he has bound and sedated the child and hidden her in the boot of his car. Stephen implies an incestuous interest in his sister and complains that Bunny has always come between them; because he believes Ann loves Bunny more than him, the child threatens Stephens dream of a future with Ann. Realizing that her brother is mad, Ann plays childhood games with him to distract him.   Newhouse, having discovered that Stephen had lied to the police about the ship that brought the Lakes to England, arrives in time to rescue Ann and Bunny and apprehend Stephen.

==Cast==
{| class="wikitable"
|- bgcolor="CCCCCC"
! Actor !! Role
|-
| Laurence Olivier || Supt. Newhouse
|-
| Carol Lynley || Ann Lake
|-
| Keir Dullea || Stephen Lake
|-
| Martita Hunt || Ada Ford
|-
| Anna Massey || Elvira Smollett
|-
| Clive Revill || Sergeant Andrews
|-
| Finlay Currie || The Doll Maker
|-
| Lucie Mannheim || The Cook
|-
| Noël Coward || Horatio Wilson
|-
| Adrienne Corri || Dorothy
|-
| Megs Jenkins || Sister
|- 
| Delphi Lawrence || 1st Mother  
|-
| David Oxley || Doctor
|- 
| Suky Appleby || Bunny Lake
|}

==Production details==
Adapting the original novel, Preminger moved the story from New York to London, where he liked working. His dark, sinister vision of London made use of many real locations; Barry Elders Doll Museum in Hammersmith stood in for the dolls hospital, the Little Peoples Garden School used a real school in Hampstead, and the Frogmore End house was one that had belonged to novelist Daphne du Mauriers father. Preminger had found the novels denouement lacking in credibility so he changed the identity of the would-be murderer, which needed many re-writes from his British husband-and-wife scriptwriters John Mortimer and Penelope Mortimer before the famously demanding director was satisfied. 

As with its thematic predecessor, Psycho (1960 film)|Psycho, audiences were not admitted after the films start, which was not common practice at the time.  This was heavily emphasized in the films promotion, including on the poster, which warned "No One Admitted While the Clock is Ticking!"
 rock band The Zombies are featured in the credits and on the films poster for their contribution of three songs to the films soundtrack: "Remember You", "Just Out of Reach" and "Nothings Changed".  The band is prominently featured performing on a television in the pub where Supt. Newhouse meets with Ann, and "Just Out of Reach" plays on a janitors radio as Ann escapes from the hospital.  With Preminger present in the studio, the band recorded a two-minute radio ad set to the tune of "Just Out of Reach" that promoted the films release and urged audiences to "Come on time!" in keeping with the films no-late-admissions policy.  These efforts represent an early instance of the now-common Hollywood practice of promotional tie-ins with popular musical acts. 

==References==
 

* Maria DiBattista (Princeton University): "Afterword". In: Evelyn Piper: Bunny Lake Is Missing (Femmes Fatales: Women Write Pulp) (The Feminist Press at The City University of New York: New York, 2004) 198-219 (ISBN 1-55861-474-5) (includes a discussion of the differences between Pipers novel and Premingers film version).

==Home Video Releases==
On August 2, 2014, Twilight Time Studio announced an upcoming Blu-ray edition of the film, which was eventually released on November 11. 
 
 

==External links==
 
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 