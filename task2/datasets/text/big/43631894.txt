A Man's Man (1929 film)
{{infobox film
| title          = A Mans Man
| image          =
| imagesize      =
| caption        =
| director       = James Cruze
| producer       = Metro Goldwyn Mayer
| writer         = Patrick Kearney(play) Forrest Halsey(writer/scenario)
| starring       = William Haines
| cinematography = Merritt B. Gerstad
| editing        = George Hively
| distributor    = MGM
| released       = May 25, 1929
| runtime        = 80 minutes
| country        = USA
| language       = Silent/English
}}
A Mans Man is a lost   1929  silent film produced and distributed by MGM and directed by James Cruze. The movie starred William Haines and was released with a soundtrack of music and effects. It is based on a Broadway play, A Mans Man by Patrick Kearney.  This is Haines last silent film. 

==Cast==
*William Haines - Mel
*Josephine Dunn -Peggy Sam Hardy - Charlie
*Mae Busch - Violet John Gilbert - Himself
*Greta Garbo - Herself
*Gloria Davenport - Annie

uncredited
*Delmer Daves
*Fred Niblo

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 


 