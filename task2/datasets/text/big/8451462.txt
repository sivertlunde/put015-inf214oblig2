Adiós muchachos
 
{{Infobox film
| name           = Adiós muchachos
| caption        = 
| director       = Armando Bo
| producer       = 
| writer         = Rafael García Ibáñez
| starring       = Pola Alonso   Arturo Arcari   Héctor Armendáriz   Lina Bardo Alfredo Dalton Juancito Díaz
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 19 January 1954
| runtime        = 85 minutes
| country        = Argentina Spain
| language       = Spanish
| budget         = 
| followed_by    = 
| amg_id         = 
| imdb_id        = 
}} Argentine film directed by Armando Bo and written by Rafael García Ibáñez. The film starred Pola Alonso and Arturo Arcari.

==Cast==
*Pola Alonso
*Arturo Arcari
*Héctor Armendáriz
*Lina Bardo
*Alfredo Dalton
*Juancito Díaz
*Francisco Pablo Donadio
*Rolando Dumas
*Carlos A. Dusso
*José María Fra
*Arturo Huber
*Jorge Leval
*Eugenio Novile
*Juan Carlos Prevende
*Virginia Romay
*Marcelo Ruggero
*Héctor Silva
*Silvia Sisley
*José Soriano
*Osvaldo Tempore
*Mateo Velich

==External links==
*  

 
 
 
 
 


 