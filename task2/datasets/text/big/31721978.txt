.303 (film)
 
{{Infobox film
| name           = .303
| image          = 
| alt            =  
| caption        = 
| director       = David Serge
| producer       = Matthew Pullicino David Serge
| writer         = David Serge
| starring       = Mark Mifsud
| music          = 
| cinematography = David Serge
| editing        = Daniel Lapira
| studio         = 
| distributor    = 
| released       =  
| runtime        = 10 minutes
| country        = Malta
| language       = English
| budget         = 
| gross          = 
}}
.303 is a 2009 short film directed by David Serge and produced by The Bigger Picture Malta.

It has won the award for Best Overall Production, Best Cinematography and the Local Jury Award at the Malta International TV Short Film Festival.  It also made the Official Selection at the Palm Springs International Short Fest,  California as well as the Bodega Bay International Film Festival. 

==Plot==

Sicily, 1943. A plane full of allied soldiers comes to invade Sicily. One of them is given a .303 calibre cartridge by his friend, which is supposed to bring him luck.

==Sources==
 

==External links==
*  
*  

 
 
 