Dil Bechara Pyaar Ka Maara
{{Infobox film
| name           = Dil Bechara Pyaar Ka Maara
| image          = Dil Bechara Pyaar Ka Maara.jpg
| image_size     =
| caption        = DVD
| director       = Onkar Nath Mishra
| producer       =
| writer         = Onkar Nath Mishra
| narrator       =
| starring       = Vikas Kalantri  Aslam Khan  Aman Sondhi Divya Palat  Jonita Doda Rajpal Yadav  Mallika Kapoor
| music          = Nikhil-Vinay
| cinematography =
| editing        =
| distributor    =
| released       = 16 December 2004
| runtime        =
| country        = India
| language       = Hindi
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}
Dil Bechara Pyar Ka Maara is a Hindi film released in the year 2004 in film|2004, directed and written by Onkar Nath 
Mishra and starring Vikas Kalantri, Aslam Khan, Aman Sondhi, Divya Palat, Jonita Doda, Rajpal Yadav and Mallika Kapoor.

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Bhago Bhago"
| Sriram Ayyer 
|-
| 2
| "Chudion Ne"
| Shreya Ghoshal, Priya Bhattacharya, Shaswati Fukan 
|-
| 3
| "Dil Bechara Pyaar Ka Maara"
| Udit Narayan, Shreya Ghoshal
|-
| 4
| "Dil Bechara Pyaar Ka Maara (Male)"
| Udit Narayan
|-
| 5
| "Hari Tum"
| Priya Bhattacharya, Sriram Ayyer
|-
| 6
| "Jane Kya Asar Tera Hua"
| Sonu Nigam, Shreya Ghoshal
|-
| 7
| "Jane Kya Asar Tera Hua (Female)"
| Shreya Ghoshal
|-
| 8
| "Manava Tu"
| Sriram Ayyer 
|-
| 9
| "Masoom Sa Ek Chehra"
| Arnab Chakravorty 
|-
| 10
| "O Baby" 
| Sriram Ayyer 
|}
==External links==
* , review and miscellaneous info
*  

 
 
 
 