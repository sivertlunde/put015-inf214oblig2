Zakhmi Dil (1994 film)
 
 
{{Infobox film
| name = Zakhmi Dil
| image =
| writer =
| starring = Akshay Kumar Ashwini Bhave Ravi Kishan
| director = Raju Subramaniam
| producer =
| music = Rishi Raj
| lyrics = Sameer
| released = 25 November 1994
| language = Hindi
| budget =  
| gross =  
}}

Zakhmi Dil is an Indian film directed by Raju Subramaniam and released in 1994. It stars Akshay Kumar, Ravi Kishan and Ashwini Bhave.

==Cast==
* Akshay Kumar ... Jaidev Anand
* Ashwini Bhave ... Gayatri
* Ravi Kishan ... Abhimanyu
* Moon Moon Sen ... Mala
* Raza Murad ... D.K. Bindu ... Mrs. Braganza
* Shashi Kiran ... Parasmal Kriplani P.K.

==Soundtracks==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| Mujhko Bhi Sang Le Chal
| Sadhana Sargam
|-
| 2
| Payaliya Geet Sunayegi 	
| Udit Narayan, Kavita Krishnamurthy
|-
| 3
| Ae Meri Zindagi
| Kumar Sanu, Sadhana Sargam
|-
| 4
| Khushiyon Ka Mausam
| Kumar Sanu
|-
| 5
| Sakhi Mujhe Jaana Hai
| Ila Arun, Jayshree
|-
| 6
| Phool Jahan Khiltey Hain
| Kumar Sanu, Sadhana Sargam
|-
| 7
| Choodi Khankaoun
| Kavita Krishnamurthy
|}

==External links==
*  

 
 
 

 