Bommalattam
{{Infobox film
| name           = Bommalattam
| image          = Bommalattam.jpg
| caption        = 
| director       = Bharathiraja
| producer       = 
| writer         = Bharathiraja
| narrator       = Bharathiraja
| starring       =  
| music          = Songs:  
| cinematography = B. Kannan
| editing        = k palanivel
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Tamil
| budget         = 
| gross          = 
}}
Bommalattam  is a Tamil film directed by the Bharathiraja starring Arjun Sarja and Nana Patekar in the lead roles. The film was dubbed and released in Hindi as Cinema and in Telugu as Rana. This marked Nana Patekars Tamil film debut.

==Plot==
The opening scene is shown in a way that director Rana(Nana Patekar) shoots a scene in his upcoming film with his films heroine. As he is not happy with the way the heroine behaves, he decides to find another heroine. In search of the heroine, he finally finds Trishna(Rukmini Vijayakumar) suited for his film. But he never discloses the identity of his heroine to media and the wallpapers of the film also doesnt show the hero as well as the heroine of the film.Finally the film gets completed and Rana needs to attend a press meet regarding his films release. But he does not go the press meet and he is shown to have had an illegal relationship with Trishna. At this instant the producer of the film calls Rana and the media come to know where he is and with whom. So the whole media people gathers at the hotel where Rana stays and he manages to escape from them and get into a car.They start chasing him and finally he kills Trishna by creating an accident like situation. In this situation Vivek(Arjun Sarja), a CBI officer takes in charge for this murder case, along with two previous murder cases which was also suspected to be done by Rana. As a suspicion he is arrested and taken into CBI custody.There Vivek starts his interrogation.In between this a poet(Kajal Aggarwal) who is also a crazy fan of Rana is shown as Viveks lover and also she was an assistant to Rana during his film shooting.Finally, during interrogation, Rana reveals of how he found Trishna and their relationship. She was a dancer, who used to dance in temples and small-time shows across Andhra Pradesh. He hires her as his new muse, and in the shooting spot, they are shown to share a very intimate relationship, fueling existing rumors that he is a womanizer. When Trishna is left without a place to sleep, Rana offers her a place in his room, and witnessing this, another unit member informs Ranas wife (Ranjitha). The next morning, as Rana is once again shown to share a very close rapport with Trishna, his wife arrives, beats up the hairdresser, and throws a tantrum, accusing him as a womanizer. He however continues shooting, appearing to be unperturbed by the incident but apparently cries lightly while instructing his actors, something which his assistant notices. He and his assistant are also shown to share a special and close relationship, as she comforts him later on. However, at the same village in which they are shooting, they face troubles in the form of the village chief (Manivannan), who is notorious and constantly lusting over Trishna. He states in rage at one point that he would kill the chief should he cause more inconvenience. The next day, the chief is murdered at a nearby quarry. Vivek recalls this and states that Rana committed this murder because he cant stand the chiefs behavior towards Trishna, but Rana neither agrees or denies this statement.

Rana is later admitted to the hospital as he faints shortly after that interrogation, but Vivek pursues him even in the hospital, and recollects the incident of the second murder. After finishing their schedule at the village, the unit head to Malaysia to continue their shooting for Cinema. There, Rana has to meet another nuisance in terms of the financiers son, who is also the films second hero. He flirts around with Trishna and tries to constantly approach her, of which she is obviously uncomfortable with, and shortly afterwards, he is found murdered as well. Once again, Rana neither denies or agrees as Vivek accuses him of murder. Ranas assistant upon witnessing what Vivek is doing confronts him and accuses him of trying to pin down Rana in jealousy. She offers her body in exchange of Rana being left alone, but Vivek reveals to her the complication that exist in the investigation. The badly burnt body in the car which fell from the cliff belongs to a man and not a woman, and all the evidence that exists at the crime scene are all fake, which are used for cinema.

With the mystery lingering, Rana is released due to the absence of strong evidence to convict him. He goes off to live in a secluded bungalow with his assistant, who describes theirs as a special relationship. Vivek arrives there too and finally uncovers the mystery. Trishna is revealed as a boy, and not a girl. Babu, being his real name, comes from a very poor background and his mother raised him as a woman since childhood thus his demeanor has become womanly since small. Rana, in him, sees an opportunity to take up a unique challenge by introducing him as a heroine and at the same time provide financial aid to his struggling family. Babu killed the village chief after the chief discovers Babus gender identity and demands that Babu, despite being a boy, still sleep with him in return of the truth not being revealed in public. And when the financiers son tells him that he has taken his picture while he is bathing (which he actually did not), Babu became insecure once again and kills him too. Rana asks Vivek to give the boy the least available sentence, but Vivek, overwhelmed by the truth, decides to let the boy go. Rana thanked Vivek, but Vivek in return says Rana is a greater human being than he is a director.

==Cast==
*Arjun Sarja
*Nana Patekar
*Kajal Aggarwal
*Rukmini Vijayakumar Vivek
*Manivannan
*Ranjitha

==Production==
The film was launched in Malaysia in 2006.  This was Nana Patekars debut film in Tamil, scenes involving him and Arjun were shot in Chennai.  Nana and Bharathi fought in shooting, replying to this matter Bharathi said: "We fought with each other a couple of times. However the fights helped us to sharpen our thoughts and to shape the movie in a nice way".  Nagravi of Insight media tiedup with the film and bought the rights.  Bharathiraja went on to say that it is one of the best films he had made.  This was originally the debut film for Kajal Agarwal since this film was delayed, Pazhani became her first release. Voice of nana has been dubbed by actor Nizhalgal Ravi. 

==Reviews==
Nowrunning wrote:"Like aged and mellowed spirits, director Bharati Raja has matured and levitated towards one of his first loves - a whodunit".  Behindwoods wrote: "Bommalattam is a perfectly crafted and executed investigative thriller. The surprise factor being Bharathiraja’s tautly woven screenplay – it negates the chance of tedium, even for a fraction of a second".  Sify wrote: "hats off to Bharathiraja for creating a taut thriller which is a masterpiece of moods, anxieties and dread. Quite simply, unmissable".  Rediff wrote: " Bharathirajas screenplay lacks punch. He seems to have been confused about whether to give importance to the characters themselves, or the thriller portion.  Hindu wrote: "The pluses of ‘Bommalattam’ place Bharatiraaja on a pedestal. The minuses play spoilsport". 

==References==
 

 

 
 
 
 
 
 