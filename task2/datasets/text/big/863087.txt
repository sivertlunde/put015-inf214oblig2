Crumb (film)
{{Infobox film
| name = Crumb
| image = Crumb Movie Poster.jpg
| caption = Theatrical release poster
| director = Terry Zwigoff
| producer = Terry Zwigoff Lynn ODonnell
| starring = Robert Crumb
| music = David Boeddinghaus
| cinematography = Maryse Alberti
| editing = Victor Livingston
| distributor = Sony Pictures Classics The Criterion Collection
| released =  
| runtime = 120 minutes
| country = United States
| language = English
| gross = $3,174,695
}} underground cartoonist USA on April 28, 1995, having been screened at film festivals the previous year. Jeffery M. Anderson (later critic for the San Francisco Examiner) placed the film on his list of the ten greatest films of all time, labeling it "the greatest documentary ever made." 

==Synopsis== Maxon and Charles Crumb|Charles, his wife and children (his sisters declined to be interviewed). Though Zwigoff had the consent of the Crumb brothers, some questioned the ability of the more disturbed brothers to provide that consent. 

==Production==
Robert Crumb initially did not want to make the film, but eventually agreed. There was a rumor, accidentally created by  . 

During the nine years that it took to make the documentary Zwigoff said he was “averaging an income of about $200 a month and living with back pain so intense that I spent three years with a loaded gun on the pillow next to my bed, trying to get up the nerve to kill myself.” 

==Reception==
Crumb was met with wide acclaim from critics, earning a 95% rating on Rotten Tomatoes.  The late critic Gene Siskel rated Crumb as the best film of the year.  Roger Ebert gave the film four (of four) stars, writing that "Crumb is a film that gives new meaning to the notion of art as therapy."   In The Washington Post, Desson Howes review was similarly positive.  The San Francisco Chronicle rated the film as "wild applause", as critic Edward Guthmann called the film "one of the most provocative, haunting documentaries of the last decade." He also noted that Robert Crumb and wife Aline had drawn a "scornful" cartoon about the film in The New Yorker. 

Critic Jeffrey M. Anderson called it "one of the most brave and honest films Ive ever seen", and listed its characteristics as those of "great documentary", giving it four (of four) stars. 

Despite strong reviews, Crumb was not nominated for the Academy Award for Best Documentary Feature (the nominating committee reportedly stopped watching the film after only twenty minutes). The Oscar snub of Crumb, and the same years equally acclaimed Hoop Dreams, caused a media furor which forced the Academy of Motion Picture Arts and Sciences to revamp its documentary nomination process.  Zwigoff stated in an interview that: “The Academy Award thing had much more to do with the fact that at the time, a lot of the documentary membership was made up of distributors of documentary films. The rules have changed since then. But they would just vote for the films they distributed because it was in their financial interest to do so.” He continued: “I just assumed they were disgusted with the film." 

In 2008, Entertainment Weekly named Crumb the 14th best film of the last 25 years.  In 2012 Slant Magazine ranked the film #74 on its list of the 100 Best Films of the 1990s, calling it "Arguably the greatest of all nonfiction films." 

===Awards===
The film won several major critical accolades: Grand Jury Prize Documentary 1995, Sundance Film Festival  ( )  
*Best Documentary 1995, National Board of Review Guthmann, Edward (December 20, 1995).  . San Francisco Chronicle. Retrieved 2012-11-17   Best Documentary 1995, New York Film Critics   Best Non-fiction film 1995, Los Angeles Film Critics  
*Best documentary 1995, Boston Film Critics  
*Feature Documentary, 1995, International Documentary Association. Los Angeles.   Best Nonfiction Film 1995, National Society of Film Critics  

==References==
 

==External links==
*  
*  
*  
*   review. The Washington Post.

 
 
{{succession box
| title= 
| years=1995
| before=Freedom on My Mind
| after= }}
 

 
 

 
 
 
 
 
 
 
 
 
 
 
 