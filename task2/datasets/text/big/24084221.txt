Tokyo Tower (film)
{{Infobox film
| name = Tokio Tower
| image = 
| caption = 
| director = Takashi Minamoto
| producer = 
| writer = Kaori Ekuni (novel), Takashi Minamoto
| starring =  
| music = 
| cinematography = 
| editing = 
| distributor = 
| released =  
| runtime = 125 minutes
| country = Japan
| language = Japanese
| budget = 
| gross = 
}}
Tokyo Tower is a 2005 Japanese romantic film directed and written by Takashi Minamoto. The film is based on a novel by Kaori Eguni.

==Plot== falls in love with a woman who is
not only married but also 20 years older. Complicating
matters even further, she also happens to be a good friend something is
missing.

The story unfolds in tandem with that of Toru’s
friend, Koji, who also falls in love with a married woman.

The two couples struggle to deal with the complexities of
their choices in an effort to find a balance between
the forces of love and the reality surrounding them.

==Cast==
* Junichi Okada	- 	Toru Kojima
* Hitomi Kuroki	- 	Shifumi
* Jun Matsumoto	- 	Koji
* Shinobu Terajima	- 	Kimiko
* Kento Handa	
* Aya Hirayama		
* Rosa Kato		
* Goro Kishitani		
* Hiroyuki Miyasako		
* Kimiko Yo

==External links==
*   (Inactive, archived at  )
*  
*  

 
 
 
 


 
 