The Rest Is Silence (1959 film)
{{Infobox film
| name           = The Rest Is Silence
| image          = 
| caption        = 
| director       = Helmut Käutner
| producer       = Harald Braun Helmut Käutner Wolfgang Staudte
| writer         = Helmut Käutner William Shakespeare
| starring       = Hardy Krüger
| music          = 
| cinematography = Igor Oberberg
| editing        = Klaus Dudenhöfer
| distributor    = 
| released       =  
| runtime        = 103 minutes
| country        = West Germany
| language       = German
| budget         = 
}}

The Rest Is Silence ( ) is a 1959 West German crime film directed by Helmut Käutner. It was entered into the 9th Berlin International Film Festival.   

==Cast==
* Hardy Krüger - John H. Claudius
* Peter van Eyck - Paul Claudius
* Ingrid Andree - Fee von Pohl
* Adelheid Seeck - Gertrud Claudius
* Rudolf Forster - Dr. von Pohl
* Boy Gobert - Mike R. Krantz
* Rainer Penkert - Major Horace
* Heinz Drache - Herbert von Pohl
* Charles Régnier - Inspector Fortner
* Siegfried Schürenberg - Johannes Claudius Richard Allan - Stanley Goulden
* Josef Sieber - Werks-Pförtner
* Robert Meyn - Dr. Voltman
* Erwin Linder - Direktor
* Werner Schumacher - Werks-Fahrer

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 

 
 