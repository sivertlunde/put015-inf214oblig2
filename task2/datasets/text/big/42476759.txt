Rich Hill (film)
{{Infobox film
| name           = Rich Hill
| image          = Rich Hill poster.jpg
| caption        = Sundance film poster
| director       = Tracy Droz Tragos Andrew Droz Palermo
| producer       = Tracy Droz Tragos Andrew Droz Palermo David Armillei
| music          = Nathan Halpern
| cinematography = Andrew Droz Palermo
| editing        = Jim Hession
| studio         =  The Orchard Independent Lens
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         = 
}}
Rich Hill is a 2014 American documentary film co-produced and directed by Andrew Droz Palermo and Tracy Droz Tragos.  The film premiered in the competition category of U.S. Documentary Competition program at the 2014 Sundance Film Festival on January 19, 2014.  It won the U.S. Grand Jury Prize: Documentary Award at the festival.  
 The Orchard and Independent Lens acquired distribution rights to the film. Independent Lens broadcast the film nationally on PBS, while The Orchard distributed the film in the U.S. and Canada outside of linear broadcast.   The film was released on August 1, 2014 in the United States.  

==Plot== rural American town.

==Reception==
The film received positive response from critics. Review aggregator  , which assigns a weighted mean rating out of 100 reviews from film critics, the film holds an average score of 74, based on 14 reviews, indicating a Generally Favorable response. 
 Variety called it "An open-hearted portrait of impoverished American life."  Duane Byrge of The Hollywood Reporter gave the film a positive review and called it "A hard-eyed but empathetic glimpse into the hardscrabble lives of struggling Missouri folk."  Katie Walsh from Indiewire in her review said: "A truly moving and edifying film, Rich Hill is the type of media object that could and should be put in a time capsule for future generations."  Dan Schindel in his review for Movie Mezzanine said "  A wonderful look at pubescent melancholy and rural living." 

==Accolades==
  
 
{| class="wikitable sortable"
|-
! Year
! Award
! Category
! Recipient
! Result
|-
| rowspan="3"| 2014 Sundance Film Festival
| U.S. Grand Jury Prize: Documentary
| Tracy Droz Tragos and Andrew Droz Palermo
|  
|-
| Sarasota Film Festival
| Documentary Directing: Special Jury Prize
| Tracy Droz Tragos and Andrew Droz Palermo
|   
|-
| Kansas City FilmFest 
| Best Heartland Feature Documentary
| Tracy Droz Tragos and Andrew Droz Palermo
|   
|}
 

==References==
 

==External links==
* 
* 
* 
*  

 
 
{{Succession box
| title= 
| years=2014
| before=Blood Brother
| after=The Wolfpack}}
 

 
 
 
 
 
 