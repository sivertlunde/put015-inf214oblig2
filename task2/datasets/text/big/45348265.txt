Dikshul
{{Infobox film
| name           = Dikshul
| image          = 
| image_size     = 
| caption        = 
| director       = Premankur Atorthy
| producer       = New Theatres
| writer         = 
| narrator       = 
| starring       = Tulsi Chakraborty Mihir Bhattacharya Chhabi Biswas Sailen Chowdhary Radharani
| music          = Pankaj Mullick
| cinematography = Rabi Dhar
| editing        = 
| distributor    =
| studio         = New Theatres, Ltd.
| released       = 12 June 1943
| runtime        = 
| country        = India Bengali
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}} Indian Bengali Bengali film directed by Premankur Atorthy.    The film was produced by New Theatres Ltd, Calcutta.  Its music direction was by Pankaj Mullick and the cinematographer was Rabi Dhar. The lyricist for the film was Kazi Nazrul Islam who was famous as the Bidrohi Kavi (Rebel Poet).    The film marked the entry of actress and singer Binota Roy as a playback singer.  The cast included 
Mihir Bhattacharya, Chhabi Biswas, Tulsi Chakraborty, Ashu Bose, Sailen Chowdhury, Harimohan Bose and Radharani. 

==Cast==
* Chhabi Biswas
* Anjali Ray
* Sailen Chowdhury
* Renuka Ray
* Radharani
* Tulsi Chakraborty
* Mihir Bhattacharya
* Ashu Bose
* Harimohan Bose
* Naresh Bose
* Manorama
* Ushabati

==References==
 

==External links==
* 

 

 
 
 

 