Cage (film)
{{Infobox film
| name           = Cage
| image          = Cage 1989 poster.jpg
| image_size     = 200px
| caption        = 1989 Theatrical Poster
| director       = Lang Elliott
| producer       = Lang Elliott
| writer         = Hugh Kelley
| starring       = Reb Brown Lou Ferrigno
| music          = Michael Wetherwax
| cinematography = Jacques Haitkin
| editing        = Mark S. Westmore
| studio         = Cage Productions Lang Elliott Entertainment
| distributor    = New Century Entertainment Image Organization 
| released       =  
| runtime        = 100 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $618,178 (USA)
| followed by    = 
}}
 martial arts action film  starring Reb Brown and Lou Ferrigno.

==Plot==
A GI in Vietnam saves his buddys life, but in the process is shot in the head. The injury results in brain damage to the point where he basically has a childs brain in a (very large) mans body. When they get out of the army the two open up a bar together, but some local gangsters make things tough for them after they refuse to take part in brutal "cage" matches where fighters battle to the point of serious injury and/or death

==Cast==
* Lou Ferrigno as Billy Thomas
* Reb Brown as Scott Monroe
* Michael Dante as Tony Baccola
* Mike Moroff as Mario
* Marilyn Tokuda as Morgan Garrett
* Al Leong as Tiger Joe
* James Shigeta as Tin Lum Yin
* Branscombe Richmond as Diablo
* Tiger Chung Lee as Chang
* Al Ruscio as Costello
* Daniel Martine as Mono
* Rion Hunter as Chao Tung
* Dana Lee as Pang
* Maggie Mae Miller as Meme
* Paul Sorensen as Matt
* Danny Trejo as Costellos Bodyguard (uncredited)

==Reception==
 

The movie received a modest reception from critics. 

== References ==
 

==External links==
* 

 
 
 
 


 