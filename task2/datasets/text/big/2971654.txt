Lucky You (film)
{{Infobox film
| name           = Lucky You
| image          = Lucky you.jpg
| caption        = Promotional poster for Lucky You
| director       = Curtis Hanson
| producer       = Denise Di Novi Carol Fenelon
| screenplay     = Curtis Hanson Eric Roth
| story          = Eric Roth
| starring       = Eric Bana Drew Barrymore Robert Duvall Debra Messing Jean Smart
| music          = Christopher Young
| cinematography = Peter Deming
| editing        = William Kerr Craig Kitson Di Novi Deuce Three Productions Warner Bros. Pictures
| released       =  
| runtime        = 124 minutes
| country        = United States
| language       = English
| budget         = $55 million   
| gross          = $8,382,477   
}} Las Vegas. The Only Game in Town.

==Plot== World Series of Poker Main Event.
 Bellagio hotels poker room, Huck goes to a party and meets aspiring singer Billie Offer (Drew Barrymore), who has just arrived in town. Billies older sister Suzanne (Debra Messing) warns her that Huck is "hustle 10, commitment zero." Back at the Bellagio, Huck is doing well at the tables before L.C. returns to town from the South of France. Huck greets his father coldly. The two play Glossary of poker terms#H|heads-up.
 stake Huck in the $10,000 main-event with a 60% (Roy) — 40% (Huck) split of any winnings, but Huck refuses. After failing to borrow money from his friend Jack (Robert Downey Jr.), Huck goes to Suzannes place hoping for a loan. Instead he runs into Billie, who gets a call confirming that she has landed a job singing at a club.

Huck proposes a celebration and at Binions Horseshoe he shows her how to play poker. L.C. arrives and shows Huck a wedding ring of Hucks late mothers that Huck had pawned and that L.C. has redeemed. Huck loses his winnings. Over dinner, he explains to Billie that his father stole from his mother before leaving her. Huck says his father taught him how to play on the kitchen table with "pennies, nickels, and dimes." They make love after dinner. As Billie sleeps, Huck steals money from Billies purse.

Huck plays in a "super satellite" for his entry to the main event. He appears to have the seat won, but a misdeal costs him. Roy agrees to stake Huck and even gives him an extra $1200 so that he can repay Billie. He apologizes to her, saying he feels they have a chance at something special. They later run into L.C., who wins all of Hucks stake money for the World Series in a quick poker game.

Billie holds the stopwatch in a golfing marathon that Huck must complete in 3 hours to win a bet. She declines to cheat for him when he finishes two seconds too late. When Huck goes to Suzannes apartment looking for Billie, he learns Billie has gone home to Bakersfield. Huck gets a black eye when Roys thugs toss him into his empty pool. They warn him to return the $11,200 stake that he owes to Roy or get a seat in the World Series within 48 hours.  Huck travels to Bakersfield to tell Billie that he meant what he said when he felt they had a chance at something special.
 2003 Main Event after a similar entry to the tournament).

After the tournament, L.C. offers his son a one-on-one rematch, playing only for pennies, nickels, and dimes. Their relationship is restored, as is Hucks and Billies in the final scene.

==Cast==
* Eric Bana as Huck Cheever
* Drew Barrymore as Billie Offer
* Robert Duvall as L.C. Cheever
* Debra Messing as Suzanne
* Robert Downey Jr. as Telephone Jack
* Horatio Sanz as Ready Eddie
* Jean Smart as Michelle Carson
* Kelvin Han Yee as Chico Bahn
* Michael Shannon as Ray
* Danny Hoch as Bobby Basketball
* Evan Jones as Jason Keyes
* Phyllis Somerville as Pawnbroker

==Poker players==
Many of the players seen are actual poker pros. They are:

 
* Sam Farha
* Chau Giang
* Barry Greenstein
* Jason Lester
* Ted Forrest
* Minh Ly
* John Murphy
* Erick Lindgren
* Daniel Negreanu
* Doyle Brunson Johnny Chan
* Hoyt Corkins
 
* Antonio Esfandiari
* Chris Ferguson
* Dan Harrington
* Phil Hellmuth
* Karina Jett
* John Juanda
* Mike Matusow
* Erik Seidel
* Mimi Tran
* Marsha Waggoner
* Robert Williamson III
* Cyndy Violette
 
Three others featured in the film play fictional characters. They are:

* Jennifer Harman as Shannon Kincaid John Hennigan as Ralph Kaczynski David Oppenheim as Josh Cohen

World Series of Poker

* Host Jack Binion Matt Savage

==Production==
Eric Bana was cast in the lead role in September 2004.  Drew Barrymore was cast in January 2005,  but almost didnt accept the job because of a lack of singing ability.    Debra Messing was cast in February 2005.  Doyle Brunson served as a poker consultant on the film. Eric Bana and Robert Duvall were coached for months by Brunson on how to play like professional poker players. Matt Savage served as a tournament consultant, while Jason Lester served as a consultant on scenes involving the Main Event championship. 
   
Filming began on March 23, 2005, in Las Vegas, Nevada,    where a majority of filming took place. Filming took place over nine days in the parking lot of Dinos Lounge on South Las Vegas Boulevard. A set was built on a soundstage in Los Angeles, California, which was used for interior scenes of the bar.  Other filming locations in Las Vegas included the fountains of the Bellagio (resort),  as well as Binions Gambling Hall and Hotel.   

In April 2005, scenes were filmed at a Summerlin golf course   and at Tiffanys Cafe inside the White Cross Drug store.  Filming also took place at the Silverton Casino Lodge, where Debra Messing filmed a scene dressed as a mermaid in the casinos mermaid show. The scene was cut from the film. 

The filmmakers wanted to film at the Bellagios poker room, but it had been renovated since 2003, the year that the films story took place. Instead, an exact replica of the original poker room was constructed on a soundstage in Los Angeles. At the time, the Bellagio was auctioning items from the old poker room, including furniture and chandeliers, all of which was purchased by the production crew to help recreate the original poker room. 

==Music==
The soundtrack to Lucky You was released on September 5, 2006.

{{Track listing
| collapsed       = no
| extra_column    = Artist
| total_length    = 50:36 

| title1          = Lucky Town
| length1         = 3:27
| extra1          = Bruce Springsteen
 Dance Me to the End of Love
| length2         = 3:56
| extra2          = Leonard Cohen performed by Madeleine Peyroux
 Choices
| length3         = 3:26
| extra3          = George Jones

| title4          = Maybe This Time
| length4         = 3:30
| extra4          = Liza Minnelli
 The Fever
| length5         = 7:36
| extra5          = Bruce Springsteen

| title6          = Bartenders Blues
| length6         = 4:26
| extra6          = Bonnie Raitt

| title7          = They Aint Got Em All
| length7         = 2:54
| extra7          = Kris Kristofferson

| title8          = The Cold Hard Truth
| length8         = 4:26
| extra8          = Drew Barrymore

| title9          = Like a Rolling Stone
| length9         = 6:10
| extra9          = Bob Dylan

| title10         = Let It Ride
| length10        = 3:25
| extra10         = Ryan Adams

| title11         = I Always Get Lucky with You
| length11        = 3:20
| extra11         = George Jones performed by Drew Barrymore

| title12         = Hucks Tune*
| length12        = 4:00
| extra12         = Bob Dylan

}}

"Hucks Tune" was written specifically for the film  and later released on   compilation album.

==Release dates==
The film was initially set for release on December 16, 2005.   This was pushed back to September 8, 2006.  By December 2006, the film had been re-scheduled for release on March 16, 2007.  In January 2007, the films eventual release date was unveiled to be May 4, 2007. 

==Reception==
Opening the same weekend as Spider-Man 3, the film debuted at $2.7 million in ticket sales; the lowest saturated opening week since 1982.  It finished with just over $5.7 million in total revenue.

The film was panned by most critics. It currently ranks at 29% on Rotten Tomatoes. 

Despite the box office and critical failure, Eric Bana was nominated for the Australian Film Institute International Award for Best Actor.

==References==
 

== External links ==
* 
*  
* 

 

 
 
 
 
 
 
 
 
 
 