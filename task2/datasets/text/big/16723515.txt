Yasukuni (film)
{{Infobox film
| name            = Yasukuni
| image           = Yasukuni (2007 Film).JPG
| caption         = Movie Poster
| writer          = 
| starring        = 
| director        = Li Ying
| producer        = Li Ying
| distributor     = 
| country         = Japan
| language        = Japanese
| runtime         = 120 minutes
| music           = 
}}
  is a 2007 film made by Japan-resident Mayumi Saito,  , Asahi Weekly, March 15, 2008, accessed April 2, 2008.  Chinese director Li Ying ( ).  It took ten years to complete and had been screened at the  , April 1, 2008, accessed April 2, 2008. 

The film looks at the history of Yasukuni Shrine in Chiyoda, Tokyo, where more than 2 million of Japans war dead are enshrined. More than 1,000 of them are war criminals convicted at the International Military Tribunal for the Far East|1946–48 Tokyo tribunal, including 14 Class-A war criminals, Hideki Tōjō among them. The film shows not only the widely reported political incidents associated with the shrine, but also takes an in-depth look at the shrines sword-making tradition, the Yasukuni sword being the films underlying motif.   Interspersed with other scenes filmed at the shrine is serene footage of the last living Yasukuni swordsmith, 90-year-old Naoji Kariya, working on presumably his final creation.

Li Ying stated that the film was a joint Asian project—the editor was Japanese, as was the cameraman, who had a relative enshrined in Yasukuni.   The production received a  , March 28, 2008, accessed April 2, 2008. 
 Japanese nationalists, Liberal Democratic portrait rights. The Directors Guild of Japan expressed apprehension about possible infringement of freedom of expression, and as a result of the politicians protests, only about ten theaters screened the movie—none in Tokyo. 

On April 3, 2008, a report from  , April 3, 2008. Retrieved April 4, 2008. 

The film finally debuted to the public in Japan on May 3, 2008, in Tokyo amidst tight police security.  Yutada Okada, president of Argo Pictures, described the sold-out screening as safe and smooth.  Movie-goers queued two hours before the first show.  Feedback reported from the audience has been positive, describing the film as objective and anti-war, as well as enlightening Japanese citizens about the shrine itself.  , Agence France-Presse, May 3, 2008. Retrieved May 5, 2008. 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 