The Matrix Revolutions
{{Infobox film
| name           = The Matrix Revolutions
| image          = Matrix revolutions ver7.jpg
| border         = yes
| alt            = 
| caption        = Theatrical release poster The Wachowski Brothers
| producer       = Joel Silver
| writer         = The Wachowski Brothers
| starring       = {{plainlist|
* Keanu Reeves
* Laurence Fishburne
* Carrie-Anne Moss
* Hugo Weaving
* Jada Pinkett Smith
}} Don Davis
| cinematography = Bill Pope
| editing        = Zach Staenberg
| studio         = {{plainlist|
* Village Roadshow Pictures
* Silver Pictures
* NPV Entertainment
}}
| distributor    = {{plainlist| Warner Bros. Pictures
* Roadshow Entertainment   
}}
| released       =  
| runtime        = 129 minutes  
| country        = Australia   United States
| language       = English
| budget         = $150 million 
| gross          = $427.3 million    
}} science fiction action film and the third   installment of The Matrix (franchise)|The Matrix trilogy. The film was released six months following The Matrix Reloaded. The film was written and directed by The Wachowskis and released simultaneously in 60 countries on November 5, 2003. While it is the final film in the series, the Matrix storyline continued in The Matrix Online.

The film was the second live-action film to be released in both regular and IMAX theaters at the same time.

==Plot==
  Neo and Bane lie unconscious in the medical bay of the ship List of ships in the Matrix series#Mjolnir ("Hammer")|Hammer. Meanwhile, Neo finds his digital self trapped in a virtual subway station—a transition zone between the Matrix and the Machine City. In that subway station, he meets a "family" of programs, including a girl named Sati, whose father tells Neo the subway is controlled by the Trainman, an exiled program loyal to the Merovingian (The Matrix)|Merovingian. When Neo tries to board a train with the family, the Trainman refuses and overpowers him.
 Seraph contacts Morpheus and Trinity on Smith intends to destroy both the Matrix and the real world. She states that "everything that has a beginning has an end", and that the war will conclude. After Neo leaves, a large group of Smiths assimilates Sati, Seraph and the unresisting Oracle, gaining her powers of precognition.
 encounter with cauterizes Neos eyes with a power cable, blinding him; however, Neo discovers an ability to perceive the world as golden light. Neo kills Bane, and Trinity pilots them to the Machine City.
 Kid to open the gate for the Hammer. When it arrives, it discharges its Electromagnetic pulse|EMP, disabling the Sentinels but also the remaining defenses. The humans are forced to retreat and wait for the next attack, thinking that it will be their last stand. Neo and Trinity are attacked by machines, causing them to crash the Logos into the Machine City. The crash kills Trinity. Neo enters the Machine City and encounters the "Deus Ex Machina", the machine leader. Neo, warning that Smith plans to conquer both the Matrix and the real world, offers to stop Smith in exchange for peace with Zion. The machine leader agrees, and the Sentinels stop attacking Zion.

The Machines provide a connection for Neo to enter the Matrix. Inside, Neo finds that Smith has assimilated all its inhabitants. The Smith with the Oracles powers steps forth, saying that he has foreseen his victory against Neo. After a protracted battle, Neo—finding himself unable to defeat Smith—allows himself to be assimilated. The machine leader sends a burst of energy into Neos body in the real world; his life is sacrificed. Because Neo is connected to the Source, the energy burst causes the Neo-Smith clone and all other Smith clones in the Matrix to be destroyed. The Sentinels withdraw from Zion, Morpheus and Niobe embrace, and Neos body is carried away by the machines. The Matrix Booting|reboots, and the Architect encounters the Oracle in a park. They agree that the peace will last "as long as it can", and that all humans will be offered the opportunity to leave the Matrix. The Oracle tells Sati that she thinks they will see Neo again. Seraph asks the Oracle if she knew this would happen; she replies that she did not know, but she believed.

==Cast==
  Neo
* Morpheus
* Trinity
* Smith
* Niobe
* The Oracle
* Harry J. Lennix as Commander Lock Link
* The Merovingian Persephone
* Nona Gaye as Zee
* Anthony Zerbe as Councillor Hamann
* Nathaniel Lees as Captain Mifune Seraph
* Bane
* The Architect
* Tanveer K. Atwal as Sati
* Bruce Spence as Trainman
* Gina Torres as Cas Kid
* Cornel West as Councillor West Bernard White as Ramachandra David Roberts as Captain Roland Anthony Wong Ghost
* Tharini Mudaliar as Kamala
* Maurice J. Morgan as Tower soldier
 

Actress Gloria Foster, who played the Oracle in the first and second films, died before the completion of her filming for the third.  She was replaced by actress Mary Alice. Her changed appearance is addressed in the films plot. 

==Production==
  Filming occurred concurrently with its predecessor, The Matrix Reloaded, and live-action sequences for the video game Enter the Matrix. This took place primarily at Fox Studios in Sydney, Australia.  

===Sound design===
Sound editing on The Matrix trilogy was completed by Danetracks in West Hollywood, California.

===Soundtrack===
  Don Davis Pale 3) is used.

Although Davis rarely focuses on strong melodies,  familiar leitmotifs from earlier in the series reappear. For example, Neo and Trinitys love theme—which briefly surfaces in the two preceding films—is finally fully expanded into "Trinity Definitely"; the theme from the Zion docks in Reloaded returns as "Men in Metal", and the energetic drumming from the Reloaded tea house fight between Neo and Seraph opens "Tetsujin", as Seraph, Trinity and Morpheus fight off Club Hels three doormen.

The climactic battle theme, named "Neodämmerung" (in reference to   of "Neodämmerung").

==Reception==

===Box office===
The films budget is an estimated United States dollar|US$150 million, Allmovie. 2010b. The Matrix Revolutions.   Rovi Corporation (Updated 2010) Available at:   Accessed 19 February 2010. Archived at  .  It grossed over $139 million in North America and approximately $427 million worldwide,  roughly half of The Matrix Reloaded box-office total. In its first five days of release, it grossed $83.8 million, http://www.boxofficemojo.com/movies/?page=weekend&id=matrixrevolutions.htm  but dropped 66% during the second week. 

The Matrix Revolutions was released on DVD and VHS on April 6, 2004. The film grossed $116 million in DVD sales.

===Critical reception===
The film received generally mixed reviews. The film received a score of 36% on Rotten Tomatoes.    The films average critic score on Metacritic is 47/100. 

Some critics criticized the film for being Anti-climax (narrative)|anticlimactic.   Additionally, some critics regard the film as less philosophically ambiguous than its predecessor, The Matrix Reloaded.   Critics had difficulty finding closure pertaining to events from The Matrix Reloaded, and were generally dissatisfied.  

Conversely, Roger Ebert of the Chicago Sun-Times gave the film three stars out of four, despite offering criticisms of his own, on the grounds that it at least provided closure to the story well enough so that fans following the series would prefer seeing it as to not. 

 

==See also==
 
* List of films featuring powered exoskeletons

==References==
 

==External links==
 
*  
*  
*  
*  
*   – a comparative-literature-style exegesis of selected parts of Matrix Revolutions.
*  
*   – A comparative guide to possible meaning and interpretations of The Matrix Revolutions
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 