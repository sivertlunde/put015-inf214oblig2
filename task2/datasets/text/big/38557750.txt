Mudda – The Issue
{{Infobox film
| name=Mudda - The Issue
| image=Mudda - The Issue.jpg
| image_size = 200px
| alt=
| caption=Theatrical release poster
| director=Saurabh Shukla
| producer=Daksha Mavani
| story=Saurabh Shukla
| based on=
| screenplay=
| starring=Arya Babbar Aditya Srivastava Rekha Vedvyasa Vijay Raaz
| music=Pritam Chakraborty
| cinematography=Taposh Mandal
| editing=K Ravi Kumar
| studio=Ramnord Research Laboratories Pvt Ltd
| distributor=
| released=  
| runtime=
| country=India
| language=Hindi
}}
 2003 Hindi Drama film directed by Saurabh Shukla and produced by Daksha Mavani. The film features an ensemble cast where director himself played a role in the film.  

==Story==
Perplexed, and frustrated with life in Bombays corrupt colleges, Lecturer Siddharth Archarya, decides to re-locate to a small town called Samaypur, where he feels he will get some satisfaction in teaching youngsters who are not influenced by politics. Alas, he is wrong. Student union rivalry and hatred has encompassed the entire town, fights break out frequently, mostly between two rival student leaders, Pratap and Rajbir, who are children of local political leaders, Harphool Singh and MLA Balli Tai. Siddharth decides to garner their energy in a positive way by making them renovate the college building - with disastrous results - and no hopes for a reconciliation. Things get even more complicated when both young men fall for a fellow collegian, Sundari, who wants to marry Pratap. It is then the parents of both Rajbir and Pratap meet secretly - and decide what their sons and Samaypurs fate is going to be.

==Cast==
*Arya Babbar as Rajbir (Ballis son)
*Prashant Narayanan as Pratap Singh (Harphools son)
*Rekha Vedavyasa as Sundari
*Aditya Srivastava as Harphool Singh
*Vijay Raaz as Lemur
*Pankaj Jha
*Saurabh Shukla as Dinanath
*Rajat Kapoor as Siddharth Acharya

==Music==

The music of the film is composed by Pritam and lyrics are written by Chandrani Ganguly,Saurabh Shukla and Sanjay Swami.

{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|-  style="background:#ccc; text-align:center;"
! # !! Title !! Singer(s)
|-
| 1
| "Deewangee"
| Sumeet Kumar
|-
| 2
| "Deewangee (Sad)"
| Sumeet Kumar
|-
| 3
| "Kaise Main Kahoon"
| Sumeet Kumar
|-
| 4
| "Godavna"
|  Arun Bakshi,Poornima
|-
| 5
| "Khwabon Ki"
| Hariharan (singer)|Hariharan, Kavita Krishnamurthy, Zubeen Garg
|-
| 6
| "Kutta Kaate"
|  Sonu Nigam, Saurabh Shukla
|-
| 7
| "Sapne Saare"
|  Zubeen Garg
|}

==References==
 

==External links==
* 

 
 
 
 