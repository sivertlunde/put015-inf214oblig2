Kedar Gouri
{{Infobox film
| name           = Kedar Gouri
| image          =
| caption        =
| director       = Nitai Palit
| writer         = Manaranjan Das
| starring       = Gaura Ghosh   Gloria Mohanty  Kishori Debi   Nitai Palit   Beena Palit   Dhira Biswal
| producer       = Balaram Das
| music          = Balakrushna Das
| cinematography =
| editing        = Binaya Banarjee
| studio         = Radha Pictures
| distributor    = Pancha Sakha Films
| released       =  
| country        = India
| runtime        = 176 min Oriya
| website        =
| amg_id         =

}}
 Ollywood / Oriya film directed by Nitai Palit.  {{cite web
|url=http://www.nuaodisha.com/OdiaMovieDetails.aspx|title=List Of Odia Movies - 1936 to 2013 
|publisher=Nuaodisha.com}}   The film is adapted from Radhanath Rais poem by the same name.  
==Synopsis==
Kedar and Gouri are deeply fond of each other since childhood. Kedars father and Gouri father are village heads and have rivalry between them. As both Kedar and Gouri grow, their fondness transfer in to intense and enduring love. But their family do not allow them to meet each other and place a barrier between them. Once Kedar in disguise tries to attend Gouris birthday function. He traces out and  is humiliated and thrown away  by Gouris father. Now it is impossible for the lovers to meet as their family tighten the security for both of them. The lovers have no other option and elope from their home. while venture in jungle, Gauri feels very thirsty. Kedar goes to nearby spring to bring water. When Kedar returns with water, he can not find Gouri anywhere around. He tires search Gouris whereabout. In the meantime he heards a tigers rumble suddenly at some distaance away. He panicly searches for Gouri. On the way he finds blood stained clothes of Gouri. Kedar is now sure that Gouri is eaten by the tiger. In a anguish and frustration Kedar stabs himself repeatedly until he is dead. But to the contrary, Gouri is safely hides in some bush away from the scene.She comes back to the scene after the tiger disappear. Her heart breaks when she finds Kedar died for her sake. Gauri feels she can not able live with out Kedar and follows suit. Thus the lovers brings end of their life for eacher to imprint a sublime love story.

==Cast==
* Gaura Ghosh   ... Kedar
* Gloria Mahanty   ... Gouri
* Beena Palit
* Kishori Devi Nityanand Palit
* Dhira Biswal
* Lakshmi
* Sanyukta Panigrahi
* Radharani Rita
* Sheela Ullash
 
==Box office==
Desipte good acting and cinematography, the film flopped. The masses could not digest this tragical love story of this genre. 
==References==
 
==External links==
*  
*  
 
 
 
 