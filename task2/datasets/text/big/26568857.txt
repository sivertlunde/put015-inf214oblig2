Day & Night (2010 film)
 

{{Infobox film
| name           = Day & Night
| image          = Day & Night poster.jpg
| alt            = The poster for the Pixar short animated film "Day & Night". At the top, text reads "Walt Disney Pictures and Pixar Animation Studios present Day & Night". Beneath the text, two hand drawn cartoon characters are pictured. They are both male. The one on the left has an image of nighttime (moon and stars) inside him, and the man on the right has an image of daytime (sun and blue sky) inside him. They both look surprised to see each other.
| caption        = Poster
| director       = Teddy Newton
| producer       = Kevin Reher
| writer         = Teddy Newton
| starring       = Wayne Dyer
| music          = Michael Giacchino
| editing        = Greg Snyder
| studio         = Pixar Animation Studios Walt Disney Pictures Walt Disney Studios Motion Pictures
| released       =  
| runtime        = 5:57
| country        = United States
| language       = English
}}
Day & Night is a Pixar animated short film, directed by Teddy Newton and produced by Kevin Reher.  It was packaged to be shown in theaters before Toy Story 3,    and has been released to purchase on iTunes in the United States. 
 2D and 3D elements,  and Up (2009 film)|Up production designer Don Shank says it is "unlike anything Pixar has produced before". 

==Synopsis== anthropomorphic characters, Day and Night. Inside Day is a day scene with a sun in the center, and inside Night is a night scene with a moon in the center.  Whatever goes on inside of Day or Night expresses normal events that typically occur within a day or night, respectively, and these events often correspond with actions or emotions that the characters Day or Night express. For example, when Day is happy he will have a rainbow inside him, and when Night is happy he will have fireworks inside him.

Day and Night meet and at first are uneasy about each other. They become jealous of each other due to the events occurring in their insides, and end up fighting at one point in the short. Eventually they see the positives in each other and learn to like each other. At the end of the film they see the things they saw in each other in themselves, as Day becomes night, and Night becomes day.

==Production== 2D and 3D animation. The outlines of both characters are hand drawn and animated in 2-D, while the scenes inside their silhouettes are rendered in 3D;  the use of a masking technique allows the 2D characters to be windows into a 3D world inside them.   The Day & Night is Pixars second short film to be partially animated in 2D, after Your Friend the Rat.

The voice used in this movie is from Dr. Wayne Dyer and was taken from a lecture he gave in the 1970s. The director of the movie incorporated the ideas taken from Dyers lecture to show that the unknown can be mysterious and beautiful, and doesnt at all have to be something to fear. The themes of the lecture date back to similar speech by Albert Einstein,  who said, "The most beautiful thing we can experience is the mysterious".    Pixar honored Dyer by providing him with a private screening of the film. 

 

 
The score was created by Oscar-winning composer Michael Giacchino, who also created the score for The Incredibles, Ratatouille (film)|Ratatouille, and Up (2009 film)|Up. 

==Response==

===Critical reception===
The short has been critically acclaimed.

The Wall Street Journal described Day & Night as a "sensationally original short", and said it "looks like nothing youve ever seen, and plays like a dream of glories to come".  Antagony & Ecstasy gave the film 10/10, writing "Newtons ability to finesse a remarkably difficult conceptual hook into a supremely easy cartoon is proof enough that he has storytelling skills much the equal of anybody working at Pixar. Yet this is also, possibly, the least of the films achievements: it is technically, formally, and thematically a work of great accomplishment, and merely being able to tell a weird story coherently is just cake at that point".  The Projection Booth rated the film 4.5 out of 5, writing "Still superior   is the preceding short Day & Night (directed by Teddy Newton), a morally and politically charged look at our perception of reality (and each other) and easily the best of Pixars shorts to date".  Cinema Crazed said the short is "ultimately very evocative of the classic Warner and MGM animated experiments that appealed to all audiences and didn’t talk down to its respective theater going crowd".  NOLA.com described it as an "artful snatch of 3-D whimsy". 

===Awards and nominations===
  Best Animated 83rd Annual Academy Awards. 
It won the award for Best Short Film at the 38th Annie Awards. 

The Visual Effects Society also gave Day & Night an award for the Outstanding Achievement in an Animated Short. 

==References==
 

==External links==
*  
*  
*   at Filmaffinity

 
{{succession box
 | before = George & A.J. short films
 | years  = 2010
 | after  = Hawaiian Vacation}}
 

 
 
 
 

 
 
 
 
 
 
 
 
 
 