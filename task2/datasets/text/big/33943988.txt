Empire M
 
{{Infobox film
| name           = Empire M
| image          = 
| caption        = 
| director       = Hussein Kamal
| producer       = Murad Ramsis Nagib
| writer         = Ihsan Abd al-Qudus Mustafa Samy
| starring       = Faten Hamama
| music          = 
| cinematography = Wahid Farid
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = Egypt
| language       = Arabic
| budget         = 
}}
 Best Foreign Language Film at the 46th Academy Awards, but was not accepted as a nominee. 

==Cast==
* Faten Hamama as Mother (Mona)
* Ahmed Mazhar
* Dawlad Abiad as Granny
* Seif Abol Naga as Mostafa (as Khaled Abol Naga)
* Hayat Kandeel as Daughter

==See also==
* List of submissions to the 46th Academy Awards for Best Foreign Language Film
* List of Egyptian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 
 
 