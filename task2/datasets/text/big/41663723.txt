Alpine Fire
 
{{Infobox film
| name           = Höhenfeuer
| image          = 
| alt            = 
| caption        = 
| director       = Fredi M. Murer
| producer       = Lang Film
| writer         = 
| screenplay     = 
| story          =
| based on       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = Switzerland
| language       = German
| budget         = 
| gross          = 
}} Best Foreign Language Film at the 58th Academy Awards, but was not accepted as a nominee. 

==Reception==
It won the Golden Leopard at the 1985 Locarno International Film Festival. 

==See also==
* List of submissions to the 58th Academy Awards for Best Foreign Language Film
* List of Swiss submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 