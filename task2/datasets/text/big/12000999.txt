The Brave One (2007 film)
 
{{Infobox film
| name           = The Brave One
| image          = Brave one 2007.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Neil Jordan
| producer       = Susan Downey Joel Silver
| writer         = Roderick Taylor Bruce A. Taylor Cynthia Mort
| narrator       =
| starring       = Jodie Foster Naveen Andrews Terrence Howard
| music          = Dario Marianelli Sarah McLachlan
| cinematography = Philippe Rousselot
| editing        = Tony Lawson
| studio         = Village Roadshow Pictures Silver Pictures
| distributor    = Warner Bros.
| released       =  
| runtime        = 122 minutes
| country        = United States Australia
| language       = English
| budget         = $70 million 
| gross          = $69,787,663   
}}
The Brave One is a 2007 American psychological thriller film directed by Neil Jordan, produced by Joel Silver, and starring Jodie Foster. It was released in the United States on September 14, 2007. The film earned Foster a Golden Globe nomination for leading actress in a drama.

==Plot==
As radio show host Erica Bain (Jodie Foster) and her fiancé David (Naveen Andrews) are walking their dog at Strangers Gate in New Yorks Central Park, they are attacked by three violent criminals. David dies from his injuries, but Erica survives. Angry and traumatized, she attempts to purchase a gun.  Terrified and unwilling to wait the month required to obtain it legally, she acquires a pistol from a stranger on the street.

She stops at a convenience store. While shopping, a man comes in screaming at the cashier and shoots her to death. The killer hears Ericas cell phone and stalks her in the aisles.  Just as the killer finds her, Erica shoots and kills him. One night, two street thugs harass and threaten passengers in a subway car. The passengers all leave at the next stop except Erica. When the thugs threaten her with a knife, Erica is ready for them and kills them both. Another night, she saves a prostitute and kills her brutal pimp. All the while, Erica attempts to track down the thugs who killed David.

She strikes up a friendship with Detective Sean Mercer (Terrence Howard), who is investigating the vigilante crimes and who is initially unaware of her role in the deaths. Erica, in trying to find out if the detective is close to solving the vigilante killings, pretends to want to interview Mercer. They talk several times. Soon, Detective Mercer comes to suspect her as the killer.

At the climax of the film, Erica finds and confronts the thugs responsible for Davids murder. She kills two, finding and releasing her dog in the process and struggles with the third. Mercer arrives on the scene and attempts to arrest him. Erica then retrieves her weapon and attempts to execute the thug. Mercer persuades Erica to lower the gun, but hands her his own in order for her to use a legal weapon to kill the last thug.  Erica kills him, but then Mercer insists that Erica should wound him (Mercer) to help frame a story blaming the thugs as the vigilante killers. Mercer places Ericas gun in the last thugs dead hand and Erica leaves the scene, eventually being rejoined by her dog in Central Park.

==Cast==
* Jodie Foster as Erica Bain
* Terrence Howard as Detective Sean Mercer
* Naveen Andrews as David Kirmani
* Nicky Katt as Detective Vitale
* Zoë Kravitz as Chloe
* Mary Steenburgen as Carol
* Luis Da Silva as Lee Jane Adams as Nicole
* John Magaro as Ethan
* Dana Eskelson as Sketch Artist

==Release==

===Critical reception===
The film received mixed reviews from critics. Rotten Tomatoes gives it a score of 43% based on 183 reviews.  On Metacritic, the film had an average score of 56%, based on 33 reviews.  Yahoo! movie ratings give it a "B-" from critics (14 reviews ).

Roger Ebert of the Chicago Sun-Times gave the film 3  stars (out of 4); saying Foster and Howard "are perfectly modulated in the kinds of scenes difficult for actors to play, where they both know more than theyre saying, and they both know it." 

===Box office===
In its opening weekend in the United States and Canada, the film was #1 at the box office, grossing $13,471,488 in 2,755 theaters.  As of December 29, 2007, the film has grossed $69,787,663 worldwide—$36,793,804 in the United States and Canada and $32,993,859 in other territories. 

===Awards and nominations===
   Golden Globe nomination when the nominees for the 65th Golden Globe Awards were announced by the Hollywood Foreign Press Association. Foster was nominated for Best Performance by an Actress in a Motion Picture - Drama. 

==See also==
* Bernhard Goetz Death Wish Eye for an Eye

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  
*   - Film trailer at IGN.com
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 