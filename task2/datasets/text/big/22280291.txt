Comedian Harmonists (film)
{{Infobox film
| name           =Comedian Harmonists
| image          =Comedian_Harmonists_Filmposter.jpg
| alt            = 
| caption        = 
| director       = Joseph Vilsmaier
| producer       =Reinhard Klooss
| writer         = 
| screenplay     =Klaus Richter, Jürgen Egger
| story          = 
| based on       =
| narrator       = 
| starring       = Ben Becker, Heino Ferch, Ulrich Noethen, Heinrich Schafmeister, Max Tidof, Kai Wiesinger
| music          = Harald Kloser, Thomas Schobel, Walter Jurmann (Songs)
| cinematography = Joseph Vilsmaier
| editing        = Peter R. Adam
| studio         = Perathon Film und Fernseh GmbH
| distributor    =Senator Film Verleih GmbH
| released       =  
| runtime        = 127 minutes
| country        = Germany
| language       = german
| budget         = 
| gross          = 
}}

 Comedian Harmonists (English title: The Harmonists) is a 1997 German film, directed by Joseph Vilsmaier, about the popular German vocal group the Comedian Harmonists of the 1920s and 30s. The film is  supported by German and Austrian film fund.

==Plot==
In 1927, unemployed German-Jewish actor Harry Frommermann is inspired by the American group The Revelers to create a German group of the same format. He holds auditions and signs on four additional singers and a pianist. Naming themselves the "Comedian Harmonists", they meet international fame and popularity. However, they eventually run into trouble when the Nazis come to power, as half the group is Jewish.

==Cast==
* Ben Becker as  Robert Biberti
* Heino Ferch as Roman Cycowski
* Ulrich Noethen as Harry Frommermann
* Heinrich Schafmeister as Erich A. Collin
* Max Tidof as Ari Leschnikoff
* Kai Wiesinger as Erwin Bootz
* Meret Becker as Erna Eggstein
* Katja Riemann Mary Cycowski
* Noemi Fischer as Chantal, Collins girl friend
* Dana Vávrová as Ursula Bootz
* Otto Sander as Bruno Levy
* Michaela Rosen as Ramona, Puffmutter
* Günter Lamprecht as Eric Charell
* Gérard Samaan as Romans father
* Rolf Hoppe as Gauleiter Streicher
* Jürgen Schornagel as Reichsmusikdirektor
* Rudolf Wessely as Mr. Grünbaum
* Susi Nicoletti as Mrs. Grünbaum
* Giora Feidman as clarinet player
* Kathie Lindner wardrobe mistress
* Trude Ackermann Roberts mother

==Reception==
Comedian Harmonists succeeded in Europe. U.S. President Bill Clinton told critic Roger Ebert it was among his favorite films of the year, although the movie did not get widespread release, hence reception in the United States.

Bernd Reinhardt of the World Socialist Web Site called it "an exciting film which is well worth seeing and which pays proper attention to the sextets music."  He also remarked on the films attention to historical detail and the importance of its theme of musical internationalism. 

==Awards and nominations== Best Feature Best Direction, losing to Wim Wenders for The End of Violence. Best Director. Special Prize
 Best Cinematographer for Comedian Harmonists at the 1998 European Film Awards.

==American version==
The American distribution of the film contains at least one difference from the original: in the original, there is a scene, when the Harmonists arrive in New York and perform in front of the U.S. Navy, where the camera singles out one African American navy man who is visibly enjoying the music, until he gets a stinging look of rebuke from a superior officer. This segment was cut from the American release. 

==Legacy==
The film led to the writing of a musical play, Veronika, der Lenz ist da - Die Comedian Harmonists, which opened at the Komödie on the Kurfürstendamm in Berlin on 19 December 1997. When this production closed, the actors who had played the original sextet formed themselves into a new group called the Berlin Comedian Harmonists,  which was still in existence in 2012.

==Notes==
 

==External links==
* 
* 
 

 
 
 
 
 
 
 
 
 
 