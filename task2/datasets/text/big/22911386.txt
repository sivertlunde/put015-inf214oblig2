Aval Oru Thodar Kathai
{{Infobox film|
| name = Aval Oru Thudar Katha
| image = Avaloruthodar.jpg
| caption = Promotional Poster
| director = K. Balachander
| story = M. S. Perumal
| screenplay = K. Balachander Sujatha Vijayakumar Vijayakumar Kamal Hassan Sripriya Fatafat Jayalaxmi
| producer = Rama Arannangal
| music = M. S. Viswanathan
| cinematography = B. S. Lokanath
| editing = N. R. Kittu
| studio = Aandal Movies
| distributor = Arul Films
| released = 13 May 1974
| runtime = 162 minutes
| country = India
| language = Tamil
| budget =
}}
 Tamil film Sujatha in her first Tamil film,  which was also her first starring role. Kamal Haasan, Vijayakumar (actor)|Vijayakumar, Jaiganesh, M. G. Soman and Sripriya played key roles. The film was shot in black-and white.  

The film is considered to be one of Balachanders   as well as Sujathas best films.  Several directors like Mani Ratnam, K. Bhagyaraj and K. S. Ravikumar named Aval Oru Thodar Kathai as one of their favorite films.    It ran for 25 weeks   and was remade or dubbed in five other Indian languages.

==Plot== blind younger brother, her mother, her drunkard brother Murthy (Jaiganesh) and his family. Her father abandons the family and becomes a saint. Her brother not only does not take responsibilities, but also creates additional problems for her. She has a longtime boyfriend, Tilak (Vijayakumar (actor)|Vijayakumar) who wants to marry her, but she doesnt because of her commitment to her family. His eyes now wander to Kavithas widowed younger sister Bharathi (Sripriya) who reciprocates his feelings. Kavitha, after reading her boyfriends love letter to her sister, arranges for them to get married, thus giving up her chance of having a life with him. Meanwhile Prasad (Kamalahasan) loves Bharathi Kavithas sister but when he comes to know that she loves Tilak he sacrifies his love for her and he marries Kavithas distressed friend, Chandra (Fatafat Jayalaxmi). Kavitha eventually accepts a marriage proposal of her boss (Gokulnath), when she realizes that her brother has become responsible enough to take care of her family. She decides to resign from hard work, but could not as the result of turning point in a typical Balachandar style climax.

==Cast== Sujatha as Kavitha
* Jaiganesh as Murthy Vijayakumar as Tilak
* Sripriya as Bharathi
* Kamal Hassan as Prasad
* Fatafat Jayalaxmi as Chandra
* M. G. Soman and Chandrasekar Leelavathi as Mother
* Vinodhini
* Pushpa

* Devaki
* Uma
* Gokulnath as Arun Ghosh

==Crew==
Asssitant Directors: Ananthu, S.A. John

Art: A. Ramaswamy

Story, Screenplay, Direction: K. Balachander 

==Production== Ernakulam Junction, Sujatha had Rajesh said that he was approached to act in the film but he couldnt take up the film. 

The song "Kadavul Amaitha Medhai" was shot at a community hall situated at Nandanam, Chennai. 


==Sound track==
{{Infobox album  
| Name = Aval Oru Thodar Kathai
| Type = soundtrack
| Longtype =
| Artist = M. S. Viswanathan
| Cover =
| Cover size =
| Caption =
| Released =  
| Recorded =
| Genre =
| Length =   Tamil
| Label =
| Producer =
| Reviews =
| Compiler =
| Misc =
}}

{{tracklist
| collapsed =
| headline =
| extra_column = Singers
| total_length =
| all_writing =
| all_lyrics =Kannadasan
| all_music = M. S. Viswanathan

| writing_credits =
| lyrics_credits =
| music_credits =
| title1 = Adi Ennadi Ulagam
| note1 =
| writer1 =
| lyrics1 =
| music1 =
| extra1 = L. R. Eswari
| length1 =
| title2 = Kadavul Amaithu Vaitha
| note2 =
| writer2 =
| lyrics2 =
| music2 =
| extra2 = S. P. Balasubrahmanyam
| length2 =
| title3 = Kannil Enna
| note3 =
| writer3 =
| lyrics3 =
| music3 =
| extra3 = S. Janaki
| length3 =
| title4 = Dheivam Thantha Veedu
| note4 =
| writer4 =
| lyrics4 =
| music4 =
| extra4 = K. J. Yesudas
| length4 =
| title5 = Aadumadi Thottil
| note5 =
| writer5 =
| lyrics5 =
| music5 =
| extra5 = P. Susheela
| length5 =
}}

==Reception==
The film is considered a classic   and cult film.  Times of India named it one "of the landmark movies in the history of Tamil cinema".  30 years after the films release, directors Mani Ratnam, K. Bhagyaraj and K. S. Ravikumar named Aval Oru Thodar Kathai as one of their favorite films.  Mani Ratnam said, "K.Balachandar has done some of the best films before and after Aval Oru Thodar Kathai. But I choose it mainly for its storyline. It is not the plot or the story, but the character that carries the film through. Although it was released about 25 years ago, the character remains fresh in one’s memory. That is the greatness of the film".  Ravikumar stated, "Through his script and direction, Balachandar brings out the tender core of his middle-class heroine, assailed by problems, in Aval Oru Thodarkathai."  Former Tamilnadu Chief Minister Kalaignar M. Karunanidhi said, "Since Aval Oru Thodarkathai days, I am an avid fan of Balachander".  In 2011, after Balachander had been given the Dadasaheb Phalke Award, Rediff named it one of Balachanders best and wrote, "Aval Oru Thodarkadhai was one of his path-breaking works. Sujatha took on the mantle of Kavitha, the hard-working woman who struggles to support her largely ungrateful family. The tough exterior conceals a heart of gold, which, tragically, is never seen or recognised by her family. The film made waves not just for its principal characters who challenged Tamil cinemas set notions; it was also the dialogues that drew gasps from the audience. In creating Kavitha, K Balachander gave life to one of Tamils most enduring, powerful female characters".  Hindu wrote, "Aval Oru Thodarkadhai is a film that will stay with you forever — the invincible heroine was a big draw. And with AOT, Sujatha arrived! As the eldest daughter, who bears the onus of supporting a large family, she came up with a memorable performance. Her matter-of-fact approach to life was very fresh for Tamil audiences, who sang paeans to KB’s creative stroke". 

Aval Oru Thodarkathai is the second film in a series of woman-centric films by Balachander in which "the daughter has to make sacrifices and take care of her family" and a reviewer from Times of India wrote that it was a tough task to beat those films.  

===Awards===
* Filmfare Award for Best Director – Tamil - K. Balachander

==Original and remakes==
  Telugu as Malayalam as Bengali remake titled Kabita playing the lead role. Kamal Haasan made his Bengali film debut with this film by acting in the same role that he played in the Tamil original. Bharat Shamsher directed the black-and-white film. The film was again remade in Hindi as Jeevan Dhaara in 1982 by director T. Rama Rao. Rekha did the role of Jayaprada, Rakesh Roshan did the role of Kamal Haasan, Amol Palekar did the role of Narayana Rao. Kanwaljit did the conductor role, Simple Kapadia (sister of Dimple Kapadia) did Phataphat Jayalaxmis role. The film was shot in color. The film was a decent hit.  
 Kannada remake titled Benkiyalli Aralida Hoovu starring Suhasini, whose real-life uncle Kamal Haasan played the guest role of a bus conductor in this remake. Balachander directed the film which was produced by Chandulal Jain. It was shot in color. 

===Character map of Aval Oru Thodar Kathai and its remakes===
{| class="wikitable" style="width:50%; text-align:center;"
|- style="background:#ccc;" Hindi || Benkiyalli Aralida Hoovu (1983) (Cinema of Karnataka|Kannada)
|- Sujatha || Suhasini
|- Rajeev
|- Leelavathi || Leelavathi
|- Vijayakumar || Prasad Babu || Ranjit Mallick || Kanwaljeet Singh || Jai Jagadish
|-
| Heroines Widow Sister || Sripriya || Sripriya || Sandhya Roy || Madhu Kapoor ||  Pavithra
|- Ramakrishna
|-
| Heroines Friend || Fatafat Jayalaxmi || Fatafat Jayalaxmi || Prema Narayan || Simple Kapadia || Vijayalakshmi Singh
|-
| Heroines Office Manager || M.G. Soman || ? || Samit Bhanja || Suresh Chatwal || Sunder Raj
|-
| Heroines Managing Director || Gokul Nath || Kamal Haasan || Satindra Bhattacharya || Rakesh Roshan || Sarath Babu
|-
| Bus Conductor || ? || ? || ? || Karan Razdan || Kamal Haasan
|}

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 