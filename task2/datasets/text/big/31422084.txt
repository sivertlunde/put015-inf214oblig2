Zone 39
 
{{Infobox film
| name           = Zone 39
| image          = 
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = John Tatoulis
| producer       = Colin South
| writer         = Deborah Parsons
| screenplay     = 
| story          = 
| based on       = 
| narrator       = 
| starring       = Peter Phelps William Zappa
| music          = Burkhard von Dallwitz
| cinematography = Peter Zakharov
| editing        = Peter Burgess
| studio         = 
| distributor    = Beyond Films
| released       =    
| runtime        = 95 minutes
| country        = Australia
| language       = English
| budget = $4 million 
| gross        = A$21,976 (Australia) 
}} science fiction psychological drama film by director John Tatoulis. It features cast members Carolyn Bock, Peter Phelps and William Zappa, and runs for
93 minutes. 

The film tells the story of a future where the environment has been ravaged, leaving the world desolate. Two surviving factions, the New Territories and the Federal Republics, have been at war for 40 years. Finally, they have agreed to peace terms thanks to the efforts of the Central Union (CU). One of the security experts for the CU, Anne (Bock), decodes the encrypted messages of her boss, only to discover that one of the security zones has suffered a deadly contamination. Mysteriously, she dies shortly thereafter, leaving her soldier husband Leo (Phelps) devastated. 

To recuperate, Leo is assigned to  guard duty at the border outpost named Zone 39. The remainder of the film deals with Leos struggle to cope with isolation and the death of his wife.  She appears to him in hallucinations, perhaps brought on by the tranquillizers he has been taking. 

==Cast==
*Peter Phelps ... Leo Megaw
*Carolyn Bock ... Nova Anne
*William Zappa ... Sharp
*Bradley Byquar ... Boas

==Production==
The film was shot at Crawford Studios in Melbourne.  Director John Tatoulis:
 In Zone 39, I was exploring a couple of things. One was the way in which a person deals with grief, the loss of a loved on. I truly believe that someone doesnt die until we stop thinking about that person. I think once we forget that person, once that person ceases to live in our memories, then that person is truly dead. Often it takes a long time for that person to truly die in peoples hearts. I wanted to explore this theme in an environment that I think were heading towards, one of being like a society that is particularly unfriendly to the individual and particularly isolates the individual and controls that individual.  
==References==
{{reflist|refs=

   

   

   
}}

 
 
 
 
 
 