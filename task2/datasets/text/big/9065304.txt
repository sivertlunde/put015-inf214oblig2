The Taste of Tea
{{Infobox film name           = The Taste of Tea (茶の味 Cha no Aji) image          = The Taste of Tea.jpg caption        = The Taste of Tea poster director       = Katsuhito Ishii producer       = Kazuto Takida Kazutoshi Wadakura writer         = Katsuhito Ishii starring       = Tadanobu Asano Takahiro Sato Maya Banno Satomi Tezuka Tomokazu Miura Tatsuya Gashuin Anna Tsuchiya Rinko Kikuchi music          = Little Tempo cinematography = Kosuke Matushima editing        = Katsuhito Ishii distributor    = Grasshoppa (Japan) Viz Media (USA) released       = July 17, 2004 runtime        = 143 min. country        = Japan language  Japanese
|budget         =
}} Japanese writer and director Katsuhito Ishii. The film has been referred to as a "surrealism|surreal" version of Ingmar Bergmans Fanny and Alexander. It was a selection of the Cannes Film Festival.   

==Synopsis==
The film is concerned with the lives of the Haruno family, who live in rural Tochigi Prefecture, the countryside north of Tokyo. Nobuo is a hypnotherapist. He teaches his son, Hajime, Go (board game)|Go. Hajime becomes an excellent Go player, but he has a rough time with girls and puberty. Yoshiko refuses to be an average housewife, and works on animated film projects at home. She uses assistance from Grandfather Akira, an eccentric old man who is a former animator and occasional model. Eight-year-old Sachiko periodically sees a silent, giant-sized double of herself which mimics or benignly watches her. She contemplates ways to rid herself of it. Uncle Ayano is a sound engineer and record producer who comes to stay for a visit. He engages in inward reflection, seeks closure regarding an old relationship, and recounts a childhood experience- a tale which influences Sachiko and ties into later events.  

==Cast==
*Tomokazu Miura - Nobuo Haruno - father
*Satomi Tezuka - Yoshiko Haruno - mother
*Maya Banno - Sachiko Haruno - daughter
*Takahiro Sato - Hajime Haruno - son
*Tadanobu Asano - Ayano Haruno - uncle
*Tatsuya Gashuin - Akira Todoroki - grandpa
*Tomoko Nakajima - Akira Terako
*Ikki Todoroki - Himself
*Anna Tsuchiya - Aoi Suzuishi
*Kenichi Matsuyama - Matsuken/The man in the red shirt
*Hideaki Anno - Kasugabe
*Rinko Kikuchi - Yuriko Kikuchi
*Ryo Kase - Rokutaro Hamadayama
*Kenji Mizuhashi - Maki Hoshino

==Awards==
*Audience Award - 2004, Entrevues Film Festival
*Grand Prix - 2004, Entrevues Film Festival
*Best Feature Film - 2004, Hawaii International Film Festival
*Best New Actress (Anna Tsuchiya) - 2004, Hochi Film Awards
*Orient Express Award - 2004, Festival de Cine de Sitges (Special Mention)
*Audience Award - 2005, Dejima Japanese Film Festival
*Best Asian Film - 2005, Fant-Asia Film Festival
*Fantasia Ground-Breaker Award - 2005, Fant-Asia Film Festival (3rd Place)
*Audience Award - 2005, New York Asian Film Festival
*Best New Actress (Anna Tsuchiya) - 2005 Kinema Junpo Awards
*New Talent Award (Anna Tsuchiya) - 2005, Mainichi Film Award
*Festival Prize - 2005, Yokohama Film Festival

==References==
 

==Sources==
* 

==External links==
*  
*  
*  

 

 
 
 
 
 
 

 