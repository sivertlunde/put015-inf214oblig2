Slap Her... She's French
{{Infobox Film
| name           = Slap Her... Shes French
| image          = Slapherfrench.jpg
| caption        = Theatrical release poster
| director       = Melanie Mayron
| producer       = Thomas Augsberger Steven Brown & Matthias Deyle Beau Flynn
| writer         = Lamar Damon Robert Lee King
| starring       = Piper Perabo Jane McGregor Trent Ford
| music          = Christophe Beck David Michael Frank
| cinematography = Charles Minsky
| editing        = Marshall Harvey ContentFilm Constantin Film Lions Gate Films Intermedia Films
| distributor    = Winchester Films
| release date(s)= February 7, 2002 (Germany) October 18, 2002 (UK)
| runtime        = 92 minutes
| country        = United States
| language       = English
}}
 American Teen teen comedy, directed by Melanie Mayron and starring Piper Perabo. 

==Plot== pageant queen foreign exchange student, an orphan named Genevieve Le Plouff.  After winning the affections of Starlas parents, friends, and boyfriend, Genevieve soon begins to take over Starlas life. 
 cheerleading squad after getting some bad grades in French, Genevieve moves in to take her place, and then the roles are reversed. Soon, Genevieve is the popular head cheerleader, and Starla is the unpopular student. Soon Genevieve takes Starlas place in the News Anchor Competition, and, framed by Genevieve, Starla is arrested for possessing a knife and getting high. But then Starla learns that Genevieve was, in fact, a former elementary school classmate, whom Starla had embarrassed so much that she felt compelled to move to France and has come back in disguise to get revenge on Starla.

With her charade exposed, Genevieve leaves town in disgrace and Starla reclaims her status in school and town although she never achieved her ambition of becoming a reporter nor a college scholarship, but now feels that shes a changed woman. However, the final scene shows Genevieve, now demonstrating her true Texan origins, stealing Starlas ID card and returning to France as she is welcomed to her new adoptive French family upon her arrival in Paris, play acting as Starla... and strongly implying that shell start the process of building a new life for herself by trickery, deception, and manipulation all over again.

==Cast==
* Jane McGregor as Starla Grady
* Piper Perabo as Genevieve LePlouff
* Trent Ford as Ed Mitchell
* Alexandra Adi as Ashley Lopez
* Nicki Aycox as Tanner Jennings Jesse James as Randolph Grady
* Julie White as Bootsie Grady
* Brandon Smith as Arnie Grady
* Matt Czuchry as Kyle Fuller
* Christen Coppen as Doreen Gilmore
* Haley Ramm as young Starla Grady (uncredited)
* Amy Bradford as French airplane passenger (uncredited)
* Ashley Blake as Megan
* Laura Halvorson as Beef Band fiddle player

== References ==
 

==External links==
 
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 