Bishonen (film)
 
 
{{Infobox film
| name           = Bishonen...
| original_name = 美少年の恋
| image          = Bishonen.jpg
| image_size     = 
| caption        = Film poster
| director       = Yonfan
| producer       = Sylvia Chang
| writer         = Yonfan
| narrator       = Brigitte Lin
| starring       = Stephen Fung Daniel Wu Shu Qi
| music          = Chris Babida
| cinematography = Henry Chung
| editing        = Ma Kam
| distributor    = Chong Cho Sau Productions (Hong Kong)  JVC (Japan)
| released       =   (Hong Kong)   (Japan)
| runtime        = 111 mins
| country        = Hong Kong
| language       = Cantonese
| gross          = $18,629 (USA) 
}} Hong Kong romantic drama gay romance. Directed by Yonfan, it stars Stephen Fung, Daniel Wu and Shu Qi.
 retrospective of Yonfans work, which featured seven of his restored and re-mastered films from the 1980s through the 2000s. 

==Synopsis== sex appeal seems to know no bounds. Everyone wants to make love to him, but he is in love with no one but himself.

Things change drastically when he notices what seems like a young couple in a shop, Sam (Daniel Wu) and Kana (Shu Qi). At first sight, he falls in love with Sam and begins following the two around.

Jets friend Ching, who is also a prostitution|hustler, runs a personal ad in a gay magazine for Jet, imploring Sam to contact Jet.

At first, Jet is angry with Ching for not asking him, but his wrath subsides quickly when indeed he meets Sam again in what seems like a chance encounter but actually is an outcome of the personal ad.

Sam turns out to be a police officer and Jet starts to befriend him, hoping this will turn into a relationship. But Sam does not seem to notice Jets intentions.

Unbeknownst to Jet, Sam had an affair with pop star K.S. (Terence Yin) five years earlier. At the same time, Ching had been in unrequited love with Sam (then calling himself Fai) when the two were office workers.

Ching comes to his apartment shared with Jet when Jet and Sam are there, instantly recognizes Sam as Fai and is furious with Jet for stealing his beloved. Sam runs back to his apartment, but Jet pursues him. The two kiss, but they are interrupted by an unknown person at the door.

Later, Sam believes the person to be his father. Unwilling to face his fathers disappointment, he kills himself. Jet lives on as a hustler and receives a letter knowing that Sam loved him.

==Cast includes==
* Stephen Fung - Jet
* Daniel Wu - Sam
* Shu Qi - Kana 
* Terence Yin - K.S
* Jason Tsang
* Kenneth Tsang
* Chiao Chiao
* Tat-Ming Cheung James Wong
* Joe Junior
* Paul Fonoroff
* Michael Lam
* Jim Lam Yim
* Man Ming Ma
* Brigitte Lin

==See also==
* Cinema of China
* List of Hong Kong films

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 
 