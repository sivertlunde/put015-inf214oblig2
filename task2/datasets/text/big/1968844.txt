Throw Momma from the Train
{{Infobox film
| name           = Throw Momma from the Train
| image          = Throwmommafromthetrain.jpg
| caption        = Theatrical release poster
| director       = Danny DeVito
| producer       = Larry Brezner
| writer         = Stu Silver
| starring       = Danny DeVito Billy Crystal Anne Ramsey Kim Greist David Newman
| editing        = Michael Jablow
| cinematography = Barry Sonnenfeld
| distributor    = Orion Pictures
| released       =  
| runtime        = 88 minutes
| country        = United States
| language       = English
| budget         = $14 million
| gross          = $57,915,972
}}
Throw Momma from the Train is a 1987 American black comedy film directed by Danny DeVito, and starring DeVito and Billy Crystal, with Rob Reiner, Anne Ramsey, Branford Marsalis, Kim Greist, and Kate Mulgrew appearing in supporting roles. 
 Mama from Strangers on a Train, which also plays a role in the film. 

The film received mixed reviews, but was a commercial success. Anne Ramsey was singled out for praise for her portrayal of the overbearing Mrs. Lift; she received a nomination for the Academy Award for Best Supporting Actress.

==Plot== killing his plot development Strangers on a Train, in which two strangers conspire to commit a murder for each other, figuring their lack of connection to the victim will, in theory, establish a perfect alibi. Having overheard Larrys public rant that he wished his ex-wife dead, Owen forms a plan to kill Margaret, believing that Larry will, in return, kill his mother. 

He tracks Margaret down to Hawaii and eventually follows her onto a cruise ship she is taking to her book signing. He then apparently pushes her overboard while she tries to retrieve an earring that fell out. Owen returns from Hawaii to tell Larry of Margarets death and that Larry now "owes" him the murder of his mother, lest he inform the police that Larry was the killer. After having spent the night drinking alone during the hours of Margarets disappearance, Larry panics because he lacks a sufficient alibi. That, along with a news report announcing that the police suspect foul play, convinces Larry that hes the prime suspect. He decides to stay with Owen and his mother in an attempt to hide from the police. Larry meets Mrs. Lift, but despite her harsh treatment of him he refuses to kill her. Eventually, when Mrs. Lift drives Owen to breaking point, Larry finally relents and agrees to go through with the murder.

After two unsuccessful attempts, Larry flees the Lift home when Mrs. Lift recognizes him as a suspect from a news broadcast about his ex-wifes disappearance. He boards a train to Mexico and, surprisingly, Owen and Mrs. Lift come along so as to avoid having to lie for him. During the journey, Larrys patience with Mrs. Lift finally runs out when she impolitely gives him advice on writing. He follows her to the caboose with the intent of throwing her from the train, but Owen begins having second thoughts about having his mother killed and gives chase. In the ensuing fight, Mrs. Lift falls from the train but is rescued by Owen and a repentant Larry. Mrs. Lift is grateful at her son for saving her, but unappreciative of Larrys help and kicks him, resulting in him losing his balance, landing on the tracks, and breaking his leg.

During his recovery in hospital, Larry discovers that Margaret is still alive; she simply fell overboard by accident and was rescued by a Polynesian fisherman whom she has decided to marry. Much to his annoyance, Larry learns that Margaret plans to sell the rights of her ordeal for $1.5 million. On the advice of a fellow patient, Larry chooses to free himself of his obsession with his ex-wife and instead focus on his own life, thereby freeing him of his writers block. 
 natural causes) and that hes going to New York City for the release of his own book. Unfortunately for Larry, Owen reveals that his book is also about their experiences together. Thinking that his book has been scooped once again, an enraged Larry proceeds to strangle him, but stops when Owen shows him that his book is a childrens pop-up book called, Momma, and Owen, and Owens Friend, Larry with the story drastically altered to be suitable for children. Months later, Larry, Owen, and Larrys girlfriend Beth (Kim Greist) vacation together in Hawaii, reflecting on the final chapter of Larrys book. Larrys and Owens books have now become best-sellers, making them both successful writers as well as close friends.

==Cast==
* Danny DeVito as Owen Lift
* Billy Crystal as Larry Donner
* Anne Ramsey as Momma Lift
* Kim Greist as Beth
* Kate Mulgrew as Margaret Donner
* Branford Marsalis as Lester
* Rob Reiner as Larrys agent Joel Bruce Kirby as Detective DeBenedetto
* Oprah Winfrey as Herself
* Farley Granger as Guy Haines (archive footage) Robert Walker as Bruno Anthony (archive footage)

==Reception==
Review aggregation website Rotten Tomatoes gives the film a score of 61% based on reviews from 31 critics, with the consensus being that "Danny DeVitos direction is too broad to offer the kind of nastiness that would have made Throw Momma from the Train truly special, but DeVitos on-screen chemistry with co-star Billy Crystal makes this a smoothly entertaining comedy." 

==Awards and nominations==
{| class="wikitable sortable"
|-
! Award !! Category !! Subject !! Result
|- Best Supporting Anne Ramsey ||  
|- Saturn Award Best Supporting Actress ||  
|- Golden Globe Best Supporting Actress – Motion Picture ||  
|- Best Actor – Motion Picture Musical or Comedy || Danny DeVito ||  
|-
|}

==References==
 

==External links==
 
*  

 

 
 
 
 
 
 
 
 
 
 
 
 