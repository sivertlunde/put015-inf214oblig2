The Masked Bride
{{Infobox film
| name           = The Masked Bride
| image          = The Masked Bride.jpg
| image_size     =
| caption        =
| director       = Christy Cabanne Josef von Sternberg (uncredited)
| producer       = Carey Wilson (scenario)
| story          = Leon Abrams
| starring       = Mae Murray Francis X. Bushman Basil Rathbone
| cinematography = Oliver Marsh
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 60 minutes
| country        = United States Silent English intertitles
| budget         =
}}
 silent romantic drama film, directed by Christy Cabanne and starring Mae Murray, Francis X. Bushman and Basil Rathbone. 

==Synopsis==
Apache dancer Gaby (Mae Murray) defies the Prefect of Police (Roy DArcy) after accosting millionaire Grover (Francis X. Bushman). The underlying truth is that Gaby was driven to theft of rich men by her partner Antoine (Basil Rathbone).

==Cast==
* Mae Murray - Gaby
* Francis X. Bushman - Grover
* Roy DArcy - Prefect of Police
* Basil Rathbone - Antoine
* Pauline Neff - Grovers Sister
* Chester Conklin - Wine Waiter
* Fred Warren - Vibout
* Leo White ...  Floor Manager

==Production notes==
Josef von Sternberg was originally hired to directed the film. After two weeks of filming, he became frustrated with Mae Murrays behavior on the set and ordered the cameramen to film the rafters. He eventually walked out on the picture and was replaced by Christy Cabanne.  

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 


 
 