The Hunchback of Notre Dame (1996 film)
 
 
 
{{Infobox film
| name          = The Hunchback of Notre Dame
| image         = Hunchbackposter.jpg
| alt           =
| caption       = North American release poster, designed by John Alvin   
| director      = Gary Trousdale Kirk Wise
| producer      = Don Hahn Jonathan Roberts
| based on      =  
| starring      = Tom Hulce Demi Moore Tony Jay Kevin Kline Paul Kandel Jason Alexander Charles Kimbrough Mary Wickes David Ogden Stiers
| music         = Alan Menken
| editing       = Ellen Keneshea Walt Disney Feature Animation Buena Vista Pictures
| released      =  
| runtime       = 91 minutes
| language      = English
| country       = United States
| budget        = $100 million   
| gross         = $325.3 million 
}} musical drama Walt Disney Walt Disney novel of Notre Dame and his struggle to gain acceptance into society.
 Stephen Schwartz, both of whom had previously collaborated on Pocahontas (1995 film)|Pocahontas, released a year prior.
 Academy Award Golden Globe Walt Disney Theatrical in Berlin, Germany, as Der Glöckner von Notre Dame, and ran from 1999 to 2002. A direct-to-video sequel, The Hunchback of Notre Dame II, was released in 2002.

==Plot==
  gypsy puppeteer, Notre Dame. He tries to kill the baby as well, but is stopped by the cathedrals archdeacon, who accuses Frollo of murdering an innocent woman. To atone for his sin, Frollo agrees to raise the deformed child in Notre Dame as his son, naming him Quasimodo.
 Festival of magic trick to evade arrest. Frollo scolds Quasimodo and sends him back inside the cathedral.
 Court of burns down countless houses in his way. Phoebus realizes Frollos evil reputation and defies Frollo, who sentences him to death. Phoebus is injured and falls into the River Seine, but Esmeralda rescues him and takes him to Notre Dame for refuge. The gargoyles encourage Quasimodo to confess his feelings for Esmerelda, but he is heartbroken to discover she and Phoebus have fallen in love.

Frollo returns to Notre Dame later that night and realizes that Quasimodo helped Esmeralda escape. He bluffs to Quasimodo saying that he knows about the Court of Miracles and that he intends to attack at dawn. Using the map Esmeralda gave him, Quasimodo and Phoebus find the court to warn the gypsies, only for Frollo to follow them and capture all the gypsies present. Frollo prepares to burn Esmeralda at the stake after she rejects his advances, but Quasimodo rescues her and brings her to the cathedral. Phoebus releases the gypsies and rallies the citizens of Paris against Frollo and his men, who try to break into the cathedral. Quasimodo and the gargoyles pour molten copper onto the streets to ensure no one enters, but Frollo successfully breaks in. He pursues Quasimodo and Esmeralda to the balcony where both he and Quasimodo fall over the edge. Frollo falls to his death in the molten copper, while Quasimodo is caught in time by Phoebus on a lower floor. Afterward, Quasimodo comes to accept that Phoebus and Esmeralda are in love, and he gives them his blessing. The two encourage him to leave the cathedral into the outside world, where the citizens hail him as a hero and accept him into society.

==Voice cast== opening song James Baxter served as the supervising animator for Quasimodo. Esmeralda (singing Gypsy dancing girl who befriends Quasimodo and shows him that his soul is truly beautiful, even if his exterior is not. Highly independent and strong-minded, she abhors Frollos treatment of Pariss gypsies and other outcasts, and seeks justice for them throughout the film. She falls in love with (and later marries) Captain Phoebus. Tony Fucile served as the supervising animator for Esmeralda.
* Tony Jay as Judge Claude Frollo&nbsp;– A ruthless, self-righteous and religiously pious judge who is Quasimodos reluctant guardian. He has an intense hatred of the gypsy population and seeks to annihilate their entire race. In comparison to Victor Hugos The Hunchback of Notre Dame|novel, he also displays a sadistic and lustful obsession with Esmeralda. Frollo generally believes all he does is in Gods will, despite frequent disagreements with the cathedrals Archdeacon.    Kathy Zielinski served as the supervising animator for Frollo.
* Kevin Kline as Captain Phoebus&nbsp;– A soldier who is Frollos Captain of the Guard. He does not approve of Frollos methods and saves people whenever they are in danger, including his love interest Esmeralda. Russ Edmonds served as the supervising animator for Phoebus and Achilles.
* Paul Kandel as Clopin&nbsp;– A puppeteer, storyteller, and mischievous leader of the gypsies. Michael Surrey served as the supervising animator for Clopin.
* Charles Kimbrough, Jason Alexander, and Mary Wickes as Victor, Hugo, and Laverne&nbsp;– Three comical gargoyle statues who are Quasimodos best friends and guardians. This was Mary Wickes final film. After Wickes death, Jane Withers provided the remaining dialogue for Laverne in the films sequel and related merchandise. David Pruiksma served as the supervising animator for Victor and Hugo, while Will Finn served as the supervising animator for Laverne.
* David Ogden Stiers as The Archdeacon&nbsp;– A kind man who helps many characters throughout the film, including Esmeralda. Dave Burgess served as the supervising animator for the Archdeacon.
*Frank Welker as Djali&nbsp;— Esmeraldas pet goat, and Hugos love interest.

==Production==

===Development=== Beauty and the Beast, Gary Trousdale had taken the opportunity to take a break from directing, instead spending several months developing storyboards for The Lion King.  Following this, Trousdale and Kirk Wise subsequently attempted developing an animated feature based on the Greek myth of Orpheus titled A Song of the Sea, adapting it to make the central character a humpback whale and setting it in the open ocean.    The concept obstinately refused to pull together, but while they were working on the project they were summoned to meet with Katzenberg. "During that time," explained Trousdale, "while we working on it, we got a call from Jeffrey. He said, Guys, drop everything – youre working on Hunchback now."    According to Wise, they believed that it had "a great deal of potential...great memorable characters, a really terrific setting, the potential for fantastic visuals, and a lot of emotion."   
 Court of Miracles. 

===Writing===
 
 Phoebus more heroic and central to the story. Out of that decision grew the idea of some sort of a triangle between Quasimodo, Esmeralda and Phoebus."  Some of the novels key characters were jettisoned entirely while the gargoyles of Notre Dame were added to the story by Trousdale and Wise, and portrayed as comedic friends and confidantes of Quasimodo as suggested in the novel, which reads "The other statues, the ones of monsters and demons, felt no hatred for Quasimodo…The saints were his friends and blessed him the monsters were his friends, and protected him. Thus he would pour out his heart at length to them."  

One of the first changes made to accommodate Disneys request was to turn the villainous Claude Frollo into a judge rather than an archdeacon, thus avoiding religious sensibilities in the finished film.  "As we were exploring the characters, especially Frollo, we certainly found a lot of historical parallels to the type of mania he had: the Confederate South, Nazi Germany, take your pick," explained Wise. "Those things influenced our thinking."     Producer Don Hahn evaluated that one inspiration for Frollo was found in Ralph Fienness performance as Amon Goeth in Schindlers List, who murders Jews yet desires his Jewish maid.  For the opening sequence, Disney story veteran Burny Mattinson constructed an effective sequence that covered much exposition, although studio chairman Jeffrey Katzenberg felt something was missing. Following Stephen Schwartzs suggestion to musicalize the sequence, French animators Paul and Gaëtan Brizzi storyboarded the sequence to Menken and Schwartzs music resulting in "The Bells of Notre Dame".  Lyricist Stephen Schwartz also worked closely with the writing team even suggesting that the audience should be left wondering what the outcome of what Phoebus would do before he douses the torch in water in defiance of Frollo.  Another was, unsurprisingly, the films conclusion. While Frollos death was retained – and, indeed, made even more horrific – both Quasimodo and Esmerelda were spared their fates and given a happy ending. This revised ending was based in part on Victor Hugos own libretto to a Hunchback opera, in which he had allowed Captain Phoebus to save Esmerelda from her execution.

===Casting===
In late 1993, pop singer Cyndi Lauper was the first actor attached to the film during its initial stages. Thinking she was cast as Esmeralda, Lauper was startled to learn she was to voice a gargoyle named Quinn, and was hired one week after one reading with the directors.  The development team would later come up with the names of Chaney, Laughton and Quinn – named after the actors who portrayed Quasimodo in previous Hunchback film adaptations. However, Disneys legal department objected to the proposed names of the gargoyles, fearing that the estates of Lon Chaney, Charles Laughton, or Anthony Quinn (who was alive at the time) might file a lawsuit over the use of their names so the names was dropped.    Trousdale and Wise then suggested naming the characters Lon, Charles, and Anthony – which resulted in the same legal concern – before instead naming the first two gargoyles after Victor Hugo, and the third as Laverne, which was selected by Kirk Wise as a tribute to Andrews Sisters singer Laverne Andrews.  Now cast as Laverne, Lauper was deemed too youthful for a friend who was to provide Quasimodo wise counsel while at the same time Sam McMurray – best known for his work on The Tracey Ullman Show – was hired for Hugo. Meanwhile, Charles Kimbrough was cast as Victor who at first was unimpressed at an animated adaptation of Hunchback, but later became rather impressed at the level of research that went into the film and how the story ideas transitioned from the novel to the screen.  After several recording sessions and test screenings, Lauper and McMurray were called by the directors who regrettably released them from their roles.    Jason Alexander, having voiced Abis Mal in The Return of Jafar, was cast as Hugo fulfilling a lifelong desire to be in a Disney film. Laverne was then revisioned into a wiser, mature character with Mary Wickes cast in the role.  Following Wickes death in October 1995,  Jane Withers was hired to voice her six remaining lines.    

 , which was one of several headquarters for Walt Disney Feature Animation.]]
 Out There".  Desiring a huskier voice different from the leading Disney heroines,  Demi Moore was cast as Esmeralda, and met with Alan Menken and Stephen Schwartz on singing. After several singing demos, the actress said "Youd better get someone else," according to Schwartz. New York City cabaret singer Heidi Mollenhauer was selected to provide the singing voice.  For the role of Phoebus, co-director Kirk Wise explained that "As were designing the characters, we form a short list of names...to help us find the personality of the character." Subsequently, the filmmakers modeled his portrayal on the personalities of Errol Flynn and John Wayne, and "One of the names on the top of the list all the time was Kevin Kline."  British actor Tony Jay, who declared his role as Frollo as his "bid for immortality",  was cast after the directors worked with him in Beauty and the Beast. After watching his portrayal as Uncle Ernie in the musical The Whos Tommy, Broadway actor Paul Kandel was selected to voice Clopin.   

===Animation===
Alongside Pocahontas, storyboard work on The Hunchback of the Notre Dame was among the first to be produced for an animated film on the new Disney Feature Animation building adjacent to the main Disney lot in Burbank, which was dedicated in 1995.   However, as the Feature Animation building was occupied with The Lion King and Pocahontas at the time, more animators were hired from Canada and  United Kingdom to join the production team for Hunchback,  and as the development phase furthered along, most of the entire animation team was moved out into a large warehouse facility on Airway in Glendale, California. As the Disney story artists, layout crew, and animators moved in their new quarters, they decided to name the building "Sanctuary." 
 Walt Disney Walt Disney The Legend of Mulan, at least seven animators penned about four minutes of screentime, mostly involving Frollo and Quasimodo. Layout, cleanup, and special-effects artists provided additional support. 

To achieve large-scale crowd scenes, particularly for the Feast of Fools sequence and the films climax, computer animation was used to create six types of characters - males and females either average in weight, fat, or thin - which were programmed and assigned 72 specific movements ranging from jumping and clapping.  Digital technology also provided a visual sweep that freed Quasimodo to scamper around the cathedral and soar around the plaza to rescue Esmeralda. 

===Music===
  Stephen Schwartz were offered multiple film projects to work on when they more or less chose to work on Hunchback being attracted to underlying themes of social outcast and Quasimodos struggle to break free of the psychological dominance of Frollo, according to Schwartz. 
 Stephen Schwartz. Out There" for Quasimodo and Frollo, "Topsy Turvy" also for Clopin, "God Help the Outcasts" for Esmeralda, "Heavens Light" and "Hellfire (song)|Hellfire" for Quasimodo, the Archdeacon, and Frollo, "A Guy Like You" for the gargoyles and "The Court of Miracles" for Clopin and the gypsies.
 Eternal in the British English version. Luis Miguel recorded the version for the Latin American Spanish version, which became a major hit.

==Themes==
The Hunchback of Notre Dame has many themes, including infanticide, lust, damnation, and sin.  One of the most important to the narrative is belief in a loving, forgiving God in Christianity|God. It also implies, according to Mark Pinsky, a "condemnation of abortion, euthanasia, and racism, and   moral resistance to genocide".   
 Tridentine form of the Confiteor as a counterpoint melody. The association of the Church with a form of evil leadership by a man who is "  in almost all respects except the title" "implies a church that is ineffective if not full of vice", the same criticism Hugo gave in his novel. The Gospel According to Disney includes a quote that says "religion... appears as an impotent, irrelevant caricature   Disney refuses to admit a serious role for religion". At one point, the archdeacon says to Esmeralda, "You cant right all the wrongs of this world by yourself... perhaps there is someone in here who can," referring to God. This questions the power religious people actually have in making the world a moral and happy place. 

There is what many view as attack on the Christian Right in the film, due to its strong theme of "religious and moral hypocrisy" - religious figures doing immoral things under the name of religion. Frollo is a complex figure who is torn between "good and evil; chastity and lust." While the church represents "the spirit of a Christian God," this is juxtaposed by the cruel actions and snap judgements of Frollo, who claims to be doing Gods work. The Gospel According to Disney explains that "while Frollos stated goal is to purge the world of vice and sin, according to the opening song, he saw corruption everywhere except within." Because "killing the woman on the steps has put Frollos soul in mortal danger," he has to take the child and look after him as penance. Even then, he absolves himself of agency in the murder by claiming "God works in mysterious ways," and ponders whether "the child may be of use to him one day." During the song "God Help the Outcasts," Esmeralda brings up the point that Jesus&nbsp;— the person in whose name religious people such as Frollo persecute and subjugate "children of God" - was in fact an outcast too. 

According to the films production notes, Quasimodo is "symbolically viewed as being an angel in a devils body." He is "trapped between heaven above   the gritty streets of urban Paris viewed as Hell." The version of the alphabet Quasimodo recites in a daily ritual reflects Frollos view of the world&nbsp;— full of abominations and blasphemy. He is also constantly reminded he is deformed, ugly, a monster, and an outcast who would be hated if he ever left the confines of the church. 

The film also criticizes materialism. When Esmeralda sings "God Help The Outcasts," she "walks in the opposite direction of more prosperous worshipers who are praying for material and earthly rewards." One literary device included in the song is the use of contrast and irony; the rich citizens pray for wealth, fame and "for glory to shine on their names" while the destitute Esmeralda prays for the poor and downtrodden. 

==Release== Jackson Square and utilizing floats and cast members from Walt Disney World.    The film was widely released two days later.

===Marketing===
As part of the promotion of the film, Walt Disney Records shipped two million products, including sing-along home videos, soundtrack CDs, and the "My First Read Along" novelized version of the film, aimed at a toddler demographic. Upon release, The Hunchback of Notre Dame was accompanied by a marketing campaign at more than $40 million with commercial tie-ins with Burger King, Payless Shoes, Nestle and Mattel.  By 1997, Disney earned approximately $500 million in profit with the spin-off products based from the film.  

===Box office=== Disney stores Beauty and the Beast, which opened in half as many theaters, and grossed about $9 million.  In foreign markets, by December 1996, the film became the fifteenth film that year to gross over $100 million surpassing the domestic box office gross, and went on to accumulate $200 million.  The film would ultimately grossed just over $100 million domestically and over $325 million worldwide, making it the fifth highest grossing film of 1996. 

===Critical response===
Review aggregator website Rotten Tomatoes gave the film a 73% positive rating based on 51 reviews with its consensus stating, "Disneys take on the Victor Hugo classic is dramatically uneven, but its strong visuals, dark themes, and message of tolerance make for a more-sophisticated-than-average childrens film."    Chicago Sun-Times film critic Roger Ebert rewarded the film 4 star, calling it "the best Disney animated feature since Beauty and the Beast&nbsp;– a whirling, uplifting, thrilling story with a heart touching message that emerges from the comedy and song".  In his written review for the Chicago Tribune, Gene Siskel awarded the film 3½ (out of a possible 4) stars describing the film as "a surprisingly emotional, simplified version of the Victor Hugo novel" with "effective songs and, yes, tasteful bits of humor".  Owen Gleiberman of Entertainment Weekly graded the film an A in his review and labeled it: "the best of Disneys serious animated features in the multiplex era, (...) an emotionally rounded fairy tale that balances darkness and sentimentality, pathos and triumph, with uncanny grace". 
 Charles Spencer of The Daily Telegraph gave it a positive review, saying "it is thrillingly dramatic, and for long stretches you forget you are watching a cartoon at all... A dazzling treat".  Variety (magazine)|Variety also gave the film a positive review, stating that "there is much to admire in Hunchback, not least the risk of doing such a downer of a story at all" and also saying: "the new film should further secure Disneys dominance in animation, and connoisseurs of the genre, old and young, will have plenty to savor". 

Also addressing the films darker themes, The Daily Mail called The Hunchback of Notre Dame "Disneys darkest picture, with a pervading atmosphere of racial tension, religious bigotry and mob hysteria" and "the best version yet of Hugos novel, a cartoon masterpiece, and one of the great movie musicals".    Janet Maslin wrote in her The New York Times review, "In a film that bears conspicuous, eager resemblances to other recent Disney hits, the filmmakers Herculean work is overshadowed by a Sisyphean problem. Theres just no way to delight children with a feel-good version of this story." 

Upon opening in France in March 1997, reception from French critics towards Hunchback was reported to be "glowing, largely positive".  French critics and audience even found resonance in the film when it mirrored a real-life incident from August 1995 where French police stormed a Parisian church and took away more than 200 illegal immigrants who were seeking sanctuary from deportation. "It is difficult not to think of the undocumented immigrants of St. Bernard when Frollo tries to sweep out the rabble," wrote one reviewer. 

===Audience response===
Some criticism was provided by fans of Victor Hugos The Hunchback of Notre Dame|novel, who were unhappy with the changes Disney made to the material. Arnaud Later, a leading scholar on Hugo, accused Disney of simplifying, editing and censoring the novel in many aspects, including the personalities of the characters. In his review, he later wrote that the animators "dont have enough confidence in their own emotional feeling" and that the film "falls back on clichés." {{cite web
 | last = Laster | first = Arnaud | title = Waiting for Hugo | work = www.awn.com | date = |url=http://www.awn.com/mag/issue1.10/articles/laster.ang1.10.html | accessdate = 2007-08-19 }}  Descendants of Victor Hugo bashed Disney in an open letter to the Libération newspaper for their ancestor getting no mention on the advertisement posters, and describing the film as a "vulgar commercialization by unscrupulous salesmen".   
 come out of the closet.  
 gay and Peter Schneider said, "The only controversy Ive heard about the movie is certain peoples opinion that, Well, its OK for me, but it might disturb somebody else." Schneider also stated in his defense that the film was test-screened "all over the country, and Ive heard nobody, parents or children, complain about any of the issues. I think, for example, the issue of disabilities is treated with great respect." and "Quasimodo is really the underdog who becomes the hero; I dont think theres anything better for anybodys psychological feelings than to become the hero of a movie. The only thing weve been asked to be careful about is the word hunchback, which we have to use in the title."  

===Accolades===
{| class="wikitable sortable" style="width:95%;"
|- style="background:#ccc; text-align:center;"
! colspan="5" style="background: LightSteelBlue;" | Awards
|- style="background:#ccc; text-align:center;"
! Award
! Date of ceremony
! Category
! Recipients and nominees
! Result
|-
| Academy Awards March 24, 1997 Best Music, Original Musical or Comedy Score Stephen Schwartz
|  
|-
| rowspan="12"| Annie Awards
| rowspan="12"| 1997 Best Animated Feature
| Walt Disney Pictures and Walt Disney Feature Animation
|  
|-
| Best Individual Achievement: Animation
| Kathy Zielinski
|  
|-
| Best Individual Achievement: Animation
| James Baxter
|  
|- Directing in an Animated Feature Production
| Gary Trousdale and Kirk Wise
|  
|- Music in an Animated Feature Production
| Alan Menken and Stephen Schwartz
|  
|-
| Producing in an Animated Feature Production
| Don Hahn
|  
|-
| Production Design in an Animated Feature Production
| David Goetz
|  
|-
| Storyboarding in an Animated Feature Production
| Brenda Chapman & Will Finn
|  
|- Voice Acting in an Animated Feature Production
| Tony Jay
|  
|-
| Demi Moore
|  
|-
| Tom Hulce
|  
|- Writing in an Animated Feature Production
| Tab Murphy (story), Irene Mecchi, Bob Tzudiker, Noni White, and Jonathan Roberts
|  
|-
| Golden Globe Awards January 19, 1997 Best Original Score - Motion Picture
| Alan Menken
|  
|-
| ASCAP Award
| 1997
| Top Box Office Films
| Stephen Schwartz
|  
|-
| Saturn Awards July 23, 1997 Best Fantasy Film
|
|  
|-
| BMI Film Music Award
| 1997
|
| Alan Menken
|  
|- Artios Awards
| 1997
| Best Casting for Animated Voiceover
| Ruth Lambert
|  
|-
| Golden Screen Award
| 1997
|
|
|  
|- Motion Picture Golden Reel Awards
| rowspan="2"| 1997
| Motion Picture Feature Films: Sound Editing
|
|  
|-
| Animated Motion Picture Feature Films: Music Editing
|
|  
|-
| Golden Raspberry Awards March 23, 1997
| Worst Written Film Grossing Over $100 Million
| Tab Murphy (story), Irene Mecchi, Bob Tzudiker, Noni White, and Jonathan Roberts
|  
|-
| Satellite Awards January 15, 1997 Best Motion Picture - Animated or Mixed Media
| Don Hahn
|  
|-
| Young Artist Awards
| 1997
| Best Family Feature&nbsp;— Animation or Special Effects
|
|  
|}

===Other achievements===

;American Film Institute Lists
*AFIs Greatest Movie Musicals - Nominated 
*AFIs 10 Top 10 - Nominated Animated Film 

===Home media===
The Hunchback of Notre Dame was first released on VHS, standard CLV Laserdisc, and special edition CAV Laserdisc on March 4, 1997 under the Walt Disney Masterpiece Collection label. Sales and rentals of the VHS release would eventually accumulate to $200 million by summer 1998.   It was originally planned for a DVD release in December 2000 as part of the Walt Disney Gold Classic Collection, but was cancelled in April 2000 for unknown reasons. It was then re-issued on March 19, 2002 on DVD along with its direct-to-video sequel, The Hunchback of Notre Dame II.

Walt Disney Studios Home Entertainment released The Hunchback of Notre Dame on Blu-ray Disc|Blu-ray alongside its sequel in a Special Edition "2-Movie Collection" on March 12, 2013. 

==Adaptations==

===Stage musical===
 
  Walt Disney Theatrical, in Berlin, Germany. The musical Der Glöckner von Notre Dame (translated in English as The Bellringer of Notre Dame) was very successful and played from 1999 to 2002, before closing. A cast recording was also recorded in German. An English-language revival of the musical premiered in San Diego on October 28, 2014. 

===Sequels and spin-offs===
In June 1998, production on a sequel titled The Hunchback of Notre Dame Deux: The Secret of the Bells was announced, and slated for release in fall 1999.  However, the sequel was delayed from its planned fall release in order to accommodate the recording of "Im Gonna Love You" by Jennifer Love Hewitt.    The sequel reunited its original voice cast, with Hewitt, Haley Joel Osment and Michael McKean voicing new characters.  In 2002, the direct-to-video sequel, The Hunchback of Notre Dame II, was released on VHS and DVD. The plot focuses once again on Quasimodo as he continues to ring the bells now with the help of Zephyr, Esmeralda and Phoebuss son. He also meets and falls in love with a new girl named Madellaine who has come to Paris with her evil circus master, Sarousch. Disney thought that it was appropriate to make the sequel more fun and child-friendly due to the dark and grim themes of the original film.
 House of Mouse. Frollo also can seen amongst a crowd of Disney Villains in Mickeys House of Villains.

===Video games===
In 1996, to tie in with the original theatrical release, The Hunchback of Notre Dame: Topsy Turvy Games was released by Disney Interactive for the PC and the Nintendo Game Boy, which is a collection of mini games based around the Festival of Fools that includes a variation of Balloon Fight.

A world based on the movie, "La Cité des Cloches" (The City of Bells), made its debut appearance in the  . It was the first new Disney world confirmed for the game. All of the main characters except Clopin and the Archdeacon (although Quasimodo mentions him in the English version) appear, and Jason Alexander and Charles Kimbrough were the only actors to reprise their roles from the movie.

===Other media===
Disney has converted its adaptation of The Hunchback of Notre Dame into other media. For example, Disney Comic Hits #11, published by Marvel Comics, features two stories based upon the film. From 1997 to 2002 Disneys Hollywood Studios|Disney-MGM Studios hosted a live-action stage show based on the film and Disneyland built a new theater-in-the-round and re-themed Big Thunder Ranch as Esmeraldas Cottage, Festival of Foods outdoor restaurant and Festival of Fools extravaganza, which is now multipurpose space accommodating private events and corporate picnics.

==References==
 

==Sources==
* 
*     
*  

==External links==
 
*  
*  
*  
*  
*  
*  

 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 