The Sun Also Rises (1957 film)
 
{{Infobox film
| name           = The Sun Also Rises
| image          = Thesunalsorises.jpg
| image_size     = 225px
| alt            = 
| caption        = Theatrical release poster Henry King
| producer       = Darryl F. Zanuck
| screenplay     = Peter Viertel
| based on       =  
| narrator       = 
| starring       = Tyrone Power Ava Gardner Mel Ferrer Errol Flynn
| music          = Hugo Friedhofer
| cinematography = Leo Tover
| editing        = William Mace
| distributor    = 20th Century Fox
| released       =  
| runtime        = 130 minutes
| country        = United States
| language       = English
| budget         = $3.5 million 
| gross          = $3,815,000  (US rentals)
}} novel of Henry King. running of the bulls" in Pamplona, Spain and two bullfights. 

==Cast==
* Tyrone Power as Jake Barnes
* Ava Gardner as Lady Brett Ashley
* Mel Ferrer as Robert Cohn
* Errol Flynn as Mike Campbell
* Eddie Albert as Bill Gorton
* Gregory Ratoff as Count Mippipopolous
* Juliette Gréco as Georgette Aubin
* Marcel Dalio as Zizi
* Henry Daniell as Army doctor
* Bob Cunningham as Harris
* Danik Patisson as Marie Robert Evans as Pedro Romero

==Production notes==
===Development===
Film rights to the novel were sold in the late 1920s for a reported $10,000. These rights were transferred to Hemingways first wife, Hadley Richardson, by the author at the time of their divorce, so he never personally benefited from the sale. "Hemingways Sun Also Rises Filmed After 30-Year Wait".
Schallert, Edwin. Los Angeles Times (1923–Current File)   02 June 1957: F1. 

Originally the film was going to be made at RKO, possibly starring Ann Harding. In 1940 agent-producer Charles Feldman bought the rights from Hardings one-time husband, actor Harry Bannister for a reported $35,000. 

In 1948, it was announced Howard Hawks had bought the film rights. He subsequently sold part of his interest to Feldman, but the project did not go beyond the development stage.  In 1955 Hawks and Feldman sold the rights of the novel to Darryl F. Zanuck at 20th Century Fox, who still hoped to use Hawks as director.  This was part of a deal whereby Feldman sold his interest in a number of projects to Fox&nbsp;– the others included Heaven Knows, Mr Allison, The Wayward Bus and Oh Men! Oh Women!. Of this deal the rights to The Sun Also Rises was estimated at $125,000. 

Zanuck hired Peter Viertel to write the script.  Viertel later reflected:
 The long lapse of time since the book was published will not cause it to lose its value. The story is ageless. It should renew its impact for our modern generation. It is fascinating in its impressions of Europe after World War I, because so many of these impressions are duplicated again today.  

===Casting===
Zanuck wanted the lead played by Gregory Peck, who had previously appeared in several Hemingway adaptations, including the popular The Snows of Kilimanjaro]]. Greg Peck Sought as Star of Story by Hemingway
Hopper, Hedda. Chicago Daily Tribune (1923-1963)   14 Jan 1956: 12.   Jennifer Jones signed to play Lady Brett. Louella Parsons: Sun Also Rises for Jennifer
The Washington Post and Times Herald (1954-1959)   08 Feb 1956: 32.   

The movie became the first to be produced for Zanucks own independent production company following his departure from Fox (although Fox would still distribute). ZANUCK WILL FILM CRIMES OF STALIN: Life of Late Soviet Dictator Will Include Data From Speech by Khrushchev On the Story Front Of Local Origin
By OSCAR GODBOUT Special to The New York Times.. New York Times (1923-Current file)   21 June 1956: 33.  

Cinematographer Charles Clarke started filming bullfighting sequences in Pamplona in June 1956. SELZNICK TO FILM HEMINGWAY BOOK: Production on Farewell to Arms to Begin January-- Jennifer Jones Will Star Sun Also Rises Planned
By OSCAR GODBOUT Special to The New York Times.. New York Times (1923-Current file)   25 June 1956: 19.  

Henry King signed to direct and Walter Reisch to produce. Jennifer Jones had to pull out from the film because of her commitment to make A Farewell to Arms for her husband David O. Selznick. Dana Wynter and Robert Stack were mentioned as possible leads. FOX NAMES STARS OF WAYWARD BUS: Widmark and Gene Tierney to Act in Movie Version of John Steinbeck Novel Welles to Co-Star
By THOMAS M. PRYOR Special to The New York Times.. New York Times (1923-Current file)   04 Oct 1956: 29.   Ava Gardner was announced for the female lead, but then pulled out to make Thieves Market with William Wyler, so Susan Hayward was cast in her place. SUSAN HAYWARD TO STAR FOR FOX: She Will Appear in The Sun Also Rises Film Under Henry Kings Direction Extra! Extra! Of Local Origin
By THOMAS M. PRYOR Special to The New York Times.. New York Times (1923-Current file)   26 Dec 1956: 34.   

However Hemingway insisted that Gardner play Lady Brett so Zanuck went after her and succeeded in getting her to sign. NOVEL BY LEMAY BOUGHT FOR FILM: Unforgiven, About Indian Wars, to Be Produced by Hecht-Hill-Lancaster
By THOMAS M. PRYOR Special to The New York Times.. New York Times (1923-Current file)   29 Jan 1957: 27.   "I am convinced Lady Brett Ashley is the most interesting character I have ever played," said Gardner. 

Zanuck later claimed that the casting of Gardner forced the film to be postponed from September 1956 to February 1957. This meant the film could not actually be shot in Pamplona "unless we wanted to shoot a fiesta in the snow". It was decided to film it in Mexico instead. 

In February 1957 Tyrone Power signed to play the male lead. Boy Friend Musical Will Have Star Cast
Hopper, Hedda. Los Angeles Times (1923-Current File)   26 Feb 1957: 22.   Mel Ferrer then joined, followed by Eddie Albert and Errol Flynn. Mel Ferrer Gets Top Sun Also Rises Role; McCrea Walk Tall Star
Schallert, Edwin. Los Angeles Times (1923-Current File)   01 Mar 1957: 19.   Flynn Joins All-Star Zanuck Cast; Skelton Wester on Schedule
Schallert, Edwin. Los Angeles Times (1923-Current File)   12 Mar 1957: 27.   Singer Juliette Greco was also given a role after being spotted singing in a cafe by Mel Ferrer and Audrey Hepburn. ZANUCK VS. DARRYL
Buchwald, Art. Los Angeles Times (1923-Current File)   23 Nov 1958: B5.  

Walter Reisch withdrew as producer because of his other commitments to Fox and Zanuck decided to produce the movie personally. STUDIO INVESTING IN 35 NEW MOVIES: Warners Announces Outlay of $85,000,000, Voicing Faith in the Industry Change of Title
By THOMAS PRYOR Special to The New York Times.. New York Times (1923-Current file)   01 Feb 1957: 25.  
 Robert Evans at the El Morocco and decided to cast him as the young bullfighter Pedro Romero in the film. Businessman Signs for Sun Also Rises
Hopper, Hedda. Los Angeles Times (1923-Current File)   03 Apr 1957: B6.   He did this against the wishes of co-stars Ava Gardner and Tyrone Power, as well as Hemingway himself.  Zanuck overruled all involved, and Evans – who later became a popular producer himself – used Zanucks response as the title for his 1994 biography, The Kid Stays in the Picture. 

===Production===
Filming started March 1957 in Moreli, Mexico. JET PILOT TO GET TARDY LAUNCHING: Universal Will Release Film Finished by Howard Hughes at R.K.O. in 1949-50 Flynn in Zanuck Film
By THOMAS M. PRYOR Special to The New York Times.. New York Times (1923-Current file)   12 Mar 1957: 39.   (It had been intended to shoot in Pamplona but the trees were not in foliage and the production could not afford to wait.  There was also shooting in Spain and France.

Although Hemingways novel had an undefined "mid 1920s" setting when it was published in 1926, the film adaptation places the story around 1922. The film uses the song "You Do Something To Me" as a motif, which was not written until 1929.

Zanuck had originally intended to shoot some scenes on the Fox backlot in Hollywood but changed his mind and took the unit to Paris and Biarritz instead. This added an estimated $250,000 to the budget. WRITERS EYE PART OF PAY-TV PROFITS: Guild Will Ask Separate Remuneration for Stories Used on Toll Video Setting for Sun Also Rises
By THOMAS M. PRYOR Special to The New York Times.. New York Times (1923-Current file)   07 May 1957: 40.  

==Reception==
===Hemingway Reaction===
Ernest Hemingway saw the film but walked out after 25 minutes stating:
 I saw as much of Darryl Zanucks splashy Cooks tour of Europes lost generation bistros, bullfights, and more bistros... Its pretty disappointing and thats being gracious. Most of my story was set in Pamplona so they shot the film in Mexico. Youre meant to be in Spain and all you see walking around are nothing but Mexicans... It looked pretty silly. The bulls were mighty small for a start, and it looked like they had big horns on them for the day. I guess the best thing about the film was Errol Flynn. THE GREAT FEUD OF MR. HEMINGWAY AND MR. ZANUCK
Buchwald, Art. Los Angeles Times (1923-Current File)   29 Nov 1957: B5.   
"That was a kind of lousy thing to say about my picture before the reviews came out," said Zanuck:
 I think a writer is entitled to criticise if there is a complete distortion. But if he sees that there has been a serious attempt to put his story on the screen, even if it failed in some instances, he doesnt have the right to destroy publicly something hes been paid money for... Over 60% of the dialogue in the picture is out of Hemingways book... We treated it as something Holy... We showed the script to him and he made some changes. We even showed it to him again after the changes were made... If the picture doesnt satisfy Hemingway he should read the book again... because the book wont satisfy him... I dont think he saw the picture. I think someone told him about it.  

Flynns performance was highly acclaimed and led to a series of roles where he played alcoholics. Thomas, Tony and Rudy Behlmer & Clifford McCarty, The Films of Errol Flynn, Citadel Press, 1969 p 212-213. 

==Later version==
A later film version of the novel, directed by James Goldstone, was made in 1984.

==References==
 

==External links==
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 