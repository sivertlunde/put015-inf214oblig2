The Zero Years
 
{{Infobox film
|name=The Zero Years
|image=ZeroYearsFilm.jpg
|caption=Theatrical release poster
|director=Nikos Nikolaidis
|producer=Nikos Nikolaidis
|writer=Nikos Nikolaidis
|starring=Vicky Harris Jenny Kitselli Arhontisa Mavrakaki Eftyhia Yakoumi Michele Valley Alkis Panagiotidis Yiorgos Mihalakis Maria Margeti Nikos Moustakas Angeliki Aristomenopoulou Dorothea Iatrou Anastasia Iatrou Nefeli Efstathiadi Christos Houliaras Themis Katz Natasa Kapsabeli
|music=Simeon Nikolaidis
|cinematography=Sifis Koundouros
|editing=Giorgos Triandafyllou
|studio=Last Picture Pro. Greek Film Center
|distributor=Greek Film Center Restless Wind 2005 (Thessaloniki International Film Festival)
|runtime=123 Minutes Greece
|language=Greek Greek
}} 2005 Cinema Greek dramatic experimental Independent independent Surrealist surrealist art film directed by Nikos Nikolaidis, his final film.

==Plot== sexual dominance set in a dystopia, the film is about four female prostitutes, all of whom were sterilized, engaging in sadomasochism with johns.

==Accolades==
The films art director, Marie-Louise Bartholomew, won the Best Production Designer Award at the  .

Professor Vrasidas Karalis, and expert on  . p.  . ISBN 1441135006.  Derek Elley of Variety (magazine)|Variety argued that the film, despite "excesses," has a "strain of irony" that remains constant and that it has an "off-center, ironic flavor."  Karalis argued that it was derivative of Nikolaidis earlier films and that it "confirmed the fossilization of a visual style which had transformed itself into a self-conscious manneristic extravaganza." 
 1967 Belle Belle de 1975 Salò, Singapore Sling (1990 in film|1990), Nikolaidis declared his intention to stop making movies in order to deal with music. 

One Italian critic had the following to say about this film:

{{Quotation Let us turn page with The Zero Years by Nikos Nikolaidis. This film director is one of the few that follow a precise "stylistic thread" and that look at a universe that remains steady in work after work. It is a closed world, overabundant with degraded objects set beside lavish furnitures or those pretending to be  . What is proposed to us is something between a baroque-decorated stage and a secondhand dealers depot. In this closed setting there are agitating characters, usually humiliated and stripped women, larvas in charge of sexual practices which are anything but erotic. In this case they are four girls imprisoned, by a despotic and totalitarian Power, in some kind of brothel for customers (of the Power?) with masochistic tendencies. During a particularly strong "session," a customer nearly dies and the three women hide him in a sort of cage. Soon the Power shows up demanding an explanation for the customers disappearance and puts the women through severe interrogations. The four female figures, spread out in age groups from youth to a sorrowful maturity, represent four stances of slaves in the face of authority (be it political, familiar, or by the group). The tissue is built with an obsessive desire for freedom and rebellion that marries to the almost-conspiratorial acceptance of the submission. All of that is ruled by the impossibility to escape a precise role – the only one that manages to get outside the door of the brothel returns inside by her own decision – together with the knowledge that there is no future and that the outside world is worse than the prison. The movie has features oscillating between the decadent and the intellectualistic, abounds with dialogues, and the direction is unable to "use the scissors" as much as it is needed. Nevertheless, in spite of these flaws, we are in front of an original text and a proposal of a cinema that, at least, is not limited to copying other experiences. Umberto Rossi }}

==Cast==
*Vicky Harris as Prostitute Vicky
*Jenny Kitselli as Leader
*Arhontisa Mavrakaki as Maro
*Eftyhia Yakoumi as Christina
*Michele Valley	as Supervisor
*Alkis Panagiotidis
*Yiorgos Mihalakis
*Maria Margeti
*Nikos Moustakas
*Angeliki Aristomenopoulou
*Dorothea Iatrou
*Anastasia Iatrou
*Nefeli Efstathiadi
*Christos Houliaras
*Themis Katz
*Natasa Kapsabeli

==References==
 

==External links==
*  at      
* 
*  at the      
*  at    
* 
* 
*  at The New York Times  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 