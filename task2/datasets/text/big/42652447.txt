Lost in Panic Room
{{Infobox film name       = Lost in Panic Room image      = Lost in Panic Room poster.jpg caption    =  traditional = 密室之不可告人 simplified = 密室之不可告人 pinyin     = Mìshì Zhī Bùkě Gàorén }} director   = Zhang Fanfan producer   = Song Chunyu Kang Zhenghao
| writer    = Xiao Yu
| based on  =  starring   = Alec Su Pace Wu Hu Ming Jerry Yuan music      = Cheng Chi Gao Ying cinematography = Cai Zhenghui editing    = Mao Mao studio     = Beijing Shengshi Huarui Film Investment Management co., LTD
|distributor= Beijing Shengshi Huarui Film Investment Management co., LTD released =   runtime = 95 minutes country = China language = Mandarin budget   =  gross    = ￥10 million
}}

Lost in Panic Room is a 2010 Chinese suspense thriller film directed by Zhang Fanfan and written by Xiao Yu. The film stars Alec Su, Pace Wu, Hu Ming, and Jerry Yuan.    It was followed by sequel Lost in Panic Cruise. The film was released in China on October 30, 2010. 

==Cast==
*Alec Su as Liu Feiyun, a novelist who is famed for writing detective stories.
*Pace Wu as Zhang Hui.
*Hu Ming as Li Xiaofeng.
*Jerry Yuan as Mike.
*Du Junze as Zhuo Ran.
*Shi Guanghui as Bai Xiuqing.
*Lin Longqi as Lin Quan.
*Yang Xiao as Lin Hai.
*Li Wanyi as Lin Xiaoyu.
*Liu Xiaoxi as Duan Xinyu.
*Zhang Yukui as Master Jiang.
*Yu Dongjiang as Zhang Siyi.

==Box office==
It grossed ￥10 million on its first weekend. 

==References==
 

==External links==
*  
*  

 
 
 
 
 


 
 