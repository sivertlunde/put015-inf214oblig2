David Holzman's Diary
{{Infobox film
| name           = David Holzmans Diary
| image          =
| caption        =
| cinematographer = Michael Wadleigh
| producer       = Michael Wadleigh
| studio         = New Yorker Films
| director       = Jim McBride
| writer         = Jim McBride
| starring       = L.M. Kit Carson
| released       =  
| runtime        = 74 minutes
| country        = United States
| language       = English
}} American film, directed by Jim McBride, which interrogates the art of documentary-making.

It may be considered a docufiction: paradoxically, despite the film is in fact a false autobiography and documentary, «a fairy tale», it represents the filmmakers life and experience and also the world in which the action takes place with documentary realist techniques. The mirror metaphor is explicit in the film as code for reality.
 documentary of his life, who discovers something important about himself while making the movie.
 Great Balls The Big Six Feet Under and The Wonder Years.
 Lorber Films was scheduled on August 16, 2011. 

==See also==
*List of American films of 1967
*Docufiction
*List of docufiction films

==References==
 

== External links ==
*   by Jaime N. Christley at Slant Magazine, June 13, 2011
*    at Rotten Tomatoes 
*   at MOMA (screening June 15–20, 2011)
*  

 

 
 
 
 
 
 
 
 
 


 