Broken Silence (film)
 

{{Infobox film
  | name = Broken Silence
  | image = 
  | caption = Promotional movie poster for the film
  | director = Wolfgang Panzer
  | producer =  
  | genre = fiction
  | starring = Martin Huber  Ameenah Kaplan  Michael Moriarty
  | music = Filippo Trecca
  | cinematography =  
  | editing = Claudio Di Mauro
  | distributor = Camera Obscura (Switzerland)
  | released = 1996
  | runtime = 106 min
  | language = English
  | budget =
  | country        = Switzerland English German German
  | preceded_by =
  | followed_by =
  }}
 Broken Silence  is a film by Swiss director Wolfgang Panzer that was first theatrically released in 1996 in Germany.

==Plot==
The Carthusian monk Fried Adelphi has spent 25 years in a Swiss monastery, keeping his vow of silence and meditation, when his prior instructs him to go and seek the owner of their monastery, in order to extend an expiring 100-year lease. The owner is a vulcanologist; she now lives a secluded life in the mountains of Indonesia. Released from his vow of silence, Fried starts his journey and experiences the culture shock to be expected already in the plane: he loses his wallet, which his seat neighbor, Ashaela, an African American drummer from New York, silently takes. Suffering from claustrophobia, Fried leaves the plane at the Delhi stopover, to continue his travel by sea, but now he has no money. Ashaela offers him a ride to town. Thus begins a journey that will take them both to various parts of India and to Indonesia. 
Fried must learn to adapt his dress rules to the Asian climate. Ashaela eventually admits the origin of the travel funds. We learn she suffers from an incurable disease and may die anytime now. The Carthusian monk throws overboard what is too much of monastic rules and habits, and with Ashaelas help, he eventually fulfills his assignment. Soon after, he cremates the womans body at a palm tree oceanfront, according to her will, but against his own religious beliefs and a Church interdict. Before ending his journey, Fried goes to New York, to bring back a pair of drumsticks to Ashaelas friend.
Then Fried visits a local church to confess. Here begins the film: the Carthusian confesses piece by piece what happened to him and what he did; the impatient New York priest listens only reluctantly at first, then becomes gradually interested. The viewer experiences the road movie at the same pace, in flashbacks.

==Cast==
*Martin Huber as Fried Adelphi
*Ameenah Kaplan as Ashaela
*Michael Moriarty as Mulligan
*Colonel R.K. Kapoor as Immigration Officer

==Production==

Spoken English in this Wolfgang Panzers film comes in different variants: German-Swiss English by Huber, New York English by Kaplan, also Indian and Indonesian English.  
Most of the location scenes were shot with a camcorder in Hi8 video, and only the New York City scenes are in 35-mm film. The audio engineer was Federico Festuccia.

==Reception==
Broken Silence was released with eleven copies in German cinemas and  within a year garnered some 160,000 spectators. It was shown in at the 1998 Katholikentag in Mainz. The jury of the Bavarian film award specially invented a new category honoring Panzers English-language film. 
"It would be easy to simply mock the rules of the religious man, that anachronistic and sometimes too little social must be the modern Europeans. Wolfgang Panzer succeeds the unexpected: almost from behind this monk starts along with his awkward intolerability, his stalwart adherence to his commandments and rituals, to grow the audience at the heart with his childish piety and his astonished awakening to the world. Frieds odyssey is an educational trip, never yet seen in the cinema. Broken Silence is not a pious nor a religious film - it tells the story of a socialization. The monk needs much time and long distances to find out from his self-centeredness. That he sometimes gives off a ridiculous figure, he begins to suspect; but he accepts it with dignity." 

===Awards===
*1995: Shanghai International Film Festival: Best Film
*1996: Geneva Cinéma Tout Ecran: Wolfgang Panzer
*1996: Cinéprix Télécom: Audience Award
*1996: Chicago International Film Festival - Silver Hugo Award: Best Supporting Actor: Michael Moriarty
*1996: Bavarian Film Awards - Special Prize: Wolfgang Panzer
*1997: Berliner Kunstpreis  (Academy of Arts, Berlin): Wolfgang Panzer 

==See also==
*Carthusians Resurrection of the dead (Christianity) Catholic views on cremation

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 