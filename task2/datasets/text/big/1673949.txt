Gilda
 
{{Infobox film
| name           = Gilda
| image          = Gilda.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Charles Vidor
| producer       = Virginia Van Upp
| screenplay     = {{Plainlist|
*Jo Eisinger
*Marion Parsonnet
}}
| story          = E.A. Ellington
| starring       = {{Plainlist|
*Rita Hayworth
*Glenn Ford
*George Macready
*Joseph Calleia
}}
| music          = Hugo Friedhofer
| cinematography = Rudolph Maté
| editing        = Charles Nelson
| distributor    = Columbia Pictures
| released       =  
| runtime        = 110 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Jack Coles Anita Ellis.  In 2013, the film was selected for preservation in the United States National Film Registry by the Library of Congress as being "culturally, historically, or aesthetically significant".   

==Plot==
 {{multiple image
 
| align     = left
| direction = vertical
| width     = 220

 
| image1    = Gilda trailer rita hayworth2.JPG
| width1    =
| alt1      =
| caption1  = Johnny Farrell (Glenn Ford) and Gilda (Rita Hayworth)
 
| image2    = Gilda trailer hayworth1.JPG
| width2    =
| alt2      =
| caption2  = "Gilda, are you decent?"
 
| image3    =
| width3    =
| alt3      =
| caption3  =
 
| image4    =
| width4    =
| alt4      =
| caption4  =
 
| image5    =
| width5    =
| alt3      =
| caption5  =
 
| image6    =
| width6    =
| alt6      =
| caption6  = E
 
| image7    = 
| width7    =
| alt7      =
| caption7  =
 
| image8    =
| width8    =
| alt8      =
| caption8  =
 
| image9    =
| width9    =
| alt9      =
| caption9  =
 

 
| header            =
| header_align      =  
| header_background =
| footer            =
| footer_align      =  
| footer_background =
| background color  =
}}

Johnny Farrell (Glenn Ford), a small-time American gambler newly arrived in Buenos Aires, Argentina, narrates. When he wins a lot of money cheating at craps, he has to be rescued from a robbery attempt by a complete stranger, Ballin Mundson (George Macready). Mundson tells him about an illegal high-class casino, but warns him not to practice his skills there.  Farrell ignores his advice, cheats at blackjack, and is taken by two men to see the casinos owner, who turns out to be Mundson. Farrell talks Mundson into hiring him and quickly gains his confidence. However, the unimpressed washroom attendant, Uncle Pio (Steven Geray), keeps calling him "Mr. Peasant".

One day, Mundson returns from a trip with a beautiful and spirited new wife, Gilda (Rita Hayworth). It is immediately apparent that Johnny and Gilda have a history together, though both deny it when Mundson questions them. Johnny visits Gilda alone in the bedroom she shares with her husband, and the two have an explosive confrontation that elucidates both their past romantic relationship, which ended badly, and their Love–hate relationship|love–hate dynamic. While it is unclear just how much Mundson knows of Gilda and Johnnys past relationship, he appears to be in ignorance when he assigns Farrell to keep an eye on Gilda. Johnny and Gilda are both consumed with their hatred of each other, as Gilda cavorts with men at all hours in increasingly more blatant efforts to enrage Johnny, and he grows more abusive and spiteful in his treatment of her.

Meanwhile, Mundson is visited by two German businessmen. Their secret organization had financed a tungsten cartel, with everything put in Mundsons name to hide their connection to it. However, when they decide it is safe to take over, Mundson refuses to transfer ownership to his backers. The Argentine secret police are interested in the Germans; government agent Obregon (Joseph Calleia) introduces himself to Farrell to try to obtain information, but the American knows nothing about that aspect of Mundsons operations. When the Germans return later, Mundson shoots and kills one of them.

That same night, at Mundsons house, Farrell and Gilda have another hostile confrontation, which begins with them angrily declaring their hate for each other, and ends with them passionately kissing. Mundson arrives at that moment, then flees to a waiting seaplane. Farrell and Obregon witness its short flight; the plane explodes shortly after takeoff and plummets into the ocean. However, Mundson has parachuted to safety, thus faking his death.

With Mundson apparently dead, Gilda inherits his estate. Gilda and Johnny marry, but while Gilda married him for love, Johnny has married her to punish them both for their mutual betrayal of Mundson. He stays away, but has her guarded day and night out of contempt for her and loyalty to Mundson. Gilda tries to escape the tortured marriage a number of times (including one especially memorable and oft-imitated sequence where she tries to humiliate Johnny into setting her free by performing a striptease for a room full of dinner patrons), but Johnny is ultimately able to thwart every attempt, determined to keep her trapped in the relationship that has become a prison for them both. Finally, Obregon tells Farrell that Gilda was never truly unfaithful to Mundson or to him, prompting Farrell to try to reconcile with her.

At that moment, Mundson reappears, armed with a gun. He faked his death to deceive the Nazis. Mundson tells them he will have to kill them both, but Uncle Pio manages to fatally stab him in the back. Obregon shows up, and Johnny tries to take the blame for the murder. Uncle Pio finally credits Johnny for being a true gentleman, while insisting that he had killed Mundson. Obregon reminds them both that Mundson had technically died months before, but there is also such a thing as justifiable homicide. Farrell gives Obregon the incriminating documents from Mundsons safe, and the police confiscate the estate for the government. Farrell and Gilda finally reconcile and confess their mutual love, and apologize for the many emotional wounds they have inflicted on each other.

==Cast==
  in a scene from the film]]
* Rita Hayworth as Gilda Mundson Farrell
* Glenn Ford as Johnny Farrell / Narrator
* George Macready as Ballin Mundson
* Joseph Calleia as Det. Maurice Obregon
* Steven Geray as Uncle Pio
* Joe Sawyer as Casey
* Gerald Mohr as Capt. Delgado Mark Roberts as Gabe Evans
* Ludwig Donath as German
* Don Douglas as Thomas Langford
* George J. Lewis as Huerta Anita Ellis, singing voice of Rita Hayworth in all but the acoustic guitar version of "Put the Blame on Mame", which Hayworth sang herself   

==Production==
Gilda was filmed September 4–December 10, 1945. 

Hayworths introductory scene was shot twice. While the action of her popping her head into the frame and the subsequent dialogue remains the same, she is dressed in different costumes—in a striped blouse and dark skirt in one film print, and the more famous off-the-shoulder dressing gown in the other. 

==Reception==

===Critical response===
When first released, the staff at Variety (magazine)|Variety magazine liked the film and wrote, "Hayworth is photographed most beguilingly. The producers have created nothing subtle in the projection of her s.a.  , and thats probably been wise. Glenn Ford is the vis-a-vis, in his first picture part in several years...Gilda is obviously an expensive production—and shows it. The direction is static, but thats more the fault of the writers." 

More recently, Emanuel Levy wrote a positive review: "Featuring Rita Hayworth in her best-known performance, Gilda, released just after the end of WWII, draws much of its peculiar power from its mixture of genres and the way its characters interact with each other...Gilda was a cross between a hardcore noir adventure of the 1940s and the cycle of womens pictures. Imbued with a modern perspective, the film is quite remarkable in the way it deals with sexual issues." 

The review aggregator Rotten Tomatoes reported that 96% of critics gave the film a positive review, based on twenty-five reviews. 

The film was entered into the 1946 Cannes Film Festival, the first time the festival was held. 

==Cultural references==
 
===Operation Crossroads nuclear test===
 , atomic bomb nicknamed "Gilda", detonated July 1, 1946 ]] bombshell status. Although the gesture was undoubtedly meant as a compliment,    Hayworth was deeply offended. Orson Welles, then married to Hayworth, recalled her anger in an interview with biographer Barbara Leaming: "Rita used to fly into terrible rages all the time but the angriest was when she found out that theyd put her on the atom bomb. Rita almost went insane, she was so angry. … She wanted to go to Washington to hold a press conference, but Harry Cohn wouldnt let her because it would be unpatriotic." Welles tried to persuade Hayworth that the whole business was not a publicity stunt on Cohns part, that it was simply homage to her from the flight crew.    
 The fourth atomic bomb ever to be detonated was decorated with a photograph of Hayworth cut from the June 1946 issue of Esquire (magazine)|Esquire magazine. Above it was stenciled the devices nickname, "Gilda", in two-inch black letters. 

===Other cultural references===
 
* Briefly shown in The Shawshank Redemption, which was based on the Stephen King novella Rita Hayworth and the Shawshank Redemption, both of which utilize a poster of Rita Hayworth as a Chekhovs Gun.

* In the classic Italian movie Bicycle Thieves, the main character Antonio glues a poster of Gilda on the city wall when his bicycle is snatched away.
* The classic Bollywood 1951 hit Baazi, directed by Guru Dutt, starring Dev Anand and Geeta Dutta is inspired by Gilda, where Anand dons the role of a gambler

*Gilda also appears in the film Michael Jacksons This Is It.
 Too Much" in a black-and-white scene based on Rita Hayworths performance in the film.

 "]]

===Memorabilia===
The two-piece costume worn by Hayworth in the "Amado Mio" nightclub sequence was offered as part of the "TCM Presents … Theres No Place Like Hollywood" auction November 24, 2014, at Bonhams in New York.  Estimated to bring between $40,000 and $60,000, the costume sold for $161,000. 

==References==
 

==External links==
 
 
*  
*  
*  
*  
*  
*   by Ned Scott

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 