Cuckoo (2009 film)
 
 
{{Infobox film
| name           = Cuckoo
| image          = Cuckoo film.jpg
| caption        = Theatrical release poster
| director       = Richard Bracewell
| producer       = Tony Bracewell Richard Bracewell
| writer         = Richard Bracewell
| starring       = Richard E. Grant Laura Fraser Tamsin Greig Antonia Bernath Adam F
| music          = Andrew Hewitt
| cinematography = Mark Partridge
| editing        = Craig Cotterill
| studio         = Punk Cinema
| distributor    = Verve Pictures
| released       =  
| runtime        = 89 minutes
| country        = United Kingdom
| language       = English
| budget         =
}}
Cuckoo is a 2009 British thriller film starring Laura Fraser, Richard E. Grant, Tamsin Greig, Antonia Bernath and Adam F, set in London, UK. It was created, written, and directed by Richard Bracewell and produced by Richard and Tony Bracewell. The film was cast by Dan Hubbard of the Hubbard casting family and scored by Bafta-nominated composer Andrew Hewitt.

Cuckoo is described as a "thriller about sounds and lies". The film tells the story of student Polly (Laura Fraser) who begins to think she is going mad as she starts to hear unexplained sounds. The tagline on the films poster and DVD is "Its all in the mind".

==Production==
 
Cuckoo was shot on location in December 2007 and January 2008 in London and Norwich, and in a studio built in a disused grain warehouse in Great Yarmouth|Yarmouth.  Filming locations included Berwick Street and Wardour Street in Soho in London.

==Cast==
* Richard E. Grant as Professor Julius Greengrass
* Laura Fraser as Polly
* Antonia Bernath as Jimi
* Adam F as Chapman
* Tamsin Greig as Simon

==Release== The Skinny wrote that the film was "as tangible a representation of a troubled mind as you are likely to encounter on film". 

Cuckoo was released in UK cinemas on 17 December 2010 by Verve Pictures and on DVD, Blu-ray and iTunes in the UK by Verve Pictures on 28 February 2011. The discs contained an audio commentary and theatrical trailer as extras. 

==Reviews== Time Out commented that the film was "not particularly exciting or original, especially as its never made quite clear what all the fuss is about".  Andrew Lowry in Total Film called it "well-intentioned but fairly undramatic". 

Positive reviews included Jason Solomons in The Mail on Sunday, who wrote: "Using dimly lit interiors and shadowy streets, Bracewells film conjures up a clammy atmosphere, emtionally and physically. The movie feels as it is slowly strangling you. It is a skilful and confident piece of low budget filmmaking."

In a four-star Daily Mirror review, David Edwards wrote that the film was "an unsettling, unconventional ... quite unlike anything our film industry is pumping out these days", adding that "Fraser is superb as a woman who just might be on the verge of a nervous breakdown".  In a four-star Den of Geek review, Doralba Picerno wrote that Cuckoo was "a little gem of a movie ... which will keep you engrossed for its duration and get you to do a lot of thinking about it afterwards. This is independent British cinema at its best, a thought-provoking feature where there are no clear demarcations of either guilt or reality and the atmosphere is rarefied and eerie." 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 