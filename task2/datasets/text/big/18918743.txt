Make Mine Mink
 
{{Infobox film
| name           = Make Mine Mink
| image_size     = 
| image	         = Make Mine Mink FilmPoster.jpeg
| caption        =  Robert Asher Hugh Stewart Earl St. John (executive producer)
| writer         = Michael Pertwee Peter Coke (play) Michael Pertwee
| based on       = Breath of Spring
| screenplay     = Peter Blackmore
| narrator       = 
| starring       = Terry-Thomas Athene Seyler Hattie Jacques Billie Whitelaw Philip Green
| cinematography = Reginald Wyer
| editing        = Roger Cherrill
| studio         = Rank Organisation
| distributor    = J. Arthur Rank Film Distributors
| released       = 9 August 1960 (London)
| runtime        = 101 mins
| country        = United Kingdom
| language       = English
}}
 1960 British Robert Asher and featuring Terry-Thomas, Athene Seyler, Hattie Jacques, Billie Whitelaw, Elspeth Duxbury, Jack Hedley and Raymond Huntley, with cameos by Kenneth Williams and Irene Handl. A group of eccentric misfits go on a spree, stealing mink coats for charity in a Robin Hood-style gang. It was based on the play Breath of Spring by Peter Coke, and its sequels.

==Plot==
A group of lodgers - Major Rayne, Nanette and Pinkie - staying at the home of Dame Beatrice, a formerly wealthy old lady, express boredom with their humdrum lives. A chance for some excitement comes when Lily, Dame Beatrices beautiful, young housekeeper, one day gives in to temptation and lifts a brand new mink coat from their neighbours. She does this in a misguided attempt to thank Dame Bea by replacing a moth-eaten, and thoroughly worn-out old fur. Dame Bea is at first delighted, but upon learning how Lily came by the coat she, and her lodgers, spring into action with a plan to replace the coat without anyone knowing.

Lily has a prison record for juvenile delinquency and theft. Knowing that her heart was right wont save her from prison if discovered. Very fond of Lily, aware of her predicament as well as of her gratitude and great affection for her benefactress, the lodgers join with Dame Bea in an effort to return the fur coat before its owners realize its absence. Despite several comical mishaps the gang manages to return the mink without raising suspicions, thanks to a plan drawn up by the retired Major.

The four are so enthused from the success of their little commando operation that they decide to set themselves up to burgle from the wealthy in order to donate funds to the needy, stealing rather than returning, mink coats then giving the proceeds to Charitable organization|charity.

Their first attempt to rob a boutique goes somewhat awry, but they still manage to get away with a fur coat. However, now that they have a mink, they discover that it is difficult to dispose of. The Major goes to a shady-looking cafe in Limehouse where hes told how to find a Fence (criminal)|fence. It turns out to be a Salvation Army outlet. They keep on searching, whilst keeping their activities from Lily, who has now taken up with a policeman whom she dates frequently.

Eventually Dame Beatrice is able to locate an actual, even reliable, fence only to find to her chagrin it is run by her nephew Freddie, who hugely enjoys the joke of his dotty old auntie turning out to be a latter-day Robin Hood. He agrees to pay her £550 for the mink and Dame Bea assures him that she can provide many more for her charity, an orphanage fallen on hard times. The money raised by Dame Beas gang will save it from closure. The mirthful misfits go on a spree, stealing numerous minks from boutiques and storerooms. Having saved the orphanage the gang decides to help other worthy causes. Their amateurish escapades become widely known through reports appearing in London newspapers. As their notoriety grows, the chances of being caught increase exponentially. On more than one occasion the members of the gang are so bumbling they only narrowly evade capture.

Then Lily discovers their nefarious doings. Horrified, she explains how lucky they are not to be behind bars due to their ineptitude and lack of criminal professionalism. Thoroughly disappointed with Dame B and the others, she makes them promise to stop the criminal activity, to which everyone agrees. However, when a request for money comes from a childrens home, they decide to pull off one last job. The Major plans a raid on a high-tone but nonetheless illegal gambling party. Dame Beatrice enters the place as a titled gambler out on a lark, while the rest of the group dress up as police officers. They stage a raid of the premises planning to make away with all the fur coats in the cloakroom. Their plan goes awry when, little did they know, a real police raid is launched. The gang manages to escape with difficulty, but they still manage to grab a few furs on their way out.

Lily confronts them with the new furs and, feeling guilty for having broken their promise to Lily, the gang expects to be arrested at any moment when Inspector Pape from Scotland Yard turns up on their doorstep. Their edginess increases when he begins talking about stolen furs. Without much hardship they convince the Inspector that theyre a group of harmless eccentrics. Theyre heartily relieved to learn, as it turns out, that the Inspector had come round in connection to the original fur stolen from Dame Beatrice rather than about their burgling sprees.

Once the Inspector departs, a furious Lily demands again that they promise to stop stealing minks ever again. The now contrite gang promises never to steal another fur.

But then another plea reaches Dame Beatrice for a charitable donation. In a discussion with her friends she shares the desperate plea. After a short confab, they decide that, while they cannot break their promise to Lily to steal no more minks, they said nothing about not stealing anything else.
 the Crown Jewels.

==Cast==
* Terry-Thomas as Major Rayne
* Athene Seyler as Dame Beatrice
* Hattie Jacques as Nanette Parry
* Elspeth Duxbury as Pinkie
* Billie Whitelaw as Lily
* Jack Hedley as Jim Benham
* Raymond Huntley as Inspector Pape
* Irene Handl as Madame Spolinski
* Sydney Tafler as Mr. Spanager
* Joan Heal as Mrs. Spanager
* Penny Morrell as Gertrude
* Freddie Frinton as Drunk Michael Balfour as Doorman Noel Purcell as Burglar
* Kenneth Williams as Freddie Warrington

==References==
 	

== External links ==
*  
*  
*  

 
 
 
 
 
 
 
 