A Viking Saga
{{Infobox film
| name           = A Viking Saga
| image          = AVikingSaga2008Cover.jpg
| caption        = US DVD cover
| director       = Michael Mouyal
| producer       = Ole Boetius Alex Cohen
| writer         = Michael Mouyal Dennis Goldberg
| screenplay     = 
| story          = 
| based on       =  
| starring       = Ken Vedsegaard Peter Gantzler Erik Holmey Hans Henrik Clemensen
| music          = Mark Allen
| cinematography = Pierre Chemaly
| editing        = Kinga Orlikowska
| studio         = 
| distributor    = 
| released       =  
| runtime        = 82 minutes
| country        = Denmark
| language       = English Danish
| budget         = 
| gross          = 
}} Rus prince who attacked and conquered Kiev in AD 882 from the Rus war-lords Askold and Dir, before moving his capitol there.

==Cast==
* Ken Vedsegaard as "Oleg of Novgorod"
* Peter Gantzler as "Askold and Dir|Askold"
* Erik Holmey as "Rurik"
* Kim Sønderholm as "Askold and Dir|Dir"

==See also==
* List of historical drama films

==External links==
*  

 
 
 
 
 
 
 

 