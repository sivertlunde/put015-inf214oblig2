Death Rage
{{Infobox film
| name           = Death Rage
| image          = Death Rage.jpg
| image_size     =
| caption        =
| director       = Antonio Margheriti
| producer       = Frank Caruso (executive producer) Raymond R. Homer (producer) Umberto Lenzi (producer)
| writer         = Guy Castaldo Don Gazzaniga
| narrator       =
| starring       = See below
| music          = Guido De Angelis Maurizio De Angelis
| cinematography = Sergio DOffizi
| editing        = Mario Morra Fima Noveck
| distributor    =
| released       = 5 April 1977
| runtime        = 98 minutes (Germany) 98 minutes (USA)
| country        = Italy
| language       = Italian
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Death Rage (Italian: Con la rabbia agli occhi) is a 1976 Italian film directed by Antonio Margheriti.

The film is also known as Anger in His Eyes in the United Kingdom, Blood Reckoning in the USA (video title) and Shadow of the Killer in the Philippines (English title)

== Plot summary ==
A chance for revenge brings a hit man out of retirement in this crime drama directed by genre specialist Antonio Margheriti (aka Anthony M. Dawson). Sal Leonardi is a well-connected American Mafioso who, while vacationing in Naples, visits a racetrack and is persuaded by good natured tout Angelo (Massimo Raniei) to put his money on a long shot. While Angelo sometimes works around the odds at the track by putting front-running horses off their stride with a pellet gun, in this case Angelos horse wins without outside interference and pays off big. But after Sal collects his winnings, hes spotted by Gennare Gallo (Giancarlo Sbragia), a local mob boss who holds a grudge against Sals partners; guns are drawn, Sal and his bodyguards are killed, while Angelo, who is also a police informant, is stripped of his winnings. Back in New York, Leonardis partners are eager to even the score against Gallo, and hey approach Peter Marciani (Yul Brynner), a former hired killer who retired after the traumatic murder of his brother. Peter is persuaded to assassinate Gallo when he learns that the Italian mobster was behind the murder of his brother; Peter flies to Naples and finds an ally in Angelo, but he soon learns that theres more to this story than hes been led to believe.

== Cast ==
*Yul Brynner as Peter Marciani
*Barbara Bouchet as Anny
*Martin Balsam as Commissario
*Massimo Ranieri as Angelo
*Giancarlo Sbragia as Gennaro Gallo
*Sal Borgese as Vincent
*Giacomo Furia as Brigadiere Cannavale
*Loris Bazzocchi as Pasquale
*Rosario Borelli as Gallos henchman
*Luigi Bonos as Peppiniello
*Renzo Marignano as Doctor
*Tommaso Palladino as Gallos henchman

== Soundtrack ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 