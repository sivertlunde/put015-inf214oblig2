The Captive City (1962 film)
 

{{Infobox film
| name           = The Captive City
| image size     = 
| image	=	La Città Prigioniera - original film poster.jpg
| border         = 
| alt            = 
| caption        = Original Italian film poster
| director       = Joseph Anthony
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       =  
| music          = Piero Piccioni
| cinematography = Leonida Barboni
| editing        =  
| studio         = 
| distributor    = Paramount Films of Italy American International Pictures (US)
| released       =   Sept 1964 (USA)
| runtime        = 110 minutes
| country        = Italy
| language       = English
| budget         = 
| gross          = 
}} Italian English-language John Appleby.

==Cast==
*David Niven as Major Peter Whitfield
*Lea Massari  as Lelia Mendores
*Ben Gazzara  as Captain George Stubbs Michael Craig  as  Capptain Robert Elliott
*Martin Balsam  as  Joseph Feinberg
*Daniela Rocca  as Doushka
*Clelia Matania  as  Climedes
*Giulio Bosetti   as  Narriman Percy Herbert   as  Sergent Major Reed
*Ivo Garrani  as Mavroti
*Odoardo Spadaro   as  Janny Mendoris
*Roberto Risso   as  Loveday
*Venantino Venantini  as General Ferolou

==References==
 

==External links==
*  
*  

 
 
 
 
 
 

 