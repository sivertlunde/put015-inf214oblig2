Sunstroke (1953 film)
{{Infobox film
| name           = Sunstroke
| image          =
| caption        =
| director       = Astrid Henning-Jensen Bjarne Henning-Jensen
| producer       = Henning Karmark Aage Stentoft
| writer         = Astrid Henning-Jensen Bjarne Henning-Jensen Preben Neergaard
| starring       = Kjeld Petersen
| music          =
| cinematography = Rudolf Frederiksen
| editing        =
| distributor    =
| released       =  
| runtime        = 89 minutes
| country        = Denmark
| language       = Danish
| budget         =
}}

Sunstroke ( ) is a 1953 Danish comedy film directed by Astrid Henning-Jensen and Bjarne Henning-Jensen.

==Cast==
* Kjeld Petersen - Portieren
* Birgitte Reimer - Frk. Vibeke Søgaard
* Preben Neergaard - Teddy / Theodor Winther
* Ole Monty - Overlæge Grå
* Helle Virkner - Mona Miller
* Lise Ringheim - Eva Linde
* Jessie Rindom - Frk. Suhr
* Dirch Passer - Politimanden
* Agnes Phister-Andresen - Gnaven hotelgæst
* Eik Koch
* Alfred Arnback

==External links==
* 

 
 
 
 
 
 
 
 


 
 