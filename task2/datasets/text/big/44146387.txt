Veena Poovu
{{Infobox film
| name           = Veena Poovu
| image          =
| caption        =
| director       = Ambili
| producer       = Sooryaprakash
| writer         = Ravisankar Ambili (dialogues)
| screenplay     = Ambili
| starring       = Nedumudi Venu Sankar Mohan Uma Sukumari
| music          = Vidyadharan
| cinematography = Vipin Mohan
| editing        = NR Natarajan
| studio         = Mithra Film Makers
| distributor    = Mithra Film Makers
| released       =  
| country        = India Malayalam
}}
 1983 Cinema Indian Malayalam Malayalam film, directed by Ambili and produced by Sooryaprakash. The film stars Nedumudi Venu, Sankar Mohan, Uma and Sukumari in lead roles. The film had musical score by Vidyadharan.   

==Cast==
*Nedumudi Venu
*Sankar Mohan
*Uma
*Sukumari
*Babu Namboothiri
*Bahadoor
*Dr Namboothiri
*MS Warrier
*Gopan
*Thrissur Elsy

==Soundtrack==
The music was composed by Vidyadharan and lyrics was written by Mullanezhi,  and Sreekumaran Thampi. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Chembarathi Kanduvennu   ||  || Mullanezhi || 
|-
| 2 || Kanni maasathil || K. J. Yesudas, Jency || Mullanezhi || 
|-
| 3 || Maalaveppaan Vanniha || Chorus, Thoppil Anto ||  || 
|-
| 4 || Nashta swargangale ningalenikkoru || K. J. Yesudas || Sreekumaran Thampi || 
|-
| 5 || Swapnam kondu thulaabhaaram || Jency || Mullanezhi || 
|}

==References==
 

==External links==
*  

 
 
 

 