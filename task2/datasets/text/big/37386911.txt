Secret Strings
{{infobox film
| name           = Secret Strings
| image          =
| imagesize      =
| caption        = John Ince
| producer       = Maxwell Karger
| writer         = June Mathis
| based on       =  
| starring       = Olive Tell
| music          = Frank D. Williams
| editing        =
| distributor    = Metro Pictures
| released       = October 7, 1918
| runtime        = 5 reels
| country        = United States
| language       = Silent (English intertitles)
}} John Ince directed.  

==Cast==
*Olive Tell – Janet Newell
*William J. Kelly – Raoul Newell
*Hugh Thompson – Hugh Maxwell
*John Daly Murphy – Richard de Giles
*Marie Wainwright – Mrs. de Giles
*Hugh Jeffrey – Ross
*Barbara Winthrop – Katia
*Bert Tuey – Williams
*John Smiley – Benjamin Moraud
*Edward Lawrence – Detective

==References==
{{reflist|refs=
   
   
}}

==External links==
* 
* 
* 
* 

 
 
 
 
 
 
 
 
 

 
 