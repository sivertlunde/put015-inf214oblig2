Eight O'Clock Walk
{{Infobox film
| name           = Eight OClock Walk
| image          = Eight_OClock_Walk_poster.jpg
| image_size     = 
| caption        = 1954 poster for Eight OClock Walk
| director       = Lance Comfort George King Guy Morgan John Baines Ian Hunter
| music          = George Melachrino
| cinematography = Brendan J. Stafford
| editing        = Francis Bieber
| studio         = British Aviation Pictures British Lion  
| released       =  
| runtime        = 87 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = £94,602 (UK) 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
Eight OClock Walk is a 1954 British drama film directed by Lance Comfort and starring Richard Attenborough, Cathy ODonnell, Derek Farr and Maurice Denham. Its plot involves a taxi-driver who is tried for the murder of a young girl on a bomb site. Based on a true story,<ref name=Picturing Justice, the online journal of law and popular culture  Eight o’Clock Walk is an anti-capital punishment film (the title refers to the hour at which executions were traditionally carried out) which points out the danger of circumstantial evidence resulting in the death of a mistakenly accused prisoner, it is only by good fortune that the film’s innocent protagonist gets away in this case – and the unstated message is clearly that not everyone might be so lucky.

==Plot==
Just-married taxi driver Thomas Lesley ‘Tom’ Manning is led to an abandoned bomb-site by an eight-year-old girl who says that she has lost her dog. The kind-hearted Manning gives her his handkerchief to dry her tears. She then runs off taunting Manning as an April-fool prank. He stumbles and raises a fist at her – and this is witnessed by Mrs Zunz.

The girl is later found murdered on the bomb site, strangled as she sang ‘Oranges and Lemons’ while feeding the ducks.

Manning is picked up by Scotland Yard for questioning and is later arrested and charged with murder, with circumstantial evidence including his handkerchief (found under the body of the girl), a fibre from his coat under the dead girl’s fingernail and the testimony of Mrs Zunz. A wartime pilot who suffered a head-wound, even Manning himself started to doubt his mind, and wondered if he had suffered from a "blackout"?

Mannings wife, Jill, convinced he is innocent, contacts lawyers, but the defending barrister refuses to see her and her imprisoned husband, because he wants to preserve an "objective view" on the case. She later wins the sympathy of the junior counsel Peter Tanner, who visits Manning in prison, believes in his protestation of innocence and makes the case his own.

The trial begins at Londons Old Bailey, where Tanner is opposed by his father, prosecuting counsel Geoffrey Tanner. The trail is presided over by Justice Harrington, whose wife is in the hospital undergoing a serious operation.

It soon becomes evident that things are going badly for Manning. Jurors are seen expressing their belief in Manning’s guilt even before the trial was over. Irenes mother gave hearsay evidence that Manning had given the victim sweets, breaking down in tears and accusing Manning of murder. Following the testimony of prosecution-witness Horace Clifford, all of the evidence seems to point to Mannings guilt.

During a recess, Peter Tanner sees Clifford outside the courthouse, giving a sweet to a young girl. Farr identifies the sweet as having been the same as the sweet found on the murdered girl.

When the trial resumes Tanner recalls Clifford for cross-examination, confronting him with the similarity of the sweets, and instructing a street musician to play ‘Oranges and Lemons’ - the same song that was played when Clifford gave the sweet to the child in front of the restaurant, and the song that the child had sung to the ducks when she was murdered.

Clifford breaks down, and Manning is cleared. The film ends with Tanner Senior and Tanner Junior walking away from the camera to share a drink - their camaraderie intact despite the bitter arguments that have gone before. This father and son have been able to fight fiercely and to carry out their legal responsibilities on opposite sides of the case, despite their friendship.

==Main cast==
  
* Richard Attenborough as Thomas Tom Leslie Manning
* Cathy ODonnell as Jill Manning
* Derek Farr as Peter Tanner Ian Hunter as Geoffrey Tanner
* Maurice Denham as Horace Clifford
* Bruce Seton as Detective Chief Inspector
* Lily Kann as Mrs. Adeline Zunz
* Harry Welchman as Justice Harrington
* Kynaston Reeves as Munro
* Eithne Dunne as Mrs. Evans
* Cheryl Molineaux as Irene Evans
* Totti Truman Taylor as Miss Ribden-White Robert Adair as Albert Pettigrew
* Grace Arnold as Mrs. Higgs
* David Hannaford as Ernie Higgs
* Sally Stephens as Edith Higgs
* Vernon Kelso as Superintendent
* Robert Sydney as Ted Lane, dispatcher
* Max Brimmell as Joe, displaced cabbie
* Humphrey Morton as P.C. Tamplin
* Arthur Hewlett as Reynolds
* Philip King as Prison Doctor
* Jean St. Clair as Mrs. Gurney
* Enid Hewitt as Grace
* Noel Dyson as Gallery Regular
* Dorothy Darke as Charwoman
* Bartlett Mullins as Hargreaves
* Sue Thackeray as Girl Ian Fleming as Jury Member Henry B. Longhurst as Clerk of Court
* Elsie Wagstaff as Mrs Peskitt
 

==References==
 

==External links==
*  
* 

 

 
 
 
 
 
 