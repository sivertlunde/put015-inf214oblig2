Quest (2006 film)
{{Infobox film
| name           = Quest Thang
| image          = 
| alt            = 
| caption        = 
| film name      = 
| director       = Amol Palekar
| producer       = 
| writer         = 
| screenplay     = Sandhya Gokhale 
| story          =  Sandhya Gokhale
| based on       =  
| narrator       = 
| starring       = Mukta Barve Rishi Deshpande  Mrinal Kulkarni
| music          = Anand Modak
| cinematography = Debu Deodhar
| editing        = Amitabh Shukla
| studio         = 
| distributor    = 
| released       =    
| runtime        = 116 min.
| country        = India
| language       = English Marathi
| budget         = 
| gross          =
}}
Quest (Marathi title: Thang) is a 2006 bilingual English and Marathi Indian drama film directed by Amol Palekar, starring Mukta Barve, Rishi Deshpande, Mrinal Kulkarni in lead roles. The film is last part of the trilogy on sexuality, which includes  Daayraa (The Square Circle, 1996) and Anahat (film)|Anahat (Eternity, 2001). It is an urban story of a woman who discovers that her husband is homosexual.   It was premiered at the Brisbane International Film Festival (BIFF) on 5 August 2006 .   

This is first English language film made by Palekar.  Both the version in Marathi and English were shot simultaneously, and the shooting was completed in 25 days. 

At the 54th National Film Awards, it won the National Film Award for Best Feature Film in English.      

==Cast==
* Mukta Barve 
* Rishi Deshpande as Aditya
* Mrinal Kulkarni 
* Vijaya Mehta 
* Shishir Sharma
* Sachin Khedekar

== References ==

 

==External links==
* 
*  

 
 
 
 
 
 
 


 
 