Tomie: Replay
{{Infobox film
| name           = Tomie: Replay
| image size     = 
| image	=	Tomie- Replay FilmPoster.jpeg
| border         = 
| alt            = 
| caption        = 
| director       = Fujirō Mitsuishi
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| based on       =   
| narrator       = 
| starring       = Sayaka Yamaguchi   Mai Hōshō   Yōsuke Kubozuka
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 95 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
}}
 of the same name by Junji Ito, specifically the Basement chapter.

==Plot==
The film begins with a six-year-old girl being rushed into a hospital ER with an unusually distended stomach. Doctors begin to operate and find a disembodied head, Tomies head, alive and growing inside the girls belly. The head is placed in a tank of alkaline solution in the basement of the hospital for further observation. Soon after, all five of the hospital workers present during the operation mysteriously leave the hospital or disappear entirely, including hospital Director Morita.
Meanwhile, a few nights later, Takeshi visits his friend Fumihito, who is recovering from some ailment in his hospital room. While visiting, Takeshi is confronted by a naked Tomie (Mai Hosho), now fully grown and escaped from the basement, who asks him to get her out of there. Takeshi takes Tomie to his apartment, and leaves Fumihito by himself, with no explanation. Later, Fumihito calls Takeshi to find out why he left. Takeshi reacts very defensively and irrationally, telling Fumihito that "Tomie belongs to me," already under Tomies evil and seductive influence.

The next morning, Yumi, Director Moritas daughter, visits the hospital in search of her father, missing since the operation. She meets Dr. Tachibana, the only doctor present during the young girls operation who hasnt yet left the hospital. Tachibana gives Yumi a journal recently written by her father, and soon after giving it to her he kills himself.
Through the journal we learn that during the operation, Director Morita and another doctor were accidentally infected with Tomies blood, and that through that blood Tomie is regenerating within the doctors, taking over their bodies and driving them mad. Yumi reads in her fathers journal about him wanting to kill a girl named "Tomie".

Later, Yumi and Fumihito meet at a party and realize they are both looking for a girl named "Tomie". They join forces to find out what happened to Yumis father, and to Fumihitos friend.
The next day, Yumi visits the family of the six-year-old girl who had had the operation. She learns that prior to that operation, the girl had received a kidney transplant from a girl named "Tomie". From this kidney, Tomie had begun to regenerate inside the girls body. Meanwhile, Fumihito visits his friend Takeshi, who had gone mad after killing and decapitating Tomie in a fit of jealousy, then watching her come back to life, regenerating a new head. Takeshi soon after is committed to a mental hospital, and Fumihito becomes Tomies new prey.
That night Yumi has a short and strange run-in with her missing father, during which he babbles about needing to kill Tomie. The following day his dead body is discovered in the hospital basement, bloated and deformed. At his funeral Yumi receives a note from Tomie, asking to meet her that night at the hospital, seemingly for a final showdown.

Yumi arrives at the hospital to find Tomie there to taunt her, and Fumihito, now under Tomies spell, there apparently to kill her. At the last minute Fumihito decides to kill Tomie instead, chopping off her head and burning the remains. Yumi and Fumihito leave the hospital in relief.

==Cast==
* Sayaka Yamaguchi
* Mai Hōshō - Tomie
* Yōsuke Kubozuka

==References==
 

==External links==
*  
*   at SaruDama

 

 
 
 
 
 
 
 
 