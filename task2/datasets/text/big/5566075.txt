Red Hot Romance
{{Infobox film 
| name           = Red Hot Romance
| image          =Red Hot Romance (1922) - Makeup Test.jpg
| caption        =Makeup test during production. Writers Emerson and Loos at left, director Fleming seated, and Sydney and Collins at right.   
| director       = Victor Fleming John Emerson Anita Loos John Emerson Anita Loos
| starring       = Basil Sydney Ernest G. Palmer
| editing        =
| distributor    = Associated First National (*later First National Pictures)
| released       =  
| runtime        = 60 minutes
| country        = United States
| language       = Silent film (English intertitles)
}}
 1922 American silent film directed by Victor Fleming. A fragmentary print survives in the Library of Congress.   

==Cast==
*Basil Sydney - Rowland Stone
*Henry Warwick - Lord Howe-Greene
*Frank Lalor - King Caramba XIII
*Carl Stockdale - General De Castanet
*Olive Valerie - Madame Puloff de Plotz
*Edward Connelly - Colonel Cassius Byrd
*May Collins - Anna Mae Byrd
*Roy Atwell - Jim Conwell Tom Wilson - Thomas Snow
*Lillian Leighton - Mammy
*Snitz Edwards - Signor Frijole

==Production==
The film was shot under the working title Wife Insurance.  A portion of the script under the working title was published in Loos and Emersons book, Breaking into the Movies (1921). 

==References==
 

== External links ==
 
*  
* 
* 

 
 

 
 
 
 
 
 
 
 


 