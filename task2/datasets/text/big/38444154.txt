Big (film)
{{Infobox film
| name           = Big
| image          = Big Poster.jpg
| caption        = Theatrical release poster
| director       = Penny Marshall
| producer       = {{Plain list|
* James L. Brooks
* Robert Greenhut
}}
| writer         = {{Plain list|
* Gary Ross
* Anne Spielberg
}}
| starring       = {{Plain list|
* Tom Hanks
* Elizabeth Perkins
* Robert Loggia John Heard
}}
| music          = Howard Shore
| cinematography = Barry Sonnenfeld
| editing        = Barry Malkin
| studio         = Gracie Films
| distributor    = 20th Century Fox
| released       =  
| runtime        = 104 minutes
| country        = United States
| language       = English
| budget         = $18 million   
| gross          = $151.7 million 
}}
 fantasy comedy John Heard, and Robert Loggia and was written by Gary Ross and Anne Spielberg.
 Like Father Vice Versa Italian film Da grande (1987). 

==Plot== Ring of Fire while attempting to impress Cynthia Benson, an older girl, puts a coin into an unusual antique arcade fortune teller machine called Zoltar Speaks, and makes a wish to be "big." It dispenses a card stating "Your wish is granted", but Josh is spooked to see it was unplugged the entire time.

The next morning, Josh has been transformed into a 30-year-old man. He tries to find the Zoltar machine, only to see an empty plaza, the carnival having moved on. Returning home, he tries to explain his predicament to his mother, who refuses to listen, thinking he is a stranger who kidnapped her son. Fleeing from her, he then finds his best friend, Billy Kopecki, and convinces him of his identity by singing a song that only they know. With Billys help, he learns that it will take a couple of months to find the machine, so Josh rents a flophouse room in New York City and obtains a job as a data entry clerk at MacMillan Toy Company. 
 
Josh runs into the companys owner, Mr. MacMillan, at   vending machine, and a pinball machine. He soon attracts the attention of Susan Lawrence, a fellow McMillan executive. A romance begins to develop, to the annoyance of her competitive boyfriend, Paul Davenport. Josh becomes increasingly entwined in his "adult" life by spending time with her, mingling with her friends and moving in with her. His ideas become valuable assets to MacMillan Toys; however, after celebrating Joshs 13th birthday with him, Billy notices a change in him and feels annoyed and neglected, suspecting that he has forgotten who he really is.
 
MacMillan asks Josh to come up with proposals for a new line of toys. He is intimidated by the need to formulate the business aspects of the proposal, but Susan says she will handle the business end while he comes up with ideas. Nonetheless, he feels overly pressured. When he expresses doubts to her and attempts to explain that he is really a child, she interprets this as fear of commitment on his part, and dismisses his explanation.
  Sea Point Park. In the middle of presenting their proposal to MacMillan and other executives, he leaves. Susan also leaves and encounters Billy, who tells her where Josh went. At the park, Josh finds the machine and makes a wish to become "a kid again." He is then confronted by Susan, who, seeing the machine and the fortune it gave him, realizes he was telling the truth. She becomes despondent at realizing their relationship is over. He tells her she was the one thing about his adult life he wishes would not end and suggests she use the machine to turn herself into a little girl. She declines, indicating that being a child once was enough, and takes him home. After sharing an emotional goodbye, he reverts to his child form. He waves goodbye to her one last time before reuniting with his family. The film ends with him and Billy hanging out together, much like the opening scene.

==Cast==
* Tom Hanks as Josh Baskin
** David Moscow as Young Josh 
* Elizabeth Perkins as Susan Lawrence
* Robert Loggia as MacMillan John Heard as Paul Davenport
* Jared Rushton as Billy Kopecki
* Jon Lovitz as Scotty Brennen
* Mercedes Ruehl as Mrs. Baskin Josh Clark as Mr. Baskin
* Kimberlee M. Davis as Cynthia Benson
* Oliver Block as Freddie Benson
* Debra Jo Rupp as Miss Patterson
* Frances Fisher as Mrs. Kopecki (only in 2007 "Extended Cut" DVD version)

==Reception==
The film was received with almost unanimous critical acclaim; based on 66 reviews collected by review aggregation website Rotten Tomatoes, 97% of critics gave it a positive "Certified Fresh" review and the consensus stating "Refreshingly sweet and undeniably funny, it is a showcase for Tom Hanks, who dives into his role and infuses it with charm and surprising poignancy."  The New York Times praised the performances of Moscow and Rushton, saying the film "features believable young teen-age mannerisms from the two real boys in its cast and this only makes Mr. Hankss funny, flawless impression that much more adorable."  It is also considered by many as one of the best films of 1988. 
 Best Actor Best Writing, Original Screenplay.
 100 Years…100 Laughs" list.  In June 2008, AFI named it as the AFIs 10 Top 10|tenth-best film in the fantasy genre.  In 2008, it was selected by Empire Magazine as one of "The 500 Greatest Movies of All Time." 

;American Film Institute Lists:
*AFIs 100 Years...100 Movies - Nominated 
*AFIs 100 Years...100 Laughs - #42
*AFIs 100 Years...100 Movie Quotes:
**"Okay, but I get to be on top." - Nominated 
*AFIs 100 Years...100 Movies (10th Anniversary Edition) - Nominated 
*AFIs 10 Top 10 - #10 Fantasy Film

==Box office==
The film opened #2 with $8.2 million its first weekend.    It would end up grossing over $151 million ($116 million USA, $36 million international).  It was the first feature film directed by a woman to gross over $100 million.

==Home media==

===Extended Edition=== trailers and TV spots.

===Blu-Ray===
A Blu-ray edition was released on 20 December 2013, in conjunction with celebrating the films 25th anniversary.

==Broadway musical==
  Broadway stage. It featured music by David Shire, lyrics by Richard Maltby, Jr., and a book by John Weidman. Directed by Mike Ockrent, and choreographed by Susan Stroman, it opened on April 28, 1996 and closed on October 13, 1996, after 193 performances.

 , as featured in Big]]

==References==
 

==External links==
 
 
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 