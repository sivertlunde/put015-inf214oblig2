Dalam
{{Infobox film
| name           = Koottam
| image          = Koottam Newspaper Poster.jpg
| caption        = Poster of Tamil version Koottam
| director       = M. Jeevan  (Ram Gopal Varma associate) 
| producer       = M. Sumanth Kumar Reddy
| writer         = Shankar Devaraj
| screenplay     = M. Jeevan
| story          = M. Jeevan Nassar Kishore Kishore Abhimanyu Abhimanyu Singh
| music          = James Vasanthan
| cinematography = 
| editing        = Madhu
| studio         = The Mammoth Media & Entertainment
| distributor    = 
| released       =  
| country        = India
| language       = Telugu Tamil
| budget         = 
| gross          = 
}} action thriller film by debutant director M Jeevan,  a protégé of noted Indian director Ram Gopal Varma. The Telugu version Dalam was produced by M. Sumanth Kumar Reddy under the banner of Mammoth Media & Entertainment and released in the first week of 15 August 2013, while Koottam, the Tamil version, will release in 2014. The film starring *Kishore (actor)|Kishore, Naveen Chandra, Nassar, Abhimanyu Singh, Piaa Bajpai  tells the story of a group of former naxals and their struggles against the police and politicians when they start their lives afresh from Jail. 

==Cast==
*Naveen Chandra as Abhi
*Piaa Bajpai as Shruthi
*Nassar as JK Kishore as Satruvu
*Abhimanyu Singh as Ladda
*Pithamagan Mahadevan
*Saikumar
*Harshavardhan Ajay
*Dhanraj
*Krishnudu as Bhadram Pragathi
*Nathalia Kaur as Item Number

==Plot==
The Scene begins in a Naxal infested area, where gun battle is ensuing between State policeman and Naxals. In the battle, the Naxals lose many men and they decide to forgo their pursuit of war and they decide to hand themselves to government. During their time in jail, they undergo many tortures but then one the Jail Seniors offers them to switch sides to the police and do hit jobs.

==Production==
The film began pre-production in early 2012 with Piaa Bajpai in the role of love interest to Naveen Chandra, the male lead in the film. Chandra had already filmed Therodum Veedhiyile in Tamil. Actors Kishore, Nasser and Saikumar play important roles in the film. James Vasanthan has scored the music. Each scene was shot twice, for a Tamil and Telugu version. 

==Soundtrack==
;Telugu version
*Ekbaar Esukora
*Thayya Thayya
*Ikkadinundi
*Addiravunna
*Yetellina Aaranyame

;Tamil version
*Ithanai Dhooram
*Kaalaana Kannu
*Nigarputha Pinangal
*Pattana Kunjamum
*Yeni Mela

==Release==
The Telugu version of the film released in August 2013 won above average reviews, with a critic noting "its a decently directed movie, and the story flows neatly for the most part" and that "despite its violent content, the film is not as loud as it is thought-provoking. The film is also suitably intriguing."  

==References==
 

 
 
 
 
 
 
 


 