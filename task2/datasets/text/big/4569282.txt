Andha Kanoon
 
{{Infobox film
| name = Andha Kanoon
| image = Andha Kanoon, 1983 film.jpg
| image_size = 
| caption = Promotional Poster
| director       = Rama Rao Tatineni|T. Rama Rao
| producer       = Poornachandra Rao Atluri
| writer         = Shoba Chandrasekhar
| narrator       =  Madhavi Pran Pran Prem Chopra Danny Dengzongpa Amrish Puri
| music          = Laxmikant-Pyarelal
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 8 April 1983
| runtime        = 
| country        = India
| language       = Hindi
| budget = 
| preceded_by = 
| followed_by = 
}}
 Madhavi has a special extended guest appearance portraying The wife of Amitabh Bachchan´s character in the film. The film was a success and became the third highest grossing movie of the year.

The film also has a huge supporting cast including Pran (actor)|Pran, Danny Denzongpa, Amrish Puri, Madan Puri, Prem Chopra and Asrani. Dharmendra also has a guest appearance.            
 Sattam Oru Iruttarai which starred Vijayakanth in lead role.

==Plot==
Jan Nisar Akhtar Khan works as a Forest Officer, and lives with his wife Zakhiya and his daughter Neelu. One day while on duty he comes across some poachers who were cutting sandal wood trees illegally. When challenged, they retaliate, a struggle ensues, and one of them, Ram Gupta, is killed. Khan is charged with killing him, tried in Court, and sentenced to several years in prison. His shocked and devastated wife kills herself along with their daughter. Years later, an angry and embittered Khan is released from prison. One day he comes across another young man, Vijay Singh, and finds out that he is struggling with taking vengeance against three men who had traumatized and killed some of his family members, and decides to help him. It is then Khan finds out that Gupta is still alive and decides to kill him, quite dramatically in the same courtroom where he was convicted for his murder. Vijay also has sister, Miss Durga Singh who has joined Police department just to take revenge against those three men, but law-fully whereas her brother Vijay has decided to kill them one by one by taking law in his own hands as he like Khan does not believe in law. How he succeeds is the entertaining story of this movie.

==Cast==
* Rajinikanth .... Vijay Kumar Singh
* Hema Malini .... Inspector Durga Devi Singh / Shanti
* Amitabh Bachchan .... Jan Nissar Akthar Khan
* Reena Roy .... Meena Srivastav Madhavi .... Zakhiya Khan (Special Extended Guest Appearance) Pran .... Anthony DCruz
* Prem Chopra .... Amar Nath
* Danny Dengzongpa .... Akhbar Ali
* Amrish Puri .... Mr. Ram Gupta
* Madan Puri .... Jailor Gupta
* Om Shivpuri .... Police Commissioner K. B. Lal
* Asrani .... Constable Asrani
* Gouthami
* Dharmendra .... Truck Driver (Guest Appearance)

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Ek Taraf Hum Tum"
| S. P. Balasubrahmanyam, Asha Bhosle
|-
| 2
| "Kabhi Na Kabhi"
| Asha Bhosle
|-
| 3
| "Mausam Ka Taqaaza"
| S. P. Balasubrahmanyam, Asha Bhosle
|- 
| 4
| "Meri Behna"
| Kishore Kumar, Asha Bhosle
|-
| 5
| "Rote Rote Hansna Seekho (Happy)"
| Kishore Kumar
|-
| 6
| "Rote Rote Hansna Seekho (Sad)"
| Kishore Kumar
|-
| 7
| "Yeh Andha Kanoon"
| Kishore Kumar
|}

==Trivia== Sattam Oru Iruttarai starring Vijayakanth and directed by S. A. Chandrasekhar.

* The Tamil film was also remade in Telugu as Chattaniki Kallu Levu starring Chiranjeevi, also directed by S. A. Chandrasekhar.

* The film was also remade in Kannada as Nyaya Ellide starring Shankar Nag, again directed by S. A. Chandrasekhar.
 Seema and directed by K.G. Rajashekaran.

==References==
 

== External links ==
*  

 
 
 
 
 