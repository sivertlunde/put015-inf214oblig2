Salt in the Wound
 
{{Infobox film
| name           = Salt in the Wound
| image          = Salt in wound poster.jpg
| caption        = Film poster
| director       = Tonino Ricci
| producer       = Tonino Ricci
| writer         = Piero Regnoli Tonino Ricci
| starring       = Klaus Kinski
| music          = Riz Ortolani
| cinematography = Sandro Mancori
| editing        = 
| distributor    = 
| released       =  
| runtime        = 98 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}
 macaroni combat" George Hilton.   

==Plot==
Two condemned soldiers (Klaus Kinski and Rai Saunders) and their overseeing West Point officer survive a Nazi ambush on their way to execution. They make their way to a desolate Italian village which happens to be in the path of a German advance. While there they learn the meaning of self-sacrifice and courage when they become emotionally involved in the people and fortunes of the town and must defend the village from the invading Nazi force. 

==Cast== George Hilton as Michael Sheppard
* Klaus Kinski as Cpl. Brian Haskins / Norman Carr Ray Saunders as Pvt. John Grayson / Calvin Mallory
* Betsy Bell as Daniela
* Ugo Adinolfi as American soldier
* Piero Mazzinghi as Priest
* Enrico Pagano as Mascetti
* Roberto Pagano as The Little Michele
* Giorgio De Giorgi as Captain
* Angelo Susani as Sergeant

==Releases==
Wild East Productions released the film on a limited edition NTSC Region 0 DVD double feature with Churchills Leopards in 2007.

== See also ==
* Euro War War Film

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 