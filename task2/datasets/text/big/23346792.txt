Die Jungfrau auf dem Dach
{{Infobox film
| name           = Die Jungfrau auf dem Dach
| image          =
| image_size     =
| caption        =
| director       = Otto Preminger
| producer       = Otto Preminger
| writer         = Carl Zuckmayer Translation of a screenplay by F. Hugh Herbert
| narrator       =
| starring       = Hardy Krüger Johannes Heesters Johanna Matz
| music          = Herschel Burke Gilbert
| cinematography = Ernest Laszlo Otto Ludwig
| distributor    = United Artists
| released       = June 19, 1953
| runtime        = 99 minutes
| country        = United States
| language       = German
| budget         =
| preceded_by    =
| followed_by    =
}} American comedy film produced and directed by Otto Preminger. The screenplay by Carl Zuckmayer is a German language translation of the script for The Moon is Blue by F. Hugh Herbert, based on his 1951 play.

==Plot==
A comedy of manners, the film centers on virtuous actress Patty ONeill, who meets playboy architect Donald Gresham on the observation deck of the Empire State Building and accepts his invitation to join him for drinks and dinner in his apartment. There she meets Donalds upstairs neighbors, his ex-fiancée Cynthia and her father, roguish David Slader. Both men are determined to bed the young woman, but they quickly discover Patty is more interested in engaging in spirited discussions about the pressing moral and sexual issues of the day than surrendering her virginity to either one of them. After resisting their amorous advances throughout the night, Patty leaves and returns to the Empire State Building, where Donald finds her and proposes marriage.

==Production== Broadway production of F. Hugh Herberts play The Moon is Blue, and its successful run of 924 performances prompted him to contract with United Artists to finance and distribute a screen adaptation over which he would have complete control. He deferred his producers and directors salaries in exchange for 75% of the films profits.  

Since Herberts play had been a huge success in Germany, Preminger decided to film English- and German-language versions simultaneously, using the same sets but different casts. The director estimated this method would increase the filming schedule by only eight to ten days and production costs by only 10 to 15 percent. The budget for both films was $373,445.  
 cameo roles William Holden and Maggie McNamara of the American cast play in the German version.  

In later years, Preminger stated he much preferred The Moon is Blue to Die Jungfrau auf dem Dach because he felt the psychology of the plot did not translate well.  

==Cast==
*Hardy Krüger ..... Donald Gresham 
*Johannes Heesters ..... David Slader 
*Johanna Matz ...  Patty ONeill 
*Sig Ruman ..... Michael ONeill 
*Dawn Addams ..... Cynthia Slader

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 