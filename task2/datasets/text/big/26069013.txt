Dragon Trap
{{Infobox film
| name           = Dragon Trap
| image          = Ejder kapani.jpg
| image_size     =
| caption        = Theatrical poster
| director       = Uğur Yücel
| producer       = Erol Avcı
| writer         = Kubilay Tat
| narrator       =
| starring       =  
| cinematography = Tolga Kutlar
| music          = Soner Akalın
| editing        = 
| distributor    = 
| released       =  
| runtime        = 110 minutes
| country        = Turkey
| language       = Turkish
| budget         = 
| gross          = $4,644,486
| preceded_by    = 
| followed_by    =
}}
Dragon Trap ( ) is a 2010 Turkish crime-thriller film, directed by Uğur Yücel, about two detectives investigating a series of murders of released pedophiles, which was sponsored by the Istanbul Police Department. The film, which went on nationwide general release across Turkey on  , is one of the highest-grossing Turkish films of 2010.   

== Production ==
The eight week shoot started on June 15, 2009 and included seven weeks in Istanbul and one week in Erzincan, Turkey.    A French team came to Turkey to contribute to the production. 

==Plot==
Ensar (Nejat İşler) has turned into a ruthless death machine during his military service, during which he fought terrorists in numerous clashes in southeast Anatolia. Having just completed his compulsory military service, Ensar returns home, but on the very day he comes home, he finds out that while he was away, his 12-year-old sister was raped. Later, unable to cope with the trauma, the little girl hung herself at a mental asylum she was put in following the incident. In the meantime, Ensar’s hometown is shaken with a number of murders. Two skillful detectives from the homicide department, Abbas (Uğur Yücel) and “Akrep” Celal (Kenan İmirzalıoğlu), along with rookie Ezo (Berrak Tüzünataç) are assigned to investigate the murders. The case will be the last one Abbas has to solve before the retirement he has been anticipating for a long time to put into practice his plans to move to a quiet and far away place with his longtime girlfriend Cavidan (Ceyda Düvenci).

==Cast==
*Uğur Yücel (Çerkez Abbas) 
*Kenan İmirzalıoğlu (Akrep Celal) 
*Nejat İşler (Ensar) 
*Berrak Tüzünataç (Ezo) 
*Ceyda Düvenci (Cavidan) 
*Ayşe Nil Şamlıoğlu (Ensars mother)
*Ozan Güven (Remzi)
*İlker Aksum (Doctor)

==Release==
The film opened in 407 screens across Turkey on   at number two in the Turkish box office chart with an opening weekend gross of $1,292,163.   

{| class="wikitable sortable" align="center" style="margin:auto;"
|+ Opening weekend gross
|-
!Date!!Territory!!Screens!!Rank!!Gross
|-
|  
| Turkey
| 407
| 2
| $1,292,163
|-
|  
| Germany
| 55
| 18
| $140,125
|-
|  
| Austria
| 9
| 21
| $20,210
|}

==Reception==

===Box office===
The film was number two at the Turkish box office for two weeks running and has made a total gross of $4,644,486. 

== See also ==
* 2010 in film
* Turkish films of 2010

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 


 
 