Vacant Possession (film)
 
{{Infobox film
| name           = Vacant Possession
| image          = 
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Margot Nash John Winter
| writer         = Margot Nash
| screenplay     = 
| story          = 
| based on       = 
| narrator       =  John Stanton
| music          = Alistair Jones
| cinematography = Dion Beebe
| editing        = Veronika Jenet
| studio         = 
| distributor    = Wintertime Films
| released       =  
| runtime        = 95 minutes
| country        = Australia
| language       = English
| budget         = 
| gross          =
}}

Vacant Possession is a 1995 Australian drama film, directed and written by Margot Nash. The film was nominated for 5 awards at the 1995 Australian Film Institute Awards.

==Plot==
 

Following the death of her mother Tessa (Pamela Rabe), a young woman, returns after many years to the weather-beaten family home on the shores of Sydneys Botany Bay. But the old family home begins to bring old wounds more and more to life. The story unfolds through flashbacks yet as it progresses the flashbacks merge into the present as it becomes apparent that the situation Tessa has returned to is very much the result of that which passed before.

This is a film about memory - personal and collective. To quote the director: the story of a house, land and two families - one white, one Aboriginal - both living in the shadow of the past. The deliberately ambiguous title could refer to the house itself, but also the history of White Australia (see Terra nullius).

==Cast==
Actor/Actress and Character
*Pamella Rabe...Tessa John Stanton...Frank
*Toni Scanlan...Joyce
*Linden Wilkinson...Kate
*Rita Bruce...Aunty Beryl
*Olivia Patten...Millie
*Simone Pengelly...Teenage Tessa Melissa Ippolito...Young Tessa Tommy Lewis ...Billy Bill Young...Estate Agent
*Barbra Wyndon...Thea
*Tony Barry...Salvation Army Man Ian Spence...Volunteer

==Awards==

===Nominations===
*Australian Film Institute 1995:
**AFI Award - Best Editing: Veronika Jenet
**AFI Award - Best Director: Margot Nash
**AFI Award - Best Screenplay: Margot Nash John Patterson

==External links==
*  

 
 
 
 
 


 