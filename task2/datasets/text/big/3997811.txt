The Three Stooges in Orbit
{{Infobox film
  | name           = The Three Stooges in Orbit
  | image          = inorbitposter.jpg
  | caption        = 
  | director       = Edward Bernds
  | producer       = Norman Maurer
  | writer         = Norman Maurer Elwood Ullman  Don Lamond Peter Dawson Peter Brocco  
  | music          = Paul Dunlap
  | cinematography = William P. Whitley	
  | editing        = Edwin H. Bryant
  | distributor    = Columbia Pictures 
  | released       =   
  | runtime        = 87 minutes
  | country        = United States
  | language       = English 
  | budget         = 
  | gross          = $1,500,000   

}}
The Three Stooges In Orbit is the fourth feature film to star the Three Stooges after their 1959 resurgence in popularity. By this time, the trio consisted of Moe Howard, Larry Fine, and Joe DeRita (dubbed "Curly Joe"). Released by Columbia Pictures, The Three Stooges In Orbit was directed by long-time Stooge director Edward Bernds, who Moe later cited as the teams finest director.

==Plot==
The Stooges are TV actors who are trying to sell ideas for their animated television show The Three Stooges Scrapbook. Unfortunately, their producer does not like anything. He gives the boys ten days to come up with a gimmick or their show will be canceled. In the meantime the Stooges lose their accommodation when they are caught cooking in their room because Curly-Joe turned up the TV-disguised refrigerator way too loud. The only affordable accommodation that will allow cooking is found in an advertisement in a newspaper. The home belongs to Professor Danforth (Emil Sitka) and it resembles a castle.

Professor Danforth is convinced that Martians will soon invade Earth. He persuades the boys to help him with his new military invention—a land, air and sea vehicle (tank, helicopter, flying submarine). In return, Danforth will create a new "electronic animation" machine for the Stooges to use in their television show. The boys think the Professor a crank but accept his eccentricities along with his accommodation. No one, especially the FBI listens to the Professors cries for help but the boys apprehend Danforths butler who dresses like a monster to terrify the Professor. In reality the butler is a Martian spy made to look like a human.
 alien spies the Twist craze through the Martians communication device, they are offended and call off the invasion, opting instead to destroy Earth.

Meanwhile, the Stooges give the vehicle a test run. They mistakenly enter a nuclear test area, when their engine malfunctions. They land near a test rig where a test nuclear depth bomb is set up. The Stooges take the bomb, thinking it is a carburetor, and fasten it to the engine. Water, meant to detonate the bomb, shoots out of the testing rig. The military is bewildered by tests failure. With the bomb attached to the engine, the vehicle now performs beyond expectations, even going into space. 
 

Later, the Martians steal the vehicle, mount a ray gun and begin destroying targets (including Disneyland). The boys sneak onto the craft to stop them. The Stooges are able to use one of the Martians ray guns to separate the fuselage from the conning tower. The fuselage, holding Ogg and Zogg, crashes into the ocean, detonating the nuclear depth bomb. Clinging to the auto-rotating helicopter section, the Stooges survive, crashing through the roof of their television studio.

==Cast==
*Moe Howard – Moe
*Larry Fine – Larry
*Joe DeRita – Curly-Joe
*Emil Sitka – Professor Danforth
*Carol Christensen – Carol Danforth
*Edson Stroll – Cpt. Tom Andrews
*George N. Neise – Ogg/Airline Pilot
*Rayford Barnes – Zogg/Airline Co-Pilot
*Norman Leavitt – Williams, the Butler
*Nestor Paiva – Chairman
*Don Lamond – Col. Smithers Peter Dawson – Gen. Bixby
*Peter Brocco – Dr. Appleby
*Cheerio Meredith – Tooth Paste Old Maid

==Production==
The Three Stooges in Orbit was born out of The Three Stooges Scrapbook, an unsold color television pilot produced in 1960 at a cost of $30,000. Producer Norman Maurer reprocessed the Scrapbook footage into black and white and built the plot around the concept of the Stooges rehearsing for their television show. In addition, Maurer was able to use many film props originally used in the film Forbidden Planet. 

==See also==
*Landwasserschlepper

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 