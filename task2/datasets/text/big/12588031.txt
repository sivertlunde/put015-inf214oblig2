InAlienable
{{Infobox film name = InAlienable image      = caption    = InAlienable theatrical poster director   = Robert Dyke writer     = Walter Koenig starring  Richard Hatch, Andrew Koenig, Alan Ruck, Richard Herd, Gary Graham, Philip Anthony-Rodriguez music by    = Justin R. Durban producer   = Walter Koenig
|distributor= Renegade Studios budget     = 
|released= 2008 runtime    = 106 minutes country  = language = English
|}}
 horror and comic elements, written and produced by Walter Koenig, and directed by Robert Dyke. It was the first collaboration of Koenig and Dyke since their 1989 production of Moontrap. Koenig said that "the story really involves that relationship between the human being and the alien. At first, its assumed that the alien   a parasite growing in a host, but because it has some of the human DNA, its significantly more than that. Even though it comes from another world, its a part of our world. Really, its a love story." 

==Plot==

Dr. Eric Norris remains wracked with guilt after a terrible tragedy that cost him his family, and when he learns that an alien parasite is not only growing inside him but shares his DNA, he develops a fiercely paternal bond with the creature. The alien "might regenerate into a surrogate son to replace his own child who was lost years earlier."  Koenig plays the antagonist, a scientific research institute administrator who dislikes Norris because he had been mutually in love with Norriss wife, who died; he blames Norris for his own now bleak life and enjoys insulting him. Norris, new love interest Amanda, and attorney Howard Ellis must defend the new child, Benjamin, from being imprisoned by government forces or, worse, destroyed. The courtroom trial covers such issues as habeas corpus and miscegenation.

At first horrified by the growth within him, Eric Norris comes to love the child that he "gives birth to" and names Benjamin. When a court trial begins to take Benjamin away from him, all sorts of arguments are raised about the real meaning of the word "alien." A disturbed man, Emil (Andrew Koenig, Walter Koenigs son), brings a gun into the courtroom and shoots Benjamin, to the loss of all concerned.

== Cast ==

*Walter Koenig  ... 	Dr. Shilling
*Alan Ruck	   ... 	Dr. Proway
*Bonnie Aarons	   ... 	Blue Skinned Woman
*Marina Sirtis	   ... 	Attorney Barry
*Erick Avari	   ... 	Howard Ellis
*Courtney Peldon	   ... 	Amanda Mayfield
*Patricia Tallman ... 	Dr. Klein Richard Hatch	   ... 	Eric Norris Andrew Koenig	   ... 	Emil
*Gary Graham	   ... 	Andreas Cabrosas
*Priscilla Garita	 ... 	Miriam
*Jay Acovone	 ... 	Gerhard
*Gabriel Pimentel	 ... 	Benjamin (younger)
*Lisa LoCicero	 ... 	Dr. Magee
*Randy Barnett	 ... 	Richard Herd
*Jarrett Grode	 ... 	Medical Employee
*Philip Anthony-Rodriguez	 ... 	Braxton
*J. G. Hertzler	 ... 	Dr. Lattis
*Judy Levitt	 ... 	Judge Deville
*Jeff Rector	 ... 	Professor Jeffries
*Johnny Drocco	 ... 	Security guard #2
*Gelbert Coloma	 ... 	FBI
*Robert Olding	 ... 	Medical Assistant #1
*Amanda Chism	 ... 	Protestor
*Vitaliy Versace	 ... 	Restaurant Patron
*Ceilidh Lamont	... 	Lawyer
*Anthony Fitzgerald	... 	Zachary - in Court Room
*Frances Emma Jenkins	... 	Lab Worker
*Kyle Robertson	 ... 	Press Photographer #2
*James Runcorn	 ... 	Bailiff
*Jennifer Gabbert	 ... 	Hospital Employee #1
*Brandon Ford Green	 ... 	Court Clerk
*Jett Patrick	 ... 	Adam
*Matt Lasky	 ... 	Burly Medical Tech
*George Anton	 ... 	Restaurant Patron
*Bruce Van Patten	 ... 	Courtroom observer
*Shweta Thakur	 ... 	Bar Customer
*Jeremy Clark	 ... 	Protester
*Bradley Laise	 ... 	Benjamin (older)
*Patrick Gough	 ... 	Security #1
*Paul Danner	 ... 	Sci-Fi Guard
*Peter Renaud	 ... 	Labworker
*Jonah Runcorn	 ... 	10 Year Old Boy
*Marvin Rouillard	 ... 	Important Man
*Jett Patrick Williams	 ... 	Adam
*Ashley Walsh	 ... 	Nurse
*Oliver Rayon	 ... 	Courtroom Patron
*Bob Ross	 ... 	Court Observer
*Noe Sanchez	 ... 	Janitor
*Zahra Zaveri	 ... 	Bar Customer
*Jane OGorman	 ... 	Bar Patron
*Giovanna Silvestre	 ... 	Laboratory Employee
*Tim Russ  ... 	news anchor

== Production == familial bonding. civil and human rights. But the actual event that sat me down at the computer was when two friends of mine who didnt know each other joined me to watch the New York Yankees play in the World Series back in 2001. My friend Tony Franke had been in the original film of The Blob (1958) and Sky Conway had always been a big fan of the movie. Inspired by Tonys stories, Sky confessed to always wanting to shoot a film about a meteor landing with an alien presence aboard. I holed myself up in my room and took it from there."   

It took Koenig a decade to bring the film to fruition.  He initially wrote the lead role for himself, but reevaluated his view "that the audience would buy me as a romantic interest", and offered it to   as one — so Im not unfamiliar with emotional mechanisms and mechanics that go along with it." 

Hatch talked about why he accepted the role: "It was very character-driven. It was about
people and relationships. It wasnt   about some monster or strange creature; it was
about human beings having to deal with very morally conflicted situations. I wasnt
choosing InAlienable because it was a science-fiction piece, although I love science
fiction. I was choosing it, as I choose any movie that I go watch, because it was about
something meaningful. It was about real people having to struggle with issues that all of
us have to face in our lives." 

Marina Sirtis of   and other familiar actors from sci-fi franchises were also willing to participate. Koenigs wife, son and daughter had roles on screen. 

It was shot on location in Southern California. It is the first feature film made by Renegade Studios, and was released over the internet on a pay-per-view basis.  , PRWeb,com, November 12, 2007 

The films debut public screening was cut short by Koenig, as the visual quality on the screen at Conglomeration! 2008 was not up to his standards. 

==Release==
This Direct-to-video|direct-to-DVD film made its debut on the Internet on a pay-per-view basis; it premiered online on December 15, 2008 with a nominal $2.99 fee charged for watching it. 

==Cultural allusions==
* The X-Files - Shilling says, "Norriss friend found a foreign organism attached to a piece of the meteor, and he gave it to him. According to what Norris told him, the thing has gotten inside of him and is growing at an accelerated pace. Norris believes that it is some sort of alien parasite thats determined to free itself and, in the process, kill him."  Gerhard, a government scientist, wonders, "Didnt I see this on X-Files?"
* Attorney Ellis says to Amanda, "I know therere aliens out there. At least, Im pretty sure therere aliens out there. I mean, how else would you explain Andy Kaufman or Jim Carrey?" tabloids at The Globe, The Star. In the same conversation, Ellis refers to The New York Times, Rolling Stone, and The Wall Street Journal.  click my boots to make the point?" The Elephant Man - Attorney Barry argues in court that "the Elephant Man was a human being... This creature is not a human being, not even remotely."
*  "

==Reception==
The film received mixed reviews. Review aggregate website Rotten Tomatoes summary, taken from Jason Buchanan of Rovi Corporation|Rovi, declared: "A thought-provoking sci-fi drama headed up by a stellar cast, InAlienable tells the tale of a successful researcher who gives birth to an extraterrestrial child, and wages a passionate fight to defend his offspring from society and government scientists." 
 The Outer courtroom drama... Total Sci-Fi Online granted only 1 star out of 10, saying, "At the core of InAlienable is an idea around which a terrific movie could be made, but unfortunately this isnt it."  The website DVD Verdict wrote, "A courtroom thriller masquerading as a sci-fi drama, InAlienable is an intriguing effort with higher acting caliber than one might expect... InAlienable is far from groundbreaking cinema, but Koenig plays around with a lot of interesting ideas and themes. Not only does he play the slimy Schilling with tang, he also builds up his story well, keeping us hooked most of the way... What anchors the film is Hatchs powerful performance, but Peldon also makes a strong impression as his partner and lover." 

==References==
 

== External links==
* 
* 
* 

 
 
 
 
 