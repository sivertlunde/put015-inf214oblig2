Up Pompeii (film)
{{Infobox film
| name           = Up Pompeii
| image          = "Up_Pompeii"_(1971).jpg
| caption        = Theatrical poster
| director       = Bob Kellett
| producer       = Ned Sherrin
| writer         = Sid Colin
| starring       = Frankie Howerd Michael Hordern Ian Wilson
| editing        = Al Gell
| distributor    = EMI Films|Anglo-EMI
| released       = 1971
| runtime        = 90 minutes
| country        = United Kingdom
| language       = English
}}
 Elstree Film Studios, Borehamwood, England. The film is based on characters that first appeared in the British television sitcom Up Pompeii! (1969–1975).

==Plot==
Lurcio (Frankie Howerd) becomes the inadvertent possessor of a scroll bearing all the names of the proposed assassins of Nero (Patrick Cargill). The conspirators need to recover the scroll fast, but it has fallen into the hands of Lurcios master, Ludicrus Sextus (Michael Hordern) who mistakenly reads the contents of the scroll to the Senate. Farcical attempts are made to retrieve the scroll before Pompeii is eventually consumed by the erupting Vesuvius.

==Cast==
* Frankie Howerd as Lurcio
* Michael Hordern as Ludicrus Sextus
* Barbara Murray as Ammonia
* Patrick Cargill as Nero
* Lance Percival as Bilius
* Bill Fraser as Prosperus Maximus
* Julie Ege as Voluptua
* Adrienne Posta as Scrubba
* Bernard Bresslaw as Gorgo (Neros Champion)
* Madeline Smith as Erotica
* Roy Hudd as Neros M.C.
* Hugh Paddick as Priest
* Royce Mills as Nausius
* Rita Webb as Cassandra
* Lally Bowers as Procuria
* Aubrey Woods as Villanus Billy Walker as Prodigious
* Russell Hunter as Jailor
* Laraine Humphrys as Flavia
* Kenneth Cranham as 1st Christian George Woodbridge as Fat Bather
* Derek Griffiths as Steam Slave
* Robert Tayman as Noxius
* Carol Hawkins as Neros Girl
* Candace Glendenning as Stone Girl

==Reception==
The film was the 10th most popular movie at the British box office in 1971. 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 

 
 