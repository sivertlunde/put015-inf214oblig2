Il viale della speranza
 
{{Infobox film
| name           = Il viale della speranza
| image          = Il viale della speranza poster.jpg
| caption        = DVD cover
| director       = Dino Risi
| producer       = 
| writer         = Franco Cannarosso Gino De Santis Ettore Maria Margadonna Dino Risi
| starring       = Silvio Bagolini
| music          = Mario Nascimbene
| cinematography = Mario Bava
| editing        = Eraldo Da Roma
| distributor    = 
| released       =  
| runtime        = 100 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}

Il viale della speranza is a 1953 Italian drama film directed by Dino Risi.   

==Cast==
* Silvio Bagolini
* Arrigo Basevi
* Nerio Bernardi - Franci
* Liliana Bonfatti - Giuditta
* Giulio Calì
* Maria-Pia Casilio - Concettine
* Pietro De Vico - Tonio
* Alessandro Fersen
* Bianca Maria Fusari
* Cosetta Greco - Luisa
* Terence Hill (as Mario Girotti)
* Carlo Hinterman
* Ettore Jannetti
* Clara Loi
* Achille Majeroni
* Nino Marchetti
* Marcello Mastroianni - Mario
* Franco Migliacci
* Vincenzo Milazzo
* Gisella Monaldi - Titina
* Corrado Pani
* Mario Raffi
* Piera Simoni - Franca
* Cesare Vieri

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 