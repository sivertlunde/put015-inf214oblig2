The Healing (film)
{{Infobox film
| image          = The_Healing_(film).jpg
| caption        = Theatrical movie poster
| name           = The Healing
| director       = Chito S. Roño 
| producer       = John Paul Abellera Lea A. Calmerin Tess V. Fuentes Malou N. Santos Charo Santos-Concio 
| music          = Jerrold Tarog
| cinematography = Charlie Peralta
| screenplay     = Roy Iglesias
| writer         = Chito Roño Roy Iglesias
| editing        = Jerrold Tarog	
| starring       = Vilma Santos Kim Chiu
| distributor    = Star Cinema
| released       =  
| country        = Philippines
| language       =  
| runtime        = 
| budget         =
| gross          = P 104,602,460 
}} horror thriller thriller directed by Chito S. Roño, starring Vilma Santos and Kim Chiu. The film was released nationwide on July 25, 2012 and is a massive hit. The film is also part of the celebration of Santos 50 years in showbusiness.

==Plot==
After her father (Robert Arevalo) suffers from a severe stroke, Seth (Vilma Santos) decides to bring him to Manang Elsa (Daria Ramirez), a faith healer. Elsa, who apparently has the ability to heal people from illnesses, performs the "healing" and prescribes herbal medicine. As Seth and her father prepare to leave, a man, who is next in line to see Elsa, collapses and loses consciousness and so the people immediately brings him inside. Seth and her father head anyway.

The following day, Seth is welcomed by her father, who has now regained his strength thanks to the healing. His recovery and miraculous healing astounds their whole neighborhood. Amazed by his grandfathers new-found health, Jed (Martin del Rosario), Seths perturbed son, asks his mother for help for his half-sister Cookie (Kim Chiu), who is suffering from glomerulonephritis so that he could take her to Manang Elsa. Seth refuses because she does not want to be held accountable by her ex-husband Val (Mark Gil) and his wife Bles (Carmi Martin) in case anything goes wrong. Jed pleads very intensely and Seth eventually agrees to give them the address under the condition that they do not tell anyone that she gave it to them.

One evening, Seth arrives home and sees that theres a party. Shes extremely disappointed to see that her father, who has much improved health, has gone back to his old drinking and even "womanizing" habits. Seth decides to veer her attention away from that and started talking to the guests at the party. She finds out that they also want to be healed. Seths housekeeper Alma ( ), suffers from goiter and Greta (Ynez Veneracion) has found a lump in her breast. Cacai (Abby Bautista), the young daughter of Seths close friend Cita (Janice de Belen), is blind. 

Eventually, the group - along with Gretas husband Ruben (Allan Paule) and Chonas husband Rex (Simon Ibarra), go to seek Manang Elsas help but Melchor (Joel Torre), her brother, informs them that Manang Elsa is sick and refuses to see anyone. Due to Seths pleading, Manang Elsa eventually cures all of them. On the way home, Seth crosses paths with Jed and Cookie, who are now secretly going to the faith healer.

The following day, everyone except Cookie is healed. She is taken to a hospital due to a high fever. Seth is then confronted by her ex-husband and grows angry. Days pass and Cookie is finally healed. Seth then starts to notice the behavioral changes of those who have been healed, noticing that they have become passive and quiet. One night, everyone is terrified after Chona, their neighbor who had goiter, is found dead after slashing her own neck with a shard of knife after stabbing a man passing by. At her wake, Rex tells Seth that Chona had seen a crow in her dream before she died. The batch of people who were healed speak out about having had the same dream. Seth saw Dodi (Chinggoy Alonzo), a man they met at Manang Elsas house, walk past her.

The following day, during a breaking news bulletin, they found out that Dodi murdered men at the gym and held his partner hostage. Dodi, looking insane, kills his partner and himself with a piece of broken glass. Seth and her neighbors discussed if theres a pattern of deaths. Ruben annoys his wife Greta that shes going to die next, much to Gretas anger.
 Demonized their images of the people who were healed before their deaths. Seth is confused as to why her father has not suffered the same fate. She asks him if had seen a crow in his dreams, but instead tells her he had a wet dream, which annoys her. Seth, along with Cita, decides to confront Manang Elsa about the chain of deaths, only to be informed by Melchor, that she, along with his wife, had been killed by Dario, the man who much earlier, was thought to have only fainted but who had actually died from a heart attack. Melchor reveals the curses real dark history that Manang Elsa unknowingly "cured" the dead, and now the life that was used to revitalize him was the lives of the following  murdered patients that Manang Elsa "cured" after him. Melchor tells her that the best way to destroy the curse is to Eliminate Dario.

Seth sets out to warn the others. While stuck in traffic, she sees Almas doppelganger walk down the street towards Almas dormitory. Seth calls Alma to warn her that her doppelganger is on its way to possess her. When Alma refuses to believe her, Seth sneaks into her apartment to find her but is too late. Alma, who has already been possessed by her doppelganger, murders her dorm mates by throwing them off the balcony and sets herself on fire. Afraid for her daughters life, Cita brings Cacai to a Chinese temple to be guarded by their relative monks. Inevitably, Seth sees Cacais image and warns Cita. However, Cita is distracted by a dragon dance and loses Cacai who is then controlled by her doppelganger. Cacai massacres the praying monks and kills herself by jumping and getting impaled on a flagpole, much to Citas misery. Seth is haunted by the spirits of her dead friends, who blame her for their deaths because Seth is the key to the healer. 
 possessed by their own alter-egos and their souls shall be offered to him. He then tells her that the best way to end the bloodshed and the continuous spread of the curse is to kill him. Seth discards the plan of poisoning him as she cannot kill anyone. When she returns to the safe-house where Cookie is hiding, she is attacked by the evil image of Cookie who has yet to possess the real one, and manages to get rid of her by electrocuting her with a toaster. Thinking that she has killed Cookies doppelganger, Seth lets Cookie back into her house. 

After a few days, Cookie is chased by her doppelganger and possessed her at her home. Jed found this out because of the CCTV camera showing Cookies doppelganger chasing her. At her fathers birthday party, Seth is visited by Cookie, who is already possessed. Seth is violently attacked and finds Boni, her houseboy, dead with his neck twisted by Cookie. Jed arrives and stops the possessed Cookie from killing Seth.

As the fight rages, Cookies doppelganger touches Jeds head, creating another doppelganger in Jeds image who then tries to stab Seth. At the prison, Dario is anticipating the new life that will be transported to him, but Melchor shows up at the prison and shoots him dead. A crow falls from the sky hitting the camera, showing the curse has ended. The doppelgangers disappear, the bloodshed ceases, and Melchor is pinned down by police officers. Seth and Jed who are now free from the curses reign of terror shake the unconscious Cookie, who wakes up surprisingly for one final scare.

==Cast==
*Vilma Santos as Seth
*Kim Chiu as Cookie
*Janice de Belen as Cita
*Pokwang as Alma
*Robert Arevalo as Odong
*Martin del Rosario as Jed
*Mark Gil as Val
*Carmi Martin as Bles
*Cris Villanueva as Ding
*Allan Paule as Ruben
*Ynez Veneracion as Greta
*Ces Quesada as Chona
*Abby Bautista as Cacai
*Daria Ramirez as Manang Elsa
*Chinggoy Alonzo as Gay Hostage Taker
*Simon Ibarra as Rex
*Mon Confiado as Gay Lover
*Cris Pasturan as Boni
*Nikki Valdez as Lani
*Joel Torre as Melchor
*Jhong Hilario as Dario Mata
*Ina Feleo as Mrs. Mata
*Mercedes Cabral as Kell
*Ana Capri as Melchors Wife

==Release==

===International screening===
Here are some selected international screening dates and venues of The Healing:   
 
{| class="wikitable"
|-
! Date !! Year !! Mall !! Location
|-
| July 27 - August 2  || rowspan=13| 2012 || Micronesia Mall || Guam
|-
| August 2 – 9  || Paradise Mall || Papua New Guinea
|-
| rowspan=6| August 10 – 16 || Pearlridge West Theatres  || Hawaii
|- Las Vegas
|- 
| UA Horton Plaza || San Diego
|-
| Parkway Plaza Stadium 12 || Seattle
|-
| Clearview Bergenfield Cinemas || New Jersey
|-
| Cinemark Tinseltown || Houston
|-
| rowspan=4| August 16–23 || Al Ghuarair || Dubai
|-
| Al Mariah Mall || Abu Dhabi
|-
| CineCenter || Qatar
|-
| Seef Cineplex || Bahrain
|-
| August 17–24 || Station Square Cineplex || Vancouver
|}

==Critical reception==

===Box office performance===
According to Box Office Mojo, the film grossed 74 Million Pesos on its first two weeks.   The film grossed P104.6 million. It became the third Filipino film of 2012 to gross over 100 million.

==Ratings==
The Cinema Evaluation Board of the Philippines gave this film a "Graded A", meaning the film will not pay taxes from the gross revenue of the film earned from the box office.

Star Cinema released 2 versions of the film, one is the censored, wider release, which is rated R-13 while the other one is directors cut, limited release, which is rated R-18.    

==See also==
*List of ghost films

== References ==
 
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 