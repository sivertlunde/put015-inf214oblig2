The Blood of Heroes
{{Infobox film
| name           = The Blood of Heroes
| image          = Bloodofheroes.jpg
| caption        = Theatrical release poster
| director       = David Webb Peoples
| producer       =
| writer         = David Webb Peoples
| starring       = Rutger Hauer Joan Chen Delroy Lindo
| music          = Todd Boekelheide
| cinematography = David Eggby
| editing        = Richard Francis-Bruce
| studio         = Kings Road Entertainment
| distributor    = New Line Cinema
| released       = October 19, 1989 (Australia) February 23, 1990 (USA)
| runtime (US)   = 90 minutes
| runtime (UK)   = 99 minutes
| country        = USA
| language       = English
| budget         = A$10,000,000 
| gross          = $882,290
}}

The Blood of Heroes is a 1989 post-apocalyptic film directed by David Webb Peoples and starring Rutger Hauer and Joan Chen.  The film is also known by the names The Salute of the Jugger and Salute to the Jugger.  The film has inspired the creation of the sport Jugger. It has also found its way into amtgard, a LARP which has been playing the game for almost 20 years.

==Plot==
Produced by Charles Roven and released by Kings Road Entertainment, the film is set in a post-apocalyptic future, where the wars waged in the 20th century have left the world barren and the past forgotten. Most live from hand to mouth in enclaves known as "market-towns" or "dog-towns", scrounging out a bare subsistence harvesting hardy crops, raising dogs as food, and trading in trinkets from the past.
 The Game. It is played by bands of roving teams known as juggs, who challenge local teams.  They might be considered professional athletes, as they make their living through the tribute paid by the town people, should they defeat the local team. Their trophy is the dog skull from the town. The Game involves two armoured teams of five attempting to score by placing a dog skull on the opposing teams goalpost. One unarmed player—the "quick"—runs with the skull while being protected by his/her teammates from attack by the opposing team.

However, not all in this time live so sparsely.  The Nine Cities, buried deep underground, are home to affluent and powerful members of the aristocracy.  Each of The Nine Cities fields its own team of juggs in an organization known as The League, and its membership is maintained with a fresh stream of new players who are proven veterans of the travelling "dog-town" games by their collection of trophy skulls.

Members of The League live in luxury almost equal to that of aristocrats.  It is a dream among roving juggs to be good enough to get The Leagues attention and, with it, all of the luxuries afforded a League player.

The team consists of Sallow (Rutger Hauer), Dog-Boy (Justin Monjo), Mbulu (Delroy Lindo), Big Cimber (Anna Katarina), and Young Gar (Vincent DOnofrio).

Sallow, the team leader, has played in the League of the Nine Cities before, but was cast out because of his indiscretions with an Overlords concubine. Kidda (Joan Chen), an ambitious peasant girl, joins the team after a game in her dog town where she virtually destroyed her competition. She and Gar inspire Sallow to challenge The League and expunge his past.

But Kidda and Gar do not realise that the City games are for much more than honour and victory, they will need to fight for their very survival.  The Game is played much harder and meaner in the Nine Cities.

==Alternate cuts==
The original US theatrical version of the film was significantly shorter than the original version released overseas. About ten minutes were cut. The biggest difference is in the ending. In the American release, the credits roll shortly after the climax, even though picture continues to roll, showing certain conversations with music covering the dialogue. In the longer cut, there are denouement scenes.

The US DVD release of the film matches the US theatrical cut in being much shorter than the original film. Full versions were released on VHS in the early 1990s in Australia, the United Kingdom, Europe and Japan. Various distributors began releasing the original cut on DVD in early 2001, known as the extended version.

==Location==
The Blood of Heroes was shot in the desert of Coober Pedy, Australia.

==See also==
* Jugger
* Quintet (film)

==References==
 

==External links==
*  
*  
*  
*   on YouTube

 

 
 
 
 
 
 
 
 
 
 
 
 