Sure Fire
 
 
{{Infobox film
| name           = Sure Fire
| image          = Sure Fire 1921.jpg
| caption        =
| director       = John Ford
| producer       =
| writer         = George C. Hull Eugene Manlove Rhodes
| starring       = Hoot Gibson
| music          =
| cinematography = Virgil Miller
| editing        = Universal Film Manufacturing Company
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = Silent (English intertitles)
| budget         =
}}
 Western film directed by John Ford and featuring Hoot Gibson.  The film is considered to be lost film|lost.   

==Cast==
* Hoot Gibson as Jeff Bransford Molly Malone as Marian Hoffman
* B. Reeves Eason Jr. as Sonny (credited as Breezy Eason Jr.) Harry Carter as Rufus Coulter
* Fritzi Brunette as Elinor Parker
* Murdock MacQuarrie as Major Parker George Fisher as Burt Rawlings Charles Newton as Leo Ballinger
* Jack Woods as Brazos Bart
* Jack Walters as Overland Kid
* Joe Harris as Romero
* Steve Clemente as Gomez (credited as Steve Clements)
* Mary Philbin

==See also==
* Hoot Gibson filmography

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 


 
 