You're Fired (film)
{{infobox film
| name           = Youre Fired
| image          = Yourefired 1919 newspaperad.jpg
| imagesize      =
| caption        = Newspaper advertisement.
| director       = James Cruze
| producer       = Adolph Zukor Jesse L. Lasky
| based on       =  
| writer         = Clara G. Kennedy (scenario) Wilfred Buckland (intertitles)
| starring       = Wallace Reid Wanda Hawley
| music          =
| cinematography = Frank Urson
| editing        =
| distributor    = Paramount Pictures
| released       =   reels (4,183 feet) (Approx. 50 minutes)
| country        = United States Silent (English intertitles)
}}
Youre Fired (1919) is a silent film comedy, produced by Famous Players-Lasky and distributed by Paramount Pictures. James Cruze directed and Wallace Reid starred. 

==Plot== stenographer he resigns at the end of his first day to avoid being fired. Job number two is at a restaurant where he is required to wear the garb of an ancient warrior known to all readers of historical novels as a halberdier, and then pose as a statue on the landing of the stairs. To the restaurant comes fair Helen, her father Gordon, and Tom (Woodward), a young gentleman willing to do anything short of murder the sake of the young lady and her golden prospects. Old Gordon has arranged a merger of a stray railroad he owns with another company, and is fighting Toms uncle, an unscrupulous financier who has promised his nephew a supply of ready cash if he can obtain the papers for the deal. Tom known that the papers are in a safe at the Old Rogers home, and hires two experts to open the safe and get the papers. All of these people are meeting at the restaurant. Helen catches sight of Billy in his ancient garb and recognizes him. She tries to find out why he is so dressed, but Bill is sworn to secrecy and dare not tell her. To show her anger she insists that he wait on her party, and is almost fired when he spills soup on her gown. Previously Billy had worked as a xylophone player at a dance where Helen was a guest, and hid behind a false mustache. Her great anger when he would not do as she demanded shows her true love for him. Billy manages to stick out the thirty days without being fired and also obtains the merger papers stolen from the safe, and returns them to Gordon, who hands over his daughter at once.

==Cast==
*Wallace Reid - Billy Deering Jr.
*Wanda Hawley - Helen Rogers
*Henry Woodward - Tom
*Theodore Roberts - Gordon Rogers
*Lillian Mason - Mrs. Oglethorpe
*Herbert Pryor - Horace Graham
*Raymond Hatton - orchestra leader
*William Lesta - restaurant proprietor

==Preservation status==
Youre Fired was considered to be a lost film for decades until a copy was repatriated from the Gosfilmofond film archive in Russia to the United States in 2010. 

==See also==
* List of rediscovered films

==References==
 

==External links==
 
* 
* 

 

 
 
 
 
 
 
 