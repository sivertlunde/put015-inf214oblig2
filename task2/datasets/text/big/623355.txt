Tieta
 
{{infobox book |  
| name          = Tieta
| title_orig    = Tieta do agreste
| translator    = Barbara Shelby
| image         = File:Tieta Cover1.jpg

| caption = 
| author        = Jorge Amado
| illustrator   = 
| cover_artist  = 
| country       = Brazil Portuguese
| series        = 
| genre         = 
| publisher     = Distribuidora Record
| release_date  = 1977
| english_release_date = 1979
| media_type    = 
| pages         = 
| isbn          = 0-380-75477-0
| preceded_by   = 
| followed_by   = 
}}

Tieta (  written by the Brazilian author Jorge Amado, published on August 17, 1977. Set in the 1970s, it narrates the return of Tieta to the remote village of Santana do Agreste, 26 years after being beaten and expelled by her father in front of all the towns people.

In 1996, a film version was made with Sonia Braga in the role of Tieta.

==Background==
Written mainly in Buraquinho Beach, Lauro de Freitas, close to Salvador, Bahia, Brazil|Salvador, Brazil, and concluded in London in mid-1977, Tieta lives up to its full title: "Tieta the Goat Girl or The Return of The Prodigal Daughter, A Melodramatic Serial Novel in Five Sensational Episodes with a Touching Epilogue: Thrills and Suspense!"  The book is one of the authors longest narratives and follows the storys protagonist through three decades.

In the work of Jorge Amado, the novel falls between those books that, in novelistic tone, chronicle the times and customs, without losing sight of social and political criticism. Published during the period of Brazils military regime, the plot anticipates issues that would become central in the life of the country, such as concern for the environment and criticism of power relations guided by favoritism and corruption. With her impetuosity and questioning spirit, Tieta joined the gallery of the authors great female characters, alongside  .

==Plot introduction==
Banished for promiscuity at the age of 17, Antonieta (Tieta) returns from São Paulo to her native village of Agreste in Bahia twenty-six years later. Thinking she is now a rich, respectable widow, her family and the village welcome her with open arms. But she is forced to reveal her true identity, as the Madam of São Paulo’s best brothel, in order to save Agreste’s beaches from an ugly and polluting factory development by calling on assistance from her well-connected clients.

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 


 