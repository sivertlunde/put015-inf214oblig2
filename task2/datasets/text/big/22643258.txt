Oorlog en vrede
 
{{Infobox film
| name           = Oorlog en vrede
| image          = 
| image_size     = 
| caption        = 
| director       = Maurits Binger
| producer       = 
| writer         = Maurits Binger Adelqui Migliar
| narrator       = 
| starring       = 
| music          =   
| cinematography = Feiko Noersma
| editing        = 
| distributor    = 
| released       = 25 October 1918
| runtime        = 
| country        = Netherlands
| language       = Silent
| budget         = 
| amg_id         = 
}} 1918 Netherlands|Dutch silent war war film drama directed by Maurits Binger. It is named after the novel War and Peace by Leo Tolstoy, but is not an adaptation of it.

==Cast==
* Annie Bos - Any Godard
* Adelqui Migliar - Jean Laurent / Mario Laurent
* Paula de Waart - Pauline Laurent
* Lola Cornero - Ninette Laurent
* Caroline van Dommelen - Nora de Roqueville
* Jan van Dommelen - Robert de Roqueville
* Willem van der Veer - Gaston de Roqueville
* Minny Erfmann - Stella Marie
* Eberhard Erfmann - Frits
* Jan Buderman - Vader Godard
* Catharina Kinsbergen-Rentmeester - Moeder Godard
* Cor Smits - Hofman
* Heléne Wehman - Rosa Hofman
* Mimi Boesnach
* Jeanne Van der Pers

== External links ==
*  

 
 
 
 
 
 
 


 