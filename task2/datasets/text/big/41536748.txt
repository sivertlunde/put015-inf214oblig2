Jesus – The Film
{{Infobox film
| name           = Jesus – The Film
| image          = Jesus_der_film_poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Michael Brynntrup with episodes by 
Anarchistische GummiZelle, Jörg Buttgereit, Die Tödliche Doris, Frontkino / Konrad Kaufmann, Birgit Hein and Wilhelm Hein, intershop gemeinschaft wiggert, Almut Iser, Dietrich Kuhlbrodt, Georg Ladanyi, Merve Verlag, Giovanni Mimmo, padeluun, Robert Paris and Andreas Hentschel, Schmelzdahin, Stiletto artist|Stiletto, Sputnik Kino / Michael Wehmeyer, Teufelsberg Produktion, Lisan Tibodo, VEB Brigade Zeitgewinn, Werkstattkino München / Doris Kuhn, Andreas Wildfang
| producer       = Michael Brynntrup
| screenplay     = Michael Brynntrup Stiletto Uli Versum Oliver Körner von Gustorff Bertram Jesdinsky Ulrich Sappok Heidi Paris Peter Gente
| music          = padeluun et al.
| cinematography = Wolfgang Böhrer, Michael Brynntrup, Jörg Buttgereit et al.
| editing        = Michael Brynntrup et al.
| distributor    = Stiftung Deutsche Kinemathek
| released       =  
| runtime        = 127 minutes
| country        = Germany
| language       = German
}} Super 8 contains 35 episodes made by 22 filmmakers and collectives from East and West Germany. Each episode is a very loose interpretation of the stories, parables and stations of the life of Jesus.

==Plot== Mary now giving birth to twins, likely to give credence to the idea that Gnostic The Gospel of Thomas was actually written by the twin brother of Jesus. However, in keeping with tradition, only the life and suffering of the One is portrayed. Throughout the entire film, Jesus is shadowed by dark agents (the Four Evangelists) who seem to be the puppet masters of the story. But this preordained life spins out of control at the Last Supper, where Jesus develops a fondness for drinking blood. On his way to Calvary, he meets Saint Veronica and surrenders to this thirst, thereby acquiring immortality as a vampire. Jesus lives.

==Production==
The film’s storyline, as well as the creative process of each episode, is inspired by the  , the initiator and coordinator of the overall project, in the role of Jesus.
 Super 8 film stock. These Super 8 cassettes were smuggled out of East Germany, often under precarious conditions, and then distributed to the participating filmmakers. Due to financial constraints, much of the film processing was done by the filmmakers themselves. This also contributed to the film’s look, recalling the expressionist classics of the silent movie era.
 Deutsche Kinemathek DCP (Digital Cinema Package) format.

==Reception==
The film premiered at the 1986 Berlinale, as part of the International Forum of New Cinema, before going on a “Mission Tour” through 40 locations across West Germany that same year. Then in 1987, the film went on a “North America Mission”, visiting 10 cities across the USA and Canada with the support of local Goethe Institutes.

The film won the top prize at the 1986 Salzgitter Film Festival and the prize for best production at the 1987 Caracas International Super 8 Festival. The film’s 11-minute trailer (entitled Veronika – Vera Ikon  ) received an honorable mention at the 1988 Friedberg International Festival of Religious Films and at the 1988 Tel Aviv International Student Film Festival.

In 2014 Jesus – the Film – the Book was published.  The book is a bilingual (German/English) compilation of materials documenting the three years of the Jesus Film project, ranging from the initial ideas and research in 1984, through facsimiles of the Jesus Letters that were sent to participating filmmakers, a detailed project diary with notes on the dynamics of group work, additional production materials along with images of the shoot and of props that attained the status of relics, to facsimiles of program flyers from the months-long “Mission Tours” through West Germany (1986) and North America (1987). The book thus stands as an independent work of its own.

* "Jesus: Der Film is silly, sublime." (Chicago Sun-Times, Peter Keough) Andy Warhol’s Interview) 
* "This film is the largest collective project in German film history. In the history of world cinema there are few works that can compare." (Jesus – Der Film – Das Buch, Randall Halle) 

==Cast==
* Michael Brynntrup as Jesus
* Panterah Countess as Mary
* Jürgen Brauch as Satan and Joseph
* Hermoine Zittlau as Caiaphas
* Fabian Saladin Prinz von Hessen-Nassau as Pontius Pilate Stiletto as Saint Peter
* Uli Versum as John the Apostle
* Oliver Körner von Gustorff as Judas Iscariot
* Bertram Jesdinsky as John the Baptist
* Ulrich Sappok as the head of John the Baptist
* Heidi Paris as Angel
* Peter Gente as John of the Apocalypse

==External links==
*  
*  
*  , Verlag Vorwerk8, Berlin 2014, ISBN 978-3-940384-58-4

==References==
 

 
 
 
 
 
 
 
 
 
 