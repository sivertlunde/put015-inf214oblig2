International Khiladi
 
 
{{Infobox film
| name           = International Khiladi
| image          = International_khiladi.jpg
| caption        = First Look of International Khiladi
| presented by   = Time Media & Entertainment Pvt. Ltd.
| director       = Umesh Mehra
| producer       = Keshu Ramsay
| writer         = Umesh Mehra (Dialogues)
| story          = Ved Prakash Sharma
| screenplay     = Ved Prakash Sharma
| starring       = Akshay Kumar Twinkle Khanna 
| music          = Aadesh Shrivastava
| cinematography = S. Pappu
| editing        = Kamal Saigal
| distributor    = Eros Entertainment
| released       =  
| runtime        = 180 min.
| country        = India Hindi 
| budget         = 
| gross          = 
}}

International Khiladi ( ; English: International Player) is a Hindi action thriller film released in 1999. The film is directed by Umesh Mehra starring Akshay Kumar and Twinkle Khanna in the lead roles.   It was the sixth instalment in the Khiladi (film series).

== Plot ==
News Reporter Payal (Twinkle Khanna) and her camera-man have been assigned the task of interviewing the worlds highest ranking criminal don, Devraj (Akshay Kumar), which they accept. In the process, Payal and Devraj fall in love with each other,much to the opposition of Bismillah (Mukesh Khanna), Devrajs guardian on one hand; and Police Inspector Amit (Rajat Bedi), and Payals brother Ravi (Vivek Shauq) on the other. What results is that Ravi is killed with Devraj being blamed and arrested. Payal tesitifies against him, and Devraj is sentenced to be hanged. When Bismillah finds about this plot, he tracks them down, but Amit kills him to save himself and Payal. But Devraj escapes from custody, and begins to plot vengeance against his enemies, including Payal, as well as the real killer of Ravi. As the story unfolds, it is revealed that Amit was the one who killed Ravi and joined Thakral to frame Devraj for it. But Amit betrays Thakral and kills him. He then takes Payal on a flight where Devraj tracks him down and kills him. The story ends with Payal and Devraj getting married.

== Cast ==
* Akshay Kumar  as  Rahul Devraj
* Twinkle Khanna  as  Payal
* Rajat Bedi  as  Amit
* Vivek Shauq  as  Ravi
* Gulshan Grover  as  Thakral
* Mukesh Khanna  as  Bismillah, Devrajs guardian
* Johnny Lever  as  Focus, Payals Cameraman
* Asrani  as  Payals boss
* Avtar Gill  as  Defence Lawyer
* Subbi Raj  as  S.P. Sharma 
* Ram Mohan  as  Prosecuting Lawyer
* Vivek Vaswani  as  The Director
* Gajendra Chouhan  as  Rahuls Father
* Gurpreet Ghuggi  as  Raj

==Soundtrack==
{{Track listing
| headline       = Songs
| extra_column   = Playback
| all_lyrics     = Dev Kohli
| all_music      = Aadesh Shrivastava

| title1 = Chookar Mere Man Ko
| extra1 = Kumar Sanu, Alka Yagnik
| length1 = 6:06

| title2 = Halka Halka Dard Hai
| extra2 = Kumar Sanu, Kavita Krishnamurthy
| length2 = 5:07

| title3 = International Khiladi
| extra3 = Sonu Nigam
| length3 = 4:11

| title4 = Kudi Kanwari 	
| extra4 = Alka Yagnik, Roop Kumar Rathod, Johnny Lever, Bela Sulakhe
| length4 = 6:31

| title5 = Lutiya Gaya
| extra5 = Sonu Nigam, Jaspinder Narula
| length5 = 4:24

| title6 = Saiyan Saiyan
| extra6 = Abhijeet Bhattacharya|Abhijeet, Alka Yagnik
| length6 = 3:30

| title7 = Yaar Maine Ek Sapna Dekha 	
| extra7 = Udit Narayan, Alka Yagnik
| length7 = 5:20
}}

==References==
 

==External links==
*  

 

 
 
 
 