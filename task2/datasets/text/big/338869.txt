The Emperor's New Groove
 
{{Infobox film
| name        = The Emperors New Groove
| image       = Grooveposter.jpg
| caption     = Original theatrical preview poster
| director    = Mark Dindal
| producer    = Randy Fullmer Don Hahn  
| screenplay  = David Reynolds (screenwriter) | David Reynolds Chris Williams
| narrator    = David Spade
| starring    = David Spade John Goodman Eartha Kitt Patrick Warburton
| music       = John Debney Marc Bonilla  
| editing     = Tom Finan Pam Ziegenhagen Walt Disney Feature Animation Buena Vista Pictures
| released    =  
| runtime     = 78 minutes
| country     = United States
| language    = English
| budget      = $100 million   
| gross      = $169.3 million 
}} buddy comedy film created by Walt Disney Feature Animation, the 40th film in List of Disney theatrical animated features|Disneys animated features canon. It was directed by Mark Dindal, produced by Randy Fullmer, and written by David Reynolds, and stars David Spade, John Goodman, Eartha Kitt, Patrick Warburton, and Wendie Malick. The films title refers to the Danish fairy tale The Emperors New Clothes by Hans Christian Andersen, though the two have little else in common.

The Emperors New Groove was altered significantly over its six years of development and production. It began as a musical epic titled Kingdom of the Sun, to have been directed by Dindal and Roger Allers (co-director of The Lion King), and was changed by Disney executives into a light-hearted buddy comedy. The documentary The Sweatbox details the production troubles that the film endured.
 Wonder Boys. A direct-to-video sequel, Kronks New Groove, was released in 2005, followed by an animated television series, The Emperors New School, in 2006.

==Plot== Kronk plot to usurp the throne. Meanwhile, Kuzco meets with List of The Emperors New Groove characters#Pacha|Pacha, a kindly peasant of a man and tells him that he wants to demolish his family home to build himself a lavish summer home called "Kuzcotopia". Pacha protests, but is dismissed.

Yzma and Kronk attempt to poison Kuzco, but give him the wrong potion and accidentally turn him into a llama. Kronk is ordered to dispose of Kuzcos unconscious emperors body, but has a stroke of conscience and saves him, before accidentally losing him the back of Pachas cart. Pacha returns home but does not tell his pregnant wife or children about the emperors decision. After awakening in the cart, Kuzco orders Pacha to take him back to the palace, but Pacha will only do so if Kuzco agrees to spare his familys home. Kuzco haughtily sets off into the jungle alone but is nearly killed before Pacha rescues him, and must eventually agree to the bargain.

Meanwhile, Yzma has taken the throne when Kronk admits that he lost Kuzco. The two set off to find him and finish the job. Pacha and Kuzco are almost back to the palace when Pacha falls through a bridge and Kuzco refuses to help him up, admitting he never meant to keep his promise. However, he soon finds himself in danger too, and they work together to save both their lives. Without the bridge their journey is delayed, giving Pacha hope that Kuzco will change his mind. Kronk and Yzma stop at a jungle restaurant at the same time Kuzco and Pacha are there disguised as a married couple. Neither party realizes the other is there until Pacha overhears Yzma and Kronk talking about trying to kill Kuzco. He tries to warn Kuzco, but Kuzco does not believe him and the two separate angrily. Kuzco then overhears Yzmas plot to kill him, but his pride prevents him from returning to Pacha.

The movie then returns to the opening scene, with Kuzco lost in the jungle alone. However, he is soon reunited with Pacha, and they enlist the help of Pachas family to keep Yzma and Kronk occupied while they escape. The race to the palace seems to end with Yzma and Kronk falling off a cliff, but they still inexplicably reach the palace first. Yzma orders Kronk to kill Pacha and Kuzco, but he gets into a conversation with his shoulder angel and devil, leading Yzma to insult Kronks cooking and resigning to do it herself. Yzma calls the guards who do not recognize Kuzco and attack the two of them, while Pacha and Kuzco take all the potions they can carry in hopes that one will turn Kuzco back.

After several guards are transformed into animals while testing potions and Yzma is transformed into a kitten, Pacha and Kuzco work together to try and get the last vial. Yzma snatches it at the last moment, but is unintentionally foiled by Kronk. Now a human again, Kuzco decides to build his summer home elsewhere, and Pacha suggests a neighboring hilltop. In the end, Kuzco is shown living next door to Pachas family in a modest cabin. Yzma, still a kitten, joins Kronks Junior Chipmunk troop.

==Cast and characters==
  Emperor Kuzco, the vain, 17-year-old (almost 18-year-old) emperor of the Incan Empire.
* John Goodman as Pacha (The Emperors New Groove)|Pacha, a portly peasant who serves as a foil for Kuzco.
* Eartha Kitt as Yzma, Kuzcos advisor who wants to get rid of Kuzco so she can take over.
* Patrick Warburton as Kronk Pepikrankenitz, Yzmas dim-witted right-hand man.
* Wendie Malick as List of The Emperors New Groove characters#Chicha|Chicha, Pachas wife.
* Kellyann Kelso and Eli Russell Linnetz as Chaca and Tipo, Pachas children.
* Bob Bergen as Bucky the Squirrel, Kronks companion who dislikes Yzma. Tom Jones as the Theme Song Guy, Kuzcos personal theme song conductor.
* Patti Deutsch as a waitress. the sequel his name is revealed to be Rudy.
* Joe Whyte as an official in charge of finding Kuzco a bride.
* Frank Welker (uncredited) as Black panthers, Fly, Cat, Llamas and Bees.

Additional voices include Andre Stojka, Jess Harnell and Sherry Lynn.

==Production==

===Kingdom of the Sun===
 

The idea of Kingdom of the Sun was conceived by Roger Allers and Matthew Jacobs,    and development on the project began in 1994.     Upon pitching the project to then-Disney CEO and chairman Michael Eisner, Allers recalled Eisner saying "it has all of the elements of a classic Disney film",    and because of his directorial success on The Lion King that same year, Eisner allowed Allers to have free rein with both the casting and the storyline.    By January 1995, Variety (magazine)|Variety reported that Allers was working on "an Incan-themed original story." 

In 1996, the production crew traveled to Machu Picchu in Peru, to study Incan artifacts and architecture and the landscape this empire was created in.  
 archetypal novel Supai and capture the sun so that she may retain her youth forever (the sun gives her wrinkles, so she surmises that living in a world of darkness would prevent her from wrinkling). Discovering the switch between the prince and the peasant, Yzma turns the real emperor into a llama and threatens to reveal the paupers identity unless he obeys her. The emperor-llama learns humility in his new form, and even comes to love a female llama-herder named Mata (voiced by Laura Prepon).  Together, the girl and the llama set out to undo the witchs plans. The book Reel Views 2 says the film would have been a "romantic comedy musical in the traditional Disney style".   
 The Hunchback David Hartley, Sting had composed eight songs inextricably linked with the original plot and characters.  This film, which was eventually entitled The Sweatbox, was made by Xingu Films (their own production company).

In summer 1997, it was announced that Roger Allers and Mark Dindal would serve as the films directors and Randy Fullmer as producer. David Spade and Eartha Kitt had been confirmed to voice the emperor, Manco, and the villainess, while Carla Gugino was in talks for a role.   Harvey Fierstein was also cast as Hucua, Yzmas sidekick. 

By the summer of 1998, it was apparent that Kingdom of the Sun was not far along enough in production to be released in the summer of 2000 as planned. At this time, one of the Disney executives stormed into Randy Fullmers office and, placing his thumb and forefinger a quarter-inch apart, angrily remarked that "your film is this close to being shut down".    Fullmer approached Allers, and informed him of the need to finish the film on time for its summer 2000 release as crucial promotional deals with McDonalds, Coca-Cola, and other companies were already established and depended upon meeting that release date. Allers acknowledged that the production was falling behind, but was confident that, with an extension of between six months to a year, he could complete the film. When Fullmer denied Allerss request for an extension, the director decided to leave the project.  On September 23, 1998,   the project was dead with production costs amounting towards $25–30 million   and twenty-five percent of the film animated. 

===Production overhaul and script rewrite=== Eric Goldbergs Chris Williams, who was a storyboard artist during Kingdom of the Sun,  came up with the idea of making Pacha an older character as opposed to the teenager that he was in the original story.  Following up on the new idea, former late-night comedy writer David Reynolds stated, "I pitched a simple comedy thats basically a buddy road picture with two guys being chased in the style of a Chuck Jones toon, but faster paced. Disney said, Give it a shot. "  One of the new additions to the revised story was the scene-stealing character of Yzmas sidekick Kronk.   Meanwhile, the name Manco was changed to Kuzco following Fullmers discovery of the Japanese slang term omanco, which translates to vagina.   Due in part of the production shutdown, Sting began to develop schedule conflicts with his songwriting duties interfering with his work on his next album he was planning to record in Italy. "I write the music, and then theyre supposed to animate it, but there are constantly changes being made. Its constantly in turnaround," the singer/songwriter admitted, but "Im enjoying it."   Because of the shutdown, the computer-animated film Dinosaur (2000 film)|Dinosaur assumed the summer 2000 release date originally scheduled for Kingdom. 
 soundtrack album such as Yzmas villain song titled "Snuff Out the Light", the love song titled "One Day Shell Love Me", and a dance number called "Walk the Llama Llama."  The plot elements such as the romance between the llama herder Pacha and Mancos betrothed Nina, the sun-capturing villain scheme, similarities to The Prince and the Pauper stories, and Incan mythology were dropped.  The character of Hucua was also dropped from the story, though he would make a cameo appearance as the candle holder during the dinner scene in the finished film. #DVD1|Audio commentary  Kuzco – who was a supporting character in the original story – eventually became the protagonist. 
 Puddy on Tom Jones, who was eleven years older than Sting. 

In February 2000, the new film was announced as The Emperors New Groove with its new story centering around a spoiled Incan Emperor – voiced by David Spade – who through various twists and falls ends up learning the meaning of true happiness from a poor peasant, played by John Goodman. The release date was scheduled for December 2000.  Despite the phrasing of the title, the film bears no relation to Hans Christian Andersens classic fairy tale The Emperors New Clothes.  However, Eisner worried that the new story was too close in tone to Disneys 1997 film Hercules (1997 film)|Hercules, which had performed decently yet below expectations at the American box office. Dindal and Fullmer assured him that The Emperors New Groove, as the film was now called, would have a much smaller cast, making it easier to involve audiences. Towards the end of production, the films ending originally had Kuzco building his Kuzcotopia amusement park on another hill by destroying a rainforest near Pachas home, and inviting Pacha and his family to visit. Horrified at the ending, Sting commented that "I wrote them a letter and said, You do this, Im resigning because this is exactly the opposite of what I stand for. Ive spent 20 years trying to defend the rights of indigenous people and youre just marching over them to build a theme park. I will not be party to this."  The ending was rewritten so that Kuzco constructs a shack similar to Pachas and spends his vacation among the villagers. 

===Design and animation===
During production on Kingdom of the Sun,   being developed concurrently and ultimately the llama characters were dropped from the storyline.   

Following the production overhaul and the studios attempts for more cost-efficient animated features, Mark Dindal urged for "a simpler approach that emphasized the characters rather than overwhelming special effects or cinematic techniques."  Because of the subsequent departure of Deja, animator Dale Baer inherited the character of Yzma. Using Eartha Kitts gestures during recording sessions, Baer commented that "She has a natural voice for animation and really got into the role. She would gesture wildly and it was fun just to watch her. She would come into each session almost serious and very professional and suddenly she would go wild and break up laughing."  Ranieri was later asked to serve as the supervising animator of Kuzco (as a human and a llama), though he would admit being reluctant at first until he discovered that Kuzco "had a side to him, there was a lot of comedy potential and as a character he went through an arc."  Pudleiner was also reassigned to work as an animator of the human version of Kuzco.  In addition to drawing inspiration from David Spade during recording sessions, the Kuzco animation team studied llamas at the zoo, visited a llama farm, watched nature documentaries, and even observed the animals up close when they came for a visit to the studio.  For the rewritten version of Pacha, animator Bruce W. Smith observed that "Pacha is probably the most human of all the characters," and further added that he "has more human mannerisms and realistic traits, which serve as a contrast to the cartoony llama he hangs out with. He is the earthy guy who brings everything back into focus. Being a big fellow about six-foot-five and weighing about 250 pounds we had to work hard to give him a sense of weight and believability in his movement."   
 Walt Disney Disney Animation France assisted in the production of The Emperors New Groove.  During the last eighteen months of production, a 120-crew of Clean-up|clean-up artists would take an animation cel drawing from the animation department, and place a new piece of paper over the existing image in order to draw a cleaner, more refined image. "Were basically the final designers," said clean-up supervisor Vera Pacheco, whose crew worked on more than 200,000 drawings for "Groove." 

==Release ==
After the release date had shifted to a winter 2000 release, similarities were made between the film and DreamWorks Animations The Road to El Dorado.  Marc Lument, a visual development artist on El Dorado, claimed "It really was a race, and Katzenberg wanted ours out before theirs". Lument also added that, "We didnt know exactly what they were doing, but we had the impression it was going to be very similar. Whoever came out second would face the impression that they copied the other."  Fullmer and Dindal denied the similarities with the latter commenting "This version   was well in the works when that movie came out," and further added "Early on, when our movie got to be very comic, all of us felt that you cant be making this farce about a specific group of people unless we are going to poke fun at ourselves. This didnt seem to be a proper choice about Incas or any group of people. It was more of a fable."   

The marketing campaign for The Emperors New Groove was relatively restrained as Disney opted to heavily promote the release of 102 Dalmatians which was released during Thanksgiving (United States)|Thanksgiving.   Nevertheless, the film was accompanied with six toy figurines of Kuzco, Kuzco as a llama, Pacha, Yzma, Yzma as a cat, and Kronk accompanied with Happy Meals at McDonalds.   

===Home media===
The standard VHS and DVD was released May 1, 2001, as well as a "2-Disc Collectors Edition" which included bonus features such as Stings music video of "My Funny Friend and Me", a Rascal Flatts music video of "Walk the Llama Llama" from the soundtrack, audio commentary with the filmmakers, a multi-skill level Set Top Game with voice talent from the movie, and a deleted scene among other features.  Unlike its theatrical box office performance, the film performed better on home video, becoming the top-selling home video release of 2001.  In September 2001, it was reported that 6 million VHS units were sold amounting towards $89 million in revenue. On DVD, it was also reported it had sold twice as many sales. The overall revenue averaged toward $125 million according to Adams Media Research. 

Disney re-released a single-disc special edition called "The New Groove Edition" on October 18, 2005. Disney digitally remastered and released The Emperors New Groove on Blu-ray on June 11, 2013 bundled in a two-movie collection combo pack with its direct-to-video sequel Kronks New Groove.  On its first weekend, it sold 14,000 Blu-ray units grossing $282,000. 

==Reception==

===Critical reaction===
The film received positive reviews from film reviews. On Rotten Tomatoes, it receives an 85% "Certified Fresh" approval rating based on 127 reviews with an average of 7.1/10. Its consensus summarized its reception as not being "the most ambitious animated film, but its brisk pace, fresh characters, and big laughs make for a great time for the whole family."  On Metacritic, the film has a score of 70 out of 100 based on 28 critics, indicating "generally favorable reviews".  Many critics and audiences generally consider the film to be one of the better films of Disneys post-Renaissance era and also one of the most comedic. 

Writing for Variety (magazine)|Variety, Robert Koehler commented the film "may not match the groovy business of many of the studios other kidpix, but it will be remembered as the film that established a new attitude in the halls of Disneys animation unit."  Roger Ebert, writing his review for Chicago Sun-Times, awarded the film 3 (out of 4) stars distinguishing the film as "a goofy slapstick cartoon, with the attention span of Donald Duck" that is separate from whats known as animated features. Ebert would later add that "it doesnt have the technical polish of a film like Tarzan (1999 film)|Tarzan, but is a reminder that the classic cartoon look is a beloved style of its own."  Entertainment Weekly critic Lisa Schwarzbaum graded the film a B+, describing it as a "hip, funny, mostly nonmusical, decidedly non-epic family picture, which turns out to be less of a heros journey than a meeting of sitcom minds." 

Published in The Austin Chronicle, Marc Sovlov gave the film 2/5 stars noting the film "suffers from a persistent case of narrative backsliding that only serves to make older members of the audience long for the days of the dwarves, beauties, and poisoned apples of Disney-yore, and younger ones squirm in their seats." Sovlov continued to express his displeasure in the animation in comparison to last years Tarzan (1999 film)|Tarzan writing it "is also a minor letdown, with none of the ecstatic visual tour de force."  Movie reviewer Bob Strauss acknowledged the film is "funny, frantic and colorful enough to keep the small fry diverted for its short but strained 78 minutes", though except for "some nice voice work, a few impressive scale gags and interesting, Inca-inspired design elements, there is very little here for the rest of the family to latch onto." Strauss would target the massive story overhaul during production as the main problem. 

===Box office=== How the Grinch Stole Christmas.  Overall, the film grossed $89,302,687 at the U.S. box office, and an additional $80,025,000 worldwide; totals lower than those for most of the Disney Feature Animation productions released in the 1990s. 

Because of its pre-Columbian theme and Latin American flavor, Disney spent $250,000 in its marketing campaign towards the Latino market releasing dual English and Spanish-language theatrical prints in sixteen multiplexes across heavily populated Latino areas in Los Angeles, California in contrast to releasing dubbed or subtitled theatrical prints of their previous animated features in foreign markets.  By January 2001, following nineteen days into its theatrical general release, the Spanish-dubbed prints were pulled from multiplexes as Latino Americans opted to watch the English-language prints with its grossing averaging $571,000 in comparison to $96,000 for the former. 

==Accolades==
{| class="wikitable plainrowheaders"
|+  List of Awards and Nominations 
|-
! scope="col" style="width:4%;"| Year
! scope="col" style="width:25%;"| Award
! scope="col" style="width:33%;"| Category
! scope="col" style="width:33%;"| Recipients and nominees
! scope="col" style="width:5%;"| Results
|-
! scope="row" rowspan="20" style="text-align:center;"| 2001 Golden Satellite Award Satellite Award Best Animated or Mixed Media
|The Emperors New Groove
| 
|- Satellite Award Best Original Song
|"My Funny Friend and Me"
| 
|- 72nd Golden Globe Awards Golden Globe Best Original Song
|"My Funny Friend and Me"
| 
|-
| scope="row" rowspan="11" style="text-align:center;"| 28th Annie Awards Annie Award Best Animated Feature
|The Emperors New Groove
| 
|- Annie Award Individual Achievement in Directing Mark Dindal 
|   
|- Annie Award Individual Achievement in Writing  Mark Dindal, Chris Williams, David Reynolds 
|  
|- Individual Achievement in Storyboarding Stephen J. Anderson  
| 
|- Don Hall Don Hall
|  
|- Individual Achievement in Production Design  Colin Stimpson
|  
|- Individual Achievement in Character Animation Dale Baer 
|  
|- Annie Award Individual Achievement in Voice Acting  Eartha Kitt
|  
|- Patrick Warburton
|   
|- Individual Achievement in Music  Sting (musician)|Sting, David Hartley 
|  
|- Annie Award Individual Achievement in Music Score John Debney 
|  
|- 73rd Academy Awards Academy Award Best Original Song
|"My Funny Friend and Me"
| 
|-
|scope="row" rowspan="3" style="text-align:center;"| Phoenix Films Critics Society Awards Best Song
|"My Funny Friend and Me" 
| 
|- Best Animated Film
|The Emperors New Groove
| 
|- Best Family Film
|The Emperors New Groove
| 
|-
|scope="row" rowspan="2" style="text-align:center;"| Las Vegas Critics Society Awards Best Family Film 
|The Emperors New Groove  
| 
|- Best Song 
|"My Funny Friend and Me"
| 
|-
! scope="row" rowspan="1" style="text-align:center;"| 2002  44th Annual Grammy Awards  Grammy Award Best Song Written for a Motion Picture, Television or Other Visual Media
|"My Funny Friend and Me" 
| 
|}

==The Sweatbox==
 
 Sting (whose wife created the documentary), Disney story artists, and the voice cast being dismayed by the new direction. At the film, Disney was not believed to be opposing Trudie Stylers documentary with Disney animation executive Thomas Schumacher, who had seen footage, commenting that "I think its going to be great!" 

The film premiered at the 2002 Toronto International Film Festival, but has gone virtually unseen by the public ever since. Disney owns the rights, but has never officially released it.  In March 2012, the documentary was leaked by a United Kingdom cartoonist, and one version was uploaded on the video-sharing website YouTube before it was ultimately pulled.  As of April 2015, some scenes from the documentary could be seen from the home media release, including the behind the scenes and the making of My Funny Friend and Me.

==Adaptations and sequels==
In April 2005, it was announced that DisneyToon Studios was producing a direct-to-video sequel entitled Kronks New Groove, which was released on December 13, 2005, timed with the premiere of Disney Channel cartoon series, The Emperors New School.  Patrick Warburton, Eartha Kitt, and Wendie Malick reprised their roles for the sequel and series, although J. P. Manoux took over the role of Kuzco (replacing David Spade for the series) and Fred Tatasciore voiced Pacha in season 1. John Goodman subsequently reprised his role for the second and final season of The Emperors New School.

Kuzco was featured as a guest in  .
 Disney Interactive Sony Computer Entertainment of America. The second, for the Nintendo Game Boy Color, was developed by Sandbox and published by Ubisoft. Both titles were released in PAL territories the following year. The PlayStation version was re-released for the North American PlayStation Network on July 27, 2010.

The Tokyo DisneySea rollercoaster attraction Raging Spirits took visual inspiration for its Incan ruins theme from the buildings in the film, with a structure based on Kuzcos palace similarly crowning the ruins site. 

==References==
 

; DVD media
 
*   | ref = DVD1}}
*   | ref = DVD2}}
 

==External links==
 
* 
* 
* 
* 
* 
* 
* 
*  A look at the production designs, background art and character designs that went into creating the mythical South American world of The Emperor’s New Groove. (It refers to the film as Disneys 39th animated feature; until 2008 Dinosaur (film)|Dinosaur was not part of Disneys canon.)
*  
*  

 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 