Come Out Fighting (1945 film)
{{Infobox film
| name           = Come Out Fighting
| image          = 
| image_size     =
| alt            =
| caption        = 
| director       = William Beaudine
| producer       = Sam Katzman
| writer         = Earle Snell
| narrator       =
| starring       = Leo Gorcey Huntz Hall Billy Benedict Gabriel Dell June Carlson Amelita Ward Addison Richards
| music          = Edward J. Kay
| cinematography = Ira H. Morgan William Austin
| studio         = 
| distributor    = 
| released       =  
| runtime        = 62 mins
| country        = United States
| language       = English
| budget         = 85,000
| gross          = 
}} 1945 American film directed by William Beaudine. It was the last in the Monogram Pictures series of "East Side Kids" films before the series was reinvented as "The Bowery Boys." Leonard Maltin, Leonard Maltins Movie & Video Guide, page 256, 1998.  Film critic Leonard Maltin described the film as "grating," giving it one and a half out of four stars. 

==Plot==
The East Side Kids are ejected from their clubhouse in a raid brought on by complaining neighbors, they have no place to train for an upcoming boxing tournament. The police commissioner is worried that his son Gilbert, who prefers ballet to boxing, is turning out to be a wimp, so he offers the gang a deal: hell lay off them if they will take his son in their gang and toughen the boy up. Gang member Muggs McGinnis takes an instant dislike to Gilbert, and sets Gilbert up to get in a fight with Danny More, the gangs best boxer, but is impressed when he sees Gilbert use ballet moves to avoid getting hit, and instead knock out Danny. Later, the East Side Kids learn that Gilberts girlfriend Rita has taken Gilbert to an illegal casino owned by local gangsters. The East Side Kids get to the casino just before cops raid the place. Muggs is able to sneak Gilbert out, but Danny is injured, and Muggs himself is caught, and is therefore barred from entering the boxing tournament. Gilbert agrees to participate in the tournament, and is in bad shape after the first two rounds. Muggs advises Gilbert to use his ballet moves, which enables Gilbert to win the match. Gilbert then confesses the truth about having been at the casino, and his police commissioner father clears Muggs of all charges. 

==Cast==

===The East Side Kids===
* Leo Gorcey as Mugs McGinnis
* Huntz Hall as Glimpy
* Billy Benedict as Skinny
* Gabriel Dell as Talman (a.k.a. Pete)
* Mende Koenig as Danny
* Buddy Gorman as Sammy

===Remaining cast===
* June Carlson as Jane Riley
* Amelita Ward as Rita Joyce
* Addison Richards as Police Commissioner James Mitchell
* George Meeker as Silk Henry Johnny Duncan as Gilbert Mitchell
* Fred Kelsey as Mr. McGinnis, Sr. Douglas Wood as Mayor
* Milton Kibbee as Police chief Pat Gleason as Little Pete Vargas
* Robert Homans as Police Sergeant Tom Riley
* Patsy Moran as Mrs. McGinnis Alan Foster as Whitey
* Davison Clark as Officer McGowan
* Meyer Grace as Jake Betty Sinclair as Stenographer

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 


 