Girl Time
 

{{Infobox film
| name           = Girl Time
| image          =
| image size     =
| caption        =
| director       = Will Cowan
| producer       = Will Cowan
| writer         =
| narrator       =
| starring       =
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =
| runtime        = 15 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}

Girl Time is a 1947 American concert film directed by Will Cowan.

==Plot summary==
 

==Cast==
*Ina Ray Hutton as Herself / Orchestra Leader
*Nellie Lutcher as Herself
*Dorothy Costello as Herself / Dancer (as The Costello Twins)
*Ruth Costello as Herself / Dancer (as The Costello Twins)
*Lucita as Herself
*Tina Ramirez as Herself / Dancer (as Tina and Coco)
*  as Herself / Dancer (as Tina and Coco)

==Soundtrack==
*Ina Ray Hutton and Her Orchestra - "When My Sugar Walks Down the Street" (Written by Gene Austin, Jimmy McHugh and Irving Mills)
*Lucita - "Hungarian Rhapsody No. 2" (Music by Franz Liszt)
*Ina Ray Hutton and Her Orchestra - "Jamaica Rhumba" (Music by Gene de Paul, lyrics by Don Raye)
*Nellie Lutcher - "Hes a Real Gone Guy" (Written by Nellie Lutcher)
*Ina Ray Hutton and Her Orchestra - "Granada" (Music by Agustín Lara)

==External links==
* 
* 

 
 
 
 
 
 
 


 
 