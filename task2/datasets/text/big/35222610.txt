Parachute Nurse
{{Infobox film
| name           = Parachute Nurse
| image          = Parnurpos.jpg
| image_size     =
| caption        = Original film poster Charles Barton
| producer       =  Wallace MacDonald Irving Briskin
| writer         = Elizabeth Meehan (story) Rian James
| narrator       = William Wright
| music          = John Leipold (uncredited)
| cinematography = Phillip Tannura
| editing        = Mel Thorson
| distributor    = Columbia Pictures
| released       =  
| runtime        = 63 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 1942 Columbia Charles Barton.

==Plot==
A cross section of American nurses enlists in the Aerial Nurse Corps during World War II.  The film shows their training including being dropped by parachute to provide aid to wounded soldiers.

==Aerial Nurse Corps==
The Aerial Nurse Corps on America was created in 1936 by Lauretta M. Schimmoler.  Although never officially recognised the unit was regarded as the predecessor of the United States Air Force Nurse Corps.   Schimmoler was technical advisor and appears in the film as a Captain.

==Cast==
Marguerite Chapman ...  Glenda White   William Wright ...  Lt. Woods  
Kay Harris ...  Dottie Morrison  
Lauretta Schimmoler ...  Jane Morgan  
Louise Allbritton ...  Helen Ames  
Forrest Tucker ...  Lt. Tucker  
Frank Sully ...  Sgt. Peters  
Diedra Vale ...  Ruby Stark  
Evelyn Wahl ...  Gretchen Ernst  
Shirley Patterson ...  Katherine Webb  
Eileen OHearn ...  Mary Mack  
Roma Aldrich ...  Nita Dominick  
Marjorie Riordan ...  Wendie Holmes 
Catherine Craig ...  Lt. Mullins   Douglas Wood ...  Maj. Devon  
Marie Windsor ...  Company C Girl

==Notes==
 

==External links==
*  

 
 
 
 
 
 

 