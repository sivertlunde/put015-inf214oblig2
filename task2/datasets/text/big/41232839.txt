Zombie Planet
{{Infobox film
| name           = 
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = George Bonilla
| producer       = Russell Coy II Tammy Bonilla Douglas Campbell Xyliena Praetor
| screenplay     = George Bonilla
| starring       = Frank Farhat Christopher Rose Matt Perry Rebecca Minton Karl Gustav Lindstrom
| music          = Klevin Scott
| cinematography = Billy W. Blackwell Todd Burrows Jon Shelton Roy M. White
| editing        = Sven Granlund Matthew Perry
| studio         = ZP Productions
| distributor    = Tempe Entertainment
| released       =   }}
| runtime        = 119 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Zombie Planet is a 2004 American horror film directed and written by George Bonilla.  Frank Farhat stars as a zombie hunter in a post-apocalyptic world.

== Plot ==
After a pharmaceutical company develops a new wonder drug, its customers turn into zombies.  T. K. Kane, a mysterious outsider, arrives in a community terrorized by local warlord Adam.  Kane protects the community and hunts down the zombies plaguing the community.

== Cast ==
* Frank Farhat as T. K. Kane
* Christopher Rose as Warren
* Matthew Perry as Adam
* Rebecca Minton as Julie
* Karl Gustav Lindstrom as Frank

== Production ==
Director George Bonilla was inspired to make a zombie film after moving to Kentucky and finding a supportive independent film community there.  Bonilla believed that zombies created from a defective drug would allow for a wide variety of effects on people, and allowed him to write in both feral and intelligent zombies.  The cast and crew were mainly made up of people with no experience in filmmaking.   Scenes were shot at the Bluegrass Aspendale housing project in Lexington, Kentucky.  Bonillas wife Tammy is the executive producer. 

== Release ==
Zombie Planet was distributed by J. R. Bookwalters Tempe Entertainment.   It was released on DVD on October 19, 2004. 

== Reception ==
Peter Dendle called the film "epically awful" and embarrassing for Lexington natives.   David Johnson of DVD Verdict said that the film is better than most Z movies, but its length leads to too much filler.   Bill Gibron of DVD Talk rated the film 3/5 stars and said that "there are kernels of creativity and outright cleverness in this big, sloppy ersatz-spectacle."  

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 