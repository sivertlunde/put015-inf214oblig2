Gair Kanooni
{{Infobox film
| name    = Gair Kanooni
| image   = Gair_Kanooni_Hand_Made_Poster.jpg
| caption = Gair Kanooni Hand Made Poster
| director = Prayag Raj
| writer   = Prayag Raj Kader Khan Govinda Sridevi Rajinikanth Kimi Katkar Shashi Kapoor
| producer = Chetna M. Modi
| music    = Bappi Lahiri
| editor   = A. Paul Durai Singham	 cinematography = Lawrence DSouza
| released = 24 March 1989
| runtime  =  Hindi
| budget   =
}}
  Govinda and Sridevi in the lead roles. Rajinikanth plays an supporting role. The film was an average grosser.

==Plot==
Officer Kapil Khanna was a strict enforcer of the law. In order to gain evidence against the local mafia Don DCosta he seek a favour from his underworld friend Aazam Khan a man with a golden heart. Aazam Khan was successful in securing evidence against DCosta but the mafia got wise to it and before Aazam Khan could reach the law, DCosta with the help of his friend Dalpat Dalal liquidated him. The blame of Aazam Khans death was owned by a petty driver Nathulal for a price paid by Dalpat Dala. Aazam Khans little son Akbar swore vengeance against Police Officer Kapil Khanna as he felt him responsible. Kapil Khannas wife delivered a baby boy in the hospital of DCosta, so did Dalpat Dalals wife to a baby girl. DCosta returned Dalpats favour by interchanging the two on Dalpats insistence. Kapil Khanna was enraged at this and he took to a legal battle against the hospital authorities. 

==Cast==
* Sridevi - Laxmi
* Shashi Kapoor - Kapil Khanna
* Govinda - Om Narayan
* Rajinikanth - Ahmad Khan
* Aruna Irani - Bantho
* Kimi Katkar - Rita
* Rohini Hattangadi - Mrs. Devi Khanna
* Kader Khan - Deepak Dalal

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Man Gora Tan Kala"
| Kishore Kumar, Asha Bhosle
|-
| 2
| "Tum Jo Parda Rakhoge"
| Bappi Lahiri, Aparna Mayekar
|-
| 3
| "Pahije Mala Pahije Mala"
| Bappi Lahiri, Asha Bhosle
|-
| 4
| "Tik Tik Tik"
| Bappi Lahiri, Alisha Chinai
|}

==References==
 

==External links==
* 

 
 
 


 