Panama Hattie
{{Infobox musical name   = Panama Hattie subtitle = image =  caption =  music  = Cole Porter  lyrics = Cole Porter book = Herbert Fields   B. G. DeSylva basis =  Broadway  West End   1954 U.S. Television
 
|awards= 
}}
 musical with music and lyrics by Cole Porter and book by Herbert Fields and B. G. DeSylva. It is also the title of a 1942 Metro-Goldwyn-Mayer musical film based upon the play. The title is a play on words, referring to the popular Panama hat.

==Productions==
Pre-Broadway tryouts started at the Shubert Theatre, New Haven on October 3, 1940, and then at the Shubert Theatre, Boston on October 8, 1940.   sondheimguide.com, accessed January 11, 2011 

The musical premiered on  . The cast featured   and Constance Dowling,  Betsy Blair, Lucille Bremer and Vera-Ellen. 
 West End at the Piccadilly Theatre on November 4, 1943 and ran for 308 performances.  It was produced by William Mollison with the entire production supervised by Lee Ephraim and dances by Wendy Toye. The cast featured Bebe Daniels as Hattie, Max Wall as Eddy, Claude Hulbert as Vivian, Frances Marsden as Florrie, Ivan Brandt as Nick, Georgia MacKinnon as Leila, Richard Hearne as Loopy and Betty Blackler as Elizabeth. 
 Barbican Cinema 1 in London in 1996 as part of the "Discovering Lost Musicals" series directed and produced by Ian Marshall-Fisher.  Louise Gold starred as Hattie, with Jon Glover as Windy.   "Musicals Tonight!" series presented a staged concert of the musical in New York City in October 2010. 

==Plot==
;Act I
Hattie Maloney owns a night club in the Panama Canal Zone where she also performs.  Three sailors from the S. S. Idaho, Skat Briggs, Windy Deegan and Woozy Hoga, ask her to sing at a party they are organizing ("Join It Right Away").  Nick Bullet, Hattie’s fiance, is a wealthy Navy officer.  They are about to meet his eight-year-old daughter Geraldine (Jerry), off the boat from Philadelphia.  He tells Hattie, "My Mother Would Love You".  Hattie, eager to make a good impression on her prospective stepdaughter, spends three weeks wages on her elaborately frilly outfit.  But when she arrives, Jerry makes fun of Hatties clothing and way of speaking.  Feeling that her marriage is off, Hattie gets drunk on rum ("I’ve still Got my Health").  Kitty-Belle, the daughter of Admiral Whitney Randolph, wants to marry Nick, and she schemes to end his romance with Hattie.

Florrie, a singer in the night club, develops a crush on Nicks very proper butler Vivian Budd ("Fresh as a Daisy").  Nick’s efforts to persuade Jerry and Hattie to get along with each other finally succeed, with Jerry making the still hungover Hattie cut the bows off her dress and shoes ("Let’s Be Buddies").  Jerry gives Hattie advice on how to behave like a lady at a party where she is to be presented to Nick’s boss, the Admiral ("I’m Throwing a Ball Tonight").  Admiral Randolph is to be presented with a cup, and his daughter Kitty-Belle suggests that Hattie might present it filled with goldenrod. This gives Whitney hay fever; Hattie is blamed, and Nick is ordered not to marry Hattie. 

;Act II
The sailors from the S. S. Idaho uncover a spy plot involving saboteurs.  Hattie swears off rum ("Make It Another Old Fashioned Please").  Hattie has it out with Kitty-Belle, whose boyfriend keeps being called in whenever Hattie is on the verge of hitting her.  Meanwhile, Florrie continues to try to attract the romantic attention of Budd ("All I’ve Got to Get Now is My Man").  Hattie, two of the sailors and Budd meet regarding these various threads ("You Said It").   Mildred Hunter, Kitty-Belle’s best friend, turns out to be a terrorist ("Who would Have Dreamed").  She gives Jerry a secret package to put in Nick’s desk.  Fortunately, Hattie overhears the plot to blow up the Panama Canal control room, finds the bomb and throws it out, saving the day.  The grateful Admiral Whitney retracts his order and the sailors praise Hattie ("God Bless the Woman").

==Songs==
Source: Panama Hattie Original Broadway Production 
 
 
;Act 1
*"A Stroll on the Plaza Sant Ana" - Ensemble
*"Join It Right Away" - Woozy, Skat, Windy, Ensemble
*"Visit Panama" - Hattie, Ensemble
*"My Mother Would Love You" - Hattie, Nick
*"Ive Still Got My Health" - Hattie, Ensemble
*"Fresh as a Daisy" - Florrie, Skat, Windy
*"Welcome to Jerry" ("Welcome to Betty" London title)- Ensemble
*"Lets Be Buddies" - Hattie, Geraldine
*"They Aint Done Right By Our Nell" - Florrie, Budd
*"Im Throwing a Ball Tonight" - Hattie, Ensemble
  
;Act 2
*"We Detest a Fiesta" - Ensemble
*"Who Would Have Dreamed?" - Janis Carter, Ty
*"Make It Another Old-Fashioned, Please" - Hattie
*"All Ive Got to Get Now is My Man" - Florrie, Ensemble
*"You Said It" - Hattie, Budd, Woozy, Skat, Windy
*"God Bless the Woman"- Woozy, Skat, Windy
 

==Adaptations==
===Film===
{{Infobox film
| name           = Panama Hattie
| image	         = 
| caption        =
| director       = 
| producer       = Arthur Freed
| writer         = 
| based on = 
| starring       = 
| music          =
| cinematography =
| editing        =
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 
| country        = United States English
| budget         = $1,121,000  . 
| gross          = $2,326,000 
}} Marsha Hunt as Leila Tree, 
Virginia OBrien as Flo Foster, Alan Mowbray as Jay Jerkins, Dan Dailey as Dick Bulliard and Lena Horne as Singer in Phils Place.   Songs used in the film are as follows:  
* "Hattie from Panama" (Roger Edens) - Chorus
* "Ive Still Got My Health" (Porter) - Ann Sothern
* "Berry Me Not" (Phil Moore)   Just One of Those Things" (Porter)- Lena Horne  
* "Fresh As a Daisy" (Porter) - Virginia OBrien
* "Good Neighbors" (Edens) - Red Skelton, Rags Ragland, Ben Blue and Chorus
* "Lets Be Buddies" (Porter) - Sothern with Jackie Horner, and OBrien with Alan Mowbray
* "Hail, Hail, the Gangs All Here" (Arthur Sullivan; Theodore F. Morse)  
* "Did I Get Stinkin At the Savoy" (E. Y. Harburg and Walter Donaldson) - OBrien
* "The Sping" (Moore and J. LeGon) - Horne  
* "The Son of a Gun Who Picks on Uncle Sam" (Harburg and Burton Lane) - Company
====Reception====
According to MGM records the film earned $1,798,000 in the US and Canada, $528,000 elsewhere, making the studio a profit of $474,000. 

===Television===
The Best of Broadway series broadcast a version of Panama Hattie on CBS Television on November 10, 1954. Ethel Merman, Ray Middleton, and Art Carney starred. 

==Reception==
Brooks Atkinson in The New York Times wrote that "By hiring a trio of knockabout comedians, Mr. De Sylva has given it all the advantages of a burlesque show...Everything is noisy, funny and in order."  Merman "rolls though it with the greatest gusto, giving it a shake and a gleam and plenty of syncopation...The Merman hangs bangles on any song that comes her way."  

"Panama Hattie was a typical example of turning a routine musical comedy into entertainment gold. Without her   there was no show, and the musical has rarely been heard of since." 

==References==
 

==Further reading==
*Schwartz, Charles.   Cole Porter: a biography, pp.&nbsp;207–08, Da Capo Press, 1979, ISBN 0-306-80097-7

==External links==
*  
*  
*  
*  

 
 
 
 

 
 
 
 
 
 
 