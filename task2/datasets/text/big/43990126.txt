Madhavikutty (film)
{{Infobox film 
| name           = Madhavikutty
| image          =
| caption        =
| director       = Thoppil Bhasi
| producer       = Hari Pothan
| writer         = Vaikkom Chandrasekharan Nair Thoppil Bhasi (dialogues)
| screenplay     = Thoppil Bhasi Madhu Jayabharathi KPAC Lalitha Adoor Bhasi
| music          = G. Devarajan
| cinematography = U Rajagopal
| editing        = G Venkittaraman
| studio         = Supriya 
| distributor    = Supriya 
| released       =  
| country        = India Malayalam
}}
 1973 Cinema Indian Malayalam Malayalam film, directed by Thoppil Bhasi and produced by Hari Pothan. The film stars Madhu (actor)|Madhu, Jayabharathi, KPAC Lalitha and Adoor Bhasi in lead roles. The film had musical score by G. Devarajan.   

==Cast== Madhu
*Jayabharathi
*KPAC Lalitha
*Adoor Bhasi
*Sankaradi
*Sreelatha Namboothiri
*T. R. Omana
*Vijayakumar
*Bahadoor
*MG Soman

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Vayalar Ramavarma and . 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Chirakulla Kilikalkke || P. Madhuri || Vayalar Ramavarma || 
|-
| 2 || Maanathu Kannikal || P Jayachandran || Vayalar Ramavarma || 
|-
| 3 || Maveli Naduvaneedum Kalam || P. Leela, Chorus || Vayalar Ramavarma || 
|-
| 4 || Sreemangalya || P. Madhuri || Vayalar Ramavarma || 
|-
| 5 || Veeraviraatakumaara || P. Madhuri, Chorus ||  || 
|}

==References==
 

==External links==
*  

 
 
 

 