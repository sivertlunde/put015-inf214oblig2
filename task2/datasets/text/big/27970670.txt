Service with a Smile (1937 film)
{{Infobox Hollywood cartoon
| cartoon_name = Service with a Smile
| series = Betty Boop
| image =
| caption =
| director = Dave Fleischer
| story_artist =
| animator = Lillian Friedman Myron Waldman
| voice_actor = Mae Questel
| musician =
| producer = Max Fleischer
| distributor = Paramount Pictures
| release_date = September 23, 1937
| color_process = Black-and-white
| runtime = 7 mins
| movie_language = English
}}

Service with a Smile (1937 in film|1937) is a Fleischer Studios animated short film starring Betty Boop and Grampy. 

==Plot==
Betty Boop runs the newly opened Hi-De-Ho-Tel, which has 40 rooms and only 2 baths. Guests have lots of complaints, ranging from a drawer shelf nailed into the wall apart from the rest of the drawer to pillowcases filled with cement bags and faulty beds with very short blankets. When they stress Betty out, she calls Grampy, who remedies the guests complaints by making several complex contraptions.

==References==
;Notes
 

==External links==
*  
*  

 

 
 
 
 
 


 