Calvaire (film)
 The Daleks}}
{{Infobox film
| name           = Calvaire
| image          = Calvaire.jpg
| image_size     =
| caption        = Official theatrical poster
| director       = Fabrice Du Welz
| writer         = Fabrice Du Welz Romain Protat
| producer       = Michael Gentile Eddy Géradon-Luyckx Vincent Tavier
| starring       = Laurent Lucas Jackie Berroyer Philippe Nahon
| music          = Vincent Cahay
| editing        = Sabine Hubeaux
| cinematography = Benoît Debie
| studio         = La Parti Productions Tarantula Studio Canal The Film Backup Films Centre du Cinéma et de lAudiovisuel de la Communauté Française de Belgique Fonds National de Soutien à la Production Audiovisuelle du Luxembourg Télédistributeurs Wallons
| distributor    = Mars Distribution
| released       =  
| runtime        = 88 minutes
| country        = Belgium France Luxembourg French
}} Belgian psychological horror film directed by Fabrice Du Welz, starring Laurent Lucas, Philippe Nahon and Jackie Berroyer.

== Plot synopsis ==

Marc Stevens is a struggling low-level performer, who makes his living performing light pop ballads and easy listening tunes at retirement homes and other small venues around Belgium. En route to perform at a Christmas special, his van — which doubles as his home — breaks down during a storm and he is stranded deep in the woods. Lost, cold, and succumbing to the elements, Marc is rescued by a local, an emaciated young man named Boris, who takes Marc to a run down inn.

The sole occupant of the inn is its proprietor, Mr. Bartel, an amiable old man who lives there as a hermit of sorts. Claiming to be a retired standup comedian, Bartel welcomes Marc to stay and offers to repair his van as a token of brotherhood between professional entertainers. Marc accepts the offer, but remains aloof, not speaking with Bartel about his own career or personal life.
 voyeuristically watching a teenage boy have intercourse with a pig, calling the experience "so tender". Meanwhile, rather than repairing Marcs van, Bartel snoops through Marcs living quarters and takes his mobile phone and some amateur pornographic photographs presented to Marc by a fan (Brigitte Lahaie).

That night, Bartel becomes even more aggressive, working himself into a frenzy while recalling his adulterous wife Gloria who abandoned him years before. He insists that Marc sing him a song before going to bed. The next day, Marc finds the homemade porn in the inn and realizes Bartel has been going through his things; when he attempts to call for help, he discovers that the telephone Bartel has been regularly using isnt even wired into the wall. Confronting Bartel, Marc discovers him vandalizing the van and pouring petrol over it; Bartel knocks Marc unconscious and blows the van up.

Marc wakes to find himself tied to a chair, clad only in an old sundress. Mr. Bartel, now babbling, addresses Marc as if he were his wife, asking why "she" has come back after leaving him. Mr. Bartel sets about shaving one half of Marcs scalp, to "protect" him from the villagers, before forcing him into bed and cuddling next to him.
 rabbit snare. He lies there prone while darkness falls, until Boris wanders by. Marc begs him for help, but Boris ignores his pleas, addressing Marc as if he were his lost dog. He sits beside Marc and pets and strokes him until the desperate Marc bites his leg, at which he goes away. The next morning, Bartel, alerted by Boris, retrieves Marc, driving him back to the inn covered by a blanket on the back of a hay truck. A pair of villagers see Boris driving the truck with something concealed under a blanket, but they take no action.
 crucifies Marc behind the inn before going into the village to have a drink at the local pub. Seemingly convinced that his wife was a "slut" who was sleeping with every man in town, Bartel warns the men drinking in the bar that now "she" has "returned", none of them can "have her". The men all appear frightened at Bartels ramblings, but once he leaves, one of the patrons sits at the antique piano and begins to play nightmarishly discordant polka music. Gradually the men all get up and begin dancing with one another. 

Back at the inn, Bartel brings Marc into the kitchen and they sit down for Christmas dinner. Boris arrives with a calf, convinced that it is his missing dog. Bartel gives a tearful, impassioned speech about love, togetherness, and the spirit of the holidays, before a sudden rifle shot rings out and a bullet explodes through the inns window, killing Boris. The villagers lay siege to the inn, intent on reclaiming the calf, and it quickly becomes apparent that they are also intent on raping Marc, in the shared delusion that Marc is Bartels returned wife. The villagers mortally wound Bartel before turning their attention to Marc; one of them then briefly rapes Marc on the dining room table. 
 Calvaire of the title. Marc manages to elude all but one of the men, who is about to capture "Gloria" when he falls into a bog and starts being swallowed up by the mire. Crying and broken, Marc approaches the drowning man. Instead of using the mans gun against him - or making any attempt to save him - Marc watches as he sinks below the surface. Finally, just before the mans head sinks into the marsh, Marc, as Gloria, responds to his impassioned question by telling him that "she" does, in fact, love him. Within seconds, the villager is dead, and Marc is alone in the wilderness. 

==Cast==
 
* Laurent Lucas  as Marc Stevens
* Brigitte Lahaie as mademoiselle Vicky
* Gigi Coursigny as madame Langhoff
* Jean-Luc Couchard as Boris
* Jackie Berroyer as Bartel
* Philippe Nahon as Robert Orton
* Philippe GrandHenry as Tomas Orton
* Jo Prestia as Fermier Mylène
* Marc Lefebvre as Lucien
* Alfred David-Pingouin as Roland
* Alain Delaunois as Gáant
* Vincent Cahay as Stan le Pianiste
* Johan Meys as Rosto
* Romain Protat, figurant dans le bar
* Damien Waselle, figurant dans le bar
* Viktor Mikol, figurant dans le bar
* Nedzad Kurtagic, figurant dans le bar
* Yves Vaucher, figurant dans le bar
* Borhan Du Welz as enfant dans le bois
* Maxime Dewitte as enfant dans le bois
* Alexis Dewitte as enfant dans le bois
* Liam Gilson as enfant dans le bois
* Raphaël Schmidt as enfant dans le bois
* Eliot Cahay as enfant dans le bois
* Farkhad Alekperov as enfant dans le bois
 

==Release==
It premiered on 18 May 2004 as part of the Cannes Film Festival and had his US release on August 25, 2006. Its official English title was The Ordeal, but most reviewers have still referred to it as Calvaire which refers to Christs ordeal on his crucifixion.

===DVD release===
The short film included on the Tartan Film DVD is called A wonderful love also directed by Fabrice Du Welz. Even the American DVD has Calvaire as the prominent title, with The Ordeal as a subtitle.

==Awards==

===Amsterdam Fantastic Film Festival===
* Grand Prize of European Fantasy Film in Silver

===Gérardmer Film Festival===
* International Critics Award
* Premiere Award
* Special Jury Prize

==References==
 

==External links==
* 
* 
* 
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 