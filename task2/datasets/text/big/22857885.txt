Rez Bomb
  2008 feature Chris Robinson.

The film is a love story/thriller and is set in Pine Ridge Indian Reservation in South Dakota, which is the poorest place in the USA. The film was written to be shot in Scotland. The other main location was Rushville, Nebraska.

The film premiered at the Montreal World Film Festival in 2008.

==Film festivals==

===2008===
Montreal World   South Dakota   Fort Lauderdale   Southern Appalachian   Santa Fe

===2009===
Victoria   Glasgow   Bermuda   Tiburon

==External links==
  
Official movie page: http://www.rezbomb.com

 
 
 


 