The Front Line (2011 film)
{{Infobox film
| name           =The Front Line
| image          = The_Front_Line_(2011_film).jpg
| caption        = South Korean Poster
| film name      = {{Film name
| hangul         =   
| hanja          =  
| mr             = Kojijŏn
| rr             = Gojijeon}}
| director       = Jang Hoon
| producer       = Lee Woo-jeong Kim Hyeon-cheol
| writer         = Park Sang-yeon
| starring       = Shin Ha-kyun Go Soo
| music          = Jang Young-gyu Dalparan
| cinematography = Kim Woo-hyung
| editing        = Kim Sang-beom Kim Jae-beom
| studio         = TPS Company
| distributor    = Showbox
| released       =  
| runtime        = 133 minutes
| country        = South Korea
| language       = Korean
| gross          =   
}} Rough Cut. 84th Academy Awards for Best Foreign Language Film,   but did not make the final shortlist.  It also won four Grand Bell Awards, including Best Film. 

== Plot ==
Early in the Korean War in 1950 as the North is rolling through South Korea, South Korean privates Eun-pyo and Soo-hyeok are captured during a battle and brought to North Korean captain Jung-yoon. Jung-yoon declaims to the prisoners that the war will be over in a week and that he knows exactly why they are fighting this devastating war, brother against brother. Afterwards he lets the prisoners go free, so that they can help reconstruct the nation after the war.

Two years later, in 1953, despite ceasefire negotiations, the fighting continues around the Aerok Hills on the eastern front, as that would help determine the future dividing line between the North and the South. The hills continue to change hands, so that even the ceasefire negotiators dont always know who controls them.

Amidst the fighting, the South Korean officer commanding Alligator Company, who are fighting for Aerok Hill, is found dead, by a Southern bullet. First Lieutenant Kang Eun-Pyo of the South Korean Armys Counterintelligence Corps (the precursor to South Koreas current Defense Security Command) is sent to investigate the murder and also an apparent mole who had mailed a letter from a Northern soldier to his family in the South.

Eun-pyo arrives at the front lines accompanied with Captain Jae-oh, the replacement commanding officer of Alligator Company, and a raw recruit. Eun-Pyos perceptions change quickly upon arriving at the eastern front. The acting commander, Captain Young-Il, is addicted to morphine, some of the men are wearing captured Northern uniforms to keep warm and refer to their fallen friends as "comrades", war orphans live among the soldiers, and Eun-Pyos old friend Kim Soo-Hyeok is not only alive but has moved up in rank now also a first lieutenant. He has also transformed from a scared and useless soldier to a ruthless killer and tactician. Also, the entire unit seems to be attempting to suppress their memories about something unspoken that happened in Pohang, earlier in the war.

It turns out that Jung-Yoon is commanding the North Korean forces against them, and after the North has again retaken the hill, the South take it back and Eun-Pyo joins them in battle. When the fighting is finished he sees that the new recruit is drunk. As alcohol was not distributed to troops, his suspicion leads him to discover Soo-Hyeok, Young-Il, and a few other soldiers enjoying the contents of a secret communication box buried within the hills in a cave that acts as a mail system and gift exchange from one side to the other. It had originally been used to trade insults between the sides, but evolved into exchanging letters and presents. The North would usually leave rice wine, and the South would leave American made cigarettes and chocolate; occasionally there would also be some personal effects in those boxes that the two sides would share, and sometimes letters as well.

Winter turns to summer and the land continues to change sides. Secrets are held not only by Soo-Hyeok but among the seasoned fighters on both sides. Near the end of the war, Chinese troops are deployed in human wave tactics to attack the hill, and the North Koreans are positioned to cut off the retreat route of the South Koreans. During the battle, Jae-oh errs by committing his men to the front line despite the pleas of his lieutenants to fall back to a more defensible position. Eventually, the hill is overrun, and Jae-Oh is murdered by Soo-Hyeok after refusing to retreat, and he takes charge himself, leading the men into the forested hills nearby to escape. Soo-Hyeok in turn is killed by a North Korean sniper nicknamed Two Seconds, who had already killed more than 30 South Koreans, including the young recruit that had accompanied Eun-Pyo into the hills. The death of Soo-Hyeok greatly saddens Eun-Pyo, who then carries Soo-Hyeoks body back into the South Korean lines.

After the battle, news of an armistice agreement reaches both sides, and celebrations start. A group of North and South Korean soldiers encounter each other washing up in a mountain valley stream, but after a tense early moment, send each other off with goodbyes. However, the armistice is not yet in effect for another 12 hours. Both sides are told by their superiors to fight for the most strategically important pieces of land. As a result, there is a final climactic battle in which the South Koreans makes a final attempt at Aerok. Here, Eun-Pyo kills Two Seconds, Captain Young-Ils arm and leg are blown off by an explosion and then is shot and killed by Jung-Yoon. Eventually, everyone on both sides is killed except for the North Korean commander Jung-Yoon, who has been wounded in the stomach, and Eun-pyo.

Eun-Pyo finds Jung-Yoon in the cave with the gift box. He asks him why exactly they are fighting. Jung-Yoon replies that he knew once, but has now forgotten. They hear on the radio that the armistice has come into effect and all fighting is to stop, to which they burst out laughing. They share a smoke, and Jung-Yoon dies shortly after from his wounds.

The movie ends with a shot of a shell-shocked Eun-Pyo walking alone down the hill among the corpses of all the fallen soldiers, leaving the fate of Aerok Hill unknown.

==Cast==
* Shin Ha-kyun as First Lieutenant Kang Eun-Pyo
* Go Soo as First Lieutenant Kim Soo-Hyeok
* Lee Je-hoon as Captain Shin Il-Young, the young company commander.
* Ryu Seung-soo as Oh Gi-Yeong
* Ko Chang-seok as Master-Sergeant Yang Hyo-Sam
* Kim Ok-bin as Cha Tae-Kyeong, the North Korean female sniper, known as Two Seconds due to the time delay between her bullet hitting the target and the sound of the shot.
* Ryu Seung-ryong as Hyeon Jeong-Yoon, the North Korean commander
* Lee David as Nam Seong-Shik, the recruit
* Seo Joon-yeol as tobacco soldier
* Choi Min as anti-aircraft army officer
* Jo Min-ho as 2P radio soldier
* Kim Rok-gyeong as reservist soldier
* Han Seong-yong as squad leader
* Ha Su-ho as Third Platoon member
* Yoon Min-soo as Alligator Company staff sergeant
* Cho Jin-woong as Yoo Jae-ho
* Park Yeong-seo as Hwang Seon-cheol
* Jung In-gi as Lee Sang-eok
* Woo Seung-min
* Jang In-ho
* Ha Seong-cheol

== Awards and nominations ==
{| class="wikitable"
|- bgcolor="#CCCCCC"
! Year !! Award !! Category !! Recipient !! Result
|-
| rowspan=31| 2011 
| rowspan=4|   20th Buil Film Awards
| Best Film || The Front Line ||  
|-
| Best Supporting Actor || Ko Chang-seok ||  
|-
| Best New Actor || Lee Je-hoon ||  
|-
| Best Art Direction || Ryu Seong-hee ||  
|-
| rowspan=12|  48th Grand Bell Awards 
| Best Film || The Front Line ||  
|-
| Best Director || Jang Hoon ||  
|-
| Best Supporting Actor || Ryu Seung-ryong ||  
|-
| Best New Actor || Lee Je-hoon ||  
|-
| Best Screenplay || Park Sang-yeon ||  
|-
| Best Cinematography || Kim Woo-hyung ||  
|-
| Best Editing || Kim Sang-beom, Kim Jae-beom ||  
|-
| Best Lighting || Kim Jae-min ||  
|-
| Best Costume Design || Jo Sang-gyeong ||  
|-
| Best Visual Effects || Jeong Seong-jin ||  
|-
| Best Sound Effects || Kim Suk-won, Kim Chang-seop ||  
|-
| Best Planning || Lee Woo-jeong ||  
|-
| rowspan=4|   31st Korean Association of Film Critics Awards 
| Best Film || The Front Line ||  
|-
| Best Director || Jang Hoon ||  
|-
| Best Screenplay || Park Sang-yeon ||  
|-
| Best New Actor || Lee Je-hoon ||  
|- 32nd Blue Dragon Film Awards
| Best Film || The Front Line ||  
|-
| Best Director || Jang Hoon ||  
|-
| Best Actor || Go Soo ||  
|-
| Best Supporting Actor || Ko Chang-seok ||  
|-
| Best New Actor || Lee David ||  
|-
| Best Screenplay || Park Sang-yeon ||  
|-
| Best Cinematography || Kim Woo-hyung ||  
|-
| Best Art Direction || Ryu Seong-hee ||  
|-
| Best Lighting || Kim Jae-min ||  
|-
| Best Music || Jang Young-gyu, Dalparan ||  
|-
| Technical Award (Visual Effects) || Jeong Seong-jin ||  
|-
| rowspan=2| 2012 
|   14th Udine Far East Film Festival 
| Audience Award || The Front Line ||  
|- 48th Baeksang Arts Awards || Best Screenplay || Park Sang-yeon ||  
|}

== See also ==
* List of South Korean submissions for the Academy Award for Best Foreign Language Film

== References ==
 

== External links ==
*    
*    
*  
*  
*  

 
 
 
 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 