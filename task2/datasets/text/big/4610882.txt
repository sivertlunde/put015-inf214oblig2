Be with You (film)
{{Infobox film
| name = Be With You
| image = Be with You film poster.jpg
| image_size = 
| caption = Film poster advertising Be with You in Japan
| writer = Yoshikazu Okada
| screenplay = 
| story = Takuji Ichikawa (Be with You)
| narrator = 
| starring = Yūko Takeuchi, Shido Nakamura
| music = Suguru Matsutani
| cinematography = Takahide Shibanushi
| editing = Shinichi Hiroshi
| studio = 
| director = Nobuhiro Doi
| producer = Hideyuki Honma
| distributor = Toho
| released =  
| runtime = 119 minutes
| country = Japan
| language = Japanese
| budget = 
| gross = US Dollar|US$46,616,207   
}}

 , is a 2004 Japanese drama film based on a Japanese novel of the same name written by Takuji Ichikawa.    The film was adapted from the novel by Yoshikazu Okada, and it was directed by Nobuhiro Doi.  It stars actress Yūko Takeuchi as Mio Aio and actor Shido Nakamura as Takumi Aio. 

Be with You was first released at the 17th Tokyo International Film Festival.  It was subsequently released in Japanese cinemas on 30 October 2004.  The film grossed a total of $46,616,207 from its screenings in Japan and overseas countries. 

==Plot==
Mio Aios death leaves her husband Takumi and six-year-old son Yuji to fend for themselves. Takumi is congenitally disorganized, suffers occasional fainting spells, and fears that his health compromised his dead wifes happiness. Yuji overhears relatives speculate that his own difficult delivery compromised Mios health, and blames himself for his mothers death. Mio had left Yuji a picture book; in the book, Mio departs for a celestial body she calls "the Archive Star" but reappears in Japan during the following years rainy season; turning the pages, Yuji eagerly awaits her return.

On a walk in the forest outside their house, Takumi and Yuji find a woman sheltered from the rain, and immediately accept her as Mio. She has no memory or sense of identity; she comes home to live with the father and son anyway. This new Mio asks Taku how they met and fell in love, and he recounts a tale of years of missed chances, beginning in high school and ending when she encouraged their marriage years later. As the rainy season draws to a close, Yuji discovers the "time capsule" he hid with his mother before her death. Mios diary is inside, and its version of the Mio-Taku romance holds the answers to the mystery.

==Cast==
* Yūko Takeuchi as Mio Aio (秋穂澪)
* Shido Nakamura as Takumi Aio (秋穂巧)
* Akashi Takei as Yuji Aio (秋穂佑司), the 6 year old son of Mio Aio and Takumi Aio.
* Karen Miyama as Aya, Yujis classmate
* Yosuke Asari as Takumi in his high school days
* Yuuta Hiraoka as Yuji when he becomes 18 years old
* Chihiro Otsuka as Mio in high school days
* Mikako Ichikawa as Midori Nagase (永瀬みどり), Takumis co-worker 
* Katsuo Nakamura as Takumis Boss You as Yujis teacher
* Suzuki Matsuo as the owner of a cake shop
* Fumiyo Kohinata as Dr. Noguchi (野口医師)
* Tokimasa Tanabe
* Kei Tanaka

==Release==
Be with You was showcased at the 17th Tokyo International Film Festival on 27 October 2004.    It was featured as one of the festivals special screenings.  The singers for the theme song, Orange Range, also held a performance of the theme song at the showcase. 

==Theme song==
Okinawan alternative rock band Orange Range sang the theme song of this movie called Hana (Orange Range song)|Hana.  It was the top selling single of 2005.

==References==
 

==External links==
*     
* 
*  
*   at the Japanese Movie Database  

 

 
 
 
 
 
 