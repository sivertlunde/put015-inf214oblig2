Dil
 
 
{{Infobox film
| name           = Dil
| image          = Dil (1990 film) DVD cover.jpg
| caption        = Vinyl record cover
| director       = Indra Kumar
| producer       = Indra Kumar Ashok Thakeria
| writer         = Rajeev Kaul Naushir Khatau Kamlesh Pandey Praful Parekh
| narrator       =
| starring       = Aamir Khan Madhuri Dixit Saeed Jaffrey Deven Verma Anupam Kher Johnny Lever
| music          = Anand-Milind Sameer (lyrics)
| cinematography = Baba Azmi
| editing        = Hussain A. Burmawala
| studio         = Film City Kamalistan Studios Natraj Studios
| distributor    = Maruti International Video Sound T-Series
| released       = 22 June 1990
| runtime        = 171 minutes
| country        = India
| language       = Hindi
| budget         =
| gross          =  (domestic gross) http://www.boxofficeindia.com/showProd.php?itemCat=196&catName=MTk5MA== 
}} romantic drama Telugu in Prashanth in the leading roles. The film was 1990s highest-grossing film, establishing the career of Kumar as a director, reaffirming the position of Khan as a lead actor and turning Dixit into a superstar who also received the Filmfare Award for Best Actress for her performance.

==Plot summary==
Hazari Prasad (Anupam Kher) is a miser who dreams of finding a rich young woman for his only son, Raja (Aamir Khan), to marry. However, Raja is a spendthrift who is only interested in spending his fathers money on wild parties.

One day as Raja is walking to his college, a passing jeep douses him with mud and the rude response of the beautiful Madhu (Madhuri Dixit) who is driving enrages Raja. He tricks Madhu into thinking that he is blind and then mocks her when the truth is revealed. The two quickly become enemies and play pranks on each other. Raja causes Madhu to trip during a dance rehearsal, and she forces him into a fight with the schools champion boxer Shakti (Adi Irani), which Raja wins.

Meanwhile, Hazari is looking for a bride with a large dowry for Raja, only to find that his involvement in the wastepaper business makes his son a less-than-stellar marriage prospect. While visiting a lavish hotel, Hazari accidentally runs into a rich man, Mr. Mehra (Saeed Jaffrey), who has one daughter. Hazari hires actors to pretend to be his staff and gives beggars large quantities of counterfeit money to masquerade as a wealthy industrialist. He quickly becomes friends with Mr. Mehra and the two agree to marry their children to each other. However, when Raja meets his prospective bride, she turns out to be Madhu and the two refuse to consider the possibility of marriage.

Madhu takes her dislike of Raja too far on a weekend school retreat when she falsely accuses him of trying to rape her. Raja is furious that his reputation has been ruined and reprimands her for her dishonesty and thoughtlessness, pointing out that many men would take their revenge, but he is different. Madhu instantly falls in love with Raja and kisses him in front of her entire class. The two enjoy an idyllic holiday. When they return to the city for their engagement party, Mr. Mehra discovers Hazaris true financial circumstances. Furious, he insults and strikes Hazari who immediately takes offense. Raja and Madhu are forbidden by their parents to see each other again.

Nevertheless, the two continue to secretly meet. When Mr. Mehra discovers this he arranges to have some thugs beat up Raja. He also decides to send Madhu away where she will not be able to contact Raja. Before he can do so, Raja sneaks into Madhus house and the two are married on the spot. Mr. Mehra banishes Madhu from the house, declaring that she is dead to him. Hazari also disowns Raja when he discovers that he has married the daughter of his greatest enemy. The couple moves into a small shack and Raja finds work as a construction worker. Despite their poverty, they are happy.

Raja is hurt at the construction site. Madhu goes to beg his father for money to pay for an emergency operation. Hazari agrees, but only if she divorces him. He removes her wedding necklace and, in despair, Madhu returns to her fathers house. Seeing her distress, he forgives her, but orders her to never see Raja again. When Raja recovers, Hazari tells him that Madhu returned to her father and never visited him at the hospital. Believing Madhu has deserted him to return to a life of luxury, Raja returns to his parents. Later, he discovers from his mother (Sarita Joshi) that Madhu did not come to see him in the hospital because of his fathers blackmail. He rushes to stop her from taking a plane to London but arrives after it takes off. Luckily, Madhu did not take the plane, and the two manage to reconcile their parents.

== Cast ==
* Aamir Khan as Raja Prasad
* Madhuri Dixit as Madhu Mehra
* Anupam Kher as Hazari Prasad, Rajas father
* Padma Rani as Savitri, Rajas mother
* Saeed Jaffrey as Mr. Mehra, Madhus father
* Shammi as Madhus grandmother
* Deven Verma as Police Inspector Ghalib
* Satyendra Kapoor as Girdharilal
* Rajesh Puri as Pandit
* Adi Irani as Shakti

== Soundtrack ==
{| class="wikitable"
|-
! No.
! Song
! Singer(s)
|-
| 1.
| "Mujhe Neend Na Aaye"
| Udit Narayan, Anuradha Paudwal
|-
| 2.
| "Hum Pyar Karne Wale"
| Udit Narayan, Anuradha Paudwal
|-
| 3.
| "Humne Ghar Choda Hai"
| Udit Narayan, Sadhana Sargam
|-
| 4.
| "Khambe Jaisi Khadi Hai"
| Udit Narayan
|-
| 5.
| "Dum Dama Dum"
| Udit Narayan, Anuradha Paudwal
|-
| 6.
| "O Priya Priya"
| Suresh Wadkar, Anuradha Paudwal
|-
| 7.
| "Saansein Teri, Chal Thi Rahe (Sad)"
| Asha Bhosle
|}

The soundtrack was very popular when it was released., especially appealing to the younger generation. Anand-Milind were nominated for best music direction, but lost to Aashiqui, that catapulted Nadeem-Shravan to the top spot. Dil is regarded as one of the best soundtracks of its time and most of its songs were instant hits. After the film released and was a success, the makers added a new song (a common trend back then), "Dum Dama Dum", which also became popular. Dil is one of the few films with all the songs being certified hits.

==Awards and Nominatons==
Filmfare Awards: At the 36th Filmfare out of 8 nominations Dil received only one award despite being the highest grossing film.
===Won===
* Filmfare Best Actress Award-Madhuri Dixit . 
===Nominations===
* Best Film- Indra Kumar
* Best Actor- Aamir Khan
* Best Supporting Actor- Anupam Kher
* Best Music- Anand-Milind
* Best Lyrics- Sameer for Mujhe Neend
* Best Playback Singer (Male)- Suresh Wadkar for O Priya
* Best Playback Singer (Female)- Anuradha Paudwal for Mujhe Neend

==Controversies==
The music ran into controversy when Gulshan Kumar of T-Series (who owned the music) had dubbed the songs originally rendered by Alka Yagnik with Anuradha Paudwal. Furthermore, he only dubbed the songs voiced by Alka Yagnik and spared the song "Humne Ghar Choda Hai" sung by Sadhna Sargam. Following this episode, Alka did not work with music directors Anand-Milind for two years and with T-Series for seven years. Interestingly, when she did work with T-Series again in 1997, history repeated itself, this time for the film Itihaas (film)|Itihaas, with her songs being dubbed by Anuradha Paudwal once again. (Itihaas means history) By then, Alka Yagnik was a much bigger name and her protests ensured that the music company retained both versions in the final soundtrack. Alka Yagniks versions were retained in the film as well.  

"O Priya Priya" was copied by Anand-Milind from the 1989 Ilayaraja original in Telugu film Geethanjali (1989 film)|Geethanjali.

==References==
 

== External links ==
*  

 
 
 
 
 