I'll Give a Million (1938 film)
 
{{Infobox film
| name           = Ill Give a Million
| image          = 
| caption        = 
| director       = Walter Lang
| producer       = 
| writer         = Boris Ingster Milton Sperling
| starring       = Warner Baxter
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 70 minutes
| country        = United States
| language       = English
| budget         = 
}}

Ill Give a Million is a 1938 American drama film directed by Walter Lang.   

==Cast==
* Warner Baxter as Tony Newlander
* Marjorie Weaver as Jean Hofmann
* Peter Lorre as Louie The Dope Monteau
* Jean Hersholt as Victor
* John Carradine as Kopelpeck
* J. Edward Bromberg as Editor
* Lynn Bari as Cecelia
* Fritz Feld as Max Primerose
* Sig Ruman as Anatole Primerose
* Christian Rub as Commissionaire

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 