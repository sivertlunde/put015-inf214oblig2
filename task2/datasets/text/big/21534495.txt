Zill-e-Shah
 
{{Infobox film
| name           = Zill-e-Shah
| image          = Zill-e-Shah poster.jpg
| caption        = Shaan
| producer       = Fyaz Khan
| writer         = Pervez Kaleem Shaan Saima Noor
| music          = Zulfiqar Ali
| cinematography =
| editing        =
| released       =  
| runtime        =
| country        = Pakistan
| language       = Punjabi
| budget         =
}}
 Pakistani film, in the language of Punjabi, the sequel to blockbuster movie Majajan.

==Synopsis==
 

Kulsoom (Noor (actress)|Noor) is engaged to Sarwar Shah (Babar Butt), but she is deeply in love with Zill-e-Shah (Shaan Shahid|Shan), whom she knows from childhood. So kulsooms marriage is sealed, but in an encounter Zill-e—Shah kills Sarwar, therefore he is sent to Jail.

Then an unfortunate incident occurs, when Kulsoom marries Abid Shah (Shafqat Cheema), the elder brother of Sarwar. Noor is in discomfort, but so is Zill-e, as he gives up his life of luxury,and goes in the realms of dancing girls and becomes an alcoholic. Soon he meets a Stara (Saima) a dancer, who he falls in love with, What will be the consequences of his actions? This does not make any sense if this is sequel to the movie title Majajan, Zill-e-shah is supposed to kill his wifes brother.

==Film release==
As soon as the film was released, the audience flocked to the cinema houses to watch the film.  It has become the first film in the history of Lollywood to be re-released in cinema houses across the country, within only a few weeks of it completing a (fair-to-middling) run at the ticket windows. Whats more, the films (re)release date - February 13 - has pitched it famously against the Bollywood giant, named Billu starring Shah Rukh Khan, Irrfan Khan and Lara Dutta.

Fayyaz (the producer) is not content with just rave reviews and a local re-release. He has already planned an international premiere for the film. "Were releasing the film in UK on March 6, with six prints. Before that, were going to have a grand premiere in Manchester for which well be flying our lead actors with us."

The excited producer also spoke of a Dubai release sometime later. Plans were underway for Zill e Shahs premiere in Indian Punjab too, but Fayyaz said, it might have to wait.

==Production==
Made on a budget of around two crore rupees, unheard-of for a Punjabi film, Zill e Shah attracted popular attention not much because of its print ads but its promotional videos that ran on the various local TV channels. Shot aesthetically, thanks to actor cum director Shaan, the film instantly generated box office heat. For one thing, the songs are a visual delight, and surely Noor and Saima have never looked better.

The producer said:
"The entire post of Zill e Shah was done in Mumbais famous Adlabs, also one of the worlds most expensive production houses. Whoever had a chance to watch the rushes simply loved the film. I was hoping for a grand premiere in India, but I dont know if that will be possible now, given the current political situation between the two countries."

==Cast==
{| class="wikitable"
|-  style="background:#ccc; text-align:center;"
! Actor
! Character/role
|- Zille Shah
|- Noor Bukhari||Kulsoom
|- Saima Noor||Sitara
|- Shah Hussain
|-
| Deeba||Shabano/Shah Bibi
|- Murad Shah
|- Abid Shah
|-
| Babar Butt||
|}

 

 
 
 
 
 


 