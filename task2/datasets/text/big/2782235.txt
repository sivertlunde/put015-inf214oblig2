The Audition (2000 film)
{{Infobox film
| name           = The Audition
| image          = 
| image_size     = 
| caption        = 
| director       = Chad Lowe
| producer       = 
| writer         = 
| narrator       = 
| starring       = Hilary Swank Chad Lowe Brittany Murphy
| music          = Philip Giffin
| cinematography = 
| editing        = 
| distributor    = 
| released       = December 9, 2000
| runtime        = 
| country        = United States
| language       = English
| budget         = 2$
| preceded_by    = 
| followed_by    = 
}}

The Audition is a 2000 short film written and directed by Chad Lowe. It features his then wife Hilary Swank. 
The movie is about a womans unhealthy addiction towards a young celebrity that she watched at an audition.

The film aired on Showtime (TV network)|Showtime. {{cite news
|url=http://articles.latimes.com/2000/apr/29/entertainment/ca-24563|title=Chad Lowe’s Not-So-Winding Road to ‘Take Me Home’|publisher=Los Angeles Times, archive April 29, 2007 |accessdate=2008-09-27 | date=April 29, 2000}}   Lowe stated that he intended to make a feature-length version to take to film festivals. {{cite web
|url=http://www.myvideostore.com/content/askhollywood/interviews/chad_lowe/index.html?client=myvideostore&part=2 |title=Chad Lowe, THE @SK HOLLYWOOD INTERVIEW: PART 2 publisher=myvideostore.com website|accessdate=2008-09-27}} 

==Plot==
 

==Cast==
* Hilary Swank
* Chad Lowe
* Brittany Murphy as Daniella
* Lisa Arning as Nikki
* Jason Bryden
* Nicki Micheaux as Janet
* Shaker Paleja as Pablo

==References==
 

==External links==
*  
* 

 
 
 
 
 


 