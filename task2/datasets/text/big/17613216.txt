Two Guys from Texas
 
  musical comedy David Butler, written by Allen Boretz and I.A.L. Diamond, produced by Alex Gottlieb, and released by Warner Bros. Pictures on September 4, 1948. This was a follow-up to Two Guys from Milwaukee, also starring Morgan and Carson, which in turn was an attempt to capture some of the appeal of the Bing Crosby and Bob Hope Road to...|Road pictures.

== Bugs Bunny cameo appearance == animated cameo appearance of cartoon character Bugs Bunny, voiced by Mel Blanc.  Friz Freleng, Warners leading animation director, was assigned to direct the special animated dream sequence, in which Bugs gives some advice to a caricatured Jack Carson.

While Bugs Bunny was not the star of the film, his appearance stands out as a landmark in his long career.  Bugs would later have a similar cameo in 1949s My Dream Is Yours, which also starred Jack Carson.

==Cast==
Dennis Morgan  ... 
Steve Carroll 
Jack Carson   ... 
Danny Foster 
Dorothy Malone    ... 
Joan Winston 
Penny Edwards    ... 
Maggie Reed 
Forrest Tucker   ... 
Tex Bennett 
Fred Clark   ... 
Dr. Straeger 
Gerald Mohr   ... 
Link Jessup  John Alvin   ... 
Jim Crocker 
Andrew Tombes    ... 
The Texan 
Monte Blue   ... 
Pete Nash 
The Philharmonica Trio   ... 
Specialty Act

== Film connections ==
Animation historians have noted the similarities between the animated dream sequence in this film and the Looney Tunes cartoon Swooner Crooner (1944). The latter, directed by Friz Frelengs colleague Frank Tashlin, concerned Porky Pig trying to reacquire the female chickens of his farm from a Frank Sinatra-esque rooster, who is driving the chicks away from the farm.

The same year Two Guys from Texas was released, animation director Art Davis parodied the films title with a
Merrie Melodies cartoon called Two Gophers from Texas, starring Mac & Tosh, better known as The Goofy Gophers. The title was spoofed yet again for Frelengs 1956 cartoon Two Crows from Tacos.

== References ==
*  
*  
*  

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 
 

 