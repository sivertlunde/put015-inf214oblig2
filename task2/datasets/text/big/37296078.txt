Djinn (film)
 
{{Infobox film
| name           = Djinn
| image          = djinn_poster.jpg
| image_size     = 215px
| border         = 
| alt            = 
| caption        = Theatrical release poster
| director       = Tobe Hooper
| producer       = Daniela Tully Tim Smythe
| writer         = David Tully
| starring       = Khalid Laith Razane Jammal
| music          = BC Smith
| cinematography = Joel Ransom
| editing        = Andrew Cohen Mark Stevens
| studio         = Image Nation FilmWorks
| distributor    = 
| released       =   (Abu Dhabi Film Festival) 
| runtime        = 85 minutes
| country        = United Arab Emirates
| language       = Arabic English
| budget         =  
| gross          = 
}}
Djinn is a 2013 supernatural thriller film directed by Tobe Hooper and written by David Tully. It is set in the United Arab Emirates and features the jinn|djinn. The film, produced by Image Nation, is in both Arabic and English languages. The films theatrical release has been delayed since 2011. Djinn premiered at the Abu Dhabi Film Festival on  , 2013.

==Premise==
 djinn from local Emiratis. 

==Cast==
 
*Razane Jammal
*Khalid Laith
*Aiysha Hart
*May El Calamawy
*Abdullah Al Junaibi
*Carol Abboud
*Malik McCall
*Rajeev Daswani
 

==Production==
 djinn were very uncommon, only recalling Wishmaster (film)|Wishmaster (1997). He said Djinn had minimal gore, similar to Hoopers previous films The Texas Chain Saw Massacre (1974) and Poltergeist (1982 film)|Poltergeist (1982). Tully came up with the premise for Djinn when an Emirati friend brought him to a village similar to the one in the film. The screenwriter learned about local stories told in the region. 

With a production budget of  ,  filming began in the United Arab Emirates in late March 2011 and took place at several locations throughout Dubai.  The subject matter was treated with caution so it would not offend local values in the different towns where filming took place. In Al Jazira Al Hamra, the cast and crew avoided using the word "djinn" and also taped over the films title on the directors chair.    By late August 2011, the film was in post-production. 

==Release==
 The National reported in January 2012 that a website said that Image Nation turned down two distribution offers, but Image Nation denied this and said the film took longer than expected in post-production.  In December 2012, The Guardian covered the films delay and cited multiple reasons. The paper reported that unofficial reasons included people related to Abu Dhabis royal family finding the film "to be politically subversive", horror films being "seen as totally foreign, culturally speaking" in the United Arab Emirates, and local pride that led to "rewrites and restructuring". Image Nations CEO Michael Garin denied these reasons and blamed the delay on meeting the Directors Guild of Americas requirements. 

Sales for Djinn were launched at the 63rd Berlin International Film Festival in February 2013 and continued at the 2013 Cannes Film Festival the following May.  The film premiered at the Abu Dhabi Film Festival on  , 2013. 

==Critical reception==

Jay Weissberg at Variety (magazine)|Variety said Djinn looked "outright bad" for a film by Hooper. The critic wrote, "This limp attempt at local horror takes elements from Rosemarys Baby (film)|Rosemarys Baby, The Grudge, and others, thrown together into a cheesy, ham-fisted ghost story... Hoopers lack of engagement isnt helped by unimaginative f/x and leaden dialogue." Weissberg did not find the film scary due to its recycling of the elements and thought that characterization was nonexistent. He also said the Djinn was not redeemed by either its cinematography or editing.   

Ronan Doyle at Indiewire also panned the film, "From its very first frame, expounding exposition over a shoddily-shot desert sequence, this is an unmitigated disaster of a movie, every bit as horrible as the events it attempts to portray." Doyle wrote the film was a disappointment "tonally as well as technically". He found Tullys script to be "insistently uninventive" in reusing common horror elements. Doyle concluded, "Djinn represents, in the end, a fundamental failure to capitalize on the chance for a particularly culturally-rooted new breed of horror film." 

Marwa Hamad, reviewing for Gulf News, wrote that the films use of djinns was a welcome change from traditional horror narratives. Hamad wrote, "The gimmicky nature of the film is undeniable, relying on the jump-in-your-seat sort of shockers rather than really messing with its viewers’ psyches... But that doesnt mean that Djinn failed to break ground." Hamad commended the portrayal of the Westernized Arab couple, "The characters crisis of nationality and lack of belonging underlies the entirety of the plot." She noted, "In that sense, the movie was able to resonate with—and subsequently instill terror into—a certain segment of viewers who usually benefit from feeling a sense of detachment from horror film victims who look and talk nothing like them." 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 