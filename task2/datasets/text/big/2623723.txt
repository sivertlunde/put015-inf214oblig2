Panic in Year Zero!
{{Infobox film
| name           = Panic in Year Zero!
| image          = Panic in year zero 1962 poster.jpg
| image_size     =
| alt            =
| caption        = Theatrical release poster
| director       = Ray Milland
| producer       = {{plainlist|
* Arnold Houghland
* Lou Rusoff
}}
| screenplay     = {{plainlist|
* John Morton<
* Jay Simms
}}
| story          = Jay Simms
| starring       = {{plainlist|
* Ray Milland
* Jean Hagen
* Frankie Avalon
* Joan Freeman
}}
| music          = Les Baxter
| cinematography = Gilbert Warrenton
| editing        = William Austin
| distributor    = American International Pictures
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         = $225,000 
}}
Panic in Year Zero!, sometimes known as End of the World, is a 1962 science fiction film directed by and starring Ray Milland. The original music score was composed by Les Baxter. It was written by John Morton and Jay Simms.  The film was released by American International Pictures as a double feature with Tales of Terror.

==Plot==
Soon after Harry Baldwin (Ray Milland), his wife Ann (Jean Hagen), their son Rick (Frankie Avalon), and daughter Karen (Mary Mitchell) leave suburban Los Angeles on a camping trip,  the Baldwins note unusually bright light flashes coming from a great distance.  Sporadic news reports on CONELRAD broadcasts hint at the start of a thermonuclear war, which is confirmed as the Baldwins see a large mushroom cloud over what was Los Angeles.

The family initially attempts to return to rescue Anns mother near Los Angeles, but soon abandons these plans as panicked refugees climb over one another to escape the fallout from the multiple nuclear explosions.  Witnessing the threads of society being torn apart, Harry decides that the family must find refuge at their secluded vacation spot.

Along the way, they stop to buy supplies, or, in the case of hardware store owner Ed Johnson (Richard Garland), take them by force when he wont accept a check.  They also encounter three threatening young hoodlums, Carl (Richard Bakalyan), Mickey (Rex Holman), and Andy (Neil Nephew), on the road, but manage to drive them off.

After a harrowing journey, the Baldwins reach their destination, finding shelter in a cave while they wait for order to be restored. They find that Johnson and his wife are their neighbors - but not for long. The three thugs appear and shoot them. A farming couple suffers the same fate and their teenage daughter, Marilyn (Joan Freeman) is kept as a sex slave. Karen is also raped when Mickey and Andy happen upon her.  With guns in hand, the Baldwin men fight back, killing the two murderers and freeing Marilyn. When Carl returns, he is killed as well, but Rick is seriously wounded.

With Marilyns help, they get the young man to Doctor Strong (Willis Bouchey). The doctor does what he can, but the boy needs to get to an army hospital over a hundred miles (160&nbsp;km) away for a blood transfusion or he will die. On their drive there, they encounter a military patrol, scouting for the army that is reestablishing order. After a tense meeting, they are allowed to continue. Watching them depart, the soldiers note that theyre among the "good ones" who escaped radiation sickness due to being in the mountains when the bombs went off. As the family drives on, a closing title card states: "There must be no end – only a new beginning".

==Cast==
 
 
* Ray Milland as Harry Baldwin
* Jean Hagen as Ann Baldwin
* Frankie Avalon as Rick Baldwin, son
* Mary Mitchel as Karen Baldwin, daughter
* Joan Freeman as Marilyn Hayes
* Richard Bakalyan as Carl
* Rex Holman as Mickey
 
* Richard Garland as Ed Johnson - Hardware Store Owner
* Willis Bouchey as Dr. Powell Strong
* Neil Nephew as Andy
* O.Z. Whitehead as Hogan - Grocery Store Owner
* Russ Bender as Harkness
* Shary Marshall as Bobbie Johnson
 

==Production==
The film was originally known as Survival. Filmland Events: Avalon Joins Milland in A-IS Survival
Los Angeles Times (1923-Current File)   28 Dec 1961: 20.  Samuel Arkoff of AIP said Avalon and Milland were teamed together because "they both have particular types of followers and the combination adds up to an attraction." Who Needs High Salaried Stars? Horrors! Film Makers Find Audiences Prefer Action
Alpert, Don. Los Angeles Times (1923-Current File)   15 July 1962: A8. 

==Reception==
 Michael Atkinson, New Orleans haunt its DVD margins...the movie is nevertheless an anxious, detail-rich essay on moral collapse." 

Glenn Erickson writes, in his DVD Savant review, "Panic In Year Zero! scrupulously avoids any scenes requiring more than minimalist production values yet still delivers on its promise, allowing audience imagination to expand upon the narrow scope of whats actually on the screen. It sure seemed shocking in 1962, and easily trumped other more pacifistic efforts. The Day the Earth Caught Fire was for budding flower people; Panic In Year Zero! could have been made as a sales booster for the gun industry." 

==See also==
* List of apocalyptic films
* List of nuclear holocaust fiction
* Survival film, about the film genre, with a list of related films

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 