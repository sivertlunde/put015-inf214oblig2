Confucius (1940 film)
{{Infobox film
| name           = Confucius
| image          = 
| caption        = 
| writer         = Fei Mu
| starring       = Tang Huaiqiu Zhang Yi Sima Yingcai
| director       = Fei Mu
| cinematography = 
| producer       = 
| studio         = Minhua Pictures
| country        = China
| runtime        = 
| released       =  
| language       = Mandarin
| budget         = 
}} Chinese film directed by Fei Mu. Produced during the war, the film was released twice in the 1940s before being thought lost film|lost. In 2001, the film was rediscovered when an anonymous donor sent a damaged copy of the print to the Hong Kong Film Archive (HKFA). The HKFA then spent seven years restoring the print which was finally screened to modern audiences at the 33rd Hong Kong International Film Festival in April 2009.   
 particular philosophy.

== Release history ==

=== Initial release ===
Confucius was produced and released during the waning days of the Orphan Island or Solitary Island period of Chinese cinema, a period where Shanghai studios still maintained some semblance of independence from Japanese occupiers.  The film also marked a new artistic phase for Fei Mu. While earlier films had been marked by fluidity, Confucius was consciously slow-moving and the images often symmetrical, a style that reflected contemporaries such as Sergei Eisenstein and Kenji Mizoguchi    Fei hoped the film, with its philosophical message, would appeal to a war-ravaged populace. Unfortunately, despite the films large budget and impressive production values for the time,  the films original release in late 1940 and early 1941 was a box-office failure.  A recut version was released after the war in 1948, though Fei had denounced this version of his film.  For the next several decades, Fei Mus original vision of Confucius was thought to be lost.

=== Rediscovery and restoration ===
In 2001 the Hong Kong Film Archive (HKFA) received a package from an anonymous donor consisting of several original reels of Fei Mus lost film.  Over the course of the next seven years, the HKFA painstakingly restored the film, which had suffered heavily in the intervening years, with the soundtrack liquefied in several scenes.  In 2009, restorers were finally ready to present the film to a new generation of moviegoers. A working print screened at the 33rd Hong Kong International Film Festival in April 2009.  It is believed that the copy received by the HKFA was of the recut version from 1948, and restoration efforts are ongoing in an attempt to integrate fragments of Fei Mus original vision from 1940. 

==See also==
*List of historical drama films of Asia
*List of rediscovered films

== Notes ==
 

== References ==
* Pang Laikwan. Chinese National Cinema. Routledge (2002). ISBN 0-415-17290-X.

== External links ==
*   at the Chinese Movie Database
*  

 
 
 
 
 
 
 
 
 