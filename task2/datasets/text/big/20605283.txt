Two Knights from Brooklyn
{{Infobox film
| name           = Two Knights from Brooklyn
| image          =
| caption        =  Kurt Neumann
| producer       = Hal Roach and Fred Guiol
| writer         = Clarence Marks and Earle Snell
| starring       = William Bendix Joe Sawyer Grace Bradely
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        =
| country        = United States English
| budget         =
| gross          =
}}
Two Knights from Brooklyn (1949 in film|1949) chronicles the adventures of two average "Joes" that form a Taxi company in Brooklyn, foil the infamous gangster, "The Frisco Ghost", and live through wives and girlfriend problems.

This film was actually compiled from two of Hal Roachs Streamliners short features, both originally directed by Neumann a few years before.

==Plot Summary==

The story is a mix of two previous movies in a McGuerin series, and it picks up from the plot in "The McGuerins From Brooklyn". Taxi drivers Tim McGuerin (William Bendix) and Eddie Corbett (Joe Sawyer) gets an award for successfully expanding their company to encomprise three hundred cars, from starting out with only one when they started their business in 1928.

During the ceremony, Corbett is asked by his newly hired young secretary, Lucy Gibbs, how he and his partner managed to achieve this goal, and there is a flashback to 1928, when McGuerin pursued the woman who would later become his wife, burlesque performing artist Sadie OBrien (Grace Bradley). This part of the story is fetched from the other previous movie: "Taxi, Mister". While McGuerins interest in Sadie increases, her alluring appearance also catches the eyes of notorious gangster Louis Glorio (Sheldon Leonard), and the two men become rivals in the pursuit of Sadies interest.

Fortunately for McGuerinn, the police discover that the gangster is the man behind the wanted anonymous criminal Frisco Ghost, and after a series of events the rivalry ends with Louis being arrested by the police.

As McGuerin tells his story to Lucy Gibbs, Sadie accidentally overhears some of the most dubious parts as the intercom is on. She becomes jealous, thinking that Lucy is trying to get her hands on her husband. Corbett gets problems too, being pursued by Marcia Morrison, who is only interested in him because his recent financial success. This sidetrack soon involves the rest of the cast and they all end up at some kind of health spa. 

==Cast==
* William Bendix as Tim McGuerin
* Joe Sawyer as Eddie Corbett
* Grace Bradely as Sadie OBrien

==External links==
* 
*  

==References==
 

 
 
 
 
 
 
 
 
 
 


 
 