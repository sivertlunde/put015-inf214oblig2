High Pressure (film)
 
{{Infobox film
| name           = High Pressure
| image	         = High Pressure FilmPoster.jpeg
| caption        = Theatrical Film Poster
| director       = Mervyn LeRoy
| producer       =  
| based on       =    Joseph Jackson
| starring       = William Powell 
| music          =  
| cinematography = Robert Kurrle
| editing        = Ralph Dawson
| distributor    = Warner Brothers
| released       =  
| runtime        = 74 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

High Pressure is a 1932 American Pre-Code Hollywood|pre-code comedy film directed by Mervyn LeRoy and starring William Powell and Evelyn Brent.    It is based on the play Hot Money by Aben Kandel.

==Plot==
Gar Evans (William Powell) agrees to promote Ginsburgs product, artificial rubber created from sewage, only after his friend Mike Donahey (Frank McHugh) assures him it is not a scam. Gar is superstitious; he believes he will only succeed if his long-suffering girlfriend Francine Dale (Evelyn Brent) joins them on the venture. She, however, has given up on him, especially since he left her five days before to pick up something, and never came back. It is only with great effort that he convinces her to give him another chance.
 Charles Middleton]) offers to buy the company on behalf of the established rubber firms, but the bid is too low for Gar. Banks then threatens to get an injunction preventing sales of Gars shares pending an investigation. Gar welcomes it. 

However, Ginsburg (promoted to "Colonel" by Gar), has misplaced the inventor of the process, Dr. Rudolph Pfeiffer (Harry Beresford). When he is finally located and set to work making a sample, Gar invites scientists to inspect the finished product, only to discover that Pfeiffer is a deranged crackpot (his next invention involves hens laying already decorated Easter eggs). Francine quits in disgust and prepares to sail to South America and marry Señor Rodriguez. Despite his lawyers advice to flee to another state, Gar insists on taking full responsibility.

Just as all seems lost, Banks offers to reimburse all the shareholders and pay Gar enough to make a $100,000 profit just to be rid of the whole mess (and restore natural rubber stock prices). Gar rushes to the dock to retrieve the Golden Gate controlling shares, which he had signed over to Francine. While there, he wins her back by promising to give up promoting, only to have Donahey show up with a scheme for Alaskan gold/marble/spruce wood. Soon, Gar is plotting his next campaign.

==Cast==
* William Powell as Gar Evans
* Evelyn Brent as Francine Dale George Sidney   as Ginsburg John Wray as Jimmy Moore, Gars lawyer
* Evalyn Knapp as Helen Wilson
* Guy Kibbee as Clifford Gray
* Frank McHugh as Mike Donahey
* Oscar Apfel as Mr. Hackett, from the Better Business Bureau Ben Alexander as Geoffrey Weston, Helens jealous boyfriend Harold Waldrige as Gus Vanderbilt, hired solely for his impressive last name Charles Middleton as Mr. Banks
* Harry Beresford as Dr. Rudolph Pfeiffer

==Reception== New York Times review, Mordaunt Hall described High Pressure as "a brightly written and constantly amusing film".    He noted that "William Powell is in his element" and "is an excellent type for this tale."  Sidney and Kibbee were also praised for their performances.  

==Preservation==
The film is preserved at the Library of Congress and in the Warner Archive. 

==References==
 

==Further reading==
* 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 