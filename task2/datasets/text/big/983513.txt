Free at Last: The Movie
{{Infobox film
| name           = Free at Last: the Movie
| image          = FreeAtLast TheMovie.jpg
| image_size     =
| caption        = Ken Carpenter
| producer       =
| writer         =
| narrator       =
| starring       = DC Talk
| music          = DC Talk
| cinematography =
| editing        = Eric J. Smith
| distributor    = Ventura Distribution
| released       =  
| runtime        = 90 min.
| country        = United States
| language       = English
| budget         =
| gross          =
}}

Free at Last: the Movie is the first DVD released by   of the release of Free at Last.

==History==

Free at Last: the Movie was filmed during the 1994 "Free at Last" world tour to capture a behind-the-scenes look at DC Talk. It was filmed for a theatrical release, but was plagued with delays due to contract negotiations between the band, record label, and the producers of the film.

A trailer for the film was first included in DC Talks long-form video Narrow Is the Road. Like Free at Last: the Movie, Narrow Is the Road was also a behind-the-scenes look of DC Talk filmed during the 1994 "Free at Last" tour, and included a lot of the same footage as Free at Last: the Movie. A second trailer was included in the enhanced CD single of "Jesus Freak". This trailer advertised a September 17, 1995 theatrical release date for the film. The release date passed, but the film was never released, and reports began to surface that the film had been abandoned.

In 2002, eight years after filming, the film was finally released direct to DVD. It was released without any editing/finishing being done to the original theatrical production that was previously halted amidst production and does not have a title or credits; instead having Title where the title should be and Credits where the credits should be.   

===Live tracks===
#"Say the Words"  originally released on Free at Last, 1992 
#"Luv Is a Verb"  originally released on Free at Last, 1992 
#"Word 2 the Father"  originally released on Free at Last, 1992 
#"Things Of This World"  originally released on Nu Thang, 1990 
#"Jesus is Just Alright (Reprise)"  originally released on Free at Last, 1992 
#"Jesus is Just Alright"  originally released on Free at Last, 1992 
#"The Hardway"  originally released on Free at Last, 1992 
#"The Hardway (Live)"  originally released on Free at Last, 1992  Lean On Me"  originally released on Free at Last, 1992 
#"Socially Acceptable"  originally released on Free at Last, 1992  DC Talk, 1989 
#"40 (Live)"  (U2 cover) — later released on Solo, 2001 
#"Heavenbound"  originally released on DC Talk, 1989 
#"I Wish Wed All Been Ready"  originally released on One Way: The Songs of Larry Norman, 1995 
#"Free at Last"  originally released on Free at Last, 1992 
#"Walls"  originally released on Nu Thang, 1990 
#"Time Is…"  originally released on Free at Last, 1992 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 

 