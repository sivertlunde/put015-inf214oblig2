Of Human Hearts
 
{{Infobox film
| name           = Of Human Hearts
| image          = Of Human Hearts 1938 poster.jpg
| image_size     =
| caption        = 1938 theatrical poster
| director       = Clarence Brown
| producer       = Clarence Brown (uncredited) John W. Considine, Jr. 
| writer         = Conrad Richter (uncredited)
| screenplay     = Bradbury Foote
| story          = Honore Morrow|Honoré Morrow
| starring       = Walter Huston James Stewart Beulah Bondi
| music          = Herbert Stothart
| cinematography = Clyde De Vinna
| editing        = Frank E. Hull
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 100-103 minutes
| country        = United States
| language       = English
}}
 1938 American drama film directed by Clarence Brown and starring Walter Huston, James Stewart and Beulah Bondi. Bondi was nominated for the Academy Award for Best Supporting Actress.

==Overview==
In the days leading up to the American Civil War, a young man clashes with his poor preacher father and neglects his supportive mother.

==Cast==
*Walter Huston as Rev. Ethan Wilkins
*James Stewart as Jason Wilkins
*Beulah Bondi as Mary Wilkins
*Gene Reynolds as Jason Wilkins as a child
*Guy Kibbee	as George Ames
*Charles Coburn as Dr. Charles Shingle
*John Carradine as President Abraham Lincoln
*Ann Rutherford as Annie Hawks
*Leatrice Joy Gilbert as Annie Hawks as a child
*Charley Grapewin as Jim Meeker
*Leona Roberts as Sister Clarke
*Gene Lockhart as Quid, the Janitor
*Clem Bevans as Elder Massey
*Arthur Aylesworth as Rufus Inchpin
*Sterling Holloway as Chauncey Ames

==Production notes==
* Production Dates: 18 Oct-20 Dec 1937
* The working title of the film and the title of the novel on which it was based, Benefits Forgot was taken from a quotation in William Shakespeares As You Like It, Act II, Scene 7: "Freeze, Freeze, thou bitter sky, Thou dost not bite so nigh as Benefits Forgot."
* The final title of the picture Of Human Hearts was selected by M-G-M after a nation-wide contest was advertised on the studios radio program, "Good News of 1938," to determine who could select the best title. The prize, $5,000, was awarded to Greenville, SC high school student Ray Harris; in addition to the prize money, Harris was also a specially invited guest at the films world premiere, which was held in his hometown.  The Good Earth. 
* A Life (magazine)|Life magazine article noted that the films battle scene, which was not based on a specific battle, cost $50,000, and required 2,000 men to film. Life also noted that the picture was one of a "new cycle of interest in the Civil War aroused by the novel Gone With the Wind."
*  Robert McWade, who portrayed Dr. Lupus Crumm in the picture, died after completing his role. According to news items in Hollywood Citizen-News and Motion Picture Daily, director Clarence Brown had told McWade, "Well, Bob, you played your last scene. You might as well go home," just before McWade died of heart failure.

==External links==
* 
*  
* 

 

 
 
 
 
 
 
 
 