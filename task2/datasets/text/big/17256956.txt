Star Wars Gangsta Rap
{{Infobox film
| name        = Star Wars Gangsta Rap
| image       =
| director    = Thomas Lee
| producer    = Megan Brown June Tedesco
| writer      = Jason Brannon Chris Crawford
| starring    = Jason Brannon Atomfilms
| released    =  
| runtime     = 3.5 min
| language    = English
| budget      =
}} Official Star Wars Fan Film Awards. 

==Version history==
The Star Wars Gangsta Rap started out as a song, written by Jason Brannon and Chris Crawford, with vocals by Brannon, drum machine by Crawford and Leonard, and keyboard by Brian Leonard. The group billed themselves as Bentframe.
 Thomas Lee Flash skills. This became the first (and most widely seen) version of the video. After showing the finished results to Bentframe, they together formed BentTV.

Due to the projects success, BentTV later created an improved version, referred to as the Special Edition, which debuted at the 2004 Official Star Wars Fan Film Awards. The audio track was not changed, but the visuals were completely redrawn, with improved color and shading and more detailed motion, all while emulating the relevant scenes in the movies more closely and adding more. The improvement in animation is analogous to Industrial Light & Magics 1997 Special Edition film touch-ups.

==Synopsis==
 
The Star Wars Gangsta Rap loosely mixes plotlines from   and    (and   in the Special Edition), satirically recreating several of the more famous scenes and dialogue.
 Emperor Palpatine who claims "its not the east or the west side." Darth Vader appears as a rapper sidekick and replies "No, its not." This continues until the Emperor says "its the DARK side", at which point Vader says "You are correct."  The Emperor then threatens to blow up the planets of "all you Vader haters out there."

Darth Vader and the Emperor trade raps with each other during the first verse, until Vader proclaims, "he will join us or die, WEVE GOT DEATH STAR!". Images of the Death Star flash in the background, and the line is repeated by Vader, while stormtroopers do a gangster rap wave in the background.

The next verse begins in Luke Skywalkers moisture farm on Tatooine, with his uncle Owen Lars shouting commands at him. Luke raps the second verse, with C-3PO and R2-D2 appearing in the background.
 gin an tonic", then flies to Cloud City and confronts Darth Vader. Darth Vader and Luke get into a lightsaber duel, Vader cuts off Lukes hand, then reveals that he is his father.

Han Solo appears in the final verse repeating "Knock him out the box Luke, knock him out..."
This is an ode to the outro of the song "Childrens Story" by rapper Slick Rick.

==Awards Legacies and Sequels==
The Star Wars Gangsta Rap is one of the most popular flash videos of all time, with more than 20 million online views. Editors of Time magazine listed it as the Best Online Comedy Movie of 2001.  
 Sci Fi Channel special on the awards. Clips from Star Wars Gangsta Rap Special Edition were aired on the VH1 special When Star Wars Ruled the World in 2004.

It has inspired other similar works and imitations, notably The Lords Of The Rhymes, done very much in the style of the Star Wars Gangsta Rap but instead parodying The Lord of the Rings trilogy.

BentTV created a Star Wars parody cartoon in 1999 a fake advertisement for a Star Wars Christmas CD hosted by Han Solo.

Director Thomas Lee would later animate the video for "Weird Al" Yankovics song "Ill Sue Ya" on his 2006 album "Straight Outta Lynwood". 

Following its success, an unofficial sequel titled "Star Wars Gangsta Rap 2" featuring far more explicit lyrics was made and then followed up by another one titled "Star Wars Gangsta Rap 3"; both were made with incorporate references to the prequel trilogy.

In November 2009, an official sequel titled "Star Wars Gangsta Rap: Chronicles" was released on atom.com. 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 