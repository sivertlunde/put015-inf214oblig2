Gudia (film)
{{Infobox film
| name           = Gudia
| image          = 
| caption        = 
| director       = Gautam Ghose
| producer       = Amit Khanna
| writer         = Ain Rasheed Khan
| starring       = Mithun Chakraborty
| music          = 
| cinematography = Gautam Ghose
| editing        = Moloy Banerjee
| distributor    = 
| released       =  
| runtime        = 139 minutes
| country        = India
| language       = Hindi
| budget         = 
}}

Gudia is a 1997 Indian drama film directed by Gautam Ghose. It was screened in the Un Certain Regard section at the 1997 Cannes Film Festival.   

==Plot==
Story of a simple ventriloquist, played by Mithun; his life and love.  Based on a play by Mahasweta Devi.

==Cast==
* Mithun Chakraborty as John Mendez
* Nandana Sen as Rosemary Braganza / Urvashi (voice) (as Nandana Dev Sen) Pran as Hameed
* Mohan Agashe as Braganza
* Masood Akhtar as Munna Bhai
* Tiku Talsania
* Avtar Gill as Politician
* Anjan Srivastav as Rosemarys boss

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 