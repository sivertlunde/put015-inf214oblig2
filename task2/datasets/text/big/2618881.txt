Swing Girls
{{Infobox film
| name           = Swing Girls
| image          = SwingGirlsPoster.jpg
| caption        = Theatrical release poster
| writer         = Junko Yaguchi Shinobu Yaguchi
| starring       = Juri Ueno Yūta Hiraoka
| director       = Shinobu Yaguchi
| producer       = Shintaro Horikawa Daisuke Sekiguchi
| music          = Hiroshi Kishimoto Mikkî Yoshino
| cinematography = Takahide Shibanushi
| editing        = Ryuji Miyajima
| distributor    = Toho Company
| released       =  
| runtime        = 105 minutes
| language       = Japanese
| budget         = 
| gross          = $18,453,875 
}}
  is a Japanese 2004 comedy film directed and co-written by Shinobu Yaguchi, about a group of high school girls who form a big band. The cast includes Yūta Hiraoka, Juri Ueno, Shihori Kanjiya, Yuika Motokariya and Yukari Toyashima. The film ranked 8th at the Japanese box office in 2004, and won seven prizes at the 2005 Japanese Academy Awards, including "Most Popular Film" and "Newcomer of the Year" awards for Yūta Hiraoka and Juri Ueno.

==Plot==
On a hot summers day, 13 schoolgirls are bored in a remedial math class. One girl, Tomoko, watches the schools brass band leave to perform at a baseball game, but they have forgotten their bento lunches. Tomoko and the other girls persuade their teacher, Mr Ozawa, to let them deliver the lunches. On the train, Tomoko eats a lunch, and the girls fall asleep and miss their stop. By the time they deliver the lunches to the swing band, they have spoiled, and all 42 band members become sick. Only the cymbal player, Nakamura, whose lunch Tomoko ate, is unaffected.

The next day, Nakamura holds an audition for band replacements. Knowing it was the girls fault the band became sick, he threatens to turn them in if they dont audition. The girls have no musical experience and clown around with their instruments. As they are eight members short of a brass band, Nakamura decides to turn the girls into a big band and perform swing jazz.

Nakamura trains the girls hard through the week. However, on the day before the game, the recovered brass band members return to school and take over the band. The girls are devastated. Tomoko and three other girls get supermarket to earn money for instruments, but lose their wages when they start a fire. The other girls spend their money on designer clothes and run off with the school baseball team. The remaining girls take jobs picking matsutake mushrooms, but they are attacked by a boar; they manage to kill it and claim reward money, as the boar had been destroying crops.

With the reward money, they buy second-hand instruments and have their ex-boyfriends repair them. The girls play their first show and receive advice from an anonymous jazz fan and amateur saxophone player. Chasing him to his home, they discover it is their math teacher, Mr Ozawa, and  convince him to lead the band. After another public performance, the girls who left are persuaded to rejoin.

The band records an audition tape for a winter music festival. They leave Tomoko in charge of the tape, but she sends it too late and the band is rejected as the bands are selected on a first-come-first-serve basis. Tomoko is too embarrassed to tell the others. Later, Nakamura discovers that Mr Ozawa is not really a professional saxophonist, and Mr Ozawa leaves the band. On the train to the music festival, Tomoko confesses the band have no place at the festival, and the train is trapped in snow. Mr Ozawa finds them and rushes them by bus to the festival, where they have a spot to replace another band could not perform due to the snow. They rush to the stage just in time to perform. They gave a 15-minute concert, wowing the crowd.

==Cast==
* Juri Ueno as Tomoko Suzuki (Tenor Sax)
* Yuta Hiraoka as Takuo Nakamura (Piano)
* Shihori Kanjiya as Yoshie Saito (Trumpet)
* Yuika Motokariya as Kaori Sekiguchi (Trombone)
* Yukari Toyoshima as Naomi Tanaka (Drums)
* Kana Sekine as Hiromi Watanabe
* Asuka as Chika Kubo
* Fumiko Mizuta as Yuka Yamamoto (Bass)
* Chiyo Nakamura as Emiko Okamura
* Masae Nemoto as Akemi Otsu
* Madoka Matsuda as Yumiko Shimizu
* Mutsumi Kanazaki as Rie Ishikawa
* Nagisa Abe as Reiko Shimoda (Trumpet)
* Misa Nagashima as Misato Miyazaki
* Eri Maehara as Kayo Yoshida

==Music==
The song played by the band for their audition tape was "In the Mood" by the Glenn Miller Orchestra. 
The first song played at the concert finale is "Moonlight Serenade" by Glenn Miller. 
The second song played is "Mexican Flyer" by Ken Woodman.  It is featured in Space Channel 5, which Tomokos little sister plays early on in the movie.   Sing Sing Sing with a Swing" by Louis Prima.

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 