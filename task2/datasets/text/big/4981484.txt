The Star Chamber
{{Infobox film
| name        = The Star Chamber
| image       = The Star Chamber.jpg
| imdb_id     = 0086356
| writer      = Roderick Taylor Peter Hyams (screenplay) Roderick Taylor (story)
| starring = {{Plainlist|
* Michael Douglas
* Hal Holbrook
* Yaphet Kotto
* Sharon Gless
}}
| director    = Peter Hyams
| producer    = Frank Yablans
| distributor = 20th Century Fox
| released    =  
| runtime     = 109 min
| country     = United States
| language    = English
| budget      = $8 million 
| gross       = $5,555,305
}} 1983 American notorious 17th-century English court.

==Promotion==
The promotional poster for the film shows a large white five-pointed star illumining a platform which vaguely resembles a judges bench in a courtroom. Left and right of the stars topmost point are three shadowy figures each, all resembling humans. In the center of the topmost point are three outlines of human heads. The implication is meant to be that all nine obvious and/or apparent humans represented are judges.

In the space formed by the wedge between the stars two bottom-most points is the following promotional statement:

"They are the most powerful members of our community. 
"They have a shattering secret. 
"A secret that will affect us all. 
"Only one man is willing to stop them. 
"On August 5 (the date of the films 1983 premiere), youll know who they really are."

==Plot==
Judge Steven Hardin (Michael Douglas|Douglas) is an idealistic Los Angeles jurist who gets frustrated when the technicalities of the law prevent the prosecution of two men who are accused of raping and killing a 10-year-old boy. They were driving slowly late at night and attracted the suspicion of two police officers, who wondered if the vans occupants might be burglars. After checking the license plate for violations, the policemen pulled them over for expired paperwork, claimed to have smelled marijuana, then saw a bloody shoe inside the van. However, the paperwork was actually submitted on time (it was merely processed late), meaning the police had no reason to pull over the van and Hardin has no choice (see fruit of the poisonous tree) but to throw out any subsequently discovered evidence, i.e. the bloody shoe. Hardin is even more distraught when the father of the boy attempts to shoot the criminals in court but misses and shoots a cop instead. Subsequently, the father commits suicide while in jail only after he informs Hardin that another boy has been discovered raped and murdered and tells him "This one is on you, your Honor. That boy would be alive if you hadnt let those men go." After hearing all this, Judge Hardin approaches his friend, Judge Caulfield (Hal Holbrook), who tells him of a modern-day Star Chamber: a group of judges who identify criminals who fell through the judicial systems cracks and then take actions against them outside the legal structure.

Judge Hardin participates in one of these proceedings in which he presents the case of the two criminals. The Star Chamber declares them "guilty" and dispatches a hired assassin. Soon afterward, however, police detective Harry Lowes (Yaphet Kotto) comes to Hardin with conclusive evidence that someone else raped and killed the boy. Realizing that he and the Star Chamber have just sentenced two men to die for a crime that they did not commit, Hardin implores the Star Chamber to recall the assassin, but is told by the other judges that the hit cannot be canceled. For the judges own protection, their system includes a buffer between themselves and the assassin; they do not know who he is, and he does not know who they are. They rationalize to Hardin that although an occasional mistake is inevitable and regrettable, what they are doing still serves societys greater good &mdash; especially, they argue, considering that the two targeted men are clearly criminals who are guilty of numerous other crimes, even if not of the specific crime for which the group convicted them.

Hardin makes it clear that he does not accept their reasoning. Caulfield warns him to back down because the group will do whatever they have to in order to protect themselves. Hardin refuses to heed this warning and says he will do whatever he can do to stop the men from being killed. He then tracks down the men in an attempt to warn them. However, when he arrives, they do not trust him, as he has stumbled across their illegal drug operation. After they attack him, Hardin is saved by the hitman, disguised as a police officer, who kills the two men before they can kill Hardin. The hitman points his gun at Hardin, but at the last moment, Lowes arrives and kills the hitman. 

The movie ends with the Star Chamber deciding another "case" without Hardin, who is outside in a car with Lowes recording their conversation.

==Cast==
* Michael Douglas: Superior Court Judge Steven R. Hardin 
* Hal Holbrook:    Judge Benjamin Caulfield 
* Yaphet Kotto:    Det. Harry Lowes 
* Sharon Gless:    Emily Hardin 
* James Sikking:   Dr. Harold Lewin
* Joe Regalbuto:   Arthur Cooms 
* Don Calfa:       Lawrence Monk
* David Faustino:  Tony Hardin

==Reception==
The Star Chamber earned positive reviews from critics, where it holds a 71% rating on Rotten Tomatoes based on 14 reviews.

==References==
 

==External links==
* 
* 

 

 

 
 
 
 
 
 
 
 
 
 