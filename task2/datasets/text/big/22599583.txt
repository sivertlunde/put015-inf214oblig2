8 × 8: A Chess Sonata in 8 Movements
8 Hans Richter, Marcel Duchamp, and Jean Cocteau released on March 15, 1957 in New York City. It features original music by Robert Abramson, John Gruen and Douglas Townsend.

Described by Richter as "part Sigmund Freud|Freud, part Lewis Carroll", it is a fairy tale for the subconscious based on the game of chess.

While living in New York, Hans Richter directed two feature films,  , Cocteau, Paul Bowles, Fernand Léger, Alexander Calder, Duchamp, and others, which was partially filmed on the lawn of his summer house in Southbury, Connecticut.

== External links ==
* 
* 

 
 
 
 
 
 


 