Attacked!!
{{Infobox film
| name = Attacked!!
| image = Attacked!!.jpg
| caption = Theatrical poster
| director = Yasuharu Hasebe   
| producer = 
| writer = Hideichi Nagahara
| starring = Asami Ogawa Yōko Azusa
| music = 
| cinematography = Shōhei Andō
| editing = Osamu Inoue
| distributor = Nikkatsu
| released =  
| runtime = 72 minutes
| country = Japan
| language = Japanese
| budget = 
| gross = 
}}
  aka Attack!! is a 1978 Japanese film in the "Violent Pink" style of Nikkatsus Pink film#Second wave .28The Nikkatsu Roman Porno era 1971.E2.80.931982.29|Roman porno series, directed by Yasuharu Hasebe and starring Asami Ogawa and Yōko Azusa.

==Synopsis==
A policewoman is attacked, handcuffed and raped while on night patrol. Rather than report the incident, she determines to find and punish the rapist herself. After the same attacker again rapes her in the police restroom, she becomes terrified of another encounter, and develops a victim mentality. The criminal is neither caught nor identified at the end of the film, leaving the policewomans feelings of terror and desire for vengeance unresolved.  

==Cast==
* Asami Ogawa ( ) as Kumiko Kawai 
* Yōko Azusa ( ) as  Eiko Yano
* Shigeru Ichiki ( ) as Tamura
* Kai Abe ( ) as Ōkubo
* Atsushi Takahashi ( ) as Ejima 
* Kunio Shimizu ( ) as Shingo Kanai
* Yuri Risa ( ) as Mayumi
* Noboru Migaki ( ) as Male gangster A
* Toshikatsu Matsukaze ( ) as Male gangster B
* Tomoko Sakurai ( ) as Female gangster A
* Fumie Akira ( ) as Fembale gangster B
* Jun Todoki ( ) as Yasugis wife

==Critical appraisal==
The films ending, in which the attackers identity remains a mystery, has generated the most polarizing comment from critics. Some consider this a suspenseful, unusual plot strategy, while others dismiss it as teasing the audience. Weisser, p. 54.  In their Japanese Cinema Encyclopedia: The Sex Films, the Weissers write, "the unsettling nature of this denouement generates considerably more tension than the standard thriller, as the viewer cant shake the feeling that the story hasnt ended at all. The fear continues long after the credits insist its The End". 

The Weissers judge Attacked!! to be a step back in the brutality Hasebe portrayed in his "offensively appalling" Rape! 13th Hour (1977), writing, "The film is still deplorable, but not nearly as repulsive as the former notorious entry". Weisser, p. 53.  Allmovie also notes that this film is less extreme than Rape! 13th Hour, but that Hasebe nevertheless creates tension through the suspenseful plot. Of the attacker never being revealed, Allmovie comments, "The ending feels a bit like a cheat to those viewers attempting to guess the rapists identity, but the film holds together as a solid thriller nonetheless".   

==Availability==
Attacked!! was released theatrically in Japan on February 4, 1978.  It was released on DVD in Japan on March 24, 2006, as part of Geneon Universal Entertainment|Geneons third wave of Nikkatsu Roman porno series. 

==Bibliography==

===English===
*  
*   
*  
*  
*  

===Japanese===
*  
*  
*  
*  

==Notes==
 

 

 
 
 
 
 
 
 