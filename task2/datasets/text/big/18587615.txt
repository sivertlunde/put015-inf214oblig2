Late Chrysanthemums
 
{{Infobox film
| name           = Late Chrysanthemums
| image          =
| image_size     =
| caption        = Scene with Tabe and Kin
| director       = Mikio Naruse
| producer       = Sanezumi Fujimoto Fumiko Hayashi (short stories) Sumie Tanaka (adaptation)
| narrator       =
| starring       = Haruko Sugimura Chikako Hosokawa Yuuko Mochizuki Sadako Sawamura
| music          = Ichirō Saitō
| cinematography = Masao Tamai
| editing        = Eiji Ooi
| distributor    = Toho
| released       =  
| runtime        = 101 minutes
| country        = Japan
| language       = Japanese
| budget         =
| gross          =
}}
 Fumiko Hayashi, published in 1948. The story has been translated into English by Lane Dunlop and is available in the anthology "A Late Chrysanthemum: Twenty-One Stories from the Japanese".

==Plot==
Kin, the first of the geisha, is consumed by the idea of wealth. As a moneylender, she is an embittered businesswoman who is insistent upon being repaid by her former geisha sisters, Tamae and Tomi. She is the love interest of a former soldier in Manchuria, Seki, who was sent to jail after trying to commit suicide with her many years ago. He returns to try to borrow money from her, but is quickly turned away. In the end, he is sent back to jail for a money-related crime. Kin then becomes excited when she hears that Tabe, her former patron and lover, is returning. However, she becomes furious after realizing that he does not love her, but rather just wants to borrow her money as well. She kicks him out and burns his photograph to erase his memory. In the end, she leaves the city on a search for property to buy.

Tamae and Tomi, both former geisha, live together. Tamae is plagued by migraines, and as a result, she is unable to work as frequently as she would like as a housekeeper in a hotel. She is troubled by her son Kiyoshis relationship with an older mistress (lover)|mistress. Yet, his new job allows her to repay her debts to Kin. Still, Tamae is saddened when her son decides to move away to Hokkaido. Tomi is a gambler who has not taken good care of herself after her days as a geisha. She laments her daughter Sachikos upcoming marriage to an older man and tries to persuade her against it. Tomi is also indebted to Kin and is unable to repay this money as a result of her addiction to gambling. 

The last of the four geisha is Nobu. She and her husband own a restaurant, which is frequented by the other women.

==Cast==
* Haruko Sugimura as Kin
* Chikako Hosokawa as Tamae
* Yuuko Mochizuki as Tomi
* Sadako Sawamura as Nobu
* Ken Uehara as Tabe
* Hiroshi Koizumi as Kiyoshi
* Ineko Arima as Sachiko
* Bontaro Miake as Seki
* Sonosuke Sawamura as Sentaro
* Daisuke Katō as Itaya

==External links==
*  at Japan Movie Database
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 