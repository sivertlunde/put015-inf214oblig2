Home on the Prairie
{{Infobox film
| name           = Home on the Prairie
| image          = Home_on_the_Prairie_1939_Poster.jpg
| border         = yes
| caption        = Theatrical release poster
| director       = Jack Townley
| producer       = Harry Grey
| writer         = {{Plainlist|
* Charles Arthur Powell
* Paul Franklin
}}
| narrator       = 
| starring       = {{Plainlist|
* Gene Autry
* Smiley Burnette
* June Storey
}}
| music          = Raoul Kraushaar (supervisor)
| cinematography = Reggie Lanning
| editing        = Lester Orlebeck
| studio         = Republic Pictures
| distributor    = Republic Pictures
| released       =  
| runtime        = 59 minutes
| country        = United States        
| language       = English
}}
Home on the Prairie is a 1939 American Western film directed by Jack Townley and starring Gene Autry, Smiley Burnette, and June Storey. Written by Charles Arthur Powell and Paul Franklin, the film is about a cattle inspectors efforts to prevent a corrupt cattle rancher from shipping to market a herd of cattle infected with hoof and mouth disease.   

==Plot==
A corrupt cattle rancher, Belknap (Walter Miller), and a cattle broker, H.R. Shelby (Gordon Hart), learn that their livestock have contracted the deadly hoof and mouth disease. They scheme to ship their herd to market before the agricultural inspector, Gene Autry, learns of the infection. That night, as Gene escorts Martha Wheeler (June Storey) to a square dance, Belknap and Shelby load their infected cattle onto a truck. 

Genes sidekick, Frog Millhouse (Smiley Burnette), discovers a dead calf and hurries to the dance to alert Gene. Gene, Frog, and Marthas father Jim Wheeler (George Cleveland), who is also a cattle rancher, leave the dance to investigate, but Jim becomes separated from Gene and Frog and soon discovers the truck bearing the diseased cattle. A gun battle ensues and Jim is badly wounded. Hearing the gunshots, Gene rushes to his rescue. In the chaos, the truck driver slips away to the Belknap ranch.

The next morning, Belknap and Shelby try to agitate the other ranchers by claiming that a quarantine is unfair. Gene, however, puts an end to their argument by announcing that a state veterinarian, Dr. Sommers (Jack Mulhall), is on his way. Alarmed at what the vet will find, Belknap and Shelby change the brand on their cattle to Jim Wheelers brand and bury the dead animals on the Wheeler property. When Dr. Sommers arrives, he quarantines Jims herd and allows the other ranchers to begin shipping their animals. 

Suspicious of Belknap, Gene tries to warn the vet, but is ignored. Looking for evidence, Gene and Frog ride to the Belknap ranch, where Gene discovers the sick cattle in the barn. Before Gene can return with Sommers, however, Belknap moves the cattle. Confronted with Belknaps empty barn, Sommers orders the Wheeler cattle destroyed. When Gene tries to stop the order, Dr. Sommers has him arrested for assault. 

While Gene languishes in jail, Frog sees the burial of dead cattle on the Belknap ranch and hurries to help Gene escape. After Frog lands in jail himself, he and Gene are rescued by a medicine mans elephant, which pulls the bars from the jail window. As the posse rides to destroy Jims herd, Gene and Frog present the sheriff with evidence that leads to Belknaps arrest and a clean bill of health for Jims cattle. 

==Cast==
* Gene Autry as Gene Autry
* Smiley Burnette as Frog Millhouse
* June Storey as Martha Wheeler
* George Cleveland as Jim Wheeler
* Jack Mulhall as Dr. Sommers
* Walter Miller as Belknap
* Gordon Hart as H.R. Shelby
* Hal Price as Sheriff
* Earle Hodgins as "Professor" Wentworth
* Ethan Laidlaw as Henchman Carter
* John Beach as Henchman Russ
* Jack Ingram as Henchman Wilson
* Bob Woodward as Henchman Madden
* Sherven Brothers Rodeoliers as Musicians (as The Rodoliers)
* Champion as Genes Horse (uncredited)   

==Production==
===Filming locations===
* Andy Jauregui Ranch, Placerita Canyon Road, Newhall, Santa Clarita, California, USA   

===Soundtrack===
* "Theres Nothing Like Work" (Eddie Cherkose, Smiley Burnette) by Smiley Burnette
* "Im Gonna Round Up My Blues" (Gene Autry, Johnny Marvin) by Gene Autry and the Sherven Brothers Rodeoliers
* "Shell Be Comin Round the Mountain When She Comes" (Traditional) by the Sherven Brothers Rodeoliers
* "Moonlight on the Ranchhouse" (Walter G. Samuels) by Gene Autry and June Storey
* "Big Bull Frog" (Walter G. Samuels) by Gene Autry (vocal and guitar)
* "Id Rather Be on the Outside" (Johnny Marvin)   

==Memorable quotes==
*Belknap: If that infection ever hits the stockyards, it will spread all over the country.
:H.R. Shelby: By that time well have our money out of em. Let somebody else worry about it.

* Frog Millhouse: Im warnin you. If you take any cattle across this line, Im going to blast you with this smokestick—and it aint loaded with beans!

* Martha Wheeler: What were you going to say? 
:Gene Autry: I wasnt going to say anything. 
:Frog Millhouse: Aw, he was so.   He was going to sing you that song about moonlight on the ranchhouse. 

==References==
;Citations
 
;Bibliography
 
*  
*  
*   

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 