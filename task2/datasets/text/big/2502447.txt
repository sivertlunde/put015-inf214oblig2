The Bingo Long Traveling All-Stars & Motor Kings
{{Infobox film
| name           = The Bingo Long Traveling All-Stars & Motor Kings
| image          = Bingo lingo dvd cover.jpg
| caption        = DVD cover Matthew Robbins William Brashler
| producer       = Berry Gordy Rob Cohen
| starring       = Billy Dee Williams James Earl Jones Richard Pryor
| director       = John Badham
| movie_music    = William Goldstein
| studio         = Motown Productions
| distributor    = Universal Pictures
| released       =  
| runtime        = 110 minutes
| country        = United States
| language       = English
| music          = William Goldstein
| awards         =
| budget         =
}}
 comedic sports film about a team of  enterprising ex-Negro League baseball players in the era of racial segregation. Loosely based upon William Brashlers novel of the same name, it starred Billy Dee Williams, James Earl Jones and Richard Pryor. Directed by John Badham, the movie was produced by Berry Gordy for Motown Productions and Rob Cohen for Universal Pictures, and released by Universal on July 16, 1976.

==Plot==

Tired of being treated like a slave by team owner Sallison Potter (Ted Ross), charismatic star pitcher Bingo Long (Billy Dee Williams) steals a bunch of Negro League players away from their teams, including catcher/slugger Leon Carter (James Earl Jones) and Charlie Snow (Richard Pryor), a player forever scheming to break into the segregated Major League Baseball of the 1930s by masquerading as first a Cuban ("Carlos Nevada"), then a Native American ("Chief Takahoma").  They take to the road, barnstorming through small Midwestern towns, playing the local teams to make ends meet.  One of the opposing players, Esquire Joe Calloway (Stan Shaw), is so good that they recruit him.

Bingos team becomes so outlandishly entertaining and successful, it begins to cut into the attendance of the established Negro League teams.  Finally, Bingos nemesis Potter is forced to propose a winner-take-all game: if Bingos team can beat a bunch of all-stars, it can join the league, but if it loses, the players will return to their old teams.  Potter has two of his goons kidnap Leon prior to the game as insurance, but he escapes and is key to his sides victory.
 major league scout in the audience. After the game, he offers Esquire Joe the chance to break the color barrier; with Bingos blessing, he accepts.  Leon glumly foresees the decline of the Negro League as more players follow Esquire Joes lead, but Bingo, ever the optimist, cheers him up by describing the wild promotional stunts he intends to stage to bring in the paying customers.

==Cast==
*Billy Dee Williams as Bingo Long
*James Earl Jones as Leon Carter
*Richard Pryor as Charlie Snow, "Carlos Nevada" and "Chief Takahoma"
*Stan Shaw as "Esquire Joe" Joseph Vanderbilt Calloway
*Tony Burton as Issac, an All-Star
*Rico Dawson as Willie Lee Shively, an All-Star
*Sam "Birmingham" Brison as Louis Keystone, an All-Star
*Jophery C. Brown as Emory "Champ" Chambers, an All-Star
*Leon Wagner as Fat Sam Popper, an All-Star John McCurry as Walter Murchman, an All-Star
*DeWayne Jessie as Rainbow, the All-Stars batboy. He later played singer Otis Day in Animal House.
*Ted Ross as Sallison Potter, Bingos nemesis and owner of the Ebony Aces
*Mabel King as Bertha Dewitt, another Negro League team owner
*Ken Foree as Honey, one of Potters henchmen Carl Gordon as Mack, another one of Potters goons

==Negro League tie-ins==
Some characters and situations are loosely based upon real-life people and incidents. Badham grew up in Birmingham, Alabama and was familiar with the Birmingham Black Barons, who shared Rickwood Field with the white Birmingham Barons. 
 Leroy "Satchel" Paige. Early in his career, Paige would call in his outfield while leading in the ninth inning against an amateur or semi-pro team and strike out the side. Bingo did a similar stunt in this movie. Leon Carter is a Josh Gibson-like power hitter, even playing the same position (catcher). "Esquire" Joe Callaway is an amalgam of another Black Baron, Willie Mays (in personality, talent, and fielding position) and Jackie Robinson (as being signed by a white team at the films end).

The Bingo Long Traveling All-Stars & Motor Kings were loosely based on the Indianapolis Clowns and other barnstorming Negro baseball teams, who likewise engaged in Harlem Globetrotters-like clowning routines.

==Production==
Luther Williams Field in Macon, Georgia was used for filming as the Negro League ballpark.  Luther Williams Field was home to the Macon Music, a minor league team in the independent South Coast League.  Additional ballpark scenes were shot at Morgan Field in Macon, a Pony and Colt League Youth Baseball field, Grayson Stadium in Savannah, Georgia, home of the Savannah Sand Gnats of the Class A South Atlantic League, and Wallace Field in Crawford County, Georgia. Exterior scenes set in St. Louis residential neighborhoods were also filmed in Savannah.  Scenes set in rural communities were filmed in Talbotton, Georgia and various small towns around Macon, including Monticello, Georgia.  Some ballplayers were played by actual former athletes, including former members of the Indianapolis Clowns, who performed various stunts shown in the film.

Steven Spielberg originally wanted to have a hand in producing the movie until the success of his film Jaws (film)|Jaws got his full attention.

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 