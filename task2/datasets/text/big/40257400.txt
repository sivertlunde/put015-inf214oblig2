The Psychedelic Priest
 
The Psychedelic Priest (also known as Electric Shades of Grey and Jesus Freak) is a 2001  American film produced by Allied International Films. It was directed by William Grefé, although he was uncredited, and written by Terry Merrill. It stars John Darrell, James Coleman, and Joe Crane. 

==Plot==
John, a Christian priest, says goodbye to his profession and takes a destinationless drive. He gets acquainted with a female hitchhiker, Sunny, who soon falls in love with him. However, John does not feel the same towards her and leaves, returning to work at the church.

==Production==
Also known as Electric Shades of Grey and Jesus Freak, The Psychedelic Priest was directed by William Grefé for Allied International Pictures, although for professional reasons he was not acknowledged as director  but instead director of photography. For his part, Grefé received a hundred thousand dollars in trading stamps.    Writer Stewart "Terry" Merrill received the directorial credit instead. Filming began in 1971 in California and during which there was no official timetable or script.  Shooting locations included Topanga, California|Topanga.  The cast and the crew were largely non-professional,    and real-life hippies starred in the film. 

==Release==
The films release was kept on hold after production, as it was felt that it would be a box-office failure.  After three decades, in 2001, The Psychedelic Priest was finally released as a direct-to-video project. Distribution was handled by Something Weird Video. 

===Reception===
DVD Verdict critic Bill Gibron described the film as an "accurate snapshot of Americas collective hangover" although stating that it "has got to be the single biggest downer since rave culture rediscovered the horse tranquilizer". 

==See also==
* List of films related to the hippie subculture

==Notes==
 

==References==
 

==External links==
*  
*  

 
 
 
 
 