Mr. Bhatti on Chutti
{{Infobox film
| name           = Mr. Bhatti on Chutti
| image          = Mr. Bhatti On Chutti.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Karan Razdan
| producer       = Ashwani Chopra
| writer         = 
| starring       = Anupam Kher Bhairavi Goswami Shakti Kapoor
| music          = Channi Singh
| cinematography = Aatish Parmar
| editing        = 
| studio         = 
| distributor    = Tulips Film Lts.
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}}
Mr. Bhatti on Chutti is a Hindi comedy film that was released on May 18, 2012. It stars Anupam Kher as Mr. Bhatti, Bhairavi Goswami as Katy and Shakti Kapoor as a tourist. The film was directed by Karan Razdan and is distributed under the banner of Tulips Film Ltd. 

==Cast==
* Anupam Kher as Mr. Bhatti
* Emma Kearney as Alice
* Bhairavi Goswami as Katy
* Shakti Kapoor as A Tourist
* Aman Irees as Mr. Bose
* Pawan Shankar as Prem Abid Ali as Inspector Javed Khan
* Neha Pendse

==Plot==
Mr Bhatti is a delusional character who wins a trip to the Lake District out of the blue. He comes over with a group of Indian tourists visiting the Lake District. Bhatti is under the impression that he has been called over by George W. Bush for supposed peace talks being the delusional man he is. But its not just the scenery and the possible meeting with Bush, which attracts Bhattis interest; Alice, an English girl also catches his eye. Alice has spent six months in India and is into meditation, bhajans and yoga.  Although Bhatti is immediately fascinated with Alice, so too is Prem, an eligible bachelor, also travelling with the group.

Brij Bhushan Bhatti (Kher) is a nerdy assistant bank manager and a wannabe private detective who keeps calling up the White House because he wants to talk to the US President George Bush and share his peace plan with him. When the intolerable Bhatti gets a mysterious package containing details of a free vacation to Liverpool, he jumps at the chance…and straight into a murky plot involving a lookalike terrorist named Abu Siddique (Anupam kher again) with a diabolical plan to blow up various parts of the United Kingdom, a moll Katie (Bhairavi Goswami), who talks in an irritating baby voice, a cop Javed Khan (Abid Ali), who wants Bhatti to infiltrate Siddiques gang to ferret out information, a Pakistani staying in London (Shakti Kapoor) and a motley crew of other equally asinine characters, who seem hell-bent on ruining the viewers day. Needless to say, the clumsy bumpkin Bhatti manages to save the day and becomes a hero for one and all.

==References==

  

 

==External links==
*  

 
 
 
 


 
 