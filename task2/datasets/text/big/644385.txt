You Can Count on Me
 
 
{{Infobox film name           = You Can Count on Me image          = You Can Count on Me Poster.jpg director       = Kenneth Lonergan producer       = Martin Scorsese  Barbara De Fina Larry Meistrich Jeff Sharp writer         = Kenneth Lonergan starring       = Laura Linney Mark Ruffalo Matthew Broderick Jon Tenney Rory Culkin music          = Lesley Barber cinematography = Stephen Kazmierski editing        = Anne McCabe distributor    = Paramount Classics released       =   runtime        = 111 minutes language       = English country        = United States budget         = $1.2 million gross          = $11 million 
}}
You Can Count on Me is a 2000 American   and Auburn, New York, they are further west in the Finger Lakes region.   The film was primarily shot in and around Margaretville, New York.
 awards season, including two Oscar nominations.

==Plot== lending officer at a bank, still lives in her childhood home in Scottsville, New York,  while Terry (Mark Ruffalo) has drifted around the country, scraping by and getting in and out of trouble.

After months of no communication with his sister, Terry is desperate for money, so he comes to visit her and her son Rudy (Rory Culkin) who are excited about reuniting with him. Sammy lends him the money, which he mails back to his girlfriend. After the girlfriend attempts suicide, he decides to extend his stay with his sister, which she welcomes.
 proposes to her after a short time. She needs time to consider it.

At the bank, the new Management|manager, Brian (Matthew Broderick), tries to make his mark with unusual demands about computer color schemes and daily timesheets. He is particularly tough on Sammy, requesting that she make arrangements for someone else to pick up her son from the school bus rather than leaving work. After some minor arguments, they end up having an affair, despite Brians wifes being six months pregnant.
 pool at a bar. She turns to her minister (Kenneth Lonergan) to counsel Terry about his outlook on life. While Terry resists his sisters advice, he stays on good terms with his nephew. Realizing her own questionable decisions, Sammy turns down her boyfriends marriage proposal and breaks off her relationship with Brian.

After a day of fishing, Terry and Rudy decide to visit Rudy Sr. in the town of Auburn. Confronted by his past, Rudy Sr. (Josh Lucas) is incensed, leading Terry to assault him and get arrested.

Sammy brings her brother and son home and asks Terry to move out, which he does the next day. He plans to go back to Alaska and scoffs at Sammys suggestion to remain in town and get his life back on track. While at first it appears the separation will be another heartache, they reconcile before Terry leaves, coming to terms with their respective lifestyles.

==Cast==
* Laura Linney as Samantha "Sammy" Prescott
* Mark Ruffalo as Terry Prescott
* Matthew Broderick as Brian Everett
* Jon Tenney as Bob Steegerson
* Rory Culkin as Rudy Prescott
* J. Smith-Cameron as Mabel
* Josh Lucas as Rudy (Kolinsky) Sr.  
* Gaby Hoffmann as Sheila Seidleman (Terrys girlfriend)  
* Adam LeFevre as Sheriff Darryl
* Kenneth Lonergan as Ron (the priest)
* Nina Garbiras as Nancy Everett
* Kim Parker as Rudy Sr.s girlfriend

==Production== primarily shot in and around Margaretville, New York in the Catskill Mountains, circa June 1999. Posters for Margaretvilles 4 July "Field Days" can be seen in shop windows. 

While the bank exteriors were filmed at Margaretvilles NBT bank, the interiors were filmed in another bank closer to New York City since NBT considered interior filming a security risk.   

The scenes where Rudy Jr. walks home in the rain were filmed with the assistance of the Margaretville Fire Department which used their trucks and hoses to create the rain. You Can Count on Me – DVD Extras: Director Commentary 

Many outdoor scenes away from the Village—most notably the fishing trip—were filmed in Phoenicia, New York.  The cemetery seen in the film is not the Villages—which cannot be seen from the road—rather it is a smaller cemetery four miles outside the village on Route 30.

==Reception==
===Critical response===
You Can Count on Me received highly positive reviews from critics and has a "certified fresh" score of 95% on Rotten Tomatoes based on 101 reviews with an average rating of 8.1 out of 10. The critical consensus states "You Can Count On Me may look like it belongs on the small screen, but the movie surprises with its simple yet affecting story. Beautifully acted and crafted, the movie will simply draw you in."  The film also has a score of 85 out of 100 on Metacritic based on 31 critics indicating "universal acclaim" 
 Analyze This!) Sundance Film Festival, qualifies as one of the two or three finest American films released this year....You Can Count on Me is an exquisitely observed slice of upstate New York life that reminds us there are still plenty of American communities where the pace is more human than computer-driven. The movie dares to portray small-town middle-class life in America as somewhat drab and predictable. Without ever condescending to its characters, it trusts that the everyday problems of ordinary people, if portrayed with enough knowledge, empathy and insight, can be as compelling as the most bizarre screaming carnival on The Jerry Springer Show."   

 ," and You Can Count on Me has a subtext so powerful that it reaches out and pulls you under. Even when the surface is tranquil, you know in your guts whats at stake." Edelstein concludes "Lonergan doesnt yet know how to make the camera show us things that his dialogue doesnt, but when you write dialogue like he does, you can take your time to learn. Hell, he can take another 20 movies to learn."   

According to Roger Ebert, "Beyond and beneath, that is the rich human story of You Can Count on Me. I love the way Lonergan shows his characters in flow, pressed this way and that by emotional tides and practical considerations. This is not a movie about people solving things. This is a movie about people living day to day with their plans, fears and desires. Its rare to get a good movie about the touchy adult relationship of a sister and brother. Rarer still for the director to be more fascinated by the process than the outcome. This is one of the best movies of the year." 

==Accolades== Southeastern Film Critics Association Awards

 
 
* 73rd Academy Awards
** Nominated for Best Actress (Laura Linney)
** Nominated for Best Original Screenplay
* AFI Fest (2000)
** Best New Writer
** New Directions Award
* Humanitas Prize (2001)
** Feature Film Category
* 16th Independent Spirit Awards
** Best First Feature
** Best Screenplay
** Nominated for Best Female Lead (Laura Linney)
** Nominated for Best Male Lead (Mark Ruffalo)
** Nominated for Best Debut Performance (Rory Culkin)
* Los Angeles Film Critics Association Awards (2000)
** Best Screenplay
** New Generation Award (Mark Ruffalo)
* Montreal World Film Festival (2000)
** Prize of the Ecumenical Jury - Special Mention
** Best Actor (Mark Ruffalo)
* National Board of Review of Motion Pictures (2000)
** Special Achievement Award
* National Society of Film Critics Awards (2001)
** Best Screenplay
** Best Actress (Laura Linney)
 
* New York Film Critics Circle Awards (2000)
** Best Screenplay
** Best Actress (Laura Linney) Sundance Film Festival
** Grand Jury Prize, Dramatic (shared with Girlfight)
** Waldo Salt Screenwriting Award
*5th Golden Satellite Awards
** Best Screenplay, Original
** Nominated for Best Actress - Drama (Laura Linney) 4th Toronto Film Critics Association Awards
** Best Actress (Laura Linney)
** Best Screenplay 53rd Writers Guild of America Awards
** Best Screenplay Written Directly for the Screen 7th Dallas–Fort Worth Film Critics Association Awards
** Best Actress (Laura Linney)
* 7th Screen Actors Guild Awards
** Nominated for Outstanding Performance by a Female Actor in a Leading Role 21st Boston Society of Film Critics Awards
** Best New Filmmaker (Kenneth Lonergan)
** Nominated for Best Actress (Laura Linney)
* 55th Bodil Awards
**Nominated for Best American Film
 

==Footnotes==
 

==References==
 

==External links==
*  
*  

 
 
{{Succession box
| title= 
| years=2000 tied with Girlfight
| before=Three Seasons The Believer}}
 

 

 

 
 
 
 
 
 
 
 
 
 
 