Malarvaadi Arts Club
{{Infobox film
| name           = Malarvaadi Arts Club
| image          = Malarvaadi Arts Club.jpg
| image_size     = 
| caption        = 
| director       = Vineeth Sreenivasan    Dileep  
| screenplay         = Vineeth Sreenivasan
| narrator       = 
| starring       =   
| music          = Shaan Rahman 
| cinematography = P. Sukumar 
| editing        = Ranjan Abraham
| studio         = Graand Production
| distributor    = Manjunatha Release
| released       =   (Kerala)   
| runtime        = 
| country        = India
| language       = Malayalam
| budget         =   approx.   
| gross          =     
}}
Malarvaadi Arts Club is a 2010 Malayalam film directed by Vineeth Sreenivasan in his directorial debut, and produced by Dileep (Malayalam actor)|Dileep.  The film was released in 60 theaters across Kerala. The cast of five young boys and two girls were selected through a talent hunt.      

== Plot ==
The story is about the life of 5 friends in a small village called Manissery. They have completed their studies and are all set to move to the next phase of their life. But in the process they face unexpected problems while trying to keep their favourite hangout Malarvaadi Arts Club alive.

"Malarvadi Koottam" (Malarvadi guys) are a bunch of trouble makers in the Manassery village. They even act as goons for a Communist party, named Labour Party. The first half of the film is about the pointless lives of these five led by Prakashan(Nivin Pauly). Prakashan has a fiery temperament; he would do anything for the people he loves, for the cause he believes in.
In a moment of financial crisis, Kumaran (Nedumudi Venu) (their mentor who formed malarvadi club)  makes the group take up music once again. On the way, the best singer  Santhosh(Shraavan) in  the group gets a chance to attend a reality show and once he emerges the champion,which is followed by misunderstandings, separation and a grand reunion in the end.

==Cast==
* Nivin Pauly as Prakashan
* Shraavan as Santhosh
* Bhagath Manuel as Purushu
* Harikrishnan as Praveen
* Aju Varghese as Kuttu
* Nedumudi Venu as Kumaran
* Lishoy as Damodaran
* Jagathy Sreekumar as Ragavan
* Suraj Venjaramood as Shekaran
* Malavika Wales as Geethu
* Apoorva Bose as Revathy
* Salim Kumar as Sashi Janardhanan as Gopalan
* Sreenivasan
* Sreeram Ramachandran as Geethus brothers friend 
* Kottayam Nazeer as Prem
* Renjith Reghu as Susheelan
* Manikandan Pattambi
*  Willson Joseph
* Deepak Parambol
* Deepika Mohan as Damodarans wife

==Reception==
===Critical response=== Sify rating it as Watchable movie said "The five new heroes, who were selected after talent hunts and rehearsal camps, have performed quite well".   

===Box office===
The movie opened in 60 cinemas on July 16, 2010 was declared a super hit at the box office. The movie grossed around   approximately from the first five days.     

== Sound Track ==
The films soundtrack contains 6 songs, all composed by Shaan Rahman and Lyrics by Vineeth Sreenivasan.

{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Manyamaha Janangale"
| Vineeth Sreenivasan, Sachin Warrier, Rajesh Menon 
|-
| 2
| "Kaathu Kaathu"
| Shaan Rahman
|-
| 3
| "Innoree Mazhayil"
| Rahul Nambiar, Vineeth Sreenivasan
|-
| 4
| "Changaayi"
| Vineeth Sreenivasan, Sujatha Mohan
|-
| 5
| "Snehame"
| Rajesh Menon 
|-
| 6
| "Nerin Vazhithan"
| Vineeth Sreenivasan, Shaan Rahman, Sachin Warrier, Divya S. Menon
|}

==References==
 

==External links==
*  
* http://popcorn.oneindia.in/title/4070/malarvadi-arts-club.html
* http://www.nowrunning.com/movie/7298/malayalam/malarvady-arts-club/index.htm
* http://movies.rediff.com/report/2010/mar/12/south-malayalam-first-look-malarvady-arts-club.htm
* http://movies.rediff.com/slide-show/2010/jun/04/slide-show-1-south-looking-at-vineeth-sreenivasans-malarvady-arts-club.htm

 
 
 
 