Toomelah (film)
{{Infobox film
| name           = Toomelah
| image          = Toomelah.jpg
| alt            = 
| caption        = Theatrical film poster
| director       = Ivan Sen
| producer       = David Jowsey
| writer         = Ivan Sen
| starring       = Daniel Connors Christopher Edwards Michael Connors
| music          = Ivan Sen
| cinematography = Ivan Sen
| editing        = Ivan Sen
| studio         = 
| distributor    = Visit Films
| released       =  
| runtime        = 106 minutes
| country        = Australia
| language       = English
| budget         = 
| gross          =
}} Toomelah Station, New South Wales.

==Plot==
Daniel is a ten-year-old boy living in Toomelah, New South Wales|NSW. After being suspended from school for threatening to stab a classmate with a pencil and finding there is little to do in his town, he decides he wants to be a part of the gang controlling the drug trade in his township, so he decides to help Linden, a well known local drug dealer. Bruce, one of Lindens rivals, is released from prison and a turf war erupts. Meanwhile, Daniel faces problems at school and in his family, such as his mothers addictions, the estrangement of his alcoholic father and the return of his aunt who was forcibly removed from the mission as a child during the Stolen Generations.     

==Cast==
*Daniel Connors as Daniel
*Christopher Edwards as Linden
*Danieka Connors as Tanitia
*Michael Connors as Buster
*Dean Daley-Jones as Bruce
*Alex Haines as Scammer
*Linden Binge as Jarome  

==Production==
When asked why he made a film about Toomelah, director Ivan Sen said:
 

Before filming, Sen obtained permission to film in Toomelah from the towns elders. Sen shot most of the scenes himself, as many of the actors in the movie are his personal friends and he believed their acting would be restricted if an entire conventional crew did the filming. 

==Awards==
{| class="wikitable"  
|-
! Ceremony
! Recipient
! Category
! Result
|- 2011 Asia Pacific Screen Awards
| Daniel Connors
| Best Performance by an Actor
|   
|- 2011 Asia Pacific Screen Awards
| Toomelah
| UNESCO Screen Award
|     
|- 2011 Pacific Meridian Film Festival
| Toomelah
| Grand Prix
|   
|}

==References==
{{Reflist|refs=
 {{Citation
  | last = Swift  | first = Brendan  | title = Toomelah and Sleeping Beauty selected for Cannes International Film Festival  | work = if.com.au  | publisher = Inside Film  | date = 15 April 2011  | url = http://if.com.au/2011/04/15/article/Toomelah-and-Sleeping-Beauty-selected-for-Cannes-International-Film-Festival/VUMKUMMPUB.html  | accessdate =12 May 2011
}} 
 {{Citation  | title = Toomelah Press Kit  | work = Official film website  | format = PDF  | url = http://www.toomelahthemovie.com/TOOMELAH-press-kit.com  | accessdate =12 May 2011
}} 
}}

==External links==
* 
* 

 

 
 
 
 
 
 
 