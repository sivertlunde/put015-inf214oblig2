Bittersweet Love
{{Infobox film
|image=Bittersweet Love.jpg David Miller
|producer=Joel B. Michaels, Gene Slott, Joseph Zappala
|screenplay=D.A. Kellogg, Adrian Morrall
|cinematography=Stephen M. Katz Bill Butler
|music=Kenneth Wannberg production design=Vincent M. Cresciman
|studio=Culver City Studios
|distributor=Avco Embassy Pictures
|release= 
|country=United States
|language=English
|runtime=92 min.
}} David Miller, and written by D.A. Kellogg and Adrian Morrall.

==Plot==
Michael and Patricia meet in an unusual way — while on a date with another woman, Michael attempts to retrieve his dates car keys from a fountain. When his date abandons him, he meets Patricia and they soon find themselves falling in love. After learning that she is pregnant, they decide to get married and hold the ceremony in Canada, where Michaels family lives.

Patricias parents have never met Michael, and are out of the country, therefore unable to attend the wedding. They meet their new son-in-law after returning from their travels. While reviewing pictures from the wedding she missed, Patricias mother is shocked to discover that she knows Michaels father; he is also Patrcias father, the result of a brief tryst between the two many years before. Therefore, a newly married couple learns that they are half-siblings, with a pregnancy already underway.

Distraught over the news, Patricia goes to her doctor to see if she can obtain a late-term abortion. The doctor tells her that if the fetal weight is still low, they may be able to terminate the pregnancy due to her situation. Patricia ultimately decides not to go through with the abortion, but is still weary of her relationship with Michael.

The couple spend the rest of the pregnancy from their friends and family. Patricias mother begs her to reconsider ending the pregnancy, or to go away and give the baby up for adoption, but Patricia reprimands her and says that doesnt want anymore family secrets. Michael also starts to research the history of incest, and its effects on people.

While at home, Patricia goes into labor and has Michael take her to the hospital. She gives birth to a healthy baby girl, Amy. Six weeks after Amys birth, Michael attempts to initiate sex with Patricia, who becomes hysterical. Unable to cope with Michael as her husband, Patricia ends the marriage. 

==Cast==
*Lana Turner as  Claire  Robert Lansing as  Howard 
*Celeste Holm as  Marian 
*Robert Alda as  Ben 
*Scott Hylands as  Michael 
*Meredith Baxter as  Patricia 
*Gail Strickland as  Roz 
*Richard Masur as  Alex 
*Denise DeMirjian as  Nurse Morrison  John Friedrich as  Josh 
*Amanda Gavin as  Judy 
*Jerome Guardino as Psychiatrist

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 


 