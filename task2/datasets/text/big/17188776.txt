The Experts (1989 film)
 
{{Infobox Film
| name = The Experts
| image = The Experts (1989 film).jpg
| caption = Dave Thomas
| producer = Marylin Black Diana Greenwood Jack Grossberg James Keach Jonathan D. Krane
| writer = Nick Thiel Steven Greene Eric Alter (screenplay) Steven Greene Eric Alter (story)
| starring = John Travolta Arye Gross Kelly Preston
| music = Marvin Hamlisch
| cinematography = Ronnie Taylor
| editing = Bud Molin
| distributor = Paramount Pictures
| released =  
| runtime = 94 minutes
| country = United States
| language =  
| budget = $3 million
| gross = $169,203
}} American comedy Dave Thomas. Paramount chief Ned Tannen.

==Plot==
  tiki lounge into a rock club, teach the townsfolk to dance, and introduce current pop culture. Both also flirt with local beauties. Things get dangerous when the townsfolk taste freedom, and the KGB decides to stop the experiment and get rid of the American "guests".

==Cast==
*John Travolta as Travis
*Arye Gross as Wendell
*Kelly Preston as Bonnie
*Deborah Foreman as Jill
*James Keach as Yuri
*Jan Rubes as Illyich
*Brian Doyle-Murray as Mr. Jones
*Charles Martin Smith as Mr. Smith
*Mimi Maynard as Betty Smith
*Eve Brent as Aunt Thelma
*Rick Ducommun as Sparks Steve Levitt as Gil
*Tony Edwards as Nathan
*David Mcalpine as Farmboy

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 
 