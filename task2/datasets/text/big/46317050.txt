The Mystery of Mr. Bernard Brown (film)
{{Infobox film
| name           = The Mystery of Mr. Bernard Brown
| image          =
| caption        =
| director       = Sinclair Hill
| producer       =
| writer         = E. Phillips Oppenheim (novel) Sinclair Hill Ruby Miller Annie Esmond Clifford Heatherley John Mackenzie
| editing        =
| studio         = Stoll Pictures
| distributor    = Stoll Pictures
| released       =  
| runtime        =
| country        = United Kingdom
| awards         =
| language       = Silent English intertitles
| budget         =
}} silent mystery Ruby Miller, novel of the same title by E. Phillips Oppenheim.  When the fiancée of a squires daughter is killed, suspicion falls on a novelist.

==Cast==
*  Ruby Miller (actress)| Ruby Miller as Helen Thirwell  
* Pardoe Woodman as Bernard Brown  
* Clifford Heatherley as Sir Alan Beaumerville  
* Annie Esmond as Lady Thirwell   Ivy King as Rachel Kynaston  
* Lewis Dayton as Sir Geoffrey Kynaston  
* Frank Petley as Benjamin Levy  
* Teddy Arundell as Guy Thirwell  
* Norma Whalley as Mrs. Martival

==References==
 

==Bibliography==
* Low, Rachael. History of the British Film, 1918-1929. George Allen & Unwin, 1971.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 

 