Empire Builders
{{Infobox film
| name           = Empire Builders
| image          = 
| image_size     = 
| caption        = 
| director       = 
| producer       = Phil Goldstone
| writer         = 
| based on      = 
| narrator       = 
| starring       = Snowy Baker Margaret Landis
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| released       = 1924
| runtime        = 49 mins
| country        = USA
| language       = silent
| budget         = 
}}
Empire Builders is a 1924 film starring Snowy Baker, the second movie he made in America. It was highly popular in Australia. 

Prominent roles were taken by the horses Boomerang and Prince Charles. 
==Plot==
Captain William Ballard of the Territorials is sent to make a treaty with the natives. He meets resistance from the Boers, still unreconciled to British rule. He falls in love with Katryn van der Poel. He succeeds in rescuing a lost party and recovering important military dispatches.  

==Cast==
*Snowy Baker as Captain William Ballard
*Margaret Landis as Katryn van der Poel
*Theodore Lorch as Hendrik van der Poel
*Pinckney Harrison as Karui the king
*J.P. Lockney as a trader
*Jere Austin as Fritz van Roon

==References==
 
==External links==
* 
*  at National Film and Sound Archive
 
 
 
 

 
 