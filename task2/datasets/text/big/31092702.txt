John Loves Mary
{{Infobox film
| name           = John Loves Mary
| image          = 
| image_size     = 
| caption        =  David Butler
| producer       = Jerry Wald
| screenplay     = Henry Ephron Phoebe Ephron
| based on       =  
| narrator       =  Wayne Morris Patricia Neal
| music          = David Buttolph
| cinematography = J. Peverell Marley
| editing        = Irene Morra
| studio         = Warner Bros. Pictures
| distributor    = Warner Bros. Pictures
| released       = February 19, 1949
| runtime        = 96 mins.
| country        = United States
| language       = English
| budget         = 
| gross          = 
| website        = 
}}
 David Butler. It stars Ronald Reagan, Patricia Neal and Jack Carson.  This was Patricia Neals film debut.

Based on Broadway play John Loves Mary written by Norman Krasna, Feb 04, 1947 - Feb 07, 1948 at the Booth Theatre and Music Box Theatre, New York, NY. 

==Plot summary==
 
John Lawrence (Ronald Reagan) is a returning GI. Mary McKinley (Patricia Neal in her film debut) is the girl he left behind.

But their reunion will have to wait: John has returned with cockney war bride Lilly Herbish (Virginia Field) in tow. It seems that John married Lilly as a favor to get her into the U.S., intending to divorce her so that she can wed her true love, Johns old pal Fred Taylor (Jack Carson). 

==Cast==
* Ronald Reagan as John Lawrence 
* Patricia Neal as Mary McKinley
* Jack Carson as Fred Taylor Wayne Morris as Lt. Victor OLeary  Edward Arnold as Sen. James McKinley  
* Virginia Field as Lilly Herbish
* Katharine Alexander as Phyllis McKinley  Paul Harvey as Gen. Biddle 
* Ernest Cossart as Oscar Dugan

==Original play==
{{Infobox play
| name       = John Loves Mary
| image      = 
| image_size = 
| caption    = 
| writer     = Norman Krasna
| characters = 
| setting    = Living Room of apartment of Senator James McKinley, in the St. Regis Hotel, New York.
| premiere   = 4 March 1947
| place      = New York
| orig_lang  = English
| subject    = 
| genre      = comedy
}}

John Loves Mary was a highly popular play, running for 423 performances. 

The debut production was directed by Joshua Logan.

===Original cast===
* Harry Bannister as Harwood Biddle	
* Lyle Bettger as Lt. Victor OLeary	
* Ralph Chambers as Oscar Dugan	
* Tom Ewell as Fred Taylor	
* Nina Foch as Mary McKinley	
* Ann Mason as Mrs. Phyllis McKinley	 William Prince as John Lawrence	
* Max Showalter as George Beechwood	
* Loring Smith as Senator James McKinley

==References==
 

==External links==
*  
*  
*  
*  
*  
*   at Internet Archive

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 