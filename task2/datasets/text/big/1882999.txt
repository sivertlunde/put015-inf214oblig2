All the Fine Promises
 
 
{{Infobox film
| name           = Toutes ces belles promesses
| image          = 
| caption        = 
| director       = Jean-Paul Civeyrac
| producer       = Lola Gans Philippe Martin
| writer         = Jean-Paul Civeyrac Anne Wiazemsky
| screenplay     = 
| story          = 
| based on       =  
| starring       = Jeanne Balibar Bulle Ogier Renaud Bécard Valérie Crunchant
| music          = Nicolas Becker Christophe Chevalier
| cinematography = Céline Bozon
| editing        = Sylvie Fauthoux
| studio         = 
| distributor    = 
| released       =   
| runtime        = 
| country        = France
| language       = French
| budget         = 
| gross          = 
}}

All the Fine Promises (original French title: Toutes ces belles promesses)  is a 2003 French movie directed by Jean-Paul Civeyrac based on the novel Hymne à lamour by Anne Wiazemsky.
 

== Plot ==

Coming off the bad end of a doomed love affair, and trying to cope with her Mothers death Marianne ( ). Marianne meets Béatrice and remembers her childhood. She reconnects with her nanny Ghislaine (Valérie Crunchant)...

==Movie cast==

*Jeanne Balibar : Marianne
*Bulle Ogier : Béatrice
*Valérie Crunchant : Ghislaine (young)
*Eva Truffaut : Marianne’s mother
*Renaud Bécard : Etienne

==Production credits==

*Producer: Les films Pelléas, Arte
*Director: Jean-Paul Civeyrac
*Written by: Jean-Paul Civeyrac& Anne Wiazemsky
*Musical Composer: Felix Mendelssohn Bartholdy, John Cage, Édith Piaf

==Awards==
*Prix Jean Vigo 2003

==External links==
* 
* 
*  

 
 

 