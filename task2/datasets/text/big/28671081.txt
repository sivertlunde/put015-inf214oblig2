Kids in America (film)
 Kids in America}}
{{Infobox film
| name        = Kids in America
| image       = Kids in America film poster.jpg
| caption     = Theatrical release poster
| director    = Josh Stolberg
| writer      = Josh Stolberg Andrew Shaifer Gregory Smith Stephanie Sherrin
| producer    = Andrew Shaifer
| distributor = Screen Media Films Rainstorm Entertainment Launchpad Releasing Slowhand Cinema
| released    =  
| runtime     = 91 minutes
| country     = United States
| language    = English
| budget      = $750,000
| gross       = $537,667
}}
Kids in America is a 2005 film  directed by Josh Stolberg that received mixed reviews.  It was written by Andrew Shaifer and Stolberg. The film is inspired by real events.

==Plot==
Inspired by real events,  Kids In America is a teen comedy about a diverse group of high school kids who band together to peacefully stand for their personal rights and dignity. Holden Donovan and his love interest, Charlotte Pratt, are fed up with Principal Donna Weller, who goes to great lengths to stop the students from enjoying their right to free expression, such as suspending Monica Rose for wearing condoms on her outfit to promote safe sex during Spirit Week and suspending Lawrence Reitzer for kissing another guy in the hallway. Meanwhile, she’s running for State School Superintendent, which, if elected, will afford her the power to practice her brand of administration beyond Booker High School. Holden himself is suspended and ultimately expelled for speaking out publicly against Weller, to whom he says, "You’re nothing but a politician".
 goth chick Dementia. Together, they engage in civil disobedience, passing out condoms and staging walk-outs, and organize the student body to take on Principal Weller and make a real change at Booker High. Due to their efforts, Weller loses the election and her job.

During the credits, Smith and Sherrin share a kiss that holds the record for the longest on screen kiss lasting just over six minutes. 

==Cast==

===Students=== Gregory Smith as Holden Donovan
*Stephanie Sherrin as Charlotte Pratt
*Chris Morris as Chuck McGinn
*Caitlin Wachs as Katie Carmichael
*Emy Coligado as Emily Chua
*Alex Anfanger as Lawrence Reitzer
*Crystal Celeste Grant as Walanda Jenkins
*Nicole Richie as Kelly Stepford
*Rosalie Ward as Monica Rose
*Genevieve Cortese as Ashley Harris
*Rakefet Abergel as Goth Girl
*Damien Luvara as Rick Garcia
*Marcella Lentz-Pope as Elizabeth Goings
*Raymond Braun as Mo Williams

===Faculty and staff===
*Julie Bowen as Principal Donna Weller
*Malik Yoba as Mr. Will Drucker
*Andrew Shaifer as Mr. Kip Stratton
*Adam Arkin as Mr. Ed Mumsford
*George Wendt as Coach Thompson
*Jeff Chase as Asst. Coach Fasso
*Leila Charles as Ms. Jane Jordan (Will Drucker’s girlfriend)

===Parents===
*Samantha Mathis as Jennifer Rose (Monica’s mother)
*Rosanna Arquette as Abby Pratt (Charlotte’s mother)
*Elizabeth Perkins as Sandra Carmichael (Katie’s mother)
*Charles Shaughnessy as Mr. Carmichael (Katie’s father)
*William Earl Brown as Boss McGinn (Chuck’s father)
*Kim Coles as Loretta Jenkins (Walanda’s mother)

==Inspiration==
This film was inspired by true events. Three students in particular are interviewed at the end of the film, including: Osceola High School, who was suspended for taping condoms to her shirt in order to promote safe sex,  Natalie “Nicky” Young, who was suspended for wearing a shirt proclaiming, “Barbie is a Lesbian,”  and
*Rachel Boim, who was suspended for writing a story about a girl experiencing a violent dream. 

==Soundtrack==
The movie contains the following songs:
* “Bonnie Taylor Shakedown” - hellogoodbye
* “Freedom Ain’t Free” - Crystal Celeste Grant and Steve Kim
* “False Alarm” - The Hometeam
* “Hands 2 tha Pump” - Da Digger
* “You Are My Friend” - Brownskin
* “Welcome to My World” - Nerf Herder
* “Race Cars” - Allister
* “Change the World” - An Angle
* “It Ain’t Right” - Ilona
* “Remembering Britt” - Day at the Fair
* “Sesame Smeshame” - The Early November
* “Anthem” - Trevor Hall
* “Sunday in the Public Restroom with George” - Rand Singer, Alex Anfanger and Chris Morris
* “Symphony” - I Am the Avalanche
* “She Rules the School” - Daniel Cieral
* “I Want You” - James Blunt If You Were Here” - Thompson Twins
* “Moving in Stereo” - The Cars
* “Exit, Emergency” - Houston Calls
* “My Sleep Pattern Changed” - The Early November
* “Knights of the Island Counter” - David Melillo
* “Letters to Summer” - The Track Record
* “U and Left Turns” - Socratic
* “Bad” - Ilona
* “The Bad Touch” - The Bloodhound Gang Halifax
* “Its the End of the World as We Know It (And I Feel Fine)|It’s the End of the World as We Know It (And I Feel Fine)” - R.E.M.
* “Somewhere on Fullerton” - Allister
* “One More Won’t Hurt” - Houston Calls
* “All Our Words” - Long Since Forgotten Brother Love

==External links==
* 
* 

==References==

 

 
 
 
 
 
 
 
 