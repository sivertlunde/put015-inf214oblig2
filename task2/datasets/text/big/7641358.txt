Man in the Dark
 
{{Infobox film
| name           = Man in the Dark
| image          = Man in the Dark (1953 film) poster.jpg
| alt            =
| caption        = Theatrical release poster
| director       = Lew Landers
| producer       = Wallace MacDonald
| screenplay     = George Bricker Jack Leonard Adaptation: William Sackheim
| story          = Tom Van Dycke Henry Altimus
| narrator       =
| starring       = Edmond OBrien Audrey Totter Ted de Corsia
| cinematography = Floyd Crosby
| editing        = Viola Lawrence
| studio         = Columbia Pictures
| distributor    = Columbia Pictures
| released       =  
| runtime        = 70 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $1.45 million (US) 
}}
Man in the Dark is a 1953 film noir drama 3-D film directed by Lew Landers and starring Edmond OBrien, Audrey Totter and Ted de Corsia.  It is a remake of the 1936 Ralph Bellamy film The Man Who Lived Twice. 

It was the first Columbia Pictures film released in 3-D.

==Plot==
Steve Rawley is serving a 10-year prison sentence from a Christmas Eve factory robbery that netted $130,000. He is offered an immediate parole if willing to undergo an experimental procedure by Dr. Marsden, a brain surgeon.

Steve is released, but the operation leaves him with amnesia. He takes another name and believes he lost his memory in a car crash. An insurance investigator, Jawald, trying to find the missing robbery money, is convinced Steve is faking.

Lefty, Arnie and Cookie, members of his old gang, kidnap Steve and demand to know where the loot is. Steve claims not to know or recognize any of them or Peg, who is said to be his girlfriend. Steve tries to phone for help but is beaten by his captors.

Peg begins to believe he is telling the truth about the amnesia. She flees with Steve to an amusement park, a place that Steve keeps seeing in his dreams. He finds a box containing the money. Atop a roller coaster, he fights Lefty, who falls to his death. Arnie is shot by police, who have been summoned by Jawald. By handing over the box of money, Steve hopes that he and Peg will be able to be together and live a normal life.

==Cast==
* Edmond OBrien as Steve Rawley
* Audrey Totter as Peg Benedict
* Ted de Corsia as Lefty
* Horace McMahon as Arnie
* Nick Dennis as Cookie
* Dayton Lummis as Dr. Marston
* Dan Riss as Jawald 

==Production== House of Wax as "the first feature produced by a major studio in 3-D", Man in the Dark actually premiered two days earlier. 
 Ocean Park in Santa Monica.

==Reception==
===Critical response===
When the film was released, Bosley Crowther, film critic for the New York Times, panned the film. He wrote, "Columbias first stereoscopic film—a conspicuously low-grade melodrama ... called Man in the Dark, ... must be viewed through polaroid glasses to be seen for any effect whatsoever, is a thoroughly unspectacular affair." 

More recently, critic Elliott Stein, writing for The Village Voice, discussed the effects used in the film: "This seems to be the 3-D flick that most exploits the short-lived medium. An endless array of stuff comes whiffling at your face—a lit cigar, a repulsive spider, scissors, forceps, fists, falling bodies, and a roller coaster. The prolific Landers may not have been a great director, but he was a pretty good pitcher." 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 