Sphodanam
{{Infobox film
| name           = Sphodanam
| image          = 
| director       = P. G. Viswambharan
| producer       = Babu K.J. Thomas
| writer         = Alleppey Sheriff Ravikumar Seema Seema Balan K. Nair K.P. Ummer Kuthiravattam Pappu Sankaradi Mala Aravindan Jagathi Sreekumar
| music          = Sankar Ganesh
| cinematography = U. Rajagopal
| editing        = K. Narayanan
| genre          = Action/Adventure
| distributor    = Vijaya Movies
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| awards         = 
| budget         = 60 lakhs
}}
 1981 Malayalam Indian family family feature directed by Ravikumar and Seema (actress)|Seema.

== Synopsis ==

Sphodanam is the story of 2 people who has to fight for justice against the landlord as they are the voice of working class. Ultimately good wins the battle over evil, but it suffers many.

==Trivia==

*This was released alongside THUSHARAM which was the blockbuster for the vishu in 1981.
*Mammoottys almost parallel character to hero Sukumaran was much noticed.
*In Film advertisements and Film Notice, Mammoottys name was mentioned as Sajin (Mammootty) as he looked for a name change in search of success.

== Cast ==
 
*Sukumaran as Gopi
*M.G. Soman as Surendran
*Mammootty  as Thankappan Ravikumar as Engineer Seema as Lalitha
*Sheela  as Devaki
*Balan K. Nair as Muthalali
*K.P. Ummer as Police Officer
*Prameela as Narayanapillas wife
*Kuthiravattam Pappu as Narayana Pilla
*Sankaradi as Krishnan Santhakumari as Muthalalis wife Shubha as Gouri
*Mala Aravindan as Vasu Pilla
*Jagathi Sreekumar	 as Kuttan Pilla
*Kaviyoor Ponnamma as Gopis mother
*Santo Krishnan as Kunjappi
*KPAC Premachandran as Sankaran
*Preman
 

==Soundtrack==
The music was composed by Shankar Ganesh and lyrics was written by ONV Kurup.
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Kaarkuzhalil poovu choodiya || K. J. Yesudas || ONV Kurup ||
|-
| 2 || Maari maari poomaari || S Janaki, Chorus || ONV Kurup ||
|-
| 3 || Nashtappeduvaanillonnum || K. J. Yesudas, Chorus || ONV Kurup ||
|-
| 4 || Valakilukkam kelkkanallo || P Jayachandran, Vani Jairam, B Vasantha, Jolly Abraham || ONV Kurup ||
|}

==References==
 

==External links==
*  

 
 
 
 
 
 

 

 