The Silent Raid
{{Infobox film
| name = The Silent Raid
| image =
| caption =
| director = Paul Rotha
| producer = Rudolf Meyer
| writer = Dr. L. de Jong
| starring = Rob de Vries, Cees Brusse, Piet Römer, Bernard Droog
| music =
| cinematography =
| editing =
| distributor = Omrop Fryslȃn
| released =  
| runtime = 98 minutes
| country = Netherlands
| language = Dutch
| budget =
}}
The Silent Raid ( ) is a 1962 Dutch war film directed by Paul Rotha. It shows a true story from World War II: the raid on Leeuwarden prison of December 8, 1944. Without firing a shot, Dutch resistance members disguised as German SD and their prisoners enter the prison, free 39 prisoners and vanish into the city. The Germans were unable to find any of the organizers or escapees.

It was entered into the 3rd Moscow International Film Festival.    The film was one of the most successful in the Netherlands (1,474,000 tickets sold). The scenario was written by Loe de Jong.

==Cast==
* Ab Abspoel as PTT-monteur Jan
* Henk Admiraal as Hein de Zwijger
* Chris Baay as Vos
* Hetty Beck as Eppies mother
* Yoka Berretty as Mies

==References==
 

==External links==
*  

 
 
 
 
 
 


 