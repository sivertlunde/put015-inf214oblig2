House of Horrors
 
{{Infobox film
| name           = House of Horror
| image          = Houseofhorrors.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = Theatrical release poster
| director       = Jean Yarbrough
| producer       = Ben Pivar
| screenplay     = George Bricker
| story          = Dwight V. Babcock Robert Lowery
| music          = William Lava Paul Sawtell Frank Skinner Dimitri Tiomkin
| cinematography = Maury Gertsman 
| editing        = Philip Cahn	
| studio         = 
| distributor    = Universal Pictures 
| released       =   1952 (Re-release)
| runtime        = 65 mins.
| country        = United States
| language       = English
| budget         = 
| gross          =
}}

House of Horrors (also known as Murder Mansion, The Sinister Shadow and Joan Medford is Missing) is a 1946 American horror film released by Universal Pictures, starring Rondo Hatton as a madman named "The Creeper."  It was filmed in September 1945.

An entire series of Creeper movies had been planned, with this being the first, and The Brute Man being the second. However, the sudden death of the main star, Rondo Hatton, prevented any future pictures from being made. It is not known how many films were planned.

==Plot==
Struggling sculptor, Marcel DeLange (Martin Kosleck) is depressed about the events going on in his life, and decides to commit suicide. But just as hes about to kill himself, he spots and saves a madman, named "The Creeper" (Rondo Hatton) from drowning. Shortly afterward, he takes the disfigured man into his care. Marcel also makes the Creeper the subject of his next sculpture and calls it his best creation. But as the reviews begin to break Marcels last nerve, he has the Creeper start killing the critics.

==Cast==

* Rondo Hatton as The Creeper
* Robert Lowery
* Virginia Grey
* Bill Goodwin
* Martin Kosleck
* Alan Napier
* Howard Freeman
* Virginia Christine
* Joan Shawlee

==In popular culture==

The The Rondo Hatton Classic Horror Awards represent Hatton in both name as well as his likeness.  The physical award is a representation of character actor Rondo Hatton, and is based on the bust of The Creeper, portrayed by Hatton in House of Horrors.

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 